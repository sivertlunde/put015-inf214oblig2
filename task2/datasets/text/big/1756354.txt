Mayabazar
 
{{Infobox film
|  name           = Mayabazar
|  image          = Mayabazar.jpg
|  caption        = Poster of the Telugu version
|  story          = Pingali Nagendra Rao
|  screenplay     = Kadri Venkata Reddy
|  director       = Kadiri Venkata Reddy
|  producer       = B. Nagi Reddy Aluri Chakrapani Vijaya Vahini Studios
|  runtime        = 184 minutes 
|  released       =  
|  country        = India Telugu Tamil Tamil
|  cinematography = Marcus Bartley Ghantasala
|  budget         =
|  gross          =
}} epic bilingual Telugu and Tamil languages, Savitri played key roles in both the versions while Gemini Ganesan replaced Akkineni Nageswara Rao in the Tamil version. The film was the eighth in the series of the adaptations of the folk tale Sasirekha Parinayam and revolves around the roles of Krishna and Ghatotkacha in the marriage of Sasirekha, the daughter of Balarama, with Abhimanyu, the son of Arjuna.
 Ghantasala composed the films music and Marcus Bartley was the cinematographer. The film was edited by the duo C. P. Jambulingam and G. Kalyanasundaram, while Madhavapeddi Gokhale-Kaladhar were the films art directors. The Telugu version was released on 27 March 1957, and the Tamil version on 12 April 1957. It was also dubbed into Kannada.
 Filmfare Award for Best Film in Telugu and was also featured at the 1957 International Film Festival of India and Indonesian Film Festival. A poll conducted by CNN-IBN in May 2013 listed it as the greatest Indian film of all time. The Telugu versions digitally remastered and colourised version was released on 30 January 2010 which too was commercially successful.

== Plot ==
:Note: This plot summary refers to the original Telugu version of the film
The story begins when Subhadra, the sister of Balarama and Krishna, marries one of the Pandava  named Arjuna, while their son Abhimanyu falls in love with Balaramas daughter, Sasirekha. Both families decide to conduct their marriage once they reach adulthood. By the time Abhimanyu and Sasirekha turn adults, Pandavas loses in a dice game to the Kauravas due to the manipulation of Sakuni. Krishna is furious for the act of Dussasana insulting Draupadi and saves her. Balarama decides to teach a lesson to the Kauravas and later reaches Hastinapuram. Sakuni and Duryodhana together shows utmost respect towards Balarama and after manipulating him, they seek his favour by asking that Sasirekha be married to Duryodhanas son Lakshmana Kumara. They do it with the intention of forcing Balarama and Krishna to support them if Pandavas wage a war after the completion of their fourteen year exile. Unaware of this, Balarama accepts.

Balaramas wife Revati refuses to honour her previous commitment of conducting the marriage of Sasirekha with Abhimanyu due to the present financial position of the Pandavas. Krishna then manages to send Subhadra and Abhimanyu to Ghatotkachas ashram. Initially, Ghatotkacha assumes them to be intruders in his forest and attacks them, but later apologises for his misunderstanding. When Subhadra narrates about what happened of her son and Sasirekha, Ghatotkacha decides to play some trickery in Dwaraka. First, with the knowledge of Krishna and a servant girl, he flies the sleeping Sasirekha along with her bed, from Dwaraka to his forest. Then using his magical powers, he assumes the form of Sasirekha and goes back to Dwaraka to wreak havoc on her wedding with Lakshmana Kumara.

He creates a Mayabazar (an illusionary palace) and makes Kauravas stay there whilst he recontinues his havoc. On the wedding day, Ghatotkacha makes Lakshmana Kumara lose his mental balance by appearing in various tantalising forms. Meanwhile in the forest, Sasirekha marries Abhimanyu in the presence of Krishna. When Sakuni accuses Krishna for all the misdeeds, the latter is forced to speak the truth using a special box and Ghatotkacha reveals his real identity. While the Kauravas are sent back to Hastinapuram, Sasirekhas parents accept her wedding and welcome the new couple back at the forest. All thank Ghatotkacha for conducting the marriage who then credits Krishna as the mastermind behind everything that occurred.

== Cast ==
;Main Cast
 
* N. T. Rama Rao as Krishna
* S. V. Ranga Rao as Ghatotkacha Savitri as Sasirekha (Telugu) / Vatsala (Tamil)
* Rushyendramani as Subhadra
* Sandhya as Rukmini
* Nagabhushanam as Satyaki
* Allu Rama Lingaiah as Sarma
* Vangara Venkata Subbaiah as Sastry
* Madhavapeddi Satyam as Daaruka
 

;Remaining Cast
 
*Akkineni Nageswara Rao (Telugu) / Gemini Ganesan (Tamil) as Abhimanyu
*Gummadi Venkateswara Rao (Telugu) / D. Balasubramaniam (Tamil) as Balarama
*Mukkamala (Telugu) / R. Balasubramaniam (Tamil) as Duryodhana
*C. S. R. Anjaneyulu (Telugu) / M. N. Nambiar (Tamil) as Sakuni
*Mikkilineni Radhakrishna Murthy (Telugu) / V. K. Srinivasan (Tamil) as Karna
*Chaya Devi (Telugu) / Lakshmi Prabha (Tamil) as Revati
*R. Nageswara Rao (Telugu) / E. R. Sahadevan (Tamil) as Dussasana
*Suryakantham (Telugu) / C. T. Rajakantham (Tamil) as Hidimbi
*Ramana Reddy (Telugu) / V. M. Ezhumalai (Tamil) as Chinnamaya
*Relangi Venkata Ramaiah (Telugu) / K. A. Thangavelu (Tamil) as Lakshmana Kumara
 

== Production ==
=== Development === Vijaya Productions Ghantasala composed the films music and Marcus Bartley was the films cinematographer.   
 name = exchange1958}} Apart from the principal technicians and actors, a crew of 400 studio workers including light men, carpenters, painters and others worked on the film during its production stage.   

=== Casting ===
{{multiple image
 
| align     = right
| direction = horizontal Savitri (right) played the female lead in both versions.
| width     =

 
| image1    = A.Nageswara Rao.jpg
| width1    = 125
| alt1      =
| caption1  =

 
| image2    = Gemini Ganesan.jpg
| width2    = 120
| alt2      =
| caption2  =

 
| image3    = Portrait of Savitri.jpg
| width3    = 136
| alt3      =
| caption3  =
}}
 Telugu and Tamil languages Savitri was the female lead in both versions, with her role being named Sasirekha in the Telugu version, and Vatsala in the Tamil version.  N. T. Rama Rao was initially hesitant to play the role of Krishna in this film but accepted after Venkata Reddy insisted him to do so. Special care was taken on his attires and body language.   This was the first time in his career that Rama Rao played the role of Krishna. 

S. V. Ranga Rao played the role of Ghatotkacha in the film and Nageswara Rao described him as the films male lead in many interviews.  Gummadi Venkateswara Rao and Mikkilineni Radhakrishna Murthy were cast for the roles of Balarama and Karna in the Telugu version respectively while Sita was cast for a supporting role as Sasirekhas maid in the film. Those three, along with Nageswara Rao were the only people alive in the films cast during the release of the digitally remastered version.  Relangi Venkata Ramaiah was cast for the role of Lakshmana Kumara, Duryodhanas son in the film and because of his popularity, a special song was shot featuring him and Savitri.  Allu Rama Lingaiah and Vangara Venkata Subbaiah played the role of Shakunis lackeys, Sarma and Sastry in the film.   Kanchi Narasimha Rao was seen as the disguise of old man donned by Krishna who stops Ghatothkacha when he enters Dwaraka. 

Telugu singer include stage actor Madhavapeddi Satyam made a cameo appearance as Daaruka in the song "Bhali Bhali Bhali Deva" which he sang too. While Ramana Reddy played the role of Chinnamayya, a tantrik teaching witchcraft at Ghatotkachas ashram, Chadalavada and Nalla Ramamurthy played the roles of his apprentices Lambu and Jambu respectively. 

=== Filming ===
During the rehearsals, Venkata Reddy noted the time each actor took to deliver his or her dialogues using a stop watch and calculated the length of each scene (including the song situations) thus arriving at the usage of negative film to the required length.  D. S. Ambu Rao, the assistant of the films cinematographer Marcus Bartley, said that the film was shot strictly by the screenplay and light set by Bartley. The song "Lahiri Lahiri" was shot in Ennore, a suburb of Chennai. The outdoor shooting of the song lasted for 10 to 15 seconds and Bartley created a moonlight illusion for the song which made it the first Indian film to do so, according to Ambu Rao.   

While erecting the set of Dvārakā|Dwaraka, 300 miniature houses were created in about 50 x 60 electrified feet space, with no duplicate houses looking alike. All of them were erected under the supervision of the art director, Madhavpetti Gokhale and Kaladhar.  It took four days of filming to get the right effect for the "Laddu gobbling" shots of Ghatotkacha in the song sequence, "Vivaha Bhojanambu".  In the scene where Ghatotkacha, in the guise of Sasirekha, uses his foot to stamp Lakshmana Kumaras foot, the expression was Savitri’s, while the foot used for stamping was that of the film’s choreographer Pasumarthi Krishnamurthys. He pasted false hair on his foot to give it the look of that of a demons. 

== Themes and influences ==
Mayabazar is a mythological film adapted from a fictional folk tale based on Mahabharatha. Film artiste trainer and director L. Satyanand cited that Mayabazar can be labelled a science fiction or comedy satire, touching all aspects from the genres of action to drama and comedy to science fiction.    The film basically revolves around the love story of Balaramas daughter Sasirekha (Vatsala in Tamil) and Arjunas son Abhimanyu. 

The characters of Krishna and Balarama along with their respective wives have difference of opinions over the couples love affair. To introduce the films theme, Venkata Reddy uses a magic box equivalent to a Television screen which displays whatever is dear to the viewers heart. In that box, Sasirekhas character sees her lover Abhimanyu, Balarama sees his favourite disciple Duryodhana and Balaramas wife seeing an array of jewellery, indicating her materialistic behaviour. 

Mayabazar was opined as a tribute to the Telugu culture, language and customs of the land; especially in the scenes where Balarama rudely asks Subhadra to leave when she confronts him with displeasure at his decision to separate Sasirekha and Abhimanyu.  Though the film is a story of Pandavas and Kauravas with the Yadavas pitching in, the Pandavas are never shown but are only heard of throughout the film. 

== Music ==
{{Infobox album
| Name      = Mayabazar
| Longtype  =
| Type      = Soundtrack Ghantasala
| Cover     = Mayabazar Album Cover.jpg
| Caption   = Album cover of the Telugu version
| Released  = 1957 Feature film soundtrack
| Length    = 44:10  (Telugu) , 39:17  (Tamil)  Telugu
| HMV
| Producer  = Ghantasala
}} Ghantasala orchestrating and recording Rajeswara Raos compositions with N. C. Sen Gupta and A. Krishnamurthy besides composing the rest of the films soundtrack and the entire background score.     
 Pingali Nagendra Surabhi Nataka Samajams plays of the 1950s which again was sourced from the 1940s Janaki Sapadham harikatha records of B. Nagarajakumari.   
 Charles Penroses The Laughing HMV and the album cover depicts a still image of S. V. Ranga Rao portraying Ghatotkacha.

{{Tracklist
| collapsed       =
| headline        = Telugu Tracklist
| extra_column    = Artist(s)
| total_length    = 41:10
| title1          = Neeve Naa
| extra1          = Ghantasala (singer)|Ghantasala, P. Leela
| length1         = 02:48
| title2          = Choopulu Kalisina Shubhavela
| extra2          = Ghantasala, P. Leela
| length2         = 03:13
| title3          = Lahiri Lahiri
| extra3          = Ghantasala, P. Leela
| length3         = 03:49
| title4          = Bhali Bhali
| extra4          = Madhavapeddi Satyam
| length4         = 02:55
| title5          = Neekosame
| extra5          = Ghantasala, P. Leela
| length5         = 03:25
| title6          = Aha Naa Pellanta
| extra6          = P. Susheela, Ghantasala
| length6         = 02:33
| title7          = Sundari Savitri
| length7         = 02:17
| title8          = Vivaha Bhojanambu
| extra8          = Madhavapeddi Satyam
| length8         = 02:26
| title9          = Vinnava Yesodhamma
| extra9          = P. Susheela, P. Leela
| length9         = 03:37
| title10         = Dayacheyandi
| extra10         = Ghantasala, K. Ravi, Pithapuram Nageswara Rao, P. Susheela
| length10        = 07:21
| title11         = Srikarulu Devathalu
| extra11         = M. L. Vasanthakumari
| length11        = 03:48
| title12         = Vardhillavamma
| extra12         = P. Leela
| length12        = 02:58
}}

{{Tracklist
| collapsed       =
| headline        = Tamil Tracklist
| extra_column    = Artist(s)
| total_length    = 38:17
| title1          = Patupadum Kuiliname
| extra1          = P. Leela
| length1         = 03:21
| title2          = Aaha Inba Nilavinile
| extra2          = Ghantasala (singer)|Ghantasala, P. Leela
| length2         = 02:45
| title3          = Neethana Ennai
| extra3          = Ghantasala, P. Leela
| length3         = 02:45
| title4          = Thangame Un Pole Savitri
| length4         = 02:17
| title5          = Dum Dumyen Kalyanam
| extra5          = Jikki, Ghantasala
| length5         = 02:39
| title6          = Kannudan Kalandhidum Subadhiname
| extra6          = Ghantasala, P. Leela
| length6         = 03:13
| title7          = Kalyana Samayal Saadham
| extra7          = Trichi Loganathan
| length7         = 02:26
| title8          = Unakkagave Naan Uyir Vaazhvene
| extra8          = Ghantasala, P. Leela
| length8         = 03:25
| title9          = Dhayai Seiveerey
| extra9          = S. C. Krishnan, Sirkazhi Govindarajan, P. Susheela
| length9         = 06:18
| title10         = Pambaramaiaadalaam
| extra10         = Jikki, P. Susheela
| length10        = 03:21
| title11         = Bale Bale
| extra11         = Sirkazhi Govindarajan
| length11        = 02:55
| title12         = Vinnava Yashodamma
| extra12         = P. Leela
| length12        = 03:37
}}

== Release == Filmfare Award Kannada and was featured at the 1957 International Film Festival of India and at the Indonesian Film Festival.  
 Public Gardens in Hyderabad to celebrate its 50th anniversary on 7 April 2007.    The celebration was jointly organised by the Department of Culture, Andhra Pradesh; Film, TV and Theatre Development Corporation and Kinnera Art Theatres.  Akkineni Nageshwara Rao and C. Narayan Reddy, who were associated with the film, were felicitated on the occasion. 

=== Critical reception ===
Mayabazar received positive reviews from critics, particularly for the work of the technical crew. W. Chandrakanth of The Hindu wrote  The greatness of the director lies here – he successfully reduces all characters to ordinary mortals displaying all the follies of human beings except Ghatothkacha or Krishna. And then he injects into the Yadava household a Telugu atmosphere, full with its simile, imagery, adage, sarcasm and wit. The result – a feast for the eyes and soul. That is Mayabazar for you.   Vijaysree Venkatraman, writing for The Hindu in 2008, remarked that the "Special effects in this summers Hollywood superhero movies were spectacular, but, for me, the mythological   hasnt lost any of its magic." She added, "If watching the genial half-demon polish off a wedding feast single-handedly remains a treat, seeing the greedy duo from the grooms side get whacked alternately by the furniture and the wilful carpet has me in splits".  M. L. Narasimham of The Hindu wrote "Though there were several movie versions in various Indian languages, the 1957 Vijaya Productions, Mayabazar is still considered the best for its all round excellence". 

G. Dhananjayan in his 2011 book The Best of Tamil Cinema, 1931 to 2010 spoke positively towards the production of the film, praising the script and the music. He mentioned Savitris performance as the films highlight, especially in the scenes where Ghatotkacha assumes her form.   The Times of India stated, "With a powerful cast and a strong script, this movie is a stealer. Savitri, NTR, ANR, SV Rangarao and not to mention Suryakantam add layers to their characters. Of course, the language and the dialogues, simply unbeatable. Some of the scenes are simply hilarious." 

== Digitisation and colourisation ==
 
Mayabazar was the first Telugu film transformed from black and white to colour version,  with a re-mastered audio from mono track to a DTS 5.1 channel system.    In late November 2007, a Hyderabad based company named Goldstone Technologies acquired the world negative rights of 14 classic Telugu films produced by Vijaya Vauhini Studios, Mayabazar being one of them, to release their digitally re-mastered versions in colour.   C. Jagan Mohan of Goldstone Technologies had years of experience during his work in AIR which made him think of redoing the sound on DTS. A team of 165 people worked for eight months. He used 180,000 shades of colour to arrive at a tone similar to that of human skin and used 16.7 million colour technology.    They had to restore the audio, clear the distortion, raise volume of the vocals and find instrumental musicians to perform the same background music to record them on seven tracks instead of a single track. The sound effects were also remastered. 

 
Apart from colouring the clothes and jewellery, Mohan said that the song "Vivaha Bhojanambu" and the wedding scene in the climax were the most challenging sequences, as food items should look more realistic after colourisation in the former. For the latter, Mohan explained  Each and every rose petal strewn on the pathway had to be colored. Further, each frame in the climax has many actors. In technical parlance, we refer to a set of colours used for skin tone, clothes, jewelry and so on as different masks. If five or six masks were used on one character, the presence of many actors in a frame called for that much more work.   Three songs "Bhali Bhali Deva", "Vinnavamma Yashodha" and "Choopulu Kalisna Subha Vela" along with many poems were removed/deleted from the remastered colour version to maintain print quality. 
 name = exchange2008}} The colour version was released on 30 January 2010 in 45 theatres in Andhra Pradesh.   The colour version received positive reviews and became a commercial success.  Regarding the colourised version, M. L. Narasimham of The Hindu felt that the effort was "laudable, but the soul was missing" and added "Get a DVD of the original (Black & White) movie, watch it and you will agree with ANR who while talking about Mayabazar once exclaimed, What a picture it was!". 

A government order issued on 29 January 2010 stated that the remastered version has been exempted from entertainment tax. However theatre owners charged full and they along with other film producers lacked clarity about the orders validity.  Despite its success, Mohan decided not to remaster the remaining 14 films saying that most of the producers who sold the rights of the negatives to TV channels lost control over them adding that there were a lot of legal issues over ownership and copyright issues whenever other producers try to do something on their own. 

== Legacy ==
  (statue pictured) portray the role of Krishna in many unrelated Telugu films making him an ideal actor for Krishnas character in Telugu cinema till date.]]
The film is considered as one of the classics in Telugu cinema, particularly for its use of technology.  It is also remembered for Nagendra Raos dialogues "Evaru puttinchakunte maatalela pudathayi" ("How would words emerge if none invents them"), "Subhadra, aagadalu, aghaaityalu naaku paniki raavu" ("Subhadra, these atrocities mean nothing to me") and phrases such as "antha alamalame kada" ("Is everything fine"), "Asamadiyulu" ("Friends"), "Tasamadiyulu" ("Enemies"), "Gilpam" and "Gimbali" (antonyms of "Bed and Room mat") which later became a part of Telugu vernacular. 
 Vara Vikrayam (1939), Bhakta Potana (1942), Shavukaru (1950), Malliswari (1951 film)|Malliswari (1951) Peddamanushulu (1954) and Lava Kusa (1963) as those films which made an impact on the society as well as Telugu cinema post their release.  Rama Rao later reprised the role of Krishna in various unrelated Telugu and Tamil films spanning a career of two decades.    
 The Ten Commandments (1956), Ben-Hur (1959 film)|Ben-Hur (1959), Sholay (1975) and Avatar (2009 film)|Avatar (2009), saying that they "are evergreen and never fade away from the mind", adding that they appealed to the people of their respective generations and continue to do so to this day.  Satyanand praised Bartleys cinematography stating that the film  was definitely ahead of its time. It is still a mystery how Marcus Bartley could morph Sasirekha through the ripples in the pond. It was an absolute masterpiece, considering the equipment in use, those days. In the absence of hi-def cameras, computer generated visual effects and high-end computers, the direction, cinematography and visual effects were efforts of sheer human genius.  

On the centenary of Indian cinema in April 2013, CNN-IBN included Mayabazar in its list, "The 100 greatest Indian films of all time".  A poll conducted by CNN-IBN in May 2013 listed it as the greatest Indian film of all time.  In August 2013, Nageswara Raos son actor Akkineni Nagarjuna wished the film be remade in accordance to the tastes of the present day audience.  The Andhra Pradesh State Government planned to introduce the film as a part of the tenth standard English syllabus from 2014. The fourth unit of the English text book was named "Films and Theaters" and there was a special mention of Mayabazar and its actors along with two images from the film. 

== In popular culture == 2009 Telugu film. 
 1987 Telugu 2011 Telugu film directed by Veerabhadram Chowdary were named after "Aha Naa Pellana". Both of them were successful.  In Rajanna (2011), a period drama, the films central character Mallamma played by Baby Annie is shown listening to the songs of Mayabazar and the same was criticised because of the films time period. However its director K. V. Vijayendra Prasad defended himself saying that Rajanna was set in 1958, a year after the release of Mayabazar. 
 Gopala Gopala (2015) where Pawan Kalyan played the role of Krishna.  A Tamil song written by Thamarai and composed by Harris Jayaraj for the film Yennai Arindhaal (2015) was named after Mayabazar. 

== Notes ==
 

== References ==
 

== Bibliography ==
*  
*  
*  

==External links==
*  
 
 

 
 
 
 
 
 
 
 
 