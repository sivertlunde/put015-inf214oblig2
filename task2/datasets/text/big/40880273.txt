Counsel for Crime
{{Infobox film
| name           = Counsel for Crime
| image          = Counsel for Crime film poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = John Brahm
| producer       = Wallace MacDonald 
| screenplay     = Fred Niblo, Jr. Grace Neville Lee Loeb Harold Buchman 
| based on       = a story by Harold Shumate
| narrator       = 
| starring       = Otto Kruger Douglass Montgomery Jacqueline Wells
| music          = Morris Stoloff
| cinematography = Henry Freulich Otto Meyer
| distributor    = Columbia Pictures
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Counsel for Crime is a 1937 American  crime film directed by John Brahm starring Otto Kruger, Douglass Montgomery and Jacqueline Wells. 

==Plot==
Following his graduation from law school, Senator Robert Maddoxs (Hall) adopted son Paul (Montgomery) is offered a job at Bill Mellons (Kruger) law firm. Mellon, an unscrupulous criminal lawyer, is actually Pauls real father, but keeps the fact a secret from him. When Paul finds out about Mellons corruption, he quits and gets a job as assistant district attorney. Soon after taking the position, Paul spearheads a state investigation into legal malpractice. This worries Mellon and prompts him to assign a crook to implicate those investigating him in a scandal. When the crook finds out the truth about Pauls birth he is accidentally shot by Mellon in an effort to conceal the information. Paul successfully prosecutes his own father, who is convicted of second-degree murder because he refused to discuss the content of the papers over which he and Mitchell were struggling, thus protecting Paul and his mother.

==Cast== 
 
 
* Otto Kruger as Bill Mellon 
* Douglass Montgomery as Paul Maddox  Jacqueline Wells as Ann McIntyre 
* Thurston Hall as Senator Robert Maddox 
* Nana Bryant as Mrs. Maddox 
* Gene Morgan as Friday 
* Marc Lawrence as Edwin Mitchell 
* Robert Warwick as Asa Stewart  Stanley Fields as George Evans
  
* Joe Caits as Hood (uncredited) Harvey Clark as Shyster (uncredited)
* Dick Curtis as Hood (uncredited)
* Harry Depp as Shyster (uncredited)
* Dick Elliott as Bundy (uncredited)
* Bud Jamison as Jailer (uncredited) George Lloyd as Jed Sawyer (uncredited)
* George Offerman Jr. as Runner (uncredited)
* Guy Usher as Judge (uncredited)
* Lloyd Whitlock as Prosecuting Attorney (uncredited) 
 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 