Ultraman: Great Monster Decisive Battle
{{Infobox Film
| name = Ultraman: Great Monster Decisive Battle
| image = Ultraman-_Great_Monster_Decisive_Battle.jpg
| image_size = 
| caption = Theatrical poster for Ultraman: Great Monster Decisive Battle (1979)
| director = Noriko Anakura 
| producer = Noboru Tsuburaya
| writer = Toshihiro Iijima
| narrator = 
| starring = Akiji Kobayashi
| music = Kunio Miyauchi
| cinematography = 
| editing = 
| distributor = Tsuburaya Productions
| released = July 21, 1979
| runtime = 100 min.
| country = Japan Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}

  is a 1979 Japanese tokusatsu kaiju film produced by Tsuburaya Productions, consisting of re-edited material from the original television series Ultraman.

== Cast ==
*Akiji Kobayashi
*Susumu Kurobe
*Sandayū Dokumamushi
*Masanari Nihei
*Hiroko Sakurai

==Monsters==
*Eleking
*Zetton
*Astromons
*Chameleking
*Alien Atlanta
*Birdon
*Bedoran
*Keronia
*Suflan
*Magular
*Pigmon Red King
*Chandrah
*Neronga
*Alien Baltan
*Giggas
*Dorako

==Availability==
Ultraman: Great Monster Decisive Battle was released on DVD in Japan on September 13, 2006. 

==Bibliography==
*  
*  
*  

==Notes==
 

 

 
 
 
 
 
 
 
 


 