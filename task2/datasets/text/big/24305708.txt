Auf Wiedersehen (film)
{{Infobox film
| name           = Auf Wiedersehn
| image          = Auf Wiedersehn.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Harald Philipp
| producer       = 
| writer         = Harald Philipp 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{plainlist|*Gert Fröbe
*Werner Peters
*Elke Sommer}}
| music          = Gert Wilden
| cinematography = Friedl Behn-Grund
| editing        = Elisabeth Neumann
| studio         = Alfa-Film GmbH 
| distributor    = Ufa Film Hansa GmbH & Co. 
| released       =   
| runtime        = 99 minutes 
| country        = West Germany 
| language       = 
| budget         = 
| gross          = 
}}
 West German film directed by Harald Philipp and starring Gert Fröbe, Joachim Fuchsberger, Günter Pfitzmann, Werner Peters and Elke Sommer. It was based on a novel by Reinhold Pabel and features Louis Armstrong in a cameo role.   

==Cast==
* Gert Fröbe – Angelo Pirrone
* Joachim Fuchsberger – Ferdinand Steinbichler
* Günter Pfitzmann – Willi Kuhlke
* Elke Sommer – Suzy Dalton
* Werner Peters – Paul Blümel
* Margot Eskens – Anna Kuhlke
* Heinz Weiss – Steve OHara
* Kurd Pieritz – Gus Wheeler
* Fritz Tillmann – George Dalton
* Stanislav Ledinek – Konrad Czerny
* Hans W. Hamacher – William Shake
* Peter Capell – Louis Holloway
* Heinrich Gies – Don Howley
* Hendrik Sick – Pietro
* Louis Armstrong – Himself
* Kurt Pratsch-Kaufmann – Mario Malfi

==References==
 

 

 
 
 
 
 