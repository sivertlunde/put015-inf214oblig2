The Amorous Adventures of Moll Flanders
 
{{Infobox film
| name = The Amorous Adventures of Moll Flanders 
| image = UK quad of Moll Flanders.jpg
| caption = British cinema poster for The Amorous Adventures of Moll Flanders Terence Young
| editing = Frederick Wilson Richard Johnson Angela Lansbury Vittorio De Sica Leo McKern George Sanders Lilli Palmer
| music = John Addison
| studio = Winchester Productions
| distributor = Paramount Pictures
| country = United Kingdom
| running time = 126 mins. 
| release date = 1965 
| gross = $2,000,000 (US/ Canada rentals) 
}}
 historical comedy Terence Young Richard Johnson and Angela Lansbury.  It is based on the 1722 novel Moll Flanders by Daniel Defoe.

==Casting==
 Richard Johnson Tom Jones but she had other commitments.  Had she appeared in the film Sean Connery would have played the male lead.

==Plot==
In the 18th Century, an orphan, Moll Flanders, grows up to become a servant for the towns mayor, who has two grown sons. Moll is seduced and abandoned by one, then marries the other, a drunken sot who dies, making her a young widow.

Moll is employed by Lady Blystone to be a servant. She meets a bandit, Jemmy, who mistakes her for the lady of the house and begins to woo her, pretending to be a sea captain. Moll rebuffs the advances of Lady Blystones actual lover, the Count, only to be fired from her job when they are spotted together.

A banker marries Moll but quickly loses her when a gang of thieves spirits her away. Moll ends up in jail and finds Jemmy there as well. Their execution is at hand when the banker, finding her there, dies of a sudden heart attack. Now a wealthy widow, Moll buys freedom for herself and her true love, and she and Jemmy have a shipboard wedding.

==Cast==
* Kim Novak - Moll Flanders  Richard Johnson - Jemmy 
* Angela Lansbury - Lady Blystone 
* Leo McKern - Squint 
* Vittorio De Sica - The Count 
* George Sanders - The Banker 
* Lilli Palmer - Dutchy 
* Peter Butterworth - Grunt 
* Noel Howlett - Bishop 
* Dandy Nichols - Orphanage Superintendent 
* Cecil Parker - The Mayor 
* Barbara Couper - The Mayors wife  Daniel Massey - Elder Brother 
* Derren Nesbitt - Younger Brother 
* Ingrid Hafner - Elder Sister 
* June Watts - Younger Sister 
* Anthony Dawson - Officer of Dragoons 
* Judith Furse - Miss Glowber 
* Roger Livesey - Drunken Parson 
* Jess Conrad - First Mohock 
* Noel Harrison - Second Mohock  Alex Scott - Third Mohock 
* Alexis Kanner - Fourth Mohock 
* Mary Merrall - A Lady 
* Richard Wattis - Jeweler 
* Terence Lodge - Draper 
* Basil Dignam - Lawyer 
* Reginald Beckwith - Doctor 
* David Hutcheson - Doctor  David Lodge - Ships captain 
* Lionel Long - Singer in Prison 
* Hugh Griffith - Prison Governor 
* Michael Trubshawe - Lord Mayor of London 
* Richard Goolden - The Ordinary 
* Leonard Sachs - Prison Doctor  Michael Brennan - The Turnkey 
* Liam Redmond - Convict Ship Captain 
* Neville Jason - Convict Ship Officer
* Claire Ufland - Young Moll

==Reception==
The Amorous Adventures of Moll Flanders was one of the 13 most popular films in the UK in 1965. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 