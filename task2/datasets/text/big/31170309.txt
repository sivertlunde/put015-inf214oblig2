Pestalozzi's Mountain
 
{{Infobox film
| name           = Pestalozzis Mountain
| image          = Pestalozzis Mountain.jpg
| caption        = Film poster
| director       = Peter von Gunten
| producer       = Alfred Nathan Peter Schneider Peter von Gunten
| starring       = Gian Maria Volonté
| music          = 
| cinematography = Jürgen Lenz
| editing        = Lotti Mehnert
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = East Germany Switzerland
| language       = German
| budget         = 
}}

Pestalozzis Mountain ( ) is a 1989 East German–Swiss drama film directed by Peter von Gunten. It was entered into the 39th Berlin International Film Festival.   

==Cast==
* Gian Maria Volonté as Pestalozzi
* Rolf Hoppe as Zehender
* Heidi Züger as Mädi
* Christian Grashof as Zschokke
* Michael Gwisdek as Perrault
* Corinna Harfouch as Juliette Benoit
* Silvia Jost as Magd
* Angelica Ippolito as Anna
* Peter Wyssbrod as Hofstetter
* Käthe Reichel as Alte Köchin
* Isolde Barth as Rosalia
* Mathias Gnädinger as Büttel
* Roger Jendly as Kutscher
* Leandra Zimmermann as Kathrin

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 

 
 