Old Mother Riley, MP
{{Infobox film
| name           = Old Mother Riley, MP
| image          = "Old_Mother_Riley,_MP"_(1939).jpg
| image_size     =
| caption        = Front of house still
| director       = Oswald Mitchell 
| producer       = F.W. Baker
| writers        = Oswald Mitchell  Con West 
| narrator       =
| starring       = Arthur Lucan   Kitty McShane   Torin Thatcher   Henry B. Longhurst
| music          = Percival Mackey
| cinematography = Geoffrey Faithfull
| editing        = Daniel Birt
| studio         = Butchers Film Service
| distributor    = Butchers Film Service
| released       = 15 August 1939 (London)(UK) 20 January 1940 (UK general release)	
| runtime        = 77 minutes
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}Old Mother Riley, MP is a 1939 British comedy film starring Arthur Lucan and Kitty McShane, which forms part of the Old Mother Riley series of films. The films plot centres on Old Mother Riley standing for election to the House of Commons.

==Plot==
Concerning Old Mother Rileys fight against her landlord, and as a means of defeating a corrupt politician intent on demolishing her street and the local pub along with it, Mother Riley taking to the soapbox. Local crowds cheer her on, and she finds herself  elected to Parliament, and eventually promoted to cabinet Minister for Strange Affairs.     

==Cast==
*Old Mother Riley -	Arthur Lucan
*Kitty Riley -	Kitty McShane
*Jack Nelson -	Torin Thatcher
*Henry Wicker -	Henry Longhurst
*Archie -	Patrick Ludlow
*Emperor of Rocavia -	Dennis Wyndham
*Littleman -	Rex Alderman
*The Supervisor -	Cynthia Stock
*Brownlow -	Kenneth Henry

==Critical reception==
*Mary Haberstroh writes in Britmovie, "“Old Mother Riley, MP” synthesizes slapstick comedy with malapropisms and everything in between to create one of the best Old Mother Riley films...Lucan is credible as Old Mother Riley wanting to seek social equality for the poor and unemployed, and Kitty McShane, Torin Thatcher, and Henry B. Longhurst turn in stellar performances in this comedy. The story itself is well written and continuous without any unexplained gaps in the plot, making it easy for the viewer to follow...The characters in the film could very well be real life figures who are trying to survive during the time of the Great Depression. “Old Mother Riley, MP” is a film that is sure to please anyone who appreciates classic British comedy."  
*In britishpictures.com, David Absalom writes, "by any reasonable definition, this is not a good film. But the character (Old Mother Riley) is so watchable, so wonderfully manic, you enjoy the film despite its faults."  
*TV Guide wrote, "graced with an insane plot...Nonsensical but full of laughs."  

==External links==
* 

==References==
 

 

 
 
 
 
 
 
 
 


 