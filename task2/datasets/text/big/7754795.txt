The Front Page (1974 film)
{{Infobox film
| name           = The Front Page
| image          = Front page movie poster.jpg
| caption        = Original poster by Bill Gold
| director       = Billy Wilder
| producer       = Paul Monash
| screenplay     = Billy Wilder I.A.L. Diamond
| based on       =  
| narrator       =
| starring       = Jack Lemmon Walter Matthau Charles Durning David Wayne Susan Sarandon Austin Pendleton Carol Burnett
| music          = Billy May
| cinematography = Jordan Cronenweth
| editing        = Ralph E. Winters
| distributor    = Universal Pictures
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $4 million Chandler, Charlotte, Nobodys Perfect: Billy Wilder, A Personal Biography. New York: Simon & Schuster 2002. ISBN 0-7432-1709-8, pp. 278-285    The Numbers. Retrieved July 7, 2012. 
| gross          = $15,000,000  
}}
 play of the same name (1928), which  inspired several other films.

== Plot == Chicago Examiner leftist whose yellow press of Chicago has painted him as a dangerous threat from Moscow. As a result the citizenry are anxious to see him put to death.
 scoop of whore from Division Street" who befriended Earl, creates a distraction by leaping from the third-floor window.
 reprieve by the governor.  Walter grudgingly accepts that he is losing his ace reporter and presents him with a watch as a token of his appreciation.  Hildy and Peggy set off to get married, and Walter telegraphs the next railway station to alert them that the man who stole his watch is on the inbound train and should be apprehended by the police.

== Cast ==
* Jack Lemmon as Hildebrand Hildy Johnson
* Walter Matthau as Walter Burns
* Susan Sarandon as Peggy Grant
* Vincent Gardenia as Sheriff "Honest Pete" Hartman
* David Wayne as Roy Bensinger
* Allen Garfield as Kruger
* Charles Durning as Murphy
* Herb Edelman as Schwartz
* Austin Pendleton as Earl Williams
* Carol Burnett as Mollie Malloy
* Martin Gabel as Dr. Max J. Eggelhofer
* Harold Gould as The Mayor/Herbie/Green Hornet John Furlong as Duffy
* Jon Korkes as Rudy Keppler
* Cliff Osmond as Officer Jacobi
* Lou Frizzell as Endicott
* Paul Benedict as Plunkett
* Dick ONeill as McHugh
* Biff Elliot as Police Dispatcher
* Barbara Davis as Myrtle

==Production== in 1931 and as His Girl Friday in 1940. Billy Wilder was quoted by his biographer Charlotte Chandler as saying: "Im against remakes in general" "because if a picture is good, you shouldnt remake it, and if its lousy, why remake it? . . . It was not one of my pictures I was particularly proud of." 

After years of producing his films, he passed producing chores to Paul Monash and concentrate on screenwriting and directing when Jennings Lang suggested he film a new adaptation of The Front Page for Universal Pictures. The idea appealed to Wilder, a newspaperman in his younger days, who recalled, "A reporter was a glamorous fellow in those days, the way he wore a hat, and a raincoat, and a swagger, and had his camaraderie with fellow reporters, with local police, always hot on the tail of tips from them and from the fringes of the underworld." Whereas the two earlier screen adaptations of the play were set in their contemporary times, Wilder decided his would be a period piece set in 1929, primarily because the daily newspaper was no longer the dominant news medium in 1974. 

Wilder hired Henry Bumstead as production designer. For exterior shots, Bumstead suggested Wilder film in San Francisco, where the buildings were a better match for 1920s Chicago than was Los Angeles. The final scene on the train was filmed in San Francisco, where a railroad enthusiast provided a vintage railway car for the setting.  The interior shot of the theater in an earlier scenes was done at the Orpheum Theatre in Los Angeles. The opening credits scenes were filmed at the Los Angeles Herald Examiner.

Wilder and Diamond insisted their dialogue be delivered exactly as written and clearly enough to be understood easily. Jack Lemmon, who portrayed Hildy Johnson, later said, "I had one regret about the film. Billy would not let us overlap our lines more. I think that would have made it better . . . I feel its a piece in which you must overlap. But Billy, the writer, wanted to hear all of the words clearly, and he wanted the audience to hear the words. I would have liked to overlap to the point where you lost some of the dialogue." 

Because of Wilders tendency to "cut in the camera", a form of spontaneous editing that results in a minimal footage being shot, editor Ralph E. Winters was able to assemble a rough cut of the film four days after principal photography was completed. 

While the film was Wilders first to show a profit since Irma la Douce (1963), the director regretted not sticking to his instincts over remakes. 

==Critical reception==
Vincent Canby of The New York Times thought the story was "a natural" for Wilder and Diamond, who "have a special (and, to my mind, very appealing) appreciation for vulgar, brilliant con artists of monumental tackiness." He continued, "Even though the mechanics and demands of movie-making slow what should be the furious tempo, this Front Page displays a giddy bitterness that is rare in any films except those of Mr. Wilder. It is also, much of the time, extremely funny." He described Walter Matthau and Austin Pendleton as "marvelous" and added, "Mr. Lemmon is comparatively reserved as the flamboyant Hildy, never quite letting go of his familiar comic personality to become dominated by the lunacies of the farce. He always remains a little outside it, acting. Carol Burnett has an even tougher time as Molly Malloy . . . This role may well be impossible, however, since it requires the actress to play for straight melodrama while everyone around her is going for laughs . . . Mr. Wilder has great fun with the period newspaper detail . . . and admires his various supporting actors to such an extent that he allows them to play as broadly as they could possibly desire." He concluded, "The hysteria is not as consistent as one might wish, nor, indeed, as epic as in Mr. Wilders own One, Two, Three. The cohesive force is, instead, the directors fondness for frauds, which, I suspect, is really an admiration for people who barrel on through life completely intimidating those who should know better." 

The British television network Channel 4 called it the "least satisfying screen adaptation of Hecht and MacArthurs play," saying it "adds little to the mix other than a bit of choice language. The direction is depressingly flat and stagy, Wilder running on empty. While it is easy to see why he was attracted to this material . . . he just does not seem to have the energy here to do it justice. Matthau and Lemmon put in their usual faultless turns, but cannot lift a pervading air of pointlessness." 

TV Guide rated the film 2½ out of four stars and noted, "This slick remake of the ebullient original falls short of being the film it could have been, despite the presence of master filmmaker Wilder and his engaging costars . . . Despite the obvious charismatic interaction between Lemmon and Matthau, the film is oddly stilted. In an overly emphatic turn, the miscast Burnett easily gives the most awful performance of her career. She projects only one emotion - a gratingly annoying hysteria. One never enjoys the film so much as when her character throws herself out of a window." 

Burnett said in This Time Together that she was so displeased with her performance that when she was on an airplane where the film was shown, she apologized on the planes intercom.  

The play again served as an inspiration for film makers with Switching Channels released in 1988.

==Awards and nominations== The Longest Yard, and Lemmon and Matthau, competing with each other for the Golden Globe Award for Best Actor – Motion Picture Musical or Comedy, lost to Art Carney in Harry and Tonto.
 Writers Guild The Apprenticeship of Duddy Kravitz. 
 David di Conversation Piece.

==DVD release== fullscreen format with an audio track in English and subtitles in English, Spanish, and French.
On May 31, 2005 it was re-released in a widescreen edition DVD by Universal Home Video.

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 