Brothel (film)
{{Infobox film
| name           = Brothel
| image          = Brothel.promo.poster.jpg
| caption        =
| director       = Amy Waddell
| producer       = Ira Deutchman Wade W. Danielson Amy Waddell
| writer         = Amy Waddell
| starring       = Serena Scott Thomas Brett Cullen Timothy V. Murphy Bruce Payne Grace Zabriskie Sarah Lassez Andrea Morris Lisa Baines Yvonne Scio
| music          = Mark Adler Anthony Marinelli
| cinematography = Seo Mutarevic
| editing        = Daniel Lawrence
| distributor    = Mount Parnassus Pictures MovieHouse Entertainment
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Jerome and Clarkdale, Arizona.

==Plot==
 Jerome which Death himself (Bruce Payne). 

==Cast==
* Serena Scott Thomas as Julianne
* Brett Cullen as Avery
* Bruce Payne as Thief
* Grace Zabriskie as Madam Lady Sadie
* Timothy V. Murphy as Gayle/Brian
* Sarah Lassez as Sophie
* Andrea Morris as Maddy
* Lisa Baines as Priscilla
* Yvonne Scio as Maddy

==Reception==

John Reid, who reviewed the film for the Sedona Red Rock News, noted "the density of atmosphere and the intensity of the actors and crew palpable on the set".  David Kanowsky, who reviewed the film for Kudos, stated that it was "a very fine ghost story without horror".  A contributor to the Independent Film Quarterly described the film as "an erotic indie version of Inception". 

==References==
 

==External links==
*  
*  

 
 
 
 
 


 