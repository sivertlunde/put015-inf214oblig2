Madurai Veeran (film)
 
{{Infobox film
| name           = Madurai Veeran
| image          = Mvmain.png
| image_size     =
| caption        =
| director       = Vincent Selva
| producer       =Sayed Kadi Teja
| narrator       =
| starring       = Githan Ramesh Saloni Aswani Karunas
| music          = Srikanth Deva
| cinematography = I. Andrew
| editing        = V. Jaishankar
| distributor    =
| released       = April 18, 2007
| runtime        =
| country        = India
| language       = Tamil
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Madurai Veeran is a Tamil romance-action film released in 2007.

The film stars Githan Ramesh and Saloni Aswani in the lead roles. The film is directed by Vincent Selva. The score and soundtrack are composed by Srikanth Deva. The film received largely negative reviews. The film was a remake of Telugu film Nuvvu Nenu directed by Teja.

==Plot==
Shiva (Githan Ramesh) is the only son of a rich businessman (Avinash). He grows up surrounded by money and he is not cared for by his parents, due to his mother dying when he was a young boy and his father is business-minded and only cares about his money. Shiva and his friends are a playful bunch, they do not study and they mainly do lots of sport. Priya (Saloni), who also goes to the college (the college was created and run by Shivas family) that Shiva goes to, is the daughter of a cow herder and a gangster Mayandi (Lal). Priya keeps her distance from Shiva, but as the film progresses it is inevitable that they become friends. Their friendship then develops into romance, but this love is opposed by both their parents. Both their parents try to separate them, they try to get them married of to different people. They eventually run away together, and a hit man is after Priyas life (hired by Shivas father). In a confrontation with the hit-man Shiva gets wounded and defeats the hit-man, he then storms into his fathers office and throws the hit-man at him. He then informs his father of his wedding in front of the district court. His father turns up with a large battalion of police officers, while Shiva turns up with a much large number of students. The film ends with their wedding successfully happening, and Mayandi gives his blessings.

==Production==
===Cast===
* Githan Ramesh as Shiva
* Saloni Aswani as Priya
* Lal as Mayandi
* Avinash
* Ganja Karuppu

==Audio track==
Songs composed by Srikanth Deva.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Length (m:ss) || Picturization ||Lyrics ||Notes
|-
| 1|| "Yadava Theruvili" ||  Gana Ulaganathan|| 4:13 || || ||
|-
| 2|| "Kalaiyum Neeye" || Srikanth Deva & Beaby Srikanth Deva || 4:47 || || ||
|-
| 3|| "Mudhal Mudhalaga" || Shreya Ghoshal || 4:34 || || ||
|-
| 4|| "Nee Thaanadi" || Harish Raghavendra || 3:51 || || ||
|-
| 5|| "Uyirum Uyirum" || Senthil Das & Susithira Raaman||3:10  || || ||
|-
| 6|| "Nyokka Makka" || Srikanth Deva, Senthil Dass & Priya || 4:05  || || ||
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 