Living on Velvet
{{Infobox film
| name           = Living on Velvet
| image          = Living-on-velvet-1935.jpg
| caption        = Theatrical release poster
| director       = Frank Borzage
| producer       = Frank Borzage Julius Epstein Screenplay: Jerry Wald Julius Epstein
| narrator       = 
| starring       = Kay Francis Warren William George Brent
| music          = 
| cinematography = Sidney Hickox William Holmes
| distributor    = Warner Brothers
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         =
}}
Living on Velvet is a 1935 American film directed by Frank Borzage starring Kay Francis, Warren William and George Brent.

==Cast==
* Kay Francis as Amy Prentiss Parker
* Warren William as Walter Gibraltar Pritcham
* George Brent as Terrence Clarence Terry Parker
* Helen Lowell as Aunt Martha Prentiss
* Henry ONeill as Harold Thornton Russell Hicks as Major at Flying Field
* Maude Turner Gordon as Mrs. Parker
* Samuel S. Hinds as Henry L. Parker
* Martha Merrill as Cynthia Parker
* Edgar Kennedy as Counterman

==Plot==
One day, Terry Parker, an airplane pilot is in a plane crash that kills his family.  He feels guilty for their death and feels like he should have died in the crash as well. Terry continues to get into trouble until his friend, Walter Pritcham, known as Gibraltar for his steady nature, brings him to a party. Terry meets the beautiful Amy Prentiss and they both fall in love. 

Terry realizes that Amy is Gibraltars girl and tries to leave Amy, but Gibraltar reunites the couple wanting Amy to be happy. Amy and Terry get married and Gibraltar gives them a house in the country on Long Island.  Terry is unemployed for some time until he get the idea to fly commuters into New York.  

However, Amy believes that Terry will not act responsibly and leaves him. Gibraltar tries to get Amy to go back to Terry, but she refuses.  Terry is in a car crash and Amy and Gibraltar rush to see him. Terry and Amy realize that they do love each other and vow never leave each other ever again.  , Turner Classic Movies Website. 

==Reception==
The New York Times remarked on March 8, 1935: "With all the advantage of a rather neat plot situation, some brittle dialogue and the presence of the amiable George Brent and the attractive Kay Francis, Living on Velvet dwindles off to an unconvincing and rather meaningless ending, which does its best, in one stroke, to destroy most of the interest which the picture had succeeded in arousing during the earlier scenes. ... It is not the fault of the cast that the picture does not merit unqualified praise."  

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 