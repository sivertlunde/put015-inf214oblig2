Dead City (film)
{{Infobox Film
| name           = Dead City
| image          = 
| image_size     = 
| caption        = 
| director       = Frixos Iliadis
| producer       = 
| writer         = Frixos Iliadis
| narrator       = 
| starring       = Irene Papas
| music          = 
| cinematography = Joseph Hepp Aristeidis Karydis-Fuchs
| editing        = Aristeidis Karydis-Fuchs
| distributor    = 
| released       = 1951
| runtime        = 76 minutes
| country        = Greece
| language       = Greek
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Dead City ( ) is a 1951 Greek drama film directed by Frixos Iliadis. It was entered into the 1952 Cannes Film Festival.     

==Cast==
* Irene Papas (as Eirini Pappa)
* Nikos Tzogias - Petros
* Eleni Zafeiriou
* Giannis Argyris - Lambros
* Christina Kalogerikou
* Lakis Skellas
* Giorgos Foundas
* Anthi Miliadi
* Anny Papageorgiou

==References==
 

==External links==
* 

 
 
 
 
 
 