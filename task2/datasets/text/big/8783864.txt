My Flesh and Blood
{{Infobox film
| name           = My Flesh and Blood
| image_size     = 
| image	=	My Flesh and Blood FilmPoster.jpeg
| caption        = 
| director       = Jonathan Karsh
| producer       = Jennifer Chaiken
| writer         = 
| starring       = Susan Tom Anthony Tom Faith Tom Joe Tom Xenia Tom Margaret Tom
| music          = B. Quincy Griffin Hector H. Perez
| cinematography = Amanda Micheli
| editing        = Eli Olson
| distributor    = 
| released       = 2003
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
My Flesh and Blood is a 2003 documentary film by Jonathan Karsh chronicling a year in the life of the Tom family.  The Tom family is notable as the mother, Susan Tom, adopted eleven children, most of whom had serious disabilities or diseases. It was nominated for and won several awards, including the Audience Award and the Directors Award at the Sundance Film Festival.

==Plot==
The documentary takes an in-depth at the Tom Family which mostly consists of children that were rejected by their birth families due to mental or physical disabilities.  The film is broken up into seasons starting out with family taking part in Halloween in the fall and ending in the Summer of the upcoming year. The familys unconventional home-life becomes a foundation for the supports, challenges, and successes that they face daily.

==People==
 
* Susan Tom, adoptive mother of the children
* Faith Tom, who survived severe burns as an infant in a crib fire.
* Joe Tom, who has cystic fibrosis and is in the last stages of the disease. Susan and Joes doctors know that his death is imminent, but, as is common with terminally ill kids, withholds the prognosis from Joe who seems to believe that he will live to be an adult. 
* Xenia Tom, who was born without legs. The condition is referred to as a congenital amputee.
* Margaret Tom, who has no severe disabilities and assists her mother.
* Anthony Tom, who has recessive dystrophic epidermolysis bullosa. Unlike Joe, he is aware that he has a terminal illness.

There are six other children of whom we dont see as much due to the constraints of keeping the film to a reasonable length.

==Awards==
===Received===
Amsterdam International Documentary Film Festival
* 2003: Audience Award
* 2003: FIPRESCI Prize
Florida Film Festival
* 2003: Special Jury Award - For documentary feature
International Documentary Association
* 2003: IDA Award - Honorable Mention - Feature Documentaries
Sundance Film Festival
* 2003: Audience Award - Documentary
* 2003: Directing Award - Documentary
===Nominations===
International Documentary Association
* 2003: IDA Award - Feature Documentaries
Sundance Film Festival
* 2003: Grand Jury Prize - Documentary
Golden Trailer Awards
* 2004: Golden Trailer - Best Documentary
Satellite Awards
* 2004: Golden Satellite Award - Best Motion Picture, Documentary

== See also ==
* Susan Tom

==External links==
*  

 
 
 
 
 
 
 