Le tueur
Le tueur ( ) is a French film directed by Cédric Anger, released in 2007.

==Plot==
Christmas Eve in Paris. Leo Zimmerman is a businessman who lives for his beloved little daughter’s smile. Outwardly, his life is exemplary. However, when Dimitri Kopas walks into his office, pretending to be a normal client, Leo understands that a contract is out on his head and that the young man has come to town to kill him. Overcome with anxiety and paranoia, no longer able to sleep, Leo decides to meet the killer face to face and to broker a strange deal.

==Cast==
* Léo : Gilbert Melki (Anna M., Monsieur Ibrahim, La Vérité si je mens! 1 et 2)
* Kopas : Grégoire Colin (Voleurs de chevaux, L’intrus, Sex is comedy)
* Stella : Mélanie Laurent (Paris, Je vais bien, ne ten fais pas, De battre mon coeur sest arrêté)
* Sylvia : Sophie Cattani
* Franzen : Xavier Beauvois
* Alana : Jeanne Allard

==Festivals==
* European Film Festival in Estoril
* Athens French Film Festival
* Rotterdam International Film Festival
* FilmFest München
* Los Angeles Festival City of Lights, City of Angels
* Karlovy Vary International Film Festival (Variety Critic’s Choice)
* Fantasia International Film Festival in Montréal
* Nominated Louis-Delluc Award for Best First Film

==External links==
*  
*  
*  
*  
*  
*  
* 

 
 
 
 
 
 


 