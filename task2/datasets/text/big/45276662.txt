Tough Love (2015 film)
 
{{Infobox film
| name           = Tough Love
| image          = 
| caption        = 
| director       = Rosa von Praunheim
| producer       = Rosa von Praunheim
| writer         = Nicolas Woche   Jürgen Lemke   Rosa von Praunheim
| starring       = Hanno Koffler
| music          = Andreas Wolter
| cinematography = Nicolai Zörn   Elfi Mikesch
| editing        = Mike Shepard
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = Germany
| language       = German
| budget         = 
}}

Tough Love ( ) is a 2015 German drama film directed by Rosa von Praunheim.
  It was screened in the Panorama section of the 65th Berlin International Film Festival    where it won the 3rd place prize in the Panoroma Audience Award.   

==Plot== Japanese martial arts and begins to train on a daily basis. Unlike his inverted familial situation his dojo provides the security of a reliable hierarchy where he can thrive. His recklessness and his achievements as a tournament fighter assure him the attention of the local underworld. Lacking alternatives he accepts the job offer of a racketeer. More than one pretty young girl is infatuated with his rugged charme. He takes advantage of their feelings by running a prosperous side business as a pimp until a girl named Hannah says something that reminds him of his abusive mother. Overwhelmed by wrath he clobbers her badly and finds himself behind bars. Still Marion, one of his lovers, remains faithful. He really opens up to a woman for the first time and that enables him to tell a therapist the secrets of his obscure childhood. Following his release he becomes Marions true companion.

==Cast==
* Hanno Koffler as Andy
* Katy Karrenbauer as Andys mother
* Oliver Sechting as barkeeper
* Luise Heyer as Marion
* Sascia Haj as Hannah
* Udo Lutz as prison guard
* Stefan Kuschner as revenue officer
* Luise Schnittert as Beate

==Reception==
Britis film critic Joe Manners Lewis evaluated the film plot as "interesting" but he also stressed that von Praunheims Film adaption| adaptation occasionally looked more like a documental reconstruction than like artistic cinema. {{cite web|url=http://www.theupcoming.co.uk/2015/02/17/berlin-film-festival-2015-tough-love-review/|title=
Berlin Film Festival 2015: Tough Love review|publisher=theupcoming.co.uk|accessdate=2015-04-27}}  

==References==
 

==External links==
*  

 
 
 
 
 
 
 