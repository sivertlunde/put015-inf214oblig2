Feathered Dreams
{{Infobox film
| name               = Feathered Dreams
| image              = Feathered Dreams poster.jpg
| alt                = 
| caption            = Theatrical release poster
| director           = Andrew Rozhen 
| producer           = Austeen Eboka   Michael Maltsev   Igor Maron   Philipp Rozhen 
| writer         = Aleksandr Rozhen
| starring       =  
| music          = Vusyk Sergey
| cinematography = Dmirtiy Nedria 
| editing        = Vladimir Morozov
| studio         = Highlight Pictures
| distributor   = Highlight Pictures
| released       =  
| runtime        = 96 minutes
| country        = Nigeria   Ukraine
| language       =  
| budget         = 
| gross          =
}}

Feathered Dreams is a 2012 Nigerian-Ukrainian drama film, directed by Andrew Rozhen, who also stars in the film with Omoni Oboli.  The film which is the first collaboration between Nigeria and Ukraine tells the story of a young Nigerian medical student in Ukraine, Sade (Omoni Oboli) who dreams of becoming a singer, but shes faced with several difficulties associated with being a foreigner.       Feathered Dreams is also the first Ukrainian English-language feature film. 

==Cast==
*Omoni Oboli as Sade
*Evgeniy Kazantsev as Bronnikov
*Andrew Rozhen as Dennis
*Philippa Peter as Nkechi
*Conrad Tilla as Sades father
*Oksana Voronina as 
*Austeen Eboka as 
*Ayila Yussaf as

==Production==
The Co-production of Feathered Dreams came as a result of the need for Ukrainian filmmakers to get into the Nigerian market and the African market at large.       The Ukrainian film industry was facing funding difficulities and lack of states support, so filmmakers were sourcing for alternatives by collaborating with thriving industries.  Igor Maron, one of the producers stated that he was inspired to be part of the project after he visited Abuja, Nigeria and he could find so many Nigerians who could speak Russian and Ukrainian because theyd studied in the former Soviet Union,    so he thought itd nice to have a film that focuses on the foreign community in Ukraine.   The director Andrew Rozhen had to travel twice to Nigeria to familiarize with the methods of film production in Nollywood.   The film was shot on location in Kiev, Ukraine in 2011 and it marks the first collaboration between Nigeria and Ukraine.   It is also the first Ukrainian English-language feature film.  Omoni Oboli travelled twice to Ukraine during the duration of filming, spending six weeks and two weeks respectively.    the director, Rozhen, who also played the male lead role in the film had no prior acting experience. His decision to act in the film was due to the lack of English speaking actors in Ukraine. 

==Music and Soundtrack==
{{Infobox album
| Name        = Feathered Dreams OST
| Type        = Soundtrack Gaitana
| Cover       = 
| Released    = 10 June 2012
| Genre       = Film soundtrack
| Length      = 11:19
| Label       = Lavina Music   Highlight Pictures
| Producer    = Vusyk Sergey
| Last album = 
| This album = 
| Next album = 
}}

Music for Feathered Dreams was composed by Sergey Vusyk. The song "My Everything" was penned by Natalia Shamaya and performed by Gaitana (singer)|Gaitana. The Original Soundtrack was released under Lavina music label and Highlight Pictures. 

===Track listing===
{{track listing
| headline = 
| extra_column = Singer(s)
| music_credits = no
| total_length = 11:19

| title1 = My Everything
| extra1 = Gaitana
| length1 = 3:57

| title2 = Khreschatyk
| extra2 = Vusyk Sergey
| length2 = 2:25

| title3 = Feathered Dreams (Main Theme)
| extra3 = Vusyk Sergey
| length3 = 1:54

| title4 = Deportation
| extra4 = Vusyk Sergey
| length4 = 1:56

| title5 = Feathered Dreams (Theme #1)
| extra5 = Vusyk Sergey
| length5 = 1:06
}}

==Awards==
Feathered Dreams was nominated for two awards at the 2013 Golden Icons Academy Movie Awards in the categories; "Best Film Diaspora" and "Best Film Director – Diaspora".   

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 