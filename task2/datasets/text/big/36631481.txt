Andrea, Paano Ba ang Maging Isang Ina?
{{Infobox film
| name      = Andrea, Paano Ba ang Maging Isang Ina?
| image_size   =
| image	= Andrea, Paano Ba ang Maging Isang Ina? poster.jpg	
| alt      =
| caption    = Official poster of Andrea, Paano Ba ang Maging Isang Ina? for the 1990 Metro Manila Film Festival
| director    = Gil Portes
| producer    = Mely Nicandro
| writer     =  Ricardo Lee
| story     = Ricardo Lee
| based on    =
| starring    = Nora Aunor, Gina Alajar
| music     = Mon Faustino
| cinematography = Ely Cruz
| editing    = Edgardo Vinarao
| studio     = 
| distributor  = 
| released    = 25 December 1990
| runtime    = 155 mins
| country    =   Philippines Filipino
| budget     = 
| gross     = 
}}
Andrea, Paano Ba ang Maging Isang Ina? (Andrea, How Is It Like to Be a Mother?) is a 1990 Filipino film and an official entry to the 1990 Metro Manila Film Festival. It is a story of a woman activist (Aunor)who goes to the mountains to search for her husband who has been killed by the military. She leaves her baby with her best friend, an apolitical middle-class woman. When she comes back to claim her baby, now a grown-up boy, her fight for custody also becomes a search for truth and motherhood in an oppressive society which relentlessly destroys ones humanity. Written by Ricardo Lee and directed by Gil Portes. 

The film won for Aunor all the Best Actress Awards given by the five annual award-giving bodies in the Philippines that time. The film was also awarded Best Picture by the Filipino Academy of Movie Arts and Sciences(FAMAS), the Young Critics Circle (YCC), and the Philippine Movie Press Club Star Awards for Movies. 

==Synopsis==
The film stars Nora Aunor as Andrea, a teacher who has chosen the difficult and perilous but crusading path of armed social rebel whose husband played by Juan Rodrigo is a comrade in the underground. When it is time for her to give birth, she seeks refuge in the house of her best friend, played by Gina Alajar, and her husband, played by Lloyd Samartino, who has absolutely no sympathy for the cause which his wifes friend is fighting for.

Childless, Lloyd and Gina get emotionally attached to the baby, especially after the mother temporarily leaves the infant to look into the circumstances of her guerilla husbands death in the hands of the military. Unfortunately, before she could return to her baby, Andrea is arrested and detained in a safehouse by military men who torture her mercilessly.

Having lost contact with the freedom-fighter and presuming her to have been killed, the surrogate parents have decided to keep the child as their own and bring him to the United States for seven years. When they return to the Philippines for a visit, Andrea confronts them and tries to claim her child back. 

==Casts==
*Nora Aunor ... Andrea.
*Gina Alajar ... 
*Llyod Samartino ... 
*Dan Alvaro ...
*Perla Bautista ...

==Awards==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Group
! width="25%"| Category
! width="25%"| Nominee
! width="10%"| Result
|- 1990
| rowspan="11" align="left"| Metro Manila Film Festival    Best Picture
| align="center"| 
|  
|- Best Director
| align="center"| Gil Portes
|  
|- Best Actress
| align="center"| Nora Aunor
|  
|- Best Supporting Actress
| align="center"| Gina Alajar
|  
|- Best Editing
| align="center"| Boy Vinario
|  
|- Best Story
| align="center"| Ricky Lee and Gil Portes
|  
|- Best Screenplay
| align="center"| Ricky Lee and Gil Portes
|  
|- Best Original Song
| align="center"| Mon Faustino 
|  
|- Best Music
| align="center"| Mon Faustino 
|  
|- Best Studio Sound Recording
| align="center"| Rolly Ruta
|  
|-
| align="left"| Gatpuno Antonio J. Villegas Cultural Awards
| align="center"| Andrea, Paano Ba ang Maging Isang Ina?
|  
|-
| rowspan="20" align="center"| 1991
| rowspan="6" align="left"| FAMAS Filipino Academy of Movie Arts and Sciences Awards
| align="left"| Best Picture
| align="center"| 
|  
|-
| align="left"| Best Actress
| align="center"| Nora Aunor
|  
|-
| align="left"| Best Editing
| align="center"| Edgardo Vinarao
|  
|-
| align="left"| Best Screenplay
| align="center"| Ricky Lee
|  
|-
| align="left"| Best Story
| align="center"| Ricky Lee
|  
|-
| align="left"| Best Director
| align="center"| Gil Portes
|  
|-
| rowspan="5" align="left"| Gawad Urian Awards (Manunuri ng Pelikulang Pilipino)
| align="left"| Best Actress
| align="center"| Nora Aunor
|  
|-
| align="left"| Best Picture
| align="center"| Edgardo Vinarao
|  
|-
| align="left"| Best Supporting Actress
| align="center"| Gina Alajar
|  
|-
| align="left"| Best Music
| align="center"| Mon Faustino
|  
|-
| align="left"| Best Screenplay
| align="center"| Ricky Lee
|  
|-
| rowspan="4" align="left"| Film Academy of the Philippines (Luna Awards)
| align="left"| Best Actress
| align="center"| Nora Aunor
|  
|-
| align="left"| Best Supporting Actress
| align="center"| Gina Alajar
|  
|-
| align="left"| Best Editing
| align="center"| Edgardo Vinarao
|  
|-
| align="left"| Best Screenplay
| align="center"| Ricky Lee
|  
|-
| rowspan="2" align="left"| Star Awards for Movies (Philippine Movie Press Club)
| align="left"| Best Actress
| align="center"| Nora Aunor
|  
|-
| align="left"| Movie of the Year
| align="center"| 
|  
|- Young Critics Circle
| align="left"| Best Performance by Male or Female, Adult or Child, Individual or Ensemble in Leading or Supporting Role
| align="center"| Nora Aunor
|  
|-
| align="left"| Best Picture
| align="center"| 
|  
|-
| align="left"| Best Screenplay
| align="center"| Ricky Lee
|  
|}

==References==
 

==External links==
 
* http://pinoyrebyu.wordpress.com/2012/04/10/modern-philippine-classics-1990/

 

 
 
 