Agar Tum Na Hote
{{Infobox film
| name           = Agar Tum Na Hote
| image          = Agar Tum Na Hote.jpg
| image size     =
| caption        =
| director       = Lekh Tandon
| producer       = Rajeev Kumar
| writer         = Ramesh Pant
| narrator       =
| starring       = Rajesh Khanna Rekha Raj Babbar Madan Puri Asrani
| music          = R D Burman Gulshan Bawra (Lyrics)
| cinematography =
| editing        =
| distributor    =
| released       = 4 November 1983
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1983 Cinema Indian Bollywood film directed by Lekh Tandon. The film stars Rajesh Khanna, Rekha and Raj Babbar in leading roles.

==Story==
Ashok Mehra, played by Rajesh Khanna, is a wealthy industrialist, whose business produces cosmetic products. He is happily married to Purnima, played by Rekha, but she suffers complications after giving birth to their daughter, and the doctors are not able to save her life, leaving Ashok devastated. He is able to pull himself together and rediscover his will to live only because of his obligation to his daughter, Mini.

Several years pass, and Ashok is able to resume his life as normal, except that he is unable to handle Mini’s (played now by Baby Shabana) envious pining for a mother to love her the way other children’s mothers love them. He is forced to lie to satisfy her, saying that it is possible that her mother will return from “heaven|God’s house”. Eventually, Mini becomes frustrated with waiting for her mother to return to her, and her behavior degrades severely, to the point that no school is willing to teach her, nor is any governess willing to deal with her.

Meanwhile, Ashok’s business, while fairly successful, is unable to adequately compete with foreign cosmetics companies, because of their general superiority to Indian companies in the way of advertising. He hires Raj Bedi, a successful photographer in the modeling industry, played by Raj Babbar, to run his new advertising campaign. Mr. Bedi willingly accepts, as Ashok is one of few Indian businessmen willing to pay Bedi’s steep prices. Mr. Bedi finds a beautiful woman, named Radha, also played by Rekha, while scouting for model at the beach.

At first, Radha is reluctant, but eventually Mr. Bedi is able to convince her to model for him in the Ashok Mehra campaign. Over the course of the project, the two fall in love, and eventually wed. Because Ashok Mehra’s model has now become his wife, Mr. Bedi is unwilling to give him the photos, and sells all of his personal possessions to arrange for the cost of backing out of the contract. Mr. Bedi’s reputation is ruined by Ashok as a result of this incomplete project, and his career takes a nosedive.

In time, Mr. Bedi is able to build a modest, but decent life for Radha and himself by taking on small jobs. However, even this modest life is threatened when Mr. Bedi, in going to extreme lengths to take the best photos possible for a particular project, suffers a crippling fall. Radha decides to take on a job in order to run the house and save money for her husband’s treatment, but no one is willing to hire a married woman, whose primary focus will be her husband rather than her job. Eventually, she decides she is willing to lie about her marital status to get a job. Her job search leads her to Ashok Mehra’s office, where his associates are looking for a governess for Mini. She is offered the position instantly based on her uncanny resemblance to the late Mrs. Purnima Mehra, although this fact is hidden from her; his business manager, Shakur Ahmed, played by Madan Puri, simply tells her that he has a great feeling about her. Unwilling at first, holding Mr. Mehra responsible for her and her husband’s current situation, she is forced to accept the position, because Ashok’s associates are offering her far more money than she can make anywhere else.

At first, Mini displays her usual poor behavior, but after watching Radha’s perseverance through her antics and devotion to her, and finally realizing that her mother will never return, Mini comes to feel that the void left by her mother has been filled. Even Ashok begins to feel that the void in his life is similarly being filled, and, being unaware that Radha is married, falls in love with her.

As the relation between Radha and Mini grows beyond a professional one, and as Radha continues to keep her bedridden husband in the dark as to the details of her job, Mr. Bedi begins to suspect his wife’s marital integrity. Ashok’s attempts to grow closer to Radha, and Bedi’s interloping friend Chandu, played by Asrani, only serve to add to his suspicions. After a heated confrontation between Ashok, Mr. Bedi, and Radha, her and Mini’s relationship comes to an abrupt end.

Ashok is unable to handle his obligation as a father to quell Mini’s obvious frustration with being deprived, once again, of a mother’s love, and sends her to live in a hostel. Another confrontation occurs between Radha and Ashok, and the two parties officially sever all ties.

Feeling guilty for judging Radha as selfish, and feeling a sense of obligation to her for giving his daughter a mother’s love, Ashok learns that a lottery ticket that Radha had purchased through him has won the jackpot, and sends Shakur Ahmed to deliver her prize on his behalf. The lottery winnings are enough to pay for Mr. Bedi to be treated in America. Ashok meets the couple at the airport, and misconceptions between the three are cleared up. The movie ends with Ashok taking advantage of a delay in the Bedis’ flight to arrange for the adoption of Mini by Mr. and Mrs. Bedi, as he is unable to give her the love she desires and deserves. The last line of the movie is delivered painfully by Ashok Mehra, directed at both of Rekha’s characters: “I Loved you once, but lost you twice.”

==Cast==
*Rajesh Khanna … Ashok Mehra
*Rekha … Purnima Mehra/Radha Bedi
*Baby Shabana … Mini Mehra
*Raj Babbar … Raj Bedi
*Madan Puri … Shakur Ahmed
*Asrani … Chandu

==Music==
Music was composed by Rahul Dev Burman and lyrics written by Gulshan Bawra.
*Agar Tum Na Hote – Kishore Kumar
*Agar Tum Na Hote – Lata Mangeshkar
*Dheere Dheere Zara Zara – Asha Bhosle
*Sach Hai Yeh Koi – Kishore Kumar
*Hum To Hai Chhui Mui – Lata Mangeshkar Shailendra Singh, Rekha

==External links==
* 

 
 
 
 
 