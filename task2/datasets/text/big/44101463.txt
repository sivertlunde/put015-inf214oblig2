Sammelanam
{{Infobox film 
| name           = Sammelanam
| image          =
| caption        =
| director       = CP Vijayakumar
| producer       = Basheer
| writer         = CP Vijayakumar Nedungadu Radhakrishnan (dialogues)
| screenplay     = Nedungadu Radhakrishnan Shankar Menaka Menaka Jalaja
| music          = Maharaja
| cinematography = Vijaya Kumar
| editing        =
| studio         = Khadeeja Productions
| distributor    = Khadeeja Productions
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, Menaka and Jalaja in lead roles. The film had musical score by Maharaja.   

==Cast== Shankar
*Menaka Menaka
*Jalaja

==Soundtrack==
The music was composed by Maharaja and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Azhakezhum Kadanjeduthoru || K. J. Yesudas, P Susheela || Bichu Thirumala || 
|-
| 2 || Jeevitha Nadiyude || K. J. Yesudas || Bichu Thirumala || 
|-
| 3 || Oodum Paavum || P Susheela || Bichu Thirumala || 
|-
| 4 || Oodum Paavum || K. J. Yesudas || Bichu Thirumala || 
|-
| 5 || Thakkaalikkavilathu || Chorus, Vilayil Valsala || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 