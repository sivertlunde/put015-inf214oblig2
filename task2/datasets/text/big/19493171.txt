Tror du jeg er født i går!
{{Infobox film
| name           = Tror du jeg er født i går!
| image          = 
| caption        = 
| director       = Lau Lauritzen, Jr. Alice OFredericks
| producer       = Henning Karmark
| writer         = Lau Lauritzen, Jr. Alice OFredericks Max Hansen
| music          = 
| cinematography = Rudolf Frederiksen
| editing        = Marie Ejlersen
| distributor    = 
| released       = 10 March 1941
| runtime        = 99 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Tror du jeg er født i går! is a 1941 Danish family film directed by Lau Lauritzen, Jr. and Alice OFredericks.

==Cast== Max Hansen - Cornelius Nielsen
* Maria Garland - Tante Emma Solberg
* Bodil Steen - Frk. Johanne Solberg
* Tove Arni - Paula
* Knud Heglund - Fætter Henrik Hahe
* Eigil Reimers - Direktør Hans Hahe
* Victor Montell - Overretssagfører Brix Jensen
* Berthe Qvistgaard - Sekretær Frk. Poulsen
* Mathilde Nielsen - Frk. Møller
* Petrine Sonne - Frk. Møller
* Karl Goos
* Erika Voigt
* Karl Jørgensen
* Thorkil Lauritzen
* Alex Suhr
* Bruno Tyron
* Helga Frier
* Susanne Friis
* Vera Lindstrøm

==External links==
* 

 
 

 
 
 
 
 
 
 
 


 