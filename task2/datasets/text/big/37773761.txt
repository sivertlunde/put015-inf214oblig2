Nearly a Deserter
 
{{Infobox film
| name           = Nearly a Deserter
| image          = Nearly a Deserter.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = 
| producer       =  Rex A. Taylor James O. Walsh
| narrator       = 
| starring       = Leatrice Joy
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = Silent English intertitles
}}
 short Silent silent comedy film produced by the United States Motion Picture Corporation under the Black Diamond Comedy name. The film was the first Black Diamond Comedy distributed by the Paramount Pictures.

==Synopsis==
Nearly A Deserter is a comedic bait and switch story that follows two hitchhikers and their plan to make a few quick bucks. One realizes that the other closely resembles Willie Smith, a wanted deserter, and turns him in with the intent of helping him escape and making off with the reward money. Escaping becomes difficult, however, when Willie’s lookalike is put through a series of tests by the camp. Their plans go completely awry when Willie bakes gunpowder into the general’s biscuits. One of the biscuits explodes in the general’s face, leading him to sentence Willie to death by firing squad. The “daughter of the regiment” takes pity on Willie, and tells him to escape by letting himself be fired out of the sunset canon. He takes her advice, leading to a wild chase across train tracks after he reunites with his partner and has to evade the general. 

==References==
 

 
 
 
 
 
 
 
 
 


 