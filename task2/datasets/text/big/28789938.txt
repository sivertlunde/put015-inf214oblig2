The Old Lady
 
{{Infobox film
| name           = The Old Lady
| image          = 
| caption        = 
| director       = Amleto Palermi
| producer       = 
| writer         = Amleto Palermi Orsino Orsini
| starring       = Emma Gramatica
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

The Old Lady ( ) is a 1932 Italian comedy film directed by Amleto Palermi. It features Vittorio De Sica in his first sound film.   

==Cast==
* Emma Gramatica - Maria
* Nella Maria Bonora - Bianchina
* Maurizio DAncora - Fausto
* Armando Falconi - Zaganello
* Memo Benassi - Lenticcio
* Maria Della Lunga Mandarelli
* Vittorio De Sica
* Anna Maria Dossena - Bianca La Nipotina
* Camillo Pilotto
* Lidia Simoneschi

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 