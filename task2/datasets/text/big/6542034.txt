The Queen (film)
 
 
{{Infobox film
 | name = The Queen
 | image = The Queen movie.jpg
 | caption = Teaser poster
 | director = Stephen Frears
 | producer = Andy Harries Christine Langan Tracey Seaward Francois Ivernel
 | writer = Peter Morgan
 | starring = Helen Mirren Michael Sheen James Cromwell Helen McCrory Alex Jennings Roger Allam Sylvia Syms
 | music = Alexandre Desplat
 | cinematography = Affonso Beato
 | editing = Lucia Zucchetti
 | studio = Granada Productions BIM Distribuzione Pathé Renn Production France 3 Cinéma Canal+
 | distributor = Pathé|Pathé Pictures Miramax| Miramax Films (US)  20th Century Fox (UK)
 | released =  
 | runtime = 97 minutes
 | country = United Kingdom
 | language = English
 | budget = £9.8 million ($15 million)
 | gross = £77,865,176 ($123,384,128) 
}} HM Queen Elizabeth II.   
 Prince Charles, royal protocol regarding Dianas official status, and wider issues about Republicanism in the United Kingdom|Republicanism.
 monarchy and The Deal, The Special Relationship. The Queen also garnered general critical and popular acclaim for Mirren playing the title role, for which she earned her numerous awards, including the Academy Award for Best Actress. Mirren was also praised by the Queen herself and invited to dinner at Buckingham Palace  (though she could not attend due to filming commitments in Hollywood). 

==Plot==
  1997 general Labour Prime prime minister. Queen (Helen Royal Family. Alma Bridge director of Buckingham and Prince Charles the Spencers. Charles, however, argues that the mother of a future king cannot be dismissed so lightly, while the Queen authorises the use of an aircraft of the Royal Air Force to bring Dianas body back to Britain.
 Union flag at half-mast over Buckingham Palace, and speak to the nation about Dianas life and legacy in a televised address.
 William and Harry (Jake Taylor Shantos and Dash Barber) from Dianas death by taking them deer stalking. While venturing out alone in her classic Land Rover, the Queen damages it crossing a river and is forced to telephone for assistance. The Queen weeps in frustration, but catches sight of a majestic red deer which Philip had been stalking with William and Harry. The Queen is struck by his beauty and the two stare at each other. Hearing a distant gunshot, the Queen shoos the animal away and decides to carry out Blairs recommendations. While preparing to return to London to attend a public funeral for Diana, the Queen is horrified to learn that the deer has been killed on a neighbouring estate, asks to see the stag, and is upset at its loss.

The Royal Family finally returns to London to inspect the floral tributes to Diana. While he was watching live television coverage with his staff, Blair becomes angry and disappointed at his Labour Party advisers, declaring that the Queen is admirable and Diana had rejected everything held most dear by the Queen. The Queen later follows Blairs advice to make a public statement on live television, during in which she speaks about the life and legacy of Diana and describes her as an exceptional and gifted human being. Two months after Dianas death, Blair visits Buckingham Palace to attend a weekly meeting with the Queen. The Queen has finally regained her popularity, but she believes that Blair has benefited himself from her acquiescence to his advice and that she will never fully recover from that week. The Queen warns Blair that he will find that public opinion can rapidly turn against him, and states that life in Britain has changed and that the monarchy must modernise in the future. When Blair suggests that he can help with this, the Queen replies to him: "Dont get ahead of yourself, Prime Minister. Remember, Im supposed to be the one advising you".

==Cast== Elizabeth I, Elizabeth I. She also played a policewoman, under cover as the Queen, in The Fiendish Plot of Dr. Fu Manchu. The Deal, The Special Relationship.
*James Cromwell as Prince Philip, Duke of Edinburgh
*Helen McCrory as Cherie Blair — Tony Blairs wife. Like Sheen, McCrory reprised this role in The Special Relationship.
*Alex Jennings as Charles, Prince of Wales  Robin Janvrin (later Lord Janvrin) — In the film, Janvrin is private secretary to the Queen, although he was the deputy private secretary at the time of Dianas death
*Sylvia Syms as Queen Elizabeth The Queen Mother 
*Tim McMullan as Stephen Lamport — Private Secretary to the Prince of Wales The Special Relationship. Lord Airlie, Lord Chamberlain
*Robin Soans as Equerry
*Lola Peploe as Janvrins Secretary  Earl Cameron as Portrait Artist
*Anthony Debaeck as Catholic Priest
*Trevor McDonald as Newsreader — ITV newsreader, covered Dianas death Prince William 
*Dash Barber as Prince Harry

==Production==

===Filming=== Peter Morgan. The Deal that allowed him to direct any follow-ups or sequels, and he was officially announced as director in September 2003.  The film was shot on location in the United Kingdom, in England in London, Halton House and Waddesdon Manor, in Buckinghamshire, Brocket Hall in Hertfordshire and in
Scotland at Balmoral Castle,  Castle Fraser  and Cluny Castle  in Aberdeenshire, and Blairquhan Castle and Culzean Castle in South Ayrshire. Alan MacDonald, which won him  Best Art Direction in a Contemporary Film from the Art Directors Guild and Best Technical Achievement at the British Independent Film Awards. 
Mirren says transforming herself into the Queen came almost naturally after the  ; Retrieved 26 November 2006.  She regularly reviewed film and video footage of Elizabeth and kept photographs in her trailer during production. Levy, Emanuel;  , emanuellevy.com; retrieved 26 November 2006  She also undertook extensive voice coaching, faithfully reproducing the Queens delivery of her televised speech to the world. Morgan has said that her performance was so convincing that, by the end of production, crew members who had been accustomed to slouching or relaxing when they addressed her were standing straight up and respectfully folding their hands behind their backs.  Mirren arranged to spend time off-camera with the supporting cast playing other members of the Royal Family, including James Cromwell, Alex Jennings and Sylvia Syms so they would be as comfortable with each other as a real family. 
Shots involving the Queen were shot in 35mm film and shots of Tony Blair were shot in 16mm film to enhance the contrast of different worlds. 

===Television viewership and DVD release===
ITVs role in the production of the film allowed them an option for its television premiere and it was broadcast on 2 September 2007 (coinciding that weekend with a memorial service to Diana) to an average audience of 7.9 million, winning its timeslot.   The DVD was released in the UK on 12 March 2007. Special features include a making-of featurette and an audio commentary by Stephen Frears, writer Peter Morgan and Robert Lacey, biographer of Queen Elizabeth II. It was released on Blu-ray and DVD in the USA on 24 April 2007 and,  , US DVD sales had exceeded $29 million. 

===Historical accuracy===
Some aspects of the characters are known to be true to their real-life counterparts. Cherie Blairs hostility to the monarchy has been widely reported, including her refusal to curtsey.    According to Morgan, "cabbage" is an actual term of endearment Philip uses for his wife («mon chou» – "my cabbage" – is a standard affectionate nickname in French). 
 Ugly Rumours football jersey jersey he wears to a family breakfast is a reference to his support of that team. The film also shows Alastair Campbell coining the term the peoples princess, but in 2007 he revealed that it was Tony Blair who came up with it. 
 Robin Janvrin private secretary Sir Robert Fellowes, a brother-in-law of Diana, Princess of Wales; Janvrin was the deputy private secretary until 1999. However, the film is accurate in depicting Janvrin as the person who delivered the news of Dianas accident to the Queen at Balmoral during the night. Junor, Penny (2005).  The Firm: The Troubled Life of the House of Windsor. London: HarperCollins. ISBN 0-00-710215-1. 

==Reception==

===Box office===
The film exceeded box-office expectations; with a budget of $15 million the film earned $56.4 million in the United States and Canada. 

===Critical reception===
Before the film was released, critics praised both Stephen Frears and Peter Morgan, who later received Golden Globe and Academy Award-nominations for Best Director and Best Screenplay. Michael Sheens performance as Tony Blair earned him particular acclaim. Helen Mirrens portrayal garnered her acclaim from critics around the world. Her portrayal made her a favourite for the Academy Award for Best Actress well before the film was released in theatres. After its showing at the Venice Film Festival, Mirren received a five-minute-long standing ovation.  Roger Ebert came out of recovery from surgery to give the film a review. He called it "spellbinding" and gave it four out of four stars.  The Queen was the most critically acclaimed film of 2006 with Mirren being the most critically acclaimed actress of the year. The Queen has 97% positive reviews on the film-critics aggregate site Rotten Tomatoes. 

Amongst the few negative reviews, Slant Magazine  s Nick Schager criticised the insider portraiture of the film as "somewhat less than revelatory, in part because Morgans script succumbs to cutie-pie jokiness   and broad caricature", mentioning particularly "James Cromwells Prince Philip, who envisions the crowned heads as exiled victims and the gathering crowds as encroaching Zulus". 

===Top ten lists===
The film appeared on many US critics top ten lists of the best films of 2006. 

 
 
*1st – Frank Scheck, The Hollywood Reporter
*1st – William Arnold, Seattle Post-Intelligencer
*2nd – Richard Roeper, Chicago Sun Times
*2nd – Lou Lumenick, New York Post
*2nd – Michael Rechtshaffen, The Hollywood Reporter
*3rd – David Ansen, Newsweek
*3rd – Ella Taylor, LA Weekly
*3rd – Richard Schickel, Time (magazine)|TIME magazine
*3rd – Sheri Linden, The Hollywood Reporter
*4th – Chris Kaltenbach, The Baltimore Sun
*4th – Claudia Puig, USA Today
*4th – Kenneth Turan, Los Angeles Times (tied with Venus (film)|Venus)
*4th – Stephen Holden, The New York Times
*5th – Dennis Harvey, Variety (magazine)|Variety
*5th – Kirk Honeycutt, The Hollywood Reporter
*5th – Mick LaSalle, San Francisco Chronicle Marie Antoinette)
*6th – Marjorie Baumgarten, The Austin Chronicle
*6th – Michael Sragow, The Baltimore Sun Shawn Levy, The Oregonian
*7th – Lawrence Toppman, The Charlotte Observer
 
*7th – Peter Travers, Rolling Stone
*9th – Jack Mathews, New York Daily News
*9th – Lisa Schwarzbaum, Entertainment Weekly Michael Phillips, Chicago Tribune
*9th – Michael Wilmington, Chicago Tribune
*9th – Nathan Rabin, The A.V. Club
*9th – Ty Burr, The Boston Globe
*10th – Glenn Kenny, Premiere (magazine)|Premiere
*10th – Staff, Film Threat
General top ten
*Carina Chocano, Los Angeles Times
*Carrie Rickey, The Philadelphia Inquirer Dana Stevens, Slate (magazine)|Slate
*Joe Morgenstern, The Wall Street Journal
*Liam Lacey and Rick Groen, The Globe and Mail
*Peter Rainer, The Christian Science Monitor
*Ruthe Stein, San Francisco Chronicle
*Steven Rea, The Philadelphia Inquirer
 

===Awards and nominations===
Helen Mirren won at least 29 major awards for her portrayal of Queen Elizabeth II, many of which are listed below. She was nominated for at least 3 more. In most of her acceptance speeches, she expressed her admiration for the real Queen, and dedicated both her Golden Globe and her Oscar to Elizabeth II.
{| class="infobox" style="width: 23em; font-size: 85%;"
|- bgcolor="#cccccc" align=center
! colspan="2" | Academy Awards record
|-
| 1. Best Actress (Helen Mirren)
|- bgcolor="#cccccc" align=center
! colspan="2" | Golden Globe Awards record
|-
| 1. Best Actress (Helen Mirren)
|-
| 2. Best Screenplay
|- bgcolor="#cccccc" align=center
! colspan="2" | BAFTA Awards record
|-
| 1. Best Picture
|-
| 2. Best Actress (Helen Mirren)
|}
79th Academy Awards (2006)
* Won: Performance by an Actress in a Leading Role — Helen Mirren
* Nominated: Best Picture— Andy Harries, Christine Langan, Tracey Seaward
* Nominated: Best Director — Stephen Frears
* Nominated: Best Original Screenplay — Peter Morgan
* Nominated: Best Original Score  — Alexandre Desplat
* Nominated: Best Costume Design — Consolata Boyle
 2006 British British Academy Film (BAFTA) Awards
* Won: Best Film
* Won: Actress in a Leading Role — Helen Mirren
* Nominated: Outstanding British Film — Andy Harries, Christine Langan, Tracey Seaward, Stephen Frears, Peter Morgan
* Nominated: The David Lean Award for Achievement in Direction — Stephen Frears
* Nominated: Actor in a Supporting Role — Michael Sheen
* Nominated: Original Screenplay — Peter Morgan
* Nominated: Anthony Asquith Award for Film Music — Alexandre Desplat
* Nominated: Editing — Lucia Zucchetti
* Nominated: Costume Design — Consolata Boyle
* Nominated: Makeup and Hair — Daniel Philipps
 2006 Screen Actors Guild Awards
* Won: Outstanding Performance by a Female Actor in a Leading Role (Theatrical movie) — Helen Mirren

2006 Directors Guild of America Awards
* Nominated: Outstanding Directorial Achievement in Motion Pictures — Stephen Frears

2006 Writers Guild of America Awards
* Nominated: Original Screenplay — Peter Morgan

2006 Producers Guild of America Awards
* Nominated: Best Picture of the Year — Andy Harries, Christine Langan, Tracey Seaward
 64th Golden Globe Awards
* Won: Best Actress, Drama — Helen Mirren Peter Morgan
* Nominated: Best Picture, Drama
* Nominated: Best Director — Stephen Frears
 2006 Broadcast Film Critics Association Awards
*Won: Actress in a Leading Role — Helen Mirren
*Nominated: Best Picture
*Nominated: Best Director — Stephen Frears
*Nominated: Best Writer — Peter Morgan
 2006 Toronto Film Critics Association Awards
*Won: Best Picture
*Won: Best Actress — Helen Mirren
*Won: Best Supporting Actor — Michael Sheen
*Won: Best Director — Stephen Frears Peter Morgan

2006 New York Film Critics Circle Awards
* Won: Best Actress — Helen Mirren Peter Morgan

2006 Los Angeles Film Critics Association Awards
* Won: Best Actress — Helen Mirren
* Won: Best Supporting Actor — Michael Sheen Peter Morgan
* Won: Best Music — Alexandre Desplat
* Runner-up: Best Picture

2006 National Society of Film Critics Awards
*Won: Best Actress — Helen Mirren
*Won: Best Screenplay — Peter Morgan

2006 Satellite Awards
* Nominated: Best Motion Picture, Drama
* Won: Best Actress in a Motion Picture, Drama — Helen Mirren
* Nominated: Best Director — Stephen Frears Peter Morgan
 2006 National Board of Review Awards
* Won: Best Actress — Helen Mirren

2006 Chicago International Film Festival
* Won: Audience Choice Award — Stephen Frears

2006 British Independent Film Awards Peter Morgan
* Nominated: Best British Independent Film
* Nominated: Best Director — Stephen Frears
* Nominated: Best Actress — Helen Mirren
* Nominated: Best Technical Achievement — Alan MacDonald (production design)
* Nominated: Best Technical Achievement — Daniel Phillips (makeup)

2006 Venice Film Festival Best Actress — Helen Mirren Peter Morgan
* Nominated: Golden Lion

==Soundtrack==
{{Infobox album 
| Name        = The Queen
| Type        = Album
| Artist      = Alexandre Desplat
| Cover       =
| Released    = 26 September 2006
| Recorded    = 2006
| Genre       = Soundtrack
| Length      =
| Label       = Milan
| Producer    = The Singer (2006)
| This album  = The Queen (2006) The Painted Veil (2006)
}} score and BAFTA Award for Best Film Music (lost to the score of Babel (soundtrack)|Babel).

# The Queen – 2:10
# Hills of Scotland – 2:25
# Peoples Princess I – 4:08
# A New Prime Minister – 1:55
# H.R.H. – 2:22
# The Stag – 1:50
# Mourning – 3:50
# Elizabeth & Tony – 2:04
# River of Sorrow – 1:59
# The Flowers of Buckingham – 2:28
# The Queen Drives – 1:48
# Night in Balmoral – 1:09
# Tony & Elizabeth – 2:06
# Peoples Princess II – 4:08
# Queen of Hearts – 3:33
# Libera Me (Verdi) – 6:27

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*   at itv.com

;Interviews
*  at The Daily Telegraph
*  at The Guardian
*  at Who (magazine)|Who

 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 