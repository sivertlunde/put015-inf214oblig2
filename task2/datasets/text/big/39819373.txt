Peacefire (film)
{{Infobox film
| name           = Peacefire 
| image          = Peacefire 2008 movie poster.jpg
| caption        = Film poster 
| director       = Macdara Vallely 
| producer       = Chris Martin   Sarah Perry  
| writer         = Macdara Valley  John Travers   Gerard Jordan   Sean Roberts   Conor MacNeill   Pauline Goldsmith 
| music          = Brendan Dolan
| cinematography = Núria Roldos 
| editing        = Macdara Vallely 
| studio         = mayFLY Entertainment 
| distributor    = RDS Media Capital  
| released       =  
| runtime        = 87 minutes  
| country        = Ireland
| language       = English
| budget         = 
| gross          = 
}}
Peacefire is a 2008 Irish independent drama film direct by Macdara Vallely.

==Plot==
Colin McNally (John Travers) is a troubled teenager being brought up by his widowed mother following his fathers death. With his carefree attitude towards the political situation in Northern Ireland, Colin becomes involved in criminal activities. He and friends, Spuds (Gerard Jordan) and Jimbo (Sean Roberts) begin to steal cars, take them for a joyride and later set fire to them. Following a car accident on one of their joyrides, Colin and his friends escape with only a few minor injuries and are arrested. While he and his friends are separately being questioned, a policeman makes Colin an offer - either he helps the police by becoming an informant in bringing down a top IRA man or he and his friends will be sent to prison. Although his father was a top IRA man before he died, Colin, knowing that he is making the wrong decision and that he would be going against his father and the others, takes the policeman up on his offer in order to stay out of prison which causes friction between him and his friends. Colin, Spuds, and Jimbo continue their criminal behavior and following numerous warnings from the police and the IRA, who has been lenient with Colin for who is father was, Spuds and Jimbo are kneecapped and Colin, having escaped this, is threatened and told to leave the country. Colin eventually has the top IRA man captured, and with his guilt he goes to the local priest for forgiveness. While there is nothing more the priest can do, the IRA men enter the church and take Colin to kill him. Colin asks one last thing of them for which it is granted. He writes a letter to his mother telling her that he is safe and well in England so that she will not worry about him. Colin is then shot dead by the IRA men.

==Cast== John Travers ... Colin
* Gerard Jordan ... Spuds
* Sean Roberts ... Jimbo
* Conor MacNeill ... Pots
* Pauline Goldsmith ... Avril
* Gerry Doherty ... Moorcroft
* Richard Parkin ... Steele
* Terry ONeill ... Patsy
* B.J. Hogg ... Fatboy
* Vincent Fegan ... Father Peter
* Brian Devlin ... Mr. Doyle

==Release==
The film was originally premiered in the Czech Republic on 5 July 2008 at Karlovy Vary International Film Festival, a week before premiering its native country of Ireland on 12 July 2008, in which it had been shown at the Galway Film Fleadh. It was shown at the Athens Film Festival in Greece on 23 September 2008. In France, Peacefire premiered at the Angers Film Festival on 21 January 2009 and in the United States in September 2009 at the Los Angeles Irish Film Festival.

==Awards==
{| class="wikitable"
|-
!| Year
!| Award Show
!| Award
!| Category
!| Result
!| Recipient(s)
|-
| rowspan="2"| 2008 || rowspan="2"| Galway Film Fleadh || rowspan="2"| Feature Film Award || Best Feature ||   || Chris Martin
|-
| Best First Feature ||   || Macdara Vallely
|-
| rowspan="2"| 2009 || rowspan="2"| Angers European First Film Award || European Jury Award || Feature Film ||   || Chris Martin
|-
| Europen Special Jury Award || ||   || Macdara Vallely
|}

==References==

 

==External links==
*  

 
 
 
 
 
 
 