Hôtel du Nord
 
{{Infobox film
| name           = Hôtel du Nord
| image          = Hôtel du Nord.jpg
| caption        = Film poster
| director       = Marcel Carné
| producer       = Jean Lévy-Strauss
| writer         = Jean Aurenche Eugène Dabit Henri Jeanson Annabella
| music          = 
| cinematography = Louis Née Armand Thirard
| editing        = Marthe Gottie René Le Hénaff
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = France
| language       = French
| budget         = 
}}
Hôtel du Nord is a 1938 French drama film directed by Marcel Carné and starring Annabella (actress)|Annabella.   

==Plot==
The film follows the comings and goings at the Hôtel du Nord on the banks of the Canal St. Martin in Paris. The films begins with the gathering of many of the hotels occupants around the dinner table for the first communion of Michéle, who lives in the hotel with her policeman father, Maltaverne. Madame Lecouvreur tells Michèle to bring a piece of cake upstairs to Raymonde (Arletty), who is talking with her boyfriend, Edmond (Louis Jouvet). A prostitute, Raymonde leaves Edmond, a photographer, who wants to develop his film. In the meantime, a young couple, Renée (Annabella) and Pierre (Jean-Pierre Aumont), enter the hotel and rent a room for the night. Once alone, Renée and Pierre discuss their plan to kill themselves as they feel they have nothing left to live for. Pierre pulls out a gun and shoots Renee, but then loses his confidence and is unable to shoot himself too. Edmond hears the shots next door and breaks into the room to find Pierre standing over Renées body. Telling Pierre to escape, Edmond lies to the police and says he found the girl alone lying dead in the room. Raymonde, having stopped to have a drink with the celebration downstairs, is still in the hotel when the police arrive, and is put in jail for four days when an officer finds that her papers are not in order. 

Later in the week, Renée, who was only wounded, wakes up in the hospital and finds out that Pierre has given himself over to the police and admitted his guilt in trying to murder his girlfriend. While Renée tries to tell the police that it was a pact to kill themselves, they do not believe her and Pierre is brought to jail. Once out of the hospital, Renee returns to the hotel and is offered a job as a maid and waitress until she gets herself back on her feet. Attractive, young, and famous because of her suicide attempt, Renee becomes popular with the men in the hotel and bar. Edmond, who was going to leave for the south of France with Raymonde, changes his plans when he sees that Renee has returned to the hotel, and falls in love with her. Edmond declares his love to Renée and tells her that he is used to be a crook and is now hiding from two men. Renée and Edmond make plans to leave Paris and begin anew, but Renée changes her mind at the last minute and returns to the hotel to wait for Pierre to get out of prison. Edmond follows Renee back to town to say goodbye, and she tells him that the men who are looking for him are upstairs in the hotel. Edmond goes upstairs and gives himself over to his pursuer who kills him, but no one hears the shots because of the commotion on the streets for Bastille Day.

== Cast == Annabella as Renée
* Jean-Pierre Aumont as Pierre
* Louis Jouvet as Monsieur Edmond
* Arletty as Raymonde
* Paulette Dubost as Ginette Andrex as Kenel
* André Brunot as Emile Lecouvreur
* Henri Bosc as Nazarède
* Marcel André as Le chirurgien
* Bernard Blier as Prosper
* Jacques Louvigny as Munar
* Armand Lurville as Le commissaire (as Lurville)
* Jane Marken as Louise Lecouvreur
* Génia Vaury as L infirmière
* François Périer as Adrien
* René Bergeron as Maltaverne

==Production==
After the controversy over the army deserter in Port of Shadows, Carné wanted to steer clear of anything with political implications for his next film. Carné reduces any politics to that of the romantic relationships. The film required elaborate set pieces, but the films most elaborate sequence is the nighttime Bastille Day street celebration as it required over four hundred extras.   

==Adaptation==
LHôtel du Nord is an award-winning novel of the first Prix du Roman Populist and is a loose collection of sentimental tales about simple people residing in a hotel. The novel begins with Monsieur and Madame Lecouvreur buying and transforming a rundown hotel. The film begins with the hotel already up and running and gives no real mention of how the hotel came about. So too, the novel ends with the Lecouvreurs reluctantly selling the hotel to a large company that plans to construct an office building on the site and the tenants must unhappily leave and separate. The films ending is entirely modified and not only is the hotel not being demolished, but the film ends with the sense that this place and the people there are left standing in time untouched by the outside world. So too, the film focuses on criminals, prostitutes, and vagabonds, and develops the novels sentimental, rather than political, themes. 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 