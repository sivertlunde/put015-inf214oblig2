Apart from You
{{Infobox film
| name           = Apart From You
| image          =
| caption        =
| director       = Mikio Naruse
| producer       =
| writer         = Mikio Naruse
| starring       = Mitsuko Yoshikawa Akio Isono Sumiko Mizukubo
| music          =
| cinematography = Suketaro Inokai
| editing        =
| distributor    = Shochiku Company
| released       =  
| runtime        = 61 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          =
}}

  is a 1933 Japanese drama film written and directed by Mikio Naruse.

The film follows an aging geisha whose teenaged son is ashamed of her profession, and their relationship with a young colleague of hers.

==Plot==
Kikue (Mitsuko Yoshikawa) is an aging geisha.  Her teenage son, Yoshio (Akio Isono), is ashamed of her profession and engages in truancy and juvenile delinquency.

A young colleague of Kikues, Terugiku (Sumiko Mizukubo), engages with Yoshio and tries to get him to be more understanding of his mother.

==Cast==
* Mitsuko Yoshikawa as Kikue
* Akio Isono as Yoshio
* Sumiko Mizukubo as Terugiku
* Reikichi Kawamura as Terugiku’s father
* Tatsuko Fuji as Terugiku’s mother
* Yoko Fujita as Terugiku’s sister
* Jun Arai as Kikue’s husband
* Choko Iida as Landlady of geisha house

==Reception==
Keith Uhlich of Slant Magazine gave the film two and a half out of four stars somparing its "superficial stylistic flourishes" to his previous film Not Blood Relations. Ulrich adds, "Yet it is nonetheless a much more focused and sustained work, bearing some evidence—via several beautifully visualized superimpositions—of the directors developing interest in character psychology." 

==Release==

===Home video=== Eclipse label. Titled "Silent Naruse", it collected five silent films made between 1931 and 1934. 

==References==
 

==External links==
*  
*  at Japan Movie Database
*   by Michael Kerpan in Senses of Cinema, Issue 39 (5 May 2006)
*   by Craig Keller at Cinemasparagus (10 September 2011)

 

 
 
 
 
 
 
 
 
 
 
 
 