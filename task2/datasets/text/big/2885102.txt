Save the Green Planet!
{{Infobox film
| name           = Save the Green Planet!
| image          = Save_the_Green_Planet_Poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         =  !
 | hanja          =  를 지켜라!
 | rr             = Jigureul jikyeora!
 | mr             = Chigurŭl chik‘yŏra}}
| director       = Jang Joon-hwan
| producer       = Sidus Pictures
| writer         = Jang Joon-hwan
| starring       = Shin Ha-kyun Baek Yoon-sik
| music          = Lee Dong-jun
| cinematography = Hong Kyung-pyo
| editing        = Park Gok-ji
| distributor    = CJ Entertainment Koch-Lorber Films
| released       =  
| runtime        = 118 minutes
| country        = South Korea
| language       = Korean
| budget         = $3 million
| gross          = $15,516 (U.S) 
}} science fiction, horror and Thriller film|thriller. The basic story begins when the main character, Lee Byeong-gu, kidnaps another man, convinced that the latter is an alien.

==Plot==
The films main character is Byeong-gu, a man who believes that aliens from Andromeda are about to attack Earth and that he is the only one who can prevent them. With his childlike circus-performer girlfriend, he kidnaps a powerful pharmaceutical executive whom he believes to be a top ranking extraterrestrial able to contact the Andromedan prince during the upcoming eclipse. After imprisoning the man in his basement workshop, Byeong-gu proceeds to torture him. 

It soon appears that the executives company poisoned Byeong-gus mother in a pharmaceuticals test, and that it is vengeance fueled psychosis that causes Byeong-gu to believe the executive is an alien. 

When a detective comes calling to investigate the disappearance, the executive tries to escape but is thwarted by the psychotic Byeong-gu. The detective at first finds nothing unusual but on his way out sees Byeong-gus dog (appropriately named Earth) gnawing on the bones of his masters past victims. After contacting a partner in the police force he is killed by Byeong-gus bees, is hacked up and fed to the dog. Byeong-gu then crucifies the executive and breaks his leg with the back of his axe, to punish him for his attempted escape. In a desperate move, the executive convinces Byeong-gu that the bottle of benzene in his car trunk is the antidote for his comatose mother. 

As Byeong-gu races to the hospital to deliver the antidote, the executive frees himself by pulling his hands through the nails. He then travels deeper into his captor’s lair, finding evidence of his grim research. Photos of mutilated corpses are littered with blood scrawled notebooks, while hands and brains of past ‘subjects’ reside in jars. Reading through the journals the executive discovers Byeong-gus traumatic past: his father was a coal miner who lost one of his arms due to his dangerous work and was killed by his wife when he attempted to attack her and his son. The child was beaten in school and was a victim of the sadistic whims of his cruel teachers. He showed early signs of violence, such as stabbing a fellow school mate with a kitchen knife. His mother was then poisoned in the aforementioned incident and at a protest his former girlfriend was beaten to death. He slowly went mad from the violence that surrounded him. 

As this is happening, the dead detectives partner arrives and finds the frantic executive. And Byeong-gu, after desperately rushing to the hospital to give the antidote to his comatose mother, killing her, becomes ever more enraged. He returns home to kill the alien, only to find the detective there as well.  After a brief struggle and a bizarre turn of events, he captures both of them and plans on killing them both.  The frantic executive then admits to being an alien and proceeds to spin an outlandish tale which stretches back to the time of the dinosaurs, about how his race was originally trying to save humanity by experimenting on the genetic code of his mother. He also agrees, in what appears to be a time-buying move, to contact the alien prince at the pharmaceutical company factory. 

Byeong-gu leaves the detective all his notes, saying that if he does not make it, he will have the responsibility of saving the planet. At the factory, the executive triggers a computer controlled robotic arm to kill Byeong-gus girlfriend, and after a long struggle, he beats his captor almost to death. When the police arrive, they shoot Byeong-gu, and as he bleeds to death he wonders aloud, "Now who will save the earth?"

When the aliens do arrive and beam up the executive aboard their ship, we learn he is in fact the alien king himself. Disgusted and angered by the torture and corruption and evils of the world, he deems Earth a failed experiment and blasts it from creation. As the credits roll still photographs recap the entire journey of Byeong-gus life, focusing instead on the beautiful, happy moments of a young boy and man with his father and mother and girlfriend.

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- 
| Shin Ha-kyun || Lee Byeong-gu
|- 
| Baek Yoon-sik || Kang Man-shik
|-
| Hwang Jeong-min || Su-ni
|- Lee Jae-yong || Detective Choo
|-
| Lee Ju-hyeon || Detective Kim
|-
| Gi Ju-bong || Squad Leader Lee
|}

==Production==
Jang first conceived of the idea for Save the Green Planet! while watching the film   character, and accordingly decided that if he made a film about a kidnapping, it would be staged from the point of view of the kidnapper.  Later, Jang stumbled across a crank website accusing actor Leonardo DiCaprio of being an alien who wanted to conquer Earth by seducing all of its women, and he decided to combine the two concepts. 

==Awards and nominations==
;Blue Dragon Film Awards
*Best Supporting Actor: Baek Yoon-sik
*Best New Director: Jang Joon-hwan

;Brussels International Fantastic Film Festival 
*Golden Raven Award

;Buenos Aires International Festival of Independent Cinema
*Best Actress: Hwang Jeong-min
*ADF Cinematography Award

;Directors Cut Awards
*Best New Director: Jang Joon-hwan

;Grand Bell Awards
*Best Supporting Actor: Baek Yoon-sik
*Best New Director: Jang Joon-hwan
*Best Sound

;Korean Film Awards
*Best Supporting Actor: Baek Yoon-sik
*Best New Director: Jang Joon-hwan

;25th Moscow International Film Festival   
*Nomination - Golden Saint George
*Silver Saint George 

;Puchon International Fantastic Film Festival
*Best of Puchon

;Busan Film Critics Awards
*Best Film
*Best Actor: Shin Ha-kyun
*Best New Director: Jang Joon-hwan

;International Film Festival Rotterdam
*KNF Award Special Mention

==Notes==
 

== External links ==
*  
*   

 
 
 
 
 
 
 
 
 
 