Yesterday Was a Lie
{{Infobox film
| name           = Yesterday Was a Lie
| image          = Ywalposter.jpg
| image_size     = 225px
| producer       = Chase Masterson
| director       = James Kerwin
| writer         = James Kerwin John Newton
| music          = Kristopher Carter
| cinematography = Jason Cochard
| editing        = James Kerwin
| distributor    = Entertainment One
| released       =  
| runtime        = 89 minutes 
| country        = United States
| language       = English
| budget         = $200,000 
}}
 John Newton, and Mik Scriba. In publicity materials, the film has been described as a combination of science fantasy and film noir.   

== Plot == John Newton) Peter Mayhew).  

== Cast ==
* Kipleigh Brown as Hoyle
* Chase Masterson as Singer John Newton as Dudas
* Mik Scriba as Trench Coat Man
* Nathan Mobley as Lab Assistant Warren Davis as Psychiatrist
* Megan Henning as Student
* Jennifer Slimko as Nurse
* Robert Siegel as Radio Interviewer Peter Mayhew as Dead Man

== Production ==
Yesterday Was a Lie was in production from 13 August to 15 September 2006. IMDB    In March 2007, a trailer of the film premièred at San Francisco Wondercon.   In August 2007, the films official blog announced the completion of a test cut of the motion picture.

== Release ==
Festival run 

The early cut of the film began a series of film festival screenings on January 17, 2008 at the Park City Film Music Festival, where it received a Directors Choice Award.   The film went on to receive Best Feature awards at numerous festivals, including the ShockerFest International Film Festival,  as well as a bronze Telly Award and the Best of Show Accolade Award.  It was awarded the Best Director, Best Screenplay, and Best Cinematography trophies at Visionfest,  the Best Actress prize (for Brown) at ShockerFest, and the Best Producer prize (for Masterson) at the LA Femme Film Festival. 
 San Diego Comic-Con announced that the test cut of Yesterday Was a Lie would be presented as the closing film of its 2008 convention. 

In October 2008, the films official blog announced that a newer cut of the film would be shown at the St. Louis International Film Festival.  In December 2008, the Beverly Hills Hi-Def Film Festival announced the new cut of the film would have its theatrical première at the closing night film of its 2009 festival. 

Theatrical release 
In August 2009, Yesterday Was a Lie was acquired by Entertainment One.  According to the films official blog, a new cut of the movie, featuring an updated soundtrack and other changes, was created for the formal release.  Yesterday Was a Lie was released theatrically in the U.S. on December 11, 2009. 

Home video 
The film was released on DVD on April 6, 2010. 

== Reception ==
During the films festival run it received generally positive reviews. 

In a review published after the films U.S. theatrical opening, Variety (magazine)|Variety praised the films "stunning black-and-white HD cinematography" and "impressively atmospheric tone" and its recreation of the "classical Hollywood   aesthetic". The films "sultry jazz score" was also singled out for mention.  However, the review also criticized the casting of the film—calling the acting "stiff" and "hopelessly amateurish"—as well as the plot, which it described as a "clunky David Lynchian cosmic mystery" leading to "grand (yet underwhelming) revelations about the nature of reality." 
 Eliot to justify its often impenetrably surreal structure." It praises the film as being in "gorgeous black-and-white and lit by some extremely competent artisans" but concludes that "the film is finally too disjointed and incomprehensible to be enjoyed as much else besides an exercise in style." 
 KGO resident film critic Dennis Willis also all reviewed the film positively, with Willis calling Yesterday "nothing less than the arrival of a major filmmaker."     Author Robert J. Sawyer blogged that the movie was "the most thoughtful and compelling science fiction film of 2009" (a quote subsequently used in press materials).  The film received a positive review from The Numbers following its DVD release. 

Yesterday Was a Lie has a rating of 83% on the Rotten Tomatoes website. 

== Soundtrack ==

{{Infobox album| 
| Name        = Yesterday Was a Lie: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Kristopher Carter, Chase Masterson
| Cover       = Yesterday_Was_a_Lie_CD_cover.jpg
| Released    = February 15, 2011
| Recorded    =
| Genre       = Soundtrack
| Length      = 65:22
| Label       = La-La Land Records
| Misc        = {{Extra chronology 
| Artist      = Chase Masterson
| Type        = Soundtrack
| Last album  =
| This album  = Yesterday Was a Lie (2011)
| Next album  = "Burned with Desire" (2012)
  }}
}}

In February 2011, La-La Land Records announced the February 15 release of the Kristopher Carter score on CD and digital download, including songs performed by Masterson from the film.  The physical CD included two bonus tracks, "Can You Help Me?" and "City Talks", not included in the digital download. The album was well received, with Daniel Schweiger of Film Music Magazine calling it "a top notch indie score in all respects." 

{{Track listing
| collapsed =no
| headline = Yesterday Was a Lie: Original Motion Picture Soundtrack
| extra_length = Performer
| title1 = Dream Time
| length1  = 5:08
| title2 = Nice Set
| length2  = 3:12
| title3 = Half-Deserted Streets
| length3  = 1:22
| title4 = Cat State
| length4  = 0:59
| title5 = Trauma Creates Ripples
| length5  = 3:22
| title6 = Yesterday Was a Lie
| length6  = 2:16
| note6 = performed by Chase Masterson
| title7 = Achromatopsia
| length7  = 2:43
| title8 = Orchestral Suite No. 3 in D Major, BWV 1068: II. Air
| length8  = 1:42
| note8 = composed by Johann Sebastian Bach
| title9 = Backdoor Research
| length9  = 1:33
| title10 = 6.626 x 10^-34
| length10 = 3:56
| title11 = Ajna
| length11 = 1:51
| title12 = Fenestra Aeternitatis
| length12 = 2:24
| title13 = Synchronicity
| length13 = 1:32
| title14 = Aker
| length14 = 5:09
| title15 = Distillation
| length15 = 4:12
| title16 = Getting a Message Through
| length16 = 2:04
| title17 = Why Do You Keep Coming Back Here?
| length17 = 3:25
| title18 = Where Do You Start?
| length18 = 4:36
| note18 = performed by Chase Masterson
| title19 = He Wont Forget You
| length19 = 4:35
| note19 = performed by Chase Masterson feat. Simon Shapiro
| title20 = Anima in an Elevator
| length20 = 2:28
| title21 = Can You Help Me?
| length21 = 1:31
| note21 = deleted segment from "Aker"; bonus track available on CD only
| title22 = City Talks
| length22 = 4:21
| note22 = performed by Simon Shapiro; bonus track available on CD only
}}

==Spin-offs==
;Graphic novel
The film has been adapted into a limited edition graphic novel by artist James Hill, first released as a San Diego Comic-Con Exclusive at 2010s Comic-Con International. 

;Web series Easter egg on the films official site.  

==References==
Notes
 

== External links ==
*  
*  
*  
*  
*  
*   article on the WonderCon panel

 
 
 
 
 
 
 
 
 