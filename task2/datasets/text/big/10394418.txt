Secrets of a Secretary
 
{{Infobox film
| name           = Secrets of a Secretary
| image          = Secretofasecretary.jpg
| caption        =
| director       = George Abbott
| producer       = George Abbott
| writer         = George Abbott (adaptation) Charles Brackett (story)
| starring       = Claudette Colbert Herbert Marshall
| music          = Johnny Green
| cinematography = George J. Folsey
| editing        = Helene Turner
| distributor    = Paramount Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States
| awards         = English
| budget         =
}}
Secrets of a Secretary is a 1931 film directed by George Abbott, and starring Claudette Colbert and Herbert Marshall. Stage actress Mary Bolands first talking film.

==Plot==
Society girl becomes a social secretary when her father dies penniless. From a story by Charles Brackett.

==Cast==
*Claudette Colbert as Helen Blake
*Herbert Marshall as Lord Danforth
*Georges Metaxa as Frank DAgnoll
*Betty Lawford as Sylvia Merritt
*Mary Boland as Mrs. Merritt
*Berton Churchill as Mr. Merritt
*Averell Harris as Don Marlow
*Betty Garde as Dorothy White
*Hugh OConnell as Charlie Rickenbacker

==See also==
*The House That Shadows Built (1931 promotional film by Paramount)

==External links==
* 
*  
*  

 

 
 
 
 
 
 
 
 


 