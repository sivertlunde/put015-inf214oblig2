Our Hospitality
 
 
{{Infobox film
| name           = Our Hospitality
| image          = Keaton_Our_Hospitality_1923.jpg
| caption        = Our Hospitality (1923)
| writer         = Clyde Bruckman
| starring       = Buster Keaton Joe Roberts Ralph Bushman Craig Ward Monte Collins Joe Keaton Kitty Bradbury Natalie Talmadge Buster Keaton Jr
| director       = Buster Keaton John G. Blystone
| producer       = Joseph M. Schenck
| cinematography = Gordon Jennings Elgin Lessley Metro Pictures Corporation
| released       =  
| runtime        = 74 minutes English intertitles
| country        = United States
}}

Our Hospitality is a silent comedy directed by and starring Buster Keaton. Released in 1923 by Metro Pictures Corporation, the movie uses slapstick and situational comedy to tell the story of Willie McKay, who gets caught in the middle of the infamous "Canfield"–"McKay" feud, an obvious satire of the real-life Hatfield–McCoy feud.

== Plot summary ==
The Canfield and McKay families have been feuding for so long, no one remembers the reason the feud started in the first place. One stormy night in 1810, after family patriarch John McKay falls victim to the feud, his wife decides her son Willie (the infant Buster Keaton Jr.) will not suffer the same fate. She moves to New York to live with her sister, who after the mothers death raises him without telling him of the feud.

Twenty years later, Willie (Buster Keaton Sr.) receives a letter informing him that his fathers estate is now his. His aunt tells him of the feud, but he decides to return to his Southern birthplace anyway to claim his inheritance.

On the train ride, he meets a girl, Virginia (played by Keatons wife Natalie Talmadge). They are shy to each other at first, but become acquainted during many train mishaps. At their destination, she is greeted by her father and two brothers; she, it turns out, is a Canfield. Willie innocently asks one of the brothers where the McKay estate is. The brother offers to show him the way (but stops at every shop in search of a pistol to shoot the unsuspecting Willie). By the time he obtains one, Willie has wandered off. Willie is very disappointed to discover the McKay "estate" is a rundown home, not the stately mansion he had imagined. Later, however, he encounters Virginia, who invites him to supper. 

When he arrives, the brothers want to shoot him, but the father refuses to allow it while he is a guest in their mansion. The father refers to this as "our hospitality". When Willie overhears a conversation between the brothers, he finally realizes his grave predicament. A parson comes to supper as well. Afterward, the parson prepares to leave, but he finds it is raining furiously. The Canfield patriarch insists the parson stay the night. McKay invites himself to do the same.

The next morning, McKay stays inside the house, while the Canfield men wait for his departure. The father catches McKay kissing his daughter. McKay finally manages to leave safely by putting on a womans dress. However, a chase ensues.

He eventually starts down a steep cliff side, but is unable to find a way to the bottom. One Canfield lowers a rope (so he can get a better shot) to which Willie ties himself, but the Canfield falls into the water far below, dragging Willie along. Finally, Willie manages to steal the train locomotive and Tender (rail)|tender, but the tender derails, dumping him into the river towards the rapids. Virginia spots him and goes after him in a rowboat; she falls into the water and is swept over the edge of the large waterfall. McKay swings trapeze-like on a rope, catching her hands in mid-fall and depositing her safely on a ledge.

When it grows dark, the Canfield men decide to continue their murderous search the next day. Returning home, they see Willie and Virginia embracing; Joseph Canfield furiously rushes into the room, gun in hand.  He is brought up short by the parson, who asks him if he wishes to kiss the bride. Seeing a hanging "love thy neighbor" sampler (needlework)|sampler, the father decides to bless the union and end the feud. The Canfields place their pistols on a table; Willie then divests himself of the many guns he took from their gun cabinet.

==Cast==
* Buster Keaton - Willie McKay
* Joe Roberts - Joseph Canfield
* Natalie Talmadge - Virginia Canfield
* Ralph Bushman - Clayton Canfield
* Craig Ward - Lee Canfield
* Monte Collins - The Parson
* Joe Keaton - The Engineer
* Kitty Bradbury - Aunt Mary
* Buster Keaton Jr. - Willie McKay (1 year old)

==Production==
Some exteriors were shot near Truckee, California, and in Oregon.  The famous waterfall rescue scene was shot using a special set at Keatons Hollywood studio.
 The General (1926), and were shot in the same Oregon locations.  

Actor and Keaton friend Joe Roberts suffered a stroke while making this film, and died of a subsequent stroke shortly after the films completion.

This is the only film to feature three generations of Keatons. Busters father plays a train engineer while Busters infant son plays a baby version of Buster in the films prologue.  Keatons wife Natalie was pregnant with their second child during filming, and late in the production she had to be filmed to hide her growing size.

==Adaptation== Telugu (Tollywood) Ajay Devgn Kannada film adaptation was titled Maryada Ramanna starring  Komal Kumar and Nisha Shah.
 Tamil ( Kollywood ) film remake called  Vallavanukku Pullum Aayudham starring Santhanam (actor)|Santhanam was released on 10 May 2014.

==See also==
* Buster Keaton filmography

==References==
*  

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 