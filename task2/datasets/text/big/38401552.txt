The Other (1930 film)
{{Infobox film
| name           = The Other
| image          = 
| image_size     = 
| caption        = 
| director       = Robert Wiene 
| producer       = 
| writer         = Paul Lindau (play)   Johannes Brandt
| narrator       = 
| starring       = Fritz Kortner   Käthe von Nagy   Heinrich George   Hermine Sterler
| music          = Artur Guttmann   Will Meisel   Friedrich Hollaender
| editing        = 
| cinematography = Nicolas Farkas
| studio         = Terra-Filmkunst
| distributor    = Terra-Filmverleih 
| released       = 12 August 1930
| runtime        = 
| country        = Germany
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
The Other (German: Der Andere) is a 1930 German drama film directed by Robert Wiene and starring Fritz Kortner, Käthe von Nagy and Heinrich George. It was based on the 1893 play Der Andere by Paul Lindau.  A French-language version The Prosecutor Hallers was shot by Wiene immediately afterwards in the same Berlin studio, but with different actors.

==Cast==
* Fritz Kortner as Staatsanwalt Hallers 
* Käthe von Nagy as Analie Frieben 
* Heinrich George as Dickert 
* Hermine Sterler as Hallers Schwester 
* Ursula van Diemen as Marion 
* Eduard von Winterstein as Dr. Koehler 
* Oskar Sima as Gruenspecht 
* Julius Falkenstein as Sekretaer Bremer 
* Paul Bildt as Prof. Wertmann 
* Otto Stoessel as Medizinalrat Rienhofer 
* Emil Heyse as Polizeikommissar 
* Hans Ahrens as Wachtmeister

==References==
 

==Bibliography==
* Jung, Uli & Schatzberg, Walter. Beyond Caligari: The Films of Robert Wiene. Berghahn Books, 1999.

==External links==
* 

 
 
 
 
 
 
 
 
 

 