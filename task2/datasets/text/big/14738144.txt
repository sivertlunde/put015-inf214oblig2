The Ghost Talks (1949 film)
{{Infobox Film |
  | name           = The Ghost Talks |
  | image          = Ghosttalks49LOBBY.jpg|
  | caption        = |
  | director       = Jules White Felix Adler
  | starring       = Moe Howard Larry Fine Shemp Howard Phil Arnold Nancy Saunders|
  | cinematography = M.A. Anderson | 
  | editing        = Edwin H. Bryant |
  | producer       = Jules White |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 16 07"
  | country        = United States
  | language       = English
}}

The Ghost Talks is the 113th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot== Peeping Tom (voiced by Phil Arnold) scares the hapless Stooges, until he convinces them that he is, in fact, a friendly spirit. After finally gaining their trust, Tom tells the trio the story of his watching Lady Godiva (Nancy Saunders), only to get a pie in the face. In turn, his ghost is cursed and trapped inside the suit of armor, having been trapped for 1000 years.

The Stooges, however, still have a job to do and tell Tom that they have to move everything in the castle, including him. He instructs the boys to leave him be, as "bad luck" will be upon them should they try to take him away. Shemp, Larry and Moe all take turns trying to move Tom, but various shenanigans, such as a frog jumping down Shemps shirt and an owl entering a skull and assuming the role of a deaths head spirit, spooks the Stooges. As they run into another room to escape, Lady Godiva rides up on a horse and takes Tom away. The Stooges rush over to the window to watch them depart, only to be pelted with three successive pies amidst a cheering crowd. 

==Production notes== remade in 1956 as Creeps (film)|Creeps, using ample stock footage.  
*Director Jules White voiced the skeleton identifying himself as Red Skeleton, a reference to comedian Red Skelton. 
*The NBC chimes are heard when Moe stabs Shemp with a crooked safety pin.

==See also==
*List of ghost films

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 