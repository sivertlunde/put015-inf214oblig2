Traveling Husbands
 
{{Infobox film
| name           = Traveling Husbands
| image          = File:TravelingHusbands.1931.jpg
| caption        = Theatrical poster for the film Paul Sloane Charles Kerr (assistant)
| producer       = William LeBaron
| writer         = Humphrey Pearson
| starring       = Evelyn Brent
| music          = Max Steiner
| cinematography = Leo Tover
| editing        = Archie Marshek
| studio         = RKO Radio Pictures
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Paul Sloane based on a screenplay by Humphrey Pearson. The film stars Constance Cummings, Frank Albertson, Evelyn Brent, Dorothy Peterson and Hugh Herbert.  Hugh Herberts brother, Tom Herbert|Tom, made his screen debut with a small role in this film, billed as Tom Francis. Produced and distributed by RKO Radio Pictures, the film premiered in New York City on August 7, 1931, and was released nationwide the following week on August 15.  It received mixed reviews from the critics.

==Plot==
While waiting to see the owner of the store who is potentially a new big client, Barry Greene, a traveling salesman, practices his sales pitch on Ellen Wilson.  Unbeknownst to Barry, Ellen is the daughter of his potential client, J.C. Wilson. Her father, obsessed with business, has neglected his daughter.  When she goes in to speak with her father, his usual lack of interest in her life causes her to decide to teach him a lesson by living a wild life. Angered by his daughter, when J.C. meets with Barry, he has no desire to listen to the salesman, and has him thrown out of the building. On his way out, Ellen offers him a ride to his hotel.  En route, the two make a date for dinner that night. 

Back at the hotel, Barrys compatriots, all "traveling husbands" (married traveling salesmen), are enjoying a party with several call girls. One of the prostitutes, Ruby Smith, has fallen in love with one of the salesmen, Ben Hall.  Barry has no interest in joining the party, despondent over his failure with J.C.  This is exacerbated when Barry learns that his expense account has been put on hold until he can prove himself.  Not being able to afford his dinner with Ellen, he calls her up and cancels their dinner date. Disappointed that her dinner has been canceled, Ellen is determined to have a good time that evening, and decides to go to the hotel for dinner by herself. When Ellen shows up at the hotel she attracts Bens attention, who joins her for dinner.  After dinner, in an attempt to awe her, Ben takes Ellen on a whirlwind tour of Detroit nightspots. 

When they arrive back at the hotel, Ben takes Ellen back to his room.  When he attempts to force his attentions on her, her cries arouse several people, one of whom is Barry.  When Barry bursts into Bens room, an altercation occurs.  In the dark, a gunshot is heard, and when the lights come on, Ben lies on the floor, shot. In the ensuing investigation, it is uncovered that Ruby shot Ben in a jealous fit of rage.  Martha, Bens wife, has arrived and reconciles with her husband.  J.C. has also arrived, and realizes how he has ignored his daughter.  He, Ellen and Barry leave the hotel together.

==Cast==
* Evelyn Brent as Ruby Smith
* Frank Albertson as Barry Greene
* Constance Cummings as Ellen Wilson
* Hugh Herbert as Hymie Schwartz
* Dorothy Peterson as Martha Hall
* Gwen Lee as Mabel
* Frank McHugh as Pinkie Carl Miller as Ben Hall Stanley Fields as Dan Murphy
* Rita La Roy as Daisy
* Lucille Williams as Vera
* Purnell Pratt as J. C. Wilson
* Spencer Charters as Joe
* Tom Herbert as Walter (as Tom Francis)

==Critical reception== Daily Mirror Daily News Herald Tribune The Sun, however, did not care for the film, calling it "melodrama at its worst."  The New York World-Telegram|World-Telegram also gave it a less than kind endorsement, saying that while the film "... flashed a few moments of amusement," those moments were so few and far between "for comfort".   Photoplay complimented the acting, calling it "top notch", while saying that the film was "risqué, but not objectionably so".  Silver Screen merely rated the film, "fair". 

==References==
 

==External links==
* 

 
 
 
 
 
 