Anjane Mein
{{Infobox film
| name           = Anjane Mein
| image          = Anjane Mein.jpg
| image_size     = 
| caption        = 
| director       = Samir Ganguly
| producer       = 
| writer         =
| narrator       = 
| starring       = Rishi Kapoor   Neetu Singh
| music          = Kalyanji Anandji
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1978 Bollywood film directed by Samir Ganguly. The film stars Rishi Kapoor and Neetu Singh.

==Cast==
*Rishi Kapoor...Raja 
*Neetu Singh...Rani
*Nirupa Roy...Mrs. Shanti Lal 
*Ranjeet...Ranjeet 
*Asha Sachdev...Rita 
*Jagdeep...Abdullah 
*Satyendra Kapoor...Manohar Lal 
*Ramesh Deo...Police Inspector Gavaskar 
*Anwar Hussain...Sher Singh 

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Dil Ka Rishta Jod Diya Hai"
| Kishore Kumar, Asha Bhosle
|-
| 2
| "Gayi Kaam Se Gayi Ye Ladki"
| Kishore Kumar
|-
| 3
| "Jeevan Ke Sab Sukh Paye Tu (Male)"
| Amit Kumar
|-
| 4
| "Jeevan Ke Sab Sukh Paye Tu (Female)"
| Lata Mangeshkar
|-
| 5
| "Main Jan Gaya Hoon"
| Kishore Kumar
|-
| 6
| "Meri Jaan Zara Thik Se Dekho"
| Kishore Kumar, Asha Bhosle
|-
| 7
| "Sachchai Ki Raah Mein"
| Lata Mangeshkar
|}
==External links==
*  

 
 
 
 

 