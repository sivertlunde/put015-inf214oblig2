I Thank You (film)
{{Infobox film
| name           = I Thank You
| image          = 
| image_size     = 
| caption        = 
| director       = Marcel Varnel
| producer       = Edward Black (producer)
| writer         = Marriott Edgar (writer) Val Guest (writer) Howard Irving Young (original story)
| narrator       = 
| starring       = See below
| music          = Noel Gay
| cinematography = Jack E. Cox Arthur Crabtree
| editing        = R.E. Dearing
| studio         = Gainsborough Pictures
| distributor    = 
| released       = 20 October 1941
| runtime        = 83 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} British comedy Edward Black at Gainsborough Pictures.  

== Plot summary ==
The film is set in London during World War II at the time of the The Blitz|Blitz. The leads are a couple of out of work variety entertainers who use great ingenuity in their efforts to get financial assistance to "put on a show". Hoping to put their proposal to the formidable Lady Randall, ex-music hall star Lily Morris, they infiltrate her house in the guise of a servant (Murdoch) and cook (Askey - in drag). After some farcical interludes, they achieve their aim after Lady Randall is persuaded to sing an old music hall standard "Waiting at the Church" at an impromptu show located underground at Aldwych tube station, - used during wartime as an underground bomb shelter. As the ex-music hall star, Lily Morris plays herself. The title of the film is a gentrified version of Arthur Askeys famous catch-phrase - "I thangyew". Also in the film is elderly comic actor Moore Marriott who plays Lady Randalls somewhat eccentric father and the somewhat ubiquitous Albert (Graham Moffatt) who appears under that name in the comedy films of both Will Hay and Arthur Askey.

== Cast ==
*Arthur Askey as Arthur
*Richard Murdoch as Stinker
*Lily Morris as Lady Randall
*Moore Marriott as Pop Bennett
*Graham Moffatt as Albert Brown
*Peter Gawthorne as Dr. Pope
*Kathleen Harrison as Cook
*Felix Aylmer as Henry Potter Cameron Hall as Lomas
*Wally Patch as Bill
*Roberta Huby as Bobbie
*Noel Dainton as Police Sergeant
*Phyllis Morris as Miss Pizer 
*Charlie Forsythe as Himself 
*Addie Seamon - Herself 
*Eleanor Farrell - Herself 
*Issy Bonn

== Soundtrack ==
*Arthur Askey - "Hello to the Sun" (Written by Noel Gay and Frank Eyton)
*Arthur Askey and Richard Murdoch - "Half of Everything Is Yours" (Written by Noel Gay and Frank Eyton)
*Eleanor Farrell - "Oh Johnny, Teach Me to Dance" (Written by Noel Gay and Frank Eyton)
*Charlie Forsythe - "Lets Get Hold of Hitler" (Written by Noel Gay and Frank Eyton)
*Lily Morris - "Waiting at the Church" (Written by Fred W. Leigh and Henry E. Pether)

== References ==
 

== External links ==
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 


 
 