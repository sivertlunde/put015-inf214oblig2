The Ister (film)
 
{{Infobox Film | name = The Ister
  | image =
  | caption = The Ister film poster Daniel Ross
  | starring = Bernard Stiegler Jean-Luc Nancy Philippe Lacoue-Labarthe Hans-Jürgen Syberberg
  | released = 23 January 2004, International Film Festival Rotterdam
  | runtime = 189 minutes
 }}
 Daniel Ross.

== Sources ==
The Ister was inspired by a 1942 lecture course delivered by the German philosopher Martin Heidegger, published in 1984 as Hölderlins Hymn "The Ister"|Hölderlins Hymne »Der Ister«. Heideggers lecture course concerns a poem by the German poet Friedrich Hölderlin about the Danube River.
 NATO bombing of Yugoslavia.

== Interviewees ==
The Ister features extensive interviews with the French philosophers  ).

An extended interview with philosopher Werner Hamacher is also included as one of the "extra features" on the DVD.

== Locations == castle at Marshal Pétain fled in 1945.
 rectorial address). 
 Breg and the Brigach, the two tributaries whose confluence marks the point at which the river becomes known as the Danube. The film then travels upstream along the Breg, past Vöhrenbach and Furtwangen, in search of the "true" mountain source of the Danube.

== Structure ==
The Ister is divided into five chapters, plus a prologue and epilogue:
*Prologue. The myth of Prometheus, or The birth of technics. Bernard Stiegler tells the myth of Prometheus.
*Chapter 1. Now come fire! "In which the philosopher Bernard Stiegler conjugates technology and time, and guides us from the mouth of the Danube to the city of Vukovar in Croatia."
*Chapter 2. Here we wish to build. "In which the philosopher Jean-Luc Nancy takes up the question of politics and guides us through the Republic of Hungary."
*Chapter 3. When the trial has passed. "In which philosopher Philippe Lacoue-Labarthe conducts us from the technopolis of Vienna to the depths of the concentration camp at Mauthausen, confronting Heideggers most provocative statement concerning technology."
*Chapter 4. The rock has need of cuts. "In which philosopher Bernard Stiegler returns to guide us deeper into the questions of mortality and history, as we emerge from Mauthausen to the Hall of Liberation in Germany."
*Chapter 5. What that river does, no-one knows. "In which the German artist and director Hans-Jürgen Syberberg guides us through the upper Danube, to the source of the river and beyond."
*Epilogue. Heidegger reads Hölderlin. Heidegger reads Hölderlins hymn, "Der Ister."

== Soundtrack ==
Three excerpts from classical works feature in the film: Symphony No. 4 in E-flat major, first movement.
*Richard Wagner, "Siegfrieds Funeral March," from Götterdämmerung, Act 3. Impromptu D. 899 (Op. 90), No. 1 in C minor.

== Premiere and awards ==
The Ister premiered at the International Film Festival Rotterdam on 23 January 2004. It has won two awards: Festival International du Documentaire de Marseille (August 2004).
*The   at the Festival du Nouveau Cinéma in Montreal (October 2004).

Additionally, Robert Koehler, film critic for Variety (magazine)|Variety, listed The Ister as the second best film released theatrically in the United States in 2006. 

== Reviews ==
* , by Michael Atkinson
* , by Babette Babich
* , by Daniel Birnbaum
* , by Carloss James Chamberlin
*The Ister, by Chris Chang, Film Comment, vol. 46, no. 1 (Jan./Feb. 2010), p.&nbsp;82.
* , by Matthew Clayfield
* , by Patrick Crogan
* , by Linda Daley
* , by Cheryl Danieri-Ratcliffe
* , by Tom Dawson
* , by Matthew Del Nevo
*"The Ister," reviewed by Roy Elveton, German Studies Review 29 (2006): 480–481.
* , by Gareth Evans
* , by Hamish Ford
* , by Scott Foundas
* , by Philip French
* , by Philippa Hawker
* , by Philippa Hawker
* , by Eric Henderson
* , by J. Hoberman
* , by Jonathan Judaken
* , by Peter Kemp
* , by Adam Kirsch
* , by Dragan Kujundzic
* , by Nathan Lee
* , by Adrian Martin
* , by John McMurtrie
* , by Peter Monaghan
* , by Deborah Nichols
* , by Scott Nygren
* , by Gaël Pasquier  
* , by Dominic Pettman
* , by Jonathan Rosenbaum
* , by Jamie Russell
* , by Galili Shahar
* , by Michael Sicinski
* , by Ruth Starkman
* , by James van Maanen
* , by Mike Wood

==References==
 

== External links ==
*Official site:  
* 
* 
*  at the Internet Movie Database

 

 
 
 
 