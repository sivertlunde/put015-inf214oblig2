Nancy, Please
 

{{Infobox film
| image          = Nancy, Please poster.jpg
| alt            = Theatrical release poster
| caption        = Theatrical release poster
| director       = Andrew Semans
| producer       = {{plainlist|
* Dave Saltzman
* Vinay Singh
}}
| writer         = {{plainlist|
* Will Heinrich
* Andrew Semans
}}
| starring       = {{plainlist|
* Will Rogers
* Eleonore Hendricks|Eléonore Hendricks
* Rebecca Lawrence Levy
* Santino Fontana
}} Chris White
| cinematography = Eric Lin
| editing        = Ron Dulin
| studio         = Small Coup Films
| distributor    = Factory 25
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
}}

Nancy, Please is a 2012 American drama film directed by Andrew Semans. It stars Will Rogers as Paul, a PhD candidate struggling to complete his thesis who gets into a seemingly insignificant conflict with his former roommate, Nancy (Eleonore Hendricks|Eléonore Hendricks), over a missing book. Pauls partner, Jen (Rebecca Lawrence Levy), and his friend, Charlie (Santino Fontana), unsuccessfully try to help resolve the conflict.

==Critical response==
Nancy, Please has received favorable reviews from film critics. Rotten Tomatoes reports 100% positive reviews and an average rating of 7.8/10 from 8 critics,  while Metacritic reports an average score of 76/100 from 5 critics.  Jeannette Catsoulis in The New York Times appreciates the "tone that’s precisely balanced between terror and farce".  In The Hollywood Reporter, Frank Scheck writes that "Andrew Semans presents an engrossing psychological thriller for his feature debut." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 


 