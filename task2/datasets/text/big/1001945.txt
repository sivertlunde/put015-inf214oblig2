My Learned Friend
 
 
 
{{Infobox film
| name           = My Learned Friend
| image          = "My_Learned_Friend"_(1943).jpg
| image_size     = 
| caption        = 
| director       = Basil Dearden Will Hay
| producer       = Michael Balcon Robert Hamer
| writer         = John Dighton Angus MacPhail
| narrator       = 
| starring       = Will Hay Ronald Shiner Charles Victor
| music          = Ernest Irving
| cinematography = Wilkie Cooper
| editing        = Charles Hasse
| studio         = Ealing Studios
| distributor    = Ealing
| released       = 
| runtime        = 74 minutes
| country        = UK
| language       = English
| budget         = 
| gross          = 
}}

My Learned Friend is a 1943 British,   using the respectful term, "my learned friend". The supporting cast included Claude Hulbert, Mervyn Johns and Ernest Thesiger. It was the last film featuring Will Hay as he had an operation, however, it wasnt the last role in his career as he would star as "Doctor Muffin" in The Will Hay Programme that aired on the radio, the radio show began in 1944. The humour of the film was different from the humour of Hays films he previously made with Moore Marriott and Graham Moffatt and the film had more of a dark humour than any of Hays other films. Hay had planned to make more films with Claude Hulbert that had a dark humour, but it was Hays last film due to illness.

==Plot== forger whom he previously defended unsuccessfully. He teams up with an incompetent solicitor to try to prevent the deaths of others involved.
 Big Ben in an attempt to prevent a time bomb being detonated.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 