Mojave (film)
{{Infobox film
| name           = Mojave
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = William Monahan
| producer       = Aaron L. Ginsburg   William Green   Justine Suzanne Jones   William Monahan
| writer         = William Monahan
| starring       = Oscar Isaac   Garrett Hedlund   Mark Wahlberg   Louise Bourgoin   Walton Goggins
| music          = Andrew Hewitt
| cinematography = Don Davis
| editing        = John David Allen
| studio         = Atlas Independent   Henceforth Pictures   MICA Entertainment
| distributor    = A24 Films  
| released       =   
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Mojave is an upcoming American crime thriller film directed and written by William Monahan.  The film stars Oscar Isaac, Garrett Hedlund, Mark Wahlberg, Louise Bourgoin and Walton Goggins.

The film premiered at the Tribeca Film Festival in April 18, 2015.   

== Plot ==
A near-suicidal artist (Garrett Hedlund) who escapes to the desert only to encounter his doppelganger-like antagonist, a homicidal drifter (Oscar Isaac).

== Cast ==
* Oscar Isaac 
* Garrett Hedlund 
* Mark Wahlberg 
* Walton Goggins 
* Louise Bourgoin 
* Fran Kranz 
* Dania Ramirez
* Matt L. Jones

== Production ==
On March 22, 2012, it was announced that Atlas Independent is producing the film which William Monahan is set to direct on his own script, and he will also produce the film.    Henceforth Pictures is also producing the film, while Relativity International will co-finance. 

=== Casting === Jason Clarke have joined the cast of the film for the lead roles.    On May 16, 2013, Garrett Hedlund was set to star in the film for a supporting role.    On July 18, 2013, Louise Bourgoin has joined the film.    On September 27, 2013, Walton Goggins has also joined the cast of the film.    On October 2, 2013, Fran Kranz has landed a role in the film, hell play a post-production assistant to Isaacs character.    On March 24, 2014, Mark Wahlberg has revealed that he has filmed a supporting role in the film along with Isaac and Hedlund, he told "Collider.com" that he plays a small role in the film.   

=== Filming ===
In March 2012, the production of the film was set to begin in August in Southern California.  In December 2012 the production was set again to start in early 2013.  On September 27, 2013, the filming was underway at a location in Los Angeles and Mojave Desert.  

=== Post-production ===
On July 10, 2014, it was announced that Andrew Hewitt would be composing the music for the film. 

== Release ==
On November 7, 2013, some images from the film were released.   

The film had its premiere at the Tribeca Film Festival in April 18, 2015. 

== Distribution ==
On April 24, 2015, A24 Films and its partner DirecTV acquired the U.S. rights to distribute the film.  

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 