The Scarf (film)
 The Scarf}}
{{Infobox film
| name           = The Scarf
| image          = The scarf poster 1951 small.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Ewald André Dupont
| producer       = Isadore Goldsmith
| screenplay     = Ewald André Dupont
| story          = Isadore Goldsmith E.A. Rolfe
| narrator       = John Ireland James Barton Emlyn Williams
| music          = Herschel Burke Gilbert
| cinematography = Franz Planer
| editing        = Joseph Gluck
| studio         = Gloria Productions Inc.
| distributor    = United Artists
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} John Ireland, James Barton and Emlyn Williams. 

==Plot==
A man escapes from an insane asylum and tries to convince a crusty hermit, a drifting saloon singer and himself that he is not a murderer.

==Cast== John Ireland as John Howard Barrington
* Mercedes McCambridge as Connie Carter James Barton as Ezra Thompson
* Emlyn Williams as Dr. David Dunbar
* Lloyd Gough as Asylum Dr. Gordon
* Basil Ruysdael as Cyrus Barrington David Wolfe as Level Louie Harry Shannon as Asylum Warden Anderson
* Celia Lovsky as Mrs. Cyrus Barrington
* David McMahon as State Trooper
* Chubby Johnson as Feed Store Manager
* Frank Jenks as Tom - Drunk cowboy
* Emmett Lynn as Jack the Waiter
* Dick Wessel as Sid - Drunk cowboy
* Frank Jaquet as Town Sheriff
* Iris Adrian as the floozy at Level Louies Place

==Reception==
===Critical response===
Film critic Bosley Crowther panned the film, "For a picture so heavily loaded with lengthy and tedious talk, talk, talk, The Scarf, the new tenant at the Park Avenue, has depressingly little to say. As a matter of fact, it expresses, in several thousand words of dialogue—and in a running-time that amounts to just four minutes short of an hour and a half—perhaps the least measure of intelligence or dramatic continuity that you are likely to find in any picture, current or recent, that takes itself seriously."  

==References==
 

==External links==
*  
*  
*  
*  

 


 

 
 
 
 
 
 
 
 