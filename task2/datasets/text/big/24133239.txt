It's a Joke, Son!
{{Infobox film
| name           = Its a Joke, Son!
| image_size     =
| image	         = Its a Joke, Son! FilmPoster.jpeg
| caption        = Original film poster
| director       = Benjamin Stoloff
| producer       = Bryan Foy (executive producer) Aubrey Schenck (producer)
| writer         = Robert E. Kent Paul Girard Smith
| narrator       =
| starring       = See below
| music          = Alvin Levin
| cinematography = Clyde De Vinna
| editing        = Norman Colbert
| studio         = Bryan Foy productions
| distributor    = Eagle-Lion Films Alpha Video Distributors (2004 DVD) MediaOutlet.com (USA DVD) Mill Creek Entertainment (USA DVD, unknown Year) Republic Pictures Home Video (USA VHS) Synergy Entertainment (USA DVD, Unknown Year) Video Yesteryear (USA VHS, Alongside Republic Pictures Home Video)
| released       =  
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}
 Senator Beauregard Claghorn; the inspiration for the cartoon character Foghorn Leghorn.  The film was the first American production of Eagle-Lion Films. p.23 Balio, Tino United Artists: The Company That Changed the Film Industry
Univ of Wisconsin Press, 1987 

== Plot summary ==
When the Daughters of Dixie nominate Magnolia Claghorn as a candidate for State Senator, the local political machine run by Northerners fear their candidate will be defeated. Through the Claghorns daughters boyfriend Jeff, the machine gets the idea to run Magnolias husband Beauregard as a candidate in order to split the anti-machine vote.

They dont count on Beaurgards incredible popularity and seek to stop him.

== Cast ==
  Senator Beauregard Claghorn
*Una Merkel as Mrs. Magnolia Claghorn
*June Lockhart as Mary Lou Claghorn
*Kenneth Farrell as Jefferson "Jeff" Davis
*Douglass Dumbrille as Big Dan Healey
*Jimmy Conlin as Senator Alexander P. Leeds
*Matt Willis as Ace, Healeys Henchman
*Ralph Sanford as Knifey, Healeys Henchman
*Daisy as Daisy
*Vera Lewis as Hortense Dimwitty
*Margaret McWade as Jennifer Whipple
*Ida Moore as Matilda Whipple

== Soundtrack ==
* Dixie (song)|Dixie
* The Bonnie Blue Flag

==Notes==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 


 