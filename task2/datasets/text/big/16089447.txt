Jocks (film)
{{Infobox film
| name           = Jocks
| image          = Jocksposter87.jpg
| caption        = Theatrical release poster
| alt            =
| director       = Steve Carver
| producer       = John C. Broderick Ahmet Yasa
| writer         = David Oas
| starring       = Scott Strader Perry Lang Mariska Hargitay Richard Roundtree
| music          = David McHugh
| cinematography = Adam Greenberg
| editing        = Tom Siiter Shady Acres
| distributor    = Crown International Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| awards         =
| language       = English
| budget         =
}} teen comedy film|comedy.  The film was directed by Steve Carver and written by Michael Lanahan and David Oas.  Las Vegas, Nevada and in Los Angeles, California. 

==Plot==
Coach Williams (  star Mariska Hargitay plays the role of the heroine, Nicole. 

  

==Cast==
* Scott Strader as The Kid
* Perry Lang as Jeff
* Mariska Hargitay as Nicole
* Richard Roundtree as Chip Williams
* R. G. Armstrong as Coach Bettlebom
* Stoney Jackson as  Andy
* Adam Mills as Tex
* Donald Gibb as Ripper
* Tom Shadyac as Chris Christopher Murphy as Tony
* Katherine Kelly Lang as Julie
* Christopher Lee as President White
* Trinidad Silva as Chito
* Marianne Gravatte Woman on the Jocks Poster

==Reception==
=== Box office ===
Jocks was released in 1987 but never received a wide release.  The film grossed only $120,808, making it one of the larger box office failures of 1987. {{cite web
| url = http://www.boxofficemojo.com/movies/?id=jocks.htm
| title = Jocks (1986)
| work = Box Office Mojo
| publisher = Amazon.com
}}
 

=== Critical response ===
 
For the most part, the film was either ignored or attacked by critics.  David Cornelius of DVD talk.com gave the film a negative review saying,  
The script is rambling and forgetful... its characters lack the very charm the movie is convinced its oozing, the tennis sequences are maddeningly dull, the romance is vacant. This is the kind of movie that thinks its a blast because it shows us college kids getting drunk and leering at women, not realizing that you need to put in these things called "jokes" to make such a premise work. 
  
 

Jocks has been seen by so few people that it has a "not available" rating on Rotten Tomatoes. {{cite web
| url = http://www.rottentomatoes.com/m/jocks/
| title = Jocks
| work = Rotten Tomatoes]
| publisher = Flixster
}}
 

== References ==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 