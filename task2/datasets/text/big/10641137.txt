Chhoti Bahu
 
{{Infobox film
| name           = Chotti Bahu
| image          = Choti_Bahu.jpg
| image_size     = 
| caption        = DVD cover
| director       = K. B. Tilak
| producer       = 
| writer         = 
| narrator       = 
| starring       = Sharmila Tagore Rajesh Khanna Nirupa Roy  music          = Kalyanji Anandji
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1971
| runtime        = 
| country        = India Hindi
| budget         = 
}}

Chhoti Bahu is a 1971 Hindi film. Produced by Seeru Daryani and Darius Gotla, the film is directed by K. B. Tilak. The film stars Sharmila Tagore, Rajesh Khanna, Nirupa Roy and I. S. Johar. The music is by Kalyanji Anandji. The film was successful at the box office.

It is a remake of 1956 Telugu film Muddu Bidda produced and directed by K. B. Tilak. 

The story is originally based on a Bengali novel "Bindur Chhele" written by the famous Bengali author Saratchandra Chatterjee.

==Cast==
*Sharmila Tagore	 ... 	Radha
*Rajesh Khanna	... 	Madhu
*Shashikala	... 	Paro
*Nirupa Roy	... 	Sita
*I. S. Johar	... 	Premnath (Nikus dad)
*Mehmood Jr.	... 	Niku
*Master Suraj		
*Tarun Bose	... 	Shriram
*Shivraj	... 	Radhas Father
*P. Jairaj	... 	Rajaram Ramprasad Bahadur
*Satyendra Kapoor	... 	Vedji Avtar
*Radhika Rani		
*Irshad Panjatan		
*Chandrima Bhaduri	... 	Ganga
*Master Ratan

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Are Ghar Ko Mat Godam Bana"
| Hemlata
|-
| 2
| "Dulhaniya Bata De Ri"
| Asha Bhosle, Usha Khanna
|-
| 3
| "He Re Kanhaiya"
| Kishore Kumar
|-
| 4
| "Munne Raja"
| Lata Mangeshkar
|-
| 5
| "O Maa Meri Maa"
| Lata Mangeshkar
|-
| 6
| "Ye Raat Hai Pyasi Pyasi"
| Mohammed Rafi
|}
==References==
 

== External links ==
*  

 
 
 
 
 
 

 