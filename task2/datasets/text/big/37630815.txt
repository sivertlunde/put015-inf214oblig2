Favorites of the Moon
{{Infobox film
| name = Les favoris de la lune Favorites of the Moon
| image = Les favoris de la lune.jpg
| director = Otar Iosseliani
| writer = {{plainlist|
*Otar Iosseliani
*Gérard Brach }}
| starring =
| music = Nicholas Zourabichvili 
| cinematography = Philippe Théaudière 
| editing =
| producer = Philippe Dussart 
| distributor =
| released =  
| runtime = 
| country = France 
| language = Italian
| budget =
}}
Les favoris de la lune (internationally released as Favorites of the Moon) is a 1984 French drama film written and directed by Otar Iosseliani. 

==Release==
The film was released in France in 1985.   

==Reception== Special Jury Prize. 

== Cast ==
*Katja Rupé: Claire
*Alix de Montaigu: Delphine Laplace
*François Michel: Philippe
*Jean-Pierre Beauviala: Colas
*Vincent Blanchet: terrorist
*Otar Iosseliani: terrorist

==References==
 

==External links==
* 

 
 
 
 
 


 