The Reunion (1963 film)
 
{{Infobox film
| name           = The Reunion
| image          = La_rimpatriata_1963.jpg
| caption        = Film poster
| director       = Damiano Damiani
| producer       = 
| writer         = Damiano Damiani Ugo Liberatore Vittoriano Petrilli Enrico Ribulsi
| starring       = Walter Chiari
| music          = 
| cinematography = Alessandro DEva
| editing        = Giuseppe Vari
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
The Reunion ( ) is a 1963 Italian comedy film directed by Damiano Damiani. It was entered into the 13th Berlin International Film Festival.   

==Cast==
* Walter Chiari as Cesarino
* Letícia Román as Carla
* Francisco Rabal as Alberto Riccardo Garrone as Sandrino
* Dominique Boschero as Tina (la triste)
* Mino Guerrini as Nino
* Paul Guers as Livio
* Gastone Moschin as Toro
* Jacqueline Pierreux as Lara (il Larone)
* Mimma Di Terlizzi as Maria

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 