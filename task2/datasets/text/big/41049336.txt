Ennamo Nadakkudhu
{{Infobox film
| name           = Ennamo Nadakkudhu
| image          = 
| caption        = 
| director       = Rajapandi
| producer       = Vinoth Kumar
| writer         = Rajapandi Prabhu Rahman Rahman
| music          = Premgi Amaren
| cinematography = A. Venkatesh (cinematographer)|A. Venkatesh
| editing        = Praveen K. L. N. B. Srikanth
| studio         = Triple V Records
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}} Tamil thriller Sukanya and Thambi Ramaiah.  Produced by Vijay Vasanths brother, V. Vinoth Kumar for Triple V Records,    it has music composed by Premgi Amaren and cinematography by A. Venkatesh (cinematographer)|A. Venkatesh, while the editing is taken care of by Praveen K. L. and N. B. Srikanth.    The story revolves around a young man in love, who gets caught between two influential people in the society.   

==Cast==
* Vijay Vasanth as Vijay
* Mahima Nambiar as Madhu Prabhu as Parthipan Rahman as Burma
* Thambi Ramaiah
* Saranya Ponvannan Sukanya
* Azhagam Perumal

==Production== Rahman plays Prabhu plays a businessman in the film.  Saranya Ponvannan was cast as the heros mother and she also sung a folk number for the film.  Vijay Vasanth trained with Koothu-P-Pattarai and also learned certain kinds of fights for his role. 

The shooting started on December 12, 2012 and the first look poster was released on the same day.  The climax action sequences were being shot in an unopened shopping mall in Chennai in March 2013. 

==Soundtrack==
The music was composed by Premgi Amaren. The audio launch of Ennamo Nadakkudhu happened in Chennai on December 16, 2013. 

* Aagayam Vizhigiradhe - Premgi Amaren
* Meesa Kokku - Vijay Yesudas, Saindhavi, Saranya Ponvannan
* Money Money - Ranjith (singer)|Ranjith, Premgi Amaran
* Orakkanna - Haricharan
* Vaa Idhu Nethiyadi - Mano (singer)|Mano, L. R. Eswari

==Critical reception==
The film received overall positive reviews. The Times of India gave 3.5 stars out of 5 and wrote, "There is a lot to like in Yennamo Nadakkudhu and first and foremost among these is the script. The film is tautly written with one scene leading into another or informing the other so that there is hardly a wasted moment. Generally, thrillers tend to lose some bit of the tension after the revelation but this film manages to keep you on the edge of the seat till its climax and it is certainly its biggest triumph. Yennamo Nadakkudhu is the surprise of the season".  Sify wrote, "Ennamo Nadakkuthu is a racy edge of the seat thriller with lots of twists and turns, till the very end. The film works due to its speed, earthy dialogues, performances from its actors and a cracker of a climax".  The New Indian Express wrote, "With an intriguing screenplay and with enough twists and turns to keep up the momentum, the film is a racy, riveting action-suspense thriller. Ennamo Nadakkuthu is an impressive work from a debutant maker".  Behindwoods.com gave 2.5 stars and wrote, "The ups, downs, twists and turns in the script make the movie interesting to watch and takes it to a pacy finale that might make the audience forget that their seats have push backs. The director has placed some of the leads pretty neatly in the initial portions of the script and the unwinding of those leads in the climax sequence shout out the subtle brilliance in the scripting. Although the movie has a few pinches of cinematic elements here and there, the perfection in the script and the flawlessness in the execution allows the movie to take a step up".  Indiaglitz.com wrote, "Ennamo Nadakkudhu is a good relief from usual masala with a practical concept told with a high quality of cinematic touch, and makes for a good watch". 

The Hindu was more critical of the film, writing, "it would have been all very well had Vijay   used just brains and got the job done, the plot might have worked and made it a neat film. But the minute we see him jumping off buildings and dodging goondas in hot pursuit, you are tempted to ask — Oh really?" 

==References==
 

==External links==
* 

 
 
 
 
 
 