Flying Fish (film)
{{Infobox film
| name           = Flying Fish
| image          = FLYING FISH -Teaser Poster-5.jpg
| alt            =  
| caption        = 
| director       = Sanjeewa Pushpakumara
| producer       = Manohan Nanyakkara & Sanjeewa Pushpakumara
| screenplay     = Sanjeewa Pushpakumara & Chinthana Dharamadasa
| story          = Sanjeewa Pushpakumara
| starring       = Kaushalya Fernando Rathnayaka Marasinghe Chaminda Sampath Jayaweera Gayesha Perera Siththi Mariyam Sanjeewa Dissanayake Sumathy Sivamohan Nilanka Dahanayake Thissa Bandaranayaka Wasanthy Ranwala Mohammed Ali Rajabdeen
| music          = Tharindu Priyankara de Silva
| cinematography = Vishwajith Karunarathna
| editing        = Ajith Ramanayake
| studio         = 
| distributor    = 
| released       =  
| runtime        = 119 minutes
| country        = Sri Lanka
| language       = Sinhala and Tamil
| budget         = 
| gross          = 
}} Hubert Bals Fund of the International Film Festival Rotterdam (IFFR).   The film made its world premiere on January 28, 2011, as part of the Rotterdam festivals Tiger Awards Competition.Film has been noted for its political value, beautiful cinematography, long takes, and shocking violence

The film draws on stories from the directors life in his hometown of Trincomalee, Sri Lanka, where Flying Fish was shot. 

==Synopsis==
The film weaves together three narratives set against the backdrop of the Sri Lankan Civil War.

===Wasana and Soldier=== Sinhalese village civil defense force to protect the village border. Among these recruits is her father, who is punished and humiliated by the soldiers from her lovers platoon for not being on guard one day. Demoralized, he shoots himself inside an empty bunker. Meanwhile, the soldier and his platoon receive a transfer to a distant posting, leaving the woman in great agony. Tortured by her fathers suicide and her rage at the soldier who leaves her pregnant, she flees the village.

===Son and Mother=== government army and the Liberation Tigers of Tamil Eelam|L.T.T.E., or Tamil Tigers escalates. Stricken with extreme poverty, she becomes a curd vendor. She also becomes involved in an affair with a young man who owns a shop in the village. Her eldest son is in his first year of high school. To support the family, he works in the local fish market selling fish. Meanwhile, he falls in love with a girl at his school. He is humiliated when their relationship is revealed to the school. Meanwhile, rumors spread about his mother’s affair. One day, the boy sees his mother having sexual intercourse with her lover in a broken house. The enraged boy stabs his mother in front of his siblings the same evening.

===Tamil Girl=== Tamil schoolgirl Eastern Sri Lanka, where the war between the state army and the L.T.T.E. has been intensifying. The Tamil Tigers secretly conduct their propaganda lectures in schools, demanding a separate Tamil Eelam state and justifying their war against the Sinhala-dominated southern government army. One night, the L.T.T.E. forcibly enter the Tamil girl’s house and demand a sum that the family can scarcely afford. They threaten to conscript the girl if her family fails to pay the money. On the night that her parents are supposed to pay the demanded money, the child slips away from the house just as the L.T.T.E visit them. Since the L.T.T.E. get neither the money nor the girl, they gun down her parents.

==Reception and controversy ==
 Ian Christie, to name some, below are excerpts from their reviews:

* "The film Is scrupulously non-partisan, deeply humane, sexually candid, coolly modernist in style and almost indecently beautiful." —Tony Rayns 
* "...stories filled with rage,suffering, sexual abandon and calamity, but the director films them with cool detachment... A work of remarkable restraint. Sri Lankan Cinema has found its true modernist" —Tony Rayns  Ian Christie 

Film banned by Sri Lankan Government in Sri Lanka and internationally

The film made its Sri Lankan premiere in Colombo on July 11, 2013, despite having been initially released in 2011.  The film sparked immediate controversy, leading the government to ban it.  The government claims that the film “insults the security forces” and that it illegally used images of the Sri Lankan military uniform, both of which Pushpakumara denies.  According to BBC and Associated Press reports, the Sri Lankan police are currently conducting a “fact-finding investigation” into the film and have questioned its cast and production crew.  Local Sinhala news sources reported that even Pushpakumara’s mother, who prepared meals for the cast and crew during the filming, has been questioned by authorities.  Sri Lankas Free Media Movement has been variously cited as criticizing both the censorship of the film and this investigation, stating that they represented the government’s desire to “militarise arts and culture.” There is, however, no trace of such a statement emanating from the organization.

==Awards==

* Best Director Award, New Territories Competition     /  St.Pertersburg International Film Festival- KINOFORUM 2011 
*  Best Asian Film, NETPAC award, 4th Bengaluru International Film Festival
* Blue Chameleon Award, 5th CinDi IFF aka Cinema Digital International Film Festival,Soul, South Korea
* Best Asian Cinematographer 2011(Nominated), Asian Film Awards, Hong Kong
* Special Jury Mention for Red Chameleon Award, 5th CinDi IFF aka Cinema Digital International Film Festival, Seoul, South Korea
* Critic’s Choice Award, 5th New Jersey South Asian Film Festival, USA
* Tiger Award (Nominated) / 40th International Film Festival Rotterdam Bryce J. Renninger,  , indieWIRE, January 10, 2011  Todd Brown,  , Twitch, January 9, 2011 
* Golden Montgolfier Award (Nominated),34th 3 continents IFF
* New Directors Award (Nominated) / 37th Seattle International Film Festival  Pesaro International Film Festival (Mostra Internazionale de Nuova Cinema) 
* Silesian Film Award (Nominated) / Ars Independent International Film Festival 2011 Katowice 
*  Best Film (Nominated)  Tokyo FILMeX IFF
* Fipresci Award (Nominated) 36th Hong Kong IFF
* SIGNIS Award (Nominated) 36th Hong Kong IFF
* Asia Pacific Screening Awards (In Competition)

==Festivals==

* 40th Rotterdam International Film Festival – the Netherlands 26 January-6 February 2011
* 37th Seattle IFF, Seattle, USA – May 19 – June 12, 2011.
* 1st Ars International Film Festival Katowich, Poland, June 15–19, 2011
* 47th Pesaro IFF, Italy – June 19–27, 2011
* Museum of Modern Art (aka MoMA) – New York, USA – July 7–13, 2011 – Under Contemporary Asian Cinema Series.
* CINEMATEK (The Belgian Royal Film archive)- Brussels, July 1–14, 2011 for L’Age d’or prize and the Cinédécouvertes Prizes
* 2nd St. Petersburg IFF (aka Kino Forum),St. Petersburg, Russia – July 10–15, 2011.
* 5th CinDi IFF aka Cinema Digital International Film Festival, August 17 to 23,2011,Soul, South Korea
* 11th Indie World Film Festival, September September 1–29, 2011, Brazil.
* 30th Vancouver International Film Festival – September 29 – October 14, 2011, Canada
* London Film Festival (LBF) – October 13–28, 2011, London
* Hawaii International Film Festival, October 13–23, 2011, USA.
* 47th Chicago IFF, Chicago, USA – October 6–20, 2011 – World Cinema.
* New Jersey South Asian Film Festival, October 21–23, 2011, USA
* 52nd Thessaloniki International Film Festival, November 4–13, 2011, New Horizon section, Greece
* 3rd i San Francisco South Asian Film Festival, November 9–13, 2011, USA.
* 15th Tallinn Black Nights Film Festival, November 16 to 30, 2011,Estonia
* 12th TOKYO FILMeX IFF, November 19–27, 2011, Tokyo, Japan.
* 34th 3continents IFF, November 22–29, 2011 Nantes France.
* 42nd International Film Festival – Goa, November 23 – December 3, 2011, India
* 4th Bengaluru International Film Festival,15–22 December 2011, Bangalore, (Asian Competition)
* 10th Pune International Film Festival 12–19 January 2012, Pune, India
* 36th Hong Kong IFF,Hong Kong, March 21 – April 5, 2012
* 18th The Prague International Film Festival FEBIOFEST, 24 March-1 April 2012, Czech Republic
* 2nd Indian Film Festival, 11–22 June 2012, Melbourne, Australia
* 5th Samsung Women International Film Festival, 14–21 July 2012,India
* Annual South Asia conference in Madison, 17–20 October 2012, Wisconsin.
* La Ciematheque ( Special screening), 1 January 2013
* Australian Cinematheque  – Change : Path Through 20 Years of Film, 8 March 2013, Queensland
* 16th Guanajuato International Film Festival, Mexico,2013

==References==
 

==External links==
* 
* 
*  
*  
*  

===Interview with director===
*  

===Reviews===

* Meredith Brody,  , Thompson on Hollywood, June 11, 2011
*  , Seattle Times, June 13, 2011
* David Morgan and Kevin Cassidy,  , Hollywood Reporter, June 1, 2011
* Ronald Glasbergen,  , Rotterdam Vandaag & Morgen, January 30, 2011
* Ronald Glasbergen,  , Rotterdam Vandaag & Morgen, February 5, 2011
*  , The Stranger
* Jay Weissberg,  , Variety, February 6, 2011
* Michael Hale,  , The New York Times, July 7, 2011
* Benjamin Mercer,  , The Village Voice, July 7, 2011

===Film festivals and screenings===
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 