Rockabye (1932 film)
{{Infobox film
| name           = Rockabye
| image          = RockabyeDVD.JPG
| image_size     =
| caption        = Cover of the DVD released in Spain
| director       = George Cukor
| producer       = David O. Selznick
| writer         = Jane Murfin Based on a play by Lucia Bronder
| narrator       =
| starring       = Constance Bennett Joel McCrea
| music          = Harry Akst Jeanne Borlini Nacio Herb Brown Edward Eliscu
| cinematography = Charles Rosher
| editing        = George Hively
| studio         = RKO Pathé
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 1932 Cinema American drama film directed by George Cukor. The screenplay by Jane Murfin is based on a play by Lucia Bronder.

==Plot== stage actress embezzler Al Broadway production.
 Second Avenue, until she confesses she was raised there herself. The two hit it off and Judy convinces Tony to produce the play. On the verge of divorce, Jake proposes he and Judy wed as soon as he is free.

Jake fails to appear at the opening night party for Rockabye, and his mother tells Judy her daughter-in-law has just had a baby and asks her to forget her son. When Jake finally arrives and assures her he still wants to marry her, Judy insists he return to his wife and newborn child. Devastated, she is comforted by Tony, who finally reveals his feelings for her.

==Production==
RKO purchased the rights to the play from Gloria Swanson and hired George Fitzmaurice to direct the film adaptation. Anxious to accommodate exhibitors who were awaiting a new Constance Bennett film, the studio rushed the script into production with Phillips Holmes as the male lead. When the completed film was shown to executives, they declared it unreleasable and called in George Cukor to salvage it. The new director replaced Holmes with Joel McCrea and Laura Hope Crews, in the role of Judys mother, with Jobyna Howland, reshot all their characters scenes, and re-edited the balance of the film.  

==Cast==
*Constance Bennett ..... Judy Carroll 
*Joel McCrea ..... Jacob Van Riker Pell 
*Paul Lukas ..... Tony de Sola 
*Walter Pidgeon ..... Al Howard 
*Jobyna Howland ..... Snooks Carroll

==Critical reception==
The New York Times observed, "There are tears enough in Rockabye to drown a plot, a circumstance which is a form of mercy in the case of this particular plot . . . As for the performance of Miss Bennett, a conservative opinion would be that she is a better actress than Rockabye makes her seem . . . Joel McCrea as the young playwright is better than the lines he has to recite."   

==References==
 

==External links==
 
* 

 
 

 
 
 
 
 
 
 
 
 