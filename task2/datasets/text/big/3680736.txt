Born to Dance
 
{{Infobox film
| name           = Born to Dance
| image          = Born to Dance - 1936- Poster.png
| image_size     = 
| caption        = 1936 theatrical poster
| director       = Roy Del Ruth Jack Cummings
| writer         = Jack McGowan Sid Silvers
| narrator       = 
| starring       = Eleanor Powell James Stewart
| music          = Cole Porter
| cinematography = Ray June
| editing        = Blanche Sewell
| distributor    = Metro-Goldwyn-Mayer
| released       = November 27, 1936
| runtime        = 106 min.
| country        = United States English
| budget         = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 American musical film starring Eleanor Powell and James Stewart, released by Metro-Goldwyn-Mayer, and directed by Roy Del Ruth. The plot of Born to Dance is not much different from the earlier film, or many others of the era—boy meets girl, boy and girl fall in love, girl puts on a spectacular song-and-dance show.The score is by Cole Porter .

==Production==
The film stars dancer Eleanor Powell and was a follow-up to her successful debut in Broadway Melody of 1936. The film co-stars James Stewart as Powells love interest and Virginia Bruce as the films resident femme fatale and Powells rival. Powells Broadway Melody co-stars Buddy Ebsen and Frances Langford return to provide comedy and musical support. The score was composed by Cole Porter.
 naval backdrop, tap dancing by Powell culminating in a patriotic salute, and finally a blast of cannon fire. This finale was also lifted in its entirety and re-used in another Powell film, I Dood It, co-starring Red Skelton. Although considered one of Powells (and MGMs) most memorable musical numbers, and often featured in retrospectives such as Thats Entertainment!, musical director Roger Edens was often quoted as being embarrassed by the segment.

The film introduced the Porter standards "Youd Be So Easy to Love" (performed by Stewart and Marjorie Lane, dubbed for Powell) and "Ive Got You Under My Skin" (performed by Bruce), which was nominated for the Academy Award for Best Song. It was the first film in which Stewart sang.

Some of the musical numbers were recorded in stereophonic sound, making this one of the first films to utilize multi-channel technology.  Rhino Records included the stereo tracks in its soundtrack album, released on CD, including Jimmy Stewarts and Marjorie Lanes performance of "Youd Be So Easy to Love." 

==Cast==
* Eleanor Powell as Nora Paige
* James Stewart as Ted Barker
* Virginia Bruce as Lucy James
* Una Merkel as Jenny Saks
* Sid Silvers as Gunny Sacks
* Frances Langford as Peppy Turner
* Raymond Walburn as Captain Percival Dingby
* Alan Dinehart as James Mac McKay
* Buddy Ebsen as Mush Tracy
* Juanita Quigley as Sally Saks
* Georges as Himself, Dance Speciality (as Georges and Jalna)
* Jalna as Herself, Dance Specialty (as Georges and Jalna)
* Reginald Gardiner as Central Park Policeman
* Barnett Parker as Model Home Demonstrator
* The Foursome as Sailor Quartette

==Soundtrack==
* Rolling Home (1936)
** Music and Lyrics by Cole Porter
** Sung by The Foursome, Sid Silvers, Buddy Ebsen, James Stewart and chorus

* Rap, Tap on Wood (1936) (Also called "Rap-Tap on Wood")
** Music and Lyrics by Cole Porter
** Sung and danced by Eleanor Powell and The Foursome; Eleanor Powells vocals dubbed by Marjorie Lane
** Also danced by Eleanor Powell at a rehearsal

* Hey, Babe, Hey (1936)
** Music and Lyrics by Cole Porter
** Sung and danced by Eleanor Powell, James Stewart, Sid Silvers, Una Merkel, Frances Langford, Buddy Ebsen and The Foursome; Eleanor Powells vocals dubbed by Marjorie Lane
** Hummed by Una Merkel
** Played also as background music

* Entrance of Lucy James (1936)
** Music and Lyrics by Cole Porter
** Sung by Raymond Walburn, Virginia Bruce and chorus

* Love Me, Love My Pekinese (1936)
** Music and Lyrics by Cole Porter
** Sung by Virginia Bruce and chorus
** Danced by Eleanor Powell

* Easy to Love (1936)
** Music and Lyrics by Cole Porter
** Played during the opening credits and as background music
** Sung by Eleanor Powell and James Stewart, Frances Langford and danced by her and Buddy Ebsen
** Reprised by the cast at the end
** Eleanor Powells vocals dubbed by Marjorie Lane

* Ive Got You Under My Skin (1936)
** Music and Lyrics by Cole Porter
** Danced by Georges and Jalna
** Sung by Virginia Bruce
** Played also as background music

* Swingin the Jinx Away (1936); (Also called "Swinging the Jinx Away")
** Music and Lyrics by Cole Porter
** Played during the opening credits
** Sung by Frances Langford, Buddy Ebsen, The Foursome and male chorus
** Danced by Buddy Ebsen and Eleanor Powell

* Sidewalks of New York (1894)
** Music by Charles Lawlor
** Lyrics by James W. Blake
** In the score during the "Rolling Home" number

* Columbia, the Gem of the Ocean (1843)
** Written by David T. Shaw
** Arranged by Thomas A. Beckett
** In the score during the "Rolling Home" number; Also in the score during the "Swingin the Jinx Away" number and partially sung by the chorus

* The Prisoners Song (If I Had the Wings of an Angel) (1924)
** Music and Lyrics by Guy Massey
** In the score when Gunny Saks is shown in the brig 

==References==
 

==External links==
*  
*  
*  

==Other uses== Born to British made-for-television film telecast in 1988.

 

 
 
 
 
 
 
 
 
 