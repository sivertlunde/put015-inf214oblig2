Prahalada (1939 film)
{{Infobox film
| name           = Prahalada பிரகலாதா
| image          = Prahalada 1939.jpg
| image size     = 200px
| caption        = A scene from Prahalada
| director       = B. N. Rao
| producer       = Salem Sankar
| writer         = T. C. Vadivelu Naicker
| narrator       =
| starring       =  
| music          = Sharma Brothers
| cinematography =  
| editing        =
| distributor    = Salem Sankar Films
| released       = 12 December 1939
| runtime        =
| country        = India
| language       = Tamil
| budget         =
| preceded by    =
| followed by    =
}} Tamil film Bengali and the story of Narasimha and Prahlada. It features M. G. Ramachandran as Lord Indra   This was the sixth film of Ramachandran who later became one of the popular actors of the Tamil film industry. The film also featured a sword fight sequence between M.G. Ramachandran and Santhanalakshmi. 

==Plot==
The story is from a short episode in the Vishnu Purana, a holy text of Vaishnavites, that narrates the story of Prahlada, an ardent devotee of Lord Vishnu much against the wish of his father Hiranyakashipu, a demon. All of Hiranyakashipus attempts to change his sons attitude are in vain. Finally, when he decides to kill his son, Lord Vishnu comes to the rescue of the son, in the form of Narasimha (a man-lion form), and kills the king.

==Cast and crew==
* T. R. Mahalingam (actor)|T. R. Mahalingam as Prahlada
* M. R. Santhanalakshmi as Kayadhu
* R. Balasubramaniam as Hiranyakashipu
* Nagercoil K. Mahadevan as Sage Narada
* M. G. Ramachandran as Lord Indra
* N. S. Krishnan
* T. A. Mathuram
* T. S. Durairaj
* P. S. Gnanam

==Production==
The story and the dialogues of the Tamil film were closely followed for the Malayalam version. The script and dialogues of the Malayalam version was by N. P. Chellappan Nair.     The film was an average success at the box office. 

==Inspiration and remakes== Bengali and Assamese language|Assamese. It is also the only story which has been made so many times, often with box-office success. 

==References==
 

==External links==
*  

 
 
 
 
 