Voyage to the Planet of Prehistoric Women
 
  

{{Infobox film
| name           = Voyage to the Planet of Prehistoric Women
| image          = Voyageplanetprehistoricwomen.jpg
| image_size     =
| caption        = DVD cover
| director       = Peter Bogdanovich (as Derek Thomas)
| producer       = Norman D. Wells Roger Corman
| writer         = Henry Ney
| starring       = Mamie Van Doren Mary Marr Paige Lee Irene Orton
| music          = Keith Benjamin
| cinematography = Flemming Olsen
| editing        = Bob Collins
| distributor    = American-International Television Filmgroup
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
}}
Voyage to the Planet of Prehistoric Women is a 1968 science fiction film directed by Peter Bogdanovich. The film is an adapted version of Curtis Harringtons Voyage to the Prehistoric Planet, which in turn is adapted from the Russian 1962 feature Planeta Bur by Pavel Klushantsev. No footage from Planeta Bur appears in Voyage to the Planet of Prehistoric Women that did not appear in Voyage to the Prehistoric Planet, and the dubbing is the same.  In the United States, this film is in the public domain.

==Plot==
Astronauts landing on Venus encounter dangerous creatures and almost meet sexy Venusian women. The astronauts kill a creature that is worshiped by the Venusian women who then attempt to use their powers of nature to kill the astronauts but fail. At last, the astronauts leave the planet and their robot that was burnt by the volcano fire is placed as a god by the Venusian women who have already destroyed the statue of their previous god (a bird).

==Cast==
* Mamie Van Doren as Moana
* Mary Marr as Verba
* Paige Lee as Twyla
* Gennadi Vernov as Astronaut Andre Freneau
* Margot Hartman as Mayaway
* Irene Orton as Meriama
* Pam Helton as Wearie
* Frankie Smith as Woman of Venus

==Production==
The movie was known as Gill Men at one stage. It was the last film made by the Filmgroup company. Ray, Fred Olen. The New Poverty Row: Independent Filmmakers as Distributors, McFarland, 1991, p 56-58.  Bogdanovich:
 It was a Russian science-fiction film that Roger had called Storm Clouds Of Venus that he had dubbed into English. And he came to me and said, "Would you shoot some footage with some women? AIP wont buy it unless we stick some women in it." So I figured out a way to work some women in it and shot for five days, and we cut it in. I narrated it, because nobody could make heads or tails of it. Roger wouldnt let me add any sound. It was just a little cheap thing we did, and people think I directed it when I really only directed 10 minutes of it.  
Bogdanovich said he had to paint out the red star on the spaceship, "in every frame. We painted in some obscure symbol that might pass for the National Aeronautics and Space Administration." Diehl, D. (1972, Apr 02). Q & A PETER BOGDANOVICH. Los Angeles Times (1923-Current File). Retrieved from http://search.proquest.com/docview/156995779?accountid=13902   

Bogdanovich hired Mamie Van Doren and several other blondes to play Venusians "because I thought everyone should be blonde on Venus. I dressed them up in rubber suits, bottoms only and put shells over their breasts. I had them traipsing around Leo Carillo Beach for a while shooting inserts that might relate to Venus".  Bogdanovich says he gave the girl characters "South Sea movie names" because "it seemed right".  

One of the girls was afraid of sharks and when she was in the water they threw her a rubber fish; she got hysterical, grabbed the fish and bit its head off. He said that people did not understand the film and its new sequences when first cut together so he added narration. He decided one of the astronauts, "the best looking one", should narrate the film. Bogdanovich wrote the narration and did the voice himself; it was the one credit he took on the film.  

Bogdanovich did not claim credit as director because "such a small piece of it is mine."  His then-wife Polly Platt worked on the film as a production designer.

==See also==
* List of films in the public domain in the United States

==References==
 

==External links==
*  
*  
*  
*   (public domain)

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 