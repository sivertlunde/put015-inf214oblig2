Tokyo Fist
{{Infobox film
  | name = Tokyo Fist
  | caption = Tokyo Fist poster
  | image	=	Tokyo Fist FilmPoster.jpeg
  | director = Shinya Tsukamoto
  | producer = Shinya Tsukamoto
  | writer = Hisashi Saito Shinya Tsukamoto
  | starring = Kahori Fujii Shinya Tsukamoto Kôji Tsukamoto
  | music = Chu Ishikawa
  | cinematography =
  | editing =
  | distributor = Theres Enterprise Inc.
  | released = 1995
  | runtime = 87 min. Japanese
  | budget = 
  }}
Tokyo Fist (1995 in film|1995) is a Japanese drama film|drama/horror film. It was written and directed by Shinya Tsukamoto, who also stars in the film along with his brother Kôji Tsukamoto and Kahori Fujii. The horror aspect of the film comes mostly from some brutal and exaggerated imagery showing the consequences of the films violence.

Like so many other films by Tsukamoto, the music for the film was composed by Japanese industrial music band Der Eisenrost.

==Plot==

The film tells about a Japanese door-to-door insurance salesman, Tsuda Yoshiharu, who takes up boxing after some chance meetings with a former high school friend, Kojima Yakuji. Tsuda lives a more or less uneventful life.  He has a fiance, Hizuru, who one day invites Kojima in to Tsudas apartment.  Kojima comes onto Hizuru, who rejects him.  Still, Tsuda finds out and gets enraged at Kojima, but Kojima beats Tsuda to a pulp and humiliates him to the point where it strains his relationship with Hizuru.  Hizuru is intrigued by the animalistic Kojima, and ends up moving in with him.  She also starts to pierce herself and get tattoos.  She wants to box, but is denied that life by the surprisingly cowardly Kojima, who says she is a scary freak of a woman.

Tsuda still has feelings for Hizuru, and he keeps trying to win her back, leading to a confrontation where they bond by beating each others faces to a pulp (Tsuda is quite mutilated in the process). In the end, Kojima and Tsuda end up in a sparring match in their boxing club, which leads to Tsuda getting beaten again, while Kojima goes on to win a real match, with no regard to his own well-being, afterwards.  Kojima wins the match, but both he and Tsudas faces begin to break apart and bleed profusely, suggesting fatal wounds.

==Cast==
*Kahori Fujii - Hizuru
*Shinya Tsukamoto - Tsuda Yoshiharu
*Kôji Tsukamoto - Kojima Takuji
*Naomasa Musaka - Hase, trainer Naoto Takenaka - Ohizumi, trainer
*Koichi Wajima - Shirota, gym owner
*Tomorowo Taguchi - Tattoo Master
*Nobu Kanaoka - Nurse

==External links==
* 
*  
*  

 

 
 
 
 
 
 


 