Now and Forever (1956 film)
{{Infobox film
| name           = Now and Forever
| image          = "Now_and_Forever"_(1956).jpg
| caption        = 
| director       = Mario Zampi
| producer       = Mario Zampi
| writer         = R.F. Delderfield Michael Pertwee
| based on       = a play "The Orchard Walls" by R.F. Delderfield 
| starring       = Janette Scott
| music          = Stanley Black
| cinematography = Erwin Hillier Richard Best
| studio         = Mario Zampi Productions (as Anglofilm)
| distributor    = Associated British-Pathé  (UK)
| released       = 21 February 1956	(London)  (UK)
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = £157,111 
}} British drama child star in Britain.   An upper class girl becomes romantically involved with a poor boy, and they elope together to Gretna Green.

==Cast==
* Janette Scott - Janette Grant
* Vernon Gray - Mike Pritchard
* Kay Walsh - Miss Muir
* Jack Warner - Mr. J. Pritchard Pamela Brown - Mrs. Grant
* Charles Victor - Farmer Gilbert
* Marjorie Rhodes - Aggie, the Farmers wife
* Ronald Squire - Waiter Wilfrid Lawson - Gossage
* Sonia Dresdel - Miss Fox
* David Kossoff - Pawnbroker
* Moultrie Kelsall - Doctor
* Guy Middleton - Hector
* Michael Pertwee - Reporter
* Henry Hewitt - Jeweller
* Bryan Forbes - Frisby
* Arthur Lowe - Vicar
* Jean Patterson - Rachel Harold Goodwin - Lorry driver

==Critical reception==
Allmovie called the film "a very slight piece, buoyed by the charm and attractiveness of its young stars. Janette Scott and Vernon Grey...Though the film seems flat and obvious when viewed on television, it truly comes to life before a large and appreciative moviehouse audience. Forgotten for many years, Now and Forever was happily rediscovered by the late film historian William K. Everson in his 1979 book Love in the Film, which was dedicated to star Janette Scott."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 