Belinsky (film)
{{Infobox film
| name           = Belinsky
| image          = 
| image_size     = 
| caption        = 
| director       = Grigori Kozintsev
| producer       = 
| writer         = Grigori Kozintsev Yury German
| narrator       = 
| starring       = Sergei Kurilov
| music          = Dmitri Shostakovich Sergei Ivanov Mark Magidson	 	
| editing        = 
| distributor    = 
| studio         = Lenfilm
| released       = 4 June 1953
| runtime        = 102 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}}
 Soviet film directed by Grigori Kozintsev, based on the life of Russian literary critic Vissarion Belinsky (1811&ndash;1848). The production of the film was completed in 1951 but it was not released until 1953, following the reshooting of various scenes demanded by Stalin .

==Cast==
* Sergei Kurilov - Vissarion Belinsky Aleksandr Borisov - Alexander Herzen
* Georgy Vitsin - Nikolai Gogol
* Yuri Lyubimov - Frolov
* Yuri Tolubeyev - Mikhail Shchepkin Nicholas I

==External links==
* 

 
 

 
 
 
 
 
 

 