Lovely Rita (film)
{{Infobox film
| name           = Lovely Rita
| image          = LovelyRita01.jpg
| caption        = Film poster
| director       = Jessica Hausner
| producer       = Barbara Albert
| writer         = Jessica Hausner
| starring       = Barbara Osika
| music          = 
| cinematography = Martin Gschlacht
| editing        = Karin Hartusch
| distributor    = 
| released       =  
| runtime        = 79 minutes
| country        = Austria
| language       = German
| budget         = 
}}

Lovely Rita is a 2001 Austrian drama film directed by Jessica Hausner. It was screened in the Un Certain Regard section at the 2001 Cannes Film Festival.   

==Cast==
* Barbara Osika - Rita
* Christoph Bauer - Fexi
* Peter Fiala - Bus driver
* Wolfgang Kostal - Ritas father
* Karina Brandlmayer - Ritas mother
* Gabriele Wurm Bauer - Fexis mother
* Harald Urban - Fexis father
* Felix Eisier - Fexis brother
* Agnes Napieralska - Ritas sister
* Rene Wanko - Ritas brother-in-law
* Marcia Knoppel - Ritas niece
* Ursula Pucher - Teacher
* Lili Schageri - Alex
* Frau Urban - Alexs mother
* Bettina Grossinger - Colleague

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 