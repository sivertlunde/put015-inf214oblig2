Six Million and One
{{Infobox film 
| name           = Six Million and One 200px
| caption        = film poster David Fisher
| sound          = Amos Zipori  David Fisher  Irit Shimrat   Fisher Features Ltd. David Fisher  Gideon Fisher  Ronel Fisher  
| music          = Ran Bagno 
| cinematography = Ronen Mayo  Claudio Steinberg  Ronen Schechner
| editing        = Hadas Ayalon
| release date   = 2011
| runtime        = 93 minutes English  German  English subtitles
}} David Fisher. This is the third and final film in the family trilogy created by Fisher after Love Inventory (2000) and Mostar Round-Trip (2011).

==Plot summary==
   ) during the making of Six Million And One. Photo taken by Irit Shimrat]] KZ Gusen 71st Infantry Division   who liberated the camp.  The elderly soldiers are still haunted and traumatized by the horrific sights they came across when entering the camp.   Through their journey, the Fishers become emblematic of the entire second generation who are still grappling with the experience of their survivor parents.

Directors statement: "For me, This isnt a film about the Holocaust, because we (Me and my siblings) spent most of our time laughing and there is nothing funny about the Holocaust; Its about a rare kind of intimacy and brotherly bond that replaced pain with bitter-sweet humor."
 

== Production ==
The film was shot on  , the  , the   and  , Austria.

The film was theatrically released in Israel by   theaters, in Austria by   and in the USA by   premiering at   NYC  and broadcast on BBC Four. 

Additional production credits: 
 Noit Geva - Artistic Consultant.
 Amos Zipori and Frank Kubitsky - Sound Recordists.
 Gil Toren - Sound Designer.
 Alon Feuerstein - Graphic Designer.
 Aharon Peer - Colorist.

==Special events==
The film is regularly shown at special events, seminars and advanced studies that concern such topics as Holocaust diaries, memory and trauma and the second generation to Holocaust survivors.

Screenings at:  
The British Conference for Jewish Studies at the University of Kent in Canterbury UK July 7, 2013.  
The American Psychoanalytic Association in NYC January 16, 2014.  
The United Nations at Vienna during The International Holocaust Memorial Day - January 27, 2014.  
The Cleveland Museum of Art March 12, 2014.  
The "The Walk Of Life" march in Gusen, Austria April 5, 2014.   
The 167 annual International American Psychiatric Association Conference NYC May 4, 2014.

== Festivals and awards ==
The film was selected and screened at the following film festivals:
* Haifa International Film Festival 2011 - World premiere. 
* International Documentary Film Festival Amsterdam (IDFA) 2011 - Feature length competition top 10 in the Audience Choices.  
 
 
"A great film, really emotional, open and wonderful interaction. I am recommending it on tonights IDFA talkshow."
- International Producer Peter Wintonick IDFA programming advisor and Media Talks team, November 17, 2011
 
 

* Crossing Europe Film Festival, Linz Austria 2012 - Opening film. 
* DOK.fest International Documentary Film Festival Munich 2012 - won Best Documentary Award. 
 
 
"A remarkable film about one of the biggest tragedies in history: In a compelling manner, the film maker, David Fisher, manages to lucidly and efficiently combine an emotional family story with elements of universal questions in a surprisingly light and charming way – taking us all on a memorable and touching journey."
-  Jury citation for Best Documentary Award, 2012
 
 

* Kraków Film Festival 2012 - won Silver Horn for the Director of the Best Feature Length Documentary .  
* Doxa Film Festival Vancouver 2012 - Honorable Mention. 
* Muestra Film Festival Bogotá, Columbia 2012. 
* Minsk International Film Festival 2012. 
* Exground Film Fest Wiesbaden, Germany 2012. 
* Seattle International Film Festival 2012.  
* Louisville International Film Festival (LIFF) 2012.  
* Cork Film Festival, Ireland 2012.  
* Macon Film Festival, Georgia USA 2013. 
* Jewish and Israeli film festivals: San Francisco 2012,  Berlin 2012 - won Best German film with Jewish content award,  Toronto 2012,  Rome and Milan 2012,  Palm Beach 2012,  Boston,  Philadelphia,  Virginia,  Pittsburgh,  Minneapolis,  Melbourne,  Hong Kong,  London. 
In 2012 the film won the Israeli Ministry of Culture and Sport award  for works in the field of Zionism.

== Critical review ==
Rotten Tomatoes gives the film a score of 90% based on 10 reviews.  
Metacritic gives the film an average score of 52 out of 100, based on 5 reviews. 
*  
* 
*  
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

==References==
 

== External links ==
*  
*   at IMDB
*   at Rotten Tomatoes
*   at Israeli Film Academy
*   at Vimeo
*   on the IDFA daily
*   on Encino Tarzana Patch
*   at Fisher Features
*   at The New Fund for Cinema and Television (NFCT)

 

 
 
 
 
 
 