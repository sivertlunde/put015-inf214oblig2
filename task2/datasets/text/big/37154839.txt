Banaz a Love Story
{{Infobox film
| name = Banaz A Love Story
| image =Final official poster of Banaz A Love Story.jpg
| alt = Banaz A Love Story
| caption = 
| director = Deeyah Khan
| producer = Deeyah, Darin Prindle, Andrew Smith, David Henshaw
| screenplay = 
| based on = 
| starring = Caroline Goode   Diana Nammi   Nazir Afzal
| music = L. Subramaniam
| cinematography = Jeremiah Chapman
| editing = 
| distributor = Fuuse Films 
| released =  
| runtime = 70 minutes
| country = United Kingdom/ Norway
| language = English
}}
Banaz A Love Story is a 2012 documentary film directed and produced by Deeyah Khan. 
{{cite web
| author = Paul Peachey 
|url= http://www.independent.co.uk/news/uk/crime/still-now-they-follow-me-footage-of-banaz-mahmod-warning-police-before-her-honour-killing-to-be-shown-for-the-first-time-8168099.html
|title=  Still now they follow me": Footage of Banaz Mahmod warning police before her honour killing to be shown for the first time 
|publisher=independent.co.uk
|date=September 24, 2012
|accessdate=September 28, 2012
}}  The film chronicles the life and death of Banaz Mahmod, 
{{cite web
| author = Tamara Hardingham-gill  
|url= http://www.dailymail.co.uk/femail/article-2207916/Banaz-Mahmod-Chilling-video-young-honour-killing-victim-warning-police-life-danger.html?ito%3Dfeeds-newsxml&ct=ga&cad=CAcQARgAIAAoATAAOABA0O-BgwVIAVgBYgVlbi1VUw&cd=CQhZnnPXWQg&usg=AFQjCNFoePy-ksI_IWlsY59UPJzD4Dk9yg
|title=  If anything happens to me, its them: Chilling previously unseen video of young honour killing victim warning police her life is in danger
|publisher= dailymail.co.uk
|date=September 24, 2012
|accessdate=September 28, 2012 Kurdish woman killed in 2006 in South London on the orders of her family in a so-called honour killing. 
{{cite web
| author =  Tracy McVeigh 
|url= http://www.guardian.co.uk/world/2012/sep/22/banaz-mahmod-honour-killing
|title=  Theyre following me: chilling words of girl who was honour killing victim
|publisher= guardian.co.uk
|date=September 22, 2012
|accessdate=September 28, 2012
}}  The film received its UK premiere at the Raindance Film Festival in London September 2012. 
{{cite web
| author =  Orestes Kouzof
|url= http://www.raindance.co.uk/site/index.php?id=593,9050,0,0,1,0
|title=  Banaz: A Love Story
|publisher= raindance.co.uk
|date=
|accessdate= September 28, 2012  
}} 

==Synopsis==
Banaz was born in Iraq and moved to England with her family when she was 10 years old. She was married at age 17 with a man 10 years older than her in an arranged marriage.  Within months the marriage turned violent. Banaz wanted a divorce and fell in love with someone of her own choosing, this behaviour was found to be shameful by her family leading to her death in January 2006.  Banaz went to the police 5 times before her death.  Detective Chief Inspector Caroline Goode of the Metropolitan Police led the investigation to recover the body of Banaz and her killers, securing the first ever extradition from Iraq to Britain.

{{cquote|bgcolor=#F0FFF0|“…. a completely shocking, revealing, and timely insight into the scourge of ‘honour killing’. … quite literally a horror movie tracking the brutal and agonising life, love, and death of Banaz Mahmod who is terrorised and ultimately put to death by the very people who should have loved her most – her family.”    Jon Snow, Channel 4  
{{cite web
| author = 
|url= http://www.asafeworldforwomen.org/rights-defenders/rd-europe/3076-banaz-love-story.html
|title= New Film Tells Brutal Story of Honour Killing in Suburban London  
|publisher=asafeworldforwomen.org
|date=
|accessdate= September 28, 2012
}}  }}

{{cquote|bgcolor=#F0FFF0|“…Like watching a car-crash in slow motion the amount of information dredged up in BANAZ: A LOVE STORY suffocates you. You know the outcome, yet here you are taking it all in wondering why the hell nobody was able to stop it from happening......This gradual drip-drip of information leading to tragedy feels much like DREAMS OF A LIFE, the account of how Joyce Vincent dropped out of society to die alone in 2003.”  David Perilli   
{{cite web
| author = David Perilli
|url= http://www.takeonecff.com/2012/banaz-a-love-story
|title=  Banaz: A Love Story
|publisher=takeonecff.com
|date=October 2, 2012
|accessdate=October 7, 2012
}}  }}
 
{{cquote|bgcolor=#F0FFF0| “If their own blood relatives discarded, betrayed, forgot and harmed them, then they are our children, our sisters our mothers that we will mourn, we will remember, we will honour their memory and we will not forget!”      Deeyah talked to Safeworld about her reason for making the film. 
{{cite web
| author = 
|url= http://www.asafeworldforwomen.org/rights-defenders/rd-europe/3076-banaz-love-story.html
|title=   Deeyah Speaks Out
|publisher=asafeworldforwomen.org
|date=
|accessdate= October 7, 2012
}}  }} 

==Film screenings==

Banaz a Love Story has been re-versioned for ITV (TV network)|ITVs UK investigative journalism series Exposure (UK TV series)|Exposure, for UK national broadcast on 31 October in co-production with  Hardcash Productions and Fuuse Films. 
   The re-versioned film for ITV Exposure is named: BANAZ - AN HONOUR KILLING.

Further screenings of Banaz A Love Story: 
*  
*Womens International Film & Arts Festival 2013 (Miami)
*The Home Office film screening UK 2012
*Glasgow Film Festival 2013
*Bergen Film Festival October 2013
*WATCH DOCS. Human Rights In Film IFF November 2013 Poland
*House Of Lords screening United Kingdom December 2013
*Swedish Parliament screening January 2014
*De Balie Amsterdam Feb 2014
*OUR LIVES...TO LIVE FILM FESTIVAL India Feb 2014
*Chulalongkorn University Thailand March 2014 
*FESTIVAL DU FILM ET FORUM INTERNATIONAL SUR LES DROITS HUMAINS | FIFDH Switzerland March 8, 2014
*United Nations Human Rights Council Geneva March 10, 2014  
{{cite web
| author = GRO ROGNMO
|url= http://www.dagbladet.no/2014/03/11/kultur/film/premiere/deeyah_khan/banaz/32226862/
|title= Viste film om æresdrap for FN
|publisher=dagbladet.no
|date=March 11, 2014
|accessdate=March 11, 2014
}} 
*UN New York May 6, 2014

==Cast==
*Banaz Mahmod
*Bekhal Mahmod
*Caroline Goode
*Diana Nammi
*Palbinder Singh
*Victor Temple
*Bobbie Cheema
*Joanne Payton
*Andy Craig
*Stuart Reeves
*Nazir Afzal

==Awards==
* Banaz a Love Story has been nominated by Royal Television Society in the category of  Journalism award for Home/British Current Affairs. 
{{cite web
| author =  
|url= http://www.rts.org.uk/rts-announces-shortlist-television-journalism-awards-20112012
|title= RTS ANNOUNCES SHORTLIST FOR TELEVISION JOURNALISM AWARDS 2011/2012
|publisher=rts.org.uk
|date=
|accessdate= February 1, 2013
}}  The Banaz film was one of 3 films nominated from all British TV from 2011 and 2012
* The Banaz film won the American Peabody Award in March 2013 for international TV Documentary. 
{{cite web
| author = ekropp
|url= http://peabodyawards.com/2013/03/72nd-annual-peabody-awards-complete-list-of-winners/
|title= 72nd Annual Peabody Awards: Complete List of Winners
|publisher=peabodyawards.com
|date=March 27, 2013
|accessdate=March 29, 2013
}} 
* The Banaz film won the 2013 Emmy award for Best International Current Affairs Film. 
{{cite web
| author =  THE DEADLINE TEAM
|url= http://www.deadline.com/2013/08/international-emmy-current-affairs-news-nominees-announced/ 
|title= International Emmy Current Affairs, News Nominees Announced
|publisher=deadline.com
|date=August 14, 2013
|accessdate=August 17, 2013
}}  2013 Bergen International Film Festival. 
{{cite web
| author = 
|url= http://www.biff.no/informasjon/article1133999.ece
|title= NORSKE DOKUMENTARVINNERE
|publisher=biff.no
|date=October 29, 2013
|accessdate=October 31, 2013
}} 

==HBVA & Memini==

During the making of the film, Deeyah worked with experts, activists and NGOs specialising in the field of honour-based violence globally, which led to a shared recognition of the urgent need for online educational resources and campaigning networks dedicated to the issue.

As a result, the making of Banaz: A Love Story, led to Deeyah founding two independent initiatives:

* HBVA ( ), an international digital resource centre working to advance awareness through research, documentation, information and training for professionals who may encounter women, girls and men at risk, building partnerships with experts, activists, and NGOs from around the world.

* Memini, an online memorial to victims of honour killing. Memini exists to acknowledge the lives and deaths of thousands who are killed in the ongoing massacre of “honour” killing. We seek to create a community of remembrance to end the silence, honour the dead and keep their memories alive, collecting and preserving the stories of women like Banaz, as well as celebrating their strength and courage.

==References==
 

==External links==
* 
* 
* 
*  
* 
* 
*Nick Cohen  

 
 
 
 
 
 
 
 