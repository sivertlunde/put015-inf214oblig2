The Secret in Their Eyes
 
{{Infobox film
| name           = The Secret in Their Eyes
| image          = Cartel-nuevo-de-el-secreto-de-sus-ojos.jpg
| caption        = Spanish language theatrical poster
| director       = Juan José Campanella
| producer       = Juan José Campanella Gerardo Herrero Mariela Besuievski
| writer         = Eduardo Sacheri Juan José Campanella
| based on       =  
| starring       = Ricardo Darín Soledad Villamil Guillermo Francella Pablo Rago Javier Godino Mariano Argento José Luis Gioia
| music          = Federico Jusid Emilio Kauderer
| editing        = Juan José Campanella
| cinematography = Félix Monti
| studio         = Tornasol Films Haddock Films 100 Bares
| distributor    = Sony Pictures Classics Distribution Company  (Argentina) 
| released       =  
| runtime        = 129 min. 
| country        = Argentina
| language       = Spanish
| gross          = $33,965,279 (Worldwide)   
}}
The Secret in Their Eyes ( ) is a 2009   and Soledad Villamil.

The story unearths the buried romance between a retired judiciary employee and a judge who worked together a quarter century ago. They recount their efforts on an unsolved 1974 rape and murder that is an obsession not only for them, but for the victims husband and the killer.  The double setting frames the period of Argentinas Dirty War (1976–1983), a violent time when criminality often went unpunished.  
 Oscar for Best Foreign Language Film at the 82nd Academy Awards, and, with 1985s The Official Story, made Argentina the first country in Latin America to win it twice.   Three weeks before, it had received the Spanish equivalent with the Goya Award for Best Spanish Language Foreign Film.  As of 2010, it was only surpassed at the Argentine box office by Leonardo Favios 1975 classic Nazareno Cruz and the Wolf (Nazareno Cruz y el lobo).  

==Plot==
Retiree Benjamín Espósito is having trouble getting started on his first novel. He pays a visit to the offices of Judge Irene Menéndez-Hastings to tell her about his plans to recount the story of the Coloto case, the one they both worked on 25 years before, when she was his new department chief and he was the federal agent assigned to the case. She suggests he start at the beginning.

The beginning is the day that Espósito  was assigned to the rape and murder of Liliana Coloto, who was attacked in her home on a fine June morning in 1974. Espósito promises her widower, Ricardo Morales, that the killer will do life for his crime. His investigation is joined by his alcoholic friend and assistant, Pablo Sandoval, and the Harvard University|Harvard-educated Hastings. Before the three can start, their rival, Romano, tries to show them up by having officers beat a confession out of two innocent construction laborers, who had been working near the couples apartment. Espósito gets them released and physically attacks Romano in a justice building hall.

Back on the case, the agent finds a clue to the murderers identity in Lilianas photo albums. He notices that pictures from her home town of Chivilcoy frequently show a suspicious young man named Isidoro Gómez; his eyes never leave her.

Irene  finds this draft of the story unbelievable, since she does not agree that an agent can identify a killer by the look in his eyes. Benjamín  insists all of a young mans feeling for a woman is spoken there.

Although Gómez was recently in Buenos Aires, he has left both his apartment and employment. Espósito and Sandoval travel to Chivilcoy and sneak into Gómezs mothers house, where they find his letters to her. Sandoval steals them but they contain nothing useful and, when their supervising judge learns of the illegal action, the case is closed.

Over an evening review of the manuscript, Benjamín reminds Irene that it was only one week later that she announced her engagement. The memory is poignant, and she decides that she cannot revisit the past through his novel anymore. 
 Racing Club, a Buenos Aires football club, indicating Gómezs fixed "passion" for the team. Therefore, Espósito and Sandoval attend a match for Racing and spot Gómez in the crowd, who slips away when a Racing goal sends the crowd into a frenzy. Gómez is pursued by the duo through the stadium and nearly vanishes before he is cornered, arrested, and taken in for questioning. Espósitos largely illegal interrogation is interrupted by Hastings, but when she finds herself looking in the suspects eyes, she uses her status and sexuality to provoke him with taunts about his masculine inadequacies. It works: he exposes himself and takes a swing at her in the same moment he confesses. Justice seems served.

Late one night, while contemplating the sacrifice of his lost friend Pablo Sandoval, Benjamín gets a call from Irene asking to see the rest of his book.

In 1975, the widower sees his wifes killer on television, included in a security detail for the president of Argentina, María Estela Martínez de Perón. Hastings and Espósito quickly establish that Romano, now working for a government agency, released the murderer to settle the old score. Romano insults them both, taunting Espósito for being beneath Hastings. Undeterred, she later invites Espósito to offer his objections to her impending marriage plans later that night. Before they can meet, however, he has to leave a very intoxicated Sandoval in his living room to run and fetch Sandovals wife to take him home, but when the two return they find the front door broken and Sandoval inside, shot to death with a submachine gun. Now fearing that Romano wants him killed, Espósito accepts the remote isolation of Jujuy Province. Hastings takes him to the train station for a disconsolate goodbye.

The novel complete, Irene shares her satisfaction with the results, although she doesnt find the scene in the train station believable. They agree the story lacks the right ending. Benjamín is looking for the answer to a question: "How does one live a life full of nothing?". With Irenes help, Benjamín locates Ricardo Morales leading a quiet life in a rural area of Buenos Aires Province, and takes his finished book there. Although the widower apparently has relinquished his obsession with the murder case, Benjamín has to ask him how he has lived without the love of his life for 25 years. When Benjamín repeats Pablos final promise to get Gómez, Ricardo hesitantly confesses that in 1975 he ended Gómezs stalking of Benjamín by kidnapping and shooting him dead.

A disturbed Benjamín starts the drive back to the city, distracted that something doesnt seem right. Impulsively, he pulls over, leaves his car by the side of the road, and stealthily returns to Ricardos property. He follows Ricardo into a small building set near the main house, where he is shocked to find Gómez living in a makeshift cell, undetectable from the outside. Gómez plaintively asks Benjamín to request that Ricardo at least speak to him. Ricardo reminds Benjamín of his promise that Gómez would never go free.

Benjamín pays his respects at Pablos grave, then goes to see Irene with an evident sense of purpose. She notices something different in his eyes, reminds him that it will be complicated, and asks him to close the door.

==Cast==
* Ricardo Darín as Benjamín Espósito
* Soledad Villamil as Irene Hastings
* Guillermo Francella as Pablo Sandoval
* Pablo Rago as Ricardo Morales
* Javier Godino as Isidoro Gómez
* Mariano Argento as Romano
* Carla Quevedo as Liliana Coloto
* José Luis Gioia as Inspector Báez

== Political context ==
The setting of the film ties its characters to the political situation in Argentina in the period just before and after Argentinas  , 2010  The dictatorships National Reorganization Process lasted from 1976 to 1983, marred by widespread human rights violations that to some sources amounted to a genocide. 

==Production== Law & Eduardo Blanco, however, is not featured in the movie; the part of Daríns characters friend is played instead by comedian Guillermo Francella. 
 stadium of football club Club Atlético Huracán|Huracán, and took three months of pre-production, three days of shooting and nine months of post-production. Two hundred extras took part in the shooting, and visual effects created a fully packed stadium with nearly fifty thousand fans.    

==Reception==
The Secret in Their Eyes received very positive reviews from critics, not only in Argentina,   but also abroad; it holds a 91% "Certified Fresh" rating at   it holds a score of 81/100, meaning "Universal acclaim", based on 33 critic reviews.

==References==
 

==External links==
 
*    
*  
*  
*  
*  
*  
*  
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 