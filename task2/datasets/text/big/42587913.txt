Between Frames: The Art of Brazilian Animation
{{Infobox film
| name           = Between Frames: The Art of Brazilian Animation
| image          = Between Frames Film Poster.jpg
| caption        = Theatrical release poster
| director       = Eduardo Calvet
| producer       = Felipe Haurelhuk
| writer         = Eduardo Calvet
| starring       = 
| music          = Renato Alencar
| cinematography = Roberto Riva
| editing        = Fernando Mendonça Bruno Torres
| studio         = Ideograph Canal Brasil
| distributor    = 
| released       =  
| runtime        = 99 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = 
}}Between Frames: The Art of Brazilian Animation — released in Brazil as Luz, Anima, Ação — is a 2012 Brazilian documentary film directed and written by Eduardo Calvet. The film recalls the memory of almost 100 years of history of Brazilian animation, revealing the main personalities who helped to build this trajectory and remembering landmarks and creations of all times from films, TV and advertising.   

==Synopsis== Ice age and Rio (2011 film)|Rio, directed by Carlos Saldanha. Besides him, other big names were also heard, like Maurício de Souza, Otto Guerra and Chico Liberato, who report their experiences on the market and give a current panorama of Brazilian production.      

==Cast==
 
*Alê Abreu as Himself
*Diego Akel as Himself
*Guilherme Alvernaz as Himself
*Alceu Baptistão as Himself
*Telmo Carvalho as Himself
*Celia Catunda as Herself
*César Coelho as Himself
*Maurício de Sousa as Himself
*Marcelo Fabri Marão as Himself
*Arnaldo Galvão as Himself
*Otto Guerra as Himself
*Pedro Iuá as Himself
*Paulo José as Himself
*Olívia Latini as Herself
*Chico Liberato as Himself
*Andres Lieban as Himself
*Marcos Magalhães as Himself
*Roberto Maia as Himself
*Daniel Messias as Himself
*Kiko Mistrorigo as Himself
*Antônio Moreno as Himself
*Paulo Munhoz as Himself
*Itsuo Nakashima as Himself
*José Mario Parrot as Himself
*Aída Queiroz as Herself
*Walbercy Ribas as Himself
*Quia Rodrígues as Himself
*Rosaria as Herself
*Carlos Saldanha as Himself
*Allan Sieber as Himself
*Maurício Squarisi as Himself
*Pedro Stilpen as Himself
*Ennio Torresan as Himself
*Rosana Urbes as Herself
*Fabio Yamaji as Himself
 

==References==
 

== External links ==
*  

 
 
 
 