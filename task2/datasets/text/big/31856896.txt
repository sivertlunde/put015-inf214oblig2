The Teaser
{{Infobox film
| name           = The Teaser
| image          = The Teaser - window card.jpg
| image size     = 
| alt            = 
| caption        = 1925 window card
| director       = William A. Seiter
| producer       = Carl Laemmle Jack Wagner Lewis Milestone (adaptation)
| based on       =   Pat OMalley Hedda Hopper Walter McGrail
| music          =  George Barnes
| editing        = 
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = Silent English intertitles
| budget         = 
| gross          =
}}
 1925 American silent romantic Jack Wagner Pat OMalley, Hedda Hopper, and Walter McGrail.  {{cite book
|last=Clive Hirschhorn|title=The Universal story|publisher=Crown|year=1983|pages=50
|url=http://books.google.com/books?ei=ZM3aTaOOLoiasAPvzJiODA&ct=result&id=u3tZAAAAMAAJ&dq=%22The+Teaser%22%2C+%22Pat+O%27Malley%22&q=%22The+Teaser%22}}  {{cite book
|title=The Saturday Evening Post|publisher=Curtis Pub. Co.|year=1925
|volume=Volume 198|pages=52|url=http://books.google.com/books?ei=ZM3aTaOOLoiasAPvzJiODA&ct=result&id=LBk7AQAAIAAJ&dq=%22The+Teaser%22%2C+%22Pat+O%27Malley%22&q=%22The+Teaser%22}}  {{cite book|last=National Board of Review of Motion Pictures|title=Films in review
|publisher=National Board of Review of Motion Pictures|year=1980|volume=Volume 31
|pages=463|url=http://books.google.com/books?id=GRJCAQAAIAAJ&q=%22The+Teaser%22,+%22Pat+OMalley%22&dq=%22The+Teaser%22,+%22Pat+OMalley%22&hl=en&ei=ZM3aTaOOLoiasAPvzJiODA&sa=X&oi=book_result&ct=result&resnum=4&ved=0CD0Q6AEwAw}}  {{cite book
|last=American Film Institute|title=The American Film Institute catalog of motion pictures produced in the United States, Part 1|editor=Kenneth White Munden|publisher=University of California Press|location=The Teaser (Universal-Jewel)|year=1997
|pages=787–788|isbn=0-520-20969-9|url=http://books.google.com/books?id=rlLbRAPOgP0C&pg=PA787&lpg=PA787&dq=%22The+Teaser%22,+%22Laura+La+Plante%22&source=bl&ots=DKQ9yxzPhu&sig=439-ly83-RQl7avWydIoausSsWw&hl=en&ei=N5faTde-KIe8sQOx__SEDA&sa=X&oi=book_result&ct=result&resnum=2&ved=0CBoQ6AEwATgK#v=onepage&q=%22The%20Teaser%22%2C%20%22Laura%20La%20Plante%22&f=false}}   It is unknown whether any copies of this film exist,  and it is considered a lost film. 

==Plot== Pat OMalley), a cigar salesman.  She is then adopted by Margaret Wyndham (Hedda Hopper), her rich and aristocratic aunt, who disapproves of James due to his crude manners. Wishing to break up the two, Aunt Margaret sends Ann away to finishing school.  In response, Ann acts out publicly and embarrasses her aunt. In the meantime, James learns how to be a proper gentleman and wins her back through having learned good manners and a more dignified bearing.

==Cast==
* Laura La Plante as Ann Barton  Pat OMalley as James McDonald 
* Hedda Hopper as Margaret Wyndham 
* Walter McGrail as Roderick Caswell 
* Byron Munson as Perry Grayle 
* Vivien Oakland as Lois Caswell 
* Wyndham Standing as Jeffry Loring 
* Margaret Quimby as Janet Comstock 
* Frank Finch Smiles as Jenkins 
* Janet Gaynor as

==Reception==
The New York Times felt there was no need to be overly enthusiastic about the filmss plot or character portrayals, when they wrote "it contains a silly, soulless lot of characters and a weird idea of drama."  When they expanded on Pat OMalleys character of the cigar salesman, they granted that while it would be reasonable for a salesman to be willing to push his wares, they questioned the script having his character be so naive as to press the issue when he is at the home of his grilfriends benefactors attempting to impress them and win her heart, by writing "one does not expect James MacDonald to be such an utter fool as to stick cigars under the noses of guests in a pretentious mansion at a time he hoped to wage war on the heart of the pretty Ann Barton."  And in speaking toward Laura La Plantes character, who is scripted as being "a sly little minx, who believes in uttering untruths when they help her out of a difficulty, even if they do reflect on other persons,", they offerered that "Miss La Plante is not particularly effective in this picture." They concluded "The story is a pathetic little thing which is not apt to interest many persons." {{cite news
|url=http://movies.nytimes.com/movie/review?res=9901E4DC1531EE3ABC4D52DFB066838E639EDE
|title=The Screen: The Manicure Girl & The Teaser
|last=Mordaunt Hall
|date=June 15, 1925
|work=The New York Times Time Magazine offered that "The extraordinarily blonde Laura La Plante occupies herself genially enough in the title part." {{cite book
|title=Cinema: The New Pictures Jun. 22, 1925
|publisher=Time (magazine)
|date=June 22, 1925 |url=http://www.time.com/time/magazine/article/0,9171,880833,00.html}} 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 