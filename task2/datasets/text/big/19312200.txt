The Fourth Alarm
{{Infobox film
| name           = The Fourth Alarm
| image          = Poster of the movie The Fourth Alarm.jpg
| image_size     =
| caption        =
| director       = Robert F. McGowan
| producer       = Hal Roach F. Richard Jones
| writer         = Hal Roach H. M. Walker Robert A. McGowan
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        = Richard C. Currier
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        = 20 minutes
| country        = United States Silent English intertitles
| budget         =
}}
 short silent silent comedy Hook and Ladder in 1932.

==Notes==
This is Mary Kornmans final Our Gang appearance as a child.  She would appear again later in several episodes as an adult.

==Cast==

===The Gang===
* Joe Cobb as Joe
* Jackie Condon as Jackie
* Johnny Downs as Johnny
* Allen Hoskins as Farina
* Jannie Hoskins as Mango
* Mary Kornman as Mary
* Mildred Kornman as Mildred
* Scooter Lowry as Skooter
* Jay R. Smith as Turkie-egg Bobby Young as Bonedust
* Billy Naylor - Our Gang member
* Pal the Dog as Himself
* Buster the Dog as Himself
* Dinah the Mule as Humidor

===Additional cast===
* Charles A. Bachman - Officer
* Ed Brandenburg - Fireman
* George B. French - Chemis
* Ham Kinsey - Fireman
* Sam Lufkin - Crowd extra
* Gene Morgan - Fireman

==See also==
* Our Gang filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 


 