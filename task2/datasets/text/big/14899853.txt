So Big (1932 film)
{{Infobox film
| name           = So Big!
| image          = So Big! (1932 film) poster for the film.jpg
| image_size     = 225px
| caption        = theatrical release poster
| director       = William A. Wellman
| producer       = Jack L. Warnerp
| writer         = J. Grubb Alexander Robert Lord
| based on       = novel by Edna Ferber
| starring       = Barbara Stanwyck
| music          = W. Franke Harling
| cinematography = Sidney Hickox William Holmes
| distributor    = Warner Bros.
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| gross          =
| budget         =
}}
 Pulitzer Prize-winning novel of the same title by Edna Ferber.
 silent So film of 1953 remake was directed by Robert Wise and starring Jane Wyman.  In 1939 Barbara Stanwyck would reprise her role as Selina Peak De Jong for a radio broadcast.

==Plot==
Following the death of her mother, Selina Peake (Barbara Stanwyck) and her father (Robert Warwick) move to Chicago, where she enrolls in finishing school. Her father is killed, leaving her penniless, and Selinas friend Julie Hemple (Mae Madison) helps her find a job as a schoolteacher in a small Dutch community. Selina moves in with the Poole family and tutors their son Roelf (George Brent). Selina eventually marries immigrant farmer Pervus De Jong (Earle Foxe) and gives birth to Dirk (Hardie Albright), nicknamed "So Big", who becomes the primary focus of her life. When Pervus dies, Selina struggles to keep the farm afloat so she can afford to finance her sons education, hoping he will become an architect.

Dirk becomes involved with a married woman, who arranges for him to get a job as a bond salesman in her husbands firm, making much more money than as an apprentice architect. Eventually he meets and falls in love with unconventional artist Dallas OMara (Bette Davis), but she refuses to marry him because of his lack of ambition. Roelf, now a renowned sculptor, meets Dirk and, learning Selina is his mother, reunites with his former tutor. She is pleased to know her influence helped mold Roelfs character, even as she accepts her own sons weaknesses and disappointments.

==Cast==
  
* Barbara Stanwyck as Selina Peake De Jong
* George Brent as Roelf Pool Dickie Moore as Dirk De Jong (younger)
* Bette Davis as Miss Dallas OMara
* Mae Madison as Julie Hempel
* Hardie Albright as Dirk De Jong
 
* Alan Hale, Sr. as Klass Poole
* Earle Foxe as Pervus De Jong
* Robert Warwick as Simeon Peake, gambler
* Dorothy Peterson as Maartje Pool
* Noel Francis as Mabel, a "fancy woman"
* Dick Winslow as Roelf, age 12
 

==Production==
 , it was her second film for Warner Bros., and the first in which she appeared with George Brent, who co-starred with her in eleven more films. Davis considered her casting in a prestigious Barbara Stanwyck project a sign Jack L. Warner was acknowledging her value to the studio. In her 1962 autobiography A Lonely Life, she recalled, "It was a source of tremendous satisfaction, and encouraged me to unheard-of dreams of glory.". 

==Critical reception==
Andre Sennwald of the New York Times called the film "a faithful and methodical treatment of Miss Ferbers novel, but without fire or drama or the vitality of the original." He added, "A fine actress, Miss Stanwyck seems ill-suited to a role that hustles her in jerky steps from girlhood to old age; a role in which she is asked to express rugged grandeur and the beauty of a life well-lived from behind a mask of grease paint . . . Little Dickie Moore is delightful as the younger So Big. Bette Davis . . . is unusually competent.". 

Variety (magazine)|Variety noted, ""Wellmans endeavor at kaleidoscopic flashes in the life of Selina Dejong . . . make for a choppy continuity . . . As it is, the 83 minutes are overly long, but in toto, its a disjointed affair."   

The New Yorker considered Barbara Stanwycks performance "the best work she has yet shown us",  while the New York Daily Mirror called her "exquisite" and added, "Her great talent as an actress never has been demonstrated more brilliantly. A sparkling performance. She is magnificent.". 

==References==
Notes
 

==External links==
*  
*  
*  
*  

 

 
 

 
 
 
 
 
 
 