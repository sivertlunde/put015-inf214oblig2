The Midnight Wedding
 
{{Infobox film
| name           = The Midnight Wedding
| image          = Midnight_Wedding.jpg
| image_size     =
| caption        = Still from the film 
| director       = Raymond Longford
| producer       = Charles Cozens Spencer
| writer         = Raymond Longford
| based on    = play by Walter Howard
| narrator       =
| starring       = Lottie Lyell
| music          =
| cinematography = Ernest Higgins
| editing        = Ernest Higgins
| distributor    = 
| studio        = Spencers Pictures
| released       = 7 December 1912 "Raymond Longford", Cinema Papers, January 1974 p51 
| runtime        = 3,500 feet 
| country        = Australia
| language       = Silent film  English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}}

The Midnight Wedding is a 1912 Australian silent film directed by Raymond Longford based on a popular Ruritanian stage play in which Longford had appeared.   It is considered a lost film.

==Plot==
In the fictitious European country of Savonia, the dashing Paul Valmar (Augustus Neville) enlists in the Hussars after the death of his mother. After five years of service he is becomes a lieutenant and is appointed sword master to the regiment, causing jealousy amongst other others, notably the young Prince Eugene von Strelsburg (George Parke) and the wealthy Captain von Scarsbruck (D.L. Dalziel). Von Scarsbruck has been rejected by Eugenes sister, the Princess Astrea (Lottie Lyell), and he gets Eugene involved in gambling. Eugene taunts Valmar about his parentage, resulting in a fight in which Eugene is injured. Valmar seeks refuge in a church.

Under the terms of her fathers will, the Princess Astrea must marry, but is given the option of von Scarsbruck or a nunnery. Father Gerard conceives of the idea of uniting Valmar with the princess. He blindfolds the officer and marries him to her on midnight. Valmar is subsequently captured and brought towards the Crown Prince. Valmar informs him that he is the Crown Princes own son.

Von Scarsbruck is still intent on forcing a marriage with Astrea by destroying her reputation. He visits her chamber late one night, and is discovered by Valmar who challenges him to a duel. Valmar is injured, and Astrea confesses to her brother that she is married to him. Valmar recovers from his wounds and fights another duel with Von Scarsbruck. During the fight the cowardly Eugene tries to strike up Valmars sword and Valmar runs him through. Astrea succeeds in stopping the fight, then the Crown Prince intervenes and puts Eugene and von Scarsbruck under arrest, banning all women from officers quarters. Astrea disguises herself as an officer and sneaks into see the injured Valnar. She is discovered by the Crown Prince and confesses she is married to him.

Three months later, Valmar has recovered from his wounds and duels von Scarsburck again. Astrea hears about this and rides to the duel just in time to see Valmar mortally wound his opponent. Valmar and Astrea marry again, this time in a large ceremony.  

==Cast==
Cast (in alphabetical order) 
*J. Barry ... Rev. A. Cette
*D.L. Dalziel ... Capt. Rudolph von Scarsbruck
*Jack Goodall ... Father Gerard
*Robert Henry ... Maj. Donelli
*Tim Howard ... Cpl. Otto
*Dorothy Judge ...Kathie
*Nellie Kemberman ... Stephanie
*Tom Leonard ... Pvt. Bobo
*Victor Loydell ... Sgt. Max
*Lottie Lyell ... Princess Astrea
*Augustus Neville ... Paul Valmar
*George Parke ... Lt. Prince Eugene
*Harry Saville ... Innkeeper
*Arthur Smith ... Dr. Eitel
*Fred Twitcham ... Crown Prince of Savonia

==Original Play==
{{Infobox play
| name       = The Midnight Wedding
| image      = The Midnight Wedding.jpg
| image_size = 
| caption    = Poster from a 1907 Australian production
| writer     = Walter Howard
| characters = 
| setting    = 
| premiere   = 
| place      = 
| orig_lang  = English
| subject    = 
| genre      = melodrama
}}
The play had was first performed in Australia in 1906  and was enormously popular. Longford had appeared in it playing the role of Von Scarsbruck although he did not act in the film.  The cast largely came from the stage production produced by Clarke and Clyde Meynell.

==Production==
The movie was the first to be shot at Charles Cozens Spencers new studio at Rushcutters Bay in Sydney, enabling it to feature elaborate sets. 

==Release==
The movie is sometimes confused with a British film of the same name that was released in Australia around the same time.  It received good reviews and was a popular success at the box office.  

==References==
 

==External links==
* 
*  at National Film and Sound Archive
*  at National Archives of Australia (registration required)
 

 
 
 
 
 
 
 