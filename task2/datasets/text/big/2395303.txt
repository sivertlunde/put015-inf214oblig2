End of the Century: The Story of the Ramones
 
{{Infobox film
| name           = End of the Century: The Story of the Ramones
| image          = End of the century poster.jpg
| caption        = Promotional poster for the documentary
| director       = Jim Fields Michael Gramaglia
| producer       = Diana Holtzberg Andrew Hurwitz Jan Rofekamp Jim Fields Michael Gramaglia
| writer         = 
| starring       = The Ramones Rob Zombie
| music          = 
| cinematography = David Bowles Jim Fields John Gramaglia Michael Gramaglia Peter Hawkins George Seminara
| editing        = Jim Fields John Gramaglia
| distributor    = Magnolia Pictures (USA)  Tartan Films (UK)
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = 
}} New York punk rock band the Ramones. The film, produced and directed by Jim Fields and Michael Gramaglia, documents the bands history from their formation in the early 1970s and 22 subsequent years of touring, to their 1996 breakup and the deaths of three of the four original members. The title is taken from the Ramones 1980 album, End of the Century.

==Overview==
The film tells the story of the Ramones from their beginnings in Forest Hills, Queens and earliest performances at New Yorks CBGBs to their unexpected induction into the Rock and Roll Hall of Fame in 2002. It features comprehensive and candid interviews with members Dee Dee Ramone, Johnny Ramone (who died a few months later), Joey Ramone, Marky Ramone, C.J. Ramone, Tommy Ramone, Richie Ramone, and Elvis Ramone. Others close to the band are also interviewed, including Joeys mother and brother, contemporaries such as Debbie Harry and Joe Strummer, and childhood friends of the members. The filmmakers first attempted to make the film in 1994 to document the final year of the band on the road but ran into difficulties with the bands management. Fields and Gramaglia successfully restarted the production in 1998 after the band had officially retired.
 Slamdance Film Festival in 2003, with its run-time subsequently shortened by nearly one hour. The final and completed version of the film didnt appear until February 2004 at the Berlin Film Festival and the film was released in the U.S. in August 2004. 

Even though the talk during the South American scenes is about Brazil, both the "mob scene" and the TV presentation take place in Buenos Aires, Argentina. In the mob scene, the kids chase the Ramones car down Carlos Pellegrini street, 100 meters north of the Obelisk of Buenos Aires|Obelisco landmark.

The Argentine presenter is   are reading off of cue cards (watch the guy on the right). Spector, who had a falling out with the Ramones due to mistreating the group, would later be imprisoned.

==External links==
* 
*  
* 

 

 
 
 
 


 