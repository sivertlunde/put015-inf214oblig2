Nil by Mouth (film)
 
 
{{Infobox film
| name           = Nil by Mouth
| image          = Nil by mouth poster.jpg
| director       = Gary Oldman
| producer       = Gary Oldman Douglas Urbanski Luc Besson
| writer         = Gary Oldman
| starring       = Ray Winstone Kathy Burke Charlie Creed-Miles
| music          = Eric Clapton
| cinematography = Ron Fortunato
| editing        = Brad Fuller
| studio         = EuropaCorp SE8 GROUP
| distributor    = Fox Film Corporation   ARP Sélection  
| released       =  
| runtime        = 128 minutes  
| country        = United Kingdom France
| language       = English
| budget         = $9 million   
| gross          = $266,130 
}}
 South East London. It was Gary Oldmans debut as a writer and director; the film was produced by Douglas Urbanski and Luc Besson. It stars Ray Winstone as Raymond, the abusive husband of Valerie (Kathy Burke). The film was a critical success, winning numerous awards.

==Plot== drug addict whom Raymond kicks out. The family is Dysfunctional family|dysfunctional, mostly due to Raymonds short temper and violent outbursts.

==Cast==
* Ray Winstone as Raymond "Ray"
* Kathy Burke as Valerie "Val"
* Charlie Creed-Miles as Billy
* Laila Morse as Janet
* Edna Doré as Kath
* Chrissie Cotterill as Paula
* Jon Morrison as Angus
* Jamie Foreman as Mark
* Steve Sweeney as Danny

==Production== council estate medical instruction (literally "nothing by mouth"), meaning that a patient must not take food or water.  It is set to the soundtrack "Peculiar Groove" by Frances Ashman.

==Release==
In 2001, Mind The Gap Theatre performed a stage adaptation in New York City as part of the British Airways sponsored UKwithNYC.

The screenplay, with introduction by Douglas Urbanski, was published in 1997 by ScreenPress Books.
 Jack English, was published in 1998 by ScreenPress Books.

==Reception==
Nil by Mouth received generally positive reviews, currently holding a 65% "fresh" rating on   is unflinching and observant." 

The film grossed $266,130 from 18 theatres in North America. 
 428 uses of the word "fuck".,  more than any film at the time until Summer of Sam beat it 2 years later. It still holds the record for the number of uses of "fuck" per minute in a dramatic film.

==Awards and nominations==
* 1997 Cannes Film Festival: Best Actress (Kathy Burke)   
** Nominee: Palme dOr (Golden Palm) 
* 1997 Edinburgh International Film Festival:
** Winner: Channel 4 Directors Award (Gary Oldman)
* 1997 European Film Awards:
** Nominee: Best Cinematographer (Ray Fortunato)
* 1997 BAFTA Awards:
** Winner: Alexander Korda Award for Best British Film (Luc Besson, Douglas Urbanski)
** Winner: BAFTA Award for Best Original Screenplay (Gary Oldman)
** Nominee: Best Performance by an Actor in a Leading Role (Ray Winstone)
** Nominee: Best Performance by an Actress in a Leading Role (Kathy Burke)
* 1998 British Independent Film Awards:
** Winner: Best Performance by a British Actor in an Independent Film (Ray Winstone)
** Winner: Best Performance by a British Actress in an Independent Film (Kathy Burke)
** Winner: Most Promising Newcomer in any Category (Laila Morse)
** Nominee: Best British Director of an Independent Film (Gary Oldman)
** Nominee: Best British Independent Film
** Nominee: Best Original Screenplay by a British Writer of a Produced Independent Film (Gary Oldman) Empire Awards:
** Winner: Best Debut (Gary Oldman)
* 1997 Royal Variety Club of Great Britain
** Winner: Best Film Actress (Kathy Burke) 
* 1997: Golden Frog Award: 
** Nominee: Cinematography (Ron Fortunato)

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 