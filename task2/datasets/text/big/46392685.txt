The Call of the Blood
{{Infobox film
| name = The Call of the Blood
| image =
| caption =
| director = Louis Mercanton
| producer =
| writer = Robert Hichens (novel)
| starring = Ivor Novello Phyllis Neilson-Terry Charles Le Bargy
| cinematography =
| editing =
| studio =
| distributor =
| released =  
| runtime =
| country = France French intertitles
| budget =
| gross =
}} silent drama film directed by Louis Mercanton and starring Ivor Novello, Phyllis Neilson-Terry and Charles Le Bargy. The film is most notable for giving a screen debut to the Welsh actor Novello, who went on to become a major star in the 1920s.  It is based on a novel of the same title by Robert Hichens. 

An Englishman commits adultery with a Sicillian woman.

==Cast==
* Gabriel de Gravone    
* Charles Le Bargy    
* Salvatore Lo Turco    
* Desdemona Mazza   
* Phyllis Neilson-Terry    
* Ivor Novello

== References ==
 

==Bibliography==
* Macnab, Geoffrey. Searching for Stars. Cassell, 2000.

== External links ==
*  

 

 
 
 
 
 
 
 
 

 