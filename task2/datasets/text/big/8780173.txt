Shaolin vs. Evil Dead
 
 
{{Infobox film
| name           = Shaolin Vs. Evil Dead
| image          = ShaolinVsEvilDeadDVDCover.jpg
| caption        =
| director       = Douglas Kung
| producer       = Jeremy Cheung
| writers        = Ho Yiu-Wang
| starring       = Gordon Liu
| music          = Brother Hung
| cinematography = Chi-kan Kwan
| editing        = Grand Yip 2004
| runtime        = 94 minutes
| country        = Hong Kong Cantonese
| budget         =
| preceded_by    =
| followed_by    =
| mpaa_rating    =
| tv_rating      =
}}

Shaolin Vs. Evil Dead (Shao Lin jiang shi) is a kung fu vampire movie starring Gordon Liu. The work is one of Lius most recent and was released between the time of Kill Bill volumes 1 & 2 and exploited his acting in the two films on the North American DVD cover. The movie title itself exploits the film Evil Dead, however there is actually no relationship between two films. The film also heavily references the style of Mr. Vampire, though has a unique plot.

The film ends abruptly, without resolution, because of a planned sequel, which is previewed in the end credits. The funding for the sequel was approved in 2005,  and the second half of the film,   was released the same year. 

==Cast==
*Gordon Liu as "Pak"
*Fan Siu-Wong as "Hak/Black"
*Jacky Woo
*Shannon Yoh

==Other credits==
*Executive producer: Sharon Yang Pan Pan
*Special effects makeup: Kwok Lai Lai
*Visual effects supervisor: Alan Chow

==References==
 

==External links==
*  

 
 
 
 
 


 
 