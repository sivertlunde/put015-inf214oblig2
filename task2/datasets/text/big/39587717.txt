The Battery (2012 film)
 
{{Infobox film
| name           = The Battery
| image          = 
| alt            = 
| caption        = 
| director       = Jeremy Gardner
| producer       = Jeremy Gardner Adam Cronheim Douglas A. Plomitallo Christian Stella
| writer         = Jeremy Gardner
| starring       = Jeremy Gardner Adam Cronheim Niels Bolle Alana OBrien
| music          = 
| cinematography = Christian Stella
| editing        = 
| studio         = O. hannah Films
| distributor    = 
| released       =   }}
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = $6,000   
| gross          = 
}}

The Battery is a 2012 American drama-horror film and the directorial debut of Jeremy Gardner. The film stars Gardner and co-producer Adam Cronheim as two former baseball players trying to survive a zombie apocalypse. The film premiered at the Telluride Horror Show in October 2012 and received a video-on-demand release June 4, 2013.  It has won audience awards at several international film festivals.

== Plot ==
After a zombie apocalypse has overtaken the entire New England area, two former baseball players, Ben (Jeremy Gardner) and Mickey (Adam Cronheim), travel the back-roads of Connecticut with no destination in mind. Their backstory reveals that they were trapped in a house in Massachusetts, along with Mickeys family, for three months. Mickeys father, mother and brother were killed before they figured out how to escape. Since then, Ben opposes sleeping indoors and the two have had many arguments regarding the matter. While Ben quickly adapts to the "hunter-gatherer" and constantly on the road lifestyle, Mickey is almost the opposite, refusing to face the situation and longs for a "normal" life. He refuses to kill zombies and learn to fish, and is always listening to his CD player with headphones on, burning through batteries and distracting himself from his surroundings, against Bens advice.

As the two visits Mickeys girlfriends now-unoccupied house, Mickeys proposal to sleep indoors is again quickly shot down by Ben. While gathering useful items from the house, Ben finds two walkie-talkie. As they test the walkie-talkies range, they accidentally overhear a conversation between two other survivors, Annie and "The Ochard", implying they belong to an organized group of survivors. Mickey eagerly contacts them and asks to join, only to be flatly refused. Despite Bens advice, Mickey tries contacting them again for a few times, but noone replies.

The duo travel further and explore a house inside a forest, where Mickey once again insists on staying indoors. Ben relents, presumably to ease his friends frustration, but takes away Mickeys headphone for the night. That evening, Ben drinks, listens to the music and dances, while Mickey speaks to the walkie talkie. Annie replies, this time firmly tells him to stay off the channel and stop contacting her group.

The following morning, Ben discovers a zombie being tied up near the house, and releases it in Mickeys room. He keeps the door shut while urging Mickey to kill it with the baseball bat he left in the room earlier. After the sound of struggle stops, Ben opens the door, discovering his friend has successfully killed the undead. Mickey angrily attacks Ben and storms outside. As Ben tries to cheer him up, Mickey breaks down in tears, telling Ben of his conversation with Annie.

Killing his first zombie and getting turned down by a community seem to have changed Mickey. He starts heeding Bens advice, learns fishing and spends much less time with his headphones on. As the two stops by the road to inspect a car, Mickey discovers the engine is still warm and gets taken hostage by a man with a knife, who orders Ben to give him their car keys. Ben tricks the man into letting Mickey go, then shoot and kill him as he flees. Immediately after, two other survivors arrive. The woman explained that the man stole the car from them, and says Ben did the right thing. Her companion Eggshead refuels the car and they prepare to leave. Mickey, recognizing Annies voice on the radio, calls her by name. Annie, not wanting them to follow her to her group, shoots Ben in the leg and throws their car keys away, then leaves with Eggshead. As it is too dark to find the keys, Ben and Mickey decides to sleep in the car until morning.

The duo are awakened by zombie noises, and turning on the headlights reveals that they have been surrounded by zombies. Since Bens leg makes it too painful to move, the two are forced to spend an undetermined amount of days inside the car. They finally decide that Mickey gets out via the sunroof and tries to find the car keys. When he returns, Ben finds out that he was bitten and is forced to shoot him. The movie ends with Ben talking to the walkie-talkie, claiming he will come and kill Annie to avenge Mickeys death. The mid-credit shows Ben has escaped the car and walks down the road, with a horde of zombie shambling behind.

   

== Production ==
The film cost $6,000 to make and was shot in 15 days in Connecticut.   The scenes were not planned in advance, and director Jeremy Gardner described it as "very seat-of-the pants." 

== Release ==
The film premiered at the Telluride Horror Show, a horror/genre film festival in Telluride, Colorado, and was released through various video-on-demand services on June 4, 2013.  The film was released on DVD in America on September 16, 2014.

== Reception ==
Lauren Taylor of Bloody Disgusting rated the film 4.5/5 stars and said it is "an excellent example of what can truly be considered horror."   Brad McHargue of DreadCentral rated it 4.5/5 stars and called it "a triumphant feat of dramatic horror."   In a positive review, Scott Weinberg of Fearnet wrote that its not a zombie film for all tastes, but "theres certainly a lot to like here."   Kurt Halfyard wrote that the film "is a very welcome case for what can be done with capable screenwriting."   Kier-La Janisse of Fangoria rated the film 3.5/4 stars and called the film "the most reinvigorating take on this overworn subgenre I’ve seen in ages." 

=== Awards ===

{| class="wikitable" border="1"
|+ Festival awards
! Festival !! Award
|-
| Buenos Aires Rojo Sangre || Best Film   
|-
| Imagine Film Festival|| Audience Award 
|-
| Dead by Dawn || Audience Award 
|-
| Toronto After Dark Film Festival || Audience Award (Gold), Best Screenplay, Best Music, Best Poster 
|-
| Festival Mauvais Genre || Audience Award   
|-
| Fantaspoa || Best Zombie Film 
|-
| Chainsaw Award || Best Limited Release Film 
|}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 