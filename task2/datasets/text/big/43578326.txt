From What Is Before
{{Infobox film
| name           = From What Is Before
| image          = Mula sa Kung Ano ang Noon (poster).jpg
| caption        = 
| director       = Lav Diaz
| producer       = Lav Diaz
| starring       = {{Plain list|
*Perry Dizon
*Roeder
*Hazel Orencio
*Karenina Haniel
*Mailes Kanapi
}}
| music          = 
| cinematography = Lav Diaz
| editing        = Lav Diaz
| studio         = Sine Olivia Pilipinas
| distributor    = 
| released       =  
| runtime        = 338 minutes
| country        = Philippines
| language       = Tagalog
}} Marcos dictatorship. The film had its world premiere in the Philippines on 3 July 2014, and competed at the 2014 Locarno International Film Festival where it won the main prize, the Golden Leopard.   
 2014 AFI Fest. 

== Plot ==
The Philippines, 1972. Mysterious things are happening in a remote barrio. Wails are heard from the forest, cows are hacked to death, a man is found bleeding to death at the crossroad and houses are burned. Ferdinand E. Marcos announces Proclamation No. 1081 putting the entire country under Martial Law.

== Cast ==
*Perry Dizon as Sito
*Roeder as Tony
*Hazel Orencio as Itang
*Karenina Haniel as Joselina
*Reynan Abcede as Hakob
*Mailes Kanapi as Heding

== Awards and nominations ==
{| class="wikitable"
|-
! Year
! Event
! Category
! Recipient
! Result
|- 2014
|Locarno Film Festival Golden Leopard for Best Film
|From What Is Before
| 
|-
|}

== References ==
 

== External links ==
* 
* 
* 

 
 

 
 
 
 
 