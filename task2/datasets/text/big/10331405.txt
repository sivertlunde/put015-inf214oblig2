Africa Express
{{Infobox film
| name = Africa Express
| image = Africa Express poster.jpg
| image_size =
| caption =
| director = Michele Lupo
| producer = Salvatore Alabiso
| writer = Mario Amendola Bruno Corbucci
| starring = Giuliano Gemma Ursula Andress Jack Palance
| music =Guido & Maurizio De Angelis
| cinematography = Roberto Gerardi
| editing =
| distributor = CFM Filmverhuur
| released = 1975
| runtime = 98 min.
| country = Italy Italian
| budget =
}}

Africa Express is a 1975 Italian adventure film starring Ursula Andress, Giuliano Gemma, and Jack Palance that was filmed in Rhodesia. A sequel Safari Express with the same leads followed a year later.

== Plot ==
John Baxter (Giuliano Gemma) is a freewheeling trader of goods in   in Detroit. Ursula Andress is Madeleine Cooper, the lady of mystery he runs into as she flees from the hunter played by Jack Palance.

== Cast ==
* Giuliano Gemma: John Baxter 
* Ursula Andress: Madeleine Cooper 
* Jack Palance: William Hunter / Preston 
* Giuseppe Maffioli: Father Gasparetto 
* Luciana Turina: Lily 
* Rossana Di Lorenzo: Mitzy 
* Nello Pazzafini: Hunters Henchman

== External links ==
*  
*  

 
 
 
 
 
 
 


 
 