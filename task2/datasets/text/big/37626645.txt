Always Be the Winners
 
 
{{Infobox film
| name           = Always Be the Winners
| image          = 
| alt            = 
| caption        = 
| film name = {{Film name| traditional    = 神龍賭聖之旗開得勝
| simplified     = 神龙赌圣之旗开得胜
| pinyin         = Shénlóng Dǔshèng Zhī Qíkāi Déshèng
| jyutping       = San4 Lung4 Dou2 Sing3 Zi1 Kei4 Hoi1 Dak1 Sing3}}
| director       = Jacky Pang
| producer       = Stephen Lo
| writer         = John Chan
| story          = 
| narrator       = 
| starring       = Tony Leung Chiu-Wai Tony Leung Ka-fai Eric Tsang Sandra Ng Ekin Cheng Charine Chan
| music          = Lowell Lo
| cinematography = Chan Yuen Kai
| editing        = Hai Kit Wai
| studio         = Ying Ji Wai
| distributor    = Regal Film Distributions
| released       =  
| runtime        = 97 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$14,094,286
}}
Always Be the Winners is a 1994 Hong Kong comedy film directed by Jacky Pang and starring Tony Leung Chiu-Wai, Tony Leung Ka-fai, Eric Tsang, Sandra Ng, Ekin Cheng and Charine Chan. The film was released during the Chinese New Year period of 1994 to celebrate the holidays.

==Plot==
Third Master Sha (Tony Leung Chiu-Wai) is a descendant of a powerful gambling family who must battle Yam Tin Sau (Ekin Cheng), the descendant of the rival gambling family to save his familys name. In order to win, Hui hires the China King of Gamblers Hui Man Long (Tony Leung Ka-fai) for help.

==Cast==
*Tony Leung Chiu-Wai as Third Master Sha
*Tony Leung Ka-fai as Hui Man Long, the China King of Gamblers
*Eric Tsang as Nanny
*Sandra Ng as Mrs. Lulu Sha
*Ekin Cheng as Yam Tin Sau
*Charine Chan as Shas third sister
*Au Gan as Shas second sister
*Teresa Mak as Scarlet Pimpernel
*Lo Hung as Nightclub patron
*Keni Tanigaki as Photographer

==See also==
*Ekin Cheng filmography

==External links==
* 
*  at Hong Kong Cinemagic
* 
*  at LoveHKFilm.com

 
 
 
 
 
 
 
 

 