Take This Lollipop
{{Infobox film
| name           = Take This Lollipop
| image          = Take This Lollipop.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Film poster
| director       = Jason Zada
| producer       = Jason Zada Brendan Kling Brian Latt (executive producer|e.p.) Oliver Fuselier (e.p.)
| writer         = Jason Zada
| starring       = Bill Oberst Jr.
| music          = Bobby Jameson
| cinematography = Mihai Malaimare Jr.
| editing        = Jason Zada
| studio         = Little Monster Tool of North America
| distributor    = Tool of North America
| released       =  
| runtime        = 1 minute
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 interactive horror horror short actor Bill app is used once, and then deleted. 

==Synopsis==
The interactive film first requests that viewers temporarily allow the application access to their Facebook account, and then incorporates information gleaned from the viewers Facebook page to fill in details of the film itself.

Showing The Facebook Stalker as a thin, creepy fellow, hunched over and typing at a computer keyboard, images provided from the accessed Facebook account begin to appear as the stalker types at his keyboard, and appears to search for the specific Facebook user who had granted access. The Stalker becomes more and more agitated as he scrolls through the discovered information, until he locates the home of the user, pulls up Google Maps, and finds directions to the users home from geographic data contained in his or her profile.  With the users profile picture taped to its dashboard, the stalker is then seen driving in his car to the users location, apparently to perform mayhem.

At the end of the film, a screen appears with an image of a red lollipop containing a razor blade. Below the image is the viewers Facebook screenname and the name of the stalkers next victim as gleaned from the viewers own profile.

==Production==

===Concept===
The title comes from a parents warning to children to avoid taking candy from strangers.      The concept developed from director Jason Zadas attraction to horror films from his youth, his wish to do something serious within that genre, his experience as a digital editor, and his understanding that people place their personal information on the internet for anyone to find. He decided to create a project that would "get under peoples skin without any gore or anything",  and that would underscore its point by making it about the viewer in a quite personal manner.     The writer/director came up with the idea in September 2011, after waking up one morning and thinking about how he loved the Halloween season.    Zada explained, "I wanted to do something that messed with people and I wrote the script. Instantly, I knew there was something special about the idea."  He briefly toyed with the idea of using an "A-list" actor, but instead chose character actor Bill Oberst Jr. for both his look and his skill. He stated, "When I saw Bills headshot, I knew he was the guy. It was a twist of a role and Bill was the right type and hed done horror movies. Some actors would overdo it, the audience needed to see what youre doing without thinking. I wanted people to feel his anger and discomfort with minimal movements. Bill went deep. He trusted the process."   Oberst himself spoke toward his development of The Facebook Stalker persona, saying "It was easy to get into character.  The filming environment was an abandoned and reputedly haunted hospital, that helped and Jasons script and direction did the rest. Stepping onto the dressed and lit set and sitting at that desk, it was very easy to feel the vibe." 

An earlier viral video by Zada was the Elf Yourself project for OfficeMax which had been seen by 194 million people in its first six weeks.     Being a fan of "exploring human interaction with media", Zada used similar techniques for Take This Lollipop, but tapped into what he sees as the "larger collective fear we have now"  toward personal information being on the internet. 

===Marketing===
The project had no real marketing at all, beyond its YouTube trailer and then an initial release on October 17, 2011 to a few personal friends, who then wrote about it on Twitter.   Within 24 hours of release, the film had been watched approximately 400,000 times and had over 30,000 "Like button|likes" on Facebook.   A week later, the film had been viewed 7 million times with 1.1 million "likes".     As of March 4, 2012, the film had received nearly 13 million "likes" on Facebook. 

===Release=== malicious app, but after Zada clarified that no Facebook information was being misused or shared, the site was unblocked. 

===Music=== Please Little Girl Take This Lollipop by Bobby Jameson,  and according to music production company Little Ears, it was scored by Future Perfect and mistimed for creepy effect. 

==Reception== Stern Magazine,    Site Oueb.    and International Business Times,    and continued discussions over how to protect children when they are using the internet, with coverage by such as the New Zealand Herald,    CNN,    and Persoenlich.   
 viral as a "customized horror movie that stars you and your friends".   

In discussing how parents must educate their children about the dangers inherent in a releasing of personal information about themselves to the internet, CNN wrote "Behind the litany of frightening facts and figures (not to mention fears like those preyed upon in viral-video Take This Lollipop, an interactive horror film that incorporates text and images from your Facebook profile) lurks a disturbing truth." 

==Recognition==
The website Co.Create listed the film as among The 5 All-Time Best Facebook Campaigns, calling it "One of the most interesting Facebook campaigns". 

===Awards and nominations===
* March 2012, Won three awards at SXSW: Overall Best in Show, first place in category Experimental, and first place in category Motion Graphics.   
* April 2012, Won D&AD Award for Digital Advertising/Web Films.    Andy Award for Agency Innovation.   
* April 2012, nominated for two Webby Awards, under categories Experimental and Weird, and Viral.   
* May 2012, Won Daytime Emmy Award for New Approaches - Daytime Entertainment.    

==References==
 

==External links==
*   at the Internet Movie Database
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 