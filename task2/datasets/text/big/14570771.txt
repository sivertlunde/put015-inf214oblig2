Ghatashraddha
{{Infobox film
| name           = Ghatashraddha
| image          = Ghatashraddha DVD COVER.jpg
| image_size     = 
| caption        = DVD cover
| director       = Girish Kasaravalli
| producer       = Sadananda Suvarna
| writer         = U. R. Ananthamurthy
| screenplay     = Girish Kasaravalli
| based on       =  
| starring       = Meena Kuttappa Ajith Kumar Narayana Bhat
| music          = B. V. Karanth
| cinematography = S. Ramachandra
| editing        = Umesh Kulkarni
| distributor    = Suvarnagiri Films
| released       = 1977
| runtime        = 108 minutes
| country        = India Kannada
}}

Ghatashraddha ( ,  ) is a 1977 Indian Kannada language film directed by Girish Kasaravalli starring Meena Kuttappa, Naraya Bhat and Ajith Kumar in lead roles. It is based on a novella by eminent Kannada writer U. R. Ananthamurthy. The film was Girish Kasaravallis first feature film as a director, and marked not only the arrival of a promising new filmmaker but also that of Kannada cinema in the Indias New Cinema horizon. 
 Best Feature Best Music Best Child Artist (Ajith Kumar).
 Indian cinema, having received 1.6 million votes. 

== Plot ==
A young Brahmin Vedic school student, who is from an aristocratic family, befriends his school masters daughter who is a pregnant widow. The boy tries but fails in concealing his friends pregnancy. The widow has an abortion forced on her, has the eponymous ritual performed on her and is excommunicated. The student returns home as his school shuts down.   

== Cast ==
* Meena Kuttappa as Yamuna
* Narayana Bhat as Shastri
* Ajith Kumar as Naani
* Ramakrishna
* Shantha
* Ramaswamy Iyengar
* Jagannath
* Suresh
* H. S. Parvathi

==Production==
Girish Kasaravalli approached U. R. Ananthamurthy for the rights of Ghatashraddha through dramatist K. V. Subbanna. Ananthamurthy, who was in the US at the time, gave his go-ahead on hearing the screenplay from Kasaravalli, on his return to India. It was on the formers suggestion that Meena Kuttappa, his former student, was signed to play the female lead in the film. 

== Awards ==
;25th National Film Awards (1977) Best Feature Film Best Music Direction - B. V. Karanth Best Child Artist - Ajith Kumar

;1977–78 Karnataka State Film Awards First Best Film Best Story – U. R. Ananthamurthy Best Screenplay – Girish Kasaravalli Best Child Actor– Ajith Kumar

== References ==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 


 