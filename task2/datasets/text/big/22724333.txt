Iligos
 

{{Infobox film
| name           = Iligos Ίλιγγος
| image          = 
| image_size     = 
| caption        = 
| director       = Giannis Dalianidis
| producer       = 
| writer         = Giannis Dalianidis
| starring       = Zoi Laskari Alexandros Alexandrakis Voula Zouboulaki Faidon Georgitsis Alekos Tzanetakos Lefteris Vournas Ilya Livykou Katerina Helmi Malena Anoussaki
| music          = Giannis Markopoulos
| cinematography = 
| photographer   = 
| editing        = 
| distributor    = Mask Films 1963
| runtime        = 90 min
| country        = Greece Greek
| cine.gr_id     = 
}}
 1963 Greece|Greek black and white drama film directed and written by Giannis Dalianidis.  It stars Zoi Laskari and Alekos Alexandrakis.

==Plot==
Elli (Zoi Laskari) leaves home for a reason that the absolutist father (Alekos Alexandros) approached romantically.  They started a rashly life.

==Cast==

*Zoi Laskari
*Alekos Alexandrakis
*Voula Zouboulaki
*Faidon Georgitsis
*Alekos Tzanetakos
*Lefteris Vournas
*Ilya Livykou
*Katerina Helmi
*Malena Anoussaki

==See also==
*List of Greek films

==External links==
* 

 
 
 
 


 