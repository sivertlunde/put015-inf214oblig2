Mutiny on the Bounty (1962 film)
 
{{Infobox film
| name           = Mutiny on the Bounty
| image          = Poster for Mutiny on the Bounty.jpg
| caption        = Original film poster by Reynold Brown
| director       = Lewis Milestone
| producer       = Aaron Rosenberg (uncredited)
| writer         = Charles Lederer Mutiny on the Bounty by Charles Nordhoff and James Norman Hall 
| starring       = Marlon Brando Trevor Howard Richard Harris
| music          = Bronislau Kaper Robert L. Surtees
| editing        = John McSweeney, Jr.
| distributor    = Metro-Goldwyn-Mayer
| released       = November 8, 1962
| runtime        = 178 min. (United Kingdom|UK:185 min.)
| country        = United States
| language       = English
| budget         = $19 million
| gross          = $13,680,000  . The Numbers. Retrieved June 13, 2013. 
}} Mutiny on HMAV Bounty Mutiny on John Gay and Ben Hecht). {{cite web
|url=http://www.imdb.com/title/tt0056264/fullcredits
|title=Full cast and crew for Mutiny on the Bounty
|publisher=IMDB
|accessdate=2011-03-29}}  The score was composed by Bronisław Kaper. 
 South Pacific. Behind the scenes, Marlon Brando effectively took over directing duties himself and caused it to become far behind schedule and over budget — resulting in director Carol Reed pulling out of the project and being replaced by Lewis Milestone who is credited as director of the picture.

==Plot==
In the year 1787, the Bounty sets sail from England for Tahiti under the command of captain William Bligh (Trevor Howard). Her mission is to transport breadfruit to Jamaica, where hopefully it will thrive and provide a cheap source of food for the slaves.
 patrician second-in-command, 1st Lieutenant Fletcher Christian (Marlon Brando). The tone for the months to come is summarized by Blighs ominous pronouncement that "cruelty with a purpose is not cruelty, it is efficiency." Aristocrat Christian is deeply offended by his ambitious captain.

Bligh attempts to reach Tahiti sooner by attempting the shorter westbound route around Cape Horn, a navigational nightmare. The strategy fails and the Bounty backtracks east, costing the mission much time. Singleminded Bligh attempts to make up the lost time by pushing the crew harder and cutting their rations.

When the Bounty reaches her destination, the crew revels in the easygoing life of the tropical paradise — and in the free-love philosophies of the Tahitian women. Christian himself is smitten with Maimiti (Tarita Teriipaia), daughter of the Tahitian king. Blighs agitation is further fueled by a dormancy period of the breadfruit: more months of delay until the plants can be transplanted. As departure day nears, three men, including seaman Mills, attempt to desert but are caught by Christian and clapped in irons by Bligh. 

On the return voyage, Bligh attempts to bring back twice the number of breadfruit plants to atone for his tardiness, and must reduce the water rations of the crew to water the extra plants. One member of the crew falls from the rigging to his death while attempting to retrieve the drinking ladle. Another assaults Bligh over conditions on the ship and is fatally keelhauled. Mills taunts Christian after each death, trying to egg him on to challenge Bligh. When a crewman becomes gravely ill from drinking seawater, Christian attempts to give him fresh water in violation of the Captains orders. Bligh strikes Christian when he ignores his second order to stop. In response, Christian strikes Bligh. Bligh informs Christian that he will hang for his action when they reach port. With nothing left to lose, Christian takes command of the ship and sets Bligh and the loyalist members of the crew adrift in the longboat with navigational equipment, telling them to make for a local island. Bligh decides instead to cross much of the Pacific in order to reach British authorities sooner and arrives back in England with remarkable speed. 

Christian sails back to Tahiti to pick up supplies and the girlfriends of the crew, then on to remote and wrongly charted Pitcairn Island to hide from the wrath of the Royal Navy. Once on Pitcairn, Christian decides that it is their duty to return to England and testify to Blighs wrongdoing and asks his men to sail with him. To prevent this possibility they set the ship on fire and Christian is fatally burned while trying to save it.

==Cast==
* Marlon Brando as 1st Lt. Fletcher Christian
* Trevor Howard as Capt. William Bligh
* Richard Harris as Seaman John Mills Alexander Smith
* Richard Haydn as Horticulturalist William Brown
* Tarita Teriipaia as Princess Maimiti Percy Herbert as Seaman Matthew Quintal
* Duncan Lamont as John Williams Gordon Jackson as Seaman Edward Birkett Michael Byrne Noel Purcell William McCoy
* Ashley Cowan as Samuel Mack John Fryer (Sailing Master)
* Tim Seely: Edward Ned Young (Midshipman)
* Frank Silvera as Minarii
* Henry Daniell as British chief court-martial admiral (uncredited)

==Reception==
===Box office performance=== box office 6th highest theatrical rentals. 

===Awards=== Oscars but was nominated for seven:   
*Academy Award for Best Picture – Aaron Rosenberg George W. Davis, Henry Grace, Hugh Hunt and J. McMillan Johnson Best Cinematography, Robert Surtees Best Effects, Special Effects – A. Arnold Gillespie (visual) and Milo B. Lory (audible) Best Film Editing – John McSweeney Jr. Best Music, Score – Substantially Original – Bronislaw Kaper Best Music, Song – Bronisław Kaper (music) and Paul Francis Webster (lyrics) – for the song "Love Song from Mutiny on the Bounty (Follow Me)"

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 