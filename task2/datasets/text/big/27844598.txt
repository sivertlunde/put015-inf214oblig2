Artham
{{Infobox film
| name           = Artham
| image          = Artham.jpg
| image_size     = Artham DVD cover
| caption        = VCD cover
| director       = Sathyan Anthikkad
| producer       = Suriya Chandralal
| screenplay     = Venu Nagavally
| based on       =  
| narrator       = Sreenivasan Murali Murali Saranya Saranya Jayaram Parvathy
| Johnson
| cinematography = Vipin Mohan
| editing        = K. Rajagopal
| studio         = Chanthu Films
| distributor    = Mudra Arts
| released       =  
| runtime        = 135 minutes
| country        = India Malayalam
}} crime drama film directed by Sathyan Anthikkad and written by Venu Nagavally. It stars Mammootty, Sreenivasan (actor)|Sreenivasan,  Murali (Malayalam actor)|Murali, Saranya Ponvannan|Saranya, and Jayaram in the main roles along with Parvathy Jayaram|Parvathy, Mamukkoya, Philomina, Mohan Raj, Thikkurissy Sukumaran Nair, Sukumari, Jagannatha Varma, and Oduvil Unnikrishnan in other pivotal roles. The music was composed by Johnson (composer)|Johnson. 
 Tamil detective novel Ethir Katru by Subha (writers)|Subha. The story revolves around a loner and award-winning novelist Ben Narendran (Mammootty) who seeks revenge against the forces who murdered his dear friend Janardanan (Jayaram). Joining in Narendrans aid are journalist Manasa (Saranya) and Advocate P. S. Nenmara (Sreenivasan). It was also later adapted in Tamil cinema as Ethir Kaatru (1990). 

==Plot==
The movie starts with a loner named Ben Narendran (Mammootty) writing a suicide note. He mentions that he is committing suicide as he has nothing to do with his life. He chooses to jump in front of the train to perform the act. He waits for the train but end up saving another young man named Janardanan (Jayaram) who has the same intention. Janardanan has committed a murder out of desperation and is scared of revenge. Narendran consoles him and offers him to take the blame of the murder so that Janardanan can live freely.

Narendran gets sentenced to lifetime imprisonment. While in jail, he writes a book under the pen name "Ben" which becomes immensely popular. He is also awarded by the state, but his true identity is never revealed. Journalist Manasa (Saranya) discovers that the author is behind bars and tries to get a parole for him. Though Narendran is not initially interested, he eventually applies for parole and is granted 28 days of parole so that he can receive the award in person.

Once out of jail, Narendran gets word that Janardanan has died mysteriously. He sets out on a mission to find the culprits and destroy those who caused his death.

==Cast==
*Mammootty as Ben Narendran Sreenivasan as Adv. P. S. Nenmara
*Saranya Ponvannan as Manasa Murali as R. K. Nambyar
*Jayaram as Janardanan Parvathi as Geetha
*Kollam Thulasi as Jaleel
*Mamukkoya as Kunjikannan
*Philomina as house owner
*Thikkurissy Sukumaran Nair as Janardanans father
*Sukumari as Janardanans mother
*Mohan Raj (Keerikkadan Jose) as Stanley
*Jagannatha Varma as Warrier
*Azeez as David
*Oduvil Unnikrishnan as Ananthan
*Karamana Janardanan Nair as John Zachariah
*Jagannathan as bookshop owner

==External links==
*  

 
 
 
 

 