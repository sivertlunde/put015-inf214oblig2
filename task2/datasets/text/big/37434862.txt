Stukas (film)
{{Infobox film
| name           = Stukas
| image          = Stukasposter.jpg
| image_size     = 
| border         = 
| alt            = A drawing of a Luftwaffe pilot
| caption        = Original film poster Karl Ritter
| producer       = 
| writer         = 
| screenplay     = {{Plainlist|
*Karl Ritter
*Felix Lützkendorf
}}
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist|
*Carl Raddatz Adolf Fischer
*Albert Hehn
}}

| music          =  , retrieved 26 October 2012   
| cinematography = 
| editing        = Conrad von Molo UFA
| distributor    = 
| released       =  
| runtime        = 99 mins
| country        = Germany
| language       = German RM Reiner Rother, "Stukas. Zeitnaher Film unter Kriegsbedingungen", in Krieg und Militär im Film des 20. Jahrhunderts, ed. Berhard Chiari, Matthias Rogg and Wolfgang Schmidt, Beiträge zur Militärgeschichte 59, Munich: Oldenbourg, 2003, ISBN 9783486567168, pp. 349&ndash;70,  . 
| gross          = 3.18 million RM 
}} Nazi propaganda Karl Ritter and starring Carl Raddatz, which follows three squadrons of Luftwaffe dive-bomber (Stuka) flyers.

==Plot and themes==
The plot largely alternates between combat and lulls in combat, Rother,  .  with the exception of two narratives. In one, three of the flyers who have been shot down behind enemy lines make their way back to the German position, finally succeeding after one of them manages to talk a French unit into capitulating.  In the other, a Posttraumatic stress disorder|shell-shocked flyer whose doctor has prescribed "a profound experience" recovers the will to fight when he hears "Siegfrieds Rhine Journey" during a performance of Wagners Götterdämmerung at the Bayreuth Festival. Rother,  .   Howard K. Smith, Last Train from Berlin, New York: Knopf, 1942,  ,  , quoted in Rolf Giesen, Nazi Propaganda Films: A History and Filmography, Jefferson, North Carolina / London: McFarland, 2003, ISBN 9780786415564,  .   (He has a flashback to his commander and the chief medical officer playing the same passage four-handed on the piano.   Laurence A. Rickels, Nazi Psychoanalysis Volume 3 Psy Fi, Minneapolis: University of Minnesota, 2002, ISBN 9780816637003,  . ) The film ends with them in flight on their way to attack England.

Stukas is an example of the Nazi contemporary film, or Zeitfilm, a type which Ritter, the scriptwriter and director, largely invented and championed as an answer to Russian revolutionary films.  The film was commissioned by the Luftwaffe and presents participation in war as a joy.   As a contemporary critic wrote, "Sheer enthusiasm transfigures the danger.... For  ,&nbsp;... fight is like intoxication, while for the squadrons captain of the cavaliers,&nbsp;... it is the elixir of life; for the captain of the Ninth,&nbsp;... it is spirit, distance, concentration."  Howard K. Smith wrote more disapprovingly in Last Train from Berlin: "It was a ... film about a bunch of obstreperous adolescents who dive-bombed things and people. They bombed everything and everybody. That was all the whole film was".  Hull, p. 188.  Michael Paris, From the Wright Brothers to Top Gun: Aviation, Nationalism, and Popular Cinema, Manchester/New York: Manchester University, 1995, ISBN 9780719040733,  .  The film emphasises "comradeship and self sacrifice"; Welch,  .  we are shown the young pilots learning to deal with comrades deaths for the greater good.   As one character says to another, "  doesnt really think about his comrades death any more, only about what they died for".  Like other Nazi war films, it makes heavy use of song; in a famous scene at the end, the squadron leader informs his pilots of their new mission against England and its dangers, we then see them seated in their aircraft, and the camera zooms in on their faces and then cuts to the clouds as they begin "ecstatic " to sing the "Stukaslied":  Always prepared and ready to attack
We the Stukas, Stukas, Stukas.
We dive from the sky
We advance on&mdash;to defeat England!    

The squadron members represent a range of types and backgrounds,  from various different parts of the Reich, shown united; additionally, in the flying scenes the pilots faces are photographed with a metallic greyish cast to suggest how they have become one with their aeroplanes. 

==Production and release== UFA in Babelsberg and around Berlin between 18 November 1940 and mid-February 1941.  As in Ritters previous film, Über alles in der Welt, miniatures and process photography were by Gerhard Huttula.  In order to show the Junkers 87 in as many of its combat applications as possible, documentary footage was included.  (Documentary footage from the wartime Bayreuth Festival, attended by large numbers of convalescing servicemen, was also used. ) The film was approved for release on 25 June 1941 and premièred on 27 June at the Ufa-Palast am Zoo in Berlin.  

==Reception==
Stukas was awarded four Prädikate (distinctions) by the   Value (volkstümlich wertvoll) and Value to Youth (Jugendwert). David Welch, Propaganda and the German Cinema: 1933&ndash;1945, Oxford: Oxford University/Clarendon, 1983, ISBN 9780198225980, p. 323.   .  Although it failed to achieve higher distinctions, it was praised for economical characterisation of the different airmen, excellence in casting and acting, and successful evocation of " heer enthusiasm transfigur  the danger&nbsp;... faith tak  away the fright of death."  Smith, on the other hand, dismissed it as "monotonous",    and modern critics regard it as a poor film, completely lacking in "elegance"; the non-combat sequences include a rowdy humour that was characteristic of the directors work,  and David Stewart Hull in his 1969 overview of Nazi cinema summed it up as "  all his worst vices: blatant propaganda, slapdash production values, crude editing, and a terrible script."  In his view, the final scene was "one of the silliest pieces of misguided propaganda ever conceived by the human mind".  Wolf Donner, in an essay published in 1995, described the ending as "absurd operetta of war".  Rainer Rothers assessment in his essay on the film, published in 2003, was that the episodic structure and avoidance of depiction of deaths have a dramatically flattening effect so that "the experience of war   virtually oscillates between a camping trip and symbiosis with the  ."  Despite the edict banning criticism and replacing it with reportage, even some contemporary reviewers noted the fast-paced and episodic nature of the film. One questioned the narrative logic of the Bayreuth cure.  Another spoke of "almost violent impetuosity" and a third noted that the action was "steeped in the soldierly, often filled to bursting point".  On the other hand Erhard Schütz, in a piece published in 2008, regarded the structural focus on attack sequences as "the film present  itself as an experience of audiovisual intoxication with suggestively intensified repetition." Erhard Schütz, "When Everything Falls to Pieces&mdash;Rubble in German Films before the Rubble Films", in German Postwar Films: Life and Love in the Ruins, ed. Wilfried Wilms and William Rasch, Studies in European Culture and History, New York: Palgrave Macmillan, 2008, ISBN 9780230608252, pp. 7&ndash;25,  .  
 RM in the seven months prior to January 1942 on costs of 1,961,000 RM.  The declining fortunes of the Luftwaffe made it "the last major   aviation film". 

Stukas is classified by the Friedrich Wilhelm Murnau Foundation as a Vorbehaltsfilm (controlled film), meaning that in Germany it may only be screened under specific conditions for educational purposes. 

==References==
 

==Further information==
* "Hitler and the Wagner Clan", Wagner&mdash;Forging the Ring, BBC Four documentary, broadcast 9 March 2007: includes the Bayreuth episode with subtitles
* Daniel Gethmann. Das Narvik-Projekt: Film und Krieg. Literatur und Wirklichkeit 29. Bonn: Bouvier, 1998. ISBN 9783416027786  : extensive treatment of war films by Ritter and by Veit Harlan

==External links== Czech subtitles)
*   IMDb  


 
 
 
 
 
 