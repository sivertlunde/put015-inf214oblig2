The Wise Quacking Duck
{{multiple issues|
 
 
}}

{{Infobox Hollywood cartoon
| series = Looney Tunes (Daffy Duck)
| image =
| caption =
| director = Robert Clampett
| story_artist =
| animator =   Robert McKimson Virgil Ross Rod Scribner Sidney Sutherland A. C. Gamer (effects animator)
| voice_actor = Mel Blanc (uncredited)
| musician = Carl W. Stalling
| producer = Leon Schlesinger Leon Schlesinger Productions
| distributor = Warner Bros. Pictures The Vitaphone Corporation
| release_date = May 1, 1943 (USA) Technicolor (three-color)
| runtime = 6 minutes
| movie_language = English
}}

The Wise Quacking Duck is a 1943 Looney Tunes cartoon released by Warner Bros. on May 1, 1943. It stars Daffy Duck.

==Synopsis==
The cartoon begins with Mr. Meek carrying an axe in his hands. He turns to the audience and explains that his wife, Sweetypuss, told him that if he didnt bring home a roast duck for dinner, she would cook   goose.
The scene cuts to Daffy eating corn while singing "I Dream of Jeanie with the Light Brown Hair" minding his business. A shadow of Meek is show getting ready to chop off Daffys head. Then he jumps and shout "Watch it, Bub!" directly in his face.
After a brief talk to Meek, he flicks his beak at his face and marches to a hay stack. Meek chops the stack various times and on the last chop, he thinks he kills the duck. Daffy fakes his death by squirting ketchup and throwing his feathers around ("You crushed my little head!") This fools Meek by pretending he is beheaded and runs rapidly.
Meek feels sad and goes back to his house, where Daffy is seen putting lots of sugar cubes into a cup of coffee. Meek guiltily tells him that he has killed a duck. Daffy cheers up Meek into giving him a cup of coffee and asks how many lumps   his wife usually gives him. Meek shows Daffy a lump (a bump on his head), and Daffy hits him on the head (giving him another lump) and pours cream on it. Daffy dances on the cakes and pies, singing "Shortening Bread", and Meek immediately recognizes the duck he thought he killed ("Say. Thats that Daffy Duck.") Daffy throws a pie at him, leading into a war between himself and Meek.
Daffy goes into army plane mode, by dropping an egg on Meek as a "Secret Bomb Site." Next, he goes into battle weaponry mode, by making various sounds and smashing household items. After all the fun and games are over, Meek gets mad, which causes the egg to fry on his head.
Daffy runs off, but is caught by Meek with a shotgun. He puts it up to Daffys face and orders him to get in the oven. Daffy goes into his stripper mode and performs a striptease to the tune "It Had to be You." Jerry Colonna). Going back to the curve run scene, Meek points his gun to Daffys face again ("No no. Not twice in a same picture.") Meek blows the feathers off him and throws him in the oven. When he hears Daffy cries he fells guilty and opens the door. The cartoon ends with Daffy bathing himself in gravy ("Say. Now youre cooking with gas.")

==Availability==
The short is presented uncut on the   in Disc 3.

 
 
 
 
 