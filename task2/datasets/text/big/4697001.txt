A Car-Tune Portrait
{{Infobox film
| name           = A Car-Tune Portrait
| image          = 
| alt            =  
| caption        = 
| director       = Dave Fleischer
| producer       = Max Fleischer
| writer         = Dave Fleischer Isadore Sparber Dave Tendlar
| starring       = David Ross
| music          = 
| cinematography = 
| editing        = 
| studio         = Fleischer Studios
| distributor    = Paramount Pictures
| released       =  
| runtime        = 7 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
A Car-Tune Portrait is a cartoon in the Color Classics series produced by Fleischer Studios. Released on June 26, 1937,  the cartoon gives an imaginative take on Franz Liszts Hungarian Rhapsody No. 2. 

==Plot==
The cartoon features a lion dressed up as a musical conductor, attempting to keep his orchestra of animal musicians in order as they half-play, half-fight their way through the piece. Memorable moments include a Dachshund playing the xylophone using his back legs while the rest of him sleeps, a group of monkeys using a flute as a pea-shooter to fire at their fellow musicians, and a horse trombonist who attempts to swat a fly using his instrument but who only succeeds in hitting the dog trumpeter in front of him. 

In keeping with the building frenzy of Liszts rhapsody, the animals become more and more violent, playing pranks on each other and generally wreaking havoc; but still the piece goes on. The final scenes see the lion conductor smashed over the head with a giant bass drum, at which point he gives in, the music finishes and the cartoon ends.

==See also==
*Song Car-Tunes, 1924-1926 series of cartoons produced by Fleischer Studios and released by Red Seal Pictures.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 