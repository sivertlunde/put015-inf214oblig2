Hoffmeyer's Legacy
 
{{Infobox film
| name           = Hoffmeyers Legacy
| image          = 
| image_size     = 
| caption        = 
| director       = Mack Sennett
| producer       = 
| writer         = 
| narrator       = 
| starring       = Ford Sterling The Keystone Cops
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Hoffmeyers Legacy is a 1912 comedy short directed by Mack Sennett and notable for being the first Keystone Cops comedy. However, many consider the first real Keystone Cop comedy to be The Bangville Police (1913).

==Cast==
* Ford Sterling ... Hoffmeyer
* Alice Davenport ... (unconfirmed supporting role)
* Charles Avery ...	Keystone Kop
* Bobby Dunn ... Keystone Kop
* Chester M. Franklin ... Man in police station
* George Jeske ... Keystone Kop	
* Fred Mace	... Hoffmeyers wife (in drag)
* Edgar Kennedy ...	Keystone Kop
* Hank Mann	... Keystone Kop
* Mack Riley ... Keystone Kop
* Mack Sennett ... Police Chief
* Slim Summerville ... Keystone Kop

==External links==
*  

 
 
 
 
 
 
 
 
 


 