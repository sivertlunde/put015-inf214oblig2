Garfield: A Tail of Two Kitties
{{Infobox film
| name        = Garfield: A Tail of Two Kitties
| image       = Garfield A Tail of Two Kitties.jpg Tim Hill John Davis Executive Producer: Michele Imperato Brian Manis Joel Cohen Alec Sokolow
| based on    =  
| narrator    = Roscoe Lee Browne
| starring    = Breckin Meyer Jennifer Love Hewitt Billy Connolly Bill Murray (voice) Tim Curry (voice)
| music       = Christophe Beck
| cinematography = Peter Lyons Collister
| editing      = Peter S. Elliot Davis Entertainment Company Paws, Inc.
| distributor = 20th Century Fox
| released    =  
| runtime     = 85 minutes
| language    = English
| country     = United Kingdom United States
| awards      =
| budget      = $60 million
| gross       = $141.7 million 
}}

Garfield: A Tail of Two Kitties (also known as Garfield 2: The Prince and the Paw-per in the    . This film was  , by The Game Factory.

== Plot == Garfield (voiced by Bill Murray) and Odie sneak into Jons luggage and join him on the trip. Garfield and Odie break out of the hotel room, then get lost.

Meanwhile, at Carlyle Castle in the British countryside, the late Lady Eleanors will is read. She leaves all of Carlyle Castle to Prince XII (voiced by Tim Curry), her beloved cat who looks just like Garfield. This upsets the Ladys nephew, Lord Dargis (played by Billy Connolly), who will now only get the grand estate once Prince is out of the picture. Lord Dargis traps Prince in a picnic basket and throws him into the river.

Garfield inadvertently switches places with Prince: Jon finds Prince climbing out of a drain and takes him to the hotel, while Princes butler Smithee finds Garfield in the street and takes him to Carlyle Castle.

In the grand estate Garfield now calls home, he receives the royal treatment, including a butler and an international array of four-legged servants and followers. Garfield teaches his animal friends to make lasagna, while Prince learns to love it at Jons place. Lord Dargis sees Garfield and thinks Prince has come back - if the lawyers see Prince/Garfield they will not sign the estate over to Dargis, who secretly wants to destroy the barnyand and evict/kill the animals to build a country spa. Dargis makes many attempts to get rid of Garfield, one involving a vicious but dim Rottweiler, Rommell (voiced by (Vinnie Jones).
 mirror gag). Jon, with the help of Odie, discovers the mix-up and goes to the castle, which coincidentally Liz is visiting.

Garfield and Prince mess with Dargis, whose plan is exposed, and are seen by the lawyers. Dargis threatens everyone if they dont sign the papers to him, taking Liz hostage. Garfield, Prince, Odie and Jon save the day, Smithee alerts the authorities, and Dargis is arrested. Garfield, who had been trying to stop Jon from proposing to Liz, has a change of heart: He helps Jon in proposing, and she does accept.

==Cast==
* Breckin Meyer as Jon Arbuckle, Odie and Garfield (character)|Garfields owner
* Jennifer Love Hewitt as Dr. Liz Wilson
* Billy Connolly as Lord Dargis
* Ian Abercrombie as Smithee
* Roger Rees as Mr. Hobbs
* Lucy Davis as Ms. Abby Westminister
* Oliver Muirhead as Mr. Greene

===Voice cast===
* Bill Murray as Garfield (character)|Garfield, the movies main protagonist Garfield
* Bob Hoskins as Winston
* Rhys Ifans as McBunny
* Vinnie Jones as Rommel
* Joe Pasquale as Claudius
* Richard E. Grant as Preston
* Jane Leeves as Eenie
* Roscoe Lee Browne as the Narrator

==Production==
The exterior castle scenes were shot at Castle Howard in Yorkshire, England and some of the interior scenes like the main staircase were at the Greystone Mansion in Beverly Hills, California, and most film scenes were shot at London.

==Reception==
Garfield: A Tail of Two Kitties received mostly negative reviews from film critics, doing no better than its original. On Rotten Tomatoes, the film holds an 11%, based on 73 reviews, with an average rating of 3.5/10. The sites consensus reads, "Strictly for (very) little kids, A Tale of Two Kitties features skilled voice actors but a plot that holds little interest."  On Metacritic, the film has a score of 37 out of 100, based on 20 reviews, indicating "generally unfavorable reviews". 

===Box office===
The film grossed $28,426,747 in the U.S. Box Office (the   made this in its first six days).  According to 20th Century Fox, the studio was aware that the film would not make as much as the first, and only made it based on the worldwide success of the first film. 
It became a huge success overseas, earning $113,011,707, or 79.9% of its worldwide gross.

==Home video release==
*The DVD was released on October 10, 2006. The DVD includes a "Drawing with Jim Davis" featurette and two games: Garfields Maze, and Odies Photo Album.  It also includes a music video, trailers, and footage not seen in theaters.
*Extended edition from Garfield: A Tail of Two Kitties Extended edition DVD in   Follow Along DVD in January 1, 2008.

==Awards==
The film was nominated for two Golden Raspberry Awards in 2006 Golden Raspberry Awards|2006, one in the category "Worst Prequel or Sequel", and one in the category "Worst Excuse for Family Entertainment", but lost to Basic Instinct 2 and RV_(film)|RV, respectively.

==References==
 
 

==External links==
 
*  
*  
*  
*  
*   at   (Requires  )

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 