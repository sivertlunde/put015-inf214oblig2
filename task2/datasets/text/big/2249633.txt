The Flying Saucer
 
{{Infobox film
| name           = The Flying Saucer
| image          = Poster of the movie "The Flying Saucer".jpg Theatrical release poster
| director       = Mikel Conrad
| producer       = Mikel Conrad
| writer         = Howard Irving Young Mikel Conrad
| narrator       =
| starring       = Mikel Conrad
| music          = Darrell Calker
| cinematography = Phillip Tannura
| editing        = Robert Crandall
| studio         = Colonial Productions, Inc.
| distributor    = Film Classics Inc.
| released       =  
| runtime        = 75 minutes 
| country        = United States
| language       = English
| budget         =
}}
 independently made American black-and-white science fiction film, written by Howard Irving Young from an original story by Mikel Conrad who also produced, directed, and stars with Pat Garrison and Hantz von Teuffen. The film was distributed in the United States by Film Classics Inc.

The Flying Saucer is the first feature film to deal with the (then) new and hot topic of   science fiction film Earth vs. the Flying Saucers, released by Columbia Pictures.

==Plot== Soviet spies the Alaskan Russell Hicks) Secret Service agent in exploring that area to discover what the Soviets may have found.

To his pleasant surprise, Mike discovers the agent is an attractive woman named Vee Langley (Pat Garrison); they set off together and slowly become mutually attracted to each other. Their cover story is that Mike is suffering from a nervous breakdown and she is his private nurse. At Mikes familys wilderness lodge, they are met by a foreign-accented caretaker named Hans (Hantz von Teuffen), new to the job.

Mike is very skeptical of the flying saucer reports until he spots one flying over the lodge. Assorted complications ensue until Mike and Vee finally discover that Hans is one of the Soviet agents who is trying to acquire the flying saucer, hidden at Twin Lakes. It turns out that the saucer is an invention of American scientist Dr. Laughton (Roy Engel). But Turner (Denver Pyle) Laughtons assistant, a communist sympathizer, has other ideas and tries to make a deal to sell the saucer to the Soviets for one million dollars.

Mikes trip to Juneau to see old friends including Matt Mitchell (Frank Darrien), is ill-advised and when Vee tracks him down, he is in the company of a bar girl, Nanette (Virginia Hewitt).  Matt gets mixed up with the Soviet agents who are trying to obtain control of the saucer. When he tries to strike a bargain with ring leader Colonel Marikoff (Lester Sharpe) at the spys headquarters, Matt is knocked unconscious. 

Matt is able to escape and seeks out Mike, but they are attacked by Soviet agents, who kill Matt. Before he dies, Matt reveals the location of the saucer. Mike rents an aircraft, flies to Twin Lakes where the saucer is hidden at an isolated cabin. When he flies back to his lodge, he tries to find Vee who has tried to spirit Lawton away, but all of the trio are captured by the turncoat Taylor and a group of Soviet agents. The Soviets lead their prisoners through a secret tunnel under the glacier, an avalanche, however, wipes out the agents. Mike, Vee and Lawton escape from the tunnel in time to see Turner fly off in the saucer, which explodes in mid-air, due to a bomb that Lawton planted on board. Their mission accomplished, Mike and Vee embrace and kiss.

==Cast==
 
* Mikel Conrad as Mike Trent
* Pat Garrison as Vee Langley
* Hantz von Teuffen as Hans
* Roy Engel as Dr. Lawton
* Lester Sharpe as Col. Marikoff
* Denver Pyle as Turner, a spy
* Earl Lyon as Alex, a spy
* Frank Darrien as Matt Mitchell Russell Hicks as Intelligence Chief Hank Thorn
* Virginia Hewitt as Nanette, bar girl
* Garry Owen as Bartender
 

==Production== Hal Roach Studios.  Additional B-roll photography was shot in Alaska on location where, according to a September 21, 1949 article in the Los Angeles Examiner, Mikel Conrad claimed to have obtained footage of actual flying saucers while shooting Arctic Manhunt in Alaska in the winter of 1947.  

The opening prologue appears before the onscreen credits and states: "We gratefully acknowledge the cooperation of those in authority who made the release of the Flying Saucer film possible at this time." The message obliquely alluded to some authorized government films of flying saucers that may have existed. None of that footage was actually used in The Flying Saucer.   Turner Classics Movies. Retrieved: January 8, 2015. 

==Re-release==
In 1953, The Flying Saucer was re-released in the USA by Realart Pictures Inc., on a double-bill with Atomic Monster, the retitled-reissue of Man Made Monster, originally released in 1941 by Universal Pictures.

==Reception== B film origins, and its tacky production values doomed it to the lower end of bills and drive-ins. Bosley Crowther, the noted film critic on The New York Times was reluctant to "blast away". "A film called "The Flying Saucer" flew into the Rialto yesterday and, except for some nice Alaskan scenery, it can go right on flying, for all we care. In fact, it is such a clumsy item that we doubt if it will go very far, and we hesitate, out of mercy, to fire even a critical shot at it." 

==References==
Notes
 
Bibliography
 
* Strick, Philip. Science Fiction Movies. London: Octopus Books Limited, 1976. ISBN 0-7064-0470-X.
* Warren, Bill. Keep Watching the Skies: American Science Fiction Films of the Fifties, 21st Century Edition. Jefferson, North Carolina: McFarland & Company, 2009. ISBN 0-89950-032-3.
 

==External links==
*  
*  
*  

 
 
 
 
 