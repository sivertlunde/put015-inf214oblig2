Les Acteurs
{{Infobox film
| name           = Les Acteurs
| image          = 
| caption        = 
| director       = Bertrand Blier
| producer       = Christine Gozlan Alain Sarde
| writer         = Bertrand Blier
| based on       =  Maria Schneider Michel Serrault Jacques Villeret Jean Yanne
| music          = Martial Solal
| cinematography = François Catonné
| editing        = Claudine Merlin
| distributor    = Bac Films
| studio         = Canal+ TF1 Films Production Les Films Alain Sarde Planete A
| released       =  
| runtime        = 103 minutes
| country        = France
| language       = French
| budget         = $10,7 million
| gross          = $3,115,702 
}}
Les Acteurs (The Actors)  is a 2000 French comedy film directed by Bertrand Blier. 

==Plot==
Some portraits of actors (exclusively men, with the exception of Josiane Balasko interpreting ... André Dussollier) who meet and tell their stories in an organized or incidentally. They question with a certain distance and irony on their craft.

==Cast==
 
* Pierre Arditi as Himself
* Josiane Balasko as André Dussollier 2
* Jean-Paul Belmondo as Himself
* François Berléand as François Nègre
* Dominique Blanc as Geneviève
* Claude Brasseur as Himself
* Jean-Claude Brialy as Himself
* Alain Delon as Himself
* Gérard Depardieu as Himself
* Albert Dupontel as The cop
* André Dussollier as Himself
* Jacques François as Himself
* Sami Frey as Himself
* Michel Galabru as The killed actor
* Ticky Holgado as The tramp
* Michael Lonsdale as The man on the street
* Jean-Pierre Marielle as Himself
* Patachou as The blind lady
* Michel Piccoli as Himself
* Claude Rich as Himself Maria Schneider as Herself
* Michel Serrault as Himself
* Jean Topart as The filmmaker
* Jacques Villeret as Himself
* Jean Yanne as Dr. Belgoder
* Bertrand Blier as Himself
* François Morel (actor)|François Morel as The man with the autograph
* Michel Vuillermoz as The nurse
* Marie-Christine Adam
* Éric Prat
 

==Production==
The movie was screen to the Mar del Plata International Film Festival in Argentina in 2001.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 
 