The Flaming Crisis
{{multiple issues|
 
 
}}
{{Infobox film
| name           = The Flaming Crisis
| image          =  File:1924 The Flaming Crisis poster.jpg
| caption        = American Theatrical Release Poster 1924
| director       = William H. Grimes Leo C. Popkin
| producer       = Lawrence Goldman
| writer         = William H. Grimes Henry Dixon
| music          = 
| cinematography = 
| editing        = William H. Grimes
| distributor    = Monarch Films
| released       =  
| runtime        = 
| country        = United States
| language       = Silent (English intertitles)
}}
The Flaming Crisis is a 1924 silent romance Western film written and directed by William H. Grimes.

==Plot==
A young black newspaperman is convicted of murder on circumstantial evidence and sentenced to prison. He escapes and makes his way to the southwestern cattle country, where he falls in love with Tex Miller, a beautiful cowgirl. Having rid the territory of an outlaw band, he gives himself up to the law, thinking that he will be sent back to prison. After discovering that the real murderer has confessed, he returns to Tex and the country he has come to love.

==Cast==
*Calvin Nicholson as The  Newspaperman
*Dorothy Dunbar as Tex Miller
*Henry Dixon as Mark Lethler

==External links==
*  

 
 
 
 
 
 
 
 
 


 
 