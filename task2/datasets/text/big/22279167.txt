A Report on the Party and the Guests
 
{{Infobox film
| name           = A Report on the Party and the Guests
| image          = A Report on the Party and the Guests.jpg 
| caption        = Film poster
| director       = Jan Němec Jan Procházka
| writer         = Ester Krumbachová Jan Němec Ivan Vyskočil
| music          =
| cinematography = Jaromír Sofr
| editing        = Oldřich Bosák
| studio         = Filmové Studio Barrandov Sigma III
| released       =  
| runtime        = 71 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         =
}}

A Report on the Party and the Guests ( , also known in English as The Party and the Guests) is a 1966 Czechoslovak drama film directed by Jan Němec. It was entered for the 1968 Cannes Film Festival,    but the festival was aborted owing to the events of May 1968 in France.

==Cast== Ivan Vyskočil as host
* Jan Klusák as Rudolf
* Jiří Němec (actor)|Jiří Němec as Josef
* Pavel Bošek as František
* Karel Mareš as Karel
* Evald Schorm
* Jana Prachařová
* Zdena Škvorecká as Eva
* Miloň Novotný as groom
* Helena Pejsková as Marta
* Dana Němcová as bride Olinka
* Antonín Pražák as Antonín
* Josef Škvorecký as Guest

==References==
 

==External links==
*  
* 

 
 
 
 
 
 
 
 
 
 
 