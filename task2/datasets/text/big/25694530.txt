The Perfect Stranger (film)
{{Infobox film
| name           = The Perfect Stranger
| image          = The Perfect Stranger (film).jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Jefferson Moore Shane Sooter
| producer       =  David Gregory (novel) Jefferson Moore (screenplay)
| narrator       =  Tom Luce
| music          = Mark Noderer Brian Sites
| cinematography = Kevin Bryan Pate Walters
| studio         = Kellys Filmworks
| distributor    = Kellys Filmworks LTD
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = $4,500,000
}} Independent Christian David Gregory.  It was featured at the 2005 Western Film and Video Festival,      and was released on October 28, 2005. Directed by Jefferson Moore and Shane Sooter,  the film starred Pamela Brumley and Jefferson Moore.

== Plot == married life. crazy and large spike scars on the mans wrists, and her life has been changed forever.  He says that she was the one who sent for him, years before. When Nikki gets home, she finds her husband there, and Sarah (her daughter) in bed.

== Cast ==
 
* Pamela Brumley as Nikki Cominsky
* Jefferson Moore as The Stranger Tom Luce as Eduardo
* Dennis Martin as Matt Cominsky
* Stella Davis as Sarah Cominsky
 

== Production ==
The film was shot on location in Louisville, Kentucky|Louisville, Kentucky and neighboring Jeffersonville, Indiana|Jeffersonville, Indiana.  

== Sequels and Spinoffs ==

The movie is followed by two sequels:  Another Perfect Stranger  (2007) and Nikki and the Perfect Stranger (2013) and a spinoff movie The Perfect Gift (2009).

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 