The Heart of Jennifer
{{Infobox film
| name           = The Heart of Jennifer
| image          =
| alt            = 
| caption        = 
| director       = James Kirkwood, Sr.
| producer       = Daniel Frohman
| story          = Edith Barnard Delano 	
| starring       = Hazel Dawn James Kirkwood, Sr. Irene Howley Russell Bassett Harry C. Browne
| music          = 
| cinematography = 
| editing        = 
| studio         = Famous Players Film Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by James Kirkwood, Sr. and written by Edith Barnard Delano. The film stars Hazel Dawn, James Kirkwood, Sr., Irene Howley, Russell Bassett and Harry C. Browne.   The film was released on August 30, 1915, by Paramount Pictures.

==Plot==
 

== Cast == 	
*Hazel Dawn as Jennifer Hale
*James Kirkwood, Sr. as James Murray
*Irene Howley as Agnes Murray
*Russell Bassett as Mr. Hale, Jennifers Father
*Harry C. Browne as Stephen Weldon

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 