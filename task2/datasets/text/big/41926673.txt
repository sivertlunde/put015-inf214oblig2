A Celebrated Case
{{Infobox film
| name           = A Celebrated Case
| image          = 
| image_size     = 
| caption        = 
| director       = George Melford?
| producer       = 
| writer         = Gene Gauntier (scenario)
| story          = 
| based on       =  
| narrator       = 
| starring       = Alice Joyce Guy Coombs Marguerite Courtot
| music          = 
| cinematography = 
| editing        = 
| studio         = Kalem Company
| distributor    = General Film. Co., Special Features Dept.
| released       =  
| runtime        = Four reels
| country        = United States
| language       = Silent
| budget         = 
| gross          = 
}}
A Celebrated Case is a 1914 American silent drama film starring Alice Joyce, Guy Coombs and Marguerite Courtot. It is based on the 1877 play Une cause célèbre by Adolphe Philippe Dennery and Eugene Cormon. A French soldier is wrongfully sentenced to the galleys for the murder of his wife.

It is considered to be a lost film.   

==Cast==
*Alice Joyce as Madeline Renaud
*Guy Coombs as Jean Renaud
*Marguerite Courtot as Adrienne
*James B. Ross as Adriennes Father
*Harry F. Millarde as Lazare
*Alice Hollister

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 