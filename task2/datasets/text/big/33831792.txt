Run, Man, Run
{{Infobox film
 | name =  Run, Man, Run
 | image =  Run, Man, Run!.jpg
 | caption =
 | director =  Sergio Sollima
 | writer = Donald OBrien John Ireland Chelo Alonso
 | music =   Bruno Nicolai
 | cinematography =
 | editing =
 | producer =
 | distributor =
 | released =  	29 August 1968
 | runtime =
 | awards =
 | country =
 | language =
 | budget =
 }} Italian Zapata Western|Zapata-themed spaghetti western movie. It is the second movie of Sergio Sollima centered on the character of Cuchillo, again played by Tomas Milian, after the two-years earlier successful western The Big Gundown. It is also the final chapter of the political-western trilogy of Sollima, and his last spaghetti western.  According the same Sollima, Run, Man, Run is the most politic, the most revolutionary and even anarchic among his movies.   

==Plot==
When Cuchillo returns to his hometown in Mexico he soon finds himself in prison, sharing a cell with desperado Ramirez. Thus hes present when Ramirez breaks out. He accompanies his new buddy who is killed little later by bandits. Cuchillo learns that Ramirez once rode with Benito Juárez. The killers believe Ramirez knew the whereabouts of a 3 million dollars hidden by Juarez. Now that Ramirez is dead they presume he has bequeathed his secret to Cuchillo. But Cuchillo makes it hard on them to catch him.

== Cast ==
* Tomas Milian: Manuel Sanchez / Cuchillo Donald OBrien: Nathaniel Cassidy John Ireland: Santillana
* Chelo Alonso: Dolores
* Linda Veras: Penny Bannington
* Marco Guglielmi: Colonel Michel Sévigny
* José Torres (actor)|José Torres: Ramirez Edward Ross: Jean-Paul
* Nello Pazzafini: Riza
* Gianni Rizzo: The Mayor Dan May: Mateos Gonzalez
* Noé Murayama: Pablo
* Attilio Dottesio: Manuel Etchevaria
* Orso Maria Guerrini: Raul
* Federico Boido: Steve Wilkins
* Calisto Calisti: Fernando Lopez

== Music ==
For contractual reasons, Nicolai is credited with the films music, but Ennio Morricone actually composed it. 

==References==
 

==External links==
* 
 
 
 
 
 
 
 

 