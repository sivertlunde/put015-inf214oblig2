The Exquisite Cadaver
{{Infobox Film
| name           = The Exquisite Cadaver
| image        = Las Crueles.jpg
| image_size     =
| caption        = 
| director       = Vicente Aranda 
| producer      =  Sidney Pink   Stanley Abrams   Carlos Durán
| screenplay = Vicente Aranda   Antonio Rabinad
| based on =  The short story Bailando para Parker by Gonzalo Suárez
| starring       =  Capucine   Carlos Estrada   Judy Matheson   Teresa Gimpera 
| music =  Marco Rossi 
| cinematography = Juan Amorós   
| editing = Maricel Bautista   Bautista Treig  
| distributor = Morgana Films 
| released       =  25 August 1969     January 1973  
| budget         = 
| runtime        = 108 minutes
| country        = Spain
| language       = Spanish
|}} Spanish art house exploitation film directed by Vicente Aranda, based on the short story Bailando Para Parker written by Gonzalo Suárez. Torres,  Diccionario del cine Español, p. 160  The plot follows a well-to-do publisher and family man who begins to receive severed body parts in the mail two years after his mistress committed suicide. Along with one of these bloody parcels is a letter of blackmail from the dead womans former lesbian lover, who seeks vengeance.

==Plot==
A girl is seen slowly and deliberately laying her head down on railroad tracks as an oncoming train approaches. Two years later, Carlos, a well-to-do family man and publisher of pulp horror novels receives an anonymous yellow package containing a severed human hand. He buries it in a nearby park. The next yellow package he receives is left unopened on a bench in the city. However, when he arrives home the package is awaiting him. This one contains a torn-up dress and a photograph of a girl. His beautiful wife reads him a telegram asking if he would like a forearm. He feebly attempts to lie about the contents with a work-related explanation to his wife. Now suspicious, Carlos’s wife follows her husband and spots a mysterious woman in black following him as well.
 
Without a word, Carlos enters the mysterious womans car. She drives him to her remote home, where she feeds him lysergic acid embedded in red blotting paper. Suddenly he is ambling down a long corridor drawn towards a womans voice lamenting her lost love. He reaches the end of the hallway to discover the voice emanating from a tape recorder. He finds a womans body in a refrigerator curled up, pale, but immaculate. When he awakens from the drugged stupor the editor is back at home, his body covered in a jaundiced yellow. His wife explains to him that she received a call from Parker, the mysterious woman, and she went to her house to fetch him. The wife believes neither the explanation given by her husband nor the one she receives from Parker about what has happened.

Trying to come clean with his wife, the editor tells her how two years ago he met the young woman of the refrigerator. Her name was Esther. In a flashback Esther is seen in a coffee shop fiddling with some pills. She appears bored, yet eager to flirt with the older publisher. Shortly afterwards they became lovers. Then Carlos and Esther, now a couple, are seen out near the sea. As she edges toward the cliff, she says, "Id die so that my love for you will last. So that indifference will not kill it". 

Calmly the editors wife confesses that she knew some of it all along. She had hired a detective to spy on her husband. In fact it was the detective who saved Esther when she put her head on the train track. Through the collective memories of the publisher, his wife, and Parker, the corrosion of the romance is recounted.

Sick and heartbroken after she was rebuffed by the editor, Esther fell under the spell of a scheming doctor claiming to cure leukemia. While still under his influence Esther met Parker in a hotel in Paris. In love with her, Parker rescued Esther from the false healer. However, Esther never fully recovered from her ill-fated affair with the editor. After some suicide attempts one day Parker found Esther dead with an empty bottle in her hand. Parker now planned to avenge her deceased lover. She started to send the macabre yellow packages. The last of which contains Esthers severed head. The editor then called the police, but when they arrive to Parkers house to investigate she has left with her driver heading back to Paris. Seduced by Parker, the editor’s wife goes inside Parkers car and leaves with her.

==Cast==
The film has an international cast headed by the French actress Capucine, the Argentinian Carlos Estrada, the British Judy Matheson and the Spanish model and actress Teresa Gimpera. Torres,  Diccionario Espasa Cine Español,  p. 161 
*Capucine as Parker (Lucia Fonte)
*Carlos Estrada as Editor
*Teresa Gimpera as Editors wife
*Judy Matheson as Esther
*José María Blanco as the writer
*Alicia Tomás as the secretary
*Luis Ciges as functionary
*Joaquín Vilar as child

==Production==
After his previous films were met with indifference by critics and audiences, for his third project Spanish film director Vicente Aranda took a commercial approach mixing fantastic and erotic overtones in The Exquisite Cadaver. The film was plagued by a series of problems: it was long in the making; Aranda suffered an accident during the shooting, which forced him to work lying down on a stretcher  Vera, Vicente Aranda, p. 66  and finally he had a legal battle with the producers. Cánovas, Miradas sobre el cine de Vicente Aranda, p. 54  It would take Aranda many years to recover ownership of this film. The experience led him to found his own production company. Cánovas, Miradas sobre el cine de Vicente Aranda, p. 55 

Aranda had bought the film rights of Gonzalo Suárezs book Trece veces Trece (Thirteen Times Thirteen) that included the short story Bailando Para Parker (Dancing for Parker). Vera, Vicente Aranda, p. 60  Aranda and Suárez fell out over Fata Morgana, a previous project directed by Aranda and written by Suárez, because of Suárezs dislike of the resulting film.  

Long in the making there were at least five different scripts and the films ending varies from its original source: Bailando Para Parker ends in a trial that is not in The Exquisite Cadaver. Vera, Vicente Aranda, p. 63  Aranda found inspiration for the script in the letters of Mariana Alcoforado, including the theme of her famous letters : To die for love  which is used in the sequence when the editor discovers the cadaver of his former lover. Production took off when American producers showed interest in the project and offered to co-produce the film, selling it in advance to the American market. 

==Title==
The Exquiste Cadaver was the original title of the film, but the producers rejected it, thinking that it had little commercial appeal. Vera, Vicente Aranda, p. 61  Aranda then proposed to call the film Las Crueles, a title that fitted the plot, and was chosen when it was released.   Years later when the film was to be broadcast on television, Aranda handed down a video transfer that still bore the original title The Exquisite Cadaver and that was also the title used when it was released on VHS. From then on the film has been better known by its original title. 

==Analysis== Hitchcockian in its development of intrigue and its macabre humor, but in the second half the mystery is explained in detail. 

==Reception==
The Exquisite Cadaver was one of four films made at the end of the 1960s based on stories written by Gonzalo Suárez.  Antonio Ecera directed Cuerpo Presente (1965) for producer Elias Querejeta. Suárez himself wrote, produced and directed Ditirambo (1967) and Vicente Aranda made Fata Morgana and The Exquiste Cadaver.  Of these four films The Exquisite Cadaver was the only one that found an audience.

==Notes==
 

==References==
*Cánovás, Joaquín (ed.), Varios Autores: Miradas sobre el cine de Vicente Aranda, Murcia: Universidad de Murcia, 2000, ISBN 84-607-0463-7
* Torres, Augusto, Diccionario del cine Español, Espasa Calpe, 1994, ISBN 84-239-9203-9

==External links==
* 
* 

 

 
 
 
 
 
 