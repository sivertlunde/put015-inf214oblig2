Free and Easy (1941 film)
{{Infobox film
| name           = Free and Easy
| image          = 
| image_size     = 
| caption        = 
| director       = George Sidney Edward Buzzell (uncredited)
| writer         = 
| narrator       = 
| starring       = Robert Cummings Ruth Hussey Judith Anderson C. Aubrey Smith
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       = February 28, 1941
| runtime        = 56 mins.
| country        = United States
| language       = English
| budget         = $244,000  . 
| gross          = $333,000 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Robert Montgomery and C. Aubrey Smith as the son-and-father team. 

==Cast==
*Robert Cummings as Max Clemington
*Ruth Hussey as Martha Gray
*Judith Anderson as Lady Joan Culver 
*C. Aubrey Smith as Duke Colver
*Nigel Bruce as Florian Clemington Charles Coleman as Powers, Culvers butler
*Reginald Owen as Sir Kelvin
*Teresa Maxwell-Conover as Lady Ridgeway

==Plot==
Max (Cummings) and his father (Bruce) are both looking to marry wealthy women, which would be easier if either one of them had any money of their own. 

Max decides on Martha (Hussey), but Martha says no when he says that he is poor as she admits she is also. So she accepts the proposal of Sir Kelvin (Owen), but changes her mind by the next day. When Florian tries to win money gambling for Maxs wedding, he loses a bundle. When Max finds out about the debt, he decides to marry the wealthy Lady Joan (Anderson) to keep Florian out of jail.

==Box Office==
According to MGM records the film earned $205,000 in the US and Canada and $128,000 elsewhere resulting in a loss of $33,000. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 

 