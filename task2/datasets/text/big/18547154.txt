Jonny Vang
{{Infobox film name           = Jonny Vang image          = Jonny Vang.jpg image_size     = caption        = director       = Jens Lien producer       = Dag Alveberg writer         = Ståle Stein Berg narrator       = starring  Nils Vogt music  Calexico
|cinematography = editing        = Pål Gengenbach distributor    = United International Pictures (UIP) released       = Norway: 14 February 2003 runtime        = 85 min. country        = Norway language  Norwegian
|budget         =      gross          = preceded_by    = followed_by    =
}}
 Norwegian film Amanda Award for "Best Actor" in 2003.

==Plot==

Jonny Vang (Aksel Hennie) lives on the Norwegian countryside, where he is trying to establish a business breeding earthworms. His ambitions to expand are thwarted by the bank manager (Trond Brænne), who will not lend him the necessary money. He lives with his mother Brita (Marit Andreassen) and her difficult friend Odvar (Bjørn Sundquist). On top of all of this, he is also carrying out an affair with Tuva (Laila Goody) the wife of his best friend Magnus (Fridtjov Såheim). Things get even worse when an unknown assailant knocks him over the head with a shovel.

==Cast==
*Jonny Vang -  Aksel Hennie
*Tuva -  Laila Goody
*Magnus - Fridtjov Såheim
*Brita -  Marit Andreassen
*Odvar - Bjørn Sundquist
*Police Officer - Nils Vogt
*Gunnar - Anders Ødegård
*Helene - Silje Salomonsen
*Bank manager -  Trond Brænne

== Production ==
Jonny Vang was director Jens Liens debut as a feature film director.  Lien had previously established himself as a director of television advertisements.  He had also been represented twice at the Cannes International Film Festival, with short films.    He described the film as "a drama comedy, a juicy story, life, lust and rock n roll. Its a dead serious comedy." 

Lien originally considered Hennie too young for the role, but the actor was persistent, and finally persuaded the director that he was the right man for the part.  The movie, where the story takes place in Gudbrandsdalen, was filmed in the town of Fåvang.   
 simultaneously translate Mexican music, and is gathered from the bands albums Spoke (album)|Spoke (1997), The Black Light (1998) and Hot Rail (2000). 

== Reception ==

Jonny Vang was reasonably well received by the Norwegian press. The newspaper Dagbladet gave the film four out of six points, and commended it for good scenes, yet found it somewhat predictable. The reviewer mentioned Calexico (band)|Calexicos soundtrack as one of the best parts of the movie.  Aftenpostens Per Haddal awarded five out of six points, and believed the movie was a well functional comedy. Also Haddal commended the choice of music. 
 Variety complimented Swedish website "DVD forum" jokingly said that he laughed often, so he hoped it was a comedy. Generally content with the movie, he found Hennies performance somewhat "theatrical". 
 Amanda Award for "Best Actor" for his effort in the film in 2003. The movie was also nominated in the category "Best Film", but lost out to Bent Hamers Salmer fra kjøkkenet.    Hennie also won the "Best Actor" award at the Brussels European Film Festival. 

== References ==
 

==External links==
*  
*  
* 

 
 
 