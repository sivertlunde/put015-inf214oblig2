Ride Him, Cowboy
{{Infobox Film
| name           = Ride Him, Cowboy
| image          = Ride Him,Cowboyposter.jpg
| caption        = Original poster Fred Allen
| producer       = Leon Schlesinger
| writer         = {{plain list|
* Kenneth Perkins (novel)
* Scott Mason
}}
| starring       = {{plain list|
* John Wayne
* Frank Hagney
* Ruth Hall
}}
| music          = 
| cinematography = Ted D. McCord
| editing        = William Clemens
| distributor    = Warner Bros.
| released       =  
| runtime        = 55 minutes
| country        = United States
| language       = English
}}
 Western film Fred Allen and starred 25-year-old John Wayne. The film was titled The Hawk in the UK.

==Plot==
John Drury (John Wayne) is passing through when townsfolk are about to kill Duke, a horse they believe to be dangerous. He convinces them to reprieve the animal if he can ride it. He does, earning the gratitude of Ruth Gaunt (Ruth Hall).

He then volunteers to deal with an outlaw known as the Hawk who has been terrorizing the area. Solid citizen Henry Simms (Frank Hagney) volunteers to guide him to the Hawks territory. But Simms is actually the Hawk and he ties Drury to a tree, leaving him to die. Simms then leads a raid on a ranch, kills a man and plants Drurys harmonica at the scene. With the help of his horse Duke, Drury manages to free himself.

However, a group of vigilantes, believing that he is the Hawk, accuse him of murder and take him to face a hanging judge. Fortunately, Ruth shows up, with the news that a wounded witness has regained consciousness and confirmed Drurys claim that Simms is the real bandit. Simms men burst in and hold everyone at gunpoint. Simms takes Ruth with him to his hideout, but Drury manages to escape and follow them. The posse overpowers Simms henchmen and captures the rest of the gang. Simms and Drury fight; when Drury is distracted by the arrival of help, Simms knocks him out and tries to flee, only to run into the deadly hooves of an enraged Duke.

==Cast (in credits order)==
 
* John Wayne as John Drury Ruth Hall as Ruth Gaunt
* Henry B. Walthall as John Gaunt
* Otis Harlan as Judge E. Clarence "Necktie" Jones
* Harry Gribbon as Deputy Sheriff Clout
* Frank Hagney as Henry Sims / The Hawk

==See also==
* John Wayne filmography

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 

 