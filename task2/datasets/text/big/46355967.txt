Purushottam (1992 film)
 
{{Infobox film
| name           = Purushottam
| image          = 
| image_size     =
| caption        = 
| director       = Prosenjit Chatterjee
| producer       = Prosenjit Chatterjee
| writer         = 
| story          = 
| screenplay     = 
| starring       = Prosenjit Chatterjee Debashree Roy Abhishek Chatterjee Dipankar Dey Kali Bandyopadhyay Shubhendu Chattopadhyay Pradip Mukhopadhyay Pallabi Chattopadhyay  
| music          = R. D. Burman  
| distributor    =
| released       =  
| runtime        = 
| genere         = Action
| studio         = Prasenjit Production
| country        = India Bengali
| budget         =
| gross          =
}}
 Bengali film directed & produced by Prosenjit Chatterjee.This Film music composed by R. D. Burman.

==Cast==
* Prosenjit Chatterjee
* Debashree Roy
* Abhishek Chatterjee
* Dipankar Dey
* Kali Bandyopadhyay
* Shubhendu Chattopadhyay
* Pradip Mukhopadhyay
* Pallabi Chattopadhyay  

==Soundtrack==
===Track listing===
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s) || Duration || Music || Lyricist
|-
| 1
| Aae Ghoom Aae
| Asha Bhosle
| 4:38
| R. D. Burman
| Sachin Bhowmik  
|-
| 2
| Aaj Andhokar Jatoi Hoke
| Kumar Sanu
| 5:49
| R. D. Burman
| Sachin Bhowmik
|-
| 3
| Deete Pari Ae Jibano
| R. D. Burman
| 7:16
| R. D. Burman
| Sachin Bhowmik
|-
| 4
| Ki Laabh Habe Kare Ei
| Asha Bhosle, Jolly Mukherjee
| 5:25
| R. D. Burman
| Sachin Bhowmik  
|-
|}

==See also==
 

==References==
 

==External links==
 
* http://www.induna.com/1000009563-productdetails/

 
 
 


 