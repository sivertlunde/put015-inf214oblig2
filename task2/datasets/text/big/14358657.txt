Alsino and the Condor
{{Infobox film
| name           = Alsino and the Condor
| image          = Alsino and the Condor movie poster.jpg
| caption        = Cuban theatrical release poster 
| director       = Miguel Littín
| producer       = Ramiro Lacayo   Hernan Littin
| writer         = Miguel Littin Tomas Perez Turrent Isadora Aguirre
| starring       = Dean Stockwell Alan Esquivel Alejandro Parodi
| music          = Leo Brouwer
| cinematography = Jorge Herrera Pablo Martinez
| editing        =
| country        = Nicaragua Mexico
| language       = Spanish
| released       =  
| runtime        = 89 minutes
| budget         = 
}} Nicaraguan film directed by Miguel Littín. It was nominated for the Academy Award for Best Foreign Language Film.    It  won the Golden Prize at the 13th Moscow International Film Festival.    The film was a co-production between Nicaragua, Mexico and Cuba. The film is loosely based on the novel Alsino by Chilean writer Pedro Prado.

It was the comeback film of actor Dean Stockwell. Notable Cuban Director of Photography Jorge Herrera died during the production due to a brain hemorrhage.

==Cast==
* Dean Stockwell as Frank
* Alan Esquivel as  Alsino
* Carmen Bunster as Alsinos Grandmother
* Alejandro Parodi as The Major
* Delia Casanova as Rosaria
* Marta Lorena Pérez as Lucia
* Reynaldo Miravalles as Don Nazario, the Birdman
* Marcelo Gaete as Lucias Grandfather
* Jan Kees De Roy as Dutch Adviser

==Awards==
===Wins===
* Moscow International Film Festival - Golden Prize, Miguel Littin, 1983.

===Nominations===
*  Academy Awards - Best Foreign Language Film, 1983.

==See also==
* List of submissions to the 55th Academy Awards for Best Foreign Language Film
* List of Central American submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 

 
 
 
 
 
 