The Shadow of a Mine
{{Infobox film
| name           = The Shadow of a Mine
| image          =
| image_size     = 
| caption        = 
| director       = Phil Jutzi
| producer       = 
| writer         = Léo Lania
| narrator       = 
| starring       = Holmes Zimmermann   Sybille Schloß
| music          = 
| editing        = 
| cinematography = Phil Jutzi
| studio         = Weltfilm   Volksfilmverband 
| distributor    = 
| released       = 16 March 1929
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent drama film directed by Phil Jutzi and starring Holmes Zimmermann and Sybille Schloß. Its original German title is Ums tägliche Brot (Our Daily Bread). It is also known as Hunger in Waldenburg.

The film was produced by the left-wing politics|left-wing Volksfilmverband in partnership with Weltfilm and the Theater am Schiffbauerdamm. Using a docudrama format, the film highlights the hardships faced by Silesian coal miners in Wałbrzych|Waldenburg. It premiered at the Tauenzienpalast in Berlin on 16 March 1929. 
 print of the film which survives. 

==Cast==
* Holmes Zimmermann as Junger Weber 
* Sybille Schloß

==References==
 

==Bibliography==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.
* Murray, Bruce Arthur. Film and the German Left in the Weimar Republic: From Caligari to Kuhle Wampe. University of Texas Press, 1990.

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 