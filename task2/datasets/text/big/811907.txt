Rouge (film)
 
 
 
:For the Kieślowski film, see  .

{{Infobox film
| name = Rouge
| image = Rougefilm.jpg
| caption = Film poster
| director = Stanley Kwan
| producer = Jackie Chan Leonard Ho Lilian Lee Pik Wah
| starring = Anita Mui Leslie Cheung
| music = Michael Lai Tang Siu Lam
| cinematography = Bill Wong
| editing = Peter Cheung Golden Harvest Golden Way Films
| distributor =
| released =  
| runtime = 96 min / USA:93 min
| language = Cantonese
| budget =
| gross = HK$17,476,414
}}

Rouge ( ;  . The movie is the adaptation of the novel by Lilian Lee.

==Plot==
Chan Chen-Pang, better known as the "12th Young Master" (played by Leslie Cheung), was a fashionable playboy who frequented the opium dens prevalent in Hong Kong in 1934, where he met the high-class and much sought-after courtesan, Fleur (Anita Mui).

They began a passionate love affair, something frowned upon by the family of 12th Master, who held sway over the business of the area. Clearly, their love was doomed when it was found out. Seeing no way to pursue their love in life, on 8 March at 11pm, the couple resolved to commit suicide together by swallowing opium, and promise to meet again in the afterlife. After waiting for Chan in hell for 50 years, Fleur returns to the world of the living to look for him, wondering why he has not emerged. A spirit now, she searches for her lover in a Hong Kong she no longer recognizes. She decides to place a newspaper advertisement seeking Chan (using the code "3811", to indicate the eighth day of the third month at eleven pm, the time of their suicide), and enlists the assistance of Yuen (Alex Man) and his understandably suspicious girlfriend, Chor (Emily Chu).

Fleur did find her lover in the end, but she was surprised, dismayed and felt cheated, as Chen-Pang did not die in the suicide pact. How he survived the suicide attempt was not explained, but Fleur seemed to have understood that he had not been entirely honest on his part. Not surprisingly, due to his wayward and flirtatious nature, he was reduced to poverty where he lived to be an old and forgotten man well into his late 70s, making a living as a Chinese opera stand-in and living in the cramped quarters of the opera set itself. Recognising Fleur instantly, he begged her for her forgiveness but his pleas fell on deaf ears. As she drifted effortlessly away, Fleur realised that he had indeed suffered much more than she had, as his guilty conscience haunted him for so many years. She returned the rouge case he gave her 50 years ago. Retribution has been paid in kind and she was content to leave as the living world is no longer her home.

==Cast includes==
* Anita Mui - Fleur
* Leslie Cheung - Chan Chen-Pang 
* Alex Man - Yuen 
* Irene Wan - Shu-Hsien
* Emily Chu - Ah Chor 
* Kara Hui - Actress Portraying Ghost 
* Lau Kar-wing - Movie Director 
* Patrick Tse - Brothel Patron
* Ruby Wong
* Jackie Chan Stunt Team - extra/stunts

==Awards==
* 1988 Torino International Festival of Young Cinema: International Feature Film Competition Special Mention
* 1988 Nantes Three Continents Festival: Golden Montgolfiere
* 1988 Golden Horse Film Festival: Best Actress (Anita Mui)
* 1989 Asia-Pacific Film Festival: Best Actress (Anita Mui) 
* 1989 Hong Kong Film Awards: Best Actress (Anita Mui), Best Director (Stanley Kwan), Best Film Editing, Best Original Film Score, Best Original Film Song ("Yin Ji Kau"), Best Picture

==See also==
*Jackie Chan filmography

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 