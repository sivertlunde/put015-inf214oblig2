Jenin, Jenin
{{Infobox film
| name           = Jenin, Jenin
| image          = Jenin jenin.jpg
| image_size     = 
| caption        = 
| director       = Mohammed Bakri
| producer       = Iyad Tahar Samoudi  , Haaretz, September 17, 2003. 
| writer         = Mohammed Bakri
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = Leandro Pantanella 
| distributor    = 
| released       = 2002
| runtime        = 54 min
| country        = Palestinian territories
| language       = Arabic
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Arab actor Palestinian truth" Palestinian accounts of a "Battle of Jenin" ( ).

==Background==

A month after 18 Israelis had been killed in two separate attacks, and a few days after a   and launched Operation Defensive Shield.
 massacre had occurred. Jenin remained sealed for days after the invasion. Stories of civilians being buried alive in their homes as they were demolished, and of smoldering buildings covering crushed bodies, spread throughout the Arab world. Various casualty figures circulated, a senior Palestinian official accused Israel of massacring more than 500 people in the camp. UN fact-finding mission was not allowed by Israeli to enter Jenin. 

Bakri participated in a nonviolent demonstration at a checkpoint during Israel’s 2002 invasion of Jenin and was shocked when Israeli soldiers shot at the crowd, wounding a fellow actor standing next to him. He tells audiences that this experience inspired him to sneak into Jenin with a camera and ask residents, “What happened?” Jane Adas,   Washington Report on Middle East Affairs, September/October 2007, p. 42  The result was the documentary Jenin Jenin, featuring a range of testimonies which suggested that a massacre had indeed occurred. Bakri gave voice to the perspective of Palestinians which would not reach the media due to the sealing of the city; as a result he chose not to interview Israeli officials for the film.

 .  Human Rights Watch estimates that between 53-56 Palestinians were killed during the Israeli offensive, just over half of them suspected to be armed combatants.  Israel concurs that around 50 Palestinian died, but describes the event as a battle and blames civilian deaths on the close proximity of fighters and civilians. 23 Israeli soldiers died.

The film title referenced Palestinian taxidrivers calling "Ramallah, Ramallah, Ramallah," or "Jenin! Jenin!" to Palestinian workers and travellers moving through Israeli checkpoints. Bakri dedicated the film to its producer, Iyad Samoudi, who was killed by Israeli soldiers, at al-Yamun in the Jenin Governorate of the West Bank, shortly after filming ended. The IDF said that Samoudi was an armed member of the al-Aqsa Martyrs Brigades. http://www.haaretz.com/hasen/spages/957775.html Haaretz: T.A. cinema to screen Jenin, Jenin on eve of directors libel trial 

==Film content==

The film has no narrator or guide and consists only of interviews with the inhabitants of Jenin edited by the producer.    

== Controversy ==

=== Official positions: Libel or censorship? === IDF  )]] Tel Aviv and Jerusalem Cinematheques in Israel showed Bakris film despite the ban. 

Bakri took the ban to court and the   quotation: "And with intellect shall distinguish the man, between the truth and the false."  On appeal, the Supreme Courts ruling was stayed, but in August 2004 the Supreme Court reaffirmed the overturning of the ban, stating that the film board does not have "a monopoly over truth".  Although the Supreme Court described the film as a "propagandistic lie," the ruling affirmed that choosing not to show both sides of a story is not grounds for censorship.

===Defamation lawsuit===

Five Israeli reserve soldiers who served in Jenin filed suit in 2002 against Bakri for defamation arguing that the movie had sullied their good names.   The plaintiffs were not mentioned in the film. The judge dismissed the case, ruling that while the film did in fact slander Israeli soldiers, the five Israeli soldiers were not personally slandered and thus had no standing to sue. The judge said in her verdict that Bakri had not shown "good faith", had brought no witnesses, and had not proved his claim that his charges were backed up by reports from human rights groups. 

=== Public critics ===
 IDF in Hadassah University Hospital in Jerusalem ) issued a public statement titled Seven Lies About Jenin,  giving his personal accounts about his visit to a private premiere  screening of the film at the Jerusalem Cinematheque. In his statement, he cited 7 discrepancies he had hoped to raise in front of the viewers who denied him the possibility to get past the second point. He claimed Bakri has skillfully made a crude, albeit well-done, manipulation that it is difficult not to be drawn into the created distorted picture; and that he was amazed that the audience was not willing to hear his own accounts, a person who had physically been there.   It should be noted that the version distributed in the English language is modified from the original movie, and some of the problematic scenes Zangen pointed out were omitted. 

===Defense===

The Mohammad Bakri Defense Committee argues that "the importance of this case reaches beyond Bakri as an individual," amounting to repression of Palestinian self-expression. About Mohammad Bakris case"   Mohammad Bakri Defense Committee  At a screening of his latest film in New York, a disturbed audience member confronted Bakri with accusations that Jenin, Jenin exaggerated the atrocities of the invasion presented a one-sided view, and Bakri responded that he had "seen hundreds of films that deny and ignore what happened to Palestinians, yet never complained or tried to ban any film."  

The main line of defense for Bakri and his film is that, as the Supreme Court found, choosing not to show both sides of a story is not grounds for censorship. Bakri is being represented by attorney Avigdor Feldman, who told Haaretz, "Bakri doesnt say anything in this film. The people who talk are those he filmed. So the residents of the refugee camp say things which sometimes are true and sometimes not. Its a movie. It reflects the subjective understanding of the speakers. Sometimes they say things that are harsher   because that is how he experienced it." Dan Izenberg,   Jerusalem Post, September 17, 2007  

The Mohammad Bakri Defense Committee adds: "For his artistic integrity and his focus on the experiences and narratives of his fellow Palestinians, Mohammad Bakri faces the potential of financial ruin in the face of spurious legal charges and dubious claims of defamation." A major component of the argument for the defense in the most recent allegations is that none of the plaintiffs, Ofer Ben-Natan, Doron Keidar, Nir Oshri, Adam Arbiv and Yonatan Van-Kaspel, are mentioned by name or shown in the film. 

== Awards and nominations ==
Jenin, Jenin was awarded Best Film at the Carthage International Film Festival and International Prize for Mediterranean Documentary Filmmaking & Reporting.   

==Notes==
 

== External links ==
* 
*  
* , distributors site
*  

 
 
 
 
 
 
 
 