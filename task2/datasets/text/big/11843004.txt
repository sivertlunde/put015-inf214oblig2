Particles of Truth
{{Infobox Film
| name           = Particles of Truth
| image          = 
| caption        = Official DVD Cover
| director       = Jennifer Elster
| producer       = Jennifer Elster
| writer         = Jennifer Elster 
| starring       = Jennifer Elster   Gale Harold
| music          = Mark Wike
| cinematography = Toshiro Yamaguchi
| editing        = Ron Len 
| distributor    = Hart Sharp Video 	  2003 
| runtime        = 101 m
| country        = United States
| language       = English
| budget         = 
| followed_by    = 
}} 2003 low-budget independent film directed, written by and starring Jennifer Elster, with Gale Harold. The film was released on July 26, 2005 by Hart Sharp Video. It premiered in the Tribeca Film Festival in 2003.

==Plot==
The film follows the lives of 10 dysfunctional individuals for 48 hours before the grand opening of an art show, focusing particularly on one dysfunctional couple played by Elster and Harold.

When this couple (Lili and Morrison) kiss for the first time, Lili goes into his bathroom and has flashbacks of her mother and realizes that she is not ready for a relationship. The film concludes that only with closure of her past, can she commit to a healthy relationship in the future.

==Cast==
{| class="wikitable"
|-
! style="background-color:silver;" | Actor
! style="background-color:silver;" | Role
|-
| Jennifer Elster || Lilli Black
|-
| Gale Harold || Morrison Wiley
|-
| Alan Samulski || Johnny
|-
| Susan Floyd || Louise
|-
| Michael Laurence || Charles
|-
| Richard Wilkinson || Will
|- 
| Elizabeth Van Meter || Flora
|-
| Mark Margolis || Grandpa Black
|-
| Leslie Lyles || Mrs. Wiley
|-
| Larry Pine || Mr. Wiley
|}

==Reception==
It competed in the narrative section of the IFP/Los Angeles Film Festival. The film continued to screen at film festivals nationally and internationally during 2003 and 2004.

In 2003, it won the Austin Film Festival Jury Award for Best Feature and in 2004, it won the Best High Definition Feature Film at the HD Fest.  It mostly has favorable reviews. The Los Angeles Times noted that "The images of Particle of Truth are so sharp and cut so deep. Its as if its writer-producer-director Jennifer Elster made them with a scalpel.  Other positive comments include "standout performances...its damn fine," and "NY indie airily pulls of what Hollywood mightily strives for--believable romantic comedy". 

==Production notes==
Viagra was misspelled intentionally for the film, this having to do with the cost of obtaining the right to use its name. 

==DVD release==
Particles of Truth was released on DVD in 2005.

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 