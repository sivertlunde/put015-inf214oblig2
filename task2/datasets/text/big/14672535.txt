Jeppe på bjerget (film)
{{Infobox film
| name           = Jeppe på bjerget
| image          = Jeppe på bjerget (1981).jpg
| caption        = 
| director       = Kaspar Rostrup
| producer       = Bo Christensen
| writer         = Henning Bahs Kaspar Rostrup Ludvig Holberg (play)
| starring       = Buster Larsen Else Benedikte Madsen Henning Jensen
| music          = Erling D. Bjerno
| cinematography = Claus Loof
| editing        = Lars Brydesen
| distributor    =
| released       =  
| runtime        = 110 minutes
| country        = Denmark
| language       = Danish
| budget         = 
| gross          = 
}}

Jeppe på bjerget is a 1981 Danish film directed by  ) and Best Supporting Actor (for Kurt Ravn). It was also entered into the 12th Moscow International Film Festival.    It directly translates to Jeppe on the mountain.

==Cast==
*Buster Larsen as Jeppe
*Else Benedikte Madsen as Nille
*Henning Jensen as Baronen
*Kurt Ravn as Erik Lakaj
*Benny Poulsen as Baronens sekretær Poul Møller as Jacob Skomager
*Axel Strøbye as Ridefogeden
*Arthur Jensen as Jacobæus Lakaj
*Claus Ryskjær as Peter Kane Lakaj
*Claus Nissen as Peder Hammer Lakaj
*Benny Bjerregaard as Severin
*Jonny Eliasen as Morten Lakaj
*Frank Andersen as Jacob Lakaj
*Ole Ishøy as Baron
*Ove Verner Hansen as Baron

 
==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 