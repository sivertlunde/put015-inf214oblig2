Nice People
{{Infobox film
| name           = Nice People
| image          = Nice-People-1922.jpg
| image_size     = 190px
| caption        = Movie poster
| director       = William C. deMille
| producer       = Adolph Zukor
| writer         = Rachel Crothers (play) Clara Beranger (scenario)
| starring       = Wallace Reid Bebe Daniels Julia Faye
| cinematography = L. Guy Wilky
| editing        =
| studio         = Famous Players-Lasky
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes (7 reels)
| country        = United States Silent (English intertitles)
| budget         =
}}
 silent drama Nice People by Rachel Crothers that had starred Tallulah Bankhead, Francine Larrimore and Katharine Cornell.  Vincent Coleman played Reids part of the Captain.   

==Cast==
*Wallace Reid - Captain Billy Wade
*Bebe Daniels - Theodora Gloucester (played by Francine Larrimore on Broadway)
*Conrad Nagel - Scotty White
*Julia Faye - Hallie Livingston (played by Tallulah Bankhead on Broadway)
*Claire McDowell - Margaret Rainsford
*Edward Martindel - Hubert Gloucester
*Eve Southern - Eileen Baxter-Jones (played by Katharine Cornell on Broadway)
*Bertram Johns - Trevor Leeds William Boyd - Oliver Comstock
*Ethel Wales - Mrs. Heyfer

==See also==
*The House That Shadows Built (1931 promotional film by Paramount with excerpt of this film)

==References==
 

==External links==
 
* 
* 
*  

 

 
 
 
 
 
 
 
 
 
 


 