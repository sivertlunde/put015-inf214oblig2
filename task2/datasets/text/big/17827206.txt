The Cat (1992 film)
 
 
{{Infobox film
| name           = The Cat
| image          = TheCat.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 衛斯理之老貓
| simplified     = 卫斯理之老猫
| pinyin         = Wèi Sī Lǐ Zhī Lǎo Māo
| jyutping       = Wai6 Si1 Lei2 Zi1 Lou2 Maau1 }}
| director       = Lam Ngai Kai
| producer       = Chua Lam Michael Lai
| writer         = 
| screenplay     = Chan Hing Ka Gordon Chan
| story          = 
| based on       =  
| starring       = Waise Lee Christine Ng Gloria Yip
| narrator       = 
| music          = Philip Chan
| cinematography = Mak Hoi Man
| editing        = Keung Chuen Tak Peter Cheung Golden Harvest Paragon Films
| distributor    = Golden Harvest
| released       =  
| runtime        = 84 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$2,733,592
}}
The Cat (衛斯理之老貓 Wai Si Li zhi Lao Mao (Wiselys Old Cat) is a 1992 Hong Kong science fiction action film directed by Lam Ngai Kai.  The film is an adaptation of the novel Old Cat by Ni Kuang and tells the story about a cat from outer space teams up with a young alien girl and her knight alongside adventure novelist Wisely to fight a murderous alien that possesses people.

==Cast and roles==
* Waise Lee as Wisely
* Christine Ng as Pak So
* Gloria Yip as Alien Girl
* Lau Siu-Ming as Errol
* Lawrence Lau as Lee Tung
* Philip Kwok as Wang Chieh-Mei
* Ni Kuang as Mr. Chen
* Wan Seung Lam as One of Wangs men
* Kong Long as Gun dealer/One of Wangs men
* Chua Lam as Professor Yu
* Chui Kin Wah as Gun dealer
* Simon Cheung as Policeman

==See also==
* Wisely Series, the novel series by Ni Kuang
* Films and television series adapted from the Wisely Series:
** The Seventh Curse, a 1986 Hong Kong film starring Chow Yun-fat as Wisely
** The Legend of Wisely, a 1987 Hong Kong film starring Sam Hui as Wisely
** The New Adventures of Wisely, a 1998 Singaporean television series starring Michael Tao as Wisely
** The Wesleys Mysterious File, a 2002 Hong Kong film starring Andy Lau as Wisely
** The W Files, a 2003 Hong Kong television series starring Gallen Lo as Wisely

==External links==
*  
*   at HK Cinemagic entry
*    

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 