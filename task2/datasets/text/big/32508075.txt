Virgin Island (1959 film)
 
{{Infobox film
| name           = Our Virgin Island Virgin Island
| image          = Our Virgin Island 1959 poster.jpg
| image_size     = 300
| caption        = 1959 theatrical poster
| director       = Pat Jackson
| producer       = Leon Close
| writer         = Ring Lardner Jr.
| narrator       = 
| starring       = John Cassavetes Virginia Maskell Sidney Poitier
| music          = Clifton Parker
| cinematography = Freddie Francis
| editing        = Gordon Pilkinton
| distributor    = 
| released       = 1959
| runtime        = 94 minutes  
| country        = United Kingdom
| language       = English
| budget         = 
}}
 British drama film directed by Pat Jackson and starring John Cassavetes, Virginia Maskell and Sidney Poitier.  It is an adaptation of the novel Our Virgin Island by Robb White and was filmed on the British Virgin Islands.

==Synopsis==
A British woman marries an American writer in spite of her familys disapproval and goes to live with him on a tropical island.

==Cast==
* John Cassavetes - Evan
* Virginia Maskell - Tina
* Sidney Poitier - Marcus
* Isabel Dean - Mrs Lomax
* Colin Gordon - The Commissioner
* Ruby Dee - Ruth
* Howard Marion-Crawford - Prescott
* Arnold Bell - Heath
* Gladys Boot - Mrs Carruthers
* Alonzo Bozan - Grant
* Edric Connor - Captain Jason
* Reginald Hearne - Doctor
* Julian Mayfield - Band Leader

==References==
 

==External links==
* 


 

 
 
 
 
 
 
 


 