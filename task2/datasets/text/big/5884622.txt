The Runaway Bride (film)
 

{{Infobox film
| name           = The Runaway Bride
| image          = TheRunawayBrideFilmPoster.jpg
| image size     = 
| caption        = Film poster James Anderson (assistant)
| producer       = William Sistrom 
| writer         = Jane Murfin 
| based on       =   
| starring       = Mary Astor Lloyd Hughes Paul Hurst 
| music          = 
| cinematography = Leo Tover 
| editing        = Archie Marshek  RKO Radio Pictures
| released       =   }}
| runtime        = 69 minutes   
| country        = United States
| language       = English
| budget = $103,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p56  gross = $204,000 
}}
 1930 comedy film starring Mary Astor, Lloyd Hughes and Paul Hurst. It was directed by Donald Crisp, from a screenplay by Jane Murfin, adapted from the play, Cooking Her Goose, by Lolita Ann Westman and H. H. Van Loan. 

The film is preserved in the Library of Congress collection. 

==Plot==
Mary Gray and Dick Mercer are racing to Atlantic City, where they intend to elope, since Marys wealthy parents would never approve of the marriage.  In Atlantic City, they arrive at the humble efficiency hotel room Mary has rented.  Dick is not impressed, and would prefer they stay in a fancier hotel. An argument ensues, ending with Dick storming out, leaving Mary alone.  

While waiting for Dicks return, a jewel thief, Red Dugan, arrives at the hotel room, with a policeman, Sergeant Daly in hot pursuit.  Desperate, Dugan secretes a stolen necklace in Marys purse, before being shot and killed by Daly, who is himself wounded in the gun battle.  

A chambermaid at the hotel, Clara Muldoon, suspects that the jewels have been passed to Mary.  The police pick up Mary, Dick and Clara, but Mary is released.  Attempting to flee from both Dick and the police, Mary seeks a job as a cook under an assumed name at the home of the wealthy bachelor, George Blaine.  While there, a mutual attraction begins to develop between Mary and George.  Clara, and the rest of the thieves gang, trace Mary to Blaines house, and abduct her while they recover the stolen jewels.  

The gang takes Mary to a fake hospital, where they hold her against her will.  Blaine tracks Mary down and rescues her, just before the police arrive to arrest the jewel thieves.

==Cast==
*Mary Astor as Mary Gray / Sally Fairchild
*Lloyd Hughes as George Edward Blaine Paul Hurst as Sergeant Daly David Newell as Dick Mercer
*Natalie Moorhead as Clara Muldoon
*Edgar Norton as Williams
*Francis McDonald as Barney Black

==Notes==
Although Donald Crisp was a seasoned director, with dozens of silent films under his belt, this would be the first and last talking film he ever directed. 

==Reception==
The film made a profit of $15,000. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 