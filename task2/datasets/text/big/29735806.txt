Special When Lit (film)
{{Infobox film
| name           = Special When Lit
| image          = 
| image size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Brett Sullivan
| producer       = Brett Sullivan Clayton Jacobsen Julian Chow Emily Rickard (line)
| writer         = 
| screenplay     = 
| story          =
| based on       =  
| narrator       = 
| starring       = 
| music          = Brett Sullivan
| cinematography = 
| editing        = Brett Sullivan
| studio         = Steam Motion and Sound PBS International
| released       =  
| runtime        = 97 minutes
| country        = United Kingdom English
| budget         = United States|$150,000 (estimated)
| gross          = 
| preceded by    = 
| followed by    = 
}}

Special When Lit is a feature length documentary film about pinball written and directed by Brett Sullivan.   The film is produced by Steam Motion and Sound.

==Production== London during 2008 and 2009.

==Synopsis==
The pinball industry made more money than did the American film industry during the 1950s through the 1970s. Special When Lit explores the former pop icon of the pinball machine, and through interviews with fans, collectors, designers and champion players from across the globe, traces pinballs history through to the present day.

==Recognition==
Eye for Film wrote "Its not the sort of subject that youd think would suit a documentary, but it works surprisingly well." It "offers fascinating insights and makes for an enjoyable watch. Special When Lit is reminiscent of that other great documentary Spellbound. Both draw the audience into a world of obsession, impress upon you the level of devotion, and charm you with the people in that world" 

Tallahassee Democrat wrote that the film was a "surprisingly compelling documentary". 

Chicago Sun-Times called the film a "significant flick in a lineup that runs the gamut from light to heavy religious to political". 

Raindance Film Festival director Elliot Grove wrote that the documentary was "masterfully shot" and that its director, Brett Sullivan, was able to bring out a certain nostalgia in his film that was both intriguing and fascinating, with its interviews like "an emotional and sensitive area, with pinball’s fans describing of the game like a relationship, their faces lighting up as if they were recounting their first kiss."  He found the winning film to be "encapsulating and absorbing." 

The Montana Kaimin said wrote "With a gripping intro, the film draws you in and keeps your attention with its larger-than-life characters."  

===Awards and nominations=== Los Angeles United Film Festival
* 2009, Nominated for Best Documentary at Raindance Film Festival Tallahassee Film Festival
* 2010, Won Audience Award for Best Feature Documentary at  

==Release and distribution==
First premiered in October 2009 at Londons Raindance Film Festival where it was nominated for Best Documentary. Big Sky Buffalo Niagara Bronx International Da Vinci Tallahassee Film Los Angeles United Film Festival, USA Film Festival, and Wisconsin Film Festival.
 PBS International Distribution for worldwide sales outside the United States.  The film was released on DVD and Blu-ray Disc|Blu-ray in January 2011. The Documentary Channel in the USA premiered Special When Lit May 21, 2011. The PBS Channel in the UK premiered Special When Lit November 5, 2011.

The film is also available on Netflix.

==Interviewees==
 
 
* Roger Sharpe
* Rick Stetta
* Sam Harvey
* Steve Epstein
* Gary Stern
 
* Lyman Sheats Jr.
* Tim Arnold
* Josh Kaplan
* John Broughton
* Pat Lawlor
 
* Steve Ritchie
* Steve Kordek
* Steve Keeler
* Raphael Lankar
* Koi Morris
 

==References==
 

 {{cite news
|url=http://pqasb.pqarchiver.com/tallahassee/access/2023202731.html?FMT=ABS&FMTS=ABS:FT&type=current&date=May+2%2C+2010&author=Mark%20Hinson&pub=Tallahassee+Democrat&desc=Heres+one+pinball+wizard+who+misses+all+the+magic&pqatl=google
|title=Heres one pinball wizard who misses all the magic
|last=Hinson
|first=Mark
|date=May 2, 2010
|work=Tallahassee Democrat
|accessdate=25 December 2010}} 

 {{cite news
|url=http://www.businessday.com.au/executive-style/culture/when-pinball-was-king-20100104-lo1f.html
|title=When pinball was king
|last=Australian Associated Press
|date=January 4, 2010
|work=Business Day
|accessdate=25 December 2010}} 

 {{cite news
|url=http://www.eyeforfilm.co.uk/reviews.php?id=8533
|title=review: Special When Lit
|last=Dudhnath
|first=Keith
|work=Eye for Film
|accessdate=25 December 2010}} 

 {{cite news
|url=http://pqasb.pqarchiver.com/tallahassee/access/2005315971.html?FMT=ABS&FMTS=ABS:FT&type=current&date=Apr+9%2C+2010&author=Mark%20Hinson&pub=Tallahassee+Democrat&desc=Film+fest+is+out+of+the+blue+and+into+the+black&pqatl=google
|title=Film fest is out of the blue and into the black
|last=Hinson
|first=Mark
|date=April 9, 2010
|work=Tallahassee Democrat
|accessdate=25 December 2010}} 

 {{cite news
|url=http://www.suntimes.com/entertainment/movies/2693172,MOV-News-united10.article
|title=Celebrating the true indie movie spirit
|date=September 10, 2010
|work=Chicago Sun-Times
|accessdate=25 December 2010}} 

 {{cite web
|url=http://www.raindance.co.uk/site/index.php?id=403,4819,0,0,1,0
|title=review: Special When Lit
|last=Grove
|first=Elliot
|date=September 20, 2009
|work=Raindance Film Festival
|accessdate=25 December 2010}} 

 {{cite web
|url=http://www.montanakaimin.com/index.php/articles/article/kaimin_arts_takes_a_look_at_the_final_weekend_of_the_big_sky_film_festival/759
|title=Kaimin Arts takes a look at the final weekend of the Big Sky Film Festival
|last=Stugelmayer
|first=Jessica
|date=February 19, 2010
|work=Montana Kaimin
|accessdate=15 March 2011}} 

 

==External links==
*   at the Internet Movie Database
*  
*   Production Company

 
 
 
 
 
 
 