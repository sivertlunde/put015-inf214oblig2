The Blue Angel
:This article is about the 1930 film. For the 1959 remake, see The Blue Angel (1959 film). For other uses, see Blue Angel (disambiguation).
{{Infobox film name            = The Blue Angel image           = Derblaueengel.jpg image_size      = 225px caption         = film poster director        = Josef von Sternberg producer        = Erich Pommer writer          = Heinrich Mann (also novel) Carl Zuckmayer Karl Vollmöller Robert Liebmann Josef von Sternberg starring        = Emil Jannings Marlene Dietrich Kurt Gerron music           = Friedrich Hollaender (music) Robert Liebmann (lyrics) Franz Waxman (orchestrations) cinematography  = Günther Rittau editing         = Walter Klee Sam Winston distributor  UFA Paramount Pictures released        =      runtime         = 99 minutes country         = Weimar Republic language        = German/English gross           = $77,982 (2001 re-release)  budget          =
|}}
 tragicomedic film directed by Josef von Sternberg and starring Emil Jannings, Marlene Dietrich and Kurt Gerron. Written by Carl Zuckmayer, Karl Vollmöller and Robert Liebmann &ndash; with uncredited contributions by Sternberg. It is based on Heinrich Manns novel Professor Unrat ("Professor Garbage", 1905), and set in Weimar Germany. The Blue Angel presents the tragic transformation of a man from a respectable professor to a cabaret clown, and his descent into madness. The film is considered to be the first major German sound film, and brought Dietrich international fame.  In addition, it introduced her signature song, Friedrich Hollaender and Robert Liebmanns "Falling in Love Again (Cant Help It)".
 language versions, lost for many years.

==Plot==
Immanuel Rath (Emil Jannings) is an esteemed educator at the local Gymnasium (school)|Gymnasium &ndash; a college preparatory high school &ndash; in Weimar Germany. He punishes several of his students for circulating photographs of the beautiful Lola-Lola (Marlene Dietrich), the headliner for the local cabaret, "The Blue Angel". Hoping to catch the boys at the club, Rath goes there later that evening and meets Lola herself.
 principal furious with his behavior.

Rath subsequently resigns from his position at the academy to marry Lola, but their happiness is short-lived, as they soon fritter away the teachers meager savings and Rath is forced to take a position as a clown in Lolas cabaret troupe to pay the bills. His growing insecurities about Lolas profession as a "shared woman" eventually consume him with lust and jealousy. The troupe returns to his hometown and The Blue Angel, where he is ridiculed and berated by the patrons, the very people he himself used to deride.  As Rath performs his last act, he witnesses his wife embrace and kiss the strongman Mazeppa, her new love interest, and is enraged to the point of insanity.  He attempts to strangle Lola, but is beaten down by the other members of the troupe and locked in a straitjacket.

Later that night, Rath is freed, and makes his way towards his old classroom. Rejected, humiliated, and destitute, he dies in remorse, clenching the desk at which he once taught.

==Cast==
*Emil Jannings as Professor Immanuel Rath
*Marlene Dietrich as Lola-Lola
*Kurt Gerron as Kiepert, the magician
*Rosa Valetti as Guste, the magicians wife
*Hans Albers as Mazeppa, the strongman
*Reinhold Bernt as the clown
*Eduard von Winterstein as the director of school
*Hans Roth as the caretaker of the secondary school
*Rolf Müller as Pupil Angst
*Roland Varno as Pupil Lohmann
*Carl Balhaus as Pupil Ertzum
*Robert Klein-Lörk as Pupil Goldstaub
*Charles Puffy as Innkeeper
*Wilhelm Diegelmann as Captain
*Gerhard Bienert as Policeman
*Ilse Fürstenberg as Raths maid

== Music ==
*"Ich bin von Kopf bis Fuß auf Liebe eingestellt" ("Falling in Love Again")
*"Ich bin die fesche Lola" ("They Call Me Naughty Lola")
*"Nimm Dich in Acht vor blonden Fraun" ("Those Charming Alarming Blond Women")
*"Kinder, heut abend, da such ich mir was aus" ("A Man, Just a Regular Man")
**music by Friedrich Hollaender, lyrics by Robert Liebmann,   sung by Marlene Dietrich
*"Ein Mädchen oder Weibchen wünscht Papageno sich!" ("A  girl or a little wife, wishes Papageno")
**by Wolfgang Amadeus Mozart, from the opera The Magic Flute

==Production==
Sternberg called the story "the downfall of an enamored man",  and he calls Rath "...a figure of self-satisfied dignity brought low."  Some critics saw the film as an allegory for pre-war Germany, but Sternberg was very clear that he did not intend to make a political stand: "The year was 1929, Germany was undivided, although the real Germany, its schools and other places pictured in the film were not German and reality failed to interest me".  

  
 dubbing and the desire of the studios to be able to sell their films in multiple international markets. 

The Blue Angel is best known for introducing Marlene Dietrich to worldwide attention, although other performers were initially considered for the role, including Trude Hesterberg (a friend of Heinrich Mann), Brigitte Helm and Lucie Mannheim. Käthe Haack had already been signed to play the part before Sternberg met Dietrich and transferred the part to her.  Dietrichs portrayal of an uninhibited woman not only established her stardom, but also established a modern embodiment of a vixen. Lola-Lolas lusty songs, written by Friedrich Hollaender (music) and Robert Liebmann (lyrics), slither their way into Raths heart, entrapping him and sealing his fate. The storys melancholic simplicity adds to the beauty of Sternbergs most remembered work, in both Germany and America. Dietrichs radiant sensuality might be blamed for the censorship the film faced in Pasadena, California. Black, p.50  C.V. Cowan, censor for Pasadena, found many scenes offensive and chose to cut them, though Jason Joy, the nations censor, did not. Reaction to the censors seal for the re-cut film was not good, and the theater removed the censorship statement. 

During filming, although he was still the nominal star of the film, Jannings could see the growing closeness between Sternberg and Dietrich and the care the director took in presenting her, and the actor became jealous, threatening to strangle the actress and misbehaving on the set. The Blue Angel was to be his last great cinematic moment; it was also one of UFAs last great films, as many of the studios major talents left Germany for Hollywood, including Sternberg and Dietrich, who were met on the dock in New York City by Sternbergs wife, who served legal papers on Dietrich for "alienation of affection". Sternberg and his wife were divorced shortly after. 

==Subsequent history==
The Blue Angel was banned in Nazi Germany in 1933, as were all the works of Heinrich Mann and Carl Zuckmayer.

The German version is much better known. The English-language version was considered a lost film for many years until a print was discovered in a German film archive and restored. The restored English version had its U.S. premiere at the Castro Theatre in San Francisco on 19 January 2009 as part of the "Berlin and Beyond" film festival.  

==Parodies and adaptations== drag as On the The Damned.
* Adaptation:   ; with Curd Jürgens Sandhya and Shreeram Lagoo    Odeon Theatre in Bucharest, starring Florin Zamfirescu as the professor and Maia Morgenstern as Lola-Lola. David Thompson Stew and Heidi Rodewald providing the score, and Scott Ellis directing. 

==See also==
*List of German films of 1919–1932

==References==
Notes
 

Bibliography
*  
* , 1966)
* , 1968)
*Wakeman, John. World Film Directors Vol. 1 (New York: H.W. Wilson, 1987)

==External links==
*  
*  
*   
*  
*  
*  
*   at Odeon Theatre (stage production)  
*  

 

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 