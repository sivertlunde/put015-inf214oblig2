The Big Timer
{{Infobox film
| name           = The Big Timer
| image          =
| image_size     =
| caption        =
| director       = Edward Buzzell
| producer       =
| writer         = Robert Riskin (story and screenplay) Dorothy Howell (screenplay)
| starring       = Ben Lyon Constance Cummings Thelma Todd
| music          =
| cinematography =
| editing        =
| distributor    = Columbia Pictures
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Big Timer is a 1932 film about a boxer whose success goes to his head. It stars Ben Lyon, Constance Cummings, and Thelma Todd.

==Plot==
Cooky Bradford wants to make enough money to buy a lunch wagon. He ends up falling for, and fighting for, boxing manager Pop Baldwins daughter, Honey.

==Cast==
*Ben Lyon as Cooky Bradford
*Constance Cummings as Honey Baldwin
*Thelma Todd as Kay Mitchell
*Tom Dugan as Catfish
*Robert Emmett OConnor as Dan Wilson
*Charley Grapewin as John Pop Baldwin
*Russell Hopton as Sullivan
*Jack Miller Jr. as Scrappy Martin

==External links==
* 
*  
*  

 

 
 
 
 
 
 
 
 


 