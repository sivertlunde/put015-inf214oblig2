Ee Preethi Yeke Bhoomi Melide
{{Infobox film name           = Ee Preethi Yeke Bhoomi Melide image          =  director  Prem
|producer       = P. Krishna Prasad writer         = Prem starring  Prem   Namratha   Rohini music          = R. P. Patnaik   V. Harikrishna ( ) cinematography = M. R. Seenu editing        = Srinivas P. Babu studio         = Ashwini Productions released       =   runtime        = 162 min. country        = India language       = Kannada
}}
 Kannada romance Sharan in other pivotal roles. Bollywood actress Mallika Sherawat appeared in a special dance number  whilst actors Ambarish, Sudharani and Vinaya Prasad appeared in brief cameo roles.  The film had musical soundtrack scored by R. P. Patnaik whilst the background score was V. Harikrishna. 

The film opened on 28 December 2007 to mixed reviews and performed badly at the box-office. 


==Cast== Prem 
* Rohini
* Namratha Sharan
* Ramesh Bhat
* Raghu Ram
* Layendra
* Sihi Kahi Chandru Master Kishan
* Tejaswini Prakash
* Mallika Sherawat in an item song
* Ambarish in guest appearance
* Sudharani in guest appearance
* Vinaya Prasad in guest appearance
* B. Jayashree in guest appearance


==Soundtrack==
The films score and soundtrack was composed by R. P. Patnaik. 
{{tracklist
| headline        = Track-list
| extra_column    = Singer(s)
| all_lyrics      = Prem (film director)|Prem, Kaviraj (lyricist)|Kaviraj, V. Nagendra Prasad
| total_length    = 
| title1          = Neenene Neenene
| extra1          = Roop Kumar Rathod, K. S. Chithra
| length1         = 
| title2          = Magalu Doddavaladalu
| extra2          = C. Ashwath, Prem (film director)|Prem, Kalpana
| length2         = 
| title3          = Barayya Barayya
| extra3          = Shankar Mahadevan, Supriya Acharya
| length3         = 
| title4          = Chandamama Kaige
| extra4          = Shreya Ghoshal
| length4         = 
| title5          = Ee Jana Ee Mana
| extra5          = R. P. Patnaik, Nihaal
| length5         = 
| title6          = Chandamama Baa
| extra6          = Shreya Ghoshal 
| length6         =
| title7          = Kallanivanu
| extra7          = Nithyashree Mahadevan , Rajesh Krishnan
| length7         =
| title8          = Oh Huduga
| extra8          = Kunal Ganjawala, K. S. Chithra
| length8         =
| title9          = Sulle Sullu
| extra9          = Kailash Kher , Shankar Mahadevan
| length9         =
| title10          = Ee Preethi Yeke Prem 
| length10         =
}}

==References==
 


==External links==
* 

 

 
 
 
 
 
 