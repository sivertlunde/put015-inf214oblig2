Ezhu Muthal Onpathu Vare
{{Infobox film 
| name           = Ezhu Muthal Onpathu Vare
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = PKR Pillai
| writer         =
| screenplay     = Anuradha
| music          = KJ Joy
| cinematography = J Williams
| editing        = K Sankunni
| studio         = Shirdi Sai Creations
| distributor    = Shirdi Sai Creations
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, Anuradha in lead roles. The film had musical score by KJ Joy.   

==Cast==
*Mohanlal
*Ratheesh
*Captain Raju Anuradha

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by Cheramangalam and Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Hey Butterfly Njanoru || KS Chithra || Cheramangalam || 
|-
| 2 || Maathala Mottu || CO Anto, Krishnachandran || Poovachal Khader || 
|-
| 3 || Madanan Thirayum || Vani Jairam || Cheramangalam || 
|-
| 4 || Premageethikal || Vani Jairam, Chorus || Cheramangalam || 
|}

==References==
 

==External links==
*  

 
 
 

 