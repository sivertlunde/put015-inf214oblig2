Bigamie
{{Infobox film
| name           = Bigamie
| image          = 
| image_size     = 
| caption        = 
| director       = Jaap Speyer
| producer       = Max Glass
| writer         = Max Glass
| narrator       = 
| starring       = Heinrich George   Maria Jacobini  Anita Dorris   Ernő Verebes
| music          = Walter Ulfig
| editing        = 
| cinematography =  Arpad Viragh
| studio         = Terra Film
| distributor    = Terra Film
| released       = 27 September 1927
| runtime        = 
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} German silent silent drama film directed by Jaap Speyer and starring Heinrich George, Maria Jacobini and Anita Dorris.

==Cast==
* Heinrich George - Otto Engel 
* Maria Jacobini - Ada 
* Anita Dorris - Elise 
* Ernő Verebes - Tänzer Fred 
* Theodor Loos - Rechtsanwalt 
* Hans Mierendorff - Manager 
* Karl Etlinger - Geselle 
* Gerhard Ritterband - Lehrling 
* Maria Forescu - Artistin 
* Else Reval - Artistin 
* Emil Heyse   
* Emil Lind   
* Johannes Riemann   
* Georg H. Schnell

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.

==External links==
* 

 
 
 
 
 
 
 
 


 
 