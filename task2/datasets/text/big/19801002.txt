Faith, Hope and Witchcraft
 
{{Infobox film
| name           = Faith, Hope and Witchcraft
| image          = 
| image size     = 
| caption        = 
| director       = Erik Balling
| producer       = John Hilbert Carl Rald
| writer         = Erik Balling William Heinesen
| starring       = Bodil Ipsen
| music          = 
| cinematography = Poul Pedersen Erik W. Williamsen
| editing        = Carsten Dahl
| distributor    = Nordisk Film
| released       = 25 March 1960
| runtime        = 115 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Faith, Hope and Witchcraft ( ) is a 1960 Danish family film directed by Erik Balling and starring Bodil Ipsen. It was entered into the 10th Berlin International Film Festival.   

==Cast==
* Bodil Ipsen - Bedstemor Gunhild
* Gunnar Lauring - Jonas
* Louis Miehe-Renard - Enok
* Peter Malberg - Peter
* Poul Reichhardt - Rubanus
* Birte Bang - Eva
* Berthe Qvistgaard - Sally
* Jakob Nielsen - Huskarlen Jacob
* Hanne Winther-Jørgensen - Bruden
* Oscar Hermansen - Brudgommen
* Poul Müller - Præsten
* Freddy Koch - Sysselmanden
* Børge Møller Grimstrup - Nystuebonden
* Ole Larsen - Gammelstuebonden
* Kjeld Jacobsen - Bestyrer på Hvalfangerstationen
* Carl Ottosen - Nicolajsen
* Victor Montell
* Valsø Holm - Hotelejer
* Gunnar Strømvad
* Ernst Schou
* Sjurdur Patursson
* Frazer Eysturoy
* Else Kornerup
* Henry Lohmann
* Axel Strøbye - Vred mand
* Vilhelm Henriques
* Flemming Dyjak - Ung mand
* John Hahn-Petersen - Bartender
* Edith Hermansen
* Edith Trane
* Bertel Lauring
* Bjørn Spiro - Betjent

==References==
 

==External links==
* 

 
 
 
 
 