Westerplatte (film)
{{Infobox film
| name           = Westerplatte
| image          = 
| caption        = 
| director       = Stanisław Różewicz
| producer       = 
| writer         = Jan Józef Szczepanski
| starring       = Zygmunt Hübner
| music          = 
| cinematography = Jerzy Wójcik
| editing        = 
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = Poland
| language       = Polish
| budget         = 
}}
Westerplatte or Westerplatte Resists (Pl. Westerplatte Broni Sie Nadal)  is a 1967 Polish historical film directed by Stanisław Różewicz.  It was entered into the 5th Moscow International Film Festival where it won a Silver Prize.   

==Cast==
* Zygmunt Hübner as Maj. Henryk Sucharski
* Arkadiusz Bazak as Capt. Franciszek Dabrowski
* Tadeusz Schmidt as Ens. Jan Gryczman
* Józef Nowak as Cpl. Piotr Buder
* Tadeusz Pluciński as Cpl. Bronislaw Grudzinski
* Bogusz Bilewski as Capt. Mieczyslaw Slaby, Medical Officer
* Bohdan Ejmont as Mate Bernard Rygielski
* Mariusz Gorczynski as Pvt. Jan Czywil
* Zbigniew Józefowicz as Sgt. Kazimierz Rasinski, Radio Operator Jerzy Kaczmarek as Cpl. Wladyslaw Baran
* Andrzej Kozak as Pvt. Eugeniusz Aniolek
* Andrzej Krasicki as Col. Karl Henke
* Adam Kwiatkowski as Cpl. Wladyslaw Domon
* Józef Lodynski as Sgt. Wojciech Najsarek, Station-Master
* Mieczyslaw Milecki as Lt. Stefan Grodecki
* Janusz Mirczewski as Cpl. Franciszek Magdziarz

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 