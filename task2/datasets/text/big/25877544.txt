Bandits vs. Samurai Squadron
{{Infobox film
| name           = Bandits vs. Samurai Squadron
| image          = 
| caption        = 
| film name = {{Film name| kanji          = 雲霧仁左衛門
| romaji         = Kumokiri nizaemon}}
| director       = Hideo Gosha
| producer       = 
| writer         = Kaneo Ikegami
| starring       = Tatsuya Nakadai Junko Miyashita
| music          = 
| cinematography = 
| editing        = 
| studio         = Shochiku
| distributor    = 
| released       =  
| runtime        = 160 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}} Japanese film directed by Hideo Gosha.

==Cast==
*Tatsuya Nakadai as Kumokiri Nizaemon
*Shima Iwashita as Chiyo
*Matsumoto Kōshirō IX|Kōshirō Matsumoto as Shikubu Abe
*Takashi Yamaguchi as Tsugutomo Owari
*Tetsurō Tamba as Kichibei
*Keiko Matsuzaka as Shino
*Junko Miyashita as Menbiki Okyo Mitsuko Baishō as Omatsu

==Reception==
Jason Buchanan at AllMovie says that director Hideo Gosha makes a "triumphant return to the samurai genre with this plot twisting, nerve shredding tale",  but critic Alexander Jacoby calls it "a bland chanbara". 

==Awards and nominations==
21st Blue Ribbon Awards 
* Won: Best Supporting Actress - Junko Miyashita

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 


 