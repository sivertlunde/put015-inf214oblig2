Mamiya kyodai
{{Infobox film
| name           = Mamiya kyodai
| image          =
| caption        =
| imdb_rating    =
| director       = Yoshimitsu Morita
| producer       =
| writer         = Yoshimitsu Morita (screenplay)
| starring       = Kuranosuke Sasaki, Muga Tsukaji, Miyuki Nakajima, Takako Tokiwa, Erika Sawajiri, Keiko Kitagawa
| music          =
| cinematography =
| editing        =
| distributor    = Kadokawa Pictures
| released       =  
| runtime        = 119 min.
| country        = Japan
| language       = Japanese
| budget         =
| preceded by    =
| followed by    =
| mpaa_rating    =
| tv_rating      =
}}

  is a 2006 Japanese comedy film written and directed by  Yoshimitsu Morita, based on a novel by Kaori Ekuni. The films theme, Hey, brother, was performed by Rip Slyme. 

==Plot==
The film follows two thirty-something eccentric brothers (Akinobu and Tetsunobu), who are also each others best friends. Their mundane lives change when sisters Naomi and Yumi accept their invitations to a party and the brothers have to decide if they are ready to exchange their happy existence for the vicissitudes of love.

==Cast==
* Kuranosuke Sasaki as Akinobu Mamiya
* Muga Tsukaji as Tetsunobu Mamiya
* Miyuki Nakajima as Junko Mamiya
* Takako Tokiwa as Yoriko Kuzuhara
* Erika Sawajiri as Naomi Honma
* Keiko Kitagawa as Yumi Honma
* Hiromi Iwasaki as Miyoko Anzai
* Ryūta Satō as Kota

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 