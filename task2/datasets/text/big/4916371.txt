Omen (2003 film)
{{Infobox film
| name           = Sanghorn (Omen)
| image          = Omenposter.jpg
| caption        = Thai movie poster.
| director       = Thammarak Kamuttmanoch
| producer       =
| writer         = The Pang Brothers
| starring       = Kawee Tanjararak,  Panrawat Kittikorncharoen,  Worrawech Danuwong
| music          =
| cinematography =
| editing        = The Pang Brothers
| distributor    = Sahamongkol Film International
| released       = April 4, 2003
| runtime        = 87 minutes
| country        = Thailand
| language       = Thai
| budget         =
| preceded by    =
| followed by    =
}} Thai suspense Pang Brothers and starred Kawee Tanjararak (Beam), Panrawat Kittikorncharoen (Big) and Worrawech Danuwong (Dan) - all members of a popular Thai boyband at the time, D2B (band)|D2B.

==Plot==
Strange things start happening when separate twists of fate bring each of three friends, Big, Dan and Beam,  into contact with three strangers - an old woman who tells prophecies, a little girl who sells garlands on the street and a young woman named Aom. What none of them realize is that not only is there a connection between the three females, but also between the guys themselves and their destinies. But it starts to become clear that one them is fated to die. But which one? And why?

==Cast==
*Kawee Tanjararak as Beam
*Panrawat Kittikorncharoen as Big
*Worrawech Danuwong as Dan
*Supatchaya Reunreung as Aom

==Trivia==
*The three friends character names are the nicknames they use in real life.
*After the movie was released, Panrawat Kittikorncharoen (Big) was involved in a traffic accident in which his car overturned in a polluted khlong in Bangkok. A large amount of bacteria entered his body and he was in a coma. He made a recovery, but suffered brain damage. He eventually died on December 9, 2007 due to complications.
*Dan and Beam have continued in music as a duo but later on the two separated. Dan joined in Sony BMG and Beam stayed inn RS Public Company Limited

==External links==
* 
*  at the  
* 

 

 
 
 
 
 


 
 