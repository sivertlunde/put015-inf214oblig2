Star-Crossed Lovers (film)
{{Infobox film
| name           = Königskinder
| image          = 
| image size     = 
| caption        = 
| director       = Frank Beyer
| producer       = Hans Mahlich
| writer         = Edith and Walter Gorrish
| narrator       =
| starring       = Annekathrin Bürger, Armin Mueller-Stahl  
| music          = Joachim Werzlau
| cinematography = Günter Marczinkowsky
| editing        = Hildegard Conrad
| studio         = DEFA
| distributor    = PROGRESS-Film Verleih
| released       = 7 September 1962
| runtime        = 85 minutes
| country        = East Germany German
| budget         =
| gross          =
}}
 1962 East East German film directed by Frank Beyer.

==Plot==
Magdalena and Michael are two children from working-class families in Berlin, who have sworn to marry each other. When they grow older, after the Nazis rose to power, Michael is arrested for being a member of the Communist Party of Germany. Magdalena joins the underground party to continue his work. Jürgen, a friend of the two who is now a storm trooper, tries to convince her not to become a communist. During the Second World War, Michael is sent a penal battalion on the Eastern Front, where he meets Jürgen again as a commanding officer. Michael overpowers him, defects to the Red Army and returns to the battalion once more to convince the soldiers to surrender, thus saving their lives. He reaches Moscow, where he sees Magdalena board a plane. He tries to call out for her, but she does not hear him. They will never meet again.

==Cast==
*Annekathrin Bürger as Magdalena
*Armin Mueller-Stahl as Michael
*Ulrich Thein as Jürgen
*Manfred Krug as captain
*Marga Legal as Mrs. Seifert
*Betty Loewen as Hanna Bartels
*Monika Lennartz as Katya
*Gertraud Kreissig as Ursula
*Natalia Illina as Vera
*Talla Novikova as pilot
*Leonid Svetlov as Sasha
*Nikolai Lukinov as Red Army Major
*Walter Lendrich as Schröter
*Günter Naumann as Herbert
*Fred Delmare as Albert
*Erik Veldre as Hans

==Production== expressionist motiffs during the shooting, to recreate the atmosphere of Germany in the 1930s. Miera Liehm, Antonin J. Liehm. The Most Important Art: Soviet and Eastern European Film After 1945. ISBN 0-520-04128-3. p. 265. 

==Reception==
Star-Crossed Lovers won Frank Beyer a special Medal of Honor in the 13th Karlovy Vary International Film Festival.  

Daniela Berghahn considered the picture as a "prominent example" to the DEFA films that "transgressed the aesthetic boundaries of Social Realism."  Axel Geiss wrote that the film was a representative of "DEFAs most important tradition: the dealing with the Nazi past."  Paul Cooke and Marc Silberman commented that the antifascist cause was shown by the picture to be ultimately more important than the romantic ideals.  

At 1985, the film was withdrawn from circulation by the DEFA Commission, after Armin Mueller-Stahl and other members of the crew emigrated to West Germany. 

==References==
 

==External links==
*  
*   at    
*  on cinema.de

 

 
 
 
 
 
 
 