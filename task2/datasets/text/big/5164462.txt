Uuno Epsanjassa
{{Infobox film
| name          = Uuno Epsanjassa
| image         = Uuno Epsanjassa cover.jpg
| caption       = DVD cover of Uuno Epsanjassa.
| director      = Ere Kokkonen
| writer        = Ere Kokkonen
| starring      = Vesa-Matti Loiri, Marjatta Raita, Tapio Hämäläinen, Marita Nordberg
| producer      = Spede Pasanen
| distributor   = Filmituotanto Spede Pasanen Ky
| released      = 1985
| runtime       = 1h 45 min
| language      = Finnish
}}
 Finnish 1985 comedy film directed by Ere Kokkonen. It is the tenth film in the Uuno Turhapuro series. It was seen by more than 600,000 people in the theatres.

==Plot==
 correspondence course tour guiding. Eventually he gets a job in a small travel agency and takes a group of Finnish tourists to Marbella, Spain. Unfortunately Uunos father-in-law Tuura (Tapio Hämäläinen) is in the group, too, with his wife (Marita Nordberg) and daughter, Uunos wife Elisabet (Marjatta Raita). Tuura tries to get a signature to an important paper from a minister whos having a holiday in the area. Meanwhile, Uuno just relaxes and enjoys the sun.

==Miscellaneous==
"Epsanjassa" is slang term of the word "Espanjassa" (in Spain). "Epsanja" itself is more lower-class slang of word of "Espanja", Finnish for Spain.

==External links==
*  

 

 
 
 
 

 