Mysteries of a Barbershop
{{Infobox film
  | name           = Mysteries of a Barbershop
  | image          = MYSTERIES OF A BARBERSHOP VALENTIN W HEAD.jpg
  | imagesize      = 
  | caption        = Karl Valentin as the Barber.
  | director       = Erich Engel
  | producer       = 
  | writer         = Bertolt Brecht, Karl Valentin
  | starring       = Erwin Faber, Karl Valentin, Max Schreck, Blandine Ebinger, Josef Eichheim, Annemarie Hase, Kurt Horwitz, Liesl Karlstadt, Hans Leibelt, Carola Neher, Otto Wernicke 
  | music          = silent
  | cinematography = 
  | distributor    = 
  | released       = 1923 (created, but not released until rediscovered in 1972)
  | runtime        = 33 min. (one reeler)
  | country        = Germany German
  | budget         = 
  }}

Mysteries of a Barbershop ( ) is a comic, slapstick  German film of 33 minutes, created by Bertolt Brecht, directed by Erich Engel, and starring the Munich cabaret clown Karl Valentin and leading stage actor Erwin Faber.  Brecht reportedly did not write a complete shooting script, but rather produced "notes" and "parts of a manuscript" (according to Faber) for this short, silent film and intended the actors to improvise the action.   Although the film was not considered a success by any of its creative team, and consequently never released as a profit making film to the public, it has been recognized and acknowledged—since its re-discovery in a Moscow archive in the 1970s—as one of the 100 most important German films of all time. 

==  "A Little Joke" by Bertolt Brecht ==

 , the Barber, and Erwin Faber, Dr. Moras, accidentally shaved to look like a Chinaman.]]In an interview with Erwin Faber, who played Dr. Moras—the "romantic star" of the film—it was clear that Mysteries of a Barbershop was intended by Brecht in this, his first attempt at filmmaking, to be nothing more than "just a little joke." 
 Happy End and the role of Polly in the film of Threepenny Opera) and the cabaretist (and wife of song writer, Friedrich Hollaender), Blandine Ebinger.  The group improvised a series of comic and mock-romantic scenes, which, according to one critic, "contains enough cruelty jokes to have made WC Fields envious." 

== Plot ==
  and Blandine Ebinger embracing in the Senegalese Salon after "The Duel".]]The plot revolves around Dr. Moras (Faber) who visits a barber (Valentin), who accidentally shaves Moras to look like a Chinese person (above, left), and then mistakenly cuts off Moras rivals head (photo, above, right), which is sewn back on by the barbers assistant (Ebinger), and ends with a sword fight - "The Duel" - and in which Faber is triumphant (actually, saved by the girl!) and Ebinger and Faber embrace in a happy ending in a mysterious Senegalese Salon (photo right).

The finale of Faber and Ebinger kissing (photo, right) illustrates one of Brechts first uses of the mock-romantic "happy ending" that would become a signature of Brechts work throughout the years of the Weimar Republic. 

One critic aptly called the short film "dadaesque absurdity combine  with clownesque slapstick."   Another reviewer called it "Karl Valentin meets Dada and the Marx Brothers." 

== Production ==

* Director: Erich Engel (with a little help from Bertolt Brecht)
* Script: Bertolt Brecht (with Karl Valentin and quite possibly Erich Engel)
* Actors: Erwin Faber, Karl Valentin, Max Schreck, Blandine Ebinger, Josef Eichheim, Annemarie Hase, Kurt Horwitz, Liesl Karlstadt, Hans Leibelt, Carola Neher, Otto Wernicke.

== References ==
 

== External links ==
*  
*  Video blocked(removed) from Youtube purportedly to copyright claim by Karl Valentin

 
 
 
 
 