Rurouni Kenshin: Kyoto Inferno
{{Infobox film
| name           = Rurouni Kenshin: Kyoto Inferno
| image          = Rurouni Kenshin, Kyoto Inferno film poster.jpeg
| alt            = 
| caption        = 
| director       = Keishi Ōtomo
| producer       = Satoshi Fukushima
| writer         = 
| based on       =  
| starring       = Takeru Satoh Emi Takei Tatsuya Fujiwara Yosuke Eguchi Munetaka Aoki Yūsuke Iseya
| music          = Naoki Satō
| cinematography = Takuro Ishizaka
| editing        = 
| studio         = 
| distributor    = Warner Bros.
| released       =   
| runtime        = 139 minutes
| country        = Japan
| language       = Japanese
| budget         = US$30 million (shared with  ) 
| gross          =   (international)
}} Rurouni Kenshin.

==Plot==
Kenshin has settled into his new life with Kaoru and his other friends when he is approached with a request from the Meiji government. Makoto Shishio, a former assassin like Kenshin, was betrayed, set on fire and left for dead. He survived, and is now in Kyoto, plotting with his gathered warriors to overthrow the new government. Against Kaorus wishes, Kenshin reluctantly agrees to go to Kyoto and help keep his country from falling back into civil war.

==Cast==
*Takeru Satoh as Himura Kenshin
*Emi Takei as Kamiya Kaoru
*Munetaka Aoki as Sagara Sanosuke
*Kaito Ōyagi as Yahiko Myojin
*Tatsuya Fujiwara as Makoto Shishio
*Ryunosuke Kamiki as Seta Sōjirō 
*Yu Aoi as Takani Megumi
*Maryjun Takahashi as Komagata Yumi   
*Ryosuke Miura as Sawagejō Chō
*Yūsuke Iseya as Shinomori Aoshi
*Tao Tsuchiya as Makimachi Misao
*Yōsuke Eguchi as Saitō Hajime (Rurouni Kenshin)|Saitō Hajime Okina
*Masaharu Fukuyama as Hiko Seijuro

==Release==
DVD release was on December 17, 2014.

==Reception==
On the box office the movie was well received, with the movie earning a total of   internationally.  The film also held the top spot at the box office in Japan during its first week. 

==Sequel==
 

==Music==
ONE OK ROCKs song Mighty Long Fall is featured in the movie. 

==References==
 

==External links==
*  
*  

 

 
 


 