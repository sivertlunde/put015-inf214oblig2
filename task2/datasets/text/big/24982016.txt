A Little Game
{{Infobox film
| name           = A Little Game
| image          = 
| alt            =  
| caption        = 
| director       = Paul Wendkos
| producer       = George Eckstein Richard Irving
| writer         = Carol Sobieski
| story          = Fielden Farrington
| starring       = Diane Baker Ed Nelson Howard Duff Katy Jurado
| music          = Robert Prince
| cinematography = Harry L. Wolf
| editing        = Michael Economou
| studio         = 
| distributor    = 
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
A Little Game is a 1971 ABC Movie of the Week that was first broadcast on October 30, 1971, starring Mark Gruner as a young boy who will do anything to get what he wants. He is responsible for the deaths of fellow students at the military academy he attends, yet his mother (Diane Baker) refuses to believe that he is guilty of anything. His stepfather (Ed Nelson) begins to wonder if the boy wants to get rid of him. It was based on the 1968 novel of the same name by Fielden Farrington who also wrote the screenplay.  The film was directed by Paul Wendkos who would film another of Farringtons novels for television Strangers in 7A the following year.


==External links==
* 

 

 
 
 
 
 
 
 



 