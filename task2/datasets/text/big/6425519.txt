Where Angels Fear to Tread (film)
 
 
{{Infobox film
| name           = Where Angels Fear to Tread
| image          = Forster_Angels.JPG
| caption        = Original poster
| director       = Charles Sturridge
| producer       = Derek Granger Tim Sullivan Derek Granger Charles Sturridge Based on the novel by E.M. Forster
| starring       = Rupert Graves Helena Bonham Carter Judy Davis Helen Mirren Barbara Jefford
| music          = Rachel Portman
| cinematography = Michael Coulter
| editing        = Peter Coulson
| studio          = Sovereign Pictures
| distributor    = Fine Line Features (US) Rank Film Distributors (UK) 
| released       = 21 June 1991
| runtime        = 116 minutes
| country        = United Kingdom
| language       = English
| budget         =
}}
 British drama Tim Sullivan, novel of the same title by E. M. Forster.

==Plot==
 Tuscan town of Monteriano with her young friend Caroline Abbott. There she falls in love with both the countryside and Gino Carella, a handsome man considerably younger and much less wealthy than herself, and she decides to stay. Appalled by her behaviour and concerned about the future of her granddaughter, Irma, her strait-laced mother-in-law dispatches her son Philip to Italy to convince her to return home, but by the time he arrives Lilia and Gino have wed. He and Caroline return home, unable to forgive themselves for not putting an end to what they see as a clearly unsuitable marriage.

Lilia is startled to discover her desire for independence is at odds with Ginos need to be the unquestioned head of the household, and she is shocked when he becomes physically abusive to clarify his position. Their relationship becomes less volatile when Lilia becomes pregnant, but she dies in childbirth, leaving her grieving husband with an infant son to raise with the help of his ageing mother.
 xenophobic Harriet, but Philip and Caroline both begin to find themselves attracted to everything Tuscan that had appealed to Lilia. Philip and Caroline also begin to sympathise with Gino and his loving relationship with his son, but though Philip says he understands everyone, he vacilates to even broach the subject of getting custody of the boy to Gino. Philip cant seem to settle it, and do the right thing, as Caroline reminds him. Harriet is left to take matters into her own hands and makes a decision that leads to tragic consequences.

In contrast to the Where Angels Fear to Tread|novel, the film adds a positive ending to the changes in the story, by hinting that love between Caroline and Philip may be possible.

==Production==
The film was shot on location in San Gimignano in the province of Siena.

==Cast==
*Rupert Graves ..... Philip Herriton
*Helena Bonham Carter ..... Caroline Abbott
*Judy Davis ..... Harriet Herriton
*Giovanni Guidelli ..... Gino Carella
*Helen Mirren ..... Lilia Herriton
*Barbara Jefford ..... Mrs. Herriton
*Sophie Kullmann ..... Irma

==Critical reception==
Janet Maslin of the New York Times observed the film "has been faithfully but unimaginatively directed by Charles Sturridge, whose . . . principal asset here is a very fine cast. The actors perform flawlessly even when the staging is too pedestrian for the ideas being expressed, and when the films flat, uninflected style allows some of those ideas to be overlooked or thrown away . . . Mr. Sturridges assault on his material is strictly frontal, with a screenplay . . . that adequately summarizes the novel but rarely approaches its depth. Although the film stumbles unimaginatively over some of Forsters more elaborate scenes . . . and although it moves gracelessly back and forth between Italy and England, its most significant lapse is visual. Tuscany, as photographed by Michael Coulter, is never as ravishing as it deserves to be either for strictly scenic purposes or for illustrating Forsters view of Italys magnetic allure. Even so, the material and the performances often rise above these limitations. At its occasional best, Where Angels Fear to Tread even captures the transcendent aspects of Forsters tale." 

Roger Ebert of the Chicago Sun-Times said the film "is rather unconvincing as a story and a movie; Forster had not yet learned to bury his themes completely within the action of a novel, as he does so brilliantly in Howards End. There are also some problems with the casting – especially that of Giovanni Guidelli, who never seems like a real character and is sometimes dangerously close to being a comic Italian. The tug-of-war over the baby is uncomfortably melodramatic, and the whole closing sequence of the movie seems written, not lived. There are some good things, especially Mirrens widow, tasting passion and love for the first time, and Davis sister, a prototype for all those dreadnought British spinsters for whom false pride is a virtue, not a sin." 

Rita Kempley of the Washington Post thought Sturridge "seems less like a driven director than an impersonal subtitler. He takes no liberties with the material; he merely translates the story from page to screen. On the whole, its rather like reading without the effort of holding the book. For many, this will do quite nicely, thank you. Others will find it all too stranglingly Anglophilia|Anglophilic, which is perhaps the point." 
 A Passage A Room with a View." 

Owen Gleiberman of Entertainment Weekly graded the film C, noting "except for Helen Mirrens brief, mischievous performance . . . the movie remains frustratingly distant from its characters inner lives." 
 A Handful of Dust, the screenplay degenerates into a static succession of talking heads. Sturridges work still seems to be TV masquerading as cinema." 

==Awards and nominations==
Judy Davis won the Boston Society of Film Critics Award for Best Supporting Actress for her performance in both this film and Husbands and Wives. 

==DVD release==
Image Entertainment released the film in anamorphic widescreen format on Region 1 DVD on 7 November 2006. The only bonus feature is the original trailer.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 