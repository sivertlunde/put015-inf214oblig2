Return to Paradise (1953 film)
{{Infobox film
| name           = Return to Paradise
| image          = Return to Paradise 1953 poster.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Mark Robson
| producer       =   Charles Kaufman
| story          = James Michener Barry Jones Roberta Haynes John Hudson
| music          = Dimitri Tiomkin
| cinematography = Winton C. Hoch
| editing        = Daniel Mandell
| distributor    = United Artists
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross = $1.8 million (US) 
}} South Seas Barry Jones, Return to Paradise, his sequel to Tales of the South Pacific. It was filmed on location in Matautu, Western Samoa (present-day Samoa). 

==Plot==
Mr. Morgan (Cooper) washes up on an island in the South Pacific, ruled by Pastor Corbett (Barry Jones), a missionary who rules as a religious despot. Morgan stays on the island despite his conflicting with Corbett and has an illegitimate child, Turia (Moira Walker), by island native Maeva (Haynes). After her death, he leaves the island, but also leaves her daughter behind. He returns to the island shortly before World War II, and finds that his daughter has fallen in love with an American pilot who crash-landed on the island. An interesting irony in the plot is that when Morgan first arrived, Corbett tried to force him to leave the island before getting one of the girls pregnant.  Then the very same things happened again at the end when his daughter,Turia, tried to have an affair with the American pilot.  Morgan intervened, and made the pilot leave.  In the last scene his daughter forgave him.

==Cast==

*Gary Cooper (Mr. Morgan) Barry Jones (Pastor Corbett)
*Roberta Haynes (Maeva)
*Moira Walker (Turia)
*John Hudson (Captain Harry Faber)
*Le Mamea Matatumua Ata (Tonga)
*Hans Kruse (Rori, age 21)
*Terry Dunleavy (Mac)
*Howard Poulson (Russ)
*Donald Ashford (Cutler)
*Herbert Ah Sue (Kura)
*Vaa (Rori, age 9)

==Soundtrack==
The title song "Return to Paradise" was featured in the soundtrack.

== DVD Release ==
Return to Paradise was brought to DVD on December 31, 2009 as part of the MGM Limited Edition Collection series.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 