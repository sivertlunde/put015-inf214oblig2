Who's Singin' Over There?
{{Infobox film
  | name =Whos Singin Over There?  Ko to tamo peva
  | image = Ko to tamo peva.jpg|
  | caption = DVD cover
  | director = Slobodan Šijan
  | producer = Milan Zmukić
  | writer = Dušan Kovačević Bata Stojković Aleksandar Berček Milivoje "Mića" Tomić|Mića Tomić Taško Načić Boro Stjepanović Slavko Štimac Neda Arnerić Miodrag Kostić Nenad Kostić
  | music = Vojislav Kostić
  | cinematography = Božidar "Bota" Nikolić
  | editing = Ljiljana "Lana" Vukobratović	
  | distributor = Centar Film (in SFR Yugoslavia) International Home Cinema (in United States|USA)
  | released = 1980
  | runtime = 86 minutes
  | country = Yugoslavia
  | language = Serbian
  | budget = Deutsche Mark|DEM180,000 
  }}

Whos Singin Over There? ( ) is a 1980 Serbian film written by Dušan Kovačević and directed by Slobodan Šijan. It was screened in the Un Certain Regard section at the 1981 Cannes Film Festival.   

==Plot==
On Saturday, April 5, 1941, one day before the   musicians, a World War I veteran, a Germanophile, a budding singer, a sickly looking man, and a hunter with a rifle. The bus is owned by Krstić Sr., and driven by his impressionable son Miško.

Along the way, they are joined by a priest and a pair of young newlyweds who are on their way to the seaside for their honeymoon, and are faced with numerous difficulties: a flat tire, a shaky bridge, a farmer whos ploughed over the road, a funeral, two feuding families, Krstić Jr.s recruitment into the army, and a lost wallet. All these slow the bus down and expose rifts among the travelers.

During the early morning of Sunday, April 6, amid rumours of war, they finally reach Belgrade only to be caught in the middle of the Luftwaffe raid (Operation Punishment). The only surviving passengers are the two Gypsy musicians who sing the films theme song before the end.

==Characters and situations==
Right from the start, through conversations about current events, issues of the day, and general chit-chat, the characters individual traits and sensibilities are clearly established. None of the passengers refer to each other by name considering theyre seeing each other for the first time and dont seem too keen on forging any deeper relationships. Although a comedy on the outer level, the movies layers reveal many observant details that indicate the Serbian societys structure and atmosphere at the time.

===Veteran===
The old World War I veteran (played by Mića Tomić) is a cranky oldtimer on his way to Belgrade to visit his recently drafted son. Hes extremely gratified that his successor is continuing the familys proud military tradition and doesnt miss an opportunity to let anybody caring to listen know about it. Hes also the only passenger whose name (Aleksa Simić) we learn when he defiantly states it along with his military regalia during a random conversation.

Being extremely irritable, he is very cantankerous in his dealings with the other passengers. He basically only addresses them when hes got something to complain and vent his anger about.

The old man expresses non-verbal concern about German rampage through Europe which was already in full swing at the time, but doesnt indicate a shred of fear regarding a possible attack on Yugoslavia. At one point, in response to Germanophiles praise of German medicine and work ethic, he confidently and angrily retorts: "I gather youd kinda like it if those scumbags came again. Bring em on, I was picking them off like fleas round these parts in 1917".
 dinar he can get". Krstić Sr. refuses, to which budding singer chimes in cynically from the back of the bus: "Cmon, let the old-timer ride for free, you can see he doesnt have a pot to piss in". Steamed by the snide comment, the veteran hops to his feet fuming and demanding to be sold 5 tickets in order to prove his buying power. Krstic Sr. turns him down again, this time angrily ("Stop annoying me old man"), and sells him a single ticket.

===Germanophile=== Bata Stojković) is the polar opposite of the impulsive veteran, hence serving as a character foil to him. Speaking in proper and pointed tone, he displays constant subtle disdain for the surroundings he moves about in. From his straight posture with a nose pointed upwards to frequent condescending remarks, everything about him seems to indicate a personal opinion about being too good for people that surround him.

It is never explicitly indicated whether hes a Nazi sympathizer. He is, however, in utter awe of all aspects of German lifestyle. In the above testy discussion with the veteran, his parting shot regarding a possible Nazi invasion of Yugoslavia is: "Well, listen sir, at least wed see some order around here".

The Germanophile is quick to criticize. While talking to the consumptive about a personal propensity for collecting rare rocks and folk tales, he laments the lack of interest in his activities that he encounters from everyday people.
 peeped on by the rest of the passengers, he remarks while watching them in disgust: "Have they no shame?". When reminded by the budding singer about the fact that "we came here to watch them", the Germanophile reasons that "that has no relevance". Later on the bus, the humiliated young bride offers him her seat, but he vehemently refuses: "Youve shown yourself in that bush - who you are and what you are".

Still later when it is discovered that the veterans wallet is missing and as Krstic Sr. announces hed search every one of the passengers until wallet is produced, the Germanophile stops him: "Why bother all these honest people when we know who around here likes to steal" pointing to two Gypsies, thus prompting their wrongful beating since the wallet was dropped along the way.

The speech patterns, manners, and visual appearance of the Germanophile are constructed in order to bear a very clear resemblance to Milan Stojadinović, the pro-German authoritarian conservative Prime Minister of Yugoslavia between 1935 and 1939.

===Hunter===
With his Elmer Fudd-like appearance and tipsy walking style, the hunter is a spaced-out goofball used mostly for comic relief. After initially being late for the bus, he attempts to board it by hailing it midway. This leads to an absurdly funny scene in which Krstic Sr. decides to exercise some misplaced authority by refusing to allow him onto the bus, reasoning: "The stop is 200m downhill. You want someone to see me letting you on outside it and fine me?", despite their surroundings at the time being a complete wasteland with absolutely no signs of life, and the fact that the Krstic Sr. is already transporting piglets in the back of the bus, which is also clearly not allowed.

The hunter is later thrown off the bus by Krstic Sr. when his rifle accidentally goes off. This was before he had the passengers angry for accidentally almost shooting the Germanophile while firing at a rabbit during an extended stopover and accidentally  shooting from an army cannon...

The hunter is played by Taško Načić.

===Budding singer===
The budding singer (played by Dragan Nikolić) is a small-town dandy with dreams of turning schlager signing into a career and achieving fame. His reason for travelling to Belgrade is an audition for a signing stint at Lipov lad bohemian kafana. Since the other passengers are older and of not much interest to him, he mostly communicates with the young bride through flirtatious banter to which she seems very receptive. When the group discovers the young married couple having sex in the woods and gathers to peep on them, the singer famously quips: "The kid doesnt have a clue" - referring to the young grooms seeming awkwardness in the sack. During the peeping, the Hypochondriac inadvertently coughs and the Singer disdainfully remarks: "May God let you cough to death!", disappointed because the cough was heard by the couple, making them stop. He later openly makes a pass at the bride, offering her to stay with him in Belgrade.

===Newlyweds===
The young newlyweds (played by Slavko Štimac and Neda Arnerić) offer a light-hearted injection of comedy and lust. The young bridegroom is obsessed with only two things: his constant eating and his irrepresible lust for his young bride. He seems singularly unaware, even as the singer makes every effort to seduce his young bride. The bride, meanwhile, wants nothing more than to see the big world—or at least to see Belgrade and the sea.  The most memorable point in their performance is probably their comic scene of lovemaking in the bushes, while the rest of the passengers secretly assemble to watch.

===Consumptive===
From the very opening, the consumptive (played by Boro Stjepanović) is seen making health-related complaints and producing a hacking cough while holding a handkerchief up to his mouth. He is a bald, middle-aged, frail looking man with defeated demeanor and dark outlook on life that only add to his overall image of a doomed weakling.

Despite constant hacking and complaining, hes seen running and engaging in other physical activities in several scenes, leading to conclusion that some of his health issues might be exaggerated. At first some of the passengers offer positive reinforcement, but soon give up after facing his strong intent on feeling and displaying self-pity. On the other hand, he really does seem to have tuberculosis — then a common disease — as at one point he coughs out some blood onto his handkerchief.

===Krstić Sr.===
A very colorful character, Mr. Krstić is the owner of the old bus the story takes place in. He is in his late 50s, overweight, seemingly harsh and greedy but very supportive of his son, and determined to help his child and himself in life. He is the owner of the bus, but he is also a pig merchant and a sort of con man. Towards the beginning of the film, he loads several pigs onto the back of the bus, despite the obvious disgust of the passengers. "When I sell the pigs in Belgrade, Ill earn more money than your tickets brought me", he says. He is also quick to criticize and throw people off his bus, but only after they have paid the ticket. His character evolves from a stereo-typical grouch (although non-malevolent) at the beginning of the movie, to a loving, caring father towards the films end. In the peeping scene, when Krstic Jr. says: "Dad, I would like to do this too", he, gaping at the young couple exhilarates: "Dad would as well, son" - a sentence which later became well-known and beloved.

===Gypsy musicians===
The two Gypsy musicians provide a running commentary through the film, like a Greek chorus. One of them plays an accordion and sings, while the other plays a Jews harp. The movie begins with them singing their recurring song, to which the refrain is "Im miserable, I was born that way, I sing to sing my pain away... dear Mama, I wish this was just a dream." Near the end of the film, they are accused of stealing the veterans wallet, and during the ensuing scuffle the bus is bombed. The musicians are the only survivors, and after they crawl out of the wreckage, the movie ends with them singing their song.

==Production==
Centar Film, the state-owned production house, wanted to make Dušan Kovačevićs script into a movie since 1978.

Reportedly, Goran Paskaljević was their first choice to direct the movie. He was supposed to shoot the Kovačević script as a contemporary-themed 50-minute TV movie set in the late 1970s on a public transport bus with the central character being an old man (played by Mija Aleksić) who is headed to visit his son in the army. However, Paskaljević decided to leave the project and shoot the feature film Zemaljski dani teku instead. The job then went to 33-year-old Slobodan Šijan who had never shot a feature film up to that point.   

The movie was made on a budget of US Dollar|US$130,000 with 21 shooting days.  The filming began on 3 April 1980.  It was shot almost entirely in Deliblatska Peščara.
 6 April Tito died, circus tour that was meant to provide the animals for the scene. Since working with untrained zoo animals was deemed too dangerous, the film-makers reluctantly had to abandon the idea at the time. However, the idea was used in the opening scene of Underground (1995 film)|Underground (1995), some 15 years later. 

In 2004 the film was turned into a ballet by the National Theatre in Belgrade.  The music is again by Vojislav Kostić and the choreography is made by Staša Zurovac.

==Cast==
*Pavle Vujisić &mdash; Conductor
*Dragan Nikolić &mdash; Singer
*Danilo "Bata" Stojković &mdash; Germanophile
*Aleksandar Berček &mdash; Miško Krstić
*Neda Arnerić &mdash; Bride
*Mića Tomić &mdash; Aleksa Simić
*Taško Načić &mdash; Hunter
*Boro Stjepanović &mdash; Consumptive
*Slavko Štimac &mdash; Bridegroom
*Miodrag Kostić &mdash; 1st Musician
*Nenad Kostić &mdash; 2nd Musician
*Bora Todorović &mdash; Mourner
*Slobodan Aligrudić &mdash; Lieutenant
*Petar Lupa &mdash; Priest
*Stanojlo Milinković &mdash; Plowman
*Ljubomir Ćipranić &mdash; Corporal
*Milovan Tasić &mdash; Plowmans son

==Reception and reaction==
The movie became an instant classic all over SFR Yugoslavia immediately upon its release. To this day, almost three decades later, it endures as one of the most quotable movies ever to come out of the Balkans. Different scenes and dialogues are almost a part of general knowledge, with many entering public vernacular.

The film earned the special jury award at the 1981 Montréal World Film Festival.

In 1996, members of the Yugoslavian Board of the Academy of Film Art and Science (AFUN) voted this movie as the best Serbian movie made in the 1947-1995 period. 

==See also==
*List of Yugoslavian films

==References==
 

==External links==
* 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 