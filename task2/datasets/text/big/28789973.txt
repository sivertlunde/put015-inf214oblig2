The Tuttles of Tahiti
{{Infobox film
| name           = The Tuttles of Tahiti
| image	         = The Tuttles of Tahiti FilmPoster.jpeg
| image_size     - 225px
| caption        = theatrical poster
| director       = Charles Vidor
| producer       = Sol Lesser Robert Carson James Hilton (adaptation)
| based on       = novel: No More Gas by James Norman Hall Charles Nordhoff
| starring       = Charles Laughton
| music          = Roy Webb
| cinematography = Nicholas Musuracs
| editing        = Frederic Knudtson
| studio         = RKO Radio Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = $847,000 Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p171 
| gross          = 
}}
 Jon Hall. It was based on the novel No More Gas by James Norman Hall and Charles Nordhoff.

==Plot== Jon Hall) fighting rooster cockfight with the more industrious and prosperous Emily (Florence Bates). 
 mortgage for the rundown family mansion and bets everything on the outcome. However, the bird turns out be a coward and flees the ring without a fight. 

Chester notices that Emilys daughter Tamara (Peggy Drake) has grown into a beautiful young woman, but the young lovers realize that Emily will never sanction Tamaras marriage to a penniless wastrel.
 salvage laws, they are now its owners. Jensen buys it and its cargo for 400,000 francs, an enormous sum.

Ignoring Emilys advice to invest the money, Jonas deposits it in a joint checking account, withdraws just enough to pay back Dr. Blondin, and gives checkbooks to everyone in the family. With their new wealth, Chester is able to marry Tamara. However, creditors descend on Jonas, and the spendthrift Tuttles soon spend the rest of their money very quickly.

When Jensen comes to collect the mortgage, Jonas cannot find the money he had saved for Blondin, and Jensen takes possession of the mansion. While chasing Chesters rooster, he finds the misplaced money and triumphantly gives it to Blondin, saving the Tuttle home. In the end, Blondin gives Jonas a new loan to buy gas for the fishing boat.

==Cast==
*Charles Laughton as Jonas Tuttle Jon Hall as Chester Tuttle
*Peggy Drake as Tamara
*Victor Francen as Dr. Blondin
*Gene Reynolds as Ru Tuttle, Jonass grandson
*Florence Bates as Emily
*Curt Bois as Jensen
*Adeline De Walt Reynolds as Mama Ruau, Jonass mother
*Ray Mala as Nat

==Reception==
The film recorded a loss of $170,000. 
==References==
Notes
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 