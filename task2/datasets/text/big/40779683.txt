Beside Still Waters (film)
 
 
}}

 
{{Infobox film
| name = Beside Still Waters
| image = 
| image_size = 
| border = 
| alt = 
| caption = 
| director = Chris Lowell
| producer = Chris Lowell
| writer = Chris Lowell Mohit Narang Reid Scott Beck Bennett Will Brill Jessy Hodges Erin Darke
| music = 
| cinematography = Timothy Naylor
| editing = Nick Houy
| studio = Storyboard Entertainment
| distributor = 
| released =  
| runtime = 76 minutes
| country = United States
| language = English
| budget = $207,061
| gross = 
}}
Beside Still Waters is an American comedy-drama film co-written, produced, and directed by Chris Lowell in his directorial debut. The film was released on November 14, 2014. 

==Plot== reality TV star James, recently unemployed Tom, Martin and his wife Abby, Charley, and Daniels ex-girlfriend Olivia and her fiancé Henry.

==Cast==
* Ryan Eggold as Daniel
* Britt Lower as Olivia
* Brett Dalton as James Reid Scott as Henry
* Beck Bennett as Tom
* Will Brill as Martin
* Jessy Hodges as Charley
* Erin Darke as Abby

==Release== world premiere Tribeca Film bought the film and had set a limited theatrical release date for November 14, 2014, and a VOD release date of November 18, 2014.  

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 


 