Wow (film)
{{Infobox film
| name           = Wow
| image          =
| caption        =
| director       = Claude Jutra
| producer       = Robert Forget
| writer         = Danielle Bail Pierre Charpentier 
| narrator       =
| starring       = Danielle Bail Pierre Charpentier Philippe Dubé Dave Gold Marc Harvey François Jasmin Michèle Mercure Philippe Raoul Monique Simard
| music          = Pierre F. Brault Jim Solkin
| cinematography = Gilles Gascon André-Luc Dupont
| editing        = Claire Boyer Yves Dion Claude Jutra
| distributor    = National Film Board of Canada
| released       =  
| runtime        = 94 min.
| country        = Canada French
| budget         =
}}
Wow was a 1969 French-speaking Quebecer|Québécois film directed by Claude Jutra, produced by the National Film Board of Canada.

==Synopsis==
In the film, nine teenagers get to act out their wildest dreams.

==Production==
Subjects in the film included some participants Jutra had worked with in his 1966 mockumentary film The Devils Toy (Rouli-roulant), a faux-anti-skateboarding propaganda film.      

==Sequel==
Thirty years after the production of Wow, the NFB co-produced a sequel Wow 2, using the same concept of adolescents acting out their dreams.   This film was directed by Jean-Philippe Duval and co-produced by  , who was a participant in the original film.

==References==
 

==External links==
* 
*Watch   at the National Film Board of Canada (in French)

 
 
 
 
 
 
 
 

 
 