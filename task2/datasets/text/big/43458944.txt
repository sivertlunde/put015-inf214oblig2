Stark Mad
{{infobox_film
| title          = Stark Mad
| image          =
| imagesize      =
| caption        =
| director       = Lloyd Bacon Frank Shaw(asst director)
| producer       = Warner Brothers
| writer         = Jerome Kingston(story)  Harvey Gates(screenplay) Francis Powers(titles;silent version)uncredited
| starring       = H. B. Warner
| music          =
| cinematography = Barney McGill
| editing        = Ralph Dawson
| distributor    = Warner Bros.
| released       = February 2, 1929
| runtime        = 70 minutes; 7 reels
| country        = USA
| language       = English
}}
Stark Mad is 1929 early sound film produced and distributed by Warner Brothers and directed by Lloyd Bacon. It starred silent screen greats H. B. Warner, Louise Fazenda, Jacqueline Logan and Henry B. Walthall. 

==Plot==
James Rutherford has organized an expedition to the jungles of Central America to find his missing son, Bob, and his guide, Simpson. Professor Dangerfield intercepts the party, bringing with him Simpson, whose jungle experience has made him a raving maniac. They go ashore and decide to spend a night at a Mayan temple. After Irene, Bobs fiancée, disappears, they come across a gigantic ape chained to the floor, and Captain Rhodes, commander of the yacht, is abducted by a strange monster with great hairy talons. Messages are found warning the party to leave. Sewald, an explorer, is mysteriously killed by an arrow. Simpsons reason returns, and he saves the party, revealing that the demented hermit, whom he has just killed, and who formerly occupied the ruins, murdered Bob two months before.

==Cast==
*H. B. Warner - Professor Dangerfield
*Louise Fazenda - Mrs. Fleming
*Jacqueline Logan - Irene
*Henry B. Walthall - Captain Rhodes
*Claude Gillingwater - James Rutherford
*John Miljan - Dr. Milo
*Andre Beranger - Simpson, a guide
*Warner Richmond - First mate
*Floyd Shackelford - Sam, a cook
*Lionel Belmore - Amos Sewald

==Preservation status==
The film was released in a silent version for theaters not converted to sound.   Both sound and silent versions are lost.   

When in February 1956, Jack Warner sold the rights to all of his pre-December 1949 films; including (Stark Mad) to Associated Artists Productions (which merged with United Artists Television in 1958, and later was subsequently acquired by Turner Broadcasting System in early 1986 as part of a failed takeover of MGM/UA by Ted Turner).

==References==
 


==External links==
* 
* 
* 

 

 
 
 
 
 