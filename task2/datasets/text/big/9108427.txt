By the People
 
{{Infobox film
| name           = By The People
| image          = By the People.jpg
| image size     = 190px
| screenplay     = Sunil Parameswaran Narain Kutty Nischal Sammad Bijoys Santosh B K Antony Anaitha Nair
| director       = Jayaraj 
| producer       = Sabu Cherian 
| banner         = Anandabhairavi|
| country        = India
| language       = Malayalam
| released       =  
| cinematography = Azagappan 
| music          = Pravin Mani
| choreography   = Brinda
|}}

By The People is a 2005 Malayalam film directed by Jayaraj starring Narain (actor)|Narain. It is the sequel of 4 the People (2004) and the prequel of Of the People (2008).             

== Synopsis ==
Two years after the events in 4 the People, a group of four students join up to fight corruption in education. They are angered with the suicide of a student who does not get a loan for her studies. The police department brings Rajan Mathew (Sunil) back from the administrative job that he had been shifted to for killing a student. 

Rajan’s investigation leads to a powerful kingmaker who controls the media and political parties. Rajan tapes the conversation. His wife is murdered, and he is crippled by the attack. He is now posted as in charge of jails. The gang that opposed corruption lead an attack on the kingmaker who lays a trap in which they get killed. Rajan employs a few members of the 4 the people, who are in jail, and uses his brains to annihilate the members of the caucus. 

An investigating officer (Satish Poduval) is close on the trail of the student gang and Rajan but gives up without getting any clue.

== Crew == Narain plays the lead role, while Kutty, Mangala, Nischal(actor)|Nischal, Sammad, Bijoys, Santosh, and B K Antony are also in the cast. The story is by director Jayaraj and the screenplay has been written by Sunil Parameswaran. Art direction is by Pradeep, costumes are by Kumar Edappal, script supervision is by Anwar Abdullah. Thayagarajan handles the action department and Kannan Sooraj is the still photographer.

The music is by Pravin Mani and all the songs have been sung by and featured on Tamil pop band S 5, comprising Anaitha Nair, Benny Dayal, Suvi Suresh, Bhargavi Pillai and Arjun Sashi. The Tamil hit single "Malaray," features as a Malayalam song.
== Cast ==
*Arun as Aravid Sebastain
*Padmakumar as Shafeek
* Arjun as Eswar Iyyer
*Narain as Rajan Mathew
*Mangala as Malli
*Sreejith Ravi as Sathyan
*Vinayakan as porter

==References==
 

== External links ==
* http://www.indiaglitz.com/channels/malayalam/trailer/7782.html
* http://entertainment.oneindia.in/malayalam/movies/by-the-people.html

 
 
 

 