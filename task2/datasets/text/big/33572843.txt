The Place Beyond the Pines
 
{{Infobox film
| name           = The Place Beyond the Pines
| image          = The Place Beyond the Pines Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Derek Cianfrance
| producer       = {{Plainlist|
* Lynette Howell
* Sidney Kimmel
* Alex Orlovsky
* Jamie Patricof
}}
| screenplay     = {{Plainlist|
* Derek Cianfrance
* Ben Coccio
* Darius Marder
}}
| story          = {{Plainlist|
* Derek Cianfrance
* Ben Coccio
}}
| starring       = {{Plainlist|
* Ryan Gosling
* Bradley Cooper
* Eva Mendes
* Ray Liotta
* Ben Mendelsohn
* Rose Byrne
* Mahershala Ali
* Bruce Greenwood
* Harris Yulin
}}
| music          = Mike Patton
| cinematography = Sean Bobbitt
| editing        = {{Plainlist|
* Jim Helton
* Ron Patane
}}
| production companies = {{Plainlist|
* Hunting Lane Films
* Pines Productions Sidney Kimmel Entertainment
* Silverwood Films
}}
| distributor    = Focus Features
| released       =  
| runtime        = 140 minutes 
| country        = United States
| language       = English
| budget         = $15 million    
| gross          = $35.5 million 
}} crime drama Blue Valentine. Mohawk word for "place beyond the pine plains."    
 
==Plot== motorcycle stuntman working in a traveling act for state fairs. During the fair in Altamont, New York, Luke is visited by his ex-lover, Romina, and learns he is the father of her infant son. Luke quits his job as a stuntman to stay in town and provide for the child, but Romina does not want him in the childs life, as she has become involved in a relationship with another man named Kofi.

Luke turns to Robin, an auto repair shop owner, for part-time employment as he repeatedly attempts to insert himself into his sons life. Earning little, Luke asks Robin for more money so he can contribute to his sons care. Robin reveals he was once a bank robber and offers to partner with Luke in hitting several local banks. They perform several successful heists, in which Luke does the robbery, then uses his motorbike as a getaway vehicle and drives it up a ramp into an unmarked truck driven by Robin. Luke uses his share of the money to win back Rominas trust and visits her and his son more often. Kofi objects to his presence and the two get into a fight at Kofis house, resulting in Lukes arrest after he hits Kofi in the head with a pair of Channelocks.

After Robin has bailed him out of jail, Luke wants to resume their bank robberies. Robin objects, not wanting to press their luck, and the two have a falling-out that results in Robin dismantling the motorbike and Luke, at gunpoint, taking back the bail money he had repaid Robin. Luke attempts to rob a bank alone and is pursued by police. He falls off his bike during the chase and seeks refuge in a single-family house, where he is pursued by Schenectady Police Officer Avery Cross. Luke corners himself upstairs and calls Romina. Just before Avery confronts him, Luke asks Romina not to tell their child about who he was. Avery enters the room and fires the first shot; Luke shoots Avery in the leg as he falls backwards out of the second-story window. Avery looks out the window to find Luke dying on the pavement.

Avery gains hero status after his takedown of Luke. Avery feels remorse about shooting Luke, especially as Averys fellow officers Scotty and Deluca illegally seize the stolen money from Rominas home and give him the lions share in honor of his newfound hero status. He later attempts to return the money to Romina, but she rejects his offer. Avery eventually tries to turn the money in to the chief of police, who dismisses him, wishing not to get involved. Following the advice of his father, a retired judge, Avery records a fellow officer asking him to remove cocaine from the evidence locker Avery is supervising for use in a separate case. Avery uses the recording to expose the illegal practices in the police department and pressures the district attorney to hire him as assistant district attorney.

Fifteen years later, Avery is running for public office and has to deal with his now-teenage son A. J., who has gotten into trouble with drugs. Avery has separated from his wife Jennifer and agrees to take A. J. into his home. A. J. transfers into the high school in Schenectady. There A. J. befriends a boy named Jason; neither A. J. nor Jason knows that Jason is Lukes son. The two are arrested for felony drug possession, and when Avery is called in to pick up his son, he recognizes Jasons name. He uses his influence to get Jasons charge dropped to a misdemeanor and orders A. J. to stay away from Jason, but the boys continue to talk.

Jason seeks the truth about his biological father, whom Romina refuses to discuss with him. His stepfather, Kofi, finally tells the boy his fathers name. He discovers Lukes past on the Internet. He visits Robins auto shop, and Robin tells Jason more about Luke, including his superior motorbiking skills. Back in school, A. J. invites Jason over to his house for a party and guilt-trips him into stealing Oxycontin for the party. Jason eventually gives in to A. J., and arrives with the drugs after stealing them from a pharmacy. At the house, Jason sees a framed photograph of Avery and realizes that A. J.s father is the man who killed his own father. After a fight with A. J., which leaves Jason hospitalized with facial injuries, Jason breaks into the Cross family home and beats A. J. at gunpoint. When Avery arrives, Jason takes him hostage and orders him to drive into the woods. Although Jason had intended to kill Avery, he reconsiders after Avery apologizes for killing Jasons father. Jason takes Averys wallet and jacket. In the wallet, Jason finds a photo of himself as a baby with his parents, which Avery had stolen from the evidence locker. Jason then leaves in Averys car.

Avery wins his bid for New York Attorney General, with A. J. at his side. Romina receives an envelope addressed to "Mom" with the old photograph of Jason with his parents. Jason leaves home and buys a motorbike, reminiscent of his fathers, and heads west, presumably intending to start anew.

==Cast==

* Ryan Gosling as Luke Glanton
* Bradley Cooper as Avery Cross
* Eva Mendes as Romina Gutierrez
* Ray Liotta as Peter Deluca
* Ben Mendelsohn as Robin Van Der Zee
* Rose Byrne as Jennifer Cross
* Mahershala Ali as Kofi
* Bruce Greenwood as Bill Killcullen
* Dane DeHaan as Jason Cankam
* Emory Cohen as A. J. Cross
* Harris Yulin as Al Cross
* Robert Clohessy as Chief Weirzbowski
* Olga Merediz as Malena

==Reception==
===Box Office=== Toronto International widely released on April 12, 2013.  The film grossed $279,457 from 4 theaters with an average of $69,864 per theater. The film ended up earning $21,403,519 in North America and $14,082,089 internationally for a total of $35,485,608, above its $15 million production budget. 

===Critical reception===
The Place Beyond The Pines received positive reviews from critics and has a "certified fresh" score of 80% on   based on 42 critics indicating "generally favorable reviews". 

Writing for the Indiewire "Playlist" blog, Kevin Jagernauth praised the film as an "ambitious epic that is cut from some of the same thematic tissue as Cianfrances previous film, but expands the scope into a wondrously widescreen tale of fathers, sons and the legacy of sins that are passed down through the generations".  David Rooney of The Hollywood Reporter praised the acting, cinematography, atmosphere, and score, but criticized the films narrative flow.  In The Daily Telegraph, Robbie Collin drew attention to the films "lower-key and largely unstarry third act" that was criticized in early reviews. "In fact, it’s the key to deciphering the entire film," he wrote. Collin drew parallels between Goslings character and James Deans Jim Stark in Rebel Without a Cause, and said Cianfrances film was "great American cinema of the type we keep worrying we’ve already lost." 

Henry Barnes of   s Ed Gonzalez, who criticized the films plot, themes, "self-importance", shallow characters, and melodramatic nature. 

====Top ten lists====
*5th — Randy Myers, San Jose Mercury News 
*6th — Kristopher Tapley, Hitfix  Vanity Fair 
*7th — Total Film 
*9th — Den of Geek 
*10th — Lisa Kennedy, The Denver Post 
*10th — Steve Persall, The Tampa Bay Times 
*No order — Stephen Witty, The Star-Ledger 
*No order — Claudia Puig, USA Today 

==References==
 

== Further reading ==
* LaSalle, Mick. " ". Houston Chronicle. April 4, 2013.

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 