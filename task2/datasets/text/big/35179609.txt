Aicha è tornata
 
 
{{Infobox film
| name           = Aicha è tornata
| image          = 
| caption        = 
| director       = Juan Martin Baigorria, Lisa Tormena
| producer       = Lisa Tormena, Juan Martin Baigorria, Gaia Vianello, Elisa Anzolin
| writer         = Gaia Vianello
| starring       = 
| distributor    = 
| released       =  
| runtime        = 34 minutes
| country        = Italy
| language       = Italian, French, Arab
| budget         = 
| gross          = 
| screenplay     = Gaia Vianello
| cinematography = Juan Martin Baigorria
| editing        = Juan Martin Baigorria, Lisa Tormena
| music          = Claudio Rocchetti
}}

Aicha è tornata is a 2011 Italian documentary film by Juan Martin Baigorria, Lisa Tormena and Gaia Vianello.  

== Synopsis ==
“I’ve come back… I miss my friends, the air in Italy… Here it’s like a prison.” The short sentences of the prologue summarize all the suffering of six migrants who have returned home to the provinces of Khouribga, Beni Mellal and Fkih Ben Salah, the so-called “Triangle of Death”, the main area of emigration from Morocco to South Europe. Often forced to return against their will, they experience the humiliation and frustration of returning to poverty, they are reabsorbed by their families and regret the freedom they won in Europe as women. 

== Trailer ==
http://vimeo.com/20434185

== Awards ==
* Mediterraneo Video Festival 2011 (Premio della Giuria Giovani al Miglior Documentario)
* Festival Italiano del Cinema Sociale 2011 (Premio speciale del Comune di Arezzo)
* XVIII° Premio Libero Bizzarri 2011 (Menzione speciale della giuria Medi(con)terraneo)

== Festivals ==

* Candiani Summer Fest 2011, Mestre, Italy  
* Estate Doc 2011, Mondiali Antirazzisti, Castelfranco Emilia,Italy  
* Mediterraneo Video Festival 2011, Agropoli, Italy  
* V° Terra di Tutti Film Festival 2011, Bologna, Italy  
* COOPI DOC "Storie dellAltro Mondo" 2011, Milano, Italy  
* Festival Italiano del Cinema Sociale 2011, Arezzo,Italy  
* XVIII° Premio Libero Bizzarri 2011,sezione Medi(con)terraneo, San Benedetto del Tronto, Italy  
* Sguardi Altrove Film Festival 2012, Milano, Italy  
* Festival del Cinema Africano,Asia e America Latina 2012, Milano, Italy  
* Foggia Film Festival 2012, fuori concorso, Foggia, Italy  
* Tolfa Film Festival 2012, Tolfa, Italy  
* Cinema dal Basso 2012, Caserta, Italy

== References ==
 

== See also ==
* Movies about immigration to Italy


 
 
 


 