Mounam Sammadham
{{Infobox Film | 
| name =  Mounam Sammadham
| image =  Mounam Sammadham.jpg
| caption =  Mounam Sammadham
| director =  K. Madhu   
| producer = Kovai Chezhiyan  Sembian Sivakumar
| writer =  S. N. Swamy Guna (dialogues) Amala  Charlie   R S Shivaji   Sreeja   Junior Balaiya   Prathapachandran   M. S. Thripunithura   Sarath Kumar   Santhana bharathi   Shanmugasundaram
| music = Ilaiyaraaja
| cinematography =  Vipin Das
| editing =  V. P. Krishnan
| studio = Kovai Chezhiyan|K. C. Film Combines
| distributor = Kovai Chezhiyan|K. C. Film Combines
| released = 15 June 1990
| runtime =  
| country = India Tamil
| budget = 
}}
 Tamil Crime crime legal thriller released in the year 1990. This movie was directed by K. Madhu and the music was composed by Ilaiyaraaja.    This is Mammootys first Tamil movie.

==Plot==
The plot revolves around the wrong conviction of a business man and the efforts taken by the protagonist, a lawyer, to find the truth.
The portrayal of the characters is similar in many ways to Malayalam cinemas investigative thrillers. The suspense is well maintained until the climax with little clue to the audience on the identity of the real culprit.

==Soundtrack==
The music by Ilaiyaraaja is melodious and ear-catching. The background music is smooth and supplements to the scenes. The lyrics written by Pulamaipithan and Gangai Amaran.

 Chic Chic Cha            K. S. Chithra
 Kalyaana Thaen Nilaa     K. J. Yesudas K. S. Chitra
 Oru Raja Vanthan         K. S. Chitra
 Thithithamsidu           K. S. Chitra

==Reception==
The screenplay and direction have helped in the persuasive portrayal of S. N. Swamys interesting story.
This movie was recommended for fans of the Indian investigative thriller genre.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 


 