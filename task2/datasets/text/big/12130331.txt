Frost/Nixon (film)
{{Infobox film
| name           = Frost/Nixon
| image          = Frost nixon.jpg
| caption        = Theatrical release poster
| director       = Ron Howard
| producer       = {{Plain list |
* Ron Howard
* Brian Grazer
* Tim Bevan
* Eric Fellner
}}
| screenplay     = Peter Morgan
| based on       =  
| starring        = {{Plain list |
* Frank Langella
* Michael Sheen
* Kevin Bacon
* Rebecca Hall
* Toby Jones
* Matthew Macfadyen
* Oliver Platt
* Sam Rockwell
}}
| music          = Hans Zimmer
| cinematography = Salvatore Totino
| editing        = {{Plain list|
* Daniel P. Hanley Mike Hill
}}
| studio         = {{Plain list |
* Imagine Entertainment
* Working Title Films
* StudioCanal
* Relativity Media
}} Universal Pictures
| released       =   
| runtime        = 122 minutes
| country        = {{Plain list|
* United States
* United Kingdom
* France
}}
| language       = English
| budget         = $25 million
| gross          = $27,426,335 
}} historical drama play of Best Picture, Best Actor Best Director.

The film reunites its original two stars from the   as British television broadcaster David Frost and Frank Langella as former United States President Richard Nixon.

==Plot== 1974 resignation speech. Meanwhile, David Frost has finished recording an episode of his talk show in Australia and watches on television as Nixon leaves the White House.

A few weeks later in the London Weekend Television (LWT) central office, Frost discusses the possibility of an interview with his producer and friend, John Birt. When Frost mentions Nixon as the subject, Birt doubts that Nixon will be willing to talk to Frost. Frost then tells Birt that 400 million people watched President Nixons resignation on live television.
 Irving "Swifty" Lazar, arrives to inform the former president of a request by Frost to interview him. Nixon rejects the proposal out-of-hand until he hears of Frosts extraordinary offer to pay Nixon $500,000. Nixon is interested and instructs Lazar to haggle; a deal is struck for $600,000. Frost and Birt fly to California to meet with Nixon. On the plane Frost meets Caroline Cushing, with whom he begins a relationship. At La Casa Pacifica, Frost makes an advance payment of $200,000 using his personal checkbook. However, Nixons post-presidential chief of staff, Jack Brennan, expresses doubts that Frost will be able to pay the entire amount.
 Washington that he thinks he can lock down with a week of work, but Frost, over-confident, decides against it.

Despite being put on notice by Nixon and being warned by his own team, Frost does not fully realize the adversarial nature of the interviews and their importance to both the participants future. Over the first three recording sessions, each two and a half hours long, Frost struggles to ask planned questions of Nixon. Nixon, well-prepared and canny, is able to take up much of the time during these sessions giving lengthy and self-serving monologues, preventing Frost from challenging him. The former president fences ably on Vietnam and is able to dominate in the area where he had substantial achievements — foreign policy related to Russia and China. Frosts editorial team appear to be breaking apart as Zelnick and Reston express anger that Nixon appears to be exonerating himself, and Reston belittles Frosts abilities as an interviewer.

Four days before the final session, which will center on Watergate, Frost is in his hotel room, waiting for Caroline to call him from Trader Vics regarding his choice for take-out food. The phone rings, and Frost, believing it to be Caroline calling, answers "Ill have a cheeseburger." He is astonished to discover that it is actually an inebriated Nixon at the other end of the line. Nixon drunkenly tells Frost that they both know the final interview will make or break their careers. If Frost fails to implicate Nixon definitively in the Watergate scandal, then Frost will have allowed Nixon to revive his political career at Frosts expense. He will thus have an unsellable series of interviews and be bankrupted. Nixon expresses his knowledge that he and Frost share a common background and psychological motivation: both had to struggle against the elite to make it to the top of their respective fields, only to be mocked and brutally knocked back down. Frost gains new insight into his subject, and perhaps also into himself. But, despite their parallel experiences, Nixon goes on to assure Frost that he will do everything in his power to emerge the victor from the final interview.

The conversation spurs Frost into action. Having spent most of his time selling the show to networks, gaining advertisers, and participating in entertainment industry parties, Frost resolves to ensure the final interview will be successful. He calls Reston and tells him to follow up on the federal courthouse hunch and works relentlessly for three days to prepare.

As the final recording begins, Frost is a much more assertive and effective adversary, ambushing Nixon with new and damning information about Charles Colson, resulting in Nixon admitting that he did unethical things. Nixon attempts to defend himself with the statement, "When the President does it, that means its not illegal." Frost, shocked by this statement, is on the verge of inducing the president to admit he took part in a cover-up, at which point Brennan bursts in and stops the recording before Nixon further incriminates himself. After Nixon and Brennan confer in a side room, Nixon returns to the interview, admits that he participated in a cover-up and that he "let the American people down".

Shortly before Frost returns to England, he and Caroline visit Nixon at his villa. Frost thanks Nixon for the interviews and presents him with a gift pair of Italian shoes that Nixon mentioned during their first meeting. Nixon is reluctant about wearing shoes without shoelaces and sees them as effeminate. Nixon, realizing he has lost, however, graciously thanks Frost and wishes him well in future endeavors. Nixon then asks to speak to Frost privately. Nixon asks if he had really called Frost before the final interview and if they had spoken about anything important. Frost replies that Nixon did indeed call and they talked about cheeseburgers. Reston says that Nixons lasting legacy was the suffix List of scandals with "-gate" suffix|"gate" being added to any political scandal. The epilogue tells that the interviews were wildly successful and that Nixon wrote a 1,000 page memoir, but never escaped controversy until his death in 1994.

Nixon watches David Frost and Caroline Cushing leave and then leans over a railing of his villa, looking out at the sunset and contemplating the future.

==Cast==
* Frank Langella as Richard Nixon
* Michael Sheen as David Frost
* Kevin Bacon as Jack Brennan
* Oliver Platt as Bob Zelnick
* Sam Rockwell as James Reston Jr.
* Matthew Macfadyen as John Birt
* Rebecca Hall as Caroline Cushing
* Patty McCormack as Pat Nixon
* Toby Jones as Swifty Lazar
* Andy Milder as Frank Gannon
* Keith MacKechnie as Marvin Minoff
* Clint Howard as Lloyd Davis
* Rance Howard as Ollie Ray Price
* Kaine Bennett Charleston as Sydney News Director
* Gabriel Jarret as Ken Khachigian
* Kate Jennings Grant as Diane Sawyer Geoffrey Blake as Interview Director
 Michael York, Raymond Price, Ken Khachigian, Sue Mengers and Neil Diamond. To prepare for his role as Richard Nixon, Frank Langella visited the Richard Nixon Presidential Library in Yorba Linda, California, and interviewed many people who had known the former president.    On the set, the cast and crew addressed Langella as "Mr. President".

==Release==
Frost/Nixon had its world premiere on October 15, 2008 as the opening film of the 52nd annual London Film Festival.    It was released in three theaters in the United States on December 5 before expanding several times over the following weeks.  It was released in the United Kingdom and expanded into wide status in the United States on January 23, 2009. 

The film was released on DVD and Blu-ray on April 21, 2009. http://www.movieweb.com/news/NE0Zk220M6di25  Special features include deleted scenes, the making of the film, the real interviews between Frost and Nixon, the Nixon Presidential Library and a feature commentary with Ron Howard. 

==Box office==
Frost/Nixon had a limited release at three theaters on December 5, 2008 and grossed $180,708 in its opening weekend, ranking number 22.    Opening wide at 1,099 theaters on January 23, 2009, the film grossed $3,022,250 at the box office in the United States and Canada, ranking number 16.  The films gross for Friday, January 30 was estimated the next day at $420,000.    Frost/Nixon grossed an estimated $18,622,031 in the United States and Canada and $8,804,304 in other territories for a total of $27,426,335 worldwide. 

 

==Critical response==
Frost/Nixon received critical acclaim, with Langellas performance receiving universal praise. Review aggregation website Rotten Tomatoes gives the film a score of 92% based on 218 reviews, with a weighted average score of 7.9 out of a possible 10.  Metacritic gives the film an average score of 80 out of 100 indicating universal acclaim. 

Critic Roger Ebert gave the film four stars, commenting that Langella and Sheen "do not attempt to mimic their characters, but to embody them".  Peter Travers of Rolling Stone gave the film 3½ stars, saying that Ron Howard "turned Peter Morgans stage success into a grabber of a movie laced with tension, stinging wit and potent human drama."  Writing for Variety (magazine)|Variety, Todd McCarthy praised Langellas performance in particular, stating, " y the final scenes, Langella has all but disappeared so as to deliver Nixon himself."  René Rodríguez of The Miami Herald gave the film two stars and commented that the picture "pales in comparison to Oliver Stones Nixon (film)|Nixon when it comes to humanizing the infamous leader" despite writing that the film "faithfully reenacts the events leading up to the historic 1977 interviews."  Manohla Dargis of The New York Times said, " tories of lost crowns lend themselves to drama, but not necessarily audience-pleasing entertainments, which may explain why Frost/Nixon registers as such a soothing, agreeably amusing experience, more palliative than purgative."   

===Dramatic license and factual inaccuracies=== the play feature commentary for the DVD release, pointing out it was a deliberate act of dramatic license, and while Frost never received such a phone call, "it was known that Richard Nixon, during ...the Watergate scandal, had occasionally made midnight phone calls that he couldnt very well recall the following day."  Aitken recalls that "Frost did not ambush Nixon during the final interview into a damaging admission of guilt. What the former president confessed about Watergate was carefully pre-planned. It was only with considerable help and advice from his adversarys team that Frost managed to get much more out of Nixon, in the closing sequences, by reining in his fierce attitude and adopting a gentler approach."   Elizabeth Drew of the Huffington Post and author of Richard M. Nixon (2007) noted some inaccuracies, including a misrepresentation of the end of the interviews, the failure to mention the fact that Nixon received 20% of the profits from the interviews, and what she says are inaccurate representations of some of the characters. Drew points out a critical line in the movie that is particularly deceptive: Nixon admitted he "...was involved in a cover-up, as you call it. The ellipsis is of course unknown to the audience, and is crucial: What Nixon actually said was, Youre wanting to me to say that I participated in an illegal cover-up. No!"   
 New York wrote that the film overstates the importance of its basis, the Frost interviews, stating it "elevates the 1977 interviews Nixon gave (or, rather, sold, for an unheard-of $600,000) to British TV personality David Frost into a momentous event in the history of politics and media." Edelstein, David,  , New York Magazine, November 30, 2008   Edelstein also noted that "with selective editing, Morgan makes it seem as if Frost got Nixon to admit more than he actually did."   Edelstein wrote that the film "is brisk, well crafted, and enjoyable enough, but the characters seem thinner (Sheen is all frozen smiles and squirms) and the outcome less consequential." 

Fred Schwarz, writing for the conservative National Review online, said that, "Frost/Nixon is an attempt to use history, assisted by plenty of dramatic license, to retrospectively turn a loss into a win.  By all accounts, Frost/Nixon does a fine job of dramatizing the negotiations and preparation that led up to the interviews. And it’s hard to imagine Frank Langella, who plays a Brezhnev-looking Nixon, giving a bad performance. Still, the movie’s fundamental premise is just plain wrong."     Though generally approving, critic Daniel Eagan notes that partisans on both sides have questioned the accuracy of the films script. 
 Muhammad Ali fight in Zaire, and that the two had been together for more than five years prior to when the film shows the two meeting. She remembered Frost as feeling that he did a pretty good job on every interview, whereas the film depicts him feeling he did a poor job with the first two interviews. She added that while the movie shows Frost driving, in fact they were always chauffeured because he was always making notes for the work he was doing.   

Diane Sawyer, portrayed in the film in her role as one of Nixons researchers, said in December 2008 that, "Jack Brennan is portrayed as a stern military guy," citing both the play and what she’d heard about the film version. "And he’s the funniest guy you ever met in your life, an irreverent, wonderful guy. So there you go. Its the movies."   

An early scene in the film set on the southern shore of Sydney Harbour in 1974, with the Sydney Opera House as a backdrop, shows buildings adjacent to the iconic structure which did not exist until 1998.

==Awards and nominations==
 
{|class="wikitable"
|-
! Award Show
! Nominations
! Result
|- Golden Globes
| Best Motion Picture
|  
|-
| Best Actor (Frank Langella|Langella)
|  
|-
| Best Director (Ron Howard|Howard)
|  
|-
| Best Original Score (Zimmer)
|  
|-
| Best Screenplay (Peter Morgan|Morgan)
|  
|-
| rowspan="5" | Vegas Film Society
| Best Actor (Langella)
|  
|-
| Best Director
|  
|-
| Best Editing
|  
|-
| Best Film
|  
|-
| Best Screenplay
|  
|- Screen Actors Guild
| Best Actor (Langella)
|  
|-
| Best Cast
|  
|- Academy Awards
| Best Picture
|  
|-
| Best Actor (Langella)
|  
|-
| Best Adapted Screenplay
|  
|-
| Best Director (Ron Howard|Howard)
|  
|-
| Best Editing
|  
|- BAFTA Awards
| Best Film
|  
|-
| Best Director
|  
|-
| Best Actor
|  
|-
| Best Screenplay-Adapted
|  
|-
| Best Editing
|  
|-
| Best Make up and Hair
|  
|}

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 