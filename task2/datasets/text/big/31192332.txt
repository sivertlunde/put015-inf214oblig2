TT3D: Closer to the Edge
{{Infobox film
| name           = TT3D: Closer to the Edge
| image          = TT3D Closer to the Edge.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Richard De Aragues
| producer       = Steve Christian Marc Samuelson
| narrator       = Jared Leto Ian Hutchinson Andy Gray
| cinematography = Thomas Kürzl
| editing        = Beverley Mills
| studio         = Isle of Man Film CinemaNX
| released       =  
| runtime        = 104 minutes
| country        = United Kingdom
| language       = English
| gross          = $2,036,488  
}}
 British documentary motorcycle race 2010 race, Ian Hutchinson.
 shot in 3D, and charts the racers dedication and the risks involved in their bid to become King of the Mountain. The film was released to the public in 2011 to critical acclaim and was a financial success. Grossing $2 million, it is the seventh highest-grossing documentary in the United Kingdom. 

==Plot==
The film charts the build up to the 2010 races, and then documents each of the races and their results. These take place along public roads through the Isle of Man, covering 37.73 miles of terrain, and packed with thousands of spectators who have come from all over the world. Using 3D technology, Closer to the Edge captures the 2010 races and attempts to show what motivates the racers.
 John McGuinness, Ian Hutchinson. 2009 Hutchinson British Superstock Championship.

The film reveals how, for more than 100 years, riders have come to the Isle of Man to compete. The narration suggests that the TT has always called for a commitment far beyond any other racing event, and since the race began over 200 people have died in the races.
 like many others, died doing something he loved. Despite the near fatal injuries, the film shows how those that survive all want to go back and do it again.

==Production==
Directed by motorsports and commercial director  . It was filmed in the Isle of Man, Northern Ireland, England, New Zealand and Los Angeles, California. In March 2011, it was announced that Jared Leto would narrate the film. 

==Critical reception== pure cinema.  Philip French of The Observer gave the film a positive review and wrote, "the speeds on such narrow, winding public roads are hair-raising and superbly photographed, the crashes spectacular and the riders far more likable than anyone involved in Formula One."  Anthony Quinn of The Independent gave the film 5 stars out of 5 and wrote that "De Aragues never loses sight of the sports high-risk stakes, where a mechanical glitch or tiny error of judgment might be the difference between life and death." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 