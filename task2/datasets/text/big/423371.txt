Macross: Do You Remember Love?
{{Infobox film
| name           = The Super Dimension Fortress Macross: Do You Remember Love?
| image          = Macross do you remember love dvd.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Shōji Kawamori Noboru Ishiguro
| producer       = Akira Inoue Hiroshi Iwata Tsuneyuki Enomoto
| writer         =
| screenplay     = Sukehiro Tomita
| story          = Shōji Kawamori
| based on       = The Super Dimension Fortress Macross by Shōji Kawamori
| narrator       = 
| starring       = Arihiro Hase Mari Iijima Mika Doi
| music          = Kentarō Haneda
| cinematography = 
| editing        =  Artland Tatsunoko Tatsunoko Topcraft
| distributor    = Big West Advertising & Toho
| released       =  
| runtime        = 115 minutes 145 minutes  (Perfect Edition)  Japan
| language       = Japanese
| budget         =
| gross          = 
| preceded by    = 
| followed by    = 
}}
 Japanese animated movie based around the The Super Dimension Fortress Macross|Macross television series.

The movie is a   to give viewers glimpses of past events.

== Plot == Lynn Minmay, but are both trapped in a section of the fortress for days. Even after their eventual rescue, this fateful meeting leads to a relationship between the singer and her number one fan.

The Zentradi, meanwhile, discover the debilitating and disruptive effect that human music has on the rank and file troops. Their supreme leader, Gorg Boddole Zer, suspects that the human culture is deeply related to an ancient music box he has kept with him for eons. Then, the Zentradi discover an opportunity to examine the humans further when Hikaru borrows a Valkyrie trainer unit without permission and flies Minmay across Saturns rings. The Zentradi capture Hikaru and Minmay, along with Lieutenant Misa Hayase, Minmays cousin/manager Lynn Kaifun, and Hikarus superior Roy Focker in the ensuing chaos.
 Milia 639, invades the ship, giving the humans a chance to escape. Hikaru and Misa escape from the ship, but Focker is killed and Minmay and Kaifun remain aboard while the two officers get caught in a space fold.

Exiting from the fold, Hikaru and Misa arrive on a desolate world that turns out to be Earth, as the entire population was wiped out by a prior Zentradi attack. As the two officers roam the remains of the planet, they become closer. They also discover an ancient city of the Protoculture (Macross)#Macross definition|Protoculture, where the mysterious origins of the alien giants is revealed. In the city, Misa discovers an artifact that contains lyrics to an ancient song.

Many days later, the Macross arrives on Earth. Just as Hikaru and Misa are debriefing their story to Captain Bruno J. Global, the fortress is attacked by a Meltrandi fleet. During the battle, ace pilot Maximilian Jenius defeats Millia aboard the Meltrandis main ship, which destroys the Macross main cannons with one shot. The Meltrandi are forced to retreat when the Zentradi arrive - with Minmays singing voice as their weapon.

Captain Global announces a truce and a military pact between the Macross and the Zentradi. Hikaru and Minmay reunite, but Minmay realizes he is now with Misa. Meanwhile, Misa works on translating the ancient song for use as a cultural weapon, as requested by Boddole Zer. However, when the Meltrandi return to attack, Boddole Zer loses patience and recklessly has his capital ship wipe out half the fleets of both factions.

Once again, the Macross finds itself in the middle of a brutal war. Hikaru persuades Minmay to perform the translated song. As the Macross flies across the battlefield, Minmays song causes a union with Britais fleet and the Meltrandi against Boddole Zer. After the Macross breaks into Boddole Zers ship, Hikaru flies his Valkyrie into the supreme commanders chamber and destroys him with his entire arsenal.

After Boddole Zers ship is destroyed, Macross bridge officer Claudia LaSalle asks why the song caused such a turnaround to the war. Misa explains that it is a simple love song.

The film ends with a concert by Minmay in front of the rebuilt Macross.

== Production Notes ==
Shoji Kawamori, Kazutaka Miyatake and Haruhiko Mikimoto worked on the mecha and character designs for the film.    Narumi Kakinouchi, one of the creators of Vampire Princess Miyu, was the assistant animation director for this movie.
 Budweiser and Tako Hai (a drink which literally translates as "Octopus Highball"). 
 Kazuhiko Kato and performed by Mari Iijima. The ending theme "Tenshi no Enogu" ("An Angels Paints") was composed and performed by Iijima.

== Release ==
The film premiered in Japanese theaters in July 7, 1984. It received a huge marketing campaign that generated very long lines of fans; many of them camped outside cinemas the night prior to the film. These events were dramatized in the anime comedy Otaku no Video from 1991.

== Relation to the TV series ==
Do You Remember Love? is a reinterpretation of The Super Dimension Fortress Macross in a feature film format. Almost all of the characters featured in the TV series appear in the film. Most of the voice actors from the TV series reprised their roles for the film. The love triangle and the various relationships are intact.

Macross 7 describes a film called Do You Remember Love? within the fictional world of Macross. Series creator Shoji Kawamori also gave an explanation about the differences in the television and film depictions of Space War I: "The real Macross is out there, somewhere. If I tell the story in the length of a TV series, it looks one way, and if I tell it as a movie-length story, its organized another way...".   

Many ships, mecha, and characters were redesigned for the film.  These designs have been featured in later entries of the Macross franchise. The Zentradi were given a language of their own and most of the dialogue of Zentradi characters is in that language.

The film diverges from the TV series in several ways:
*In the series beginning, Hikaru and Minmays relationship starts before his joining U.N. Spacy and her meteoric rise to stardom. In the film, Hikaru is already a pilot in Skull Team, while Minmay is already an established star.
*The Earth is bombarded by the Zentradi before the Macross returns while in the series, the bombardment happened afterwards.
*Three pilots/planes are featured as Skull Leader / Skull 1: Roys (VF-1S Strike, Roy Colors - Grey + Yellow), Maxs (VF-1S, Max Colors - White + Blue) and Hikarus (VF-1S Strike, Hikaru Colors - White + Red), as opposed to the TV series where there is only one Skull 1, which was flown only by Roy and Hikaru.
*Hikaru and Misa discover a Protoculture city-ship on the ruins of Earth that rose out of the ocean after the Zentradi attack. The Protoculture have a generally higher profile in the movie than on the show and more is revealed about them. The films title song is itself a Protoculture relic. Supervision Army. The Meltlandi, in addition to being in a separate fleet of their own have distinctive ship and mecha designs. 
*In the original Macross TV series, the Zentradi#In Macross|Zentradis dialogue was automatically translated into Japanese.    In this animated film, they are actually heard speaking a fictional extraterrestrial language specifically developed for the movie as subtitles are provided for the audience, much like the Klingon language|Klingon language in Star Trek (of which a word wasnt spoken until they both appeared in their first theatrical version).  This language was subsequently used in further installations of the Macross universe.
*Max and Milia meet face to face during their battle in their original sizes, in which Max immediately falls in love with the giantess. He later undergoes enlargement in the sizing chamber so he can fight side-by-side with his lover, a reversal of the events in the TV series, where Milia shrunk to Maxs size instead.
*The Macross is designed slightly differently and instead of having the Daedalus and Prometheus docked as its arms it has two ARMD carriers.  This became the design of the Macross on further series installments such as Macross II, Macross Plus and Macross Frontier.
*The origin of the SDF-1 Macross is also different. Instead of being a Supervision Army Gun Destroyer like in the TV series, in the Do You Remember Love? film the SDF-1 was originally a Meltlandi Gun Destroyer that crashed on Earth and was reconstructed by humans.  The Zentradi attack Earth as soon as they discover the ship which apparently belongs to their Meltlandi enemies.
*The Daedalus is not referenced in the film while the beached wreckage of the Prometheus is discovered by Hikaru and Misa shortly after their space fold back into Earth. In the 1997 home video game adaptation of the film (released on the Sega Saturn and PlayStation), an all-new introduction scene depicts Skull Squadron taking off from the Prometheus before the carrier is sliced into two by a Zentraedi cannon blast from above orbit.
*During the press conference scene, three micronized (and bald-headed) Zentrans in suits and ties are sitting on a side table next to where Captain Global makes the announcement of a ceasefire. They are credited as Rori, Konda, and Warera, the three Zentradi spies from the TV series.
*Zentradi Supreme Leader Gorg Boddole Zers physical appearance in the film completely differs from that in the TV series. Instead of being merely a bald Zentran, his head is cybernetically fused with his mobile space fortress.  Also, Boddole Zer towers incredibly high above the Zentradi in comparison to the TV series where he was slightly taller than Britai Kridanik.
*Exsedol Folmos appearance here (as having a larger cerebrum, tendrils, and greenish appearance) would actually be worked into the Macross continuity. Exsedol feared losing his cerebral capacity due to being micronized for an extended period of time. He voluntarily underwent this change so as to preserve his intellect. In the Macross universe, it is also stated that Exedore played himself in the film.
*Quamzin Kravshera, a major recurring enemy in the series, only appears briefly in this movie, having only two lines of dialogue. He is unnamed in the dialogue but named in the credits. He was the Zentran who fought Roy Focker to the death. In Macross 7, the Firebomber band actually meets the Zentradi actor who playded Quamzin. He wore a replica of Quamzins uniform from the TV series. However, in the movie, he only appeared in power armor.

==International versions==
  legal battles dubbed and subtitled format by Kiseki Films in the UK on video in the 1990s, but was notably one of their few catalogue titles not being released on DVD.

==Video games==
*A   by the same name was made based on this feature anime. A loose game sequel called   was also made.
*A CD-based   was released for the Sega Saturn in 1997 and the Sony PlayStation in 1999. It was a 2D shooter that followed the movies storyline using cut scenes from the film and additional footage. The Super Dimension Fortress Macross PlayStation 2 video game players are able to choose either a long and easier "TV path" or the more difficult and shorter "Movie path" of the game, which is based on the events of Do You Remember Love? and also has several missions that feature situations not shown on film.
*Characters of the film appear in the Super Robot Wars Alpha videogame, as well as two different paths to choose during gameplay (one which follows some events of the TV series, and the other which follows the events from movie). The player can use Max Jenius to try to recruit Milia Fallyna to your side in one stage, but the way their final confrontation plays out in a later stage determines whether Milia gets micronized (as in the series), or Max gets macronized (as in the movie) when she finally joins you.

==References==
 

==External links==
*   
* 
* 
*  at Macross Compendium
*  at Macross Mecha Manual

 

 
 
 
 
 
 
 
 
 