Mr. Perrin and Mr. Traill
{{Infobox film
| name = Mr. Perrin and Mr. Traill
| image =Mr. Perrin and Mr. Traill.jpg
| image_size =
| caption =
| director = Lawrence Huntington 
| producer = Alexander Galperson
| writer = Hugh Walpole  (novel)   L.A.G. Strong    T.J. Morrison
| narrator = David Farrar Edward Chapman
| music = Allan Gray 
| cinematography = Erwin Hillier 
| editing = Ralph Kemplen
| studio = Two Cities Films
| distributor = General Film Distributors   Eagle-Lion Films (US)
| released = 25 August 1948
| runtime = 92 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} David Farrar, Edward Chapman Cornish coast.

==Synopsis==
An elderly schoolmaster is upset when a new, younger teacher arrives at the school proving extremely popular with the students. Their rivalry is eventually driven to a tragic conclusion. 

==Partial cast==
  David Farrar as David Traill  
* Marius Goring as Vincent Perrin  
* Greta Gynt as Isobel Lester  
* Raymond Huntley as Moy-Thompson   Edward Chapman as Birkland  
* Mary Jerrold as Mrs. Perrin  
* Ralph Truman as Comber  
* Finlay Currie as Sir Joshua Varley  
* Maurice Jones as Clinton  
* Lloyd Pearson as Dormer  
* May MacDonald as Mrs. Dormer  
* Viola Lyel as Mrs. Comber
* Pat Nye as Matron
 

==References==
 

==External Links==
 

 

 
 
 
 
 
 
 
 
 
 