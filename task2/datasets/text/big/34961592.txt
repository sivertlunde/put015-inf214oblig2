Hamilton (2006 film)
 
{{Infobox film
| name           = Hamilton
| image          =
| caption        =
| director       = Matthew Porterfield
| producer       = Jordan Mintzer
| screenplay     = Matthew Porterfield
| starring       = Christopher H. Myers Stephanie Vizzi
| music          =
| cinematography = Jeremy Saulnier
| editing        = Matthew Porterfield
| studio         = The Hamilton Film Group
| distributor    = The Hamilton Film Group
| released       = April 2, 2006
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} 
Hamilton is a 2006 independent drama film directed by Matthew Porterfield, set and shot in Baltimore, Maryland, USA. The film was screened at several international film festivals, including the Maryland Film Festival. It was released on DVD by The Cinema Guild as part of a two-disc set with Porterfields second feature, Putty Hill, on November 8, 2011.

==Plot==
The films plot deals with two accidental parents and how they manage to work their lives around being premature parents.

==Cast==
* Christopher H. Myers as Joe
* Stephanie Vizzi as Lena
* Sarah Siepp-Williams as Candace
* Gina Christine Mooers as Linda
* Jasmine Bazinet-Phillips as Courtney
* Megan Clark as April
* Madeleine Saar Reeser as Adeline
* Tiffany Boone as Briana
* Marie Collins as Marie
* Sarah Jane Gerrish as Vicky

==Production==
Principal photography mostly took place in Baltimore, Maryland.

==Release==
The film was released at the Wisconsin Film Festival on April 2, 2006.

===Home Media===
The film was released on DVD on November 8, 2011

==Reception==
The film has an 83% on Rotten Tomatoes based on 6 reviews.

==External links==
*  

 
 
 
 
 