Mickey's Mellerdrammer
 
{{Infobox film
| name           = Mickeys Mellerdrammer
| image_size     =
| image	=	Mickeys Mellerdrammer FilmPoster.jpeg
| caption        =
| director       = Wilfred Jackson
| producer       = Walt Disney
| writer         =
| narrator       =
| starring       = Pinto Colvig Walt Disney Marcellite Garner Billy Bletcher
| music          = 
| cinematography =
| editing        = Walt Disney Productions
| distributor    = United Artists
| released       = 18 March 1933 USA
| runtime        = 8 Minutes
| country        = USA English
| budget         =
}} Walt Disney Productions and released by United Artists. The title is a corruption of "melodrama", thought to harken back to the earliest minstrel shows, as a film short based on Harriet Beecher Stowes abolitionism in the United States|anti-slavery novel Uncle Toms Cabin and stars Mickey Mouse and his friends who stage their own production of the novel.

The cartoon shows Mickey Mouse and some of the other characters dressed in blackface with exaggerated, orange lips; bushy, white sidewhiskers made out of cotton; and his now trademark white gloves.

==Plot==

In Mickeys Mellerdrammer, Mickey Mouse, Minnie Mouse, Goofy (known then as Dippy Dawg) and others present their own low budget light-hearted rendition of the 19th century Tom Shows for a crowd in a barn converted into a theater for the occasion.

Horace Horsecollar plays the white slave owner Simon Legree. Minnie plays the young white girl Eva. Mickey plays old Uncle Tom with cotton around his ears and chin, and the young slave girl Topsy. Clarabelle Cow plays the slave woman Eliza. Goofy plays the production stage hand.

The cartoon opens with Mickey and Clarabelle Cow in their dressing rooms applying blackface makeup for their roles. The cartoon is much more focused on the Disney characters efforts to put on the play, than an animated version of Uncle Toms Cabin. The cartoon contains many images of Mickey and the other characters using makeshift props as sight gags.

The cartoon closes with the characters coming out for a bow, and Horace Horsecollars character is pelted with rotten tomatoes. When Goofy shows his face from behind the stage, he is hit with a chocolate pie, leaving him in what appears to be blackface.

==Ethnic stereotyping==

Stereotyped characterizations of black people were then common, and revealed a hostility to emancipation.  Mickeys Mellerdrammer was one of many films and cartoons of its era that referenced Uncle Toms Cabin, and has been cited as particularly notable.  Henry Louis Gates Jr., wondered how the cartoon evaded censorship, given that Mickey and Minnie portray Tom and Eva, and are "as they say, an item, and unmistakably so." Additionally, Mickey is seen cross-dressing in the role of Topsy. 
In the beginning of this short you first see Clarabelle Cow in her dressing room applying her black makeup right out of a jar and leaving an exaggerated area around her lips white. You then see Mickey Mouse who takes a more “comical” approach to applying the makeup. He lights a match then proceeds to blow the match out and allow the ashes to paint his face, also leaving a large area around his lips white.  Mickey’s bushy side whiskers and white gloves are also a representation of the use of blackface.  If we look back, this is exactly how the actors in the earliest minstrel shows were dressed. Along with Mickeys Mellerdrammer, these ethnic caricatures are evident in many of Disney’s short film productions. The characters were drawn with great white eyes, dark face-makeup and bright, exaggerated orange or white lips. They even embodied the personalities of what African Americans were perceived as. The blackface characters were jazzy and soulful, just like the Mickey Mouse we know today. However, in other cases the characters would take on the negative traits that were assumed of the African Americans, such as laziness, danger, and stupidity  

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 