The Silver Trail
 
{{Infobox film
| name           = The Silver Trail
| image          =
| image_size     =
| caption        =
| director       = Bernard B. Ray
| producer       = Bernard B. Ray (producer) Harry S. Webb (associate producer)
| writer         = James Oliver Curwood (story "The Mystery of the Seven Chests") James Oliver Curwood (original story) Bennett Cohen (continuity) Forrest Sheldon (dialogue)
| narrator       =
| starring       = See below
| music          =
| cinematography = Pliny Goodfriend
| editing        = Frederick Bain
| distributor    =
| released       = 27 February 1937
| runtime        = 58 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

The Silver Trail is a 1937 American film directed by Bernard B. Ray.

== Plot summary ==
 

== Cast ==
*Rex Lease as Bob Crandall Mary Russel as Molly Welburn, aka Mary Allen Ed Cassidy as Frank Sheridan Roger Williams as Sam Dunn Steve Clark as Tom (scenes deleted)
*Slim Whitaker as Henchman Slug
*Oscar Gahan as Henchman Curt James Sheridan as Henchman Tex
*Tom London as Looney Rin Tin Tin Jr. as Rinty

== Soundtrack ==
*Goebel Leon Reeves - "Ive Got a Good Job (Drifting Around)"
*Goebel Leon Reeves - "Lonesome Cowboy"

== External links ==
* 
* 

 

 
 
 
 
 
 
 


 