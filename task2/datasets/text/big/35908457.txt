Johnny Dark (film)
{{Infobox film
| name           = Johnny Dark
| image          = Johnny Dark.jpg
| image size     
| caption        =
| director       = George Sherman
| producer       = William Alland
| executive producer =
| writer         = starring        = Tony Curtis Piper Laurie
| music          =
| cinematography = Carl E. Guthrie
| editing        = Edward Curtiss
| studio         = Universal Studios
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Johnny Dark is a 1954 film starring Tony Curtis as a racing car driver. 

==Plot==
Johnny Dark and his pal Duke Benson work for Fielding Motors, where owner James Fielding manufactures family-friendly automobiles. Chief engineer Scotty overhears the guys complaining about the company and spots a sports-car design Johnny and Duke have done.

New employee Liz catches the eye of the guys. Her secret is that she is Fieldings granddaughter. When a major stockholder, Winston, protests the companys unwillingness to create new products for more profits, Scotty blurts out that Fielding Motors is developing a new sports car.

Liz is chosen as the cars designer while Johnny and Duke go to work building it. Duke invites her to go dancing, but is jealous when he spots her kissing Johnny, who has discovered Lizs true identity.

Duke is fired after flipping the car during a practice run. He blames it on brake failure, but Johnny feels its just an excuse. Liz is disappointed in Johnny for not defending his friend.

The car is entered in a Canada-to-Mexico race. Johnny must drive it himself, Duke having been hired to drive another vehicle. Fielding dislikes making a sports car, but accepts Scottys wager on the race. The car has a breakdown and Johnny must push it into Las Vegas, but when a radio broadcast implies that Fielding doesnt care, Scotty and a team of mechanics rush to Vegas to help Johnny get back into the race and win, which Liz and Duke help him celebrate.


==Cast==
*Tony Curtis as Johnny Dark
*Piper Laurie as Liz Fielding Don Taylor as Duke Benson Paul Kelly 	as William H. Scotty Scott
*Ilka Chase 	as Abbie Binns
*Sidney Blackmer as James Fielding
*Ruth Hampton  as Miss Border to Border
*Russell Johnson as Emory
*Joe Sawyer as Carl Svenson Robert Nichols as Smitty
*Pierre Watkin as Ed J. Winston
*Scatman Crothers as Himself
*Ralph Montgomery as Morgan

==References==
 

==External links==
*  at IMDB

 

 
 
 
 
 
 
 
 