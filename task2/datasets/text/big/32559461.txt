Family Nest
{{Infobox film
| name           = Family Nest
| image          = Family Nest dvd cover.jpg
| border         = 
| alt            = 
| caption        = DVD cover
| director       = Béla Tarr
| producer       = 
| writer         = Béla Tarr
| starring       = Laszlone Horvath Gaborne Kún Gábor Kun
| music          = János Bródy Mihály Móricz
| cinematography = Ferenc Pap
| editing        = Anna Kornis
| studio         = Balázs Béla Stúdió
| distributor    = 
| released       =  
| runtime        = 108 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| gross          =
}}

Family Nest ( ) is a 1979 Hungarian black-and-white drama film, directed by Béla Tarr. The film won Grand Prize at the 1979 Mannheim-Heidelberg International Film Festival, which tied with Spanish film El Super. 

==Plot==
 

The film follows a young couple as they live in apartment of the husbands parents, as well as the wifes efforts to find new housing.

==Cast==
*Laszlone Horvath as Irén
*László Horváth as Laci
*Gábor Kun as Lacis Father
*Gaborne Kún as Laci Mother

==Reception==
Family Nest received moderately positive reviews from film critics. Rotten Tomatoes reports an 80% approval rating, with an average rating of 6.9/10.  Jonathan Rosenbaum of Chicago Reader wrote, "This is strong stuff, but the highly formal director of Almanac of Fall, Damnation, and Satantango is still far from apparent."  Keith Uhlich of Slant Magazine rated the film 2/4 stars and called the characters "unconvincing mouthpieces for a highly unsubtle political critique." 

==References==
 

==External links==
 

 
 
 
 
 
 
 

 
 