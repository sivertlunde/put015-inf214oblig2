Hosilu Mettida Hennu


{{Infobox film
| name           = Hosilu Mettida Hennu
| image          =
| caption        =
| director       = V T Thyagarajan
| producer       = T H Rama Murthy
| writer         =
| screenplay     = Vishnuvardhan Aarathi Leelavathi Ambareesh
| music          = T. G. Lingappa
| cinematography = R N Krishna Prasad
| editing        =
| studio         =
| distrubutor    =
| released       =  
| country        = India Kannada
}}
 1976 Cinema Indian Kannada Kannada film, Leelavathi and Ambareesh in lead roles. The film had musical score by T. G. Lingappa.   

==Cast== Vishnuvardhan
*Aarathi Leelavathi
*Ambareesh
*T. N. Balakrishna

==Soundtrack==
The music was composed by TG. Lingappa. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Susheela || R. N. Jayagopal || 03.20
|- Susheela || R. N. Jayagopal || 03.24
|}

==References==
 

==External links==
*  

 
 
 
 


 