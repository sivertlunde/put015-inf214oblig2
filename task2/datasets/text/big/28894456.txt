Sus (film)
{{Infobox film
| name           = Sus
| image          = Sus film.jpg
| caption        =  Robert Heath
| producer       = Clint Dyer   Robert Heath   Oliver James Ledwith   Robin Mahoney   Jono Smith
| screenplay     = Barrie Keeffe
| narrator       = 
| starring       = Ralph Brown Clint Dyer
| music          = Sally Herbert
| cinematography = Jono Smith
| editing        = Robin Mahoney
| distributor    = Independent
| released       =  
| runtime        = 91 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} Robert Heath 1979 General Election, where a black suspect is brought in and interrogated on suspicion of murder. It was written by Barrie Keeffe. It takes its name from the Sus law in operation at the time.

==Cast==
* Ralph Brown - Karn
* Clint Dyer - Delroy
* Anjela Lauren Smith - Georgina
* Rafe Spall - Wilby

==References==
 

==External links==
*  
* http://www.thisislondon.co.uk/film/review-23831614-behind-bars-in-sus.do

 
 
 
 
 
 
 
 


 
 