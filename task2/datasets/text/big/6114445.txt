Cash and Carry (film)
{{Infobox Film |
  | name           = Cash and Carry
  | image          = Cashandcarrythreestooges37lobby.jpg
  | caption        = 
  | director       = Del Lord 
  | writer         = Clyde Bruckman Elwood Ullman John Ince Al Richardson Cy Schindell Harlene Wood 
  | producer       = Jules White 
  | distributor    = Columbia Pictures 
  | cinematography = Lucien Ballard  Charles Nelson
  | released       = September 3, 1937
  | runtime        = 18 21" 
  | language       = English
  | country        = United States
}}

Cash and Carry is the 25th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges return home to their shack in the city dump after six months of unsuccessful prospecting. Just as they get to the dump, their last car tire blows. They arrive home to find it inhabited by a young orphaned woman and her crippled younger orphaned brother, Jimmy. At fist, they want Jimmy and his sister to leave until they see Jimmy is crippled. Curly even tries helping Jimmy with his homework before Moe takes Larry and Curly outside to search for tires for their car. 

They are looking around the piles of cans when Curly finds a can full of coins. "Canned coin," as Curly calls it.  They think the can was left by accident and start sifting through the pile of cans for more treasures. Jimmy and his sister come out of the shack talking about a few coins the girl has just earned. After asking why the brother and sister were looking, the Stooges realize it was their hidden can of money they had accidentally found and return it to them. The sister explains that they are trying to raise $500 for an operation to fix Jimmys legs. They already had $62 saved for the operation.

Taking pity on the pair, the trio decide to help raise the rest of the money needed for the operation. They first try the bank to see if by just depositing the money into an account, the interest would raise the necessary funds.  Unfortunately the banker explains that it would take years of waiting before it would grow to $500.  Its then that two confidence men (Nick Copeland, Lew Davis) cheat the Stooges out of the $62 and their car for a map they claim will lead to a treasure. 

The Stooges take the map and tools and go to the house on the map.  Inside, they look for the spot marked on the map called Walla Walla.  After trying a couple walls, Curly finds one in the basement.  Mistaking a coin he dropped for the hidden treasure, he thinks he found Walla Walla.  Following the map, they dig down several feet and find another wall.  This is the wall they think will lead to the treasure, but they accidentally drill into the United States Treasury.  

At first, they think theyve hit the jackpot.  Larry remarks that theres enough money for Jimmys operation, to which Curly adds there was enough for all of them to have an operation. They are removing stacks of money when they are arrested. The Stooges end up meeting President Franklin D. Roosevelt, who has learned of Jimmys plight. The President then pardons the Stooges and pays for Jimmys operation.

==Production notes== tabloid newspapers referred to Grant and Hutton as "Cash and Cary". 

Writer Clyde Bruckmans story was later adapted for comedian Andy Clyde in his short films A Miner Affair  and Two April Fools (1954). 

Nick Copeland and Lew Davis would reprise their roles as con men who swindle the Stooges in the next entry, Playing the Ponies.
 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 