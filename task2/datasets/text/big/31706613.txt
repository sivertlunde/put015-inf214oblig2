A Ticklish Affair
{{Infobox film
| name           = A Ticklish Affair
| image          = A Ticklish Affair.jpg
| caption        =
| director       = George Sidney
| producer     = Joe Pasternak
| screenplay    = Ruth Brooks Flippen
| based on     =  
| narrator       =
| starring       = Shirley Jones Gig Young Red Buttons
| music          = George Stoll
| cinematography = Milton R. Krasner
| editing        = John McSweeney, Jr.
| studio         = Euterpe, Inc.
| distributor    = Metro-Goldwyn-Mayer (MGM)
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
A Ticklish Affair (aka Moon Walk) is a 1963 film directed by George Sidney. It stars Shirley Jones, Gig Young and Red Buttons, with a screenplay by Ruth Brooks Flippen,  based on a short story by Barbara Luther. It was nominated for a Golden Globe in 1964. 

==Plot==
Commander Key Weedon (Gig Young), a pilot with the U.S. Navy, is sent to investigate when an S.O.S. emergency signal is spotted in the San Diego region. He discovers it is the doing of a six-year-old boy, Grover Martin, whose uncle Simon (Red Buttons), an airline pilot, gave the boy a blinker light as a gift.

The childs mother, Amy (Shirley Jones), is an attractive widow, and Key develops an immediate interest in her. Her three sons also enjoy the attention Key gives all of them.  Amy has a blind spot when it comes to naval officers, however, not wanting a permanent relationship with one because they are constantly on the move. She had been a military child herself, and missed not having permanent "roots".  Sure enough, Key gets orders to go to Italy, so Amy refuses his marriage proposal, though they love each other.

Uncle Simon has a new treat for Grover and his brothers. He ties them to helium balloons and flies them as one would a kite. Unfortunately, Grover cuts his tether and he goes floating for miles over San Diego. Large-scale rescue operations are quickly organized by the Navy, and it turns out to be Key himself who lowers himself on a rope ladder from a blimp to rescue the boy. A grateful Amy then decides that wherever he goes, Key is the man for her.

==Cast==
As appearing in A Ticklish Affair, (main roles and screen credits identified):   Turner Classic Movies. Retrieved: August 28, 2013.   
* Shirley Jones as Amy Martin
* Gig Young as Key Weedon
* Red Buttons as Uncle Simon Shelley
* Carolyn Jones as Tandy Martin
* Edgar Buchanan as Captain Martin Peter Robbins as Grover Martin
 
* Bill Mumy as Alex Martin
* Bryan Russell (actor) as Luke Martin
* Eddie Applegate as Yeoman Corker Bell
* Edward Platt as Captain Haven Hitchcock
* Robert Foulk as Policema
 

==Production==
 Jones and Ingels 1990, p. 158.   ]]---may be deleted--!-->
  was seen prominently in A Ticklish Affair.]] The Courtship of Eddies Father (1963). Jones reluctantly fulfilled her contract despite her reluctance to take on "fluff" roles in a rom-com like A Ticklish Affair. Jones and Ingels 1990, p. 158.  Despite the "screen chemistry" of the leads, critics did not consider the film interesting family fare.   
 NAS San Diego, with the U.S. Navy providing access to its naval resources.   Filming at NAS San Diego included scenes aboard the USS Coral Sea (CVA-43)|USS Coral Sea aircraft carrier and the use of operational aircraft and helicopters, even a U.S. Navy blimp. LoBianco, Lorraine.   Turner Classic Movies. Retrieved: August 28, 2013. 

==Reception==
A Ticklish Affair was not well received by critics, who dismissed it as overly sentimental. Reviewer Colin Bennett of The Age  called it, "the kind of glossy, sentimental family comedy which is made with the entire cooperation of the United States Navy. Characters and situations are from a well-tried formula ... The piece-de-resistance of this shatteringly wholesome affair is the rescue by the said navy of one small boy floating out to sea dangling from a cluster of balloons."   
 Broadway productions and later as the matron of televisions The Partridge Family  (1970–1974). 

==References==
Notes
 
Citations
 

Bibliography
 
* Jones, Shirley. Shirley Jones: A Memoir. New York: Gallery Books, 2013. ISBN 978-1-47672-595-6.
* Jones, Shirley and Marty Ingels. Shirley and Marty: An Unlikely Love Story. New York: William Morrow & Co., 1990. ISBN 978-0-688-08457-8.
* Monder, Eric. George Sidney:a Bio-Bibliography. Westport, Connecticut: Greenwood Press, 1994. ISBN 978-0-31328-457-1.
* Wise, James E. and Anne Collier Rehill. Stars in Blue: Movie Actors in Americas Sea Services. Annapolis, Maryland: Naval Institute Press, 2007. ISBN 978-1-59114-944-6.
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 