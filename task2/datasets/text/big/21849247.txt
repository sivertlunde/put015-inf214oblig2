A Lonely Cow Weeps at Dawn
{{Infobox film
| name           = A Lonely Cow Weeps at Dawn
| image          = A Lonely Cow Weeps at Dawn.jpg
| image_size     = 
| caption        = A Lonely Cow Weeps at Dawn in its theatrical release as Molester Father-in-Law, the Sons Bride and...
| director       = Daisuke Gotō
| producer       = Yutaka Ikejima
| writer         = Daisuke Gotō
| narrator       = 
| starring       = Ryōko Asagi   Horyu Nakamura    Yumeka Sasaki
| cinematography = Masahide Iioka
| editing        = Shōji Sakai
| studio         = Shintōhō
| distributor    = Shintōhō (Japan)   Pink Eiga Inc. (USA)
| released       = 2003-04-08 (Japan)  2009-2-23 (on DVD USA)
| runtime        = 61 minutes
| country        = Japan Japanese
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

  aka A Cow at Daybreak (Yoake no ushi) and Cowshed of Immorality (2003) is a Japanese pink film directed by Daisuke Gotō and starring Ryōko Asagi and Horyu Nakamura. It has been shown in film festivals, and was named the fifth best pink film released in the year 2003.

==Synopsis==
Noriko, a young widow, lives with her senile father-in-law, Shukichi. In order to convince Shukichi that his favorite cow is not dead, Noriko rises before dawn, poses as the cow and allows Shukichi to milk her instead. Conflict arises when Shukichis daughter tries to put an end to this relationship. 

==Cast==
* Noriko
* Hidehisa Ebata
* Haruki Jō
* Sakura Mizuki
* Seiji Nakamitsu... Hajime
* Hōryū Nakamura... Shukichi
* Toshimasa Niiro
* Yumeka Sasaki... Mitsuko

==Production and critical reception==
Director Daisuke Gotō cites Yasujirō Ozus Late Spring (1949) and Bernardo Bertoluccis 1900 (film)|1900 (1976) as influences on A Lonely Cow Weeps at Dawn.    The film was screened at the 2004 New School of Pink-Movies, a showcase for the younger   generation of pink film makers. Though Gotōs career extends to the days of Nikkatsus Roman porno films—he has the distinction of filming the last entry in that long-running series —he was a relative unknown at the time.    The film was chosen as the fifth best pink film release for 2003 at the annual Pink Grand Prix ceremony. 

American audiences first saw A Lonely Cow Weeps at Dawn at the Fantastic Fest in Austin, Texas in September 2008.  {{cite web |url=http://www.offscreen.com/biblio/pages/essays/behind_the_pink_curtain/|title=
Behind the Pink Curtain Retrospective ~ Fantastic Fest 2008 ~|accessdate=2009-03-09|publisher= | archiveurl= http://web.archive.org/web/20090131123630/http://www.offscreen.com/biblio/pages/essays/behind_the_pink_curtain/| archivedate= 31 January 2009  | deadurl= no}}  In his Behind the Pink Curtain: The Complete History of Japanese Sex Cinema (2008), pink film authority Jasper Sharp writes A Lonely Cow Weeps at Dawn is, "pink film at its most inventive, albeit frankly bewildering." 

The British site, DVDTimes, writes that A Lonely Cow Weeps at Dawn is "a film of bittersweet sentiments, which credibly deals with loneliness, greed and the coming to terms with old age." The site judges Hajime Oba’s musical soundtrack as "beautifully poignant", and the cinematography, "keeps things visually alluring, with the Japanese countryside providing a pleasant change of pace." 

==Availability==
 
A Lonely Cow Weeps at Dawn was released theatrically on April 8, 2003.  The U.S. company Pinkeiga released the film on Region 0 DVD on February 23, 2009. 

==References==
 

==Reviews==
*  
*  
*  
*  

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 

 

 
 
 
 