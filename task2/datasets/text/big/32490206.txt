Bound in Morocco
{{infobox film
| name           = Bound in Morocco
| image          = Bound in Morocco1918newspaperad.jpg
| imagesize      =
| caption        = Newspaper ad for Bound in Morocco and Mack Sennetts The Summer Girls (1918)
| director       = Allan Dwan
| producer       = Douglas Fairbanks Adolph Zukor Jesse L. Lasky Elton Thomas
| story          = Elton Thomas
| writer         = Allan Dwan (scenario)
| starring       = Douglas Fairbanks
| music          =
| cinematography = Hugh McClung
| editing        =
| studio         = Fairbanks Pictures Corp. Gaumont (France)
| released       =  
| runtime        = 64 minutes
| country        = United States Silent English intertitles
}} silent action comedy/romance film starring Douglas Fairbanks. Fairbanks produced and wrote the films story and screenplay (under the pseudonym Elton Thomas), and Allan Dwan directed.  The film was produced by Douglas Fairbanks Pictures Corporation and distributed by Famous Players-Lasky/Artcraft Pictures. 

==Preservation status==
Bound in Morocco is now considered a lost film.  

==Cast==
*Douglas Fairbanks - George Travelwell
*Pauline Curley - Ysail
*Edythe Chapman - Ysails mother
*Tully Marshall - Ali Pah Shush
*Frank Campeau - Basha El Harib, governor of Harib
*Jay Dwiggins - Kaid Mahedi el Menebhi, Lord High Ambassador to Morocco Fred Burns - Bandit Chief
*Albert MacQuarrie

==See also==
*List of lost films

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 