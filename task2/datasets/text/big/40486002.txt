Crayon Shin-chan: Serious Battle! Robot Dad Strikes Back
{{Infobox film name = Crayon Shin-chan: Serious Battle! Robot Dad Strikes Back image = Poster for 22nd movie of Crayon Shin-chan in 2014.jpg caption = director = Wataru Takahashi producer =  writer = Kazuki Nakashima starring = Akiko Yajima as Shinnosuke Nohara  music = Toshiyuki Arakawa Basil Poledouris Goro Ohmi Randy Edelman Joe Hisaishi Shogakukan Kan Sawada cinematography =  editing =  studio = Shin-Ei Animation distributor = Toho released =   runtime = 97 minutes country = Japan language = Japanese budget = gross = US$17,090,000
         (JP¥1,820,000,000)
}}
  is an 2014 Japanese anime film produced by Shogakukan. It is the 22nd film of the popular comedy manga and anime series Crayon Shin-chan, released in Japanese theatres on 19 April 2014. It is directed by Wataru Takahashi and the script is written by Kazuki Nakashima of Kill la Kill. The story of the movie was published in comic form in the October issue of Manga Town, with script written by Nakashima and art by Aiba Kenta.    

==Overview==
This is the first time in the Crayon shin-chan movie series that Shinnosukes father is the main character. In this movie, Shinnosukes father Hiroshi gets converted into a robot. 
  When his father goes for a massage to fix his sprained back, he returns home as a robot, "with a snort more powerful than an electric fan!" 
In the year 2014, the "father revolution" breaks out throughout the land. In these troubled times, Robot dad along with Shinnosuke have to protect their family.

==Plot==
Hiroshi got a slipped disk due to a back injury. He was taken to an Este salon by a mysterious beautiful girl
who appeared suddenly, to receive a free trial of beauty treatment as well as a massage. When Hiroshi returned home after a beauty treatment, he was surprised to see himself in the form of a robot. Shinnosuke gets overjoyed, whereas Misae gets worried on seeing him as a naked robot. The robot version of Hiroshi turns out to be convenient. The robot Hiroshi could be controlled by a remote control and pretty much do anything including cooking and cleaning. Meanwhile Hiroshi realizes that his turning into a robot has to do something with that Este salon. However, this new change was a dark conspiracy hatched by “Chichi Yure Doumei (The Association of Fathers)” to create a strong father figure for all the fathers in Japan. Soon, many dads in the whole nation go out of control, and the Nohara family (Shinnosukes family) and Kasukabe start falling apart. Before the near-collapse of Kasukabe, Shinnosuke and Hiroshi i.e. Robot dad stand up to save the day. Featuring the most intense battle of middle-aged men, the movie brings a touching story that makes all fathers and families in Japan cry. 
  

==Story==
In the beginning, Shinnosuke and Hiroshi were watching the new Kantam Robot movie. It featured characters like Kantam Robot, his wife Sheila, their son Kantam Jr. and the mastermind Akogidesu. After watching the movie, they went out. They played sometime in the nearby park. There they met a middle-aged man named Susukita Osamu. Hiroshi gave Shinnosuke a ride on his shoulder. But then he gets a strained back. On their way home, they meet a mysterious lady called Omega Ranran who worked at a massage salon. She is characterised by large breasts, which Shinnosuke imitates to tease Hiroshi. She talks with them, offers Hiroshi a free massage. But only Hiroshi was allowed to go into the store. Hiroshi was forced to sleep by the lady, and was remodelled...

Later, Hiroshi woke up and his body felt better. Meanwhile, Shinnosuke was telling Misae and others about the ladys meeting with Hiroshi, and imitating the lady with large breasts. Hiroshi then arrived at home and met his family, who were surprised on seeing a robot.  Misae said ”who are you?”. They could not believe their eyes at first, but finally Misae understood that the robot is Hiroshi. But she doesn’t let him take into the house. Hiroshi had to sleep in Shiro’s house. He realised that it had got to do something with the massage salon and the lady.

Next morning they went to the massage salon. But the salon was vanished. So Misae went to the Kasukabe Police Station. The chief Kuroiwa Jintaro ordered the police officer, Dandanbara Teruyo to find the lady massagist who had vanished. But they couldn’t find.

Nohara family lived a few days with Robot Dad.

One day Himawari kindergarten had a school trip to a river side building under construction. Kasukabe Boueitai was forced to step up the elevator by a robot. But the top of elevator was nothing. So they jump up and hung on a reinforced concrete, swinging in the air. But the rope of reinforced concrete was cut off and they dropped into the air. They were in a dangerous situation. Then Robot Dad comes and saves them all.

With this, Nohara family accepted Robot Dad. While Misae was worried, Shinnosuke was overjoyed to have a Robot dad. The robot dad had a remote control by which he can be controlled. Robot dad turns out to be very handy. He did cooking for the family. He used his legs as grass cutters and hands as bush cutters, and did a wonderful job in gardening. He also repaired the TV antenna. He could do many things which human Hiroshi couldnt do. Shinnosuke used the remote to do him various actions, including the buriburi dance. When the energy of the Robot dad was over, he had to be refilled from a hole in the Robots butt. Once Shinnosuke and Robot dad had an arm wrestling match. Shinnosuke was defeated easily at first, but the second time Robot dad let him win as he didnt want to upset a kid. Misae once says emotionally that "whether you are robot or human, you are a father, one of the best fathers". Once during dinner time, Himawari accidentally presses the two switches suggestive of the nipples, and two missiles were launched (but the house was not blown off as shown in the trailer, although it did cause damage).

Few days later, Shinnosuke met the lady, the same massagist,  and she gives him a moustache. After he came back home, during dinner Shinnosuke let Robot dad put on the moustache. Suddenly Robot dad changes totally. He becomes a tyrant and rules his family.

The next day Robot dad gathers all fathers in the park and drives out the tyranny mothers from the park. Then they march along the street, with banners in their hand. Seeing that the changed Robo Hiroshi is out of control, Shinnosuke thought of putting back the former Robot dad. So Kasukabe Defense Group lead him to the back alley and attacked him. Later, he was almost broken and treaded by Tekkenji Doukatsu, the leader of the Fathers Alliance. Robot Hiroshi was brought somewhere on the track, and Shinnosuke was chasing.

Then they arrived the factory, and Shinnosuke had only the head of Robot dad. Shinnosuke lost his way and found a huge room where there were many robots which looked the same type as Robot Hiroshi. So he put the Robo Dad’s head to a cleaning robot that looked like a Roomba. Robot Hiroshi woke up and shouted ”What is this body!? Where is my body?”. They went into another room. They found the real Hiroshi, naked and sleeping in the water ball. Shinnosuke pushed a button, and human Hiroshi woke up. They both said "Who are you?!" and claimed to be the real Hiroshi. So Hiroshi and Robot dad quarrelled. Then they three were detected by Chichiyure Doumei (Fathers union). Robot dad is actually a robot in which Hiroshis memory had been copied, so that he behaved like real Hiroshi. Hiroshi had not turned into a robot in reality. Then Robot dad finds new body, and they escape. They had known that the factory was the river side building. So they went home.

The fathers had occupied the police station. So Robot dad and Nohara family went there. Tekkenji comes out and battles with Robot dad. After a serious battle, Robot dad wins and is pleased; he looks at misae. But Misae hugged Hiroshi the real human dad. Robot dad was disappointed.

Nohara family, Dandanbara and Robot dad were caught by Tekkenji. They came to know that Tekkenji’s real identity is Kuroiwa Jintarou, the chief police officer. Tekkenji was a robot controlled by Jintarou. Jintarou ordered Dr.Ganma to delete Robot dad’s the memory. He decided to give devilish treatment to Shinnosuke. Robo dad cooked large serving fried green peppers, and forced Shinnosuke to eat them. Shinnosuke looked afraid but eats it all with courage. He shouted “come back Robot dad!”. Hearing this, Robot dad come back to his original sense. Tekkenji driven by Jintaro attacked them. The battle is fought once again. But the battle affects the building terribly and was breaking. They escaped and went outside.

Kuroiwa rode a new big robot named Itsuki Hiroshi(a famous singer). Then Robot dad changed into big Robot dad, an ultimate weapon made instantly by Robot dad while sucking all cranes, metal bars, into a field generated , so to piece them together to make an ultimate weapon. Shinnosuke and Hiroshi rode on it. Itsuki then launches a rocket towards big Robot dads head, but it failed because of the fact that big Robot dads head can move sideways. Then Shinnosuke made big Robot dad launch the same rocket onto Itsuki, but no damage is made. Then, Dr. Ganma switched on the melting wave, which then made big Robot dad to melt.  

Then another fierce battle happened. Robot dad then gave Shinnosuke the remote control, telling him to do the old trick, to Hiroshis confusion. robot dad went for the large oval-shaped building so to create another form. He made use of the shape to make a butt . After it has done, Shinnosuke controlled Robot dad to do the buriburi dance with the form. Itsuki escapes, and tries to use the melting wave but this time it failed, and ended up reflecting it onto Dandanbara, Misae and Omega. Then after a while, the butt caught Itsuki, and was destroyed. They finally beat Itsuki robot.

After the victory, Robot dad said to Hiroshi ”Which one is the real Hiroshi? Let’s decide with arm wrestling.” The final battle began. The battle was a heated one with both sides refusing to give up. In the end human Hiroshi beat Robot Hiroshi. It was decided that there can be only one Hiroshi. Robot dad said his last words, and Shinnosuke said that he will grow taller, taller than Itsuki robot. Then Robot dad appears to be smiling, and was broken. Shinnosuke missed Robot Dad.
    

==Slogan==
The movies slogans are
*"Robot, but dad.."
*"Father force, full throttle!"
 

==Cast==
* Akiko Yajima as Shinnosuke Nohara
* Miki Narahashi as Misae Nohara
* Keiji Fujiwara as Hiroshi Nohara
* Satomi Korogi as Himawari Nohara

===Guest Cast===
* Korokke as Ganma Hakase
* Emi Takei as Dandanbara Teruyo
 

==Characters Specific to the Movie==
 

===Robot Hiroshi (Robot dad)===
It is the appearance of Nohara Hiroshi after getting converted into a robot by a mysterious organization with dubious beauty salon. Body is that the face is made of ivory, and his trademark beard has also been faithfully reproduced. In his chest, two switches which are suggestive of the nipples are attached. Fuel is marked with filler neck to the portion of oil in the butt, it can be injected from there. 

====Fighting Robot Dad====
He is the Robot Dad "Stubborn Father Circuit" who was operated by the Fathers alliance, after wearing parts made by them such as the beard, and there was a sudden change in his nature, a strict personality. The blue small tube had changed to red, and there were two red streaks on the face. The deformed switches suggestive of the nipples were wielded into two bamboo swords. Moreover, if beaten with the bamboo sword then from the contact portion, blue colored lightning runs.

===Chichi Yure Doumei (The Association of Fathers)===
It is the organization that envisaged the reinstatement of the fathers position in the Japanese family which had become weak. The name stands for "Father, stand up with courage (Yuuki)".

====Tekkenji Doukatsu====
He is the man who formed the "Chichi Yure Doumei" in the sad situation where the dignity of the modern father is lost.

====Omega Ranran====
She is the only girl in the Fathers Association. Characterised by large breasts, she is the mysterious lady who took Hiroshi to the este salon.
She is named after the Greek Letter Ω (Omega).

====Dr.Ganma (Ganma Hakase)====
He is a genius scientist belonging to a mysterious organization which made Hiroshi a robot. He has a world authority on Robotics. He is the parody character of Dr.Tenma of Astro Boy.
He is named after the Greek letter γ ( Gamma).

===Down Kasukabe Police Station===

====Kuroiwa Jintarou====
He is the chief of the Down Kasukabe Police Station. Twink in a little smug, he wears the shoes the shoes of false bottom. Moreover, he loves himself and always checks him in front of the mirror.

====Dandanbara Teruyo====
She is a woman police officer who was assigned under Kasukabe Police Station, but is a significant troublemaker. She has a dream of becoming a detective.

===Others===

====Susukita Osamu====
He is a middle-aged man whom Hiroshi met when he dropped by the park. He wears glasses and is a smoker.

====Yamada John Seinen====
He is the hero of Kantam Robot movie, and is the partner of Kantam Jr. He is 29 years old.

====Kantam Jr.====
He is the son of Kantam Robot and Sheila that appeared in the Kantam Robot movie. He is 17 years old.

====Akogidesu====
He is the mastermind villain of the Kantam Robot movie.

==Theme Songs==

===Opening Theme Song===
Title: Kimi ni 100 Percent (Warner Music Japan)  
Singer: Kyary Pamyu Pamyu

===Ending Theme Song=== Family Party (Warner Music Japan)  
Singer: Kyary Pamyu Pamyu

 
 

==Reception==
The film grossed an all total of US$17,090,000 (JP¥1,820,000,000) in Japan. 
 
This is the third largest grossing Crayon Shin-chan movie in Japan as of 2014, and the largest grossing since the first two films in 1992 and 1993. 

==DVD Release==
The DVD and Blu-ray of this movie was released on 7th November 2014 by Bandai Visual.  

==See also==
* Crayon Shin-chan
* Yoshito Usui

==References==
 

 
 

 
 
 
 
 