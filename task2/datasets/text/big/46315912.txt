White Slippers
{{Infobox film
| name           = White Slippers
| image          =
| caption        =
| director       = Sinclair Hill
| producer       = 
| writer         = Charles Edholm  (novel)   Sinclair Hill
| starring       = Matheson Lang   Joan Lockton   Gordon Hopkirk 
| cinematography = 
| editing        = 
| studio         = Stoll Pictures 
| distributor    = Stoll Pictures
| released       = August 1924
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent adventure film directed by Sinclair Hill and starring Matheson Lang, Joan Lockton and Gordon Hopkirk.  It was based on a novel by Charles Edholm. It is set in Mexico and is known by the alternative title The Port of Lost Souls.

==Cast==
*    Matheson Lang as Lionel Hazard 
* Joan Lockton as Alice  
* Gordon Hopkirk as Ramon Guitterez  
* Arthur McLaglen as Lorenzo  
* Nelson Ramsey as Hairy Joe 
* Irene Tripod as Dona Pilar  
* Jack McEwen as Mexican Rat  
* Adeline Hayden Coffin as Mother  

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
  
 
 

 