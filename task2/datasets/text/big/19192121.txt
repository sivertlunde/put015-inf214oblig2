Barbara Frietchie (film)
{{Infobox film name           = Barbara Frietchie image          = Barbara Frietchie - film poster.jpg caption        = The 1924 film poster. producer       = Regal Pictures ??and/or W. W. Hodkinson director       = Lambert Hillyer writer         = Lambert Hillyer Agnes Christine Johnston starring       = Florence Vidor Edmund Lowe cinematography = Henry Sharp distributor    = Producers Distributing Corporation released       =   runtime        = 85 minutes country        = United States language       = budget         =
}} 1915 version 1924 version. The 1915 version, directed by Herbert Blaché, starred Mary Miles Minter and Anna Q. Nilsson. The 1924 version, directed by Lambert Hillyer, starred Florence Vidor and Edmund Lowe.  

Lydia Knott, mother of director Hillyer and a well known character actress in her own right, appears quite prominently in this film as a member of the Frietchie family but for some reason she is uncredited.

==Cast==
*Florence Vidor - Barbara Frietchie
*Edmund Lowe - William Trumbull
*Emmett King - Colonel Frietchie
*Joseph Bennett - Jack Negly
*Charles Delaney - Arthur Frietchie
*Louis Fitzroy - Col. Negly
*Gertrude Short - Sue Rogers
*Mattie Peters - Mammy Lou
*Slim Hamilton - Fred Gelwek
*Jim Blackwell - Rufus
*George A. Billings - Abraham Lincoln
*Lydia Knott - (*uncredited)

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 


 