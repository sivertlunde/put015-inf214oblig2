An Ideal Husband (1998 film)
{{Infobox film
| name           = An Ideal Husband
| image          = 
| caption        = 
| director       = William P Cartlidge
| producer       = Daniel Figuero
| writer         = Wlliam P Cartlige
| based on       =  
| starring       = James Wilby Sadie Frost
| music          = Nigel Hess
| cinematography = Jake Polonsky
| editing        = Matthew Glen
| distributor    = 
| released       = 
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         =
}} the play of the same name by Oscar Wilde.

== Plot ==
While the film retains the premise of Wildes play and much of the original dialogue, it updates the action to the present-day. The external scenes of the film were shot at various locations in the English home counties, principally in Buckinghamshire

Sir Robert Chiltern, a rich landowner, belongs to the English county set and is a member of an (unnamed) local government authority somewhere north of London. Well-off and with a loving and trusting wife, his honour and very existence are threatened when Mrs. Laura Cheveley appears with evidence of a past misdeed of Sir Roberts. It transpires that Roberts wealth stems from insider trading concerning a proposed canal project at an unspecified location. 

She attempts to blackmail Sir Robert into supporting the project - in which she has invested heavily - and in desperation, Sir Robert turns for help to his friend Lord Goring, an apparently idle philanderer. Goring knows the lady from years before.

After several varieties of machination, the story ends happily. Lord Goring marries Roberts sister Mabel, Mrs. Chevely is outsmarted, and Lady Chiltern retains her faith in her husbands honour and idealism.

== Cast ==
* Trevyn McDowell – Lady Gertrude Chiltern
* Jonathan Firth – Lord Arthur Goring
* Sadie Frost – Mrs. Laura Cheveley
* James Wilby – Sir Robert Chiltern
* Robert Hardy – Lord Caversham
* Prunella Scales – Lady Markby

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 


 
 