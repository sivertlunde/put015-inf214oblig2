Colonel Redl
{{Infobox film
| name           = Colonel Redl
| image          = Colonel redl.jpg
| caption        = Theatrical release poster
| director       = István Szabó
| producer       = Manfred Durniok
| writer         = István Szabó Péter Dobai
| based on       =  
| starring = {{Plainlist|
* Klaus Maria Brandauer
* Gudrun Landgrebe
* Hans Christian Blech|Hans-Christian Blech
* Jan Niklas
* Armin Mueller-Stahl
}}
| music          = Zdenkó Tamássy
| cinematography = Lajos Koltai
| editing        = Zsuzsa Csákány
| studio         = Jadran Film  MAFILM Objektív Filmstúdió
| distributor    = Orion Pictures (USA)
| released       =  
| runtime        = 144 minutes
| country        = Hungary Austria West Germany German Hungarian Hungarian
| budget         =
}} Hungarian director István Szabó. The plot, set in the period before World War I, follows the rise of Alfred Redl, an officer in the Austro-Hungarian empire. Redl, who comes from a humble background, enters military school as a boy and has an illustrious military career pushed forward by his loyalty to the crown. He is appointed the head of an intelligence gathering unit, but his attraction to men eventually causes his downfall.

The screenplay, loosely inspired from British playwright John Osbornes play A Patriot for Me, charts the rise of inter-ethnic tensions in Austro-Hungary, which were to bring about the assassination in Sarajevo and the empire eventual disintegration.

The film stars Klaus Maria Brandauer, Jan Niklas and Gudrun Landgrebe. It was nominated for an Academy Award for Best Foreign Film and won the Jury Prize at Cannes Film Festival in 1985.
==Plot== Ruthenian boy Galicia in Emperor Franz Josef. Redl is never to forget that he owes to the emperor his promising career.

At the military academy, the young  Redl soon  stands out for his talent, drive and loyalty to the Crown. One of his teachers forces him to inform on Kristof Kubinyi, a student whos the source of a practical joke; though he beats himself up for incriminating his comrade, Alfred soon realizes that to rise in the ranks he must overcome his peasant background by ingratiating himself with his superiors. Alfred and Kristof become fast friends. Kubinyi  invites Redl home for the holidays to the elegant residence of his parents, who lead a life of privilege and nobility in Hungary. There, Alfred meets Kristofs pretty sister, Katalin, who welcomes him warmly. To Kubinyis aristocratic parents, Redl hides his true humble background pretending to be of Hungarian ancestry and a member from an old family who lost all its fortune.

Redl and Kubinyi slowly climb the ladder of career-officers. Once they become adults, the two friends have different political ideals. As a Hungarian, Kibunyi slowly falls prey to the national aspirations of a Habsburg-free Hungary, while Redl remains fiercely patriotic and faithful to his benefactor, the Austrian Emperor.  For Redl his relationship with Kibunyi goes beyond friendship as Redl harbors an unrequited love for his comrade. When the two young men visit a brothel, Redl seems more interested in watching his friend Kristof having sex with one of the prostitutes than in engaging the gal in his own room. Redl, suppresses his attraction to Kristof, however, and transfers it, as best as he can, to Katalin, his friends beautiful sister. Back at the academy, Alfred serves as a second in a duel between Kristof and another classmate, who is killed in the contest. This foolishness jeopardizes the careers of both Kubinyi and Redl, but the commanding officer, Colonel von Roden, having noted Redls hard work and loyalty to the Emperor, arranges a promotion for him and a prized assignment in Vienna. In Vienna, Redl is able to renew his friendship with Katalin, who is, by then, unhappily married. They become lovers in spite of Katalin knowing well that it is her brother who Alfred really loves.                                                                                                                               
                                                                              
Redl  is assigned to a garrison serving on the Russian border. The discipline there is lax and Redl readily stands out as a serious-minded young officer. When the district commander decides to retire, Redl is recommended for the job. As commanding officer, he proves very demanding, working hard to reinvigorate the discipline of his outfit. This does not sit well with the junior officers, including Kristof, especially because they feel superior to Redl by birth. When Redl and Kristof have a falling out over Kristofs sloppy habits and poor performance, Kristof mocks Redls lowly origins in conversation with other officers.

Colonel von Roden intervenes on Redls behalf again, bringing him back to Vienna to serve as deputy chief of the counter-espionage service. Its a nasty kind of job, since it entails spying on officers throughout the service, trying to identify those engaging in espionage activities for the Russians. On Katalins suggestion, Redl undertakes a loveless marriage of convenience in order to quell rumors of his homosexual proclivities. His wife, Clarissa, suffers from ill health and remains a distant figure in his life.

Redls single-minded devotion to duty soon draws him into the orbit of the heir to the crown, Archduke Franz Ferdinand, who is a ruthless schemer (whose ultimate objective is to overthrow the Emperor in a coup detat). Redl participates in one of the Archdukes plots, which involves setting up an aging Ukrainian officer for a dramatic fall so as to shake the army out of its complacency. The man is accidentally shot to death, however, during the search and seizure, negating the value of the plan. The Archduke then decides to make Redl the fall guy instead. Redl contributes to his own downfall by allowing himself to be seduced by an attractive young Italian officer. Redl is now doomed. Under arrest, hes given a service pistol with which to take his own life. It falls upon Kristof to provide Redl with the gun and order him to commit suicide. After experiencing anger, hesitation and despair, Redl finally shot himself in the head. The film ends with a brief depiction of the famous assassination of the Archduke, at Sarajevo, that triggered World War I.

==Cast==
* Klaus Maria Brandauer as Colonel Alfred Redl
* Hans Christian Blech as Major General Von Roden
* Armin Mueller-Stahl as Archduke Franz Ferdinand
* Gudrun Landgrebe as  Katalin Kubinyi
* Jan Niklas as Colonel Kristóf Kubinyi
* László Mensáros as Colonel Ruzitska
* András Bálint as Captain Dr. Gustav Sonnenschein
* László Gálffi as Alfredo Velocchio
* Dorottya Udvaros as Clarissa
* Károly Eperjes as Lieutenant Jaromil Schorm
* Róbert Rátonyi as Baron Ullmann

==Awards==
The film won the BAFTA Award for Best Foreign Language Film. It was nominated for an Academy Award for Best Foreign Language Film, but lost to The Official Story.   
 Jury Prize at the 1985 Cannes Film Festival.   

==See also==
* List of submissions to the 58th Academy Awards for Best Foreign Language Film
* List of Hungarian submissions for the Academy Award for Best Foreign Language Film

==References==
 

== External links ==
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 