Eastern Condors
 
 
{{Infobox film
| name = Eastern Condors
| image = Easterncondorsposter.jpg
| caption = Original film poster
| director = Sammo Hung
| producer = Corey Yuen Wu Ma Jeffrey Lau Leonard Ho
| writer = Barry Wong
| starring = Sammo Hung Yuen Biao Joyce Godenzi Yuen Wah Lam Ching-ying Yuen Woo-ping Corey Yuen Billy Chow
| music = Sherman Chow
| cinematography = Au Gaam Hung Arthur Wong
| editing = Peter Cheung Golden Harvest Media Asia
| released =  
| runtime = 100 minutes
| country = Hong Kong English
| budget = 
}} Hong Kong action film directed by Sammo Hung, who also starred in the lead role. The film co-stars Yuen Biao, Joyce Godenzi, Yuen Wah, Lam Ching-ying, Yuen Woo-ping, Corey Yuen and Billy Chow. The film was released in the Hong Kong on 9 July 1987.

It was set in the aftermath of the Vietnam War. Although set in Vietnam, the most of the film was shot in the Philippines. The scenes set in the United States were actually filmed in Canada.
The ensembles of convict soldiers in the film are reminiscent of similar squads in American action war films like The Dirty Dozen.
Hung believed his normal size and body shape, whilst suited to his comedic characters in his other films, would be inappropriate for a soldier. In order to get into shape for the lead role, Hung lost 30 pounds in 3 months, by surviving on a diet of nothing but chicken and rice. {{cite video
  | people = Sammo Hung
  | title = Eastern Condors, interview
  | medium = DVD
  | publisher = Contender Entertainment Group
  | date = April 2001}} 

==Plot==
Lieutenant Colonel Lam is an American army officer given a top secret mission by the US military. The mission entails entering Vietnam to destroy an old American bunker filled with missiles before the Viet Cong can get to them.

Due to the dangerous nature of the mission, a group of Chinese American soldier convicts are selected to accompany him, led by Tung Ming-sun. Survivors are promised a pardon, Citizenship in the United States|U.S. citizenship and $200,000 each. After a brief training session they are dropped into Vietnam. During the jump, Lam learns too late that the mission has been aborted.

Once in enemy territory, they are met by some Cambodian guerrillas led by Godenzi and take refuge in a small town. There they meet Weasel (aka Chieh Man-yeh), and his mentally ill "Uncle", Yeung.
 POW camp, where the prisoners are forced to play Russian roulette in a similar manner to the film The Deer Hunter. After escaping, they discover that one of the Cambodian guerrillas is a traitor. With the Vietnamese military in pursuit, they are able to reach the bunker, where a final showdown with the (giggling) Vietnamese general occurs.

==Cast==
* Sammo Hung - Tung Ming-sun
* Yuen Biao - "Weasel" / Chieh Man-yeh  
* Haing S. Ngor - Lung Yeung 
* Joyce Godenzi - Guerrilla Girl Leader 
* Chui Man-yan - Guerrilla Girl #2
* Ha Chi-chun - Lau Shun Ying / Guerrilla Girl #3
* Lam Ching-ying - Lieutenant Colonel Lam
* Melvin Wong - Colonel Yang Young
* Charlie Chin - Szeto Chin 
* Cheung Kwok-keung - Ching Dai-kong
* Billy Lau - Ching Dai-Hoi
* Yuen Woo-ping - Grandpa Yun Yen-hoy
* Corey Yuen - Judy Wu
* Peter Chan - Ma Puk-kau / Potato Onion Head
* Chin Kar-lok - Nguyen Siu-tran
* Hsiao Ho - Phan Man-Lung
* Lau Chau-sang - Stuttering Keung 
* Yuen Wah - Vietnamese Giggling General 
* Yasuaki Kurata - Generals elite soldier 
* Dick Wei - Generals elite soldier 
* Billy Chow - Generals elite soldier
* Ng Min-kan - Generals elite soldier James Tien - Village Thug / Angry Customer
* Phillip Ko - Vietnamese Corporal Soldier 
* Wu Ma - Vietnamese Corporal Officer 
* Kenny Ho - Col Youngs commando
* Max Mok - Col Youngs commando
* Chung Fat - Col Youngs commando
* Michael Miu - Col Youngs commando
* Ken Tong - Col Youngs commando
* Ben Lam - Col Youngs commando
* Andy Dai - Col Youngs commando
* Danny Poon - Col Youngs commando
* Chris Li - Col Youngs commando Wan Chi-keung - Col Youngs commando
* Angela Mao -

==Awards and nominations== 1988 Hong Kong Film Awards
** Nominated: Best Action Choreography
** Nominated: Best New Performer (Ha Chi-chun)
** Nominated: Best Supporting Actress (Joyce Godenzi)

==Home media== Region 2.

==See also==
*Sammo Hung filmography
*List of Hong Kong films
*Yuen Biao filmography

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 