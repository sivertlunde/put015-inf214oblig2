Arm in Arm Down the Street (1966 film)
{{Infobox film
| name           =Arm in Arm Down the Street
| image          =Arm in Arm Down The Street 1966 film.jpg
| image_size     =
| caption        =
| director       =Enrique Carreras
| producer       =
| writer         =Ariel Cortazzo, Armando Mook (play)
| narrator       =
| starring       =
| music          =Tito Ribero
| cinematography =Antonio Merayo 	
| editing       =Jorge Gárate 
| distributor    =
| released       = 4 August 1966
| runtime        =95 minutes
| country        = Argentina, Spain Spanish
| budget         =
}} 1966 Argentina|Argentine Spanish comedy-drama film directed by Enrique Carreras and starring Rodolfo Bebán, Evangelina Salazar and Susana Campos.  It won the Silver Condor Award for Best Film, given by the Argentine Film Critics Association in 1967 for the best picture of the previous year. 

==Cast==
*Rodolfo Bebán
*Evangelina Salazar
*Susana Campos
*Enzo Viena
*Luis Tasca
*Maruja Gil Quesada
*Javier Portales
*Lilian Valmar
*Rodolfo López Ervilha
*Mirtha Dabner

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 
 