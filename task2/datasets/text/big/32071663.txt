The Clue of the New Pin (1961 film)
{{Infobox film
| name           = The Clue of the New Pin
| image          = "The_Clue_of_the_New_Pin"_(1961).jpg
| caption        = 
| director       =   Allan Davis
| producer       =  Jack Greenwood
| writer         = Philip Mackie
| based on       = a novel by Edgar Wallace 
| starring       =  Paul Daneman  Bernard Archard  James Villiers
| music          = Ron Goodwin  (uncredited)
| cinematography = Bert Mason
| editing        = Anne Barker
| studio         = Merton Park Studios
| distributor    = Anglo-Amalgamated Film Distributors  (UK)
| released       = 1961
| runtime        = 58 mins
| country        = United Kingdom
| language       = English
}} British crime film directed by Allan Davis and starring Paul Daneman, Bernard Archard and James Villiers.  It was one of the series of Edgar Wallace Mysteries, British second-features, produced at Merton Park Studios in the 1960s.  
 The Clue a film in 1929.

==Plot==
TV journalist Tab Holland assists Scotland yard with the murder of a reclusive millionaire whose corpse is discovered locked in a vault. The key to the vault is mysteriously found on the table beside the corpse. 

==Partial cast==
* Paul Daneman - Rex Lander
* Bernard Archard - Superintendent Carver
* James Villiers - Tab Holland Katherine Woodville - Jane Ardfern
* Clive Morton - Ramsey Brown
* Leslie Sands - Sergeant Harris David Horne - John Tredmere
* Ruth Kettlewell - Mrs Rushby
* Wolfe Morris - Yeh Ling
* Maudie Edwards - Barmaid

==Critical reception==
TV Guide called it "slightly better than most of the 47 Edgar Wallace second features that producer Greenwood put out between 1960 and 1963."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 
 