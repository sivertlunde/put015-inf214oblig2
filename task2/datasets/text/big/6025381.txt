Anamika (1973 film)
{{Infobox film
| name           = Anamika
| director       = Raghunath Jhalani
| producer       = Tahir Hussain
| story          = Surendra Prakash
| screenplay     = Shashi Bhushan
| writer         = Madan Joshi (dialogues) Sanjeev Kumar Rajesh Behl [[Helen (actress)
|Helen]]
| music          = R. D. Burman Majrooh Sultanpuri (lyrics)
| cinematography = Munir Khan
| editing        = Babu Lavande Gurudutt Shirali
| released       =  
| country        = India Hindi
}}

Anamika ( ) is a 1973 Hindi film made in India and directed by Raghunath Jalani. It is a romantic, suspense film with memorable songs by composer R.D. Burman and lyricist Majrooh Sultanpuri, such as "Meri Bheegi Bheegi Si" sung by Kishore Kumar and "Bahon Mein Chale Aao" sung by Lata Mangeshkar.

This film is unrelated to the 2008 film Anamika (2008 film)|Anamika starring Dino Morea. 

==Plot== Sanjeev Kumar) who finds a young woman (Jaya Bhaduri) thrown from a speeding car with her hands tied and her mouth gagged. He, his uncle, and his secretary (Asrani) take the unconscious girl home, and when she wakes up the next morning, she doesnt remember anything about her life and insists that she is the writers wife. The uncle convinces the writer to humor the girl until she regains her memory. In the meantime, she shows him the crazy, fun part of life. He calls her Anamika, which means "Girl without a Name." As he is falling in love with her, he sees clues about her past that raise more questions: Is she a married woman named Archana or is she a prostitute named Kanchan? Just as unexpectedly as she had come into his life, she leaves him. When he sees her again at a party, she refuses to recognize him. The movies mystery deepens as the writer finds out who the woman actually is.

==Cast==
*Sanjeev Kumar  as  Devendra Dutt
*Jaya Bhaduri  as  Anamika/Kanchan/Archana
*Iftekhar  as  Dr. Irshad Husain
*Asrani  as  Hanuman Singh
*A. K. Hangal  as  Shiv Prasad
*Meena T.  as  Bijli, Maid Baby Pinki  as  Dolly
*Shivraj  as  Nareshs Father Helen  as  Rubai, Cabaret Dancer Yunus Parwaiz  as  Ram Lakhan
*Raja Duggal  as  Photoshop Owner Achla Sachdev  as  Archanas Mother
*Narendra Nath  as  Ganga Prasad Malhotra
*Rajesh Behl  as  Naresh

==Crew==
*Director - Raghunath Jhalani
*Story - Surendra Prakash
*Screenplay - Shashi Bhushan
*Dialogue - Madan Joshi
*Producer - Tahir Hussain
*Editor - Babu Lavande, Gurudutt Shirali
*Cinematographer - Munir Khan
*Art Director - Shanti Dass
*Production Manager - M. C. Dalal, Raju Ghandiyar
*Assistant Director - Ramesh Ahuja (chief), Ranganath Jadhav, Mohammed Maroof
*Assistant Editor - Dilip Dutt
*Assistant Art Director - Madhukar S. Shinde, Dilip Singh
*Costume and Wardrobe - Bhanu Athaiya, Mohammed Hussain, Sudha Parekh, Shalini Shah, Gopal (assistant)
*Choreographer - Suresh Bhatt

==Music==
*Song "Meri bheegi bheegi si" was listed at #4 on Binaca Geetmala annual list 1973
*Song "Bahon mein chale aao" was listed at #36 on Binaca Geetmala annual list 1973
Film expert and musicologist Rajesh Subramanian says the song Bahon mein chale aao was shot at Sur Mandir (bungalow owned by S D Burman) at Khar Road and the swing used in the song was actually purchased by R D Burman for his first wife Rita Patel.

{{Track listing
| headline        = Songs
| extra_column    = Playback
| all_lyrics      = Majrooh Sultanpuri
| all_music       = Rahul Dev Burman

| title1          = Aaj ki raat koi aane ko hai
| extra1          = Asha Bhosle
| length1         = 6:03

| title2          = Bahon mein chale aao sajna
| extra2          = Lata Mangeshkar
| length2         = 4:01

| title3          = Jaoon to kahan
| extra3          = Asha Bhosle
| length3         = 3:32

| title4          = Logo na maro
| extra4          = Asha Bhosle
| length4         = 4:33

| title5          = Meri bheegi bheegi si palkon pe reh gae
| extra5          = Kishore Kumar
| length5         = 4:05
}}

==Remake== Telugu and titled Kalpana (1977) starring Murali Mohan and Jayachitra. 

==References==
 

== External links ==
*  

 
 
 
 
 
 