Ten Who Dared
{{Infobox film
| name           = Ten Who Dared
| image          = Poster of the movie Ten Who Dared.jpg
| director       = William Beaudine
| producer       = Walt Disney James Algar
| based on       =  
| writer         = Lawrence Edward Watkin
| music          = Oliver Wallace
| cinematography = Gordon Avil
| editing        = Norman Palmer Cotton Warburton Ben Johnson John Beal Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
}}
 Walt Disney 1869 expedition. Hired by Walt Disney Studios in 1959 as a technical adviser, Otis R. Marston lead a film crew through the Grand Canyon to film river running and background scenes for the film. 

According to Allmovie, critics consistently rate this as one of the worst movies made by Disney.   The staff of Halliwells Film Guide called it "tedious and unconvincing". 

One of the replica boats used on the film, the Emma Dean, was recovered by local raconteur, Stan A. Jones, in 1969 from the Golden Oak Ranch, a Disney movie lot in Placerita Canyon, Newhall, Santa Clarita, California. 
The boat is on display at the Powell Museum in Page, Arizona. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 


 