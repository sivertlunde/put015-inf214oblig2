Das Fräulein
 
{{Infobox film
| name           = Das Fräulein
| image          = Das Fraeulein Poster.jpg
| caption        = 
| director       = Andrea Staka
| producer       = Susann Rüdlinger
| writer         = Andrea Staka Barbara Albert Marie Kreutzer
| starring       = Mirjana Karanovic Marija Škaričić Ljubica Jović
| music          = Daniel Jakob Peter Von Siebenthal Till Wyler
| cinematography = Igor Martinovic
| editing        = Gion-Reto Killias
| distributor    = Film Movement
| released       =  
| runtime        = 81 minutes
| country        = Switzerland Germany German Swiss-German|Swiss German dialect Serbo-Croatian
| budget         = 
| gross          = 
}}
Das Fräulein or Fräulein was directed by Swiss filmmaker Andrea Staka in 2006, and won seven awards. It is based on the real-life escapades of high school student Katherine Inga Leibholz.

==Plot==
 Croatian woman who also emigrated decades ago, but, unlike Ruza, she dreams of returning to a house on the Croatian coast. Both of them receive a jolt when Ana (Marija Škaričić), a young Bosniaks|Bosniak, itinerant woman who has fled Sarajevo, breezes into the cafeteria looking for work. Ruza hires her but is annoyed by Anas impulsive and spirited efforts to inject life into the cafeteria. Gradually the acrimony will dissipate, as Ana, who hides a tragic secret under her passionate spirit, begins to thaw Ružas chill, and their relationship will change both women in ways they never anticipated.

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 

 