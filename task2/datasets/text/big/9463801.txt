Battling Butler
{{Infobox film
| name           = Battling Butler
| image          =File:Battling Butler lobby card.jpg
| caption        =Lobby card
| director       = Buster Keaton
| producer       = Buster Keaton Joseph M. Schenck
| writer         = Al Boasberg Lex Neal
| narrator       = Walter James
| music          = 
| cinematography = Bert Haines  Devereaux Jennings
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       = September 19, 1926
| runtime        = 71&nbsp;min.
| country        = USA
| language       = Silent film English intertitles
| budget         =
}}
Battling Butler is a 1926 comedy silent film directed by and starring Buster Keaton.

==Plot==
Alfreds father wants him to make a man of himself so sends him off on a hunting and fishing trip. He doesnt catch or shoot anything, but he does fall in love with a mountain girl. When her father and brothers laugh at this they are told that he is Alfred "Battling" Butler, the championship fighter. From there on the masquerade must be maintained.

==Origins==
The film is one of Keatons instances where his source is based on a stage play. The play was actually called Battling Buttler and starred Charlie Ruggles on Broadway in 1923. It initially ran from October 8, 1923 – July 5, 1924 playing first at the Selwyn Theatre and then moving to the Times Square Theatre. In total there were a whopping 313 performances, a very impressive run for an original play. 

==Cast==
 
* Buster Keaton as Alfred Butler
* Sally ONeil as the mountain girl Walter James as her father
* Budd Fine as her brother
* Francis McDonald as Alfred "Battling" Butler
* Mary OBrien as his wife Tom Wilson as his trainer
* Eddie Borden as his manager
* Snitz Edwards as Alfreds valet

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 