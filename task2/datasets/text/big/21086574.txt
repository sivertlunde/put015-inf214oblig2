Two Guys from Milwaukee
{{Infobox film
| name           = Two Guys from Milwaukee
| image          =
| image_size     =
| caption        = David Butler
| producer       = Alex Gottlieb
| writer         = {{Plain list| Charles Hoffman
* I. A. L. Diamond 
}}
| starring       = {{Plain list|
* Dennis Morgan
* Jack Carson
* Joan Leslie
}} Fredrick Hollander
| cinematography = Arthur Edeson
| editing        = Irene Morra
| distributor    = Warner Bros.
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Two Guys from Milwaukee (UK title: Royal Flush) is a 1946 comedy film about a Balkan prince who wants to see for himself what America is really like. So he slips away from his entourage in New York and pretends to be an average guy. The movie stars Dennis Morgan, Jack Carson, and Joan Leslie.
 David Butler, and writer I. A. L. Diamond reteamed for the 1948 followup, Two Guys from Texas.

==Plot summary==
The Balkan Prince Henry arrives to Pennsylvania Station in New York City to do some research on how the "ordinary" man lives and works. Since his travel companions are unaware of his bold plan, he has to sneak away unseen to escape them. He catches a taxi outside the station, and gets to know the driver, Buzz Williams.

Henry makes up a background story for himself,not revealing he is a royal prince. He claims to be from Milwaukee, but it turns out the taxi driver is also born and bred there, which makes it harder for Henry to go through with his lie.

Buzz invites Henry into his Brooklyn home, and teaches the prince all there is to know about real life. Henry is introduced to Buzzs sister, Nan Evans, and her young daughter Peggy.

Unfortunately for Henry, there is a picture of him in the newspapers the next day, and it says he has been kidnapped. Henry assures Buzz that he will return in good time to stop his country from being converted into a republic. Buzz then helps Henry disguise himself by taking him to the barber shop where his girlfriend Connie Read works, and he shaves off his mustache.

That evening Buzz and Henry plan to go on a double date with Connie and her friend Polly. Before that, Buzz asks Connie to show Henry around the area. During the day, Connie and Henry fall for each other, and Henry ultimately suggests they go somewhere and eat alone, without the other two, who will be waiting for them at Happy Hash House according to the plan.

Henry arranges money to pay for dinner and Buzzs costs from his help, Count Oswald. Then he and Connie have dinner, and afterwards meet up with Buzz, Polly and Oswald at the restaurant. They go to a movie together, and Buzz pays for his ticket with Balkan money he got from Oswald.

The movie theater manager becomes suspicious about the money and calls the FBI. They arrive and follow Henry as he goes to Connies apartment. Henry is apprehended by the feds and brought back to his hotel where Oswald waits for him. The next day Buzzs niece Peggy comes to beg Henry to stay away from Connie because Buzz is so jealous. She wants Henry to help get Buzz and Connie back together, and Henry invites them all to his hotel to listen to a speech he is about to make on a radio broadcast to the people of his country.

While they are in the hotel suite, Connie tries to convince Buzz that they are not right for each other. Henry practices his speech and asks Buzz for help, which makes him talk about his love for the United States with quite the passion. They are unaware that the microphone is on and Buzzs words are broadcast on the radio. Since Buzzs speech is very good, he is quickly famous for his eloquence. Furthermore, the speech is so good that Connie falls back in love with him.

The people hearing Buzz talk are inspired by his speech, and vote for a transformation from monarchy to republic. Thus, Henry loses his royal title and privileges, and is free to stay in the US. He chooses to do that, and rushes off to Connie to ask for her hand in marriage. He is unaware that Buzz has done the same, and Connie has to decide which one of the completely different men she wants.

Ultimately Connie chooses to marry Buzz, the man she has known for a long time and been in love with since she met him. Disappointed, Henry decides to go to Milwaukee, his pretended hometown, where he has been offered to work for a beer company. On the plane, he sees his favorite actress, Lauren Bacall, but is then again discouraged when her husband, Humphrey Bogart, is sitting next to her. 

==Cast==
 
 
* Dennis Morgan as Prince Henry
* Joan Leslie as Connie Read
* Jack Carson as Buzz Williams
* Janis Paige as Polly
* S. Z. Sakall as Count Oswald
* Patti Brady as Peggy
* Rosemary DeCamp as Nan
* Tom DAndrea as Happy
* John Ridgely as Mike Collins Pat McVey as Johnson
* Franklin Pangborn as Theatre Manager
* Francis Pierlot as Dr. Bauer
 
==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 