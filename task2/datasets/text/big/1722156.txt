Space Cowboys
{{Infobox film
| name           = Space Cowboys
| image          = Space cowboys ver3.jpg
| image_size     = 225px
| caption        = Theatrical release poster
| director       = Clint Eastwood
| producer       = Clint Eastwood Andrew Lazar
| writer         = Ken Kaufman Howard Klausner
| starring       = Clint Eastwood Tommy Lee Jones Donald Sutherland James Garner Marcia Gay Harden William Devane Loren Dean Courtney B. Vance James Cromwell
| music          = Lennie Niehaus
| cinematography = Jack N. Green
| editing        = Joel Cox
| studio         = Malpaso Productions Village Roadshow Pictures
| distributor    = Warner Bros. Roadshow Entertainment   
| released       =  
| runtime        = 130 minutes
| language       = English
| budget         = $60 million Hughes, p.151 
| gross          = $128,884,132
}} space adventure adventure film Soviet satellite, nuclear missiles.

==Plot== flat spin from which they cannot recover, and both eject, narrowly missing being struck by a Boeing B-50 Superfortress|B-50 Superfortress piloted by "Tank" Sullivan (Matt McColm). After parachuting to the ground, Frank hits Hawk for putting their lives at risk, until Jerry ONeill (John Mallory Asher) breaks up their fight. When they get back to the air base, the base commander, Bob Gerson (Billie Worley) is disappointed by Hawks lack of responsibility. He tells them they are just in time for a press conference during which Gerson announces that the USAFs involvement with space flight testing was being terminated and transferred to the newly-created NASA. This puts an end to their dreams of space flight.
 Soviet communications satellite that is about to decay out of orbit and crash. The onboard systems respond but are so archaic that nobody at NASA can read the circuit diagrams or understand the communication systems. The satellite guidance system was copied from the original Skylab guidance systems that Frank (Clint Eastwood) designed. Frank is asked by Sara Holland (Marcia Gay Harden) and an astronaut who work for project manager Gerson (James Cromwell) to help, but Frank despises Gerson and refuses to have anything to do with him. Holland argues with Gerson about conflicting political and engineering issues affecting the decision to rescue the decaying satellite.
 Vice President, demanding that they be sent on the mission.

Competitive rivalry between the young astronauts and Team Daedalus continues, while Holland grows fond of Hawkins. During medical tests, Hawk is found to have terminal pancreatic cancer, and has only about eight months left to live. Due to the urgent nature of the mission and since his illness would not impair his effectiveness on the mission, he is deemed flightworthy.

The mission goes ahead with two crews, old and new, flying the space shuttle, which is also named Daedalus. As they approach the satellite, however, they become suspicious when the satellite reacts to their scans. Upon closer investigation, the team discovers that it is armed with six nuclear missiles, a dangerous and still active relic of the Cold War and a violation of the Outer Space Treaty. The team also learns that the system the satellite uses (from Skylab) was allegedly stolen by the KGB from Gersons personal files. It is implied that Gerson has prior dealings with the Russians and the confession was to protect Gerson from treason charges. Furthermore, the satellite is programmed to launch its payload at predetermined targets should it be allowed to fall out of orbit. To prevent the global catastrophe that would ensue, the team decides to use the payload-assist rockets that the shuttle is carrying in order to push the satellite out of Earth orbit into deep space.

However, Ethan Glance (Loren Dean), one of the younger astronauts, follows Gersons secret orders to try to move IKON into a stable orbit by himself. He connects the PAM rockets against Corvins orders, accidentally activating the satellite, and is incapacitated in the process. It collides with the shuttle, causing extensive damage. Roger Hines (Courtney B. Vance), one of the mission specialists, is also seriously injured in the collision, leaving the four senior astronauts to handle the crisis as the nuclear missile-laden satellite continues to fall from orbit.

Corvin and Hawkins deactivate the satellite. They then discover that there are not enough undamaged rockets to stabilize its now rapidly deteriorating orbit. With time running out, they decide to use the satellites own missiles rockets to push it away. There is one hitch: somebody has to go along to manually launch the missiles at the right time to ensure they do not enter an Earth-bound trajectory. Hawk decides to steer the rockets because he is the best pilot of the group and is dying anyway. He aims for the Moon, his lifelong ambition.

Meanwhile, the rest of the crew on the shuttle are not out of danger. The shuttles computers are not responding and most of the propulsion systems are damaged. NASA controllers decide to have the crew bring the shuttle as low as possible, then abandon ship and let it crash into the ocean. Corvin performs a de-orbit burn successfully as the space shuttle re-enters the atmosphere. Corvin makes it safely through, and flies to Florida, where he has Jerry make sure that the younger astronauts parachute to safety. Tank and Jerry refuse to leave Corvin alone to pilot the shuttle. Corvin lands the shuttle at the Shuttle Landing Facility at Kennedy Space Center, duplicating Hawks previous performance in a shuttle simulator.

Later, Corvin and his wife Barbara (Barbara Babcock) stand by their home and stare at the moon, contemplating whether Hawk has made it there. As their Earth-bound view of the moon fades into a nearer image of the moon, the Frank Sinatra song "Fly Me to the Moon" is heard. In a viewpoint sweeping close across the surface of the moon, wreckage from IKON and Hawks body are seen. Hawk is lying in a slightly reclined sitting position against a rocky outcrop where he had apparently dragged himself from the wreckage. As the view comes closer, the Earth is seen reflected in Hawks golden sun visor.

==Cast==
* Clint Eastwood as Colonel Frank Corvin, Ph.D., USAF (Ret.)
* Tommy Lee Jones as Colonel William "Hawk" Hawkins, USAF (Ret.)
* Donald Sutherland as Captain Jerry ONeill, USAF (Ret.)
* James Garner as Captain the Reverend "Tank" Sullivan, USAF (Ret.)
* James Cromwell as Bob Gerson
* Marcia Gay Harden as Sara Holland
* William Devane as Flight Director Eugene "Gene" Davis
* Loren Dean as Ethan Glance
* Courtney B. Vance as Roger Hines
* Rade Šerbedžija as General Vostow
* Barbara Babcock as Mrs. Barbara Corvin
* Blair Brown as Dr. Anne Caruthers
* Jay Leno as Himself
* Toby Stephens as Young Frank Corvin in 1958
* Eli Craig as Young "Hawk" Hawkins in 1958
* John Mallory Asher as Young Jerry ONeill in 1958
* Matt McColm as Young "Tank" Sullivan in 1958
* Billie Worley as Young Bob Gerson in 1958
* Jon Hamm as Young Pilot
* Chris Wylde as Jason the birthday boy

==Production==
Filming started in July 1999 and lasted three months.  Scenes were filmed on location at the Johnson Space Center in Houston, Texas, and the Kennedy Space Center and Cape Canaveral Air Force Station in Florida.  Interior shots of the flight simulator, shuttle, and mission control were filmed on sets at Warner Bros. 
 dubbed by their older counterparts.

The original music score was composed by longtime Eastwood collaborator Lennie Niehaus.

==Reception==

=== Critical response ===
Space Cowboys was well received by critics. Rotten Tomatoes gives it a score of 78% based on reviews from 118 critics. 

The film received a moderately favorable review from Roger Ebert: "its too secure within its traditional story structure to make much seem at risk &mdash; but with the structure come the traditional pleasures as well." 
 Best Sound Editing.

=== Box office === True Crime Absolute Power&mdash;combined. Hughes, p.152 

==References==
 

==Bibliography==
*  

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 