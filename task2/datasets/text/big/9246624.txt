Chronicle of an Escape
{{Infobox film
| name           = Chronicle of an Escape
| image          = Fugaposter.jpg
| caption        = Theatrical release poster
| director       = Israel Adrián Caetano
| producer       = Oscar Kramer Hugo Sigman
| screenplay     = Israel Adrián Caetano Esteban Student Julian Loyola
| story          = Claudio Tamburrini
| starring       = Rodrigo de la Serna Pablo Echarri Nazareno Casero
| music          = Ivan Wyszogrod
| cinematography = Julián Apezteguia
| editing        = Alberto Ponce
| distributor    = 20th Century Fox The Weinstein Company
| released       =  
| runtime        = 104 minutes
| country        = Argentina
| language       = Spanish
| budget         =
}}
Chronicle of an Escape ( ) is a 2006 Argentine film, directed by Israel Adrián Caetano. The screenplay is written by Caetano, Esteban Student, and Julian Loyola, based on the autobiographical Pase libre – la fuga de la Mansion Seré written by Claudio Tamburrini. The movie is also known as Buenos Aires, 1977. The motion picture was produced by Oscar Kramer and Hugo Sigman, and stars Rodrigo de la Serna, Pablo Echarri, Nazareno Casero, and others.

The winner of the Silver Condor Award for Best Film, it was Argentinas entry for the 2007 Golden Globes Awards for the Best Foreign Language Film, and director Israel Adrián Caetano was nominated at the 2006 Cannes Film Festival for a Golden Palm.   

The film tells the true story of four men who narrowly escaped death at the hands of a military death squad during the Argentine Dirty War in the 1970s.

==Plot==
In 1977, Claudio Tamburrini (  on the suspicion he is an anti-government terrorist.

Tamburrini is tortured by his jailers frequently. Yet, they look for information he does not have because he is not a political activist and never was. Tamburrini expects to be killed by the ruthless guards at any time. After four months of imprisonment, and many sessions of torture, Tamburrini and his fellow captives Guillermo (Nazareno Casero), Vasco and another man dive out a window during a rainstorm. The four, naked and with nothing but their senses, begin a desperate flight to freedom.

==Cast==
* Rodrigo de la Serna as Claudio Tamburrini
* Pablo Echarri as Huguito
* Nazareno Casero as Guillermo Fernández
* Daniel Valenzuela as Alemán
* Lautaro Delgado as El Gallego
* Matías Marmorato as El Vasco
* Martín Urruty as El Tano
* Diego Alons as Lucas
* Leonardo Bargiga as Capitán Almagro

==Background==
===Basis of film===
 
The film is based on a real political event that took place in Argentina after Jorge Rafael Videlas reactionary military Military dictatorship|junta assumed power on March 24, 1976. During the juntas rule the parliament was suspended, unions, political parties and provincial governments were banned, and in what became known as the Dirty War between 9,000 and 30,000 people deemed left-wing "subversives" forced disappearance|disappeared. 

The film is about one such instance: the kidnapping of Claudio Tamburrini, a goalkeeper of a B-league Association football|soccer/fútbol team, whos taken to a clandestine detention center and tortured.  He escaped and wrote Pase libre – la fuga de la Mansion Seré, a harrowing account of his experience.

Before the production began director Israel Adrián Caetano traveled to meet Tamburrini in Stockholm, where he now lives, to learn the truth about the event. There he also met Guillermo Fernández, the man who initiated the breakout.  Both former prisoners helped Caetano and the screenwriters stick closely to the theme of the film - survival.

===Production===
The production was difficult according to Caetano.  He said, "The filming was an endless challenge: shooting almost entirely within four walls, trusting the outcome to the acting, the framing, and the light. It was not always easy to generate fear, paranoia, and neurosis from this prison. Our challenge was how to re-create the madness these prisoners endured." 

==Reception==
===Critical response===
The Hollywood Reporters John DeFore thinks American audiences will like the thriller and its pace, especially after the men escape in the rain.  He believes that Chronicle of an Escape will give American audiences "an icky feeling" when they view the "fact-based Argentine story through the stylized lens of a horror film. Laced with dread that builds to a thoroughly gripping third act, it should do well with art house audiences who like their history lessons to come with a shot of adrenaline." 

Critic Deborah Young, writing for Variety (magazine)|Variety magazine, generally liked the film yet felt the films atonal score was at times irritating.  She wrote, " orceful acting plays a key role in giving the story credibility, with De La Serna and Casero lighting the way."  She also says the film "feels hollow at the core, leaving a feeling of lingering disappointment over a missed opportunity to probe recent history." 

Critic Clark Collis liked the film and wrote, "Chronicle of an Escapes premise may remind you of Sly Stallones enjoyably preposterous 1981 soccer/incarceration flick, Escape to Victory|Victory, but this true-story-based movie scores in a different way...The result is blessed with great performances; director Israel Adrián Caetano lets events speak — and plead and weep — for themselves." 

Stephen Holden, critic for The New York Times, discussed the film from todays political perspective in the United States. He wrote, "Despite its restraint, Chronicle of an Escape is deeply unsettling. Although the events it depicts happened 30 years ago in South America, it inevitably triggers anguished thoughts of Abu Ghraib, Guantanamo Bay detention camp|Guantánamo Bay and extraordinary rendition...But as a suspenseful drama of captivity and escape, the movie is a carefully paced, persuasively acted thriller. Its first two-thirds observe Claudios four-month internment, during which he becomes resigned to the idea that he will be killed. Unlike the other captives, he does not belong to a subversive group; midway through the film he discovers to his fury that an acquaintance and fellow prisoner, while under torture, lied and named him as the owner of a mimeograph machine that printed antigovernment leaflets. The message is explicit: Torture may make people talk, but they’ll say anything to avoid further abuse." 

Critic Jan Stuart echoed Stephen Holden and wrote, "If there is anything more disturbing than any of the tortures glimpsed in the film, it is turning on the evening news afterward and being reminded that the use of such hardball practices are still, in the minds of some, a subject for debate." 

The review aggregator Rotten Tomatoes reported that 60% of critics gave the film a positive review, based on fifteen reviews. 

===Accolades===
;Awards
* Clarín Awards: Clarin Award; Best Director, Adrián Caetano; Best Film; Best New Actor, Nazareno Casero; Best Original Music, Iván Wyszogrod; Best Screenplay, Adrián Caetano, Esteban Student, and Julian Loyola; Best Supporting Actor, Nazareno Casero; 2006.
* Argentine Academy of Cinematography Arts and Sciences Awards: Premios Sur; Best Supporting Actor, Nazareno Casero; Best New Actor, Nazareno Casero; Best Original Music, Iván Wyszogrod, 2006. 
* Argentine Film Critics Association Awards: Silver Condor, Best Editing, Alberto Ponce; Best Film; Best Adapted Screenplay, Adrián Caetano, Julian Loyola, and Esteban Student; 2007.

;Nominations Cannes Film Festival: Golden Palm, Adrián Caetano, 2006. 
* Independent Spirit Awards: Independent Spirit Award Best Foreign Film, Adrián Caetano, Argentina, 2007.
* Argentine Film Critics Association Awards: Silver Condor, Best Actor, Rodrigo De la Serna; Best Art Direction, Juan Mario Roust and Jorge Ferrari; Best Cinematography, Julián Apezteguía; Best Director, Adrián Caetano; Best Music, Iván Wyszogrod; Best Sound, Fernando Soldevila; Best Supporting Actor, Nazareno Casero; Best Supporting Actor, Pablo Echarri; 2007.
* Penthouse Film of the Year: Golden Woodie, Adrián Caetano, 2006.

==Distribution==
The film first opened in Argentina on March 24, 2006.

The film was shown at various film festivals, including:  the 2006 Cannes Film Festival, France; the Toronto Film Festival, Canada; the Toulouse Latin America Film Festival, France; the Espoo Ciné International Film Festival, Finland; and others.

In the United States it was featured at the Austin Film Festival in Texas on October 26, 2006.

==References==
 

==External links==
*  
*  
*  
*   at the cinenacional.com   La Nación by Fernando López  
*   review at LeerCine by  Santiago García  
*   trailer at YouTube  

 

 
 
 
 
 
 
 
 
 
 
 