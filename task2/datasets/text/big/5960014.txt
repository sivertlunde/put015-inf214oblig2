Rehguzar
 

{{Infobox Film |
  name     = Rehguzar |
  image          = rehguzar.jpg |
  caption  =  |
  imdb_id        =  |
  writer         = Partho Mukharjee |
  starring       = Shekhar Suman, Simple Kapadia, Aarti Mehta, Jyoti Sarup |
  director       = Jyoti Sarup |
  producer       = Premlata Mehta |
  distributor    = Surya Bharti Creations | telecasted on Doordarshan |
  runtime        = 135 |
  language = Hindi |
  music          = Sardar Anjum |
  awards         = |
  budget         = 150,000 (estimated) |
}}
 National Award released for telecasted on Doordarshan in 1989. This film had the music of Sardar Anjum, a then famous Ghazal writer/singer. This is the only film for which he ever gave music.

==Plot summary==
A small town, aspiring, lyricist Shekhar (played by Shekhar Suman) comes to the dream city called Bombay (now Mumbai) to make it big. He stays in the city on rent in a house, which is owned by a person (played by Bharat Bhushan) from his town. The daughter of his landlord, Simple (played by Simple Kapadia) falls in love with him and he reciprocates the feelings too. Meanwhile he meets the secretary (played by Jyoti Sarup) of a famous lyricist in the business. The secretary buys his lyrics and passes it on to his boss who records the songs, which keep becoming a hit. Getting frustrated of ghost writing for others he searches ways to get famous himself. He meets a famous female singer, Aarti (played by Aarti Mehta) who, liking his lyrics, sings for his songs and helps him get fame.

The conflict for him arises when he comes to know that the Aarti also loves him. The secretary takes advantage of the situation and tries to create a rift between Aarti and Shekhar so that she sings for the lyrics written by his boss. While Shekhar is in a dilemma, of what to do because he is in love with Simple, and Aarti is the one who has helped him achieve whatever he wanted in life, Aarti finds out she has Cancer. Clearing things with Shekhar, she then dies of the disease.

==Cast==
*Shekhar Suman ...Shekhar
*Simple Kapadia ...Simple
*Aarti Mehta ...Aarti
*Bharat Bhushan ...Landlord
*Mayur ...Film Star
*Jyoti Sarup ...Lyrics writers Secretary

== References ==

 

==External links==
*  

 
 
 


 