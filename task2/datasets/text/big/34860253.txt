The First Time (2012 Chinese film)
 
{{Infobox film
| name           = The First Time
| image          = The First Time poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Han Yan
| producer       = Bill Kong
| writer         = 
| screenplay     = Han Yan
| story          = 
| based on       = 
| narrator       = 
| starring       = Angelababy Mark Chao Jiang Shan
| music          = Keith Chan   Zhou Jiaojiao
| cinematography = Charlie Lam
| editing        = Angie Lam
| studio         = BDI Films
| distributor    = 
| released       =  
| runtime        = 106 min.
| country        = China/Hong Kong
| language       = Mandarin
| budget         = 
| gross          = 
}}
The First Time ( ) is a 2012 Chinese romance film written and directed by Han Yan (韓延).  

It is a remake of the 2003 South Korean film ...ing.

==Plot==
Sonq Shiqiao (Angelababy), 22, lives with her devoted mother, widowed shop owner Zheng Qing (Jiang Shan). Shiqiao, who always dreamed of being a ballet dancer, cannot exert herself physically as she suffers from form of muscle weakness|myasthenia, a neuromuscular disease, that her father died of; the medication she takes also causes memory lapses. One day, at a charity fair, she bumps into Gong Ning (Mark Chao), a former high-school friend she always liked, and the two end up dating, despite the initial disapproval of her mother. Gong Ning dropped out of university to spend more time with a rock band he leads; also, his girlfriend, dancer Peng Wei (Cindy Yen), has dumped him because of his inability to focus his life. However, for Shiqiao, Gong Ning is the perfect partner. 

==Cast==
*Angelababy as Sonq Shiqiao
*Mark Chao as Gong Ning / Lü Xia
*Jiang Shan as Zheng Qing, Shiqiaos mother
*Cindy Yen as Peng Wei, Gong Nings ex-girlfriend
*Allen Chao as Gong Nings father Tian Yuan as Gu Qi, rock band member
*Bai Baihe as Wei Jiajia, rival rock singer
*Huang Xuan as Li Rao, Peng Weis new boyfriend
*Zhao Yingjun as rock band member
*Wu Xiaoliang
*Fan Lin

==References==
 

 
 
 
 
 
 