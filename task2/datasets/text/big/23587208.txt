Les Innocents (film)
{{Infobox film
| name           = Les Innocents
| image          = Les Innocents, film poster.jpg
| caption        = Theatrical release poster
| director       = André Téchiné
| producer       = Philippe Carcassonne	  Alain Terzian	 
| writer         = Pascal Bonitzer   André Téchiné
| starring       = Sandrine Bonnaire  Simon de La Brosse  Abdel Kechiche  Jean-Claude Brialy
| music          = Philippe Sarde
| cinematography = Renato Berta
| editing        = Martine Giordano
| studio         = 
| distributor    = 
| released       = 23 December 1987
| runtime        = 100 min
| country        = France 
| language       = French
| budget         = 
| gross          = 
}}
 Les Innocents   ( ) is a 1987 French drama film directed by André Téchiné and starring Sandrine Bonnaire, Simon de La Brosse and Abdel Kechiche. The plot follows a girl who, whilst looking for her runaway brother, encounters a number of people who influence her life. The film was partially inspired by a William Faulkner novel. Téchiné uses several French-Arab relationships to mirror the tensions between France and its former colonies.

Jean-Claude Brialy won a Cesar Awards|César award for Best Supporting Actor and the film was nominated for three other César awards: Best Film, Best Director and Best Original Music.

==Plot==
Jeanne, a young woman born and raised in Northern France, is visiting the Mediterranean for the first time. She arrives at the southern French seaport of Toulon to attend the wedding of her older sister Maïté to Nourredine, a North African. However, her main goal is to repatriate her deaf-mute younger brother, Alain, with whom she had been living following the death of their parents. At the wedding celebration, Jeanne meets Klotz, a middle age orchestra conductor prone to drinking. Jeanne re-encounter with Alain is not a happy one. She learns that her teenager brother has been supporting himself pickpocketing under the tutelage of Saïd, a Frenchman of Algerian descent who approached Jeanne on her arrival in the city. Jeanne sleeps next to her brother; but the next morning, Alain runs away stealing all of Jeanne’s money, even a family heirloom that she has brought as a wedding gift for Maïté.

Desperate to find Alain, Jeanne goes to the elementary school where Maïté works, to see if she knows where Alain might be. Maïté is disappointed with her two siblings and she is in no mood to help. In her lunch hour from work, a lover is waiting for  Maïté, who confesses to Jeanne that she got married with Nourredine only to have a child.

Jeannes first lead in finding her brother is the struggling orchestra conductor Klotz, a bisexual older man who is infatuated with Saïd. Saïd treats Klotz very badly, but Klotz lust after the handsome and cocky Saïd. When Jeanne visits Klotzs luxurious beach-side villa, she meets Klotz son Stéphane, who is recovering from a coma, and Stéphanes overbearing mother, Myrian. Stéphane is immediately attracted to Jeanne and takes her in his motorcycle to the auditorium where his father is rehearsing with his orchestra.

When Jeanne finds Alain, she moves into the modest hotel he shares with Saïd, which is run by a repatriated Pied-Noir from Algeria. At the same time Jeanne starts a relationship with Stéphane, but there is a secret link between Stéphane and Saïd. Saïd takes Jeanne to see a burns victim and reveals to her that Stéphane had once been part of a far right racist gang which had set fire to an immigrant hostel, and was later stabbed by Saïd in revenge.

During a brief stay in Algeria, Saïd telephones Stéphane to tell him of the night he has spent with Jeanne, which motivates Stéphane to denounce Saïd to the racist gang. However, Stéphane catches up with Saïd and warns him not to go back to the hotel. Saïd, however, drags Stéphane with him and they are both shot dead. The film ends with Jeanne staring at the two bodies.

==Cast==
*Sandrine Bonnaire as Jeanne Abdel Kechiche as Saïd
*Jean-Claude Brialy as Klotz
*Simon de La Brosse as Stéphane
* Tanya Lopert as Mme Klotz
*Marthe Villalonga as Hotels owner
*Jacques Nolot as doctor
*Marie-France Garcia|Marie-France as singer
*Christine Paolini as Maïté
*Krimo Bouguetof as Noureddine

==Analysis==
The lines between love, sex, and politics become hopelessly blurred in this French drama. Jeanne finds herself torn between the two men a French and a North African in a romantic and sexual dilemma that mirrors Frances political turmoil regarding the nations growing Arab population.

==Accolades==
It was nominated to four César Awards: Best Film; Best Director; Best original music and Jean-Claude Brialys performance in this film earned him a Best Supporting Actor award from the French Academy of Cinema.

==DVD release==
Les Innocents is available in Region 2 DVD. It was released in France.

== References ==
*Marshall, Bill, André Téchiné, Manchester University Press, 2007, ISBN 0-7190-5831-7

== External links ==
*  

 

 
 
 
 
 
 
 