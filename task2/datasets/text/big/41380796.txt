I Love the Sound of the Kalachnikov It Reminds Me of Tchaikovsky
  
}}
I Love the Sound of the Kalachnikov It Reminds Me of Tchaikovsky is a 2001 English documentary directed by Philippe Vartan Khazarian. 
 A short visit to a war zone in Nagorno-Karabakh evokes memories of both the terrible fate of the city of Agdam and director Khazarians own personal tragedy years earlier in France. 

==Plot==
An autobiographical documentary film that goes beyond the barriers of the genre and is something between videoart, experimental film and home video and seeks to throw light on the consequences of the Armenian Genocide of 1915, which forced the director´s family to emigrate to France. In the spirit of Bertolt Brecht´s theory of art and distanciation, Khazarian frees himself from reality, combining in the film amateur films about his own family and contemporary footage from the battlefield in Nagorno-Karabakh. His film is not a recapitulation of historical fact, but rather a visual meditation on the fetishist aesthetics of war, diverse sexual orientations and the consequences of emigration. The film deals with topics such as war, destruction and sexuality, which, in the director´s view are indissolubly linked.

==Nominations==
I Love the Sound of the Kalashnikov, It Reminds Me of Tchaikovsky was selected in the Rotterdam Film Festival 2001.   and was soon nominated in 19 international film festivals, receiving 4 international awards, 12 international selections and 6 international competitions including :
*Festival Centre Piece, MIX NYC, NYC Lesbian and Gay Experimental Film Festival 2003    
*Special Mention of the Jury, Toronto Image Film Festival 2003    where it was personally introduced by Canadian director Atom Egoyan
*Opening Night Film, East/West International Film Festival 2001 
*International Competition Nominee, Sunnyside of the doc 2001 
* Official Selection, San Francisco Armenian Film Festival 2006   
*Official Selection, Lisboa International Documentary Film Festival 2002   
*International Selection, Lisboa ‘In the closet’ Film Festival 2004
*Representing Contemporary British Cinema, Febio Fest Prague International Film Festival 2004  
*Special Mention of the Jury, Dei Popoli Film Festival 2002 
*Guardian Award First Film Competition Nominee, Edinburgh Film Festival 2001 
*International Selection, Tursak International Selection 2002
*International Selection, Haifa International Film Festival 2001  
*International Selection, Beirut International Film Festival 2002 
*International Selection, Mostra Internazionale del Nuovo Cinema 2001 
*International Selection Finalist London Lesbian&Gay Film Festival   
*International selection, Lisboa L&G International Film Festival 2003

==References==
 

 
 
 
 
 
 
 