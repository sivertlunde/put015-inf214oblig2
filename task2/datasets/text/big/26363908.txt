Zis Boom Bah
{{Infobox film
| name           = Zis Boom Bah
| image          = Grace Hayes, Peter Lind Hayes, and Mary Healy in Zis Boom Bah (1941).jpg
| image_size     = thumb Mary Healy in Zis Boom Bah (1941)
| director       = William Nigh
| producer       = Sam Katzman (producer) Peter A. Mayer (associate producer)
| writer         = Connie Lee (original story) & Harvey Gates (original story) Harvey Gates (screenplay) & Jack Henley (screenplay)
| narrator       = Mary Healy
| music          =
| cinematography = Marcel Le Picard
| editing        = Robert Golden
| distributor    =
| studio         = Monogram Pictures
| released       = 1941
| runtime        = 61 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Zis Boom Bah is a 1941 American film directed by William Nigh. The film is also known as College Sweethearts.

== Plot ==
Grace Hayes, as herself essentially, Grace Hayes, has been content to play the vaudeville circuit, and support her son, and the wealthy family who shunned her.
 Mary Healy, as Mary Healy, in cognito. 
 going to Hell in a hand basket” despite the earnest efforts of the kind hearted Dean, Richard "Skeets" Gallagher, as Professor Warren. The college and the old families are running out of money and spirit.

Grace buys the local diner, turns it into a version of her real-life club, encouraging the kids to  , and put on a show to raise the funds and spirit the college needs to survive. It doesn’t take long, with a little elbow grease, for the kids’ heart to shine through.

The question remains whether the college will survive, or if the Dean, Peter and the kids will be joining Grace and Mary on the road.

== Cast ==
*Grace Hayes as Grace Hayes
*Peter Lind Hayes as Peter Kendricks Mary Healy as Mary Healy
*Huntz Hall as Skeets Skillhorn
*Jan Wiley as Annabella Frank Elliott as Mr. Kendricks
*Richard "Skeets" Gallagher as Professor Warren
*Benny Rubin as Nick
*Eddie Kane as James J. Kane
*Leonard Sues as Noisey
*Roland Dupree as Pee Wee

uncredited
*Betty Compson

== Soundtrack ==
* "Annabella" (by Johnny Lange and Lew Porter)
* "It Makes No Difference When Youre in the Army" (by Johnny Lange and Lew Porter)
* "Ive Learned to Smile Again" (by Neville Fleeson)
* "Good News Tomorrow" (by Neville Fleeson)
* "Put Your Trust in the Moon" (by Joan Baldwin and Charles R. Callender)
* "Miss America" (by Earl Hammand and Lee Ellon)

==Production== Las Vegas nightclub, The Red Rooster. 
 Mary Healy were married from 1940 to Hayes death in 1998, and regularly worked together, notably on the film The 5,000 Fingers of Dr. T (1953).

Benny Rubin, who tap dances his way through the film, as young college kid Nick, was 42, when the film was made. 

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 