Rang
 
 
 

{{Infobox film
| name           = Rang
| image          =
| caption        = 
| director       = Talat Jani
| producer       = 
| writer         = Rumi Jaffrey Talat Jani Sagar Sarhadi
| starring       = Jeetendra Amrita Singh Kamal Sadanah Divya Bharti Ayesha Jhulka
| music          = Nadeem-Shravan
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
Rang ( : رنگ, translation: Colours) is a Bollywood film directed by Talat Jani and starring Jeetendra, Amrita Singh, Kamal Sadanah, Ayesha Jhulka and Divya Bharti in one of her last film appearances. Bharti died three months before the film was released and it was dedicated to her memory. The film was a moderate box office success due to Bhartis sudden demise.

==Plot==
 
Yogi Joshi (Kamal Sadanah) is the hardworking middle class student who goes to college with the traditional Pooja (Ayesha Julka), who is the daughter of a single-father. Pooja has been in love with Yogi for quite some time, but Yogi seems to see her as a good friend. Pooja is very possessive of a particular parking spot in school and does not like anyone parking in that spot.  

Yogis father (Bharat Kapoor) works at the factory owned by the successful Indu Singh (Amrita Singh). Indu admits her daughter into the same college as Yogi and Pooja. She is a trustee of the college and is assured by the Principal that Kajal is in good hands. Kajal (Divya Bharti) goes to college and tries to park in Poojas spot, but Pooja cuts her off.  The two get in an altercation and to show Pooja how much power Kajal has, she drives her jeep into the corridors of the college and stops right in front of Pooja and her friends, including Yogi. Pooja holds her own and is unimpressed. Pooja and Yogis friend Jojo, who had been run over by Kajal in her rampage, tells his friends that he cut Kajals breaks. Yogi, feeling responsible for Jojos actions, runs after Kajals car. He catches up to her right as her car falls over a cliff into a river. He saves the unconscious Kajal from drowning and brings her home. Indus mother, the snobby Suchitra (Bindu (actress)|Bindu) offends Yogi by trying to give him money, but he politely refuses.

That night, Kajal and Yogi dream about each other as they fall in love. The next day, Kajal approaches Yogi and asks if they could be friends.  Yogi points out the difference in their class and status. Specifically he mentions that Kajal has a car, while he only has his feet. So Kajal decides that she wont use a car until Yogi goes on a date with her. Yogi finds out that Kajal is walking home and is worried. He finds her drenched in sweat being followed by her two Help in her car. He decides to go on a date with her to convince her. They go to see a movie, Bobby (1973 film)|Bobby. They imagine themselves as the characters during the famous song "Hum Tum Ek Kamre Mein Band Ho." During a song sequence they are shown to be spending a lot of time together and have fallen in love. Suchitra sees the two of them together and is angry.

On Rose Day, Kajal knows that Yogi is waiting for her with a rose so she obsesses on what to wear and is late to college. Pooja sees Yogi holding a rose and waiting for someone. She assumes the rose is for her and takes it from his hands and runs away. She feels that this is a sign that Yogi too loves her.

Meanwhile, Yogis father is the Union leader and when the factory workers decide to protest against Indus company, she fires Mr. Joshi. He is helped by chairman Ajay Malhotra (Jeetendra) who is a hero to factory workers. Ajay Malhotra turns out to be Poojas father. When he finds out that Pooja is in love with Mr. Joshis son, he goes to the Joshi residence in the pouring rain to ask for Yogis hand in marriage for Pooja. Mrs. Joshi (Rita Bhaduri) knew her son was in love with someone and had assumed it to be Pooja, who had come to her house looking for Yogi.  

When Yogi finds out about the proposal, he informs his parents that he is in love with Indus daughter Kajal. His parents show him that he wants to marry the daughter of the woman who ruined his father, as opposed to the daughter of the man who saved him. So Yogi tries to break it off with Kajal, but he cannot bare to stand by this decision. When Yogi tells Pooja that he is in love with Kajal, Pooja tries to commit suicide. A distraught Ajay goes to Kajal and begs her to save his daughter by breaking up with Yogi.  She responds by telling him that she cant give up her life to save his daughters life.  

Kajal is touched by Ajays fatherly devotion and thinks of her own father. She asks her grandfather, Madhav Singh (Kader Khan) about her father. Her grandfather finds an old picture of her father. To her surprsie, Ajay Malhotra is her father. Madhav Singh tells Kajal that her father was an activist for workers rights. Suchitra had never approved of the match as Indus family was rich and Ajay was a lowly middle class factory worker. However the two were in love, so they married and they had a daughter. When Ajay led the Singh factory workers into strike, Suchitra had instigated the workers against Ajay by making it seem as if he had been bribed to end the strike. He confronted his mother-in-law and asked Indu to choose between her mother and her family. She hesitated as she felt dizzy. Ajay didnt see her being dizzy and thought Indu was choosing her mother, so he angrily took their baby daughter and walked out on their relationship. Indu fainted and found out that she was pregnant again. Suchitra convinced her to raise this child by herself and let Ajay go.

When Kajal realises that Pooja is her elder sister, she decides to sacrifice her love to save her sisters life by moving to America. This decision leaves Yogi heartbroken, which puts Kajal in a miserable state. Unable to see his granddaughter in pain, Madhav goes to Ajay and informs him that Kajal is his daughter and that he is sacrificing one daughter for another.  

Ajay, unaware that he had another daughter, rushes to stop Kajal from going to America. He moves Kajal into her own apartment and spends time with her. He keeps his relationship with Kajal a secret from Pooja as he is waiting for the right time to tell her. Ajay also helps Yogi mend his relationship with Kajal by explaining her situation. The lovers reunite.

Pooja starts suspecting that her father is keeping something from her and follows him one day. She follows him to Kajals apartment and finds her father spending time with her. She then sees Yogi joining them and her father being supportive of Yogi and Kajals relationship. Pooja feels betrayed and confronts her father. Her father informs Pooja that Kajal is her younger sister. Kajal again decides to go to America, this time to get out of the way of Ajays relationship with Pooja. This time, Pooja stops her and embraces her little sister.

Suchitra finds out that Kajal never reached America and has Yogi arrested for kidnapping. Ajay comes to the rescue and confesses that he has Kajal. The police cannot charge Ajay for taking care of his daughter, so they release Yogi. Indu goes to Kajal and tries to force her to go home, but Kajal refuses. Indu is hurt and walks away. On her way out, Ajay invites Indu to their daughters wedding. Pooja then goes to see her estranged mother. Pooja confronts Indu for never caring about her. Indu tells her that she never stopped thinking of her elder daughter. Pooja begs Indu to reconcile with her father so that the girls will have both a mother and father, instead of having to choose between the two. Madhav Singh finally stands up to his wife Suchitra to go to Kajals wedding. Suchitra melts and decides to go to the wedding too.

Ajay tries to contact Indu, but she arrives before he can do that. The two finally reconcile and decide to put their past behind them. The family is finally together.

==Cast==

*  Divya Bharti ...  Kajal 
*  Kamal Sadanah  ...  Yogi joshi
*  Ayesha Jhulka  ...  Pooja Malhotra
*  Jeetendra  ...  Ajay Malhotra  
*  Amrita Singh  ...  Indu     Bindu  ...  Suchitra Madhav Singh  
*  Kader Khan  ...  Madhav Singh  
*  Rita Bhaduri  ...  Mrs. Joshi (as Reeta Bhaduri)  
*  Bharat Kapoor  ...  Mr. Joshi  
*  Raju Shrestha  ...  Jojo (as Raju Shreshta)  
*  Dinesh Hingoo ...  Board member 
*  Tiku Talsania ...  Principal 
*  Arun Bakshi ...  Lecturer 
*  Jimmy Patnayak    
*  Chhote Ustad  ...  Rangeela (as Chhotu Dada)  
*  Nilofar  ...  Poojas friend (as Nilofer)  
*  Baby Ashrafa  ...  Jyoti   Sunil
*  D.L. Khan

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="centr"
! # !! Title !! Singer(s)
|-
| 1
| "Tumhein Dekhen Meri Aankhein"
| Kumar Sanu, Alka Yagnik, P.Sunanda
|- 
| 2
| "Teri Mohabbat Ne"
| Kumar Sanu, Alka Yagnik
|- 
| 3
| "Tujhe Na Dekhoon" Kumar Sanu, Alka Yagnik
|- 
| 4
| "Hum Tum Picture Dekh Rahe"
| Udit Narayan, Alka Yagnik
|- 
| 5
| "Dil Cheer Ke Dekh"
| Kumar Sanu
|- 
| 6
| "Mere Pyar Ka Hisab" 	
| Kumar Sanu, Alka Yagnik
|-
| 7
| "Coming Coming Coming"
| Alka Yagnik
|-
| 8
| "Dil Cheer Ke Dekh-Female"
| Alka Yagnik
|}

==External links==
*  at the Internet Movie Database

 
 
 
 
 