Fertile Ground (film)
{{Infobox film
| name           = Fertile Ground
| image          = Fertile Ground.jpg
| border         = 
| alt            = 
| caption        = Theatrical poster 
| director       = Adam Gierasch
| producer       = Moshe Diamant Courtney Solomon
| writer         = Jace Anderson Adam Gierasch
| starring       = Gale Harold Leisha Hailey
| music          = Joseph Conlan
| cinematography = Yaron Levy
| editing        = Andrew Cohen
| studio         = 
| distributor    = After Dark Films
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
Fertile Ground is a 2011 horror film directed by Adam Gierasch. The film was released as part of the After Dark Originals line on January 28, 2011 and stars Leisha Hailey as a pregnant woman who finds herself subjected to several strange supernatural occurrences.  

==Plot==
Emily (Leisha Hailey) and Nate Weaver (Gale Harold) are a happily married couple living in New York. Nate is a recognized artist, Emily a fashion designer, and the two are expecting their first child. Emily suffers a terrible miscarriage during a dinner party and she is informed that the scarring left on her womb means she will never be able to conceive again.

To give them both a fresh start, Nate and Emily move out into the countryside to Nates family home, which has been uninhabited for some time. At first enchanted by it, Emily is horrified when a skeleton is discovered on the property, but it is obvious that the body is very old, so no danger can be attached to the house. After Emily discovers an old trunk filled with old baby things in the cellar, she pays a visit to the local Historical Society and is told that the house has a past she and Nate were unaware of. Several suspicious deaths have occurred at the property in the 18th and 19th century, and Nates aunt had vanished without a trace when the house was new.

Nate grows distant and cold towards Emily as he starts painting again, acting strangely. Tense and isolated, Emily starts to see visions of a woman covered with blood, and faints. After seeing the doctor, who thinks she is suffering from trauma, she gets a phone call to tell her that she is actually pregnant. Ordered to spend the bulk of her pregnancy in bed resting due to the high risk of miscarriage, Emily starts to get bored and frustrated by Nate, who is working longer and longer hours and seems uninterested in her or the coming baby. Emilys visions continue; she organizes a party for her city friends to rid herself of the loneliness. At the party, she believes that Nates agent is having an affair with him. Later, the agent somehow falls through the window of Emilys studio and is hospitalized. Nate thinks Emily may have pushed her as he saw her walk away from the window after the woman fell.

When Nate is informed that his agent has died, he storms out angrily. Emily reads through a folder of press cuttings left by the Historical Society and discovers that all the women who died at the house were killed by their husbands. Every woman had been pregnant at the time; supposedly, the house "changed" their husbands and turned them evil. Emily has another vision of the dead woman and believes that Nate is coming back to kill her. She calls her best friend, who tells her that she will come and pick her up.

Terrified, Emily hides in her bedroom with a knife when she hears Nate come back into the house. She tries to stab him with the knife but instead stabs her friend—who had driven back with Nate. Emily climbs up onto the roof to save herself. She believes she is fighting one of the ghosts of the house; they fall from the roof and she stabs him repeatedly. After killing him, Emily realizes she has actually killed Nate.

At the hospital, a scan reveals that  . Emily is left in a padded room, rocking her imaginary child in her arms.

==Cast==
* Leisha Hailey as Emily Weaver
* Gale Harold as Nate Weaver
* Jami Bassman as Mary Weaver
* Chelcie Ross as Avery
* JoNell Kennedy as Brittany McGraw

==Production== Burnt Offerings, The Changeling, and Repulsion.    The two also noted that while this was their second film for After Dark Films, their previous work had already been completed prior to it being picked up by After Dark while Fertile Ground had not yet been filmed and as such, After Dark had more input on its production.    Anderson drew upon some of her own experiences while writing the character of Emily and utilized an image she saw in a dream along with a few articles she had read previously.  Gierasch noted that the film was "a huge departure for  ", as it was the first film that they had made that had more dramatic elements in it as opposed to "blood and demons and arms being chopped off".  They cast Leisha Hailey as Emily after she "walked into the room and started reading" at a casting session, as they felt that she captured the character perfectly in the audition.  Filming took place in Iowa and was mostly uneventful until a nearby farm harvested their soy bean crop, which caused an enormous swarm of stinging insects to descend on the house and sting Hailey. 

==Reception==
Critical reception for Fertile Ground has been mixed and DVD Verdict commented that it was "a little slow, but the performances are good, theres a bit of blood, and I enjoyed the plot."  Fearnet panned the film and wrote "not only "meh, forgettable," but its also a tiresome thriller that will evaporate from your short-term memory in less time than in takes to watch. Aside from a few fine acting performances, and Gieraschs greatly improved skill at setting a creepy stage, theres virtually nothing here thats new, noteworthy, or indicative of a duo thats already made about a dozen films."  Variety (magazine)|Variety also criticized the film, which they felt was disappointing in comparison to Gierasch and Andersons 2008 film Autopsy (film)|Autopsy  IGN commented that the film "does have its moments" but that the script was an "uninspired, dull mess".  In contrast, Aint It Cool News gave a more favorable review, stating that although Fertile Ground was overly similar to previous films with similar plot lines, it was overall a "jarring ghost story with real life terrors of pregnancy layered in".  DVD Talk gave it a mixed review, noting that the movie had "a lot going for it, but that darned mushy middle prevents it from reaching the greatness that it almost achieved. This is not quite a haunted house classic, but still recommended." 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 