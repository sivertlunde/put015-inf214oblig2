The Lonely Man
{{Infobox film
| name           = The Lonely Man
| image          = The Lonely Man poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Henry Levin
| producer       = Pat Duggan 
| screenplay     = Harry Essex Robert Smith
| starring       = Jack Palance Anthony Perkins Neville Brand Robert Middleton Elisha Cook, Jr. Claude Akins Lee Van Cleef
| music          = Van Cleave
| cinematography = Lionel Lindon
| editing        = William B. Murphy
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film directed by Henry Levin and written by Harry Essex and Robert Smith. The film stars Jack Palance, Anthony Perkins, Neville Brand, Robert Middleton, Elisha Cook, Jr., Claude Akins and Lee Van Cleef. The film was released on November 10, 1957, by Paramount Pictures.   

==Plot==
 

== Cast ==		
*Jack Palance as Jacob Wade
*Anthony Perkins as Riley Wade
*Neville Brand as King Fisher
*Robert Middleton as Ben Ryerson
*Elisha Cook,  Jr. as Willie 
*Claude Akins as Blackburn
*Lee Van Cleef as Faro Harry Shannon as Dr. Fisher James Bell as Judge Hart Adam Williams as Lon
*Denver Pyle as Brad
*John Doucette as Sundown Whipple
*Paul Newlan as Fence Green 
*Elaine Aiken as Ada Marshall
*Tennessee Ernie Ford as Singer

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 