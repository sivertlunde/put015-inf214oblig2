A Snitch in Time
{{Infobox Film |
  | name           = A Snitch in Time|
  | image          = SnitchinTimeTITLE.jpg |
  | caption        =  |
  | director       = Edward Bernds |
  | writer        = Elwood Ullman |
  | starring       = Moe Howard Larry Fine Shemp Howard Jean Willes Henry Kulky John L. Cason John Merton Fred F. Sears|
  | art direction =  Charles Clague | 
  | editing        = Henry DeMond |
  | producer       = Hugh McCollum |
  | distributor    = Columbia Pictures |
  | released       =  
  | runtime        = 16 28" |
  | country        = United States
  | language       = English
}}

A Snitch in Time is the 128th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The trio own a furniture shop ("Ye Olde Furniture Shoppe: Antiques Made While U Waite") who are staining some furniture they have delivered to Miss Scudder (Jean Willes), an attractive curly-haired brunette who owns a boarding house. While attending to their duties (and nearly destroying the furniture in the process), several new boarders at Miss Scudders place are actually a trio of crooks who have just robbed a jewelry store. The Stooges are held at gunpoint while Miss Scudder is tied up and gagged in her kitchen while the crooks ransack the house to steal several valuable heirlooms in her possession. The Stooges and Miss Scudder work together and unravel the crooks plot.

 

==Production notes== 	
A Snitch in Time has been consistently ranked as the most violent Stooge film of the Shemp era.  Unlike the Curly Howard|Curly-era equivalent They Stooge to Conga, in which all three Stooges receive their fair share of abuse, most of the violence in A Snitch in Time is directed at Moe. In its opening four minutes, Moe gets his nose and buttocks jammed into the blade of a whirling circular saw, as well as getting glue in his eye and stuck on his hands. 

David J. Hogan, author of the 2011 book Three Stooges FAQ, commented that "kids of the day—before bicycle helmets, seat belts, and moratoriums on peanut butter—loved this kind of torment. Its still funny today, but you keep waiting for the spray of blood."  Hogan adds that a February 2001 post to the website www.threestooges.net commented that "Only Dawn of the Dead gives you more pain for your entertainment dollar".    

Interestingly, though Columbia short subject head/director Jules White was known for the usage of excessive violence in his films, A Snitch in Time was directed by Edward Bernds, who always maintained that the violence was not to be excessive in the films he directed.   

The title A Snitch in Time parodies the aphorism "a stitch in time saves nine." 

== References ==
 

== External links ==
*  
*  
* 

 

 
 
 
 
 
 
 
 