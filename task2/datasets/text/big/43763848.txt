Italy in a Day
{{Infobox film
| name           = Italy in a Day
| image          = ItalyInADayMoviePoster500px.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Gabriele Salvatores
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 75 minutes
| country        = Italy United Kingdom
| language       = Italian Chinese English
| budget         = 
| gross          = 
}}

Italy in a Day - Un giorno da italiani is a 2014 Italian-British documentary film directed by Gabriele Salvatores. It was part of the Out of Competition section at the 71st Venice International Film Festival.  
 crowdsourced documentary Britain in a Day as a prototype to produce Italy in a Day, which included clips selected from 45,000 video submissions recorded on 26 October 2013.   The film was said to reflect the fears of Italians during a recession, their uncertainty over their national identity, and their refusal to give up on dreams.   

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 

 
 