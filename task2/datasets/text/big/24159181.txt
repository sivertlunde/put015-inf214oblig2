End of the Line (1987 film)
{{Infobox film
| name           = End of the Line
| image          =
| image_size     =
| caption        =
| director       = Jay Russell
| producer       = Lewis M. Allen Peter Newman Mary Steenburgen
| writer         = Jay Russell John Wohlbruck
| narrator       =
| starring       = Wilford Brimley Kevin Bacon Levon Helm Holly Hunter
| music          = Andy Summers
| cinematography = George Tirl
| editing        = Mercedes Danevic
| distributor    = Orion Classics
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}

End of the Line is a 1987 drama film directed by Jay Russell.  

Leo Pickett and Will Haney, railroad  workers in Little Rock, Arkansas, find out the parent company of the Southland railroad is about to close their yard and layoff the employees, switching all future shipments to the air freight business.

In a last-ditch effort to save their jobs, the two men "borrow" a locomotive and drive it from Clifford, Arkansas, to Chicago, Illinois, to make their case to Thomas G. Clinton, the railroads Chairman of the Board.

==Production==
End of the Line was produced with the cooperation of the Missouri Pacific Railroad, which provide technical assistance to production crews as well as the contribution of multiple sets of rolling stock and locomotives. All Southland rolling stock and locomotives, including trackage rights for filming rights was provided by the Missouri Pacific, as well as limited assistance by the Union Pacific, which approved use of some rolling stock, trackage, and locomotives for completion of the film.

==Cast==
* Kevin Bacon as Everett
* Wilford Brimley as Will Haney
* Holly Hunter as Charlotte Haney
* Mary Steenburgen as Rose Pickett
* Levon Helm as Leo Pickett
* Michael Beach as Alvin
* Barbara Barrie as Jean Haney
* Bob Balaban as Warren Gerber

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 