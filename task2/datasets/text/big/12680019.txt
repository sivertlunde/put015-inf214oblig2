...All the Marbles
 
 
{{Infobox film
| name           = …All the Marbles
| image          = Allthemarbles.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Robert Aldrich
| producer       = William Aldrich
| writer         = Rich Eustis Mel Frohman
| narrator       = Tracy Reed
| music          = Frank De Vol
| cinematography = Joseph F. Biroc
| editing        = Richard Lane Irving Rosenblum
| distributor    = Metro-Goldwyn-Mayer/United Artists
| released       =  
| runtime        = 113 minutes
| country        = United States
| language       = English
| budget         = $9.3 million Alain Silver and James Ursini, Whatever Happened to Robert Aldrich?, Limelight, 1995 p 311 
| gross          = $6.5 million (North America gross)  89,679 admissions (France)   at Box Office Story 
}}

…All the Marbles (reissued as The California Dolls) is a 1981 comedy-drama film about the trials and travails of a female wrestling tag team and their manager. It was directed by Robert Aldrich (his final film) and stars Peter Falk, Vicki Frederick and Laurene Landon. The Pittsburgh Steeler hall of famer Joe Greene (American football)|"Mean" Joe Greene plays himself.

Among the young unknown actresses who auditioned, but did not receive a part, was Kathleen Turner. 

The wrestlers were trained by the former womens world wrestling champion Mildred Burke. 

According to Laurene Landon (who portrayed California Doll Molly), while the film did not perform well at the box office in the United States, it made a healthy profit in foreign markets, and producers were planning a sequel, to be set primarily in Japan, when Robert Aldrichs death put a halt to the project. 

The film is known outside the USA as The California Dolls, because "all the marbles" is an American idiom that makes little sense in most other countries.

==Plot==
Harry becomes manager of a tag team of gorgeous lady wrestlers. On the road, they endure a number of indignities, including bad motels, small-time crooks and a mud-wrestling match, while trying to reach Reno for a big event at the MGM Grand.

==Cast==
* Peter Falk as Harry
* Vicki Frederick as Iris
* Laurene Landon as Molly
* Richard Jaeckel as Dudley
* Burt Young as Eddie Cisco John Hancock as Big John Faith Minton as Big Mama

==Production==
Second unit photography began 5 November 1980. Principal photography took place 14 November to 24 February 1981 on location in Youngstown, Akron Ohio, Chicago, Las Vegas, Reno and Los Angeles. Nat Segaloff, Final Cuts: The Last Films of 50 Great Directors, Bear Manor Media 2013 p 20-21 

==Reaction==
In his October 16, 1981, review in The New York Times, the film critic Vincent Canby singled out Falk for "one of his best performances".

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 