Sju svarta be-hå
{{Infobox film
| name           = Sju svarta be-hå
| image          = 
| caption        = 
| director       = Gösta Bernhard
| producer       = 
| writer         = Gösta Bernhard Ilya Ilf
| starring       = Dirch Passer
| music          = 
| cinematography = Jan Lindeström
| editing        = Hans Gullander
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Sweden 
| language       = Swedish
| budget         = 
}}

Sju svarta be-hå is a 1954 Swedish crime film directed by Gösta Bernhard and starring Dirch Passer.

==Cast==
* Dirch Passer - Jens Nielsen
* Annalisa Ericson - Gertrud Hall, actress (as Anna-Lisa Ericsson)
* Åke Grönberg - Sture Kaxe
* Hjördis Petterson - Sofia Pang (as Hjördis Pettersson)
* Stig Järrel - Captain Jacob Grönkvist
* Katie Rolfsen - Tina Andersson
* Irene Söderblom - Defrauded Lady
* Siv Ericks - Margareta Beckman
* Rut Holm - Hilda Johansson
* Anna-Lisa Baude - Valborg Jeppman
* Gösta Bernhard - Drunk
* Curt Åström - Vesslan
* John Melin - Burglar
* Ulla-Carin Rydén - Miss Svensson Nils Olsson - Beggar
* Georg Adelly - Policeman

==External links==
* 
* 

 
 
 
 
 
 
 


 
 