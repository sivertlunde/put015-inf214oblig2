The Daring Caballero
{{Infobox film
| name           = The Daring Caballero
| image          = 
| alt            = 
| caption        = 
| director       = Wallace Fox
| producer       = Philip N. Krasne
| screenplay     = Betty Burbridge
| story          = Frances Kavanaugh 	
| starring       = Duncan Renaldo Leo Carrillo Kippee Valez Charles Halton Pedro de Cordoba Stephen Chase
| music          = Albert Glasser
| cinematography = Lester White 
| editing        = Martin G. Cohn 
| studio         = Inter-American Productions
| distributor    = United Artists
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film directed by Wallace Fox and written by Betty Burbridge. The film stars Duncan Renaldo, Leo Carrillo, Kippee Valez, Charles Halton, Pedro de Cordoba and Stephen Chase. The film was released on June 14, 1949, by United Artists.  

==Plot==
 

== Cast ==
*Duncan Renaldo as Cisco Kid
*Leo Carrillo as Pancho
*Kippee Valez as Kippee Valez
*Charles Halton as Ed J. Hodges
*Pedro de Cordoba as Padre Leonardo
*Stephen Chase as Major Bruce Brady
*David Leonard as Patrick Del Rio
*Edmund Cobb as Marshal J.B. Scott
*Frank Jaquet as Judge Perkins
*Mickey Little as Bobby Del Rio
*Wes Hudman as Deputy Post 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 