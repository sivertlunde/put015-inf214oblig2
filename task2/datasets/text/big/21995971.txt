Life of an Expert Swordsman
 
{{Infobox film
| name           = Life of an Expert Swordsman
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Hiroshi Inagaki
| producer       = Tomoyuki Tanaka
| writer         = Hiroshi Inagaki Edmond Rostand  (play) 
| screenplay     = 
| based on       =   
| starring       = Toshiro Mifune
| music          = Akira Ifukube
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}} Cyrano de Bergerac. The film was released in the English-speaking world with the title Samurai Saga.
 love interest, Princess Chiyo, who is analogous to Cyranos love interest, Roxane.

At the end of the film, when the mortally wounded Komaki visits Princess Chiyo at her convent to bring her the latest news of the outside world, he mentions the defeat of Kojirō Sasaki in a duel by the famed samurai Musashi Miyamoto. Prior to filming Life of an Expert Swordsman, Mifune had played Miyamoto in Samurai Trilogy, also directed by Inagaki, which chronicled Miyamotos life, culminating in his legendary duel with Sasaki.

==External links==
* 
* 

 
 

 
 
 
 
 
 
 


 