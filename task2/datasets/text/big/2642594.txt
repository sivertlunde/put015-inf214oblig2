The Exterminating Angel (film)
 
{{Infobox film
| name = The Exterminating Angel
| image = angelexterminadore.jpg
| caption = El ángel exterminador poster
| director = Luis Buñuel
| producer = Gustavo Alatriste
| writer = Luis Buñuel
| starring = Silvia Pinal Enrique Rambal
| distributor = Gustavo Alatriste
| language= Spanish
| released =  
| runtime = 93 minutes
| country = Mexico
| budget =
}}

The Exterminating Angel ( ), is the second Buñuel film of the Buñuel/Alatriste/Pinal film trilogy, written and directed by Luis Buñuel, starring Silvia Pinal, and produced by her then-husband Gustavo Alatriste.
 Mexican cinema and one of the best 1000 films by the New York Times. 

The Exterminating Angel was released on R4 DVD by Madman Entertainments Directors Suite in 2006.  The Criterion Collection released the film on DVD in 2009.

== Background == General Francisco Franco and asked to direct a movie of his choice. Buñuel wrote and directed Viridiana, which starred Silvia Pinal and was produced by her then husband, Gustavo Alatriste. It was the first film Buñuel made in his native country. Released in 1961, the film sparked controversy both in Spain and the Holy See|Vatican, and as a result all existing negatives were ordered to be destroyed. The film, however, won the Palme dOr at the 1961 Cannes Film Festival,    and copies of the film that had been shipped to Paris survived and were subsequently distributed. Viridiana would be released in Spain 16 years later, in 1977.

Following the Viridiana scandal, Buñuel returned to Mexico, but kept his production team and decided to make another movie starring Pinal. The film, originally called The Outcasts of Providence Street, was renamed The Exterminating Angel after Buñuel picked it from an unfinished play his friend José Bergamín was writing at the time. The film was released in Mexico in 1962, and was just as controversial as its predecessor had been.

Buñuel would complete a trilogy of sorts working with Pinal and Alatriste in a third film released in 1965 - Simon of the Desert.

== Plot ==
During a formal dinner party at the lavish mansion of Señor Edmundo Nobile and his wife, Lucia, the servants unaccountably leave their posts until only the major-domo is left. After dinner the guests adjourn to the music room, where one of the women, Blanca, plays a piano sonata. Later, when they might normally be expected to return home, the guests unaccountably remove their jackets, loosen their gowns, and settle down for the night on couches, chairs and the floor. 

 
By morning it is apparent that, for some inexplicable reason, they are psychologically, but not physically, trapped in the music room. Unable to leave, the guests consume what little water and food is left from the previous nights party. Days pass, and their plight intensifies; they become quarrelsome, hostile, and hysterical - only Dr. Carlos Conde, applying logic and reason, manages to keep his cool and guide the guests through the ordeal. One of the guests, the elderly Sergio Russell, dies, and his body is placed in a large cupboard. Much later in the film, Béatriz and Eduardo, a young couple about to be married, lock themselves in a closet and commit suicide.

In the meantime, the guests manage to break open a wall enough to break a water pipe. Eventually, several sheep and a bear break loose from their bonds and find their way to the room; the guests take in the sheep and proceed to slaughter and roast them on fires made from floorboards and broken furniture. Dr. Conde reveals to Nobile that one of his patients, Leonora, is dying from cancer and accepts a secret supply of morphine from the host to keep her fit. The supply of drugs is however stolen by Francis and Juana, a brother and sister. Ana, a Jew and a practitioner of Kabbalah, tries to free the guests by performing a mystical ceremony, which fails. 
 La Valkiria") sees that they are all in the same positions as when their plight began. Obeying her instructions, the group starts reconstructing their conversation and movements from the night of the party and discover that they are then free to leave the room. Outside the manor, the guests are greeted by the local police and the servants, who had left the house on the night of the party and who had similarly found themselves unable to enter it.

To give thanks for their salvation, the guests attend a Te Deum at the cathedral. When the service is over, the churchgoers along with the clergy are also trapped. It is not entirely clear though, whether those that were trapped in the house before are now trapped again. They seem to have disappeared. The situation in the church is followed by a riot on the streets and the military step in to brutally clamp down on the rioters. The last scene shows a pack of sheep entering the church in a row, accompanied by the sound of gunshots.

== Cast ==
* Enrique García Álvarez as Alberto Roc
* Jacqueline Andere as Alicia de Roc
* César del Campo as Col. Alvaro
* Nadia Haro Oliva as Ana Maynar
* Ofelia Montesco as Beatriz
* Patricia de Morelos as Blanca
* Augusto Benedico as Dr. Carlos Conde
* Luis Beristáin as Cristian Ugalde
* Enrique Rambal as Edmundo Nobile
* Xavier Massé as Eduardo
* Xavier Loyá as Francisco Avila
* Ofelia Guilmáin as Juana Avila
* Claudio Brook as Julio, the majordomo
* José Baviera as Leandro Gomez
* Bertha Moss as Leonora
* Silvia Pinal as Leticia "La Valkiria"
* Lucy Gallardo as Lucía de Nobile
* Tito Junco as Raúl
* Patricia Morán as Rita Ugale
* Antonio Bravo as Sergio Russell
* Rosa Elena Durgel as Silvia

== Cultural references ==
In the 2011 film Midnight in Paris the main character, Gil (Owen Wilson), travels back in time to 1920s Paris and suggests a story to a perplexed young Buñuel (Adrien de Van) about guests who arrive for a dinner party and can’t leave.  Buñuel asks, "But why can’t they leave? I don’t understand." After Gil leaves, Buñuel is still muttering to himself, "...Whats holding them in the room?..."

The alternative rock band The Creatures have a song called "Exterminating Angel".

A 1995 episode of the sitcom One Foot in the Grave is called "The Exterminating Angel", in reference to a scene in the episode in which a large number of characters are trapped in a conservatory (though unlike the film, they are physically locked in).

A 2002 episode of Buffy the Vampire Slayer titled "Older and Far Away" references the movie when a set of characters are unable to leave a house after a party. Initially the characters seems to be psychologically unable to leave, but later the characters desire to leave but are physically unable to due to a spell.

In October, 2014 Stephen Sondheim revealed that he and playwright David Ives were working on a new musical with a plot inspired by both The Exterminating Angel and The Discreet Charm of the Bourgeoisie.
   

== Awards == 1963 Bodil Awards, the film won the Bodil Award for Best Non-European Film.   

== See also ==
*Le charme discret de la bourgeoisie

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 