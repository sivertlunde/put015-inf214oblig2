The Trench (film)
 
 
{{Infobox Film |
name           = The Trench |
image          = The Trench (film) 1999.jpg |
image_size     = 250px |
caption        = | William Boyd |
producer       =  Steve Clark-Hall | William Boyd |
narrator       = | Paul Nicholls Daniel Craig Julian Rhind-Tutt Danny Dyer James DArcy| 
music          = |
cinematography = | Jim Clark|
distributor    = Arts Council of England |
released       = 17 September 1999  (UK)  |
runtime        = 98 minutes   |
country        = United Kingdom France  |
language       = English |
budget         = |
gross          = |
preceded_by    = |
followed_by    = |
}}
 1999 independent William Boyd Battle of Paul Nicholls, James DArcy plus Daniel Craig and Ben Whishaw and Charlie Crick (uncredited)

==Synopsis== Paul Nicholls), 17, along with his older brother, Eddie (Tam Williams), has volunteered for service. The whole platoon, all of them in their late teens, depend on the war-hardened Sergeant Winter (Daniel Craig) and the scholarly Lieutenant Hart (Julian Rhind-Tutt) for their survival. When word arrives that the platoon will join the first wave of attacks, they do not yet know they will be present when the British Army loses the greatest number of soldiers in a single day in its history.

==Cast== Paul Nicholls as Pte. Billy Macfarlane
*Daniel Craig as Sgt. Telford Winter
*Julian Rhind-Tutt as 2Lt. Ellis Harte
*Danny Dyer as LCpl. Victor Dell
*James DArcy as Pte. Colin Daventry
*Tam Williams as Pte. Eddie Macfarlane
*Anthony Strachan as Pte. Horace Beckwith
*Michael Moreland as Pte. George Hogg
*Adrian Lukis as Lt. Col. Villiers
*Ciarán McMenamin as Pte. Charlie Ambrose
*Cillian Murphy as Pte. Rag Rookwood
*John Higgins as Pte. Cornwallis
*Ben Whishaw as Pte. James Deamis
*Tim Murphy as Pte. Bone
*Danny Nutt as Pte. Dieter Zimmermann

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 


 