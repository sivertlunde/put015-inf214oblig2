The Golden Coach
{{Infobox film
| name           = The Golden Coach
| image          = Goldencoachposter.jpg
| caption        = Film poster for The Golden Coach
| director       = Jean Renoir
| producer       =
| writer         =
| starring       = Anna Magnani Odoardo Spadaro Duncan Lamont
| music          = Antonio Vivaldi
| cinematography =
| editing        =
| distributor    =
| released       = 3 December 1952 (Italy) 27 February 1953 (France)
| runtime        = 103 min
| country        = France Italy
| awards         = English
| budget         =
| preceded_by    =
| followed_by    =
}}
The Golden Coach ( ;  ) is a 1952 film directed by Jean Renoir that tells the story of a commedia dellarte troupe in 18th century Peru. The screenplay was written by Renoir, Jack Kirkland, Renzo Avanzo and Giulio Macchi and is based on the play Le Carrosse du Saint-Sacrement by Prosper Mérimée. It stars Anna Magnani, Odoardo Spadaro and Duncan Lamont.

==Plot==
The Viceroy of a remote 18th century Peruvian town has purchased a magnificent golden coach from Europe.  The Viceroy hints of his intention to give the coach to his mistress, the Marquess|Marquise, but has decided to pay for it with public funds, since he plans to use it to overawe the populace and flatter the local nobility, who enthusiastically look forward to taking turns parading in it.  By coincidence, the coach arrives on the same ship that carries an Italian commedia dellarte troupe composed of men, women and children who perform as singers, actors, acrobats and comics.  The troupe is led by Don Antonio, who also portrays the stock character of Pantalone on stage, and features Camilla, who plays the stock role of Columbina.

Once members of the troupe refurbish the town’s dilapidated theater, their performances meet with success only after local hero, Ramon, a Torero|Toreador, becomes smitten with Camilla and starts leading the applause.  Similarly, after a command performance at the Viceroy’s palace, the gentry withhold their favor until the Viceroy signals his approval and asks to meet the women of the company.  He, too, is taken with Camilla, who is the only person who makes him feel comfortable and light-hearted.  He gives her a splendid necklace, which enrages her jealous swain, Felipe, who has been accompanying the troupe on their travels.  Felipe attacks Camilla and causes a riotous backstage brawl, after which he runs off to join the army.

The Viceroy has become infatuated with Camilla and announces that he has decided to pay for the coach with his own money, in order to give it to her as a love gift.  This outrages the Marquise along with the rest of the nobility, who are already smarting over the Viceroy’s demands for money to finance military defenses against an insurgency.  Led by the Duc de Castro, they threaten to strip the Viceroy of his post, an action that can only succeed if endorsed by the country’s Bishop.  When the Viceroy vacillates in the face of this intimidation, Camilla spurns him in disgust.

After watching a triumphant performance by Ramon in the bullring, Camilla impetuously gives him her necklace, which emboldens him to visit her lodging that night and propose that they become a celebrity couple in order to enhance their earning power as performers.  There he encounters Felipe, who has returned from extended army service in order to reclaim Camilla and take her away with him to live a simple life among the natives.  While the two men swordfight each other, the Viceroy arrives to tell Camilla that he has defied the nobility and is giving her the coach, which she can claim from him immediately.  Upon questioning, he admits to her that he expects the Bishop, who arrives on the morrow, to approve the nobles’ plan to depose him.  Felipe and Ramon are arrested for dueling in public.

All is resolved the next morning when Camilla gives the coach to the Bishop as a gesture of piety.  The Bishop announces his plan to use the coach to transport the sacraments to sick and dying peasants and calls for peace and reconciliation between all the disputing parties.  As the curtain falls, Don Antonio reminds Camilla that, as an actress, she is only able to realize her true self when she is performing on the stage.

==Cast==

*Anna Magnani as Camilla
*Odoardo Spadaro as Don Antonio
*Nada Fiorelli as Isabella
*Dante (Harry August Jensen) as Arlequin
*Duncan Lamont as Ferdinand, The Viceroy
*George Higgins as Martinez
*Ralph Truman as Duc de Castro
*Gisella Mathews	 as Marquise Irene Altamirano
*Raf De La Torre	 as The Chief Justice
*Elena Altieri as Duchesse de Castro Paul Campbell as Felipe
*Riccardo Rioli as Ramon, the Toreador
*William Tubbs as Aubergiste, the Innkeeper
*Jean Debucourt as The Bishop

==Production==

The film was shot at Cinecittà in Rome, with cinematography by Claude Renoir and music by Antonio Vivaldi.  A French-Italian co-production (filmmaking)|co-production, it was filmed in English. A shoot in French was planned as well, but it had to be abandoned due to financial problems.  Renoir let his assistant director Marc Maurette direct the dubbing in French, which was the first to premier. An Italian version has also been made. Renoir reputedly preferred the English version,  regarded as the only original version, and it is the only one that has been restored in 2012. François Truffaut reportedly referred to The Golden Coach as "the noblest and most refined film ever made".   

==References==
 


==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 