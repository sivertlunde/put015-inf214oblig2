Vella kalpi
  

{{Infobox film
| name           = Vella kalpi
| image          = DVD_cover_of_the_film_"Vella_kalpi".jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Aleksandrs Leimanis
| producer       =  
| writer         =  
| starring       =  
| music          = Raimonds Pauls
| cinematography = Martins Kleins
| editing        = Elza Preisa Rigas Kinostudija Rigas Kinostudija
| released       = 1970
| runtime        = 90 minutes
| country        = Latvian SSR
| language       = Latvian
| budget         = 
| gross          = 
}}
 Rigas Kinostudija. It was written and directed by Aleksandrs Leimanis during the time Latvia was part of the Soviet Union.    It was released in the United Kingdom as Devils servants and the Soviet Union as Slugi dyavola. In 1972, Rīgas kinostudija released a sequel to this film named Vella kalpi Vella dzirnavās (English: Devils servants in the Devils mill).

==Cast==
* Lolita Cauka as Ruta
* Haralds Ritenbergs as Andris
* Eduards Pavuls as Ermanis
* Olga Drege as Anna
* Elza Radzina as Getrude
* Ingrid Andrina as Cecilija
* Baiba Indriksone as Lene
* Karlis Sebris as Samsons
* Edgars Zile as Salderns
* Evalds Valters as Mayor Eks
* Janis Grantins as Daniels Rebuss
* Janis Osis as Manteifels
* Haralds Topsis as Klavs Angers
* Valentins Skulme as General Svenson
* Zigrida Stungure as Elizabete

==References==
 

==External links==
*  


 

 
 