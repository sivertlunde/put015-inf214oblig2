Invitation to Hell
 
{{Infobox film
| name           = Invitation to Hell
| image          = 
| caption        =  Michael J. Murphy
| producer       = Caroline Aylward
| writer         = Carl Humphrey
| starring       = Becky Simpson Joseph Sheahan Colin Efford
| music          = 
| cinematography = 
| editing        = 
| distributor    = Scorpio Video (VHS) Sarcophilous Films (DVD)
| released       = 1982
| runtime        = 45 minutes
| country        = United Kingdom English
| budget         = £1,000 (estimated)
}}
 Michael J. Murphy about Satan possessing several people at a countryside English estate.

==Plot==
Laura invites her college friend Jacky out to her country estate for a costume party.  What Jacky does not know is that she is a pawn in a demonic game.  That evening she is drugged and taken out to what appears to be a black magic altar.  The next morning she awakens with claw-like scratches on her legs and, when she tries to escape, finds she is trapped in this location.  But why are her friends keeping her here and who do they truly serve?

==Cast==
*Becky Simpson as Jacky
*Joseph Sheahan as Ed
*Colin Efford as Maurice
*Steven Longhurst as Alan
*Russell Hall as Rick
*Catherine Rolands as Laura
*Tina Barnett as Tina

==Production== The Last Night.

==Release==
The film was released on VHS in the United Kingdom by Scorpio Video in 1983.  It was later released in the United States by Mogul Communications. 

After being released on a series of bootleg DVD releases, the film was officially released on DVD by Sarcophilous Films in 2008.   This special edition release features both films and includes "making of" extras for both films. 

==References==
 

==External links ==
* 

 
 
 
 
 
 

 