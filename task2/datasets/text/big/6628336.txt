Tail Sting
 
{{Infobox film
| name           = Tail Sting
| image          =
| caption        = 
| director       = Paul Wynne
| producer       = Vicky Pike Morris Ruskin Paul Wynne
| writer         = Timothy Griffin Peter Soby Jr. Tara Price Elizabeth Perry Todd Sherry
| music          = 
| cinematography = Angel Colmenares
| editing        = Paul Wynne
| distributor    = 
| released       = 
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
}} genetically enhanced giant scorpions wreaking havoc on board an airplane.

==Plot==
In Australia a hijack attempt is made on a van leaving a genetic engineering institute in New South Wales.  The hijackers are thwarted by Security Guard Fred (Joe Boony) who gets the cargo to an airport in Melbourne.

A cancelled flight at the airport forces an unlikely mix of characters to board a chartered flight to Los Angeles rather than wait for a plane the following day.  This includes a girls karate team, a businessman desperate not to miss a meeting in the US (Dave McCracken), and a mix of others including a group of scientists accompanied by Fred.

Shortly into the flight it is revealed that one of the scientists, Scott Milhouse (Ray Davis), planned the hijacking attempt. The van contained genetically modified scorpions intended to be used to develop new drugs to fight AIDS and cancer. The scorpions excessive size is blamed on the fact they were cloned and engineered using DNA from "loads of things", including prehistoric scorpions. Milhouse intends to sell specimens to an unknown third party.  As Fred foiled the hijack attempt, Milhouse decides to steal some of the specimens himself.  He attempts to move some of the specimens into a coffin in the planes cargo hold when Fred interrupts.  A fight ensues and Milhouse accidentally kills Fred.  Milhouse moves the specimens before balancing the coffin on the edge of a container and leaving the hold.

Soon thereafter the coffin tumbles from its perch and the scorpions are released.  The scorpions grow to immense proportions and kill several members of the crew and passengers.  The pilot, Captain Jack Russel, realises something is amiss when he finds his navigator dead in the cargo hold. His fears are confirmed when a scorpion drops into the cockpit and attempts to sting him.

Milhouses mental state deteriorates and he accidentally shoots the head scientist with Freds gun, which also causes one of the planes doors to open.  Several people and all but one of the scorpions are sucked out of the plane.  

Milhouse is stung by the queen scorpion and the others lose track of him. Captain Jack loses his eyes to the scorpion, prompting his love interest Dr. Ryan to set out on a mission to kill the queen while Courtney, a member of the karate team, acts as Captain Jacks eyes in the cockpit.

Ryan corners the queen at the planes minibar, but Milhouse reappears and points out that he has been stung so many times that he is immune to the scorpions venom.  Ryan pushes Milhouse into the grip of the queen, then zaps the scorpion moments before the plane lands.

Captain Jack and Ryan leave the plane together, having fallen in love.  As everyone departs the plane, a normal-sized scorpion runs across the runway.

==External links==
* 
* 

 
 
 
 
 
 