With Byrd at the South Pole
{{Infobox film | name = With Byrd at the South Pole
| image = With byrd at the south pole 1930 poster.jpg
| caption = Theatrical release poster
| director = 
| producer = Jesse L. Lasky Adolph Zukor
| writer = Julian Johnson  (titles)
| starring = Richard E. Byrd
| music = Manny Baer
| cinematography = Joseph T. Rucker Willard Van der Veer
| editing = Emanuel Cohen
| distributor = Paramount Pictures
| released = June 19, 1930
| runtime = 82 minutes
| language = English country = United States
| budget = 
}} Little America-Exploration Best Cinematography. 

This was the first documentary to win any Oscar and the only one to win cinematography.

==Cast==
*Richard E. Byrd...Himself (Expedition Commander) (as Rear Admiral Richard E. Byrd) Claire Alexander...Supply Officer Pilot
*George H. Black...Seaman and Tractor Man  Topographer 
*Kennard F. Bubier...Aviation Mechanic
*Christopher Braathen...Seaman, Ski Man 
*Jacob Bursey...Seaman, Dog Driver  Fireman 
*Francis D. Coman...Medical Officer (as Dr. Francis D. Coman) 
*Frederick E. Crockett...Dog Driver
*Victor H. Czegka...Machinist
*Frank T. Davies...Physicist
*Joe de Ganahl...Mate
*E.J. Demas...Aviation Mechanic
*James A. Feury...Fireman
*Edward E. Goodale...Dog Driver
*Charles F. Gould...Carpenter
*Lawrence M. Gould...Geologist and Geographer/2nd Comm. (as Dr. Lawrence M. Gould)
*William C. Haines...Meteorologist
*Malcolm P. Hanson...Radio Operator 
*Henry R. Harrison Jr....Aerologist 
*Harold June...Aviation Pilot 
*Charles E. Lofgren...  Personnel Officer 
*Howard F. Mason...Radio Operator 
*Ashley C. McKinley...Aerial Photographer (as Captain Ashley C. McKinley) 
*Thomas B. Mulroy...Chief Engineer  Surveyor
*Russell Owen...Newspaper Correspondent 
*Alton U. Parker...Aviation Pilot (as Captain Alton U. Parker) 
*Carl O. Petersen...Radio Operator 
*Martin Ronne...Sailmaker 
*Benjamin Roth...Aviation Machanic 
*Paul A. Siple...Boy Scout  Dean Smith...Aviation Pilot 
*Sverre Strom...Second Officer  Cook
*George A. Thorne Jr....Seaman, Ski Man, Surveyor 
*Norman D. Vaughan...Dog Driver
*Arthur Treadwell Walden...In Charge of Dogs
*Floyd Gibbons...Narrator

==DVD info==

This was released on DVD in February, 2000. It is part of a milestone collection that is very limited.    

==See also==
*The Lost Zeppelin (1929)

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 