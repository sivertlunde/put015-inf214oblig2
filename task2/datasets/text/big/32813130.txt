Three Faces East (1926 film)
{{infobox film
| name           = Three Faces East
| image          =
| imagesize      =
| caption        =
| director       = Rupert Julian
| producer       = Cecil B. DeMille
| writer         = Anthony Paul Kelly (play) C. Gardner Sullivan (scenario) Monte Katterjohn (scenario)
| starring       = Jetta Goudal Robert Ames Clive Brook
| music          = Hugo Riesenfeld
| cinematography = J. Peverell Marley
| editing        = Claude Berkeley
| distributor    = Producers Distributing Corporation
| released       = February 3, 1926 (premiere)
| runtime        = Seven reels (7,419 feet)
| country        = United States
| language       = Silent film English intertitles
}}
 silent film refilmed in sound in 1930.   

This film is listed as surviving in Bois dArcy in France. 

==Cast==
*Jetta Goudal - Miss Hawtree/Fraulein Marks
*Robert Ames - Frank Bennett
*Henry B. Walthall - George Bennett
*Clive Brook - Valdar
*Edythe Chapman - Mrs. Bennett
*Clarence Burton - John Ames
*Ed Brady - Firking

unbilled
*Rupert Julian - The Kaiser

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 