Near Death Experience (film)
{{Infobox film
| name           = Near Death Experience
| image          = Near_Death_Experience_(film).jpg
| caption        =
| director       = Benoît Delépine Gustave Kervern
| producer       = Benoît Delépine   Gustave Kervern
| writer         = Benoît Delépine   Gustave Kervern
| starring       = Michel Houellebecq 
| music          = Guillaume Lebraz
| cinematography = Hugues Poulain  
| editing        = Stéphane Elmadjian 	
| studio         = No Money Productions
| distributor    = Ad Vitam Distribution
| released       =  
| runtime        = 87 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

Near Death Experience is a 2014 French drama film directed, produced and written by Benoît Delépine and Gustave Kervern. It was selected to be screened at the Horizons section of the 71st Venice International Film Festival. 

== Cast ==
* Michel Houellebecq as Paul
* Marius Bertram as Le Vagabond 
* Benoît Delépine as Le collègue Orange 1 
* Gustave Kervern as Le collègue Orange 2 
* Manon Chancé as Lautomobiliste

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 
 