Gnomeo & Juliet
{{Infobox film
| image            = Gnomeo & Juliet Poster.jpg
| caption          = Theatrical release poster
| director         = Kelly Asbury
| producer         = Baker Bloodworth David Furnish Steve Hamilton Shaw Mark Burton Andy Riley Kevin Cecil Emily Cook Kathy Greenberg Steve Hamilton Shaw
| story            = Kelly Asbury John R. Smith Rob Sprackling Andy Riley Kevin Cecil Steve Hamilton Shaw
| based on         =  
| starring         = James McAvoy Emily Blunt Michael Caine Maggie Smith Jason Statham Jim Cummings Patrick Stewart Ashley Jensen Stephen Merchant Matt Lucas Ozzy Osbourne
| music            = Elton John Chris P. Bacon James Newton Howard
| editing          = Catherine Apple
| studio           = Touchstone Pictures Miramax Films Rocket Pictures Arc Productions Walt Disney Studios Motion Pictures
| released         =  
| runtime          = 84 minutes 
| country          = United Kingdom United States Canada
| language         = English
| budget           = $36 million 
| gross            = $194 million   
}}
Gnomeo & Juliet is a 2011 British-American-Canadian  .

==Plot== Richard Wilson) are two elderly neighbours who despise each other. When they leave their respective gardens, their garden gnomes come alive. The Montague garden is filled with blue-hat gnomes, and the Capulet garden has red-hat gnomes. Like their human gardeners, the gnomes also despise each other.

The gnomes hold a back alley lawnmower race, with Gnomeo (James McAvoy) driving for the blues and Tybalt (Jason Statham) for the reds. Tybalt cheats to win the race, destroying Gnomeos lawnmower. Gnomeo and his best friend, Benny (Matt Lucas), are disappointed to see Mrs. Montague ordering a new "kitty" lawnmower.

That night, Gnomeo and Benny infiltrate the red garden in black disguise.  Benny sprays Tybalts well and accidentally triggers a security light. During the escape Gnomeo ends up in a nearby garden where he bumps into a disguised Juliet (Emily Blunt), the daughter of the red gnomes leader Lord Redbrick (Michael Caine). Juliet is attempting to retrieve a unique orchid, and the two romantically fight over it. They each discover the others color before fleeing the garden. When they both go back to their own gardens, Juliet tells her frog-sprinkler friend Nanette (Ashley Jensen) about her newfound love. Nanette states that the relationship is romantically tragic.

Gnomeo and Juliet have secret meetings in the nearby garden, where they meet a pink plastic flamingo named Featherstone (Jim Cummings) who encourages their love.

Gnomeos mother and the blue gnomes leader Lady Bluebury (Maggie Smith), is distraught after the reds infiltrate the garden and destroy the plant nurtured by Gnomeos deceased father. The blues want Gnomeo to take revenge on the reds, and he realizes that he cannot refuse unless he tells his secret. Just as he is about to spray the prized tulips of the reds, Juliet sees him and he backs out of the attack.

When he and Juliet meet up again, they argue until Featherstone stops them, telling them he lost his wife when the two people living in the house, where the garden is, broke up and never saw each other again.  Benny sees them and runs into the alleyway, where Tybalt is waiting with his lawnmower, attempting to run Benny down and chops off his hat. Gnomeo intervenes, and he and Tybalt fight on the red lawnmower until the lawnmower runs into the wall. Gnomeo jumps off last minute, but Tybalt crashes into the wall, destroying himself. The reds attempt to attack Gnomeo, thinking that Tybalt died because of him, but Juliet, to the surprise of her clan, defends Gnomeo, saying that she loves him. Gnomeo ends up on a road, and everyone believes he was run over by a truck. Lord Redbrick glues the heartbroken Juliet to her fountain because he does not want to lose her as he lost her mother.

Gnomeo is still alive, eventually reaching a park where he climbs onto a statue of William Shakespeare (Patrick Stewart) and tells him his story. Shakespeare tells Gnomeo that his story is very similar to Romeo and Juliet and that it is likely Gnomeos will have a sad ending as well.

Benny gets onto Mrs. Montagues computer and changes her lawnmower order to a powerful Terrafirminator unit, intending to get revenge on the reds. However, the Terrafirminator goes out of control and destroys most of the two gardens while the gnomes wage a full scale war. Gnomeo makes it back to Juliet with the help of Featherstone. However, when he arrives, the Terrafirminator then frees itself, sending it flying.  He tries to try to un-glue Juliet, but he is unable to. She tells him to go, but he refuses.  The two share a passionate kiss just as the lawnmower crashes into the fountain, self-destructing in the process. When everyone believes that both are dead, Lord Redbrick and Lady Bluebury decide to end the feud. Miraculously, Gnomeo and Juliet emerge from the ruins and the two clans celebrate.

The film ends happily with the red and blue gnomes finally coming together to celebrate their newfound peace. Tybalt is revealed to still be alive having been glued back together, Featherstone is reunited with his wife after Benny finds and orders her online, Gnomeo and Juliet are married on a purple lawnmower, which symbolizes the new union of both gnome clans.

==Cast== Romeo Montague. Juliet Capulet. Lord Capulet.
* Jason Statham as Tybalt; a red gnome counterpart to Tybalt. Lady Montague.
* Patrick Stewart as the statue of William Shakespeare.
* Ashley Jensen as Nanette, a plastic garden frog and Juliets best friend; counterpart to Nurse (Romeo and Juliet)|Nurse.
* Matt Lucas as Benny, Gnomeos impulsive and tall-hatted best friend; counterpart to Benvolio
* Stephen Merchant as Paris, a nerdy red gnome who was arranged to marry Juliet; counterpart to Count Paris.
* Ozzy Osbourne as Fawn, a garden deer and Tybalts best friend; counterpart to Characters in Romeo and Juliet#Peter|Peter.
* Jim Cummings as Featherstone, a lonely plastic flamingo with a thick Spanish accent; counterpart to Friar Laurence.
* Hulk Hogan as Terrafirminator Announcer.
* Julie Walters as Ms. Montague, the elderly owner of the Blue garden. Richard Wilson as Mr. Capulet, the owner of the Red garden.
* Kelly Asbury as Red Goon Gnomes; counterparts to Characters in Romeo and Juliet#Gregory and Sampson|Gregory, Sampson, Characters in Romeo and Juliet#Anthony, Potpan, unnamed servants|Anthony, and Potpan.
* Shroom, a silent mushroom and Gnomeo’s friend/pet; counterpart to Romeos servant Characters in Romeo and Juliet#Balthasar|Balthasar. Abram and other miscellaneous servants of the Montagues.
* Dolly Parton as Dolly Gnome, the lawnmower race announcer.
* Julia Braams as Stone Fish, a stone fish that attached to the Fishing Gnome. In the end, it is seen being dragged by Gnomeo and Juliets purple lawnmower.
* James Daniel Wilson as Fishing Gnome, a red gnome.
* Tim Bentinck as Conjoined Gnome Left, a red gnome.
* Julio Bonet as Mankini Gnome, a red gnome.
* Neil McCaul as Conjoined Gnome Right, a red gnome.
* Maurissa Horwitz as Call Me Doll, Bennys newfound girlfriend.
* John Todd as Dancer (uncredited).

There is no counterpart for Characters in Romeo and Juliet#Mercutio|Mercutio, as stated by James McAvoy in an interview with NBC, ”Gnomeo in this is a little bit of amalgamation between Romeo and Mercutio. We don’t have that Mercutio character in this. We don’t have that leader of the pack, which Romeo isn’t but Gnomeo is a little bit." 

==Production== Mark Burton, Kevin Cecil, Emily Cook, Kathy Greenberg, Andy Riley, and Steve Hamilton Shaw worked on the final screenplay. Prior to the casting of James McAvoy and Emily Blunt, the roles of Gnomeo and Juliet were to be voiced by Ewan McGregor and Kate Winslet, respectively. 
 G rating from the Motion Picture Association of America|MPAA, despite some mild language being used in some bits of the film. The films worldwide premiere was at Disneys El Capitan Theatre in Hollywood on January 23, 2011.

The film was distributed in the United Kingdom and Canada by E1 Entertainment, and the film was released in 3D. Elton John and director Asbury presented 10 minutes of the film at the Cannes Film Festival.   
 Starz Animation Toronto (which also made 9 (2009 animated film)|9) produced all of the animation for the film, including the stereoscopic version.
 California Suite, in the roles of Lady Blueberry and Lord Redbrick, respectively.

==Soundtrack==
 
The soundtrack was released on February 8, 2011, three days before the films initial release. It features music by Elton John, Nelly Furtado, Kiki Dee, and selections from the score composed by Chris P. Bacon and James Newton Howard. The soundtrack was supposed to feature a much anticipated duet between John and Lady Gaga titled "Hello, Hello"; however, the version of the song featuring Lady Gaga only appears in the film, and on the soundtrack, the song features only John. 

==Reception==

===Critical reaction===
Gnomeo & Juliet received generally mixed reviews; review aggregate website Rotten Tomatoes reported that 55% of professional critics gave positive reviews based on 117 reviews with an average rating of 5.6/10.    Its consensus states "While it has moments of inspiration, Gnomeo & Juliet is often too Self-reference|self-referential for its own good".  Another review aggregator, Metacritic, gave the film a 53% rating based on 28 reviews on its review scale. 

===Box-office===
Gnomeo and Juliet earned $99,967,670 in the North America and an $94,000,000 in other countries for a worldwide total of $193,967,670.  Gnomeo & Juliet was ultimately a sleeper hit for Disney, outperforming the much higher-budgeted (and eventual box office bomb)  Mars Needs Moms the studio released a month following Gnomeo & Juliet. On its first weekend, the film had a worldwide opening of $30,680,933, finishing in second place behind Just Go with It ($35.8 million).   However, on its second weekend —Presidents Day weekend— it topped the worldwide box office (although not being in first place either in North America  or overseas ) with $29,832,466, ahead of Unknown (2011 film)|Unknown which ranked second ($26.4 million).

It opened in 2,994 theaters in North America on Friday, February 11, 2011, grossing $6.2 million on its first day and ranking third behind   and Just Go with It. It then finished the weekend with $25.4 million in third place.  However, it scored the largest opening weekend ever for an animated feature released during the winter period (both January and February). It also made the largest debut on record for a minor animated movie (i.e., one with little status, expectations and/or built-in audience), according to Box Office Mojo.  With a $99.97 million total it stands as the highest-grossing animated feature among those released in winter. 

In the United Kingdom, Ireland, and Malta, it topped the weekend box office by earning £2,945,627 ($4,716,248) on its opening. In total it has grossed $25,283,924, making the UK the only market, except North America, where it grossed more than $10 million. 

==Accolades==
{| class="wikitable"
|-
!Award !! Category !! Winner/Nominee !! Result
|- Annie Awards Directing in a Feature Production Kelly Asbury
| 
|- Storyboarding in a Feature Production Nelson Yokota
| 
|- Voice Acting in a Feature Production Jim Cummings
| 
|- Writing in a Feature Production Andy Riley, Kevin Cecil, Mark Burton, Kathy Greenburg, Emily Cook, Rob Sprackling, John R. Smith, Kelly Asbury, Steve Hamilton Shaw
| 
|- Broadcast Film Critics Association Awards Best Song Hello Hello, performed by Elton John and Lady Gaga/written by Elton John and Bernie Taupin
| 
|- Golden Globe Awards Golden Globe Best Original Song
|"Hello Hello"
| 
|- Satellite Awards Original Song
|"Hello Hello"
| 
|}

==Home media== deleted and alternate scenes, as well as a feature with Ozzy Osbourne called "The Fawn of Darkness".   

==Sequel== John Stevenson, the director of Kung Fu Panda, has been set to direct the sequel. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 