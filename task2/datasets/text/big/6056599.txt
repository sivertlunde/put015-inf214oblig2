The Dallas Connection
{{Infobox Film
| name           = The Dallas Connection
| image          = Poster of the movie The Dallas Connection.jpg
| image_size     = 
| caption        = 
| director       = Christian Drew Sidaris Arlene Sidaris Wess Rahn Brian Bennos
| writer         = Christian Drew Sidaris
| starring       = Bruce Penhall Mark Barriere Julie Strain
| music          = Ron Di Lulio Mark Morris
| editing        = Craig Stewart
| distributor    = Starlight
| released       =   October 10, 1994 (video premiere)
| runtime        = 94 min.
| country        = United States|U.S.A. English
| boob count (individual) = 26
| budget         = 
| followed_by    = 
}}
The Dallas Connection is a 1994 action adventure film starring Bruce Penhall, Mark Barriere and Julie Strain. It was written and directed by Christian Drew Sidaris and is part of the "Triple B" film series produced by Andy Sidaris.

==The Triple B Series by Andy Sidaris==

*1. Malibu Express (1985)
*2. Hard Ticket to Hawaii (1987)
*3. Picasso Trigger (1988)
*4. Savage Beach (1989) Guns (1990) Do or Die (1991)
*7. Hard Hunted (1992)
*8. Fit to Kill (1993)
*9. Enemy Gold (1993)
*10. The Dallas Connection (1994)
*11. Day of the Warrior (1996)
*12.   (1998)

==See also==
*Girls with guns

==External links==
* 
*  at Allmovie
* 

 
 

 
 
 
 
 
 
 


 