Kostas (film)
{{Infobox film
| name           = Kostas
| image          = 
| image_size     =
| alt            =
| caption        = 
| director       = Paul Cox
| producer       = Kostas Kallergis
| writer         = Linda Aronson
| based on = original concept by Paul Cox
| narrator       =
| starring       = Takis Emmanuel Wendy Hughes
| music          = 
| cinematography = 
| editing        = John Scott
| studio         = Victorian Film Corporation Illumination Films
| distributor    = Greg Lynch Film Distributors
| released       = 1 June 1979 (premiere) 16 August 1979 (release)
| runtime        = 104 mins (original) 93 mins (release)
| country        = Australia
| language       = English
| budget         = AU$224,000 David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p198 
| gross          = 
| preceded_by    =
| followed_by    =
}}
Kotas is a 1979 film directed by Paul Cox about a Greek taxi driver. 

==Production==
The film was shot over four weeks in March 1979. $100,000 of the budget came from the Victorian Film Corporation. 

Post production on the film was rushed so Cox could ready it in time for the Melbourne Film Festival which he now says was a mistake. However it was the best received of all Coxs features to date. 

==References==
 

==External links==
*  at IMDB
*  at Oz Movies
 

 
 
 


 