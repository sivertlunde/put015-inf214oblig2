The Journals of Knud Rasmussen
{{Infobox film
| name           = The Journals of Knud Rasmussen
| image          = The Journals of Knud rasmussen.jpg
| caption        = DVD cover Norman Cohn Norman Cohn Zacharias Kunuk Elise Lund Larsen Stephane Rituit Vibeke Vogel
| writer         = Eugene Ipkarnak Madeline Ivalu Herve Paniaq Pauloosie Qulitalik Lucy Tulugarjuk Abraham Ulayuruluk Louis Uttak
| starring       = Natar Ungalaaq Pierre Lebeau Leah Angutimarik Jens Jørn Spottag Neeve Irngaut
| music          = Richard Lavoie
| cinematography = Norman Cohn
| editing        = Cathrine Ambus Norman Cohn Félix Lajeunesse SF Film
| released       = March 11, 2006 (Igloolik screening) September 7, 2006 (Canada) October 8, 2006 (USA) November 10, 2006 (Denmark)
| runtime        = 112 minutes
| country        = Canada Denmark
| language       = Inuktitut Danish English
| budget         =  $6,300,000 (estimated)
| gross          = 
}}
The Journals of Knud Rasmussen is a 2006 Canadian-Danish film about the pressures on the traditional Inuit culture when explorer Knud Rasmussen introduces European cultural influences. Produced by Isuma, the film was directed by Zacharias Kunuk, who also directed the award-winning Inuit film Atanarjuat, and Norman Cohn. It premiered on September 7, 2006 at the Toronto International Film Festival, after pre-release screenings in Inuit communities in Canada and Greenland. 

The cast includes Peter-Henry Arnatsiaq, Pakkak Innushuk, Natar Ungalaaq, Neeve Uttak, Pierre Lebeau, Kim Bodnia and Jens Jørn Spottag.

==Synopsis==
Set primarily in and around Igloolik in 1922, the film depicts the relationship between a group of Inuit in Arctic Canada and three Danish ethnographers and explorers, Knud Rasmussen, Therkel Mathiassen and Peter Freuchen during Rasmussens "Great Sled Journey" of 1922. The film is shot from the perspective of the Inuit, showing their traditional beliefs and lifestyle. It tells the story of the last great Inuit shaman and his beautiful and headstrong daughter; the shaman must decide whether to accept the Christian religion that is converting the Inuit across Greenland.

==Cast==
*Natar Ungalaaq as Nuqallaq

==External links==
*  (1927), Knud Rasmussens book which the film is based from.
*  provides an excellent background prior to viewing.
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 