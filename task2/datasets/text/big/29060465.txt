Then I Sentenced Them All to Death
{{Infobox film
| name           = Then I Sentenced Them All to Death
| image          =
| director       = Sergiu Nicolaescu
| producer       =
| writer         = Titus Popovici
| starring       = Amza Pellea Ion Besoiu Cristian Şofron Ioana Bulcă Octavian Cotescu Iurie Darie Gheorghe Dinică Ernest Maftei Ştefan Mihăilescu-Brăila Sergiu Nicolaescu
| music          = Tiberiu Olah
| cinematography = Alexandru David
| runtime        = 103 minutes
| country        = Romania
| language       = Romanian
}}

Then I Sentenced Them All to Death ( ) is a 1972 Romanian historical and social film about World War II. It was directed by Sergiu Nicolaescu.    It was released on 24 August 1972 in Romania.

==Plot==

The story follows a young boy, Elev, who is adopted by his uncle, Ioan, and his wife after Elevs father, a partisan during WWII, is killed by soldiers. Ioan is the local village priest and though gentle and intelligent, is frustrated by his wards apparent inability to love him. Elev, who equally ignores girls his own age, would far rather spend time with Todor, nicknamed Ipu, the so-called village idiot. Together they fish and reenact the French Revolution in the ruins of a neighboring village that was decimated for a minor infraction against the German occupying force.

One day Elev watches a German cavalry officer conducting riding practice amid hay bales. Some time later the horse is seen galloping past without a rider and Elev finds the officers corpse near the road. He reports the murder, but not before surreptitiously stealing the mans pistol. At the funeral, the local German commandant coldly informs the village that if the killer doesnt step forward by the next morning, the communitys leaders (including Ioan and his wife, the mayor, the town doctor and the notary) will be executed.

That night Ioan hosts a sumptuous feast. Todor, who is usually the target of nothing but derision and ridicule, is not only invited for the first time, but is made the guest of honor. The doctor gives him an impromptu, suspiciously cursory physical and informs him that his poor lifestyle is catching up with him. It soon becomes clear what is going on: the community leaders want Todor to lie and confess to the murder so that they can be saved. 

After some soul searching Todor gives in to their emotional supplications, but gradually realizes that for a brief window of time he holds a great degree of power and importance. He asks Ioan to perform a mock martyrs funeral so he can see what it will be like and, at the eleventh hour, demands money and land for the elderly, sick and crippled members of his family. 

But at dawn the Germans make a surprise retreat from the village and all are saved. Todor falls to his knees in tears, though it is unclear whether he is relieved to escape death or bereft over his lost opportunity for heroism. Ioan and the other local intellectuals immediately revert to their previous superior attitudes. Elev, observing their hypocrisy, mentally sentences them all to death. He draws out the officers pistol and a single shot is heard, though the film makes it ambiguous who, if anyone, is the victim.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 