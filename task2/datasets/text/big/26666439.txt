Vellathooval
{{Infobox film
| name           = Vellathooval
| image          = Vellathooval.jpg
| image_size     = 
| caption        = 
| director       = I. V. Sasi
| producer       = C. M. Raji
| writer         = John Paul
| narrator       = 
| starring       = Rejith Menon  Nithya Menon Lalu Alex Johnson
| cinematography =
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Vellathooval is a 2009 Malayalam movie by I. V. Sasi and also last film done by him, starring Rejith Menon, Nithya Menon and Lalu Alex. It is about a crazy girl and her jorney with a poor friend.

== Plot ==
Jiya, daughter of James and Lisa and grand daughter of Mrs. Koshi, who enjoyed her teenage like a white feather (Vellathooval). She is very conservative and have her own views. Manu gets fed up with his life. After his fathers death, his mother marries Vijayaraghavan (actor)|Vijayaraghavan. He is always drunk and tortures Manu and his mom. So Manu fights with him and leaves home. He thinks he could become a murderer if he stays at home. Meantime Jiya also leaves her home. She goes with Manu. Manu advices jiya to go back to home many times, but she didnt agree with him. Simultaneously Jiya and Manu were becoming very good friends but the society misunderstood them and considered them lovers. Her uncle the  City Police Commissioner Abi (K B Ganesh kumar) and his teams search every corners for them. At last Jiya injures a young college mate Willy in the stomach. He dies in the hospital. Willys friends and police follow Jiya and Manu.

== Cast ==
* Rejith Menon as Manu
* Nithya Menon as Jiya
* Lalu Alex
* Jagathy Sreekumar
* Revathi Seema
* Shweta Menon
* K. B. Ganesh Kumar
* Vijayaraghvan
* Sithara as Jiyas Aunt
* Manju Satheesh as teacher
* Sreelatha Namboothiri as Jiyas grandmother
* Ambika Mohan as Highrange Janu
* Deepika Mohan

== External links ==
*  
* http://www.indiaglitz.com/channels/malayalam/preview/10840.html
* http://www.nowrunning.com/movie/6231/malayalam/vellathooval/index.htm
* http://movies.rediff.com/review/2009/may/25/review-vellathooval.htm
* http://sify.com/movies/malayalam/review.php?id=14889870&ctid=5&cid=2428

 
 
 


 