The Lunchbox
 
 
 

{{Infobox film
| name           = The Lunchbox
| image          = The Lunchbox poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Ritesh Batra
| producer       = Arun Rangachari Anurag Kashyap Guneet Monga
| writer         = Ritesh Batra
| starring       = Irrfan Khan Nimrat Kaur Nawazuddin Siddiqui
| music          = Max Richter
| cinematography = Michael Simmonds
| editing        = John F. Lyons NFDC
| distributor    = Walt Disney Studios Motion Pictures   Sony Pictures Classics   
| released       =  
| runtime        = 105 minutes   
| country        = India France    Germany 
| language       = Hindi English
| budget         =   
| gross          =   
}}
 NFDC (India), International Critics Week at the 2013 Cannes Film Festival, and later won the Critics Week Viewers Choice Award also known as Grand Rail dOr.  It was shown at the 2013 Toronto International Film Festival.    The film was released in India on 20 September 2013.  The film was a box-office success. 

The Lunchbox was nominated for the Film Not in the English Language category of the British Academy Film Awards 2015. 

==Plot==
Saajan is a lonely accountant about to retire from his job. Ila is a young wife seeking her husbands attention, looking for ways to put romance back in her marriage, and tries to cook her way to her husbands heart. Her aunt who lives in the apartment above her gives her advice on cooking and marriage through shouted instructions.  Through a rare mixup of the famous "dabbawalas" (a complicated system that picks up and delivers lunches from restaurants or homes to people at work) of Mumbai, the lunchbox with the delicious food Ila prepared for her husband instead gets delivered to widower Saajan. When the lunch containers are delivered back to Ila empty, she is happy, thinking her husband had enjoyed her cooking.  When she questioned him, he did not seem to think much of it.  Realizing the mistake of the delivery, she writes a note to the recipient of her lunch thanking him for enjoying the food and sends him her husbands favorite meal the following day.

Saajan replies saying that the food was salty. Upon advice from her neighbour, who believes the lunch went to Ilas husband, she responds by using hot chili in a lunch that turns out to be so spicy that Saajan resorts to eating bananas to help cool off his mouth. An exchange of the messages sent back and forth with the lunches ignites a friendship between Saajan and Ila who share memories and thoughts in notes passed back and forth in the lunch pails. Meanwhile Saajan deals with a young employee, Shaikh, he is supposed to train to replace him when he retires. Shaikh appears immature, annoying, lazy and irresponsible. On her end, Ila feels her husband is very remote. He not only complains about her food (which is delivered to him from the restaurant Saajan has ordered his lunches from), but she finds out he is having an affair. At the same time, she supports her mother as her father suffers from lung cancer.

As Saajan and Shaikh get to know each other, Shaikh reveals that he is an orphan and has worked in several places, including Saudi Arabia. He is currently living with his girlfriend. As he rides the train home each day after work, he chops vegetables for their dinner on his portfolio. As Saajan trains Shaikh, he has told him what to do and left him on his own, but Shaikh had bluffed his way into the job and many mistakes come to the attention of the boss who calls both of them into his office, puzzled that, after decades of service with never a mistake, so much had gone wrong.  He chastises Saajan but tells Shaikh to leave.  Saajan takes the blame for all that has gone wrong and saves Shaikhs job. This act moves Saajan and Shaikh to have a true friendship prompting Shaikh to invite Saajan to dinner at his home with his girlfriend and asking him to be his best man in his wedding, since he has no one else to stand up for him and no family.

In their correspondence through the lunch containers, Ila mentions that Bhutan is a poor but happy and beautiful place where the cost of living is much less than India. Saajan contemplates moving there with her upon his retirement and writes back asking if she will be ready to move to Bhutan with him. Ila writes back saying they should meet and suggests a popular food joint. She is there at the appointed time but Saajan doesnt turn up. A dejected Ila sends an empty lunchbox the next day. Saajan writes to her saying that he did arrive, but did not meet her because he saw how young and beautiful she is and considered himself too old for her and advises her to move on.

Ilas father dies and as his body is taken away, Ilas mother confesses to her that she has been unhappy for most of her marriage. This helps Ila to feel released.  She decides to search for Saajan by asking the address where the dabbawalas sent her food and goes to Saajans office to meet him, but there she learns that he has retired and headed to Nashik. Ila writes a last message saying farewell to Saajan announcing that she will leave her husband and go to Bhutan.

The movie comes to an end with Saajan returning from Nashik and going in search of Ila with the Dabbawalas song being played in the background.

==Cast==
* Irrfan Khan as Saajan Fernandes
* Nimrat Kaur as Ila
* Nawazuddin Siddiqui as Shaikh
* Denzil Smith as Mr. Shroff
* Bharati Achrekar as Mrs. Deshpande
* Nakul Vaid as Rajiv, Ilas husband
* Yashvi Puneet Nagar
* Lillete Dubey as Ilas mother

==Production==

=== Development === The Talented The Namesake (2007), and Germanys Match Factory became its international sales agent.  

=== Writing ===
Batra completed the first draft of the screenplay in 2011.  He was assisted by Rutvik Oza.  It went on to win an Honorable Jury Mention at the 2012 Cinemart at the Rotterdam International Film Festival. Thereafter the project was part of the Talent Project Market of Berlin International Film Festival and was mentored at the screenwriters lab (Torino Film Lab) at the Torino Film Festival.    The character of Ila played by Nimrat Kaur, six months prior to the shooting, and the character played by Nawazuddin Siddiqui was further developed and improvised during shooting. 

=== Casting ===
Irrfan Khan liked the script of the film and the concept of his character, not speaking much but talking through notes. After seeing Batras short film and a couple of meetings he agreed to act in the film. Batra wanted to work with Nawazuddin Siddiqui, another principal character of the film, for a long time. For the female lead, auditions were conducted, wherein Nimrat Kaur was selected. Kaur had extensive experience at the Mumbai theatre and worked in films like Peddlers (film)|Peddlers.   Some of the dabbawalas who the director befriended while researching for the film, also have been cast in minor roles. 

=== Filming ===
The film was shot in 2012 in Mumbai  at a budget of ₹100&nbsp;million. Prior to the filming, the cast rehearsed for six months. It was shot using the Arri Alexa digital film camera.  Many of the scenes were logistically broken down to make way for last minute location changes. Mumbais famous dabbawalas were provided actual lunchboxes to deliver, were followed by a four member film crew, which filmed the process in documentary style. 

==Release and reception==

===Screenings and film festivals=== International Critics Week at the 2013 Cannes Film Festival, where it received a standing ovation and positive reviews.    and won the Critics Week Viewers Choice Award also known as Grand Rail dOr.  Variety (magazine)|Variety called it "a notable debut from tyro helmer-scripter", for creating a film with "crossover appeal of Monsoon Wedding", and also praised acting of Irrfan Khan and Nimrat Kaur.   

Thereafter, Sony Pictures Classics picked up all North American rights for distribution.   

In India, this film was released in more than 400 screens on 20 September 2013.    

===Box office===
The Lunchbox grossed ₹71&nbsp;million in its first weekend of release in India,   and ₹110&nbsp;million in its first week.  The film continued to gross significant amounts over the next few weeks, earning over ₹200 million in the first three weeks and another estimated ₹40–50 lakhs on its fourth weekend.    It became 2014s highest grossing foreign film in the United States until the release of PK (film)|P.K.  As of 12 June 2014, the films U.S. collection stands at $4.06 million.    As of 28 May 2014, the films worldwide collection stands at  .   

===Critical reception===
The review aggregator Rotten Tomatoes gave the film a score of 96% based on reviews from 92 critics, with an average rating of 7.7/10. The sites consensus is: "Warm, affectionate, and sweet but not cloying, The Lunchbox is a clever crowd-pleaser from first-time director Ritesh Batra".  
 The Telegraph gave two thumbs up to The Lunchbox calling it "as much a moving and muted love story as it is an evocative portrayal of loneliness."  Taran Adarsh of Bollywood Hungama gave the movie a 4/5 stating, "A well-told old-fashioned romance, The Lunchbox gracefully unknots the trials, tribulations, fears and hopes of everyday people sans the glamour that the city of Mumbai has become synonymous with."  Karan Anshuman of the MumbaiMirror also went with a perfect score of 5/5 saying the film was, "one of the best films to come out of India in a long time." 

Raja Sen of Rediff.com praised the film further, giving another perfect score of 5/5 and offered particular compliments to the director Ritesh Batra, stating "Batra, who has also written The Lunchbox, has allowed his smashing actors tremendous room to improvise, all the while himself sketching in nuanced details about the city, its food-ferriers, and the many disparities Mumbai is crammed with."  Filmmaker/critic Khalid Mohammed of the Deccan Chronicle said "What stays in the mind at the end of The Lunchbox is pretty much what stays in mind at the end of a memorable set by jazzmen&nbsp;– not their lapses but the heights they scale."  Aditya Grover of YouthTimes gave it 4/5 stars and said, "The Lunchbox is delicious and delightful! If you’re in the mood to witness genuinely moving cinema, you’re in for a treat. The delectable taste of this lunchbox remains in your mouth much after you’ve left the theatre. Go for it!" 
Suparna Sharma of The Asian Age gave it 4 out of 5 stars and said: "The Lunchbox is a gently pulsating sweet-sad story of loneliness and love, of wilting spirits finding water again. There are three women in three marriages in this film, of which two are ailing. The third one is over, almost, only the last rites haven’t been performed. There are two men in the film – one who has lived a full life and is getting ready to quietly slip off the face of the earth; the other is eager to begin… What’s both shocking and soothing is what the film shows us — that it takes very little for a soul to come back to life. Mostly, just a hint of hope will do."  

Trisha Gupta in the Sunday Guardian wrote "The Lunchbox is a lovely little film. But it does tick all the boxes that might appeal to festival audiences: quaint Asian urbanism (Mumbai trains, dabba delivery), Indian home-cooking, romance. It provides local colour, without being demandingly untranslatable." 

===Oscar selection controversy=== Best Foreign Film Category, with many critics unanimously praising it and voting for it to be the representative film.  Celebrity director Karan Johar also put his support behind the film saying "All kinds of audience can connect with it and yet within the parameters of love story it is completely unusual. You feel all the love in the world for the protagonists and the unusual aspect of it is they havent met." 
 Gujarati film The Good Road instead.  This decision sparked outrage from many supporters of The Lunchbox, including its cast and crew. The films producer Anurag Kashyap quickly took to Twitter and expressed his disgust, saying "I dont know who the Federation is, but it goes to show the complete lack of understanding to make films that can travel across borders."  He later deleted both his Twitter and Facebook accounts, saying, "this is a moment of defeat for me, and for independent cinema, because, for once, our chances were great."   Karan Johar also said he felt very disappointed that such a wonderful chance at Oscar glory with The Lunchbox was spoiled.  Guneet Monga, The Lunchbox s other producer, said she was flabbergasted as to how the Federation could select a movie that didnt even have an American distributor, and also listed the number of global festivals and appreciation her film received, concluding that it sadly and supposedly "wasnt enough for the FFI".   

In an interview with Siddharth Sivakumar of Tinpahar, Goutam Ghose, the chairman of the committee revealed, "Personally I liked The Lunch Box very much. But eventually the eighteen member jury supported The Good Road. Now I can say that some people from Bombay felt that the basic premise of The Lunch Box was wrong. Because the Dubbawalas never do such mistakes. Films are after all works of fiction, with the right to cinematic liberty! Although Lunch Box was my personal favourite, but as a chairman one should not impose his or her choice on others. And as you know this became suddenly a big controversy. And I think the media was again to some extent responsible for this decision. Because every day during the deliberation or the screenings, the media projected Lunch Box as the chosen one. Its my assumption, that the members probably thought, "My God! If the media has already taken the decision then why we are here?" It was a Chomskian manufacturing consent- Lunch Box, Lunch Box, Lunch Box every day!! So the members, who are all very important people from the industry, had an opposite impulse. I dont know, but maybe thats the way it happened." 

===Accolades===
 
{| class="wikitable plainrowheaders"
|- style="text-align:center;"
! scope="col"  style="background:#ccc; width:40%;"| Award / Film Festival
! scope="col"  style="background:#ccc; width:30%;"| Category
! scope="col"  style="background:#ccc; width:25%;"| Recipient(s)
! scope="col"  style="background:#ccc; width:15%;"| Result
|- style="border-top:2px solid gray;" Star Guild Awards   Star Guild Best Film Anurag Kashyap, Arun Rangachari and Guneet Monga
| 
|- Star Guild Best Director Ritesh Batra
| 
|- Star Guild Best Actor Irrfan Khan
| 
|- Star Guild Best Actress Nimrat Kaur
| 
|- Star Guild Best Actor in a Supporting Role Nawazuddin Siddiqui 
| 
|- Best Screenplay Ritesh Batra
| 
|- Best Story Ritesh Batra
| 
|- Yash Chopra Award for the Most Promising Debut – Director Ritesh Batra
| 
|- Star Guild Best Female Debut Nimrat Kaur
| 
|- Star Verdict Performer of the Year Irrfan Khan
| 
|- Asia Pacific Screen Awards  Asia Pacific Best Screenplay Ritesh Batra
| 
|- ASPA Jury Jury Grand Prize Ritesh Batra
| 
|-
! scope="row" rowspan=7|Asia-Pacific Film Festival   Best Film Ritesh Batra
| 
|- Best Director Ritesh Batra
| 
|- Best Actor Irrfan Khan
| 
|- Best Actress Nimrat Kaur
| 
|- Best Supporting Actor Nawazuddin Siddiqui
| 
|- Best Screenplay Ritesh Batra
| 
|- Outstanding Achievement Award Irrfan Khan
| 
|- 8th Asian Film Awards  Best Film
| The Lunchbox
|  
|- Best Actor
| Irrfan Khan
|  
|-
| Best Screenwriter
| Ritesh Batra
|  
|- Dubai International Film Festival  Best Film – Feature Anurag Kashyap, Arun Rangachari and Guneet Monga 
| 
|- Special Mention – Feature Ritesh Batra
| 
|- Best Actor – Feature Irrfan Khan
| 
|- Filmfare Awards   Filmfare Critics Best Film (Critics) Ritesh Batra
| 
|- Filmfare Award Best Debut Director Ritesh Batra
| 
|- Filmfare Award Best Supporting Actor Nawazuddin Siddiqui
| 
|- Filmfare Award Best Story Ritesh Batra
| 
|- Filmfare Award Best Editing John F. Lyons
| 
|- Filmfare Award Best Sound Design Michael Kaczmarek
| 
|- Flanders International Ghent International Film Festival  Canvas Audience Award Ritesh Batra
| 
|- International Critics Week    Grand Rail dOr (Viewers Choice Award)
|The Lunchbox
| 
|- 15th IIFA International Indian Film Academy Awards  IIFA Award Best Actress in a Leading Role Nimrat Kaur
| 
|- IIFA Award Best Actor in a Supporting Role Nawazuddin Siddiqui 
| 
|- IIFA Award Best Story Ritesh Batra
| 
|- London Film Festival  Best Film Ritesh Batra
| 
|- Films from Oslo Films from the South Festival  Best Feature Ritesh Batra
| 
|-
! scope="row" |Reykjavík International Film Festival  Church of Iceland Award Ritesh Batra
| 
|- Screen Awards|Screen Weekly Awards   Best Film Anurag Kashyap, Arun Rangachari and Guneet Monga 
| 
|- Most Promising Debut Director Ritesh Batra
| 
|- Best Actor Irrfan Khan
| 
|- Best Actress Nimrat Kaur
| 
|- Best Supporting Actor Nawazuddin Siddiqui 
| 
|- Best Screenplay Ritesh Batra
| 
|- Best Story Ritesh Batra
| 
|- Zee Cine Awards   Best Debuting Director Ritesh Batra
| 
|- Best Actor in a Supporting Role Nawazuddin Siddiqui 
| 
|- Best Story Ritesh Batra
| 
|- British Academy Film Awards Best Foreign Language Film Ritesh Batra
|  
|}

==See also==
*Bollywood films of 2013

== References ==
 

==External links==
*  
*  
*  

 
  

 
 
 
 
 
 
 
 
 
 