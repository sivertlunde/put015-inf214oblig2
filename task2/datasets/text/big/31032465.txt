BumRush
{{Infobox film
| name           = BumRush
| image          = BumRush.jpg
| caption        = original movie poster
| director       = Michel Jetté
| screenplay     = Michel Jette Bad News Brown
| producer       = Louise Sabourin   Michel Jetté
| music          = Charles Papasoff
| cinematography = Georges Archambault
| editing        = Geoffroy Lauzon   Louise Sabourin
| distributor    = Forban Films
| released       =  
| runtime        = 
| country        = Canada
| language       = French English
| budget         = 
| gross          = 
}} Canadian film French and Bad News Brown in the role of gang leader Loosecanon. Musician and actor Bad News Brown was murdered soon after the film was shot. The film features some of his musical works. BumRush premiered on Canadian theaters on April 1, 2011. 

==Plot==
BumRush was inspired by real events that happened in Montreal.  Bumrush refers to "storming into an establishment." After Montreal police conduct a series of raids against a criminal biker gang and the Italian Mafia, rival street gangs rush in to fill the void. The Kingdom, a bar, sees a new bloody chapter in the Quebec underworld, as the "IB 11" gang conducts a series of attacks on the bar that force the owner to hire five extraordinary men under the leadership of "LKid" to confront the gangs.

==Cast==
*Emmanuel Auger - LKid
*Dara Lowe - Catherine
*Pat Lemaire - Papy
*Jézabel Drolet - Lou, Papys sister Bad News Brown - Loosecanon, a senior in "IB 11" gang
*Robert Pace - Le Boss, head of "IB 11"
*Sylvain Beauchamp - Tank, LKids guard
*Constant Gagné - Shrink
*Alain Nadro - Momo
*Paul Dion - Victor, police investigator
*Jacques Kartier - Bam Bam, senior in "IB 11"
*Giuseppe Giancaspro - "Brain" Martino, a senior in the Italian clan
*Carmine Napolitano - Tiny, "Brain"s bodyguard
*Dj Cook - Mongol, member of NMB gang
*Jerry Guerrier - 8 Ball
*Louis-David Morasse - Buzz, senior in the bikers gang
*Sheilla Azmitia - Angy, Loosecanons girlfriend
*Vandal Vyxen - Loli, 8 Balls gilrfiend
*Frost - Q
*Tchak - senior in "IB 11"

==References==
*  (in French) 
 

==External links==
*  
*  

 
 
 
 
 
 