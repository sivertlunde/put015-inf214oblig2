Big Bad Wolves
 
{{Infobox film
| name           = Big Bad Wolves
| image          = Big Bad Wolves US Theatrical Poster.jpg
| alt            = 
| caption        = US release poster
| director       = {{Plainlist|
* Aharon Keshales
* Navot Papushado}}
| producer       = {{Plainlist|
* Tami Leon
* Hillick Michaeli
* Avraham Pirchi
* Moshe Edery
* Leon Edery}}
| writer         = {{Plainlist|
* Aharon Keshales
* Navot Papushado}}
| starring       = {{Plainlist|
* Lior Ashkenazi
* Tzahi Grad
* Dovale Glickman
* Rotem Keinan}} Haim Frank Ilfman
| cinematography = Giora Bejach
| editing        = Asaf Corman
| production companies = {{Plainlist|
* United Channel Movies
* United King Studios}}
| distributor    = Magnet Releasing
| released       =  
| runtime        = 110 minutes  
| country        = Israel
| language       = Hebrew
| budget         = 
| gross          = $291,239 
}} thriller film written and directed by Aharon Keshales and Navot Papushado. It was the official selection of Tribeca Film Festival.  

==Plot==
Somewhere in Israel, three children play hide-and-seek in the woods. One of the girls hides in the closet in an abandoned house, from where she is abducted by an unknown perpetrator. Dror, a school teacher, is suspected of the crime and is arrested by the police. He is subjected to torture by a police team led by Micki to reveal the location of the missing girl. This whole episode is shot on his phone by a kid who happens to be playing in the vicinity and is subsequently uploaded onto YouTube.

An unnamed caller leads the police to the location of the girls body in a field. She has been sexually assaulted and her head is missing. According to Jewish law, a Jew is to be buried as he was born - complete with all his limbs and organs. Micki is subsequently fired from the police force, but he plans to kidnap Dror to extract a confession and thus clear his name. The case is handed over to another police officer, Rami. The girls father, Gidi, himself a retired military man, also suspects Dror of his involvement and plans to kidnap him. 

Dror is first kidnapped by Micki but later both of them are kidnapped by Gidi and taken to an abandoned house in the middle of an area surrounded by Arab villages. Here, both Micki and Gidi take turns torturing Dror, until he convinces Micki that he might after all be innocent. Gidi, a hardened veteran, doesnt fall for the trick and manages to disarm and shackle Micki in his basement. Upon hearing the fabricated story that he might be ill, Gidis father comes to visit him with some hot soup and accidentally encounters the two captives in the basement. Gidis father, an Army veteran, decides to help his son in tracing his granddaughters missing head. 

Micki—who has in his possession a rusty nail—asks Dror to lie about the location of the girls head to buy them some time to escape. Gidi then leaves the house to locate his daughters missing head and Micki also escapes and goes out to look for help. He makes a phone call to the police department, where he learns that his daughter is missing. It then strikes him that Dror referred to his daughter in a conversation while he had no way of knowing that Micki had a daughter. Gidi searches the described location for his daughters head but upon not finding anything comes back and starts cutting off Drors head with a hacksaw. Micki also reaches the house and tries to ask Dror for the location of his daughter but Dror dies before he can reply.

In the final scene, police officer Rami, who is investigating the case, is shown inspecting Drors house for any clues. He does not find anything and leaves the house. Mickis daughter is shown unconscious behind a fake wall.

==Cast==
* Lior Ashkenazi as Micki
* Tzahi Grad as Gidi
* Dovale Glickman as Yoram, Gidis father
* Rotem Keinan as Dror, a school teacher
* Guy Adler as Eli, a cop
* Dvir Benedek as Tsvika, the police commissioner
* Gur Bentwich as Shauli, a cop
* Nati Kluger as Eti, a real estate agent 
* Kais Nashif as a Man on horse
* Menashe Noy as Rami, Mikis boss
* Rivka Michaeli as Yorams wife

==Release==
The film was released in Israel on 15 August 2013 and opened in other theatres at the dates given below.

{| border=1 align=left cellpadding=4 cellspacing=0 width=300 style="margin: 0 0 1em 1em; background: #ccccff; border: 1px #8888cc solid; border-collapse: collapse; font-size: 95%;"
|- style=background:#ccccff
! Region
! Release date
! Festival or Distributor
|- United States|U.S.A 21 April 2013 Tribeca Film Festival
|- United States|U.S.A 4 May 2013 Stanley Film Festival
|- Canada
| 26 July 2013 Fantasia International Film Festival
|- Israel
| 15 August 2013
| style="background: #f7f8ff; white-space: nowrap;" |
|- UK
| 26 August 2013 London FrightFest Film4 Frightfest
|- Germany
| 27 August 2013 Fantasy Filmfest
|- United States|USA 11 October 2013 Chicago International Film Festival 
|- Spain
| 20 October 2013 Sitges Film Festival 
|-
|}
 

===Critical reception===
The film has received mostly positive reviews,  holding an 78% approval rating on Rotten Tomatoes.

Quentin Tarantino called it the best film of 2013. 

===Accolades===
* Tribeca Film Festival: Official Selection
* Stanley Film Festival: Official Selection
*  

==Soundtrack==
The music for Big Bad Wolves was written by Israeli-born composer Frank Ilfman, who had previously worked together with the directors on Rabies. The music was recorded at Air Lyndhurst Studios on 23 December 2012 with the London Metropolitan Orchestra conducted by orchestrator Matthew Slater. The score has been released digitally and on CD by MovieScore Media and Kronos Records.

{{Track listing
| all_music = Frank Ilfman
| headline = Big Bad Wolves: Original Soundtrack
| total_length = 53:56
| title1 =  Big Bad Wolves: Main Theme
| length1 = 4:15
| title2 = Hide and Seek: Opening Titles
| length2 = 4:10
| title3 = The March
| length3 = 2:00
| title4 = Scream for Me
| length4 = 3:16
| title5 = The Chair of Horror
| length5 = 2:41
| title6 = The Phone Call
| length6 = 1:46
| title7 = The Chase
| length7 = 3:15
| title8 = Help Me
| length8 = 2:38
| title9 = Saved by the Bell
| length9 = 2:45
| title10 = A Story About a Little Girl
| length10 = 4:16
| title11 = Hammer and Bones
| length11 = 1:55
| title12 = Man Rides a Horse
| length12 = 1:35
| title13 = The Truth Will Set You Free
| length13 = 2:42
| title14 = The Green House
| length14 = 3:53
| title15 = Now Talk
| length15 = 2:00
| title16 = Bike vs Car
| length16 = 2:37
| title17 = The Last Breath
| length17 = 3:48
| title18 = The Missing Girl and Epilogue
| length18 = 4:48
}}

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 