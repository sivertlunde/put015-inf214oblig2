A Good Time for a Dime
 
 
{{Infobox Hollywood cartoon cartoon name=A Good Time for a Dime
|series=Donald Duck
|image=A_good_time_for_a_dime_poster.jpg image size=
|alt=
|caption=Film Poster Dick Lundy
|producer=Walt Disney story artist= voice actor=Elvia Allman Pinto Colvig Walt Disney Clarence Nash
|musician=Bert Lewis
|animator=J. C. Melendez Ted Bonnicksen Bob Carlson Walt Clinton John Elliotte Jack Gayek Art Fitzpatrick layout artist= background artist= Walt Disney Productions
|distributor=RKO Radio Pictures release date=  color process=Technicolor
|runtime=7 minutes 37 seconds
|country=United States
|language=English preceded by=Golden Eggs followed by=Early to Bed
}} Daisy perform penny arcade nickelodeon peep show. Donald also struggles with a crane drop machine and a miniature airplane ride.

==Plot==
Inside a penny arcade, Donald inserts a coin to play a Mutoscope entitled "Dance of the Seven Veils". The pictures show dances of a Daisy Duck looking dancer until Donalds viewing is rudely interrupted by a black out. Next Donald tries out a crane machine to win a camera, but he finishes empty handed. Then Donald goes on a coin-operated airplane ride, but his ride is very short. When he tries to get another ride for free, the plane goes out of control, Donald nearly getting caught in the airplane propellers and becoming airsick. With that, Donald leaves the arcade.

==External links==
* 
 
 
 
 
 
 
 
 