The Other Woman (2014 film)
 
{{Infobox film
| name           = The Other Woman
| image          = The Other Woman (2014 film) poster.jpg
| caption        = Theatrical release poster
| director       = Nick Cassavetes
| producer       = Julie Yorn
| writer         = Melissa Stack
| starring       = {{Plainlist|
* Cameron Diaz
* Leslie Mann
* Kate Upton
* Nikolaj Coster-Waldau
* Nicki Minaj
* Taylor Kinney
* Don Johnson
*Olivia Culpo
}}
| music          = Aaron Zigman Robert Fraisse
| editing        = {{Plainlist|
* Jim Flynn
* Alan Heim
}}
| studio         = {{Plainlist|
* LBI Productions
* TSG Entertainment
}}
| distributor    = 20th Century Fox 
| released       =  
| runtime        = 109 minutes  
| country        = United States
| language       = English
| budget         = $40 million 
| gross          = $196.8 million   
}}

The Other Woman is a 2014 American romantic comedy film directed by Nick Cassavetes and written by Melissa Stack. The film stars Cameron Diaz, Leslie Mann, Kate Upton and Nikolaj Coster-Waldau, with Nicki Minaj, Taylor Kinney and Don Johnson in supporting roles. The film follows three women Carly (Diaz), Kate (Mann), and Amber (Upton) who are all romantically involved with the same man, Mark (Coster-Waldau). After finding out about each other, the trio decide to take revenge on Mark.

Development of The Other Woman began in January 2012, when 20th Century Fox hired Stack to write the script, based on the original idea from 1996 comedy The First Wives Club. Casting began in November 2012 and ended in June 2013. Filming began on April 29, 2013 in New York City, using locations like Manhattan, Long Island, The Hamptons, and The Bahamas. Aaron Zigman composed the score and LBI Productions produced the film.

The film was released on April 25, 2014 in the United States, distributed worldwide by 20th Century Fox. Despite negative reviews, the film has been a box office success, becoming number one at the box office during its opening weekend and grossed over $196 million worldwide against its budget of $40 million. It was released on DVD and Blu-ray Disc|Blu-ray on July 29, 2014, and earned more than $13 million in home media sales.

==Plot==
Carly, an attorney, has just started a relationship with Mark, a man she had sex with eight weeks prior. She is upset when Mark tells her he has to go out of town, but decides to go over to his house to seduce him. She is horrified to meet Kate, Marks wife. While they are initially hostile, the two women befriend one another. Kate discovers that Mark is seeing another woman, whom she initially believes to be Carly. However, she and Carly discover that Mark is seeing a third woman: Amber.

Carly and Kate travel to the beach, where Kate has a run-in with Amber and the two women inform her that Mark has been cheating on all of them. They decide to take revenge by doing things such as subsequently spiking his drinks with estrogen and laxative to give him breasts and to cause him diarrhea, as well as putting hair removal cream in his shampoo. Through their pranks, they discover that Mark has been embezzling from various companies at his workplace. Meanwhile, Carly begins to connect romantically with Kates brother Phil. In addition, Amber confides to Carly that she is seeing someone else as well. However, their camaraderie begins to shatter when Kate finds herself falling in love with Mark again after an investors dinner. Carly exposes Marks fraud, upsetting Kate.

Later, Mark goes to the Bahamas on a supposed business trip, Kate decides to follow him there and blow his cover. She finds Carly and Amber at the airport, who explain what Mark has been up to: he has been using Kate as the owner of the companies he defrauded, which would cause her to go to prison if discovered. She finds out that Mark has been seeing yet another woman whom he meets on this trip. This, and the possibility of facing prison in Marks place, urges Kate to take actions with help from Carlys legal expertise.

When Mark returns from vacation, he visits Carly at her office. He is locked in the conference room by Carlys assistant and friend, Lydia, and is shocked to find all three women together. They confront him with his infidelities and embezzlement. With Carly as her attorney, Kate presents divorce papers and a list of their assets. Kate reveals that she has returned the embezzled money to the companies, which saves the divorcing couple from prison time but leaves Mark bankrupt, much to his shock and hysterical outrage. Additionally, Marks business partner Nick arrives and fires Mark upon the discovery of the crime, making him more outraged (and having himself injured in various ways). Mark then finds his car being towed away and earns a punch in the face from Carlys father, Frank. Nick offers Kate the chance to take over Marks job in appreciation of her honesty.

Later, Carly and Phil fall in love, and the couple are expecting a child, Amber marries Frank, and Kate works as a CEO with Marks former business partners and the company making a profit under her leadership.

==Cast==
 
* Cameron Diaz as Carly Whitten, an attorney who finds out that her boyfriend Mark is already married and has another girlfriend.   
* Leslie Mann as Kate King, a housewife who discovers that her husband Mark is cheating on her with two women. 
* Kate Upton as Amber, an Amazon swimsuit supermodel, Marks second girlfriend. 
* Nikolaj Coster-Waldau as Mark King, a wealthy businessman, who is cheating on his wife Kate and two girlfriends Carly and Amber, at the same time. 
* Nicki Minaj as Lydia, Carlys assistant. 
* Taylor Kinney as Phil King, Kates brother
* Don Johnson as Frank Whitten, Carlys father who dates women half his age.  David Thornton as Nick
* Olivia Culpo as Raven-Haired Beauty

==Production==

===Development=== Black Listed screenwriter Melissa Stack was hired by 20th Century Fox to write an untitled female revenge comedy, which Julie Yorn would produce through LBI Productions.    The films script was described as the original idea from the 1996 film The First Wives Club, but with younger leads.  On November 13, the films title was revealed to be The Other Woman.  On January 31, 2013, Nick Cassavetes signed on to direct the film. 

===Casting===
On November 13, 2012, TheWrap reported that Cameron Diaz was in talks for the lead role.    Diazs representative also revealed that actress Kristen Wiig was under consideration for the wife role.  On March 13, 2012, Leslie Mann and Nikolaj Coster-Waldau were in talks to join the film, while Diaz was confirmed for her role.    On April 15, Kate Upton joined the film.  The same day, Taylor Kinney was announced to be in talks for a role.    On April 25, Nicki Minaj signed on to make her feature film debut.    On June 5, Don Johnson also joined the film to play Diazs characters father.   

===Filming=== Westhampton Beach.  In the late-June, some scenes were also shot in Chinatown, Manhattan|Chinatown.  From July 18-23, filming took place in New Providence, where Nassau, Bahamas|Nassau, The Bahamas was used as the filming location.  Atlantis Paradise Island was also used as the shooting location.  Isola Trattoria and Crudo Bar  at Mondrian Hotel in SoHo, Manhattan  was where the women met for a celebration toast at the end of the film.

===Music===
The Other Womans music was composed by Aaron Zigman, who was reportedly set to score the film on May 31, 2013.     The full album track list, featuring songs from different artists including Etta James, Ester Dean, Morcheeba, Cyndi Lauper, Britt Nicole, Patty Griffin, Lorde, Keyshia Cole and Iggy Azalea. 

==Release==
On March 31, the film had a premiere in Amsterdam, and the next day on April 1, it was premiered at Curzon Mayfair Cinema in London.  The film later had a premiere on December 21 in Westwood, California|Westwood, California. 

On March 25, 2014, Fox appealed the Motion Picture Association of America film rating system|R-rating, which Motion Picture Association of America gave the film for sexual references.    But studio wanted to get PG-13, because according to Box Office Mojo, the R-rated movies ever released, all could gross averaged just $7.8 million.  So on April 9, the MPAAs rating appeals board took back the R and awarded the film with a PG-13. And the sources confirmed that there were no changes made to get the film PG-13.  The Other Woman was released on April 24 2014 in the Netherlands and on April 25 in the United States.

===Box office===
The film was a box office hit earning over five times its production costs.
The Other Woman opened at number 1 in North America on April 25, 2014 in 3,205 theaters debuting atop the weekend box office with earnings of $24.7 million across the three days.  The film has grossed $83,911,193 in America and $112,870,000 in other countries for a worldwide total of $196,781,193. 

===Home media=== Gag Reel, Giggle Fit, Gallery and deleted scenes.  In the United States, the film has grossed $9,592,336 from DVD sales and $4,163,463 from Blu-ray sales, making a total of $13,755,799. 

==Reception==

===Critical response===
The Other Woman received negative reviews. On film aggregation website  , reported an average score of 39/100 (indicating "generally unfavorable reviews"), based on reviews from 35 critics. 

  Michael Phillips of Chicago Tribune gave the film two and a half stars out of four, saying "Line to line, its fresher than any number of guy-centric "Hangover"-spawned affairs, despite director Cassavetes lack of flair for slapstick."  The Boston Globes Ty Burr gave the film one out of four stars and said, "Its "The First Wives Club" rewritten for younger, less demanding audiences, or a "9 to 5" with absolutely nothing at stake."  Stephanie Zacharek of The Village Voice said, "The Other Woman doesnt give these actresses much to do except look ridiculous, if not sneaky and conniving." 

 
Michael Sragow of Orange County Register gave the film grade C, saying that film is "a coarse, rickety comedy."  Richard Corliss wrote For the magazine Time (magazine)|Time, "All three women are less watchable and amusing than Nicki Minaj as Carlys legal assistant Lydia."  Film critic Stephen Holden of The New York Times said that the film is "so dumb, lazy, clumsily assembled and unoriginal, it could crush any actor forced to execute its leaden slapstick gags and mouth its crude, humorless dialogue."  James Berardinelli of ReelViews asked, "Has it come to this for director Nick Cassavetes?"  Christy Lemire of Roger Ebert|RogerEbert.Com gave the film two out of four stars and said, "While "The Other Woman" raises some thoughtful questions about independence, identity and the importance of sisterhood, ultimately it would rather poop on them and then throw them through a window in hopes of the getting the big laugh."  Wesley Morris of Grantland said, "No one knows which takes are funny and which arent. More than once, all three women, especially poor Upton, are caught looking like they dont know what theyre doing."  Richard Roeper gave the film one out of five stars and called the film "Brutal." 
 New York said, "You cant shake the feeling that in a just world, all these women - even Kate Upton - would have better material than this."  Connie Ogle of The Miami Herald gave the film three out of four stars and called the film a "goofy, ridiculous, with more gross-out humor than is strictly necessary but still funny. It falls into the category of Girlfriend Films - as in, go with your girlfriends and leave your date/partner/spouse at home with the PlayStation or the NBA playoffs."  Colin Covert of Star Tribune gave the film three out of four stars, saying "Its an escapist womens empowerment comedy like many others, but elevated by the simple virtue of being, for most of its length, very, very funny."  Ann Hornaday of The Washington Post gave the film one and a half stars out of four, saying "A movie as generic and forgettable as the sofa-size art on its characters walls."  Linda Holmes wrote for NPR, calling the film "a conceptually odious, stupid-to-the-bone enterprise ..."  Betsy Sharkey of Los Angeles Times gave the advice to guys to "Step away from the vehicle, because The Other Woman is out of control and intent on running down a certain kind of male." 

===Awards and nominations===
{| class="wikitable"
|-
! Year !! Award !! Category!! Recipient !!Result
|-
|rowspan="3" | 2014 Teen Choice Awards  Choice Movie: Comedy
|The Other Woman
|  
|- Choice Movie Actress: Comedy Cameron Diaz
| rowspan=4  
|- Choice Movie: Chemistry Cameron Diaz Leslie Mann Kate Upton
|-
|rowspan="4" | 2015
|rowspan="2" | 41st Peoples Choice Awards|Peoples Choice Awards  Favorite Comedic Movie
|The Other Woman
|- Favorite Comedic Movie Actress Cameron Diaz
|- Golden Raspberry Awards {{cite web|title=The 35th Annual Golden Raspberry (RAZZIE®) Awards Nominees for
Worst Actress 2014|url=http://www.razzies.com/history/2015/2015-noms-actress.html|publisher=Golden Raspberry Award Foundation|accessdate=February 8, 2015}}  Worst Actress Cameron Diaz
|  Sex Tape) 
|- 2015 MTV MTV Movie Awards  Best Shirtless Performance Kate Upton
| 
|}

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 