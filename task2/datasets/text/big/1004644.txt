Robot Monster
 
 
{{Infobox film
| name = Robot Monster
| image = Robotmonster.jpg Theatrical release poster
| director = Phil Tucker
| producer = Phil Tucker Al Zimbalist
| writer = Wyott Ordung
| narrator =Slick Slavin (uncredited) 
| starring = George Nader Claudia Barrett George Barrows
| music = Elmer Bernstein
| cinematography = Jack Greenhalgh
| editing = Bruce Schoengarth Merrill White
| studio   =Three Dimensional Pictures, Inc.
| distributor = Astor Pictures
| released =   
| runtime = 62 min.
| country = US English
| budget = $50,000 (estimated)  gross = $1,000,000
}}
  American black-and-white 3D film|3-D science fiction film, both produced and directed by Phil Tucker, written by Wyott Ordung, and starring George Nader, Claudia Barrett, and George Barrows. The film was then distributed by Astor Pictures.

Robot Monster tells the story of Moon robot Ro-Mans mission to destroy all the humans on Earth. It manages to kill all but eight survivors, who have become immune to his death ray. But Ro-Man runs afoul of his leader, The Great Guidance, after he becomes attracted to Carla, the surviving scientists eldest daughter, and refuses to harm her. The Great Guidance kills Ro-Man and must teleport to Earth to finish what has been started. Warren 1982, pp. 146–147. 

==Plot==
Evil Moon robot Ro-Man Extension XJ-2 (George Barrows), referred to as just Ro-Man, has seemingly destroyed all human life on Earth with a Calcinator death ray, all except for eight humans that remain alive. The survivors are an older scientist (John Mylong), his wife, two daughters, his young son Johnny (Gregory Moffett), his assistant, and two space pilots that shortly take off in a spaceship for an orbiting space platform. All eight have now developed an immunity to Ro Mans death ray, having received an experimental antibiotic serum developed by the scientist.

Ro-Man must complete the destruction of all humans, even if it means his physically killing them one-by-one, before his mission to subjugate the Earth is complete. After fruitless negotiations, Ro-Man, with a laser in hand, destroys the spaceship headed for the orbiting platform, killing the two pilots aboard. He later strangles the youngest daughter, Carla (Pamela Paulson), and tosses the assistant scientist, Roy (George Nader), to his death over a cliff.

Ro-Mans mission is waylaid, though, when he develops an illogical attraction to Alice (Claudia Barrett), the scientists eldest daughter. He refuses to eliminate her, forcing the alien leader, The Great Guidance, to teleport to Earth after first killing the disobedient Ro-Man. The Geat Guidance then attempts to finish the genocide by releasing prehistoric dinosaurs and a massive earthquake on the remaining survivors.

But Johnny is alive, having just awoken from a concussion-induced fever dream. Up to now, all that has happened has just been his nightmare. His parents, who had been looking for him, rejoice and take him home. 

Suddenly, Ro-Man, his arms raised in a threatening manner, rushes out of a cave. 

==Cast==
 
* George Nader as Roy
* Claudia Barrett as Alice
* Selena Royle (credited as Selena Royale) as Mother
* John Mylong as The Professor
* Gregory Moffett as Johnny
* Pamela Paulson as Carla
* George Barrows as Ro-Man/Great Guidance John Brown as Voice of Ro-Man/Great Guidance
 

==Production==
Twenty-five-year-old writer/director Phil Tucker made Robot Monster in four days for an estimated $16,000. Except for a few scenes at a house in Los Angeles and a building site near Dodger Stadium,  most footage was filmed outdoors in Bronson Canyon, the site of innumerable motion pictures and TV settings.  Principal photography on Robot Monster wrapped on March 23, 1953. 

Robot Monster s very low budget did not allow for a robot costume as first intended, so Tucker hired his friend George Barrows, who had made his own gorilla suit, to play Ro-Man; Tucker then added the space helmet.  Night club comic Slick Slavin reportedly filmed an opening prologue for the movie. "Movieland briefs." Los Angeles Times, April 1, 1953, p. B8. 
  Invaders from Mars, released a month earlier by 20th Century Fox. Both films contain a young boy stumbling upon an alien invasion who is captured as he struggles to save his family and himself. As the alien commences the final destruction of Earth, the boy awakens to find it was all a dream. Claudia Barrett recalled in an interview that the films original screenplay was designed as reality, but director Tucker changed his mind and then shot a new twist ending that shows the films story has been a boys dream that is about to come true. 

In Robot Monster s opening credits "N. A. Fischer Chemical Products" is given prominent credit for the "Billion Bubble Machine," used as part of Ro-Mans communication device for reporting to his superior, the Great Guidance. 

===3-D===
Robot Monster was shot and projected in dual-strip, polarized 3-D film|3-D. The stereoscopic photography in the film is considered by many critics to be of a high quality, especially by a crew who had no experience with the newly developed camera rig. Hayes 1998, p. 295.  Producer Al Zimbalist later told The New York Times that shooting the film in 3D (which involved using another camera) added an extra $4,510.54 to the budget. 

===Special effects=== Lost Continent, Flight to Mars  Also spliced into the film is view screen footage with a brief appearance of the Rocketship X-M (1950) spaceship boarding; a matte painting of the ruins of New York City was also included from Captive Women (1952). 

===Film score=== The Great Michael Jacksons Thriller. 

Bernstein recalled he was stuck in a period where he was "Blacklisting|greylisted" because of his left wing politics and only offered minor films,  but said he enjoyed the challenge of trying to help a film.  Wyatt Ordung stated that Bernstein scored the film with an eight-piece orchestra, and Capitol Records expressed interest in producing an album. 

==Reception==
Robot Monster was originally released with the 3 Dimensional Pictures short Stardust in Your Eyes, starring nightclub comedian Trustin Howard as Slick Slaven.  In December 1953, the Los Angeles Times reported that "theater men" considered the film "one of the top turkeys of the year". "Movie directors death try balked: Letter sent to newspaper results in his being found unconscious in room at hotel." Los Angeles Times, December 16, 1953, p. 18. 
 worst films ever made. The film was included as one of the selections in the book The Fifty Worst Films of All Time (And How They Got That Way) (1978).   Robot Monster currently holds a 31% approval rating on the review aggregate website Rotten Tomatoes.
Despite rumors to the contrary, the film received some decent reviews, and it grossed $1,000,000 in its initial release, more than 62 times its original investment.   Everything2.com. Retrieved: January 8, 2007.  The film was soon sold to television, where its infamy slowly spread to new generations of cult movie fans. 

===Critical===
The Los Angeles Times called it "a crazy mixed up movie ... even children may be a little bored by it all."  The review in Variety (magazine)|Variety noted, "Judged on the basis of novelty, as a showcase for the Tru-Stereo Process, Robot Monster comes off surprisingly well, considering the extremely limited budget ($50,000) and schedule on which the film was shot."    Variety, December 31, 1952. 

==Aftermath== Knickerbocker Hotel. He was only saved because he had written a suicide letter and sent it to a newspaper, who sent a reporter and some detectives to the hotel. He was discovered with a pass in his pocket from the psychopathic ward of the Veterans Affairs Hospital.  In the letter Tucker said he had not been paid for Robot Monster and was unable to get a job. "When I was refused a job - even as an usher," Tucker wrote, "I finally realized my future in the film industry was bleak." It came out that Tucker and the producer had quarreled, and movie exhibitors had instructions not to let Tucker in to see the film unless he paid admission. 
 Bill Warren claims that Tuckers attempted suicide was due to depression and a dispute with the films distributor, who had allegedly refused to pay Tucker his contracted percentage of the films profits. 

The actors connected to Robot Monster included George Nader who won the Golden Globe award in 1954 as "Most Promising Male Newcomer of the Year" (although his award was not tied to his Robot Monster performance). He signed with Universal Studios where he starred only in secondary features; other new male stars, like Tony Curtis and Rock Hudson, were assigned to major film roles. 
 Communist sympathizer. House Committee on Un-American Activities and eventually cleared her name. By then the damage to her reputation had already been done; she made only two additional films, Robot Monster being her last. 

===Film rights===
Robot Monster is not in the public domain. The copyright renewal is (RE-107-158) on 11-8-1981 by Medallion TV Enterprises. All rights were assigned worldwide to Wade Williams. 

==In popular culture==
Robot Monster was later featured on the B-movie-mocking television series Mystery Science Theater 3000 and the Canned Film Festival, which continued to spread its cult reputation. In an unrelated connection, television ads for Google Nexus show a little girl playing in a replica of the Robot Monster space helmet worn by Ro-Man.
 
==See also==
* List of 3D films
* List of films in the public domain
* List of films considered the worst

==References==
Notes
 

Citations
 

Bibliography
 
* Hayes, R. M. 3-D Movies: "A History and Filmography of Stereoscopic Cinema. Jefferson, North Carolina: McFarland Classics, 1998. ISBN 978-0-78640-578-7.
* Mitchell, Charles P. A Guide to Apocalyptic Cinema. Portsmouth, New Hampshire: Greenwood Publishing Group, 2001. ISBN 978-0-31331-527-5.
* Parla, Paul and Charles P.  Mitchell. "Claudia Barrett interview". Screen Sirens Scream!: Interviews with 20 Actresses from Science Fiction, Horror, Film Noir and Mystery Movies, 1930s to 1960s. Jefferson, North Carolina: McFarland  & Company, 2009. ISBN  978-0-7864-4587-5.
* Rux, Bruce. Hollywood Vs. the Aliens: The Motion Picture Industrys Participation in UFO Disinformation. Berkeley, California: North Atlantic Books/Frog, Ltd., 1997. ISBN 978-1-88331-961-8.
* Spencer, Kristopher. Film And Television Scores, 1950-1979: A Critical Survey by Genre. Jefferson, North Carolina: McFarland & Company, 2008. ISBN 978-0-78643-682-8.
* Strick, Philip. Science Fiction Movies. London: Octopus Books Limited, 1976. ISBN 978-0-70640-470-8.
* Bill Warren (film historian and critic)|Warren, Bill. Keep Watching The Skies, Vol I: 1950–1957. Jefferson, North Carolina: McFarland & COmpany, 1982. ISBN 978-0-89950-032-4.
* Zone, Ray. 3-D Revolution: The History of Modern Stereoscopic Cinema. Lexington, Kentucky: University Press of Kentucky, 2012. ISBN 978-0-8131-3611-0.
 

==External links==
 
*  
*  
*  
*  

=== Mystery Science Theater 3000 ===
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 