Just One of the Guys
 
{{Infobox film
| name = Just One of the Guys
| image = just_one_of_the_guys.jpg
| caption = Theatrical release poster
| director = Lisa Gottlieb
| producer = Andrew Fogelson
| screenplay = Dennis Feldman Jeff Franklin
| story = Dennis Feldman
| starring = {{Plainlist|
* Joyce Hyser
* Clayton Rohner Billy Jacoby
* Toni Hudson
* William Zabka
}}
| cinematography = John McPherson
| editing = Tony Lombardo Tom Scott
| studio = Columbia Pictures Summa Entertainment Group Triton
| distributor = Columbia Pictures
| runtime = 90 minutes
| released = April 26, 1985
| country = United States
| language = English
| budget = 
| gross = $11,528,900
}}
 directed by Lisa Gottlieb. The film is marketed with the tagline "Terri Griffith is about to go where no woman has gone before." It ranked number 48 on Entertainment Weekly&nbsp;s list of the "50 Best High School Movies".  The film is a loose adaptation of William Shakespeares Twelfth Night.  

==Plot==
Terry Griffith (Joyce Hyser) is an aspiring teenage journalist living in Phoenix, Arizona who feels that teachers dont take her school newspaper articles seriously because of her good looks. After failing to get her dream job as a newspaper intern, she comes to the conclusion that it is because she is a girl.
 Billy Jacoby) and her best friend Denise (Toni Hudson) to disguise herself as a boy. Her brother and friend also help to keep tabs on her throughout the experiment. Along the way she meets Rick (Clayton Rohner), a gold-hearted nerd who becomes her pet project. After helping him through an image makeover and encouraging him to start talking to girls (which results in him taking the most popular girl in school out to the prom), Terry starts to fall for him.
 Billy Zabka), dealing with her real college boyfriend Kevin (Leigh McCloskey) and being set up on a blind date with a potential new "girlfriend" named Sandy (Sherilyn Fenn), Terry manages to be accepted as "one of the guys". However, shes rocked when she turns in an article and her teacher says its just not good, making Terry realize her gender and looks were never the issue.

At the senior prom, a jealous Greg picks a fight with Rick, who ultimately trounces the bully in front of the entire class. But when Terrys boyfriend shows up unexpectedly and finds out whats been going on, at first Rick assumes that Terrys big secret was that she was a gay man. Terry has no choice but to reveal herself to Rick. Although she admits to loving him, Rick angrily rejects her, prompting a desperate Terry to kiss him in front of everyone. Seemingly unfazed, Rick simply walks away.

Heartbroken and humiliated, Terry retreats to her room and writes a long article on what it is like to be a girl in boys clothing, detailing all of her experiences, both good and bad, in and out of school.

In the end, Terry returns to her own school. When her article is printed in the newspaper, she receives high praise from her teachers and friends and finally earns her dream job at the newspaper office. Nevertheless, she still finds herself yearning for Rick, who has not spoken to her since the prom. One day during the summer, while hanging out with Buddy, Rick suddenly turns up after reading her article. Realizing their true feelings for each other, they reconcile and make plans for another date. They decide to go for a drive in Terrys car, but before Buddy can join them, an attractive blonde on a motorcycle rides up and beckons to him with a smile. Buddy then climbs onto the back of her motorcycle, and both couples happily drive away as the film closes.

==Cast==
*Joyce Hyser as Terry Griffith
*Clayton Rohner as Rick Morehouse Billy Jacoby as Buddy Griffith
*Toni Hudson as Denise Billy Zabka as Greg Tolan
*Leigh McCloskey as Kevin
*Sherilyn Fenn as Sandy
*Deborah Goodrich as Deborah
*Arye Gross as Willie
*Robert Fieldsteel as Phil
*Stuart Charno as Reptile
*John Apicella as Coach Mickey Morrison
*Kenneth Tigar as Mr. Raymaker
*Steven Basil as Mark
*Matt Giancola as Biff

==Soundtrack==
{{Infobox album  
| Name = Just One of the Guys
| Type = soundtrack
| Artist = various artists
| Cover =
| Released = 1985
| Recorded =
| Genre = Rock music|Rock, Pop music|pop, Contemporary R&B|R&B
| Length =  Elektra
| Producer =
| Last album =
| This album =
| Next album =
}}
{{Album ratings|noprose=yes
|rev1=Allmusic
|rev1score=   
}}

# "Just One of the Guys" by Shalamar – 3:55
# "Girls Got Something Boys Aint Got" by Midnight Star – 3:56
# "Tonight Youre Mine, Baby" by Ronnie Spector – 4:57
# "Prove It to You" by Dwight Twilley – 3:20 Berlin – 4:23
# "Way Down" by Billy Burnette – 3:34
# "Burning" by Brock/Davis – 4:20
# "Thrills" by Greg French – 3:15
# "Hard Way" by Brock/Davis – 4:48 Tom Scott – 2:29

The songs and music that were played in the film not on the soundtrack.
# "Trouble (Lindsey Buckingham song)|Trouble" by Lindsey Buckingham
# "Down on the Street" by The Stooges Private Domain
# "Comb My Hair" by Johnny Lyon
# "Buns" by Bonedaddys
# "Gone Too Far" by Neurotica

==Similar films==
*Just One of the Girls (1993)
*Motocrossed (2001) (television film)
*Shes the Man (2006)
*Ladybugs (film)|Ladybugs (1992)

==See also==
*Cross-dressing in film and television

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 