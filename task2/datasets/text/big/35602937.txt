All the Best (film)
{{Infobox film
| name           = All the Best
| image          = All the Best Telugu poster.jpg
| caption        = Film poster
| director       = J. D. Chakravarthy
| producer       = G. Sambasiva Rao
| story          = J. D. Chakravarthy
| screenplay     = Krishna Mohan Challa Srikanth J. D. Chakravarthy Kota Srinivasa Rao Lucky Sharma Rao Ramesh Anisha Singh Hemachandra
| cinematography = G. Siva Kumar
| editing        = Venkatesh
| released       =  
| runtime        =
| studio         = Sudha Cinema
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
All the Best is a 2012 Telugu film written and directed by J. D. Chakravarthy. The film stars Meka Srikanth|Srikanth, J. D. Chakravarthy and Lucky Sharma in lead roles.  The film was released on June 29, 2012 with mixed reviews and became one of the successful films of 2012. Especially Telangana Shakuntalas character of Waheed Rehman was much praised by the critics and audiences.This films comedy at each and every inch made it successful to run a 100 days. 

==Cast== Srikanth
* J. D. Chakravarthy
* Lucky Sharma Chandra Mohan
* Kota Srinivasa Rao
* Brahmanandam
* Raghu Babu
* Brahmaji
* Krishna Bhagavaan Pradeep Rawat
* Rao Ramesh Ranganath
* Jeeva
* Suthivelu
* Anisha Singh

==Soundtrack==
{{Infobox album
| Name = All the Best
| Longtype =
| Type = soundtrack Hemachandra
| Cover =
| Released =  
| Recorded = 2012
| Genre = Film soundtrack
| Length = 21:08
| Language = Telugu
| Label = Aditya Music
| Producer =
}} Hemachandra and the album was released under Aditya Musiclabel.  All the songs in the album were penned by young lyricist Geetha Poonik.

{{Track listing
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 21:08
| writing_credits =
| lyrics_credits  = no
| music_credits   =
| title1          = Sundari Tingaari
| note1           =
| lyrics1         =
| extra1          = Vedala Hemachandra|Hemachandra, Rita, Pranavi
| length1         = 03:46
| title2          = All The Best
| note2           = 
| lyrics2         =
| extra2          = N. C. Karunya, Noel, Bindu Ramakrishna, Aditya
| length2         = 03:48
| title3          = Thuogoji Pagoji - 1
| note3           =
| lyrics3         = Mano
| length3         = 03:31
| title4          = Lammi Lammi
| note4           =
| lyrics4         =
| extra4          = Sunil Kashyap, Kajal
| length4         = 03:18
| title5          = Sana Sana
| note5           =
| lyrics5         =
| extra5          = Hemachandra, Monisha
| length5         = 03:14
| title6          = Thuogoji Pagoji - 2
| note6           =
| lyrics6         =
| extra6          = Priya Himesh, Mano
| length6         = 03:31
}}

==References==
 

==External links==
*  

 
 
 


 