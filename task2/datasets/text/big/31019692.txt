Blackmail (1947 film)
{{Infobox film
| name           = Blackmail
| image          = Blackmail (1947 film) poster.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Lesley Selander
| producer       = William J. OSullivan
| screenplay     = Royal K. Cole Albert DeMond
| story          = Robert Leslie Bellem
| narrator       =
| starring       = William Marshall Adele Mara Ricardo Cortez
| music          = Mort Glickman
| cinematography = Reggie Lanning
| editing        = Tony Martinelli
| studio         =
| distributor    = Republic Pictures
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         =
}}
Blackmail (1947) is an American crime film noir directed by Lesley Selander.  The drama features William Marshall, Adele Mara and Ricardo Cortez.  The lead character is based on a pulp magazine hero Dan Turner, Hollywood Detective.

==Plot== shamus is approached by an entertainment executive to stop a blackmail plot against him.

==Cast== Dan Turner
* Adele Mara as Sylvia Duane
* Ricardo Cortez as Ziggy Cranston
* Grant Withers as Police Inspector Donaldson
* Stephanie Bachelor as Carla Richard Fraser as Antoine le Blanc
* Roy Barcroft as Spice Kellaway
* George J. Lewis as Blue Chip Winslow
* Gregory Gaye as Jervis Tristram Coffin as Pinky
* Eva Novak as Mamie, the Maid
* Bud Wolfe as Gomez

==Reception==
When released the New York Times gave the film a mixed review, writing, "Evidently the writers and/or Republic, the manufacturer, were convinced that some fast dialogue would enhance the rather confused goings on. But this yarn about a California playboy who becomes involved with shakedown artists and is aided by a brash, private investigator needed more than an occasional bright quip to keep things clear and moving." 

==References==
 

==External links==
*  
*  
*  
 
 
 
 
 
 
 
 
 
 
 
 
 