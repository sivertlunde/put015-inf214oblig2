Chandrakantham
{{Infobox film 
| name           = Chandrakantham
| image          =
| caption        =
| director       = Sreekumaran Thampi
| producer       = Rajashilpi
| writer         = Sreekumaran Thampi
| screenplay     = Sreekumaran Thampi
| starring       = Prem Nazir Jayabharathi Adoor Bhasi Balakrishnan
| music          = M. S. Viswanathan
| cinematography = Vipin Das
| editing        = MS Mani
| studio         = Rajashilpi
| distributor    = Rajashilpi
| released       =  
| country        = India Malayalam
}}
 1974 Cinema Indian Malayalam Malayalam film, directed by Sreekumaran Thampi and produced by Rajashilpi. The film stars Prem Nazir, Jayabharathi, Adoor Bhasi and Balakrishnan in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast==
 
*Prem Nazir
*Jayabharathi
*Adoor Bhasi
*Balakrishnan
*Kedamangalam Sadanandan
*Sankaradi
*T. R. Omana
*James
*PK Joseph
*T. S. Muthaiah
*Ali Baby Sumathi
*Bahadoor
*Master Rajakumaran Thampi
*Satheesh Sumithra
 

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by Sreekumaran Thampi and Traditional. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aa Nimishathinte || S Janaki || Sreekumaran Thampi || 
|-
| 2 || Aa Nimishathinte || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 3 || Chirikkumpol Neeyoru || KP Brahmanandan || Sreekumaran Thampi || 
|-
| 4 || Engirunnaalum Ninte || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 5 || Hridayaavahini Ozhukunnu Nee || M. S. Viswanathan || Sreekumaran Thampi || 
|-
| 6 || Mazhameghamoru Dinam || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 7 || Nin Prema Vaanathin || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 8 || Paanchaala Raaja Thanaye || Bahadoor || Traditional || 
|-
| 9 || Prabhaathamallo Nee || M. S. Viswanathan || Sreekumaran Thampi || 
|-
| 10 || Punaraan paanjetheedum || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 11 || Pushpaabharanam || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 12 || Raajeevanayane || P Jayachandran || Sreekumaran Thampi || 
|-
| 13 || Suvarnamekha Suhaasini || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 14 || Swargamenna Kaananathil || K. J. Yesudas || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 