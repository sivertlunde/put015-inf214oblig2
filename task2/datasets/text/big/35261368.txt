The Unknown (1915 comedy film)
 
{{Infobox film
| name     = The Unknown
| image    =
| caption  =
| director = J. E. Mathews
| producer = Archie Fraser Colin Fraser
| writer   =
| based on =
| starring = Peter Felix Porky Keans
| music    =
| cinematography =
| editing  =
| studio   = Fraser Film Release and Photographic Company
| distributor = Fraser Film Release and Photographic Company
| released =  
| runtime  = 2,000 feet 
| language = Silent film English intertitles
| country  = Australia
| budget   =
}}
The Unknown was a 1915 film directed by J. E. Mathews released in support of The Sunny South or The Whirlwind of Fate (1915). 

It is considered a lost film. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p 52 

==Cast==
*Jack Kearns
*Mick King
*Peter Felix
*Jeff Smith
*Frank Longhrey

==Production==
The movie was shot in Newcastle over December 1914 and January 1915. 

It starred two boxers and vaudeville star Jack Kearns. 

==Reception==
The film premiered at Waddingtons Globe Theatre, George Street in Sydney. According to the Referee "Mick King, Herr Kearns, and Peter Felix have, in this picture, displayed surprising histrionic ability."   

The Motion Picture News called it "a really good comedy, Keystone in appearance". 

==References==
 

 
 
 
 
 


 