Frat Party at the Pankake Festival
 
{{Infobox album
| Name        = Frat Party at the Pankake Festival
| Type        = video
| Artist      = Linkin Park
| Cover       = Linkin_Park-Frat_Party_CD.jpg
| Released    = November 20, 2001
| Recorded    = Recorded at various locations
| Genre       = Nu metal, rap metal, alternative metal
| Length      = 50:03 (main title)
| Label       = Warner Reprise Video, Warner Bros. Records
| Director    = 
| Producer    = Bill Berg-Hillinger, Joe Hahn, David May, Angela Smith
| Reviews     = 
| Chronology  = 
| Last album  = 
| This album  = Frat Party at the Pankake Festival (2001)
| Next album  = The Making of Meteora (2003)
}}

Frat Party at the Pankake Festival is the first documentary DVD by Linkin Park, released on November 20, 2001 through Warner Bros. Records and Warner Reprise Video. The DVD was produced by Bill Berg-Hillinger, Joe Hahn, David May and Angela Smith.

== Synopsis == easter eggs that unlock even more bonus special features.

== Chapter listing ==
{{tracklist title1   = Intro length1  = 1:51 title2  Papercut
|length2  = 3:12 title3   = Beginnings length3  = 5:24 title4   = Points of Authority length4  = 3:30 title5   = The Live Show length5  = 3:17 title6   = "Crawling (song)|Crawling" Video Shoot length6  = 1:21 title7   = Crawling length7  = 3:35 title8   = Touring length8  = 9:28 title9   = Cure for the Itch length9  = 0:54 title10  = The Band length10 = 7:56 title11  One Step Closer length11 = 2:55 title12  = The Future length12 = 2:43 title13  = In the End length13 = 3:36 title14  = End length14 = 0:16
}}

{{tracklist headline  = Special Features collapsed = yes title1    = Making of In the End Video length1   = 12:42 title2    = Crawling Live Music Video with Multiangles length2   = 4:07 title3    = Mike and Joes Art Piece length3   = 1:35 title4    = Chesters Tattoos length4   = 1:23 title5    = Crawling Bryson Bluegrass Version note5     = audio only length5   = 4:06 title6    = High Voltage note6     = audio only length6   = 3:45 title7    = My December note7     = audio only length7   = 4:23 title8    = Weblink
}}

== Guest appearances == Disturbed members Slipknot turntablist Sid Wilson appears on the DVD, where he is testing out Joe Hahns turntables and disc jockeys in one scene. Rock band Deftones, including members Chino Moreno and Stephen Carpenter, appear in a few shots of one scene, where they were taken a photo before they played at a concert when Mike Shinoda was going undercover for the KGB, as Chester Bennington explained in one scene for the DVD.

== Easter Eggs ==

=== Esaul (A Place for My Head Demo) ===
To access it, you must activate subtitles first. Then go to the "Chapter Selection" screen and select the "Beginnings" chapter. When Brad Delson is talking, the words "from the beginning" will appear at the bottom of the screen (at about 9:33). When the words appear in the screen, press the "Enter" button on your remote. You will be taken to a video of early studio footage from 1999, before the band was signed. The song that they are performing an early version of "A Place for My Head", called "Esaul".

=== Points of Authority Live at The Dragon Festival ===
To access it, go to the "Disc Setup" screen and wait, without press any button. After about two minutes the screen will change and a honeycomb-like keyboard design will appear. Using your remote, enter the following sequence of numbers: 1-4-8-5-9-5-2. At the end of number 2 (on computer simply click on the numbers), you will be taken to a video depicting an alternate version of the "Points of Authority" video featured in the main title.

=== One Step Farther ===
This Easter egg is fairly easy to find. Simply turn on the "Directors Commentary" audio track and watch the video for "One Step Closer." The audio track will be playing backward while the video will still be playing forward.

=== One Step Closer (Humble Brothers Remix) ===
When viewing the One Step Closer video as part of the main title (accessed from the Chapter Selection, not the Track Selection menu), wait until the last time Chester says "Shut up!" right before "Im about to break!" and press Enter on your remote/keyboard. This takes you to an audio-only section where you can listen to the remix of One Step Closer by the Humble Brothers. This is an earlier version of the one that appears later on the remix album Reanimation (album)|Reanimation, without Jonathan Davis providing additional vocals.

=== Broken Table Incident in London clip ===
There are 4 ways to do this:
* Stop the DVD and then set it to title 3, chapter one.
* Go to Credits and press enter, then previous chapter.
* Go to Special Features then back to the Main Menu. Press 9 and enter on your remote. (This method does not work on computers)
* Go to Main Menu then Special Features. Go back again to the Main screen and then press UP UP DOWN DOWN LEFT RIGHT LEFT RIGHT buttons waiting about a second after each buttons. (This one works the best)

You will be taken to a 12-minute footage in London including Mikes incident of a glass table that hes broke in half, causing Joe and Mike dismantle and hide it to avoid paying a fine.

== Personnel ==

=== Linkin Park ===
* Chester Bennington – lead vocals
* Rob Bourdon – drums, backing vocals
* Brad Delson – guitars, backing vocals
* Joe Hahn – DJ, sampling, backing vocals Dave "Phoenix" Farrell – bass, backing vocals
* Mike Shinoda – MC, vocals, beats, sampling, guitar

=== Production ===
* Produced by Bill Berg-Hillinger for Id Playground and Mr. Hahn
* Executive producer: Mr. Hahn
* Edited by Bill Berg-Hillinger
* DVD-video producers: David May for Warner Bros. Records and Angela Smith for Metropolis DVD
* Menu design and animation: Sean Donnelly, Metropolis DVD
* Technical director: James Moore, Metropolis DVD
* DVD authoring by Metropolis DVD, NYC
* Worldwide representation: Rob McDermott for the Firm
* Additional representation by: Carey Segura and Ryan DeMarti
* Booking agent: Mike Arfin for Artist Group International
* Legal: Danny Hayes for Selvern, Mandelbaum and Mintz
* Business manager: Michael Oppenheim and Jonathan Schwartz for Gudvi, Chapnick and Oppenheim
* Photography for package: James Minchin III
* Art direction and design: Tom Peanutz

== Trivia == Minutes to Midnight, Mike Shinoda and Chester Bennington admit they are bad in naming titles, citing Frat Party at the Pankake Festival as an example. 

== References ==
 

 

 
 
 
 
 
 
 
 