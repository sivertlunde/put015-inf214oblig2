Jaguar Lives!
{{Infobox film
| name           = Jaguar Lives!
| image          = Jaguar-lives-poster.jpg
| image_size     = 
| caption        = 
| director       = Ernest Pintoff
| writer         = 
| narrator       =  Joe Lewis
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = July 1979
| runtime        = 96 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
 Joe Lewis, Christopher Lee, Donald Pleasence and Barbara Bach.  Its plot follows a secret agent who battles an international drugs ring.

==Plot==
After his partner is killed, a secret agent and karate expert (Joe Lewis) sets out to defeat an international drug cartel.

==Cast== Joe Lewis - Jonathan Cross (Jaguar)
* Christopher Lee - Adam Caine
* Donald Pleasence - General Villanova
* Barbara Bach - Anna Thompson
* Capucine - Zina Vanacore
* Joseph Wiseman -   Ben Ashir
* Woody Strode - Sensei
* John Huston - Ralph Richards
* Gabriel Melgar - Ahmed
* Anthony De Longis - Bret Barrett
* Sally Faulkner - Terry
* Gail Grainger - Consuela
* Anthony Heaton - Coblintz
* Luis Prendes - Habish
* Simón Andreu - Petrie

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 