Marines, Let's Go
{{Infobox film
| name           = Marines, Lets Go
| image          = MarinesLetsGo.jpg
| caption        = Original film poster
| director       = Raoul Walsh
| producer       = Raoul Walsh
| writer         = Raoul Walsh (story) John Twist Tom Reese Linda Hutchings Barbara Stuart
| music          = Irving Gertz title song sung by Rex Allen
| cinematography = Lucien Ballard
| editing        =
| art director   =
| titles         = 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 103 min.
| country        = United States
| language       = English budget          = $1,665,000 
}}
 1961 CinemaScope Tom Reese) What Price Battle Cry).

==Production==
Walsh filmed the movie on location in Japan with extras from the US Marine Corps who were pulled off filming due to the possibility of their being sent to  .   The film was completed in Okinawa.

The Marine technical advisor of the film was Colonel Jacob G. Goldberg (1911–2008), who served 30 years in the Marine Corps. 

==Reception== PT 109.

==See also==
 
*List of films featuring the United States Marine Corps

==References==
 

==External links==
*  , The New York Times, August 16, 1961.
* 
*  

 

 
 
 
 
 
 
 
 
 