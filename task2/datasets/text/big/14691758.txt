Don't Change Your Husband
 
{{Infobox film
| name           = Dont Change Your Husband
| image          = Film Daily 1919 Cecil B DeMille Dont Change Your Husband.png
| caption        = Advert for the film
| director       = Cecil B. DeMille
| producer       = Jesse L. Lasky
| story          = Jeanie MacPherson
| starring       = Elliott Dexter
| music          = 
| cinematography = Alvin Wyckoff
| editing        = Anne Bauchens
| distributor    = Paramount Pictures
| released       =  
| runtime        = 86 minutes
| country        = United States 
| language       = Silent (English intertitles)
| budget         = United States dollar|$73,922.14 
| gross          = $292,134.10 
}}
 silent comedy film directed by Cecil B. DeMille and starring Gloria Swanson. The film was the third of six "marriage films" directed by DeMille and the first DeMille film starring Gloria Swanson.     A print of the film is stored at the George Eastman House.   

==Plot==
Based upon a description in a film magazine,  Leila Porter (Swanson) has grown tired of her husband James Denby Porter (Dexter), the glue king, as she is romantic but he is prosaic. Moreover, he is careless of his personal appearance, gets cigar ash in the carpet, and eats green onions before he tries to kiss her. She obtains a divorce and then marries James friend Schuyler Van Sutphen (Cody), but discovers that Van Sutphen is a real beast. When she later discovers that her ex-husband has changed as a result of the divorce, still loves her, and would be happy to have her back, Leila divorces once again in order to remarry James.

==Cast==
* Elliott Dexter - James Denby Porter
* Gloria Swanson - Leila Porter
* Lew Cody - Schuyler Van Sutphen
* Sylvia Ashton - Mrs. Huckney
* Theodore Roberts - The Bishop, Rt. Rev. Thomas Thornby
* Julia Faye - Nanette aka Toodles
* James Neill - Butler
* Ted Shawn - Faun
* Irving Cummings - Undetermined Role (uncredited)
* Clarence Geldart - Manager of Gambling Club (uncredited)
* Raymond Hatton - Croupier at Gambling Club (uncredited)
* Jack Mulhall - Member of Gambling Club (uncredited)
* Guy Oliver - Mr. Frankel, Dressmaker (uncredited)
* Sam Wood - Undetermined Role (uncredited)

==References==
 

==External links==
 
* 
* 

 

 
 
 
 
 
 
 