Restrepo (film)
 
{{Infobox film
| name           = Restrepo
| image          = Restrepo poster.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Film poster
| director       = Tim Hetherington Sebastian Junger
| producer       = Tim Hetherington Sebastian Junger
| cinematography = Tim Hetherington Sebastian Junger
| editing        = Michael Levine National Geographic Entertainment United States}}
| runtime        = 93 minutes  
| country        = United States
| language       = English
| gross          = $1,436,391 (worldwide)   
}}
 Afghanistan war, directed by American journalist Sebastian Junger and British/American photojournalist Tim Hetherington.
 Vanity Fair, 503rd Infantry PFC Juan naturalized U.S. citizen. 

==Synopsis==
The film follows the 2nd Platoon of Battle Company on a 15-month deployment in the Korengal Valley of northeast Afghanistan in the Nuristan area. The Korengal flows north to the Pech, which then flows east to the Kunar River valley on the border with Pakistan.  The film chronicles the lives of the men from their deployment to the time of their return home. The Korengal Valley was at the time regarded as "the deadliest place on Earth" (as stated in the documentary itself, trailers, and television commercials on the National Geographic Channel). The goal of the deployment was to clear the Korengal Valley of insurgency and gain the trust of the local populace.

They begin their deployment at Combat Outpost (OP) Korengal, or "KOP", and early in the campaign PFC Juan S. Restrepo is killed, as well as another team member, PFC Vimoto. The film portrays negotiations with the local people, construction of an advanced outpost, OP Restrepo, as well as the challenges and intermittent firefights they face.

In the latter portion of the film, the dangerous mission Operation Rock Avalanche is shown along with some of its tragic consequences, such as dead civilians and soldiers, as well as the emotional distress that the soldiers are left within its aftermath. For his actions during the operation Staff Sergeant Salvatore Giunta, who is not featured in the film, would later become the first living person to receive the Medal of Honor since the Vietnam War.  

==Reception== Grand Jury Prize for best documentary at the 2010 Sundance Film Festival.    The film received a certified fresh rating of 96% on Rotten Tomatoes; the consensus states: "Forsaking narrative structure for pure visceral power, Restrepo plunges viewers into the experiences of soldiers on the front lines of the Afghan War." 

Roger Ebert awarded Restrepo four out of four stars.  Additionally, numerous critics and publications included it in their annual top film selections.   
 top documentary films of 2010 by the National Board of Review.
 Academy Award Inside Job. 

==Sequel==
In March 2014, Realscreen magazine revealed    that Junger was working on a sequel to Restrepo, titled Korengal (film)|Korengal. The film was released in theaters in New York on May 30, 2014, with a pay-VOD release following in September 2014.

The sequel "departs from the Cinéma vérité|vérité style of the original somewhat, as it features an original score and some archival news footage". 

==See also==
* The Battle for Marjah, an HBO documentary covering the efforts of Bravo Company, 1st Battalion, 6th Marine Regiment, in Operation Moshtarak
* Armadillo (film)|Armadillo, a 2010 Danish documentary about Danish troops stationed at Forward Operating Base Armadillo|"Armadillo" forward operating base in Helmand Province, Afghanistan
* 16 Days in Afghanistan, a highly referenced 2008 documentary about the country

==References==
 

==External links==
*  
*  
*  
*  
*   at  
*  
*  
*  

 
 
{{succession box
| title= 
| years=2010
| before=We Live in Public
| after=How to Die in Oregon}}
 
 

 
 
 
 
 
 
 
 
 