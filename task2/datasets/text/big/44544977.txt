Apache Trail (film)
{{Infobox film
| name           = Apache Trail
| image          = Apache Trail poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Richard Thorpe
| producer       = Samuel Marx
| screenplay     = Maurice Geraghty
| story          = Ernest Haycox
| starring       = Lloyd Nolan Donna Reed William Lundigan Ann Ayars Connie Gilchrist Chill Wills
| music          = Sol Kaplan
| cinematography = Sidney Wagner  Frank Sullivan
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film directed by Richard Thorpe and written by Maurice Geraghty. The film stars Lloyd Nolan, Donna Reed, William Lundigan, Ann Ayars, Connie Gilchrist and Chill Wills. The film was released on June 24, 1942, by Metro-Goldwyn-Mayer.  

==Plot==
Tom Folliard (William Lundigan) is released from jail and seeks work at his prior employer, a stagecoach line. He is sent to manage a stagecoach rest stop in a remote area. Upon arrival, he meets a woman, Señora Martinez (Connie Gilchrist) and her daughter Rosalia (Donna Reed) who cook and clean at the rest stop. When the next stage arrives, among its cargo is a strong box with cash. Soon after, Folliards brother "Trigger" Bill Folliard (Lloyd Nolan), a known outlaw arrives and seekes shelter from the local Apaches, whom he has offended. Upon his discovery that a strong box is present, he plans to steal it and make a getaway. During Tom Folliards absence from the rest stop, Trigger Bill gets the upper hand on the stagecoach line employees watching him and tries to escape with the proceeds but is thwarted by his brothers arrival back at the rest stop. Soon thereafter, the Apaches attack the rest stop but are repulsed. They demand Trigger Bill and in return they will leave the rest stop alone. After realizing that they wont be able to hold off the Apaches much longer, Trigger Bill attempts to flee but is killed by the pursuing Apaches. Meanwhile, Tom, after a brief flirtation with one of the stagecoach passengers, Constance Selden (Ann Ayars), tells Señora Martinez that he would like to court her daughter.

== Cast ==
*Lloyd Nolan as Trigger Bill Folliard
*Donna Reed as Rosalia Martinez
*William Lundigan as Tom Folliard
*Ann Ayars as Constance Selden
*Connie Gilchrist as Señora Martinez
*Chill Wills as Pike Skelton
*Miles Mander as James V. Thorne
*Gloria Holden as Mrs. James V. Thorne
*Ray Teal as Ed Cotton
*Grant Withers as Les Lestrade
*Fuzzy Knight as Juke
*Trevor Bardette as Amber
*Tito Renaldo as Cochee
*Frank M. Thomas as Maj. Lowden
*George Watts as Judge Keeley

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 