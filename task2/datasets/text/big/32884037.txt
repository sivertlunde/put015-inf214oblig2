The Adventures of Algy
 
 
{{Infobox film
| name           = The Adventures of Algy
| image          = The_Adventures_of_Algy.jpg
| image_size     = 
| caption        = Still from the film
| director       = Beaumont Smith
| producer       = Beaumont Smith
| writer         = Beaumont Smith
| based on      = 
| narrator       = 
| starring       = Claude Dampier
| music          = 
| cinematography = Lacey Percival Frank Stewart Syd Taylor Charles Barton
| editing        = 
| studio         = Beaumont Smiths Productions
| released       = 20 June 1925 
| runtime        = 97 mins
| country        = Australia
| language       = silent
| budget         = 
}}
Hullo Marmaduke is a 1925 Australian film comedy from director Beaumont Smith about a "silly ass" Englishman (Claude Dampier) who inherits a sheep station in New Zealand. It is an unofficial follow up to Hullo Marmaduke (1924), which also starred Dampier.

Unlike most of Smiths silent films, most of the movie survives today.

==Plot==
Algy (Claude Dampier) is an Englishman who travels to New Zealand to claim a sheep station he has inherited. He falls in love with a neighbour, Kiwi McHill (Bathie Stuart), then travels to Australia. He runs into Kiwi again, using dances she has learned from her Maori friends in a Sydney revue. When he returns to New Zealand he strikes oil on his farm and he and Kiwi are married.

==Cast==
*Claude Dampier as Algernon Allison
*Bathie Stuart as Kiwi McGill
*Eric Harrison as Murray Watson
*Billie Carlyle as Mollie Moore
*George Chalmers as John McGill
*Lester Brown as stage manager
*Eric Yates
*Beaumont Smith
*Hilda Attenboro
*Verna Blain

==Production==
The film was shot on location in New Zealand (Wellington, Rotarua) and Sydney (Circular Quay) during early 1925. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 127.  There were two dance sequences, one in a Maori village and the other in a Sydney theatre, plus extensive scenic photography of New Zealand.

Dampier later married his co-star, Billie Carlyle. 

==Reception==
Reviews for the film were generally positive.   Film writers Andrew Pike and Ross Cooper wrote that:
 The film... reveals a heavy reliance on titles to propel the insubstantial plot along, and frequently the images are little more than illustrations for the printed text.  
Smith was becoming exhausted with film production and concentrated on distribution and exhibition instead over the next eight years. He returned to directing with The Hayseeds (1933).

==References==
 

==External links==
*  in the Internet Movie Database
*  at the National Film and Sound Archive
*  at the New Zealand Feature Project

 

 
 
 
 
 