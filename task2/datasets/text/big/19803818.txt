The Last Egyptian
{{Infobox book
| name          = The Last Egyptian  A Romance of the Nile 
| title_orig    = 
| translator    = 
| image         = File:TheLastEgyptian.jpg
| caption       = First edition
| author        = L. Frank Baum
| illustrator   = Francis P. Wightman
| cover_artist  = 
| country       = United States
| language      = English
| subject       =  romance 
| publisher     = Edward Stern & Co.
| pub_date      = 1907
| english_pub_date = 
| media_type    = Print (hardcover)
| pages         = 287 pp
| oclc          = 419213619
| preceded_by   = 
| followed_by   = 
}} anonymously on May 1, 1908  by Edward Stern & Co. of Philadelphia, with eight color plate illustrations by Francis P. Wightman. Baum left his name off of the book because he was concerned that "masquerading as a novelist" might hurt his career as a writer for children; but he identified himself as the author of the book during his lifetime when making fantasy films for children proved a financial disaster.
 his wife (her daughter) to read.

==Plot summary==
The extensive diacritical marks appear in the novel as published by Stern.
 English Egyptologist; an Egyptians|Egyptian, Kāra, and a dragoman named Tadros.  Kāra, being white-skinned, is mistaken by Bey for a Copt, though he is no Christian, and he has no respect for Arab Muslims, either.  Kāra claims to be a descendent of Ahtka-Rā, High Priest of Ămen, whom he says ruled Rameses II as his Puppet state|puppet, including hiding the latters death for two years--archaeology says Rameses reigned 67 years, but according to Kāra, he ruled only 65.
 avenge her against Lord Roane for leaving her in poverty.  It is within the cliff that their home is built in that the treasure is kept, behind a wall built over an opening of a cavern too deep to use as a shelter.

Tadros and the Bey compete to acquire these papyri from him to sell, and Kāra nearly kills the former for stealing one, but he stops, knowing he can use him.  He allows him to have that one in exchange for the girl Nepthys, whose principal interest is cigarette smoking, whom Tadros is set to acquire for anothers harem.
 dwarf embalmer, Sebbet, to transport her remains for Mummy|mummification.  He meets Winston on his dahabeah and accompany him to Cairo.  In Cairo, Kāra seeks to have his gems recut in the modern style, but instead sells them for cash, and takes his steps toward revenge on Lord Roane.

Charles Consinor, 9th Earl of Roane, is now elderly and of poor reputation, while his son, Viscount Roger Consinor is a professional gambler.  Kāra manipulates things to get Charles (Lord Roane) a diplomatic post in Cairo.  There, he catches Roger cheating with marked cards and loaded dice at the club, puts Nepthys in his personal harem, and then proceeds to make moves on Lord Roanes granddaughter, Aneth Consinor, who has been sent back to the family from school on account of unpaid tuition.  He falls in love with Aneth (as does Winston), causing him to send Nepthys back home, but when she refuses to marry him, considering him a friend and herself unready for marriage, he quickly returns to his desire for his grandmothers revenge.  Winston tells Kāra that thee latter cannot marry her because they are cousins, but Kāra cares not, stating that Egyptian kings married their sisters, so marrying a cousin is nothing.
 contractor on embankment project.  Kāra is aware of this and tries to blackmail Lord Roane into forcing Aneth into marriage with him.  Roane refuses, saying his granddaughter should not hurt for his misdeeds, calling Kāra an "infamous nigger," to which Kāra "redden  at the epithet."  Kāra then approaches Aneth with the proposition, and she agrees to marry him out of filial duty, to which he responds by giving her forged documents for her to destroy, while he retains the incriminating documents.  Winston, upon learning that Kāras accusation is true, conspires with Aneths Chaperone (social)|companion, Mrs. Lola Everingham, (wife of an engineer known for his work in Asia) to woo her into marriage with himself.  This causes the dutiful girl more pain if anything, creating a longing for something she will not let herself have.

Paying off Tadros to help them instead of Kāra, Winston and the Consinors plan to abduct Aneth to Winstons  .

Tadros and Viscount Consinor leave the vessel at Fedah, expecting Kāra to return soon for more treasure, as he has secured the help of his brother-in-spirit, Sheik Antar, a large Arab who dyes his grey beard black, and his Muslim followers, who also live in a small town on the Nile inhospitable to strangers.  Tadros is aware that Kāra has been buying on credit, and will be forced to soon return for more gemstones, including to pay Antar.
 rushes that Hatatcha used as a bed.  Tadros asks him if he is "comfortable"—to which he replies "not very"—but he clarifies enough to remain still for several hours.  From there he is able to see what Kāra presses in the wall to enter into the secret passage.

Kāra places the Talisman of Ahkta-Rā on his finger, believing it will give him his ancestors power if he uses it only temporarily, in spite of the curse upon it, but as he is trying to lug two sacks full of gems, a statue of Isis, which had fallen the last time he was in the tomb, falls again, knocking it off his hand, and when he stumbles, a lamp he has tied to the button of his shirt is knocked out.  In the darkness, he sees the Talisman return to its spot, or at least he is confusing it with the candle Consinor is using that was left for him by Tadros.

Kāra attacks Roger, but Roger is a skilled wrestler and manages to get on top of him as Kāra tries to asphyxiate him.  He is able to knock Kāras head to the ground long enough that he loses consciousness, allowing Roger to flee.  Kāra, though, has inadvertently removed the dagger that keeps open the vault door, which Hatatcha informed him cannot be opened from the inside, and even as Roger can hear him regain consciousness and get up, he is unaware that Kāra is quite trapped and continues to run.  Mistaking Roger for Kāra, Nepthys stabs him fatally.

When Winstons dahabeah arrives at Fedah, Tadros tells Antar that the police have come and taken Kāra and will also arrest Antar and his men if caught.  Although he must work to convince Antar he has nothing to do with the police, eventually he gets them to flee northward.  Not knowing what has happened to Kāra, and not wishing Nepthys to be punished for the death of Roger Consinor, he gives the same story to Winston and the Consinors, and is triumphantly hired to be their dragoman as they go on to Luxor for the wedding of Aneth Consinor to Gerald Winston.

==Film adaptation==
{{Infobox film
| name           = The Last Egyptian
| image          =
| caption        =
| director       = J. Farrell MacDonald
| producer       = L. Frank Baum Louis F. Gottschalk
| writer         = L. Frank Baum
| narrator       = Vivian Reed Howard Davies Jane Urban
| music          = Louis F. Gottschalk
| cinematography = James A. Crosby
| editing        =
| distributor    = Alliance Program
| released       =  
| runtime        =
| country        = United States
| budget         =
| gross          =
}} Daughters of Destiny, although they are now both attributed to Baum with no mention of Staunton.  The film was not successful; the Oz name had been temporarily tainted as "box office poison" for producing films then-considered too juvenile, and even a name change to Dramatic Feature Films did not help it in the eyes of exhibitors.

This film, unlike the other three features of The Oz Film Manufacturing Company, has never been issued for home use.  The only known copy of the film is held by the  , but only three of the five reels are owned by MoMA.  Baum scholars Hearn and David Moyer presented the film with commentary.

===Cast===
*Kāra, the Last Egyptian: J. Farrell MacDonald
*Winston Bey: Howard Davies
*Tadros the Dragoman: J. Charles Haydon
*Nepthys: Jane Urban
*Princess Hatatcha: Mai Wells
*Sebbet the Embalmer: Fred Woodward Vivian Reed Frank Moore
*Viscount Consinor Jefferson Osborne
*Tilga, Keeper of the Harem: Louise Emmons|Mrs. Emmons
*Mrs. Everingham: Ora Buckley
*Sheik Antar: Pierre Couderc

==References==
 
This article written with a third printing of the Stern edition.
*Richard Mills and David L. Greene. "The Oz Film Manufacturing Company, Part Three."  The Baum Bugle, Autumn 1973.
*  at matildajoslyngage.org

==External links==
*Read   on Google Books
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 