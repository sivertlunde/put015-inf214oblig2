Love Shines
 
 
{{Infobox film
| name        = Love Shines
| image       = RonSexsmith_LoveShines_poster_web.gif
| caption     = Love Shines poster
| director    = Douglas Arrowsmith
| writer      = Douglas Arrowsmith
| producer    = Paperny Entertainment
| starring    = Ron Sexsmith
| distributor = Paperny Entertainment
| released    =  
| runtime     = 89 minutes
| country     = Canada
| language    = English
}}
Love Shines is a documentary film about Canadian songwriter Ron Sexsmith by filmmaker Douglas Arrowsmith. The film is produced by Paperny Entertainment and commissioned by The Movie Network and Movie Central with funding from Astral Medias Harold Greenberg Fund and the Rogers Documentary Fund.

It was nominated for three Canadian Screen Awards in 2013 for Best Performing Arts Program or Series or Arts Documentary Program or Series, Best Direction in a Performing Arts Program or Series, and Best Picture Editing in a Documentary Program or Series.

==Synopsis==
Love Shines follows Ron Sexsmith as he attempts to turn his niche following into mainstream success by recording his album Long Player Late Bloomer with legendary producer Bob Rock.

==Release dates== Hot Docs Festival in Toronto.  It debuted on television on BBC Four in March 2011 and on HBO Canada in May 2011.

==Subjects interviewed==
*Ron Sexsmith - singer, songwriter
*Steve Earle - singer, songwriter and producer
*Elvis Costello - singer, songwriter
*Leslie Feist - singer, songwriter
*Tony Ferguson - vice-president of Interscope Records
*Daniel Lanois - producer (U2, Bob Dylan, Emmylou Harris)
*Bob Rock - producer (Metallica, Bon Jovi, Mötley Crüe) Rob Bowman - Grammy Award-winning professor, author and critic
*Kiefer Sutherland - actor, co-owner of Ironworks Studios
*Michael Dixon  - Ron Sexsmith’s manager
*Colleen Hixenbaugh - Ron Sexsmith’s wife
*Christopher Sexsmith - Ron Sexsmith’s son
*Dorothy Grodesky - Ron Sexsmith’s mother

==See also==
*Bob Rock
*Long Player Late Bloomer
*Paperny Entertainment

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 

 
 