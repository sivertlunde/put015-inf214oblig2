The Lone Wolf Keeps a Date
{{Infobox film
| name           = The Lone Wolf Keeps a Date
| image          = The lone wolf keeps his date poster.jpg
| image_size     = 
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Jack Joseph Spence
| producer       = 
| writer         = 
| screenplay     = Sidney Salkow Earl Felton
| story          = 
| based on       =   
| narrator       = 
| starring       = Warren William Frances Robinson
| music          = 
| cinematography = Barney McGill
| editing        = Richard Fantl
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       = January 1941
| runtime        = 65 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Lone Wolf film produced by Columbia Pictures. It features Warren William in his fourth performance as the title detective Lone Wolf, and Edward Gargan, Lester Matthews and Don Beddoe as the films antagonists. The film was directed by Sidney Salkow and written by Salkow and Earl Felton.

The film centres on former jewel looter Michael Lanyard, also known by his alias "Lone Wolf", aiming to recover his stamp collection as well as rescuing a damsel in distress. Filming took place in August and September 1940. The Lone Wolf Keeps a Date was theatrically released in the United States in January 1941.

==Plot==
After admonishing his butler Jamison (Eric Blore) for conning money and adding a rare Cuban stamp to his coveted collection, former jewel looter and current detective Michael Lanyard (Warren William, also known as the Lone Wolf, flies back to Miami from Havana. He meets the gorgeous Patricia Lawrence (Frances Robinson) on board the plane. Initially shy, Lawrence decides to confide in Lanyard about her troubles — one of her mail-sender boyfriend Scottys (Bruce Bennett) clients was killed some time ago after assigning Scotty to send a package stashed with dollar notes. Descending at the Miami airport, they are ambushed by kidnappers Chimp (Edward Gargan), Mr. Lee (Lester Matthews), and Big Joe Brady (Don Beddoe). The swift Lone Wolf outruns the criminals with Lawrence. He hides the retrieved stack of money in a hotel safe, but is discovered by Inspector Crane (Thurston Hall) and his assistant Dickens (Fred Kelsey), along with Miami police head Captain Moon (Jed Prouty). Lanyard evades capture and sets out to arrest the three villains on his own. The detective also realises that his prized stamp collection has also been swiped away by Big Joe Brady. He manages to track them down and has the police arrest them. Exonerated, the Lone Wolf mulls over missing an important stamp convention. 

==Production==
The title character Lone Wolf was played by Warren William — his fourth time doing so. Eric Blore continued playing Lanyards butler Jamison.  Although Walter Baldwin is credited as playing a "Night watchman" in the film,  he actually did not appear in it. 

Sidney Salkow served as director of the film for the production company and distributor Columbia Pictures, while Salkow and Earl Felton wrote the screenplay, based on a fictional detective novel by Louis Joseph Vance. Barney McGill signed on as cinematographer. Morris Stoloff headed the musical direction, and Richard Fantl edited the film. Principal photography officially began on August 21, 1940, and ended halfway into September 1940. 

==Release==
The film was officially released in North American cinemas in January 1941. It is alternatively referred to as Revenge of the Lone Wolf and Alias the Lone Wolf.  In his 2010 book Mystery Movie Series of 1940s Hollywood, Ron Backer wrote that the film "is the best of the Warren William Lone Wolf movies" although "it seems to lack that certain something that made the earlier Lone Wolf movies so entertaining". He offered in his conclusion that it made "a good entry in the Lone Wolf series, with less sexual violence this time round".  In contrast, Leonard Maltin wrote for his Movie & Video Guide (1998) that the film was "listless". 

==References==
 

==Bibliography==
*  }}
*  }}
*  }}

==External links==
*  
 
 
 
 
 
 