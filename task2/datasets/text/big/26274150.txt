Return to Yesterday
{{Infobox film
| name           = Return to Yesterday
| image          = 
| image_size     = 
| caption        =  Robert Stevenson
| producer       = S.C. Balcon Robert Stevenson
| narrator       = 
| starring       = Clive Brook   Anna Lee   Dame May Whitty
| music          = Ernest Irving
| cinematography = Ronald Neame Charles Saunders
| studio         = Ealing Studios
| distributor    = Associated British Film Distributors 
| released       = 9 March 1940
| runtime        = 69 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Robert Stevenson. It stars Clive Brook and Anna Lee.  It was based on the play Goodness, How Sad by Robert Morley. The film was made at Ealing Studios.

==Synopsis== 
A British Hollywood star disappears and returns to the small seaside town where he had once worked as a struggling actor at the local theatre. Without anyone realising who he is, he agrees to appear in the theatres latest production and falls in love with the leading lady.

==Cast==
* Clive Brook as Robert Maine
* Anna Lee as Carol Sands
* Dame May Whitty as Mrs. Emily Truscott
* Hartley Power as Regan
* Milton Rosmer as Fred Sambourne
* David Tree as Peter Thropp  
* Olga Lindo as Grace Sambourne  
* Garry Marsh as Charlie Miller  
* Arthur Margetson as Osbert  
* Elliott Mason as Mrs Priskin  
* O. B. Clarence as Truscott   David Horne as Morrison  
* Frank Pettingell as Prendergast
* Ludwig Stössel as Captain Angst 
* Wally Patch as Night Watchman 
* H.F. Maltby as Inspector 
* Mary Jerrold as Old lady at station 
* Alf Goddard as Attendant  John Turnbull as Stationmaster 
* Patric Curwen as Jim, the Guard 
* Eliot Makeham as Fred Grover 
* Mollie Rankin as Christine Lawford
* Bruce Seton as Journalist 
* Peter Glenville as Minor role

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 