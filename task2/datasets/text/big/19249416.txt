The People Speak (film)
{{Infobox film
| name = The People Speak
| image = DVD cover of The People Speak.jpg Chris Moore Anthony Arnove Chris Moore Anthony Arnove Howard Zinn
| writer = Howard Zinn Anthony Arnove
| released =  
| country = United States English
}} 
The People Speak is a 2009 American documentary feature film that uses dramatic and musical performances of the letters, diaries, and speeches of everyday Americans. The film gives voice to those who, by insisting on equality and justice, spoke up for social change throughout U.S. history and also illustrates the relevance of this to todays society. 

The film is narrated by historian Howard Zinn and is based on his books A Peoples History of the United States and, with Anthony Arnove, Voices of a Peoples History of the United States. 
 Chris Moore, Anthony Arnove, and Howard Zinn. It is co-directed by Moore, Arnove and Zinn.

==Production==
The movie was shot on location in Boston, Massachusetts|Boston, in front of live audiences at Emersons Cutler Majestic Theatre in January 2008 and at Malibu Performing Arts Center, Malibu, California|Malibu.

==Cast== Chris Robinson, Darryl "D.M.C." McDaniels, David Strathairn, Don Cheadle, Eddie Vedder, Harris Yulin, Jasmine Guy, John Legend, Josh Brolin, Kathleen Chalfant, Kerry Washington, Lupe Fiasco, Marisa Tomei, Martín Espada, Matt Damon, Michael Ealy, Mike OMalley, Morgan Freeman, P!nk, Qorianka Kilcher, Reg E. Cathey, Rich Robinson, Rosario Dawson, Sandra Oh, Sean Penn, Staceyann Chin, Viggo Mortensen

==Screenings==
Clips from the film were screened at the  .   In November 2009, the movie premiere was held at Jazz at Lincoln Center in New York.
 The History Channel on Sunday, December 13, 2009.

==Reviews==
The film was well received by the critics. The Los Angeles Times  
described it as "Striking, exhilarating... the performances are thrilling”. According to the Boston Globe, "The documentary... works beautifully... Each passionate reading flows out of the previous one." 

USA Today   reported Damon as saying: "This is the perfect format for a history lesson. You’re getting the actual historical text verbatim, so there’s no spin, performed by these great actors. History is intimidating. There’s so much to know. If I could go back to college again, I would be a history major." According to the newspaper, Springsteen taped at his ranch in New Jersey while Brolin was at work and Brolin said: “They ended up spending five or six hours. And I got this little card from Bruce Springsteen that said: ‘Josh, thank you so much for making my childhood dream come true. I had the greatest day of my life.’"

==Soundtrack== Taj Mahal, Allison Moorer, The Black Crowes Rich Robinson and Xs Exene Cervenka and John Doe) at the Malibu Performing Arts Center, where they perform both vintage and recent protest-music classics.
 Do Re Mi”; and two early Dylan songs “Masters Of War” and “Only A Pawn In Their Game” are performed by Eddie Vedder and Rich Robinson, respectively.
 Sail Away”, 2006 debut album; Pinks “Dear Mr. President,” heard on her album of that same year; and Brownes “The Drums Of War”, which debuted on his 2008 album Time the Conqueror.
 Taj Mahal. More info at: www.peopleshistory.us/news/people-speak-soundtrack-CD-on-Verve  

==The People Speak – International==

===The People Speak UK===
 A Single Chris Moore. Like the original, the UK version draws on writings that have influenced British history and it includes performances by British screen and stage actors. These include Ian McKellen, Joss Stone, Saffron Burrows, Mark Strong, Celia Imrie, Noel Clarke, Sir Ben Kingsley, Mark Steel, Stephen Rea and others. 

The documentary aired on History UK on October 31, 2010.

===The People Speak Australia=== The History Channel Australia on 9 July 2012 at  Carriageworks in Redfern, Sydney in front of a live audience.   It was produced by WTFN Entertainment and directed by Phillip Tanner.
 Jack Thompson, John Jarratt, Leah Purcell, Magda Szubanski, Rebecca Gibney, Ryan Kwanten, Sam Worthington. There were musical performances by Christine Anu, Julia Stone and Tex Perkins. Narration was by Thomas Keneally.    
 The History Channel 2 December 2012. 

Other international versions of A People’s History are also being developed. These will be based on the same format, exploring peoples histories in other countries.

==The People Speak – Education==
An educational version of The People Speak is in production, in association with Voices of a Peoples History of the United States.

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 