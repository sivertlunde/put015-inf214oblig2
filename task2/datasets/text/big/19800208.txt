Wedding Worries
{{Infobox film
| name           = Wedding Worries
| image          =
| caption        = Edward Cahn
| producer       = Metro-Goldwyn-Mayer
| writer         = Hal Law Robert A. McGowan
| narrator       =
| starring       =
| music          =
| cinematography = Jackson Rose
| editing        = Leon Borgeau
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 10 51"
| country        = United States
| language       = English
| budget         =
}}
 short comedy Edward Cahn.  It was the 202nd Our Gang short (203rd episode, 114th talking short, 115th talking episode, and 34th MGM produced episode) that was released.

==Plot==
Having read horror stories about wicked stepmothers, the gang is determined to break up the marriage between Darla Hoods widowed father and his new bride. Never bothering to find out, as Darla has, that the second Mrs. Hood is a wonderful woman, the kids pull off all sorts of pranks at the wedding ceremony, from playing the radio too loud to releasing a cylinder of laughing gas. The wedding guests start smiling then laughing as the gas fills the room. Someone in the building discovers what theyre doing and shuts off the canister. The wedding is temporarily postponed and the gang is sentenced to a spanking, assembly-line style.   

==Notes==
Wedding Worries marked the final appearance of long-time Our Gang sweetheart Darla Hood, after five years of service.

==Cast==
===The Gang=== Mickey Gubitosi (later known as Robert Blake) as Mickey
* Darla Hood as Darla Hood
* Billy Laughlin as Froggy
* George McFarland as Spanky
* Billie Thomas as Buckwheat

===Additional cast=== Barbara Bedford as Miss Douglas
* Margaret Bert as Delia the housekeeper
* Chester Clute as Judge Martin
* Sid DAlbrook as Mickeys father
* Ben Hall as Froggys father William Irving as Guest
* Jack Lipson as Guest
* Stanley Logan as Father of the bride
* Byron Shores as Doctor James B. Hood
* Joe Young as Best man

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 