That Touch of Mink
{{Infobox film
| name           = That Touch of Mink
| image          = Touch of Minkposter.jpg
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Delbert Mann Robert Arthur Martin Melcher Stanley Shapiro
| writer         = Stanley Shapiro Nate Monaster
| starring       = Doris Day Cary Grant
| music          = George Duning
| cinematography = Russell Metty
| editing = Ted J. Kent
| distributor    = Universal Pictures
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| gross          = $17,648,927  . The Numbers. Retrieved June 13, 2013. 
}}

That Touch of Mink is a 1962 romantic comedy starring Cary Grant and Doris Day, and directed by Delbert Mann. The film co-stars Gig Young, John Astin, Audrey Meadows, and Dick Sargent. In addition, baseball stars Mickey Mantle, Roger Maris, and Yogi Berra make cameo appearances.

== Plot == Rolls Royce splashes her dress with mud while she is on her way to a job interview.  

Philip wants an affair, while Cathy is holding out for marriage. Watching from the sidelines are Philips financial manager, Roger, who sees a therapist because he feels guilty about helping his boss with his numerous conquests, and Cathys roommate, Connie Emerson, who knows what Philip is after.

Philip wines and dines Cathy. He takes her to see the New York Yankees play. They watch from the Yankees dugout (he owns part of the team). Cathys complaints about the umpire while seated alongside Mickey Mantle, Roger Maris and Yogi Berra (playing themselves), cause Art Passarella to throw all of them out of the game. 

Philips conscience weighs on him, so he withdraws an invitation to Bermuda, which only serves to make Cathy agree to go. While in Bermuda, Cathy comes down with a nervous rash, much to her embarrassment and his frustration.  

The Bermuda trip is repeated, but this time Cathy drinks to soothe her nerves and ends up drunk. While intoxicated, Cathy falls off the balcony onto an awning below. She is then carried in her pajamas through the crowded hotel lobby.

At the urging of Roger and Connie who are convinced that Philip is in love with her, Cathy goes on a date with Beasley, whom she dislikes, to make Philip jealous. Her plan succeeds and she and Philip get married. On their honeymoon, he breaks out in a rash.

==Cast==
 
* Cary Grant as Philip Shayne
* Doris Day as Cathy Timberlake
* Gig Young as Roger
* Audrey Meadows as Connie
* John Astin as Beasley
* Alan Hewitt as Dr. Gruber, Rogers therapist
* Dick Sargent as Harry Clark
* Joey Faye as Short Man
* Laurie Mitchell as Showgirl
* John Fiedler as Mr. Smith
* Willard Sage as Tom Hodges
* Jack Livesey as Dr. Richardson
* Yogi Berra as Himself
* Mickey Mantle as Himself
* Roger Maris as Himself
* Dorothy Abbott as Stewardess (uncredited) Richard Deacon as Mr. Miller (uncredited)
* William Lanteau as Leonard (uncredited)
* Ralph Manza as Cab Driver (uncredited)

 

==Reception==
===Box office performance=== theatrical rentals. 4th highest grossing film of 1962.

===Awards=== Robert Clatworthy, George Milo), Best Sound (Waldon O. Watson) & Best Writing, Story and Screenplay — Written Directly for the Screen (Stanley Shapiro, Nate Monaster).      
*Won the Golden Globe for Best Comedy Picture and Cary Grant was nominated for Best Motion Picture Actor — Musical/Comedy.
*Won the Golden Laurel for Top Comedy, while Doris Day won for Top Female Comedy Performance, Cary Grant for Top Male Comedy Performance and Gig Young for Top Male Supporting Performance.
*Won the Writers Guild of America Award for Best Written American Comedy.

===Production notes===
* Cary Grant was a big fan of The Honeymooners and Audrey Meadows in particular, and was responsible for getting her the part of Connie. 

* In her autobiography, Doris Day wrote that Cary Grant was very professional and exacting with details, helping her with her wardrobe choices for the film and decorating the library set with his own books from home. However, he was a completely private person, totally reserved, and very distant. Their relationship on this film was amicable but totally devoid of give-and-take. 
 Lover Come Back, and Send Me No Flowers.  In fact, Rock Hudson had expected to be cast as Philip, but director Delbert Mann wanted Cary Grant.  Grant hated the finished film.

==References==
 

==External links==
*  
* 
* 

 
 

 
 
 
 
 
 
 
 
 