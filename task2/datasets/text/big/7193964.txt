Veta
 
{{Infobox film
| name           = Veta
| image          =
| image size     =
| caption        =
| director       = A. Kodandarami Reddy
| producer       = Dhananjay Reddy   Dinesh Salgia (Hindi version)
| writer         = Paruchuri Gopala Krishna   Paruchuri Venkateswara Rao   Tripathi S.N. (dialogue: Hindi version)
| starring       = Chiranjeevi   Jayaprada
| music          = K. Chakravarthy
| cinematography = Swamy V.S.R.
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
| gross          =
}} Telugu film Starring Chiranjeevi, Jayaprada and Sumalatha in the lead roles. The film was made by Samyuktha Movies whose earlier blockbuster was Khaidi. The movie is based on Alexandre Dumas The Count of Monte Cristo. The successful team of director A. Kodandarami Reddy, composer K. Chakravarthy and writers Paruchuri brothers was repeated in this movie as well. The movie was dubbed into Hindi as Farrar Qaidi.

==Plot==
Chiranjeevi is a sailor working on a ship during the British rule.
He is in love with a wealthy woman Jayaprada and her bava, who works for the British army also wants to marry her.
When the captain of the ship (C.S.Rao) is poisoned and killed, Jayaprada’s bava uses his power to put Chiru away for good at the secluded andaman jail.
Chiru is trapped in this nightmare that lasts for thirteen years.
Haunted by the baffling course his life has taken, over time everything he ever believed about right and wrong is abandoned and replaced by all-consuming thoughts of vengeance against those who betrayed him. With the help of an equally innocent fellow inmate (Jaggaiah), who was once a Jamindar that opposed the British only to lose his wealth, wife and daughter (Sumalatha).
Chiru plots and succeeds in his mission to escape from prison, whereupon he transforms himself into the mysterious and wealthy Jamindar.
He finds the daughter of Jaggayya and shelters her with him. With cunning ruthlessness, he cleverly insinuates himself into the Highclass nobility and systematically destroys the men who manipulated and enslaved him.

== Cast ==
* Chiranjeevi
* Jayaprada
* Kongara Jaggayya
* Sumalatha Ranganath
* Nutan Prasad

== Crew ==
* Director: A. Kodandarami Reddy
* Writers: Paruchuri Brothers
* Producers: Dhananjay Reddy & Dinesh Salgia (Hindi version)
* Music: K. Chakravarthy
* Cinematography: V.S.R. Swamy
* Editing: Vellaiswamy Veturi
* Playback singers: S.P. Balasubramaniam, K.J. Yesudas & P. Susheela
* Choreography: Tara

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 