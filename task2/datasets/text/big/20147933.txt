Oatsurae Jirokichi Koshi
{{Infobox Film
| name           = Oatsurae Jirokichi Koshi
| image          = 
| image_size     = 
| caption        =  Daisuke Itō
| producer       = Nikkatsu
| writer         = 
| narrator       = Shunsui Matsuda Midori Sawato
| starring       = Denjirō Ōkōchi Naoe Fushimi Nobuko Fushimi Minoru Takase
| music          = 
| cinematography = Hiromitsu Karasawa
| editing        = 
| distributor    = Digital Meme
| released       =   1931 (Japan)
| runtime        =61 min
| country        = Japan Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Daisuke Itō. It is the only completely preserved silent film directed by Ito and related the life of a legendary thief, Jirokichi the Rat, in an exquisite original story and through the revolutionary use of dynamic intertitles. The trio composed  of director Ito, top-class cinematographer Karasawa and lead actor Denjirō Ōkōchi won great popularity for their masterful work.

==External links==
* 

 
 
 


 