The Short and Curlies
{{Infobox film
| name           = The Short and Curlies
| image          = "The_Short_and_Curlies".jpg
| image size     = 
| alt            = 
| caption        = David Thewlis
| director       = Mike Leigh
| producer       = Simon Channing-Williams Victor Glynn
| writer         = Mike Leigh
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Alison Steadman Wendy Nottingham Sylvestra Le Touzel David Thewlis
| music          = Rachel Portman Roger Pratt
| editing        = Jon Gregory
| studio         = 
| distributor    = 
| released       = 1987
| runtime        = 18 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          =
}} High Hopes (with music by Rachel Portman).

The short, 18-minute film, made after three weeks of rehearsal, concerns a chatty hairdresser, Betty (Alsion Steadman), her shy daughter, Charlene (Wendy Nottingham), and one of her customers, Joy (Sylvestra Le Touzel). Joy works in a chemists shop and is chatted up by Clive (David Thewlis, in the first of his three Leigh roles) over the Durex counter.  "On the one hand there is ritual, physical indignity, rubber, prevention; on the other, new life, love , marriage. Charlene slips sadly and silently between two stools..." 

==Notes==
Alison Steadman, who has a hairdresser sister, said later that she loved playing Betty: "If Mike said I could do one of my parts again, thats the one I would choose. I loved her from the word go, the minute I got the curlers and the scissors in my hands. It just clicked. Sylvestra is a terrible giggler, and we just giggled the whole time, it was terrible. Great fun though." 

Thewliss Clive is an incipient motormouth – though not in the same class as Johnny in Naked (1993 film)|Naked – with a line in bad jokes; Whats round and really violent? A vicious circle.

==Reception==
The Short and Curlies was nominated for a BAFTA Award for Best Short Film. 

==References==
 

==External links==
* 

 
 
 
 
 
 


 