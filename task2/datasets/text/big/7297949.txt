The Saint of Gamblers
 
 
{{Infobox film
| name           = The Saint of Gamblers
| image          = 
| caption        = 
| film name = {{Film name| traditional    = 賭聖２之街頭賭聖
 | simplified     = 赌圣2之街头赌圣
 | pinyin         = Dǎo Xéng Ěr Zhī Jiā Toū Dǎo Xéng
 | jyutping       = Dou2 Sing3 Ji6 Zi1 Gaai1 Tau Dou2 Sing3}}
| director       = Wong Jing
| producer       = Wong Jing
| writer         = Wong Jing
| starring       = Eric Kot Chingmy Yau Ng Man Tat Diana Pang Natalis Chan Donnie Yen
| music          = Lee Hon Kam Marco Wan
| cinematography = Andrew Lau
| editing        = Ma Go
| studio         = Wong Jings Workshop Ltd. Golden Harvest
| released       =  
| runtime        = 98 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK $13,111,400
}} Hong Kong film directed by Wong Jing. It is a spin-off of the All for the Winner series, with only Ng Man Tat reprising his role.

==Cast==
* Natalis Chan - Announcer
* John Ching - Tung
* Eric Kot - God Bless You Ben Lam - Ray Thai
* Ng Man-tat - Uncle Tat
* Diana Pang - Hokei
* Shing Fui-On - Assassin
* Ashton Chen - Siu-Loong
* William Tuen - Japanese gambler
* Manfred Wong - Dwarf
* Chingmy Yau - Yuen-Fan
* Donnie Yen - Lone Seven
* Corey Yuen - Mahjong player

==See also==
* God of Gamblers, a 1989 Hong Kong film

==External links==
*  

 
 

 
 
 
 

 