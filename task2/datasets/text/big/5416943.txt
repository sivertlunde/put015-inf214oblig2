A Night in the Life of Jimmy Reardon
{{Infobox film
| name = A Night in the Life of Jimmy Reardon
| image = A Night in the Life of Jimmy Reardon VideoCover.jpg
| caption = DVD cover
| image_size = 250px
| director = William Richert
| producer = Mel Klein Noel Marshall Russell Schwartz
| screenplay = William Richert
| based on =   Louanne Matthew Matthew L. Perry Paul Koslo
| music = Bill Conti  (US version)  Elmer Bernstein  (non US version) 
| cinematography = John J. Connor
| editing = Suzanne Fenn
| studio = Island Pictures
| distributor = 20th Century Fox
| released = February 26, 1988
| runtime = 90 minutes
| country = United States
| language = English
| budget = $5 million
| gross = $6,264,058
}} coming of age drama film written and directed by William Richert and starring River Phoenix, Ann Magnuson, Meredith Salenger, Matthew Perry and Ione Skye. It is based upon the novel Arent You Even Gonna Kiss Me Goodbye?, also by William Richert. The story centers on a high school graduate who must find out if he wants to go to business school at the request of his father or go his own way and get a full-time job, while also deciding on who he wants to be in life and if he should leave his house.

A Night in the Life of Jimmy Reardon was filmed in 1986 and released in 1988. The released film deviates considerably from the original directors cut, which is now available under the title Arent You Even Gonna Kiss Me Goodbye?.

== Plot summary ==
Set in a wealthy Chicago suburb during the 1960s, middle-class Jimmy Reardon (River Phoenix) hangs out with his upper-class best friend, Fred Roberts (Matthew Perry), and sleeps with Freds snobby girlfriend, Denise Hunter (Ione Skye). He spends his time writing poetry and drinking coffee while he decides what to do after high school. His parents wont help him pay for tuition unless he attends the same business college as his father did, but Jimmy doesnt want to follow that path. Instead, he focuses on coming up with enough money for a plane ticket to go to Hawaii with his wealthy yet chaste girlfriend, Lisa Bentwright (Meredith Salenger). On the night of a big party, Jimmy is given the task of driving home his mothers divorced friend, Joyce Fickett (Ann Magnuson), who conveniently seduces him. Since he is late picking up Lisa, she goes to the dance with the rich Matthew Hollander (Jason Court) instead. Jimmy then crashes the family car and shares an intimate rapproachment with his father (Paul Koslo) while becoming a hero.

== Cast ==
* River Phoenix as Jimmy Reardon
* Ann Magnuson as Joyce Fickett
* Meredith Salenger as Lisa Bentwright
* Ione Skye as Denise Hunter
* Louanne Sirota as Suzie Middleberg
* Matthew Perry as Fred Roberts
* Paul Koslo as Al Reardon
* Jane Hallaren as Faye Reardon
* Jason Court as Mathew Hollander
* James Deuter as Mr. Spaulding
* Marji Banks as Emma Spaulding Margaret Moore as Mrs. Bentwright
* Anastasia Fielding as Elaine
* Kamie Harper as Rosie Reardon
* Johnny Galecki as Toby Reardon
* Alan Goldsher as Musician #2

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 