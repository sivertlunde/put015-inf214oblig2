Dialogues with Madwomen
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Dialogues with Madwomen
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Allie Light
| producer       = Irving Saraf
| writer         = 
| screenplay     = 
| story          = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Irving Saraf
| editing        = Irving Saraf
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} documentary by Allie Light focusing on mental illness in women. 

==Synopsis==
In Dialogues with Madwomen, filmmakers Allie Light and Irving Saraf have seven "madwomen" — including Light herself — into telling their stories. Using a mixture of home movies, archival footage of psychiatric wards, re-enactments, and interviews with their subjects, Light and Saraf have created a complex, moving portrait of women in whom Depression (mood)|depression, schizophrenia, and multiple personalities coexist with powerful, sometimes inspired levels of creativity.

==See also==
*Atypical antipsychotics, which came onto the market after this film was made
 
==Karen Wong==
In December 2013, a man whose DNA linked him to Karen Wong, one of the seven women in the film, was found guilty and convicted for her murder. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 


 
 