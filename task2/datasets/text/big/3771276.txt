My Bloody Valentine (film)
 
 
{{Infobox film
| name = My Bloody Valentine
| image = My bloody valentineposter.jpg
| caption = Theatrical poster
| director = George Mihalka
| producer = John Dunning André Link Stephen Miller
| story = Stephen Miller
| screenplay = John Beaird Keith Knight Patricia Hamilton Gina Dick Terry Waterland
| music = Paul Zaza
| cinematography = Rodney Gibbons
| editing = Gérald Vansier Rit Wallis
| studio = Secret Film Company
| distributor = Paramount Pictures
| released =  
| runtime = 90 minutes 93 minutes (released uncut version) 99 minutes (complete uncut version)
| country = United States/Canada
| language = English
| budget = $CAD2,300,000
| gross = $5,672,031
}}
My Bloody Valentine is a 1981 slasher film directed by George Mihalka and written by John Beaird. The film tells the story of a group of teenagers who decide to throw a Valentines Day party only to incur the vengeful wrath of a maniac in mining gear who begins a murder spree, and stars Paul Kelman, Lori Hallier and Neil Affleck.
 slasher genre Friday the 13th (1980), the movie was filmed on location in Sydney Mines, Nova Scotia, Canada. 
 MPAA due remake into theaters).

==Plot==
Two miners in full gear, including face masks, are seen going down into a deep mine shaft. They reach an alcove and one of the figures takes off half of their gear, revealing to be a woman. As the other miner refuses to remove his gear, he sticks his mining pick in a wall, and she does a strip tease, fondling the mans mask and breathing tubes in a sexual manner, resulting the miner killing her by impaling her chest with the picks blade.

The small town of Valentine Bluffs is seen preparing for a Valentines Day dance. The town is home to a coal mine where many of the local men work, both young and old. Hanniger, the town Mayor, discusses the Valentines Day dance with a woman named Mabel Osborne, who runs a laundromat. Hanniger makes a vague reference to a past tragedy associated with Valentines Day, and revealing that this is the first Valentines dance to be held in decades. 

20 years before, a mining accident trapped five men underneath the ground. The accident was due to the negligence of two supervisors who chose to go to the Valentines dance instead, leaving their posts while the men were still below and having failed to check for dangerous levels of methane gas in the mine. During this time, four of them died and one named Harry Warden survived via cannibalism and was rescued. A year later, Harry returned for revenge on the two supervisors responsible for the accident, by killing them with his mining pick and cutting out their hearts, leaving them in decorative Valentine boxes and warning the town to never hold another Valentines dance ever again. The warning had been heeded for 20 years until the legend of Harrys threat becomes a distant memory, and the town tries to put the past behind it by having another dance since Harry Warden was locked up in an insane asylum since the murders.

The younger people of the town are excited about it. Three of these young people are involved in a love triangle: T.J. Hanniger (Paul Kelman), the Mayors son, has recently returned from an unexpected departure where he tried to leave Valentine Bluffs behind, including his girlfriend Sarah (Lori Hallier). In the meantime, Sarah has begun a relationship with T.J.s friend Axel (Neil Affleck). Sarah still has feelings for T.J., infuriating Axel and causing tension between the two men.

Meanwhile, Mayor Hanniger (Larry Reynolds) and the towns chief of police, Jake Newby (Don Francks), get an anonymous box of Valentines chocolates, and when they open it, it contains a human heart, with a note to prevent the dance from ever happening, vowing to kill more people until its stopped. Hanniger and Newby are anxious about what to decide. 

That evening, Mabel (Patricia Hamilton) is attacked by a man dressed in mining gear, who corners her in the laundromat and murders her with a mining pick. The next morning, Jake finds her body stuffed inside one of the dryers, with her heart removed and her skin scorched from the heat. Newby tries to hush up the incident in the hopes of avoiding a panic, releasing information that Mabel died of a heart attack. Newby phones the mental institution where Harry Warden was incarcerated, but there is no trace of him there; his records are lost and his contact cannot tell Newby where Harry is or whatever became of him. Newby and Hanniger fear that Harry Warden has come back to continue his killing spree, and their worst fears seem to be confirmed when the coroner tells them the heart in the box was indeed, the heart of the woman from the mine; approximately 30 years old. With the death of Mabel, the dance is cancelled and the union hall is locked up. T.J. and his group of friends gather at the local bar, where they hear stories from the bartender about Harry Warden and his rampage. They scoff at him, angering the bartender Happy (Jack Van Evera), who overhears them planning on holding their party in the mine afterwards. After they leave, Happy sets up a dummy to scare them, but as Happy leaves, the real miner lunges and kills him by driving his mining pick into his head.
 
The following night, when the large group of young revelers arrives at the mine, T.J. and Axel come to blows over their rivalry. A young man, named Dave (Carl Marotte), is murdered in the kitchen, drowned and scalded in a pot of boiling water by Harry and later cuts out his heart and throws it in the boiling water, where it cooks alongside a large batch of beef franks. One of the girls finds it later, thinking it to be a gag, and Daves body initially goes unnoticed in the freezer of the kitchen. Newby also gets a hint of trouble when another candy box, apparently containing another blood-soaked heart, arrives at the police building with a menacing note, explaining that the party did not end. The chief, unaware of the party at Hanniger mine, can only ask about the party. A young couple named John (Rob Stein) and Sylvia (Helene Udy) are making out in the shower area of the mine facility. When John leaves to go get beer, Sylvia is attacked by Harry, who first traps her by dropping miners coveralls from the ceiling, and then impaling her head on a shower nozzle. John returns to find Harry gone and Sylvia hanging impaled from the shower nozzle, shocking him.
 Keith Knight) allows some of the others to convince him to take them down into the mine. He heads down there with his girlfriend Patty (Cynthia Dale), Sarah, Howard (Alf Humphreys), and another couple, Michael (Thomas Kovacs) and Harriet (Terry Waterland). After they descend into the mine, the bodies of Sylvia and Dave are discovered. Gretchen (Gina Dick), Howards love interest, finds Dave dead in the freezer, and then John runs in telling everyone about Sylvias murder. Axel warns that Harry Warden is probably responsible and is prowling the premises, and the party disperses in a panic. T.J. and Axel find out that the others went down into the mine, and they take an elevator down to warn them and lead them to safety. While down there, Michael and Harriet go off into the engine room to make love, and Warden impales them with a large drill bit. Newby runs into a frightened Gretchen who tells him that Dave and Sylvia have been murdered by Warden. Newby tells Gretchen and the others to all go home, lock their doors, and not leave home. Hollis discovers the bodies and is attacked by Warden, who uses a nail gun to his head. Patty and Sarah discover him and they catch a glimpse of Harry, who disappears into a tunnel. Howard runs off in a panic, leaving Patty and Sarah alone.

Axel and T.J. appear shortly thereafter and try to lead the two girls to safety, but the control panel to the mining cart has been tampered with. The elevator has been similarly disabled, so they climb up the service ladder. Halfway up, they are startled when Howards body is hanged on a rope, having Howards head decapitated from his hanging, resulting sprayed blood all over Patty and Sarah. Believing Warden is now above them on the ladder, they retreat. While rounding a bend in the mine shaft, T.J., Sarah and Patty hear Axel scream behind them. When they go back, they find his mining helmet drowning below, thinking he has fallen into a well, which T.J. explains its too deep to traverse. T.J. says Axel is surely lost, and they continue. Above ground at the surface entrance, Newby has learned of Warden reappearing at the mine when he pulls over a car for speeding where three of the party-goers, Tommy, John, and Gretchen tell him about the two murders. Newby rushes off to the mine with the remains of the police support to rescue the survivors and to capture Warden once and for all.

In the mine, Warden steps out from a doorway and kills Patty with a blow to the stomach from his mining pick. Sarah and T.J. fend him off in a series of confrontations and chases, until they face off with him inside a small alcove that leads to an abandoned tunnel. The fight destroys the outdated wooden supports, and the tunnel begins to collapse. Wardens mining pick gets caught in a support beam, and he draws a hunting knife. During the fight, it is revealed that the miner is not Warden, but Axel (Faking his demise so he can continue his spree). A flashback reveals Axel is the son of one of the supervisors murdered by Harry Warden. As a very young boy, Axel was in the room and saw his fathers violent death while hiding underneath the bed. The shock apparently snapped his mind, leaving him unstable, his potential for violence unleashed by the recurrence of the Valentines dance. The entrance to the old tunnel collapses, trapping Axel underneath a great deal of debris, apparently killing him. Newby and the police arrive as T.J. and Sarah start to walk away, but Axel screams; Sarah, who is still emotionally attached to Axel, rushes back to see him. Only his arm is visible, and she holds his hand, but it pulls away from the rubble; on the other side, Axel has amputated his own arm with his hunting knife to free himself from the cave-in. Through a hole in the debris, the others can only see him as he stumbles backwards toward the abandoned tunnel, babbling aloud about Harry Warden and threatening to return to kill everyone. 

As the one-armed Axel runs off to find another way out of the mine, he says "Sarah, be my bloody Valentine!". The film ends as an insane Axel runs deeper into the mine singing "Daddys gone away, Harry Warden made you pay" to himself. The screen fades to black as Harry Wardens laughing can be heard, revealing that hes alive as a ballad for Harry Warden plays over the credits.

==Cast==
 .]]
*Paul Kelman as Jesse "T.J." Hanniger
*Lori Hallier as Sarah
*Neil Affleck as Axel Palmer
*Don Francks as Chief Jake Newby
*Cynthia Dale as Patty Keith Knight as Hollis
*Alf Humphreys as Howard Landers
*Patricia Hamilton as Mabel Osborne
*Gina Dick as Gretchen
*Terry Waterland as Harriet
*Thomas Kovacs as Mike Stavinski
*Larry Reynolds as Mayor Hanniger
*Jim Murchison as Tommy
*Helene Udy as Sylvia
*Rob Stein as John
*Carl Marotte as Dave
*Jack Van Evera as Happy
*Peter Cowper (actor) as Harry Warden/The Miner

==Production==
 
Director George Mihalka, based upon the strength of his earlier movie Pick-Up Summer, was approached by Cinepix Productions, headed by André Link and John Dunning with a two movie contract. Mihalka was asked to direct a horror/slasher story, presented to Dunning by Stephen Miller in mid-1980, and, after Mihalka agreed to direct, John Beaird was bought in to write the screenplay. 

The film was originally entitled "The Secret", however, the producers decided to change it to "My Bloody Valentine", so to overtly reference the holiday trend with which the slasher genre was becoming increasingly popular, through films such as Black Christmas, Halloween and Friday the 13th.

Shooting on My Bloody Valentine began in September 1980, taking place around the Princess Colliery Mine in Sydney Mines, Nova Scotia, which had closed in 1975. Two mines were considered for the setting, the other in Glace Bay, Nova Scotia. The production company decided on the Sydney Mines location due to "the exterior   a dreary, cold and dusty area   no other buildings around it so it looked like it was totally in the middle of nowhere." 

Upon arrival at the town for principal photography, the crew found that the townspeople, unbeknownst to them, had redecorated the mine so to make it more presentable, thus destroying the dark atmosphere that had convinced the production company to base the film there. The cast therefore spent a few days staying in Sydney Mines, encouraged by the director to get a feel for the small-town location.

Mihalka has said since making the movie that the most difficult element of My Bloody Valentine was filming in the mines. Located 2,700 feet underground, filming in the mine was a lengthy process, as, due to limited space in the elevators, it would take an hour to assemble the cast and crew underground. Also, due to the methane levels, lighting had to be carefully planned as the amount of bulbs that could be safely utilised was limited. 
 Prom Night the previous year, provided the soundtrack. Thom Kovacs, Helene Udy, Carl Marotte and Rob Stein had all appeared in Mihalkas earlier Pick-Up Summer.
 John McDermott sang the closing theme,  .

==Censorship==
 
Much has been made of the censorship issues around My Bloody Valentine. For the MPAA to award the movie with an R-rating, cuts were requested to every death sequence in the movie. Even after cutting the movie to match the requirements made by the MPAA, the film was returned with an X-rating and more cuts were demanded. Stills of the trimmed footage were published in Fangoria magazine whilst the movie was still in production, even though the sequences were excised in the theatrical version; even today the complete uncut version has not been released. However, an "uncut" version of the film with three minutes put back in was released in 2009.

There are two reasons that are frequently attributed to the extreme cutting of the film. It has been suggested that Paramount Pictures was keen to remove the offending footage due to the backlash they had received from releasing Friday the 13th the previous year—as a side note, Paramounts Friday the 13th Part 2, which premiered a couple of months after My Bloody Valentine, also suffered extensive cutting, which has never been released.

The second reason, that Mihalka attributes, is that the movie was cut due to the murder of John Lennon in December 1980, stating that there was a major backlash against movie violence in the wake of his death. 

The 2009 DVD reinstates around two and a half minutes of footage back into the movie, which contradicts an earlier claim by director Mihalka that the film had been trimmed by 8–9 minutes. It has been argued that the so-called uncut DVD still has some sequences missing, particularly the double-impalement of Mike and Harriet which the director recalls filming. It is thought that the remaining footage appears to be composed of expository scenes, such as dialogue and other non-violence related material. This is given credence by the fact that Mihalka gave his seal of approval to this release, and a written introduction by him precedes the beginning of the special edition DVD, stating that this version was the way that the film was meant to be seen.
 ONTV is believed to have shown the entire uncut version in 1982, the only time it was seen publicly until 2009.

==Release==
===Critical reception===
 
The movie grossed $6,000,000 at the United States box office upon its theatrical release on February 11, 1981. The movie has a large cult following, and fans of the horror genre now consider it a classic. Critically, My Bloody Valentine received mixed reviews. On Rotten Tomatoes it stands with a 40% critics rating with an average rating of 4.7/10.  
 slasher genre." Popular filmmaker Quentin Tarantino called it his all-time favorite slasher film. 

===Home media releases===
My Bloody Valentine was released to both videotape and LaserDisc in the 1980s. In the United Kingdom, the original pre-certificate video release contained an extra four seconds in the sequence where the killer escapes by cutting off his arm; however, the 1989 release was identical to the R-rated version. Rumours were rife that the film had been issued uncut in the East Asian market, most notably Japan; however, director Mihalka denies the possibility of this.
 April Fools Day in March 2008. Both discs were supplied by Paramount. The third and most recent DVD release was issued on January 13, 2009, the same week as the remake was released in theatres. This version integrates the cut footage back into the film and features two featurettes and optional introductory sequences to the previously missing murder sequences. Two featurettes are also included. Director Mihalka, cast members Lori Hallier, Neil Affleck, Helene Udy and Carl Marotte, composer Paul Zaza and make-up artists Thomas Burman and Ken Diaz are all involved.

===Remake===
On January 16, 2009, shortly after the uncut version of the film was released, a remake titled My Bloody Valentine 3D was released in theaters.

==Popular culture==
The pop-punk band Good Charlotte has a song of the same title.

==References==
 

==External links==
*  
*  
*  
*  
*  
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 