The Return of Dracula
{{Infobox film
| name           = The Return of Dracula
| image          =
| caption        =
| director       = Paul Landres
| writer         = Pat Fielder
| narrator       =
| starring       = Francis Lederer and Norma Eberhardt
| music          = Gerald Fried
| cinematography = Jack McKenzie
| editing        = Sherman Rose
| studio         = produced by Gramercy Pictures, distributed by United Artists
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         =
}} 1958 horror film starring Francis Lederer as Dracula. The female lead, Rachel, is played by Norma Eberhardt.    It is filmed in black and white (with a brief color shot of blood) and directed by Paul Landres. 

== Plot ==
It is set in a small town in California in the 1950s, where Count Dracula arrives in the form of an artist named Belak Gordal (Lederer) who has traveled from Europe to visit his cousin, Cora Mayberry (played by Greta Granstedt). The story revolves around his interaction with Coras daughter, Rachel (Eberhardt).  , Turner Classic Movies website, accessed October 12, 2011  

== Production ==
When shown on television, it was titled The Curse of Dracula.  In the U.K it was released theatrically as The Fantastic Disappearing Man.  Later in 1958, the film Horror of Dracula appeared in theaters in both England and the U.S., and The Return of Dracula would have a lack of attention due to Christopher Lees new stardom as the Count. 

=== Cast ===
*Francis Lederer as Bellac Gordal / Count Dracula
*Norma Eberhardt as Rachel Mayberry
*Ray Stricklyn as Tim Hansen
*John Wengraf as John Merriman
*Virginia Vincent as Jennie Blake
*Gage Clarke as Reverend Doctor Whitfield
*Jimmy Baird as Mickey Mayberry
*Greta Granstedt as Cora Mayberry
*Enid Yousen as Frieda

== Reception ==
=== Lederer reprise Dracula role ===
On 27 October 1971, Francis Lederer reprised his role of Count Dracula on an episode of Night Gallery entitled"The Devil Is Not Mocked". In this story, Dracula tells his grandson how he fought Nazis during World War II.

==References==
 

==External links==
*  

 

 
 
 
 
 
 