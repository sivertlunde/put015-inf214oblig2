Hoodoo for Voodoo
Hoodoo For Voodoo is a 2007 Horror Comedy film from Abyssmal Entertainment and Writer/Director Steven Shea.

== Cast ==
*Brunhilda Zekthi as Danielle
*Chris McDaniel as Squid
*Sacha Crutchfield as Asia
*Garrett Harrison as Blake
*Aaliyah Madyun as Sasha
*Forest Crumpler as Eddy G
*Valensky Sylvain as Bones
*Brittany Messina as Candi
*Kian OGrady as John Hughes
*Linnea Quigley as Queen Marie
*Tiffany Shepis as Ayida
*Debbie Rochon as The Ticketmaster
*Hollie Winnard as The Virgin
*Lloyd Kaufman as Crackhead Charlie

== Festivals and awards ==
*Official Selection - Screamfest FL 2006
*Official Selection - Melbourne Independent Filmmakers Festival 2006
*Official Selection - Halloween Horror Picture Show 2007

*Winner - Best Horror Film - Melbourne Independent Filmmakers Festival
*Winner - Best Original Score - Melbourne Independent Filmmakers Festival

== References ==
* 
* 

== External links ==
* 
* 
* 

 
 

 