The Long Gray Line
{{Infobox film
| name           = The Long Gray Line
| image          = The Long Gray Line 1955 poster.jpg
| image_size     = 220px
| caption        = 1955 Theatrical Poster
| director       = John Ford Robert Arthur
| writer         = Nardi Reeder Campion
| based on       =  
| screenplay     = Edward Hope
| narrator       = Tyrone Power
| starring       = Tyrone Power Maureen OHara
| music          = George Duning
| cinematography = Charles Lawton Jr. Charles Lang
| editing        = William A. Lyon
| distributor    = Columbia Pictures
| released       =  
| runtime        = 138 min.
| country        = United States English
| budget         = $1,748,000 (estimated)
| gross          = $4.1 million (US) 
}}
 Marty Maher. West Point dishwasher to a non-commissioned officer and athletic instructor. Maher was buried there in January 1961. 

Maureen OHara, one of John Ford|Fords favorite leading ladies, plays Mahers wife and fellow Immigration|immigrant, Mary ODonnell. The film costars Ward Bond as Herman Koehler, the Master of the Sword (athletic director) and Armys head football coach (1897-1900), who first befriends Maher. Milburn Stone appears as John J. Pershing, who in 1898 swears Maher into the Army.  Harry Carey, Jr., makes a brief appearance as the young cadet Dwight D. Eisenhower. Philip Carey plays (fictional) Army football player and future general Chuck Dotson.
 Robert Francis appeared before his death at age 25, due to an air crash in which he was the pilot.

== Plot summary == Koehler (Ward Bond), impressed with his boxing skills, wants him as an assistant in athletics instruction.

Marty meets Mrs. Koehlers cook, Mary ODonnell (Maureen OHara), also recently arrived from Ireland.  They marry and settle into a house on campus. Marty becomes a corporal, and Mary saves enough money to bring his father (Donald Crisp) and brother (Sean McClory) to America. Mary becomes pregnant, but the baby dies only hours after birth, and Mary learns that she may never have another child. The cadets become the children they will never have. Over time, Marty continues to earn the love and respect of cadets such as Omar Bradley, James Van Fleet, George Patton and Dwight D. Eisenhower. The Mahers grow close to the family of "Red" Sundstrom, a former cadet killed in World War I.

Years later, Marty is still at West Point, and James "Red" Sundstrom, Jr., along with the sons of others whom Marty had trained, has become a cadet. Later, Mary attempts to view one of the parades she so loves, but her health is poor and she is forced to watch the proceedings from her porch. As Marty is fetching her shawl, she quietly dies. On Christmas Eve, Marty prepares for quiet evening but is joined by a group of cadets. Kitty (Betsy Palmer) arrives with Red, Jr., who has earned his captains bars on the battlefield and wants Marty to pin them on. The film concludes with a full dress parade in Martys honor. As the band plays a series of Irish tunes, all the people Marty loves, both living and dead, join the marching cadets on the field.   

==Cast==
* Tyrone Power as Martin Maher
* Maureen OHara as Mary ODonnell Robert Francis as James N. Sundstrom, Jr.
* Donald Crisp as Old Martin
* Ward Bond as Captain Herman Koehler
* Betsy Palmer as Kitty Carter
* Philip Carey as Charles "Chuck" Dotson (as Phil Carey)
* William Leslie as James Nilsson "Red" Sundstrom Steve Forrest as Sergeant
* Harry Carey, Jr. as Dwight D. Eisenhower
* Patrick Wayne as Abner "Cherub" Overton
* Sean McClory as Dinny Maher Peter Graves as Corporal Rudolph Heinz
* Milburn Stone as Captain John J. Pershing
* Erin OBrien-Moore as Mrs. Koehler (as Erin OBrien Moore)
* Walter D. Ehlers as Mike Shannon
* Willis Bouchey as Major Thomas
* Russell Reeder as Commandant

==Reception==
Variety called The Long Gray Line "..a standout drama on West Point".  Bosley Crowther of The New York Times called the film sentimental but a rich and rousing tribute to West Point, and likens Powers Martin Maher to "Mr.Chips with a brogue". 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 