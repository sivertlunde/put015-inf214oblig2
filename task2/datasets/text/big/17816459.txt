I've Loved You So Long
{{Infobox film
| name           = Ive Loved You So Long
| image          = LovedSoLongPoster.jpg
| caption        = Film poster
| director       = Philippe Claudel
| producer       = Yves Marmion
| writer         = Philippe Claudel
| starring       = Kristin Scott Thomas Elsa Zylberstein
| music          = Jean-Louis Aubert
| cinematography = Jérôme Alméras
| editing        = Virginie Bruant
| distributor    = UGC Distribution
| released       =  
| runtime        = 115&nbsp;minutes
| country        = France Canada
| language       = French
| budget         = 
| gross          = US$22,272,176   
}}
Ive Loved You So Long ( ) is a 2008 French-language drama film written and directed by Philippe Claudel. It tells the story of a woman struggling to interact with her family and find her place in society after spending fifteen years in prison.

==Plot== Nancy in Lorraine (region)|Lorraine. Why Juliette was in prison is revealed slowly throughout the film: first, that she was in prison for 15 years, then that her crime was murder, then that the victim was her 6-year-old son Pierre, and finally the reason why she killed him.

Léa, a college professor of literature, is considerably younger than Juliette. Because of the nature of Juliettes crime, their parents denied Juliettes existence and refused to allow Léa to visit her. In addition, Juliette had refused to speak throughout her trial. As a result, Léa knows nothing about the circumstances surrounding the crime and, when pressed for details, Juliette refuses to discuss what happened until the end of the film.

While struggling to find employment, Juliette enjoys platonic companionship with two men, a probation officer who understands how prison can damage the human spirit, and Michel, one of Léas colleagues, who is sympathetic to her ordeal of having been imprisoned. 

Gradually, Juliette begins to fit in with Léa and her family, makes friends, and finds a permanent job as a secretary at a hospital.  She also develops a close relationship with her young nieces, much to the distress of their father, who is concerned about their safety while in their aunts presence.  Slowly, after seeing how she interacts with the family, he begins to accept her.  

Juliette agrees to accompany Léa on a visit to their mother, who is confined to a nursing home with Alzheimers disease. For a brief moment the woman recognizes and embraces her, remembering her as a little girl rather than the estranged daughter who murdered her grandson.

Léa accidentally discovers a clue to why Juliette killed Pierre. Juliette diagnosed her son as suffering from a fatal and painful disease.  Léa confronts Juliette with what she learned, and Juliette explains that, when Pierres condition progressed so that he could barely move, Juliette killed him with an injection, knowing that otherwise he would suffer unbearable pain.  At the trial she spoke no word of defense or explanation, feeling that she deserved punishment for bringing her son into the world, condemned to die. After a cathartic, emotional scene between the two sisters, Léa looks at a window and comments on how beautiful the rain is. Juliette agrees, and the film ends with Juliette saying, "I am here."

==Cast==
* Kristin Scott Thomas as Juliette Fontaine
* Elsa Zylberstein as Léa
* Serge Hazanavicius as Luc
* Laurent Grévill as Michel
* Frédéric Pierrot as Capt. Fauré
* Jean-Claude Arnaud as Papy Paul
* Claire Johnston as Mother
* Lise Ségur as Ptit Lys
* Mouss Zouheyri as Samir

==Theatrical release==
The film premiered at the Berlin International Film Festival on 14 February 2008 and opened in France and Belgium on 19 March. It was shown at the Telluride Film Festival, the Toronto International Film Festival, the Cambridge Film Festival, the Vancouver International Film Festival, and the Chicago International Film Festival before going into limited theatrical release in the US on October 24, when it opened on nine screens and earned US$72,205 on its opening weekend. It eventually grossed US$3,110,292 in the US and US$15,735,514 in foreign markets for a total worldwide box office of US$18,845,806. 

==Critical reception==
Reviews for Ive Loved You So Long were generally positive, earning a 90% freshness rating at Rotten Tomatoes based on 115 reviews as of May 23, 2009.  The consensus stated that Ive Loved You So Long is a sublimely acted family drama as well as a noteworthy directorial debut from Phillipe Claudel. The film fared 79% worth of 28 generally favorable reviews amongst critics at Metacritic. 

===Tone===
Critics noted the films potentially problematic mixture of tones, as it veers between foreboding and sentimentality. A. O. Scott of the New York Times said, "Mr. Claudel’s practice of fading slowly to black between scenes, and the spidery tones of Jean-Louis Aubert’s score, create an atmosphere of mystery and dread that is both appropriate to the story and a little misleading. If I’ve Loved You So Long is not exactly a horror movie, it is nonetheless filled with fear and foreboding . . . This kind of narrative is familiar enough, and so are the risks of sentimental talk-show piety associated with it." He concluded, however, that the film has a "tough-minded resistance to the temptations of melodrama". Scott was not entirely convinced by the films ending. He wrote, "A revelation comes near the end that is both tremendously moving and a bit disappointing, in the way that the solutions to great mysteries frequently are. This turn does not diminish the accomplishment of Ms. Scott Thomas’s deep, subtle and altogether stunning performance, but it does alter the scale of the movie, turning it into a more manageable, less existentially unsettling drama. Which is a relief, I suppose, but also a bit of a letdown."    

Derek Elley of Variety (magazine)|Variety called the film "utterly engrossing despite being, on the surface, about very little" and added, "Claudels script is built out of everyday, unmelodramatic events, succinctly dialogued and not nearly as downbeat as the movie sounds on paper."    Kenneth Turan was even more positive, describing the film as "An example of the French tradition of high-quality adult melodrama, conventional in technique but not story, this thoughtful, provocative film is slow developing because its all about character".   

===Acting and direction===
Critics praised the acting, especially that of Kristin Scott-Thomas. A. O. Scott felt she mitigated the films tonal problems: "Luckily, Ms. Scott Thomas’s furious honesty rules out easy, unearned redemption".  Turan wrote, "When youre doing a film like this, you want the best acting you can get, and writer and first-time director Philippe Claudel chose brilliantly when he picked Kristin Scott Thomas to star as the shattered Juliette . . . Ive Loved You is not without weaknesses . . . but performances this strong and direction this sensitive make us simply grateful to have an emotional story we can sink our teeth into and enjoy." 

Mick LaSalle of the San Francisco Chronicle commented: Kristin Scott Thomas performance . . . is one of a small handful of highlights by which people will remember this year in movies. This is acting at its most exalted. This is film being used for its supreme purpose and function, to show us, moment by moment, the grand movements of a soul. If were lucky, we get one or two gifts like this a year . . . Ive Loved You So Long is worth seeing more than once, just to watch how Thomas scores the performance from beginning to end . . .   plays Juliette as someone with no energy left for pretense . . . At all times, she has about her an aura of sadness and defensiveness . . . None of this is actually spoken in writer-director Philippe Claudels screenplay.    
Elley too found Scott Thomas to be "aces in the lead role, with flashes of mordant wit that prevent it from becoming a dreary study in self-pity." However, he felt that "Zylberstein, a variable actress whos very dependent on her directors, is good here, but lacks Scott Thomas quiet heft and cant quite handle Leas occasional emotional outbursts. Still, the sisters dramatic final talk works just fine." 

The critics also praised Claudels direction. Scott wrote, "Claudel is gratifyingly absorbed in details of setting and character. And even though the unfathomable horror in Juliette’s past dominates everything else, the small felicities and absurdities of real life manage to peek through the gloom."  LaSalle praised his work with the actors: Its the beauty of Claudels design that he is able to suggest the specific nature of Juliettes conflict through pictures, by setting up moments of tension and then generously showing us the face of his lead actress . . . They say a director has to make three great films before he can be called a great. For his debut film, Claudel can check off the first box. He proves himself as adept at controlling a story as he is at directing actors, and his intuitive leap - casting Thomas - was inspired and transformative. He has made Thomas sexy and volatile and has turned her into an actress whose future movies absolutely must be seen.  

===Top tens=== New York David Denby of The New Yorker. 

==Awards and nominations== BAFTA Award for Best Film Not in the English Language (winner)
*BAFTA Award for Best Actress in a Leading Role (Kristin Scott Thomas, nominee)
*BAFTA Award for Best Original Screenplay (nominee) British Independent Film Award for Best Foreign Film (nominee) Chicago Film Critics Association Award for Best Foreign Language Film (nominee)
*César Award for Best First Feature Film (Philippe Claudel, winner)
*César Award for Best Actress in a Supporting Role (Elsa Zylberstein, winner)
*César Award for Best Film (nominee)
*César Award for Best Actress (Scott Thomas, nominee)
*César Award for Best Original Screenplay (nominee)
*César Award for Best Music Written for a Film (Jean-Louis Aubert, nominee) TOFIFEST 2009 (winner) European Film Award for Best European Actress (Scott Thomas, winner)
*Golden Globe Award for Best Foreign Language Film (nominee)
*Golden Globe Award for Best Actress – Motion Picture Drama (Scott Thomas, nominee) London Film Critics Circle Award for Best British Actress of the Year (Scott Thomas, winner)
*Satellite Award for Best Actress – Motion Picture Drama (Scott Thomas, nominee)

==Home media==
The film was released on DVD in France on 24 September 2008, in the UK on 9 February 2009 and in Canada on 10 February. Sony issued it on DVD in anamorphic widescreen format in the US on March 3. It has an audio track in French with English subtitles and an English audio track with Kristin Scott Thomas dubbing her own dialogue. Bonus features include deleted scenes with optional commentary by Philippe Claudel.

==See also==
*Juliet of the Spirits — Juliette is compared to the Fellini film (as Juliette des esprits)

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 