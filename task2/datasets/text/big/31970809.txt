Duniya (1968 film)
{{Infobox film
| name           = Duniya
| image          = Duniyafilm.png
| caption        = Promotional Poster
| director       = Tatineni Prakash Rao|T. Prakash Rao
| producer       = Amarjeet
| story          = K. A. Narayan
| screenplay     = K. A. Narayan Vrajendra Gaur Johnny Walker Lalita Pawar
| music          = Shankar Jaikishan
| cinematography = Faredoon A. Irani 
| editing        = Shivaji Awdhut
| studio         =
| distributor    = 
| released       =  
| runtime        = 149 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          =  180,000,000
}} Johnny Walker, duo with lyrics provided by Hasrat Jaipuri, S. H. Bihari and Gopaldas Neeraj, edited by Shivaji Awdhut and was filmed by Faredoon A. Irani. The story revolves around three friends Amarnath, Gopal and his sister, Mala, how they face their problem in the life and how they realize from the problem in the rest of the story.

==Plot==
The movie deals about three friend Amarnath, Gopal and his sister, Mala. As Mala and Gopals mom is a widow and is unable to care for both the children, she decides to let her Bombay-based cousin, Shobha R. Sharma, adopt Mala. Gopal is always in trouble with the villagers for petty theft, and when he learns that Mala has left, he too runs away from home. Years later, all three have grown up. Malas mom is deceased, while she continues to live with her aunt and uncle, Public Prosecutor Ramnath Sharma; Amarnath is now an Advocate working with Senior Advocate Mehta, and is in love with Mala, not knowing that she is his childhood friend; while Gopal has been to prison in Poona, Madras, and the Police in Hyderabad want him for questioning. Soon Amarnath will be defending Gopal, alias Ram Singh, for the murder of one Mohanchand - and it will be an uphill task for Amarnath - for not only do the Police have airtight evidence against Gopal, Mohanchand was to be the future son-in-law of Ramnath - who has sworn to bring Gopal to justice by hook or by crook.

==Cast==
* Dev Anand as Amarnath Sharma or Amar
* Vyjayanthimala as Mala Public Prosecutor Ramnath Sharma Johnny Walker as James Bond of India
* Lalita Pawar as Mrs. Sharma
* Prem Chopra as Mohanchand or Mohan
* Sulochana Latkar as Janki R. Sharma
* Madan Puri as Madan
* Nana Palsikar as Girdharilal
* Achala Sachdev as Shobha R. Sharma
* Jagdish Raj as Madans associate
* Laxmi Chhaya as Laxmi
* Tun Tun as Chabeli in cameo appearance
* Brahm Bhardwaj as Advocate Mehta
* Suresh as Ram Singh or Gopal

==Soundtrack==
The films soundtrack was composed by Shankar Jaikishan duo while the lyrics was provide by Hasrat Jaipuri, S. H. Bihari and Gopaldas Neeraj. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Picturization || Length (m:ss) ||Lyrics ||Notes
|-
| 1|| "Dooriyan Nazdikiyan Ban Gayi" ||   || 
|-
| 2|| "Falsafa Pyar Ka Tum Kya Jano" ||   || 
|-
| 3|| "Jawan Tum Ho Jawan Ham Hai" ||   ||
|-
| 4|| "Yeh Dharti Hindustan Ki" ||   || 
|-
| 5|| "Tuhi Meri Laxmi Tuhi Meri Chhaya" ||   || Lyrics on actress Laxmi Chhaya
|-
| 6|| "Duniya Isi Ka Naam Hai" ||   ||
|}

==Box office== tenth highest grossing film of 1968 with verdict average success at box office. 

==Award==
;Filmfare Awards Best Cinematographer - Faredoon A. Irani

==References==
 

==External links==
*  
*   at Upperstall.com
*   at  

 
 
 
 
 
 
 
 