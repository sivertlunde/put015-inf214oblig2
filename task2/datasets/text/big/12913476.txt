Her Choice
 
{{Infobox film
| name           = Her Choice 
| image          = 
| image_size     = 
| caption        = 
| director       = Albert G. Price
| producer       = Arthur Hotaling
| writer         = Albert G. Price
| narrator       = 
| starring       = Mae Hotely
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent film English intertitles
| budget         = 
}}
 silent comedy film featuring Oliver Hardy.

==Cast==
* Mae Hotely - Mrs. Stern
* Ed Lawrence - Mr. Stern
* Raymond McKee - Harry
* Jerold T. Hevener - Count Lamont
* Ben Walker - Lord Chase
* Oliver Hardy - (as Babe Hardy)

==See also==
* List of American films of 1915
* Filmography of Oliver Hardy

==External links==
* 

 
 
 
 
 
 
 
 