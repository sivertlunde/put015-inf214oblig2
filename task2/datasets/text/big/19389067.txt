Hook and Ladder (1932 film)
{{Infobox film
| name           = Hook and Ladder
| image          = Hook ladder.JPEG
| image_size     =
| caption        =
| director       = Robert F. McGowan
| producer       = Robert F. McGowan Hal Roach
| writer         = Robert A. McGowan Hal Roach H. M. Walker
| narrator       =
| starring       =
| music          = Leroy Shield Marvin Hatley
| cinematography = Hap Depew
| editing        = Richard C. Currier MGM
| released       =  
| runtime        = 17:59
| country        = United States
| language       = English
}}
 short comedy film directed by Robert F. McGowan.    It was the 116th (28th talking episode) Our Gang short that was released.

==Synopsis==
Answering the Fire Chiefs request for volunteers, the Our Gang kids form their own firefighting squadron, replete with ersatz uniforms, a fire pole, a dog-and-cat-powered alarm, and a jerry-built fire engine that must be seen to be believed. After a few false alarms and delays, the kids are afforded the opportunity to put out a real fire, which they do with the expertise of veteran smoke-eaters.

==Cast==
===The Gang===
*Sherwood Bailey as Spud Matthew Beard as Stymie
*Dorothy DeBorba as Dorothy
*Kendall McComas as Breezy 
*George McFarland as Spanky Dickie Moore as Dickie
*Buddy McDonald as Speck
*Harold Wertz as Bouncy
* Pete the Pup as Himself
* Laughing Gravy as Dog in Dickies car

===Additional cast===
* Gene Morgan as Fireman
* Don Sandstrom as Fire hazard bit

==Notes and critique== Fire Fighters are also re-used. An amusing running gag involving Spanky McFarlands worm medicine punctuates this lively series entry. 
 When The Wind Blows. Most of the orchestral scoring was employed during the scenes where the gang was fighting a real fire.
 Dickie Moores first appearance. He would be a lead character but would only remain for a season. This was also Sherwood Baileys and Buddy MacDonalds last appearances on the series.

It is also the final time an Our Gang title card says: Our Gang Comedies: Hal Roach presents His Rascals in...

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 