Salaam Kashmier
{{Infobox film
| name               = Salaam Kashmir
| image               = Salaam Kashmir.jpg
| alt                = 
| caption            = 
| director           = Joshiy
| producer           = Mahaa Subair
| writer             = Sethu
| starring           = Suresh Gopi Jayaram Mia George
| music              = M. Jayachandran
| cinematography     = Manoj Pillai
| editing            = Shyam Sasidharan
| studio             = Varnachithra Big Screen
| distributor        = Varnachithra Release
| country            = India
| language           = Malayalam
| released           =  
| runtime            = 
| budget             =
| gross              =
}} Krishna Kumar, Vijayaraghavan and Nasrani Achayan from Travancore. 

==Plot==
The film, which has an intriguing storyline, revolves around two men - Tomy Eeppan Devassy and Sreekumar. Jayaram plays Sreekumar, a character who does all the domestic work expected from a wife in normal course. Into this peaceful world enters Tomy (Suresh Gopi) disrupting the domestic bliss and bringing out an unexpected twist. With his arrival everything goes haywire in Sreekumar’s life and he reaches a position where he can no longer be with his family. The film unravels the mysterious past of both the characters finding the cause behind their unusual demeanor. 

==Cast==
* Suresh Gopi as Lt. Colonel Tommy Eappan Devassy
* Jayaram as Major Sreekumar
* Mia George as Suja Sreekumar/Leena Jacob
* Lalu Alex as Roy Krishna Kumar as Captain Satheesh
* Junaid Sheikh as Terrorist Leader Vijayaraghavan as Jacob
* Anoop Chandran as Madhavan
* Ponnamma Babu as Lakshmi Kurup
* P. Sreekumar as Kurup
* Valsala Menon as Valyammachi
* Baiju Jose as Rameshan
* S.P.Sreekumar as Vinu
* Krishna Prabha

==References==
 
 

==External links==
*  

 
 