Cloverfield
{{Infobox film
| Name           = Cloverfield
| image          = Cloverfield theatrical poster.jpg
| image_size     = 
| alt            = 
| caption        = US theatrical release poster
| director       = Matt Reeves
| producer       = J. J. Abrams Bryan Burk
| writer         = Drew Goddard Odette Yustman
| cinematography = Michael Bonvillain
| editing        = Kevin Stitt
| studio         = Bad Robot Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 84 minutes  
| country        = United States
| language       = English
| budget         = $25 million 
| gross          = $170.8 million   
}} science fiction found footage monster film gigantic monster attacks the city.
 VFX and CGI were Double Negative and Tippett Studio.

==Plot==
<!--
Per WP:FILMPLOT, summaries should be 400-700 words.
http://en.wikipedia.org/w/index.php?title=Cloverfield&oldid=501154375 
-->
The film is presented as found footage from a personal video camera recovered by the United States Department of Defense. A disclaimer text states that the footage is of a case designated "Cloverfield" and was found in area US447 "formerly known as Central Park". The video consists primarily of segments taped the night of Friday, May 22, 2009. Occasionally, older segments are shown from a previous video that was mostly taped over. 
 platonic friend, who is filming her. They make plans to go to Coney Island that day. The footage then cuts to Friday, May 22 when Robs brother Jason and his girlfriend Lily prepare a farewell party for Rob, who will be moving to Japan. Their friend Hud uses the camera to film testimonials during the party.
 a large Army National 42nd Infantry Division attacking the monster, and smaller "parasite" creatures falling off its body and attacking nearby pedestrians and soldiers. 
 Spring Street 59th Street station, where they exit the subway. They come across a command center and field hospital, where Marlena is taken away and is witnessed dying behind a curtain from the bite.  One of the military leaders tells the group when the last evacuation helicopter will depart before the military executes its "Hammer Down Protocol," which will destroy Manhattan, in an attempt to kill the monster.
 Marine Corps helicopter and escapes. Moments later, Rob, Beth, and Hud are taken away in a second helicopter and witness a United States Air Force|U.S. Air Force Northrop Grumman B-2 Spirit|B-2 Spirit bombing the monster. The monster falls, then lunges at the protagonists helicopter, causing it to crash into Central Park.
 

The film skips to Saturday, May 23 (less than an hour later), with a voice on the crashed helicopters radio warning that the Hammer Down protocol will begin in fifteen minutes. The three friends regain consciousness and flee the helicopter, but the creature suddenly appears, and kills Hud. 

Rob and Beth grab the camera and take shelter under Greyshot Arch in Central Park. As air raid sirens begin to blare and the bombing starts, Rob and Beth take turns leaving their last testimony of the days events. The bridge crumbles and the camera gets knocked out of Robs hand, getting buried beneath some rubble.  As the air raid approaches, Rob and Beth each proclaim their love for each other just before another bomb goes off, at which point theyre heard screaming while the monster roars in pain and the video freezes, and Rob, Beth, and the monster are presumably killed in the explosion. 

The film then cuts to the footage of Rob and Beths Coney Island date on April 27. In the distance unnoticed by them, something falls from the sky into the ocean, then Rob faces the camera towards him and Beth, and zooms in on Beth, who says "I had a good day." The tape then freezes and cuts out. 
 

==Cast==
 
* Michael Stahl-David as Robert "Rob" Hawkins
* T. J. Miller as Hudson "Hud" Platt Odette Yustman as Elizabeth "Beth" MacIntyre
* Jessica Lucas as Lily Ford
* Lizzy Caplan as Marlena Diamond
* Mike Vogel as Jason Hawkins Ben Feldman as Travis Billy Brown as Staff Sgt. Pryce

To prevent the leaking of plot information, instead of auditioning the actors with scenes from the film, scripts from Abrams previous productions were used, such as the television series Alias (TV series)|Alias and Lost (TV series)|Lost. Some scenes were also written specifically for the audition process, not intended for use in the film. Despite not being told the premise of the film, Caplan stated that she accepted a role in Cloverfield solely because she was a fan of the Abrams-produced Lost (in which her former Related co-star Kiele Sanchez was a recurring character), and her experience of discovering its true nature initially caused her to state that she would not sign on for a film in the future "without knowing full well what it is". She indicated that her character was a sarcastic outsider, and that her role was "physically demanding".   

==Production==

===Development===

 . He explained, "We saw all these Godzilla toys, and I thought, we need our own American monster, and not like King Kong. I love King Kong. King Kong is adorable. And Godzilla is a charming monster. We love Godzilla. But I wanted something that was just insane and intense."    
 greenlit Cloverfield, to be produced by Abrams, directed by Matt Reeves, and written by Drew Goddard. The project was produced by Abrams company, Bad Robot Productions.    The visual effects producer was Chantal Feghali.

The severed head of the Statue of Liberty was inspired by the poster of the 1981 film Escape from New York, which had shown the head lying in the streets in New York. According to Reeves, "Its an incredibly provocative image. And that was the source that inspired producer J. J. Abrams to say, Now this would be an interesting idea for a movie." 

===Title===
The film was initially titled Cloverfield; however, this changed throughout production before it was finalized as the original title. Matt Reeves explained that the title was changed frequently due to the hype caused by the teaser trailer. "That excitement spread to such a degree that we suddenly couldnt use the name anymore. So we started using all these names like Slusho and Cheese.  And people always found out what we were doing!" The director said that "Cloverfield" was the governments case designation for the events caused by the monster, comparing the titling to that of the Manhattan Project. "And its not a project per se. Its the way that this case has been designated. Thats why that is on the trailer, and it becomes clearer in the film. Its how they refer to this phenomenon   this case", said the director.  The films final title, Cloverfield, is the name of the exit Abrams takes to his Santa Monica office.     In turn, the road used to lead to the Santa Monica Airport, which originally bore the name Clover Field.

One final title, Greyshot, was proposed before the movie was officially titled Cloverfield. The name Greyshot is taken from the archway that the two survivors take shelter under at the end of the movie. Director Reeves said that it was decided not to change the title to Greyshot because the film was already so well known as Cloverfield.   

The film received a subtitle in Japan, where it was released as  . The subtitle "Destroyer" was chosen by Abrams and was translated into Japanese as   by Paramount Japan at his request. The subtitle   was chosen for the manga spinoff, Cloverfield/Kishin, released exclusively in Japan.   

===Production===
The casting process was carried out in secret, with no script being sent out to candidates. With production estimated to have a budget of $30&nbsp;million, principal photography began in mid-June 2007 in New York.  One cast member said that the film would look like it cost $150&nbsp;million, despite producers not casting recognizable and expensive actors.  Filmmakers used the Panasonic HVX200 for most of the interior scenes, and the Sony CineAlta F23 high-definition video camera to tape nearly all of the New York exterior scenes.  Filming took place on Coney Island, with scenes shot at Denos Wonder Wheel Amusement Park and the B&B Carousel.  The scenes of tanks firing at the creature while the main characters hide in a stairwell were filmed on Hennesy Street on Warner Bros. backlot in Burbank, CA. Some interior shots were taped on a soundstage at Downey, California. Bloomingdales in the movie was actually shot in an emptied Robinsons-May store that was under reconstruction in Arcadia, California. The outside scenes of Sephora and the electronics store were taped in Downtown Los Angeles. 

 , as viewed through the films first-person narrative]]
The film was shot and edited in a  , sending people videos through e-mail&nbsp;– we felt it was very applicable to the way people feel now."   

Several of the filmmakers are heard but not seen in the film. The man yelling "Oh my God!" repeatedly when the head of the Statue of Liberty lands in the street is producer Bryan Burk, and director Matt Reeves voiced the whispered radio broadcast at the end of the credits.  After viewing a cut of the film, Steven Spielberg suggested giving the audience a hint at the fate of the monster during the climax, which resulted in the addition of a countdown overheard on the helicopters radio and the sounding of air raid sirens to signal the forthcoming Hammer Down bombing. 

====Style of cinematography====
 .]] motion sickness, including nausea and a temporary loss of balance. Audience members prone to migraines have cited the film as a trigger. Some theaters showing the film, such as AMC Theatres, provided posted and verbal warnings, informing viewers about the filming style of Cloverfield while other theatres like Pacific Theatres just verbally warned customers in detail at the box office about experiencing motion sickness upon viewing the film and what to do if they had to step out and vomit. 
 encoding of the video and can cause compression artifacts to fast motion across the field of view. 

===Creature design===
  Phil Tippetts separation anxiety". This recalls real-life elephants who get frightened and lash out at the circus, because the director felt "theres nothing scarier than something huge thats spooked".   

==Marketing==
Before the films release, Paramount carried out a viral marketing campaign to promote the film which included viral tie-ins similar to Lost Experience.    Filmmakers decided to create a teaser trailer that would be a surprise in the light of commonplace media saturation, which they put together during the preparation stage of the production process. The teaser was then used as a basis for the film itself. Paramount Pictures encouraged the teaser to be released without a title attached, and the Motion Picture Association of America approved the move.  As Transformers (film)|Transformers showed high tracking numbers before its release in July 2007, the studio attached the teaser trailer for Cloverfield that showed the release date of January 18, 2008 but not the title.  A second trailer was released on November 16, 2007 which was attached to Beowulf (2007 film)|Beowulf, confirming the title. 
 scoopers that follow upcoming films. The controlled release of information on the film has been observed as a risky strategy, which could succeed like The Blair Witch Project (1999) or disappoint like Snakes on a Plane (2006), the latter of which had generated online hype  but failed to attract large audiences.

===Pre-release plot speculation=== Time Out reported that the film was about an alien called "The Parasite".  IGN also backed the possibility of that premise, with The Parasite rumored to be a working title for the film.  Online, Slusho and Colossus had been discussed as other possible titles,  as well as Monstrous,  although this was dispelled by Abrams at ComicCon.  Entertainment Weekly also disputed reports that the film would be about a parasite or a colossal Asian robot such as Voltron. 

Visitors of the website Aint It Cool News have pointed out September 11, 2001 attacks|9/11 allusions based on the destruction in New York City such as the decapitated Statue of Liberty. The film has also drawn alternate reality game enthusiasts that have followed other viral marketing campaigns like those set up for the TV series Lost, the video games Halo 2 and Halo 3. Members of the forums at argn.com and unfiction.com have investigated the background of the film, with the "1-18-08" section at Unfiction generating over 7,700 posts in August 2007. The members have studied photographs on the films official site, potentially related MySpace profiles,  and the Comic-Con teaser poster for the film.    A popular piece of fan art posited that the monster was a mutated Humpback Whale. 

===Viral tie-ins===

All major characters received a personal Myspace page which are accessible and photos are available though blog posts have been removed. For links see List of Cloverfield characters.
 Iron Man, Indiana Jones and the Kingdom of the Crystal Skull, Kung Fu Panda, and The Love Guru). 

The drink Slusho! served as part of the   film, and Uhura orders a Slusho! during the bar scene.  When Cloverfield was hosted at Comic-Con 2007, gray Slusho! T-shirts were distributed to attendees.  Fans who had registered at the Slusho! website received e-mails of fictional sonar images before the films release that showed a deep-sea creature heading toward Manhattan.  Fans who ordered merchandise received pieces of torn Tagruato documents and Japanese newspapers along with their products. Slusho! has also appeared in Fringe (TV Series)|Fringe and Heroes (TV Series)|Heroes.

Producer Burk explained the viral tie-in, "It was all done in conjunction with the studio... The whole experience in making this movie is very reminiscent of how we did Lost Experience|Lost."  Director  Reeves described Slusho! as "part of the involved connectivity" with Abrams Alias and that the drink represented a "meta-story" for Cloverfield. The director explained, "Its almost like tentacles that grow out of the film and lead, also, to the ideas in the film. And theres this weird way where you can go see the movie and its one experience... But theres also this other place where you can get engaged where theres this other sort of aspect for all those people who are into that. All the stories kind of bounce off one another and inform each other. But, at the end of the day, this movie stands on its own to be a movie.... The Internet sort of stories and connections and clues are, in a way, a prism and theyre another way of looking at the same thing. To us, its just another exciting aspect of the storytelling." 

Marketing tie-in websites also include:
 , password: jllovesth,
 ,
 ,
www.USGX8810B467233PX.com and others.

At Menuism.com there are reviews for a Japanese restaurant called Garbanzos in Norway that mention Tagruato, Slusho! and Seabed Nectar.

===Merchandise===
A four-installment prequel manga series by Yoshiki Togawa titled   was released by Japanese publisher Kadokawa Shoten.  The story focuses on a Japanese high school student named Kishin Aiba, who somehow bears a connection to the monster. 

Based on the films successful opening weekend, Hasbro began accepting orders for a   collectible toy figure of the monster with authentic sound  and its parasites that were shipped to fans by December 24, 2008. 

==Music and sound==
{{Infobox album 
| Name        = Robs Party Mix
| Type        = compilation
| Artist      = various artists
| Cover       =
| Released    = January 17, 2008
| Recorded    =
| Genre       = Alternative rock, blues rock, britpop, electronic music|electronic, indie pop, indie rock
| Length      = 64:02
| Label       =
| Producer    =
}}
Due to its presentation as footage from a consumer digital recorder, Cloverfield has no film score, with the exception of the composition "Roar! (Cloverfield Overture)" by Michael Giacchino that plays over the end credits. Similarities between "Roar!" and the music of Godzilla composer Akira Ifukube have been noted, and it has been suggested that Giacchinos overture is a tribute to Ifukubes work,   which was confirmed by Matt Reeves in the DVDs commentary track.  The sound track was supervised by William Files  and Douglas Murray  at Skywalker Sound.

Robs Party Mix or Cloverfield Mix is a collection of the music played in the opening party sequences of the film that was released exclusively on Apples iTunes Store on January 22, 2008 in lieu of a traditional soundtrack album. The Cloverfield score, "Roar! (Cloverfield Overture)" by Michael Giacchino that plays over the end credits    is not featured on the album, as it is the mixtape played at the party and is not the official soundtrack of the film. This album was distributed to guests at a Cloverfield premiere party held at the Dark Room in New York City on January 17, 2008. 

A complete soundtrack release of all the music in the film, including Giacchinos "Roar!" end title piece, has now also been released exclusively on iTunes; it has not been officially released in retail stores. A CD entitled Robs Party Mix comes packaged in a special edition of Cloverfield made available for sale in Canadian Wal-Mart stores beginning on April 22, 2008.

{{Track listing
| headline        = Track listing
| extra_column    = Artist
| title1          = West Coast
| note1           = Coconut Records
| length1         = 3:32
| title2          = Taper Jean Girl
| note2           =
| extra2          = Kings of Leon
| length2         = 3:05 Beautiful Girls
| note3           =
| extra3          = Sean Kingston
| length3         = 4:01
| title4          = Do I Have Your Attention
| note4           =
| extra4          = The Blood Arm
| length4         = 3:35
| title5          = Got Your Moments
| note5           =
| extra5          = Scissors for Lefty
| length5         = 3:11
| title6          = Give Up the Funk (Tear the Roof off the Sucker)
| note6           = Parliament
| length6         = 5:46
| title7          = 19-2000
| note7           =
| extra7          = Gorillaz
| length7         = 3:27 The Underdog
| note8           = Spoon
| length8         = 3:42
| title9          = Pistol of Fire
| note9           =
| extra9          = Kings of Leon
| length9         = 2:20
| title10         = Disco Lies
| note10          =
| extra10         = Moby
| length10        = 3:22
| title11         = Do the Whirlwind
| note11          = Architecture in Helsinki
| length11        = 4:39
| title12         = Grown So Ugly
| note12          =
| extra12         = The Black Keys
| length12        = 2:24
| title13         = Four Winds
| note13          = Bright Eyes
| length13        = 2:09
| title14         = The Ride
| note14          =
| extra14         = Joan As Policewoman
| length14        = 3:09
| title15         = Seventeen Years
| note15          =
| extra15         = Ratatat
| length15        = 4:26
| title16         = Wraith Pinned to the Mist and Other Games
| note16          =
| extra16         = Of Montreal
| length16        = 4:15
| title17         = Fuzz
| note17          =  
| extra17         = Mucc
| length17        = 4:47
}}

==Release==
  January release Ride Along in 2014 with a weekend gross of $41.5 million).    Worldwide, it has grossed $170,602,318, making it the first movie in 2008 to gross over $100&nbsp;million. 

===Critical reception=== average score of 64, based on 37 reviews. 

Marc Savlov of The Austin Chronicle called the film "the most intense and original creature feature Ive seen in my adult moviegoing life   a pure-blood, grade A, exhilarating monster movie." He cites Matt Reeves direction, the "whip-smart, stylistically invisible" script and the "nearly subconscious evocation of our current paranoid, terrorism|terror-phobic times" as the keys to the films success, saying that telling the story through the lens of one characters camera "works fantastically well".  Michael Rechtshaffen of The Hollywood Reporter called it "chillingly effective", generally praising the effects and the films "claustrophobic intensity". He said that though the characters "arent particularly interesting or developed", there was "something refreshing about a monster movie that isnt filled with the usual suspects".  Lisa Schwarzbaum of Entertainment Weekly said that the film was "surreptitiously subversive,   stylistically clever little gem", and that while the characters were "vapid, twenty-something nincompoops" and the acting "appropriately unmemorable", the decision to tell the story through amateur footage was "brilliant".  Roger Ebert in the Chicago Sun-Times wrote that the film is "pretty scary at times" and cites "unmistakable evocations of 9/11". He concludes that "all in all, it is an effective film, deploying its special effects well and never breaking the illusion that it is all happening as we see it". 
 New York fates are trauma of 9/11 and turns it into just another random spectacle at which to point and shoot".  Michael Phillips of the Chicago Tribune warned that the viewer may feel "queasy" at the references to September 11, but that "other sequences   carry a real jolt" and that such tactics were "crude, but undeniably gripping". He called the film "dumb", but "quick and dirty and effectively brusque", concluding that despite it being "a harsher, more demographically calculating brand of fun", he enjoyed the film.  Bruce Paterson of Cinephilia described the film as "a successful experiment in style but not necessarily a successful story for those who want dramatic closure".  Some critics also pointed out the similarity to the Half-Life (series)|Half-Life video game series, in particular the "Ant-lion" monsters from Half-Life 2, and the constant first-person perspective. 
 Blair Witch nearly ten years earlier, Cloverfield helped prove, particularly in its first half hour, that what you dont see can be the scariest thing of all." 

The film was nominated for four awards: two Saturn Awards for "Best Supporting Actress (Lizzy Caplan)" and "Best Science Fiction Film" and two Golden Trailer Awards for "Best Thriller for Trailer" and "Most Original Trailer".  The film went on to win a Saturn Award for "Best Science Fiction Film". It was also ranked #12 on Bravos 13 Scarier Movie Moments. 

==Home media== Suncoast and Target and Borders also has an exclusive booklet encased with their DVD.
 Region 2 DVD was released on June 9 in both one-disc and two-disc editions. The limited steel-book edition is only available from HMV, while Play.com offers exclusive cover artwork. The HMV-exclusive steel-book contains two discs.

The DVD includes two   in the lower left-hand corner of the screen, and there is an additional beeping tone indicating the end of the tape. 

A Blu-ray Disc|Blu-ray edition was released on June 3, 2008.  It includes a "Special Investigation Mode," as well as all the bonus features of the 2-disc DVD in HD.

== Possible sequel==
At the films premiere, Reeves talked about possibilities of what a sequel will look like if the film succeeds.  According to Reeves, "While we were on set making the film we talked about the possibilities and directions of how a sequel can go. The fun of this movie was that it might not have been the only movie being made that night, there might be another movie! In todays day and age of people filming their lives on their camera phones and Handycams, uploading it to YouTube... That was kind of exciting thinking about that."  In another interview, Reeves stated:

 

Reeves also pointed out that the final scene on Coney Island shows something falling into the ocean in the background without an explanation. This may have been either the satellite owned by the fictional Japanese media company, Tagruato, or the creature itself. A company news piece on the   website mentions that a piece of the Japanese Governments ChimpanzIII satellite fell off into the Atlantic. Producers Bryan Burk and J. J. Abrams also revealed their thoughts on possible sequels to Entertainment Weekly. According to Burk, "The creative team has fleshed out an entire backstory which, if were lucky, we might get to explore in future films".    Abrams stated that he does not want to rush into the development of the sequel merely because the first film has been a success; he explained that he would rather create a sequel that is true to the previous film. 

At the end of January 2008, Reeves entered early talks with Paramount to direct a sequel, which would likely be filmed before Reevess other project, The Invisible Woman.  Reeves now said:

 

In September 2008, when asked by CraveOnline what the current status is on Cloverfield 2,  Abrams stated that at this point, they were still discussing it; however, he still feels reluctant to work on a sequel. In the same interview, Abrams said that they were working on something that "could be kind of cool." When asked if it would take place in a different location, Abrams replied by saying that "it would be a totally different kind of thing but its too early to talk about."  In a 2010 interview with Attack of the Show, Abrams had stated that they might abandon the filming style, stating that he and the rest of the crew would like to try something new. 
 Super 8 was initially speculated to be either a sequel or prequel to Cloverfield,  but this was quickly denied by Abrams. 

In January 2011, horror film fan site BloodyDisgusting.com stated that a Cloverfield sequel may in fact never happen. They talked to director Reeves and he said that if he can ever get the time to sit down and talk with Drew Goddard and J. J. Abrams about sequel possibilities they will certainly make a sequel, but due to all threes busy schedules Reeves does not see this happening any time soon.  In a 2011 interview, Matt Reeves gave an update on the status of Cloverfield 2, saying, "Getting the right idea together has been taking a long time. &nbsp;... You are going to see it - we just dont know when  &nbsp;... At the moment we are talking about the story quite a lot. Drew Goddard, who wrote the original, is going to pen the sequel and JJ Abrams is very much involved.&nbsp;... However, the three of us have been so busy that getting the right idea together has been taking a long time." When asked if the sequel will be shot in real-time, Reeves stated "You see, thats a difficult part: we want it to be shot like the first but how can you continue that idea successfully for a second time?&nbsp;... We have a lot of affection for the original and the sequel cant just be the same thing. But that is tricky when you need to have a monster destroying stuff once again."   

In a 2012 interview, screenwriter Goddard gave an update saying, "Im in, Im ready to do it...someone call J. J. and tell him to get moving, but because Matt and J. J. and I have been fortunate enough to be busy, its hard syncing our schedules up. Were all very passionate about returning to that world." When asked if an idea is on paper, he responded: "If you asked each of us what we wanted to do, youll get three different answers, which is how the first film was. The aesthetic of Cloverfield benefits from that. Three voices pulling it. Look, nothing would make me happier than to get the three of us in the room to get started."   In a later interview in April of that same year, Goddard said, "We didnt set out to make a franchise, we set out to make a good movie. But I love that world and that universe, so if there was an idea that excited us enough, and we felt like there was a reason to do it, we would do it. The nice thing about when you work with a guy like J.J., and the power he gets, the studios not going to force him to do anything. And he has been able to say, well do it when were ready. Were not going to just do it because it will help your bottom line, were going to do it because theres an idea that excites us. And so thats informed our discussions. We dont feel like we have to, so its like Can we come up with something that excites us enough to do it?" 

==See also==
*Unfriended

==References==
 

==External links==
 
 
*   (  Adobe Flash Player)
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 