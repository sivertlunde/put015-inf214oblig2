Vitus (film)
{{Infobox film
| name           = Vitus
| image          = VitusPoster.jpg
| caption        = U.S. theatrical poster
| alt            = A boy playing the piano
| director       = Fredi M. Murer
| producer       = Christian Davi Christof Neracher
| writer         = {{Plain list | 
* Peter Luisi
* Fredi M. Murer
* Lukas B. Suter
}}
| starring       = {{Plain list | 
* Teo Gheorghiu
* Bruno Ganz
* Julika Jenkins
* Urs Jucker
}}
| music          = Mario Beretta
| cinematography = Pio Corradi
| editing        = Myriam Flury
| distributor    = Sony Pictures Classics
| released       = Switzerland   United States June 29, 2007
| runtime        = 100 minutes 123 minutes (US)
| country        = Switzerland English
| budget         = 
| gross          = 
}}

Vitus is a drama film written and directed by Fredi M. Murer. It was released on February 2, 2006, in Switzerland. It stars real-life piano prodigy Teo Gheorghiu, Bruno Ganz, Julika Jenkins, and Urs Jucker.

== Plot ==
Vitus, played by Teo Gheorghiu, is a highly gifted pianist at the age of 12. His parents mean well, but are over-protective, so Vitus rebels and seeks refuge with his grandfather (Bruno Ganz), who loves flying. After faking a head injury, Vitus secretly amasses a fortune on the stock market. The money allows his grandfather to purchase a Pilatus PC-6 and his father to return triumphantly to the company that had fired him previously. Vitus pursues his former babysitter, Isabel, but she prefers someone older and does not return his affections.
 Piano Concerto on stage with the Zurich Chamber Orchestra.

== Reception ==

=== Critical response ===
Western critics gave Vitus generally favorable reviews. Review aggregation website Rotten Tomatoes gives the film a 64% rating, based on reviews from 58 critics.  Metacritic rated the film 63 out of 100, based on 19 reviews. 

=== Awards ===
 
* Best Swiss Film of 2006 (Swiss Film Awards) 

== References ==
 

== External links ==
*  
*   at Apple.com
*  
*   at the  
*  
*  
*  
*  

 
 
 
 
 
 

 
 