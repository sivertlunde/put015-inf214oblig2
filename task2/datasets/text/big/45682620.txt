Innocent (1921 film)
{{Infobox film
| name           = Innocent 
| image          =
| caption        =
| director       = Maurice Elvey
| producer       = 
| writer         = Marie Corelli (novel)   William J. Elliott  Edward ONeill
| music          = 
| cinematography = 
| editing        = 
| studio         = Stoll Pictures
| distributor    = Stoll Pictures
| released       = March 1921
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama Edward ONeill. The film marked the screen debut of Rathbone, with his casting as a villainous figure pointing towards the sort of roles he would play in later British and Hollywood films.  The film was made by Stoll Pictures, Britains leading film company of the era, at Cricklewood Studios.

==Synopsis==
A naive country girl comes to the city, where she is seduced by a cynical artist. 

==Cast==
*  Madge Stuart as Innocence  
* Basil Rathbone as Amadis de Jocelyn  Lawrence Anderson as Robin   Edward ONeill as Hugo de Jocelyn  
* Frank Dane as Ned Langdon  
* W. Cronin Wilson as Armitage  
* Ruth Mackay as Lady Maude  
* Mme. dEsterre as Miss Leigh 
* Annie Esmond as Housekeeper

==References==
 

==Bibliography==
* Kabatchnik, Amnon. Sherlock Holmes on the Stage: A Chronological Encyclopedia of Plays Featuring the Great Detective. Scarecrow Press, 2008.
* Low, Rachael. The History of the British Film 1918-1929. George Allen & Unwin, 1971.

==External links==
*  

 

 
 
 
 
 
 
 
 
 

 
 