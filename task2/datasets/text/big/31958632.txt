Bedevilled (2010 film)
{{Infobox film name        = Bedevilled image       = BedevilledPoster.jpg caption     = Theatrical poster
| film name = {{Film name hangul      =       hanja       =    의   rr          = Kim Bok-nam salinsageonui jeonmal mr          = Kim Pok-nam sarinsakŏnŭi jŏnmal }} director    = Jang Cheol-soo producer    = Park Kyu-young writer      = Choi Kwang-young starring    = Seo Young-hee Ji Sung-won music       = Kim Tae-seong cinematography = Kim Gi-tae editor      = Kim Mi-joo distributor = Sponge ENT released    =   runtime     = 115 minutes country     = South Korea language    = Korean budget      =   gross       =   
}} thriller film starring Seo Young-hee and Ji Sung-won. The film premiered as an official selection of International Critics Week at the 2010 Cannes Film Festival.

It is the feature directorial debut of Jang Cheol-soo, who worked as an assistant director on the Kim Ki-duk films Samaritan Girl and Spring, Summer, Fall, Winter... and Spring.  The film was a runaway hit in Korea, with the box-office returns far exceeding its   ( ) budget. 

==Plot==
Hae-won is a middle-rank officer working in a Seoul bank. A severe, tense single woman, she is being brought down by the work-related stress and the hypercompetitive environment she finds herself in. Desperate, she takes up an offer from a long-forgotten friend and takes off for a private vacation in Mudo, a desolate Southern island in which she had spent childhood. Arriving at the island, she is warmly welcomed by Bok-nam, with whom she had a close friendship when both were in their teens but whose constant letters shes since ignored. Life on the backward, undeveloped island is hard, and Bok-nam is treated as little more than a slave by her abusive husband Man-jong, his brother and the local old women. All of Bok-nams love is reserved for her young daughter Yeon-hee, with whom she tries to escape from the small, claustrophobic island. But when that results in tragedy, the woman finally snaps, unleashing all her demons, as Bok-nam takes a sickle in her hand.    

== Cast ==

=== Main characters ===
* Seo Young-hee ... Kim Bok-nam
* Ji Sung-won ... Hae-won
* Park Jeong-hak ... Man-jong
* Baek Su-ryun ... Dong-hos granny
* Bae Sung-woo ... Cheol-jong
* Oh Yong ... Deuk-su
* Lee Ji-eun ... Kim Yeon-hee
* Kim Gyeong-ae ... Pa-jus granny
* Son Yeong-sun ... Sun-yis granny
* Lee Myeong-ja ... Gae-tongs granny
* Yu Sun-cheol ... Old man with Alzheimers
* Jo Deok-jae ... Police officer Seo
* Chae Shi-hyeon ... Mi-ran

=== Supporting characters ===
 
* Tak Seong-eun ... Ji-su
* Hong Seung-jin ... Yankees
* Hwang Min-ho ... Dodgers
* Hong Jae-seong ... Police officer Jang
* Jeong Gi-seob ... Officer Choi
* Ahn Jang-hun ... Mr. Jang
* Myeong Ro-jin ... Bank manager
* Kim Gyeong-ran ... Old lady at bank
* Jae Min ... Victim
* Park Jeong-sun ... Victims father
* Seong Won-yong ... Supervisor
* Han Dong-hak ... Superintendent
* Yuk Sae-jin ... Mudo policeman
* Shim Seung-hyeon ... Mudo policeman
* Kim Yong ... Seoul policeman
* Yu Seung-oh ... Seoul policeman
* Na Jong-ho ... Old villager
* Hang Hae-ji ... Old villagers daughter
* Yu Ae-jin ... Young Bok-nam
* Chun Yeong-min ... Young Hae-won
* Park Jong-bin ... Young Man-jong
* Lee Da-un ... Young Cheol-jong
* Park Seung-ah ... Young Deok-su
* Kim Woo-seok ... Young Seo
* Seo Seung-hwa ... Banker
* Kim Hyeon-su ... Banker
* Lee Kang-hee ... Tearoom waitress
* Ahn Su-yeon ... Restaurant waitress
* Kim Woo-geun ... Coupon boy
 

== Awards ==
2010 Puchon International Fantastic Film Festival   
* Best of Puchon
* Best Actress – Seo Young-hee
* Fujifilm Eterna Award

2010 Cinema Digital Seoul Film Festival  
* Butterfly Award

2010 Fantastic Fest  
* Audience Award
* Best Actress AMD & Dell "Next Wave" Spotlight Competition – Seo Young-hee
 AFI Fest  
* New Auteurs

2010 Grand Bell Awards  
* Best New Director – Jang Cheol-soo

2010 Korean Association of Film Critics Awards
* Best Actress – Seo Young-hee
* Best New Director – Jang Cheol-soo

2010 Korean Film Awards   
* Best Actress – Seo Young-hee
* Best New Director – Jang Cheol-soo

2010 Directors Cut Awards
* Best Actress – Seo Young-hee
* Best New Director – Jang Cheol-soo

2011 KOFRA Film Awards (Korea Film Reporters Association) 	
* Best Actress – Seo Young-hee
 Gerardmer International Fantastic Film Festival
* Grand Prix
 Fantasporto Oporto International Film Festival    
* Best Actress – Seo Young-hee

2011    
* Black Tulip (Grand Jury Prize)

2011 Golden Cinematography Awards
* Best Film
* Best New Actress – Ji Sung-won
* Bronze Medal Cinematography – Kim Gi-tae

== References ==
 

== External links ==
*    
*   at Finecut
*  
*  
*  

 
 
 
 
 
 