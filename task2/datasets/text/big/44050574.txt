Tragic Story with Happy Ending
{{Infobox film
| name           = Tragic Story with Happy Ending
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = Regina Pessoa
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       = 
| music          = Normand Roger
| cinematography = 
| editing        = 
| studio         =       
| distributor    = 
| released       =   
| runtime        = 7 minutes 43 seconds
| country        = Portugal Canada France
| language       = 
| budget         = 
| gross          =  
}}
Tragic Story with Happy Ending (French:Histoire tragique avec fin heureuse) is a 2005 animated short by Regina Pessoa. To make the 7 minute 43 second film, Pessoas drawings were transferred to glossy paper, brushed with india ink, scratched with a blade to give the effect of an engraving, then photographed. National Film Board of Canada film composer Normand Roger supplied the score for the film.        

The film was the second in a trilogy of animated shorts by Pessoa about childhood, between A Noite (1999)  and Kali the Little Vampire.   

==Reception==
Tragic Story with Happy Ending has received numerous awards, becoming one of Portugals most acclaimed animated films.  They include the Cristal and TPS Cineculte awards for short film at the Annecy International Animated Film Festival, as well as a special international jury prize at the Hiroshima International Animation Festival.   

==References==
 

==External links==
* 
*  at Ciclope Films
* 

 
 
 
 
 
 
 

 