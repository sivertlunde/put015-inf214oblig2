Aaj Ki Awaaz
{{Infobox film
| name           = Aaj Ki Awaaz
| image          = Aaj Ki Awaaz.jpg
| image_size     = 
| caption        = 
| director       = Ravi Chopra
| producer       = B. R. Chopra
| writer         = 
| narrator       = 
| starring       = Raj Babbar Smita Patil Nana Patekar Ravi
| cinematography = 
| editing        = 
| distributor    = B. R. Films
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
}} Naan Sigappu Death Wish 

==Cast==
*Raj Babbar  as  Prof. Prabhat Kumar Varma
*Smita Patil  as  Public Prosecutor Rajni V. Deshmukh
*Nana Patekar  as  Jagmohandas
*Om Shivpuri  as  Judge
*Dheeraj Kumar  as  Prof. Lalwani
*Shafi Inamdar  as  Inspector Shafi
*Vijay Arora  as  Srivastava
*Arun Bakshi  as  Inspector Veerkar
*Ashalata  as  Mrs. V.V. Deshmukh
*Chandrashekhar  as  Police Commissioner Sathe
*Iftekhar  as  Judge V.V. Deshmukh
*Alok Nath  as  Hotel (bar owner)
*Gufi Paintal  as  Orderly in Mental Hospital
*Dalip Tahil  as  Suresh Thakur
*Dinesh Thakur  as  Advocate Dayal
*Bashir Khan  as  Prabhats student
*Chandni  as  Sudhas friend
*Deepak Qazir  as  Kishan Khanna
*Sonika Gill  as  Sudha Advani
*Urmila Bhatt  as  Mrs. Varma
*Raksha Chauhan  as/small> Madhu, Prof. Prabhats sister who gets raped and killed.

==Crew==
*Director: Ravi Chopra
*Producer: B. R. Chopra
*Banner: B R Films

==Music==
{|class="wikitable"
|-  
! Song !! Singer (s)
|-
| "Aaj Ki Awaj"
|  Mahendra Kapoor
|-
| "Dil Hi Dil Main"
| Mahendra Kapoor
|-
| "Mera Chhota Sa Ghar"
| Mahendra Kapoor
|-
| "Bharat To Hai Azad"
| Mahendra Kapoor
|}

==Awards==
 
|- 1985
| Hassan Kamal (for title song)
| Filmfare Best Lyricist Award
|  
|-
| Smita Patil
| Filmfare Award for Best Actress
|  
|}

==References==
 

==External links==
* 

 
 
 

 