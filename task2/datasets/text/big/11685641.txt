The Only Thing
{{Infobox film
| name           = The Only Thing
| image          =
| caption        = Jack Conway
| producer       =
| writer         = Elinor Glyn
| narrator       =
| starring       = Eleanor Boardman Conrad Nagel
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 80 minutes
| country        = United States Silent English English intertitles
| budget         =
}}
 silent 1925 1925 costume Jack Conway long career at MGM. The film is also notable for featuring, in her second film role, a young Joan Crawford, playing a minor part as a lady in waiting.  

==Plot==
Adventures of an English duke-diplomat, mythical kingdom royalty and revolution on the shores of the Mediterranean.

==Cast==
* Eleanor Boardman - Thyra, Princess of Svendborg
* Conrad Nagel - Duke of Chevenix
* Edward Connelly - King of Chekia
* Arthur Edmund Carewe - Gigberto
* Louis Payne - Lord Charles Vane
* Vera Lewis - Princess Erek
* Carrie Clark Ward - Princess Anne
* Constance Wylie - Countess Arline Dale Fuller - Governess
* Ned Sparks - Gibson
* Joan Crawford

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 
 