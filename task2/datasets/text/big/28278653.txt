The Silent Village
 
 
{{Infobox film
| name           = The Silent Village
| image          = silentvillage1943.jpg
| image_size     =
| caption        = Screenshot from the film
| director       = Humphrey Jennings
| producer       = David Vaughan
| writer         =
| starring       =
| music          = Becket Williams
| cinematography =
| editing        = Stewart McAllister
| distributor    = Crown Film Unit
| released       =  
| runtime        = 36 min.
| country        = United Kingdom
| language       = English, Welsh
}}

The Silent Village is a 1943 British propaganda short film in the form of a drama documentary, made by the Crown Film Unit and directed by Humphrey Jennings. The film was named one of the top 5 documentaries of 1943 by the National Board of Review. 

==Plot== Upper Swansea Valley; men are shown working at the colliery, women engaged in domestic tasks in their homes and the inhabitants singing in the Methodist chapel. Most of the dialogue in this section is spoken in Welsh language|Welsh, with no subtitles provided. The section closes with another title card stating "such is life at Cwmgiedd...and such too was life in Lidice until the coming of Fascism".

The German occupation is heralded by the arrival in the village of a black car, blaring military music and political slogans from its loudhailer. Little is shown of the occupation itself, its violence being implied by a soundtrack of marching boots, gunfire and harshly amplified orders and directives, in the sound-as-narrative technique Jennings had previously developed in Listen to Britain. The identity of the community is eroded, with the Welsh language being suppressed and no longer permitted as the teaching medium in the school, and trade union activity being made illegal. The villagers resistance takes the form of covert activities including the publication of a Welsh news sheet. Eventually, even the singing of Welsh hymns in the chapel is outlawed.
 Land of Our Fathers" as they go, are lined up against the wall of the village churchyard.

==Background==
The film was designed as a tribute to the mining community of Lidice, Czechoslovakia, which had been the scene of an appalling Nazi atrocity on 10 June 1942 when its entire adult male population (173 men and boys over 16) was executed and all 300 women and children sent to Nazi concentration camps, which few would ultimately survive. News of the massacre caused much shock in the UK, particularly in the coal-mining areas of the country. The film recreates events in Lidice but transports them to a South Wales mining community to indicate that if the German invasion of Britain had been successful in 1940, then the kind of atrocities currently being perpetrated in German-occupied Europe would just as likely be happening contemporaneously in the UK; also as a reminder to the British people of what they were fighting against.

==Production== Arthur Horner Arthur Horner, who felt that the proposed film would be symbolic of the unity and solidarity felt by all mining communities with the people of Lidice.   

Filming began in September 1942 and continued through to December. Jennings had decided that no professional actors would be brought in, and his entire cast consisted of untrained local people, although he was careful to ensure that nobody appeared on screen without giving their permission and that anyone who felt uneasy or uncomfortable about being filmed was excluded from shots. People were filmed going about their actual daily lives in a largely improvised manner with no script being provided. In a letter to his wife, he wrote: "Down here I’m working on a reconstruction of the Lidice story in a mining community – but more important than that really is being close to the community itself and living and working inside it, for what it is everyday. I really never thought to live and see the honest Christian and Communist principles acted on as a matter of course by a large number of British – I won’t say English – people living together." 

==Critical reception==
On its release, the film gained a reputation as a particularly pertinent, powerful and moving short film. Contemporary opinion places it among the products of Jennings most fruitful period as a director alongside Listen to Britain,   said: "Jennings penchant for understatement and striking imagery carried its own force – and the film, calling for solidarity among miners faced with the German threat to freedom, was instrumental in forging enduringly strong relationships between Czech and Welsh miners, in particular." 

==References==
 

== External links ==
*  
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 