The Burglar
 
 
{{Infobox film
| name           = The Burglar
| image          = Theburglarposter.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Paul Wendkos
| producer       = Louis W. Kellman
| screenplay     = David Goodis
| based on       =  
| starring       = Dan Duryea Jayne Mansfield Martha Vickers
| music          = Sol Kaplan
| cinematography = Don Malkames
| editing        = Paul Wendkos Herta Horn
| distributor    = Columbia Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $90,000 (estimated)
| gross          =
}}
 crime Thriller thriller film noir released by Columbia Pictures, based on the 1953 novel of the same name by David Goodis (who also wrote the script). The picture stars Dan Duryea in the titular role and Jayne Mansfield.

==Plot==
Professional burglar Nat Harbin (Dan Duryea) and his two associates, Baylock (Peter Capell) and Dohmer (Mickey Shaughnessy), set their sights on wealthy spiritualist Sister Sarah (Phoebe Mackay), who has inherited a fortune — including a renowned emerald necklace — from a Philadelphia financier. Using Nats female ward, Gladden (Jayne Mansfield), to pose as an admirer and case the mansion where the woman lives, they set up what looks like a perfect break-in; even when Nats car is spotted by a couple of cops, he bluffs his way through, gets the necklace, and makes the getaway. But the trio—plus Gladden—cant agree on how to dispose of the necklace, and soon their bickering becomes a lot less important than the fact that someone is on to what theyve done—a woman (Martha Vickers) is working on Nat, while a man (Stewart Bradley) is working on Gladden. Equally serious, the trio kills a New Jersey state trooper while on their way to warn her. And among the cops chasing them is one with larceny in his heart and murder on his mind.

==Cast==
* Dan Duryea as Nat Harbin
* Jayne Mansfield as Gladden
* Martha Vickers as Della
* Peter Capell as Baylock
* Mickey Shaughnessy as Dohmer
* Stewart Bradley as Charlie

==Background==
This film was remade in 1971 as The Burglars, directed by Henri Verneuil and starring Omar Sharif, Jean Paul Belmondo and Dyan Cannon.

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 