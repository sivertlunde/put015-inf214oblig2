Surprised Parties
{{Infobox film
| name           = Surprised Parties
| image          =
| caption        = Edward Cahn
| producer       = Metro-Goldwyn-Mayer
| writer         = Hal Law Robert A. McGowan
| narrator       =
| starring       =
| music          =
| cinematography = Walter Lundin
| editing        = Leon Borgeau
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 10 51"
| country        = United States
| language       = English
| budget         =
}}
 short comedy Edward Cahn.  It was the 206th Our Gang short (207th episode, 118th talking short, 119th talking episode, and 38th MGM produced episode) that was released.

==Plot==
Upset because, as a leap year baby (he only has a birthday every four years), Froggy bemoans the fact that he has almost never had a birthday party. The gang decides to throw a surprise party in Froggys honor, but to keep him in the dark, they pretend to kick him out of the clubhouse. In order to plan a surprise party for Froggy, the gang has to throw him out of the clubhouse.    Vengefully, Froggy sneaks back and sets all sorts of booby-traps for the other gang members. But when the party takes place, it is Froggy who bears the brunt of his pre-set pranks.   

==Note== Edward Cahn as Our Gangs regular director. He would return in 1943 to direct Three Smart Guys. Herbert Glazer takes over directing beginning with Doin Their Bit.

==Cast==
===The Gang===
* Janet Burston as Janet Mickey Gubitosi (later known as Robert Blake) as Mickey
* Billy Laughlin as Froggy Laughlin
* George McFarland as Spanky
* Billie Thomas as Buckwheat

===Additional cast===
* Margaret Bert as Froggys mother
* Giovanna Gubitosi as Gloria
* Leon Tyler as Jimmy

===Party guests===
Buz Buckley, James Gubitosi, Tommy Tucker, Frank Ward

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 