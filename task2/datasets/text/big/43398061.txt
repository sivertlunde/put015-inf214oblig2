Return to Ithaca
{{Infobox film
| name           = Return to Ithaca
| image          = 
| caption        = 
| director       = Laurent Cantet
| producer       = Laurent Baudens   Didar Domehri   Gaël Nouaille 
| writer         = Laurent Cantet    Leonardo Padura Fuentes 	
| starring       = Isabel Santos Jorge Perugorria Fernando Hechavarría Néstor Jiménez Pedro Julio Díaz Ferran
| music          = 
| cinematography = Diego Dussuel
| editing        = Robin Campillo
| studio         = Full House   Orange Studio   Haut et Court   Panache Productions   Funny Balloons   La Compagnie Cinématographique
| distributor    = Haut et Court 
| released       =   
| runtime        = 95 minutes
| country        = France
| language       = Spanish
| budget         = 
| gross          = 
}}

Return to Ithaca ( ) is a 2014 comedy-drama film directed by Laurent Cantet. The film premiered at the 71st Venice International Film Festival, where it won the Venice Days Award.     It was screened in the Special Presentations section of the 2014 Toronto International Film Festival. 

== Cast ==
* Isabel Santos as Tania
* Jorge Perugorria as Eddy 
* Fernando Hechavarría as Rafa
* Néstor Jiménez as Amadeo
* Pedro Julio Díaz Ferran as Aldo

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 

 