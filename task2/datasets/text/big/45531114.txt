Första divisionen
{{Infobox film
| name           = Första divisionen
| image          =
| caption        =
| director       = Hasse Ekman
| producer       = Lorens Marmstedt
| writer         = Hasse Ekman Alvar Zacke
| narrator       =
| starring       = Lars Hanson Gunnar Sjöberg Stig Järrel Hasse Ekman Irma Christenson
| music          = Lars-Erik Larsson
| cinematography =
| editing        =
| distributor    = Terrafilm 
| released       =  
| runtime        = 92 minutes
| country        = Sweden Swedish
| budget         =
| gross          =
}}
 Swedish drama drama film directed by Hasse Ekman.

==Plot summary==
After a period of hospitalization, caused by an aircraft accident, second lieutenant Gunnar Bråde returns to his old division. During a exercise a bomb hits his planes propeller and he is forced to jump out with a parachute over the target area. This is not observed by all pilots, who continues to drop their bombs... 

== Cast ==
*Lars Hanson as Colonel Magnus Ståhlberg
*Gunnar Sjöberg as captain Krister Hansson
*Stig Järrel as Lieutenant Rutger Sperling 
*Hasse Ekman as second lieutenant Gunnar Bråde 
*Emil Fjellström as sergeant major Persson 
*Ragnar Falck as sergeant Bertil "Jocke" Johansson, signaller
*Carl Reinholdz as Corporal "Storken" Karlsson 
*Irma Christenson as Mona Falkstedt, Kristers fiancée
*Britta Brunius as Greta Johansson, Jockes wife
*Linnéa Hillberg as Mrs. Bråde, Gunnars mother
*Hugo Björne as Åkerman, oculist
*Kotti Chave as Lieutenant Billman, "Bill" 
*Bror Bügler as captain Fallenius
*Bengt Järrel as Billmans signaller
*Sif Ruud as a nurse 

== External links ==
*  

 

 
 
 
 
 
 

 