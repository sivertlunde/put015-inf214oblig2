Certainty (film)
 
{{Infobox film
| name           = Certainty
| director       = Peter Askin
| producer       = Will Battersby Per Melita Mike OMalley
| writer         = Mike OMalley
Art Directed by Stephanie Barkley
| based on       =  
| starring       = Tom Lipinski Adelaide Clemens Kristen Connolly Will Rogers Valerie Harper Bobby Moynihan Giancarlo Esposito
| distributor    = FilmBuff
| released       =  
| country        = United States
| language       = English
}}Certainty is a 2011 comedy-drama film directed by Peter Askin, produced by Will Battersby, Per Melita and Mike OMalley and written by Mike OMalley. 
{{cite news
  |url=http://www.variety.com/article/VR1118019448
  |title=Certainty Heads into Production
  |date=17 May 2010 Variety
  |first=Gordon|last=Cox
}}   The cast includes newcomers Tom Lipinski, Adelaide Clemens, Kristen Connolly and Will Rogers alongside veterans Valerie Harper, Bobby Moynihan and Giancarlo Esposito.   The film was produced by Will Battersby, Per Melita and OMalley. 

==Synopsis==
Certainty tells the story of Dom and Deb who are attending their Pre-Cana engagement encounter weekend, the process a couple must go through in order to get married in the Catholic church.

==Release==    Long Beach, and Boston.  At the Boston Film Festival, it won Best Screenplay, Best Editing and Best Ensemble cast. 
{{cite web
  |url=http://www.bostonherald.com/track/inside_track/view/20110923we_hear_oprah_winfrey_mike_omalley_tom_brady_and_more/
  |title=We Hear: Oprah Winfrey, Mike O’Malley, Tom Brady and more
  |date=23 September 2011 
  |work=Boston Herald
}} 

It was released theatrically and digitally at the end of November 2012 by FilmBuff.

==References==
 

==External links==
*  
* {{cite web
  |title=Peter Askin Talks Certainty, Sarasota Film Festival
  |work=Indiepix Blog
  |url=http://blog.indiepixfilms.com/featured/6306/
}}
* {{cite web
  |title=Mike OMalley Returns to NH For Film preview
  |work=NewHampshire.com
  |url=http://www.newhampshire.com/article/20121012/NEWHAMPSHIRE09/710129953/-1/newhampshire
}}

 