Aazaan
 
 
{{Infobox film
| name           = Aazaan
| image          = azaan movie poster.jpg
| alt            =  
| caption        = Movie poster
| director       = Prashant Chadha
| producer       = M. R. Shahjahan Shaju Ignatius  Sunaman Sood
| writer         = Prashant Chadha Shubra Swarup Heeraz Marfatia.
| screenplay     = Shubra Swarup
| starring       = Sachiin J Joshi  Candice Boucher
| music          = Salim-Sulaiman
| cinematography = Axel Fischer BVK
| editing        = Amitabh Shukla  Humphrey Dixon
| studio         = JMJ Entertainment PVT LTD Alchemia Films
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi 
| budget         =  
| gross          =  (Nett)
}}
Aazaan ( ,  أذان) is a 2011 action spy film directed by Prashant Chadha starring Sachiin J Joshi and South African model Candice Boucher. Aazaan marks the debut of entrepreneur-turned-actor Sachiin J Joshi of Energy Drinks and Candice Boucher who was seen in the title role.  It is one of the most expensive films in Bollywood. Aazaan was made under   it is one of the most expensive films created in 2011.

==Plot== Indian Home RAW headquarters, Sofiya (Amber Rose Revah) joins the investigation. RAW HQ is threatened by a threat who calls himself Doctor. In HQ, army officer Aazaan Khan (Sachiin J Joshi) is interrogated about the case because of his brothers, Aman Khan, involvement as a terrorist in the suicide bombings.

Aazaan is deployed in Waziristan to infiltrate the enemy force and to find the whereabouts of his brother. He travels from country to country to find the culprits and even becomes one of them to gain their confidence. Aazaan comes across two Pakistanis from whom he soon finds the whereabouts of the kingpin, the Doctor. The Doctor has taken hold of Mahfouz (Dalip Tahil) and has seized the cure for the virus from him, so now he has both the weapon and its antidote. He knows that Aazaan is with the cops, and both are led to be killed in the marketsquare. Aazaan breaks free and tries to rescue the scientist, but its too late. Before being detonated by a strapped bomb, he tells Aazaan to find a sand artist in Morocco and a girl with her, as they have the cure for the virus.

Aazaan escapes and runs to Morocco, where he finds the sand artist Aafreen, who is taking care of an orphaned girl. Aafreen knows about Aazaan and his missing brother Aman. Soon both fall in love. But the terrorists follow Aazaan in Morocco too, and the three of them decide to escape to India at the earliest so that the childs blood can be used to make a cure for the virus in India.

As they are waiting for Sam Sharma to turn up in the chopper, Aazaan, Aafreen and the girl face a rude shock when Sam pulls out a gun and shoots Aafreen. The child is forcefully taken away, and Aazaan is captured and led away. Apparently Sam was a traitor working on the side of the Doctor.

==Cast==
* ). Sachiin trained in Krav maga and Kalaripayatu for his role.   
*Dalip Tahil as Mahfouz: He is a savant on a mission and is trying to save world from being destroyed by Doctor.
*  was hired to train her for her role in the film. 
*Arya Babbar as Imaad: He is a misled, passionate man.
*Alyy Khan as Sam Sharma
*Ravi Kissen as Pandey: A controversial RAW officer.
*Sachin Khedekar as Home Minister
*Sajid Hassan as Doctor: He is ex-CIA and into biological warfare and is determined to carry out the absolute annihilation of India.
*Samy Gharbi as Malak: He is tough from the inside out and loyal to Doctor.
*Vijayendra Ghatge as Home Minister
*Sarita Choudhury as Menon
*Amber Rose Revah as Sofiya: Prashant saw her role in From Paris With Love and sent her the script of Aazaan. Being part Polish, he found her perfect for the role although she had never worked in any Polish films. 
*Neet Mohan as Aman Khan
*Prashant Prabhakar as The Indian man (Raw Agent)

==Production==

===Pre-production=== From Paris With Love fame will be starring alongside. Amitabh Shukla and Humphrey Dixon were signed as the editor. 

The international action company Action Concept is been given the work to direct the action stunts since the film involves shooting on multiple foreign locations. 

===Filming===
Filming took place at multiple locations like Morocco, Hong Kong, Algeria, Poland (Kraków), France, Germany and India.  The climax was shot in Chechnya, Russia. Azaan is the first film to be shot in Chechnya after it became an ecological disaster zone, with oil and chemical leaking into the ground and radioactive material left lying about. 

The shooting was scheduled in these countries for 70 days which included many local cast to add the genuine look to Aazaan.  During the shoot for a torture scene in Marrakech, Morocco, Sachiin went bare-bodied for hours at a stretch at a temperature of 0 to −4 Celsius impressing the production crew.  Alvernia Studios in Poland handled the physical production work including camera, light and music score recording. 

Some of the action scenes were shot in Bangkok including stunts in which Sachiin Joshi jumped from a 55-storey tower. Some climax shots were shot by director Prashant Chadha with the rest of the cast including Ravi Kissen and Arya Babbar. 

===Post-Production===
The Digital Intermediate and VFX of Aazaan was done by Pixion Studios.   

===Accidents===
* It was reported that Sachiin got injured when filming a scene where he had to lay down with a sniper and got hit next to his eye. 
* Sachiin fell of a trailer crash scene. Stunt involved Sachiin jumping off a speeding trailer but fell of much before the marking point and hit the footpath. "We all saw him jump off the trailer and hit the road hard but it all happened so fast that we couldnt do much. Though Sachiin didnt call it a day, after some pain killers he was back on the sets," said the director about the accident. 

==Release==
The first look of Aazaan was unveiled at Festival de Cannes 2011 and received an overwhelming response. 

The film was released on 14 October 2011.

==Reception==
Aazaan received mixed response from critics praising it for its polishing and music but commenting on its lack of strong plot. Nikhat Kazmi of Times of India gave the film a 3 star rating out of five praising its shoot-outs, action sequences and cinematography.  IBNLive called it a movie with no content but high on style pointing flaws in the films script.  Subhash K. Jha of NDTV gave a 3-1/2 star rating out of five praising its screenplay and said, "Aazaan is not a film which wastes time in self-congratulation after staging one more terrorist explosion. It moves on relentlessly. The world has to be saved. Self-importance is miraculously averted." 

Rediff movies gave the film 1 star out of five calling it a jerky ride and a wasted effort.  Komal Nahta, a well-known critic, also gave a single star criticising its confusing screenplay, the performance of the new hero and the unconvincing climax.  Movie Talkies gave the film 1.5 stars calling it Aazaan, A Recipe For Disaster 

==Music==
{{Infobox album
| Name        = Aazaan
| Type        = Soundtrack
| Artist      = Salim-Sulaiman
| Cover       = Aazaansoundtrack.jpg
| Released    = 
| Recorded    =
| Genre       = Film soundtrack
| Length      =
| Label       = Xtreme Music
| Producer    =
}}

The music for Aazaan is given by the duo of Salim-Sulaiman, is under the Xtreme Music label,  and was launched by Sanjay Dutt.  On IIFA Rocks the duo of Salim-Sulaiman showcased the first exclusive preview of Aazaan music performing live with host of dancers from the Shiamak Davar Institute of Performing Arts. Three songs – Afreen, Bismillah and Khuda Ke Liye – were performed.   

===Audio listing===
{{track listing
| lyrics_credits = yes
| extra_column = Singer(s)
| note1 = Theme
| title1 = Aazaan
| length1 = 2:18
| title2 = Afreen
| extra2 = Salim Merchant
| length2 = 4:12
| lyrics2 = Amitabh Bhattacharya
| note3 = Dessert Mix
| title3 = Afreen
| extra3 = Salim Merchant
| length3 = 3:47
| lyrics3 = Amitabh Bhattacharya 
| note4 = Remix
| title4 = Afreen
| extra4 = Salim Merchant
| length4 = 4:21
| lyrics4 = Amitabh Bhattacharya 
| note5 = Reprise
| title5 = Afreen
| extra5 = Rahat Fateh Ali Khan
| length5 = 4:46
| lyrics5 = Amitabh Bhattacharya
| title6 = Bismillah
| extra6 = Kailash Kher  
| length6 = 3:57
| lyrics6 = Irfan Siddique
| title7 = Habibi Habibi
| extra7 = Benny Dayal, Mitika
| length7 = 4:51
| lyrics7 = Shrraddha Pandit
| title8 = Khuda Ke Liye
| extra8 = Salim Merchant, Shrraddha Pandit
| length8 = 4:49
| lyrics8 = Amitabh Bhattacharya
| note9 = Remix
| title9 = Khuda Ke Liye
| extra9 = Salim Merchant, Shrraddha Pandit
| length9 = 5:05
| lyrics9 = Amitabh Bhattacharya
}}

===Reception===
The music of Aazaan was received warmly. Bolywoodhungama.com praised the vocals of Rahat Fateh Ali Khan and listed Afreen and Khuda Ke Liye as their best pick from the listing, awarding the album an over-all of three stars our of five. 

==Controversy==
* A group of people have filed a case against the film that it has the potential of hurting the religious sentiments of a specific community and thus Aazaan should be banned, demanding a change of title. 
* Another problem emerged with Censor Board of China for depicting the country as Indias main enemy. As a result the word China was muted out in the film. 
* After Tamil film 7 aum arivu and Hollywood film crazies it was the third film in which china was shown as villain

==References==
 

==External links==
* 
* 

 
 
 
 
 
 