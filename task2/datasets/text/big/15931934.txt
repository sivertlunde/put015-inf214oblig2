Plunder (serial)
{{Infobox film
| name           = Plunder
| image          =
| caption        =
| director       = George B. Seitz
| producer       = George B. Seitz
| writer         = Bertram Millhauser
| starring       = Pearl White Warren William
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 15 episodes
| country        = United States Silent English English intertitles
| budget         =
}}
 drama film serial directed by George B. Seitz. During the production of this serial, on August 10, 1922, John Stevenson, a stuntman for Pearl White, was killed doing a stunt from a moving bus to an elevated platform.   

==Cast==
* Pearl White - Pearl Travers
* Warren William - Mr. Jones (as Warren Krech)
* Harry Semels - Jud Deering
* Tom McIntyre
* William Nally
* Wally Oettel
* Edward J. Pool
* Charles Patch Revada

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 