The Uninvited (2003 film)
 
{{Infobox film name           = The Uninvited  image          = The Uninvited (2003 film).jpg caption        = Theatrical poster director       = Lee Su-yeon producer       = Oh Jung-wan   Jeong Hoon-tak writer         = Lee Su-yeon  starring       = Jun Ji-hyun Park Shin-yang cinematography = Jo Yong-gyu music          = Jang Young-gyu editing        = Kyung Min-ho distributor    = CJ Entertainment released       =   runtime        = 125 minutes country        = South Korea language       = Korean budget         =  film name      = {{Film name hangul         = 4  hanja          = 4  rr             = 4 Inyong shiktak mr             = 4 Inyong sikt‘ag}}
}} 2003 South Korean psychological horror film directed by Lee Su-yeon.

==Plot==
Kang Jung-won (Park Shin-yang), an interior decorator, is overcome with inexplicable anxiety as his long-overdue wedding with Hee-eun (Yoo Sun) approaches. One evening, Jung-won falls asleep on the subway on his way home. He is barely able to wake up at the last station. As he comes round, he sees two young girls asleep on the seat next to him. He cannot wake them before he has to jump off as the train leaves the station. He arrives home to find that his wife-to-be has bought them a new metal dining table.

The next day, Jung-won is working when he hears on the radio that two young girls were found poisoned on the subway. In the course of fitting some lights in a ceiling, he is hit by falling debris and cuts his forehead. After a trip to the hospital for some stitches, he goes home to find the two dead girls seated at his new dining table.

Jung-won, who is now working on renovating a psychiatrists office, bumps into Jung Yeon (Jun Ji-hyun), a patient on her way out of a therapy session. She has been receiving treatment after her friend, Moon Jung-sook (Kim Yeo-jin), killed both of their children a year earlier. Another accident leads to Jung-won taking Yeon back to his apartment where she too sees the apparition of the dead children.

Having been tormented by nightmares and the hallucinations, Jung-won is desperate to find out something about the apparitions that haunt him. Yeon runs away refusing to help him, so he searches through the patients records at the clinic to find out more about her. Using the information he succeeds in persuading her to help him uncover his past. Jung-won discovers that he had been born with shamanic abilities similar to Yeon and accidentally killed his father and sister.

Unfortunately, he suffers the consequences rediscovering his past. His fiancee Hee-eun suspects that he is having an affair and leaves him. Jung-sook is convicted of the murder of Yeons child, but she suddenly commits suicide as she is leaving the courthouse. Shocked, Yeon calls Jung-won who comes over to console her. He talks to her husband, Park Moon-sub (Park Won-sang), who suspects that it was his wife, not her dead friend, who killed their children.

Jung-won, caught up in his desire to deny his past and his fear of Yeon, turns down Yeons cry for help when her husband tries to have her committed to a mental hospital. His refusal crushes Yeon, who throws herself off Jung-wons apartment building. Jung-won sees her as she falls. In the final scene, Jung-won sits in his dust-covered apartment. Face lined like an old man, he brings a steaming dish of food to the dining table and sits down.

His dining table is full, not with the family he had been planning but the apparitions of the two poisoned girls on the subway and Yeon.

==Critical reception==
Reception to the film was mixed. Koreanfilm.org called it a "striking film driven by a strong vision" but sometimes it felt "like a flawed work." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 