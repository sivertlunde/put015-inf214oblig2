Naked Hearts
{{Infobox film
| name           = Naked Hearts
| image          = 
| caption        = 
| director       = Édouard Luntz
| producer       = Raoul Ploquin
| writer         = Édouard Luntz
| starring       = Gérard Zimmermann
| music          = 
| cinematography = Jean Badal
| editing        = Colette Kouchner
| distributor    = 
| released       = 1966
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
}}

Naked Hearts ( ) is a 1966 French drama film directed by Édouard Luntz. It was entered into the 16th Berlin International Film Festival.   
 greasers (referred to in French as blousons noirs), and meet in prison at the beginning of the film. The narrative traces their efforts to negotiate the dynamics of the group, find a job, and stay out of trouble (both are on provisional release after their initial brush with the law).

Naked Hearts is unusual for its period in its use of non-professional actors and for its mix of narrative and documentary elements. The group of young men at the center of the film is composed of residents of Nanterre, and Luntz purportedly worked closely with them in writing the story. Segments of voiceover appear at several points in the film, apparently recorded conversations with young people from the area.

==Cast==
* Gérard Zimmermann - Zim
* Marise Maire - Jacqueline
* Eric Penet - Jean-Pierre
* Françoise Bonneau - Patricia
* Arlette Thomas - Jean-Pierres Mother
* Elliott Stein
* Nat Lilienstein

==References==
 

==External links==
* 

 
 
 
 
 
 
 