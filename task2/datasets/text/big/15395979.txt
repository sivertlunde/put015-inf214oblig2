Bianco, rosso e Verdone
{{Infobox film
| name           = Bianco, rosso e Verdone
| image          = Bianco, rosso e Verdone.jpg
| image size     =
| caption        = Film poster
| director       =  Carlo Verdone
| producer       = Sergio Leone
| writer         = Leonardo Benvenuti, Piero De Bernardi, Carlo Verdone
| narrator       =
| starring       = Carlo Verdone, Mario Brega, Elena Fabrizi
| music          = Ennio Morricone
| cinematography = Luciano Tovoli
| editing        = Nino Baragli
| distributor    = Medusa Produzione
| released       = 20 February 1981
| runtime        = 110 minutes
| country        = Italy Italian
| budget         =
}}
 1981 Italy|Italian comedy film directed and starred by Carlo Verdone, playing three characters.

It was produced by Sergio Leone, soundtrack composed by Ennio Morricone and guest starred by Mario Brega, all formerly scored in the Dollars trilogy and spaghetti western movies in the 1960s.

==Cast==
* Carlo Verdone as Furio, Mimmo and Pasquale Ametrano
* Irina Sanpiter as  Magda, Furios wife
* Elena Fabrizi as  Mimmos grandmother
* Milena Vukotic as  a call-girl
* Mario Brega as  "The Prince" truck driver
* Angelo Infanti as Raul the seducer
* Andrea Aureli as Mimmos Uncle
* Elisabeth Wiener as Pasquales German wife
* Anna Alessandra Ariorio
* Vittorio Zarfati as the motel doorkeeper
* Giovanni Brusadori
* Guido Monti
* Giuseppe Pezzulli

==Plot==
Italian election day in the early 80s. Three men leave to reach their voting places. Furio, a pedantic and chatterbox clerk living in Turin, is with his family on his way to Rome, so are half witted Mimmo with his diabetic grandmother. Pasquale, an Italian emigrant in Germany and married to a local valkyrie| valkyrie-like  woman, leaves alone with his Alfasud car to Matera, South Italy.
Theirs is an eventful journey through Italian motorways. Furios wife Magda is on the edge of a nervous breakdown because of his character. Mimmo is continuously mocked by his disabled but smart grandmother. Pasquale, ignoring all current hardships of Italy, gets some theft in each stop.
Magda, Mimmo and their relatives spend a night in the same motel, while Furio stays at hospital because of a car crash. The woman is courted by Raul, a handsome man that follows them from the beginning. Mimmo is taken by a call-girl who works in the motel. Hes so idiot to ignore her job and he mistakes her pubic hair with a "fur underwear". Pasquales car eventually miss seats, windscreen and decorative wheel rims.
They eventually get to their polling stations. Magda escapes with Raul while Furio is voting. Mimmos grandma dies in the voting cabin and the scrutiners argue the validity of her vote while Mimmo is crying and screaming. Pasquale gives to the scrutiners his anger. He speaks a fictitious and unintelligible dialect, upon his misfortunes and hardships of Italy and uselessness of his vote.

==Comment==
Verdones second movie, coming through his successful Fun is beautiful (Un sacco bello). He uses the same formula of the first work, as director and multi-role starring. The election day as background to mock the miseries and hardships of Italian society, especially in the emigrants misfortunes. The producer Sergio Leone was formerly afraid the character Furio would result hateful to viewers, so he was of an old aged and actual diabetic Lella Fabrizis performance. Hes latter agreed with Verdones choices. The obsessive, pedantic Furio character often falls in Verdones works, telling to be inspired by ones of his relatives. Russian actress Irina Sanpiter was chosen because of her big blue eyes and angelical, pale face. Pasquales dated character stereotypes the helpless Italian emigrant, emphasyzed by his untidy and old fashioned wear, garish car decoration, listening of Italian 50s music.

==External links==
*  

 
 
 
 
 
 
 
 