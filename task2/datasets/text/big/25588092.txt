Alice in Wonderland (1931 film)
{{Infobox Film
| name           = Alice in Wonderland
| image          = alice-poster-1931.jpg
| image_size     = 
| caption        = 
| director       = Bud Pollard
| producer       = Hugo Maienthau
| writer         = Lewis Carroll (book) John E. Goodson (adaptation) Ashley Ayer Miller (screenplay)
| narrator       =  Ruth Gilbert Leslie King Pat Gleason Ralph Hertz Meyer Berensen
| music          =  Charles Levine
| based on       =  
| editing        = Bud Pollard
| studio         = Metropolitan Studios
| distributor    = Unique Foto Films
| released       = 30 September 1931 
| runtime        = 58 min
| country        = United States
| language       = English
| budget         = 
}}

Alice in Wonderland (1931) is an independently made black-and-white American film based on Lewis Carrolls 1865 novel Alices Adventures in Wonderland, directed by Bud Pollard, produced by Hugo Maienthau, and filmed at Metropolitan Studios in Fort Lee, New Jersey.
 Alice and Warner Theatre in New York City.

==Synopsis== Duchess (Mabel Dormouse (Raymond Schultz), while the Cheshire Cat (Tom Corless) leaves his grin behind.
 Caterpillar (Jimmy Queen of Gryphon (Charles Silvern), and at a bizarre trial, Alice finally becomes fed up with all the strange events and people. 

==Cast== Ruth Gilbert as Alice
*Leslie T. King as Mad Hatter
*Ralph Hertz as White Rabbit
*Vie Quinn as Queen of Hearts
*N. R. Cregan as King of Hearts
*Pat Glasgow as Knave of Hearts
*Mabel Wright as Duchess
*Lillian Ardell as Cook
*Tom Corliss as Cheshire Cat
*Meyer Beresen as March Hare
*Raymond Schultz as Dormouse
*Charles Silvern as Gryphan
*Gus Alexander as Mock Turtle
*Jimmy Rosen as Caterpillar

==Background==
  Broadway adaptation that combined Alice in Wonderland with Through the Looking Glass, and which was one of the hits of the year.
 Alice in Wonderland which starred an unknown, Charlotte Henry, with an all-star cast that featured  W.C. Fields, Cary Grant and Gary Cooper. In 1932, Alice Liddell, the inspiration for the Alice of the original books, and by now an elderly lady, visited America to take part in these centenary celebrations. 
 Warner Theatre in New York. However, the film was not financially successful and received little critical attention. Today, it is rarely if ever shown, and for a time there was even some doubt as to whether prints of it still existed. It has never been shown on television. 

==References==
 

==External links==
* 
* , December 28, 1931
* 
* 

 

 
 
 
 
 
 
 