He Comes Up Smiling
{{infobox film
| name           = He Comes Up Smiling
| image          =File:He Comes Up Smiling poster.jpg
| imagesize      =
| caption        =Film poster
| director       = Allan Dwan
| producer       = Douglas Fairbanks
| writer         = Charles Sherman (novel He Comes Up Smiling) Byron Ongley (play) Emil Nyitray (play) Frances Marion (scenario) Theodore Reed (scenario editor)
| starring       = Douglas Fairbanks
| music          =
| cinematography = Joseph H. August Hugh McClung
| editing        =
| studio         = Famous Players-Lasky/Artcraft
| distributor    = Paramount Pictures
| released       = September 8, 1918
| runtime        = 5 reels (4,876 feet)
| country        = United States
| language       = Silent English intertitles
}}
He Comes Up Smiling is a 1918 comedy produced by and starring Douglas Fairbanks and directed by Allan Dwan.

This film was based on a novel by Charles Sherman He Comes Up Smiling which was adapted into a 1914 play of the same name by Byron Ongley and Emil Nyitray. Fairbanks starred in the play with Patricia Collinge as the female lead. This film "survives incomplete". 

==Cast==
*Douglas Fairbanks - Jerry Martin Marjorie Daw - Billie Bartlett
*Herbert Standing - Mike
*Frank Campeau - John Bartlett
*Bull Montana - Baron Bean
*Albert MacQuarrie - Batchelor
*Kathleen Kirkham - Louise
*Jay Dwiggins - General
*William Elmer 
*Robert Cain

==References==
 

==External links==
* 
* 
*  at silentera.com
* 

 
 
 
 
 
 
 
 
 


 