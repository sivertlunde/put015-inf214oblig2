...First Do No Harm
 
{{Infobox film
| name           = ...First Do No Harm
| image          = ...first do no harm.jpg
| director       = Jim Abrahams
| producer       = Jim Abrahams
| writer         = Ann Beckett Walt Disney Video Tom Butler Mairon Bennett Michael Yarmush Millicent Kelly Diana Belshaw
| released       =  
| runtime        = 94 minutes
| language       = English
| music          = Hummie Mann
| cinematography = Pierre Letarte
| editing        = Terry Stokes
}} medications with terrible side effects, is controlled by the ketogenic diet. Aspects of the story mirror Abrahams own experience with his son Charlie.

==Plot== CT scan, epileptic seizures and the child diagnosed with epilepsy.

Robbie is started on phenobarbital, an old anticonvulsant drug with well-known side effects including cognitive impairment and behavior problems. The latter cause the child to run berserk through the house, leading to injury. Lori urgently phones the physician to request a change of medication. It is changed to phenytoin (Dilantin) but the dose of phenobarbital must be tapered slowly, causing frustration. Later, the drug carbamazepine (Tegretol) is added.

Meanwhile, the Reimullers discover that their health insurance is invalid and their treatment is transferred from private to county hospital. In an attempt to pay the medical bills, Dave takes on more dangerous truck loads and works long hours. Family tensions reach a head when the children realize the holiday is not going to happen and a foreclosure notice is posted on the house.
 intravenously to no effect. Eventually, paraldehyde is given rectum|rectally. This drug is described as having possibly fatal side effects and is seen dramatically melting a plastic cup (a glass syringe is required).

The neurologist in charge of Robbies care, Dr. Melanie Abbasac (Allison Janney), has poor bedside manner and paints a bleak picture. Abbasac wants the Reimullers to consider surgery and start the necessary investigative procedures to see if this is an option. These involve removing the top of the skull and inserting electrodes on the surface of the brain to achieve a more accurate location of any seizure focus than normal scalp EEG electrodes. The Reimullers see surgery as a dangerous last resort and want to know if anything else can be done.

Lori begins to research epilepsy at the library. After many hours, she comes across the   in Baltimore, Maryland with continual medical support—something they cannot afford.

That evening, Lori attempts to abduct her son from the hospital and, despite the risk, fly with him to an appointment she has made with a doctor at Johns Hopkins. However, she is stopped by hospital security at the exit to the hospital. A sympathetic nurse warns Lori that she could lose custody of her son if a court decides she is putting her sons health at risk.

Dave makes contact with an old family friend who once practiced as a physician and is still licensed. This doctor and the sympathetic nurse agree to accompany Lori and Robbie on the trip to Baltimore. During the flight, Robbie has a prolonged convulsive seizure, which causes some concern to the pilot and crew.

When they arrive at Johns Hopkins, it becomes apparent that Lori has deceived her friends as her appointment (for the previous week) was not rescheduled and there are no places on the ketogenic diet program. After much pleading, Dr. Freeman agrees to take Robbie on as an outpatient. Lori and Robbie stay at a convent in Baltimore.
 fast that is used to kick-start the diet. Despite the very high-fat nature of the diet, Robbie accepts the food and rapidly improves. His seizures are eliminated and his mental faculties are restored. The film ends with Robbie riding the family horse at a parade through town. Closing credits claim Robbie continued the diet for a couple of years and has remained seizure- and drug-free ever since.

==Background== John Freeman, director of the Pediatric Epilepsy Center at Johns Hopkins Hospital. Charlie was started on the diet and rapidly became seizure-free. In addition, medications were tapered and his mental development restored. Abrahams was outraged that nobody had informed him of the diet. He created the Charlie Foundation to promote the diet and funded research studies to demonstrate its effectiveness.

Although the film plot has parallels with the Abrahams story, the character of Robbie is a composite one and the family circumstances are fictional. Several minor characters in the film are played by people who have been on the ketogenic diet and had their epilepsy "cured" as a result. The dietitian Millicent Kelly plays herself. Charlie Abrahams appears as a young boy playing with Robbie in the hospital, whose mother quickly removes him when she discovers Robbie has epilepsy—as though it were an infectious disease.
 John Freeman said "The movie was based on a true story and we see this story often, but not everyone is cured by the diet and not everyone goes home to ride in a parade." He later noted that the film had "fueled a grass-roots effort for more research on the diet."

The ketogenic diet was developed by Russel Wilder at the Mayo Clinic in 1921. The diet aims to reproduce some of the metabolic changes seen during fasting. Fasting had been shown to be effective in treating epilepsy but is obviously not sustainable. Although initially popular in all age groups, the diet was largely replaced by effective anticonvulsant medications beginning with phenytoin in 1938. It remained a treatment of last resort in children with intractable seizures. Since the film was produced, the diet has seen a dramatic revival with numerous published research studies and it is now in use at 75 epilepsy centers in 45 countries.

The film was first aired on CityTV and ABC for public viewing on 16 February 1997. It is available on DVD from retailers and directly from the Charlie Foundation. Meryl Streeps performance was nominated for an Emmy, a Golden Globe and in the Satellite Awards in the category Best Actress in a TV Film. Writer Ann Beckett was nominated for the Humanitas Prize (90&nbsp;minute category). Seth Adkins won a Young Artist Award for his performance as Robbie.

==See also==
* Never events

==References==
* {{cite web
 | url = http://www.charliefoundation.org/noframes/whoweare/essay.php
 | title = Things I Wish They Had Told Us: A Parents Perspective on Childhood Epilepsy
 | accessdate = 2008-03-30
 | author =Jim Abrahams
 | year = 2003
 | publisher = The Charlie Foundation archiveurl = archivedate = 2008-02-13}}
* {{cite journal
 | url = http://www.hopkinsneuro.org/epilepsy/research.cfm?research=A_Talk_with_John_Freeman.htm
 | title = Talk with John Freeman: Tending the Flame
 | accessdate = 2008-03-30
 | volume = 16
 | issue = 2
 | year = 2003
 | author = John Freeman
 | journal = Brainwaves
| archiveurl= http://web.archive.org/web/20080507111717/http://www.hopkinsneuro.org/epilepsy/research.cfm?research=A_Talk_with_John_Freeman.htm| archivedate= 7 May 2008 }}
* {{cite news
 | title = Movie First Do No Harm Boosts Popularity of Diet for Epileptic Children
 | date = 2000-07-06
 | author =Denise Mann
 | publisher = WebMD Medical News
}}
* {{cite web
 | url = http://www.epilepsyontario.org/client/EO/EOWeb.nsf/web/First+Do+No+Harm+(Movie)
 | title = ...first do no harm
 | author = Venita Jay
 | accessdate = 2008-03-30
 |date=April 1997
 | publisher = Epilepsy Ontario Sharing News
}}
* {{cite news
 | url = http://www.charliefoundation.org/articles/usa-today-reports-%E2%80%98miracle%E2%80%99-diet-treat-children-epilepsy%E2%80%93-11999
 | title = Recognizing a miracle The high-fat ketogenic diet can ease seizures in epileptic children
 | author = Kathleen Fackelmann
 | accessdate = 2010-07-23
 | date = 1999-01-12
 | publisher = USA Today
}}
* {{cite journal
 | author = Freeman JM, Kossoff EH, Hartman AL
 | url = http://pediatrics.aappublications.org/cgi/content/full/119/3/535
 | title = The ketogenic diet: one decade later
 | journal = Pediatrics
 |date=March 2007
 | volume = 119
 | issue = 3
 | pages = 535–43
 | pmid = 17332207
 | doi = 10.1542/peds.2006-2447
}}

==External links==
*  
*  
*   A US charity and information resource, set up by Jim Abrahams.

 

 
 
 
 
 
 
 
 
 
 