Pandem
{{Infobox film
| name           = Pandem
| image          =
| caption        =
| writer         = Veegesna Satish  
| screenplay     = Sabhapati 
| producer       = Valluripally Ramesh Babu Sabhapati
| Kalyani
| Chakri
| cinematography = Sarath
| editing        = Basva Paidireddy
| studio         = Maharshi Cinema   
| released       =  
| runtime        = 132 minutes
| country        = India
| language       = Telugu
| budget         =
}}
 Kalyani in the lead roles and music composed by Chakri (music director)|Chakri.            

==Cast==
 
* Jagapati Babu as Seenu Kalyani as Seeta 
* Ramraju as Lingaraju
* Ahuti Prasad as Satya Murthy
* M. S. Narayana as Surinarayana Kondavalasa as Jilla Rambabu
* Krishna Bhagawan as Muriki Appala Naidu
* Sivaji Raja  as Abbulu Chinna as RMP Doctor Pruthvi Raj 
* Ravi Varma as Rajesh
* Subbaraya Sharma as Villager
* Sarika Ramchandra Rao as Pamula Narsaiyya 
* Chitram Basha   Jeeva as Police Inspector
* Apoorva
* Mahathi as Pankajam
* Drasharamam Saroja as Saroja
* Lavanya
* Shobha Rani
* Sakhi
 

==Soundtrack==
{{Infobox album
| Name        = Pandhem
| Tagline     = 
| Type        = film Chakri
| Cover       = 
| Released    = 2005 
| Recorded    = 
| Genre       = Soundtrack
| Length      = 21:52
| Label       = Aditya Music Chakri
| Reviews     =
| Last album  = Dhana 51   (2005) 
| This album  = Pandem   (2005) 
| Next album  = * Chakram (2005 film)|Chakram   (2005)
}}

Music composed by Chakri (music director)|Chakri. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 21:52
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Kukku Kukku 
| lyrics1 = Vennelakanti Kousalya
| length1 = 4:50

| title2  = Guppedu Gundela
| lyrics2 = Vennelakanti
| extra2  = Chakri,Kousalya
| length2 = 4:36

| title3  = Ammadu Gummadu
| lyrics3 = Bhaskarabhatla
| extra3  = Shankar Mahadevan,Kousalya
| length3 = 3:45

| title4  = Ammalaganna Ammalara
| lyrics4 = Sahithi
| extra4  = Adarshini, Matin, Simha, Vasu
| length4 = 4:18

| title5  = Abbayo Vayasu
| lyrics5 = Kandikonda 
| extra5  = Ravi Varma,Sunanda
| length5 = 4:09
}}
   

==References==
 

 
 
 


 