Lighthouse Hill (film)
{{Infobox film
| name           = Lighthouse Hill
| image          = 
| alt            = 
| caption        = 
| director       = David Fairman
| producer       = David Fairman Hamish Skeggs
| writer         = Sharon Y. Cobb
| starring       = 
| music          = Christopher Gunning
| cinematography = Tony Imi
| editing        = Alan Strachan
| studio         = 
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} British comedy film directed by David Fairman and starring Jason Flemyng, Kirsty Mitchell and Frank Finlay. 

==Cast==
* Jason Flemyng - Charlie Davidson 
* Kirsty Mitchell - Grace Angelini
* Frank Finlay - Alfred 
* Kulvinder Ghir - Raymonburr 
* John Sessions - Mr Reynard 
* Katie Sheridan - Young Grace
* Julie T. Wallace - Bunny 
* Annabelle Apsion - Honey 
* Maureen Lipman - Audrey Davidson  Samantha Janus - Jennifer
* Samantha Beckinsale - Sally 
* Mark Benton - Peter 
* David Bowles - Mac

==References==
 
==External links==
* 

 
 
 
 
 

 