Jack the Ripper (1976 film)
 
{{Infobox film
| name           = Jack the Ripper
| image	         = Jack the Ripper FilmPoster.jpeg
| caption        = Film poster
| director       = Jesús Franco
| producer       = Erwin C. Dietrich Max Dora
| writer         = Jesús Franco
| starring       = Klaus Kinski
| music          = 
| cinematography = Peter Baumgartner Peter Spoerri
| editing        = Marie-Luise Buschke
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = West Germany Switzerland
| language       = German
| budget         = 
}}

Jack the Ripper ( ) is a 1976 German thriller film directed by Jesús Franco and starring Klaus Kinski.    In this Swiss-German film Klaus Kinski portrays Jack the Ripper.   

==Cast==
* Klaus Kinski as Dr. Dennis Orloff / Jack the Ripper
* Josephine Chaplin as Cynthia
* Andreas Mannkopff as Inspektor Selby
* Herbert Fux as Charlie, the Fisherman
* Lina Romay as Marika Stevenson
* Nikola Weisse as Frieda
* Ursula von Wiese as Miss Higgins (as Ursula v. Wiese)
* Hans Gaugler as Mr. Bridger, the blind man
* Francine Custer as Sally Brown, first victim
* Olga Gebhard as Ms. Baxter
* Angelika Arndts as Ms. Stevenson
* Peter Nüsch as Sergeant Ruppert (as Peter Nuesch)
* Esther Studer as Jeanny, second victim
* Regine Elsener as Blondy
* Lorli Bucher as Lulu
* Mike Lederer as Coach Driver
* Otto Dornbierer as Charlies friend

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 
 