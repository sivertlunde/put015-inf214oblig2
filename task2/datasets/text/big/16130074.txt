Ang Lihim ni Antonio
{{Infobox film
| name           = Ang Lihim ni Antonio
| image          = Ang-lihim-ni-antonio.jpg
| caption        = Theatrical release poster
| director       = Joselito Altarejos
| writer         = Lex Bonife
| starring       = Kenji Garcia  Jiro Manio Ferdinand Zandro Salonga Sharmaine Buencamino Josh Ivan Morales Nino Fernandez
| producers      = Joselito Altarejos Vicente del Rosario III, Valeria del Rosario, Hermie Go
| cinematography = Arvin Viola
| music          = Ajit Hardasani
| distributor    = Viva Films (through Digital Viva Productions)
| released       =  
| runtime        = 100 minutes
| country        = Philippines
| language       = Tagalog
}} Filipino film director Joselito Altarejos. It tells the story about teenage boy whose emerging gay sexuality alienates him from his friends and family, until his libertine uncle, Jonbert (Josh Ivan Morales), comes to live with him and his mother. Antonio thinks he has found a kindred spirit, until the older mans intentions toward the boy become incestuous and an act of unthinkable violence leaves the family reeling. 

==Plot==
Antonio (Kenji Garcia) is a curious fifteen-year-old boy who is beginning to come to terms with his own sexuality. Although his straight best friend, Mike (Jiro Manio), has been supportive of his coming out, his first sexual conquest has led to the destruction of his friendship with his other best buddy, Nathan (Ferdinand Zandro Salonga).

Antonio’s exploration of his identity unfolds as his family begins to break up. His altruistic mother, Tere (Shamaine Buencamino) is in complete denial that his father has already abandoned them.

Antonio temporarily shares his room with his uncle Jonbert (Josh Ivan Morales). He sexually touches his uncle while the latter is sleeping. The next day Jonbert tells Antonio that he noticed what Antonio did, and enjoyed it. From then on they regularly masturbate each other and have oral sex. However, one day Jonbert wants to anally penetrate Antonio, and rapes him when he declines. His mother arrives and kills Jonbert.

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Kenjie Garcia || Antonio
|-
| Jiro Manio || Mike
|-
| Shamaine Buencamino || Tere
|-
| Josh Ivan Morales || Uncle Jonbert
|-
| Ricky Ibe || Eli
|-
| Joey Deleon || Tonet
|}

==Soundtrack==
Music of the film is performed by Ajit Hardasani entitled "Awit Para Kay Antonio" (Song for Antonio). Lyrics by Lex Bonife and music by Ajit Hardasani. 

==Location==
It was shot entirely in Marikina City.

==References==
 

==External links==
*  

 
 
 
 
 