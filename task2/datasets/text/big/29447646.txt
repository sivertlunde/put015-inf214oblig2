Un clair de lune à Maubeuge
{{Infobox film
| name           = Un clair de lune à Maubeuge
| image          = 
| caption        = 
| director       = Jean Chérasse
| producer       = Edic, Ardennes Films, C.F.C
| writer         = Claude Choublier Pascal Bastia
| starring       = Claude Brasseur Louis de Funès Pierre Perrin
| cinematography = 
| editing        = 
| distributor    = 
| released       = 19 December 1962 (France)
| runtime        = 106 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy film from 1962, directed by Jean Chérasse, written by Claude Choublier, starring Claude Brasseur and Louis de Funès (uncredited). The film was known under the title Moonlight in Maubeuge (international English title). 

== Cast ==
*Claude Brasseur: Walter, the right hand of Tonton Charly Pierre Perrin: Paul Prunier, the taxi compositor
*Bernadette Lafont: Charlotte, the secretary
*Rita Cadillac: Monique, a secretary
*Michel Serrault: Charpentier, the lecturer
*Jean Carmet: Fernand, the driver
*André Bourvil: Lui-même, singing the song to television
*Jacques Dufilho: the director of the Maison de la Radio
*Jean Lefebvre: a miner Robert Manuel: Tonton Charly, the manager of Superdisco music
*Maria Pacôme: the journalist
*Louis de Funès (uncredited)

== References ==
 

== External links ==
*  
*   at the Films de France

 

 

 
 
 
 
 
 