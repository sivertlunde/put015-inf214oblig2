Nude per l'assassino
 
{{Infobox film
| name           = Nude per lassassino
| image          = Nude per lassassino.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Italian poster
| director       = Andrea Bianchi
| producer       = Silvestro DeRossi   Sergio Simonetti   Film & TV Database   Nude per lassassino (1975)   Full Credits  |publisher=British Film Institute |accessdate=May 26, 2012}} 
| screenplay     = Massimo Felisatti 
| story          = Andrea Bianchi
| starring       = Solvi Stubing   Nino Castelnuovo   Edwige Fenech Erna Schürer
| music          = Berto Pisano
| cinematography = Franco Delli Colli
| editing        = Francesco Bertuccioli   Claudio Cumo 
| studio         = Fral Cinematografica 
| distributor    = Golden Era 
| released       =  
| runtime        = 97 minutes 
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}

Nude per lassassino is a 1975 giallo film directed by Andrea Bianchi. It has been released on home media under the title Strip Nude for Your Killer. Written by Massimo Felisatti, the film stars Nino Castelnuovo, Edwige Fenech and Solvi Stubing, and features music by Berto Pisano. Nude per lassassino has received mixed to negative reviews from critics.

After a model dies while undergoing an illegal abortion, a killer disguised in a motorcycle helmet tracks down and kills the doctor involved. Several models and photographers working at the same agency as the dead girl are also murdered, leading two surviving photographers to fear for their lives as they try to track down the killer.

Nude per lassassino is one of several collaborations between Bianchi, Felisatti and cinematographer Franco Delli Colli. The films has been described as a formulaic giallo thriller, and has been cited as an influence on the development of the American slasher film genre.

== Plot ==
 racing leathers and a motorcycle helmet. At the Albatross Modelling Agency, womanising photographer Carlo (Nino Castelnuovo) embarks on an affair with fellow photographer Magda (Edwige Fenech). Meanwhile, a newly hired model, Patrizia (Solvi Stubing), fends off the unwanted advances of Maurizio (Franco Diogene), whose wife owns the agency. One evening, Mario (Claudio Pellegrini), who also works at the agency, invites a woman in racing leathers into his home for a drink, and is stabbed to death. The police question the agencys owner Gisella (Lia Amanda) and another model, Lucia (Femi Benussi) about the killing, but learn very little. Gisella and Lucia are sleeping together; one night when Gisella leaves after a fight, Lucia is attacked and killed.

Maurizio propositions another model, Doris (Erna Schürer), but when she refuses to sleep with him for money, he attempts to rape her. However, he suffers from premature ejaculation and Doris leaves unharmed. Shortly afterwards, Maurizio is stabbed by the killer. Carlo later witnesses Gisella being murdered, and is able to photograph the attack; however, he runs off and is injured in a hit and run accident. While he is in hospital, Magda recovers his camera and attempts to develop the film, but the killer breaks in and destroys the negatives. Carlo hurries home, but the killer has gone—going instead to kill Doris and her abusive boyfriend Stefano. Carlo finds Madga alive, but the killer returns to attack them both. During the struggle, the killer is knocked down a flight of stairs. The killer is unmasked and revealed to be Patrizia, who accuses Carlo of causing the death of her sister—the girl who died in the botched abortion, and whose death it is revealed Carlo helped to conceal. However, Patrizia dies of her injuries, leaving no trace of Carlos involvement.

== Production ==
 La moglie di mio padre.  Nude per lassassino  score was written by Berto Pisano, who would also go on to write the score for Bianchis 1979 film Malabimba.   Film & TV Database   Pisano, Berto |publisher=British Film Institute |accessdate=May 26, 2012}}  Bianchi would later cast Femi Benussi, who portrayed one of the slain models, in two other films—La moglie di mio padre and Cara dolce niopte.   Film & TV Database   Benussi, Femi |publisher=British Film Institute |accessdate=June 22, 2012}} 
 Bava and Argento and elaborated upon by a number of directors in the early 1970s had become well codified" by the time the film was produced.  The film has also been cited as being "the perfect bridge to the American slasher film", with its emphasis on "violence and sex" and a plot "dumbed down to the barest minimum". 

== Release ==
 X by the British Board of Film Classification, following the removal of five minutes of material from the film.  It has subsequently received home media releases in English by Blue Underground as Strip Nude for Your Killer, first on DVD on October 25, 2005,  and on Blu-ray Disc on March 27, 2012.  Shameless Screen Entertainment also released a DVD version under the title Strip Nude for Your Killer on October 27, 2008.  The film has also been distributed under the titles Tenebre braccia della morte and Strip Naked for Your Killer. 

== Reception ==
Nude per lassassino has been met with mixed to negative reviews. Writing for Allrovi, Jason Buchanan rated the film two stars out of five, calling it "unabashedly sadistic".    Budd Wilkins, writing for Slant (magazine)|Slant magazine, rated the film three stars out of five, calling it "one of the more sordid examples" of the giallo genre. Wilkins noted that the films violence "isnt necessarily stronger than in contemporary giallo films like Argentos Deep Red", but that it is "more resolutely tied to aberrant sexuality than almost anywhere else in the genre". Wilkins also compared the films central premise—that of revenge for a failed abortion—to that of Massimo Dallamanos 1972 film What Have You Done to Solange?.   DVD Review |publisher=Slant (magazine)|Slant |first=Budd |last=Wilkins |date= May 4, 2012 |accessdate=May 26, 2012}}  DVD Talks Adam Tyner also gave the film two stars out of five, summarising it as "not much of a movie". Tyner noted that Nude per lassassino "doesnt set out to be revered as an artistic triumph", and described it as "worth at least a rental" for fans of the genre.   

Writing for The A.V. Club, Noel Murray compared the film to Sergio Martinos 1972 film Your Vice Is a Locked Room and Only I Have the Key (Il tuo vizio è una stanza chiusa e solo io ne ho la chiave), noting that "both take place among the idle European aristocracy, with vapid models, rugged motocross drivers, bigoted executives, and debauched artists wandering through a world of soft fabrics and bloody, gashed skin".  Bloody Disgustings Mike Pereira rated Nude per lassassino 0.5 out of 5, writing that it "reeks of amateur hour". Pereira felt that the film featured "dead-on-arrival pacing" and "terrible filmmaking and acting    A staff review for the Italian magazine Nocturno (magazine)|Nocturno rated the film four out of five, describing it as "one of the most daring" giallo films of the 1970s.     Previewing the films Blu-ray release, IGNs David McCutcheon described Nude per lassassino as an "infamous shocker that packs more grisly violence and sexual depravity into each frame than just about any other film". 

== Notes ==

 

== Footnotes ==

 

=== References ===

* 
* 

== External links ==

*  
*  

 

 
 
 
 
 
 
 
 