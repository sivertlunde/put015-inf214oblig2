Johnny English Reborn
 
 
{{Infobox film
| name = Johnny English Reborn
| image = Johnny English Reborn Poster.jpg 
| alt = Johnny English Reborn poster
| caption = UK theatrical release poster
| director = Oliver Parker
| producer = Tim Bevan Eric Fellner Chris Clark William Davies
| screenplay = Hamish McColl Robert Wade
| starring = Rowan Atkinson Gillian Anderson Dominic West Rosamund Pike Daniel Kaluuya Richard Schiff
| music = Ilan Eshkeri Danny Cohen
| editing = Guy Bensley
| studio = StudioCanal Relativity Media Working Title Films Universal Pictures
| released =  
| runtime = 101 minutes 
| country = United Kingdom France
| language = English
| budget = $45 million   
| gross = $160,078,586 
}} British spy spy comedy film parodying the James Bond secret agent genre. The film is the sequel to Johnny English (2003), and stars Rowan Atkinson reprising his role as the title character    and directed by Oliver Parker.
 James Bond Casino Royale spy genre,  Johnny English Reborn was met with mixed reviews but has grossed a total of $160,078,586 worldwide.

==Plot==
Five years after the events of the first film, former Sir Johnny English (Rowan Atkinson) is learning martial arts in Tibet as penance for a botched mission in Mozambique which resulted in him being stripped of his Knighthood. However, he is contacted by MI7, who request that he returns to service.

Johnny returns to MI7s London headquarters (now Toshiba British Intelligence) and assigned by new boss "Pegasus" (Gillian Anderson) to stop a plot to assassinate the Chinese Premier during scheduled talks with the Prime Minister. Johnny also meets with fellow agent Simon Ambrose (Dominic West) and MI7s resident inventor, Patch Quartermain (Tim McInnerny). He is also assigned a junior agent, Colin Tucker (Daniel Kaluuya).
 CIA agent Titus Fisher (Richard Schiff). Fisher reveals that he is a member of a group of assassins called Vortex, who sabotaged Englishs mission in Mozambique. Vortex holds a secret weapon, which requires three metal keys to unlock, and Fisher reveals one. Fisher is killed by an assassin (Pik-Sen Lim) disguised as a cleaner and the key falls into the hands of a Vortex member. A foot chase along the buildings of Hong Kong results in English retrieving the Key from the agent. On a flight back to London, Johnny gives a suitcase containing the key to a flight attendant, who is revealed to be a Vortex agent. English is humiliated in front of the Foreign Secretary and Pegasus by the loss of the key, and assaults Pegasuss mother, mistaking her for the killer cleaner. He escapes the situation on a tandem bicycle while wearing a suit.

Kate Sumner (Rosamund Pike), MI7s behavioural psychologist, prompts English to recall the events of his mission in Mozambique, and the identity of the second Vortex operative, Karlenko (Mark Ivanir), a Russian spy.  Johnny meets him at an exclusive golf course outside of London. As they are playing golf, the same assassin from Hong Kong reappears and uses a sniper rifle against Karlenko. Johnny takes Karlenko and escapes the golf course in a helicopter. They fly to the nearest hospital using the roads as a way of direction. As they arrive, Karlenko reveals that Vortexs last agent is a member of MI7, and dies.

In a meeting at MI7, it is revealed that talks between Britain and China will continue in a heavily guarded fortress called Le Bastion in the Swiss Alps. Over dinner, English informs Ambrose that he knows of a mole in MI7. Ambrose prepares to kill English, until it becomes clear that the traitors identity is unknown. Tucker confronts Ambrose in the bathroom, knowing he is the mole, but English scolds Tucker and orders him to leave. Ambrose convinces English that Quartermain is the traitor. English entrusts the key to Ambrose, who then tells Pegasus that English is the traitor.
 timoxelyn barbebutenol that allows them to control a person for a brief time before they die of heart failure. Ambrose, the only surviving member of Vortex, plans to use the drug to kill the Premier in exchange for 500 million United States dollar|USD.

English visits Tucker and convinces him to help infiltrate Le Bastion. After passing through security at the fortress, English warns Pegasus (who now knows who the real traitor is) of the threat, and unknowingly drinks the spiked drink, rendering him vulnerable to Ambroses commands. Ambrose orders English to kill the Premier using a pistol disguised as a tube of lipstick. English tries to resist the effects of the drug, and engages in a fight with himself while Tucker attempts to interrupt communication between Ambrose and English. Tucker disrupts the frequency, replacing it with radio station, resulting in English beginning to dance. Ambrose reasserts his command, exposing himself in the process. English resists, shooting at Ambrose, who escapes.

The effects of the drug wear off and English seemingly dies of heart-failure. The paramedics try to save English with CPR, but it failed; he is revived by Kate, who kisses him on the lips, making his heart rate boost. English pursues Ambrose, parachuting from the building and hijacking a snowmobile. English reaches Ambrose, who is in a gondola lift. The two fight, with English falling out of the car. Ambrose tries to shoot English, who shoots a rocket disguised as an umbrella at the cable-car, killing Ambrose. English then sits back and says happily - "Tucker, you clever boy!"

Because of his actions, English is due to have his knighthood reinstated by The Queen, but as the ceremony takes place, the apparent Queen reveals herself to be the killer cleaner dressed up as the Queen and attempts to kill English with the knighthood sword. English fends off the cleaner, to which she runs off. English manages to catch and attack her, but when guards come in with the killer cleaner. English realizes that he has assaulted the real Queen.

A post-credits scene, influenced by the barber scene in The Great Dictator, sees English preparing a meal for Kate to the tune of "In the Hall of the Mountain King"
==Cast==
* Rowan Atkinson as MI7 Agent Johnny English, a clumsy yet kind-hearted spy
* Rosamund Pike as Kate Sumner, MI7s behavioural psychologist and Englishs love interest. Pike previously appeared in Die Another Day from the James Bond franchise that this film spoofs.
* Daniel Kaluuya as Agent Colin Tucker, Englishs youthful sidekick and the logical thinker and smarter of the two
* Gillian Anderson as MI7 Head Pamela Thornton, codenamed as Pegasus, the new boss at MI7
* Tim McInnerny as Patch Quartermain, MI7 agent and inventor
* Dominic West as Simon Ambrose, a MI7 agent and the main antagonist in the film. He is the third and final member of Vortex.
* Mark Ivanir as Artem Karlenko (alias Sergei Pudovkin), a Russian former double agent who was recruited by MI7 in Moscow, having been under the employ of the KGB. After Karlenko came over to England, he used the cover identity of Sergei Pudovkin, a man of immense wealth and a member of an exclusive golf club
* Richard Schiff as Titus Fisher, an ex-CIA agent who turned renegade and is the first member of Vortex
* Pik-Sen Lim as the Killer Cleaner, who frequently encounters English during his missions.
* Burn Gorman as Slater, a corrupt MI7 agent and Ambroses accomplice
* Joséphine de La Baume as Madeleine, Ambroses henchwoman
* Stephen Campbell Moore as British Prime Minister
* Lobo Chan as Xiang Ping, the Chinese Premier
* Togo Igawa as Ting Wang, a Tibetan guru and Englishs mentor, as well as an MI7 sleeper agent Ellen Thomas as Colin Tuckers mother
* Miles Jupp as an MI7 technician

==Car==
Johnny English drives a Rolls-Royce Phantom Coupé with an experimental 9.0 litre V16 engine. There are only a few of these engines in existence, produced during tests for the Phantom Coupé, and they were not used in production models. For the production of the film, Atkinson approached the company and requested that they install one into a car, making the vehicle seen in the film unique.   

==Production==
  Universal Pictures first announced that they were producing a sequel to Johnny English on 8 April 2010, seven years following the first film. 

Filming began on 11 September 2010 in Central London at Cannon Street, with further production scheduled for the week beginning 13 September 2010 at Brocket Hall, Hertfordshire and later in Hawley Woods in Hampshire, Macau and Hong Kong.   Filming took place on The Mall, London in Central London on 25 September 2010. Filming also took place in Kent along the A299 carriageway and Cliffs End, Ramsgate. 

The Johnny English Theme from the original film is quoted four times in the score.

Ben Miller, who played Bough in the previous movie, appeared but his scenes were cut from the final film.

===Box office===
Johnny English: Reborn opened to an estimated $3,833,300 in its first weekend in US and Canada. In the UK it grossed $7,727,025, $2,628,344 in Australia and $3,391,190 in Germany. After five weeks in release, it grossed $8,305,970 in the US and Canada and $151,772,616 elsewhere bringing to a total of $159,270,879. 

===Reception===
Much like its predecessor, the film received mixed  reviews from critics. Review aggregator Rotten Tomatoes reports that 39% of 87 critics have given the film a positive review, with a rating average of 4.8 out of 10. The websites consensus is "Arguably a marginal improvement on its mostly-forgotten predecessor, Johnny English Reborn nonetheless remains mired in broad, tired spy spoofing that wastes Rowan Atkinsons once considerable comedic talent".  Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film a score of 46 based on 20 reviews.   CinemaScore polls reported that the average grade moviegoers gave the film was a "B" on an A+ to F scale.   
 At the Movies, Margaret Pomeranz rated the film 3 stars and David Stratton rated the film 2 stars (the highest being 5 stars).   Indian film critic Nikhat Kazmi of the Times of India gave the film a positive review praising Atkinsons characteristic flair for comedy once again, giving it a 4 star rating out of 5. 

===Home media===
Johnny English Reborn was released on DVD and Blu-ray combo pack featuring the first film on 14 February 2012 in the UK, and on 28 February 2012 in North America. 

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 