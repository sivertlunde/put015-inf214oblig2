I, Justice (film)
{{Infobox film
| name           =  I, Justice
| image          = I, Justice.jpg
| alt            = 
| caption        = A promotional poster of the film. 
| director       = Zbyněk Brynych.
| producer       = Eliská Nejedla
| writers        = Miroslav Hanuš
| screenplay     = Milos Macourek
| narrator       = 
| starring       = Karel Höger, Fritz Diez
| music          = Jirí Sternwald
| cinematography = Josef Vanis
| editing        = Miroslav Hájek
| studio         = Barrandov Studios DEFA|DEFA-Babelsberg
| distributor    = 
| released       = 12 April 1968
| runtime        = 88 minutes
| country        = Czechoslovakia East Germany
| language       = Czech, German.
| budget         = 
| gross          = 
}} Czechoslovak psychological thriller, directed by Zbyněk Brynych.

==Plot== Allied raids in which Hitler is captured, faces trial and a death sentence, put under the guillotines blade and then is rescued by his supporters at the last moment - only to face it all again, over and over. The experience drives Hitler into an unberable mental agony; The doctor decides to put an end to his misery and kills him.

==Production==
The films script was based on the eponymous novel by Miroslav Hanuš, published at 1946.   
Hanuš book was criticized for what critics perceived to be a superficial treatment of the moral questions raised by the crimes of Nazism.  .  
 Transport From Paradise - which dealt with the life in the Theresienstadt concentration camp, and was adapted from Arnošt Lustigs autobiographical novel, Night and Hope - and the 1964 The Fifth Horseman is Fear, about a Jewish doctor in German-occupied Prague. 

==Reception==
The film was received poorly, and failed to secure any nominations or awards.  Czechoslovak film critic   wrote that "Doctor Heřman found a solution for himself, but not for the viewer, at least not the one who expected to see evil defeated on the moral level, and not just on the physical one."  The New York Times reviewer called the movie a "gripping moral and political exposé of frightening fantasy". 

==Select cast==
*Karel Höger as Doctor Heřman.
*Fritz Diez as Adolf Hitler.
*Angelica Domröse as Inga.
*Jirí Vršťala as Harting.
*Karel Charvát as Herbert.
*Jindřich Narenta as Man With Glasses

==References==
 

==External links==
*  on the IMDb.
*  on Allmovie.
*  on the Barrandov Televisions website.

 
 
 
 
 
 
 