The Right to Know (film)
 
{{Infobox film
| name           = Tasten in het duister
| image          = 
| image_size     = 
| caption        = 
| director       = Stephan Brenninkmeijer
| producer       = 
| writer         = Dick van den Heuvel
| narrator       = 
| starring       = 
| music          =   
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1996
| runtime        =120 minutes
| country        = Netherlands Dutch
| budget         = 
}} 1996 Netherlands|Dutch film directed by Stephan Brenninkmeijer.

==Plot==
Isnt it every childs greatest fear? That, one day, you find out that your father and mother are not your real parents at all? Press photographer Nora ten Have is a young, self-reliant woman, whos always believed shes had a happy youth. Until cracks appear in her happy memories. When she finds out that she was adopted when she was three years old, she is suddenly left without the very basis of her existence. This forces her to start searching: for her history, for what happened in the past.... but most of all, "who am I" and "where did I come from".

==Cast==
*Cynthia Abma	... 	Nora ten Have
*Hidde Maas	... 	Peter Olaf
*Michael van Buuren	... 	Hans
*Kees Brusse	... 	Karel ten Have
*Ellis van den Brink	... 	Jeanne ten Have
*Leslie de Gruyter	... 	Terlingen
*Margot van Doorn	... 	Joyce
*Mark Rietman	... 	Felix
*Margo Dames	... 	Marloes Olaf
*Jaak Van Assche	... 	Jules
*Gaston van Erven	... 	Doctor Gorter
*JanAd Adolfsen	... 	Arno Hofstede
*Hedy Wiegner	... 	Annabel
*Gerda Van Roshum	... 	Widow of the victim
*Sjaan Duinhoven	... 	Heleen Olaf

== External links ==
*  

 
 
 
 


 