Ee Lokam Evide Kure Manushyar
{{Infobox film
| name           = Ee Lokam Evide Kure Manushyar
| image          =
| caption        =
| director       = PG Vishwambharan
| producer       = Sajan John Paul (dialogues) John Paul Innocent Thilakan Shyam
| cinematography = K Ramachandrababu
| editing        = VP Krishnan
| studio         = Saj Productions
| distributor    = Saj Productions
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, Innocent and Thilakan in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
 
*Jayabharathi
*Mammootty as Ummer Innocent
*Thilakan as Krishnapilla 
*Nedumudi Venu Rohini
*Amjad Khan as Abbas
*Kajal Kiran
*Lalu Alex Meena
*Paravoor Bharathan
*Philomina
*Rahman
*TG Ravi as Keshavan
 

==Soundtrack== Shyam and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kanavin Manimaaran || Chorus, Cochin Ibrahim || P. Bhaskaran || 
|-
| 2 || Kannum Kannum || Unni Menon, Chorus, Lathika || P. Bhaskaran || 
|-
| 3 || Sankalpam || KG Markose || P. Bhaskaran || 
|-
| 4 || Thattimutti Kaithatti || KG Markose, Lathika || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 