The Longest Yard (2005 film)
{{Infobox film
| name            = JagWire
| name            = The Longest Yard
| image           = Longest yard ver2.jpg
| caption         = Theatrical release poster
| director        = Peter Segal
| producer        = Jack Giarraputo
| screenplay      = Sheldon Turner
| story           = Albert S. Ruddy
| based on        =  
| starring        =  
| music          = Teddy Castellucci
| cinematography = Dean Semler
| editing        = Jeff Gourson
| studio         =  
| distributor    = Paramount Pictures   Columbia Pictures  
| released       =  
| runtime        = 113 minutes
| country        = United States
| language       = English
| budget         = $82 million
| gross          = $190.3 million
}} sports comedy film of football against their guards.
 Dalip "The Great Khali" Singh Rana.
 Big Daddy in 1999).

==Plot== NFL player joyriding in his girlfriends Bentley, gets arrested, and is sentenced to three years in a Texas Federal Prison.
	 hot box to coerce Crewe into helping the prison guards football team, led by the hostile Captain Knauer (William Fichtner). Crewe informs Hazen that what Hazens team needs is a tune-up game to boost the guards confidence, and therefore sets out to form a decent team to play against the guards out of fellow inmates. With the help of newfound friend, Caretaker (Chris Rock), they start off with a poorly organized team, before being noticed by another prisoner, former college football star, Nate Scarborough (Burt Reynolds), who decides to help coach the team and help gather several intimidating inmates as a boost to the teams strength. 
 personal fouls in which he elbows, punches or grabs Crewe, Crewe continues without complaint, because the match is a question of pride and respect. Although Deacon beats Crewe, one of the brothers, a fast runner named Earl Megget (Nelly), impressed with Crewes decision to take the beating, joins the football team as its running back. When the guards learn of this, they confront Earl in an attempt to provoke an assault by him by saying the racial epithet "nigger", but Earl does not allow himself to be provoked. Having witnessed this, the other "brothers", including Deacon, decide to join the team too.

Hazen and the guards continue attempts to hinder Crewes team by flooding their field, but the team decides to practice in the mud anyway.

Inmate Unger (David Patrick Kelly) spies on the activities of the inmates and after being pressured by the guards, rigs Crewe’s radio with an explosive. Caretaker unknowingly enters the cell to give a photo to Crewe, but is killed when he tries to turn the dial on the radio.

On game day, the inmates, now calling themselves "Mean Machine", and using equipment provided by the late Caretaker, overcome a rough start, due to individual inmates attempts to retaliate against guards for the abuse theyve suffered. Crewe angrily tells the inmates that winning the game is more important and will damage the guards more than their personal grudges, and gets them to play as a team. The first half ends with the score tied. The angered Hazen informs Crewe in private that if he does not lose he will be charged for Caretakers murder. Crewe acquiesces to Hazens threat, asking that the guards refrain from using excessive force on the field after getting a comfortable lead, to which Hazen agrees to do so after they obtain a two touchdown lead. After Crewe fakes an injury in order to leave the field, his teammates voice their displeasure over his obvious deserting over the team.

After seeing that Hazen has broken his promise and two members of the Mean Machine are injured, Crewe asks Skitchy if the time spent in jail for punching the warden was worth it. Skitchy replies, "It was worth every goddamn second," and Crewe returns to the field. The team initially doubts Crewe’s resolve and allows him to be sacked twice. After losing his helmet and still getting the first down, Crewe, realizing that his inmates are still not protecting him due to his prior actions, calls a huddle, and admits to the point shaving that disgraced him, and to the injury he faked as a result of Hazens threat, and sabotage to the other inmates, and asks for their forgiveness.

The Mean Machine, united again as a team, scores two touchdowns to cut the guards lead to 35-28. Scarborough comes in for one play as replacement and scores a touchdown off a trick play involving a fumble called a Fumblerooski. Mean Machine decides to go for the two-point conversion and the win. As they get up to the line they seem to be confused, and Crewe and Scarborough start arguing in order to trick the guards. Moss gets the snap and passes it to Crewe, who scores the winning conversion, winning the game. Knauer, with a newfound respect for Crewe, tells him that he showed extraordinary nerve, and lets him know that he will vouch that Crewe had nothing to do with Caretakers death.

Hazen admonishes Knauer for losing a fixed game and notices that Crewe is heading towards the exit. Thinking Crewe is trying to escape, Hazen orders that Crewe be shot for attempting to escape. Knauer hesitates and at the last moment realizes (and scornfully tells Hazen) that Crewe is only picking up the game football. Crewe returns it to Hazen, telling him to "stick it in   trophy case." Moss and Joey Battle (Bill Goldberg) give Hazen a Gatorade shower, and when he tells them that this has earned them a week in the hot box, Battle defiantly yells "Who gives a shit?!"

==Cast==
{| class="toccolours" style="border-collapse: collapse;"
|-
|valign="top"|
Convicts
* Adam Sandler as Paul "Wrecking" Crewe
* Chris Rock as James "Caretaker" Farrell
* Burt Reynolds as Coach Nate Scarborough
* Nelly as Earl Megget
* Michael Irvin as Deacon Moss
* Bill Goldberg as Joey "Battle" Battaglio
* Terry Crews as "Cheeseburger" Eddy
* Bob Sapp as Switowski
* Nicholas Turturro as Brucie
* The Great Khali as Turley
* Lobo Sebastian as Torres
* Joey Diaz as Anthony "Big Tony" Cobianco
* Rob Schneider as Punky
* Lobo Sebastian as Torres
* David Patrick Kelly as Unger
* Eddie Bunker as Doc "Skitchy" Rivers
* Steve Reevis as "Baby Face" Bob Rainwater
* Tracy Morgan as Miss Tucker
|width="20"|&nbsp;
|valign="top"|
Prison staff
* James Cromwell as Warden Rudolph Hazen
* William Fichtner as Cpt. Brian Knauer
* Kevin Nash as Sgt. Engleheart Steve Austin as Officer Dunham
* Brian Bosworth as Officer Garner
* Brandon Molale as Officer Malloy
* Michael Papajohn as Officer Papajohn
* Conrad Goode as Officer Webster
* Bill Romanowski as Officer Lambert
* Cloris Leachman as Lynette Grey
|width="20"|&nbsp;
|valign="top"|
Others
* Allen Covert as Referee
* Jim Rome as Himself
* Chris Berman as Himself
* Lauren Sanchez as Herself
* Patrick Bristow as Walt
* Adam Schefter as Himself Peter King as Himself
* Courteney Cox as Lena (uncredited)
* Dan Patrick as Police Officer
|width="20"|&nbsp;
|valign="top"|
|}
 

Rap group D12 (except for Eminem) appeared in the film, credited as Basketball Convicts. Eminem is mentioned in the scene where Crewe asks the black basketball players to play on the convicts football team. When Crewe appears at the basketball court, D12 member Swifty McVay says, "Yo man, check out this fake Slim Shady".

==Production== New Mexico Murdock Stadium at the El Camino College in Torrance, California. The car chase scene was filmed whereabouts in Tall Beach, California. Other parts of the film were filmed in Los Angeles and New Mexico.  The golf course scene was filmed at Lost Canyons Golf Club in Simi Valley, California. 

==Reception==

===Box office===
The film did well at the box office. Its $47.6 million opening weekend was the largest of Sandlers career and only second to  . Despite the large number of remakes released at the theaters, its worth noting that The Longest Yard is the highest grossing comedy remake of the modern box office era (from 1980 on).  . Box Office Mojo. Retrieved December 1, 2014. 

===Critical response   ===
The overall critical response was mixed.  It received a 31% Freshness rating on   and Kontroll, until finally encouraging his readers to "drop any thought of seeing anything else instead" if they can see Crash (2004 film)|Crash. Ebert, Roger (May 26, 2005).  . rogerebert.com.   

==Awards== BET Comedy Award for Outstanding Supporting Actor in a Theatrical Film. 

Burt Reynolds earned a Golden Raspberry Award nomination for Worst Supporting Actor for his performance in the film. 

==Soundtrack==
 
The official soundtrack, which consisted entirely of hip-hop music, was released on May 24, 2005 by Universal Records. It peaked at 11 on the Billboard 200|Billboard 200 and 10 on the Top R&B/Hip-Hop Albums.

The film itself contains a mixture of hip hop and rock music, featuring music by Creedence Clearwater Revival, Norman Greenbaum and AC/DC among others.

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 