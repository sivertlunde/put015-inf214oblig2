A Matter of Faith
 
{{Infobox film
| name           = A Matter of Faith
| image          = amatteroffaithposter.JPG
| image_size     = 
| alt            = 
| caption        = Theatrical poster
| director       = Rich Christiano
| producer       = Five & Two Pictures 
| writer         = Rich Christiano Dave Christiano
| starring       = Harry Anderson Jordan Trovillion Jay Pickett Clarence Gilyard
| released       =   
| runtime        = 88 minutes 
| country        = United States
| language       = English
| gross          = $486,538  . Box Office Mojo. Retrieved April 28, 2015. 
}} Christian drama film directed by Rich Christiano and stars Harry Anderson, Jordan Trovillion, Jay Pickett, and Clarence Gilyard. The film was released into theaters on October 17, 2014 by Five & Two Pictures.  The film follows a Christian student (Jordan Trovillion) and her father (Jay Pickett) who are challenged by a Biology professor who teaches the Theory of Evolution.

==Plot==
 
A Christian girl, Rachel Whitaker (Jordan Trovillion) goes off to college for her freshman year and begins to be influenced by her popular Biology professor (Harry Anderson) who teaches that evolution is the answer to the origins of life. When Rachel’s father, Stephen Whitaker (Jay Pickett) senses something changing with his daughter, he begins to examine the situation and what he discovers catches him completely off guard. Now very concerned about Rachel drifting away from her Christian faith, he tries to do something about it.

==Cast==
 
*Harry Anderson as Profesor Kaman
*Jordan Trovillion as Rachel Whitaker
*Jay Pickett as Stephen Whitaker
*Clarence Gilyard as Professor Portland
*Chandler Macocha as Evan Carlson
*Justin Brandt as Jason
*Barrett Carnahan as Tyler Mathis
*Stephanie Shemanski as Ally
*Sarab Kamoo as Kimberly Whitaker
*Scott Alan Smith as Phil Jamison
 

==Reception== Young Earth creationist Christians and Christian organizations.  

The Young Earth creationist Christian apologetics organization, Answers in Genesis (AiG), endorsed the film.  Commenting on the film, Ken Ham, the president of AiG, stated that:  

==References==
 

==External Links==
* 
* 

 
 
 
 
 
 
 
 
 