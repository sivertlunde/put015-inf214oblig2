List of lesbian, gay, bisexual or transgender-related films of 1992
 
This is a list of lesbian, gay, bisexual or transgender-related films released in 1992. It contains theatrically released cinema films that deal with or feature important gay, lesbian or bisexual or transgender characters or issues and may have same-sex romance or relationships as an important plot device.

==1992==
{| class="wikitable sortable"
! width="16%" | Title
! width="4%" | Year
! width="15%" | Director
! width="15%" | Country
! width="13%" | Genre
! width="47%" | Notes
|- valign="top"
|-
|Basic Instinct|| 1992 ||   ||    || Drama, mystery, thriller||
|-
|Being at Home with Claude|| 1992 ||   ||   || Drama||
|-
|Claire of the Moon|| 1992 ||   ||  || Romantic, drama||
|-
| || 1992 ||   ||     || Romantic, drama, thriller||
|-
|Okoge (film)|Okoge|| 1992 ||   ||   || Drama||
|-
|Dönersen Islık Çal|| 1992 ||   ||   || Drama||
|- Walking After Midnight|| 1992 ||   ||   || Romantic, drama||
|-
|Flaming Ears|| 1992 ||  , A. Hans Scheirl, Dietmar Schipek ||   || Fantasy, science fiction||
|-
|For a Lost Soldier|| 1992 ||   ||   || Romantic, war, drama||
|-
| || 1992 ||  , Aerlyn Weissman ||   || Documentary||
|-
|Gayniggers from Outer Space|| 1992 ||   ||   || Science fiction, short||
|-
|I Am My Own Woman || 1992 ||   ||   || Documentary, drama ||
|-
| || 1992 ||   ||  || Comedy, drama||
|-
|Orlando (film)|Orlando|| 1992 ||   ||   || Romantic, drama||
|-
|Peters Friends|| 1992 ||   ||   || Comedy, drama||
|-
|RSVP (1992 film)|RSVP|| 1992 ||   ||   || Short ||
|-
|Savage Nights|| 1992 ||   ||    || Drama ||
|-
|Swoon (film)|Swoon|| 1992 ||   ||  || Crime, drama||
|}

 

 
 
 