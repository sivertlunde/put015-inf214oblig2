Fighting Blood
{{Infobox film
| name           = Fighting Blood
| image          = 
| caption        = 
| director       = D. W. Griffith
| producer       = D. W. Griffith
| writer         =  George Nichols
| music          = 
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = 
| released       =  
| runtime        = 18 minutes
| country        = United States  Silent English intertitles
| budget         = 
}}
 short silent silent Western Western film George Nichols and featuring Lionel Barrymore and Blanche Sweet. A print of the film survives in the film archive of George Eastman House.   

==Cast== George Nichols as The Old Soldier
* Kate Bruce as The Old Soldiers Wife
* Robert Harron as The Old Soldiers Son
* Lionel Barrymore
* Mae Marsh
* Blanche Sweet
* Kate Toncray as The Sons Girlfriends Mother

==See also==
* List of American films of 1911
* D. W. Griffith filmography
* Blanche Sweet filmography
* Lionel Barrymore filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 