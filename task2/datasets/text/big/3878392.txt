Bon Voyage (1944 film)
{{Infobox film
| name           = Bon Voyage
| image          =
| director       = Alfred Hitchcock
| writer         = Angus MacPhail J.O.C. Orton
| story          = Arthur Calder-Marshall
| starring       = John Blythe
| distributor    = Milestone Films
| released       = 1944
| runtime        = 26 minutes
| country        = United Kingdom French
}}
 Ministry of Cidade de Deus (2002).

==Plot==
The story is told in flashback, once from the perspective of the protagonist, and then a second time with a deeper understanding that is provided by the intelligence officer in London.

A Scotsman, a downed Royal Air Force air gunner who was previously a prisoner of war explains how he travelled with great difficulty through German-occupied France. He was accompanied most of the way by a companion who was another escaped prisoner of war, and they were both aided by various courageous Resistance workers. His companion gave him a letter to deliver once he reached London, supposedly a very personal and private letter. 

However, when we see the Intelligence officers explanation of the same events, it becomes clear that the gunners companion, who was supposedly helping him along, was in fact a Gestapo spy, who murdered several of the Resistance fighters and reported the rest to the authorities, and that the "personal letter" the gunner was going to deliver in London contains secret information that would have helped the enemy.

==Home media==
Milestone Films has released Bon Voyage, paired with other 1944 French language Hitchock short film Aventure malgache, on VHS and DVD.  

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 
 