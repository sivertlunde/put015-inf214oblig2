Rani Saheba (1930 film)
{{Infobox film
| name           = Rani Saheba
| image          = 
| image_size     = 
| caption        = 
| director       = V. Shantaram
| producer       = Prabhat Film Company
| writer         = Baburao Pendharkar
| narrator       = 
| starring       = V. Shantaram Keshavrao Dhaiber Baburao Pendharkar Anant Apte 
| music          = 
| cinematography = S. Fattelal Vishnupant Govind Damle 
| editing        = 
| distributor    =
| studio         = Prabhat Film Company
| released       = 1930
| runtime        = 
| country        = India
| language       = Silent film
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1930 Indian Indian silent film.    It is cited as the first childrens film made in India.    The film was co-directed by V. Shantaram and Keshavrao Dhaiber.    The cinematographers were S. Fattelal and Vishnupant Govind Damle and the cast included Keshavrao Dhaiber, Baburao Pendharkar, V. Shantaram and Anant Apte.   
 Gopal Krishna Chandrasena (1931), and Zulum (1931).    Out of these Rani Saheba and Khooni Khanjar are cited as some of the "lighter" films produced by Prabhat Films.    Master Anant Apte was "nicknamed" Bazarbattu following the success of his role in the film.   

==Cast==
* Keshavrao Dhaiber
* Baburao Pendharkar
* V. Shantaram
* Anant Apte


==References==
 

==External links==
* 

 

 
 
 
 
 
 

 