Shuttlecock (film)
 
{{Infobox film
| name           = Shuttlecock
| image          = Shuttlecock 1991.jpg
| alt            =
| caption        = film poster
| film name      = 
| director       = Andrew Piddington
| producer       =  
| writer         =  
| screenplay     = 
| story          = 
| based on       =  
| starring       =  
| narrator       = 
| music          =  
| cinematography =  
| editing        =  
| production companies =  
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        =   
| language       = English
| budget         = 
| gross          =  
}}
 British thriller film directed by Andrew Piddington and starring Alan Bates, Lambert Wilson and Kenneth Haigh.  It is based on the 1981 novel Shuttlecock (novel)|Shuttlecock by Graham Swift.  

==Plot==
Major James Prentis (Alan Bates) is a British spy of World War II and war hero  who goes under the code name of "Shuttlecock". Alienated from his family and children, he ends up in a mental institution in Lisbon, Portugal, where he eventually decides to publish his memoirs 20 years after the war. His son, John (Lambert Wilson), becomes increasingly alarmed with the enigmatic Dr. Quinn (Kenneth Haigh), the director of the institution, and concludes after reading his fathers memoirs that Quinn is responsible for his fathers mental decline.

==Cast==
 
* Alan Bates as Major James Prentis VC
* Lambert Wilson as John Prentis
* Kenneth Haigh as Dr. Quinn
* Jill Meager as Marian Prentis
* John Cassady as Eddie Arthur Cox as Fizz / Fox
* David Ryall as Pound
* Gregory Chisholm as Martin Prentis
* Beatrice Buchholz as Beatrice Carnot
* Jacqui Sedlar as Jenny
* Dominic Chesterton as Peter Prentis
* João Perry as Eric
* Luísa Barbosa as Ana
 

==Production==
The production for the film was marred with problems, which resulted in a long delay in release.  Producer Graham Leader acquired the rights to the film in the late 1980s after a meeting with Graham Swift, the writer of the novel, and paid $7,500 for the rights over two years.  Tim Rose Price,  the writer of several BBC dramas, agreed to write the script, and worked on a script with Leader throughout 1989. Jon Amiel agreed to direct. Producers Leader and Charles Ardan approached Channel 4 and British Screen to finance the picture; Channel 4    agreed to shout $900,000, payable when the film was completed, and British Screen offered just over $500,000, if Leader could attract a co-producer on the European continent who could finance $1.5 million.   Leader had searched for financing in the United States, but was turned down by the likes of Orion Classics and Avenue Pictures, who wanted the characters and settings Americanized. French independent French company Les Productions Belles Rives eventually agreed to partly finance the film.  After one of Frances largest film labs conceded to provide materials and film-processing services, a shortfall of $225,000 still remained.  

The film was shot on location in Lisbon, Portugal over six weeks at the end of 1990 and beginning of 1991, with a French, English and Portuguese crew. Producer Leader described the filming for Shuttlecock as "highly unorganized chaos", and the actors were surrounded with confusion on set.  Financing was halted during the filming when a French bank which had loaned the money for production "decided to take its fees out of the loan rather than out of the profits from the film". To get themselves out of difficulty, producers Leader, Piddington and Rose put half of their fees for it back into the film.  A British investor turned up during the filming, promising to provide financing, but a New Zealand heiress froze all of his bank accounts before anything was signed.  Filming was wrapped up in January 1991 in London, before post-production began at Pinewood Studios.  The total budget eventually amounted to around $3 million. 

The film was screened at the San Sebastian Film Festival, shortly after a rushed job with adding Spanish subtitles, where it failed to win the $250,000 Golden Shell Award and any others, making it ineligible for Cannes. Further editing to the film over the next six months was done, but the producers failed to attract a distributor.  There was optimism for a time that it would be released at the Hamptons International Film Festival, but it amounted to nothing. 
Financing for the film was eventually derived mainly from Channel 4, Les Productions Belles Rives, and KM Films,  and a plethora of other associate producers such as Sea Lion Films, Gigantic Pictures, Minerva Productions, and distributors such as Alliance Communications, Le Monde Entertainment, and Movie Screen Entertainment.   

==Reception==
The film received a mixed response from critics.   as part of its series "Film on Four", the Sunday Telegraph named the film its "TV Pick of the Week," referring to it as "a film with things to say and a muscular yet sensitive way of saying them." 

The film has been remade under the title "Sins of a Father", reported the New York Times Feb. 1, 2015.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 