It's Great to Be Alive (film)
{{Infobox film
| name = Its Great to Be Alive
| image =
| image_size =
| caption =
| director = Alfred L. Werker
| producer =
| writer = Arthur Kober Paul Perez
| starring =    Gloria Stuart Edna May Oliver Herbert Mundin Joan Marsh
| music =
| cinematography = Robert Planck
| editing = Barney Wolf
| distributor = Fox Film Corporation
| released = July 8, 1933
| runtime = 69 minutes
| country = United States
| language =
}}
 The Last Man on Earth (1924), and later influenced the novel Mr. Adam (1946) by Pat Frank.

==Synopsis==
A young aviator, Carlos Martin (played by Raul Roulien), is dumped by his girlfriend (Gloria Stuart), and heads on a solo flight across the Pacific Ocean. He has engine trouble and makes an emergency landing on an uninhabited island out in the Pacific. Shortly afterward, a global epidemic of a new disease called masculitis kills every fertile male human on the planet. When efforts to cure the disease fail, the human race is doomed. Humanitys institutions are all run by women, including the Chicago underworld. Carlos escapes the island, and once he returns home and hears the news, it now depends on him to continue the human race.

One scene in this film depicts look-a-likes of the two top scientists of the era, Albert Einstein and Auguste Piccard, trying to find a cure for masculitis. Another scene portrays a burlesque show dubbed "Girls of all Nations". Other cast members include Edna May Oliver, Joan Marsh, Edward Van Sloan, and Peaches Jackson.

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 