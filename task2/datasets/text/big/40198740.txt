Mary Queen of Scots (2013 film)
 
{{Infobox film
| name           = Mary Queen of Scots
| image          = Mary Queen of Scots (2013 film).jpg
| caption        = Film poster
| director       = Thomas Imbach
| producer       = Andrea Staka Thomas Imbach
| writer         = Stefan Zweig Thomas Imbach
| starring       = Camille Rutherford
| music          = Sofia Gubaidulina
| cinematography = Rainer Klausmann
| editing        = Tom La Belle
| distributor    = Pathé
| released       =  
| runtime        = 120 minutes
| country        = Switzerland
| language       = English French
| budget         = 
}}

Mary Queen of Scots is a 2013 Swiss period drama directed by Thomas Imbach. It is his first film in English and French language starring the bilingual French actress Camille Rutherford. The film portrays the inner life of Mary, Queen of Scots|Mary, the Queen of Kingdom of Scotland|Scotland. The film is based on Austrian novelist Stefan Zweigs 1935 biography, Mary Stuart, a long-term bestseller in Germany and France but out of print in the UK and the US for decades until 2010. 
The film was first screened at the 2013 International Film Festival Locarno and was later shown at the 2013 Toronto International Film Festival.   

==Plot== Scotland to France as a child for her protection.

Mary is raised in French court, where she had for companions her Scottish ladies Mary Beaton, Mary Seton, Mary Fleming and Mary Livingston, as well as the Italian musician David Rizzio. Upon reaching adulthood Mary is married to the Dauphin, Francis II of France|Francis, who becomes King of France when his father dies. News arrives that Queen Mary I of England has died and her sister, Elizabeth, has become Queen despite her official status as a bastard. Mary is aware of her own legitimate claim to the English throne; although she writes to Elizabeth promising not to challenge her, she insists on using the English coat of arms and the title "Queen of France, Scotland and England".
 Lord Moray, Darnley without the Lords consent, and supporting the controversial Scottish border Lord James Hepburn, 4th Earl of Bothwell|Bothwell. Marys love for Darnley fades when she learns of his extremism in pursuing the English throne; Mary has strong affection for Elizabeth, and wants to be her heir instead of usurping her.
 gunpowder explosion, England in the hopes of getting protection from Elizabeth. Instead, Elizabeth has Mary incarcerated, and decades later Mary is executed by order of the cousin she has never met.

==Cast== Mary
* Bothwell
* Darnley
* Moray
* Rizzio
* Knox
* Clive Russell as Douglas Ruthven
* Marie de Guise
* Bruno Todeschini as De Croc
* Roxane Duran as Mary Seton
* Gaia Weiss as Mary Fleming

==Reception==
"Mary Queen of Scots rises well above its roots as a historical drama, for it is an extremely well-directed and imaginatively conceived piece of filmmaking. (…) What consistently impresses is the absolute ,rightness’ of everything in this film. It is amongst the most thrilling treatments of a historical subject I have ever seen". 

"Mary Queen of Scots features an appealing reworking of familiar contents: While in many ways a traditional period drama, its time-shifting structure and dreamlike narration manages to critique the very strictures of the genre". 

"Swiss director Thomas Imbach’s take on the much-told story of Mary Queen of Scots is an intimate version – rather than an all-singing, all-dancing epic drama – the dwells on personalities and discussion rather than armies, ships and battles. Cleverly Imbach avoids the usual clichéd aspects of the story. The period setting, evocative locations and good use of costumes are impressive". 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 