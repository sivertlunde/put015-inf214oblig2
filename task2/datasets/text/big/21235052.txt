Emergency Ward (film)
{{Infobox Film
| name           = Emergency Ward
| image          =
| image size     =
| caption        =
| director       = Tulio Demicheli
| producer       = Enrique Faustin
| writer         = Roberto Gil
| narrator       =
| starring       = Roberto Escalada
| music          =
| cinematography = Fulvio Testi
| editing        = Nello Melli
| distributor    =
| released       = 28 February 1952
| runtime        =
| country        = Argentina
| language       = Spanish
| budget         =
| preceded by    =
| followed by    =
}}

Emergency Ward ( ) is a 1952 Argentine film directed by Tulio Demicheli. It was entered into the 1953 Cannes Film Festival.   

==Cast==
* Aída Alberti
* Tito Alonso
* Arturo Arcari
* Margarita Corona
* Renée Dumas
* Roberto Escalada
* Mario Fortuna
* Analía Gadé
* Elisa Galvé (as Elisa Christian Galvé)
* Santiago Gómez Cou
* Diana Ingro
* Diana Maggi
* Lalo Malcolm
* Juan José Miguez
* Nelly Panizza
* Nathán Pinzón
* Perla Santalla
* Carlos Thompson

==References==
 

==External links==
* 

 
 
 
 
 
 
 