China Girl (1987 film)
{{Infobox film
| name = China Girl
| image =  China girl poster.jpg
| image_size =
| caption = Theatrical release poster
| director = Abel Ferrara
| producer = Michael Nozik
| writer = Nicholas St. John
| narrator =
| starring = James Russo Richard Panebianco Sari Chang David Caruso Russell Wong
| music = Joe Delia
| cinematography = Bojan Bazelli
| editing = Anthony Redman
| distributor = Vestron Pictures
| released =  
| runtime = 89 min
| country = United States English
| budget =
| gross = $1,262,091 http://boxofficemojo.com/movies/?id=chinagirl.htm 
}}

China Girl is a 1987 film directed by independent filmmaker Abel Ferrara, and written by his longtime partner Nicholas St. John.

==Story== West Side Story."

==Release==
The film was released theatrically on September 25, 1987 in 193 theaters and grossed $531,362 its opening weekend. the film grossed a domestic total of $1,262,091 and its widest release was to 193 theaters. After its theatrical run, the film was released on videocassette by Vestron Video. The film is available on region 2 DVD but has never been released on region 1 and as of January 17, 2010, Lions Gate has yet to announce any plans for a DVD release.

==Cast==
*Richard Panebianco as Tony
*Sari Chang as Tye
*James Russo as Alby
*Russell Wong as Yung Gan
*David Caruso as Mercury
*Joey Chin as Tsu Shin
*Judith Malina as Mrs. Monte
*James Hong as Gung Tu
*Robert Miano as Enrico Perito
*Paul Hipp as Nino
*Doreen Chan as Gau Shing
*Randy Sabusawa as Ma Fan
*Keenan Leung as Ying Tz
*Lum Chang Pan as Da Shan
*Sammy Lee as Mohawk

==Critical reception==
The staff at Variety (magazine)|Variety magazine said of the film, "China Girl is a masterfully directed, uncompromising drama and romance centering on gang rumbles (imaginary) between the neighboring Chinatown and Little Italy communities in New York City." and they especially praised the performances of Russell Wong and Joey Chin saying "Russell Wong (as handsome as a shirt ad model) and sidekick Joey Chin dominate their scenes as the young Chinese gang leaders. 
 Time Out magazine wrote that the film is a, "superior exploitation picture is a tough, stylish but often painfully misjudged reworking of Romeo and Juliet, with rival teenage gangs battling it out, sparked by the inter racial love affair between an Italian (Panebianco) and a Chinese girl (Chang), Ferrara makes excellent use of the Chinatown and Little Italy locations, and delivers the choreographed violence with his usual muscular panache." and that "The major strength of the script is its accommodation of three generations: the elders and their aspiring sons are seen to conspire against the warring youngsters, putting money before family."   

Johnathan Rosenbaum of the Chicago Reader praised the films photography and action scenes calling them "Bojan Bazellis location photography is luminous and exciting, and the battle lines charted in Nicholas St. Johns script are fairly complex."  

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 