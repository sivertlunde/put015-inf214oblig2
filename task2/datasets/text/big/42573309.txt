The Hobbit: The Battle of the Five Armies
 
{{Infobox film
| name           = The Hobbit: The Battle of the Five Armies
| image          = The Hobbit - The Battle of the Five Armies.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Peter Jackson
| producer       = {{Plainlist|
* Carolynne Cunningham
* Zane Weiner
* Fran Walsh
* Peter Jackson}}
| screenplay     = {{Plainlist|
* Fran Walsh
* Philippa Boyens
* Peter Jackson
* Guillermo del Toro}}
| based on       =  
| starring       = {{Plainlist|
* Martin Freeman
* Ian McKellen Richard Armitage
* Evangeline Lilly
* Lee Pace Luke Evans
* Benedict Cumberbatch
* Ken Stott
* James Nesbitt
* Cate Blanchett
* Ian Holm
* Christopher Lee
* Hugo Weaving
* Orlando Bloom}}
| music          = Howard Shore
| cinematography  = Andrew Lesnie
| editing         = Jabez Olssen
| production companies = {{Plainlist|
* New Line Cinema
* Metro-Goldwyn-Mayer
* WingNut Films}}
| distributor    = Warner Bros.
| released       =  
| country        = {{Plainlist|
* New Zealand
* United States United Kingdom  }}
| language       = English
| runtime        = 144 minutes       174 minutes  
| budget         = $250 million 
| gross          = $955.1 million   
}}
The Hobbit: The Battle of the Five Armies   is a 2014   (2012) and   (2013), and together they act as a prequel to Jacksons The Lord of the Rings (film series)|The Lord of the Rings film trilogy.

The film also marked the sixth and final middle-earth in film|Middle-earth adaptation of Tolkiens work. Produced by New Line Cinema, Metro-Goldwyn-Mayer and WingNut Films, and distributed by Warner Bros. Pictures. The Battle of the Five Armies was released on 11 December 2014 in New Zealand, 12 December 2014 in the United Kingdom and on 17 December 2014 in the United States. 
 Richard Armitage, Luke Evans, Benedict Cumberbatch, Ken Stott and James Nesbitt. It also features Cate Blanchett, Ian Holm, Christopher Lee, Hugo Weaving and Orlando Bloom.
 second highest-grossing 26th highest-grossing Best Sound Editing.

==Plot==
 
 . Bilbo learns from Balin that it would be best if the Arkenstone remains hidden from Thorin, who orders the entrance of the Lonely Mountain to be sealed off.
 Light of Eärendil and banishes Sauron and the Ringwraiths to the East. Azog, marching on Erebor with his vast Orc army, sends Bolg to Gundabad to summon their second army. Legolas and Tauriel witness the march of Bolgs army, bolstered by Goblins and giant bats.

While Bard and the Laketown survivors shelter in Dale, Thranduil arrives with an elf army, supplies and aid, and forms an alliance with Bard, wishing to claim an elven necklace of white gems from the Mountain. Bard attempts to negotiate and reason with Thorin to avoid war, but the dwarf refuses to cooperate. After Gandalf arrives at Dale to warn Bard and Thranduil of the Orc army who will take Erebor as part of a campaign to revive the kingdom of Angmar, Bilbo sneaks out of Erebor to hand the Arkenstone over to Thranduil and Bard. When Bard and Thranduils armies gather at the gates of Erebor, offering to trade the Arkenstone for Thranduils gems and Laketowns share of the gold, Thorin is shocked to learn of the Arkenstone in the Elves hands and he nearly kills Bilbo in a furious rage. After Gandalf forces Thorin to release Bilbo, the arrival of Thorins cousin Dáin II Ironfoot|Dáin with his Dwarf army worsens matters. A battle of Dwarves against Elves and Men is imminent, when Wereworms emerge from the ground releasing Azogs army from their tunnels. With the Orcs outnumbering Dains army, Thranduil and Bards forces, along with Gandalf and Bilbo, join the battle as some of the Orcs attack Dale. 
 Ravenhill with Great Eagles Radagast and Beorn, and the Orc armies are finally destroyed.

Bilbo regains consciousness and finds that Azog has been killed by Thorin, who makes peace with him before succumbing to his own injuries. On Thranduils suggestion, Legolas leaves to meet with a young Dunedain ranger going by the name Aragorn|Strider. As Tauriel mourns Kili, Thranduil tells her that her love for Kili was real. Grieved by the deaths of Thorin, Fili, and Kili, the people of Laketown, the elves, and the dwarves bury them inside the tombs of Erebor. As a result, Thorins cousin Dain is crowned King Under the Mountain, the citizens of Laketown are given the riches promised to them by Thorin, and Dain restores to the elves the white elven gems that King Thror had stolen from them years ago.
 auctioned off presumed dead. He aborts the sale and tells the other hobbits to leave but finds his home has been almost totally pillaged.

Sixty years later, Bilbo receives a visit from Gandalf and runs out to greet him, thus setting in motion the events of  .

==Cast==
 ]]
 
{{columns-list|2| Young Bilbo Baggins Old Bilbo Baggins Gandalf the Grey Richard Armitage Thorin II Oakenshield  The Necromancer    Legolas Greenleaf
*Evangeline Lilly as Tauriel Luke Evans Bard the Bowman
*Lee Pace as Thranduil  Dwalin
*Ken Balin
*Aidan Turner as Kíli 
*Dean OGorman as Fíli Dori
*Jed Nori
*Adam Adam Brown Ori
*John Callen as Óin, son of Gróin|Óin
*Peter Hambleton as Glóin, son of Gróin|Glóin
*William Kircher as Bifur
*James Nesbitt as Bofur  Stephen Hunter Bombur
*Cate Blanchett as Galadriel 
*Hugo Weaving as Elrond Saruman the White
*Sylvester McCoy as Radagast the Brown Azog the Defiler Bolg 
*Billy Connolly as Dáin II Ironfoot|Dáin
*Mikael Persbrandt as Beorn Laketown
*Ryan Alfrid
*Mark Braga
*John John Bell Bain
*Peggy Tilda and Sigrid Feren
}}

==Production==
 
The Hobbit was originally envisioned as a two-part film, but Jackson confirmed plans for a third film on 30 July 2012, turning his adaptation of The Hobbit into a trilogy.       According to Jackson, the third film would contain the Battle of the Five Armies and make extensive use of the appendices that Tolkien wrote to expand the story of Middle-Earth (published in the back of The Return of the King). Jackson also stated that while the third film will largely make use of footage originally shot for the first and second films, it would require additional filming as well.  The third film was titled There and Back Again in August 2012.    In April 2014, Jackson changed the title of the film to The Battle of the Five Armies as he thought the new title better suited the situation of the film.    He stated on his Facebook page, "There and Back Again felt like the right name for the second of a two film telling of the quest to reclaim Erebor, when Bilbos arrival there, and departure, were both contained within the second film. But with three movies, it suddenly felt misplaced—after all, Bilbo has already arrived there in the Desolation of Smaug."    Shaun Gunner, the chairman of The Tolkien Society, supported the decision: "The Battle of the Five Armies much better captures the focus of the film but also more accurately channels the essence of the story." 

===Score===
  Billy Boyd, The Lord of the Rings, wrote and recorded the song "The Last Goodbye" to be played over the end credits of the film. 

Callum Hofler of Entertainment Junkie gave the score an overtly positive review, stating, "Shore has had to pull out all the stops to produce something suitably satisfying, to reward fans of the series for their patience, persistence and support. After running through the album numerous times, satisfying is not the term I would assign this; emotionally-poignant, colossal and monumental are all far more accurate." Hofler concluded with, "If there was any way this franchise needed to conclude, than this is the picturesque and most desirable variation possible." He awarded the special edition of the score a 9.3 out of a possible 10.  Erin Willard of SciFi Mafia was generally positive, except that she noted, "There came a point near the end of the first of the two discs where I finally just had to pull out the earbuds; the constant and increasing dissonance started to actually hurt my ears." She awarded the score a 4 out of 5 stars. 

==Release==
===Marketing===
A teaser trailer for the film was released on 28 July 2014 attached to  .  

To promote the films release, Wellington-based   at the end of December as Vortex Dungeon units. The campaign only runs until February 2015.   

Smaug made a guest appearance, animated by WETA and voiced again by Cumberbatch, on the satire show The Colbert Report on December 12, 2014 to promote the film. 

===Theatrical release===
The world premiere of The Hobbit: The Battle of the Five Armies was held in London at Leicester Square on 1 December 2014.   The film opened in theatres on 11 December 2014 in New Zealand, on 12 December in the United Kingdom and on 17 December in the United States. Warner Bros released the film on 18 December 2014 in Greece and 26 December in Australia.       The film was released in China on January 23, 2015.   

===Home media===
The Hobbit: The Battle of the Five Armies was released on March 6, 2015 on Digital Release from digital retailers. The DVD and Blu-ray were released on March 24, 2015 in the United States.      It topped the home video sales chart in its opening week. 

 
 

==Reception==
 
===Box office=== 26th highest-grossing film of all time.   Its grosses exceeded its estimated $250 million production cost 12 days after its release.  

The film failed to earn $1 billion at the box office despite various pundits projecting it to reach that milestone. The Hollywood Reporter said that the possibility of The Battle of the Five Armies grossing $1 billion worldwide was extremely low due to "plunging exchange rates around the globe" witnessed that year and that Warner Bros. and MGM ultimately will take in nearly $90 million less than expected due to rising dollar and plunging foreign currencies.  However, despite this failure, Forbes has declared the trilogy "an unmitigated financial grand-slam for all parties." 

====United States and Canada====
In the U.S. and Canada, it is the lowest grossing of the three films in The Hobbit franchise,  and also the lowest grossing of the six middle-earth adaptations,  but the sixth highest-grossing film of 2014.  It opened on Tuesday, December 16, 2014 at 7 p.m across 3,100 theaters and widened to 3,875 theaters the following day.  It earned $11.2 million from Tuesday late-night shows, which was the second highest of 2014, matching the record previously set by   ($17 million).       It then topped the box office on its opening day (Wednesday, December 17, 2014), earning $24.5 million (including previews),   }} which was the third highest middle-earth adaptation Wednesday opening behind the Wednesday openings of   ($34.5 million) and   ($26.2 million).    

In total, the film earned $54,724,334 in its traditional three-day opening and $89,131,544 over its five-day course making it the second biggest five-day opening in the Hobbit trilogy, beating the $86.1 million five-day opening gross of The Desolation of Smaug, but still behind An Unexpected Journeys $100.2 million five-day opening. However, on a three-day basis the film underperformed expectations and fell short of its predecessors.   The film set a December  ).  3D accounted for 49% of the total gross while IMAX generated 15% or $13.4 million over five days, and $7.4 million over three days, and premium large format screens comprised 8% of the total opening weekend gross with $7.2 million from 396 theaters.       The film passed the $100 million mark on December 23, 2014 &ndash; on its seventh day.  It became the third film of 2014 to earn $100 million in just under a week following  /Marvel Studios|Marvels Guardians of the Galaxy ($134.4 million in its first week).  It was in first place at the North American box office for three consecutive weekends despite facing competition from numerous new releases each weekend,     but was finally overtaken by Taken 3 in its fourth weekend.    

====Other territories====
The film began its international roll-out a week prior to its wide North American release.  It opened Wednesday, December 10, 2014 in 11 European markets, earning $11.3 million and December 11, 2014 in 17 additional markets, earning $13.7 million, for a two day total of $26.6 million and topped the charts in each of the territories.      Through Sunday, December 14, 2015, it had an openening weekend total of $122.2 million from 37 countries in 15,395 screens       topping the box office and outperforming the previous two installments on a local currency and admissions basis.  71% of the total gross ($86.7 million) came from 3D showings.  However, the overseas opening weekend was still lower than the openings of An Unexpected Journey ($138 million)    and The Desolation of Smaug ($135.4 million)  &ndash; both on a dollar basis. It set a December IMAX opening record with $6.4 million across 160 IMAX screens, previously held by An Unexpected Journey with $5.03 million.  The film opened to an additional 59 countries in its second weekend and earned $109 million from 19,315 screens still holding the top spot and fell gradually by 13% as a result of facing minor competitions.     It passed the $200 million mark in its second weekend ($269 million in 12 days).    In its third weekend the film added a further $89 million abroad, remaining at number 1 and passed the $400 million mark.    It passed the $500 million mark in its fourth weekend    and $600 million in its seventh weekend.  It was in first place at the box office outside North America for four consecutive weekends  and five in total. 

In Australia, the film was released on December 26, 2014 and set an opening day record with $5.59 million, which is the biggest of 2014, the second biggest  ).      IMAX generated $6.8 million of the total gross which is the second-highest IMAX three day gross in China behind Transformers: Age of Extinction  $10 million gross.  In just 10 days of release, the film earned over $92.92 million in China.   

The film achieved numerous records in international markets during its opening weekend. It set an all-time Warner Bros. opening record in Russia ($13.75 million),  in Argentina ($2.1 million),  and in Sweden and Finland.  It also set a 2014 opening record in Germany ($20.5 million),  France ($15.05 million)  and in Spain ($6.5 million).  It also had the best Middle-earth saga opening in the UK ($15.2 million)  and in Mexico ($6.3 million).  In Brazil, the film scored the second biggest Warner Bros. opening of all time with $6.8 million (behind Harry Potter and the Deathly Hallows - Part 2).  Other high openings were recorded in Korea ($10.3 million), Poland ($5.3 million), Italy ($6.05 million), Malaysia ($2.85 million) and Taiwan ($2.75 million).   In total earnings, its largest markets are China ($121.7 million), UK, Ireland and Malta ($61.3 million) and Australia ($27 million). 

===Critical response===
 , reviews for the film were mostly positive, with critics "praising director Peter Jacksons effort at transforming J.R.R Tolkiens fantasy novel into an epic adventure film trilogy."  According to CBS News, critics said the film "will satisfy" fans but "otherwise, it may be worth waiting until its available to rent."  The Los Angeles Times said the critical consensus was that the film is "a flawed but fitting finale to the Hobbit trilogy."      The film also holds a Metacritic score of 59 out of 100, based on 45 critics, indicating "mixed or average reviews".  In CinemaScore polls conducted during the opening weekend, cinema audiences gave the film an average grade of "A-" on an A+ to F scale. 
 IGN Movies said, "Theres a little too much padding in the final Hobbit flick, and the best sequence is without doubt the films first. But the central battle is indeed spectacular, and as The Age of Orc approaches, it rounds out this particular story in stirring and emotional fashion."  Russell Baillie of The New Zealand Herald said The Hobbit: The Battle of the Five Armies is "something less than the supposed defining chapter of Jacksons time in Middle-earth as its been billed. But action-wise, it certainly goes out with a very pleasing bang." 

Conversely, Inkoo Kang of   described the film as "a paragraph on steroids" that was "neither very terrible nor remotely unexpected. Its a series of stomping footnotes in search of a climax."  The BBCs Nicholas Barber wrote that with the Hobbit series Jackson had succeeded in bridging the gap between The Hobbit and The Lord of the Rings, and that The Battle of the Five Armies was a "colossal technical achievement", but he also criticised that the film as such was not compelling because of its repetitive battle scenes and a lack of plot.  Nicolas Rapold of The New York Times said, "Bilbo may fully learn a sense of friendship and duty, and have quite a story to tell, but somewhere along the way, Mr. Jackson loses much of the magic." 

==Accolades==
 
{| class="wikitable sortable" style="width: 99%;"
|-
! Year
! Award
! Category
! Recipient
! Result
! scope="col" class="unsortable"| Ref.
|- 2014
| Heartland Film Festival
| Truly Moving Picture Award
| The Hobbit: The Battle of the Five Armies
|  
| style="text-align:center;" | 
|-
| Phoenix Film Critics Society Awards
| Best Visual Effects Matt Aitken, Eric Saindon, Scott Chambers
|  
| style="text-align:center;" | 
|- 2015
| Academy Awards Best Sound Editing
| Brent Burge and Jason Canovas
|  
| style="text-align:center;" | 
|- Screen Actors Guild Awards Outstanding Performance by a Stunt Ensemble in a Motion Picture
| The Hobbit: The Battle of the Five Armies
|  
| style="text-align:center;" |   
|- 20th Critics Choice Awards|Critics Choice Movie Awards Best Hair & Makeup
| The Hobbit: The Battle of the Five Armies
| 
| style="text-align:center;"  rowspan="2" | 
|- Best Visual Effects
| The Hobbit: The Battle of the Five Armies
| 
|- British Academy Film Awards Best Special Visual Effects
| Joe Letteri, Eric Saindon, David Clayton, R. Christopher White
|  
| style="text-align:center;" |   
|-
| Denver Film Critics Society Best Original Song Billy Boyd, Philippa Boyens, Fran Walsh
|  
| style="text-align:center;" | 
|- 20th Empire Empire Awards Best Film
| The Hobbit: The Battle of the Five Armies
|  
| style="text-align:center;" rowspan="4" | 
|- Best Director
| Peter Jackson
|  
|- Best Actor
| Richard Armitage
|  
|- Best Sci-Fi/Fantasy
| The Hobbit: The Battle of the Five Armies
|  
|- 41st Saturn Saturn Awards Best Fantasy Film
| The Hobbit: The Battle of the Five Armies
|  
| style="text-align:center;"  rowspan="7" | 
|- Best Writing
| Fran Walsh, Philippa Boyens, Peter Jackson and Guillermo del Toro
|  
|- Best Supporting Actor Richard Armitage
|  
|- Best Supporting Actress
| Evangeline Lilly
|  
|- Best Music
| Howard Shore
|  
|- Best Make-up Peter King, Rick Findlater and Gino Acevedo
|  
|- Best Special Effects
| Joe Letteri, Eric Saindon, David Clayton and R. Christopher White
|  
|}

==Notes==
 

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 