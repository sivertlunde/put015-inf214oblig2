List of horror films of 1976
 
 
A list of horror films released in 1976 in film|1976.

{| class="wikitable sortable" 1976
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" class="unsortable"| Cast
! scope="col" | Country
! scope="col" class="unsortable"| Notes
|-
!   | Alice, Sweet Alice
| Alfred Sole || Lillian Roth, Brooke Shields, Alphonso DeNoble ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as Communion, Holy Terror}}
|-
!   | A*P*E
| Paul Leder || Rod Arrants, Joanna Kerns ||    ||  
|-
!   | Blood Bath
| Joel Reed || Harve Presnell, Doris Roberts, P.J. Soles ||   ||  
|-
!   | Bloodlust
| Marijan David Vajda || Werner Pochath ||   ||  
|-
!   | Blood Sucking Freaks
| Joel Reed || Ernie Pysher, Viju Krem, Seamus OBrien ||   ||  
|- Burnt Offerings
| Dan Curtis || Karen Black, Oliver Reed, Burgess Meredith ||   ||  
|-
!   | Carrie (1976 film)|Carrie
| Brian De Palma || Sissy Spacek, Piper Laurie ||   ||  
|-
!   | Death Machines
| Paul Kyriazi || Mari Honjo, Ronald L. Marchini, Philip DeAngelo ||   ||  
|-
!   | Dr. Black and Mr. White William Crain || Bernie Casey, Rosalind Cash, Marie OHenry ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as The Watts Monster}}
|-
!   | Eaten Alive
| Tobe Hooper || Neville Brand, Mel Ferrer, Marilyn Burns ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as Death Trap, Horror Hotel Massacre, Legend of the Bayou, Starlight Slaughter}}
|-
!   | God Told Me To Richard Lynch, Tony Lo Bianco ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as Demon}}
|-
!   | Grizzly (1976 film)|Grizzly
| William Girdler || Mary Ann Hearn, Charles Kissinger, Joan McCall ||   ||  
|-
!   | The House with Laughing Windows
| Pupi Avati || Lino Capolicchio, Francesca Marciano ||   ||  
|-
!   | Inquisition
| Jacinto Molina || Juan Gallardo, Daniela Giordano ||   ||  
|-
!   | J. D.s Revenge
| Arthur Marks || Glynn E. Turman, Joan Pringle, Carl Crudup ||   ||  
|- Jack the Ripper
| Jesus Franco || Lina Romay, Klaus Kinski, Herbert Fux ||    ||  
|-
!   | Land of the Minotaur
| Kostas Karagiannis || Donald Pleasence, Jessica Dublin, Peter Cushing ||     ||  
|- The Legend of the Wolf Woman
| Rino Di Silvestro || Dagmar Lassander, Annik Borel, Tino Carraro ||   ||  
|-
!   | Mansion of the Doomed
| Michael Pataki || Richard Basehart, Gloria Grahame, Lance Henriksen ||   ||   
|-
!   | The Omen David Warner ||   ||  
|-
!   | Rattlers (film)|Rattlers John McCauley || Elizabeth Guadalupe Chauvet, Sam Chew, Dan Priest ||   || Television film 
|- The Rat Savior
| Krsto Papic || Boris Festini, Zdenka Trach, Mirjana Majurec ||   ||  
|-
!   | Savage Weekend David Gale, William Sanderson ||   ||  
|-
!   | Squirm (film)|Squirm
| Jeff Lieberman || Jean Sullivan, Don Scardino, Patricia Pearcy ||   ||  
|-
!   | Snuff (film)|Snuff
| Michael Findlay, Horacio Fredriksson, Simon Nuchtern || Margarita Amuchástegui, Aldo Mayo, Liliana Fernández Blanco ||     ||  
|-
!   | To the Devil a Daughter Peter Sykes || Richard Widmark, Christopher Lee, Honor Blackman ||    ||  
|-
!   | The Town That Dreaded Sundown Ben Johnson, Andrew Prine, Dawn Wells ||   ||  
|-
!   | Track of the Moonbeast
| Richard Ashe || Chase Cordell, Leigh Drake, Gregorio Sala ||   ||  
|-
!   | Who Can Kill a Child?
| Narciso Ibáñez Serrador || Lewis Fiander, Prunella Ransome ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as Island of the Damned, Lucifers Curse, Death is Childs Play, Would You Kill a Child?}}
|}

==References==
 

==Citations==
 
*  <!--
-->
*  <!--
-->
 

 
 
 

 
 
 
 