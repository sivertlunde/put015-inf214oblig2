Barrier Device
{{Infobox film
| image          =
| alt            =
| caption        = Grace Lee
| producer       = {{plainlist|
* Caroline Libresco 
* Rosie Wong
}}
| writer         = Grace Lee
| starring       = {{plainlist|
* Sandra Oh
* Suzy Nakamura
}}
| music          = {{plainlist|
* Billy Cote
* Mary Lorson
}}
| cinematography = Patricia Lee
| editing        = Grace Lee
| studio         =
| distributor    =
| released       =   }}
| runtime        = 26 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Grace Lee.  It stars Sandra Oh as a sex researcher and Suzy Nakamura as a subject.  It won several awards, including the silver medal at the  29th Student Academy Awards.

== Plot ==
Researcher Audrey conducts a study on female condoms.  In the course of her work, she discovers that Serena, one of her subjects, is involved with her ex-fiance.  Torn between professional integrity and curiosity, Audrey attempts to learn more about Serenas life without compromising the her work.

== Cast ==
* Sandra Oh as Audrey
* Suzy Nakamura as Serena
* Melinda Peterson as Dr. Campbell
* Jonathan Liebhold as Dwight
* Brian Kim as Brian

== Production ==
Barrier Device was Grace Lees masters thesis at UCLA.  Lee directly asked Oh to appear in her film. 

== Release ==
Barrier Device premiered at the 2002 CAAMFest. 

== Reception ==

=== Awards ===

{| class="wikitable"
! Year !! Organization  !! Award !! Recipient !! Result !! Ref
|-
| 2002  || Student Academy Awards || Silver medal || Grace Lee ||   ||  
|-
| 2002 || Urbanworld Film Festival || Grand Jury Prize || Grace Lee ||   ||  
|-
| 2002 || Palm Springs International Film Festival || Best Short over 15 Minutes || Grace Lee ||   ||  
|-
| 2002 || Directors Guild of America || Best Asian American Film || Grace Lee ||   ||  
|-
|}

== References ==
 

== Further reading ==
*  

== External links ==
*  

 
 
 
 
 
 
 
 