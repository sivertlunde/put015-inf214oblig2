Joe MacBeth
{{Infobox film
| name           = Joe MacBeth
| image          = Joe MacBeth film poster.jpg
| caption        = Theatrical release poster
| director       = Ken Hughes
| producer       = M.J. Frankovich George Maynard (executive producer)
| writer         = Ken Hughes Philip Yordan
| based on         =  
| starring       = Paul Douglas Ruth Roman Bonar Colleano
| music          = Trevor Duncan	 	
| cinematography = Basil Emmott
| editing        = Peter Rolfe Johnson
| studio         = 
| distributor    = Columbia Pictures
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
}}

Joe MacBeth is a 1955 British–American crime drama, directed by Ken Hughes and starring Paul Douglas, Ruth Roman and Bonar Colleano.  It is a modern retelling of Shakespeares Macbeth, set in a 1930s American criminal underworld. The films plot closely follows that of Shakespeares original play. 

==Cast==
* Paul Douglas – Joe MacBeth
* Ruth Roman – Lily MacBeth
* Bonar Colleano – Lennie
* Grégoire Aslan – Duca, aka The Duke
* Sid James – Banky
* Harry Green – Big Dutch
* Walter Crisham – Angus
* Kay Callard – Ruth
* Robert Arden – Ross
* George Margo – Second Assassin
* Minerva Pious – Rosie
* Philip Vickers – Tommy
* Mark Baker – Benny
* Bill Nagy – Marty
* Nicholas Stuart – Duffy
* Teresa Thorne – Ruth
* Shirley Douglas
* Alfred Mulock – First assassin
* Louise Grant
* Beresford Egan

==References==
 

==Bibliography==
* Jackson, Russell. The Cambridge companion to Shakespeare on film. Cambridge University Press, 2007.

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 


 