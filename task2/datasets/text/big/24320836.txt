The Shanghai Drama
 
{{Infobox film
| name           = The Shanghai Drama
| image          = Le drame de Shanghaï.jpg
| caption        = Film poster
| director       = Georg Wilhelm Pabst
| producer       = Romain Pinès Marc Sorkin
| writer         = Alexandre Arnoux Oscar Paul Gilbert Henri Jeanson Léo Lania
| starring       = Raymond Rouleau
| music          = 
| cinematography = 
| editing        = Jean Oser
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = France
| language       = French
| budget         = 
}}

The Shanghai Drama ( ) is a 1938 French drama film directed by Georg Wilhelm Pabst.    The films sets were designed by Guy de Gastyne.

==Cast==
* Raymond Rouleau as Franchon
* Louis Jouvet as Ivan
* Christl Mardayn as Kay Murphy, cabaret singer (as Christiane Mardayne)
* Elina Labourdette as Nana, the nurse
* Valéry Inkijinoff as Black Dragon Agent
* Dorville as Bill, cabaret owner
* André Alerme as Mac Tavish (as Alerme)
* Suzanne Desprès as Vera
* Gabrielle Dorziat as Superintendent of school
* Marcel Lupovici as Assassin for Black Dragon Robert Manuel as Le client attaqué
* PierreasLouis as Un marin américain
* LinhasNam as Cheng
* FounasSen as Wife of Black Dragon Agent
* Mila Parély as Dancing Girl

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 