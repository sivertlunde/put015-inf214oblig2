Anjaane (2005 film)
{{Infobox film
| name           = Anjaane
| image          = Anjane the unknown.jpeg
| image_size     = 
| caption        = 
| director       = Harry W. Fernandes
| producer       = Dr. Anil K. Mehta
| writer         = 
| narrator       =  Helen Tanwi Gouri Mehta Anand Mehta
| music          = Himesh Reshammiya
| cinematography = Rajan Kinagi
| editing        = 
| distributor    = 
| released       = 30 December 2005
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Helen Tanwi Gouri MehtaAnand Mehta.
 The Others starring Nicole Kidman.

==Synopsis==
This film is the third release of Holland based Dr. Anil K. Mehta. 
The chief protagonists are Shivani   and Aditya  , a married couple with 2 children, Tanwi Gouri Mehta and Anand Mehta. They are wealthy and live in a mansion. Their idyllic life is shattered with the arrival of Sonia, who has an affair with Adiyta. Eventually, Aditya leaves his family for Sonia, leaving the wife and two kids alone. When it is found out that Sonia is barren, Aditya demands custody of his children from Shivani and approaches the courts for a custody battle. Eventually, the courts rule against Shivani and award custody to Aditya and Sonia. The movie changes its ambience from the family dispute to one of suspense and horror shortly after.

==Reviews==
The film was not well received. It was criticized for being a "copycat" of the Spanish film "The Others" and the acting of Koirala was described by reviewers as "uninterested" and "mechanical". The direction under Fernandes was "devoid of passion", although the cinematography was praised.

==References==
* 
* 
* 
* 

== External links ==
*  

 
 
 
 


 