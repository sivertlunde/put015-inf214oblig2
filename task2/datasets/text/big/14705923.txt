The Threepenny Opera (1931 film)
{{Infobox film
| name           = The Threepenny Opera
| image          = Threepennyopera1931.jpg
| caption        =
| director       = G. W. Pabst
| producer       = Seymour Nebenzal
| writer         = Béla Balázs Leo Lania Ladislaus Vajda Margo Lion Carola Neher Lotte Lenya Reinhold Schünzel
| music          = Kurt Weill
| musical director = Theo Mackeben
| cinematography = Fritz Arno Wagner
| art direction  = Andrej Andrejew
| editing        = Hans Oser (German version), Henri Rust (French version)
| distributor    = Nero-Film Tobis Filmkunst Warner Bros. Berlin
| released       =  
| runtime        = 113 minutes (German version) / 107 minutes (French version)
| country        = Germany
| language       = German-language|German- and French-language|French-language versions
| budget         =
}}

The Threepenny Opera ( ) is a 1931 German musical film directed by G. W. Pabst. It was produced by Seymour Nebenzals Nero-Film for Tonbild-Syndikat AG (Tobis), Berlin and Warner Bros. Pictures GmbH, Berlin. The film is loosely based on the 1928 musical theatre success The Threepenny Opera by Bertolt Brecht and Kurt Weill. As was usual in the early sound film era, Pabst also directed a French language version of the film, LOpéra de quatsous, with some variation of plot details (the French title literally translates as "the four penny opera"). A planned English version was not made. The two existing versions were released by The Criterion Collection on home video. English version The Threepenny Opera from the Warner Archive Collection has not been announced on DVD.

The Threepenny Opera differs in significant respects from the play, and the internal timeline is somewhat vague. The whole of society is presented as corrupt in one form or another. Only some of the songs from the play are used, in a different order.

==Plot==
  (right) and actor Albert Préjean (as Mackie Messer) during filming the film LOpéra de quatsous (The Threepenny Opera) in 1931.]]
Macheath aka "Mack the Knife" ("Mackie Messer" in German) is presented as an anti-hero and is in league with Tiger Brown, Chief of Police, who is in charge of the coronation of an unspecified queen.

Macheath marries Polly Peachum, daughter of Jonathan Jeremiah Peachum, but still visits the brothel on Turnmill Street. Peachum is displeased at his daughters marriage, and threatens Brown with disruption of the coronation - arranging for a protest march of beggars and others. Macheath is arrested, after a rooftop escape from the brothel where he had gone to visit Jenny, his former lover  during a Raid (military)|raid. Macheath is imprisoned and sentenced to death.

Polly, meanwhile, buys a bank, and runs it with Macheaths henchmen, making him a bank director, and she then arranges surety for Macheath to leave prison. This causes a change of heart by her parents - her father tries to stop the protest march but fails.

Jenny visits the prison, and aids Macheaths escape: he makes his way to the bank, where he discovers his new status. Brown, whose police career is ruined by the demonstration, and Peachum, also come to the bank and agree to link up.

==Cast==
;German-language version
* Rudolf Forster as Mackie Messer
* Carola Neher as Polly
* Reinhold Schünzel as Tiger-Brown
* Fritz Rasp as Peachum
* Valeska Gert as Frau Peachum
* Lotte Lenya as Jenny
* Hermann Thimig as Pfarrer Ernst Busch as Moritatensänger
* Vladimir Sokoloff as Smith, Gefängniswärter Paul Kemp, Oskar Höcker, Krafft-Raschig as Mackie Messers Platte (Mackies gang)
* Herbert Grünbaum as Filch
* Sylvia Torff as Bordellbesitzerin

;French-language version
* Florelle as Polly Peachum
* Albert Préjean as Mackie
* Gaston Modot as Peachum
* Margo Lion as Jenny
* Vladimir Sokoloff as Smith, geôlier
* Lucy de Matha as Mme. Peachum
* Jacques Henley as Tiger-Brown
* Bill-Bocketts as chanteur de rues
* Hermann Thimig as pasteur
* Antonin Artaud as nouveau mendiant
*   as mendiant
* Marie-Antoinette Buzet as fille à Turnbridge
* Arthur Duarte, Marcel Merminod, Pierre Léaud (the father of Jean-Pierre Léaud), Albert Broquin as bande à Mac

==External links==
*  
*   at Rotten Tomatoes
*   on The Threepenny Opera by Tony Rayns
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 