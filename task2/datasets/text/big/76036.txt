Pull My Daisy
 
  Beat Generation; Richard Bellamy (Bishop) and Delphine Seyrig (Milos wife), dancer  Sally Gross (bishops sister), and Pablo Frank, Robert Franks then-young son.
 bohemian friends crash the party, with comic results.
 poem of the same name written by Kerouac, Ginsberg, and Cassady in the late 1940s. Part of the original poem was used as a lyric in David Amrams jazz composition that opens the film. (In 1959, Metro-Goldwyn-Mayer released a feature film called The Beat Generation.)
 studio set.

Leslie and Frank discuss the film at length in  . An illustrated transcript of the films narration was also published in 1961 by Grove Press.

Pull My Daisy was selected for preservation in the United States National Film Registry by the Library of Congress in 1996, as being "culturally, historically, or aesthetically significant".

The impromptu narration juxtaposed with particular shots portray the Beat Generation in an autobiographical sense.  The film editing process began with a picture lock and was a conservative, narrative story arc conceived by Jack Kerouac, directed by Alfred Leslie and shot by Robert Frank.  The narration was then improvised by Kerouac resulting in a film that defines the Beat Generation, making a comment on a number of topics representative of conservative America, including protests against industrialization, education, anti-Semitism, sexuality, gender roles, religion and patriotism.

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 