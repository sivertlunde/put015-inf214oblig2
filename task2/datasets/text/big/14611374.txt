The Days (film)
{{Infobox film
| name           = The Days
| image          = The_Days.jpg
| image_size     = 
| caption        = 
| director       = Wang Xiaoshuai
| producer       = Zhang Hongtao Wang Xiaoshuai
| writer         = Wang Xiaoshuai
| narrator       = 
| starring       = Liu Xiaodong Yu Hong
| music          = Liang Heping Wu Di Liu Jie
| editing        = Qingping
| distributor    = Fortissimo Films
| released       = 1993
| runtime        = 80 min. China
| Mandarin Chinese
| budget         = US$10,000 (approximate)
| preceded_by    = 
| followed_by    = 
}}	
 filmmaker Wang Xiaoshuais 1993 directorial debut. Filmed entirely in black-and-white, The Days follows the life of Dong (played by actor and artist Liu Xiaodong), and Chun (Yu Hong), married artists who have recently graduated from the Beijing Art Institute. Living meagerly in the hope of making enough money off their works, it soon becomes obvious to everyone but themselves that the marriage has begun to die.

Wangs first film on his own after graduating from the Beijing Film Academy in 1989, The Days was shot on a meager budget of less than $10,000 (U.S.), with filming on the weekends with Wangs friends playing the lead roles. 

Made outside of the state film system, The Days was blacklisted upon its release by the Chinese Film Bureau.  On the international front, however, the film was seen in a different light. Riding the high that Chinese cinema was enjoying abroad at the time (notably by older directors such as Zhang Yimou and Chen Kaige), Wang Xiaoshuais small independent film was an early indication that a new movement was beginning to supplant the old one.

==Inspiration== Farewell My Concubine received critical accolades on the international circuit. Wang, however, found them "unnatural and pretentious."  As such, he set out to create a film that spoke to the contemporary Chinese generation. As Wang himself has said, "there may not be an obvious story line in The Days, but at least it presents the truth about the lives of people from my generation in the wake of the Tiananmen Square tragedy." 

==Production==
Upon graduation from the Beijing Film Academy in 1989, Wang was initially assigned to the state-run Fujian Film Studio. Rather than beginning his tenure there, Wang decided to stay in Beijing in an effort to create an independent film. Berry, p. 165.  This process lasted nearly a year and ultimately collapsed when Wang failed to obtain proper financing. As a result, he began working at Fujian, writing screenplays that would never be made.  His tenure at the Fujian Film Studio would be equally brief, however, and by 1992, Wang had returned to Beijing determined to make it on his own. Berry, p. 166. 

In the beginning, Wang, along with actor Liu Xiaodong and classmate Zhang Yuan attempted an extremely small-scale production that would eventually collapse. The effort, though a failure, galvanized Wang to begin again with The Days. Due to an extremely low budget, the production would prove to be arduous. The actors and crew, mostly Wangs friends, donated both time and money to the film.  Equipment was rented and usually obsolete.  Even obtaining film was a trial, as Wang and cinemtographer Liu Jie were forced to travel to the manufacturer in Baoding to personally request film stock. 

Logistically, the film was also a difficult process as all the actors and crew had normal jobs. As a result, filming could only take place two days a week, on the weekends.  Equipment would be rented on Friday and returned on Monday, the process repeating each week. 

==Cast==
*Dong, a young artist played by actual artist Liu Xiaodong. His name is the "Dong" in the Chinese title with a literal meaning of "Winter."
*Chun, Dongs young wife, played by artist Yu Hong. Her name means "Spring (season)|Spring." Both Yu and Liu were personal friends of Wang Xiaoshuai. 

==Reception==
The Days was screened at several film festivals notably International Film Festival Rotterdam|Rotterdam, and Berlin International Film Festival|Berlin; it ultimately went on to win the Golden Alexander at the 1995 International Thessaloniki Film Festival and a Best Director award for Wang at the 1995 Taormina Film Festival.  The Days was also selected as one of the best 100 films of all time by the BBC in 1995. 
 film distributors at all, but television stations. Berry, p. 168. 
 
Like many underground features, however, The Days was never released in the Peoples Republic of China, either commercially or on television.  Indeed, because the film had been screened at several international film festivals without permission, the Film Bureau initially blacklisted Wang from making any new films. 

==References==
 

==External links==
 
* 
* 
*  at the Chinese Movie Database
*  at Fortissimo Films

 

 
 
 
 
 
 
 
 
 
 