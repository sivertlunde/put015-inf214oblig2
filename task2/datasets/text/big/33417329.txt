Malaya Marutha
{{Infobox film
| name = Malaya Marutha
| image =
| caption =
| director = K. S. L. Swamy (Lalitha Ravi)
| producer = C. V. L. Shastry
| writer = Lalitha Ravi Madhavi
| music = Vijaya Bhaskar
| cinematography = B. Purushottam Gopinath
| editing =
| distributor =
| released =  
| runtime =
| country = India
| language = Kannada
| budget =
| gross =
}} Musical Kannada Madhavi in the lead roles. The film was directed and written by K. S. L. Swamy (Lalitha Ravi) and produced by C. V. L Shastry under "Shastry Movies" production house. Vijaya Bhaskar has composed the soundtrack and the background score.

==Cast== Vishnuvardhan
* Madhavi
* Saritha
* M. S. Umesh

==Soundtrack==
{| class="wikitable" style="width:70%;"
|-
! Song Title !! Singers !! Lyrics
|-
| "Malaya Marutha Gaana" || K. J. Yesudas, S. Janaki || Chi. Udhayashankar
|-
| "Yellelloo Sangeethave" || K. J. Yesudas || Chi. Udhayashankar
|-
| "Sharade Daye" || K. J. Yesudas || Chi. Udhayashankar
|-
| "Natana Visharada" || K. J. Yesudas || Kanagal Prabhakar Shastry
|-
| "Srinivasa Enna Bittu" || K. J. Yesudas || Purandara Dasa
|-
| "Sangeetha Jananamu" || K. J. Yesudas || Thyagaraja
|-
| "Madhuramba Bhajare" || K. J. Yesudas || Muthuswami Dikshitar
|-
| "Hindanagali Hidivadeda" || Vani Jayaram, K. J. Yesudas || Akka Mahadevi
|-
| "Ellaru Maaduvadu" || K. J. Yesudas || Kanaka Dasa
|-
| "Sakala Karya Karanige" || K. J. Yesudas || Vijaya Narasimha
|-
| "Adharam Madhuram" || Vani Jayaram, S. P. Balasubramanyam || Vallabha Acharya
|-
| "Ee Sneha Ninade" || Vani Jayaram, S. P. Balasubramanyam || R. N. Jayagopal
|-
| "Amma Ninna Nodidare" || S. P. Balasubramanyam || Chi. Udhayashankar
|-
| "Asathoma Sadgamaya" || Ravi || Chi. Udhayashankar
|-
| "Yam Shaivassampupasathe" || K. J. Yesudas || Chi. Udhayashankar
|}
 

==Awards==
* K. J. Yesudas won Karnataka State Film Award for Best Male Playback Singer in 1986.
This film screened at 11th IFFI mainstream section.

==References==
 

==External links==
*  

 

 
 
 
 


 