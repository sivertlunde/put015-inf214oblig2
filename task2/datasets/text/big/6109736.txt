Her Minor Thing
{{Infobox Film |
  name        = Her Minor Thing |
  image       = Her minor thing dvd cover.jpg |
  caption     = DVD cover |
  writer      = Jim Meyers Debra Meyers |
  starring    = Estella Warren Christian Kane Michael Weatherly |
  director    = Charles Matthau |
  producer    = Jim Meyers Debra Meyers Heather Simpson Vikki Wagner |
  music       = Todd Holden Capps |
  cinematography = John J. Connor |
  editing     = Jessica Congdon |
  distributor = First Look International |
  country     = USA | 
  language    = English | 
  }}

Her Minor Thing is a 2005 motion picture. The romantic comedy is directed by Charles Matthau, written by Jim Meyers and Debra Meyers, and stars Estella Warren, Christian Kane, and Michael Weatherly.

The scripts working title was "Men Are Jerks".

==Synopsis==

Everything was going fine for hot young 25 year old Jeana (Estella Warren) until her newscaster boyfriend accidentally revealed on TV that Jeana herself is still a virgin despite being in her mid 20s. Jeana is completely humiliated by this, and adding insult to injury is the fact that the revelation of her virginity has made her a target for every oversexed man in the immediate area, all of them determined to "make a real woman out of her". Noting dejectedly that ever since it was revealed that she is a virgin, everyman who looks at her "wants a date", all her optimistic romantic ideals have been shattered. Ultimately she cannot salvage her relationship with her newscaster boyfriend, but in the end, she does finally find a man, a photographer, who is decent enough to be the man to whom she finally loses her virginity.

==Cast & Crew==
===Cast===
{| class="wikitable"
|- bgcolor="#FFFFFF"
! Actor || Role
|-
| Estella Warren || Jeana Summers
|-
| Christian Kane || Paul
|-
| Michael Weatherly || Tom Lindeman
|-
| Rachel Dratch || Caroline
|-
| Flex Alexander || Marty
|-
| Kathy Griffin || Maggie
|-
| Ivana Miličević || Zsa Zsa
|-
| Victoria Jackson || Norma
|- Abdinazir Gedi         
|}

===Production Team===
{| class="wikitable"
|- bgcolor="#FFFFFF"
! Name ||
|-
| Charles Matthau || director
|-
| John J. Connor || cinematographer
|-
| Jim Meyers || writer, producer
|-
| Debra Meyers || co-writer, producer
|- Heather Simpson || producer
|-
| Vikki Wagner || producer
|-
|}

==Production==
 
The film was shot in 23 days, in June–July 2004. 

==Awards Won==

* 2006 Special Jury Award - Christian Kane - Special Jury Prize Acting Achievement

==See also==
* Estella Warren
* Christian Kane
* Michael Weatherly
* Rachel Dratch
* Kathy Griffin
* Victoria Jackson

==References==
 
==External links==
*  
*  
*  
*  


 
 
 
 
 
 