Ice Station Zebra
 
 

{{Infobox film
|  name           = Ice Station Zebra
|  image          = Ice Station Zebra (film) poster.jpg poster by Howard Terpning
|  director       = John Sturges
|  producer       = James C. Pratt  Martin Ransohoff  John Calley
|  writer         = Alistair MacLean  Douglas Heyes  Harry Julian Fink  W.R. Burnett|W. R. Burnett
|  starring       =  Rock Hudson  Ernest Borgnine  Patrick McGoohan  Jim Brown 
|  cinematography = Daniel L. Fapp
|  music          = Michel Legrand
|  editing        = Ferris Webster
|  distributor    = Metro-Goldwyn-Mayer
|  released       =  
|  runtime        = 148 minutes English
|  budget         = $8 million Glenn Lovell, Escape Artist: The Life and Films of John Sturges, University of Wisconsin Press, 2008 p264-269 
| gross           = $4.6 million 
}}
 novel of the same name. Both have parallels to real-life events that took place in 1959. The film was photographed in Super Panavision 70 by Daniel L. Fapp, and presented in 70&nbsp;mm Cinerama in premiere engagements. The original music score is by Michel Legrand. 

==Plot summary==

=== Part One ===

A satellite reenters the atmosphere and ejects a capsule which parachutes to the Arctic, coordinates 85N 21W (approx 320 miles WNW of Nord, Greenland, in the Arctic Ocean ice pack). During an ice storm, a figure soon approaches, guided by a homing beacon, while a second individual secretly watches from nearby.
 captain of the U.S. nuclear attack submarine USS Tigerfish (SSN-509), stationed at Holy Loch, Scotland. He is ordered by Admiral Garvey (Lloyd Nolan) to rescue the personnel of Drift Ice Station Zebra, a British civilian scientific weather station moving with the ice pack. However, the mission is actually a cover for a highly classified assignment.
 Marine platoon. While underway, a SH-2 Sea Sprite helicopter delivers combat commander Captain Anders (Jim Brown), who takes command of the Marines, and Boris Vaslov (Ernest Borgnine), an amiable Russian defector and spy, who is a trusted colleague of Jones.

The Tigerfish makes its way under the ice to Zebra’s last-known position. Ferraday decides to use a torpedo to blast an opening in the thick ice. However, the crewmen suddenly find the torpedo tube is open at both ends, killing torpedo officer Lt. Mills as seawater floods in, plunging the sub toward its rated crush depth. Jones helps to close the tube but, even so, Ferraday and his crew are barely able to save themselves. During the investigation of the torpedo tube, Ferraday quickly determines that this malfunction should be impossible but Jones describes how someone could intentionally rig the tube to malfunction. Both Jones and Ferraday conclude that there is a saboteur aboard. Ferraday suspects Vaslov, while Jones suspects Anders, who is the least known member of the rescue team to Jones, Ferraday and Vaslov, and universally disliked for his strict demeanor. Jones demands Ferraday complete the mission regardless of the risk, and Ferraday refuses, unless he knows the purpose of the mission first. At that moment, an area of thin ice is located, and Ferraday surfaces the Tigerfish.

===Part Two===
 
Ferraday, Vaslov, Jones, and a rescue party set out for the weather station in zero visibility. They reach Zebra to find several of its buildings burned and the scientists nearly dead from hypothermia. Jones and Vaslov begin questioning the survivors. It becomes obvious that the two spies are looking for something.

Ferraday reveals to Jones that he knows more about the mission than he is supposed to, saying "We dont believe in going on a mission totally blindfolded". Jones reveals to Ferraday that an advanced experimental British camera was stolen by the Soviets, along with an enhanced film emulsion developed by the Americans. The Soviets sent it into orbit to photograph the locations of all the American missile silos. However, the camera malfunctioned and continued to record Soviet missile sites as well. A second malfunction forced re-entry in the Arctic, close to Ice Station Zebra. Soon after, undercover Soviet and British agents arrived to recover the film capsule, and the civilian scientists at Zebra were caught in the crossfire between them.

As the weather clears, Ferraday sets his crew to searching for the capsule. Jones eventually finds a hidden tracking device. He is blind-sided and knocked unconscious by Vaslov, who is a Soviet agent and the saboteur they have been looking for. But before Vaslov can make off with his prize, he is confronted by Anders. As the two men fight, a dazed Jones shoots and kills Captain Anders due to Vaslovs manipulation of the scenario.

 
Tigerfishs radar picks up Soviet aircraft heading toward Zebra.  Ferraday remains suspicious of Vaslov, but allows him to use the tracker to locate the capsule, buried in the ice. As Ferradays crew extracts the capsule, Russian paratroopers land at the scene. Their commander, Colonel Ostrovsky (Alf Kjellin), demands the capsule. Believing that the Americans have already secured the canister, the Russian commander threatens to activate the self-destruct mechanism with his radio-detonator. Ferraday stalls while Vaslov defuses the booby-trapped capsule and takes out the film. Ferraday hands over the empty container, but the deception is revealed and a brief firefight breaks out. In the confusion, Vaslov makes a break with the film canister. Jones stops Vaslov, mortally wounds him, and retrieves the film.

Ferraday orders Jones to hand the film over to the Soviets. However, Ferraday had earlier found a radio-detonator identical to Ostrovskys. The Russians send the canister aloft by balloon for recovery by an approaching jet fighter. Lieutenant Walker makes a desperate attempt to get Ostrovskys detonator, but fails and is killed. Commander Ferraday then activates his detonator, destroying the film. Ostrovsky concedes that both his and Ferradays missions are accomplished, at least in part, and leaves, allowing the Tigerfish to complete the rescue of the civilians. A dissolving segue shows a teletype machine churning out a news story hailing the success of the "humanitarian" mission as an example of cooperation between the West and the Soviet Union.

== Cast ==

* Rock Hudson as Commander James Ferraday, USN
* Ernest Borgnine as Boris Vaslov
* Patrick McGoohan as David Jones of MI6
* Jim Brown as Captain Leslie Anders, USMC
* Tony Bill as 1st Lieutenant Russell Walker, USMC
* Lloyd Nolan as Admiral Garvey, USN
* Alf Kjellin as Colonel Ostrovsky, the Soviet commander
* Gerald S. OLoughlin as Lieutenant Commander Bob Raeburn, USN
* Ted Hartley as Lieutenant Jonathan Hansen, USN
* Michael Mikler as Lt Courtney Cartwright, USN (navigator)
* Ron Masak as radioman Paul Zabrinczski, USN
* Murray Rose as torpedoman officer Lt George Mills, USN

===Differences from the novel===

While based on Alistair MacLeans 1963 Cold War Thriller (genre)|thriller, the film version diverges from its source material.

The most obvious changes involved the names of the novels characters: USS Dolphin, launched between the release of the novel and the making of the film, was a diesel submarine.
* British spy Dr. Carpenter was renamed David Jones.
* Commander Swanson was changed to Commander Ferraday.

Beyond the name change, the films submarine has a more traditionally   episode "Submarine".
 Soviet defector Soviet interest fishing trawler waiting outside Holy Loch when the Tigerfish sets sail.

In the novel, there is no Russian submarine, no Russians on the ice, and no confrontation of any kind on the ice with the Russians.

The novels fire on board the submarine does not occur in the film, whereas the nearly fatal flooding of the forward torpedo room is common to the film and the novel.

The sabotage of the torpedo tubes is committed by a suspected intelligence agent in the film. In the novel, a port maintenance worker is suspected.
 Soviet paratroopers and the American Marines, but concludes on a more ambiguous note than the novel, reflecting the perceived thaw in the Cold War following the Cuban Missile Crisis.

==Production==

The film rights to the 1963 novel were acquired the following year by producer Martin Ransohoff who hoped to capitalize on the success of the 1961 blockbuster The Guns of Navarone (film)|The Guns of Navarone by adapting another Alistair MacLean novel for the silver screen as a follow-up.

Navarone stars Gregory Peck and David Niven were initially attached to this film, with Peck as the sub commander and Niven as the British spy, plus Edmond OBrien and George Segal in the other key roles. Filming was set to begin in April 1965, but scheduling conflicts and USA Department of Defense|U.S. Department of Defense objections   over Paddy Chayefskys screenplay and its depiction of naval life on board the submarine delayed the start.
 Guppy IIA Guppy IA   was used, near Pearl Harbor. The underwater scenes used a model of a Skate class submarine|Skate class nuclear submarine.

Second unit cameraman John M. Stephens developed an innovative underwater camera system that successfully filmed the first continuous dive of a submarine, which became the subject of the documentary featurette The Man Who Makes a Difference.

During filming, McGoohan had to be rescued from a flooded chamber by a diver who freed his trapped foot, saving his life. 
 TV series Number Six transferred into the body of another character.

==Plot origin and cultural impact==
 Corona  Soviet agents. In 2006, the USA National Reconnaissance Office declassified information stating that "an individual formerly possessing Corona access was the technical adviser to the movie" and admitted "the resemblance of the loss of the Discoverer II capsule, and its probable recovery by the Soviets" on Spitsbergen Island, to the book by Alistair MacLean. 

The story has parallels with the Central Intelligence Agencys Operation Coldfeet, which took place in May–June 1962. In this operation, two American officers parachuted from a CIA-operated B-17 Flying Fortress|B‑17 Flying Fortress to an abandoned Soviet ice station. After searching the station, they were picked up three days later by the B-17 using the Fulton surface-to-air recovery system.
 HMS Thetis in Liverpool Bay in 1939. As in the film the drip cock was blocked on the newly built Thetis (by dockyard-applied fresh paint) which led to the rear cap being opened while the bow cap was already open to the ocean. Water entered at the rate of one ton per second and the Thetis sank with the loss of  98 lives. In the movie the drip cock has been blocked with epoxy adhesive.
 United States, is said to have watched a private print of Ice Station Zebra 150 times on a continuous loop in his private hotel suite during the years prior to his death.  In his 2013 autobiography My Way, singer Paul Anka writes that Hughes kept a permanent penthouse at the Desert Inn hotel in Las Vegas and owned a local TV station. "We knew when Hughes was in town," Anka wrote. "Youd get back to your room, turn on the TV at 2 a.m. and the movie Ice Station Zebra would be playing. At 5 a.m., it would start all over again. It was on almost every night. Hughes loved that movie."
 ABC made-for-television William Windom, Dewey Martin, which also featured Zebra cast members Lloyd Haynes and Ron Masak. 
 letter Z phonetic alphabet. International Geophysical Year (IGY.).  USS Skate surfaced in an open lead next to Station A at 0700 on August 14, 1958." Frans van der Hoeven (Senior Scientist at Station Alpha) and Norbert Untersteiner (Scientific Leader of Station Alpha), filmed the event and Untersteiners narration explains how the submarine found the ice island: "Station A received request from USS Skate for a position report on August 12. On August 13, still unable to provide an accurate position, two men kept a boat with an outboard motor going from 1400 to 2400 hours, but had no contact from the submarine.
On August 14, at 0700 the Skate surfaced about eight miles from Station A. The sub’s skipper requested the motor boat again, which circled in a large lead next to the island. At 0835 the sub surfaced in the exact center of the lead."  
 
You can now view the 33-minute film at Climate Science TV. "http://climatescience.tv/2012/06/floating-on-top-of-the-world-the-international-geophysical-year-and-drifting-station-alpha.html" The submarine event is located at 26:08 minutes.

The Skate did not stay as long as planned. At 0800 hours the following day, "...ice movement began throwing up pressure ridges 100 feet in front of the camp and it also started closing the lead in which the submarine surfaced." As parting gifts the Navy gave the Scientists and the Air Force support team eight gallons of ice cream and the Air Force gave the Navy two sides of polar bear meat.
 1978 disaster 1983 James James Bond 1982 Cold thriller Firefox (film)|Firefox.

Sportscaster Chris Berman, known for his whimsical nicknames for athletes, referred to major league pitcher Bob Sebra as "Ice Station Sebra".

An episode of Sealab 2021 has researchers in the Antarctic trapped on "Ice Station Zebra".

In the eleventh episode of the third season of Breaking Bad, Walter White hands Skyler a check from "Ice Station Zebra Associates". Ice Station Zebra is his attorneys fake company in the show (first mentioned in the eighth episode of the second season).

== Reception ==

=== Box office ===
Ice Station Zebra was released on October 23, 1968. The film became a major hit, which gave a much-needed boost to Rock Hudsons flagging career. 

=== Critical response ===

Critic Roger Ebert did not like the film, saying it was "so flat and conventional that its three moments of interest are an embarrassment", and finally describing it as "a dull, stupid movie", expressing disappointment that the special effects did not, in his opinion, live up to advance claims.   

Director     Access date 2009-09-18.
 

=== Awards ===
 Romeo and Juliet).

==Remake==
On May 6, 2013, the Hollywood Reporter reported that Warner Bros. will undertake a remake of Ice Station Zebra, with Christopher McQuarrie signed to direct and write the screenplay for the film. 

== References   ==
 

== Further reading ==
* Lawrence H. Suid. Sailing the Silver Screen: Hollywood and the U.S. Navy (Annapolis, MD, USA, Naval Institute Press, 1996) ISBN 1-55750-787-2

== External links ==
 
*  
*  
*  
*  
*  
*  

 
 
 

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 