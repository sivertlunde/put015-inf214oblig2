The Fair Barbarian
{{Infobox film
| name           = The Fair Barbarian
| image          = 
| alt            = 
| caption        = 
| director       = Robert Thornby
| producer       = Jesse L. Lasky
| screenplay     = Frances Hodgson Burnett Edith M. Kennedy 
| starring       = Vivian Martin Clarence Geldart Douglas MacLean Jane Wolfe Josephine Crowell Mae Busch
| music          = 
| cinematography = James Van Trees
| editing        = 
| studio         = Pallas Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 comedy silent film directed by Robert Thornby and written by Frances Hodgson Burnett and Edith M. Kennedy. The film stars Vivian Martin, Clarence Geldart, Douglas MacLean, Jane Wolfe, Josephine Crowell and Mae Busch. The film was released on December 17, 1917, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*Vivian Martin as Octavia Bassett
*Clarence Geldart as Martin Bassett 
*Douglas MacLean as Jack Belasys 
*Jane Wolfe as Belinda Bassett
*Josephine Crowell as Lady Theobald
*Mae Busch as Lucia
*William Hutchinson as Reverend Poppleton
*Alfred Paget as Mr. Burmistone 
*Ruth Handforth as Miss Chickie
*Elinor Hancock as Lady Barold 
*Charles K. Gerrard as Captain Francis Barold 
*Helen Jerome Eddy as Maid
*John Burton as	 Dugald Binnie
*Charles Ogle

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 

 