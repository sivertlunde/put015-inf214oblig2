My Teacher, Mr. Kim
{{Infobox film
| name           = My Teacher, Mr. Kim
| image          = My Teacher, Mr. Kim.jpg
| caption        = My Teacher, Mr. Kim movie poster
| director       = Jang Kyu-sung
| producer       = Kim Mi-hee
| writer         = Jang Kyu-sung   Lee Won-hyeong
| starring       = Cha Seung-won Byun Hee-bong Sung Ji-roo
| music          = Jo Seong-woo
| cinematography = Kim Yun-su
| editing        = Ko Im-pyo
| distributor    = Cinema Service
| released       =  
| runtime        = 117 minutes
| country        = South Korea
| language       = Korean
| budget         =
| film name      = {{Film name
| hangul         = 선생 김봉두
| hanja          =   김봉두
| rr             = Seonsaeng Kim Bong-du
| mr             = Sŏnsaeng Kim Pong-tu}}
}} 2003 South Korean comedy-drama film about a mans transformation from a flawed scoundrel into a conscientious teacher when he gets transferred to a rural village school. 

==Plot== room salons, and he actively encourages and coaxes parents to give him bribe money in exchange for his favoring their children. But one day, he finally gets caught in the act, and becomes the target of parent complaints.
 Gangwon Province. In the countryside, cell phones are useless and even buying cigarettes at the nearest corner store is out of the question. There are only five students in the whole school, and hes also discontented with the extremely naive villagers who offer him all sorts of vegetables and fruits instead of money. To make matters worse, a grumpy old man named Mr. Choi blackmails him into teaching him the Korean alphabet.

Driven to near insanity by boredom and peace, Mr. Kim maps out a plan to transfer all of his students to Seoul, thereby closing down the rural school. For starters, Mr. Kim begins an after-school program to focus on developing each students special talents so that theyll want to get a better education in Seoul. Contrary to his bad intentions, this results in a happy, supportive environment for the children and the village community, such that the school officials reconsider their policy of closing down the school because of Mr. Kims "wonderful devotion" to the kids.

But when a businessman unexpectedly appears, saying hed like to turn the school into a survival game site, Mr. Kim becomes tempted by money once again.

==Cast==
*Cha Seung-won ... Kim Bong-doo
*Byun Hee-bong ... Mr. Choi
*Sung Ji-roo ... Chun-shik
*Lee Jae-eung Kim Eung-soo ... Nam-oks father
*Lee Jae-gu
*Ku Bon-rim
*Jung Jin-gak ... Seoul principal
*Park Soo-il ... leisure businessman
*Seo Hee-seung ... Gangwon principal

==References==
 

==External links==
*    
*  
*  
*  

 
 
 
 
 
 
 
 