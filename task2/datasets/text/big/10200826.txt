Dances of the Kwakiutl
{{Infobox film
| name           = Dances of the Kwakiutl
| image          = 
| image_size     =
| caption        = 
| director       = William Heick Robert Gardner
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = Documentary Educational Resources
| released       = 1951
| runtime        = 9 min.
| country        = U.S.A. English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
Dances of the Kwakiutl is a 1951 film directed by  , featuring a performance by Kwakiutl people of their secretive Hamatsa ceremony.

==See also==
*In the Land of the Head Hunters, a 1914 silent film

==References==
* {{Cite web|url=http://der.org/films/dances-of-the-kwakiutl.html
|accessdate=2007-03-22
|title=Dances of the Kwakiutl
|work=www.der.org}}
* Ira Jacknis. Visualizing Kwakwakawakw Tradition: The Films of William Heick, 1951-1963, BC STUDIES: The British Columbia Quarterly - A Special Double Issue-Number 125 & 126, 2000. ISBN 0-00-005294-9

 
 
 
 
 
 
 
 
 
 
 


 