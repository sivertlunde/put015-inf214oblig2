Sirius (film)
{{Infobox film
| name = Sirius
| image = Sirius_documentary_poster.jpg
| alt = 
| caption = Theatrical release poster
| director = Amardeep Kaleka
| producer = Steven M. Greer Rizwan Virk Amar Kaleka J. D. Seraphine Xaypani Baccam Jared Bonshire Benjamin Gerry
| narrator = Thomas Jane
| cinematography = Ramy Romany
| music = Todd Richards Peter Kater
| editing = Laurie Knapp
| studio = Neverending Light Productions
| released =  
| runtime = 110 minutes
| country = United States
| language = English
}} Amardeep Kaleka, Steven M. Greers book Hidden Truth, Forbidden Knowledge.   Partially funded by a successful Kickstarter campaign,  the movie is narrated by Thomas Jane and follows Greers efforts to reveal what he claims is information about top secret energy projects and propulsion techniques. 
 Ata that was found in the Atacama desert in northern Chile in 2003.  The film premiered on April 22, 2013 in Los Angeles, California as well as online.   

==Synopsis==
Sirius is divided into three sections. The first section covers the filmmakers attempt to offer evidence that they believe shows proof for the existence of extraterrestrial intelligence. This portion of the film features government documents, government and military witness testimony, and audio and visual evidence. The second section contains interviews of people who claim that there are secret technologies being suppressed for development or use. Some of the interviewers also claim that they can derive energy from what they call a "Vacuum state|zero-point field", while opining that the technology has been suppressed by various groups. Sirius also covers claims that man-made anti-gravity vehicles have been used for nearly 50 years, some of which have been used to perpetuate ET abduction hoaxes in order to maintain control over the population. The final section explores how the filmmakers believe peaceful contact with extraterrestrials is being made through the CE-5 protocols developed by Greer.

==Reception==
Critical reception was mostly negative.   Hollywood Reporter criticized the film, saying that "Kaleka is so undiscerning in his choice of interviewees and so scattershot in his presentation that potentially credible subjects look nutso by association, and no sighting anecdote generates enough narrative momentum to dent a viewers skepticism".  The Village Voice also gave a predominantly negative review, expressing frustration over the film not living up to its potential. 

==References==
 

==External links==
*  
*  

 
 
 
 
 