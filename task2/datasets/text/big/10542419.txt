Love and Duty (1931 film)
{{Infobox film
| image          = LoveandDuty.jpg
| caption        =
| name           = Love and Duty
| writer         = Zhu Shilin Ho Ro-se (novel)
| starring       = Ruan Lingyu Jin Yan
| director       = Bu Wancang
| cinematography = Huang Shaofen
| producer       = Li Minwei
| studio         = Lianhua Film Company
| runtime        = 152 minutes
| released       =  
| country        = China
| language       = Silent film Written Chinese intertitles
| budget         =
| film name = {{Film name| jianti         = 恋爱与义务
| fanti          = 戀愛與義務
| pinyin         = Liànài yǔ yìwù}}
}}
Love and Duty is a 1931 Chinese silent film, directed by Bu Wancang and starring Ruan Lingyu and Jin Yan. 

==Production history== Polish expatriate, S. Rosen-hoa ("Ho Ro-se"), who had married a Chinese engineer, Love and Duty became one of the first films produced by the leftist Lianhua Film Company. 

The film was very popular for its day, in no small part due to the pairing of Ruan, who was already a darling of the Shanghai film industry, and Jin Yan, a Korean people|Korean-born actor who was one of the major leading men in early Chinese cinema. 

==Plot==
The film tells the story of Yang Naifan (Ruan Lingyu) who runs from her arranged marriage to be with her true love, Li Zuyi (Jin Yan). The film details the poverty she must endure for breaking with tradition.

==Rediscovery== 2K digital restoration under Italys LImmagine Ritrovata, after which it was screened at the Shanghai Film Festival that same year.

==Remakes==
Love and Duty has been remade twice, in 1938 and 1955.  The first was from the wartime Shanghai "Orphan Island" studio Xinhua Film Company, again directed by Bu Wancang, with Jin Yan reprising his earlier role and Yuan Meiyun in the role originally created by Ruan Lingyu.  The second remake was by the Hong Kong Shaw Brothers Studio.  Both remakes were Mandarin dialect sound films.

==References==
 

==See also==
*List of rediscovered films

==External links==
*  at the Melbourne International Film Festival
* 
*  profiled at the XVth EACS-Conference from the University of Heidelberg
* 

 
 
 
 
 
 
 

 