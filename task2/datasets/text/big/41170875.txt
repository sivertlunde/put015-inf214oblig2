Woe to the Young
{{Infobox film
| name           = Woe to the Young Αλίμονο στους νέους
| image          = 
| image_size     = 
| caption        = 
| director       = Alekos Sakellarios
| producer       = 
| writer         = Alekos Sakellarios Christos Giannkopoulos
| starring       = Dimitris Horn Maro Kontou Smaro Stefanidou Andreas Douzos Giorgos Velentzas
| music          = Manos Hatzidakis
| cinematography = 
| editing        = 
| distributor    = 
| released       = October 30, 1961 
| runtime        = 89 mins
| country        = Greece Greek
}}

Woe to the Young (Greek: Αλίμονο στους νέους) is a 1961 film. Is based on the myth of Faust who sold his soul to the Devil. A rich old man (Dimitris Horn), who wants to be young again, aiming to marry a young girl, makes a deal with the Devil. He becomes young but with no wealth anymore. He spends too fast the little money he had and finally the girl, after her mothers pressure, rejects him to marry another rich old man of the neighborhood.

==Cast==

*Dimitris Horn
*Maro Kontou
*Smaro Stefanidou
*Andreas Douzos 
*Giorgos Velentzas

==External links==
* 

 
 
 

 