Born Invincible
{{Infobox Film
| name           = Born Invincible
| image          = Born Invincible s.jpg
| image_size     = 
| caption        = 
| director       = Joseph Kuo Nam Hung
| producer       = Joseph Kuo Nam Hung
| writer         = Joseph Kuo Nam Hung   Yiu Hing Hong
| narrator       = 
| starring       = Carter Wong   Jack Long   Mark Long   Lo Lieh   Nancy Yen
| music          = 
| cinematography = 
| editing        = 
| distributor    = Ocean Shore
| released       = 
| runtime        = 83 minutes Taiwan
| language       = Mandarin Chinese
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1978 Cinema Taiwanese kung fu film directed by Joseph Kuo, with action choreography by Yuen Woo-ping, and starring Carter Wong Ka Tat, Jack Long and Leih Lo.  Currently, only the English language dubbed version is widely available on UK DVD (pan and scan) format. The original Mandarin version was released onto VHS format in the 1980s and is now out of print. A rare Japanese language dubbed version (ドラゴン太極拳) can be found online.

== Plot ==
 
Born Invincible opens with a montage showing a youth learning the techniques of Tai chi. A narration explains the rigors of  Tai chi chuan|Tai chi, and the effects it has on the individuals who learn it. When a person masters Tai chi, their body becomes impregnable to any weapon. But a side effect of mastering Tai chi is that the person’s hair turns white by age thirty, and their voices attain a high tone. One such Tai chi master is the villainous Chief Chin (Carter Wong) of the Chin Yin Chi clan. While students of the Lei Ping Kung Fu school go through an early morning training session, they witness two thugs from the Chin Yin Chi clan chase an old man and his daughter over the hill. The thugs start beating the helpless old man, much to the chagrin of his horrified daughter. The lead student, Ming Tu (Jack Long) interrupts the attack and warns the thugs to stop. Some other students get involved along with Ming Tu, and the thugs use some weapons on the students. Ming Tu manages to fight off the duo, but not before one of his fellow students is mortally wounded. The two thugs warn the students that the Chin Yin Chi clan will be back in force to kill them. Ming Tu escorts the old man and his daughter back to the Lei Ping school, where the wise master takes them under his protection.

The two leaders of the Chin Yin Chi clan, Chief Chin and Chin Pa (Lo Lieh) show up at the Lei Ping school, demanding that the master hand over the old man. He refuses and one of his top pupils offers to go and fight for the schools honor. Despite his magnificent martial arts ability, he is no match against the Chin Yin Chi’s top dogs. Realizing the serious predicament they are in, the master himself goes to face the aggressors. Before doing so, he instructs his students to carry on the school’s name, and he selects Ming Tu as his successor, should he not survive the duel. The master and the Chin Yin Chi fighters engage in mortal combat, and soon the wise one is overpowered and killed. The old man, having taken an oath of peace, picks up a sword for the first time in twenty years and leaps into battle. His skills prove very admirable, but he too is taken down by the powerful duo. The students are horrified, and realize they cannot stand up to the invulnerable Chief Chin. The old man’s daughter is shattered. Ming Tu assumes leadership of the school and promises his students that the school will continue as the master intended, with him in charge. He also promises them that together they will take revenge against the Chin Yin Chi clan. Ming Tu comes up with a plan to take Chin Pa out of the equation, since he is not invulnerable like his cohort. So he walks into the Chin Yin Chi shrine and challenges the Chin Pa to a duel. Chin Pa’s weapon of choice is a loaded steel baton with a blade that wounds his opponents. Ming Tu falls victim to this weapon, but escapes from the shrine.

This defeat only makes him more determined to beat Chin Pa. Ming Tu begins undergoing even more rigorous training to prepare for the next confrontation. He thinks up a way to counter Chin Pa’s baton—by covering his sword in oil so his opponent can’t get a grip on it. Sure enough, this works, and Ming Tu is finally able to kill Chin Pa. Chief Chin discovers the death of his comrade and goes looking for Ming Tu. Ming seeks additional training before he can face Chief Chin, but is forced into a confrontation with him. Ming cannot defeat the Tai chi master, because he has no weak spot. Eventually, Chief Chin beats Ming unmercifully. The next senior pupil of the Wei Ping school, Sa Chien (Mark Long) takes charge since Ming Tu is out of action. He trains hard to beat Chief Chin, but when the inevitable confrontation takes place, Sa Chien cannot get the job done but barely escapes. The master’s daughter (Nancy Yen Nan Hsi) meets an old nun who tells her how to find Chief Chins weak point "when he is not himself". With the aid of the revenge-driven daughter, Sa Chien discovers that Chief Chin has one vulnerable spot after all, but getting him to expose it is near impossible unless he can get him to act other than "himself".

== External links ==
*   at Hong Kong Cinemagic
*  
*  
*   at AmebaVision (Japanese)

 
 
 
 