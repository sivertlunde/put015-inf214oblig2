The Night of January 16th (film)
{{Infobox film
| name           = The Night of January 16th
| image          = NightOfJanuary16thmovie.jpg
| alt            = Color movie poster for The Night of January 16th. In the top half, a woman drags a mans body by his feet. In the bottom half, a man and woman look up apprehensively.
| caption        = Movie poster
| film name      =  William Clemens
| producer       = 
| writer         = 
| screenplay     = {{Plainlist|
*Delmer Daves
*Robert Pirosh
*Eve Greene
}}
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist| Robert Preston
*Ellen Drew
*Nils Asther
}}
| music          = Gerard Carbonara
| cinematography = John J. Mescall
| editing        = Ellsworth Hoagland
| studio         = Paramount Pictures
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 William Clemens, same name by Ayn Rand. The story follows Steve Van Ruyle and Kit Lane as they investigate the apparent murder of Lanes boss, in an attempt to clear her as a suspect.

==Plot== Robert Preston) Paul Stanton) doesnt agree, and Lane goes to trial. 

Van Ruyle attempts to prove Lanes innocence with fake evidence, but his ruse is discovered. The two flee with evidence from Faulkners apartment, which they use to track down the mysterious Haraba. They trace him to a hotel in Havana, Cuba, where they discover that "Haraba" is a pseudonym being used by Faulkner himself, who has faked his own death. When Faulkner takes Lane captive, Van Ruyle rushes with police to Faulkners room to rescue her. Faulkner is arrested, and Van Ruyle and Lane decide to get married.

==Cast== Robert Preston as Steve Van Ruyle 
* Ellen Drew as Kit Lane
* Nils Asther as Bjorn Faulkner
* Clarence Kolb as Tilton
* Willard Robertson as Inspector Donegan
* Cecil Kellaway as Oscar, the Drunk Donald Douglas as Attorney Polk Paul Stanton as the District Attorney
* Margaret Hayes as Nancy Wakefield

==Production== Al Woods, who was producing the play on Broadway, considered making a movie version through a production company of his own.  Instead, in 1938 RKO Pictures bought the rights for $10,000, a fee split between Woods and Rand. RKO looked at Claudette Colbert and Lucille Ball as possibilities to star, but they also gave up on the adaptation. The rights were resold to Paramount Pictures in July 1939 for $35,000.       

Rand did not participate in the production at Paramount. Three other writers (Delmer Daves, Robert Pirosh, and Eve Greene) were brought in to prepare a new screenplay.     Paramount planned for the film to star Barbara Stanwyck and Don Ameche, who was to be loaned out by 20th Century Fox, where he was on contract. Ameche refused to take the part, which delayed the start of the production, causing Stanwyck to drop out also. As a result, Ameche was suspended for two weeks by 20th Century Fox and sued by Paramount for $170,000 in damages. Paramount considered Paulette Goddard and Ray Milland for the roles before finally casting Drew and Asther.   

The production used the working titles Private Secretary and Secrets of a Secretary, but the movie was released in 1941 as The Night of January 16th, following the title of the play.  The play took place entirely in a courtroom, and its best-known feature was that it used a jury selected from members of the audience, who would decide the defendants guilt or innocence at the end. This feature was impossible to reproduce in a movie, so the new screenplay altered the plot significantly, focusing on Steve Van Ruyle, a character that did not exist in the play. Unlike the play where Faulkner is already dead, in the movie he appears as a living character who is then apparently murdered. The name of the prime suspect, Faulkners assistant, was changed from Karen Andre to Kit Lane, and the action is not focused on the courtroom.    Rand claimed only a single line from her original dialog appeared in the movie, which she dismissed as "cheap, trashy vulgarity". 

==Reception==
The film received little attention when it was released, and most of the reviews were negative.  A review in Variety (magazine)|Variety praised Drews performance, but described the direction as "heavyhanded" and the plot as involving "an unbelievable set of coincidences". 

==See also==
*The Match King, another movie inspired by the same events as the play this movie is based on

==References==
 

===Works cited===
*  
*  
*  
*  
*  

==External links==
* 

 
 
 
 
 
 
 
 
 
 