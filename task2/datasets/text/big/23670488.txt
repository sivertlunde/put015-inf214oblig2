Marcia o Crepa
{{Infobox Film
| name = Marcia o Crepa
| image = COMMANDOfilm.JPG
| image_size =
| caption = US release film poster
| director = Frank Wisbar
| producer = Willy Zeyn
| writer = William Denby Mino Guerrini Milton Krims Giuseppe Mangione Frank Wisbar Arturo Tofanelli(story)
| narrator = Dorian Gray  
| music = Angelo Francesco Lavagnino
| cinematography = Cecilio Paniagua
| editing =Mario Serandrei
| distributor = Tempo Film (Italy) American International Pictures (USA)
| released = 1962 (Italy) 1963 (UK) 1964 (USA, France)
| runtime = 101 minutes
| country = Belgium, Spain, Italy, Germany
| language =
| budget =
| gross = 977,460 admissions (France)   at Box Office Story 
| preceded_by =
| followed_by =
}}
 1962 European (Italian, German, Spanish) co-production (filmmaking)|co-production war film about the Algerian War of Independence. It was released in 1964 in the USA by American International Pictures on a double feature with Torpedo Bay/Beta Som. 

== Cast ==
* Stewart Granger : capitaine Leblanc  Dorian Gray : Nora
* Carlos Casaravilla : Ben Bled
* Ivo Garrani : Colonel Dionne
* Alfredo Mayo : 	Mayor
* Pablito Alonso : 	Arab Kid
* Hans von Borsody : Fritz
* Maurizio Arena : 	Dolce Vita 
* Fausto Tozzi : Brascia
* Dietmar Schönherr : 	Petit Prince
* Peter Carsten : 	Barbarossa
* Leo Anchóriz : 	Garcia Riccardo Garrone : Paolo

==Plot and production== FLN guerilla Dorian Gray) and an Arab child. Their mission is a success but when their escape helicopter is shot down they have to fight their way back to the French lines.

The theme music Concerto Disperato by Angelo Francesco Lavagnino became a top selling instrumental in Italy performed by Nini Rosso and in the UK with a cover version by Ken Thorne reaching #4. 

In the UK this film was shown at Odeon cinemas as part of a double feature with The Day of the Triffids.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 