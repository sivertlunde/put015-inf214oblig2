Scooby-Doo! and the Reluctant Werewolf
{{Infobox television 
| name           = Scooby-Doo and the Reluctant Werewolf
| image          = DVD cover of Scooby-Doo and the Reluctant Werewolf.jpg
| image_size     = 
| caption        = Australian DVD Cover Horror Science fiction
| distributor    = 
| creator        =  Ray Patterson
| executive_producer       = William Hanna Joseph Barbera
| producer       = Berny Wolf
| writer         = Mark Bernay Jim Ryan
| story          = 
| based on       =  
| narrator       = 
| voices         = Don Messick Casey Kasem Hamilton Camp B. J. Ward (actress)|B.J. Ward Rob Paulsen Frank Welker Alan Oppenheimer Pat Musick Ed Gilbert Mimi Seton Jim Cummings Joan Gerber Brian Stokes Mitchell
| composer       = Sven Libaek
| cinematography = 
| editor         = Mark Bernay
| company        = Hanna-Barbera Productions
| country        = United States English
| Syndicated
| first_aired    = October 1, 1988
| runtime        = 90 minutes
| website        = 
}}
 animated comedy movie for television produced by Hanna-Barbera. It was the final entry to air as part of the Hanna-Barbera Superstars 10 series of TV movies, and also marked Scrappy-Doos last appearance in a Scooby-Doo production until the first live-action Scooby-Doo (2002 film)|Scooby-Doo movie in 2002.

==Plot==
Every year, all of the classic Hollywood monsters (consisting of Frankensteins monster, his wife Bride of Frankenstein|Repulsa, a Imhotep|Mummy, the Witch Sisters, Bone Jangles the Skeleton (undead)|Skeleton, Strange Case of Dr Jekyll and Mr Hyde|Dr. Jekyll/Mr. Snyde, Swamp Thing, and Dragonfly) gather at Count Draculas castle in Transylvania for the "Monster Road Rally", an ultimate road race similar to Wacky Races, awarding the winner with the "Monster of the Year" award as well as many other prizes only monsters would enjoy (and one prize — a trip to Hawaii — that none of the monsters want). This year, however, Dracula receives a postcard from the Wolfman stating that he has retired to Florida and will not be participating. 

Lacking one of the most well known monsters, Dracula fears they will have to cancel the race. Luckily, Draculas minion Wolfgang notifies him there is another option, namely, to create a new werewolf. After searching an old book for information, it is revealed that every five centuries, the full moon comes into a perfect position to transform a human into a werewolf, on three nights in a row that begin the following night. The one next in line to become the next werewolf is revealed to be is none other than Shaggy Rogers, who recently demonstrated his skills on the racetrack by winning a funny car race with the help of his pet dogs that serve as his pit crew, a talking Great Dane named Scooby-Doo, and Scoobys young nephew Scrappy.
 America to transform Shaggy into a werewolf, and bring him back to his castle. The first night, they attempt to cut a hole in the roof above Shaggys bedroom to let the moonlight shine on him, but Scooby learns of their plan and rescues Shaggy just in time, but fails to convince Shaggy and Scrappy of the Hunch Bunchs presence. The second night, they go after Shaggy while at a supermarket, but they again miss their window thanks to Scooby. On the final night, while the trio is at a drive-in movie, along with Shaggys girlfriend Googie, the Hunch Bunch manage to transform Shaggy by exposing him to moonlight by dropping the sunroof of his customized race car. 

However, a snafu enters their plan when they learn that Shaggy has the hiccups, and changes between human and werewolf with each hiccup. Googie, failing to see him as a werewolf, sends him to the snack bar for something to cure his hiccups, and he attracts horror from the other movie watchers along the way. Scooby, hearing them speaking of a werewolf loose in the theater, hides in a nearby car. The Hunch Bunch attempts to grab Shaggy, who flees, and is then forced to flee from the crowd when they see him as a werewolf. Upon meeting Scooby and seeing his reflection, Shaggy flees the drive-in with his car, Scooby, Scrappy, and Googie in tow, escaping his pursuers with the cars customization. The Hunch Bunch then knocks the group out with moon dust from their "bat-copter" and flies back to Transylvania, car in tow.

Upon reviving the group and quashing their hopes that the current situation is a nightmare, Dracula informs Shaggy that he was turned into a werewolf in order to fill the missing slot in his monster road rally. Shaggy, having no desire to be a werewolf, is displeased with his current situation, and refuses. Dracula tries all manner of coercion: speaking of the pre-race party and all its rewards, showing the awards for the race, imprisonment in one of the guest bedrooms, and even trapping him in a room with a needle wall. Ultimately, however, Shaggy still refuses, forcing Dracula to strike a bargain with him: if he agrees to drive in the race, and wins, Dracula will change him back to a human, and allow him and his friends to leave. Shaggy accepts the bargain.

They are given good lodgings and treated like guests in the castle, allowed all the food they wish for breakfast (though the food in question is flavored with "scream beans", much to their displeasure). Dracula then shows them the trail that Shaggy will have to follow for the race, and consents to allowing them to navigate the track in their own race car, with the Werewolf Wagon currently undergoing maintenance for Shaggy. Dracula attempts several rotten tricks on the track out of sadistic pleasure, but regardless, Shaggy completes the course expertly, creating anxiety for the vampire that he may lose his werewolf. He subsequently changes the racetrack, sabotages the Werewolf Wagon, and has the Hunch Bunch deprive him of sleep. 

Googies help revitalizes Shaggy, and upon beginning the race with Scooby in the passenger seat, he has little difficulty repairing the Wagon. The Hunch Bunch, a couple of the racers, and Dracula himself try their best to sabotage him, but thanks to Googie and Scrappy, who follow along in their own car as their pit crew, they end up doing more harm to themselves than him. At one point, the Hunch Bunch tries to dispose of the duo, forcing Shaggy to save them, but even this only serves as a minor inconvenience, and Shaggy keeps first place for most of the race. Finally, losing his temper, Dracula unleashes his trump card, a massive beast named Ghengis Kong who manages to halt their progress. As the other racers near the finish line, however, Googie and Scrappy come up once more and rescue Shaggy and Scooby, then both pairs work together to make the monster fall down on the other cars, leaving an easy path to victory for Shaggy.

Dracula, furious, refuses to revert the spell, having had his fangs crossed when they made the agreement. The heroes steal Draculas spell book and make their getaway. Dracula chases after them in his weaponized car, and then his plane after the car is destroyed due to Shaggys cleverness. The four only barely manage to dodge Draculas powerful gadgets, and right before Dracula gets the best of them, a thunderstorm ensues and Draculas plane is struck by lightning, sending him plummeting into the ocean below where he is chased off by a shark.

In the end, Googie uses the book to change Shaggy back to normal. That night, the gang all sit down to watch another horror movie, glad that their problems are over, but in the final scene, Dracula, Brunch and Crunch sneak up behind their window, and say "Were baaaack!".

==Voice cast==
*Don Messick as Scooby-Doo (character)|Scooby-Doo, Scrappy-Doo Shaggy 
*B. J. Ward (actress)|B.J. Ward as Googie, Repulsa
*Hamilton Camp as Dracula
*Jim Cummings as Frankenstein, Skull Head, Genghis Kong
*Joan Gerber as Dreadonia, Woman at Store
*Ed Gilbert as Dr. Jekyll and Mr. Snyde Brian Mitchell as Bonejangles
*Pat Musick as Vanna Pira
*Alan Oppenheimer as The Mummy
*Rob Paulsen as Brunch
*Frank Welker as Crunch

==Home Media releases==
Warner Home Video released the movie on VHS and DVD in Region 1 on March 5, 2002.  In 2005, Warner Home Video re-released the DVD again.

==References==
 

==External links==
 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 