Get a Horse!
 
{{Infobox Hollywood cartoon cartoon_name = Get a Horse! series = Mickey Mouse image = Get a Horse! poster.jpg caption = Poster director = Lauren MacMullan  story_artist = Paul Briggs Nancy Kruse Lauren MacMullan Raymond S. Persi animator = Eric Goldberg (lead) Adam Green (lead) voice_actor = Walt Disney Marcellite Garner Russi Taylor Billy Bletcher Will Ryan musician =Mark Watters layout_artist = Alfred "Tops" Cruz Jean-Christophe Poulan background_artist = producer = Dorothy McKim  studio = Walt Disney Animation Studios Walt Disney Pictures distributor = Walt Disney Studios Motion Pictures release_date =   color_process = Digital film Color Black-and-white country      = United States runtime = 6 minutes  movie_language = English
}} 3D animation|animated CGI animation, Mickey Mouse cartoons, and features archival recordings of Walt Disney as the voice of Mickey Mouse.       It is the first original Mickey Mouse theatrical animated short since Runaway Brain (1995), and the first appearance of Oswald the Lucky Rabbit in a Disney animated production in 85 years.

== Plot == candlestick phone and Horace sprays foam from a fire extinguisher into the smartphone and out from Petes phone.

Petes car then lands in a frozen lake and the screen fills with water, giving Mickey the idea to poke a hole in the screen with his tail and let the water leak out, causing Pete, Minnie and the other cartoon animals to flood out onto the stage. Mickey and Minnies reunion is short-lived, however, as Pete gives chase to the characters in and out of the screen until he snatches Minnie again, punches Mickey onto a support beam and nails the screen shut. Horace and the others decide to swing from the beam and try to break though the screen like a wrecking ball, but the plan only manages to flip the screen upside-down, causing Pete to fall from the ground.

Mickey and the others flips it again and Pete lands on the ground, his car crashing down on him. Getting an idea, Minnie encourages Mickey to flip the screen again, this time having Pete land on a cactus, which sets off a chain of events. First, Pete gets electrocuted on some telephone cables, then he has his face get hit by all the steps on a ladder, lands face first in the mud, and gets his rear end poked on a pitchfork, and falls onto a seesaw, where he gets hit on the head by numerous tools. Then one of the tools, a sledgehammer, rams the pitchfork into his deeper into his rear end. Finally, the sledgehammer falls on the opposite side of the seesaw, where Pete is launched and lands face first in his jalopy.

Horace, Mickey, and Minnie begin to laugh hysterically at Petes pain. Suddenly, Horaces hand gets stuck behind the screen due to him pounding the screen. Mickey tries to pull him out, but only succeeds by spinning the screen horizontally like a Flip book|flip-book. To Mickeys realization, it rewinded the scene. Seeing this as an opportunity, Mickey and Horace begin spinning the screen around like a flip-book until Pete is completely knocked out.
 Oswald briefly iris closes, Pete, who has woken up by now, tries to get back in through the screen, but gets his head (and half his body) stuck. Seconds later, the flap on Petes pants open up to reveal the words "THE END" and Pete bellows "Hey!!", as the screen cuts to the credits. After the credits, the Walt Disney Pictures logo is in black-and-white, and Clarabelle jumps over the castle, making the arch with her milk.

==Cast==
* Walt Disney as Mickey Mouse 
* Marcellite Garner and Russi Taylor    as Minnie Mouse Peg Leg Pete 
* Raymond S. Persi as Car Horn 

==Production== Eric Goldberg, and its computer animation by Adam Green.    To achieve the 1928 look, aging and blur filters were added to the image, while for the CG part, they created new models, faithful to the character designs of 1928.    Originally temporary, the production team incorporated archival recordings of Walt Disneys Mickey Mouse voice from 1928 to 1947, and spliced it into the characters dialogue. 

==Release==
Get a Horse! premiered June 11, 2013 at the Annecy International Animated Film Festival in Annecy, France.  It made its United States premiere on August 9, 2013, at the D23 Expo in Anaheim, California,  and theatrically accompanied Walt Disney Animation Studios Frozen (2013 film)|Frozen, which was released on November 27, 2013.    The short made its home debut on the Blu-ray and DVD release of Frozen on March 18, 2014. 

===Critical response===
Todd McCarthy of The Hollywood Reporter lauded the short film as "one of the wittiest and most inventive animated shorts in a long time". He particularly points out that the film "begins as an early black-and-white Mickey Mouse cartoon but then bursts its boundaries into color and 3D in marvelously antic ways that call to mind the stepping-off-the-screen techniques of Buster Keatons Sherlock, Jr. and Woody Allens The Purple Rose of Cairo. Its a total winner."  Scott Foundas of Variety agreed, labeling the film as "utterly dazzling".  Drew McWeeny of HitFix lauded it as "the perfect companion piece" and "enormously entertaining". He continues on that "Filmmaker Lauren MacMullan perfectly nails the look and feel of the early days of the Disney studio, and it is the first time I have ever laughed out loud at Mickey Mouse. Its an inventive and technically precise short, and it also celebrates and deconstructs Disneys animated history in a very fun way." 

===Accolades===
{| class="wikitable" width="95%"
! colspan="5" style="background: LightSteelBlue;" | Awards
|-
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Result
|-
| Academy Awards  March 2, 2014 Best Animated Short Film
| Lauren MacMullan & Dorothy McKim
|  
|-
| Annie Award 
| February 1, 2014 Best Animated Short Subject
| Lauren MacMullan
|  
|-
| San Diego Film Critics Society 
| December 11, 2013
| Best Animated Film
|
|  
|}

==References==
 

==External links==
*   at Walt Disney Animation Studios
*   at Disney.com
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 