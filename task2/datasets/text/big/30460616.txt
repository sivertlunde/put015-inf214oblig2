Shattered Image
 
{{Infobox film
| name = Shattered Image
| image_size =
| image	= Shattered Image FilmPoster.jpeg
| caption =
| director = Raoul Ruiz
| producer = Seven Arts Productions
| writer = Duane Poole
| starring = William Baldwin Anne Parillaud Lisanne Falk
| cinematography = Robby Müller
| music = Jorge Arriagada
| distributor = Lions Gate Films
| released =  
| runtime = 103 minutes
| language = English
| country = United States
| gross = $102,523 (24 Screens)
}} thriller drama film written by Duane Poole and directed by Raoul Ruiz. It starred  William Baldwin, Anne Parillaud and Lisanne Falk.

==Plot==
Confusing realities surface in this paranoid film dealing with the fragile nature of a young woman (Anne Parillaud) recovering from rape and an apparent attempted suicide. In one reality, she is a killer destroyer of men. In another she is the new wife on a Jamaican honeymoon with her new husband (William Baldwin), who is trying to help her recover. Which is real is the question as the story unfolds.

==Cast==
*William Baldwin as Brian
*Anne Parillaud as Jessie Markham
*Lisanne Falk as Paula/Laura Graham Greene as Detective
*Bulle Ogier as Mrs. Ford
* Billy Wilmott as Lamond
* ONeil Peart as Simon
* Leonie Forbes as Isabel

==Reception==
The movie received negative reviews.    

==References==
 

==External links==
* 

 

 
 
 
 


 