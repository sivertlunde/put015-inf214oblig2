Her Battle for Existence
 
 
{{Infobox film
| name           = Her Battle for Existence
| image          = 
| caption        =
| director       =
| producer       = Thanhouser Company
| writer         =
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States English inter-titles
}} silent short short drama produced by the Thanhouser Company. The fictional drama follows Susan Dale, a young woman used to luxury. She chooses between two suitors, but her fiancé breaks the engagement off after Susans father loses his money and life. Susan now tries to support herself, but fails in the role and decides to kill herself. Her other suitor rushes in and stops her suicide and they get married. Little is known about the production of the film, but it was released as a split reel with Sand Mans Cure on April 22, 1910. The film is presumed lost film|lost.

== Plot ==
An official synopsis was published in the The Moving Picture World. It states, "Susan Dale has been brought up in luxury. She has two suitors, Will Emory and Jim Briggs. She chooses Will, and at the opening of the picture informs Jim of her choice. But a change comes to the fortunes of Susans father: an unlucky speculation claims his wealth and his life. When Will discovers that his fiancé is penniless his love cools; he breaks the engagement and goes to Europe to avoid the girl. Susan, friendless, starts out to earn her own living. She applies for a job in a lawyers office, but leaves when the man insults her. She secures a place in a department store, but is discharged because, fatigued after an arduous day at the counter, she took a seat while on duty. She applies for work in a factory, but is rejected, owing to lack of experience. Finally, in desperation, she takes a place as maid in the home of Mrs. Gray. There Burg, the offensive lawyer, meets her, and in a spirit of revenge, denounces her as a thief. The mistress discharges her and Susan decides to end her life. Fortunately for Susan, Burg meets Jim Briggs, the rejected suitor, on the street and laughingly tells him how he brought about Susans discharge. Jim knocks the wretch down and rushes to Mrs. Grays residence. He arrives in time to dissuade Susan from her purpose, and all ends happily to the peal of wedding chimes."   

== Production == Romeo and Juliet. Lloyd B. Carleton was the stage name of Carleton B. Little, a director who would stay with the Thanhouser Company for a short time, moving to Biograph Company by the summer of 1910.    Film historian Q. David Bowers does not attribute either as the director for this particular production nor does Bowers credit a cameraman.  Blair Smith was the first cameraman of the Thanhouser company, but he was soon joined by Carl Louis Gregory who had years of experience as a still and motion picture photographer. The role of the cameraman was uncredited in 1910 productions.    Bowers does not indicate any cast roles or provides any further information into the production of this work. 

== Release and reception ==
The film was released on April 22, 1910. The production was a split reel, containing both Her Battle for Existence and Sand Mans Cure, with an estimated length of 1000 feet.  The American Film Institute refers more specifically to the entire reel being 998 feet in length, but does not distinguish between the lengths of the productions.  A Thanhouser Filmography Analysis, provided by Thanhouser Company Film Preservation, lists the film as comprising three quarters of a reel - approximately 750 feet. The reasoning is not provided, but the previous release and the next to follow were split reels of similar lengths.  Billboard (magazine)|Billboard would not identify the film as a split release or give the length for this and the other Thanhouser split reel that followed. 
The film had advertisements for theaters across the United States, locations included Missouri,  Arizona,  New York,  Wisconsin,  Vermont,  and California. 

The film received mixed reviews by film critics. The first review of the film in the The Moving Picture World found no fault with the acting and photography, but did not believe the ending was believable. The second review was more favorable, but again noted the scenario is a cautionary one for women without aid or occupation to support themselves in life.  The New York Dramatic Mirror stated, "A number of overdrawn and inconsistent incidents in this film story do not rob it of its interesting strength. The acting is good and the scenic backgrounds appropriate."  However, the reviewer makes references to the plot that are not consistent with the official synopsis.  The film is presumed lost film|lost. 

== References ==
 

 
 
 
 
 
 
 