Gentlemen Prefer Blondes (1928 film)
 
{{Infobox film
| name           = Gentlemen Prefer Blondes
| image          = Gentleman Prefer Blondes - 1928.jpg
| caption        = 1928 lobby poster, with Ruth Taylor and Holmes Herbert. Mal St. Clair
| producer       = Adolph Zukor Jesse L. Lasky John Emerson (scenario) Anita Loos and Herman Mankiewicz (titles)
| based on       =   Ruth Taylor Alice White
| music          =
| cinematography = Harold Rosson
| editing        = Jane Loring William Shea
| distributor    = Paramount Pictures
| released       =   reels (6,871 ft)
| country        = United States
| language       = Silent film English intertitles
}} Mal St. Broadway version Gentlemen Prefer Gentlemen Prefer Blondes with Jane Russell as Dorothy Shaw and Marilyn Monroe as Lorelei Lee in 1953.

==Cast== Ruth Taylor as Lorelei Lee
*Alice White as Dorothy Shaw
*Ford Sterling as Gus Eisman
*Holmes Herbert as Henry Spoffard
*Mack Swain as Sir Francis Beekman
*Emily Fitzroy as Lady Beekman
*Trixie Friganza as Mrs. Spoffard
*Blanche Friderici as Miss Chapman
*Edward Faust as Robert
*Eugene Borden as Louis
*Margaret Seddon as Loreleis Mother
*Luke Cosgrove as Loreleis Grandfather
*Chester Conklin as Judge
*Yorke Sherwood as Mr. Jennings
*Mildred Boyd as Lulu

==See also==
*List of lost films

==References==
 

==External links==
* 
* 
*  at Virtual History
* 

 
 
 

 
 
 
 
 
 
 
 
 
 


 