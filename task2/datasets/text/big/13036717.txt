Mules and Mortgages
 
{{Infobox film
| name           = Mules and Mortgages
| image          = Mules and Mortgages (1919) - Ad 1.jpg
| image size     = 
| caption        = Ad for film
| director       = J. A. Howe
| producer       = Albert E. Smith
| writer         = J. A. Howe
| narrator       = 
| starring       = Jimmy Aubrey Oliver Hardy
| music          = 
| cinematography = R. D. Armstrong
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States  Silent (English intertitles)
| budget         = 
}}
 silent comedy film featuring Oliver Hardy.

==Cast==
* Jimmy Aubrey - Jim
* Maude Emory - Girl, under the threat of eviction
* Oliver Hardy - Strongarm (as Babe Hardy)
* Snooky - Minnie, a Chimp (as Snookums the Chimpanzee)

==See also==
* List of American films of 1919
* Oliver Hardy filmography

==External links==
* 

 
 
 
 
 
 
 

 