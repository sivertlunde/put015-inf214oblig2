The Man on the Roof
{{Infobox film
| name           = The Man on the Roof
| image          = Man on the roof cover.jpg
| border         = yes
| alt            = 
| caption        = Swedish DVD cover
| director       = Bo Widerberg
| producer       = Per Berglund
| screenplay     = Bo Widerberg
| story          = 
| based on       =  
| starring       = Carl-Gustaf Lindstedt Sven Wollter Thomas Hellberg Håkan Serner
| music          =  
| cinematography = Per Källberg Odd-Geir Sæther
| editing        = Sylvia Ingemarsson Bo Widerberg
| studio         = 
| distributor    = AB Svensk Filmindustri
| released       =  
| runtime        = 110 minutes
| country        = Sweden
| language       = Swedish SEK
| gross          = 
}} police officer The French Connection.
 Best Film Best Actor (Håkan Serner).   

==Plot==
A policeman (Nyman) who is a patient at a hospital in Stockholm is brutally murdered, stabbed repeatedly with a bayonet. The investigation that follows is led by Martin Beck and Einar Rönn. It turns out that the murdered man had sadistic tendencies and was known among his colleagues for abusing his police privileges and brutalizing civilians. Although his colleagues had been aware of his behaviour, the police forces esprit de corps had suppressed complaints about him and prevented any reprisals.

The investigation proceeds, and finally Beck and his team find a trail that leads to the murderer, who turns out to be an ex-policeman named Eriksson. Erikssons wife Marja had diabetes, and one day, in need of insulin, she had fallen into a coma. She was mistaken by the police as a drunk and put in a jail cell, under the orders of Nyman, where she died. Eriksson blamed the police force for the death of his wife. Now, some years later, he has become a social misfit and the authorities are in the process of removing his daughter Malin from his custody. 

As Beck and his team close in on Eriksson he climbs up on the roof of the apartment building where he lives in central Stockholm, bringing with him both an automatic rifle and a sharpshooters rifle. He starts to fire at any policeman and police vehicle he can spot, picking off several policemen. When the police commissioner decides to bring in the anti-terrorist units, including two police helicopters, Eriksson shoots up one of the helicopters such that it crashes on a crowded plaza near the building where he resides. Beck tries an individual initiative, climbing to the roof on a flimsy external ladder, but is shot in the chest, although he survives. Finally, two other members of Becks team, along with another policeman and a civilian resident in the building, use explosives to gain access to the roof, and Eriksson is shot in the shoulder and arrested.

==Cast==
*Carl-Gustaf Lindstedt as Martin Beck
*Sven Wollter as Lennart Kollberg
*Thomas Hellberg as Gunvald Larsson
*Håkan Serner as Einar Rönn
*Ingvar Hirdwall as Åke Eriksson
*Birgitta Valberg as Mrs. Nyman
*Carl-Axel Heiknert as Palmon Harald Hult
*Torgny Anderberg as Malm
*Bellan Roos as Mrs. Eriksson
*Gus Dahlström as Mr. Eriksson
*Folke Hjort as Melander
*Eva Remaeus as Mrs. Kollberg
*Gunnel Wadner as Mrs. Beck

==Production==
The actor Carl-Gustaf Lindstedt was picked for the part as the policeman Beck after Wideberg had seen him with a serious face in a talk show not knowing he was on air. Earlier Lindstedt was mostly known for roles in comedy films.

Filming took place between 11 December 1975 and 30 April 1976, using a budget of 3.9 million  . Retrieved on 2009-07-21.  Bo Widerberg didnt like the fake theater blood so pigs blood was used.

==Reception== Best Foreign Language Film at the 50th Academy Awards, but was not accepted as a nominee. 

==See also==
* List of submissions to the 50th Academy Awards for Best Foreign Language Film
* List of Swedish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 