The Trial (2014 film)
{{Infobox film
| name           = The Trial
| director       = Chito S. Rono
| image          = File:The Trial Star Cinema.jpg
| caption        = Official movie poster
| writer         = Enrico C. Santos   Kriz G.Gazmen Ricardo Lee
| screenplay     = Enrico C. Santos   Kriz G.Gazmen
| producer       = Charo Santos-Concio Malou Santos  Marizel Samson Martinez
| starring       = John Lloyd Cruz   Jessy Mendiola   Enrique Gil   Richard Gomez   Gretchen Barretto
| Sound Engineer =  Addis Tabong
| music          =  Cesar Francis Concio
| editing        =  
| Design         =  Winston Acuyong
| Creative       =  Kriz G. Gazmen
| studio         = Star Cinema
| distributor    = Star Cinema
| released       =  
| country        = Philippines
| language       = Tagalog   English
| gross          = P55,961,544  
}}

The Trial is a 2014 Philippine family-drama film starring John Lloyd Cruz, Richard Gomez, Gretchen Barretto, Enrique Gil and Jessy Mendiola. The film is directed by Chito S. Roño. Also, it is produced and released by Star Cinema for its 20th Anniversary.   

==Plot==
Ronald (John Lloyd Cruz), a mentally challenged man, who was put into trial after allegedly raping his teacher Bessy (Jessy Mendiola).   As Amanda (Gretchen Barretto), Julian (Richard Gomez), and Ronald try to mount a convincing defense, they discover soon that theyre all connected in more ways than just the case.

==Cast==
* John Lloyd Cruz as Ronald Jimenez Jr.
* Richard Gomez as Julian Bien
* Gretchen Barretto as Amanda Bien
* Enrique Gil as Martin Bien
* Jessy Mendiola as Isabelle "Bessy" Buenaventura
* Sylvia Sanchez as Sampi Jimenez
* Vincent De Jesus as Ronald Jimenez Sr.
* Vivian Velez as Lallie Lapiral
* Benjamin Alves as Paco Sequia
* Isay Alvarez as Atty. Patricia Celis
* Joy Viado as Dora 
* Mon Confiado as Borta
* Nico Antonio as Pacos Friend
* Gee Canlas as Megan
* Lui Manansala as Mrs. Deputado

==Awards & Nominations==
17th Gawad PASADO Awards 2015
31st PMPC Star Awards for Movies 2015
Gawad Tanglaw Awards 2015
==See also==
 
 
* My Neighbors Wife In the Name of Love
* No Other Woman The Mistress
* A Secret Affair One More Try
 
* Seduction (2013 film)|Seduction
* The Bride and the Lover
* When the Love Is Gone Trophy Wife
* Once a Princess The Gifted
 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 

 