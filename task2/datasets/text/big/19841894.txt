Afterwards
 
 
{{Infobox film
| name           = Afterwards
| image          = Afterwards poster.jpg
| caption        = French promotional poster
| director       = Gilles Bourdos
| producer       = Olivier Delbosc Christian Gagné Christian Larouche Marc Missonnier
| screenplay     = Gilles Bourdos Michel Spinosa
| story          = Guillaume Musso
| starring       = Romain Duris John Malkovich Evangeline Lilly
| music          = Alexandre Desplat
| cinematography = Pin Bing Lee
| editing        = Valérie Deseine
| distributor    = Seville Pictures
| released       =  
| runtime        = 107 minutes
| country        = Canada France
| language       = English
| budget         = Canadian dollar|C$10 million
| gross          = 
}}
Afterwards ( ) is a 2008 English-language psychological thriller film directed by Gilles Bourdos and starring Romain Duris, John Malkovich and Evangeline Lilly. Based on Guillaume Mussos novel Et après..., the story tells of a workaholic lawyer who is told by a self-proclaimed visionary that he must try to prevent his imminent death. The film was shot in New York City, Montreal and various New Mexico locations over June–July 2007, and had a French release in January 2009.

==Plot== New York plaintiffs lawyer obsessed with work, he meets Joseph Kay (Malkovich), a doctor who claims that he can foresee other peoples deaths, and that he is a "messenger" sent to help Nathan put his lifes priorities in order. 
Nathan and his wife (Lilly) had recently lost their son to Sudden infant death syndrome|SIDS, leading to their divorce.  Once Nathan is convinced about the doctors ability, he visits his ex-wife and daughter in New Mexico.

==Cast==
* Romain Duris as Nathan Del Amico, a workaholic New York lawyer whose success has led him to neglect his wife and daughter   
* John Malkovich as Joseph Kay, a doctor who claims to be able to predict peoples deaths 
* Evangeline Lilly as Claire, Nathans recently divorced wife and childhood sweetheart    
* Reece Thompson as Jeremy 
* Sara Waisglass as Tracey, the daughter
* Sophi Knight as 7 year old Claire

==Production==
Gilles Bourdos and Michel Spinosa wrote Afterwards as a film adaptation of Guillaume Mussos French novel Et après.... The film, a Canadian dollar|C$10 million co-production between Canadas Christal Films Productions and Frances Fidélité Films, was filmed over six weeks in New York, Quebec and New Mexico.  Filming in Manhattan commenced on June 4, 2007.  Production moved to New Mexico from June 15–19, where scenes were shot at various locations in Albuquerque, Alamogordo, Jemez Springs and Tularosa.  Filming resumed in Montreal on July 7 and lasted for approximately 25 days;  the city was chosen as a filming location for its tax deduction incentives and the ease in making "Montreal look like anywhere in the world".  Though writer/director Bourdos is French, as is Mussos adapted novel and co-financier Christal Films Productions, Afterwards was shot almost entirely in English language|English. 

==Release==
The film had its world premiere on September 7, 2008 at the 2008 Toronto International Film Festival, looking for a U.S. distribution buyer. It was theatrically released by Mars Distribution on January 14, 2009 in France and Belgium,    earning United States dollar|US$159,500 from 250 screens on its opening weekend in France.  It went on to gross $1.1 million in its first week and ranked fifth in the Paris area. 

Seville Pictures has bought the rights to the films Canadian distribution from Christal Films,  which also produced the film and provided 30% of finances. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 