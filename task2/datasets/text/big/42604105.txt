Miss Bluebeard
{{infobox film
| title          = Miss Bluebeard
| image          =
| imagesize      =
| caption        =
| director       = Frank Tuttle
| producer       = Adolph Zukor Jesse Lasky
| writer         = Avery Hopwood(play:Little Miss Bluebeard)  Townsend Martin(scenario)
| starring       = Bebe Daniels
| cinematography = J. Roy Hunt
| editing        = 
| distributor    = Paramount Pictures
| released       = January 26, 1925
| runtime        = 62 minutes
| country        = USA
| language       = Silent film..(English intertitles)
}}
Miss Bluebeard is a surviving  1925 silent film comedy directed by Frank Tuttle and  starring Bebe Daniels.  It is based on a play, Little Miss Bluebeard, by Avery Hopwood. 

==Cast==
*Bebe Daniels - Colette Girard
*Robert Frazer - Larry Charters
*Kenneth MacKenna - Bob Hawley
*Raymond Griffith - The Honorable Bertie Bird
*Martha ODwyer - Lulu(*as Martha Madison)
*Diana Kane - Gloria Harding
*Lawrence DOrsay - Colonel Harding
*Florence Billings - Eva
*Ivan F. Simpson - Bounds(*as Ivan Simpson)

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 