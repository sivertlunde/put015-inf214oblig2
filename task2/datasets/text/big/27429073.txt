The Human Centipede 2 (Full Sequence)
 
 
{{Infobox film
| name           = The Human Centipede 2 (Full Sequence)
| image          = Human Centipede 2 Poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Tom Six
| producer       = {{Plainlist|
* Tom Six
* Ilona Six}}
| writer         = Tom Six
| starring       = {{Plainlist|
* Laurence R. Harvey
* Ashlynn Yennie}}
| music          = James Edward Barker
| cinematography = David Meadows
| editing        = Nigel de Hond Six Entertainment Company
| distributor    = {{Plainlist|
* Bounty Films  
* IFC Midnight     Accessed 3 October 2011. }}
| released       =     Accessed 3 October 2011. 
| runtime        = {{Plainlist|
* 88 minutes  
* 84 minutes  
* 91 minutes  }}
| country        = {{Plainlist|
* Netherlands
* United Kingdom
* United States}}
| language       = English
| budget         =
| gross          = $141,877 
}} black and white exploitation film written, directed, and co-produced by Dutch filmmaker Tom Six. The sequel to Sixs 2009 film The Human Centipede (First Sequence), the film tells the story of a mentally challenged British man who watches and becomes obsessed with the first Human Centipede film, and decides to make his own "centipede" consisting of twelve people, including Ashlynn Yennie, an actress from the first film.
 BBFC because of its "revolting" content but was eventually granted an 18 certificate after over 30 cuts were made. The film was also banned in Australia for a short period of time. It is banned in New Zealand. 

==Plot==
In the tollbooth of a multistorey carpark, Martin Lomax (Laurence R. Harvey) is watching The Human Centipede (First Sequence) on his laptop, a film he is obsessed with. Overweight, asthmatic, and mentally challenged, Martin lives with his overbearing mother (Vivien Bridson), who blames him for having his father put in prison for physically and sexually abusing Martin when he was a boy. Dr. Sebring (Bill Hutchens), Martins psychiatrist, also touches him inappropriately and prescribes him heavy medication. Martin keeps a pet centipede, which he gleefully feeds insects to.

Martin acquires a dingy warehouse after killing the lessor and begins abducting people to use for his own human centipede. His victims include: an aggressive young man (Bobby) and his girlfriend (Emma Lock), two drunk girls (Kandice Caine and Georgia Goodrick) who catch Martin masturbating with sandpaper, a man (Daniel Jude Gennis) and his pregnant wife (this couple has a toddler, whom Martin leaves in the back seat of their car uninjured), and another man. Martin also catches Dr. Sebring and a cabbie (Gabe Kerr) having sex with a prostitute (Maddi Black), and proceeds to kill Sebring before abducting the cabbie and prostitute. When Martins mother finds and destroys his scrapbook, Martin then kills her by bludgeoning her head repeatedly with his crowbar until her skull caves in. He then lures his detested neighbour (Lee Nicholas Harris) to the scene, shoots, and kidnaps him. Martins final victim is Ashlynn Yennie, the actress who played "Jenny" in the first film (now playing herself, having been lured by Martin under the pretense of a film audition with Quentin Tarantino).

With twelve victims, Martin begins assembling his "centipede". He severs the tendons in each persons knees to prevent them from fleeing and uses a hammer to knock out their teeth. He slices open the buttocks of one of his victims, causing him to bleed to death. In a quick change of plan, instead of actual surgeon tools, he uses a staple gun and duct tape to attach each persons lips to the next persons buttocks. During the assembly process, the pregnant woman is presumed dead; a grieving Martin places her in the corner. His "human centipede" is ultimately ten people long with Ashlynn in front.

After performing the crude procedure, Martin begins experimenting. Disturbed by Ashlynns screams, he tears her tongue out with pliers. He then injects each victim with a syringe of laxative, forcing each of them to explosively evacuate their bowels into the mouth of the person behind them. After pausing, he then wraps his genitals in barbed wire and rapes the woman at the end of his "centipede". As he finishes, the pregnant woman awakes and runs outside screaming, apparently in labour. She leaps into a victims car and births her child, which is clearly shown, who then gets onto the road. As the engine starts, she stomps on the accelerator, crushing the babys skull under the pedal but still manages to escape.

The neighbor rips his face from the person in front of him, separating the "centipede" into two halves. Furious that his centipede is ruined, Martin shoots all the victims, and when he runs out of ammunition, uses a knife to slit the throats of the remaining victims. As he advances on Ashlynn, he appears to give pause, and kneels in front of her. She punches him in the genitals, shoves the funnel into his rectum, and drops his pet centipede into it. In agony, Martin stabs her in the neck and staggers out. Ashlynn is seen slightly moving afterwards.

The scene cuts back to the tollbooth, with Martin watching the credits of First Sequence on his laptop, with exactly the same reaction as the initial scene. However, the toddler left in the car from the previous kidnappings can be heard crying in the background.

==Cast==
 
* Laurence R. Harvey as Martin Lomax, a short, Obesity|obese, asthmatic, and mentally challenged man in his 40s who becomes obsessed with First Sequence.    Martin does not actually have any dialogue in the film except for a few laughs and moans.   
*   film.
* Maddi Black as Candy / Human centipede No. 2, a prostitute who is kidnapped by Martin while performing oral sex on Dr. Sebring.
* Kandace Caine as Karrie / Human centipede No. 3, a woman who is captured by Martin after coming from a party drunk along with Valerie.
* Gabe Kerr as Paul / Human centipede No. 4, a cab driver that buys a prostitute for himself and Dr. Sebring, but is taken by Martin along with the prostitute.
* Lucas Hansen as Ian / Human centipede No. 5, an aggressive young man that lives in the building.
* Lee Nicholas Harris as Dick / Human centipede No. 6, the upstairs neighbor of Martin and his mother, who has threatened them with violence when the mother complains about his loud music.
* Dan Burman as Greg / Human centipede No. 7, a young man who is the first victim and is first seen gagged and unconscious in Martins van.
* Daniel Jude Gennis as Tim / Human centipede No. 8, a rich man. When he and his pregnant wife are abducted by Martin, their young child is left crying in their car for the rest of the film.
* Georgia Goodrick as Valerie / Human centipede No. 9, a woman who is captured by Martin after coming from a party drunk along with Karrie.
* Emma Lock as Kim / Human centipede No. 10: The girlfriend of Ian.
* Katherine Templar as Rachel, a pregnant woman that is abducted by Martin.
* Peter Blankenstein as Alan, a man who is taken by Martin when he complains about the ATM having no cash.
* Vivien Bridson as Mrs. Lomax, Martins mother.
* Bill Hutchens as Dr. Sebring, Martins psychiatrist.
* Peter Charlton as Jake, the lessor of the warehouse Martin uses for his "centipede".
 

==Production==
Director Tom Six stated in 2010 that he was working on a sequel to The Human Centipede (First Sequence), and a possible third film depending upon its success.    He said that the plot would follow on from the first film, but with a centipede made from 12 people as opposed to the three victims of the first film. The tag-line would be "100% medically inaccurate", in contrast to his "100% medically accurate" claim for the first film. Tom Six claimed the sequel would be much more graphic and disturbing, making the first film seem like "My Little Pony compared with part two."   
 Weekend of Horrors convention in May 2010, when Ashlynn Yennie and Akihiro Kitamura, who had starred in First Sequence hinted that their characters, despite their deaths in First Sequence, might be returning for the sequel.    Additionally, Ashley C. Williams, whose character was left alive at the end of First Sequence, stated in September 2010 that she was shooting a horror film in Britain, which led to speculation from FEARnet that she would be reprising the role of Lindsay from the first film.    In a further interview, Ashlynn Yennie confirmed Sixs claims that the sequel will contain "the blood and shit" which viewers did not see in the first film. 

Principal photography for Full Sequence began in London in June 2010 with a largely British cast.    A teaser trailer was released on 24 September, in which Six introduced Martin, a man wearing a cardboard box over his head, as the new doctor.   

According to Six, he intentionally made Full Sequence very different from First Sequence, due to two reasons. First, back when he was writing the script of First Sequence, he knew people would want more "blood and shit" than is shown, and second, the two parts reflect the two different characters: the coloured First Sequence, with a slow-moving camera, fit the story of Dr. Heiter, while Martin Lomaxs character required a "dark and dirty" film. Six shot Full Sequence in colour, but "was always thinking about black and white" and realized while editing that it was "much scarier" in black and white.    It was also Sixs idea to have little dialogue in the films second half, except for moans, screams, and whimpers.   Accessed 3 October 2011. 

==Release==

===Classification refusal in United Kingdom===
In June 2011, the British Board of Film Classification (BBFC) refused to classify The Human Centipede 2 (Full Sequence) for a direct-to-video release, effectively meaning that the film could not legally be supplied in any format in the UK.    The BBFC had given the preceding First Sequence title an 18 certificate.    The board stated that they had considered First Sequence to be "undoubtedly tasteless and disgusting",    but deemed it acceptable for release because the "centipede" was the product of a "revolting medical experiment".   They had also taken legal advice that First Sequence was not in breach of the Obscene Publications Act.   

By contrast, the BBFC report on Full Sequence stated that the films content was too extreme for an 18 certificate and was "sexually violent and potentially obscene".    The board members felt that the centipede of Full Sequence existed purely as "the object of the protagonists depraved sexual fantasy".  They criticised the film for making "little attempt to portray any of the victims... as anything other than objects to be brutalised, degraded and mutilated for the amusement and arousal of the central character, as well as for the pleasure of the audience"    and stated their opinion that the film was potentially in breach of the Obscene Publications Act.   The BBFC stated that they would not reclassify the film in future, as "no amount of cuts would allow them to give it a certificate". 

Six responded to the BBFCs decision in a statement released the next day to Empire (magazine)|Empire magazine. Six criticised the BBFC for including film spoilers in their report, and stated that the film was "...fictional. Not real. It is all make-belief (sic). It is art..." and that viewers should be able to choose for themselves whether or not they decided to view the film.    Six also referred to the BBFCs refusal to classify the film as "exceptional".    

In October 2011, the BBFC granted the film an 18 certificate after 32 compulsory cuts totalling 2 minutes and 37 seconds were made. The cuts included: Martin masturbating with sandpaper around his penis; graphic sight of a mans teeth being removed with a hammer; graphic sight of lips being stapled to naked buttocks; graphic sight of forced defaecation into and around other victims mouths; Martin with barbed wire wrapped around his penis violently raping a woman; a newborn baby being killed; and the graphic sight of injury as staples are torn away from individuals mouths and buttocks.   

===Classification refusal in Australia=== Brendan OConnor asked for a review of the rating (handled by a separate group, the Classification Review Board). On 28 November 2011, the film was reviewed and by unanimous decision of a three-person board, refused classification.  Conservative Christian groups (including Collective Shout, responsible for calling for a review of the original R18+ rating for A Serbian Film) and Family Voice Australia acclaimed the decision. 

Following the films ban on review, Australian applicant Monster Pictures announced its plans to submit a modified version for classification on 9 December 2011.  On 14 December 2011, Monster Pictures announced that a "slightly trimmed" (with 30 seconds cut   ) version of the film has been passed with an R18+ certificate in Australia.  

The edited version was released on DVD and Blu-ray Disc|Blu-ray on 23 February 2012.  

Due to the reaction by Australian film authorities, it was not submitted for classification by distributors in New Zealand and was not screened there, therefore leaving it objectionable (thus rejected and therefore completely banned). 

===United States release=== barf bags at the screening,  and stationed an ambulance outside the theater as a gimmick.  However, one audience member became so physically ill during the premiere that actual paramedics had to assist her. 
 limited theatrical midnight showings.

The film was released in an "unrated directors cut" on DVD and Blu-ray Disc|Blu-ray on 14 February 2012;  the film runs a total 91 minutes.

==Reception==

===Box office===
The Human Centipede 2 (Full Sequence) opened at 45th place with $49,456 (an average of $2,748 for the 18 theaters it premiered at). With four theaters added the next weekend, the film dropped a modest 29.9% with $34,679. Its third weekend saw a 56.2% drop despite having two more theaters added. However, in its fourth weekend, the film was pulled out of 12 theaters, causing a 61.9% drop ($5,792). In its final weekend, $2,267 was grossed, putting the movies resting spot at #95.   

The film grossed $5,824 in Iceland and $1,511 in the United Kingdom. Released in April 2012, the horror picture grossed $21,111 in Peru. At the end of its run, the film made $141,877, about half of what the previous installment grossed.

===Critical response===
The film received generally negative reviews and currently has a rating of 30% at Rotten Tomatoes based on 77 reviews, with a consensus that reads "The Human Centipede II (Full Sequence) attempts to weave in social commentary but as the movie wears on, it loses its ability to repulse and shock and ends up obnoxious and annoying."  At Metacritic it rates 17 out of 100, or "Overwhelming dislike", from 22 reviews. 

Giving the film a score of 7 out of 10, Bloody Disgusting writer Brad Miska said the film was a "brilliant response to critics of his first film. It makes a strong statement that its just a movie and that people take his work way too seriously, while also implementing a unique concept". Miska added that he "found it an intensely engaging and absolutely hilarious meta experience that gets its point across with flying colors", but was critical of the scripts lack of depth.    Entertainment Weekly writer Owen Gleiberman gave the film a B+ rating, stating that viewers "may feel gripped by the horror of what youre seeing and the terror of whats coming". Gleiberman noted how "The scatological climax would have the Marquis de Sade gagging into his popcorn."    Writing in the New York Post, V. A. Musetto gave the film 3 out of 4 and said Full Sequence "is sick, disgusting and vile. (but) Its also demonically funny, stylish and ingenious."   

Jen Yamato, writing for Movieline, criticized the films excessive gore and the way director Tom Six seemed to dislike his own audience. "Its not really a film one can or should enjoy, which is what Six seems to be telling his own audience, the fans who giggled through The Human Centipede and demanded more! Gorier! More extreme! Well, those people will get what they asked for."  Eric Kohn, writing for indieWire, criticized the excessive grotesqueness of the film as well as Sixs vanity. "Well, what if it turns someones own body against them—is that a measure of success? To some degree, yes; its designed to turn the tables on its own gore-hungry fans by depicting a fictionalized version of one of their own so revolting they think twice about their twisted tendencies. But its so indulgently perverse, and so viscerally disturbing to watch—not to mention a painfully vain exercise in self-worship—that the lesson is incredibly hard-won. Take a word of warning, if youre on the fence; you dont have to see The Human Centipede II to know you dont want to see it." 

Some reviewers found the extreme nature of the film boring. Robert Koehler, writing for Variety (magazine)|Variety, found the gore so excessive it was boring and a form of lazy filmmaking. "More boring than stomach-churning, the film nevertheless contains scattered scenes and sequences so far beyond the tolerance of the squeamish that it cant be overstated; one, detailing the violent birth and death of a baby, is here simply to shock the most jaded of the jaded," he wrote. 

Reviewer Robert Saucedo of InsidePulse.com was more generous toward the film, but found its execution lacking. "The film ... has a hint of intelligence hiding behind its beady little eyes. Smeared with blood and poo as it may be, this intelligence exposes a film that has something to say. The problem, unfortunately, is that director Tom Six is like a child – attempting to make a profound statement but unable to get it out eloquently or even in anything not resembling a whimper or a groan most of the time ... Who would have guessed? Human Centipede II is a treatise about horror fandom as delivered by a giggling, poop-infatuated toddler." 

Mark Olsen, film critic for the Los Angeles Times expressed concern over the films conclusion as well as its basic premise. The conclusion (which he admits is open to interpretation; did Lomax commit these crimes or not?) leaves the audience either believing that the film is a "cop-out repudiation of everything that has come prior" or that even more graphic torture is coming in the third film.   Accessed 7 October 2011.  Regardless, Olsen concluded that writer-director Six has left himself with no good option for the third film.

Roger Ebert, of the Chicago Sun-Times, who did not assign a star rating to the original, gave this film zero stars on review, calling it "reprehensible, dismaying, ugly, artless and an affront to any notion, however remote, of human decency."   Retrieved 29 January 2012,  He would later name it the worst movie of 2011. 

===Accolades===
{|class="wikitable" style="font-size: 90%;" border="2" cellpadding="4" background: #f9f9f9;
|- align="center"
! colspan=4 style="background:#B0C4DE;" | Awards
|- align="center"
! style="background: #CCCCCC;" | Award
! style="background: #CCCCCC;" | Category
! style="background: #CCCCCC;" | Recipient(s)
! style="background: #CCCCCC;" | Outcome
|- Fangoria Chainsaw Awards 2012  Worst Film
|The Human Centipede 2: Full Sequence
| 
|- Best Limited-Release/Direct-to-DVD Film
|The Human Centipede 2: Full Sequence
| 
|-
|}

==Sequel==
 
Six has revealed that the third film will again be very different from the previous entry, but will also start with its ending, making the trilogy similar to a centipede. In the end, the parts of the trilogy will form one continuous film about four and a half hours long. He also stated that the third film will answer some "lasting questions", will have a strange happy ending, and will be the last of the series as he does not want to do any more Centipede films.  In an interview with DreadCentral.com, Six said the third film will "make the last one look like a Disney film. Were going to shoot the third film entirely in America, and its going to be my favorite... Its going to upset a lot of people."

As of January 2013, Dieter Laser and Six had ended their legal battle due to creative differences which had been ongoing since March 2012. Laser and Harvey were confirmed as returning for the third film. The film was to be set in America, would star a "big American celebrity", would have "a storyline that nobody would expect" and have a centipede consisting of 500+ people. 
 Tommy "Tiny" Lister, Jr.. Filming concluded in June with part of the film taking place in a prison. The official tagline for the film is "100% Politically Incorrect". 

==See also==
 
* List of banned films
* List of black-and-white films produced since 1970

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 