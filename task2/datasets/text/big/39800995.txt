The Count of Cagliostro
{{Infobox film
| name           = The Count of Cagliostro
| image          = 
| image_size     = 
| caption        = 
| director       = Reinhold Schünzel 
| producer       = Victor Micheluzzi  Reinhold Schünzel
| writer         = Robert Liebmann   Reinhold Schünzel
| narrator       = 
| starring       = Reinhold Schünzel   Anita Berber   Conrad Veidt   Carl Goetz
| music          = 
| editing        =
| cinematography = Carl Hoffmann   Kurt Lande
| studio         = Micro-Film 
| distributor    = 
| released       = 7 January 1920
| runtime        = 
| country        = Austria
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| 
}} Austrian silent silent horror Italian adventurer Alessandro Cagliostro.  The films art direction was by Oscar Werndorff.

==Cast==
* Reinhold Schünzel as Cagliostro 
* Anita Berber as Lorenza Cagliostros Skalvin 
* Conrad Veidt as The Minister 
* Carl Goetz as Fürst 
* Hugo Werner-Kahle as Cagliostros Diener 
* Hanni Weisse as Favorite of Fürst 
* Hilde Woerner as Zofe der Favoritin  Walter Huber   
* Heinrich Jensen   
* Armin Seydelmann   
* Ferry Sikla

==References==
 

==Bibliography==
* Hutchings, Peter. The A to Z of Horror Cinema. Scarecrow Press, 2009.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 