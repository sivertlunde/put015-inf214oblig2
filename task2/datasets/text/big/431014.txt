Big Fish
 
 
{{Infobox film
| name           = Big Fish
| image          = Big-fish-movie-poster.jpg
| caption        = Theatrical release poster
| director       = Tim Burton
| producer       = Richard D. Zanuck Bruce Cohen Dan Jinks
| screenplay     = John August
| based on       =  
| starring       = Ewan McGregor Albert Finney Billy Crudup Jessica Lange Helena Bonham Carter Alison Lohman Robert Guillaume Marion Cotillard Steve Buscemi Danny DeVito 
| music          = Danny Elfman
| cinematography = Philippe Rousselot
| editing        = Chris Lebenzon
| studio         = Jinks/Cohen Company The Zanuck Company Tim Burton Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 125 minutes
| country        = United States
| language       = English
| budget         = $70 million   
| gross          = $122.9 million 
}} Daniel Wallace.  The film was directed by Tim Burton and stars Ewan McGregor, Albert Finney, Billy Crudup, Jessica Lange, and Marion Cotillard. Other roles are performed by Helena Bonham Carter, Matthew McGrory, and Danny DeVito among others. Finney plays Edward Bloom, a former traveling salesman from the Southern United States with a gift for storytelling, now confined to his deathbed.  Blooms estranged son, a journalist played by Crudup, attempts to mend their relationship as his dying father relates tall tales of his eventful life as a young adult, played by Ewan McGregor.
 Minority Report Planet of the Apes (2001) and brought Ewan McGregor and Albert Finney on board.
 vignettes evoking Oscar and a Grammy Award nomination for Danny Elfmans original score.

== Plot ==
At his sons wedding party, Edward Bloom ( ) was born, he was out catching an enormous uncatchable fish, using his wedding ring as bait. Will is annoyed, explaining to his wife Joséphine (Marion Cotillard) that because his father lives in a fantasy world and has never told the straight truth about anything, he felt unable to trust him. He is troubled to think that he might have a similarly difficult relationship with his future children. Wills relationship with his father becomes so strained that they do not talk for three years. But when his fathers health starts to fail from cancer, Will and the now pregnant Joséphine return to to his hometown in Alabama to visit. On the plane, Will recalls his fathers tale of how he braved a swamp as a child after he was dared by a few other children. He meets a witch (Helena Bonham Carter). She shows Don Price and another boy how they were going to die. They run away, frightened. When the witch shows Edward his death in her glass eye, he accepts it without fear. With this knowledge, Edward knew there were no odds he could not face.

As Will sits by his dying fathers bedside, Edward continues telling tall tales, claiming he spent three years confined to a bed as a child because his body was growing too fast. While in high school, Edward became a successful athlete, but found the town of Ashton too small for his ambition, and set off with the misunderstood giant Karl (Matthew McGrory). The witch with the glass eye is seen bidding him farewell. While traveling, Edward and Karl see two separate roads out of Ashton. Edward suggest they each take one way. Hell take the old dirt road and Karl should take the new paved road. They will meet on the other side. Karl feared that Edward was attempting to abandon him, but Edward gives him his backpack to prove that he isnt. 

After walking through a scary swamp, Edward discovers the hidden town of Spectre, where everyone is friendly to the point of comfortably walking around barefoot. Their shoes can be seen hanging from a wire near the entrance. When he enters the town he is greeted by the Mayor and his wife. The Mayor has a clipboard that says Edward was meant to be in their town but he had arrived early. He also tells him of the poet Norther Winslow (Steve Buscemi) who was also from Ashton. While there Edward has an encounter with a mermaid. She swims away before he could see her face. Edward leaves because he does not want to settle anywhere yet, but promises to the town mayors daughter Jenny (Hailey Anne Nelson), who developed a crush on him, that he will return. He believed that he was fated to be there someday.

Edward meets up with Karl. They attend the Calloway Circus where Edward falls in love at first sight with a mysterious woman. Together, Karl and Edward begin working at the circus. Karl meets his destiny by working as the giant man, replacing the old one who is much smaller than him. Edward works without pay, as he has been promised by the ringmaster Amos Calloway (Danny DeVito), who claims to know the mysterious woman, that each month he will learn something new about the mysterious woman. Three years later, having only learned trivia about her, Edward discovers Amos is a werewolf. In return for his refusal to harm him in his monstrous state, Amos tells Edward the girls name is Sandra Templeton (Alison Lohman) and she studies at Auburn University.

Edward goes to Sandra to confess his love. He learns Sandra is engaged to Don Price (David Denman), whom Edward always overshadowed during his days in Ashton. Sandra refuses Edwards proposal but that does not discourage Edward. He writes "I love Sandra" everywhere he could. Don arrived to challenge Edward to a fight over Sandra. Sandra makes Edward promise not to fight Don. Edward allows Don to beat him up. Sandra, disgusted by Dons violence, ends their engagement and falls for Edward. Edward later reveals that Don died from a heart attack on the toilet bowl at an early age (as Don saw in the Witchs eye). 
 Siamese twin dancers Ping (Ada Tai) and Jing (Arlene Tai) to help him get back to the United States, where he will make them stars. He is unable to contact anyone on his journey home, and the military declares him dead. This limits Edwards job options when he does return home, so he becomes a traveling salesman. Meeting the poet Norther Winslow from Spectre again, Edward unwittingly helps him rob a bank, which is already bankrupt. Edward explains this to Winslow, who then decides that he will work at Wall Street. Winslow later thanks Edward for his "advice" by sending him $10,000, which he uses to buy his family a dream house.

In the present, still unimpressed by his fathers stories, Will demands to know the truth, but Edward explains that he is who he is: a storyteller. While doing his own investigation into his fathers stories, Will finds the small town of Spectre, and meets an older Jenny (Helena Bonham Carter), who explains that Edward rescued the town from bankruptcy by buying it at an auction and rebuilding it with financial help from many of his previous acquaintances. Will suggests his father had been having an affair with Jenny, to which she replies that while she had indeed fallen in love with him, Edward could never love any woman other than Sandra. 

When Will returns home, he is informed his father had a stroke and is at the hospital. He goes to visit him there and finds him only partly conscious, and unable to speak at length. Since Edward can no longer tell stories, he asks Will to tell him the story of how it all ends. Beat and deciding to play along, Will tells his own tall tale about helping Edward escape from the hospital and they go to the river where everyone in Edwards life appears to bid him goodbye. Will carries his father into the river where he becomes what he always had been: a very big fish. Edward then dies, knowing his son finally understands his love of storytelling.

At Edwards funeral, Will is astonished when all of the characters from Edwards stories appear to pay their condolences to his father; Amos, Karl, Norther Winslow, Jenny, Ping and Jing all arrive, though each one is a slightly less fantastical version of themselves than in Edwards stories – the sisters, for example, are not conjoined but are merely identical twins. Will finally realizes the truth of his fathers life for which his stories were embellishments. When his own son is born, Will passes on his fathers tall tale stories, remarking that his father became his stories, allowing him to live forever.

== Cast ==
*Albert Finney as the old Edward Bloom
**Ewan McGregor as a young Edward
**Scott Christopher Mcpherson Jr. as Edward as a child
*Jessica Lange as Sandra K. Bloom, Edwards wife
**Alison Lohman as the young Sandra, née Templeton
*Billy Crudup as William "Will" Bloom
*Marion Cotillard as Joséphine Bloom
*Helena Bonham Carter as Jennifer Hill (Jenny). Bonham Carter also plays an elderly Witch who gives a young Bloom a vision of his future death
**Hailey Anne Nelson as Jenny as an eight-year-old when Edward first meets her
*Robert Guillaume as Dr. Bennett, the family doctor
*Matthew McGrory as Karl, the misunderstood Giant
*Danny DeVito as Amos Calloway, a circus ringmaster and werewolf
*Steve Buscemi as Norther Winslow, a poet from Ashton who supposedly went missing, having never left the idyllic town of Spectre
*Ada Tai and Arlene Tai as Ping and Jing, conjoined twins who perform as singers for soldiers in Korea
*Bevin Kaye as River Woman (Fish)
*David Denman as Don Price, a boy from Ashton who was always overshadowed by Edwards achievements and former lover of Sandra Templeton
**John Lowell as Don as a child
*Loudon Wainwright III as Beamen, the mayor of Spectre, and Jennys father
*Missi Pyle as Mildred, Beamens wife
*Miley Cyrus as 8-year-old Ruthie   Daniel Wallace as Economics teacher
*Deep Roy as Mr. Soggybottom, the circus clown and Amos Attorney
*George McArthur as Colossus, former giant in circus, replaced by Karl

== Themes ==
  }}
The reconciliation of the father-son relationship between Edward and William is the key theme in Big Fish.       Novelist Daniel Wallaces interest in the theme of the father-son relationship began with his own family. Wallace found the "charming" character of Edward Bloom similar to his father, who used charm to keep his distance from other people.    In the film, Will believes Edward has never been honest with him because Edward creates extravagant myths about his past to hide himself, using storytelling as an avoidance mechanism.    Edwards stories are filled with fairy tale characters (a witch, mermaid, giant, and werewolf) and places (the circus, small towns, the mythological city of Spectre), all of which are classic images and archetypes.   The quest motif propels both Edwards story and Wills attempt to get to the bottom of it.  Wallace explains: "The fathers quest is to be a big fish in a big pond, and the sons quest is to see through his tall tales." 

Screenwriter John August identified with Wills character and adapted it after himself. In college, Augusts father died, and like Will, August had attempted to get to know him before his death, but found it difficult. Like Will, August had studied journalism and was 28 years old. In the film, Will says of Edward, "I didnt see anything of myself in my father, and I dont think he saw anything of himself in me. We were like strangers who knew each other very well."     Wills description of his relationship with Edward closely resembled Augusts own relationship with his father.  Director Tim Burton also used the film to confront his thoughts and emotions concerning the death of his father in 2000:  "My father had been ill for a while... I tried to get in touch with him, to have, like in this film, some sort of resolution, but it was impossible." 

Religion and film scholar Kent L. Brintnall observes how the father-son relationship resolves itself at the end of the film.  As Edward dies, Will finally lets go of his anger and begins to understand his father for the first time:
 In a final gesture of love and comprehension, after a lifetime of despising his fathers stories and his father as story-teller, Will finishes the story his father has begun, pulling together the themes, images and characters of his fathers storied life to blend reality and fantasy in act of communion and care.  By unselfishly releasing the anger he has held about his fathers stories, Will gains the understanding that all we are are our stories and that his fathers stories gave him a reality and substance and a dimension that was as real, genuine, and deep as the day-to-day experiences that Will sought out.  Will comes to understand, then, that his father—and the rest of us—are our stories and that the deeper reality of our lives may, in fact, not be our truest self.    

== Production ==
=== Development === Daniel Wallace. Minority Report (2002). 

Spielberg courted   (2002),    and DreamWorks also backed out of the film. 

With Spielberg no closer to committing, August, working with Jinks and Cohen,  considered Stephen Daldry as a potential director.  "Once Steven decided he wasnt going to do it, we put the script back to the way it was," recalls Jinks. "Steven even said, I think I made a mistake with a couple of things I asked you guys to try." August took his favorite elements from the previous drafts, coming up with what he called "a best-of Big Fish script. "By the time we approached Tim Burton, the script was in the best shape it had ever been." 

 
 Planet of the Apes (2001), the director wanted to get back to making a smaller film. Burton enjoyed the script, feeling that it was the first unique story he was offered since Beetlejuice (1988). Burton also found appeal in the storys combination of an emotional drama with exaggerated tall tales, which allowed him to tell various stories of different genres.    He signed to direct in April 2002,    which prompted Richard D. Zanuck, who worked with Burton on Planet of the Apes, to join Big Fish as a producer. Zanuck also had a difficult relationship with his own father, Darryl F. Zanuck, who once fired him as head of production at 20th Century Fox. 

=== Casting === Tom Jones White Oleander (2002).  Burtons girlfriend, Helena Bonham Carter, was also cast in two roles. Her prosthetic makeup for The Witch took five hours to apply. "I was pregnant throughout filming, so it was weird being a pregnant witch," the actress reflected. "I had morning sickness, so all those fumes and the make-up and the rubber...it was hideous." 

Burton personalized the film with several cameos. While filming in Alabama, the crew tracked down   makes a brief appearance as Sandras economics teacher in the "Courtship of Sandra Templeton" scene. 

=== Filming === CGI of conjoined twins.  ]] Montgomery (such Cloverdale neighborhood) Tallassee and on the campus of Huntingdon College.  Scenes for the town of Spectre were filmed on a custom set located on an island between Montgomery and Millbrook, Alabama. Principal photography for Big Fish in Alabama lasted from until the first week of April.   and is estimated to have generated as much as $25 million for the local economy.   

Burton filmed all the dramatic hospital scenes and most of those involving Finney first, before moving on to the McGregor section of Blooms life.  Although McGregor was on set from the very beginning of filming, Burton chose to shoot all Finneys scenes first.  Location filming in Alabama experienced a setback due to weather problems.  During the production of the Calloway circus scenes, a tornado watch was issued and flooding on the set interrupted filming for several weeks.   Despite the delays due to weather, Burton was able to deliver the film on budget and on schedule. 

The director attempted to use as limited an amount of digital effects as possible. However, because he wanted to evoke a Southern Gothic fantasy tone for Big Fish, color grading techniques were applied by Sony Pictures Imageworks.  Stan Winston Studios, with whom Burton worked with on Edward Scissorhands (1990) and Batman Returns (1992), designed Helena Bonham Carters prosthetic makeup and created the animatronics.  Scenes with Karl the Giant were commissioned using forced perspective filmmaking.   

=== Music ===
 
The soundtrack was composed by regular Burton collaborator Danny Elfman.  Burton approached Pearl Jam during post-production to request an original song for the soundtrack and closing credits. After screening an early print of the film, Pearl Jam vocalist Eddie Vedder went home and wrote "Man of the Hour", completing the demo by the next day. It was recorded by the band four days later.     Guitarist Mike McCready stated, "We were so blown away by the movie...Eddie and I were standing around talking about it afterwards and were teary-eyed. We were so emotionally charged and moved by the imagination and humanity that we felt because of the movie." 

== Release ==
Columbia Pictures planned to wide release Big Fish in the United States on November 26, 2003  before pushing it back to December 10 for a limited release.  The film premiered on December 4, 2003 at the Hammerstein Ballroom in Manhattan.  The domestic wide release in the U.S. came on January 9, 2004, with the film appearing in 2,406 theaters and earning $13.81 million in its opening weekend. The film eventually grossed $66.81 million in U.S. totals and $56.11 million in foreign countries, with a total of $122.92 million worldwide.   

=== Critical response ===
Big Fish received positive reviews from film critics. Based on 212 reviews collected by Rotten Tomatoes, 77% of the critics positively reviewed Big Fish, for an average score of 7.2/10.  By comparison, Metacritic calculated an average score of 58/100, based on 43 reviews. 
 picaresque in Rolling Stone magazine praised Burtons direction, feeling it was a celebration of the art of storytelling and a touching father–son drama.   

Mike Clark of   of Time (magazine)|Time magazine was disappointed, finding the father-son reconciliation storyline to be over-dramatically cliché. "You recall The Boy Who Cried Wolf? Edward Bloom is the man who cried fish."  Big Fish was placed at 85 on Slant Magazines best films of the 2000s. 

=== Home media === Region 1 DVD was released on April 27, 2004,  and Region 2 was released on June 7.  The DVD features a Burton audio commentary track, seven featurettes and a trivia quiz. A special edition was released on November 1, 2005, with a 24-page hardback book entitled Fairy Tale for a Grown Up.  The film was released on Blu-ray Disc on March 20, 2007. 

== Accolades == Best Motion Best Supporting Best Original Best Original Song (Pearl Jams "Man of the Hour"). 
 Best Film, Best Direction Best Adapted Best Actor Best Production Best Visual Best Makeup and Hair (Jean Ann Black and Paul LeBlanc). 
 Best Actor Best Fantasy Film. 
 Best Score Soundtrack Album for a Motion Picture. 

== References ==
 

== Further reading ==
*  
*  
*  
*   
*  

== External links ==
 
*  
*  
*  
*  
*  
*   at Montgomery Advertiser

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 