Ghost Day (film)
{{Infobox film
| name           = Ghost Day
| image          = Ghost-day-2012-film-poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Thai film poster for Ghost Day
| director       = {{plainlist|
*Thanit Jitnukul
*Titipong Chaisat
*Sorathep Vetwongsatip}}
| producer       =  {{plainlist|
*Thanit Jitnukul
*Thawatchai Phanpakdee }}
| writer         =  {{plainlist|
*Titipong Chaisati
*Sorathep Vetwongsatip
*Nidchaya Boonsiripunth
*Samkan Chotikasawad
*Yossapong Phonsup
*Thanit Jitnukul 
}}
| based on       =  
| starring       = {{plainlist|
*Joey Boy
*Pimradapa Wright
*Padong Songsang
*Surasak Wongthai 
}}
| music          = Thippatai Pirompak 
| cinematography = Wattana Wanchooplao 
| editing        = {{plainlist|
*Sunit Asvinikul
*Nopadol Kumduang
*Titipong Chaisati }}
| studio         =  Phranakorn Film 
| distributor    = 
| released       =  
| runtime        = 92 minutes 
| country        = Thailand 
| language       = 
| budget         = 
| gross          = $79,292
}}

Ghost Day  ( ) is a 2012 Thai horror comedy film directed by  Thanit Jitnukul, Titipong Chaisat, and Sorathep Vetwongsatip. The film is set in modern day Bangkok where the production team behind the television series Ghost Day is told by their show is going to be cancelled. To stop the end of their program, the staff decide to find a real haunted area and, after viewing an video online by ghostbusters Mhen (Apisit Opasaimlikit) and Chiad (Padong Songsang), they decide to approach them to do a deal. Mhen and Chiad have brought along a real female ghost who appears and possesses the shows director, Pom (Boriboon Chanruang). 

The film was shown at the 16th Puchon International Fantastic Film Festival and was the fifth highest grossing film in Thailand on its opening week.

==Release==
Ghost Day was shown at the 16th annual Puchon International Fantastic Film Festival.  

The film was released in Thailand on February 23, 2012.    It was the fifth highest grossing film in Thailand on its opening week grossing $44,438.  It grossed a total of $79,292 on its theatrical run in Thailand. 

==Reception==
Derek Elley of Film Business Asia gave the film a six out of ten rating, referring to the film as "one of the better  , though still very silly." 

==Notes==
 

==See also==
 
*List of Thai films
*List of horror films of 2012
*List of comedy films of the 2010s

==External links==
* 

 
 