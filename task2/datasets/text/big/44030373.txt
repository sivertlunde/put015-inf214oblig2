Maanasaveena
{{Infobox film
| name           = Maanasaveena
| image          =
| caption        =
| director       = Babu Nanthankodu
| producer       = 
| writer         = Sreekumaran Thampi
| screenplay     = Sreekumaran Thampi Madhu Jayabharathi Adoor Bhasi Prameela
| music          = ML Srikanth
| cinematography = Vipin Das
| editing        = G Kalyana Sundaram
| studio         = Sree Lakshmi Ganesh Pictures
| distributor    = Sree Lakshmi Ganesh Pictures
| released       =  
| country        = India Malayalam
}}
 1976 Cinema Indian Malayalam Malayalam film,  directed by Babu Nanthankodu. The film stars Madhu (actor)|Madhu, Jayabharathi, Adoor Bhasi and Prameela in lead roles. The film had musical score by ML Srikanth.   

==Cast==
  Madhu
*Jayabharathi
*Adoor Bhasi
*Prameela
*T. R. Omana Raghavan
*Unnimary
*Bahadoor
*Kuthiravattam Pappu Vincent
 

==Soundtrack==
The music was composed by ML Srikanth and lyrics was written by Sreekumaran Thampi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Maayayaam Maareechan || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 2 || Nilaavo Ninte Punchiriyo || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 3 || Santhaanagopaalam || LR Eeswari || Sreekumaran Thampi ||
|-
| 4 || Swapnam Tharunnathum || P Susheela || Sreekumaran Thampi ||
|-
| 5 || Thulasi Vivaaha Naalil || S Janaki || Sreekumaran Thampi ||
|-
| 6 || Urakkam Mizhikalil || P Susheela, ML Srikanth || Sreekumaran Thampi ||
|}

==References==
 

==External links==
*  

 
 
 


 