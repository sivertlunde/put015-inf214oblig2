Sharey Chuattor
{{Infobox film
| name           = Sharey Chuattor
| image          = SC53.jpeg
| image size     = 180px
| alt            = Original poster of Sharey Chuattor
| caption        = Original poster of Sharey Chuattor
| director       = Nirmal Dey
| story          = Bijon Bhattacharya
| starring       = Tulsi Chakrabarti Molina Devi Uttam Kumar Suchitra Sen Bhanu Bannerjee Jahar Roy
| music          = Kalipodo Sen
| cinematography = Amal Das
| editing        = Kali Raha
| released       = 20 February 1953   
| country        = India Bengali
}} Bengali comedy film, directed by Nirmal Dey, story by Bijon Bhattacharya, starring Tulsi Chakrabarti and Molina Devi, and co-starring Suchitra Sen and Uttam Kumar. Contemporary playback singers like Dhananjay Bhattacharya|Dhananjay, Dwijen Mukherjee, Shamol Mitra and Manabendra Mukherjee acted in this film. Bhanu and Jahar were also cast.

==Story outline==

The Annapurna Boarding House owned by Rajanibabu is a peaceful abode where the residents are all friendly except Shibbabu, a senior learned man, who sometimes acts as a killjoy. The story begins when Romola, a relative of Rajanibabu and her family comes to stay in the boarding, after being thrown out of their rented house. Rajanibabu calls a meeting where all the residents except Shibbabu cast vote in favour of Romolas staying. 
Rampriti, the son of a wealthy family, who also stays at the boarding, is away home. On the day of his return, he telephones Annapurna Boarding to inform the cook Modon to prepare food for him that night. Seeing no one to answer the phone, Romola picks it up, but Rampriti does not believe that theres a girl in the boarding and so an exchange of hot words takes place. Rampriti, after returning narrates the incident to Kedar, another resident of the Boarding. Romola overhears them and berates Rampriti, silencing him. 
However, they fall in love and exchange love letters, but this fact becomes known to the other residents and they start teasing both of them. Rajanibabu, the go-between, is given a love letter to deliver but as hes hurrying to catch the train home, he keeps it in his pocket. At home, he quarrels with his wife and leaves the house in the middle of night. His wife manages to find the love letter and thinks that her husband is running an extramarital affair. 
Seeing that Romola and Rampriti are in love, Rajanibabu makes the marriage arrangements for them and calls Rampritis  and Romolas father. 
Rajanibabus wife prepares a puja to bring him back. She becomes impatient and goes to the Boarding House. She charges her husband of infidelity and drags him to another room where Rajanibabu tells her the truth about the letter. He says, it  is about Rampriti and Romola, the couple who are getting married the very day. The residents make fun of them and the film ends with Rampriti and Romola sitting side by side at the marriage altar.

==Cast==

{| class="wikitable"
|-
! Cast !! Name in the film
|-
| Tulsi Chakrabarti || Rajanibabu
|-
| Molina Devi || Rajanibabus wife
|-
| Uttam Kumar || Rampriti
|-
| Suchitra Sen || Romola
|- Bhanu Bannerjee || Kedar
|-
| Jahor Roy || Kamakhya
|-
| Nadadwip Halder || Modon
|-
|-
| Gurudas Bannerjee || Romalas father
|-
| Padma Devi || Ramalas mother
|-
| Sital Bannerjee || Resident at the mess
|-
| Dhananjay Bhattacharya || Akhil Babu
|-
| Sital Bannerjee || Resident at the mess
|-
| Panchanan Bhattacharya || Elderly resident at the mess
|-
| Reba Bose || 
|-
|-
| Shyamal Mitra || Resident at the mess
|-
| Manabendra Mukherjee || Resident at the mess
|-
| Dwijen Mukherjee || Resident at the mess
|-
| Shyam Laha || Resident at the mess
|-
| Ranjit Roy || Resident at the mess
|- 
|}

==Music==
* Playback singers were: Shyamal Mitra, Manabendra Mukherjee, Dwijen Mukherjee, Dhananjay Bhattacharya, Pannalal Bhattacharya, Sanat Singha, Suprava Sarkar

* Lyrics by Sailen Roy
* Music Direction by Kalipodo Sen

==References==
; Citations
 

; Sources
*  
* http://calcuttatube.com/sharey-chuattar-1953-bengali-movie/119/

 
 