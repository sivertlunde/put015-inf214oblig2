The Violet of Potsdamer Platz
{{Infobox film
| name =  The Violet of Potsdamer Platz
| image =
| image_size =
| caption =
| director = Johann Alexander Hubler-Kahla
| producer =  Lothar Stark 
| writer =  Otto Ernst Hesse    Bobby E. Lüthge    Helena von Fortenbach
| narrator =
| starring = Rotraut Richter   Paul W. Krüger   Margarete Kupfer   Else Elster
| music = Jim Cowler   
| cinematography = Georg Muschner   Paul Rischke  
| editing =  Walter Wischniewsky         
| studio = Lothar Stark-Film  NDLS 
| released =  16 November 1936 
| runtime = 89 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Violet of Potsdamer Platz (German:Das Veilchen vom Potsdamer Platz) is a 1936 German drama film directed by Johann Alexander Hubler-Kahla and starring  Rotraut Richter, Paul W. Krüger and Margarete Kupfer. 

==Cast==
* Rotraut Richter as Mariechen Bindedraht 
* Paul W. Krüger as Vater Pietsch - Droschkenkutscher 
* Margarete Kupfer as Mutter Pietsch 
* Else Elster as Rosa - ihre Enkelin 
* Fritz Kampers as Otto Schnöcker 
* Anton Pointner as Seidewind 
* Paul Westermeier as Knallkopp 
* Hermann Schomberg as Schupo Lemke  Hans Richter as Fritz 
* Alfred Beierle as Held - Geldverleiher 
* Otto Kronburger as Hansen - Kriminalrat 
* Lotte Werkmeister as Blumenfrau

== References ==
 

== Bibliography ==
* Rentschler, Eric. The Ministry of Illusion: Nazi Cinema and Its Afterlife. Harvard University Press, 1996.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 