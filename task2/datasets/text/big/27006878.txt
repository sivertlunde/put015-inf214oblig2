Romance (1920 film)
{{Infobox film
| name           = Romance
| image          = Romance (1920) - Sydney & Keane.jpg
| image_size     = 200px
| caption        = Film still with Sydney and Keane
| director       = Chester Withey
| producer       = ?Doris Keane (copyright) ?Albert Grey (copyright)
| writer         = Edward Sheldon (play Romance) Wells Hastings (scenario)
| starring       = Doris Keane Basil Sydney
| cinematography = Louis Bitzer
| editing        =
| distributor    = United Artists
| released       =  
| runtime        = 70 minutes (7 reels)
| country        = United States Silent (English intertitles)
}}

Romance is a 1920 American silent film directed by Chester Withey and released through United Artists. The film is based on the 1913 play Romance by Edward Sheldon and stars Doris Keane, the actress who created the role in the play. This was Miss Keanes only motion picture. David Wark Griffith|D.W. Griffith allowed the use of his Mamaroneck Studios for the production. The nephew of Griffiths favorite cameraman, Billy Bitzer,  was the cinematographer. The story was later remade as Romance (1930 film)|Romance in 1930, an early talking vehicle for Greta Garbo.

No copies of Romance are known to survive making it another lost film. 

==Plot==
As described in a film publication,  a youth (Arthur Rankin) in the prologue seeks advice from his grandfather (Sydney), who then recalls a romance of his own youth which is then shown as a Flashback (narrative)|flashback. A priest (Sydney) is in love with an Italian opera singer (Keane), and the drama involves the conflict between his efforts to rise above worldly things or to leave with her. The romance ends with a deep note of pathos.

==Cast==
*Doris Keane - Madame Cavallini
*Basil Sydney - The priest
*Norman Trevor - Cornelius Van Tuyl
*Betty Ross Clarke - Susan Van Tuyl
*Amelia Summerville - Miss Armstrong
*A.J. Herbert - Mr. Livingston
*Gilda Varesi - Vanucci (as Gilda Varesi Archibald) John Davidson - Beppo
==Production==
The movie was based on a hit play and was financed by fledgling United Artists. $150,000 was spent on the story and Doris Keanes salary. It went $100,000 over budget and recorded a loss of $80,000. {{cite book
 | last = Balio
 | first = Tino
 | authorlink =
 | coauthors =
 | title = United Artists: The Company Built by the Stars
 | date = 2009
 | publisher = University of Wisconsin Press
 | isbn = 978-0-299-23004-3
 }}p34 
==References==
 

==External links==
 
* 
* 

 
 
 
 
 
 
 