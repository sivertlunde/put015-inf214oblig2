The Women (2008 film)
{{Infobox film
| name = The Women
| image = Womenposter08.jpg
| caption = Theatrical release poster
| director = Diane English
| producer = Diane English Mick Jagger Bill Johnson Victoria Pearman
| writer = Diane English
| starring = Meg Ryan Annette Bening Eva Mendes Debra Messing Jada Pinkett Smith Candice Bergen Cloris Leachman Bette Midler Carrie Fisher Debi Mazar Keegan Connor Tracy Ana Gasteyer Jill Flint Lynn Whitfield Joanna Gleason India Ennenga
| music = Mark Isham
| cinematography = Anastas N. Michos
| editing = Tia Nolan Shukovsky English Entertainment Picturehouse
| released =  
| runtime = 114 minutes
| country = United States
| language = English
| budget = $16 million
| gross = $50,007,546
}}
 1939 film 1936 play by Clare Boothe Luce.

In the original film, most of the characters were Manhattan socialites whose primary interest was idle gossip. In the 2008 version, several work in the fields of fashion design and publishing, and the character of Alex Fisher is openly a lesbian.

A feature of the film, shared with the 1939 version, is that the movie does not show a single male actor or extra, with the exception of the baby at the very end of the film.

==Plot== suburban Connecticut editor of manicurist Tanya, she confides in the ever-pregnant Edie Cohen but hesitates to tell Mary, who discovers the news from the same woman after getting a manicure herself. Despite her mother Catherines exhortation to keep quiet about what she knows and a holiday away, Mary confronts Crystal first, in a lingerie store, and then Steven, before asking for a divorce.

Sylvie, Edie, and writer Alex Fisher join forces to support their spurned friend, but complications arise when Sylvie, facing the loss of her job, conspires with local gossip columnist Bailey Smith by confirming Marys marital woes in exchange for Bailey contributing a celebrity profile to the magazine. Mary is stunned by Sylvies betrayal and ends their friendship. Marys daughter begins to ditch school and confides in Sylvie because her mother, distracted by the upheavals in her once idyllic life, becomes more distant.

Mary is fired from her job by her father, has a makeover, and decides to open her own clothing design firm with some financial assistance from Catherine. As she begins to get her life in order, she makes an effort to bond with Molly, who reveals her fathers relationship with Crystal is unraveling and reunites with Sylvie, who has quit her job. With this knowledge in hand, Mary sets out to repair her fractured marriage as she prepares to unveil her new line of womenswear in a fashion show attended not only by boutique owners but the buyer from Saks Fifth Avenue|Saks, as well. Sylvie tells Mary that she has met a guy and is thinking of giving him her real phone number. In the final scene, Edies water breaks and she has a baby boy. Whilst Edie is in labour Mary receives a call from her husband and is encouraged by the others to answer it; she then arranges a date with him.

After the ending credits we see that a magazine titled Sylvie is published with the four friends on the cover and Alexs book is out.
A hint is given about Crystals possibly going out with Alexs ex-girlfriend Natasha.
The women talk about the movie, the magazine, the book and the joys, heartaches and uniquely special triumphs of being a woman.

==Production==
 
In The Women: The Legacy, a bonus feature on the DVD release of the film, Diane English discusses her fifteen-year-long struggle to bring a contemporary version of the 1939 classic film to the screen. She wanted to present a version in which the female characters were strong and self-reliant and supported and defended each other rather than resort to treachery and catty remarks to achieve their goals. Since the concept of women going to Reno in search of a divorce is archaic, she needed to eliminate this aspect of the original plot from her treatment, which necessitated deleting several characters from the story. One character that is not in its original form is Lucy, who in the play and original movie was the maid in Reno, here she is seen as Marys dog.

English wrote the first screenplay in 1993 during hiatus from Murphy Brown. The following year, Julia Roberts and Meg Ryan agreed to co-produce and star, with James L. Brooks as director and a supporting cast including Blythe Danner, Marisa Tomei, Debi Mazar, and Candice Bergen. In 1996, the first table reading of the script was held on the Sony Pictures lot. Despite the enthusiasm of everyone involved, the project stalled when Roberts and Ryan decided they wanted to play the same role. 

English spent the following year revising the screenplay, during which time Brooks dropped out to direct As Good as It Gets. Roberts also lost interest and moved on. English first entertained the idea of directing the film herself in 2001. Over the next few years, Sandra Bullock, Ashley Judd, Uma Thurman, Whitney Houston, and Queen Latifah were among those to express interest, although none were attached officially.
 Sex and the City convinced them there was an audience for an all-female film. 

The film was shot on location in New York City and Georgetown, Massachusetts|Georgetown, Dover, Massachusetts|Dover,  Gloucester, Massachusetts|Gloucester, Sudbury, Massachusetts|Sudbury, Medfield, Massachusetts|Medfield, and Boston in Massachusetts. As with the play and 1939 film, English was careful to make sure no men appear on screen, even in long shots and crowd scenes. The only male character in the film is Edies baby boy, born in the final scene of the film, and the waiter at the café, at the credit scene. 

==Cast==
 
* Meg Ryan as Mary Haines
* Annette Bening as Sylvie Fowler
* Eva Mendes as Crystal Allen
* Debra Messing as Edie Cohen
* Jada Pinkett Smith as Alex Fisher
* Candice Bergen as Catherine Frazier
* Cloris Leachman as Maggie
* Tilly Scott Pedersen as Uta
* Bette Midler as Leah Miller
* Carrie Fisher as Bailey Smith
 
* Debi Mazar as Tanya
* Ana Gasteyer as Pat
* Jill Flint as Annie
* Lynn Whitfield as Glenda Hill
* Joanna Gleason as Barbara Delacorte
* Keegan Connor Tracy as Dolly Dupuyster
* Natasha Alam as Natasha
* India Ennenga as Molly Haines
* Susie Bubble as Mindy
* Christy Scott Cashman as Jean
 

==Critical reception==
The film received a significant negative response from critics and holds only a 13% rating on the web aggregate Rotten Tomatoes.

Roger Ebert of the Chicago Sun-Times was one of the few critics who enjoyed the film. He awarded it three out of four stars and commented, "What a pleasure this movie is, showcasing actresses Ive admired for a long time, all at the top of their form ... Diane English ... focuses on story and character, and even in a movie that sometimes plays like an infomercial for Saks Fifth Avenue, we find ourselves intrigued by these women ... The Women isnt a great movie, but how could it be? Too many characters and too much melodrama for that, and the comedy has to be somewhat muted to make the characters semi-believable. But as a well-crafted, well-written and well-acted entertainment, it drew me in and got its job done."  
 screwball before veering in the direction of weepiness and grasping at satirical urbanity along the way ... Rarely has class struggle, or catfighting, for that matter, been so tediously waged. And rarely have so many fine actresses been enlisted in such a futile cause."  

Kenneth Turan of the Los Angeles Times observed, "While the original film ... saw itself as a catty entertainment about New York society women coping with the infidelity of the husband of one of their friends, English has something grander and more complex in mind ... This version sees itself as both a farce and a manifesto, a glorification of female friendship and a celebration of womens need for self-realization ... All that would be a handful to pull off for the most experienced filmmaker, but English has never directed before, and it shows. The visual choices she makes in The Women are invariably static, and except for whatever energy the performers can manage, the storytelling has a dispiriting flatness to it ... The film becomes unfocused as it stumbles over all the points it wants to make. Given Englishs writing skills, the dialogue doesnt help as much as it should, tending too much toward one-liners that aim for raunchy whenever possible. Never particularly believable, the story quickly unravels into schematic contrivance and wish-fulfillment fantasy."  

David Wiegand of the San Francisco Chronicle said, "English doesnt make much of it very enjoyable. Shes so careful to resist the Neanderthal sensibilities of the original film, she often neglects to make her version of the story, well, fun. Worse, its only occasionally believable ... Even those who never saw Cukors movie will feel something is missing in Englishs version. Yes, some of whats missing is humor and snappy dialogue, but that could be forgiven, if only some of the characters were more believable and the direction not quite as uneven. English knows how to get good performances out of her cast, but her pacing is languid and sloppy, so much so that one is tempted to believe that for all she knows about pacing a 30-minute situation comedy|sitcom, English isnt quite ready to tackle the longer form."  

Peter Travers of Rolling Stone rated the film one out of four stars, calling it a "misbegotten redo" and "a major dud." He added, "Everyone ... struggles with a script that resists being crowbarred into the 21st century."  
 Worst Actress, losing to Paris Hilton for The Hottie and the Nottie.

Richard Schickel of Time (magazine)|Time called the film "one of the worst movies Ive ever seen." 

==Box office==
Despite the mostly negative reviews, the film was a moderate box office success. On its opening weekend, the film earned $10,115,210, ranking #4 behind Righteous Kill, The Family That Preys, and Burn After Reading. The film eventually grossed $26,902,075 in the US and $23,105,471 in foreign markets for a total worldwide box office of $50,007,546. 

==Home media==
The film was released on DVD on December 19, 2008 in the USA and 19 March 2009 in the UK.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 

 
 