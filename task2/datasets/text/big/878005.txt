Ong-Bak: Muay Thai Warrior
{{Infobox film
| name           = Ong-Bak: Muay Thai Warrior
| image          = ONG-BAK.jpg
| caption        = Theatrical poster
| director       = Prachya Pinkaew
| producer       = Somsak Techaratanaprasert Prachya Pinkaew
| eproducer      = 
| aproducer      = 
| writer         = Prachya Pinkaew Panna Rittikrai Suphachai Sittiaumponpan
| starring       = Tony Jaa Petchtai Wongkamlao Pumwaree Yodkamol
| music          = Atomix Clubbing Studio
| cinematography = Nattawut Kittikhun
| editing        = Thanat Sunsin  Thanapat Taweesuk
| studio         = Baa-ram-ewe
| distributor    = Sahamongkol Film International
| released       =  
| runtime        = 108 minutes (Original Thailand Version) 105 minutes (International Version)
| country        = Thailand Thai
| budget         = $1,100,000
| gross          = $20,112,926
}}
Ong-Bak: Muay Thai Warrior (   ), also known in the United States as Ong-Bak: The Thai Warrior is a 2003 Thai   and Ong Bak 3|Ong-Bak 3.

== Plot == Buddha statue named Ong-Bak. The village falls in despair after thieves from Bangkok decapitate the statue and take the head with them. Ting, a villager extremely skilled in Muay Thai, volunteers to travel to Bangkok to recover the stolen head of Ong-Bak. His only lead is Don, a drug dealer who attempted to buy the statue one day earlier.
 yaba dealers. Reluctant to help Ting, Humlae steals Tings money and bets all of it in an underground fighting tournament at a bar on Khaosan Road. Ting tracks down Humlae and gets his money back after stunning the crowd by knocking out the champion in the ring with one kick. His extraordinary skill grabs the attention of Komtuan, a gray-haired, wheelchair-bound crime lord who needs an electrolarynx to speak. It is discovered that Don had stolen Ong-Baks head to sell to Komtuan, who sees no value in it and orders him to dispose of it.
 baccarat game scam at an illegal casino. Ting fights off most of the thugs and helps Humlae and Muay Lek escape in exchange for helping him find Don. They return to the bar, where Ting wins the respect of the crowd after defeating three opponents consecutively. The trio find Dons hideout, triggering a lengthy Auto rickshaw#Thailand|tuk-tuk chase. The chase ends at a port in Chao Phraya River, where Ting discovers Komtuans cache of stolen Buddha statues submerged underwater.

After the statues are recovered by local police, Komtuan has his thugs kidnap Muay Lek and have Humlae tell Ting to fight his bodyguard Saming near the Thai-Burma border in exchange for Muay Lek and the Ong-Bak head. Ting is forced to throw the match against the drug-enhanced Saming, and Humlae throws in the towel. After the fight, Komtuan reneges on his promise to release Muay Lek and return Ong-Bak, and he orders his henchmen to kill the trio. Ting and Humlae subdue the thugs and head for a mountain cave, where Komtuans men are decapitating a giant Buddha statue. Ting defeats the remaining thugs and Saming, but is shot by Komtuan. Before the crime lord attempts to destroy the Ong-Bak head with a sledgehammer, Humlae jumps to protect it, taking the brunt of the hammer blows. The giant Buddha statue head suddenly falls, crushing Komtuan to death and critically injuring Humlae. Humlae gives Ting the Ong-Bak head, and with his dying breath, asks him to look after Muay Lek and make sure she graduates from college.

The head of Ong-Bak is restored in Ban Nong Pradu. Ting, now ordained as a monk, arrives into the village in a procession on an elephants back while the villagers and Muay Lek celebrate his ordination.

==Cast==
*Tony Jaa as Ting
*Petchtai Wongkamlao as Humlae/George (as Mum Jokemok in ending credits)
*Pumwaree Yodkamol as Muay Lek
*Chattapong Pantana-Angkul as Saming
*Pumwaree Yodkamol as Mue
*Suchao Pongwilai as Komtuan (as Suchoa Pongvilai in ending credits)
*Wannakit Sirioput as Don (as Wannakit Siriput in ending credits)
*Chumphorn Thepphithak as Uncle Mao (as Chumporn Teppitak in ending credits)
*Rungrawee Barijindakul as Ngek (as Rungrawee Borrijindakul in ending credits)
*Cheathavuth Watcharakhun as Peng (as Chetwut Wacharakun in ending credits)
*Dan Chupong as Bodyguard (as Chupong Changprung)
*Panna Rittikrai as Nong Pradu Villager (uncredited)

Club fighters:
*David Ismalone as Mad Dog
*Hans Eric as Pearl Harbour
*Paul Gaius as Lee
*Nick Kara as Big Bear
*Nudhapol Asavabhakhin as Toshiro

==Production==
Ong-Bak introduced international audiences to a traditional form of muay Thai (or Muay Boran, an ancient muay Thai style), a kickboxing style that is known for violent strikes with fist, feet, shins, elbows and knees. The fights were choreographed by Panna Rittikrai, who is also Tony Jaas mentor and a veteran director of B-movie action films.
Jaa, trained in Muay Thai since childhood, wanted to bring Muay Thai to mainstream so he decided to make this movie. Jaa and Panna struggled to raise money to produce a demo reel to drum up interest for the making of the film. Their first reel was made on expired film stock, so they had to raise more money and start over.

During the foot chase through the alleys, there is writing on a shop house door that reads "Hi Speilberg  , let do it together." This refers to the directors desire to someday work with  , we are waiting for you." The French producer-directors company, EuropaCorp, would go on to purchase the international distribution rights to the film.

One of Jaas favorite scenes is at the gas station. With his pants on fire, Ting kicks one of the villains in the face. The flames spread upwards very fast and burned Jaas eyebrows, eyelashes and nose. He then had to do a couple of more takes to make sure it was right.

==Alternate versions==
After Ong-Bak became a hit in Thailand, sales rights for outside Asia were purchased by Luc Bessons EuropaCorp, which in turn re-edited the film. Most of the subplot involving Muay Leks sister, Ngek, was removed, and the final showdown between Ting and Saming was shortened. EuropaCorp also re-scored the soundtrack with some Hip hop music|hip-hop sounds, replacing the Thai rock score; this is this version that has been made available in the United States and most of the Western world.

For the United Kingdom release, the soundtrack was scored yet again; this time with an orchestral score, but the film was left uncut with the subplot of Ngek.

The Hong Kong cut of the films theatrical release omits a "bone breaking" sequence toward the end, where Georges arm is snapped and Ting in turn snaps the leg of a bad guy. DVD releases in Hong Kong have the scene restored.

An alternative ending offered on the Thai, U.S., Australian, and UK DVD releases has Humlae surviving. He is seen at the end bandaged up, limping, with his arm broken, supported by his parents. Prachya Pinkaew stated in an interview that although there was debate, they ultimately decided it would be appropriate for him to make a meaningful sacrifice for the village.

===Alternate titles=== Premier Asias UK release.
* For the release in Singapore, Australia and other territories, as well as film festivals, the movie was released as Ong-Bak: Muay Thai Warrior.
* In the United States, Canada and other areas, the movie was released as Ong-Bak: The Thai Warrior. English title was Thai Fist.
* In Japan, the film was released as   (the Japanese word for "Mach number|Mach").
* In Italy, the title was Ong-Bak: Nato per Combattere, which translates as Ong-Bak: Born to Fight.
* In India, the title was Enter the New Dragon in reference to Bruce Lee.
* In Mexico, the title was Ong-Bak: El Nuevo Dragón, which translates as Ong-Bak: The New Dragon, in reference to Bruce Lee.

==Home media==
Ong-Bak: The Thai Warrior was released in the U.S. on DVD by Magnolia Pictures on August 30, 2005  and on Blu-ray Disc by 20th Century Fox on February 2, 2010. 

==Box office and reception==
Ong-Bak premiered as the closing film of the 2003 Bangkok International Film Festival, and then opened in a wide release in Thailand cinemas in February 2003. On February 11, 2005, the film was released in North America in 387 theatres under the title Ong-Bak: The Thai Warrior.  In its opening weekend, it grossed US$1,334,869 ($3,449 per screen), on its way to a US total of $4,563,167.
 CGI and wire-fu.  The film currently holds an 86% "Fresh" rating on Rotten Tomatoes based on 105 reviews, with the consensus being: "While Ong-Bak: The Thai Warrior may be no great shakes as a movie, critics are hailing the emergence of a new star in Tony Jaa, whose athletic performance is drawing comparisons with Bruce Lee, Jackie Chan, and Jet Li.". 

==Prequels== The Bodyguard (co-directed by Panna Rittikrai), and then starred in the much-anticipated Tom-Yum-Goong in 2005 in film|2005. In March 2006, it was announced that filming for Ong Bak 2 would start that fall and the film would be a prequel to the original.  The film was eventually released in December 2008 in film|2008, with Jaa debuting as director.

A second prequel, Ong Bak 3, followed where the second film left off. This means that Ong Bak takes place chronologically after Ong Bak 2 and Ong Bak 3.

==References==
;Notes
 

;Bibliography
* Yusof, Zack (November 21, 2003).  , The Star (Malaysia). (Retrieved from Google cache on March 28, 2006)
* Franklin, Erika (May 2005).   , Firecracker Media.

==External links==
* 
* 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 