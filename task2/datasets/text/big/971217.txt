Twisted (2004 film)
{{Infobox film
| name = Twisted
| image = Twisted-movie.jpg
| caption = Theatrical release poster
| director = Philip Kaufman
| writer = Sarah Thorp
| producer = Barry Baeres Anne Kopelson Arnold Kopelson Florina Massbaum Linne Radmin
| starring = Ashley Judd Samuel L. Jackson Andy García
| music = Mark Isham
| cinematography = Peter Deming Peter Boyle
| studio = Kopelson Entertainment
| distributor = Paramount Pictures
| released =  
| runtime = 97 minutes
| country = United States
| language = English
| budget = $50 million 
| gross = $41 million 
}}
Twisted is a 2004 American thriller film written by Sarah Thorp and directed by Philip Kaufman. It stars Ashley Judd, Samuel L. Jackson and Andy García. The film is set in San Francisco, California.

==Plot==
Having solved a high-profile case involving a serial killer, Jessica Shepard (Ashley Judd) is a rising officer in the San Francisco Police Department. She is transferred to the homicide division and promoted to the rank of Inspector#Municipal police|inspector. Her deceased fathers former partner, John Mills (Samuel L. Jackson), serves as her proud mentor. Shepard finds that she might once again have to prove herself in a department that takes no prisoners.

When one of her former one-night stands is murdered, Shepard and her new partner, Mike Delmarco (Andy García), are assigned to the case. Shepard, who has a drinking problem, soon falls under suspicion. Three more murders follow, each victim having had a relationship with her.

Shepard begins to experience a mental breakdown, blacking out for increasingly long periods of time. Her father, a police patrolman, had gone on a killing spree back in the 1970s and then murdered her mother. She begins to fear that she has the same violent tendencies and that she has been committing murder in her disoriented state.

Shepard finds out that Mills is the killer. He killed all of her lovers, as well as her parents, because he considered it his mission to prevent her growing up to be a dissolute woman like her mother. As her fathers partner, Mills had felt the responsibility to inform him that his wife was a nymphomaniac, which drove him insane.

As he had an illicit affair with Shepards mother, Mills felt the need to kill her lovers. Also ashamed that he destroyed his partners marriage and drove him insane, Mills decided to put him out of his misery by killing him.

Shepard secretly transmits Mills confession on a mobile phone, allowing her old partner to track them down. When Mills tries to shoot her partner, Shepard shoots him in the chest and he falls off the dock. The film closes on him drifting on the ocean surface surrounded by sea lions as the cops look on.

==Cast==
* Ashley Judd as Jessica Shepard
* Samuel L. Jackson as John Mills
* Andy García as Mike Delmarco
* David Strathairn as Dr. Melvin Frank
* Russell Wong as Lieutenant Tong
* Camryn Manheim as Lisa
* Mark Pellegrino as Jimmy Schmidt
* Titus Welliver as Dale Becker
* D.W. Moffett as Ray Porter
* Richard T. Jones as Wilson Jefferson
* Leland Orser as Edmund Cutler
* James Oliver Bullock as John Flanagan
* William Hall as Chip Marshall
* Joe Duer as Larry Geber
* Jim Hechim as Bob Sherman

==Reception==
Despite its well-established director and cast, Twisted received almost universally negative reviews, with a rating of 1% on Rotten Tomatoes based on 135 reviews with an average score of 2.9 out of 10. The consensus states: "An implausible, overheated potboiler that squanders a stellar cast, Twisted is a cliched, risible whodunit." The film is one of the lowest rated on the site.  One reviewer described the role-reversal of the film as "contrived", while allowing that the film "may dole out a few guilty pleasures". 

===Box office===
The film earned $25,198,598 in the United States and Canada and $15,756,005 in other territories for a combined worldwide gross of $40,954,603 —well below the films production budget of $50 million.

==See also==
* Yawara

==References==
  

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 