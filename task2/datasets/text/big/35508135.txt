The Reunion (2011 Danish film)
{{Infobox film
| name           = The Reunion
| image          = The Reunion (2011 Danish film) film poster.jpg
| caption        = Film poster
| director       = Niels Nørløv
| producer       = Rene Ezra Tomas Radoor
| writer         = Claudia Boderke Lars Mering
| starring       = Nicolaj Kopernikus
| music          = 
| cinematography = Rasmus Arrildt
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}

The Reunion ( ) is a 2011 Danish comedy film directed by Niels Nørløv.    

==Cast==
* Nicolaj Kopernikus as Niels
* Anders W. Berthelsen as Andreas
* Troels Lyby as Thomas
* Therese Glahn as Hanne
* Camilla Søeberg as Jette
* Mira Wanting as Simone
* Lene Nystrøm as Eva
* Troels Malling Thaarup as Ole (as Troels Malling)
* Brian Lykke as Tom
* Mia Nielsen-Jexen as Sanne (as Mia Jexen)
* Signe Skov as Lærke
* Søren Bregendal as Carsten

==References==
 

==External links==
*  

 
 
 
 
 
 
 