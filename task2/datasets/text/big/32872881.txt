Waltzing Matilda (film)
 
 
{{Infobox film
| name           = Waltzing Matilda
| image          =
| image_size     =
| caption        =
| director       = Pat Hanna associate Raymond Longford "Raymond Longford", Cinema Papers, January 1974 p51 
| producer       = Pat Hanna
| writer         = Pat Hanna John P. McLeod George Breston
| narrator       =
| starring       = Pat Hanna Coral Browne
| music          = 
| cinematography = Arthur Higgins
| editing        =
| studio = Pat Hanna Productions
| distributor    =Universal Pictures 
| released       =2 December 1933
| runtime        = 77 mins
| country        = Australia
| language       = English
| budget         =
| gross          =£8,000 "Counting the Cash in Australian Films", Everyones 12 December 1934 p 19 
| preceded_by    =
| followed_by    =
| website        =
}}

Waltzing Matilda is a 1933 Australian film starring and directed by Pat Hanna.  It features Coral Browne in an early role. 

==Plot==
Chic Williams and his friend James Brown wrongly believe they have injured a policeman in a drunken fight. Fleeing a private detective who is following them, they head for the bush.

Chic and James find works as drovers at Banjaroo Station, where his old army mate Joe works as an overseer. James falls in love with the station owners daughter. The detective arrives and tells James he has inherited money. James and his girlfriend announce their engagement and Chic sets off alone as a swagman, accompanied by a chorus of Waltzing Matilda. 

== Cast ==
*Pat Hanna as Chic Williams
*Joe Valli as Joe McTavish
*Frield Fisher as Albert
*Coral Browne		
*Norman French as James Brown	
*Joan Lang 	
*Nellie Mortyne		
*Dorothy Parnham as Dorothy Young
*George Moon
*Bill Innes

==Production==
Pat Hanna held a competition for the name of the film. A prize of ₤10 was shared amongst the 20 people who suggested "Waltzing Matilda". 

The character of Chic William was well established on stage and in Pat Hannas previous movies but changed for this film- he is poor, drinking heavily and drifting from job to job. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 163. 

The movie was shot at Efftee Studios in Melbourne in mid 1933. 

The romantic male lead, Norman French, was a casting director for Efftee and Hanna. 

==Reception==
The film only ran for a short time in the cities but enjoyed more popularity in the country.  Reviews were mixed.  It was classified as a "middle grade" success at the box office. 

Hanna was frustrated at the financial return he was getting for his efforts and this was his last film.  However in 1950 he said he was still making money by re-releasing his films. 

==References==
 

== External links ==
* 
*  at Oz Movies

 
 
 
 