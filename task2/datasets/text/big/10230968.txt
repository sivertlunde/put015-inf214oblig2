Shola Aur Shabnam (1992 film)
 
{{Infobox film
| name           = Shola aur Shabnam
| image          = Shola aur Shabnam.jpg
| image_size     =
| caption        = Poster
| director       = David Dhawan
| producer       = Pahlaj Nihalani
| writer         = Anees Bazmee Rajeev Kaul Praful Parekh
| narrator       =  Govinda Divya Bharati
| music          = Bappi Lahiri
| cinematography = 
| editing        = David Dhawan
| distributor    = Chirangdeep International
| released       =  
| runtime        = 175 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
|}}
 1992 Bollywood Govinda and Divya Bharati. Shola aur Shabnam became a hit at the box office with the song "Tu Pagal Premi Awara" being applauded by the audience.

==Plot==
The spoiled and arrogant brother, Bali (Mohnish Behl), of notorious gangster, Kali Baba (Gulshan Grover), enlists in the National Cadet Corps, and wants to have his way with his fellow-students and trainer, Inder Mohan Lathi (Anupam Kher), with comical results. When he meets Divya Thapa (Divya Bharati) for the first time, he is smitten by her, and wants to marry her by hook or by crook. But Divya is in love with Karan (Govinda (actor)|Govinda), and both plan to marry each other. Circumstances act against them, and they flee together, with Kali Babas men, and Divyas Police Commissioner dad, Yashpal Thapa (Alok Nath), in hot pursuit, to an unknown destination, surrounded by a web of lies, deception, and no known resource to assist them. 

==Cast==
 Govinda as Karan
*Divya Bharati as Divya 
*Gulshan Grover as Kali Shankar
*Mohnish Behl as Baali (Kalis brother)
*Anupam Kher as Major Inder Mohan Lathi aka I.M.Lathi
*Raja Bundela as Raja Satyajeet as Satya (credited as Satyajit)
*Girish Malik as Girish (credited as Girish Mallic) Bindu as Girls Hostel Principal
*Alok Nath as Comm. Yashpal Thapa
*Mahavir Shah as Police Inspector Mahadev
*Govind Namdeo as Police Inspector Tiwari
*Sudha Chandran as Karans Sister
*Harish Patel as Velji (Kalis Brother in Law)
*Guddi Maruti as Guddi (Divyas friend)
*Reema Lagoo as Sharda Thapa
*Shakti Kapoor as Boxer Deva (Special Appearance)
*Archana Puran Singh as Boxer Devas Wife
*Mac Mohan as Jaichand

==Soundtrack==
 Anjaan and Govinda (actor)|Govinda. 

===Track listing===
{{Track listing
| extra_column = Performer(s)
| lyrics_credits = yes
| title1 =  Bole Bole Dil Mera Bole
| extra1 = Abhijeet Bhattacharya & Chorus Anjaan
| length1 = 6:49
| title2 = Gori Gori O Baanki Chhori Govinda
| Anjaan
| length2 = 7:12
| title3 = Jane De Jane De Mujhe Jane De
| extra3 = Amit Kumar, Kavita Krishnamurthy Anjaan
| length3 = 5:36
| title4 = Tere Mere Pyar Mein Shailendra Singh Govinda
| length4 = 7:08
| title5 = Tu Pagal Premi Awara
| extra5 = Shabbir Kumar, Kavita Krishnamurthy Anjaan
| length5 = 8:31
}}

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 