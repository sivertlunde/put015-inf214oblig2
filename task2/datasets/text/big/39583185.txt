Ms Lekha Tharoor Kaanunnathu
{{Infobox film
| name           = Ms.Lekha Tharoor Kanunnathu
| starring       = {{Plainlist|
* Meera Jasmine
* Badri Shankar
* Suraj Venjarammoodu
* Nandhu
* Geetha Vijayan
* Sunil Sughada
}}
| music          = Ramesh Narayan
| cinematography = Chandra Mouli
| studio         = Saranam Pictures
| distributor    = Saranam Pictures Release
| released       =  
| runtime        = 137 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} The Eye. The movie features stars such as Meera Jasmine, Badri, Shankar Panicker|Shankar, Suraj Venjarammoodu, Nandhu and Geetha Vijayan. 

==Plot==
Television host Lekha Tharoor (Meera Jasmine) unexpectedly begins experiencing disturbing, violent visions. After explaining her mental state to friends and colleagues, she is presumed psychotic and is referred to a psychiatric hospital for treatment.

==Cast==
* Meera Jasmine as Lekha Tharoor Badri as Doctor Alex Shankar
* Suraj Venjarammoodu
* Nandhu as Nandagopalan Nair
* Geetha Vijayan as Muthulakshmi
* Nilambur Ayisha 
* Sunil Sukhada Krishna
* Jose
* Rosin Jolly as Veni
* Asha
* Arun Ghosh

==Production==
This is the second film by Shajiyem, who has been making television since it was introduced to Kerala|Kerala, India.    He directed the second television serial to air in Malayalam, Deva Manohari, which was a revision of a story by Ashita. Shajiyems American Dreams won the 2004 Kerala State Award for Best Television Serial.  The directors first feature film, Parasparam (1983), starred Nedumudi Venu and Zarina Wahab. 

Meera Jasmine, who began reappearing in films by late-2012 after a brief break,  plays the lead role in the film.    Shajiyem had the actress in mind when he was developing the character,  saying: "When she heard the story, she readily agreed to take the role and we eagerly awaited her available dates."  Badri, who was one of the heroes in Mamas Cinema Company (2012), plays the male lead. 

The film is set in the city of Kochi, and was filmed in Kochi, Pollachi and Palakkad.  Shooting ended in late May 2013.   

==References==
 

 
 
 
 
 
 