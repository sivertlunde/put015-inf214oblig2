Quadrophenia (film)
 
 
{{Infobox film
| name           = Quadrophenia
| image          = Quadrophenia_movie.jpg
| caption        = UK poster
| director       = Franc Roddam
| producer       = Roy Baird Bill Curbishley
| writer         = Dave Humphries Franc Roddam Martin Stellman Pete Townshend Toyah Phil Philip Davis Sting Ray Raymond Winstone
| cinematography = Brian Tufano
| editing        = Sean Barton Mike Taylor
| distributor    = The Who Films
| released       =  
| music          = The Who Various Artists
| runtime        = 117 min.
| budget         = £2 million  
| box office     = £14 million 
| country        = United Kingdom
| language       = English
}} of the Toyah and, towards the end of the film, Sting (musician)|Sting. It was directed by Franc Roddam in his feature directing début. Unlike the film adaptation of Tommy (1975 film)|Tommy, Quadrophenia is not a musical film.

==Plot== Philip Davis) and Spider (Gary Shail).  One of the Mods rivals, the Rockers, is in fact Jimmys childhood friend, Kevin (Ray Winstone). An assault by aggressive Rockers on Spider leads to a retaliation attack on Kevin. Jimmy participates in the assault, but when he realises the victim is Kevin, he doesnt help him, instead driving away on his scooter.

A bank holiday weekend provides the excuse for the rivalry between Mods and Rockers to come to a head, as they both descend upon the seaside town of Brighton.  A series of running battles ensues.  As the police close in on the rioters, Jimmy escapes down an alleyway with Steph (Leslie Ash)&nbsp;– a girl on whom he has a crush&nbsp;– and they have sex.  When the pair emerge, they find themselves in the middle of the melee just as police are detaining rioters.  Jimmy is arrested, detained with a violent, charismatic Mod he calls Ace Face (Sting (musician)|Sting), and later fined the then-large sum of £50.  When fined £75, Ace Face mocks the magistrate by offering to pay on the spot, to the amusement of fellow Mods.

Back in London, Jimmy becomes increasingly depressed.  He is thrown out of his house by his mother, who finds his stash of amphetamine pills.  He then quits his job, spends his severance package on more pills, and finds out that Steph has become the girlfriend of his friend Dave.  After a brief fight with Dave, the following morning his rejection is confirmed by Steph and with his beloved Lambretta scooter accidentally destroyed, Jimmy takes a train back to Brighton.  In an attempt to relive the recent excitement, he revisits the scenes of the riots and of his encounter with Steph.  To his horror, Jimmy discovers that his idol, Ace Face, is in reality a lowly bellboy at a Brighton hotel.  Jimmy steals Aces scooter and heads out to Beachy Head, crashing the scooter over a cliff, which is where the film begins with Jimmy walking back from the cliff top in the sunset back drop.

==Cast==
* Phil Daniels as Jimmy Cooper
* Leslie Ash as Steph Philip Davis as Chalky
* Mark Wingett as Dave Sting as Ace Face Raymond Winstone as Kevin Herriot
* Gary Shail as Spider
* Garry Cooper as Peter Fenton, Stephs boyfriend
* Toyah Willcox as Monkey
* Trevor Laird as Ferdy
* Andy Sayce as Kenny Kate Williams as Mrs Cooper / Jimmys mother
* Michael Elphick as Mr George Cooper / Jimmys father
* Kim Neve as Yvonne Cooper / Jimmys sister
* Benjamin Whitrow as Mr. Fulford / Jimmys employer
* Daniel Peacock as Danny
* Jeremy Child as Agency Man John Phillips as Magistrate
* Timothy Spall as Harry the Projectionist Patrick Murray as Des the projectionist assistant
* George Innes as Cafe Owner
* John Bindon as Harry North, gangster
* P. H. Moriarty as Barman at Villain Club
* Hugh Lloyd as Mr. Cale
* Gary Holton as aggressive Rocker 1 John Altman as Johnny John the Mod Fagin
* Jesse Birdsall as aggressive Rocker 2
* Oliver Pierre as Jimmy and Dannys tailor
* Julian Firth as drugged up Mod
* Simon Gipps-Kent as party host
* Mickey Royce as Ken Jonesy Jones James Lombard as Nicky
* Introducing Cross Section

John Lydon (Johnny Rotten of the Sex Pistols) screen-tested for the role of Jimmy. However, the distributors of the film refused to insure him for the part and he was replaced by Phil Daniels.  
{{cite book last = Catterall first = Ali last2 = Wells first2 = Simon title = Your Face Here: British Cult Movies Since the Sixties
  |publisher=HarperCollins UK year = 2002 isbn = 978-0-00-714554-6}} 

Ray Winstone, Phil Daniels, P. H. Moriarty and Julian Firth all appeared in the film Scum (film)|Scum, after filming Quadrophenia.

Michael Elphick played an ageing Rocker in 1980s TV series Boon (TV series)|Boon.

Most of the cast were reunited after 28 years at Earls Court on 1 and 2 September 2007 as part of The Quadrophenia Reunion at the London Film & Comic Con run by Quadcon.co.uk.    Subsequently the cast agreed to be part of a Quadrophenia Convention at Brighton in 2009. 

Spall and Daniels also appeared together in Chicken Run with Spall voicing Nick and Daniels voicing Fetcher, along with Whitrow, who voiced Fowler.

==Soundtrack==
 

==Production notes==
 

Several references to The Who appear throughout the film, including an anachronistic inclusion of a repackaged Who album that was not available at the time, a clip of the band performing "Anyway, Anyhow, Anywhere" on the TV series Ready Steady Go!, pictures of the band and a "Maximum R&B" poster in Jimmys bedroom, and the inclusion of "My Generation" during a party gatecrashing scene. The film was almost cancelled when Keith Moon, the drummer for The Who, died, but in the words of Roddam, the producers, Roy Baird and Bill Curbishley, "held it together" and the film was made.

Only one scene in the whole film was shot in the studio; all others were on location. Beachy Head, where Jimmy may or may not have tried to kill himself at the end of the film, was the location of a real-life suicide that supposedly influenced the films ending.

The stunt coordinators underestimated the distance that the scooter would fly through the air after being driven off Beachy Head. Franc Roddam, who shot the scene from a helicopter, was almost hit.
 Jeff Dexter, America whose first major gig at "Implosion" at The Roundhouse, Chalk Farm, was the opening act to The Who on 20 December 1970.

==DVD releases==
Universal first released the film on DVD in 1999 with an 8-minute montage featurette. It used the VHS print, resulting in a much lower-quality video than expected.
 matted wide screen transfer, a commentary, several interviews, galleries, and a quiz. However, it was a shorter cut of the film, with several minutes of footage missing.
 Region 2 two-disc special edition. The film was digitally remastered and included a brand new commentary by Franc Roddam, Phil Daniels and Leslie Ash. Disc 2 features an hour-long documentary and a featurette with Roddam discussing the locations.  Unlike their previous DVD, it was the complete, longer version, and it was matted to the correct aspect ratio.

On 1 January 2012, The Criterion Collection hinted in their annual New Years drawing that they would be releasing a special edition version of this movie, presumably on both DVD and Blu-Ray formats.  This edition was released on 28 August 2012.

==References==
 
*Ali Catterall and Simon Wells, Your Face Here: British Cult Movies Since The Sixties (Fourth Estate, 2001) ISBN 0-00-714554-3

==External links==
 
 
*  
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 