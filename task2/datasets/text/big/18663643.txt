Signorinella
{{Infobox film
| name           = Signorinella
| image          = 
| caption        = 
| director       = Mario Mattoli
| producer       = Isidoro Broggi
| writer         = Aldo De Benedetti Marcello Marchesi Mario Mattoli
| starring       = Gino Bechi
| music          = 
| cinematography = Aldo Tonti
| editing        = Giuliana Attenni
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

Signorinella is a 1949 Italian comedy film directed by Mario Mattoli and starring Gino Bechi.

==Cast==
* Gino Bechi - Don Cesare Balestri
* Antonella Lualdi - Maria Censi
* Aroldo Tieri - Un ladro
* Ave Ninchi - Iris
* Aldo Silvani - Ernesto De Blasi
* Dina Sassoli - Carmela
* Vinicio Sofia - Modesto Rinaldi
* Ughetto Bertucci - Un ladro
* Aldo Bufi Landi - Bruno de Blasi
* Ada Dondini - Cesira, la governante
* Enzo Garinei - Assistante del notaio (as Vincenzo Garinei)
* Inga Gort - Monica
* Enrico Viarisio - Comm. Gegé Lapicella

==External links==
* 

 

 
 
 
 
 
 


 
 