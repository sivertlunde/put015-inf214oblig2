Drillbit (film)
{{Infobox film
| name           = Drillbit
| image          =
| image_size     =
| caption        =
| director       = Alex Chandon
| writer         = Alex Chandon
| narrator       = Jim Van Bebber
| protducer      = Alex Chandon
| starring       = Ben Befell Tom Cox Saul Brignell
| music          =
| cinematography = Alex Chandon
| editing        = Alex Chandon
| distributor    = SOI Film Entertainment.
| released       = 1992
| runtime        = 33 minutes
| country        = United Kingdom English
| budget         = £800 
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
}}
Drillbit is a low-budget short horror film directed by Alex Chandon and stars Ben Bethell, Tom Cox and Saul Brignell. 

==Plot==
An employee detects a pharmaceutical companys sinister plan within the executive office, he will be wiped out with his entire family. Only his son Brian survives the horrific attack, although he has a drill bit rammed into his  skull and recognizes only one thought: revenge!

==Cast==
* Ben Bethell		
* Saul Brignell		
* Bill Corbett William Bill Corbett as Zombie Raver
* Tom Cox		
* Just		
* Neil Keenan as Biker Gang Member / Aids Mutant
* Miranda Morten		
* Lino Raffa as Goon
* Dominic Hailstone
* Matt "Magnum The rat" Russell

==Production==
The film was narrated by Jim Van Bebber and the Special FX was created from Duncan Jarman and Dominic Hailstone.

==References==
 

==External links==
* 

 

 
 
 
 
 

 