Schlußakkord
{{Infobox film
| name           =  
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Detlef Sierck (Douglas Sirk)
| producer       = Bruno Duday
| writer         = 
| screenplay     = Kurt Heuser, Detlef Sierck
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Flatlist|
* Lil Dagover
* Willy Birgel
* Maria von Tasnady
}}
| music          = Kurt Schröder; classical music excerpts
| cinematography = Robert Baberske
| editing        =  UFA
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}} Nazi period, the first melodrama directed by Detlef Sierck, who later had a career in Hollywood as Douglas Sirk and specialised in melodramas. It was made under contract for   (UFA), stars Lil Dagover and Willy Birgel and also features Maria von Tasnady, and premièred in 1936. It shows stylistic features later developed by Sierck/Sirk and makes symbolic and thematic use of music.

==Production and release==
Production took place from February to April 1936.  The film had two premières, on June 27, 1936 at the annual cinema owners convention in Dresden and on July 24, 1936 at the Gloria-Palast in Berlin, after which it was placed on general release. Cinzia Romani, tr. Robert Connolly, Tainted Goddesses: Female Film Stars of the Third Reich, New York: Sarpedon, 1992, repr. Rome: Gremese, 2001, ISBN 9788873014638, p. 52.  Hake, p. 112. 

==Plot==
At a New Years Eve party in New York, Hanna Müller (Maria von Tasnady) is informed that her husband has been found dead in Central Park, presumably a suicide. The couple had left Germany because he had embezzled money. Meanwhile, the young son they left behind in an orphanage, Peter, is adopted by Erich Garvenberg (Willy Birgel), a famous conductor, and his wife Charlotte (Lil Dagover), who is having an affair with an astrologer, Gregor Carl-Otto. Hanna Müller goes to the orphanage to enquire after her son and Erich Garvenberg hires her as a nanny. They grow close through their love for the boy. Charlotte Garvenberg learns of Müllers husbands criminality and fires her. Müller returns to abduct her son, but Charlotte, who is being blackmailed by Carl-Otto, overdoses on morphine and dies. Müller administered the drug and is suspected of murder, but at the trial a maid reveals that Charlotte had said she was committing suicide. Hanna and Erich Garvenberg can now marry.  

==Partial cast list==
* Maria von Tasnady: Hanna Müller
* Willy Birgel: Erich Garvenberg
*   refused the role, saying she was too busy )
* Maria Koppenhöfer: Frau Freese, the maid
* Peter Bosse: Hannas son Peter
* Theodor Loos: Professor Obereit, the paediatrician
* Albert Lippert: Gregor Carl-Otto, an astrologer
* Kurt Meisel: Baron Salviany, Carl-Ottos friend
* Erich Ponto: judge
* Hella Graf: Frau Czerwonska
* Paul Otto: prosecutor
* Alexander Engel: Mr. Smith, landlord
* Eva Tinschmann: head nurse
* Walter Werner: Dr. Smedley, doctor in New York
* Carl Auen: New York criminal investigator
* Erich Bartels: court official
* Johannes Bergfeld: adoption notary
* Ursula Deinert: dancer
* Christa Mattner: Peters foster mother
* Erna Berger: soprano soloist
* Luise Willer: alto soloist
* Rudolf Watzke: bass soloist
* Hellmuth Melchert: tenor soloist  

==Themes and imagery==
The film contrasts American with German culture and "a decadent past" (the Weimar Republic) with a "healthy, hopeful present" (the Third Reich) that reaffirms the values of the "old" (pre-Weimar) Germany. Hake, p. 119.  The interiors, by Erich Kettelhut, a co-designer on Metropolis (1927 film)|Metropolis, have symbolic force;  in particular, Charlotte Garvenberg is surrounded by mirrors, suggesting narcissism, preoccupied with her own happiness at the expense of her husband or other integration into society, so that her fate in the film "in a way, rehearses the conditions under which   culture came to an end", in selfishness, "erotic obsessions" and "empty rituals".  In contrast Erich Garvenberg and Hanna are both guided by duty, and Garvenberg is a decisive leader and Hanna is able to draw strength from her rootedness in German culture and her healthy maternal feelings. 

Sierck stated in an interview that he saw melodrama in its original and etymological sense, as "music + drama".  In Schlußakkord, Kurt Schröders score is reminiscent in style of later work by Erich Korngold and incorporates several excerpts of classical music, including radio broadcasts and gramophone records. Symphony No. 9 (Beethoven)|Beethovens Ninth Symphony was performed for the soundtrack by the orchestra of the Berlin State Opera with well-known soloists including Hellmuth Melchert and Erna Berger. 
 swing are played at the New York New Years Eve party and a party given by Charlotte; Charlotte is late to a performance by her husband of Beethovens Ninth Symphony, fails to gain entrance and back home exclaims to her maid, " ometimes he is so foreign to me. Always with Bach, Beethoven, and whatever their names are"; while in an interwoven scene, an ailing Hanna in New York, hearing on the radio the notes of Beethovens Ninth Symphony which Garvenberg is conducting, whispers "Beethoven", remembers Germany and decides to return to her homeland, whereupon the scene shifts back to the concert hall, where the performance has reached the "Ode to Joy". The sequence contrasts Charlottes estrangement in Berlin with the expatriate Hannas need to belong (and to be reunited with her child).   In the scene where they discover they love each other, Hanna tells Garvenberg that the Adagio movement saved her life.  Other passages of classical music serve as leitmotifs in the film. A passage from The Nutcracker|Tchaikovskys Nutcracker Suite is introduced in the opening credits and recurs "repeatedly . . . each time announcing an emotional crisis or insight." A theme from "Dance of the Toy Flutes" first occurs as Hanna mentions her child when the police in New York are questioning her, and the music changes to "lyrical and pastoral tones" as the viewer sees in turn New York tenements, the lighted skyline of Manhattan, the Atlantic, the historic centre of Berlin, and finally the boy in the orphanage.  The theme recurs when Hanna gives Charlotte her medicine and when she dreams about that last encounter, foreshadowing that she will be reunited with her son. Hake, p. 116.  Other Nutcracker passages occur when Hanna is at the theatre with the director of the orphanage and are intercut with scenes of Charlotte and her lover;  and after an argument with Charlotte, Hanna goes to an opera in the style of Richard Strauss, where an older woman sings an aria, "Drop of Hemlock, Sweet and Deadly," expressing Hannas fear of elimination, but it is a younger woman who is poisoned on stage.  Finally, the closing scene is at a performance of Judas Maccabaeus (Handel)|Handels oratorio Judas Maccabaeus, and the camera moves from the newly united family to the triumphant angels on the ceiling of the concert hall. 

Sabine Hake points out that in addition to expressing the deepest feelings of the characters, the use of music in the film establishes the "social, psychological, and cultural terms" in which it defines community, and that the use of classical music quotations as well as the films visual symbolism presuppose a shared middle-class cultural frame of reference. 

==Reception==
The film was successful and strengthened Siercks negotiating position with UFA.   The Film-Kurier review praised Sierck for "manag  to blend the various emotional and affective elements of the plot into a moving musical unity" with "appropriate emphases" and "sustaining dramatic tension from start to finish."  Schneider in Licht-Bild-Bühne called it " he most honest, most decent and, in its form, most compelling film of   years."  Another Berlin critic wrote that "Sierck . . . shows with this film that he ranks with the most important contemporary filmmakers" and singled out in particular his not favouring some "stars" over other actors: " ll his actors are stars from the moment they appear on the screen."  However, most of the reviews focussed on the stars Willy Birgel and Lil Dagover rather than on the direction. 

In 1969, David Stewart Hull wrote that the film was "done with much the same flair which Sierck (Douglas Sirk) was to evidence . . . two decades later in the United States" but also that "the excellent musical sequences saved the film from banality." David Stewart Hull, Film in the Third Reich: A Study of the German Cinema, 1933–1945, Berkeley, California: University of California, 1969,  , p.&nbsp;103. 

==Awards== Propaganda Ministry award of distinction): Artistic Value (künstlerisch Wertvoll)  Hake, p. 115.  
* Best Musical Film, Venice International Film Festival   

==Accord final==
In 1939, Sierck made Accord final in France for France-Suisse Film; this is the same title as Schlußakkord but the plot is different.   In French, unlike German, the title is ambiguous. 

==References==
 

==Further reading==
* Linda Schulte-Sasse. "Douglas Sirks Schlußakkord and the Question of Aesthetic Resistance". The Germanic Review 73.1, 1998. pp.&nbsp;2–31.  .  .
* Andrew G. Bonnell. "Melodrama for the Master Race: Two Films by Detlef Sierck (Douglas Sirk)". Film History 10.2, 1998. pp.&nbsp;208–18.  .

==External links==
 

 
 
 
 