The Big Gay Musical
{{Infobox Film
| name       = The Big Gay Musical
| image      = TheBigGayMusical2.jpg
| image_size = 
| alt        = 
| caption    = Theatrical release poster
| director   = Casper Andreas Fred M. Caruso
| producer   = Fred M. Caruso Casper Andreas Jay Arnold Tom DAngora Michael Duling Edward Miller (executive)
| writer     = Fred M. Caruso
| starring   = Lena Hall Daniel Robinson Joey Dudding Jeff Metzler Casper Andreas
| music      = Rick Crom
| cinematography = Jon Fordham
| editing    = Alexander Hammer
| Studio     = Big Gay Musical Productions
| distributor= Embrem Entertainment  (USA DVD) 
| released   =  
| runtime    = 
| country    = United States
| language   = English
| budget     = 
| gross      = 
}}
 promiscuous or come out to his Conservatism|conservative, religious parents. 
 Genesis story,  protests from Televangelism|televangelists, a deprogramming camp that tries to turn gay kids straight. By the end of the film, the characters realize that life would be better if they just accepted themselves the way they are. 

==Plot== conservative parents that he is gay. Eddie comes out to his family, who do not accept the news well. Paul turns to the Internet for dates, but cant even get a decent one-night stand. However, Paul continually bumps into a fan who has developed romantic feelings for him, and after a series of disastrous one night stands, he starts dating Michael at the end of the musicals opening night. Eddies religious parents, on a different note, decide to attend the musical because of a promise they made to their son, even though they are appalled by homosexuality and the theme of the show. As the play goes on though, they begin to believe that maybe they judged their son too harshly, and come to accept the fact that he is gay. Eddies parents make up with him at the end of the shows opening night.

==Partial cast==
* Lena Hall as Wife / Eve
* Daniel Robinson as Paul / Adam
* Joey Dudding as Eddie / Steve
* Jeff Metzler as David
* Casper Andreas as Usher
* Liz McCartney as Patty-Maye
* Brian Spitulnik as Michael
* Andre Ward as Jose
* Steve Hayes as God
* Jim Newman as Bruce
* Michael Schiffman as Charles
* Marty Thomas as Dorothy
* Kate Pazakis as Herself
* Michael Musto as Himself
* Jack Aaronson as Himself Brent Corrigan as Hustler
* Rick Crom as Drunk

==Production==
The directors chose to cast openly gay Broadway actors for all the films key roles.   Principle filming began October 24, 2008. Cast includes theatre veterans, Liz McCartney, Jim Newman, Joey Dudding, Marty Thomas, Andre Ward, Daniel Robinson, Jeff Metzler, Brian Spitulnik, Josh Cruz, Lena Hall and Steve Hayes, and features the choreography of Shea Sullivan with songs written by Rick Crom.    It was the first feature film for stage actors Daniel Robinson and Joey Dudding. The two leads were at first concerned with the manner in which sex scenes were to be handled in the film, with Robinson originally turning down the role. He is reported as stating, "It was a little too over the top for me. I wasnt comfortable with the sex scene  and the nudity. It was too nude and gay for me."  He expanded, "A huge reason why I didnt take the film in the first place was that I didnt want to be seen as a gay actor. Big Gay Musical was like putting a tattoo on my body."  He eventually decided the film was a good opportunity, and in looking back on the production, acknowledged that the sex scene "was harder to read than to do."  Another of his early concerns were fears about having his naked body exposed on film forever. Robinson and Dudding both offered that the most difficult aspect to their respective roles was portraying two different characters with different story lines and motivations within the same film.   

==Critical response==
In speaking about the film, Variety (magazine)|Variety wrote of one of the films many segments, and made special note of one dealing with Genesis creation narrative|Genesis, by writing that "Seldom has blasphemy been so entertaining, and if only Caruso were capable of sustaining such wit (and energy, as agile lensing and editing keep things lively), the movie could have stuck to documenting his stage show."  They expanded on flaws by writing "subsequent numbers stall, with lame caricatures of Tammy Faye Bakker and long stretches at an ex-gay conversion camp offset by an eye-candy male cast parading about in hot pants and angel wings."  The added that to the films credit, "the directors insisted on casting openly gay Broadway actors in all the key roles, trading the usual daytime-soap-caliber cast of equivalently low-budget, L.A.-produced gay fare for multitalents with real singing and dancing chops." They concluded that the film was "more than adequate for festival and DVD consumption."   

After Elton shared that the films structure is "a little unusual" in that after the prologue the film opens with "a rather extended musical and dance number from a preview performance of a stage play, Adam & Steve Just the Way God Made Em, which tells the story of the Bible from a decidedly gay perspective." They called the film "a surprisingly pleasant romp," writing that "this little gay musical has what seems to be about two full months of flawless preview performances!"   

New York Cool reviewer Frank J. Avella wrote that his first response was negative, but when he read of the involvement of Casper Andreas in the project he became more interested in the film and learned, contrary to his original impression, "no one breaks into song without a good reason."   Avella made special note of how the storyline revolves around the players and cast of the stageplay Adam and Steve Just the Way God Made Them, a musical production within the film itself which presents the Genesis creation narrative from a gay perspective. He wrote "The film continuously and cleverly returns to the stage show, using it as a framing device of sorts" as a means to underscore the films greater story of acceptance-of-self. In summarizing, the reviewer offered that "production values are terrific and the ensemble is, for the most part, admirable".    He wrote that Daniel Robinson was "quite impressive" and his slut song "brings down the house", but expanded that Joey Dudding playing a "sweet and sensitive Eddie" truly moves the audience by giving an accurate portrayal of a guy on the cusp of coming out.   

==Release== Philadelphia List Q Fest and had its official theatrical premiere August 9, 2009, in Provincetown, Massachusetts,    before screenings at multiple LGBT film festivals through 2009 and 2010.

==Soundtrack==
The soundtrack of the film was scheduled for release on July 14, 2009. The CD includes all the original music from the film as well as some contemporary tracks that were also featured in the movie.     
 
 
:1, "Overture"
:2, "Creation"
:3, "The Party Isnt Over"
:4, "Eves Lament"
:5, "Christian Medley"
:6, "I Will Change"
:7, "I Wanna Be a Slut"
:8, "Im Gonna Go Straight"
 
:9, "Someone to Sing Me a Love Song"
:10, "Musical Theatre Love"
:11, "Someone Up There"
:12, "God Loves Gays"
:13, "As I AM"
:14, "Finale"
:15, "I Am Alone"
 

==References==
 

==Further reading==
*   San Francisco Bay Times. November 13, 2008. Accessed 01-21-2009.
*   TheaterMania.com. October 23, 2008. Accessed 01-21-2009.
*   October 4, 2008. Accessed 01-21-2009.
* Padva, Gilad. Uses of Nostalgia in Musical Politicization of Homo/Phobic Myths in Were the World Mine, The Big Gay Musical, and Zero Patience. In Padva, Gilad, Queer Nostalgia in Cinema and Pop Culture, pp.&nbsp;139–172 (Palgrave Macmillan, 2014, ISBN 978-1-137-26633-0).

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 