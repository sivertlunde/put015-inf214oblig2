Jennie Gerhardt (film)
 
{{Infobox film
| name           = Jennie Gerhardt 
| image          = Jennie Gerhardt  1933.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Marion Gering
| producer       = B.P. Schulberg
| writer         = 
| screenplay     =S.K. Lauren, Josephine Lovett, Joseph Moncure March, Frank Partos 
| story          = 
| based on       = the novel Jennie Gerhardt by Theodore Dreiser 
| starring       = 
| narrator       =  Howard Jackson and John Leipold (all uncredited)
| cinematography = Leon Shamroy
| editing        = Eda Warren
| studio         = B. P Schulberg Productions, Paramount Pictures
| distributor    = 
| released       =   
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Edward Arnold. The film is based on the 1911 novel Jennie Gerhardt by Theodore Dreiser.   

==Cast==
*Sylvia Sidney as Jennie Gerhardt Donald Cook as Lester Kane
*Mary Astor as Letty Pace Edward Arnold as Sen. Brander
*H.B. Warner as William Gerhardt

==Reception==
The film received a mixed critical reception upon release in June 1933. Motion Picture Herald said that Jennie Gerhardt is "strictly an adult picture. Selling it successfully depends upon your ability to construct campaigns that will intrigue human interest and sympathy in the character of Jennie." He added that despite its large cast, much of the action was centred about Sylvia Sidney.   Mordaunt Hall of The New York Times stated that "although the film is handicapped by a lack of suspense and some choppy and trite dialogue, it possesses a laudable sincerity, which may result in its being popular with those who are partial to this genre of story." He further said: "Actually the producers have succeeded in putting into the picture a wealth of detail and the pity is that it is done without the necessary suspense. It is like a story told in a monotone, with vacillating characters who are a little too much at the beck and call of the director. The incidents, while not implausible, do not possess sufficient spontaneity.  

Keith Newlin, author of  A Theodore Dreiser Encyclopedia, described it as an "episodic picture (broken into a number of short scenes)" and considered it to be more like the novel than most other Dreiser film adaptations. However, he believed that Gering had inexplicably "inserted several jarring scenes apparently in an attempt to provide the audience with some comic relief."    Dreiser himself approved of the film, considering it to be "moving" and "beautifully interpreted".    

==References==
 

==External links==
*  

 

 
 
 
 
 
 