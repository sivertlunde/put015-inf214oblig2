Prothom Kadam Phool
{{Infobox film
| name = Prothom Kadam Phool   প্রথম কদম ফুল
| image =
| image size = 
| caption = Poster of Prothom Kadam Phool
| director = Inder Sen
| producer = 
| writer = Achintya Sengupta
| starring = Soumitra Chatterjee   Tanuja
| music = Sudhin Dasgupta
| editing = 
| released = 31 December 1969
| runtime = 70 minutes
| country = India Bengali
}}
Prothom Kadam Phool (  and Soumitra Chatterjee in the lead.  

==Plot==

Sukanta meets kakali in a library reading room and discovers his passion for her though lrejected initially by Kakali. Prantas wife i.e the sister in law of Sukanta has susected the truth yet she does not dare to encourage the affair since Sukanta is not established. Again on another day both Kakali and Sukanta become stranded in a lift of the library for long hours for the want of power. Here they come closer to each other now they start meeting frequently here and there. Once they enjoy a picnic together along with the friends of sukanta .One day sukanta comes to the house of kakali but her parents do not approve of her. After a long interval Sukanta meets Baren his school friend who is now very well off. Baren works in a european firm in a high post He has his own four wheeler and a handsome flat where he stays with his widowed mother. When Sukanta visits her Barens mother expresses her wish to give her son a pleasant bride. Kakali one day comes to the house of sukanta. Her maiden appearance moved every single member of Sukantas family. In fact mother Sukanta want them to marry. The parents of Kakali refuse. So kakali left the house and property of her father . Kakali joins Baren as a receptionist. And trouble starts in Sukantas family. Sukanta the research scolar misjudges the relation of Kakali and Baren .Kakali leaves one day after a heated exchange and takes. The nephew of Sukanta who loves his aunt misses her .He goes missing. . In the meantime Baren comes to everything from Kakali and he takes the initiative to solve. Ultimately Kakali comes to the police station where a happy reunion takes place.

==Cast==
*Soumitra Chatterjee
*Tanuja
*Subhendu Chatterjee
*Chhaya Devi
*Shamit Bhanja
*Anubha Ghosh
*Padma Debi
*Bhanu Banerjee

==Soundtrack==
The films soundtrack has been scored by Sudhin Dasgupta and three out of four songs were written by him. Pulak Banerjee wrote the song "Ami Shri Shri Bhojohori Manna". Asha Bhosle and Manna Dey sang the songs. 

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#ccc; text-align:center;"
! #!!Title !! Singer(s) !! Duration
|-
| 1
| "Kon Se Alor Swapno Niye"
| Asha Bhosle || 3:19 
|-
| 2
| "Ei Shahar Theke Aaro Onek Dure"
| Manna Dey || 3:35 
|-
| 3
| "Deke Deke Chole Gechhi" 
| Asha Bhosle || 3:15 
|-
| 4 
| "Ami Shri Shri Bhojo Hori Manna"
| Manna Dey || 3:04
|-
|}

 

==References==
 

 
 
 
 

 