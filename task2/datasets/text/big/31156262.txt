Manushyabandhangal
{{Infobox film 
| name           = Manushyabandhangal
| image          =
| caption        =
| director       = Crossbelt Mani
| producer       =
| writer         = MK Mani
| screenplay     = MK Mani Madhu Sheela Jayabharathi
| music          = V. Dakshinamoorthy
| cinematography = P Ramaswami
| editing        = G Venkittaraman
| studio         = Karthika Films
| distributor    = Karthika Films
| released       =  
| country        = India Malayalam
}}
 1972 Cinema Indian Malayalam Malayalam film, directed by Crossbelt Mani . The film stars Prem Nazir, Madhu (actor)|Madhu, Sheela and Jayabharathi in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
 
*Prem Nazir Madhu
*Sheela
*Jayabharathi
*Adoor Bhasi
*NN Pillai
*P. J. Antony
*CA Balan
*Girish Kumar
*JAR Anand
*Lakshmi Amma
*Nambiar
*Paravoor Bharathan
*Ramankutty
*S. P. Pillai Sujatha
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ezhu Sundara Kanyakamaar || K. J. Yesudas || P. Bhaskaran || 
|-
| 2 || Kanakaswapnangal || K. J. Yesudas, P Susheela, P Jayachandran, LR Eeswari || P. Bhaskaran || 
|-
| 3 || Maasam Poovani Maasam || K. J. Yesudas || P. Bhaskaran || 
|-
| 4 || Manushya Bandhangal || K. J. Yesudas || P. Bhaskaran || 
|-
| 5 || Mizhiyillengilum || P Susheela || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 