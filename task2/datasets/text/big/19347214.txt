Small Talk (film)
{{Infobox film
| name           = Small Talk
| image          = Small talk TITLE.JPEG
| image_size     = 
| caption        = 
| director       = Robert F. McGowan
| producer       = Robert F. McGowan Hal Roach
| writer         = Robert F. McGowan H. M. Walker
| narrator       = 
| starring       = Bobby Hutchins   Mary Ann Jackson   Joe Cobb   Allen Hoskins   Jean Darling   Harry Spear
| music          = 
| cinematography = Art Lloyd
| editing        = Richard C. Currier MGM
| released       =  
| runtime        = 24:57 
| country        = United States 
| language       = English
| budget         = 
}}
Small Talk is a 1929 Our Gang short comedy film directed by Robert F. McGowan.    Produced by Hal Roach and released to theaters by Metro-Goldwyn-Mayer, it was the 86th entry in the Our Gang series to be released, and the first to be made with film sound|sound.   

==Plot==
The gang are all orphans, hoping to be adopted by nice families where "spinach is not on the menu". Wheezer, the youngest child, gets adopted by a wealthy couple, while his older sister Mary Ann does not. The gang all comes to visit Wheezer in his new home, setting off an alarm that causes the police and the fire department to come over. At that time, Wheezers new mother and father decide to adopt Mary Ann as well. The couples friends all each adopt a child as well; even Farina is adopted by the maid at Wheezers new home.

==Notes== Little Mother, Cat, Dog & Co. and Saturdays Lesson—would be released afterwards.
*Small Talk was originally part of the Little Rascals television syndication package for the 1950s until the 1980s, when it was dropped from the package over concerns about its length and sound quality.
*Besides the emphasis throughout on the banal, even annoying sounds and noises of every day life, one scene in particular dwells on the frightening, supernatural aspects of then-contemporary life. Farina encounters a whistling clock (ironically never spotting its Negro form), then is progressively frightened by an undead (robotic) caged bird whistling, a real parrot talking, and a mechanical music box that plays music when he opens it.

==Quotes==
**Mary Ann Jackson: "Oh darn it! Why do they have to make orphans and spinach for anyhow?"

==Cast==
===The Gang===
* Joe Cobb as Joe
* Jean Darling as Jean
* Allen Hoskins as Farina
* Bobby Hutchins as Wheezer
* Mary Ann Jackson as Mary Ann
* Harry Spear as Harry
* Pete the Pup as Himself

===Additional cast===
* Helen Jerome Eddy as Helen Eddy, Wheezers new mother
* Edith Fortier as Maid
* Pat Harmon as Policeman
* Charles McMurphy as Police chief
* Lyle Tayo as Mrs. Brown of the orphan asylum

==See also==
* Our Gang filmography

==References==
 

==External links==
* 
*http://theluckycorner.com/rmt/089.html

 
 
 
 
 
 
 
 
 


 