The Novel of Werther
{{Infobox film
| name = The Novel of Werther
| image =
| image_size =
| caption =
| director = Max Ophüls
| producer = Seymour Nebenzal Hans Wilhelm   Fernand Crommelynck    Max Ophüls
| narrator =
| starring = Pierre Richard-Willm   Annie Vernay   Jean Galland   Jean Périer 
| music = Paul Dessau  
| cinematography = Fédote Bourgasoff   Paul Portier   Eugen Schüfftan
| editing =Gérard Bensdorp   Jean Sacha
| studio = Nero Film
| distributor = Monopol Film
| released = 14 December 1938 
| runtime = 85 minutes
| country = France  French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical drama film directed by Max Ophüls and starring  Pierre Richard-Willm, Annie Vernay and Jean Galland. It is based on the 1774 novel The Sorrows of Young Werther by Johann Wolfgang von Goethe|Goethe. 

==Cast==
* Pierre Richard-Willm as Werther
* Annie Vernay as Charlotte
* Jean Galland as Albert
* Paulette Pax as la tante Emma
* Jean Périer as le président
* Edmond Beauchamp as le meurtrier
* Georges Bever as le chambellan
* Geno Ferny as le portraitiste
* Fernand Blot as le collègue de Werther
* Denise Kerny as la bonne
* Henri Guisol as Schertz, le greffier
* Roger Legris as Franz, le valet
* Jean Buquet as le petit Gustave
* Maurice Schutz as le sonneur
* Léonce Corne asle majordome
* Philippe Richard as le grand-duc
* Charles Nossent as le cocher
* Léon Larive as le cabaretier
* Georges Vitray as le bailli
* Génia Vaury as une fille
* Henri Beaulieu
* Henri Darbrey
* Pierre Darteuil
* Maurice Devienne
* Martial Rèbe
* Robert Rollis

== References ==
 

== Bibliography ==
* Gillespie, Gerald & Engel, Manfred & Dieterle, Bernard. Romantic Prose Fiction. John Benjamins Publishing, 2008. 

== External links ==
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 