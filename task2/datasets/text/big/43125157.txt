Alias French Gertie
 
{{Infobox film
| name           = Alias French Gertie
| image          = Alias_French_Gertie_Poster.jpg
| alt            = 
| caption        = Film Poster
| film name      = 
| director       = George Archainbaud    Henry Hobart   
| writer         =  Wallace Smith 
| story          = 
| based on       =   
| starring       = Bebe Daniels Ben Lyon 
| narrator       = 
| music          = 
| cinematography = J. Roy Hunt 
| editing        =  RKO Radio Pictures 
| distributor    = 
| released       =   }}
| runtime        = 66 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Wallace Smith, based upon the unproduced play, The Chatterbox by Bayard Veiller.  It starred Bebe Daniels and Ben Lyon, who were making their first on-screen appearance together. 

A copy of this film survives in the Library of Congress. 

==Plot==
Marie is a jewel thief.  Posing as French maid, she has cased out the safe of her employer and intends to steal its contents. However, the night she chooses for the robbery, there is another thief who also shows up to empty the safe, Jimmy.  Jimmy opens the safe, and the two agree to split the contents fifty-fifty.  They are interrupted by the arrival of the police. Jimmy gallantly secretes Marie away, and takes the rap himself, impressing her.

After serving his years sentence, Jimmy is reunited with Marie, who know goes by the alias of Gertie, and the two form a partnership in crime.  After several bank robberies, Marie and Jimmy agree that after one last haul, they will go straight.  Marie, who has become friends with the next-door neighbors in her apartment building, Mr. and Mrs. Matson, who entice Jimmy to invest his $30,000 savings in Matsons business.  Unfortunately, the Matsons turn out to be crooks themselves, and have swindled Jimmy out of his lifes savings.  

When Jimmy determines to go back to safecracking, beginning with Maries former employers, Marie hatches a plot to encourage him to go straight.  When a good-hearted detective, Kelcey, lets them off the hook with the promise that they will go straight, they agree.

==Cast==
* Bebe Daniels as Marie
* Ben Lyon as Jimmy
* Robert Emmett OConnor as Kelcey John Ince as Mr. Matson
* Daisy Belmore as Mrs. Matson
* Betty Pierce as Nellie

(Cast list as per AFI Film Database) 

==Reception==
The New York Times critic, Mordaunt Hall, gave the film a lukewarm review, praising the acting of Bebe Daniels, while not being as kind to Ben Lyon.  Overall, he said the film, "... has not been handled with the subtlety and smoothness it deserves.  Nevertheless, up to a certain point, it is a production that holds the interest, but what should have been the main idea is sacrificed for a more obvious turn of events."   

==Notes==
This is the first film in which Bebe Daniels and Ben Lyon co-starred.  They were married a short time after this, in June 1930, and the two remained married until her death in 1971.    

The play from which this screenplay was adapted, The Chatterbox, does not appear to have ever been produced. 
 FBO silent Bruce Gordon, and directed by Ralph Ince.   

==References==
 

==External links==
* 
* 

 
 
 
 
 
 