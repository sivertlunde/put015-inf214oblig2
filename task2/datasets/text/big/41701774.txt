La Comédie du bonheur
{{Infobox film
| name           = Le Comedie du Bonheur
| image          =
| caption        =
| director       = Marcel LHerbier
| producer       = 
| writer         = Gaetano Campanile-Mancini André Cerf Fernand Noziere
| based on = play “Samoe glavnoe” by Nicolas Evreinoff Jean Cocteau (additional dialogue)
| starring       = Michel Simon Ramon Novarro  Jacqueline Delubac Micheline Presle Louis Jourdan 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1940 (Italy) 1942 (Frances) 
| runtime        = 
| country        = France Italty
| awards         = French
| budget         = 
}}
Le Comedie du Bonheur (also known as Ecco La Felicità in Italy and Comedy of Happiness in England) is a 1940 French-Italian film. It was the first completed film of Louis Jourdan.

==Plot==
Wealthy banker Francois Jourdain starts spending his money on philanthropic purposes, so his relatives have him committed to a psychiatric clinic to save their inheritance. During Mardi Gras celebrations, Jourdain escapes and takes residence in a boarding house, which is inhabited by several miserable individuals, including Lydia, the suicidal Russian Fedor, and bitter old spinter Miss Aglae. He hires actors led by Deribin to take up residence and cheer up the inhabitants by putting on a musical production of Quo Vadis.

The lead juvenile Félix begins romancing Lydia to improve her self-esteem. Felixs wife Anita convinces Fédor that life is worth living and Deribin tries to cheer up Miss Aglaé. Jourdains family go looking for him. 

==Production==
The movie was based on a 1921 play and was shot at Scalera Studios in Rome.  It was one of the few films made outside Hollywood by Ramon Novarro. 

==References==
 
==External links==
*  at IMDB
*  at Monsieur Louis Jourdan
*  at Films de France

 
 
 
 

 