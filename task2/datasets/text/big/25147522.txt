Elle: A Modern Cinderella Tale
{{Infobox film
| name           = Elle: A Modern Cinderella Tale
| image          = ElleModernCinderellaFilmPoster.jpg
| alt            =  
| caption        = Promotional Poster
| director       = Sean Dunson John Dunson
| executive producer       = Oscar Solano Tony Distefano Natalie Bailess Tina Treadwell
| producer    =  Carlucci Weyant
| writer         = Bo Ransdell Ryan Dean Thomas R. Martin
| starring       = Ashlee Hewitt Sterling Knight Thomas Calabro Kiely Williams Katherine Bailess Tyler Nicole Emma Winkler Juliette Hing Shawn-Caulin Young
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Frame Of Mind Entertainment
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} teen musical musical comedy-drama film directed by John and Sean Dunson and starring Ashlee Hewitt and Sterling Knight. The screenplay was written by Bo Ransdell, Ryan Dean and Thomas Martin and produced by Sean Dunson, John Dunson and Carlucci Weyant.

Hewitt stars as Elle Daniels, an intern who dreams of becoming a singer. Hewitt announced to fans she was filming for the movie on June 20, 2009,  while Knights involvement was confirmed on March 5, 2010.  The film premiered on April 24, 2010, at the Newport Beach Film Festival.  It was released on DVD on April 26, 2011. 

==Plot==
Elle Daniels (Ashlee Hewitt) has dreamed of being a famous singer-songwriter for as long as she can remember. On the day of her audition for Berklee, the plane her parents are travelling in crashes. Crushed, Elle moves in with her uncle Allen (Thomas Calabro), who runs a small, independent record company called Spunn Records.

Allen offers Elle a job as an intern and even though this means supporting Spunns biggest act: a bubblegum pop trio called Sensation, led by the horrendous Stephanie (Katherine Bailess). Elle swallows her pride and tries to forget about her dreams. She becomes good friends with fellow intern Kit (Juliette Hing-Lee), and spends her days phoning radio stations and running errands.

But then Ty Parker (Sterling Knight), a famous pop singer extraordinaire, shows up at Spunn, saying that he needs something different, even if it means his fans shunning him. Allen agrees to set him up on a duet with Kandi Kane, a British pop star with "a million hits online" and Ty agrees to think about in search of something new and willing to take any risk it takes.

Elle, being convinced by Kit to start singing again, goes to the recording studio and sings "Love is with Me Now", which Ty hears, and mistakes her for the British Kandi Kane, awestruck by her voice. Surprised and pleased, Elle plays along, leaving at midnight when she receives a text from uncle Allen. After discovering that she accidentally over recorded the Sensation single and feeling guilty for lying to Ty, Elle goes into hiding, donning stupid disguises.

Ty, who is all jumpy about signing Spunn, is also shocked to discover that Kandi is anything but blonde on an awkward meeting set up for the two by Allen, and leaves in a fit of rage, snapping at Allen that this time, its about what he really wants.

Ty is determined to find the girl he met with the help of TJ (Brandon Mychal Smith) but fails. Stephanie discovers that the song was Elles and Sensation team up with Kandi to show Elle her place. On Kits advice, Elle decides to give true love a shot, and with Kits would-be boyfriend Andy (Shawn Caulin-Young)s help, she reveals herself through the song she performed in the studio. Ty hears her sing, and Elle is viciously insulted by Sensation and Kandi for being an obsessive fan. Instead of supporting them, Ty walks out and comforts the heartbroken Elle and they start dating. He also convinces her that her talent would never hurt her, and the death of her parents had nothing to do with her. Allen also tells Elle that he admired her parents very much and the song that she accidentally over-recorded was "beautiful."

All seems to be well and Ty convinces Elle to audition for Berklee again. They write a song "Fairytale" together, until Kandi returns for her claim on the single and emotionally tortures Ty to leave Elle or she would sue everyone related to him. Ty calls her a horrible person, but agrees to break up with Elle anyway to protect her.

Heartbroken, Elle misunderstands everything, which causes Kit and Andy to carry out an investigation where they hear the receptionist calling Kandi "Miss Smirkle." Although suspicious, Andys courage fails, but Kit kisses him for support and he mans up immediately.

Pretending to have a pastry delivery for her by the studio, Andy goes to Kandi, buying Kit time to break into the room, where she is shocked to unearth the fact that Kandi is in fact Brenda Smirkle, an average girl who is using Kandis identity and is anything but British.

After getting immense proof on tape, Kit reveals "Brendas" secret to Allen, who suggests that she leave before he calls the police. In the process, he also dumps Sensation, and Stephanie is revealed to be bald. Ty rushes to make Elles audition and arrives just as she is standing tongue-tied on the stage. He backs her up with a guitar and she blows the judges away by singing "Fairytale". She is immediately accepted to Berklee and forgives Ty after the performance. Both of them share a long awaited, heart felt kiss, where Elle narrates that fairy tales do exist.

The film ends with colorful credits where "Happily Ever After" by Ashlee Hewitt plays in the background, indicating what happened to all the main characters. Elle graduated from Berklee, Tys album went triple Platinum, Brenda (aka Kandi) was pretending to be a different pop star, and Kit and Andy were happily together with their own detective agency.

==Cast==
* Ashlee Hewitt as Elle Daniels
* Sterling Knight as Ty Parker
* Thomas Calabro as Allen
* Juliette Hing-Lee as Kit
* Brandon Mychal Smith as TJ
* Katherine Bailess as Stephanie
* Kiely Williams as Kandi Kane\Brenda Smirkle
* Shawn-Caulin Young as Andy
* Tyler Nicole as Becky
* Emma Winkler as Jamie
* Lexi DiStefano as Bathroom Girl

==Reception==
  
The film received mixed reviews from audiences. Since its release, 114 users on Rotten Tomatoes have rated the film, giving it an average rating of 2.9 out of 5. 

===Awards===
In 2010, the film won the Audience Award for Best Family Film at the Newport Beach Film Festival. 

==Soundtrack==
A soundtrack for the film has not been physically released yet. However, on February 7, 2011, a Myspace page for the soundtrack was created,  offering fans 31-second previews of 14 songs in total. 
 My Hero, CheNelle, Tommy Mac, Pamela Joe and Oranyan Coltrane. 

The soundtrack has been released on iTunes. 

==References==
 

==External links==
*  
 

 
 
 