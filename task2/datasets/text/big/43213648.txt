Kyō Kara Hitman
{{Infobox film
| name = Kyō Kara Hitman
| image = 
| caption = 
| director =  
| producer = Toshiki Komatsu Shigeji Maeda Tsuneo Seto
| writer =     Masao Iketani   Masayoshi Wagatsuma  
| starring = Shinji Takeda
| music =  
| cinematography = Tetsu Shimomoto
| editing = Chikako Namba
| choreography =  King Records Toei Video Company  
| released =    
| runtime = 101 minutes
| country = Japan
| language = Japanese
| budget = 
}}

  is a 2009 Japanese film adaptation of the eponymous manga by  . It was directed by   and stars Shinji Takeda,    who also composed, arranged, and performed the films theme song, "The Hitman".   

== Plot ==
An ordinary salaryman gets mixed up in the death of a legendary hitman, and has to lead a double life as the hitmans successor. 

== Cast ==
* Shinji Takeda as Tokichi Inaba 
* Mari Hoshino as Misako Inaba
* Yuri Morishita as Chinatsu
* Masaya Kikawada as Dual
* Kanji Tsuda as Round Glasses
* Kaoru Abe as Nekota
* Kaname Endō as Tanaka
* Motoki Fukami as Takao
* Hidekazu Ichinose as Suzuki
* Meguru Katō
* Daijiro Kawaoka as Mushroom Head
* Takayasu Komiya as Shuichi Endo
* Ken Maeda as Kamio
* Kōichirō Nomoto
*  
* Mitsuyoshi Shinoda as Nonezumi
* Hiroshi Yamamoto as Gaku Yamamoto
* Tomohisa Yuge as Yaginuma

== References ==
 

== External links ==
* 
*  
*   ("Kyō Kara Hitman" manga) (in Japanese)

 
 
 
 
 
 
 
 
 


 

   