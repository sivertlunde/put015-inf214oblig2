Vidarunna Mottukal
{{Infobox film
| name = Vidarunna Mottukal
| image =
| caption =
| director = P. Subramaniam
| producer = P Subramaniam
| writer = Nagavally R. S. Kurup
| screenplay = Madhu Kaviyoor Raghavan Sumathi Baby Sumathi
| music = G. Devarajan
| cinematography =
| editing =
| studio = Neela
| distributor = Neela
| released =  
| country = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, Raghavan and Baby Sumathi in lead roles. The film had musical score by G. Devarajan.   

==Cast== Madhu as Headmaster Sathyaseelan
*Kaviyoor Ponnamma as Lakshmi Raghavan as Gopal Baby Sumathi as Kanchana
*Kailasnath as Chandran (Child Artist)
*Mallika Sukumaran as School teacher Saikumar as Vikraman (Child Artist)
*KPAC Sunny as Sulus husband
* Radhamani as Sulu Ambika as Sumam (Child artist)
*Anandavally as Ammini
*Chavara VP Nair as Doctor
*SP Pilla as Pappan Pilla
*CI Paul as Hassan
*Kuthiravattam Pappu as Madhupan
*Lalithasree as Kamakshi
*Aranmula Ponnamma as Gopals mother
*Vijayalakshmi as Muthassi

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Sreekumaran Thampi and Bankim Chandra Chatterji. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Kaattiloru Malarkkulam || Chorus, Santha, Rajeswari || Sreekumaran Thampi || 
|- 
| 2 || Sabarmathithan Sangeetham || P. Madhuri, Chorus, Karthikeyan || Sreekumaran Thampi || 
|- 
| 3 || Vande Maatharam || K. J. Yesudas, P. Madhuri, Karthikeyan || Bankim Chandra Chatterji || 
|- 
| 4 || Vande Maathram   || Chorus || Bankim Chandra Chatterji || 
|- 
| 5 || Vidarunna Mottukal || || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 


 