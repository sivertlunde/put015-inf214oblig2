The Sleeping Room
 
 
{{Infobox film
| name           = The Sleeping Room
| image          = File:The Sleeping Room 2014 Shackleton film.jpg
| alt            =  
| caption        = 
| director       = John Shackleton
| producer       = {{Plainlist|
* Gareth I Davies
*John Shackleton}}
| writer         = {{Plainlist|
* Ross Jameson
* Alex Chandon
*John Shackleton}}
| starring       = {{Plainlist|
* Leila Mimmack
*Joseph Beattie
*Julie Graham Christopher Adamson David Sibley
*Chris Waller}}
| music          = Paul Saunderson
| cinematography = Simon Poulter
| editing        = John Gillanders
| studio         = Movie Mogul Films
| distributor    = 
| released       =   
| runtime        = 78 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
The Sleeping Room is a 2014 British horror movie that was directed by John Shackleton.  It had its world premiere on August 23, 2014 at the London FrightFest Film Festival and stars Leila Mimmack as a call girl that finds herself entangled in a series of strange events surrounding a hidden room.  Funding for The Sleeping Room was raised using equity crowdfunding and is credited as being the first British film to use this method. 

==Plot== Christopher Adamson). Shortly after that, Blue and Bill discover a secret room that is the key to unlocking many dark and terrifying secrets relating to Blue’s family, and the death of her mother

==Cast==
*Julie Graham as Cynthia Christopher Adamson as Fiskin
*Joseph Beattie as Bill
*Chris Waller as Glenny
*Leila Mimmack as Blue David Sibley as Freddie
*Lucy Clements as Helena
*Nicola Colmer as TV presenter
*Billy Chainsaw as Neighbour
*Mike Altmann as Jim Whipps
*Antonia Northam as Abigail
*Chrisanthe Grech as Librarian
*Barry Kristopher Sullivan as TV presenter

==Reception== The Forgotten, marks a new bright future for genre filmmaking in the UK that, in a perfect world, would be held in the same esteem as Hammer’s prolific output."  Anton Bitel of Grolsch Film Works also praised The Sleeping Room, stating "Once the possessions and ghostly manifestations have fully kicked in, it all becomes a little Punch and Judy ... but The Sleeping Room works best as an incestuous love letter to Brighton and the towns darker, ever-present history."  Shock Till You Drop gave a mixed review for The Sleeping Room, writing "While the leads are appealing and Fiskin is very creepy looking, the mystery fails to deliver suspense or scares. The snuff films are meant to terrify, but they are more silly than scary as glimpsed through an old machine. And the conclusion, a dragged out chase and showdown in the old building, is perfunctory and fairly dull, making a short movie feel much longer than it actually is." 

==References==
 

==External links==
*  
*   on Kickstarter

 
 
 