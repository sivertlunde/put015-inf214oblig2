Jealousy Is My Middle Name
 
{{Infobox film
| name = Jealousy Is My Middle Name
| image = Jealousy is my Middle Name movie poster.jpg
| caption = Poster for "Jealousy is My Middle Name"
| film name = {{Film name
 | hangul =      
 | hanja =  는 나의 힘
 | rr = Jiltuneun naui him
 | mr = Chilt‘unŭn naŭi him}}
| director = Park Chan-ok
| producer = Shin Chang-il Peter Kim
| writer = Park Chan-ok
| starring = Park Hae-il Bae Jong-ok Moon Sung-keun Seo Young-hee
| music = Jeong Hun-yeong
| cinematography = Park Yong-su
| editing = Kwon Ki-suk Park Chan-ok
| distributor = 
| released =  
| runtime = 124 minutes
| country = South Korea
| language = Korean
| budget = 
}}
Jealousy Is My Middle Name ( ) is a 2003 South Korean film. It won Best Film honors at the Pusan International Film Festival and the Rotterdam Film Festival and was the directorial debut of Park Chan-ok. It was inspired by the poem of the same name by Ki Hyung-do.

==Plot==
Quiet, intelligent, solemn and recently dumped by his girlfriend, graduate student Lee Weon-san (Park Hae-il) takes a job at a literary magazine, ostensibly to supplement his income, but really to get close to the editor - the reason he’s now single. The editor (Moon Sung-keun), unaware of who Lee is, takes a shine to him and makes him his personal assistant. He likes having him around as he’s the only person he feels comfortable with, which means he often takes advantage of Lee’s passive nature, making him run errands for him all over town.

The fiercely independent Lee, however, works without complaint, having started a new relationship with part-time photographer/part-time vet Park Seong-yeon (Bae Jong-ok). When she takes a full-time job at the magazine, however, Lee pleads with her not to get involved with the editor, a plea that goes unheeded and sets Lee thinking once again about vengeance. It’s here that the film really starts to veer from the conventional path.

==Awards and nominations==
;2002 Busan International Film Festival 
* New Currents Award

;2003 Busan Film Critics Awards
* Best New Actor – Park Hae-il

;2003 Chunsa Film Art Awards
* Best New Actor – Park Hae-il

;2003 Blue Dragon Film Awards
* Best Screenplay – Park Chan-ok

;2003 Korean Film Awards
* Best New Actor – Park Hae-il

;2003 Directors Cut Awards
* Best New Actor – Park Hae-il

==References==
 

==External links==
*  
*  
*  

 
 
 
 

 