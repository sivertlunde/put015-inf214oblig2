Un plan parfait
{{Infobox film
| name           = Un plan parfait
| image          = Un plan parfait 2012 poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Pascal Chaumeil
| producer       = {{Plainlist|
* Nicolas Duval-Adassovsky
* Laurent Zeitoun
* Yann Zenou
}}
| screenplay     = {{Plainlist|
* Laurent Zeitoun
* Yoann Gromb
}}
| story          = Philippe Mechelen
| starring       = {{Plainlist|
* Diane Kruger
* Dany Boon
* Alice Pol
}}
| music          = Klaus Badelt
| cinematography = Glynn Speeckaert
| editing        = Dorian Rigal-Ansous
| studio         = Quad Productions Universal Pictures International (UPI)
| released       =  
| runtime        = 104 minutes
| country        = France
| language       = French
| budget         = 
| gross          = $10,292,492  
}}
Un plan parfait ( ) is a 2012 French action adventure comedy film directed by Pascal Chaumeil and starring Diane Kruger, Dany Boon, and Alice Pol.    Written by Laurent Zeitoun and Yoann Gromb, and based on a story by Philippe Mechelen, the film is about a successful woman in love who tries to break her family curse of every first marriage ending in divorce, by dashing to the altar with a random stranger before marrying her boyfriend.

==Plot==
Isabelle (Diane Kruger) is prepared to marry Pierre (Robert Plagnol), the man she has loved for the past ten years. First she has to overcome a curse that her female family members have been battling for years—that all their first marriages end in divorce. Isabelle comes up with the perfect plan. She will marry a stranger and get a quick divorce to avoid the curse and be happily married forever the second time. She flies to Copenhagen and finds the perfect pigeon in Jean Yves (Dany Boon), an editor for the Guide du Routard, but her plans are complicated when he believes that she is in love with him. When Isabelles arranged marriage goes awry, she tracks down the pigeon in hopes she can somehow marry him and divorce him without too much trouble. When he travels to Kenya, she follows him in the hope that he will sign her divorce papers.

==Cast==
* Diane Kruger as Isabelle 
* Dany Boon as Jean-Yves 
* Alice Pol as Corinne 
* Robert Plagnol as Pierre 
* Jonathan Cohen as Patrick 
* Bernadette Le Saché as Solange 
* Etienne Chicot as Edmond 
* Laure Calamy as Valérie
* Malonn Lévana as Louise 
* Olivier Claverie as Maître Maillard 
* Jean-Yves Chilot as Ambassadeur de France 
* Muriel Solvay as Femme de lamabassadeur 
* Amélie Denarié as Employée mairie 
* Emeline Bayart as Secrétaire cabinet dentaire 
* Jean-Paul Bezzina as Thomasdu guide Hachette

==Production==
Un plan parfait was filmed on location in Moscow, Russia and Antwerp, Flanders, Belgium.   

==Reception==
===Critical response===
In his review in The Hollywood Reporter, Jordon Mintzer wrote that the film "works best as a portrait of two opposites who come together out of empathy, rather than out of love."    In his review in The Montreal Gazette, TCha Dunlevy felt that the film "may have worked on paper, but the execution leaves much to be desired".   

===Box office===
Upon its theatrical release, Un plan parfait earned $10,292,492 worldwide.   

==References==
 

==External links==
*  

 
 
 
 