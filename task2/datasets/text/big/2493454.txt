Flesh Gordon
 
{{Infobox film
| name = Flesh Gordon
| image = Flesh Gordon (1974).jpg
| image_size = 220px
| alt =  George Barr
| director = Michael Benveniste Howard Ziehm
| producer = Walter R. Cichy Bill Osco Howard Ziehm
| writer = Michael Benveniste
| narrator = Robert V. Greene
| starring = Jason Williams Suzanne Fields Joseph Hudgins William Dennis Hunt Candy Samples Mycle Brandy John Hoyt
| music = Ralph Ferraro
| cinematography = Howard Ziehm
| editing = Abbas Amin
| studio = Graffiti Productions
| distributor = Mammoth Films
| released =  
| runtime = 78 minutes 90 minutes    
| country = United States
| language = English
| budget = $470,000
| gross = $906,000
}} science fiction adventure comedy erotic spoof serials from the 1930s. The film was produced by Walter R. Cichy, Bill Osco, and Howard Ziehm and was co-directed by Howard Ziehm and Michael Benveniste, who also wrote the screenplay. The cast includes Jason Williams, Suzanne Fields, and William Dennis Hunt. The film was distributed by Mammoth Films.

The storyline is purposely reminiscent of the first Flash Gordon multi-chapter serial, but written and directed with a purposely  s: the hero Flesh Gordon (Flash Gordon); his love interest Dale Ardor (Dale Arden); the evil Emperor Wang the Perverted (Ming the Merciless); scientist Dr. Flexi Jerkoff (Dr. Alexi Zarkov); seductive Amora, Queen of Magic (Mings daughter Aura); and effeminate Prince Precious (Prince Barin). The film features production values comparable to the original serial, stop-motion animation of creatures, and frequent use of gratuitous nudity and brief sex scenes.

==Plot==
Distinguished professor Gordon explains that Earth is being tormented by periodic "sex rays" which send people into a sexual frenzy. When one of the rays hits the Ford Tri-Motor passenger aircraft carrying Flesh Gordon and Dale Ardor, the pilots abandon the controls and everyone aboard has manic sex. When they finish, Flesh and Dale escape the imminent crash by parachute. They land near the workshop of Flexi Jerkoff, who has a plan to stop the sex rays at their source.

They then travel to the planet Porno in Jerkoffs phallic rocket ship, and are briefly hit by a sex ray, resulting a frantic three-way orgy. They crash land after being shot by the minions of Emperor Wang, and are attacked by several one-eyed "Penisauruses" before being taken prisoner by Wangs soldiers. They are brought before Wang, who is presiding over an orgy of more than a dozen men and women. Jerkoff is sent to work in Wangs laboratory, while Wang announces his intention to make marry Dale, and Flesh is sentenced to death, but is saved when Amora takes him to be her sex slave.

Wang shoots down Amoras ship, and Flesh is the only survivor. He is reunited with Jerkoff, and they resume their efforts to defeat Wang, now with Amoras Power Pasties. Wang and Dales wedding is interrupted when Dale is kidnapped by lesbians, whose Queen attempts to initiate Dale into their cult. Flesh and Jerkoff save her, unexpectedly aided by Prince Precious of the Forest Kingdom. With help from their new ally, Jerkoff builds a weapon to destroy the sex ray. They confront Wang and trick his "rapist robots" into turning on him, but Wang escapes, seeking the aid of the towering idol of the Great God Porno. Porno comes to life and captures Dale as they flee, blandly commenting on his actions. Jerkoff shoots the living idol, freeing Dale and causing the god to fall on Wang and the sex ray.  Flesh, Dale, and Jerkoff are celebrated as heroes, and return to Earth.

==Cast==
* Jason Williams as Flesh Gordon
* Suzanne Fields as Dale Ardor
* Joseph Hudgins as Dr. Flexi Jerkoff
* William Dennis Hunt as Emperor Wang the Perverted
* John Hoyt as Professor Gordon
* Candy Samples as Chief Nellie
* Nora Wieternik as Amora, Queen of Magic
* Lance Larsen as Prince Precious
* Jack Rowe as Guard for Emperor Wang
* Robert V. Greene (voice) as Narrator
* Craig T. Nelson (uncredited voice) as The Great God Porno

==Production==
Flesh Gordon was shot in 1971 and, according to producer Bill Osco, cost $470,000 to make. Osco intended to hold out for a major distributor to pay a $1 million advance to secure the American release rights. 
 MPAA X rating of rating of R. The films original running time was 78 minutes, but the later, unrated "collectors edition" video release runs 90 minutes.

Flesh Gordon employed   so actual footage shot at the base of the observatory could be integrated in the film.

  who designed and illustrated the films one-sheet movie poster, and Cornelius Cole III, who animated the films opening title credits sequence. Longtime fan and science fiction and fantasy writer Tom Reamy served in the films Art Department as the productions Property Master. He tracked down many of the screen-used props in the film, including authentic, full-sized Ford Tri-Motor wicker passenger seats (matching the films Tri-Motor aircraft miniature) used in an early scene in the film.

The towering creature was not originally intended to speak, but it proved so expressive that dialogue was dubbed over to match its mouth movements. Addressed as the Great God Porno in this dialogue, the special effects crew named him "Nesuahyrrah," a tribute to stop-motion animation master Ray Harryhausen, spelling his name backwards. 

According to Ziehms DVD   at that time. The X-rated footage was surrendered to L. A. vice police. Although some explicit shots can be briefly seen during Wangs throne room orgy scenes, the "collectors edition" video, labelled "the original, uncensored version", is no more explicit than any of the earlier video releases.
 Flash Gordon film serial that it bordered on plagiarism. To avoid a lawsuit, Ziehm added an opening text scroll that stated that Flesh Gordon was a burlesque style parody of the Depression Era superheroes of Americas past; he also added "Not to be confused with the original Flash Gordon" to all advertising materials.

==Critical reception==
Vivian Sobchack commented that Flesh Gordon is "a skin flick hilariously molded around the Flash Gordon serials, and fully and lovingly aware of genre conventions from special effects to dialogue".   

The review aggregate website Rotten Tomatoes has the film rated with a fresh rating of 67%. 

==Legacy==
A sequel, Flesh Gordon Meets the Cosmic Cheerleaders, followed in 1989.

A four-issue comic book miniseries, written by Daniel Wilson and published by Aircel Comics, was published in 1992. 

==References==
 

==External links==
*  
*  
*  
* The Second Supper  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 