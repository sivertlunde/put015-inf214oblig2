The Mute of Portici
{{Infobox film
| name           = The Mute of Portici
| image          = 
| image_size     = 
| caption        = 
| director       = Giorgio Ansoldi
| producer       = 
| writer         = Giorgio Ansoldi Cesare Ardolino Alessandro De Stefani Giacinto Solito
| narrator       = 
| starring       = Flora Mariel
| music          = 
| cinematography = Alvaro Mancori
| editing        = 
| distributor    = 
| released       = 1952
| runtime        = 92 minutes
| country        = Italy
| language       = Italian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Mute of Portici ( ) is a 1952 Italian drama film directed by Giorgio Ansoldi.   

==Cast==
* Flora Mariel - Lucia Maniello
* Doris Duranti - Elvira DHerrera
* Paolo Carlini - Masaniello
* Ottavio Senoret - Alfonso
* Umberto Sacripante - La Spia
* Raf Pindi - Il Vicere
* Paolo Dola - Cap. Perrone
* Maurizio Di Nardo
* Isarco Ravaioli
* Anna-Maria Ferrero (uncredited)
* Marcello Mastroianni (uncredited)
* Jacques Sernas (uncredited)

==References==
 

==External links==
* 

 
 
 
 
 
 
 