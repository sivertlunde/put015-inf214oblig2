The Last of England (film)
{{Infobox film
| name           = The Last of England
| image_size     =
| image	=	The Last of England FilmPoster.jpeg
| caption        =
| director       = Derek Jarman
| producer       = James Mackay & Don Boyd
| writer         = Derek Jarman
| narrator       = Nigel Terry Jonathan Phillips Spencer Leigh Spring - Mark Adley
| music          = Simon Fisher Turner Andy Gill Marianne Faithfull Mayo Thompson Diamanda Galás Barry Adamson
| cinematography = Derek Jarman, Christopher Hughes, Richard Heslop, Cerith Wyn Evans
| editing        = Derek Jarman, Peter Cartwright, Angus Cook
| distributor    =
| released       = 1987
| runtime        = 87 min.
| country        = United Kingdom
| language       = English
| budget         = GBP£276,000
| gross          =
| preceded_by    =
| followed_by    =
}} British film directed by Derek Jarman.
 painting by the Pre-Raphaelite artist Ford Madox Brown.
 punk movement of the time).

The book and to a lesser extent the film are very much in the tradition of Roland Barthes Camera Lucida, Susan Sontags On Photography, Jeanette Wintersons Art Objects and to a lesser extent John Bergers Ways of Seeing in that he has used the deeply familiar and personal as a vehicle for dialogue about art and contemporary culture.

Derek Jarman received the 1988 Teddy Award in Berlin for the film.

==External links==
*  
*  

 

 
 
 
 
 
 
 


 