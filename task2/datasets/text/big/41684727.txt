Whiplash (2014 film)
 
{{Infobox film
| name                 = Whiplash
| image                = Whiplash poster.jpg
| caption              = Theatrical release poster
| director             = Damien Chazelle
| producer             = {{Plain list|
* Jason Blum
* Helen Estabrook
* Michel Litvak
* David Lancaster
}}
| writer               = Damien Chazelle
| starring             = {{Plain list|
* Miles Teller
* J. K. Simmons
* Paul Reiser
 
}}
| music                = Justin Hurwitz
| cinematography       = Sharone Meir Tom Cross
| production companies = {{Plain list|
* Sierra/Affinity
* Bold Films
* Blumhouse Productions
* Right of Way Films
}}
| distributor          = Sony Pictures Classics
| released             =  
| runtime              = 106 minutes  
| country              = United States
| language             = English
| budget               = $3.3 million   
| gross                = $33.1 million 
}}
 Princeton High School Studio Band.  Starring Miles Teller and J. K. Simmons, the film depicts the relationship between an ambitious jazz student (Teller) and an abusive instructor (Simmons). Paul Reiser and Melissa Benoist co-star as the students father and love interest respectively. The film opened in limited release domestically in the US and Canada on October 10, 2014, gradually expanding to over 500 screens and finally closing after 24 weeks on March 26, 2015. Over this time the film grossed $33.1 million against a production budget of $3.3 million.
 Sony Pictures Best Film Best Sound Best Supporting Best Adapted Best Picture.

==Plot== New York. He has been playing drums from a young age and aspires to become one of the greats like Buddy Rich. Famed conductor Terence Fletcher (J. K. Simmons) walks in on Andrew practising in the music room late one night and eventually invites him into his studio band as the alternate for core drummer Carl Tanner (Nate Lang). Fletcher is abusive toward his students, mocking and insulting them; when the band rehearses the Hank Levy piece "Whiplash" and Andrew struggles to keep his tempo, Fletcher hurls a chair at him, slaps him, and otherwise humiliates him in front of the class.

At a local jazz competition, Andrew accidentally misplaces Carls sheet music; as Carl cannot play without it, Andrew steps in informing Fletcher that he can perform "Whiplash" from memory. Fletcher promotes him to core drummer. Soon after, Fletcher recruits Ryan Connolly (Austin Stowell), the core drummer from Andrews former lower-level class. Ryan is clearly a worse drummer, but Fletcher promotes him to core almost immediately, infuriating Andrew. Determined to impress Fletcher, Andrew practices until his hands bleed and breaks up with his girlfriend Nicole (Melissa Benoist), believing she will distract him. 

The next day, Fletcher tearfully reveals in class that a talented former student of his, Sean Casey, has died in a car accident. The band rehearses "Caravan (1937 song)|Caravan", but Ryan struggles with the tempo; Fletcher auditions Andrew, Ryan and Carl for hours while the class waits outside. Fletcher finally gives the position to Andrew.

On the way to a jazz competition, Andrews bus breaks down. Determined to make the performance, he rents a car but arrives late without his drumsticks. After an argument with Fletcher and a tirade against his fellow musicians, Andrew drives back to the car rental office and retrieves the drumsticks. As he speeds back, his car is hit by a truck. He crawls from the wreckage and arrives on stage badly injured. When he struggles to play "Caravan" due to his injuries, Fletcher stops the band midway through the performance to tell Andrew that he is "done". Andrew attacks Fletcher in front of the audience and is dragged away.

Days later, Andrew is expelled from Shaffer and meets with a lawyer representing the parents of Sean Casey. The lawyer explains that Sean actually hanged himself, having suffered anxiety and depression after joining Fletchers class. Seans parents want to prevent Fletcher from teaching. Andrew agrees to testify anonymously, resulting in Fletcher being fired.

Months later, Andrew has abandoned music and is living with his father. He happens to walk past a jazz club one day and sees Fletcher performing on stage. Fletcher invites him for drinks. During that time, he explains that he pushes his students beyond the expected so they might achieve greatness. He invites Andrew to perform at a festival concert with his band. Andrew agrees and invites Nicole, but she is in a new relationship and declines.

On stage at the jazz festival, Fletcher reveals that he knew all along that Andrew had testified against him, and this concert is his revenge. He leads the band in a new piece for which Andrew was not given sheet music. Andrew is humiliated and flees the stage to be comforted by his father, but as Fletcher is addressing the audience, Andrew returns to the drumset and starts playing "Caravan". The rest of the band joins him, surprising Fletcher, who eventually follows suit. Andrew ends the performance with an extravagant drum solo, and at the end of it Fletcher gives him a smile, which Andrew gratefully returns, having pleased his teacher at last.

==Cast==
 
* Miles Teller as Andrew Neiman, an ambitious young jazz student at Shaffer who plays the drums.
* J. K. Simmons as Terence Fletcher, the jazz instructor at Shaffer. 
* Paul Reiser as Jim Neiman, Andrews father, a high school teacher.
* Melissa Benoist as Nicole, a movie theater concessionist, who becomes Andrews girlfriend.
* Austin Stowell as Ryan Connolly, another student drummer who later joins Fletchers class.
* Nate Lang as Carl Tanner, the original core drummer in Fletchers class.
* Chris Mulkey as Uncle Frank, Andrews uncle. Jayson Blair as Travis
* Kavita Patil as Sophie Michael Cohen as Stagehand Dunellen
* Kofi Siriboe as Greg
* Suanne Spoke as Aunt Emma
* April Grace as Rachel Bornholdt
 

==Production==
While attending Princeton High School, writer/director Damien Chazelle was in a "very competitive" jazz band and drew on the experience of "just dread" that he felt in those years.    He based the conductor, Terence Fletcher, on his former band instructor (who died in 2003) but "pushed it further" adding in bits of Buddy Rich as well as other notorious band leaders. 
 Black List that includes the top motion picture screenplays not yet produced.  Right of Way Films and Blumhouse Productions produced, and in order to secure financing for the feature, helped Chazelle turn 15 pages of his original screenplay into a short film starring Johnny Simmons in the role of the drummer and J. K. Simmons in the role of the teacher.  The 18-minute short film went on to receive much acclaim after screening at the 2013 Sundance Film Festival,    which ultimately attracted investors to sign on and produce the complete version of the script.  The feature-length film was financed for $3.3 million by Bold Films. 
 Palace Theater, Orpheum Theatre.  

Early on Chazelle gave J. K. Simmons direction that "I want you to take it past what you think the normal limit would be", telling him: "I dont want to see a human being on-screen anymore. I want to see a monster, a gargoyle, an animal." Many of the band members in the movies were real musicians or music students and Chazelle tried to capture real moments of terror from them. However, Chazelle noted that in between takes Simmons was "as sweet as can be" which Chazelle credits for keeping "the shoot from being nightmarish." 

The film was shot in 19 days, with a schedule of 14 hours of filming per day.       Chazelle was involved in a serious car accident in the third week of shooting and was hospitalized with a diagnosis of possible concussion, but he returned to filming the next day to finish the film in time.  Despite being set in New York City, the film was filmed in Los Angeles with a few exterior shots filmed in NYC. 

==Reception==

===Box office===
Whiplash opened in a limited release in the United States in six theaters and grossed $135,388, averaging $22,565 per theater and ranking number 34 at the box office. The film expanded wider and earned $6,671,000 domestically and $901,092 in other territories for a total gross of $7,572,092, above its $3.3 million production budget.    The film had a total domestic gross of $13.1 million.   

===Critical response===
 .]] rating average of 8.6 out of 10. The sites critical consensus states, "Intense, inspiring, and well-acted, Whiplash is a brilliant sophomore effort from director Damien Chazelle and a riveting vehicle for stars J. K. Simmons and Miles Teller."  On Metacritic, another review aggregator, the film has a score of 88 out of 100, based on 49 critics, indicating "universal acclaim". 
 Academy Award for Best Sound Mixing and the 87th Academy Award for Best Film Editing.   

Amber Wilkinson from  ... often cutting to the crash of Andrews drums."  James Rocchi of Indiewire gave a positive review and said, "Whiplash is...full of bravado and swagger, uncompromising where it needs to be, informed by great performances and patient with both its characters and the things that matter to them."  Henry Barnes from The Guardian gave the film a positive review, calling it a rare film "about music that professes its love for the music and its characters equally." 

Forrest Wickman of  , Richard Brody argued that "Whiplash honors neither jazz nor cinema".  The term "Whiplash backlash" is increasingly used to describe the reaction to the film by jazz fans. 

===Top ten lists===
 
* 1st&nbsp;– William Bibbiani, CraveOnline
* 1st&nbsp;– Chris Nashawaty, Entertainment Weekly
* 2nd&nbsp;– A.A. Dowd, The A.V. Club
* 2nd&nbsp;– Scott Feinberg, The Hollywood Reporter
* 2nd&nbsp;– Mara Reinstein, Us Weekly
* 3rd&nbsp;– Tasha Robinson, The Dissolve
* 3rd&nbsp;– Amy Taubin, Artforum
* 4th&nbsp;– Ignatiy Vishnevetsky, The A.V. Club
* 4th&nbsp;– Kyle Smith, New York Post
* 4th&nbsp;– Peter Hartlaub, San Francisco Chronicle Michael Phillips, Chicago Tribune New York
* 5th&nbsp;– Ty Burr, The Boston Globe
* 5th&nbsp;– Genevieve Koski, The Dissolve
* 5th&nbsp;– James Berardinelli, Reelviews
* 6th&nbsp;– Peter Travers, Rolling Stone
* 5th&nbsp;– Betsy Sharkey, Los Angeles Times (tied with Foxcatcher (film)|Foxcatcher)
* 6th&nbsp;– Richard Roeper, Chicago Sun-Times
* 6th&nbsp;– Joe Neumaier, New York Daily News
* 7th&nbsp;– Jesse Hassenger, The A.V. Club
* 7th&nbsp;– Rex Reed, New York Observer
* 7th&nbsp;– Noel Murray, The Dissolve
* 7th&nbsp;– Jocelyn Noveck, Associated Press
* 7th&nbsp;– Wesley Morris, Grantland
* 7th&nbsp;– Alison Willmore, BuzzFeed
* 8th&nbsp;– Ben Kenigsberg, The A.V. Club
* 9th&nbsp;– Nathan Rabin, The Dissolve
* 10th&nbsp;– Owen Gleiberman, BBC
* Top 10 (listed alphabetically, not ranked)&nbsp;– Claudia Puig, USA Today
 

==Accolades==
  40th Deauville Adapted Screenplay Tom Cross Academy Award for Best Sound Mixing.

==References==
 

==External links==
 
*  
*  
*  
*  
*   on SundanceTV

 
 
{{Succession box
| title= 
| years=2014
| before=Fruitvale Station Me & Earl & the Dying Girl}}
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 