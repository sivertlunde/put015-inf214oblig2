Dore (film)
{{Infobox film|
| name = Dore
| image = 
| caption =
| director = Shivamani
| based on = "Beliya Hoogalu" by Ku. Veerabhadrappa Bharathi
| producer = M. Chandrashekar
| music = Hamsalekha
| cinematography = Krishna Kumar
| editing = K. Balu
| studio = Sri Nimishamba Productions
| released =  
| runtime = 147 minutes
| language = Kannada
| country = India
| budgeBold textt =
}}
 Kannada drama romance drama Bharathi in the lead roles.  The story is based on the novelist Ku. Veerabhadrappas novel named "Beliya Hoogalu". 

The films score and soundtrack was scored by Hamsalekha and the cinematography was by Krishna Kumar.

== Cast ==
* Shiva Rajkumar 
* Hema Panchamukhi Bharathi
* Srinivasa Murthy
* Dheerendra Gopal
* Sudheer
* Girija Lokesh
* Ashalatha
* Dingri Nagaraj
* Vasudeva Rao
* Kashi
* Shankar Ashwath

== Soundtrack ==
The soundtrack of the film was composed by Hamsalekha. 

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Halli Haadu Thandana
| extra1 = S. P. Balasubrahmanyam & K. S. Chithra
| lyrics1 = Hamsalekha
| length1 = 
| title2 = Laaliya Thumba
| extra2 = S. P. Balasubrahmanyam & K. S. Chithra
| lyrics2 = Hamsalekha
| length2 = 
| title3 = Ago Bandhanu
| extra3 = K. J. Yesudas & K. S. Chithra
| lyrics3 = Hamsalekha
| length3 = 
| title4 = Ramanna Ninge Kosambari
| extra4 = S. P. Balasubrahmanyam 
| lyrics4 = Hamsalekha
| length4 = 
| title5 = Hunnimeya Deepa
| extra5 = S. P. Balasubrahmanyam 
| lyrics5 = Hamsalekha
| length5 = 
| title6 = Thai Thai Thai Mano & S. Janaki
| lyrics6 = Hamsalekha
| length6 = 
| title7 = Raitha Raitha Rajkumar 
| lyrics7 = Hamsalekha
| length7 = 
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 


 

 