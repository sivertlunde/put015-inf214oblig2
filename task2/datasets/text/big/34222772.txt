The Human Centipede 3 (Final Sequence)
 
{{Infobox film
| name = The Human Centipede 3 (Final Sequence)
| image = The Human Centipede 3 Poster.jpg
| alt = 
| caption = 
| director = Tom Six
| producer = {{Plainlist|
* Tom Six
* Ilona Six}}
| writer = Tom Six
| starring = {{Plainlist|
* Dieter Laser
* Laurence R. Harvey
* Robert LaSardo Tommy "Tiny" Lister
* Jay Tavare
* Eric Roberts
* Bree Olson
* Clayton Rohner}}
| music = Misha Segal
| cinematography = David Meadows
| editing = Nigel de Hond Six Entertainment Company
| distributor = IFC Midnight
| released =  
| runtime = 103 minutes
| country = Netherlands
| language = English
| budget = 
| gross = 
}} Human Centipede trilogy.    It was confirmed to be in production in May 2013.   

Featuring the leading actors from the first two films,   on 22 May 2015. 

== Plot ==
The plot is today (5 May 2015) unknown. The only thing that has been publicly announced is that the film is set in an American maximum security prison in the middle of the desert.   

==Cast==
 
* Dieter Laser as Bill Boss
* Laurence R. Harvey as Dwight Butler
* Vivien Bridson as Lomax Boss
* Tom Six as himself
* Eric Roberts  as Governor Hughes
* Bree Olson  as Daisy
* Clayton Rohner as Dr. Jones
* Hamzah Saman as Inmate 093
* Peter Blankenstein as Inmate 106 Tommy "Tiny" Lister, Jr.  as Inmate 178
* Robert LaSardo  as Inmate 297
* Carlos Ramirez as Inmate 309
* Jay Tavare as Inmate 346
* Bill Hutchens as Inmate 488
* Chris Clanton as Prisoner
 

==Production==
On 29 May 2013, a press release confirmed that Eric Roberts had been cast in the film, as per the previous press release stated that the film would star an American celebrity.  The film will also see the return of both Laser and Harvey, albeit in different roles. Both Laser and Harvey have starred in previous entries as the main antagonists, Dr. Josef Heiter and Martin respectively.

On 7 November 2013, Six said during the American Film Market this third installment would take place in a prison and the human chain would be composed of 500 people.  In Order to the promotion, Six released in March 2015, 250 hand signed prints of the film. 

==Release==
On 7 April 2015, Entertainment Weekly announced the films theatrical and video on demand release date to be 22 May 2015.   

==References==
 

==External links==
* 

 

 

 
 
 
 
 
 
 
 
 
 
 

 