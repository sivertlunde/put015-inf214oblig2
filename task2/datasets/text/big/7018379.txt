Junk Mail (film)
{{Infobox Film |
  name         = Junk Mail (film) |
  image        = Junk Mail.jpg |
  caption        =Theatrical poster for Junk Mail|
  writer       = Pål Sletaune  Jonny Halberg |
  starring     = Robert Skjærstad  Andrine Sæther  Per Egil Aske  Eli Anne Linnestad  Trond Høvik  Henriette Steenstrup  Ådne Olav Sekkelsten  Trond Fausa Aurvaag | 
  director     = Pål Sletaune |
  producers     = Peter Bøe and Dag Nordahl|
  cinematography = Kjell Vassdal |
  composer      = Joachim Holbek |
  editor       = Pål Gengenbach  
  distributor  = Lions Gate Films |
  released     = February 21, 1997 (Norway) |
  runtime      = 83 min |
  language     = Norwegian |
  budget       = $2,100,000 |
  music        = |
  awards       = |
  }}
 Norwegian film made in 1997.  The film won many awards including Best Actress for Eli Anne Linnestad, Best Actor for Robert Skjærstad and Best Film for Pål Sletaune at the Amanda Awards in Norway and the Mercedes-Benz award at the Cannes Film Festival.  

The film makes evocative use of its spectacular setting, the Norwegian capital of Oslo.  It avoids the most famous locations and instead uses the lesser-known streets, harbour and railway sidings; it was mainly filmed on location.   

==Synopsis==

Junk Mail tells the story of lazy, nosy postman Roy Amundsen who has a bad habit of dipping into the mail of his customers.  He takes this further when he finds the keys to the apartment of laundry assistant Line Groberg and decides to investigate her apartment.  This leads to all manner of complications when he saves her life and then discovers that shes been coerced into covering up a crime and looking after money for local gangster.

Junk Mail received generally positive reviews and currently holds a 93% rating on Rotten Tomatoes.

==External links==
*  
*  

 
 
 

 