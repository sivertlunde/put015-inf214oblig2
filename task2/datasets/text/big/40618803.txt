Golfo (1914 film)
{{Infobox film
| name           = Golfo 
| image          = 
| caption        = 
| director       = Konstadinos Bahatoris
| producer       = Konstadinos Bahatoris
| writer         = 
| starring       = Virginia Diamadi Olympia Damaskou Dionysios Venieris Georgios Ploutis
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Greece
| language       = Silent
| budget         = 
}}

Golfo is a 1914 Greek silent film directed by Konstadinos Bahatoris. It is the first Greek feature film. It was basen on a popular Greek bucolic play written by Spyridon Peresiadis.

==Plot==
Tasos is a young shepherd in love with Golfo and intends to marry her. However, the rich shepherdess Stavroula with the help of her father manages to lure him with the promise of a dowry. Eventually he realises his mistake and returns to Golfo, but she has poisoned herself. Driven by guilt, Tasos commits suicide.

==Cast==
*Virginia Diamadi as Golfo
*Dionysios Venieris		
*Olympia Damaskou as Stavroula
*Georgios Ploutis as Tasos
*Hrysanthi Kondopoulou-Hatzihristou		
*Pantelis Lazaridis		
*Theodoros Litos		
*Ahilleas Madras		
*Th. Petsos		
*Thanos Zahos

==External links==
* 
* 

 
 
 
 
 
 