Lunch Hour
{{Infobox film
| name        = Lunch Hour
| image       =
| caption     =
| producer    = James Hill
| writer      = John Mortimer
| based on    = play by John Mortimer
| starring    = Shirley Anne Field Robert Stephens
| studio      = Eyeline Productions Bryanston Films
| released    = 1961
| runtime     = 64 mins
| language    = English
| budget      =
}}
Lunch Hour is a 1961 film based on a one-act play by John Mortimer. It is about a man and a woman who have an affair during their lunch hour. Shirley Ann Field described it as perhaps "the most enjoyable film Id ever done" because the cast and crew all worked so closely together. 

The cast in the original stage production included Wendy Craig with whom Mortimer had an affair and conceived a son. 

The movie was recently issued on DVD. 
==References==
 
==External links==
*  at IMDB

 
 
 