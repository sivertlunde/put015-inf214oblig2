Mabel's Busy Day
{{Infobox film
  | name = Mabels Busy Day
  | image =Mabel Busy Day.jpg
  | imagesize=180px
  | caption =Mabel Normand in film
  | director = Mabel Normand
  | producer = Mack Sennett
  | writer = Mabel Normand
  | starring = Mabel Normand Charles Chaplin Chester Conklin  Slim Summerville  Billie Bennett Harry McCoy Wallace MacDonald   Edgar Kennedy   Al St. John Charley Chase Mack Sennett Henry Lehrman
  | music = Frank D. Williams
  | editing =
  | distributor = Keystone Studios
  | released = June 13, 1914
  | runtime = 10 minutes English intertitles
  | country        = United States
}}

Mabels Busy Day is a 1914 short comedy film starring Mabel Normand and Charles Chaplin; the film was also written and directed by Mabel Normand.  The supporting cast includes Chester Conklin, Slim Summerville, Edgar Kennedy, Al St. John, Charley Chase, and Mack Sennett.
 

==Synopsis==
Mabel (Mabel Normand) tries to sell hot dogs at a car race, but isnt doing a very good job at it. She sets down the box of hot dogs and leaves them for a moment. Charlie (Charles Chaplin) finds them and gives them away to the hungry spectators at the track as Mabel frantically tries to find her lost box of hot dogs. Mabel finds out that Charlie has stolen them and sends the police after him. Chaos ensues.

==Cast==
* Mabel Normand - Mabel
* Charles Chaplin - Tipsy nuisance
* Chester Conklin - Police sergeant
* Slim Summerville - Policeman
* Billie Bennett - Woman
* Harry McCoy - Hotdog thief
* Wallace MacDonald - Spectator
* Edgar Kennedy - Tough hotdog customer
* Al St. John - Policeman
* Charley Chase - Spectator
* Mack Sennett - Customer
* Henry Lehrman - Spectator

==See also==
* Charlie Chaplin filmography

==External links==
 
* 
* 
*  

 
 
 
 
 
 
 
 


 