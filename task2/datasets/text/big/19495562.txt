Den opvakte jomfru
{{Infobox Film
| name           = Den opvakte jomfru
| image          = Den opvakte jomfru.jpg
| image_size     = 
| caption        = 
| director       = Lau Lauritzen, Jr. Alice OFredericks
| producer       = Henning Karmark
| writer         = Poul Henningsen
| narrator       = 
| starring       = Marguerite Viby
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen
| editing        = Marie Ejlersen
| distributor    = 
| released       = 16 April 1950
| runtime        = 100 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Den opvakte jomfru is a 1950 Danish family film directed by Lau Lauritzen, Jr. and Alice OFredericks.

==Cast==
* Marguerite Viby - Skønjomfruen Anne Pedersdatter
* Helge Kjærulff-Schmidt - Profossor Ebenezer Steenhammer
* Lisbeth Movin - Frk. Grøndal
* Kjeld Jacobsen - Journalist
* Kjeld Petersen - Pressefotograf
* Sigurd Langberg - Profossor A.P. Jørgensen
* Elith Pio - Chefredaktør
* Knud Schrøder - Redaktionssekretær
* Minna Jørgensen - Steenhammers husbestyreinde
* Ib Schønberg - Abbeden

==External links==
* 

 
 

 
 
 
 
 
 
 
 