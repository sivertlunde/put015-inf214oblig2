After the Fall (film)
 
{{Infobox film
| name           = After the Fall
| image          = After the Fall film poster.jpg
| caption        = After the Fall film poster
| director       = Saar Klein
| producer       = 
| writer         = Saar Klein Joe Conway
| starring       = Wes Bentley
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
}}

After the Fall is a 2014 American drama film directed by Saar Klein. The film, originally titled Things People Do at the time of its release,    had its premiere in the Panorama section of the 64th Berlin International Film Festival.   

The film had its premiere in theaters and VOD on December 12, 2014.  

==Cast==
* Wes Bentley
* Jason Isaacs
* Vinessa Shaw
* Haley Bennett
* Sam Trammell as Lee
* W. Earl Brown
* Jeremiah Bitsui
* Missy Yager as Carol
* Chad Brummett as Rick
* Nathan Brimmer
* Stacy Shane as Larry

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 