London Town (film)
 
 
{{Infobox film
  | name = London Town
  | image =LondonTown.jpg
  | caption =Sid Field and Petula Clark
  | director = Wesley Ruggles	
  | producer = Wesley Ruggles		
  | writer = Val Guest Sig Herzig Elliot Paul
  | starring =Sid Field Petula Clark Greta Gynt Kay Kendall Sonnie Hale  Tessie OShea Johnny Burke)
  | cinematography = Erwin Hillier
  | editing = Sidney Stone	
  | distributor = Eagle-Lion Films|Eagle-Lion Distributors Limited
  | released = 30 September 1946 (UK release)
  | runtime = 126 min
  | country = United Kingdom
  | language = English
  | budget =
  }}
 1946 Technicolor British film|cinema.

==Plot summary==
The screenplay by Sig Herzig, Val Guest, and Elliot Paul, based on a story by director Wesley Ruggles, revolves around comedian Jerry Sanford (Sid Field), who arrives in London believing he has been hired as the star of a major stage production, when in fact hes merely an understudy. Thanks to his daughter Peggy (Petula Clark, already a screen veteran at age fourteen), who sabotages the revues star Charlie de Haven (Sonnie Hale), he finally gets his big break. The premise allows for a variety of musical numbers and comedy sketches performed by, among others, Kay Kendall in her film debut and Tessie OShea.

==Cast==
* Sid Field as Jerry Sanford
* Greta Gynt as Mrs. Eve Barry
* Petula Clark as Peggy Sanford
* Kay Kendall as Patsy
* Sonnie Hale as Charlie de Haven
* Claude Hulbert as Belgrave, Charlies dresser
* Mary Clare as Mrs. Gates
* Tessie OShea as herself
* Jerry Desmonde as George
* Beryl Davis as Paula
* Scotty McHarg as Bill
* W.G. Fay as Mike
* Reginald Purdell as Stage Manager
* Alfie Dean as Heckler
* Charles Paton as Novelty Shopkeeper
* Pamela Carroll as Street Singer
* Marion Saunders Obligato in Street Singer
* Lucas Hovinga as Dancer 
* Jack Parnell as Drummer
* Sheila Bligh as London Town Dozen & One Girl
* Dorothy Cuff as London Town Dozen & One Girl Pat Hughes as London Town Dozen & One Girl
* Sheila Huntington as London Town Dozen & One Girl
* Pauline Johnson (actress) as London Town Dozen & One Girl
* Pamela Kay as London Town Dozen & One Girl
* Freda Lansley as London Town Dozen & One Girl
* Mary Midwinter as London Town Dozen & One Girl
* Giselle Morlais as London Town Dozen & One Girl
* Louise Newton as London Town Dozen & One Girl
* Enid Smeeden as London Town Dozen & One Girl
* Pauline Tyler as London Town Dozen & One Girl
* Jackie Watson as London Town Dozen & One Girl
* Stella Hamilton as Dancer (uncredited)
* James Kenney as Extra (uncredited)
* Wally Patch as Constable (uncredited)
* Susan Shaw as Extra (uncredited) Ann Sullivan as Singer, Street Scene (uncredited)
 
==History==
The critical and financial failure of the extravagant film, Britains first major Technicolor musical, is part of British film legend. Financed by the Rank Organisation at a time of rationing and shortages of materials in the period immediately after World War II, it was filmed in the shell of "Sound City Shepperton," which had been made available as a film studio after being requisitioned during the war as a factory for aircraft parts. (The studio was later renamed Shepperton Studios and is still used for film production.)
 American at the helm, and the task fell to Wesley Ruggles, who produced as well.
 Academy Award-winning Western epic Cimarron (1931 movie)|Cimarron (1931) and the Mae West comedy Im No Angel (1933), both more than a decade old &ndash; and his Hollywood career was on a downslide, he was an odd choice indeed.
 Johnny Burke), Ted Heath and his orchestra), and costumes by the legendary designer Orry-Kelly, while at the same time re-equipping the studio from the ground up. He was confident that box-office business was booming at the time and that demand for a flashy musical entertainment would be such that he would make a healthy profit, so his financial controls were slack.

So much was spent on production that the film needed to perform better than possible just to break even but, dismissed by critics (who described it as "tacky" and "tasteless") and ignored by audiences, it was a legendary flop. In hindsight however, especially for nostalgia fans, many of its kitschy aspects make it fascinating, and film historians consider it an interesting record of the times in which it takes place. Following Britains victory in the war, it can be seen as a tribute to London and its residents, and as a celebration of popular Cockney culture, especially its music hall traditions.

==Music== Charles Collins), and "My Heart Goes Crazy," which was the title under which an abridged U.S. version of the film was released by United Artists in 1953.

Oddly Clark, who had started her career singing for the troops on the British Broadcasting Corporation|BBC, performed in none of the films musical numbers. (She is in fact seen singing in "Any Way the Wind Blows".)  In September 2006, the films soundtrack - plus bonus tracks including four early studio recordings by Clark - was released on CD by Sepia Records. It is true to note that this film massively increased Clarks standing as so many reviewers who slammed the film praised the performance of the little girl who outshone her co-stars.

==Media== BFI Southbank  

In September 2011 the film is to be released in its full length version by Odeon Entertainment, digitally remastered,and for the first time to be commercially available. Clark has repeatedly been asked about the film and has always been very positive in her comments about Sid Field. In June 2011 she wrote a short piece of praise in relation to Kay Kendall, this item is now displayed at the Kay Kendall Museum in Withernsea, East Yorkshire. This release follows an internet campaign to get the film restored and issued by Clarks fans. In her note she mentions that she was 11 years old and that Kendall was 18 and that it was Kendalls début but Clark herself was a veteran of four films by this time.

==References==
 
==External links==
* 

 

 
 
 
 
 