Rikyu (film)
{{Infobox film
| name           = Rikyū
| image          = rikyu film.jpg
| image_size     =
| caption        = DVD Cover Art
| director       = Hiroshi Teshigahara
| writer         = Genpei Akasegawa Hiroshi Teshigahara
| starring       = Rentarō Mikuni Tsutomu Yamazaki
| music          = Tōru Takemitsu
| cinematography = Fujio Morita
| editing        = Toshio Taniguchi
| distributor    = Capitol Films (USA)
| released       =  
| runtime        = 135 min
| country        = Japan Japanese
| based on       =  
}} Momoyama Period. Rikyū is portrayed as a man thoroughly dedicated to aesthetics and perfection, especially in relation to the art of tea. While serving as tea master to the new ruler Toyotomi Hideyoshi, Rikyū finds himself in a uniquely privileged position, with constant access to the powerful feudal lord and the theoretical ability to influence policy, yet he studiously avoids deep involvement in politics while attempting to focus his full attention to the study and teachings of the way of tea. To the extent that he expresses himself, he does so diplomatically, in a way to avoid disrupting the harmony of his relationship with Hideyoshi. Yet, as society is changed violently and radically around him, also finding himself the focus of jealousy and misdirected suspicions, Rikyū ultimately can not avoid confronting larger social issues. He is compelled to express an opinion on Hideyoshis military plans. This one breach of his studied isolation from world affairs leads quickly to tragic consequences.

Director Hiroshi Teshigahara|Teshigahara, himself a master and teacher of the Japanese traditional art of ikebana, brings the viewer into appreciation and deep sympathy for Rikyus aesthetic idealism and his careful diplomatic efforts to avoid excessive entanglement in political affairs. The film itself is very studied in its aestheticism, and very expressive of the shocking force of life intruding into the guarded hermetic space of the artist/idealist.

==Cast==
*Rentarō Mikuni as Sen no Rikyū
*Tsutomu Yamazaki as Toyotomi Hideyoshi
*Yoshiko Mita as Riki
*Matsumoto Kōshirō IX as Oda Nobunaga
*Nakamura Kichiemon II as Tokugawa Ieyasu
*Ryō Tamura as Toyotomi Hidenaga Nene
*Tanie Kitabayashi as Ōmandokoro
*Sayoko Yamaguchi as Yodo-dono|Cha-cha
*Mitsugorō Bandō as Ishida Mitsunari
*Hisashi Igawa as Yamanoue Sōji
*Donald Richie as Gaspar Coelho

==Awards== Tsuribaka Nisshi of the same year. He also won four other Japanese acting awards for the role. Tōru Takemitsu won the Japanese Academy award for best musical score. Director Hiroshi Teshigahara won awards from the Berlin International Film Festival, and the Montréal World Film Festival.

==External links==
*  
*  
*   at Strictly Film School
*     at the Japanese Movie Database

 

 
 
 
 
 
 
 
 