At Last, Okemah!
{{Infobox film
 | name           = At Last, Okemah!
 | image          = AtLastOkemahPoster.jpg
 | director       = Michael Glover Smith
 | producer       = Ben Chandler Clayton Monical Kevin Viol
 | writer         = Adam Selzer Michael Glover Smith
 | starring       = Kevin Viol Hector Reyes Robyn Pennacchia Jesse Wheeler Paul Perroni Jon Langford Mia Park Suzy Brack Duane Sharp Deirdre Hayes
 | cinematography        = Jonathan Cohon
 | editing        = Kevin Viol
 | music          = Adam Selzer Kevin Viol
 | released       =  
 | runtime        = 17 minutes
 | country        = United States
 | language       = English
}}
At Last, Okemah! is a 2009 short film directed by Chicago-based independent filmmaker Michael Glover Smith, based on an original screenplay by novelist Adam Selzer and Smith. The film had its world premiere at the Chicago International REEL Shorts Festival on Sunday, September 13, 2009, where it won an Audience Choice Award.  It was also an Official Selection of the 2009 Asheville Film Festival, the 2010 Tallahassee Film Festival and the 2010 Chicago International Music and Movies Festival, where the film effectively "opened" a concert by cast member Jon Langford. This unique double bill was a "Recommended" screening by Cine-File, Chicagos guide to independent and alternative cinema. 

==Plot==
At Last, Okemah! is a comedic retelling of Don Quixote featuring Jeff-nominated actor Kevin Viol in the lead role of Winston Thomas, a former hipster who becomes the self-styled greatest, most authentic folk singer of all time. After a mystical encounter, Winston embarks on a journey to Okemah, Oklahoma to be anointed by the spirit of Woody Guthrie.

==History==
At Last, Okemah! was shot on high definition digital video in 6 days in the spring of 2009. Most of the actors in the movie are well known in the worlds of Chicago music, theater, television and independent film; in addition to Viol and Langford, the cast includes noted thespians Mia Park (host of televisions Chic-a-Go-Go), Suzy Brack, Paul Perroni and Duane Sharp (star of Zen Noir). "At Last, Okemah!" is dedicated to writer Miguel de Cervantes and musician Woody Guthrie.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 