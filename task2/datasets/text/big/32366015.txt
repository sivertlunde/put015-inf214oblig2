Prairie Fever
{{Infobox film
| name           = Prairie Fever
| image          = Prairie Fever poster.jpg
| image_size     =
| alt            =
| caption        =
| director       = Stephen Bridgewater David S. Cass Sr. Michael Moran
| writer         = Steven H. Berman
| narrator       =
| starring       = Kevin Sorbo Lance Henriksen Dominique Swain
| music          = Joe Kraemer
| cinematography = Al Lopez
| editing        = Jennifer Jean Cacavas
| studio         = Grand Army Entertainment Larry Levinson Productions Blockbuster
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 Western directed by Stephen Bridgewater, starring Kevin Sorbo, Lance Henriksen and Dominique Swain.

==Plot summary==
 
Preston Biggs (Kevin Sorbo) a former sheriff of Clearwater, escorts three women suffering from prairie fever to Carson City. Lettie (Jillian Armenante) tried to kill her husband, Abigale (Dominique Swain) too fragile for prairie life, and Bible-quoting Blue (Felicia Day) just snapped on her farm, with the help of a gambler Olivia (Jamie Anne Allman). 

==Cast==
* Kevin Sorbo as Sheriff Preston Biggs
* Lance Henriksen as Monte James
* Dominique Swain as Abigail
* Jamie Anne Allman as Olivia Thibodeaux
* Jillian Armenante as Lettie
* Felicia Day as Blue
* Lucy Lee Flippin as Faith
* Robert Norsworthy as Bartender
* Blake Gibbons as Charlie
* Don Swayze as James
* Richard Clarke Larsen as Carson City Hotel Clerk
* Silas Weir Mitchell as Frank
* Ken Magee as Homer Chris McKenna as Sheriff Logan
* E.E. Bell as Luke

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 