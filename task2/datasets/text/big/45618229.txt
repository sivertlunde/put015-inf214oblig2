Currito of the Cross (1949 film)
{{Infobox film
| name =   Currito of the Cross 
| image =
| image_size =
| caption =
| director = Luis Lucia
| producer = 
| writer = Alejandro Pérez Lugín (novel)   Antonio Abad Ojuel       Luis Lucia
| narrator =
| starring = Pepín Martín Vázquez   Jorge Mistral   Manuel Luna   Nati Mistral Juan Quintero  
| cinematography = José F. Aguayo   Juan Serra
| studio = CIFESA
| distributor = CIFESA
| released = 5 January 1949
| runtime = 98 minutes
| country = Spain Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
}} novel of the same title by Alejandro Pérez Lugín. 

==Cast==
*  Pepín Martín Vázquez as Currito de la Cruz 
* Jorge Mistral as Ángel Romera Romerita  
* Manuel Luna as Manuel Carmona  
* Nati Mistral as Rocío  
* Tony Leblanc as Gazuza  
* Juan Espantaleón as Don Ismael  
* Félix Fernandez (actor)|Félix Fernandez as Copita 
* Amparo Martí as Sor María 
* Francisco Bernal as Banderillero de Romerita  
* Eloísa Muro as Teresa  
* Arturo Marín as Marqués  
* Rosario Royo as Manuela   Manuel Requena as El Gordo 
* María Isbert as Margaret  
* José Prada as Doctor  
* Alicia Torres 
* Santiago Rivero as Miembro cuadrilla Carmona

== References ==
 
 
==Bibliography==
* Bentley, Bernard. A Companion to Spanish Cinema. Boydell & Brewer 2008. 

== External links ==
* 

 
 
 
 
 
 
 

 