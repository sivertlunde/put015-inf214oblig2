True Confession
{{Infobox Film
| name           = True Confession
| image          = True Confession- 1937 Poster.png
| image_size     = 
| caption        = 1937 Theatrical Poster
| director       = Wesley Ruggles
| producer       = Albert Lewin
| writer         = Georges Berr (Play) Louis Verneuil (Play) Claude Binyon
| starring       = Carole Lombard Fred MacMurray John Barrymore Frederick Hollander
| cinematography = Ted Tetzlaff
| editing        = Paul Weatherwax	
| distributor    = Paramount Pictures
| released       = December 24, 1937
| runtime        = 85 minutes
| country        = United States
| language       = English
}}
 Cross My Heart.

==Plot==
Helen Bartlett (Lombard) is the wife of the honest lawyer Ken (MacMurray). She is a "writer" but cannot think of anything to write and instead lives in her fantasy world of telling lies. When she discovers that they are broke, she attempts to get Ken to take a case of a man who stole hams. Ken finds out that the man really did steal the hams, and therefore does not take the case. Helen is forced to get a job as a secretary for businessman Otto Krayler (John T. Murray). While working, he attempts to seduce Helen, which causes Helen to quit the job. However, she discovers that she accidentally left her hat and coat at the apartment. She returns only to find that Otto Krayler has been killed and $12,000 of money the supposed motive. The police suspect Helen and take her into custody. To further complicate her situation, Helen divulges a vivid account of the murder, discussing how she did it and everything, and then says that she had nothing to do with it. 

Ken represents Helen at the trial and believes that there is no way that the jury will believe that Helen did not commit the murder, and therefore has her plead self-defense. As the trial continues, an obnoxious man named Charley Jasper (Barrymore) believes that Helen did not murder Krayler, but he keeps it to himself.

Helen wins the case and publishes a hugely successful novel of her life story. Having earned a fortune, Helen and Ken buy a lavish home on Marthas Lake, but Ken expresses remorse that their fortune has come out of crime. Helen wonders if she should confess her innocence, but Ken states that perjury would be worse than the crime she had already committed. Meanwhile, Charley visits Helen and Ken with Kraylers wallet and attempts to blackmail them into saying that he (Charley) killed Krayler and having Helen put into perjury. Helen then tells Ken that she did not kill Krayler and has Charley confess that his brother-in-law was the real murderer. Ken leaves the house, sickened by Helens lying, but Helen chases after him and lies once more by saying that she is pregnant. Ken then takes Helen into the house in an attempt to teach her not to lie.

==Cast==
*Carole Lombard as Helen Bartlett
*Fred MacMurray as Ken Bartlett
*John Barrymore as Charley Jasper
*Una Merkel as Daisy McClure
*Porter Hall as Prosecutor
*Edgar Kennedy as Darsey
*Lynne Overman as Bartender
*Irving Bacon as the coroner
*Fritz Feld as Kraylers butler
*John T. Murray as Otto Krayler
*Richard Carle as Judge
*Hattie McDaniel appears in a small role as a maid named Ella.

==External links==
*   
*  

 

 
 
 
 
 
 
 
 
 