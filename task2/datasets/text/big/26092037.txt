Sixty Glorious Years
{{Infobox film
| name           = Sixty Glorious Years
| image_size     =
| image	=	Sixty Glorious Years FilmPoster.jpeg
| caption        =A poster with the films US title: Queen of Destiny
| director       = Herbert Wilcox
| producer       = Herbert Wilcox
| writer         = Charles de Grandcourt (writer) Miles Malleson (writer) Sir Robert Vansittart (dialogue) Sir Robert Vansittart (scenario)
| narrator       =
| starring       = See below Anthony Collins
| cinematography = Freddie Young
| editing        =
| distributor    =
| released       =  
| runtime        = 95 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}

Sixty Glorious Years is a 1938 British color film directed by Herbert Wilcox. The film is a sequel to the 1937 film Victoria the Great.

The film is also known as Queen of Destiny in the US.

== Cast == Queen Victoria Prince Albert Duke of Wellington
*Walter Rilla as Prince Ernst Charles Carson Sir Robert Peel Lord Palmerston
*Lewis Casson as Lord John Russell
*Pamela Standish as The Princess Royal Gordon McLeod John Brown
*Henry Hallett as Joseph Chamberlain
*Wyndham Goldie as Arthur Balfour William E. Gladstone Herbert H. Asquith
*Derrick De Marney as Benjamin Disraeli
*Joyce Bland as Florence Nightingale Frank Cellier as Lord Derby Lord Salisbury
*Aubrey Dexter as The Prince of Wales 
*Stuart Robertson as Mr. Anson
*Olaf Olsen as Prince Fredrick
*Marie Wright as Maggie
*Laidman Browne as Gen. Gordon
*Greta Schröder as Baronin Lehzen

== External links ==
* 
* 

 

 

 
 
 
 
 
 
 
 


 