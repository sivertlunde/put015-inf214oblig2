Hideko the Bus-Conductor
 Japanese film directed by Mikio Naruse. It is a short film of 54 minutes.

==Synopsis==
Hideko, a young lady working as a conductor with a bus company in Kofu, Yamanashi, has an idea for her bus that could avert the dwindling number of passengers. She asks the visiting author Ikawa to write for her a commentary to the local sites that she can read out to her passengers as they travel through the countryside. However, they are out on their practice run, when ...

==Background==
Hideko Takamine, in the principal role as Hideko, was already a famous film star for her childhood roles, and the name of the main character in the original story was changed from Koma to her own name in the film. This was the first film that Naruse, who had experience with literary adaptations, made with Takamine. Most of the film is shot on location in Kofu.

This was the last film that Naruse made before Japan declared war in December 1941. The world presented in the film, however, is peaceful and pleasant, far removed from Japanese peoples life at the time of shortages and censorship.

Naruse scholar Catherine Russell has described this film as "a remarkable film about a young woman coming out as a professional, articulate, speaking subject." 

==Release==
This film has not been released on DVD.

==References==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 

 