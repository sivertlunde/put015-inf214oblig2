Shubho Mahurat
{{Infobox film
| name     = Shubho Mahurat
| image          =Shubho Mahurat.jpg
| caption     = DVD cover for Shubho Mahurat.
| director       = Rituparno Ghosh
| writer         = Agatha Christie (story)  Rituparno Ghosh (screenplay)
| music          = Debojoyti Mishra
| starring       = Sharmila Tagore Raakhee Nandita Das Tota Roy Chowdhury Sumanta Mukherjee Anindya Chatterjee
| released   = 2003
| runtime        = 150 minutes
| country        = India Bengali
| music = 
}}
 Bengali feature film directed by Rituparno Ghosh. The film is based on Agatha Christies Miss Marple story The Mirror Crackd from Side to Side.

==Plot== NRI producer, Padmini Chowdhury (Sharmila Tagore), has come to India to invest in a film. Her second husband Sambit Roy (Sumanta Mukherjee), an out-of-work director is assigned the job of direction.

The out-of-work director had a shady past. The witness to his shady past was an aspiring actor, who was subsequently thrown out of the acting circuit. Thereafter, this actor started a catering service for the film unit.

The NRI producer insisted on casting a retired actress, Kakoli (Kalyani Mandal) in a prominent role. Kakoli, a drug addict, subsequently died under mysterious circumstances, on the day of Shubho Mahurat. The journalist Mallika Sen (Nandita Das) was the only person present at the time of the  death. The suspicion naturally fell on the husband of the actress.
 IPS officer named Arindam (Tota Roy Chowdhury) took up the investigation. During the course of investigation, Arindam befriends Mallika. Meanwhile, Mallika caught the fancy of a still photographer named Jojo (Anindya Chatterjee) who, incidentally, is closely related to the NRI producer.

This NRI producer had a past no less interesting than the other protagonists. The NRI producer was an actress of repute in her heyday. She divorced her husband and left the country to settle abroad. Before that, she had given birth to an abnormal child. The course of the film revealed that she bore a grudge against Kakoli for spreading a contagious disease to her while she was pregnant, resulting in the abnormal child.

Another amorous affair runs parallel to the main theme: The camera assistant was repenting for his past affair with the hairdresser. The hairdresser had her problems and she was extracting money very often from the camera assistant. All through the movie, the aunt of the journalist, Ranga Pishima (Raakhee), gave vital leads to the investigation through deductive logic. She was able to read the mind of the niece correctly. Her valuable inputs finally leads to solving of the murder mystery.

==Cast==
* Sharmila Tagore as Padmini Chowdhury
* Raakhee as Ranga Pishima
* Nandita Das as Mallika Sen
* Sumanta Mukherjee as Sambit Roy
* Kalyani Mandal as Kakoli

==Awards==
* 2003 - National Film Award for Best Supporting Actress for Raakhee
* 2003 - National Film Award for Best Feature Film in Bengali

==External links==
* 

 
 

 
 
 
 
 
 
 