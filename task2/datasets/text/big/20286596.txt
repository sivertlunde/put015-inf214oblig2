Kannamma
{{Infobox film
| name           = Kannamma
| image          =
| image_size     =
| caption        = 
| director       = S. S. Baba Vikram
| writer         = M. Karunanidhi| Meena Bose Prem
| producer       =
| editing        = Vikram Raja
| released       = February 4, 2005 Tamil
| music          =  S. A. Rajkumar
}}

Kannamma ( ) is a 2005 Tamil language movie directed by S. S. Baba Vikram. Karunanidhi is famous for his dialogues in "Parasakthi", which saw the rise of Sivaji Ganesan as a Tamil superstar.Kannamma opens with a soliloquy on the people, the nation and the DMK patriarchs hopes for its future. But an inauspicious note is introduced with the mocking recitation of Subramania Bharatis famous lines addressed to "Kannamma".The film is peppered with messages on nationalism, caste and communal harmony. But it lacks the spellbinding effect of certain war films.

==Plot==
The story is that of a rich medical student, Kannamma - played by Meena - who falls in love with Anandan (Premkumar) after he saves her from an acid attack by her driver, Babu (Karate Raja).

Anandans friend Madan (Venkat) becomes Kannammas ever-vigilant sentinel, risking his own marriage with Mala (Vindhya).

Kannamma comes to know of Anandans martyrdom through TV. Her baby is disputed and she is brought before a village council. These scenes sorely test the viewers intelligence.

With a new generation gradually taking over the party, octogenarian Karunanidhis message perhaps lies in the two songs he has written, one of which is addressed to youth. The other song, "Iru vizhi mazhai", is sung by Vani Jayaram.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- Meena ||Kannamma -Heroine
|- Prem ||Anandan-Hero
|- Madan
|- Mala
|-
|Vadivukkarasi||
|- Karate Raja||Driver Babu
|- Kuyili (actress)|Kuyili||
|-
|Chandrasekhar||
|- Vaiyapuri
|}

== Reception ==
 

== References ==
 

 
 
 
 


 