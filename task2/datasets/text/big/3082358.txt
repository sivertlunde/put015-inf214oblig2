Patty Hearst (film)
{{Infobox film
| name           = Patty Hearst
| image          = Pattyhearstposter.jpg
| image_size     =
| caption        = Film poster
| director       = Paul Schrader
| producer       = Marvin Worth
| writer         = Nicholas Kazan Patricia Hearst (book) Alvin Moscow (book)
| narrator       = William Forsythe Ving Rhames Frances Fisher Jodi Long Olivia Barash Dana Delany Scott Johnson
| cinematography = Bojan Bazelli
| editing        = Michael R. Miller
| distributor    = Atlantic Releasing
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1,223,326
}} Patricia Hearst Every Secret Thing (co-written with Alvin Moscow), which was later rereleased as Patty Hearst – Her Own Story.

The film depicts the kidnapping of student Patty Hearst by the Symbionese Liberation Army, her transformation into an active follower of the SLA after a long-lasting imprisonment and process of brainwashing, and her final arrest after a series of armed robberies.

==Cast==
* Natasha Richardson – Patricia Hearst William Forsythe – Teko
* Ving Rhames – Cinque
* Frances Fisher – Yolanda
* Jodi Long – Wendy Yoshimura
* Olivia Barash – Fahizah
* Dana Delany – Gelina
* Marek Johnson – Zoya
* Kitty Swink – Gabi
* Peter Kowanko – Cujo (as Pete Kowanko) Tom ORourke – Jim Browning
* Scott Kraft – Steven Weed
* Jeff Imada – Neighbor
* Ermal Williamson – Randolph A. Hearst
* Elaine Revard – Catherine Hearst
* Destiny Reyes Allstun - Vicky Hearst

==Production==
Patty Hearst premiered at the 1988 Cannes Film Festival on May 13 in the feature film competition.    The film opened on September 23, 1988 in the US and grossed $601,680 in its opening weekend. It made a total domestic gross of $1,223,326. 

Schrader has said that he made Patty Hearst on a low budget and for a small salary to recover from the commercial and artistic failure of Light of Day.  The film has a very distinctive visual style, not least because it is made almost entirely from Patty Hearsts point of view and therefore the first part is set largely in a dark closet with occasional blinding shafts of light when the captors open the door. 

The ending, like those of Schraders American Gigolo and Light Sleeper, echoes that of Robert Bressons Pickpocket (film)|Pickpocket, showing the central character physically imprisoned but beginning to think hopefully about a new phase in her life.

==Reception==
The film garnered a generally mixed critical response, although Richardsons performance was applauded by most critics. Amongst credited critics, the film has a rating of 50% positive reactions on Rotten Tomatoes, with 8 reviews counted.  Writing in the New York Times, Vincent Canby praised the film "Patty Hearst is a beautifully produced movie, seen entirely from Pattys limited point of view. It is stylized at times, utterly direct and both shocking and grimly funny."  Roger Ebert writing for the Chicago Sun-Times praised Richardsons performance; "The entire film centers on the remarkable performance by Natasha Richardson as Hearst." but concluded that "This whole story seemed so much more exciting from the outside." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 