Beast of the Yellow Night
 
 
{{Infobox film
| name           = Beast of the Yellow Night
| image          = Beast of the Yellow Night.jpg
| image size     =
| caption        = Film poster
| director       = Eddie Romero John Ashley David J. Cohen Roger Corman Beverly Miller
| writer         = Eddie Romero
| narrator       =
| starring       = John Ashley 
| music          = Nestor Robles
| cinematography = Justo Paulino
| editing        = Ben Barcelon
| distributor    = New World Pictures (USA)
| released       = 1971
| runtime        = 87 minutes
| country        = Philippines Tagalog
| budget         =
}} American horror film, directed by Eddie Romero. 

==Plot==
  John Ashley) from death on condition that he become his disciple. Satan has Langdon inhabit the bodies of several people over the years, bringing out their latent evil. As it turns out, he becomes a hairy murderous beast — a werewolf on the rampage carrying out the evil deeds of the Devil for the next 25 years.

==Cast== John Ashley ...  Joseph Langdon/Philip Rogers
*Mary Charlotte Wilcox ...  Julia Rogers (as Mary Wilcox)
*Leopoldo Salcedo ...  Inspector Santos
*Eddie Garcia ...  Det. Lt. Campo
*Ken Metcalfe ...  Earl Rogers
*Vic Diaz ...  Satan
*Andres Centenera...  Blind Man
*Ruben Rustia
*Don Linman
*José García
*Rey PJ Abellana
*Carpi Asturias
*Jose Roy Jr.
*Criselda
*Joonee Gamboa
*Peter Magurean
*Nora Nunez
*Johnny Long
*John Carradine

==Production==
Ashley had made a series of movies in the Philippines with   for New World Pictures. Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 21  

The success of the movie led to Corman making a series of films in the Philippines, including The Big Doll House.

==Release==
 
A full-length RiffTrax for the movie was released on August 8, 2014, with commentary by Mike Nelson, Kevin Murphy, and Bill Corbett of Mystery Science Theater 3000 fame.

==Reception==
 


==References==
 

==External links==
*  
*  at Internet Archive
 
 

 
 
 
 
 
 
 
 
 
 
 

 
 