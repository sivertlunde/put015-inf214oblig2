Land of Hunted Men
{{Infobox film
| name           = Land of Hunted Men
| image_size     = 
| image	         = Land of Hunted Men FilmPoster.jpeg
| caption        = 
| director       = S. Roy Luby
| producer       = Clark L. Paylow (associate producer) George W. Weeks (producer)
| writer         = William L. Nolte (story) Elizabeth Beecher (screenplay)
| narrator       = 
| starring       = See below
| music          = 
| cinematography = James S. Brown Jr.
| editing        = S. Roy Luby
| studio         = 
| distributor    = 
| released       =  
| runtime        = 58 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
}}

Land of Hunted Men is a 1943 American film directed by S. Roy Luby, one of the Range Busters films.

== Plot ==
 

== Cast ==
*Ray Corrigan as "Crash" Corrigan  Dennis Moore as Denny Moore
*Max Terhune as "Alibi" Terhune 
*Elmer as Elmer, Alibis Dummy
*Phyllis Adair as Dorrie Oliver Charles King as "Faro" Wilson
*John Merton as Pelham (Mine Manager)
*Ted Mapes as Henchman Piebald
*Frank McCarroll as Henchman Tabasco
*Forrest Taylor as "Dad" Oliver Steve Clark as Sheriff Andy Wallace
*Fred "Snowflake" Toones as Snowflake

== Soundtrack ==
* "Trail to Mexico" (Written by Johnny Lange and Lew Porter)

== External links ==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 


 