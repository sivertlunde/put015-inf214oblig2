Island of Fire
 
 
{{Infobox film
| name           = Island of Fire
| image          = Island-of-Fire-poster.jpg
| caption        = Film poster
| director       = Chu Yen-ping Jimmy Wang Yu Ko Chun-hsiung
| writer         = Lee Fu Yip Wan-Chiu Tony Leung Golden Harvest
| music          = Fu Lap Eckart Seeber
| released       =  
| runtime        = 96 min.
| country        = Taiwan Hong Kong
| language       = Mandarin
}}
 Hong Kong action film directed by Chu Yen-ping, and starring Jackie Chan, Andy Lau, Sammo Hung, Tony Leung Ka-fai and Barry Wong. It was shot in Taiwan and the Philippines. 
 Jackie Chan film, displaying a Jackie Chan image on the cover as though the lead actor. In fact, Chan and only appears in a supporting role, with Tony Leung Ka-Fai as the central character.
 1982 film Fantasy Mission Force.

==Synopsis==
A police officer (Tony Leung Ka-fai) goes undercover in a prison, hoping to determine how the fingerprints on a recently killed felon could belong to a con who had been executed three months before. While inside, he is tortured for getting involved in internal matters.

His fellow prisoners include Chan (who accidentally killed a card player while trying to raise money for an operation to save his girlfriends life), Andy Lau (who has himself thrown in jail to exact revenge for his dead brother), Wang (a leader of the prisoners), and Sammo Hung (a compassionate but pathetic inmate who frequently escapes to visit his young son).

When Leung kills an especially corrupt guard and is sentenced to a firing squad, he learns that the "executed" convicts are actually used by the warden (Ko Chun-hsiung) as hitmen to assassinate untouchable criminals.

==Cast==
* Jackie Chan as Da Chui / Lung (Hong Kong version) / Steve (US version)
* Andy Lau as Iron Ball / Lau / Boss Lee
* Sammo Hung as Fatty John Liu Hsi Chia
* Tony Leung Ka-fai as Wei Wang / Andy / Andrew
* Barry Wong as Inspector Wong Jimmy Wang Yu as Kui / Lucas (as Wang Yu)
* Ko Chun-hsiung as Prison Chief / Prison Superintendent
* Tou Chung-hwa as Chiu / Charlie (as Tao Chung Hwa)
* Jack Kao as Ho Lin
* Ken Lo as Bodyguard
* Rocky Lai as Iron Balls thug / Prisoner
* Chin Ho as Iron Balls assistant
* Teddy Yip Wing-cho as Prisoner
* Wang An
* Yen Ru-chen
* Kao Ming
* Kang Ho
* Fang Jing
* Yuen Yi
* Wai Hung-ho
* Tou Hu
* Wang Yao
* Bang Yu
* Chien Tsao

==Versions==
There are three different versions of this film: a 93-minute Hong Kong version, a 96-minute American version was released by Columbia Tristar Home Video in 2000 and later by Lionsgate in 2011 and a 125-minute Taiwanese version which focuses more on character development and plot detail.

==See also==
* Andy Lau filmography
* Jackie Chan filmography
* List of Hong Kong films
* List of Taiwanese films
* Sammo Hung filmography

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 