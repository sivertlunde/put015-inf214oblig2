Deadly Impact
{{Infobox film
| name           = Deadly Impact
| image          = Deadly Impact.jpg
| alt            =
| caption        = film poster
| director       = Robert Kurtzman
| producer       =  
| writer         = Alexander Vesha
| starring       =  
| music          = Steven Gutheinz Paul Elliott
| editing        = Andrew Sagar
| studio         = Metro-Goldwyn-Mayer
| distributor    = MGM Home Entertainment
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = US Dollar|$6,000,000
| gross          =
}} also known American crime thriller film written by Alexander Vesha and directed by Robert Kurtzman.     The film stars Sean Patrick Flanery, Joe Pantoliano, Carmen Serano, and Greg Serano. {{cite news
| last1=Barton
| first1=Steve
| title=Deadly Impact Trailer from MGM Home Entertainment
| url=http://www.dreadcentral.com/news/36755/deadly-impact-trailer-mgm-home-entertainment
| accessdate=August 21, 2014
| publisher=Dread Central
| date=April 2, 2012}} 
 {{cite news
| last1=Barton
| first1=Steve
| title=Deadly Impact Crashing onto Unrated DVD April 20th
| url=http://www.dreadcentral.com/news/36200/deadly-impact-crashing-unrated-dvd-april-20th
| accessdate=August 21, 2014
| publisher=Dread Central
| date=March 2, 2012}} 
 {{cite news
| last1=Longsdorf
| first1=Amy
| title=New on DVD this week
| url=http://www.northjersey.com/arts-and-entertainment/new-on-dvd-this-week-1.953887
| accessdate=August 21, 2014 The Record
| date=April 23, 2010}} 
==Plot==
Detective Tom (Sean Patrick Flanery) and his partner Ryan Alba (Greg Serano) have spent the last three years unsuccessfully trying to locate the Lion (Joe Pantoliano), an international hitman that has always eluded their grasp. They believe that theyve found his location via a tipster but instead find Tom’s wife Kelly (Michelle Greathouse) tied up in a basement filled with explosives, surrounded by a black outline. The Lion calls Tom and commands him to kill Kelly or risk the entire house blowing up and killing everyone inside. Any attempt to evacuate the house or rescue Kelly will result in everyone getting blown up and the Lion will not accept any substitutions, as he wants Tom to suffer for messing up the Lions network in the Southwest USA. His only mercy is that Tom may turn out the lights in the basement. With no other option, Tom has the lights turned off and he shoots and kills Kelly.

The film flashes forwards to eight years later to show a grieving Tom, who is now retired and living in Mexico. As hes had past experiences dealing with the Lion, FBI agent Isabel Ordonez (Carmen Serano) travels out to persuade him to help track down and capture the Lion. She shows him evidence that the Lion will be meeting associates at a nightclub. Tom refuses but secretly copies down the address so he can take his own revenge for Kellys death. At the nightclub Tom is initially unsuccessful at tracking down the Lion as he does not know what the other man looks like, even as the Lion walks directly past him in order to meet with his associate. The Lion, who is aware of the presence of both Tom and the FBI agents, the receives his requested materials from his associate before killing the associate and detonating a bomb to cover his escape. Tom quickly follows after the Lion and after an extended chase scene, the Lion manages to subdue Tom and is about to cut his throat when an unmarked police car enters the alley, forcing the Lion to abandon Tom and make his escape. The FBI agent driving the car, "Hops" (David House), mistakes Tom for one of the Lions associates and tries to arrest him. Hops is later corrected on this and Tom is brought on to assist the FBIs talk force along with his old partner Ryan, who is now part of the FBI. The FBI believes that the Lion is targeting Senator Cordero (Fredrick Lopez), who used to be a DEA agent.

As more bombs go off, the Lions true identity is revealed (David Kaplow) and he kidnaps Ryans daughter in an attempt to have Ryan assassinate Cordero in exchange for his daughters safety. Hes successful, as Ryan does as hes instructed, but Ryan is in turn shot and killed by Corderos bodyguard. Eventually Kaplow makes his big move and bombs the FBI headquarters, during which time he kidnaps Ordonez. Tom makes his way to Ordonezs house, where he sees that Kaplow has tied her up and strapped a bomb to the agent. The two men fight and Kaplow manages to force Tom to handcuff himself. A policeman arrives on the scene but is killed by Kaplow, who is wounded in the process. Kaplow then escapes the house and detonates the bomb, believing that this is the end of Tom and Ordonez. However Kaplow then receives a call from Tom stating that they are not dead and that Kaplows bullet proof vest is rigged to bombs. Finally, Tom detonates the bomb and Kaplow explodes inside the police car.

==Principal cast==
 
* Sean Patrick Flanery as Tom Armstrong
* Joe Pantoliano as David Kaplow
* Carmen Serano as Isabel Ordonez
* Greg Serano as Ryan Alba
* Amanda Wyss as Julie Mulligan
* Robert Kurtzman as Homeowner
* David House as Special Agent William Hopter
* Julianne Flores as Amy
* Mike Miller as Hollis
* Fredrick Lopez as Senator Cordero
* Kevin Wiggins as Captain Duvall
* Michelle Greathouse as Kelly Armstrong
* James Tarwater as Skittles
* Luce Rains as Red Tie
* Mike Seal as Associate
* Kieran Sequoia as Aide
* Joel Bryant as FBI Agent
* John Koyama as X-Ray Agent
 

==Production==
According to director Robert Kurtzman, the original script was entitled "Angelmaker", with the working title changed to "To Live and Die" before settling on "Deadly Impact".    The director said that Metro Goldwyn Mayer let him make the film right away after seeing the script. The film was shot in Albuquerque, New Mexico, USA.  Parts of the film was shot in an abandoned courthouse,  which was made to look more modern. The rest of the film was shot within a ten block radius of that area. The opening scene took around half of a day to shoot, and the entire shoot lasted from August 21, 2007 to September 20, 2007. The film completed on July 21, 2008. 

==Release== French television as Impact mortel.   

==Reception==
Critical reception for Deadly Impact has been largely negative.             Reviewers for DVD Talk heavily panned the film,    with one writing "Rather uninspired and more than a little bit predictable, Deadly Impact doesnt really bring much of interest to a genre already riddled with clichés."    DVD Verdict also panned Deadly Impact, cautioning readers that it was "one hell of a bomb" and to "  waste any time on this dud".    Beyond Hollywood was also critical in their review, stating that "Deadly Impact is an almost painful experience to behold, even when compared to the stuff Steven Seagal and Val Kilmer are pumping out these days. After the film’s mildly intriguing opening number, the production promptly loses its cinematic footing and never recovers."   

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 