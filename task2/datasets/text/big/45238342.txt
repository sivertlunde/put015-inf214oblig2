Yaarana (2015 film)
{{Infobox film
| name           = Yaarana
| image          = File:Yaarana_Poster.jpg
| caption        = 
| director       = Ranbir Pushp
| producer       = Sukhbir Sandhar / Ranjana Kent
| writer         = 
| screenplay     = 
| story          = 
| starring       = Gavie Chahal Geeta Zaildar Yuvraj Hans  Yuvika Chaudhary  Kashish Singh Rupali Sood, Yashpal Sharma, Dolly Minhas & Puneet Issar. 
| music          = Gurmit Singh
| cinematography = W.B. Rao
| editing        = 
| studio         = Sukhbir Sandhar Films PVT Ltd / Karina Films
| distributor    = Om Jee Cineworld, VIP Films & Entertainment & Studio 7 Production
| released       =  
| run time        = 
| country        = India Punjabi
| budget         = 
| gross          =
}}
 Punjabi film directed by Ranbir Pushp. The film is produced by Sukhbir Sandhar & Ranjana Kent with Starring Gavie Chahal, Geeta Zaildar, Yuvraj Hans,Kashish Singh in the lead. The film is scheduled for release on April 24, 2015.  Movie is produced under banner Sukhbir Sandhar Films PVT Ltd & Karina Films.

==Cast==
*Gavie Chahal 
*Geeta Zaildar
*Yuvraj Hans 
*Yuvika Chaudhary 
*Kashish Singh 
*Rupali Sood 
*Yashpal Sharma 
*Dolly Minhas 
*Puneet Issar

==Plot==
The film revolves around the common sport of Football, Football is the one main thing that evokes the emotions of all kinds of people from the communities and Colleges of Punjab. The film is set in the Bhai Gurdas Group of Institutes, which has the history and culture of being the best sports college in Punjab. 

== Soundtrack ==
{{Infobox album
| Name = Yaarana
| Type = Soundtrack
| Artist = 
| Cover = 
| Released =   Feature film soundtrack
| Length =  
| Label = Tips Music
}}
The soundtrack is composed by Gurmit Singh Gurcharan Singh & Manoj Nayan

{{Track listing
| lyrics_credits = no
| music_credits = yes
| extra_column = Singer(s)
| total_length = 25:48
| title1 = Patola Patna
| lyrics1= 
| music1= Gurmeet Singh, Gurcharan Singh & Manoj Nayan
| extra1 = Geeta Zaildar, Yuvraj Hans
| length1 = 3:17
| title2 = Teri Aankh Sharabi
| lyrics2= 
| music2=  Gurmeet Singh, Gurcharan Singh & Manoj Nayan
| extra2 = Geeta Zaildar
| length2 = 4:13
| title3 = Pillak Pu Baby
| lyrics3= 
| music3=  Gurmeet Singh, Gurcharan Singh & Manoj Nayan
| extra3 = Gurmeet Singh
| length3 = 2:44
| title4 = Tan Tana Tan
| lyrics4= 
| music4=  Gurmeet Singh, Gurcharan Singh & Manoj Nayan
| extra4 = Gurmeet Singh, Yuvraj Hans
| length4  = 4:25
| title5 = Tere Khyalan
| lyrics5= 
| music5=  Gurmeet Singh, Gurcharan Singh & Manoj Nayan
| extra5 = Yuvraj Hans
| length5 = 3:11
| title6 = Tere Nain Mere Nain
| lyrics6= 
| music6=  Gurmeet Singh, Gurcharan Singh & Manoj Nayan
| extra6 = Yuvraj Hans Sonika Sharma
| length6 = 4:13
| title7 = Dhoom Dhadaka Dhoom
| lyrics7= 
| music7=  Gurmeet Singh, Gurcharan Singh & Manoj Nayan
| extra7 = Daler Mehndi
| length7 = 3:01
| title8 = Mahi Ve
| lyrics8= 
| music8=  Gurmeet Singh, Gurcharan Singh & Manoj Nayan
| extra8 = Tochi Rana
| length8 = 3:37
}}

== References ==
 

== External links ==
*  
* 
*[https://www.facebook.com/NavalpreetRangiProductions Yaarana Overseas Distribution by VIP Films & Entertainment 
 
 
 

 