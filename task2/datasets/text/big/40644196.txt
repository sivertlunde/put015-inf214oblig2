The Killing of America
{{Infobox film
| name = The Killing of America
| image = The_Killing_of_America_DVD_Cover.jpg
| image_size = 
| alt = 
| caption =
| director = Sheldon Renan Leonard Schrader
| producer = Leonard Schrader Mataichirô Yamamoto
| writer = Chieko Schrader Leonard Schrader Chuck Riley
| music = W. Michael Lewis Mark Lindsay
| editing = Lee Percy
| studio = Filmlink International Towa Productions
| distributor = 
| released = February 1982
| runtime = 90 minutes
| country = United States Japan
| language = English
| budget = 
| gross =
}} American documentary film directed by Sheldon Renan and Leonard Schrader. The film was premiered in New York City in February 1982 and was recently shown at the 2013 Fantasia Festival.   

==Synopsis==
The Killing of America focuses on what the director feels is the decline of the United States. It features interviews from Ed Dorris, a retired sergeant of the Los Angeles Sheriffs Department, as well as Los Angeles County Coroner Thomas Noguchi. The documentary also shows several interviews with convicted killers such as Sirhan Sirhan as well as footage of murders and news broadcasts.

==Cast==
*Chuck Riley as Narrator
*Ed Dorris
*Thomas Noguchi
*Sirhan Sirhan
*Wayne Henley
*Ed Kemper

==Release history==
The Killing of America was initially shown in New York City in 1982 at The Public Theater,  but did not receive a commercial release in the United States, although it did receive a home video release in Britain.    The film received a wide release in Japan, where financial backers reportedly pressured Renan to add footage of peace vigils for John Lennon as a way to make the documentary less depressing.  Years later the documentary would receive a 2013 United States release at Fantasia Festival. 

==Reception== mondo genre". 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 


 