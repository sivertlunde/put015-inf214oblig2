L'Auberge Espagnole
{{Infobox film
| name           = LAuberge Espagnole
| image          = Aubergeespagnole.jpg
| border         = yes
| caption        = French theatrical release poster
| director       = Cédric Klapisch
| producer       = Bruno Levy
| writer         = Cédric Klapisch
| starring       = Romain Duris Judith Godrèche Audrey Tautou
| music          = Various Artists
| cinematography = Dominique Colin
| editing        = Francine Sandberg
| distributor    = Fox Searchlight Pictures (USA, Australia, New Zealand, South America, Asia, Scandinavia )
| released       =  
| runtime        = 122 minutes
| country        = France Spain
| language       = French Spanish English Catalan Danish German Italian
| budget         = €5,300,000
| gross          = $31,024,110 
}}

LAuberge Espagnole ( ; literally: "the Spanish inn"; released in some English-speaking territories as Pot Luck or The Spanish Apartment) is a 2002 French-Spanish film directed and written by Cédric Klapisch. It is a co-production between Spain (Mate Producciones S.A., Via Digital) and France (BAC Films, Ce qui me meut, France 2 Cinéma, Studio Canal).  

It is about Xavier (Romain Duris), an economics graduate student studying for a year in Barcelona, Spain as part of the Erasmus programme, where he encounters and learns from a group of students who hail from all over Western Europe. It is the first part of the self-titled "Spanish Apartment Trilogy" of films centered on the character of Xavier and his progression from student to family man and friends he initially encounters in a student share-house in Spain.

The films portrayal is in the first-person perspective of the main character, Xavier, and is hence mainly narrated in French. Some of the dialogue is in English and a significant amount is in Spanish, as well as small amounts in Catalan, Danish, German and Italian.
 Russian Dolls (2005) and Chinese Puzzle (2013). 

==Plot==
Xavier (Romain Duris), a 24-year-old French student, leaves his country for the ERASMUS programme in Barcelona as part of his professional pursuits, despite it being against the wishes of his girlfriend Martine (Audrey Tautou). On the flight over, he meets a young French couple, a doctor and his wife (Anne-Sophie), who let him stay in their home while he searches for an apartment. Xavier manages to find an apartment with other students from England, Belgium, Spain, Italy, Germany, and Denmark, creating an ambiance of chaos and culture shock. The roommates begin to develop a unique companionship as they struggle together through their cultural and linguistic challenges in their program. Martine pays Xavier a visit and returns disappointed when she realizes things are not the same. Meanwhile Xavier develops a romantic affair with the French doctors wife, seducing her with tips he has been learning from his Belge lesbian roommate. The English roommate Wendys brother William visits for some time and turns out to be quite abrasive with his culturally insensitive comments, creating tension among the roommates. Martine eventually breaks up with Xavier, bringing him to depression and hallucinations. When Xavier seeks the French doctors advice, he reveals that Anne-Sophie told him everything and tells him to stop seeing her. As the discordance among the roommates escalates, their friendship is repaired as they team up to help Wendy elude a sticky situation (her boyfriend Alister makes a surprise visit while she was hooking up with an American). After leaving Barcelona and bidding farewell to his many close friends, Xavier returns to Paris and gets the job at the Ministry but realizes he misses his experiences that now make him a different person. He subsequently runs away on his first day and pursues his dream to become a writer, recounting the story of his experiences in the Auberge Espagnole.

==Meaning of the title== playing on the French phrase, since the main characters are all literally sharing an apartment in Spain.

==Primary cast==
* Romain Duris as Xavier
* Barnaby Metschurat as Tobias
* Judith Godrèche as Anne-Sophie
* Cécile de France as Isabelle
* Kelly Reilly as Wendy
* Audrey Tautou as Martine
* Cristina Brondo as Soledad
* Kevin Bishop as William
* Federico DAnna as Alessandro
* Christian Pagh as Lars

==Soundtrack==
LAuberge Espagnole features a diverse soundtrack, which includes:

* Radiohead - "No Surprises"
* Daft Punk - "Aerodynamic (instrumental)|Aerodynamic"
* Sonia & Selena - "Que Viva La Noche" Te Deum"
* Ali Farka Touré - "Ai Du" Opus 64 No 2 Waltz in C sharp minor"
* Africando All Stars - "Betece"
* Mala Rodriguez - "La Cocinera"

==Awards==
Win:
* César Award|César Award for Most Promising Actress — Cécile de France

Nominations:
* César Award for Best Film
* César Award for Best Director — Cédric Klapisch
* César Award for Best Actress in a Supporting Role — Judith Godrèche
* César Award for Best Writing — Cédric Klapisch
* César Award for Best Editing — Francine Sandberg

==References==
 

==External links==
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 