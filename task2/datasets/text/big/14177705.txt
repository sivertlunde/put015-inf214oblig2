The Shopworn Angel
{{Infobox film
| name           = The Shopworn Angel
| image          = The-shopworn-angel-1938.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = H.C. Potter
| producer       = Joseph L. Mankiewicz
| based on       =  
| writer         = {{Plainlist|
* Howard Estabrook Waldo Salt
}}
| starring       = {{Plainlist|
* James Stewart
* Margaret Sullavan
* Walter Pidgeon
}} Edward Ward
| cinematography = Joseph Ruttenberg
| editing        = W. Donn Hayes
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = $531,000  . 
| gross          = $1,042,000 
}}
 1938 American MGM release featured the second screen pairing of Margaret Sullavan and James Stewart following their successful teaming in the Universal Pictures production Next Time We Love two years earlier. 
 Waldo Salt The Shopworn Angel (1928), a part-talkie released by Paramount Pictures starring Nancy Carroll, Gary Cooper, and Paul Lukas.

==Plot== Broadway musical theatre star, knocks down Bill Pettigrew (James Stewart), a naive young soldier from Texas. A policeman orders the chauffeur to take Bill back to camp. During the ride, he becomes slightly acquainted with the cynical, but not cold-hearted Daisy. 

Upon their arrive at the army camp, Bill lets his buddies assume that Daisy is the date he had lied about. In fact, he has no one. When they find out the truth, they decide to get even. On their next leave, they take Bill to Daisys show, so he can introduce them. However, Daisy pretends that she is Bills girl. As they spend more time together, she begins to warm to him, much to the increasing jealousy of her wealthy real boyfriend, Sam Bailey (Walter Pidgeon), who is financing Daisys show.

When Sam takes Daisy out for an afternoon at his Connecticut estate for the first time, she tells him that Bill has shown her what true love looks like and made her realize she actually does love Sam. She also believes that the rivalry has also given new depth to Sams love for her. 

That same day, Bill learns that his unit is finally going to ship out for the fighting in European theatre of World War I|Europe. When he cannot get a leave, he goes AWOL so he can propose marriage. Daisy opts to accept so that he can sail for France with something to look forward to. Sam objects to the odd arrangement privately to Daisy, but kindly refrains from telling Bill the truth. The two marry; then Bill has to leave immediately.

He sends her cheerful letters every day. Then, a letter comes from the War Department. As Daisy is in the middle of a performance, her maid Martha takes it to Sam, sitting in the audience. When Sam opens the letter, Bills ID tag falls out. Daisy sees it, tears fill her eyes as she realizes that Bill has been killed, but she bravely finishes singing "Pack Up Your Troubles in Your Old Kit-Bag and Smile, Smile, Smile".

==Cast==
* James Stewart as Bill Pettigrew
* Margaret Sullavan as Daisy Heath
* Walter Pidgeon as Sam Bailey
* Hattie McDaniel as Martha
* Nat Pendleton as "Dice", one of Bills army buddies Alan Curtis as "Thin Lips", another army buddy
* Sam Levene as "Leer", an army buddy
* Eleanor Lynn as Sally
* Charles D. Brown as McGonigle, Daisys boss

==Production==
 
The film underwent major personnel changes during its development stages. The directing assignment first went to Richard Thorpe, then Julien Duvivier, before Potter was given the task. Originally cast as Daisy Heath was Jean Harlow, who died before filming began. She initially was replaced by Joan Crawford, who then yielded the role to Rosalind Russell, before newly signed MGM contract player Sullavan finally came on board. Melvyn Douglas originally was signed to play Sam Bailey, but the role ultimately went to Walter Pidgeon. 
 Hays Code, chorus girl into a leading lady and Sam from her gangster lover into her wealthy, high society boyfriend.

Although not deemed an official remake of The Shopworn Angel, the Paramount Pictures film That Kind of Woman (1959) shared a very similar plot.

==Soundtrack== Pack Up dubbed by Mary Martin.

==Critical reception==
Time (magazine)|Time described the film as "a tearjerker in the grand manner — simple, senile and heroically sentimental." 

==Box Office==
According to MGM records the film earned $722,000 in the US and Canada and $320,000 elsewhere resulting in a profit of $146,000. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 