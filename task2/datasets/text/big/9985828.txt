Fury in the Pacific
{{Infobox film
| name           = Fury in the Pacific
| image          =
| image_size     =
| caption        =
| director       =
| producer       =
| writer         =
| narrator       =
| starring       = See below
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 22 March 1945
| runtime        = 20 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 United States Marines, and directed by a series of combat cameraman — of whom nine became casualties of the battles they were filming.  The film is especially noteworthy for its praise of the fighting abilities of Japanese soldiers (a rarity for American propaganda during World War II), and its fast-paced editing.

The film is sometimes erroneously credited to Frank Capra, but he did not, in fact, direct the film. 

 
 

==Plot summary==
 

==Cast== Richard Carlson as Narrator

==Soundtrack==
 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 