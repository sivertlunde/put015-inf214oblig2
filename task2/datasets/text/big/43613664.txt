Motherhood (1917 film)
{{infobox film
| title          = Motherhood
| image          =
| imagesize      =
| caption        =
| director       = Frank Powell
| producer       = Frank Powell
| writer         = Frederic Arnold Kummer(story) Clara Beranger
| starring       = Marjorie Rambeau
| music          =
| cinematography =
| editing        =
| distributor    = Mutual Film
| released       = March 26, 1917
| runtime        = 50 minutes
| country        = USA
| language       = Silent/Titles
}}
Motherhood is a lost  1917 silent film drama directed by Frank Powell and starring Marjorie Rambeau. 

==Cast==
*Marjorie Rambeau - Louise
*Frank Ford - The Father
*Robert Elliott - Albert
*Paul Everton - Enemy Captain
*Aubrey Beattie - The Corporal
*Agnes Ayres - The Mother (as Agnes Eyre)
*Ruth Byron - Captains Wife
*Lillian Page - Captains Mother
*Anne Sutherland - Charlotte
*Frank Frayne - Her Son
*Robert Eaton - Her Son
*Lorna Volare - The Loved One

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 