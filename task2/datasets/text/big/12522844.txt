The Life of Reilly
 
:See also The Life of Riley (disambiguation).
{{Infobox Film
| name           = The Life of Reilly image           = The Life of Reilly theatrical poster.jpg caption         = The Life of Reilly Movie Poster
| director       = Barry Poltermann Frank L. Anderson
| producer       = Bob Fagan and Wrye Martin
| writer         = Charles Nelson Reilly Paul Linke
| starring       = Charles Nelson Reilly
| music          = Frank L. Anderson (addl music by Donita Sparks)
| cinematography = Anthony Balderrama
| editing        = Barry Poltermann
| distributor    = 
| released       = 2007, Theatrical
| runtime        = 84 minutes
| country        = United States English
| budget         = 
| gross          = 
}} 
The Life of Reilly is a 2006 American film adaptation of actor  , and directed by Frank L. Anderson and Barry Poltermann, the film is an edited version of Reillys much longer stage show, filmed live before audiences at the El Portal Theater in North Hollywood, California in October 2004.  The final film is compiled from Reillys final two performances, interspersed with clips, images and music.  

The Life of Reilly premiered at the South by Southwest Film Festival in March 2006 to positive reviews and proceeded to play for over a year on the festival circuit, including the Seattle International Film Festival and Newfest.  A limited theatrical release began in November 2007.

==Original stage performance== Tony Award-winning Broadway actor, had been giving to college theater students.  The stage play was written by Reilly and Paul Linke, and directed by Linke.  

Reilly took the play around the country, including the New Conservatory Theatre Center, San Francisco, California and the Irish Repertory Theatre, New York City, both in 2001.

 ."  The running time of the performance often ranged over three hours, a point cited in some critics reviews.  For example, Variety (magazine)|Variety said, "If theres something exasperating about the shows expansiveness, theres something endearing, too, about Reillys insistence on his two full acts of stage time. His celebrity has been peculiar and peripheral -- hes far better known to most for his high-camp guffaw on "Match Game" than for directing Julie Harris in "The Belle of Amherst" or appearing on Broadway in "Bye Bye Birdie (musical)|Bye, Bye Birdie" and "How to Succeed in Business."  So why not let him run a little amok? At 70, hes earned it. And with his sharp wit and scruffily friendly, eccentric stage presence, hes wonderful company."

For his work in "Save it For the Stage: The Life of Reilly" Reilly was nominated for a Drama Desk Award in 2001 and an Outer Critics Circle Award in 2002, both for Outstanding Solo Performance.

==Plot==
The film begins with Reilly recounting his childhood and his parents in New York City and Connecticut. We meet his family—an institutionalized father, a racist, baseball bat-wielding mother, and a lobotomized aunt, amongst others. "Eugene O’Neill would never get near this family," Reilly declares.
   
Prior to being put into an institution, Reilly’s father, a Paramount Pictures poster artist and illustrator, was offered the chance to go into business with another illustrator with the intention of making their first animated film in color together. The catch was simply that the senior Mr. Reilly would have to move himself and his family to California. Perhaps a defining moment in young Reilly’s life, his father asked his mother her thoughts and she unceremoniously rejected the possibility. The other illustrator went west without the senior Mr. Reilly as a partner. That other illustrator was named Walt Disney.
   
After this missed opportunity, Reillys father began drinking heavily and eventually had a nervous breakdown. Upon being institutionalized, the family was forced to move out of the Bronx up to Connecticut to live with Mrs. Reilly’s family.
   
When Reilly turned eighteen, he moved to New York City. "If you wanted to be an actor in those days," he explains, "You did something that’s really unheard of today… you studied."  Young and hungry (literally), Reilly managed to find an acting class at New School. Its rather liberal open door policy allowed aspiring actors admission even if they didn’t have the money to pay.
   
Reilly’s class was taught by a young, award-winning, soon-to-be-legendary actress, Uta Hagen. In the class were such future stars and notables as Steve McQueen, Jerry Stiller, Anne Meara, Hal Holbrook, Jason Robards, Geraldine Page and Gene Hackman. It was a group of future Academy Award, Tony Award and Emmy Award winners, and, as Charles explains, "We wanted to go on the stage, none of us had any money, and this entire list… couldn’t act for shit."
    Bye Bye Birdie and Hello, Dolly! (musical)|Hello, Dolly!
 The Ghost memorable guest spot on The X-Files. Over the years, he developed a habit of looking through the week’s TV Guide to see how many times he would be on TV that week. Coming in at over a hundred separate appearances, he reflected on the NBC executive who told him he would not be allowed on television—but now Reilly wondered, "Who do I have to fuck to get off?!"
   
Success came to Reilly’s professional life, and he has used all the knowledge and expertise he has gained through a lifetime spent acting to teach. His longtime friend Burt Reynolds gave him a theater in which to teach the craft of acting, and it has fulfilled Reilly ever since. As we leave him in what he calls "The twilight of an extraordinary life", we see a portrait of an artist, a victim of prejudice who rose above it, a trailblazing comedic personality, an entertainer, a son, a teacher, and a man laid bare for all to see.

==Production==
The Life of Reilly was filmed on HD video live in front of an audience.  Reilly was ill at the time with chronic asthma and was forced to cancel one of the three performances.  The bulk of the final film was shot on the final nights performance, which would turn out to be the last time Reilly performed in front of an audience.  Additional material was filmed during rehearsals, and the final film is an edited version of his full three-hour-plus live performance, interspersed with short film clips, animation and re-enactments.

==Theatrical release==
The Life of Reilly received a regional theatrical release primarily in Landmark Theaters in the United States and Canada in late 2007 and throughout 2008 to very positive reviews but limited box office and did not receive a DVD release.  

==Home media release==

In early 2009 the filmmakers announced that the film had been slated for release by New Yorker Films but was indefinitely delayed due to the bankruptcy of the distributor.  In October 2010 the filmmakers made a 2-disc DVD and 2-disc blu ray available on  .

The sets include the theatrical release, a 3-hour length staged version of the play, a "making of" featurette, and a feature-length commentary with Burt Reynolds, Anne Meara, Jerry Stiller, and Dom Deluise.

==Critical reception==
After its release, The Life of Reilly garnered a "100% Fresh" rating from critics at Rotten Tomatoes   and was listed as the best reviewed film of 2007.   

John Simon (critic) reviewed The Life of Reilly saying that "(Reillys) life provided the blueprint, but Reillys literary artistry had to construct it into a fabulous narrative full of sharply observed detail, irresistible humor, unmilked melancholy and humanely observed humanity. Much of it is worthy of Balzac, Dickens or Mark Twain."    

The Village Voice wrote that "Rambling, blithe, nostalgic, and out for revenge, Reilly presents a witty anecdotal timeline of his life, and the bittersweet milestones play like a Spalding Gray monologue loosened up with a few shots of tequila." 

The film appeared on several critics and reader top ten lists of the best films of 2007 including: The Village Voice/LA Weekly Film Poll of Best Films of 2007, which featured "The Life of Reilly" in its top ten "Best Documentary" list;     Nerve.coms 2007 Readers Poll, which listed "The Life of Reilly" as the 3rd Funniest Film of 2007;      and Ed Gonzalez of Slant Magazine, who picked "The Life of Reilly" as the years 10th best film overall.   

==References==
 

==External links==
*  
*  
*  
* 

 
 
 
 
 
 
 
 