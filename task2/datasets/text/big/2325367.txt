Society (film)
 
{{Infobox film
| name = Society
| image = SocietyPoster.jpg
| caption = Film poster for Society.
| director = Brian Yuzna
| writer = {{plainlist|
* Woody Keith
* Rick Fry
}}
| starring = {{plainlist|
* Billy Warlock
* Devin DeVasquez
* Evan Richards
* Ben Meyerson
}}
| producer = Keith Walley
| distributor = Wild Street Pictures
| released =  
| music = {{plainlist|
* Mark Ryder
* Phil Davies
}}
| cinematography = Rick Fichter
| editing = Peter Teshner
| runtime = 99 minutes
| language = English
| country  = United States
| budget =
}}
Society is a 1989 American horror film directed by Brian Yuzna. Though completed in 1989, but not released in the US until 1992. It was Yuznas directorial debut and was written by Rick Fry and Woody Keith. The film stars Billy Warlock as Bill Whitney, Devin DeVasquez as Clarissa Carlyn, Evan Richards as Milo and Ben Meyerson as Ferguson. Screaming Mad George was responsible for the special effects.

Society is considered to be a minor entry in the body horror subgenre.  A sequel, Society 2: Body Modification, was in development, with a script written by Stephan Biro.

==Plot==
Bill Whitney seems to have it all.  His family is wealthy and he lives in a mansion in Beverly Hills, California.  Hes popular at his high school, looks to be a shoo-in for class president, has a cute cheerleader girlfriend and owns a new Jeep Wrangler to drive around in.  Despite this, he feels as though he does not fit in with his family or their high-society friends.  When his sisters ex-boyfriend Blanchard gives him a surreptitiously recorded tape of what sounds like his family engaged in a vile, murderous orgy, Bill begins to suspect that his feelings are justified.
 coming out party.  Bill insists that what hed heard before was real and calls Blanchard to get another copy.  When he arrives at their meeting place, Bill discovers an ambulance and police officers gathered around Blanchards crashed van.  A body is placed into the back of the ambulance, but Bill is prevented from seeing its face.

Bill attends a party hosted by his upper-class classmate Ferguson.  There, Ferguson lasciviously confirms that the first audio tape Bill listened to—with the sounds of an orgy on it—was the real tape.  Angry and confused, he leaves the party with Clarissa, a beautiful girl hed been admiring.  They have sex at her house and Bill meets Clarissas bizarre, hair-loving mother.

Bill returns home the next day and confronts his parents and sister, who are all in the master bedroom dressed in lingerie. At Blanchards funeral, Bill and his friend Milo discover that Blanchards corpse either needed a lot of reconstructive work for display, or is not real.  Bill is approached by Martin Petrie, his rival for the high school presidency, who says he must speak with Bill and agrees to a secret meeting. Bill discovers Petrie with his throat cut. He sees someone race off through the woods then runs off to get the police. When he returns with the cops, the car and the body are missing, with a different car in their place.

With Dr. Clevelands help, they drug Bill.  As Milo secretly trails him, Bill is taken to a hospital.  Bill awakens in a hospital bed and thinks he hears Blanchard crying out, but discovers that nothing is there.  He leaves the hospital and finds his Jeep waiting for him.  Milo tries to warn him, but he drives back to his house.

Back home again, Bill finds a large, formal party.  He is snared by the neck and Dr. Cleveland reveals all of the secrets he has been searching for.  He is not really related to his family after all.  In fact, his family and their high-society friends are actually a different species from Bill.  To demonstrate, they bring in a still-living Blanchard.  The wealthy party guests strip to their underwear and begin "shunting".  The rich literally feed on the poor, physically deforming and melding with each other as they suck the nutrients out of Blanchards body.  Their intention is to do to the same to Bill.  In a fight with Ferguson, Bill manages to pull the pliable Ferguson inside-out.  With Milo and Clarissas help—who is also of this alternate species, but has fallen in love with Bill—he escapes.

==Cast==
* Billy Warlock as Bill Whitney
* Connie Danese as Nan Whitney
* Ben Slack as Dr. Cleveland
* Evan Richards as Milo
* Patrice Jennings as Jenny Whitney
* Tim Bartell as Blanchard 
* Charles Lucia as Jim Whitney
* Heidi Kozak as Shauna
* Brian Bremer as Martin Petrie
* Ben Meyerson as Ferguson
* Devin DeVasquez as Clarissa Carlyn
* Maria Claire as Sally
* Conan Yuzna as Jason
* Jason Williams as Jasons Friend
* Pamela Matheson as Mrs. Carlyn

==Reception==
For its British release, Society was marketed in Video Trade Weekly with a picture of the films theatrical premiere.  Mark Kermode called this "stupid yet brilliant", as it demonstrated that the distributor did not know how to market the film properly but also showed recognition that traditional marketing for a genre film was irrelevant.   Society was a success in Europe, but was shelved for three years before getting a release in the U.S.   Said director Yuzna in an interview: "I think Europeans are more willing to accept the ideas that are in a movie.  Thats why for example Society did really well in Europe and in the US did nothing, where it was a big joke. And I think its because they responded to the ideas in there. I was totally having fun with them, but they are there nonetheless." 

In 1990, Society won the Silver Raven award for "Best Make-Up" at the Brussels International Festival of Fantasy Film.   Tom Tunney of Empire (film magazine)|Empire rated it 4/5 stars and wrote, "Way ahead of its time, this is a balls-out satire on the disgraceful layers that can lurk just beneath the Avon surface. This is anti-Ferris Bueller and fiendishly funny."   Variety (magazine)|Variety described it as "an extremely pretentious, obnoxious horror film that unsuccessfully attempts to introduce kinky sexual elements into extravagant makeup effects".   Michael Wilmington of the Los Angeles Times wrote, "No one who sees the last half-hour of this movie will ever forget it—though quite a few may want to."   Marc Savlov of The Austin Chronicle wrote that the British press, who gave the film positive reviews, overrated it and stated that it would not play well to American audiences. 

In the early 2010s, Time Out London conducted a poll with several authors, directors, actors, and critics who have worked within the horror genre to vote for their top horror films.  Society placed at number 78 on their top 100 list.   Bartlomiej Paszylk wrote in The Pleasure and Pain of Cult Horror Films that the film has "one of the craziest and most disgusting endings in movie history". 

==Adaption==
Scottish comic book company Rough Cut Comics acquired the comic book rights to Society in 2002, producing an official sequel.

The comic book series returned in 2003 with Society: Party Animal by writer Colin Barr and artists Shelby Robertson (issue 1) and Neill Cameron (issue 2).  

==See also==
* Social class

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 