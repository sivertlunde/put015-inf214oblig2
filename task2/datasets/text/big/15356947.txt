The Ugly Duckling (1939 film)
 
 
{{Infobox Hollywood cartoon
| cartoon name = The Ugly Duckling
| series = Silly Symphonies
| image = The Ugly Duckling (1939 film) poster.jpg
| image size =
| alt = Poster for The Ugly Duckling
| caption = Poster for The Ugly Duckling Jack Cutting
| producer = Walt Disney
| story artist =
| narrator =
| voice_actor = Clarence Nash    
| musician =
| animator = Stan Quackenbush
| layout artist =
| background artist =
| studio = Walt Disney Productions
| distributor = RKO Radio Pictures
| release_date = April 7, 1939
| color_process = Technicolor
| runtime = 8 minutes 59 seconds
| country = United States
| language = English
| preceded by =
| followed by =
}}
 Jack Cutting, and released in theaters on April 7, 1939. Music was composed by Albert Hay Malotte, who was uncredited for the film. The animated short was first distributed by RKO Radio Pictures. " ". www.bcdb.com 
 Academy Award for Best Short Subject (Cartoons),    and also happened to be the last entry in the Silly Symphony series.

In the Andersen tale, a duckling is harassed because of his homeliness. To his delight, he matures into a swan, the most beautiful bird of all, and his troubles are over. In this version, the baby swans sufferings are shortened, as he is found by his family, after only a few minutes of rejection and ostracism, instead of a whole year. This abbreviated version is read by Lilo to Stitch in the 2002 Disney film Lilo & Stitch. The story has a deep impact on Stitch, who sets out to look for his real family.

==See also==
* The Ugly Duckling (1931 film)|The Ugly Duckling (1931 film)
* List of Disney animated shorts

==References==
 

==External links==
*   at  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 

 

 