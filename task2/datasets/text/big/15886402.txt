The Final Destination
 
 
{{Infobox film
| name           = The Final Destination
| image          = Final destination 09.jpg
| caption        = Theatrical release poster
| director       = David R. Ellis
| producer       = Craig Perry Warren Zide
| writer         = Eric Bress
| based on       =  
| starring       = Bobby Campo Shantel VanSanten Mykelti Williamson  Brian Tyler
| cinematography = Glen MacPherson Mark Stevens
| studio         = Zide/Perry Productions
| distributor    = New Line Cinema  
| released       =  
| runtime        = 82 minutes  
| country        = United States
| language       = English
| budget         = $40 million {{cite news 
|url=http://latimesblogs.latimes.com/entertainmentnewsbuzz/2009/08/movie-projector-the-final-destination-halloween-ii-splitting-horror-audience.html | title=Movie projector: The Final Destination, Halloween II splitting horror audience | work=Los Angeles Times | date=August 27, 2009 | accessdate= January 14, 2010}} 
| gross          = $186,167,139   
}} horror film HD 3D film|3D. It is currently the highest grossing Final Destination film, earning $186 million worldwide but also received the worst critical reception of the franchise. It was followed by Final Destination 5 in 2011.

This was one of the last films to be theatrically released by New Line Cinema until it was merged with its sister studio Warner Bros.

==Plot== Nick OBannon, Lori Milligan, Janet Cunningham Hunt Wynorski. George Lanter, Andy Kewzer, Samantha Lane, Carter Daniels, Nadia Monroy, leave the stadium seconds before Nicks vision becomes a reality. Nadia gets angry and yells at them when she is suddenly obliterated by a stray tire that flies out of the stadium.
 previous incidents, Death is cylinder and extruded through a grid fence in the shape of rhombic pieces of flesh.
 Jonathan Groves, another survivor they dont recall at first, who is crushed when an overflowing bathtub falls through the ceiling. George is struck by a speeding ambulance as they leave and dies.

At the movies, Lori begins to see omens as well and suspects they are still in danger. While Nick rushes to save them, a fire erupts behind the movie screen. Nick manages to convince Lori to leave, but Janet refuses and is killed by flying debris when the fire sets off an explosion. As Nick and Lori attempt to escape Lori is pulled into a malfunctioning escalators gears, and Nick realizes that it was just another vision. Unable to save George, Nick rushes to the theater and barely manages to stop the fire before the explosion occurs.

Weeks later, Nick notices a loose scaffold prop outside of a café and warns a worker about it before meeting Lori and Janet inside. He suddenly realizes that the mall disaster vision was merely a feint meant to lead them to where they needed to be for Death to strike. At that moment, the scaffold collapses, causing a truck to swerve and crash through the window, killing them. The final scene turns into an X-ray format; Janet is shown to have been crushed under the tires, while Loris neck is snapped, and Nick is thrown into a wall, bashing his skull and jaw.

==Cast==
* Bobby Campo as Nick OBannon
* Shantel VanSanten as Lori Milligan
* Nick Zano as Hunt Wynorski
* Haley Webb as Janet Cunningham
* Mykelti Williamson as George Lanter
* Krista Allen as Samantha Lane
* Andrew Fiscella as Andy Kewzer
* Justin Welborn as Carter Daniels
* Stephanie Honore as Nadia Monroy
* Lara Grice as Cynthia Daniels
* Jackson Walker as Jonathan Groves

==Production==

===Development=== James Wong was on board to direct, but because of scheduling conflicts with Dragonball Evolution, he decided to drop out. Consequently, the studio executives opted for David R. Ellis to return because of his work on Final Destination 2. He accepted because of the 3D.    For the 3D, Perry said that he wanted it to add depth to the film instead of just "something pop  out at the audience every four minutes."   

===Filming===
 

Although shooting was to be done in Vancouver, which was where the previous three films were shot, David R. Ellis convinced the producers to shoot in New Orleans instead to bring business to the city, and because the budget was already large.  The opening crash sequence at "McKinley Speedway" was filmed at Mobile International Speedway in Irvington, Alabama. Filming began in March 2008 and ended in late May in the same year.  Reshoots were done in April 2009 at Universal Studios Florida. 

==Music==

===Soundtrack=== Brian Tyler. He took over scoring the series after the untimely death of the composer for the first three films, Shirley Walker.

;Commercial songs from film, but not on soundtrack 

* "Devour (song)|Devour" by Shinedown
* "How the Day Sounds" by Greg Laswell Anvil
* Why Cant War
* "Dont You Know" by Ali Dee and the Deekompressors
* "Faraway" by Dara Schindler
* "Dream of Me" by Perfect
* "Make My" by The Roots
* "The Stoop" by Little Jackie
* "Sweet Music" by Garrison Hawk
* "Corona and Lime" by Shwayze
* "Make You Crazy" by Brett Dennen

===Score=== Brian Tyler, omitting commercially released songs that were featured in the film.

;U.S. edition 
{{Infobox album
| Name = The Final Destination (Original Motion Picture Soundtrack)
| Type = Film score Brian Tyler
| Cover =  
| Released = August 25, 2009
| Length =
| Label = JVC, Sony Music Australia
}}
# "The Final Destination" – 2:56
# "The Raceway" – 3:07
# "Memorial" – 2:46
# "Nailed" – 3:22
# "Nicks Google Theory" – 1:30
# "Revelations" – 2:28
# "Raceway Trespass" – 1:39
# "Stay Away from Water" – 2:38
# "Flame On" – 1:43
# "Moment of Joy" – 1:17
# "Signs and Signals" – 2:51
# "George Is Next" – 1:12
# "Car Washicide" – 3:05
# "Newspaper Clues" – 1:57
# "Premonition" – 1:50
# "The Salon" – 3:53
# "Questioning" – 1:04
# "Death of a Cowboy" – 2:08
# "Gearhead" – 1:56
# "Sushi for Everyone" – 2:53
# "The Movie Theater" – 3:03
# "You Cant Dodge Fate" – 1:28
# "The Final Destination Suite" – 13:29

The soundtrack attracted generally favorable reviews. Christian Clemmensen of Filmtracks.com gave the score 3 out of 5 stars and felt Tyler was "capable   to further explore new stylistic territory while making substantial use of the structures and tone of   Shirley Walkers music." His approach to the scores were called "intelligent", and provide "adequate if not strikingly overachieving recordings is testimony to his immense talents."

The reviewers were also impressed with the extension of the sound used by Walker in Final Destination 3. "It relates to an affection for Walkers contribution to the industry," said an unnamed critic. 

A SoundNotes reviewer grades the film with an impressive score of 7.5/10, remarking "Brian Tyler slugs his way through the inadequacies of The Final Destination and produces a score with reasonable entertainment value and enough of an appeal to make it function well apart from the woeful film." 

==Release== select theaters. 

===Box office=== Halloween II, earning $28.3 million during its first weekend.   It is also topped the box office in the United Kingdom|UK.  The film remained #1 at the box office in North America for two weeks. On September 11, 2009, it gained just over a million dollars and dropped to No. 7.  The film grossed $66.4 million domestically and $119.3 million in foreign sales, with a total of $186.5 million worldwide. 

===Home media=== A Nightmare on Elm Street (2010).  The Blu-ray Disc release, also a combo pack, includes a standard DVD of the film.
 Target stores, some of the DVDs included an exclusive Final Destination comic book.

The movie was released uncut in Australian theaters with an Australian Classification Board#Film and video game classifications|MA15+ (Strong horror violence, sex scene) rating. When the movies DVD/Blu-ray Disc release was reviewed, the ACB (Australian Classification Board) noted several scenes in the 2D version that exceeded the guidelines of the MA15+ category. There were two editions released in Australia: a DVD version which only contains a censored 2D version (most of the blood effects taken off and gore trimmed) and a DVD release awarded an R18+ rating (High impact violence) with both uncensored 2D and 3D versions (and 3D glasses included). The covers between the two releases vary.

==Reception== normalized rating out of 0–100 reviews from film critics, has a rating score of 30 based on 14 reviews. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 