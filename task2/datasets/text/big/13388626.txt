Three Friends (film)
 
{{Infobox film
| name           = Three Friends
| image          = Biograph poster2.jpg
| caption        = Poster for the film.
| director       = D. W. Griffith
| producer       = Biograph Company
| writer         = M. S. Reardon
| narrator       =
| starring       = Blanche Sweet Henry B. Walthall
| cinematography = G. W. Bitzer
| editing        =
| distributor    = General Film Company
| released       =  
| runtime        = 17 minutes (16 Frame rate|frame/s)
| country        = United States 
| language       = Silent with English intertitles
| budget         =
}}

Three Friends is a 1913 American silent film directed by  D. W. Griffith and starring Blanche Sweet.   

==Cast==
* Henry B. Walthall as The Husband
* Blanche Sweet as The Wife John T. Dillon as First Friend
* Lionel Barrymore as Second Friend Joseph McDermott as Third Friend
* Clara T. Bracy as In First Factory
* Kathleen Butler as In First Factory Harry Carey as In Saloon and In First Factory
* Frank Evans as In First Factory
* J. Jiquel Lanoe as In First Factory
* Walter P. Lewis as Foreman
* Wilfred Lucas as In Second Factory
* Mae Marsh as The Wifes Friend
* W. C. Robinson as In Saloon
* J. Waltham as In Second Factory

==See also==
* Harry Carey filmography
* D. W. Griffith filmography
* Blanche Sweet filmography
* Lionel Barrymore filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 