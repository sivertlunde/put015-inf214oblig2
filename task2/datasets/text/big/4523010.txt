Strange Invaders
 

{{Infobox film
| name           = Strange Invaders
| image          = Strange invaders.jpg
| caption        = Promotional movie poster for the film
| director       = Michael Laughlin
| producer       = Walter Coblenz
| writer         = Bill Condon Michael Laughlin Walter Halsey Davis
| starring = {{Plainlist|
* Paul Le Mat Nancy Allen
* Diana Scarwid Michael Lerner
* Louise Fletcher
}}
| music          = John Addison
| cinematography = Louis Horvath
| editing        = John W. Wheeler
| studio         = EMI Films MGM (2001, DVD)
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $5.5 million
| gross          = $1,362,303
}}
 Nancy Allen and Diana Scarwid. The film was intended to be the second installment of the aborted Strange Trilogy with Strange Behavior, another 1950s spoof by Laughlin, but the idea was abandoned after Strange Invaders failed to attract a wider audience. Scarwids performance earned her a Razzie Award nomination for Worst Supporting Actress.

== Plot ==
In 1958, the town of Centerville, Illinois was invaded by a race of aliens. The invaders could fire lasers from their eyes and hands and reduce humans to "crystallized" glowing blue orbs. They took over the form of the humans who were either captured or killed.

Twenty five years later, university lecturer Charles Bigelow (Paul Le Mat) learns that his ex-wife, Margaret Newman (Diana Scarwid), has disappeared while attending her mothers funeral in Centerville, and travels there to find her. The disguised aliens all appear human and the town of Centerville appears to have not progressed beyond 1958. The aliens try to capture Bigelow as he escapes, but only capture his dog, Louie.

Seeing a photo of an alien in a tabloid magazine, Bigelow soon finds Margaret, who is now revealed to be one of the aliens. She warns Bigelow to escape with Elizabeth (Lulu Sylbert), their human/alien hybrid daughter, to protect her from the aliens, who want to take her to their home-world. Bigelow and his daughter Elizabeth escape from the departing alien ship and the town-folk blue orbs are transformed back to their original human forms.

== Cast ==
* Paul Le Mat as Prof. Charles Bigelow Nancy Allen as Betty Walker
* Diana Scarwid as Margaret Newman Michael Lerner as Willie Collins
* Louise Fletcher as Mrs. Benjamin
* Wallace Shawn as Earl
* Fiona Lewis as Waitress / Avon Lady
* Kenneth Tobey as Arthur Newman
* June Lockhart as Mrs. Bigelow Charles Lane as Dr. Prof. Hollister
* Lulu Sylbert as Elizabeth Bigelow Joel Cohen as Tim
* Dan Shor as Teen Boy in Prologue
* Dey Young as Teen Girl in Prologue
* Jack Kehler as Gas Station Attendant
* Mark Goddard as Detective
* Thomas Kopache as State Trooper
* Bobby Pickett as Editor
* Connie Kellers as Connie
* Nancy Johnson as Stewardess #1
* Betsy Pickering as Stewardess #2
* Jonathan Ulmer as Room Service Waiter
* Ron Gillham as First Alien
* Al Roberts as Man in Dark Glasses
* Edwina Follows as Nurse
* Patti Medwid as Room Service Waitress

==Production==
Director Michael Laughlin re-teamed with Bill Condon, his co-writer and associate producer from Strange Behavior. The first image Laughlin came up with was that of a midwest landscape with an "old-fashioned mothership sliding in".    He wrote the first few pages himself and then he and Condon completed the screenplay in two parts, each writing different sections. They wrote the script without any deal in place but were confident that it was going to be made into a film. They even figured out the budget, scouted locations, cast the actors, and worked on the production design while arranging the financing. This pre-production was all done at the expense of Condon and Laughlin. To help produce the film, Laughlin brought in his friend Walter Coblenz, who had been the assistant director on the Laughlin-produced film Two-Lane Blacktop. They shopped the script for Strange Invaders around Hollywood. 

Laughlins previous film, Strange Behavior, had been released by a small distributor and this time around he wanted his film to be handled by a major.  Orion Pictures liked the script and was looking for a good film at a modest price with mainstream appeal. Orion provided half of the films $5.5 million budget with Englands EMI Films coming up with the rest. Orion received distribution rights for North America while EMI handled the rest of the world. As part of the financing deal, Orion and EMI demanded several script changes, which Condon and Laughlin found difficult, because they had to try to explain their ideas verbally.  The financial backers influence reduced the films scope. For example, in the original script, the American government was a much bigger threat, with a big sequence taking place at an Air Force base. These changes bothered Laughlin, because they resulted in a lack of a well-defined middle section in the script. Swires 1983, p. 61. 
 Michael Murphy in mind&nbsp;— he had been in Strange Behavior&nbsp;— but EMI refused to allow him to be cast much to the directors confusion "because there didnt seem to be a good reason for his rejection. I guess it was a matter of personal taste".  Orion and EMI suggested Mel Gibson and Powers Boothe instead but Laughlins choice was Paul Le Mat, because he had not played that kind of role before and had a "Joel McCrea quality" that he was seeking.  For the role of Betty, Laughlin wanted an actress from New York and not someone from California playing a New Yorker. Condon was a big fan of Brian De Palmas films and Nancy Allen who appeared in several of them. Louise Fletchers government agent was originally written as a man, a "Bob Balaban bureaucrat", but during the screenwriting process, Condon and Laughlin decided to change the character to a woman and cast Fletcher who had been in Strange Behavior. 
 The Thing, and later, the writer and director of the cult horror classic The Boneyard, who had his name removed from the credits after heated debates with Laughlin about the way the effects were being used and shot. Laughlin relented and allowed Cummins to reshoot a lengthy scene near the end of the film where the aliens shed their human guises as they prepare to embark on a 1950s style spacecraft. Laughlin planned a third film in a proposed "Strange Trilogy", titled, The Adventures of Philip Strange, a World War II spy thriller with science fiction elements and hoped to cast many of the same actors and crew from his two previous films. Swires 1983, p. 63. 

==Reaction==
In his review for   magazines David Ansen wrote, "Hovering unclassifiably between nostalgia and satire, this amiably hip genre movie confirms Laughlin as a deliberately minor but unique stylist. Its up to the viewer to determine just how faux his naif style is, but either way you choose to take it, Strange Invaders offers a good deal of laid-back fun".    Jay Scott in his review for the Globe and Mail wrote, "Strange Invaders is a pastiche, a film-school jumble of aphorisms and winks at the audience that are neither as knowing nor as amusing as they are meant to be".   

==References==
 

== External links ==
*  
*  
*  
*  
*  
*   at  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 