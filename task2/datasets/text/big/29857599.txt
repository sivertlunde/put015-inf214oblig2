Gunahon Ka Devta (1990 film)
Not to be confused with Gunahon Ka Devta (TV series). 
Not to be confused with Gunahon Ka Devta (novel)
{{Infobox film 
 | name = Gunahon Ka Devta
 | image = 1990-Gunahon Ka Devta.jpg
 | caption = DVD Cover
 | director = Kawal Sharma
 | producer = Arun S. Thakur
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Sangeeta Bijlani Aditya Pancholi Danny Denzongpa Shakti Kapoor Paresh Rawal
 | music = Anu Malik
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released =  July 6, 1990
 | runtime = 135 min.
 | language = Hindi Rs 6 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1990 Hindi Indian feature directed by Kawal Sharma, starring Mithun Chakraborty, Sangeeta Bijlani, Aditya Pancholi, Danny Denzongpa, Shakti Kapoor and Paresh Rawal

==Plot==

Gunahon Ka Devta is an action film starring Mithun Chakraborty and Sangeeta Bijlani in lead roles. Baldev Raj Sharma is an honest and diligent Police Inspector and was selected by the Police department to felicitate and honor him in public for his truthful service, but things take an ugly turn, when he is arrested and stripped off his title for a brutal killing, and he is imprisoned. His wife alone bring up their son, Suraj. Like his father, Suraj too becomes a Police inspector and his only aim is to find out the truth behind his fathers false implication. The Climax reveals the true killer.

==Cast==

* Mithun Chakraborty ... Inspector Baldev Raj Sharma and Advocate Suresh Sharma (Double Role)
* Sangeeta Bijlani ... Bhindes sister
* Aditya Pancholi ... Sunny Khanna
* Danny Denzongpa ... Raghuvir
* Shakti Kapoor ... Fake Police Inspector Bhinde
* Chandrashekhar
* Sujata Mehta ... Inspector Shilpa Verma
* Paresh Rawal ... Advocate Khanna
* Bob Christo ... Bob
* Brij Gopal
* Ranjeeta Kaur ... Mrs. Baldev Sharma
* Kulbhushan Kharbanda ... Goenka / Dhabar
* Viju Khote ... Cook
* Kim
* Lilliput
* Disco Shanti
* Shivraj
* Subbiraj ... Police Inspector Verma
* Sudhir

==References==
* http://www.imdb.com/title/tt0494953/
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Gunahon+Ka+Devta+%281990%29

==External links==
*  

 
 
 
 

 