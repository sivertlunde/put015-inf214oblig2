Up to a Certain Point
 

{{Infobox film
| name           = Up to a Certain Point (English) Hasta cierto punto (Spanish)
| writer         = Tomás Gutiérrez Alea Serafín Quiñones Juan Carlos Tabío
| starring       = Óscar Álvarez Mirta Ibarra Omar Valdés Coralia Veloz Rogelio Blain Ana Viña
| director       = Tomás Gutiérrez Alea
| music          = Leo Brouwer
| cinematography = Mario García Joya
| editing        = Miriam Talavera
| studio         = Instituto Cubano del Arte e Industrias Cinematográficos
| distributor    = ICAIC (Cuba) New Yorker Films (USA)
| released       = 1983 (Cuba) 13 March 1985 (USA)
| runtime        = 68 minutes
| country        = Cuba Spanish
| producer       = Humberto Hernández
}}

Up to a Certain Point (Spanish: Hasta cierto punto) is a 1983 Cuban movie directed by Tomás Gutiérrez Alea.  It focuses on a theater director who starts up a relationship with a female dockworker.  However, his machismo complicates matters.

The movie is a general look at gender roles in Cuba as well as the conflict between machismo and the idealized image of liberated women under communism.

==External links==
* 

 
 
 


 
 