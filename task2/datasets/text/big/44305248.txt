My Past
{{Infobox film
| name           = My Past
| image          =
| caption        =
| director       = Roy Del Ruth
| producer       = Warner Brothers
| writer         = Charles Kenyon
| starring       = Bebe Daniels
| music          = David Mendoza Erno Rapee
| cinematography = Barney McGill
| editing        = Ralph Dawson
| distributor    = Warner Brothers
| released       = May 14, 1931
| runtime        = 72-74 minutes
| country        = USA
| language       = English
}}
My Past is a 1931 Pre-Code sound film directed by Roy Del Ruth and starring Bebe Daniels. It was produced and distributed by Warner Brothers and was known under the alternative title: "The Ex-Mistress". 

A surviving talkie also preserved in the Library of Congress collection. 

==Cast==
*Bebe Daniels - Miss Doree Macy
*Lewis Stone - Mr. John Thornley
*Ben Lyon - Robert Byrne, aka Bob
*Joan Blondell - Marion Moore
*Natalie Moorhead - Consuelo Byrne, Connie
*Albert Gran - Lionel Reich
*Virginia Sale - Miss Taft, Thornleys Secretary

==References==
 

==External links==
* 
* 
* (Listal)

 

 
 
 
 
 


 