Twister (1996 film)
 
 
{{Infobox film
| name           = Twister
| image          = Twistermovieposter.jpg
| caption        = Theatrical release poster
| director       = Jan de Bont Kathleen Kennedy
| writer         = Michael Crichton Anne-Marie Martin Joss Whedon  
| starring       = Bill Paxton Helen Hunt Jami Gertz Cary Elwes 
| music          = Mark Mancina Eddie Van Halen
| cinematography = Jack N. Green Michael Kahn Universal Pictures Amblin Entertainment Universal Pictures  
| released       =  
| runtime        = 113 minutes
| country        = United States
| language       = English
| budget         = $92 million   
| gross          = $494.4 million 
}}
 disaster drama storm chasers researching tornadoes. It was directed by Jan de Bont from a screenplay by Michael Crichton and Anne-Marie Martin. Its executive producers were Steven Spielberg, Walter Parkes, Laurie MacDonald and Gerald R. Molen. Twister was the 1996 in film#Highest-grossing films|second-highest-grossing film of 1996 domestically, with an estimated 55 million tickets sold in the US.

It is notable for being both the first Hollywood feature film to be released on DVD format and one of the last to be released on HD DVD.  Twister has since been released on Blu-ray Disc.
 storm chasers VORTEX of the National Oceanic and Atmospheric Administration|NOAA.  The device used in the movie, called "Dorothy", is copied from the real-life TOtable Tornado Observatory|TOTO, used in the 1980s by National Severe Storms Laboratory|NSSL.

==Plot== F5 tornado strikes during the night. This tornado is so strong that the storm cellar door is ripped off, and Jos father (Richard Lineback) is pulled into the storm to his death. Jo, her mother (Rusty Schwimmer), and Toby survive.

Twenty-seven years later in the present day in 1996 in Oklahoma, 32-year-old Jo (Helen Hunt), now a meteorologist, is reunited with her estranged husband, Bill "The Extreme" Harding (Bill Paxton), a former weather researcher and storm chaser, who has since become a weather reporter. He is planning to marry sex therapist Melissa Reeves (Jami Gertz). They need Jos signature on the divorce papers and have tracked her down during an active bout of stormy weather. Jo has built four identical tornado research devices called DOROTHY, based on Bills designs. The device is designed to release hundreds of sensors into the center of a tornado to study its structure from the inside, with the purpose of creating a more advanced storm warning system. Bill and Melissa join Jo and her team of storm chasers, and the team encounters Dr. Jonas Miller (Cary Elwes), a smug, corporate-funded meteorologist and storm chaser. When Bill discovers that Jonas has created a device based on DOROTHY, called DOT-3, he vows to help Jo deploy DOROTHY before Miller can claim credit for the idea.

During the first tornado (possibly Fujita scale|F1), Jos truck and DOROTHY I are both destroyed. They continue storm chasing in Bills truck, with Melissa in the back seat. They find a second tornado, a confirmed F2, and head off on a back road when the twister shifts its track. They soon find themselves driving through heavy rain, and instead run smack right into a cluster of two tornadoes, spinning them around on the highway until the tornadoes dissipate. Theyre fine, but Melissa becomes hysterical from the ordeal, and Bill has to calm her down.

The team visits Jos aunt Megs house in Wakita, Oklahoma for food and rest. Whilst Jo is busy showering Bill tells Melissa about his relationship with Jo, his constant rivalry with Jonas and how scientists study Tornadoes and she asks "Is there an F5 Tornado?" and whether anyone has ever seen one. Bill answers by saying "just one of us". He is referring to the day Jos Dad died.

They soon learn that a "hopping" F3 tornado is on the ground, but they have trouble finding it. Jo drives ahead of the team to intercept the oncoming tornado, but a telephone pole falls on the back of Bills truck and knocks DOROTHY II out onto the road, disabling it. As the tornado lifts and touches down closer, Bill pulls Jo into the truck and moves to safety. The two confront each other over their marriage and Jos obsession with stopping tornadoes, due to being upset about her fathers death.
 The Shining, forcing everyone to take shelter in a pit in a car repair shop warehouse. By this time Melissa has been traumatized by the experiences and recognizes the unresolved feelings between Bill and Jo. In desperation for her own safety, she peacefully breaks off the engagement with Bill. The tornado continues on to Wakita, devastating the town and injuring Meg while destroying her house. Bill and Jo rescue Meg and her dog from the collapsing house. Megs injuries are not serious, but she is taken to the hospital for safety while her dog stays with the group. The team then hears that an even stronger storm, an F5, is forming 25 miles south of their position. Inspecting Megs wind chime sculptures, Jo realizes that the most likely method to successfully deploy DOROTHYs sensors into a tornado would be to add additional body surface to catch the wind.

As they reach the F5, the team adds aluminum from soda cans to work as wind flaps, but the deployment of DOROTHY III is a failure. Meanwhile, Jonas attempts to deploy DOT-3 and in the process he ignores Bills warnings to turn around. Moments later, a piece of metal comes flying through the windshield, Jonass truck is caught by the tornado, exploded and he and his driver Eddie (Zach Grenier) are killed. Jo and Bill set out on their own and are able to deploy the last DOROTHY successfully, using Bills truck as an anchor. From miles away, the research team sees immediate results on their computers from the sensors. Bill and Jos celebration is cut short, however, as the tornado shifts course toward them. They take shelter in a shed where they anchor themselves to irrigation pipes with leather straps. The tornado destroys the shed and Jo and Bill find themselves in the vortex of the super-massive funnel, securely floating in air.

After the F5 dissipates, Jo and Bill find themselves alone on the floor of the former shed. They  decide to run their own lab and rekindle their marriage.

==Cast==
* Bill Paxton as Bill "The Extreme" Harding, a weather reporter and former storm chaser and original creator of Dorothy
* Helen Hunt as Dr. Jo Harding (née Thornton), an obsessed meteorologist, storm chaser, Bills estranged wife and co-creator of Dorothy
* Jami Gertz as Dr. Melissa Reeves, Bills fiancée and reproductive therapist who knows nothing about Bills past
* Cary Elwes as Dr. Jonas Miller, a rival storm chaser who stole Bills idea for Dorothy and uses corporate sponsorship to achieve his own ends
* Philip Seymour Hoffman as Dustin Dusty Davis, a hyperactive storm chaser who has been with Bill and Jo since the beginning (Code name: Barn Burner)
* Alan Ruck as Robert "Rabbit" Nurick, the storm chasers navigator and map keeper
* Jeremy Davies as Brian Laurence, cameraman for the storm chasers
* Joey Slotnick as Joey, computer expert and wind tracker
* Sean Whalen as Alan Sanders, Rabbits driver and partner
* Todd Field as Tim "Beltzer" Lewis, a storm chaser who handles the computer tracking systems
* Wendle Josepher as Haynes, Beltzers partner/sidekick who keeps the electronics systems working Scott Thomson as Jason "Preacher" Rowe, eldest of the storm chasers, who throws in some religious anecdotes and comments
* Lois Smith as Aunt Meg Greene, Jos aunt and only living relative, who is a metal artist and lives in Wakita with a dog named Mose
* Zach Grenier as Eddie, Jonass driver
* Alexa Vega as Young Jo
* Richard Lineback as Mr. Thornton (Jos Father)
* Rusty Schwimmer as Mrs. Thornton (Jos Mother)
* Jake Busey as Mobile Lab Technician

==Production== Universal Pictures. UIP got the international distribution. The original concept and 10-page tornado-chaser story were presented to Amblin Entertainment in 1992 by screenwriter Jeffrey Hilton. Steven Spielberg then presented the concept to writer Michael Crichton. Crichton and his wife, Anne-Marie Martin, were paid a reported $2.5 million to write the screenplay.

After spending more than half a year on pre-production on Godzilla (1998 film)|Godzilla, director Jan De Bont left after a dispute over the budget and quickly signed on for Twister.   

The production was plagued with numerous problems.  

Halfway through filming both Bill Paxton and Helen Hunt were temporarily blinded by bright electronic lamps used to get the exposure down to make the sky behind the two actors look dark and stormy. Paxton remembers that "these things literally sunburned our eyeballs. I got back to my room, I couldnt see".  To solve the problem, a Plexiglas filter was placed in front of the beams. The actors took eye drops and wore special glasses for a few days to recuperate. After filming in a ditch that contained bacteria, Hunt and Paxton had to have hepatitis shots. During the same scene, she repeatedly hit her head on a low wooden bridge because she was so exhausted from the demanding shoot that she forgot not to stand up so quickly.  Hunt did one stunt in which she opened the door of a vehicle that was speeding through a cornfield, stood up on the passenger side and was hit by the door on the side of her head when she let it go momentarily. As a result, some sources claim that Hunt got a concussion. De Bont said, "I love Helen to death, but you know, she can be also a little bit clumsy." She responded, "Clumsy? The guy burned my retinas, but Im clumsy ... I thought I was a good sport. I dont know ultimately if Jan chalks me up as that or not, but one would hope so". 
 Don Burgess left the production after five weeks, claiming that De Bont "didnt know what he wanted till he saw it. He would shoot one direction, with all the equipment behind the view of the camera, and then hed want to shoot in the other direction right away and wed have to move   and hed get angry that we took too long ... and it was always everybody elses fault, never his".  De Bont claims that they had to make schedules for at least three different scenes every day because the weather changed so often that "Don had trouble adjusting to that".  When De Bont knocked over a camera assistant who had missed a cue, Burgess and his crew left, much to the shock of the cast. Burgess and his crew stayed on one more week until a replacement was found in Jack N. Green. Just before the end of the shoot, Green was injured when a hydraulic house set, designed to collapse on cue, was mistakenly activated with him inside it. A rigged ceiling hit him in the head and he injured his back, necessitating a visit to the hospital. Green missed the last two days of principal photography and De Bont took over as his own director of photography. 

De Bont had to shoot many of the films tornado-chasing scenes in bright sunlight when they could not get overcast skies and asked Industrial Light & Magic to more than double its original plan for 150 "digital sky-replacement" shots.  Principal photography had a time limit because Hunt had to return to film another season of Mad About You but Paul Reiser was willing to delay it for two-and-a-half weeks when the Twister shoot was extended. De Bont insisted on using multiple cameras, which led to the exposure of 1.3 million feet of raw film (most films use no more than 300,000 feet). 

De Bont claims that Twister cost close to $70 million with $2–3 million going to the director. It was speculated that last-minute re-shoots in March and April 1996 (to clarify a scene about Jo as a child) and overtime requirements in post-production and at ILM, raised the budget to $90 million.  Warner Bros. moved up the films release date from May 17 to May 10 in order to give it two weekends before   opened.

The film is known for its successful product placement by featuring the latest iteration of the Dodge Ram pickup truck and several other new vehicle models.

==Soundtrack==
Twister featured both a traditional orchestral film score by Mark Mancina and several rock music songs, including an instrumental theme song composed and performed for the film by Van Halen. Both the rock soundtrack and the orchestral score were released separately on compact disc.

===Twister: Music From The Motion Picture Soundtrack===
# Van Halen — "Humans Being"
# Rusted Root — "Virtual Reality"
# Tori Amos — "Talula"  (BTs Tornado Mix) 
# Alison Krauss — "Moments Like This"
# Mark Knopfler — "Darling Pretty"
# Soul Asylum — "Miss This" Belly — "Broken"
# k.d. lang — "Love Affair"
# Lisa Loeb & Nine Stories — "How"
# Red Hot Chili Peppers — "Melancholy Mechanics" Long Way Down"  (Remix) 
# Shania Twain — "No One Needs to Know"
# Stevie Nicks & Lindsey Buckingham — "Twisted" Edward & Alex Van Halen — "Respect the Wind"

There is also some other music, such as Deep Purples "Child In Time" (heard when the team takes the road at the beginning and the assistant maxes the volume in his truck).
The song queued up on a tv in Dustys van is Eric Claptons - Motherless Child

===Twister: Motion Picture Score===
# Oklahoma: Wheatfield
# Oklahoma: Wheres My Truck?
# Oklahoma: Futility
# Oklahoma: Downdraft
# Its Coming: Drive In
# Its Coming: The Big Suck
# The Hunt: Going Green (feat. Trevor Rabin on guitar)
# The Hunt: Sculptures
# The Hunt: Cow
# The Hunt: Ditch
# The Damage: Wakita
# Hailstorm Hill: Bobs Road
# Hailstorm Hill: Were Almost There
# F5: Dorothy IV
# F5: Mobile Home
# F5: Gods Finger
# Other: William Tell Overture/Oklahoma Medley
# Other: End Title/Respect the Wind - written by Edward and Alex Van Halen

There are some orchestrated tracks that were in the movie but were not released on the orchestral score, most notably the orchestrated intro to "Humans Being" from when Jos team left Wakita to chase the Hailstorm Hill tornado. Other, lesser-known tracks omitted include an extended version of "Going Green" (when we first meet Jonas) and a short track from when the first tornado is initially spotted.

==Reception==
===Critical response===
 

The film holds a 57% "rotten" score at Rotten Tomatoes based on 53 reviews.  It holds a score of 68 at Metacritic, indicating "generally favorable reviews". 

Roger Ebert gave the film two-and-a-half stars out of four and wrote, "You want loud, dumb, skillful, escapist entertainment? Twister works. You want to think? Think twice about seeing it".    In her review for The New York Times, Janet Maslin wrote, "Somehow Twister stays as uptempo and exuberant as a roller-coaster ride, neatly avoiding the idea of real danger".    Entertainment Weekly gave the film a "B" rating and Lisa Schwarzbaum wrote, "Yet the images that linger longest in my memory are those of windswept livestock. And that, in a teacup, sums up everything thats right, and wrong, about this appealingly noisy but ultimately flyaway first blockbuster of summer".    In his review for the Los Angeles Times, Kenneth Turan wrote, "But the ringmaster of this circus, the man without whom nothing would be possible, is director De Bont, who now must be considered Hollywoods top action specialist. An expert in making audiences squirm and twist, at making us feel the rush of experience right along with the actors, De Bont choreographs action and suspense so beautifully he makes it seem like a snap."      Time (magazine)|Time magazines Richard Schickel wrote, "when action is never shown to have deadly or pitiable consequences, it tends toward abstraction. Pretty soon youre not tornado watching, youre special-effects watching".    In his review for the Washington Post Desson Howe wrote, "its a triumph of technology over storytelling and the actors craft. Characters exist merely to tell a couple of jokes, cower in fear of downdrafts and otherwise kill time between tornadoes".   

===Box office===
The film opened on May 10, 1996 and earned $41,059,405 from 2,414 total theaters, making it the number-one movie at the North American box office. It has gone on to earn a total of $241,721,524 at the North American box office. As of November 2012, it has earned a worldwide total of $494,471,524. It currently sits at number 76 on the all-time North American box office charts. Worldwide it sits at number 105 on the all-time earners list, not adjusted for inflation.  It is the 1996 in film#Highest-grossing films|second-highest-grossing film of 1996.

===Awards===
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- Academy Awards Academy Award Best Visual Effects Stefen Fangmeier
| 
|- John Frazier
| 
|- Habib Zargarpour
| 
|- Henry La Bounta
| 
|- Academy Award Best Sound Steve Maslow
| 
|- Gregg Landaker
| 
|- Kevin OConnell Kevin OConnell
| 
|- Geoffrey Patterson
| 
|- Golden Raspberry Award Golden Raspberry Worst Supporting Actress Jami Gertz
| 
|- Worst Written Film Grossing Over $100 Million Michael Crichton and Anne-Marie Martin
| 
|- Stinkers Bad Movie Awards 
|- Worst Picture
|- Ian Bryce
| 
|- Michael Crichton
| 
|- Kathleen Kennedy Kathleen Kennedy
| 
|- Worst Supporting Actress Jami Gertz
| 
|- Worst Screenplay for a Film Grossing Over $100 Million Worldwide Michael Crichton and Anne-Marie Martin
| 
|-
|}

==Urban legend== The Shining.    The facts of this incident were exaggerated into an urban legend that the theater was actually playing Twister during the tornado.  Commentary at Snopes.com   

On May 10, 2010, a tornado struck Fairfax, Oklahoma, destroying the farmhouse where numerous scenes in Twister were shot. J. Berry Harrison, the owner of the home and a former Oklahoma state senator, commented that the tornado appeared eerily similar to the fictitious one in the film. Harrison had lived in the home since 1978. 

==Theme park attraction==
 
The film was used as the basis for the attraction Twister...Ride It Out at Universal Studios Florida, which features filmed introductions by Bill Paxton and Helen Hunt.

==See also== The Wizard of Oz Into the Storm - A "spiritual successor" to Twister
* 1999 Oklahoma tornado outbreak

==References==
 

==External links==
 
*  
*  
*  
*   Wakita, Oklahoma

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 