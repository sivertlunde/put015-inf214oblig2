The Interrupters
{{Infobox film
| name           = The Interrupters
| caption        = The Interrupters theatrical poster
| image	         = The Interrupters FilmPoster.jpeg Steve James
| producer       = Steve James Alex Kotlowitz Zak Piper (co-producer)
| starring       = Ricardo "Cobe" Williams Eddie Bocanegra Ameena Matthews Tio Hardiman Gary Slutkin
| music          = Joshua Abrams
| cinematography = Steve James
| editing        = Steve James Aaron Wickenden
| studio         = Kartemquin Films / Rise Films
| distributor    = Cinema Guild (US Theatrical), PBSd (US DVD), Dogwoof (UK)
| released       =  
| runtime        = 125 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $282,448
}}
The Interrupters is a 2011 documentary film, produced by Kartemquin Films, that tells the story of three violence interrupters who try to protect their Chicago communities from the violence they once employed.  It examines a year in which Chicago drew national headlines for violence and murder that plagued the city.

The film features the work of CeaseFire (organization)|CeaseFire, an initiative of the Chicago Project for Violence Prevention. In 2004, Tio Hardiman (ex-Director of CeaseFire Illinois) created and implemented The Violence Interrupter concept. Violence interrupters Ameena Matthews, Cobe Williams and Eddie Bocanegra look back on their past experiences with street violence to try to steer young men and women in the right direction.   Matthews, the daughter of former Chicago gang leader Jeff Fort, comes to the aid of the mother of Derrion Albert, a Chicago high school student whose death made national headlines when it was captured on videotape.

Produced by  . 

==Setting==
The documentary takes place on South Side, Chicago|Chicagos South Side in a myriad of troubled communities.  One of the main communities featured is Englewood, Chicago|Englewood.  Englewood lies approximately 12 miles south of the Loop and began as a home to German and Irish farm and railroad workers. "Englewood." Encyclopedia of Chicago. Chicago:  Chicago Historical Society, 2005. Web. 5 Mar 2012  "Zook."Black P Stones." Chicago Gangs. (2012) Web. 5 Mar 2012 

However, after World War II, Englewood and the South Side of Chicago as a whole became known as the "Black Belt."  This is in reference to the predominately African American population moving to this area.  As of the year 2000, 0.6% of Englewood residents are White, while 98.2% are Black or African American.  5.2% of Englewood residents over the age of twenty-five have obtained a bachelor degree or higher. The unemployment rate of Englewood residents over the age of sixteen is 25.8%.  

Residents participate in gang activities and drug trafficking to make money.  The crime statistics for February 24, 2012 detail thirty-three documented crimes in a span of twenty-four hours.  One third of those crimes were narcotic offenses.  

Most of the drug activity can be traced back to two notorious gangs that run the area: The Gangster Disciples and the Black P Stones.  The Interrupters Ameena Matthews has ties to the Black P Stones as her father, Jeff Fort, was the founder and leader.  

==Release and reception==
The Interrupters premiered at the 2011 Sundance Film Festival.   On March 3, 2011, The Interrupters made its international premiere at the 2011 Adelaide Film Festival in Australia.  The film also picked up the Special Jury Award at the 18th annual Sheffield Doc/Fest - the UKs largest documentary festival - in June 2011. 

It was broadcast in Canada on the CBC News Network documentary series The Passionate Eye on January 28, 2012.  It was broadcast as part of the PBS series, Frontline (U.S. TV series)|Frontline, in February 2012.  Early reviews for the film suggested that The Interrupters was an early candidate for a nomination for the Academy Award for Best Documentary Feature, but it was not nominated.  

The film holds a "Certified Fresh" 100% approval rating on Rotten Tomatoes based on 83 reviews, with the consensus stating "Impeccably crafted and edited, The Interrupters is a tough and honest documentary about street violence that truly has the power to inspire change."

With 36.2% of the vote, The Interrupters won a poll asking readers of IndieWIRE|IndieWire, an International Independent Film website, "what buzz heavy 2011 film are you most excited to see?". 

After watching a screening of The Interrupters in October 2011, Bermudas National Security Minister vowed to implement a CeaseFire-style program in the country. 

The film won several awards, including "Best Documentary" from the 2012 Independent Spirit Awards, "Outstanding Achievement in Nonfiction Feature Filmmaking" and "Best Direction" from Cinema Eye Honors, and "Best Documentary" from the 2011 Miami International Film Festival, the Minneapolis / St. Paul Film Festival, and the African American Arts Alliance of Chicago Black Excellence Awards. Film critic Roger Ebert called the film "mighty and heart-wrenching." 

== References ==
 

== External links ==
*  
*  
*  
*  
*   on PBS NewsHour

 
 

 
 
 
 
 
 
 
 