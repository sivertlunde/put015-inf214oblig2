Raasi
{{Infobox film
| name = Raasi
| image =
| director = Murali-Abbas
| writer = Rambha Prakash Raj Vadivelu
| producer = S. S. Chakravarthy
| banner = NIC Arts
| cinematography = 
| editing = 
| music = Sirpy
| distributor =
| released = April 14, 1997
| runtime = 145 mins Tamil
}}
 Rambha in the lead roles with Prakash Raj and Vadivelu playing supporting roles. The film was released on 14 April 1997 to negative and mixed reviews and reception.

==Plot==
This film is about the family of Ajith and his Uncle Bala Singh (Rambhas father). The problem between two families start when Ajith is unable to spend too much money for an occasion in his uncles family. Whether Ajith and Rambha got united after solving out all the problems forms the crux of the story.

==Cast==
*Ajith Kumar as Kumar Rambha as Meena
*Prakash Raj
*Nagesh
*Vadivelu
*Jayachitra Sriman
*Neena as Kavitha

==Production==
Shilpa Shetty was initially approached to play the leading female role but her inavailability led to the producers selecting Rambha. 

==Release==
The film received negative and mixed reviews upon release with a critic claiming there was "nothing original in the story or its handling" whilst mentioning that "people have mostly inexplicable changes of heart throughout the movie". 
This film commercial successed.  after kathalkottai bigest success ajith act 5 classical success films

==Soundtrack==
*Kathalin Desam - Mano & Swarnalatha
*Yenady Yenady - Mano & Swarnalatha
*Yennai Thedatho - Chitra
*Vinnum Mannum - Mano & Unnikrishnan
*Thendral - Unnikrishnan & Chitra
*Poomalai - Singers: Hariharan

==References==
 

 
 
 
 


 