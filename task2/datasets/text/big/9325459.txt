Woubi Chéri
{{Infobox film
| name = Woubi Chéri
| image =WoubiCheri.jpg
| image size =
| caption =
| director = Laurent Bocahut Philip Brooks
| producer =
| writer =
| narrator =
| starring =
| music =
| cinematography =
| editing =Nadia Ben Rachid {{Cite web
  | last = 
  | first = 
  | authorlink = 
  | title = Nadia Ben Rachid: Film Editor Bio
  | work = 
  | publisher = anneaghionfilms.com
  | url = http://www.anneaghionfilms.com/bio_rachid.html
  | doi = 
  | accessdate =2010-01-01 }}
 
| distributor =
| released = 1998
| runtime =62 minutes
| country =France Côte dIvoire
| language =French
| budget =
| preceded by =
| followed by =
}}Woubi Chéri (English: Darling Woubi {{Cite book
  | last = Gikandi
  | first = Simon
  | authorlink = 
  | title = Encyclopedia of African literature
  | publisher = Taylor & Francis
  | year = 2003
  | location = 
  | pages = 315
  | url = http://books.google.co.uk/books?id=hFuWQmsM0HsC
  | doi = 
  | id = 
  | isbn =0-415-23019-5 }} Ivorian documentary that shows a few days in the life of various members of the gay and transgender community in Abidjan, Côte dIvoire. {{Cite book
 | last = Canty Quinlan
 | first = Susan
 | authorlink =
 |author2=Fernando Arenas
  | title = Lusosex: gender and sexuality in the Portuguese-speaking world
 | publisher = University of Minnesota Press
 | year = 2002
 | location =
 | pages = xxxii
 | url = http://books.google.co.uk/books?id=Iv6CUeIQySsC
 | doi =
 | id =
 | isbn =0-8166-3921-3 }}
  It is one of a very few films from Africa to deal with LGBT issues. 
 bisexual and also in conventional marriages. {{Cite book
  | last = López
  | first = Alfred J.
  | authorlink = 
  |author2=John C. Hawley
   | title = Postcolonial whiteness: a critical reader on race and empire
  | publisher = SUNY Press
  | year = 2005
  | location = 
  | pages = 68
  | url = http://books.google.co.uk/books?id=8t2oOnplU58C
  | doi = 
  | id = 
  | isbn =0-7914-6361-3 }}
  The film won Best Documentary awards at the New York Lesbian, Gay, Bisexual, & Transgender Film Festival, the Turin International Lesbian & Gay Film Festival, and the Transgender Festival in London. 

==See also==
*Dakan — a 1997 Guinean drama film dealing with homosexuality Forbidden Fruit — a 2000 Zimbabwean film about a lesbian relationship

==References==
 

==External links==
*  
* 

 
 
 
 
 
 
 
 
 
 


 
 