Sambhavami Yuge Yuge
{{Infobox film 
| name           = Sambhavami Yuge Yuge
| image          =
| caption        =
| director       = AB Raj
| producer       = KP Kottarakkara
| writer         = KP Kottarakkara
| screenplay     = KP Kottarakkara
| starring       = Prem Nazir Adoor Bhasi Jose Prakash Prameela
| music          = MS Baburaj
| cinematography = JG Vijayam
| editing        = K Sankunni
| studio         = Ganesh Pictures
| distributor    = Ganesh Pictures
| released       =  
| country        = India Malayalam
}}
 1972 Cinema Indian Malayalam Malayalam film, directed by AB Raj and produced by KP Kottarakkara. The film stars Prem Nazir, Adoor Bhasi, Jose Prakash and Prameela in lead roles. The film had musical score by MS Baburaj.   

==Cast==
 
*Prem Nazir
*Adoor Bhasi
*Jose Prakash
*Prameela
*Sankaradi
*T. S. Muthaiah
*Adoor Bhavani
*K. P. Ummer Khadeeja
*N. Govindankutty Sadhana
*Vijayasree
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ammayallaathoru Daivamundo || P Jayachandran, Chorus || Sreekumaran Thampi || 
|-
| 2 || Bhagavaan Bhagavathgeethayil || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 3 || Ellaam Maayajaalam || K. J. Yesudas, P Jayachandran || Sreekumaran Thampi || 
|-
| 4 || Mookkillaaraajyathe || K. J. Yesudas, P Susheeladevi || Sreekumaran Thampi || 
|-
| 5 || Naadodimannante || P Jayachandran, P. Leela, MS Baburaj || Sreekumaran Thampi || 
|-
| 6 || Thuduthude Thudikkunnu || P Jayachandran, KP Brahmanandan, B Vasantha || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 

 
 
 

 