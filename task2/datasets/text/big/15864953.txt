Riders of the Plains
{{Infobox film
| name           = Riders of the Plains
| image          = 
| caption        = 
| director       = Jacques Jaccard
| producer       = 
| writer         = Karl R. Coolidge Jacques Jaccard
| starring       = Jack Perrin Marilyn Mills
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 15 episodes 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 Western film serial directed by Jacques Jaccard.   

==Cast==
* Jack Perrin
* Marilyn Mills
* Ruth Royce
* Charles Brinley
* Kingsley Benedict
* Running Elk
* Robert Miles
* Rhody Hathaway
* Clark Comstock
* Boris Karloff

==See also==
* List of film serials
* List of film serials by studio
* Boris Karloff filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 