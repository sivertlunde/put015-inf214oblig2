An Affair of the Skin
{{Infobox film
| name           = An Affair of the Skin
| image          =
| caption        =
| director       = Ben Maddow
| producer       = Helen Levitt Ben Maddow
| writer         = Ben Maddow
| starring       = {{Plainlist |
*Viveca Lindfors Kevin McCarthy
*Lee Grant
*Diana Sands
*Herbert Berghof
}}
| music          =
| cinematography = Roger Barlow David Shore
| editing        = Verna Fields
| distributor    = Zenith International Film Corp.
| released       = 1963
| runtime        = 102 minutes
| country        =  
| awards         =
| language       =
| budget         =
}}
An Affair of the Skin is a 1963 film written and directed by Ben Maddow. It is a complex story of the romantic entanglements of its several characters as seen through the eyes of a black woman photographer. Shortly after its release, the film was harshly reviewed in Time Magazine and The New York Times.   Woody Hauts recent characterization is more sympathetic:  

Maddow is reported as feeling that the initial release of An Affair of the Skin had been rushed for financial reasons. In 1973, ten years later its initial release, Maddow re-edited and released it again under the title Love As Disorder. As described by John Hagan, "An offscreen narration by the photographer was added to establish her as an observer: a participant in the action but also a caustic chronicler of it. As in much of Maddows work, inner disorder is seen against a background of social unrest as described in a highly imagistic manner by a person who has both emotional involvement and critical detachment." 

==Plot summary==
This drama story take place in New York and describes the sexual relations of the three main characters, Victoria Allen and his wife Katherine including the problems they encounter, as told in three intertwined stories. Victoria is an aging fashion model who is afraid to let go of her young male lover because she thinks she is too old to ever get another man to love her. Allen and Katherine are an unhappily married neurotic couple with problems of their own. 

== Cast ==

:Victoria - Viveca Lindfors Kevin McCarthy
:Katherine McCleod - Lee Grant
:Janice - Diana Sands
:Max - Herbert Berghof
:Claire - Nancy Malone Osceola Archer
:Waiter - Will Lee

==References==
 
==External links==
* 

 
 
 
 
 
 
 
 
 

 