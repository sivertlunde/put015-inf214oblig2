Moby Dick (1956 film)
 
{{Infobox film
| name           = Moby Dick
| image          = Moby dick434.jpg
| caption        = 1976 theatrical re-release poster
| director       = John Huston
| producer       = {{Plainlist|
* Associate producers:
*      Jack Clayton
*      Lee Katz
* Co-producer:
*      Vaughn N. Dean
* Producer:
*      John Huston
}}
| screenplay = {{Plainlist|
* Ray Bradbury
* John Huston
}}
| based on  = {{Plainlist|
* Moby-Dick by
*      Herman Melville
}}
| narrator       = 
| starring       = {{Plainlist|
* Gregory Peck
* Richard Basehart
* Leo Genn
* Orson Welles
}}
| music          = Philip Sainton
| cinematography = Oswald Morris
| editing        = Russell Lloyd
| studio        = Moulin Productions
| distributor    = Warner Bros. 
| released       =   
| runtime        = 116 min.
| country        = United States
| language       = English
| budget         = United States dollar|US$ 4,500,000 gross = $5.2 million (US)  
}} film adaptation of Herman Melvilles novel Moby-Dick. It was directed by John Huston with a screenplay by Huston and Ray Bradbury. The film starred Gregory Peck, Richard Basehart, and Leo Genn.

The music score was written by Philip Sainton.

==Plot== sea mammal, but his obsession with vengeance is so great that he cannot turn back, eventually leading to the death of Ahab and all of his crew, save his newest able seaman, Ishmael.

==Cast==
*Gregory Peck as Captain Ahab
*Richard Basehart as Ishmael
*Leo Genn as Starbuck
*James Robertson Justice as Captain Boomer
*Harry Andrews as Stubb
*Bernard Miles as The Manxman Noel Purcell as Ships Carpenter
*Edric Connor as Daggoo
*Mervyn Johns as Peleg
*Joseph Tomelty as Peter Coffin
*Francis de Wolff as Captain Gardiner
*Philip Stainton as Bildad
*Royal Dano as Elijah
*Seamus Kelly as Flask
*Friedrich von Ledebur as Queequeg
*Orson Welles as Father Mapple
*Tamba Allenby as Pip (uncredited) Tom Clegg as Tashtego (uncredited)
*Ted Howard as Perth (uncredited)
*John Huston as the voice of Peter Coffin and a Pequod lookout (uncredited)

Peck was initially surprised to be cast as Ahab (part of the studios agreement to fund the film was that Huston use a "name" actor as Ahab). Peck later commented that he felt Huston himself should have played Ahab. Ironically, Huston had originally intended to cast his own father, the actor Walter Huston in the role, but his father had died by the time the film was made. Peck went on to play the role of Father Mapple in the 1998 television miniseries adaptation of Melvilles novel, with Patrick Stewart as Ahab.

Welles later used the salary from his cameo to fund his own stage production of Moby Dick, in which Rod Steiger played Captain Ahab.
 Treasure Island. It was destroyed by fire in Morecambe, England in 1972. 

The schooners used were Harvest King and James Postlethwaite, both from Arklow, Ireland. 

According to Gregory Peck, between his performances in this film and the 1998 Moby Dick miniseries, he liked the miniseries better because it was more faithful to the novel.

==Production==
During a meeting to discuss the screenplay, Ray Bradbury informed John Huston that regarding Melvilles novel, he had "never been able to read the damned thing". According to the biography The Bradbury Chronicles, there was much tension and anger between the two men during the making of the film, allegedly due to Hustons bullying attitude and attempts to tell Bradbury how to do his job, despite Bradbury being an accomplished writer. Bradburys novel Green Shadows, White Whale includes a fictionalized version of his writing the screenplay with John Huston in Ireland. Bradburys short story "Banshee" is another fictionalized account of what it was like to work with Huston on this film. In the television adaptation of the story for The Ray Bradbury Theater the Huston character was played by Peter OToole and the Bradbury surrogate by Charles Martin Smith.
 Walter as Moulin Rouge. The Mirisches made a deal with Warner Bros. in order to release the film. Under the agreement, Warners would distribute Moby Dick for seven years, after which all rights would revert to the Mirisch brothers company, Moulin Productions. 
 Madeira Islands, Las Palmas de Gran Canaria, Canary Islands, Spain. Captain Alan Villiers commanded the ship for the film.  
 public house, originally called Linehans and at that time owned by Paddy Linehan, whose exterior appears in the movie. It was renamed Moby Dicks shortly after filming by Mr. Linehan. It is still owned and run by the Linehan family and boasts a fine collection of photographs taken of the cast and crew during the making of the film. While there, John Huston used the bar as his headquarters to plan each days filming. The towns harbor basin, in front of Moby Dicks bar, was used to stand in as New Bedfords harbor, and some local people appear as extras in the ships departure scene. Youghals nineteenth century lighthouse also appears in a scene of the Pequod putting to sea (at sunset) on her fateful voyage.  

Of the three film versions of Moby Dick made between 1926 and 1956, Hustons is the only one which is faithful to the novel and uses its original ending.
 Dunlop in Stoke-on-Trent, England.    Moby Dick was 75&nbsp;ft long and weighed 12 Long ton|tons, and required 80 drums of compressed air and a hydraulic system in order to remain afloat and operational.  However the artificial whale came loose from its tow-line and drifted away in a fog.  Peck confirmed in 1995 that he was aboard the prop.  According to Morris, after the prop was lost the Pequod was followed by a barge with various whale parts (hump, back, fin, tail). 90% of the shots of the white whale are various size miniatures filmed in a water tank in Shepperton Studios in London. Whales and longboat models were built by a special effects man, August Lohman, working in conjunction with art director Stephen Grimes. Studio shots also included a life-size Moby jaw and head - with working eyes. The head apparatus which could move like a rocking horse was employed when actors were in the water with the whale.  Gregory Pecks last speech is delivered in the studio while riding the white whales hump (a hole was drilled in the side of the whale so Peck could conceal his real leg).

The films problems were further escalated by rising costs. The film went overbudget, from $2 million to around $4.4 million, which crippled Moulin Productions; Moby Dick was ultimately sold to United Artists in order to recoup some of the Mirisch brothers debt (Warners still distributed the film, corresponding to their original licensing agreement). Mirisch, p. 77  Moby Dick did not recoup its budget upon its initial release. 
 Anjelica stated in a 2003 Larry King Live interview that her father had "adored" Peck.   
 Robert Shaw), by showing him watching the 1956 version of the film and laughing at the inaccuracies therein. However, permission to use footage of the original film was denied by Gregory Peck as he was uncomfortable with his performance.   

At the age of four, Anjelica Huston met Peck dressed as Ahab when she visited the set of her fathers film.  Decades later, she and Peck would meet again and become close friends with each other until the latters death.  

==Changes from the original novel==
   
Although the film was quite faithful to the original novel, even down to the retention of Melvilles original poetic dialogue, there were several slight changes:

*In the film, Elijahs prophecy ("At sea one day, youll smell land where there be no land, and on that day, Ahab will go to his grave, but hell rise again within the hour. He will rise and beckon, then all, all save one shall follow.") foretells exactly what will happen to the Pequod and her crew in the film. In the novel, Elijah does not make a prophecy, but subtly hints that something will happen.    
 New Bedford while in the novel they meet in New Bedford but sail out of Nantucket.

*The demonic harpooneer Fedallah is totally omitted from the film. In the novel, it is the dead Fedallah who ends up lashed to the back of Moby Dick,   but in the film, this happens to Ahab. In the novel, Ahab is merely dragged into the water by the harpoon rope and is never seen again.

*In the film, when the dead Ahab "beckons" to the crew (an incident caused by the whale rolling back and forth while Ahab is tied to its back), Starbuck, who had previously bitterly opposed Ahabs quest for vengeance, is so moved by the sight that he becomes like a man possessed, and orders the crew to attack Moby Dick. This leads to the death of all except Ishmael, as the whale leaps on them in a fury. In the novel, Starbuck does not participate in the final hunt and the ship and her crew are lost after the Pequod is rammed by Moby Dick. In the movie, the Pequod is also rammed by the whale, but only after Moby Dick has killed the whole crew except Ishmael. Pip, the African-American cabin boy, has stayed on board the ship at Ahabs command, and is killed when the mast falls on him after the whale rams the ship.

==Reception==
The film has an 84% rating on Rotten Tomatoes, with the consensus that "It may favor spectacle in place of the deeper themes in Herman Melvilles novel, but John Hustons Moby Dick still makes for a grand movie adventure." 

==References==
 

==Further reading==
* Mirisch, Walter (2008). I Thought We Were Making Movies, Not History. University of Wisconsin Press, Madison, Wisconsin. ISBN 0-299-22640-9.

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 