Mosaic (film)
{{Infobox film
| name           = Mosaic
| image          = 
| caption        =
| director       = Roy Allen Smith
| producer       =
| story          = Stan Lee
| screenplay     = Scott Lobdell
| narrator       = Garry Chalk Ron Halder Nicole Oliver
| music          = William Kevin Anderson
| editing        = Shawn Logue
| studio         = Manga Entertainment
| distributor    = Anchor Bay Entertainment
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} animated superhero Garry Chalk, Ron Halder, and Nicole Oliver. It was released under the Stan Lee Presents banner, which is a series of direct-to-video|direct-to-DVD animated films distributed by POW Entertainment with Anchor Bay Entertainment. The story was by Stan Lee, with the script by former X-Men writer Scott Lobdell.

Mosaic was released on DVD on  January 9, 2007, and had its television premiere on March 10, 2007, on Cartoon Networks Toonami. 

==Plot==
Aspiring young actress Maggie Nelson (Anna Paquin) gains chameleon-like powers after she is exposed to an electrical storm and a magic rune which her father, an Interpol agent, had brought home to study after it was found at the scene of a murder at a New York City museum. Her powers are from a secret and ancient race known as the Chameliel, who are able to hide in plain sight of the public due to their shape shifting abilities, and she is told all about the Chameliel after meeting a young Chameliel named Mosaic (Kirby Morrow). The murder victim at the museum was a Chameliel who was killed by another Chameliel named Maniken, who is intent on gaining the alchemical powers of his dead wife Facade, and ruling the world. After Maniken kidnaps her father, Maggie becomes determined to help Mosaic to fight Maniken.  

They go from New York City, to the catacombs of Rome, to a large radio dish at the magnetic North Pole, trying to stop Maniken from stealing some of the many powerful Chameleon stones that are hidden around the world and sacrificing Maggies father to use them in a ceremony that will grant Maniken the powers of his wife and rule Earth like a god. During the battle, Maggie uses her acting skills and shape-shifting abilities to fool Maniken into believing she is his dead wife come back to life, to distract him from noticing that Mosaic is planting explosives on the radio dish and getting her father to safety. Also while in battle, it is revealed that Mosaic is Maniken and Facades son who volunteered to the rest of the Chameliel to go after his father and stop him. They both tumble into an icy gorge, never to rise again. Maggie then sneaks aboard the Interpol copter her father is on, overhearing him vowing to destroy all Chameliel. Upon returning home, she plans to continue acting and agrees to her fathers request to continue her studies, but at the same time, she vows to search for the remaining Chameleon stones and use their power for good, and honor Mosaic and the rest of the Chameliel by becoming the new Mosaic.

==Cast==
* Anna Paquin as Maggie Nelson/Mosaic
* Kirby Morrow as Morrow (Mosaic)
* Kathleen Barr as Facade and Mrs. Nottenmyer Garry Chalk as Nathan Nelson
* Cam Clarke as Stephan
* Ron Halder as Manikin and an Italian captain
* Stan Lee as Stanley the security guard
* Scott McNeil as Mr. Bullwraith and a landlord
* Nicole Oliver as Agent Newell Jim Ward as a tour guide and a belligerent detective

==Maggies powers==
Maggies powers include shapeshifting, super strength, the ability to cling to surfaces allowing for scaling walls and ceilings (much like Spider-Man), seeing the DNA of other creatures (which is how she can compare humans to apes), regeneration, infrared vision, camouflage, and the ability to become invisible and mimic voices as well as a gift for understanding the Chameliel language (it is never explained if this ability shows that she can understand other languages or speak them but it makes sense). Strangely, when Maggie becomes invisible, her outline gives off the appearance that she is completely naked, even though she is in fact wearing clothes. Why her clothes are not outlined when her body is invisible is never made clear.

==References==
   

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 