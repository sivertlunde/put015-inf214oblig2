Milan (1946 film)
 
 
{{Infobox film
| name           = Milan
| image          = 
| director       = Nitin Bose
| producer       = Hiten Chaudhary
| writer         = Rabindranath Tagore  screenplay      = Sajanikanta Das
| starring       = Dilip Kumar Mira Misra Anil Biswas cinematography  = R. Karmarkar editing         = Kali Raha  studio          = The Bombay Talkies Ltd. runtime         = 120 min
| released       = 1946
| language       = Hindi country         = India
}} 1946 Hindi Bengali story Anil Biswas    and the lyrics written by Pyare Lal Santoshi and Arzu Lakhnavi.

The story revolves around a young law student who is forced into an arranged marriage though he is in love with somebody else. Following the wedding the drama focuses on the brides mistaken identity and the search for her husband.

==Story==

The story is set in 1905. Ramesh (Dilip Kumar) is studying law in Calcutta and has just appeared for his final exams. He is a friend and neighbour of Jogen who is also studying law. Jogen (Shyam Laha) lives with his father Annada Babu (Moni Chatterjee) and sister Hemnalini (Ranjana). Ramesh and Hemnalini are fond of each other and Ramesh visits their house most days for tea. Their association is disliked by another friend Akshay (Pahari Sanyal) who also likes Hemnalini. Ramesh has been asked to come back to his village for the holidays by his father but is dissuaded from going by Hemnalini. Rameshs father Braja Mohan (K. P. Mukherjee) is from the priestly caste and lives in the village. He receives an anonymous letter stating that his son is involved with the neighbours daughter who comes from a tradesman caste and that he spends his entire time there.  Braja Mohan goes to the city and brings Ramesh back with him to the village. He has arranged Rameshs wedding with a poor widows daughter.  Ramesh tries to convince his father about his involvement with Hemnalini. His father after satisfying himself that Ramesh has as yet not committed himself to Hemnalini prevails upon Ramesh to marry Sushila because of the promise he had given to the girls mother. There is a storm at night when the wedding party from the grooms side is returning to their village by boat. During the crossing the boat capsizes. Nearly all on the boat are drowned including Rameshs father and Ramesh appears to be the only one to survive. He sees a woman in bridal dress lying unconscious on the bank. He brings her to his village but soon understands that this is a case of mistaken identity. She continuously checks him when he calls her Sushila and tells him her name is Kamala. He realises there was another bridal procession and their boat too had capsized.

After the formalities of his fathers funeral service Ramesh decides to take Kamala to Calcutta. He finds out about her only living relative, an uncle, and writes to him. A letter arrives telling him of the death of Kamalas uncle but the sender has mentioned the husbands name and profession. His name is Nalin and hes a doctor. Ramesh starts searching for Dr. Nalin. He has made no mention of his marriage to Hemnalini or her family nor told anyone regarding the mistaken identity of his supposed bride to avoid any embarrassment to the girl.

On arrival in Calcutta, Ramesh suggests that Kamala get an education. After allaying her apprehension regarding her age he admits her in a girls boarding school in the same city. Akshays sister also studies in the same school and through her Akshay gets to know the truth about Rameshs marriage. In the evening he questions Ramesh in front of Hemnalini. Ramesh deflects the question and asks Hem to trust him. Preparations are on at Annadas house for the wedding of Hemnalini and Ramesh. Their wedding is set for the coming Sunday but Ramesh is asked by the principal to take Kamala home for the weekend. Ramesh postpones the wedding and brings Kamala back from school. Akshay brings Jogen to Rameshs house where they see Kamala and on being questioned Ramesh keeps silent. Hemnalini goes into a state of shock when shes told about Rameshs wife. Her father takes her to Kashi to recuperate. Ramesh decides to leave Calcutta and he takes Kamala with him to Ghazipur. Kamala reads the letter Ramesh has written to Hem explaining the entire situation and mentioning Kamalas husbands name.  She finally recognises the truth about her and Rameshs situation. She decides to kill herself and leaves the house.  She is rescued and comes under Nalins mothers care. She realises that Nalin is her husband but finds out that Hem and he are to be betrothed. However, Nalin is not happy about the betrothal as he refuses to believe that Kamala is dead and wants to wait a while longer.  Finally the truth comes out and shes accepted by her husband and his mother while Ramesh and Hemnalini get back together.

==Cast==
*Dilip Kumar: Ramesh
*Mira Misra:      Kamala
*Ranjana:         Hemnalini
*Moni Chatterjee: Annada
*S. Nazeer      : Nalianaksha
*Pahari Sanyal: Akshay
*Shyam Loha:      Jogen
*K. P. Mukherjee : Braja Mohan (Rameshs father)

==Production==
 Jwar Bhata in (1944), had his acting groomed by Nitin Bose in this film.    Dilip Kumar acknowledged this in an interview: "Nitin Bose became a friend after Milan and he changed the way I interpreted and studied my scripts and roles".   

==Reception==

According to one source the film was not a success but achieved critical acclaim for cinematographer R. Karmakar.    However, another source called the film a success and one of the reasons cited was its music.   
The Box Office collections for 1946 were   which in adjusted gross is more than     

==Discography==
The composer was Anil Biswas and lyricists Pyare Lal Santoshi and Arzu Lakhnavi.    Parul Ghosh who sang most of the songs in the film was Anil Biswass younger sister and had the distinction of being one of the first recorded playback singer in the industry.    Geeta Dutt was the other female singer in the film.

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="centr"
! # !! Title !! Singer(s)!! Lyricist
|-
| 1
| "Gungun Gungun Bole Bhanwarva"
| Parul Ghosh
| Pyare Lal Santoshi
|-
| 2
| "Upar Hai Badariya Kari"
| Shankar Dasgupta 
| Pyare Lal Santoshi
|-
| 3
| "Suhani Beriyan Beet Jaayein"
| Parul Ghosh  
| Arzu Lakhnavi
|-
| 4
| "Who Kahein Aap Ki"
| 
| Arzu Lakhnavi
|-
| 5
| "Main Kiski Laaj Nibhaoon"
| Parul Ghosh
| Arzu Lakhnavi
|-
| 6
| "Jisne Bana di Bansuri"
| Parul Ghosh
| Arzu Lakhnavi
|-
| 7
| "Chhan Mein Bajegi"
| Geeta Dutt
| Pyare Lal Santoshi
|-
| 8
| "Tumhe Saajan Manaye"
| Geeta Dutt
| Pyare Lal Santoshi
|-
|}

==References==
 

==External links==
* 

 
 
 
 
 
 
 