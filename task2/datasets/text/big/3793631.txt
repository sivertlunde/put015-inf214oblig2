Children of Men
 
 
 
{{Infobox film
| name           = Children of Men
| image          = Children of men ver4.jpg
| alt            = A man is shown from shoulder-up standing behind a glass pane with his head visible through a hole in the glass. A tagline reads, "The year 2027: The last days of the human race. No child has been born for 18 years. He must protect our only hope."
| caption        = Theatrical release poster
| director       = Alfonso Cuarón
| producer       = {{Plainlist|
* Hilary Shor Iain Smith Tony Smith
* Marc Abraham Eric Newman 
}}
| screenplay     = {{Plainlist|
* Alfonso Cuarón
* Timothy J. Sexton
* David Arata Mark Fergus Hawk Ostby
}}
| based on       =  
| starring       = {{Plainlist|
* Clive Owen
* Julianne Moore
* Michael Caine
* Chiwetel Ejiofor
* Charlie Hunnam 
}}
| music          = John Tavener
| cinematography = Emmanuel Lubezki
| editing        = {{Plainlist| Alex Rodríguez
* Alfonso Cuarón
}}
| production companies = {{Plainlist|
* Strike Entertainment
* Hit and Run Productions
}} Universal Pictures
| released       =  
| runtime        = 109 minutes  
| country        = {{Plainlist|
* United Kingdom
* United States 
}}
| language       = English
| budget         = $76 million    
| gross          = $69.9 million 
}} science fiction thriller film novel of the same name, was credited to five writers, with Clive Owen making uncredited contributions. The film takes place in 2027, where two decades of human infertility have left society on the brink of collapse. Illegal immigrants seek sanctuary in the United Kingdom, where the last functioning government imposes oppressive immigration laws on refugees. Owen plays civil servant Theo Faron, who must help a refugee (Clare-Hope Ashitey) escape the chaos. Children of Men also stars Julianne Moore, Michael Caine, Chiwetel Ejiofor, and Charlie Hunnam.
 Best Cinematography Best Film BAFTA Awards, Best Cinematography Best Production Best Science Fiction Film.

==Plot== asylum seekers British forces round up and detain immigrants. Theo Faron, a former activist turned cynical bureaucrat, is kidnapped by the Fishes, a militant immigrants rights group. They are led by Theos estranged wife, Julian Taylor, from whom he separated after their sons death.
 government minister. The bearer must be accompanied, so Theo agrees to escort Kee in exchange for a large sum. Luke, a Fishes member, drives them and former midwife Miriam towards the coast to a boat. They are ambushed by an armed gang and Julian is killed. Luke kills two police officers who stop their car and they escape to a safe house.

Kee reveals to Theo that she is the only known pregnant woman on Earth. Julian had told her to only trust Theo, intending to hand Kee to the "Human Project", a supposed scientific group in the Azores dedicated to curing infertility. However, Luke persuades Kee to stay. That night, Theo eavesdrops on a meeting of Luke and other Fishes, and discovers that Julians death was orchestrated by the Fishes so Luke could take over as leader, and the Fishes could use the baby as a political tool to support the coming revolution. Theo wakes Kee and Miriam and they steal a car, escaping to the secluded hideaway of Theos aging friend Jasper Palmer.
 Bexhill refugee suicide drug contractions begin on a bus, Miriam distracts a suspicious guard by feigning mania and is taken away.

At the camp, Theo and Kee meet a Romani woman, Marichka, who provides a room where Kee gives birth to a girl. The next day, Syd finds them in their room and informs Theo and Kee that war has broken out between the British Army and the refugees, including the Fishes. Having learned that they have a bounty on their heads, he attempts to capture them, but Marichka and Theo fight him off and the group escapes. Amidst the fighting, the Fishes capture Kee and the baby. Theo tracks them to an apartment under heavy fire from the military and escorts her out. Awed by the baby, the combatants temporarily stop fighting. Marichka leads Theo, Kee, and the baby to a boat in a sewer and Theo rows away. As they watch the bombing of Bexhill by the Royal Air Force from a distance, Theo reveals that he has been shot. He dies from his wounds as the Tomorrow approaches through the fog.

==Cast==
 
* Clive Owen as Theo Faron, a former activist who was devastated when his child died during a flu pandemic.    Theo is the "archetypal everyman" who reluctantly becomes a saviour.    Cast in April 2005,  Owen spent several weeks collaborating with Cuarón and Sexton about his role.  Impressed by Owens creative insights, Cuarón and Sexton brought him on board as a writer.    "Clive was a big help," Cuarón told Variety (magazine)|Variety. "I would send a group of scenes to him, and then I would hear his feedback and instincts."   
* Julianne Moore as Julian Taylor. For Julian, Cuarón wanted an actor who had the "credibility of leadership, intelligence,   independence".   Moore was cast in June 2005, initially to play the first woman to become pregnant in 20 years.   "She is just so much fun to work with," Cuarón told Cinematical.  "She is just pulling the rug out from under your feet all the time. You dont know where to stand, because she is going to make fun of you."  Michael Phillips homage to Steve Bell.
* Chiwetel Ejiofor as Luke
* Charlie Hunnam as Patric
* Clare-Hope Ashitey as Kee, an illegal immigrant and the first pregnant woman in eighteen years. She did not appear in the book, and was written into the film based on Cuaróns interest in the recent single-origin hypothesis of human origins and the status of dispossessed people:  "The fact that this child will be the child of an African woman has to do with the fact that humanity started in Africa. Were putting the future of humanity in the hands of the dispossessed and creating a new humanity to spring out of that." 
* Pam Ferris as Miriam
* Peter Mullan as Syd Ministry of Arts programme "Ark of the Arts", which "rescues" works of art such as Michelangelos David (Michelangelo)|David (albeit with a broken leg), Pablo Picassos Guernica (painting)|Guernica, and Banksys British Cops Kissing. He mentions that he tried to save Michelangelos Pietà (Michelangelo)|Pietà, but a mob destroyed it before he could.
* Oana Pellea as Marichka
* Paul Sharma as Ian
* Jacek Koman as Tomasz
 

==Themes==
===Hope===
Children of Men explores the themes of hope and faith  in the face of overwhelming futility and despair.        The films source, the novel The Children of Men by P. D. James, describes what happens when society is unable to reproduce, using male infertility to explain this problem.      In the novel, it is made clear that hope depends on future generations. James writes, "It was reasonable to struggle, to suffer, perhaps even to die, for a more just, a more compassionate society, but not in a world with no future where, all too soon, the very words justice, compassion, society,’ struggle, evil, would be unheard echoes on an empty air." 

The film switches the infertility from male to female,  but never explains its cause: environmental destruction and divine punishment are considered.  This unanswered question (and others in the film) have been attributed to Cuaróns dislike for expository film: "Theres a kind of cinema I detest, which is a cinema that is about exposition and explanations&nbsp;... Its become now what I call a medium for lazy readers&nbsp;... Cinema is a hostage of narrative.  And Im very good at narrative as a hostage of cinema."   Cuaróns disdain for back-story and exposition led him to use the concept of female infertility as a "metaphor for the fading sense of hope".   The "almost mythical" Human Project is turned into a "metaphor for the possibility of the evolution of the human spirit, the evolution of human understanding."    Without dictating how the audience should feel by the end of the film, Cuarón encourages viewers to come to their own conclusions about the sense of hope depicted in the final scenes: "We wanted the end to be a glimpse of a possibility of hope, for the audience to invest their own sense of hope into that ending. So if youre a hopeful person youll see a lot of hope, and if youre a bleak person youll see a complete hopelessness at the end."   

===Contemporary references===
Children of Men takes an unconventional approach to the modern action film, using a documentary, newsreel style.  Film critics Michael Rowin, Jason Guerrasio and Ethan Alter observe the films underlying touchstone of immigration. Alter notes that the film "makes a potent case against the Opposition to immigration|anti-immigrant sentiment" popular in modern societies like the United Kingdom and the United States, with Guerrasio describing the film as "a complex meditation on the politics of today".    
 The Maze.   Other popular images appear, such as a sign over the refugee camp reading "Homeland Security".   The similarity between the hellish, cinéma vérité stylized battle scenes of the film and current news and documentary coverage of the Iraq War, is noted by film critic Manohla Dargis, describing Cuaróns fictional landscapes as "war zones of extraordinary plausibility".   

In the film, refugees are "hunted down like cockroaches", rounded up and put into cages and camps, and even shot, leading film critics like Chris Smith and Claudia Puig to observe symbolic "overtones" and images of the Holocaust.    This theme is reinforced in the scene where an elderly refugee woman speaking German is seen detained in a cage,    and in the scene where British Homeland Security strips and beats illegal immigrants; a song by The Libertines, "Arbeit Macht Frei", plays in the background.   "The visual allusions to the Nazi roundups are unnerving," writes Richard A. Blake.  "It shows what people can become when the government orchestrates their fears for its own advantage."   

Cuarón explains how he uses this imagery to propagate the theme by cross-referencing fictional and futuristic events with real, contemporary, or historical incidents and beliefs:

 

In the closing credits, the Sanskrit words "Shantih Shantih Shantih" appear as end titles.   Writer and film critic Laura Eldred of the University of North Carolina at Chapel Hill observes that Children of Men is "full of tidbits that call out to the educated viewer". During a visit to his house by Theo and Kee, Jasper says "Shanti, shanti, shanti." Eldred notes that the "shanti" used in the film is also found at the end of an Upanishad and in the final line of  T. S. Eliots poem The Waste Land, a work Eldred describes as "devoted to contemplating a world emptied of fertility: a world on its last, teetering legs". "Shanti" is also a common beginning and ending to all Hindu prayers, and means "peace," referencing the invocation of divine intervention and rebirth through an end to violence. 

===Religion=== Divine Comedy, Canterbury Tales, heroic journey to the south coast mirrors his personal quest for "self-awareness",  a journey that takes Theo from "despair to hope". 

According to Cuarón, the title of P. D. James book (The Children of Men) is a Catholic allegory derived from a   of the  ." 
  This divergence from the original was criticised by some, including Anthony Sacramone of First Things, who called the film "an act of vandalism", noting the irony of how Cuarón had removed religion from P.D. James fable, in which morally sterile nihilism is overcome by Christianity. 

The film has been noted for its use of Christian symbolism; for example, British terrorists named "Ichthys|Fishes" protect the rights of refugees.  Opening on Christmas Day in the United States, critics compared the characters of Theo and Kee with Joseph and Mary,  calling the film a "modern-day Nativity story".     Kees pregnancy is revealed to Theo in a barn, alluding to the manger of the Nativity scene, when Theo asks Kee who the father of the baby is she jokingly states she is a virgin, and when other characters discover Kee and her baby, they respond with "Jesus Christ" or the sign of the cross.  Also the Archangel Gabriel (among others divinities) is invoked in the bus scene.

To highlight these spiritual themes, Cuarón commissioned a 15-minute piece by British composer John Tavener, a member of the Eastern Orthodox Church whose work resonates with the themes of "motherhood, birth, rebirth, and redemption in the eyes of God."  Calling his score a "musical and spiritual reaction to Alfonsos film", snippets of Taveners "Fragments of a Prayer" contain lyrics in Latin, German and Sanskrit sung by mezzo-soprano, Sarah Connolly. Words like "mata" (mother), "pahi mam" (protect me), "avatara" (saviour), and "alleluia" appear throughout the film.  

==Production==
 
* Alfonso Cuarón – director, screenwriter, editor
* Timothy J. Sexton – screenwriter
* David Arata – screenwriter Mark Fergus – screenwriter Hawk Ostby – screenwriter
* Hilary Shor – producer Iain Smith – producer Tony Smith – producer
* Marc Abraham – producer Eric Newman – producer
* Emmanuel Lubezki – cinematographer
* Jim Clay – production designer
* Geoffrey Kirkland – production designer
* Álex Rodríguez (film editor)|Álex Rodríguez – editor
* Jany Temime – costume designer
* John Tavener – composer
 
 Harry Potter The Battle of Algiers as a model for social reconstruction in preparation for production, presenting the film to Clive Owen as an example of his vision for Children of Men.  In order to create a philosophical and social framework for the film, the director read literature by Slavoj Žižek, as well as similar works.    The film Sunrise (film)|Sunrise was also influential. 

===Location=== A Clockwork London bombings occurred, but the director never considered moving the production.  "It would have been impossible to shoot anywhere but London, because of the very obvious way the locations were incorporated into the film," Cuarón told Variety. "For example, the shot of Fleet Street looking towards St. Pauls would have been impossible to shoot anywhere else."  Due to these circumstances, the opening terrorist attack scene on Fleet Street was shot a month and a half after the London bombing. 
 Red Desert.   Cuarón added a pig balloon to the scene as homage to Pink Floyds Animals (Pink Floyd album)|Animals.   Other art works visible in this scene include Michelangelos David (Michelangelo)|David,  Pablo Picasso|Picassos Guernica (painting)|Guernica,  and Banksys British Cops Kissing.   London visual effects companies Double Negative and Framestore worked directly with Cuarón from script to post production, developing  effects and creating "environments and shots that wouldnt otherwise be possible". 
 The Historic Dockyard in Chatham was used to film the scene in the empty activist safehouse. 

===Style and design===
"In most sci-fi epics, special effects substitute for story.  Here they seamlessly advance it," observes Colin Covert of Star Tribune.  Billboards were designed to balance a contemporary and futuristic appearance as well as easily visualizing what else was occurring in the rest of the world at the time, and cars were made to resemble modern ones at first glance, although a closer look made them seem unfamiliar.   Cuarón informed the art department that the film was the "anti-Blade Runner",  rejecting technologically advanced proposals and downplaying the science fiction elements of the 2027 setting.  The director focused on images reflecting the contemporary period.   

===Single-shot sequences===
Children of Men used several lengthy   effects.   

Cuarón had experimented with long takes in  . Jonah was so unflashy compared to those films. The camera keeps a certain distance and there are relatively few close-ups. Its elegant and flowing, constantly tracking, but very slowly and not calling attention to itself." 

The creation of the single-shot sequences was a challenging, time-consuming process that sparked concerns from the studio. It took fourteen days to prepare for the single shot in which Clive Owens character searches a building under attack, and five hours for every time they wanted to reshoot it. In the middle of one shot, blood splattered onto the lens, and cinematographer Emmanuel Lubezki convinced the director to leave it in. According to Owen, "Right in the thick of it are me and the camera operator because were doing this very complicated, very specific dance which, when we come to shoot, we have to make feel completely random." 
 continuity during the roadside ambush scene was dismissed by production experts as an "impossible shot to do". Fresh from the visual effects-laden Harry Potter and the Prisoner of Azkaban, Cuarón suggested using computer-generated imagery to film the scene. Lubezki refused to allow it, reminding the director that they had intended to make a film akin to a "raw Documentary film|documentary". Instead, a special camera rig invented by Gary Thieltges of Doggicam Systems was employed, allowing Cuarón to develop the scene as one extended shot.   A vehicle was modified to enable seats to tilt and lower actors out of the way of the camera, and the windshield was designed to tilt out of the way to allow camera movement in and out through the front windscreen. A crew of four, including the director of photography and camera operator, rode on the roof. 
 Double Negative team created over 160 of these types of effects for the film.   In an interview with Variety, Cuarón acknowledged this nature of the "single-shot" action sequences: "Maybe Im spilling a big secret, but sometimes its more than what it looks like. The important thing is how you blend everything and how you keep the perception of a fluid choreography through all of these different pieces." 

Tim Webber of VFX house Framestore CFC was responsible for the three-and-a-half minute single take of Kee giving birth, helping to choreograph and create the CG effects of the childbirth.  Cuarón had originally intended to use an animatronic baby as Kees child with the exception of the childbirth scene. In the end, two takes were shot, with the second take concealing Clare-Hope Ashiteys legs, replacing them with prosthetic legs. Cuarón was pleased with the results of the effect, and returned to previous shots of the baby in animatronic form, replacing them with Framestores computer-generated imagery|computer-generated baby. 

===Sound===
 
Cuarón uses sound and music to bring the fictional world of social unrest and infertility to life.    A creative yet restrained combination of rock, pop, electronic music, hip-hop and classical music replaces the typical  s version of "Hush (Billy Joe Royal song)|Hush" blaring from Jaspers car radio becomes a "sly lullaby for a world without babies" while King Crimsons "The Court of the Crimson King" make a similar allusion with their lyrics, "three lullabies in an ancient tongue". 

Amongst a genre-spanning selection of electronic music, a remix of Aphex Twins "Omgyjya Switch 7", which includes the Male This Loud Scream audio sample by Thanvannispen,  not present on the original (nor indeed on the official soundtrack) can be heard during the scene in Jaspers house, where Jaspers "Strawberry Cough" (a potent, strawberry-flavoured blend of marijuana) is being sampled.  During a conversation between the two men, Radioheads "Life in a Glasshouse" plays in the background.
 Pressure are also featured. 

For the Bexhill scenes during the films second half, the director makes use of silence and cacophonous sound effects such as the firing of automatic weapons and loudspeakers directing the movement of "fugees" (illegal immigrants).  Here, classical music by George Frideric Handel, Gustav Mahler, and Krzysztof Pendereckis "Threnody to the Victims of Hiroshima" complements the chaos of the refugee camp.  Throughout the film, John Taveners Fragments of a Prayer is used as a spiritual motif to explain and interpret the story  without the use of narrative. 

A few times during the film, a loud, ringing tone evocative of tinnitus is heard. This sound generally coincides with the death of a major character (Julian, Jasper) and is referred to by Julian herself, who describes the tones as the last time youll ever hear that frequency. In this way, then, the loss of the tones is symbolic of the loss of the characters. 

==Release== world premiere at the 63rd Venice International Film Festival on 3 September 2006.  On 22 September 2006, the film debuted at number 1 in the United Kingdom with $2.4&nbsp;million in 368 screens.    It debuted in a limited release of 16 theaters in the United States on 22 December 2006, expanding to more than 1,200 theaters on 5 January 2007.   , Children of Men had grossed $69,612,678 worldwide, with $35,552,383 of the revenue generated in the United States. 

===Critical reception===
The film received critical acclaim; on the review tallying website  , the film has a rating of 84 based on 36 reviews, indicating "universal acclaim". 

  of The New York Times called the film a "superbly directed political thriller", raining accolades on the long chase scenes.  "Easily one of the best films of the year" said Ethan Alter of Film Journal International, with scenes that  "dazzle you with their technical complexity and visual virtuosity."  Jonathan Romney of The Independent praised the accuracy of Cuaróns portrait of the United Kingdom, but he criticized some of the films futuristic scenes as "run-of-the-mill future fantasy."  Film Comments critics poll of the best films of 2006 ranked the film number 19 while the 2006 readers poll ranked it number two.  On their list of the best movies of 2006, The A.V. Club, the San Francisco Chronicle, Slate and The Washington Post placed the film at number one.    Entertainment Weekly ranked the film seventh on its end-of-the-decade, top ten list, saying, "Alfonso Cuaróns dystopian 2006 film reminded us that adrenaline-juicing action sequences can work best when the future looks just as grimy as today."   

Peter Travers of Rolling Stone ranked it number 2 on his list of best films of the decade, writing:

 

According to Metacritics analysis of the most often and notably noted films on the best-of-the-decade lists, Children of Men is considered the eleventh greatest film of the 2000s.

===Top ten lists===
The film appeared on many critics top ten lists as one of the best films of 2006: 

 
* 1st – Ann Hornaday, The Washington Post
* 1st – Keith Phipps, The A.V. Club
* 1st – Peter Hartlaub, San Francisco Chronicle
* 1st – Tasha Robinson, The A.V. Club
* 2nd (of the decade) – Peter Travers, Rolling Stone
* 2nd – Ray Bennett, The Hollywood Reporter
* 2nd – Scott Tobias, The A.V. Club
* 3rd – Roger Ebert, Chicago Sun-Times
* 4th – Kevin Crust, Los Angeles Times
* 4th – Wesley Morris, The Boston Globe
* 5th – Rene Rodriguez, The Miami Herald
* 6th – Manohla Dargis, The New York Times
* 7th – Empire (magazine)|Empire
* 7th – Kirk Honeycutt, The Hollywood Reporter
* 7th – Ty Burr, The Boston Globe
* 8th – Kenneth Turan, Los Angeles Times (tied with Pans Labyrinth)
* 8th – Scott Foundas, LA Weekly (tied with LEnfant (film)|LEnfant)
* 8th – Scott Foundas, The Village Voice Dana Stevens, Slate (magazine)|Slate
* Unordered - Liam Lacey and Rick Groen, The Globe and Mail
* Unordered - Peter Rainer, The Christian Science Monitor
* Unordered - Mark Kermode, BBC Radio 5 Live
 

In 2012, director Marc Webb included the film among his list of Top 10 Greatest Films when asked by Sight & Sound for his votes for the BFI The Top 50 Greatest Films of All Time. 

===Accolades=== USC Scripter Best Adapted Screenplay at the 79th Academy Awards.
 Best Cinematography Best Production Best Cinematography 21st American Society of Cinematographers Awards.  The Australian Cinematographers Society also awarded Lubezki the 2007 International Award for Cinematography for Children of Men. 
 Best Dramatic Presentation, Long Form by the members of the World Science Fiction Convention. 

===Home media=== human geographer Fabrizio Eva, cultural theorist Tzvetan Todorov, and philosopher and economist John N. Gray; "Under Attack" features a demonstration of the innovative techniques required for the car chase and battle scenes; Clive Owen and Julianne Moore discuss their characters in "Theo & Julian"; "Futuristic Design" opens the door on the production design and look of the film; "Visual Effects" shows how the digital baby was created.  Deleted scenes are included.  The film was released on Blu-ray Disc in the United States on 26 May 2009. 
 

==See also== Lifetime TV series about an infertility pandemic
* The White Plague, a Frank Herbert novel about a genetically-targeted plague unleashed upon humanity that kills only women, forcing humans to face the prospect of extinction.
* Engines of Creation (book)|Engines of Creation, a nonfiction book by K. Eric Drexler about the prospect of strong nanotechnology which notes that targeted infertility (intentional, unintentional, benevolent, malevolent) is a possibility of strong nanotechnology or molecular manufacturing.

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 