Unjustified Absence
{{Infobox film
| name =   Unjustified Absence
| image =
| image_size =
| caption =
| director = Max Neufeld
| producer =  Giuseppe Amato
| writer =  István Békeffy (play)   Amedeo Castellazzi   Aldo De Benedetti   Carlo Della Posta   Max Neufeld
| narrator =
| starring = Alida Valli   Amedeo Nazzari   Lilia Silvi   Paolo Stoppa
| music = Cesare A. Bixio
| cinematography = Václav Vích
| editing = Maria Rosada
| studio =  Era Film
| distributor = Minerva Film
| released = 15 November 1939
| runtime = 90&nbsp;minutes
| country = Italy Italian
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Unjustified Absence (Italian:Assenza ingiustificata) is a 1939 Italian comedy film directed by Max Neufeld and starring Alida Valli, Amedeo Nazzari and Lilia Silvi. A girl leaves school to marry a doctor, but becomes annoyed by his constant absences and decides to secretly resume her studies. 

==Cast==
*Alida Valli as Vera Fabbri
*Amedeo Nazzari as Il dottore Carlo Cristiani
*Lilia Silvi as Luisa Albertini
*Paolo Stoppa as Eugenio Sinibaldi
*Guglielmo Barnabò as Il signor Fabbri
*Lia Orlandini as La signora Fabbri
*Lauro Gazzolo as Il preside
*Olga Solbelli as La professoressa cattiva
*Giacomo Moschini as Il professore buono
*Pina Gallini as Uninsegnante
*Giana Cellini as Una infermiera
*Bianca Della Corte as Una compagna di scuola di Vera
*Daniella Drei as Una compagna di scuola di Vera
*Liliana Vismara as Una compagna di scuola di Vera
*Armandina Bianchi as Una compagna di scuola di Vera
*Maria-Pia Vivaldi as Una compagna di scuola di Vera
*Nella Morganti as Una compagna di scuola di Vera
*Luisa Papa as Una compagna di scuola di Vera

== References ==
 

== Bibliography ==
*Gundle, Stephen. Mussolinis Dream Factory: Film Stardom in Fascist Italy. Berghahn Books, 2013.

== External links ==
* 

 

 
 
 
 
 
 
 


 