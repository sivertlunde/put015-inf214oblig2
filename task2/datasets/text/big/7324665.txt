Cheech & Chong's The Corsican Brothers
{{Infobox film
| name           = Cheech & Chongs The Corsican Brothers
| image          = Cheech & Chongs The Corsican Brothers.jpg
| caption        = Theatrical release poster
| director       = Tommy Chong
| producer       = Peter Macgregor-Scott
| writer         = Tommy Chong Cheech Marin Rikki Marin
| starring = {{Plainlist|
* Cheech Marin
* Tommy Chong
}}
| music          = George S. Clinton
| cinematography = Harvey Harrison
| editing        =
| distributor    = Orion Pictures
| released       =  
| runtime        = 82 min.
| country        = United States
| awards         =
| language       = English
| budget         =
| gross          = $3,772,785
}}
 Alexandre Dumas novel, The Corsican Brothers.

==Plot==
 rock band gypsy storyteller. She tells them the story of the Corsican brothers who attempt to overthrow a cruel member of a monarchy. After the story, they continue to play in the streets and give an epic final performance before the credits start to roll.

==Cast==
*Cheech Marin as Louis Corsican
*Tommy Chong as Lucian Corsican
*Roy Dotrice as The Evil Fuckaire/Ye Old Jailer
*Shelby Chong as Princess I
*Rikki Marin as Princess II
*Edie McClurg as The Queen
*Robbi Chong as Princess III
*Rae Dawn Chong as The Gypsy
*Kay Dotrice as The Midwife
*Jennie C. Kos as The Pregnant Mother
*Martin Pepper as Martin
*Yvan Chiffre as Tax Collector #1
*Dan Schwarz as Tax Collector #2
*Jean-Claude Dreyfus as Marquis Du Hickey
*Serge Fedoroff as Nostrodamus

==External links==
* 
*  
*  
* 

 
 

 
 
 
 
 
 
 

 