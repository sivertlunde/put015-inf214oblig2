Before Sunset
 
{{Infobox film
| name = Before Sunset
| image = Before Sunset poster.jpg
| image_size = 220px
| alt = 
| caption = Theatrical release poster
| director = Richard Linklater
| producer = Richard Linklater Anne Walker-McBay	
| screenplay = Richard Linklater Ethan Hawke Julie Delpy
| story = Richard Linklater Kim Krizan
| based on =  
| starring = Ethan Hawke Julie Delpy
| music = Julie Delpy
| cinematography = Lee Daniel
| editing = Sandra Adair
| studio = Castle Rock Entertainment
| distributor = Warner Independent Pictures
| released =  
| runtime = 80 minutes  
| country = United States
| language = English French
| budget = $2.7 million 
| gross = $15,992,615
}} romantic drama film, the sequel to Before Sunrise (1995). Like its predecessor, the film was directed by Richard Linklater. He shares screenplay credit with actors Ethan Hawke and Julie Delpy, and with Kim Krizan, the screenwriter for the first film featuring these two characters. 
 real time as they spend an afternoon together. 
 Best Adapted Screenplay.
 Before Midnight, which was released in 2013 and also gained acclaim.

==Plot== Shakespeare and Company. Flashbacks express elements of his time with Céline in Vienna. Three journalists attend the reading to interview Jesse: one is convinced the books main characters meet again, another that they do not, and a third who wants them to but is doubtful that will occur. As Jesse speaks with the audience, his eyes wander and he sees Céline there, smiling at him.

After the presentation, the bookstore manager reminds Jesse of his need to leave for the airport in about an hour for his plane. Céline and Jesses time together is again constrained. They make the best of it, and their conversations become deeply personal. They begin with themes of work and politics and, with increasing passion, approach their earlier feelings for each other.

They touch on their failure to have met as planned six months after their first encounter. Jesse returned to Vienna but Céline did not, because her grandmother had suddenly died. Since the pair had never exchanged addresses, they had no way to contact each other at the time. 

Their talk reveals their life changes. Jesse, now a writer, is married and has a son. Céline has become an advocate for the environment, and has a photojournalist boyfriend. They each express some dissatisfaction with their lives. 

They talk as they walk in Paris, with scenes showing a café, garden, bateau mouche, and Jesses hired car. Their former feelings are slowly rekindled, as their one night together looms large in memory, unmarred by ordinary trials. Jesse says his book was inspired by his hope of seeing Céline again. She says that it brought back painful memories. They begin to approach each other tentatively but pull back. 

In the concluding scene, Céline and Jesse arrive at her apartment. Jesse persuades her to play a waltz on her guitar. Her song is about their earlier brief encounter. Jesse puts a Nina Simone CD on the stereo system. Céline dances to the song, "Just in Time," as Jesse watches her. Céline imitates Simone, saying, "Baby ... you are gonna miss that plane." Jesse smiles and says, "I know."

==Cast==
* Ethan Hawke as Jesse
* Julie Delpy as Céline
* Vernon Dobtcheff as Bookstore manager
* Louise Lemoine Torres as Journalist #1
* Rodolphe Pauly as Journalist #2
* Mariane Plasteig as Waitress
* Diabolo as Philippe
* Denis Evrard as Boat attendant
* Albert Delpy (Julie Delpys father) as Man at grill
* Marie Pillet (Julie Delpys mother) as Woman in courtyard

==Production==
After the filming of Before Sunrise, Linklater, Hawke, and Delpy discussed making a sequel. Linklater considered a version to be filmed in four locations and with a much larger budget. When his proposal did not secure funding, he scaled back the concept of the movie.    In a 2010 interview, Hawke said that the three had worked on several potential scripts over the years. As time passed and they did not secure funding, they adapted elements of the earlier scripts for Before Sunrise in their final draft of Before Sunset.    

Linklater described the process of completing the final version of the film as 
 "we sat in a room and worked together in about a two- or three-day period, worked out a very detailed outline of the whole film in this sort of real-time environment. And then, over the next year or so, we just started e-mailing each other and faxing. I was sort of a conduit – they would send me monologues and dialogues and scenes and ideas, and I was editing, compiling and writing. And thats how we came up with a script."  Hawke said, "Its not like anybody was begging us to make a second film. We obviously did it because we wanted to." 
 Shakespeare and Left Bank. Marais district 4th arrondissement, 11th arrondissement, 12th arrondissement, on board a bateau mouche from Quai de la Tournelle to Quai Henri IV, the interior of a taxi, and finally "Célines apartment." Described in the film as located at 18 passage des Petites-Ecuries, it was filmed in Cour de lÉtoile dOr off rue du Faubourg St-Antoine.
 the summer was one of the hottest on record, the cast and crew suffered along with the city residents, as temperatures exceeded 100 degrees F (38&nbsp;°C) for most of the production. 

The film is notable for essentially taking place in real time, i.e. the time elapsed in the story is the run time of the film. In the fast-changing temperate Paris climate, this created challenges for the cinematographer Lee Daniel to match the color and intensity of the skies and ambient light from scene to scene. The scenes were mostly shot in sequence, as they were still developing the screenplay. Producer Anne Walker-McBay worked with less time and less money than she had on Before Sunrise, but still brought the film in on time and on budget. The sequel was released nine years after Before Sunrise, the same amount of time that has lapsed in the plot since the events of the first film.

The film was released in the wake of Hawkes divorce from Uma Thurman. Some commentators drew parallels between Hawkes personal life and the character of Jesse in the film.  Additional comment has noted that both Hawke and Delpy incorporated elements of their own lives into the screenplay.   Delpy wrote two of the songs featured in the film, and a third by her was included in the closing credits and movie soundtrack.

==Release== premiered at the Berlin International Film Festival in February 2004, and received a limited release in the United States on July 2, 2004.

===Box office===
In its opening weekend, the film grossed $219,425 in 20 theaters in the United States, averaging $10,971 per theater. During its entire theatrical run, the film grossed $5.8 million in the United States and nearly $16 million worldwide.   

===Critical reception===
Before Sunset received a highly positive reception from the critics. It held a 95% favorable rating at Rotten Tomatoes based on 159 reviews,  and was assigned a weighted average score of 90/100 by Metacritic based on 39 reviews from mainstream publications, indicating "universal acclaim".  The film appeared on 28 critics top 10 lists of the best films of 2004,    and took the 27th spot on Metacritics list of The Best-Reviewed Movies of the Decade (2000–09).  In the United Kingdom, the film was ranked the 110th-greatest movie of all time by a 2008 Empire (magazine)|Empire poll.  In 2010, the critics at The Guardian placed Before Sunrise/Before Sunset at number 3 in their list of the best romantic films of all time, and called the ending of Before Sunset "one of the most tantalising and ingenious endings in all cinema." 

In comparing this film with the first featuring these characters, American film critic Roger Ebert wrote, "Before Sunrise was a remarkable celebration of the fascination of good dialogue. But Before Sunset is better, perhaps because the characters are older and wiser, perhaps because they have more to lose (or win), and perhaps because Hawke and Delpy wrote the dialogue themselves."  In her review for the Los Angeles Times, Manohla Dargis lauded the film as a "deeper, truer work of art than the first," and praised director Linklater for making a film that "keeps faith with American cinema at its finest." 

Reviewing the acting, Peter Travers of Rolling Stone observed, "Hawke and Delpy find nuance, art and eroticism in words, spoken and unspoken. The actors shine."  Philip French of The Observer wrote, 
 "Both Hawke and Delpy are excellent and their performances have real depth. This time, too, theyre doing more than appearing as fictional creations in a Richard Linklater film. They now share the writing credit with him and are clearly putting much of their experiences of the past decade into characters they have possessed and been possessed by."  

On the merits of the script, A. O. Scott of The New York Times noted, it was "sometimes maddening," but "also enthralling, precisely because of its casual disregard for the usual imperatives of screenwriting." He elaborated, 
 "Cant they just say what they mean? Can you? Language, after all, is not just about points and meanings. It is a medium of communication, yes, but also of avoidance, misdirection, self-protection and plain confusion, all of which are among the themes of this movie, which captures a deep truth seldom acknowledged on screen or in books."  

;Top 10 lists
As noted by Metacritic, the film appeared on the following critics top 10 lists of 2004. 

 
 
*1st – Village Voice Film Poll (94 film critics surveyed)
*1st – Stephanie Zacharek, Salon.com
*1st – Marc Savlov, Austin Chronicle
*2nd – David Ansen, Newsweek
*2nd – Mick LaSalle, San Francisco Chronicle
*2nd – Carla Meyer, San Francisco Chronicle
*3rd – Ty Burr, The Boston Globe
*3rd – Kimberley Jones, Austin Chronicle
*3rd – James Berardinelli, ReelViews
*3rd – Charles Taylor, Salon.com
*3rd – Dennis Lim, Village Voice
*4th – Manohla Dargis, The New York Times Michael Atkinson, Village Voice
*4th – Michael Sragow, Baltimore Sun
 
*5th – Ruthe Stein, San Francisco Chronicle
*6th – Michael Wilmington, Chicago Tribune
*6th – Owen Gleiberman, Entertainment Weekly
*6th – Jack Mathews, New York Daily News
*6th – J. Hoberman, Village Voice
*7th – Jonathan Rosenbaum, Chicago Reader
*7th – Lawrence Toppman, Charlotte Observer
*8th – Scott Foundas, LA Weekly
*9th – Claudia Puig, USA Today
*9th – Marjorie Baumgarten, Austin Chronicle
*No order specified – Ella Taylor, LA Weekly
*No order specified – Carina Chocano, Los Angeles Times
*No order specified – Steven Rea, Philadelphia Inquirer
*No order specified – Shawn Anthony Levy, Portland Oregonian
 

===Awards and nominations===
;Awards Boston Society of Film Critics Award – Best Film (2nd place)

;Nominations Best Writing (Adapted Screenplay) for Richard Linklater, Ethan Hawke, Julie Delpy, and Kim Krizan. Best Screenplay for Richard Linklater, Ethan Hawke, and Julie Delpy Best Adapted Screenplay for Richard Linklater, Ethan Hawke, Julie Delpy, and Kim Krizan.
* 2004 Berlin International Film Festival – Golden Bear
* 2004 Gotham Awards – Best Film

==Sequel==
Linklater, Hawke, and Delpy all discussed the possibility of a sequel to Before Sunset.     Hawke said he wanted to develop the relationship between Jesse and Céline, and said, "Ill be shocked if we never make another one".   
 Best Adapted Screenplay. 

==References==
 

==External links==
 
*  
*  
*  
*  
*   filming locations at Movieloci.com
*   by Los Angeles Times

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 