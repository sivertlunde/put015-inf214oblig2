Give Me Liberty (1936 film)
{{Infobox film
| name           = Give Me Liberty
| image          = 
| caption        = 
| director       = B. Reeves Eason
| producer       = Vitaphone
| writer         = Forrest Barnes
| starring       = John Litel Nedda Harrigan
| music          = M.K. Jerome, Jack Scholl 
| cinematography = W. Howard Greene
| editing        = Louis Hesse
| distributor    = Warner Brothers Pictures
| released       =  
| runtime        = 22 minutes
| country        = United States 
| language       = English
| budget         = 
}}
 short drama Best Short Subject (Color).      

==Cast==
* John Litel - Patrick Henry
* Nedda Harrigan - Doxie Henry
* Carlyle Moore, Jr. - Captain Milton
* Robert Warwick - George Washington George Irving - Thomas Jefferson
* Boyd Irwin - British Commissioner
* Gordon Hart - Anti-Rebel Delegate Speaker
* Myrtle Stedman - Martha Washington
* Shirley Lloyd - Party Guest Giving Patrick a Violin
* Ted Osborne - Randolph Peyton (as Theodore Osborne)
* Carrie Daumery - Party Guest
* Jesse Graves - Moses (Washingtons servant)
* Wade Lane - Judge
* Charles Frederick Lindsley - Narrator
* Wilfred Lucas - His Excellency, permitting Henrys arrest
* Jack Mower - Gentleman
* Bancroft Owen - Tom
* Paul Panzer - Man with fur hat
* Sam Rice - Convention delegate extra
* John J. Richardson - Man kneeling to trip the Commissioner
* Cyril Ring - Delegate shouting "Treason! Treason!"
* Lottie Williams - Party guest at the Henrys William Worthington - Pendleton

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 