1984 (1956 film)
{{Infobox film
| name           = 1984
| image          = 1984film.jpg
| caption        = Theatrical release poster Michael Anderson
| producer       = N. Peter Rathvon
| writer         = Ralph Gilbert Bettison William Templeton George Orwell (novel)
| starring       = Edmond OBrien Michael Redgrave Jan Sterling
| music          = Malcolm Arnold
| cinematography = C.M. Pennington-Richards
| editing        = Bill Lewthwaite
| studio         = Holiday Film Productions Ltd.
| distributor    = Columbia Pictures
| released       = March 6, 1956
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = £80,073 
}}
 novel of Michael Anderson, 1954 television version of the novel, playing the character of Syme, which for the movie was amalgamated with that of Parsons. The character OBrien (1984)|OBrien, the antagonist, was renamed "OConnor", since the name of the main actor was Edmond OBrien. After the customary distributor agreement expired, the movie was withdrawn from theatrical and TV distribution channels by Orwells estate and was not obtainable legally for many years, although it has since been released in DVD format and clips have been copied to the website YouTube.  

==Cast==
* Edmond OBrien as Winston Smith. General OConnor.
* Jan Sterling as Julia.
* David Kossoff as Charrington.
* Mervyn Johns as Jones.
* Donald Pleasence as R. Parsons.
* Carol Wolveridge as Selina Parsons.
* Ernest Clark as Outer Party Announcer.
* Patrick Allen as Inner Party Official.
* Michael Ripper as Outer Party Orator.
* Ewen Solon as Outer Party Orator.
* Kenneth Griffith as Prisoner.

==See also==
*List of films featuring surveillance

==References==
 

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 


 