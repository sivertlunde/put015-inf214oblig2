In the Mix (film)
{{Infobox film name     = In the Mix image          = In the mix.jpg caption = Promotional poster for In the Mix producer         = John Dellaverson director       = Ron Underwood screenplay     = Jacqueline Zambrano story          = Chanel Capra Cara Dellaverson Brian Rubenstein starring  Usher Emmanuelle Chriqui Chazz Palminteri Robert Davi Kevin Hart music         = Aaron Zigman cinematography = Clark Mathis editing         = Don Brochu studio  Ush Entertainment distributor  Lionsgate (USA) 20th Century Fox (International) released        =   runtime         = 95 minutes language        = English  country        = United States budget         = gross  = $10,223,896   http://www.boxofficemojo.com/movies/?id=inthemix.htm 
}}
 pop singer Usher (entertainer)|Usher. It was released in the United States on November 23, 2005, the film being targeted at the traditionally large Thanksgiving weekend audience.

== Plot ==

When New York’s hottest night club disc jockey, Darrell, is asked to DJ a party for mob boss Frank, Darrell becomes acquainted with Franks beautiful daughter Dolly. Dolly is engaged to Chad, who her father wants her to marry. When Dolly and Darrell fall in love, Darrell is put in danger as both Franks mob and Darrells ex-girlfriend Cherise attempt to intervene.

== Cast ==
   Usher as Darrell
*Chazz Palminteri as Frank
*Emmanuelle Chriqui as Dolly Kevin Hart as Busta
*Robert Costanzo as Fat Tony
*Robert Davi as Fish
*Matt Gerald as Jackie
*Anthony Fazio as Frankie Junior
*Geoff Stults as Chad
*Chris Tardio as Angelo
*K.D. Aubert as Cherise
*Isis Faust as Lexi
*Nick Mancuso as Salvatore
*Page Kennedy as Twizzie
*Deezer D as Jojo

==Release==
The film grossed $10,223,896 at the US box office. 

==Reception==
In the Mix received negative reviews from critics, where it currently holds a 14% rating on Rotten Tomatoes based on 37  reviews. The film is also listed on the IMDb Bottom 100, where it currently ranks at #89 with a score of 2.4 out of 10 based on 6,172 votes. 

==Soundtrack==
#Rico Love – "Settle Down" One Chance Jon A. Gordon, Michael A. Gordon ( )– "Thats My Word"
#Christina Milian – "Be What Its Gonna Be" Chris Brown – "Which One" (featuring Noah!)
#Keri Hilson – "Hands & Feet"
#Rico Love – "Sweat" (featuring Usher (entertainer)|Usher)
#Ryon Lovett – "Get Acquainted" One Chance, Jon A. Gordon, Michael A. Gordon ( )– "Could This Be Love" Anthony Hamilton – "Some Kind of Wonderful"
#Robin Thicke – "Against the World"
#YoungBloodz – "Murda" (featuring Cutty and Fat Dog)
#Rico Love – "On the Grind" (featuring Juelz Santana and Paul Wall)

== References ==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 