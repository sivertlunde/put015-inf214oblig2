Helium (film)
{{Infobox film
| name = Helium
| image = 
| caption = 
| director = Anders Walter
| producer = 
| writer = Christian Garnst Miller-Harris Anders Walter
| starring = Casper Crump Pelle Falk Krusbæk   Marijana Jankovic
| music = 
| cinematography = Rasmus Heise
| editing = Lars Wissing
| studio = 
| distributor = 
| released =  
| runtime = 23 minutes
| country = Denmark
| language = Danish
}}
Helium is an Oscar winning 2014 short film by Danish film maker Anders Walter.

==Plot==
Alfred is a young boy staying in a hospital who suffers from an undisclosed terminal illness. Enzo, a janitor at the hospital, meets Alfred while working and the two develop a friendship. Enzo tells Alfred of Helium, an attractive alternative to Heaven, because Alfred imagines Heaven is very boring. Enzo tells Alfred that to get to Helium, he will fly in an airship that will know to pick him up because of his red balloon dog, which Enzo has made for him.

Alfreds illness worsens, and he is moved to a unit to which Enzo doesn’t have access. Enzo sneaks onto the unit, but is caught by the head nurse and barred from seeing Alfred. As Alfreds condition worsens, Enzo wonders if he is making it worse for the boy. He voices these concerns to a nurse saying, “I’m feeding him lies.” She disagrees and tells Enzo “you’re giving him hope.” 

Alfred gets worse, and having no access to him, Enzo writes out “the end of the story” to have the nurse read to him. As the nurse is about to read the ending to a dying Alfred, she changes her mind and, instead, sneaks Enzo onto the unit to tell the rest himself.

As he tells the story, the film switches to Alfred’s point of view. With no dialogue, he gets out of bed fully dressed and goes to the window. A giant airship is waiting for him. He walks across an extended ladder from the hospital to the airship. As the airship flies away, hospital windows full of red balloon dogs can be seen.

==Cast==
* Pelle Falk Krusbæk as Alfred. A hospitalized young boy with an undisclosed terminal illness.
* Casper Crump as Enzo. A janitor at the hospital.
* Marijana Jankovic as nurse. The nurse at the hospital who is responsible for Alfred.

==Accolades==
{| class="wikitable" width="95%"
! colspan="5" style="background: LightSteelBlue;" | Awards
|-
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Result 
|-
| Academy Award  
| March 2, 2014 Best Live Action Short Film
| Anders Walter Kim Magnusson
|  
|-
|}

==References==
 

==External links==
* 

 

 
 
 

 