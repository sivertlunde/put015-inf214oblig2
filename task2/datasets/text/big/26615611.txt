Abodh
{{Infobox film
| name           = Abodh
| image          = Abodh (movie poster).jpg
| image_size     =
| caption        = poster
| director       = Hiren Nag
| producer       = Tarachand Barjatya
| writer         =
| narrator       =
| starring       = Tapas Paul Madhuri Dixit Vinod Sharma
| music          = Ravindra Jain
| cinematography =
| editor       =
| distributor    = Rajshri Productions
| released       = August 10, 1984
| runtime        =
| country        = India Hindi
| budget         =
}}
 1984 Bollywood film directed by Hiren Nag. It marked the debut of actress Madhuri Dixit. 

==Plot==

Naive, childish and precocious, Gauri lives in a small town with her parents. At a fair, she gets into an argument with Shankar. Shortly thereafter, Gauri is told that her marriage has been arranged. At first Gauri is thrilled, then changes her mind when she finds out that her groom is Shankar, but finally reconciles herself to this marriage. After she re-locates to Shankars residence, she spends much of her time playing with Shankars little brother. Her husband starts to realize that Gauri has not matured yet. He is unable to get intimate with her. Gauri soon becomes bored, and re-locates to her parents house, where she resides for several months. It is here that she truly grows up, realizing the importance of marriage and intimacy. She then returns to Shankars house, where she is welcomed back - though not quite warmly enough. Gauri starts to realize that her childishness has created distance between her and Shankar. He re-locates to study in another town. For several months, the family does not get any news about him. Shankars father Gajanan Singh visits the hostel where his son is supposed to be staying. However, Gajanan finds that Shankar has left the hostel. For 3 months, Shankar goes missing. In the end, Shanker meets Gauris childhood friend Ratna by chance. After talking to Ratna, Shankar realises that Gauri has matured and loves him. He goes running back to meet Gauri. They both reconcile.

==Cast==
*Tapas Pal as Shankar Singh
*Madhuri Dixit as Gauri
*Vinod Sharma as Gajanan Singh (Shankars father)
*Leela Mishra as Shankars grandmother (Gajanans mom)
*Sheela David as Ratna
*Rajshri Nair as (as Rajshri)
*Savita Prabhune as Gauris sister-in-law
*Dinesh Hingoo as Wedding Photographer
*Mohan Choti as Photographer at Fair
*Sunder Purohit
*Ashok Saraf as Hanuman

==References==
 

==External links==
*  

 
 
 


 