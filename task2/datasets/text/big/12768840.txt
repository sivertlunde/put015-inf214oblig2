Muriel (film)
{{Infobox film
| name           = Muriel
| image          = Muriel1963.jpg
| caption        =
| director       = Alain Resnais
| producer       = Anatole Dauman
| writer         = Jean Cayrol
| starring       = Delphine Seyrig Jean-Pierre Kérien Jean-Baptiste Thiérrée   Nita Klein
| music          = Hans Werner Henze
| cinematography = Sacha Vierny
| editing        = Kenout Peltier   Eric Pluet
| distributor    =
| released       = 24 July 1963 (France)
| runtime        = 115 min.
| country        = France/Italy
| language       = French
| budget         =
| preceded_by    =
| followed_by    =
| website        =
|}}
 Nuit et Brouillard (Night and Fog) (1955).

==Plot==
Hélène, a widow who runs an antique business from her own apartment in Boulogne-sur-Mer, is visited by a past lover, Alphonse.  Her stepson, Bernard, is tormented by the memory of a girl named Muriel whom he has participated in torturing while doing military service in Algeria.

The story takes place over 15 days in September–October 1962.  (The screenplay provides specific dates and times for each scene, but these are not apparent in the film.)  An extended sequence takes place on the first day (a section lasting about 45 minutes: the introductions of Alphonse and his niece Françoise to Hélène and Bernard, and their first meal together).  Another long sequence takes place on the last day (the Sunday lunch and its revelations, and the scattering of the principal characters in their different directions).  The intervening days are represented in a series of fragmented scenes, which are chronological but seldom consecutive, and the passage of time is blurred.

==Cast==
* Delphine Seyrig, as Hélène Aughain, a 40-ish widowed antique dealer who retains obsessive memories of her love affair with Alphonse when she was 16, and the unexplained manner of their separation in 1939.
* Jean-Pierre Kerien, as Alphonse Noyard, Hélènes sometime lover, who says he has spent many of the intervening years in Algeria running a bar.
* Nita Klein, as Françoise, an aspiring young actress having an affair with the much older Alphonse, and biding her time before leaving him.
* Jean-Baptiste Thierrée, as Bernard, Hélènes stepson, recently returned from doing his military service in Algeria where his role in the torture of a girl called Muriel has left him with traumatic memories.
* Martine Vatel, as Marie-Do, the intermittent girl-friend of Bernard who is ready to set off for a new life in South America.
* Claude Sainval, as Roland de Smoke, a Boulogne property-developer and amorous friend of Hélène.
* Laurence Badie, as Claudie, a friend of Hélène with whom she shares a taste for gambling.
* Jean Champion, as Ernest, the brother-in-law of Alphonse.
* Philippe Laudenbach, as Robert, a fellow-soldier with Bernard in Algeria, now an active member of the Organisation de larmée secrète|OAS.
* Jean Dasté, as the man with a goat, who has returned from Australia.

==Production==
Resnais and Jean Cayrol first discussed the project of Muriel in 1959. They developed the script while Resnais was working on LAnnée dernière à Marienbad as well as on two other (uncompleted) projects relating to the then contentious topic of the war in Algeria. Interview with François Thomas, included with Arte DVD of Muriel (2004).  Cayrol, though primarily a poet and novelist, was himself interested in film-making and editing, and he produced a screenplay for Muriel in which nearly all of the complex editing sequences were outlined. 

Filming took place between November 1962 and January 1963. Location shooting was done in Boulogne-sur-Mer, which is almost another character in the film, a town whose centre has seen rapid rebuilding after extensive war damage and which is presented as both ancient and modern, uncertainly balanced between its past and future. Robert Benayoun, Alain Resnais, arpenteur de limaginaire. (Paris: Ramsay, 2008.) p.109.   The scenes in Hélènes apartment where most of the action takes place were filmed on a set at Studios Dumont in Épinay, but Resnais asked the designer Jacques Saulnier to reconstruct exactly a real apartment which he had seen in Boulogne, even down to the colour of the woodwork.   The décor of the apartment is modern but, because of Hélènes business as an antique dealer, it is full of furniture of different styles and periods which continually change through the film.   Resnais explained his intentions: "We used everything that could give this impression of incompleteness, of unease. ...The challenge of the film was to film in colour, that was essential, never to move the camera position, to film a week behind the   scenario, to invent nothing, and to do nothing to make it prettier". 

Filming lasted for 12 or 13 weeks, the longest shooting time of any of Resnaiss films.  According to Resnais, there were around 800 shots in the film instead of the usual 450; the many static camera set-ups were time-consuming; and it was only in the final shot of the film that the camera moved. 

Music for the film was written by Hans Werner Henze who picked up the visual principle of multiple fixed camera shots by adopting a musical style which mirrored the fragmentation of the film structure.   A series of verses, by Cayrol, are sung throughout the film (by Rita Streich); the relative lack of clarity of the words on the soundtrack was attributed by Resnais to the effect of having a German composer (who at the time did not speak French) setting French words.   The full words of the verses are included in the published screenplay of the film. 

The song "Déja", with words about the passing of time, which is sung unaccompanied by the character of Ernest near the end of the film, was written for a musical review in 1928 by Paul Colline and Paul Maye.  It was one of several elements in the film which were prompted by Resnaiss interest in "music-hall" and the theatre. 

==Themes==
At a press conference at the Venice Film Festival in 1963, Resnais said that his film depicted "the malaise of a so-called happy society. ...A new world is taking shape, my characters are afraid of it, and they dont know how to face up to it."   Muriel has been seen as part of a cinema of alienation of the 1960s, films which "betray a sudden desperate nostalgia for certain essential values".   A sense of disruption and uncertainty is constantly emphasised, not least by the style of jump-cutting between events. "The technique of observing absolute chronology while simultaneously following a number of characters and treating even casual passers-by in the same manner as the main characters gives rise to a hallucinatory realism." 
 Le Petit Soldat, had been banned in France in 1960 and was not shown until 1963. Also in 1960, Resnais had been one of the signatories of the Manifesto of the 121, in which a group of intellectuals had declared opposition to the French governments military policy in Algeria.)  At the midpoint of Muriel, a sequence of newsreel with Bernards voiceover commentary presents the inescapable evidence of an incident of torture which continually haunts Bernard and explains his obsession with the girl he calls Muriel.

This "moment of truth" which has not been confronted is echoed in different forms in the past lives of each of the other main characters.   Hélène has been unable to overcome her sense of loss and betrayal from a past love affair; Hélène, Alphonse and Bernard all carry troubled memories of having lived through and survived World War II; and Boulogne itself presents the image of a town uneasily rebuilding itself over the devastation that it suffered in that war.  Hélènes apartment, with its half-finished décor and ever-shifting furniture, and seen by the camera only as a disjointed collection of spaces until the films final shot, offers a metaphor for the traumatised brain which is unable to put itself in order and see itself whole. 

==Reception==
The film was first presented in Paris on 24 July 1963, and it was then shown at the Venice Film Festival in September 1963.  It was for the most part very badly received by both the press and the public.  Resnais observed later that it had been his most expensive film to make and the one which had drawn perhaps the smallest audiences.   He also noted the paradox that it had subsequently become almost a cult film, attributing its difficulties for the public to the fact that its principal characters were people who continually made mistakes, which created a sense of unease. 

It nevertheless drew much attention from French film-makers and critics.   was one of several commentators who noted in Muriel a significantly innovative style and tone: "Muriel marks the advent of cinematic dodecaphony; Resnais is the Schoenberg of this chamber drama". 

Among English-language reviewers there was much perplexity about Muriel, described by the critic of The New York Times as "a very bewildering, annoying film".   The reviewer for The Times (London) shared an initial feeling of distrust and hostility, but admitted that "the films stature increases with a second viewing".   This recognition that Muriel benefited from, or required, multiple viewings was something which a number of commentators have agreed upon. 

Susan Sontag, reviewing the film in 1963, deemed Muriel to be "the most difficult, by far, of Resnais   three feature films", and went on to say that "although the story is not difficult to follow, Resnais techniques for telling it deliberately estrange the viewer from the story".  She found those techniques to be more literary than cinematic, and linked Resnaiss liking for formalism with contemporary trends in new novels in France such as those of Michel Butor.  While admiring the film for its intelligence and for the beauty of its visual composition, its performances, and its music, she remained dissatisfied by what she saw as its emotional coldness and detachment. 

The appearance of the Muriel on DVD led to some reconsideration of its qualities, generally with greater sympathy than on its first appearance.  The positive view of the film was unequivocally summarised by Philip French: "its a rich, beautifully acted masterpiece, at once cerebral and emotional, that rewards several viewings and is now less obscure than it seemed at the time". 

==Awards==
Delphine Seyrig won a Volpi Cup for best actress at the 1963 Venice Film Festival. The film was nominated for a Golden Lion.

==Restoration==
A restored version of the film was released on DVD in France in 2004 by Argos Films/Arte France Développement from a distorted video master that squeezed the image into a 1.66:1 picture format.

A DVD version with English subtitles was issued in the UK in 2009 by Eureka, in the Masters of Cinema series.  It uses the same transfer as the 2004 French DVD, but the mastering corrects the image resulting in a picture that fills out a "telecinema" screen format ratio of 1.78:1.

Argos later created a new HD scan after Resnais was shown the distorted video master used for the 2004 DVD. He approved the new HD master, but it has yet to be released on DVD or Blu-ray. 

==References==
 

==External links==
* 
* 
*  at LoBservatoire   
* , by Gildas Mathieu: an article about the filming of Muriel  

 

 
 
 
 