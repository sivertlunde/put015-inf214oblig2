Doc (film)
 

{{Infobox film
| name           = Doc
| image_size     = 
| image	=	Doc FilmPoster.jpeg
| alt            = 
| caption        = 
| director       = Frank Perry
| producer       = Frank Perry
| writer         = Pete Hamill
| starring       = Stacy Keach Faye Dunaway Harris Yulin Michael Witney Denver John Collins  Dan Greenburg
| music          = Jimmy Webb
| cinematography = Enrique Bravo Gerald Hirschfeld
| editing        = Alan Heim
| studio         = FP Films
| distributor    = United Artists
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Doc is a 1971 American western film, which tells the story of the gunfight at the O.K. Corral and of one of its protagonists, Doc Holliday. It stars Stacy Keach, Faye Dunaway and Harris Yulin. It was directed by Frank Perry, while Pete Hamill wrote the original screenplay. The film was shot in Almeria in southern Spain.

==Plot== Cochise County, who is striving to become the towns new sheriff in the election campaign.
 Cowboys include Tom and Frank McLaury, and Billy Claiborne.
 Civil War ended, he left Atlanta, Georgia and went to Richmond, Virginia and then to Baltimore, Maryland, to be a dentist. After some time he decided to go out to the West, looking for a drier environment to cure his tuberculosis, for which he visits a Chinaman for herbs. (At another point in the movie, he is taking laudanum.)
 showdown at the OK Corral takes place during a Festival|fiesta. John Behan (Richard Mckenzie), Wyatt Earp and Doc Holliday all survive the gunfight. Ike Clanton, Tom and Frank McClaury, and Billy Claiborne do not.

==Cast==
* Stacy Keach as Doc Holliday
* Faye Dunaway as Kate Elder
* Harris Yulin as Wyatt Earp
* Michael Witney as Ike Clanton
* Denver John Collins as The Kid
* Dan Greenburg as John Clum|Clum, editor The Tombstone Epitaph
* John Scanlon  as Bartlett, saloon owner Richard McKenzie John Behan
* John Bottoms as Virgil Earp
* Ferdinand Zogbaum as James Earp Mattie Earp Alley Earp James Greene Frank McLowery
* Antonia Rey as Concha, whore
* Philip Shafer as Morgan Earp

==Awards==
*Doc won the 1971 Western Writers of America Spur Award for the Best Movie Script by Peter Hamill.

==External links==
*  
*  

 

 
 
 
 
 