Brothers from the Walled City
 
 
{{Infobox film
| name           = Brothers from the Walled City
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 城寨出來者
| director       = Lam Ngai Kai
| producer       = Mona Fong
| writer         = The Shaw Brothers Creative Group
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Chin Siu-ho
| music          = Shing Chin Yung Su Chen-hou
| cinematography = Lan Nai-Tsai Tsao An-Sung
| editing        = Chiu Cheuk Man Ma Chung Yiu Yu Shao Feng
| studio         = Shaw Brothers Studio
| distributor    = 
| released       =  
| runtime        = 89 min
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 4.04 M. HK$
}} Hong Kong film directed by Lam Ngai Kai and produced by the Shaw Brothers Studio.

==Cast==
* Chin Siu-ho
* Philip Ko Fei
* Johnny Wang Lung Wei
* Liu Lai Ling
* Wong Ching
* Kwan Hoi-san
* Wong Mei Mei
* Lung Tin Sang
* So Hang Suen
* Pak Man Biu

==See also==
* Kowloon Walled City

==External links==
*  
*  
*  

 
 


 