Guilty of Treason
{{Infobox film
| name           = Guilty of Treason
| image          =
| caption        =
| director       = Felix E. Feist Robert Golden   Jack Wrather
| writer         = Emmet Lavery
| narrator       = Paul Kelly   Richard Derr
| music          = Emil Newman   Hugo Friedhofer John L. Russell
| editing        = Walter A. Thompson
| studio         = Freedom Productions
| distributor    = Eagle-Lion Films Darker Images Video (2002 Video) Reel Media International (2004) (Worldwide) (VHS) Alpha Video Distributors (2005 DVD) Reel Media International (2005 & 2007) (Worldwide) (All Media) Hollywood Classics (VHS)
| released       = February 20, 1950
| runtime        = 86 minutes
| country        = United States English
| budget         =
| gross          =
| awards         =
}}
 Paul Kelly. cardinal from Communist regime. Russian occupation.

==Synopsis==
The film uses the framing device of a speech to members of the Overseas Press Club of America by a journalist who has just returned from Budapest where he witnessed the treason trial of Mindszenty. He also tells the story of Stephanie Varna, a young teacher who decides to stake a moral stand against Communism despite her love for a Russian officer.

==Production== The Prisoner.

==Main cast==
* Charles Bickford as Joszef Cardinal Mindszenty 
* Bonita Granville as Stephanie Varna  Paul Kelly as Tom Kelly  
* Richard Derr as Colonel Aleksandr Melnikov 
* Roland Winters as Commissar Belov 
* Berry Kroeger as Colonel Timar 
* John Banner as Doctor Szandor Deste  
* Alfred Linder as Janos 
* Thomas Browne Henry as Colonel Gabriel Peter
* Nestor Paiva as Matyas Rakosi 
* Morgan Farley as Doctor   Lisa Howard as Soviet Official
* Elisabeth Risdon as Mindszentys mother

==Bibliography==
* Shaw, Tony. Hollywoods Cold War. Edinburgh University Press, 2007.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 