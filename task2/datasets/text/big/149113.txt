Wayne's World 2
{{Infobox film
| name = Waynes World 2
| image = Waynes World 2.jpg
| caption = Waynes World 2 movie poster
| director = Stephen Surjik
| producer = Lorne Michaels Bonnie Turner Terry Turner
| based on =  
| starring = Mike Myers Dana Carvey Christopher Walken Tia Carrere Ralph Brown Aerosmith
| music = Carter Burwell
| cinematography = Francis Kenny Malcolm Campbell NBC Films
| distributor = Paramount Pictures
| released = December 10, 1993
| runtime = 95 minutes
| budget = $40 million
| gross = $48,197,805
| country = USA English
}} sketch on NBCs Saturday Night Live and is the sequel to Waynes World (film)|Waynes World.

==Plot==
Wayne and Garth now do their acclaimed Friday-night TV show in an abandoned doll factory in Aurora, Illinois|Aurora. At the end of the broadcast, Wayne, Garth, and his friends, head into the Mirthmobile, and off to an Aerosmith concert. After the bands performance, Wayne and Garth head backstage (thanks to backstage passes), and praise Aerosmith as they did Alice Cooper in the previous film.
 Jurassic Park). They then find that their early attempts to sign bands and sell tickets fall flat, and Wayne wonders if the whole thing was a good idea.

Meanwhile Waynes girlfriend Cassandra has a new producer, Bobby Cahn (Christopher Walken), who slowly tries to pull her away from Wayne (blocking calls to her in the process) and Illinois.  After Wayne admits that he was spying on her, Cassandra breaks off the relationship and hastily gets engaged to Bobby on the rebound.  Garth meets a beautiful woman, Honey Hornée (Kim Basinger), at the laundromat, and she quickly ropes him in with her charms.  Eventually, it is revealed that Honey is manipulating Garth into killing her ex-husband, and Garth quickly bails on the relationship.

Back in Aurora, tickets for Waynestock are finally selling, but no bands have shown up.  Wrestling with what to do, Wayne departs the festival grounds so he can find Cassandra, leaving Garth to keep the rowdy crowd in check.  In a parody of The Graduate, Wayne travels to a church and breaks up Cassandras wedding before escaping the ceremony with her.  Meanwhile, Garth has stage fright during the concert. Upon returning to Waynestock, the bands still have not shown up. As in the first film, three endings occur.
* Wayne and Garth consult Morrison, who said that no one will come, telling them that all that matters is they tried. They turn around to go back to Waynestock and they lose their way, not knowing how to escape the desert dream sequence, and therefore presumably die of thirst (the sad ending).
* They drive their car to find the bands, but a helicopter corners them, so they drive their car off a cliff (the Thelma & Louise ending).
* The promised bands arrive and the whole event is a huge success (the happy ending).  

During the middle of the end credits, after the concert is over and everyone has left, the weird naked Indian stands crying because the whole park is trashed (in a parody of the 1971 "Keep America Beautiful" "Crying Indian" campaign). Wayne and Garth tell him not to cry, they were planning on cleaning up all the trash.  Wayne and Garth clean up the trash while the weird naked Indian watches.  

===Waynestock=== Adlai Stevenson Shut Up and Dance" at the concert. Additionally, the Gin Blossoms and Nash Kato (of Urge Overkill) can be seen being escorted out of a limousine-length Mirthmobile (a stretched AMC Pacer) by the weird naked Indian as the closing credits begin.

==Cast==
===Main Characters=== Wayne Campbell
* Dana Carvey as Garth Algar
* Tia Carrere as Cassandra Wong
* Christopher Walken as Bobby Cahn
* Kevin Pollak as Jerry Segel
* Ralph Brown as Del Preston
* James Hong as Jeff Wong, Cassandras father and martial arts expert
* Kim Basinger as Honey Hornée Jim Downey as the dubbed voice of Jeff Wong
* Chris Farley as Milton, an aimless friend of Wayne and Garths
* Ed ONeill as Glen
* Michael A. Nickles as Jim Morrison
* Larry Sellers as The Naked Indian
* Frank DiLeo as Frankie Mr. Big Sharp
* Lee Tergesen as Terry, Wayne and Garths bud
* Scott Coffey as a Metalhead

===Notable cameos===

* Drew Barrymore as Bjergen Kjergen
* Olivia dAbo as Betty Jo
* Charlton Heston as the "good actor" gas station attendant who replaces the "bad actor" Al Hansen
* Jay Leno as Himself
* Heather Locklear as Herself
* Ted McGinley as "Mr. Scream"
* Tim Meadows as Sammy Davis, Jr.
* Robert Smigel and Bob Odenkirk as nerds backstage at the concert
* Bobby Slayton as the Watermelon Guy
* Harry Shearer as "Handsome" Dan
* Rip Taylor as Himself
* Steven Tyler as Himself (lead vocalist of Aerosmith) Joe Perry as Himself (lead guitarist of Aerosmith)
* Brad Whitford as Himself (rhythm guitarist of Aerosmith) Tom Hamilton bassist of Aerosmith) drummer of Aerosmith)
* Rich Fulcher as Garths body double when they "travel to London"
* WPIG 95.7 as the Aurora radio station. The call sign, frequency and branding were based on the real life country-music outlet in Olean, New York. A Tyrannosaurus Jurassic Park

==Reception==
The film received mixed to positive reviews.  Rotten Tomatoes gives the film a score of 60% based on 42 reviews.  

Roger Ebert gives the film 3 out of 4 stars, and writes that the characters of Wayne and Garth are "impossible to dislike." 

=== Box office === blockbusters such The Pelican Brief. 

==Soundtrack==
 

==See also==
  
* Waynes World
* Waynes World (film)
* List of Saturday Night Live feature films

==References==

 

==External links==
 
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 