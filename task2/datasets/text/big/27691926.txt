I Will, I Will... for Now
{{Infobox film
| name           = I Will, I Will... for Now
| image          = 
| image_size     = 
| caption        = 
| director       = Norman Panama
| writer         = 
| narrator       = 
| starring       = Elliott Gould Diane Keaton Paul Sorvino
 John Cameron
| cinematography = John A. Alonzo Robert Lawrence
| studio         = 
| distributor    = 20th Century Fox
| released       = November 15, 1976
| runtime        = 107 min.
| country        = United States
| language       = English
| budget         = 
| gross          = $1,740,000 (US/ Canada) 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
I Will, I Will... for Now is a 1976 film directed by Norman Panama. It stars Elliott Gould and Diane Keaton.  

==Plot==
The marriage of Les and Katie Bingham is in big trouble. Theyve already split up once, and now theyre giving it one more try, but the bedroom of their New York apartment is not a happy place. Les finds her too cold. Katie finds him too fast.

The Binghams weigh the opinions of lawyer Lou, who also has a romantic interest in Katie. Theres also temptation for Les in the form of sexy neighbor Jackie, who gives him a copy of "The Joy of Sex" as a gift. But as soon as he tries out one of the positions in it, Les throws out his back.

The couple takes one last desperate try to revive their passion and save their relationship. They travel to California to join a sex-therapy group, where much goes wrong, but all ends well.

==Cast==
*Elliott Gould as Les Bingham
*Diane Keaton as Katie Bingham
*Paul Sorvino as Lou Springer
*Victoria Principal as Jackie Martin
*Robert Alda as Dr. Magnus

==References==
 

==External links==
* 

 

 
 