UFOs: Past, Present, and Future
 
 
{{Infobox film
| name           = UFOs: Past, Present, and Future
| image          =
| image_size     =
| alt            =
| caption        = 
| director       = Ray Rivas
| producer       = Allan Sandler Carol Johnsen James Castle  Bruce Bryant
| writer         = Robert Emenegger Carol Johnsen
| starring       = Rod Serling Burgess Meredith José Ferrer
| music          = Robert Emenegger
| cinematography = Stan Lazan Donald Peterman
| editing        = Ken Lavet Jack Schrader Caryl Wickman
| studio         = Sandler Institutional Films
| distributor    = VCI Video Communications, Inc
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
UFOs: Past, Present, and Future is a 1974  s Close Encounters of the Third Kind.    It is based on the book UFOs: Past, Present, and Future by Robert Emenegger.   

==Overview==
The film is narrated by Rod Serling, Burgess Meredith, and José Ferrer. Serling and Meredith had previously worked together on The Twilight Zone.    The 1979 re-release features commentary by noted UFOlogist and astronomer Dr. Jacques Vallée.  The film uses dramatizations, interviews with government officials and scientists, and selected footage to provide context for UFO sightings, both ancient and contemporary.

"What you are witnessing is based on fact. Some will find it fascinating, some will find it frightening: but it is all true."

==Production==
In 1971, writer/composer Robert Emenegger was asked by either the U.S. Republican Party, officials at California’s Norton Air Force Base, or the Department of Defense itself to produce a film about UFOS using only official DoD and NASA source material,    and was allegedly promised footage of a 1964 landing at Holloman Air Force Base. Only a few seconds of this special footage ultimately made it into the film.   

Ray Rivas was given "unprecedented" access to DoD facilities,    with the director saying "Secretary of the Air Force Robert Seamans gave the order to co-operate."

==Synopsis==
The film opens with Serling asking open-ended philosophical questions about the origin of humanity on Earth, juxtaposing evolution and religion with a variation of the ancient astronaut hypothesis. Serling steps in front of the camera, similar to his routine on The Twilight Zone, and suggests that just as humans look at the sky and question where we came from, so too might extraterrestrial beings.

The storys first vignette takes place outside  . As they begin to check the engine, a bright white disk-shaped object approaches and shines a bluish beam directly at them. After a moment, it flies away. Once the object disappears, their truck starts right up, and the men leave to notify the police. Later, an Air Force investigation analyzes their story along with fifteen similar reports, and concludes that ball lightning phenomena is responsible. Serling warns that this may not be the only explanation.

The next section, detailing historical records, opens with Meredith reading from the  . Starting in the late 1890s, sightings of a "flying cigar" were reported across the U.S., including Oakland, CA, and Denver, CO.

A young Dr. Jacques Vallée then attempts to dispel the notion that UFO sightings were limited to the United States. He describes a wave of sightings that occurred in the late 1940s and 1950s in Scandinavia, Mexico, the Soviet Union, China, and the western coast of Africa. In 1973, a team led by a Jean Balgou of the Astrophysical Institute flew in a Concorde jet packed with scientists. At high altitude in the midst of a total eclipse, he uses a stop motion camera to photograph the sun rising over the horizon, and captured a suspicious object. While NASA takes no official position on the existence of UFOS, several astronauts on the Gemini, Apollo and Skylab missions have come forward with their own unexplainable observations and photographs.

Analyzed next are reports from former military officials and scientists. According to testimony from former U.S Air Force officials, the Department of Defense first became interested in the UFO phenomenon in the late 1940s, concerned that it may constitute a military threat from a foreign power. The Air Force began a formal investigation under Project Sign which after only two weeks was complicated by the death of airman Capt. Mantell, an experienced pilot who crashed under suspicious circumstances outside Louisville, KY. Public pressure was growing for answers, but the results of the investigation were classified and later squashed by USAF General Hoyt Vandenberg due to insufficient evidence. Several UFOs recorded by radar over the US capital reportedly were important enough for President Truman to request being personally informed on all developments in the case. An F-94 intercept over DC caused all unknown radar signs to disappear once the fighters entered the city. As the fighters left DC airspace, the unknown objects reappeared. The confusion resulted in "the largest, and longest, press conference in the Pentagon since World War 2." The radar issues were blamed on temperature inversion.

In January 14, 1953, the Central Intelligence Agency became involved, convening a panel of top scientists to further explore the phenomenon and its potential threat to national security. Their analysis concluded that the objects involved in several high-profile sightings were not aircraft, balloons, birds, "but that they were self-luminous, unidentified objects." Yet the panel found that they were indeed birds - being unidentified, they could be nothing else.

In 1966, an incident outside Ann Arbor pushes U.S. Representative Gerald Ford to set up a congressional hearing on UFOS, and it is opened by L. Mendel Rivers at the House Armed Services Committee.

Several unexplained animal mutilations of livestock are examined. While the official explanation is usually predation, interviews with ranchers suggest that they were not the result of predatory animals. The incisions in the animals do not conform to those made by steel tools, seemingly ruling out human behavior.

The films conclusion begins with a brief overview of possible aircraft shapes, and a number of illustrations of extraterrestrials featured from Emeneggers book. A roundtable discussion featuring several of the films scientists revolves around the hubris of mankind in believing we are the only sentient life in the universe.

Finally, a dramatized scenario of what may have occurred at Holloman Air Force Base is told by Serling. Three unidentified objects are detected approaching Holloman. Base Command contacts Edwards AFB, and try to attempt contact with the objects without success. A red alert is sounded, and fighters take off. A helicopter with a professional photographer aboard was in the air, and shot several feet of film. One of the crafts break away, and seems to attempt a landing. A crew on the ground runs off several hundred more feet of film. The vehicle hovers silently perhaps ten feet off the ground before landing on three extension pads. Several Air Force officials and scientists at the base await outside as the crafts panel opens:

"Stepping forward, are one, then two, and a third of what appear to be men, dressed in tight-fitting jumpsuits. Perhaps short, by our standards. With an odd blue-grey complexion, eyes set far apart, a large pronounced nose, they wear a headpiece that resembles a rope-like design. The commander and two scientists step forward to greet the visitors. Arrangements are made by some sort of communication, and the group quickly retires to an inner office in the King 1 area. Left behind stand a stunned group of military personnel. Who the visitors are, and where theyre from, and what they want, is unknown."

==Cast==
* Rod Serling - Narrator
* Burgess Meredith - Narrator
* Dr. Jacques Vallee - Narrator
* Dr. J. Allen Hynek - Interviewee
* Colonel Robert Friend USAF - Interviewee
* Colonel William Coleman, USAF former Project Blue Book Spokesman - Interviewee
* Colonel George Weinbrenner, former head of Foreign Technology at Wright Patterson AFB - Interviewee

==Reception==
In 1976, Miami News reporter Marilyn Moore called it "a fine presentation on the topic, narrated by the late Rod Serling which serves as a sad reminder of the many excellent shows he gave us over the years."   

==Awards and nominations==
The film was nominated for Best Documentary Film at the 33rd Golden Globe Awards. It lost to Youthquake!.      

==References==
 

==External links==
*   
*  

 
 
 
 

 