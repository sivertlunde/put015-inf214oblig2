Felon (film)
{{Infobox Film
| name           = Felon
| image          = Felonposter08.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Ric Roman Waugh
| producer       = Dan Keston Nick Phillips Christopher Wilhem
| writer         = Ric Roman Waugh
| starring       = Stephen Dorff Harold Perrineau Marisol Nichols Anne Archer Sam Shepard Val Kilmer
| music          = Gerhard Daum
| cinematography = Dana Gonzales
| editing        = Jonathan Chibnall
| studio         = Stage 6 Films
| distributor    = Sony Pictures Home Entertainment
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = $2,900,000 (estimated)
| gross          = $36,756 
}} state prison after he kills an intruder. The story is based on events that took place in the 1990s at the notorious California State Prison, Corcoran.  The film was released in the United States on July 18, 2008.

==Plot==
Family man Wade Porter (Stephen Dorff) is living the American Dream with his girlfriend Laura (Marisol Nichols) and their son Michael: they have a nice house, Wade has just raised a loan to make his company grow and they are going to get married. However, their dream becomes a nightmare when Wade unintentionally kills a burglar on his lawn that had broken into their house in the middle of the night.
By attacking the fleeing unarmed intruder outside of the house, he is arrested for murder, and during the first night in the county jail, Wade gets into a brawl after being attacked by an inmate in the community cell. 
With $1,000,000 bail he cant raise, after 90 days, the public defender is able to negotiate a deal for Wade to serve three years for involuntary manslaughter, or he could spend more time awaiting trial. Sentenced to Corcoran State Prison, during the bus ride over, Danny Sampson (Chris Browning), leader of the local Aryan Brotherhood, stabs a man and hides the knife with a young Aryan member named Snowman (Johnny Lewis) sitting behind Wade. In a moments panic, Snowman hides the knife under Wades seat and forces him to deny knowledge of it. As a result, Wade is sent to solitary confinement until the mess can be sorted out. Upon further investigation by Lieutenant Jackson (Harold Perrineau), Jackson is convinced Wade is an accessory to murder because he denies knowledge of the knife, and decides to send Wade to the Security Housing Unit (SHU), of which Jackson is the commanding officer.

Elsewhere, John Smith (Val Kilmer), an inmate serving a life sentence in San Quentin prison for killing the entire families of two men who brutally raped and murdered his wife and daughter, is shown in solitary confinement for starting a riot among the prison population for what appears to be his own pleasure. During a visit with his friend Gordon (Sam Shepard), a former prison guard who John had saved from being killed, Gordon reveals John has been given one final chance before being stuck in solitary confinement for life. Gordon requests that John be transferred, and subsequently, John is sent to the SHU as well, and becomes Wades new cellmate. 

Life is tough in the SHU. In addition to 23-hour lockdown days, and the inability to see his family at first, the daily hour of yard time consist of inmates fighting, on which the guards bet. Scared and desperate, Wade initially groups with the Aryans, even going so far as to shave his head. At first upon the request of Snowman and his cronies, Wade cooperates by participating in a majority of the fights. When Wade decides at one point thats hes fed up with being the one to do so and confronts Snowman, hes attacked by the group, and narrowly escapes with the help of John. As a result, both John and Wade are now green lit by the Aryans, soon after, Sampson meets with Wade, John and the rest of the Aryans. Revealing he knew Snowman hid the knife under the bus on the way to the prison, and has him beaten and stabbed. Sampson also informs Wade that he is now without the Aryans protection unless he joins, but assures him none of his men will come after him.

At different points throughout the film, its shown that not all of the guards are in favor of Jacksons methods. Officer Diaz (Greg Serano) complies fully, and appears as sadistic as Jackson, but Sgt. Roberts (Nick Chinlund) does not want to get caught and lose his pension, and newly hired Officer Collins (Nate Parker) clearly has a higher moral standard than his boss. Due to Jacksons influence, however, both remain quiet and cooperative on the operation. 
In addition to the prison violence, Wades regular visits with Laura start to take their toll on their relationship. Michael has nightmares after one of his visits, and the familys finances are running low, even after selling the house and the truck, and using up virtually all of Wades business loan. Things come to a head when Snowman, having been released from the infirmary, agrees with Lt. Jackson to testify against Wade and Sampson concerning the bus murder, and be placed in protective custody. Wade and Sampson are subsequently put on trial and sentenced for an additional six years and life, respectively. Laura, at the encouragement of her mother, breaks up with Wade through a letter. Enraged, and seemingly having nothing to live for, Wade breaks down and resorts to fighting the prisoners. 

One night, Jackson is called to a hospital after his son was run over by a drunk driver, and due to this being his third DUI offense, he is being guarded by police. Jackson visits the mans room, and promises he will spread word around California prisons that the man is a pedophile, guaranteeing that he will be beaten and tortured mercilessly no matter where he does his time.  
Laura visits Wade again and reveals neither she nor their son want to live without him, and she will wait until he gets out. After talking to John, Wade devises a plan that can reveal the truth about the violence in the prison, and get him released. Laura goes to meet Johns friend Gordon, and together they persuade an FBI agent to accompany them to the prison. Wade asks Jackson, meanwhile, to schedule a fight with the black inmate who first assaulted Wade in the jail. Lt. Jackson, however, declares that this match be to the death. The morning of the fight, Laura, Gordon and an FBI agent arrive to meet with the warden of the prison. Wade, meanwhile, enters the yard and fights, which ends with him slamming the inmates head into the ground repeatedly. Jackson orders Wade to kill the inmate, and when he does not comply, Jackson threatens to shoot Wade. Suddenly, John, Sampson, and the rest of the prison population stand in front of Wade to protect him, having grown tired of the Jacksons sadistic actions. Jackson orders everyone out of the yard and turns the surveillance cameras off.
Wade and Smith are ordered back in the yard, and forced to kneel with Jackson holding a knife, while Officer Diaz aims his gun from the control room. Jackson tells them since they are convicts, no one will care what they think, and because the cameras are off, Jackson can murder them both and then claim John murdered Wade, and the guards had to kill John in retaliation.

John pulls out a knife he concealed and slashes Jacksons leg and throat, but is killed immediately after by gunshots. Suddenly Officer Collins, having secretly turned the cameras on in the control room, sounds the alarm, alerting the rest of the prison.
Hearing the sirens, Laura, Gordon, and the FBI agent discover what has happened in the yard from the warden. The footage is reviewed, and Wade is placed in protective custody, his sentence reduced back to the original three years (which will end in less than three months). Wade is visited by Gordon, who passes on Lauras message about setting up a new wedding date, and leaves him with a letter John wrote to Wade before he died.
Before departing, Gordon reveals that John never told him of anything that happened in the SHU, and even though John saved him from getting killed, Gordon kept visiting because John was one of his true friends, to which Wade replies "Mine too." Wade then says that John died the way he wanted, and that the prison never broke him, to which Gordan agrees.  
In a voice over, John reads the contents of his letter to Wade, urging Wade to protect his family and do anything for them, even if it means having to kill again. Snowman is then shown being brought back to the SHU from protective custody, and is put in a cell with Sampson. The last shot shows Wade walking out of the prison a free man, and his wife and child run to embrace him as Johns voice concludes the letter with "So long, friend."

==Cast==
* Stephen Dorff as Wade Porter
* Marisol Nichols as Laura Porter
* Vincent Miller as Michael Porter
* Val Kilmer as John Smith
* Harold Perrineau as Lt. Jackson
* Greg Serano as Officer Diaz
* Nate Parker as Officer Collins
* Nick Chinlund as Sgt. Roberts
* Anne Archer as Maggie
* Sam Shepard as Gordon
* Chris Browning as Danny Sampson
* Jake Walker as Warden Harris

==Production==
It is set and filmed in Santa Fe, New Mexico in 24 days on October 9 and November 2, 2007.

==Reception==
The film received mainly positive to mixed reviews from critics. As of January 10, 2012, Felon holds a score of 7.6 out of ten on the Internet Movie Database (IMDb) based upon 34,497 votes.  As of January 26, 2009, the review aggregator Rotten Tomatoes reported that 62% of critics gave the film positive reviews, based on 21 reviews.  Metacritic reported the film had an average score of 58 out of 100, based on 10 reviews &mdash; indicating good or above average reviews.  The film had a limited screening in the US.

==Home media== Region 1 in the United States on August 12, 2008, and also Region 2 in the United Kingdom on October 6, 2008, it was distributed by Sony Pictures Home Entertainment.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 