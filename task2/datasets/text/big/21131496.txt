La Virgen gitana
{{Infobox film
| name           = La Virgen gitana
| image          = 
| caption        = 
| director       = Ramón Torrado
| producer       = Saturnino Huguet Daniel Mangrané
| writer         = Francisco Naranjo Ramón Torrado
| starring       = Paquita Rico
| music          = 
| cinematography = Manuel Berenguer
| editing        = Antonio Cánovas
| distributor    = 
| released       = 24 March 1951
| runtime        = 92 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

La Virgen gitana is a 1951 Spanish comedy film directed by Ramón Torrado. It was entered into the 1951 Cannes Film Festival.   

==Cast==
* Paquita Rico - Carmelilla
* Alfredo Mayo - Eduardo Miranda
* Lina Yegros - Cristina Álvarez de Miranda
* Lola Ramos - Reyes
* Alfonso Estela - Vicente
* Félix Fernández (actor)|Félix Fernández - Miguel
* Modesto Cid - Agustín
* María Severini
* Rosa Fontsere (as Rosa Fontseré)
* Camino Delgado
* Pedro Mascaró
* Ana Morera
* Fortunato García - Juez

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 