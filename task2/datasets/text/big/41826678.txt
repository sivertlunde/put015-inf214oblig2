Chaitrada Premanjali
{{Infobox film
| name = Chaitrada Premanjali
| image = 
| caption =
| director = S. Narayan
| writer = S. Narayan Raghuveer  Shwetha   Lokesh   Abhijeeth
| producer = M. Shashikala
| music = Hamsalekha
| cinematography = P. K. H. Das
| editing = Suresh Urs
| studio = M. C. Productions
| released = 1992
| runtime = 154 minutes
| language = Kannada
| country = India
| budget =
}} drama film Raghuveer and Shwetha in the lead roles along with Lokesh and Abhijeeth in other pivotal roles.  The film met with positive reviews upon release from critics and audience eventually becoming the musical hit of the year.

The film featured original score and soundtrack composed and written by Hamsalekha. The soundtrack proved to be one of the biggest hit albums of the year.

== Cast == Raghuveer as Prem
* Shwetha as Anju
* Abhijeeth 
* Lokesh
* Srinivasa Murthy
* Rajanand
* Jyothi
* Ashalatha
* Sathyajith
* S. Narayan

== Soundtrack ==
The music was composed and written by Hamsalekha for Lahari Music audio company. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = O Kogile
| extra1 = S. P. Balasubrahmanyam
| lyrics1 = Hamsalekha
| length1 = 
| title2 = O Malenadina 
| extra2 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics2 = Hamsalekha
| length2 = 
| title3 = Dance Dance
| extra3 = S. P. Balasubrahmanyam
| lyrics3 = Hamsalekha
| length3 = 
| title4 = Nannavare Nanage
| extra4 = S. P. Balasubrahmanyam, Manjula Gururaj
| lyrics4 = Hamsalekha
| length4 = 
| title5 = Daari Bidu
| extra5 = Manjula Gururaj
| lyrics5 = Hamsalekha
| length5 = 
| title6 = Chaitrada Premanjaliya
| extra6 = S. P. Balasubrahmanyam
| lyrics6 = Hamsalekha
| length6 = 
}}

== References ==
 

== External links ==
*  
*  
 
 
 
 
 
 
 
 
 


 