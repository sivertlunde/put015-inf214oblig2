A-Haunting We Will Go (1942 film)
{{Infobox film name           = A-Haunting We Will Go image          = L&H_A-Haunting_We_Will_Go_1942.jpeg caption        = Theatrical release poster director       = Alfred L. Werker producer       = Sol M. Wurtzel writer         = Lou Breslow Stanley Rauh starring  Dante the John Shelton music          = David Buttolph Cyril J. Mockridge cinematography = Glen MacWilliams editing  Alfred Day distributor    = 20th Century Fox released       = August 7, 1942 runtime        = 66 40" language  English
|country        = United States
}}

A-Haunting We Will Go is a 1942 Laurel and Hardy feature film released by 20th Century Fox and directed by Alfred L. Werker. The story is credited to Lou Breslow and Stanley Rauh.    The title is a play on the song "A-Hunting We Will Go".

==Plot==

Stan Laurel and Oliver Hardy are two hobos roaming the Arizona countryside. After being arrested for loitering, they spend a night in jail in town. When they are released the day after they are ordered to leave town immediately. Since the men lack every kind of transportation, they come up with the desperate idea of traveling as escort to a coffin and an undertaker’s railroad transport of a corpse out of town to Dayton, Ohio. The corpse will still be in the coffin, of course, but they at least get the transport for free. 

Stan and Oliver are happily unaware that the men who has hired them, Frank Lucas and Joe Morgan, are gangsters, working as henchmen for their boss Darby Mason, who is wanted by the law. Mason’s real name is Norton, but this name is known to the police. Mason has seen in the newspapers that a search for an heir to a very large fortune has started in Dayton. The heir’s name is Egbert Norton, and Mason plans to sneak unseen into Dayton and pretend to be the heir once he is inside the city limits.  Mason has planned to hide at a sanatorium, run by the dubious doctor Lake, until he can re-surface and collect the inheritance from the attorney, Malcolm Kilgore.
 Dante the magician, a stage magician, who transports his stage trunk on the train. Once aboard the train, Stan and Oliver also manage to be tricked by two con-men, Phillips and Parker. For their last dollars they buy a fake money-making machine from the con-men. They are so poor they can’t even pay for dinner on the train. Dante the Magician picks up their bill for them, and they promise to repay the artist once they get to Dayton, where the magician is to perform on stage.  

Arriving in Dayton, the trunk with Mason in it is sent to the theater where Dante will perform, while the magician’s trunk is sent to the sanatorium, where Lake awaits its arrival. Lake opens the trunk and realizes that there has been an accidental switch of trunks. He contacts the attorney, Kilgore, telling him that Norton isn’t available for an interview that day. 

Stan and Ollie go to the theater and pay back what they owe to Dante. They are so silly and Dante finds them very amusing. He hires them on the spot, as assistants and comic relief added to his magic act that very night.  

Lake finds out that the trunk with Mason has been sent to the theater, and goes there to collect it. Lake is tailed by henchman Morgan and another guy named Dixie Beeler. They suspect Lake of trying to double-cross them in some way. 

Kilgore goes to the sanatorium to talk to Lake about his patient Norton, and finds the magician’s trunk, full of handbills. Confused by this, he also goes to the theater to see if he can find out what is going on. He arrives to the theater just as the show begins. 

Stan and Ollie has been given ordersfrom Morgan to find Mason, who they have lost. They run around the theater as best they can in search of the right trunk. As the show begins they discover the trunk involved in the first trick, standing directly in front of the audience, where they can’t reach it without making a commotion. Dante performs his trick, and discovers Lake’s dead body in the trunk when he opens it. 

Stan and Ollie are separated from each other in the theater. Ollie starts searching for Stan. A police officer, Lt. Foster, arrests the stage manager Tommy White and Dante for the suspicion of murdering Lake. Ollie finds attorney Kilgore knocked out on the floor of the theater. Mason knocked him out when he was suspiciously wandering around the theater.  Ollie explains to Kilgore how the three men he works for are looking for the coffin, and that he told them to exit the theater unnoticed. Tommy finds out about the mishap and explains that the exit Ollie showed the three men leads to a trap door into the lion cage in the basement. They realize that the gangsters have fallen into the lion cage. They rush down to the basement and find Mason and his men trying to get escape the lions. 

Kilgore reveals that he is in fact a federal investigator by the name of Steve Barnes, and that the inheritance was only a sugar trap to flush out Mason from his hiding and catch him.  Trapped in the lion’s cage, Mason confesses to killing Lake and putting him in the trunk. The gangsters are all arrested. Ollie continues his search for Stan. He finally finds him inside what he believes is a giant egg. He then realizes that he has been shrunk by magic. 

==Cast==
 
 
*Stan Laurel - Stanley
*Oliver Hardy - Ollie
*Harry August Jansen - Dante The Magician
*Sheila Ryan - Margo John Shelton - Tommy White
*Don Costello - Doc Lake
*Elisha Cook Jr. - Frank Lucas
*Edward Gargan - Police Lt. Foster
*Addison Richards - Attorney Malcolm Kilgore George Lynn - Darby Mason James Bush - Joe Morgan
*Lou Lubin - Dixie Beeler
*Robert Emmett Keane - Parker Richard Lane - Phillips
*Willie Best - Waiter Harry Blackstone - Magician
*Wade Boteler - Police Announcer
 
*Buz Buckley - young admirer Barry Downing - young admirer
*Mary Lou Harrington - young admirer Terry Moore - young admirer
*Leon Taylor - young admirer
*Roland Carpenter - Dantes stage assistant
*Violet Church - Dantes stage assistant
*Mildred Gaye - Dantes stage assistant
*Les Clark - Dantes stage assistant Harry Carter - bit role
*Edgar Dearing - officer
*Tom Dugan - motorist
*Ralph Dunn - officer
*Frank Faylen - railroad detective
*Wilbur Mack - railroad passenger
*Mantan Moreland - railroad porter
*Eddy Waller - Wilcox, baggage man
*Bud Geary - electrician
*Paul Kruger - stage hand
*Margaret Roach - showgirl
*Walter Sande - expressman
 

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 