The Woman of the Iron Bracelets
{{Infobox film
| name           = The Woman of the Iron Bracelets
| image          =
| caption        =
| director       = Sidney Morgan 
| producer       = Frank E. Spring 
| writer         = Frank Barrett  (novel)   Sidney Morgan Eve Balfour George Bellamy
| music          = 
| cinematography = 
| editing        = 
| studio         = Progress Films
| distributor    = Butchers Film Service
| released       = September 1920  
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent crime film directed by Sidney Morgan and starring  Eve Balfour (actress)| Eve Balfour, George Keene and Marguerite Blanche. A fleeing young woman facing a murder charge, goes to the assistance of a young man who is being cheated out of his inheritance by his stepfather. The iron bracelets of the title refer to her handcuffs.

==Cast== Eve Balfour as Noah Berwell  
* George Keene as Harry St. John  
* Marguerite Blanche as Olive St. John   George Bellamy as Dr. Harvey 
* Arthur Walcott as Mr. Lawson 
* Alice De Winton as Mrs. Lawson

==References==
 

==Bibliography==
* Low, Rachael. The History of the British Film 1918-1929. George Allen & Unwin, 1971.

==External links==
*  

 

 
 
 
 
 
 
 
 
 

 