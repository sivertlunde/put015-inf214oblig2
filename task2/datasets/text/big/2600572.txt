Three Men and a Little Lady
{{Infobox film
| name           = Three Men and a Little Lady
| image          = three men and a little lady.jpg
| caption        = Theatrical release poster
| director       = Emile Ardolino
| producer       = Ted Field Robert W. Cort
| screenplay     = Charlie Peters
| story          = Sara Parriott Josann McGibbon
| starring       = Tom Selleck Steve Guttenberg Ted Danson Nancy Travis Christopher Cazenove Robin Weisman Fiona Shaw
| music          = James Newton Howard Adam Greenberg
| editing        = Michael A. Stevenson
| studio         = Touchstone Pictures Interscope Communications Buena Vista Pictures
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $71,609,321
}}
Three Men and a Little Lady is a 1990 American comedy film, and the sequel to the 1987 film Three Men and a Baby. Tom Selleck, Steve Guttenberg, and Ted Danson reprise the leading roles.

==Plot== Englishman and that they intend to move to England after the wedding, taking Mary with them.

Peter and Michael (joined later by Jack) travel to England, where Peter realizes that Sylvias fiancé, Edward, intends to pack Mary off to a boarding school (Pileforth Academy) as he has no real interest in her. He denies everything and Sylvia refuses to believe Peter, knowing he has disliked Edward from the beginning.

An attraction between Peter and Sylvia is nevertheless growing, something he refuses to acknowledge. He breaks into Pileforth in an attempt to get proof of Edwards scheme to send Mary there. He is discovered by the headmistress, Miss Elspeth Lomax, who says she has been told by Edward that Peter is in love with her. She begins to take her clothes off and tries to have sex with him, but he manages to get away and later explains to her that Edward made up the fact that he had feelings for her, but apologizes for the misunderstanding.

Peter, with help from Miss Lomax, heads off to stop the wedding. To cause a delay, Michael has kidnapped the vicar and Jack disguises himself as an elderly replacement one. 

Peter and Miss Lomax arrive at the church after numerous delays. He confronts Sylvia with the truth, Miss Lomax herself confirming that Edward has been lying. Sylvia confronts him and he admits the truth but it is too late--they are already married. Or so it would seems...until Jack reveals himself to everyone. Not only has he finally proved his acting skills, but the marriage is null and void.

Sylvia declares her intention to go home, but Peter stops her and declares his love. They wed with Mary as their bridesmaid.

==Cast==
* Tom Selleck as Peter Mitchell
* Steve Guttenberg as Michael Kellam
* Ted Danson as Jack Holden
* Nancy Travis as Sylvia Bennington-Mitchell
* Christopher Cazenove as Edward Hargreave
* Fiona Shaw as Miss Elspeth Lomax
* Robin Weisman as Mary Bennington
* John Boswall as Barrow, Edwards butler
* Sheila Hancock as Vera Bennington

==Production==
Filmed on location in New York and the United Kingdom, the scenes in the latter location were primarily shot in Banbury in north Oxfordshire. Particular use is made of Broughton Castle. The scenes where the car broke down and Peter makes a call from a phone box are shot at Burton Dassett Country Park, in south Warwickshire. The school which Mary was to attend (Pileforth Academy) was shot at two locations. The external shot of the school is the Jesuit boarding school Stonyhurst College in the Ribble Valley, Lancashire. The internal scenes of the school were shot at the (former) Benedictine boarding school Douai School near Thatcham, West Berkshire.

==Reception==

Critical reaction to the film was mostly mixed to negative. 

Critic Paul Brenner of Rotten Tomatoes.com cited the film as "treacly," whilst many others on the site have stated that a sequel was not necessary.   

Andy Webb of The Movie Scene.com cited enjoyable parts of the movie, viewing the unusual family life of having three fathers as a main entertainment factor.  Critics Rita Kempley and Desson Howe of the Washington Post also spoke positively of the film, citing the three main characters comical rap, the race for Peter to stop the wedding, and the relationship between him and Miss Lomax as the movies most enjoyable scenes. However, Howe also criticized it, claiming Ted Danson and Steve Guttenburg were overshadowed for the remainder of the film.  

NeedACoffee.com has criticized the DVD for not having any special features or so much as a trailer.  Although Emile Ardolinos death in 1993 prevented him from providing any form of directors commentary, with no commentary provided by the actors or the crew, the site stated that overall, purchasing the DVD if one already owned a VHS copy of the movie was unnecessary.

The film currently holds a rating of 29% based on 14 reviews on Rotten Tomatoes.com,  and holds a rating of 5.1 stars out of ten on IMDB.com based on 13 critic reviews. 

==Box office==
The film grossed United States dollar|USD$72 million. 

==Soundtrack== Boy Meets Girl, which featured during the final wedding scene and end credits.

==Sequel==
In June 2010 it was announced that Disney was interested in filming a third film, tentatively titled Three Men and a Bride.  As of 2013, the film is still listed as being in development. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 