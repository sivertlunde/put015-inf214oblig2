Soggade Chinni Nayana
{{Infobox film name           = Soggade Chinni Nayana image          =  caption        =  director       = Kalyan Krishna screenplay     = Ram Mohan P producer  Nagarjuna Ram Mohan P starring       = Nagarjuna Ramya Krishnan Lavanya Tripathi music          = Anup Rubens  cinematography = P. S. Vinod editing        =  studio         = Annapurna Studios Sunshine Cinemas Pvt Ltd distributor    = released       = country        = India language       = Telugu runtime        = budget         =  gross          =
}}
 Nagarjuna and Ram Mohan P. The film features Nagarjuna in a dual role while Ramya Krishnan and Lavanya Tripathi would be seen as the female leads. Production began on 19 November 2014.

==Cast== Nagarjuna as Bangaru Raju and Grandson
* Ramya Krishnan 
* Lavanya Tripathi
* Brahmanandam
* Hamsa Nandini
* Anasuya
* Vennela Kishore
* Chalapathi Rao

== Production ==

=== Development === Nagarjuna announced his next film in late August 2014 which would be directed by debutante Kalyan Krishna and produced by himself and Ram Mohan P under the banner of Annapurna Studios.  The films title was confirmed as Soggade Chinni Nayana, a famous song from Dasara Bullodu starring Akkineni Nageswara Rao. 

=== Casting === panche kattu for one of the characters.  He reportedly wanted to cast Pranitha Subhash as the other female lead but things did not work out.  Sonal Chauhan was considered for the other female lead.  The makers had a change of mind and finalized Lavanya Tripathi as the second female lead.  It was known later that Nagarjuna will be seen as a grandfather and a grandson in the film. 

Lavanya said that she would play a traditional Indian girl paired opposite the grandson character and added that her role in the film has a romantic touch.  Brahmanandam was confirmed as a part of the films cast in mid December 2014.    Hamsa Nandini was selected for a crucial supporting role and was also confirmed to perform a special song along with Nagarjuna.  Anchor Anasuya was selected to play the role of Nagarjunas cousin in the film and she was expected to join the films sets in February 2015.  This role was initially offered to Swati Reddy who backed out citing lack of dates. 

=== Filming ===
The film was announced to be shot in and around Rajahmundry in late September 2014.  The film was formally launched on the morning on 19 November 2014 at Annapurna Studios in Hyderabad.  Few hours after the launch, the films principal photography began at a set in the same studio.  Few key scenes on Nagarjuna and Brahmanandam were shot at Ramoji Film City in mid December 2014.  After completing a couple of shooting schedules, a fresh one began on 11 February 2015.  Filming continued at Mysore from 10 March 2015 after completing most of the filming in Rajahmundry.  Few scenes were shot at a 1500 year old Vishnu temple in Mysore. 

== References ==

 

 
 
 
 
 
 