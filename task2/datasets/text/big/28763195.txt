A Shot in the Dark (1933 film)
{{Infobox film
| name           = A Shot in the Dark
| image          = "A_Shot_in_the_Dark"_(1933_film).jpg
| caption        =  George Pearson
| producer       = Julius Hagen
| writer         = Gerard Fairlie Terence Egan
| based on       = novel by H. Fowler Mear
| screenplay     = 
| story          = 
| starring       = Dorothy Boyd O.B. Clarence Jack Hawkins Michael Shepley
| music          =  Ernest Palmer
| editing        = Lister Laurance
| studio         = Twickenham Studios
| distributor    = Radio Pictures  (UK)
| released       =  
| runtime        = 53 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 George Pearson and starring Dorothy Boyd, O. B. Clarence, Jack Hawkins and Michael Shepley.  When a wealthy old man dies suddenly, a local reverend suspects something and begins to investigate.

==Cast==
* Dorothy Boyd as Alaris Browne 
* O. B. Clarence as Reverend John Makehan 
* Jack Hawkins as Norman Paull 
* Michael Shepley as Vivien Waugh 
* Davy Burnaby as Colonel Michael Browne 
* A. Bromley Davenport as Peter Browne 
* Russell Thorndike as Doctor Stuart 
* Hugh E. Wright as George Yarrow 
* Henrietta Watson as Angela Browne 
* Margaret Yarde as Kate Browne 

==Critical reception==
Britmovie noted a "typical multi-suspect “quota quickie”" ;  and Classic Horror online wrote, "we nominate SHOT IN THE DARK as the worst film ever made! But this is not to detract anything from its entertainment value. Films such as these were produced on both a limited budget and a limited time scale. Taking this into consideration, these films are little marvels for what they could achieve, and earn themselves a position in the annals of film history. Many famous actors made their first film appearances in these pictures, but now that many of them are lost to us forever, the recognition of the remaining few becomes a neccessity."  

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 