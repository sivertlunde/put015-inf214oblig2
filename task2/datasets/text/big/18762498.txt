The Case of the Frightened Lady (film)
 
 
{{Infobox film
| name           = The Case of the Frightened Lady
| image_size     =
| image	=	"The_Case_of_the_Frightened_Lady"_(1940).jpg
| caption        = Danish poster George King
| producer       =  S.W. Smith Robert Stevenson	(uncredited)
| based on       = play by Edgar Wallace
| narrator       =
| starring       = Marius Goring Helen Haye Penelope Dudley Ward
| music          = Jack Beaver
| cinematography =  Hone Glendinning Leslie Norman
| studio         = George King Productions (as Pennant Pictures)
| distributor    = British Lion Film Corporation  (UK)
| released       = 28 September 1940  (UK)
| runtime        = 81 minutes
| country        = UK
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} mystery thriller directed by George King produced by play by Edgar Wallace. 
 first production in 1932 was directed by T. Hayes Hunter and starred Emlyn Williams.  The BBC also produced two television versions; the first in 1938 and the second in 1983 which starred Warren Clarke and Virginia McKenna.   

In 2008, the film was released on DVD by Odeon Entertainment as part of their Best of British collection. Prior to this release, the film had not been seen in public since its original release.  

==Plot== thriller that revolves around the Lebanon family who live at Mark’s Priory. Lady Lebanon (Helen Haye) tells her son, Lord William Lebanon (Marius Goring) that he must marry his cousin Isla Crane (Penelope Dudley Ward) to continue the family line. However, William has no intention of marrying Isla and matters are made more complicated due to Isla falling in love with architect, Richard Ferraby (Patrick Barr), who has come to Mark’s Priory to draw up renovation plans. At the same time, the strange behavior of two footmen and the family physician (Felix Aylmer) add to the mystery surrounding the family and eventually rumor and speculation lead to a murderous conclusion.

==Cast==
*Lord Willie Lebanon - 	Marius Goring
*Isla Crane - 	Penelope Dudley Ward
*Dowager Lady Lebanon - 	Helen Haye
*Dr Lester Charles Amersham - 	Felix Aylmer George Merritt
*Sergeant Totty - 	Ronald Shiner
*Richard Ferraby - 	Patrick Barr
*Gilder - 	Roy Emerton
*Brooks - 	George Hayes
*Studd, The Chauffeur - 	John Warwick
*Jim Tilling, The Gamekeeper - 	Torin Thatcher
*Mrs Tilling - 	Elizabeth Scott
*Vicar - 	Roddy Hughes

==Critical reception==
The New York Times wrote, "the sort of thing Edgar Wallace could make intriguing on paper—or, on the stage, as he did in telling of the horrendous doings at Marks Priory in Criminal at Large about ten years ago. But the old shocker has lost most of its punch...There are several reasons why Frightened Lady doesnt come off as it should. One is that Director George King has not evidenced any regard for suspense, the other is that the performances, on the whole, are uninspired. But perhaps the real reason is that the story itself is outmoded for cinematic treatment" ; {{cite web|url=http://www.nytimes.com/movie/review?res=9800E2D71E3FE13BBC4852DFB767838A659EDE|title=Movie Review -
  The Case of the Frightened Lady - At the Globe - NYTimes.com|work=nytimes.com}}  while Britmovie called it "a tightly written murder mystery...probably one of the best scored films of the 1940s, with the piano dirges being played throughout the movie, “The Case of the Frightened Lady” is a fast moving story... (it) remains a classic for those who enjoy this genre of film" ;  and Vérité noted "a fun and feisty thriller that unlike so many modern films, doesnt outstay its welcome."  

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 