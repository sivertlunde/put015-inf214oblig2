Two Men in Town (1973 film)
{{Infobox film
| name = Two Men in Town
| image = Deux hommes dans la ville.jpg
| image_size =
| border =
| alt =
| caption =
| director = José Giovanni
| producer = Alain Delon
| writer = José Giovanni
| screenplay =
| story =
| based on =  
| narrator =
| starring = {{plainlist|*Jean Gabin
*Alain Delon
*Michel Bouquet
*Mimsy Farmer
}}
| music =
| cinematography = Jean-Jacques Tarbes
| editing = Francoise Javet
| studio =
| distributor =
| released =     
| runtime = 100 minutes
| country = {{plainlist|
*France
*Italy }}
| language =
| budget =
| gross = 2,454,112 admissions (France) 
}}
Two Men in Town (  aka. Two Against the Law) is a 1973 Franco-Italian film directed by José Giovanni.

==Plot==
After an early release from prison in 1952, an ex-safe cracker finds honest work and a new love. But when a vengeful cop begins to stalk him, and his ex-gang tries to lure him back, his determination to go straight is pushed to the breaking point.

==Cast==
* Jean Gabin - Germain Cazeneuve
* Alain Delon - Gino Strabliggi
* Mimsy Farmer - Lucie
* Victor Lanoux - Marcel
* Cécile Vassort - Évelyne
* Ilaria Occhini - Sophie
* Michel Bouquet - Inspector Goitreau
* Guido Alberti - The owner of the printing shop
* Malka Ribowska - The lawyer of Gino (as Malka Ribovska)
* Christine Fabréga - Geneviève Cazeneuve - the wife of Germain
* Gérard Depardieu - A young gangster
* Robert Castel - André Vaultier - a neighbor of Gino in Montpelleir
* Albert Augier - Rasuin
* Maurice Barrier - The judge

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 


 
 