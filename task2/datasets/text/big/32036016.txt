Taqdeer (1983 film)
{{Infobox film
 | name = Taqdeer
 | image = 
 | caption = DVD Cover
 | director = Brij
 | producer = Brij
 | writer = 
 | dialogue = 
 | starring = Shatrughan Sinha Mithun Chakraborty Hema Malini Zeenat Aman Nadira Seema Deo Madhu Malini Ranjeet
 | music = Kalyanji-Anandji
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = February 04, 1983
 | runtime = 125 min.
 | language = Hindi Rs 1.4 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1983 Hindi Indian feature directed by Brij, starring Shatrughan Sinha, Mithun Chakraborty, Hema Malini, Zeenat Aman, Nadira, Seema Deo, Madhu Malini and Ranjeet.

==Plot==

Taqdeer is an action film, featuring Shatrughan Sinha and Mithun Chakraborty, supported by Hema Malini, Zeenat Aman, Nadira, Seema Deo, Madhu Malini and Ranjeet.

==Cast==
*Mithun Chakraborty
*Hema Malini
*Zeenat Aman
*Nadira
*Seema Deo
*Madhu Malini
 
==References==
*http://www.imdb.com/title/tt0158259/
*http://ibosnetwork.com/asp/filmbodetails.asp?id=Taqdeer+%281983%29

==External links==
*  

 
 
 
 
 

 