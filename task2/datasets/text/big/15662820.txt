Onbadhu Roobai Nottu
{{Infobox film
| name = Onbadhu Roobai Nottu
| image = Onbadhu Roobai Nottu.jpg
| caption = Film poster
| director = Thangar Bachan
| producer = A. S. Ganesan
| writer = Thangar Bachan Archana Nassar Rohini
| cinematography = Thangar Bachan
| music = Bharathwaj
| studio = Indian Cinema Factory
| distributor =
| released =  
| runtime =
| country = India
| language = Tamil
}} Tamil drama Rohini in supporting roles, the film released in November 30, 2007 to high critical acclaim.  

==Plot==
Madhava Padayachi (Sathyaraj) narrates his life to a young man who saves him from shame. He goes to his home village by a bus and feels for Madhava Padayachi who was a rich farmer long time back in his village. Madhavar, as he is fondly called, his wife Velayi (Archana) and their three sons live a happy life in their village. He is an honest and hard working farmer who will do anything for others. And he is well respected by everyone in the village. Velayi is his strength but the others in the family do not approve his nature especially in money matters.

However, Dhandapani (Shivasankar), a close relative, instigates Madhavars children and makes them to speak harshly to their father. This deeply hurts Madhavar and he leaves his home along with his wife. Madhavar goes to his good old friend Hajabhai (Nasser) and his wife Kameela (Rohini) and accommodates himself in their house. Hajabhai was helped by Madhavar in his bad times in the past. But an unforeseen incident makes Madhavar to leave Hajabhais home and he sets off on a long journey that recalls his past years of his life. Rest of the film shows his journey as an oldman and how fate plays its role.

==Cast==
* Sathyaraj as Madhava Padayachi Archana as Velayi
* Nassar as Hajabhai Rohini as Kameela
* Nitesh Kumar
* Inbanila
* Sivashankar as Dhandapani
* Suryakanth
* Sathish

==Production==
The film was launched in February 2004 and progressed slowly through production. 

==References==
 

==External links==
 

 
 
 
 
 