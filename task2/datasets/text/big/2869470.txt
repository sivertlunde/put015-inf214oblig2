Too Beautiful for You
{{Infobox film
| name           = Trop belle pour toi
| image          = VTrop belle pour toi.jpg
| caption        =
| director       = Bertrand Blier
| producer       =
| writer         = Bertrand Blier
| starring       = Gérard Depardieu Carole Bouquet Josiane Balasko
| music          =
| cinematography = Philippe Rousselot
| editing        = Claudine Merlin
| distributor    =
| released       =  
| runtime        = 91 minutes
| country        = France
| language       = French
| budget         =
| gross          = $14,978,791 
}}

Too Beautiful for You ( ) is a 1989 French comedy film-drama film written and directed by Bertrand Blier. It tells the story of Bernard (Gérard Depardieu), a well established BMW car dealer in the South of France, whos cheating on his beautiful wife (Carole Bouquet) with his ordinary-looking secretary (Josiane Balasko).

==Plot==
Barthélémy Bernard, owner of a BMW car dealership, is married to a beautiful woman, Florence, but he falls in love with a very plain-looking woman, Colette, who has been hired as an interim secretary to his store. This relationship will change his life, as much as Schuberts music.
==Cast ==
* Gérard Depardieu as Bernard
* Josiane Balasko as Colette
* Carole Bouquet as Florence
* Rolland Blanche as Marcello
* François Cluzet as Pascal
* Didier Bénureau as Léonce
* Philippe Loffredo as Tanguy
* Sylvie Orcier as Marie-Catherine
* Myriam Boyer as Geneviève
* Flavien Lebarbé as The son
* Juana Marques as The daughter
* Denise Chalem as Lorène
* Jean-Louis Cordina as Gaby
* Stéphane Auberghen as Paula
* Philippe Faure as Colettes husband
* Jean-Paul Farré as The pianist

==Reception==
The film had 2,031,131 admissions in France. 

==Accolades== Prix spécial Cannes Film Festival   
* 1990 received five César Awards :
** César Award for Best Film
** César Award for Best Writing
** César Award for Best Director
** César Award for Best Actress (Carole Bouquet)
** César Award for Best Editing

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 


 
 