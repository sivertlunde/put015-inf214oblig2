Frownland (film)
{{Infobox film
| name = Frownland
| image = Frownland.jpg
| caption = Promotional poster
| director = Ronald Bronstein
| producer = Marc Raybin
| story   =  Ronald Bronstein
| starring = Dore Mann
| cinematography = Sean Price Williams
| editing  = Ronald Bronstein
| distributor = 
| released =  
| runtime  = 107 minutes
| country = United States
| language = English
}}
Frownland is a 2007 American independent film written and directed by Ronald Bronstein. It stars Dore Mann as Keith, a self-described "troll", who sweats and stutters his way through his job as a door-to-door salesman, dubiously selling coupons to assist people affected by multiple sclerosis. The film is populated by a cast of characters as dysfunctional and full of neuroses as Keith. The title comes from the song Frownland off the album Trout Mask Replica,  by Captain Beefheart (who suffered from multiple sclerosis).

It premiered at South by Southwest in 2007, where it won the Special Jury Prize,  and was self-distributed in New York City on March 7, 2008. 

==Cast==
* Dore Mann as Keith
* Paul Grimstad as Charles
* David Sandholm as Sandy
* Carmine Marino as Carmine
* Mary Bronstein (wife of director Richard Brownstein)  as Laura

==Awards==
Frownland won Best Film Not Playing at a Theater Near You at the Gotham Awards in 2007. 

==Release==
Frownland opened on March 7, 2008 at the IFC Center in New York City, where it was introduced by Lodge Kerrigan, who had previously seen the film at the Maryland Film Festival. 

==Home media==
The film was released on DVD by Factory 25 on September 29, 2009. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 