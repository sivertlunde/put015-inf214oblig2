Johnny Guitar
 
{{Infobox film
| name            = Johnny Guitar
| image           = Johnny guitar.jpg theatrical poster
| director        = Nicholas Ray Herbert J. Yates
| writer          = Ben Maddow, credited to Philip Yordan Roy Chanslor (novel)
| starring        = Joan Crawford Sterling Hayden Mercedes McCambridge Scott Brady
| music           = Peggy Lee Victor Young
| cinematography  = Harry Stradling Sr.
| editing         = Richard L. Van Enger
| distributor     = Republic Pictures 
| released        =  
| runtime         = 110 minutes
| country         = United States
| language        = English
| gross = $2.5 million (US) 
}} western drama film starring Joan Crawford, Sterling Hayden, Mercedes McCambridge, and Scott Brady.
 Herbert J. Yates.

In 2008, Johnny Guitar was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". 

==Plot==
On the outskirts of a wind-swept Arizona cattle town, an aggressive and strong-willed saloonkeeper named Vienna maintains a volatile relationship with the local cattlemen and townsfolk. Not only does she support the railroad being laid nearby (the cattlemen oppose it), but she permits "The Dancin Kid" (her former amour) and his confederates to frequent her saloon.  The locals, led by McIvers but egged on by Emma Small, a onetime rival of Vienna, are determined to force Vienna out of town, and the hold-up of the stage (they suspect, erroneously, by "The Dancin Kid") offers a perfect pretext. Vienna faces them down, helped by the mysterious and just arrived Johnny Guitar.  He turns out to be her ex-lover and a reformed gunslinger whose real name is Johnny Logan.  Their smouldering love/hate relationship develops until McIvers gives Vienna, Johnny Guitar and "The Dancin Kid" and his sidekicks 24 hours to leave.  

The Dancin Kid and his gang rob the town bank to fund their escape to California, but the pass is blocked by a railroad crew dynamiting a way in, and they flee back to their secret hideout behind a waterfall.  Emma Small convinces the townsfolk that Vienna is as guilty as the rest and the posse ride to her saloon.  Vienna appears to be getting the best of another verbal confrontation when one of the wounded bank robbers, Turkey, is discovered under a table. Emma persuades the men to hang Vienna and Turkey, and burns the saloon down. At the last second Vienna is saved by Johnny Guitar.

Vienna and Johnny escape the posse and find refuge in The Dancin Kids secret hideaway. The posse tracks them down and The Kid and his men are killed. Emma challenges Vienna to a showdown. Vienna is wounded in the duel, but she manages to kill Emma. A halt is called to the bloodbath by the posses leader, McIvers. Vienna and Johnny depart, hopeful that better days lie ahead.

==Cast==
*Joan Crawford as Vienna 
*Sterling Hayden as Johnny Guitar (Johnny Logan) 
*Mercedes McCambridge as Emma Small 
*Scott Brady as The Dancin Kid 
*Ward Bond as John McIvers 
*Ben Cooper as Turkey Ralston 
*Ernest Borgnine as Bart Lonergan 
*John Carradine as Old Tom 
*Royal Dano as Corey 
*Frank Ferguson as Marshal Williams 
*Paul Fix as Eddie  Rhys Williams as Mr. Andrews  Ian MacDonald as Pete

==Production notes==
Crawford and Nick Ray were scheduled to make a film called Lisbon at Paramount. But when the script proved unacceptable,  Crawford brought the book (to which she held the film rights and for which Author Roy Chanslor had dedicated the book to her) to Republic and had them hire Ray to direct it.    

Crawford wanted either Bette Davis or Barbara Stanwyck for the role of Emma Small, but they were too expensive.  Claire Trevor was next in mind for the role but was unable to accept due to the fact that she was tied up with another film.  Finally, Nicholas Ray brought in McCambridge. 

Most people claimed Crawford was easy to work with, always professional, generous, patient and kind.   Issues between the two women cropped up early on, but Ray was not alarmed - at first. He found it "heaven sent" that they disliked each other and felt it added greatly to the dramatic conflict.  The reasons for the feud appear to date back to a time when Crawford had once dated McCambridges husband, Fletcher Markle. According to some of the other co-stars, McCambridge needled Crawford about it.  McCambridge also appears to have disliked the fact that Crawford and Ray were in the midst of an affair. Crawford, on the other hand, disliked what she perceived to be "special attention" that Ray was giving to McCambridge. 

Making things worse was the fact that McCambridge was battling alcoholism during this period,  something she admitted later contributed to the problems between herself and Crawford. 

After filming, McCambridge and Hayden publicly declared their dislike of Crawford, with McCambridge labeling Crawford, "a mean, tipsy, powerful, rotten-egg lady."     Hayden said in an interview, "There’s is not enough money in Hollywood to lure me into making another picture with Joan Crawford. And I like money."  

Crawford for her part said of McCambridge, "I have four children - I do not need a fifth."   However, Crawford was surprised at the comments by Hayden, claiming she had a letter from him saying he would love to work with her again.  

Later, Ray claimed that Crawford, during a rage, drunkenly threw McCambridges costumes into the street.  However, McCambridge only had one costume throughout the entire film. Crawford later laughingly admitted she had thrown McCambridges own clothing into the street.  Ray also said of that time, "Joan was drinking a lot and she liked to fight," but that she was also "very attractive, with a basic decency."  

==Reception==
Originally opening to negative reviews, the film later became liked and would become regarded as one of Rays best films, topped by the famous title song.

Variety (magazine)|Variety commented, "It proves   should leave saddles and Levis to someone else and stick to city lights for a background.   is only a fair piece of entertainment.   becomes so involved with character nuances and neuroses, all wrapped up in dialogue, that   never has a chance to rear up in the saddle...The people in the story never achieve much depth, this character shallowness being at odds with the pretentious attempt at analysis to which the script and direction devotes so much time."  

Bosley Crowther singled out Crawfords physical bearing for criticism in his New York Times review, stating "...no more femininity comes from her than from the rugged Mr. Heflin in "Shane." For the lady, as usual, is as sexless as the lions on the public library steps and as sharp and romantically forbidding as a package of unwrapped razor blades."  

The film is beloved of French critics and filmmakers, such as  s".

Spanish director Pedro Almodóvar pays homage to the film in his 1988 release, Women on the Verge of a Nervous Breakdown. His lead character Pepa Marcos (Carmen Maura), a voice artist, passes out while dubbing Viennas voice in a scene where Johnny (voiced earlier by Pepas ex-lover Iván) and she banter about their conflicted past. Almodovars film also ends with a chase and an obsessed woman shooting at his lead character.

The Chicago Readers Jonathan Rosenbaum lists Johnny Guitar as one of the 100 best American films. 

Japanese film director Shinji Aoyama listed Johnny Guitar as one of the Greatest Films of All Time in 2012. He said, "Johnny Guitar is the only movie that I‘d like to remake someday, although I know that it’s impossible. It’s probably closest to the worst nightmare I can have. I know for sure that my desire to remake this movie comes from my warped thought that I want to remake my own nightmare." 

Despite a number of initial negative reviews, in the USA and Canada Johnny Guitar grossed more than $2,500,000 as of January 1955 ($21,396,003.72 in 2012 dollars, adjusted for inflation   and was #27 on Varietys list of top money makers of 1954. 

==Commentary== McCarthy witch-hunts. Western — Truffaut, who admired the film, called it "a phony Western".
 The Killing, Sterling Hayden stated that he did not care for Johnny Guitar. "They put string, like you get at the grocery store, over my guitar in case I accidentally hit them," he said, acknowledging that "I cant play guitar, and cant sing a good-goddamn, either." "I was at war on that film, during the daytime, with Joan Crawford," he recalled, "and at night with my second wife." Despite his reservations about the film, Hayden acknowledged its popularity.

According to Martin Scorsese, contemporary American audiences "didnt know what to make of it, so they either ignored it or laughed at it." European audiences, on the other hand, free of conventional biases, saw  Johnny Guitar for what it was: "an intense, unconventional, stylized picture, full of ambiguities and subtexts that rendered it extremely modern." 

The film has been released in VHS and DVD formats.

==Adaptations==
Johnny Guitar was adapted into a stage musical, which debuted Off-Broadway in 2004, with a book by American television producer Nicholas van Hoogstraten, lyrics by Joel Higgins, and music by Martin Silvestri and Joel Higgins. It starred Judy McLane, Ann Crumb, Steve Blanchard, and Robert Evan, and was the recipient of the Outer Critics Circle Award for Best Musical, as well as a nominee for the Lucille Lortel Awards and the Drama Desk Awards.

==In popular culture== Johnny Guitar", written by Peggy Lee and Victor Young and originally sung by Peggy Lee herself in the films closing moments, is a hugely popular song covered many times.  
* The song "Johnny Guitar" can be heard throughout the 2010 game  . 
* The guitarist of the band Carter the Unstoppable Sex Machine had the stage nickname "Johnny Guitar".  dubbers for the film into Spanish. La Sirène du Mississipi, played by Jean-Paul Belmondo and Catherine Deneuve, go to a showing of “Johnny Guitar” in Lyon and discuss the film in the street afterwards.

==References==
 

==External links==
* 
* 
* 
* 
* 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 