Mabel at the Wheel
 
{{Infobox film
| name = Mabel at the Wheel
| image = Mabel at the Wheel.jpg
| imagesize= 180px
| caption = Mabel Normand and Charles Chaplin in the film
| director = Mabel Normand Mack Sennett
| producer = Mack Sennett
| writer =
| starring = Charles Chaplin Mabel Normand Harry McCoy Chester Conklin Mack Sennett Al St. John Joe Bordeaux Mack Swain William Hauber Frank D. Williams
| editing =
| distributor = Keystone Studios
| released =  
| runtime = 18 minutes English (Original titles)
| country = United States
| budget =
}}
  1914 cinema of the United States|American-made motion picture starring Charles Chaplin and Mabel Normand, and directed by Mabel Normand and Mack Sennett.

==Synopsis==
Charlie offers Mabel a ride on his two-seater motorcycle, which she accepts in preference to his rivals racing car. Unfortunately as they go over a bump, she falls off into a puddle. The rival, who has followed in his car, picks up the now stranded Mabel. He lets her drive, sitting tight beside her.

Charlie at last notices she is gone and falls off the bike. He sees them together now stopped and standing beside the car. They leave the car for a short while and Charlie lets down the rear tyre. His rival returns and is furious. They throw rocks at Charlie and he throws them back. The rivals friend appears and gets caught up in the rock-throwing confusion.

We cut to "The Auto Race" where Charlie hovers round the cars. The drivers usher him away when they see he has a sharp pin. Charlie stands puffing heavily on a cigarette. He uses his pin to get through the crowd, where he propositions Mabel and gets slapped. Charlie then whistles and two thugs appear and kidnap his rival just before the race starts. But Mabel decides to don his racing clothes and take the wheel in his place.

As the race progresses, despite a very late start, Mabel, with a co-driver beside her, manages to gain a lead of three laps. Charlie with his henchmen, tries to sabotage the race by using oil and bombs on the track. The oil temporarily spins Mabels car, no.4, around and it goes backwards for a lap until the oil spins it around again to continue the right way. The car tips over on a bend but a group of men push the heavy Bentley V8 upright again. Meanwhile the rival escapes his ropes and sees Mabel driving his car. The crowd stand as she crosses the finishing line. The rival and his friend go to congratulate her. Meanwhile Charlie throws a bomb in the air and blows up both himself and his two thugs.

==Cast==
* Charles Chaplin - Villain
* Mabel Normand - Mabel
* Harry McCoy - Mabels boyfriend
* Chester Conklin - Mabels father
* Mack Sennett - Reporter
* Al St. John - Henchman
* Joe Bordeaux - Dubious character
* Mack Swain - Spectator
* William Hauber - Mabels co-driver
* Dan Albert - Cheering Spectator (uncredited) 
* Charles Avery - Spectator in Grandstand (uncredited) 
* Charley Chase - Race Spectator (uncredited) 
* Alice Davenport - Spectator in Grandstand (uncredited) 
* Minta Durfee - Spectator in Grandstand (uncredited) 
* Edgar Kennedy - Spectator in Grandstand (uncredited) 
* Charles Lakin - Cheering Spectator (uncredited) 
* Grover Ligon - Henchman (uncredited) 
* Fred Mace - Dubious character (uncredited) 
* Edward Nolan - Spectator (uncredited) 
* Fred Wagner - Race Starter (uncredited)

==See also==
* List of American films of 1914
* Charlie Chaplin filmography

==External links==
* 
*  
*  

 
 
 
 
 
 
 
 
 
 