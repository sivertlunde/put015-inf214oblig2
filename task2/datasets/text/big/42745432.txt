Hot Pursuit (2015 film)
{{Infobox film
| name           = Hot Pursuit
| image          = Hot Pursuit 2015 poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Anne Fletcher
| producer       = Dana Fox Bruna Papandrea Reese Witherspoon
| writer         = David Feeney John Quaintance
| based on       =  
| starring       = Reese Witherspoon Sofía Vergara
| music          = Christophe Beck
| cinematography = Oliver Stapleton
| editing        = Priscilla Nedd-Friendly
| studio         = New Line Cinema Metro-Goldwyn-Mayer Pacific Standard
| distributor    = Warner Bros. Pictures
| released       =   
| runtime        = 87 minutes 
| country        = United States
| language       = English
| gross          = 
}}

Hot Pursuit is an upcoming American action comedy film directed by Anne Fletcher and written by David Feeney and John Quaintance. The film stars Reese Witherspoon and Sofía Vergara. The film is scheduled to be released on May 8, 2015, by Warner Bros. Pictures. 

== Plot == dirty cops.

== Cast ==
* Reese Witherspoon as Officer Cooper
* Sofía Vergara as Daniella 
* Jodi Lyn Brockton as Dealer
* Robert Kazinsky Michael Mosley
* John Carroll Lynch as Captain Emmett
* Matthew Del Negro as Hauser
* Mike Birbiglia as Steve
* Richard T. Jones as Det. Jackson
* Leslie Quezada as Dancer #3
* Chelsea Avina as Quinceañera Girl #1

== Production ==

=== Filming ===
The principal photography of the film began on May 12, 2014 in New Orleans, Louisiana, which was set to last for two months.    In the second week of filming, Witherspoon was spotted during the shooting which was taken place in Ponchatoula, Louisiana|Ponchatoula, LA. 

===Marketing===
The films first trailer was released on February 12, 2015.  

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 