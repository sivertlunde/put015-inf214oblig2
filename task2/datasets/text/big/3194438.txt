A Return to Salem's Lot
{{Infobox film
| name           = A Return to Salems Lot
| image          = A Return to Salems Lot (1987).jpg
| image_size     =
| caption        = UK VHS cover
| director       = Larry Cohen
| producer       = Larry Cohen Paul Kurta
| writer         = Larry Cohen James Dixon
| starring       = Michael Moriarty Andrew Duggan Samuel Fuller Evelyn Keyes June Havoc
| music          = Michael Minard
| editing        = Armond Leibowitz
| distributor    = Warner Bros.
| released       = September 11, 1987
| runtime        = 100 minutes
| country        = United States
| language       =
| budget         =
| gross          =
}}
A Return to Salems Lot is a 1987 horror film written and directed by Larry Cohen.  

==Plot== adolescent son American flag instead of the traditional stake. As the trio escapes Salems Lot, the vampires are left in the sun to burn along with their homes.

==Cast==
*Michael Moriarty as Joe Weber
*Samuel Fuller as Van Meer
*Andrew Duggan as Judge Axel
*Evelyn Keyes as Mrs. Axel
*June Havoc as Aunt Clara
*Tara Reid as Amanda

==Release==
The film was given a limited release theatrically in the United States by Warner Bros. in 1987. It was released on VHS by Warner Home Video the following year.  The film was released to on demand DVD by the Warner Archive Collection in 2010. 

==Reception==
Critical reception has been mixed to negative.  DVD Talk commented that the film was "Too interesting to miss, but regrettably not very scary". 

==See also==
*Vampire film

==References==
 

==External links==
* 
*  

 
 
 

 
 
 
 
 
 
 
 