Laaga Chunari Mein Daag
 
 
{{Infobox film
| name           = Laaga Chunari Mein Daag
| image          = Laaga Chunari Mein Daag Poster.jpg 
| caption        = Theatrical release poster
| director       = Pradeep Sarkar
| producer       = Aditya Chopra
| story          = Aditya Chopra
| screenplay     = Rekha Nigam 
| narrator       = Rani Mukerji 
| starring       = Rani Mukerji Jaya Bachchan Konkona Sen Sharma Abhishek Bachchan Kunal Kapoor
| music          = Shantanu Moitra
| cinematography = Sushil Rajpal
| editing        = Kaushik Das
| distributor    = Yash Raj Films
| released       =  
| runtime        = 137 mins
| country        = India
| language       = Hindi
| budget         =  (approx.) http://ibnlive.in.com/news/big-b-loved-laaga-chunari-mein-daag-defends-director/50821-8.html 
| gross          =     (Total Worldwide Collection until close date at 4 weeks)
}}

Laaga Chunari Mein Daag – Journey Of A Woman  ( ,  }}, English: My Veil is Stained) is a 2007 Indian drama film directed by Pradeep Sarkar and starring Rani Mukerji, Jaya Bachchan, Konkona Sen Sharma, Kunal Kapoor and Anupam Kher with special appearances by Abhishek Bachchan and Hema Malini. Produced by Aditya Chopra it premiered on 12 October 2007. The film was the first directed by Sarkar under the Yash Raj Films banner.

==Plot==

Vibhavari, affectionately called Badki (Rani Mukerji), and Shubhavari, affectionately called Chutki (Konkona Sen Sharma) are the daughters of Shivshankar Sahay (Anupam Kher) and Sabitri (Jaya Bachchan). They live together on the banks of the Ganges in Banaras. Life is full of happiness and joy for the two, though the family is relatively poor. Badki especially is protective of her family and is determined to ensure Chutki completes her education.

As things go from bad to worse, Badki goes to Mumbai to find work. After encountering many obstacles and feeling desperate and out of options, she is forced to become the exclusive call girl "Natasha". Misleading her family – telling them she is an event planner – and sending them money for her fathers medicines and to keep the family home from being foreclosed on, Badki hides her secret. Chutki completes her MBA and, unannounced, comes to live with Badki in Mumbai. The younger sister becomes a trainee at the Matrix Advertising agency, and falls in love with her boss, creative director Vivaan (Kunal Kapoor). Badki finds love in attorney Rohan (Abhishek Bachchan) but leaves him, afraid that he would be disgusted by her profession. Chutki inadvertently discovers what Badki does for a living.

The two sisters return to Banaras for Chutkis wedding. Badki comes face to face with Rohan, and he is revealed to be Vivaans brother. Old feelings resurface between the two and eventually Rohan asks Badki to marry him. Badki refuses, feeling that Rohan would never accept her after knowing her profession. Chutki convinces Badki to think about her happiness for once and accept his proposal. During the conversation Shivshankar, Sabitri and Chutki finally face the lies and secrets realising what Badki sacrificed for the family. Badki reveals her profession to Rohan, who shocks her by announcing that he knew, from the moment he saw her with her client in Zurich, that she was a call girl. Despite that he still wants to marry her. Badki readily accepts, finally gaining the happiness she has sought.

== Cast ==

===Main cast===
* Rani Mukerji as Vibhavari Sahay (Badki): Badki (which means "big sister") is also known as Natasha. The elder daughter, Badki is fiercely protective of her family. When her education is stopped to compensate for expenses she does not fret; she instead carries on and helps her mother. Her father has always disliked the fact that she was not born a son, which hurts her. She goes to Mumbai to help her family and does not hesitate in making sacrifices for them.
* Jaya Bachchan as Sabitri Sahay: Sabitri is the one who holds the Sahay family together. As her husbands bitterness grows and affects his health, she leans on Badki for support. She is the only one in the family who knows what Badki has done to keep them all steady.
* Konkona Sen Sharma as Shubhavari Sahay (Chutki): Chutki ("little sister") has everything she could ever want. Her mother and sister love her to the core and do everything to protect her. She is intelligent and sharp-minded but unaware of the sacrifices Badki is making for the family.
* Anupam Kher as Shivshankar Sahay: A former economics teacher, Shivshanker is unable to cope with an unfair dismissal at the college and his bitterness grows and grows. As the family sinks into poverty, he does nothing to help. He buys lottery tickets thinking he will win and, more importantly, bring back his days of glory. He resents the fact that Badki was not born a son and vents out his frustrations on his loved ones. His disappointment in Badki continues when she goes to Mumbai, thinking she will achieve nothing.
* Kunal Kapoor as Vivaan Varma: Having lived all his life in the big city, Vivaan has no idea how the rest of India lives or functions. When he meets Chutki, he dismisses her citing that she is a small village girl with no prospect of a job in his advertising agency. He changes his mind when he comes face-to-face with her independent attitude and is blown away.
* Abhishek Bachchan as Rohan Varma: Rohan is a top attorney. His look may be that of someone who is serious, but inside he has a childlike innocence. He meets Badki and falls in love with her but she runs away from him. Rohan is set on finding out why Badki is the way she is.
* Kamini Kaushal 
* Hema Malini

===Other cast===
 
 
* Tarana Raja as Sophia
* Ninad Kamat as Karan
* Harsh Chhaya as K. Gupta
* Murli Sharma as Sunil   
 
* Tinnu Anand as Rajshankar Rajjo Sahay 
* Sushant Singh as Ratan Sahay 
* Suchitra Pillai as Michele 
 

== Production ==
Saif Ali Khan was originally approached for the role of Rohan that went to Abhishek Bachchan,    and Vidya Balan had earlier been offered the role that went to Konkona Sen Sharma. Laaga Chunari Mein Daag was Jaya Bachchans first film since Nikhil Advanis  Kal Ho Naa Ho in 2003.

The production of the film gathered some controversy when a lighting crew-member drowned in the Ganges River.    During a shooting session in Varanasi, bodyguards of Rani Mukerji aggressively moved media people and fans away from the filmset. A political and media storm followed, as various groups insisted that Mukerji should have stopped the security guards. The actress then apologised to the media, though claiming the media were trying to get too close to both her and Konkona Sen Sharma.      Some scenes involving Mukerji and Bachchan were shot in Bern, Switzerland and Lucerne, Switzerland.

==Crew==
* Director: Pradeep Sarkar
* Producer: Pradeep Sarkar and Aditya Chopra
* Writer: Pradeep Sarkar
* Dialogue: Rekha Nigam
* Music Director: Shantanu Moitra
* Lyrics: Swanand Kirkire
* Editing: Kaushik Das
* Cinematographer: Sushil Rajpal
* Choreography: Howard Rosemeyer
* Costumes: Sabyasachi Mukherjee, Subarna Ray Chaudhuri, Shiraz Siddique
* Sound design: Rishi Oberoi

== Music ==
{{Infobox album |  
  Name        = Laaga Chunari Mein Daag&nbsp;— Journey Of A Woman |
  Type        = Album |
  Artist      = Shantanu Moitra |
  Cover       = Laaga Chunari Mein Daag (soundtrack - album cover).jpg|
  Released    =  10 September 2007 (India)|
  Recorded    = | Feature film soundtrack |
  Length      = |
  Label       =  Yash Raj Music |
  Producer    = Aditya Chopra |
  Reviews     = |
  Last album  =    (2007) |
  This album  = Laaga Chunari Mein Daag (2007) |
  Next album  = Khoya Khoya Chand (2007)|
}}
The films soundtrack was released on 10 September 2007. Songs such as Hum To Aise Hain and Kachchi Kaliyaan featured playback singers such as Sunidhi Chauhan, Shreya Ghoshal and Krishnakumar Kunnath|KK. The soundtrack received a 3 out of 5 rating on indiaFm.com.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="centr"
! #!!Song !! Singer(s) !! Length !! Picturised on
|-
| 1
|Hum To Aise Hain
| Sunidhi Chauhan, Shreya Ghoshal, Swanand Kirkire & Pranab Biswas
| 05:07
| Rani Mukerji, Konkona Sen Sharma, Jaya Bachchan & Anupam Kher
|-
| 2
|Zara Gungunalein Chalo
| Babul Supriyo & Mahalakshmi Iyer
| 04:46
| Abhishek Bachchan and Rani Mukerji
|-
| 3
| Chunari Mein Daag
| Shubha Mudgal & Meeta Vashisht
| 04:21
| Rani Mukerji
|-
| 4
|Ik Teekhi Teekhi Si Ladki KK & Shreya Ghoshal
| 04:45
| Kunal Kapoor & Konkona Sen Sharma
|-
| 5
|Ehi Thaiyaa Motiya Rekha Bhardwaj
| 04:40
| Hema Malini
|-
| 6
| Kachchi Kaliyaan
| Sonu Nigam, Krishnakumar Kunnath|KK, Sunidhi Chauhan & Shreya Ghoshal
| 04:35
| Abhishek Bachchan, Rani Mukerji, Kunal Kapoor, Anupam Kher, Jaya Bachchan & Konkona Sen Sharma
|}

==Reception==
The film premiered on 12 October 2007 in Mumbai and was released on the same day in North America. The film opened well as it was highly anticipated. However due to negative reviews as well as the competition it faced from the more successful Bhool Bhulaiya it became a box office dud in India. It was however more successful overseas.

In an interview with the filmmaker Pradeep Sarkar, said: "It was tight budget film made with Rs 150&nbsp;million and already on the first week it has made Rs 250&nbsp;million gross worldwide. In what way we are saying that it is not doing well?"  asked Sarkar. "Admitting that the film did take a slow start in the domestic market, hes hopeful it will grow on audiences in the weeks ahead, much in the same way that his debut film Parineeta did. " 
 Marathi film Doghi" (made by Sumitra Bhave with Uttara Baokar, Sadashiv Amrapurkar, Sonali Kulkarni and Renuka Daftardar).  Anupama Chopra found the film "a cauldron of wonderful cinematic talent, undone by half-baked writing". 

Critical response in the United States to the film was more mixed. Frank Lovece of Film Journal International said that the film put "glossy Bollywood confection" in a historical context, calling it a "good old-fashioned, Douglas Sirk-style womens weepie ... so universal you could substitute Joan Crawford for Rani Mukerji and New York City for Mumbai".  Maitland McDonagh of TV Guide found the film "breaks no new ground but is solidly entertaining"  while David Chute of L.A. Weekly said, "The movie works so hard to transform its shocking subject into acceptable material for middlebrow melodrama that it never deals with it".  Rachel Saltz of the New York Times termed the film, "A fascinating blend of musical, melodrama and feminist fairy tale". 

==Box office==
According to Box Office Mojo,the Domestic gross in the Opening Weekend was 19.7&nbsp;million(approx.)or US$320,987; http://www.boxofficemojo.com/movies/?page=main&id=lagachunarimeindaag.htm  being release in 60  theatres, the close Date was 4 November 2007  and It was in release for 28  days(4 Weeks).

Total lifetime grosses was 41.6&nbsp;million (approx.) or US$675,102  (7.2%  of the total Worldwide Collection), at the Foreign market the collection was 533.1&nbsp;million (approx.) or US$8,679,460  (92.8% of the total Worldwide Collection).  Total Worldwide collection 574.7&nbsp;million or US$9,354,562  reported until 4 November 2007. 

==Awards==

=== Filmfare Awards ===
Nominations
* Filmfare Best Actress Award- Rani Mukerji
* Filmfare Best Supporting Actress Award- Konkona Sen Sharma

=== Stardust Awards ===
Nominated
* Stardust Star of the Year Award - Female- Rani Mukerji

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 