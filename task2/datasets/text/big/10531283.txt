Khubsoorat
 Khoobsurat}}
 
{{Infobox film
| name = Khubsoorat
| image = Khubsoorat 1980 film poster.jpg
| caption = Film poster
| director = Hrishikesh Mukherjee
| producer = Hrishikesh Mukherjee N. C. Sippy Gulzar
| story = D. N. Mukherjee
| starring = Ashok Kumar Rekha Rakesh Roshan
| music = R. D. Burman
| cinematography = Jaywant Pathare
| editing = Subhash Gupta
| distributor = 
| released =  
| runtime = 
| country = India
| language = Hindi
| budget = 
| gross =
}} Indian Hindi film directed by Hrishikesh Mukherjee, whose direction together with dialogues by Gulzar.

The film won the 1981 Filmfare Best Movie Award. The lead actress, Rekha, won her first Filmfare Best Actress Award for her role as Manju Dayal (she was also nominated for a role in the film Judaai (1980 film)|Judaai that year).

==Plot== Ashok Kumar), runs her household very strictly. While most of her family resent this, they obey her instructions to be on her good side. 

Things start to change after her second son gets married to Anju. Manju (Rekha), Anjus sister, comes over to stay for a few days with the Gupta family. Manju resents the way Nirmala treats people and feels some permanent changes are needed in the household. She begins to stir up trouble as she slowly convinces the others that its OK to speak ones heart and break the rules sometimes. Everyone in the household is enamored by Manjus vivaciousness and fun-loving spirit, except Nirmala who considers her antics childish and irresponsible.

One day while all other members of the family except Nirmala are away from the house for three or four days, leaving Manju in the house, Dwarka Prasad suffers a heart attack when he verbally attacks his wife for being unfriendly and harsh towards Manju. Nirmala then realizes Manjus responsible and caring attitude as she helps Dwarka Prasad receive medical treatment and takes care of him.

The movie ends with Manju marrying Nirmalas third son Inder (Rakesh Roshan) as she is accepted by Nirmala.

==Cast==
*Ashok Kumar as Dwarka Prasad Gupta 
*Rekha as Manju Dayal
*Rakesh Roshan as Inder Gupta 
*Shashikala as Bari Bhabhi  Aradhana as Anju Gupta 
*Dina Pathak as Nirmala Gupta  David as Ram Dayal 
*S.N. Banerjee as Uma Shankar 
*Keshto Mukherjee as Ashrafi Lal 
*Ranjit Chowdhry as Jagan Gupta

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Sun Sun Sun Didi"
| Asha Bhosle
|-
| 2
| "Qayda Qayda"
| Sapan Chakraborty, Rekha
|-
| 3
| "Piya Bawri Piya Bawri"
| Ashok Kumar, Asha Bhosle
|-
| 4
| "Sare Niyam Tod Do"
| Asha Bhosle
|-
| 5
| "Sare Niyam Tod Do"
| Sapan Chakraborty, Rekha
|}

==Awards==
*1981 Filmfare Best Movie Award - N.C. Sippy, Hrishikesh Mukherjee.
*1981 Filmfare Best Actress Award - Rekha

== External links ==
*  
*  , rediff.com, 28 August 2006. Retrieved 2 October 2009.

 
 

 
 
 
 
 
 
 
 