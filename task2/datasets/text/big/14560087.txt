The Eunuch
 
 
 
{{Infobox film name = The Eunuch image = The Eunuch.jpg alt =  caption = DVD cover art traditional = 鬼太監 simplified = 鬼太监 pinyin = Guǐ Tài Jiān}} director = Teddy Yip producer = Runme Shaw writer = Lo Wei starring = James Tien music = Stanley Chow cinematography = Charles Tung Yau Kei editing = Chiang Hsing-lung studio = Shaw Brothers Studio distributor = Shaw Brothers Studio released =   runtime =  country = Hong Kong language = Mandarin budget =  gross = 
}} Teddy Yip, and starring Pai Ying and Lisa Chiao Chiao.

==Cast==
*Pai Ying as Lord Kuei Teh Hai
*Lisa Chiao Chiao as Yen Yen
*Chung Wa as Prince Chu Chin
*Yeung Chi-hing as Green Bamboo Master
*Mang Ga as Bamboo Masters daughter
*Wang Hsieh as Szu Sung
*Lo Wei as King
*Ou-yang Sha-fei as Kun Erh Kings wife
*Hao Li-jen as Man Liang Po James Tien as Kueis low rank officer
*Yeung Chak-lam as fighter beaten in contest
*Little Unicorn as soldier who kills child
*Chu Gam as Lord Wang
*Wong Chung-shun as Lord Lui
*Chow Siu-loi as Swordsman Wu Chun
*Lei Lung as soldier searching for waiter
*Billy Chan as assassin
*Chui Chung-hok as assassin
*Yung Yuk-yi as Yen Yens Female master
*Sammo Hung as first fighter in final contest Mars as soldier
*Ling Hon as spectator
*Yee Kwan as spectator
*Chin Chun as spectator
*Tsang Choh-lam as waiter who robes baby
*Simon Chui as Kuan Wu (waiter / assassin)
*Ho Wan-tai as Kueis man
*Yeung Wah as spectator
*Chai Lam as spectator
*Mama Hung as Yen Yens mother
*Kwan Yan
*Leung Seung-wan
*Wu Chi-chin
*To Wing-leung
*Gam Tin-chue
*Lam Yuen

==External links==
* 
* 
*  on Hong Kong Cinemagic

 
 
 
 
 
 


 
 