Yonggary
{{Infobox film
| name           = Yonggary
| image          = Yongary_1967_Poster.jpg
| caption        = Theatrical poster
| film name = {{Film name
 | hangul         =  
 | hanja          =   용가리
 | rr             = Taekoesu Yonggary
 | mr             = Taegoesu yonggari }} Kim Ki-duk
| producer       = 
| writer         = Kim Ki-duk Seo Yun-sung
| starring       = Oh Yeong-il Nam Jeong-im
| music          = Jeon Jeong-Keun
| cinematography = Byeon In-jib
| editing        = 
| studio         = Keukdong Entertainment Company
| distributor    = American International Pictures|AIP-TV (USA)
| released       =  
| runtime        = 80 minutes
| country        = South Korea
| language       = Korean
| budget         = 
}} Kim Ki-duk, Toei would help co-produce the film with Keukdong Entertainment.

The film was released direct to Television in the United States by American International Pictures in 1968 as Yongary, Monster from the Deep.

In 1999, a remake simply called Yonggary (1999 film)|Yonggary was produced by Younggu-Art Movies and Zero Nine Entertainment and released direct to video in the United States as Reptilian.

==Synopsis==
In the Middle East, a bomb is set off that creates massive earthquakes. Meanwhile in South Korea, a young couple is about to get married and the tension builds when South Korea sends a manned space capsule to investigate the bomb site. The earthquake makes its way to South Korea, caused by a giant monster named Yongary (inspired by a mythical creature in Korean lore). Yongary attacks Seoul and makes his way to the oil refineries where he consumes the oil. A child related to the aforementioned couple turns off the refineries oil basins; Yonggary, enraged, starts attacking until a chemical explosion at the refinery proves to have an effect on it. The Korean Government then uses oil to draw Yonggary on a local river, and kills it with a refined version of the ammonia compound.

==Availability==
In the United States, the original film is now widely available on DVD in various budget-DVD packs and single budget DVDs. MGM released the original Yongary, Monster from the Deep as part of their Midnite Movies series on September 11, 2007. It is paired as a double feature with the giant ape film Konga (film)|Konga (1961 in film|1961).  James Owsley, a former Director of Technical Services for MGM, could not find the original Korean negative, and believes that it may no longer exist.  However, it was soon discovered that roughly 48 minutes of the cut of the film in its original language still exists.

==Remake== The film was remade in 1999 by Shim Hyung-rae. To date, the Yonggary remake is the most expensive Korean film ever produced. The film was a moderate success and due to its success, a 2001 Upgrade Edition was produced a few months after its initial release. This version was slightly different from the original 1999 cut. The 2001 Upgrade featured new additional scenes, a new soundtrack by Chris Desmond, and new effects. 60% of the original 1999 cut made it to the 2001 upgrade version. It first premiered at a Japanese film festival in 2000 and then had its worldwide release in January 2001, which flopped due to poor promotion. The original 1999 cut was never released on VHS or DVD and the only version of the film available is the 2001 upgrade edition, which is now known as Reptilian. It has been debated whether prints of the original 1999 cut had been destroyed by the studio when at the time they believed that the 2001 upgrade edition would be a box office success. The 1999 cut was a proper remake of the first film, where Yonggary dies at the end, and the aliens were not in it.

== In other media == Ron Sparks only in the show occasionally as a voice with Ed telling him "youre not in this episode". Instead of being taped on the usual set the episode features Liana on the set of a giantess video that she is shooting, where (like Yongary) she is destroying a city. It also features a lengthy comparison by Ed of Yongarys rolling around to his dog when he has to go to the bathroom.
The 1993 comedy Young-Goo and Dinosaur Zu-Zu, also directed by Shim Hyung-rae parodies Yongary several times, mainly the design of the titular monster.   

==References==
 

=== Bibliography ===
*  

==External links==
*  
*  
*  
*  
* 

 
 
 
 
 
 
 