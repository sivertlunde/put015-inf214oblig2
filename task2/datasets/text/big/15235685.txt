The Ramblin' Kid
 
{{Infobox film
| name           = The Ramblin Kid
| image          = 
| caption        = 
| director       = Edward Sedgwick
| producer       = 
| writer         = Earl Wayland Bowman Richard Schayer
| starring       = Hoot Gibson Laura La Plante
| cinematography = Virgil Miller
| editing        = 
| distributor    = Universal Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
}}
 Western film directed by Edward Sedgwick and featuring Hoot Gibson and Laura La Plante. 

==Cast==
* Hoot Gibson as The Ramblin Kid
* Laura La Plante as Carolyn June Harold Goodwin as Skinny Rawlins William Welsh as Lafe Dorsey
* W.T. McCulley as Sheriff Tom Poole
* Charles K. French as Joshua Heck
* G. Raymond Nye as Mike Sabota
* Carol Holloway as Mrs. Ophelia Cobb
* Goober Glenn as Parker George King as Sing Pete
* Gyp Streeter as John Judd

==See also==
* Hoot Gibson filmography

==References==
 

==External links==
* 
*  at emovieposter.com

 
 
 
 
 
 
 
 


 
 