Slow Burn (2000 film)
{{Infobox film
| name           = Slow Burn
| image          = 
| caption        = 
| director       = Christian Ford
| producer       = Robert Swanson Kate Driver Roger Soffer Brian Agnew James Agnew
| writer         = Christian Ford Roger Soffer Stuart Wilson
| music          = Anthony Marinelli
| cinematography = Mark Vicente
| editing        = Jack Hofstra Troy Takaki
| studio         = Blue Rider Pictures Morlaw Films
| distributor    = Artisan Entertainment 
| released       = 12 September 2000
| runtime        = 
| country        = United States
| language       = English
| budget         = $10,000,000 
| gross          =
}}
 Stuart Wilson, and Josh Brolin.

==Plot==
Trina (Driver) continues a family quest to find diamonds hidden deep in the desert long after her parents death. Her search comes to fruition when two escaped convicts (Spader and Brolin), having found the diamonds, stumble onto Trinas path. An old family friend (Wilson) watches events unfold from a distance.

==Cast==
* Minnie Driver as Trina McTeague
* James Spader as Marcus Stuart Wilson as Frank Norris
* Josh Brolin as Duster
* Chris Mulkey as Jacob McTeague
* Caprice Benedetti as Catalina McTeague

==External links==
*  

 
 


 