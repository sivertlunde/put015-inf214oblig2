Secret Ballot (film)
{{Infobox film
| name           = Secret Ballot
| image          = RayeMakhfijpg.jpg
| image_size     =
| caption        = DVD cover
| director       = Babak Payami
| producer       = Marco Mueller
| writer         = Babak Payami
| narrator       = 
| starring       = Nassim Abdi Cyrus Abidi
| music          = Michael Galasso
| cinematography = Farzad Jadat
| editing        = Babak Karimi
| distributor    =  2001
| runtime        = 105 min.
| country        = Iran   Italy   Canada   Switzerland  Persian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Secret Ballot ( ) is an Iranian film from 2001 in film|2001. It was directed by Babak Payami. The film covers a day in the life of an agent collecting votes on an island in Iran.

==Plot summary==
 
The story begins at a small, two-man army post on a remote island. It is voting day, and an election agent is due to arrive by boat. A young woman arrives, collects the official voting box, and demands that the soldier on duty escort her around the island. They climb into a military jeep and begin driving around, looking for voters. The nameless woman is totally dedicated to her duty, a true believer in the importance of voting, a tireless worker, rather voluble and certainly not submissive. This confuses and angers the dim-witted soldier, who feels that a man should be the voting agent. Chador and all, shes clearly a liberated woman, a "city gal" as described by the soldier.

The couple-by-necessity do eventually (jeep trouble aside) scour the land to find eligible voters among the sparse locals. The trek starts in a desert and gradually moves to somewhat greener places. It is educational for both parties. They encounter a variety of people (mostly illiterate peasants) and situations, which simultaneously instructs the two roamers and the audience.  By the end of the film, only a few people have voted, and the young woman is largely disillusioned about the entire process.  Several people refused to vote for the "approved" candidates, and one voted for her. There is an undercurrent of an unspoken affection having developed between her and the soldier.

==Overview==
Raye makhfi was filmed entirely in Kish Island, a resort island in the Persian Gulf although it is not mentioned in the movie. In fact the setting looks like a semi-barren landscape that could be a mainland or an island. There, by the sea, on a desert beach, two soldiers stand guard against smugglers. Apparently, the locals are not particularly fluent in Persian language|Persian, the national language of Iran, with some speaking in gulf dialect Arabic. On several occasions, the election agent is frustrated by the language barrier. It is implied that a number of foreign nationals (fishermen, peddlers) are present on the island &mdash; they are not eligible to vote, of course. The locals are very strict Islamists; some of the women even wear face masks in public. Women are completely submissive to the wishes of their men and will not vote unless given permission.

A serious natural, sly, or tongue-in-cheek humor emerges in this realistic, ethnographic, patriotic work that is quite outspoken which stresses solitude|isolation, not poverty. And it is not bound by politically correct limitations. The young woman from the city, commenting on the minimum voting age, says of a local "She can marry at 12 but she cannot vote."  

She is fanatical about the importance of everybody having the opportunity to cast a vote, contrasted to the soldiers near indifference. He sees no real point, and some of the islanders agree with him. Yet others go to great lengths to find her. They live in a remote rural area with unpaved roads, little electricity, and few cars, and Payami is using the various people that meet to show some of the views on the democratic process and life in general. One woman is young enough to wed but not old enough to vote. Some people disagree with the entire list of candidates; others feel it is wrong for women to vote at all. The negative experiences are enough to test the agents unwavering confidence in her beliefs, and her conviction is enough to cause the soldier to question his indifference.

Director Payami uses minimalism in Raye makhfi, letting a small spectrum of sociopolitical stances emerge naturally and often humorously. For instance, there is a traffic light in the middle of a desert, but it is stuck on red.

Payami has a pronounced use of long shots (avoiding close-ups in favor of vistas, framing people and sights at a distance) and long takes with shooting scenes continuously for three minutes or more. This directly impacts upon the mood in the film.

==Cast==
 
*Nassim Abdi as  Girl
*Cyrus Abidi as Soldier
*Youssef Habashi
*Farrokh Shojaii
*Gholbahar Janghali

==Awards==
In 2001, the film won:
*the FIPRESCI Prize - Special Mention at the London Film Festival
*the Special Jury Award at the São Paulo International Film Festival
*the Best New Director at the Valladolid International Film Festival
*the Netpac Award, the OCIC Award, the Pasinetti Award (Best film), the Special Directors Award and the UNICEF Award at the Venice Film Festival
:and was nominated for the Golden Spike (Valladolid IFF) and the Golden Lion (Venice FF).
In 2002, the film won
* the Jury Award at the Newport International Film Festival
* Special Mention at the Rotterdam International Film Festival
In 2003, it was nominated for the PFS Award (Democracy) at the Political Film Society, USA.

==References==
 

==External links==
* 
* 

 
 
 
 