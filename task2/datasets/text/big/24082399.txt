Vengo (film)
 
{{Infobox film
| name           = Vengo
| image          =
| caption        =
| director       = Tony Gatlif
| producer       = Tony Gatlif Luis Ángel Bellaba
| writer         = Tony Gatlif David Trueba
| starring       = Antonio Canales
| music          = Tony Gatlif
| cinematography = Thierry Pouget
| editing        = Pauline Dairou
| company        = Princes Films
| released       = 2000
| runtime        = 90 minutes
| country        = France
| language       = French Spanish
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
Vengo is a 2000 French film by Tony Gatlif. It is the passionate story of a blood feud that centers on Caco, a proud man who must fight for his familys honor and safety. An ode to the artistry and magic of flamenco dancing, Vengo is set against the compelling backdrop of two gypsy families locked in an age-old struggle for power.

==Cast and characters==
* Antonio Canales : Caco
* Orestes Villasan Rodriguez : Diego
* Antonio Dechent : Primo Alejandro
* Bobote : Habib
* Juan Luis Corrientes : Primo Tres

==External links==
* 

 

 
 
 
 
 


 