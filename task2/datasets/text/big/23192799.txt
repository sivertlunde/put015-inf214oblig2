Sandstorm (film)
 
{{Infobox film
| name           = Sandstorm
| image          = 
| caption        = 
| director       = Mohammed Lakhdar-Hamina
| producer       = 
| writer         = Mohammed Lakhdar-Hamina Yamina Bachir
| starring       = Nadir Benguedih
| music          = 
| cinematography = Youcef Sahraoui
| editing        = 
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = Algeria
| language       = French
| budget         = 
}}
 Best Foreign Language Film at the 55th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Nadir Benguedih
* Himmoud Brahimi
* Hadja
* Sabrina Hannach
* Merwan Lakhdar-Hamina
* M. Mahboub
* Albert Minski
* Leila Shenna
* Sissani
* Nadia Talbi

==See also==
* List of submissions to the 55th Academy Awards for Best Foreign Language Film
* List of Algerian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 