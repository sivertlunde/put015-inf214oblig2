Mapado
{{Infobox film 
| name        = Mapado
| image       = Mapado film poster.jpg
| caption     = Theatrical poster
| film name      = {{Film name
 | hangul      =  
 | rr          = Mapado 
 | mr          = Map‘ado}}
| director    = Choo Chang-min
| writer      = Jo Joong-hoon Yeo Woon-kye Kim Soo-mi Kim Hyeong-ja
| producer    =
| distributor = CJ Entertainment
| budget      =
| released    =  
| runtime     = 105 minutes
| country     = South Korea
| language    = Korean
}}
Mapado ( ) is a 2005 South Korean film directed by Choo Chang-min. 

==Plot==
A gangster and a corrupt police officer travel to the tiny remote island of Mapado to hunt down a young woman who has run off with a winning lottery ticket. Upon arriving, they discover that no one lives there except for five old women who have not once seen a man for 20 years. Both men soon experience a nightmare of hard labour and harassment.

==Spot==
"Do" can either mean "province" or "island" in Korean. Seom means island in the Korean language as well, although "do" is a Sino-Korean word used in name compound words, but "seom" standing alone. But it was not taken in the real islands area. Scenes of the film was from Dongbaek village of Yeonggwang, South Korea.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 
 