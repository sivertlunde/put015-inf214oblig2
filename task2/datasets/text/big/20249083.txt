Noir Drive
{{multiple issues|
 
 
}}
 

{{Infobox Film
| name           = Noir Drive
| image          =
| caption        = Original Theatrical Poster by Lawrence Wong
| director       = Benson Riddle
| producer       = Matthew Bristow
| writer         = Benson Riddle Steve Price
| music          = Geoffrey Russell
| cinematography = Judd Overton
| editing        = Joe Maurici
| sound design   = Phil Jeffers
| production design = Nic Buckle
| released       = 2008
| runtime        = 19 minutes 
| country        = Australia
| language       = English
}} Steve Price. Set in Sydney, Australia in 1952, it tells the story of Reilly (played by Passmore), a private detective, who gets shanghaied into a web of lies and deceit following the murder of a wealthy businessman. Previously missing, the sudden return of his beautiful yet mysterious widow (played by Berry) only complicates matters further.

Noir Drive premiered at the 2008 LA Shorts Fest. 

==Plot==

In Sydney in 1952 and returning from a month away, the private investigator Reilly finds his office has been taken over by the buildings janitor, Jerry Vane, who has been impersonating him in his absence. Roughing Vane over, Reilly discovers he has taken a missing persons case in which an insurance company is trying to locate someone named Alex Pleshette. However when Reilly presents himself to the insurance office to drop the case, the owner Max Arnett claims he never hired his agency to begin with. On the way out however, Arnetts receptionist inadvertently gives away the truth that Reilly was in fact hired.

Heading back to his office, Reilly is confronted by two thugs in an alley who beat him and tell him to lay off the Pleshette case. In his office a short time later, a beautiful young woman in black arrives. Presenting herself as Mrs. Pleshette, Reilly assumes she is Alex Pleshettes wife, however she curtly informs him that her husband is dead, and that she is actually Alexandra, better known as Alex Pleshette. She tells him to stop interfering as she is now no longer missing, and departs – leaving him even more confused.

Next day, Reilly finds out from his police detective friend Jack Bosley that Alex Pleshette witnessed the murder of her husband, a wealthy tycoon named Donald Pleshette who was at the time being investigated for company insurance fraud. However, she was knocked unconscious by the killer and as a result is unable to remember the event. Not long after this, she went missing and no one knew where she was, not even her lover Gabe Preston, her husbands disgruntled business partner. Reilly recognizes that Preston would thus have a motive for murder, but Bosley explains that he has a strong alibi.
 marital privilege, whereby spouses cannot testify against each other. They are then interrupted by the arrival of Gabe Preston. Alex introduces them before promptly leading Reilly out of the house. On his way out, Reilly explains to Alex that whoever hired him did so on the instruction of her husbands killer, worried that she may regain her memory at any time, and wanting her close by if she did. Reilly says he believes she knows more than she realises, but she replies "at times we all want to be kept in the dark".

Back at his office, Reilly looks through the photos taken by Vane, and in one sees Max Arnett meeting up with Gabe Preston. He promptly calls Bosley, who confirms that Prestons alibi was in fact Arnett. Reilly rushes off to the Pleshette mansion but Preston and Alex have left for good.

Far way on a tropical island resort, Preston and Alex lounge back on a beach. Alex however is unsettled – her memory then triggered by the pop of the champagne cork – and she realises it was Gabe Preston who murdered her husband. Looking down to reveal the wedding ring she now wears as Mrs. Alex Preston, it occurs to her that Preston now not only has access to the entire fortune, but that she is unable to testify against him because of marital privilege.

==Notes==
 

==External links==
* 
* 

 
 
 