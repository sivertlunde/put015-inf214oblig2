Kill Your Darlings (2006 film)
{{Infobox Film
| name           = Kill Your Darlings
| image          = Kill Your Darlings.jpg
| caption        = Promotional poster for Kill Your Darlings.
| director       = Björne Larson
| producer       = 
| writer         = Björne Larson  Benito Martinez
}} Johan Sandström. Swedish actors Stellan and Alexander Skarsgård, and Andreas Wilson from Oscar-nominated Evil (2003 film)|Evil) and some less famous US actors (Julie Benz, Greg Germann and Lolita Davidovitch).

The title seems to be a reference to the popular advice for writers, "Murder your darlings," commonly misattributed but written by Sir Arthur Quiller-Couch.

==Cast==
*Lolita Davidovich  as Lola
*Andreas Wilson  as Erik
*Fares Fares  as Omar
*Alexander Skarsgård  as Geert
*Julie Benz  as Katherine
*John Larroquette  as Dr. Bangley
*Greg Germann  as Stevens Benito Martinez  as Officer Jones John Savage  as Rock Terry Moore  as Ella Toscana
*Skye McCole Bartusiak  as Sunshine
*Michelle Bonilla  as Susan (as Michelle C. Bonilla)
*Scott Williamson  as Rob Wolfton
*Ashlyn Sanchez  as Meadow
*Michael Cutt  as Lou (as Michael J. Cutt)
*Stellan Skarsgård  as Eriks Father

==See also==
*Peter Hansen Gibson
*Gudrun Giddings

==References==
 

==External links==
* 
* 

 
 
 


 
 