The Return of a Man Called Horse
{{Infobox film
| name          = The Return of a Man Called Horse
| image         = return_of_a_man_called_horse_movie_poster.jpg
| caption       = Original theatrical poster
| director      = Irvin Kershner
| writer        = Jack DeWitt Dorothy M. Johnson
| starring      = Richard Harris
| producer      = Terry Morse Jr.
| music         = Laurence Rosenthal
| cinematography= Owen Roizman Michael Kahn
| country       = United States MGM (2003, DVD)
| released      =  
| runtime       = 129 min.
| language      = English
}} A Man Called Horse and it was followed by The Triumphs of a Man Called Horse in 1982.
 aristocrat who has become a member of a tribe of Lakota Sioux. 

==Plot==
Trappers with government support force the Yellow Hands Sioux off their sacred land. The Indians retreat, but await supernatural punishment to descend on their usurpers. Harris reprises his role as John Morgan, 8th Earl of Kildare, who had lived with the tribe for years and is known as Horse, leaves his English fiancé and estate and returns to America, where he discovers the Yellow Hand people have been largely massacred or put into slavery by the unscrupulous white traders and their Indian cohorts. The few survivors, including wise old Running Bull and stubborn old Elk Woman (Gale Sondergaard) have gone into the Badlands and been forced to eat their dogs. "Why did you return?" asks Elk Woman. "I had to come back," says Morgan. "I had to prove something to myself... there was an empty place in my soul. I could not forget." He finds the tribe dispirited, because of the actions of the trappers, and he begins to devise a strategy to overpower the trappers stronghold, convincing the Indians to take direct action. Soon even the Indian women and boys are assigned tasks to aid the assault on regaining their ancestral land.

==Cast==
*Richard Harris as John Morgan
*Gale Sondergaard as Elk Woman Geoffrey Lewis as Zenas
*William Lucking as Tom Gryce
*Jorge Luke as Running Bull
*Jorge Russek as Blacksmith
*Claudio Brook as Chemin De Fer
*Enrique Lucero as Raven
*Regino Herrera as Lame Wolf
*Pedro Damián as Standing Bear
*Humberto López as Thin Dog
*Alberto Mariscal as Red Cloud
*Eugenia Dolores as Brown Dove
*Patricia Reyes Spíndola as Gray Thorn
*Ana De Sade as Moon Star

==Filming==
Much of the film was shot in 1975 in South Dakota in the United States. Other scenes were filmed in the United Kingdom and Mexico. 

==Reception==
The film received mixed reviews on its release. Roger Ebert, while not highly critical of the film, noted that the film attempted to take itself too seriously and paid unnecessary attention to detail. According to Ebert "The film reveals its basic white-chauvinist bias, but it certainly seems to take itself seriously. Its of average length, but paced like an epic. There are four main movements in the plot: Return, Reconciliation, Revenge and Rebirth. If this seems a little thin for a two-hour movie, believe me, it is, even with all that portentous music trying to make it seem momentous."  , August 19, 1976, Retrieved on July 7, 2008  
 A Man Called Horse. Ebert commented that "What gets me is that initiation rite, which is repeated in this film in such grim and bloody detail youd think people didnt have enough of it the last time. First Morgan has his pectoral muscles pierced with knife blades. Then eagles talons are drawn through the wounds and tied to leather thongs. Then he hangs by the thongs until sufficiently purified. Youd think one ceremony like that would do the trick, without any booster shots." 

==DVD==
The Return of a Man Called Horse was released to DVD by MGM Home Video on April 1st, 2003 as a Region 1 widescreen DVD.

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 