The Girl in 419
{{Infobox film
| name           = The Girl in 419
| image          = The Girl in 419 poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Alexander Hall George Somnes
| producer       = B. P. Schulberg
| screenplay     = Allen Rivkin Manuel Seff P.J. Wolfson James Dunn Gloria Stuart David Manners William Harrigan Shirley Grey Jack La Rue Johnny Hines
| music          = John Leipold
| cinematography = Karl Struss
| editing        =
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 James Dunn, Gloria Stuart, David Manners, William Harrigan, Shirley Grey, Jack La Rue and Johnny Hines. The film was released on May 26, 1933, by Paramount Pictures.  {{cite web|url=http://www.nytimes.com/movie/review?res=9F06E7D6163AEF3ABC4851DFB3668388629EDE|title=Movie Review -
  The Girl in 419 - In an Emergency Hospital. - NYTimes.com|work=nytimes.com|accessdate=25 February 2015}} 
 
==Plot==
 

== Cast ==  James Dunn as Dr. Daniel French
*Gloria Stuart as Mary Dolan
*David Manners as Dr. Martin Nichols
*William Harrigan as Peter Lawton
*Shirley Grey as Nurse Irene Blaine
*Jack La Rue as Sammy
*Johnny Hines as Slug
*Vince Barnett as Otto Hoffer
*Kitty Kelly as Kitty 
*Edward Gargan as Lt. Babs Riley James Burke as Detective Jackson Clarence Wilson as Walter C. Horton
*Gertrude Short as Lucy
*Effie Ellsler as Mrs. Young
*Hal Price as Rankin

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 