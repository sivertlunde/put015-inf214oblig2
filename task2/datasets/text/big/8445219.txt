Ankush
{{Infobox film
| name           = Ankush
| image          = 
| caption        = 
| director       = N. Chandra
| producer       = N. Chandra Subhas R. Durgakar
| writer         = N. Chandra, Sayed Sultan
| screenplay     = 
| story          = 
| based on       =  
| starring       = Nana Patekar Nisha Singh Raja Bundela Arjun Chakraborty
| music          = Kuldeep Singh
| cinematography = H. Laxminarayan
| editing        = N. Chandra
| studio         = 
| distributor    = 
| released       =  
| runtime        = 149 minutes
| country        = India Hindi
| budget         = 
| gross          = 
}}

Ankush is a 1986 Hindi movie written, directed, edited and co-produced by N. Chandra starring Nana Patekar. Made at a budget of Rs 12 lakh, the film and grossed Rs 95 lakh to become surprise hit of 1986, the year when many blockbusters failed.  

==Story== law of the land. The movie has a tragic end with all four getting capital punishment for doing what they thought was correct, specially so when law of land fails to book the criminals for their wrongdoings.

==Production==

Chandra started his career with Gulzar in 1971, and later also worked as an editor with him. Influenced by Gulzars Mere Apne (1971) and bringing in his experiences growing up in Mumbai, Chandra wrote a story of four frustrated unemployed men who roam the streets of Bombay. He even based the character Ravindra upon the role played by Vinod Khanna in the original. The role of Ravindra was written with leading actor of Marathi cinema, Ravindra Mahajani in mind, however later when Chandra couldnt afford, however Nana Patekar was eager to do the same role and signed on for Rs. 10,000.    

==Reception==
Although a small budget film with then unknown actors film was a big hit in Mumbai (then Bombay) as it  realistically depicted social conditions prevailing then in the aftermath of famous Mumbai (then Bombay) cotton textile mills strike which rendered thousands jobless. The film stars a very young Nana Patekar, and has an excellent and memorable Bhajan of Indian Cinema : Itni Shakti Humen de na daata by Ghanshyam Wasvani.

Writer, Editor, Director and co-producer N. Chandra|N.Chandra earned both popular as also critical acclaim for this film.  N Chandra scored a hat-trick at the box office with his first three films,  Ankush (1986), Pratighaat (1987) and Tezaab (1988).    Ankush has earned the status of a cult classic over time, and the ending scene which symbolizes their hanging in front of the India Gate has received much attention. The film is one of the very first realistic depictions of inner city adolescents growing up in an atmosphere of social and economic deprivation.

==Remake==
The film was remade in Tamil as Kavithai Paada Neramillai and in Kannada as Ravana Rajya.

==Cast==

*Nana Patekar as Ravindra Kelkar Ravi
*Nisha Singh as Anita
* Madan Jain as Shashi
*Arjun Chakraborty as Arjun
*Mahavir Shah as Gupta
*Raja Bundela as Saxena
* Rabia Amin as Manda
* Gajanan Bangera as Shashis elder brother
*Ashalata Wabgaonkar as Anitas  Grand mother
* Suhas Palshikar

== References ==
 

==External links==
*  

 
 
 
 
 
 
 