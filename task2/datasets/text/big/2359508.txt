Living in Missouri
{{Infobox film
| name = Living In Missouri |
 image = Living_In_Missouri_Poster.jpg|
 caption = A film poster for the DVD Special Edition of Living In Missouri. | Shaun Peterson |
 producer = Shaun Peterson Connor Ratliff | Connor Ratliff |
 starring = Ian McConnel Connor Ratliff Christina Puzzo Holmes Osborne |
distributor = Film Threat DVD |
 released = October 12, 2001 (Austin Film Festival)|
 runtime = 90 minutes |
 country = United States |
 language = English |
  awards = Best Feature Film (Seattle Underground Film Festival), Best Acting Ensemble (Ashland Independent Film Festival)|
 budget = $7,500 |
}}

Living In Missouri (2001) is an award-winning indie film comedy about an emotionally maladjusted love triangle.

The film, directed by Shaun Peterson, made its world premiere at the Austin Film Festival in 2001, and has spent several years playing festivals and special engagements. The cast includes Ian McConnel, screenwriter Connor Ratliff, and veteran character actor Holmes Osborne (Donnie Darko, Affliction (1997 movie)|Affliction, That Thing You Do!).

==Plot synopsis==
Living In Missouri is a Comedy Of Manners which tells the story of Ryan, Amy, and Todd, whose humdrum Midwestern lives are starting to come apart at the seams. Personal betrayals abound when childhood friendships, broken-down marriages, and long-repressed desires come into conflict over the course of one tumultuous Missouri autumn.

Ryan and Todd have been best friends since the 7th grade, and time has not been kind to either of them. Ryan is now married to Amy, with two young children and a 9-to-5 job he hates. The sexually frustrated Todd still lives in the basement of his parents house, works in a video store, and secretly envies Ryans married life. Meanwhile, Amy is stuck in the middle, balancing career and family with almost no help from Ryan, whose selfish behavior is quickly destroying whats left of their marriage.

When Amy secretly turns to Todd for a shoulder to cry on, the two of them begin meeting regularly to discuss her marital woes. Todd misinterprets Amys attention for love, and believes that they are actually "having an affair," which leads to a series of painful misunderstandings, ending in disaster.

Living In Missouri is a challenging, darkly comic film, shot on location in Missouri and Kansas. Boasting a colorful cast of supporting characters, the film veers from dark humor to intense drama, at each turn authentically capturing the bizarre spirit of the Suburban Midwest.

==Production==

===Writing The Film===
Living In Missouri was written while Connor Ratliff was actually living in Great Britain, where he was an acting student at the  Liverpool Institute for Performing Arts.

The screenplay was developed using some unorthodox strategies: during the writing process, the main characters of Ryan, Todd, and Amy actually had “real” identities on the Internet. In early 1998, Ratliff created a website,  , which featured journal entries from the fictional Ryan, who described his day-to-day activities and often wrote at length about his wife and best friend. The site also contained often-erroneous predictions and rumors relating to the world of science fiction movies.

The site attracted thousands of curious and often irate sci-fi fans who often took issue with “Ryan” and his renegade brand of web journalism. In a bizarre turn of events, several media outlets were fooled by the site, and many of Ryan’s rumors began to appear in print, most prominently in SPIN magazine.

Ratliff used the website as a kind of “character workshop” to help develop the characters while fleshing out the screenplay, and several key events in the film first appeared as entries in Ryan’s web journal. When the first draft of the screenplay was completed, he contacted director Shaun Peterson, whom he had been friends with since high school. Peterson put his own self-penned project, Climax, Kansas, on hold to helm Living In Missouri.

===Filming In Missouri===
  Hoodlum Priest (directed by a young Irvin Kershner), so the creative team was dealing with a community that was not accustomed to dealing with the rigors of independent movie-making.

For five weeks, the crew remained in one small, furnished house that served as living quarters, production office, and a primary location for the movie. There were the usual problems associated with low-budget cinema—a frenzied shooting schedule, unforeseen technical glitches, sheer physical exhaustion—and a few cases of bad luck.

With several key scenes to be filmed in “Ryan and Amy’s house,” the crew returned one day to find all of the dining room furniture had suddenly disappeared. It turns out that while the filmmakers were out on location, the landlords had entered the premises to remove some of their belongings, leaving an empty space in the middle of Ryan and Amy’s living room area. Panic ensued, and the remaining scenes had to be cleverly re-imagined in order to avoid massive continuity errors.

On the whole, however, local businesses and townspeople were curious and helpful, if somewhat bewildered by the scope of the enterprise. Aside from the occasional visit from a local hoping to land a speaking role, most people remained distant and allowed filming to go on without interference.

Throughout the shoot, the weather remained consistent, if unexpectedly sunny for the middle of November. Filming wrapped in early December, just days before a storm front moved in, bringing heavy snowfall.

===Editing In L.A.===
When Ratliff returned to England shortly after filming, Peterson was left to finish the film with minimal resources and almost no money.

Arriving in L.A. with 40 hours of footage, Peterson began to assemble the film using a home computer system. While new technology allowed the feature to be completed without the pressure of a high-priced editing suite, Peterson and editor Ford were faced with technical challenges dealing with bugs in the system. Simple tasks often took days as they dealt with malfunctioning software.

After months of problem solving, the film finally began to take shape, developing into a drama, mixing dark comedy with scenes of sadness.

==Film Festivals==
Living In Missouri was an Official Selection at the 2001 Austin Film Festival, where it was one of only 11 films in competition and was also chosen to be the subject of a panel discussion with cast & crew.
 Grand Canyon, The Big Chill; writer of Raiders of the Lost Ark and The Empire Strikes Back) and Gary David Goldberg Family Ties, Brooklyn Bridge).

The panel discussion drew a curious and appreciative crowd that was particularly interested in learning more about the process of making a low-budget feature film in the era of digital video. Cast and crew members shared anecdotes about the shooting process, and gave provided a little bit of technical advice for any prospective filmmakers thinking about how to get started.

The Q&A was followed by a blitz of publicity stunts—travelling concession stands offering FREE SNOWCONES served by the "real" characters from the film, FREE DONUTS in the morning, and free Living In Missouri trading cards were given to everyone who attended the screenings. The stunts caught peoples attention, and sparked a range of reactions—one Hollywood producer called the campaign "ingenious," while the security officers at the Omni hotel dubbed it "a public disturbance" and ordered the unauthorized snow cone stand removed from the lobby immediately.

Following the two successful Austin screenings, the film played at two other festivals in the same month: the Seattle Underground Film Festival, where it won BEST FEATURE FILM, and the Ashland Independent Film Festival, where it won the award for BEST ACTING ENSEMBLE. It went on to win a total of 10 awards at festivals in San Francisco, New York, L.A., Rhode Island, and Kansas City.

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 