Les jeux sont faits (film)
 
{{Infobox film
| name           = Les jeux sont faits
| image          = 
| caption        = 
| director       = Jean Delannoy
| producer       = Louis Wipf
| writer         = Jacques-Laurent Bost Jean Delannoy Jean-Paul Sartre
| starring       = Micheline Presle
| music          = 
| cinematography = Christian Matras
| editing        = Henri Taverna
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = France
| language       = French
| budget         = 
}}
 same name. It was entered into the 1947 Cannes Film Festival.     

==Plot==
 
In a country very similar to Occupied France, two people are murdered at the same moment. Ève is poisoned by her influential husband, who wants her money and her naïve younger sister. Pierre, a worker and a leader of the resistance, is shot by an informer. Meeting in the afterlife, the two fall in love. As they were fated to do so, but prevented by others, they are granted 24 hours back on earth. Their first mission is to do a favour to a dead man who was worried about his young daughter. Then, after brief sex, they address unfinished business. Ève confronts her evil husband and tries to convince her sister of his treachery. Pierre goes to a meeting of resisters and tries to convince them that their organisation is compromised by traitors. The 24 hours are up and most of the time was spent not on enjoying and deepening their relationship, which was often edgy, but on efforts to help others. Back in the afterlife they agree to part.

==Cast==
* Micheline Presle - Eve Charlier
* Marcello Pagliero - Pierre Dumaine
* Marguerite Moreno - La dame de lau-delà
* Charles Dullin - Le marquis
* Fernand Fabre - André Charlier
* Jacques Erwin - Jean Aguerra
* Colette Ripert - Lucette
* Marcel Mouloudji - Lucien Derjeu
* Guy Decomble - Poulain
* Howard Vernon - Le chef milicien
* Jim Gérald - Renaudel
* Renaud Mary - Un milicien
* André Carnège - Le ministre de la justice
* Andrée Ducret - Madame Astin
* Robert Dalban - Georges

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 