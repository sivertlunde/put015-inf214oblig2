City Under Siege (film)
 
 
{{Infobox film
| name = City Under Siege
| image = City Under Siege poster.jpg
| caption = 
| film name = {{Film name| traditional = 全城戒備
| simplified = 全城戒备}}
| producer = Alvin Lam Benny Chan
| writer =  
| starring =  
| music = Anthony Chue
| editing = Benny Chan Chan Sing Yan
| cinematography = Fletcher Poon Tony Cheung Chan Kwok Hung
| studio = Universe Entertainment Beijing Enlight Pictures Limited Shanxi Film Studio Shenzhen Film Studio Guangzhou City Ying Ming Culture Communication 
| distributor = Universe Films Distribution
| released =  
| runtime = 110 minutes
| country = Hong Kong
| language = Cantonese Mandarin Japanese
| budget = $10 million USD   
| gross = US$13,851,248 
}} Benny Chan. The film follows a group of circus performers who goes on a rampage after an accidental exposure to chemical gas left by the Japanese army in World War II, granting them superhuman abilities.  

==Plot==
Sunny is the orphaned child of a former knife throwing master. After his fathers death, he is taken in by his uncle, who allows him to perform as a clown, since he did not inherit his fathers knife throwing skills. Sunny still wants to be a knife thrower, though. He is tormented by his cousins, who are all performers in his uncles troupe.

One day, Sunny overhears a plan the rest of the troupe a making to explore a cave, rumored to be filled with gold. Instead of killing Sunny, they use him as the point man for their expedition. They find crates filled with capsules and force Sunny to open them. Inside the first are numerous plates of gold. They then try to kill Sunny, and continue opening the rest of the capsules, which end up containing experimental gas which transforms them into superhuman monsters.

==Cast==
* Aaron Kwok as Sunny, an ordinary circus clown who was accidentally exposed to chemical gas turns into a mutant with superhuman abilities. 
* Shu Qi as Angel   
* Collin Chou as Zhang Dachu    
* Zhang Jingchu as Xiuhua.  Wu Jing as Sun Hao. 

==Release==
The film was released in Hong Kong on 12 August 2010. 

==References==
 

==External links==
* 
* 
*  at the Hong Kong Cinemagic

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 