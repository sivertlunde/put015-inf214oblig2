Livin' tha Life

Livin tha Life is a 2003 film produced in Compton, CA by Central Avenue Filmworks. The Film was directed by Joe Brown and distributed by Artisan Entertainment.

==Synopsis==
The basis of the film is a day in the life of two friends, Jamal and Peanut in through their daily routine in Compton. When the two accidentally apprehend a thief found breaking into Jamal’s house, the thief then drops dead in front of them. The two place him in the trunk and try and continue on with their day.

==Cast==
* Shawn Harris - Killer D
* Archie Howard - Big Man
* Hurricane - Barber
* Jarell Jackson - Jamal
* Pepper Jackson - The Kid
* Rodney Perry - Uncle Fred
* Kaluha Richardson - Girlfriend
* Edward D. Smith - Peanut
* Stixx - Burglar
* Zai Wilburn - Lil Mad Dog  

==Other Info== The Game the album of the same name.
* The film had a four hundred dollars shooting budget.
* This film has been described as a blend of the films "Friday (1995 film)" and "Weekend at Bernies.
* The clip where the Kid demands a Japanese lady at a liquor store for a refund has since become a viral YouTube video.

==References==
 

== External links ==
*  
* http://movies.nytimes.com/movie/281452/Livin-tha-Life/overview
* http://movies.yahoo.com/movie/1808466737/info

 
 