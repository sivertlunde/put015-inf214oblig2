Love U...Mr. Kalakaar!
{{Infobox film
| name           = Love U... Mr. Kalakaar!
| image          = Love-u-mr-kalakaar.jpg
| caption        = Theatrical release poster
| director       = S. Manasvi
| producer       = Ajit Kumar Barjatya Kamal Kumar Barjatya Rajkumar Barjatya
| writer         = 
| screenplay     = S. Manasvi
| story          = S. Manasvi
| based on       =  
| starring       = Tusshar Kapoor Amrita Rao
| music          = Sandesh Shandilya
| cinematography = Saurabh Vishwkarma
| editing        = Navnita Sen Datta
| studio         = 
| distributor    = Rajshri Productions
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Love U...Mr. Kalakaar! (English: Love you, Mr Artist) is a 2011 Bollywood film. The film is directed by debutant S. Manasvi, who is an alumnus of Film and Television Institute of India. It is the first film in which Tusshar Kapoor and Amrita Rao appear together.  It was released on 13 May 2011.

==Plot==
Tusshar Kapoor plays Sahil, an artist who draws cartoons. He falls in love with Ritu (Amrita Rao), the daughter of a businessman, for whom Sahil had designed a mascot. The story moves on and both fall in love. Her father hates artists, and he decides she will not marry him. Upon her compulsion, her father decided to hire Sahil as the managing director of his company for three months. He has to earn profits in those three months to marry his daughter. Company politics come into play and Sahil falls short of his goals. But eventually his dream of becoming a cartoon strip-writer is realized when a newspaper decides to publish his cartoon "Office Space."

==Cast==

* Tusshar Kapoor as Sahil 
* Amrita Rao as Ritu
* Ram Kapoor as Ritus Father
* Madhoo as Ritus Aunt
* Prem Chopra as Ritus Grandfather
* Jai Kalra as Manohar
* Kiran Kumar as Jayant Singh Chauhan
* Snigdha Akolkar as Charu
* Prashant Ranyal as Aman (Saumya Daan as the dubbing voice)
* Yateen Karyekar as Israni
* Kunal Kumar as Hiten

==Reception==

Love U...Mr. Kalakaar! received mixed reviews from Indian critics. Nikhat Kazmi of the Times of India gave it 2.5 out of five stars and wrote in her review, "The film does have a sweetness that grows on you and seasoned actors like Ram Kapoor and Madhoo add a dignity to their roles. The love birds Tusshar and Amrita are in sync with each other, but the film is so predictable and so long, it loses impact. Surely, a bit of reinvention is the need of the hour for Rajshri Inc too. After all, its a fast and furious world we live in today.".  Bollywood portal FilmiTadka rated it 3 out of 5 stars and said, "The story is weak at the beginning but picks up pace in the second half, overall it’s a decent first attempt by the director."  Preeti Arora of Rediff found the movie unimpressive and wrote, "The Rajshri fans who thrive on old world love stories might just enjoy this. Simple and uncomplicated crafted in true Barjatya fashion. Or if you like the idea of Tusshar Kapoor in a lead romantic role. There arent too many of these films being made these days. Thankfully!"  Subhash K Jha wrote, "It is Harindranath Chattopadhyay who said, Its very difficult to be simple. It takes a lot of guts to make a film as simple, honest and transparent as Love U...Mr Kalaakar in this day and age when Munni and Sheila are happily letting it all hang out and Raginis love is lost in the lust and greed of an MMS mess." 

==Controversies==

Mongoose hair paintbrushes used in this film by the artist is a crime against the Wild Life Protection Act, 1972. 

==References==
 

== External links ==

*  
*  
*   at Bollywood Hungama

 
 
 
 
 