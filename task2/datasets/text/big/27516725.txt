The Flood (1994 film)
{{Infobox film
| name           = The Flood
| image          = 
| caption        = 
| director       = Igor Minaiev
| producer       = Jérôme Paillard Daniel Toscan du Plantier
| writer         = Igor Minaiev Jacques Baynac
| starring       = Isabelle Huppert
| music          = 
| cinematography = Vladimir Pankov
| editing        = 
| distributor    = 
| released       = August 1994
| runtime        = 99 minutes
| country        = France Russia
| language       = French
| budget         = 
}}

The Flood ( ) is a 1994 French-Russian crime film directed by Igor Minaiev and starring Isabelle Huppert.   

==Cast==
* Isabelle Huppert - Sofia
* Boris Nevzorov - Trofim Svetlana Kryuchkova - Pelagiya
* Mariya Lipkina - Ganka (as Masha Lipkina) Vladimir Kuznetsov
* Mikhail Pyam
* Andrei Tolubeyev
* Fyodor Valikov
* Natalya Yegorova
* Aleksey Zaytsev

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 