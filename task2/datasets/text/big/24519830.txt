The Beat (1988 film)
{{multiple issues|
 
 
}}
{{Infobox film
| name         = The Beat
| image        =
| caption      =
| writer       = Paul Mones David Jacobson David McCarthy John Savage Markus Flanagan Paul Dillon
| director     = Paul Mones Nick Wechsler
| cinematography = Tom DiCillo
| music        = Carter Burwell
| editing      = Elizabeth Kling
| studio       =
| distributor  = Vestron Pictures
| released     = June 3, 1988
| runtime      = 98 min.
| country      = United States English
| budget       =
| gross        =
| preceded_by  =
| followed_by  =
}}

The Beat is a film released in the U.S. in 1988.

==Plot summary==

A new kid moves into a tough neighborhood controlled by gangs, and tries to teach them poetry.

==Release==

The movie was first shown at The Cannes Film Festival in 1987 and released into U.S. theaters the next year. In 1989, Vestron Video released the movie on videocassette. The movie has never been released on DVD, and as of December 30, 2009, Lions Gate has not yet announced any plans for a DVD release.

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 

 
 