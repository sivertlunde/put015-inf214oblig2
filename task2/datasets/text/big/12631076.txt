Woman Without a Face
 
{{Infobox film
| name           = Woman Without a Face
| image          =
| caption        =
| director       = Gustaf Molander
| producer       =
| writer         = Ingmar Bergman Gustaf Molander
| starring       = Alf Kjellin Anita Björk Gunn Wållgren
| music          =
| cinematography = Åke Dahlqvist
| editing        = Oscar Rosander
| distributor    =
| released       = 16 September 1947
| runtime        = 102 minutes
| country        = Sweden
| language       = Swedish
| budget         =
| gross          =
}}
Woman Without a Face ( ) is a 1947 Swedish drama film directed by Gustaf Molander and written by Ingmar Bergman.

==Cast==
* Alf Kjellin - Martin Grandé
* Anita Björk - Frida Grandé
* Gunn Wållgren - Rut Köhler
* Stig Olin - Ragnar Ekberg
* Olof Winnerstrand - Mr. Grandé, Martins father
* Linnéa Hillberg - Mrs. Grandé, Martins mother
* Georg Funkquist - Victor
* Marianne Löfgren - Charlotte, Ruts mother
* Åke Grönberg - Sam Svensson
* Sif Ruud - Magda Svensson

==External links==
*  

 

 
 
 
 
 
 
 
 