The Divine Move
{{Infobox film name           = The Divine Move image          =  director       = Jo Bum-gu producer       = Park Mae-hee   Yu Jeong-hun   Hwang Geun-ha writer         = Yu Seong-hyeop starring       = Jung Woo-sung   Lee Beom-soo  music          = Chang Hyuk-jin cinematography = Kim Dong-young editing        = Shin Min-kyung distributor    = Showbox/Mediaplex released       =   runtime        = 117 minutes country        = South Korea language       = Korean gross          = 
}} noir film about a former baduk players quest for revenge.    
 Go in the West) which often turns what was a losing or close game into a winning effort at the most crucial moment.

==Plot== baduk player framed for the murder of his own brother and locked up in prison. He vows revenge and trains ferociously. After serving his seven-year sentence, he gets in touch with his brothers former associate "Tricks," hermit and blind master player "The Lord," and skillful junkyard owner Mok-su; together, they begin formulating a plan to get back at Sal-soo and his men. Tae-seok slowly penetrates Sal-soos inner circle and his gambling joint, and eliminates Sal-soos men one by one. But Sal-soo discovers Tae-seoks true identity and engages him in one final game that will seal the fates of the two men involved.

==Cast==
 
*Jung Woo-sung as Tae-seok 
*Lee Beom-soo as Sal-soo
*Ahn Sung-ki as Joo-nim ("The Lord")
*Kim In-kwon as Kkong-soo ("Tricks")
*Lee Si-young as Bae-kkob ("Belly button") 
*Ahn Gil-kang as Carpenter Heo 
*Lee Do-kyeong as Master Wang
*Choi Jin-hyuk as Seon-soo ("Player") 
*Jung Hae-kyun as Adari
*Ahn Seo-hyun as Ryang-ryang
*Kim Myung-soo as Tae-seoks older brother
*Hwang Choon-ha as Hunchbacked minion
*Lee Il-seop as Master Noh
*Kim Se-dong as Master Lee 
*Kim Joo-myeong as Chinese top 
*Lee Yong-nyeo as "Open tail"
*Yoo Soon-cheol as 70-year old boss 
*Hong Seong-deok as Professional cutter 
*Park Ji-hoon as Acting general 
*Yoon Hee-cheol as Elderly senior 
*Kim So-jin as Young-sook 
*Yoo Jae-sang as Tae-seoks nephew
*Choi Il-hwa as Mob leader 
*Kim Hong-pa as Warden
*Kwon Tae-won as Soft touch president 
*Bae Seong-woo as Mahjong man 
 

==Box office==
Since opening in South Korea on July 3, 2014, the film has grossed   ( ) on 3.5 million admissions.     

==International release==
The Divine Move received a limited theatrical release in the  s lineup.  

==Awards and nominations==
{| class="wikitable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan=8| 2014
| rowspan=4|   51st Grand Bell Awards  
| Best Actor
| Jung Woo-sung
|  
|-
| Best Supporting Actor
| Kim In-kwon
|  
|-
| Best New Actor
| Choi Jin-hyuk
|  
|-
| Best Editing
| Shin Min-kyung
|  
|-
| rowspan=4|   35th Blue Dragon Film Awards
| Best Actor
| Jung Woo-sung
|  
|-
| Best New Actor
| Choi Jin-hyuk
|  
|-
| Best Editing
| Shin Min-kyung
|  
|-
| Technical Award
| Choi Bong-rok (martial arts)
|  
|-
|}

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 
 