Preminchukundam Raa
{{Infobox film
| name           = Preminchukundam Raa
| image          = Preminchukundam_Raa.jpg
| caption        = 
| director       = Jayanth C. Paranjee
| producer       = Daggubati Suresh Babu|D.Suresh Babu D. Rama Naidu  
| story          = Deenraj
| screenplay     = Jayanth C. Paranjee
| writer         = Paruchuri Brothers   Venkatesh Anjala Zaveri  
| music          = Mahesh Mahadevan   Mani Sharma  
| cinematography = K Ravendra Babu
| editing        = Marthand K. Venkatesh  
| studio         = Suresh Productions
| distributor    =
| released       =   
| runtime        = 2:31:03
| country        = India
| language       = Telugu
| budget =
| gross =
}}
 Venkatesh and Anjala Zaveri played the lead roles and music composed by Mahesh Mahadevan. The film recorded as Industry Hit at the box-office.    

==Plot==
Veerabhadraiah (Jaya Prakash Reddy) a powerful factionist in Rayalaseema who is against love marriages. Sivudu (Srihari) his henchmen, Veerabhadraiahs word is a ordinance to him, Sivudu says yes to Veerabhadraiahs each and every deed, even he killed his own father. Reddappa (K.Ashok Kumar) is Veerabhadraiahs opponent, both of them having family rivalry and want to eliminate each other.
 Chandra Mohan). Giri falls in love with Kaveri, since both of them are childhood friends she too falls in love with Giri. At the same time Giris parents fixes his engagement. After knowing this Kaveri starts hating him. Giri comes back to Hyderabad cancels the engagement and goes back to Kurnool but Kaveri shows aversion towards him, after sometime she realizes his true love towards her and starts re-loving him.

Now, there is twist Kaveris father is none other than Veerabhadraiah. He comes to know about their affair and he resents and threatens Giris family. Immediately, Giri also reacts on Veerabhadraiah and takes Kaveri with him. Veerabhadraiah sends his goons along with Sivudu in their chase. Giri & Kaveri reach Hyderabad, his parents reject their love because they are afraid of Veerabhadraiah, even SRK Master also curses them. Giri takes Kaveri, leaves the house and with his friends help makes their marriage arrangements. At the same time Sivudu & gang attack them. Giri is injured and Kaveri is separated from him and accidentally reaches SRKs house, he also secures their love but forcefully Sivudu takes her back to Rayalaseema.

Simultaneously, Chakrapani & Vaani arrives to Hyderabad, they sees injured Giri in railway station and joins him in hospital. Looking at his condition Giris parents decides to talk to Veerabhadraiah and goes to Rayalaseema, he looks down and keeps them in house arrest. Giri comes to conscious and reaches Rayalaseema. At the same time Reddappa and his gang attack Sivudu and his family, Giri saves them and his words completely changes Sivudu. Finally, Giri protects his parents and elope with Kaveri, while they are escaping Veerabhadraiah tries to kill them, Sivudu obstructs his way and says that he is not correct which makes him to realize and Giri & Kaveri tied in.

==Cast==
{{columns-list|2| Venkatesh as Giri
*Anjala Zaveri as Kaveri
*Jaya Prakash Reddy as Veerabhadraiah
*Srihari as Sivudu Chandra Mohan as Chakrapani
*Ahuti Prasad as Keshava Rao
*Paruchuri Venkateswara Rao as SRK Master
*Gokkina Rama Rao as Sivudus father
*Raghunath Reddy as Giris father
*K.Ashok Kumar as Reddappa
*Benerjee as Reddappas henchmen Jeeva as Samba
*Babu Mohan as Kavi Venu Madhav as Venu
*Gundu Hanumantha Rao as Paper Boy
*Uttej as Imran Annapurna as Giris mother
*Sudha as Vaani
*Rama Prabha as Bamma
*Rajitha as Indira
*Bangalore Padma as Satyavathi
*Krishnaveni 
*Indu Anand as SRKs wife
*Banda Jyothi 
*Kalpana Rai as Damayanthi
*Master Anand Vardhan as Chinnu
*Baby Niharika as Pinky
}}

==Soundtrack==
{{Infobox album
| Name        = Preminchukundam Raa
| Tagline     = 
| Type        = film Mahesh
| Cover       = 
| Released    = 1997
| Recorded    = 
| Genre       = Soundtrack
| Length      = 28:27
| Label       = Aditya Music Mahesh
| Reviews     =
| Last album  = 
| This album  =
| Next album  = 
}}

Music composed by Mahesh Mahadevan, re-recording by Mani Sharma. Music released on Aditya Music. 
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 28:27
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Alachudu Premalokam	 Sirivennela Seetarama Sastry  SP Balu, Chitra
| length1 = 4:06

| title2  = Surya Keeritam 
| lyrics2 = Bhuvanachandra
| extra2  = SP Balu, Anuradha Sriram
| length2 = 5:34

| title3  = Meghale Thakindi 
| lyrics3 = Sirivennela Seetarama Sastry
| extra3  = SP Balu,Chitra
| length3 = 4:21

| title4  = Sambarala
| lyrics4 = Bhuvanachandra
| extra4  = SP Balu,Chitra,Sangeeth Sajith
| length4 = 5:31

| title5  = Pellikala Vachesindhe
| lyrics5 = Sirivennela Seetarama Sastry
| extra5  = Mano (singer)|Mano,Chitra, Swarnalatha
| length5 = 4:12

| title6  = O Panaipothundi Babu Chandrabose
| extra6  = Mano
| length6 = 4:43
}}

==Box-office==
It was the first Telugu movie to run 100 days in more than 50 centres and it ran for 100 days in 57 centres. 

== References ==
 

==External links==
* 

 

 
 
 