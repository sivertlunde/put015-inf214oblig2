Rosa Morena (film)
{{Infobox film
| name = Rosa Morena
| image = Rosa Morena (film).jpg
| director = Carlos Augusto de Oliveira
| producer = Hank Levine Ivan Teixeira Thomas Gammeltoft
| writer = Carlos Augusto de Oliveira  Morten Kirkskov
| starring = Anders W. Berthelsen Bárbara Garcia David Dencik Vivianne Pasmanter Pablo Rodrigues Iben Hjejle
| music = Frithjof Toksvig
| cinematography = Philippe Kress
| editing = 
| studio = Fine & Mellow Productions  Ginga Eleven
| distributor = Vinny Filmes
| released =  
| runtime        = 95 minutes 
| country = Brazil  Denmark 
| language = Portuguese Danish English
| budget = 
}}

Rosa Morena is a 2010 Danish-Brazilian drama film directed by Carlos Augusto de Oliveira. The film premiered at the 2010 São Paulo International Film Festival.

== Plot ==
Unable to adopt a child in Denmark, Thomas, a 42-year-old architect, travels to Brazil to try to realize the dream of being a father.  In São Paulo, he meets Jakob, an old Danish friend who introduces him to Maria, a humble young woman who is pregnant from her third child. She enters into an agreement with Thomas to donate the baby in exchange for better living conditions. Thomas moves to the house of Maria and their relationship evolves in unexpected ways. 

== References ==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 

 
 