Bugs and Thugs
{{Infobox Hollywood cartoon
| series            = Looney Tunes (Bugs Bunny/Rocky and Mugsy)
| image             = BugsandThugs Lobby Card.png
| caption           = Lobby card
| director          = Friz Freleng|I. Freleng
| story_artist      = Warren Foster Manuel Perez Arthur Davis
| layout_artist     = Hawley Pratt
| background_artist = Irv Wyner
| voice_actor       = Mel Blanc
| musician          = Milt Franklyn
| producer          = Edward Selzer
| studio            = Warner Bros. Cartoons
| distributor       = Warner Bros. Pictures The Vitaphone Corporation
| release_date      = March 13, 1954 (USA)
| color_process     = Technicolor
| runtime           = 7 minutes
| movie_language    = English
}} animated short film in the Looney Tunes series produced by Warner Bros. Cartoons, Inc.

It features Bugs Bunny with Rocky and Mugsy. The film is a semi-remake of Racketeer Rabbit, with a similar oven gag. It was directed by Friz Freleng.

== Plot ==
It begins with Bugs emerging from his hole in a city park, reading the newspaper on his way to the nearest bank, for a withdrawal from his personal depository of carrots. He reads that "Rabbit Season Opens Today" and comments on his pleasure of living in a "more secure" urban environment (miraculously avoiding heavy traffic crossing the street while reading the paper).
 Carson City is the capital of Nevada, uh, George Washington was the first President." Rocky responds: "This guy knows too much, Mugsy. Well take him for a ride." When Bugs becomes too gabby during the ride, Rocky points his gun at him and tells him to shut up.  Bugs doesnt stop, so Rocky tells him to "shut up shuttin up".
 nickel from Acme - straight 8 - overhead valves - with California license plates!"), but gets pulled out of the telephone booth and they escape. The gag is that Bugs still holding on to the telephone – and a policeman with a thick Irish accent is pulled out of the telephone wire onto the road ("Operator – weve been disconnected! ooh-hoo...").

Soon, Rocky and Mugsys car stops in front of a railroad grade crossing protected by a swinging Wigwag (railroad)|"wigwag" signal warning of the approach of a train. Rocky tells Bugs to let them know when its clear. Bugs tells them to go and the oncoming train proceeds to smash into the car with Rocky and Mugsy still inside.

Bugs is soon forced to fix the car at gunpoint by Rocky. Mugsy was told to button his lip and Mugsy literally buttons his lip.   Bugs repairs everything except one tire and says that theyre stuck. Rocky tells Bugs that he is the only one stuck and forces Bugs at gunpoint to run alongside the car while holding the right front axle (Rocky tells Mugsy to take the "scenic route").  Soon, they arrive at their cliff house hideout.  When Rocky tells Mugsy to take him into the back room and "let Bugs have it", Bugs easily convinces Mugsy that Rocky meant for him to give Bugs the gun ("Come, come, you heard what the boss said: Let me have it!").  When Mugsy relents, he gets shot by Bugs and stumbles back into the front room and tells Rocky "I let him have it boss, just like you said," and falls unconscious on top of Rocky, who quickly punches him off.
 Private Eyeball Forgers Found, Counterfeiters Caught, and Chiselers Chiseled."

== See also ==
* List of Bugs Bunny cartoons

== External links ==
*  

 
{{succession box 
| before= Captain Hareblower  Bugs Bunny Cartoons 
| years= 1954 
| after= No Parking Hare
}}
 

 
 
 
 
 