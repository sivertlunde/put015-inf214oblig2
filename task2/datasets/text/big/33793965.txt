Just Mickey
 
{{Infobox Hollywood cartoon|
| cartoon_name = Fiddling Around / Just Mickey
| series = Mickey Mouse
| image =
| caption =
| director =Walt Disney
| story_artist = 
| animator = 
| voice_actor = Walt Disney
| musician =
| producer = Walt Disney Walt Disney Productions
| distributor = Columbia Pictures
| release_date =   (USA)
| color_process = Black-and-white
| runtime = 6:50
| movie_language = English
| preceded_by = The Barnyard Concert (1930) The Cactus Kid (1930)
}} Walt Disney Productions and released by Columbia Pictures. The film shows Mickey Mouse playing a violin on stage as a one-person show; as indicated by the title, Mickey is the only character to appear in the film. It was directed by Walt Disney and is the first Mickey cartoon not animated by Ub Iwerks.

==Summary== Hungarian Dance no. 5 and eventually must leave the stage on account of his overwhelmed emotions. The audience cheers, the mouse re-enters, and as an encore plays the last measure of the William Tell overture; mockingly laughed at by one particular heckler, he nonetheless plays the piece to great success despite breaking his instrument and bow in half in the process.

==Title==
An issue concerning this cartoon is whether the title of the short is Fiddling Around or Just Mickey. The former was copyrighted as such  and was seen on the original theatrical poster, while the latter is its common title and it was shown on the recreated title card seen the Walt Disney Treasures DVD release Mickey Mouse In Black And White: Volume 2, as no original release print with the title had yet surfaced.
In 2009, David Gerstein uncovered an original 1930 release print which shows the title as Fiddling Around, implying that Just Mickey was a working title. 

==References==
 

==External links==
*  

 

 
 
 
 
 


 