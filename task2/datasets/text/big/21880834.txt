Two Fists, One Heart
 
{{Infobox film
| name           = Two Fists, One Heart
| caption        = 
| image          = Two Fists, One Heart FilmPoster.jpeg
| director       = Shawn Seet
| producer       = David Elfick
| writer         = Rai Fazio
| starring       = Daniel Amalm Ennio Fantastichini Jessica Marais Tim Minchin
| music          = 
| cinematography = Hugh Miller
| editing        = Deborah Peart Milena Romanin
| studio         =  Walt Disney Home Entertainment HanWay Films (worldwide)
| released       =  
| runtime        = 105 minutes
| country        = Australia English Italian Italian
| budget         = $8.5M (which included a $4 million investment from Screen Australia)
| gross          = $305K
}}
 boxer whose Sicilian father/trainer wants Anthony to pursue boxing but he forsakes it 
when he meets Kate and perceives differently than his father the violence in the sport. She is a girl from the other side of the tracks. But Anthony reconsiders when his fathers boxing student quits; Anthony begins to train much more than before in preparation for the biggest fight of his life.

==Cast==
* Daniel Amalm as Anthony Argo
* Ennio Fantastichini as Joe Argo
* Jessica Marais as Kate
* Rai Fazio as Nico
* Tim Minchin as Tom
* Nicole Trunfio as Jessica

==Reception==
Cinema Autopsy awarded the film 2.5 stars.  The film was heavily criticized in the article Im Here to Save Screen Australia Time and Money,  and Screen Australias CEO Ruth harley expressed disappointed herself in the article Screen Aus reviews Two Fists Failure. 

==Budget and Box Office==
Two Fists, One Heart cost $8,500,000 (which included a $4 million investment from Screen Australia) and made $305,300.

==See also==
*Cinema of Australia

==References==
 

==External links==
*  

 
 
 
 
 
 


 