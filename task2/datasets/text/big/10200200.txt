Lovers (1991 film)
{{Infobox film
| name           = Amantes
| image          = Amantes movie poster.jpg
| caption        = Spanish theatrical release poster
| director       = Vicente Aranda 
| producer       = Pedro Costa,  Televisión Española (Televisión Española|TVE)
| writer         = Carlos Pérez Marinero  Alvaro del Amo   Vicente Aranda
| starring       = Victoria Abril, Jorge Sanz, Maribel Verdú
| music          = José Nieto (composer)|José Nieto
| cinematography = José Luis Alcaine
| editor         =  Teresa Font
| distributor    = Aries Films (1992) (USA) (subtitled),  Columbia TriStar
| released       = 12 April 1991 (Spain) 25 March 1992 (United States)
| runtime        = 103 minutes
| country        = Spain
| language       = Spanish
| budget         = Spanish peseta|P208,120,000
|}}
 Spanish film noir written and directed by Vicente Aranda, starring Victoria Abril,  Jorge Sanz and Maribel Verdú. The film brought Aranda to widespread attention in the English-speaking world.  It won two Goya Awards (Best Film and Best Director) and is considered one of the best Spanish films of the 90s.

==Plot ==
In Madrid, in the mid-1950s, Paco, a handsome young man from the provinces serving the last days of his military service, is in search of both a steady job and lodging. He is engaged to be married to Trini, his major’s maid.  Trini is not only sweet and pretty, but also has saved a sizable amount of money through years of hard work and frugal living, which will enable her and Paco to start their lives together comfortably. With a factory job lined up, Paco moves out of his barracks and look for somewhere to live until the wedding. Trini unwittingly refers him to Luisa, a beautiful widow who periodically takes in boarders and rents him a spare bedroom.

Besides supplementing her income with boarders, Luisa engages in swindles with underworld contracts, and is not above cheating her partners by skimming money off her illicit earnings. Instantly smitten by Paco, the attractive Luisa quickly seduces her new tenant.  Paco, frustrated by his unfruitful job hunt and by Trinis refusal to sleep with him until they are married, offers little resistance when Luisa seduces him, initiating an affair. He is dazzled with the sexual delight to which she introduces him.  So intense is Paco’s attraction for Luisa, that he abandons Trini for long periods, finally showing up at the major’s house to spend Christmas Eve with her. Trini feels a distance between herself and Paco, and while the couple is strolling in the street, she is surprised to see the “old widow” and immediately guesses that she and Paco are having a relationship.

Trini seeks the advice of the major’s wife, who tells her that she should use her own sexual powers to win Paco back. Waiting for Luisa to leave the apartment, Trini goes to Paco’s room and gives herself to him, making sure that Luisa later sees her leaving. At first, her tactic works and Paco reaffirms his love for her and they leave to visit Trini’s mother in her village. However, Trini is no match for her rival as a lover and Paco cannot get Luisa out of his mind.

When they come back to Madrid, Paco is willing to continue his twin relationship, but Luisa, who knows of Trini’s existence, is wildly jealous of her rival. Things become more complicated for Paco by Luisa’s shady business dealings with Minuta and Gordo, members of a gang of swindlers whom she owes money. They have threatened her life, and Paco, attempting to aid his lover, suggests that he get the money by swindling Trini of her savings. Luisa would prefer that they simply kill Trini, but proposes that Paco should marry Trini, then steal her savings and run away with Luisa. Paco uneasily agrees.

The plan is for Paco to propose marriage to Trini and bring her to the provincial city of Aranda del Duero where they have planned to purchase a bar. Under that pretense, Paco and Trini leave Madrid. Luisa follows them, unsure of Paco’s resolve. While Trini is asleep, Paco steals the money from her handbag. He offers it to Luisa but pulls out of their plan to flee together. Luisa, very upset, tells him hes botched the plan, and when he tells her to wait and, "Things will be okay," she excitedly utters, "Kill her!", and walks away, tossing the money at her feet – it is Paco she wants. Paco retrieves the money, and, driven by guilt, he returns to Trini to explain the situation.  After the disappearance of the money, Trini realized the fraud and understands that her love for Paco is doomed. When Paco comes back to the hotel room and confesses the plan, Trini tells him she prefers death to abandonment. Thwarted in her attempt to cut her own wrist with Paco’s razor, she begs him to kill her since that is what he really wants. Later, as the two sit in the rain on a bench in front of the cathedral of the town, Trini hands Paco the razor. In despair, she asks Paco to kill her. He does so, then rushes to the train station to prevent Luisa from leaving. Placing his bloody hands on her compartment window, signaling to Luisa that the mission has been accomplished, she gets off the moving train. The couple embraces passionately on the platform as the train pulls out. A title informs viewers that the police captured the pair three days later.

==Production==
 

Amantes had its origin in La Huella del Crimen (The trace of the crime), a Spanish TV series depicting infamous crimes which happened in  . The actual crime took place in the 1949 in La Canal, a small village near Burgos and so it was also dubbed as El crimen de La Canal (Crime at La Canal).

The crime concerned a widow, Francisca Sánchez Morales, engaged in blackmailing who persuaded a young man, José García San Juan, to kill his young wife. Three days later, the couple was caught and never saw each other again. They were condemned to capital punishment, still prevailing by those years in Spain (not even their attorney wanted to defend them). Eventually, they got their sentences commuted and they served between ten and twelve years. The widow died of a heart attack just after leaving jail, and the young man started a new, anonymous and prosperous life in Zaragoza.

A reinterpretation of the crime was built up during the pre-production of Los Amantes de Tetuán. The script was written by Alvaro del Amo, Carlos Perez Merinero and Vicente Aranda using some elements of the actual crime and reinventing many others to recreate the background of the characters about which little was known. The TV project was halted from 1987 to 1990, and when the time came to start production, Los Amantes de Tetuán took a separate life from La huella del crimen. It was going to be the last chapter to be filmed because Vicente Aranda was immersed in the making of a miniseries for Televisión Española|TVE. By then, Aranda was initially reluctant to make another production for TV and proposed to expand the script and make it into a full feature film for the big screen.  Thanks to the recent success of his miseries for TV Los Jinetes del Alba the project was approved by Televisión Española|TVE.
 Gentlemen Prefer Double Indemnity. Las Edades de Lulu, a movie seen by Aranda, inspired the eroticism of the film.

The most famous sequence of the film in which Luisa introduces a handkerchief in Paco’s anus to withdraw it in the moment of climax, was not in the script. Vicente Aranda explains:

: I told the actors that I felt something was lacking and that we needed something more explicit, a novelty. I opened a kind of contest and each one gave different options. Jorge Sanz came out with the idea of the handkerchief. The production was successful team collaboration. From the beginning at the preparation of the film, there was a spirit of team collaboration, something very positive that made our work fun. I don’t know why, but we all knew we were making a film that was going to be important. 

The intense and tragic climax was benefited by the weather, with a snowstorm that was not scheduled at all for the shooting, as producer, Pedro Costa, explained at the DVD commentaries. That scene filmed in front of the famous Burgos Cathedral was articulated around two popular Spanish Christmas carols, Dime, niño, de quién eres and La Marimorena, whose traditional cheerful melody, by contrast, is changed for the score into an elegiac and sad one.  The score was composed by Jose Nieto, often working with Aranda (Intruder (1993 film)|Intruso, Jealousy (1999 film)|Celos, Juana la Loca, Carmen) and the film editor was Aranda’s regular, his wife, Teresa Font. Equally noteworthy is the film’s striking cinematography by Jose Luis Alcaine, with whom Aranda had previously worked on no fewer than five films. Alcaine is especially adept at evoking the “sooty” look of Madrid winters, which convey the somber qualities of urban life during the early years of Franco’s dictatorship. D’Lugo, Marvin:   Guide to the Cinema of Spain, Greenwood Press, 1997, p.32 

==Cast==

Victoria Abril, here in her ninth collaboration with Vicente Aranda, was thought of from the beginning for the role of Trini, the virginal maid, while Concha Velasco was offered the role of Luisa. When Velasco declined, Aranda deemed Victoria was mature enough to play Luisa and proposed her to switch roles and take the one of the widow.  She was reluctant, saying that she saw herself more as Trini, the sacrifice maid rather than the wicked widow. Victoria Abril was pregnant, by the time she gave birth and was ready to start the film she had had a change of heart and accepted to be Luisa. Vicente Aranda, 2006 Declaración de Intenciones www.vicentearanda.es. 

The role of Trini fell on Maribel Verdú, a young actress who had made her film debut with Aranda in El Crimen del Capitán Sánchez (Captain Sánchezs crime) in 1984, as younger sister of Victoria Abrils character, and who was the leading actress of that film. Maribel Verdú, whose artistic  culmination maybe is Ricardo Francos 1997 La Buena Estrella is better known internationally for her roles in Alfonso Cuarón’s Y tu mamá también and in Guillermo del Toros 2006 film Pans Labyrinth.
 Oscar winner Si te dicen que Caí, a film which also starred Victoria Abril. The three lead actors of Amantes had been together before in Vicente Aranda’s previous project, Los Jinetes del Alba, in another love triangle.

*Victoria Abril - Luisa
*Jorge Sanz - Paco
*Maribel Verdú - Trini
* Enrique Cerro - commandant
* Mabel Escaño - commandant’s wife
* Alicia Agut -Trini’s mother
* Jose Cerro - Minuta
* Gabriel Latorre - Gordo
* Saturnino Garcia - Pueblerino

==Themes==

One of the main themes of Amantes is the destructive potential of obsessive passion and sexual desire a topic further explore by Aranda in La Pasion Turca (1994).  Amantes forms with Intruder (1993 film)|Intruso (1993) and Jealously (1999 film)|Celos (1999) a trilogy of films about love as uncontrollable passion that ends tragically. These three films directed by Vicente Aranda are loosely based in real crime stories.

==Analysis==
 
In Amantes, Aranda focuses tightly on his three leading actors, effectively conveying the dreadful, consuming power of passion. The films portrays a mortal struggle between a strong phallic widow and a younger patriarchal woman for the love of a young man in a romantic love triangle. The widow derives her power from an intense sexuality and the religious idealism belongs to the young victim. Kinder, Marsha:  Blood Cinema: The Reconstruction of National Identity in Spain, Berkeley University Press, 1993. p. 206  Set in film noir genre, it depicts the young man, Paco, not as the master of the two women who love him, but as the malleable object of their competing scenarios.  Luisa is introduced with a mouthful of candy, and draped with Christmas tinsel, almost as though she were a gift to the sexually frustrated Paco. Aranda thus sets up his central conflict with precision and power. Despite the physical beauty of the two women, it is the handsome young man who is treated as the primary object of desire. He is seduced by both women and is the object of the camera’s erotic gaze. Kinder, Marsha:  Blood Cinema: The Reconstruction of National Identity in Spain, Berkeley University Press, 1993. p. 207  He embodies the 1950s generation torn between “two Spains,” The formulation of the two Spains are represented by the two female rivals. Identified with the images of rural Spain, Trini is the traditionally stoically self-sacrificing embodiment of catholic Spain versus the modern vision of a newly emerging industrialized Spain, incarnated in Luisa who is identified with the city and with the iconography of foreign culture (Kimonos, Christmas tree ornaments), representing the modernizing version of Spain.  Though the narrative eventually cast Luisa as the heartless instigator of Trini’s murder, she can also be perceived as a rebel against the oppression of a macho culture while Paco, a traditional tragic film noir hero, is turned into a murderer by the two women who love him.

Despise being positioned as the opposing stereotypes of virgin and whore; Trini and Luisa are equally passionate and strong willed. Both are frequently robed in blue, a color feature in traditional pictorial representations of the virgin, and Trini takes with her a painting of the Immaculate Conception and hangs it in the hotel room just before her death. While Luisa directs her violent passions outwards, confessing to Paco that she murdered her husband, Trini turns them inwards on herself, following in the footsteps of her lame mother who threw herself in front of a cart after learning of her husband ‘s infidelity.  Just as Luisa forced Paco to take hold of his penis in their pursuits of pleasure; Trini coerces him to wield the razor that will release her from pain.

The sexual scenes of the film are designed to underscore the female domination of the male; even in terms of his sexual identity.  Amantes forcefully depicts the subversive power of Luisa’s sexuality. From the moment she opened the door to Paco, wearing a colorful dark blue robe and draped with glittering streamers she is using to decorate a Christmas tree, Luisa appears as a profane alternative to the Madonna. Her body substitutes for the Christmas tree and all of its religious symbolism, offering eroticism in place of religious ecstasy. Not only she is the sexual subject who actively pursues her own desire and who first seduces Paco, but she continues to control the lovemaking. In a graphic sex scene, we see her penetrating his anus with a silk handkerchief and then withdrawing it in the moments of ecstasy. Despite her sexually dominating her young lover, Luisa remains loving and emotionally vulnerable. Paco response not only makes him obsessed, but he rejects the more traditional passive sexuality of his fiancé Trini.

The murder scene is remarkable for its understated approach. Staged on a bench in front of the cathedral of Burgos (the small town of Aranda del Duero in the film), the murder retains the aura of Christian ritual, especially since the minimalist representation of violence is limited to a few close-up of the victim’s barefoot and a few drops of bright red blood falling on the pure white snow. Earlier, Paco had sat on the same bench praying, observed by a beautiful young mother who was carrying a bright blue umbrella and tying the shoelace of her young son, a symbolic Madonna who seems to foresee the murder. 

In the final scene, Paco goes to the train station to find Luisa. Pressing his bloody hands against the window, he draws her off the train for a final murderous embrace, a shot that is held, then blurs and finally freezes, signifying the triumph of their passion.

A printed epilogue delivers the ironic moralizing punch line to the narrative. Three days later, Paco and Luisa were arrested in Valladolid (a city well known for its right-wing sentiments) and they never saw each other again.

== Amantes and other films==
 Double Indemnity; Luisa also frequently wears dark glasses like the character played by Barbara Stanwyck in that Film. There are similarities to François Truffaut’s La Femme dà côté, another amour fou film in which a lame older woman own story foretells the tragic ends.

Vicente Aranda’s cold style to unrestrained passions has also been compare to the  similar approach  to passion in Ingmar Bergman’s films. The American movie To Die For that was inspired by the Pamela Smart case, the most famous crime in New Hampshire history, was compared to the similar plot of Amantes.

==Reception==

Amantes opened in February 1991 at the 41st Berlin International Film Festival where Victoria Abril won the Silver Bear for Best Actress.    In Spain it was a great success with critics and audiences and won two Goya Awards including Best Film and Best director. 
Amantes was the first of Aranda’s films to get an American release, opening in March 1992 becoming an important art house hit, getting more than one and a half million dollars at the box-office. The movie shocked audiences because of the frankness of its sex scenes.

The film remains Vicente Aranda’s best-regarded work; he explains “ Whenever someone wants to flatter me, they bring up the subject of Amantes. I haven’t been able to make a film that takes its place. 

==Awards and nominations==

* Two Goya Awards: Best Film and Best Director. (1992) Berlin Film Festival – Best Actress (Victoria Abril) 1991 
* Flanders International Film Festival  (1991) Special Mention  Maribel Verdú
* Two ADIRCAE Awards: Best Actress ( Victoria Abril ) Best Director ( Vicente Aranda) 1991
* Fotogramas de Plata: Best Film 1992
* Mystfest; Best Film 1991
* Ondas Awards : Best Film; best acting (Maribel Verdu) 1991
* Sant Jordi Awards ; Best Film  1992

==References==
 

== Bibliography==
* Alvarez, Rosa y Frias Belen, Vicente Aranda:  El Cine Como Pasión, Huelva, XX Festival de Cine Iberoamericano de Huelva, 1994
* Cánovás Belchí, Joaquín (ed.), Varios Autores,: Miradas sobre el cine de Vicente Aranda, Murcia: Universidad de Murcia, 2000.P. Madrid
* Colmena, Enrique: Vicente Aranda, Cátedra, Madrid, 1986, ISBN 84-376-1431-7
* Guarner, José Luis: El Inquietante Cine de Vicente Aranda,Imagfic, D.L.1985.
*Kinder, Marsha:  Blood Cinema: The Reconstruction of National Identity in Spain, Berkeley University Press, 1993. ISBN 0-520-08157-9
* D’Lugo, Marvin:   Guide to the Cinema of Spain, Greenwood Press, 1997.ISBN 0-313-29474-7

==External links==
* 
* 
*  

 
 

 
 
 
 
 
 
 
 
 