Hameshaa
{{Infobox film
| name           = Hameshaa
| image          = Hameshaa.jpg
| caption        = Sanjay Gupta
| producer       = G. P. Sippy
| writer         = Sanjay Gupta Kader Khan
| starring       = Saif Ali Khan Kajol Aditya Pancholi
| music          = Anu Malik
| music release  =
| country        = India
| cinematography = V. Manikandan
| editing        =
| distributor    = Eros Labs
| released       =  
| runtime        = 138 mins Hindi
| budget         = Rs. 4 crores 
| gross          = Rs. 44,987,977
}}
 romance movie Sanjay Gupta. The film stars Kajol, Saif Ali Khan and Aditya Pancholi in the lead roles. Aruna Irani and Kader Khan have supporting roles in the film. The film explores reincarnation.

== Synopsis ==

Raja (Saif Ali Khan) and Yash Vardhan (Aditya Pancholi) are childhood friends. Though they come from different backgrounds, Raja being poor and Yash being wealthy, they treat each other as brothers. Rani Sharma (Kajol) enters their lives, and they both fall in love with her. Raja and Rani both love each other very much and vow to be together. However Yash, jealous and bitter tragically kills Raja. Rani witnesses what he did and tells Yash that they will be together again and he will not be able to do anything. She falls to her death with intent, leaving Yash heartbroken and as a living corpse.

Many years have passed and Yash is unmarried and lives with his Dai Ma (Aruna Irani). Fate brings him to Reshma (also played by  Kajol) who looks like Rani. Yash soon discovers Raju (also played by  Saif Ali Khan) who looks like Raja. Yash looks after Reshma but she encounters Raju and remembers bits of her past life. After several other encounters with him she remembers what happened in her past life but Raju does not remember a thing. Yash wishes to marry Reshma and does whatever he can to make Raju leave. Reshma confronts Yash, she escapes him and goes to Raju who is about to leave the city. She explains what happened and he remembers what happened to him. They barely manage to escape Yash, but he is determined to make sure that Reshma stays with him only. Yash chases Raju and Reshma to where Raja and Rani originally died. Raju is hanging on to Yashs hand, ready to die just like Raja did, but reverses it. Now Yash is hanging on for dear life and just as Yash has Raju convinced, Raju lets go, and kills Yash, and he and Reshma unite.

== Cast ==
*Kajol...... Rani/Reshma
*Saif Ali Khan...... Raja/Raju
*Aditya Pancholi...... Yash Vardhan
*Aruna Irani...... Dai Ma
*Laxmikant Berde
*Kader Khan...... Rajus chacha
*Satyen Kappu...... Reshmas father
*Milind Gunaji...... Police Inspector

==Soundtrack==
{| border="7" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s) !! Length
|-
| 1
| "Neela Dupatta"
| Abhijeet Bhattacharya|Abhijeet, Sadhana Sargam
| 06:26
|- 
| 2
| "Hamesha Hamesha" 
| Kumar Sanu, Sadhana Sargam
| 06:54
|- 
| 3
| "Aisa Milan Kal Ho Na Ho"
| Kumar Sanu, Alka Yagnik
| 06:28
|- 
| 4
| "Ae Dil Hamen Itna Bata"
| Udit Narayan, Sadhana Sargam
| 06:28
|- 
| 5
| "Dil Tujhpe Fida"
| Kumar Sanu, Alka Yagnik
| 07:03
|- 
| 6
| "Rangtadi Rangtadi" 
| Alka Yagnik, Ila Arun
| 08:24
|- 
| 7
| "Marke Bhi Laut Aayenge" Abhijeet
| 06:36
|-
| 8
| "Beyond Eternity Theme From Hameshaa (Instrumental)"
| 
| 03:50
|-
| 9
| "Nazrein Dhoonde Dhoonde"
| Alka Yagnik
| 05:00
|}

==External links==
*  

 
 
 
 