My Run
{{Infobox film
| name           = My Run
| image          =
| caption        = Theatrical poster
| director       = Tim VandeSteeg
| producer       = Tim VandeSteeg Mark Castaldo
| writer         = Kim Pederson
| narrator       = Billy Bob Thornton
| starring       = Terry Hitchcock Teri Sue Hitchcock Chris Hitchcock Jason Hitchcock
| music          = Steve Horner
| cinematography = Matt Ehling
| editing        = David Frank
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         =
}}
My Run is a 2009 documentary film directed and produced by Tim VandeSteeg and produced by Mark Castaldo.  The film chronicles Terry Hitchcocks journey of completing 75 marathons in 75 consecutive days to raise awareness for single parent families. It first screened at the Austin Film Festival on October 23, 2009.

==Plot==
My Run tells the true story of two journeys. The first, which began in 1984, opens the film when Terry Hitchcocks wife Sue dies of breast cancer. Only a few days later he loses his job. Suddenly, he finds himself alone with his three young children and no income. 

The film follows Terry as he learns to function as a single parent, and discovers how difficult it is to maintain and nourish a strong loving family and how faith can be instrumental in strengthening your will to keep moving forward. His experience also teaches him that single parents and their children are unsung everyday heroes. 
 

The film jumps forward to 1996, when Terry takes the first step of his "Mega-Marathon" from Minneapolis to Atlanta. He runs every day, covering the equivalent of a marathon or more for 75 consecutive days to arrive just in time for the summer Olympic Games. He runs in honor of his wife and to bring attention and a voice to the everyday heroes; the single parents and their kids.

Terry expresses in the film that he wants to let everyone know that nothing is impossible, that ordinary people can do extraordinary things and by doing so inspire others. "Every one of us can do something", Terry says, "thats what it’s all about". 

==Production==
Director/Producer Tim VandeSteeg was inspired to make this film after meeting Terry Hitchcock through a mutual contact.    

Billy Bob Thorntons narration was recorded in the same studio where his group The Boxmasters records. 

== Critical reception ==
Josh Board, from Sandiego.com says "Run, dont walk, to catch this in the theatres when its released." 

===Accolades===
{| class="wikitable" style="font-size: 95%;"
|-
! Award
! Date of ceremony
! Category
! Recipient(s)
! Result
|-
| Austin Film Festival 
| November&nbsp;5, 2009
| Documentary Feature Competition Audience Award
|
|  
|-
| Newport Beach Film Festival 
| April&nbsp;30, 2010
| Outstanding Achievement in Documentary
|
|  
|-
| Mammoth Film Festival 
| December&nbsp;17, 2009
| Best Documentary
|
|  
|-
| Las Vegas Film Festival 
|
| Grand Jury Prize
|
|  
|-
| DocMiami International Film Festival 
|
| Audience Award
|
|  
|-
| Visionfest 
|
| Outstanding Achievement Documentary Filmmaking
|
|  
|-
| Big Bear Lake International Film Festival 
|
| Audience Award
|
|  
|}

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 