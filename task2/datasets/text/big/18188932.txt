Up and Down (film)
{{Infobox film
| name           = Up and Down
| image          = Upanddownfilm.jpg
| caption        = 
| genre          = black comedy
| director       = Jan Hřebejk     
| producer       = Ondřej Trojan
| writer         = Jan Hřebejk Petr Jarchovský
| starring       = Jirí Machácek Petr Forman Jan Tříska Emília Vášáryová Natasa Burger
| music          = Ales Brezina
| cinematography = Jan Malír
| editing        = Vladimír Barák
| distributor    = Falcon  Sony Classics
| released       =   (at 2004 Cannes Film Festival|Cannes)   (Czech Republic)   (U.S.)
| runtime        = 108 minutes
| country        = Czech Republic
| awards         = Czech Lion
| language       = Czech
| budget         = $2,000,000 
| gross          = $2,705,163 
| preceded_by    = 
| followed_by    =
}}
 2004 Cinema Czech comedy Cannes Film Market on May 19, 2004.

The film was the Czech Republics submission to the 77th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.   

==Plot==
Milan and Goran are two criminals who smuggle illegal immigrants. One night after they complete a smuggle, they discover that one of the immigrants has left a baby behind. Milan and Goran decide to sell the baby to Lubos and Eman, who are responsible for running an illegal baby adoption center. Lubos and Eman make attempts to sell the baby to Miluska and Frantisek, a barren couple. Concurrently a university professor is diagnosed with an inoperable brain tumor, setting into action a complicated train of family reunions, partings, and conflicts.

==Cast==
*Jiří Macháček.....Frantisek Fikes
*Petr Forman.....Martin Horecký
*Jan Tříska.....Professor Otakar Horecký
*Emília Vášáryová.....Vera Horecká 
*Natasa Burger.....Miluska
*Ingrid Timková.....Hana Svobodová 
*Kristýna Liška Boková.....Lenka Horecká
*Pavel Liska.....Eman
*Marek Daniel.....Lubos
*Jan Budar.....Milan
*Zdenek Suchý.....Goran

==Awards and nominations==
Czech Lion
*Won, "Best Actress" - Emília Vášáryová
*Won, "Best Director" - Jan Hřebejk
*Won, "Best Film" 
*Won, "Best Film Poster" - Ales Najbrt
*Won, "Best Screenplay" - Petr Jarchovský
*Won, "Critics Award" - Jan Hřebejk
*Nominated, "Best Actor" - Jirí Machácek
*Nominated, "Best Art Direction" - Milan Býcek & Katarina Bielikova
*Nominated, "Best Cinematography" - Jan Malír
*Nominated, "Best Music" - Ales Brezina
*Nominated, "Best Sound" - Michal Holubec
*Nominated, "Best Supporting Actor" - Petr Forman
*Nominated, "Best Supporting Actor" - Jan Tříska
*Nominated, "Best Supporting Actress" - Ingrid Timková

Pilsen Film Festival
*Won, "Best Film"

==References==
 

==External links==
* 
*  
* 

 

 
 
 
 
 
 


 
 