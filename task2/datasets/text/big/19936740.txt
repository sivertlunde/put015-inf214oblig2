Bitten (film)
{{Infobox film
| name=Bitten
| caption=
| image	=	Bitten FilmPoster.jpeg
| director=Harv Glazer
| producer= Robert Wilson
| writer        = Tim McGregor Tyler Levine
| starring      = Jason Mewes Erica Cox Richard Fitzpatrick Nic Nac Stuart Stone Jordan Madley
| music = Stu Stone
| cinematography = Simon Shohet
| editing       = Ron Wisman Jr.
| distributor = Albatros Film (Japan) Nexeed (Japan) LK-TEL (Argentina) Paramount Home Entertainment (Australia)
| released =  
| runtime= 60 minutes
| country= Canada
| language= English
| budget= CAD 2,500,000 (est.)
| gross=
}}
Bitten is a 2008 low budget black comedy vampire film that stars Jason Mewes, as a paramedic who rescues Danika, a female vampire, from an alley way.

==Plot==
Jack, a paramedic, is frustrated with his life after he breaks up with his girlfriend.  He discovers a girl (Danika) in an alley way covered in blood, clinging to life. Jack takes Danika in and soon discovers that she is a vampire.

Jack and Danika try to find a way to feed her cravings to drink blood while killing as little as possible.  All their attempts end without success because a vampire needs fresh human blood. Jack also must find places to hide the bodies in his apartment, including the body of his ex-girlfriend, who had gone to his apartment to reclaim several of her possessions before Danika had bitten and killed her. Jack discovers upon his ex-girlfriends corpse reanimating into a vampire (and having to kill her when she attacks him in a rage) that only a stab to the heart will kill a vampire.

When Danika becomes more violent, killing several people, including a young woman, Jack is forced to kill her with the help of his paramedic co-worker and friend, who stabs Danika in the heart while she attacks Jack, and succeeds in killing her, but not before Jack is bitten, leaving Jacks fate to be unknown during the final credits and an out-take sequence. However, we then see that Jack has become a vampire, apparently cared for by his co-paramedic, who is feeding him from a dish of blood.

==Cast==
*Jason Mewes as Jack
*Erica Cox as Danika
*Richard Fitzpatrick as Roger
*Nic Nac as Pusher
*Stuart Stone as Twitch
*Jordan Madley as Sherry
*Grace Armas as Mrs. Lee
*Amy Lynn Grover as Maya
*Jeff Pangman as Day Shift EMT
*Shawn Goldberg as Junkie
*Stefan Brogren as Bearded Man
*Suresh John as Store Owner

==Release== Sci Fi Channel on March 15, 2009  and was released on DVD on July 6, 2010. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 
 