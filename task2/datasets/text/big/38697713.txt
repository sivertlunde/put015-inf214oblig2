The Jockstrap Raiders
 
 animated film. Running for 19 minutes, it was written, produced and directed by Mark J Nelson as a thesis film for his Master of Fine Arts in Animation at UCLA. The film took almost five years (part-time) to complete and was made in Los Angeles. 

== Plot ==
The film is set in Leeds, England at the time of the First World War and centres on a group of misfits unable to enlist in the regular armed forces. When they hear of an imminent attack by the German Kaiser, who is constructing a bridge across the English Channel, they embark on a mission to save their country using homemade planes and explosives. The squadron are forced to strip to their jock straps in order to get their fleet of aircraft airborne and, despite many mishaps, succeed in blowing up the bridge and saving the nation.

== Awards ==
The Jockstrap Raiders has won a number of awards, including:
* Silver medal for Animation at the Student Academy Awards 
* Best animation, best film and best sound effects at the British Animation Festival 2012  Aesthetica Short Film Festival 2012 
* Audience choice winner 2011 for best short at Orlando Film Festival  Macon Film Festival (MaGa) 
* 2012 best short short at GI Film Festival. 

==References==
 

== External links ==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 


 