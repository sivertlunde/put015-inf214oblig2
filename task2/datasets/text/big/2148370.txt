North Country (film)
 
{{Infobox film
| name           = North Country
| image          = NorthCountryPoster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Niki Caro Nick Wechsler
| writer         = Michael Seitzman
| based on       =   Laura Leedy Gansler}}
| starring       = {{Plainlist|
* Charlize Theron
* Frances McDormand
* Sean Bean
* Richard Jenkins
* Michelle Monaghan
* Jeremy Renner
* with Woody Harrelson
* and Sissy Spacek}}
| music          = Gustavo Santaolalla
| cinematography = Chris Menges
| editing        = David Coulson
| studio         = {{Plainlist|
* Industry Entertainment Nick Wechsler Productions
* Participant Media}}
| distributor    = Warner Bros.
| released       =  
| runtime        = 126 minutes  
| country        = United States
| language       = English
| budget         = $35 million 
| gross          = $25.2 million 
}}
North Country is a 2005 American  .

==Plot== the community not to associate with her. Her only friends are Glory Dodge and Glorys husband, Kyle. Glory, who works at the local iron mines (the towns main source of income), aids Josey in getting a job there. Glory and Kyle also allow Josey to stay at their home with her children, due to Joseys poor relationship with her father.

Josey quickly befriends the other female workers at the mine, who include Glory, Sherry, Peg and Big Betty, and soon realizes the women are constant targets for sexual harassment and humiliation by most of their male co-workers, most notably Bobby Sharp, a former friend of Joseys from high school, as most of the townspeople believe women should not be allowed to be miners. After witnessing the harassment of the other women, and being subjected to it herself, Josey tries to talk to her direct supervisor about the problem, but he refuses to take her concerns seriously, as he also believes women should not be working at the mine.

Joseys attempts to address the problem prompts the men to spread lies about her trying to seduce them, which leads Bobbys wife to publicly berate Josey. When Sammy comes to believe the gossip about his mothers promiscuity, it also leads them to grow estranged. After the mines board of directors refuses to hear Joseys complaints about the treatment of women at the mine, and after being sexually assaulted by Bobby at work, she quits and asks Bill White, a lawyer friend of Kyle and Glory, to help her file a lawsuit against the company. Bill tells her that the best way to win a case like this is by convincing the other women to back up her statements in court, which would make it the first class-action sexual-harassment lawsuit ever filed in the country. The female miners however, are hesitant, as this would mean risking their jobs or making the harassment even worse. Josey also discovers that Glory has amyotrophic lateral sclerosis, or ALS.

Hank is disappointed by Joseys decision, and Alice leaves him, tired of his criticism of their daughter. At a union meeting, Josey attempts to address the miners and explain her reasons for suing the mine. When they refuse to hear her and start verbally abusing her, Hank, who is also in attendance, stands up for his daughter and reprimands his co-workers for their rude treatment of Josey and all the women at the mine.
 impregnated from lying about the sex being consensual.

Glory has come to the court in her wheelchair and from the back of the room, her husband reads a letter saying she stands with Josey. Other women then stand up to support Joseys complaint. They are followed by more women, family members, and miners, making the case a class action. With this, the mining company loses the case and is forced to pay the women for what they suffered, in addition to establishing a sexual harassment policy at the workplace. Josey, vindicated, thanks Bill for all that he has done for her and her family and departs to teach Sammy how to drive, telling him that she intends to buy him a car on his eighteenth birthday.

==Cast==
 
* Charlize Theron as Josey Aimes (based on Lois Elaine Jensen)
** Amber Heard as young Josey
* Frances McDormand as Glory Dodge (based on Patricia Shannon Kosmach 1937-1994)
* Sean Bean as Kyle Dodge, Glorys husband
* Richard Jenkins as Hank Aimes, Joseys father
* Sissy Spacek as Alice Aimes, Joseys mother
* Michelle Monaghan as Sherry, a female miner
* Jeremy Renner as Bobby Sharp, a miner
** Cole Williams as young Bobby
* Woody Harrelson as Bill White, a lawyer Thomas Curtis as Sam "Sammy" Aimes, Joseys son
* Elle Peterson as Karen Aimes, Joseys daughter
* Rusty Schwimmer as Big Betty, a female miner
* Jillian Armenante as Peg, a female miner
* Linda Emond as Leslie Conlin
* Brad William Henke as Lattavansky
* John Aylward as Judge Halsted
* Corey Stoll as Ricky Sennett, a miner
* Jacqueline Wright as Bobbys wife
 

==Production==
Lois Jenson, on whom the character of Josey is based, actually began working at the EVTAC (from "Eveleth Taconite") mine in Eveleth, Minnesota in 1975 and initiated her lawsuit in 1984, four years before the year in which the film begins. Its time line was condensed, but in reality it took fourteen years for the case to be settled. Jenson declined to sell the rights to her story or act as the films consultant. 
 Hibbing in Silver City Santa Fe in New Mexico.

==Soundtrack==
# "North Country" by Gustavo Santaolalla – 2:08 Girl of the North Country" by Leo Kottke – 3:33
# "Tell Ol Bill" by Bob Dylan – 5:08
# "Werewolves of London" by Warren Zevon – 3:28
# "Bette Davis Eyes" by Kim Carnes – 3:49
# "If I Said You Had a Beautiful Body (Would You Hold It Against Me)" by The Bellamy Brothers – 3:17
# "Lay Lady Lay" by Bob Dylan – 3:19
# "A Saturday in My Classroom" by Gustavo Santaolalla – 3:46
# "Sweetheart Like You" by Bob Dylan – 4:37
# "Baby Dont Get Hooked on Me" by Mac Davis – 3:05
# "Do Right to Me Baby (Do Unto Others)" by Bob Dylan – 3:52
# "Standing Up" by Gustavo Santaolalla – 2:43
# "Paths of Victory" by Cat Power – 3:24

Songs in the film that werent in the soundtrack release include "Wasnt That a Party" by The Irish Rovers, "Shake the House Down" by Molly Hatchet and karaoke versions of George Thorogoods "I Drink Alone" and Pat Benatars "Hit Me with Your Best Shot."

==Release==
The film premiered at the 2005 Toronto International Film Festival and was shown at the Chicago International Film Festival before going into theatrical release in the US, where it grossed $6,422,455 in its opening weekend, ranking 5th at the box office.  Budgeted at $30&nbsp;million, it eventually grossed $18,337,722 in the US and $6,873,453 in foreign markets for a total worldwide box office of $25,211,175.   

===Critical reception===
On the review aggregator Rotten Tomatoes, 69% of critics gave the film positive reviews, based on 162 reviews, with Theron and McDormand receiving critical acclaim for their performances.  On Metacritic, the film has an average score of 68 out of 100, based on 39 reviews. 

Manohla Dargis of the New York Times called it "a star vehicle with heart – an old-fashioned liberal weepie about truth and justice" and added, "  is one of those Hollywood entertainments that strive to tell a hard, bitter story with as much uplift as possible. That the film works as well as it does, delivering a tough first hour only to disintegrate like a wet newspaper, testifies to the skill of the filmmakers as well as to the constraints brought on them by an industry that insists on slapping a pretty bow on even the foulest truth." 

In his review in the Chicago Sun-Times, Roger Ebert observed, "North Country is one of those movies that stir you up and make you mad, because it dramatizes practices youve heard about but never really visualized. We remember that Frances McDormand played a woman police officer in this same area in Fargo (film)|Fargo, and we value that memory, because it provides a foundation for Josey Aimes. McDormands role in this movie is different and much sadder, but brings the same pluck and common sense to the screen. Put these two women together (as actors and characters) and they can accomplish just about anything. Watching them do it is a great movie experience." 

Ruthe Stein of the San Francisco Chronicle called the film a "compelling if occasionally unnecessarily convoluted movie . . . The first 15 minutes or so are a mess . . . Fortunately,   calms down and becomes extremely engrossing, especially in the courtroom battles . . . its all carefully calculated for dramatic effect and succeeds brilliantly in drawing you in and eliciting tears in the process . . . North Country would have benefited from crisper editing. It runs at least 15 minutes longer than necessary . . . For all its flaws,   delivers an emotional wallop and a couple of performances worthy of recognition come award time." 

In Rolling Stone, Peter Travers awarded the film two out of a possible four stars and commented, "Any similarities between Josey and Lois Jenson, the real woman who made Eveleth Mines pay for their sins in a landmark 1988 class-action suit, are purely coincidental. Instead, we get a TV-movie fantasy of female empowerment glazed with soap-opera theatrics. The actors, director Niki Caro (Whale Rider) and the great cinematographer Chris Menges all labor to make things look authentic. But a crock is a crock, despite the ferocity and feeling Theron brings to the role . . . Though the dirt and grime in North Country are artfully applied, its purely cosmetic and skin-deep." 

In "Stories from North Country," a documentary accompanying the film on the DVD, Lois Jenson, on whom the story is based, said, "I think its important for people to see this." Regarding Charlize Theron, Jenson said, "She has the character.   She knew the part. She knew what it needed – the depth she needed to go to. Shes done a great job with it."

David Rooney of Variety (magazine)|Variety said, "  indulges in movie-ish manipulation in its climactic courtroom scenes. But it remains an emotionally potent story told with great dignity, to which women especially will respond . . . The film represents a confident next step for lead Charlize Theron. Though the challenges of following a career-redefining Oscar role have stymied actresses, Theron segues from Monster (2003 film)|Monster to a performance in many ways more accomplished . . . The strength of both the performance and character anchor the film firmly in the tradition of other dramas about working-class women leading the fight over industrial workplace issues, such as Norma Rae or Silkwood." 

In the St. Petersburg Times, Steve Persall graded the film A and called it "deeply, undeniably moving . . . crusader cinema at its finest." 

===Awards and nominations===
* Academy Award for Best Actress (Charlize Theron, nominee)
* Academy Award for Best Supporting Actress (Frances McDormand, nominee)
* Golden Globe Award for Best Actress – Motion Picture Drama (Theron, nominee)
* Golden Globe Award for Best Supporting Actress – Motion Picture (McDormand, nominee)
* BAFTA Award for Best Actress in a Leading Role (Theron, nominee)
* BAFTA Award for Best Actress in a Supporting Role (McDormand, nominee)
* Screen Actors Guild Award for Outstanding Performance by a Female Actor in a Leading Role – Motion Picture (Theron, nominee)
* Screen Actors Guild Award for Outstanding Performance by a Female Actor in a Supporting Role – Motion Picture (McDormand, nominee)
* Satellite Award for Best Actress – Motion Picture Drama (Theron, nominee)
* Satellite Award for Best Supporting Actress – Motion Picture (McDormand, nominee)
* BFCA Critics Choice Award for Best Actress (Theron, nominee)
* BFCA Critics Choice Award for Best Supporting Actress (McDormand, nominee)
* Las Vegas Film Critics Society Award for Best Supporting Actress (McDormand, winner) Washington D.C. Area Film Critics Association for Best Actress (Theron, nominee)

==See also==
* List of American films of 2005
* Sexual harassment
* Hostile environment sexual harassment Michelle Vinson v. Merit One Savings Bank
* Oncale v. Sundowner Offshore Services
*  

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 