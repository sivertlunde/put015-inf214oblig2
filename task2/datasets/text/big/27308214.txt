Prithvi (2010 film)
{{Infobox film
| name           = Prithvi
| image          = Prithvi_ips.jpg
| director       = Jacob Varghese
| producer       = Rajakumar N. S. Soorappa Babu
| writers        =
| screenplay     = Parvathi Avinash
| music          = Manikanth Kadri
| cinematography = Sathya. P
| editing        = Kishore Te.
| released       =  
| runtime        = 134 minutes Kannada
| country        = India
| budget         = 
}}
 Parvathi in lead roles. Manikanth Kadri composed the music of the film. The film is based on the political scenario in Karnataka. 

==Plot== IAS Officer who  works as District Commissioner for Bellary District. His main aim is to eradicate the ongoing corruption in the District and stick to his principles: honesty and corruption-free administration.

Once he takes office, he discovers that the people of the area are daily drinking contaminated water that is polluted by unstoppable illegal mining companies. He notices that people fall ill due to the polluted water and foresees the future may be very critical if the people continue to have the same water for drinking.

His mission is to find out the reason behind the polluted water. He seizes all illegal mining companies around Bellary with no mercy. As a result, Bellary Mining Lords try to kill him.

How Prithvi reacts to Bellary Mining Lords conspiracy, even dare to question Governments decision and be successful in his mission against illegal mining companies is the further story of the film.

==Cast==
* Puneeth Rajkumar as Prithvi Parvathi as Priya
* Avinash
* Srinivasa Murthy
* C. R. Simha
* John Kokin

==Music==
{{Infobox album
| Name        = Prithvi
| Type        = soundtrack
| Artist      = Manikanth Kadri
| Cover       =
| Released    =  
| Recorded    = Feature film soundtrack
| Length      =
| Label       =
}}

The music for the film is scored by Manikanth Kadri.

{| class="wikitable"
|- 
| Track # || Song || Singer(s) || Duration || Location
|- Swetha Mohan, Tippu || || 
|- Shruthi Haasan || || Wadi Rum and Jerash, Jordan
|-
| 3 || "Ku Ku Kogileyinda" || Rajesh Krishnan, Sunidhi Chauhan || ||
|- Hamshika Iyer, Kunal Ganjawala || ||
|-
| 5 || "Hagella Nee Nodabeda" ||  Anitha, Haricharan || || Petra and Wadi Rum, Jordan
|-
| 6 || "Jagave Ninadu" || Asif Akbar, Benny Dayal, Sindhuja Rajaram || ||
|-
| 7 || "Ninagende" || Martin Visser || ||
|-
| 8 || "Hejjegondu Hejje" || Benny Dayal,Asif Akbar, Sindhuja Rajaram || ||
|}

==Reception==
The film ran for more than 150 days in cinemas.  The film was also dubbed in Malayalam.

==References==
 

 
 
 
 