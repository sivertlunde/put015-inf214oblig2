Final Destination (film)
 
 
{{Infobox film
| name = Final Destination
| image = Final Destination movie.jpg
| image_size = 225px
| caption = Theatrical release poster James Wong
| producer = Glen Morgan Warren Zide Craig Perry
| screenplay = James Wong Glen Morgan Jeffrey Reddick
| story = Jeffrey Reddick
| starring = Devon Sawa Ali Larter Kerr Smith Tony Todd 
| music = Shirley Walker Robert McLachlan
| editing = James Coblentz
| studio = Zide/Perry Productions Hard Eight Pictures
| distributor = New Line Cinema (US) StudioCanal (Germany)
| released =  
| runtime = 98 minutes
| country = United States  
| language = English
| budget = $23 million 
| gross = $112,880,294 
}}
 James Wong. Death gradually takes the lives of those who should have perished on the plane.

The film started off as a spec script written by Reddick for an episode of  The X-Files, in order for Reddick to get a TV agent. He never submitted it to The X-files after a colleague at New Line Cinema persuaded him to write it as a feature-length film.  Later, Wong and Morgan, The X-Files writing partners, became interested in the script and agreed to rewrite and direct the film, marking Wongs film directing debut.             Filming took place in New York and Vancouver, with additional scenes filmed in Toronto and San Francisco. It was released on March 17, 2000, and became a financial success, making $10 million on its opening weekend.    The DVD release of the film, released on September 26, 2000, in the United States and Canada,    includes commentaries, deleted scenes, and documentaries.       
 Best Horror Best Performance novels and comic books.

==Plot== Alex Browning Carter Horton. Tod Waggner, Terry Chaney, Valerie Lewton, Billy Hitchcock two FBI agents, who believe that Alex had something to do with the explosion.

Thirty-nine days later, the survivors, who mostly resent Alex, attend a memorial service for the victims. After the service, Tod is killed when a chain reaction causes him to slip on leaking toilet water, and be hung from a retractable clothes line in the shower. His death is deemed a suicide, however, Alex doesnt believe that Tod killed himself. He and Clear sneak into the funeral home to glimpse at Tods body, where they meet mortician William Bludworth, who tells them that they have ruined Death personification|Deaths plan and Death is now claiming the lives of those who were meant to die on the plane. The next day, Alex and Clear discuss what the mortician said at a cafe. Alex believes that if they look out for omens they can cheat Death again, although Clear is skeptical. They encounter the rest of the survivors; and, when Carter provokes Alex, Terry storms off in frustration, and is suddenly killed by a speeding bus.

After watching a news report on the cause of the explosion, Alex concludes that the survivors are dying in the order they were meant to die on the plane. He deduces that Ms. Lewton is next, and rushes to her house to ensure her safety. Thinking Alex is up to no good, Ms. Lewton calls the FBI agents, who take him in for questioning. Alex fails to convince the agents what is happening, but they decide to let him go. Nonetheless, he is too late to save Ms. Lewton, whose house explodes after she is impaled by a kitchen knife. The remaining survivors reunite and Alex explains whats going on as they drive through town. During the discussion Carter learns that he is next on Deaths list. Frustrated over having no control over his life, Carter stalls his car on railroad tracks, wanting to die on his own terms. He changes his mind at the last minute; but cant get out when his seat belt jams. Alex manages to save Carter when his seat belt rips, just before his car is smashed by an oncoming train. As Billy warns the others to stay away from Carter, he is suddenly decapitated by flying shrapnel from the car wreckage.

Alex deduces that since he intervened, Death skipped Carter and moved onto Billy. Realizing that he is next, Alex hides out in a fortified cabin, but he recalls switching seats with two girls in his premonition, meaning Clear is actually next, and rushes to save her while being chased by agents. Meanwhile, Clear is trapped inside her car with a leaking gas tank, surrounded by loose live wires. Alex arrives in time to save Clear and grabs the wire, allowing her to escape from the car seconds before it explodes, leaving Alex incapacitated.

Six months later, Alex, Clear, and Carter arrive in Paris to celebrate their survival. While discussing their ordeal, Alex explains that Death never skipped him. After seeing more omens he leaves the table, and Clear warns him of an oncoming bus, which swerves and crashes into a large neon sign that swings off its hinges towards Alex. Carter pushes Alex out of the way at the last second, and Alex tells Carter that Death has skipped him. When Carter asks whos next, the sign suddenly swings back down towards him, and the screen cuts to black, followed by a loud smashing sound..

==Cast==
* Devon Sawa as Alex Browning
* Ali Larter as Clear Rivers Carter Horton Valerie Lewton Agent Weine Agent Schreck Chad E. Tod Waggner Billy Hitchcock
* Tony Todd as William Bludworth Terry Chaney George Waggner Larry Murnau Christa Marsh Blake Dreyer Barbara Browning Ken Browning

==Production==
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" | "One thing we were all in agreement on from the start is that we didnt want to do a slasher movie. We didnt want a guy in a dark cloak or some kind of monster chasing after these kids. Thats been done again and again. I became very excited when we decided to make the world at large, in the service of death, our antagonist. Everyday objects and occurrences then take on ominous proportions and it becomes less about whether or not our characters are going to die and more about how they will die and how they can delay their deaths. The entertainment value is in the ride not in the outcome, and by placing the premise of the film on the inevitability of death, we play a certain philosophical note".
|-
| style="text-align: left;" | — James Wong on how he accepted the directing and writing privileges for the film. 
|}

===Development===
The original idea was written by Jeffrey Reddick as a spec script for The X-Files in order to get a TV agent.  "I was actually flying home to Kentucky and I read a story about a woman who was on vacation and her mom called her and said, Don’t take the flight tomorrow, I have a really bad feeling about it.’ She switched flights and the plane that she would have been on crashed," said Reddick. "I thought, that’s creepy—what if she was supposed to die on that flight?"  Building on his idea, Reddick wrote script and got an agent, but never submitted the script to The X-Files after a colleague at New Line Cinema suggested he write it as a feature.  One of the biggest misconceptions about the project is that it was based on the real-life disaster of TWA Flight 800 that occurred in 1996. Like in the movie, the TWA disaster involved a Boeing 747 that exploded after take-off from JFK International Airport in New York en route to Paris.  The TV spec script  for The X-Files  however, was actually written in 1994.
 James Wong and Glen Morgan. Both writers were willing to make it into a film, although they rewrote the script to comply with their standards.  "I believe that at one time or another weve all experienced a sense of prescience.  We have a hunch, a feeling, and then that hunch proves true," Wong said.  "We want to do for planes and air travel what Jaws (film)|Jaws did for sharks and swimming".
 Michael Myers Vancouver airport waiting for a flight when John Denver came on over the loudspeaker. I remember saying to myself, Hey, he just died in a plane crash – thats a little weird.  We wrote that version of that experience into the script."   

Producers Craig Perry and Warren Zide from   accepted financing and distributing rights for the film after Reddick came to them personally.  
 Billy Hitchcock, Carter Horton, Terry Chaney, Clear Rivers, Alex Browning, Tod Waggner.]]

===Casting===
 
"One of the most important things we were looking for in casting was the actors ability to play the subtleties – the little things that a character doesnt say or do that create the edge, the things that get under your skin and spook you," Morgan said about the auditions.

Alex Browning, the last role cast, went to Canadian actor Devon Sawa, who previously starred in the 1999 film Idle Hands.  Sawa said that when "  read the script on a plane,   found   peeking out the window at the engine every couple of minutes" and "  went down and met Glen and Jim and   thought they were amazing and already had some great ideas".     However, Morgan and Wong were undecided about casting him for the part, so they requested him to perform again as they reviewed his previous works. Morgan was astounded by Sawas performance in Idle Hands, and Sawa was hired. 

Sawa described his role as "in the beginning,   was kinda loopy and cotter, and you know, probably not the most popular guy in school. I think he might have been a dork, you know, doing their stuff and they had their own thing going and theyre after the two beautiful girls in school, but theres no chance of that happening. I guess after the plane goes down, his world completely changes".  "Devon has an every man quality that makes him accessible," Wong said. "He doesnt appear as if hes supremely self-assured.  Hes more of a regular kid who can take on the complexities of the role and become a hero".    Perry was amazed by Sawas vulnerability in acting, describing him as "a very distinctive actor. Hes very loose and hes kind of a cut-up when hes not on camera, but the moment the cameras on, Id never seen anybody to completely slide right through the moment". 
 Varsity Blues, was cast as female lead Clear Rivers. "The film shows how easy it is to turn on someone, to blame someone when youre scared," Larter said. "Its also about trusting your intuitions and yourself". She defined her part as "that girl who has a lot of loss in her life and has fallen for herself, and had made a life within that. Shes an artist, she lives by herself, and shes kinda holding to her grip for what the world has given her".   
 American Pie, was hired as class clown Billy Hitchcock.  Scott admired the film, and felt that "its   dark and eerie as any Twilight Zone".  He laughed at his role, saying that "  is lacking some social skills, he doesnt have quite a few friends, and hes like the tag-along".  Scott was surprised when in the script his character was written as fat. The writers eventually changed it for Scott. 

Dawsons Creek star Kerr Smith was cast as jock Carter Horton. Smith identified Carter as "your typical high school bully whose life depends on anger" and mentioned the fact that Carter feared Alex not having control of his own life. 

{{quote box width = 35em border = 1px
|align=left
|bgcolor=#ffffe0 fontsize = 85% quote = "Theres not a lot of good stuff, you know, for my age. You get a lot of scripts and all but theyre teen ensembles and theyre just crap. And then I got Flight 180... I mean, its just awesome". salign = left source =— Devon Sawa on the script of Final Destination.  
}}

Kristen Cloke, Morgans wife, was cast as teacher Valerie Lewton.    "I have incredible respect for them," said Cloke. "Jims the kind of director who knows exactly what he wants. As an actor, I can find a way to get there if I know specifically what Im going for, and Jim gives me that. The fact that he wont move on until hes got exactly what he wants creates a safe environment, which allows me to experiment and try different things".  Cloke described her part as "strong and sassy – in control. After the crash she comes unglued, probably more than any of the kids, and its a quick, drastic change. I had to understand the psychology of a person who can turn on a dime like that". 
 Chad E. Donella were cast as students Terry Chaney and Tod Waggner, respectively.  "When I first read the script, the thing that struck me most was that the characters were well-written and the relationships between them were strong and believable," Detmer said. "Thats important, because you have to care about these people in order to be worried about what might happen to them".  Detmer defined Terry as "very put-together   seems content to defer to   – to not make waves. But the stress of what happens affects their relationship and interestingly enough brings out a certain strength in her".  On the other hand, Donella observed how similar his role was to himself. "I believe in fate. I think you come into this life with some things to accomplish and youre taken out earlier or later depending on the game plan". 

Tony Todd, who played Candyman in the 1992 film Candyman (film)|Candyman, was cast as mortician William Bludworth.  Morgan initially wanted Todd for the role because he felt his deep voice would give the film an eerie tone. 

Additional cast members included Daniel Roebuck and Roger Guenveur Smith as FBI agents Agent Weine and Agent Schreck; Brendan Fehr, Christine Chatelain and Lisa Marie Caruk as students George Waggner, Blake Dreyer and Christa Marsh; Barbara Tyson and Robert Wisden as Barbara and Ken Browning, Alex parents; and Forbes Angus as teacher Larry Murnau. 

  was the location of the Flight 180 explosion, but the crew actually used Vancouver International Airport (above) for the film. ]]

===Filming=== The Guilty during production, and even commented that "  had to share a trailer with Bill Pullman because it was bigger and would make him look more famous".  Smith, who was a regular in Dawsons Creek, had to hold episodes for the film.   
 lifecasts of the actual actors.  The death scenes, the memorial, the forest scene and the scenes in Paris were all filmed in Victoria, British Columbia|Victoria.  Additional scenes were filmed in Toronto and San Francisco.  For the airport, the crew used Vancouver International Airport as a stand-in for John F. Kennedy International Airport, the airport mentioned in the film.   

===Effects===
The plan behind the scenes was to create an intriguing visual signature.  To serve the subtleties of the script and to help personify death, production designer John Willet developed the concept of "skewing" the sets.  "What Ive tried to do with the sets themselves, with their design and with various color choices, is to make things just a little unnatural," Willet explained.  "Nothing that calls attention to itself, but instead creates a sense of uneasiness—the unsettling feeling that somethings not quite right".  To achieve this mystique, Willet designed two versions of virtually every set—one version was used before the crash and the other sets were used for scenes after the jet explodes.

"On the skewed sets I force the perspective either vertically or horizontally," Willet explained. "Nothing is square and, although you cant put your finger on it, it just makes you feel like something is not right".  Skewing was also part of the overall design for the color palette used in set decoration and costume design. "In the real world, the colors are bright and rich," Willet said. "In the skewed world, theyre washed out and faded.  Nothing is obvious, and its only in the overall effect that these subtle differences will work their magic". 

 

The plane scene during which passengers die in mid-air was created inside a very large sound stage. The three-ton hydraulic gimbal was operated automatically.  "We spent two months building this central set piece that weighs about 45,000 pounds and holds 89 people," special effects supervisor Terry Sonderhoff explained. Used for filming the on-board sequences, it could be shifted on the gimbal to create a pitching movement of up to 45 degrees side-to-side and 60 degrees front-to-back, realistically conveying the horror of airborne engine failure.  Sawa said that "the screams of the cast inside the gimbal made it appear more real".  Wong said, "You walk into the studio and theres a huge gimbal with a plane on top and you think, What have I done? I was afraid we were gonna have 40 extras vomiting."   

A miniature model of the Boeing 747 airplane was created for the explosion scene.  The model, one of the most detailed miniature scenes in the film, was about 10 feet long and 7 feet wide, and the landing gear was made from all machined metals.    According to visual effects supervisor Ariel Velasco Shaw, the miniature had to be launched about 40 feet up into the air to make it look like a real Boeing 747 exploded into a fireball.  If blowing up a four-foot plane, the explosion must be a minimum of eight feet in the air.  To film the explosion in detail, the crew used three cameras running 120 frames per second and one camera running 300 frames per second (if they had filmed using a real-time camera, the succession of the explosion would not be filmed in a particular order).  

The train scene (in which Carters car is smashed by the train) was one of the most difficult scenes to shoot. The car used for the crash was a replica of the original, severed in half prior to filming.  According to Sonderhoff, in order to ensure the safety of the actors, they had to make sure that there was no real sheet metal in the car.   

For the death scenes, the crew used several lifecasts of the actors and chocolate syrup for fake blood.  Creating the Rube Goldberg effect for Ms. Lewtons death scene was the most difficult to plan according to the crew.  Perry said that "it was very hard to generate an atmosphere of dread, to create suspense out of scenes that are common".  

===Music===

====Soundtrack==== Into the Joe 90 (during the end credits). 

====Score====
{{Infobox album
| Name = Final Destination: The Complete Original Motion Picture Score
| Type = Film
| Artist = Shirley Walker
| Cover = 
| Released = March 17, 2000
| Recorded = 
| Genre = Film score
| Length = 47:53
| Label = Weendigo Records
| Producer = 
| Chronology = 
| Last album =   (1999)
| This album  = Final Destination: The Complete Original Motion Picture Score (2000)
| Next album =   (2003)
}}
Final Destination: The Complete Original Motion Picture Score was released on March 17, 2000.       The films score was conducted by  .   Walker said, "  are great believers in melody and having music for the characters and situations they find themselves in. Of course, the atmosphere had to be there also, especially for a film with as much suspense building as this film has". 

The score is mostly low-key, with the exception of the suspense and death scenes.  It was performed by a union orchestra, obliging New Line Cinema to grant the film its own score.    Walker described her score as "very theme-driven, conservative music that covers the range from bizarre animal noises with stronger visceral impact to stirring emotional music with well-defined melodies that evolve through the storytelling".  The "Main Title" piece, used for the opening credits, was rare for opening a film aimed at a youth audience at the time. "What a treat for me to get to write a piece that calls you into the movie and lets you know something bad is going to happen from the get go", Walker said.  According to Walker, "Main Title" consumed most of her time, due to its "dark theme and counter melody which carries throughout the score". 

The score was positively received by critics.  Judge Harold Jervais of DVD Verdict wrote how "  Walkers wonderfully creepy and effective score are mixed together to form a very pleasing, almost organic-like whole".    Mike Long of DVD Review said that "Shirley Walker’s eerie score comes across powerfully with a wide spatial integration".  Derek Germano of The Cinema Laser wrote that "Walkers creepy musical score is really a winner, and is one of the things that will help to make Final Destination a minor genre classic a few years down the road".   

==Release==

===Box office=== Erin Brockovich science fiction film Mission to Mars.  The film remained at No. 3 during the second weekend, before dropping to No. 7 on its third weekend.       Final Destination continuously dropped across subsequent weekends until it fell from the top-10 list on its eighth weekend.    The film lasted in theaters for 22 weekends, its last screening airing in 105 theaters and grossing $52,675, placing at No. 56.    Final Destination grossed $53,331,147 in the United States and Canada on its total screening, and earned $59,549,147 in other territories, earning an overall gross of $112,880,294 internationally. 

===Home media===
Final Destination was released on DVD on September 26, 2000, in the United States and Canada.  The DVD bonus features include three audio commentaries, three deleted scenes, and two documentaries.        The first commentary features Wong, Morgan, Reddick, and editor James Coblentz describing the minute subtleties included by the creative team throughout the film, which either allude to death or foreshadow the deaths in the film invisible upon initial airing.  They also discuss how the film was made and how they fought the executives of New Line Cinema over various factors.  

The second commentary includes Sawa, Smith, Cloke, and Donella discussing what was involved in certain scenes and how they each were cast.   The third commentary is the isolated music score of Walker included in the films score.  

Deleted scenes cover two subplots of Alex and Clear, an alternate ending where Alex dies after rescuing Clear from the live wires, Clear bearing a baby which she names Alex, and Clear and Carter finishing as the only survivors of the film.   
 filmographies of the cast and crew.   

==Reception==

===Critical response=== average rating normalized rating out of 100 to reviews from mainstream critics, the film holds a mixed/average score of 36 based on 28 reviews.  On June 14, 2010, Nick Hyman of Metacritic included Final Destination in the websites editorial 15 Movies the Critics Got Wrong, noting that "the elaborate suspense/action set pieces from the first two films are more impressive than most".   

{{quote box width = 35em border = 1px
|align=right
|bgcolor=#ffb6c1 fontsize = 85% quote = "Providing itself some laughs and scares, Final Destination is a flawed but often entertaining teen horror flick". salign = left source =— Marjorie Baumgarten of The Austin Chronicle   
}}
On the negative side, Stephen Holden of The New York Times said that "even by the crude standards of teenage horror, Final Destination is dramatically flat".    Lou Lumenick of the New York Post commented that "the films premise quickly deteriorates into a silly, badly acted slasher movie—minus the slasher".    Kevin Maynard of Mr. Showbiz described the film as "crude and witless",    while Rita Kempley of The Washington Post wrote that "your own final destination just might be the box office, to demand your money back".   

Robert Cashill of Newsweek remarked that the film "should be in video store bins",    and Jay Carr of The Boston Globe commented that it "starts by cheating death and ends by cheating us".    Phoebe Flowers of Miami Herald felt the film "stoops well below substituting style for substance",    whereas Lisa Alspector of the Chicago Reader described the film as "disturbing—if less sophisticated than the best SF (science fiction)-horror TV".    Luke Thompson of the Dallas Observer found it "a waste of a decent premise";    Ernest Hardy of LA Weekly said that the film "fails because it takes itself both too seriously and not seriously enough".    Although Barbara Shulgasser of the Chicago Tribune said that it "met the low standards of a mediocre TV movie",    Desmond Ryan of the Philadelphia Inquirer commented that it was "as full of terrible acting as it is devoid of suspense".    Both Susan Wloszczyna of USA Today and Walter Addiego of the San Francisco Examiner thought it was "stupid, silly and gory".      

In contrast, the film gathered positive reviews from top critics. Roger Ebert of the Chicago Sun-Times enjoyed the film and gave it three out of four stars, stating that "Final Destination will no doubt be a hit and inspire the obligatory sequels. Like the original Scream, this movie is too good to be the end of the road. I have visions of my own".    Mick LaSalle of the San Francisco Chronicle praised the film, saying "  was playful and energized enough to keep an audience guessing".    Joe Leydon of Variety (magazine)|Variety praised the film, saying "  generates a respectable amount of suspense and takes a few unexpected turns while covering familiar territory",    while Kevin Thomas of the Los Angeles Times said it was "a terrific theatrical feature debut for television veterans Glen Morgan and James Wong".    Chris Kaltenbach of The Baltimore Sun found the film "fitfully thrilling",    while Maitland McDonagh of TV Guide defined the film as "serviceable enough, if you come to it with sufficiently modest expectations".   

Despite the films generally mixed reception, critics praised Sawas performance as Alex.  Holden of The New York Times commented that "The disaster and Alexs premonitions set up a heavy-handed fable about death and teenage illusions of invulnerability".  David Nusair of Reel Film Reviews remarked "Sawas personable turn as the hero is matched by a uniformly effective supporting cast rife with familiar faces (i.e. Seann William Scott, Brendan Fehr, Tony Todd, etc)...,"    while Leydon of Variety (magazine)|Variety pointed out that "Sawa is credible as the second-sighted Alex—unlike many other actors cast a teen protagonists, he actually looks like he might still be attending high school—but the supporting players are an uneven bunch".  LaSalle of the San Francisco Chronicle praised Sawa and Ali Larters pairing, saying that "Larter and Sawa, who becomes more scruffy and wild-eyed as the film progresses, make an appealing pair". 

===Accolades=== Young Hollywood Robert McLachlan was nominated for Best Cinematography in a Theatrical Feature at the Canadian Society of Cinematographers Awards in 2001, but lost to Pierre Gill for his work on The Art of War.   

  
The films concept was listed at No. 46 in Bravo (UK TV channel)|Bravos 100 Greatest Scary Moments, in which Smith represented the film.    The Flight 180 explosion scene was included in the lists of best fictional plane crashes or disaster scenes by Break Studios, Unreality Magazine, New Movies.net, The Jetpacker, Maxim (magazine)|MaximOnline, and Filmsite.org.                      Filmsite.org also included the plane scene and the deaths of three characters (Tod, Terry, and Ms. Lewton) in its Scariest Movie Moments and Scenes, and all fatalities in its Best Film Deaths Scenes.       The demise of Detmers character entered the listings of the most shocking deaths on film of George Wales and Simon Kinnear of Total Film (No. 29 and No. 10, respectively), Simon Hill of Eat Horror (No. 10), and Dirk Sonningsen of Mania (No. 10).          {{cite web|last=Mania|first=Mania: Beyond Entertainment|title=10 WTF Movie Character Deaths
|url=http://www.mania.com/10-wtf-movie-character-deaths_article_122631.html|accessdate=April 15, 2012}} 

==See also==
 
* Final Destination 2
* Final Destination 3
* The Final Destination
* Final Destination 5
* Final Destination|Final Destination franchise List of unmade episodes of The X-Files

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 