The Education of Sonny Carson
 
{{Infobox Film
| name           = The Education of Sonny Carson
| image          = Education of sonny carson.JPG
| image_size     =
| caption        = Original theatrical poster
| director       = Michael Campus
| producer       = David Golden Irwin Yablans
| writer         = Sonny Carson Fred Hudson
| narrator       =  Don Gordon Joyce Walker Paul Benjamin Mary Alice
| music          = Coleridge-Taylor Perkinson
| cinematography = Edward R. Brown
| editing        = 
| distributor    = 
| released       = July 17, 1974
| runtime        = 104 minutes USA
| language       = American English
| budget         = $1,000,000
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

The Education of Sonny Carson is a 1974 film based on the best-selling autobiography of Sonny Carson.

==Plot==
The film starts out with the viewers obtaining insight of a young Sonny and some of his early struggles, which include, the first scene in which Sonny and three of his friends are in the process of breaking into a local market to steal food; and money, from the cash register. Sonny is subdued by police and soon finds himself on a "lenient" sentence of three months at the age of thirteen. While incarcerated a young Sonny meets Willie, the leader of a local gang called The Lords. It is then the audience is brought to an older Sonny and we learn he is now heavily involved in the gangs activities and of the rivalry The Lords have with a fellow gang called The Tomahawks. Sonny is now entrenched in the every day life of a Lord which includes frequent brawls with their rivals The Tomahawks. During one of these brawls one of Sonnys friends a fellow Lord by the name of Lil Boy gets fatally stabbed in the back with a stiletto. Sonny and the rest of the gang show up at Lil Boys wake, and, its when they leave that we are brought to the attention of Sonny trying to purchase flowers from a local shop but couldnt afford a bouquet.

He then proceeds to rob a white man carrying a telegram with change of $100 in it. He then purchases the flowers and places them on Lil Boys casket. This crime ends up getting Sonny hemmed up and we bear witness to him nearly getting beaten to death by police in an interrogation room. He is then sentenced to no less than one year and no more than 3 upstate. Here he re-connects with his friend Willie who informs Sonny of the harsh realities of prison life. During his time incarcerated Sonnys father visits giving him emotional hope that he will survive and that his dad and family are still supporting him. However the harsh social condition during the time also lead to a gruesome incarceration and Sonny bears witness to the guards frequent brutality which includes Willie nearly being beaten to death. After the savage beating Willie informed Sonny that he could no longer sustain but he would "At least take one of them with him" the next scene shows the guards pulling Willie out of his room at night and proceeding to throw him over the rail causing his death.

After these traumatic experiences we see Sonny finally escape the horrors of prison and get a celebratory greeting by his family. It is afterwards however when Sonny is looking for members of his old gang that he learns that drugs have hit the streets hard and have taken most of his former friends (and his girlfriend) with them. He puts his priorities in order and finds a "born again" purpose in life under his new name, Iwina Lmiri Abubadika. The film ends in the 1970s, long before Abubadikas controversial involvement in New York politics. 

==Background==
The Education of Sonny Carson had a budget of $1 million.  Due to this low budget the director was forced to use new and creative styles of directing, using kids from the neighborhood and simple “dark” rooms to imply interrogation rooms and jail cells and such.  In order to portray Sonny Carsons violent gang initiation, the film camera was placed in a metal cage. The scene was filmed from two points of view: one of Sonny running through a row of gang-members beating him with chains and clubs, the other from Sonnys own perspective as he was being humiliated and injured in order to join the gang.

Michael Campus has stressed over this low budget and says he didnt know how he did it. The film explores social issues facing the African American population during their struggles to obtain civil rights, and sheds light on both political and social travesties at the time such as poverty, drug abuse, violence and police discrimination.

==Influence== Prodigy of Mobb Deep, AZ (rapper)|AZ, Common (rapper)|Common, Ghostface Killah, Lauryn Hill and 2 Chainz have sampled dialogue from the film in their music.

==External links==
*  

==References==
 

 
 
 
 
 