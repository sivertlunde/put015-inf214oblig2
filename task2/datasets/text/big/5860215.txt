Ang Tanging Ina
 
{{Infobox film
| name           = 
| image          = AngTangingInaDVDCover.jpg
| caption        = Ang Tanging Ina DVD Cover Wenn Deramas
| producer       = Elma Madua
| writer         = Mel Mendoza del Rosario Keiko Aquino
| starring       = Ai-Ai de las Alas Marvin Agustin Carlo Aquino Nikki Valdez Heart Evangelista Eugene Domingo Alwyn Uytingco Shaina Magdayao Serena Dalrymple Jiro Manio Kaye Abad Angelica Panganiban John Prats
| music          = Jesse Lasaten
| editing        = Vito Cajili
| cinematography = Sherman So
| studio         = Star Cinema
| distributor    = Star Cinema
| runtime        = 95 minutes
| released       =  
| country        = Philippines
| language       = Filipino Tagalog
| gross          = P178.09-M
| This Movie    = Ang Tanging Ina (2003)
| successor   = Ang Tanging Ina Nyong Lahat  (2009)

}}
Ang Tanging Ina (English: The Only Mother) is a 2003 Filipino comedy film, starring Ai-Ai de las Alas and Eugene Domingo. The movie was the highest grossing Filipino film until it was surpassed by Sukob in 2006. It currently ranks No. 6 after You Changed My Life (2009), Sukob (2006), its sequel Ang Tanging Ina Ninyong Lahat (2008–2009), Kasal, Kasali, Kasalo (2006–2007), and A Very Special Love (2008). Each film was produced or co-produced by Star Cinema.

The film is followed by a television series of the same name and three sequels entitled Ang Tanging Ina Nyong Lahat, Ang Tanging Ina Mo (Last na To!), and Enteng Ng Ina Mo.

== Synopsis ==
With three dead husbands and twelve children to take care of, what is the modern day working mother supposed to do? Ina Montecillo (Ai-Ai de las Alas) is completely clueless on how to be an income provider and a homemaker at the same time given her dwindling finances of her household. As she desperately hides from her children her efforts to make all ends meet,  her children grow resentful of her as she becomes a part of their own problems, but it seems that her best is never good enough. Funnily yet, Ina rallies her cause with all the courage she can muster to be the best mother she knows how.

== Plot ==
Ina Montecillo (Ai-Ai de las Alas) meets Tony (Edu Manzano), whom she considers "the man of her dreams". They both have four children, but Tony dies shortly after falling from a stool. After finding a replacement father for her children, she meets Alfredo (Tonton Gutierrez), and they have four more children. Alfredo dies after falling from a pedestrian overpass, and Ina meets Kiko (Jestoni Alarcon), who fathers her other four children. On their wedding day, Kiko gets electrocuted and Ina decides to stop finding another husband.

One morning, Ina wakes up finding her children Juan (Marvin Agustin), Tudis (Nikki Valdez), Tri (Carlo Aquino), Por (Heart Evangelista), Pip (Alwyn Uytingco), Six (Marc Acueza), Seven (Shaina Magdayao), Cate (Serena Dalrymple), Shammy (Jiro Manio), Ten-Ten (Yuuki Kadooka), Connie and Sweet either troubled, problematic, panicking, fighting, or confused. Later on, she finds out that her family might go poor, and so works several jobs from construction to selling bootleg DVDs just to make ends meet. 

She is reminded by Por of her Débutante|début, while Juan begs her permission to let him find work. Ina agrees, but finds Juans chores at home very confusing. Ina is told by her ex-driver Bruno, who reveals he is gay, that she can earn Philippine peso|₱2,000 a night at a stripper club. Meanwhile, Juan meets his high school girlfriend Jenny (Kaye Abad) working at an amusement park, where he decides to apply. Tudis quits her job and tries to pursue her desired career as an artist. Tri tries to impress his girlfriend Gretchen (Angelica Panganiban) and her parents (Pinky Amador and Mandy Ochoa) with his intelligence. Por tries to convince her crush, Jeffrey (John Prats), to be escort her at her début. Pip, who is a closeted homosexual, spies on his crush doing his exercise routine, while Six invites their mother to a mass treat. Seven was assigned to lead a school programme on Bakit Natatangi Ang Aking Ina ("Why My Mother Is Unique") while Shammy tries to protect his reputation from his classmate, Nhels brother, because he is not yet Circumcision|circumcised. Pip prevents them from fighting and when Nhel arrives, he accidentally mentions that Shammy is uncircumcised, causing Shammy to hate him.
 high fever cabbie Eddie (Dennis Padilla) to fall in love with her. During their subsequent family meeting, she emotionally explains to the children that she took on several jobs not because it was her obligation, but because she loves them. Her complicated speech has the opposite effect, with Juan deciding to run away with Jenny. Rowena (Eugene Domingo) comforts the careworn Ina, who thinks of how to solve everything.

After quitting her job at the club, Ina takes a bus ride where she sees a   together, so that he can communicate with the family.

== Cast ==
*Ina Montecillo (Ai-Ai de las Alas): Ina is the wife of Alfredo, Tony, Kiko, and Eddie, and is Juan, Tudis, Tri, Por, Pip, Six, Seven, Cate, Shammy, Ten-Ten, Connie & Sweets mother. She is Rowenas best friend. She loves her children very much, and will do anything to fulfill their desires. She has worked as a construction worker, illegal DVD seller, vendor, stripper, and other jobs you can think of. She later becomes the savior of several passengers in a bus explosion. In the end, she solves all the problems of her children, and her children becomes proud of her once more. She also marries Eddie, but they cant have another child because of Eddie being involved in a fireworks accident.

*Rowena (Eugene Domingo): Rowena is Inas best friend. She is very supportive towards her, and will do anything to make Ina happy.

*Juan Montecillo (  for a new life.

*Getrudis "Tudis" Montecillo (  where she meets the man of her life.

*Dimitri "Tri" Montecillo (Carlo Aquino): Tri is Inas third child. He is a smart student in his college, and was dating Gretchen. But when Gretchens parents figured out his mother was a stripper, Gretchen breaks up with him. This spoils him, and decides to absent school days. In the end, Ina fixes their relationship, and he and Gretchen get back together. He also decides to continue his studies.

*Portia "Por" Montecillo (  as a missionary.

*Tirso "Pip" Montecillo ( , and his family doesnt know it either. He is in love with Nhel. In the end, Ina accepts his personality, and he & Nhel begin dating.

*Sixto "Six" Montecillo (Marc Acueza): Six is Inas sixth child. He is a smart student, and is fond of declamations and orations. When he invites his mother to a "mass treat", Ina misinterprets it as a "trick or treat" event, humiliating him. In the end, he forgives his mother. Years later, he migrates to United Kingdom to work there as a nurse.

*Severina "Seven" Montecillo (Shaina Magdayao): Seven is Inas seventh child. She is also an intelligent student, and is very fond of essays and programs. But when she becomes the leader of a program with the theme "Bakit Natatangi Ang Aking Ina (Why My Mother Is Very Special)", she becomes afraid Ina will humiliate her. In the end, she finishes the program, and becomes proud of her mother once more.

*Catherine "Cate" Montecillo (Serena Dalrymple): Cate is Inas eighth child. She is a bit of a tomboy, and doesnt attend her school frequently. She causes some of the problems of her siblings such as Sixs mass treat and Ten-Tens disappearance.

*Samuel "Shammy" Montecillo ( , another problem to the family. In the end, he says sorry to his mother for being impatient, and later was cured.

*Martin "Ten-Ten" Montecillo (Yuuki Kadooka): Ten-Ten is Inas tenth child. He was revealed to be deaf by Cate, surprising his mother. He was also lost one morning, worrying his family. Later on, Ina finds him in the local church. Also in the end credits, his family teaches him the sign language to communicate with them.

*Jenny ( . Later on, Juan proposes to her, which she accepts. After Juan was scolded by his mother, they both escape their families and lived their own life. But after finding out Juan wasnt ready yet, they both decide to return to their respective homes. In the end, she and Juan continued their relationship. Years later, she and Juan migrated to New Zealand for a new life.

*Gretchen (Angelica Panganiban): Gretchen is Tris high school girlfriend. She comes from a rich family, wherein her parents gives the best for her. After her parents find out Ina was a stripper, she decides to break up with him. Later on, Ina pleads to her to continue her relationship with Tri. In the end, she and Tri get back together.

*Jeffrey (John Prats): Jeffrey is Pors high school crush. He also becomes Pors escort at her début. Years later, Por breaks up with him.In the second tanging ina, he became the boyfriend of Seven at first Ina doesnt agree with their relationship but comes to understand it in the end.

*Eddie (  whom Ina met when Shammy was in the hospital. He was in love at first sight with Ina. He was later involved in a fireworks accident, damaging his bottom. He marries Ina, but because of the accident, they can no longer have another child.

*Tony (Edu Manzano): Tony is Inas first husband. He is Juan, Tudis, Tri & Pors father. During a gamble, Tony is blocked by spectators. He stands on a stool, trying to see the two fighting insects. He later outbalances, breaking his spinal cord & column.

*Alfredo (Tonton Gutierrez): Alfredo is Inas second husband. He is Pip, Six, Seven & Cates father. He had a fatal fall from an overpass, breaking his back.

*Kiko (  day, the bells ribbons were electrified accidentally. Thus, when he pulled the ribbon, he was killed shortly.

*Gretchens Parents (Pinky Amador) and (Mandy Ochoa): Gretchens parents are well-behaved, and will give anything for their daughter. At first, they liked Tri, but when they saw Ina as a stripper in the club, they convinced their daughter to break-up with Tri.


Uncredited:

*Connie & Sweet Montecillo: Portrayed by twin baby girls, Connie & Sweet are minor characters in the film. They didnt cause too much distress to Ina, but are very frequent in crying. (Note: They are important characters for they are Inas eleventh and twelfth child, and is a major character in the sequel Ang Tanging Ina Nyong Lahat, wherein twins Bianca & Janelle Calma portrayed them.)

==Sequel==
 
In 2008, the Star Cinema brought the second installment of Ang Tanging Ina film series which still starred Ai-Ai de las Alas, Eugene Domingo, and several others. The film revolves around Ina (Ai-Ai de las Alas) who lately became the President of the Philippines and overturns the Philippines by ruling it into a whole new dimension of enjoyment which eventually causes her a lots of serious problem about the country and with her family as well.

==Ang Tanging Ina (TV series)==
{{Infobox television|
| show_name = Ang Tanging Ina
| image =
| genre = Comedy, Drama
| runtime = 30–45 minutes
| creator = ABS-CBN
| starring = Ai Ai de las Alas Marvin Agustin Nikki Valdez Carlo Aquino
| country = Philippines
| network = ABS-CBN
| first_aired =  
| last_aired =  
| num_episodes =
| website =
}}

After the hit blockbuster movie also became the hit TV sitcom (2003–2005).

===Cast===
* Ai-Ai de las Alas as Ina Montecillo
* Marvin Agustin as Juan Montecillo
* Nikki Valdez as Getrudis "Tudis" Montecillo
* Carlo Aquino as Dimitri "Tri" Montecillo
* Heart Evangelista as Portia "Por" Montecillo
* Alwyn Uytingco / Ketchup Eusebio as Tirso "Pip" Montecillo
* Marc Acueza as Sixto "Six" Montecillo
* Shaina Magdayao as Severina "Seven" Montecillo
* Serena Dalrymple as Catherine "Cate" Montecillo
* Jiro Manio as Samuel "Shammy" Montecillo
* Yuuki Kadooka as Martin "Ten-ten" Montecillo
* Eugene Domingo as "Rowena"
* Dennis Padilla as "Eddie"
* Roderick Paulate as "Goliath" - Inas long-lost brother
* Tuesday Vargas as "Kring-Kring"
* John Estrada as "Geronimo"
* Bentong and Archie Alemania as Guest Stars
* Baron Geisler as Chaos Montecillo

==External links==
* 

 
 
 
 
 
 
 
 
 
 