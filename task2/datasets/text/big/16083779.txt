The Challenge (1982 film)
{{Infobox film
| name           = The Challenge
| image          = The Challenge1982.jpg
| image_size     =
| caption        =
| director       = John Frankenheimer
| producer       = Ron Beckman Robert L. Rosen
| writer         = Richard Maxwell Marc Norman John Sayles
| starring = {{plainlist|
* Scott Glenn
* Toshirō Mifune
* Donna Kei Benz
}}
| narrator       =
| music          = Jerry Goldsmith
| cinematography = Kozo Okazaki Jack Wheeler John W. Wheeler
| studio         = CBS Theatrical Films
| distributor    = Embassy Pictures
| released       =  
| runtime        = 112 min.
| country        = United States
| language       =
| budget         =
| gross          = $3,600,000
}}
 American action film directed by John Frankenheimer and co-written by John Sayles. The film stars Scott Glenn and Toshirō Mifune.

==Plot==
The sword (one of a pair known as "The Equals") was a family heirloom, passed down through the generations, and had been lost during World War II. It was finally tracked down in California where Yoshiidas son, Toshio, finds it. He looks for someone to hire to act as a decoy in order to safely smuggle it into Japan, and back to its rightful owner, Yoshida-san. Rick Murphy (Scott Glenn) is contracted to smuggle a rare sword into Japan, only to find upon his arrival that it is a fake. Aggravated that he has been used as a decoy, he is faced with the prospect of being killed by Yoshida-sans (Toshirō Mifune) brother, Hideo. Instead, he is advised to infiltrate Yoshidas martial arts school and obtain the sword. He does so, yet finds himself being drawn into the ways of Japanese etiquette and tradition to the point where he returns the sword to Yoshida himself after having the perfect opportunity to escape with it. Murphy then humbly asks Yoshida-san if he can be forgiven and taken back in because he wants to learn the ways of Bushido. Yoshida agrees, but only if Murphy follows Yoshidas conditions.

Murphy continues to bumble his way through life at Yoshida-sans school until, after a treacherous and almost fatal attempt by one of the higher members of the school to steal the sword, he leaves and is found in a hotel in Kyoto by Akiko, Yoshidas only daughter. Finding romance, they go out to see the sights and sounds of the city, including watching a Shinto ceremony. During the hub-bub of the crowded parade, Murphy and Akiko gets separated and Hideos henchmen kidnap her and deliver her to her uncle, Hideo. Yoshida-san, laden with ancient weaponry, ventures out to Hideos industrial complex where he is shot and wounded by Ando (Calvin Jung), the lead henchman. Ando is slain by Hideo for this, and Murphy – who has joined him in his quest – opts to fight Hideo to defend his sensei. Murphy manages to defeat Hideo and win the day.

==Alternate version==
A re-edited version of the film entitled Sword of the Ninja was created for television. In this version, about ten minutes of footage are cut, some of the graphic violence of the original version is removed, and "fades" are added to make room for commercial breaks.

==Reception==
Opening Weekend
$804,512 (USA) (25 July 1982) (239 Screens)

Gross
$3,534,852 (USA) (19 September 1982)
$2,977,706 (USA) (6 September 1982)
$804,512 (USA) (25 July 1982)
$3,534,852 (USA)
$412,101 (Germany)

Weekend Gross
$116,724 (USA) (19 September 1982) (70 Screens)
$522,668 (USA) (6 September 1982) (228 Screens)
$804,512 (USA) (25 July 1982) (239 Screens)

Rotten Tomatoes score: 75%
Avg. rating: 3.8/5

==Behind the scenes==
The Kyoto International Conference Center was used as the location of Hideos headquarters.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 