The Fantastic All-Electric Music Movie
{{Multiple issues|
 
 
}}
 The Sonny and Cher Show during the early to mid-1970s.

The film was released on home video in 1981 by Video Gems under license from Fine Arts Films, an animation company founded by John Wilson in 1955. As well as the sixteen animated music videos from the show, the film includes an animated short based on Igor Stravinskis Petrushka (ballet)|Petrushka. 

==Starring musical voices==
*Joni Mitchell
*Helen Reddy
*Jim Croce
*Covin
*Cher
*Sonny and Cher
*Sonny Bono Melanie
*Wayne Carpenter (Kinks)
*Stan Kentons Big Band Latin Rhythm

==Songs featured==
*Big Yellow Taxi 
*Angie Baby
*Both Sides Now
*Bad, Bad Leroy Brown 
*One Tin Soldier 
*Dark Lady 
*Gypsy Rose 
*Candy Man 
*Cowboys Work 
*Brand New Key 
*Black and White 
*Demon Alcohol 
*Higher Ground 
*You and Me Babe (love story) 
*Reachin 
*Congo Valentine

==Content description==
Animator John Wilson produced animated music video shorts for original songs and covers of original songs sung by Sonny Bono and Cher. The songs "Big Yellow Taxi" and "Both Sides Now" are sung by Joni Mitchell. "Higher Ground" is covered by Sonny and Cher.

The animated music video for Joni Mitchells Both Sides Now utilizes ground-breaking computer animation.

==Distribution==
Television: CBS TV Sonny and Cher Show 1973-77
Home Video: Video Gems 1981 VHS

==References==
 

 
 
 
 
 
 
 
 


 