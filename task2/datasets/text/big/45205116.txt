Simham Navvindi
{{Infobox film
| name           = Simham Navvindi
| image          =
| caption        =
| writer         = Paruchuri Brothers  
| story          = D. Yoganand
| screenplay     = D. Yoganand 
| producer       = Nandamuri Harikrishna
| director       = Dasari Yoganand|D. Yoganand
| starring       = N. T. Rama Rao  Nandamuri Balakrishna  Kala Ranjani  Chakravarthy
| cinematography = Nandamuri Mohana Krishna
| editing        = R. Vithal
| studio         = Ramakrishna Cine Studios   
| released       =  
| runtime        = 2:07:30
| country        =   India
| language       = Telugu
| budget         =
| gross          = 
}}

Simham Navvindi ( , Comedy film produced by Nandamuri Harikrishna on Ramakrishna Cine Studios banner, directed by Dasari Yoganand|D. Yoganand. Starring N. T. Rama Rao, Nandamuri Balakrishna, Kala Ranjani in the lead roles and music composed by K. Chakravarthy|Chakravarthy.      

==Cast==
 
*N. T. Rama Rao as Narasimham
*Nandamuri Balakrishna as Balakrishna
*Kala Ranjani as Radha Allu Ramalingaiyah as Parvathalu
*Nutan Prasad as Lingaiah
*Thyagaraju  Rallapalli as Gantaiah
*KK Sarma as Girisam
*Prabha as Swapna Annapurna as Balakrishnas grandmother
*Mamatha as Kanakamma
*Kakinada Shymala as Narasimhams sister
*Sri Lakshmi as Buchamma
 

==Soundtrack==
{{Infobox album
| Name        = Simham Navvindi
| Tagline     = 
| Type        = film Chakravarthy
| Cover       = 
| Released    = 1983
| Recorded    = 
| Genre       = Soundtrack
| Length      = 23:28 
| Label       = AVM Audio Chakravarthy
| Reviews     =
| Last album  = Bezawada Bebbuli   (1983)  
| This album  = Simham Navvindi   (1983)
| Next album  = Chanda Sasanudu   (1983)
}}

Music composed by K. Chakravarthy|Chakravarthy. Music released on AVM Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !! Lyrics !!Singers !!length
|- 1
|Guvva Guvva Ekkadike
|C. Narayana Reddy  Nandamuri Raja, S. Janaki
|3:40
|- 2
|Hey Bhamchuku Bham 
|C. Narayana Reddy Nandamuri Raja, P. Susheela
|3:49
|- 3
|Jabili Vachindi 
|C. Narayana Reddy Nandamuri Raja,S. Janaki
|4:05
|- 4
|Munjalanti Chinnadana Veturi Sundararama Murthy SP Balu,P. Susheela
|4:00
|- 5
|Okkasari Navvu 
|C. Narayana Reddy Nandamuri Raja,S.Janaki
|3:54
|- 6
|Yela Yela Neekundi 
|C. Narayana Reddy Nandamuri Raja,S. Janaki
|4:00
|}
   

==Others==
* VCDs and DVDs on - Universal Videos, SHALIMAR Video Company, Hyderabad

==References==
 

 
 
 


 