Bhagawat
{{Infobox film
| name           = Baghavat
| image          = 
| alt            =  
| caption        = 
| director       = Ramanand Sagar
| producer       = Ramanand Sagar
| writer      
| screenplay     =  
| story          =
| starring       = Dharmendra Hema Malini Reena Roy Amjad Khan Padma Chavhan
| music          = Naushad Return
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}

Bhagavat (1982) is a Hindi language film starring Dharmendra, Amjad Khan, Hema Malini and Reena Roy. The film is directed by Ramanand Sagar.  It became a "semi-hit" at the box office. 

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Ae Mere Deewane Dil"
| Asha Bhosle
|-
| 2
| "Mere Mehboob Tujhe Salam"
| Mohammed Rafi, Asha Bhosle
|-
| 3
| "Duniya Se Duniyanwalon Se"
| Mahendra Kapoor, Asha Bhosle
|-
| 4
| "Yeh Apna Yarana Bhool Nahin Jana"
| Rajeshwari
|-
| 5
| "Nagan Sa Roop Hai Tera"
| Mohammed Rafi
|-
| 6
| "Banjare Hum Banjare"
| Shabbir Kumar, Asha Bhosle
|}
==References==
 

==External links==
* 

 
 
 
 

 