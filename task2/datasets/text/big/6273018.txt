Pas în doi
{{Infobox film
| name           = Pas în doi
| image          = 
| image size     = 
| caption        = 
| director       = Dan Pița
| producer       = Vily Auerbach
| writer         = Dan Piţa
| starring       = Claudiu Bleont
| music          = 
| cinematography = Marian Stanciu
| editing        = Cristina Ionescu
| distributor    = 
| released       = 1985
| runtime        = 
| country        = Romania
| language       = Romanian
| budget         = 
}}

Pas în doi, also known as Passo Doble, is a 1985 Romanian film directed by Dan Pița. It stars Claudiu Bleonț, Petre Nicolae, Anda Onesa and Ecaterina Nazare. It was entered into the 36th Berlin International Film Festival where it won an Honourable Mention.   

==Synopsis==
The film is a love story about a rebel with a cause&ndash;a rebel who cannot decide between a pure, innocent burning love and a docile, peaceful, quiet love.

==Cast==
* Claudiu Bleont
* Petre Nicolae
* Ecaterina Nazare
* Anda Onesa
* Mircea Andreescu
* Valentin Popescu
* Tudorel Filimon
* Camelia Maxim
* Lucretia Maier

==References==
 

==External links==
 

 
 
 
 
 
 


 
 