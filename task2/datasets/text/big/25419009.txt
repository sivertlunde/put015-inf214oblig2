Supari (film)
{{Infobox film
| name = Supari
| image =Supaari.jpg
| caption = Star cast in the film
| director = Padam Kumar
| producer = Padam Kumar
| producer(Canada)= Roger Nair
| studio= Lionheart Production House
| writer = Padam Kumar Anuradha Tiwari
| narrator =
| starring = Uday Chopra, Rahul Dev, Nandita Das, Purab Kohli, Nauheed Cyrusi, Irrfan Khan
| music = Shekhar Ravjiani Vishal Dadlani
| cinematography = Velraj
| editing = Sanjay Verma
| distributor = Aum Creations
| released = 20 June 2003
| runtime = 152 minutes
| country = India
| language = Hindi
| budget =
| gross =
| gross adjusted =
| preceded by =
| followed by =
}} directed and produced by Padam Kumar and mostly shot in Canada, in and around Toronto, Niagara on the lake and Niagara Falls, ON by Roger Nair for Lionheart Production House. This was the first Bollywood film to be available online on a website Kazaa after the release.Uday Chopra, Rahul Dev, Nandita Das, Purab Kohli, Nauheed Cyrusi and Irrfan Khan star in lead roles. The story is set in the Mumbai underworld.

==Plot==
Aryan Pandit lives a middle-class lifestyle with his family in Nasik, India, where the entire family depend on his salesman fathers earnings. He relocates to Mumbais St. Andrews College with big dreams of being wealthy and driving a red Ferrari. He befriends three other middle-class youth: Papad, Mushy, and Chicken. He borrows money from Matka Rajan, gambles it, loses everything, and is unable to re-pay. As a result Rajan sets his goons on him. 

Aryan goes to a man named Baba for help, who in turn takes him to Mamta Sekhari. Sekhari offers to hire him as a hit man, agreeing to pay him 20,000 rupees for every killing, so that he can re-pay his gambling loan. Aryan reluctantly accepts, is trained by Baba to shoot a gun, and kills his first target, none other than Rajan himself. He tells his friends about his good fortune, and they join forces with him. In all killing contracts they are provided with a photograph, the location, and are instructed to look at the photo just 15 minutes before killing the person. In this manner the friends enjoy their new-found wealth. When Aryans Parsi girlfriend, Dilnawaaz finds out, she wants him to quit. He does, but his friends refuse to let go of this easy money and luxurious life. Shortly thereafter, the trio are given a contract; before the killing they look at the photograph ... of Aryan. The trio knows that it is too late to back out of this supari (contract killing). Soon events takes a worst turn as one by one gets killed. Chicken commits suicide and a gun battle ensues where Papad and Mushy are killed with Sekharis men. Aryan puts everything to an end by killing Mamta Sekhari and Baba once and for all.

==Cast==
* Uday Chopra &mdash; Aryan Pandit
* Rahul Dev &mdash; Papad
* Nandita Das &mdash; Mamta Shekari
* Purab Kohli &mdash; Chicken
* Nauheed Cyrusi &mdash; Dilnawaaz Dillu
* Irrfan Khan &mdash; Baba 
* Akashdeep Saigal (Sky Walker) &mdash; Mushy

==Crew==
* Director: Padam Kumar
* Producer: Padam Kumar
* Studio:   Roger Nair
* Canada:   Lionheart Production House
* Music: Shekhar Ravjiani, Vishal Dadlani
* Lyrics:
* Cinematography: Velraj|T. Ramji Velraj
* Film editing: Sanjay Verma
* Art direction: Pramod Guruji Vinod Guruji  

==References==
 

==External links==
* 

 

 
 
 
 
 
 