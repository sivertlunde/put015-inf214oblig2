Manzil (1960 film)
 
{{Infobox film
| name = Manzil
| image =
| caption =
| director = Mandi Burman 
| producer = Kalpana Pictures
| writer = Vrajendra Gaur
| starring = Dev Anand Nutan Achala Sachdev K.N. Singh Pratima Devi Krishan Dhawan
| music = Sachin Dev Burman|S.D. Burman
| cinematography = Nariman A. Irani
| editing = Dharamvir
| distributor =
| released =  
| runtime =
| country = India Hindi
| budget =
}}
Manzil (English: Destination) is a 1960 Bollywood film directed by Mandi Burman. It stars the popular duo Dev Anand and Nutan in the leading roles. Krishan Dhawan, Achala Sachdev and K.N. Singh star in supporting roles. It was declared "average", taking the seventeenth spot at highest grossing, earning 760,000 rupees at the box office. 

==Plot== Simla in 1929, Rajkumar Mehta (Dev Anand), or Raju as he is lovingly called, has just returned from England. He meets his childhood friend, Pushpa (Nutan), and tells her about the situation at home. Rajus father, Mehta (K.N. Singh) wanted him to continue with the family business, but Raju ended up learning music instead. This leads to constant tiffs between the two, as Mehta considers music a profession for beggars. Raju doesnt show it, but he is in love with Pushpa, and she loves him back. More trouble stirs when Captain Prem Nath (Krishan Dhawan) expresses his will to marry Pushpa.

Raju and Pushpa go to a bar one night, and meet the Captain there. The Captain doesnt recognize Raju, and Raju starts playing on the piano, evidently jealous. Pushpa somehow convinces the Captain to go away, and the two sing, Aye Kash Chalte Milke. A few days later, when the Captain comes visiting, (Pushpa is staying in Rajus house), he recognizes Raju, and says that when he saw him play the piano at the bar, he thought Raju was the bandmaster. Mehta hears this and is enraged. A few hours later, Raju comes out of his room, only to see his piano being thrown out. In a fit of rage, Raju leaves the house for Bombay.

Pushpa tries to convince him not to go, but Raju leaves anyway, and promises to take her along when he becomes successful. In Bombay, Raju struggles to find accommodation, and a tourist guide he meets ends up robbing him. Thankfully, the paan-seller offers him a room, and a wealthy prostitute, Tiltibai (Zebunissa) comes looking for Raju. She takes him to her house, impressed by his performance, but tries to woo him. Raju slowly starts to find success, but resists Tiltibais advances towards him. Embittered, Tiltibai destroys the letters between Raju and Pushpa.

One day, Pushpas mother (Pratima Devi), asks her brother, Mangal (Badri Prasad) to check up on Raju. Mangal comes back and tells Pushpa that he saw Raju in the company of a prostitute. Pushpa refuses to believe her uncle and goes to Bombay, only to see the same. Shattered, she returns and gets married to the Captain.

Finally, Raju becomes successful, and after composing the music score for a film, returns to Simla, only to see Pushpa getting married. His sister, Shoba Mehta (Achala Sachdev) sees him, and Raju is shocked when she tells him that Pushpa saw him with Tiltibai and thought she had lost him for good. Raju becomes an alcoholic, and doesnt care for his newfound wealth or fame. Pushpa, too, is unhappy with her marriage, because she still loves Raju, but tries to be faithful to her husband.

Raju finally meets Pushpa, and denies having any relationship with Tiltibai. Before Pushpa can say anything, the Captain finds them together. Believing that Pushpa was cheating on him, he draws a gun and tries to shoot Raju. Will the Captain succeed or will the two lovers be united?

==Music==
The music of Manzil wasnt a big hit when it was released, but now it is considered one of the best works of S.D. Burman. Two songs in particular, Aye Kash Chalte Milke and Chupke Se Mile Pyaase Pyaase, are regarded as classics. The lyrics were penned by Majrooh Sultanpuri.
{{Infobox Album |  
 Name = Manzil
| Type = Album
| Artist = Sachin Dev Burman
| Cover = 
| Released = 1960
| Recorded =  Feature film soundtrack
| Length = 
| Label = HMV-Saregama
| Producer = Sachin Dev Burman
}}

===Songs===
{| border="2" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Chupke Se Mile Pyaase Pyaase
| Mohammed Rafi and Geeta Dutt
|-
| 2
| "Aye Kash Chalte Milke
| Manna Dey and Asha Bhosle
|-
| 3
| "Dil To Hai Deewana Na"
| Mohammed Rafi and Asha Bhosle
|-
| 4
| "Yaad Aa Gayi Woh Nashili Nigahen" Hemant Kumar
|-
| 5
| "Are Hato, Kahe Ko Jhooti"
| Manna Dey
|-
| 6
| "Humdum Se Gaye Humdum Ke Liye"
| Manna Dey
|-
| 7
| "Yaad Aa Gayi Woh Nashili Nigahen (Sad)" Hemant Kumar
|}

==References==
 

==External links==
*  

 
 
 
 