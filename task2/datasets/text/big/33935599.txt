La Femme et le Pantin
{{Infobox film
| name           = La Femme et le Pantin / Femmina
| image          = La_femme_et_le_pantin.jpg 
| caption        = 
| director       = Julien Duvivier 
| producer       = Christine Gouze-Rénal Fred Surin
| writer         = Albert Valentin    Marcel Achard     
| based on       =    | }}
| starring       = Brigitte Bardot
| music          =  Jean Wiener José Rocca
| cinematography = Roger Hubert
| editing        = Jacqueline Sadoul
| distributor    = 
| released       =      
| runtime        = 100 minutes
| country        = France  Italy
| awards         =  French
| budget         =  gross = 2,453,892 admissions (France) 
}}

La Femme et le Pantin ( ) is a 1959 French-Italian drama film  directed by Julien Duvivier.   adaptation of the correspondent classic novel.  
At first glance Brigitte Bardot was predestined for this film because for many people she had become downright the incarnation of a femme fatale. Yet for that very reason everybody knows right from the start how the story will end. Eva Marchand walks the streets so proudly and so obviously aware of her attractivity that there is no doubt she knows it too. There is even a scene where a grown man spontaneously kisses her feet and she couldnt be less impressed. She shows just enough reaction to prove that her feet are not wooden. So when the ill-fated Don Matteo Diaz (Antonio Vilar) gets obsessed with her and doesnt even try to hide that, it must become his downfall, for it cannot be any other way.

==Plot==
Matteo Diaz is a wealthy gentleman who loves and respects his wife but doesnt find her attractive any longer because she is Paralysis|paralysed. He already has a certain reputation among women. Accordingly he doesnt hesitate to get close to Eva Marchand as soon as he has realised her beauty. But to his surprise she cant be bothered to show any appreciation for his advances. He begins trying to explain and justify his way of life and his interest in her. The more he tries, the harder she makes it on him. Eventually he is so humiliated that even his walk shows he is a broken man and that is the end of story.

==Production== crew used to call the director "Dudu" but despite that their rapport wasnt always perfect during the making of this film. She also stresses how the shooting in Sevilla during the Seville Fair became a hardship because of the heat.

==Cast==
{|width="100%" align="center"
|width="45%" valign="top"|
* Brigitte Bardot : Éva Marchand
* Antonio Vilar : Don Matteo Diaz
* Lila Kedrova : Manuela
* Daniel Ivernel : Berthier
|width="45%" valign="top"|
* Dario Moreno : Arbadajian
* Jacques Mauclair : Stanislas Marchand
* Jess Hahn : Sidney
|}
==References==
 
==External links==
* 
* 

== Further reading ==
 

 

 
 
 
 
 
 
 
 