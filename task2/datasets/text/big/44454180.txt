Here Comes the Band (film)
{{Infobox film
| name           = Here Comes the Band
| image          = 
| alt            = 
| caption        =  Paul Sloane
| producer       = Lucien Hubbard
| screenplay     = Paul Sloane Ralph Spence Victor Mansfield  Ted Lewis Virginia Bruce Harry Stockwell Ted Healy Nat Pendleton Edward Ward
| cinematography = Charles Edgar Schoenbaum
| editing        = Frank E. Hull 	
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Paul Sloane Ted Lewis, Virginia Bruce, Harry Stockwell, Ted Healy and Nat Pendleton. The film was released on August 30, 1935, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Ted Lewis and His Orchestra as Orchestra Ted Lewis as Ted Lowry
*Virginia Bruce as Margaret
*Harry Stockwell as Ollie Watts
*Ted Healy as Happy 
*Nat Pendleton as Piccolo Pete
*Addison Richards as Colonel Wallace Donald Cook as Don Trevor
*George McFarland as Spanky Lowry 
*Robert McWade as Judge
*Henry Kolker as Simmons Attorney
*Robert Gleckler as Simmons
*Richard Tucker as Jim 
*Bert Roach as Drummer in Band
*Tyler Brooke as Dentist
*Ferdinand Gottschalk as Armand de Valerie
*May Beatty as Miss Doyle Charles Lane as Mr. Scurry

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 