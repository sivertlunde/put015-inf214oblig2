Comanche Territory (1997 film)
{{Infobox film
| name           = Comanche Territory
| image          = 
| caption        = 
| director       = Gerardo Herrero
| producer       = Gerardo Herrero
| writer         = Arturo Pérez-Reverte
| starring       = Imanol Arias
| music          = 
| cinematography = Alfredo Mayo
| editing        = 
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

Comanche Territory ( ) is a 1997 Spanish drama film directed by Gerardo Herrero. It was entered into the 47th Berlin International Film Festival.   

==Cast==
* Imanol Arias as Mikel Uriarte
* Carmelo Gómez as José
* Cecilia Dopazo as Laura Riera
* Mirta Zecevic as Jadranka
* Bruno Todeschini as Olivier
* Gastón Pauls as Manuel
* Natasa Lusetic as Helga
* Ecija Ojdanic as Jasmina
* Javier Dotú as Andrés
* Iñaki Guevara as Carlos
* Ivan Brkic as Francotirador
* Vedrana Bozinovic as Mujer violada
* Anne Deluz as Cámara de Olivier

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 