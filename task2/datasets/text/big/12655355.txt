Lakshmi Putrudu
{{Infobox film
| name = Lakshmi Putrudu
| image = Lakshmi Putrudu.jpg
| image_size =
| border =
| alt =
| caption =
| director = Raj Kapoor Polisetty Rambabu
| writer =
| screenplay = Raj Kapoor Bharati
| based on =  
| narrator = Diya Brahmanandam Mumaith Khan
| music =   Suresh
| editing =
| studio =
| distributor =
| released =  
| runtime = 145 mins
| country = India
| language = Telugu
| budget =   3 Crore
}}
 2008 Telugu Telugu drama Tamil director Raj Kapoor and is produced by Polisetty Rambabu, known for Gopi – Goda Meedha Pilli of the RS Films banner. The films stars Uday Kiran, Diya (actress)|Diya, Brahmanandam, and Mumaith Khan.    

==Plot==
Prabhu (Uday Kiran) is a college student who does not allow anyone to do wrong. He quite often argues with others to the point where he is repeatedly arressted. When he was younger his foster father (Nizhalgal Ravi), a psychiatrist used to bail him out. In the meantime, Swetha (Diya (actress)|Diya), the sister of commissioner of police Ravichandra, has been ditching her college classes, so her father has a police escort take her to school. When she tries to escape from her escort, they pursue her. They cross paths with the troublesome Prabhu, who battles them. In the ensuing fight, Prabhu says I love you to Swetha and she is so impressed by his gallantry, she falls in love.

Prabhu then learns that his foster parents are taking him to meet his real father Lakshmi Narayana (Satyaraj) who had been receiving treatment in a mental hospital. He tries to speak with his father, but the man is unresponsive. On the advice of Swethas father, he takes his father to Kerala for treatment. When there, he meets a former bodyguard of Lakshmi Narayana who tells him that not only was his father the former Election Commissioner, but that his current mental breakdown state was caused by local politician Ramadasu (Vijayan). Coincidentally, Ramadasu has been brought to that same Kerala facility by his own son (Riyaz Khan). When Ramadasus son learns that his fathers former enemy Lakshmi is still alive, he attempts to kill him.

==Cast==
 
 
* Uday Kiran as Prabhu Diya as Swetha
* Brahmanandam
* Mumaith Khan
* Nizhalgal Ravi as Prabhus foster father
* Satyaraj as Prabhus father
* Vijayan as Ramadasu
* Riyaz Khan as Ramadasus son
 
* Raghubabu
* Rajan P Dev
* Shakeela
* Apoorva
* Gauthamraju
* Duvvasi Mohan
* Gundu Sudarshan
 

==Music== Tollywood debut, IT minister Tammareddy Bharadwaja. 

{{Track listing
| extra_column = Singer(s)
| lyrics_credits = no
| title1 = Em Chusi Nanne Nuvvu | extra1 = Jassie Gift, Sainora Philip | lyrics1 = | length1 = 04:40
| title2 = Zee Bhoom Baa | extra2 = Achyut, Naveen | lyrics2 = | length2 = 03:55
| title3 = Taamaranai Taamaranai | extra3 = Jyotsna Radhakrishnan|Jyotsna, Naresh Iyer | lyrics3 = | length3 = 04:58 Ranjit | lyrics4 = | length4 = 04:09
| title5 = Adigadigo Toofan | extra5 = Rahul Nambiar | lyrics5 = | length5 = 02:40
| title6 = Em Smile Raa | extra6 = Suchitra | lyrics6 = | length6 = 04:35
}}

==Production== Hyderabad  and Kerala,    with a then-predicted release date of September 2007.  However, by the end of September, the film was still in post-production.   

===Music release=== Ravi Narayana Information Technology, Tammareddy Bharadwaja Operation Duryodhana, same name.   Distribution rights to the audio CD were purchased by Srinivas, a State Trading corporation chairman and former legislator.   

==Reception== Imam was "disastrous", and the director failed in properly narrating the story.   

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 