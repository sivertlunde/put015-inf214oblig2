The Handsome, the Ugly, and the Stupid
{{Infobox film
| name           = The Handsome, the Ugly, and the Stupid (Il bello, il brutto, il cretino)
| image          =The Handsome, the Ugly, and the Stupid.jpg
| image_size     =
| caption        = 
| director       = Giovanni Grimaldi
| producer       = Gino Mordini
| writer         = Giovanni Grimaldi 
| narrator       =
| starring       = Franco Franchi & Ciccio Ingrassia and Mimmo Palmara
| music          = Lallo Gori
| cinematography = Aldo Giordani
| editor       = Renato Cinquini
| distributor    = 
| runtime      = 92 min
| country      = Italy
| language     = Italian
| released     = 1967
}}
 1967 Italy|Italian film directed by Giovanni Grimaldi. It is a spaghetti western parody of The Good, the Bad and the Ugly.      

==Plot summary==
In the Wild West the bandits Franco and Ciccio, being unlucky in robberies, have an ingenious way to collect money. Ciccio gets captured by the guards of the sheriffs, while Franco, enjoying the bounty of money, saves his friend shortly before he is hanged every time. But one day Franco is unable to save his friend from a death sentence, and he believe Ciccio lost forever. After consulate drunk in a saloon, and having also won a lot of money playing poker, Franco meets magically Ciccio. He is not dead, and he plans to take revenge on Franco, but when he discovers with his friend the existence of a great treasure, buried in the desert of the West, the two friends that take up the membership.

==Cast==
*Franco Franchi: Franco, the ugly
*Ciccio Ingrassia: Ciccio Ingrassy, the stupid
*Mimmo Palmara: the "Handsome"
*Birgit Petri: Fabienne
*Lothar Gunther: Cpt. Imbriatella 
*Enzo Andronico: Prisoner 
*Gino Buzzanca: Bookmaker
*Bruno Scipioni

== References ==
 

==External links==
*  

 
 
 
 
 
 
 
 
 


 
 