Dandelion Wine (film)
{{Infobox Film
| name           = Dandelion Wine
| image          = 
| image_size     = 
| caption        = 
| director       = 
| producer       = Igor Apasyan
| writer         = Igor Apasyan Ray Bradbury (books),  Aleksei Leontyev
| narrator       = 
| starring       = Innokenti Smoktunovsky, Liya Akhedzhakova, Vladimir Zeldin
| music          = Shandor Kallosh
| cinematography = Aleksandr Nosovsky
| editing        = 
| distributor    = Odissey, Ostankino, VGTRK
| released       = 1997
| runtime        = 208 min
| country        =   Russia    Ukraine Russian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 book of the same name by Ray Bradbury. 

Its the last film of Innokenti Smoktunovsky, released after his death.

== Cast ==
* Innokenti Smoktunovsky
* Andrei Novikov Sergei Kuznetsov
* Vsevolod Polishchuk
* Liya Akhedzhakova
* Yevgeni Gerchakov
* Lev Perfilov
* Lidiya Dranovskaya
* Vera Vasilyeva
* Vladimir Zeldin
* Sergei Suponev

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 


 
 