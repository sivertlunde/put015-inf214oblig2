Adhyapika
{{Infobox film
| name = Adhyaapika
| image =Adyapika 1968.jpg
| caption =
| director = P. Subramaniam
| producer = P. Subramaniam
| writer = Kanam EJ
| screenplay = Kanam EJ Madhu Thikkurissi Ambika
| music = V. Dakshinamoorthy
| cinematography = ENC Nair
| editing = N Gopalakrishnan
| studio = Neela
| distributor = Neela
| released =  
| country = India Malayalam
}}
 1968 Cinema Indian Malayalam Malayalam film, Ambika in lead roles. The film had musical score by V. Dakshinamoorthy.    

==Cast==
   Madhu 
*Thikkurissi Sukumaran Nair 
*Vaikkam Mani Ambika 
*Aranmula Ponnamma 
*Babysree
*Bahadoor 
*Ittan
*Joseph Chacko
*Kottarakkara Sreedharan Nair 
*Leela
*Master Venu Meena  Padmini 
*Piravam Mary
*Ramachandran
*Ramakrishna 
*S. P. Pillai 
*K. V. Shanthi 
*Sobha
*T. K. Balachandran 
*Vanakkutty
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Susheela || ONV Kurup || 
|- 
| 2 || Agnikireedamaninjavale || K. J. Yesudas || ONV Kurup || 
|- 
| 3 || Kanya Nandana || P Leela || ONV Kurup || 
|- 
| 4 || Maavupoothu || P Leela, Kalyani Menon, Renuka, Padma || ONV Kurup || 
|- 
| 5 || Manassinullile mayipeeli (Rajakumari) || P Leela || ONV Kurup || 
|- 
| 6 || Mannidam Pazhayoru || Kamukara || ONV Kurup || 
|- 
| 7 || Nirdaya Lokam || K. J. Yesudas || ONV Kurup || 
|- 
| 8 || Pallimanikale || P Leela, Chorus, Renuka || ONV Kurup || 
|- 
| 9 || Swapnasundari || K. J. Yesudas || ONV Kurup || 
|}

==References==
 

==External links==
*  

 
 
 


 