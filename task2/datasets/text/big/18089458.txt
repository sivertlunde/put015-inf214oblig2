The Knot
 
{{Infobox film
| name           = The Knot
| image          =
| caption        = Yin Li
| producer       =
| writer         = Liu Heng Zheng Kehui
| starring       = Chen Kun Vivian Hsu Li Bingbing
| music          = Zou Ye
| cinematography = Wang Xiaole
| editing        =
| distributor    =
| released       =  
| runtime        = 117 minutes
| language       = Mandarin
| budget         =
| film name = {{Film name| jianti         = 云水谣
| fanti          = 雲水謠
| pinyin         = Yún shǔi yáo}}
}} Chinese film directed by Yin Li. It was Chinas submission to the 80th Academy Awards for the Academy Award for Best Foreign Language Film, but was not nominated.        

It won the Best Film in the 2007 Golden Rooster Awards, and was named Outstanding Film by the 2008 Hundred Flowers Awards.

==Plot==
They fell in love; Chen Qiushui was 20. Wang Biyun was 18. When Qiushui fled Taiwan after the 228 Massacre, Biyun gave him a gold engagement ring and they promised to meet again. Qiushui served as an army doctor during the Korean War, where he met Wang Jindi, a nurse from Shanghai who fell in love with him instantly.  Years had gone by, Qiushui married Jindi and settled in Tibet. While in Taiwan, Biyun buried Qiushuis mother and continued to pray for his return.

Flashback to modern time, Biyun is living in New York. Her niece played by Isabella Leong, a writer, has travelled to Tibet to find out what happened to Qiushui. Through the pictures she sends back via internet, Biyun finally gets to see the familiar face once again.

==Cast==
*Chen Kun
*Kuei Ya-lei
*Isabella Leong
*Li Bingbing Steven Cheung
*Vivian Hsu Chin Han

==See also==
 
*Cinema of China
*List of submissions to the 80th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
* 
* 
*  at the Chinese Movie Database

 
 
 
 
 
 


 
 