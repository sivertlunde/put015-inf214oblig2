Timerider: The Adventure of Lyle Swann
{{Infobox film name           = Timerider: The Adventure of Lyle Swann image          = TimeRiderPoster.jpg caption        = Timerider: The Adventure of Lyle Swann theatrical poster director       = William Dear producer       = William Dear writer         = Michael Nesmith William Dear starring  Belinda Bauer Ed Lauter Tracey Walter L. Q. Jones Richard Masur Chris Mulkey cinematography = Larry Pizer editing        = R.J. Kizer music          = Michael Nesmith distributor    = Jensen Farley Pictures released       =   runtime        = 94 minutes country        = United States language       = English gross          = $6,000,000 
}}
Timerider: The Adventure of Lyle Swann, directed by William Dear, is a 1982 time travel-influenced action film starring Fred Ward as Lyle Swann, a cross country dirt bike racer. The movie was scored, produced and co-written (with director William Dear) by Michael Nesmith.  

==Plot==
Lyle Swann (Fred Ward) is a well-known dirt bike motorcycle racer who is in the desert competing in the Baja 1000, a multiclass vehicle cross-country race. Swann has a reputation for being a great rider but is plagued by technical problems from the high-tech gadgetry he incorporates into his C and J framed XT500 Yamaha. When Swann accidentally goes far off course, he stumbles across a time travel experiment that utilizes "maser velocity acceleration" to send objects (in this case, a simian subject by the name of Ester G) back in time.
 San Marcos, Belinda Bauer), who gives him a safe place to hide and severely wounds one of Reeses men Carl Dorsett (Tracey Walter). The village priest (Ed Lauter) compels them to withdraw, but Reese continues to plot the capture of Swanns dirtbike. In the village, Swann is seduced by Claire and sleeps with her, but she is later kidnapped by Reeses henchman Claude Dorsett(Richard Masur) as revenge for her shooting and wounding his brother Carl. They also manage to capture the dirtbike, leading to a series of hijinks, while Swann gets help from a posse of two U.S. Marshalls, Ben Potter and Daniels (L. Q. Jones, Chris Mulkey) who are trying to capture or kill the gang. Potter has a personal vengeance against Reese, for Reese killed Potters son two years previously. Swann manages to retrieve his dirtbike and rescue Claire. Potter is killed by Reese in an ambush, and his 2nd, Daniels, is mortally wounded and dies later.
 his own great-great-grandfather.

==Main cast==
*Fred Ward: Lyle Swann Belinda Bauer: Claire Cygne
*Peter Coyote: Porter Reese
*Richard Masur: Claude Dorsett
*Tracey Walter: Carl Dorsett
*Ed Lauter: Padre Quinn
*L. Q. Jones - Ben Potter (American marshal)
*Chris Mulkey - Daniels, (2nd American marshal)

==Production==
===Screenplay=== Rhesus monkey Time After Time; and was also the "time travel arrival day" in a later film, 1985s Back to the Future. 
 Somewhere in Time. These paradoxes were highlighted in the 2004 South Park episode "Goobacks," where various time-traveling techniques in movies are compared.

The films screenplay was written by Michael Nesmith, who was formerly a member of the band The Monkees. The movie is produced by Zoomo Production, which is a subsidiary of Michael Nesmiths Pacific Arts Corporation. The movie was also released by Pacific Arts Video, another entity of Nesmith, who appears briefly as one of the Baja 1000 officials in the beginning of the film.

==DVD release with alternate version==
Two brief scenes were cut from the above theatrical version for the 2001 Anchor Bay Entertainment DVD release:

*The scene where the time experiment scientists pinpoint Swanns exact day in the past as being November 5, 1877 is missing.
*The scene where Reese is struck by the rescue helicopters tail rotor, leaving only his sheared off, bloody feet and boots is missing. Instead, the original sound effects remain, and a shot of Reese cowering on the ground is inserted.

Shout! Factory announced they would be releasing the film on Blu-ray on March 19, 2013. 

==Critical reception==
Upon its theatrical release, Vincent Canby of The New York Times commented, "At the point at which I walked out, about 55 minutes into the story, there hadnt been a single characterization, situation, line of dialogue, camera angle or joke to indicate that anyone connected with Timerider had the remotest idea of what he was doing." 

Contemporary critics reviewed the film upon its 2001 DVD release:

Patrick Naugle of DVD Verdict said, "Timerider starts off a bit slow. It seems as if the first twenty minutes are taken up watching Swann ride his bike all over the dry desert plains. After that sluggish beginning, Timerider kicks into a funny and enjoyable fish-out-of-water story." 

J.C. Maçek III of WorldsGreatestCritic.com wrote, "Timerider: The Adventure of Lyle Swann was great fun to watch when I was ten years old or so... and, to be fair, its a good bit of fun to this day. However, as an adult the film all-too-often comes off as silly and indulgent." 

==Soundtrack== Pacific Arts).

===Track listing===

{{tracklist
| headline        = 
| all_writing     = 
| all_lyrics      = 
| all_music       = Michael Nesmith
| title1          = The Baja 1000 
| length1         = 3:13
| title2          = Lost In the Weeds   
| length2         = 1:43
| title3          = Somewhere Around 1875
| length3         = 1:06
| title4          = Scared to Death 
| length4         = 0:58
| title5          = Silks and Sixguns
| length5         = 0:57
| title6          = Dead Mans Duds
| length6         = 1:28
| title7          = Two Swanns at the Pond
| length7         = 2:20
| title8          = I Want That Machine
| length8         = 0:51
| title9          = Escape to San Marcos
| length9         = 2:24 
| title10         = Claires Cabin
| length10        = 2:01 
| title11         = No Jurisdiction 
| length11        = 0:54
| title12         = Murder at Swallows Camp
| length12        = 2:17
| title13         = Claires Rescue
| length13        = 1:54 
| title14         = Up the Hill to Nowhere 
| length14        = 3:19
| title15         = Out of Ammo 
| length15        = 3:08
| title16         = Reprise
| length16        = 3:31
| total_length    = 
}}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 