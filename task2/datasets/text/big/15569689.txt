With Stanley in Africa
 
{{Infobox film
| name           = With Stanley in Africa
| image	         = With Stanley in Africa FilmPoster.jpeg
| caption        = Film poster
| director       = William James Craft Edward A. Kull
| producer       =
| writer         = George H. Plympton
| starring       = George Walsh Louise Lorraine
| music          =
| cinematography =
| editing        = Universal Film Manufacturing Co.
| released       =  
| runtime        = 18 episodes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 adventure film serial directed by William James Craft and Edward A. Kull and released by Universal Film Mfg. Co. This serial is considered to be a lost film. {{Cite web
 |url=http://www.silentera.com/PSFL/data/W/WithStanleyInAfrica1922.html
 |title=With Stanley in Africa
 |accessdate=3 January 2009
 |work=The Progressive Silent Film List
 |publisher=Silent Era
 |date=
 }} 

==Cast==
* George Walsh - Henry Morton Stanley
* Louise Lorraine
* Charles Mason William Welsh
* Gordon Sackville
* Jack Mower
* Fred Kohler

==See also==
* List of film serials
* List of film serials by studio
* List of lost films

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 