Paris Follies of 1956
{{Infobox film
| name           = Paris Follies of 1956
| image          =Poster of Paris Follies of 1956.jpg
| image_size     =
| caption        =
| director       = Leslie Goodwins
| producer       = Buck Houghton (associate producer) Bernard Tabakin (producer)
| writer         = Milton Lazarus (writer)
| narrator       =
| starring       = See below
| music          =
| cinematography = Edwin B. DuPar
| editing        = Allied Artists
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}

Paris Follies of 1956 is a 1955 American film directed by Leslie Goodwins.

The film is also known as Fresh from Paris in the USA (TV title).

== Plot summary ==
 

== Cast ==
*Forrest Tucker as Dan Bradley
*Margaret Whiting as Margaret Walton Dick Wesson as Chuck Russell
*Martha Hyer as Ruth Harmon
*Barbara Whiting as Barbara Walton
*Lloyd Corrigan as Alfred Gaylord
*Wally Cassell as Harry
*Fluff Charlton as Taffy
*James Ferris as Jim William Henry as Wendell
*The Sportsmen Quartet as Themselves Frank Parker as Himself

== Soundtrack ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 

 