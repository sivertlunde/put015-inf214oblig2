Hate Story 2
 
 
{{Infobox film
| name = Hate Story 2
| image = 
 
| caption = Theatrical release poster
| director = Vishal Pandya
| producer = Bhushan Kumar  Vikram Bhatt
| screenplay = 	Madhuri Banerjee
| starring =   Rashid Khan
| cinematography = Raju Khan and Swarup  
| editing = Kuldip Mehan
| distributor = T-Series|T-Series Film
| released =  
| runtime = 139 minutes
| country = India Hindi
| budget = 118&nbsp;million
| admissions =
| gross = 2560&nbsp;million (7 days domestic collection )(US$4.2 millions)
}}
 thriller film Hate Story film series

==Plot==
The film begins with an aged man entering a graveyard to lay flowers on someones grave. He hears the noise of someone struggling in a coffin nearby. He digs into that place and opens the coffin. To his utmost surprise, he discovers a girl buried alive in the coffin. He takes the girl and admits her to the hospital. Inspector Anton Veldez (Siddharth Kher),the officer-in-charge of the case, tells the doctor to let him know as soon as she regains consciousness and asks one of his officers to stay alert in case the girl is attacked again. Interestingly, that very officer turns out to be a dishonest one and calls an anonymous person informing him of the girl being alive. He, then, secretly enters her cabin for a second attempt to kill her and this time too, by suffocation. He presses a pillow on her face and she manages to press the alarm button. This causes the nurse to rush to her cabin. The officer goes out of the cabin pretending to be unaware of the attack. Now, that the girl has become conscious, the doctor calls Inspector Anton. When Anton, along with the doctor, goes into her cabin, they find her missing. Anton, frustrated, sends his officers to look for her and himself goes to the ground floor to meet the dean. While he is waiting for the lift, he remembers the monitor in her cabin still working meaning that the girl was right there when they entered. He goes back to the cabin but the girl had already escaped. She crawls her way to the store room. The girl is revealed to be Sonika (Surveen Chawla) and is a mistress of a powerful and influential political leader Mandar Mhatre (Sushant Singh). Sonika is a photography student. Sonika is kept isolated and tortured by Mandar. She reluctantly finds comfort in her college friend Akshay Bedi (Jay Bhanushali) who secretly loves her. They both fall in love but Sonika is helpless since she cant tell Akshay about her being the mistress of Mandar. Akshay gets to know the truth and comforts her. They both move to a different home and decide to spend their lives together. They declare their love and have sex. Sonika is happy and finds some hope that she will be free from the torture that she had to tolerate. Mandar tries to find Sonika but she is no where to be found in her house. Later on he gets to know that she has eloped with her lover. Mandar and his men reach Akshays house and attack him. Sonika cries and pleades Mandar to leave Akshay but he refuses to. Mandars men kill Akshay by tying him to a rope and throwing him in a lake in a car. Mandar also buries Sonika alive in a coffin. After she is rescued she vows to extract vengeance upon Mandar for murdering Akshay and for herself.She at first kills the police officer who tried to kill her.Then she writes a diary describing about her torture.She dives inside the same lake and puts it in the car.She then kills one of the three men who were involved along with Mandar in killing Akshay.She then informs Anton about Akshays dead body.However eventhough Anton gets hold of Akshay and the diary Mandar makes it look that Sonika killed Akshay. Sonika then again manages to put everything in right order and kills the another man of Mandar involved with Mandar in killing Akshay. Then Sonika teams up with Mandars arch rival and get hold of the third man of Mandar involved in killing Akshay. However all of a sudden Mandar himself kills that person. It is revealed that the arch rival had becomes patner after Mandar offered to share his seats with him in election. Mandar makes it look that sonika did it and thus police officer catch her. However she manages to escape with Antons help. Then Sonika kills Mandar with Mandars wifes help (who came to know about Mandars wrongdoing accidentally).

==Cast==
* Sushant Singh as Mandar Mhatre 
* Surveen Chawla as Sonika Prasad
* Jay Bhanushali as Akshay Bedi
* Siddharth Kher as Inspector Anton Varghese
* Rajesh Khera as Atul Mhatre
* Neha Kaul as Mandars Wife

==Production==
Both lead actors learnt scuba diving for the film.  Jasmin Oza did choreography for Hate Story 2.  The trailer crossed 7 million views within 10 days of its release.      Actress Sunny Leone was approached to perform an item song Pink Lips, composed by Meet Bros Anjjan and sung by Khushbu Grewal. 

==Promotion==
The Indian censor boards objected to the sex scenes airing as part of the trailer and music video on TV channels, therefore the producers of the film have edited certain portions.    Kapil Sharma refused to promote the film on the grounds that he did not want to expose an erotic thriller to a family audience watching his TV show, Comedy Nights with Kapil. 

==Reception==

Meena Iyer of The Times of India gave the film 2.5 stars out of 5 and wrote, "The proceedings on screen lack tempo that is needed for an erotic thriller."  Sweta Kaushal of the Hindustan Times gave the film 1.0 starts out of 5 and wrote, "With a totally predictable story line and extremely illogical twists and turns, Hate Story 2 fails to entertain." 

==Sequel==
Vishal Pandya was named as the director of a sequel which was slated to begin shooting by November 2014 . 

==Soundtrack==
One song from Madhuri Dixit-Vinod Khanna starrer 1988 film Dayavan Aaj Tum Pe  was re-created in the film.  Sunny Leone will be performing a sensuous item song, Pink Lips, which is a promotional track for the film.   
{| class="wikitable"
|- style="background:#ff9; text-align:centr;"
! Track# !! Song !! Singer(s) !! Music Director !! Lyrics !! Length
|-
| 1 || "Aaj Phir Tumpe Pyaar Aaya Hai" || Arijit Singh, Samira Koppikar || Arko Pravo Mukherjee || Aziz Qaisi & Arko Pravo Mukherjee || 4:21
|-
| 2 || "Pink Lips" || Meet Bros Anjjan & Khushboo Grewal  || Meet Bros Anjjan || Kumaar || 3:29
|- KK || Rashid Khan || Tanveer Ghazi || 5.54
|-
| 4 || "Hai Dil Ye Mera"  || Arijit Singh || Mithoon || Mithoon || 4.58
|-
| 5 || "Aaj Phir Tumpe Pyaar Aaya Hai- Remix" || Arijit Singh, Samira Koppikar || Arko Pravo Mukherjee || Aziz Qaisi & Arko Pravo Mukherjee || 3:56
|-
| 6 || "Pink Lips- Remix" || Meet Bros Anjjan & Khushboo Grewal  || Meet Bros Anjjan, Sumit & Prince || Kumaar || 3:52
|- KK || Rashid Khan || Tanveer Ghazi || 4.45
|-
| 8 || "Hai Dil Ye Mera- Remix"  || Arijit Singh || Mithoon || Mithoon || 5.41
|}

==See also==
*List of Bollywood films of 2014

==References==
 

==External links==
*  

 
 
 
 
 
 