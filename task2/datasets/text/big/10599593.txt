Aan Milo Sajna
{{Infobox film
| name           = Aan Milo Sajna
| image          = Aanmilo.jpg 
| image_size     =
| caption        = 
| director       = Mukkul Dutt
| producer       = Jagdish Kumar
| writer         = Sachin Bhowmick Ehsan Rizvi
| narrator       = 
| starring       = Rajesh Khanna Asha Parekh Vinod Khanna Aruna Irani Nirupa Roy
| music          = Laxmikant-Pyarelal
| lyrics         = Anand Bakshi
| cinematography = V. Babasaheb
| editing        = Pratap Dave
| studio         = Famous Cine Studios Filmistan Studio Natraj Studios Rajkamal Studios
| distributor    = Baba Digital Media Shemaroo Video Pvt. Ltd.
| released       = 24 December 1970
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}

Aan Milo Sajna is a 1970 Hindi film written by Sachin Bhowmick, produced by Jagdish Kumar and directed by Mukul Dutt. The film stars Rajesh Khanna, Asha Parekh, Vinod Khanna, Rajendra Nath, Aruna Irani and Nirupa Roy. The music is by Laxmikant Pyarelal with lyrics by Anand Bakshi.  The film became a super hit at the box office and the song "Acha to Hum Chalte Hain" was a phenomenon.  Aan Milo Sajna was released when Rajesh Khanna was enjoying his superstardom and this film was one of the 15 consecutive hits he featured in his superstardom era.

==Plot Summary==
Widowed and ailing Savitri Choudhury lives a wealthy lifestyle along with her son, Anil, in a palatial mansion in India. She knows that Anil is only waiting for her to die so that he can inherit the wealth and refuses to give him any money. The Diwan convinces Anil to mend his ways, get married, and patch-up with his mom. Shortly thereafter Anil does appear to have mended his ways, gets involved in charity, and even introduces a young woman named Deepali to his mom. Deepali moves in the mansion, looks after Savitri so much so that Savitri decides to make her the sole beneficiary of her estate. What Savitri does not know is that Anil has hired Deepali to act as his fiancée, and that Deepali herself is not who she claims to be, and is actually in love with a local horse-riding peasant, Ajit, whose father was convicted of killing Savitris husband, and soon Ajit himself will be arrested by the Police for having an affair and then killing a woman named Sita.

==Cast==
* Ajit: Rajesh Khanna
* Varsha/Deepali: Asha Parekh
* Anil Choudhury: Vinod Khanna
* Savitri: Nirupa Roy
* Muft Ram Mauji : Rajendra Nath
* Chicoo: Jr. Mehmood Shubha
* Varshas father : Tarun Bose
* Mohan: Sujit Kumar
* Ramesh (Ajits father): Abhi Bhattacharya
* Aruna Irani

==Music==
The musical score for the film was composed by Laxmikant-Pyarelal. The lyrics were written by Anand Bakshi. 
{| class="wikitable"
|-
!Song
!Singer
!Length
|- Achcha To Hum Chalte Hain Kishore Kumar and Lata Mangeshkar 
|- Jawani Oh Diwani Kishore Kumar
|-  Palat Meri Jaan Asha Bhosle
|- Rang Rang Ke Phool Khile Lata Mangeshkar and Mohammed Rafi
|- Tere Karan Mere Sajan Lata Mangeshkar
|- Ab Aan Milo Sajna  Mohammed Rafi and Lata Mangeshkar
|-  Falak Se Tod Ke Dekho Sitare  Mohammed Rafi
|}

==References==
 

== External links ==
*  

 
 
 

 