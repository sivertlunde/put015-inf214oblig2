The Motel Life (film)
 
{{Infobox film
| name           = The Motel Life
| image          = The Motel Life film.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster Gabriel Polsky
| producer       = Alan Polsky Gabriel Polsky Ann Ruark
| screenplay     = Noah Harpster Micha Fitzerman-Blue
| based on       =  
| starring       = Emile Hirsch Stephen Dorff Dakota Fanning Kris Kristofferson David Holmes
| cinematography = Roman Vasyanov
| editing        = Hughes Winborne
| studio         = Polsky Films
| distributor    = Random Media
| released       =  
| runtime        = 90 minutes  
| country        = United States
| language       = English
}} thriller film novel of the same name. The film was shot in Reno and Virginia City and also features animated sequences drawn by Mike Smith.

==Plot==
In 1990, Frank and Jerry Lee Flannigan, brothers who drift aimlessly, attempt to escape their hopeless lives through their creativity and excessive drinking.  When Jerry Lee strikes and kills a child in a hit-and-run accident, the two immediately pack up their belongings and leave town.  However, Jerry Lee abandons his brother at a diner, destroys the car, and steals his sometime girlfriend Pollys pistol.  Jerry Lee loses his nerve before he can commit suicide and instead shoots himself in the leg, which was already amputated at knee.  Subsequent flashbacks reveal that that the boys mother died when they were young, and, with their father missing, the two set off on their own; Jerry Lees leg is injured when they attempt to stow away on a train.
 Tyson vs Douglas boxing match.  Flush with cash after Douglas upset victory, Frank donates some of his winnings to the dead boys family and purchases a car from childhood father figure Earl Hurley, who advises him not to think of himself as a loser.

Frank sneaks his brother out of the hospital just as the police arrive.  Jerry Lee is excited to find that Frank has rescued an abused dog, and the three of them head to a small town.  Although Frank professes there to be no reason to head there, he later reveals that his former girlfriend, Annie James, lives there.  She has sent him postcards asking for forgiveness for an unspecified action.  When Jerry Lee presses Frank to discuss his thoughts and feelings, Frank explains that he caught Annies forced prostitution by her abusive mother.  Encouraged by Jerry Lee, Annie and Frank slowly rekindle their relationship.  Meanwhile, Jerry Lee sinks further into depression, claiming that no woman will love a man with one leg, especially after he has killed a child.

Franks alcoholism and apparent ulcers begin to worry Jerry Lee.  At the same time, Jerry Lees infected leg begins to grow worse.  Unable to take care of himself, Jerry Lee is forced to request aid from Frank when he takes a shower and urinates.  The brothers bond further over their hardships, but Frank remains cautiously noncommittal about his relationship with Annie.  As Jerry Lee becomes more ill, he states that Franks stories often feature tragic endings, especially for the women.  When Jerry Lee is once again hospitalized, Frank recounts a new story with a happy ending, but before he can finish it, Jerry Lee dies from the infection.  In the films final scene, Frank meets Annie at her workplace, and he commits to her.

==Cast==
* Emile Hirsch as Frank Flannigan
** Andrew Lee as 14-year-old Frank
* Stephen Dorff as Jerry Lee Flannigan Garret Backstrom as 16-year-old Jerry Lee
* Dakota Fanning as Annie James
* Kris Kristofferson as Earl Hurley
* Joshua Leonard as Tommy
* Dayton Callie as Uncle Gary
* Noah Harpster as Al Casey
* Jenica Bergere as Polly Flynn

==Production==
The Polskys first cast Emile Hirsch but were unsure Stephen Dorff fit the role of Jerry Lee. Dorff convinced them when he agreed to test with Emile, who he had met years before at a party with the premonition: "I think were going to play brothers one day." 

Portland, Oregon-based artist Mike Smith performed the films animated sequences, which director Werner Herzog praised. 

==Release== select theaters, on iTunes, and Video on Demand on November 8, 2013.

==Reception==
  rated it 61/100 based on 19 reviews.   Peter Travers of Rolling Stone wrote, "Striking. Tinged with humor and heartbreak. Emile Hirsch and Stephen Dorff are outstanding, engaged and enthralling."  Sheila OMalley of Rogerebert.com wrote, "A beautifully warm film with a very kind heart. Every frame feels right, every choice feels thought-out, considered. All adds up to a heartbreaking whole. Stephen Dorffs performance is a damn near masterpiece of pathos."   Andy Webster of The New York Times wrote, "The story may be slight, but the performances and ambience resonate."  Jessica Kiang of Indiewire wrote, "But while it doesnt reinvent the wheel, or revolutionize the genre, it achieves its modest ambitions affectingly well, in no small part due to a clutch of cherishable performances, especially from leads Emile Hirsch and Stephen Dorff".   Roman Vasyanovs cinematography was praised by The New York Daily News  and The Wall Street Journal, who called him "a shooter to keep our eyes on". 

Drew Hunt of Slant Magazine wrote that "the film flatlines at a messy pace because of the frequent shifts in time and space".   Boyd van Hoeij of Variety (magazine)|Variety described it as "a film so full of explanatory flashbacks and animated sequences visualizing its characters invented yarns that their real dramas are almost obscured."   Peter Bradshaw of The Guardian wrote, "Alan and Gabe Polskys film about two hobo brothers on the run labours with heroes who are neither sympathetic or interesting".   Mick LaSalle of the San Francisco Chronicle wrote that of the protagonists that "youd have to be a very, very nice person to care about how it all works out for them." 

===Awards===
In 2012, The Motel Life received three awards out of four nominations at the Rome Film Festival. Hughes Winborne and Fabienne Rawley  won Associazione Italiana Montaggio Cinematografico e Televisivo (AMC) Award, Gabe and Alan Polsky won the Audience Award and Noah Harpster and Micah Fitzerman-Blue won Best Screenplay. Gabe and Allan Polsky were also nominated for the Golden MarcAurelio Award. 

Werner Herzog hosted a special screening of the film at The Academy of Motion Pictures Arts and Sciences and expressed his admiration for the Polskys directorial debut, stating, "Its really an accomplishment of two young filmmakers...You see a portion of America you have never seen in movies."     Kristofferson said that The Motel Life is the finest film he has ever been in. 

==Soundtrack==
# "Fit to Be Tied" by Jonathan Clay
# "Roll Em Dice" by Lee Silver
# "Oil Can" by Joe DAugustine
# "Drifting Apart" by Marty Stuart
# "They Killed John Henry" by Justin Townes Earle
# "250 Miles" by Parker Griggs (Radio Moscow)
# "Wait" by James Hince & Alison Mossha (The Kills)
# "In Cold Blood" by Jack Shaindlin
# "Mr. Mudd & Mr. Gold" by Townes Van Zandt
# "Give Em Hell" by Anthony Catalano, Celeste Spina (Little Hurricane)
# "Prairie Saga" by Raymond Beaver
# "Reverse Harmonics" by Joe DAugustine
# "Dark Horse" by Brandy St. John
# "Girl from the North Country" by Bob Dylan
# "The Boyfriends" by Willy Vlautin, Sean Oldham, Dave Harding (Richmond Fontaine)

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 