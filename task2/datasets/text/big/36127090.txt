Himmatwala (2013 film)
 
 
{{Infobox film
| name           = Himmatwala
| image          = Himmatwala(2013).jpeg
| image_size     =
| border         =
| alt            =
| caption        = Theatrical release poster Sajid Khan
| producer       = Vashu Bhagnani Ronnie Screwvala Sajid Khan Sajid Khan Sajid Khan
| based on       =  
| narrator       =
| starring       = Ajay Devgan Tamannaah Bhatia Paresh Rawal Mahesh Manjrekar Leena Jumani Adhyayan Suman Zarina Wahab
| music          = Sajid-Wajid Sachin-Jigar Background Score: Sandeep Shirodkar
| cinematography = Manoj Soni
| editing        = Nitin Rokade
| studio         = Pooja Entertainment
| distributor    = UTV Motion Pictures
| released       =  
| runtime        = 150 minutes   
| country        = India
| language       = Hindi
| budget         =     
| gross          =  . 
}} Sajid Khan 1983 film of the same name by K. Raghavendra Rao which was in turn a remake of 1981 Telugu cinema Ooriki Monagadu. The film was released on 29 March 2013 . http://movies.ndtv.com/movie-reviews/movie-review-himmatwala-798  

==Plot==
Set in 1983, the story begins with Ravi (Ajay Devgan) winning a fighting competition at a club. He is called as Himmatwala meaning courageous man. He then goes to Ramnagar village where he meets his mother, Savitri and his younger sister, Padma who are living a miserable life.

Ravis mother tells him that his father was an honest and respected man, and he was the priest of the temple. His father was framed by Sher Singh, the tyrant zamindar (landlord), for robbing the temple since the latter saw him commit a murder. Disgusted, Ravis father commits suicide. In revenge, young Ravi tries to kill Sher Singh but failed. When Ravis house is burnt, Savitri tells him to run away as Sher Singh will kill him.

With revenge in his mind, Ravi beats up Narayan Das, Singhs manager and brother-in-law, as well as threatening Sher Singh. The next day, he publicly humiliates Sher Singhs daughter, Rekha because she was beating up her innocent driver. In response, Rekha (Tamannaah Bhatia) unleashes a tiger on the village in front of Ravi. Her plan was unsuccessful, however, as it backfired. Rekha falls down from the terrace and is about to be attacked by the animal, when Ravi jumps and saves her life. Rekha falls in love with Ravi, and afterwards, saves Ravis life against his fathers plans.

On the other hand, Ravi comes to know that Padma is in love with Shakti, Narayan Dass son. He has his objections and so has Narayan. However, Sher Singh tells Narayan Das that marrying Shakti with Padma would give them an upper hand over Ravi as they can ill-treat Padma and keep Ravi in control. Meanwhile, Padma now knows that Ravi is not her real brother, and the real Ravi died in a road accident. Before dying, the real Ravi asked him to take care of his family. Padma is initially upset, but then reconciles after Ravi saves her life.

Shortly after Padma and Shakti get married, both the father and son start ill-treating Padma. In revenge, Ravi uses Rekha against her own father, just like what she advises him. After Rekha tells her father that she is pregnant with Ravi, Sher Singh begs Ravi to marry his daughter. Eventually, Ravi punished Narayan Das and Shakti by doing all the household chores. He also wins the Sarpanch election, and Sher Singh finally gives all the property documents back to the villagers, which Sher Singh had illegally taken from them before.

However, Shakti overhears Rekha and Ravis conversation about the fake pregnancy and that he is not the real Ravi. Angered, Sher Singh tries to kill him by bringing 20 fighters from the city, but failed. While they are beating Ravi, the tiger (which Ravi had fought with) comes and saves his life. Ravi then brutally beats Shakti, and is about to kill Sher Singh when his mother stops him.

In the end, Sher Singh, Narayan Das and Shakti ask for the forgiveness from Ravi, Savitri and the rest of the villagers.

==Cast==
 
* Ajay Devgan as Ravi
* Tamannaah Bhatia as Rekha
* Paresh Rawal as Narayan Das
* Mahesh Manjrekar as Sher Singh
* Adhyayan Suman as Shakti
* Zarina Wahab as Savitri
* Leena Jumani as Padma 
* Anil Dhawan as Ravis father
 
;Special appearances:
 
* Ritesh Deshmukh as Real Ravi
* Chunky Pandey as Michael Jaikishan
* Surveen Chawla in song Dhoka Dhoka
* Amruta Khanvilkar in song Dhoka Dhoka
* Sayantani Ghosh in song Dhoka Dhoka
* Rinku Ghosh in song Dhoka Dhoka
* Mona Thiba in song Dhoka Dhoka
* Sonakshi Sinha 
 

==Promotion==
The first look poster of Himmatwala film was released on 8 August 2012, whilst the trailer was released on 24 January 2013. 

==Soundtrack==
{{Infobox album
| Name = Himmatwala
| Type = Soundtrack
| Artist = Sajid-Wajid
| Cover = Himmatwala 2013.jpg
| Released =  
| Recorded = 2012 Feature film soundtrack
| Length = 19:10 UTV
| Producer = Sajid-Wajid
| Last album = Dabangg 2 (2012)
| This album = Himmatwala (2013) Chashme Baddoor (2013)
}}

The soundtrack includes remakes of the songs from the original version of the film, "Taki O Taki" and "Nainon Mein Sapna". The song promo of "Naino Mein Sapna" was released on 8 February 2013. The background score was composed by Sandeep Shirodkar, while the music has been composed by Sajid-Wajid and Sachin - Jigar. Lyricist Indeevar wrote for "Taki O Taki" and "Nainon Mein Sapna", alongside Sameer (lyricist)|Sameer, who wrote for all the songs except a disco-based song, for which lyricist Mayur Puri wrote.

===Track listing===
{{Track listing
| heading = Songs
| extra_column = Artist(s)
| total_length = 19:05
| lyrics_credits = yes
| music_credits = yes

| extra_column = Singer(s)
| title1 = Nainon Mein Sapna
| lyrics1 = Indeevar
| music1 = Sajid-Wajid
| extra1 = Amit Kumar, Shreya Goshal
| length1 = 4:20
| title2 = Taki O Taki
| lyrics2 = Indeevar
| music2 = Sajid-Wajid
| extra2 = Mika Singh, Shreya Goshal
| length2 = 4:00
| title3 = Dhoka Dhoka Sameer
| music3 = Sajid-Wajid
| extra3 = Bappi Lahiri, Sunidhi Chauhan, Mamta Sharma
| length3 = 4:53
| title4 = Bum Pe Laat
| lyrics4 = Sameer
| music4 = Sajid-Wajid
| extra4 = Shaan (singer)|Shaan, Soham Chakraborty & Shubh Mukherjee
| length4 = 2:47
| title5 = Thank God Its Friday
| lyrics5 = Mayur Puri
| music5 = Sachin - Jigar
| extra5 = Sunidhi Chauhan
| length5 = 3:12
}}

==Critical reception==
Madhureeta Mukherjee of The Times of India gave the film 2.5 stars stating: " his one might impress the wannabe Himmatwalas out there. But itll take more than himmat to go back to the gawdiest era of all. Rest, as they say, is history. Repeated!"  Rediff gave the film 2 stars out of five stating that: "Sajid Khan loves big scale but the production values of all his film and their aesthetics are consistently tacky. Himmatwala is no different.". 

Taran Adarash of Bollywood Hungama gave the film 1.5 out of 5 stars stating that: "A film that promises big entertainment, Himmatwala is hugely disappointing!".  Shubra Gupta of The Indian Express gave the film 1.5 stars and said "I expected Himmatwala to be predictable. I also expected it to be annoying, and it doesnt disappoint on both scores. But I didnt think it would be so dull. In the rest, it needs all your courage. Hai himmat?"  Saibal Chatterjee of NDTV Movies gave the film 1.5 stars and said "Himmatwala is a mindless potpourri that brings together the worst ingredients of 1980s Hindi cinema and parlays them into a messy mélange that quivers repeatedly under its own weight." 

Komal Nahta rated the film 1 star out of 5 saying that "Director Sajid Khan has tried to recreate the era of the eighties but has overlooked the fact that the audience has moved decades ahead."  Anupama Chopra of Hindustan Times rated the film 1 star commenting, "When Himmatwala ended, I felt like I had aged a few years. Honestly, you need real courage to brave this one."  Oneindia.com gave 1 star and wrote, "Honestly, I still cant believe that Im in my fine senses after watching the nonsensical Himmatwala." 

==Box office==
Himmatwala opened better at single screens as compared to multiplexes. Single screens on average opened at around 70% mark while multiplexes were much closer to the 60% mark. 

Himmatwala collected around   net on its first day.  It had a good jump on Sunday as it grossed around   net for a   net weekend,   as well as strong collections of around   net in Tuesday.  Himmatwala has had a good first week and collected   net, which is better than Singham but less than Golmaal 3.  However, the film received a 70% drop from its first day ( ) as multiplex businesses were hit by the new release Cocktail (2012 film)|Cocktail film. Nevertheless, single screen business remained good, and it reached   net in two weeks. 

Himmatwala grossed   net approx in week three, totaling to  .  In its end run in domestic market, Himmatwala had grossed  .    Its final distributor share was around  . 

==References==
 

==External links==
*  

 
 
 
 
 