When the Angels Sleep
{{Infobox film
| name =   When the Angels Sleep 
| image =
| image_size =
| caption =
| director = Ricardo Gascón 
| producer = 
| writer = Cecilio Benítez de Castro  (novel)   Ricardo Gascón 
| narrator =
| starring = Amedeo Nazzari   Clara Calamai   Maria Eugénia   Silvia Morgan
| music = Juan Durán Alemany 
| cinematography = Enzo Serafin  
| editing = Juan Palleja 
| studio = CISI   PECSA Films 
| distributor =  PESCA Films (Spain)
| released = 28 April 1947  
| runtime = 105 minutes
| country = Italy   Spain  Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
}}
When the Angels Sleep (Spanish:Cuando los ángeles duermen) is a 1947 Spanish-Italian drama film directed by Ricardo Gascón and starring Amedeo Nazzari, Clara Calamai and Maria Eugénia. In Italian the films title is  Quando gli angeli dormono. 

==Cast==
*  Amedeo Nazzari as Blin  
* Clara Calamai as Elena 
* Maria Eugénia as Bianca  
* Silvia Morgan as Susana  
* Ana Farra as Paulina  
* Mona Tarridas 
* Gina Montes as Bárbara  
* Camino Garrigó as Braulia  
* Pedro Mascaró as Oriol  
* Alfonso Estela as Lalio  
* Modesto Cid as Bonifacio  
* Carlos Agostí as Emilio 
* Arturo Cámara as Carmona  
* Rafael Luis Calvo as Ventura  
* Fernando Sancho as Peral  
* Jorge Morales as Víctor 
* César Pombo as Hurtado 
* Luis Villasiul as Cura Riévana  
* Enriqueta Villasiul as Dora 
* Félix Dafauce as Chas 
* Alberto Vialis

== References ==
 
 
==Bibliography==
* de España, Rafael. Directory of Spanish and Portuguese film-makers and films. Greenwood Press, 1994. 
* Lancia, Enrico. Amedeo Nazzari. Gremese Editore, 1983. 

== External links ==
* 

 
 
 
 
 
 
 
 
 

 
 