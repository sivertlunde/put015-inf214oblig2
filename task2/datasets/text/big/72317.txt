Creature from the Black Lagoon
 
 
{{Infobox film
| name           = Creature from the Black Lagoon
| image          = Creature from the Black Lagoon poster.jpg Theatrical release poster  by Reynold Brown Jack Arnold
| producer       = William Alland
| story          = Maurice Zimm
| screenplay     = Harry Essex Arthur A. Ross Richard Carlson Julia Adams Richard Denning Antonio Moreno
| music          = Henry Mancini Hans J. Salter Herman Stein
| cinematography = William E. Snyder
| editing        = Ted J. Kent
| studio         = Universal Studios
| distributor    = Universal Studios|Universal-International
| released       =  (premiere)   (et al., regional openings)
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = unknown
| gross          = $1,300,000
}}
 horror 3-D Jack Arnold Richard Carlson, Julia Adams, Ben Chapman on land and by Ricou Browning underwater. It premiered in Detroit on February 12 and was released on a regional basis, opening on various dates. Furmanek, Bob and Greg Kintz.   3dfilmarchive.com, 2012.  Retrieved: November 19, 2013. 
 projected by polarized light anaglyph 3-D Beta and VHS videocassettes. 

Creature from the Black Lagoon is considered a classic of the 1950s and generated two sequels: Revenge of the Creature (1955), which was also filmed and released in 3-D in hopes of reviving the format, and The Creature Walks Among Us (1956), filmed in 2-D. The creature, also known as the Gill-man, is usually counted among the classic Universal Monsters.

==Plot==
  Amazon uncovers fossilized evidence Richard Carlson), an ichthyologist who works at an aquarium in California and has been a guest at Maias marine biology institute in Brazil for over a month. Reed persuades his boss, the financially-minded Dr. Mark Williams (Richard Denning), to fund a return expedition to the Amazon to look for the remainder of the skeleton.
 piscine amphibious humanoid, a living member of the same species from which the fossil originated. The creature, curious upon seeing the expedition, investigates the camp site, but when its sudden appearance frightens the members, they attack it, and in response, the enraged creature kills them.

The excavation of the area where Carl found the hand turns up nothing. Mark is ready to give up the search, but David suggests that perhaps thousands of years ago the part of the embankment containing the rest of the skeleton fell into the water and was washed downriver, broken up by the current. Lucas says that the tributary empties into a lagoon. Lucas calls it the "Black Lagoon", a paradise from which no one has ever returned. The scientists decide to risk it, unaware that the amphibious "Gill-man" that killed Carls assistants earlier has been watching them. Taking notice of the beautiful Kay, it follows the Rita all the way downriver to the Black Lagoon. Once the expedition arrives, David and Mark go diving to collect fossils from the lagoon floor. After they return, Kay goes swimming and is stalked underwater by the creature, who then gets briefly caught in one of the ships draglines. Although it escapes, it leaves behind a claw in the net, revealing its existence to the scientists.

Subsequent encounters with the Gill-man claim the lives of Lucass crew members, before the Gill-man is captured and locked in a cage on board the Rita. It escapes during the night and attacks Edwin, who was guarding it. Kay hits the beast with a lantern; driving it off before it can kill Edwin. Following this incident, David decides they should return to civilization, but as the Rita tries to leave, they find the entrance blocked by fallen logs, courtesy of the escaped Gill-man. While the others attempt to remove the logs, Mark is mauled to death trying to capture the creature single-handedly underwater. The creature then abducts Kay and takes her to his cavern lair. David, Lucas, and Carl give chase to save her. Kay is rescued and the creature is riddled with bullets before he retreats to the lagoon where his body sinks in the watery depths.

==Cast==
 
  Richard Carlson as Dr. David Reed
* Julie Adams as Kay Lawrence
* Richard Denning as Mark Williams
* Antonio Moreno as Dr. Carl Maia
* Nestor Paiva as Lucas
* Whit Bissell as Dr. Edwin Thompson
* Bernie Gozier as Zee
* Henry A. Escalante as Chico
* Perry Lopez as Tomas
* Rodd Redwing as Luis Ben Chapman as Gill-man (land)
* Ricou Browning as Gill-man (underwater)
 

==Production== Mexican cinematographer House of Jack Arnold was hired to direct the film in the same format. Vieira 2003, pp. 141–143. 
 Disney animator The Wizard prosthetics for amputees during World War II, created the bodysuit, while Chris Mueller, Jr. sculpted the head.
 Ben Chapman portrayed the Gill-man for the majority of the film shot at Universal City, California. Many of the on-top of the water scenes were filmed at Rice Creek near Palatka, Florida. The costume made it impossible for Chapman to sit for the 14 hours of each day that he wore it, and it overheated easily, so he stayed in the backlots lake, often requesting to be hosed down. He also could not see very well while wearing the headpiece, which caused him to scrape Julie Adams head against the wall when carrying her in the grotto scenes. Ricou Browning played the Gill-Man in the underwater shots, which were filmed by the second unit in Wakulla Springs, Florida. 

==Novelization== novelized in Universal horror Walter Harris. Sikorsky helicopter, and kidnapping Kay more than once, the creature is killed by the crew of a US Navy torpedo boat.

The 1977 novel also differs greatly with respect to the human characters. Only David Reed and Kay Lawrence remain the same. Mark Williams is a German named "Bruno Gebhardt" and dies not as a result from drowning, but by the monster falling on him. Lucas is named "Jose Goncalves Fonseca de Souza" and is a mostly sympathetic character, until his suggestion of throwing the wounded and unconscious Reed to the monster makes an enraged Gebhardt/Williams throw "him" to the beast instead. Dr. Thompson and Dr. Maia both die grisly deaths, whereas in the movie they survive; Maia is eaten by the monster, and Thompson is impaled on a long tree branch flung at him by the creature like a spear (in an apparent nod to a deleted scene from Revenge of the Creature wherein the Gill-man killed a guard in this fashion).

==Remake==
In 1982,   remake in May 1999, development of the Creature from the Black Lagoon remake was revived. 

In December 2001,  ) to write a script in March 2003. 
 The Thing polluting the Amazon. "Its about the rainforest being exploited for profit", he said. 
 The Crazies Jurassic Park, Davy Jones designer). The director said the design was "very faithful to the original, but updated", and that the Gill-man would still be sympathetic. 

In 2009, it was reported that Carl Erik Rinsch might direct a 2010 remake that would be produced by Marc Abraham, Eric Newman and Gary Ross;   however, a project featuring this ensemble had been abandoned by 2011.

In March of 2012, Universal announced that a reboot was in production, and would simply be titled The Black Lagoon rather than Creature from the Black Lagoon, in order to distinguish the two versions. In October 2012, the studio hired Dave Kajganich to write the film.  The film was expected to hit theaters by May of 2014, but was ultimately cancelled.

  On April 9, 2015, Tracking Board reports that the studio are offering actress Scarlett Johansson for the lead role. 

==Legacy==
 
The extensive and persistent impact on media and popular culture of Creature from the Black Lagoon began even before it was seen in theaters. 

==See also==
 

==References==
Notes
 

Citations
 

Bibliography
 
* Ferrari, Andrea. Il Cinema Dei Mostri. Milan, Italy: Mondadori, 2003. ISBN 88-435-9915-1.
* Murray, Andy. Into the Unknown: The Fantastic Life of Nigel Kneale. Stockport, Cheshire, UK: Critical Vision, 2005. ISBN 1-900486-50-4.
* Vieira, Mark A. Hollywood Horror: From Gothic to Cosmic. New York: Harry N. Abrams, 2003. ISBN 0-8109-4535-5.
 

==External links==
 
*  
*  
*  
*   at Rotten Tomatoes
*   – Official site of Ben Chapman, who played the Gill-man
*  

 
 
 
 .
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 