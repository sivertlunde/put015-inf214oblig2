Balunga Toka
{{Infobox film
| name           = Balunga Toka
| image          = Balunga toka.jpg
| caption        = Theatrical poster
| director       = Sudhakar Basanta
| producer       = Prabhas Chandra Rout
| writer         = Muni Patra
| screenplay     = Sudhakara Basanta
| dialogue       = Deepak
| starring       = Anubhav Mohanty Barsha Priyadarshini  Papu Pom Pom Minaketan Das
| music          = Abhijit Mazumdar
| cinematography = R. Bhagat Singh
| editing        = Chandra Sekhar Mishra
| released       = 2 October 2011
| runtime        = 157 min
| country        = India Oriya
| budget         = 80 lakhs
| gross          = 2.5 crore
}}
Balunga Toka is a 2011 Oriya romantic directed by Sudhakara Basanta. The movie starring with Anubhav Mohanty and Barsha Priyadarshini as a lead role.The film released in 2 October 2011.  
    The film was a remake of Tamil film 7G Rainbow Colony.

==Synopsis==
Chinku is living with his parents and younger sister . He always involves in fights in street, not interested in studies .  due to his rough behavior, his father always scolds him. Chinkus life changes when he meet Preeti and falls love with her. Due to Preetis efforts and Chinkus skill in motorcycle assembling, he gets a job. When Priti and Chinkus intimacy devlops, Pritis mother refuses to get marry her daughter with Chinku. Priti escapes from home to unite with Chinku. But due to some misunderstanding with Chinku, Preeti leaves Chinku and dies in accident. But in Chinkus mind Preeti is still alive.

==Cast==
* Anubhav Mohanty	... 	Chinku
* Barsa Priyadarshini	... 	Preeti
* Minaketan Das	... 	Chinkus father 
* Papu Pam Pam	... 	Babli 
* Priyanka Mahapatra	... 	Chinkus mother
* Prativa Panda	... 	Rocky
* Salil Mitra		
* Arabinda Sadhangi	... 	Das Babu 
* Jiban Panda		
* Manoj Panda
 

==Boxoffice==
The film proved to be a big hit and  have crossed 100 days in several theaters in Odisha. 
  It fatched  INR 2.5 Crore in box office. 

==Awards==
* 3rd Tarang Cine Awards2012 
** Best Film - Prabhas Raut
** Best Actor -Anubhav Mohanty
** Best supporting Actor -Minaketan Das
** Best comedian - Papu Pam Pam  
* 3rd Etv Oriya Film Awards2012  
** Best film  - Prabhas Raut
** Best Director - Sudhakara Basanta 
** Best Actor - Anubhav Mohanty
** Best comedian  - Papu Pam Pam
** Best music director - Abhijit Majumdar
**Best lyrics writer - Nirmal Nayak  
* Lalchand Entertainment Awards 2012
** Best Actor - Anubhav Mohanty 

== References ==
 

==External links==
*  

 
 
 
 
 