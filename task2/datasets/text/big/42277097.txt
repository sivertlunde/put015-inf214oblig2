Zombie Undead
{{Infobox film
| image          =
| alt            =
| caption        = Rhys Davies
| producer       = Rhys Davies
| writer         = Kris Tearse
| starring       = Ruth King Kris Tearse Rod Duncan Barry Thomas Sandra Wildbore Christopher J. Herbert
| music          = David Fellows Kris Tearse
| cinematography = Neill Phillips
| editing        =
| studio         = Hive Films
| distributor    =
| released       =   |df=y}}
| runtime        = 79 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}} Rhys Davies, written by Kris Tearse, and starring Ruth King, Kris Tearse, Rod Duncan, Barry Thomas, Sandra Wildbore, and Christopher J. Herbert. After a terrorist attack in Leicester, survivors take cover from zombies.

== Plot ==
After a terrorist attack, Sarah takes her injured father to a busy and overworked hospital, only to lose consciousness during the stressful attempt to save his life. When she comes to, the hospital seems deserted. Sarah quickly discovers that zombies have taken over the building, and, along with other survivors, flees the hospital and attempts to survive against the undead hordes.

== Cast ==
* Ruth King as Sarah
* Kris Tearse as Jay
* Barry Thomas as Steve
* Christopher J. Herbert as Phil
* Steven Dolton as Farmer
* Sandra Wildbore as Mary

== Production ==
Zombie Undead was filmed in Leicester. Filming lasted for 18 months.   The filmmakers tried to avoid exposition and focus on the characters. 

== Release ==
Zombie Undead premiered in Leicester on 15 January 2010  and opened in British theaters in April 2011.   Metrodome Distribution released it on DVD on 30 May 2011.   MVD Entertainment Group released it on DVD in the US on 24 July 2012.   In its opening weekend, it took in a total of £10. 

== Reception == Time Out London rated it 1/5 stars and called it a cliched and "laughably inept DIY horror movie".   Jamie Russell of Total Film rated it 2/5 stars and wrote, "Zombie Undead is another no-budget, no-brains outing to treat the walking dead as an excuse for a lack of writing ability."   Mark L. Miller of Aint It Cool News called it "a breath of fresh air" but sloppy and poorly acted.   Jeremy Blitz of DVD Talk rated it 2/5 stars and wrote, "Unfortunately, the producers of British indie Zombie Undead present little new material, though they do show a few flashes of near brilliance."   Peter Dendle called it a boring zombie film that was made for marketing purposes. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 


 