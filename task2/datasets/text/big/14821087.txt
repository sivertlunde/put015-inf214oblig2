The Prince of Avenue A
 
{{Infobox film
| name           = The Prince of Avenue A
| image          = The Prince of Avenue A - 1920 - lobbycard.jpg
| caption        = Contemporary lobby card
| director       = John Ford
| producer       = Carl Laemmle
| writer         = Charles Dazey Frank Mitchell Dazey Charles J. Wilson
| starring       = James J. Corbett
| cinematography = John W. Brown
| editing        =
| distributor    = Universal Film Manufacturing Company
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         =
}}

The Prince of Avenue A is a 1920 American drama film directed by John Ford. The film is considered to be lost film|lost.      

==Cast==
* James J. Corbett as Barry OConnor Richard Cummings as Patrick OConnor
* Cora Drew as Mary OConnor
* Frederick Vroom as William Tompkins
* Mary Warren as Mary Tompkins George Fisher as Regie Vanderlip
* Harry Northrup as Edgar Jones
* Mark Fenton as Father OToole
* John Cook as Butler (as Johnnie Cooke)
* Lydia Yeamans Titus as Housekeeper

==See also==
*List of lost films

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 