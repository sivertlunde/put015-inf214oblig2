Janani Janmabhoomi
{{Infobox film
| name           = Janani Janmabhoomi
| image          =
| caption        =
| writer         = D. V. Narasaraju  
| story          = K. Viswanath
| screenplay     = K. Viswanath
| producer       = K. Kesava Rao
| director       = Kasinathuni Viswanath|K. Viswanath Satyanarayana Sharada Sharada
| music          = K. V. Mahadevan
| cinematography = Kadthuri
| editing        = G. G. Krishna Rao
| studio         = Sri Bramharambika Films   
| released       =  
| runtime        = 148 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Sharada in the lead roles and music composed by K. V. Mahadevan.      

==Cast==
 
*Nandamuri Balakrishna as Ramesh
*Sumalatha Ralyalakshmi
*Kaikala Satyanarayana
*Sharada Sharada
*Subhalekha Sudhakar as Suresh 
*P. J. Sarma 
*Sakshi Ranga Rao 
*Gokkina Rama Rao 
*Bhimiswara Rao
*Potti Prasad 
*Malladi 
*Jeet Mohan Mitra
*Dham 
*Rama Prabha 
*Dubbing Janaki 
 

==Soundtrack==
{{Infobox album
| Name        = Janani Janmabhoomi
| Tagline     = 
| Type        = film
| Artist      = K. V. Mahadevan
| Cover       = 
| Released    = 1984
| Recorded    = 
| Genre       = Soundtrack
| Length      = 19:12
| Label       = AVM Audio
| Producer    = K. V. Mahadevan
| Reviews     =
| Last album  = Abhimanyudu   (1984) 
| This album  = Janani Janmabhoomi   (1984)
| Next album  = Mangammagari Manavadu   (1984)
}}
Music composed by K. V. Mahadevan. Music released on AVM Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!Lyrics!!Singers !!length
|- 1
|Tadisina Andalalo Sirivennela Sitaramasastri|Sirivennela Sitarama Sastry SP Balu, P. Susheela
|4:12
|- 2
|Thule Thule Tuhellenamma Veturi Sundararama Murthy SP Balu,P. Susheela
|4:17
|- 3
|Ghallu Ghalluna Kaalla Veturi Sundararama Murthy SP Balu, S. Janaki
|4:23
|- 4
|Paluku Tenela Thali Pavalincha Veturi Sundararama Murthy SP Balu,P. Susheela
|2:25
|- 5
|Sari Ganga Taanalu Veturi Sundararama Murthy SP Balu, S. P. Sailaja
|3:55
|}

==References==
 

 
 
 
 
 


 