Divot Diggers
{{Infobox film
| name           = Divot Diggers
| image          = Divot diggers.JPEG
| image_size     =
| caption        =
| director       = Robert F. McGowan
| producer       = Hal Roach
| writer         =
| narrator       =
| starring       =
| music          = Marvin Hatley Leroy Shield
| cinematography = Francis Corby
| editing        = Louis McManus MGM
| released       =  
| runtime        = 14 22"
| country        = United States
| language       = English
| budget         =
}} short comedy film directed by Robert F. McGowan; It was the 142nd Our Gang short to be released.

==Plot==
The action takes place at an expansive California golf course, where the gang merrily play their own ragtag version of golf with makeshift clubs. When the courses regular caddies quit en masse, the desperate caddy master hires the gang members as replacements. The kids—and their gibberish-spouting pet chimpanzee—proceed to drive an adult foursome crazy, then put the finishing touch on an imperfect day by accidentally commandeering a lawn-mowing tractor.

==Cast==
===The Gang===
* Darla Hood as Darla Eugene Lee as Porky
* George McFarland as Spanky
* Carl Switzer as Alfalfa
* Billie Thomas as Buckwheat
* Patsy May as Baby Patsy
* Harold Switzer as Harold
* Pete The Pup as Himself Jiggs The Chimpanzee as Chimpanzee

===Additional cast===
* Leonard Kibrick as Caddy
* Billy Bletcher as Bill, golfer
* Tom Dugan - Aggravated golfer
* Jack Hatfield as Mr. Hatfield, the caddy master
* Thomas Pogue as Mr. Jackson, golfer
* David Thursby as John, golfer
* Russ Powell as Chimpanzee (voice)
* Hubert Diltz - Tractor driver
* Jack Hill as Golfer
* Matty Roubert as Caddy

==Notes==
*After several years away from the Our Gang series, longtime mentor Robert F. McGowan briefly resumed his directorial activities.
*Divot Diggers featured several new Leroy Shield musical compositions, including "Hot and Dry", "Standing on the Corner", "Beyond the Rainbow" and "Up in Room 4".   

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 