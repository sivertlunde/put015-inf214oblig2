Resident Evil: Degeneration
{{Infobox film
| name               = Resident Evil: Degeneration
| image              = BioDPoster.jpg
| caption            = Japanese release poster
| alt                = 
| director           = Makoto Kamiya Hiroyuki Kobayashi
| based on           =  
| written by         = Shotaro Suga
| story              = Hiroyuki Kobayashi Yoshiaki Hirabayashi Paul Mercier Laura Bailey Roger Craig Smith
| music              = Tetsuya Takahashi Capcom Studios Sony Pictures Animation Digital Frontier
| cinematography     = Atsushi Doi Yusaku Toyoshima
| distributor        = Sony Pictures Entertainment
| released           =  
| runtime            = 96 minutes
| gross              = Over ¥40 million 
| country            = Japan  United States English
}}
 CG animation feature in Capcoms Resident Evil franchise. The film was made by Capcom Studios in cooperation with Sony Pictures Animation and Sony Pictures Entertainment. Degeneration made its premiere in Japan on October 11, 2008 at the Tokyo Game Show,  and was released nationwide one week later on October 18.
 Resident Evil live-action film series, Degeneration is set within the same universe as the original video game series.  The main characters are Leon S. Kennedy and Claire Redfield, who appear together for the first time since the 1998 game Resident Evil 2.

== Plot ==
 
Following the outbreak of the T-virus in Raccoon City and its subsequent destruction by nuclear bomb, the Umbrella Corporation goes under and is bought by WilPharma, a pharmaceutical company. WilPharma soon comes under fire after another T-virus outbreak occurs in India.
 Special Response Team and the United States Marine Corps, aiding evacuated survivors. Officers Angela Miller and Greg Glenn are joined by Leon S. Kennedy. Leon and Miller manage to save Claires group; however, they are forced to leave Greg, who was infected during the escape. Trucks from WilPharma arrive to administer the T-vaccine; however, they suddenly explode.

Claire accompanies WilPharma chief researcher Frederic Downing to the research facility for more vaccines. Downing reveals they plan to make a vaccine to the G-virus next, angering Claire; she knows first-hand its extremely dangerous. Excusing himself, Downing leaves Claire in his office. Claire informs Leon about WilPharmas possession of the G-virus, and learns that he and Angela have found the house of her brother, Curtis, burned down. Downing phones Claire, warning that he tailed a suspicious man, who has activated a time bomb. Claire catches a brief glimpse of Curtis through a window, only for the bomb to detonate.
 biohazard alarm and open the building. However, the detection of Curtis in the underground center causes another alarm, in which sections of the building are ejected to fall deep underground.

Curtis attacks them, seeing Leon as a threat and Angela as a mate. Curtis manages to briefly regain control, telling Angela to run before losing himself again. As the sections are ejected, Leon and Angela climb up wreckage, only to hang from a broken catwalk. About to fall, Curtis grabs hold of Angelas leg but is shot in the head by Leon, and falls to his death. The impact of Curtis with the bottom of the pit causes an eruption of fire, from which Leon and Angela are saved when a bulkhead closes just below them. In the aftermath, Claire accuses Senator Davis of being part of the cover up and the Harvardville Airport incident. Leon reveals Davis didnt know anything, and Claire realizes that Downing engineered the outbreaks, as well as the destruction of the vaccine, the bombing of the research building and the bio-terrorism incidents by selling T-virus samples. Meanwhile, Downing talks to General Grandé, a client eager to buy the T-virus, now that news reports have revealed its potential, though he warns against using the G-virus. Waiting for a contact to sell WilPharma information to, Downing mistakes a car containing Leon and Claire for his contact. He is arrested by Angela.
 the Raccoon City incident and created his current identity. Downing used his alias to sell the viruses to a list of potential customers while researching the vaccine. Angela realizes Downing manipulated Curtis, but Claire notes that even though this does not clear Curtiss name, he had the same motives to prevent another disaster like Raccoon City as she, Leon, and Angela do.

Meanwhile, news gets out that Davis has resigned from office over allegations of insider stock trading with WilPharma stocks. A newspaper draped over Davis face reads "Tricell Offers to Purchase WilPharma". Davis hand falls from the desk revealing him to be dead, and on his computer WilPharma files are being deleted, which when completed reveal a Tricell, Inc. insignia on his screen.

The film ends with Tricell employees in hazmat suits searching the underground ruins of the WilPharma research building, where they discover a fragment of Curtiss body infected with the G-virus, which they seal in a biohazard container.

== Cast == Paul Mercier as Leon S. Kennedy
* Alyson Court as Claire Redfield Laura Bailey as Angela Miller
* Roger Craig Smith as Curtis Miller
* Crispin Freeman as Frederic Downing
* Mary Elizabeth McGlynn as Ranis aunt
* Michelle Ruff as Rani Chawla
* Michael Sorich as Senator Ron Davis
* Steven Blum as Greg Glenn
* Salli Saffioti as Ingrid Hunnigan

The Japanese singer and lyricist Anna Tsuchiya sang the ending theme for the film, titled GUILTY. 

== Release ==
The film received a limited (2-week / 3-screen ) theatrical release in Japan on October 17, 2008.    It also had a limited theatrical release in the United States, opening on November 13 in  .

Resident Evil: Degeneration was released on Universal Media Disc|UMD, DVD, and Blu-ray formats December 24, 2008 (on December 26 in Japan and December 27 in North America).  It was later released in the European Union in January–February 2009.  More than 1.6 million home video copies were shipped as of September 2010. 

The special features include: the "Generation of Degeneration" featurette, character profiles, voice bloopers, a mock-up Leon interview, five trailers, two Resident Evil 5 trailers and previews. In the "Generation of Degeneration" special feature, the filmmakers explain that this movie is in effect "Resident Evil 4.5", i.e. showing what happens after Resident Evil 4.

== Mobile game ==
 .

== Reception ==
The film received mixed critical reception. The Wired News blog GameLife gave the film 3/10  and the website UGO.com gave the film an overall score of a B.  It also held the score of 57% from user ratings at Rotten Tomatoes (19,476 votes).  Over 1.6 million DVD and Blu-ray units were sold worldwide. 

== Sequel == 3D in Japan.   

== See also ==
 

== References ==
 

== External links ==
*  
*    
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 