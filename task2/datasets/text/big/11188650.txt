After This Our Exile
 
 
{{Infobox Film
| name = After This Our Exile Patrick Tam
| producer = Li-kuang Chiu Eric Tsang Yu Dong
| written = Patrick Tam Kai-Leong Tian (screenplay)
| starring = Aaron Kwok Charlie Yeung Gouw Ian Iskandar
| music = Robert Ellis-Geiger
| editing = Patrick Tam
| cinematography = Pin Bing Lee
| budget = HKD 20,000,000 (estimated)
}}
 Hong Kong Patrick Tam.

==Plot==
In hopeless pursuit of happiness, Shing (Aaron Kwok) is a man who desperately attempts to hold on to the dwindling threads of his family. Once a man who had a dream, Shing has become a deadbeat gambler whose marriage is failing with wife Lin (Charlie Yeung). Shings machoistic ego overrides any reasonable logic for change, which forces Lin to leave Shing repeatedly. After finally managing to escape, Shing is left with nothing but his son, Lok-Yun (Goum Ian Iskandar).

Hoping in vain to pay back loansharks, Shing turns to his loving son, Lok-Yun, who has somehow retained his filial loyalty. In his most desperate hour, Shing forces his struggle of survival onto his son, Lok-Yun, through thievery and tests the strength of loyalty and the boundaries of trust in their father-son relationship. With each passing day, the bond of love is threatened with Shings unrepentant ways.

==Cast==
* Aaron Kwok - Chow Cheung-Sheng
* Charlie Yeung - Lee Yuk-Lin
* Gouw Ian Iskandar - Chow Lok-Yun
* Kelly Lin - Fong
* Qin Hailu - Ha Je
* Valen Hsu - Jennifer
* Lester Chit-Man Chan - Strong Man
* Lan Hsin-mei
* Allen Lin - Sick boys father
* Qin Hao - School bus driver
* Tsui Ting Yau - Chow Lok-yun (young adult)
* Wang Yi-xuan - Sick boys mother
* Xu Liwen - Rich boys mother
* Faith Yang
* Mak Kwai-Yuen
* Mok Kam-Weng
* Daniel Yu

 
 

==Release==
The movie runs for 121 minutes, but a 159 minutes long directors cut has been released in Hong Kong. The directors cut was also shown at the Asia Society in New York City on Friday, 20 July 2007, as a part of the Asian American International Film Festival.

==Awards and nominations==
1st Rome Film Festival
* Competition Section

11th Busan International Film Festival
* Official Selection

Tokyo International Film Festival
* Best Artistic Contribution
* Best Asian Film

10th Toronto Reel Asian International Festival
* Opening Film

43rd Golden Horse Awards
* Won: Best Feature Film
* Won: Best Actor (Aaron Kwok)
* Won: Best Supporting Actor (Gouw Ian Iskandar)
* Nominated: Best New Performer
* Nominated: Best Original Screenplay
* Nominated: Best Cinematography
* Nominated: Best Makeup & Costume Design

26th Hong Kong Film Awards
* Won: Best Picture
* Won: Best Director (Patrick Tam)
* Won: Best Supporting Actor (Gouw Ian Iskandar)
* Won: Best Screenplay
* Won: Best New Performer (Gouw Ian Iskandar)
* Nominated: Best Actor (Aaron Kwok)
* Nominated: Best Supporting Actress (Kelly Lin)
* Nominated: Best Cinematography
* Nominated: Best Editing

==References==
 

==External links==
*  

   
{{succession box
| title = Golden Horse Awards for Best Film
| years = 2006
| before= Kung Fu Hustle
| after = Lust, Caution (film)|Lust, Caution
}}
{{succession box
| title = Hong Kong Film Awards for Best Film
| years = 2007
| before= Election (2005 film)|Election
| after = The Warlords
}}
 

 
 

 
 
 
 

 