The Watcher in the Woods
{{Infobox film
| name = The Watcher in the Woods
| image = The Watcher in the Woods, film poster.jpg
| caption = Theatrical release poster John Hough Ron Miller  
| writer = Brian Clemens Harry Spalding Rosemary Anne Sisson
| based on = A Watcher in the Woods by Florence Engel Randall
| starring = Bette Davis Carroll Baker David McCallum Lynn-Holly Johnson Kyle Richards Ian Bannen  Richard Pasco
| music = Stanley Myers
| cinematography = Alan Hume
| editing = Geoffrey Foot Walt Disney Productions Buena Vista Distribution
| released =  
| runtime = 84 minutes
| language = English
| country = United States
| gross = $5 million
}} 1976 novel of the same name by Florence Engel Randall, the film tells the story of a teenage girl and her little sister who become encompassed in a supernatural mystery regarding a missing girl in the woods surrounding their new home in the English countryside. 
 Walt Disney Productions in the 1980s, when the studio was targeting young adult audiences. The film suffered from various production problems and was pulled from theatres after its initial release in 1980. It was re-released in 1981 after being re-edited and a revised ending added.

==Plot==

Helen and Paul Curtis (Carroll Baker and David McCallum) and their daughters Jan (Lynn-Holly Johnson) and Ellie (Kyle Richards), move into a manor. Mrs. Aylwood (Bette Davis), the owner of the residence, notices that Jan bears a striking resemblance to her daughter, Karen, who disappeared inside a chapel near the village 30 years previously.

Jan begins to see strange blue lights in the woods, triangles and glowing objects. Eventually, Ellie goes to buy a puppy she names "Nerak" (an anagram for Karen). After seeing the reflection of the name "Nerak" (Karen spelled backwards), Jan finds out about the mystery of Mrs. Aylwoods missing daughter.

Several strange occurrences appear, beginning with Mrs. Aylwood saving Jan after she falls into a pond looking at a blue circle, and ending with Jan finding a man named John, who explains that Karen did disappear but has not died. He tells her that in a seance-like ceremony, Karen disappeared after lightning struck the tower and a bell fell on top of her. They find out that the disappearance of Karen is linked to a solar eclipse. Jan figures out she needs to repeat the sequence through the strange possession of Ellie.

In the chapel, something possesses Ellie and explains the accidental switch that took place 30 years ago. Ellie explains that Karen was taken to another dimension, while an alien-like being, the Watcher, came to earth. The Watcher then appears independently as a pillar of light, fueled by the "circle of friendship". It engulfs Jan and lifts her into the air, but Jans friend Mike Fleming (Benedict Taylor) intercedes and pulls her away before the Watcher disappears. At the same time, the eclipse ends and Karen, still the same age as when she disappeared, reappears &ndash; still blindfolded. She removes the blindfold as her mother enters the chapel.

==Cast==
*Bette Davis as Mrs. Aylwood
*Carroll Baker as Helen Curtis
*David McCallum as Paul Curtis
*Lynn-Holly Johnson as Jan Curtis
*Kyle Richards as Ellie Curtis
*Ian Bannen as John Keller
*Richard Pasco as Tom Colley
*Frances Cuka as Mary Fleming
*Benedict Taylor as Mike Fleming
*Eleanor Summerfield as Mrs. Thayer
*Georgina Hale as Young Mrs. Aylwood
*Katherine Levy as Karen Aylwood (Replaced by an uncredited actor in the 1981 version)

==Production==

===Development===
The Watcher in the Woods is based on Florence Engel Randalls 1976 novel A Watcher in the Woods. Producer Tom Leetch pitched the project to Disney executive Ron Miller, stating that "This could be our The Exorcist (film)|Exorcist."  Brian Clemens adapted the novel into a screenplay. However, Disney decided that Clemens version was too dark and had Rosemary Anne Sisson revise it. This script was later revised again by Gerry Day in July 1979.  During filming, Ron Miller would often intervene to tone down intense scenes, leading to tension between himself and Leetch. Miller recruited John Hough to direct the film after seeing his previous movie, The Legend of Hell House with Roddy McDowall.

When the film was pulled from theatres, several new endings were penned by various writers at Disney to substitute for the original. In addition to the work of studio writers, a number of science fiction writers, including Robert Silverberg, Joe Haldeman, and the Niven/Pournelle team, all working separately, were brought in and paid for alternate endings, but apparently none of those were used. Harrison Ellenshaw, the visual effects designer, later stated that there were "roughly 152" possible endings.  Ellenshaw wrote the version of the ending that eventually accompanied the re-release of the film.

===Casting=== Anchor Bay DVD release, casting the role of the young Mrs. Aylwood was complicated, since the character is featured in two separate time periods; Bette Davis, who was already cast as Mrs. Aylwood, was considered for playing both the young and old versions of the character. 

According to Hough, Davis "desperately" wanted to play both parts; so much so, that the production crew had make-up and hair specialists flown in from Los Angeles in order to work on Davis in preparation for screen tests; the goal was to reverse her age appearance by thirty years.  After the screen tests were completed and viewed by the crew, Hough was concerned about Davis playing the younger character, and felt that the make-up and hair work had  "maybe knocked about twenty years off of her age, but not forty";  Davis was 72 years old at the time. Upon viewing the tests, Hough cued for the crew to leave the screening room, and said, "Bette, I dont think youve made it". After taking one long drag from her cigarette, Davis replied: "Youre goddamn right". {{cite video
  | people = John Hough (director)|Hough, John 
  | title = The Watcher in the Woods
  | medium = DVD
  | publisher = Anchor Bay Entertainment, Walt Disney Pictures
  | location =  date =2002 }}  British actress Georgina Hale ended up taking the role of the younger Mrs. Aylwood; according to Hough, she took the part largely because of her admiration for Davis.

In casting the leading part of Jan, Diane Lane had been the initial choice;  however, due to complications, the part eventually went to Lynn-Holly Johnson, who had gained attention in the United States as a professional figure skater, as well as for her acting role as a blind ice skater in the 1978 film, Ice Castles, for which she received a Golden Globe nomination.
 Escape to Witch Mountain in an uncredited role as a younger version of her sister, Kim Richards.

===Shooting=== The Haunting, most notably the grand mansion in which John Kellers character lives; this was the same house used for filming The Haunting (Ettington Park, Warwickshire).

==Alternate endings==
The film had three different alternate endings, which are presented below. Each of these conclusions are featured on the films 2002 Anchor Bay DVD release; the "other world" ending and the 1980 ending are included as supplemental material, while the final 1981 ending is the official ending of the movie.

===Original ending concept===
An "other world" sequence was an integral part of the intended ending for the film; it was never completed.

The original ending featured an appearance by the growling Watcher, a skeletal,   and shot by a different director; this ending includes no physical manifestation of the watcher, besides an eerie beam of light.]] Jan reached out to Karen, and when the two embraced they were teleported back to the chapel. The girls then returned to the manor, where Mrs. Aylwood and her daughter were reunited. As they walked arm in arm, Jan explained everything to Ellie: the Watcher &ndash; who was switched with Karen by accident during the eclipse &ndash; needed Jan to free the girl.

The visual effects for the "other world" scenes were not finished in time for the release because the film was rushed out to coincide with Bette Daviss 50th anniversary as a film actor in 1980 (Davis was first hired by Universal Studios in 1930). Rather than finish the existing effects shots, Disney opted to rewrite and re-shoot the ending, toning down the references to the occult, deeming them too dark.

===Initial theatrical ending (1980)===
The first theatrical ending, which was shown with the films week-long screening in New York City, featured only part of the intended ending, leaving out all of the "other world" sequence and replacing it with Helens interrogation of Tom, Mary, and John at the chapel, after Jan disappears during their re-enactment of the séance. It did, however, include the appearance of the alien creature as it picks up Jan and disappears into thin air. While Helen is questioning everyone in the chapel, Jan re-appears, and emerges from a beam of light, hand-in-hand with Karen. The girls return to the house, where Mrs. Aylwood and Karen are re-united in the front yard, and Jan discusses the watcher with Ellie. This ending forced the film to rely on Jans brief, cryptic explanation to provide closure. This conclusion to The Watcher in the Woods was nearly unintelligible as a result, thus giving the film the reputation of not having an ending. It also omitted Mrs. Aylwoods condemnation of recreating the séance on the basis that it was witchcraft. After a week-long run of sneak previews in New York City was poorly received by critics (and audiences alike, who deemed the alien special effects as too unrealistic), the film was pulled from theatres; director John Hough described this cut of the film as being "laughed off the screen".  After its removal from the cinema, work on a new ending began, this time without Hough.

===Final theatrical ending (1981)=== ectoplasmic pillar of light) was less threatening and more supernatural. The nature of Karen and the Watchers switch was clearly explained by Ellie in the chapel (whilst possessed by the Watcher). The new footage (including the forest scenes that replaced the original opening credits) was directed by Vincent McEveety, although he was not credited due to union rules which forbade a screen credit unless the director worked on the film for a certain number of hours.

==Home media==
Though the film was released on VHS in the 1980s, it was not until 2002 that it was picked up for a DVD release by Anchor Bay Entertainment. There are a total of three separate DVD versions of the film released, each described below:

; Anchor Bay Entertainment release (US) 
Released by  , which would feature the censored opening credits (in which the Watcher scares a girl and incinerates her doll) and a finished version of the "other world" ending.

However, Anchor Bay encountered considerable resistance from Disney. In the end, they were forced to drop the original credits and release the "other world" footage as an abbreviated (14 minute long) and unfinished alternate ending. The other alternate ending (6 minutes long) is an approximation of the first theatrical ending. Both of these alternate endings were later included in Disneys own DVD release of the film in 2004. This version comes with a wealth of extras including an audio commentary by director John Hough, a detailed biography of Hough, two alternate endings, three theatrical trailers, and a TV commercial. The DVD also comes with a 20 page collectible booklet and card insert of the films original poster art (seen on the DVDs front cover). The Anchor Bay DVD is now permanently out of print.

; Buena Vista release (UK/Australia) 
Released by Buena Vista Home Entertainment on March 29, 2004 (primarily in the United Kingdom and Australia). There are no extra features on this release.

; Walt Disney release (US/Canada) 
Released by Walt Disney Video on August 3, 2004. This version has far less features than the Anchor Bay release, however, it is the only version currently available through retailers. This version only includes the two alternate endings and two theatrical trailers. It does not come with the audio commentary, biography, third trailer, TV commercial, booklet, or card insert.

==Accolades==
*The film was nominated for a Saturn Award (Best International Film 1982).
*Kyle Richards was nominated for a Saturn Award (Best Supporting Actress 1982) and Young Artist Award (Best Young Motion Picture Actress 1982) for her portrayal of Ellie Curtis.

==References==
 

==External links==
*   
* , detailed articles on the controversies surrounding The Watcher in the Woods by journalist Scott Michael Bosco.
* , article about the film, novel, and lost footage.
*  at Ultimate Disney.
*  at Ultimate Disney.
* .
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 