The Hourglass Sanatorium
{{Infobox film
| name = The Hourglass Sanatorium
| image = Sanatoriumpodklepsydra.jpg
| caption = Theatrical release poster
| director = Wojciech Jerzy Has
| producer =
| screenplay = Wojciech Jerzy Has
| based on =  
| starring =Jan Nowicki Tadeusz Kondrat Mieczysław Voit Halina Kowalska Gustaw Holoubek
| music = Jerzy Maksymiuk
| cinematography = Witold Sobocinski
| editing = Janina Niedźwiecka
| studio = Zespół Filmowy Silesia
| distributor = Film Polski
| released =  
| runtime = 119 minutes
| country = Poland
| language = Polish
| budget =
}} Jury Prize at the 1973 Cannes Film Festival.

==Plot==
Joseph (Jan Nowicki) travels through a dream-like world, taking a dilapidated train to visit his dying father, Jacob, in a sanatorium. When he arrives at the hospital, he finds the entire facility is going to ruin and no one seems to be in charge or even caring for the patients. Time appears to behave in unpredictable ways, reanimating the past in an elaborate artificial caprice.

Though Joseph is always shown as an adult, his behavior and the people around often depict him as a child. He befriends Rudolf, a young boy who owns a postage stamp album. The names of the stamps trigger a wealth of association and adventure in Joseph. Among the many occurrences in this visually potent phantasmagoria include Joseph re-entering childhood episodes with his wildly eccentric father (who lives with birds in an attic), being arrested by a mysterious unit of soldiers for having a dream that was severely criticized in high places, reflecting on a girl he fantasized about in his boyhood and commandeering a group of historic wax mannequins. Throughout his strange journey, an ominous blind train conductor reappears like a death figure.

Has also adds a series of reflections on the Holocaust that were not present in the original texts, reading Schulzs prose through the prism of the authors death during World War II and the demise of the world he described.

==Cast==
* Jan Nowicki as Józef
* Tadeusz Kondrat as Jakub, Józefs father
* Irena Orska as Józefs mother
* Halina Kowalska as Adela
* Gustaw Holoubek as Dr. Gotard
* Mieczysław Voit as train conductor
* Bożena Adamek as Bianka
* Ludwik Benoit as Szloma
* Henryk Boukołowski as firefighter
* Seweryn Dalecki as Teodor the clerk
* Jerzy Przybylski as Mr. de V.
* Julian Jabczyński as dignitary
* Wiktor Sadecki as dignitary
* Janina Sokołowska as nurse
* Wojciech Standełło as Jew
* Tadeusz Schmidt as officer
* Szymon Szurmiej as Jew reciting verses from Ecclesiastes false
* Paweł Unrug as ornithologist
* Filip Zylber as Rudolf
* Jerzy Trela as jester

==Production==
The Hourglass Sanatorium is not solely an adaptation of   where Schulz grew up, and Has own pre-World War II memories of the same region.  The film was produced by Zespół Filmowy Silesia. Principal photography took place at the Wytwórnia Filmów Fabularnych studios in Łódź.   

==Release== Jury Prize.   The Polish premiere took place on 11 December 1973.  The film is among 21 digitally restored classic Polish films chosen for Martin Scorsese Presents: Masterpieces of Polish Cinema.     The film currently remains without a region 1 home video release as of  December 2014. Low-res bootleg copies are not common but attainable. 
==See also==
* Cinema of Poland
* List of Polish language films

==References==
 Essay on the film:  

==External links==
*  
*   at  

 
 

 
 
 
 
 
 
 
 