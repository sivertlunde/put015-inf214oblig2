Lekin...
 
 
 
{{Infobox film
| name = Lekin
| image = Lekin.jpg
| image_size =
| caption = Lekin Gulzar
| producer = Lata Mangeshkar Hridayanath Mangeshkar
| writer = Gulzar (lyricist)|Gulzar(story) Kailash Advani, Arun Kaul (screenplay)|
| story =
| dialogue =
| starring = Vinod Khanna Dimple Kapadia Hema Malini Gulzar (lyrics) Gulzar (Lyrics)
| cinematography = Manmohan Singh
| editing = Subhash Sehgal
| distributor =
| art direction = Nitish Roy
| costume = Bhanu Athaiya
| released =  
| runtime = 171 min
| country = India
| language = Hindi
}}
 Hindi drama film made in 1991, loosely based on a short story Kshudhit Pashaan by Rabindranath Tagore, and directed by Hindi lyricist Gulzar (lyricist)|Gulzar. It stars Vinod Khanna, Dimple Kapadia, Amjad Khan, Alok Nath, and Beena Banerjee, and featured a special appearance by Hema Malini.

The film was produced by Indian singer Lata Mangeshkar, whose rendition of "Yara Seeli Seeli" won the 1991 National Film Award for Best Female Playback Singer. The song also won films lyricist, Gulzar (lyricist)|Gulzar, both the 1991 National Film Award for Best Lyrics and the 1992 Filmfare Best Lyricist Award.
The films music director, Latas brother Hridayanath Mangeshkar, won the 1991 National Film Award for Best Music Direction. Other awards included the National Film Award for Best Art Direction for Nitish Roy and National Film Award for Best Costume Design for Bhanu Athaiya.

==Synopsis==
The movie takes place in the Indian desert state of Rajasthan, which gives it something in common with Rudaali, another off-beat drama set in Rajasthan that was released around the same time and also featured Dimple Kapadia and Amjad Khan.

The plot centrs around a government employee, Sameer Niyogi (Vinod Khanna), who is sent to Rajasthan to take inventory of items in the abandoned haveli (mansion) of the long-deceased Maharaja Param Singh (Vijayendra Ghatge). When he arrives in Rajasthan, he meets his old friend Shafi (Amjad Khan) who is a tax collector in the area and lives with his wife Sharda (Beena Banerjee).

Sameer begins seeing what he thinks could very well be visions of another time and place, visions that he is shown by a beautiful woman he encounters by the name of Reva (Dimple Kapadia). But is Reva real or just a figment of Sameers imagination? The drama that unfolds catapults Sameer into a different time and place and shakes his own personal foundation of reality, even as Shafi and his wife fear that their old friend may be losing his mind.  In addition to the excellent performances and overall haunting quality of the cinematography, the plot twists and guest appearance of Hema Malini add to the films charms.

Also, as much as Sameer, Reva and Shafi, the desert of Rajasthan itself is a character in this film. Rolling sand dunes and long-deserted havelis create a sense of mystery and wonder, the stuff of dreams (or nightmares). Old commitments call across distances of not only places but also time and birth even, Lekin is such a saga.

==Plot==
Government officer Sameer takes charge of the old fort and palace of Jasor, last ruled by one King Param Singh. En-route his journey to Jasor, Reva meets Sameer in the train in a mysterious way. She keeps meeting and disappearing, during his stay in Jasor. The mysterious appearance and disappearance initially shakes Sameer but reassurance that spirits exist from an expert on the field gives him an unknown inner motivation to find out the truth behind Reva, and his own self as well as find out the reason he is connected with this story. As the story is revealed by Reva herself, she is a spirit stranded in a period of time, attempting to cross the desert to meet her long lost sister Tara. The older sister, Tara, comes to Maharaja Param Veers palace for a singing and dancing performance one night. The Maharaja eyes her malevolently and orders his men to not let her go out of the palace that night so he can rape her.

Ustad Miraj Ali, the musical maestro in the kings court, who also happens to be Tara and Rewas music teacher, learns of the kings plan. He warns the father of Tara and Rewa and advises them to run away from the town by crossing the desert. The King learns of this and imprisons Miraj Ali and Rewa while Taras camel runs ahead in the desert but is never heard of. The cruel Maharaja also orders Tara and Rewas father to be lashed till he bleeds to a near-death condition and then orders his men to put him on a camels back and send him into the desert.

The lecherous King then turns his attention to Rewa. He would wait until she would grow into a young woman. Rewa spends 8 years in captivity and one day the King wishes to sexually gratify himself with her. Ustad Miraj Ali along with the help of one of the kings servants hatch a plan to help Rewa run away from prison to save herself from the King. We learn that Rewa narrowly escaped the clutches of indulgent King Param Singh. Her mentor Ustad Miraj Ali gives an oath of the Holy Quran to one of his acquaintances, Mehru, who is supposed to help Reva cross the desert. But in the attempt to cross the desert, Mehru is caught by the Kings men and punished by lashes and dropped to his village in a near-dead condition. Rewa gets killed in a severe desert sandstorm. She gets frozen in a moment of time. As events unfold towards the end, we come to know that Sameer is the rebirth of Mehru and that Rewas elder sister, Tara successfully managed to cross the desert when her camel ran ahead. Tara is now older and has a daughter named after her dead, lost sister, Rewa. Ustad Miraj Ali also is alive and at Taras house although he is very old. As soon as Sameer reaches Taras house with the news about Rewa, Ustad Miraj Ali recognises him as Mehru and dies in his arms. Sameer also discovers a skeleton of Raja Param Singh in the castle dungeon. The 2 gold teeth in the skull help establish the identity of that skeleton as belonging to King Param Singh. It remains a mystery how King Param Singh died in the castle dungeon and how Ustad Miraj Ali managed to escape.

Sameer eventually ends up fulfilling his commitment to help Revas spirit not only cross the desert but also liberate her from the period of time in which she is stranded.

==Message conveyed==
Lekin is the story of a soul of a woman, Rewa, caught in a time warp, somewhere between the realm of the living and the dead. She dies while she is trying to escape her long captivity and her ominous portending rape by the local Raja. She is caught in a storm in the desert as she is desperately trying to cross the desert to reach her paternal home. The winds blow in such a fateful way that her body and soul are left struggling under their force, till she succumbs and falls and then gets slowly submerged under the ruthless sands. The man, Sameer, to whom she appears, as a lost soul, time and again, is perplexed till he finally realises that she does not want to die, but sorrow and suffering have made her too scared to live.

Rewa thus is the quintessential representation of many of us who have gone through a traumatic experience that has wrenched the life out of us. We want our life back, just as it was before the trauma, but are too scared….to live. We do not want to die but are too fearful of life itself. So like Rewa, we get caught in no mans land, flitting between the living and the dead, the real and the unreal, the concrete and the illusion, the fact and fiction, the conscious and the sub-conscious. And just like Rewa has the hope that Mehroo (the man who is supposed to be her saviour) will come and take her across the desert, we all harbour hopes that someone else will come and break this ever compulsive repetitive cycle of our pain and suffering and free us from its seemingly inescapable clutches.

The moment we realise that the compulsive repetitiveness is the creation of our own mind, we free ourselves. The moment we realise that we are in a prison, the door of which is perpetually open, we free ourselves. The moment we live in the present, and stop ruminating on the past or worrying about the future, we free ourselves. The moment we realise that our soul has no constraints and no one can trap it or stunt it and that it transcends beyond all boundaries, we free ourselves. We all have to ourselves escape the deserts of our own minds, just as Rewa ultimately does, all by herself. The mind can be its own prison and its own salvation. It depends on us and what we choose it to be. So live every moment. Thats all that is ours anyway.

==Cast==
* Vinod Khanna – Sameer Niyogi
* Dimple Kapadia – Reva
* Amjad Khan – Shafi Ahmed Siddiqui
* Beena Banerjee – Sharda, Shafis wife
* Alok Nath – Ustad Meraj Ali
* Hema Malini – Tara (guest appearance)
* Moon Moon Sen – Pammi
* Vijayendra Ghatge – Raja Param Singh

==Songs==
All the songs from Lekin... were composed by Hridaynath Mangeshkar and the lyrics were written by Gulzar (lyricist)|Gulzar. Suniyo Ji Araj Mhario is based on the composition in Raga Vihanginee by Pt. Mani Prasad
* "Yaara Seeli Seeli" – Lata Mangeshkar
* "Kesariya Baalma Oji Ke Tumse Laage Nain" – Lata Mangeshkar
* "Kesariya Baalma Mohe Bawari Bole Log" – Lata Mangeshkar
* "Suniyo Ji Araj Mhario" – Lata Mangeshkar
* "Main Ek Sadi Se Baithi Hoon" – Lata Mangeshkar
* "Jhoote Naina Bole Saanchi Batiyaan" – Asha Bhosle, Satyasheel Deshpande
* "Surmai Shaam" – Suresh Wadkar
* "Ja Ja Re" – Lata Mangeshkar, Hridaynath Mangeshkar

==External links==
*  

 

 
 
 
 
 
 
 
 