Calling Paul Temple
{{Infobox film
| name           = Calling Paul Temple
| image_size     = 
| image	=	Calling Paul Temple FilmPoster.jpeg
| caption        = 
| director       = Maclean Rogers
| producer       = Ernest G. Roy
| writer         = Francis Durbridge (radio play)   A.R. Rawlinson   Kathleen Butler
| narrator       =  John Bentley Dinah Sheridan Margaretta Scott
| distributor    = Eagle-Lion (West Germany)
| released       = June 1948
| runtime        = UK 92 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}} British crime John Bentley, Dinah Sheridan and Margaretta Scott.  Paul Temple is called in to help Scotland Yard track down a serial killer who has murdered several wealthy women. John Bentley took over the role of Temple from Anthony Hulme and played him in two further films. It was produced by Ernest G. Roy at the Nettlefold Film Studios in Walton On Thames.

The film featured extensive location work in Canterbury.

==Cast== John Bentley ...  Paul Temple
* Dinah Sheridan ...  Steve Temple
* Margaretta Scott ...  Mrs. Trevellyan
* Abraham Sofaer ...  Dr. Kohima
* Celia Lipton ...  Norma Rice
* Jack Raine ...  Sir Graham Forbes
* Alan Wheatley ...  Edward Lathom
* Hugh Pryse ...  Wilfred Davies
* Wally Patch ...  Spider Williams
* Shaym Bahadur ...  Rikki
* Maureen Glynne ...  Girl in Boat Michael Golden ...  Frank Chester
* Harry Herbert ...  Boatman
* Aubrey Mallalieu ...  Waiter
* John McLaren ...  Leo Brent
* Ian McLean ...  Inspector Crane George Merritt ...  Ticket Inspector
* Mary Midwinter ...  Carol Reagan Hugh Miller ...  Doctor
* Gerald Rex ...  Boy in Boat
* Paul Sheridan ...  Maitre d`Hotel
* Marion Taylor ...  Esther van Ralston
* Merle Tottenham ...  Millie

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 