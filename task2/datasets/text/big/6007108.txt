Lockdown (2000 film)
 
{{Infobox film
| name= Lockdown
| image= Lockdown movie poster 2000.jpg
| caption = Theatrical release poster
| writer=Preston A. Whitmore II |
  starring=Richard T. Jones Gabriel Casseus Master P David "Shark" Fralick DeAundre Bonds Melissa De Sousa Bill Nunn Clifton Powell Sticky Fingaz Joe Torry|
  director=John Luessenhop |
 director of photography=Christopher Chomyn |
  music=John Frizzell | 	 
  distributor=TriStar Pictures (International) Rainforest Films (US) |
  released=September 15, 2000 (International release), February 14, 2003 (US release) |
  runtime=105 minutes |
  country=United States |
  language=English |
  movie_series=|
  awards=
| producer=Mark Burg Jeff Clanagan Oren Koules Stevie Lockett
| gross=$449,482 
}}

Lockdown is a 2000 drama film, starring Richard T. Jones, Clifton Powell, David "Shark" Fralick and Master P.

==Plot==
Avery Montgomery (Jones) is taking time off from college to spend time with his girlfriend Krista (Melissa De Sousa), and help raise their young son. Despite the lack of black swimmers, Avery develops to a championship level, and as a result of a particularly impressive win he gets the opportunity for a possible scholarship.

Cashmere (Casseus), happens to be one of Averys best friends since childhood. Despite the fact that their personalities and lifestyles are quite different, Avery being a levelheaded straight-edge that stays out of trouble. The trio--including a guy named Dre--have been friends since childhood.

Earlier in the day before the swim meet, Cashmere had a run-in with Broadway, another dealer who works under Cashmere in the hierarchy. Cashmere is expected to bring back five $10 rocks. $50 is also expected or else he gets beat up or killed. Broadway happened to be short in his return and it angered Cashmere, who proceeds to knock out Broadway, kick him down a flight of metal stairs, and pulls out a gun to assert his power.

Broadway, knowing he cant fight a gun, gives up and runs off, but vows to get revenge and, after an attempted robbery later in the day where he shoots and kills a young girl at a drive through, he wipes off the gun and tosses it into the backseat of Cashmeres open convertible when he is out of the car, which looks similar enough to his for a mistaken identity case.

After the swim meet, Cashmere and Dre, who were there to cheer him on, convince Avery to come out and celebrate his big victory with them but he just wants to spend some time with Krista instead. However, she tells him to go out and he changes his mind before hops in the car and they drive off. At a certain point, Dre, who is riding in the backseat, finds the gun, and questions Cashmere about violating their unwritten rule. Cashmere has no idea what he is talking about, and the three men realize that they have a strange gun, in their possession, and had no idea where it came from and what it could have been used for.

As they were arguing over how to get rid of it, some cops spotted them and, thinking their car looked similar to Broadways car, came up behind them and pulled them over. One of them, a female officer, orders them out of the car at gunpoint, which they comply to, but a few moments later Cashmeres pitbull runs toward the officer, who shoots him dead, in self-defense. Cashmere pulls out his gun in anger, and manages to cock it and point it at the officer before being shot in the shoulder and knocked down.

After being wrongfully convicted, the three are sent to the same medium security prison while serving a ten-year prison sentence for manslaughter (whilst Cashmere probably gets 30 years since he most likely also gets charged with attempting to murder a police officer). Each man experiences different events: Cashmere, who beats up and threatens his cellmate; Dre who is raped and turned to a prison sex slave by his psychotic cellmate named Graffiti (David "Shark" Fralick), who controls much of the prisons drug flow and is the leader of a neo-nazi gang; and Avery has a cellmate who is a man named Malachi Young (Clifton Powell) who is in jail for 18 years (probably for murder). Avery tries to take his anger out on him just like Cashmere but Malachi has the upper hand and pins him against the door before warning him that if hes bringing conflict to the cell, hes bringing it to him. Graffitis rival, Clean Up (Master P) is another drug trafficker who Cashmere began to work for.

Meanwhile, Charles Pierce, the College Scout who Avery met on his fateful night, also happened to be an accomplished lawyer and knew without being told that Avery was wrongfully convicted and decided to help his release efforts. Avery was resentful and resistant at first, towards both Pierce and Krista (at one point yelling at her to never come back because it "would do them both better") but eventually accepted both their visitations and that they could only help. Graffiti finds two drug shells that he got from his two bisexual girlfriends in his cell toilet and he along with his gang are back in business but a corrupt guard named Perez warns them not to retaliate against Clean Ups gang. Graffiti and the gang kill Nuke anyway. His dead body is found in the prison laundry room, and the warden orders a lockdown for 24 hours while on the next night, Dre tries to resist Graffiti but is overpowered and is raped by force of oral sex. One day, Dre shoots up heroin comes back to his cell to find an angry Graffiti waiting for him to return. Dre tries to strangle Graffiti while eventually starting to build some resistance. Graffiti, whos twice his size, eventually gets the upper hand back and starts to beat him up bad. Then Avery who happened to be on his way to see Dre, despite Malachis warnings not to (because a bitch is property and you dont mess with another inmates property which can mean even the simple smalltalk Avery wanted) sees him getting beat to death and jumps in to protect him. Avery, who is strong, muscular and Graffitis size, jumps on him and beats him to a pulp, but not before a bunch of Graffitis guys jump on Avery and Malachi has to save him, beating up several of them and throwing one over the second floor railing onto a table.

Malachi, Avery, Graffiti, Dre and the others involved had to go to a disciplinary hearing, in which Malachi, in the ultimate act of unselfishness and sacrifice (after being allowed to go in first, even though he had almost nothing to do with most the incident), takes responsibility for the entire thing to spare Avery a dirty discipline record and assault charge. Then in an unexplained and unnecessary move, flies into a rage in front of the board, screaming and throwing chairs around the room and getting shipped out to a maximum security prison for his actions. But at this point, Avery has gleaned enough that he can now be solo like Malachi and it wont be a huge loss for him. Avery then gets a new cellmate who tries the same greeting to Avery and Avery gives him the same response he got from Malachi and he becomes his mentor.

Later on, Cashmere and Cleanup approach Dre (after realizing the only way to get their drug business going is to get rid of Graffiti) to inform him that its not over with Graffiti and that he needs to watch his back (implying that Graffiti is going to kill him, so its kill or be killed situation). Cashmere offers Dre drugs to help him do what he needs to do to Graffiti. Dre approaches Graffiti at a singing event at the prison and stabs him to death. Dre is killed with a blow to the head from the nightstick of the crooked guard, Perez.

Meanwhile, somebody has been snitching and busting Clean Ups drug mules and he comes to the conclusion that Avery is the snitch. This is not true, but he assumes anyway and wants Cashmere to take care of him. During a riot in the prison yard (which happens to break out at the same time Charles Pierce brought prison officials a dying declaration from Broadway...right before he hanged himself in prison doing his own time, which meant Averys immediate release), Cashmere goes up to Avery and stabs him. Avery fights through the stab wound and tries to fight Cashmere off for his life. Cleanup, seeing hes not doing the proper job, stabs Cashmere in the back and tries to kill Avery himself. As he is now fighting off Cleanup, Cashmere, who still had love for Avery despite their differences inside, comes up behind Clean Up and stabs him to death.

Avery, after a minute of wondering if he could still trust him, realizes that it was all in the game and even though he didnt see the Broadway letter yet, realized inside that Cashmere (who he originally blamed for getting him locked up) didnt do anything either after all. Meanwhile, a guard named Barkley tries to order Cashmere to drop the shank. Cashmere, either because he somehow didnt hear it or just out of spite, keeps holding it. Just as he and Avery are about to embrace, a guard unloads a shot into his back on Barkleys order, killing him instantly.

Prison Officials get to Avery before anything else can potentially happen and he gets released into the waiting arms of Krista and his son.

==Cast==
* Richard T. Jones as Avery Montgomery 
* Gabriel Casseus as Cashmere 
* DeAundre Bonds as Dre
* Melissa De Sousa as Krista
* Bill Nunn as Charles  
* Clifton Powell as Malachi Young
* Sticky Fingaz as Broadway
* Joe Torry as Alize 
* Master P as Clean Up
* David "Shark" Fralick as Graffiti
* Andrew Divoff as Gendarme (Graffiti friend)
* Lloyd Avery II as Nate
* Dianna St. Hilaire as Martina

== References ==
{{reflist|refs=
}}

== External links ==
*  
*  

 

 
 
 
 
 
 