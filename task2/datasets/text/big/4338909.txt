Countdown (1968 film)
{{Infobox film
| name           = Countdown
| caption        = 
| image          = Countdown FilmPoster.jpeg
| director       = Robert Altman
| producer       = William Conrad
| screenplay     = Loring Mandel
| based on       =   James Caan Joanna Moore Michael Murphy Ted Knight
| music          = Leonard Rosenman
| cinematography = William W. Spencer
| editing        = Gene Milford
| distributor    = Warner Bros.-Seven Arts
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         =
}} James Caan and Robert Duvall as astronauts tvying to be the first American to walk on the Moon as part of a crash program to beat the Soviet Union. 

==Plot==
In the late 1960s, astronauts orbiting the Earth in the Apollo 3 spacecraft have their mission ended early. They grumble about it, but their commander, Chiz (Robert Duvall), knows the reason for the abort: the Pilgrim Program. The Russians will be sending a moon landing mission up in four weeks. The Americans had a secret alternate plan to the Apollo program (Pilgrim) in case this happened. One man would be sent to the moon in a one-way rocket, a modified Project Gemini craft. He would stay on the moon for a few months in a shelter pod launched and landed before him. Later, a manned Apollo mission would come to retrieve him.
 James Caan), one of Chizs crew, is tapped. Chiz is outraged, but agrees to train Lee in the few days they have. Chiz pushes Lees training hard, half to get him ready, half hoping he will drop out and Chiz can step in. Lee persists, driven by the same astronaut dream. 

After a press leak about Pilgrim, the Russians launch a week early. Deflated at not being first, everyone carries on. The shelter pod (a LEM lander) is launched and landed successfully. Lee is launched on schedule. He encounters a power drain malfunction en route which tests his character and hinders radio contact. The Russians have also lost contact with their team. As Lee orbits the moon, he does not see the beacon of the shelter. With only seconds left before he must abort and return to Earth, he lies about seeing it. Mission Control okays his retro burn and he lands. Now all radio contact is lost. Lee gets out of the Gemini lander and walks around with three hours of oxygen in his suit. He finds the crashed Russian lander on its side, the three dead cosmonauts sprawled around it.

Everyone on Earth is nervously awaiting news, but none comes. Lee takes the Soviet flag from a dead cosmonaut and lays out the Soviet flag on a nearby rock with his own American flag. With little air left and nowhere to go, Lee spins the toy mouse his son gave him. It points right, so he walks in that direction. People on Earth are losing hope as his time has run out. Lee looks at his watch to see that he has just minutes of air left. A red glow on his arm catches his attention. It is the locator beacon atop the shelter. Lee is last seen walking towards the shelter.

==Cast== James Caan as Lee Stegler
*Joanna Cook Moore as Mickey Stegler (as Joanna Moore)
*Robert Duvall as Chiz
*Barbara Baxley as Jean
*Charles Aidman as Gus
*Steve Ihnat as Ross Duellan Michael Murphy as Rick
*Ted Knight as Walter Larson
*Stephen Coit as Ehrman
*John Rayner as Dunc
*Charles Irving as Seidel
*Bobby Riha as Stevie Stegler (as Bobby Riha Jr.)

==Production==
Countdown benefited from the cooperation of NASA, lending facilities to enhance the production.  

==Reception== Howard Thompson calls the film a "limp space-flight drama" which "makes the moon seem just as dull as Mother Earth".    A February 1985 review in Malaysias New Straits Times calls Countdown "dated" and complains that the characters have "no depth or direction".    In a June 1995 story on the history of spaceflight movies, Thomas Mallon appreciates that the film "highlights the space programs early can-do ethos" but calls it a "little movie" with "few touches of Mr. Altmans later cynical wit" and "somehow not terribly suspenseful".   

==Other media==
A comic book   was published by Dell Comics in October 1967.

== See also ==
  Apollo 13, a 1995 film dramatizing the Apollo 13 incident
*  Gravity (film)|Gravity, a 2013 3D science-fiction space drama film
*  List of films featuring space stations
*  Love (2011 film)|Love, a 2011 film about being stranded in space
*  Survival film, about the film genre, with a list of related films

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 