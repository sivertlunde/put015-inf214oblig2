977 (film)
{{Infobox film
| name           = 977
| image          = 977.2006Poster.jpg
| image size     = 
| caption        = Russian film poster
| director       = Nikolay Khomeriki
| producer       = Arsen Gottlieb Vladimir Luzanov
| writer         = Nikolay Khomeriki Yuni Davydov Aleksandr Rodionov
| starring       = Fyodor Lavrov
| music          = 
| cinematography = Alisher Khamidkhodjaev
| editing        = Igor Kireyev
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = Russia
| language       = Russian
| budget         = 
}}
 Russian drama film directed by Nikolay Khomeriki. It was screened in the Un Certain Regard section at the 2006 Cannes Film Festival.   

==Cast==
* Fyodor Lavrov - Ivan
* Klavdiya Korshunova - Rita
* Yekaterina Golubeva - Tamara
* Pavel Lyubimtsev - Sergei Sergeyevich
* Alisa Khazanova - Sonya
* Andrei Kazakov - Gosha
* Olga Demidova
* Sergei Tsepov - Valera
* Igor Ovchinnikov Sergei Petrov
* Stanislav Mikhin
* Anna Ardova
* Tatyana Mitienko - Lyubov
* Irina Barskaya - Twin Ira
* Darya Barskaya - Twin Masha
* Leos Carax - Technic

==References==
 

==External links==
* 
* 

 
 
 
 
 
 