BoyTown
 
 
{{Infobox film
|  name           = BoyTown |
  image          =|
  caption        = Promotional poster for BoyTown.|
    producer       = Greg Sitch   Mick Molloy |
  director       = Kevin Carlin |
  writer         = Mick Molloy   Richard Molloy | Bob Franklin   Wayne Hope   Gary Eck   Lachy Hulme   Sally Phillips   Lois Ramsey |
  music          = Gareth Skinner |
  cinematography = Mark Wareham |
  editing        = Angie Higgins | Roadshow Films |
  released       = 19 October 2006 |
  running time   = 88 minutes |
  language       = English |
  country        = Australia|
  budget         = |
}} Bob Franklin, Wayne Hope and Gary Eck. It was filmed in Melbourne, Victoria (Australia)|Victoria.

An official preview showing of the film was held in Sydney on 16 October 2006. BoyTown was released in Australia on 19 October 2006. It grossed   in its home country. 

==Plot==
BoyTown, the greatest boyband of the eighties and the group that started the boyband phenomenon leave their terrible & bad-paying lives for one last crack at the big time. They return to the stage with slightly older fans and slightly larger pants to complete some unfinished business. These days they spend less time singing about tears, eternity, angels and their baby and more time singing about divorce, shopping and picking up the kids from school & working the nearby Coles, construction site or the nearest radio station in Melbourne (with dinner).

As their triumphant tour draws to a conclusion all the personal issues that have been safely buried away for twenty years come bubbling to the surface with explosive consequences. Suddenly the band has to deal with: allegations of infidelity; paternity tests; a miming fiasco; a band member outing himself live on stage; an awards night disaster; solo albums; a mysterious disappearance and a plane journey that will cement the BoyTown legend forever & make them music legends with expert music help.

===BoyTown Confidential=== Tony Martin as "Kenny Larkin", his character from the movie.

The mockumentary was supposed to be included in its entirety on the DVD release of BoyTown, but was not included. Mick Molloys Molloy Boy Productions has commented that it was left out due to lack of post-production funding, however Tony Martin said that he would have paid the estimated $5000 post-production cost as he believed it was one of his finest works. Speculation persists that Molloy thought the mockumentary would upstage the film itself. This has led to an ongoing rift between longtime collaborators Martin and Molloy. 

==Cast==
*Glenn Robbins as Benny G
*Mick Molloy as Tommy Boy Bob Franklin as Bobby Mac
*Wayne Hope as Carl
*Gary Eck  as Corey
*Sally Phillips as Holly
*Lachy Hulme as Marty Boomstein Sarah Walker as Katie
*Lois Ramsey as Gran
*Victoria Hill as Rosalita
 Tony Martin, Josh Lawson, Ed Kavalee, Akmal Saleh, James Mathison and Ella Hooper.

== Soundtrack ==
The soundtrack was released in Australia by Liberation Music on 30 September, featuring vocal performances by Joel Silbersher, Bram Presser, Simon Cleary, Christian Argenti and Julian Argenti as "BoyTown", with music by Gareth Skinner, and lyrics by Mick Molloy & Richard Molloy.

Tracklisting:
#  "Boytown 89"
#  "Tough Titties"
#  "Love 2 Love"
#  "I Cry"
#  "Angel Baby"
#  "Picking The Kids Up From School"
#  "Layby Love"
#  "Ring My Bell"
#  "Pussywhipped"
#  "Love Handles"
#  "Parent Teacher Night"
#  "Dishpan Hands"
#  "Cellulite Lady"
#  "Special Time (Of The Month)"
#  "Do Me"
#  "Stay at Home Dad"
#  "Boytown 2006"
#  "Seasons in the Sun"
#  "My Beautiful Wife"
#  "Gold" (Performed By Spandau Ballet)
#  "Love 2 Love"

==Box office==
BoyTown grossed $3,135,972 at the box office in Australia. 

==See also==
* Cinema of Australia

==References==
 

== External links ==
*  official website
* 
* 
*  official Myspace

 
 
 
 
 
 
 
 
 