Arizona Days (1937 film)
{{Infobox film
| name           = Arizona Days
| image_size     =
| image	         = Arizdayspos.jpg	
| caption        = Film poster John English
| producer       = Edward Finney
| writer         = Lindsley Parsons (story) Sherman L. Lowe (screenplay)
| narrator       =
| starring       = Tex Ritter Eleanor Stewart Syd Saylor
| music          =
| cinematography = Gus Peterson
| editing        = Frederick Bain
| distributor    = Grand National Pictures
| released       = 22 November 1936
| runtime        = 57 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Western singing cowboy Tex Ritter made for producer Edward Finney for Grand National Pictures.  Many public domain prints are missing sequences from the original release.

==Plot==
Tex and his sidekick "Grass" Hopper are delighted to join a travelling music show.  When a group of cowboys come in without paying, Tex steps down from the stage, pulls his six gun and holds the audience up until the manager points out the men who did not pay their admission fee.  After seeing the way Tex gathers revenue from cheats, the County commissioner offers Tex a job as a tax collector.

==Cast==
Tex Ritter	... Tex Malinson  
Syd Saylor  ... Claude "Grass" Hopper  
William Faversham ... Professor McGill  
Eleanor Stewart ... Marge Workman  
Forrest Taylor ... Harry Price  
Snub Pollard  ... Cookie  
Tommy Bupp 	... Billy Workman   
Glenn Strange ... Henchman Pete  
Budd Buster	... Sheriff Ed Higginbotham  
Salty Holmes... Harmonica Player 

==Soundtrack==
High, Wide and Handsome 
Written by Tex Ritter and Ted Choate 
Sung by Tex Ritter  
Tombstone, Arizona 
Written by Tex Ritter and Jack C. Smith 
Sung by Tex Ritter   
Arizona Days  
Written by Tex Ritter and Jack C. Smith 
Sung by Tex Ritter  
If Love Were Mine  
Written by Frank Sanucci
Sung by Tex Ritter

== External links ==
*  


 
 
 
 
 
 
 


 