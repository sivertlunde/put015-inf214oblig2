The Monk and the Woman
 
{{Infobox film
| name           = The Monk and the Woman
| image          = 
| image_size     =
| caption        = 
| director       = Franklyn Barrett
| producer       = Franklyn Barrett George Marlow Harry Musgrove
| writer         = Franklyn Barrett D.H. Souter (titles)   
| based on       = play by Frederick Melville
| narrator       =
| starring       = 
| music          =
| cinematography = Franklyn Barrett
| editing        =  studio = Australian Famous Players
| distributor    = 
| released       = 8 October 1917 
| runtime        = 6,000 feet
| country        = Australia
| language       = Silent film  English intertitles
| budget         = £1,000 
| preceded_by    =
| followed_by    =
}}

The Monk and the Woman is a 1917 Australian silent film directed by Franklyn Barrett. It is considered to be Lost film|lost. 

==Plot==
In eighteenth century France, the evil Prince de Montrale (Harry Plimmer) falls in love with Liane (Maud Fane), but she runs away from him and seeks refuge in a monastery. The prince finds her and orders the abbott to keep her in custody. A young novice, Brother Paul (Percy Marmont), is placed in charge of Liane and falls in love with her, despite having just taken his vows of celibacy.

The king (Monte Luke) commands that Liane marry the Prince. A wedding is prepared but Paul defeats the prince in a duel, steals his cloak and takes his place at the wedding. The impersonation is discovered and Paul is taken away to be executed. De Montrale leads a revolt against the king but Paul keeps fight off the attack until loyalists arrive. Paul then returns to the monastery forever. 

==Cast==
*Maud Fane as Countess Liane
*Percy Marmont as Brother Paul
*Harry Plimmer as Prince de Montrale
*Monte Luke as the king
*Alma Rock Phillips as de Vernet
*Ruth Wainwright as Mme de Vigne, the kings favourite
*Hugh Huntley as Armande
*Charles Beetham
*George Young
*Mattie Ive as page boy

==Original play==
The play was first produced in England in 1912 where it ran for a year. Theatre entrepreneur George Marlow bought the Australian rights and presented it in 1912.  It was controversial and received protests from church groups, being condemned by the Australian Catholic Federation.  

The film version changed the ending so that Brother Paul renounced his loved one and returned to the monastery, whereas in the play the two lovers stayed together. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 71 

==Production==
The film adaptation was likely made in response to the 1917 film The Church and the Woman. Shooting began in August 1917 and took place on the stage of the Theatre Royal in Sydney, and on location in Frenchs Forest, Sydney, with some additional scenes shot at W.D. Daileys castle in Manly.   

Percy Marmont, Hugh Huntley and Maud Fane were all British stage stars then touring Australia; this was Marmonts film debut.  

==Release==
Despite changing the ending, the film was still seen as controversial by the Catholic Federation, who objected to its depiction of Catholicism. However, it was still passed by the censor.  Advertisements heavily played up the controversy  and the movie was a major success at the box office.   It was still screening in cinemas in 1922. 

===Critical===
In the words of the Sunday Times "costume plays are notoriously at once difficult and expensive of production, and the utmost care must be exercised in order to escape anachronism. In this direction the pitfalls  seem to have been avoided in the skilled hands o£ Mr W. Franklyn Barrett, who adapted the play to photography and produced; of Mr. D. H. Souter, who is responsible for the artistic titles; and of Mr. Rock Phillips, who undertook the art production." 

===Legal Issues===
George Marlow sued the makers of The Church and the Woman (1917) and got a court injunction forcing the producers of the latter to advertise their film specifically saying it was different from The Monk and the Woman.  This was done. 

==See also==
*List of lost films

==References==
 

==External links==
* 
*  at National Film and Sound Archive
* 
 

 
 
 
 
 
 
 
 