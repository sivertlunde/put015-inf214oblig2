Born to Race (2011 film)
{{Infobox film
| name           = Born to Race
| image          = 
| image_size     = 
| caption        = 
| director       = Alex Ranarivelo
| writer         = 
| narrator       =  Joseph Cross John Pyper-Ferguson   Nicole Bandaan   Brando Eaton   Sherry Stringfield   Spencer Breslin
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 31 December 2011 (USA) 28 January 2013 (UK)
| runtime        = 98 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Joseph Cross and John Pyper-Ferguson.

== Plot ==
 Joseph Cross), a rebellious young street racer on a collision course with trouble. After an accident at an illegal street race, he is sent to a small town to live with his estranged father (John Pyper-Ferguson), a washed up NASCAR racer. Attending to High School, Danny meets Jessica (Nicole Bandaan). She invites him to a party. Attempting to integrate Danny accepts the invitation. At the party Danny runs into the local hot shot Jake Kendall (Brando Eaton). The two characters clash. Jake rules the neighborhood with fierce and Danny will have to live up to those standards in order to become accepted, which means that he shall have to race again.

Meanwhile the relationship between Danny and his father has its own ups and downs. Danny cannot get over the fact that his father has left him and his mother when he was still a young child. In his struggle to find his ways in his new hometown he cannot get around his father and through dialogue the two slowly become to realize that they have more in common then they were suspecting. When Danny decides to enter the NHRA High School Drags, he is forced to seek his fathers help in taking down his rival Jake Kendall.

== Cast ==
 Joseph Cross as Danny Krueger
*John Pyper-Ferguson as Frank Krueger 
*Brando Eaton as Jake Kendall
*Nicole Badaan as Jessica
*Sherry Stringfield as Lisa Abrams
*Spencer Breslin as Max
*Christina Moore as Ms. Parker 
*Erik Kingas Mr. Briggs 
*Johanna Braddy as Rachel  Matt McCoy as Joe 
*Grant Showas Jimmy Kendall  Walter Perez as Tony 
*Michael Esparza as Sanchez 
*Ali Afshar as Himself 
*Whitmer Thomas as Harry

== External links ==
*  

 
 
 


 