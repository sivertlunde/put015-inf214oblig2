A Whisper to a Roar
A British actor Alfred Molina.

==Production== financial crisis of 2008. However, Prince Moulay Hicham agreed to fund the project in full soon after.
 Esraa Ahmed Ahmed Maher of Egypt; Anwar Ibrahim of Malaysia; Morgan Tsvangarai of Zimbabwe; and Viktor Yushchenko of Ukraine.  Filming concluded in 2011 with final trips to Egypt following the Arab Spring and to Malaysia to interview former Prime Minister Mahathir Mohamad.

Prince Moulay Hicham was tapped to interview many of the heads-of-state included the in film, creating an extensive library of footage that is being developed into a separate project.  Arab Spring broke out in Tunisia and Egypt. The crew traveled back to Egypt again in April 2011 to capture the aftermath. They were also able to get a late interview with Mahathir Mohamad in Malaysia.
 Antonio Villaraigosa.

==Reviews==
 National Archives, the United States Department of State, Facebook, Google, and several academic institutions both in the United States and abroad.  Former Secretary of State, Hillary Rodham Clinton said of A Whisper to a Roar: "The film is not only a riveting documentary, but also offers inspiration to people everywhere who seek to make governments accountable to the citizens they serve.  The stories in “A Whisper to a Roar” demonstrate that democracy is a product of tremendous sacrifice, and we are all responsible for securing its promise for future generations."

The film was widely reviewed including by The New York Times, The Village Voice, The Los Angeles Times and Variety (magazine)|Variety.    

The film was released on DVD and digital download on April 16, 2013 and is available in eight languages.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 