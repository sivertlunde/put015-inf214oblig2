Periyar (2007 film)
{{Infobox film
| name           = Periyar
| image          = Periyar_Movie_official_DVD_Cover.jpg
| caption        =
| director       = Gnana Rajasekaran
| producer       = Liberty Creations Limited
| writer         = Gnana Rajasekaran
| starring       = Sathyaraj Jyothirmayi Kushboo Swarnamalya Vennira Aadai Nirmala Vidyasagar
| cinematography = Thangar Bachan
| editing        = B. Lenin
| studio         = Liberty Creations Limited
| distributor    = Liberty Creations Limited
| released       =  
| runtime        = 188
| country        = India
| language       = Tamil
| budget         =
| gross          =
}} Tamil biographical film, made about the life of the social reformer and rationalist Periyar E. V. Ramasamy   with Sathyaraj who himself is a rationalist in the lead role.
==Plot== Kashi which changed his life where he understands the cruelty of the Hindu Caste System that only Brahmins are welcomed and he was refused meals at choultries which exclusively fed Brahmins forbidding other Hindu castes. Having starved severely, Periyar found no other better way than to enter a choultry disguises himself with the appearance of a Brahmin wearing a thread on his bare chest but it is found out and is disgusted with life in Kashi.  Periyar returns and joins his fathers business and later becomes the Chairman of the Erode Municipality. Later, which he resigns from this post and joins the freedom struggle. He becomes the President of Congress party of Madras Presidency. Later he quits the Congress party and joins the Justice Party. He goes on to format the DK. His role in the Anti-Hindi agitation and Vaikom struggle. His second marriage and the formation of the DMK. It covers his entire life till death. 

==Cast==
*Sathyaraj as Periyar E. V. Ramasamy Chandrasekhar as Periyars friend Ramanathan
*Jyothirmayi as Nagammal
*Kushboo as Maniammai
* Nizhalgal Ravi
*S. S. Stanley as C. N. Annadurai
*Mohan Ram as B. R. Ambedkar
*Swarnamalya

==Music==
{{Infobox album |  
| Name       = Periyar
| Type       = Soundtrack Vidyasagar
| Cover      = 
| Released   = 2007
| Recorded   = 
| Genres     = World Music
| Length     =
| Label      =  Vidyasagar
| Last album = Majaa (2007)
| This album = Periyar (2007)
| Next album = Muniyandi Vilangial Moonramandu (2007)
}} Vidyasagar

{{Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| length1 = 
| title1 = Kadavul Ulagathai Padaichaar
| extra1 = Mathu Balakirushnan, Guru Charan, Muralitharan, Surya Prakash 
| lyrics1 = Vairamuthu
| length2 = 
| title2 = Idai Thazhuvi Kola Jadai
| extra2 = Priya Subramaniyan 
| lyrics2 = Vairamuthu
| length3 =
| title3 = Kadavula Ni Kallaa Melor
| extra3 = Manicka Vinayagam, J.K.V.Roshini, Mathu Balakirushnan, Chandran 
| lyrics3 = Vairamuthu
| length4 =
| title4 = Thai Thai Thakka Arul Seiy
| extra4 = Vijayalakshmi Subramaniam 
| lyrics4 = Vairamuthu
| length5 =
| title5 = Thaayum Yaaro Thanthai Yaaro
| extra5 = K.J.Yesudas
| lyrics5 = Vairamuthu
}}

==see also==
*Kamaraj (film)
*Bharathi (film)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 