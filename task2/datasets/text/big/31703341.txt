Playing for Keeps (2012 film)
{{Infobox film
| name           = Playing for Keeps
| image          = Playing for Keeps Poster.jpg
| caption        = Theatrical release poster
| director       = Gabriele Muccino
| producer       = Gerard Butler Kevin Misher Jonathan Mostow Alan Siegel John Thompson Tim Ubels
| writer         = Robbie Fox
| starring       = Gerard Butler Jessica Biel Catherine Zeta-Jones Dennis Quaid Uma Thurman Judy Greer Andrea Guerra
| cinematography = Peter Menzies Jr.
| editing        = Padraic McKinley Misher Films Millennium Films 
| distributor    = FilmDistrict
| released       =  
| runtime        = 105 minutes  
| country        = United States
| language       = English
| budget         = $35 million
| gross          = $24,086,455 
}}
Playing for Keeps is a 2012 American romantic comedy film directed by Gabriele Muccino, starring Gerard Butler with Jessica Biel, Catherine Zeta-Jones, Dennis Quaid, Uma Thurman and Judy Greer in supporting roles. The film was released on December 7, 2012, in the United States and Canada by FilmDistrict.

==Plot== game memorabilia and become a sports announcer are largely met with ambivalence. Georges relationship with his son Lewis (Noah Lomax) is equally unsuccessful due to him only seeing Lewis sporadically. When he discovers that his ex-wife Stacie (Jessica Biel) is getting married to her boyfriend Matt (James Tupper), George grows despondent. 

After dropping off an audition tape of him practicing his sports announcements, George goes to help his sons soccer team practice. The team isnt very good, with the coach giving little attention to his players. The teams parents quickly pressure Stacie  to ask George to become the new coach, which he reluctantly agrees to. Once coach, George attracts the attentions of various mothers and receives a bribe from Carl King (Dennis Quaid), who wants him to give his children preferential treatment. He specifically draws attention from the divorced Barb (Judy Greer), ex-sportscaster Denise (Catherine Zeta-Jones), and Carls wife Patti (Uma Thurman). Denise appears to be particularly forward with George, sending him an e-mail telling him that shes thinking of him. 

At practice the next day George is invited to a dinner party at Carls house, is approached by Barb, and is also told by Denise that she has been given a copy of his audition tape to watch and pass along. At Carls party George learns that Carl has beenhaving affairs and that his wife is aware of his infidelities, unbeknownst to Carl. Carl then lends George a Ferrari under the implication that he "takes care of his friends", which George uses to drive to see Stacie. The two discuss what could have developed between the two of them, to which Stacie says that she doesnt wonder about the past anymore.

When he gets home, he discovers Barb waiting for him. She confesses that shes very lonely and has set up a dating profile in order to find a match. After asking him if he finds her attractive, the two sleep together. The following day George is called by Carl, who asks him to pick up some money from Patti in order to bail him out of jail, as he got into a fight at the party. Doing so makes him late to pick up his son, but he manages to entertain Lewis by letting him ride in his lap and drive the Ferrari. During this time George discovers that Lewis is sad that his mother is marrying Matt and that he wont call Matt "dad". George is then called by Denise, who informs him that ESPN is looking for a new soccer sportscaster and that he must come to the studio to record a tape. This enrages Stacie and Lewis due to him being late to pick up Lewis again, weakening his relationship with the both of them.

Arriving home, George is berated by his landlord Param (Iqbal Theba) for not paying his rent while driving a Ferrari and receives a call from Patti, telling him that shes in his bed. He soon finds that shes in the landlords bed and George manages to distract the landlord by paying him with Carls bribe money. Despite this, Patti continues to approach George sexually, who rebuffs her while saying that she should leave Carl rather than having an affair. His relationship with Lewis worsens when Denise kisses George the next day, leading Lewis to realize why his father was late. This spurs Lewis into having a fight during a game later on, prompting Lewis to tell his mother that he wants to quit playing soccer. George manages to later coax Lewis into playing soccer in the rain, which both of them finds fun. Meanwhile Stacie and George begin to reconnect romantically, which causes small rifts in her relationship with Matt.

George manages to earn the job with ESPN, but this necessitates him moving across the country to Connecticut. He asks Stacie to come with him. She initially refuses, but then he meets her at her car and she kisses him. At the game later that same day, George finds that Barb has begun to date his landlord and that Carl has discovered pictures of Patti in Georges house from the time she came on to him. The two men begin to fight, which Stacie witnesses. Stacie sees the pictures, which upsets her despite George claiming that it wasnt what it looked like. During the fight, Lewiss team wins the game.

After the game George leaves for his new job. In the end George chooses to stay with Lewis rather than moving to Connecticut. He also begins a new relationship with Stacie, who has broken off her engagement with Matt, and becomes a local sportscaster in Virginia with his friend Chip.

==Cast==
* Gerard Butler as George Dryer
* Jessica Biel as Stacie Dryer
* Uma Thurman as Patti King
* Noah Lomax as Lewis Dryer, Georges and Stacies son
* Catherine Zeta-Jones as Denise
* Dennis Quaid as Carl King
* Judy Greer as Barb
* James Tupper as Matt
* Iqbal Theba as Param
* Marlena Rayne as Samantha
* Aidan Potter as Hunter

==Production== extras to appear in the film. 
Filming began during the week of April 5, 2011. 

On July 16, 2012, FilmDistrict changed the title from "Playing the Field" to "Playing for Keeps". 

==Reception==
Critical reception for the movie has been overwhelmingly negative, with the film holding a 4% "rotten" rating on  , the film has a score of 27 out of 100, based on 25 reviews, indicating generally unfavorable reviews. 

===Accolades===
 
{| class="wikitable"
|-
! Award !! Category !! Recipient(s) !! Result
|- 33rd Golden Raspberry Awards 
| Worst Supporting Actress
| Jessica Biel
|  
|}

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 