Song of China
{{Infobox film
| image          = 
| caption        = 
| name           = Song of China
| writer         = Zhong Shigen Chen Yanyan Zhang Yi Li Zhuozhuo
| music          = Wei Zhongle
| director       = Fei Mu Luo Mingyou
| cinematography = Wang Shaofen
| producer       = Luo Mingyou
| studio         = Lianhua Film Company
| runtime        = 65 min
| released       = China: 1935 United States: November 9, 1936 China
| Mandarin
| budget         =
|
}} 1935 Cinema Chinese film directed by Fei Mu and Luo Mingyou for the Lianhua Film Company. 

Unlike earlier Lianhua films that railed against traditional society, Song of China is representative of the New Life Movement of Chiang Kai-Shek, which celebrates traditional Confucian values.   

Song of China is one of the few Chinese films made in the 1930s to be screened in the United States. 

== Cast ==
*Zheng Junli  Chen Yanyan
*Lin Chuchu Zhang Yi
*Li Zhuozhuo

== Plot ==
A young man (Zheng Junli) returns home to see his elderly father before the latter dies. The father tells his son to extend the love he feels for his family to the greater good. The young man tries to teach the same principles to his children, who instead run off to the city to enjoy the city. 

The man, however, moves to the countryside to build an orphanage. When his children eventually return, he gives them the same advice his father had given him. 

== References ==
 

== External links ==
*  
*  
*   at the UCSD Chinese Cinema Web-based learning center

 
 
 
 
 
 
 
 

 