A Brewerytown Romance
 
{{Infobox film
| name           = A Brewerytown Romance
| image          = 
| caption        = Frank Griffin
| producer       = Arthur Hotaling
| writer         = Frank Griffin
| starring       = Eva Bell
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = Silent film English intertitles
| budget         = 
}} silent comedy film featuring Oliver Hardy.

==Plot==
Jealousy brews between a dancers boyfriend and a tango champion.

==Cast==
* Eva Bell - Lena Krautheimer
* Raymond McKee - Emil Schweitzer Frank Griffin - Tango Heinz (as Frank C. Griffin)
* Oliver Hardy - Cassidy (as Babe Hardy)

==See also==
* List of American films of 1914
* Oliver Hardy filmography

==External links==
*  

 
 
 
 
 
 
 
 
 
 