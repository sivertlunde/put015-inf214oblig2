Daddy Longlegs (2009 film)
{{Infobox film
| name = Daddy Longlegs
| image = Daddy_Longlegs_U.S._poster_2009.pdf
| caption = Theatrical release poster
| director = Josh Safdie Benny Safdie Andrew Spade (Executive) Red Bucket Films (Co-producer) Sophie Dulac Productions (Co-Producer) Sam Lisenco (Co-Producer) Michel Zana (Co-Producer)
| writer = Josh Safdie Benny Safdie Ronald Bronstein
| starring = Ronald Bronstein Sage Ranaldo Frey Ranaldo Dakota Goldhor Leah Singer
| music = 
| cinematography = Brett Jutkiewicz Josh Safdie
| editing = Brett Jutkiewicz Benny Safdie Josh Safdie Ronald Bronstein
| distributor =  IFC Films
| released =  
| runtime = 97 minutes
| language = English
| country = United States
| budget = 
}} Tom Scott and Casey Neistat.  It premiered at the 2009 Directors Fortnight section of the 61st Annual Cannes Film Festival, under the title Go Get Some Rosemary.  It premiered in the United States at the 2010 Sundance Film Festival in the non-competitive Spotlight section.  It is scheduled to premiere commercially via IFC Films, theatrically and on-demand, on May 14, 2010. 

==Plot==
Lenny, a divorced father and a projectionist at a Manhattan movie theater, spends two weeks with his young sons Sage and Frey.

==Cast==
Source: 
* Ronald Bronstein as Lenny Sokol
* Sage Ranaldo as Sage
* Frey Ranaldo as Frey Ranaldo
* Eleonore Hendricks as Leni
* Leah Singer as Paige
* Abel Ferrara as Mugger
* Lee Ranaldo in a cameo appearance

==Production== Walter Reade Theater (interiors) and the Cinema Village (exterior). 

==Awards== Gotham Award in the category of Best Breakthrough Actor for his performance.  The film took home the John Cassavetes Award at the 2011 Independent Spirit Awards  

==References==
 

==External links==
*  
*   at IFC Films
*  
*  
*  
*   at Metacritic
*  

 
 
 
 
 