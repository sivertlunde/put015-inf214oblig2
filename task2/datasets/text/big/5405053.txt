Different Strokes (film)
{{Infobox film
| name           = Different Strokes
| image          = 
| image_size     = 
| caption        = 
| director       = Michael Paul Girard
| producer       = Robert Giordano
| writer         = Michael Paul Girard	 Samantha James
| music          = 
| cinematography = Gerald M. Williams
| editing        = H. Kim
| distributor    = Coastline Films
| released       = 1997
| runtime        = 88 min 75 min (Edited version) 
| country        = United States
| language       = English
}}

Different Strokes (also titled Different Strokes:  The Story of Jack and Jill...and Jill) is a 1997 erotic drama film about a love triangle involving a young couple and another woman.  Written and directed by Michael Paul Girard, the film stars Dana Plato, Landon Hall, and Bentley Mitchum.  The films title exploits Platos fame from the TV series, Diffrent Strokes. It was Platos first film appearance since 1992, and would be her second to last film before her death in 1999.

Nathan Rabin gave the film a harsh review, stating "The shamelessly titled Different Strokes (...) lacks anything resembling even community-theater-level acting", concluding the film "is notable mainly for its aggressive lack of shame. From its title to its threadbare plot to its community-access-level production values, the film reeks of crass exploitation." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 