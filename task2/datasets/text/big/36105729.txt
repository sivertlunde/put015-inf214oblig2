The Frozen Child
{{Infobox film
| name           = The Frozen Child
| image          = 
| image_size     = 
| caption        = 
| director       = Béla Balogh
| producer       = 
| writer         =  József Eötvös    (story)    Ede Sas
| narrator       = 
| starring       = Mária Szepes   Ferenc Szécsi   Viktor Galánthay   Anna Breznay
| music          = 
| editing        = 
| cinematography = Miksa Ádler
| studio         = Star Filmgyár
| distributor    = 
| released       = 1 September 1921 
| runtime        = 
| country        = Hungary Silent  Hungarian intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Hungarian silent silent drama film directed by Béla Balogh and starring Mária Szepes, Ferenc Szécsi, Anna Breznay and Viktor Galánthay. It is one of the few surviving Hungarian films of the early 1920s. It was unusual for its depiction of poverty in Hungary at a time when this was discouraged or censored by the authorities. 

==Cast==
* Mária Szepes - Terike 
* Ferenc Szécsi - Lacika 
*  Anna Breznay - Házmesterné 
* Mór Ditrói - Pap 
* Viktor Galánthay - Nagy Jóska 
* Rezsö Inke - Barabás,Terike apja 
* Ilona Linke - Kovácsné,Lacika anyja 
* Imre Pintér - Vincze bácsi 

==References==
 

==Bibliography==
* Cunningham, John. Hungarian Cinema: From Coffee House to Multiplex. Wallflower Press, 2004.

==External links==
* 

 

 
 
 
 
 
 
 
 