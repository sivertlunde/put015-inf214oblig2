Bomma Borusa
{{Infobox film
| name           = Bomma Borusa
| image          = Bomma Borusa.jpg
| image_size     = 
| caption        = Official DVD Box Cover
| director       = Kailasam Balachander|K. Balachander
| producer       = Avichi Meiyappa Chettiar
| writer         = K. Balachander
| narrator       =  Chandra Mohan Raja Babu
| music          = R. Goverdhanam
| cinematography = 
| editing        = 
| distributor    = A. V. M. Productions
| released       = 1971
| runtime        = 
| country        = India
| language       = Telugu
| budget         = 
| gross          = 
| preceded_by =
| followed_by =
}}
 Comedy / Drama film directed by Padma Shri awardee Kailasam Balachander|K. Balachander. Chandra Mohan, Raja Babu, and Ramaprabha. 
{{Cite web
|url= http://www.bharatmovies.com/telugu/info/Bomma_Borusa.htm
|title= Bomma Borusa
|accessdate=26 December 2008
|work=  
|publisher=  Bharat Movies
|date=  }} 
It was produced by Avichi Meiyappa Chettiar and distributed by AVM Productions. The film music was composed by R. Goverdhanam and lyrics were written by Kosaraju Raghavaiah.
 
{{Cite web
|url=  http://www.chimatamusic.com/searchlyr.php?st=Kosaraju%20Raghavaiah%20Chowdary
|title= All Songs of Kosaraju Raghavaiah Chowdary
|accessdate=23 December 2008
|work=  
|publisher= Chinmata Music
|date=  }} 
 {{Cite web
|url= http://www.chimatamusic.com/chandraMohan.php
|title= Chandra Mohans Best Songs - 70s
|accessdate=23 December 2008
|work=  
|publisher= Chinmata Music
|date=  }} 

== Plot ==

It is an old classical family drama. Sundaram (Ramakrishna) is a richest businessman and he had younger brother sekhar(chandra mohan) is a college going guy who loves his brother so much. Sundarams mother-in-law Parvatham (S.varalkshmi) who cares for his assets and shows her dominating character on Sekhar & Sundaram. She bets with Sekhar to separate him from his brother and puts some conditions that he should not tell his brother about the bet to prove his strength. Parvatham second son in law (Chalam) identifies that his mother in law has moved to Sundaram house. Soon second son in law comes to he house and place a bet with Parvatham that she only should reveal that he is the son in law of her. Since then both of them starts proving their capacity to win in the bet.The film revolved around who wins the bet and how it changes in Parvathamma beliefs.

== Cast ==
{| class="wikitable"
|-
! Actor !! Character
|- Chandra Mohan || Sekhar
|-
| S. Varalakshmi || Parvathamma
|-
| Chalam || Ranga
|-
| Rama Krishna || Sundaram
|-
| Vennirade Nirmala || Athi Kesavans Wife
|-
| Allu Ramalingaiah || Chidambaram Setty
|-
| Mukkamala || General Bussy
|- Raja Babu || Appula Appa Rao
|-
| Ramaprabha || Ammaji
|-
| Vijaya Nirmala ||
|}

== Soundtrack ==
# Vesukunta Chempalu Vesukunta (male)
# Vesukunta Chempalu Vesukunta (female)
# Bomma Borusa Pandem Veyyi Needo Naado Paicheyi
# Vallu Jhillan Tunnadi
# Sarile Pove Vagaladi Telugu film was composed by R. Goverdhanam and lyrics were written by Kosaraju Raghavaiah.
 
 

== Additional sources ==

 

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 


 