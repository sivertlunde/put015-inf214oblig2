Aventurera
{{Infobox film
| name           = Aventurera
| image          = Aventurera.jpg
| caption        = 
| director       = Alberto Gout
| producer       = Guillermo Calderón Pedro Arturo Calderón
| writer         = Alvaro Custodio Carlos Sampelayo Andrea Palma Tito Junco Ruben Rojo
| music          = Antonio Díaz Conde
| cinematography = Alex Phillips
| editing        = Alfredo Rosas Priego
| distributor    = Cinematográfica Calderón
| released       =  
| runtime        = 101 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}} Andrea Palma. Its considered a masterpiece of the Rumberas film.

==Plot==
The quiet life of the young Elena ( . Rosaura abused and deeply humiliated Elena, who ends up running away from her with the help of Lucio, only to have to flee the city when Lucio gets involved in an assault and ends up in prison. Elena decides to start a new life working as a showgirl in Guadalajara. There she meets Mario (Ruben Rojo), a handsome young man who falls for her. Elena accepts his marriage proposal, only to discover, through a bitter twist of fate, that Rosaura is the mother of Mario. Elena decides to continue with her plans as a way of torturing Rosaura and avenge all the evil that caused her. But Lucio escape from prison, complicating the Elena riot situation.

==Cast==
* Ninón Sevilla... Helena Tejero Andrea Palma... Rosaura
* Tito Junco... Lucio
* Ruben Rojo... Mario
* Miguel Inclán... Rengo
* Maruja Grifell... Elenas mother
* Pedro Vargas
* Ana María González

==Reviews==
When Alberto Gout directed Aventurera, the filmmaker already had a solid industrial experience. It is, in fact, in his fourteenth film, for which he was hired by the Calderón studios in order to make a vehicle for showcasing his exclusive actress Ninón Sevilla, who had worked for Calderon studios since Pecadora (1947). Aventurera has the perfect industrial film ingredients that bind to the  ), three impossible musical numbers (created by Ninón Sevilla), an emblematic story of innocence and perversion.
Ninón Sevilla turning crazy all the critics of Cahiers du cinéma, which wrote some of the most ardent pages that have been engaged of any Mexican actress in that journal. 

The film inspired the 1990s and 2000s success stage play produced by the actress Carmen Salinas. The stage production featured the Mexican stars Edith González, Itatí Cantoral, Maribel Guardia and many others.

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 