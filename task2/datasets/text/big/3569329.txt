Eddie (film)
{{Infobox film
| name           = Eddie
| image          = Eddie poster.jpg
| caption        = Theatrical release poster
| alt            = Woman wearing a suit and holding a basketball under her arm
| director       = Steve Rash
| producer       = David Permut Mark Burg
| story          = Steve Zacharias Jeff Buhai Jon Connolly David Loucka
| screenplay     = Jon Connoly David Loucka Eric Champnella Keith Mitchell Steve Zacharias Jeff Buhai
| starring       = Whoopi Goldberg  Frank Langella  Dennis Farina
| music          = Stanley Clarke
| cinematography = Victor J. Kemper
| editing        = Richard Halsey Island Pictures Buena Vista Pictures  (North America)  PolyGram Filmed Entertainment  (international) 
| released       = May 31, 1996 (U.S.)
| runtime        = 100 minutes USA
| awards         = 
| language       = English
| budget         = $30 million
| gross          = $31,387,164
}}
Eddie is a 1996 comedy film starring Whoopi Goldberg and Frank Langella. The film barely broke even at the box office, grossing $31,387,164 in the US. The film was directed by Steve Rash.

==Plot ==
The New York Knicks are also-rans in the National Basketball Association|NBA, their roster filled with players who either lack talent or are too distracted by off-the-court issues. Nonetheless, limousine driver and rabid fan Edwina "Eddie" Franklin (Whoopi Goldberg) attends every Knicks game in the nosebleed section of Madison Square Garden.
 heckled earlier.  Eddies popularity piques the interest of the new Knicks owner, "Wild Bill" Burgess (Frank Langella). After he forces Bailey to quit, Burgess names Eddie the new head coach.
 center Ivan Radmonovich focuses only on scoring, and his lack of defensive technique leads him to frequently commit unnecessary blocking fouls.  Superstar forward Stacey Pattons me-first approach causes him to force low percentage shots because he is unwilling to pass, and his poor attitude hurts the teams morale.  At a pivotal moment in practice, Eddie earns the teams respect by taking a hard foul from Patton in order to demonstrate proper defense to Ivan.  Eddie also begins to bench Patton in favor of veteran Nate Wilson, who was generally considered washed up because of chronic knee injuries.  Wilson experiences a late career resurgence, the Knicks begin to win, and New York embraces the team and Eddies colorful personality.

The teams winning streak eclipses their abysmal start, and as the season winds down, the Knicks are within reach of the playoffs.  In order to qualify, they will need to beat the Charlotte Hornets, now coached by John Bailey, in the last game of the season.
 Los Angeles Rams to St. Louis upon her assumption of the Rams ownership in 1995). A conflicted Eddie doesnt show to the game until right before tip-off.
 Larry Johnson proves difficult to contain, but strong play from Wilson keeps the score close.  Bailey responds by telling Johnson to foul Wilson hard in the hopes of knocking him out of the game.  Johnson makes an unsuccessful attempt right before halftime: Wilson is hurt, but he is able to hit both foul shots and continue playing.  In the fourth quarter Johnson blows out Wilsons knee, forcing him out of the game.  Stacey Patton enters the game and shows a new team-first attitude, particularly on the Knicks last offensive possession.  Eddie begins to diagram a play to get Patton the ball for a final shot, but assistant coach Carl Zimmer instead suggests that Bailey, remembering Pattons tendency to force low percentage shots, will swarm him with defenders.  The Knicks instead inbound to Patton, who passes to an open teammate after three defenders come to him.  The Knicks take a one point lead with seconds left in the game.

Now, with the Knicks on the cusp of winning, Eddie realizes that a victory will cause the team to be relocated to St. Louis.  She decides to get on the Gardens PA system and reveal the entire plan to the world.  The fans react with shock and anger, and a frustrated Burgess is forced to promise the crowd he wont sell the team or move them out of New York City.

When play resumes, the Hornets have one last chance to win.  Bailey tells his team to get the ball to Johnson and have him drive the lane, expecting Ivan to commit a blocking foul.  Instead, Ivan remains set in his defensive position and Johnson is called for Personal foul (basketball)|charging, which nullifies the basket, giving the Knicks the win and the playoff berth.

The film ends without explaining what happens to the Knicks in the playoffs that year.

==Cast==

* Whoopi Goldberg as Edwina Eddie Franklin

* Frank Langella as William Wild Bill Burgess

* Dennis Farina as Coach John Bailey

* Richard Jenkins as Carl Zimmer

* Lisa Ann Walter as Claudine

* John Benjamin Hickey as Joe Nader

* John Salley as Nate Wilson
 Mark Jackson as Darren (Preacher) Taylor

* Malik Sealy as Stacy Patton

* Dwayne Schintzius as Ivan Radovadovitch

* John DiMaggio as construction worker
 Brad Daugherty, Larry Johnson, Anthony Mason, John Starks appeared as streetballers.  Kurt Rambis appeared as the head coach of the Lakers. Chris Berman, Marv Albert and Walt Frazier also appeared in the movie as broadcasters.
 Fabio and David Letterman also appeared in the movie.  Gene Anthony Ray (who played Leroy in both Fame the series and the 1980 film) also features as associate choreographer, one of his last credits before his death in 2003, aged 41.

During the production of the film Goldberg and Langella started dating, their relationship lasted until 2001.

== Reception ==
The film received negative reviews from critics. Rotten Tomatoes gives the film a score of 15% based on reviews from 33 critics.  Roger Ebert gave the film one and a half stars.  Razzie Award as Worst Actress for her performance.

==Soundtrack==
 
A soundtrack containing hip hop and R&B music was released on May 21, 1996 by Polygram Records. It peaked at 119 on the Billboard 200|Billboard 200 and 44 on the Top R&B/Hip-Hop Albums.

==Notes==
 
1. http://www.mrcranky.com/movies/eddie.html . See also  

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 