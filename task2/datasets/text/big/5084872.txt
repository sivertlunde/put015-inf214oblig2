The Anderson Tapes
{{Infobox film
| name           = The Anderson Tapes
| image          = The Anderson Tapes film poster.jpg
| image_size     = 225px
| caption        = original movie poster
| director       = Sidney Lumet 
| producer       = Robert M. Weitman
| screenplay     = Frank Pierson
| based on       =  
| starring       = Sean Connery
| music          = Quincy Jones
| cinematography = Arthur J. Ornitz
| editing        = Joanne Burke
| studio         = Robert M. Weitman Productions
| distributor    = Columbia Pictures
| released       =   
| runtime        = 95 minutes
| country        = United States English
| budget         = 
}}
 Alan King. novel of the same name by Lawrence Sanders. The film is scored by Quincy Jones and marks the feature film debut of Christopher Walken.

Revolving around a bold robbery, the film was prescient in focusing on the pervasiveness of electronic    This theme would become a movie staple following the Watergate scandal a few years later, for example, the 1974 film The Conversation.

== Plot == 1 East 91st Street) in New York City and Anderson, almost instantly, decides to burgle the entire building in a single sweep&nbsp;— filling a furniture van with the proceeds. He gains financing from a nostalgic Mafia boss and gathers his four-man crew. Also included is an old ex-con drunk, "Pop", whom Anderson met in jail, and who is to play concierge while the real one is bound and gagged in the cellar.

Less welcome is a man the Mafia foists onto Anderson&nbsp;— the thuggish "Socks". Socks is a psychopath who has become a liability to the mob and, as part of the deal, Anderson must kill him in the course of the robbery. Anderson is not keen on this, since the operation is complicated enough, but is forced to go along.
 BNDD (a precursor to the Drug Enforcement Administration|DEA), who are checking over a released drug dealer; the FBI, investigating Black activists and the interstate smuggling of antiques; and the IRS, which is after the mob boss who is financing the operation. Yet, because the various federal, state and city agencies performing the surveillance are all after different goals, none of them is able to "connect the dots" and anticipate the robbery.
 Mayflower moving and storage crew, the crooks cut telephone and alarm wires and move up through the building, gathering the residents as they go and robbing each apartment.

(The scenes of the residents being seized, and in some cases assaulted, are shown in contrast to them giving statements to the police after the robbery, which appears to indicate that it succeeded.)
 paraplegic and asthmatic who is left behind in his air-conditioned room. Using his amateur radio equipment, he calls up other radio amateurs, based in Hawaii, Portland, Maine and Wichita Falls (said to be in Kansas), who contact the police. The alarm is thus raised, after some problems as to which side (callers or emergency services) should take the phone bill.

As the oblivious criminals work, the police array enormous forces outside to prevent their escape and send a team in via a neighboring rooftop.

In the shootout that follows, Anderson kills Socks, but is himself shot by the police. The other robbers are killed, injured or captured, but none gets away with it. Pop gives himself up after letting the police believe that he is the real concierge for a while. Having never adapted to life on the outside, he looks forward to going back to prison. 

In the course of searching the building, the police discover some audio listening equipment left behind by the private detective who was hired to check up on Ingrid and track it to find Anderson in critical condition after having tried to escape. To avoid embarrassment over the failing to discover the robbery despite having Anderson on tape in several surveillance operations, and since many of the recordings were illegal, the agencies order the tapes to be erased.

==Cast==
  
*Sean Connery as John "Duke" Anderson
*Dyan Cannon as Ingrid Everleigh
*Martin Balsam as Tommy Haskins
*Ralph Meeker as Captain Delaney Alan King as Pat Angelo
*Christopher Walken as the kid
*Val Avery as "Socks" Parelli
*Dick Anthony Williams as Spencer
*Garrett Morris as Everson, police sergeant
*Stan Gottlieb as William "Pop" Myer
 
*Paul Benjamin as Jimmy Anthony Holland as psychologist
*Richard B. Shull as Werner
*Conrad Bain as Dr. Rubicoff Margaret Hamilton as Miss Kaler 
*Judith Lowry as Mrs. Hathaway
*Max Showalter as Bingham
*Janet Ward as Mrs. Bingham
*Scott Jacoby as Jerry Bingham, a young paraplegic ham radio operator
 

Cast notes
*This was the first major motion picture for Christopher Walken, as well as the last on-screen film appearance by Margaret Hamilton.  Murder on The Hill, Family Business. 12 Angry Men.
*Two characters from the novel on which the film was based were merged for the film:  "Ingrid Macht" and "Agnes Everleigh" became "Ingrid Everleigh". 
*Sean Connerys performance as the likeable criminal Duke Anderson was instrumental in his breakout from being typecast as James Bond. It also restored him to the ranks of top male actors in the United States. 

== Production == Convent of Rikers Island Hi Brown Studio  and ABC-Pathé Studio, both in New York City.  The production was on a tight budget, and filming was completed in the short period of six weeks, from mid-August to October 16, 1970.   The film was the first for producer Robert M. Weitman as an independent producer. TCM   

Columbia Pictures was not happy with the planned ending of the film, in which Connery escaped to be pursued by police helicopters, fearing that it would hurt sales to television, which generally required that bad deeds not go unpunished. 

==See also==
* List of films featuring surveillance

==References==
Notes
 

== External links ==
*  
*  
*   at Allmovie

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 