Raithu Bidda
{{Infobox film
| name           = Raithu Bidda
| image          = Raitu Bidda 1939.JPG
| image_size     =
| caption        =
| director       = Gudavalli Ramabrahmam
| producer       = Challapalli Maharaja
| writer         = Tripuraneni Gopichand,  Malladi Viswanatha Kaviraju,  Gudavalli Ramabrahmam,  Tapi Dharma Rao
| narrator       = Vangara
| music          = Bhimavarapu Narasimha Rao
| cinematography = Sailen Bose
| editing        =
| studio         = Saradhi Studios
| distributor    =
| released       = 1939
| runtime        =
| country        = India Telugu
| budget         =
}}

Raithu Bidda (  social problem film directed by Gudavalli Ramabrahmam.
It is a social reformist film during the period of British India, at the time of battle against Zamindari system. The film was banned by the British Administration in the region.  Ironically it was produced by one of the Zamindars of the time Challapalli Maharaja. The film had a public re-release in 1948.  

==Plot==
The film is a seminal reformist melodrama critiquing the Zamindari system from the viewpoint of the Kisan Sabha agitations in Andhra Pradesh. Small-time landowner Narsi Reddy (Raghawan) borrows money from a shavukar who represents the major zamindar (Sitapathy) of the village. When Narsi Reddy votes for a peasant candidate (Kosaraju) rather than for the political party supported by the landlord, his son is attacked and he is publicly humiliated.

==The Cast==
{| class="wikitable"
|-
! Actor !! Character
|-
| Bellary Raghava || Narisi Reddy
|-
| Gidugu Venkata Seethapathi Rao || The Zamindar
|-
| Bhimavarapu Narasimha Rao || Kaasaa Subbanna
|-
| P. Suri Babu || Ramajogi
|-
| Tanguturi Suryakumari || Seetha
|-
| Nellori Nagaraja Rao || Tahsildar
|-
| Kosaraju Raghavaiah || Raami Reddy
|-
| S. Varalakshmi || Beggar Girl
|-
| M. C. Raghavan || The Money Lender
|-
| Vedantam Raghavaiah || Dancer in the Dasavatharam song
|-
| Padmavati Devi
|-
| Sundaramma
|-
| Vangara Venkata Subbaiah
|-
| Cherukupalli Ellapragada Nehru
|-
| Malladi Viswanatha Kaviraju
|}

==Soundtrack==
# Nidra Melukora Tammuda Gaadha Nidra Melukora Tammuda - (Snger: P. Suri Babu)
# Mangalamamma Maa Poojalu Gaikonumamma - Group song
# Raitu Paina Anuragamu Choopani - (Singer: P. Suribabu)
# Vayinchuma Murali Vayinchu Krishna
# Kanna Biddakai Kalavara Paduchunu Kanneeru Karchunu - (Singer: P. Suribabu)
# Ravoyi Vanamali Birabira Ravoyi - (Singer: Tanguturi Suryakumari)
# Sai Sai Idena Bharathi Nee Pere - (Burrakatha by P. Suri Babu group)
# Sukshetramulu Dayasoonulai Peedinchu (Poem by P. Suri Babu)
# Raithuke Otivvavalenanna Nee Kashta Sukhamula - (Singers: P. Suri Babu and others)
# Dasavatraramulu - (Veedhi Natakam)

==References==
 

==External links==
*  

 
 
 
 
 