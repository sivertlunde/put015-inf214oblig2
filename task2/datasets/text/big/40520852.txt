Baavra Mann
{{Infobox film
| name           = Baavra Mann
| image          = Baavra Mann- Poster.jpg
| director       = Jaideep Varma
| producer       = VS Kundu
| cinematography = Paramvir Singh   Pankaj Rishi Kumar
| editing        = Dintu George   Dhruv Sehgal
| music          = Vivek Sachidanand
| studio         = Films Division

| released       =  
| runtime        = 127 minutes
| country        = India English
}}
Baavra Mann is a documentary film on the Indian filmmaker Sudhir Mishra directed by Jaideep Varma.   Though ostensibly a biographical film, it is equally a portrait of the decline of the Indian creative and intellectual scene as touched by Sudhir Mishra’s life, especially with regard to Sagar University, the theatre scene in Delhi and the film culture of Mumbai.

The film was begun and completed as a part-time film project through 2010 to 2013. The film’s production house was called Saturday Films because the majority of the work used to happen on Saturdays.

In 2013 the film was taken over by Films Division, India, who is now the official producers.    

The film was completed in 2013 and travelled to festivals in New York Indian Film Festival and DC South Asian Festival – it won the “Best Documentary” award at the latter. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 