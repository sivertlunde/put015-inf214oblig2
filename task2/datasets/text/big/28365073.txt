To Be a Lady
 
 
{{Infobox film
| name           = To Be a Lady
| image          = Tobealady.jpg
| image_size     =
| caption        = Lobby card George King
| producer       = George King
| writer         = Violet E. Powell C. H. Nicholson
| starring       = Chili Bouchier Bruce Lester
| music          =
| cinematography =
| editing        = Elmo Williams
| distributor    = Paramount British Pictures
| released       = 1934
| runtime        = 68 minutes
| country        = United Kingdom
| language       = English
}}
 George King, and starring Chili Bouchier (credited here as Dorothy Bouchier) and Bruce Lester. The film is the first screen editing credit of American film editor Elmo Williams.

==Plot==
Diana Whitcombe (Bouchier) works at her aunts country inn, but dreams of escaping to London and making her way in society. When chance provides her with the necessary funds, she makes her way to the big city and takes up employment in a hairdressing salon where she befriends French fellow assistant Annette (Ena Moon), and moves into the same hostel in which Annette is living.
 Ritz Hotel. Diana is worried that has nothing suitable to wear to such a rarefied establishment, but is delighted when Annette produces a beautiful dress which she offers to loan to her. Unknown to Diana however, the dress has been stolen by a maid friend of Annettes from her wealthy employer, and passed to Annette for safe-keeping before it is sold to a dealer.

Diana and Jerry meet for their Ritz rendezvous. Unfortunately, also present is the Countess Delavell (Vera Bogetti) lunching with her theatrical friend Dudley Chalfont (Charles Cullum), and it is the Countess stolen dress which Diana is wearing. At the end of the meeting Jerry, explaining that he has to leave to fulfil engagements in Scotland, proposes to Diana and she accepts. Meanwhile the Countess maid, aware that she is already under suspicion, steals some valuable jewellery, alerts Annette and the pair take off for France.

The Countess, believing Annette to be implicated in the thefts, visits the salon, identifies Diana as the girl who was wearing her dress, and Diana is arrested for receiving stolen property. She is found guilty and imprisoned for a month. She writes to Jerry at the address he has given her, but receives no acknowledgement. Jerry has in fact been seriously injured in a road accident en route to Scotland and is hospitalised for a lengthy period, but unaware of this, Diana believes he has abandoned her. On her release from prison, she decides to seek stage work and runs into Dudley. Dudley believes in her innocence and that she has been wronged, and offers her accommodation in his flat. He soon falls in love with her and asks her to marry him.

Jerry is finally released from hospital and returns to London to look for Diana. Finding her living in another mans flat, he confronts her over her fickleness and in anger at his lack of faith in her, she sends him away. She realises that her feelings are still for Jerry and it would be unfair of her to marry Dudley, so in despair she leaves London and returns to her home village. Aware of what the situation must be, the kind-hearted Dudley travels to the village with Jerry, where he engineers a reconciliation between the two.

==Cast==
* Chili Bouchier as Diana Whitcombe
* Bruce Lester as Jerry Dean
* Vera Bogetti as Countess Delavell
* Charles Cullum as Dudley Chalfont
* Ena Moon as Annette
* Pat Ronald as Justine
* Florence Vie as Mrs. Jubb
* Tony de Lungo as Manager

==Production==
To Be a Lady was mounted as a star vehicle for Bouchier, who at the time was one of Britains biggest home-grown female screen stars, and one of the few who had been able to make the transition from silent to sound films without any loss in popularity. Indications are that the film was marketed as a prestige production, with advance publicity emphasising aspects such as "lavish mounting", "expert direction" and "faultless recording and camera work". It was reported that it had taken an entire days filming for King to be satisfied with the kitten rescue sequence, which lasted a bare 60 seconds in the finished film.

==Reception==
Surviving contemporary critical reaction suggests that the film was less well-received that had been hoped.  The Picturegoer spoke of "moderate" acting, "unconvincing" plot and said "the wholly obvious theme is indifferently directed".  Kine Weekly was similarly unimpressed with the storyline, but somewhat more enthusiastic about the standards of acting and direction.

==Preservation status== 75 Most Wanted" list of missing British feature films.  It is the only sound film of Bouchiers not known to survive.

==References==
 

==External links==
*  , with extensive notes
*  
*  

 

 
 
 
 
 
 
 
 
 
 