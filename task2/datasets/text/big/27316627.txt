A Horse with No Name (film)
{{multiple issues|
 
 
}}
{{Infobox film
| name           = A Horse with No Name
| image          =
| caption        = 
| director       = Matthew and Barnaby OConnor
| starring       = Alex Price Molly Ryman Steve Malone Dave Fatta Jennifer Wilson McGuire Jennie Russo Sam Taylor Mark Dubin Darren Morrison Ron Bowdery L*A*W Simon Bell Olav Larson Ira Marlowe
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         = United States dollar|$10,000
}}
A Horse with No Name is an independently produced feature film by Matthew and Barnaby OConnor. Its two key distinguishing factors are the budget, which was $10,000 (considered very small by Hollywood standards)  and the fact the film was written as it was made - something none of the actors in the film knew was happening. As far as the cast were aware the script had already been written and they would receive dialogue on the day to keep it fresh. This whole process was documented in the feature length behind the scenes documentary 13 States which was described by one review as behind the scenes carnage the likes of which you have never seen before. http://dof413productions.co.uk/2010/06/a-horse-with-no-name 

==Plot==
In the opening scenes of the film, a down and out DJ, Vince Vinyl (Alex Price) meets Sophie (Molly Ryman), a high society American girl, in a New York cafe. The two of them unexpectedly hit it off and after deciding to spend the day together develop a tentative friendship. These New York scenes are inter cut with scenes of Vince traveling across the U.S. at a much later date. His travel companion is Randy the trucker (Steve Malone), who he has been forced to hitch a ride with against his will. As the film progresses, it shows how the friendship between Vince and Sophie develops into something deeper before Sophie is forced to return to her home in Los Angeles. It is this that inspires Vinces epic trans-American journey.

==Production==
As the behind the scenes documentary 13 States shows, production of the film was plagued with difficulties, with several instances where the filmmakers appear close to collapsing from exhaustion and stress. This was mainly due to the fact there was no script and the filmmakers were trying to keep this a secret from the other people involved in the production. Many of the actresses they approached initially were wary of becoming involved, perhaps because they sensed that all may not be what it appeared. Aside from this, the production was very underfunded, given the notoriously expensive costs of living and working in New York City.  In the end, all internal scenes were actually shot in Syracuse, New York|Syracuse, some 300 miles from the city (but still part of New York State) as costs of hiring locations within the city proved prohibitively expensive.

==Soundtrack== Sam Taylor, who was inducted into the Arizona Blues Hall Of Fame in 1997 is one example. Others include Ronald I. Becker (1985–present), also known as "KC Beck", a US Soldier turned country singer who features Thats Just Me as the title track for the documentary. Perhaps in keeping with the spirit of the production, all musicians contributed tracks on a deferred payment basis, where the musicians are not paid until the film has been sold and recouped its costs.

==Controversy==
Advanced previews of the film and the documentary were privately screened at the Cannes Film Festival in 2008 to a mixed reception. Some considered it a modest triumph of independent film making. The Business of Film Daily praised the style and direction of the film and even compared the directors to a young Sam Raimi.   Acclaim for the film was not universal however. Lloyd Simandl wrote "To some professionals it may actually be offensive as it prides itself on no script... That may be funny to some but to most in this business it is not." He went on to describe the two directors as "a bunch of goof offs laughing   the fact that they have no script, no money ... and likely no talent."

Not everyone shared this view however. The making of documentary 13 States was described by DOF 4:13 Productions as, "The definitive American Road Trip, a script that wasn’t and behind the scenes carnage the likes of which you have never seen before", most notably for how it captured the whole experience. 

<!-- ==Cast==

{| class="wikitable"
|-
! Actor
! Role
|- Alex Price || Vince Vinyl
|-
| Molly Ryman || Sophie
|-
| Steve Malone || Randy
|-
| Dave Fatta || Larry
|-
| Tim Scanlon || Simon
|-
| Jennie Russo || Vanessa
|-
|}
-->

==References==
 
 

==External links==
*  
*  

 
 
 
 