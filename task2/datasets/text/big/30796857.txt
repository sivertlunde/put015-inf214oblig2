Mod Fuck Explosion
{{Infobox film
| name           = Mod Fuck Explosion
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Jon Moritsugu
| producer       = 
| writer         = Jon Moritsugu
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 76 minutes
| country        =  English
| budget         = 
| gross          = 
}}
Mod Fuck Explosion is a film by Jon Moritsugu about a young girl named London who is trying to find meaning in the world, or a leather jacket of her very own. Unaccepted by the Mods or the bikers, she tries to find her own path through life. Meanwhile the Mods and the bikers have a vendetta against each other that is sure to erupt in a smorgasbord of violence. The film was written by Moritsugu and stars his wife Amy Davis as the angst ridden London.  

== Awards and Festivals ==
Mod Fuck Explosion won "Best Feature" at the New York Underground Film Festival in 1995, as well as "Best Feature" in the 1996 Honolulu Underground Film Festival and "2nd Place Feature" in the Freakzone Film Festival in France. Mod Fuck Explosion also was accepted to these following festivals: Berlin Film Festival; Cannes Film Festival; Mannheim-Heidelberg Film Festival, Germany; Vancouver Film Festival; Stockholm Film Festival; Singapore International Film Festival; Chicago Underground Film Festival; The Fringe Fest, Australia; Copenhagen Film Festival; Asian American International Film Festival, NYC; Goteberg Film Festival, Sweden; USA Film Festival, Dallas; St. Louis Film Festival; San Francisco Asian American Film Festival; Institute of Contemporary Art, London.  

== Credits ==
*Written and Directed by Jon Moritsugu  

=== Cast ===
* Amy Davis- London
* Desi del Valle- M16
* Bonnie Steiger- Mother
* Jacques Boyreau- Madball (Mod)
* Victor Fischbarg- X-Ray Spex (Mod) (as Victor of Aquitaine)
* Alyssa Wendt- Cake (Mod)
* Bonnie Dickenson- Cherry (Mod)
* Lane McClain- Shame (Mod)
* Abigail Hamilton- Columbine (Mod)
* Deena Davenport- Babette (Mod)
* Sarah Janene Pullen- Snap (Mod)
* Jonathan Scott Fellman- Satellite (Mod)
* Patrick Bavasi- Tack (Mod)
* Jon Moritsugu- Kazumi (Biker)
* Issa Bowser- Deathray (Biker)
* James Duval- Smack (Biker)
* Christine Wada- Biker girl (Biker)
* Anthony Kwan- Razorblade (Biker)
* Nicholas Lyovin- Sledgehammer (Biker)
* Elisabeth Canning- Cleopatra (Biker)
* Lisa Guay- Nasty (Biker)
* Justin Bond- Amphetamine
* Leigh Crow- Candyman
* Timothy Innes- Nail Clipper Yuppie
* Heiko Arnold Adler- Warhead
* Nancy Allen- Mexico
* Jefferson Davis Parker III- Bar Creep
* Mark Beaver- Cock thief
* Dan Kandel- Sidekick
* Michelle Haunold- Records seller
* Kathleen Blihar- Pale at the record shop / bar stool
* Jude Brown- Pale at the record shop
* Michael Clare- Pale at the record shop
* Andrew Forward- Pale at the record shop
* Clifford Webb- Kustom Kar Jock Boy
* Vincent Haverty- Kustom Kar Jock Boy
* Peter Martinez- Kustom Kar Jock Boy
* Lady- Dame at bar
* Christine Shields- Step dancer
* Henry S. Rosenthal- Drummer
* Jon Jost- Drummer
* Antonia Kohl- Bar stool
* Jim Dwyer- Bar stool
* Thet Win- Bar stool
* James Pask- Bus driver

=== Crew ===
* Marcus Hu- executive producer
* Timothy Innes- associate producer
* Jon Moritsugu- producer
* Henry S. Rosenthal- producer
* Andrea Sperling- co-producer
* American Soul Spiders- Original Music	 	
* Dixieland- Original Music	 	
* Tengoku Karyo- Original Music	 	
* Unrest- Original Music	 	
* Todd Verow- Cinematographer	 	
* Jon Moritsugu- Film Editing	 	
* Todd Verow- Production Design	 	
* Jennifer M. Gentile- Art Direction	 	
* Jason Rail- hair stylist
* Jason Rail- makeup artist
* Michelle De Lorimer- sound
* Alenka Pavlin- sound
* Deidre Schletter- sound
* Fred Brandon Chu- gaffer
* Fred Brandon Chu- key grip
* Patrick Taylor- first assistant camera
* Marianne Dissard- special thanks
* Clay Walker- special thanks

== References ==
 

== External links ==
* 

 
 
 
 