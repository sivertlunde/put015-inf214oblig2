Cabin Fever 2: Spring Fever
{{Infobox film
| name           = Cabin Fever 2: Spring Fever
| image          = Cabin Fever 2.jpg
| caption        = Theatrical release poster
| director       = Ti West
| producer       = Lauren Moews Vilchik Patrick Durham Jonathan Sachar
| screenplay     = Joshua Malkin
| story          = Randy Pearlstein Ti West Michael Bowen Larry Fessenden Mark Borchardt Judah Friedlander Giuseppe Andrews
| music          = Ryan Shore
| cinematography = Eliot Rockett
| editing        = Janice Hampton Lionsgate
| released       =  
| runtime        = 86 min.
| country        = United States
| language       = English
| budget         =
}} Cabin Fever.

==Plot== Deputy Winston Olsen (Giuseppe Andrews), the local policeman from the previous film. Winston assures the shocked bus driver that he had hit a moose.

The creek Paul was lying in was connected to a bottled-water company and the infected water was distributed to the local high school.

John (Noah Segan), a senior at the high school, is deciding whether to go to prom with his long-time crush Cassie (Alexi Wasser) or stay home. His friend Alex (Rusty Kelley) is against going until he hooks up with a girl named Liz (Regan Deal). She then says if she can get off work that night, she will meet him there.

John asks Cassie to go to prom but she refuses. Meanwhile, Winston is at a restaurant where a worker from the bottled-water company dies from the infection. He then realizes the creek got heavily contaminated and goes to the water plant to tell the officials that the water is contaminated. The worker he informs is quickly killed by a group of CCD (Contamination Control Division) soldiers in NBC suits. Winston leaves before they can get to him.
 Michael Bowen) then kicks John out. Cassie follows him and John confesses his love to her. CCD then force John and Cassie back into the school. The CCD locks all of the main exits from the school and kill Principal Sinclair when he demands to know what is going on. The infection then begins to kill the students in the gym at an alarming rate. Sandy (Lindsey Axelsson), the most popular girl in school is also murdered. The gym is then gassed from the outside. Cassie, John, and Alex watch in horror as all the students are killed. Winston is picked up by his cousin Herman (Mark Borchardt), ready to leave town.

Alex discovers he is infected and that the disease is incurable necrotizing fasciitis. The only way to stop the infection is to amputate the infected limb. However, Alexs infection is already too severe and he stays behind. John begins to show signs of the infection too and lets Cassie amputate his hand to stop it.

Cassies boyfriend Marc then comes out of nowhere, hits Cassie with a hammer, and tries to kill John but Cassie kills him with a nail gun. The two leave the school only to be ambushed by the CCD. John stalls them, allowing Cassie to escape. She finds herself on the highway and stops Hermans van. Winston and Herman take Cassie with them. The camera shows her back where she is starting to show signs of infection. 

The end of the movie shows Alexs date Liz at her workplace. She is a stripper at Teazers. She spreads the infection to her customers, who in turn spread it further around the country and even to Mexico. She arrives home later that night, sick, and goes to bed, thinking "I shouldve fucking gone to prom." After the credits, Dane and Darryl are shown watching TV and Dane says "Prom blows."

== Cast ==
* Noah Segan - John 
*Rusty Kelley - Alex
* Alexi Wasser - Cassie 
* Giuseppe Andrews - Winston  
* Regan Deal - Liz Grillington
* Marc Senter - Marc Michael Bowen - Principal Sinclair
* Lindsey Axelsson - Sandy
* Angela Oberer - Ms. Hawker  
* Amanda Jelks - Frederica
* Judah Friedlander - Toby
* Alexander Isaiah Thomas - Dane
* Patrick Durham - Banker Lucas
* Mark Borchardt - Herman 
* Larry Fessenden - Bill 
* Lisa H. Sackerman - Sores Girl/Prom Goer 
* Rider Strong - Paul (cameo)

== Release == Cabin Fever.   After extensive re-editing and re-shooting by the producers, director Ti West requested to have his name removed from the film and replaced with the popular pseudonym Alan Smithee. Since he was not a member of the Directors Guild of America, his request was denied by the producers and he remains credited as the films director. West has since disowned the final product claiming that it is more a product of the producers and executives than that of his own. 

== Production == Cabin Fever  cast members to come back for the sequel.

== Soundtrack ==
The film score was composed by Ryan Shore. 

== Prequels ==
Producer Lauren Moews has expressed interest in producing a Cabin Fever 3.  The third movie,  , which is about the origin of the virus, and how it spreads among vacationers on a remote and seemingly deserted island, and the fourth film,  , will be about a family who travels to a Caribbean island and has to deal with the same virus.  Both are being shot back-to-back by the companies Indomina and Hypotenuse Pictures.
 Adam Marcus and Debra Sullivan. Patient Zero and Outbreak are set for release in 2014. 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 