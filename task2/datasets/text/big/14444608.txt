Rainbow Over Texas
 
  in the film]] 1946 in which Roy Rogers plays himself as a famous cowboy-singer returning to Texas.

The self-portrayal of Roy Rogers as a more glamorous version of himself in Rainbow Over Texas revealed the great lengths to which Hollywood film studios would go in promoting their own film stars and made patently clear the self-referential advertising employed by studio productions in order to garner greater box office sales.

Since that time, "rainbow over Texas" has become a colloquialism for anyone who self-aggrandizes their own life in mythic and fantastical terms.  For example, an individual who confabulates their previous experiences or resume out of either ignorance or self-importance is likened to a "rainbow over Texas". 

== References ==
 
*  
*  

 
 
 
 
 
 


 