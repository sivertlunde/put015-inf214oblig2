Chalo Paltai
{{Infobox film
| name           = Chalo Paltai
| image          = Chalo Paltai poster.jpg
| image_size     = 
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Haranath Chakraborty
| producer       = Mahendra Soni Shrikant Mohta
| writer         = 
| screenplay     = N.K. Salil
| story          = 
| based on       =  
| narrator       = 
| starring       = Prosenjit Chatterjee Aryann Bhowmik Tathoi Deb
| music          = Anupam Roy
| cinematography = Soumik Haldar
| editing        = Rabiranjan Moitra
| studio         = Shree Venkatesh Films
| distributor    = 
| released       =  
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali film based on the lives and desires of the present generation children. Directed by Haranath Chakraborty and produced by Mahendra Soni and Shrikant Mohtaunder the banner of Shree Venkatesh Films, the film features actors Prosenjit Chatterjee and Aryann Bhowmik in the lead roles, playing the roles of father and son respectively.This film is based on the Marathi film "Shikshanachya Aaicha Gho!". Music of the film has been composed by Anupam Roy. The film released on March 3, 2011.   

== Plot ==
The film revolves around the relations between a father and a son. Subhomoy (Prosenjit Chatterjee), a widower, lives with his two children- Gourav (Aryann Bhowmik) and Munni (Tathoi Deb). Though poor in studies, Gourav loves cricket as a passion and is an extremely talented in it. Gourav wants to become a cricketer in future, only resulting in his father to oppose his dreams, whos wish is to see his son as something good (an engineer or a doctor). Gourav failed in the school pre-board exams and as a result, his principal did not allow him to sit for Madhyamik. Heartbroken by this, Subhomoy pleaded to the principal to give him 5 days time, during which Gourav will prosper in studies and produce the required results. But things remain the same. One day, a violent clash occurred between Gourav and Subhomoy and in fury, Subhamoy stroke Gourav, to which he fell and his head got a severe injury after getting hit from the bed. He became unconscious and was immediately taken to the hospital, where the doctors reported that was in coma. Subhomoy, who was deeply hurt with this incident, changed his thoughts and started protesting against the educational system of the state, which pressurized childrens minds and took away all their freedom needed to enjoy their childhood. He states his arguments on TV and soon becomes a local hero and the voice of thousands of common people. But the government authorities took no notice of him. Therefore, Subhomoy hatches a plan to directly talk to the Chief Minister. He ties a bomb (which later turns to be fake) with his body and manages to enter into the CMs office. He impresses the CM with his lecture on the disadvantages of the modern day education structure, to which the CM replied that he agreed that there is the need of a change and government will try to achieve it. The film ends as the doctors perform an operation on Gourav which turns out to be successful and he returns as a cricketer, hitting a century in his first cricket match.

== Cast ==
* Prosenjit Chatterjee as Subhomoy
* Aryann Bhowmik as Gourav
* Tathoi Deb as Munni
* Rajatava Dutta as Imran
* Mouli Ganguly as Malini
* Debranjan Nag as Subhomoys neighbour
* B. D. Mukherjee as Chief Minister
* Sambaran Banerjee as Himself (Cricket coach)
* Supriyo Dutta

== References ==
 
 
 

 