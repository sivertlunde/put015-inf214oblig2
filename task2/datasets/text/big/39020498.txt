Captain Applejack
{{Infobox film name = Captain Applejack image = Captain_Applejack_1931_Poster.jpg producer = director = Hobart Henley writer = Maude Fulton based on the play by Walter C. Hackett starring = John Halliday Mary Brian Kay Strozzi Arthur Edmund Carewe music = Leonid S. Leonardi Edmund Ross Erno Rapee Louis Silvers cinematography = Ira H. Morgan editing = Harold McLernon distributor = Warner Bros. released =   runtime = 63 minutes language = English
|country = United States
}}
 John Halliday, Mary Brian, Kay Strozzi and Arthur Edmund Carewe. The film was based on a 1921 play, of the same name, starring Wallace Eddinger and written by Walter C. Hackett. The play had previously been filmed as a silent film in 1923 under the title of Strangers of the Night.

==Synopsis==
John Halliday lives in an extravagant old mansion with his ward, played by Mary Brian, and his old Aunt, who is played by Louise Closser Hale. Brian is in love with Halliday but he doesnt realize it and treats her like a child. Halliday is bored with his sheltered and mundane live and craves excitement. He plans to sell the family mansion and use the money to travel around the world on a quest for adventure and excitement. Hale is shocked when she finds out about Hallidays plans while Brian supports him. Halliday, however, soon finds unexpected adventure, danger, mystery and excitement right in his own house. On a dark and stormy night, a mysterious woman knocks on the door, played by Kay Strozzi, seeking shelter from the storm and from a violent man, played by Arthur Edmund Carewe, who is apparently pursuing her. As a matter of fact, Strozzi and Carewe are a couple of thieves who are seeking a treasure which is hidden in Hallidays house. This treasure was hidden in the house by Hallidays pirate ancestor, who was known as Captain Applejack. Carewe shows up at the house but, eventually, Halliday manages to get both of them out of the house. Halliday then falls asleep and dreams of his pirate ancestor, of his ship, and of his conquest of a pretty woman, who is at first resistant, but in the end completely surrenders to him. When he awakes he finds that a parchment really exists in the house and that his visitors are really thieves and are seeking a hidden teasure. He races to find the treasures indicated on the parchment before the thieves can find it themselves. In the end he put the villains to rout, finds the treasure and discovers that he also loves Brian.

==Pre-Code Aspects==
The film is filled with Pre-Code Hollywood|pre-code material, especially during the pirate dream sequence. During that sequence, Captain Applejack brazenly forces a woman to submit to his sexual advances and actually grabs her breasts.

==Cast== John Halliday — Ambrose Applejohn  
*Mary Brian — Poppy Faire
*Kay Strozzi — Madame Anna Valeska
*Arthur Edmund Carewe — Ivan Borolsky, aka Jim 
*Alec B. Francis — Lush, the Butler 
*Louise Closser Hale — Aunt Agatha  
*Claud Allister — John Jason 
*Julia Swayne Gordon — Mrs. Kate Pengard
*Otto Hoffman — Horace Pengard  
*William B. Davidson — Bill Dennett

==Preservation==
The film survives intact and has been shown on television and cable.

==References==
 

==External links==
*  

 
 
 
 
 