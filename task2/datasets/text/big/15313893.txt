The Colonel (1917 film)
{{Infobox film
| name           = The Colonel
| image          = 
| caption        = 
| director       = Michael Curtiz
| producer       = 
| writer         = Richárd Falk Ferenc Herczeg
| starring       = Cläre Lotto Bela Lugosi Charles Puffy
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Hungary Silent
| budget         = 
| gross          = 
}}
The Colonel ( ) is a 1917 Hungarian film directed by Michael Curtiz. It featured Bela Lugosi in one of his earliest screen roles. It is based on a play by Ferenc Herczeg.

==Cast==
* Géza Boross
* Janka Csatay
* Sándor Góth
* Árpád id. Latabár
* Cläre Lotto
* Bela Lugosi
* Gerö Mály
* Charles Puffy (as Károly Huszár)
* Zoltán Szerémy
* László Z. Molnár

==See also==
* Michael Curtiz filmography
* Bela Lugosi filmography

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 