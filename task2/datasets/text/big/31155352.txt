Oral Koodi Kallanayi
{{Infobox film
| name = Oraal Koodi Kallanaayi
| image =
| caption =
| director = PA Thomas
| producer = PA Thomas
| writer = S. L. Puram Sadanandan
| screenplay = J. Sasikumar
| starring = Prem Nazir Sheela Adoor Bhasi Thikkurissi Sukumaran Nair
| music = KV Job
| cinematography = PB Maniyam
| editing = KD George
| studio = Thomas Pictures
| distributor = Thomas Pictures
| released =   
| country = India Malayalam
}}
 1964 Cinema Indian Malayalam Malayalam film, directed and produced by PA Thomas. The film stars Prem Nazir, Sheela, Adoor Bhasi and Thikkurissi Sukumaran Nair in lead roles. The film had musical score by KV Job.   

==Cast==
  
*Prem Nazir 
*Sheela 
*Adoor Bhasi 
*Thikkurissi Sukumaran Nair 
*Jeevan Prakash
*Pappukutty Bhagavathar 
*P. J. Antony 
*PA Thomas
*J. Sasikumar 
*T. S. Muthaiah 
*Prathapachandran 
*Alleppey Vincent  Ambika 
*Devaki 
*Gemini Chandra
*Gemini Ganesan 
*JAR Anand 
*Kushalakumari
*Manikyam
*Murali
*Panjabi
*Pankajavalli 
*S. P. Pillai 
*VS Achari
 

==Soundtrack==
The music was composed by KV Job and lyrics was written by Sreemoolanagaram Vijayan, Abhayadev and G Sankara Kurup. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Chaaykkadakkaaran beeraankaakkede || K. J. Yesudas, P. Leela || Sreemoolanagaram Vijayan || 
|- 
| 2 || Enthinum Meethe Muzhangatte || P. Leela || Abhayadev || 
|- 
| 3 || Kaarunyam Kolunna || P. Leela, Chorus || G Sankara Kurup || 
|- 
| 4 || Kannuneer Pozhikkoo || K. J. Yesudas || Abhayadev || 
|- 
| 5 || Karivala Vikkana || P. Leela || Abhayadev || 
|- 
| 6 || Kinaavilennum Vannene || K. J. Yesudas, P. Leela || Abhayadev || 
|- 
| 7 || Maanam Karuthaalum || K. J. Yesudas || Abhayadev || 
|- 
| 8 || Poovukal Thendum || P. Leela, Chorus || G Sankara Kurup || 
|- 
| 9 || Unnanam Uranganam || CO Anto || Abhayadev || 
|- 
| 10 || Veeshuka Nee Kodumkatte || Jayalakshmi || Abhayadev || 
|}

==References==
 

==External links==
*  

 
 
 


 