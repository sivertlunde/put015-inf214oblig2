Tarzan and the Brown Prince
{{Infobox film image       = Tarzhemgrotto.jpg name        = Tarzan and the Brown Prince caption     = Original film poster  writer      = Santiago Moncada Dario Sabatello based on    =   starring    = Steve Sipek Kitty Swan Robin Aristorenas Peter Lee Lawrence director    = Manuel Caño producer    = Dario Sabatello cinematography = Marcello Masciocchi music       = Sante Maria Romitelli editing     =  distributor = CITA Films Produzione D.S. (Dario Sabatello) released    =   runtime     =  language    = Various
}}

Tarzan and the Brown Prince is a 1972 Spanish/Italian co-production (filmmaking)|co-production Tarzan film with Steve Sipek and Kitty Swan repeating their roles from 1968s King of the Jungle. The film became a serialised Filipino graphic novel written in Tagalog and illustrated by Franc Reyes  who acted as an illustrator on the film.   The role of the Brown Prince was played by Filipino child actor Robin Aristorenas.

==Synopsis==
After a ruler dies, tradition dictates the new ruler must accomplish a series of harrowing challenges whilst competing with other aspirants.  One of them, a young prince (Robin Aristorenas) engages in the contest, but evildoers plan the young princes demise.  Tarzan protects him while ensuring he meets the tests.

==Production==
Steve Sipek also known as Steve Hawkes  gained recognition by playing Tarzan in two Spanish/Italian produced films. Tarzán en la gruta del oro/King of the Jungle/Tarzan in the Golden Grotto (1968) that was filmed in Suriname, South America, Florida, Africa, Spain and Italy where the producers ran out of money and had to begin filming again. Unlike his first "Tarzan" film, Sipek claimed the film company  paid the huge licensing fees from Edgar Rice Burroughs estate and were able to use the name "Tarzan"  for the character. 

Portions were filmed in Rainbow Springs Florida  where both Sipek and Swan were burned in a fire that got out of control.

==References==
 

==External links==
*  
*Original film trailer http://www.youtube.com/watch?v=F2IdPeuOyN4

 

 
 
 
 
 
 
 


 