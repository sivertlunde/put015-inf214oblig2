All for One (film)
{{Infobox film
| name           = All for One
| image          = AllForOne2011Poster.jpg
| caption        = Film poster
| director       = Rasmus Heide
| producer       = Ronnie Fridthjof
| writer         = Mick Øgendahl
| starring       = Jon Lange
| music          = 
| cinematography = Philippe Kress
| editing        = 
| distributor    = 
| released       =  
| runtime        = 81 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}

All for One ( ) is a 2011 Danish comedy film directed by Rasmus Heide.    

==Cast==
* Jon Lange as Martin
* Jonatan Spang as Nikolai
* Rasmus Bjerg as Timo
* Mick Øgendahl as Ralf
* Lisa Werlinder as Sofie
* Charlotte Fich as Line Gordon Kennedy as Toke
* Signe Anastassia Mannov as Helle (as Signe A. Mannov)
* Kurt Ravn as Arno
* Mille Dinesen as Niemeyers wife
* Rutger Hauer as Niemeyer
* Søren Malling as Revisor

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 