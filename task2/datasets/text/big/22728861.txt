Takekurabe (1955 film)
{{Infobox Film
| name           = Takekurabe
| image          = Takekurabe poster.jpg
| caption        = Japanese movie poster
| director       = Heinosuke Gosho
| producer       = Shintoho, Sadao Sugihara (producer)
| writer         = Toshio Yasumi (writer)
| starring       = 
| music          = Yasushi Akutagawa
| cinematography = Joji Ohara
| editing        = 
| distributor    =  1955 
| runtime        = 95 min.
| country        = Japan
| awards         =  Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
| 
}} Japanese film directed by Heinosuke Gosho.

It is a film adaptation of Higuchi Ichiyōs 1895-1896 novel Takekurabe.

Production design was made by Kazuo Kubo, sound recordist was Michio Okazaki and lighting technician was Susumu Irie. 

== Cast ==
* Hibari Misora
* Keiko Kishi
* Shogoro Ichikawa
* Atsuko Ichinomiya
* Chouko Iida
* Takashi Kitahara
* Kurayoshi Nakamura
* Kyū Sakamoto
* Takamaru Sasaki
* Isuzu Yamada - Isuzu Yamada won Blue Ribbon Awards for Best Supporting Actress in Takekurabe in 1956. 
* Mitsuko Yoshikawa

== References ==
 

== External links ==
* http://www.imdb.com/title/tt0048689/

 
 

 
 
 
 
 