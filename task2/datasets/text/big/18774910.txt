Pandorum
 

{{Infobox film
| name           = Pandorum
| image          = Pandorum-Poster.jpg
| caption        = Theatrical release poster
| director       = Christian Alvart
| producer       = Robert Kulzer Jeremy Bolt Paul W. S. Anderson
| screenplay     = Travis Milloy
| story          = Travis Milloy   Christian Alvart
| starring       = Dennis Quaid Ben Foster Cam Gigandet Antje Traue Cung Le Eddie Rouse
| music          = Michl Britsch
| cinematography = Wedigo von Schultzendorff
| editing        = Philipp Stahl Yvonne Valdez
| studio         = Constantin Film   Impact Pictures M6 (France)
| released       =  
| runtime        = 108 minutes 
| country        = Germany United Kingdom
| language       = English
| budget         = US$33 million
| gross          = $20,645,327   
}} survival adventure. The film was directed by Christian Alvart and produced by Robert Kulzer, Jeremy Bolt and Paul W.S. Anderson. Travis Milloy wrote the screenplay from a story by Milloy and Alvart. It stars Dennis Quaid and Ben Foster. Filming began in Berlin in August 2008. Pandorum was released on September 25, 2009 in the United States,  and on October 2, 2009 in the UK.
 deep space and triggered by emotional stress leading to severe paranoia, delirium, and nosebleeding. The film received mixed to negative reviews, but had gained a cult following over time.

==Plot== carry capacity of Earth, leading humanity to build an interstellar ark named Elysium. The mission is to send 60,000 people on a 123-year trip to establish a colony on an Earth-like planet named Tanis. The passengers and crew are in sleeper ship|hypersleep, with the crew on a hypersleep rotation to cover the entire journey.  Eight years into the mission the ship receives a transmission from Earth in multiple languages: "Youre all thats left of us. Good luck, God bless, and godspeed."
 general anesthesia for eight years. The surges are the result of an unstable reactor, which leaves them unable to reach the Bridge (nautical)|bridge. Bower ventures deep into the now seemingly abandoned ship to jump-start the reactor before it goes critical. He soon remembers how he was inspired by the flight since childhood while also having flashbacks of his wife whom he hopes to find in hyper-sleep on the ship. After having a claustrophobic panic attack, he begins to suffer from Pandorum, a severe psychological illness which once caused a space captain to send his entire crew to their deaths believing the flight was cursed.
 cook named Leland, has been living for a long time.

Gallo has now overcome his shock and claims he had to kill his rotation team in self-defense because they developed Pandorum after Earth mysteriously vanished. Gallo speaks with gruesome fascination of Pandorum, provoking Payton into developing its early symptoms. He attempts to silence Gallo, who then grows insolent and continues to provoke Payton. At the same time, Leland invites Bowers team to dinner, and shows them mural drawings depicting the true story of Gallos past and tells them what he heard from other passengers.  Gallo developed Pandorum when the ship received news of Earths disappearance, as did any humans that he brought out of hypersleep.  Gallo then manipulated the other psychotics into exiling themselves within the massive ship to play a nasty game which involved fighting, capturing, torturing and eating each other. Eventually Gallo went back into hypersleep, leaving the descendants of the psychotics to evolve over the course of successive generations into the troglofauna creatures who continue the game Gallo started with their ancestors. Their mutation is due to an enzyme given to the humans members in hypersleep that was supposed to both help their bodies adjust to the conditions on Tanis and speed up evolutionary ecological selection. Instead, they ending up adapting to the completely artificial conditions of the ships environment. As the story draws to a close, Bowers team falls unconscious

They wake to find themselves hanging from chains by their feet. Leland admits that he intends to eat them, but relents when Bower informs him that the power surges indicate that reactor failure will occur in less than an hour. Leland then joins the group. Bower regains some of his memory when his party reaches the passengers hypersleep chambers. He recalls that his wife had left him while still on Earth, so he enlisted to Elysiums crew as he had nothing else left. As Bower remembers, he grieves to the point of giving up on saving the ship. Nadia helps him remain optimistic and motivated, saying that his wife actually saved his life by leaving him, as his presence on the ship will help humanity survive. When Bowers group find the reactor, they also find that it is the lair for the large mutant community. Bower fails to make a stealthy approach, and Manh distracts the roused mutants so Bower can restart the reactor, which destroys most of the mutants. Leland flees, and Manh is cornered by their leader, which leads to a one-sided battle in the monsters favor. The creatures arrogance allows Mahn to kill it, though he is himself killed when he hesitates to slay a mutant child.

With the power restored, Payton can finally access the bridge, but Gallo assaults him to prevent him going. Payton sees Gallo injecting him with the sedative, then suddenly disappearing as he is left holding the syringe himself. It is thus revealed that "Payton" was hallucinating his younger self and is actually an older Gallo. Leland appears and Gallo incapacitates him with the sedative, then enters the bridge. Bower and Nadia soon arrive, with Bowers amnesia cleared away, now remembering that the man in Paytons uniform is not the Payton he knows from his rotation.

Gallo opens the shutters on the bridges windows, revealing that the ship is apparently lost somewhere in space where no stars are visible. The revelation is the final trip to cause Bowers mind to slip into Pandorum, allowing Gallo to manipulate him like he did the Pandorum-afflicted ancestors of the mutants. He tries to convince Bower that they must continue the wild primitive state within the ship to replace human civilization, since it led to the overpopulation on their planet.

Just as the ships log reveals that 923 years have passed since the mission started, Nadia observes bioluminescent ocean life through the windows, indicating that the stars are missing because the ship landed itself on Tanis and has been lying underwater for 800 years. Gallo attacks Bower and Nadia, but Bowers psychosis has him seeing mutants on the bridge.  In his delirium, Bower breaks a window, flooding the ship. Nadia manages to snap Bower to reality, and they get themselves into a hypersleep pod. The flood triggers a hull breach emergency which automatically ejects all active pods to the surface while Gallo, Leland, and the remaining mutants drown.

Bower and Nadia surface near a lush coastline, and witness the other pods ascending one by one. Thus begins Year One on the Earth-like Tanis, with 1,213 survivors from the original 60,000 humans.

== Cast ==
* Dennis Quaid as Lieutenant Payton
* Ben Foster as Corporal Bower
* Cam Gigandet as Corporal Gallo
* Antje Traue as Nadia
* Cung Le as Manh
* Eddie Rouse as Leland
* André Hennicke as Hunter Leader
* Norman Reedus as Shepard
* Wotan Wilke Möhring as Young Bowers Father
* Niels-Bruno Schmidt as Insane Officer Eden

== Production ==
The film began life as a preliminary script written by Travis Milloy in the late-1990s. The story was originally  set on a prison ship named Pandorum, transporting thousands of Earths deadliest prisoners to another planet; the cannibal hunters were the end result of the prisoners degeneration. The characters played by Antje Traue and Cung Le were inmates. Ben Fosters character was a non-prisoner who did not trust anyone.

Believing no studio would want to make the film, Milloy thought about making it as a low-budget film shot on video in an abandoned paper mill with unknown actors. However, it attracted the attention of filmmaker Paul W. S. Anderson and Jeremy Bolt, and they gave it to Impact Pictures, who green-lit it. The producers gave the script to director Christian Alvart who was struck by the similarities to his own screenplay titled No Where. His dramatic story was about four astronauts aboard a settlers ship who suffer from amnesia. Alvart decided that they should meld the two screenplays together, and the producers and Milloy agreed. With the ship now changed to a settlers ship, the use of the word "Pandorum" was changed from the name of the ship to a type of mental illness caused by sustained deep space travel. 

Pandorum was announced in May 2008 with Dennis Quaid and Ben Foster in lead roles. Christian Alvart was attached to direct the film, based on a script by Travis Milloy. The movie was financed by Constantin Film through a joint venture deal with subsidiary Impact Pictures.     The partnership helped fund the $40 million production. Constantin drew subsidies from Germanys Medienboard Berlin-Brandenburg (MBB) regional film fund, the   (FFA) and the   (DFFF). The German Federal Film Fund provided $6 million to the production, the funds second-largest 2008 payout after $7.5 million for Ninja Assassin.      Filming took place at Babelsberg Studios in Potsdam in August 2008.  

== Release, directors cut, and sequel  ==
 , Cung Le and Antje Traue talk about Pandorum at a panel discussion at WonderCon 2009.]]

Summit Entertainment handled foreign sales and presented Pandorum to buyers at the 2009 Cannes Film Festival due to a deal with Contender Films in the UK, so instead Metro-Goldwyn-Mayer Pictures took over and handled foreign sales to the film.   Overture Films distributed Pandorum in North America, Icon in the United Kingdom and Australia, Svensk in Scandinavia, and Movie Eye in Japan. The film was set up as a possible franchise, so that if it performed well, Impact Pictures could green-light one or more sequels. 

The DVD and Blu-ray Disc release occurred on January 19, 2010 in the United States  over Anchor Bay Entertainment. 

The director and producer commentaries on the DVD indicate that an unrated version of the movie exists but has not been released.

The film had gained a cult following, as in 2010 fans started a Facebook group – 500,000 to get Pandorum sequel – To help reassure the producers to make sure a sequel comes out. Director Christian Alvart later became a member of the group. 

== Reception ==
Pandorum mostly gained average/mixed or subpar reviews. Review aggregator Rotten Tomatoes reports the film holding 28% positive reviews out of 81, with the site rating it 4.2/10.    The sites consensus is that "While it might prove somewhat satisfying for devout sci-fi fans, Pandorum  s bloated, derivative plot ultimately leaves it drifting in space". 

At Metacritic, which judges on a 0–100 scale, the film holds a "generally  unfavorable" score of 28 based on 13 reviews.    Science fiction magazine SFX (magazine)|SFX was more positive, stating that "Pandorum is the finest interstellar horror in years", and awarding the film 4 stars out of 5.  Film Ireland also gave Pandorum a positive review, appreciating the films synergy of cinematic techniques, set design, and developed characters. 

The film grossed $20,645,327 worldwide, therefore failing to bring back its $33 million budget.  The film opened at #6 at the US box office with weekend receipts totaling $4,424,126. This is probably due to lack of advertisement as the facebook fanpage mentioned above states that most of its members only learned about the film through Netflix or late night television. The lack of promotion was likely due Overtue Films lacking the budget, as the studio went bankrupt a several months later. It is also possibly due to the fact it had to compete with Paranormal Activity and Surrogates.

== Soundtrack ==
{{Infobox album  
| Name        = Pandorum
| Type        = soundtrack
| Artist      = Michl Britsch
| Cover       = Pandorum OST.jpg
| Released    = September 25, 2009
| Recorded    = 2009 Electronic
| Length      = 71:06
| Label       = Königskinder Schallplatten GmbH
| Producer    = Michl Britsch
| Reviews     = 
}}

Track listing
# "All That Is Left of Us" (2:43)
# "Pandorum" (3:58)
# "Anti Riot" (4:17)
# "Shape" (2:03)
# "Hunting Party" (2:48)
# "Kulzer Complex" (4:40)
# "Tanis Probe Broadcast" (2:01)
# "Scars" (2:20)
# "Fucking Solidarity" (3:28)
# "Gallos Birth" (2:22)
# "Biolab Attack" (2:25)
# "Kanyrna" (3:22)
# "The Stars All Look Alike" (4:32)
# "Boom" (3:55)
# "Reactor" (4:08)
# "Skin on Skin" (3:21)
# "Fight Fight Fight" (2:56)
# "Bowers Trip" (7:51)
# "Discovery / End Credits" (7:55)

== See also ==
*Lovecraftian horror 
*Survivalism in fiction
*Malthusianism Psychological effects of spaceflight

== References ==
 

== External links ==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 