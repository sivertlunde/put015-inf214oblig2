China: The Panda Adventure
{{Infobox film
| name           = China: The Panda Adventure
| image          = China The Panda Adventure.jpg
| image_size     = 
| caption        =  Robert M. Young
| writer         = 
| narrator       = 
| starring       = Maria Bello
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = June 9, 2001
| runtime        = 48 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Robert M. Yu Xia.  
Set in 1936 China, Ruth Harkness has come to settle the affairs of her husband, Bill, who died while observing the rare and unstudied panda bear. His journal describes the panda as shy and docile, while great white hunter Dak Johnson describes them as ferocious beasts. This intrigues her, and she sets off to retrace Bills steps and save the pandas from Johnson. She encounters many obstacles, both natural and created by Johnson.
==Cast==
*Maria Bello as Ruth Harkness Yu Xia as Quentin Young
*Xander Berkeley as Dakar Johnston

==References==
 

==External links==
* 

 

 
 