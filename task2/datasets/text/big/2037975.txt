Harry and the Hendersons
 
{{Infobox film
| name           = Harry and the Hendersons
| image          = Harry and the hendersons.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = William Dear
| producer       = William Dear Richard Vane
| writer         = William Dear William E. Martin Ezra D. Rappaport
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = John Lithgow Melinda Dillon Don Ameche David Suchet Margaret Langrick Joshua Rudoy Lainie Kazan Kevin Peter Hall
| music          = Bruce Broughton
| cinematography = Allen Daviau
| editing        = Donn Cambern
| studio         = Amblin Entertainment
| distributor    = Universal Pictures
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = $16 million
| gross          = $49,998,613 
}} fantasy comedy Rick Baker cryptozoological creature Best Makeup, of the same name. 

In the United Kingdom the film was originally released as Bigfoot and the Hendersons, though the TV series retained the American title. The DVD and all current showings of the movie in the UK now refer to the movie by its original title.

The film earned mostly mixed reviews and was a modest success at the box office during its release.

==Plot==
George Henderson (John Lithgow) is returning home with his family from a camping trip when they hit something with the family station wagon. George investigates, and discovers to his horror and awe, that they had hit a Bigfoot|Sasquatch. Deciding to take the creature home, George does so, strapping it to the roof of the car. Meanwhile, a mysterious hunter has been tracking the creature and discovers the Hendersons license plate, which fell off when they hit the creature. At home, curiosity gets the best of George, and he goes out to the garage to examine the creature. Much to his shock, the creature was not dead and has since disappeared. He hears noises from his kitchen and sees the creature, it has knocked over the fridge in its attempt to find food. After waking the whole house, the family realizes that the creature is friendly and kind. George has a change of heart, at first he wished to make money from the creature, but now decides to take him back to the wild. Naming the creature "Harry", George tries to lure him into the station wagon, but Harry realizes that the Hendersons mean him harm and instead he disappears.

Saddened, the family resume their normal lives, but as the sightings of Harry become more frequent and the media fervor heightens, George decides he needs to find Harry in order to keep him safe. George visits the "North American Museum of Anthropology" to speak with Dr. Wallace Wrightwood an expert on Bigfoot, but is disheartened when he realizes its ramshackle state. Giving his number to the clerk (Don Ameche) inside the Museum, George resumes his search of Harry. The hunter from the woods is revealed to be Jacques LaFleur (David Suchet). Once a legendary hunter, he became obsessed with Bigfoot and has hunted for one ever since becoming a laughing stock. LaFleur tracks down the Hendersons, and begins to get closer to finding Harry. After a Harry sighting, George goes into the city to search for him. Meanwhile, the police are dealing with the sudden "Bigfoot Mania" by apprehending several local Bigfoot enthusiasts that are hunting Bigfoot in case the Bigfoot in question is someone dressed as Bigfoot. Following a car chase, George is able to save Harry from LaFleur, and LaFleur is subsequently arrested by the arriving police officers. When George brings Harry home, he and the Hendersons bury the hunting trophies and pay their respects to the dead animals that were converted into hunting trophies.
 The Addams Family. At the police station, LaFleur calls up someone stating that he has a lead on Bigfoot and tells him to secure his release by this day. George calls Dr. Wallace Wrightwood from the museum and asks to have dinner to speak about Bigfoot. At the Henderson house, George is met by the same museum clerk who is revealed to be Dr. Wrightwood himself, having also become a laughing stock. Dr. Wrightwood tries to tell George and the rest of the family to give up on Bigfoot as it has destroyed his life and will do so to theirs. His faith is rejuvenated when he meets Harry, and instantly he agrees to take him to safety, away from the city. By this time, LaFleur has been bailed of jail and heads to the Henderson house. George and Harry escape the house with Dr. Wrightwood and his old truck. LaFleur gives chase and eventually catches up with the Henderson family.

Fleeing to the mountains of Washington state, George tries to make Harry leave, going so far as to hit Harry. Confused and upset, Harry does not leave, allowing LaFleur to catch up to them. When LaFleur attacks the Hendersons dog, Harry attacks LaFleur until George intervenes. Through Georges faith and Harrys kindness, LaFleur changes his mind and decides that Harry deserves to live peacefully. As the family says goodbye, George thanks Harry for all he has done for the family and tells him to take care of himself to which Harry replies "OK" (his first spoken words). As Harry leaves, several other Sasquatches appear from their hiding places and also disappear in the wilderness with him much to the amazement of the Hendersons. When Dr. Wrightwood asks LaFleur what hes going to do next, LaFleur quotes "I dont know. Theres always Loch Ness." As the two of them laugh at that comment, the Hendersons keep waving goodbye to Harry.

During the credits, there is sketch animation of different scenes of the movie.

==Cast==
* John Lithgow as George Henderson
* Melinda Dillon as Nancy Henderson
* Margaret Langrick as Sarah Henderson
* Joshua Rudoy as Ernie Henderson
* Kevin Peter Hall as Harry (in-suit performer) Rick Baker as Harry (puppeteer)
** Tom Hester as Harry (puppeteer)
** Tim Lawrence as Harry (puppeteer) Fred Newman as Harry (voice)
* Lainie Kazan as Irene Moffat
* Don Ameche as Dr. Wallace Wrightwood
* David Suchet as Jacques LaFleur
* M. Emmet Walsh as George Henderson Sr.
* William Dear as Sighting Man

==Reception==
===Box office=== The Untouchables.  It went on to gross $29.7 million at the North American domestic box office and $20.2 million internationally for a total of $49.9 million worldwide.

===Critical response===
The response from critics was mixed. Rotten Tomatoes gives the film a score of 44% based on reviews from 18 critics. 

 
 
  

===Awards===
  
* Academy Awards    Best Makeup  (Won)

==Music==
Bruce Broughton composed the films original score, and co-wrote "Love Lives On" with Barry Mann (music), Cynthia Weil (lyrics) and Will Jennings (lyrics), performed by Joe Cocker over the end credits (in place of Broughtons planned end title cue) and later released as a single. MCA Records released a soundtrack album on record and cassette; in 2007 Intrada Records issued an expanded album, marking the musics premiere CD release.

===1987 MCA soundtrack album===
Side 1:
# Love Lives On - Joe Cocker    
# Main Title    
# Some Dumb Thing    
# Irene!    
# Harry In The House   
# Harry Takes Off  

Side 2:  
# Your Feets Too Big - Jimmy Walker; arr. Chris Boardman    
# Drawing Harry  
# Taking Harry Home    
# Foot Prints    
# Goodbyes  
# "Harry And The Hendersons"  

===2007 Intrada album===

The album begins with the film version of "Love Lives On," which has a flute solo rather than the guitar heard on the single and on the 1987 soundtrack album.

# Love Lives On - Joe Cocker   
# Main Title   
# Taking Harry Home   
# Harry in the House   
# Night Prowler   
# Some Dumb Thing   
# Irene!   
# Eye To Eye   
# Our Little Pet   
# Tracking Harry   
# Harry Takes Off   
# Big Freeway   
# Sasquatch   
# The Great Outdoors   
# Bigfoot Museum   
# Planning the Hunt   
# Drawing Harry   
# Night Pursuit   
# First Things First   
# Wrightwood Meets Harry   
# Bed Pals   
# Traffic Jam!   
# Footprints   
# Goodbyes   
# Harry and the Hendersons   

==Home media==
The film was released in January 2011 on DVD entitled Harry and The Hendersons Special Edition.  A single-disc Blu-ray of the film was released on March 4, 2014.

==TV spin-off== Harry and the Hendersons. Kevin Peter Hall reprised Harry until his death in 1991. Following Kevin Peter Halls death, Harry was performed by Dawan Scott in 1991-1992 and by Brian Steele in 1992-1993.

==References==
 

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 