Jim Bludso
 
{{Infobox film
| name           = Jim Bludso
| image          = Scenefrom Jim Bludso 1917 newspaper.jpg
| caption        = Scene from the film, published in a newspaper.
| director       = Tod Browning Wilfred Lucas
| producer       = Fine Arts Film Company
| writer         = Tod Browning John Hay I.N. Morris
| starring       = Wilfred Lucas Olga Grey
| cinematography = Alfred Gosden
| editing        =
| distributor    = Triangle Film Corporation
| released       =  
| runtime        = 5 reels
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}

Jim Bludso is a 1917 American   and Elias Savada suggest that Lucas name was added to the credit for contractual reasons, and that Browning directed Jim Bludso alone.    As Jim Bludso is presumed Lost film|lost,  it is uncertain what the original title card might have read in terms of directorial credit. The film was produced by the Fine Arts unit within the Triangle Film Corporation, the same studio that made the popular Douglas Fairbanks comedies for Triangle, for whom Browning had previously worked as a scenarist.   

==Cast==
* Wilfred Lucas - Jim Bludso
* Olga Grey - Gabrielle
* Georgie Stone - Little Breeches
* Charles Lee - Tom Taggart
* Winifred Westover - Kate Taggart
* Sam De Grasse - Ben Merrill
* James OShea - Banty Tim
* Monte Blue - Joe Bower
* Al Joy - Gambler
* Lillian Langdon
* Bert Woodruff

==Synopsis==
Engineer Jim Bludso and his sidekick, Banty Tim, return to Gilgal, Illinois after the end of the American Civil War. Upon arrival, they discover that Jims wife, Gabrielle, has left him for another man and abandoned their son. Kate Taggart, the daughter of a storekeeper in town, takes pity on Jim and they develop a fondness for one another. Gabrielle, now dumped, returns and Jim forgives her and resumes their married life. Meanwhile, a flood is coming, and Ben Merrill&mdash;constructor of Gilgals levee&mdash;knows the structure wont hold against the tide, so he willfully causes it to fail and plans to blame the resulting catastrophe on Jim and Banty Tim. Gabrielle is mortally wounded in the flood, and her dying words implicate Merrill and identify him as the man who wooed her away from her family. Jim is on board the boat Prairie Bell when this news reaches him, as is Merrill; they get into a fight, and Prairie Bell bursts into flames and explodes. Jim is rescued and returns to Gilgal to marry Kate.

==Adaptation==
Jim Bludso was a poem from the Pike County Ballads of John Hay, a familiar set piece in the repertoire of elocutionists, actors and other public speakers; the Kalem Company had already made a one-reeler out of the same property in 1912. For the film, Browning fashioned his script from both Jim Bludso and another poem, Little Breeches. Much of the films dramatic arc also came from a 1903 stage play adaptation by I.N. Morris. Hays original poem memorialized Jim Bludsos courage and selflessness in sacrificing his own life so that the passengers on his burning boat might survive. For the film, a happy ending was devised and an entirely different set of circumstances led to the demise of Prairie Bell, which Bludso is piloting in Hays poem.

==See also==
*List of lost films

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 