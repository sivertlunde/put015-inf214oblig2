Anjalika
{{Infobox film
| name           = අංජලිකා Anjalika
| image          =
| director       = Channa Perera
| producer       = Malith Palliyaguruge
| writer         =  
| starring       = Channa Perera Pooja Umashankar Anarkali Akarsha Jayanth Gunawardhane
| editing        = Ravindra Guruge
| released       = 28 June 2006
| runtime        = Sinhala
| music          = Rohana Weerasinghe
| awards         =
| budget         =
}}
 2006 Sinhala Sinhala romance Pooja and Anarkali in the leading roles while Rex Kodipilli, Narada Bakmeewewa, Maureen Charuni and Sanath Gunatilaka also play key supporting roles. Produced by Malith Palliyaguruge, the film had music scored by Rohana Weerasinghe. It released in June 2006 to good reviews and good box office collections.

== Plot ==
A young man (Thivankamal) returns home to Sri Lanka, after many years in England and he falls in love with a village girl named Anjalika. This puts Thivanka in a dilemma because Anjalika happens to be the daughter of a plantation caretaker employed by Thivankas father. To make matters worse Thivankas childhood friend Kavya is in love with him and wants to marry him. The inevitable clash of Rich and Poor, ends in the kidnapping and subsequent death of Anjalika. Thivanka still reeling from his grief decides to accompany a friend to Malaysia in order to clear his mind. While in Malaysia, Thivanka accidentally meets a girl named Uttara who looks identical to his lover Anjalika who he thinks is dead.  He follows her and finally comes to know that she is Anjalika and it was a plot by his father to make his son marry Kavya. Finally it is a happy ending and Thivanka and Anjalika gets united!

== Cast ==
 
*Channa Perera as Thivanka
*Pooja Umashankar as Anjalika/Uttara
*Anarkali Akarsha as Kavya
*Rex Kodipilli
*Narada Bakmeewewa
*Maureen Charuni
*Luxmun Mendis
*Sanath Gunatilaka
 

== Production ==

 

== Release ==
The film opened to good reviews with Chamitha Kuruppu of the Sunday Observer online was very complimentary towards Pooja:
"...All the credit goes to pretty Pooja Umashankar from India who portrays the title character Anjalika. The film will no doubt be a commercial hit, thanks to Poojas brilliant acting, superb dancing skills and of course her gorgeous looks. Young Poojas performance as mischievous Anjalika living a carefree life hanging around with children in a village, deserves all the praise. Anjalika will definitely be a commercial hit, thanks to lovely Pooja, breathtaking sceneries and Rohana Weerasinghes brilliant music. There is no doubt that Sri Lankan film lovers will welcome Pooja with warm hearts and adore her performance. In simple words, Pooja has stolen the limelight. Applause to you Channa for your sweet and worthy introduction to the Sinhala cinema!...".  Anjalika was a huge Box Office hit in Sri Lanka and Pooja Umashankar got many offers in Sinhala films after her debut in this Sri Lankan Film.
 
 

== Soundtrack ==

 

== References ==
 

== External links ==

 
 
 