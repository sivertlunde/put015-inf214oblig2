Bala (2002 film)
{{Infobox Film
| name           = Bala
| image          = Bala_DVD_cover.jpg
| image_size     = 
| caption        = International release DVD cover
| director       = Deepak
| producer       = Rajabalu Raju Magalingam
| writer         = Deepak
| narrator       = 
| starring       = Shaam Meera Jasmine Raghuvaran Thilakan Nagesh Santhoshi Karunas Mayilsamy Murali Sumithra Rajan P. Dev Monalisha
| music          = Yuvan Shankar Raja
| cinematography = Priyan
| editing        = V. T. Vijayan
| studio         = Goldmine Pictures
| distributor    = 
| released       = December 13, 2002
| runtime        = 
| country        =   Tamil
| preceded_by    = 
| followed_by    = 
}}
 Indian Cinema Tamil film starring Shaam and Meera Jasmine in lead roles and Raghuvaran, Thilakan, Nagesh, Rajan P. Dev, Karunas and Sumithra in supporting roles. Music by Yuvan Shankar Raja became popular upon release and was a major highlight of the film. The film was released on December 13, 2002 and is considered an average grosser.

==Plot==
Bala is the favourite hit man of gangster Pasupathi (Dev). When Bala is not zooming around in jeeps with a wild-looking gang, parading down lanes with the same gang faithfully following a step behind him, or knocking down one person or another, hes successfully wooing Arthi, the girl he has fallen for at first sight. Arthi is the daughter of Jeyamani, a rival gangster. Ailing don Paranthaman (Thilakan), mentor of the two rivals, seeing his protégés at each others throats, brings a compromise by suggesting that Arthi be married to Pasupathis wayward son. Bala naturally becomes a pariah in both camps, till its alls well that ends well.

==Cast==
* Shaam ...Bala
* Meera Jasmine ... Aarthi
* Karunas
* Thilakan ...Paranthaman
* Raghuvaran ...Jeyamani
* Santhoshi ...Poornima
* Nagesh
* Rajan P. Dev ...Pasupathi
* Mayilsamy
* Kalairani
* Sabitha Anand
* Murali
* Sumithra
* Kamalesh Pawan ...Jeyamanis henchman (cameo)

==Production==
Vidya Balan, the original choice for lead actress, was replaced by Meera Jasmine in the film. 

Bala is directed by debutant Deepak who had assisted editors Lenin-Vijayan, and director A.R. Gandhikrishna. Produced by Rajababu and Raju Mahalingam for Goldmine Pictures, Bala has music by Yuvan Shanker Raja, editing by V.T. Vijayan and cinematography by Priyan. The supporting cast has Thilakan, Vadivel, Rajan P. Dev, Murali of Malayalam films. Mayilsamy, Sumitra, Monish, Raghuvaran, Karunas, Nagesh and Meera Krishnan.

Some scenes were shot on a boat about fifteen kilometre from the harbour, in Chennai whereas at the Vauhini Studios, Chennai, a lavish set was erected where Shaam and Meera Jasmine danced to the beat of a song. 

==Soundtrack==
{{Infobox album |  
| Name = Bala
| Type = soundtrack
| Artist = Yuvan Shankar Raja
| Cover = Bala_soundtrack_CD_cover.jpg
| Released = October 20, 2002
| Recorded = 2002 Feature film soundtrack
| Length = 22:08
| Label = Five Star Audio
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album  = April Maadhathil (2002)
| This album  = Bala (2002)
| Next album  = Mounam Pesiyadhe (2002)
}}

The soundtrack, featuring 5 songs, was composed by Yuvan Shankar Raja and released on October 20, 2002.  Lyrics were penned by Arivumathi, Kabilan, Pa. Vijay and Pazhani Bharathi. The song "Bailamo Bailamo" was originally composed for the Srikanth (actor)|Srikanth-starrer April Maadhathil, but eventually used in this film. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration !! Lyrics!! Notes
|- 1 || Bailamo Bailamo || Yuvan Shankar Raja || 4:20 || Pa. Vijay ||
|- 2 || Poopoovai || Unni Menon, Ganga || 4:55 || Arivumathi ||
|- 3 || Theendi Theendi || Unnikrishnan, Sujatha Mohan || 4:10 || Pa. Vijay ||
|- 4 || Vaanathu Poochi || Karthik (singer)|Karthik, Mathangi || 4:30 || Kabilan ||
|- 5 || En Kannan || Shankar Mahadevan || 4:13 || Pazhani Bharathi ||
|}

==References==
 

==External links==
*   at CineSouth

 
 
 
 