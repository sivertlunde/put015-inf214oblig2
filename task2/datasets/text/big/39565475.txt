No Money Needed
{{Infobox film
| name           = No Money Needed
| image          = 
| image_size     = 
| caption        = 
| director       = Carl Boese 
| producer       = Arnold Pressburger Hans Wilhelm
| narrator       =  Hans Moser   Ida Wüst
| music          = Artur Guttmann
| editing        = G. Pollatschik 
| cinematography = Willy Goldberger   Karl Sander
| studio         = Allianz Tonfilm 
| distributor    = Bayerische Film
| released       = 5 February 1932
| runtime        = 92 minutes
| country        = Germany
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German comedy Hans Moser. It premiered on 5 February 1932.  It was based on a play by Ferdinand Alternkirch and was shot during November 1931.  A virtually bankrupt businessman in a small town manages to convince people that his newly arrived cousin, who is equally impoverished, is a millionaire.

==Cast==
* Hedy Lamarr as Käthe Brandt 
* Heinz Rühmann as Heinz Schmidt  Hans Moser as Thomas Theodor Hoffmann 
* Ida Wüst as Frau Brandt  Hans Junkermann as Herrmann Brandt 
* Kurt Gerron as Bank President Binder 
* Paul Henckels as The Mayor 
* Hans Hermann Schaufuß as Hotelier 
* Albert Florath as Sparkassenvorsteher 
* Fritz Odemar as Schröder 
* Gerhard Dammann as Mann 
* Hugo Fischer-Köppe   
* Ludwig Stössel   
* Siegfried Berisch   
* Wolfgang von Schwindt   
* Heinrich Schroth   
* Karl Hannemann   
* Leopold von Ledebur  

==References==
 

==Bibliography==
* Barton, Ruth. Hedy Lamarr: The Most Beautiful Woman in Film. University Press of Kentucky, 2010.
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 

 

 
 
 
 
 
 
 
 