Athanokkade
{{Infobox film
| name           = Athanokkade
| image          = Athanokkade_poster.jpg
| writer         = Surender Reddy
| starring       = Kalyan Ram Sindu Tolani Ashish Vidyarthi
| director       = Surender Reddy
| producer       = Nandamuri Janaki Ram
| distributor    =
| editing        = Gowtham Raju
| studio         = N.T.R. Arts
| cinematography = C. Ram Prasad
| released       = 7 May 2005
| runtime        =
| country        = India
| language       = Telugu
| music          = Mani Sharma
| budget         =
}} Telugu film Tamil as Aadhi. The Hindi version of this film is International Don.

== Plot ==
At the age of 11, Ram’s family and Anjali’s family members are killed by Anna (Ashish Vidyarthi). Rams father is Prakash Raj, who is an ACP. Due to a conflict between Anna and Rams father, he kills the complete family. In that tragic incident everyone is killed except Anjali (who is a small girl by then), Ram and their uncle. Ram is adopted into a different family and Anjali lives with her uncle. They both are not aware of the other’s survival in that incident. How Ram and Anjali take revenge on Anna is the remaining plot.

== Cast ==
* Kalyan Ram ... Ram
* Sindhu Tolani ... Anjali
* Ashish Vidyarthi ... Anna
* Prakash Raj ... Father of Ram

== Crew ==
* Director: Surender Reddy
* Screenplay: Surender Reddy
* Story: Surender Reddy
* Dialogue: Marudhuri Raja
* Producer: Janakiram Nandamuri
* Music: Mani Sharma
* Cinematography: C. Ram Prasad
* Art Direction: Narayana Reddy
* Editing: Gowtam Raju
* Action : Stun Shiva

==Release==
* The film was released with 72 prints initially and later 24 prints were added. 

==Review==
* This film became a big hit.It ran for 50 days in 80 centres and crossed 150 days in 46 centres.

==Awards==
;Filmfare Awards South Best Villain - Ashish Vidyarthi

;Nandi Awards
* Best Debut Director - Surender Reddy Best Character Chandra Mohan

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 