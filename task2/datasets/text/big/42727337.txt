A Mile from Home
{{Infobox film
| name               = A Mile From Home
| image              = 
| alt                = 
| caption            = 
| director           = Eric Aghimien
| producer           = Eric Aghimien
| story          =  
| starring       =  
| music          = Vincent VNC Umukoro Joshua Ekene
| cinematography = 
| editing        = 
| screenplay     =  
| studio         = Hills Pictures
| distributor    = 
| released       =  
| runtime        = 
| country        = Nigeria
| language       = English
| budget         = 
| gross          =
}}

A Mile from Home is a 2013 Nigerian action-drama film written, produced and directed by Eric Aghimien, starring Tope Tedela, Chiedozie Sambasa Nzeribe, Alex Ayalogu, Eric Nwanso and Tolu Akinbileje. {{cite news
|first=Osagie
|last=Alonge
|title=Trailer: Watch The Action Drama A Mile From Home
|newspaper=NET
|date=2013
|url=http://thenet.ng/2013/05/trailer-watch-the-action-drama-a-mile-from-home/
}}  Tedela portrays Lala, a university student with an identity crisis who joins a notorious gang to get revenge. The film was nominated in two categories at the 2nd Africa Magic Viewers Choice Awards and won best actor in a drama for Tedela.
The movie won the 2014 Africa Movie Academy Award for Achievement in Visual Effect.

== Plot ==
A Mile from Home chronicles the life of a university student, Jude Odaro/Lala (Tope Tedela) who joined a gang in his quest to avenge an injustice meted out to him by Stone, a notorious gangster who forcefully dispossesses him of a precious possession. 

Suku (Chiedozie Nzeribe), the Leader of the gang loves him and made him the number two man in the gang. Suku introduced him into crime and trusted him with everything he has and control. Jude got more committed to the gang and earned a new name, Lala. Jude finally yielded to his feeling for, Ivie, Sukus girlfriend and he is willing to die loving her. 

Don Kolo, who was convicted for drug dealing just got out of jail and deported from South Africa. He is broke and desperately wants to start his drug business at home. he needs supplies but has no money.
Suku and his men got a big supply from their contact Chief Lukas and Don Kolo will do anything to take it from them.

== Cast ==
*Tope Tedela as Lala
*Chiedozie Sambasa Nzeribe as Suku
*Alex Ayalogu as Don Kolo
*Tolu Akinbileje as Ivie
*Eric Nwanso as Deba

== Awards & Nominations ==
{| class="wikitable"
|-
! Award !! Category !! Recipient !! Result
|- Africa Magic Viewers Choice Awards
| Best Actor in a Drama
| Tope Tedela
|  
|-
| Best Lighting Designer
| Eric Aghimien
|  
|- Africa Movie Academy Awards
| Achievement in visual Effects
| Eric Aghimien
|  
|-
| Achievement in Make-Up
| 
|  
|-
| Best Young/Promising Actor
| Tope Tedela
|  
|- Best of Nollywood Awards
| Best Movie
| Eric Aghimien
|  
|-
| Best Actor in a Leading Role
| Tope Tedela
|  
|-
| Best Director
| Eric Aghimien
|  
|-
| Best Edited Movie
| Eric Aghimien
|  
|-
| Best Supporting Actor
| Chiedozie Sambasa Nzeribe
|  
|-
| Revelation of the Year
| Tope Tedela
|  
|-
| Best Screenplay
| Eric Aghimien
|  
|-
| Best Special Effects
| Eric Aghimien
|  
|- Golden Icons Academy Movie Awards
| Best Drama Film
| Eric Aghimien
|  
|-
| Best Director
| Eric Aghimien
|  
|-
| Best Motion Picture
| Eric Aghimien
|  
|-
| Most Promising Actor
| Tope Tedela
|  
|-
| Best On-Screen Duo
| Tope Tedela & Chiedozie Sambasa Nzeribe
|  
|-
| Best Producer 
| Eric Aghimien
|  
|-
| Best Edited Film
| Eric Aghimien
|  
|-
| Best Make-Up
| 
|  
|-
| Best Cinematography
| Eric Aghimien 
|  
|-
| Best Sound
| Vincent VNC Umukoro & Joshua Ekine
|  
|- Nigeria Entertainment Awards
| Best Actor
| Tope Tedela
|  
|-
| rowspan=10 | Nollywood Movies Awards
| Best Movie
| Eric Aghimien
|  
|-
| Best Director
| Eric Aghimien
|  
|-
| Best Cinematography
| Eric Aghimien
|  
|-
| Best Lead Male
| Tope Tedela
|  
|-
| Best Costume Design
| Godwin Aghimien
|  
|-
| Best Rising Star 
| Tope Tedela
|  
|-
| Best Makeup
| 
|  
|-
| Best Original Screenplay
| Eric Aghimien
|  
|-
| Best Set Design
| Biodun Olagbaju & Eric Aghimien 
|  
|-
| Best Soundtrack
| Vincent VNC Umukoro
|  
|}

== References ==
 

 

 
 
 
 
 
 
 


 