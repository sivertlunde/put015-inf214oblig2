Lankesh Patrike
 

Lankesh Patrike is an Indian weekly published in Kannada, Bangalore. It was started by Kannada writer P. Lankesh in 1980.

Lankesh Patrike introduced a number of new writers to the Kannada literary scene. Some of them, like B.T. Lalita Naik, Vaidehi (Kannada writer)|Vaidehi, Nataraj Huliyar, Sara Abubakkar and Banu Mushtaq, later went on to win accolades as writers. Lankesh Patrike was known for being the voice of the oppressed, the dalits, women and the minority communities.

P. Lankesh died in 2000. His daughter, Gauri Lankesh took over as the editor and his son Indrajit Lankesh as the publisher. Due to differences between the two siblings in 2005 Gauri Lankesh left the publication to start one of her own.    Gauri Lankesh alleged that Indrajit Lankesh threatened her with a revolver, while he lodged a complaint about a missing computer. 
 
==Film==
Director Indrajit Lankesh, P. Lankeshs son, has directed a Kannada film with the same name. It was released on 16 May 2003. The film features Darshan (actor)|Darshan, Vasundhara Das and Aditi Gowitrikar in the lead roles along with Ananth Nag, Devaraj, Lokanath in other pivotal roles.

The music for the film was composed by Babji-Sandeep and was produced by K. Manju. The film won the Karnataka State Film Award for Best Editor for B. S. Kemparaj for this film.  
 
==See also==
*Lankesh

==References==
 

 
 
 


 