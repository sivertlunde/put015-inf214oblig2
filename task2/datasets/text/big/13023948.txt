Stowaway in the Sky
{{Infobox film
| name           = Stowaway in the Sky
| image          = Stowawayposter.jpg
| image size     =
| caption        = U.K. Theatrical Poster
| director       = Albert Lamorisse
| producer       = Albert Lamorisse
| writer         = Albert Lamorisse
| narrator       = Jack Lemmon
| starring       =
| music          = Jean Prodromidès
| cinematography = Maurice Fellous Guy Tabary
| editing        = Pierre Gillette
| distributor    = Films Montsouris Lopert Pictures Corporation
| released       = September 14, 1960 (France) June 18, 1962 (United States)
| runtime        = 85 minutes
| country        = France French
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1960 France|French family adventure film, directed by Albert Lamorisse. 

Albert Lamorisse used his ten-year-old son Pascal as the main character in the film.

==Plot==
The film tells the story of Pascal, a small child whos fascinated by his grandfathers lighter-than-air Balloon (aircraft)|balloon. The older man claims hes invented the best mode of transportation: a balloon that can be controlled when in the sky.  The altitude, direction, and speed of the balloon are all under the direction of the pilot.

As the grand-père takes the balloon on a demonstration, Pascal climbs on board and lifts them both upward to an adventure. The balloon travels all around France, Brittany, over the ocean, and over Mont Blanc in the Alps.

However, the balloon turns out to be not so controllable: church spires become objects of threat, factory smokestacks become volcano-like, a stag hunt is no longer about the thrill of the chase, and they inadvertently kidnap washing on a clothesline and a guest at a wedding party in Brittany.

The land-bound adults have conniptions as the balloon wafts by, yet, Pascal has a great time.

==Background==
Jack Lemmon, the narrator of the films English version, was so impressed with the film that he bought the American rights.

==Cast==
* Pascal Lamorisse as Pascal
* Maurice Baquet as Le mécanicien
* André Gille as Le grand-père (grandfather)
* Jack Lemmon as Narrator (English version narrator)

==Critical reception==
In a brief film review of the film the weekly news magazine Time (magazine)|Time wrote, "Stowaway in the Sky will enchant moppet, matron and greybeard with its breath-catching, balloonists-eye view of the fair land of France." 

==Awards==
Wins
* Venice Film Festival: OCIC Award; Le Voyage en ballon; 1960.

Nominations
* Venice Film Festival: Golden Lion; Le Voyage en ballon; 1960.

==Footnotes==
 

==External links==
*  .
*  .

 
 
 
 
 
 
 
 