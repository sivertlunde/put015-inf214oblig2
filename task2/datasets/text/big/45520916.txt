The Adventures of Jimmy
 
The Adventures of Jimmy (1950 in film|1950) is an early, short experimental film directed by, and starring, James Broughton. It was Broughtons second film as sole director, following Mothers Day (1948 in film|1948), and was filmed by Art in Cinema founder Frank Stauffacher with an original musical score by Weldon Kees.

==Plot==

Jimmy (Broughton) is a woodsman who lives in a remote, mountain cabin. Several times he travels to the city in search of a wife, each time changing his appearance and clothing somewhat in hopes of improving his chances. 

==Legacy==

The film reportedly combined elements of farce and slapstick with the emerging experimental film genre, and as such would have been considerably lighter fare than Broughtons prior collaboration with Sidney Peterson, The Potted Psalm (1946 in film|1946) or his own Mothers Day. Although Broughton was no longer including it in his touring film programs by the late 1970s, at that time The Adventures of Jimmy was still available for rental from the Filmmakers Cooperative and Canyon Cinema. At some point the title was withdrawn from circulation, and The Adventures of Jimmy has become a very difficult title to see; regrettable, as it contains the film score by Weldon Kees. Kees was primarily a poet, painter and short story author whose music making is only sparsely documented.

==References==
 

 

 
 
 
 
 
 