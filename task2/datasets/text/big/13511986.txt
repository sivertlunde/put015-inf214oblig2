City of Water
{{ infobox film
| name           = City of Water
| image          = City-of-Water.jpg
| caption        = Poster for film
| distributor    = Municipal Art Society
| released       =  
| runtime        = 33 minutes
| country        = United States
| language       = English
}}
City of Water is a documentary about the future of the New York City waterfront by the Metropolitan Waterfront Alliance and the Municipal Art Society.

==Production==
The film was two years in the making.    and explores the aspirations of public officials, environmentalists, community activists, and working waterfront advocates for a diverse, vibrant waterfront at a time when the shoreline is changing faster than ever before. The documentary contains interviews with the following people: NYC Deputy Mayor Daniel L. Doctoroff, "green" economic consultant Majora Carter, author Phillip Lopate, US Representative Nydia Velázquez, CUNY Professor William Kornblum, Director of the Mayors Office of Longterm Planning and Sustainabililty Rohit Aggarwala, environmentalist Cathy Drew, historian Richard Melnick, founder of the Long Island City Community Boathouse Eric Baard, and others. Velazquez and Carter "present compelling cases for getting communities involved in reclaiming the waterfront, so that luxury condos — or sheer neglect in the case of the Bronx — do not limit their access" while Baard "points out the folly of placing guardrails all along the water’s edge, thus limiting open water access to boaters"   

Metropolitan Waterfront Alliance President Roland Lewis has described how the documentary "encapsulates the fulcrum issues that face the waterfront right now. Our generation has been presented with a unique opportunity to change the waterfront. The decisions that are facing us right now will affect our grandchildren."   

==Release==
The film premiered on the Brooklyn waterfront on June 21, 2007    and has since been screened at the National Arts Club, Socrates Sculpture Park, Empire-Fulton Ferry State Park and Water Taxi Beach in Queens, where it "played to an enthralled crowd of residents interested in the river."  The film was also screened at the Center for Architecture, New York County Lawyers Association and other venues in 2007, and at the Downtown Alliance and Gotham Center in 2008. The film was broadcast on Channel Thirteen WNET in April and will be screened at the Brooklyn Arts Council International Film Festival in May.

City of Water was directed by Jasper Goldman and Loren Talbot, cinematography by Igor Martinovic, edited by Cameron Clendaniel, with music by Scott Starrett. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 