The Caller (1987 film)
{{Infobox film| name = The Caller
 | image = Callerposter1987.jpg
 | caption = Video release poster
 | director = Arthur Allan Seidelman
 | producer = Frank Yablans   Charles Band (exec.)
 | writer = Michael Sloane
 | starring = Malcolm McDowell   Madolyn Smith
 | music = Richard Band
 | cinematography = Daniele Nannuzzi
 | editing = Bert Glatstein
 | distributor = Empire Pictures
 | released =   May 1987    December 27, 1989 (home video release)
 | runtime = 97 min.
 | country = United States English
 | budget = 
   | preceded_by = 
 | followed_by =
}} mystery Thriller thriller film starring Malcolm McDowell and Madolyn Smith, distributed independently by Empire Pictures.

The special effects were done by FX engineer John Carl Beuchler, known for his long list of film credits including  ,  , and Re-Animator. 

==Plot==
A mysterious man joins a woman in her forest cabin. He initially claims that his car had broken down and he needs her assistance. Soon things become suspect. The two examine each others stories for inconsistencies. The woman claims she caused the accident to lure the man up so she can kill him. The man claims he is a police officer, investigating the possibility the woman has killed her own family.

Neither claim stands up well to scrutiny. The movie then ends with a genre-bending twist.

==Release==
Originally intended for a theatrical release, the film was only shown at the 1987 Cannes Film Market and at the 1987 MystiFest in Italy.   On December 27, 1989, the film was finally released in the United States on videocassette by Trans World Entertainment.   MGM released a manufactured-on-demand DVD-R of the film on March 15, 2011. 

==References==
* 

==External links==
* 

 

 
 
 
 
 
 


 