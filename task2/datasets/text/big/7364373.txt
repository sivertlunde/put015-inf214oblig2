The Covered Wagon
{{Infobox film
| name           = The Covered Wagon
| image          = The Covered Wagon poster.jpg
| caption        = Theatrical poster
| director       = James Cruze
| producer       = Jesse L. Lasky
| writer         = Jack Cunningham (adaptation)
| based on       =   Lois Wilson Karl Brown
| music          = Josiah Zuro Hugo Riesenfeld
| editing        = Dorothy Arzner
| distributor    = Paramount Pictures
| released       =  
| runtime        = 98 mins.
| country        = United States
| language       = Silent English intertitles
| budget         = $782,000
| gross          = $3.5 million   accessed 19 April 2014  
}} silent Western Western film Lois Wilson as Molly Wingate.  On their quest they experience desert heat, mountain snow, hunger, and Indian attack.   

==Cast==
*J. Warren Kerrigan as Will Banion (hero) Lois Wilson as Molly Wingate (heroine) Alan Hale as Sam Woodhull (villain) 
*Ernest Torrence as William Jackson 
*Tully Marshall as Jim Bridger 
*Ethel Wales as Mrs. Wingate  Charles Ogle as Jesse Wingate 
*Guy Oliver as Kit Carson  Johnny Fox as Jed Wingate

;Cast notes
Tim McCoy, as Technical Advisor, recruited the Indians who appeared in this movie.
 {{cite book 
  | last = Franklin
  | first = Joe
  | title = Classics of the Silent Screen
  | publisher=Bramhall House}} 

==Production background==
The film was a major production for its time, with an estimate budget of $782,000.   

In his 1983 book Classics of the Silent Cinema, radio and TV host Joe Franklin claimed this film was "the first American epic not directed by Griffith". {{ Cite book
 | last = Franklin
 | first = Joe
 | title = Classics of the Silent Screen
 | publisher = Bramhall House}} 

In the 1980 documentary  , Jesse L. Laskey Jr. maintained that the goal of director James Cruze was " ... to elevate the Western, which had always been sort of a potboiler kind of film, to the status of an epic." 
  
The film required a large cast and film crew and many extras,  and was filmed in various locations, including Palm Springs, California {{Cite book
 | last = Niemann
 | first = Greg
 | title = Palm Springs Legends: creation of a desert oasis
 | publisher = Sunbelt Publications
 | year = 2006
 | location = San Diego, CA
 | pages = 286
 | url =  http://books.google.com/books?id=RwXQGTuL1M0C&pg=PA169&lpg=PA169&dq=palm+springs+film+locations&source=bl&ots=N8AgHRYZbg&sig=RMIwy_g3fEV3KoK1aa7P4VxuJWM&hl=en#v=onepage&q=palm%20springs%20film%20locations&f=false
 | doi =
 | id =
 | isbn = 978-0-932653-74-1
 | mr =
 | jfm =
|oclc=61211290}}  ( )   and several places in Nevada and Utah.  The dramatic buffalo hunt and buffalo stampede scenes were filmed on Antelope Island, Great Salt Lake, Utah. During filming for the movie, seven bison from the Antelope Island Bison Herd were shot and killed.

Significantly, the Conestoga wagons gathered by Paramount from all over the Southwest were not replicas, but the real wagons that had brought the pioneers west. They were cherished heirlooms of the families who owned them. The producers offered the owners $2 a day and feed for their stock if they would bring the wagons for the movie. Most of the extras seen on film are the families who owned the covered wagons and were perfectly at home driving them and living out of them during the production. 

==Reception== DeForest Phonofilm Bella Donna on 1 April 1923 with a Phonofilm soundtrack, also only at the premiere at the Rivoli.

The film was the most popular movie of 1923 in the US and Canada. 

==References==
 

==See also==
*The House That Shadows Built (1931) promotional film released by Paramount with excerpt of The Covered Wagon

==External links==
*  
* 
* 
*  at Virtual History
*  at imp awards
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 