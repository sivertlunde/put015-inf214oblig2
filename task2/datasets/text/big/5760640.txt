Rendu
 

{{Infobox film
| name = ரெண்டு Rendu
| image =
| director = Sundar C
| writer = Subha  (Dialogue)
| screenplay = Sundar C Subha
| story = Sundar C Madhavan Reema Santhanam Manivannan
| producer = Kushboo Sundar Aascar Film Pvt. Ltd
| music = D. Imaan
| cinematography = Prasad Murella
| editor = Mu. Kasi Vishwanath
| released = 24 November 2006
| runtime = 146 minutes Tamil
| budget =
}}
 Tamil  film directed by Sundar C. The movie stars R. Madhavan|Madhavan, Reema Sen, Anushka Shetty, Bhagyaraj and Vadivelu. The films music is composed by music director D. Imaan. The film was a commercial success.

==Plot==
 Shakti (R. Madhavan|Madhavan) is a young man from a village, with no job but much ambition and dreams. As he sees his life pass him by, stuck in a rut in his hometown, he decides that he will try his fortunes elsewhere. Chennai beckons him and off goes Shakti, to join his uncle Kirikalam (Vadivelu), who has a stall of magic tricks at an exhibition. No, Kirikalam is not really a Illusionist|magician, but is posing as one and his amateurish tricks dont exactly bring in big crowds.

Next door to Shakti and Kirikalams stall though, is one ever-populated with visitors. The reason: its an all-girls stand, with mermaid costumes as the theme. Velli (Reema Sen) is the head mermaid at this stall run by her sister, and bitter quarrels between Shakti and Velli ensue, when Shakti feels that Velli and her team are unfairly taking their customers away and Velli has this impression of Shakti being an unsavoury character. This misunderstanding is seen at different occasions where Velli and Madhavan just happen to be in the wrong place at the wrong time - be it when Shakti gets off the bus from his village, asks for directions and is told to follow Velli, walking in the distance (Velli thinks hes a Promiscuity#Male promiscuity|womaniser) or when Shakti finally catches up with a miscreant who had picked his pocket and demands his money back—only Velli walks into the scene only at the demanding part. This whole collage of scenes is animated, with Kirikalam and Shakti trying to woo customers back to their stall, Shakti and Velli getting into arguments and a general state of "fun" ruckus. Of course, love has been brewing as an undercurrent between the two young people.

Manivannan plays the character of the owner of the exhibition grounds and keeps harassing Velli under some pretext or the other, like the rent not being paid on time. In fact, he has a soft spot for Velli, feelings thoroughly rejected by her. He finally abducts her, but Shakti rescues Velli. Similarly, Vellis plight is repeated when her "mora maapillai" (betrothed suitor) enters the scene and tries to force her to be with him, and Shakti steps in once again to save and protect Velli. The undercurrents of love blow up into a full-fledged torrent and Velli and Shakti make up for all of their lost time professing their love for each other and romancing their way happily through the exhibition.

Meanwhile, we see a series of murders in different towns of the state and two perpetrators of these crimes slinking away from each.
Not every time is it foolproof though, and at one crime scene, a witness spots the main culprit. He is instructed by the police to sketch and give them a pictorial description of the murderer, when it comes to the polices knowledge that both the criminals have been
seen at Kumbakonam some time ago. Going through various fact files, the police comes across the incident of a bad fire at a marriage hall in Kumbakonam and as the officer in charge of the investigation (played by Bhagyaraj) flips through the picture files of the deceased, Kannans (Madhavan - the 2nd) face is shown and the witness jumps.

He tells the police he doesnt need to sketch anything when the face of the main murderer itself is staring them in the eye - he points to the photo of Kannan - it is the face of Shakti, only this one has light eyes.

Investigations into the murder lead the police to Chennai and as luck would have it, Shakti is spotted, recognized and arrested.

Kannan, on the other hand follows the murder investigation and is upset to find that an innocent man has been captured for crimes he has committed. He meets Kirikalam and Velli and plots to help Shakti get out. And so he does, and Shakti and he escape by the skin of their teeth.

After much chase-and-hide, Kannan tells the story of what led him to murder all those people - flashback time.

Kannan, his elder brother and his father were renowned caterers and were commissioned to cook for a marriage function in Kumbakonam. The host was Kannans fathers friend and this gentlemans daughter was getting married in a joyous ceremony. There, Kannan meets Jyoti (Anushka Shetty|Anushka) and falls in love instantly. At first he panics that Jyoti is the bride at the function but is vastly relieved when he finds out she is not. She really likes Kannan too and it all seems like Wonderland.

Unfortunately, the groom creates trouble at the wedding and walks out, leaving the family distraught and desperate. Suddenly Kannan makes a suggestion: Father, this is our friends wedding and the bride is a wonderful girl. Why cant Anna (elder brother) marry her?
A brilliant idea and it is decided—only fate has to fell her axe right then, when all hell breaks loose and a raging fire attacks the marriage house. The father of the bride is a good man, and a dignitary in the town of Kumbakonam. He comes to know that a bunch of powerful thugs and gang leaders have sold a large piece of government property to a north Indian businessman, completely illegally at exorbitant prices, with no gain for the townspeople themselves. He strongly opposes them and all their mischief comes to public notice and foils their plans. They want his blood as revenge.

They set fire to the building of the wedding and kill every single inmate inside - but two people escape, Kannan and his friend. Kannan, whose light eyes are a result of the fire (he walks with help and not very clear sight) is heartbroken and devastated at the loss of his entire family and lady love at what was to be a joyous, happy occasion. He vows retribution on the villains and sets about cold-bloodedly fulfilling this. Shakti decides to help Kannan achieve his revenge. At the end, however, Kannan sacrifices his own life to kill the final baddie, and he reunites with his family and lady love in heaven.

==Cast==

{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- Madhavan || Kannan, Shakthi
|-
| Anushka Shetty || Jothy
|-
| Reema Sen || Velli
|- 
| Bhagyaraj || CBI
|-
| Vadivelu || Great Kirikalan
|- Santhanam || Seenu
|}

==References==
 

==External links==
*  

 

 
 
 
 