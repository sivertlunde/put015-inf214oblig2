Trick or Treats
{{Infobox film
| name           = Trick or Treats
| image          = Trick or Treats poster.jpg
| alt            = 
| director       = Gary Graver
| producer       = Caruth C. Byrd Hedy Dietz Gary Graver Glenn Jacobson Lee Thornburg
| screenplay     = Gary Graver
| starring       = Jacqueline Giroux (as Jackelyn Giroux) Peter Jason Chris Graver David Carradine Carrie Snodgress
| cinematography = Gary Graver
| editing        = Gary Graver
| distributor    = Lone Star Pictures
| released       = 29 October 1982
| runtime        = 91 min.
| country        = United States
| language       = English
}}
 slasher film.

==Plot==
A baby sitter is stuck watching over a young brat on Halloween night who keeps playing vicious pranks on her. To add to her trouble the boys deranged father has escaped from an asylum and is planning on making a visit.

== Critical reception ==

Allmovie gave the film a negative review, writing "Genre fans generously overlook bad logic in exchange for action and overkill, but the plot holes that litter director/screenwriter Gary Gravers story are never plugged with the cheap spectacle that might have given this by-the-numbers stalker film a reason to exist." 

==Release==
The film was released on VHS in the United States by Vestron Video. {{cite web|url=http://www.imdb.com/title/tt0084825/companycredits|title=Company credits for
Trick or Treats|publisher=imdb.com|accessdate=2011-04-01}}   This version is currently out of print.

On 11/09/2013, the film was officially released on DVD by Code Red DVD. 

==See also==
*Final Exam (film)

== External links ==
*  
*  
*  

==References==
 

 
 
 
 
 
 

 