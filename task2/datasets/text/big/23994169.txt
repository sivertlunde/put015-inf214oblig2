The Beirut Apt
 
{{Infobox film
| name           = The Beirut Apt
| image          = The-beirut-apt.jpg
| caption        = Theatrical release poster
| director       = Daniel Salaris
| producer       = Gavin Hallier   Co-producers:   Popular Production   Collettivo Don Quixote 
| writer         = 
| starring       = 
| music          = 
| cinematography = Fabio Colazzo
| editing        = Roberto Carini 
| distributor    = Malastrada Film
| released       = 2007
| runtime        = 50 minutes
| country        = Italy United Kingdom
| language       = English
| budget         = 
}} Lebanese gays and lesbians and comprises interviews and testimonials of their experiences conducted in a Beirut apartment, thus the title of the documentary.

An official soundtrack was released. The documentary was an official selection of 22nd London Lesbian and Gay Film Festival, as well as the 23rd Turin International Lesbian & Gay Film Festival.

==Synopsis==
* 

It shows Lebanese gays and lesbians passing through the conflict of sexual, religious and social identity, the war against Israel and the "Article 534" of the Lebanese Penal Code that forbids "indecent acts". Main characters featured are Youssef, dancer, Maha, a psychotherapist, Rachid, a shopworker and Faisal, a Palestinian origin Reike healer. 

==See also==
*LGBT rights in Lebanon

==References==
 

==External links==
* 

 
 
 
 
 
 


 