The Matrix Reloaded
 
{{Infobox film
| name                 = The Matrix Reloaded
| image                = Poster - The Matrix Reloaded.jpg
| caption              = Theatrical release poster The Wachowski Brothers 
| producer             = Joel Silver
| writer               = The Wachowski Brothers 
| starring             = {{Plain list|
* Keanu Reeves
* Laurence Fishburne
* Carrie-Anne Moss
* Hugo Weaving
* Jada Pinkett Smith
* Gloria Foster
}} Don Davis
| cinematography       = Bill Pope
| editing              = Zach Staenberg
| production companies = {{Plain list|
* Village Roadshow Pictures
* NPV Entertainment
* Silver Pictures
}}
| distributor          = {{Plain list|
* Warner Bros.  
* Roadshow Entertainment   
}}
| released             =  
| runtime              = 138 minutes  
| country              = {{Plain list|
* United States
* Australia  
}}
| language             = English
| budget               = $150 million   
| gross                = $742.1 million 
}} science fiction The Wachowski general release by Warner Bros. in North American theaters on May 15, 2003, and around the world during the latter half of that month. It was also screened out of competition at the 2003 Cannes Film Festival.    The video game Enter the Matrix, which was released on May 15, and a collection of nine animated shorts, The Animatrix, which was released on June 3, supported and expanded the storyline of the film. The Matrix Revolutions, which completes the story, was released six months after Reloaded, in November 2003.

==Plot== Neo can contact her. One of the Caduceus crew, Bane (The Matrix)|Bane, encounters Agent Smith, who takes over Banes avatar. Smith then uses this avatar to leave the Matrix, gaining control of Banes real body.

In Zion, Morpheus announces the news of the advancing machines to the people. Neo receives a message from the Oracle and returns to the Matrix to meet her bodyguard Seraph (The Matrix)|Seraph, who then leads him to her. After realizing that the Oracle is part of the Matrix, Neo asks how he can trust her; she replies that it is his decision. The Oracle instructs Neo to reach the Source of the Matrix by finding the Keymaker (The Matrix)|Keymaker, a prisoner of the Merovingian (The Matrix)|Merovingian. As the Oracle departs, Smith appears, telling Neo that after being defeated, he refused to be deleted, and is now a rogue program. He demonstrates his ability to clone himself using other inhabitants of the Matrix, including other Agents, as hosts. He then tries to absorb Neo as a host, but fails, prompting a battle between Smiths clones and Neo. Neo manages to defend himself, but is forced to retreat from the increasingly overwhelming numbers.
 the Twins, the Merovingians chief henchmen. Morpheus defeats the Twins, Trinity escapes, and Neo flies in to save Morpheus and the Keymaker from Agent Johnson. In the real world, Zions remaining ships prepare to battle the machines. Within the Matrix, the crews of the Nebuchadnezzar, Vigilant and Logos help the Keymaker and Neo reach the door to the Source.
 destroy a power plant to prevent a security system from being triggered, and the crew of the Vigilant must destroy a back-up power station. The Logos succeeds, while the Vigilant is bombed by a Sentinel in the real world, killing everyone on board. Although Neo asked Trinity to remain on the Nebuchadnezzar, she enters the Matrix to replace the Vigilant crew and complete their mission. However, her escape is compromised by an Agent, and they fight. As Neo, Morpheus and the Keymaker try to reach the Source, the Smiths appear and try to kill them. The Keymaker unlocks the door to the Source, allowing Neo and Morpheus to enter and escape from the Smiths, but the Smiths kill the Keymaker while he tries to close the door to the Source. Neo enters a door and meets a program called the Architect (The Matrix)|Architect, the Matrixs creator.

The Architect explains that Neo is part of the design of the sixth iteration of Matrix, designed to stop the fatal system crash that naturally occurs due to the concept of human choice. As with the five previous Ones, Neo can choose either to return to the Source with his unique code to reboot the Matrix and pick survivors to begin to repopulate the soon-to-be-destroyed Zion, or cause the Matrix to crash and kill everyone connected to it; combined with Zions destruction, this would mean mankinds extinction. Neo learns of Trinitys situation and chooses to save her instead. As she falls off a building, he flies in and catches her, then removes a bullet from her body and restarts her heart. Back in the real world, Sentinels destroy the Nebuchadnezzar. Neo displays a new ability to disable the machines with his thoughts, but falls into a coma from the effort. The crew are picked up by another ship, the Mjolnir ("Hammer")|Hammer. Its captain, Roland, reveals the other ships were wiped out by the machines after someone activated an EMP too early, and that they found only one survivor afterwards - revealed to be Bane.

==Cast==
  Neo
* Morpheus
* Trinity
* Smith
* Niobe
* The Oracle Link
* Persephone
* The Merovingian The Keymaker
* Harry Lennix as Commander Lock
* Anthony Zerbe as Councillor Hamann Zee
* The Architect Twins
* Daniel Bernhardt as Agent Johnson Seraph
* Bane
* Leigh Whannell as Axel Cas
* Nathaniel Lees as Captain Mifune
* Roy Jones, Jr. as Captain Ballard
* David A. Kilde as Agent Jackson
* Matt McColm as Agent Thompson
* Cornel West as Councillor West
* Steve Bastoni as Captain Soren Anthony Wong Ghost
* Kid
 

Zee was originally played by Aaliyah, who died in a plane crash on August 25, 2001, before filming was complete, requiring her scenes to be reshot with actress Nona Gaye.  {{cite web|author=August 27, 2001 |url=http://edition.cnn.com/2001/SHOWBIZ/Music/08/27/aaliyah.obit/
 |title=Aaliyah: A beautiful persons life cut short |publisher=Archives.cnn.com |date=2001-08-27 |accessdate=2015-02-08}} 

==Production==

===Filming=== Fox Studios in Australia, concurrently with filming of the sequel, The Matrix Revolutions|Revolutions. The freeway chase and "Burly Brawl" scenes were filmed at the decommissioned Naval Air Station Alameda in Alameda, California. The producers constructed a 1.5-mile freeway on the old runways specifically for the film. Some portions of the chase were also filmed in Oakland, California, and the tunnel shown briefly is the Webster Tube, which connects Oakland and Alameda. Some post-production editing was also done in old aircraft hangars on the base as well.
 Route 59, the stretch of freeway known as the "Innerbelt", for filming of the freeway chase when it was under consideration. However, producers decided against this as "the time to reset all the cars in their start position would take too long".    MythBusters would later reuse the Alameda location in order to explore the effects of a head-on collision between two semi trucks, and to perform various other experiments.

Around 97% of the materials from the sets of the film were recycled after production was completed; for example, tons of wood were sent to Mexico to build low-income housing. 

Some scenes from the film Baraka (film)|Baraka by Ron Fricke were selected to represent the real world shown by the wallmonitors in the Architect (The Matrix)|Architects room.  
The scene where The Oracle (Gloria Foster) appears were filmed before her death on September 29, 2001.

===Visual effects===
 
Following the success of the previous film, the Wachowskis came up with extremely difficult action sequences, such as the Burly Brawl, a scene in which Neo had to fight 100 Smiths. To develop technologies for the film, Warner Bros. launched ESC Entertainment.   
 virtual camera reflectvity of captured and simulated adequately and Paul Debevec et al. captured the reflectance of the human face and Borshukovs work was strongly based on the findings of Debevec et al.
 dense aka. photogrammetric capture technique called optical flow.   

The algorithm for Universal Capture was written by George Borshukov, visual effects lead at ESC, who had also created the photo-realistic buildings for the visual effects in The Matrix. With this collected wealth of data and the right algorithms, they finally were able to create virtual cinematography in which characters, locations, and events can all be created digitally and viewed through virtual cameras, eliminating the restrictions of real cameras, years of compositing data, and replacing the use of still camera arrays or, in some scenes, cameras altogether. The ESC team render the final effects using the program mental ray. 

===Music===
  Don Davis, novel of the same name by William Gibson, a major influence on the directors.

Leitmotifs established in The Matrix return — such as the Matrix main theme, Neo and Trinitys love theme, the Sentinels theme, Neos flying theme, and a more frequent use of the four-note Agent Smith theme — and others used in Revolutions are established.
 Marilyn Manson, Slap It" Fluke — listed on the soundtrack as "Zion" — was used during the rave scene.

Linkin Park contributed their instrumental song "Session" to the film as well, although it did not appear during the course of the film. P.O.D. composed a song called "Sleeping Awake", with a music video which focused heavily on Neo, as well as many images that were part of the film. Both songs played during the films credits.

It was originally planned for the electronic band Röyksopp to create the soundtrack, but this offer was turned down.  

==Reception==

===Box office===
The film earned an estimated $5 million during Wednesday night previews in North America. Reloaded grossed $37.5 million on its Thursday opening day in North America from 3,603 theaters, which was the second highest opening day after Spider-Man s $39.4 million and highest for a Thursday. The film earned $91.7 million in its first weekend,  and ultimately grossed $281.5 million in the US, and $742.1 million worldwide. 

===Critical response===
Reloaded had mostly positive critical reception, with a Rotten Tomatoes approval rating of 73%.  The films average critic score on Metacritic is 62/100.  However, Entertainment Weekly named it as one of "The 25 Worst Sequels Ever Made". 

Some positive comments from critics included commendation for the quality and intensity of its action sequences,  and its intelligence.  Tony Toscano of Talking Pictures had high praise for the film, saying that "its character development and writing...is so crisp it crackles on the screen" and that "Matrix Reloaded re-establishes the genre and even raises the bar a notch or two" above the first film, The Matrix. 

On the other hand, negative comments included the sentiment that the plot was alienating,   with some critics regarding the focus on the action as a detriment to the films human elements.   Some critics thought that the number of scenes with expository dialog worked against the film,  and the many unresolved subplots, as well as the cliffhanger ending, were also criticized. 

===Awards===
 

===Controversy===
The film was initially banned in Egypt because of the violent content and because it put into question issues about human creation "linked to the three monotheistic religions that we respect and which we believe in." 

==See also==
 
* Simulated reality
* List of films featuring powered exoskeletons
 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 