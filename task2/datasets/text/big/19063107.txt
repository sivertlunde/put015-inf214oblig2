First to Fight (film)
{{Infobox film
| name           = First to Fight
| image          = First to Fight.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Christian Nyby
| producer       = Jimmy Lydon
| writer         = Gene L. Coon
| based on       = 
| screenplay     = 
| narrator       = 
| starring       = Chad Everett Marilyn Devin Dean Jagger
| music          = Fred Steiner
| cinematography = Harold E. Wellman
| editing        = George R. Rohrs
| studio         = Warner Bros. Pictures
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 92 min.
| country        = United States English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 United States John "Manila" Iwo Jima. 

The title of First to Fight was derived from the US military practise of sending in  . Retrieved: April 26, 2015.  

==Plot==
In 1942, a force of American Marines are attacked by the Japanese in the jungles at Guadalcanal Campaign|Guadalcanal.  Sergeant "Shanghai" Jack Conell (Chad Everett)  is the sole survivor of his squad, and when he makes it back to his own lines, he is given a field promotion to Lieutenant and awarded the Medal of Honor by Lt. Col. Baseman (Dean Jagger). 
 War Bonds Tour, Connell is reluctant to trade on his heroism and does not consider himself a hero, just a survivor. When he returns home, despite efforts of his friends to find him dates, he falls in love with Peggy Sandford (Marilyn Devin) and the two are married. Her fiance had been killed and Peggy extracts a promise from Connell that he will not go back into the war. For a time, he trains new recruits at Camp Pendleton Marine Base, but is emotionally distraught as he comes to think of himself as a slacker and treats his trainees harshly in the belief that they need to be hardened for battle. 

With a confrontation with Lt. Col. Baseman who is afraid for him and his mental state, Connell is offered the chance to go back into the lines. He volunteers to return to the fighting, but even with Peggy, now pregnant and fearing for him, releasing him from his promise, Connell finds it difficult to become the warrior he once was. After freezing in combat, he eventually takes charge of his unit and leads them successfully in a raid against a Japanese island stronghold.

==Cast==
 
* Chad Everett as Jack Connell  
* Marilyn Devin as Peggy Sanford  
* Dean Jagger as Lt. Col. Baseman  
* Bobby Troup as Lt. Overman  
* Claude Akins as Capt. Mason  
* Gene Hackman as Sgt. Tweed  
* James Best as Sgt. Carnavan  
* Norman Alden as Sgt. Schmidtmer  
* Bobs Watson as Sgt. Maypole  
* Ken Swofford as OBrien  
* Ray Reese as Hawkins  
* Garry Goodgion] as Karl  
* Robert Austinas Adams  
* Clint Ritchie as Sgt. Slater  
* Stephen Roberts as Pres. Franklin D. Roosevelt
 

==Production==
First to Fight was shot at Camp Pendleton Marine Base,  . Retrieved: April 26, 2015.  Most of the equipment matched period pieces from World War II and helped to make the film more authentic. 

Footage from Casablanca (film)|Casablanca (1942) was also incorporated in First to Fight. 

==Reception== Hollyowwod was ably backed up by Oscar-winner Dean Jagger, actor and Jazz singer Bobby Troup, Claude Akins and James Best, both of whom would later find fame as TV stars. 

==References==
Notes
 

Bibliography
 
* Brady, James. Hero of the Pacific: The Life of Marine Legend John Basilone. New York: Wiley, 2010. ISBN 978-0-470-37941-7.
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 