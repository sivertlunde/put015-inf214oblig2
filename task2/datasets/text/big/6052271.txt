Redskin (film)
{{Infobox film name = Redskin image = RedSkin2.jpg producer = director = Victor Schertzinger writer = Elizabeth Pickett starring = Richard Dix Julie Carter music = Louis De Francesco J.S. Zamecnik cinematography = Ray Rennahan Edward T. Estabrook Harry Hallenberger (uncredited) editing = Otho Lovering distributor = Paramount Pictures released =   runtime = 90 minutes language = English
|country= United States
}}

 
Redskin is a 1929 American feature film with a synchronized score and sound effects, filmed partially in Technicolor, and released by Paramount Pictures.

==Cast== Richard Dix - Wing Foot
*Julie Carter - Corn Blossom
*Tully Marshall - Navajo Jim
*George Regas - Notani
*Noble Johnson - Pueblo Jim
*Jane Novak - Judith Stearns
*Larry Steers - John Walton
*Augustina Lopez - Grandmother Yina
*Bernard Siegel - Chahi
*Jack Padjan - Barrett (uncredited)

==Production background==
Color film was used for the scenes taking place on the Indians land, while black and white was used only in the scenes set in the white mans world. Roughly three-fourths of the film is in color. 

==Home video==
Redskin is currently available in the United States on disc 4 of the DVD collection Treasures III: Social Issues in American Film, 1900-1934.

==See also==
*List of early color feature films
*The Vanishing American (1925)

==References==
 

== External links ==
*  
*  
 
 
 
 
 
 
 
 
 
 
 