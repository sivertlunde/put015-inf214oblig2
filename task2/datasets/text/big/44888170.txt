The Victoria Cross (film)
{{Infobox film
| name           = The Victoria Cross
| image          = 
| alt            = 
| caption        =
| director       = Edward LeSaint
| producer       = Jesse L. Lasky Margaret Turnbull
| starring       = Lou Tellegen Cleo Ridgely Sessue Hayakawa Ernest Joy Mabel Van Buren Frank Lanning
| music          = 
| cinematography = Harold Rosson
| editing        = 
| studio         = Jesse L. Lasky Feature Play Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent Margaret Turnbull. The film stars Lou Tellegen, Cleo Ridgely, Sessue Hayakawa, Ernest Joy, Mabel Van Buren and Frank Lanning. The film was released on December 14, 1916, by Paramount Pictures.  

==Plot==
 

== Cast ==   
*Lou Tellegen as Major Ralph Seton
*Cleo Ridgely as Joan Strathallen
*Sessue Hayakawa as Azimoolah
*Ernest Joy as Sir Allen Strathallen
*Mabel Van Buren as Princess Adala
*Frank Lanning as Cassim
*Harold Skinner as Seereek

== References ==
 

== External links ==
*  
 

 
 
 
 
 
 
 
 