Kolomba Sanniya
{{Infobox film
| name = Kolomba Sanniya
 | image = KolombaSanniya1.jpg
 | imagesize=
 | caption =
 | director = Manik Sandrasagara
 | producer = Tyronne Fernando
 | writer =
 | starring = Geetha Kumarasinghe Joe Abeywickrema Denawaka Hamine Freddy Silva Eddie Jayamanne Don Sirisena
 | music = Sunil Shantha Clarence Wijewardena
 | cinematography =
 | editing =
 | distributor =
 | country    = Sri Lanka 1976
 | runtime = Sinhala
 | budget =
   | followed_by =
}}

Kolomba Sanniya (Colombo Mania) is a 1976 Sinhalese language comedy film directed by Manik Sandrasagara that follows the lives of middle and upper-class people in rural and urban Sri Lanka. The film stars Geetha Kumarasinghe, Joe Abeywickrema and Denewake Hamine are notable for containing the first depiction of best comedy in a Sinhala film.

==Synopsis==
Andare, (Joe Abeywickrema) a rustic villager finds a valuable gem when he goes behind a bush to perform his morning ablutions. (no toilets in the village) He sells the gem and buys a house in Colombo. The whole family, Andares elder sister (Denawaka Hamine), brother Jakolis (Eddie Jayamanne), son (Freddie Silva) and daughter (Geetha Kumarasinghe) try their best to get adjusted to the life of Colombo 7 - Cinnamon Gardens and as they find eventually, it is not an easy task.

==Cast==
Geetha Kumarasinghe - Andaress daughter 
Joe Abeywickrema - Andare 
Denawaka Hamine - Andares elder sister 
Freddie Silva - Andares son 
Eddie Jayamanne - Andares brother 
Don Sirisena

==Music==
The music in the film was composed by Sunil Santha and Clarence Wijewardena. The song Dum dama dama yana, Dum bara bage... was written by Father Marceline Jayakody and the music by maestro Sunil Santha. Music was also consulted by Sunil Mendis.

==References==
 
 

==External links==
* 
* 
* 
* 
* 

 
 
 


 