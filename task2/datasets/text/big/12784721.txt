No Sleep til Shanghai
 
{{Infobox film name = No Sleep til Shanghai image = No Sleep til Shanghai cover.jpg
|caption= No Sleep til Shanghai film poster director = Todd Angkasuwan writer = starring = Jin Au-Yeung producer = Carl Choi Todd Angkasuwan
|distributor= Raptivism/Imperial Records|Imperial/Caroline Distribution|Caroline/EMI
|released= May 22, 2007 runtime = 70 minutes language = Mandarin
|budget =
|}}
 Jin and was directed by Todd Angkasuwan.

==Plot==
Riding the notoriety of winning the weekly Freestyle Friday rap battle on BET’s 106 and Park for seven consecutive weeks, Jin shattered seemingly insurmountable boundaries and stereotypes by becoming the first Asian American rapper to sign a recording deal with a major label when he signed with Virgin Records (Ruff Ryders). The film follows Jin and his crew as they tour Asia to promote Jin’s debut album, The Rest is History. Along the way, we are treated to a fascinating glimpse into the life of a rapper, as well as the rapidly growing hip hop communities in Asian cities such as Hong Kong, Shanghai, Taipei, and Tokyo.  The film gained wide acclaim and some shock from screening audiences at the Atlanta Film Festival as they reacted to the startling visage of Jamaican-American promoter Andrew Ballen speaking fluent Chinese on the Shanghai leg of the tour.

==Awards and nominations==
* Atlanta Hip Hop Film Festival - Best Documentary (nominee)
* San Francisco Intl Asian American Film Festival - Best Documentary (nominee)

==External links==
*  
*  

 
 
 
 
 
 


 