Corn on the Cop
{{multiple issues|
 
 
}}

{{Infobox Hollywood cartoon
|cartoon_name=Corn on the Cop
|series=Merrie Melodies
|director=Irv Spector Manny Perez Warren Batchelder  Bob Matz
|layout_artist=Dick Ung Tom OLoughlin Joanie Gerber
|musician=Bill Lava
|producer=David H. DePatie Friz Freleng
|story_artist=Friz Freleng Warner Bros. Pictures
|release_date=July 24, 1965
|color_process=Technicolor
|runtime=6:00
|movie_language=English
|preceded_by=Suppressed Duck
|followed_by=Tease for Two
}}
 Joanie Gerber.

The title is a play on "corn on the cob."

==Plot==
On Halloween night, Granny is shopping for candy at a local grocery store. The next customer is an armed robber, who is wearing a blouse and skirt identical to the real Granny. Policemen Daffy and Porky are given the suspects description and attempt to apprehend the robber.

Most of the rest of the cartoon depicts Daffy and Porky confusing Granny with the actual suspect (because of their identical clothing), and bungling said attempts to capture the crook. An annoyed Granny, who has no idea what is going on, mistakes the inept policemen for mischievous trick-or-treating|trick-or-treaters, while the robber (who is hiding out in a vacant apartment in the same building Granny is living) also foils every attempt by Daffy and Porky.

Eventually, Granny figures out what is going on and catches her "double". After giving the suspect a spanking, she hands him over to Officer Flaherty. Flaherty commends Granny for catching the robber, after which Granny tells him "there are two other juvenile delinquents" who should be sent home to their parents (referring to Daffy and Porky), but when she asks for their addresses, Daffy gives her their precinct address and begs her to back off.

==Milestones==
Granny - voiced here by Joan Gerber instead of June Foray - makes her final appearance. "Corn on the Cop" also reveals Grannys actual last name: Webster (in the closing scene where Daffy and Porkys superior police officer addresses Granny by name). 

==References==
 

==External links==
*  

 
 
 
 


 