Spanish Affair
  Spanish co-produced drama film directed by Don Siegel and Luis Marquina. It featured Carmen Sevilla, Richard Kiley and José Guardiola. 

==Plot==
Merritt Blake, a young American architect, comes to Spain to close a series of commercial agreements. When he learns that he will need to convince three Spanish architects of the design which he has proposed, he insists on inviting Mari (Antonios assistant) to function as interpreter.

They set off, first to visit the Conde de Rivera, who tells Merritt that he will support the project if his colleague in Barcelona favors the plan. Mari cautions Merritt that this is not a "done deal."

On the way to Barcelona, Mari notices that her former boyfriend is following her. She fears that he will assume she is involved inappropriately with the architect. She tries to return to Madrid, but the architect insists she continue the journey with him.

==Cast==
*Richard Kiley as Merritt Blake
*Carmen Sevilla as Mari Zarubia
* Jose Guardiola as Antonio
*Jesus Tordesillas as Sotelo
*Jose Manuel Martin as Fernando
*Francisco Bernal as waiter
*Purita Vargas as Purita
*Antonio S. Amaya as Miguel flamenco singer

==Curiosities==
One of the cars which appears in the film is a Pegaso Z-102 Spyder.

==References==
 
http://www.imdb.com/title/tt0052230/
http://www.filmaffinity.com/es/film182752.html

 

 
 
 
 
 
 
 


 