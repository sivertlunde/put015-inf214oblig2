The Three Mesquiteers (film)
{{Infobox film
| name           = The Three Mesquiteers
| image          = 
| caption        =  Ray Taylor
| producer       = Nat Levine
| writer         = Jack Natteford Charles R. Condon Bob Livingston
| music          = 
| cinematography = William Nobles
| editing        = William P. Thompson
| studio         = Republic Pictures
| distributor    = 
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         = 
}} Western "The Three Mesquiteers" Bob Livingston Ray "Crash" Ray Taylor.

==Cast==
*Bob Livingston as Stony Brooke
*Ray Corrigan as Tucson Smith
*Syd Saylor as Lullaby Joslin
*Kay Hughes as Marian Bryant
*J.P. McGowan as Brack Canfield
*Al Bridge as Olin Canfield
*Frank Yaconelli as Pete (Italian vet)
*John Merton as Bull (chief henchman)
*Gene Marvey as Bob Bryant
*Milburn Stone as John (a vet)
*Duke York as Chuck (one-armed vet)
*Nina Quartero as Rosita (waitress) (billed as Nena Quartaro)
*Allen Connor as Milt (one-legged vet)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 