Scattergood Rides High
{{Infobox film
| name           = Scattergood Rides High
| image          = Scattergood Rides High poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Christy Cabanne John E. Burch (assistant)
| producer       = Jerrold T. Brandt 
| screenplay     = Michael L. Simmons
| starring       = Guy Kibbee Jed Prouty Dorothy Moore Charles Lind Kenneth Howell
| music          = Paul Sawtell
| cinematography = Jack MacKenzie
| editing        = Henry Berman 
| studio         = Pyramid Productions
| distributor    = RKO Pictures
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Scattergood Rides High is a 1942 American comedy film directed by Christy Cabanne and written by Michael L. Simmons. It is the sequel to the 1941 film Scattergood Meets Broadway. The film stars Guy Kibbee, Jed Prouty, Dorothy Moore, Charles Lind and Kenneth Howell. The film was released on May 8, 1942, by RKO Pictures.   

==Plot==
 

== Cast ==
*Guy Kibbee as Scattergood Baines
*Jed Prouty as Mr. Van Pelt
*Dorothy Moore as Helen Van Pelt
*Charles Lind as Dan Knox
*Kenneth Howell as Phillip Dane
*Regina Wallace as Mrs. Van Pelt
*Frances Carson as Mrs. Dane
*Arthur Aylesworth as Cromwell
*Paul White as Hipp
*Philip Hurlic as Toby
*Walter Baldwin as Martin Knox
*Lee Phelps as Trainer 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 

 