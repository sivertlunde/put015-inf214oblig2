Sella Turcica (film)
{{Infobox film
| name           = Sella Turcica
| image          = SellaTurcicaToeTag.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD released by Toetag Pictures
| film name      = 
| director       = Fred Vogel
| producer       = Fred Vogel   Shelby Vogel
| writer         = Fred Vogel   Don Moore   Shelby Vogel
| screenplay     = 
| story          = Fred Vogel   Don Moore   Shelby Vogel   Jerami Cruise
| based on       = 
| narrator       = 
| starring       = Allie Nickel   Jade Risser   Harvey Daniels   Camille Keaton   Sarah Thornton   Sean P. McCarthy   Damien A. Maruscak
| music          = Mike Hammer
| cinematography = Gabe Spangler
| editing        = Jason Kollat
| studio         = Toetag Pictures
| distributor    = Toetag Pictures
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = $20,000
| gross          = 
}}

Sella Turcica is a 2010 horror film written and directed by Fred Vogel, and co-written by Shelby Vogel and Don Moore.

== Plot ==
 LRMC in Germany, they were found to be suffering from unexplainable physical impairments. Brad lost the ability to walk, taste and see in color, and is plagued by frequent and severe headaches that are exacerbated by high-pitched noise.

Brad is sent home to his family, where his condition causes some tension, something not helped by his sister Ashleys insensitive boyfriend, Gavyn. As hours pass, Brads body and mind deteriorate. He has periods of unresponsiveness, suffers from seizures, a wound on his right foot worsens, he becomes more gaunt, his eyes and tongue swell, his fingernails and teeth yellow, and he begins expelling a black fluid from his ears, mouth and anus. In the morning after his return, Brad is found by his mother, Karmen, ingesting a large amount of salt in the kitchen, having apparently entered the room under his own power. As his mother gets his wheelchair from the parlor, Brad returns to his bed, and becomes semi-catatonic, refusing to move and denying that he needs to go to a hospital when his brother Bruce suggests it.

When Gavyn stops by, his blunt comments about Brads condition and his relationship with Ashley sparks a fight between him and Bruce. After Bruce goes outside to cool off, and gets into an argument with his mother in the yard, Brad (who has just killed and ate the family dog, Fulci) wheels into the kitchen, stands up, and attacks Gavyn. Gavyns screams alert Bruce and Karmen, who find him dead from severe head trauma, and Brad convulsing on the floor. As Bruce tries to revive Brad, Ashley (who had just arrived home from dance practice) leads their mother out of the room. As soon as Ashley and Karmen leave, Brad springs to life, rips Bruces lower jaw off, and clumsily stumbles after his mother and sister, emitting animalistic noises.

Brad finds Ashley and Karmen, and kills the former by punching through a door and impaling her through the neck on the splintered wood, subsequently ripping her head off. As Brad struggles to get to her, Karmen bludgeons him with a metal wall ornament, prompting a slug-like creature to burst out of his sella turcica (seemingly confirming Brads earlier, sarcastic theory that he and his squad were abducted and experimented on by aliens). Karmen beats Brad and the entity to death, and the film ends with home movies of the Robacks, and Karmens husband returning home to discover his wife has hanged herself in the garage.

== Cast ==

* Camille Keaton as Karmen Roback
* Damien A. Maruscak as Sergeant Bradley Adam Roback
* Harvey Daniels as Gavyn
* Allie Nickel as Tina Roback
* Sarah Thornton as Heidi
* Fred Vogel as The Driver
* Joe Cruise as Richard Roback
* Scarlett McCarthy as Sherri Roback
* Jade Risser as Ashley Roback
* Sean P. McCarthy as Bruce Roback

== Reception ==

Moria awarded Sella Turcica two and a half stars out of five, finding the characters well-rounded and the drama and performances compelling, but the finale unsatisfying, stating it "is the point where the film goes from a well-established character ensemble about people reacting to a family member becoming ill to a predictable gore bloodbath" and that the effects were "impressively gore-drenched but not always the most convincing".  The exploitation film database The Worldwide Celluloid Massacre described Sella Turcica as "pretty good horror" with an interesting storyline, passable acting, and impactful gore scenes. 

Arrow in the Head had a lukewarm reaction to Sella Turcica, writing "It tries really hard. It does. Unfortunately its dramatic portion is overwrought and its gore freak-out ending feels like a cheat to make the movie more memorable than it deserves to be, even if said gore is fantastic".  The film was called "rotten" and "dead" by Soiled Sinema, which criticized the acting and felt the only worthwhile element of the "dreary production" was the ending massacre. 

== See also ==

*Deathdream, a similar film released in 1972.
*List of zombie short films and undead-related projects

== References ==

 

== External links ==

*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 