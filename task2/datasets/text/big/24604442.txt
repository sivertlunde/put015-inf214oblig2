Kaliyattam
{{Infobox film
| name           = Kaliyattam
| image          = Kaliyattam.jpg
| image_size     = 
| alt            = 
| caption        = VCD Poster cover
| director       = Jayaraaj
| producer       = K. Radhakrishnan 
| screenplay     = Balram Mattanoor
| based on       = William Shakespeares Othello
| narrator       =  Lal Manju Warrier Biju Menon Kaithapram Rajamani  (Film score) 
| cinematography = M J Radhakrishnan
| editing        =  
| studio         = Jayalakshmi Films 
| distributor    = Surya Cine Arts 
| released       = 1997
| runtime        = 130 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 adaptation of Lal played Paniyan, the equivalent to Iago. Manju Warrier played Thamara, the Desdemona version. And Biju Menon played Kanthan, Michael Cassio|Cassios role.
 National Film Best Actor, Best Director for their work on the film.  

==Plot==
This is an adaptation of Shakespeares Othello, revolving around Kannan Perumalayan (Suresh Gopi)), a Theyyam artist who corresponds to Othello (character)|Othello, and Thamara (Manju Warrier), the beautiful daughter of the village head. While Unni Thampuran hates Kannan because he had a crush on Thamara, and Paniyan, (Lal (actor)|Lal) who plays a Koomali, covets the role of Theechamundi which Perumalayan holds. Paniyan plants the seeds of doubt about Thamaras fidelity in Kannans mind, making him suspect that Thamara and his assistant Kanthan (Biju Menon) are having an affair. Kannan spots a silk robe which he had presented to Thamara in Kanthans hands.

Kannan, out of grief and anger, takes Thamaras life by suffocating her with a pillow. On the same night, Paniyan plans to get Kanthan killed by Unni Thampuran, but the plan goes awry and Thamburan is killed. Amidst these events, Kannan is told of his mistake by Paniyans wife Cheerma, before Paniyan murders her. Kannan overpowers Paniyan, crushing his legs with a stone, and allows him to live the rest of his life crippled. Kannan Perumalayan gives the Theechamundi role to Kanthan and commits suicide in the Theyyam ritual fire.

==Cast==

* Suresh Gopi as Kannan Perumalayan (Othello (character)|Othello) Lal as Paniyan (Iago)
* Manju Warrier as Thamara (Desdemona)
* Biju Menon as Kanthan (Michael Cassio|Cassio)
* Bindu Panicker as Cheerma (Emilia (Othello)|Emilia)
* Narendra Prasad as Thamburan (Brabantio)

==Soundtrack==
The music and lyrics were composed and written by Kaithapram Damodaran Namboothiri.

{| class="wikitable" border="1"
! Song || Playback 
|-
| "Ennodenthinee Pinakkam" 
| Bhavana Radhakrishnan
|-
| "Vannathi Puzhayude"
| K. J. Yesudas
|-
| "Velikku Veluppaankaalam"
| K. J. Yesudas
|-
| "Ennodendinee Pinakkam"
| K. J. Yesudas
|-
| "Kathivanoor Veerane"
| Kallara Gopan
|-
| "Kathivanoor Veerane"
| Sreeja
|-
| "Ezhimalayolam"
| Kaithapram
|-
| "Sree Raagam Paadum Veene" Sujatha
|-
| "Paadathe Paadunna Raagam"
| M. G. Sreekumar
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 