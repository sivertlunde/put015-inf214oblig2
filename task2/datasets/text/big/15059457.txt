Aag Aur Shola
{{Infobox film
| name           = Aag Aur Shola 
| image          = 
| caption        =
| director       = K. Bapaiah
| producer       = Prasan Kapoor
| writer         =  Mandakini  Sridevi  Shakti Kapoor  Kadar Khan 
| music          = Laxmikant-Pyarelal
| cinematography = 
| editing        = 
| distributor    = 
| released       =  1986
| runtime        = 
| country        = India
| language       = Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          =
}}

Aag Aur Shola (English translation - Fire and Flames) is 1986 Hindi language Movie directed by K. Bapaiah and starring Jeetendra, Mandakini (actress)|Mandakini, Sridevi, Shakti Kapoor, Kadar Khan. 

==Plot==
Whenever Inspector Ram, in charge of police station in Bombay, try to arrest some criminal a gangster named Nagesh help these arrested criminals. Nagesh has political influence. Nageshs sister Usha is in love with Raju. When Nagesh learn this, he beats up Raju. Raju survives. He and his mother approach Vishal for help.

==Cast==
 
*Jeetendra as Vishal Mandakini as Usha
*Sridevi as Aarti
*Shakti Kapoor as Nagesh
*Kadar Khan as College Lecturer
*Asrani
*Jagdeep
*Viju Khote as Havaldar
*Suresh Oberoi as Aartis mamaji
*Shoma Anand as Ushas elder sister Bindu as Aartis mamiji
*Aruna Irani as Rajus Bua
*Ashish Channa
*Manik Irani as Jumbo
*Guddi Maruti as Collegian
*Yunus Pervez as Aartis groom
*Satish Shah as Vidyasagar, College Student
*Dalip Tahil as Inspector Ram
 

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aaj Subah Jab Main Jaga"
| Mohammed Aziz, Lata Mangeshkar
|-
| 2
| "Nakhrewali Maare Naksa"
| Mohammed Aziz
|-
| 3
| "Mithi Mithi"
| Manhar Udhas, Asha Bhosle
|-
| 4
| "Barsa Re Barsa"
| Manhar Udhas, Anuradha Paudwal
|-
| 5
| "Ek Ladki Jiska Naam"
| Mohammed Aziz, Kavita Krishnamurthy
|}
==References==
 

== External links ==
*  

 
 
 
 

 