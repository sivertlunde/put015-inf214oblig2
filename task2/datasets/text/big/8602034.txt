Carnival Night
{{Infobox film
| name           = The Carnival Night
| image          = Carnival Night.jpg
| image size     = 210px
| caption        = Original film poster by Mikhail Heifits, 1956.
| director       = Eldar Ryazanov
| producer       =
| writer         = Boris Laskin Vladimir Polyakov
| narrator       =
| starring       = Igor Ilyinsky Lyudmila Gurchenko
| music          = Anatoli Lepin
| cinematography = Arkadi Koltsaty
| editing        =
| distributor    =
| studio         = Mosfilm
| released       = 1956 (Soviet Union) November 2, 1957 (U.S.)
| runtime        = 78 min.
| country        = Soviet Union
| language       = Russian
| budget         =
| gross          =
| followed_by    =
}}
 Soviet musical Soviet box office leader of 1956 with a total of 48.64 million tickets sold. 

==Plot==

It is New Years Eve and the employees of an Economics Institute are ready with their annual New Years entertainment program. It includes a lot of dancing and singing, jazz band performance and even magic tricks. Suddenly, an announcement is made that a new director has been appointed and that he is arriving shortly. Comrade Ogurtsov arrives in time to review and disapprove of the scheduled entertainment. To him, holiday fun has a different meaning. He imagines speakers reading annual reports to show the Institutes progress over the year, and, perhaps, a bit of serious music, something from the Classics, played by the Veterans Orchestra. 

Obviously, no one wants to change the program a few hours before the show, much less to replace it with something so boring! Now everyone has to team up in order to prevent Ogurtsov from getting to the stage. As some of them trap Ogurtsov one way or another, others perform their scheduled pieces and celebrate New Years Eve.

== Cast ==
* Igor Ilyinsky as Serafim Ivanovich Ogurtsov
* Lyudmila Gurchenko as Lena Krylova Yuri Belov as Grisha Koltsov
* Andrei Tutyshkin as Fyodor Petrovich Mironov, bookkeeper
* Olga Vlasova as Adelaida Kuzminichna Ramashkina
* Tamara Nosova as Tosya Burigina
* Georgi Kulikov as Seryozha Usikov
* Gennadi Yudin as Jazz band Conductor
* Vladimir Zeldin as Clown
* B. Nemker as Clown Sergei Filippov as Comrade Nekadilov, lecturer
* Shmelev Sisters as Singing Waitresses
* V. Gusakov as Dancer
* Y.Gusakov as Dancer
* Tamara Kruchova as acrobat girl

==Crew==
*Director: Eldar Ryazanov
*Writers: Boris Laskin, Vladimir Polyakov
*Cinematography: Arkady Koltsaty
*Composer: Anatols Liepiņš (Anatoly Lepin)
*Music orchestration: Eddie Rosner, also jazz band leader (uncredited)

==External links==
* 
* 
*   and  

 

 
 
 
 
 
 