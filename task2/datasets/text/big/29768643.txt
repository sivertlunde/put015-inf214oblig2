Drop Squad
{{Infobox film
| name           = Drop Squad
| image size     = 
| image	=	Drop Squad FilmPoster.jpeg
| alt            = 
| caption        = 
| director       = David C. Johnson
| producer       = Butch Robinson Shelby Stone
| writer         = 
| screenplay     = David C. Johnson & Butch Robinson
| story          = David Taylor; David C. Johnson & Butch Robinson (screen story)
| based on       = 
| narrator       = 
| starring       = Eriq La Salle Vondie Curtis-Hall Ving Rhames Kasi Lemmons
| music          = Mike Bearden
| cinematography = Ken Kelsch
| editing        = Kevin Lee
| studio         = 
| distributor    = Gramercy Pictures
| released       =  
| runtime        = 
| country        = US
| language       = English
| budget         = 
| gross          = $734,693
}}
 American film released in 1994. The film depicts a team of African Americans who kidnap fellow black people who have betrayed their community and seek to "Deprogramming|deprogram" them so that they will change their ways.    In the film, the squads acronym DROP stands for "Deprogramming and Restoration of Pride".    The film has been described as " art thriller, part social satire". 

The film was based in part on The Session, a 45-minute film which director David Johnson had made in 1988 on a budget of $20,000,    and ultimately derived from a short story by David C. Taylor titled "The Deprogrammer".    Johnson described the differences between the two films as follows: "The short film was basically satire, an absurdist piece .... D.R.O.P. Squad, on the other hand, is realism. The characters have more at stake." 

Spike Lee served as executive producer for the film.   

==Plot==
The film portrays an advertising executive, Bruford Jamison (Eriq La Salle) who is in charge of the "minority development division" for an advertising agency.   Among the ad campaigns he is involved with is one for a malt liquor called "Mumblin Jack", whose billboard depicts a woman in a skimpy bikini straddling a bottle, with the slogan "It Gits Ya Crazy!"   Another ad campaign depicted in the film is a commercial filled with racial stereotypes (in which Spike Lee has a cameo) for a fried chicken restaurants Gospel-Pak, which offers a Bible verse printed on every napkin.     Brufords sister Lenora (Nicole Powell) calls in the Drop Squad to deprogram him.  Bruford winds up being subjected to three weeks of psychological and physical brutality.  Among the other persons who are shown being subjected to the deprogramming are a corrupt politician and a drug dealer. 

The film also depicts a conflict among the members of the Drop Squad as to the tactics they should use. Rocky (Vondie Curtis-Hall), the squads leader, believes in using only nonviolent tactics,  such as "subjecting them to a barrage of slides, posters, slogans and family photographs in hopes of restoring their sense of community",  while Garvey (Ving Rhames) believes that harsher methods have become necessary. 

==Response==
The total North American box office gross for the film was $734,693. 

Drop Squad has been suggested as a possible influence on Spike Lees 2000 film Bamboozled.  

==References==
 

==External links==
* 

 