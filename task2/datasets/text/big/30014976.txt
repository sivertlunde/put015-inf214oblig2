Geronimo (1993 film)
 
{{Infobox television film
| name           = Geronimo
| image          = Geronimo1993.jpg
| caption        = Poster
| alt            =  Roger Young
| producer       = 
| writer         = J.T. Allen
| starring       = Joseph Runningfox Nick Ramus Michael Greyeyes August Schellenberg Patrick Williams
| cinematography = Donald M. Morgan
| editing        = Millie Moore
| studio         = Turner Pictures TNT
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 TNT television movie starring Joseph Runningfox in the title role.   First Nations actor Jimmy Herman appeared in this motion picture as the Old Geronimo. It also features Apache/Mexican actor Adan Sanchez who was later on the PBS series Wishbone (TV series)|Wishbone.   
It debuted on television five days before the theatrical release of  . 

It was shot in Tucson, Arizona.

==Plot==
 

==Cast==

*Joseph Runningfox as Geronimo
*Nick Ramus 
*Michael Greyeyes as Juh
*Tailinh Agoyo as Alope (as Tailinh Forest Flower)
*Jimmy Herman as Old Geronimo 
*August Schellenberg as Cochise
*Michelle St. John 
*Eddie Spears as Ishkiye
*Cody Lightning as Young Daklugie
*Ray Geer as Teddy Roosevelt 
*Annie Olson as Mrs. Roosevelt

==See also==
* Geronimo

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 