Count Five and Die
{{Infobox film
| name           = Count Five and Die
| image          = Count Five and Die.jpg
| image size     =
| caption        = 
| director       = Victor Vicas
| producer       = Earnest Gartside
| writer         = Jack Seddon and David Pursall
| narrator       = 
| starring       = Jeffrey Hunter Nigel Patrick Annemarie Düringer David Kossoff. 
| music          = John Wooldridge	 	  Arthur Grant Russell Lloyd 20th Century Fox
| released       = 1957
| runtime        = 92 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded by    = 
| followed by    = 
}} Arthur Grant.  

== Plot ==

Filmed in London, the story concerns the feeding of misinformation to German Intelligence about the exact location of the D-Day landings in 1944. Captain Bill Ranson (Jeffrey Hunter) and Major Julien Howard (Nigel Patrick) are two allied intelligence officers posing as documentary film makers in the occupied Low Countries. Their concerns centre on the credentials of Rolande Hertog (Annemarie Düringer), a new recruit to the intelligence service recently arrived from Holland. 

==Cast==
*Jeffrey Hunter	 ... 	Captain Bill Ranson
*Nigel Patrick	... 	Major Julien Howard
*Annemarie Düringer	... 	Rolande Hertog
*David Kossoff	... 	Dr. Mulder
*Rolf Lefebvre   	... 	Hans Faber, chief spy
*Larry Burns            ... 	Martins, building porter-spy
*Anthony Ostrer		
*Claude Kingston	... 	Willem Mulder
*Philip Ray		
*Robert Raglan	... 	Lt. Miller
*Peter Prouse	... 	Sgt. Bill Parrish Philip Bond	... 	Piet van Wijt
*Otto Diamant	... 	Mr. Hendrijk
*Marianne Walla	... 	Mrs. Hendrijk
*Beth Rogan	... 	Mary Ann Lennig

== References ==
 

== External links ==
* 

 
 
 
 
 
 
 
 
 
 
 
 


 
 