The Undead (film)
 
{{Infobox film
| name           = The Undead
| image          = Theundeadposter.jpg
| caption        = film poster by Albert Kallis
| director       = Roger Corman
| producer       = Roger Corman
| writer         = Charles B. Griffith Mark Hanna
| narrator       = Pamela Duncan Richard Garland Allison Hayes Val Dufour Mel Welles
| music          = Ronald Stein
| cinematography = William Sickner Frank Sullivan
| distributor    = American International Pictures
| released       =  
| runtime        = 75 min.
| country        = United States English
| budget         = $70,000 
}}
 1957 horror Pamela Duncan, Allison Hayes, Richard Garland and Val Dufour. It follows the story of  prostitute, Diana Love (Duncan), who is put into a hypnotic trance by psychic Quintis (Dufour), thus causing her to regress back to a previous life.  Hayes later starred in Attack of the 50 Foot Woman (1958). The film was released by American International Pictures as a double feature with Voodoo Woman.

==Plot==
The psychic sends the prostitute back in time to find out about her past-life experiences. She goes back as Helene, a woman from the Middle Ages who is to die at dawn under suspicion of being a witch.  In an attempt to save Diana (the prostitute) and keep all of time from being distorted, Quintis (the psychic) goes back in time to convince Helene to let herself be killed. If she avoids her death, it will change history.   

==Cast== Pamela Duncan as Diana Love/Helene
*Richard Garland as Pendragon
*Allison Hayes as Livia
*Val Dufour as Quintus Ratcliff
*Mel Welles as Smolkin
*Dorothy Neumann as Meg-Maud
*Billy Barty as The Imp
*Bruno Ve Sota as Scroop
*Aaron Saxon as Gobbo
*Richard Devon as Satan

==Production==
 The Search for Bridey Murphy by Morey Bernstein was made into a film in 1956. However by the time The Undead was being made, the popularity of reincarnation was starting to dwindle. Therefore Corman decided that they needed to change it up a little and added the time travel elements of Quintis, and a title change.

Charles B. Griffith originally wrote the script in iambic pentameter which he says Corman loved at first but then grew cold feet about and asked to be changed. 
 Allied Artists. Drama: Preminger Places Saint Joan First in New Deal; Hypnosis Rage Goes On
Schallert, Edwin. Los Angeles Times (1923-Current File)   04 May 1956: B9. 

The movie was filmed in a converted supermarket, and was completed in only six days. Its original title was The Trance of Diana Love.   The bats that the imp and witch continually change into were left over from another Corman movie, It Conquered the World.

==Legacy== eighth season where they comment on everything from small sets, tossing cats, bad dialog, and the horrors of having seen other Corman movies.

==References==
 

==External links==
* 
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 