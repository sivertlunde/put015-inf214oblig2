Dusk to Dawn
 
{{Infobox film
| name           = Dusk to Dawn
| image          = Dusk to Dawn (1922) - Mulhall & Vidor.jpg
| caption        = Film still with Florence Vidor and Jack Mulhall
| director       = King Vidor
| producer       = King Vidor
| writer         = Frank Howard Clark Katherine Hill
| starring       = Florence Vidor
| cinematography = George Barnes
| editing        = 
| distributor    = Associated Exhibitors
| released       =  
| runtime        = 60 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 silent drama film directed by King Vidor. It is unknown whether any recording of the film survives; it may be a lost film.      

==Plot==
An Indian maid and American girl (both played by Florence Vidor) share a single soul which shifts between them each day when they are awake. {{cite journal
 | title =The Shadow Stage
 | journal =Photoplay
 | volume =22
 | issue =6
 | page=67
 | publisher =Photoplay Publishing Company
 | location =Chicago
 | date =Nov 1922
 | url =https://archive.org/details/photoplayvolume222chic
 | accessdate =Dec 9, 2013 }} 

==Cast==
* Florence Vidor as Marjorie Latham / Aziza
* Jack Mulhall as Philip Randall
* Truman Van Dyke as Ralph Latham
* James Neill as John Latham
* Lydia Knott as Mrs. Latham
* Herbert Fortier as Mark Randall
* Norris Johnson as Babette
* Nellie Anderson as Marua
* Sidney Franklin as Nadar Gungi
* Peter Burke as Itjah Nyhal Singh

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 