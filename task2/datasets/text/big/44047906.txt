Anthappuram
{{Infobox film
| name = Anthappuram (1980)
| image =
| caption =
| director = K. G. Rajasekharan
| producer = BVK Nair & Sasikumar for Mangalya Movie Makers
| writer = 
| screenplay = K. G. Rajasekharan
| starring = Prem Nazir, Jayan, Seema, Ambika, Bhasi, Jose Prakash, Kaviyoor Ponnamma etc
| music = Shankar Ganesh
| cinematography = Indu
| editing = VP Krishnan
| studio = Mangalya Movie Makers
| distributor = Mangalya Movie Makers
| released =  
| country = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film, directed by K. G. Rajasekharan and produced by BVK Nair. The film stars Prem Nazir, Ambika (actress)|Ambika, Jayan and Kaviyoor Ponnamma in lead roles. The film had musical score by Shankar Ganesh.   

==Cast==
  
*Prem Nazir as Vijayan  Ambika as Usha 
*Jayan as Vasu 
*Kaviyoor Ponnamma as Bhavani 
*Adoor Bhasi as Nair 
*Jose Prakash as Sekhara Pillai 
*Anandavally 
*Baby Vandana 
*Bindulekha
*Jagannatha Varma as Balakrishna Pillai 
*Justin
*Kamal Roy as Child Artist 
*Master Suresh
*Nellikode Bhaskaran as Asan  Seema as Meenu  Sudheer as Susheelan 
*Vanchiyoor Radha 
*Manavalan Joseph as Policeman 
*Thodupuzha Radhakrishnan
 

==Soundtrack==
The music was composed by Shankar Ganesh and lyrics was written by Mankombu Gopalakrishnan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Gopura Vellari Praavukal Naam || Ambili, Chorus || Mankombu Gopalakrishnan || 
|- 
| 2 || Maanya Mahaa Janangale || Vani Jairam || Mankombu Gopalakrishnan || 
|- 
| 3 || Mukhakkuru Kavilinayil || K. J. Yesudas || Mankombu Gopalakrishnan || 
|- 
| 4 || Narayana || Ambili || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 
 


 