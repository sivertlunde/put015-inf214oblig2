Krabat (film)
{{Infobox film
| name           = Krabat
| image          = Krabat-marco-kreuzpaintner.jpg
| caption        = Theatrical release poster
| director       = Marco Kreuzpaintner
| producer       = Jakob Claussen Uli Putz Bernd Wintersperger Thomas Wobke
| writer         = Marco Kreuzpaintner  
| based on       =  
| starring       = David Kross Daniel Brühl Christian Redl Robert Stadlober Paula Kalenberg Daniel Steiner Hanno Koffler
| music          = Annette Focks
| cinematography = Daniel Gottschalk
| editing        = Hansjoerg Weissbrich
| studio         = 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 115 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}} novel of the same name. The plot is about a boy, Krabat (played by David Kross), who learns black magic from a sorcerer (played by Christian Redl). A DVD-Video encode of the film is distributed in the United Kingdom as Krabat and the Legend of the Satanic Mill. 

It premiered in the US at the Seattle International Film Festival in 2009.   

==Plot==
When the Plague sweeps across Europe after the Thirty Years War a boy named Krabat (David Kross of "The Reader") is left without family, food, or hope. An old Mill Keeper takes him in as an apprentice. There are eleven other boys working at the mill, and Krabat develops a friendship with one of them, a young man named Tonda (Daniel Brühl).
Soon, Krabat learns that the apprentices are also taught dark sorcery by the master, and one of the rituals (during Easter) lead to an excursion to the nearby village Schwarzkolm where Krabat meets a young girl and falls in love with her. There, Tonda also talks to one of the girls; both seem to be in love with each other. Later, Tonda warns Krabat that the master must never know the name of his girl.   

One day, while protecting the nearby village from soldiers, Tonda makes an error and his girls name (Worschula) is revealed to the master. The next day, Worschula turns up in the creek, dead. Krabat mistakenly blames Lyschko, another apprentice. Tonda becomes a recluse and anticipates the end of the year.
Krabats first Silvester (New Years Eve) brings to light the true horror of the mill. Every Silvester, one of the boys must be sacrificed so the master may remain young. And so at midnight, Krabats best friend Tonda is viciously murdered, and when Krabat tries to help him he is stopped by the other boys who tell him that "there is nothing we can do".  Before he dies, Tonda tells Krabat there is another boy in the mill Krabat can confide in. He also tells Krabat to take two sacks of flour to the village.

Krabat is distraught over Tondas death, but does as he is told. Bringing the sacks of flour to a tree near the village, Krabat once again meets the girl he first met while protecting the village. He is in love, but does not let the girl tell him her name, fearing for her life. Instead, he calls her Kantorka (Choir leader).
During the ritual at Easter night, he goes to the village to meet her, this time along with a boy called Juro who appears to be mentally disabled and not able to learn the trade or properly do magic. When Juro tells Krabat that they must leave and go back to the mill, Krabat insists that he will stay with Kantorka. Juro then uses powerful magic to convince Krabat to come back with him, revealing that he is in truth highly intelligent and powerful, even able to change the weather. Juro promises Krabat that he will help him escape the master, and tells him that his girl must ask for him on the first day of the year. 
Krabat tells Kantorka that she must do so, and she agrees and gives Krabat a lock of her hair, telling him to have another boy deliver it to her when the time is right.

When Krabat returns, a series of climactic events are set in motion.

==Cast==
*Krabat - David Kross
*Tonda - Daniel Brühl
*The Master - Christian Redl
*Lyschko - Robert Stadlober
*Kantorka - Paula Kalenberg
*Juro - Hanno Koffler
*Worschula - Anna Thalbach
*Michal - Charly Hübner
*Merten - Moritz Grove
*Hanzo - Tom Wlaschiha
*Andrusch - Sven Hönig
*Staschko - Stefan Haschke
*Lobosch - David Fischbach
*Petar - Daniel Steiner
*Kubo - Tom Lass
*Kito - Daniel Fripath
*Baro - Ionut Baias
*Godfather Death - Mac Steinmeier
*Krabats Mother - Carmen Ungureanu

==Awards==
Krabat was nominated for the Deutscher Filmpreis in 2009 in the categories Best Production Design, Best Music and Best Sound Design. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 