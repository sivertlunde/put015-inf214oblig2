Swami (1977 film)
{{Infobox film
| name           = Swami
| image          = Swami.jpg
| image_size     = 159 px
| caption        =
| producer       = Jaya Charavarty
| director       = Basu Chatterjee
| Music Director = Rajesh Roshan
| writer         = Manu Bhandari dialogue Basu Chatterjee writer Sharat Chandra Chatterji (book)
| Lyrics         = Ameet Khanna
| narrator       =
| starring       = Shabana Azmi Vikram Girish Karnad
| music          = Rajesh Roshan
| cinematography = K. K. Mahajan
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =
}}

Swami is a 1977 Hindi film directed by Basu Chatterjee and produced by Jaya Chakravarty (mother of Hema Malini). 

The film stars Shabana Azmi, Vikram, Girish Karnad, Utpal Dutt.  Hema Malini and Dharmendra make a guest appearance together in the film. The films music is by Rajesh Roshan.

==Plot==
Saudamini (Shabana Azmi) is a bright village girl with academic ambitions and an appetite for literature and philosophy. Her intellectual uncle (Utpal Dutt) indulges her brainy bent, encouraging her studies and running interference between Mini and her mother (Sudha Shivpuri), a pious widow whose only concern is to see Mini married, and quickly.  Mini has a nascent love affair with her neighbor Narendra (Vikram), the zamindars son, a student in Calcutta who on his frequent visits brings her Victorian literature, listens raptly to her discourse, and is bold enough to kiss her opportunistically when they are caught together in a rainstorm.  Circumstances  conspire against Mini and Narendra, though, and soon Mini finds herself married against her wishes to a wheat trader Ghanshyam (Girish Karnad) from a neighboring village.  Plunged into despair, Mini struggles to become accustomed to life in her unwanted marriage and her new home, where Ganshyams stepmother seems to favor Minis sisters-in-law and where her new husband treats her with a patience that she finds perplexing.  

==Soundtrack==
The music is by veteran music director, Rajesh Roshan, and he received a Filmfare nomination for composing some memorable numbers, one sung by vocalist Yesudas, "Kaa Karoon Sajni" and Lata Mangeshkars, "Pal Bhar Mein Yeh Kya Ho Gaya".

===Tracklist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Ka Karoon Sajni Aaye Na Baalam"
| Yesudas
|-
| 2
| "Pal Bhar Mein Yeh Kya Ho Gaya"
| Lata Mangeshkar
|-
| 3
| "Yaadon Mein Woh"
| Kishore Kumar
|-
| 4
| "Aaj Ki Raat Kuchh Hogi Aisi Baat"
| Asha Bhosle, Yesudas
|}

==Awards and nominations==
* 1977  
* 1977  
* 1977  
* 1977 Filmfare Nomination for Best Picture
* 1977 Filmfare Nomination for Best Music-Rajesh Roshan
* 1977 Filmfare Nomination for Best Male Playback Singer-Yesudas for the song Ka Karoon Sajni Aye Na Baalam 

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 