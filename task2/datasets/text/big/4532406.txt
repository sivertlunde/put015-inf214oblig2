The Black Pimpernel
{{Infobox Film
| name           = The Black Pimpernel
| image          = Svartanejlikan.jpg
| image_size     = 
| caption        = 
| director       = Ulf Hultberg
| producer       = 
| writer         = Bob Foss
| narrator       = 
| starring       = Michael Nyqvist; Lisa Werlinder and Claire Ross-Brown
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 14 September 2007
| runtime        = 
| country        = Sweden
| language       = 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Swedish drama film directed by Ulf Hultberg and starring Michael Nyqvist and Lisa Werlinder. The film also features Claire Ross-Brown in a minor part.

The film is about Harald Edelstam, Sweden’s ambassador to Chile, who after the military coup of Augusto Pinochet in 1973, managed to save the lives of more than 1,300 people by taking them to his embassy and transporting them to Sweden.

His name comes from the fictional hero The Scarlet Pimpernel who saved many lives during the French Revolution.

The film was shot in Chile in the spring of 2006 and opened September 14, 2007.

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 


 