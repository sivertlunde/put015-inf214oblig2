The Last Meeting
 
 
{{Infobox film
| name           = The Last Meeting
| image          = 
| caption        = 
| director       = Antonio Eceiza
| producer       = Elías Querejeta
| writer         = Antonio Eceiza Elías Querejeta
| starring       = Antonio Gades
| music          = 
| cinematography = Luis Cuadrado
| editing        = Pablo González del Amo
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

The Last Meeting ( ) is a 1967 Spanish drama film directed by Antonio Eceiza. It was entered into the 1967 Cannes Film Festival.   

==Cast==
* Antonio Gades - Antonio Esteve
* Daniel Martín (actor) - Juan
* Calderas - tango singer
* Francisco Carames
* Pepe de la Peña - dancer
* Perico el del Lunar - guitarist
* Emilio de Diego - guitarist
* Enrique Esteve - dancer
* Cristina Hoyos - dancer (as Cristina)
* José Luna El Tauro - dancer
* Juan Maya - guitarist
* José Meneses - martinete singer
* Daniel Moya - guitarist
* Félix Ordóñez - dancer

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 