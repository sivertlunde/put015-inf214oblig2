Urangatha Sundary
{{Infobox film 
| name           = Urangatha Sundary
| image          =
| caption        =
| director       = P. Subramaniam
| producer       = P Subramaniam
| writer         = Sree
| screenplay     = Sree Sathyan Rajasree K. V. Shanthi Jayabharathi
| music          = G. Devarajan
| cinematography = ENC Nair
| editing        = N Gopalakrishnan
| studio         = Neela
| distributor    = Neela
| released       =  
| country        = India Malayalam
}}
 1969 Cinema Indian Malayalam Malayalam film, directed and produced by P. Subramaniam . The film stars Sathyan (actor)|Sathyan, Rajasree, K. V. Shanthi and Jayabharathi in lead roles. The film had musical score by G. Devarajan.    It is a remake of Alfred Hitchcocks film Rebecca (1940).

==Plot==
Prabhavathi meets rich Vikraman Karthavu and they both fall in love and get married. Vikraman brings Prabhavathi to his home- A huge imposing mansion in middle of Moors. Prabhavathi learns from servants about Vikramans first wife Vilasini who died in mysterious circumstances about a year ago. The housekeeper who was also the nanny of vilasini is visibly disturbed by this and is cold towards prabhavathi. vikraman leaves on a business trip for few weeks and prabavathi is left to herself. This is when she encounters supernatural phenomena in the mansion and is haunted by the memories and spirit of vilasini. Recovering from shock prabavathi decides to do her own investigation of vilasinis mysterious death. One by one she uncovers shocking dark secrets about vilasini, vikraman and various other people.

==Cast==
  Sathyan as Vikraman Kartha
*Rajasree as Vilasini
*K. V. Shanthi as Prabhavathi
*Jayabharathi as Madhumathi
*Adoor Pankajam as Madhavi
*Bahadoor as Panicker
*K. P. Ummer as Sudhakaran
*Karamana Janardanan Nair as Advocate John
*Nellikode Bhaskaran as Purushothaman
*Pankajavalli as Ammavi
*S. P. Pillai as Madman
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chandanakkallil || P Susheela || Vayalar Ramavarma || 
|-
| 2 || Enikkum Braanthu || Kamukara || Vayalar Ramavarma || 
|-
| 3 || Gorochanam Kondu || P. Leela || Vayalar Ramavarma || 
|-
| 4 || Paalaazhi Madhanam || P Susheela || Vayalar Ramavarma || 
|-
| 5 || Paalaazhi Madhanam || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 6 || Paathiraappakshikale || P Susheela || Vayalar Ramavarma || 
|-
| 7 || Priyadarshini || K. J. Yesudas, B Vasantha || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 
 
 