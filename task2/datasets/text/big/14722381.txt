Fear City
{{Infobox film name       = Fear City  image      = FearCityPoster.jpg caption    = Film poster director   = Abel Ferrara writer  Nicholas St. John starring   = Tom Berenger Billy Dee Williams Jack Scalia Melanie Griffith Michael V. Gazzo producer = Bruce Cohn Curtis cinematography = James Lemmo 	 editing = Jack W. Holmes Anthony Redman distributor = Aquarius Releasing studio = Zupnik-Curtis Enterprises Rebecca Productions Castle Hill Productions budget =  released =   runtime  = 95 minutes language = English country = United States
}} thriller directed by Abel Ferrara. The lead roles are played by Billy Dee Williams and Tom Berenger.

In 2012 the film was released on Blu-ray by Shout! Factory.

==Plot==
A serial killer who is an expert at martial arts is preying on strippers in Manhattans Times Square. Night after night, he visits smoky strip clubs, waiting for his victims. The owners of the largest company of strippers in the city are Matt Rossi (Berenger) and Nicky Parzeno (Scalia). Rossi is a retired boxer who retired after having killed an opponent in the ring. He is now seeing their whole business under threat, at the same time as he fears that the woman he loves might be the next victim.

==Cast==
* Tom Berenger as Matt Rossi 
* Billy Dee Williams as Al Wheeler 
* Jack Scalia as Nicky Parzeno 
* Melanie Griffith as Loretta 
* Rossano Brazzi as Carmine 
* Rae Dawn Chong as Leila 
* John Foster as Pazzo  
* Neil Clifford as The Karate-Killer
* María Conchita Alonso as Silver (as Maria Conchita) 
* Joe Santos as Frank
* Ola Ray as Honey
* Tracy Griffith as Sandra Cook
* Emilia Crow as Bibi (as Emilia Lesniak) 
* Jan Murray as Goldstein
* Janet Julian as Ruby
* Michael V. Gazzo as Mike
* Jan Murray as Goldstein Daniel Faraldo as Sanchez
* Peter Mele as Hitman #1
* Robert Miano as Hitman #2
* Raphael Berko as Hitman #3
* Antony Ponzini as Mobster #1
* Frank Sivero as Mobster #2
* Brent Jennings as Hawker #1

==External links==
*  

== References ==
 

 

 
 
 
 
 
 
 
 
 


 