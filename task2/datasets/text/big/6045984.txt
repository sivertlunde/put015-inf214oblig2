The Castaway Cowboy
{{Infobox Film
| name           = The Castaway Cowboy
| image	=	The Castaway Cowboy FilmPoster.jpeg
| image_size     = 225px
| caption        = Theatrical poster
| director       = Vincent McEveety
| producer       = 
| writer         = 
| narrator       = 
| starring       = James Garner Vera Miles Eric Shea Robert Culp
| music          = 
| cinematography = 
| editing        =  Walt Disney Productions Buena Vista Distribution
| released       = August 1, 1974 
| runtime        = 91 mins
| country        = US
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Walt Disney Productions starring James Garner, Vera Miles, Eric Shea, and Robert Culp about a Texas rancher who gets Shanghaiing|shanghaied, then jumps ship and finds himself washed ashore in Hawaii. Filmed on location in Hawaii, the movie was directed by Vincent McEveety and written by Don Tait and Richard M. Bluel.

==Plot==
Texas cowboy, Lincoln Costain (James Garner), gets "Shanghaiing|shanghaied" in San Francisco, then jumps ship and washes ashore on the Hawaiian island of Kauai, right into the arms of widow Henrietta MacAvoy (Vera Miles) and her son (Eric Shea) who are struggling to make a living as farmers.  A lot of wild cattle often trample their crops, so Costain gets the idea to start cattle ranching instead. The Hawaiian farm hands dont readily take to the American cowboy culture, and Calvin Bryson (Robert Culp), is a banker with eyes to grab Henriettas land and maybe Henrietta herself.

==Cast==
*James Garner as Lincoln Costain
*Vera Miles as Henrietta MacAvoy
*Eric Shea as Booton Little Maca MacAvoy
*Robert Culp as Calvin Bryson Elizabeth Smith as Liliha (MacAvoy housekeeper)
*Manu Tupou as Kimo
*Gregory Sierra as Marruja (Brysons henchman)
*Shug Fisher as Capt. Cary
*Nephi Hannemann as Malakoma (witch doctor)
*Lito Capiña as Leleo
*Ralph Hanalei as Hopu
*Kim Kahana as Oka (as Kahana)
*Lee Woodd as Palani Luis Delgado as The Hatman (loses hat to Costain)
*Buddy Joe Hooker as Boatman taking Costain to ship
*Patrick Sullivan Burke as Sea captain in poker game
*Jerry Velasco as Hawaiian cowboy (voice)

==External links==
*  
*  
*  
*  
*   at Archive of American Television

 

 
 
 
 
 
 
 
 
 
 
 
 


 