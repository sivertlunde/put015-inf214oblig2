The Last Full Measure (film)
{{Infobox Film
| name           =   The Last Full Measure
| image= 
| image_size     = 
| caption        = 
| director       =   Alexandra Kerry
| producer       =   Nina Leidersdorff
| writer         =   Alexandra Kerry Tracy Droz Tragos
| narrator       =
| starring       =   Reiko Aylesworth Xander Berkeley Somah Haaland
| music          =   
| cinematography =   Michael Hansen 
| editing        =   Vikash Patel
| studio         =   
| distributor    =   American Film Institute| American Film Institute (AFI)
| released       =   April 1, 2004 (USA)
| runtime        =   18 minutes
| country        =   USA
| language       =   English
| budget         =   USA$50,000 (estimated)
| gross          = 
| preceded_by    = 
| followed_by    = 
}}	
The Last Full Measure is a 2004 short film written and directed by Alexandra Kerry, daughter of U.S. Democratic Senator John Kerry. Set in 1973, during the Vietnam War, it explores the emotions of a nine-year-old girl awaiting her fathers return from the war. The cast includes 24 (TV series)|24 stars Xander Berkeley and Reiko Aylesworth.

The title is drawn from the Gettysburg Address of President Abraham Lincoln.

==External links==
* 
* 

 
 
 
 


 