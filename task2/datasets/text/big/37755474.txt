The Divorcee (1919 film)
{{Infobox film
| name           = The Divorceé
| image          = The Divorcee (1919) - Ad 1.jpg
| imagesize      = 200px
| caption        = Ad for film
| director       = Herbert Blaché
| producer       = Metro Pictures Maxwell Karger
| writer         = June Mathis (scenario) Katherine Kavanaugh (scenario)
| based on       =  
| starring       = Ethel Barrymore E. J. Ratcliffe Holmes Herbert
| music          =
| cinematography = George K. Hollister
| editing        =
| distributor    = Metro Pictures
| released       =  
| runtime        = 5 reel#Motion picture terminology|reels; 4,400 feet
| country        = United States Silent (English intertitles)
}}
The Divorceé is a 1919 American society drama starring Ethel Barrymore in her last silent film. The film is based on a 1908 play, Lady Frederick by young Somerset Maugham, which had starred Barrymore on Broadway.  The play was already quite dated when this film was made, but the actress was always comfortable with this kind of Soap opera|soap-operish melodramatic material. Herbert Blaché directed, and June Mathis wrote the scenario based on Maughams play. The film was produced and distributed by the Metro Pictures company.

It is believed to be a lost film.   The last surviving copy perished in the 1967 MGM vault fire.

==Plot==
As described in a film magazine,   Betsy OHara (Barrymore) marries Lord Frederick Berolles (Ratcliffe) to please her parents, and while now is Lady Frederick, she finds herself miserable when her husband constantly insults her after discovering that she loves another. It does not help that Sir Paradise Fuldes (Herbert), the man she would have married except for his poverty, soon thereafter comes into a fortune. Her sister Kitty (Childers), while married to a man of position, has foolishly compromised herself with Robert Montgomery (Kilgour), a cad who forces her to visit his rooms on threat of disclosure. While assisting her sister in the recovery of some old love letters, she is surprised in the apartments of Montgomery, and assumes the situation to save her sisters reputation. She is then divorced, and sinks in the social scale. Lord Charles Mereston (Entwistle) meets her while gambling in Monte Carlo and falls in love with her. Lady Frederick does not encourage him, but he will not take no for an answer. His wife Lady Mereston (Gordon) appeals to her brother Sir Paradise to intervene, and while Lady Frederick resents the interference, she sends Mereston about his business using an ingenious device, and finds her reward in the arms of Paradise.

==Cast==
*Ethel Barrymore as Lady Frederick Berolles
*E. J. Ratcliffe as Lord Frederick Berolles
*H. E. Herbert (aka Holmes Herbert) as Sir Paradise Fuldes
*Naomi Childers as Kitty Beresford
*John Goldsworthy as Horace Beresford
*Joseph Kilgour as Robert Montgomery
*Maude Turner Gordon as Lady Mereston
*Harold Entwistle as Lord Mereston
*Eugene Strong as Young Lord Mereston
*Ricca Allen as Madame Claude

==References==
 

==External links==
 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 