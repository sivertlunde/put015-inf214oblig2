The Great Escape (film)
 
{{Infobox film
| name           = The Great Escape
| image          = Great_escape.jpg
| image_size     = 225px Frank McCarthy
| director       = John Sturges
| producer       = John Sturges Walter Newman (uncredited)
| based on       =  
| starring       = Steve McQueen James Garner Richard Attenborough James Donald Charles Bronson Donald Pleasence James Coburn
| music          = Elmer Bernstein
| cinematography = Daniel L. Fapp Walter Riml
| editing        = Ferris Webster
| studio         = Mirisch Company
| distributor    = United Artists
| released       =    
| runtime        = 165 minutes
| country        = United States
| language       = English German French
| budget         = $3.8 million   
| gross          = $11,744,471
}} epic film British and prisoners of German POW camp during World War II, starring Steve McQueen, James Garner, and Richard Attenborough.
 book of mass escape from Stalag Luft III in Sagan (now Żagań, Poland), in the province of Lower Silesia, Nazi Germany. The characters are based on real men, and in some cases are composites of several men. The film was made by the Mirisch Company, released by United Artists, and produced and directed by John Sturges.

The film had its Royal World Premiere at the Odeon Leicester Square in Londons West End on 20 June 1963. 

==Plot== Allied prisoners Colonel von Luger, tells the senior British officer, Group Captain Ramsey, "There will be no escapes from this camp." Von Luger points out the various features of the new camp designed to prevent escape, as well as the perks the prisoners will receive as an incentive not to try. After several failed escape attempts on the first day, the POWs settle into life at the prison camp.
 SD agents bring RAF Squadron Leader Roger Bartlett to the camp. Known as "Big X", Bartlett is introduced as the principal organiser of escapes. As Kuhn leaves, he warns Bartlett that if he escapes again, he will be shot. However, locked up with "every escape artist in Germany", Bartlett immediately plans the greatest escape attempted, with tunnels for breaking out 250 prisoners, to the point that as many troops and resources as possible will be wasted on finding POWs instead of being used on the front line.
 American in blind due to progressive myopia caused by intricate work by candlelight; Hendley takes it upon himself to be Blythes guide in the escape. The prisoners work on three tunnels simultaneously, calling them Tom, Dick and Harry|"Tom", "Dick", and "Harry".
 USAAF Captain Captain Virgil RAF Flying Officer Archibald Ives conceive of an escape attempt through a short tunnel at a blind spot right near the edge of the camp, a proposal which is accepted by Bartlett on the grounds that vetoing every independent escape attempt would raise suspicion of the collective escape attempt being planned. However, Hilts and Ives are caught and returned to the cooler. Upon release from the cooler, Bartlett requests that Hilts use his next escape attempt as an opportunity for surveillance for the other prisoners; Hilts refuses. Meanwhile, Hendley forms a friendship with German guard Werner, which he exploits on several occasions to smuggle documents and other items of importance to the prisoners.
 4th of July celebration arranged by the three Americans, the guards discover "Tom." In despair, Ives walks to the barbed wire that surrounds the camp and climbs it in view of guards. Hilts runs to stop him, but is too late, and Ives is shot dead near the top of the fence. The prisoners switch their efforts to "Harry." Hilts agrees to reconnoiter outside the camp and allows himself to be recaptured. The information he brings back is used to create maps showing the nearest town and railway station.

 

The last part of the tunnel is completed on the night of the escape, but it proves to be 20 feet short of the woods, which are to provide cover. Bartlett orders the escape to continue. Danny, having spent much of his time in the tunnel and barely surviving multiple cave-ins, develops claustrophobia and nearly refuses to go, but is helped along by Willie. However, due to his impatience, Griffith is discovered while exiting the tunnel and thwarts the completion of the escape effort; 76 manage to escape.
 neutral Switzerland, Sweden and Spain, almost all the POWs are recaptured or killed. Hendley and Blythe steal an aircraft to fly over the Swiss border, but the engine fails and they crash-land. Soldiers arrive and Blythe, his eyesight damaged, stands and is shot. Hendley surrenders and is captured as Blythe dies.

Bartlett is identified in a crowded railway station by Gestapo agent Kuhn. Ashley-Pitt overpowers Kuhn and kills him with his own gun, but is shot and killed by soldiers while attempting to escape. In the commotion, Bartlett and MacDonald slip away, but are caught while boarding a bus after MacDonald blunders by replying in English to a suspicious Gestapo agent who wishes them "Good luck" in English. Hilts steals a motorcycle and is pursued by German soldiers, jumps a first line barbed wire fence at the German-Swiss border and drives on to the Neutral Zone, but becomes entangled in the second line of the barbed fence on the Swiss border and is captured.

Three truckloads of recaptured POWs go down a country road and split off in three directions. One truck, containing Bartlett, MacDonald, Cavendish, Haynes and others, stops in a field and the POWs are told to get out and "stretch their legs." They are shot dead. In all, fifty escapees are murdered. Hendley and nine others are returned to the camp. Von Luger is relieved of command of the prison camp by the SS for failing to prevent the breakout.
 Baltic coast, Resistance to Spain. Hilts is returned to the camp alone and taken back to the cooler. Lieutenant Goff, one of the Americans, gets Hiltss baseball and glove and throws it to him when Hilts and his guards pass by. The guard locks him in his cell and walks away, but momentarily pauses when he hears the familiar sound of Hilts bouncing his baseball against a cell wall. The film ends with the caption "This picture is dedicated to the fifty."

==Cast==
  
*Steve McQueen as Capt. Virgil Hilts, the "Cooler King"
*James Garner as Flt. Lt. Robert Hendley, the "Scrounger" Big X"
*James Donald as Gp. Capt. Ramsey, the SBO (Senior British Officer)
*Charles Bronson as Flt. Lt. Danny Velinski, "Tunnel King"
*Donald Pleasence as Flt. Lt. Colin Blythe, the "Forger"
*James Coburn as Fg. Off. Louis Sedgwick, the "Manufacturer"
*Hannes Messemer as Oberst von Luger, the Kommandant
*David McCallum as Lt. Cmdr. Eric Ashley-Pitt, "Dispersal" Gordon Jackson as Flt. Lt. Andrew MacDonald, "Intelligence"
*John Leyton as Flt. Lt. William Dickes, "Tunnel King"
*Angus Lennie as Fg. Off. Archibald Ives, the "Mole" Nigel Stock as Flt. Lt. Dennis Cavendish, the "Surveyor" Robert Graf as Werner, the "Ferret"
 
*Jud Taylor as 2nd Lt. Goff Hans Reiser as Herr Kuhn
*Harry Riebauer as Sgt. Strachwitz William Russell as Sorren
*Robert Freitag as Hauptmann Posen
*Ulrich Beiger as Preissen
*George Mikell as SS officer
*Lawrence Montaigne as Haynes, "Diversions"
*Robert Desmond as Griffith, "Tailor"
*Til Kiwe as Frick
*Heinz Weiss as Kramer Tom Adams as Dai Nimmo, "Diversions"
*Karl-Otto Alberty as SS Officer Steinach
 
 William Russell, George Mikell, Lawrence Montaigne & Karl-Otto Alberty.

==Production==

===Adaptation=== Walter Newman The Great Escape. Brickhill had been a prisoner at Stalag Luft III during World War II.
 Jens Müller & Per Bergsland) and Dutch (Bram van der Stok).  While a few Americans in the POW camp initially contributed towards construction of the tunnels and worked on the early escape plans, they were moved to their own compound seven months before the tunnels were completed. Paul Brickhill|Brickhill, Paul, The Great Escape 

Ex-POWs asked film-makers to exclude details about the help they received from their home countries, such as maps, papers, and tools hidden in gift packages, lest it jeopardise future POW escapes. The film-makers complied. 

In reality Canadians played an important role in the construction of the tunnels and the escape itself. Of the 1,800 or so POWs in the compound, 600 were involved in preparations for the escape; 150 of these were Canadian. Wally Floody, an RCAF pilot and mining engineer who was the real-life "tunnel king", was engaged as a technical advisor for the film. 

  (left) with Wally Floody, a former Canadian POW who was part of the real Great Escape and acted as a technical advisor in production of the film.]]

===Casting===
Steve McQueen, in a role based on at least two pilots, David M. Jones and John Dortch Lewis,  has been credited with the most significant performance. Critic Leonard Maltin wrote that "the large, international cast is superb, but the standout is McQueen; its easy to see why this cemented his status as a superstar." {{cite book
  | last = Maltin
  | first = Leonard
  | authorlink = Leonard Maltin
  | title = Leonard Maltins Family Film Guide
  | publisher = Signet
  | year = 1999
  | location = New York
  | page = 225
  | url =
  | id =
  | isbn =0-451-19714-3 }} 

Richard Attenborough was cast as Sqn Ldr Roger Bartlett RAF ("Big X"), a character based on Roger Bushell, the South African-born British POW who was the mastermind of the real Great Escape.    This was the film that first brought Attenborough to wide popular attention in the United States.

Group Captain Ramsey RAF (the "SBO") was based on Group Captain  .

Flt Lt Colin Blythe RAF ("The Forger") was based on Tim Walenn and played by Donald Pleasence.  Pleasence himself had served in the Royal Air Force during World War II. He was shot down and spent a year in German prisoner-of-war camp Stalag Luft I.
 USAAF and was wounded, but had not been shot down. Like his character, Danny Valinski, he suffered from claustrophobia.

James Garner had been a soldier in the Korean War and was twice wounded. He was a scrounger during that time, as is his character Flt Lt Hendley. 
 Kommandant of Stalag Luft III, "Colonel von Luger," a character based on Oberst Friedrich Wilhelm von Lindeiner-Wildau. {{cite book
  | last = Carroll
  | first = Tim
  | title = The Great Escapers
  | publisher = Mainstream Publishing
  | year = 2004
  | isbn = 1-84018-904-5 }} 

Angus Lennies Flying Officer Archibald Ives, "The Mole", was based on Jimmy Kiddel, who was shot dead while trying to scale the fence.   
 Jens Müller. The successful escape of Coburns Australian character via Spain was based on Dutchman Bram van der Stok.

===Location and set design=== Bavaria Film Studio in the Munich suburb of Geiselgasteig in rural Bavaria where sets for the barrack interiors and tunnels were constructed. The camp was constructed in a clearing in the Perlacher forest near the studio.       The German town near the real prison camp was Sagan (now Żagań, Poland); it was renamed Neustadt in the film.  Many scenes were filmed in and around the town of Füssen in Bavaria, including its railway station. The nearby district of Pfronten  with its distinctive St. Nikolaus parish church (Pfronten)|St. Nikolaus Church and scenic background also features often in the film.  Many scenes involving trains and stations were filmed near Deisenhofen station and on the Großhesselohe - Holzkirchen railway line.  

The film depicts the tunnel codenamed Tom as having its entrance under a stove and Harrys as in a drain sump in a washroom. In reality, Dicks entrance was the drain sump, Harrys was under the stove, and Toms was in a darkened corner next to a stove chimney.  

The motorcycle chase scenes culminating in the jumping of the barbed wire were shot on meadows outside Füssen, and the "barbed wire" that Hilts crashed into before being recaptured was simulated by strips of rubber tied around barbless wire, constructed by the cast and crew in their spare time.     The jump scene was performed by stuntman Bud Ekins in place of Steve McQueen.  Other parts of the chase scene were done by McQueen playing both Hilts and the soldiers chasing him because of McQueens ability on a motorcycle.   

===Music===

====Intrada Records album====

=====Disc One=====
{{Tracklist
| collapsed = yes
| headline  = Original Motion Picture Soundtrack
| title1    = Main Title
| length1   = 2:30
| title2    = At First Glance
| length2   = 3:07
| title3    = Premature Plans
| length3   = 2:28
| title4    = If At Once
| length4   = 2:31
| title5    = Forked
| length5   = 1:28
| title6    = Cooler
| length6   = 1:59
| title7    = Mole
| length7   = 1:28
| title8    = "X"/Tonight We Dig
| length8   = 1:30
| title9    = The Scrounger/Blythe
| length9   = 3:50
| title10   = Water Faucet
| length10  = 1:23
| title11   = Interruptus
| length11  = 1:33
| title12   = The Plan/The Sad Ives
| length12  = 1:43
| title13   = Green Thumbs
| length13  = 2:28
| title14   = Hilts And Ives
| length14  = 0:38
| title15   = Cave In
| length15  = 2:01
| title16   = Restless Men
| length16  = 1:56
| title17   = Booze
| length17  = 1:47
| title18   = "Yankee Doodle"
| length18  = 0:55
| title19   = Discovery
| length19  = 3:40
}}

=====Disc Two=====
{{Tracklist
| collapsed = yes
| headline  = Original Motion Picture Soundtrack (Contd)
| title1    = Various Troubles
| length1   = 3:52
| title2    = Panic
| length2   = 2:05
| title3    = Pin Trick
| length3   = 0:59
| title4    = Hendley’s Risk
| length4   = 1:43
| title5    = Released Again/Escape Time
| length5   = 5:25
| title6    = 20 Feet Short
| length6   = 3:06
| title7    = Foul Up
| length7   = 2:37
| title8    = At The Station
| length8   = 1:33
| title9    = On The Road
| length9   = 3:27
| title10   = The Chase/First Casualty
| length10  = 6:49
| title11   = Flight Plan
| length11  = 2:09
| title12   = More Action/Hilts Captured
| length12  = 6:07
| title13   = Road’s End
| length13  = 2:06
| title14   = Betrayal
| length14  = 2:20
| title15   = Three Gone/Home Again
| length15  = 3:13
| title16   = Finale/The Cast
| length16  = 2:47
}}

=====Disc Three=====
{{Tracklist
| collapsed = yes
| headline  = Original 1963 United Artists Score Album
| title1    = Main Title
| length1   = 2:07
| title2    = Premature Plans
| length2   = 2:08
| title3    = Cooler And Mole
| length3   = 2:26
| title4    = Blythe
| length4   = 2:13
| title5    = Discovery
| length5   = 2:54
| title6    = Various Troubles
| length6   = 2:40
| title7    = On The Road
| length7   = 2:54
| title8    = Betrayal
| length8   = 2:05
| title9    = Hendley’s Risk
| length9   = 2:24
| title10   = Road’s End
| length10  = 2:00
| title11   = More Action
| length11  = 1:57
| title12   = The Chase
| length12  = 2:49
| title13   = Finale
| length13  = 3:14
}}

==Reception== one of the highest grossing films of 1963, despite heavy competition. In the years since its release, its audience has broadened, cementing its status as a cinema classic.    It was entered into the 3rd Moscow International Film Festival where McQueen won the Silver Prize for Best Actor.   

Critical and public response was mostly enthusiastic, with a 92% rating on   described it as "pretty good but overlong POW adventure with a tragic ending".  In Time (magazine)|Time magazine 1963: "The use of color photography is unnecessary and jarring, but little else is wrong with this film. With accurate casting, a swift screenplay, and authentic German settings, Producer-Director John Sturges has created classic cinema of action. There is no sermonizing, no soul probing, no sex. The Great Escape is simply great escapism".   

In a 2006 poll in the United Kingdom, regarding the family film that television viewers would most want to see on Christmas Day, The Great Escape came in third, and was first among the choices of male viewers.   

In 2009, seven POWs returned to Stalag Luft III for the 65th anniversary of the escape    and watched the film. According to the veterans, many details of the first half depicting life in the camp were authentic, e.g. the machine-gunning of Ives, who snaps and tries to scale the fence, and the actual digging of the tunnels.  In 2014, the RAF staged a commemoration of the escape attempt, with 50 serving personnel carrying a photograph of one of the men shot. 

===Awards and honors===
*Nominated Academy Award for Film Editing (Ferris Webster)
*Nominated Golden Globe Award for Best Picture
*Winner Moscow International Film Festival Best Actor (Steve McQueen)
*Nominated Moscow International Film Festival Grand Prix (John Sturges)
*Selected National Board of Review Top Ten Films of Year
*Nominated Writers Guild of America Best Written American Drama (James Clavell, W.R. Burnett) (Screenplay Adaptation)

==In popular culture==
References to scenes and motifs from the film, as well as Elmer Bernsteins theme, have appeared in other films, television series, and video games.

;Film 
*The films   all contain references or homages to the film.   

;Television  Goodness Gracious Me and Red Dwarf have all parodied or paid homage to the film. 
*In "Escape From Stalag Luft 112B," an episode of Ripping Yarns, Maj. Errol Phipps (played by Michael Palin) is depicted as being the only prisoner of war never to escape from the camp of the title. 
*A fictional, made-for-television sequel,  , appeared in 1988. It starred Christopher Reeve and Judd Hirsch, with Donald Pleasence as an SS villain.  Mitchell & Webb, a sketch called "Cheesoid" features a scene where a character who has lost their sense of smell locates a clove of garlic deliberately placed beforehand on the floor, in the same way that Flt Lt Blythe places the pin on the floor in the film of the Great Escape.  Another sketch involving a German language version of the gameshow Numberwang sees an English contestant tricked into revealing her nationality in the same way that Big X is tricked by the Gestapo agent. 
*The television comedy series Hogans Heroes has been compared to the film The Great Escape, but beyond the similarities of being set in a Luft Stalag during World War II, prisoners trying to escape and digging tunnels and German guards (which are common to all such movies) the similarities end.  Hogans Heroes involves prisoners, not trying to escape, but carrying out acts of espionage and sabotage involving spies, defectors, escaped prisoners from other camps, resistance groups, German secret weapons and beautiful women. Characterization of the Germans is also completely different especially the camp commandant as well as the interaction between the prisoners and guards.
* In the television drama series "Suits" season 3 episode 9 Jessica Pearson instructs Louis Litt to conduct himself as the "Cooler King" a reference to Steve McQueens character in the movie.

;Sport  England football team matches since 1996.      They released an arrangement of the theme as a single for the 1998 FIFA World Cup and a newer version for UEFA Euro 2000. 

;Video games
In 1986 Ocean software released The Great Escape (video game)|The Great Escape for the Commodore 64, ZX Spectrum and DOS platforms.
 Eidos for Xbox and PlayStation 2.

The Great Escape is referenced in   and was one of the main inspirations of the stealth concept in the Metal Gear series.

In the video game Freelancer (video game)|Freelancer, the protagonist, Edison Trent, is dressed in an outfit identical to the one worn by Steve McQueens character Virgil Hilts.

==References==
;Notes
 

;Bibliography
*   Details the manhunt by the Royal Air Forces special investigations unit after the war to find and bring to trial the perpetrators of the "Sagan murders".
*  
*  
*  
*  
*  
*   Memoir by the surviving Norwegian escapee. Harry "Wings" Day.
*  

==External links==
 
*  
*  
*  
*   (See 30:23–34:47 of video.)
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 