The Virgo, the Taurus and the Capricorn
{{Infobox film
| name = The Virgo, the Taurus and the Capricorn
| image =  The Virgo, the Taurus and the Capricorn.jpeg
| caption =
| director = Luciano Martino
| writer =
| starring = Edwige Fenech
| music = Franco Pisano
| cinematography =
| editing =
| producer =
| distributor =
| released =
| runtime =
| awards =
| country =
| language = Italian
| budget =
}}  1977 Cinema Italian commedia sexy comedy film directed by  Luciano Martino.   According the film critic Paolo Mereghetti, the film is "one of the most funny films" in the commedia sexy allitaliana genre. 

==Plot==

An established Milanese architect who lives in Rome (Alberto Lionello), indomitable unfaithful to his wife (Edwige Fenech), does everything not to be discovered by her. But when this happens, the wife takes revenge by cheating on her husband with a young architecture student (Ray Lovelock).

== Cast ==
*Alberto Lionello: Gianni Ferretti
*Edwige Fenech: Gioia Ferretti
*Aldo Maccione: Felice Spezzaferri
*Alvaro Vitali: Alvaro 
*Mario Carotenuto: Comm. Benito Gussoni  Ray Lovelock: Patrizio Marchi
*Erna Schürer: Tourist with Patrizio
*Olga Bisera: Enrica Riccardo Garrone: Husband of Enrica
*Michele Gammino: Raffaele 
*Giacomo Rizzo: Peppino Ruotolo
*Ugo Bologna: Father of Gianni 
*Gianfranco Barra: Alberto Scapicolli
*Lars Bloch: American professor
*Lia Tanzi: Luisa

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 