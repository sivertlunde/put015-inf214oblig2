Bitter Feast
{{Infobox film
| Name           = Bitter Feast
| image          = Bitterpage.jpg
| caption        = Teaser poster.
| director       = Joe Maggio
| writer         = Joe Maggio
| producer       = Derek Curl Larry Fessenden Brent Kunkle Peter Phok
| starring       = James LeGros Joshua Leonard Amy Seimetz Larry Fessenden Megan Hilty John Speredakos Mario Batali
| cinematography = Michael McDonough
| music          = Jeff Grace
| editing        = Seth Anderson
| studio         = Dark Sky Films Glass Eye Pix
| distributor    = Dark Sky Films
| released       =  
| country        = United States
| language       = English
}}
Bitter Feast is an American psychological horror film directed and written by Joe Maggio.  It stars James LeGros as a chef pushed over the edge by food critic J.T. Franks (Joshua Leonard) vicious review. The film also features actors Larry Fessenden, Megan Hilty and a cameo from real life master chef Mario Batali. 

==Plot==
Peter Grey is a successful New York  chef, disgruntled by caustic reviews from the food critic J.T. Franks. After Franks bad review, Grey is fired by his boss Gordon. He kidnaps Franks, takes him to a cabin in the woods, and forces him to cook his own perfect dinner.

==Cast==
* James LeGros as Peter Gray
* Joshua Leonard as JT Franks
* Larry Fessenden as William Coley
* Megan Hilty as Peg
* Mario Batali as Gordon
* Owen Campbell as Johnny
* Tobias Campbell as Young Peter
* Paula El Sherif as Cooking Show Guest
* Sean Reid as Co-Worker
* Graham Reznick as Local Newscaster
* Amy Seimetz as Katherine Franks
* John Speredakos as Phil

==Production==
Directed by  Joe Maggio.  Starring James LeGros and Joshua Leonard.  The film was shot in New York City and Woodstock, New York|Woodstock.  Larry Fessenden produced the film with his company Glass Eye Pix. 

==Release==
The film premiered as part of the LA Film Festival on June 18, 2010.  An exact date for the theatrical release has not been determined.  The DVD was released in October 26, 2010. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 