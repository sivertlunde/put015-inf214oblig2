Thief of the Mind
{{Infobox film
| name = Thief of the Mind
| image = Thief-of-the-Mind-film-poster.jpg
| caption = Original film poster
| director = Janchivdorjiin Sengedorj
| producer = "Wisdom Film"   Baljinnyamyn Amarsaikhan
| screenplay = Dorjkhandyn Törmönkh
| based on = Story of B. Gansükh
| starring = Baljinnyamyn Amarsaikhan
| music = 
| cinematography = D. Angarag
| editing = B. Battuvaan
| distributor = 
| released = 2011
| runtime = 
| country = Mongolia
| language = 
}}
Thief of the Mind or Mind Thief ( ) is a 2011 Mongolian film of the film production company "Wisdom Film". It was written and directed by Janchivdorjiin Sengedorj with the Mongolian actor Baljinnyamyn Amarsaikhan as producer and lead actor in the role of Gantulga. The film was screened during the Ulaanbaatar International Film Festival.

==Background== Mongolian Tögrögs 90 million in 1992. He was nickname as "90 million" as his story unfolded shocking the Mongolians. He was sentenced to several years in prison, but managed to escape. He was caught again and his sentence was extended. After release, Gansükh wrote a book translated as Life transformed by temptation about the affair. Journalist Dorjkhandyn Törmönkh adapted the story for the movie. 

==Synopsis==
A journalism student wants to become a film producer. By chance, he overhears a conversation about the Mongolian rare findings that are brought to America and decides to explore this for his film getting financing from high sources in the Mongolian government.But things get terribly complicated. 

==Awards==
During the annual Mongolian Academy Awards in 2012, the film won most awards becoming the most decorated film of 2011.   

Awards won:
*Best Film - for Thief of the Mind
*Best Lead Role - (male) - for Baljinnyamyn Amarsaikhan
*Best Supporting Role - (female) - for Sharavyn Delgerjargal
*Best Cinematography - for D. Angarag
*Best Sound Editing - for B. Battuvaan
*Best Screenplay - for Dorjkhandyn Törmönkh

==References==
 

==External links==
* 
* 

 
 