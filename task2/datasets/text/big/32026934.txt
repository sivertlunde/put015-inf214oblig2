Seeking a Friend for the End of the World
 
{{Infobox film
| name           = Seeking a Friend for the End of the World
| image          = Seeking a Friend for the End of the World Poster.jpg
| caption        = Theatrical release poster
| director       = Lorene Scafaria
| producer       = {{plainlist|
* Steve Golin
* Joy Gorman
* Mark Roybal
* Steven M. Rales
}}
| writer         = Lorene Scafaria
| starring       = {{plainlist|
* Steve Carell
* Keira Knightley
* Adam Brody
* Derek Luke
* William Petersen
}}
| music          = {{plainlist|
* Jonathan Sadoff
* Rob Simonsen
}}
| cinematography = Tim Orr
| editing        = Zene Baker
| studio         = {{plainlist|
* Mandate Pictures Indian Paintbrush
}}
| distributor    = Focus Features
| released       =  
| runtime        = 101 minutes 
| country        = United States
| language       = English
| budget         = $10 million 
| gross          = $9.6 million 
}} directorial debut. The film stars Steve Carell and Keira Knightley. The title and plot are a reference to a track on Chris Cornells 1999 album, Euphoria Morning, called "Preaching the End of the World". 

==Plot==
The film opens with a radio announcement that a space shuttle mission to destroy an asteroid on a collision course with Earth has failed. The world has three weeks until impact, at which time humanity will be wiped out. Dodge Petersen (Steve Carell) and his wife Linda (Nancy Carell) listen to the broadcast from the side of the road. After Dodge has an uninterested response, Linda looks at him in abject disgust and flees the vehicle without saying a word.

Dodge returns to his near-empty New York City workplace the next day, where he sells insurance. While everyone around him has reacted differently—from suicide to constant drugs to guilt-free sex—Dodges life has not changed at all. He does not stop going into his meaningless job until one of his friends leaps to his death.

After attending a friends wild party, where he passes on sex and drugs, Dodge returns home to reminisce about his high school sweetheart, Olivia, when he notices his neighbor Penny (Keira Knightley) crying on the fire escape. She admits that she has just broken up with her boyfriend Owen (Adam Brody) for making her miss her last opportunity to see her family in England. At her apartment, Penny gives Dodge three years worth of his mail that was incorrectly delivered to her and in the process unwittingly tells him that his wife was having an affair.

Dodge storms off into the night, guzzling codeine-laced cough syrup and window cleaner in a suicide attempt. He wakes up in a park, with a note on his sweater—reading "Sorry"—and a dog tethered to his foot. Dodge takes the dog home, opens the old mail, and is surprised to discover a three-month-old letter from Olivia, which explains that he was "the love of her life". Later, a riot breaks out on his block. Abandoning Owen amidst the rioters, Dodge explains to Penny that he knows someone who could fly her to England if she helps him find Olivia. She agrees and the two set off for Dodges hometown in Delaware.

Along the way, Dodge and Penny run out of gas, share a bizarre experience with a motorist (William Petersen), witness the eruption of an orgy at a restaurant, have sex in a pick-up truck, and spend a night in jail before getting a ride to Camden, New Jersey, where they meet Speck (Derek Luke), a man from Pennys past who is prepared for the apocalypse. Speck has a working satellite phone in his bunker, and lets Penny contact her family. Penny and Dodge borrow a car of Specks and eventually make it to Olivias home. Dodge and Penny walk up toward the door of the home. It turns out that no one is there so they stay, take a rest, make a meal, and drink some wine. Dodge and Penny find the address of Olivias home and drive over there. Dodge exits the vehicle and starts toward the door, much to Pennys disappointment, and returns, saying that he left a note.

Later, after stumbling across a mass baptism at the ocean, Dodge and Penny spend the rest of the afternoon falling for each other. As dusk approaches, they end up in Somerset, where they pay a visit to Dodges estranged father Frank (Martin Sheen). Their reunion leads to reconciliation and the three then bond. After Penny falls asleep, Dodge carries her outside and places her in the passenger side of his fathers small two-seater airplane, whispering that she is the love of his life and at the same time knowing that this is the last time he will ever see her.

Dodge then returns home, turning on the television to discover that the asteroid is due to arrive earlier than expected. With only sixteen hours left, Dodge goes to Pennys apartment and listens to her records until the power goes out. In the dark, he lights candles when suddenly Penny appears. Having told his father to turn around, she asks Dodge how he could have let her go. He admits his mistake, and the two share a tearful reunion.

Shortly after, as they lie in bed, Penny is scared, knowing that, in only moments, human life on Earth will cease to exist, but Dodge distracts her. Penny tells him of her childhood. They feel and hear a deep booming sound, and Penny tells Dodge how she wished they had known each other sooner. Dodge replies that there never would have been enough time together, and professes his love for her. When they feel and hear a second deep booming sound, Penny panics, telling him she thought they would save each other, and Dodge assures her they did. Their final seconds are spent smiling at each other as a bright light appears behind Penny, which rapidly grows brighter and brighter until it envelops them.

==Cast==
 
*Steve Carell as Dodge Petersen
*Keira Knightley as Penelope "Penny" Lockhart
*William Petersen as Glenn
*Melanie Lynskey as Karen Amalfi
*Adam Brody as Owen
*Tonita Castro as Elsa
*Mark Moses as Anchorman
*Derek Luke as Alan Speck
*Connie Britton as Diane
*Patton Oswalt as Roache
*Rob Corddry as Warren
*Rob Huebel as Jeremy
*Gillian Jacobs as Katie
*T.J. Miller as Darcy
*Amy Schumer as Lacey
*Jim OHeir as Sheriff
*Martin Sheen as Frank Petersen
*Nancy Carell as Linda Petersen
*Roger Aaron Brown as Alfred
*Leslie Murphy as Amy
*Kasey Campbell as Danny
 

==Production==
Lorene Scafaria wanted to "tell the story of boy meets girl with a really ticking clock", prompted by recent events in her own life, including a "death in the family, a breakup, and a new relationship".   The story of an "unexpected romance blossoming between two strangers while on an impromptu road trip" is familiar territory for Scafaria, as that was the premise of her previous screenplay, Nick and Norahs Infinite Playlist. 

Scafaria said that Adam Brody helped her with the script, giving her a male perspective, to the soundtrack.  Filming began May 2011, in Los Angeles, California. 

==Reception==
The movie received mixed reviews from critics, with many praising the cast, particularly Knightley and Carell.   According to Rotten Tomatoes, 56% of critics gave it a positive review, with the consensus "Tender, charming, and well-acted, Seeking a Friend for the End of the World is unfortunately hamstrung by jarring tonal shifts and a disappointing final act."  Roger Ebert of the Chicago Sun Times gave the film a positive review and said that "The best parts of this sweet film involve the middle stretches, when time, however limited, reaches ahead, and the characters do what they can to prevail in the face of calamity. How can I complain that they dont entirely succeed? Isnt the dilemma of the plot the essential dilemma of life?" Joe Neumaier of New York Daily News said that the film was "One of the years most emotionally affecting movies."   Writing for Vogue (magazine)|Vogue magazine, film critic Nathan Heller stated that the script was "desperately in need of a good edit" while commending the performances of Knightley and Carell, asserting: "Carell and, more surprisingly, Knightley are comedians proficient enough to sell the banter". 
The film performed poorly on its opening weekend, earning only $3 million and managed to debut at Number 4 on the UK Box Office for the week ending July 15, 2012.

==Home media==
Seeking a Friend for the End of the World was released on DVD and Blu-ray Disc and made available for digital streaming in the United States on October 23, 2012. 

==References==
{{Reflist|2|refs=
   
   
   }}

==External links==
*  
*  
*   at  siamzone.com

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 