Aanandham Paramaanandham
{{Infobox film
| name           = Aanandham Paramaanandham
| image          =
| caption        =
| director       = IV Sasi
| producer       =
| writer         = Rama Arankannal Sherif (dialogues)
| screenplay     = Sherif Ravikumar
| music          = G. Devarajan
| cinematography = Vipin Das
| editing        = K Narayanan
| studio         = Jaleela Enterprises
| distributor    = Jaleela Enterprises
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, Ravikumar in lead roles. The film had musical score by G. Devarajan.    It is a remake of the 1967 Tamil film Anubavi Raja Anubavi.     

==Cast==
 
*Kamalahasan as Babu
*Unnimary as Raji
*Chandralekha as Rekha Ravikumar as Raju
*Roja Ramani as Ammini
*Sukumari as Babu & Shekharankuttys mother
*KPAC Lalitha as Lalitha
*Bahadoor as Chandrashekharan
*Chandrakala
*Janardanan as Inspector Raghavan Kunchan
*Kuthiravattam Pappu
*Paravoor Bharathan as Advocate Vasudevan
*T. P. Madhavan as Advocate
*Uma
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Sreekumaran Thampi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aananda Vaanathin || P. Madhuri, B Vasantha || Sreekumaran Thampi ||
|-
| 2 || Aanandam Paramaanandam || P Susheela, P. Madhuri || Sreekumaran Thampi ||
|-
| 3 || Koodiyaattam Kaanan || K. J. Yesudas, P. Madhuri || Sreekumaran Thampi ||
|-
| 4 || Koodiyattam Kaanaan (Bit) || K. J. Yesudas, P. Madhuri || Sreekumaran Thampi ||
|-
| 5 || Maalaakhamaarude Manamozhuki || P Susheela || Sreekumaran Thampi ||
|-
| 6 || Wonderful || K. J. Yesudas, Karthikeyan || Sreekumaran Thampi ||
|}

==References==
 

==External links==
*  

 
 
 


 