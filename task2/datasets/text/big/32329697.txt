Nana (1985 film)
{{Infobox film
| name           = Nana
| image          = 
| caption        = 
| director       = Rafael Baledón
| producer       = Jose Bolaños
| writer         = Émile Zola Irma Serrano
| narrator       = 
| starring       = Irma Serrano Isela Vega Verónica Castro Gregorio Casal
| music          = Rafael Elizondo
| cinematography = León Sánchez
| editing        = Carlos Savage
| distributor    = 
| released       =  
| runtime        = 
| country        = Mexico
| language       = Spanish
| budget         = 
}} Mexican Musical erotic and Second Empire. 
The film is an adaptation of the same name Mexican stageplay produced and starred by the actress and singer Irma Serrano in the 1970s.

==Plot==
In the Paris of the 19th century, a prostitute causes uproar. The name of the prostitute is Teresa, but everyone knows her as Naná (Irma Serrano). Naná and her best friend Satan (Veronica Castro) work as prostitutes, first on the streets of Paris, and after in a small room. In her youth, Naná was sexually abused by her stepfather and thrown from her home by her mother. As the result of this violation, she gave birth to a son. Naná prostitutes herself to raise her son, whom she stays away from her, under the care of her aunt. Nana occasionally works in a theater, which is really an underground brothel. One night, Naná is presented in the theater as "The Venus of Fire," and caused a sensation by showing her naked body. Immediately the most powerful men come to her offering jewelry and luxuries for her favors. Naná accepts the attentions of a banker, who presents her with a house in the French countryside. In this house, Naná holds an affair with a young aristocrat who she called Coquito (Jaime Garza). This same night, she is pressed by the banker to fulfill her sexual favors, while the owner of the theater-brothel where she worked, forced herto return to fulfill a contract. Naná repudiate both men and seeks solace in the Count Muffat (Manuel Ojeda), a distinguished and respectable aristocrat dedicated to the charity. However, Muffat confesses that he has also succumbed to her charms and desires her. Disappointed, Naná decides to leave her life as a courtesan and returns to work on the streets. However, one night that she is chased by the police, Naná is rescued by Satin (Isela Vega), a courtesan and friend. After spending the night together, Satin convinces her to return to her life as a courtesan. Naná then accepted becoming mistress of Count Muffat and returns to the theater to present her nude shows. With support from Muffat Nana tries to become a serious actress, snatching the characters to the actress and courtesan Rosa Mignon, but is ridiculed, because they say that she only serves to show her naked body. Disappointment causes that Naná falling into the lowest degradation. Her home becomes in the center of the vice of Paris, where people go to get drunk, performed orgies and all sorts of sexual debauchery. One night, the Count Muffat, who has been ruined by the debauchery of Naná, decides to face her in the middle of a party hosted by her for the triumphs of a mare (called Naná in her honor) in a horse show. In comparison, Naná reveals to the Count Muffat the adultery of his wife in this same house. The Count decides to leave her. That night, her young lover Coquito, commits suicide after he to discover that Naná had sexual relations with his brother, a soldier. After tonight, Naná decided to retire from life as a courtesan. Her friend Satan dies victim from tuberculosis. 
It is revealed that Naná reunites with her child, who dies soon later by smallpox. Naná returns to Paris two years later, infected by smallpox and in abject poverty. Nana died in the streets of Paris. Her body is confused between the bodies of the tramps, while a carnival through the streets of Paris. In this moment, the Prussian army invades the city.

==Cast==
* Irma Serrano as Nana
* Manuel Ojeda as Muffat
* Isela Vega as Satin
* Verónica Castro as Satán
* Jaime Garza as Coquito
* Roberto Cobo as Francisco

==Production notes==
The films is an adaptation of a polemic stageplay of the 1970s produced and starred by the Mexican actress and singer Irma Serrano La Tigresa in her own stage, the Teatro Frú Frú, in México City. The stageplay caused controversy in Mexico by his representation of nude, erotism and lesbic performances. The film reproduces the original stageplay and was censured by many movie theaters because his high erotic content.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 