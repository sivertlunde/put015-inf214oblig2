Gaata Rahe Mera Dil
{{Infobox film
 | name = Gaata Rahe Mera Dil
 | image = GaataRaheMeraDilShetty.png
 | caption = Promotional Poster
 | director = Dilip.S.Naik
 | producer = N.B.Prabhu for Akshata Arts
 | writer = K.K.Singh
 | dialogue = 
 | starring = Shilpa Shetty Ronit Roy Rohit Roy Anupam Kher Kulbhushan Kharbanda Reema Lagoo
 | music = Anand Milind
 | lyrics = 
 | cinematography = Kamlakar Rao
 | editing = V.N.Mayekar
 | art director = 
 | choreographer = 
 | released = 
 | runtime = 120 min. Hindi
 | budget = 
 | preceded_by = 
 | followed_by = 
 }}
 Indian feature directed by Dilip.S.Naik, starring  Ronit Roy, Rohit Roy and Shilpa Shetty in the lead roles.   

The film was supposed to be the launch pad for Shilpa Shetty, but dropped after initial schedule, so Baazigar became her debut release.   Similarly for Rohit Roy, Jazbaat (1994 film)|Jazbaat   became his debut release as Gaata Rahe Mera Dil went unreleased.

==Plot==

Gaata Rahe Mera Dil is a triangular love story.

==Cast==

*Shilpa Shetty
*Ronit Roy
*Rohit Roy
*Anupam Kher
*Kulbhushan Kharbanda
*Reema Lagoo
*Amrit Patel
*Suhas Joshi
*Suresh Bhagwat
*Avtar Gill
*Rakesh Bedi

==References==

 

==External links==
*  

 
 
 
 
 