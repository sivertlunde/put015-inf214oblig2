Willie & Phil
{{Infobox film name = Willie & Phil image = Margotkidderwillieandphil.jpg director = Paul Mazursky writer = Paul Mazursky starring = Margot Kidder Michael Ontkean Ray Sharkey cinematography = Sven Nykvist released =   studio = 20th Century Fox runtime = 115 minutes language = English country = United States budget = $5.5 million | gross  =  $4,400,000 
}}

Willie & Phil is a 1980 film  directed by Paul Mazursky and starring Margot Kidder and Michael Ontkean.

==Plot==
The film is set in the 1970s and amidst the counterculture chic of that era. Willie, a high school English teacher who plays jazz piano (Michael Ontkean), and Phil, a fashion photographer (Ray Sharkey), meet coming out of the Bleecker Street Cinema where Jules et Jim has just been shown and become friends. They both fall in love with Jeannette, a girl from Kentucky (Margot Kidder).

==Critical responses==
The film was reviewed by Pauline Kael in The New Yorker. "It could be that the theme of Jules et Jim, which preoccupies Mazursky - woman as the source of life and art, and woman as destroyer - is just what he cant handle. The ad for Willie & Phil does bring out the films latent subject: we see the open mouth of a giant goddess who is holding two men in the palm of her hand. They reach up to her with their offerings - one with a bottle of wine, the other with a bunch of flowers. She may be breathing life into these dwarf suitors or preparing to devour them along with their gifts. Either way, shes a source of awe and terror. All through the picture, Mazursky has been trying to demystify what he experiences as mystifying. This movie is a little monument to screwed-up notions of what women are."  

==Cast==
* Willie Kaufman - Michael Ontkean
* Phil DAmico - Ray Sharkey
* Jeannette Sutherland - Margot Kidder
* Maria Kaufman - Jan Miner
* Sal Kaufman - Tom Brennan
* Mrs. DAmico - Julie Bovasso
* Mr DAmico - Louis Guss
* Patti Sutherland - Kaki Hunter

==References==
 

==External links==
*  
*  

 

 
 
 
 