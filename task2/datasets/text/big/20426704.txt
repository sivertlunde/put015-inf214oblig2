Harry's War (1999 film)
 
{{Infobox Film
| name           = Harrys War (1999)
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Frankland
| producer       = John Foss / Richard Franklin
| writer         = Richard Frankland
| narrator       = 
| starring       = David Ngoombujarra
| music          = 
| cinematography = Peter Zakharov
| editing        = Jill Bilcock
| distributor    = Golden Seahorse Productions
| released       = 1999
| runtime        = 28 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} SBS  ABC TV. 

Harrys War is based on Franklands Uncle, Harry Saunders (brother of Reg Saunders), who fought for Australia in the South Pacific in World War Two. Saunders fought for his country in the hope that his actions would help Aboriginal people gain citizenship.

==Awards== AFI Award (1999) - Best Screenplay in a Short Film (nominated)
*Melbourne International Film Festival (1999) - OCIC Award
*Hollywood Black Film Festival (2000) - Jury Prize for Best Short Film
*St Kilda Film Festival (2000) - Best original Screenplay
*Atom Awards (2000) - Best Short Film
*St Tropez Film Festival (2000) - Best Short Film

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 

 