Impatient Vivek
{{Infobox film
| name           = Impatient Vivek
| image          = impatientvivek.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Rahat Kazmi
| producer       = Ashok Chauhan Malvee S.
| screenplay     = Rahat Kazmi
| story          = Rahat Kazmi B. K. Tyagi
| starring       = Vivek bakshi Sayali Bhagat
| music          = Shahdaab Bhartiya Neeraj Shrivastava Raja Ali Aanamik
| cinematography = Seetharam
| editing        = Jalindar Thorat
| studio         = A Search Films
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}

Impatient Vivek is a 2011 Bollywood romantic comedy film, directed by Rahat Kazmi and produced by Ashok Chauhan & Malvee S. It released under the Search Film banner.   The film was the first Bollywood release of 2011, alongside No One Killed Jessica, on Jan 7.

==Cast==
* Vivek Sudarshan as iV
* Sayali Bhagat as Shruti
* Hrishikesh Joshi as Jaggi
* Charu Asopa as Rani
* Rounaog Ahuja as Annu
* Muni Jha
* Pratik Dixit
* Nirmal Soni
* Ashok Beniwal
* R. P. Singh
* Kirti Kapoor
* Vijay Bhatia
* Sharmila Goenka
* Hemangi Rao
* Anurag Saxena
* Zia A. Khan

==Soundtrack==
The music is composed by Neeraj Shrivastava, Raja Ali, Shahdaab Bhartiya, Aanamik. Lyrics are penned Neeraj Shrivastava, Rahat Kazmi, Nusrat Badr, Sanjay Mishra, Aamir Ali, Binish Khan.

===Track listing===
{{track listing
| extra_column = Singer(s)
| music_credits = yes
| title1 = Ek Pari Pagal Si
| extra1 = Vivek Sudarshan, Rahul Seth, Sharmishtha
| music1 = Neeraj Shrivastava
| length1 = 5:00
| title2 = Impatient Vivek
| extra2 = Dominique Cerejo, Vivek Sudarshan
| music2 = Aanamik
| length2 = 4:46
| title3 = Ishq Musibat
| extra3 = Vivek Sudarshan
| music3 = Raja Ali
| length3 = 3:44
| title4 = Khuraphat
| extra4 = Vivek Sudarshan
| music4 =  Neeraj Shrivastava
| length4 = 4:58
| title5 = Meri Nindiya Udne
| extra5 = Vivek Sudarshan, Tarannum
| music5 = Raja Ali
| length5 = 3:31
| title6 = Pal Hai Makhmali
| extra6 = Hariharan (singer)|Hariharan, Asmita D
| music6 = Shahdaab Bhartiya
| length6 = 4:55
| title7 = Tujhe Manga Hai
| extra7 = Vivek Sudarshan
| music7 = Raja Ali
| length7 = 4:36
| title8 = Ye Banda Maskalandar
| extra8 = Vivek Sudarshan, Shaznine
| music8 = Raja Ali
| length8 = 3:02
| title9 = Ek Pari Pagal Si
| note9 = Love Reprise
| extra9 = Vivek Sudarshan lyrics & music9 = Neeraj Shrivastava
| length9 = 5:26
| title10 = Tujhe Manga Hai
| note10 = Remix
| extra10 = Vivek Sudarshan, Shaznine
| music10 = Raja Ali
| length10 = 4:15
}}

== Box Office ==
At the box office, it collected a total of just Rs 0.07 crore and was declared a disaster. 

==References==
 

==External links==
*  
*  

 
 
 
 
 


 
 