Ramdhanu
 
 
{{Infobox film
| name           = Ramdhanu
| image          = Ramdhanu - The Rainbow.jpg
| caption        = Theatrical release poster
| director       = Nandita Roy & Shiboprosad Mukherjee
| presenter      = Atanu Raychaudhuri
| producer       = Windows & Jalan International Films
| writer         = Nandita Roy & Shiboprosad Mukherjee
| starring       = Rachna Banerjee Gargi Roychowdhury Shiboprosad Mukherjee Kharaj Mukherjee Suzanne Bernert Sasha Ghoshal and Introducing Akashneel Mitra
| music          = Vinit Ranjan Moitra, Smriti Lala
| cinematography = Sirsha Ray
| editing        = Moloy Laha
| released       =  
| country        = India
| runtime        = 135 minutes
| language       = Bengali
| budget         =  
| gross          = 
}} Accident and Alik Sukh, and produced by Windows and Jalan International Films.  The film is presented by Atanu Raychaudhuri. The film was released on 6 June 2014. The film has done well at the box office. The film has been cinematographed by Sirsha Ray and edited by Moloy Laha.        

==Background== RTE Act - on 4 August 2009. One of the issues addressed is that of school interviews: the act orders that for admission, there should be no interview of parents or children, so as to ensure enrollment for all. But is this ruling applicable to private schools? 
As the act came into being, the private schools protested, claiming that it violated their right to run themselves without government interference. The act was revised and made non- applicable to unaided, private and boarding schools. And so the interview process continued in private schools, this time disguised behind the term ‘interaction sessions’.

==Plot Synopsis==
Mitali was a worried mother. This time, once again, her 5 year old son Gogol had failed to pass the admission test in a reputed school and his application was rejected. This was the fourth time and Mitali was getting desperate. She was determined to get her only son admitted into a reputed school. This was an opportunity that had been denied to her, so she wanted it desperately for her son. Laltu Dutta, her husband, owned a chemist’s shop and all his efforts centered around running his business successfully. He was extremely fond of his family and tried to do his best to fulfill his wife’s aspirations. When Gogol started being rejected school after school, a despondent Mitali took the advice of her friend and kept a tuition teacher to teach Gogol. But soon Mitali got disappointed by the young teacher’s casual attitude and decided to teach Gogol herself. Mitali’s desperation worries Laltu and he decides to try every means to fulfill his wife’s aspirations. He even approaches a tout and is willing to pay him a handsome amount if he can procure admission for his son. The tout claims that he can get his son admitted to any school they desire for a fee of Rs. 10 Lakhs. To Laltu, it was an exorbitant sum. He does not know how to procure that amount. Should he terminate his fixed deposits? But that was the only savings he had! On the other hand, he could not bear to see the disappointment on his wife’s face each time their son was rejected. The family decides to go to Bolpur, in the district of Birbhum, West Bengal, where Mitali’s parents resided. It was a special occasion as her brother was returning home from abroad, after a long time, along with his foreigner wife. The holiday turned out to be most interesting as Jennifer, the brother’s wife, was keen on adapting to traditional Bengali customs, learning the language and insisting on speaking in broken Bengali. In fact, she called Mitali’s insistence on getting her son admitted to an English-medium school and learning the English language as ‘linguistic imperialism’. While Gogol spent a wonderful holiday with his grandparents and his new aunt, Mitali was anxiously waiting for the interview call for her son. When it finally arrives, they rush back home. The three of them attend the interview, but this time, Mitali felt it was Laltu who had ruined her son’s chances by giving ludicrous answers to the questions put by the school’s interview board. Mitali is furious with Laltu. But she is not the one to give up hope so easily. She readies herself for the final bid. On the advice of another parent, Mitali decides to enrol herself along with her husband in a school that coaches parents to conduct themselves at interviews. Laltu is very reluctant at first, but finally gives in to his wife’s persuasions. What follows is poignant as well as hilarious as Laltu tries to learn the language, etiquette and build up confidence to face the toughest of situations. The teacher of the coaching school was a wonderful lady who held their hands and taught them not only how to conduct themselves at interviews but also face difficulties and recognise the worth of life itself! Does Gogol finally get through the interview? Will Mitali be able to fulfil her dreams or does she learn a greater lesson from all this and the importance of her child’s well-being? Gogol, her five-year-old, who loved the song of birds, the flight of kites, the rustling of the leaves, the wide expanse of the blue sky, was his sensitive soul being crushed by the ambitions of his parent? Does Mitali finally look deep into her son’s eye and realise what makes him truly happy? Does she finally follow her heart? Many such moments are captured in the story as it hurtles towards a dramatic climax.

==Cast==
* Gargi Roychowdhury as Mitali Dutta
* Shiboprosad Mukherjee as Laltu Dutta
* Rachna Banerjee as Madam
* Kharaj Mukherjee as Akash Singhania
* Akashneel Mitra as Gogol
* Rajat Ganguly as Sanjus father
* Rumki Chatterjee as Sanjus mother
* Chitra Sen as Mitali’s mother in Bolpur
* Arijit Guha as Mitali’s father in Bolpur
* Sasha Ghoshal Mitali’s brother in Bolpur
* Suzanne Bernert as Mitali’s sister-in-law in Bolpur.

==Crew==
* Produced by Windows & Jalan International Films
* Presented by Atanu Raychaudhuri
* Directed by Nandita Roy & Shiboprosad Mukherjee
* Screenplay & Dialogue by Nandita Roy & Shiboprosad Mukherjee
* Story inspired by Suchitra Bhattacharya
* Director of Photography Sirsha Ray
* Editor Maloy Laha
* Art Director Amit Chatterjee
* Sound by Anirban Sengupta & Dipankar Chaki
* Costume Designer Ruma Sengupta
* Music by Vinit Ranjan Moitra
* Background design by Vinit Ranjan Moitra
* Publicity Design by Saumik & Piyali

==Direction== Accident
Shiboprosad Accident
.

==Casting== Anup Singh Accident
’ in 2012 and ‘Muktodhara’ in 2012 directed by Nandita Roy and himself.      
Rachna Banerjee is a popular actress in Oriya and Bengali Film Industry. She has done many films in Oriya, Bengali, Hindi, Telugu, Tamil and Kannada languages. Her acting career began in 1994. She has acted opposite legendary actors like Amitabh Bachchan in the Hindi film Sooryavansham and Mithun Chakraborty in Oriya and Bengali films. 
Gargi Roychowdhury, a popular actress of the Bengali screen, began her career as a theatre artist in 1995 with Bohurupee Theatre Group. Her first television work was in 1996 with Rituporno Ghosh’s ‘Bahanno Episodes’.  During that time she has also been a newscaster and radio artist. She has been the brand ambassador for many national and international companies.

==Box Office==
After a decent 70% opening on 6 June, Ramdhanu steadily attracted audience, eventually settling for a 100% occupancy across theatres last weekend. It didnt look back. While weekday collections rarely dropped below 70%, this weekend it roared back to a near 100% occupancy. At Priya in south Kolkata, which had a single show, the collections crossed Rs 4.6 lakh in the very first week. Made on a budget of INR 75 lakh, the film opened to a bumper response in the city, notching up in excess of INR 70 lakh by the 3rd weekend and in turn, emerged as the most profitable film of the year so far. It eventually ran for 7-weeks in the direct centres of the city after which it continued for 2 more weeks in the shifted centres.        

==Response== Bengali film, Game (2014 film)|Game. Quoting the Times of India review, "Ramdhanu is a family drama with a touch of innocence and simplicity that will make you want to watch it again."  {{cite web|title=Star-studded Game gets lukewarm response, Ramdhanu a hit|url=
http://timesofindia.indiatimes.com/entertainment/bengali/movies/news-interviews/Star-studded-Game-gets-lukewarm-response-Ramdhanu-a-hit/articleshow/36394092.cms|publisher=The Times of India|accessdate=11 June 2014}}                                                   }                 
                      

==Remake==
Rajesh Nair, director of Malayalam thriller Escape from Uganda, is keen on remaking the film in Malayalam. Shiboprosad also quipped that they are also planning to remake the film in hindi. 

==References==
 

==External Links==
 

==See also==
* Muktodhara Accident
* Icche
* Alik Sukh

 