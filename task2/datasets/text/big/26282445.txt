Abohoman
{{Infobox film
| name = Abohomaan
| image = Abohomaan (2010).jpg
| caption  = Theatrical poster
| director = Rituparno Ghosh
| writer = Rituparno Ghosh
| starring = Dipankar De Jisshu Sengupta Mamata Shankar Riya Sen Ananya Chatterjee
| producer = Big Pictures
| music = 21 Grams
| cinematography = Avik Mukherjee
| editing =Arghya Kamal Mitra
| released =  
| runtime = 122 minutes Bengali
| country = India
}}
Abohomaan ( ) is a 2010 Bengali-language film by Rituparno Ghosh, which explores the nuances of relationships through a married film director who falls in love with an actress who is the same age as his son. {{Cite web|url=http://movies.ibnlive.in.com/news/movies/abohomaan-to-release-on-jan-22/180002/0
|title=Finally, Rituparno Ghoshs next is ready to hit the theatres|publisher=movies.ibnlive.in.com|accessdate=2010-01-16 Big Pictures. National Film Best Director Best Actress respectively. {{Cite web|url=http://www.realbollywood.com/news/2010/09/rituparno-happy-celebrating-abohoman-success.html|title=Rituparno happy, but not celebrating ‘Abohoman’ success|publisher=ReallyBollywood.com|accessdate=2010-09-16
|last=|first=}}  The film was screened in the Marché du Film section of the 2009 Cannes Film Festival. 

Earlier, Rituparno Ghosh had planned to make the film in Hindi as Kya Haseen Sitam, with Naseeruddin Shah, Shabana Azmi and Vidya Balan, later decided to make it in Bengali, as Abahoman. 

==Plot==
Aniket (Deepankar De) is one of the finest filmmakers of Bengal, Deepti (Mamata Shankar) is an actress, with whom he had fallen in love while casting in one of his films, who had sacrificed her career for love and marriage. Apratim (Jisshu Sengupta) is their only son. They had been a perfect family.

The plot thickens when Aniket auditions a young actress, Shikha (Ananya Chatterjee), who bears an uncanny resemblance to his wife when she was younger. Deepti enthusiastically begins to coach Shikha for her husbands film &mdash; so much so that Shikha becomes even more like the young woman Deepti used to be and, as a result, the aging Aniket falls in love with Shikha, a woman as young as his son, despite the sadness and trouble it brings to his family. However, although portrayed as an apparent love between Aniket and Shikha, Shikha was a Muse for Aniket. Her acting skills and her spontaneity mesmerized him. To him, she was Sreemoti, the name which he had first given to Deepti. Deepti later gave this name to Shikha. Even when Sikha became popular in the acting world by the name Sreemoti, Aniket always referred to her as Sikha. For him, Deepti - his wife was the real Sreemoti. This is revealed towards the end of the movie, when Aniket desires to see Sreemoti, because he has to take a shot, and Deepti comes in front of him. This leaves a lot to be explained but the storyline suggests that Aniket was in love with Sreemoti, more than Deepti or Shikha.

==Cast==
* Deepankar De as Aniket
* Mamata Shankar as Deepti
*Jisshu Sengupta as Apratim
*Ananya Chatterjee as Shikha
*Riya Sen as Chandrika

==Awards==
The film has won the following awards since its release:
 National Film Awards (India) Best Director - Rituparno Ghosh Best Actress - Ananya Chatterjee Best Editing - Arghyakamal Mitra
* Won - Silver Lotus Award - National Film Award for Best Feature Film in Bengali

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 