I Met Him in Paris
{{Infobox Film   
| name           = I Met Him in Paris 
| image          = Imethimin paris.jpg 
| caption        = I Met Him in Paris poster
| director       = Wesley Ruggles 
| producer       = Adolph Zukor
| writer         = Claude Binyon Helen Meinardi (story) Robert Young
| music          =  
| cinematography = Leo Tover
| editing        = Otho Lovering
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 86 min.
| country        = USA
| awards         = 
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Robert Young.

==Cast==

* Claudette Colbert as Kay Denham 
* Melvyn Douglas as George Potter  Robert Young as Gene Anders 
* Lee Bowman as Berk Sutter 
* Mona Barrie as Helen Anders  George Davis as Cutter Driver 
* Fritz Feld as Hotel Clerk 
* Rudolph Anders as Romantic Waiter (as Rudolph Amendt)  Alexander Cross as John Hanley 
* George Sorel as Hotel clerk 
* Louis LaBey  as a Bartender 
* Egon Brecher as Emile, upper tower man 
* Hans Joby as the Lower tower man 
* Jacques Vanaire as the  French masher (as Jacques Venaire) 
* Eugene Borden as a Headwaiter

== See also ==
* 1937 in film

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 