Dreams with Sharp Teeth
{{Infobox film
| name           = Dreams with Sharp Teeth
| image          = Dreams-w-sharp-teeth.jpg
| image_size     = 200
| alt            = 
| caption        = DVD cover
| director       = Erik Nelson
| producer       = Erik Nelson Randall M. Boyd
| writer         = Erik Nelson Harlan Ellison
| starring       = Harlan Ellison Robin Williams Neil Gaiman Peter David Ronald D. Moore Josh Olson Tom Snyder Richard Thompson
| cinematography = Wes Dorman
| editing        = Randall Boyd
| studio         = Creative Differences
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Dreams with Sharp Teeth is a 2008 biographical documentary film about writer Harlan Ellison. 
 talking head segments from colleagues and fans including Robin Williams, Peter David, and Neil Gaiman.

==History==
In 1981, then-24-year-old producer Erik Nelson began shooting footage of Ellison while the author was at work on his typewriter. The footage was meant for a PBS segment set to air in March of that year. Ellison allowed Nelson to repeatedly film and interview him over subsequent years, stating that he thought Nelson to be "a fan working on a student project", and has stated that he never suspected that the film would amount to a serious production on such a professional level.

The result of those sessions, and subsequent sessions spanning decades from the original, have been culled and edited, with additions from contemporaries of Ellison into what has become a documentary following a rough arc of Ellison’s life and activities.

Dreams with Sharp Teeth received its first public screening Thursday, April 19, 2007 at the Writers Guild Theatre in Los Angeles.  

The films screenings have been met with critical acclaim by contemporaries of Ellison and Nelson, and the production company is currently searching for distribution to bring it to a larger audience.

The DVD version was released on May 26, 2009. Additional materials on the DVD include a short feature on the films debut screening, a conversation over pizza between Ellison and Gaiman, and readings of extracts from a number of short stories.

==References==
 

==External links==
*  
*   Review at indieWIRE.com
*   Review at filmcritic.com

 
 
 
 
 
 
 

 
 