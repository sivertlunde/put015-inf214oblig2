I nuovi mostri
{{Infobox film name            = I nuovi mostri| image           = I nuovi mostri.jpg| caption         = Italian film poster| director        = Dino Risi Ettore Scola Mario Monicelli| producer        = Pio Angeletti Adriano De Micheli| writer          = Agenore Incrocci Ruggero Maccari Giuseppe Moccia Ettore Scola Bernardino Zapponi| starring        = Vittorio Gassman Ornella Muti Ugo Tognazzi Alberto Sordi| music           = Armando Trovajoli| cinematography  = Tonino Delli Colli| editing         = Alberto Gallitti| distributor     = Titanus| released        =   runtime         = 115 minutes 106 minutes (alternative cut) 102 minutes (French cut) 87 minutes (cut edition) | country         = Italy language        = Italian| budget          =  
|}}

I nuovi mostri (English-language version: Viva lItalia!; meaning of Italian original title: "The new monsters") is a 1977 commedia allitaliana film composed by 14 episodes, directed by Dino Risi, Ettore Scola and Mario Monicelli. It is a sequel of I mostri, made in 1963. It was nominated for the Academy Award for Best Foreign Language Film at the 51st Academy Awards.   

== Plot ==
The film, like the previous one, consists of short episodes that portray the evil and meanness of Italian middle-class society during the years of lead in the 70s.

== Segments ==

===Tantum ergo===
A bishop ( . The bishop goes into the church and is immediately opposed by Protestants; but he by his eloquence slowly manages to deceive the weak minds of the Protestants, who make peace with Christ and the Church in general. The young priest, aware of not being able to "modernize" the thought of those people to Christ, curses Italians and goes away. The bishop, looking at him, smiles victorious.

===Autostop===
A traveler drives up in his car a beautiful girl (Ornella Muti) who is doing hitchhiking. While trying to woo her, the man reads the newspaper, and analyzes the news of a group of girls who escaped from the reformatory attacking a bank. The man thinks that the female traveler on board may be  one of the escaped; the girl threatens him. But she has no intention of threatening the man because she is a criminal, but because the man has shamelessly courted her for the whole trip. The girl brings down the man from the car, pretending to want to run away, but the man reacts to error and kills her.

===With the greetings of friends===
In a small town in Sicily, a mafia boss is mortally wounded by the enemy. When the police rescue him and ask him who was to hurt him, because he did not reveal the secret of the mafia gang, the boss dies in silence, denying even being assaulted.

===Mommy and mammon===
Ugo Tognazzi and Nerina Montagnani are the protagonists of this episode. In Rome two homeless roam the streets, picking up what they can. The police and people avoid them. The poor woman is smart and protective, while the forty-year-old son is a shy and stoned fool who follows her everywhere. At the end of their day of wandering, the two poor tramps return to their home invaded by rubbish. The two try to assemble some junk to sell to people on the street the next day.

===First aid===
Alberto Sordi is a rich snob who is invited to an evening dinner by friends. During the trip he accidentally hits a pedestrian, and decides to do a good deed (though hes very bored by the delay that is doing the dinner), and is on board for the battered him to the hospital. The man is soon forced to make the rounds of all the hospitals in Rome, because no department wants the sick. The episode criticizes the brutality of bureaucratic estab care of Italy, because in the episode the battered invested may not be admitted to the military hospital because he has not done military service, or can not be entrusted to an hospital care has the name of a saint, because the battereds atheist.

===The bird of the Po Valley (Val Padana)===
An unscrupulous manager has a young girlfriend who is very good at singing. She performs with great success in all the night clubs in the Po Valley, and his manager can not but be happy. One day the girl, before retiring, must make one last concert, but gets sick to his throat. The manager is furious and upset, and so as not to lose the money he spent to fund the concert, deliberately knocks the girl down the stairs, making them break a leg. In the evening the girl performs singing, plaster and sick, and arouses such emotion in public that people gives her a lot of money. The manager is very happy.

===The suspect===
A group of young communists do explode a bomb in a city square. The young guys are brought to the police station and the magistrate interrogates and scolds them. Soon, however the man realizes that the young, although they are communists, are all sons of prominent persons (lawyers and doctors), and then he decides to drop the matter by sending everyone home. When young people leave, the judge takes issue with a cop to get his frustrations.

===Pornodiva===
Two parents do to audition the young daughter for a scene in a movie. Too late, the two realize that her daughter is participating in a pornographic flm where she needs to interpret a scene of anal sex with a monkey! The two parents are shocked and call a lawyer to sue the producer of the film; however, the lawyer, to earn a bit of money, he manages to convince parents to recite to her daughter the bad scene.

===Hostaria!===
A group of foreign tourists come into a Roman typical tavern, and order the daily specials. The head waiter (Gassman) takes orders and enters rudely in the kitchen, causing the ire of a cook (Tognazzi), who begins to provoke him. Soon the two take a beating, pulling over each other food. At the end of the fight the two waiters serve the dishes to the tourists, full of rubbish which the two filled them during their fight.

===The model citizen===
During the night a man is witnessing the beating of a victim by a thug. The man, frightened by the scene, runs home and watches the sad lynching by the window, smiling. Exactly the morning of the same day, he had boasted to friends of being an honest and courageous man.

===Kidnapping dear!===
A man makes an announcement on the news, telling viewers to contact him as soon as possible because his wife has been kidnapped. When the announcement ends, the man smiles looking at the thread of his phone voluntarily off the hook.

===Like a queen===
A man, incited by the cruel lover, brings his elderly mother in a nursing home, trying to convince the woman to stay cheerful. But the mother is not happy, because shes aware that the life of the retired hospice is sad and devastating. The woman does not trust even the nurses, and tries to escape, but his son takes her up and forces her to enter the hospice. When the mother is taken away by the nurses, the son has a moment of repentance and cries to the nurses to treat his mother like a queen.

===Without the words===
A beautiful hostess falls in love with a beautiful guy who comes from the middle east. The two spend a beautiful evening of love, and the next day the girl has to fly yet with the aircraft for service to passengers. The man gives her a radio as a sign of farewell, and the hostess, touched, kisses him for the last time. As the plane part, the news announces that a terrorist is going to destroy an aircraft to an Italian airport with a bomb. In fact, the man of the east used the hostess to give her radio with the bomb inside.

===Eulogy===
Is just died a comic theatrical, and his colleagues show accompany him to the grave in the cemetery. Among those present there is a friend of the dead (Alberto Sordi), which weaves a eulogy. Although the situation is very sad, the man who speaks manages to make everyone laugh with his jokes, recalling the happy times in which he improvised in the theater the gags with the deceased. At the end of the eulogy, the man begins to sing and downbeat, and these follow him throughout the cemetery.

== Editions ==
The full version of the film runs about 115 minutes, but upon its release in Italy, the film was censored a few episodes considered too "strong". The length of the cut version of the film lasts about 87 minutes.

== Episodes ==
The original version is composed by 14 episodes.  
{|class="wikitable"
!Episode!!Director!!Starring
|- Ettore Scola||Ugo Tognazzi
|- Dino Risi||
|- Dino Risi||Vittorio Gassman
|- Mario Monicelli||Ornella Muti
|- Ettore Scola||Vittorio Gassman
|- Mario Monicelli||Alberto Sordi
|- Dino Risi||Ugo Tognazzi
|- Ettore Scola||Vittorio Gassman
|- Dino Risi||
|- Ettore Scola||Vittorio Gassman
|- Ettore Scola||Alberto Sordi
|- Ettore Scola||Vittorio Gassman and Ugo Tognazzi
|- Dino Risi||Ornella Muti
|- Ettore Scola||Alberto Sordi
|}

==See also==
* List of submissions to the 51st Academy Awards for Best Foreign Language Film
* List of Italian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 