Head Games (film)
{{Infobox film
| name           = Head Games: The Global Concussion Crisis (2014)
| image          =
| image_size     =
| caption        =
| director       = Steve James
| producer       = Steve James   Steve Devick
| writer         = 
| narrator       =
| starring       = 
| music          = BIlly Corgan Craig J. Snider
| cinematography = Dana Kupper Keith Walker
| editing        = David E. Simpson Katerina Simic Liz Kaar
| studio         = 
| distributor    = 
| released       = March 4, 2014
| runtime        = 75 min
| country        = United States
| language       = English
| gross          = 
}}

{{Infobox film
| name           = Head Games (2012)
| image          =
| image_size     =
| caption        =
| director       = Steve James
| producer       = Bruce Sheridan Steve James Anthony Athanas Casey Cowell Jon Cronin Steve Devick Andrew E. Filipowski Frank J., Jr. & Jacqueline C. Murnane Hank Neuberger Jim ODonovan
| writer         = 
| narrator       =
| starring       = 
| music          = BIlly Corgan Craig J. Snider
| cinematography = Dana Kupper Keith Walker
| editing        = David E. Simpson
| studio         = 
| distributor    = Variance Films
| released       = September 21, 2012
| runtime        = 91 min
| country        = United States
| language       = English
| gross          = 
}}
 associated with American football and hockey, but also covers Dementia pugilistica|boxing, soccer, lacrosse, and professional wrestling. It covers findings that chronic traumatic brain injury is occurring in female sports. Also covered is physiological evidence of brain injury in adolescent athletes.
 Steve James, director of the highly acclaimed documentary, Hoop Dreams.  It is film followup to Christopher Nowinskis book, Head Games. 

The film features interviews with Nowinski (founder of the Sports Legacy Institute), Dr. Robert Cantu (a professor of neurosurgery at Boston University School of Medicine, Dr. Ann McKee, and Robert Stern, who are experts on chronic traumatic encephalopathy). In addition to other medical experts, it also extensively interviews athletes, their families, and journalists.

== Details ==

=== Cast ===
Head Games: The Global Concussion Crisis (2014)

*Bob Costas
*Brendan Shanahan 
*Robert Cantu, MD
*Ann McKee, MD 
*Robert Stern, PhD
*Hunt Batjer, MD 
*Gary Dorshimer, MD
*Ruben Echemendia, PhD
*Douglas Smith, MD
*Steven Galetta, MD
*Laura Balcer, MD, MSCE
*Christina Master, MD
*Dr. Willie Stewart
*Dr. Barry ODriscoll
*Dr. James Robson
*Greg "Diesel" Williams
*Dr. Alan Pearce
*David Dodick, MD
*Dr. Huw Williams

Head Games (2012)

*Christopher Nowinski
*Alan Schwarz
*Keith Primeau
*Cindy Parlow Cone
*Bob Costas
*Isaiah Kacyvenski
*Bill Daly
*Brendan Shanahan
*Robert Cantu, MD
*Ann McKee, MD
*Robert Stern, PhD
*Hunt Batjer, MD
*Gary Dorshimer, MD
*Ruben Echemendia, PhD
*Douglas Smith, MD
*Steven Galetta, MD
*Laura Balcer, MD, MSCE
*Christina Master, MD
*Eric Laudano, M.H.S., A.T.C.

=== Title ===
Head Games was inspired by the book Head Games written by former Ivy League football player and WWE wrestler Christopher Nowinski.

== Critical and Media Reception ==
Head Games was a critical success, winning Best Documentary at the 2012 Boston Film Festival and Sports Illustrated Best Sports Movie of 2012. Head Games was also an official selection for both the 2012 International Documentary Film Festival Amsterdam and the 2012 Sprout Film Festival. The films was also noted in iTunes Best of 2012 and Rotten Tomatoes Top Movies of 2012. 

Roger Ebert gave the film three stars noting that "the documentary by Steve James paints a devastating picture of the long-term consequences of head injuries among pro NFL players."   Ebert also called Head Games one of the years "best documentaries."   The New York Times praises Head Games stating "Head Games gains credibility and power from compassion for athletes and respect for their accomplishments. But it also tries to open the eyes of sports lovers to dangers that have too often been minimized and too seldom fully understood."   The Pittsburgh Post Gazette listed the film as one of the years best films. 

In March 2014, UK Distributor Dogwoof announced that it would be a launching doc-centric digital distribution platform entitled IF365 to help filmmakers get their work on top digital platforms. IF365 will use Dogwoof’s relationship with platforms such as iTunes and Netflix to showcase feature documentaries including Head Games: The Global Concussion Crisis. 

==References==
 

==External links==
* 
* 
* 
*  (Globe & Mail)
*  (Slate)
*  (AV Club)
*  (CBS Sports)
*  (Democracy Now!)

 

 
 
 
 
 
 
 
 
 
 
 
 