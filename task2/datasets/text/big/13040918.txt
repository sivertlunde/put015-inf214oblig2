Cosmic Zoom
{{Infobox Film
| name           = Cosmic Zoom
| image          = 
| image_size     = 
| caption        = 
| director       = Eva Szasz
| producer       = Joe Koenig   Wolf Koenig   Robert Verrall
| writer         = 
| narrator       = 
| starring       = 
| music          = Pierre F. Brault
| cinematography = 
| editing        = 
| distributor    = National Film Board of Canada
| released       = 1968
| runtime        = 8 minutes
| country        = Canada
| language       = 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Cosmic Zoom is a 1968 short film directed by Eva Szasz and produced by the National Film Board of Canada. It depicts the relative size of everything in the universe in an 8-minute sequence using animation and animation camera shots.

==Synopsis==
The film starts with an aerial image of a boy rowing a boat on the Ottawa River. The movement then freezes and view slowly zooms out, revealing more of the landscape all the time. The continuous zoom-out takes the viewer on a journey from Earth, past the Moon, the planets of the Solar System, the Milky Way and out into the far reaches of the known universe. The process is then reversed, and the view zooms back through space to Earth, returning to the boy on the boat. It then zooms in to the back of the boys hand, where a mosquito is resting. It zooms into the insects proboscis and on  into the microscopic world, concluding at level of an atomic nucleus. It then zooms back out to the original view of the boy on the boat.

==Inspiration and follow ups== Powers of Ten (updated in 1977) used the same idea and techniques, as did the 1996 IMAX film Cosmic Voyage.   

==Release==
Cosmic Zoom was one of seven NFB animated shorts acquired by the American Broadcasting Company, marking the first time NFB films had been sold to a major American television network. It aired on ABC in the fall of 1971 as part of the children’s television show Curiosity Shop, executive produced by Chuck Jones.   

==See also==
*Earths location in the universe

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 