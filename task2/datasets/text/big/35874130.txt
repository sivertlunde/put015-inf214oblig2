Won't Back Down (film)
{{Infobox film
| name           = Wont Back Down
| image          = Wont_Back_Down_Poster.jpg
| caption        = Theatrical poster
| director       = Daniel Barnz Mark Johnson
| writer         = Brin Hill Daniel Barnz
| starring       = Maggie Gyllenhaal Viola Davis Holly Hunter Oscar Isaac Rosie Perez Ving Rhames Marianne Jean-Baptiste
| music          = Marcelo Zarvos
| cinematography = Roman Osin
| editing        = Kristina Boden
| studio         = Walden Media Gran Via Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 121 minutes   
| country        = United States
| language       = English
| budget         = $19 million 
| gross          = $5.4 million 
}}
Wont Back Down (previously titled Still I Rise, Learning To Fly and Steel Town) is a drama film directed by Daniel Barnz starring Maggie Gyllenhaal, Viola Davis and Holly Hunter. It was released on September 28, 2012. 

== Plot ==
Two determined mothers, a car dealer/bartender (Maggie Gyllenhaal) and a teacher (Viola Davis), look to transform their childrens failing inner city school. Facing a powerful and entrenched bureaucracy and corruption from the teachers union president (Holly Hunter) and the schools principal (Bill Nunn), they risk everything to make a difference in the education and future of their children. 

== Cast ==
*Maggie Gyllenhaal as Jaime Fitzpatrick
*Viola Davis as Nona Alberts
*Ned Eisenberg as Arthur Gould
*Bill Nunn as Principal Holland
*Holly Hunter as Evelyn Riske
*Oscar Isaac as Michael Perry
*Rosie Perez as Brenna Harper
*Lance Reddick as Charles Alberts
*Ving Rhames as Principal Thompson
*Marianne Jean-Baptiste as Olivia Lopez

== Production ==

=== Background ===
 .]] parent trigger law in Sunland-Tujunga, Los Angeles, California in 2010, where several groups of parents attempted to take over several failing public schools. The Parent Trigger law, which was passed in California and other states in 2010, allowed parents to enforce administrative overhaul and overrule administrators in under-performing public schools if petitioned.  If successful, petitions allow parents to direct changes such as dismissal of staff and potential conversion of a school to a charter school.    

=== Release === American educational system,  produced the film, with 20th Century Fox releasing it on September 28, 2012.    American actresses Maggie Gyllenhaal and Viola Davis were among the first to be cast,  with Academy award-winning actress Holly Hunter being cast later on.  The film marked Hunters first film appearance in seven years since The Incredibles and The Big White. The films trailer was released on May 17, 2012. 
The films budget was $25 million, not counting the undisclosed amount for marketing the film.

===Promotional campaign===
Private foundations and the United States Chamber of Commerce|U.S. Chamber of Commerce have contributed more than $2 million for a publicity campaign for the film. Television ads, bookmarks, websites and private screenings a six-month cross-country tour will promote the film. Promoters have scheduled private screenings in states from New York to Georgia and Utah, to promote the movie and its parent trigger message. 

== Reception ==

===Box office===
The film grossed just $5.3 million at the box office, and, according to   

===Critical response===
The film has received mixed reviews from critics, as Wont Back Down currently holds a 33% rating on Rotten Tomatoes based on 97 reviews.  Variety (magazine)|Variety called the film a "heavy-handed inspirational drama" that "grossly oversimplifies the issue at hand." The site continued, "Barnzs disingenuous pot-stirrer plays to audiences emotions rather than their intelligence, offering meaty roles for Maggie Gyllenhaal as a determined single mom, and Viola Davis as the good egg among a rotten batch of teachers, while reducing everyone else to cardboard characterizations. Absent high-profile champions, femme-centric pic could suffer from low attendance."   Michael Medved liked the film, giving it three and a half stars (out of four) and calling it "..one of the better films of 2012." 
 Republican and Democratic Party 2012 national conventions several weeks before its theatrical release. 

===Controversy===
Some critics have contended that the film is an ideological vehicle of conservative activist Philip Anschutz and that the film is slanted to promote the parent trigger movement.  

On the other hand, some critics have contended that the movie shows a watered-down version of what parents are really up against when trying to implement the Parent Trigger law.  

==Accolades==
Viola Davis won the NAACP Image Award for Outstanding Actress in a Motion Picture for her role as Nona Alberts; and she was nominated for a Black Reel Award for Best Actress for her role.

== See also ==
* Education in the United States
* Waiting for "Superman" The Lottery

==Home Media==
Wont Back Down was released on DVD and Blu-ray on January 15, 2013

== References ==
 

== External links ==

*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 