The Incredible Petrified World
{{Infobox Film
| name           = The Incredible Petrified World
| image          = Incrediblepetrifiedworld.jpg
| caption        = A promotional film poster for The Incredible Petrified World
| director       = Jerry Warren
| producer       = Jerry Warren
| writer         = John W. Steiner
| starring       = John Carradine Robert Clarke Phyllis Coates Allen Windsor Sheila Noonan
| music          = Josef Zimanich
| cinematography = Victor Fisher  James Sweeney
| distributor    = Governor Films Inc.
| released       = 1960
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Incredible Petrified World is a 1958 science fiction film directed by Jerry Warren and starring John Carradine. It was only theatrically released on April 16, 1960, on a double bill with Teenage Zombies. 

==Plot outline==
Professor Millard Wyman’s (John Carradine) sends a crew of two men, Paul Whitmore (Allen Windsor) and Craig Randall (Robert Clarke), and two women Lauri Talbott (Sheila Noonan) and Dale Marshall (Phyllis Coates), down to ocean depths never before explored.  But, there’s a technical problem during the launch and the mission is believed lost.

Miraculously the crew survives the mishap.  However, they fear that their inevitable deaths are only postponed because they broke free of the cable connecting them to the surface and lost communication.  Someone spots light out the bell’s window.  They don’t understand it but they believe they moved up from where the mishap began.  They determine that the pressure should be tolerable at a depth where light can be seen.  They don their scuba gear and leave the bell.  Instead of reaching the surface they surface in a cave.  The crew explores the cave and finds a giant lizard.  They also find a skeleton, then a living man tells the crew that he suffered a shipwreck fourteen years prior and found these caves after sinking into the ocean.  He claims there is no way out and a volcano provides air to the caves.

Meanwhile, men working on the mission from the surface with sonar discover some unusual shapes moving near the doomed diving bell.  Their bosses think it’s probably nothing.  Prof. Wyman’s younger brother builds a bell but the launch is canceled.  Prof. Wyman shows the man in charge of his brother’s mission modifications that he’s developed from the mistakes he made with the first bell.  His pitch works and the second bell is launched.  However, with the man in the cave becoming more suspicious and the volcano grows more unstable the second mission may not find them in time.

==Production and release==
In an interview, star Robert Clarke said that the films cinematographer is actually a well-known Hollywood cameraman who used the pseudonym "Victor Fisher" so he wouldnt get in trouble with the union for taking a job on this non-union picture.  The cavern sequences were shot at Colossal Cave in Tucson, AZ. 
The film was completed around March 1957. It remained unreleased for 2 years until it was put on the bottom of a double feature with another of Jerry Warrens films Teenage Zombies. 
Phillys Coate was never paid by Jerry Warren for her role in the film.

==See also==
* List of films in the public domain

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 


 