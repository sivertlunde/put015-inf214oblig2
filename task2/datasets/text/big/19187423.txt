The Eternal City (1915 film)
{{infobox film
| name           = The Eternal City
| image          =
| caption        = Hugh Ford
| producer       = 
| based on       =   
| starring       = Pauline Frederick
| music          =
| cinematography =
| studio         = Famous Players Film Company
| distributor    = Select Film Booking Agency
| released       = April 12, 1915 December 15, 1918 (US re-release)
| runtime        = 80 mins.
| country        = United States Silent English English intertitles
}}
 silent drama Hugh Ford and Edwin S. Porter, produced by Adolph Zukor and based upon the novel and play of the same name by Hall Caine. The film was released through Paramount Pictures for the Famous Players Film Company, and is based on the 1902 Broadway play that starred Viola Allen and Frederic De Belleville.    The film is now considered Lost film|lost.   
 in 1923, directed by George Fitzmaurice and starring Barbara La Marr, Bert Lytell, and Lionel Barrymore. 

==Cast==
*Pauline Frederick - Donna Roma
*Thomas Holding - David Rossi
*Kittens Reichert - Little Roma
*Arthur Oppenheim - Little David
*George Stillwell - Leone
*Della Bella - Leones wife
*Frank Losee - Baron Bonelli
*Fuller Mellish - Pope Piux XI
*J. Jiquel Lanoe - Charles Minghelli
*George Majeroni -Dr. Roselli
*John Clulow - Bruno Rocco
*Amelia Rose - Elena Rocco
*Freddie Verdi - Joseph Rocco
*Lottie Alter - Princess Bellini
*Lawrence Grant - English ambassador

==Production notes==
The film was shot on location in London and at the Roman Forum, the Colosseum, and the Vatican gardens in Rome, Italy. Production was interrupted by World War I, and the remainder of the film was shot in New York City.  

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 