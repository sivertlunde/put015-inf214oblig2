Appointment with Fear (film)
 
{{Infobox film
| name           = Appointment with Fear
| image          = Appointment with Fear.jpg
| caption        =
| director       = Alan Smithee (Ramsey Thomas)
| producer       = Moustapha Akkad Tom Boutross
| writer         = Bruce Meade Ramsey Thomas
| narrator       =
| starring       = Douglas Rowe Michele Little Kerry Remsen Pamela Bach Garrick Dowhen
| music          = Andrea Saparoff
| cinematography = Nicholas von Sternberg
| editor         = Paul Jasiukonis
| studio         = Moviestore Entertainment Trancas International Films
| distributor    = New World Pictures
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         =
}}
 Appointment with Fear, also known as Deadly Presence, is a 1985 American film directed by Ramsey Thomas for Moustapha Akkad and Moviestore Entertainment.  The film stars  Michele Little, Douglas Rowe, Garrick Dowhen, and Kerry Remsen.

The film had theatrical release in 1985 by New World Pictures and release on VHS in October 1987 by International Video Entertainment, followed in 1991 by Lions Gate Films Home Entertainment.     Ramsey Thomas had his name removed from the project, replacing it with the pseudonym Alan Smithee. 

==Plot==
A man (Garrick Dowhen) in a white Ford van gets out and stabs his wife on the portico of a house. Before she dies, she gives her baby to Heather (Kerry Remsen) and urges her that he must be kept safe, knowing that his father will try to kill him. The case is followed by  Detective Kowalski (Douglas Rowe), an off-beat, seedy looking detective. Kowalski later finds out the killer is in solitary confinement in the state mental facility but is somehow leaving his body in spirit and is under an Ancient Egyptian curse which gives him a need to kill his baby to be "King of the Forest" for another year. Kowalski later visits a specialist seeking advice on how to confront the Ancient Egyptian spirit and curse. Carol (Michele Little), a friend of Heather with a love of recording sounds, spots the white van following her in her pickup truck and becomes suspicious. She informs her love interest Bobby (Michael Wyle) of her fears and he dismisses them and punctures the tire of the van which is parked nearby.

Heather takes the baby to a luxury mansion house, where she takes care of it with Carol. She has a dream in which she predicts the murder of the baby and becomes extremely anxious. Detective Kowalski meanwhile has his car hijacked in the wilderness by the killer spirit and blown to smithereens. The killer learns of the babys whereabouts and unknown initially to Carol,  kills another of her friends and an old vagrant who lives in the back of her truck at the house. He then murders Samantha (Pamela Bach), another of Carols friends in the jacuzzi. After discovering Samantha, Carol runs outside with a shotgun and shoots at the white van, although the killer is not present. Then, after discovering the body of her other friend, Carol remarks that she thinks the killer is trying to kill the baby. Carol orders Bobby to go upstairs and to protect Heather and the baby and vows to "kill the bastard". Bobby discovers that Heather is missing and the baby is alone and then departs on his bike/side car and finds the detective. The killer breaks in through a window and is set on fire by Carol, prompting the spirit to leave the burning body and manifest himself again. The killer is then seen leaving the house with the baby in his arms and is confronted by Carol with a shot gun and orders him to give up the baby. When he refuses and lays the baby down by a tree and attempts to perform a ritual, Carol shoots him several times, with no effect. Bobby and the detective arrive on the scene and the detective urges her to pierce the killer with a nearby pole. As she does so a dramatic scene occurs with a flash to the body in the mental ward and it explodes and is followed by a strong wind where the spirit had been. Bobby presents the baby in safe arms to Heather who remarks "Isnt he beautiful". As she looks away supernatural green lights appear in the babys eyes as the credits roll.

==Partial cast==
*Douglas Rowe as Detective Kowalski
*Michele Little as Carol
*Kerry Remsen as Heather
*Garrick Dowhen as The man
*Pamela Bach as Samantha  James Avery as Connors
*Danny Dayton as Norman
*Mike Gomez as Little Joe 
*Debi Sue Voorhees as Ruth
*Peter Griffin as George 
*Michael Wyle as Bobby
*Vincent Barbour as Cowboy
*Gertrude Clement as Old Woman 
*Nick Conti as Young Detective
*Brioni Farrell as Mrs. Sorenson
*Kathi Gibbs as Cleo 
*Charlotte Speer as Mrs. Pierce

==References==
 

==External links==
*  
 

 
 
 
 
 
 