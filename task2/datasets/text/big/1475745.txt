Marion Bridge (film)
{{Infobox film
| name           = Marion Bridge
| image          = Mb poster.jpg
| image size     = 200
| caption        = Theatrical release poster
| director       = Wiebke von Carolsfeld
| producer       =
| writer         = Daniel MacIvor
| narrator       = Stacy Smith   Marguerite McNeil   Ellen Page
| music          =
| production design = Bill Fleming
| cinematography =
| editing        =
| distributor    =
| released       = September 7, 2002   (Toronto Film Festival)
| runtime        = 90 min.
| country        = Canada English
}}
 2002 Canada|Canadian film directed by Wiebke von Carolsfeld.  It was selected as the Best Canadian First Feature Film at the 2002 Toronto International Film Festival. Based on a play by Daniel MacIvor. 

==Plot== Stacy Smith), the middle child who has retreated from the outside world. Her arrival sets in motion a chain of events that allows the family to reconnect with the world and one another.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 