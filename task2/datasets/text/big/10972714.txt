Brighton Rock (1947 film)
 
 
{{Infobox film
| name           = Brighton Rock
| image          = Brighton Rock.jpg
| caption        =
| director       = John Boulting
| producer       = Roy Boulting
| writer         = Graham Greene Terence Rattigan
| based on       =  
| starring       = Richard Attenborough Hermione Baddeley William Hartnell Carol Marsh
| music          = Hans May
| cinematography = Harry Waxman	
| editing        = Peter Graham Scott
| distributor    = Charter Film Productions
| released       = December 1947
| runtime        = 92 min
| country        = United Kingdom
| language       = English
| budget         =
| gross          = £190,147 (UK) 
}}

Brighton Rock is a 1947 British film noir directed by John Boulting and starring Richard Attenborough as Pinkie (reprising his breakthrough West End creation of the character some three years earlier),  Carol Marsh as Rose, William Hartnell as Dallow and Hermione Baddeley as Ida. 
 Brighton Rock by Graham Greene. In the United States, Brighton Rock was retitled Young Scarface.

==Plot== psychopathic teen-aged hoodlum known as "Pinkie Brown|Pinkie."  The films real-life theme was the race-track gangs of the 1930s, which fought public battles with straight razors in their competition to control crime at racecourses in southern England.  One of these racecourses was at Brighton, a popular seaside resort.
 climax of the film takes place at the Palace Pier, which differs from the novel, the end of which takes place in the nearby town of Peacehaven.

In the story, Pinkie is the strong arm for a small gang that operates at Brightons race track. After a newspaper reporter named Fred writes a story that gets the gang leader killed by rivals, Pinkie takes over leadership and tracks Fred down, murdering him on an amusement park ride. The police think it a heart attack or suicide. But Pinkie still wants to cover his tracks and gets one of his gang members, Spicer, to take care of the matter. But Spicer accidentally leaves a clue that could unravel the murder—a clue that a waitress named Rose discovers. So Pinkie murders Spicer. He also courts and marries Rose so she can’t testify against him. But then, with self-appointed amateur sleuth Ida Arnold asking a lot of questions about Freds murder, and the larger rival gang taking over racetrack racketeering, Pinkie decides he needs to kill Rose, too, before leaving town. So he convinces her that he will soon be caught and go to the gallows, and therefore the two should commit suicide together. He gives Rose a gun with which to kill herself, promising he’ll kill himself next. Out of love for Pinkie she reluctantly agrees and almost follows through. But a member of Pinkies gang turns on him and helps the police stop her just in time before Pinkie commits suicide by throwing himself off the pier. But afterwards Rose persists in believing that Pinkie really loved her.

==Cast==
 
* Richard Attenborough as Pinkie Brown
* Hermione Baddeley as Ida Arnold
* William Hartnell as Dallow
* Carol Marsh as Rose Brown
* Harcourt Williams as Prewitt
* Wylie Watson as Spicer Nigel Stock as Cubitt
* Victoria Winter as Judy
* Reginald Purdell as Frank
* George Carney as Phil Corkery
* Charles Goldner as Colleoni
* Alan Wheatley as Fred Hale
* Lina Barrie as Molly
* Joan Sterndale-Bennett as Delia
* Harry Ross as Bill Brewer
* Campbell Copelin as Police Inspector
* Marianne Stone as Lazy Waitress
* Norman Watson as Racecourse Evangelist
* Ronald Shiner as the Look – Out (uncredited)
 

==Reception==
At the time of its release, Brighton Rock received critical acclaim and was the most popular British film of 1947. The film currently scores 100% on Rotten Tomatoes.
The film was banned in New South Wales. 

==New adaptation==
 
A new adaptation of the novel, written and directed by Rowan Joffé, was released in the United Kingdom on 4 February 2010. Joffe has moved the setting from the 1930s to the 1960s, during the mods and rockers era.  

==Revival==
The original film had a run at Film Forum in New York City 19–26 June 2009, and The New York Times previewed the revival, saying "both   Catholicism and his movie-friendliness are in full cry in John Boultings terrific 1947 gangster picture." 

==See also==
* BFI Top 100 British films

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 