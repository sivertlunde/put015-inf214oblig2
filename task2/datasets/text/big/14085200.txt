Ograblenie po...
{{Infobox Film name = Ograblenie po… image = Ograblenie po - openning frame.png image_size = caption =  director = Yefim Gamburg producer =  writer = Mikhail Lipskerov narrator =  starring =  music = Gennadi Gladkov cinematography =  editing =  distributor = Soyuzmultfilm released = 1978, 1988 runtime = 18 min 5 sec country = Soviet Union language = Russian budget =  gross =  followed_by = 
}}
…-styled robbery ( ,   of crime films of the 1960s and 1970s in the corresponding countries. The animated characters parody popular actors of that era—Louis de Funès, Sophia Loren, Saveli Kramarov, and others.
 Hollywood action films with exaggerated explosions, car chases, striptease, violent deaths and a cold-blooded corrupt Sheriff character based on Marlon Brando. The French part parodies film noir, and its characters are based on Jean Gabin, Fernandel, Alain Delon, Brigitte Bardot and Louis de Funès. The Italian part parodies colorful Italian mafia films with characters based on Marcelo Mastroianni and Sophia Loren.
 Soviet economy—the robbers are trying to rob a branch of Sberkassa, but they cannot do it because it is always closed for cleanup or repairs. This last part was not shown in the USSR (see Censorship in the USSR) because of the problematic satire and because Saveli Kramarov emigrated to the USA by the time the film was completed. The full version was re-released only in 1988, in the time of Glasnost.
 MGM lion.

== Voice cast ==
*Vsevolod Larionov Gennadi Morozov

== External links ==
*  (1978) at Animator.ru
*  (1988) at Animator.ru
* 
* 

 

 
 
 
 
 
 
 
 
 


 