Congorama
{{Infobox film
| name           = Congorama
| image          = Congorama Poster.jpg
| caption        = Christal Films theatrical poster for Congorama
| director       = Philippe Falardeau
| producer       = Joseph Rouschop Éric Tavitian
| writer         = Philippe Falardeau
| starring       = Olivier Gourmet Paul Ahmarani Jean-Pierre Cassel Gabriel Arcand Lorraine Pintal Claudia Tagbo Arnaud Mouithys : Jules
| music          = Jarby Mc Coy
| cinematography = André Turpin
| editing        = Frédérique Broos 
| distributor    = Benelux Films (Netherlands - theat.) Dutch Filmworks (Netherlands - DVD) Christal Films (Canada) U.G.C. PH (France)
| released       =  
| runtime        = 105 minutes
| country        = Canada, Belgium, France
| language       = French
| budget         =  , accessed 2006-09-13 
| budget         =
}}
 Congorama  is a Canadian film directed by Philippe Falardeau, released in 2006.

==Plot==
Michel is a Belgian inventor. He cares for his father, a paralysed writer, is married to a Congolese woman and is the father of an interracial child whom he reassures as to his parentage. He discovers at the age of 41 that he was adopted, actually having been born in Sainte-Cécile, Quebec. In the summer of 2000, he travels to Quebec, supposedly to sell some of his inventions. While on a near-impossible quest to find his birth family in the town where he was born, he crosses paths with Louis Legros, son of another inventor, in a meeting which will change their lives.

==Premiere==
Congorama received its world premiere at the  , accessed 2008-04-12 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 