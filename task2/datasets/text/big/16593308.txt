American Cousins
{{Infobox Film
| name           = American Cousins
| image          = AmericanCousinsPoster.JPG
| caption        = DVD Cover
| director       = Don Coutts
| producer       = Margaret Matheson
| writer         = Sergio Casci
| starring       = Danny Nucci Gerald Lepkowski Shirley Henderson Vincent Pastore Dan Hedaya Russell Hunter Olegar Fedoro Stevan Rimkus Donald Shaw
| cinematography = Jerry Kelly
| editing        = Lindy Cameron
| distributor    = 
| released       = 28 November 2003
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  	
| followed_by    = 
}}
 The List magazine. 

==Plot summary==
Two New Jersey mobsters are almost killed when a deal in Eastern Europe goes wrong. They take refuge with their law-abiding cousin Bobby,  who runs a fish and chip shop in Glasgow, Scotland. But when one of the Americans takes a shine to Bobbys girl and the other clashes with local hoodlums, Bobby realises he has to stand up for himself or risk losing everything he loves.

==Cast==
* Dan Hedaya
* Danny Nucci
* Vincent Pastore
* Russell Hunter
* Shirley Henderson
* Gerald Lepkowski
* Olegar Fedoro

==Production==
American Cousins was the debut feature for director   Film Festival award for Best European Short Film. 
 Best Foreign Language Film. 

==Awards==
*Audience Award BAFTA Scotland
*Best Screenplay BAFTA Scotland (Sergio Casci)
*Best Actress Cherbourg-Octeville Festival (Shirley Henderson)
*Jury Award Newport Beach Film Festival
*Best Film Milan International Film Festival
*Best Director Milan International Film Festival (Don Coutts) 
*Audience Award Milan International Film Festival

Screenwriter Sergio Casci was also nominated for the BAFTA Carl Foreman Award.

== References ==
 

== External links ==
*  
*  
*  
*  

 
 
 
 