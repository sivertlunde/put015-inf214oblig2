The Frozen Dead
{{Infobox film
| name           = The Frozen Dead
| image          = The Frozen Dead 1966 poster.jpg
| image_size     = 300
| caption        = 1967 US Theatrical Poster
| director       = Herbert J Leder
| writer         = Herbert J. Leder
| narrator       = 
| starring       = Dana Andrews Anna Palk Philip Gilbert
| music          = Don Banks
| cinematography = 
| editing        =  Seven Arts Seven Arts (US)
| released       = October 1966 (UK) 15 November 1967 (US gen. release)
| runtime        = 95 min 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
 science fiction horror film directed by Herbert J. Leder and starring Dana Andrews, Anna Palk and Philip Gilbert.  In this film, a Nazi scientist plans to revive a number of frozen Nazi leaders. John Hamilton, The British Independent Horror Film 1951-70 Hemlock Books 2013 p 178-180 

==Cast==
* Dana Andrews - Dr. Norberg 
* Anna Palk - Jean Norburg 
* Philip Gilbert - Dr. Ted Roberts 
* Kathleen Breck - Elsa Tenney 
* Karel Stepanek - General Lubeck 
* Basil Henson - Dr. Tirpitz 
* Alan Tilvern - Karl Essen 
* Anne Tirard - Mrs. Schmidt  Edward Fox - Norburgs Brother (Prisoner #3) 
* Oliver MacGreevy - Joseph the Butler 
* Tom Chatto - Inspector Witt  John Moore - Bailey the Stationmaster  Charles Wade - Alfie the Porter

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 