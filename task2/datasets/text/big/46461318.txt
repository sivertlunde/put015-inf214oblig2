Burning Blue
 

{{Infobox film
| name           = Burning Blue
| image          = 
| alt            = 
| caption        = Theatrical release poster
| director       = DMW Greer
| producer       = John Hadity Arthur J. Kelleher Andrew Halliday
| writer         = DMW Greer Helene Kvale
| starring       = Trent Ford Rob Mayes
| music          = James Lavino
| editing        = Bill Henry
| distributor    = Lionsgate Entertainment
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
|}}

Burning Blue is a 2013 American drama film, directed by DMW Greer and starring Trent Ford and Rob Mayes. It is based on the 1992 play by Greer about a US Navy accident investigation which becomes a gay witch hunt during the "Dont Ask, Dont Tell" era.

==Plot==
US Navy pilots Lts. Daniel Lynch (Trent Ford) and Will Stephensen (Morgan Spector) are best friends hoping to both become the youngest pilots to be accepted into the space program. After two accidents, one of which is due to Wills failing eyesight, their unit is subject to an NCIS investigation led by John Cokely (Michael Sirow). At the same time, a third pilot, Matt Blackwood (Rob Mayes) arrives on the carrier and quickly develops a close friendship with Dan, driving a wedge in between Dan and Will. Cokelys investigation leads to him uncovering rumours about Dan and Matts relationship just as they both begin to fall in love. When Matt decides to leave his wife and move in with Dan there is a third accident and Cokelys investigation ramps up the pressure on Dan.

==Cast==
 
* Trent Ford as Daniel Lynch
* Rob Mayes as Matthew Blackwood
* Morgan Spector as William Stephensen
* William Lee Scott as Charlie Trumbo
* Michael Sirow as John Cokely
* Cotter Smith as Admiral Lynch
* Tammy Blanchard as Susan
* Tracy Weiler as Nancy
* Gwynneth Bensen as Tammi
* Mark Doherty as Skipper
* Chris Chalk as Special Agent Jones
 

==Reception==
The film has received generally negative reviews, with a score of 28% in review aggregator Rotten Tomatoes.  The writing and directing were criticised but Ford and Mayes were praised for their portrayals. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 