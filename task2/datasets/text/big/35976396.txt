Shakespeare: The Hidden Truth
  UK in 2013.

The film follows the Norwegian organist and amateur cryptologist Petter Amundsen. Amundsen claims, after a thorough investigation of Shakespeare’s works, to be able to prove that in fact several people were involved the writing of what is today known as the works of William Shakespeare. 

When released in Norway, the film spurred controversy in Norwegian media.     Film Magasinet scored the film 4/6 stars. 

==Plot==
Norwegian Petter Amundsen has scrutinized the first editions of Shakespeares works, and claims to have discovered an intricate code system hidden in the books. He furthermore insists on that these codes prove that Shakespeare was in fact not the writer we know today, but a name a secret brotherhood chose to hide behind. Amundsens theories have been rejected by several Shakespeare experts, but no one has rejected them as fiercely as Robert Crumpton. Crumpton is determined to prove Amundsens theories wrong, and Amundsen has decided to convince Crumpton of the opposite. 

==Reception in Norway==
The film received mixed reviews in Norway.   

== External links ==
*  (official website)
*  on IMDb

==References==
 

 
 
 
 
 


 
 