A Submarine Pirate
 
{{Infobox film
| name           = A Submarine Pirate
| image          = A Submarine Pirate 1915.jpg
| caption        = Syd Chaplin at the periscope Syd Chaplin
| producer       = Mack Sennett
| writer         = Mack Sennett
| starring       = Syd Chaplin
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States Silent English intertitles
| budget         =
}}
  short comedy film starring Syd Chaplin and featuring an early uncredited appearance by Harold Lloyd.   

==Cast== Syd Chaplin as Ambitious waiter
* Wesley Ruggles as The inventors accomplice
* Glen Cavender as A shrewd inventor
* Phyllis Allen as A pugnacious guest
* Virginia Fox
* Edgar Kennedy
* Harold Lloyd as Cook
* Heinie Conklin
* Fritz Schade
* Ted Edwards as Waiter (uncredited)
* Charles Lakin as Desk Clerk (uncredited)

==See also==
* Harold Lloyd filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 


 