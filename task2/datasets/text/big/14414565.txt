Confession (1937 film)
{{Infobox film
| name           = Confession
| image          = Confession 1937 poster.jpg
| caption        = 1937 US Theatrical Poster
| director       = Joe May
| producer       = Hal B. Wallis (uncredited exec. producer) Jack L. Warner (uncredited exec. producer)
| writer         = Hans Rameau Julius J. Epstein (English adaptation) Margaret P. Levino (English adaptation)
| narrator       = Ian Hunter Basil Rathbone Jane Bryan
| music          =
| cinematography = Sidney Hickox
| editing        =
| distributor    = Warner Brothers
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = $500,000 (est.) 
| gross          =
}}
 Ian Hunter, Basil Rathbone and Jane Bryan.  It was directed by Joe May and is a scene-for-scene remake of the 1935 German film Mazurka (film)|Mazurka starring Pola Negri, which Warner Brothers Studios acquired the U.S. distribution rights for and then shelved in favour of the remake.

With an estimated $513,000 budget, it started production in March 1937 and was released August 19, 1937, in New York City.

==Cast==
*Kay Francis as Vera Ian Hunter as Leonide
*Basil Rathbone as Michael Michailow
*Jane Bryan as Lisa
*Donald Crisp as the Presiding Judge
*Mary Maguire as Hildegard
*Dorothy Peterson as Mrs. Koslov
*Laura Hope Crews as Stella
*Robert Barrat as the Prosecuting Attorney
*Ben Welden as the Defense Attorney
*Veda Ann Borg as Xenia
*Joan Valerie as Wanda (credited as Helen Valkis)

==Plot== conservatory lessons instead, she discovers that he has lied to the professor to insinuate himself as her tutor.  Michialow kisses Lisa, who despite awareness that the situation is unsavory, responds to the kiss.

The third day, when her mother returns, Michialow calls Lisa at home and persuades her to sneak out. He takes her to a seamy cabaret to continue his patient seduction where he wont be recognized.  During a suggestive number sung by tawdry chanteuse Vera Kowalska (Francis), the couple are illuminated in a spotlight as Michailow again kisses Lisa. Vera and Michialow recognize one another and she faints from shock. He tries to leave hastily with Lisa, but Vera shoots him dead. At her trial Vera confesses to the murder but refuses to disclose her motive. As the lawyers are making their closing speeches, her newly discovered suitcase is brought as evidence before the presiding judge (Crisp). When he orders it opened to attempt to determine if it contains mitigating evidence, Vera abruptly decides to give a full statement to the court if the suitcase is not opened and the courtroom cleared of all witnesses and spectators.

Vera reveals that in 1912 she was a young diva in Warsaw appearing in an opera composed by Michialow, a womanizer who claimed to be madly in love with her.  She left the company to marry soldier Leonide Kirow (Hunter), and three years later was a mother with a husband at war. At her doctors advice, Vera attended a charity ball, where she was reunited with her old company, including Michailow. He lured her to a party at his apartment, where she became drunk and passed out. The next morning, while pondering how to tell her husband before gossip reached him, he returned from the front as an amputee, and out of a sense of guilt she remained silent.  Michailow bombarded her with letters begging to see her, which she hid from Leonide without answering them, until one day she went to Michailow to warn him to stop.  Leonide followed her, and thinking the worse, sued her for adultery.  Michialow fled to avoid testifying on her behalf, and she was found guilty, losing custody of her daughter.

For fifteen years, reduced to being a cheap singer, Vera searched Europe for Leonide (who had changed his name to Koslov and disappeared) and her child. When she at last located them (the day of the shooting), she learned that Leonid has been dead three years and that he had remarried. Her daughter, who is Lisa, has no idea that the second wife is not her real mother. Veras suitcase contains papers proving her statement, and she testified to prevent them from being read in open court, to save Lisas reputation and her relationship with the woman she believes is her mother. When open session resumes, all the parties avoid any mention of the details, and while Vera is found guilty, her sentence is mitigated and Lisa is left unaware of the truth.

==Notes==
 

==External links==
*  
*  

 
 
 
 
 
 