Jackpot (2009 film)
{{Infobox film
| name           = Jackpot
| image          = Jackpot (2009 movie poster).jpg
| alt            =  
| caption        = Poster of the film
| director       = Kaushik Ganguly
| producer       =Srikant Mohota,Mahendra Soni
| writer         = 
| screenplay     = 
| story          =  Rahul Banerjee Sohini Paul
| music          = Jeet Ganguly
| cinematography = 
| editing        = 
| studio         = Shree Venkatesh Films
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = India Bengali
| budget  = ₹ 8.5 million
| gross     = ₹ 4.7 million
}} Bengali film Rahul Banerjee, and Sohini Paul (the daughter of Tapas Paul).     It was Pauls second film after her debut in Bow Barracks Forever. 

==Plot== Rahul Banerjee) from Sundarbans Sajnekhali, are given two tasks to fulfill. Dodo is given the duty of a police constable at Bhawanipur police station while Arka is given the duty of a driver. They are given three days and two nights to battle out and the prize is fixed at Rs. 1 Crore. The one who makes fewer mistakes wins the jackpot.

Anchor Suman Dey becomes the host, a runaway from a mental hospital. Mayhem begins when Piu’s older sister and brother-in-law report at the Bhawanipur police station (where Dodo and Mithu are posted) and Arka comes to know about Piu’s problems. As Arka and Dodo are in disguise, they use fake names to cover up their identity. Arka becomes Jackie and Dodo becomes Potla. Thus while Arka flees to the hills with Piu to fulfill his job and win the money, Dodo with Goldar and Mithu start chasing them, not knowing that Piu is actually with Arka. After a lot of hue and fuss Arka manages to take Piu to her lost love (she was escaping only to find her lost love, Abhirup) and Dodo catches up with them.

Arka and Dodo are crowned champions. Arka donates the entire Rs. 50 lakh for Pius medical expenses. Dodo donates his share of Rs. 50 lakh for the same noble cause. It’s revealed that Dodo and Arka are best friends and Dodo had persuaded Arka to participate in the show. Mithu and Dodo become love birds, while Arka devotes his concentration to the orphaned, lonely and mentally stagnated Piu.

==Cast==
* Hiran Chatterjee as  Arka
* Koel Mallick as Piyo  Rahul Banerjee as Dodo
* Sohini Paul as Mithu
* Biswajit Chakraborty as police inspector (boro babu)
* Dev (actor)|Dev, in a special dance appearance for the remake of the song “Jibone Ki Pabona”

==See also==
* Waarish
* Ek Mutho Chabi

==References==
 

==External links==
*  
*  

 
 
 

 
 
 
 