Serenade (1940 film)
{{Infobox film
| name =  Serenade 
| image =
| image_size =
| caption = Jean Boyer
| producer = Robert Tarcali   
| writer =   Ernst Neubach   Pierre Wolff   Jacques Companéez   Max Maret
| narrator =
| starring = Lilian Harvey   Louis Jouvet   Bernard Lancret   Félix Oudart
| music = Paul Abraham    
| cinematography = Boris Kaufman   Maurice Pecqueux   Claude Renoir 
| editing =  Louisette Hautecoeur   Marc Sorkin    
| studio = Tarcali-Films 
| distributor = Astra Paris Films
| released = 28 February 1940
| runtime = 90 minutes
| country = France French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Jean Boyer and starring Lilian Harvey, Louis Jouvet and Bernard Lancret. It portrays a fictional romance between the Austrian composer Franz Schubert and an English dancer. The film was the first of two the Anglo-German actress Lillian Harvey made in France, after leaving Nazi Germany.  

== Cast ==
* Lilian Harvey as Margaret Brenton 
* Louis Jouvet as Le baron Hartmann 
* Bernard Lancret as Franz Schubert 
* Félix Oudart as Schwindt, le directeur 
* Roger Bourdin as Vogl 
* Marcel Lupovici as Lami de Schubert 
* Auguste Bovério as Beethoven 
* Marcel Vallée as Hostlinger, le editeur 
* Alexandre Rignault as Le gendarme 
* Pierre Magnier as Le Prince Metternich 
* René Stern as Un élégant 
* Madeleine Suffel as Anny 
* Claire Gérard as La propriétaire 
* Marthe Mellot as La vendeuse 
* Georges Bever as Le commis  Jacques Butin as Le secrétaire 
* Edmond Castel as Laubergiste 
* Philippe Richard as Le prieur 
* Robert Arnoux as Chavert  Henri Richard as Un diplomate

== References ==
 

== Bibliography ==
* Ascheid, Antje. Hitlers Heroines: Stardom & Womanhood In Nazi Cinema. Temple University Press, 2010.
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 