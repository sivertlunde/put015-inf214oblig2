Akkare Ninnoru Maran
{{Infobox film
| name = Akkare Ninnoru Maran
| image =
| caption =
| director = Girish
| producer = Sanal Kumar G Suresh Kumar Sreenivasan (dialogues) Sreenivasan
| Menaka Sukumari Innocent
| music = Kannur Rajan
| cinematography = S Kumar
| editing = N Gopalakrishnan
| studio = Sooryodaya Creations
| distributor = Sooryodaya Creations
| released =  
| country = India Malayalam
}} 
 1985 Cinema Indian Malayalam Malayalam film, Innocent in lead roles. The film had musical score by Kannur Rajan.    

==Plot summary==
Sree Thankappan Nair (Nedumudi Venu) is a rich man, who has cheated his sister out of her property. Her son Achuthan (Maniyanpilla Raju) wants to reclaim the land forcibly taken by his uncle,as well as marry his daughter, Nandini (Menaka) with whom he was in love. He and his friends hatch a plan to hoodwink Thankappan Nair, pretending to have earned a fortune working in Dubai.

==Cast==
*Nedumudi Venu as Sree Thankappan Nair (Ammavan)
*Maniyanpilla Raju as Achuthan
*Menaka as Nandini Mukesh as Pavithran
*Jagadeesh as Vishwan
*Sukumari as Savithri, Achuthans amma Innocent as Sankaran, Pavithrans acchan
*Sreenivasan as Ali Koya/ Arab Businessman / Menon from Kottayam
*Poojapura Ravi as Kannaran
* Lalithasree
*Bobby Kottarakkara from Kottarakkara as Musician
* Mala Aravindan as Raghavan
* Thodupuzha Vasanthi

==Soundtrack==
The music was composed by Kannur Rajan and lyrics was written by Priyadarshan.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kannanee Bhoomiyil   || KP Brahmanandan, Chorus, Satheesh Babu || Priyadarshan ||
|}

==Production==
Gireesh who was working as Associate director to Priyadarshan was contacted by Anand, who produced the movie "Parayanum Vayya Parayathirikkanum Vayya" and expressed his interest in producing a movie with Gireesh as director.Later Anand dropped the project.However Gireesh secured another producer with help from his few friends.Shooting was completed in 26 days in and around Palghat and Thiruvilwamala.

==Legacy==
*The scene in which Sreenivasan is dressed as Arabi muthalali and Mukesh translates Arabic to Nedumudi Venu still remains to be the one of the best comedy scenes in Malayalam cinema and still have a huge fan following in social medias and YouTube.

*Dialogues in the movie E.g.:Malas "Chirikkalle Chirikkalle", Innocents "Dayivayi Q paalikkuka" got a wide attention among people.

*It is one of the few Malayalam cinemas to have an 8 rating in imdb.

*Mala Aravindan was highly appreciated for his quick wits and dialogue delivery.

==References==
  

== External links ==
*  

 