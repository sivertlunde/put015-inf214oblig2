Needhi
{{Infobox film
| name           = Needhi
| image          = 
| image_size     =
| caption        = 
| director       = C. V. Rajendran
| producer       = K. Balaji
| writer         = A. L. Narayanan
| story          = Virendra Sinha
| narrator       = 
| starring       = Sivaji Ganesan Sowcar Janaki Jayalalitha
| music          =  
| cinematography = Masthan
| editing        = B. Kandaswamy
| studio         = Sujatha Cine Arts
| distributor    = Sujatha Cine Arts
| released       = 1972
| runtime        = 159 mins
| country        = India
| language       = Tamil
| budget         = 
}}
 Chandrababu are part of the cast. 

==Plot==
Sivaji Ganesan earns a living driving a truck. He is a rash driver and more than usually also driving while drunk. One night he stops at a prostitutes, spends the night with her, and gets up late the next morning. He rushes out and drives at breakneck speed in thick fog to make up for lost time while again drinking. He inevitably ends up running over and killing a farmer. But despite the opportunity to hit and run, he decides to stay and face the consequences. He is arrested by the police, charged, and brought before the courts.
 Ganesan attempts in vain to convince the Judge to change his ruling. He is transported to his new "prison" under police protection, where he is met by hostile villagers. The farmers family detest his presence, and call him enemy. Sivaji Ganesan attempts to escape on the first night, but is apprehended and brought back to serve his time.

He gradually comes to terms with the twist of fate that has forced him to become a subsistence farmer and live under the ever unforgiving eyes of the farmers family. Over time he starts sincerely working for the family and its interests. He meets Jayalalithaa, a happy-go-lucky girl who operates a small bi-scope machine to entertain the village kids. They take an instant liking to each other, which blossoms into love. He also finds friends amongst the previously hostile villagers. He works hard on the family plot while also protecting it from the clutches of a local landlord who has ill intentioned designs on the land and also on the farmers sister.

Surmounting many obstacles, Sivaji Ganesan is able to arrange the marriage of the sister with her childhood sweetheart. With the help of a benevolent police force and the Judge, he is also able to thwart the many attempts of the landlord to seize the familys land, and that of other villagers who have mortgaged their land with the same landlord. The farmers widow however (Sowcar Janaki), is unable to forgive Sivaji Ganesan for having killed her husband.

Things take a dramatic turn for the worse when Sivaji Ganesan is framed and arrested for the accidental death of Jayalalithaas drunk grandfather. At the same time, the landlord has the lush harvest produced by Sivaji Ganesan and other villagers covertly set ablaze, and has Jayalalithaa kidnapped, primarily to punish Sivaji Ganesan. Sowcar Janaki, who has been working in one of the landlords saw mills thinking he is an honourable man, witnesses his misdeeds, and finally realises her mistake. She is able to rescue Jayalalithaa but gets trapped by the landlord instead, who attempts to rape her. Meanwhile Sivaji Ganesan stages an escape from his holding cell and with the help of Jayalalithaa, is able to come to Sowcar Janakis rescue in the nick of time. He confronts the landlord and violently assaults him as payback. The police show up and arrest the landlord for his role in defrauding the villagers and destroying their harvest.

The deceased farmers family finally accept Sivaji Ganesan as one of their own, and arrange his marriage to Jayalalithaa. In a final twist though, his two years imprisonment is complete, and the Police arrive to escort him back to town. He pleads with the Judge to let him serve a life sentence, and the Judge smiles, vindicated that his experiment has been successful. 

==Cast==

* Sivaji Ganesan 
* Jayalalithaa 
* Sowcar Janaki 
* Chittor V. Nagaiah 
* Ganthimathi  Chandrababu

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Engaladhu Bhoomi" Chandrababu
|-
| 2
| "Naalai Mudhal Kudikka"
| T. M. Soundararajan
|-
| 3
| "Mappilaiya Paathukkadi Maina"
| T. M. Soundararajan
|-
| 4
| "Odudhu Paar"
| P. Susheela
|}

==References==
 

==External links==
*  

 

 
 
 