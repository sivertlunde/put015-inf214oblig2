Urban Feel
{{Infobox film
| name           = Urban Feel
| image          = 
| caption        = 
| director       = Jonathan Sagall
| producer       = 
| writer         = Jonathan Sagall
| starring       = Dafna Rechter
| music          = 
| cinematography = Dror Moreh
| editing        = 
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = Israel
| language       = Hebrew
| budget         = 
}}
 released in 1998. It tells the story of Eva and Robbie (played by Dafna Rechter and Sharon Alexander), a young Tel Aviv couple in a troubled marriage, which is rocked by the return of Emmanuel - played by Jonathan Sagall - Evas charming and mischievous ex-boyfriend. 

It won Best Feature Film at the 1998 Haifa International Film Festival, and was nominated for twelve Israeli Academy Awards, winning two. It was also entered into the 49th Berlin International Film Festival.   

==Cast==
* Dafna Rechter as Eva
* Jonathan Sagall as Emanuel
* Shimon Ben-Ari as Marco
* Tchia Danon as Gabriela
* Ziv Baruch as Jonah
* Zachi Dichner as Police officer

==References==
 

==External links==
* 

 
 
 
 
 
 