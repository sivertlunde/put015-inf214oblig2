Unacceptable Levels
 
{{Infobox film
| name           = Unacceptable Levels
| image          =
| caption        =
| director       = Edward Brown
| producer    =
| writer          = Edward Brown
| narrator      =
| starring      =
| music         =
| cinematography =
| editing       =
| studio        =
| distributor =
| released   =  
| runtime     =
| country      = United States
| language  = English
| budget       =
| gross          =
}}

Unacceptable Levels is a 2013 documentary film about the widespread use of artificial chemicals and their effects on the natural environment and on human health.

==Featured people==
The film features Ralph Nader, Devra Lee Davis, Stacy Malkan, Ken Cook, Christopher Gavigan, Alan Greene, John Warner, Andy Igrejas, Joan Blades, William Hirzy, Richard Clapp, Tyrone Hayes, Jeffrey Hollender, and Randy Hayes. 

==Screenings==
The film is touring the United States in the summer of 2013, with screenings scheduled for San Francisco on July 11, Chicago on July 24, and Austin, Texas, on August 24. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 

 