Whispers of Life
 
{{Infobox film
| name           = Whispers Of Life
| image          = Whispers of Life Poster.jpg
| caption        = Official movie poster
| director       = Florian Halbedl
| producer       = Joshua M. Ferguson
| writer         = Florian Halbedl
| cinematography = George Campbell
| editing        = Mike Banas
| studio         = Turbid Lake Pictures
| distributor    = McIntyre Media and Contemporary Arts Media 
| filmed in      = Vancouver, British Columbia, Canada
| released       =  
| runtime        = 11 minutes
| country        = United States
| language       = English
| Budget         = 10k
}} Patrick Gilmore, Veronica Jeske and Aaron Rota. Whispers Of Life was first shown in Palm Springs, California at the Growing up Gay Cinema Diverse (world premiere).   The film received international distribution in 2014 by Contemporary Arts Media. The short film has won four awards, including two audience awards, Jury Award, Festival Favorite, and was nominated for best short film in Canada.    

== Plot ==
After a gay teen is threatened by bullies, a stranger steps into his life and actively uses the power of imagination to challenges his thoughts of suicide. 

=== Reception ===
Whispers Of Life was positively received by critics. Van City Buzz said the film was short but very powerful.   Homorazzi said, "It’s a visually beautiful piece of work created by an amazing group of people".  

== References ==
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 