It Happens Every Thursday
{{Infobox film
| name = It Happens Every Thursday
| image =
| image_size =
| caption =
| director = Joseph Pevney
| producer = Leonard Goldstein   Anton Leader
 | writer = Jane S. McIlvaine  (novel)   Leonard Praskins   Barney Slater   Dane Lussier 
| narrator =
| starring =  Loretta Young   John Forsythe   Frank McHugh   Edgar Buchanan 
| music = Herman Stein 
| cinematography = Russell Metty  Frank Gross    
| studio =  Universal Pictures
| distributor = Universal Pictures
| released = April 22, 1953
| runtime = 80 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
It Happens Every Thursday is a 1953 American comedy film directed by Joseph Pevney and starring Loretta Young, John Forsythe and Frank McHugh. It was Loretta Youngs final film, as she switched to television work after this point. The film cost $617,085 to make, with Young receiving $75,000 for her appearance. 

==Synopsis==
A high-powered couple move from New York City to a small-town where they operate the local newspaper.

==Main cast==
* Loretta Young as Jane MacAvoy  
* John Forsythe as Bob MacAvoy  
* Frank McHugh as Fred Hawley  
* Edgar Buchanan as Jake  
* Gregg Palmer as Chet Dunne   Harvey Grant as Steve MacAvoy 
* Jimmy Conlin as Matthew  
* Jane Darwell as Mrs. Eva Spatch 
* Willard Waterman as Myron Trout 
* Gladys George as Mrs. Lucinda Holmes  
* Edith Evanson as Mrs. Peterson  Edward Clark as Homer  
* Kathryn Card as Mrs. Dow  
* Eddy Waller as James Bartlett 
* Regis Toomey as Mayor Hull

==References==
 

==Bibliography==
* Dick, Bernard F. Hollywood Madonna: Loretta Young. University Press of Mississippi, 2011. 

==External links==
* 

 

 
 
 
 
 
 
 
 

 