The Mysterious Pilot
{{Infobox film
| name           = The Mysterious Pilot
| image          = Mysterious Pilot.jpg
| image_size     =
| caption        = Chapter One poster
| director       = Spencer Gordon Bennet Louis Weiss
| writer         = George M. Merrick George Rosener (screenplay) Based in the novel The Silver Hawk, by William Byron Mowery
| narrator       = Frank Hawks
| music          = Abe Meyer (musical director/uncredited)
| cinematography = Edward Linden Herman Schopp B&W
| editing        = Earl Turner (film editor)
| distributor    = Columbia Pictures
| released       =  
| runtime        = 15 chapters (300 min)
| country        = United States
| language       = English
| budget         =
}} Columbia Serial movie serial Frank Hawks. This was the second serial produced by Columbia. 

==Plot==
Carter Snowden (Kenneth Harlan) about to marry Jean McNain (Dorothy Sebastian), is accused of murder. When his accuser is killed, Jean flees the train she is on, and heads into the Canadian woods. Snowden sends a bodyguard to find Jean, who appeals to Captain Jim Down (Frank Hawks) for help. With his friend "Kansas" (Rex Lease) and Indian Luke (Yakima Canutt), Jim hides Jean.

Snowden tracks down Jean and tries to lure her to his aircraft by telling her that Jim is injured and needs her. As soon as they realize what has happened, Jim and Kansas take to the air and force Snowdens aircraft down. Jean is unhurt but Snowden dies in the crash. Trying to get down to Jean, Jims parachute gets tangled in the trees and Jean ends up rescuing him.
==Chapter titles==
  
# The Howl of the Wolf
# The Web Tangles
# Enemies of the Air
# In the Hands of the Law
# The Crack-up
# The Dark Hour
# Wings of Destiny
# Battle in the Sky 
# The Great Flight
# Whirlpool of Death
# The Haunted Mill
# The Lost Trail
# The Net Tightens
# Vengeance Rides the Airways
# Retribution 
 Source:  
==Cast== Frank Hawks as Captain Jim Down 
* Dorothy Sebastian as Jean McNain
* Esther Ralston as Vivian McNain RCAF Sergeant "Kansas" Eby
* Guy Bates Post as "Papa" Bergelot
* Kenneth Harlan as Carter Snowden
* Yakima Canutt as Indian Luke
* George Rosener as Fritz
* Clara Kimball Young as Martha, Fritzs Wife 
* Frank Lackteen as Yoroslaff, a henchman Harry Harvey as "Soft Shoe" Cardigan, a henchman
* Tom London as Kilgour, a henchman Bob Walker as Boyer, a lumberjack henchman Ted Adams as Carlson, a henchman

==Production==
Mysterious Pilot was adapted from the novel "The Silver Hawk" by William Byron Mowery.  Cline 1984, p. 11.  Frank Hawks was billed in Mysterious Pilot as the "Fastest airman in the world." Harmon and Glut 1973, p. 109.  After each episode, Hawks appeared to deliver a "flying lesson". A Sikorsky S-39 amphibian was featured in the serial.  

==References==
===Notes===
 
===Bibliography===
 
* Cline, William C. "In Search of Ammunition." In the Nick of Time. New York: McFarland & Company, Inc., 1984. ISBN 0-7864-0471-X.
* Farmer, James H. Celluloid Wings: The Impact of Movies on Aviation. Blue Ridge Summit, Pennsylvania: Tab Books Inc., 1984. ISBN 978-0-83062-374-7.
* Harmon, Jim and Donald F. Glut. "Real Life Heroes: Just Strangle the Lion in Your Usual Way". The Great Movie Serials: Their Sound and Fury. New York: Routledge Publishing, 1973. ISBN 978-0-7130-0097-9. 
* Weiss, Ken and Ed Goodgold. To be Continued ...: A Complete Guide to Motion Picture Serials. New York: Bonanza Books, 1973. ISBN 0-517-166259.
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 