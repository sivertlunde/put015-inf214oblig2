Clams and Mussels
{{Infobox film |
  name           = Almejas y mejillones|
  image          = Almejasmejillones.jpg|
  caption        = Poster|
  director       = Marcos Carnevale|
  producer       = Albana Bolster |
  writer         = Marcos Carnevale|
  starring       = Leticia Brédice  Jorge Sanz|
  music          = Luis Elices  Francisco Musulén|
  cinematography = Alfredo F. Mayo|
  editing        = Jorge Valencia|
  distributor    = Buena Vista International Films Productions|
  released       = 2000 in film|2000|
  runtime        = |
  country        = Argentina|
  language       = Spanish |
  followed_by    = |
  budget         = $4,000,000|
  gross          = $18,052,128|
}}
 2000  Argentine comedy film directed and written by Marcos Carnevale based on a story by Lito Espinosa. The film starred Leticia Brédice, and Jorge Sanz

==Synopsis==
Paula has money and convinces Rolondo to accompany her to play poker. Over time Rolondo falls in love with her, but as it turns out, Paula is a lesbian. Rolondo lives in the carnivals of Tenerife in the Pub Morocco (a gay bar) where Fredy performs (intimate friend of Paula). The atmosphere of absolute freedom and sensuality that surrounds him little by little opens mind Rolondos mind.

==Main cast==
*Leticia Brédice ....  Paola
*Jorge Sanz ....  Rolondo/Diana
*Antonio Gasalla ....  Fredy
*Loles León ....  Socorro
*Silke ....  Inma
*Divina Gloria ....  Samantha
*Gerardo Baamonde ....  Camarero
*Ernesto Claudio ....  Quique

==Other==
*Daniel Álvarez Marino ....  Productor
*David Amador ....  Joven Yate
*Deftler Boog ....  Dr. Zupnik
*María Cárdenas ....  Consuelo
*Jaime Falero ....  Carlos
*Valentín Fariña ....  Pescadero
*Ulises Hernández ....  Jugador 2
*Didier Roussel ....  Jugador 1
*Cecilia Sarli ....  Chica Guardarropa

==Production Companies==
*Alma Ata International Pictures S.L.
*Antena 3 Televisión
*Buena Vista International Films Productions (in association with)
*Millecento Cinema
*Naya Films S.A. (in association with)
*Patagonik Film Group
*Vía Digital

==Distributors==
*Argentina Video Home (Argentina) (video)
*Buena Vista Home Entertainment (200?) (Brazil) (VHS)
*Buena Vista International Spain S.A. (Spain)
*Buena Vista International (Argentina)

==Overview==
 

==Release and acclaim==
The film premiered in 2000.

==External links==
*  

 
 
 
 
 

 
 