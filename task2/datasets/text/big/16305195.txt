Flight from Glory
{{Infobox film
| name           = Flight from Glory
| image          = Flight From Glory FilmPoster.jpeg
| caption        = Theatrical release poster
| director       = Lew Landers
| producer       = Robert Sisk Robert D. Andrews (as credited) (story) David Siverstein John Twist
| starring       = Chester Morris Whitney Bourne Onslow Stevens Van Heflin
| music          = Denzil A. Cutler
| cinematography = Nicholas Musuraca
| editing        = Harry Marker
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 67 minutes
| country        = United States English
| budget         =
| gross          =
}} golden age of aviation. Directed by capable veteran Lew Landers, and stars  Chester Morris, Whitney Bourne, Onslow Stevens and Van Heflin. Flight from Glory was considered one of the films that broke new ground in "pioneering airline sagas" comparing favorably to big-budget features such asThirteen Hours by Air (1936). 

==Plot==
Ellis (Onslow Stevens) runs Trans-Andean Air Service, a run-down company transporting supplies from Dalgado, a tiny, remote outpost, over the Andes Mountains to some mines. To save money, Ellis uses worn-out aircraft and "black sheep" pilots and crew no one else will employ. He hires George Wilson (Van Heflin), and is surprised when he brings his new wife, Lee (Whitney Bourne). Chief pilot Paul Smith (Chester Morris) tries to get her to leave, but the Wilsons have no money. As time goes on, George proves to be a drunk. Paul protects him as best he can, as he has fallen in love with Lee. She eventually confesses that she loves him.
 Richard Lane}, an experienced pilot dies in crash witnessed by George, he begins to crack up. When George is too drunk to fly, Garth Hilton (Douglas Walton) takes his place and is killed in yet another crash. Distraught and seeking revenge, George then forces Ellis at gunpoint into an aircraft and takes off. In the mountains, George jumps to his death, leaving Ellis to die like too many others he had hired. Smith is left to take over, but decides to join Lee and leave together. "Mousey" Mousialovitch (Solly Ward), the chief mechanic and former pilot, takes over the operation, with the mine owners promising new aircraft will be delivered.

==Cast==
As appearing in Flight from Glory, (main roles and screen credits identified):   Turner Classic Movies. Retrieved: October 22, 2012. 
* Chester Morris as Paul Smith
* Whitney Bourne as Lee Wilson
* Onslow Stevens as Ellis
* Van Heflin as George Wilson Richard Lane as Hanson Paul Guilfoyle as Jones
* Solly Ward as "Mousey" Mousialovitch
* Douglas Walton as Garth Hilton Walter Miller as "Old Timer"
* Rita La Roy as Molly, the cook
* Pasha Khan as Pepi

==Production== Boeing Model 100.]]

Flight from Glory was directed by B-movie specialist Lew Landers, who would eventually helm nine aviation films for RKO Radio Pictures. Flight from Glory was one of a series of aircraft-themed adventure films, such as Without Orders (1936), The Man Who Found Himself (1937), Sky Giant (1938),Air Hostess (1939) and Arctic Flight (1952).  Chester Morris also appeared in nine aviation-themed films. Santoir, Christian.   Aeromovies. Retrieved: November 8, 2014.  

Flight from Glory was primarily filmed from late-June to early-July 1937. Preston Foster was first slated for the role of "Ellis" while Chester Morris had to be obtained on loan to RKO. Flight from Glory was also one of the early films that featured Van Heflin, who was being groomed for stardom by appearing in low-budget B-features. After starring in Broadway, Heflin had made his first screen appearance opposite Katharine Hepburn in A Woman Rebels (1936). 
 Boeing Model de Havilland DH.4, Fairchild 24 and Stearman C3.   Turner Classic Movies. Retrieved: October 22, 2012. 

==Reception==
Later film reviewer Dennis Schwartz considered Flight from Glory as "a low-budget programmer fighting for elevation, thats not bad considering not much is expected."  Aviation film historian James Farmer described Flight from Glory, as "(a) pulpish, predictable yarn" and disparaged the use of "... Crashes, Crashes, Crashes!" Farmer 1984, p. 306. 

==References==

===Notes===
 

===Citations===
 

===Bibliography===
 
* Dwiggins, Don. Hollywood Pilot: The Biography of Paul Mantz. Garden City, New York: Doubleday & Company, Inc., 1967.
* Farmer, James H. Celluloid Wings: The Impact of Movies on Aviation. Blue Ridge Summit, Pennsylvania: Tab Books Inc., 1984. ISBN 978-0-83062-374-7.
* Hanson, Patrica King, ed. The American Film Institute Catalog of Motion Pictures Produced in the United States Feature Films, 1931–1940. Berkeley, California: University of California Press, 1993. ISBN 978-0-52007-908-3.
* Pendo, Stephen. Aviation in the Cinema. Lanham, Maryland: Scarecrow Press, 1985. ISBN 0-8-1081-746-2.
 

==External links==
* 
* 

 

 
 
 
 
 
 
 