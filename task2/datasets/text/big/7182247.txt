Swarabhishekam
{{Infobox film
| name           = Swarabhishekam
| image          = Swarabhishekam.JPG
| image_size     =
| caption        =
| director       = K. Viswanath
| producer       = H. Gopala Krishna Murthy
| writer         = K. Viswanath
| narrator       = Srikanth Sivaji Sivaji Laya Laya Urvashi Urvashi K. Viswanath Amuktamalyada Brahmanandam Tanikella Bharani Rajyalaxmi Vidyasagar
| cinematography = V. Durga Prasad
| editing        = G. Krishna Rao
| studio         =
| distributor    =
| released       = 2004
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}} musical drama Vidyasagar won the National Film Award for Best Music Direction. The film has garnered the National Film Award for Best Feature Film in Telugu for that year.   

==Plot==
Srirangam Brothers - Srinivasachari (K Viswanath) and Ranga (Srikanth) - are versatile Carnatic musicians. Srinivasachari is happily married but has no children. Ranga is a widower with two children. Srirangam Brothers are the biggest music directors in Telugu film industry (Tollywood) and compose music for only traditional and classical genres. Srirangam Brothers always try to help aspiring singers in their music compositions. Surekha (Laya),  a TV anchor, falls in love with Ranga and they get married.

After marriage, Surekha, who initially, respects her brother-in-law, starts to develop ill feelings towards him and becomes envious that even though her husband Ranga is the main factor for the music duos success and has equal contribution in Srirangam Brothers music scores, Srinivasachari is the one who gets all the credit.

When Srinivasachari is awarded the Padmasri by the Government of India, Surekha rejects her sisters gifted saree and shows no inclination to come along with her family to attend Srinivasacharis felicitation by the President. Also, Surekha starts to show her hostile and disrespectful behavior towards Srinivasachari.

This situation provokes Srinivasachari who starts behaving unpredictably and lashes out at a film producer about his brothers music compositions (during his music sitting) and also at the media. When Ranga in his office becomes aware of this situation through the film producer, he returns home only to get dispirited to see Rangas sister-in-law getting slapped by her husband, which leads to a heated argument between the two brothers and the family gets separated.

Surekha slowly understands Srinivasacharis and Rangas music calibre and realizes that the two brothers can only excel in their music when they stay united. This transformation in Surekha eventually leads to the unity of the family just like Srirangam Brothers different blends excel when they compose their music together.

==Soundtrack==
 Sirivennela and Shanmukha Sharma.

{| class="wikitable" width="50%"
|-
! Song title !! Singers || Lyricist  
|- Veturi
|-
| "Kasthuri Thilakam" || Shankar Mahadevan, Radhika || Veturi
|-
| "Okka Kshanam" || S P Balasubramanyam, S P Sailaja || Veturi
|-
| "Adhi Needhani Idhi Naadhani" || S P Balasubramanyam, K. S. Chithra || Veturi
|- Sirivennela
|- Sunitha || Traditional
|-
| "Ramaa Vinodhi Vallabha" || Madhu Balakrishnan, Sriram Parthasarathy, K. S. Chithra || Shanmukha Sharma
|-
| "Anujudai Lakshmanudu" || K. J. Yesudas, S P Balasubramanyam || Veturi
|-
| "Mangalam" || Various || Traditional
|-
|}

==Awards== National Film Awards
*National Film Award for Best Feature Film in Telugu  - K. Viswanath Vidyasagar
;Nandi Awards Vidyasagar

==References==
 

 

 
 
 
 
 
 