Ain't That Ducky
{{multiple issues|
 
 
}}

{{Infobox Hollywood cartoon|
| cartoon_name = Aint That Ducky
| series = Looney Tunes
| image =
| caption = The title card of Aint That Ducky.
| director = Friz Freleng
| story_artist = Michael Maltese
| animator = |
| voice_actor = Mel Blanc Victor Moore
| musician = Carl Stalling
| producer = |
| distributor = Warner Bros.
| release_date = May 19, 1945
| color_process = Technicolor
| runtime = 7 mins
| movie_language = English
}}

Aint That Ducky is a 1945 Warner Bros. Looney Tunes cartoon, directed by Friz Freleng, and starring Daffy Duck. Daffy has to outwit a hunter after him — and find out whats in the briefcase another duck is carrying around. While Mel Blanc did the voices for the ducks in this cartoon, the hunter was voiced by Victor Moore, a departure from either Blanc or Arthur Q. Bryan.

==Plot==
Daffy is taking a bubble-bath in a pond when he hears the sobbing of a small yellow duck carrying a briefcase. When Daffy tries to find out whats wrong with the duck, the little yellow duck stops sobbing long enough to emit a loud "AAH, SHUT UP!". When Daffy tries to find out whats in the briefcase thats causing the yellow duck so much grief, the duck tells Daffy, "AAH, KEEP YOUR HANDS OFF, MISTER ANTHONY!" (a reference to the host of the radio advice series The Goodwill Hour; see the ending of Baby Bottleneck). Even the presence of a hunter doesnt stop the yellow duck from stopping Daffy with a loud, "AAH, LAY OFF, YOU... DUCK!"
 tells the artist that theres supposed to be a barrel in the scene - "It says so in my script! Someones been laying down on the job. Jack Warner|J.L. will hear of this!" (a hand then draws a barrel for Daffy to hide in, but with the yellow duck inside).

About two-thirds of the way through, Daffy and the hunter team up to try to get the briefcase, but are stopped when they run down the road. The hunter then tries running after Daffy, but runs over a cardboard cutout of Daffy, thinking its the real thing. When the little yellow duck ruins Daffys fun at the expense of the hunter, Daffy decides enough is enough and tries grabbing the briefcase away — but is knocked down the side of the mountain, melting down the rocks. When the hunter is also knocked down, he reveals that he was able to swipe the briefcase. The two take a look inside, and soon are just as distressed as the yellow duck — the content is a piece of paper with the words "The End" on it, displayed as the cartoon ends.

==Production notes==
Daffy cries out the catchphrase "Sufferin succotash!"

==See also==
* List of cartoons featuring Daffy Duck

==References==
 

==External links==
*  

 
 
 
 
 
 
 