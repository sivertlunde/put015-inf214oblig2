Episode III: Enjoy Poverty
 
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Episode III: Enjoy Poverty
| image          = 
| caption        = 
| director       = Renzo Martens
| producer       = Inti Films Renzo Martens Menselijke Activiteiten
| writer         = 
| starring       = 
| distributor    = 
| released       = 2008
| runtime        = 90 minutes
| country        = Netherlands
| language       = 
| budget         = 
| gross          = 
| screenplay     = Renzo Martens
| cinematography = Renzo Martens
| editing        = Jan de Coster
| music          = 
}}
 Dutch artist Renzo Martens. 

==Background==
The film is a sequel to Martens Episode I – Enjoy Poverty (in which he filmed refugees from Chechnya).    Episode III – Enjoy Poverty premiered as the opening film of the International Documentary Film Festival Amsterdam (IDFA) in 2008. Subsequently, the film was screened at numerous international film festivals and broadcast on thirteen different television channels. Renzo Martens won the Flemish Culture Prize in 2010 and was nominated for the Artes Mundi Prize in 2014.

== Synopsis ==
 Congolese interior. In these regions, where poverty is the highest export product, Martens start an emancipation program in which he encourages local communities to monetize their poverty. The local photographers, who are showing proudly their pictures of marriages and births, are encouraged to face their lenses on the most cruel and shocking situations. Renzo Martens explains carefully how they should shoot the jutting ribs of a malnourished child to make the picture attractive to sell to Western newspapers. When hope is flickering in the eyes of the Congolese photographers, it all comes down when they notice they will never obtain a press card to enter closed war zones. In this respect, Martens can only acknowledge the hopeless situation and organizes a party in the jungle to indulge their poverty.

==Critical reflection==

The film is a study of the political claims of contemporary Western art, which are often praised but in many cases at the expense of another exploitation. Martens criticize this aspect of contemporary art by repeating it. Without taking a close defined position against this injustice and exploitation, the film unfolds itself as the point of critique as such. That is what makes the film autoreferential. 

Frieze (magazine)|Frieze magazine criticised the film for perpetuating the very thing it was protesting against - the pleasure of watching people in dire circumstances - and steroetyping the Congolese plantation workers. 

==Awards and nominations==

2013
* Cultural fund for documentary film, Stipend, Prins Bernhard Cultuurfonds, Netherlands

2010
* Price of the Dutch Film Fund, Netherlands

2009
* Doctape Award, RIDM, Montréal, Canada
* Flanders Cultural Price for Film, Belgium
* Hot Docs International Film Festival Canada, nominated Silverdocs Film Festival, Washington DC, US, nominated
* Hors Pistes, Centre Pompidou, France, nominated

==Screenings (selection)==

===Art venues===

2014
* Sydney Biennale, Sydney, Australia
* ZKM, Karlsruhe, Germany
* The BOX, Los Angeles, USA

2013
* Moscow Biennal, Moscow, Russia
* Walker Art Center, Minneapolis, USA
* Haus am Waldsee, Berlin, Germany
* Nikolaj Kunsthal, Copenhagen, Denmark

2012
* Kenderdine Art Gallery, Saskatoon, Canada
* Sligo, Ireland

2011
* Kunsthalle Goteborg, Goteborg, Sweden
* Kunsthalle Charlottenburg, Copenhagen, Denmark
* Foam, Amsterdam, Netherlands
* Le bal, Paris, France
* Hart House, Toronto, Canada
* Nomas Foundation, Rome, Italy

2010
* Stedelijk Museum, Amsterdam, Netherlands
* 6th Berlin Biennale for Contemporary Art, Berlin, Germany
* Witte de With, Rotterdam, Netherlands
* Museu Colecção Berardo, Lissabon, Portugal 
* Tate Modern, London, UK

2009
* Wilkinson Gallery, London, UK
* Van Abbe Museum, Eindhoven, Netherlands
* De Hallen, Haarlem, Netherlands
* Espace d Art Contemporain La Tollerie, Clermond Ferrand, FRance
* Abington Art Center, (curated by Sue Spaid), Philadelphia, USA
* Pavilion, Bucharest, Hongary

2008
*  Stedelijk Museum Bureau, Amsterdam, Netherlands
*  Manifesta 7, Rovereto, Italy
* Galeria Vermelho, São Paulo, Brasil
*  Wurttembergischer Kunstverein, Stuttgart, Germany
* De Appel, Amsterdam, Netherlands

===Festivals===

* CPH:Dox, Copenhagen
* Anasy Festival, Abu Dhabi
* Filmfestival, Guadalajara
* Leeds Filmfestival, Leeds
* Atlantic Mirror, Rio de Janeiro
* One World Filmfestival, Bukarest
* Prirzen Docufest, Kosovo
* Salem Filmfestival, Massachusettes, USA
* Tarifa African filmfestival
* Vermont Filmfestival
* Jersey Amnesty Filmfestival
* Doc Lounge Sweden Docpoint, Sweden
* Helsinki International Filmfestival, Finland
* Raccontare el Vero, Parma
* Aarhus Filmfestival
* African Filmfestival, Milan
* Bergen Filmfestival
* Nederlands Filmfestival, Utrecht
* Jerusalem Filmfestival, Jerusalem
* Talinn Black Nights Filmfestival, Estland
* New Zealand filmfestival, New Zealand
* Planet Doc review, Poland
* Docville festival, Belgium
* Alba International Filmfestival
* Dockenama, Maputo
* MiradasDoc, Tenerife
* Rencontres Internationales du Film Documentaire, Canada
* Planet Doc review, Warzawa
* DocHouse, London
* SilverDocs, Washington DC
* HotDocs, Toronto
* Thessaloniki Documentary Filmfestival, Thessaloniki
* IDFA, Amsterdam

===Television channels===

* VPRO, Netherlands
*  VRT, Belgium
* Al Jazeera, Qatar
* SVT, Sweden
*  YLE, Finland
* NRK TV, Norway
*  ORF, Germany
*  DBS Satellite Israel
* TV2, Africa

==References==

 



 
 
 
 
 