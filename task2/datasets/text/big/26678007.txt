Skeleton Crew (film)
{{Infobox film
| name           = Skeleton Crew
| image          = SkeletonCrewHorror.jpg
| alt            = 
| caption        = DVD released by Anchor Bay Entertainment
| director       = Tero Molin   Tommi Lepola
| producer       = Ilkka Niemi
| writer         = Tero Molin
| story          = Tero Molin   Teemu Molin
| starring       = Steve Porter   David Yoken   Anna Alkiomaa   Jonathan Rankle   Rita Suomalainen
| music          = Tuomas Kantelinen
| cinematography = Tero Molin   Tommi Lepola
| editing        = Petri Kyttälä   Juha Kuoppala
| studio         = Timeless Films   Northern Discipline Pictures
| distributor    = Anchor Bay Entertainment
| released       =  
| runtime        = 93 minutes
| country        = Finland
| language       = English
| budget         = 
| gross          = 
}}

Skeleton Crew is a 2009 horror film directed by Tero Molin and Tommi Lepola, and written by Tero Molin and Teemu Molin.

== Plot ==
 The Auteur") had filmed himself torturing patients to death. Most of the doctors films were confiscated, though the rooms in which they were developed and screened were never found.

Around thirty years later, the asylum is being used as the set of Silent Creek, a film based on the murders committed in it. While doing recordings, two soundmen hear disembodied voices, and find a hidden room. The chamber contains Anderssons undiscovered work, and while the bulk of the cast and crew of Silent Crew are disgusted by the snuff films, they decide not to call the police, since doing so would shut down production. Steven, the director of Silent Creek, becomes obsessed with Anderssons films (which he is drawn to one night by an apparition) and begins acting deranged, claiming that Silent Creek is "not real enough".

After finding Anderssons camera among the snuff films, Steve tricks Bruce (the actor playing Andersson) into killing an actress with a drill (which he said was just a prop) while he films it. The next day, the rest of the cast and crew find a note on Anderssons projector telling them to turn it on. The projector shows Steven (dressed like Andersson) disemboweling Mari while ranting about how he is "The Auteur". Steven then cuts a restrained Bruce in half with a chainsaw, which is shown through a television in the break room. Since the telephones and vehicles all fail to work, the remaining employees decide to make a run for it, after they look for the missing Mike. During the search, Klasu is taken after drinking drugged liquor.

In the previously sealed attic the rest of the group find a Moviola showing Skeleton Crew itself, everything up until that very moment. As everyone theorizes that it is like reality itself has become blurred and they are inside a horror film, the machinery shows Klasu in a pit with Mike. Steven throws two spiked clubs into the hole, and has Klasu and Mike fight for their freedom. The former wins, but Steven goes back on his word, and burns Klasu to death with spotlights. The next to die is Erno, who is locked in the attic and bombarded with sound, causing a fatal aneurysm.

While he, Lisa, and Anna are looking for another way out due to the main doors being sealed, Darius is captured, strung up, and impaled by a spear attached to a camera set on a Camera dolly|dolly. Steven then takes Anna, severs her right arm and left leg, and leaves her for Lisa to find. Anna begs for death, and Lisa comes close to mercy killing her, but upon realizing that this scenario is almost an exact recreation of the opening of Silent Creek, fakes passing out. When an angered Steven approaches her, Lisa shoots him several times with a dead crew members gun. Steven survives being shot, and upon realizing he cannot be killed due to the supernatural presence in the asylum, Lisa shoots herself in the mouth to spite Steve and ruin his film. However, Lisa survives the suicide attempt, and wakes up tied to chair, with Steven about to torture her with a blowtorch.

In a post-credits scene, Steven is shown watching Skeleton Crew in a theatre. A viewer yells out "Ah, mate, that really sucked. Werent even any fucking tits!"

== Cast ==

* Rita Suomalainen as Lisa
* Steve Porter as Steven S./Sanders
* Anna Alkiomaa as Anna
* Jonathan Rankle as Darius K.
* Jani Lahtinen as Klasu
* Ville Arasalo as Erno
* David Yoken as Bruce
* Riikka Niemi as Mari
* John J. Lenick as Mike
* Ramo Kalupala as Pete
* Eija Koskimaa as Nurse 1
* Jukka Toivonen as Nurse 2
* Wiley Cousins as Jerry
* Markku Peltola as Doctor Andersson
* Karoliina Blackburn as Nurse in Snuff Films

== Release ==

Lightning Media released Skeleton Crew on DVD 21 July 2009. 

== Reception ==

David Johnon of DVD Verdict wrote that it was well put together and "different and cool".   Bill Gibron of DVD Talk rated the film 1.5/5 stars and called it "a crime, the kind of waste of effort affront that makes you rethink your love of the genre in the first place." 

== References ==

 

== External links ==

*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 