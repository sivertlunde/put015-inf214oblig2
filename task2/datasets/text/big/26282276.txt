Prem Nazirine Kanmanilla
{{Infobox film
| name           = Prem Nazirine Kanmanilla
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Lenin Rajendran
| producer       = 
| writer         = Lenin Rajendran   Vaikom Chandrashekharan Nair
| narrator       =  Sreenivasan
| music          = M. B. Srinivasan
| cinematography = Venu
| editing        = G. Murali  
| studio         = Susmitha Creations
| distributor         = Susmitha Creations
| released       =  
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Sreenivasan who Shankar and Nedumudi Venu.   The movie also shows the Kerala society and its political leadership and how various people behave during such a tragic event.

==Plot==
A group of frusturated youngsters decides to kidnap the famous Malayalam actor Prem Nazir from one of the film shooting sets. The captured Prem Nazir is then taken to a dilapidated home in a dense forest. The kidnappers treat Prem Nazir with due respect and treats him in the best possible manner, given the place where they are put up. One kidnapper (Sreenivasan) comes from an upper-caste Hindu family, and his father worked as the local temple oracle. Though highly educated he does not find a good job. Another gang member (Vijay Menon) has a very troubled childhood during which his parents were murdered and he had to live as a domestic helper of the murderer. The kidnap of Prem Nazir is to express their frusturation against the cruel society.

Kerala is now totally shocked to hear about this news. Old actresses Ragini and Kumari are seen watching old movies in which Prem Nazir acted with them. New movie releases (including Richard Attenboroughs Gandhi (film)|Gandhi) has been suspended and old Prem Nazir movies are now played in the theatres. For the movie distributors this is a quick way to make some extra revenue. The producer of the latest movie in which Prem Nazir was acting, is in a very bad state. Only two more scenes in his movie were yet to be shot. In desperation he requests Prem Nazirs brother Prem Nawaz to act as a dummy, so that he can somehow complete the movie. The producer, named Innocent (actor)|Innocent, pleads with Prem Nawaz that the movie would get completed, and he can release the movie and generate some quick money. His idea is to release the movie before Prem Nazir is rescued.

The political scene in the state is in a worse condition. The ruling government of the day is accused of being lax on investigations. The party in the opposition feels that government did not provide adequate protection to Prem Nazir because, Nazir sympathised with the party in opposition. Demands are made asking the Home Minister (Thilakan) to resign. The Home Minister flatly refuses and tries to retain his position. He feels there are international agencies out there involved in the kidnapping and so assistance from the central government is required.

Meanwhile the state police also mobilizes large numbers of police officers from the armed reserve and armed police battalions for a combing operation. At the same time, Prem Nazir, who by then had become quite friendly with the kidnappers, manage to convince them about the stupidity of their actions. He advices them to surrender. They soon plan to escape of the forest with a plan to surrender when they are ambushed by a police party. The police whose only aim was to rescue Prem Nazir uses excessive force to capture the kidnappers. They are beaten all the way to the police vehicles and one of them is shot. A dejected Prem Nazir looks on helplessly as he knows that the kidnappers are not hardcore criminals, but he cannot convince the police and also the government leadership and the people.

==Cast==
*Prem Nazir – himself
* Shankar Pannikar - himself Sreenivasan –  kidnapper (I)
*Maniyanpilla Raju –  kidnapper (II)
*Vijay Menon –  kidnapper (III)
*Mammootty –  himself
*Achankunju – Maran
*Thilakan – Home Minister
*Santhakumari – Bhargavi Innocent – Innocent, the film producer

==Trivia==
 
* The name may have been based on an older movie Post Man-e kaananilla ("Post man goes missing") in which Prem Nazir played the lead role.
* Innocent, one of the popular comedians in the Malayalam movie, today  acts as Innocent a film producer. In real life, in 1980s Innocent was a film producer.
* Director P. G. Vishwambharan, actors Mammootty, Nedumudi Venu and Prem Nawaz act as themselves in the movie.
* John Samuel, who was quite popular anchor for the programme Prathikaranam in Doordarshan Malayalam in early 1990s acted as the Deputy Superindent of Police investigating this case.

==References==
 

==External links==
*  

 
 
 
 