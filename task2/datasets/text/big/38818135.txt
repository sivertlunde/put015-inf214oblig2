Very Ordinary Couple
{{Infobox film name           = Very Ordinary Couple  image          = File:Very_Ordinary_Couple_poster.jpg film name      = {{Film name hangul         = 연애의 온도  hanja          =  의   rr             = Yeon-ae-eui Ondo  mr             = Yŏn-ae-ŭi Ondo}} director       = Roh Deok producer       = Kim Seon-yong   Han Jae-rim writer         = Roh Deok starring       = Kim Min-hee   Lee Min-ki  music          = Kim Jun-seong cinematography = Park Jong-cheol editing        = Jeong Jin-hee distributor  Lotte Entertainment released       =   runtime        = 112 minutes country        = South Korea language       = Korean budget         =  gross          = 
}}
Very Ordinary Couple ( ; lit. "Temperature of Love" or "Degree of Love") is a 2013 South Korean romantic comedy film written and directed by Roh Deok (alternatively spelled Noh Deok), starring Kim Min-hee and Lee Min-ki as a recently separated couple who, being employees at the same bank, must deal with the prospect of continually seeing each other on a daily basis. This inevitably leads to tension and flareups, but over time their feelings towards each other begin to change.   

This is Roh Deoks feature directorial debut. She based the screenplay on her own love life and those of her circle of friends. 

== Plot == Standard Chartered, and their seemingly nasty split turns scandalous in the office. After going through a period of irrational behavior, including physical fights and Young’s impulsive one-night stand with a mutual colleague at work, the two decide to give their relationship another try. Once again, they are madly in love.

The real story of the film begins as the two begin to date again, in spite of their shared doubts and fears. As the second honeymoon phase ends, the same problems ― those that had led to their initial break-up ― surface: the boredom, the trust issues, and the mutual dishonesty about what they really want. The hot-tempered Dong-hee seems like he needs an anger management class, while Young tries to avoid conflicts by telling small lies ― unaware of how lies, regardless of the motivations, can destroy trust.

Above all, the couples shared experience of the break-up gradually makes them insecure about their relationship. This eventually leads to a suffocating disaster: Dong-hee tries very hard to pretend that he is interested in the things Young suggests doing, only to please her and save their relationship ― although hed really rather sleep in than go to an amusement park on his day off. Young, on the other hand, gets extremely self-conscious of everything she tells Dong-hee, as she is worried that she will upset him off by "saying something wrong" ― just like how she did before the first break-up.

There is a sadness that fills the screen as the two desperately try to save their relationship, pretending nothing is wrong and trying to believe that things will work out in the end. "I will be good to her," Dong-hee repeatedly says in the film. "I’ll try harder to make this work."

But watching them forcing on a smile while obviously annoyed by each other’s company, the audience can instinctively tell that the two will soon part ways ― in spite of their genuine efforts and affection for each other. The movie reaches its climax as the two flawed characters realize that theyve messed it up again, that they can no longer be in denial, and there are some things in life that cannot be controlled no matter how hard they try.  

== Cast ==
* Kim Min-hee as Jang Young   
* Lee Min-ki as Lee Dong-hee   
* Ha Yeon-soo as Hyo-seon
* Ra Mi-ran as Ms. Son, deputy department head in bank
* Choi Moo-sung as Mr. Kim, section chief in bank
* Kim Kang-hyeon as Mr. Park, senior clerk in bank
* Park Byeong-eun as Mr. Min, deputy department head in bank
* Lee Moon-jeong as Ms. Choi, clerk in bank
* Yoon Kyeong-hee as intern
* Choi Gwi-hwa as husband of Ms. Son
* Moon Chang-gil as bank branch manager

==Reception==
Billed as a romantic comedy, but which might also be described simply as a relationship drama, Very Ordinary Couple made an impressive start at the box office with 1.8 million admissions in four weeks. Viewers praised the strong acting in the film as well as its comparatively realistic, non-romantic view of contemporary relationships. Debut director Roh Deok had previously worked as a scripter on Jang Joon-hwans cult classic Save the Green Planet! (2003) and also received praise for her short film The Secret within Her Mask (2005).  

==Awards and nominations== 2013 Baeksang Arts Awards
*Best Film Actress - Kim Min-hee   

2013 Shanghai International Film Festival
*Asian New Talent Award for Best Feature   

2013 Mnet 20s Choice Awards
*Nomination - 20s Movie Star, Female - Kim Min-hee

2013 Buil Film Awards
*Nomination - Best Actress - Kim Min-hee
*Nomination - Best New Director - Roh Deok
*Nomination - Best New Actress - Ha Yeon-soo
 2013 Blue Dragon Film Awards
*Nomination - Best Actress - Kim Min-hee
*Nomination - Best New Director - Roh Deok

== References ==
 

== External links ==
*    
*  
*  

 
 
 
 
 
 