Shiva Mecchida Kannappa
 
{{Infobox film name           = Shiva Mecchida Kannappa image          =  director       = Vijay producer       = S A Govindaraj   Raghavendra Rajkumar writer         =    starring  Rajkumar  Geetha   Sarala Devi  music          = T. G. Lingappa cinematography = R Chitti Babu   editing        = P Bhakthavathsalam distributor    =  released       = March 24, 1988  runtime        = 149 min. country        = India  language       = Kannada 
}} Kannada biographical film directed by Vijay. The film is a remake of 1959 film Bedara Kannappa, the debut film of actor Rajkumar (actor)|Rajkumar. Produced by Raghavendra Rajkumar, the film is about the life of Kannappa Nayanar, a hunter who becomes Lord Shiva devotee played by Puneet Rajkumar in younger days and Shivarajkumar in adult life.  The rest of the cast includes Geetha (actress)|Geetha, Sarala Devi, C. R. Simha and Rajkumar himself playing a brief cameo role. The film had musical score by T. G. Lingappa and the dialogues and lyrics written by Chi. Udaya Shankar.

The film opened on 24 March 1988 and was declared a musical blockbuster.

==Cast==
*Rajkumar (actor)|Rajkumar...guest appearance 
*Shivarajkumar as Kannappa (adult)
*Puneeth Rajkumar as Kannappa (child) Geetha
*Saraladevi 
*C. R. Simha
*Balaraj
*Shankar Patil
*Doddanna
*Sathyajith
*Bhatti Mahadevappa
*Srishailan
*Honnavalli Krishna
*Seetharam
*Padma Kumata
*Sangeetha
*Priya
*Kamalashree
*Shyamala

==Soundtrack==
The musical score by T. G. Lingappa received unanimous response and was well appreciated. 

{|class="wikitable"
! No. !! Song !! Singers !! Lyrics 
|-
| 1 || "Kanninda Nee Baana" || S. P. Balasubrahmanyam, B. R. Chaya || Chi. Udaya Shankar
|-
| 2 || "Mella Mellane Bandane" || Vani Jairam || Chi. Udaya Shankar
|-
| 3 || "Kannilla Kiviyilla" || S. P. Balasubrahmanyam || Chi. Udaya Shankar
|- Rajkumar ||Chi. Udaya Shankar
|-
| 5 || "Deva Mahadeva" || S. P. Balasubrahmanyam || Chi. Udaya Shankar
|- Rajkumar || Kanaka Dasa
|-
|}

==References==
 

==External source==
* 
*  

 
 
 
 
 
 


 