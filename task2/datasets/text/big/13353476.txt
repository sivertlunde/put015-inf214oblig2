What Time Is It? (film)
 

{{Infobox film
| name           = What Time Is It?
| image          = Che ora e.jpg
| image_size     = 
| caption        = Italian film poster
| director       = Ettore Scola
| producer       = Mario Cecchi Gori Vittorio Cecchi Gori
| writer         = Beatrice Ravaglioli Ettore Scola Silvia Scola
| narrator       = 
| starring       = Marcello Mastroianni Massimo Troisi Anne Parillaud Renato Moretti Lou Castel
| music          = Armando Trovajoli
| cinematography = Luciano Tovoli
| editing        = Raimondo Crociani
| distributor    = 
| released       = 1989
| runtime        = 97 Min 
| country        = Italy Italian
| budget         = 
}}
 Italian drama film directed by  Ettore Scola.    It was co-produced with France. 

==Cast==
* Marcello Mastroianni - Marcello, The Father
* Massimo Troisi - Michele, the son
* Anne Parillaud - Loredana
* Renato Moretti - Sor Pietro
* Lou Castel - Fisherman

==Awards==
*4 Venice Film Festival awards: Including Best Actor to both Marcello Mastroianni and Massimo Troisi.

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 

 
 