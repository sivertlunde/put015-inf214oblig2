X-Men (film)
 
 
{{Infobox film
| name           = X-Men
| image          = XMen1poster.jpg
| alt            = Poster shows a big X with a city skyline in the background. In the foreground are the films characters. The films name is at the bottom.
| caption        = Theatrical release poster
| director       = Bryan Singer
| producer       = {{Plain list|
* Lauren Shuler Donner Ralph Winter
}}
| screenplay     = David Hayter
| story          = {{Plain list|
* Tom DeSanto
* Bryan Singer
}}
| based on       =  
| starring       = {{Plain list| 
* Patrick Stewart
* Hugh Jackman
* Ian McKellen
* Halle Berry
* Famke Janssen
* James Marsden
* Bruce Davison Rebecca Romijn-Stamos
* Ray Park
* Anna Paquin                                                                                                       }}
| studio         = {{Plain list|
* 20th Century Fox Marvel Entertainment Group
* Bad Hat Harry Productions
* The Donners Company
}}
| distributor    = 20th Century Fox
| released       =  
| runtime        = 104 minutes
| country        = United States 
| language       = English
| music          = Michael Kamen
| cinematography = Newton Thomas Sigel
| editing        = {{Plainlist|
* Steven Rosenblum
* Kevin Sitt John Wright}}
| gross          = $296.3 million      
| budget         = $75 million
}}
X-Men is a 2000 American  aviers X-Men, and the Brotherhood of Mutants, led by Magneto (comics)|Magneto.
 John Logan, Beast and Nightcrawler were deleted over budget concerns from Fox. X-Men marks the Hollywood debut of actor Hugh Jackman, who was a last second choice for Wolverine, cast three weeks into filming. Filming took place from September 22, 1999 to March 3, 2000, primarily in Toronto. X-Men was released to positive reviews and was a financial success, starting the X-Men film franchise and spawning a reemergence of superhero films.

==Plot== Erik Lehnsherr Robert Kelly Mutant Registration Congress which Professor Charles Xavier (Stewart) who privately discuss their differing viewpoints on the relationship between humans and mutants.
 Marie accidentally Cyclops and Storm arrive and save Wolverine and Rogue, and bring them to the X-Mansion in Westchester County, New York. They are introduced to Xavier,  who leads a group of mutants called the X-Men, who are trying to seek peace with the human race, educate young mutants on their powers, and stop Magneto from escalating the war with humanity.
 Toad and shapeshifter Mystique Mystique and Bobby Drake) that Xavier is angry with her and that she should leave the school. Xavier uses his mutant-locating machine Cerebro to find Rogue at a train station. Mystique infiltrates Cerebro and sabotages the machine.

At the train station, Wolverine convinces Rogue to stay with Xavier, but a fight ensues when Magneto, Toad and Sabretooth arrive and kidnap Rogue. Kelly arrives at Xaviers school, but dies shortly after due to the instability of his artificial mutation causing his cells to break down into a puddle of water. The X-Men learn that Magneto was severely weakened while testing the machine on Kelly, and realize that he intends to use Rogues power-transferring ability so that she can power the machine in his place, which will kill her. Xavier attempts to use Cerebro to locate Rogue, but Mystiques sabotage causes him to fall into a coma. Fellow telepath Jean Grey fixes and uses Cerebro, learning that Magneto plans to place his mutation-inducing machine on Liberty Island and use it to mutate the world leaders meeting for a summit on nearby Ellis Island.

The X-Men scale the Statue of Liberty. Storm electrocutes Toad, and Wolverine stabs Mystique. Magneto transfers his powers to Rogue, forcing her to use them to start the machine. Wolverine escapes and kills Sabretooth with the help of Scott and Jean. Storm uses her weather-controlling powers and Jean uses her telekinesis to lift Wolverine to the top of Magnetos machine. Wolverine saves Rogue when Cyclops knocks out Magneto, and destroys the machine. Wolverine touches the dying Rogues face, and his regenerative abilities are transferred to her, causing her to recover. Professor Xavier recovers from his coma. The group learns that Mystique is still alive, and impersonating Senator Kelly. Xavier tells Wolverine that near where he was found in Canada is an abandoned military base that might contain information about his past. Xavier visits Magneto in a prison cell constructed entirely of plastic, and the two play chess. Magneto warns that he will continue his fight, to which Xavier promises that he and the X-Men will always be there to stop him.

==Cast== Charles Xavier / Professor X Xavier School for Gifted Youngsters, Xavier hopes for peaceful coexistence between mutantkind and mankind and is regarded as an authority on genetic mutation. Although he is restricted to a wheelchair, he is a powerful mutant with vast telepathic abilities. Along with Magneto (comics)|Magneto, he is the inventor of the Cerebro supercomputer, which further amplifies his abilities.
 Logan / Wolverine
:A mutant Canadian loner who makes a living in cage fights and has lived for fifteen years without memory of who he is, apart from his dog tags marked "Wolverine" and an adamantium-encased skeleton (as well as adamantium claws). His mutant powers include enhanced, animal-like senses (enabling him to sense other people) and the ability to heal rapidly from numerous injuries, including the surgery that bonded the metal to his skeleton, which makes his age impossible to determine.
 Erik Lehnsherr / Magneto Holocaust survivor who was once friends with Xavier (who he helped build Cerebro), until his belief that humans and mutants could never co-exist led to their separation. His mutant powers include powerful magnetic fields, metal manipulation, and a sophisticated knowledge in matters of genetic manipulation, which he uses to plan a mutation of the world leaders to allow mutant prosperity.
::* Brett Morris as young Erik Lehnsherr 
 Ororo Munroe / Storm Senator Kelly says that she sometimes hates humans, but mostly because she is afraid of them. Her mutant powers include weather manipulation. 

* Famke Janssen as Jean Grey|Dr. Jean Grey: telekinesis and telepathy that are similar to but much less advanced than that of Xaviers, as displayed when she is stunned by the usage of Cerebro.
 Scott Summers / Cyclops
:Xaviers second-in-command and the X-Mens field leader, as well as an instructor at the Institute. He is in love with Jean Grey and has a relationship with her. His mutant powers include a strong red beam of force shooting from his eyes, which is only held in check by sunglasses or a specialized ruby-quartz visor, which also enables him to control the strength of the beam to fire when in combat.
 Senator Robert Kelly
:An anti-mutant politician who wishes to ban mutant children from schools using a Mutant Registration Act. He is kidnapped by Magneto in a test of his mutation machine, which causes his body to turn into a liquid-like substance.
 Rebecca Romijn-Stamos Raven Darkholme / Mystique
:Magnetos loyal second-in-command, who seems completely facile with respect to modern technology. Her mutant powers include altering her shape and mimicking any human being, which is almost secondary to her role as "the perfect soldier". 
 Mortimer Toynbee / Toad
:A very agile mutant British punk and henchman of Magneto. His mutant powers include a menacing streak, a long, prehensile tongue, a slimy substance that he spits onto others, and quick, toad-like abilities to move.
 Victor Creed / Sabretooth
:A brutal and sadistic mutant Canadian mercenary and henchman of Magneto. His mutant powers include a ferocious, feline-like nature, enhanced animal-like senses and healing abilities similar to Wolverines, and claws extending past each finger. 
 Marie DAncanto / Rogue
:A seventeen-year-old girl forced to leave her home in Mississippi when she puts her boyfriend into a coma by kissing him. Her mutant powers include absorbing anyones memories, life force, and, in the case of mutants, powers through physical touch.
 Bobby Drake / Iceman
:A student at Xaviers School for Gifted Youngsters who takes a liking to Rogue. His mutant powers include ice manipulation and changing temperatures to subzero degrees.

David Hayter, Stan Lee, and Tom DeSanto make cameo appearances.
 , appeared as the truck driver who drops Rogue off at the bar at which Wolverine fights. {{cite book
| last = Hughes | first = David
| title = Comic Book Movies | publisher = Virgin Books | year = 2003 | pages = 177–188 Jubilee and Colossus sketching Gambit was comic book and superhero film genre. 

==Production==

===Development===
 . ]] animated X-Men TV series for Fox Kids. 20th Century Fox was impressed by the success of the TV show, and producer Lauren Shuler Donner purchased the film rights for them in 1994,     bringing Andrew Kevin Walker to write the script. 
 Wolverine into Juggernaut and John Logan, James Schamus,  and Joss Whedon were brought on for subsequent rewrites. One of these scripts kept the idea of Magneto turning Manhattan into a "mutant homeland", while another hinged on a romance between Wolverine and Storm (Marvel Comics)|Storm.  Whedons draft featured the Danger Room, and concluded with Jean Grey dressed as the Phoenix Force (comics)|Phoenix.    According to Entertainment Weekly, this screenplay was rejected because of its "quick-witted pop culture-referencing tone", {{cite news
| author = Craig Seymour | title = X-Man Out
| work = Entertainment Weekly | date = May 10, 2000 Jubilee and included Professor X, Cyclops, Jean Grey, Nightcrawler (comics)|Nightcrawler, Beast, Iceman, and Storm. Under Chabons plan, the villains would not have been introduced until the second film. 
 Apt Pupil. Fox then announced a Christmas 1998 release date. {{cite news | author = Michael Fleming Variety | date = April 14, 1997 | accessdate = March 25, 2008}}  {{cite news
| author = Anita M. Busch
| url = http://www.variety.com/vstory/VR1117466461.html
| title = Singer set to direct Foxs Men Variety | Rogue an Senator Kellys claim that he has a list of mutants living in the United States recalls Joseph McCarthys similar claim regarding communists. 

Fox, who had projected the budget at $75 million, rejected the treatment which they estimated would have cost $5 million more. Beast, Nightcrawler, Pyro (comics)|Pyro, and the Danger Room had to be deleted before the studio greenlighted X-Men.  {{cite news
| author = Dan Cox
| url = http://www.variety.com/article/VR1117478916.html Variety
| date = July 29, 1998 | accessdate = March 25, 2008}}  Fox head Thomas Rothman argued that this would enhance the story,  and Singer concurred that removing the Danger Room allowed him to focus on other scenes he preferred. Elements of Beast, particularly his medical expertise, were transferred to Jean Grey.  Singer and DeSanto brought Christopher McQuarrie from The Usual Suspects, and together did another rewrite. {{cite news
| author = Ed Solomon, Chris McQuarrie, Tom DeSanto, and Bryan Singer
| title = February 1999 X-Men script | publisher = Sci-Fi Scripts| date = February 24, 1999
| url = http://www.scifiscripts.com/scripts/xmenscript.txt
| accessdate=July 1, 2007}}  {{cite news
| author = Chris Petrikin
| url = http://www.variety.com/article/VR1117490409.html Variety | date = January 20, 1999 | accessdate = March 25, 2008}}  David Hayter simultaneously rewrote the screenplay, receiving solo screenplay credit from the Writers Guild of America, while Singer and DeSanto were given story credit.  The WGA offered McQuarrie a credit, but he voluntarily took his name off when the final version was more in line with Hayters script than his. {{cite news
| author = Borys Kit
| url = http://www.hollywoodreporter.com/hr/content_display/film/news/e3i367bfce562b7ee624637405023e9228f
| title = McQuarrie to pen Wolverine sequel
| work = The Hollywood Reporter | date = August 13, 2009
| accessdate = August 13, 2009
| archiveurl= http://web.archive.org/web/20100110110714/http://www.hollywoodreporter.com/hr/content_display/film/news/e3i367bfce562b7ee624637405023e9228f
| archivedate = January 10, 2010
}} 

===Casting===
  in early October 1999.    Jackman was then cast three weeks into filming, based on a successful audition. 
 Conspiracy Theory, Boy Scout. Apt Pupil. McKellen responded to the gay allegory of the film, "the allegory of the mutants as outsiders, disenfranchised and alone and coming to all of that at puberty when their difference manifests," Singer explained. "Ian is activist and he really responded to the potential of that allegory." 

===Filming=== Minority Report for release in June 2000, but he had chosen to film A.I. Artificial Intelligence, and Fox needed a film to fill the void.  This meant that Singer had to finish X-Men six months ahead of schedule, although filming had been pushed back.  The release date was then moved to July 14. 
 Toronto Union Station and Hamilton GO Centre were set. Spencer Smith Park (in Burlington, Ontario) doubled for Liberty Island. A scale model was used for the Statue of Liberty. 

===Design and effects===
The filmmakers decided not to replicate the X-Men costumes as seen in the comic book. Stan Lee and Chris Claremont supported this decision. Claremont joked, "you can do that on a drawing, but when you put it on people its disturbing!"  Producer/co-writer Tom DeSanto had been supportive of using the blue and yellow color scheme of the comics,  but came to conclude that they would not work onscreen.  To acknowledge the fan complaints, Singer added Cyclops (comics)|Cyclops line "What would you prefer, yellow spandex?" – when Wolverine complains about wearing their uniforms – during filming. Singer noted that durable black leather made more sense for the X-Men to wear as protective clothing,  and Shuler Donner added that the costumes helped them "blend into the night". 

Wolverines claws required a full silicone cast of Hugh Jackmans arm, and over 700 pairs for Jackman and his stunt doubles.  It took nine hours to apply Rebecca Romijns prosthetic makeup.  She could not drink wine, use skin creams, or fly the day before filming, because it could have caused her body chemistry to change slightly, causing the 110&nbsp;prosthetics applied to her skin to fall off.  Between takes, the makeup department kept Romijn isolated in a windowless room to ensure secrecy. Romijn reflected, "I had almost no contact with the rest of the cast; it was like I was making a different movie from everyone else. It was hell." 
 Academy Award. FX2 Visual Effects, 2003, 20th Century Fox 
 compositor Claas morphed Bruce Davison into a liquid figure for Kellys mutation scene. Cunningham said, "There were many digital layers: water without refraction, water with murkiness, skin with and without highlights, skin with goo in it. When rendered together, it took 39 hours per frame." They considered showing Kellys internal organs during the transformation, "but that seemed too gruesome", according to Cunningham.   

==Music==
Singer approached John Williams to compose the film score, but Williams turned down the offer because of scheduling conflicts.  John Ottman was originally set as composer,  but Michael Kamen was eventually hired.

All music composed by Michael Kamen.

===Track listing===

* 1. Death Camp - 3:05 - Contains an Unused Beginning.
* 2. Ambush - 3:26 - Slight Alternate with unused pieces.
* 3. Mutant School - 3:48 - From 1:25 to the end is present an Alternate take or unused music.
* 4. Magnetos Lair - 5:01 - From 00:00 to 1:41ish an alternate mix of Senators New Power from the Complete Score Bootleg is present.
* 5. Cerebro - 2:13 - Album Mix of the cue heard in the movie.
* 6. Train - 2:35 - Complete Mix of the cues heard in the movie.
* 7. Magneto Stand-Off - 3:01 - The first 30 seconds are unused in the movie.
* 8. The X-Jet - 3:47 - From 2:08 an alternate to the same or another piece is heard.
* 9. Museum Fight - 2:21 - Edited and/or different version of the piece heard in the movie.
* 10. The Statue of Liberty - 2:38 - An alternate to the cue Fight on the Head from the Complete Score Bootleg.
* 11. Final Showdown - 2:31 - An unused beginning is present on this track, while the last 20 seconds are a different mix than what is heard in the movie. Moreover on this cue, the synth is very difficult to hear, whilst in the movie it is very clear.
* 12. Logan and Rogue - 5:57 - Album Mix of Logan Holds Rogue with an edited end and Mystiques themes arrangement of chords overlapped, Logan Leaves the Institute and an alternate/Album Mix of Checkmate, all from the Complete Score Bootleg.

==Marketing==
On June 1, 2000, Marvel published a comic book prequel to X-Men, entitled X-Men: Beginnings, revealing the backstories of Magneto, Rogue and Wolverine.  There was also a comic book adaptation based on the film. 

===Video game===
 
A console video game was released on July 6, 2000 featuring costumes and other materials from the film. It received mixed to positive reviews from critics.

==Reception==

===Box office=== Fantastic Four, Hulk (film)|Hulk and Daredevil (film)|Daredevil).  X-Men was released in 3,025&nbsp;theaters in North America on July 14, 2000, earning $54,471,475 in its opening weekend. The film eventually grossed $157,299,717 and made $139,039,810 in other countries, coming to a worldwide total of $296,339,527.  X-Men was the ninth highest-grossing film of 2000.  The film made over $50 million in home video sales. 

===Critical response===
The film received positive reviews. Based on 153 reviews collected by Rotten Tomatoes, 82% were positive.  Metacritic collected an average score of 64/100 from 33 reviews. 

Kenneth Turan found "so much is happening you feel the immediate need of a sequel just as a reward for absorbing it all. While X-Men doesnt take your breath away wire-to-wire the way The Matrix did, its an accomplished piece of work with considerable pulp watchability to it."  ReelReviews.nets James Berardinelli, an X-Men comic book fan, believed, "the film is effectively paced with a good balance of exposition, character development, and special effects-enhanced action. Neither the plot nor the character relationships are difficult to follow, and the movie avoids the trap of spending too much time explaining things that dont need to be explained. X-Men fandom is likely to be divided over whether the picture is a success or a failure".  Desson Thomson of The Washington Post commented, " he movies enjoyable on the surface, but I suspect many people, even die-hards, will be less enthusiastic about what lies, or doesnt, underneath". 
 Ebert & Roeper.   Peter Travers of Rolling Stone noted, "Since its Wolverines movie, any X-Men or Women who dont hinge directly on his story get short shrift. As Storm, Halle Berry can do neat tricks with weather, but her role is gone with the wind. It sucks that Stewart and McKellen, two superb actors, are underused." 

===Awards===
  Best Science direction (Singer), writing (David costume design, Best Actor Supporting Actress Performance by Supporting Actor Special Effects and Saturn Award for Best Make-up|Make-up.  Empire (magazine)|Empire readers voted Singer Best Director. 

==Home media==
 
X-Men was originally released on DVD and VHS "a few months after its theatrical release".    The DVD included several bonus features:

* Deleted scenes
* "The Mutant Watch" featurette
* Excerpts from Bryan Singer interview on The Charlie Rose Show
* Hugh Jackmans screen test
* Still photo gallery
* TV spots

It was followed in 2003 by a two-disc DVD release entitled X-Men 1.5. The DVD includes the theatrical version of the film and some of the extra features from the previous release, with several new additional features, including: 

* Audio commentary by Bryan Singer and Brian Peck
* Several Making-Of features, with additional optional branching footage
* Trailers for X-Men 2 and Daredevil (film)|Daredevil

X-Men was released on Blu-ray in April 2009, with bonus features reproduced from the X-Men 1.5 DVD release.    Unlike the US edition, the UK release of the Blu-ray includes a picture-in-picture mode called "BonusView", and an in-feature photo gallery.   This was followed by a release of a Blu-ray/DVD/digital copy combo version in May 2011. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*   at Marvel.com

{{Navboxes|list1=
 
 
 
 
}}

 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 