Navra Maza Navsacha
 
{{Infobox film
| name       = Navra Maza Navsacha
| file name = नवरा माझा नवसाचा
| cover = navra_majha_navsacha_poster.jpg
| director     = Sachin Pilgaonkar
| producer    = Sushriya Arts
| released       =  
| country        = India
| language       = Marathi
}}

Navra Maza Navsacha ( . The film was later remade in kannada as Ekadantha.        The film stars Sachin Pilgaonkar and Supriya Pilgaonkar in leading roles. The movie portrays the journey of the couple from Mumbai to Ganapati Pule to fulfill a vow.

==Plot==
Vakratund (Sachin Pilgaonkar) and Bhakti (Supriya Pilgaonkar) is a married couple. They have married against Bhaktis parents wish. Vacky as he is known as is an artist but does not have commercial success. Even after 10 years of marriage the couple is not blessed with children.

Bhakti gets to know from Vackys aunt (Nirmiti Sawant) that Vacky was born by a vow taken by his father. Vackys father had vowed to God that if his child is born healthy he will bring his child to Ganapati Pule naked. This vow is left unfulfilled as Vackys parents pass away.

Bhakti tries to persuade Vacky to go to Ganpati Pule naked. But, he does not agree to it. As suggested by a friend, Vacky confronts a sage. Who advises to take a mannequin dressed as Vacky secretly in a public transport to Ganapti Pule to fulfill the vow. Vacky and his friend Kishore manage to sneak in the mannequin in a state transport bus the previous night of the travel to Ganapati Pule.

The remaining story has the highs and the lows that the couple face while travelling in the bus from Mumbai to Ganapati Pule. The situations and characters generate humor.

Finally, Vacky manages to fulfill the vow at Ganapati Pule.

==Cast==
* Sachin Pilgaonkar as Vakratund
* Supriya Pilgaonkar as Bhakti
* Ashok Saraf as Bus Conductor Lalu
* Vijay Patkar as Undercover Cop
* Ali Asghar as Film Director
* Reema Lagoo as Politician (Guest Appearance)
* Sonu Nigam as himself (Guest Appearance)
* Johny Lever as Nepali (Guest Appearance)
* Vijay Patwardhan
* Kuldip Pawar as Police Inspector
* Nirmiti Sawant as Vackys Aunt
* Kishori Shahane as Dancer (Guest Appearance)
* Satish Tare as Dhongi Baba
* Sunil Tawde as Bus Driver Prasad

==Remakes== Prema and Ramesh Aravind in the lead roles.

==Reception==
The film received good reviews.    The Indian Express a prominent Indian Daily named it as a part of a Remake Saga.
 In.com give Rating of 7.3 out of 10.

==References==
 

==External links==
*  

 