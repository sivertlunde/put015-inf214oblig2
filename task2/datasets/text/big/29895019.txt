Hero and the Terror
{{Infobox film
| name = Hero and the Terror 
| image = Hero and the Terror poster.jpg  
| caption = Theatrical release poster
| director = William Tannen
| producer = Menahem Golan Yoram Globus Lance Hool
| writer = Michael Blodgett
| starring = Chuck Norris 
| music = David Michael Frank  editing = Christian Wagner  cinematography = Eric Van Haren Noman  released =   distributor = Cannon Films runtime = 96 minutes country  = United States  language = English budget =   gross = $5,301,200 (USA)
}}

Hero and the Terror is a 1988 action film starring martial arts star Chuck Norris, directed by William Tannen. Produced by Menahem Golan, written by Michael Blodgett, and was distributed by Cannon Films. The film stars Norris as Danny OBrien as a cop trying to stop a serial killer, Simon Moon known as "The Terror".  

It is based on Michael Blodgetts 1982 novel of the same name.

==Plot==
Danny OBrien (Chuck Norris) is a cop who likes to work alone that never waits for his back up. In L.A. he is trying to apprehend the notorious Simon Moon (Jack OHalloran), also known as The Terror. Simon has been killing women by snapping their necks and taking them to his lair in an abandoned movie theater. OBrien is attacked by Simon who almost kills him in the struggle. When the killer flees the scene and climbs up a ladder he slips and falls, knocking himself unconscious. When the backup arrives they think OBrien caught The Terror and the people of L.A. call him "Hero". Simon is then arrested and taken to jail.

When Dr. Highwater (Billy Drago) goes to visit Simon he escapes by cutting through the bars of his cell. He then steals a laundry van by push starting it but loses control and falls straight down into a cliff face. When the media hears about this they pronounce Simon dead and the people of L.A. are relieved. 

Three years later the murders start back up again and OBrien thinks its The Terror. He eventually finds where his lair is and heads in to confront Simon himself. He encounters an enclosed room not on the map and heads in. In there he finds the bodies of The Terrors victims and starts searching around for him. Simon jumps out and attacks him and Danny tries to fight him off. OBrien eventually kills The Terror and the film ends.

== Cast ==

* Chuck Norris as Danny OBrien
* Brynn Thayer as Kay Steve James as Robinson
* Jack OHalloran as Simon Moon
* Jeffrey Kramer as Dwight
* Ron ONeal as Mayor
* Murphy Dunne as Theater Manager
* Heather Blodgett as Betsy
* Tony DiBenedetto as Dobeny
* Billy Drago as Dr. Highwater
* Joe Guzaldo as Copelli Peter Miller as Chief Bridges
* Karen Witter as Ginger
* Lorry Goldman as Gingers Manager
* Christine Wagner as Doctor

==Production==

Hero and the Terror was Chuck Norriss first major attempt at diversifying from his traditional martial arts roles.  

==Reception==

The movie had a mostly negative reception. It currently has a 0% rating on movie rating website Rotten Tomatoes. Despite the poor reception Chuck Norris acting was praised and it has a cult following.  Most fans call it one of his better movies.    

===Box office===

Hero and the Terror grossed $1.84 million nationwide its first weekend at the box office, finishing in a disappointing 12th place. 

== Release ==
The film premiered August 26, 1988 in the United States.  It will be first time released on Blu Ray in June 2015, by Kino Lorber. 

==References==
 

==External links==
* 
*  
*  

 
 
 
 
 
 