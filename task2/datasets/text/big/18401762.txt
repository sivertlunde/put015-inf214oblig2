Engine Sentai Go-onger: Boom Boom! Bang Bang! GekijōBang!!
{{Infobox film  name = Engine Sentai Go-onger: Boom Boom! Bang Bang! GekijōBang!! kanji = 炎神戦隊ゴーオンジャー BUNBUN！BANBAN！劇場BANG!! romaji = Enjin Sentai Gōonjā Bunbun! Banban! Gekijōban!!}} image = Kiva & Go-onger Movie.jpg caption = Film poster for both Engine Sentai Go-onger: Boom Boom! Bang Bang! GekijōBang!! and   director = Noboru Takemoto writer = Sho Aikawa producer = Toei
|starring = Yasuhisa Furuhara Shinwa Kataoka Rina Aizawa Masahiro Usui music = Megumi Ōhashi cinematography = Fumio Matsumura editing = Ren Sato distributor = Toei Co. Ltd released =   runtime = 35 minutes language = Japanese country = Japan italic title=force
}}

  is the theatrical film adaptation of the Japanese 2008 Super Sentai series Engine Sentai Go-onger.

As with all of the Super Sentai films, Boom Boom! Bang Bang! GekijōBang!! is a double bill with the film for the 2008  . Both films premiered in Japanese theaters on August 9, 2008. The film features guest star J-pop singer Sonim (singer)|Sonim, portraying the antagonist Empress Maki.  

==Plot== Edo Japan where power is essential. Attempting to find the missing Engine Casts of Speedor, Bus-on and Bearrv while Jumboale and Carrigator work hard to keep the portal between the two worlds open, the Go-ongers pursue the Honōshū.

After escaping a group of Samurai, the primary Go-ongers run for their lives before being saved by Tsuki-no-Wa, who brings them to the others. But Raiken and Gokumaru arrive with Hant and Gunpei after taking Birc and Gunpherds Engine Souls, forcing the Honōshū to reveal themselves as Engine Souls. However, they are unable to bond with the primary Engine Casts with the Yōma taking them. After learning the Honōshūs story, the Go-ongers offer their aid in storming Makis castle the day, finding the Sutō siblings who pretend to work for Maki in order to return the Engine Casts to them.

As the others destroy Makis Yōma servants with Hyper Cannonball to get back Shishi-no-Shin & Tsuki-no-Was Engine Casts, Go-on Red is overwhelmed by Maki until the Honōshū arrive. However, with Engine-Oh and the other Engines fighting Maki in her monstrous form before being broken down. But Sōsuke manages to grab on to one of Makis heads, fighting his way to Retsu-Takas Engine Cast. Once getting the Engine Cast, Maki assumes her true form as the Honōshū risk themselves to form Engine Daishougun with Sōsuke piloting them to destroy Maki and burn her castle to the ground. With the battle over, as Samurai Worlds denizens return to normal, Engine Daishogun turns to stone much to Sōsukes dismay. The Go-on teams soon return to their world.

==Characters==

===From series===
; : Nicknamed, the "Speed King," Sōsuke is a former racer with a sunny disposition. Unyielding and always full of confidence, he confronts anything without hesitation to fight right away. He is often seen flipping a Go-onger coin. As Go-on Red, his attacks are performed at high speed and his partner is Speedor.
; : Nicknamed the "Cyclopedia" for his attention to the smallest detail, Renn is a former bus driver who can be talkative and good at cooking. As Go-on Blue, Renns physical strength is magnified and his partner is Bus-on.
; : Nicknamed the "Sweet Angel", she used to work at a racing store. Cheerful and anxious, she keeps smiling with an upbeat attitude even in a crisis. As Go-on Yellow, she can fight in any hard terrain and her partner is Bearrv.
; : Nicknamed, the "Vagabond", Hant is a freeter working part-time at the Doki Doki Pizzeria until Gunpeis actions cost him his job. Hant becomes a Go-onger while assisting Gunpei. His Engine partner is Birca.
; : Nicknamed the "Chaser", Gunpei is a former police officer with excellent marksmanship and a passion for justice. He witnessed the Go-ongers in their first battles against the Gaiark, with his investigation of them allowing him to become a Go-onger himself. It took Gunpei a while to accept the primary Go-ongers, as he felt that their childish antics didnt give them the right to become heroes. His Engine partner is Gunpherd.
; : Nicknamed, the "Philosopher," leader of the Go-on Wings, who believes his team outranks the Go-ongers and is amazing in combat. His Engine partner is Toripter.
; : Nicknamed, the "Lovely Sensation," Hirotos younger sister, with a "princess" attitude and an interest in the Go-onger. Her Engine partner is Jetras.
; : Leader of the Gaiarks Land Pollution Branch. The Go-ongers encounter a ronin sounding like Yogostien in Samurai World.
; : Leader of the Gaiarks Water Pollution Branch. The Go-ongers encounter a woman with similarities to Kegalesia in Samurai World.
; : Leader of the Gaiarks Air Pollution Branch. The Go-ongers encounter a young samurai sounding like Kitaneidas in Samurai World.

===Film only=== hydra form named   until Go-on Red takes the Engine Cast, forcing the Yōma into her true form  . In the end, she was destroyed by Engine Dai-Shogun with her castle burned to the ground.
; : The blue lion-like Yōma servant of Maki created from Shishi-no-Shins Engine Cast, able to assume a vehicle form and uses a kusarigama/kusarifundo weapon. He was destroyed by Go-on Blue, Yellow, Green, Black, Gold, and Silvers Hyper Cannonball.
; : The golden bear-like Yōma servant of Maki created from Tsuki-no-Was Engine Cast, able to assume a vehicle form. He can wield four claws and a large shuriken. He was destroyed by Go-on Blue, Yellow, Green, Black, Gold, and Silvers Hyper Cannonball.

====Honōshū====
The movie also introduces the three   who sought to become stronger. They were robbed of their hearts and Engine Casts by Makis magic when they came to Samurai World, their souls assuming human forms called the   to fight her and restore themselves. With the Go-ongers aid, the Honōshū Warriors remember their reason for becoming stronger as they regain their Engine Casts and execute   to assume their true forms, combining into the Samurai World analog to the Go-ongers Engine-Oh,  . Its finishing attack is the   with its enlarged flaming  .    After Makis death, Engine Dai-Shogun exhausted its energy from fighting her and its components souls fade out as it is turned to stone.

*  is a young samurai warrior who is the leader of the Honōshū. His true form is  , the Samurai World analog to Speedor, a hybrid between a hawk and a racing car who forms the upper body and head of Engine Dai-Shogun. Prior to it, in order not to make his Engine Cast be use for evil once hes gone, he separated a part of his soul into the Engine Sword he created and gave to two brothers to safeguard for him.
*  is an older samurai warrior who wields a naginata. His true form is  , the Samurai World analog to Bus-on, a hybrid between a lion and a bus who forms Engine Dai-Shoguns hip, legs, and katana. bear and 4WD truck who forms Engine Dai-Shoguns lower body.

==Internet spin-off film==
On June 2, 2008, Toei announced on its various official websites that there would be a series of short five-minute internet movies that are Spin-off (media)|spin-offs of both King of the Castle in the Demon World and Boom Boom! Bang Bang! GekijōBang!! to be accessed by a mobile phone service. In the case of Boom Boom! Bang Bang! GekijōBang!!, the featurettes are called  , released on July 11, 2008. In "Bom Bom! Bom Bom! Net de Bong", the Gaiark Pollution Ministers begin setting up their plan break the dimensional barriers that separate the Braneworlds in order to create their Gaiark haven, setting up the opening sequences for the actual movie. However, while the Go-ongers are trapped in Samurai World, the Ministers attempt to take advantage of the event by subduing Bomper, who battles them personally when they attempt to set a trap for the Go-ongers as they return to their dimension.   

===Webisodes===
# 
# 
# 
# 
# 

==Cast==
*Sōsuke Esumi:  
*Renn Kōsaka:  
*Saki Rōyama:  
*Hant Jō:  
*Gunpei Ishihara:  
*Hiroto Sutō:  
*Miu Sutō:  
*Kegalesia, Landlady:  
*Empress Maki:  
*Retsu-Taka:  .   
*Shishi-no-Shin:  
*Tsuki-no-Wa:  
*Samurai:  

===Voice actors===
*Speedor:  
*Bus-on:  
*Bearrv:  
*Birca:  
*Gunpherd:  
*Carrigator:  
*Toripter:  
*Jetras:  
*Jum-bowhale:  
*Bomper:  
*Yogostein, Ronin:  
*Kitaneidas, Young Samurai:  
*Raiken:  
*Gokumaru:  

==Songs==
;Opening theme
* 
**Lyrics: Mike Sugiyama
**Composition: Takafumi Iwasaki
**Artist: Hideyuki Takahashi (Project. R)
;Ending theme
*  Engine Sentai Go-onger#endnoteQRap|* 
**Lyrics: Mike Sugiyama
**Composition & Arrangement: Kenichiro Ōishi
**Artist: Project. R (Takayoshi Tanimoto, Hideaki Takatori, Sister MAYO, Hideyuki Takahashi, Takafumi Iwasaki, YOFFY, Mayumi Gojo, Kenichiro Ōishi) with Engine Kids
:"Engine Formation Rap -GekijōBang! Custom-" was also used as the ending theme for episode 25 of the series.

==References==
 

==External links==
*  - Official movie website hub  
*  - Official movie website  

 

 
 
 
 
 