The Final Countdown Tour 1986: Live in Sweden – 20th Anniversary Edition
 
{{Infobox film
| name        = The Final Countdown Tour 1986: Live in Sweden - 20th Anniversary Edition
| image       = Europe The Final Countdown Tour 1986 20th Anniversary DVD.jpg
| director    =
| producer    =
| writer      = Europe
| Europe
| Warner Bros. Entertainment
| released    =  
| runtime     = 115 min Swedish
| imdb_id     =
| music       =
| awards      =
| budget      =
}}
 Swedish hard rock band Europe (band)|Europe. It was released on October 4, 2006. The main feature is a concert filmed at Solnahallen in  Solna Municipality|Solna, Sweden on May 26, 1986.

Interviews, unreleased pictures from the Final Countdown tour and biography are included as bonus material.

==Track listing== The Final Countdown" 
# "Wings of Tomorrow"
# "Ninja"
# "Carrie (song)|Carrie" 
# "On the Loose" Drum Solo
# "Cherokee (Europe song)|Cherokee" 
# "Time Has Come" Open Your Heart" Rock the Night"
# "Stormwind" 
# "Dance the Night Away"
# "The Final Countdown" (Reprise)

==Bonus Features==
*Band interviews made in 2006. Final Countdown recording sessions.
*The band revisiting the studio where they recorded The Final Countdown.
*Biography.

==Personnel==
*Joey Tempest – lead vocals, acoustic guitars
*John Norum – lead & rhythm guitars, backing vocals
*John Levén – bass guitar
*Mic Michaeli – keyboards, backing vocals
*Ian Haugland – drums, backing vocals

 

 
 
 