Suhana Safar
{{Infobox film
| name           = Suhana Safar
| image          = 
| image_size     = 
| caption        = 
| director       = Vijay
| producer       = 
| writer         =
| narrator       = 
| starring       =Shashi Kapoor  Sharmila Tagore
| music          =Laxmikant-Pyarelal
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1970
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1970 Bollywood drama film directed by Vijay. The film stars Shashi Kapoor and Sharmila Tagore.

==Cast==
*Shashi Kapoor ...  Sunil
*Sharmila Tagore ...  Sapna
*Lalita Pawar ...  Bus Passenger
*David Abraham ...  Bus Passenger 
*Manmohan Krishna ...  Mohan - bus passenger
*Master Bhagwan ...  Bus Passenger 
*Leela Mishra ...  Bus Passenger 
*K.N. Singh ...  Dr. Singh
*Ramayan Tiwari ...  Daku Mangal Singh  Sunder ...  Bus Passenger
*Randhir ...  Rana Randhir Singh Chauhan
*Mukri ...  Mukkaramjah Mknaik Mukri
*Keshto Mukherjee ...  Keshto 
*Mohan Sherry ...  Bus Driver 

==Soundtrack== 
With Laxmikant Pyarelals music and Anand Bakshis lyrics, Mohammad Rafi sang some fabulous songs for Shashi Kapoor. "Suhana Safar" & "Sari Khushiyan" remained evergreen hit.
{| border="7" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Suhana Safar " 
| Mohammad Rafi 
|-
| 2
| "Sari Khushiyan Hain"
| Mohammed Rafi
|-
| 3
| "Tim Tim Chamke Re Tara"
| Lata Mangeshkar
|-
| 4
| "Paise Ka Kya Yakeen" 
| Mohammed Rafi
|-
| 5
| "Suhana Safar (Female)"
| Suman Kalyanpur
|-
| 6
| "Chudiyan Bazar Se Mangwa De Re"
| Mohammed Rafi
|-
| 7
| "La Gloria (Music)"
|
|}

==External links==
*  

 
 
 
 

 
 