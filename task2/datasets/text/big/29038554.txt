Bibliothèque Pascal
{{Infobox film
| name           = Bibliothèque Pascal
| image          = Bibliotheque Pascal.jpg
| border         = yes
| caption        = Film poser
| director       = Szabolcs Hajdu
| producer       = 
| writer         = Szabolcs Hajdu
| starring       = Orsolya Török-Illyés
| music          = 
| cinematography = András Nagy
| editing        = Péter Politzer
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Hungary
| language       = Romanian, English, Hungarian
| budget         = 
}} Best Foreign Language Film at the 83rd Academy Awards,    but it didnt make the final shortlist.   

==Cast==
* Orsolya Török-Illyés as Mona
* Andi Vasluianu as Viorel
* Shamgar Amram as Pascal
* Razvan Vasilescu as Gigi Paparu
* Oana Pellea as Rodica
* Tibor Pálffy as Saxophone player
* Florin Piersic Jr. as Countryman
* Mihai Constantin as Gicu
* Orion Radies as Little boy
* Alexandru Bindea as Police chief
* Mihai Calin as Police officer

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Hungarian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 