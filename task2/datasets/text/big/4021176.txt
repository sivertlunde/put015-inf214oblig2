Sudden Death (1995 film)
{{Infobox film
| name = Sudden Death
| image = sudden_death.jpg
| caption = Theatrical release poster
| director = Peter Hyams
| producer = Howard Baldwin Moshe Diamant
| writer = Gene Quintano 
| story = Karen Elise Baldwin
| starring = Jean-Claude Van Damme Powers Boothe Raymond J. Barry Whittni Wright Ross Malinger Dorian Harewood
| music = John Debney
| cinematography = Peter Hyams
| editing = Steven Kemper
| studio = Shattered Productions
| distributor = Universal Pictures
| released =  
| runtime = 110 min.
| budget = $35 million
| gross = $64,350,171
}}
Sudden Death is a 1995 American action film directed by Peter Hyams, and starring Jean-Claude Van Damme and Powers Boothe. The film was released in the United States on December 22, 1995. The film, set in a hockey stadium, was written by Gene Quintano based on a story by Karen Elise Baldwin, the wife of Pittsburgh Penguins owner Howard Baldwin, who was the producer.
 Rapid Fire. the previous year.

==Plot== Pittsburgh Fire Civic Arena.
 United States government employee Joshua Foss (Powers Boothe) is holding Vice President of the United States|U.S. Vice President Daniel Binder (Raymond J. Barry) and several other VIPs hostage in a luxury suite.
 overtime and save his children simultaneously.

Darren is first pulled into the plot when Emily is kidnapped by Carla, the sole female member of the terrorists dressed as the mascot List of NHL mascots#Iceburgh|Iceburgh. She witnesses Carla murder numerous people, and is spared when she runs out of ammunition. Carla places Emily in the suite with the other hostages about to be executed. Darren, who had given pursuit, is left to search in vain. Realizing she has been "made", Carla returns to deal with Darren and a long fight breaks out. He proves to be more than a match for Carla. Darren manages to kill Carla by kicking her into a large dishwasher, which pulls on her penguins head strap and strangles her.
 Secret Service Agent Matthew Hallmark (Dorian Harewood), who advises that Darren stand by while the agents take charge. He angrily refuses, saying that he will handle this himself.

The Secret Service and the Pittsburgh Police team up to surround and cordone off the arena and a standoff ensues. Meanwhile, Darren manages to find a few of the bombs and disable them, whilst Foss goes about killing several hostages. Eventually, Hallmark manages to sneak inside and meet with Darren. It quickly transpires that Hallmark is just another one of Foss puppets, influenced by money. Darren kills Hallmark and uses Hallmarks phone to contact Foss, who taunts the man with the news that he is holding his daughter captive.
 sudden death and prolonging the game, but only until the next goal is made. Darren decides that theres no time left to find the remaining bombs and climbs up to the roof of the arena. He advances upon the owners box from above and forces his way in, rescuing Emily and the remaining hostages.

Meanwhile, Foss manages to escape and blend in with the chaos that has ensued by one of Foss henchmen falling from the roof through the score display earlier and blowing it up. However, Foss sets off one of the bombs, flooding part of the arena, and recaptures Emily when she recognizes him. They head up towards the top of the arena, where a helicopter is waiting to lift Foss away. Darren intervenes and saves his daughter. Foss flees, and a wounded Darren shoots the pilot through the floor. The pilot falls back taking the joystick with him, stalling out the chopper and a screaming Foss is killed as the chopper goes into the arena and explodes on impact with the ice.

Darren is being led to a waiting ambulance, his son and daughter comment to the paramedics about how their father is a hero, while Tyler had before told Emily that their father was too scared to be a fireman again. A contented Darren is put inside the ambulance as the film ends.

==Cast== Civic Arena 
*Powers Boothe as Joshua Foss, a traitorous Secret Service Counterfeit Division agent who leads a team of terrorists
*Raymond J. Barry as Vice President of the United States|U.S. Vice President Daniel Binder
*Whittni Wright as Emily McCord, Darrens daughter
*Ross Malinger as Tyler McCord, Darrens son Secret Service agent in charge of the Vice-Presidents protection detail; later revealed to be working with Joshua Foss
*Kate McNeil as Kathi
*Michael Gaston as Hickey, Fosss computer hacker
*Audra Lindley as Mrs. Ferrara 
*Brian Delate as Blair
*Faith Minton as Carla, Fosss only henchwoman
*Manny Perry as Brody, Fosss henchman
*Brian Hutchison as Young Secret Service Agent

==Hockey figures==
*Jay Caufield as Brad Tolliver
*Bill Clement as Pre-game announcer Cleveland Lumberjacks players as Chicago Blackhawks players 
*Ian Moran as Chris Chelios 
*Jeff Jimerson as Himself (credited "Anthem Singer")
*Mike Lange as Himself (credited "Play-by-Play Announcer")
*Luc Robitaille as Himself
*Paul Steigerwald as Himself (credited "Color Commentator")
*Markus Näslund as Himself (uncredited)
*Bernie Nicholls as Himself (uncredited)
*Ken Wregget as Himself (uncredited)
*John Barbero as PA Announcer (uncredited)

==Production==
It is set and filmed in Pittsburgh, Pennsylvania and Middletown, New York in 98 days on August 29 and December 7, 1994.

==Reception==
The movie received a mixed reaction.     Sudden Death currently holds a 52 percent rating on Rotten Tomatoes. However, some considered Sudden Death one of Van Dammes best films to date (alongside Hard Target and Timecop). Critic Roger Ebert stated that "Sudden Death isnt about common sense. Its about the manipulation of action and special-effects sequences to create a thriller effect, and at that its pretty good." 

===Box office===
Sudden Death opened in the  s Heat (1995 film)|Heat.

==Novelization==
{{Infobox book |  
| name         = Sudden Death
| title_orig   = 
| translator   = 
| image        = 
| caption = 
| author       = Stephen Mertz
| cover_artist = 
| country      = United States
| language     = English
| series       = 
| genre        = Novel
| publisher    = Boulevard Books
| release_date = 1995
| media_type   = Print (Paperback)
| pages        = 218 pp
| isbn         = 1-57297-032-4
| oclc          = 33266093
}}
 American writer Stephen Mertz {{cite journal 
 | title = Stephen Mertz, Contemporary Authors Online, Detroit: Thomson Gale, 2008}}  The audio book is read by Powers Boothe.
 

==References==
 

==External links==
* 
* 
* 
* , retrieved October 26, 2011

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 