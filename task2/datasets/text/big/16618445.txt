The Turning Point (1945 film)
{{Infobox film
| name           = The Turning Point (Великий перелом)
| image          = The Turning Point (1945 film).jpg
| image_size     = 
| caption        = 
| director       = Fridrikh Ermler
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Lenfilm
| distributor    = 
| released       = 1945
| runtime        = 108 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
| gross          = 
}}
The Turning Point ( , Velikij perelom) is a Soviet 1945 film directed by Fridrikh Ermler based on a screenplay by Boris Chirskov. The film was one of the earlier Palme dOr winners of 1946. Runtime - 108 min. It was produced by Lenfilm, distributed in the USA by Artkino Pictures, and restored in 1967 by Lenfilm. Working title - General of the Army ( )

==Cast==
* Mikhail Derzhavin as Col. Gen. Muravyov
* Pyotr Andriyevsky as Col. Gen. Vinogradov
* Yuri Tolubeyev as Lavrov
* Andrei Abrikosov as Lt. Gen. Krivenko
* Aleksandr Zrazhevsky as Lt. Gen. Panteleyev
* Nikolai Korn
* Mark Bernes as Minutka
* Vladimir Marev
* Pavel Volkov as Stepan

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 
 