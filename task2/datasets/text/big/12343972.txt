People Are Bunny
 
{{Infobox Hollywood cartoon
| cartoon_name               = People Are Bunny
| series                     = Merrie Melodies/Bugs Bunny and Daffy Duck
| image                      = PeopleareBunny Lobby Card.PNG
| caption                    = Lobby card
| director                   = Robert McKimson John W. Burton, Sr.
| story_artist               = Tedd Pierce
| animator                   = Ted Bonnicksen Warren Batchelder Tom Ray George Grandpre
| layout_artist              = Robert Gribbroek William Butler
| film (sound) editor_artist = Treg Brown Frank Nelson (uncredited)
| musician                   = Milt Franklyn
| distributor                = Warner Bros. Pictures, Inc.
| release_date               = December 19, 1959 (USA)
| color_process              = Technicolor
| runtime                    = 6:10
| movie_language             = English
}}
People Are Bunny is a 1959 Warner Bros. Merrie Melodies cartoon, directed by Robert McKimson, starring Bugs Bunny and Daffy Duck. People Are Bunny spoofs the Art Linkletter show People Are Funny (where people performed different "stunts" to win money) where the parody of Art Linkletter is voiced by an uncredited Daws Butler.

==Plot==
  Frank Nelson, offers $1,000.00 for the first viewer to bring a rabbit to Station QTTV. Attempting to convince Bugs Bunny to come to the station, Daffy first tries a ruse with TV show tickets, but Bugs immediately suspects Daffy is up to no good and declines. Daffy then grabs a gun from Bugs fireplace and tells Bugs to oblige or be shot.

At the scene of Station QTTV, Daffy has Bugs at gunpoint when they see a parade of prizes coming out of a studio (car, boat, fur coat, refrigerator, "Key to Fort Knox", etc.), and they see people going into the show "People Are Phoney" starring Art Lamplighter. With dollar signs in his eyes, Daffy locks Bugs in a telephone booth and runs into the studio. Bugs receives a call in the telephone booth from an announcer who tells Bugs if he correctly answers a question, he will win a jackpot. Bugs answers the math question and the jackpot dispenses through the coin return slot. The announcer then asks Bugs how he knew the answer so quickly. Bugs says, "One thing we rabbits know how to do is multiply."

Meanwhile, Daffy appears as a contestant on People Are Phoney, where his task is to help a little old lady across the street while on camera. Things backfire in a hurry when the old lady starts belting Daffy with her umbrella, belligerently declaring she doesnt need help crossing the street. Daffy staggers, is missed by a speeding truck ("Nyaah, ya missed me", he gloats, sticking out his tongue), then gets hit by a motorcycle. Art Lamplighter tells the hysterical audience that Daffy didnt quite make it, and it goes to show that "People Are Phoney."

Sorely mad, Daffy comes back to the telephone booth where Bugs is counting the jackpot. Bugs says he got a call in the phone booth, which Daffy doesnt believe. Bugs says at any time now an announcer might call again. Bugs makes the sound of a ringing phone and cons Daffy into thinking they want another contestant. Daffy pushes Bugs out of the booth, telling Bugs to let him have it. Daffy grabs the "receiver" - now a stick of dynamite - and it explodes as Bugs walks away. He shrug: "So I let him have it."
 usher (actually You Are Indians as he mutters "All right, wheres the wise guy?" slapping his scalp back onto his head.

At the end, Bugs is disguised as a producer and he tells Daffy that hes suddenly wanted for Costume Party (a reference to the real Masquerade Party), tricking him into donning a rabbit costume. The show he is sent to is the QTTV Sportsman Hour to which Daffy intended to bring Bugs, and Bugs collects the fee Daffy wanted for himself. When Daffy protests that he is no rabbit but a duck, the host declares it is now duck season, and a bunch of hunters shoot at Daffy. Bugs shrugs off Daffys plight, noting: "Eh, they always shoot blanks on TV," Daffy, his beak full of bullet holes, mutters: "Blanks, he says." Emptying a stack of buckshot from his mouth, he offers them to Bugs: "Have a handful of blanks! Sheesh!"

==Production==
The short reuses animation from "Wideo Wabbit", "Bonanza Bunny", and "A Star is Bored". 

==Availability==
"People Are Bunny" is available, uncensored and uncut, on the Looney Tunes Superstars DVD. However, it was cropped to widescreen.

==Edited versions==
* On the Fox version of The Merrie Melodies Show, the part where Daffy walks up to Bugs after getting blasted by the hunters, spits out a handful of buckshots, and groans: "Blanks, he says! Have a handful of blanks" was cut with a fake iris-out after Bugs says: "Eh, they   always use blanks on TV."

* The syndicated version of The Merrie Melodies Show leaves in the ending, but replaces Daffy getting shot by the hunters with a still shot of Bugs.  

==References==
 

==External links==
*  

 
{{Succession box 
| before = A Witchs Tangled Hare  Bugs Bunny Cartoons 
| years  = 1959 
| after  = Horse Hare
}}
 

 
 
 
 
 