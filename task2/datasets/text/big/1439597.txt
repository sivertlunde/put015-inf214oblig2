Doug's 1st Movie
 
{{Infobox film
| name           = Dougs 1st Movie
| image          = Dougs 1st Movie Poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Maurice Joyce
| producer       = David Campbell Melanie Grisanti Jim Jinkins Bruce Knapp Jack Spillum
| writer         = Ken Scarborough
| based on       =   Fred Newman Chris Phillips Constance Shulman Frank Welker Alice Playten Guy Hadley
| music          = Mark Watters
| editing        = Alysha Cohen Christopher Gee Jumbo Pictures Walt Disney Television Animation Buena Vista Pictures Distribution
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         = $5 million
| gross          = $19.4 million 
}} Disney version Nickelodeon television Fred Newman, Chris Phillips, Jumbo Pictures Buena Vista, Donald and Daisy Duck.

==Plot==
Doug and Skeeter discover a monster that lives in Lucky Duck Lake. Believing in his evil deeds, they are scared of him at frirst, but later on, they find him to be nice. Despite this, however, the monster is proof that Bill Bluff is polluting the lake, and this fact created this monster. The Monster almost eats the book Moby Dick, but Skeeter stops him and says, "Stop! You almost ate Herman Melville! You dont eat books, that is a NO!" and the monster returns it to them as an apology, so they name him Herman Melville. Unfortunately for them, all of this commotion with the monster makes Doug forget that he was supposed to meet Patti at Mr. Swirlys. When he remembers this at the last minute, he runs to Swirlys as fast as he can using the quickest shortcuts that he knows. Once he gets there, Patti is nowhere to be seen. He asks Mr. Swirly if hes seen her, and he says the she was here for a while and looked very upset, and then left with a guy who kept talking about his big plans for a dance. When Doug heard this, he knew that this guy was no one else but Guy Graham, a snobby upper class man who wants Patti. Meanwhile, Roger and the AV nerds are building a robot to kidnap Herman, but when they build the robot it acts like Rogers babysitter.

Doug then rushes to the Funky Town night club, where Guy and Patti are working on the dance. He apologizes to Patti there and she accepts his apology, but Guy cuts in and says that Doug is "just a stupid little kid." Doug, very angry now, says that he has proof that Bluff is evil and is polluting the lake. Guy then calls Doug a liar. Doug then invites them both to the report that is being held in front of Mr. Dinks house about Herman and the pollution. Doug then leaves, but the picture of the monster falls out of his pocket without him knowing. Guy picks up the picture and realizes that Doug was telling the truth; he then calls Bluff, with whom he has connections to. At the reporting, Doug sees that a news reporters camera is inflatable. He then realizes that the news company is a fake, that it is supporting Bill Bluff and trying to compensate the monster. Doug then has to tell everyone that there has been a mistake, Patti gets mad at him, thinks that he is a liar and walks away with Guy.

That night, Bluff finds the boys with Herman and kidnaps the monster. The next morning, Doug knows that this is his last chance to save Herman. He goes to the school newspaper room, hoping to find Guy who can lead him to Bluff. Hoping he can asked Guy for help and he was going to swallow his pride for nothing because Guy wasnt going to help him. Guy isnt in the room, but Doug sees a newspaper article that says that Mr. Bluff and his men blast a monster to smitherines at a school dance. Doug is at first sad and believes that Herman has died, but he then realizes that the school dance isnt until tonight and this is what is being planned to happen so, Doug and Skeeter call Roger and The Sleech twins to help. At the school dance, he has to make the biggest choice of his life, going after Patti or saving Herman. When he does that, Bluff catches the quintet in front of Crystal Lake and starts to enslave them, but is stopped by Mrs. Dink. Beebe arrives and forces Bluff off of the scene to defend her friends. Doug then finds Patti in front of the woods and Doug tries to tell her he is in love with her but is interrupted by Herman. With the return of Herman and a copy of the newspaper Patti sees that Doug had been telling her the truth all along and dumps Guy. Also, Skeeter gives Herman the Moby Dick book for something to eat in the lake and Herman also gives Doug a flower to give to Porkchop to which Porkchop comes running out of the woods into Hermans arms. The kids say goodbye to Herman; after Herman jumps back into the lake, Doug tells Patti he likes her and Roger almost becomes friends with Doug, but is interrupted by the robot. Doug starts dancing with Patti and Skeeter dances with Beebe as the music continues in the background, ending the film.

==Cast and Characters==
Main Characters
*Doug Funnie: Shy, kindhearted, and determined. At first he is interested in finding Herman, but as Valentines Day approaches, he has to safely reveal him to Patti to impress her.
*Skeeter Valentine: determined, kindhearted, and intelligent. He, along with Doug is willing to find Herman, but soon realizes they have to protect it from the clutches of Mr. Bluff
*Roger Klotz- Selfish, moody, and cunning. When Roger discovers that Herman is siding with Doug and Skeeter, he turns over a soft spot and decides to help defend Herman, not knowing he is actually good.
*Herman Melville- The monster. Kindhearted, curious, and loving- At first, he appears intimidating to Doug and Skeeter, but when he shows them he is actually good, he befriends them.
*Bill Bluff- Wealthy, greedy, and cruel towards children. When he discovers that Doug and Skeeter found the monster he created, he intends to destroy it, not knowing the creature was actually good.
*Guy Graham- Arrogant, self-centered, and narcissistic. He is an upper-classman who is friends with Mr. Bluff, and intends to steal Patti from Doug. His downfall was that he writes news on the paper before it happen, and this caused Doug to find out what they were going to do to Herman Melville and Doug saves Herman Melville and exposed Guys evil plot.

Cast
* Tom McHugh as Doug Funnie, Lincoln Fred Newman as Skeeter Valentine, Mr. Dink, Porkchop, Ned Chris Phillips as Roger Klotz, Boomer, Larry, Mr. Chiminy
* Constance Shulman as Patti Mayonnaise
* Frank Welker as Herman Melville
* Alice Playten as Beebe Bluff, Elmo (This would be her final role in an animated movie before her death in 2011)
* Guy Hadley as Guy Graham
* Doug Preis as Mr. Funnie, Mr. Bluff, Willie, Chalky, Bluff Agent
* Eddie Korbich as Al & Moo Sleech, Robocrusher David OBrien as Quailman Announcer
* Doris Belack as Mayor Tippi Dink
* Becca Lish as Judy Funnie, Mrs. Funnie, Connie Greg Lee as Principal White
* Bob Bottone as Bluff Assistant
* Bruce Bayley Johnson as Mr. Swirly
* Fran Brill as Mrs. Perigrew
* Melissa Greenspan as Briar Langolier

=== Additional voice artists ===
* Rodger Bumpass
* Paul Eiding
* Jackie Gonneau
* Sherry Lynn
* Mickie McGowan
* Phil Proctor
* Brianne Siddall
* Claudette Wells

==Production== Nickelodeon set 1998 film by Paramount Pictures). 
 Jumbo Pictures along with the cartoon, they decided to re-circle the project for the Doug film. This film was originally planned as a direct-to-video release under the title The First Doug Movie Ever, but due to the success of The Rugrats Movie, it saw a theatrical release. The film was released on VHS on September 21, 1999, and on DVD as a Disney Movie Club exclusive on July 20, 2012. However, the DVD used the TV edit version with commercial fade outs and sped up end credits.

==Release==

===Critical reception===
 
The film garnered a 26% approval rating on Rotten Tomatoes, with 8 of a total 32 reviews being determined as positive.  Critics were harsh to Dougs 1st Movie when it was released theatrically. Many noted that the film felt too much like an extended episode of the show (story and animation-wise) and many mention that the film should have stayed a direct-to-video release. Screenit.com awarded the film 4 out of 10. The reviewer mentioned it was mediocre and did not have "that magic or cinematic feel to warrant the big screen treatment" and it felt like the regular series. 

===Box office===
Dougs 1st Movie, and ironically also his last, opened at #5 in its opening weekend with $4,470,489, for an average of $1,971 from a very wide 2,268 theaters. While this may be deemed as low for an average Hollywood film, Doug only cost $5 million to make due to its direct-to-video budget and a somewhat low-key promotional campaign. As such, the film still managed to gross $19,421,271 in ticket sales, creating a large profit for Disney and making it a box office success.

==References==
 

==External links==
*  
*  
*  
*  
 
 

 
 
 
 
 
 
 