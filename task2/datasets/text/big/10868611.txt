Nervous Night
{{Infobox Film
| name           = Nervous Night
| image          = HootersNervousNight.jpg
| image_size     = 
| caption        =  John Charles Jopson 
| producer       = Karen Bellone
| writer         = 
| narrator       = 
| starring       = The Hooters
| music          = The Hooters
| cinematography = Julio Macat Anastas Michos
| editing        = 
| distributor    = CBS/Fox Video
| released       = October 10, 1986
| runtime        = 60 minutes USA
| English
}}
 1986 film John Charles Jopson.

==Background==
As a follow-up to The Hooters 1985   in Philadelphia, and a series of short films, each one starring a different band member.
 All You Time After Time" (a No. 1 song for Cyndi Lauper that was co-written by band member Rob Hyman), "And We Danced" and "Day By Day."

==Awards==
At Billboard (magazine)|Billboards 8th Annual Video Music Conference on November 22, 1986, The Hooters received two awards: For Nervous Night: Best Longform Program and Best Concert Performance for the "Where Do the Children Go" video taken from the film.

==Personnel==
*Eric Bazilian: lead vocals, guitar, mandolin
*Rob Hyman: lead vocals, keyboards, accordion
*David Uosikkinen: drums
*John Lilley: guitar
*Andy King: bass guitar, vocals

== External links ==
*  

 

 
 

 