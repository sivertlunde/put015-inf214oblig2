The Best Man (1964 film)
{{Infobox film
| name           = The Best Man 
| image          = Best man22234.jpg
| image_size     =
| caption        = theatrical poster
| writer         = Gore Vidal  Kevin McCarthy 
| director       = Franklin J. Schaffner 
| producer       = Stuart Millar Lawrence Turman 
| music          = Mort Lindsey 
| cinematography = Haskell Wexler
| editing        = Robert Swink
| distributor    = United Artists 
| released       = April 5, 1964 (US)
| runtime        = 102 minutes 
| country        = United States
| language       = English
| budget         =
| gross          =
}}
  play of Kevin McCarthy.  

Tracy was nominated for an Academy Award for Best Supporting Actor for this, his final film.

==Plot==
William Russell (Fonda) and Joe Cantwell (Robertson) are the two leading candidates for the presidential nomination of an unspecified political party. Both have potentially fatal vulnerabilities.
 Adlai Stevenson). A sexual indiscretion has alienated his wife Alice (Margaret Leighton). In addition, he has a past nervous breakdown to live down.

Cantwell (believed to be based upon John F. Kennedy with some Richard Nixon and Joseph McCarthy mixed in) portrays himself as a populist "man of the people," and patriotic anti-communist campaigning to end "the missile gap" (a Kennedy campaign catch-phrase). Cantwell is a ruthless opportunist, willing to go to any lengths to get the nomination. 
 nominating convention in Los Angeles and lobby for the crucial support of dying former President Art Hockstader (Tracy). The pragmatic Hockstader (a character based on Harry Truman, particularly his comments on "striking a blow for liberty" whenever he drinks a bourbon) prefers Russell, but worries about his indecisiveness and overdedication to principle; he despises Cantwell, but appreciates his toughness and willingness to do what it takes.
 Bobby Kennedy who was known as "Ruthless Robert" in political circles during the 1950s and early 1960s). Cantwell has mistakenly assumed that Hockstader was for the more liberal man. The former president tells Cantwell that he doesnt mind a "bastard" but objects to a stupid one. He publicly endorses neither man.
 Lyndon Johnson). 

One of Russells aides digs up Sheldon Bascomb (Shelley Berman), who served in the military with Cantwell and is willing to link him to homosexual activity while stationed in Alaska during World War II. Hockstader and Russells closest advisors press Russell to grab the opportunity, but he resists. In a memorable line, Hockstader says that he doesnt care if Cantwell "has had carnal knowledge with a McCormick Reaper," but still thinks the dirt should be used against him.

After the first ballot, Russell arranges to meet Cantwell privately, but when Bascomb is confronted face-to-face by Cantwell, he refutes his slander. Russell threatens to use the allegation anyway, but Cantwell knows Russell does not have the stomach for tactics that dirty.
 Governor Robert Meyner). It puts an end to both their chances, but unites the party around an electable candidate.

==Cast== Secretary of State
*Cliff Robertson as Joe Cantwell, a sitting United States Senator|U.S. Senator
*Lee Tracy as Art Hockstader, the former President of the United States
*Edie Adams as Mabel Cantwell
*Margaret Leighton as Alice Russell
*Ann Sothern as Sue Ellen Gamadge, the partys female Vice Chair
*Shelley Berman as Sheldon Bascomb, a former Army comrade of Cantwells
*Gene Raymond as Don Cantwell, Joes brother and campaign manager Kevin McCarthy as Dick Jensen, Russells campaign manager
*John Henry Faulk as Governor T.T. Claypoole
*Richard Arlen as Senator Oscar Anderson
*Howard K. Smith as Himself
*Gore Vidal made an uncredited cameo appearance as a senator/convention delegate
 

Tracy repeated the role of Hockstader that he had originated on stage.  Tracy was nominated for a Best Supporting Actor Academy Award (but lost to Peter Ustinov in Topkapi (film)|Topkapi).

Faulk was a Texas-based radio personality who was blacklisted during the 1950s and won a lawsuit that helped restore his reputation. 

Kevin McCarthy was a cousin of Eugene McCarthy, who became a presidential contender in 1968.

Bosley Crowthers review of the film in the New York Times cited William R. Ebersol in the role of Governor John Merwin as one of those who "stand out in a cast that is notable for its authenticity."  It was Ebersols only film and he does not speak.

==References==
 

==External links==
*  
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 