Účastníci zájezdu
<!-- Hide infobox until needed
{{Infobox film
| name           =
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = 
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
--->  Czech comedy film based on the 1996 novel Účastníci zájezdu (The Sightseers) by Michal Viewegh. It was released in 2006.

== Cast ==
* David Hlaváč - Roko
* Šárka Opršálová - Irma
* Miroslav Krobot - Karel (older)
* Eva Leinweberová - Jituš
* Anna Polívková - Jolana
* Eva Holubová - Jolanas mother
* Bohumil Klepl - Jolanas father
* Jana Štěpánková - Helga 
* Jitka Kocurová - Pamela 
* Kristýna Leichtová - Denisa 
* Martin Pechlát - Vladimír 
* Martin Sitta - Jarda
* Jaromír Nosek - Ignác 
* Ondrej Koval - Max 

==External links==
*  

 
 
 
 


 