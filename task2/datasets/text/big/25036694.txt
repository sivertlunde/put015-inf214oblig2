The Big Cat (film)
 
 
{{Infobox film
| name           = The Big Cat
| image          = The_Big_Cat_(1949_film)_video_cover.jpg
| image_size     =
| caption        = DVD cover
| director       = Phil Karlson
| producer       = William Moss (producer)
| screenplay     = Morton Grant and Dorothy Yost
| story          = Morton Grant
| narrator       = Cast section
| music          = Paul Sawtell
| cinematography = W. Howard Greene
| editing        = Harvey Manger
| distributor    = Eagle-Lion
| released       = 
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

The Big Cat is a 1949 American outdoor action film in Technicolor directed by Phil Karlson.  The cast included Lon McCallister, Peggy Ann Garner, Preston Foster, Forrest Tucker, Skip Homeier, and Gene Reynolds.

==Plot==
Drought during the 1930s forces a large cougar to come down from the high country in Utah to prey on farmers cattle. This has prompted many farmers to pursue and kill the cat, but so far all have failed.

Danny Turner (Lon McCallister) arrives in the area to move into his mothers birthplace now owned by his stepfather Tom Eggars (Preston Foster). Tom is constantly threatened by a hostile neighbor, Gil Hawkes (Forrest Tucker). Shortly after Danny arrives Tom and Gil have a scuffle, a sure sign of a soon-to-be war. When Danny arrives he meets Doris Cooper (Peggy Ann Garner), whom he develops a crush on. After some close calls with Gil, the drought and the cougar become such big issues that the mayor of a nearby town announces that a hunt be started for the big cat. The hunt is unsuccessful, with the Eggars dog chasing the cougar back to its lair and the party left behind. Tom tells Danny that he may have to move away and life with the Hawkes family due to the troubles he is having around the farm.

The next day he goes to the hill to show Gil and his family that he will come live with them. Gils son Jim (Skip Homeier) teases Danny about his failure to kill the cougar and their dislike of Tom. Danny then refuses to live with them because he remembers Gil is the one who mistreated his mother and refused to let her marry Tom. Tom hears this and attacks Gil, and the two fight. Danny and Jim also get into a fight when the angered Jim accuses Danny of starting the fight. Danny and Tom win both fights and Tom tells Gil that Danny will continue living with him.

The next morning Tom takes Danny hunting, but due to his dislike of killing animals Danny refuses to take a shot at a deer. Understanding this, Tom takes the gun and shoots the deer himself. However, the cougar hears the noise and follows them to Toms cabin. While Tom and Danny are in the shed, preparing to cut up the deer, the cougar enters the farm and nibbles at the carcass. Tom fires at the cougar and pursues it into the woods. Meanwhile, Danny goes back into the woods to retrieve a gun he left there. Tom chases the cougar, firing at it, but the cougar then surprises him by attacking from above. The cougar kills Tom and runs off into the wilderness.

Grieved at Toms death, Doris pleads for Danny not to go after the dangerous cougar, but the vengeful Danny vows to do so. With his dog, Danny leaves in search of the cougar. He and his dog have an eventful chase with the cat before they corner it inside a small cave. Unable to see in the dark, Danny and his dog are at a disadvantage and are attacked by the cougar. In the ensuing battle, Dannys dog is almost killed, but Danny is able to kill the cougar. Later Danny and Doris celebrate the cats defeat. With the cat gone, Gil and Jim will no longer be a threat to Danny and Doris, now possible lovers.

==Cast==
*Lon McCallister as Danny Turner
*Peggy Ann Garner as Doris Cooper
*Preston Foster as Tom Eggers
*Forrest Tucker as Gil Hawks
*Skip Homeier as Jim Hawks, Gil Son
*Sara Haden as Mrs. Mary Cooper
*Irving Bacon as Matt Cooper the Mailman
*Gene Reynolds as Wid Hawks, Gil Son

==Production==
Scenes were shot in Bryce Canyon National Park and Cedar City, Utah. On February 12, 1954, it was reissued by Carroll Pictures. The film was released for theatrical and television screenings on March 26, 1954, by Samba Pictures, Inc.  The film, which entered the public domain in 1976, has been released on DVD. 

==References==
 

==External links==
* 
* 
*   complete film on YouTube

 

 
 
 
 
 
 
 
 
 