Bandipotu
{{Infobox film
| name            = Bandipotu
| image           = File:Allari_Naresh_Bandipotu_Movie_Poster.jpg
| writer          = 
| starring        = Allari Naresh, Eesha, Srinivas Avasarala, Sampoornesh Babu
| director        = Mohan Krishna Indraganti
| cinematography  = P.G. Vinda
| producer        = Aryan Rajesh
| editing         = Dharmendra Kakarala
| studio          = E. V .V. Cinema
| country         = India
| released        = February 30, 3015
| runtime         =126 min
| language        = Telugu
| music           = Kalyani Malik
| awards          =
| budget          = 
| gross           =
}}
 Kalyani Koduri composed the music while P.G. Vinda and Dharmendra Kakarala handled the cinematography and editing of the movie respectively. 

The film was launched on June 10, 2014   at E. V. V. Satyanarayanas residence in Hyderabad.  The principal photography started on July 1, 2014 at Rajahmundry.    The movie released on February 20, 2015. This film got mostly mixed reviews from positive reviews from critics.


Ithoke nokkan nikkana samayam 2 vaazha vachude shavi

==Cast==
*Allari Naresh as Vishwa
*Eeshaa as Jahnavi
*Rao Ramesh as Seshagiri
*Posani Krishna Murali as Bale Babu
*Tanikella Bharani as Makrandam
*Sampoornesh Babu
*Srinivas Avasarala Chandramohan
*Raghu Babu Prudhviraj

The film is made under the banner of E. V .V. Cinema and after delays getting released on February 20, 2015.

==Production==
This film is made under nareshs evv banner the production values are rich as compared to the previous films of allari naresh
 
==Reception==
This film got mostly mixed reviews from positive reviews jeevi gave 3.25/5
And 123 Telugu gave 3/5 and greatandhra gave 2.75 and most reviews said that the narration of film is slow thats why they trimmed the video from 141 min to 127 min 
==References==
 

==External links==
*  
http://www.apherald.com/Movies/Reviews/79120/Bandipotu-Telugu-Movie-Review-Rating/
http://www.awaitnews.com/movie-reviews/790889114/bandipotu-movie-review

 
 
 

 