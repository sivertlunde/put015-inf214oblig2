Armaan (1966 film)
{{Infobox film
| name           = Armaan
| image          = 66arman1.jpg
| image_size     =
| caption        = Armaan Pakistans first Platinum Jubilee film
| director       = Pervaiz Malik
| producer       = Waheed Murad
| writer         = Waheed Murad
| narrator       =
| starring       = Waheed Murad Zeba Zahoor Ahmad Bibbo Rozina Nirala
| music          = Sohail Rana
| cinematography =
| editing        =
| distributor    = Film Arts
| released       =  
| runtime        = approx. 3 hours
| country        = Pakistan
| language       = Urdu
| budget         =
}}
Armaan ( i black-and-white film produced by Waheed Murad and directed by Pervaiz Malik. It was the first Pakistani film to complete 75 weeks in cinemas and, thus, became the first Pakistani "Platinum Jubilee" film. The film was picturized in black-and-white prints in Karachi.    

== Plot ==
 
The film tells of a beautiful but underprivileged girl Najma (Zeba) living in Murree with her aunt (Bibbo) and her two daughters Dolly and Seema. One Morning she is told to escort Seema home as she has returned from a trip. When she arrives, a tearful Seema tells her of an affair she had with a man called Sohail. As a result of the affair she had become pregnant, however she abstained from telling Sohail as he had finally found the job of his dreams and was going away to work. So she had the baby, hid the child from the world and awaited Sohails return. But he never came. She entrusts Najma with the responsibility of taking care of the child and keeping her secret. Najma agrees and takes her home, and she leaves the baby with an old peasant in the hills for safekeeping.

Meanwhile, in Karachi, the son of Seth Khan Bahadur (Zahoor Ahmed), Nasir (Waheed Murad), leads a privileged life and is habitual of going to nightclubs and various parties with his friend Shahid (Nirala). One day, after coming home late from a night club, his father catches him red handed and proceeds to scold him. After this he tells Nasir that his late friends wife has sent a letter to him proposing marriage for one of her two daughters and Nasir is to leave the next day for Murree. Nasir is reluctant but has no choice so he agrees, however he is given the choice to choose between the two daughters. Nasir takes his friend Shahid along and on the way switches roles with him, thereby exempting himself from marriage.

Upon reaching their destination Shahid and Nasir become acquainted with the family and Shahid, acting as Nasir, immediately takes a liking to Dolly. Nasir(acting as Shahid), however, takes a liking to Najma and the two fall in love.

After a while the Shahid and Nasir are told that Seth Khan Bahadur has been invited to Murree to finalize the marriage. Nasir, the real Nasir, comes to the house disguised as his father and successfully fools everyone. However, his real father arrives and exposes the whole truth.

After his father has told him off, Nasir tells him of Najma and how the two are in love and she is the perfect person to take as a wife rather than Dolly or Seema. His father, seeing how serious he is, joyfully agrees. However, when his father talks to Najmas aunt, she reacts strangely and says that Najma is not the innocent girl she seems to be. She then calls the old peasant, who Najma left Seemas baby with, into the room carrying Seemas baby. Najmas aunt asks her to put the baby in his mothers arms. She then puts the baby in Najmas arms, and Najma, bound by her promise to Seema, says nothing. Nasir is shocked by this ill news and refuses to accept it. He desperately tries to persuade Najma to deny the accusation. Najma, sworn to secrecy tells Nasir to forget her. Nasir is crushed and leaves in anger.

Nasir, returned to Karachi, is overcome with depression and resorts to drinking to forget his sadness. His father worries for him and asks him to marry Seema. In Murree, Seema says no to the marriage but is forced into it by her mother. She ventures to find Najma, who now lives with the old peasant and her nephew. She asks Najma to break her promise, but Najma stands by it and tells her to marry Nasir as it is for the best. Finally Seema agrees.

Meanwhile, in Murree, the nephew of the old peasant tries to force Najma to marry him and in the struggle she apparently kills him. Then she runs away to Karachi with the baby and arrives at Nasirs house.

Nasir is infuriated at her arrival and is repulsed because she brought the baby as well. Seema however persuades Nasir to give Najma shelter in their house. This allows her to be close to her baby as well.

Meanwhile, Seemas long gone lover, Sohail returns only to find her married to Nasir. He tries to meet with her but Najma stops him just in time and pleads to him to go back, but doesnt tell him what has happened as she is bound by her promise. Nasir thinks that Sohail is Najmas lover when he sees them talking and falls into another misunderstanding.  

Sohail however, keeps writing letters to Seema which the household servant gladly takes to Seema every time, hiding them from Nasir. One day however, he is caught red handed. But Nasir is unaware that the letters were meant for Seema and not Najma. He confronts Sohail at his house and questions his relationship with Najma. When Sohail reveals the truth, he is awe struck.

He confronts Najma and asks her what gave her the right to toy with his emotions and weave this web of lies. He also reveals to Seemas mother who has arrived that Najma had hidden the truth all along and the baby is Seemas. This way she kept away shame from their household. Seemas mother expresses her gratitude to Najma, but tells her to leave because in her midst they would always live in shame. Najma, now dejected by Nasir and completely helpless accepts this.

When she leaves, she is found by the old peasants nephew who hadnt died after all. Their chase proceeds to a railway track above a river and both of them fall over. When this news reaches Nasir he is devastated. Meanwhile amidst all the depression, Seema takes poison and dies as well.

Nasir and his family, along with Shahid, travels to Murree where Nasir drowns in depression and becomes an alcoholic. He spends his time in the valleys of Murree where he hallucinates that Najma is present among the trees. One day when Shahid is out on a stroll he sees Najma on crutches, it turns out that the fall from the rail track didnt kill her, but broke her legs. Shahid persuades her to come home with him and marry Nasir who is depressed all the time and also tells her about Seemas death.

Both of them proceed to Nasirs home, only to find him missing. His father and Seemas mother welcome her with open arms, but tell her that Nasir has left the house without telling anyone and in his condition, he might do something desperate.

Najma rushes to find him trying to commit suicide on the cliffs. She sings to him and asks him to come back. Nasir sees her and the two embrace each other.

== Music ==
The brilliant soundtrack composed by Sohail Rana contributed enormously to the Armaans success and is considered to be his magnum opus. The soundtrack is perhaps the most memorable in the history of Pakistani cinema and many of the songs are cited as classics in film music. The soundtrack features the voices of Ahmed Rushdi, Mala (Pakistani singer)|Mala, Naseema Shaheen and Khursheed Nurali (Sheerazi). Sohail Rana won the prestigious Nigar Award for Best Musician in 1966 for composing the soundtrack and Ahmed Rushdi won the Nigar Award for Best Male Singer in 1966 for singing "Akele Na Jaana".

=== Songs ===
#"Akele Na Jaana" (Ahmed Rushdi)
#"Akele Na Jaana" (Mala (Pakistani singer)|Mala) 
#"Ko Ko Koreena" (Ahmed Rushdi)
#"Zindagi Apni Thi Ab Tak" (Khursheed Nurali (Sheerazi) and Ahmed Rushdi)
#"Betaab Ho Udher Tum" (Ahmed Rushdi)
#"Jab Pyar Mein Do Dil" (Ahmed Rushdi)
#"Meri Qismat Bata" (Mala (Pakistani singer)|Mala)
#"Oont Pey Betha Mera Munna" (Naseema Shaheen)

Armaans soundtrack has remained hugely popular amongst Pakistani citizens since its release. The songs, "Akele Na Jaana" and "Ko Ko Koreena" have gained cult status, so much so that they still continue to be favourites almost 50 years after their release.

== Reception ==
Armaan broke several records at the Pakistani Box Office. It became the first "Platinum Jubilee" in the history of Pakistani Cinema and ran for 76 weeks. In addition to its commercial success, Armaan won several awards at the Nigar Award ceremony in 1966 including Best Film. 
=== Remake === Armaan was remade in 2013 starring Fawad Khan and Aamina Sheikh.

== Nigar Awards ==
In total, the film won 6 Nigar awards:

{| class="wikitable"
|-
! Award
! Awardee
|-
| Best Movie for the year 1966
| Producer: Waheed Murad
|-
| Best Director
| Pervaiz Malik
|-
| Best Actress
| Zeba
|-
| Best Musician
| Sohail Rana
|-
| Best playback singer
| Ahmed Rushdi (for the song: "Akele na jana")
|-
| Best comedian
| Nirala
|}

==References==
 

== External links ==
*  

 

 
 
 
 
 