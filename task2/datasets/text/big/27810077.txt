Shōwa Erotic Romance: The Virgin's Bashfulness
{{Infobox film
| name = Shōwa Erotic Romance: The Virgins Bashfulness
| image = Shōwa Erotic Romance - The Virgins Bashfulness.jpg
| image_size = 
| caption = Theatrical poster for Shōwa Erotic Romance: The Virgins Bashfulness (2006)
| director = Yutaka Ikejima 
| producer = 
| writer = Kyōko Godai
| narrator = 
| starring = Itsuka Harusaki Kozue Ikeda Yuria Hidaka Atsushi Tsuda Seiji Nakamitsu
| music = Ichimi Ōba
| cinematography = Shōji Shimizu
| editing = Shōji Sakai
| studio = Cement Match
| distributor = OP Eiga
| released = April 8, 2006
| runtime = 61 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 2006 Japanese pink film directed by Yutaka Ikejima and written by his wife and frequent collaborator, Kyōko Godai. It won the award for Fifth Best Film at the Pink Grand Prix ceremony, and Itsuka Harusaki was given a Best New Actress award for her performance in the film. 

==Synopsis==
The story follows the lives of the Kazama family in Tokyo in the 30th year of the Shōwa period: 1955. The father, Ichirō, is a salaryman. His wife, Eiko, is a housewife. The daughter, Akiko, works as a clerk in a factory, and Shigeru, the son, is a college student.   

==Cast==
* Itsuka Harusaki ( ) as Akiko Kazama 
* Kozue Ikeda ( ) as Yuriko Matsuda
* Yuria Hidaka ( ) as Ruiko Sakurazawa
* Atsushi Tsuda ( ) as Shigeru Kazama
* Seiji Nakamitsu ( ) as Ichirō Kazama
* Naohiro Hirakawa ( ) as Satoshi Tanaka
* Anzu Yoshihara ( ) as Eiko Kazama
* Kazu Itsuki ( ) as Tsuchitani
* Kanae Ohara ( ) as Mitsue
* Takayuki Mogi ( ) as Endō
* Yutaka Ikejima as Matsuda

==Availability==
  on April 8, 2006.    It became available online on March 5, 2008 through the Hokuto Corporations online service, DMM. 

==References==

===Bibliography===
*  
*  

==External links==
*   at www.okura-movie.co.jp (Official site)

===Notes===
 

 
 
 
 
 

 
 

 
 
 
 
 
 
 
 


 
 