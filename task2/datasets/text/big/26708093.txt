Pillaiyar Theru Kadaisi Veedu
{{Infobox film
| name = Pillaiyar Theru Kadaisi Veedu
| image =
| caption =
| director = Thirumalai Kishore
| producer =R. B. Choudary
| writer = Thirumalai Kishore Nagarajan Suhasini Jayaprakash Soori Chakri
| cinematography = M. V. Panneerselvam
| editing = V. Jaisankar
| studio = Super Good Films
| distributor =
| released =  
| runtime =
| country = India
| language = Tamil
| budget =
| gross =
}} romantic comedy Suhasini play Chakri in his debut Tamil film.  

==Plot==

Ganesan (Ramesh) is a happy-go-lucky youth in a village. A great admirer of actor-director Vijaya T Rajendar, Ganesan spends all his time with friends (Parotta Suri and a few others). This is ridiculed by his father (Jayaprakash).

Ganesans life takes a turn when he comes across Sandhya (Sanchitha Padukone). She is a friend of Ganesans sister who comes on a vacation to the village. He admires her beauty and falls flat for her. When Ganesan gathers guts to open his heart to Sandhya, enters Valli (Suhasini), Ganesans uncles (Ilavarasu) daughter.
 
Fearing that her father will get her married to a local goon (Bose Venkat), she decides to marry Ganesan. Suddenly Sandhya falls ill and Ganesan takes her to hospital. He is shocked when the doctor (Prakash Raj) informs that she has final-stage pancreatic cancer, and that she will die in a few months.
 
Ganesan, in a bid to keep her happy till her death, marries her and comes home. As a result, he incurs the wrath of his father and is driven out of the house. However, a sudden twist in the plot causes the movie to end on an emotional note.

==Cast==
* Githan Ramesh as Ganesh
* Sanchita Padukone as Sandhya Suhasini as Valli
* Jayaprakash
* Prakash Raj Soori
* Ilavarasu
* Swaminathan
* Bose Venkat as Durai (a local goon)
* Ahila as Gowri

==Critical reception==
Behindwoods wrote: "Overall PTKV is a movie that was intended to be simple and straight; about family, love and sentiments".  Kollywood Today wrote: "The film is dominated by ridiculous factors which becomes spoiler".  Hindu wrote: "Pillaiyaar Theru Kadaisi Veedu only gives a sense of déjà vu. But the final sequences are suspenseful   comes a twist to the tale. And the sudden shift to serious mode catches you unawares. Writer-director Thirumalai Kishore, a first-timer, scores with a story thats strong and narration thats reasonably intelligent. 

==References==
 

==External links==
*  

 
 
 
 