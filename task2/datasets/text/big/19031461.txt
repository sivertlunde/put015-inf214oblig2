Children of Jerusalem: Yehuda
 
 
 
{{Infobox film
| name           = Children of Jerusalem: Yehuda
| image          = Child-of-Jerusalem_Yehuda.gif
| director       = Beverly Shaffer
| starring       = Yehuda
| released       = 1994
| runtime        = 27 min. Hebrew
}} 1994 film, documentary series that shows the holy city of Jerusalem from the distinctive perception of the municipality’s children, who hail from various cultural, economic, social and religious backgrounds.

==Summary==

Children of Jerusalem: Yehuda, focuses on the country of Israel and its capital city from the point of view of a pre-teen Hasidic boy as he gets ready to observe the religious holiday of Sukkot.

At one point in the film the ten-year-old boy explains that his name ‘Yehuda’ stands for the lion.  The young boy sees the lion as unique for the reason that it is powerful and not fearful of anything.

The section of Jerusalem that Yehuda resides in is serene, but only quick stroll from noisy activity of the metropolis.  His household is composed of his father, mother and ten siblings. With a cherubic-like face, this youth has a strong yearning to be a moral Jew and Yehuda has a sincere admiration towards his beliefs and complete, unconditional confidence in his religion. Shadowing the youngster in the course of his everyday life and taking note of the various stories he tells, the documentary uncovers a pre-pubescent existence focused completely on faith, portraying Yehuda as an example of Jerusalem’s substantial ultra-Orthodox society.

It’s obvious that after his religious convictions, his family unit and cultural customs have an important place in Yehuda’s world.  While his younger brother rests alongside him, the main character browses from the beginning to the end of an old photograph album.  It contains the pictures of his forefathers that arrived form Poland and Russia.  When looking at the photos the young title character speaks of his ancestors, as if met each one individually, when in reality they all have been deceased for many decades before his birth.

The predicament that Yehuda encounters are at times strictly connected to his religious beliefs, but in some instances they are common issues that many preteens face.  Only a small proportion of children have to be anxious regarding being teased for possessing elongated peyot  , but universally many children can associate with the frustration of discovering how to ride a bike without training wheels.

Every feature that uses a child as its main subject has the danger of portraying the subject very carefully.  However Children of Jerusalem sustains a neutral space between its focal point, enabling the observer glances into the severe governmental implications of a guiltless kid’s perception.

== See also ==

* 
*History of the Jews in Poland
*History of the Jews in Russia
*Hasidic Judaism

==References==

*{{cite web
  | title = Children of Jerusalem Series -Yehuda - NFB - Collection
  | publisher = National Film Board of Canada
  | date = September 1996
  | url = http://www.nfb.ca/collection/films/fiche/index.php?id=32444
  | accessdate = August 25 }}

==External links==
*  
* Watch   at the National Film Board of Canada

 
 
 
 
 
 
 
 
 
 