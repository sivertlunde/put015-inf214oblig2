Pambaram
{{Infobox film
| name           = Pambaram
| image          =
| caption        = Baby
| producer       = TK Balachandran
| writer         = Sathyavathi T. K. Balachandran (dialogues)
| screenplay     = T. K. Balachandran Shubha Prathapachandran
| music          = A. T. Ummer
| cinematography = KB Dayalan
| editing        = K Narayanan
| studio         = Teakebees
| distributor    = Teakebees
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film, Baby and Shubha and Prathapachandran in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Prem Nazir
*Kaviyoor Ponnamma Shubha
*Prathapachandran
*Philomina
*Poojappura Ravi Bhavani

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Chirayinkeezhu Ramakrishnan Nair. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Amme abhayam tharu || K. J. Yesudas || Chirayinkeezhu Ramakrishnan Nair || 
|-
| 2 || Hemanthayaaminee || K. J. Yesudas || Chirayinkeezhu Ramakrishnan Nair || 
|-
| 3 || Shaarikappaithalin Kadhaparayaam || S Janaki || Chirayinkeezhu Ramakrishnan Nair || 
|-
| 4 || Varika nee vasanthame || S Janaki, Jolly Abraham || Chirayinkeezhu Ramakrishnan Nair || 
|}

==References==
 

==External links==
*  

 
 
 

 