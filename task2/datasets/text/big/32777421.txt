The Stowaway (1958 film)
 
{{Infobox film
| name           = The Stowaway
| image          = The Stowaway.jpg
| image size     = 
| caption        =  Lee Robinson Ralph Habib Lee Robinson Paul-Edmond Decharme Lee Robinson Joy Cavill
| based on       = novel Le Passager Clandestin by Georges Simenon 
| starring       = Martine Carol Roger Livesey Arletty
| music          = 
| cinematography = Desmond Dickinson
| editing        = Stanley Moore Monique Kirsanoff
| distributor    = 
| studio  = Southern International Productions(Australia) Discifilm/Silverfilm (France)
| released       = 22 March 1958 (France) December 1958 (Australia)
| runtime        = 93 minutes
| country        = Australia
| language       = English French
| budget         = ₤250,000  gross = 1,776,374 admissions (France) 
}}
 Lee Robinson and French Lebanese director Ralph Habib. It was shot on location in Tahiti. 

There are French and English versions of this French Australian film. The French version is known as Le Passager clandestin.

==Synopsis==
A group of adventurers compete with one another to find the missing heir Rene Marechal, thought to be near Tahiti. Major Owens, a middle aged Englishman with a shady past, discovers the island on which Marechal lives but is murdered by the criminal Mougins.

Mougins sets out for the island with Colette, a night club singer who is Marechals former mistress. She is rescued by Jean, who had earlier helped Colette stow away on the boat to Tahiti. Jean and Mougins fight and Mougins falls overboard and is eaten by a shark. Jean and Collette decide to give up the search for Marechal and live on the islands.

==Cast==
* Martine Carol as Colette
* Roger Livesey as Major Owens
* Arletty as Gabrielle
* Serge Reggiani as Mougins
* Karlheinz Böhm|Carl-Heinz Boehm as Jean
* Reg Lye as Buddington
* James Condon as the purser
* Charley Mauu as Taro
* Yvon Chabana as Max
* Vahinerii Tauhiro as Vahinerii
* Doris Fitton

==Production==
The film was shot towards the end of 1957 in Tahiti and the Society Islands. 

Lee Rafferty and Chips Robinson contributed money towards the production via sales from Walk into Paradise and funds loaned from Herb McIntyre from the superannuation fund of Universal Pictures Australian branch. 
 
Dialogue scenes were filmed twice, in English (by Robinson) and French (by Habib). Robinson claimed he didnt like Habibs style of direction.

 He was a mad home movies crank and would stand by the camera or even ten feet away from it and be shooting the scene that was his first take. I used to wonder how the hell does he know what is going on there. Often he was on an entirely different angle to the camera. He often seemed more concerned about getting a good scene on his little 16 mm camera. Right from the beginning I found I was in a marvellous position as the second follow-up director because I could see everything that was being done and then rack my brains for some little thing that might spice the scene up a bit.  

Noted Sydney theatre actor Doris Fitton had a supporting role.

==Release==
The film was released in France but only received a limited release in Australia. It was not as successful as Walk into Paradise.

==References==
 

==External links==
* 
*  at National Film and Sound Archive
*  at Oz Movies
 

 
 
 
 
 
 
 
 
 
 
 