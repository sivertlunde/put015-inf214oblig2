Fort Algiers
{{Infobox film
| name           = Fort Algiers
| image          = Fort Algiers poster.jpg
| alt            = 
| caption        = Theatreical release poster
| director       = Lesley Selander
| producer       = Joseph N. Ermolieff
| screenplay     = Theodore St. John 	
| story          = Frederick Stephani Leif Erickson Anthony Caruso John Dehner Robert Boon Henry Corden
| music          = Michel Michelet 	
| cinematography = John F. Seitz 
| editing        = 
| studio         = ERCO Productions Edward L. Alperson Productions
| distributor    = United Artists
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Leif Erickson, Anthony Caruso, John Dehner, Robert Boon and Henry Corden. The film was released on July 15, 1953, by United Artists.   

==Plot==
A female secret agent is sent to French North Africa posing as a night club singer to investigate the massacre of a French Foreign Legion outpost.  She discovers a treacherous leader planning an attack on strategic oil fields.

==Cast== 
*Yvonne De Carlo as Yvette
*Carlos Thompson as Jeff
*Raymond Burr as Amir Leif Erickson as Kalmani Anthony Caruso as Chavez
*John Dehner as Major Colle
*Robert Boon as Mueller
*Henry Corden as Yessouf
*Joe Kirk as Luigi Lewis Martin as Colonel Lasalle
*Leonard Penn as Lt. Picard William Phipps as Lt. Gerrier
*Michael Couzzi as Richetti Charles Evans as General Rousseau
*Sandra Bettin as Sandra 
*Robert Warwick as Haroon 

==References==
 

==External links==
*  
 
 
 
 
 
 
 
 
 
 

 