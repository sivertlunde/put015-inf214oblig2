A Cozy Cottage
{{Infobox film
| name           = A Cozy Cottage
| image          = ACozyCottage1963Poster.jpg
| caption        = Film poster
| director       = Tamás Fejér
| producer       =
| writer         = István Csurka
| starring       = Margit Bara
| music          =
| cinematography = István Hildebrand
| editing        = Zoltán Farkas
| distributor    =
| released       =  
| runtime        = 80 minutes
| country        = Hungary
| language       = Hungarian
| budget         =
}}

A Cozy Cottage ( ) is a 1963 Hungarian drama film directed by Tamás Fejér. It was entered into the 1963 Cannes Film Festival.   

==Cast==
* Margit Bara as Panni - Máté felesége
* Miklós Gábor as Palotás
* György Pálos as Máté József
* Éva Schubert as Máthé titkárnõje
* Zoltán Latinovits as János
* Mária Medgyesi as Szekeresné
* Nóra Tábori as Palotásné
* Nándor Tomanek as Szekeres Péter

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 