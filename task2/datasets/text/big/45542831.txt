Lady Maiko
{{Infobox film
| name = Lady Maiko
| image = 
| alt  = 
| caption = 
| film name = 舞妓はレディ
| director = Masayuki Suo
| producer =  
| writer =  Masayuki Suo
| screenplay = 
| story = 
| based on =  
| starring = Mone Kamishiraishi Hiroki Hasegawa Sumiko Fuji
| narrator =  
| music = Yoshikazu Suo
| cinematography = Rokuro Terada
| editing = Junichi Kikuchi
| studio =  
| distributor = Toho
| released =  
| runtime = 134 minutes
| country = Japan
| language = Japanese
| budget = 
| gross =  
}}
  is a 2014 Japanese musical comedy film written and directed by Masayuki Suo, starring Mone Kamishiraishi, Hiroki Hasegawa, and Sumiko Fuji. It screened in competition at the 2014 Shanghai International Film Festival on June 16, 2014.  It was released in Japan on September 13, 2014. 

==Cast==
*Mone Kamishiraishi as Haruko Saigo
*Hiroki Hasegawa as Noritsugu Kyono
*Sumiko Fuji as Chiharu Kojima
*Tomoko Tabata as Momoharu
*Tamiyo Kusakari as Satoharu
*Eri Watanabe as Mameharu
*Naoto Takenaka as Tomio Aoki
*Masahiro Takashima as Yoshio Takai
*Gaku Hamada as Shuhei Nishino
*Ittoku Kishibe as Orikichi Kitano
*Fumiyo Kohinata as Kanpachiro Ichikawa
*Satoshi Tsumabuki as Yuichiro Akagi
*Jurina Matsui as Fukuna
*Tomu Muto as Fukuha

==Reception== Shall We Dance?, which similarly revolved around gently non-conformist characters doing (and enjoying) what they shouldt in rigid Japan." 

Derek Elley of   gave the film 3 and a half stars out of 5, saying, "Kamishiraishi, the 16-year-old newcomer who beat out 800 other aspirants for the lead role, is a diminutive vocal dynamo and a good fit as the country-girl heroine, right down to her native Kagoshima dialect." 

Kwenton Bellette of Twitch Film felt that "  beautiful tourist-baiting scenes of Kyoto and the geisha district are brought to vivid life thanks to the detail-laden environment and costume design although the film contains itself to one tea-house through the majority of its length."    Maggie Lee of Variety (magazine)|Variety wrote: "Craft contributions are aces, the richly costumed and decorated production presenting Kyotos landscaped gardens, seasonal scenery and architecture to most pleasing effect." 

It debuted at number 5 at the Japanese box office on its opening weekend, earning $1 million from 91,800 admissions. 

==Awards==
* 69th Mainichi Film Award for Best Music (Yoshikazu Suo)  Japan Academy Prize for Best Music (Yoshikazu Suo) 

==References==
 

==External links==
*   
* 

 

 
 
 
 
 