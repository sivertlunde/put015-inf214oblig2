Stranger on the Run
{{Infobox film
| name = Stranger on the Run
| image =
| image_size =
| caption =
| director = Don Siegel
| producer = Richard E. Lyons
| writer = Dean Riesner, Reginald Rose
| narrator =
| starring = Henry Fonda Anne Baxter Michael Parks
| music = Leonard Rosenman
| cinematography = Russell Harlan
| editing = Bruce B. Pierce
| distributor =
| released =  
| runtime = 97 minutes
| country = United States
| language = English
| budget =
| gross =
}}
 Western television film that in some countries premiered in cinemas. The first broadcast was on NBC on October 31, 1967. The film stars Henry Fonda, Anne Baxter and Michael Parks, and was directed by Don Siegel.

In the film, former inmate and alcoholic Ben Chamberlain (Fonda) comes to a town enquiring about a woman. When she is found dead, Sheriff McKay (Parks), at the suggestion of his friend Hotchkiss (Dan Duryea), organises a human hunting party, thinking that Chamberlain is the culprit, but in reality, he had no connection with the murder. 

Chamberlain must cross the desert and reach the border. McKay gets there before him. Chamberlain comes across a homesteader (Baxter) and they start to develop a connection, but this is quickly cut short when the posse arrives.

== Cast ==
*Henry Fonda as Ben Chamberlain
*Anne Baxter as Valvera Johnson
*Michael Parks as  Vince McKay
*Dan Duryea as O.E. Hotchkiss
*Sal Mineo as George Blaylock
*Lloyd Bochner as Mr. Gorman Michael Burns as Matt Johnson
*Tom Reese as Leo Weed
*Bernie Hamilton as Dickory
*Zalman King as Larkin
*Madlyn Rhue as Alma Britten
*Walter Burke as Berk
*Rodolfo Acosta as Mercurio George Dunn as Pilney
*Pepe Hern as Manolo
*Kay Scott (unaccredited)

== References ==
 

==External links==
* 

 
 


 