Støv for alle pengene
 
{{Infobox film
| name           = Støv for alle pengene
| image          =
| caption        =
| director       = Poul Bang
| producer       =
| writer         = Bent From Ida From
| narrator       =
| starring       = Søren Elung Jensen
| music          = Sven Gyldmark
| cinematography = Ole Lytken Aage Wiltrup
| editing        = Lars Brydesen
| studio         = Saga Studios
| released       = 13 December 1963
| runtime        = 93 minutes
| country        = Denmark
| language       = Danish
| budget         =
}}

Støv for alle pengene is a 1963 Danish comedy film directed by Poul Bang and starring Søren Elung Jensen.

==Cast==
* Søren Elung Jensen - Hr. Henriksen
* Helle Virkner - Bodil Henriksen
* Jan Priiskorn-Schmidt - Klaus Henriksen
* Dirch Passer - Alf Thomsen
* Hanne Borchsenius - Frk. Monalisa Jacobsen
* Ove Sprogøe - Thorbjørn Hansen
* Bodil Udsen - Rigmor Hansen
* Karl Stegger - Tim Feddersen
* Karen Lykkehus - Fru Feddersen
* Henning Palner - Viggo Svendsen
* Beatrice Palner - Lene Svendsen
* Asbjørn Andersen - Redaktøren
* Paul Hagen - Sælgeren
* Elith Foss - Chresten Christensen
* Ebba Amfeldt - Oda Christensen
* Valsø Holm - Købmanden
* Bent Vejlby - Pilot i sprøjtefly
* Holger Vistisen - Chaufør i mælkebil
* Jørgen Buckhøj - Rundviser på spritfabrikken
* Gyda Hansen - Kassedame
* Gunnar Strømvad
* Lise Thomsen
* Povl Wøldike

==External links==
* 

 
 
 
 
 
 


 
 