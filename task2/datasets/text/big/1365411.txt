My Cousin Rachel (film)
{{Infobox film
| name           = My Cousin Rachel 
| image          = Rachel_moviep.jpg
| image_size     =
| caption        = original film poster
| producer       = Nunnally Johnson
| director       = Henry Koster
| writer         = Nunnally Johnson Daphne Du Maurier (novel) Richard Burton Olivia de Havilland Audrey Dalton
| music          = Franz Waxman 
| cinematography = Joseph LaShelle 
| editing        = Louis R. Loeffler
| studio         = 
| distributor    = Twentieth Century-Fox 
| released       =  
| runtime        = 98 minutes
| language       = English
| country        = United States
| budget         = $1.2 million 
| gross         = $1.3 million (US rentals) 
}}
 Richard Burton, John Sutton. novel by Daphne du Maurier.

==Cast==
* Olivia de Havilland - Rachel Sangalletti Ashley Richard Burton - Philip Ashley
* Audrey Dalton - Louise Kendall
* Ronald Squire - Nicholas Nick Kendall
* George Dolenz - Guido Rainaldi John Sutton - Ambrose Ashley Tudor Owen - Seecombe
* J. M. Kerrigan - Reverend Pascoe
* Margaret Brewster - Mrs. Pascoe
* Alma Lawton - Mary Pascoe
* Ola Lorraine - Pascoe Daughter
* Kathleen Mason - Pascoe Daughter
* Earl Robie - Philip at Age 5
* Argentina Brunetti - Signora
* Mario Siletti - Caretaker

==Awards and nominations== Richard Burton
* Academy Award for Best Art Direction (nomination) - Lyle Wheeler, John DeCuir, Walter M. Scott
* Academy Award for Best Cinematography (nomination)
* Academy Award for Costume Design (nomination) Richard Burton
* Golden Globe Award for Best Actress - Motion Picture Drama -(nomination) - Olivia de Havilland    

==Availability==
A DVD featuring an isolated score as a supplement was released by niche label Twilight Time in September 2011, limited to 3000 units and available exclusively from online retailer ScreenArchives.com.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 