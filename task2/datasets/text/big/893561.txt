1492: Conquest of Paradise
 
 
{{Infobox film
| name           = 1492: Conquest of Paradise
| image          = 1492film.jpg
| caption        = Theatrical release poster
| director       = Ridley Scott 
| producer       = Alain Goldman Ridley Scott
| writer         = Roselyne Bosch
| starring       = Gérard Depardieu Sigourney Weaver Armand Assante Fernando Rey
| music          = Vangelis
| cinematography = Adrian Biddle
| editing        = William M. Anderson Françoise Bonnot Les Healey Armen Minasian Deborah Zeitman
| studio         = Gaumont Film Company Légende Enterprises France 2 Due West CYRK Films
| distributor    = Paramount Pictures   Pathé  
| released       =  
| runtime        = 142 minutes
| country        = United States France Spain United Kingdom
| language       = English
| budget         = $47 million
| gross          = $7,191,399
}} epic 1992 European adventure/drama film directed by Ridley Scott and written by Roselyne Bosch, which tells the fictionalized story of the discovery of the New World by Italian explorer Christopher Columbus (Gérard Depardieu) and the effect this had on the indigenous peoples of the Americas.

The film was released by Paramount to celebrate the 500th anniversary of Columbus voyage. 

==Plot== Queen Isabella I (Sigourney Weaver) to gain the necessary funding. In the beginning, Columbus becomes obsessed in making his trip westwards to Asia, but lacks crew and a ship. The Spanish council also heavily disapproves of it, and are not keen on any act of independent thought. After continuous warnings at the monastery, he becomes involved in a brawl with the monks, ending up lying in the monastery courtyard to pay penance. His eldest son, Diego, one of the monks, looks on disapprovingly. As Columbus continues his penance through a vow of silence, he is approached by a representative of an interested party who wishes to fund the journey. Columbus meets with the queen, who grants him his journey in exchange for his promise to bring back sufficient amounts of riches in gold.
 Santa Maria. During the voyage at night, one of the crewmen notices him navigating by the stars, a skill previously known only to the Moors. Columbus then happily teaches him the secret. Nine weeks go by and still no sign of land. The crew becomes restless and the other captain turns against Columbus. He tries to reinvigorate them, to let them see the dream that he wishes to share. While some of the crewmen were still not convinced, the main sail suddenly catches the wind, which the crewmen see as a small act of Gods willingness. At night, Columbus notices mosquitoes on the deck, indicating that land is not far off. Some days later, Columbus and the crew spot an albatross flying around the ship, before disappearing. Suddenly, out of the mist they see the first sign of land&nbsp;— an island with lush vegetation and sandy beaches, the first discovery of the New World.

They befriend the local natives, who show them gold they have collected. Columbus teaches one of them Spanish so that they are able to communicate. He then informs them that they are to return to Spain momentarily to visit the Queen and bring the word of God. They leave behind a group of crewmen to begin the colonisation of the New World. Columbus receives a high Spanish honour from the Queen and has dinner with the Council. They express disappointment with the small amount of gold he brought back, but the Queen approves of his gifts. On return to the island, however, all the crewmen left behind are found to have been killed. When the tribe is confronted by Columbus and his troops, they tell him that other strangers came and savaged them. Columbus chooses to believe them, but his commanding officer is not convinced. They begin to build the city of La Isabela and eventually manage to hoist the town bell into its tower, symbolising the arrival of Christianity in the New World.

Four years later, the commanding officer cuts the hand off one of the natives, accusing him of lying about the whereabouts of gold. The word of this act of violence spreads throughout the native tribes and they all disappear into the forest. Columbus begins to worry about a potential war arising, with the natives heavily outnumbering them. Upon return to his home, he finds his house ablaze, confirming his unpopularity among a certain faction of the settlers. Soon, the tribes arrive to fight the Spaniards and the island becomes war-torn, with Columbus governorship being reassigned with orders for him to return to Spain. After being informed about the discovery of the mainland by an Italian, he is sentenced to many years in prison, but is bailed out by his sons soon after. When summoned by the Queen about seeing the New World again, he makes a case for her about his dream to see the New World. She agrees to let him take a final voyage, on the conditions he does not go with the brothers of his crew, and that he never return to the colony. The closing scene shows him old, with his youngest son writing down his tales of the New World.

==Cast==
{{columns-list|2|
* Gérard Depardieu as Christopher Columbus
* Armand Assante as Gabriel Sanchez Queen Isabella I
* Loren Dean as Older Ferdinand Columbus
* Ángela Molina as Beatriz Enriquez de Arana
* Fernando Rey as Antonio de Marchena
* Michael Wincott as Adrián de Moxica
* Tchéky Karyo as Martín Alonso Pinzón
* Kevin Dunn as Captain Méndez
* Frank Langella as Luis de Santángel
* Mark Margolis as Francisco de Bobadilla
* Kario Salem as de Arojaz
* Billy L. Sullivan as Younger Ferdinand Columbus
* John Heffernan as Brother Buyl
* Arnold Vosloo as Hernando de Guevara
* Steven Waddington as Bartolomeo Columbus
* Fernando Guillén Cuervo as Giacomo Columbus
* José Luis Ferrer as Alonso de Bolaños
* Bercelio Moya as Utapán
* Juan Diego Botto as Diego Columbus
* Achero Mañas as Ships Boy King Ferdinand V
* Albert Vidal as Hernando de Talavera
* Isabel Prinz as Dueña Jack Taylor as de Vicuña
* Ángela Rosal as Pinzóns Wife
* Silvia Montero as Pinzóns Daughter (uncredited)
}}

==Music==
  Conquest of Portuguese Prime-Minister Portuguese Socialist Party as its anthem since then. 

The theme is also used at the starting line of the   and became a signature piece for World Professional Champion figure skaters Anita Hartshorn and Frank Sweiding.

Despite the films dismal box office intake in the United States, the films score became a successful album worldwide.

==Release and reception==
===Box office===
1492: Conquest of Paradise opened in theatres on 9 October 1992. It was rated PG-13 in the United States because of violence and brutality. The film was not a success, debuting at No. 7, and ultimately grossing far below its $47 million budget. 

===Critical===
Overall, the film received mixed reviews,      with the review aggregator Rotten Tomatoes giving the film a "rotten" 39% rating based on 18 reviews. However, respected film critic Roger Ebert said in his review that the film was satisfactory, stating "Depardieu lends it gravity, the supporting performances are convincing, the locations are realistic, and we are inspired to reflect that it did indeed take a certain nerve to sail off into nowhere just because an orange was round."   by Roger Ebert 

==See also==
 
* , another film about Columbus released in 1992
*Carry On Columbus, a comedy-film about Columbus released in 1992
*The Magic Voyage, an animated film about Christopher Columbus also released in 1992

==References==
 

==External links==
* 
*  at Rotten Tomatoes
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 