The Woman on the Jury
{{infobox film
| name           = The Woman on the Jury
| image          =
| imagesize      =
| caption        =
| director       = Harry O. Hoyt
| producer       = Associated First National 
| based on       =  
| writer         = Mary OHara (scenario) Frank Mayo Lew Cody Bessie Love
| music          =
| cinematography = James Van Trees
| editing        = Leroy Stone Associated First National
| released       =   reels (7,408 feet)
| country        = United States
| language       = Silent film (English intertitles)
}} lost 1924 Associated First National and directed by Harry Hoyt. It is based on a Broadway stage play, The Woman on the Jury, and stars Sylvia Breamer and Bessie Love. The story was refilmed in 1929 as an early talkie under the title The Love Racket starring Dorothy Mackaill.   

==Cast==
*Sylvia Breamer as Betty Brown Frank Mayo as Fred Masters
*Lew Cody as George Montgomery/George Wayne
*Bessie Love as Grace Pierce
*Mary Carr as Mrs. Pierce
*Hobart Bosworth as Judge Davis
*Myrtle Stedman as Marion Masters
*Henry B. Walthall as Prosecuting Attorney Roy Stewart as Defense Attorney
*Jean Hersholt as Juror
*Ford Sterling as Juror
*Arthur Lubin as Juror
*Stanton Heck as Juror
*Fred Warren as Juror
*Edwards Davis as Juror
*Arthur Stuart Hull (as Arthur S. Hull) as Juror
*Kewpie King as Juror
*Leo White as Juror

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 