The Hawaiians (film)
 
{{Infobox film  
|  name          = The Hawaiians
|  image         = Poster of the movie The Hawaiians.jpg
|  caption       = 
|  director      = Tom Gries James R. Webb Mako
|  producer      = Walter Mirisch
|  music         = Henry Mancini
|  editing       = Ralph E. Winters Byron W. Brandt
|  cinematography= Lucien Ballard Philip H. Lathrop
|  studio        = The Mirisch Production Company
|  distributor   = United Artists 
|  released      = 17 June 1970
|  runtime       = 134 min.  English 
|  followed_by   =
| gross = $2.3 million (US/ Canada rentals) 
|}}
 1970 United American historical James R. Webb. The cast included Charlton Heston as Whipple Hoxworth and Geraldine Chaplin. The performance by Tina Chen led to a Golden Globe nomination as best supporting actress.

The film was based on the books later chapters, which covered the arrival of the Chinese and Japanese and the growth of the plantations. The third chapter of the book had been made into a film, Hawaii (1966 film)|Hawaii, in 1966.

==Plot==
The story begins forty years after the events depicted in the original Hawaii (1966 film)|Hawaii, as a new generation of Americans and Asians must deal with a changing island and world; one of them is a sea captain.

Whipple "Whip" Hoxworth (Charlton Heston) returns home to Hawaii to find his grandfather has died and left his fortune to Hoxworths cousin, Micah Hale (Alec McCowen). Hoxworth, the black sheep of his otherwise very conservative and disapproving family, starts a plantation, staffing it with newly arrived Chinese indentured servants Mun Ki (Mako Iwamatsu|Mako) and his second wife/concubine Nyuk Tsin (Tina Chen).

Mun Ki fathers children with Nyuk Tsin, all the while dreaming of returning to China and his first and officially "real" wife. Nyuk Tsin, named "Wu Chows Auntie" (Wu Chow being their first son) to support the traditional fiction that Mun Kis official spouse in China is the real mother of his children, has other ideas.

Whip steals valuable pineapples from French Guiana in the hope that they will grow in Hawaii. He gives the forlorn plants to Wu Chows Auntie, knowing that she has a "green thumb". When she succeeds in nurturing the plants into flourishing, the overjoyed Whip offers to buy her some land as a reward. Over Mun Kis opposition, she accepts. This is the first step in the rise of both Whip and Wu Chows Auntie, as well as of the pineapple industry in Hawaii. 

Meanwhile, Whip marries native Hawaiian Purity (Geraldine Chaplin) and has a son with her. However, because of her inbred royal Hawaiian ancestry, she is mentally fragile. Eventually, her mind gives way, and she can no longer abide to live with Whip. Their son Noel (John Phillip Law) grows to manhood experiencing an uneasy relationship with his father.

When Mun Ki contracts leprosy, Wu Chows Auntie accompanies him to the leper colony on Molokai. Upon Mun Kis death years later, she returns to be reunited and reacquainted with her now-grown, educated, and prospering children. 

A complication arises when Noel falls in love with Wu Chows Aunties only daughter. Neither parent approves of the marriage, but in the end, they grudgingly accept it.

==Reception==
The movie opened to mixed reviews, with many critics feeling it was not as successful as the 1966 movie Hawaii (1966 film)|Hawaii, which was liked by both moviegoers and critics. It made less money than the original.
 her fathers face, without the other qualities of his presence."    He calls Tina Chen "not remarkable", even though she has a "role almost equal to Hestons". 

 . The incessant ebb and flow is intended as a metaphor for the turbulent tides of Hawaiian life. But the real metaphor here is the pineapple, which in the good old gangster days was a synonym for Box office bomb|bomb.  
 Best Supporting Bill Thomas was nominated for an Academy Award for Best Costume Design.

==Home media==
The Hawaiians was released on a home video format (DVD) for the first time on January 28, 2011, as part of the MGM Limited Edition Collection series.

==References==
 

==External links==
* 
* 
* 
* , video on demand from Hulu

 

 

 
 
 
 
 
 
 
 
 
 