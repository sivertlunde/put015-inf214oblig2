Kaatrinile Varum Geetham
{{Infobox film
| name = Kaatrinile Varum Geetham
| image = 
| caption = 
| director = SP. Muthuraman
| producer = S.Bhaskar for Vijayabhaskar Films
| story= 
| dialogues= 
| screenplay=  Muthuraman Kavitha Manjula Vijayakumar
| music = Ilaiyaraaja
| cinematography = 
| editing = 
| released = 1978
| country = India
| language = Tamil
}}
 1978 Tamil Tamil  Indian feature directed by Muthuraman and Kavitha in the lead roles. 

== Cast ==
 Muthuraman
*Kavitha (actress)
*Manjula Vijayakumar

==Soundtrack==
The music for this film was composed by Ilaiyaraaja.
{| class="wikitable"
|-
! # !! Song!! Singers !! Lyrics
|-
| 1
| Chithirai Sevvanam P Jayachandran
| Panju Arunachalam
|-
| 2
| Kandane Engum
| S Janaki
| Panju Arunachalam
|-
| 3
| Kanden Engum (Pathos)
| Vani Jairam
| Panju Arunachalam
|-
| 4
| Kannil Minnum
| Vani Jairam
| Panju Arunachalam
|-
| 5
| Oru Vanavil Pole P Jayachandran, S Janaki
| Panju Arunachalam
|-
|}

==References==
 

==External links==
 

 
 
 
 
 
 


 