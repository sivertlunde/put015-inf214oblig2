Night Fright
 {{Infobox film
| name           = Night Fright
| image_size     =
| image	=	Night Fright FilmPoster.jpeg
| caption        =
| director       = James A. Sullivan
| producer       = Wallace Clyce Jr. (producer)
| writer         = Russ Marker (writer)
| narrator       =
| starring       = See below
| music          = Christopher Trussell
| cinematography = Robert C. Jessup
| editing        = Arthur Sullivan
| distributor    =
| released       = November 1967
| runtime        = 88 minutes 65 minutes (UK)
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Night Fright is a 1968 American science-fiction horror film directed by James A. Sullivan.

In the early 1980s, the film was re-titled in the United Kingdom for VHS release as E.T.N.: The Extraterrestrial Nastie, E.T.N.: The Extraterrestrial Nasty, The Extraterrestrial Nastie and The Extraterrestrial Nasty .

== Plot summary ==
A Texas community is beset by a rash of mysterious killings involving some of the students from the local college. The sheriff investigating the deaths discovers the startling identity of the killer responsible for the murders. A NASA experiment involving cosmic rays has mutated an alligator into an ogre-like form and bullet-proof unstoppable killing machine with a thirst for blood.

== Cast ==
*John Agar as Sheriff Clint Crawford
*Carol Gilley as Nurse Joan Scott
*Ralph Baker Jr. as Chris Jordan
*Dorothy Davis as Judy
*Bill Thurman as Deputy Ben Whitfield
*Roger Ready as Prof. Alan Clayton
*Gary McLain as Wes Blau
*Darlene Drew as Darlene Scott
*Frank Jolly as Rex Bowers
*Bill Holly as Deputy Pat Lance
*Janiz Menshew as Carla
*Russ Marker as Mitch
*Toni Pearce as Betty the Waitress
*Christi Simmons as Annie
*Brenda Venus as Sue
*Byron Lord as Government Man
*Ronnie Weaver as Government Man
*Olivia Pinion as Partygoer
*Nancy Mann as Partygoer
*Lewis Helm as Partygoer
*Jeanie Wilson as Mary Bennett
*Rod Paxton as Buddy Williams
*The Wildcats as Themselves

== Soundtrack ==
The soundtrack was performed by the Houston, Texas garage band The Wildcats. The band also played in another movie scored by Christopher Trussell also filmed in 1967 entitled Fulfillment: Something Worth Remembering.

== External links ==
* 
* 

 
 
 
 
 
 
 


 