Men of the Lightship
 
 
{{Infobox film
| name           = 
| image          = If War Should Come.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Men of the Lightship was included in a re-release as part of the If War Should Come DVD David MacDonald
| producer       = Alberto Cavalcanti
| writer         = David Evans Hugh Gray Alan Hodge
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Richard Addinsell
| cinematography = Frank "Jonah" Jones
| editing        = Stewart McAllister
| studio         = Crown Film Unit
| distributor    = 
| released       = 1940
| runtime        = 24 min, 4 seconds
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 Ministry of lightship by lightships (stationary ships used as lighthouses) as neutral vessels during war. 

The filmmakers attempted to recreate the original incident as realistically as possible; the crew of the lightship is composed of real lightship men rather than professional actors. The realism of the film was praised in press reviews upon its release, and it was considered one of the best British propaganda films of the period. Men of the Lightship was also distributed in America in 1941, in an edited version produced under the supervision of Alfred Hitchcock. 

==Plot== minesweeper to destroy it.

Later, while the crew are gathered on deck, two Luftwaffe aircraft fly overhead. To the mens surprise, the aircraft begin firing at them with machine guns, wounding the captain in the arm. As the attack intensifies, and the aircraft begin to drop bombs, the entire crew manage to escape the vessel on a lifeboat (shipboard)|lifeboat. The efforts of the German bombers eventually result in the lightship sinking.

The escaped crew row during the night in an attempt to reach the shore. Before they can land the boat, the lifeboat capsizes and  overcome by tiredness, the men drown. A new lightship is then stationed in place of the sunken vessel.
 
 

==Cast==
The filmmakers cast real lightship men rather than professional actors. The only identified actor is Bill Blewitt, a fisherman who appeared, as himself, in four documentary films, notably, The Saving of Bill Blewitt (1937), as well as appearing as an actor in four other productions between 1942 and 1945. 
Not all the Crewmwn died as shown in the film. John Sanders of Great Yarmouth was the sole survivor.

==Production==
Men of the Lightship was based on a real incident that happened to the East Dudgeon lightship on 29 January 1940. The  . Retrieved: 17 March 2012.  
 credits note David MacDonald "reconstructed" the film (rather than "directed"). Rattigan 2001, pp. 279–283.  As with other documentary films produced by the Crown Film Unit during the war, this desire for realism led to the use of "real people" in place of professional actors. When the film was originally shot, professional actors had been used for the roles of the lightships crew, but producer Alberto Cavalcanti contacted David MacDonald during the films production, asking him to re-shoot the sections containing professional actors because he thought they were "totally unconvincing", in contrast with the "splendid" performances of the amateurs. Swann 1989, p. 163.  These scenes were then re-filmed with real lightship men. 
 RAF officials. The suggestion was that additional propaganda value could be added to the films narrative if the German aircraft were shot down by RAF aircraft. This idea was rejected by the filmmakers,  although the RAF still assisted with the reconstruction of the attack, lending two Bristol Blenheim bombers. 

==Reception==
Men of the Lightship was highly praised by both the   described it as "the best propaganda film England has put out this war", "worth half a dozen films of the calibre of The Lion Has Wings." Wilson, Betty.   Sydney Morning Herald, 3 October 1940. Retrieved: 9 April 2012. 

When the film was released in Australia in early 1941, it was met with a positive reception. In the Sydney Morning Herald, it was described as a "tensely thrilling and moving document".   The Australian Womens Weekly praised the films realism, stating: "It is difficult to believe you are not watching the real lightship and its crew, so natural is the acting of the cast, so vivid the action scenes." 

The high praise received for the film meant that some cinemas in the United Kingdom listed it as the main feature. Richards and Sheridan 1987, p. 360.  In a survey of propaganda shorts conducted by Mass-Observation in July 1941, Men of the Lightship was one of the best rated titles, receiving only positive responses. MacKay 2002, p. 179. 

==U.S. release== Robert Montgomery.  The modified version of the film was distributed by Twentieth-Century Fox in 1941.  Hitchcock also later oversaw the editing of the film Target for Tonight. On both occasions, he was uncredited. McGilligan 2004, pp. 280–281. 

==DVD release== BFI GPO Film Unit Collection, If War Should Come. The collection of 18 films cover the period 1939–1941.  

==References==
;Notes
 
;Citations
 
;Bibliography
 
* Huntley, John.   North Stratford, New Hampshire: Ayer Publishing, 1972. ISBN 978-0-405-03897-6.
* Leitch, Thomas M. The Encyclopedia of Alfred Hitchcock: From Alfred Hitchcock Presents to Vertigo (Library of Great Filmmakers). New York: Facts On File Inc., 2002. ISBN 0-8160-4386-8.
* MacKay, Robert.   Manchester, UK: Manchester University Press, 2002. ISBN 978-0-7190-5894-3.
* McGilligan, Patrick.   New York: HarperCollins, 2004. ISBN 978-0060988272.
* Rattigan, Neil.   Madison, New Jersey: Fairleigh Dickinson University Press, 2001. ISBN 978-0838638620.
* Richards, Jeffrey and Dorothy Sheridan.   London: Routledge & Kegan Paul, 1987. ISBN 978-0-7102-0878-1.
* Swann, Paul.   Cambridge, UK: Cambridge University Press, 1989. ISBN 978-0-521-33479-2.
 

==External links==
*   at BFI Screenonline
*  

 
 
 
 
 