Broadway (1942 film)
{{Infobox film
| name           = Broadway
| image size     =
| image	=	Broadway FilmPoster.jpeg
| caption        =
| director       = William A. Seiter
| producer       =
| writer         =
| narrator       = Pat OBrien Janet Blair Broderick Crawford
| music          = Charles Previn Frank Skinner
| cinematography = George Barnes
| editing        = Ted J. Kent
| distributor    =
| released       = 1942
| runtime        =
| country        = United States
| language       = English
| budget         =
}}
 Pat OBrien, Janet Blair, Broderick Crawford, Marjorie Rambeau, Anne Gwynne, and S.Z. Sakall.  Raft plays himself, recalling an incident early in his pre-movie career as a dancer.  The movie was directed by William A. Seiter.

It was based on a Broadway show produced by Jed Harris.
 The George Raft Story (1961) featured a different actor (Ray Danton) playing Raft.

==Cast==
*George Raft	 ... 	Himself Pat OBrien	... 	Dan McCorn
*Janet Blair	... 	Billie Moore
*Broderick Crawford	... 	Steve Crandall
*Marjorie Rambeau	... 	Lillian (Lil) Rice
*Anne Gwynne	... 	Pearl
*S. Z. Sakall	... 	Nick
*Edward Brophy	... 	Porky Marie Wilson	... 	Grace
*Gus Schilling	... 	Joe
*Ralf Harolde	... 	Dolph
*Arthur Shields	... 	Pete Dailey
*Iris Adrian	... 	Maisie
*Janet Warren	... 	Ruby (as Elaine Morey)
*Dorothy Moore	... 	Ann
==Production==
The film was an adaptation of a Broadway show which had been filmed in 1929. Raft had wanted to make it at Universal but Jack Warner refused to loan him out so Raft spent eight months on suspension. Eventually Warners relented and Raft made the film. Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 100 
==Reception==
The film was a success with audiences. 
==References==
 
==External links==
* 

 

 
 
 
 
 
 
 
 
 


 