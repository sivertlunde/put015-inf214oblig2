Duel for Gold
 
 
 
{{Infobox film
| name           = Duel for Gold  aka: Huo Bing 
| image          = Duel for Gold DVD coverart.jpeg
| image_size     =
| caption        = Duel for Gold DVD coverart
| director       = Chor Yuen
| producer       = Runme Shaw
| writer         = Ni Kuang
| narrator       =
| starring       =  
| music          =
| cinematography = Cho-Hua Wu
| editing        = Hsing-Lung Chiang
| distributor    = Shaw Brothers
| studio         = Shaw Brothers Studio
| released       =  
| runtime        = 100 minutes
| country        = Hong Kong Mandarin
| budget         =
}}
 1971 Cinema Hong Kong Chin Han, Wang Ping, Tsung Hua and Chen Chun.     The screenplay was written by Ni Kuang. 

==Plot==
 
Six swordsmen, including two sisters and their husbands, a ranger and a security man, are after millions of taels of gold stored in a secure vault in Datong Prefecture, and betray and outwit each other during their quest. 
 Ping Wang), arrive in a town known to be rich in silver and gold. They do street performances and dangerous sword stunts until Yieh is apparently injured. In the emergency they are taken into the nearby treasury house. Chief treasury guard Wen (Richard Chan Chun) determines her injuries are fake and a fight ensues.

==Cast==
  Chin Han as Master Shen / Meng Lung
* Ivy Ling Po as Meng Yu Yen
* Lieh Lo as Lone Shadow Teng Chi Yan Ping Wang as Meng Yu Ying
* Richard Chan Chun as Wen Li Hsien / Wen San Hu
* Mei Sheng Fan as Steward Chiu
* Siu Loi Chow as Chius cohort
* Hua Chung as Hua Tieh Erh
* Sai Gwa-Pau as Waiter
* Chun Erh as Treasury Security Guard
* Wei Lieh Lan as Treasury Security Guard
* Hsiao Chung Li as Treasury Manager
* Peng-Fei Li as Treasury Sargeant Chang
* Han Lo as Treasury Proprietor
* Fan Mei-Sheng as Qiu Fu
* Lee Pang-Fei as Sergeant Chang
* Chung Wa as Hua Dieh Er
* Tong Tin-Hei as Steward Chius partner
* Yeung Chak-Lam as Steward Chius partner
* Wong Wai as Steward Chius partner
* Lee Tin-Ying as Steward Chius partner
* Law Hon as Bank boss
* Lee Siu-Chung as Bank manager
* Lau Kwan as Bank clerk
* Little Unicorn as Bank Security Guard
* Simon Chui Yee-Ang as Bank Security Guard
* Yue Man-Wa as Bank Security Guard
* Yee Kwan as Bank Security Guard
* Lan Wei-Lieh as Bank Security Guard
* Tony Lee Wan-Miu as Constable
* Lam Siu as Constable
* Wu Por as Constable
* Kong Lung as Constable
* Kwan Yan as Constable
* Yue Hong as Waiter
* Woo Wai as Waiter
* Tsang Choh-Lam as Brothel worker
* Kwok Chuk-Hing as Prostitute
* Wong Kung-Miu as Servant
* Chow Siu-Loi as Chou
* Max Lee Chiu-Jun as Bank Security Guard
* Hung Ling-Ling as Prostitute
 

==Release== Mandarin title was Huo bing, its Hong Kong Cantonese title was Fo ping and the working English language title Thieves Fall Out. After release the films world-wide English subtitled version was titled Duel for Gold.

==Reception==
 

==References==
 

==External links==
* 
* 

==References==
 

 
 
 
 
 


 
 