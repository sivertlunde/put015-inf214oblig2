Guess Who's Sleeping in My Bed?
{{Infobox film
| name           = Guess Whos Sleeping in My Bed?
| image          =
| caption        =
| director       = Theodore J. Flicker
| producer       = Mark Carliner
| writer         = Pamela L. Chais
| based on       = Six Weeks in August (play) by Pamela L. Chais Dean Jones Kenneth Mars Susanne Benton Todd Lookinland
| music          = Morton Stevens
| cinematography = John A. Alonzo
| editing        = John Martinelli ABC
| released       =  
| runtime        = 74 minutes
| country        = United States
| awards         = English
}} comedy film ABC on October 31, 1973 as the ABC Movie of the Week. The teleplay was written by Pamela H. Chais based on her play Six Weeks in August.

==Synopsis==
Francine Gregory (Barbara Eden) is a divorced woman whose charming, vagabond, penniless ex-husband George (Dean Jones) brings hilarity and havoc into her life when he moves into her house with his new wife, baby and dog during his annual summer visit.

==Cast==
*Barbara Eden as Francine Gregory Dean Jones as George Gregory
*Kenneth Mars as Mitchell Barnard
*Susanne Benton as Chloe Gregory
*Todd Lookinland as Adam Gregory
*Reta Shaw as Mrs. Guzmando
*Diana Herbert as Delores

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 

 