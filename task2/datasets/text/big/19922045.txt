Double Face
 
{{Infobox film
| name           = Double Face
| image          = Double Face (German poster).jpg 
| caption        = German theatrical poster to Double Face (1969)
| director       = Riccardo Freda
| producer       = Oreste Coltellacci Horst Wendlandt
| writer         = Riccardo Freda Lucio Fulci Paul Hengge Romano Migliorini Gianbattista Mussetto Edgar Wallace
| starring       = Klaus Kinski
| music          = 
| cinematography = Gábor Pogány
| editing        = Anna Amedei Jutta Hering
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = Italy West Germany
| language       = Italian
| budget         = 
}}

Double Face ( ,  ) is a 1969 Italian thriller film directed by Riccardo Freda and starring Klaus Kinski.  Lucio Fulci co-wrote the screenplay.     
It was released in France as "Liz and Helen", and later in a French adult version under the title "Chaleur et jouissance" (translation: "Heat and Pleasure")

==Cast==
* Klaus Kinski – John Alexander
* Christiane Krüger – Christine
* Günther Stoll – Inspector Stevens
* Annabella Incontrera – Liz Sydney Chaplin – Mr. Brown
* Barbara Nelli – Alice Margaret Lee – Helen Alexander
* Alice Arno – (hardcore inserts – French 1976 version)
* Carlo Marcolino – Servant
* Luciano Spadoni – Inspector Gordon
* Ignazio Dolce – (uncredited)
* Bedy Moratti – (uncredited)
* Gastone Pescucci – Peter (uncredited)
* Claudio Trionfi – (uncredited)
* Alfred Vohrer – Edgar Wallace (voice: German version) (uncredited) (archive footage)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 