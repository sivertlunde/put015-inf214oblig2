Roma (1972 film)
 
{{Infobox film
| name = Roma
| image = Roma_moviep.jpg
| image_size = 215px
| alt = 
| caption = Original film poster
| director = Federico Fellini
| producer = Turi Vasile
| screenplay = Federico Fellini Bernardino Zapponi
| story = Federico Fellini Bernardino Zapponi
| starring = Peter Gonzales
| music = Nino Rota Carlo Savina
| cinematography = Giuseppe Rotunno
| editing = Ruggero Mastroianni
| studio = Les Productions Artistes Associés Ultra Film
| distributor = Ital-Noleggio Cinematografico   United Artists
| released =  
| runtime = 128 minutes 119 minutes  
| country = Italy France
| language = Italian German English French Latin Spanish
| budget = 
| gross = Italian lire|ITL869,900,000
}}
 

Roma, also known as Fellinis Roma,  is a 1972 semi-autobiographical, poetic comedy-drama film depicting director Federico Fellinis move from his native Rimini to Rome as a youth. It is formed by a series of loosely connected episodes. The plot is minimal, and the only character to develop significantly is Rome herself. Peter Gonzales plays the young Fellini, and the film features mainly unknowns in the cast.

==Plot==
Federico Fellini recounts his youth in Rome, an extremely crude, corrupt, cruel city, without shame or morals. Memorable is the scene where he along with his friends in their young teens go to a third-class theater to see some simple shows. People do not applaud; instead whistles, burps, fart sounds and angry tirades are hurled against the poor actors who eventually have had enough of their audiences vulgar and unprecedented rudeness, leading them to turn against the public.

==Alberto Sordis performance==
During editing a scene with Alberto Sordi was cut because it was considered too immoral and cruel. In it, he played a rich man sitting at a bar watching some poor kids who are playing ball. A poor man, blind, sick and lame, comes to cross the street, preventing the rich man to contemplate the scene. Alberto Sordi, annoyed, begins shouting insults at the blind man: "Get out of the way, you ugly old man! Get out!".

==Cast==
* Peter Gonzales as Federico Fellini, age 18
* Fiona Florence as Dolores - young prostitute
* Pia De Doses as Princess Domitilla
* Renato Giovannoli as Cardinal Ottaviani

;Uncredited
* Dennis Christopher as The Hippie
* Anna Magnani (her final film role) as herself
* Marcello Mastroianni as himself
* Feodor Chaliapin, Jr. as actor playing Julius Caesar
* Alberto Sordi as himself
* Gore Vidal as himself
* John Francis Lane as himself
* Elliott Murphy as extra
* Federico Fellini as himself
* Cassandra Peterson

==Historical contrasts and modern alienation== wartime Fascist papal audience.

==Narrative devices==
The plot (such as it is) centers on two journeys to Rome by the director. The first is as a young man in the late 1930s and early 1940s. The second is as the director of a film crew creating a movie about Rome. The film alternates these two narratives.

==Release== Best Foreign Language Film at the 45th Academy Awards, but was not accepted as a nominee. 

==See also==
* List of submissions to the 45th Academy Awards for Best Foreign Language Film
* List of Italian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 