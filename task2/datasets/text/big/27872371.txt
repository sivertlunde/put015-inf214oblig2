Hemingway & Gellhorn
{{Infobox film
| name         = Hemingway & Gellhorn
| image        = Hemingway_&_Gellhorn_poster.jpg
| alt            =  
| caption       = Official poster
| director      = Philip Kaufman Barbara Turner Barbara Turner
| starring       = Nicole Kidman Clive Owen
| music          = Javier Navarrete
| cinematography = Rogier Stoffers
| editing        = Walter Murch
| studio         = HBO
| distributor    = 
| released      =   
| runtime       = 154 minutes
| country        = United States
| language     = English
| budget        = $14 million
| gross          = 
}}

Hemingway & Gellhorn is an HBO biopic film about the lives of journalist Martha Gellhorn and her husband, writer Ernest Hemingway. It was directed by Philip Kaufman, and first aired on HBO on May 28, 2012.    

==Plot==
Telling the story of one of America’s most famous literary couples, the movie begins in 1936 when the pair meet for the first time in a Key West bar. They ran into each other again in Spain where they were both covering the Spanish Civil War. They were staying in the same hotel on the same floor, and she resisted his advances. However, during a bombing raid, they found themselves trapped in the same room, and lust overcame them. They became lovers and stayed in Spain until 1939. In 1940 Hemingway divorced his second wife so that they could marry. Over time she became prominent in her own right. In 1945 she asked Hemingway for a divorce. He credits her with inspiring him to write the novel, For Whom the Bell Tolls. 

==Cast==
*Nicole Kidman as Martha Gellhorn
*Clive Owen as Ernest Hemingway
*David Strathairn as John Dos Passos
*Molly Parker as Pauline Pfeiffer
*Parker Posey as Mary Welsh Hemingway
*Rodrigo Santoro as Paco Zarra (based on José Robles)
*Mark Pellegrino as Max Eastman
*Peter Coyote as Maxwell Perkins
*Lars Ulrich as Joris Ivens
*Robert Duvall as General Petrov
*Tony Shalhoub as Mikhail Koltsov
*Leonard Apeltsin as Russian Operative
*Jeffrey Jones as Charles Colebaugh
*Santiago Cabrera as Robert Capa
*Aitor Inarra as Felipe Leon
*Diane Baker as Mrs. Gellhorn
*Steven Wiig as Simo Häyhä
*Keone Young as Mr. Ma Madame Chiang

==Production==
Pat Jackson, the films sound effects editor, said that the biggest challenge in doing sound for the film was "making the archival footage and the live-action footage shot locally appear seamless." 

==Reception== Paste commented "In terms of the acting, there’s little room for complaint. At 45, Kidman remains a fetching and powerful screen presence. Here, she captures Gellhorn’s idealistic, gung-ho leftism without making herself sound overly self-righteous" but was less positive about Clive Owens role as Ernest Hemingway stating "While Owen easily embodies Hemingway’s extraordinary charisma (and certainly his legendary temper), his performance is often undermined by the British actor’s inability to hold his American accent."  Jeremy Heilman of MovieMartyr.com agreed with Rosemans opinions stating "Kidman is strong here as Martha Gellhorn, using her exceptional figure and old-fashioned movie star glamour to full effect" and that Owens performance was "inconsistent, goofy one moment and strongly seductive the next."  Todd McCarthy of TheHollywoodReporter.com said of Kidman "Kidman is terrific in certain scenes and merely very good in others; there are a few too many moments of her traipsing around Spain, blond hair flying glamorously, not knowing quite what she’s doing there. But for the most part, she rivets one’s attention, lifting the entire enterprise by her presence. 

The   observed that "none of the reviews quite prepared me for the unchained malady of Hemingway & Gellhorn." Of the director they say "it’s as if Kaufman answered the call of wild and it turned out to be a loon."    gives the film 50% on the Tomatometer with an audience score of 42%. 

===Accolades===
{| class="wikitable"
|-
! Award
! Category
! Recipient(s)
! Result
|- 17th Satellite Awards Satellite Award Best Miniseries or Television Film
|
| 
|- Satellite Award Best Actor – Miniseries or Television Film Clive Owen
| 
|- Satellite Award Best Actress – Miniseries or Television Film Nicole Kidman
| 
|- 19th Screen Actors Guild Awards Screen Actors Outstanding Performance by a Male Actor in a Miniseries or Television Movie Clive Owen
| 
|- Screen Actors Outstanding Performance by a Female Actor in a Miniseries or Television Movie Nicole Kidman
| 
|- 28th TCA Awards TCA Award Outstanding Movie, Miniseries, or Special
|
| 
|- 64th Primetime Emmy Awards Primetime Emmy Outstanding Miniseries or Movie James Gandolfini, Barbara Turner, Peter Kaufman, Nancy Sanders, Trish Hofmann, and Mark Armstrong
| 
|- Primetime Emmy Outstanding Lead Actor in a Miniseries or Movie Clive Owen
| 
|- Primetime Emmy Outstanding Lead Actress in a Miniseries or Movie Nicole Kidman
| 
|- Primetime Emmy Outstanding Supporting Actor in a Miniseries or Movie David Strathairn
| 
|- Primetime Emmy Outstanding Directing for a Miniseries, Movie, or Dramatic Special Philip Kaufman
| 
|- Outstanding Art Direction for a Miniseries, Movie, or Special Jim Erickson, Nanci Noblett, and Geoffrey Kirkland
| 
|- Outstanding Cinematography for a Miniseries, Movie, or Special
| Rogier Stoffers 
| 
|- Primetime Emmy Outstanding Costumes for a Miniseries, Movie, or Special Ruth Myers
| 
|- Outstanding Hairstyling for a Miniseries, Movie, or Special
| Frances Mathias and Yvette Rivas
| 
|- Outstanding Makeup for a Miniseries or Movie (Non-Prosthetic)
| Kyra Panchenko, Gretchen Davis, and Paul Pattison 
| 
|- Outstanding Music Composition for a Miniseries, Movie, or Special (Original Dramatic Score) Javier Navarrete
| 
|- Outstanding Single-Camera Picture Editing for a Miniseries, Movie, or Special Walter Murch
| 
|- Primetime Emmy Outstanding Sound Editing for a Miniseries, Movie, or Special
| Kim Foscato, Andy Malcolm, Casey Langfelder, Pete Horner, Joanie Diener, Goro Koyama, Steve Boeddeker, Pat Jackson, Douglas Murray, Andrea S. Gard, and Daniel Laurie 
| 
|- Outstanding Sound Mixing for a Miniseries, Movie, or Special
| Douglas Murray, Pete Horner, Lora Hirschberg, and Nelson Stoll 
| 
|- Primetime Emmy Outstanding Supporting Visual Effects in a Miniseries or Movie
| Nathan Abbot, Kip Larsen, Chris Morley, and Chris Paizis
| 
|- 70th Golden Globe Awards Golden Globe Best Actor – Miniseries or Television Film Clive Owen
| 
|- Golden Globe Best Actress – Miniseries or Television Film Nicole Kidman
| 
|- Directors Guild of America Award Outstanding Directing – Television Film Philip Kaufman
| 
|- Writers Guild of America Award Long Form – Original Jerry Stahl Barbara Turner
| 
|}

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 