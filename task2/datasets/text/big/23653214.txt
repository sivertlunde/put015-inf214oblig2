The General in Red Robes
{{Infobox film
| name           = The General in Red Robes
| image          = The_General_in_Red_Robes_poster.jpg
| caption        = 
| director       = Lee Doo-yong 
| producer       = Kwak Jeong-hwan
| writer         = Moon Sang-hun  Lee Doo-yong 
| starring       = Hwang Hae Ko Eun-ah
| music          = Hwang Mun-pyeong
| cinematography = Son Hyeon-chae
| editing        = Ree Kyoung-ja
| distributor    = Hap Dong Films Co., Ltd.
| released       =  
| runtime        = 88 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
| film name      = {{Film name
 | hangul         =  
 | hanja          =  의  
 | rr             = Hongui Janggun
 | mr             = Hong-ŭi Changgun
 | context        = }}
}} 1973 South Korean film directed by Lee Doo-yong. It was awarded Best Film at the Grand Bell Awards ceremony.     
 
==Synopsis== Imjin War, Kwak Jae-Wu leads an army against the Japanese invaders. Once he and his soldiers have helped defeat the Japanese, Kwak refuses a government post as reward from the Royal Court, and chooses instead to live the rest of his life in Bipa Mountain.   
 
==Cast==
* Hwang Hae 
* Ko Eun-ah
* Do Kum-bong
* Lee Kang-jo
* Kim Young-in
* An Gil-won
* Han Tae-il
* Yu Il-su
* Kim Mu-yeong
* Cheon Bong-hak

==Bibliography==
===English===
*   
*  
*  
===Korean===
*  
*  

==Notes==
 

 
 
 
 
 
 

 
 
 
 
 
 
 