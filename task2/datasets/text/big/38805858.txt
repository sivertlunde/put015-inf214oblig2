Gintama: The Movie: The Final Chapter: Be Forever Yorozuya
{{Infobox film
| name                 = Gintama: The Movie: The Final Chapter: Be Forever Yorozuya
| image                = GintamaFinalChapterposter.jpg
| caption              = Theatrical release poster
| director             = Yoichi Fujita
| producer             = {{plainlist|
* Susumu Hieda
* Yasuyuki Ban
* Hiromitsu Higuchi
}}
| screenplay           = Akatsuki Yamatoya
| story                = Hideaki Sorachi
| based on             =  
| starring             = {{plainlist|
* Tomokazu Sugita
* Daisuke Sakaguchi
* Rie Kugimiya
* Susumu Chiba
* Kazuya Nakai
* Kenichi Suzumura
* Akira Ishida
* Kōichi Yamadera
}}
| music                = Audio Highs
| cinematography       = Ei Rouhei
| editing              = Takeshi Seyama
| production companies = {{plainlist|
* Shueisha
* Aniplex
* TV Tokyo Sunrise
* Dentsu
* Bandai Warner Bros. Japan
}}
| distributor    =  Warner Bros. Japan
| released       =  
| runtime        = 110 minutes
| country        = Japan
| language       = Japanese
| gross          = United States dollar|$16.6 million  
}} animated film Sunrise based on the Gin Tama manga and anime series. It was directed by the director from the anime series Yoichi Fujita and based on a story by Hideaki Sorachi, Gin Tama s original author.  It stars Tomokazu Sugita, Daisuke Sakaguchi, Rie Kugimiya among others. The film focuses on the freelancer samurai Gintoki Sakata in a time travelling story where he encounters older personas of the people he met in Edo.

The Final Chapter was first announced August 2012 although major details were not released until early 2013. Although the film has been marketed as "Final Chapter" Sorachi and Fujita did not confirm it was the last anime production from Gin Tama; the former wrote the story with the concept of the series ending. Two themes were provided by the bands SPYAIR and Tommy heavenly6, with latters song having already been used in the television series.

==Plot== Shinpachi Shimura, who has turned into a cool samurai with no trace of tsukkomi, and List of Gin Tama characters#Kagura|Kagura, who has changed into a beautiful woman with no Chinese speech pattern. The futures Gintoki is assumed to have died as a result of the "White Curse." Kagura and Shinpachi have been trying to deal with their leaders disappearance and Gintoki, disguised thanks to the Time Thief, tries to help them overcome their sadness.
 Isao Kondo, Kotaro Katsura Gengai Hiraga. Sogo Okita, Toshiro Hijikata Elizabeth who have joined forces to form a new group opposing the bakufu while Gengai is revealed to be an imposter. Interacting with the new group Gintoki learns the White Curse was started by a group of sorcerers known as   who Gintokis Joi faction fought in a previous war. Gintoki was infected with a virus which had undergone an incubation period and the futures Gintoki disappeared while fighting it. One victim of the White Curse is Shinpachis sister, List of Gin Tama characters#Tae Shimura|Tae, who is close to her death. In order to cure her, Gintokis friends go to search for the Emmi. Gintoki finds and kills the Emmi who is revealed to be the futures Gintoki who set this series of events off to be killed by his past self.
 Taizo Hasegawa in disguise. It is revealed that Tama is the Time Thief who was used by the people from the future to stop Gintoki from erasing his existence and help him defeat the Emmi before the curse starts. Together with his friends and the last generation of the old Jouishi (Katsura, Takasugi, Sakamoto, and the white demon otherwise known as the Gintoki from the past) fight against the Emmi. Finally, Gintoki and the white demon defeat the Emmi, stopping them from starting the curse. Gintoki reunites with the Yorozuya and then him and his friends return to their own timeline, all promising to meet again. The ending scene consists of the original members of the Joi watching from a cliff.

==Cast==
The cast from the TV series returned to voice the characters with:
*Tomokazu Sugita as Gintoki Sakata. A freelancer samurai travelling across time to find the source of the virus. Sugita expressed satisfaction with the script.   
*Daisuke Sakaguchi as Shinpachi Shimura. Gintokis apprentice of samurai who has grown into a skilled warrior in the future. Sakaguchi found it felt like the story fitted the series. Due to Shinpachis portrayal as a stronger character, Sakaguchi commented it was difficult voicing him for the movie. 
*Rie Kugimiya as Kagura. A young girl who grew into a quiet skilled fighter in the future. Kugimiya shared similar feelings about her work as the older Kagura based on her calm dialogue despite sharing several traits from her common persona. 
*Mikako Takahashi as Sadaharu / Otsuu-chan
*Susumu Chiba as Isao Kondo
*Kazuya Nakai as Toshiro Hijikata
*Kenichi Suzumura as Sogo Okita
*Tetsuharu Ōta as Sagaru Yamazaki
*Akira Ishida as Kotaro Katsura
*Satsuki Yukino as Tae Shimura
*Fumiko Orikasa as Kyubei Yagyu
*Yū Kobayashi as Ayame "Sacchan" Sarutobi
*Yūko Kaida as Tsukuyo
*Kujira as Otose
*Omi Minami as Tama
*Yū Sugimoto as Catherine
*Fumihiko Tachiki as Taizo Hasegawa
*Bin Shimada as Gengai Hiraga
*Kōji Yusa as Ayumu Tojo
*Kōichi Sakaguchi as Musashi-Like Man
*Takehito Koyasu as Shinsuke Takasugi
*Shinichirō Miki as Tatsuma Sakamoto
*Akira Kamiya as Future Elizabeth
*Tsutomu Isobe as Enmi
*Kōichi Yamadera as Time Bandit

==Production==
The movie was announced in August 2012 in Shueishas Weekly Shonen Jump magazine where the manga was published. The announcement came alongside the confirmation the story was being written by Hideaki Sorachi.  The television series used to show part of is logo until revealing the full title during March 2013.   Although it was titled "Final Chapter", Sorachi said the manga series was not ending and he believed it could refer to the film being Sunrises last production based on the series. Having been told by Sunrise about the films title and material, Sorachi wrote a story that could be considered the end of the series.  Additionally, director Yoichi Fujita commented they would make a continuation if it became a hit. 

===Music===
The film uses two musical themes: the insert theme   by SPYAIR and the ending theme "Pray" by Tommy heavenly6. While "Pray" was previously used as the series first opening theme, "Genjō Destruction" was composed for this movie and its single was released on July 3, 2013.  SPYAIR had previously contributed with two other themes for the TV series. SPYAIR read the script of the movie and later had a talk with Fujita in order to get an idea for "Genjō Destruction." They learned the theme was going to be used in a fight scene and thus worked to compose a high quality song in order to make it match with the movie.    A twenty-three theme CD soundtrack was released on July 3, 2013 by Aniplex. 

==Release==
In December 2012 at Jump Festa the cast from the Gintama anime series commented they were not sure when the film was going to be released due to delays from the script.  The delay to Summer 2013 was confirmed in January 2013 in an episode from the TV series.  An anime event titled "Soul of Silver" was made to promote the movie in Osaka. The DVD featuring videos and interviews from event was released on October 23, 2013.  A novelization of the film by Ohsaki Tomohito was released by Shueisha on July 8, 2013. 

The film premiered in theaters on July 6, 2013.  A preopening was made at Ryogoku Kokugikan on June 29, 2013 with a fan event featuring appearances by the voice actors from the series.  Movie goers can buy special flavored popcorn representing the characters of Gintoki Sakata, Kagura and Toshiro Hijikata.  Additionally, all of them are given notepads with "Volume 0-style" cover made by Hideaki Sorachi. 

===Box office and sales===
The movie debuted in the Japanese box office fourth earning ¥281,776,256 (US$2,821,707) on 127 screens.  The film grossed US$16.6 million in Japan and was the 8th highest-grossing anime film in Japan in 2013.  Oricon reported in August 2013 that it sold over one million tickets surpassing the ticket sales from the previous Gin Tama film.  It was released in DVD and Blu-ray format on December 18, 2013 by Aniplex. The two are available in both regular and limited editions, the latter including a bonus extra CD.  A week after its release, the Blu-ray sold 38,783 units in Japan,  while in mid January 2014 it reached a total of 44,778 units sold. 

==References==
 

==External links==
*    
*  

 

 
 
 
 
 
 
 
 