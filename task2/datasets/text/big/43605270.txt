Ennai Pol Oruvan
{{Infobox film
| name           = Ennai Pol Oruvan
| image          =
| image_size     =
| caption        =
| director       = T. R. Ramana A. C. Tirulokchandar
| producer       =
| writer         =
| screenplay     = Sharada Ushanandini Chittor V. Nagaiah
| music          = M. S. Viswanathan
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Tamil
}}
 1978 Cinema Indian Tamil Tamil film,  directed by T. R. Ramana and A. C. Tirulokchandar. The film stars Sivaji Ganesan, Sharada (actress)|Sharada, Ushanandini and Chittor V. Nagaiah in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast==
*Sivaji Ganesan Sharada
*Ushanandini
*Chittor V. Nagaiah

==Soundtrack==
The music was composed by M. S. Viswanathan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Susheela || Vaali || 03:05
|-
| 2 || Thangangale Naalai Thalai || T. M. Soundararajan || Kannadasan || 03:04
|-
| 3 || Mouna Kalaigiradhu || T. M. Soundararajan || Kannadasan || 02:58
|-
| 4 || Aanattam Pennattam || T. M. Soundararajan, L. R. Eswari || Kannadasan || 03:35
|-
| 5 || Izhu Izhu ||  || Kannadasan || 03:36
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 