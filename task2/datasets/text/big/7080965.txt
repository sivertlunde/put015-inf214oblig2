Bagavathi
{{Infobox film
| name = Bagavathi
| image = Bagavathi Vijay Movie.jpg Vijay Reemma Jai Vadivelu
| director = A. Venkatesh (director)|A. Venkatesh
| writer = A. Venkatesh (director)|A. Venkatesh Pattukkottai Prabakar   (Dialogues ) 
| producer = K. Muralidharan V. Swaminathan G. Venugopal
| released =  
| runtime = 164 minutes
| language = Tamil Deva Srikanth Deva (1 song)
| editing = V. T. Vijayan
| cinematography = R. Rathnavelu
| studio = Lakshmi Movie Makers
| country = India Australia
}}
 Tamil film Vijay and Deva composed the music for the film. Vijay introduced himself as an action hero through this film with firing punch dialogues and crackling style as mass hero.The film opened to positive reviews and it was declared a hit at the box office.

==Plot==
Bagavathi (Vijay (actor)|Vijay), owns a tea shop cum hotel near Chennai High court. He lives with the aim of bringing up his younger brother Guna (Jai (actor)|Jai) as a doctor. He also befriends a modern girl Anjali (Reemma Sen) who is a daughter of a Judge. Guna has a girl friend Priya with whom he accidentally develops a physical relationship. Gunas love is objected by Priyas father Easwarapandiyan (Ashish Vidyarthi). Bagavathi to fulfill his brothers wish tries to convince Priyas father. But in vain. Guna attempts to marry Priya even without the knowledge of his brother. But Priyas father collapses the marriage and Guna is killed in front of Bagavathi.

Guna on his last moments promises that he would be with his brother forever. After Gunas death, everyone comes to know Priya is pregnant due to the accidental relationship with Guna earlier. Bagavathi reminds of his brothers last words and thinks his brother is going to be born again. Meanwhile, to protect the social status, Priyas father attempts to kill the child before it is being born. Bagavathi turns to a don to save his brothers child. Overcoming all the hurdles put by Priyas father, Bagavathi manages to protect Priya for safe birth of his brothers child with the help of Anjali,her mother and his tea shop helper Vadivelu.

==Cast== Vijay as Bhagavathi
*Reemma Sen as Anjali
*Vadivelu as Vadivelu/Vibration Jai as Guna
*Ashish Vidyarthi as Easwarapandiyan Monica as Priya Ponnambalam as Vibuthi Ganesan
*Ilavarasu as Singamuthu
*K. Vishwanath as The Chief Minister
*Yugendran as Anand
*Thalaivasal Vijay as Ganga
*Seema as District Judge (Anjalis Mother)
*Sathyapriya as Priyas Mother
* Vimalraj as Henchman
* Bayilvan Ranganathan as Traffic police
*Singamuthu as Nattamai

==Production==
Vijay was hesitant at first to take up a full mass hero subject and asked the director for 3 months time to think about it. The director Venkatesh selected a newcomer, Jai (actor)|Jai, to play the role of Guna after seeing him at Jais uncle, Deva (music director)|Devas, recording studio - mentioning that the youngster looked like actor Vijay.  Although his role in the film was relatively minimal, he has since gone on to appear in successful films such as Chennai 600028, Subramaniyapuram and Engeyum Eppodhum. 

The film was shot at various locations including Chennai and Vishakapattinam. 

==Release== Telugu was prepared and released in late 2005. 

==Soundtrack==

{{Infobox Album |  
| Name        = Bagavathi
| Type        = soundtrack Deva
| Cover       = 
| Released    = 2002
| Recorded    =  Feature film soundtrack |
| Length      = 
| Label       = Hit Music Deva
| Reviews     = 
}}

The soundtrack consists of six songs composed by Deva. The lyrics were written by Vaali (poet)|Vaali, Snehan, Pa. Vijay, Kalaikumar, Na. Muthukumar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers 
|-
| 1 || Allu Allu || Udit Narayan 
|-
| 2 || Atchamillai Atchamillai || Shankar Mahadevan 
|-
| 3 || July Malargalae || Karthik (singer)|Karthik, Sadhana Sargam 
|-
| 4 || Kai Kai Vaikiraan || Anuradha Sriram, Shankar Mahadevan 
|-
| 5 || Shyo Shyo || Timmy, Sadhana Sargam 
|- Vijay 
|}

==References==
 

 

 
 
 
 