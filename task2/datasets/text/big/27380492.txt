A Name for Evil
{{Infobox Film

 | name = A Name for Evil
 | image_size = 
 | caption = 
 | director = Bernard Girard
 | producer = 
 | writer = Bernard Girard
 | narrator = 
 | starring = Samantha Eggar
 | music = 
 | cinematography = 
 | editing = 
 | studio = 
 | distributor = Penthouse
 | released = 1973
 | runtime = 74 min
 | country = United States
 | language = English
 | budget = 
 | gross = 
 | image= A Name for Evil VideoCover.jpg
}}

A Name for Evil is a 1973 American horror film starring Samantha Eggar and Robert Culp.

==Plot==
Dissatisfied with the family architectural business, a man and his wife pack up and move out to his great-grandfathers old house in the country. While trying to patch it up, the house starts to make it clear to him that it doesnt want him there, but the local church (with some off-kilter practices of their own) seems to take a shine to him...

== Cast ==
*Samantha Eggar as Joanna Blake
*Robert Culp as John Blake
*Sheila Sullivan as Luanna Baxter
*Mike Lane as  Fats
*Sue Hathaway as  Mary
*Ted Greenhalgh as Hugh

==Release==
The film was released theatrically in the United States by Cinerama Releasing Corporation in 1973.

The film was released on VHS in the U.S. by Paragon Video Productions in the 1984.

== References ==
*Horror and science fiction films II by Donald C. Willis

* 
* 

 
 
 
 
 
 
 

 