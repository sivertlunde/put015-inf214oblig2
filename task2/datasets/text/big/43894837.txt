Pan-Americana
{{Infobox film
| name           = Pan-Americana
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = John H. Auer Ruby Rosenberg (assistant)
| producer       = John H. Auer
| writer         = 
| screenplay     = Lawrence Kimble
| story          = Frederick Kohner John H. Auer
| based on       =  
| starring       = Phillip Terry Audrey Long Robert Benchley Eve Arden Ernest Truex Marc Cramer Jane Greer (uncredited)
| narrator       = 
| music          = Constantin Bakaleinikoff
| cinematography = Frank Redman
| editing        = Harry Marker RKO Radio Pictures
| distributor    = 
| released       =   }}
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}

Pan-Americana is a 1945 American romantic comedy film produced and directed by John H. Auer, from a screenplay by Lawrence Kimble, based on a story by Auer and Frederick Kohner. RKO released the film on March 22, 1945, and the picture stars Phillip Terry, Audrey Long, Robert Benchley, Eve Arden, Ernest Truex, Marc Cramer, and Jane Greer (uncredited).

==References==
 

 
 
 
 
 
 


 