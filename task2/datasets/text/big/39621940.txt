Rasputin (1938 film)
{{Infobox film
| name           = Rasputin
| image          = 
| image_size     = 
| caption        = 
| director       = Marcel LHerbier
| producer       = Max Glass Alfred Neumann (novel)   Steve Passeur   Max Glass  Marcel LHerbier 
| narrator       = 
| starring       = Harry Baur   Marcelle Chantal   Pierre Richard-Willm   Jean Worms
| music          =  Darius Milhaud
| editing        = Raymond Leboursier
| cinematography = Philippe Agostini   Michel Kelber 
| studio         = Max Glass Film
| distributor    = Comptoir Français du Film 
| released       =  
| runtime        = 95 minutes
| country        = France
| language       = French
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} French historical film directed by Marcel LHerbier and starring Harry Baur, Marcelle Chantal and Pierre Richard-Willm.  It depicts the rise and fall of the Russian mystic Grigori Yefimovich Rasputin, the advisor to the Romanov royal family. The films sets were designed by the art director Guy de Gastyne.

==Cast== 
* Harry Baur as Rasputin
* Marcelle Chantal as Tsarine Alexandra 
* Pierre Richard-Willm as Comte Igor Kourloff 
* Jean Worms as Tsar Nicholas II 
* Jany Holt as Groussina 
* Jacques Baumer as Prokoff 
* Georges Malkine as Begger 
* Lucien Nat as Ostrowski 
* Carine Nelson as Ania Kitina  Palau as Piotr 
* Georges Prieur as Grand-Duc Nikolaievich 
* Alexandre Rignault as Bloch 
* Gabrielle Robinne as Tsarine-mère 
* Martial Rèbe as Iliodore 
* Denis dInès as Évèque Gregorian 
* Georges Vitray as Ivanov 
* André Gabriello as Stankevitch  Joffre as Larchimandrite 
* Georges Paulais as Un quémandeur 
* Léon Larive as Le pope 
* Georges Bever as Le servant du pope 
* Jean Claudio as Le tsarevitch 
* Marcel Barencey as Membre du Saint-Synode 
* Marguerite Templey as La générale 
* Ginette Gaubert as Princesse Dolgoroukine 
* Paul Escoffier as Médecin de la cour 
* Suzanne Devoyod as La supérieure du couvent 
* Lucien Hector as Lhomme de la mobilisation 
* Robert Moor as Maître-dhôtel dAnia 
* Génia Vaury as Nadia 
* Cécile Didier as Maria 
* Mady Berry as  Dounia 
* Zélie Yzelle as Une paysanne 
* Valentine Camax as Une religieuse 
* Colette Régis as Une religieuse 
* Lucien Pascal as Le politicien 
* Roger Blin as Le jeune paysan 
* Alexandre Mihalesco as Le prédicateur 
* Albert Brouett as Un solliciteur 
* Georges Morton as Un solliciteur 
* Albert Malbert as Le policier

==References==
 

==Bibliography==
* Kennedy-Karpat, Colleen. Rogues, Romance, and Exoticism in French Cinema of the 1930s. Fairleigh Dickinson, 2013.

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 