The Time That Remains
{{Infobox Film 
 | 
 | name = The Time That Remains
 | alt = 
 | caption = 
 | director = Elia Suleiman
 | producer = Hani Farsi Michael Gentile
 | writer = Elia Suleiman
 | narrator = 
 | starring = Elia Suleiman Saleh Bakri Leila Mouammar Bilal Zidani Samar Qudha Tanus 
 | music = Matthieu Sibony
 | cinematography = Marc-André Batigne
 | editing = Véronique Lange
 | studio = 
 | distributor = Le Pacte
 | released =  
 | runtime = 109 minutes
 | country = United Kingdom Italy Belgium France English
 | budget = 
 | gross = 
 | image = The Time That Remains VideoCover.jpeg
}}
The Time That Remains is a 2009 semi-biographical drama film written and directed by Palestinian director Elia Suleiman. The film stars Ali Suliman, Elia Suleiman, Saleh Bakri and Samar Qudha Tanus. It gives an account of the creation of the Israeli state from 1948 to the present.  Suleiman participated in the 2009 Cannes Film Festival, as his new film competed in the official selection category.  The Time That Remains was also screened at the 2009 Toronto International Film Festival.  In November 2009, the film won the Jury Grand Prize (with About Elly) at the Asia Pacific Screen Awards. The film won the Critics Prize of the Argentinian Film Critics Association at Mar del Plata International Film Festival. 

==Plot==
In four episodes, Suleiman recounts family stories inspired by his father Fuads private diaries starting from when he was a resistance fighter in 1948, and his mother’s letters to family members who were forced to leave the country during the same period. In addition, Suleiman also combines his own memories in an attempt to provide a portrait of the daily life of the Palestinians who were labeled "Israeli-Arabs" after they chose to remain in their country and become a minority.

==References==
 

==External links==
*  
*  
*  
*   at Facebook
*  
*   at Metacritic
*   at  Twitter
*  
*  
*  
*  . Jerusalem Post.
*  
 
 
 
 
 
 
 
 