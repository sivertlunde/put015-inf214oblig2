The 51st State
 
 
 
 
{{Infobox film
| name            = The 51st State
| image           = Fifty first state ver1.jpg
| image size      = 
| caption         = Theatrical release poster
| writer          = Stel Pavlou
| starring        = Samuel L. Jackson Robert Carlyle Emily Mortimer Meat Loaf Ricky Tomlinson
| director        = Ronny Yu
| cinematography  = Poon Hang-Sang
| producer        = David Pupkewitz Malcolm Kohll Andras Hamori Mark Aldridge
| distributor     = Momentum Pictures (United Kingdom|UK) Screen Gems (United States|US) Alliance Atlantis (Canada) Paramount Pictures (Australia)
| released        =  
| runtime         = 92 minutes
| country         = Canada United Kingdom 
| language        = English
| budget          = $27,000,000      
| gross           = $14,439,698 
}} action comedy film. The film stars Samuel L. Jackson, Robert Carlyle, Emily Mortimer, Ricky Tomlinson, Sean Pertwee, Rhys Ifans, and Meat Loaf. The film follows the story of an American master chemist (Jackson) who heads to the United Kingdom to sell his formula for a powerful new drug. All does not go as planned and Jackson soon becomes entangled in a web of deceit.

The 51st State premiered in the United Kingdom on 7 December 2001. It was released worldwide under the name Formula 51 in October 2002, where it grossed $14.4 million, just over half of the budget.

==Plot==
  marijuana when a patrolman pulls over his car. Because of his arrest and conviction, he is unable to find work as a pharmacologist. Back in the present day a drug lord called "the Lizard" (Meat Loaf) calls a meeting with his organization, hoping to sell a brand new substance invented by Elmo. The meeting goes awry when Elmo, in a bid to escape from the Lizards control, blows up the building, killing everyone but the Lizard, who falls through the floor. With revenge on his mind, the Lizard contacts Dakota (Emily Mortimer), a contract killer, who previously killed the only witness in a case against the Lizard. Dakota initially refuses to chase down and kill Elmo, but when the Lizard offers to clear her gambling debts and give her a $250,000 bonus to complete the hit, Dakota takes the job.
 Liverpool versus Manchester United game.

At the meeting, Elmo makes his pitch, claiming that his product, POS 51, a synthetic drug that can be produced with minimal facilities, is 51 times as potent as Methylenedioxymethamphetamine|ecstasy, opiates, amphetamines, and cocaine. A second opinion from Pudsey, Durants chemist, confirms Elmos claims and Durant gives him over a million dollars in bonds. Since it is 18 million dollars short of the agreed payment, Elmo threatens to leave.

Meanwhile, in a room across the street, Dakota is watching through a rifle scope, waiting for her chance to kill McElroy, but Felix keeps unknowingly standing in the way. As she is about to pull the trigger, the Lizard calls, now being concerned about keeping Elmo alive until he can get the formula. Instead of killing Elmo, she is to kill anyone who is meeting with him. She switches rifles to an automatic weapon and kills everyone in the room except Felix and Elmo. Escaping with Elmo, Felix is shot in the rear by Dakota, who remarks, "Told you to get out the way, Felix". As Elmo and Felix leave the hotel, the two are attacked by a gang of skinheads, who are also out to get the drug. Elmo protects himself and Felix with a golf club.
 chicken by Elmo, who escapes. Kane returns to the crime scene and strings up Durant on the bottom of a large cargo container. He demands 50 percent of Durants deal with McElroy. A miscommunication leads to the cargo container being dropped, rather than being lowered slowly, crushing and killing Durant in the process.

Elmo asks Felix to contact the next drug lord in town, who is a gun dealing club owner and drug distributor named Iki (Rhys Ifans), promising him his formula for £20 million. Elmo and Felix make their way to a chemist (drug store) to get the ingredients for POS 51. One of the drugs defining attributes is that it can be made with Over-the-counter drug|over-the-counter products, none of which yet appear on any governments banned substance list. They are followed by the skinheads, who are armed this time. Elmo isnt that dismayed at being captured, as the skinheads claim they have a lab to produce the drug. The lab turns out to be an Animal Testing Facility that the skinheads have broken into. Elmo makes two batches of the drug; one blue and one red. He claims that the red pill is the stronger version, and after he takes one, the skinheads try it. While they are partying, waiting for the effect of the drug, in the next room Elmo spits out his red pill. He relates to Felix that its primary effect is that of a powerful laxative, which should take effect any moment. Elmo and Felix leave after throwing some rolls of toilet paper to the screaming skinheads, who are writhing on the floor.

The two visit Ikis rave club, where Elmo initiates his deal with the raver king and delivers the drug to the waiting crowd. The distribution is interrupted by Kane and a police raid, Felix is surrounded by police and arrested. Dakota appears, and it is revealed that her real name is Dawn and that she and Felix were romantically involved. She captures Elmo and attempts to leave with him via the roof and the escape ladder. Elmo gets the upper hand, suspending her over the edge of the roof. Having no choice, she strikes a deal with him and they escape from Kane.

Meanwhile, Felix is in police custody, being interrogated by Kane. The corrupt cop wants in on their deal with Iki or he will get Felix on charges of possession of a firearm. He arranges the time and the place, letting Kane in on the deal.
 Liverpool vs. Manchester United game, in a private viewing box, at Anfield. This time the deal is interrupted by the Lizard, who shoots Iki and demands the formula to POS 51. The Lizard celebrates with a drink, as Elmo reveals the true nature of the drug-its a placebo and the ingredients cancel each other out, making it the "most expensive candy" in the world. Elmo tells the Lizard that POS stands for Power of Suggestion.

Kane interrupts the moment, just as Elmos cocktail, ingested by the Lizard, takes effect. The cocktail contains a chemical that becomes explosive when it reaches a certain temperature; this is the same chemical Elmo used in his earlier attempt to leave his employment with the Lizard. Pulling an umbrella from his  golf bag, Elmo, Felix and Dawn take cover behind it. Police arrive and arrest Kane, who is lying unconscious on the floor, covered in bloody bits of the Lizard. Having made their escape, the three divide the money, Dawn and Felix agree to give their relationship a second chance. During the credits, a putt falls in a hole. Elmo is playing golf in front of the castle (filmed at Cholmondeley Castle|Cholmondeley, Cheshire) seen on the card pinned to the Lizards lab wall in the beginning of the film (the same card with the formula burned by Elmo in the skybox). The castle is the home of the McElroy clan.

Elmo finishes a round of golf with Hector Dougal McElroy, whom Elmo reveals is a descendant of the same family that once owned Elmos ancestors. Elmo now owns the McElroy castle, and as he walks towards it, he strips naked and says "Elmo is in the house!"

==Cast==
* Samuel L. Jackson as Elmo McElroy
* Robert Carlyle as Felix DeSouza
* Emily Mortimer as Dawn "Dakota" Parker
* Meat Loaf as The Lizard
* Sean Pertwee as Detective Virgil Kane
* Ricky Tomlinson as Leopold Durant
* Rhys Ifans as Iki Ade as Omar

===Casting===
The DVD commentary reveals that the script was originally written with Laurence Fishburne in mind, sometime before Samuel L. Jackson became a star.

==Production==

===Development=== slave trade drug trade. Pavlou and his business partner Mark Aldridge showcased their idea at the Cannes Film Festival in France which lead to film development company Focus Films offering funding for development. Soon the movie caught the eye of Samuel L. Jackson, who eventually came on board as both a producer and star of the film. 
 UK via Alliance Atlantis and The Film Consortium.   

===Pre-production===
Movie star and film producer Samuel L. Jackson recommended Hong Kong director Ronny Yu to direct the film with belief that the movies overall style was suited to that of Yus previous movie credits, such as his 1998 film Bride of Chucky. With the roles of Elmo McElroy (Samuel L. Jackson) and Felix DeSouza (Robert Carlyle) both secured, producer Andras Hamori suggested Meat Loaf to play the bad guy, which was approved by director Yu who called the idea a "truly inspired piece of casting". 

===Filming===

====Locations==== India Building, St Georges Hall and the Liver Building.

Production designer Alan Macdonald used the films production base in Boundary Street to build various sets for interior scenes, as well as a vast disused warehouse space in Blackstock Street.

==Release==

===Home media=== trailer and cast and crew interviews. 

==Reception== US release, the film was renamed Formula 51. Both names reference the drug &mdash; POS 51, which the film centres around &mdash; that is apparently "fifty-one times more powerful than cocaine, fifty-one times more hallucinogenic than Lysergic acid diethylamide|acid, fifty-one times more explosive than MDMA|ecstasy."   

===Critical reception===
 
The film received a rating of 25% based on 102 reviews on Rotten Tomatoes,  as well as a score of 23 out of 100 at Metacritic representing "generally unfavorable reviews". 

Roger Ebert of the Chicago Sun-Times newspaper called the movie "a farce", giving the film one out of four stars, and particularly negative comments on the films content and script.  
Generally positive reviews were given by both BBC  and Empire (film magazine)|Empire reviewers, with Alan Morrison of the latter calling it "full-on fun" and that the movie "goes beyond the boundaries" of British films.  
IGN|IGN.com also gave the film a generally positive review, concluding that "you get exactly what you pay for" and that the film was overall very "enjoyable". 

===Box office===
The film had its world premiere on 7 December 2001, in Londons West End Curzon Cinema  and took in £913,239 on its opening weekend. In the United States it made $2,818,142 on its opening weekend and was viewed across the country on 1,857 screens. 

In total, the film earned over $14.4 million at the worldwide box office, $5.2 million of that in the U.S. and $9.2 million elsewhere. 

==Soundtrack==
  Stephen Day.   

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 