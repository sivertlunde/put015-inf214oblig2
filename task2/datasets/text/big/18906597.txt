The Galloping Major (film)
{{Infobox film
| name           = The Galloping Major
| image_size     = 
| image          = The Galloping Major FilmPoster.jpeg
| caption        = 
| director       = Henry Cornelius
| producer       = Monja Danischewsky
| writer         = Monja Danischewsky Henry Cornelius Basil Radford
| narrator       = 
| starring       = Basil Radford Jimmy Hanley  Janette Scott A. E. Matthews Rene Ray
| cinematography = Stanley Pavey
| editing        =
| music          =
| distributor    = British Lion Film Corporation
| released       = 8 May 1951
| runtime        = 82 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross = £153,770 (UK) 
}} British comedy comedy sports Charles Hawtrey The Galloping Major", and the plot was centred on gambling at the horse racing track. A syndicate in a London suburb club together to buy a race horse to run in the Grand National.

==Cast==
* Basil Radford - Major Arthur Hill
* Jimmy Hanley - Bill Collins
* Janette Scott - Susan Hill
* A. E. Matthews - Sir Robert Medleigh
* Rene Ray - Pam Riley
* Hugh Griffith - Harold Temple
* Joyce Grenfell - Maggie
* Charles Victor - Sam Fisher
* Sydney Tafler - Mr. Leon Charles Lamb - Ernie Smart, Horse Owner Charles Hawtrey - Lew Rimmel
* Alfie Bass - Newspaper seller
* Sid James - Bottomley
* Kenneth More - Rosedale Film Studio Director
* Leslie Phillips - Reporter

==Location==
* "Lambs Green" in the film is actually Belsize Village, London NW3. The "cafe" is now a Greengrocers (2012), but the whole area is easily recognisable.
* The race track was filmed at Alexandra Palace, which can be seen briefly in the background.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 