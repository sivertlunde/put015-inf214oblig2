The Greed of William Hart
{{Infobox film

| name           = The Greed of William Hart
| image          =|thumb|
| caption        = Theatrical Poster
| director       = Oswald Mitchell
| producer       = Gilbert Church
| writer         = John Gilling Henry Oscar Jenny Lynn Aubrey Woods
| music          = 
| cinematography = D.P. Cooper, S.D. Onions
| editing        = John F. House
| studio         = Bushey Studios
| distributor    = Ambassador Film Productions
| released       =  
| runtime        = 80 mins
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
}} bodysnatchers closely modeled on the real Burke and Hare. 

==Plot==

In 1828 Edinburgh, Scotland, two Irish immigrants, Mr. Hart (Tod Slaughter) and Mr. Moore (Henry Oscar), take up murdering the locals and selling their bodies to the local medical school, which needs fresh bodies for anatomy lectures and demonstrations. When a young woman goes missing, medical student Hugh Alston (Patrick Addison) suspects the two are involved in foul play, but the arrogant, amoral Dr. Cox (Arnold Bell) --the main buyer for the bodies-- attempts to hinder his investigation. Meanwhile, the murderous duo set their sights on eccentric local boy "Daft Jamie" (Aubrey Woods) and an old woman.

==Cast==
* Tod Slaughter - William Hart
* Henry Oscar - Mr Moore
* Jenny Lynn - Helen Moore
* Winifred Melville - Meg Hart
* Aubrey Woods - Daft Jamie Wilson
* Patrick Addison - Hugh Alston
* Arnold Bell - Dr. Cox
* Mary Love - Mary Patterson
* Ann Trego - Janet Brown
* Edward Malin - David Patterson
* Hubert Woodward - Innkeeper Swanson
* Dennis Wyndham - Sergeant Fisher

==Production==
 British Board of Film Censors, however, insisted that all references to the real-life murderers be removed. The film was then re-titled and Dubbing (filmmaking)|re-dubbed with different character names, substituting "Hart" and "Moore" for Hare and Burke, respectively, and "Dr. Cox" for Robert Knox|Dr. Knox. Some names, including victims Mary Patterson, Mrs. Docherty, and "Daft Jamie" Wilson, remain unchanged. 

Writer John Gilling would go on to script another version of the same story in 1960, titled The Flesh and the Fiends. This version used the correct names for the killers.

The film was made at Bushey Studios.

==References==
 

==Bibliography==
* Richards, Jeffrey (ed.) The Unknown 1930s: An Alternative History of the British Cinema, 1929-1939. I.B. Tauris, 1998.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 


 
 