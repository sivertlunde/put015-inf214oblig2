Enakkaga Kaathiru
 
 
{{Infobox film name           = Enakkage Kaathiru image          = EnakkagaKaathiru.JPG producer       = P. S. Nivas director       = P. S. Nivas writer         = E. Ramadas (dialogues) screenplay     = G. K. story          = G. K. music          = Ilaiyaraaja cinematography = P. S. Nivas starring  Suman Sumalatha Nisha Bhanuchander Malini Janagaraj Janagaraj Suresh                  studio         = Niveda Combines distributor    = Niveda Combines editing        =  runtime        = 160 minutes released       = 11 September 1981 country        = India language       = Tamil
}}

Enakkaga Kaathiru is a 1981 Tamil-language film directed by P. S. Nivas, starring Suman (actor)|Suman,  Sumalatha and Bhanuchander.  

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Dhaham || Uma Ramanan || Vairamuthu || 03:40
|- Gangai Amaran || 05:41
|-
| 3 || Ooty Malai || Ilaiyaraaja || 04:40
|-
| 4 || Pani Mazhai || Deepan Chakravarthy, S. P. Sailaja || 04:36
|}

==References==
 

== External links ==
 

 
 
 
 
 


 