Muqabla
 
{{Infobox film
| name           = Muqabla
| image          =  
| caption        =
| director       = T. Rama Rao|
| producer       = Anumolu Venkata Subba Rao 
| writer         =  Govinda Paresh Rawal
| music          = Dilip Sen, Sameer Sen
| cinematography = 
| editing        =
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = 
}}

Muqabla is 1993 Hindi film directed by T. Rama Rao and starring Karishma Kapoor, Govinda (actor)|Govinda, Paresh Rawal. Other cast include Shakti Kapoor, Aditya Pancholi, Asrani, Aruna Irani, and Farha Naaz.

== Plot ==
Suraj and Deepak live a poor lifestyle in a village along with their widowed father, who has always taught them to be honest. Both re-locate to the city and find employment with the Police Force, while Suraj is a Havaldar, Deepak is a Traffic Constable. Suraj does his job honestly and diligently and is often reprimanded by Inspector Waghmare. Then differences arise between Suraj and Deepak when the later comes to testy in favor of an accused, Narendra Khanna, who was arrested by Suraj for killing a man. Things come to a boil when Sonis husband is brutally murdered in broad daylight, and when officers of both Shaitaan Chowki and Kala Chowki refuse to investigate nor even register this homicide, she decides to take matters into her own hands

== Cast == Govinda  as  Havaldar Suraj 
* Karisma Kapoor  as  Karisma 
* Farha Naaz  as  Vandana
* Aditya Pancholi  as  Traffic Constable Deepak  
* Satyendra Kapoor  as  Surajs father
* Aruna Irani  as  Soni 
* Asrani  as  Sonis husband
* Paresh Rawal  as  Communications Minister Jaganath Jaggu Mishra 
* Shakti Kapoor  as  Khanna Saahab
* Satyendra Kapoor  as  Surajs dad
* Arun Govil  as  Havaldar Satyaprakash
* Vikas Anand  as  Superintendent of Police

==Soundtrack==
{{Tracklist
| all_music      = Dilip Sen-Sameer Sen
| lyrics_credits = yes
| extra_column   = Playback

| title1  = Chal Chaliye Wahan Pe Diljani
| lyrics1 = 
| extra1  = Anuradha Paudwal, Sonu Nigam

| title2  = Chhodo Mujhe Jaane Do
| lyrics2 = 
| extra2  = Anuradha Paudwal, Sonu Nigam

| title3  = Chhodo Mujhe Jaane Do
| lyrics3 = 
| extra3  = Anuradha Paudwal, Sonu Nigam

| title4  = Dekho Pyare Rooth
| lyrics4 = Ibrahim Ashq
| extra4  = Anuradha Paudwal, Suresh Wadkar

| title5  = Dil Tera Hai Deewana
| lyrics5 = Abhijeet

| title6  = Ek Ladki Ne Mujhe Jaadu Kiya
| lyrics6 = Nawab Arzoo
| extra6  = Anuradha Paudwal, Abhijeet, Sonu Nigam

| title7  = Jaane Jana Aaja
| lyrics7 = Dilip K. Tahir
| extra7  = Udit Narayan

| title8  = Jisko Pooja The Is Dil Ne
| lyrics8 = 
| extra8  = Anuradha Paudwal

| title9  = Khanke Yeh Kangna 
| lyrics9 = 
| extra9  = Anuradha Paudwal

| title10  = Naino Ko Karne Do
| lyrics10 = Maya Govind
| extra10  = Anuradha Paudwal, Sonu Nigam

| title11  = Tere Dil Mein Jo Hai
| lyrics11 = Lalit Sen
| extra11  = Anuradha Paudwal, Vipin Sachdeva, Sachdev

| title12  = Tumbak Tu Baba
| lyrics12 = Nawab Arzoo
| extra12  = Alisha Chinoy
}}

== External links ==
*  

 
 
 


 