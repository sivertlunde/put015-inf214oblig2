Silent Tongue
{{Infobox film
| name           = Silent Tongue
| image          = Silent Tongue.jpg
| image_size     =
| caption        =
| director       = Sam Shepard
| producer       =
| writer         = Sam Shepard
| narrator       =
| starring       = Richard Harris Sheila Tousey Alan Bates Dermot Mulroney River Phoenix
| cinematography =
| music          = Patrick OHearn
| distributor    = Trimark Pictures
| released       =  
| runtime        = 105 min.
| country        = United States United Kingdom France Netherlands English
| budget         =
}} Western written and directed by Sam Shepard. It was filmed in Spring 1992, but not released until 1994. It was filmed near Roswell, New Mexico and features Richard Harris, Sheila Tousey, Alan Bates, Dermot Mulroney and River Phoenix.

The film is about a young man named Talbot Roe (Phoenix), whos gone insane over the death of his wife. Talbots father, Prescott Roe (Harris) feels his sons pain and wants to find him a new wife. He goes back to the place where he bought Talbots first wife, from Eamon McCree (Bates). He finds the dead wifes sister (Tousey), who is a champion horse rider and Mr. McCrees daughter, which makes her only half-Indian. Roe asks McCree if he could have his last daughter for his son, but McCree refuses. Then, Roe kidnaps her and tries to get her to help him, and she takes the deal for gold and four horses. But Talbot isnt taking any chances for her—hes too afraid that shell try to take his wifes corpse from him. And for the last few nights, he sees the ghost of his dead wife, who wants him to destroy her corpse, but he wont.

==Cast==
*Richard Harris as Prescott Roe 
*Sheila Tousey as Awbonnie / Ghost 
*Alan Bates as Eamon McCree 
*River Phoenix as Talbot Roe 
*Dermot Mulroney as Reeves McCree 
*Jeri Arredondo as Velada McCree 
*Tantoo Cardinal as Silent Tongue 
*Bill Irwin as Comic  David Shiner as Straight Man  Tommy Thompson as Medicine Show Band Performer  Jack Herrick as Medicine Show Band Performer 
*Bland Simpson as Medicine Show Band Performer  Clay Buckner as Medicine Show Band Performer  Chris Frank as Medicine Show Band Performer 
*Arturo Gil as Little Person Acrobat #1

==Delay in release==
The film was the last to be released featuring a performance by River Phoenix, who died on October 31, 1993 from a drug overdose. The films release was delayed, and Phoenix continued to work on The Thing Called Love (the film he had just completed at the time of his death), which was released before Silent Tongue.

==See also==
* List of ghost films

==External links==
* 
 
 
 
 
 
 
 
 
 
 
 
 
 
 