The Year Without a Santa Claus
 
{{Infobox television film name     = The Year Without a Santa Claus image    = YWOASCDVD.jpg caption  = format  Animated Musical Musical
|runtime  = 48 mins creator  = director = Jules Bass Arthur Rankin Jr. producer = Jules Bass Arthur Rankin Jr. writer   = William Keenan based on   starring = Shirley Booth Mickey Rooney Dick Shawn George S. Irving Bob McFadden Bradley Bolke Rhoda Mann music    = Maury Laws country  =   United States   Japan language = English network  = ABC studio = Rankin-Bass Productions released =   first_aired = preceded_by = Santa Claus Is Comin to Town followed_by = A Miser Brothers Christmas
}} animated television special. The story is based on Phyllis McGinleys 1956 book of the same name, illustrated by Kurt Werth. It was originally broadcast December 10, 1974 on American Broadcasting Company|ABC.

==Plot==
Santa Claus wakes up with a cold on Christmas Eve.  He is told by his doctor–who thinks that nobody cares about Christmas any more–that he should make some changes to his routine. Santa decides to take a holiday instead of delivering gifts, and leaves it to the elves, Jingle and Jangle, to find proof that people still believe in Santa Claus.
 white line, and wearing funny-looking clothes on a Sunday." They try to disguise Vixen as a dog, but Vixen is captured and sent to the dog pound, where she becomes very sick. They befriend a boy named Ignatius Thistlewhite and visit the skeptical Mayor of Southtown.  The Mayor laughs hysterically at their story but agrees to free Vixen if Jingle and Jangle prove that they are elves by making it snow in Southtown on Christmas Day.

On their behalf, Mrs. Claus visits the Miser Bros. to ask Snow Miser to make it snow in Southtown. Snow Miser declares that the town is under Heat Misers control. Mrs. Claus proposes a compromise. Heat Miser will agree only if Snow Miser will surrender the North Pole to his control. When Snow Miser refuses, Mrs. Claus appeals to their mother, Mother Nature, who forces them to compromise.

Santa, dressed in civilian clothes, arrives in Southtown to rescue Vixen. He discovers that some people still believe in him and in the spirit of Christmas when all the worlds children make him presents. This sets off headlines around the world.
 Blue Christmas". Touched by all the caring and generosity, Santa decides to pack the sleigh and make his Christmas Eve journey after all, including a public stop in a snowy Southtown.

==Production== ABC where it aired annually until 1980, and still airs on the ABC Family cable network. Warner Bros. Television is the shows current distributor, through their ownership of the post-1973 Rankin/Bass TV library.

==Songs==
#"The Year Without a Santa Claus"
#"I Could Be Santa Claus"
#"I Believe in Santa Claus"
#"Its Gonna Snow Right Here in Dixie"
#"The Snow Miser Song"
#"The Heat Miser Song" Blue Christmas"
#"Here Comes Santa Claus"
#"The Year Without a Santa Claus (reprise)"

==DVD details==
*Release date: October 31, 2000 (Original DVD), October 2007 (Deluxe Edition DVD), October 2010 (Blu-ray)
*Full Screen
*Region: 1
*Aspect Ratios: 1.33:1
*Audio tracks: English
*Special Features:
**Rudolphs Shiny New Year
** Nestor, The Long-Eared Christmas Donkey
**Stop Motion 101 (Deluxe Edition Exclusive)
**We Are Santas Elves: Profiling Arthur Rankin Jr. & Jules Bass (Deluxe Edition Exclusive)

==Live-action remake== 
 
A 90-minute 2006 live-action remake of the Rankin-Bass classic The Year Without a Santa Claus which premiered on NBC December 11, 2006. A widescreen DVD was released on December 12, 2006 (UPC 085391115120). 

This remake follows the same basic concept as the original: Santa, disillusioned by childrens lack of belief in him and in the spirit of giving, decides not to deliver toys this Christmas Eve, despite the arguments by Mrs. Claus and two of his helper-elves, Jingle and Jangle. They decide to provide Santa with some proof that children still believe and that they still deserve toys from Santa, so the elves visit the United States in search of Christmas spirit.

==Sequel==
 
This 2008 sequel used stop-motion animation like the original. It was animated by Cuppa Coffee Studios. Mickey Rooney, at age 88, reprised his role as Santa Claus; and George S. Irving, at age 86, reprised his role as Heat Miser. Snow Miser, originally voiced by Dick Shawn who died in 1987, was voiced by Juan Chioran. Mrs. Claus, originally voiced by Shirley Booth who died in 1992, was voiced by Catherine Disher.

==In popular culture== Batman & Robin, Mr. Freeze (Arnold Schwarzenegger) conducts his henchmen in a version of "The Snow Miser" song in his hideout.
*Portland based band Heatmiser took its name from a character in this seasonal classic.

==See also==
* List of animated feature films
* List of stop-motion films

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 