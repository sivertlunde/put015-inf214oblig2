Eduart
{{Infobox film
| name           = Eduart
| image          = 
| caption        = 
| director       = Angeliki Antoniou
| producer       = 
| writer         = Angeliki Antoniou
| starring       = Eshref Durmishi
| music          = 
| cinematography = Jürgen Jürges
| editing        = 
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = Greece
| language       = Greek
| budget         = 
}}
Eduart is a 2006 Greek drama film directed by Angeliki Antoniou. It was Greeces submission to the 80th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.         It was also entered into the 29th Moscow International Film Festival.   

==Cast==
* Eshref Durmishi as Eduart
* André Hennicke as Christoph
* Ndriçim Xhepa as Raman
* Ermela Teli as Natasha
* Adrian Aziri as Elton
* Gazmend Gjokaj as Pedro
* Manos Vakousis as Giorgos Harisis
* Edi Mehana as Ali

==See also==
*List of submissions to the 80th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 