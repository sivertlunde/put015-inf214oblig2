Head Against the Wall
{{Infobox film
| name           = Head Against the Wall
| image          = Latetecontre.jpg
| caption        =
| director       = Georges Franju
| producer       =
| writer         = Book author:  
| starring       = Pierre Brasseur Paul Meurisse Jean-Pierre Mocky Anouk Aimée Charles Aznavour Jean Galland
| music          = Maurice Jarre
| cinematography = Eugen Schüfftan
| editing        =
| distributor    = 1959
| runtime        = 95 minutes
| country        = France French
| budget         =
}}
 1959 French-language|French drama film directed by Georges Franju and starring Pierre Brasseur, Paul Meurisse, and Jean-Pierre Mocky. The story follows François who is institutionalized by Marbeau (Pierre Brasseur) for daring to defy his wealthy father. François verges on insanity during his involuntary internment. The film was released as The Keepers on its English release.

==Plot==
François is committed to a mental hospital by his father Maître Gérane, a wealthy man who does not tolerate objection. The institution is owned by Dr. Varmont whose traditional treatment methods conflict with the beliefs of Dr. Emery, supporter of a more sensitive approach. François becomes friends with an epileptic, Heurtevent and together they try to escape, but they fail and the sweet Heurtevent commits suicide. Afterwards François manages to flee and hides at the home of his friend, Stéphanie. 

==Production==
* Title: The head against the walls
* Director: Georges Franju
* Screenplay: Based on the novel of Hervé Bazin
* Adaptation: Georges Franju, Jean-Pierre Mocky
* Dialogues: Jean-Charles Pichon
* Assistant Director: Jacques Rouffio
* Images: Eugen Schüfftan, assisted by Claude Zidi
* Operator: Georges Miklachewsky
* Music: Maurice Jarre
* Sets: Louis Le Barbanchon
* Mounting: Suzanne Sandberg
* Makeup: Louis Dor, assisted by Marcelle Testard
* Photographer: Henri Caruel
* Script: Marcelle Rattle
* Production: Sirius, Atica, Elpenor-Movies
* Production Manager: Lucien Masson, Jerome Goulven, John Velter
* Distribution: Sirius
* Filming from May 16 to July 5, 1958
* Duration: 92 min
*  , black and white
* Release date: March 20, 1959
* French Film

==Cast==
* Pierre Brasseur as Dr. Varmont
* Paul Meurisse as Dr. Emery
* Jean-Pierre Mocky as François Gérane
* Anouk Aimée as Stéphanie
* Jean Galland as Maître Gérane
* Thomy Bourdelle as Colonel Donnadieu
* Jean Ozenne as Comte Elzéar de Chambrelle
* Charles Aznavour as Heurtevent
* Rudy Lenoir as the stashed
* Roger Legris as Decauville the driver
* Henri San Juan as the patron of the pool
* Edith Scob as the crazy singer
* Max Montavon as internal to the refectory
* Luis Masson as the internal
* Luc Andrieux as a nurse
* Jacques Mancier as the man with the gun
* Pierre Mirat as a guard
* Henri Poirier as the priest
* Jacques Seiler as a nurse

==Soundtrack==
{{Infobox album  
| Name        = Ma Période Française
| Type        = Soundtrack
| Longtype    =
| Artist      = Maurice Jarre
| Cover       = Mauricejarresoundtrack.jpg
| Released    = February, 2005
| Recorded    =
| Genre       = Film music
| Length      = 71:11
| Label       = Play Time
| Producer    =
| Reviews     =
| Last album  =
| This album  =
| Next album  =
}}
Long after the films original release, in February 2005, the French soundtrack record label Play Time released the soundtrack on compact disc along with other soundtracks performed by Jarre. This also includes soundtracks from other Franju films including Eyes Without a Face and Thérèse Desqueyroux (1962 film)|Thérèse Desqueyroux. {{cite web
| title = Anthologie 80ème Anniversaire
| publisher = FGL Productions
| year =
| month =
| url = http://www.fglmusic.com/produit?id=467
| language = French
| accessdate = 2008-08-26
}} 

===Tracklisting===
{{Track listing
| collapsed       = no
| headline        =
| extra_column    = Film
| total_length    =
| all_writing     =
| all_lyrics      =
| all_music       = Maurice Jarre 
| writing_credits =
| lyrics_credits  =
| music_credits   =
| title1          = Générique / Surprise-partie
| extra1          = La Tête contre les Murs
| length1         = 4:30
| title2          = Thème de Stéphanie
| extra2          = La Tête contre les Murs
| length2         = 4:30
| title3          = Enterrement à l’asile
| extra3          = La Tête contre les Murs
| length3         = 2:44
| title4          = Générique
| extra4          = Eyes Without a Face
| length4         = 2:05
| title5          = Thème romantique
| extra5          = Eyes Without a Face
| length5         = 2:50
| title6          = Filature
| extra6          = Eyes Without a Face
| length6         = 1:23
| title7          = Des phares dans la nuit
| extra7          = Eyes Without a Face
| length7         = 3:32
| title8          = Valse poursuite
| extra8          = Eyes Without a Face
| length8         = 1:45
| title9          = Final
| extra9          = Eyes Without a Face
| length9         = 1:01
| title10         = Générique
| extra10         = Thérèse Desqueyroux
| length10        = 1:54
| title11         = Non-lieu
| extra11         = Thérèse Desqueyroux
| length11        = 1:35
| title12         = Thérèse Desqueyroux
| extra12         = Thérèse Desqueyroux
| length12        = 2:50
| title13         = La femme idéale
| extra13         = Les Dragueurs
| length13        = 2:36
| title14         = La ballade des dragueurs
| extra14         = Les Dragueurs
| length14        = 2:47
| title15         = Surboum chez Ghislaine
| extra15         = Les Dragueurs
| length15        = 2:01
| title16         = Loiseau de paradis
| extra16         = LOiseau de Paradis
| length16        = 2:48
| title17         = Lunivers dUtrillo
| extra17         = Un court-métrage de Georges Régnier 
| length17        = 4:44
| title18         = Générique
| extra18         = Le Soleil dans l’œil
| length18        = 2:28
| title19         = Thème
| extra19         = Mort, où est ta Victoire ?
| length19        = 3:30
| title20         = Valse de Platonov
| extra20         = Recours en Grâce
| length20        = 3:50
| title21         = Les animaux (générique)
| extra21         = Les Animaux
| length21        = 1:20
| title22         = Pavane des flamands roses
| extra22         = Les Animaux
| length22        = 2:43
| title23         = La fête
| extra23         = Les Animaux
| length23        = 2:18
| title24         = Surf des loutres
| extra24         = Les Animaux
| length24        = 1:59
| title25         = Mourir à Madrid
| extra25         = Mourir à Madrid
| length25        = 4:21
| title26         = Générique
| extra26         = Week-End à Zuydcoote
| length26        = 2:28
| title27         = Sergent Maillat
| extra27         = Week-End à Zuydcoote
| length27        = 3:10
| title28         = Final
| extra28         = Week-End à Zuydcoote
| length28        = 1:29

}}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 

 
 