Love Is a Headache
{{Infobox film
| name           = Love Is a Headache
| image          = File:Love is a Headache lobby card.jpg
| alt            = 
| caption        = Lobby card
| director       = Richard Thorpe
| producer       = Frederick Stephani
| screenplay     = Marion Parsonnet Harry Ruskin William R. Lipman 
| story          = Louis E. Heifetz Herbert Kline 
| starring       = Gladys George Franchot Tone Ted Healy Mickey Rooney Frank Jenks Ralph Morgan Edward Ward
| cinematography = John F. Seitz
| editing        = Conrad A. Nervig
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Love Is a Headache is a 1938 American comedy film directed by Richard Thorpe and written by Marion Parsonnet, Harry Ruskin and William R. Lipman. The film stars Gladys George, Franchot Tone, Ted Healy, Mickey Rooney, Frank Jenks and Ralph Morgan. The film was released on January 14, 1938, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Gladys George as Carlotta Charlie Lee
*Franchot Tone as Peter Lawrence
*Ted Healy as Jimmy Slattery
*Mickey Rooney as Mike OToole
*Frank Jenks as Joe Cannon
*Ralph Morgan as Reginald Reggie Odell
*Virginia Weidler as Jake OToole
*Jessie Ralph as Sheriff Janet Winfield
*Fay Holden	as Mary
*Barnett Parker as Hotchkiss
*Julius Tannen as Mr. Hillier

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 