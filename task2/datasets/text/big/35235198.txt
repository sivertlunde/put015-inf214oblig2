Don't Change the Subject
{{Infobox film
| name= Dont Change the Subject 
| alt= 
| image = 
| director= Mike Stutz
| producer= Matt Ima, Fiona Walsh
| writer= 
| music= Charlie Recksieck, The Bigfellas
| cinematography= Jay Lafayette
| editing= Gail Yasunaga
| distributor= 
| released= 
| runtime= 97 minutes
| country= United States
| language= English
| budget= 
}}
Dont Change the Subject is a 2012 documentary film executive produced and directed by Mike Stutz, focusing on suicide and attempts to deal with the subject more directly through the use of humor and the arts as well as interviews.  The film weaves in and out of three modes: 1) Stutz experience with suicide in his family; 2) Candid interviews with suicide survivors and experts in fields such as mental health, religion, theater; and 3) Commissioning artists to create works for the film that tackle suicide in a unique way.

== Synopsis == The Lucent Dossier Experience.

As some of the interviews take a turn from the dark to the more cathartic, the tone of the movie brightens at the end during the performances of the artistic material.  The interview subjects give positive, inspiring messages to the audience about things they can actively do to help prevent suicides and connect more deeply with those they love.  The movie closes with images of another ceremony at the ocean which bookends the Day Of The Dead images from the beginning of the film.

== Release ==
The film be officially released in Fall 2012 at a suicide arts festival in Los Angeles being produced by the makers of the film.  Sneak preview screenings and showings at colleges, mental health organizations have already commenced as of early 2012.

== Critical reception ==
Some in the mental health community have called into question the issues of Suicide contagion|contagion, but many others have disputed this position and overall reviews have been positive.  

== References ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 