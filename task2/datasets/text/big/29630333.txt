They Found a Cave
{{Infobox film
| name           = They Found a Cave
| image          =
| image size     =
| caption        =
| director       = Andrew Steane
| producer       = Charles E. Wolnizer
| writer         = James Pearson William Eldridge
| based on = novel by Nan Chauncy
| narrator       =
| starring       = Beryl Meekin Mervyn Wiss
| music          =Peter Sculthorpe Larry Adler (harmonica)
| cinematography =Mervyn Gray
| editing        = Robert Walker Alan Harkness
| studio = Visatone Island Pictures
| distributor    = Columbia Pictures (Aust) Childrens Film Foundation (UK)
| released       = 20 December 1962 (Australia)
| runtime        = 61 min
| country        = Australia
| language       = English
| budget         =
| preceded by    =
| followed by    =
}} Australian childrens film|childrens adventure film directed by Andrew Steane. The movie was originally made from a book by the same name by author Nan Chauncy. In 2010, a company called Argosy Films, set up a website to find the production crew and actors/actresses of They Found a Cave and Bungala Boys for the 50th anniversary. 

==Plot==
Four English orphans – Cherry, Nigel, Brick and Nippy – migrate to Tasmania, to the care of their Aunt Jandie on her farm outside Hobart. Their arrival is greeted with enthusiasm by young farm boy Tas, and weeks of exploration and good times follow before Aunt Jandie enters hospital, leaving the children in the care of Ma and Pa Pinner, her foreman and housekeeper.

A few days of tyrannical treatment by the Pinners force the children to seek refuge in a secret cave, where they set up home to await the return of Jandie.  Despite Pas repeated efforts to recapture them, it is here the children stay until Nigels secret trip to town uncovers a plot by the Pinners to abandon the farm and swindle Aunt Jandie.

==Cast==
* Beryl Meekin – Ma Pinner
* Mervyn Wiss – Pa Pinner
* Barbara Manning – Aunt Jandie
* Anne Davies – Cherry
* Christopher Horner – Tas
* Michael Nation – Nigel
* Michael Woolford – Nippy Peter Conrad – Brick
* Michael Boddy as Sergeant Bentley
*Cecily McKinley as Mrs Clandy
*Joseph Smith as Bluey

==Production==
The Australian Council for Childrens Film and Television had been campaigning for locally made childrens films on Australian subjects. In 1961, Island Film Services, a Tasmania company formed by Charles E. Wolnizer, co-produced the film with Visatone Television. It was based on a popular childrens novel by Nan Chauncy. 

The lead actors were selected out of 500 children.   

The film was shot in south-east Tasmania in the summer and autumn of 1961 over seven months with interiors done at Elwick Showground in Hobart and some interiors done in Sydney. Most of the cast and crew were Tasmanian. Director Andrew Steane was former head of the Tasmanian Department of Film Production. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998 p232 

Larry Adler recorded harmonica for the music score while touring Australia. 

==Reception==
The film was positively reviewed but was not a success at the box office.  

==References==
 

==External links==
* 
*  at Oz Movies
* 
*  
 

 
 
 
 
 