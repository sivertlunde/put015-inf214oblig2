500 & 5 (Ayynoorum Ayynthum)
 

{{Infobox film
| name           = 500&5(Ayynoorum Ayynthum)
| image          = Movie poster 500 & 5.jpg
| alt            =
| caption        = First look poster
| director       = Raghu Jeganathan
| producer       = Accessible  Horizon Films
| writer         = Raghu Jeganathan
| story          =
| starring       = Deepak Sundararajan Shankar Lakshmi Priya Living Smile Vidya Chinnu Kuruvilla T M Karthik
| music          = Balaji Ramanujam
| cinematography = Mohandass Radhakrishnan Gerald
| editing        = Raghu Jeganathan Ramesh Mourthy
| art director = Kousalya Jeganathan
| studio         = Accessible Horizon Films
| runtime        = 120 minutes
| country        = India
| language       = Tamil
}}
 Tamil drama film   written and directed by Raghu Jeganathan  starring  Deepak Sundararajan, Shankar, Chinnu Kuruvilla, Living Smile Vidya and TM Karthik in the lead. 

==Synopsis==

Ayynoorum Ayynthum (500 & 5) is an Alternative, Independent and Experimental feature film in Tamil directed by Raghu Jeganathan and created by Mohandass Radhakrishnan, Kousalya Jeganathan and Ramesh Mourthy. The film has been produced by Accessible Horizon Films, an independent film collective.
 
The story is about the bizarre journey of a 500-Rupee note through 5 different characters – Sudalai(A Goons stooge, superstitious Sudalai wants to become a Don. Will he realize
his dream or will his misplaced sense of self get in the way of it?), Adi(Successful
film-director Adi, whos separated from his wife thanks to his philandering ways, is
desperate to redeem himself and get her back. Will he be able to?), Sundari(Spunky
Sundari is in love and works at a phone recharge shop. All she wants is to safeguard the
souvenir her boyfriend gives her. Will she hold on to it?), Jenny(Volatile, troubled Jenny
lives in a world where she walks a fine line between psychedelic reality and nightmarish
fantasy. Will she rise above it or get sucked into a self-destructive vortex of darkness?) and the Nameless revolutionary(The Nameless, avant-garde revolutionary who lives without money ignites an unprecedented rebellion with which he threatens to raze down the hyper-capitalistic juggernaut) - and how they perceive it in their own ways.
 
Though the film follows the 500-Rupee note, it actually focuses on the 5 key characters whose lives are entirely and contrastingly different from each other in the way they look at money.

==Cast==

* Deepak Sundararajan as Sudalai
* Shankar as Adi
* Living Smile Vidya as Sundari
* Chinnu Kuruvilla as Jenny
* TM Karthik as The Radical

Other key cast members include Lakshmi Priyaa Chandramouli, Vishwanth Natarajan, Ramesh Mourthy, Vinoth Kumar.
 
The total number of cast members is about 90, out of which 70% are new or first-time actors in film.

==Production==

 

==Concept==
After struggling to find producers and financing in a conventional way, the Accessible Horizon Films team decided that they had to something radical. But still, money was a big hurdle to get their scripts produced. That’s when during and after several discussions about ‘Money’, the team decided to make their film about money(a 500 Rupee note) and perceptions about it.

==Writing==
Once the core concept ‘the journey of a 500-Rupee note’ was decided, Director Raghu wrote the screenplay in 15 days at a single stretch. Some of the characters of the film are loosely inspired from real life like the ‘Money-less revolutionary’ character played by TM Karthik Srinivasan, was inspired from people like Mark Boyle, Heidemarie Schwermer, Daniel Suelo among others.

==Casting==
The Casting of the film took 2 months with intense audition sessions held in Chennai except for one character ‘Jenny’ which took almost 4 months because of the extreme nature of the character.
Several of the key cast members hail from a theater background with performances in Tamil, English and Malayalam. And while some are upcoming actors from the Tamil film industry, many of the cast members are new actors.

==Filming and Production==
The entire shooting schedule took about 21 days split over 3 to 4 schedules.

According to the team, ‘the film is a result of the Independent Filmmaking revolution’

==Promotion and Trailer==
A one minute trailer drew attention as soon as a promo was released on YouTube. 

A 5K marathon run was conducted to promote the concept of a world without money and around 150 people participated in that marathon. 
 

==Music==

 

==Soundtrack==
{{Infobox album|  
| Name       = 500&5 (Ayynoorum Ayynthum)
| Type       = Soundtrack
| Artist     = S.Balaji & S.Ramanujam
| Cover      = 
| Released   =   Feature film soundtrack
| Length     = Tamil
| Label      = Accessible Horizon Films
| Producer   = Accessible Horizon Films
}}

The songshave been written and composed by S. Ramanujam and S. Balaji. 
{{tracklist
| headline        = Tracklist
| extra_column    = Singer(s)
| total_length    =
| all_lyrics      = S.Balaji and S.Ramanujam

| title1          = Ulagathiley
| lyrics1         =
| extra1          = S.Balaji
| length1         = 5.17

| title2          = Ganamanatho
| lyrics2         =
| extra2          = Backiyaraj, Hemambiga
| length2         = 4.25

| title3          = Panathamattum
| lyrics3         =
| extra3          = S.Balaji
| length3         = 6.13

| title4          = Jenny’s Poem
| lyrics4         = Dr. Prakash
| extra4          = Kousalya Jeganathan
| length4         = 1.49

| title5          = Peiyappola
| lyrics5         =
| extra5          = S.Balaji
| length5         = 6.05
}}

==International Film Festivals/Critical reception==
The film was Officially Selected at the International Film Festival of Kerala, Trivandrum in 2012   and at the Filmburo Baden-Wurttemberg’s Stuttgart Indian Film Festival, Germany in 2013.   This is the only Tamil film to be selected in both these International film festivals  

==Release==

 

==References==
 

==External links==
* 

 
 
 
 
 
 