Public Cowboy No. 1
{{Infobox film
| name           = Public Cowboy No. 1
| image	         = Public Cowboy No. 1 FilmPoster.jpeg
| caption        = Theatrical release poster
| border         = yes
| director       = Joseph Kane
| producer       = Sol C. Siegel (associate) Oliver Drake
| story          = Bernard McConville
| starring       = {{Plainlist|
* Gene Autry
* Smiley Burnette
* Ann Rutherford
}}
| music          = Raoul Kraushaar
| cinematography = Jack A. Marta
| editing        = {{Plainlist|
* Lester Orlebeck
* George Reid
}}
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
}} Western film directed by Joseph Kane and starring Gene Autry, Smiley Burnette, and Ann Rutherford. Based on a story by Bernard McConville, the film is about a singing cowboy who chases down rustlers who are using airplanes, shortwave radios, and refrigerated trucks to steal cattle.   

==Plot==
A rash of strange cattle rustlings have occurred in which cattle are slaughtered on the range and their carcasses taken away. Sheriff Matt Doniphon (William Farnum) and his deputies, Gene Autry (Gene Autry) and Frog Millhouse (Smiley Burnette), watch over one ranchers cattle as they are driven to Box Canyon. After the sheriff and his men leave, the rustlers move in, radio the cattles location from an airplane, and then bring in refrigerator trucks. The rancher and one of his workers are murdered, the cattle killed, and the carcasses taken away.

Newspaper editor Helen Morgan (Ann Rutherford), responding to the increased cattle raids, demands that Sheriff Doniphon be replaced, claiming he is too old-fashioned to deal with modern rustlers. Gene defends the sheriff against Helens editorial. Having been raised by the sheriff after being orphaned by outlaws as a young boy, Gene knows the mans character and abilities. Helen, however, refuses to change her stance.

While investigating the recent raids, Gene and Frog grow suspicious of the Chicago and Western Packing Co., owned by Jack Shannon (Arthur Loft) and run by Jack and his brother Jim (House Peters Jr.). The deputies find the carcasses of some rustled cattle and demand to see the hides in order to check the brands. Lying to the deputies, Jim tells them that the cattle belong to his partner, Thad Slaughter (Maston Williams), and that Slaughter has the hides at his ranch. 

On their way to Slaughters ranch, Gene and Sheriff Doniphon discover Frog locked in one of Jims trucks. They chase after the truck and Jim shoots the sheriff, who is not seriously wounded. Later that afternoon, Frog identifies Jim as the one who shot the sheriff. Jim is taken to jail, and Jack and Slaughter grow concerned that Jim may talk and expose their operation. That night, Slaughter summons Jim to the jail window and beats Jim to death. 

The next day, many of the towns citizens demand that Sheriff Doniphon resign, blaming him for the murder and the ongoing cattle raids. Eustace P. Quackenbush (James C. Morton) and his uniformed private detectives are soon hired to put an end to the raids and restore order with their modern, scientific methods. At the welcoming party, Jack learns that rancher Bidwells men are all in town and alerts his rustlers to go to Bidwells ranch, where Frog and Stubby (Frankie Marvin) lay in wait for the desperados, wearing a cow costume. When he sees the rustlers approaching, Frog sends an emergency message to Gene, who then uses the radio to call all local cowboys to defend Bidwells ranch against the rustlers. Hearing the broadcast, the rustlers attempt to flee. Frog and Stubby also have to flee from an amorous bull.

Meanwhile, on their way to the Bidwell ranch, the automobiles and motorcycles used by Quackenbush and his detectives get stuck in the mud, while the cowboys ride past the detectives on their trusty horses and quickly round up the gang. Sheriff Doniphon shoots Jack as he attempts to use Helen as a hostage, thereby proving to her that old-fashioned methods are still the best. While Frog and Stubby try to outrun a bull attracted by their cow costume, Gene and Helen ride back to town together, passing Quackenbush and his detectives who are still stuck in the mud and suffering from the effects of their tear gas grenades that have accidentally detonated.

==Cast==
* Gene Autry as Deputy Sheriff Gene Autry
* Smiley Burnette as Frog Millhouse
* Ann Rutherford as Helen Morgan
* William Farnum as Sheriff Matt Doniphon
* Arthur Loft as Jack Shannon
* Frankie Marvin as Deputy Stubby
* House Peters Jr. as Jim Shannon
* James C. Morton as Eustace P. Quackenbush
* Maston Williams as Thad Slaughter
* Frank LaRue as Justice
* Milburn Morante as Ezra, the Newspaper Printer
* Champion as Genes Horse (uncredited)   

==Production==
===Filming locations===
* Kernville, California, USA   

===Soundtrack===
* "Wanderers" (Felix Bernard, Paul Francis Webster) by Gene Autry, William Farnum, and others
* "The West Aint What It Used to Be" (Fleming Allen) by Gene Autry (vocal) and Smiley Burnette (harmonica)
* "I Got the Heebie-Jeebie Blues" (Smiley Burnette) by Smiley Burnette (vocal and guitar)
* "I Picked Up the Trail When I Found You" (Fleming Allen) by Gene Autry (vocal and guitar)
* "The Defective Detective from Brooklyn" (Smiley Burnette) by Smiley Burnette on the radio
* "Old Buck-A-Roo" (Fleming Allen) by Gene Autry on the radio   

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 