Panduranga Mahatyam (film)
 
{{Infobox film
| name           = Panduranga Mahatyam
| image          = Panduranga Mahatyam.jpg
| image_size     =
| caption        =
| director       = Kamalakara Kameswara Rao
| producer       = Nandamuri Trivikrama Rao
| writer         = Samudrala Ramanujacharya
| narrator       =
| starring       = N.T. Rama Rao Chittor V. Nagaiah Anjali Devi Rushyendramani B. Padmanabham B. Saroja Devi Peketi Sivaram Vijaya Nirmala
| music          = T. V. Raju
| cinematography = M. S. Rehman
| editing        = G. D. Joshi
| studio         = National Art Theatre Pvt. Ltd.
| distributor    =
| released       = November 28, 1957
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}

Panduranga Mahatyam is a 1957 Telugu film directed by Kamalakara Kameswara Rao and starring N. T. Rama Rao playing the role of Pundarika. The lyrics written by Samudrala Ramanujacharya are excellent. The songs of Ghantasala Venkateswara Rao particularly "Jaya Krishna Mukunda Murari" and "Amma Ani Pilichina" are remembered as the best film songs expressing devotion and sorrow.

==Plot==
Pundarikudu (N.T. Rama Rao) is from a pious Brahmin family who is not interested in the worship of God and irreligious, a complete rebel who is asked to mend his ways by his parents (Nagaiah and Rushyendramani). He steals their gold for a prostitute Kalavathy (B. Saroja Devi). His mother takes the blame, and they part ways. Kalavathy takes all his wealth and throws him out.

He goes to a hermitage where he falls in love with three disciples of the Sage, who break his legs for this blasphemy. He then realizes the importance of his parents, goes back to them, begs for forgiveness, and prays to Lord Panduranga to grant him the boon of merging into him along with his parents &mdash; giving the message that one has to serve ones parents and it is the quickest way to Salvation.

==Cast==
{| class="wikitable"
|-
! Actor !! Character
|-
| Nandamuri Taraka Rama Rao || Pundarikudu
|-
| Anjali Devi || Rama
|-
| Chittor V. Nagaiah || Jahnavi
|-
| Rushyendramani || Laxmi
|-
| B. Padmanabham || Brother of Pundarika
|-
| B. Saroja Devi || Kalavati
|-
| Govindarajula Subba Rao
|-
| K. V. S. Sarma || Vikatasura
|-
| Kasturi Siva Rao
|-
| Vangara Venkata Subbaiah
|-
| Chhaya Devi
|-
| Balakrishna
|-
| Peketi Sivaram
|-
| Ammaji || Champa
|-
| Showkar Janaki
|-
| Vijaya Nirmala || Lord Krishna (Bala Krishnudu)
|}

==Crew==
* Art: Thota Tharani
* Audiography: A. Krishnan
* Choreography: Vempati Satyam
* Cinematography: M. A. Rehman
* Costumes: Achyuta Rao
* Editing: G. D. Joshi
* Lyrics and dialogues: Samudrala Jr.
* Make-up: Haribabu, Doraswamy and Raman
* Music: T. V. Raju
* Nirvahana: Pundari Kakshaiah
* Direction: K. Kameshwara Rao
* Producer: Nandamuri Trivikrama Rao
* Banner: National Art Theatre Pvt. Ltd.

==Music==
* "Aanandhamidhenoi" (Singer: P. Susheela; Lyricist: Samudrala)
* "Amma Ani, Ye Paadaseema" (Singer: Ghantasala; Lyricist: Samudrala)
* "Chebithe Vintivaa Guru Guru" (Singers: Pithapuram Nageswara Rao, Madhavapeddi Satyam; Lyricist: Samudrala)
* "Ekkadoyi Muddula Bava" (Singers: A. P. Komala, Pithapuram Nageswara Rao; Lyricist: Samudrala)
* "Hara Hara Sambho" (Singer: Ghantasala; Lyricist: Samudrala)
* "Jaya Jaya Gokula" (Singer: Chittor V. Nagaiah; Lyricist: Samudrala)
* "Jaya Krishna Mukunda Murari" (Singer: Ghantasala; Lyricist: Samudrala)
* "Kanavera Muni Raja" (Singer: P. Leela; Lyricist: Samudrala)
* "Neevani Nenani" (Singers: Ghantasala, P. Susheela; Lyricist: Samudrala)
* "Oh Daari Kaanani" (Singer: M. S. Rama Rao; Lyricist: Samudrala)
* "Pedhavula Ragam" (Singer: Jikki; Lyricist: Samudrala)
* "Sannuthi Seyave Manasaa" (Singer: Chittor V. Nagaiah; Lyricist: Samudrala)
* "Tharam Tharam" (Singer: Ghantasala; Lyricist: Samudrala)
* "Tholu Thitthi Idhi" (Singers: Madhavapeddi Satyam, Pithapuram; Lyricist: Samudrala)
* "Vannela Chinnela" (Singers: Ghantasala, P. Leela; Lyricist: Samudrala)

==Features==
* "Jaya Krishna Mukunda Murari" rendered by Ghantasala and composed by T.V. Raju is one of the longest songs in Telugu, lasting for about 15 minutes.
* Vijaya Nirmala, who debuted in Telugu with this as a child artiste played the baby Krishna.
* "Amma Ani Pilichina Aaalakinchavemamma" is another song in this movie that melted many hearts and brought them to tears.
* B. Saroja Devi, an actress in Tamil and Kannada movies was happy about acting with N. T. Rama Rao in her debut film in Telugu language.

==Awards==
* Nandamuri Taraka Ramarao won the Filmfare Best Actor Award (Telugu) (fourth in succession) for his portrayal of Pundarika.

==External links==
*  
*  

 
 
 
 
 
 