Aahat – Ek Ajib Kahani
{{Infobox film
| name           =Aahat – Ek Ajib Kahani 
| image          = Aahat74.jpg
| caption        = 
| director       = Kishor
| producer       = Franklin Fernandes,  Vishal
| writer         = 
| starring       =Dharmendra, Vinod Mehra, Jaya Bachchan, Amrish Puri
| music          = Chetan
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Aahat – Ek Ajib Kahani  is a 2010 Bollywood film directed by Kishor and produced by Vishal.  Although shot in 1974, the film went unreleased until February 26, 2010.    According to Salim Jafer, the distributor of the film in India, he refused to state exactly why the film was never released, but asserted that "at first there were all sorts of problems and then the producer of the movie, Franklin Fernandes died." 

The film is about a rich, confident, but blind young woman (Jaya Bachchan) married to a photographer (Vinod Mehra). After a necklace is stolen they encounter the villain, played by Amrish Puri.     The main focus of the film is on the disability of Bachchans character, that her blindness does not prevent her from achieving.

==Cast==
*Dharmendra
*Vinod Mehra
*Jaya Bachchan
*Shreeram Lagoo Sulochana
*Amrish Puri Bindu

==Release==
Over 200 prints of the film were distributed around India. The film was screened at the Liberty Cinema in Fort, Shradha in Dadar and Amar in Chembur. 

==References==
 

 
 
 
 


 