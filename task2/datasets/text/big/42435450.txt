Mannina Maga
 
{{Infobox film
| name           = Mannina Maga
| image          =
| image_size     =
| caption        =
| director       = Geethapriya
| producer       = M. V. Venkatachalam   Alexander
| writer         = Geethapriya
| screenplay     = Geethapriya
| narrator       = 
| starring       =  
| music          = Vijaya Bhaskar
| cinematography = V. Manohar
| editing        = Bal G. Yadav
| distributor    =
| released       = 1968
| runtime        = 114 minutes
| country        = India
| language       = Kannada
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Kalpana and Jayakumari in the lead roles.  Produced by Sudarshan Movies banner and written by Geethapriya, the film received rave reviews and went on to win National Film Award for Best Feature Film in Kannada and ran for more than 100 days in cinema halls.

==Cast== Rajkumar
* Kalpana
* M. P. Shankar
* Jayakumari
* H. R. Shastry
* Dikki Madhava Rao
* Vijayaprasad
* Ashwath Narayana
* B. Jaya (actress)|B. Jaya
* Shanthamma
* Jayachitra

==Soundtrack==
{{Infobox album
| Name        = Mannina Maga
| Type        = Soundtrack
| Artist      = Vijaya Bhaskar
| Cover       = Mannina Maga audio cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 1968
| Recorded    =  Feature film soundtrack
| Length      = 16:24
| Label       = Saregama Kannada
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

The films soundtrack tuned by Vijaya Bhaskar found immense popularity with "Bhagavanta Kaikotta" and "Idena Sabhyate" songs reaching the cult status with philosophical lyrics by Geethapriya. The album has five soundtracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 16:24
| lyrics_credits = yes
| title1 = Idhenu Sabhyathe
| lyrics1 = Geethapriya
| extra1 = P. Susheela
| length1 = 3:09
| title2 = Mellage Nadeyole
| lyrics2 = Geethapriya
| extra2 = P. Susheela
| length2 = 3:28
| title3 = Alutghihudhu Maanava
| lyrics3 = Geethapriya
| extra3 = P. B. Sreenivas
| length3 = 3:16
| title4 = Bhagavantha Kaikotta
| lyrics4 = Geethapriya
| extra4 = P. B. Sreenivas
| length4 = 3:10
| title5 = Bareyada Kaigalu
| lyrics5 = Geethapriya
| extra5 = S. Janaki
| length5 = 3:21
}}

==Awards==
* 1968 - National Film Award for Best Feature Film in Kannada

* Karnataka State Film Award for Best Film
* Karnataka State Film Award for Best Screenplay - Geethapriya

==References==
 

==External sources==
* 
* 

 
 

 
 
 
 
 
 
 
 