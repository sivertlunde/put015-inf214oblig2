Masala (2012 film)
 
{{Infobox film name           = Masala image           caption        = Masala Movie Poster director       = Sandesh Kulkarni produced       = Girish Kulkarni,  Umesh vinayak Kulkarni starring       = Girish Kulkarni Amruta Subhash Mohan Agashe Dilip Prabhavalkar Hrishikesh Joshi Shashank Shende cinematography =  Music = Anand Modak country        = India language  Marathi
}}

Masala (  directed by Sandesh Kulkarni and written by Girish Kulkarni. It is Kulkarnis directorial debut. The film is loosely based on the life story of Hukmichand Chordia of Pravin Masalewale fame. 

== Synopsis ==

A couple is forced to repeatedly move in search of sustainable business ventures. They live in constant fear of meeting their creditors, whose money they spent in unfruitful investments. When the couple meets another couple, things start changing for the better by following the wise counsel of their new acquaintances. The latter part of the movie showcases these changes.

== Reception ==

The movie was well received by the audience and appreciated by Film Critics - India|critics. 

== Cast ==
* Girish Kulkarni
* Amruta Subhash
* Mohan Agashe
* Dilip Prabhavalkar
* Hrishikesh Joshi
* Shashank Shende

== References ==
 

 
 
 