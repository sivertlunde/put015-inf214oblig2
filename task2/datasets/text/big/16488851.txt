Pie in the Sky (1996 film)
 
{{Infobox Film
| name           = Pie in the Sky
| image          = Pieintheskyposter.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Bryan Gordon
| producer       = Allan Mindel
| writer         = Bryan Gordon
| narrator       = 
| starring       = Josh Charles Anne Heche John Goodman Christine Lahti
| music          = Michael Convertino
| cinematography = Bernd Heinl
| editing        = Colleen Halsey	
| distributor    = Fine Line Features
| released       = 9 February 1996
| runtime        = 95 min.
| country        = United States Australia
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} traffic gridlock who falls in love with an avant-garde dancer. The film was written and directed by Bryan Gordon, and stars Josh Charles, Christine Lahti, John Goodman, and Anne Heche.

==Cast==
* Josh Charles as Charlie Dunlap
* John Goodman as Alan Davenport
* Christine Lahti as Ruby
* Anne Heche as Amy
* Dey Young as Mrs. Tarnell
* Christine Ebersole as Mom Dunlap
* Peter Riegert as Dad Dunlap
* Wil Wheaton as Jack
* Bob Balaban as Paul 
* Larry Holden as Amys boyfriend
* David Rasche as Amys dad William Newman as a Funeral Guest

== External links ==
*  

 
 
 
 
 
 


 