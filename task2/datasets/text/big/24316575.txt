Invisible Man: Rape!
{{Infobox film
| name = Invisible Man: Rape!
| image = Invisible Man - Rape!.jpg
| image_size = 
| caption = Theatrical poster for Invisible Man: Rape! (1978)
| director = Isao Hayashi   
| producer = Akihiko Yamaki
| writer = Kenshō Nakano (original story) Kochiho Katsura (screenplay)
| narrator = 
| starring = Izumi Shima
| music = Shin Takada
| cinematography = Kuratarō Takamura
| editing = 
| distributor = Nikkatsu
| released = December 23, 1978
| runtime = 65 min.
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1978 Japanese film in Nikkatsus Pink film#Second wave (The Nikkatsu Roman Porno era 1971–1982)|Roman porno series, directed by Isao Hayashi and starring Izumi Shima.

==Synopsis==
Ippei, a college student, finds a formula that can render him invisible. He uses it to spy on womens bathhouses, and have sex with female students. A professor becomes aware of his activities and punishes him.    
  

==Cast==
* Teru Satō: Ippei 
* Izumi Shima: Momoko (Ippeis wife)
* Maria Mari: Machiko (Momokos sister)
* Erina Miyai: Akira
* Shin Takada
* Yūko Asuka
* Nami Aoki
* Jun Aki
* Tatsuya Hamaguchi
* Toshihiko Oda
* Ichirō Kijima
* Genki Koyama

==Critical appraisal==
In their Japanese Cinema Encyclopedia: The Sex Films, the Weissers give Invisible Man: Rape! a rating of two points out of four. Judging Isai Hayashi to be an "apathetic" and "by-the-numbers" director, they note that this film is one of his best. Besides the interesting theme, they write that the film is helped by a good-looking and competent cast. 

  (1982) and Invisible Maniac (1980) than contemporary rape-themed Roman porno films. 

==Background==
Director Isao Hayashi worked with Nikkatsu from the beginning of the Roman porno era, directing Castle Orgies, one of the first two releases of the series.  The Weissers note that it may be Hayashis work with historical period dramas that gives his directorial style a pedantic feel.  Along with Lady Kamakura: Cherry Boy Club (1975) Invisible Man: Rape! was one of Hayashis most successful films. 

The lead actress in Invisible Man: Rape!, Izumi Shima, had been promoted as "Nikkatsus most beautiful actress", and was debuted in Lady Chatterley In Tokyo (1977), a project tailored for her.  When that film failed to gain much approval, Shima was given a string of lower-profile projects. Her career was revived in 1982 when SM-author Oniroku Dan chose her to star in his first film production, Dark Hair, Velvet Soul. After that, Shima made a name for herself in the S&M genre, especially with films based on works by Dan, and became regarded as one of Nikkatsus leading actresses of the 1980s. 

==Availability==
Invisible Man: Rape! was released theatrically in Japan on December 23, 1978.  It was released on home video in Japan in VHS format on June 7, 1996. 

==Bibliography==

===English===
*  
*  
*  
*  
*  

===Japanese===
*  
*  
*  
*  
* {{cite web |url=http://www.nikkatsu-romanporno.com/shousai/r133.html|script-title=ja:透明人間・犯せ！ 
 |accessdate=2009-09-11|publisher= |language=Japanese}}

==Notes==
 

 
 
 
 
 