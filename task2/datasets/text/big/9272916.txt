Alli Arjuna
 
{{Infobox film
| name           = Alli Arjuna
| image          = Saran
| Saran
| Karan Preetha Vijayakumar
| producer       = Chandraleela Bharathiraja
| cinematography = A. Venkatesh (cinematographer)|A. Venkatesh
| editing        = Suresh Urs
| music          = A. R. Rahman
| studio         = Janani Art Creations
| distributor    = Janani Art Creations
| released       = 14 January 2002 
| runtime        = 148 minutes
| country        = India Tamil
}} Tamil drama film directed by Saran (director)|Saran. The film featured Manoj Bharathiraja, Richa Pallod, Dhamu, Velmurugan Thangasamy Manohar Charle|Charle, Karan and Preetha Vijayakumar in major roles.  Music was by A. R. Rahman.

==Plot==
Arivazhagan (Manoj) has grown up to be a rowdy because of the neglect and ill-treatment by the hands of his parents and his only aim in life is to be an irritant to his father. The entire family attends the marriage of Savitri (Richa) in Bangalore and when the groom disappears, Arivu offers to take his place. The marriage never happens but Savitri shows up at Arivus house later, asking him to give her refuge. When he moves out of the house to take up residence with his friends, she follows him there too. Initially irritated by her, Arivu realises that he is gradually falling in love with her. Meanwhile, the friends learns that Savitri left her own surroundings just to get over the gruesome suicide of her close friend Nisha (Preetha), a victim of eve-teasing. Nishas brother Kishore (karan) and her family are still haunted by her death and the brother promises to avenge on the person who was the cause of his sisters death. Who is responsible for Nishas suicide, and will Arivu succeed on proposing his love towards Savitri forms the rest of the story.

==Cast==
* Manoj Bharathiraja as Arivazhagan/Arivu Karan as Kishore
* Richa Pallod as Savithri/Saavi
* Preetha Vijayakumar as Nisha
* Vinu Chakravarthy as Daddy
* Charle as Oomaiyan
* Dhamu as Kamatchi
* Vaiyapuri as Meenatchi
* Jaiganesh as Arivazhagans Father
* Fathima Babu as Arivazhagans Mother
* Nizhalgal Ravi as Savithris father Ambika as Savithris Mother
* Mahanadi Shankar as Kumar
* Santhana Bharathi
* Vanaja
* Uma Maheshwari
* Shyam Ganesh
* Vindhya in a special appearance

==Trivia==
Director Saran united with A R Rahman for the first time after his work with Music director Bharadwaj for his first three films. This would however be their last collaboration after which Director Saran had joined Bharadwaj for the films he had directed after.The movie was declared box office bomb due to its poor story, screenplay and characterization.

==Soundtrack==
{{Infobox album|  
 Name = Alli Arjuna |
 Type = Soundtrack |
 Artist = A.R.Rahman |
 Cover = | 2001 |
 Recorded = Panchathan Record Inn | Feature film soundtrack |
 Length = |
 Label = The Best Audio |
 Producer = A.R. Rahman |
 Reviews = |
 Last album = Parthale Paravasam  (2001) |
 This album = Alli Arjuna (2001) |
 Next album = Kannathil Muthamittal   (2002)|
}}
The soundtrack of the movie was composed by A.R.Rahman. The lyrics are done by Vairamuthu and Arivumathi. 4 songs are re-used from Rahmans earlier Hindi films Pukar,1947 Earth and One 2 Ka 4. The soundtrack was  released in 2001.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; "
|- bgcolor="#CCCCCC" align="center"
! Song !! Artist(s) !! Lyrics !! Notes
|-
| "Sollayo Solaikilli"
| S.P.Balasubramanyam, Swarnalatha
| Vairamuthu
| Reused from "Sunta Hai Mera Khuda" from Pukar (2000 film)|Pukar
|-
| "Shingu Lingu"
| K. S. Chithra
| Vairamuthu
|
|-
| "Onne Onne"
| Sadhana Sargam, Shankar Mahadevan
| Arivumadhi
| Reused "Banno Rani" from 1947 Earth|Earth
|-
| "Osaka Morayaa" Karthik
| Vairamuthu
| Reused "Osakka Muraiyaa" from One 2 Ka 4
|-
| "Endhan Nenjil" Srinivas
| Vairamuthu
| Reused "Sona Nahi Naa Sahi" from One 2 Ka 4
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 