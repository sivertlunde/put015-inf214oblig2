Forgiveness (2008 film)
{{Infobox film
| name = Forgiveness
| image = Esthers_diary_cover_4.jpg
| alt = 
| caption = Film poster as Esthers Diary
| director = Mariusz Kotowski
| producer = Gilda Longoria Mariusz Kotowski
| story = Mariusz Kotowski
| screenplay = Allan Knee
| starring = Juli Erickson Shelley Calene-Black Sydney Barrosse Jaime Goodwin
| music = Federico Chavez-Blanc
| cinematography = James Rodriguez
| editing = Brian ONeill
| studio = Bright Shining City Productions
| distributor = 
| released =  
| runtime = 84 minutes
| country = United States
| language = English
| budget = 
| gross = 
}} American dramatic dramatic Holocaust Holocaust film feature length film. 

==Plot==
A Roman Catholic child Apollonia Kowalski (Juli Erickson) and a Jewish child Esther Blumenfeld (Dell Aldrich), were childhood best friends in 1940s Poland. The two girls were separated when Esther was taken away to a Nazi concentration camp. When the war ended, both girls separately emigrated to the United States with their families. They remained separated thereafter. 

Years later, Apollonias daughter Maria Patterson (Shelley Calene-Black) and Esthers daughter Sarah Blumenfeld (Sydney Barrosse), have become successful professional women, but each still deals with the memories of the Holocaust via strained relationships with their respective mothers. Esther dies but has left a memoir of her experiences in the camp with her daughter, Sarah. Apollonia is confined to nursing home and though Maria tries her best to care or her, she and her mother are at constant odds. A secret that Apollonia has been hiding comes to light, and the uncovering of this secret causes Apollonia and Maria to resolve their differences and brings Maria and Sarah into each others lives.

==Partial cast==
 
* Shelley Calene-Black as Maria
* Juli Erickson as Apollonia
* Sydney Barrosse as Sarah
* Jamie Goodwin as Jeff Patterson
* Wilbur Penn as Clayton
* Dell Aldrich as Esther (voice)
* Christina Childress as Jennifer
* Jon Davis as Roger (voice)
* Greg Dorchak as Pete
* Chris Doubeck as Paul
* Sean Elliot as Matthew
* Krystal Morton as Young Esther
* Kathryn Olsen as Young Apollonia
* Steve Uzzell as Dean Denson
* Cyndi Williams as Lisa (voice)
 

==Background== Michael Kenna. French short Night And Fog. 

Forgiveness was shot on location in Austin, Texas in August 2007, with the director having to deal with seasonal rains and the illness of one of the lead actresses. 

==Release== Mariusz Kotowskis production company Bright Shining City Productions released Esthers Diary on DVD in September 2010; the film is currently being sold direct via Bright Shining City Productions official website.

===Awards===

====As Forgiveness====
 Bayou City Inspirational Film Festival (2008) 
* Telly Awards - Silver (2008)  Silver Remi Award - WorldFest (2009) 
* EMPixx Awards - Platinum (2009) 

====As Esthers Diary====

* Best Narrative Feature - Great Lakes International Film Festival (2010)
* Relix Award - Glen Rose Neo-Relix Film Festival (2010)
* EMPixx Award - Platinum (2010)
* The Indie Fest - Award of Merit (2010)  
* Telly Awards - Bronze (2010) 

==Reception==
Jerusalem Post wrote "Forgiveness is like a complex jigsaw puzzle or murder mystery in which apparently disconnected pieces fall into place only gradually. For those looking for a movie dominated by strong, independent women, this is the one to see."  
 Jewish Journal Polish filmmaker and reflecting the memories of wartime Poland, it was filmed with an American cast and set in a contemporary American city. It was offered that the film had a compelling plot line, but its strength was in how it dealt with the relationships of its female characters. They summarize, "A bond between two adult, self-confident women has rarely been portrayed as effectively." 

==See also== List of Holocaust Films

 

==References==
{{reflist|refs=

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

}}

==External links==
*  
*  

 
 
 
 
 
 
 
 
 