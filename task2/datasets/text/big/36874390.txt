Play It as It Lays (film)
{{Infobox film
| name           = Play It as It Lays
| image          =
| image_size     = 
| alt            = 
| caption        =
| director       = Frank Perry
| producer       = Dominick Dunne Paul Newman
| writer         = Screenplay: Joan Didion John Gregory Dunne Play It as It Lays|Novel: Joan Didion
| narrator       = 
| starring       = Tuesday Weld Anthony Perkins Adam Roarke Tammy Grimes
| music          = Don Fendley
| cinematography = Jordan Cronenweth
| editing        = Sidney Katz
| studio         = 
| distributor    = Universal Studios
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = $1 million 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} novel of Pretty Poison.

==Plot==
Maria Wyeth-Lang (Weld), an unhappily married ex-model and B-movie actress, and her only friend, B.Z. (Perkins), a gay movie producer, contemplate suicide.

==Cast==
*Tuesday Weld ..... Maria Wyeth-Lang 
*Anthony Perkins ..... B.Z.
*Adam Roarke ..... Carter Lang
*Tammy Grimes ..... Helene
*Richard Anderson ..... Les Goodwin
*Diana Ewing ..... Susannah
*Severn Darden ..... Hypnotist
*Tyne Daly ..... Journalist
*Jennifer C. Lesko  ..... Kate Wyeth-Lang

==Awards and nominations== The Emigrants.

==Critical reception==
Roger Ebert of the Chicago Sun-Times gave the film four stars. Positive remarks were also expressed for the two leads performances. Ebert cited, "What makes the movie work so well on this difficult ground is, happily, easy to say: It has been well-written and directed, and Tuesday Weld and Anthony Perkins are perfectly cast as Maria and her friend B.Z. The material is so thin (and has to be) that the actors have to bring the human texture along with them. They do, and they make us care about characters who have given up caring for themselves." 

Molly Haskell of The Village Voice was less enthusiastic, stating that she had "a hard time remembering  ". 

Vincent Canby of The New York Times found the screenplay and direction "banal", but effused praise for the performances of Weld and Perkins. "The film is beautifully performed by Tuesday Weld as Maria and Anthony Perkins as B.Z., but the whole thing has turned soft," Canby writes.  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 