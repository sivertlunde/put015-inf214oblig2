Anbe Aaruyire (1975 film)
{{Infobox film
| name           =  Anbe Aaruyire
| image          = Anbe_Aruyurie.jpg
| caption        = Official DVD Box Cover
| director       = A. C. Tirulokchandar
| producer       = R. Venkatraman
| writer         = A. L. Narayanan
| screenplay     = A. C. Tirulokchandar Manjula Nagesh Manorama Sukumari
| music          = M. S. Viswanathan
| cinematography = M. Viswanath Rai
| editing        = B. Kandaswamy
| studio         = Amutham Producions
| distributor    = Amutham Producions
| released       =  
| runtime        = 133 mins
| country        =   India Tamil
| budget =
}}
 1975 Tamil Manjula in lead roles. The film, produced by R. Venkatraman under Amutham Pictures, had musical score by M. S. Viswanathan and was released on September 26, 1975. 
The film was super hit at box-office.  The film is remake of 1967 Telugu film Grihalakshmi, starring Akkineni Nageswara Rao and P. Bhanumathi.

==Plot==
R. Sattanathan (Major Sundarrajan) a leading criminal lawyer and Bangalore Ramaswamy (V. K. Ramasamy (actor) |V. K. Ramasamy), Sattanathans professional competitor were once friends but now foes. Sattanathan has vowed that he will win a case against Ramaswamy at least once in his lifetime, for which he even makes his son Saravanan (Sivaji Ganesan) study law, so that if not him at least his son will defeat Ramaswamy. But contrary to Sattanathans expectation Saravanan falls in love with Ramaswamys daughter Devi (Manjula Vijayakumar |Manjula). Helping them in their love is Ramesh (Nagesh) Ramaswamys nephew. A lot of hilarious incidents and confusions follow before the lovers are united.

==Cast==
* Sivaji Ganesan Manjula
* Nagesh
* Major Sundarrajan
* V. K. Ramasamy (actor) |V. K. Ramasamy
* K. A. Thangavelu
* Suruli Rajan Manorama
* Sukumari
* Kanthimathi
* Pushpamala
* Venniradai Moorthy
* Y. G. Mahendra
* T. S. B. K. Moulee

==Soundtrack==
The music composed by M. S. Viswanathan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vaali (poet) Vaali || 03:04
|-
| 2 || Oosai Varamal || T. M. Soundararajan, P. Susheela || 03:59
|-
| 3 || Pattanathu Mappillaikku || T. M. Soundararajan, L. R. Eswari || 04:32
|-
| 4 || Raajaveethi Bhavanivanthathu || T. M. Soundararajan, P. Susheela, Saibaba, L. R. Anjali || 05:54
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 