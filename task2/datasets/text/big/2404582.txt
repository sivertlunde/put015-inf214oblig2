Gabriel Over the White House
{{Infobox film
| name           = Gabriel Over the White House
| image          = 
| image_size     = 
| caption        = 
| director       = Gregory La Cava
| producer       = Walter Wanger William Randolph Hearst Carey Wilson Bertram Bloch
| starring       = Walter Huston Karen Morley Franchot Tone
| music          = William Axt
| cinematography = Bert Glennon
| editing        = Basil Wrangell
| distributor    = Metro-Goldwyn Mayer
| released       = March 31, 1933
| runtime        = 86 minutes
| country        = United States English
| budget         = $232,400 Bernstein p434  gross = 
| preceded_by    = 
| followed_by    = 
}}
Gabriel Over the White House is a 1933 American   in tone (albeit veering toward National Socialism)"  and which "posits a favorable view of fascism."   
 Carey Wilson based upon the novel Rinehard by Thomas Frederic Tweed, who did not receive screen credit, and received the financial backing and creative input of William Randolph Hearst.
 David Landau.

==Plot== FDR lookalike")    miraculously recovers, emerging "a changed man, an activist politician, a Franklin Roosevelt|Roosevelt." 
 Congress impeaches him, he responds by dissolving the legislative branch, assuming the “temporary” power to make laws as he "transforms himself into an all-powerful dictator."  He orders the formation of a new “Army of Construction” answerable only to him, spends billions on one New Deal&ndash;like program after another, and nationalizes the manufacture and sale of alcohol. 
 Hitler or Chairman Mao," by firing squad." Alter, 6  By threatening world war with America’s newest and most deadly secret weapon, Hammond then blackmails the world into disarmament, ushering in global peace. 

The film is unique in that, by revoking the Constitution, etc., President Hammond does not become a villain, but a hero who "solves all of the nations problems,"  "bringing peace to the country and the world,"  and is universally acclaimed “one of the greatest presidents who ever lived.”  The Library of Congress comments:
 

==Context and analysis== Liberal Prime Prime Minister United Kingdom. liberal Democratic Wanger secured Democratic presidential Republican administrations. 
 MGM synopsis radical to the nth degree,"  studio boss Louis B. Mayer "learned only when he attended the Glendale, California preview that Hammond gradually turns America into a dictatorship," writes film historian  Leonard J. Leff.  "Mayer was furious, telling his lieutenant, Put that picture back in its can, take it back to the studio, and lock it up!"   
 First Lady Eleanor Roosevelt wrote that "if a million unemployed marched on Washington... Id do what the President does in the picture!"  Alter comments:
 
In the crisis of the   Eleanor Roosevelt “lamented that the nation lacked a benevolent dictator to force through reforms."  Influential  columnist Walter Lippmann told Roosevelt, "The situation is critical, Franklin. You may have no alternative but to assume dictatorial powers";  in his column, Lippmann wrote, "The more one considers the scope and the variety of the measures that are needed for relief and reconstruction the more evident it is that an extraordinary procedure&mdash;dictatorial powers, if that is the name for it&mdash;is essential..."  In his inaugural address, FDR said:
 
The American people, he added, "have asked for discipline and direction under leadership. They have made me the present instrument of their wishes. In the spirit of the gift I take it." 

The New York Herald Tribune welcomed FDRs inauguration with the headline "FOR DICTATORSHIP IF NECESSARY."  FDR economic adviser Herbert Feis commented, "The outside public seems to believe as if Angel Gabriel had come to earth." 

The film was released in Britain, but was not a commercial success. Newsreel film of the Royal Navy was spliced into the yacht sequence in the British version, implying that both Britain and the United States were co-operating to obtain disarmament.

The movie made a net profit of $206,000. 

Following the election of Barack Obama, People (magazine)|People magazine film critic Leah Rozen included Gabriel over the White House as one of "five films you should absolutely see before inauguration day." Asked "Why is this a film we have to see before Obama comes into the White House?" Rozen said "it couldnt be more timely... its at a time of economic panic, huge financial disaster... You kind of go, Gee, did they just write this now?..." 

==Cast==
*Walter Huston	 ...	President Judson Hammond
*Karen Morley	 ...	Pendola Molloy
*Franchot Tone	 ...	Hartley Beekman - Secretary to the President 
*Arthur Byron	 ...	Jasper Brooks - Secretary of State Dickie Moore	 ...	Jimmy Vetter
*C. Henry Gordon	 ...	Nick Diamond David Landau	 ...	John Bronson
*Samuel S. Hinds	 ...	Dr. Eastman (billed as Samuel Hinds)
*William Pawley	 ...	Borell
*Jean Parker	 ...	Alice Bronson
*Claire Du Brey	 ...	Nurse (billed as Claire DuBrey)

 

Cast notes:
* Thomas A. Curran the early American silent film star plays an uncredited bit part.

==Notes==
 

==References==
* 
* 
* 
* 

== External links ==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 