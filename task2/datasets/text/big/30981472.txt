Kunwara
{{Infobox film
| name = Kunwara
| image =Kunwara.jpg
| image_size =
| caption = 
| director = David Dhawan
| producer = Allu Aravind Ramesh S. Taurani Mukesh Udeshi
| writer = Rumi Jaffery Nagendra Babu Yunus Sajawal
| narrator =
| starring =Govinda (actor)|Govinda,  Urmila Matondkar
| music = Aadesh Shrivastava
| cinematography = Chota K. Naidu
| editing = Sachin Adurkar K. Raul Kumar
| distributor =Geetha Arts
| released =14 July 2000
| runtime = 134 min
| country = India
| language = Hindi
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}

Kunwara is a Bollywood comedy film directed by David Dhawan, released on 14 July 2000. The film stars Govinda (actor)|Govinda, Urmila Matondkar, Nagma, Om Puri and Johnny Lever in lead roles. The film is a remake of Telugu film Bavagaru Bagunnara?

==Plot==
Raju (Govinda) and Urmila (Urmila Matondkar) meet in New Zealand and fall in love. They plan to meet again very soon in India. On the way, Raju meets a woman named Sharmila (Naghma), a heartbroken, suicidal pregnant woman who tells Raju of her cruel boyfriend. In order to save her respect, Raju decides to play her husband and they return together Sharmilas home, where he meets her family. However, soon enough, Raju finds out that Urmila is none other than Sharmilas sister.

==Cast== Govinda ... Rajoo  
* Urmila Matondkar ... Urmilla Singh  
* Nagma ... Sharmila Singh 
* Kader Khan as Vishwanath Pratap Singh
* Om Puri ... Balraj Singh  
* Johnny Lever ... Gopal (as Jhony Lever)  
* Smita Jaykar ... Balrajs Wife  
* Mohan Joshi ... Inders Dad  
* Raza Murad ... Thakur (Ajays dad)

==Reception and awards==
Kunwara received mostly negative reviews from critics, although some of the performances were received well. Sukanya Verma from Rediff.com wrote, "Dont ask any questions. Dont look for any kind of logic. Go with the flow. Otherwise, youre not ready for this David Dhawan-Govinda flick."  Taran Adarsh of Bollywood Hungama criticised the film, concluding, "Kunwara has the super-successful David Dhawan-Govinda combination as its strong point, but the film pales in comparison to the hits the two have delivered earlier. The film may start off well, but it lacks a good script to sustain after the initial curiosity subsides." 

Kunwara was nominated for two awards in the same category at the annual Filmfare Awards:
*Filmfare Award for Best Performance in a Comic Role - Govinda
*Filmfare Award for Best Performance in a Comic Role - Johnny Lever

== Music ==
{{Infobox album |  
| Name = Kunwara
| Type = Album
| Artist = Aadesh Shrivastava
| Cover =
| Released =   2000 (India)
| Recorded = Feature film soundtrack
| Length =
| Label =  Tips Music Films
| Producer = Aadesh Shrivastava
| Reviews =
| Last album = 
| This album = Kunwara (2000)
| Next album = 
}}
 Sameer

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#ccc; text-align:center;"
! # !! Song !! Singer(s) !! Length 
|-
| 1
|"Jab Ladka Ho Kunwara"
| Sonu Nigam, Alka Yagnik
| 04:43
|-
| 2
| "Main Kunwara Aa Gaya" Sonu Nigam
| 03:24
|-
| 3
| "Mehndi Lagake" Kumar Sanu, Alka Yagnik
| 05:55
|-
| 4
| "Meri Chamak Chalo" Sonu Nigam, Alka Yagnik
| 05:10
|-
| 5
| "Na Heera Na Moti" Sonu Nigam, Hema Sardesai
| 05:00
|-
| 6
| "Sun Mere Sasure Main" Vinod Rathod, Sonu Nigam
| 03:33
|-
| 7
| "Urmila Re Urmila" Sonu Nigam, Alka Yagnik
| 04:50
|-
| 8
| "Yeh Ladki Jawaan" Kumar Sanu, Alka Yagnik
| 04:38
|}

==References==
 

==External links==
* 
* 

 

 
 
 
 
 