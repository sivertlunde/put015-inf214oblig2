Romance on the Range (film)
{{Infobox film
| name           = Romance on the Range
| image          = Romance on the Range 1942 Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Joseph Kane
| producer       = Joseph Kane
| screenplay     = J. Benton Cheney
| starring       = {{Plainlist|
* Roy Rogers
* George "Gabby" Hayes
* Sally Payne
* Linda Hayes
* Sons of the Pioneers 
}}
| music          = Cy Feuer
| cinematography = William Nobles
| editing        = Lester Orlebeck
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film directed by Joseph Kane and starring Roy Rogers, George "Gabby" Hayes, Sally Payne, Linda Hayes, and Sons of the Pioneers.   

==Cast==
* Roy Rogers as Roy Rogers
* George "Gabby" Hayes as Gabby
* Sally Payne as Sally
* Linda Hayes as Joan Stuart
* Edward Pawley as Jerome Banning
* Harry Woods as Henchman Steve
* Hal Taliaferro as Sheriff Wilson
* Glenn Strange as Stokes, Henchman
* Roy Barcroft as Pete, Henchman
* Sons of the Pioneers as Musicians, cowhands

==Reference==
 

==External links==
*  

 

 
 
 
 
 
 
 


 