Of Freaks and Men
 
{{Infobox film
| name = Of Freaks and Men
| image =
| caption =
| director = Alexei Balabanov
| producer = Sergei Selyanov
| writer = Alexei Balabanov
| narrator =
| starring = Sergei Makovetsky Dinara Drukarova Viktor Sukhorukov Lika Nevolina Alyosha De Chingiz Tsydendambayev
| music =
| cinematography = Sergei Astakhov
| editing = Marina Lipartia
| distributor =
| released =
| runtime = 89 minutes
| country = Russia
| language = Russian
| budget =
| preceded_by =
| followed_by =
}}
Of Freaks and Men ( ; Pro urodov i lyudey) is a 1998 Russian film directed by Alexei Balabanov.

==Premise==
Filmed initially in black and white, then entirely in sepia tone, this film set in turn of the century Russia is centered on two families and their decline at the hands of one man, Johann, and his pornographic endeavours. Hailed by some as a masterpiece, the movie comments on the decline of Russian society as a result of the rise of capitalism.

==Soundtrack==
  Romeo and Juliet; and Modest Mussorgsky|Mussorgskys Pictures at an Exhibition.

== See also ==
*List of post-1960s films in black-and-white
*Sadism and masochism in fiction

==External links==
*  

 
 
 
 
 
 
 
 
 


 
 