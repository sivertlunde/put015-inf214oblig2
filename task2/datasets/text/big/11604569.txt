I Live My Life
{{Infobox film
| name           = I Live My Life
| image          = Posterilivemylife.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = W.S. Van Dyke
| producer       = Bernard H. Hyman
| writer         = 
| screenplay     = Joseph L. Mankiewicz
| story          = 
| based on       =  
| narrator       = 
| starring       = Joan Crawford Brian Aherne Frank Morgan Aline MacMahon
| music          = Dimitri Tiomkin
| cinematography = George J. Folsey
| editing        = Tom Held
| studio         = Metro-Goldwyn-Mayer
| distributor    = Loews Cineplex Entertainment|Loews
| released       =  
| runtime        = 97 min.
| country        = United States
| language       = English
| budget         = $586,000  . 
| gross          = $1,478,000 
}}

I Live My Life is a 1935 American comedy-drama film, starring Joan Crawford, Brian Aherne, and Frank Morgan, and is based on the story "Claustrophobia" by A. Carter Goodloe.

==Plot summary==
Kay Bentley (Joan Crawford), a bored socialite seeks a more fulfilling life and goes on a Greek holiday. While on vacation, Kay falls for Terry ONeill (Brian Aherne), an archaeologist who challenges Kays beliefs, yet, also falls for her enough to follow her home. He feels awkward in Kays flighty, social circles, yet they become engaged to marry. As Kay and Terry continue to quarrel over their differing lifestyles. Kay and Terry reach a compromise and do marry in the end.

==Cast==
* Joan Crawford as Kay Bentley
* Brian Aherne as Terence Terry ONeill
* Frank Morgan as G.P. Bentley
* Aline MacMahon as Betty Collins
* Eric Blore as Grove, Bentleys Butler
* Fred Keating as Gene Piper
* Jessie Ralph as Kays Grandmother
* Arthur Treacher as Gallup, Mrs. Gages Butler Frank Conroy as Doctor
* Etienne Girardot as Professor
* Esther Dale as Brumbaugh, Mrs. Gages Housekeeper
* Hale Hamilton as Uncle Carl
* Hilda Vaughn as Miss Ann Morrison
* Frank Shields as Outer Office Secretary
* Sterling Holloway as Max

===Box office===
According to MGM records the film earned $921,000 in the US and Canada and $557,000 elsewhere resulting in a profit of $384,000. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 