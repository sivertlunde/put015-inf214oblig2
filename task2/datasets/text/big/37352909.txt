Moonlight and Honeysuckle
{{infobox film
| name           = Moonlight and Honeysuckle
| image          =Moonlight and Honeysuckle (1921) - Louis & Minter.jpg
| imagesize      =200px
| caption        = left to right: unnamed player, Willard Louis and Mary Miles Minter
| director       = Joseph Henabery
| based on       =  
| writer         = Barbara Kent (scenario)
| starring       = Mary Miles Minter
| music          =
| cinematography = Faxon M. Dean
| editing        =
| distributor    = Paramount Pictures
| released       = July 1921 (United States)
| runtime        = 50 mins.
| country        = United States
| language       = Silent (English intertitles)
}}
 silent romantic comedy film produced and released by the Realart Company. It is based on a 1919 Broadway play, Moonlight and Honeysuckle, by George Scarborough and starring Ruth Chatterton. The film version starred Mary Miles Minter in the Chatterton role and was by Joseph Henabery.   

Moonlight and Honeysuckle is now considered to be a lost film. 

==Cast==
* Mary Miles Minter - Judith Baldwin
* Monte Blue - Ted Musgrove
* Willard Louis - Senator Baldwin
* Grace Goodall - Hallie Baldwin
* Guy Oliver - Congressman Hamil William Boyd - Robert V. Courtney
* Mabel Van Buren - Mrs. Langley
* Justine Johnstone - Bit part (Uncredited)

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 
 


 
 