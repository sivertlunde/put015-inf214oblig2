Down on the Farm (1920 film)
{{infobox film
| name           = Down on the Farm
| image          = Down on the Farm by Mack Sennett Film Daily 1920.png
| imagesize      = 180px
| caption        = Ad for film
| director       = Erle C. Kenton Ray Grey F. Richard Jones
| producer       = Mack Sennett
| writer         = Ray Grey Raymond Griffith Mack Sennett
| cinematography = Fred Jackman Perry Evans
| editing        =
| distributor    = United Artists
| released       = April 25, 1920
| runtime        = 50 minutes; 5 reels 
| country        = United States Silent (English intertitles)
}}
Down on the Farm is a 1920 silent film feature length rural comedy produced by Mack Sennett and starring Louise Fazenda and Harry Gribbon.  

Copies survive at both the Library of Congress and reputedly Gosfilmofond, Russian State Archive. 

==Cast==
*Louise Fazenda - Louise, The Farmers Daughter
*Harry Gribbon - The Rustic Sweetheart
*Bert Roach - Roach, The Farmer James Finlayson - The Sportive Banker with the Mortgage
*Billy Armstrong - The Man of Mystery
*Don Marion - The Baby(*billed John Henry, Jr.)
*Marie Prevost - The Faithful Wife
*Ben Turpin - The Faithful Wifes Husband
*Dave Anderson - Grocery Man
*Joseph Belmont - The Minister
*Eddie Gribbon - Bankers Henchman
*Kalla Pasha - Mailman
*Fanny Kelly - Gossipy Villager
*Sybil Seely - Maid of Honor (*billed Sibye Trevilla)

uncredited
*Jane Allen 
*Thelma Bates
*Pepper The Cat - Herself
*Teddy The Dog - Himself
*Elva Diltz
*Frank Earle
*Virginia Fox
*George Gray
*Harriet Hammond - Herself, Prologue
*Phyllis Haver - Herself, Prologue
*Mildred June 
*Patrick Kelly - Villager
*Larry Lyndon - Villager
*Kathryn McGuire - Villager
*John Rand - Villager
*Eva Thatcher - Villager

==References==
 

==External links==
 
*  
* 

 

 
 
 
 
 
 
 


 