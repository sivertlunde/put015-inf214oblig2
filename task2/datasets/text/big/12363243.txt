Hearts of Youth
{{Infobox film
| name           = Hearts of Youth
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Tom Miranda Millard Webb    
| producer       = 
| writer         = 
| screenplay     = Millard Webb  
| story          = 
| based on       = Ishmael (novel) E. D. E. N. Southworth Harold Goodwin Lillian Hall
| narrator       = 
| music          = 
| cinematography = Walter Williams  
| editing        =  Fox Film Corporation 
| distributor    = Fox Film Corporation 
| released       = May 1921
| runtime        = 
| country        = United States Silent
| budget         = 
| gross          = 
}} Harold Goodwin, Colin Kenny Fox Film Corporation     

==Plot==
Ishmael Worth renounces his young sweetheart, Beatrice, because he believes himself to be illegitimate and does not want to bring shame to her. Later it is revealed that his mother and father  had  married. His fathers previous wife, thought to be dead, turns up to confront him; but the fact that the first wife was a bigamist makes her marriage to Ishmaels father null and void and the marriage between his mother and father therefore valid. Ishmael, having a legitimate father, now can give Beatrice an honest name. 

==Cast==
* Iris Ashton &mdash; Mrs. Grey
* Glen Cavender &mdash; Reuben Grey George Fisher &mdash; Herman Brudenell
* Grace Goodall &mdash; Countess Hurstmonceaus Harold Goodwin &mdash; Ishmael Worth
* Lillian Hall &mdash; Beatrice Merlin Colin Kenny &mdash; Lord Vincent
* Fred Kirby &mdash; Judge Merlin

==References==
 

==External links==
* 
* 
* 
 
 
 
 
 
 


 