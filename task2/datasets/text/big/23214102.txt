Iqraar by Chance
 
{{Infobox film
| name           = Iqraar by Chance
| image          = Iqraar by Chance.png
| alt            =  
| caption        = Movie Poster
| director       = K. Ravi Shankar
| producer       = Reeta J Shukla
| writer         = Rajeev Agarwal Arbaaz Khan
| music          = Sunidhi Chauhan Shreya Ghosal Sonu Nigam Kunal Ganjawala Udit Narayan
| cinematography = Neelabh Kaul
| editing        = Sanjay Verma
| studio         = 
| distributor    = 
| released       =  
| runtime        = 129 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Iqraar By Chance is a 2006 Bollywood film directed by K. Ravi Shankar and starring Shilpa Anand and Amarjeet Shukla.  

==Plot==
Born to East Indian parents, Rashmi Mehra(Shilpa Anand) lives a wealthy and care-free life in England, often overspends money, leading her dad to question her closely. On one such occasion, she manages to convince him that she can earn as much as  £5000 in one month. He accepts her challenge and she sets out to find work.

She soon finds out that it is not easy to get any job, especially without using her dads name & any job skills. She does manage to convince the owner of Suno FM Radio to let her con a young man in falling in love with her, while she ditches him on a reality show.
 Arbaaz Khan) and kidnaps Rashmi mistaken as his bride.

==Cast==
* Shilpa Anand... Rashmi Mehra
* Amarjeet Shukla...... Raj Arbaaz Khan... CBI Officer R.B. Mathur
* Rahul Dev	... Sikka
* Deepa Bakshi... Tina Talwar
* Narendra Bedi... Constable Dildaar Singh
* Kurush Deboo... Detective Dcosta
* Aslam Khan... Sanju (as Aslam Ahmed Khan)
* Manoj Pahwa... Talwar - club owner
* Upasna Singh... Kalawati Kal Talwar
* Tiku Talsania... Radio Channel Owner
* Richa Varma... Sonia

==Release==
The Film was released on 6 October 2006. DVD was released by EROS Entertainment on 27 March 2007. 

==References==
 

==External links==
*  
*  
*   at Bollywood Hungama

 

 
 
 
 
 