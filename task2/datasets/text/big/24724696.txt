Replay (2001 film)
 
{{Infobox film
| name           = Replay
| image          = La répétition 2001.jpg
| caption        = Film poster
| director       = Catherine Corsini Philippe Martin
| writer         = Catherine Corsini Pascale Breton Pierre-Erwan Guillaume Marc Syrigas
| starring       = Emmanuelle Béart Pascale Bussières
| music          = 
| cinematography = Agnès Godard
| editing        = Sabine Mamou
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = France
| language       = French
| budget         = 
}}

Replay ( ) is a 2001 French drama film directed by Catherine Corsini. It was entered into the 2001 Cannes Film Festival.   

==Cast==
* Emmanuelle Béart - Nathalie
* Pascale Bussières - Louise
* Dani Levy - Matthias (as Dani Lévy)
* Jean-Pierre Kalfon - Walter Amar
* Sami Bouajila - Nicolas
* Marilu Marini - Mathilde
* Clément Hervieu-Léger - Sacha
* Marc Ponette - Alain
* Raphaël Neal - Patrick
* Sébastien Gorteau - Jean-Philippe
* Vincent Macaigne - Henri
* Jeanne David - Colette
* Daniel Isoppo - Maurice
* Marie-Charlotte Dutot - Louise (11 ans)
* Marie Loboda - Nathalie 11 ans

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 
 