Kali Basti
{{Infobox film
| name           = Kali Basti
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Sudesh Issar
| producer       = Satish Khanna	
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Shatrughan Sinha Reena Roy
| music          = Laxmikant-Pyarelal
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by Sudesh Issar and produced by Satish Khanna. It stars Shatrughan Sinha and Reena Roy in pivotal roles. 

==Cast==
* Shatrughan Sinha...Karan Singh
* Reena Roy...Lajjo - Gangarams sister
* Vijayendra Ghatge...Inspector Raghuvanshi
* Prem Chopra...Kuber Nath
* Brahmachari...Gangaram
* Raj Kishore...Havaldar
* Om Prakash...Peter Pereira
* Rajni Sharma...Rakhi Singh

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Kabhi Bole Iaan"
| Suresh Wadkar, Asha Bhosle 
|-
| 2
| "Kis Viad Ko Nabs"
| Mohammed Rafi, Anuradha Paudwal
|-
| 3
| "Aaj Peete Hain"
| Mahendra Kapoor, Manhar Udhas
|-
| 4
| "Aaj Peete Hain (Sad)"
| Mahendra Kapoor
|}

==References==
 

==External links==
* 

 
 
 

 