Traci Townsend
{{Infobox Film
| name           = Traci Townsend
| image          = Traci_Townsend_DVD.jpg
| caption        = DVD cover
| director       = Craig Ross, Jr.
| producer       = Bobby Thompson Cheryl Bedford Fred Lewis Mark Holdom
| writer         = Bobby Thompson
| narrator       =  Victor Williams Marlon Young
| music          = Geno Young
| cinematography = Carl Bartels
| editing        = Craig Ross, Jr.
| distributor    = Warner Home Video
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
}} directed by Craig Ross, Jr., which stars Jazsmin Lewis and Mari Morrow. It was written by Bobby Thompson, who was also a producer of the film.

==Plot==
A beautiful and successful journalist interviews her three previous boyfriends to find out why they never proposed. Each distinctly different interview comically teaches Traci more about herself than she would care to know.

==Cast==
*Jazsmin Lewis &mdash; Traci Townsend
*Mari Morrow &mdash; Sylvia
*Richard T. Jones &mdash; Travis
*Jay Acovone &mdash; Jesse "The Boss" Victor Williams &mdash; Darrell
*Suzanne Whang &mdash; Rosa
*Marlon Young &mdash; Pierre
*Amy Hunter &mdash; Vick
*Aaron D. Spears &mdash; Dante
*Myquan Jackson &mdash; Jay

==Accolades==
2006 Boston International Film Festival
*Best Acting Performance — Jazsmin Lewis (winner)
 2006 Hollywood Black Film Festival
*Audience Choice Award (winner)

==References==

 

==External links==
*  
*  

 
 
 
 
 
 