Diamonds (1920 film)
{{Infobox film
| name           = Diamonds
| image          = 
| image_size     = 
| caption        = 
| director       = Friedrich Feher 
| producer       = 
| writer         = Johannes Brandt   Robert Wiene
| narrator       =  Paul Morgan
| music          = 
| editing        = 
| cinematography = Eugen Hamm
| studio         = Ungo-Film
| distributor    = 
| released       = December 1920
| runtime        = 
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} German silent silent crime film directed by Friedrich Feher and starring Louis Ralph, Erika Glässner and Julius Brandt. A policeman goes undercover to unmask a customs officer as corrupt.  Fehers direction was criticised for being too loose. 

==Cast==
* Louis Ralph
* Erika Glässner 
* Julius Brandt Paul Morgan

==References==
 

==Bibliography==
* Jung, Uli & Schatzberg, Walter. Beyond Caligari: The Films of Robert Wiene. Berghahn Books, 1999.

 
 
 
 
 
 


 
 