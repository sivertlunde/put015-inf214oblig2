Vagdanam
{{Infobox film
| name           = Vagdanam
| image          = Vagdanam.jpg
| image_size     =
| caption        = Athreya
| Athreya D. Srirama Murthy K. Satyanarayana Athreya Bollimunta Sivaramakrishna
| narrator       = Krishna Kumari Girija Chalam Suryakantham
| music          = Pendyala Nageshwara Rao
| cinematography = P. L. Roy
| editing        =
| studio         =
| distributor    =
| released       = 1961
| runtime        = 150 minutes
| country        = India Telugu
| budget         =
}}
Vagdanam (  written, produced and directed by famous writer Acharya Atreya.  The story is loosely based on the Novel Datta by Bengali writer Saratchandra Chatterjee.

It is a musical hit film with some memorable songs including Velugu Choopavayya Madilo Kalatha Bapavayya, Naa Kantipapalo Nilachipora and Sreenagajaa Tanayam (Harikatha). The music score provided by Pendyala Nageswara Rao and songs voiced by Ghantasala Venkateswara Rao. But it is not commercially successful.

==Plot==
Vishwanatham (Nagayya), Ranganatham (Gummadi) and Jagannatham are childhood friends. Jaganatham marries a girl from lower caste and is cast out of the village. After the death of his wife, Jagannatham gets addicted to liquor. Vishwanatham helps him and sends money for the education of his son. Suryam (Akkineni Nageshwara Rao) is son of Jagannatham completed Medical education. Vishwanatham wants to give his daughter Vijaya (Krishna Kumari) to Suryam. But Ranganatham wants his son Chandram (Chalam) to marry Vijaya. Jagannatham dies accidentally falling from the stairs before his son returns. Vishwanatham dies of shock soon. Suryam returns to the village and started to provide medical services to the poor people suffering from Malaria. Radha (Girija) helps him as Nurse. After the death of Vishwanatham, Ranganatham creates differences between Suryam and Vijaya. How the differences get cleared with the intervention of Ramdas (Relangi) is rest of the story.

==Cast==
{| class="wikitable"
|-
! Actor !! Character
|-
| Akkineni Nageshwara Rao || Suryam, son of Jagannatham
|- Krishna Kumari || Vijaya, daughter of Vishwanatham
|-
| Gummadi Venkateswara Rao || Ranganatham
|-
| Chalam || Chandram, son of Ranganatham)
|-
| Chittor V. Nagaiah || Zamindar Vishwanatham
|-
| Relangi Venkataramaiah || Ramadasu
|- Suryakantham || Baalamani, wife of Ramadasu
|-
| B. Padmanabham || Padmanabham, son of Ramadasu
|- Girija || Radha, the Nurse
|-
| Surabhi Kamalabai || Bujjamma
|-
| Prabhavathi ||
|-
| Baby Swarnalatha ||
|}

==Crew==
* Director: Acharya Atreya
* Associate director: K. Raghavendra Rao
* Story:  Saratchandra Chatterjee (Novel "Datta")
* Dialogues: Acharya Atreya
* Screenplay : Acharya Atreya and Bollimunta Sivaramakrishna
* Producers: Acharya Atreya, D. Srirama Murthy and K. Satyanarayana
* Original Music: Pendyala Nageshwara Rao
* Cinematography: P. L. Roy and V. Roy
* Lyrics : Srirangam Srinivasa Rao, Dasaradhi Krishnamacharyulu, Narla Chiranjeevi, Acharya Atreya
* Playback singers: B. Vasantha, Pithapuram Nageswara Rao, Ghantasala Venkateswara Rao, P. Susheela, S. Janaki

==Soundtrack==
# Bangaru Naava Bratuku Bangaru Naava (Lyrics: Acharya Atreya; Singer: P. Susheela; Cast: Krishna Kumari, Girija and Akkineni)
# Kaasipatnam Chudarababu Kallakapatam Leni Gareebu (Lyrics: Srirangam Srinivasa Rao; Singers: Ghantasala and P. Susheela; Cast: Akkineni and Girija)
# Maa Kittaya Puttina Dinam (Lyrics: Acharya Atreya; Singers: Pithapuram Nageswara Rao, B. Vasanta, S.Janaki
# Naa Kanti Papalo Nilichipora Nee Venta Lokala Gelavaneera (Lyrics: Dasaradhi; Singers: Ghantasala and P. Susheela; Cast: Krishna Kumari and Akkineni)
# Tappatlo Talalo Devudi Gullo Bajalo (Lyrics: Narla Chiranjeevi; Singers: B. Vasantha, U. Sarojini and S. Janaki)
# Vanne Chinnelanni Vunna Chinnadanive (Lyrics: Acharya Atreya; Singer: Ghantasala; Cast: Akkineni and Krishna Kumari)
# Velugu Chupavaya Madilo Kalata Bapavayya (Lyrics: Acharya Atreya; Singers: Ghantasala and P. Susheela; Cast: Relangi and Krishna Kumari)
# Sri Nagaja Tanayam -  ; Singer: Ghantasala Venkateswara Rao; Cast: Relangi, Suryakantham and Padmanabham)

==References==
 

==External links==
*  

 
 
 
 