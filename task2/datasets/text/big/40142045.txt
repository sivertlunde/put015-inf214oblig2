The Inbetweeners 2
 
 
{{Infobox film
| name                 = The Inbetweeners 2
| image                = Inbetweeners_2_Movie_Poster.jpg
| alt                  = Four young men in summer clothes strolling casually together in a desert
| caption              = Theatrical release poster
| director             = {{Plainlist|
* Damon Beesley
* Iain Morris}}
| producer             = Spencer Millman 
| writer               = {{Plainlist|
* Damon Beesley
* Iain Morris}}
| starring             = {{Plainlist|
* Simon Bird James Buckley
* Blake Harrison Joe Thomas}}
| narrator             = Simon Bird 
| music                = {{Plainlist|
* David Arnold Michael Price}}
| cinematography       = Ben Wheeler
| editing              = William Webb
| production companies = {{Plainlist|
* Film4 Productions
* Bwark Productions}}
| distributor          = Entertainment Film Distributors
| released             =  
| runtime              = 96 minutes 
| country              = United Kingdom
| language             = English
| gross                = $63.8 million    
}} E4 sitcom The Inbetweeners. It was written and directed by series creators Damon Beesley and Iain Morris.
 Joe Thomas, James Buckley and Blake Harrison. In media interviews, the films writers and actors stated that it was to be an end to the series.

The Inbetweeners 2 was released on 6 August 2014 in the United Kingdom and the Republic of Ireland,    to positive reception from critics. It surpassed the record of its predecessor for the highest gross on the opening day of a comedy in the UK, with £2.75 million,    and ended its first weekend with a gross of £12.5 million, the largest opening of any film in 2014,    then remained on top for a second week.  With an overall gross of £33.3 million, it was the highest-grossing British film in the domestic market in 2014.    On 21 August, it was released in Australia, to a mixed reception, and topped the box office in its opening weekend.

==Plot==
  Neil and the last Simon is obsessive and verbal abuse|abusive. Simon and Will are depressed and ostracised at university; Neil is working in a bank; and Jay is taking a gap year in Australia. He emails Neil when he and Simon visit Will, claiming that he is now a top DJ, owns a popular night club, lives in a luxury mansion, and has daily sex with multiple partners, convincing them to visit him in Australia for their Easter holidays.

Once they arrive at the club they find that Jay in fact only works as a toilet attendant. While at the club, Will meets Katie, an old friend from his private school days, who is Backpacking (travel)|backpacking, and she convinces him to join her. It also emerges that Jay actually lives in a tent in the front garden of his uncle Bryans house. Whilst there, Simon attempts to break-up with Lucy via a Skype call, but, to his horror, Bryan interferes and tricks Lucy into thinking Simon asked her to marry him, to which she accepts.

 , along with the backpackers]]
The next day, the four travel to a youth hostel in Byron Bay in a Peter Andre-themed car, as Will wants to follow Katie there. He attempts to become friends with one of the backpackers, Ben, but is unsuccessful when the backpackers quickly deduce that he is a tourist, rather than a "traveller". After arriving, Will Skypes his mother and notices that someone else is staying at their house, which she denies and subsequently hangs up.

The following day, the boys and the backpackers visit a water park, where Jay intends to find his ex-girlfriend, Jane. The park staff tell Jay that Jane has found a new job in the vast outback, but they dont know specifically where he can find her. Lucy tells Simon via Skype that Jane is working on a stud farm in the remote settlement of Birdsville, and the boys intend to drive there. Will, however, decides to follow Katie instead. He struggles to fit in with the spiritual activities of the travellers and discovers that Katie is seeing multiple people at once, launching him into one of his foul-mouthed tirades towards them, which leads Katie into having sex with Ben.
 drinking Neils urine, but Neil is too dehydration|dehydrated. The group hold hands together as they realise that they will most likely die, but are rescued by Jane and her co-workers. It is revealed they have only been stranded for two hours. Jane realises how far Jay came to win her over again, and although she is touched by the gesture, she does not take him back.

Back at Jays uncles house, the boys find that their parents have travelled to find them after hearing of their near-death experience. The boys are shocked to find Mr Gilbert (their old head of sixth form and Wills nemesis) there too, and Wills mother announces that she and Gilbert are in a relationship, to the abject horror of Will and amusement of the other three friends. Meanwhile, Jay finally shows some backbone and slaps his uncle for insulting Janes weight, instigating a fight between his uncle and father. Over Skype, Lucy breaks-up with Simon because she is now in a relationship with his university best friend Pete, which Simon responds to by cheering and abruptly hanging-up on her.

The four boys decide to travel to Vietnam in a montage during the films credits. As they return to England, Neil begins a relationship with one of the travellers while Wills mother reveals that she and Gilbert are engaged. Will attempts to run back to the plane, but is wrestled to the ground by security.

==Cast== James Buckley and Blake Harrison]]
 
* Simon Bird as Will McKenzie    James Buckley as Jay Cartwright 
* Blake Harrison as Neil Sutherland  Joe Thomas Simon Cooper 
* Emily Berrington as Katie Evans
* Belinda Stewart-Wilson as Polly McKenzie 
* Tamla Kari as Lucy
* Freddie Stroma as Ben Thornton-Wild
* Lydia Rose Bewley as Jane David Schaal as Terry Cartwright
* Alex MacQueen as Kevin Sutherland
* Martin Trenaman as Alan Cooper
* Robin Weaver as Pamela Cooper
* Greg Davies as Mr. Phil Gilbert
* Oliver Johnstone as Kristian
* Susan Wokoma as Della
* Brad Kannegiesser as Jasper David Field as Uncle Bryan (uncredited) 
 

Daisy Ridleys scenes were cut from the final version.  

==Production==
===Origin=== Christopher Young openly recognised the possibility of another film based on the series, claiming that "if there is a sequel it will come from the creative elements&nbsp;... Weve talked about it. In the short term people are dispersing and doing other things but Im sure in the medium term a sequel is very possible. It wont be immediate but its definitely not closed."  Co-writer Damon Beesley later admitted "we didnt know how successful it would be and that it would have a life on screen. But they did translate to big-screen characters, people did care about them and did go back and see it more than once – and thats very rare in cinema. The idea of not following that up seemed insane to most people".    The actors had mixed emotions on making a sequel. Although Buckley and Thomas felt put off by the success of the first film, Harrison and Bird became convinced on reading the script. 
 Gold Coast, describing it as "a place where people go to get drunk, pick some fruit and get drunk again".   

===Development===
On 21 August 2012, it was announced that a sequel was in early stages of pre-production.  On 8 November, it was announced by series creators Morris and Beesley that a script was being written and it was at "version 0.5"  On 1 May 2013,  it was announced  that, although nothing had been signed, a sequel was in the planning stage possibly set in Australia and to be released sometime in 2014.  

On 2 August 2013, the sequel was officially confirmed for release in August 2014.   The series Facebook page revealed on 15 March 2014 that the sequel would be released on 6 August 2014. 
 Aborigine a "fire wanker".  A second trailer, this time full length, was released on The Inbetweeners official Facebook page on 18 June, showing more of the storyline. 

===Filming===
  settlement of Marree, South Australia ]]
Filming began in Australia on 7 December 2013,   before moving to the UK in January 2014.   Part of the film was shot in Marree, South Australia, an isolated Outback settlement without mobile reception or Internet.   
 Man Up, so Morris and Beesley directed The Inbetweeners 2.  Bird said that the actors were initially disheartened by the absence of Palmer, and nervous about the direction of Morris and Beesley due to their lack of experience in the position. 

Morris had considered filming the Australian scenes in South Africa due to the comparatively high costs in Australia, which despite a higher budget caused the sequel to have fewer resources than its predecessor.  Although all locations for the first film had been within 10 minutes of the hotel, locations in the second were separated by a three-hour flight and nine-hour car journey.  While filming in the Outback, the Australian crew provided two doctors and 40 litres of IV fluid, although the only point in which a doctor was called was when Buckley thought that he was having a heart attack, which was in reality indigestion from chips and lager.  Beesley considered it "the maniacs choice of a film to be your first film".  The water park scenes were filmed at WetnWild Gold Coast, which Thomas described as "some quite challenging scenes". 

When asked whether he ever felt averse to any material in the film due to perceived offence, Morris said that the crews attitude was “let’s shoot everything, push it, and then if it feels like too much when we’re watching it, we can always pull it back in the editing room”. 

==Release==
The film premiered at Leicester Square, London, on 5 August 2014. In attendance were guests including Beverley Knight and Union J.  In Australia it was distributed by Roadshow Entertainment  and released on 21 August.  The same company took the film to New Zealand a week later. 

===Box office===
The Inbetweeners 2 grossed £2.75 million on its opening day of 6 August 2014, surpassing its predecessor as the top grossing opening day in the UK for a comedy film.  By the end of its first weekend, it topped the UK box office with a gross of £12.5 million, surpassing   (£11.7 million) as the largest UK opening in 2014; this, however, was less than the £13.2 million opening of The Inbetweeners Movie in 2011.  It topped the box office for a second week, in which it grossed £9.83 million.    In its third week, it fell to second spot behind new release Lucy (2014 film)|Lucy.   
 Guardians of the Galaxy into second place.  It fell to second place in its second week, with Guardians of the Galaxy returning to top spot. 

As of 12 October 2014, the film had made $55,652,783 in the United Kingdom, $6,598,273 in Australia, and $473,316 in New Zealand. 

With an overall gross of £33.3 million, The Inbetweeners 2 was the highest grossing British or Irish film in the domestic market in 2014, ahead of  . 

===Critical reception===
Of the thirty-four reviews surveyed by review aggregator Rotten Tomatoes, 71% of reviews were positive.  On Metacritic, the film has a 55/100 rating based on 7 critics, indicating "mixed or average reviews". 

====United Kingdom====
{{ Album reviews
| rev1       = The Daily Telegraph
| rev1Score  =     
| rev2       = Digital Spy
| rev2Score  =     
| rev3       = The Guardian
| rev3Score  =     
| rev4       = Daily Mirror
| rev4Score  =     
| rev5       = Daily Express
| rev5Score  =     
| rev6       = Empire (magazine)|Empire
| rev6Score  =     
| rev7       = Total Film
| rev7Score  =   
| rev8 = The Observer
| rev8score =     
| rev9 = Time Out (magazine)|Time Out
| rev9score =     
}} The Fannytastic Four leave us on a poo-flecked, piss-soaked, sun-burned high that more than overcomes its familiar flaws to become a real contender for the year’s funniest film. Four star wankers". 
 The Cuban Four Stooges Carry On staple, horror of sex (especially among the over-25s)". He however predicted that on the record of the first film, The Inbetweeners 2 would be a financial success.  In Time Out (magazine)|Time Out, Tom Huddleston gave the film one star out of five, saying "‘The Inbetweeners 2’ is riddled with contempt: for its characters, for its audience and most notably for the entire female gender. That a film in 2014 can still get away with depicting all women as either dumb, hapless sluts or ball-busting harridans is frankly unbelievable." 

====Australia====
{{ Album reviews
| rev1       = 612 ABC Brisbane
| rev1Score  = B+ 
| rev2       = Urban Cinefile
| rev2Score  = Mixed 
| rev3       = Herald Sun
| rev3Score  =   
| rev4       = Quickflix
| rev4Score  =   
| rev5 = FILMINK
| rev5Score =     
| rev6 = The Standard
| rev6Score =     
| rev7 = Sydney Morning Herald
| rev7Score =     
}} invaded   shores". Although opining that the film was "slightly more insensitive to its female leads than previous efforts", he concluded that a scene in which faeces chase Will down a waterslide "challenges Caddyshack for the mantle of Best Ever S***  in Water gag".   

In the Herald Sun, Leigh Paatsch gave the film one star. He criticised the casting, describing the main characters as "supposed to be aged about 20   played by blokes who all look as if they’re 30-plus, and carry on as if they’re not yet 10", and also found the film misogynistic, saying "the derogatory manner in which women are spoken of (and often depicted) is relentlessly, callously crass. Sometimes even hateful".    A mixed review from Philippa Hawker of the Sydney Morning Herald concluded "The Australian elements seem hastily inserted and incidental: the movie could have been set in any country that had a water park and a place to get lost. But as a hymn to male bonding, and an exploration of the comic possibilities of what happens when a turd hits a water slide, The Inbetweeners 2 is a precisely crafted, assured piece of work". 

===Home media===
The Inbetweeners 2 was released on DVD and Blu-ray Disc|Blu-ray in the United Kingdom on 1 December 2014.  A DVD edition also including the first film was released at the same time. 
 audio commentaries — one with Morris and Beesley and the other by the four lead actors — in addition to a behind-the-scenes featurette, deleted scenes, and a blooper reel.  The films release on home media was sponsored by STA Travel, who offered a prize of a holiday to the Australian state of Queensland. 

==Legacy==
At its premiere on 5 August 2014, Bird said of the film: 

 

In an interview with the BBC, Thomas said that Morris and Beesley had been "very adamant" that the series had finished. On his co-stars, he added "There is a bond there that I think would be a stupid thing to waste. You dont get that bond very often with other performers and we do have it and its a valuable thing". 

In the same interview, when asked whether the series had finished, both co-creators answered with a simultaneous "Yes!". Morris expressed that "I think the time is right. After the first film, I wanted to hear more from Jay, Will, Neil and Simon. But this time I feel there is enough. There is more than enough Jay in this world", and Beesley added "The end of the story has always felt like the time where they go off and start living their adult life. And I think this film takes us up to that point". 

The Inbetweeners 2 was blamed for an increase in a craze known as "logging", deliberate defecation in swimming pools to distress other guests.  

==See also==
 
* List of films based on British sitcoms
* List of 2014 box office number-one films in the United Kingdom
* List of 2014 box office number-one films in Australia

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
   
 
 
 
 
 
 
 
 