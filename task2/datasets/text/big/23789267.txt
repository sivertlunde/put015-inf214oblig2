Ace of Hearts (2008 film)
{{Infobox film
| name           = Ace of Hearts
| image          = Ace-of-hearts-dvdcover.jpg
| caption        = DVD cover
| director       = David Mackay
| producer       = Randy Cheveldave
| writer         = Frederick Ayeroff
| starring       = Dean Cain Britt McKillip Mike Dopud Anne Marie Loder
| music          = Michael Richard Plowman
| cinematography = Gordon Verheul
| editing        = Charles Robichaud
| studio         = PeaceArch Entertainment Readers Digest|Readers Digest for Young Families
| distributor    = Fox Faith
| released       =  
| runtime        = 100 minutes
| country        = Canada United States
| language       = English
| budget         = 
| gross          = 
}}
 Canadian family drama film directed by David Mackay. It features Dean Cain, Britt McKillip, Anne Marie Loder, and Mike Dopud in the lead roles. 

==Plot==
The film is about a Police dog|K-9 dog named Ace. The police are trying to catch a burglar whom they have nicknamed "Goliath". Out on a call, the police and their dogs are on the track of the thief, Torco (Dopud). After a pursuit in the woods, Ace takes down Torco, out of sight of his human partner, Dan (Cain), who soon arrives. When police see injuries to Torcos neck, they believe they are the results of Ace biting him. And Torco does not face charges as the evidence against him is sketchy. Officials demand Ace be euthanized because hes now classified as a biter. Dan drives Ace to the vet hospital in Spokane, and leaves him to be put down.

Dan goes to the capture scene and sees the area is under 24-hour video surveillance. He also finds a piece of barbed wire, which he believes Torco used to slash himself with. Dan tries to convince his captain that Ace is innocent and to stop the euthanasia, but to no avail. The surveillance video is being cleaned up by technicians. Meanwhile Ace has trickily escaped the vet hospital, and heads home. The veterinary technician, not wanting to have his incompetence found out, puts some other ashes from the crematory into a container and marks them as Aces.

Dan tries to get over the grief of losing Ace by trying out other dogs, but to no success. Ace, while making his way back to Dan, captures a petty-thief and is lauded by the media. Dans daughter, Julia (McKillip), sees Ace on a TV news show, but her parents do not believe it is Ace, since his supposed cremated remains are just then delivered to their home by mail. But Julia eventually convinces Dan to take her to Wenatchee to see if the dog is Ace. When they arrive Ace has just escaped again. During their drive back home, sleepy Julia sees Ace, who has hopped into the back of a towed car. Ace sees her and waves, which is a trick she taught him. But Dan convinces Julia she was probably just dreaming, and they proceed home.

Dans police captain brings him in to show him the surveillance tape of the capture. It shows Torco first fending off Ace with barbed wire, but then slashing himself with it. Dan was right all along, and he is upset with the captain. Meanwhile, Julia stakes out Torcos house since she, too, believes he framed Ace. But while looking around, she accidentally breaks in, and then finds incriminating evidence. Torco comes home from work early and sees Julia leaving. After entering his house he discovers evidence that Julia had been inside. Torco goes to Julias house. 

After Julia returns home, as she tells her mother (Loder) about Torco, the two of them are pursued by Torco, but Ace comes to the rescue just-in-time, and brings down Torco. But Torco stabs Ace with a piece of broken glass. Dan arrives home and Torco is arrested. Ace is taken to a vet hospital and will recover. Dan then allows Julia to take Ace to the K-9 competition, where Ace wins first place. While Julia plays with Ace in the familys backyard, Dan agrees to take his wife to Tahiti, for their long-ago postponed honeymoon, and the end credits roll.

==Cast==
* Dean Cain as Daniel Harding
* Britt McKillip as Julia Harding
* Mike Dopud as Torco
* Anne Marie Loder as Lily Harding
* David Green as Captain Porter
* Zak Santiago as Officer Sanchez
* Keith "Blackman" Dallas as Boss Crowder
* Daniel Boileau as Vet Tech
* Solo / Bobby as Ace

==External links==
*  

 
 
 