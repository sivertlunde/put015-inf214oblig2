How I Live Now (film)
 
 
{{Infobox film
| name           = How I Live Now
| image          = How I Live Now poster.jpg
| image size     = 250
| border         = 
| alt            = 
| caption        = UK poster Kevin Macdonald
| producer       = John Battsek Alasdair Flind Andrew Ruhemann Charles Steel
| writer         = Jeremy Brock Tony Grisoni Penelope Skinner
| based on       =   Tom Holland Anna Chancellor
| music          = Jon Hopkins
| cinematography = Franz Lustig
| editing        = Jinx Godfrey
| studio         = Cowboy Films Film4
| distributor    = Magnolia Pictures (USA) Entertainment One (UK/Canada) Madman Entertainment (Australia)
| released       =  
| runtime        = 101 minutes  
| country        = United Kingdom
| language       = English
| budget         = 
| gross          =  $60,213 (US) 
}} of the Kevin Macdonald Tom Holland, George MacKay, Corey Johnson and Sabrina Dickens. It was screened in the Special Presentation section at the 2013 Toronto International Film Festival.   

==Plot==
Daisy, a neurotic and anorexic American teenager, is sent to the English countryside for the summer to stay with her Aunt Penn and her cousins, Eddie, Isaac, and Piper. She arrives at Heathrow Airport to tightened security and reports of a bombing in Paris, and Isaac drives to her cousins farm, which she discovers to be dilapidated and very messy. Although initially abrasive, Daisy warms to them upon learning that her deceased mother used to stay there frequently. She also falls in love with Eddie, her eldest cousin, finding him to be as introverted and strong-willed as her. A few days after her arrival, her aunt flies to Geneva to attend an emergency conference because she is an expert in terrorist extremist groups, and the group take advantage of her absence to explore their local woodlands.
 terrorist coalition detonates a nuclear bomb in London that potentially kills hundreds of thousands; the nuclear fallout reaches as far away as their home. In the aftermath, electricity goes out, and they learn from an emergency radio broadcast that martial law has been imposed. The next day, an American consular official arrives at the house and offers Daisy passage home. Unable to help her cousins, he advises them to remain indoors and wait for evacuation. After they move to a nearby barn, Daisy has sex with Eddie and decides that she would rather stay with them. The next day, however, the British Army storms the shelter and takes them to a nearby town. There, they learn that the boys and girls are to be evacuated to separate parts of the country. Both Eddie and Daisy resist separation, and Daisy is restrained with cable ties; Eddie calls to her to return to their home when she gets the chance. Daisy and Piper are then taken to the home of a British Army major and his wife, who foster them. Determined to escape, Daisy discreetly begins hoarding supplies, but their neighbourhood is attacked by the enemy before she has time to take everything she needs.

As Daisy and Piper hike through the countryside, Daisy interprets her dreams of Eddie as indications of his current situation. One night, she and Piper witness enemy personnel violently gang-rape a group of survivors in a camp. Already disturbed by this experience, they discover a massacre at the camp where Isaac and Eddie were taken. Daisy reluctantly checks the bodies; although Eddie is not among the dead, Isaacs body is. She mournfully takes his glasses and later buries them. As they leave, they are spotted by two armed men, who chase them through the woods. Piper and Daisy decide to hide, but the men discover Piper. Daisy threatens them with a gun and impulsively shoots them both; she kills one and wounds the other. The horror of what she has done, along with her fears, begins to take its toll on Daisy. Later, she realises that they have lost their map and compass, and the girls are on the verge of giving up when they see Eddies pet hawk fly overhead. They realise it will lead them home and follow it.

Upon arriving home, their elation turns to horror when they discover that the entire military garrison stationed there has been killed, and the house is ransacked and empty; only Jet, Pipers dog, remains. Eddie is not at the barn where they took shelter either, and although Piper is elated to be home, Daisy breaks down in tears outside. The next day, however, the two hear Jet barking, and Daisy runs out into the woods, where she finds Eddie lying unconscious; he has severe burns, gashes, and his eyes are swollen shut. As she nurses him, a ceasefire is announced, electricity is returned, a new government forms, and the country begins to recover. However, it becomes clear that Eddie is suffering from post-traumatic stress disorder, and he does not speak aloud. After he accidentally cuts himself while gardening, Daisy tenderly sucks the blood from his cut, which mimics his actions earlier; he responds by kissing and hugging her, which gives her hope that he is beginning to recover from his disorder.

==Cast==
* Saoirse Ronan as Daisy Tom Holland as Isaac
* Anna Chancellor as Aunt Penn George MacKay as Edmond Corey Johnson as Consular official
* Harley Bird as Piper Danny McEvoy as Joe
* Darren Morfitt as the Sergeant
* Jonathan Rugman as the News Reporter
* Stella Gonet as Mrs McEvoy
* Des McAleer as Major McEvoy

==Production==
Filming began in June 2012 in England and Wales. 

===Release===
The film was released on 4 October 2013 in the United Kingdom and was set for release on 28 November 2013 in Australia. On 25 July 2013, Magnolia Pictures acquired the US rights to distribute the film. 

==Reception==
  rated it 57/100 based on 29 reviews.  Justin Chang of Variety (magazine)|Variety called it an "uneven but passionate adaptation".   Todd McCarthy of The Hollywood Reporter called it "a derivative teen romance in an apocalyptic setting."   Jeanette Catsoulis of The New York Times wrote that the film "struggles to balance a nebulous narrative on tentpole moments of rich emotional resonance."  Alan Scherstuhl of The Village Voice called it a "tender, humane, and searing" film with "scenes of great beauty and world-ending terror." 

The film is rated R16 in New Zealand for violence, horror, offensive language and sexual themes.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 