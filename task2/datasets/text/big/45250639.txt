What Happened, Miss Simone?
 
What Happened, Miss Simone is a 2015 biographical documentary film about Nina Simone directed by Liz Garbus. The film opened the 2015 Sundance Film Festival. The screening was followed by a tribute performance by John Legend.  The film will be released by Netflix in spring 2015. The documentary combines previously unreleased archival footage and interviews with Simones daughter and friends. The title of the film is taken from a Maya Angelou quote. 

==Production== Lisa Simone Kelly served as the films executive producer. 

==Reception==
Indiewire gave the film a B grade.  Michael Hogan wrote for Vanity Fair that, "The risk of making a documentary of a towering artist is that, by explaining her, you only end up diminishing her. Not Nina Simone—not this time. In Liz Garbus’s telling, Simone’s talent and personality shine through, as gloriously singular, and uncontrollable, as ever." 

==References==
 

==External links==
*  at the Internet Movie Database

 

 
 
 
 
 
 
 
 


 