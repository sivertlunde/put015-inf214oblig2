All Is at Stake
{{Infobox film
| name           = All is at Stake
| image          = 
| image_size     = 
| caption        = 
| director       = Max Nosseck 
| producer       = 
| writer         = Max Kolpé
| narrator       = 
| starring       = Luciano Albertini   Claire Rommer   Ernő Verebes   Carl Auen
| music          = Hans May
| editing        = 
| cinematography = Walter Loch Bach
| studio         = Deutsche Lichtspiel-Syndikat 
| distributor    = Deutsche Lichtspiel-Syndikat 
| released       = 7 June 1932
| runtime        = 76 minutes
| country        = Germany
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    =  German comedy comedy thriller film  directed by Max Nosseck and starring Luciano Albertini, Claire Rommer and Ernő Verebes.

==Cast==
* Luciano Albertini   
* Claire Rommer   
* Ernő Verebes 
* Carl Auen 
* Domenico Gambino   
* Eddie Polo   
* Willi Schur   
* Walter Steinbeck   
* Norbert Miller   
* Gustav Püttjer

==References==
 

==Bibliography==
* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910-1933. Berghahn Books, 2005.

==External links==
* 

 

 
 
 
 
 
 
 
 


 