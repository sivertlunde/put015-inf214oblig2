Haunts of the Very Rich
{{Infobox film
| name = Haunts of the Very Rich
| image =Haunts_of_the_Very_Rich.jpg
| caption = 
| director = Paul Wendkos
| producer = Lillian Gallo
| writer = T.K. Brown III and William P. Wood
| cinematography =
| starring = Lloyd Bridges, Cloris Leachman, Edward Asner, Anne Francis, Tony Bill
| studio = ABC Circle Films ABC
| released =  
| runtime = 75 minutes
| country = United States
| language = English
| budget =
}}

Haunts of the Very Rich is a 1972 Television film|made-for-TV  thriller (genre)|thriller, broadcast as an ABC Movie of the Week.

==Plot==
A group of rich tourists and businesspeople fly to a mysterious tropical resort, the Portals of Eden, hosted by Seacrist (Moses Gunn). There, they spend their time relaxing and being pampered in paradise. Following a storm after their first night, the guests suddenly find themselves all alone, with dwindling supplies, without communication with the outside world, and abandoned by the resort staff. The guests experience several brushes with death.

==Cast==
*Lloyd Bridges ....... Dave Woodrough
*Cloris Leachman ....... Ellen Blunt
*Edward Asner ....... Al Hunsicker
*Anne Francis ....... Annette Larrier
*Tony Bill ....... Lyle
*Donna Mills ....... Laurie
*Robert Reed ....... Rev. John Fellows
*Moses Gunn ....... Seacrist
*Beverly Gill ....... Miss Vick
*Todd Martin ....... Harris
*Phyllis Hill ....... Rita
*Michael Lembeck ....... Delmonico
*Susan Foster ....... Miss Upton

==Production notes==
Haunts of the Very Rich was filmed around the Miami, Florida area, with the resort scenes filmed at Villa Vizcaya.

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 
 