Agadam
{{Infobox film
| name = Agadam
| image = Agadamtamilfilm.jpg
| caption = Promotional Poster
| director = Mohamad Issack
| producer = Mohamad Issack Rasiyabi Mohamad
| writer = Mohamad Issack
| starring = Tamizh Srini Iyer Sri Priyanka Baskar
| music = Basker
| cinematography = E.J. Nauzad
| studio = Last Bench Boys Productions
| released =  
| runtime = 123 minutes
| language = Tamil
| country = India
| budget = 
}}
 Indian Tamil Tamil horror real time, Telugu as Seesa with Sivaji (Telugu actor)|Sivaji, Chesvaa and Namratha in the lead roles. 

==Cast==
*Tamizh
*Srini Iyer
*Sri Priyanka
*Baskar
*Kalaisekaran
*Saravana Balaji
*Anisha
*Master Ajay

==Production==
  Guinness World Record for being the longest uncut film,  beating the record held by Russian Ark directed by Alexander Sokurov, which used a single 96-minute Steadicam sequence shot.    A director registered with the union (L. Suresh of Eththan fame), a notary public and a government teacher were present during the shoot,    while a team followed Isaak and the cinematographer E.J. Nauzad Khan and filmed the entire making of the film in order to send the video to the Guinness Record jury.  The camera used was a Sony NEX-FS100. 

Director Issack gave up his software job to "make a film that is different, yet entertaining" and said that he wanted his first film to make an impact.  He spent over a year on the script and stated that he was inspired by Russian Ark.  Ten people were involved in the making of the film, the director, the cameraman, and 8 actors,  who were all newcomers. The team rehearsed for over a month before the filming and the entire sequence was said to be rehearsed over 40 times.  Agadam was also the first film to be shot in the night effect with a hand-held camera.  The film, which had no songs, was awarded a U/A certificate by the Censor Board. 

==Reception==
M. Suganth of The Times of India rated it 1.5 out of 5, saying "Agadam is a cautionary tale that proves that while digital has made it possible for anyone to make a film, not everyone should make one. This is an unnecessarily over-long film with a sloppy script, amateur performances, shoddy camerawork, template music and unintentionally funny sequences, with not a single redeeming feature (no, the longest shot doesnt count). The only horror here is not the one the characters experience but what we, the audience, undergo." 

==References==
 

==External links==
*  

 
 
 
 
 
 