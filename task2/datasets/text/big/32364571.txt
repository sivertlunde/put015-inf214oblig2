Takhan Teish
 
{{Infobox Film
| name           = Takhan Teish
| image          = takhanteish.jpg
| caption        = Poster of Takhan Teish
| director       = Atanu Ghosh
| producer       = Tara Film Productions
| writer         = Atanu Ghosh Neel Mukherjee, Aparajita Ghosh Das, Tanusreeshankar, Biswajit Chakrabarty, Biplab Chatterjee, Locket Chatterjee 
| music          = Rocket Mondol and Mayukh Bhoumik
| cinematography = Soumik Halder
| editing        = Sujoy Dutta Roy
| lyrics         = Suchandra Chowdhury
| art director   = Indranil Ghosh
| distributor    = Piyali Films
| released       = January 21, 2011
| runtime        = 113 minutes
| country        = India Bengali
| format         = 35mm
| sound          = Dolby 5.1
|}}

Takhan Teish (2010) is the second feature film of Atanu Ghosh which was premiered at the 6th Osaka International Film Festival, Japan.   

==Plot==

Tamodeep is brilliant, sensitive and eternally perturbed by his grandfathers prophecy that he would either be an artist or run away from home. A junior doctor by profession, Tamodeep has little communication with his near and dear ones, even lesser with his mother Sraboni, against whom he nurses a grudge for intruding into his life during his adolescent years. Quiet and introvert on the surface, Tamodeep is quite restless within. His desires often take complex routes and hover around Meghna, his former biology teacher and now a psychological counselor on radio, Mohini, the sex siren of soft porn movies and Sriparna, a law student whom he befriends on a cyber social networking site. The night before his 23rd birthday, Mohini, gets admitted in his nursing home as an accident victim and Tamodeeps life is destined to change forever.. In his desperate attempt to prevent Mohini from going out of his life, Tamodeep has to go through a series of overlapping events involving the four women in his life - his mother Sraboni, his former teacher Meghna, his Facebook buddy Sriparna and his ultimate fantasy Mohini, who steer him through a unique journey of enlightenment and self-realisation.
 {http://www.imdb.com/title/tt1849109/plotsummary?ref_=tt_stry_pl 

==Cast==

*Jisshu Sengupta as Tamodeep
*Indrani Halder as Meghna
*Paoli Dam as Mohini
*Rajatabha Dutta as Sandipan
*Aparajita Ghosh Das as Sriparna
*Tanusreeshankar as Sraboni

==Singer==
Jayati Chakrabarty

==About the director==
Directed by Atanu Ghosh, whose debut film, Angshumaner Chhobi (2009) was selected in the Indian Panorama of the International Film Festival of India, 2009 as well as in Competitive Section. It was awarded the prestigious Aravindan Puraskaram for the Best debut film of 2009, the Lankesh Chitra Prashasti (Karnataka State Award for India’s Best Debut Director ) BFJA Award as well as Audience Award (Rainbow Film Festival,London) besides numerous other awards for actors. Takhan Teish is his second film.

==External links==
*  
* http://www.imdb.com/title/tt1849109/plotsummary?ref_=tt_stry_pl
*http://www.telegraphindia.com/1110124/jsp/entertainment/story_13483577.jsp
*http://www.telegraphindia.com/1100501/jsp/entertainment/story_12399155.jsp
*http://timesofindia.indiatimes.com/entertainment/regional/bengali/movie-reviews/Takhan-Teish/movie-review/7334053.cms
*http://www.indianexpress.com/news/takhan-teish/748464/

==References==
 

 
 