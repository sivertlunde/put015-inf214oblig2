Stronger Than Death (1920 film)
{{Infobox film
| name           = Stronger Than Death
| image          = Stronger Than Death (1920) - Nazimova.jpg
| caption        = Scene in the film
| director       = Herbert Blaché
| producer       = Richard A. Rowland Maxwell Karger Alla Nazimova
| writer         =
| starring       = Alla Nazimova
| music           
| cinematography = Rudolph Berquist
| editing        =
| distributor    = Metro Pictures
| released       = January 11, 1920
| runtime        = 70 minutes; 7 reels
| country        = United States
| language       = Silent (English intertitles)
}}
Stronger Than Death is a 1920 American silent romance-drama film directed by Herbert Blaché and produced by and starring Alla Nazimova. It was distributed by Metro Pictures.  

This movie was preserved by MGM, the inheritor of Metro Pictures library, with a print in the George Eastman House Motion Picture Collection. 

==Cast==
*Alla Nazimova - Sigrid Fersen Charles Bryant - Major Tristam Boucicault
*Charles K. French - Colonel Boucicault
*Margaret McWade - Mrs. Boucicault
*Herbert Prior - James Barclay
*William Orlamond - Rev. Mr. Meredith (*as William H. Orlamond)
*Milla Davenport - Mrs. Smithers
*Bhowgan Singh - Ayeshi
*Henry Harmon - Vahana

==References==
 

==External links==
 
* 
* 

 
 
 
 
 
 
 


 
 