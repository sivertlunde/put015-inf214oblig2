Sanjh Aur Savera
{{Infobox film
| name           =Sanjh Aur Savera 
| image          = Sanjh Aur Savera.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Hrishikesh Mukherjee
| producer       = Sevantilal Shah
| writer         = 
| screenplay     = Dhruva Chatterjee, Anil Ghosh
| story          = 
| based on       =  
| narrator       = 
| starring       =  Shanker
| cinematography = Jaywant Pathare 
| editing        = Das Dhaimade
| studio         = S.J. Films
| distributor    =  1964 
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 1964 Bollywood romantic drama film starring Guru Dutt, Meena Kumari and  Mehmood Ali|Mehmood. It was produced by Sevantilal Shah and directed by Hrishikesh Mukherjee.   

==Plot==
Dr. Shankar Chaudhry, a wealthy doctor who resides in Bombay with his younger sister, Manju, and his widowed mother, Rukmimi, finds that his mother has arranged a marriage for him with advocate Madhusudans daughter, Maya. He concedes to the marriage, even though neither he nor his mother have met her.  During the ceremony, Maya faints and recuperates under the care of her cousin, brother Prakash, and Madhusudan, and then the following day accompanies Shankar to his home. Maya rejects Shankars sexual advances, as she is participating in a Holy Fast. 

On a visit to Varanasi|Banaras, where Madhusudan now resides, Shankar remarries Maya at Bhagwan Vishwanaths Temple. Their relationship becomes intimate and she becomes pregnant.

Shankar arrives home one day and discovers that Prakash and Maya are missing; his search for them proves fruitless. He later learns that his wife is an imposter and that she is probably married to Prakash.

==Cast==

*Meena Kumari as	Gauri
*Guru Dutt as Dr. Shankar Chaudhry Mehmood as Prakash
*Shubha Khote as Radha (as Subha Khote)
*Manmohan Krishna 		
*Padmadevi(as Padma Devi)
*Jagdev 		
*Harindranath Chattopadhyay as Mama, Radhas uncle
*Brahm Bhardwaj 	
*Kanu Roy 	 Rashid Khan 			
*Preeti Bala 			
*Praveen Paul as Manorama - Radhas mom (as Ruby Paul)
*Zeb Rehman

==Soundtracks==

*Ajhun Na Aye Balamwa Revival: Suman Kalyanpur / Mohammed Rafi	
*Ajhun Na Aye Balamwa: Suman Kalyanpur / Mohammed Rafi
*Chand Kanwal Mere Chand Kanwal: Suman Kalyanpur
*Man Mohan Krishna Murari: Lata Mangeshkar
*O Sajna Mere Ghar Angna: Lata Mangeshkar
*Taqdeer Kahan Le Jayegi Malum Nahin: Mohammed Rafi 	
*Yehi Hai Woh Sanjh Aur Savera: Asha Bhosle / Mohammed Rafi 	
*Zindagi Mujhko Dikha De Rasta: Mohammed Rafi		
	 	

==References==
 


==External links==
*  


 
 
 
 
 
 

 