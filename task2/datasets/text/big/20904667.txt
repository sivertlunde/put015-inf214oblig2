Sarkari Mehmaan
{{Infobox film
| name           = Sarkari Mehmaan
| image          = Sarkari Mehmaan.jpg
| image_size     = 
| caption        = 
| director       = N.D. Kothari
| producer       = N.D. Kothari
| writer         = Navneet
| narrator       =  Jasmin
| music          = Ravindra Jain
| cinematography = 
| editing        = I. M. Kunnu
| distributor    = 
| released       = Friday,12th of January 1979  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1979 Bollywood Jasmin in lead roles.

==Plot==
Police inspector Anand is very honest, he is known for his honesty, diligence and bravery. He single handedly captures and arrests notorious smuggler and criminal Jack Ranjit, who is sentenced to several years of jail. Anand Vinod Khanna in disguise, also captures gangster Gulkhan Amjad Khan, who is also sentenced to a long term in jail. Anands wife Rekha is not very honest and trustworthy and has an affair with another man named Somesh. There are a lot of fights and arguments between husband and wife and Anand threatens to kill her. He starts following his wife hoping to catch her and Somesh redhanded. Shortly after that Rekha and Somesh are found dead and Anand is arrested for their murder though he has not killed them. To prove his innocence and to find the real murderers he escapes from jail and surprisingly comes back face to face with his foes. Ultimately he is proved innocent and the real murderer is arrested. 

== Cast ==
*Vinod Khanna as Anand Jasmin as Bindiya
*Amjad Khan
*Ranjeet
*Om Shivpuri
*Viju Khote
*Satyendra Kapoor
*Padma Khanna
*Jagdeep
*Birbal Bindu
*Vikas Anand as Prosecuting Attorney
*Manju Asrani as Rekha
*Manmohan as Somesh
*Indrani Mukherjee as Anands sister
*Lalita Pawar
*Polson as Prison inmate
*Shivraj as Bank Manager Sunder
*Tun Tun as Bank employee 

==Crew==
*Film director|Director- N. D. Kothari
*Film producer|Producer- N. D. Kothari
*Music Director- Ravindra Jain
*Lyrics- Hasrat Jaipuri,Naqsh Lyallapuri,Ravindra Jain
*Narrative|Story- Navneet
*Screenplay- Navneet
*Dialogue -Aziz Quaisi
*Executive Producer -V. N. Kothari
*Editor - I. M. Kunnu
*Production Designer - Krishan Malhotra, Sampat Singh Kothari
*Art Director - V. R. Karekar
*Costumes Designer - Mohamed Umar
*Audiographer - Raj Trehan,Zia Ul Haque
*Choreographer - P. L. Raaj
*Action Director - F. Makrani, Ravi Khanna
*Playback Singer - Asha Bhosle 
*Assistant Director - Sushil Kumar
*Hair Stylist - Shekar Ram Patriwal

==References==
 

==External links==
*  

 
 
 
 


 
 