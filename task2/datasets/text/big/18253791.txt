Chris & Don
{{Infobox film
| name           = Chris & Don
| image          = Chris&Don film.jpg
| caption        = 
| director       = Guido Santi Tina Mascara 
| producer       = James White Tina Mascara Guido Santi Andrew Herwitz
| writer         =  Michael York
| starring       = Christopher Isherwood Don Bachardy
| music          = Miriam Cutler
| cinematography = Ralph Q. Smith
| editing        =
| distributor    = Zeitgeist Films
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
}}
Chris & Don: A Love Story is a 2007 documentary film that chronicles the lifelong relationship between author Christopher Isherwood and his much younger lover, artist Don Bachardy. Chris & Don combines present-day interviews, archival footage shot by the couple from the 1950s, excerpts from Isherwoods diaries, and playful animations to recount their romance. It was directed by Guido Santi and Tina Mascara, and was the centerpiece film at NewFest, the New York LGBTQ Film Festival, in 2008.

== Plot ==
Chris & Don tells the story of a romance that began on the beaches of Santa Monica in the 1950s,  when Christopher Isherwood at age 48 met Don Bachardy who then was eighteen years old. Isherwood, an established author with works such as The Berlin Stories, which helped inspire much of Cabaret (musical)|Cabaret, helped Bachardy discover and develop his affinity for drawing and painting as he became a renowned portrait painter during the second half of the 20th century to the present. The documentary includes insight from friends, including Liza Minnelli and John Boorman, who tell of the countless struggles the two faced as one of the first openly gay couples in Hollywood. Despite the age difference, the couple endured until Isherwood succumbed to prostate cancer in 1986.

==Critical reception== ruling affirming same-sex marriage and the release of the film. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 