A Doll's House (1973 Garland film)
{{Infobox Film
| name           = A Dolls House
| image          = ADollsHouseGarland1973Poster.jpg
| image_size     = 
| caption        = US Theatrical Poster
| director       = Patrick Garland 
| producer       = Hillard Elkins
| writer         = Christopher Hampton
| starring       = Claire Bloom Anthony Hopkins John Barry
| cinematography = Arthur Ibbetson John Glen
| distributor    = EMI Films (UK) Paramount Pictures (USA theatrical) Metro-Goldwyn-Mayer (DVD)
| released       =  
| runtime        = 105 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} British film, directed by Patrick Garland.  It is based on Henrik Ibsens 1879 play A Dolls House.                    The main character of this film is Nora Helmer, a simple woman married to a working man, the straight but authoritarian Torvald. The two have a good marriage, until things from their past threaten to destroy their lives.

==Cast==
* Claire Bloom - Nora Helmer
* Anthony Hopkins - Torvald Helmer
* Ralph Richardson - Dr. Rank
* Denholm Elliott - Nils Krogstad
* Edith Evans - Anne-Marie
* Anna Massey - Kristine Linde
* Helen Blatch - Helen
* Kimberley Hampton - Bob
* Daphne Riggs - Old Woman
* Mark Summerfield - Ivar
* Stefanie Summerfield - Emmy

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 