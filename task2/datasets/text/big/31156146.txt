Manpeda
{{Infobox film 
| name           = Manpeda
| image          =
| caption        =
| director       = PMA Azeez
| producer       = Bahadoor
| writer         = Azeez V A A Sreekumaran Thampi (dialogues)
| screenplay     =
| starring       = Jayabharathi Jesey T. S. Muthaiah Prem Navas
| music          = MS Baburaj
| cinematography =
| editing        =
| studio         = Chaithanyachitra
| distributor    = Chaithanyachitra
| released       =  
| country        = India Malayalam
}}
 1971 Cinema Indian Malayalam Malayalam film, directed by PMA Azeez and produced by Bahadoor. The film stars Jayabharathi, Jesey, T. S. Muthaiah and Prem Navas in lead roles. The film had musical score by MS Baburaj.   

==Cast==
*Jayabharathi
*Jesey
*T. S. Muthaiah
*Prem Navas
*Bahadoor
*Nellikode Bhaskaran

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Neelathaamarapoove || Raveendran || Sreekumaran Thampi || 
|-
| 2 || Ushassinte Gopurangal || Raveendran, Cochin Ibrahim || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 