Hobo with a Shotgun
{{Infobox film
| name = Hobo with a Shotgun
| image = Hobo-with-a-shotgun-movie-poster.jpg
| alt = 
| caption = Theatrical release poster
| director = Jason Eisener
| producer = Rob Cotterill Niv Fichman Paul Gross
| writer = John Davies Brian Downey Gregory Smith
| music = Alexander Rosborough
| cinematography = Karim Hussain
| editing = Jason Eisener
| studio = Rhombus Media Whizbang Films Inc. Yer Dead Productions
| distributor = Alliance Films Magnet Releasing
| released =  
| runtime = 86 minutes
| country = Canada
| language = English
| budget = $3 million 
| gross = $748,453  
}} exploitation action trailer of the same name from Quentin Tarantino and Robert Rodriguezs South by Southwest Grindhouse (film)|Grindhouse trailers contest.
== Plot == Brian Downey) Gregory Smith). stockade screaming for assistance. Two cars approach the man revealing Ivan, Slick and The Drake. An argument ensues and it is revealing the man in the stockade is The Drakes younger and disliked brother, Logan (Robb Wells). The Drake explains to the townspeople that his brother will serve as an example of his control and carries out the public decapitation of Logan with a barbed-wire noose attached to The Drakes truck.
 shotgun from the shelf and kills the three bandits. Realizing that Hope Town needs justice, he chooses the shotgun over the lawnmower, which is also $49.99. He pays for the gun and goes on a rampage in which he murders dozens of criminals, including the filmmaker, a pimp (Duane Patterson), a coke lord (Mark A. Owen) and a pedophile dressed as Santa Claus (Brian Jamieson).

The Drake, infuriated by the positive effect that the Hobos actions are having on the townspeople, instructs Ivan and Slick to get rid of him. After using a flamethrower on a school bus filled with children to remind the citizens of Hope Town that they are to be feared, Ivan and Slick suddenly burst into a television studio during a live news broadcast. They kill the anchorman (George Strombolopolous) and demand that all homeless people be killed. The Drake then appears to personally request the Hobo be brought to him. The town is plunged into anarchy as a mass mob-orchestrated genocide of the towns vagrants begins. As Abby is walking home, she is accosted by a corrupt cop, who attempts to rape her. The Hobo intervenes and kills him. A nearby mob of people hear gunshots and begin rushing to investigate. Abby smuggles the Hobo past them in a shopping cart covered with the entrails of the dead cop. After escaping the mob, the pair are spotted by Otis, who proceeds to inform Slick and Ivan.

Back at her apartment, Abby asks the Hobo why he disappeared the previous night, he tells her of his desire to start a lawnmowing business. Abby enthusiastically partners up with him in his venture. The two are then are attacked by Ivan and Slick. The Hobo coerces Ivan to flee in his car by holding Slick at gunpoint. The Hobo shoots a pleading Slick in the genitals. The Hobo then takes a wounded Abby to the hospital. Slick uses his remaining energy to call The Drake on a payphone, before supposedly being taken down to hell in a demonic, perpetually burning schoolbus. The Drake, mourning the death of his favorite son, summons Rip and Grinder (Nick Bateman and Peter Simas), a duo of armour-clad demons known as The Plague, to take the Hobo down. While Abby is recovering, the Hobo visits the maternity ward and monologues to the babies. The Plague capture The Hobo after he returns to Abbys room. They deliver him to The Drake, who intends to execute him in front of the townspeople in order to set an example. Abby, having recovered, returns to the pawn shop to manufacture some weapons to help the Hobo (attaching an axe to the Hobos shotgun and turning the lawnmower into a shield). Leaving a crowd approaches her (thinking she is homeless), instead she rallies them to hunt down The Drake.

Abby finds and confronts The Drake. She threatens to kill Ivan with the lawnmower, but The Drake simply shoots his astonished son when he decides Ivan is a disappointment and not worth saving. In the ensuing fight, she kills Grinder before The Drake severs her hand with the lawnmower shield. She manages to incapacitate The Drake by stabbing him repeatedly in the stomach with her exposed arm bone. Abby saves the Hobo and he prepares to fight Rip. Rip then tells Abby that she must join The Plague to replace Grinder, but the Hobo refuses to allow her to do it and The Plague departs. The Hobo prepares to finish off The Drake, who was trying to crawl away to safety, when the corrupt police arrive. The townspeople, motivated by Abbys bravery, decide to take a stand with the Hobo against the corrupt police. The Hobo refuses to have them sacrifice themselves for him and shoots The Drake in the head. The police open fire, killing the Hobo as Abby screams. The corrupt police are then all blown away by the townspeople in retaliation for killing the Hobo.

In an extended ending that was cut from the final cut of the film, Abbys hand is replaced by several shotguns as she becomes part of The Plague.

== Cast ==
* Rutger Hauer as the Hobo: a homeless man who becomes a vigilante after witnessing the high crime rate and corrupt, ineffective police.
* Molly Dunsworth as Abby: a local prostitute who befriends the Hobo, she became at first horrified by his actions then after watching his bravery decides to help him. Brian Downey as The Drake: A ruthless, psychopathic crime lord of Hope Town, he uses fear and intimidation in order to stay in control of Hope Town. He is also the father of Ivan and Slick.  Gregory Smith as Slick: Drakes favorite son and Ivans brother. Slick is highly remorseless, sadistic and cold killer, his father prefers him over Ivan due to his leadership skills and icier brand of psychopathic tortures, his father also refers to him as having "more potential" than him and his brother.
* Nick Bateman as Ivan: Drakes son and Slicks equally sadistic brother, although he was more violent and aggressive than Slick, his father refers to him as not having "that much potential".
* Nick Bateman and Peter Simas as Rip and Grinder respectively: two armored demons who help The Drake in capturing the Hobo, they seem to be contract killers and they have a hit list of historical and Biblical figures such as Abraham Lincoln and Jesus Christ.
* Robb Wells as Logan
* Jeremy Akerman as Chief of Police
* Drew OHara as Otis
* David Brunt as Dirty Cop
* Pasha Ebrahimi as "Bum Fight" filmmaker
* Mark A. Owen as the Coke lord
* Brian Jamieson as the pedophile Santa Claus
* Duane Patterson as the Pimp
* George Stroumboulopoulos as News Anchor

== Development == Halifax on April 19, 2010. 
 cameos in the film as a police officer|cop.  It was the second of Grindhouse s fake trailers to be turned into a feature film, the first being Machete (film)|Machete,  and is followed by Machete Kills.

== Release ==
Hobo with a Shotgun staged its world premiere at the 2011 Sundance Film Festival.  The film was released on a limited basis to Canadian theatres on March 25, 2011  and in the United States on May 6, 2011.  The film was also released via OnDemand services such as Xbox Live, PlayStation Network and iTunes on April 1, 2011. 

== Home media ==
The film received its Australian premiere on the opening night of the Sydney Film Festival on 8 June 2011. It was later released on DVD and Blu-ray Disc in the United States on July 5, 2011 and in the UK on July 15, 2011.

== Reception ==
 
Reviews of the film are generally positive, and it currently holds a 66% "certified fresh" approval rating out of 111 reviews on the review aggregate website Rotten Tomatoes with an average rating of 5.8 out of 10 with the consensus being, "It certainly isnt subtle – or even terribly smart – but as a gleefully gory homage to low-budget exploitation thrillers, Hobo with a Shotgun packs plenty of firepower." 

Since its release on Netflix, the film has gained a cult film status, as its release was not very widespread.

== References ==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 