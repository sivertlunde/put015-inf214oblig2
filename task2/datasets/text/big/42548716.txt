Mr. Leos caraX
{{Infobox film
| name           = Mr Leos caraX
| image          = MLCX poster.jpg
| alt            = 
| caption        = Sundance film poster
| director       = Tessa Louise-Salomé
| producer       = Tessa Louise-Salomé Chantal Perrin-Cluzet
| writer         = Tessa Louise-Salomé Chantal Perrin-Cluzet Adrien Walter
| music          = Gaël Rakotondrabe
| cinematography = Kaname Onoyama
| editing        = Tessa Louise-Salomé Gabriel Humeau Laureline Attali
| studio         = Petite Maison Production ARTE France Théo Films
| distributor    = 
| released       =  
| runtime        = 71 minutes
| country        = France
| language       = English French Japanese
}}
Mr Leos caraX (also known as Mr. X) is a 2014 French documentary film written, directed and produced by Tessa Louise-Salomé.   The film premiered in-competition in the World Cinema Documentary Competition at 2014 Sundance Film Festival on January 20, 2014.  

==Synopsis==
The film focus on mysterious, solitary French filmmaker, Leos Carax, about his work and his reputation as icon of world cinema.

==Reception==
Mr Leos caraX received mixed reviews upon its premiere at the 2014 Sundance Film Festival. In his review for Variety (magazine)|Variety, Guy Lodge said that "Tessa-Louise Salomes documentary is an alluring if not especially illuminating tribute to singular filmmaker Leos Carax."  Boyd van Hoeij in his review for The Hollywood Reporter said that "A film that works as a reminder of Caraxs unique talents, but isnt quite insightful itself."  While, Kyle Burton of Indiewire praised the film by saying that "Louise-Salomé injects the film with his subjects fascinating presence, by arranging clips and overlaying images on the artificially decrepit interview sets. Still, she wisely limits the extent to which her documentary replicates Carax’s ambition." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 


 
 