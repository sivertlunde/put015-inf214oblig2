The New Country
{{Infobox film
| name           = The New Country
| image          = TheNewCountryPoster.gif
| caption        = Poster
| director       = Geir Hansteen Jörgensen
| producer       = Dan Berglund Tomas Eskilsson Christer Nilson
| writer         = Lukas Moodysson Peter Birro
| starring       = Mike Almayehu Michalis Koutsogiannakis Lia Boysen
| music          = Dan Berglund Jonas Bohlin Esbjörn Svensson
| cinematography = Marek Wieser
| editing        = Fredrik Morheden
| distributor    = Göta Film
| released       =  
| runtime        = 232 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 SVT and the feature film version won more awards around the world than any other Swedish feature in 2001. Some Swedish newspapers and critics has chosen The New Country as best Swedish TV mini-series ever and it is by many considered the beginning of Swedish "multicultural" cinema.

== Plot ==
Two refugees, Ali and Massoud run away from their asylum camp and meet Louise, a former Miss Sweden. Together in an old rusty car they are on the run from the police and old memories through the Swedish summer countryside.

==Cast (main characters)==
*Mike Almayehu   (Ali)
*Michalis Koutsogiannakis   (Massoud)
*Lia Boysen   (Louise)

==External links==
* 
* 
* 

 
 
 
 

 