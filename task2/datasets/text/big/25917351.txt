Adventures of Gallant Bess
 
{{Infobox film
| name           = Adventures of Gallant Bess
| image size     =
| image	=	Adventures of Gallant Bess FilmPoster.jpeg
| caption        =
| director       = Lew Landers
| producer       = Jerry Briskin (producer) Matthew Rapf (producer)
| writer         = Matthew Rapf (screenplay)
| narrator       = Cameron Mitchell John Harmon
| music          = Irving Gertz William Bradford
| editing        = Harry Komer
| distributor    = Eagle-Lion Films
| released       = 1948 (USA)
   (Sweden) 

| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

Adventures of Gallant Bess is a 1948 American contemporary Western film directed by Lew Landers and filmed in Cinecolor. It has no relation to producer Matthew Rapfs father Harry Rapfs film Gallant Bess.

== Plot summary == wrestle with slippery oil breaking Teds leg in the process.

Having no money, Ted sells Bess to the traveling carnival where she is mistreated and forced into performing in their show. As Ted recovers he plans to reunite with Bess.

== Cast == Cameron Mitchell as Ted Daniels
*Audrey Long as Penny Gray
*Fuzzy Knight as Woody
*James Millican as Bud Millerick John Harmon as Blake
*Edward Gargan as Deputy Sheriff
*Harry Cheshire as Edward "Doc" Gray
*Cliff Clark as Sheriff
*Evelyn Eaton as Billie

== References ==
 

== External links ==
 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 