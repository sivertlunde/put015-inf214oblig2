Adaptation (film)
 
 
 
{{Infobox film
| name           = Adaptation.
| image          = Adaptation. film.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Spike Jonze
| producer       = Jonathan Demme Vincent Landay Edward Saxon
| screenplay     = Charlie Kaufman Donald Kaufman
| based on       =   Brian Cox
| music          = Carter Burwell
| cinematography = Lance Acord
| editing        = Eric Zumbrunnen Intermedia Propaganda Films Saturn Films
| distributor    = Columbia Pictures
| released       =  
| runtime        = 114 minutes
| country        = United States
| language       = English
| budget         = $19 million
| gross          = $32,801,173
}}
 Brian Cox, Tilda Swinton, Ron Livingston and Maggie Gyllenhaal in supporting roles.

Though the film is billed as an adaptation of The Orchid Thief, its primary narrative focus is Charlie Kaufmans struggle to adapt The Orchid Thief into a film, while dramatizing the events of the book in parallel. Adaptation. also adds a number of fictitious elements, including Kaufmans twin brother (also credited as a writer for the film) and a romance between Orlean and Laroche, and culminates in completely invented events including fictional versions of Orlean and Laroche three years after the events related in The Orchid Thief, Kaufman and his fictional twin brother.

The film had been in development as far back as 1994. Jonathan Demme brought the project to Columbia Pictures with Kaufman writing the script. Kaufman went through writers block and did not know what to think of The Orchid Thief. Finally he wrote a script based on his experience of adapting The Orchid Thief as a screenplay. Jonze signed to direct, and filming was finished in June 2001. Adaptation. achieved critical acclaim, and gained numerous awards at the 75th Academy Awards, 60th Golden Globe Awards and 56th British Academy Film Awards, especially for its writing and acting. Cooper won the Academy Award for Best Supporting Actor, while Kaufman won the BAFTA Award for Best Adapted Screenplay.

==Plot==
The self-loathing Charlie Kaufman is hired to write the screenplay for The Orchid Thief. Kaufman is going through depression and is not happy that his twin brother Donald has moved into his house and is mooching from him. Donald decides to become a screenwriter like Charlie and attends one of Robert McKees famous seminars.

Charlie, who rejects formulaic script writing, wants to ensure that his script is a faithful adaptation of The Orchid Thief. However, he comes to realize that the book does not have a usable narrative and that it is impossible to turn into a film, leaving him with a serious case of writers block. Already well over his deadline with Columbia Pictures, and despairing at writing his script with self-reference, Charlie travels to New York to discuss the screenplay with Orlean directly. Unable to face her and with the surprising news that Donalds spec script for a clichéd psychological thriller, called The 3, is selling for six or seven figures, Kaufman resorts to attending McKees seminar in New York and asks him for advice.  Charlie ends up asking his brother Donald to join him in New York to assist with the story structure.

Donald pretends to be Charlie and interviews Orlean, but is suspicious of her account of the events of her book because she acts as though she is lying. He and his brother Charlie follow Orlean to Florida where she meets John Laroche|Laroche, the orchid-stealing protagonist of Orleans book and her secret lover. It is revealed that the Seminole wanted the ghost orchid in order to manufacture a drug that causes fascination; Laroche introduces this drug to Orlean.  After Laroche and Orlean catch Charlie observing them taking the drug and having sex, she decides that Charlie must die.

She forces Kaufman at gunpoint to drive to the swamp, where she intends to kill him. Charlie and Donald escape and hide in the swamp where they resolve their differences and Charlies problems with women. Laroche accidentally shoots Donald. Fleeing, Charlie and Donald drive off but crash into a rangers truck; Donald dies in the accident.  Charlie runs off into the swamp to hide but is spotted by Laroche. However, Laroche is killed by an alligator before being able to kill Charlie.

Orlean is arrested. Charlie makes up with his mother, tells his former love interest Amelia that he is still in love with her, and finishes the script. It ends with Charlie in a voice-over announcing the script is finished and that he wants Gérard Depardieu to portray him in the film.

==Cast== Charlie Kaufman / Donald Kaufman
* Meryl Streep as Susan Orlean
* Chris Cooper as John Laroche
* Cara Seymour as Amelia Kavan Brian Cox as Robert McKee
* Tilda Swinton as Valerie Thomas
* Ron Livingston as Marty Bowen
* Maggie Gyllenhaal as Caroline Cunningham
* Judy Greer as Alice
* Stephen Tobolowsky (deleted scenes) as Ranger Steve Neely
* Bob Yerkes as Charles Darwin
* Jim Beaver as Ranger Tony

 .]]
Tom Hanks was originally set for the double role of Charlie and Donald Kaufman. Variety (magazine)|Variety reviewed the film as if Donald were a real person.  Cage took the role for a $5 million salary,    and wore a fatsuit during filming.   
 Brian Cox to filmmakers.   
 cameos as Doug Jones New Yorker journalist.

==Production==
  }}
 Susan Orleans The Orchid Thief dates back to 1994.    Fox 2000 purchased the film rights in 1997,    eventually selling them to Jonathan Demme, who set the project at Columbia Pictures. Charlie Kaufman was hired to write the script, but struggled with the adaptation and writers block.    Kaufman eventually created a script of his experience in adaptation, exaggerating events, and creating a fictional brother named Donald Kaufman. Kaufman put Donald Kaufmans name on the script and dedicated the film to the fictional character.    By September 1999, Kaufman had written two drafts of the script;    he turned in a third draft in November 2000.   

Kaufman explained,     In addition Kaufman stated, "I really thought I was ending my career by turning that in!"   }}
 Drew "Moriarty" Intermedia came aboard to finance the film in exchange for international distribution rights.    Filming started in late March 2001 in Los Angeles, and finished by June.  The "evolution" fantasy sequence was created by Digital Domain, while Skywalker Sound was responsible for the audio mixing of Adaptation.

==Release==
Columbia Pictures had at one point announced a late 2001 theatrical release date.  Adaptation opened on December 6, 2002 in the United States for a limited release. The film was released nationwide on February 14, 2003, earning $1,130,480 in its opening weekend in 672 theaters. Adaptation. went on to gross $22.5 million in North America and $10.3 million in foreign countries, coming at a total of $32.8 million.   

===Home media===
Adaptation was released on DVD by Umbrella Entertainment in September 2012. The DVD is compatible with region code 4 and includes special features such as the behind the scenes featurette titled How To Shoot In A Swamp and talent profiles.   

==Reception==

===Critical response===
Based on 197 reviews collected by Rotten Tomatoes, Adaptation. received a 91% overall approval rating.  Metacritic scored the film 83 based on 40 reviews.   

  of The Boston Globe thought "This is epic, funny, tragic, demanding, strange, original, boldly sincere filmmaking. And the climax (narrative)|climax, the portion that either sinks the entire movie or self-critically explains how so many others derail, is bananas."    David Ansen of Newsweek felt Meryl Streep had not "been this much fun to watch in years",    while Mike Clark of USA Today gave a largely negative review, mainly criticizing the ending: "Too smart to ignore but a little too smugly superior to like, this could be a movie that ends up slapping its target audience in the face by shooting itself in the foot."   

===Accolades=== Actor in Supporting Actress) Charlie and Best Adapted Best Motion Best Adapted Grand Prix of the Belgian Syndicate of Cinema Critics. 

==See also==
*   (1997) by Robert McKee
*   (1998) by Susan Orlean
* Identity (film)|Identity, a 2003 film in which the twist ending bears a distinct resemblance to the twist ending of The 3, the fictional script by Donald Kaufmanns character.   2003 Ted Dekker novel of the same name) with a distinct resemblance to the plot of The 3, the fictional script by Donald Kaufmanns character. 
* “Adaptation, Metafiction, Self-Creation,” in Genre: Forms of Discourse and Culture, Spring 2007, vol. 40: 1 by Julie Levinson

==References==
 

==External links==
 
*  
*  
*   at BeingCharlieKaufman.com
*  
*  
*  
*  , by Charlie Kaufman, Spike Jonze. Nick Hern Books, 2002. ISBN 1-85459-708-6.

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 