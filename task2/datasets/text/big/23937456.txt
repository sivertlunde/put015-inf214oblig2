The Way of War
{{Infobox film
| name           = The Way of War
| image          = The way of war (movie poster).jpg
| image_size     = 
| alt            = 
| caption        = Theatrical poster John Carter
| producer       = David Pomier Richard Salvatore Scott Schafer John Carter Scott Schafe
| narrator       =  John Terry Lance Reddick J. K. Simmons Clarence Williams III Jaclyn DeSantis
| music          = James Melvin
| cinematography = Kevin Sarnoff
| editing        = Matthew Booth
| studio         = First Look Studios
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $5,000,000 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} John Carter from a screenplay by John Carter and Scott Schafe. Filming took place largely in Baton Rouge, Louisiana.

== Plot == conspiracy connecting American security and integrity.

==Cast==
* Cuba Gooding Jr. as David Wolfe
* J. K. Simmons as Sgt. Mitchell
* Clarence Williams III as Mac
* Vernel Bagneris as Samir
* Lance Reddick as The Black Man John Terry as the Secretary of Defense
* Jaclyn DeSantis as Sophia, Davids wife

== Reception ==
The film received mostly negative reviews when it was released direct-to-DVD in 2008. "The story is unevenly pieced together through cheap flashbacks, leaving the audience in the dark for nearly two-thirds of the film until, finally, things start to come together, sort of. But by then, sadly, its too little, too late. The mystery is lost in a sea of weary ideas and well-worn plot devices," said one reviewer for IGN.

==References==
* 

 
 
 
 


 