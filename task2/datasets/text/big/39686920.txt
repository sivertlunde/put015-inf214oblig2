Father and Son (1929 film)
{{Infobox film
| name           = Father and Son
| image          = 
| image_size     = 
| caption        = 
| director       = Géza von Bolváry 
| producer       = Marcel Hellman
| writer         = Franz Schulz 
| narrator       = 
| starring       = Harry Liedtke   Rolf von Goth   Charles Puffy   Ruth Weyher
| music          = 
| editing        =
| cinematography = Willy Goldberger
| studio         = Deutsche Lichtspiel-Syndikat
| distributor    = Deutsche Lichtspiel-Syndikat 
| released       = 24 October 1929
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent film directed by Géza von Bolváry and starring Harry Liedtke, Rolf von Goth and Charles Puffy.  The films art direction was by Robert Neppach and Erwin Scharf.

==Cast==
* Harry Liedtke as Jean Bonnard 
* Rolf von Goth as Marcel Bonnard 
* Charles Puffy as Epstein 
* Ruth Weyher as Madame Tibot 
* Marie Glory as Stella Valéry 
* Anton Pointner as Monsieur Tibot 
* Ida Wüst as Bonnards Wirtschafterin 
* Yvette Darnys as Fifi 
* Charlotte Susa as Nanon 
* Jim Gérald as Tibots Rechtsanwalt

==References==
 

==Bibliography==
* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910-1933. Berghahn Books, 2005.

==External links==
* 

 
 
 
 
 


 