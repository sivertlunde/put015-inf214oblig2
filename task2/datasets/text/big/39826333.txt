Poltergeist (2015 film)
{{Infobox film
| name           = Poltergeist
| image          = Poltergeist 2015 poster.png
| alt            = 
| caption        = Theatrical release poster
| director       = Gil Kenan
| producer       = Roy Lee Sam Raimi Robert G. Tapert
| screenplay     = David Lindsay-Abaire
| based on       = Poltergeist (1982 film)|Poltergeist by Steven Spielberg Michael Grais Mark Victor Jane Adams
| music          = Marc Streitenfeld
| cinematography = Javier Aguirresarobe
| editing        = Jeff Betancourt
| studio         = Metro-Goldwyn-Mayer Ghost House Pictures Vertigo Entertainment
| distributor    = 20th Century Fox
| released       =  
| runtime        = 91 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} supernatural thriller fantasy horror horror film the 1982 Jane Adams. The film is scheduled to be released on May 22, 2015.

==Plot== suburban home is invaded by angry Ghost|spirits. When the terrifying apparitions escalate their attacks inside a small suburban house in a quiet neighborhood and take the youngest daughter, the family must come together to rescue her.

== Cast ==
*Sam Rockwell as Eric Bowen
*Jared Harris as Carrigan Burke
*Rosemarie DeWitt as Amy Bowen
*Saxon Sharbino as Kendra Bowen
*Kyle Catlett as Griffin Bowen
*Kennedi Clements as Madison Bowen
*Nicholas Braun as Boyd
*Susan Heyward as Sophie Jane Adams as Dr. Claire Powell
* Soma Bhatia as Lauren

== Production ==
In early September 2013, the crew shot the interior scenes for the film in an old residence in Toronto.  Exterior shots were filmed on the West Mountain of Hamilton, Ontario|Hamilton.  The film will be released in 3D film|3D. 

== Release ==
On August 6, 2014, the films release date was shifted from February 13, 2015, to July 24, 2015.  On March 4, 2015, the date was shifted again to May 22, 2015, which was previously set for Spy (2015 film)|Spy.  It will be screened in 3D film|3D.  

=== Marketing ===
The films first trailer was released on February 5, 2015.    Forrest Wickman of Slate Magazine opinion was that the trailer made the film appear to be too similar to the original film.  James Hibberd of Entertainment Weekly said that the trailer "retains and amplifies several elements from the original", and praised that "the modernizing doesn’t result in, say, the family’s daughter being kidnapped by ghosts in Snapchat".  Brad Miska of Bloody Disgusting stated that "while every fiber of my being wants to reject it,   actually looks pretty insane", and praised the trailers final shot.  Ben Kuchera of Polygon (website)|Polygon also opined that the trailer appeared to be similar to the original film, but that it "looks great, as a horror movie". 

==See also==
*List of ghost films

== References ==
 

== External links ==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 