Sun in the Last Days of the Shogunate
{{Infobox Film
| name           = Sun in the Last Days of the Shogunate
| image          =
| image_size     = 
| caption        = Japanese theatrical release poster
| director       = Yuzo Kawashima
| producer       = 
| writer         = Yuzo Kawashima, Shōhei Imamura, Keiichi Tanaka
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Nikkatsu
| released       = July 14, 1957 http://www.jmdb.ne.jp/1957/cg002790.htm accessed 16 January 2009 
| runtime        = 110 minutes 
| country        = Japan
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1957 black-and-white Japanese film comedy directed by Yuzo Kawashima with a screenplay by Kawashima, Shōhei Imamura and Keiichi Tanaka. It was voted the fifth best Japanese film of all time in a poll of 140 Japanese critics and filmmakers conducted by the magazine Kinema Junpo in 1999. 

==Plot== last days of the Shogunate. Saheiji (played by comedian Frankie Sakai) seeks to outwit the inhabitants of a brothel in order to survive in straitened times.  A group of samurai meanwhile seek to destroy any foreigners that cross their path.

==Notes==
Parallels are drawn between the world of the samurai and the world of Kawashimas Japan. The hypocrisy surrounding prostitution, about to be outlawed in Japan at that time in 1950s Japan, the abuse of power, and financial greed at a time of crisis, are all assayed.  

== Cast ==
* Frankie Sakai as Saheiji
* Yōko Minamida as Koharu
* Sachiko Hidari as Osome
* Yujiro Ishihara as Takasugi Shinsaku
* Nobuo Kaneko as Denbei, the owner of the Sagami-ya
* Masumi Okada as Kisuke
* Tomio Aoki as Chusuke
* Shōichi Ozawa as Kinzō, the book lender
* Izumi Ashizawa as Ohisa Akira Nishimura as Shinkō
* Taiji Tonoyama as Kurazō
* Hideaki Nitani as Shidō Monta (a.k.a. Inoue Kaoru)
* Akira Kobayashi as Kusaka Genzui

== References ==
 

== External links ==
*  
* New remaster Ver(2011) http://www.nikkatsu.com/bakumatsu/english.html

 
 
 
 
 
 
 
 
 
 
 

 
 