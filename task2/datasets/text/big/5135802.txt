Eaten Alive
 
:For the television special, see Eaten Alive (TV special).
 
{{Infobox film
| name           = Eaten Alive
| image          = Eatenaliveposter.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Tobe Hooper
| producer       = Alvin L. Fast Larry Huly Robert Kantor Mardi Rustam Mohammed Rustam Samir Rustam
| writer         = Kim Henkel Alvin L. Fast Mardi Rustam
| starring       = Neville Brand Mel Ferrer Carolyn Jones Marilyn Burns
| music          = Wayne Bell Tobe Hooper
| cinematography = Robert Caramico
| editing        = Michael Brown
| studio         = Mars Productions Corporation
| released       = May 1977  (USA) 
| runtime        = 91 minutes
| country        = United States
| language       = English
}}
 horror film, William Finley, Marilyn Burns, Janus Blythe and Kyle Richards.

==Plot==
After refusing a request from frisky customer named Buck (Robert Englund), naive prostitute Clara Wood (Roberta Collins) is evicted from the town brothel by the madam, Miss Hattie (Carolyn Jones). Clara makes her way to a decrepit hotel, called the Starlight Hotel, located deep in a swamp, where she encounters the middled-aged, mentally disturbed proprietor Judd (Neville Brand) and his pet Nile crocodile in the swamp beside the porch. Upon realizing Clara was a prostitute, the sexualy frustrated Judd attacks her while ranting and raving. When Clare fights back, Judd stabs her with a garden rake and feeds her dead body to the crocodile.
 William Finley), arrive at the hotel, along with their young daughter Angie (Kyle Richards). They soon experience the trauma of Angies overly curious pet dog being eaten by the crocodile. Faye and Roy take their daughter to their room to try to calm her down. 

Meanwhile, Harvey Wood (Mel Ferrer) and his daughter Libby (Crystin Sinclaire) arrive at the hotel seeking information on Clara, Harveys runaway daughter. Judd denies ever meeting Clara to them. Harvey and Libby soon leave after checking into the hotel to try locate Clara at the brothel. 

Roy decides to shoot the crocodile for eating their dog. Judd intervienes and during the struggle, Roy shoots Judd in his right leg, only it is revealed that Judds leg is a wooden prostetic. During the fight, Roy is stabbed by Judd with a scythe and he is then attacked and devoured by the crocodile. After getting Angie to sleep, Faye goes into the bathroom for a bath, where she is interrupted by Judd, who begins to beat her. Angie wakes up and flees the hotel, but is trapped underneath the building by Judd, who proceeds to tie up Faye to a bed in a vacant room at the hotel.

Accompanied by Sheriff Martin (Stuart Whitman), Harvey and Libby question Miss Hattie, who denies ever seeing Clara. Harvey returns to the hotel alone while Libby stays in town to eat dinner with the Sheriff. After ariving back, Harvey hears Angies crying, and while investigating is attacked by Judd, who kills him by stabbing him in the neck with his scythe. Judd then drags Harveys body into the swamp where the crocodile eats it. 

While Sheriff Martin and Libby are at the bar having dinner and drinks, Martin kicks out Buck and his girlfriend Lynette (Janus Blythe) after a fight nearly breaks out. The pair venture to the hotel, much to the annoyance of Judd who had earlier warned Buck to stay away from his land. While in their room, they hear Angies cries for help. Buck goes to investigate and is pushed into the swamp by Judd, where he is quickly devoured. Lynette hears the commotion, and goes outside only to be chased through the swamp by the scythe-wielding Judd. She manages to get to a road where she flags down a passing car and gets away.

Libby arrives back at the hotel and goes up to her room. Judd arrives back also, and opens up the gate for the crocodile to get in under the house so it can eat Angie. Meanwhile, Libby discovers Faye tied up in the vacant room. After untying her, the women attempt to leave, however Judd chases them back upstairs where he wounds Faye with the scythe. Libby escapes and begins to help Angie out of the swamp she has managed to get into. Judd throws Faye off a balcony, before he attempts to push Angie back into the swamp while struggling with Libby, but Faye arrives and pushes Judd over the wire fence into the swamp, where he is devoured by his pet crocodile. Sheriff Martin arrives at the hotel to arrest Judd after being called upon by Lynette only to find the emotionally shaken Libby and Angie along with the wounded Faye. All that is left of Judd is his wooden leg floating in the swamp beside the hotel.

==Cast==
*Neville Brand as Judd
*Mel Ferrer as Harvey Wood
*Carolyn Jones as Miss Hattie
*Marilyn Burns as Faye William Finley as Roy
*Stuart Whitman as Sheriff Martin
*Roberta Collins as Clara
*Kyle Richards as Angie
* Janus Blythe as Lynette
*Robert Englund as Buck

==Production==
The film was shot entirely on soundstages at Raleigh Studios in Hollywood, California. The plot was very loosely based on the story of Joe Ball (also known as the Bluebeard from South Texas or the Alligator Man) who lived in Elmendorf, Texas, in the 1930s after Prohibition ended. He owned a bar with an alligator pit serving as an entertainment attraction. Several murders of women ensued, but it was never proven that the flesh found in the pit was human. Joe Ball committed suicide upon possibility of capture.

==Release==

===Video nasty=== video nasties" in the UK during the height of the issue during 1983 and 1984. Mary Whitehouse took a personal objection to this movie. 

===Critical reception===
 
Eaten Alive has received a negative response from critics, and currently holds a 18% approval rating on movie review aggregator website Rotten Tomatoes based on ten reviews.  An African hand-made poster for the movie is included in the book Extreme Canvas: Hand-Painted Movie Posters from Ghana by Ernie Wolfe.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 