Don't Go Near the Park
{{Infobox Film
| name           = Dont Go Near the Park
| image          = dont go near the park.jpg
| image_size     = 
| caption        = Dont Go Near the Park US DVD cover
| director       = Lawrence D. Foldes
| producer       = Lawrence D. Foldes
| writer         = Linwood Chase Lawrence D. Foldes
| narrator       =  Tamara Taylor Chris Riley
| cinematography = William DeDiego
| editing        = Dan Perry
| distributor    = Cardinal IV Film Distributors
| released       =   
| runtime        = 80 mins
| country        =   USA English 
}}
Dont Go Near the Park (also known as Curse of the Living Dead, Nightstalker and Sanctuary for Evil) is a 1981 American horror film directed by Lawrence D. Foldes. The film gained notoriety when it was successfully prosecuted in the UK and placed on the video nasty list.

==Synopsis==
After being cursed by their mother, a caveman brother and sister are forced to live their lives by preying on young people in a park and devouring their entrails in order to stay young.  After 12,000 years of killing they attempt to lift the curse and become immortal by finding a virgins soul they can sacrifice, with the brother fathering a daughter for that purpose.

== Critical reception ==

Allmovie wrote, "There are movies that are bad and then there are movies so bad they are mindblowing. Dont Go Near the Park is a perfect fit in the latter category."  

==Cast==
*Aldo Ray as Taft
*Meeno Peluce as Nick Tamara Taylor as Bondi
*Barbara Monker as Patty / Tra / Griffiths Wife / Petranella
*Crackers Phinn as Mark / Gar
*Linnea Quigley as Bondis Mother Chris Riley as Cowboy

== References ==

 

==External links==
* 
* 

 
 
 
 


 