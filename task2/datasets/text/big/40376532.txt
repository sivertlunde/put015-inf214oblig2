The Ride (2010 film)
{{Infobox film
| name           = The Ride
| image          = File:TheRidePoster.png
| caption        = 
| director       = Meredith Danluck
| producer       = 
| writer         = 
| starring       = 
| music          = The Weight
| cinematography = Jake Burghart
| editing        =  VICE Films
| distributor    = 
| released       =  
| runtime        =  91 min
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The Ride is a documentary on professional bull riders.   It was produced by Vice Media and directed by Meredith Danluck, after originally being created for VBS.tv. It premiered at the SXSW Film Festival in 2010.

==Synopsis==
With event footage, bull riding, pyrotechnics, and big arena rock and roll, The Ride goes through the lives of professional cowboys  on the Professional Bull Riders circuit. The circuit is a tour of the top 45 bull riders in the world, consisting of roughly 32 stops a year around the US. The winner of the world title in Vegas also wins a million dollars. 

==Cast==
*JB Mauney - professional bull rider
*Willy Ropp - Amish bull rider
*Flint Rassmussen - PBR entertainer 
*Shorty Gorham - bull fighter 
*Leann Hart - singer songwriter 
*Tom Teague - millionaire bucking bull breeder

==Production==
Filmed in in Spring of 2008, it has an original score by Brooklyn band The Weight. Director Meredith Danluck and DP Jake Burghart both previously worked on the VBS.tv showcase entitled Toxic Garbage Island. It was produced by VICE Films  as a presentation of a Jeff Yapp production. 

==Release, reception==
It premiered in March 2010 at the SXSW Film Festival.  According to a review in the Austinist, "People are the soul of a well-told story and Danluck is enamored with her down-home characters at the expense of a deeper look at the sport, its history and rules. Danluck’s not-quite-novel glimpses into rural southern culture eclipse the beautifully shot ringside action. She’s hooked on the rugged romance of cowboydom, from cattle to Coors, and less transfixed by the intricacies that lead these people into the bull’s eye." 

== References ==
{{Reflist| refs =
 {{cite news
| title       = SXSW Movie Trailer and Preview: The Ride (An Exclusive Look at VICE Films’ Take on the Modern American Cowboy)
| first       = 
| last        = 
| url         = http://www.slashfilm.com/sxsw-movie-trailer-and-preview-the-ride-an-exclusive-look-at-vice-films-take-on-the-modern-american-cowboy/
| newspaper   = 
| publisher   = Slashfilm
| date        = 
| accessdate  = 2013-08-28
}} 

 {{cite news
| title       = The Ride
| url         = http://twitchfilm.com/2010/03/the-ride.html
| newspaper   = 
| publisher   = TwitchFilm
| date        = March 2010
| accessdate  = 2013-08-12
}} 

 {{cite news
| title       = SXSW Film Interview: Meredith Danluck Want to Take You For a Ride
| first       = Chris
| last        = Garcia
| url         = http://austinist.com/2010/03/15/sxsw_film_interview_meredith_danluc.php
| newspaper   = Austinist
| publisher   = 
| date        = March 12, 2010
| accessdate  = 2013-08-12
}} 

 {{cite news
| title       = Review: "The Ride"
| first       = 
| last        = 
| url         = http://variety.com/2010/film/reviews/the-ride-1117942522/ Variety
| publisher   = 
| date        = April 1, 2010
| accessdate  = 2013-08-12
}} 

<!-- 
 {{cite news
| title       = 
| first       = 
| last        = 
| url         = 
| newspaper   = 
| publisher   = 
| date        = 
| accessdate  = 2013-08-12
}} 
-->
 {{cite news
| title       = Vice Film’s "The Ride" Saddles Up Bulls and Winnebagos on a Cowboy Cross-Country Quest
| first       = 
| last        = 
| url         = http://www.museyon.com/blog/2010/02/19/vice-films-the-ride-saddles-up-bulls-and-winnebagos-on-a-cowboy-cross-country-quest/
| newspaper   = 
| publisher   = Museyon Guides
| date        = February 19, 2010 
| accessdate  = 2013-08-12
}} 

   
}}

==External links==
* 
* 
 

 
 
 
 
 
 


 