Hum Se Na Takrana
{{Infobox film
 | name = Hum Se Na Takrana 
 | image = Hum Se Na Takrananew.jpg
 | caption = DVD Cover
 | director = Deepak Bahry
 | producer = Deepak Bahry
 | writer = 
 | dialogue = 
 | starring = Dharmendra Mithun Chakraborty Shatrughan Sinha Kimi Katkar Anita Raj
 | music = Laxmikant-Pyarelal
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = June 01, 1990
 | runtime = 135 min.
 | language = Hindi Rs 5 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1990 Hindi Indian feature procuced and directed by Master Bhagwan

==Plot==

Hum Se Na Takrana is an action film starring Dharmendra and Mithun Chakraborty in lead roles. The Story shows a young former losing all his land through a forged contract.

==Cast==
*Dharmendra
*Shatrughan Sinha
*Mithun Chakraborty
*Anita Raj
*Kimi Katkar
*Ranjeet
*Leena Das
*Master Bhagwan
*Dinesh Thakur

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Hum Se Na Takrana"
| Mohammed Aziz, Shailender Singh
|-
| 2
| "Jitne Ghungroo Jadhe"
| Kavita Krishnamurthy, Anupama
|-
| 3
| "Mera Pyar Hai Tera Vada"
| Kavita Krishnamurthy
|-
| 4
| "Mata Tere Dar Pe"
| Nitin Mukesh, Shabbir Kumar, Shailender Singh, Kavita Krishnamurthy
|-
| 5
| "Iska Naam Jawani Hai"
| Mohammed Aziz, Kavita Krishnamurthy
|-
| 6
| "Sun O Mere Humjoli"
|  Mohammed Aziz, Kavita Krishnamurthy
|}

==References==
* http://www.imdb.com/title/tt0359444/
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Hum+Se+Na+Takrana

==External links==
*  

 
 
 
 

 