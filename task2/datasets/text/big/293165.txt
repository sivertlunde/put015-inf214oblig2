Gigli
 
 
 
{{Infobox film
| name           = Gigli
| image          = Gigliposter.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Martin Brest
| producer       = {{Plainlist|
* Martin Brest
* Casey Silver}}
| writer         = Martin Brest
| starring       = {{Plainlist|
* Ben Affleck
* Jennifer Lopez
* Justin Bartha}} John Powell
| cinematography = Robert Elswit
| editing        = {{Plainlist|
* Julie Monroe
* Billy Weber}}
| studio         = {{Plainlist|
* Revolution Studios Casey Silver Productions
* City Light Films}}
| distributor    = Columbia Pictures
| released       =  
| runtime        = 121 minutes  
| country        = United States
| language       = English
| budget         = $75.6 million    
| gross          = $7.3 million 
}}
Gigli ( ,  ) is a 2003 American romantic comedy film written and directed by Martin Brest and starring Ben Affleck, Jennifer Lopez, Al Pacino, Christopher Walken, and Lainie Kazan.
 worst films ever made. The film was also a box office bomb, grossing back only $7.2 million against a $75.6 million budget.

==Plot== kidnap the mentally challenged younger brother of a powerful federal prosecutor to save New York-based mob boss Starkman (Al Pacino) from prison. Gigli successfully convinces the young man, Brian (Justin Bartha), to go off with him by promising to take him "to the Baywatch", which seems to be Brians singular obsession, and turns out to just be the beach. The man who ordered the kidnapping, Louis (Lenny Venito), does not trust Gigli to get the job done right, so he hires a woman calling herself Ricki (Jennifer Lopez) to take charge.
 slits her wrists and has to be rushed to the hospital. While at the hospital, Gigli goes to the morgue and cuts off a corpses thumb, which he sends to his boss as Brians thumb. Gigli and Ricki go back to his apartment where Gigli confesses his love, and the two sleep together.

They are summoned to meet with the mobs boss. Starkman reveals that he didnt approve of the plan to kidnap a federal prosecutors brother and scolds them because the thumb they sent wont match Brians fingerprint; he then kills Louis. Starkman is about to kill Ricki and Gigli as well, but Ricki talks him out of it. They decide to take Brian back to where they found him. On the way, they discover Baywatch (or a similarly themed show or film) shooting an episode on the beach. They leave a happy Brian there. At the last minute, Ricki decides to leave town with Gigli, who finally reveals her real name - Rochelle.

==Cast==
* Ben Affleck as Larry Gigli
* Jennifer Lopez as Ricki/Rochelle
* Justin Bartha as Brian
* Lainie Kazan as Mrs. Gigli
* Al Pacino as Starkman
* Lenny Venito as Louis
* Christopher Walken as Detective Stanley Jacobellis
* Missy Crider as Robin Terrence Camilleri as Man in dryer

==Production== Long Beach and Los Angeles, California.

==Release==
===Box office===
Gigli grossed $3,753,518 in its opening weekend from 2,215 theaters averaging $1,694 per theater and ranking #8 at the box office. The film set a record to date for the biggest Second weekend in box office performance|second-weekend drop in box office gross of any film in wide release since that statistic was kept; it dropped by 81.9% in its second weekend compared to its first, grossing $678,640.  By its third weekend in release, only 73 US theaters were showing it, a 97% drop from its first weekend. The film ultimately earned $6,087,542 domestically and $1,178,667 internationally for a total of $7,266,209 on a $75.6 million production budget. 

The film was withdrawn from US theaters after only three weeks (one of the shortest circulation times for a big-budget film), earning a total of only $6 million domestically and $1 million abroad. In the United Kingdom, the film was dropped by virtually every cinema after critics panned it. 

In 2014, The Los Angeles Times listed the film as one of the most expensive box office flops of all time. 

===Critical reception===
Gigli was panned by critics. On   based on 37 critics, indicating "overwhelming dislike". 

On Ebert and Roeper, critics Roger Ebert and Richard Roeper both gave the film thumbs down, although Ebert showed some sympathy towards the film, stating it had "clever dialogue", but was "...too disorganized for me to recommend it". Roeper called the film "a disaster" and "one of the worst movies Ive ever seen". He then included Gigli on his 100 worst films of the decade at #7. 

Ebert and   and  , Jen & Ben offer less pain." 

Entertainment Weekly s Owen Gleiberman gave the film a "C+", stating "A watchable bad movie, but its far from your typical cookie-cutter blockbuster. There are no shoot-outs or car chases, and there isnt much romantic suspense, either." 

One of the few positive reviews came from Amy Dawes of Variety (magazine)|Variety, who wrote that the story was ludicrous and that the film would tank, but that on balance she found it a fun film with several good performances. 

===Awards and nominations=== Razzies in Worst Picture, Worst Actor, Worst Actress, Worst Director, Worst Screenplay and Worst Screen Couple. A year later, the film won a seventh Razzie for "Worst Comedy of Our First 25 Years". The film was also nominated for eleven and received five Stinkers Bad Movie Awards in 2003; Worst Actor and Worst Fake Accent - Male, Worst Actress and Worst Fake Accent - Female and Worst On-Screen Couple. 

==Legacy== Mets are doing so badly that they will be renamed The New York Gigli."
 satirical newspaper, ran an article about the film, titled "Gigli focus groups demand new ending in which Affleck and Lopez die." 

==See also==
 
* List of films considered the worst
* List of films featuring diabetes

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 