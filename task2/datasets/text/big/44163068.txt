Mansoru Maha Samudram
{{Infobox film 
| name           = Mansoru Maha Samudram
| image          =
| caption        =
| director       = PK Joseph
| producer       =
| writer         = Kanam EJ
| screenplay     = Kanam EJ Aruna K. P. Ummer
| music          = M. K. Arjunan
| cinematography =
| editing        = K Sankunni
| studio         = KMM Movies
| distributor    = KMM Movies
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, Aruna and K. P. Ummer in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
*Mammootty
*Ratheesh Aruna
*K. P. Ummer Seema

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Kanam EJ. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kunnin purangalil || K. J. Yesudas || Kanam EJ || 
|-
| 2 || Manassoru Samudram || K. J. Yesudas || Kanam EJ || 
|-
| 3 || Suravalli vidarum || K. J. Yesudas || Kanam EJ || 
|-
| 4 || Thudakkam || S Janaki, Chorus || Kanam EJ || 
|}

==References==
 

==External links==
*  

 
 
 

 