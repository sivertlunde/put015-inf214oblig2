Thaer Thiruvizha
 
{{Infobox film
| name           = THAER THIRUVIZHA   
| image          = 
| caption        = 
| director       = M.A.Thirumugham 
| producer       = « Sandow » M.M.A.Chinnappa Devar
| writer         = Madurai Thirumaran 
| story          = « Sandow » M.M.A.Chinnappa Devar 
| starring       = M. G. Ramachandran Jayalalitha C. R. Vijayakumari S. A. Asokan R. Muthuraman Nagesh
| music          = K. V. Mahadevan
| cinematography = N.S.Varma
| editing        = M.A.Thirumugham
| studio         = Devar Films
| distributor    = Devar Films
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
}} 1968 Tamil language drama film directed by M.A.Thirumugham. 
The film features M. G. Ramachandran, Jayalalitha, C. R. Vijayakumari and S. A. Ashokan in lead roles.

==Plot==

Between the campaign and the Tamil capital...

By means of his Indian coracle, Saravanan (MGR), a big-hearted boatman assures small connections and makes in this way his living.

His younger sister Sivagami (C. R. Vijayakumari) in fact so much. Thats the way it goes, with Parvathi Ammal (S. N. Lakshmi), their mother, that they love.

When Saravanan has to gather endows her with Sivagami, he leaves his province for the neighboring town.

During his absence, a film unit, under the leadership of spirited one director Muthu (R. Muthuraman) (come for locations) seduces Sivagami and rejected then without arousing her suspicions, by getting back to the capital.

Parvathi Ammal discovers the pregnancy of her daughter Sivagami who wants at all costs to wash her dishonor.

She leaves for Madras to find Muthu. She meets accidentally on Vaalee (Jayalalitha) who is to be the beloved of Saravanan and become friendly (Both young ladies, on the other hand, do not know that they are a sister-in-law due to Saravanan).

Vaalee, when to her, a saleswoman of lassi, with the well dipped character, had to resolve to avoid the ceaseless and more and more indecent harassings of her maternal uncle Somu (S. A. Ashokan), a gallows bird, by coming to hide in the big city.

This last one also moves into the metropolis.

Informed the lot of his younger sister, Saravanan sees his mother succumbing in the confusion.

The older brother decides in his turn to join Sivagami and to look for the man who is at the origin of all their misfortunes : Muthu !

==Cast==
{| class="wikitable" width="72%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran || as Saravanan and MGR (For a scene)
|-
| Nagesh || as Vel, the producer
|-
| S. A. Ashokan || as Solaimulanga / Somu
|-
| R. Muthuraman || as The Cinema Director Muthu / Ram 
|-
| Sandow M. M. A. Chinnappa Thevar || as A goon
|-
| Jayalalitha || as Vaalee alias Vanamalai
|-
| C. R. Vijayakumari || as Sivagami, Saravanan s sister
|- Manorama || as Vels wife
|-
| S. N. Lakshmi (Guest-star)  || as Parvathi Ammal, Saravanan and Sivagami s mother
|-
| Seetha Lakshmi (Guest-star)  || as Vaalee s mother
|-
| S.M.Thirupadhiswamy || as The Police officer
|-
| Tiruchi Selandhar Rajan || as The Cameraman
|}

The casting is established according to the original order of the credits of opening of the movie, except those not mentioned

==Around the movie==

Again on the concept of the relation elder brother - younger sister.

The actress C.R.Vijayakumari plays the younger sister of MGR twice during year 1968.
The second will be in KANAVAN, in August.

The movie speaks among others about the universe of the cinema.

In THAER THIRUVIZHA, MGR makes it a small intervention.

He plays it without own role.
 Manorama and Nagesh in the famous scene(stage) of the play.

THAER THIRUVIZHA : a movie in the movie !

The film, produced by Sandow M. M. A. Chinnappa Thevar under his famous Devar Films, had musical score by K. V. Mahadevan, as usual.

It is the fourteenth Devar Films, with MGR at the head of poster.

==Soundtrack==
The music composed by K. V. Mahadevan

==External links==
*  

 
 
 
 
 


 