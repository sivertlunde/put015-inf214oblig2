Wake Me When It's Over (film)
{{Infobox film
| name           = Wake Me When Its Over
| image_size     =
| image	         = Wake Me When Its Over FilmPoster.jpeg
| caption        =
| director       = Mervyn LeRoy
| producer       = Mervyn LeRoy
| writer         = Howard Singer (novel)
| based on       = Wake Me When Its Over
| screenwriter   = Richard L. Breen
| narrator       =
| starring       = Ernie Kovacs Dick Shawn Margo Moore
| music          = Cyril J. Mockridge
| cinematography = Leon Shamroy
| editing        = Aaron Stell
| studio         = Mervyn LeRoy Productions Inc.
| distributor    = Twentieth Century-Fox
| released       = April 8, 1960
| runtime        = 126 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $2,000,000 (US/ Canada) 
}}

Wake Me When Its Over is a 1960 comedy film starring Ernie Kovacs and Dick Shawn. A World War II veteran gets called back into service by mistake and sent to a dreary Pacific island. It is based on the novel of the same name by Howard Singer.

==Plot== Air Force. Gus is reluctant because he was shot down and became a prisoner of war, but the military had listed him as killed. A red-tape foulup results in Gus being back in uniform, assigned to a ramshackle radar station on a backwater island near Japan. The airmen assigned there are apathetic, slovenly, and unmotivated. Its equipment and supplies are a collection of junk, abandoned or surplus.
 Air Force pilot, is in charge. His superiors have all but forgotten the base is still on the island. Gus gets to know Ume Tanaka (Nobu McCarthy), daughter of the villages unfriendly mayor, who shows him a pool of natural hot springs. Gus and Charlie conspire to open a resort hotel, using the men as labor and the broken down equipment as materials, with Doc Farringtom (Jack Warden) scamming journalist Joab Martinson (Robert Emhardt) about the waters "healing powers" to gain free publicity.

Doc summons no-nonsense Lt. Nora McKay (Margo Moore) to lend a womans touch to the project and Charlie develops a romantic interest in her. The airmen, including Charlie, are motivated by the project and their pretty young lieutenant, become a military outfit again, and construct a first-class facility, the Hotel Shima. Nora staffs the hotel with 40 young women from the village, and following local custom, the girls are "sold" for two years to Gus as their "papa-san" at the insistence of their fathers. When Martinson gets drunk and embarrasses himself in front of all the guests, he vindictively writes a story painting the hotel as a den of sin.
 congressional panel from Washington D.C. also launches an investigation, Charlie ends up buzzing the trial in a jet as Capt. Farrington blackmails Colonel Hollingsworth with the knowledge that he received Hotel Shima supplied goods. Stark ends up testifying on Brubakers behalf while all sorts of crazy antics occur during the trial. Stark and Lt. McKay later make up after an earlier disagreement broke them up.

Ultimately Brubaker is found not guilty on one count, but guilty of taking government property. During sentencing, it is discovered that the court has tried the wrong man due to the earlier government error. Stymied, the panel finally decides to find Brubaker not guilty and leave the hotel to the people of the island. As Brubaker says goodbye to Ume and he sets off to leave he sees that Colonel Hollingsworth has been assigned to the base in his place. Ume waves goodbye as Brubaker leaves.

==Cast==
* Ernie Kovacs as Captain Charlie Stark
* Dick Shawn as Gus Brubaker
* Margo Moore as Lieutenant Nora McKay
* Jack Warden as Captain Dave "Doc" Farrington
* Nobu McCarthy as Ume Tanaka (as Nobu Atsumi McCarthy)
* Don Knotts as Sergeant Percy Warren Robert Strauss as Sergeant Sam Weiscoff
* Noreen Nash as Marge Brubaker
* Parley Baer as Colonel Archie Hollingsworth
* Robert Emhardt as Joab Martinson
* Marvin Kaplan as Hap Cosgrove
* Tommy Nishimura as Private Jim Harigawa
* Raymond Bailey as General Weigang
* Vin Scully as a CBS reporter
* Judy Dan as Geisha Girl (uncredited)

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 