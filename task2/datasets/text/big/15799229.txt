The Dead Sleep Easy
 
 
{{Infobox film
| name           = The Dead Sleep Easy
| image          = TheDeadSleepEasy poster.jpg
| caption        = 
| director       = Lee Demarbre Robert Menzies
| writer         = Ian Driscoll Ian Hodgkinson Dave Courtney Ana Sidel Martin Kove 
| music          = Michael Dubué 
| cinematography = 
| editing        = 
| distributor    = Odessa Filmworks Zed Filmworks
| released       =  
| runtime        = 
| country        = Canada
| language       = English CAD 250,000   
| gross          = 
}}
The Dead Sleep Easy is a 2007 Canadian drama film, a co-production of Odessa Filmworks and Zed Filmworks produced on location in Guadalajara, Jalisco, Mexico. Its tagline is "When youre this far south, sometimes its better to be dead than alive."

==Plot== Ian Hodgkinson) who becomes employed by a Mexican organised crime gang after he kills an opponent whose uncle is a mob leader. After witnessing a massacre of illegally smuggled migrants into the United States, The Champ decides to seek redress for this crime.

==Cast== Ian Hodgkinson - The Champ
* Dave Courtney - Tlaloc
* Ana Sidel - Maya
* Martin Kove - Bob DePugh
* Phil Caracas - Carlito Alex Lora
* Aaron Katz - Minuteman
* Talia Russo - Bunny
* Pedro Fendez - Hector

==Distribution and release==
The film has had numerous special screenings:

* 22 September 2007: Calgary, Calgary International Film Festival  Victoria (British Columbia) Film Festival 
* 15 February 2008: Toronto 
* 22 February 2008: Ottawa
* 13 March 2008: Montreal 
* 15 March 2008: Philadelphia, Pennsylvania, Backseat Film Festival 
* 20 March 2008: Dawson City, Yukon, International Short Film Festival 
* 27 March, 28 March 2008: Hollywood, Los Angeles, California|Hollywood, California, Ricardo Montalbán Theatre 

The film was given a DVD release on 5 March 2009 through Anchor Bay Canada.

==References==
 

===Bibliography===
* 
*  
*  
*  
*  
*  
*  
* Rue Morgue Radio:   8 February 2008
*  
*    
*  

== External links ==
*  
*  

 
 
 
 
 
 
 