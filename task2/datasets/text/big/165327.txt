All My Babies
{{Infobox film
| name           = All My Babies
| image          =
| star           = Mary Frances Hill Coley 
| director       = George C. Stoney
| producer       = George C. Stoney
| writer         = George C. Stoney
| narrator       = 
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}} midwives in Georgia Department of Public Health, and written by  Stoney. The film follows Mary Francis Hill Coley (1900–66), an African American midwife from Albany, Georgia who helped deliver over 3,000 babies in the middle part of the 20th century.  
 legal oversights and eventual elimination of lay midwifery (also called Midwifery#Direct-entry_midwives|direct-entry midwifery) in many States. 

In 2002, this film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". The film is the subject of a short 2006 article by Christine DellAmore.   Photographic stills by Robert Galbraith from the film formed the basis of an exhibition curated by Linda Janet Holmes called Reclaiming Midwives: Stills from All My Babies.  

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 

 