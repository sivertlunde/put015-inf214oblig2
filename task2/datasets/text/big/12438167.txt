Yellow Earth
{{Infobox film
| name           = Yellow Earth
| image          = YellowEarthPoster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Chen Kaige
| producer       =
| writer         = Chen Kaige Zhang Ziliang
| narrator       =
| starring       = Wang Xueqi
| music          = Zhao Jiping
| cinematography = Zhang Yimou
| editing        =
| distributor    =
| released       = 1984
| runtime        = 89 minutes
| country        = China Mandarin
| budget         =
| gross          =
}}
 1984 Chinese Chinese drama film. It was the directorial debut for Chen Kaige. The films notable cinematography is by Zhang Yimou.  At the 24th Hong Kong Film Awards ceremony on 27 March 2005, a list of 100 Best Chinese Motion Pictures was tallied, and Yellow Earth came in fourth.  The film was produced by Guangxi Film Studio ( ).

Zhang Yimou, a colleague of Chen, photographed the film. Richard James Havis, author of Changing the Face of Chinese Cinema: An Interview with Chen Kaige, said that the film was the first Chinese film "at least since the 1949 Communist Liberation, to tell a story through images rather than dialog." Havis p. 8.  Therefore the film attracted controversy in China. Havis added that the film "was also equivocal about the Communist Partys ability to help the peasants during the Communist revolution" which differed from the propaganda films that were produced after 1949." 

==Background==
Yellow Earth is set in Shaanxi, a province of central China, in early spring 1939. The Chinese Communist Party and the Kuomintang have stopped fighting one another and are working together to overwhelm the invading Empire of Japan|Japanese. The provinces are divided into Kuomintang-controlled areas and Communist-controlled areas. Shaanxi is divided into three sections: the northern and southern parts are controlled by the Kuomintang, and the middle by the Communists. Because of their co-operation, navigation between the areas is permitted.

==Plot==
Gu Qing, a soldier from the propaganda department of the CCP Eighth Route Army in CCP-controlled Shaanxi, travels alone from Yanan to the northern KMT-controlled area of Shaanxi, Shanbei, with the task of collecting the peasants folk songs in order to re-write them with communist lyrics in order to boost the morale of the Eighth Route Army soldiers.

Yellow Earth begins with a scene depicting a communist soldier walking several miles. He reaches a small village where he is assigned to live with a poor family with the task of recording local folk songs for use in the army later. He learns the hardships of peasant life and especially of that of a peasant girl named Cuiqiao. The story then focuses on the girl, who at only age 14 is forced to marry a significantly older man as her wedding dowry was used to pay for her mothers funeral and brothers engagement. The Communist soldier "Brother Gu" attempts to dissuade Cuiqiao from running off and joining the CCP, but recognizes her despair.  After her marriage, Cuiqiao crosses the Yellow River at night but it is unclear whether she made it or not. The story then fast forwards to a rain dance being performed by the villagers, as the land has dried up and peoples crops have died.

==Reception==
Richard James Havis, author of "Changing the Face of Chinese Cinema: An Interview with Chen Kaige," said that the film "immediately brought attention to" the "Fifth Generation" of Chinese filmmakers.  The films international premiere occurred in the 1985 Hong Kong International Film Festival. Havis said that the film "proved a sensation" there. 

==References== Cineaste - Americas Leading Magazine on the Art and Politics of the Cinema. 2003. Volume 29, Issue 1. p.&nbsp;8 - 11. ISSN 0009-7004. Available at ProQuest.

==Notes==
 

==External links==
 
*  
*  
*  at the Chinese Movie Database
* 

 
  

 
 
 
 
 
 
 
 