King for a Day (1983 film)
{{Infobox film
| name = Господин за един ден Gospodin za edin den ( )
| image = Gospodin za edin den 1983.jpg
| caption = 
| imagesize = 
| director = Nikolay Volev
| writer = Nikola Statkov
| cinematography =  Krasimir Kostov Todor Kolev Itzhak Fintzi Yordanka Stefanova Stoyan Gadev Ivan Grigorov
| music = Ivan Staykov Bulgarian Cinematography Studios for feature films „Boyana“
| released = 1983
| country = Bulgaria
| runtime = 87 minutes
| language = Bulgarian
}}
 Todor Kolev and Itzhak Fintzi. The screenplay is written by Nikola Statkov based on his short stories “The Outlander” and “The Mister”.

The main character Purko (Todor Kolev) is a poor peasant with many children, constantly starting extravagant initiatives to get out of poverty during the hard times between the two world wars. The only consolation he finds in the music with his clarinet and his inborn musical talent until one day he meets an elegant couple from a town. They promise him prosperity if he mortgage his house and invest the money in their business. 
 The Double (1980) directed also by Nikolay Volev and Dangerous Charm (1984) directed by Ivan Andonov. The performance by Kolev, with a reverence to the great comedians of the silent cinema,  received a broad critical acclaim.

==Cast== Todor Kolev as Asparuh Kanchev - Purko
*Itzhak Fintzi as the tax-collector 
*Yordanka Stefanova as Purkos wife
*Stoyan Gadev as the priest
*Ivan Grigorov as Mito the barefooted  (a fellow-villager) 
*Nikola Pashov as the village mayor
*Ivan Obretenov as a fellow-villager
*Trifon Dzhonev as Bay Linko, the tavern-keeper
*Veliko Stoyanov
*Pavel Popandov
*Georgi Mamalev as the engineer Kerkelezov (eccentric inventor)
*Boris Radinevski
*Kina Mutafova

==References==
 

===Sources===
*    
*    

==External links==
*  
*   at the Bulgarian National Television  

 
 
 
 