Keane (film)
{{Infobox film
| name            = Keane
| image           = Keane movie poster.jpg
| director        = Lodge Kerrigan  Brian Bell
| writer          = Lodge Kerrigan
| starring        = Damian Lewis Abigail Breslin Amy Ryan  John Foster
| editing         = Andrew Hafitz
| distributor     = Magnolia Pictures 
| released        =  
| runtime         = 100 minutes
| country         = United States
| language        = English
| budget          = $850,000   
| gross           = $394,390 (Worldwide)    
|}}
Keane is a 2004 American drama film written and directed by Lodge Kerrigan. Set in New York City, it focuses on a mentally disturbed man trying to come to terms with the abduction of his daughter several months earlier and the relationship he develops with a young girl and her mother.

The film premiered at the 2004 Telluride Film Festival and was shown at the Toronto International Film Festival, the New York Film Festival, the 2005 Cannes Film Festival, and the Deauville Film Festival, where it won both the Critics Award and the Jury Special Prize, before it went into limited theatrical release in New York City on September 9, 2005.

==Plot==
Searching for his missing daughter Sophia in the Port Authority Bus Terminal, from which she was abducted several months earlier, William Keane confronts ticket agents and random passersby with a newspaper account of her disappearance, but no one recalls seeing the little girl. After spending the night wandering the streets and sleeping along the side of the highway, he returns to the cheap hotel where he is living and finds he is unable to get into his room. The desk clerk tells him his payment is in arrears, and Keane covers the cost of another weeks stay with a disability check.

Alone in his hotel room, Keane drinks beer and talks to himself about his ex-wife and the birth of their daughter, and he reads the clippings about another abducted New Jersey girl who was found and reunited with her parents he keeps in an envelope. He makes contact with a drug dealer and purchases cocaine, and the more he ingests the more paranoid he becomes, certain he is being followed and watched and even going so far as physically attacking a man he believed was watching him. He goes to a nightclub and snorts coke with a woman named Michelle, then has sex in a bathroom stall with her.

Back at his motel, Keane meets Lynn Bedik and her daughter Kira, who is close in age to his missing child. Lynn clearly is having financial difficulties, and he insists she take the $100 he offers her. She asks Keane to watch Kira for a few hours, then calls the motel and leaves a message she will not be returning that night as planned. Keane reassures a despondent Kira, who fears Lynn has abandoned her, that her mother loves her and will be back.

The following day, Keane takes Kira to a local indoor skating rink and teaches her how to ice skate. While they are playing skee ball in the adjacent arcade, Keane believes he is being watched by another patron and becomes agitated. Kira manages to calm him and they return to the motel. When Lynn arrives later, she explains she was with Kiras father Eric, who has arranged for them to move to Albany, New York, where he has found a job.

Desperate not to lose Kira because she reminds him so much of Sophia, Keane goes to her school, takes her without permission, and brings her with him to the Port Authority, allegedly to meet her mother there and board a bus to Albany. There he sends her to buy candies, as his daughter had done several months earlier, just  minutes before she was abducted. It seems as if Keane is reviving the tragic loss of his daughter, perhaps expecting the abductor to show up again and try this time to abduct Kira to  -as he was expecting him to show up every time he was visiting the station for all those months, imagining his plan and his schedule. This doesnt happen. He cries for his losses and decides to really get her to her mother. Kira tells him she loves him and he says he loves her too.

==Cast==
*Damian Lewis – William Keane
*Abigail Breslin – Kira Bedik
*Amy Ryan – Lynn Bedik
*Christopher Evan Welch – Motel Clerk
*Tina Holmes – Michelle
*Liza Colón-Zayas – 1st Ticket Agent
*John Tormey – 2nd Ticket Agent 
*Chris Bauer – Bartender

==Production==
A film about child abduction was prompted by Lodge Kerrigans fear of his own daughter disappearing during a shopping excursion. "I realized, how in just four minutes, four minutes! your child could be abducted. Your life could be changed forever and there would be no way to recover from it. I knew that kind of visceral feeling would be a good starting point."    The result was In Gods Hands, produced by Steven Soderbergh and starring Maggie Gyllenhaal and Peter Sarsgaard, which centered on the disintegration of a family after a child had been abducted. The film never was released due to "irreversible negative damage."  With nothing left of his film to salvage, Kerrigan began researching and writing a new project that became Keane. 

Kerrigan knew the location of the film would be a key plot element, and he wrote much of the script while walking around New York City, trying to get a feel for the main character and their surroundings. "It was exhilarating, working with that kind of energy. I also wrote a lot of the film there as well. I would go and play out a scene and try to act it out... to some degree try to find the emotions, by recreating, beat by beat, his mindset, the abduction of his daughter. It all goes back to location... when you are working in the actual area, you can answer all of the questions that would arise with much more immediacy." 

The film was shot with a handheld camera with single takes lasting up to four minutes with no Cutaway (filmmaking)|cutaways. It contains long periods of little or no dialogue and has no musical score. Shooting was completed within 32&nbsp;days,  and many of the Manhattan and North Bergen, New Jersey locations Kerrigan selected were remote, unfamiliar streets and backdrops used to help emphasise the downward spiral of the central character. "Poverty is part of it... people who suffer from mental illness are marginalized...and that place, the surrounding area, with all the confusion, the buses and people coming in and out...it is really an appropriate location." 

==Critical reception==
Keane has received 83% fresh ratings on the film review website rottentomatoes based on 59 reviews. 
Todd McCarthy of Variety (magazine)|Variety observed, "Watching Lewis so thoroughly inhabit the demented Keane, one can only wonder how an actor can live with such a character for weeks and weeks and maintain a semblance of sanity and contact with real life.   amazingly manages to find nuances of character while running his engine above the emotional red line throughout. Its a resonant, haunting performance." 

Kevin Thomas of the Los Angeles Times stated, "In a wholly unexpected and ultimately gratifying experience from writer-director Lodge Kerrigan, Keane is emotionally involving right from the beginning through its final frame . . . Aided by a most resourceful and empathetic cinematographer, John Foster, Kerrigan . . . seems to be trusting an intuition that never fails him. His focus naturally is primarily on Keane, yet he captures the relentlessly drab and impersonal urban landscapes in a way that reinforces the terrible isolation of Williams anguished odyssey. As a rigorous filmmaker, Kerrigan eschews conventional exposition, which raises the possibility that William may be sufficiently deranged to have imagined his daughters presumed abduction, perhaps even the childs existence. Tantalizing as this may sound, Kerrigan doesnt seem to be the kind of filmmaker whos playing with a tricky ambiguity, and Keanes recollections of the last moments leading up to his daughters disappearance seem too vivid to be anything but excruciatingly real." 

John McMurtrie of the San Francisco Chronicle called the film "taut and suspenseful" and said, "Damian Lewis delivers a convincing, powerful and highly nuanced performance . . . He lets the audience feel compassion for Keane, and he does so without a hint of sentimentality. Easier said than done." 

Peter Travers of Rolling Stone rated the film three out of four stars and commented, "Kerrigan is without peer at plumbing the violence of the mind. Keane means to shake us, and does." 

Meghan Keane of the New York Sun stated, "Some uncertainties - like whether his daughter was abducted, or even existed - are provoking, but most annoy. Keanes mental instability seems to wax and wane as is convenient for the narrative. He seems crazy when it makes for a compellingly awkward scene, and mentally competent when he wants something - like companionship or anonymous sex . . . By the end, the complete discomfort that Mr. Kerrigan has created seems like a forced experiment - one that may have been a rewarding challenge to create, but is difficult to watch, closer to asphyxiation than entertainment." 

==Awards and nominations==
John Foster was nominated for the Independent Spirit Award for Best Cinematography but lost to Robert Elswit for Good Night, and Good Luck.
 Gotham Award nominations went to Lodge Kerrigan for Best Film and Damian Lewis for the Breakthrough Award.

==Home media==
The film was released on Region 1 DVD on March 21, 2006. It is in anamorphic widescreen format with an English audio track and Spanish subtitles.

The DVD includes an alternate version of the film edited by executive producer Steven Soderbergh. Fifteen minutes shorter than the original, it is preceded by a message from Soderbergh that reads, "While I was away on location, Lodge sent me a copy of Keane to look at before he locked picture. I loved the film and told him so, but I also sent him this version to look at, in case it jogged anything (it didnt). In any case, we agreed it was an interesting (to us) example of how editing affects intent. Or something."

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 