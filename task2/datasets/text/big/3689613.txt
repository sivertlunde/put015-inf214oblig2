A Better Place
{{Infobox film
| name           = A Better Place
| image          = A_Better_Place.jpg
| image_size     = 215px
| alt            = 
| caption        = DVD cover
| director       = Vincent Pereira
| producer       = Vincent Pereira Paul C. Finn Scott Mosier Kevin Smith
| writer         = Vincent Pereira
| starring       = Eion Bailey Robert DiPatri
| music          = Michael Ferentino Andres Karu Ben Reed auto vs. pedestrian
| cinematography = Ian Dudley
| editing        = Vincent Pereira
| studio         = View Askew Productions
| distributor    = Synapse Films
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = $100,000
}}
A Better Place is a 1997 drama film written and directed by Vincent Pereira. It stars Robert DiPatri and Eion Bailey. It was produced in association with View Askew, Kevin Smiths production company, and released to DVD by Synapse Films. It was nominated for the Golden Starfish Award for Best American Independent Film at the 1997 Hamptons International Film Festival.

==Plot==
Barret Michaelson is an unwelcome newcomer in a public high school, often bullied by his new classmates. He has no friends until another misfit with a bad reputation, Ryan, saves him from a beating in the mens locker room. Ryan is a misanthropic existentialist with violent tendencies and a dark past. It is revealed that Ryans father murdered his mother and then committed suicide in front of Ryan when he was only ten years old.

The two become fast friends who spend much of their time together engaged in philosophical conversation, but their friendship comes to an abrupt halt when the two are involved in an incident with a local landowner who claims they are trespassing on his land. Ryan throws a rock at the man, causing him to fall and break his neck on a rock. The two manage to successfully make it look like an accident, but the incident forces Barret to pull away from his friendship with Ryan. This causes Ryan to become very emotional, and to purchase a black market gun. Barret soon agrees that they should put the incident behind them and continue to be friends, but Ryan becomes increasingly morose and attached to Barret.
 joint (marijuana cigarette). Barret tries to warn him, but it is to no avail, and Ryan kills him. After the killing, Barret tries to incapacitate Ryan by hitting on the head with a rock, but it doesnt work. In a struggle, the gun goes off, claiming Ryans life. Barret then shoots him once again, and tries to turn the gun on himself, but by that time the gun is out of bullets.

==Cast==
* Eion Bailey as Ryan
* Robert DiPatri as Barret Michaelson
* Joseph Cassese as Todd
* Carmen Llywelyn as Augustine
* Brian Lynch as Eddie
* Bryan Sproat as Justin
* Molly Castelloe as Meg Dennis Pepper as Dennis Pepper
* Vincent Pereira as Jake Richard Lynch as Mr. Raimi
* Scott Mosier as Larry Stan Dunbar as Large rude student

==Themes==
Themes include misanthropy, Introversion and extroversion|introversion, teen violence, suicide, fatalism, and existentialism.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 