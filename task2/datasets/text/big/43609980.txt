Last of the Comanches
{{Infobox film
| name           = Last of the Comanches
| image size     = 
| image = Last of the Comanches.jpg
| caption        = Theatrical release poster
| director       = André de Toth
| producer       = Buddy Adler
| writer         = Kenneth Gamet
| starring       = Broderick Crawford Barbara Hale
| music          = George Duning
| cinematography = Ray Cory Charles Lawton Jr.
| editing        = Al Clark
| distributor    = Columbia Pictures
| studio         = 
| released       =  
| runtime        = 85 minutes
| country        = USA
| language       = English
| budget         =  gross = 
}} World War Sahara starring Humprey Bogart.  Actor Lloyd Bridges appeared in both films.

==Plot==
With Sgt. Matt Trainor, (Broderick Crawford) as its Leading Commander, whats left of a massacred cavalry troop from the isolated frontier town Dry Buttes, along with a ragtag group of stagecoach passengers, fight for survival against fierce Comanches led by Black Cloud, (John War Eagle) at a desert ruin.

==Cast==
* Broderick Crawford as Sgt. Matt Trainor
* Barbara Hale as Julia Lanning
* Johnny Stewart as Little Knife
* Lloyd Bridges as Jim Starbuck
* Mickey Shaughnessy as Rusty Potter George Mathews as Romany ORattigan
* Hugh Sanders as Denver Kinnaird
* Ric Roman as Martinez
* Chubby Johnson as Henry Ruppert
* Martin Milner as Billy Creel
* Milton Parsons as Satterlee the Prophet
* Jack Woody as Cpl. Floyd
* John War Eagle as Black Cloud

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 

 