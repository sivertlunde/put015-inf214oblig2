Dum (2003 Hindi film)
 
{{Infobox Film
| name           = Dum
| image          = Duum.jpg
| caption        = Movie Poster
| director       = E. Nivas
| producer       = Ali Morani Karim Morani Dharani
| story          =  Dharani
| starring       = Vivek Oberoi Diya Mirza Atul Kulkarni Govind Namdeo
| music          = Sandeep Chowta
| cinematography =
| editing        =
| distributor    = Sony BMG
| released       = 24 January 2003
| runtime        =
| country        = India
| language       = Hindi
| gross          = Rs.245&nbsp;million
| Website        =
}}
 action film directed by E. Nivas and produced by Ali and Karim Morani. The film stars Vivek Oberoi, Diya Mirza, Govind Namdeo and Atul Kulkarni in lead roles.  Sushant Singh, Mukesh Rishi and Sheeba have important supporting roles. The films music was penned by Sandeep Chowta, whichs banner is Sony Music Studios. It is a remake of Tamil hit Dhill (2001).

==Summary==

Uday and his friend Mohan aspire to become cops and serve the country, despite their unfavourable backgrounds. Raj Dutt Sharma, an honest training officer, sees potential in them and makes their dream come true. However, Uday crosses wrong kind of person in form of Inspector Shankar. Shankars ego is badly bruised and he leaves no stone unturned to make Udays life a living hell.

==Plot==

Uday and Mohan come from poor middle-class families. Against their families wishes, the boys are hell bent on joining the police force. The duo aim to make it big solely on the basis of their abilities. Despite lack of any recommendations or leverages to make it to the police academy, lady luck smiles on them in the form of Raj Dutt Sharma, their training officer.

The duo too return his favour by realizing their dreams and making him proud. They soon become popular as no nonsense upright cops. One day, however, Kaveri, who happens to be Udays girlfriend, nearly gets molested by Inspector Shankar aka Encounter Shankar. Uday thwarts Shankars attempts and beats him badly. Shankar swears vengeance on him and leaves. On learning this, Sharma tells that Shankar is an egotic, corrupt cop who uses his powers for all the wrong kinds of motives.

He reveals that on orders of Minister Deshmukh, a goon named Babu Kasai killed his rival. Sharmas wife Lakshmi was one of the many witnesses who saw the murder, but only she came forward to testify. Shankar, who was also on Deshmukhs payroll, barged in Sharmas household and killed Sharmas daughter in front of Lakshmi. After sending Lakshmi into shock, Shankar was promoted over Sharma, the latter was demoted and given the job of selecting officers for training.

This was one of the reasons why Sharma selected the duo. Uday now decides that he will not stop until Shankars menace ends once and for all. Here, Shankar already goes into offensive by killing Mohan. After cornering Uday, Shankar thinks that he is safe. But Uday retaliates by attacking Shankar. He kills Babu with Shankars gun stolen by him, making everybody believe that Shankar gunned the criminal. In retaliation, Shankar kills Deshmukh and frames Uday for it. Now, it is revealed that Babus death was faked and that he is actually in captivity of Uday.

Unaware of this, Shankar manages to launch a massive manhunt against Uday. On learning that Babu is still alive, Shankar springs into action to track down Babu, Uday and Lakshmi. He traces the trio and a shootout occurs. Babu gets fatally injured during the fracas. Taking advantage of the situation, Shankar tries to corner and kill Uday. Meanwhile, Babu, who is on his deathbed, confesses all his crimes to Kaveri, who videotapes it.

The Commissioner himself turns up at the crime scene, where Kaveri shows the dying confession to him. With Shankars real face exposed, the Commissioner orders both Uday and Shankar to surrender. Shankar tries to run away, but Uday tracks him down and kills him, thus avenging all the wrongs Shankar caused. Uday surrenders, after which he is duly tried at the court. Based on the evidence, he is exonerated, after which he is cheerfully greeted by the people in the end.

==Cast==

*Vivek Oberoi as UDAY SHINDE
*Diya Mirza as Kaveri
*Atul Kulkarni as Inspector "Encounter" Shankar
*Govind Namdeo as Deshmukh
*Sheeba as Lakshmi Sharma
*Sushant Singh as Mohan
*Mukesh Rishi as Raj Dutt Sharma
*Yana Gupta as an item girl in the song "Babuji Zara Dheere Chalo" Yashpal Sharma as Babu Kasai

==Direction team==
* Associate Director - Rafiq Lasne
* Assistant Directors - Ramesh , Rajsekhar, Amar Mukherjee, Parag Kalita

==Critical reception==

=== Box office ===

Dum had an excellent first week start, but a weak second. Although the film had several clapworthy sequences, it fell prey to mediocrity in the second week. Relying too heavily on the tried and tested stuff, Dum faced rough weather at the box-office, and was declared a flop.
It earned Rs.245&nbsp;million in India. It grossed about till $246.000, in USA.
It was rated 5.21/10 by the audience of a website.   

==Soundtrack==
The official track listing.
{{tracklist
| extra_column    = Artist(s)
| title1          = Dum
| extra1          = Sandeep Chowta
| length1         = 4:58
| title2          = Jeena
| extra2          = Sonu Nigam, Sowmya Raoh
| length2         = 4:48
| title3          = Babuji Zara Dheere Chalo
| extra3          = Sukhwinder Singh, Sonu Kakkar
| length3         = 4:59
| title4          = Someday
| extra4          = Leslie Lewis, Anuradha Sriram
| length4         = 4:54
| title5          = Dil Hi Dil Mein
| extra5          = Sonu Nigam, Sowmya Raoh
| length5         = 4:33
| title6          = Suntaja
| extra6          = Sukhwinder Singh, Jolly Mukherjee, Parthiv, Sowmya Raoh
| length6         = 5:03
| title7          = Dum
| extra7          = Sonu Nigam
| length7         = 4:57
| title8          = Babuji Zara - Bijli Mix
| extra8          = Sukhwinder Singh, Sonu Kakkar
| length8         = 5:49
}}

== References ==
 

== External links ==
* 

 
 
 
 
 