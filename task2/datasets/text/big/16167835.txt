Kurumbu
{{Infobox film
| name = Kurumbu
| image =
| caption = Vishnuvardhan
| writer = Vishnuvardhan Nivas Diya Nikita Thukral
| producer = Akkineni Indira Anand
| music = Yuvan Shankar Raja
| cinematography = Ra. Krishnaa
| editing = A. Sreekar Prasad
| studio = Indira Innovations
| released = 21 November 2003
| runtime =
| country = India
| language = Tamil
| budget =
}}
 Indian Tamil Tamil film Telugu film Allari (film)|Allari, stars Allari Naresh, who also starred in the original version, which marked his acting debut, along with the female artists Diya and Nikita Thukral in lead roles. The film released on 21 November 2003 to poor reviews, failing to repeat the success of the original version and hence, ending up as a box-office bomb, being considered as the only commercially unsuccessful film of director Vishnuvardhan.

It is a triangular love story between three young teenagers. The story portrays how the hero finds his true love.

==Cast==
* Allari Naresh as Ravi
* Nikita Thukral as Aparna Diya as Ruchi
* Nassar as Ruchis father
* Pyramid Natarajan
* Meera Krishnan Kuyili
* Nirosha
* R. S. Shivaji
* Ramya Krishnan in a cameo role

==Soundtrack==
{{Infobox album|  
| Name = Kurumbu
| Type = soundtrack
| Artist = Yuvan Shankar Raja
| Cover = Kurumbu cd cover.jpg
| Released =  7 August 2003 (India)
| Recorded = 2003 Feature film soundtrack
| Length =
| Label = Sa Re Ga Ma
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album  = Thennavan (2003)
| This album  = Kurumbu (2003)
| Next album  = Pudhukottayilirundhu Saravanan (2003)
}}
 remixed song, which is the first ever remix of a Tamil film song to feature in another Tamil film, starting off a new trend and the era of remixes in the Tamil film industry.   The lyrics were penned by Pa. Vijay. Interestingly, another soundtrack of Yuvan Shankar Raja, Thennavan was released on the same day.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration !! Notes
|- 1 || "Kingini Mingini" || Yuvan Shankar Raja, Shalini Singh, Prashanthi || 3:57 ||
|- 2 || "Oviya" || P. S. Balram || 4:11 ||
|- 3 || "Adichchi Pudichchi" || Devan, Sunitha Sarathy || 4:07 ||
|- 4 || "Kurumbu" || Instrumental || ||
|- 5 || "Theme Music" || Instrumental || ||
|- 6 || "Aasai Nooru Vagai" ||  , composed by Ilaiyaraaja, featuring the original voice of singer Malyasia Vasudevan with lyrics by Vaali (poet)|Kavignar Vaali
|- 7 || "Adichchi Pudichchi - II" || Afroze, Gopika Poornima || ||
|- 8 || "Vaa Masakaatre" || Srinivas (singer)|Srinivas, Harish Raghavendra, Srilekha Parthasarathy, Subiksha || 4:49 ||
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 