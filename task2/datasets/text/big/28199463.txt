Screaming Eagles (film)
{{Infobox film
| name           = Screaming Eagles
| image          = Screagle.jpg
| caption        = Original film poster
| director       = Charles F. Haas David Diamond David Lang Robert Presnell Jr.
| starring       = Tom Tryon Jan Merlin Jacqueline Beer
| music          = Harry Sukman
| cinematography = Harry Neumann
| editing        = Robert S. Eisen Allied Artists
| released       = May 27, 1956 (U.S. release)
| runtime        = 79 min
| language       = English
| budget         =
}}
 1956 black-and-white Allied Artists. 101st Airborne Division jumps into France. The title of the film refers to the nickname of the Division, based on its shoulder sleeve insignia. The film is notable for its large cast of up-and-coming actors.

==Plot== 502nd Parachute Infantry Regiment.  Amongst them is Private Mason (Tom Tryon) who doesnt get along with his fellow paratroopers but is kept in the unit by his platoon commander Lt. Pauling (Jan Merlin).

Soon after the drop, Lt. Pauling is blinded and Mason learns how to be a paratrooper when he takes care of his Lieutenant.

==Cast==
* Tom Tryon - Pvt. Mason
* Jan Merlin - Lt. Pauling
* Alvy Moore - Grimes
* Martin Milner - Corliss
* Pat Conway - Sgt. Forrest
* Jacqueline Beer - Marianne
* Joe di Reda - Dubrowski
* Mark Damon - Lambert Paul Burke - Dreef
* Edward G. Robinson, Jr. - Smith Richard Dix) - Peterson
* Robert Roark - Torren Robert Blake - Hernandez
* Keith Larsen

==Production==
Parts of the film were filmed at Fort Benning, Georgia.  The technical advisers were Richard Haynes Case a D-Day veteran of the 101st   and Werner Klingler, a German film director who also had a role in the film. Case had also acted as an adviser to The Man in the Grey Flannel Suit in the same year. 

Jan Merlin recalled that originally he was supposed to play Private Mason due to his reputation for playing villains. As his character was to continually carry the blinded Lieutenant who was to have been played by the much taller Tom Tryon, the two agreed to switch their roles to make things easier.  As Tryon usually played heroes he welcomed the change in roles.

==References==
 

==External links==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 