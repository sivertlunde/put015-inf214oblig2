Lady in Cement
{{Infobox film
| name           = Lady in Cement
| image          = Ladyincementposter.jpg
| image_size     =
| caption        = Promotional film poster Gordon Douglas
| producer       = Aaron Rosenberg
| based on       =  
| screenplay     = Jack Guss
| narrator       =
| starring       = Frank Sinatra Raquel Welch Richard Conte
| music          = Hugo Montenegro
| cinematography = Joseph F. Biroc
| editing        = Robert L. Simpson
| studio         = Arcola Pictures
| distributor    = 20th Century Fox
| released       =  
| runtime        = 93 minutes
| country        = United States English
| budget         = $3,585,000 
| gross          =
}}
 1968 detective Gordon Douglas and starring Frank Sinatra, Raquel Welch, Dan Blocker, Martin Gabel and Richard Conte.

A sequel to the 1967 film Tony Rome, and based on the novel by Marvin H. Albert, Lady In Cement was released on November 20, 1968.

==Plot==
While diving off the Miami coast seeking one of the eleven fabled Spanish Galleons sunk in 1591, private investigator Tony Rome (Frank Sinatra) discovers a dead blonde, her feet encased in cement, at the bottom of the ocean.

Rome reports this to Lieutenant Dave Santini (Richard Conte) and thinks little more of the incident, until Waldo Gronski (Dan Blocker) hires him to find a missing woman, Sandra Lomax. 

Gronski has little in the way of affluence, so he allows Rome to pawn his watch to retain his services.

After investigating the local hot-spots and picking up on a few names, Rome soon comes across Kit Forrester, whose party Sandra Lomax was supposed to have attended. 

Rome’s talking to Forrester raises the ire of racketeer Al Mungar, a supposedly reformed gangster who looks after Kit’s interests.

Thinking there may be a connection between Lomax, Forrester and Mungar, Rome starts probing into their backgrounds and begins a romantic relationship with Kit. 

With both cops and crooks chasing him and the omnipresent Gronski breathing down his neck, Rome finds himself deep in a case which provides few answers.

==Cast==
* Frank Sinatra as Tony Rome
* Raquel Welch as Kit Forrester
* Dan Blocker as Waldo Gronski
* Richard Conte as Lt. Dave Santini
* Martin Gabel as Al Mungar Richard Deacon as Arnie Sherwin
* Lainie Kazan as Maria Baretto Pat Henry as Rubin
* Steve Peck as Paul Mungar (as Steven Peck)
* Virginia Wood as Audrey
* Frank Raiter as Danny Yale
* Peter Hock as Frenchy
* Alex Stevens as Shev
* Christine Todd as Sandra Lomax
* Mac Robbins as Sidney the organizer
* Tommy Uhlar as The Kid, Tighe Santini

==Critical reception==
Opening to mixed reviews, Lady in Cement is generally considered to be a middling sequel to Tony Rome. Critic   noted that “Dan Blocker is excellent as a sympathetic heavy,” whilst John Maloney liked the “fresher script” and “sharp direction.”

==Trivia==
In Lady in Cement, director Gordon Douglas, and star Frank Sinatra dropped a few inside references, including an instrumental of the Sinatra song "You Make Me Feel So Young" during one scene. A clip of the TV series Bonanza was used in one scene, on which co-star Dan Blocker played Hoss Cartwright. There is also a reference to Sinatras ex-wife Ava Gardner during a scene in which Rome comments on knowing a girl who used to date bullfighters. .   Rome rides in a taxi which bears an advertisement for Dean Martins Restaurant & Lounge on its rear fender.

==DVD release== The Detective, both also directed by Douglas. No bonus features were included.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 