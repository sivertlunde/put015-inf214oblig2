The Last Hurrah (2009 film)
{{Infobox film
| name           = The Last Hurrah 
| image          = The Last Hurrah Poster.jpg
| caption        = Theatrical release poster
| director       = Jonathan W. Stokes
| producer       =  
| writer         = Jonathan W. Stokes
| starring       =  
| music          =  
| cinematography = Charles DeRosa
| editing        = Jay Trautman
| studio         = New Epic
| distributor    = Cinema Libre Studio
| released       =  
| runtime        = 88 minutes United States
| language       = English
| budget         =   120,000 (est)
| gross          = 
}}
 American comedy comedy drama Jonathan Stokes. After receiving recognition at film festivals in 2009, the film was released on DVD in February 2010, to mildly warm reviews.      

==Plot== hipsters come together for one last wild night.  Over the course of the graduation party, characters fall in and out of love and struggle to figure out what to do with the rest of their lives.

==Partial cast== Zack Bennett as Will 
* David Wachs as Jason 
* Randy Wayne as Dogbowl  Ravi Patel as Ara 
* Kate Micucci as Susan
* Valerie Azlynn as Nicole 
* Jon Weinberg as Steve 
* Alicia Ziegler as Melissa 
* Heidi Johanningmeier as Tara

==Production==
The movie is filmed in one single, continuous 88 minute shot. The director states that the film was inspired by Richard Linklater and Woody Allen, with dialogue intended to be "a lot of fast-paced one-liners. A lot of philosophy."     Obstacles encountered while shooting the film as one take included coordinating a film crew in a party scene filled with extras, and equipment limitations such as cameras with 30 minutes shot length. Filming took five nights of shooting attempts - on the fifth night, "the film came together." 

==Recognition==

===Critical response===
DVD Verdict offered "while it isnt a disaster, it lacks the punch to keep viewers fully engaged",  finding the plot intriguing, but that there seemed to be more attention paid to the continuous shot than to the script and story. The review continued that the party concept had potential, but became "monotonous", and was not deeply engrossing.   The film was deemed "worth watching", with sound acting; "technically, its a solid indie film, but in terms of story, it lacks energy and variety." 
 WeHo News American Pie, the film is described as "transcending the genre."   While criticizing the dialogues overuse of homophobic epithets and lack of any LGBT characters, the review concluded that the project as a whole was "a clever, audacious film well worth a viewing." 
 Campus Circle teen comedies have become reviled due to their becoming "synonymous with cheap laughs and awkward, over sexual punch lines,"   but that Last Hurrah "reminds audiences of what has since been lost from the high point of ’80s filmmaking." 

===Awards and nominations===
*2009, won Audience Award honorable mention at Dances With Films.     SoCal Film Festival     Maverick Movie Awards nominations:
*: Best Director for Jonathan W. Stokes 
*: Best Screenplay for Jonathan W. Stokes 
*: Best Supporting Actor for David Wachs 
*: Best Supporting Actress for Sarah Scott 
*: Best Supporting Actress for Kate Micucci 
*: Best Soundtrack for The Mae Shi, Division Day, and Meiko.   

==Release==
The DVD was released on February 23, 2010 by Cinema Libre  and is available for streaming in the U.S.    According to DVD Verdict, video transfer is good with decent picture quality, color is "nice" and both picture and audio quality are acceptable. Extras include a blooper reel and informal but informative actor interviews. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 