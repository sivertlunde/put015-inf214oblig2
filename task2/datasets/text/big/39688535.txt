My Lucky Star (2013 film)
{{Infobox film
| name           = My Lucky Star
| image          = My Lucky Star poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Dennie Gordon
| producer       = Zhang Ziyi Ming Beaver Kwei   Ling Lucas   William Cheng   Ryan Wong   Jonathan Hua Lang Lim
| writer         = 
| screenplay     = Amy Snow   Chris Chow   Hai Huang   Yao Meng
| story          = Dennie Gordon   Ming Beaver Kwei  Amy Snow
| based on       = 
| narrator       = 
| starring       = Zhang Ziyi   Leehom Wang  Ruby Lin   Yao Chen   Ryan Zheng   Terry Kwan  Ada Choi  Jack Kao   Morris Rong   Alfred Hsing  Emilia Norin
| music          = Nathan Wang
| cinematography = Armando Salas
| editing        = Zack Arnold
| studio         = Bona International Film Group
| distributor    = Polybona Films
| released       =     
| runtime        = 114 mins
| country        = China
| language       = Mandarin   Cantonese   English
| budget         = $8 million
| gross          = $22,786,000   
}}
My Lucky Star ( ) is a 2013 Chinese romantic adventure film directed by   and Yao Chen reprising their roles.
 White Collar, 30 Rock and The Office. My Lucky Stars screenplay is a similarly collaborative effort, co-written by Amy Snow, Chris Chow, Hai Huang and Yao Meng (based on a story by Snow, Gordon and producer Ming Beaver Kwei). Zhang co-produced, reuniting with her Sophies Revenge producers Kwei and Ling Lucas.

==Plot== Venetian Hotel & Casino in Macau, where Sophie goes undercover as a gondolier on the resorts famous canal. In addition to the fast-paced, comedic action, which includes several fight scenes and a daring parachute jump off the hotel skyscraper, romantic sparks fly between Sophie and David. The film is also interspersed with dynamic animated sequences (straight from Sophies doodles) and concludes with the ballad, "Love a Little (Ai Yidian)"  sung by Wang and Zhang.

==Cast==
* Zhang Ziyi as Sophie
* Leehom Wang as David 
* Yao Chen as Lily
* Ruby Lin as Lucy
* Ada Choi as Xixi
* Ryan Zheng as Bo
* Terri Kwan as Charlize
* Jack Kao as Mr. Gao
* Morris Rong as Li Wan
* Singh Hartihan Bitto as Arms Dealer
* Alfred Hsing as Li Wans Henchman
* Zheng Kai as Po
* Zhang Jin
* Liu Hua

==Production==
The 2009 comedy Sophies Revenge was a success in China, earning RMB97 million. It marked a change of pace for Zhang, who gained fame for her work in serious dramas and action movies like her award-winning performance in Wong Kar-wais The Grandmaster (film)|The Grandmaster and Ang Lees Oscar-winning Crouching Tiger, Hidden Dragon. Initially producer Ming Beaver Kwei brought the project to Village Roadshow Pictures as a direct follow-up to Sophies Revenge. My Lucky Star eventually evolved into a standalone story, now under Bona Film International Film Group. My Lucky Star retains certain elements from its predecessor. Zhang reprises her role as Sophie, with Chen and Lin returning as her best friends, Lucy and Lily. Also returning are cinematographer Armando Salas, and two of the producers of Sophies Revenge—Kwei and Ling Lucas. While a big part of the success of the first movie was the presence of pop heart-throbs Peter Ho and So Ji-sub, My Lucky Star casts Mandopop superstar Wang as Zhangs romantic leading man.   

The Chinese trailers for My Lucky Star also tout the connection with the "Sophie Is Back" tagline and even the Mandarin titles are similar—Fei chang wan mei or "Extraordinary Perfection" and "Fei chang xinyun" ("Extraordinary Luck"). My Lucky Star is also notable for being the first Chinese-language movie to be directed by an American woman, Dennie Gordon.   
 The Los Angeles Times how she initially got involved with the production:
 "I was trying to do a remake of What a Girl Wants and there was a company in China that was interested. There was a guy there who was very keen on doing it, Song Ge, and he would just show up wherever I was shooting something. He was sort of tracking me and we just built this relationship. He had produced Sophie’s Revenge with Ziyi -- we call her Z -- and he wanted to do another project with her. And so he put us together. She said, I really want you to develop a movie for me like Anne Hathaway would star in, or like 10 years ago Kate Hudson would star in. ... I want to do a romantic comedy with some adventure. I want to do something different. I want to do a Sophie movie because I love that character but I don’t want to do the old Sophie, I want to take her to a new place. So I pitched her -- what if we took you to the land of James Bond or Romancing the Stone, a spy-tastic adventure where Sophie has to step up to the plate? And she loved that."    

Chow wrote the first draft of the script in China, while stateside, the story was developed by Gordon, with American screenwriter Snow and producer Kwei. Chinese writers Huang and Meng also contributed to the script. After several translated drafts and lots of cross-cultural collaboration, the team was able to strike the right balance of humor and cultural relevance for Chinese audiences. In an interview with  , Snow explained how much she enjoyed the process:
 "It was so fun when I got to work with Sean  , who working on set as the writer," says Snow. "We had both been working on the same script but had never met, and it was so great to be able to talk through these scenes -- to go Oh, I love what you did with that! or to say That moment doesnt work. I cant figure it out!"    

The result is a movie that pays homage to American action films and classic romantic comedies, as well as the Pink Panther, James Bond and Austin Powers (film series)|Austin Powers series.

"It feels like a throwback here but it’s revolutionary in China," Gordon told The Los Angeles Times. " This mixing romance, comedy, action and travel-adventure, it’s never been done. We are really hoping they dig it. There’s really no Chinese film we could point to and say, oh, it’s kind of like this. So it feels a little retro, a little vintage here, but there its not." 

With an $8 million budget, My Lucky Star began shooting in July 2012 on a 56-day schedule with locations in China, Singapore, Hong Kong and Macau. Like many productions, it experienced its share of challenges. After only one day of shooting, the films entire Hong Kong set was destroyed by a major typhoon. 

"I thought we’d just get the insurance like we do in the West," Gordon recalled to The Los Angeles Times. "And they said oh no, we don’t have insurance. So we had to figure out how to make up the difference -- we were down for 10 days and had to rebuild the set. I just started pulling pages out of the script. We shot a very tight little movie." 

Due to the movies limited budget, the team also struggled with the stunt equipment and action choreography, until a very famous friend came to their rescue: Jackie Chan. While visiting the set to see his friends and former co-stars Zhang and Wang, Chan offered his action team, including the legendary stunt master Wu Gang.

"The next day,   team and all of their equipment were at my disposal. It was amazing; we could have never afforded that team. We have about six major stunt set pieces in the film that are all crazy-great and that team was with me every step of the way. We couldnt afford his guys; theyre the best guys on the planet. We had his entire team and all their harnesses, so when you see the movie, you see Wu Gang, action stunts -- thats Jackies No. 1 guy. He just really stepped up to the plate. Hes just that extraordinary, that generous."  Chans contributions to the production earned him a "Special Thanks" in the end credits of My Lucky Star.

Another challenge for Gordon was dealing with the language barrier and cultural differences among her mostly Chinese crew. Gordon relied on her Cuban-American cinematographer Armando Salas and her English-speaking stars for guidance.
 DGA Guild interview. “They’d come to me and speak back and forth in Mandarin, then they’d say to me in English, ‘It’s funnier this way because what it means is...’ She also worked hard to create the kind of on-set atmosphere she had on her previous films where cast members know that any and all dialogue ideas are welcome. “I like   to improvise a lot,” something uncommon in the world of Chinese filmmaking. “I don’t want to be stuck not being able to have off-the-cuff dialogue.” 

It took longer for Gordon to gain the trust of the technical crew, with whom she was unable to communicate directly in Chinese. Ultimately, the universal language of cinema solved the dilemma.
 Wachowskis did this on the first "The Matrix|Matrix"—and so I cut together a trailer about three weeks in and then took that trailer, like three minutes, took it around on my iPad to every single person on the crew. And I said, I just want you to see the movie we are making together. And it was like, Boing! They got it." 

The film benefits from on-location shooting at two world-class resorts. At the Marina Bay Sands Hotel in Singapore, key scenes were shot on the 57-story high rooftop Ku De Ta nightclub and at the stunning infinity pool overlooking the cityscape. My Lucky Stars finale unfolds with a gondola ride on the famous canal of the Venetian Resort in Macau.   

Speaking on a panel at the 54th annual Asia-Pacific Film Festival, Kwei explained Macau, in particular is likely to be a popular choice for future film productions. "Macau already has a great infrastructure--airport, proximity to Hong Kong, the people, hotels. If you tell people about it, they will want to come and take a look."   

My Lucky Star is dedicated to the memory of Taiwanese actor Morris Rong. Rong, who plays Li Wan, died of a heart attack at the age of 43 in January 2013, months before the movie was released.   

==Box Office==
My Lucky Star was released in China by the Bona Film Group on September 17, 2013. Coinciding with the busy Mid-Autumn Festival|Mid-Autumn Moon Festival, a time generally known for low cinema attendance, My Lucky Star beat expectations with a blockbuster weekend. It opening in first place at the Chinese box office, the movie took in RMB10.5 million ($1.71 million) the first day of its release and grossed RMB83.6 million ($13.7 million) for the week, accounting for approximately 23% of nationwide screenings. At RMB130 million, it has already outpaced Sophies Revenges RMB97 million gross.   

Just days later, My Lucky Star was released internationally by China Lion Film, the distributors widest 2013 release, in Australia, New Zealand, and Singapore and received a limited 23-theater North American release on September 20, 2013 (including at cinemas in New York, Los Angeles, Chicago, San Francisco and Vancouver, B.C.). The film also expanded to Hong Kong, Taiwan and Malaysia in October 2013. 

According to site Box Office Mojo, My Lucky Star has earned $22.8 million in China and $224,747 in other Asian markets. As of early October 2013, My Lucky Star had grossed $64,232 in its North American release.   The movies domestic success helped contribute to a strong third-quarter for Bona Film Group, which reported a 42% growth for the quarter.   

==Critical reception==
My Lucky Star has received mostly mixed reviews, with an aggregate score of 56% on Rotten Tomatoes (9 reviewers praised the movie as "Fresh", 7 critics panned it as "Rotten").   

The Metacritic site tallied a combined score of 33, with the movie receiving mostly negative reviews.   
 The New York Times called it "sweet and harmless, but it’s also a little disorienting. After watching it, you may need a few moments to remember which decade this is, because the film has the tone and silliness of a Pink Panther movie or an episode of the 1960s sitcom Get Smart   
 The Seattle Times praises the films star and its production values:
 "My Lucky Star plays out like a sparkling, brightly lit travelogue with Zhang as a bubbly tour guide. Whether she’s posing as a nightclub sex kitten, singing gondolier or leather-clad badass, Zhang’s every bit as appealing here as she was in her martial-arts classics."    

Variety (magazine)|Variety found the film to be "wholesome, effortless entertainment that runs smoothly enough but seldom takes one’s breath away in the romance department." Zhang displays sufficient comic verve to make the silly gags hit the sweet spot while Gordon "skillfully sets a snappy yet even pace, effectively balancing verbal jokes and campy drama with purely physical stunts as the protags gallivant from one metropolis to another." 
 The Washington Post deeming it "utterly provincial."   

The Los Angeles Times found Zhang to be "at her spunky best" but considered Gordons direction "thoughtless" and Wangs performance "uncharismatic."   

==Soundtrack==
* Theme song: “Love a Little (Ai Yidian)”  
**Performer: Leehom Wang & Zhang Ziyi

* Other Songs 
** “Kan De Zui Yuan De Di Fang”  by Angela Chang
** “Every Single Day”  by M.KING

==References==
 

==External links==
* 
* 

 

 
 
 