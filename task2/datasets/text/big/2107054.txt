The Sheik (film)
{{Infobox film
| name           = The Sheik
| image          = The Sheik with Agnes Ayres and Rudolph Valentino, movie poster, 1921.jpg
| director       = George Melford 
| writer         = Monte M. Katterjohn (adaptation)
| based on       =  
| starring       = Rudolph Valentino Agnes Ayres Adolphe Menjou
| music          = Irving Berlin (1970s reissue) William Marshall
| studio         = Famous Players-Lasky
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 mins.
| country        = United States
| language       = Silent English intertitles
| budget = under $200,000
| gross = $1.5 million (US/Canada)    accessed 19 April 2014 
}}
  silent romantic of the same name by Edith Maude Hull and was adapted for the screen by Monte M. Katterjohn. The film was box office hit and helped propel Valentino to stardom.   

In the sequel, Son of the Sheik, Valentino played both the Sheik and his son, while Ayres reprised her role.

==Plot==
 

In the North African town of Biskra, headstrong Lady Diana Mayo (Ayres) refuses a marriage proposal because she believes it would be the end of her independence. Against her brothers wishes, she is planning a month-long trip into the desert escorted only by natives.
 Arabs may enter. Annoyed at being told what she cannot do, and her curiosity piqued, Diana borrows an Arab dancers costume and sneaks in. Inside, she finds men gambling for new wives. When she is selected to be the next prize, she resists. Sheik Ahmed Ben Hassan (Valentino) intervenes, then realizes she is white. Amused, he sends her away. Afterward, Mustapha Ali (Charles Brinley) informs the Sheik she is the woman he has been hired to guide tomorrow. The Sheik hatches a plan. Early the next morning, he sneaks into her room and tampers with the bullets in her revolver as she is sleeping.

 
As her brother leaves her to her desert excursion, she assures him he will see her in London next month. The Sheik and his men come upon Diana riding alone. She tries to flee while shooting at the Sheik, but he easily captures her. Back at his encampment, he orders her about. She is unused to such treatment, but the Sheik tells her she will learn and demands she dress like a woman (she is wearing pants) for dinner.

Diana tries again to escape, this time into a raging sand storm. The Sheik saves her from certain death, and tells her she will learn to love him. Later, he finds Diana alone in her quarters weeping. The Sheik considers forcing himself upon her, but decides against it and calls for a serving girl. The serving girl (called Zilah in the book and film) heads over to where Diana is, and offers her a hug. Diana accepts, and pours out her tears in Zilahs arms.

After a week, the Sheik is delighted by the news that his close friend from his days in Paris (where he was educated) is coming for a visit. Diana is dismayed at the thought of being seen in Arab dress by a Westerner, but the Sheik does not understand her shame. When she is introduced to writer and doctor Raoul St. Hubert (Menjou), Dianas spirit is nearly broken. He befriends her and reprimands the Sheik for his callous treatment of her. The Sheik returns her Western clothing, though he refuses to release her.

When Raoul is called away to tend to an injured man, Diana shows concern that it might be the Sheik. Seeing this from hiding, the Sheik is elated that she may be warming up to him at last. He gives Diana her gun back, telling her he trusts her.
 Walter Long). Fortunately, the Sheik and his men reach her first.

The Sheik reveals to Raoul he is in love with Diana; his friend convinces him to let her go. Meanwhile, Diana is allowed out once more. She playfully writes "I love you Ahmed" in the sand. Then Omairs band captures her, killing her guards and leaving the wounded Gaston for dead.

When the Sheik goes looking for Diana, he sees her message, then learns from Gaston who has abducted her. He gathers his men to attack Omairs stronghold. Omair tries to force himself on Diana, but is almost stabbed by one of his women. Then the Sheik and his men sweep in. After a long fight, the Sheik kills Omair, but is himself gravely injured.

Raoul tends to him and tells Diana he has a chance. She sits and holds the Sheiks hand. When she remarks that his hand is big for an Arab, Raoul reveals that the Sheik is not one. His father was British and his mother Spanish.  They died in the desert, and their child was rescued and raised by the old Sheik; when the old man died, Ahmed returned to rule the tribe. When Ahmed wakes up, Diana confesses her love.

==Cast==
*Rudolph Valentino as Sheik Ahmed Ben Hassan
*Agnes Ayres as Lady Diana Mayo Ruth Miller as Zilah, the serving girl
*George Waggner as Yousaef, a tribal chieftain Frank Butler as Sir Aubrey Mayo, Dianas brother
*Charles Brinley as Mustapha Ali
*Lucien Littlefield as Gaston
*Adolphe Menjou as Raoul de Saint Hubert Walter Long as Omair
*Loretta Young as Arab child (uncredited)
*Polly Ann Young (uncredited)

== Production ==
The film was based on Edith Maude Hulls best selling novel The Sheik. The novel became a best seller in part because of its controversial dealings with racial miscegenation and rape. Due to the controversial subject matter, certain aspects of the novel were left out of the film. In the novel, Sheik Ahmed rapes Lady Diana but in the film, rape is only suggested as a curtain closes on Sheik Ahmed and Lady Diana before there is any physical contact between the two.    

There appears to be some dispute as to where the film was shot. Emily W. Leider, author of Dark Lover: The Life and Death of Rudolph Valentino, argues that the desert exterior scenes were filmed in Oxnard, California, and the Guadalupe Dunes of Santa Barbara County.   However, one 1983 newspaper article featured in the Suffolk County News contends that exteriors scenes were shot at the "Walking Dunes" in Montauk, New York and at the Kaufman Astoria Studios. Author Richard Koszarski claims that this is incorrect and cites it as an "urban legend".   Another source says filming was done in the desert near Palm Springs, California. {{Cite book|last=Niemann|first=Greg title = Palm Springs Legends: Creation Of A Desert Oasis| publisher=Sunbelt Publications
|year=2006|pages=286|url=http://books.google.com/books?id=RwXQGTuL1M0C&pg=PA169&lpg=PA169&dq=palm+springs+film+locations&source=bl&ots=N8AgHRYZbg&sig=RMIwy_g3fEV3KoK1aa7P4VxuJWM&hl=en#v=onepage&q=palm%20springs%20film%20locations&f=false|isbn=978-0-932653-74-1|oclc=61211290}}  ( )  

==Reception==
The Sheik premiered in Los Angeles on October 21, 1921.  Critical reception was mixed as some critics felt that it was mistake to leave out the rape of Lady Diana by Sheik Ahmed as it altered the original message of the novel. Leider 2004 p. 167  Some critics felt that the "toned-down" film version would not be well received;   however, the film was a major success with audiences, and set new attendance records where it premiered.  In its first week of release, it set attendance records at two of New Yorks major theatres, the Rialto and the Rivoli Theatre (South Fallsburg, New York)|Rivoli.  The New York Telegraph estimated that in the first few weeks 125,000 people had seen the film. 

Due to the films success, Jesse Lasky declared the last week of November 1921 as "The Sheik Week", and screened the film at 250 theatres in the United States on November 20, 1921.  The film ran for six months in Sydney, Australia, as well as 42 weeks in one theatre in France. It was the first Valentino film to show in his native Italy. 

Within the first year of its release, The Sheik exceeded $1 million in ticket sales (the film was made for under $200,000).  The Sheik helped to solidify Valentinos image as one of the first male sex symbols of the screen and made him an international star. While he was a popular draw with female audiences, some male audiences mocked his onscreen persona and questioned his masculinity.    Valentino would later attempt to portray roles that went against his "Sheik" image with limited success.  His final film was the sequel The Son of the Sheik, which premiered nationwide after his death in August 1926.  

===Proposed Remake===
Film rights were bought by Edward Small who recreated segments of The Sheik in the biopic Valentino (1951 film)|Valentino (1951). That film starred Anthony Dexter, who Small announced would star in a remake of The Sheik. However this did not eventuate. Mitchell Likely Capn Andy; Preston to Star as Heavy With Rooney
Schallert, Edwin. Los Angeles Times (1923-Current File)   22 Aug 1950: A11. 

==Home media==
The Sheik has been released on Region 1 DVD by several companies over the years including Image Entertainment in 2002. In 2004, Instant Vision released the film on Region 2 DVD. 

==Cultural influence==
The Sheik became so popular that the word came to be used to mean a young man on the prowl. The object of a Sheiks desire was dubbed "a Sheba." 
 Rex Ingrams The Arab, Baby Peggy short Peg o Movies spoofed the film. The title of the Oswald the Lucky Rabbit cartoon The Shriek (1933) is a parody of The Sheik. {{cite web
|url=http://lantz.goldenagecartoons.com/1933.html
|title=The Walter Lantz Cartune Encyclopedia: 1933
|accessdate=2011-06-03
|publisher=The Walter Lantz Cartune Encyclopedia
}}  Much later, Elvis Presleys film Harum Scarum drew from The Sheik as well. 

The popular song "The Sheik of Araby" was written for the novel, but popularized by the film. "Kashmiri Song", originally released in 1902, was popularized once again by the film (Valentino sings parts of it twice). Jimmy Buffett mentions the Sheik in "Pencil Thin Mustache".

Chapter one of Art Spiegelmans graphic novel Maus is called "The Sheik", and contains a number of references to Valentino and his most famous film.

The Sheik is the official mascot of Hollywood High School, and the athletic teams and student body are known as the Sheiks, the name drawn from this movie.

Ray Stevens million selling 1962 pop hit "Ahab the Arab" was heavily influenced by both this film and The Son of the Sheik in the songs use of Arabian atmosphere and music.

Serbian popular singer Dragana Šarić, also referred to as Bebi Dol, had a hit song called "Rudi" dedicated to Valentino in 1983. In the song, he is referred as "the Sheiks son."

The Egyptian Lover cites the film as the inspiration for his name. 

The life story of   by taking time away from her studies to protest against American involvement in Vietnam, taking a job as an adventure sports instructor in Colorado, becoming fascinated with the Middle East and marrying King Hussein of Jordan.

==See also==
 
*The House That Shadows Built (1931 promotional film by Paramount)

==References==
 

==External links==
 
*  
* 
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 