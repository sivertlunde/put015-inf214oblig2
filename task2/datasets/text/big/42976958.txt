Babysitting (film)
{{Infobox film
| name           = Babysitting
| image          = Babysitting poster.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Nicolas Benamou   Philippe Lacheau
| producer       = Christophe Cervoni    Marc Fiszman 
| writer         = Julien Arruti   Tarek Boudali   Philippe Lacheau   Pierre Lacheau   	
| starring       = Philippe Lacheau   Alice David   Vincent Desagnat 
| music          = Maxime Desprez   Michael Tordjman  
| cinematography = Antoine Marteau 
| editing        = Olivier Michaut-Alchourroun  
| studio         = Axel Films   Madame Films   Cinéfrance 1888   Good Lap Production  
| distributor    = Universal Pictures International
| released       =  
| runtime        = 85 minutes
| country        = France
| language       = French
| budget         = €3.4 million 
| gross          = $20.6 million   
}}
 found footage" style. It is directed by Nicolas Benamou and Philippe Lacheau. The film is also the directorial debut of Philippe Lacheau which he co-wrote and also starred in along with Alice David and Vincent Desagnat. 

== Cast ==
* Philippe Lacheau as Franck
* Alice David as Sonia
* Vincent Desagnat as Ernest
* Tarek Boudali as Sam
* Julien Arruti as Alex
* Grégoire Ludig as Paul
* David Marsais as Jean
* Gérard Jugnot as M. Schaudel
* Clotilde Courau as Mme Schaudel
* Philippe Duquesne as Agent Caillaud
* Charlotte Gabris as Estelle
* David Salles as Inspecteur Laville
* Philippe Brigaud as Monsieur Monet
* Enzo Tomasini as Rémi

== Sequel ==
Universal Pictures has confirmed that a sequel was in production as of January 2015. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 
 