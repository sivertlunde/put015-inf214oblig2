Nattupura Pattu
{{Infobox film
| name           = Nattupura Pattu
| image          = Nattupura Pattu DVD cover.jpg
| image_size     = 
| caption        = DVD cover
| director       = Kasthuri Raja
| producer       = Vijayalakshmi Kasthuri Raja
| writer         = Kasthuri Raja
| starring       =  
| music          = Ilaiyaraaja
| cinematography = Kichas
| editing        = L. Kesavan
| distributor    =
| studio         = Kasthoori Manga Creations
| released       =  
| runtime        = 135 minutes
| country        = India
| language       = Tamil
}}
 1996 Tamil Anusha and Abhirami playing supporting roles. The film, produced by Vijayalakshmi Kasthuri Raja, had musical score by Ilaiyaraaja and was released on 9 February 1996.   

==Plot==

Parijatham (Kushboo) was a famous folk dancer in her village and many rich landlords tried to woo her. Parijathams mother Pattamma wanted a son-in-law who will let Parijatham dance after the marriage. She finally got married with another folk dancer Palanisamy (Sivakumar). Palanisamys brother Kottaisamy (Selva (actor)|Selva) was a good for nothing young man who spent his time with Kattamuthu (Goundamani). Later, Parijatham changed Kottaisamy into a responsible person. He got married with the folk dancer Mala (Abhirami). Rumors around Parijatham and the rich landlord Naicker (Vinu Chakravarthy) became more intense. Palanisamy could not bear this rumor and split up with the pregnant Parijatham. One day, Kattamuthus sister was raped by a rich landlord, she then committed suicide and Kottaisamy ridiculed the rapist in public. After this incident, the rapist joined forces with the other landlords, they prevented Kottaisamy to dance for the village festival. So Kottaisamy and Mala struggled to survive, Mala eventually died during a dance show and Kottaisamy became a drunkard. In the meantime, Palanisamy brought up alone his baby son and Parijatham stopped to dance.

Many years later, Palanisamys son Velpandi (Prem (actor)|Prem) falls in love with Amaravathi (Anusha (actress)|Anusha). Palanisamy finally apologizes to his wife Parijatham while Kottaisamy decides to take revenge on his ennemies. What transpires next forms the rest of the story.

==Cast==

  Selva as Kottaisamy
*Sivakumar as Palanisamy
*Kushboo as Parijatham Manorama as Pattamma
*Goundamani as Kattamuthu Senthil as Chinna Karuppan
*Vinu Chakravarthy as Naicker
*Kumarimuthu as Pattammas father Prem as Velpandi Anusha as Amaravathi
*Abhirami as Mala
*Charle
*Jaiganesh
*Shanmugasundaram
*John Babu
*Anwar Ali Khan as Minor
*Taj Khan
*Venkat
*Idichapuli Selvaraj
*Karuppu Subbaiah
*Periya Karuppu Thevar
 

==Soundtrack==

{{Infobox album |  
| Name        = Nattupura Pattu
| Type        = soundtrack
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1996
| Recorded    = 1996 Feature film soundtrack
| Length      = 25:41
| Label       = 
| Producer    = Ilaiyaraaja
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1996, features 6 tracks with lyrics written by the director himself Kasthuri Raja.   The song "Otharoova" was well received and became a chartbuster.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration
|- 1 || Oththa Roova || Arun Mozhi, Devi || 1:57
|- 2 || Aatharikkum || K. S. Chithra || 4:31
|- 3 || Kezhukkaal || Arun Mozhi, Devi || 5:01
|- 4 || Kokki Vaichchen || Mano (singer)|Mano, Malaysia Vasudevan, K. S. Chithra, Devi || 4:49
|- 5 || Nattupura Pattu || Manorama (Tamil actress)|Manorama, K. S. Chitra || 6:52
|- 6 || Satti Potti || Arun Mozhi, Devi || 2:31
|}

==References==
 

 
 
 
 
 