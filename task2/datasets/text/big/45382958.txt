Just Married (1928 film)
{{Infobox film
| name           = Just Married
| image          = 
| alt            = 
| caption        = 
| director       = Frank R. Strayer
| producer       = Jesse L. Lasky Adolph Zukor Frank Butler George Marion Jr. Adelaide Matthews Anne Nichols Gilbert Pratt James Hall Ruth Taylor Harrison Ford William Austin Ivy Harris Tom Ricketts Maude Turner Gordon
| music          = 
| cinematography = Edward Cronjager
| editing        = William Shea B. F. Zeidman
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 comedy silent Frank Butler, James Hall, Ruth Taylor, Harrison Ford, William Austin, Ivy Harris, Tom Ricketts and Maude Turner Gordon. {{cite web|url=http://www.nytimes.com/movie/review?res=9C05E4D9103AE03ABC4B52DFBE668383639EDE|title=Movie Review -
  Just Married - THE SCREEN; An Old Ufa Film. Other Photoplays. - NYTimes.com|work=nytimes.com|accessdate=12 February 2015}}   The film was released on August 18, 1928, by Paramount Pictures.

==Plot==
 

== Cast == James Hall as Bob Adams Ruth Taylor as Roberta Harrison Ford as Jack Stanley William Austin as Percy Jones
*Ivy Harris as Mrs. Jack Stanley
*Tom Ricketts as Makepeace Witter
*Maude Turner Gordon as Mrs. Witter
*Lila Lee as Victoire
*Arthur Hoyt as Steward
*Wade Boteler as Purser
*Mario Carillo as Magnoir 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 