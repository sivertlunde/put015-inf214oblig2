Kuch Naa Kaho
 
 
{{Infobox film
| name           = Kuch Naa Kaho
| image          = Kuchnaakaho.jpg
| caption        = Theatrical release poster
| director       = Rohan Sippy
| producer       = Ramesh Sippy
| story          = Rohena Gera
| screenplay     = Neeraj Vora Arbaaz Khan
| music          = Shankar-Ehsaan-Loy
| cinematography = V. Manikandan
| editing        = Rajiv Gupta
| studio         =
| distributor    = Ramesh Sippy Entertainment
| released       =  
| runtime        = 176 mins
| country        = India
| language       = Hindi
| budget         =
| gross          =  
| website        =
}}
 Arbaaz Khan. It was released on 5 September 2003.

==Plot==

Happy American bachelor Raj (Abhishek Bachchan) reluctantly attends his cousins wedding in Mumbai where he finds himself pushed towards marriage by his overzealous uncle (Satish Shah). His uncles employee, Namrata (Aishwarya Rai), begrudgingly chaperones Raj on a series of set-ups that he deliberately sabotages. Eventually on one of these dates, Raj watches Namrata dancing and realises he loves her. He finally gets the strength to tell her, but to his surprise he discovers she has a 9- or 10-year old son, Aditya, and is already married.

Raj is confused, but forms a strong fatherly relationship with her son anyway. His uncle informs him that Namratas husband disappeared right before Adityas birth; although Namrata is still technically married, Raj takes heart. Soon enough, however, she finds out about Rajs feelings. In turmoil, she attempts to distance herself from him, knowing they can never be together. Nevertheless, Raj follows her to Adityas boarding school, having promised the boy to pose as his father. While Raj drives Namrata home, he sarcastically comments that her circumstances must have been her fault. Upset, she tells Raj that her husband was the one that left her for another woman whilst she was pregnant. Eventually Namrata comes round and they both get together.
 Arbaaz Khan) walks back into her life to begin where they left off - but first, admitting he and his mistress have been engaged in criminal activity since his departure. Namrata does not want anything to do with him, but she is powerless to make him leave, even though she is now in love with Raj. After a chance meeting with Sanjeev, Raj invites Sanjeev to his cousins wedding. Adi also comes and goes straight to Raj and calls him "Dad." Sanjeev, jealous and angry, insults Raj and his family and creates a scene at the wedding. Namrata finally gets the courage to tell Sanjeev publicly that she deplores his abandonment of her and the child, his irresponsible and immoral acts, considers herself no longer his wife, and that she is in love with Raj. Raj and Namrata get married, have a child and live happily ever after.

==Cast==

* Abhishek Bachchan as Raj
* Aishwarya Rai Bachchan as Namrata  Arbaaz Khan as Sanjeev
* Satish Shah as Rakshas
* Master Parth Dave as Aditya
* Jaspal Bhatti as Binni Ahluwalia
* Suhasini Mulay as Dr. Spocks
* Himani Shivpuri as Lemon Ahluwalia
* Razzak Khan as Bird Walker
* Divya Palat as Rachna Singh Gangwar
* Tanaz Currim as Ms. Loccolith
* Ramona Sunavala as Babeethaa
* Jennifer Winget as Pooja
* Yusuf Hussain as Roshni Sehgal
* Zoya Afroz as Aarya
* Eijaz Khan

==Soundtrack==
{{Infobox album |  
  Name        = Kuch Naa Kaho |
  Type        = Album |
  Artist      = Shankar-Ehsaan-Loy |
  Cover       = Kuchnaakahoalbumcover.jpg|
  Released    =  4 April 2003 (India)|
  Recorded    = | Feature film soundtrack |
  Length      = |
  Label       =  Sa Re Ga Ma |
  Producer    = Shankar-Ehsaan-Loy |
  Reviews     = |
  Last album  = Armaan (2003 film)|Armaan (2003) |
  This album  = Kuch Naa Kaho (2003) |
  Next album  = Kal Ho Naa Ho (2003)|
}}
The music is composed by Shankar-Ehsaan-Loy. Lyrics are penned by Javed Akhtar.

===Track listing===
{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = yes
| title1 = ABBG
| extra1 = Mahalakshmi Iyer, Udit Narayan
| lyrics1 = Javed Akhtar
| length1 =
| title2 = Achchi Lagti Ho
| extra2 = Kavita Krishnamurthy, Udit Narayan
| lyrics2 = Javed Akhtar
| length2 =
| title3 = Baat Meri Suniye To Zara
| extra3 = Mahalakshmi Iyer, Shankar Mahadevan
| lyrics3 = Javed Akhtar
| length3 =
| title4 = Kehti Hai Yeh Hawa Richa Sharma, Shankar Mahadevan
| lyrics4 = Javed Akhtar
| length4 =
| title5 = Kuch Naa Kaho Shaan
| lyrics5 = Javed Akhtar
| length5 =
| title6 = Tumhe Aaj Maine Jo Dekha
| extra6 = Shankar Mahadevan, Sujata Bhattacharya
| lyrics6 = Javed Akhtar
| length6 =
}}

==Box office==
Kuch Naa Kaho had an amazing opening. It collected 
 81,536,000 and despite of good opening it was declared a flop at the Indian Box 
Office. 

==References==
 

==External links==
* 
* 

 
 
 
 
 