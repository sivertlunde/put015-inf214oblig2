Illegal Entry (film)
{{Infobox film
| name           = Illegal Entry
| image          = Illegal entry 1949 poster small.jpg
| image_size     =
| alt            =
| caption        = Theatrical release lobby card
| director       = Frederick De Cordova
| producer       = Jules Schermer
| screenplay     = Joel Malone
| story          = Ben Bengal Herbert Kline Dan Tyler Moore 	
| narrator       =
| starring       = Howard Duff Märta Torén George Brent
| music          = Milton Schwarzwald
| cinematography = William H. Daniels
| editing        = Edward Curtiss
| studio         =
| distributor    = Universal Pictures
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Illegal Entry is a 1949 American film noir thriller directed by Frederick De Cordova, and starring Howard Duff, Märta Torén and George Brent. 

==Plot summary==
An undercover agent (Howard Duff) attacks a Mexican border smuggling operation.

==Cast==
* Howard Duff as Bert Powers
* Märta Torén as Anna Duvak ONeill
* George Brent as Chief Agent Dan Collins
* Gar Moore as Lee Sloan
* Tom Tully as Nick Gruber Paul Stewart as Zack Richards
* Richard Rober as Dutch Lempo
* Joseph Vitale as Joe Bottsy
* James Nolan as Agent Benson
* Clifton Young as Billy Rafferty David Clarke as Carl
* Robert Osterloh as Agent Crowthers Anthony Caruso as Teague
* Donna Martell as Maria

==Reception==

===Critical response===
The New York Times film critic, Bosley Crowther, gave the film a mixed review, "A formidable introduction which features Attorney General Tom Clark and certain Immigration Bureau oficials who bespeak that service well does not camouflage Illegal Entry ... Howard Duff plays this hero in an acceptable rough-and-ready style and Marta Toren is attractive as the girl ... The backgrounds of southern California and Mexico are authentic enough. But the whole picture has the quality of a mechanical, oft-repeated show." 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 