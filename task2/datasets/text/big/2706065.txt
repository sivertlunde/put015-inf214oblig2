Red Sun
 
{{Infobox film
| title          = Red Sun
| image          = Red_sun_movieposter.jpg
| image_size     =
| caption        = film poster Terence Young
| producer       = Ted Richmond William Roberts
| starring       = Ursula Andress Charles Bronson Alain Delon Toshirō Mifune
| music          = Maurice Jarre
| cinematography = Henri Alekan
| editing        = Johnny Dwyre
| distributor    = National General Pictures (US) Films Corona (France)
| released       = Europe 1971 United States 1972
| runtime        = 112 min.
| country        = France Italy Spain   
| language       = English 
| budget         =
| gross          =3,300,488 admissions (France) 
}}
 Western film Terence Young. It was released in Europe in 1971 and in the U.S. in 1972.

==Plot==
Link Stuart (Bronson) is a ruthless outlaw, and co-leader along with Gauche (Delon) of a gang of bandits. Link and Gauche lead their gang on a successful train robbery, and discover that one of its cars carries a Japanese ambassador, who is bringing a ceremonial katana (sword) as a gift for President Ulysses S. Grant. Gauche takes the sword, and kills one of the two samurai guards, while members of his gang attempt to murder Link by throwing dynamite into the train car he occupies, then leaving him for dead.

The surviving Japanese delegation rescues Link, and the ambassador instructs him to assist the surviving samurai guard, Kuroda (Mifune), in tracking down Gauche so that he may kill him and recover the sword and his honor. Kuroda is given one week to fulfill this task, or commit seppuku. Link reluctantly agrees, but he realizes that Kuroda will kill Gauche immediately, before he is able to extract the location of the stolen loot. Link repeatedly attempts to elude Kuroda, only to be thwarted by the irrepressible samurai.

While tracking Gauches gang, Kuroda eventually reveals that his samurai values are disappearing as his countrymen no longer value the customs of old. Link gains a measure of respect for the strict bushido code Kuroda follows, and eventually comes to an agreement with the samurai that Gauche will not be killed before he reveals the location of the stolen money first. The duo eventually abduct Gauches woman, Cristina (Andress), who leads the men to Gauche and his gang.

On the way to Gauche, however, the three run afoul of a group of Comanches, and Cristina is forced to kill one of them in self-defense, compelling the bands enraged chief into chasing after them. When Link and Kuroda finally find Gauche, the Indians attack, forcing the two unlikely friends to join forces with the bandits against their common enemy. In the ensuing fight, the Comanches are repelled, but Kuroda is mortally wounded by Gauche as he tries to fulfill his revenge. Disarmed by Link, Gauche tries appealing to Links greed, but Link decides that the dying samurais honor is more important to him than learning the location of the stolen money, so he kills Gauche. Just before Kuroda expires, Link promises him that he will return the katana to the Japanese ambassador. He does so, thus preserving Kurodas honor.

==Cast==
* Charles Bronson as Link Stuart
* Ursula Andress as Cristina
* Toshirō Mifune as Kuroda Jubei
* Alain Delon as Gauche
* Capucine as Pepita
* Barta Barri as Paco
* Guido Lollobrigida as Mace
* Anthony Dawson as Hyatt 
* Georges Lycan as Sheriff Stone (as George W. Lycan)
* Luc Merenda as Chato  Tetsu Nakamura as the Japanese Ambassador 
* Mónica Randall as Maria 

===Cast notes===
Bronson starred in The Magnificent Seven, an American remake of Seven Samurai, which featured Mifune.

==Production==
Toshiro Mifune signed early. Clint Eastwood was mentioned as a possible early co star. Tate Case Chatter Goes On--and On
Los Angeles Times (1923-Current File)   22 Sep 1969: e19.   It was originally to be made for Warner Bros but was eventually made by Frances Corona Films, headed by Robert Dorfman and Ted Richmond. MOVIE CALL SHEET: Wendell Burton to Star
Martin, Betty. Los Angeles Times (1923-Current File)   23 Oct 1970: d17.   
==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 