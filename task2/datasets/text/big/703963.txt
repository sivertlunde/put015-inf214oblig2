Some Kind of Wonderful (film)
{{Infobox film
| name           = Some Kind of Wonderful
| image          = Some kind of wonderful poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Howard Deutch John Hughes
| writer         = John Hughes John Ashton
| music          = Stephen Hague John Musser
| cinematography = Jan Kiesser
| editing        = Bud S. Smith M. Scott Smith Hughes Entertainment
| distributor    = Paramount Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $18.5 million 
}} John Hughes in the 1980s, although this one was directed by Howard Deutch.

==Plot== American State public high school. The tomboyish Watts (Mary Stuart Masterson) has always considered working class misfit Keith Nelson (Eric Stoltz) her best friend. But when Keith asks out the most popular girl in school, Amanda Jones (Lea Thompson), Watts realizes she feels something much deeper for him. Meanwhile, Hardy Jenns (Craig Sheffer), Amandas Chevrolet Corvette|Corvette-driving spoiled ex-boyfriend from the rich section of town, plans to cause trouble for Keith; although he has no real feelings for Amanda, he feels humiliated at losing her to a social inferior. However, Keith cunningly manages things to his own and Amandas advantage and gives Hardy his well-deserved comeuppance by simply telling him that by using his money to treat people like possessions and toys he will never know true happiness or have anyone in his life that will appreciate him for more than just his money and good looks. In the end, Amanda realizes that she needs time alone and Keith clues into Wattss feelings for him.

==Cast==
* Eric Stoltz as Keith Nelson
* Mary Stuart Masterson as Watts
* Lea Thompson as Amanda Jones
* Craig Sheffer as Hardy Jenns John Ashton as Cliff Nelson
* Elias Koteas as Duncan
* Molly Hagan as Shayne
* Maddie Corman as Laura Nelson
* Jane Elliot as Carol Nelson
* Candace Cameron Bure as Cindy Nelson
* Chynna Phillips as Mia
* Scott Coffey as Ray
* Carmine Caridi as Museum Guard
* Lee Garlington as Gym Instructor
* Pamela Anderson as Party Guest

==Production== Howard the Duck—had turned out to be a major bomb. 

==Reception==
The film was generally well received by critics. Review aggregator website Rotten Tomatoes reported that 80% of critics gave it a positive rating, based on 35 reviews.    Roger Ebert of the Chicago Sun-Times praised the film, calling it worthwhile and entertaining.    Janet Maslin of The New York Times stated that Some Kind of Wonderful is the "much-improved, recycled version of the Pretty in Pink story".    Richard Schickel of Time magazine|Time, however, criticized the film for being unrealistic.  Mastersons performance was singled out for praise by several critics.    

==Soundtrack==
{{Infobox album  
| Name = Some Kind of Wonderful
| Type = Soundtrack
| Artist = Various Artists
| Cover =
| Released = February 27, 1987
| Recorded = new wave
| Length = 36:20
| Label = MCA Records
| Producer =
}}
{{Album ratings
| rev1 =  Allmusic
| rev1Score =      
}}
# "Do Anything" – Pete Shelley Furniture
# "Cry Like This" – Blue Room I Go Crazy" – Flesh for Lulu
# "She Loves Me" – Stephen Duffy
# "The Hardest Walk" – The Jesus and Mary Chain
# "The Shyest Time" – The Apartments
# "Miss Amanda Jones" – The March Violets
# "Cant Help Falling in Love" – Lick the Tins
# "Turn to the Sky" – The March Violets

== See also ==
 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 