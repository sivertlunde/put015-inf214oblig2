O meu marido esta a negar
{{Infobox film
| name           = O meu marido esta a negar 
| image          = O meu marido esta a negar.jpg
| caption        = Screenshot
| director       = Rogério Manjate
| producer       = Força Maior
| writer         = 
| starring       = Hermínia Novais, Gabriel António
| distributor    = 
| released       = 2007
| runtime        = 21 minutes
| country        = Mozambique
| language       = 
| budget         = 
| gross          = 
| screenplay     = Rogério Manjate
| cinematography = Panu Kari
| editing        = Panu Kari
| music          = Francisco Cardoso
}}
O meu marido esta a negar  is a 2007 documentary film about a play of the same name, written and directed by Rogério Manjate.
The film and play both discuss HIV/AIDS issues.  I Love You, also concerning HIV/AIDS, the same year. {{cite web
 |url=http://www.cca.ukzn.ac.za/index.php?option=com_content&view=article&id=227%3Arogerio-manjate-mozambique&catid=31&Itemid=45
 |title=Rogerio Manjate
 |publisher=Center for Creative Arts, University of KwaZulu-Natal
 |accessdate=2012-03-10}} 
The films title means "My Husband is in Denial". 

== Film synopsis ==

Hermínia discovers that she is an   and help change their behavior. 

==Play performances==

The Theatre of the Oppressed is an interactive style that originated in Brazil and has been exported to over 70 countries on five continents. 
The format was created by Augusto Boal, who was nominated for the Nobel Peace Prize for his success in using theater as a tool for social activism. {{cite web
 |url=http://www.irinnews.org/printreport.aspx?reportid=76575
 |work=IRIN
 |title=MOÇAMBIQUE: A arte imita a vida
 |date=5 February 2008
 |language=Portuguese
 |accessdate=2012-03-10}} 
"O meu marido esta a negar" has been performed as a play throughout Mozambique as part of  a program to overcome cultural obstacles in the treatment and prevention of HIV.
It is presented in public places - markets, schools and businesses - and the public is invited to attend and present their own solutions to the unequal power relations shown in the play.
The result is more effective than a lecture in Portuguese or distribution of written material to a largely illiterate population. {{cite web
 |url=http://eportuguese.blogspot.com/2008/02/moambique-arte-imita-vida.html
 |date=February 19, 2008
 |title=MOÇAMBIQUE: A arte imita a vida
 |language=Portuguese
 |publisher=World Health Organization
 |accessdate=2012-03-10}} 

Alvim Cossa, an actor who lost four members of his family from HIV/AIDS, described a performance in a busy market in Maputo: "We asked the public to put themselves in the place of the oppressed character, the pregnant wife, and to suggest solutions to her dilemma. Dressed in her skirt and head scarf, men and women in the audience took the place of the wife to encourage her husband to do the test, or explain that you can use traditional medicine, but for HIV should go to the hospital - all contribute to the education of the spectators". 

==Festival screenings==

The film was screened at the Durban International Film Festival held between 23 July to 3 August 2008. {{cite web
 |url=http://dbnweb2.ukzn.ac.za/cca/DIFF_2008%20-%20schedule.htm
 |title=29th Durban International Film Festival : 23 July to 3 August, 2008
 |publisher=Durban International Film Festival
 |accessdate=2012-03-10}} 
In October 2008 it was shown at the Festival Dockanema in Maputo. {{cite web
 |url=http://www.signis.net/article.php3?id_article=2754
 |work=SIGNIS
 |title=Festival Dockanema à Maputo : cinéma documentaire et mémoire
 |date=13 October 2008
 |author=Guido Convents
 |language=French
 |accessdate=2012-03-10}} 
It has also been screened at the 2009 African Film Festival of Cordoba. 

== References ==
{{Reflist |refs=
 {{cite web
 |url=http://www.fcat.es/FCAT/index.php?option=com_zoo&task=item&item_id=545&Itemid=37
 |title=O meu marido esta a negar
 |publisher=FCAT
 |accessdate=2012-03-10}} 
}}
 

 
 
 
 
 
 
 
 