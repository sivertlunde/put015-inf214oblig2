Maanmizhiyaal
{{Infobox film 
| name           = Maanmizhiyaal
| image          =
| caption        =
| director       = Krishnaswamy
| producer       = MS Vasavan
| writer         = MS Vasavan
| screenplay     = MS Vasavan Ashokan Sithara Sithara Shari Shari Sukumari
| music          = Murali Sithara
| cinematography = ENC Nair
| editing        = Rajasekharan
| studio         = Sreemuruka Arts Productions
| distributor    = Sreemuruka Arts Productions
| released       =  
| country        = India Malayalam
}}
 1990 Cinema Indian Malayalam Malayalam film, Shari and Sukumari in lead roles. The film had musical score by Murali Sithara.   

==Cast== Ashokan
*Sithara Sithara
*Shari Shari
*Sukumari
*Maniyanpilla Raju
*Jagannatha Varma
*KPAC Sunny
*Poojappura Ravi
*Baby Jinju as Child Artist

==Soundtrack==
The music was composed by Murali Sithara and lyrics was written by Vayalar Madhavankutty. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Illikkaatile || K. J. Yesudas || Vayalar Madhavankutty || 
|-
| 2 || Kunungikkunungi || Lathika, KV Sivadasan || Vayalar Madhavankutty || 
|}

==References==
 

==External links==
*  

 
 
 

 