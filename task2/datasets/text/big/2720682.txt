Summer in the Golden Valley
{{Infobox film
| name           = Summer in the Golden Valley
| image          = Ljetouzlatnojdolinidvd.jpg
| director       = Srđan Vuletić
| producer       = Ademir Kenović
| writer         = Srđan Vuletić
| starring       = Haris Sijarić Svetozar Cvetković Kemal Čebo
| music          = Simon Boswell
| cinematography = Slobodan Trninić
| editing        = Catherine Kelber
| music          = Edo Maajka - Zlatna Dolina
| distributor    =
| released       =
| runtime        = 105 min.
| country        = Bosnia and Herzegovina, France, United Kingdom Bosnian
| budget         =
| gross          =
}}

Summer in the Golden Valley (Original title in  n film by Srđan Vuletić, produced by Ademir Kenović. The movie is about a 16 year old boy who has to repay his dead fathers debt. In order to collect money, his friend and he get involved in Sarajevos underground crime.

== Plot ==
At the traditional Muslim funeral service for his father Fikret Varupa, sixteen-year-old boy from Sarajevo, learns that his father owes money to Hamid, a man he does not even know. The debt is considerable and Hamid does not want it to go to the grave with the body, so the debt automatically passes from the father to the son.
Since in Bosnia this way of collecting debts, at a funeral, is considered to be utterly humiliating, it is never, ever applied. Fikret and his entire family become subjects of ridicule.
Fikret, who is practically still a child, is decisive to "redeem his fathers soul". Wishing to repay his fathers debt and to secure the forgiveness, Fikret wanders into the real world of Sarajevo, the world that is ruled by post-war chaos, misery and poverty and becomes an ideal target for two corrupted policemen who wish to "help" him: they plant the kidnapped girl on him.

==Cast==
* Haris Sijarić as Fikret
* Svetozar Cvetković as Ramiz
* Kemal Čebo as Tiki
* Zana Marjanović as Sara
* Emir Hadžihafizbegović as Hamid
* Aleksandar Seksan as Cico
* Admir Glamocak as Klupa
* Dragan Jovičić as Jasmin
* Saša Petrović as Grand Father
* Ðani Jaha as Špico
* Miraj Grbić as Ćupo
* Maja Salkić as Air Hostess 1
* Amar Bektešević as Child actor
* Dženana Nikšić as Selma
* Senad Bašić as Ispijeni

==Awards and nominations==

===Wins===

*Bermuda International Film Festival - Jury Prize - 2004
*Golden Iris - Brussels European Film Festival - 2004
*Tiger Award - Rotterdam International Film Festival - 2004
*MovieZone Award - Rotterdam International Film Festival - 2004
*Sofia International Film Festival - Special Jury Award - 2004
*Sofia International Film Festival - FIPRESCI Prize - 2004

===Nominations===
*Sofia International Film Festival - Grand Prix - 2004
*Sarajevo Film Festival - The Heart of Sarajevo (Best Film Award)

==External links==
* 
* 

 

 
 
 
 
 
 


 
 