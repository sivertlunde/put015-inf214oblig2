Spookley the Square Pumpkin
{{Infobox Film
| name           = Spookley the Square Pumpkin
| image          = Poster of the movie Spookley the Square Pumpkin.jpg
| image_size     = 190px
| caption        = DVD cover art
| director       = Bernie Denk
| producer       = Jeffrey Zahn & Mark Zander (producers) Jonathon Flom & Joe Troiano (executive producers)
| writer         = Tom Hughes (screenplay), Joe Troiano (story)
| narrator       = Bobby "Monster Mash" Pickett
| starring       = Ben Small
| music          = Joe Troiano & Jeffrey Zahn
| cinematography = Michael Sherman
| editor         = Muvico
| distributor    = Kidtoon Films Lionsgate
| released       =  
| runtime        = 45 minutes
| country        = Canada
| language       = English
| budget         = unknown
}}
Spookley the Square Pumpkin is a 2004 computer-animated film released direct-to-DVD in January 2005   and is based on the book The Legend of Spookley the Square Pumpkin by Joe Troiano. It was made by Holiday Hill Farm, and released by Kidtoon Films and Lionsgate. It had its television premiere on October 16, 2013 on Cartoon Network. The Legend of Spookley the Square Pumpkin airs on Disney Junior every autumn.  Spookleys Favorite Halloween Songs was released in 2012.  Several Spookley the Square Pumpkin activity books were also released.  

==Plot==
Spookley the Square Pumpkin is found in the pumpkin patch by two bats, Bella and Boris. The round pumpkins tease Spookley, which made him sad. At first the other pumpkins make fun of him because Spookley is square. Later, Jack (the scarecrow) allows him to compete in the Jack-a-Lympics contest. Three spiders, who show sympathy for Spookley, decide to help him in the Jack-a-Lympics just so they can help themselves to the prize, a crown made of candy corn. One of the spiders is revealed to have a crush on Bella. Eventually, Spookley shows kindness to the other pumpkins that made fun of him by saving their lives on the farm during a terrible storm, since hes immune to the storms strong conditions. At the end, it shows the farmer finding Spookley and carrying him to his house where he places Spookley on the porch lit up with a candle for all to see. The other pumpkins begin to glow as well.

==External links==
*  
*  

 
 
 
 
 
 
 

 
 