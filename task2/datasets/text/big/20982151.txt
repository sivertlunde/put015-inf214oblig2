Murder Seen
{{Infobox Film
| name           = Murder Seen
| director       = Rob W. King
| producer       = Minds Eye Pictures Thomega Entertainment Inc.
| writer         = Marilyn Webber
| starring       = Nicole Eggert Callum Keith Rennie Timothy Bottoms Kent Allen
| music          = Rob Bryanton
| released       = 2000
| runtime        = 90 minutes
| country        = Canada
| language       = English
}} 2000 Canada|Canadian film by Rob W. King.

== Plot ==
Murder Seen centers around Zoey Drayden (Nicole Eggert), a botany grad student, who happens to experience disturbing visions. After receiving a phone call for help late at night and discovering some proof that her visions and the phone call have an actual basis, she goes to the police. They link her story to the case of an abducted student and first detain her as the only  suspect. Detectives Keegan (Callum Keith Rennie) and Stepnoski (Timothy Bottoms) eventually give some credit to her assertions. But when they find a new suspect and are obliged to release him for lack of proof, Zoey decides to track him herself.

== Cast ==
*Nicole Eggert (VF. Véronique Alycia) as Zoey Drayden
*Callum Keith Rennie as Detective Keegan
*Timothy Bottoms as Detective Stepnoski
*Kent Allen as Ivan Stark
*Will Sanderson as Craig Masters
*Gerald Lenton-Young as Lieutenant
*Lisa Marie Pollock as Janette Collier

== Filming ==
The film was produced by Minds Eye Entertainment of Regina in association with Thomega Entertainment Inc. of Saskatoon (Canada). It was the second of eight action thrillers of the "Saskatoon Initiative". 
 
While set in and around a college campus in Minneapolis, Minnesota, Murder Seen was filmed on location in Saskatoon, namely in the University of Saskatchewan Campus. 

== References ==

 

== External links ==
*  

 
 
 
 
 
 
 