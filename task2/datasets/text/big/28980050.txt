East, West, East: The Final Sprint
{{Infobox film
| name           = East, West, East: The Final Sprint
| image          = 
| caption        = 
| director       = Gjergj Xhuvani
| producer       = High Point Films 
| writer         = Gjergj Xhuvani
| starring       = Shkumbin Istrefi Nela Lucic
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Albania Italy
| language       = Albanian Italian Slovenian Serbian
| budget         = 
}}
East, West, East: The Final Sprint ( ) is an Albanian comedy film directed by Gjergj Xhuvani. The film is about an amateur cycling team from Albania that learns while on their way to France to take part in a tournament that a revolution is underway in Albania.

==Awards== Best Foreign Language Film at the 83rd Academy Awards,    but it didnt make the January shortlist.   

Gjergj Xhuvani won the Best Director award at the Tirana Film Festival. 

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Albanian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 


 
 