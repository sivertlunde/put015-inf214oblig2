While I Live
{{Infobox film
| name           = While I Live
| image          = Whileilive1947.jpg
| image_size     = 200px
| caption        = UK DVD cover John Harlow
| producer       = Edward Dryhurst John Harlow, Doreen Montgomery
| starring       = Sonia Dresdel Tom Walls Carol Raye Charles Williams
| cinematography = Freddie Young
| editing        = Doug Robertson 20th Century Fox (UK)
| released       = 7 October 1947
| runtime        = 85 min.
| country        =   English
}}
 1947 British John Harlow. Charles Williams, reprised at intervals throughout the film, which became hugely popular in its time and is still regularly performed.  The film itself became widely known as The Dream of Olwen.

==Plot==
In 1922 in Cornwall, a prodigious young pianist and composer Olwen Trevelyan (Audrey Fildes) is struggling with the ending of a piano tone poem she is composing.  Driven to complete the piece by her domineering elder sister Julia (Sonia Dresdel), Olwen becomes agitated and despondent, and one night sleepwalks to the edge of a cliff near their home.  Julia follows her and shouts her name but Olwen, abruptly awakened, loses her balance and falls to her death on the rocks below.  Julia is unable to come to terms with Olwens death and the guilt of her own role in it, over the years becoming a reclusive, obsessive figure whose main raison dêtre is to keep Olwens memory alive.  Olwens final composition gains her posthumous recognition, and each year on the anniversary of her death it is broadcast on the radio.
 faith healer Nehemiah (Tom Walls), who also claims second sight, becomes involved and it seems increasingly as though history is repeating itself, culminating when the young woman too is found standing precariously on the edge of the cliff from which Olwen fell.

==Cast==
* Sonia Dresdel as Julia Trevelyan
* Tom Walls as Nehemiah
* Carol Raye as Sally Grant
* Patricia Burke as Christine Sloan
* Clifford Evans as Peter Sloan
* John Warwick as George Grant
* Audrey Fildes as Olwen Trevelyan
* Charles Victor as Sgt. Pearne
* Edward Lexy as Selby
* Ernest Butcher as Ambrose
* Enid Hewit as Ruth
* Sally Rogers as Hannah

==References==
 
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 