King of Chinatown
{{Infobox film
| name           = King of Chinatown 
| image          = King of Chinatown poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Nick Grinde
| producer       = Harold Hurley
| screenplay     = Lillie Hayward Irving Reis
| story          = Herbert J. Biberman
| starring       = Anna May Wong Akim Tamiroff J. Carrol Naish Sidney Toler Philip Ahn Anthony Quinn Bernadene Hayes
| music          = Gerard Carbonara John Leipold
| cinematography = Leo Tover
| editing        = Eda Warren
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 57 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

King of Chinatown is a 1939 American crime film directed by Nick Grinde and written by Lillie Hayward and Irving Reis. The film stars Anna May Wong, Akim Tamiroff, J. Carrol Naish, Sidney Toler, Philip Ahn, Anthony Quinn and Bernadene Hayes. The film was released on March 17, 1939, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Anna May Wong as Dr. Mary Ling
*Akim Tamiroff as Frank Baturin
*J. Carrol Naish as Professor
*Sidney Toler as Dr. Chang Ling
*Philip Ahn as Robert Bob Li
*Anthony Quinn as Mike Gordon
*Bernadene Hayes as Dolly Warren
*Roscoe Karns as Rip Harrigan
*Ray Mayer as Potatoes
*Richard Denning as Protective Association Henchman
*Archie Twitchell as Hospital Interne
*Eddie Marr as Henchman Bert George Anderson as Detective
*Charles B. Wood as Henchman Red
*George Magrill as Second Gangster
*Charles Trowbridge as Dr. Jones
*Lily King as Chinese Woman
*Wong Chung as Chinese Man
*Chester Gan as Mr. Foo
*Pat West as Fight Announcer
*Guy Usher as Investigator

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 