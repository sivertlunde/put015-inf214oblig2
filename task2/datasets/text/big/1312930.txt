Wrong Is Right
 
{{Infobox film
| name = Wrong Is Right
| image          = Wrong_is_right.jpg
| image_size     =  
| caption        = original movie poster
| director       = Richard Brooks
| producer       = Richard Brooks Andrew Fogelson George Grenville
| screenplay     = Richard Brooks
| based on       =  
| starring       = {{plainlist|
* Sean Connery
* Robert Conrad
* George Grizzard
* Hardy Krüger
* Ron Moody
* Leslie Nielsen
* Katharine Ross John Saxon
* Henry Silva
* G. D. Spradlin
* Robert Webber
* Rosalind Cash
}}
| music          = Artie Kane
| cinematography = Fred J. Koenekamp George Grenville
| studio         =
| distributor    = Columbia Pictures
| released       =  
| runtime        = 117 min.
| country        = United States
| language       = English
| budget         =
| gross          = $3,583,513 (Dom.)   
}} thriller film government conspiracy, and Islamic terrorism.

==Plot== Arab country of Hegreb to interview his old acquaintance, King Ibn Awad (Ron Moody).

Awad has learned that the President of the United States (George Grizzard) may have issued orders for his removal; as a result, Awad is apparently making arrangements to deliver two suitcase nukes to a terrorist, with the intention of detonating them in Israel and the United States, unless the President resigns.

In the intricate plot that unfolds, nothing is quite the way it seems, and Hale finds himself caught between political leaders, revolutionaries, CIA agents and other figures, trying to get to the bottom of it all.

==Cast==
* Sean Connery as Patrick Hale
* Robert Conrad as General Wombat
* George Grizzard as President Lockwood
* Katharine Ross as Sally Blake
* G.D. Spradlin as Philindros John Saxon as Homer Hubbard
* Henry Silva as Rafeeq
* Leslie Nielsen as Mallory
* Hardy Krüger as Helmut Unger
* Robert Webber as Harvey
* Rosalind Cash as Mrs. Ford
* Dean Stockwell as Hacker
* Jennifer Jason Leigh as Young Girl
* Mickey Jones as Gunman

==Awards==
Rosalind Cash was nominated for an Image Award for Best Performance by an Actress in a Motion Picture.

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 