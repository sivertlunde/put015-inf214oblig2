Bharatamlo Arjunudu
{{Infobox film
| name           = Bharatamlo Arjunudu
| image          =
| alt            =  
| caption        =
| director       = K. Raghavendra Rao K S Prakash Rao
| writer         = Paruchuri Brothers
| story          = Javed Akhtar
| screenplay     = K. Raghavendra Rao Venkatesh Kushboo Kushboo
| Chakravarthy
| cinematography = K S Prakash
| editing        = Kotagiri Venkateswara Rao
| studio         = Prakash Studios
| distributor    =
| released       =    
| runtime        = 140 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Telugu film K S Venkatesh and Kushboo played the lead roles and music composed by K. Chakravarthy|Chakravarthy. 
   The film was a remake of Hindi film Arjun (1985 film)|Arjun. The film recorded as flop at the box-office. 

==Plot==
Arjun (Venkatesh) is unemployed but tolerant and kind hearted stays with his father Dasaradharamayah (P. L. Narayana) and step mother (Tatineni Rajeswari) and step sister Kalyani (Samyutha). One day Arjun beats up a group of ruffians who are thrashing a poor man for not paying extortion taxes headed by Ungarala Ramappa & Ungarala Kishtappa (Paruchuri Brothers) protected by MLA Benerjee (Rao Gopal Rao). With this incident, Arjuns life changes, he invokes the wrath of the local goon. Ugarala Kishtapa warns his parents and insults his sister, at the infuriated Arjun thrashes him and destroys all his activities. Arjun is arrested C.I. Keshava Rao (Nutan Prasad) henchmen to Benerjee let off him with a warning, but a Inspector Shekar (Betha Sudhakar|Sudhakar) notes that Arjun is actually doing the right thing, later he also begins to fall in love with Arjuns sister Kalyani. Arjun is also fall in love by Sensational Subhadra (Kushboo Sundar|Kushboo), a journalist his college meet. 
 Sai Kumar) is killed in the public. Though Arjun tries his best, no one comes forward to give witness to the murder out of fear, because of which the killers are left free. Soon Arjuns family put him out of the house, he is approached by Ranganayakulu, he invites him to his house treats him as his own son. With Arjuns to help Ranganayakulu destroys all illegal activities of Benerjee and also gets his sister Kalyanis marriage with Shekar at her request, and also handles the shop owner where his father is working for ill treating the latter. Finally, Ranganayakulu tells Arjun to get some secret files and documents against Benerjee, which can be used to expose him in public. Arjun gets that secret file on his life risk. 

Now story takes a twist Arjun comes to known that Ranganayakulu is a jackal who played double game with him and has joined hands with Benerjee and that none of the evidence he collected has really been published anywhere, as promised. Frustrated, angered, Arjun goes to fight the politicians at their speech rally but is simply thrown out. Finally, Arjun recollects all the evidences and keeps them before public and court, all the baddies are arrested and the film ends with marriage Arjun & Subhadra.

==Cast==
  Venkatesh as Arjun Kushboo as Sensational Subhadra
*Rao Gopal Rao as M.L.A.Benerjee Allu Ramalingaiyah as Subhadras Father
*Nutan Prasad as C.I. Keshava Rao Ranganath as Ranganayakulu
*Kota Srinivasa Rao as Chenchuramayah Sudhakar as Inspector Shekar
*Paruchuri Brothers as Ungarala Ramappa & Ungarala Kishtappa
*Chalapathi Rao as Mallesh Sai Kumar as Gokhale
*P. L. Narayana as Dasarathara Chitti Babu as Bullet
*Chidatala Appa Rao as Tea Stale Babai 
*Mada as Panganamala Ranganatham Mucharalla Aruna as Jyothi
*Samyuktha as Kalyani
*Kuaili as Club Dancer
*Tatineni Rajeswari as Arjuns Step-Mother
*Kalpana Rai as Miss Swapna
*Nirmalamma as Gokhales Bamma
 

==Soundtrack==
{{Infobox album
| Name        = Bharatamlo Arjunudu
| Tagline     = 
| Type        = film Chakravarthy
| Cover       = 
| Released    = 1987
| Recorded    = 
| Genre       = Soundtrack
| Length      = 21:37
| Label       = Lahari Music Chakravarthy
| Reviews     =
| Last album  = Ajeyudu  (1987) 
| This album  = Bharatamlo Arjunudu  (1987)
| Next album  = Pasivadi Pranam  (1987)
}}
Music composed by K. Chakravarthy|Chakravarthy. Lyrics written by Jonnavithhula Ramalingeswara Rao. Music released on Lahari Music. 
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!length
|- 1
|Neeku Naaku Kudirenu SP Balu, S. Janaki
|4:16
|- 2
|Gongura Selona SP Balu,S. Janaki
|4:30
|- 3
|Andhala Konalo SP Balu,S. Janaki
|4:51
|- 4
|Nee Magasiri SP Balu,S. Janaki
|4:48
|- 5
|Agni Sikhala  SP Balu
|3:12
|}

== References ==
 

 
 
 
 
 

 
 