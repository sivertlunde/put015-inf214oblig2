Roped
 
 
{{Infobox film
| name           = Roped
| image          = 
| caption        = 
| director       = John Ford
| screenplay     = Eugene B. Lewis
| story          = Eugene B. Lewis Harry Carey
| music          = 
| cinematography = John W. Brown
| editing        = 
| distributor    = Universal Film Manufacturing Company
| released       =  
| runtime        = 60 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 Harry Carey. The film is considered to be lost film|lost.     Roped is one of at least 25 films in which director John Ford and actor Harry Carry collaborated on between the years of 1917 and 1921. Ford saw Carry as a mentor and their worked on the story ideas for several of their films together.

During these collaborations, Carry made more per film then Ford. By 1919, the year Roped came out, Ford was making 300 dollars a week, Carry was making 1,250.  This differential in pay led to tension between the two. 

==Plot==
Cheyenne Harry is a wealth ranch owner.  After his cowboys put an ad in the newspaper trying to find him a wife,  Harry marries Aileen Judson-Brown.  A year into their marriage, Aileen gives birth to their first child.  The new family live with Aileen’s status seeking mother, Mrs. Judson-Brown. Mrs. Judson-Brown tries everything in her power to break up the marriage so her daughter can marry the wealthier Ferdie Van Duzen. Mrs. Judson-Brown steals Harry and Aileen’s baby and tells Harry that Aileen no longer loves him and their baby has died. Heart broken, Harry moves out west.

Harry receives news from Mrs. Judson-Brown’s butler that his baby is still alive.  Harry finds his child and Aileen confesses her true love. The film ends with the reunited family heading West together, leaving Harry’s hateful mother-in-law behind.

==Cast== Harry Carey as Cheyenne Harry
* Neva Gerber as Aileen
* Mollie McConnell as Mrs. Judson-Brown (as Molly McConnell)
* Arthur Shirley as Ferdie Van Duzen
* J. Farrell MacDonald as Butler

==See also==
* Harry Carey filmography
* John Ford filmography
* List of lost films

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 