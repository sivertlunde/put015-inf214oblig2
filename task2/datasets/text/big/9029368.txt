Coral Reef Adventure
{{Infobox Film
| name           = Coral Reef Adventure
| image          = Coralreefadventure.jpg
| caption        = 
| director       = Greg MacGillivray 
| producer       = 
| writer         = Jack Stephens Osha Gray Davidson Stephen Judson
| narrator       = Liam Neeson Howard Hall Michele Hall Jean-Michel Cousteau 
| music          = Crosby, Stills & Nash
| cinematography = Brad Ohlund
| editing        = Stephen Judson
| distributor    = MacGillivray Freeman Films
| released       =  
| runtime        = 45 minutes
| country        = United States
| language       = English
| budget         = $10 million
}} American documentary IMAX theaters. It is narrated by actor Liam Neeson, and directed by Greg MacGillivray.
 South Pacific, husband and wife underwater photography-duo Michele and Howard Hall explore the declining reefs and failing health of the worlds oceans. From Australias Great Barrier Reef, to a friends coral reef-sustained village in Fiji, the diving expeditions show a range of coral reefs, from flourishing ones filled with unusual and exotic inhabitants, to vast stretches of bleached coral decline which prompted the Halls activism. Along their journey, scientists working to understand and save the reefs meet with the Halls.
 famed oceanographer Jacques Cousteau, also makes an appearance, as do well-known dive guide and singer Rusi Vulakoro,  brother of Vude singer Laisa Vulakoro, who guides the Halls in their dive adventure.

This documentary film is the third oceanic, ecologically-themed IMAX production from director MacGillivray, after The Living Sea and Dolphins (2000 film)|Dolphins. Crosby Stills & Nash contribute to the films soundtrack. The Giant Screen Theater Association named it the best film achievement of 2003. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 
 