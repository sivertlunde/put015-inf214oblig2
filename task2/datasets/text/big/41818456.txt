One of a Kind (film)
 
{{Infobox film
| name           = One of a Kind
| image          = 
| caption        = 
| director       = François Dupeyron
| producer       = Paulo Branco
| writer         = François Dupeyron
| starring       = Grégory Gadebois Céline Sallette
| music          = Armelle Mahé   François Maurel
| cinematography = Yves Angelo
| editing        = Dominique Faysse
| distributor    = Alfama Films
| released       =  
| runtime        = 123 minutes
| country        = France
| language       = French
| budget         = 
}} Best Actor at the 39th César Awards.   

==Plot==
Frédi is a born healer who can save people by touching them with his hands. His mother bequeathed this gift to him. When she dies honours her by putting his aptitude to good use.

==Cast==
* Grégory Gadebois as Frédi
* Céline Sallette as Nina
* Jean-Pierre Darroussin as the father
* Marie Payen as Josiane
* Philippe Rebbot as Nanar

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 