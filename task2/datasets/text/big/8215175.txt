Termites of 1938
{{Infobox Film |
  | name           = Termites of 1938|
  | image          = TermitesTITLE.jpg |
  | caption        =  |
  | director       = Del Lord |
  | writer         = Elwood Ullman | John Ince Symona Boniface|
  | cinematography =  ) | 
  | editing        = Art Seid |
  | producer       = Charley Chase Hugh McCollum |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 16 33"|
  | country        = United States
  | language       = English
}}

Termites of 1938 is the 28th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
A high-society woman named Muriel (Bess Flowers) wants to attend a swanky dinner party thrown by a friend of hers, but her husband, Arthur, decides instead to go on a fishing trip.  Rather than showing up stag to the party, Muriel tells her housekeeper to call the Acme Escort Service to bring a few "college boy" escorts. However, the housekeeper accidentally calls the Acme Exterminator Co., run by the Stooges. After testing out a few unorthodox techniques involving catching a mouse, including Curly playing a piccolo and Moe being shot into a wall by a loaded cannon, the phone rings. Moe, his ears still ringing from the cannon, answers and, unwittingly, the boys are hired to be Muriels escorts to the party.

At the mansion, the guests all arrive, and not long afterwards the Stooges show up in tuxes in a beat-up car. The Stooges, convinced that they were hired to clear the house of pests, are about to get to work when they hear that dinner is served. Moe tells his pals that they "always feed ya at these high class joints", and the Stooges rush to the dining room to eat. The Stooges astonish the guests with their lack of proper dinner etiquette. 

After dinner, the guests enter the main hall where a small group of musicians are playing. The Stooges then take over and begin to perform while actually syncing to music playing from a nearby victrola. The Stooges continue their mock performance until Moe accidentally grabs a saw and cuts the bass in half. Several mice inside begin to scurry about, causing the guests to run away, and the boys decide then and there to get to work. The boys make a shambles of the house.

Larry and Curly drill holes into the wall and spray for termites, not realizing theyre actually drilling into the next room.  Moe places a large fancy carpet onto some rope to spray for moths. After having no luck finding termites, Larry and Curly try one more time, only to accidentally hit Muriel with the tip of the drill. She screams and jumps, causing her to break into the stairs. Its at this point that Arthur, the owner of the house, arrives home to find Muriel stuck halfway out of the stairs.  He finds the Stooges, and chases them out of the mansion, wielding a Gopher Bomb.  The boys run to their car and make an escape, but Arthur throws the bomb into the car, causing it to explode and scatter the Stooges all over the street.

==Production notes==
Termites of 1938 was filmed on October 19–23, 1937.    The films title is a parody of the film title Gold Diggers of 1937.    The film has two musical quirks unusual for Stooge shorts. First, the Three Stooges opening theme, "Listen to the Mockingbird," is played again when the Stooges first appear onscreen.  Second, music derived from Victor Schertzingers score for the 1933 Columbia feature Cocktail Hour is featured during the dinner scene.    This is the result of producer Charley Chase, who liked incidental music from his time at the Hal Roach studio. 
 Tom Kennedy. 

==References==
 

==External links==
*  .
*  
*  

 

 
 
 
 
 
 
 