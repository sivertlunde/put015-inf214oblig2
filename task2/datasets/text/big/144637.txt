After the Thin Man
{{Infobox film
| name           = After the Thin Man
| image          = Afterthethinman.jpg
| image size     = 190px
| caption        = Theatrical release poster
| director       = W.S. Van Dyke
| producer       = Hunt Stromberg
| based on       =
| screenplay     = Albert Hackett Frances Goodrich
| writer         = Dashiell Hammett (characters, story)
| starring       = William Powell Myrna Loy James Stewart Penny Singleton Elissa Landi
| music          = William Axt
| cinematography = Oliver T. Marsh
| editing        = Robert Kern
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 113 minutes
| language       = English
| country        = United States
| budget         = $673,000 (est.)   
| gross          = $3,165,000 (worldwide est.)  
}}
 The Thin Alan Marshal, and Penny Singleton.

This was actually the fourth pairing for Myrna Loy and William Powell. The two made 14 pictures together, six of them in the Thin Man series. 

==Plot==
  San Francisco on New Years Eve, where Noras stuffy family expect the couple to join them for a formal dinner. Nick is despised by Noras Aunt Katherine, the family matriarch, as his immigrant heritage and experience as a "flat foot" are considered below Nora. The true reason for their invitation is that Noras cousin Selmas neer-do-well husband Robert has been missing. Nick is coerced into a little quiet detective work for the family.

They easily find Robert at a Chinese nightclub, where hes been conducting an affair with Polly, the star performer. Robert tries to extort money from Selmas unrequited love, David Graham (James Stewart): $25,000 and Robert will leave Selma alone permanently. Unknown to Robert, Polly and the nightclubs owner, Dancer, plan to grift the money and dispose of him. After being paid off, and returning home for some clothes, Robert is shot at the stroke of midnight. David finds Selma standing over Robert and hurriedly disposes of her gun. Despite this, the police determine that shes the prime suspect, and her fragile mental state only strengthens the case. Selma insists that she never fired her gun, and Nick is now obliged to investigate and determine the true murderer.
 the previous film, the true murderer is the least likely suspect, betrayed by a trivial slip-up during a final interrogation and denouement featuring all the suspects. The case solved, and once again, traveling by train, Nora reveals to Nick that they are expecting a baby, although Nick has to be prodded into putting the "clues" together and she comments: "And you call yourself a detective."

==Cast==
* William Powell as Nick Charles
* Myrna Loy as Nora Charles Skippy as Asta, their dog
* James Stewart as David Graham
* Elissa Landi as Selma Landis
* Joseph Calleia as "Dancer"
* Jessie Ralph as Katherine Forrest Alan Marshal as Robert Landis
* Teddy Hart as Floyd Casper
* Sam Levene as Lieutenant Abrams
* Penny Singleton (billed Dorothy McNulty) as Polly Byrnes
* William Law as Lum Kee
* George Zucco as Dr. Adolph Kammer
* Paul Fix as Phil Byrnes 
* Eadie Adams as "Singer at Welcome Home Party"

==Production==
The films story was written by Dashiell Hammett, based on his characters Nick and Nora, but not a particular novel or short story. Albert Hackett and Frances Goodrich wrote the screenplay.

==Reception== Oscar in Best Writing, Screenplay. It is often referred to as the best of the Thin Man sequels. 

==References==
 

==External links==
 
*  
*  
*  
*   on Lux Radio Theater: June 17, 1940

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 