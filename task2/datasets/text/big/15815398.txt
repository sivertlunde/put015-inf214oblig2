Dear Zachary: A Letter to a Son About His Father
{{Infobox film
| name = Dear Zachary: A Letter to a Son About His Father
| image = DearZacharyTheatricalPoster.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Kurt Kuenne
| producer = Kurt Kuenne
| writer = Kurt Kuenne
| music = Kurt Kuenne
| cinematography = Kurt Kuenne
| editing = Kurt Kuenne MSNBC Films
| distributor = Oscilloscope Laboratories
| released =  
| runtime = 93 minutes
| country = United States
| language = English
| budget = 
| gross = $18,334 
}}
Dear Zachary: A Letter to a Son About His Father is a 2008 American documentary film conceived and created by Kurt Kuenne. 
 scrapbook for the son who never knew him. As events unfold, the film becomes a sort of true-crime documentary. 

In an interview with MovieWeb, Kuenne says that the documentary began as a project only to be shown to friends and family of Andrew Bagby. But as the events unfolded, Kuenne decided to release the film publicly.  

Kuenne is donating all profits from the film to a scholarship established in the names of Andrew and Zachary Bagby.  

==Plot==

Kurt Kuenne and Andrew Bagby grew up as close friends in the suburbs of San Jose, California, and Bagby frequently appeared in Kuennes home movies. As these movies became more professional in quality in later years, Bagby invested in them with money he had saved up for medical school. While studying in Newfoundland and Labrador|Newfoundland, Canada, Bagby began a relationship with Shirley Turner, a twice-divorced general practitioner thirteen years his senior. Bagbys parents, friends, and associates were uneasy about  the relationship because of what they saw as Turners off-putting behavior. Turner moved to Council Bluffs, Iowa, while Bagby worked as a resident in family practice in Latrobe, Pennsylvania. 
 Keystone State Park. Bagby was found dead the following day, face down, with five gunshot wounds.  When Turner learned she was a suspect in the murder investigation, she fled to St. Johns, Newfoundland. As the legal drama unfolded, Kuenne began collecting footage from his old home movies and interviewed Bagbys parents, David and Kathleen, for a documentary about his life. 
 rendition for a trial in the United States|U.S.. However, the extradition process was repeatedly prolonged by Turners lawyers based on legal technicalities. When a provincial court ruled that enough evidence pointed to Turner as Bagbys killer, she was put in jail and Bagbys parents, David and Kathleen, were awarded custody of Zachary. Meanwhile, Kuenne traveled across the U.S. and England to interview Bagbys friends and extended family. Kuenne also went to Newfoundland and visited Zachary in July 2003.

In jail, Turner wrote to a judge and, contrary to normal legal procedure, received advice on how to appeal her arrest and imprisonment. Turner was later released by a Newfoundland judge, Gale Welsh, who &mdash; despite what the film presents as ample evidence that Turner was psychologically disturbed &mdash; felt she did not pose a threat to society in general. Turner was therefore released on bail and successfully sued for joint custody of Zachary with the Bagbys, although their arrangement was tenuous. The arrangement ended in tragedy when, on August 18, 2003, Turner jumped into the Atlantic Ocean with thirteen-month-old Zachary in a murder-suicide.  David and Kathleen were left dumbfounded and grief-stricken. Kuenne tried to arrange interviews with the prosecutors and judges who facilitated Turners freedom, but was rebuffed.

Distraught over Zacharys death, and outraged at the Canadian legal systems failure to protect the child, David and Kathleen mounted a campaign to reform the countrys bail laws, which they believed had helped allow Turner to kill her child and herself. A panel convened by Newfoundlands Ministry of Justice agreed, releasing a report stating that Zacharys death had been preventable and that the governments handling of Turners case had been inadequate. Turners psychiatrist was found guilty of misconduct for having helped her post bail, and the head of Newfoundlands child welfare agency resigned. David Bagby wrote a best-selling book about his familys ordeal during the saga. Kuenne finished his documentary and dedicated it to the memory of both Bagby and his son; the film ends with the Bagbys and their relatives, friends, and colleagues reflecting on the father and son, as well as the impact that David and Kate had on all of them.

==Release==
The film premiered at the Slamdance Film Festival and was shown at Cinequest Film Festival, South by Southwest, the Hot Docs Canadian International Documentary Festival, the Sarasota Film Festival, the Sidewalk Moving Picture Festival, the Calgary International Film Festival, and the Edmonton International Film Festival, among others, before going into limited theatrical release in the United States, opening in one city at a time in select metropolitan areas. It was broadcast by MSNBC on December 7, 2008 and has been repeated several times since. 

==Critical reception==
Peter Debruge of Variety (magazine)|Variety called the film "a virtuoso feat in editing" and noted, "The way Kuenne presents the material, with an aggressive style that lingers less than a second on most shots, its impossible not to feel emotionally exhausted." 
 The Thin Blue Line, Dear Zachary borrows some narrative dramatic tricks, and they pay off remarkably well. Its hands down one of the most mind-blowing true-crime movies in recent memory, fiction or nonfiction." 
 WGN Radio Chicago.  
  The website Film School Rejects place the film in third place in their 30 Best Films of the Decade list.  The Film Vault included the film on their top 5 good movies you never want to see again.  
==Awards and nominations==
The Chicago Film Critics Association nominated the film for Best Documentary. The Society of Professional Journalists presented it with its Sigma Delta Chi Award for Best Television Documentary (Network), it received the Special Jury and Audience Awards at the Cinequest Film Festival, it was named an Audience Favorite at Hot Docs, it received the Audience Awards at the St. Louis International Film Festival and the Sidewalk Moving Picture Festival, it was named Best Documentary at the Orlando Film Festival and was awarded with the jury award for best international documentary at Docville (Belgium). 

==Changes==
On March 23, 2010, Bill C-464 (also known as Zacharys Bill) was introduced by MP Scott Andrews (Avalon) to the Canadian Parliament.  The goal of Zacharys Bill was to protect children and force "judicial decision makers" to keep the safety of children in mind during bail hearings and in custody disputes, particularly when a child is in the custody of someone who has been charged with a "serious crime".  Seven years after Zacharys death, and over two years after the film inspired MP Andrews to draft Bill C-464, Zacharys Bill was signed into law. 

== See also ==
* Murder of Zachary Turner

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 