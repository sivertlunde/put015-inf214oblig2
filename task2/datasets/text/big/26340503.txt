Naked Massacre
{{Infobox film
| name           = Naked Massacre
| image          =
| image_size     =
| caption        =
| director       = Denis Héroux
| producer       = Peter Fink (producer) Georg M. Reuther (producer)
| writer         = Fred Denger (writer) Denis Héroux (writer) Géza von Radványi (screenplay) Géza von Radványi (story) Clement Woods (writer)
| narrator       =
| starring       = See below
| music          = Voggenreiter Verlag
| cinematography = Heinz Hölscher
| editing        = Yves Langlois
| distributor    =
| released       =
| runtime        = 92 minutes
| country        = West Germany, Canada, France, Italy
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Naked Massacre is a 1976 West German / Canadian / French / Italian film directed by Denis Héroux.

== Plot summary ==
 
Naked Massacre roughly tells the story of mass murderer Richard Speck, set in Northern Ireland.

== Cast ==
*Mathieu Carrière as Cain Adamson
*Debra Berger as Bridget
*Christine Boisson as Christine
*Myriam Boyer as Leila
*Leonora Fani as Jenny
*Ely Galleani as Pam
*Carole Laure as Amy
*Eva Mattes as Catherine
*Andrée Pelletier as Eileen

== DVD Release ==
In 2007, Apprehensive Films released Naked Massacre onto DVD.   

== References ==
 

== External links ==
* 
* 
 

 
 
 
 
 
 
 
 
 


 