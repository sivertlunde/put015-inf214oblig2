Ananda Thandavam (film)
{{Infobox film
| name = Anandha Thandavam
| image = Ananda Thandavam.jpg
| director = Gandhi Krishna|A.R.Gandhi Krishna
| producer = Viswanathan Ravichandran
| writer = Gandhi Krishna|A.R.Gandhi Krishna  Sujatha Rangarajan Rishi
| music = G. V. Prakash Kumar
| cinematography = Jeeva Shankar
| editing        = V. T. Vijayan Aascar Film Pvt. Ltd
| released =  
| runtime =
| country = India
| language = Tamil
| budget =
}}
Ananda Thandavam (  film adaptation of the  ) & Madhumitha (Tamanna Bhatia): their love story captured so many hearts that writer Sujatha, who had initially finished the first novel, started the second part due to high demand from readers. The film was released on 10 April 2009 to poor reviews but very well at commercial

==Plot==
The film deals with the life of Raghu, whose mother dies in childbirth. He grows up with plenty of affection from his father and grows into a quiet, sensible and a bit of a serious person. He is frustrated because he is unemployed, when he meets Madhumitha (Tammana Bhatia); a native character, she plays childish pranks and brings energy into his life.

Her parents also bring joy in Raghus life as they accept who Raghu is and eventually, Raghu finds employment. Everything seems to go well when trouble comes in the form of Radha Krishnan (Rishi (actor)|Rishi). He is a spoilt NRI boy and worst of all, Madhu agrees with her parents opinion and marries Radha. They both leave for the US while Raghu attempts to commit suicide. He escapes it narrowly and with further encouragement from his father and friends, leaves for US to study.

He meets Ratna (Rukmini Vijayakumar) and love slowly begins to blossom between them. One time, Raghu faces his ex-lover Madhumitha and discovers that her husband is a traitor to her and that she is unaware of it. He resolves to tell her the truth but Madhu is blinded against Radhas faults because of respect for him. She later discovers her husband in bed with another woman, but this just makes her situation worse because she had been under abuse.

One day, she escapes from Radha and goes to meet Raghu. Unfortunately for her, Raghu gets engaged to Ratna the very same day, and unable to bear any more sadness, ends up cutting her veins and begging Raghu to take her back. Ratna intervenes and Madhu asks for Raghu, Ratna is disgusted and complains to her family. They come and take Raghu away, leaving Madhu devastated, drunk and bleeding through her veins. Madhu then kills herself through careless driving.

At the airport, everyone is gathered and Madhus parents are collecting the coffin with Madhus body. Radha tries to lie to Madhus parents that he was perfect to her in every way and cannot understand why this happened. In the meantime, Raghu arrives who had also come to see Madhus body gets enraged and charges at him with a revolver finds out that it is locked. He is later soothed by Ratna.

==Cast==
* Siddharth Venugopal as Raghu
* Tamannaah as Madhumitha
* Rukmini Vijayakumar as Ratna  Rishi as Radhakrishnan 
* Ammu
* Amirtha Charlie
* Venki
* Bala Singh
* Kalai Rani
* Madan Bob Kitty as Raghus father
* Five Star Krishna
* Rama Natarajan as Mohanram 
* Ann Marie Seall as Jennifer
* Aditi Sriram 
* Sanchaya Satish 
* Sanjana Satish  Srinath

==Production notes==
AR Gandhi Krishna, former assistant to  , was "cranking the camera". 

==Soundtrack==
The songs were composed by G. V. Prakash Kumar|G. V. Prakash. 
{{Infobox album|  
 Name = Ananda Thandavam |
 Type = soundtrack |
 Artist = G. V. Prakash Kumar | Feature film soundtrack |
 Last album = Kuselan (2008) |
 This album = Ananda Thandavam (film) (2008) |
 Next album = Naan Aval Adhu (2008) |
}}
{| class="wikitable" width="50%"
|-
! Song title !! Singers
|-
| "Poovinai" || Srinivas (singer)|Srinivas, Shreya Ghoshal
|-
| "Pattu Poochi" || Naresh Iyer, Harmony
|-
| "Kallil Aadum" || Benny Dayal, Swetha Mohan
|-
| "Kana Kaangiren" || Nithyashree Mahadevan|Nithyashree, Shubha Mudgal, Vinita
|-
| "Megam Pola" || Shankar Mahadevan
|-
| "Ananda Thandavam theme" || G. V. Prakash Kumar|G.V. Prakash, Naresh Iyer
|-
|}

==Critical Reception==
Sify.com says that the actor "can do little to save the film".    Rediff.com felt that Siddharths performance was one of the weaker aspects of the film noting "though he does his best when hes romancing his beloved, and there are sparks during emotional scenes, he seems to miss his cue a few times, making you wonder if someone with acting chops might not have done a better job." 

==References==
 

==External links==
* Prasad Kovilkar,  
* S.R. Ashok Kumar, , 10 May 2008, "Sujatha’s novel on the big screen"
 

 
 
 
 
 