Code of Scotland Yard
{{Infobox film
| name           = Code of Scotland Yard
| image          = "Code_of_Scotland_Yard"_(1947).jpg
| caption        = French poster by Boris Grinsson George King 
| producer       = George King
| screenplay     = Reginald Long  Katherine Strueby
| based on       = the play by Edward Percy
| starring       = Oskar Homolka  Muriel Pavlow  Derek Farr
| cinematography = Hone Glendinning
| music          = George Melachrino
| editing        = Manuel del Campo
| studio         =  Pennant Pictures
| distribution   = British Lion Films (UK)
| released       = 1947
| runtime        = 91 minutes
| country        = United Kingdom English
| gross = £140,694 (UK) 
}}
 British crime George King and starring Oskar Homolka, Muriel Pavlow, Derek Farr and Irene Handl. It was also known as The Shop at Sly Corner, from the popular stage play of that name by Edward Percy.     It features an appearance by the young Diana Dors.

==Synopsis== French refugee fencing stolen goods, the employee attempts to blackmail the Frenchman, with fatal results. The antique dealer cares only for his concert violinist daughter (Pavlow), and when he sees her future is threatened, he kills the blackmailer.  

==Cast==
*Descius Heiss ...	Oskar Homolka
*Robert Graham ...	Derek Farr
*Margaret Heiss ...	Muriel Pavlow
*Archie Fellowes ...	Kenneth Griffith
*Corder Morris ...	Manning Whiley
*Mrs Catt ...	Kathleen Harrison
*Major Elliot ...	Garry Marsh
*Ruby Towser ...	Irene Handl
*Inspector Robson ...	Johnnie Schofield
*Professor Vanetti ...	Jan Van Loewen
*Mildred ...	Diana Dors
*Woman in Shop ...	Katie Johnson
*Theatre Usher ...	Eliot Makeham

==Critical reception==
*Allmovie wrote, "Oscar Homolka, a Viennese character actor who worked prolifically on both sides of the Atlantic, is the principal attraction."  
*TV Guide called it an " interesting melodrama rich with character, thanks to the excellent performance by Homolka and a uniformly fine British cast."  

==External links==
*  

==References==
 

 

 
 
 
 
 


 
 