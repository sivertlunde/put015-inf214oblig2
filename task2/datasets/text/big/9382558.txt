To the Shores of Tripoli
{{Infobox film
| name           = To the Shores of Tripoli
| image          = To the Shores of Tripoli - 1942 - poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = H. Bruce Humberstone
| producer       = Darryl F. Zanuck
| writer         = Lamar Trotti (screenplay) Steve Fisher (story) John Payne Maureen OHara Randolph Scott Alfred Newman Harry Jackson William V. Skall
| editing        = Allen McNeil
| distributor    = 20th Century Fox
| released       =  
| runtime        = 86 min.
| country        = United States English
| budget         =
| gross          = $2 million 
}}
 1942 Cinema American Technicolor John Payne, Maureen OHara and Randolph Scott. The film was directed by H. Bruce Humberstone and produced by Milton Sperling.

==Plot==
Titled after a lyric in the Marines Hymn, which contains the phrase "...&nbsp;to the shores of Tripoli" (which is, itself, a reference to the Battle of Derne) the film is one of the last of the pre-Pearl Harbor service films. When the film was in post-production the Pearl Harbor attack occurred having the studio shoot a new ending where Payne re-enlists.
 private where he meets his drill instructor Gunnery Sergeant Dixie Smith (Randolph Scott) and falls in love with a Navy nurse, Lieutenant Mary Carter (Maureen OHara). Smith is given a letter from Winters father. Captain Christopher Winters (Minor Watson) writes Smith of his playboy son. Sgt. Smith served in World War I under the elder Winters; Smith affectionately calls Winters "The Skipper". Chris Winters can not understand that Officers and Enlisted Men do not associate under the non-fraternization policy, even if the officer is a woman and the enlisted man is a male.

Chris society girlfriend Helene Hunt (Nancy Kelly) wants Chris to get a cushy civilian job in Washington, D.C. and uses her uncles power and her influence on the base commander, General Gordon. In sequences filmed at the Marine Corps Recruit Depot San Diego, Smith gives the younger Winters an opportunity to demonstrate his leadership potential by drilling his platoon.  To Smiths amusement the Marines mock Chris and perform slapstick antics during the drill as Winters marches them away. As Smith is enjoying himself the platoon marches back and performs close order drill of a high order of perfection. Smith is greatly surprised until he looks over the platoon and notices several Marines have black eyes, chipped teeth and bruises. Chris Winters says, "I was captain of the boxing team at Culver."

Winters is selected for Sea School and on gunnery practice during naval maneuvers he bravely saves Dixie Smiths life when repairing gunnery targets. Chris picks a fight with Smith. However, Smith claimed he struck the first blow, by being busted in rank Smith will save Chris from the Naval Prison. Despite winning the respect of Dixie Smith and his fellow Marines, Chris decides to leave the Marines. But then he hears the news of the Pearl Harbor attack when driving in a car with Helene. His way is blocked by his old platoon marching to a Navy transport ship. Chris Winters runs to Sgt. Dixie Smith to reenlist; Chris enters the ranks that close up as he dresses in his old uniform from his satchel, he tosses away his civilian clothes and is in uniform except for his two-toned shoes. Chriss proud father, (Watson) wounded in World War I, asks his son to "Get a Jap for me".

==Cast== John Payne as Chris Winters, Jr.
* Maureen OHara as Mary Carter
* Randolph Scott as Sgt. Dixie Smith
* Nancy Kelly as Helene Hunt
* William Tracy as Johnny Dent Max Slapsie Maxie Rosenbloom as Okay Jones
* Harry Morgan as Mouthy (as Henry Morgan)
* Edmund MacDonald as Butch Russell Hicks as Maj. Wilson
* Margaret Early as Susie
* Minor Watson as Captain Christopher Winters, Sr.
* Alan Hale Jr. as Tom Hall Richard Lane as Lieutenant

==Production==
The original planned ending was a simple romantic coupling with Maureen OHaras Navy nurse, but after Pearl Harbor, it switched to John Payne signing up for war.

Portions of the film were shot at the Marine Base in San Diego. 

The Marines credit the movie as the biggest single recruitment aid in 1942.  In 1940, before Pearl Harbor, there were only 19,400 Marines; when WWII ended there were 485,052 Marines and this number was subsequently reduced to 77,000 as the USA de-mobilized.
 Battle Cry.

 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 