Hôtel du Paradis
{{Infobox film
| name           = Hôtel du Paradis
| image          = 
| image_size     = 
| caption        = 
| director       = Jana Boková
| producer       = Simon Perry
| writer         = Jana Boková
| starring       = Fernando Rey
| music          = 
| cinematography = Gérard de Battista
| editing        = Yves Deschamps
| distributor    = 
| released       =  
| runtime        = 
| country        = France
| language       = French
| budget         = 
}}

Hôtel du Paradis is a 1986 French drama film directed by Jana Boková. It was screened out of competition at the 1987 Cannes Film Festival.   

==Cast==
* Fernando Rey – Joseph
* Fabrice Luchini – Arthur
*   – Frédérique
* Georges Géret – Dr. Jacob
* Hugues Quester – Maurice
* Marika Rivera – Marika
*   – Sarah
* Raúl Gimenez – Emilio
* Juliet Berto – Prostitute
* Sacha Briquet – Georges
* Lou Castel – Tramp
* Michael Medwin – English producer
* Sheila Kotkin – Sheila
* Aurelle Doazan – Dream Girl
* Max Berto – Max
* Pascal Aubier – Head Waiter
* Alex Joffé
*   – Patric
*   – Lucienne Bayer
*   – Caretaker
*  
*   – Barbara
* Remi Deroche – Harry
* Catherine Mathely – Marianne
* Irene Langer – Irene

==References==
 

==External links==
* 

 
 
 
 
 
 