Dangerous Hours
{{Infobox film
| name           = Dangerous Hours
| image          = Dangerous Hours (1919) - 1.jpg
| caption        = Film still with Lloyd Hughes
| director       = Fred Niblo
| producer       = Thomas H. Ince
| writer         = C. Gardner Sullivan (screenplay)
| based on       =  
| starring       = Lloyd Hughes Barbara Castleton Claire Du Brey
| music          =  George Barnes
| editing        = 
| studio         = Famous Players-Lasky/Artcraft
| distributor    = Paramount Pictures
| released       =  
| runtime        = 
| country        = United States  Silent English intertitles
| budget         = 
}}
 silent drama film directed by Fred Niblo. Prints of the film survive in the UCLA Film and Television Archive.    It premiered in February 1920. 

The film was based on a short story "A Prodigal in Utopia" published in the Saturday Evening Post. The films working title was Americanism (Versus Bolshevism), the title of a pamphlet published by Ole Hanson, the mayor of Seattle who claimed to have broken the Seattle General Strike in 1919.

==Plot==
The film tells the story of an attempted Russian infiltration of American industry, and includes a depiction of the "nationalization of women" under Bolsheviks|Bolshevism, including "extras on horseback, rounding up women, throwing them into dungeons and beating them."  

College graduate John King (Hughes) is sympathetic to the left in a general way. Then he is seduced, both romantically and politically, by Sophia Guerni (Du Brey), a female agitator. Her superior is the Bolshevik Boris Blotchi (Richardson), who has a "wild dream of planting the scarlet seed of terrorism in American soil." Brownlow, 263  Sofia and Boris turn their attention to the Weston shipyards that are managed by Johns childhood sweetheart. The workers have valid grievances, but the Bolsheviks set out to manipulate the situation. They are "the dangerous element following in the wake of labor as riffraff and ghouls follow an army."  When they threaten Johns earlier love, he has an epiphany and renounces revolutionary doctrine. 

==Cast==
* Lloyd Hughes as John King
* Barbara Castleton as May Weston
* Claire Du Brey as Sophia Guerni Jack Richardson as Boris Blotchi
* Walt Whitman as Dr. King
* Louis Morrison as Michael Regan (as Lew Morrison)
* Gordon Mullen as Andrew Felton

==Response to the film==
A reviewer in the movie magazine Picture Play protested the films stew of radical beliefs and strategies: "Please, oh please, look up the meaning of the words bolshevik, and soviet. Neither of them mean   anarchist, scoundrel or murderer &ndash; really they dont!" 

==See also== Red Scare of 1919-20

==References==
 

==Bibliography==
* Kevin Brownlow, The Parades Gone By... (Berkeley: University of California Press, 1968)
* Patricia King Hanson and Alan Gevinson, eds., The American Film Institute Catalog of Motion Pictures Produced in the United States, vol.F-1 Feature Films, 1911-1920 (Berkeley: University of California Press, 1988), p. 187

==External links==
* 
* 

 

 
 
 
 
 
 
 