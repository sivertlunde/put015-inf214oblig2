What Women Want (2011 film)
 
{{Infobox film
| name           = What Women Want
| image          = What Women Want.jpg
| alt            = 
| caption        = 
| film name = {{Film name| traditional    = 我知女人心
| simplified     = 我知女人心
| pinyin         = Wǒ Zhī Nǚ Rén Xīn
| jyutping       = Ngo2 Zi1 Neoi5 Jan2 Sam1}}
| director       = Chen Daming
| producer       = Chen Daming Don Yu
| screenplay     = Chen Daming
| starring       = Andy Lau Gong Li
| music          = Christoper OYoung
| cinematography = Max Wang
| editing        = Nelson Quan Emperor Motion Focus Film Limited Beijing Polybona Film Distribution Co. Ltd.
| released       =  
| runtime        = 
| country        = China
| language       = Mandarin   Cantonese
| budget         = US$5 million
| gross          = US$11,831,362 
}} 2000 Cinema American film What Women Want. The film stars Andy Lau and Gong Li. It was released in China on the 3rd of February 2011, on the Chinese New Year.

The plot is strongly based on the original Hollywood version, with some adaptations to 2010s China. The plot takes place mostly in an advertising company in Beijing, in which slick Andy Lau gets acquainted with his new talented competition, played by Gong Li. He is helped when he gets the ability to hear womens thoughts due to a freak accident.

==Cast==
* Andy Lau as Sun Zigang
* Gong Li as Li Yilong
* Yuan Li as Yanni
* Banny Chen as Xiao Fei
* Hu Jing as Zhao Hung
* JuJu as Xiao Wu
* Li Chengru as CEO Dong
* Anya Wu as Dongs wife
* Osric Chau as Chen Erdong
* Wang Deshun as Sun Meisheng
* Chen Daming as Young Sun Meisheng
* Mavis Pan as Pans secretary
* Russell Wong as Peter
* Kelly Hu as Girl in Lotto Commercial
* Ping Wong as Foo Ping Pong

==See also==
* Andy Lau filmography

==References==
 

==External links==
*  
*   at Hong Kong Cinemagic
*  
*   at  

 
 
 
 
 
 
 
 
 


 
 