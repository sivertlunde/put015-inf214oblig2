Apollo 13 (film)
{{Infobox film
| name           = Apollo 13
| image          = Apollo thirteen movie.jpg
| caption        = Theatrical release poster
| alt            = A thin light-gray crescent Moon stretches diagonally from lower left to upper right against a black background, with a blue and white crescent Earth in the far distance. In front of the portion of the moon that is in shadow on the left appears a small image of the Apollo 13 Command/Service module joined to the Lunar Module, with vapor streaming from a hole in the side of the Service Module — the words "Houston, we have a problem" appear directly above the craft in white lower case lettering. The names of the principal actors appear in white lettering at the top of the image, and the title APOLLO 13 in block white upper-case letters appears at the lower right.
| director       = Ron Howard
| producer       = Brian Grazer
| screenplay     = William Broyles, Jr. Al Reinert
| based on       =  
| starring       = Tom Hanks Kevin Bacon Bill Paxton Gary Sinise Ed Harris 
| music          = James Horner
| cinematography = Dean Cundey Mike Hill
| studio         = Imagine Entertainment
| distributor    = Universal Pictures
| released       =  
| runtime        = 140 minutes
| country        = United States
| language       = English
| budget         = $52 million   
| gross          = $355.2 million 
}}

Apollo 13 is a 1995 American   by astronaut Jim Lovell and Jeffrey Kluger.

The film depicts astronauts Lovell, Jack Swigert and Fred Haise aboard Apollo 13 for Americas third Moon landing mission. En route, an on-board explosion deprives their spacecraft of most of its oxygen supply and electric power, forcing NASAs flight controllers to abort the Moon landing, and turning the mission into a struggle to get the three men home safely.

Howard went to great lengths to create a technically accurate movie, employing NASAs technical assistance in astronaut and flight controller training for his cast, and even obtaining permission to film scenes aboard a reduced gravity aircraft for realistic depiction of the "weightlessness" experienced by the astronauts in space.
 Best Film Best Sound).    In total, the film grossed over $355 million worldwide during its theatrical releases.

==Plot==

On July 20, 1969, veteran astronaut Jim Lovell hosts a party for other astronauts and their families, who watch on television as Neil Armstrong takes his first steps on the Moon during the Apollo 11 mission. After the party, Lovell, who orbited the Moon on Apollo 8, tells his wife Marilyn that he intends to return to the Moon and walk on its surface.

On October 30, 1969, while giving a VIP tour of NASAs Vehicle Assembly Building, Lovell is informed by his boss Deke Slayton that he and his crew will fly the Apollo 13 mission instead of Apollo 14. Lovell, Ken Mattingly, and Fred Haise begin training for their new mission. Days before launch, it is discovered that Mattingly was exposed to rubella|measles, and the flight surgeon demands his replacement with Mattinglys backup, Jack Swigert, as a safety precaution. Lovell resists breaking up his team, but Slayton gives him the ultimatum of either being relieved of command and bumped to a later mission, or accepting a replacement. Lovell chooses the latter.

As the launch date approaches, Marilyns fears for her husbands safety manifest in nightmares, but she goes to Cape Kennedy the night before launch, to see him off despite her misgivings.
 Gene Kranz pulls it away from the spent stage.

Three days into the mission, the crew send a live television transmission from Odyssey, but the networks, believing the public now regards lunar missions as routine, decline to carry the broadcast live. Swigert is told to perform a standard housekeeping procedure of stirring the two liquid oxygen tanks in the Service Module. When he flips the switch, one tank explodes, emptying its contents into space and sending the craft tumbling. The other tank is soon found to be leaking, prompting Mission Control to abort the Moon landing, and forcing Lovell and Haise to hurriedly power up Aquarius as a "lifeboat" for the return home, while Swigert shuts down Odyssey before its battery power runs out. On Earth, Kranz rallies his team to do what is necessary to get the astronauts home safely, declaring "failure is not an option." Controller John Aaron recruits Mattingly to help him figure out how to restart Odyssey for the final return to Earth.

As Swigert and Haise watch the Moon passing beneath them, Lovell laments his lost chance of walking on its surface, then turns their attention to the task of getting home. With Aquarius running on minimum systems to conserve power, the crew is soon subjected to freezing conditions. Swigert suspects Mission Control is unable to get them home and is withholding this from them. In a fit of rage, Haise blames Swigerts inexperience for the accident; the ensuing argument is quickly squelched by Lovell. When the carbon dioxide exhaled by the astronauts reaches the Lunar Modules filter capacity and approaches dangerous levels, an engineering team quickly invents a way to make the Command Modules square filters work in the Lunar Modules round receptacles. With the guidance systems on Aquarius shut down, and despite Haises fever and miserable living conditions, the crew succeeds in making a difficult but vital course correction by manually igniting the Lunar Modules engine.
 heat shield USS Iwo Jima.

As the astronauts are given a heros welcome on deck, Lovells narration describes the events that follow their return from space&mdash;including the investigation into the explosion, and the subsequent careers and lives of Haise, Swigert, Mattingly and Kranz&mdash;and ends with him wondering when mankind will return to the Moon.

==Cast==
{{Multiple image
| direction = vertical
| width     = 175
| footer    = Top to bottom: Hanks, Bacon and Paxton, who portray astronauts Lovell, Swigert and Haise respectively.
| image1    = TomHanksJan2009.jpg
| alt1      = 
| caption1  =
| image2    = Kevin Bacon SDCC 2014.jpg
| alt2      = 
| caption2  =
| image3    = Bill Paxton by Gage Skidmore.jpg
| alt3      = 
| caption3  = 
}}

* Tom Hanks as Apollo 13 Commander Jim Lovell. Jim Lovell stated that before the book was even written, the rights were being shopped to potential buyers    and that his first reaction was that actor Kevin Costner would be a good choice to play him.     However, by the time Howard acquired the directors position, Costners name never came up in serious discussion, and Hanks had already been interested in doing a film based on Apollo 13. When Hanks representative informed him that there was a script being passed around, he had the script sent to him.    John Travolta was initially offered the role of Lovell, but declined.   
* Kevin Bacon as Apollo 13 backup Command Module Pilot Jack Swigert Lunar Module Pilot Fred Haise Command Module Pilot Ken Mattingly. Sinise was invited by Howard to read for any of the characters, and chose Mattingly.    Flight Director Gene Kranz. Harris described the film as "cramming for a final exam." Harris described Gene Kranz as "corny and like a dinosaur", but was respected by the crew.   
* Kathleen Quinlan as Lovells wife Marilyn Chris Ellis as Director of Flight Crew Operations Deke Slayton Chris Kraft
* Marc McClure as Black Team Flight Director Glynn Lunney EECOM (Electrical, Sy Liebergot Ray McKinnon FIDO (Flight Dynamics Officer) Jerry Woodfill. FAO (Flight Activities Officer)
* Loren Dean as EECOM John Aaron composite of protocol officer Bob McMurrey, who relayed the request for permission to erect a TV tower to Marilyn Lovell, and an unnamed OPA staffer who made the request on the phone, to whom she personally denied it as Quinlan did to "Henry" in the film. "Henry" is also seen performing other OPA functions, such as conducting a press conference. {{Cite book
  | last = Kluger | first = Jeffrey | authorlink = Jeffrey Kluger |author2=Jim Lovell
  | title = Lost Moon: The Perilous Voyage of Apollo 13
  | publisher = Pocket Books   | location = New York | edition = First Pocket Books printing
  | date = July 1995
  | pages = 118, 209–210, 387
  | isbn = 0-671-53464-5 }}
  David Andrews as Apollo 12 Commander Pete Conrad
* Christian Clemenson as Flight Surgeon Dr. Charles Berry John Young CAPCOM (Capsule Communicator) 1
* Ned Vaughn as CAPCOM 2
* Tracy Reiner as Haises then-wife Mary
* Mary Kate Schellhardt as Lovells older daughter Barbara
* Max Elliott Slade as Lovells older son James (Jay), who attended military school at the time of the flight
* Emily Ann Lloyd as Lovells younger daughter Susan
* Miko Hughes as Lovells younger son Jeffrey
* Thom Barry as an orderly at Blanchs retirement home USS Iwo Jima; Howard had intended to make him an admiral, but Lovell himself, having retired as a Captain, chose to appear in his actual rank. Horror film director Roger Corman, a mentor of Howard, appears as a congressman being given a VIP tour by Lovell of the Vehicle Assembly Building, as it had become something of a tradition for Corman to make a cameo appearance in his protégés films.       The real Marilyn Lovell appeared among the spectators during the launch sequence.  CBS News anchor Walter Cronkite appears in archive news footage and can be heard in newly recorded announcements, some of which he edited himself to sound more authentic. 

In addition to his brother, Clint Howard, several other members of Ron Howards family appear in the movie:
* Rance Howard (his father) appears as the Lovell family minister.
* Jean Speegle Howard (his mother) appears as Lovells mother Blanch.
* Cheryl Howard (his wife) and Bryce Dallas Howard (his daughter) appear as uncredited background performers in the scene where the astronauts wave goodbye to their families. 
Brad Pitt was offered a role in the film, but turned it down to star in Seven (film)|Se7en.     Reportedly, the real Pete Conrad expressed interest in appearing in the film. 

Jeffrey Kluger appears as a television reporter. 

== Production ==

=== Pre-production and props ===

While planning the film, director Ron Howard decided that every shot of the film would be original and that no mission footage would be used.    The spacecraft interiors were constructed by the Kansas Cosmosphere and Space Centers Space Works, who also restored the Apollo 13 Command Module. Two individual Lunar Modules and two Command Modules were constructed for filming. While each was a replica, composed of some of the original Apollo materials, they were built so that different sections were removable, which enabled filming to take place inside the capsules. Space Works also built modified Command and Lunar Modules for filming inside a Boeing KC-135 reduced gravity aircraft, and the pressure suits worn by the actors, which are exact reproductions of those worn by the Apollo astronauts, right down to the detail of being airtight. When the actors put the suits on with their helmets locked in place, air was pumped into the suits to cool them down and allow them to breathe, exactly as in launch preparations for the real Apollo missions. 
 Mission Control USS Iwo USS New Orleans, was used as the recovery ship instead. 

 
Howard anticipated difficulty in portraying weightlessness in a realistic manner. He discussed this with Steven Spielberg, who suggested using a KC-135 airplane, which can be flown in such a way as to create about 23 seconds of weightlessness, a method NASA has always used to train its astronauts for space flight. Howard obtained NASAs permission and assistance in filming in the realistic conditions aboard multiple KC-135 flights.   

=== Cast training and filming ===

To prepare for their roles in the film, Hanks, Paxton, and Bacon all attended the U.S. Space Camp in Huntsville, Alabama. While there, astronauts Jim Lovell and David Scott, commander of Apollo 15, did actual training exercises with the actors inside a simulated Command Module and Lunar Module. The actors were also taught about each of the 500 buttons, toggles, and switches used to operate the spacecraft. 

The actors then traveled to Johnson Space Center in Houston where they flew in NASAs KC-135 reduced gravity aircraft to simulate weightlessness in outer space. While in the KC-135, filming took place in bursts of 25 seconds, the length of each period of weightlessness that the plane could produce. The filmmakers eventually flew 612 parabolas which added up to a total of three hours and 54 minutes of weightlessness. Parts of the Command Module, Lunar Module and the tunnel that connected them were built by production designer Michael Corenblith, art directors David J. Bomba and Bruce Alan Miller and their crew to fit inside the KC-135. Filming in such an environment, while never done before for a film, was a tremendous time saver. In the KC-135, the actors moved wherever they wanted, surrounded by floating props; the camera and cameraman were weightless so filming could take place on any axis from which a shot could be set up. 

In Los Angeles, Ed Harris and all the actors portraying flight controllers enrolled in a Flight Controller School led by Gerry Griffin, an Apollo 13 flight director, and flight controller Jerry Bostick. The actors studied audiotapes from the mission, reviewed hundreds of pages of NASA transcripts and attended a crash course in physics.      Astronaut Dave Scott was impressed with their efforts, stating that each actor was determined to make every scene technically correct, word for word.   

== Soundtrack ==

{{Infobox album| 
| Name        = Apollo 13: Music From The Motion Picture
| Type        = Soundtrack
| Artist      = James Horner
| Cover       = 
| Released    = 27 June 1995
| Recorded    =
| Genre       = Soundtrack
| Length      = 77:41 MCA
| Producer    = 
| Last album  =
| This album  =
| Next album  =
}}

{{Album ratings
| rev1      = AllMusic
| rev1Score =   
| rev2      = Filmtracks.com
| rev2Score =   
| rev3      = SoundtrackNet
| rev3Score =   
| rev4      = Tracksounds
| rev4Score =   
}} Best Original Score. 

{{Track listing
| collapsed =no
| headline = Apollo 13: Original Motion Picture Soundtrack
| all_music = James Horner, except where noted
| title1 = Main Title
| length1  = 1:32
| title2 = One Small Step
| length2  = 0:42
| title3 = Night Train
| length3  = 3:27
| note3 = performed by James Brown
| title4 = Groovin
| length4  = 2:26
| note4 = performed by The Young Rascals
| title5 = Somebody to Love
| length5  = 2:55
| note5 = performed by Jefferson Airplane
| title6 = I Can See for Miles
| length6  = 4:09
| note6 = performed by The Who
| title7 = Purple Haze
| length7  = 2:48
| note7 = performed by The Jimi Hendrix Experience
| title8 = Launch Control
| length8  = 3:28
| title9 = All Systems Go/The Launch
| length9  = 6:39
| title10 = Welcome to Apollo 13
| length10 = 0:38
| title11 = Spirit in the Sky
| length11 = 3:50
| note11 =  performed by Norman Greenbaum
| title12 = House Cleaning/Houston, We Have a Problem
| length12 = 1:34
| title13 = Master Alarm
| length13 = 2:54
| title14 = Whats Going On?
| length14 = 0:34
| title15 = Into the L.E.M.
| length15 = 3:43
| title16 = Out of Time/Shut Her Down
| length16 = 2:20
| title17 = The Darkside of the Moon
| length17 = 5:09
| note17 = performed by Annie Lennox
| title18 = Failure is Not an Option
| length18 = 1:18
| title19 = Honky Tonkin
| length19 = 2:42
| note19 = performed by Hank Williams
| title20 = Blue Moon
| length20 = 4:09
| note20 = performed by The Mavericks
| title21 = Waiting for Disaster/A Privilege
| length21 = 0:43
| title22 = Re-Entry & Splashdown
| length22 = 9:05
| title23 = End Titles
| length23 = 5:34
| note23 = performed by Annie Lennox
}}

== Release ==

The film was released on 30 June 1995 in North America and on 22 September 1995 in the UK.
 IMAX DMR technology. 

=== Box office performance ===

The film was a box office success, gaining $355,237,933 worldwide.    The films widest release was 2,347 theaters.  US gross of $25,353,380, which made up 14.7% of the total US gross. 

{| class="wikitable" border="1"
|-
|+ Apollo 13 box office revenue
|-
! Source !! Gross (United States dollar|USD) !! % Total !! All time rank (unadjusted)
|-
| US
| $173,837,933 
| 48.9%
| 126 
|-
| Non-US
| $181,400,000 
| 51.1%
| N/A
|-
| Worldwide
|  $355,237,933 
| 100.0%
| 140 
|}

===Reception=== normalized 0–100 rating to reviews from mainstream critics, calculated an average score of 77 based on 22 reviews.   
 Rolling Stone praised the film and wrote: "Howard lays off the manipulation to tell the true story of the near-fatal 1970 Apollo 13 mission in painstaking and lively detail. Its easily Howards best film."  Movie Room Reviews said "This film is arguably one of the most dramatic and horrendous spaceflight stories ever told." 
 Quiz Show, Apollo 13 beautifully evokes recent history in ways that resonate strongly today. Cleverly nostalgic in its visual style (Rita Ryacks costumes are especially right), it harks back to movie making without phony heroics and to the strong spirit of community that enveloped the astronauts and their families. Amazingly, this film manages to seem refreshingly honest while still conforming to the three-act dramatic format of a standard Hollywood hit. It is far and away the best thing Mr. Howard has done (and Far and Away was one of the other kind)."  The academic critic Raymond Malewitz focuses on the DIY aspects of the "mailbox" filtration system to illustrate the emergence of an unlikely hero in late twentieth-century American culture—"the creative, improvisational, but restrained thinker—who replaces the older prodigal cowboy heroes of American mythology and provides the country a better, more frugal example of an appropriate husband." 

Ron Howard stated that, after the first test preview of the film, one of the comment cards indicated "total disdain"; the audience member had written that it was a "typical Hollywood" ending and that the crew would never have survived.  Marilyn Lovell praised Quinlans portrayal of her, stating she felt she could feel what Quinlans character was going through, and remembered how she felt in her mind.   

===Home media===

A 10th-anniversary  . 

In 2006, Apollo 13 was released on HD DVD; on 13 April 2010, it was released on Blu-ray disc as the 15th anniversary edition, on the 40th anniversary of the Apollo 13 accident (Central Standard Time).   

==Accolades==

{| class="wikitable" style="width:99%;"
|-
! Year !! Award !! Category !! Recipient!! Result !! Ref.
|-
|rowspan="34"| 1996 Academy Awards (1996) Best Film Editing Mike Hill Daniel Hanley
| 
| rowspan="9" style="text-align:center;" |  
|-  Best Sound Steve Pederson, David MacMillan
|  
|-  Best Actor in a Supporting Role
| Ed Harris (lost to Kevin Spacey in Usual Suspects)
|  
|-  Best Actress in a Supporting Role
| Kathleen Quinlan (lost to Mira Sorvino in Mighty Aphrodite)
|  
|-  Best Art Direction
| Michael Corenblith (art director), Merideth Boswell (set decorator) (lost to Restoration (1995 film)|Restoration)
|  
|-  Best Original Dramatic Score
| James Horner (lost to Il Postino)
|  
|-  Best Picture
| Brian Grazer (lost to Braveheart)
|  
|-  Best Visual Effects Matt Sweeney (lost to Babe (film)|Babe)
|  
|-  Best Adapted Screenplay  Sense & Sensibility)
| 
|- American Cinema Editors (Eddies)
|  Best Edited Feature Film Mike Hill, Daniel P. Hanley
| 
| rowspan="1" style="text-align:center;"|
|- 
|rowspan="1"| American Society of Cinematographers
|  Outstanding Achievement in Cinematography in Theatrical Releases
| Dean Cundey
| 
| rowspan="1" style="text-align:center;"|
|-  BAFTA Film Awards
|  Best Production Design
| Michael Corenblith
| 
| rowspan="5" style="text-align:center;"|
|- 
|  Outstanding Achievement in Special Visual Effects
| Robert Legato, Michael Kanfer, Matt Sweeney, Leslie Ekker
| 
|- 
| Best Cinematography
| Dean Cundey
| 
|- 
| Best Editing
| Mike Hill, Daniel Hanley
| 
|- 
| Best Sound David MacMillan, Rick Dior, Scott Millan, Steve Pederson
| 
|-  Casting Society of America| Casting Society of America (Artios)
|  Best Casting for Feature Film, Drama
|  Jane Jenkins, Janet Hirshenson
| 
| rowspan="1" style="text-align:center;"|
|-  Chicago Film Critics Association| Chicago Film Critics Association Awards
|  Best Picture
| Apollo 13
| 
| rowspan="1" style="text-align:center;"|
|-  Directors Guild of America
| Outstanding Directorial Achievement in Motion Pictures 
| Ron Howard, Carl Clifford, Aldric LaAuli Porter, Jane Paul
| 
| rowspan="1" style="text-align:center;"|
|- Golden Globe Awards Best Supporting Actor – Motion Picture
|  Ed Harris as Gene Kranz
| 
| rowspan="4" style="text-align:center;"|
|-  Best Supporting Actress – Motion Picture 
| Kathleen Quinlan as Marilyn Lovell
| 
|-  Best Director – Motion Picture 
| Ron Howard
| 
|-  Best Motion Picture – Drama
| Apollo 13
| 
|- 
|rowspan="1"| Heartland Film Festival
| Studio Crystal Heart Award 
| Jeffrey Kluger
| 
| rowspan="1" style="text-align:center;"|
|-  Hugo Awards Best Dramatic Presentation
| Apollo 13
| 
| rowspan="1" style="text-align:center;"|
|-  MTV Movie Awards
| Best Male Performance
| Tom Hanks as Jim Lovell
| 
| rowspan="2" style="text-align:center;"|
|-
|  Best Movie
| Apollo 13
| 
|- Producers Guild PGA Awards
| Motion Picture Producer of the Year Award Brian Grazer, Todd Hallowell
| 
| rowspan="1" style="text-align:center;"|
|-
|rowspan="1"|  Saturn Awards Best Action / Adventure / Thriller Film
| Apollo 13
| 
| rowspan="1" style="text-align:center;"| 
|-  Screen Actors Guild Awards
| Outstanding Performance by a Male Actor in a Supporting Role  Ed Harris as Gene Kranz
| 
| rowspan="2" style="text-align:center;"|
|-
| Outstanding Performance by a Cast
| Kevin Bacon, Tom Hanks, Ed Harris, Bill Paxton, Kathleen Quinlan and Gary Sinise
| 
|- Space Foundation| Space Foundations Douglas S. Morrow Public Outreach Award
| Best Family Feature – Drama
|Apollo 13
| 
| rowspan="1" style="text-align:center;" | 
|- Writers Guild of America Awards Best Screenplay Adapted from Another Medium  William Broyles Jr., Al Reinert
| 
| rowspan="1" style="text-align:center;"|
|- Young Artist Awards
| Best Family Feature – Drama
|Apollo 13
| 
| rowspan="1" style="text-align:center;"|
|-
| rowspan="1"| 2001
| American Film Institute
| AFIs 100 Years... 100 Thrills
| Apollo 13
|  
| rowspan="1" style="text-align:center;"|
|-
| 2005
| American Film Institute
| AFIs 100 Years... 100 Movie Quotes
| "Houston, we have a problem." (#50)
| 
| style="text-align:center;" |   
|- 
|rowspan="1"| 2006
| American Film Institute
|  AFIs 100 Years... 100 Cheers
|Apollo 13 (#12)
| 
| rowspan="9" style="text-align:center;" |   
|- 
|}

== Technical and historical accuracy ==
 
The film depicts the crew hearing a bang quickly after Swigert followed directions from mission control to stir the oxygen and hydrogen tanks. In reality, the crew heard the bang 93 seconds later.  , Apollo by the Numbers: A Statistical Reference, NASA History Series, Office of Policy and Plans, Richard W. Orloff, Sept. 2004. See "Oxygen tank #2 fans on. Stabilization control system electrical disturbance indicated a power transient. 055:53:20." 

The dialogue between ground control and the astronauts was taken nearly verbatim from transcripts and recordings, with the exception of one of the taglines of the film, "Houston, we have a problem." (This quote was voted #50 on the list "AFIs 100 Years... 100 Movie Quotes".) According to the mission transcript, the actual words uttered by Jack Swigert were "I believe weve had a problem here" (talking over Haise, who had started "Okay, Houston"). Ground control responded by saying "This is Houston, say again please." Jim Lovell then repeated, "Houston, weve had a problem." 

One other incorrect dialogue is after the re-entry blackout. In the movie, Tom Hanks (as Lovell) says "Hello Houston... this is Odyssey... its good to see you again." In the actual re-entry, the Command Module was finally acquired by a Sikorsky SH-3D Sea King recovery aircraft which then relayed communications to Mission Control. Capcom and fellow astronaut Joe Kerwin (not Mattingly, who serves as Capcom in this scene in the movie) then made a call to the spacecraft "Odyssey, Houston standing by. Over." Jack Swigert, not Lovell, replied "Okay, Joe," and unlike in the movie, this was well before the parachutes deployed; the celebrations depicted at Mission Control were triggered by visual confirmation of their deployment. 
 Flight Dynamics Officer Jerry Bostick:
:"As far as the expression Failure is not an option, you are correct that Kranz never used that term. In preparation for the movie, the script writers, Al Reinart and Bill Broyles, came down to Clear Lake to interview me on What are the people in Mission Control really like? One of their questions was Werent there times when everybody, or at least a few people, just panicked? My answer was No, when bad things happened, we just calmly laid out all the options, and failure was not one of them. We never panicked, and we never gave up on finding a solution. I immediately sensed that Bill Broyles wanted to leave and assumed that he was bored with the interview. Only months later did I learn that when they got in their car to leave, he started screaming, Thats it! Thats the tag line for the whole movie, Failure is not an option. Now we just have to figure out who to have say it. Of course, they gave it to the Kranz character, and the rest is history." 
 DVD commentary track, recorded by Jim and Marilyn Lovell and included with both DVD versions,    mentions several inaccuracies included in the film, all done for reasons of artistic license:
 
* In the film, Mattingly plays a key role in solving a power consumption problem that Apollo 13 was faced with as it approached re-entry. Lovell points out in his commentary that Mattingly was a composite of several astronauts and engineers—including Charles Duke (whose rubella led to Mattinglys grounding)—all of whom played a role in solving that problem. 
* When Jack Swigert is getting ready to dock with the LM, a concerned NASA technician says: "If Swigert cant dock this thing, we dont have a mission." Lovell and Haise also seem worried. In his DVD commentary, the real Jim Lovell says that if Swigert had been unable to dock with the LM, he or Haise could have done it. He also says that Swigert was a well-trained Command Module pilot and that no one was really worried about whether he was up to the job,  but he admitted that it made a nice sub-plot for the film. What Lovell and Haise were really worried about was the rendezvous with Swigert as they left the Moon.  transmission of disease, depicted a tradition not begun until the Space Shuttle program. 
*The film depicts Marilyn Lovell dropping her wedding ring down a shower drain. According to Jim Lovell, this did occur,  but the drain trap caught the ring and his wife was able to retrieve it.   Lovell has also confirmed that the scene in which his wife had a nightmare about him being "sucked through an open door of a spacecraft into outer space" also occurred, though he believes the nightmare was prompted by her seeing a scene in Marooned (film)|Marooned, a 1969 film they saw three months before Apollo 13 blasted off.   

==See also== From the Earth to the Moon, a docudrama mini-series based around the Apollo missions.
* Gravity (film)|Gravity, a 2013 film about astronauts escaping from orbit.
* Marooned (film)|Marooned, a 1969 film directed by John Sturges, about astronauts marooned in an Apollo Command/Service Module.
 

== References ==
 
 

== External links ==
 
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 