Geetha (film)
{{Infobox film
| name = Geetha
| image = Kannada film Geetha poster.jpg
| caption = Film poster
| director = Shankar Nag
| producer = Pramod Narayan
| story = Arundathi Nag Shankar Nag
| screenplay = Arundathi Nag Shankar Nag
| starring = Shankar Nag Akshata Rao
| music = Ilayaraja
| cinematography = B. C. Gowrishankar
| editing = P. Bhakthavathsalam
| studio = Pushpa Production
| distributor = Padmashri Pictures Kushtagi Films
| released =  
| runtime = 125 minutes
| country = India Kannada
| budget = 
| gross = 
}}
 romance drama film directed by and starring Shankar Nag and Akshata Rao in the lead roles. The supporting cast features K. S. Ashwath, Ramesh Bhat and Gayathri.  Ilayaraaja composed the soundtrack and the background score for the film that and was declared a blockbuster musical hit. The track "Santhoshakke" from the film became widely popular in Karnataka and is still performed for in concerts and other events.  

==Plot== charity concert in her college. Sanjay (Shankar Nag), a singer, performs at the show singing the track "Santhoshakke". Floored by performance and his good luck, she pursues him. A university champion in badminton, she invites him to her club to play, with an intention to get closer to him and succeeds. However, Geetha suffers from an illness, the symptoms of which is seen in her holding her neck tight and screaming. Her father Srinivas (K. S. Ashwath) suffers from a heart ailment. Following another of Sanjays concert, she confesses her love to him. He, however fails to remember her every time, and eventually, falls in love with her.

On diagnosis, it is revealed to Geethas mother, Dr. Mukta (Sowcar Janaki) that she suffers from blood cancer, which is not revealed to her father and her. Eager to learn to drive a car, Geetha pesters Sanjay. Sanjay flusters every time she asks him, and finally reveals to her about his ex-girlfriend Sunanda, who succumbed as a brake failed vehicle she was driving exploded. Deciding to get married, Sanjay speaks to Geethas mother, when she reveals to him of the latters illness. The illness takes a toll on Geethas eyes and loses her eyesight among other visible bodily wounds.
Sanjay, with the help of Dr. Rudrappa (Lohithashwa) gets an Interferon drug, an alleged cure of cancer, imported from California. He collects it from the airport and Geetha breathes her last by the time he arrives at her house with the drug.

==Cast==
* Shankar Nag as Sanjay
* Akshata Rao as Geetha
* K. S. Ashwath as Srinivas
* Sowcar Janaki as Mukta
* Ramesh Bhat as Satish
* Gayathri
* Shyamala
* Shivaram as Linganna
* Mandeep Roy as Hanumantha
* Tiger Prabhakar as an astrologer
* Lohithaswa as Doctor Rudrappa
* Hanumanthachar
* Kunigal Ramanath
* Baby Sunali

==Soundtrack==
{{Infobox album
| Name        = Geetha
| Type        = Soundtrack
| Artist      = Ilayaraja
| Cover       = 
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 1981
| Recorded    =  Feature film soundtrack
| Length      = 26:47
| Label       = Sangeetha Music
| Producer    = 
| Last album  = Chinnari Chitti Babu (1981)
| This album  = Geetha (1981)
| Next album  = Janma Janmada Anubandha (1981)
}}
 Hindi film Cheeni Kum. "Nanna Jeeva" was adapted as "Devan Thandha Veenai" for Unnai Naan Sandhithen. "Kelade" was adapted as "Devathai Ilam Devi" for Aayiram Nilave Vaa.

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 26:47
| lyrics_credits = yes
| title1 = Geetha
| lyrics1 = Chi. Udaya Shankar
| extra1 = S. P. Balasubrahmanyam
| length1 = 4:11
| title2 = Santhoshakke
| lyrics2 = Chi. Udaya Shankar
| extra2 = S. P. Balasubrahmanyam
| length2 = 4:16
| title3 = Kelade Nimageega
| lyrics3 = Chi. Udaya Shankar
| extra3 = S. P. Balasubrahmanyam
| length3 = 5:11
| title4 = Yene Kelu
| lyrics4 = Chi. Udaya Shankar
| extra4 = S. P. Balasubrahmanyam, S. Janaki
| length4 = 4:36
| title5 = Nanna Jeeva
| lyrics5 = Chi. Udaya Shankar
| extra5 = S. P. Balasubrahmanyam, S. Janaki
| length5 = 4:10
| title6 = Jotheyali
| lyrics6 = Chi. Udaya Shankar
| extra6 = S. P. Balasubrahmanyam, S. Janaki
| length6 = 4:23
}}

==References==
 

 
 
 
 
 
 