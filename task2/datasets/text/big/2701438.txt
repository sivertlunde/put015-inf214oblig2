Look Both Ways
{{Infobox film
| name           = Look Both Ways
| image          = Look Both Ways Australian Poster.png
| caption        = Promotional poster
| director       = Sarah Watt
| producer       = Andrew Myer Barbara Masel Bridget Ikin Vicki Sugars
| writer         = Sarah Watt Anthony Hayes
| music          =  Amanda Brown 
*Ashley Klose
| cinematography = Ray Argall
| editing        = Denise Haratzis
| distributor    = Madman Entertainment (Australia)
| released       =  
| runtime        = 100 minutes
| country        = Australia
| language       = English
| budget         =
| gross          = $2,490,011 
}} 
 AFI Awards, VCE English Course from 2007 to 2010. 

==Plot== Photojournalist Nick (William McInnes) discovers he has testicular cancer that has spread to his lungs. On his way home he goes to the site of a train accident to report on it, and meets Meryl (Justine Clarke) an emotionally vulnerable artist, who has witnessed a man get run over by a train. Over the course of the weekend, their relationship develops sexually as another chance encounter allows them to discover more about each other; the two gradually allow themselves to let go of their fears and form a meaningful relationship.

Meanwhile, Nicks colleague, Andy Walker, has to deal with the news that his estranged girlfriend, Anna, is pregnant, made more difficult because neither of them really wanted or planned for a baby. Andy also has to cope with his ex-wife, who doesnt trust his ability to take good care of his two children.
The lives of Julia (the partner of the man run over by the train) and the driver of the train are explored: Both characters are shown going through the seven stages of grief. The train driver bridges the gap with his estranged teenage son during the course of the movie. The rain at the end of the film symbolizes relief.   

The films credits are complemented by a series of photographs showing Nick and Meryl staying together, eventually traveling together and Nick surviving cancer.

==Cast==
* William McInnes as Nick
* Justine Clarke as Meryl Lee Anthony Hayes as Andy Walker
* Lisa Flanagan as Anna Andrew Gilbert as Phil
* Daniella Farinacci as Julia
* Maggie Dence as Joan
* Edwin Hodgeman as Jim
* Andreas Sobik as Train Driver
* Alex Rafalowicz as Train Drivers Son
* Sacha Horler as Linda
* Robbie Hoad as Rob (Train Victim)
* Mary Kostakidis as SBS Newsreader

==Awards== Queensland Premiers Literary Awards Film Script - the Pacific Film and Television Commission Award for Sarah Watt
* 2005 Won Toronto International Film Festival: Discovery Award, Sarah Watt Anthony Hayes)
* 2005 Nominated  ), Best Lead Actress (Justine Clarke), Best Production Design, Best Sound, and Best Supporting Actress (Daniella Farinacci) 
* 2005 Won   award, Sarah Watt
* 2005 Won Inside Film Awards: Best Direction, Best Script, and Best Editing
* 2005 Nominated Inside Film Awards: Best Actress (Justine Clarke), Best Feature Film, Best Music, and Best Sound
* 2005 Nominated European Film Awards: Screen International Award, Sarah Watt Film Critics Circle of Australia Awards: Best Film, Best Director, Best Actor in a Lead Role for (William McInnes), Best Original Screenplay and Best Editor
* 2005 Nominated  ), and Best Musical Score
* 2006 Won Australian Screen Directors Association: Best Direction of a First Feature Film
* 2006 Won NatFilm Festival: Critics Award, Sarah Watt
* 2006 Won International Film Festival Rotterdam KNF Award, Sarah Watt
* 2006 Won Motovun Film Festival &mdash; Propeller Award (Grand Prize), Sarah Watt
* 2006 Won Golden Trailer Awards: Most Original Foreign Trailer
* 2006 Won Mar del Plata Film Festival: Best Script (Sarah Watt), Best actress (Justine Clark)
* 2006 Nominated Mar del Plata Film Festival: Best Film

==Box office==
Look Both Ways grossed $2,969,712 at the box office in Australia. 

==See also==
 
* 2005 in film
* Cinema of Australia
* List of Australian films
* South Australian Film Corporation

==References==
 

==External links==
*  
*  
*  
*   by Brendan Winter

 

 
 
 
 
 
 
 
 
 