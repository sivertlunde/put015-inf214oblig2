Ravedactyl: Project Evolution
{{Infobox film
| name = Ravedactyl: Project Evolution
| image = RavedactylCover.png
| image_size =
| caption = Cover to the Ravedactyl comic book
| director = Graig F. Weich
| producer = Graig F. Weich
| writer = Graig F. Weich
| narrator =
| starring =
| music = Graig F. Weich
| cinematography = Graig F. Weich
| editing = Graig F. Weich
| distributor =
| released =  
| runtime =
| country = United States
| language = English
| budget =
| gross =
}} director Graig Weich, based on his superhero comic book Ravedactyl, which won the New York International Independent Film and Video Festival.  It stars Coolio, Donald Faison, and Dave Prowse.    

==Plot==
In the story, the protagonist acquires a power that he cannot fully control, that will allow him to unlock the secrets to humanitys existence. 

==Cast==
* Coolio as Maduzor
* Donald Faison as Gunner
* David Prowse as Sunder
* Sergio Velez as Tricity / Trench
* Graig F. Weich as Farrell/Ravedactyl
* G.F.W. as Clint/Civilian Justice/Code Name: Justice

==Awards==
*2003 New York International Independent Film and Video Festival Award for Best Short Film (for Ravedactyl: Project Evolution)  

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 
 


 
 