Plastic City (film)
{{Infobox film
| name           = Plastic City
| image          = Plastic City HG Poster.jpg
| caption        = Hong Kong release poster
| director       = Yu Lik-wai
| producer       = 
| writer         = 	  Anthony Wong Joe Odagiri Domingos Antonio
| music          = Yoshihiro Hanno	 
| cinematography = Lai Yiu-fai
| editing        = Wenders Li
| studio         = Bitters End Gullane Filmes Novo Films Sundream Motion Pictures Xstream Pictures
| distributor    = Bitters End (Japan)  Paris Filmes (Brazil)
| released       =  
| runtime        = 118 minutes
| country        = Brazil China Hong Kong Japan
| language       = Mandarin Portuguese Japanese
| budget         = BRL|R$ 5,5 million 
| gross          =
}} thriller film directed by Yu Lik-wai.

==Plot==
Set in the traditional neighborhood of Liberdade in downtown São Paulo, Plastic City has the immense Eastern community that was established in the city over the years as protagonist, people who are seeking for a land of opportunity and a place for new business - legal or illegal. It is in the midst of Chinese cultural traditions and the chaos of urban life that the immigrant Yuda and his son Kirin are commanding a piracy mafia in Brazil, but this empire went into decline when they were threatened by a powerful organization with strong international influences. 

== Cast == Anthony Wong as Yuda
*Joe Odagiri as Kirin
*Domingos Antonio as Federal Agent
*Alessandro Azevedo as Camacho
*Ricardo Bittencourt as Guilhermo
*Chao Chen as Yanno
*Rodrigo dos Santos as Marquito
*Barbara Garcia as Black Dancer Huang Yi as Ocho
*Renata Jesion as Reporter
*Hiyan Kubagawa as Kirin 5 Years
*Tainá Müller as Rita
*Eliseu Paranhos as Sheriff
*Antônio Petrin as Coelho
*David Pond as Uncle Tong
*Felipe Eduardo Salvador as Chapinha
*Babu Santana as Beto
*Thogun as Ignacio

==References==
 

==External links==
*    
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 

 