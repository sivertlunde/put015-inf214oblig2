Winnie the Pooh and Christmas Too
 
{{Infobox film
| name           = Winnie the Pooh and Christmas Too
| image          = Winnie the Pooh and Christmas Too Coverart.png
| caption        = Winnie the Pooh and Christmas Too VHS cover
| director       = Jamie Mitchell
| producer       = Jamie Mitchell Ken Kessel
| writer         = Karl Geurs Mark Zaslove Michael Gough Edan Gross
| music          = Steve Nelson  Thomas Richard Sharp
| edtior         = Lee Phillips Simon Cruse Rick Hinson
| distributor    = Disney–ABC Domestic Television
| studio         = Disney Television Animation
| released       =  
| runtime        = 26 minutes (United States|US)
| country        = United States
| language       = English
}}
 Disney television series The New Adventures of Winnie the Pooh, originally broadcast on Saturday, December 14, 1991 on American Broadcasting Company|ABC.

TV Guide ranked the special number 6 on its 10 Best Family Holiday Specials list. 

==Plot==
Two days before Christmas, Christopher Robin writes out a letter to Santa Claus for him and his friends in the Hundred Acre Wood, asking for the following presents:
 Rabbit likes a new fly swatter to keep the bugs off his carrots
* Eeyore could use an umbrella to keep the snow off his house
* Tigger likes a snowshoe for his tail so he can bounce on the snow without his hands and feet (because otherwise he sinks into the snow)
* Christopher Robin wants is a sled "big enough for me and maybe a friend or two" Piglet said Santa Claus could bring whatever he wanted
 Piglet informs him, that he did not ask for anything himself, so they go find the letter, which has not gotten very far. Afterwards, they, along with Tigger and Eeyore, go to Rabbits house and rewrite the letter to include what Pooh wants (a pot of honey, of course). Along the way, though, they become greedy and start upgrading their desires (Rabbit wants a bug sprayer, for instance).
 Gopher (he is supposed to be hibernating). In the meantime, Pooh and Piglet go back to the point where Christopher Robin sent the letter and cast it off into the wind again. But the wind shifts southward, and the letter follows Pooh all the way to his house. He goes to Piglet and informs him of what happened Knowing that the rest of the gang will not get their presents as a result of this, Pooh tells Piglet they must take it into their hands to make sure the gifts are delivered.

Disguised as Santa, Pooh sneaks out and delivers Tigger, Rabbit, and Eeyore a super-bouncer barrel, a bug sprayer, and a mobile home, respectively - or rather, handmade versions of the said items that break apart upon use. Demanding to know what is going on, the three of them corner "Santa", who insists that he is who he claims to be. But then, Piglet (disguised as a "sorry-lookin reindeer") slips and makes his sled fall downhill, thus loosening Poohs disguise.

After explaining what happened, Pooh decides to try to deliver the letter to Santa himself, telling the rest of the gang it would be worth missing Christmas if he could "bring Christmas" to them. He does not get far, though, as the wind suddenly takes the letter, so he gives up and goes back to the gang. Even after he tells them that he failed, they are happy to have him back, because they have realized what Christmas is really about (they admit having him with them at Christmas is more important than getting gifts and were willing to give their gifts up to have him back). Afterwards, Christopher Robin shows up on his new sled and brings them the gifts they had originally asked for. Everyone is happy except Pooh, who feels he doesnt deserve his gift. But then giving Christopher Robin a hug he gets very happy.

==Voices==
* Jim Cummings as Winnie-the-Pooh
* Paul Winchell as Tigger Rabbit
* Piglet
* Peter Cullen as Eeyore Michael Gough Gopher
* Edan Gross as Christopher Robin

==Broadcast history== Beauty and the Beast. The first airing of the special ranked 53rd out of 92 shows that week, averaging a 10.8/20 rating/share, ranking second place in its first half hour behind The Golden Girls and first place in its second half hour ahead of Walter and Emily. 

The special was first released on VHS in 1994. The VHS was re-released in 1997, with a sneak peek at Recess (TV series)|Recess before the special. Also, it was later partnered with Mickeys Christmas Carol. It was originally broadcast on American Broadcasting Company|ABC. The special was broadcast on CBS in 1995. After Disneys purchase of ABC, that network once again became the home of all subsequent broadcasts. It re-aired on ABC in 1996 and kept on air until 1999. It returned for the first time on December 11, 2007, but immediately edited down. The edited 2007 version aired on ABC Family, as part of their "25 Days of Christmas" as of December 2008.

Currently, the only  , where it is edited into the main feature. However, Christoper Robins lines are re-dubbed by his voice actor in the films main story, and Rabbits animation is recolored to have him in his usual yellow-furred appearance (as opposed to his greenish fur in the New Adventures series).

==References==
 

==External links==
*  

*  

 
 

 
 
 
 
 
 
 
 
 