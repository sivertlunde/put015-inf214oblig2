Guskō Budori no Denki
{{Infobox film
| name           = Guskō Budori no Denki
| image          = File:Guskō_Budori_Poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Gisaburō Sugii
| producer       = 
| writer         = 
| screenplay     = Gisaburō Sugii
| story          = 
| based on       =  
| narrator       = 
| starring       = Shun Oguri Shiori Kutsuna
| music          = Ryōta Komatsu
| cinematography = 
| editing        = 
| studio         = Tezuka Productions
| distributor    = 
| released       =   
| runtime        = 105 minutes
| country        = Japan
| language       = Japanese
| budget         = 5 million
| gross          = US$2,868,421 
}}
 , or The Life of Guskō Budori, is a 2012 Japanese animated film directed by Gisaburō Sugii.   

The movie is based on the 1932 fantasy novel by the same name by Kenji Miyazawa. Like Night on the Galactic Railroad before it, the characters are depicted as anthropomorphic cats.

It is the second animated movie adaptation of the story. The previous adaptation was released in 1994 by Bandai Visual.

==Cast==
*Shun Oguri as Guskō Budori
*Shiori Kutsuna as Neri

==References==
 

==External links==
*    
*  
*  

 

 
 
 
 

 