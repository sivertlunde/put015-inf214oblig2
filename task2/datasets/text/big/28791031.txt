Dirty Girl (2010 film)
{{Infobox film
| name           = Dirty Girl
| image          = Dirty girl film poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Abe Sylvia
| producer       = Rachel Cohen Rob Paris Charles Pugliese Jana Edelbaum
| writer         = Abe Sylvia
| starring       = Juno Temple Milla Jovovich William H. Macy Mary Steenburgen Dwight Yoakam Jeremy Dozier Maeve Quinlan Tim McGraw
| music          = Jeff Toyne
| cinematography = Steve Gainer
| editing        = Jonathan Lucas
| distributor    = The Weinstein Company
| studio         = Serendipity Point Films
| released       =   (theatrical release)
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = $4 million 
| gross          = $55,125  
}}
Dirty Girl is a 2010 coming of age Comedy-drama|comedy-drama film written and directed by Abe Sylvia. It stars Juno Temple,  Milla Jovovich and William H. Macy. It premiered at the 2010 Toronto International Film Festival on 12 September 2010.  It was distributed theatrically by The Weinstein Company  on October 7, 2011. 

==Plot==
1987 Oklahoma: Danielle Edmondston (Juno Temple) is a troubled and promiscuous high school student. She argues with her mother, Sue-Ann (Milla Jovovich), who is about to marry a Mormon, Ray (William H. Macy), and amidst the chaos she befriends Clarke Walters (Jeremy Dozier), a shy, gay classmate. Together, they flee in a car owned by Clarkes homophobic father, Joseph (Dwight Yoakam), and embark on a road trip to Fresno, where Danielle expects to find her birth father, Danny Briggs. Meanwhile, Sue-Ann and Clarkes mother, Peggy (Mary Steenburgen), chase after them.

Joseph breaks into Danielles house in an attempt to find Clarke, only to find that the entire family is gone in vacation, besides Danielle, who has already left with Clarke. Joseph is then arrested for breaking into the house. He calls Peggy to bail him out, only to find out that Peggy refuses to let him out and that she will not allow him to harm Clarke for being gay anymore. Joseph, aggravated, has to stay in the cell until a judge can see him.

On the way, Danielle and Clarke pick up a hitch-hiker named Joel (Nicholas DAgosto), who after they stop for rest, has sex with Clarke. Clarke awakens the next morning to find that he is gone, leaving him heartbroken. Clarke blames Danielle for this. After seemingly moving on and getting back in the car, it breaks down on the side of the road. Clarke and Danielle continue on foot, trying to rent a car, only to find Joseph has been released from prison and has reported their credit card stolen. Desperate for money, the two enter a bar and Danielle enters a stripping contest. After she is booed profusely, Clarke realizes that it is a biker gay bar. Danielle tells him he must strip instead.

Clarke is cheered as he dances, but is caught by Joseph who enters during this. Danielle collects the prize money, but they are both taken in Josephs other car. Clarke  provokes his father into pulling the car over to attack him, while he tells Danielle to flee. Danielle manages to make it to a bus station, upset having to had leave Clarke behind. She finds her fathers (Tim McGraw) house, where she is met by her mother, who asks her to leave. Danielle manages to make it to her father, who rejects her, revealing he has a young daughter.

Sad, Danielle goes home, later learning that Clarkes father has sent him to military school. Danielle enters the talent show and sings "Dont Cry Out Loud (song)|Dont Cry Out Loud" by Melissa Manchester, who is Clarkes favorite singer. As she breaks down singing, Clarke enters dressed in a military uniform. They finish the song together and get into Danielles car. Clarke reveals that his mother let him out of military school and that his father is now in an apartment alone and his Mother is planning on divorcing him.  Danielle, with a less rebellious attitude, and Clarke, now no longer afraid to be himself, drive off into the sunset.

==Cast==
* Juno Temple as Danielle Edmondston
* Milla Jovovich as Sue-Ann Edmondston
* William H. Macy as Ray
* Mary Steenburgen as Peggy Walters
* Dwight Yoakam as Joseph Walters
* Jeremy Dozier as Clarke Walters
* Maeve Quinlan as Janet
* Tim McGraw as Danny Briggs
* Nicholas DAgosto as Joel
* Elsie Fisher as Tiffany Briggs
* Brian Baumgartner as Concierge
* Nate Hartley as Charlie
* Reiley McClendon as Mike
* Jonathan Slavin as Mr. Potter
* Brent Briscoe as Officer Perry
* Jack Kehler as Doc Shelby
* Gary Grubbs as Principal Mulray

==Production==
Abe Sylvia developed the story in 2004 while attending UCLA. Sylvia describes it as a fictional account of “growing up in the 1980s” that draws upon some of his adolescent experiences in Oklahoma. Brian Brooks and Bryce Renninger.  . indieWire. June 10, 2010  

 . November 24, 2009  Jovovich subsequently replaced Hawkins in the role of Sue-Ann, and Mary Steenburgen replaced Kudrow in the role of Peggy.

Filming began in Southern California in 2010 and was completed in Los Angeles in May 2010. 

==Reception==
Dirty Girl received mostly negative critical reviews, with a "Rotten" rating of 25% at the review aggregator website Rotten Tomatoes, based on 28 reviews.  In the New York Times, A.O. Scott declared that he found himself cheering not for the main characters on their road trip, but for the actors Temple and Dozier who were doing their best to salvage a chaotic script that "has far less insight, and much less panache, than a randomly chosen episode of Glee (TV series)|Glee."  

The film was a disaster at the box-office, earning only $55,125 against an estimated budget of $4 million, due to a limited release.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 