Ghulami
{{Infobox film
| name = Ghulami
| image = Ghulamifilm1.jpg
| image_size = 
| caption = Promotional Poster
| film name      = Ghulami
| director       = J.P. Dutta
| producer       = 
| writer         = 
| Lyrics         = Gulzar
| narrator       = Amitabh Bachchan
| starring       = Dharmendra Mithun Chakraborty Naseeruddin Shah Reena Roy Smita Patil Kulbhushan Kharbanda Raza Murad
| music          = Laxmikant-Pyarelal
| cinematography = Ishwar Bidri
| editing        = 
| studio         = 
| distributor    = Nadiadwala Sons Bombino Video Pvt. Ltd.
| released       = June 28, 1985
| runtime        = 201 minutes
| country        = India
| language       = Hindi
| budget = 
| preceded_by = 
| followed_by = 
}} 1985 Hindi Indian feature directed by Mazhar Khan, Kulbhushan Kharbanda, Raza Murad, Smita Patil, Reena Roy, Anita Raj, Naseeruddin Shah and Om Shivpuri, Lyrics by Gulzar & Music by Laxmikant-Pyarelal. 
It was shot at Fatehpur, Rajasthan.
Amitabh Bachchan narrated the film.

==Plot== Thakur landlords. It was based on story of a school master Ranjit Singh Choudhary.

The poor peasant (mainly belonging to a lower caste in the discriminatory caste system) is born with a loan which had been taken by his ancestors and dies in debt, leaving it for his next generation; his land and belongings are always under the threat of confiscation due to non-payment of the loan and, worst, the honour of the women of his family is under constant threat from the lusty landlords, still enjoying the undesirable benefits of the feudalistic landownership system continuing for centuries in the rural India. On 15 August 1947, India got independence but the poor peasants are still accursed to bear the brunt of the slavery of the mighty.

O.P. Dutta presented the story of a well-educated and self-respectful peasant (Dharmendra) who unfortunately finds himself on the lower rung of the ladder of the social structure because of his birth. The daughter of the supercilious landlord possesses a different nature and outlook and she (Smita Patil) is his close friend.

The village police, as usual, is sold in the hands of the Rajput Thakur (Om Shivpuri) and join hands with him and his arrogant and lustful sons (Bharat Kapoor and Mazhar Khan) to oppress and humiliate the already victimized by misusing the power of the uniform. A like-minded ex-serviceman (Mithun Chakraborty) join hands with him and a lower-caste kind-hearted Hawaldar (Kulbhushan Kharbanda) comes to his side after losing his son in an confrontation with the upper-caste and rich mighties who dont allow the lower-caste grooms to ride a horse. The main protagonist has to become a bandit in the eyes of the law to save the land and the honour of the hundreds of peasants, leaving his pregnant wife (Reena Roy) behind. His friends husband (the landlords daughter) is a senior police officer (Nasiruddin Shah) who becomes his enemy after knowing about his friendship with his wife. The violence-ridden climax underscores this harsh reality only that the rebels die, the cruel and unjust system doesnt.

==Cast==
*Dharmendra  as  Ranjit Singh Choudhary
* Mithun Chakraborty  as  Javar Pratap
* Naseeruddin Shah  as  Thakur Sultan Singh
* Reena Roy  as  Moran
* Smita Patil  as  Sumitra 
* Anita Raj  as  Tulsi
* Kulbhushan Kharbanda  as  Havaldar Gopi Dada
* Raza Murad  as  Fatehpurs Thanedar
* Om Shivpuri as  Bade Thakur
* Avtar Gill as  Shaamu
* Bharat Kapoor  as  Thakur Shakti Singh Mazhar Khan  as  Thakur Jaswant Singh
* Sulochana Latkar  as  Makhan Singhs wife
* Ram Mohan  as  Masterji
* Anjan Srivastav  as  Bade Thakurs Munim
* Surendra Pal  as  Daku Suraj Bhan
* Rajan Haksar  as  Makhan Singh Choudhary
* Huma Khan  as  Singer

==Soundtrack==
The music composed by Laxmikant–Pyarelal.
{{Infobox album Name     = Ghulami Type     = film Artist    = Laxmikant–Pyarelal Cover    = Ghulamiaudi.jpg Released =  Genre  Feature film soundtrack Length   =  Label    = 
}}
{{Track listing
| extra_column = Singer(s)
| all_lyrics        = Gulzar
| all_music       = Laxmikant–Pyarelal
| title1              = Mere Peeko Mujhse Roothi | extra1 = Lata Mangeshkar
| title2              = Mere Peeko Waqt Beeja Tha | extra2 = Lata Mangeshkar
| title3              = Peele Peele | extra3 = Manhar Udhas, Shabbir Kumar, Suresh Wadkar
| title4              = Zihal-e-Miskin Makun ba-Ranjish| extra4 = Shabbir Kumar, Lata Mangeshkar
}}

==References==
 
*http://ibosnetwork.com/asp/filmbodetails.asp?id=Ghulami

==External links==
* 

 
 
 
 
 
 
 