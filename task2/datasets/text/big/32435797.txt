Grave Encounters
{{Infobox film
| name           = Grave Encounters
| image          = Grave-Encounters-Poster-2011.jpg
| caption        = Promotional poster
| director       = The Vicious Brothers
| producer       = Twin Engine Films Digital Interference Productions Shawn Angelski Michael Karlin
| writer         = The Vicious Brothers
| starring       = Sean Rogerson Ashleigh Gryzko Mackenzie Gray Juan Riedinger Merwin Mondesir Matthew K McBride
| music          = Quynne Craddock
| cinematography = Tony Mirza
| editing        = The Vicious Brothers
| studio         = West Wing Studios
| distributor    = Tribeca Film Festival
| released       =  
| runtime        = 95 minutes
| country        = Canada
| language       = English
| budget         = $2 million
| gross          = $5,408,334 {{cite web
| title = Box Office Mojo
| url = http://boxofficemojo.com/movies/intl/?page=&country=IT&id=_fGRAVEENCOUNTERS01
| accessdate = 2011-08-26}}  
}} found footage paranormal Reality reality television haunted psychiatric hospital in search of evidence of paranormal activity as they shoot what ends up becoming their final episode.

Written and directed by The Vicious Brothers (Collin Minihan and Stuart Ortiz), the film premiered on April 22, 2011 at the Tribeca Film Festival and received mostly mixed reviews. The film was released on August 25, 2011 in select theaters using the Eventful Demand It and Video on Demand via Comcast. The film premiered internationally in Italy via distributor Eagle Pictures under the title ESP Fenomeni Paranormali on June 1, 2011.
 sequel in Riverview Hospital, a mental institute in Coquitlam, British Columbia.

==Plot== ghost investigation television series. Jerry introduces the host, Lance Preston (Sean Rogerson). Hartfield explains that Grave Encounters was cancelled after the fifth episode, when the footage for episode six mysteriously went missing. The footage was finally recovered in 2010 and brought to Hartfield.
 medium Houston Grey. They are investigating the abandoned Collingwood Psychiatric Hospital, where an estimated 80,000 patients were treated. Since its closure, many instances of paranormal activity had been reported there.
 orbs in the pictures he took. Later, while retrieving the surveillance cameras that he had set up, Matt goes missing. As the team looks for him, T.C. is pushed from the stairs and injured. He suggests to the team that there was something in the hospital playing with them, but is now getting violent.

At 6:19 a.m, T.C. tries to open the hospitals doors. The doors open, but to another hospital corridor instead of the outside. Later, Sasha finds that it is 8:34&nbsp;a.m. Though the sun was to rise at 7:45 a.m, it is still dark outside. That afternoon, Lance finds all their food has rotted and explains to the camera that the corridors keep changing. While the group is sleeping again, team finds the word HELLO scratched into Sashas back. After the group encounters a young woman in a hospital gown whose face demonically distorts, Houston is separated and killed by an invisible force.

T.C., Sasha and Lance take time to rest and awaken to realize that they now have patient I.D. wristbands with their names on them. The team finds Matt in a room, wearing a hospital gown. Matt appears mentally unstable at this point. T.C. is dragged into a bathtub full of blood and disappears, while Matt commits suicide by jumping down an elevator shaft. Sasha becomes violently sick and starts throwing up blood. While Lance and Sasha nap, a mist rolls over them, causing  Sasha to disappear. Several scenes follow Lance alone as his mental health spirals downward. He eventually stops filming.

Lance turns the camera back on after finding the secret operating room of Dr. Arthur Friedkin, who performed unethical experiments on Collingwoods patients. Lance finds pictures of the experiments, along with pictures of his team also being experimented on. He also finds evidence of satanic rituals and black magic. Lance turns around to see Friedkin performing a lobotomy on one of the team members. Lances screams are heard before the camera cuts out. The camera is turned back on, showing Lance alive, though his eye is bleeding from a lobotomy.

==Cast==
*Sean Rogerson as Lance Preston
*Ashleigh Gryzko as Sasha Parker
*Merwin Mondesir as T.C. Gibson
*Mackenzie Gray as Houston Grey 
*Juan Riedinger as Matt White
*Arthur Corber as Dr. Arthur Friedkin
*Bob Rathie as Kenny Sandavol (Caretaker)
*Matthew K. McBride as Spiritual Force
*Ben Wilkinson as Jerry Hartfield
*Alex Timmer as The Tongueless Demon
*Eva Gifford as Demon Girl
*Michele Cummins as Bathtub Demon
*Shawn Macdonald as Morgan Turner
*Fred Keating as Gary Crawford
*Max Train as Punk Guy
*Marita Eason as Punk Girl
*Luis Jamer as Javier Ortega

==Production== Riverview Hospital, which has been the location for a many other television and film-based productions, and released on October 18, 2011 on DVD. The film came out for full digital download on iTunes on December 19, 2011.

==Critical response== The Ring", {{cite web
| url = http://www.nypress.com/blog-8813-catching-up-with-the-vicious-brothers.html
| title = Catching Up With The Vicious Brothers
| accessdate = 2011-08-26
| publisher = New York Press}}  other reviews were not that favorable. The rating website Rotten Tomatoes gave the film a "rotten" score of 49%. {{cite web
| url = http://www.rottentomatoes.com/m/grave_encounters/
| title = Grave Encounters (film)
| accessdate = 2015-04-13
| publisher = Rotten Tomatoes}} 

==Sequel== John Poliquin, and was written by The Vicious Brothers.

==See also==
*List of ghost films

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 