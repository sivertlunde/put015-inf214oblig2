Madame Aema 5
{{Infobox film
| name           = Madame Aema 5
| image          = Madame Aema 5.jpg
| caption        = Theatrical poster for Madame Aema 5 (1991)
| film name      = {{Film name
 | hangul         =   5
 | hanja          =   5
 | rr             = Aemabuin 5
 | mr             = Aemapuin 5}}
| director       = Suk Do-won 
| producer       = Choe Chun-ji Kim Jun-fu
| writer         = Lee Mun-ung
| starring       = So Bi-a
| music          = Kang In-goo
| cinematography = Ham In-ha
| editing        = Ree Kyoung-ja
| distributor    = Yun Bang Films Co., Ltd.
| released       =  
| runtime        = 96 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
Madame Aema 5 (hangul|애마부인 5 (1991) - Aema Buin 5) is a 1991 South Korean film directed by Suk Do-won. It was the fifth entry in the Madame Aema series, the longest-running film series in Korean cinema. 

==PLot==
Continuing the storyline started in Madame Aema 4, Aemas husband is still carrying on an affair with the Japanese woman, Hanako. After much romantic complication, just as Aema decides to divorce her husband she discovers that he has died in Japan because of Hanako.   

==Cast==
* So Bi-a: Aema 
* Choi Dong-joon: husband
* Jeon Hye-seong: Erica
* Choe Ho-jin: Ho-jin
* Yeon Hyeon-cheol: Hwa-ga
* Min Hui: Hanako
* Lee Jeong-yeol: Jung-hun
* Sin Jin-hui: Ju-hee
* Jeong Young-kuk
* Kim Gi-jong

==Bibliography==

===English===
*  
*  
*  

===Korean===
*  
*  
*  

==Notes==
 

 
 
 
 
 
 


 
 