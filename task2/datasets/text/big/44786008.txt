Bambola
{{Infobox film
 | name =Bambola
 | image = Bambola.jpg
 | caption =
 | director =  Bigas Luna
 | writer = Bigas Luna Cesare Frugoni
 | story =  
 | starring =  Valeria Marini
 | music =   Lucio Dalla 
 | cinematography =  Fabio Conversi
 | released = 1996
 | language =  Italian
 }} erotic melodrama film written and directed by Bigas Luna.  

== Cast ==

* Valeria Marini: Mina, aka "Bambola"
* Stefano Dionisi: Flavio
* Jorge Perugorría: Furio
* Manuel Bandera: Settimio
* Antonino Iuorio: Ugo
* Anita Ekberg: Mother Greta

== Controversy ==

Because of the many scenes of sexual abuse, the film was R-rated, something that caused the ire of lead actress Valeria Marini, who asserted that she had been promised the cut of the three more explicit scenes and a ban for just people under 14 years.  The actress therefore sued the producer Marco Poccioni demanding the immediate withdrawal of the film, but her request was eventually rejected. 

== Reception ==
The film received extremely negative criticism. Film critic Morando Morandini referred to it as "the most silly, foolish and amateurish film of Bigas Luna",    while Paolo Mereghetti was even tougher, saying he "never have come out of a movie theater with a much deeper discomfort."   

==References==
 

==External links==
* 
 
 
 
 
 
 
  
 
 
 

 
 