Exit Through the Gift Shop
 
{{Infobox film
| name           = Exit Through The Gift Shop
| image          = Exit-through-the-gift-shop.jpg
| director       = Banksy
| producer       = Holly Cushing Jaimie DCruz James Gay-Rees
| writer         =
| narrator       = Rhys Ifans Invader André (artist)|André
| music          = Geoff Barrow
| cinematography = Chris King
| studio         = Paranoid Pictures
| distributor    = Revolver Entertainment (UK)   Producers Distribution Agency (US)
| released       =  
| runtime        = 87 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          = $5,308,618
}}
Exit Through the Gift Shop: A Banksy Film is a film by street artist Banksy that tells the story of Thierry Guetta, a French immigrant in Los Angeles, and his obsession with street art. The film charts Guettas constant documenting of his every waking moment on film, from a chance encounter with his cousin, the artist Invader (artist)|Invader, to his introduction to a host of street artists with a focus on Shepard Fairey and Banksy, whose anonymity is preserved by obscuring his face and altering his voice, to Guettas eventual fame as a street artist himself. The film premiered at the 2010 Sundance Film Festival on 24 January 2010.
It is narrated by Rhys Ifans. The music is by Geoff Barrow. It includes Richard Hawleys "Tonight The Streets Are Ours".  The film was nominated for the Academy Award for Best Documentary Feature.

There has been debate over whether the documentary is genuine or a mockumentary, although Banksy answers "Yes" when asked if the film is real. 

==Synopsis== Thierry Guetta Monsieur André Zevs on Poster Boy, Ron English, Dotmasters, Swoon (artist)|Swoon, Azil, Borf and Buffmonster. What Guetta fails to tell Fairey is that he has no plan to compile his footage into an actual film, and indeed never looks at his footage.

Guetta continues to hear more about Banksy – a prominent and particularly secretive artist. His attempts to contact Banksy meet with failure, until one day Banksy visits LA without his usual accomplice, who is refused entry to the US. Stuck in LA without a guide, Banksy contacts Fairey, who calls Guetta. Guetta becomes Banksys guide in LA, later following him back to England, winning the privilege to film Banksy on his home turf – a feat that confuses Banksys crew. Banksy, however, sees the opportunity to document street art, which he recognizes as having a "short life span", and after Guetta aids him in recording both production, deployment and crowd reactions to his "Murdered Phone-box" piece, asks him to film the preparations for his "Barely Legal" show. The two become friends, as Guetta provides Banksy with some relief from his anonymity. Returning to LA, Guetta becomes bored, and eventually ends up producing his own stickers and decals and putting them up in the city.
 Guantanamo Bay detainee doll in Disneyland. He visits the location and places the doll while Guetta films. A short while later, however, the rides stop, and the parks security catch Guetta, who is taken to an interrogation room, while Banksy switches clothes and blends into the crowd. During interrogation, Guetta refuses to admit any wrongdoing, and when allowed a phone call, covertly alerts Banksy to his situation. When confronted by security personnel, he destroys the evidence in his stills camera, but stashes the videotape in his sock and is eventually let go, much to the amazement of Banksy who then says he trusts him implicitly because of the incident.

A few days later, "Barely Legal" opens, and becomes an overnight mainstream success. Street art prices begin to rocket in auction houses. Banksy is both surprised and disillusioned by the sudden hype surrounding street art, and urges Guetta to finish his supposed documentary. Guetta begins to edit together the several thousand hours of footage, and produces a film titled Life Remote Control. The result is 90&nbsp;minutes of distorted fast cutting about seemingly random themes. Banksy questions Guettas ability as a filmmaker, deeming his product "unwatchable", but realizes the street art footage itself is valuable. Banksy decides to have a shot at producing a film himself. To make sure that Guetta remains occupied, Banksy suggests he make his own art show.

{{Quote box
| quote = "I think the joke is on... I don’t know who the joke is on, really. I don’t even know if there is a joke."
| source = — Banksys former spokesman Steve Lazarides
| width = 25%
| align = right
}}

Guetta happily accepts the assignment, adopting the name "Mr. Brainwash", putting up street art in the city and six months later, re-mortgaging his business to afford renting copious equipment and a complete production team to create pieces of art under his supervision. He rents a former CBS studio to prepare his first show, "Life Is Beautiful", and scales up his production to something much larger than Banksy suggested, but with little focus. When Guetta breaks his foot after falling off a ladder, Banksy realises that the show may well become a trainwreck, and sends a few professionals to help Guetta out. While the producers take care of the practical side of the show, Guetta spends his time on more publicity, asking support from both Fairey and Banksy, eventually taping up huge billboards with their quotes, and ultimately ending up on the cover of L. A. Weekly. Preparation is seriously behind schedule, and Guettas production team insists that he must make decisions &mdash; yet Guetta spends his time hyping up and marketing his work for tens of thousands of dollars. Eight hours before the opening, paintings are still missing from the walls, and since Guetta is busy giving interviews, the eventual layout of the show is decided by the crew itself.

Despite all this, however, the show becomes a raging success with the crowd, and after the first week of the show, Guetta sells almost a million dollars worth of art, with his pieces showing in galleries all around the world, to the utter confusion of both Fairey and Banksy. In an ending montage, Guetta insists that time will tell whether he is a real artist or not.

==Production==
Banksy has said in interviews that editing the film together was an arduous process, noting that "I spent a year   watching footage of sweaty vandals falling off ladders"  and "The film was made by a very small team. It would have been even smaller if the editors didnt keep having mental breakdowns. They went through over 10,000 hours of Thierrys tapes and got literally seconds of usable footage out of it."  Producer Jaimie DCruz wrote in his production diary that obtaining the original tapes from Thierry was particularly complicated.

==Reception and hoax speculation==
Film industry veterans John Sloss and Bart Walker founded a new distribution company to release the film in the US, Producers Distribution Agency (PDA).  With a unique grassroots campaign, PDA brought the film to a US theatrical gross of $3.29MM. 
The film received overwhelmingly positive reviews, holding 96% on   overnight? The Boston Globe movie reviewer Ty Burr found it to be quite entertaining and awarded it four stars.  Roger Ebert gave it 3.5 stars out of 4, starting his review saying that "The widespread speculation that Exit Through the Gift Shop is a hoax only adds to its fascination." He dismissed the notion of the film being a "put on" saying "I’m not buying it; for one thing, this story’s too good, too weirdly rich, to be made up. For another, the movie’s gently amused scorn lands on everyone."  In an interview with SuicideGirls, {{cite web url = http://suicidegirls.com/interviews/Jaimie+D%27Cruz+and+Chris+King%3A+Exit+Through+the+Gift+Shop/ title = Jaimie DCruz and Chris King: Exit Through the Gift Shop publisher = SuicideGirls.com date = 14 Dec 2010 accessdate =24 February 2011
 }}.
  filmmakers Jaimie DCruz and Chris King denied that it was a hoax, and expressed their growing frustration with the speculation that it was: "For a while we all thought that was quite funny, but it went on for so long. It was a bit disappointing when it became basically accepted as fact, that it was all just a silly hoax ... I felt it was a shame that the whole thing was going to be dismissed like that really - because we knew it was true." 

The New York Times movie reviewer Jeannette Catsoulis wrote that the film could be a new subgenre, a "prankumentary". 

Guetta faced copyright issues following the release of the film. Glen Friedman, a prominent American photographer,  successfully sued Guetta over the use of a photograph of the rap group Run DMC.   Guetta tried to claim that he had altered it enough to be considered an original piece of art. However the presiding judge over the case, Judge Pregerson, ultimately ruled Friedman’s photograph was protected under the transformative fair use law.  Guetta also faced copyright claims from Joachim Levy, a Swiss filmmaker, who edited and produced Guetta’s film Life Remote Control,  clips of which were shown in the film. Levy was not credited for his work he did on the film, however Guetta owned the footage which was then licensed to Banksy.   

New York Film Critics Online bestowed its Best Documentary Award on the film in 2010. French journalist Marjolaine Gout gave it 4 stars out of 5, linking Mr. Brainwash and Jeff Koons and criticizing Thierry Guettas art as toilet papering. 

Oscilloscope Laboratories released Exit Through the Gift Shop on DVD and Blu-ray in 2011. 

==Archival footage== Ron English, Rash (film)|Rash, Restless Debt of the Third World, Spending Time, Turf War, Elis G The Life of a Shadow, Memoria Canalla, C215 in London, Beautiful Losers. 

==References==

===Notes===
 

===Further reading===
 
* 
* 
* 
* 
* Jackson, Candace; Schuker, Lauren A. E.,  , The Wall Street Journal, 12 February 2010.
* Ryzik, Melena,  , The New York Times, 13 April 2010.
*  as commentary on the relationship between art, innovation, and commerce. On Mises.org
*Michael Hutak,  . Review of Australian première at the 2010 Sydney Film Festival; critiques the film for posing as hoax.

 

==External links==
* 
* 
* 
*  - audio report by NPR
*  Film Review, Bright Lights Film Journal

 
 
 
 
 
 
 
 