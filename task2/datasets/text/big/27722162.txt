Feu Mathias Pascal
{{Infobox film
| name           = Feu Mathias Pascal
| image          = FeuMathiasPascal.jpg
| image_size     =
| caption        =
| director       = Marcel LHerbier
| producer       = Cinégraphic Films Albatros
| writer         = Marcel LHerbier Luigi Pirandello (novel) Ivan Mosjoukine Marcelle Pradot Michel Simon
| cinematography =
| editing        =
| distributor    = Films Armor
| released       =  
| runtime        = 171 minutes 
| country        = France
| language       = Silent
| budget         =
| preceded_by    =
| website        =
}}
 1925  Il fu Mattia Pascal.

==Background== Sei personnaggi Il fu Mattia Pascal, he was sufficiently impressed by the film-makers earlier work to give his enthusiastic agreement. Marcel LHerbier, La Tête qui tourne. (Paris: Belfond, 1979.) pp. 115–117.   
 Ivan Mosjoukine who was under contract to the Films Albatros company. Negotiations then led to a shared production agreement for the film between LHerbiers own company Cinégraphic and Albatros. 

==Production==
Filming began in December 1924 with location shooting in Rome, San Gimignano, and Monte Carlo. Studio filming took place at the Montreuil and Épinay studios. Set designs were produced by Alberto Cavalcanti and Lazare Meerson (in his first assignment).

LHerbier was delighted with the dynamic performance of Mosjoukine in the leading role, as well as by the rest of his cast, among whom Pierre Batcheff and Michel Simon were both making their film débuts. 

==Plot==
After the financial ruin of his family, Mathias works in the library of the village of Miragno. He marries Romilde, whom he had previously been courting on behalf of his timid friend Pomino, and they live with his shrewish mother-in-law. When his mother and baby daughter die on the same day, Mathias in despair runs away to Monte Carlo. In the casino he soon wins 500,000 francs. On his way home he reads in a newspaper that he is believed to have committed suicide and another body has been identified as his. He decides to seize this chance of freedom and to start a new life in Rome. There, under the name of Adrien, he falls in love with his landlords daughter, Adrienne, who is engaged to an archaeologist, Térence Papiano. At a séance, Papiano and his brother Scipion steal Adriens money. Unable to go to the police, Adrien/Mathias resigns himself to returning to Miragno. He discovers that Romilde has remarried, to Pomino, and they have a new child. He decides to leave them in peace, and sets off again for Rome and Adrienne.

==Cast== Ivan Mosjoukine as Mathias Pascal
* Marcelle Pradot as Romilde
* Lois Moran as Adrienne Paléari
* Marthe Mellot as Madame Pascal
* Michel Simon as Jérôme Pomino
* Jean Hervé as Terence Papiano
* Pierre Batcheff as Scipion Papiano

==Reception==
The film received its first screening in Paris in July 1925. Because of its length, it had to be shown in two parts, which LHerbier felt was damaging to its impact.   Nevertheless, it was mostly well received by both critics and public, and overall it represented the best contemporary success that LHerbier had with any of his films. It also received distribution abroad (rarely for a French film of the period).   

The receipts of the film were valued at 1,219,026 francs; around two-thirds of this amount was earned outside France. 

==Alternative titles==
English versions of the title have included:  The Late Matthias Pascal;  The Late Matthew Pascal;  The Late Mathew Pascal; The Late Mattia Pascal;  and in the United States, The Living Dead Man.

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 