Saints and Sinners (1994 film)
{{Infobox film
| name         = Saints and Sinners
| image        = Saints and Sinners 1994 Film Poster.jpg
| image_size   = 
| caption      = American poster of "Saints and Sinners".
| genre        = Crime, Drama
| runtime      = 100 minutes
| creator      = 
| director     = Paul Mones
| producer     = Paul Mones Jon Kilik (executive producer) Guy Riedel (executive producer) Marcia Shulman (associate producer) Michele Weisler (co-producer)
| writer       = Paul Mones	 Jennifer Rubin Scott Plank
| editing      = Leo Trombetta
| music        = Steven Miller Tom Varner
| budget       = 
| country      = United States
| language     = English
| released     = 1994
}} Jennifer Rubin, and Scott Plank. It was written, directed and produced by Paul Mones. The films production company was MDP Worldwide.

==Background==
The film stars Damian Chapa as Pooch, Jennifer Rubin as Eva, and Scott Plank as Big Boy.  It was first premiered in France in September 1994 at the Deauville Film Festival and later saw a proper theatrical release in the country on 19 April 1995 via the distributor Les Films Number One, whilst in the UK the films video premiere was during May 1996.  In France, Kinemastar was the films publicity company. http://www.imdb.com/title/tt0117539/companycredits?ref_=tt_dt_co 

On February 20, 1996, the film was released on VHS in America via Live Home Video/Lions Gate.  In Brazil, it was released on VHS via Look Filmes and via Videosonic Arts in 1995 in Greece.  A UK VHS release was also issued.  In Germany it was released on DVD,  whilst a Scandinavian DVD release was also issued.  The film was issued in America on Amazon Instant Video in recent years. 

The films main tagline reads "Guns. Drugs. Sex. Anything two can do, three can do better." 

For the film, Paul Mones was nominated in 1994 for the Critics Award of the Deauville Film Festival, whilst in 1995, Mones won the Special Jury Prize for the film by the Cognac Festival du Film Policier. 

==Plot==
After many years Pooch returns to his old New York neighborhood and teams up with his old high school friend Big Boy to run the local rackets. Big Boy realizes that Pooch is a valuable and trustworthy asset in his plans of becoming local crime lord, and the two get involved with the local drug trade of the neighborhood. With the help of their pal Juanito, the pair continue to get close to having total control and will be able to make a proposition to the mob behind it all. However Big Boy doesnt know that Pooch is a cop being charged for corruption, who was given one last chance to avoid jail by going undercover to clean up his old neighborhood. To Pooch, this reunion is painful because as an undercover cop, he is working as an informer within Big Boys gang, with plans to bring Big Boy down. While Pooch is undecided about whether he should help them, or help himself, as well as struggling between loyalty and betrayal, the two friends relationship is further tested with an arrival of aspiring-model and pathological-liar Eva, who Pooch meets in a health food restaurant. Both men fall in love with her and it isnt long before all three of them are sleeping together, whilst each have their own agenda.       

==Cast==
* Damian Chapa as Pooch Jennifer Rubin as Eve
* Scott Plank as Big Boy
* William Atherton as Terence McCone
* Damon Whitaker as Rockitt
* Charles Guardino as Tommy The Cow Bob Larkin as Tony
* Panchito Gómez as Juanito
* DeJuan Guy as Arthur
* Sal Landi as Director
* Bob Delegall as Captain Reineke
* Eugenie Bondurant as Mel Taybach
* Philip Casnoff as Detective Battaglia
* Jeff Cadiente as Bartender
* Billy Bastiani as Waiter
* Gianin Loffler as Rico
* Roy Matlen as Dacay
* Ricco Chapa as Little Boy Juan Fernández as Priest

==Reception==
Sandra Brennan of Allmovie gave the film three out of five stars, calling it a "gritty crime thriller". 

Uncle Scoopys Movie House gave the film a C-, stating "Saints and Sinners was meant to be gritty, but that, for me, is its downfall. There is entirely too much scenery-chewing and very little subtlety, and the script does not quite take the time and effort needed to sell the friendship between Plank and Chapa." 

The 2005 book VideoHounds Golden Movie Retriever gave the film one and half out of five stars. 

On February 16, 1996, Susan King of the Los Angeles Times, described the film as a "sleazy, erotic thriller". 

On March 23, 1996, Roanoke Times stated "A screwy supercharged energy drives "Saints and Sinners," a crime drama/buddy flick/romance about two childhood friends and their adventurous girlfriend." 

Dragan Antulov of rec.arts.movies.reviews gave a rating of 6 out of 10. 

==References==
 

==External links==
*  

 
 
 
 