Mazhathullikkilukkam
 
 
{{Infobox Film
| name           = Mazhathullikkilukkam
| image          = Mazhathullikkilukkam.jpg
| image_size     = 
| caption        =  Akbar -Jose
| producer       = Sharada
| writer         = Sahilesh, Divakaran, J. Pallassery (screenplay)
| narrator       =  Dileep  Sharada  Bharathi
| music          = Suresh Peters
| cinematography = P. Sukumar
| editing        = Ranjan Abraham
| distributor    = Sargam Speed Release
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
 Akbar -Jose. Sharada and Bharathi in the lead roles.

The movie was produced by Sharada under the banner of Sharada Productions and was distributed by Sargam Speed Release.

After Mazhathullikilukkam and Sadanandante Samayam, Akbar-Jose team split. Akku Akbar, from the duo, went on to make commercially successful films including Veruthe Oru Bharya. 

== Plot ==
The story starts when Soloman(Dileep (actor)|Dileep) and his sister travel to kannadahalli to meet two retired teachers. He reaches the house on top of a hill-Swargam where he finds two old women who live alone with a cook and a home nurse to look after them. Alice (Sharada (actress)|Sharada) and Anna (Bharathi (actress)|Bharati) are sisters who are spinsters and they own the huge estate. The home nurse, Sophiya (Navya Nair), cook (Sukumari), cooks son (Cochin Haneefa) and the priest (Nedumudi Venu) are the only people close to them. Soloman takes charge as the manager of the estate and soon wins over the two oldies. 

He realizes that they are two angels and he tries to bring back joy into their morbid life. Soon, Soloman becomes the apple of their eye and they both consider him as their own son. Meanwhile he falls in love with the home nurse Sophiya with whom he had regular fights. Their wedding is fixed and the sisters give away all their wealth to both of them. The real twist in the story occurs when Sophiya reveals to Soloman that her brother was a photographer who got murdered by some drug addict students. Then during a flashback it is revealed, Soloman has a dark and disturbing past. Soloman is actually falsely accused in a murder case of the photographer and is hiding from the police. Soloman is filled with guilt and decides to leave the place on the pretext of higher education abroad. Sophiya overhears a conversation between Soloman and his sister. Unaware of all this, Soloman goes to the teachers and insists on them having payasam from him. Meanwhile a very guilty Sophiya confesses to the priest about what she heard and that she had poisoned Solomans payasam. Just then Soloman visits the priest and learns all this. Soloman rushes back only to find the two teachers dead. Soloman takes the punishment for Sophie and when he comes out he goes to the tombs of Anna and Alice John where he meets Sophie. The story end with them deciding to live together.

== Cast == Dileep as Soloman
* Navya Nair as Sofiya Sharada as Anna John Bharathi as Alice John
* Cochin Haneefa as Mathukkutty
* Salim Kumar as Mayandi
* Nedumudi Venu as Father Palaykkal
* Boban Alummoodan as Sabu
* Machan Varghese as Kuriakose
* Kozhikode Narayanan Nair as Father Ignetious
* Jose Pellissery
* Narayanankutty
* Suvarna Mathew as Treesa
* Sukumari as Kikkili Kochamma


== Songs ==
The songs in the movie has been composed by Suresh Peters with lyrics by R. Rameshan Nair. The BGM has been done by Ouseppachan. The songs were distributed by Super Star Audios.

# Kinavinte: Viswanath
# Therirangum mukile: P. Jayachandran
# Velippenninu thaalikku: Srinivas (singer)|Srinivas, Sujatha Mohan
# Puthuvettam thedi vannu: M. G. Sreekumar
# Ravinte devahrudayathil: K. J. Yesudas
# Swargam nammude: Vidhu Prathap
# Ravinte devahrudayathin: Chithra Sriram

== External links ==
*  
* http://popcorn.oneindia.in/title/2910/mazhathullikkilukkam.html

 
 
 