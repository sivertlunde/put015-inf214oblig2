The Lawful Cheater
{{Infobox film
| name = The Lawful Cheater
| image = The Lawful Cheater poster.jpg
| image size =
| border =
| alt =
| caption = Theatrical release poster Frank OConnor
| producer = B.P. Schulberg
| writer = Adele S. Buffington Frank OConnor
| screenplay =
| story =
| based on =  
| narrator =
| starring = Clara Bow David Kirby Raymond McKee
| music =
| cinematography =
| editing =
| studio = B.P. Schulberg|B.P. Schulberg Productions Preferred Pictures Corporation
| released =  
| runtime = 1492.91 m (5 reels)
| country = United States Silent (English intertitles)
| budget =
| gross =
}}
 silent crime Frank OConnor British Board of Film Censors. 

The film is presumed to be a lost film. 

==Plot==
Molly Burns (Clara Bow) is a young woman whose indiscreet behavior causes her to be caught and jailed in a police "round up" of suspicious characters. Her prison experience causes her to reflect upon and reform her own life. She convinces jail authorities that her two brothers and her boyfriend could be dissuaded from a life of crime. After her early release, she attempts to reform her indiscreet friends.

==Cast==
* Clara Bow as Molly Burns
* David Kirby as Rooney
* Raymond McKee as Richard Steele Edward Hearn as Roy Burns George Cooper as Johnny Burns
* Fred Kelsey as Tom Horan
* Gertrude Pedlar as Mrs. Perry Steele
* Jack Wise as Graveyard Lazardi
* John T. Prince as Silent Sam Riley
* Gilbert Roland as Undetermined Role (uncredited))

==Reception==
In American film cycles: the silent era, the film is called an "offbeat crime drama".  In noting the film was a "cheaply produced melodrama" with a storyline that was "slight and trite", Hal Erickson of AllRovi wrote that beyond the film benefitting from actual use of New York City locations, Clara Bow acted as the films "sole redeeming factor." He noted that at one point in the film, Bows character of Molly Burns appeared in male drag which even if "far from convincing", was "fun to watch."   

==See also==
*List of lost films

==References==
 

==External links==
*  
*   at silentera.com
* 

 

 
 
 
 
 
 
 
 
 