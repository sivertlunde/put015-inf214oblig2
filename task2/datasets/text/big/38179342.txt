Target Kolkata
{{Infobox film
| name           = Target Kolkata
| image          = Target Kolkata poster.jpg
| image_size     = 250px
| border         = 
| alt            = 
| caption        = Movie poster
| director       = Kartick Singh
| producer       = Shyamol Biswas
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  See below
| music          = 
| cinematography = 
| editing        = 
| studio         = SRB Films
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}} Bengali film directed by Kartick Singh, produced by Shyamol Biswas and presented by SRB Films. The music of this film is composed by Nayan Bhattacharya and Dron Acharya .       This was a surprise hit of 2013 and had an occupancy of around 55-60% for 2 weeks.

== Plot ==
 

== Cast == Rishi
* Bidita Bag
* Subrata Dutta
* Suparna Malakar
* Sreela Majumdar
* Suhasini Mulay
* Bodhisattwa Majumdar
* Prasun Gayen
* Jagannath Guha
* Arindam Sil

== Songs ==
{{Track listing
| headline        = 
| extra_column    = Singer
| total_length    =

| all_lyrics      = Banibrata Mukherjee and Anindya Bose
| all_music       = Nayan Bhattacharya and Dron Acharya

| title1          = Elomelo
| extra1          =  
| length1         =

| title2          = Pahi paka pnepepe kay
| extra2          =  
| length2         =

| title3          = Hele dhorte pare na
| extra3          =  
| length3         =

| title4          = Rojnamcha
| extra4          =  
| length4         =

| title5          = Janeman
| extra5          =  
| length5         =

| title6          = Rangila
| extra6          =  
| length6         =

| title7          = Target Kolkata (theme)
| extra7          =  
| length7         =

}}

== See also ==
* Life in Park Street

== References ==
 

 
 
 


 