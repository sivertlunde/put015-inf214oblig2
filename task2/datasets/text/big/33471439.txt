Grandmaster (film)
{{multiple issues|
 
 
}}
 
 

{{Infobox film
| name = Grandmaster
| image = Grandmaster.jpg
| alt =  
| caption = Teaser poster
| director = B. Unnikrishnan
| producer = Ronnie Screwvala Siddharth Roy Kapur
| writer = B. Unnikrishnan
| starring = Mohanlal Priyamani Anoop Menon Narain (actor) Jagathy Sreekumar Babu Antony
| music = Songs:  
| cinematography = Vinod Illampally
| editing = Manoj
| studio = UTV Motion Pictures Max Lab Entertainment  (Kerala)  UTV Motion Pictures  (Overseas) 
| released =   
| runtime = 130 minutes
| country = India
| language = Malayalam
| budget = 
}} Indian thriller film written and directed by B. Unnikrishnan and produced by Ronnie Screwvala and Siddharth Roy Kapur. Mohanlal stars as Chandrasekhar, an IPS officer who begins investigating a series of murders after receiving an anonymous note. The plot of the film is partly inspired from the Agatha Christie novel The A.B.C. Murders. The supporting cast includes Narain (actor)|Narain, Priyamani, Anoop Menon, Babu Antony and Jagathy Sreekumar.
 Malayalam films available for internet video streaming in the U.S. and Canada through Netflix. The film got positive reviews from the critics and was a commercial success at the box office.

==Plot==
Chandrasekhar (Mohanlal) is made the head of the Metro Crime Stopper Cell in Kochi. Chandrasekhar arrests Jerome, a psychologically deviant man and rescues three girls he had kidnapped, but his ex-wife Deepthi (Priyamani) manages to get Jerome sent to treatment in a psychiatric facility, rather than a prison sentence. It turns out that the three girls had been mocking Jerome, which had led to his actions of kidnapping them in the first place.

Chandrasekhar gets a congratulatory note from an anonymous man calling himself "Z" who challenges Chandrasekhar to figure out how he is going to bump off one target after the other. Chandrasekhar is forced to investigate a string of murders: coffee shop owner Alice (Fathima Babu), famous singer Beena Thomson (Roma Asrani) and leading businesswoman Chandrika Narayanan (Seetha (actress)|Seetha). These murders follow a mysterious alphabetical pattern, and each victim also has a cross slit on their forehead. Around the same time, Chandrasekhars daughter is preparing for a play under the lead of famous actor Mark Roshan (Arjun Nandakumar).

Chandrasekhar divorced three years earlier because Chandrasekhar discovered that Deepthi was using information from Chandrasekhar for professional gain. Deepthi confesses to Chandrasekhar that the three recently murdered women were all involved in the murder of Paul Mathew and made it look like a suicide. Chandrasekhar realises that Deepthi is the next target and comes with her to their daughters play where they meet a religious fanatic named Victor Rosetti (Babu Antony) who had been present in all three crime scenes, disguised as a salesman, and has been stalking Deepthi. The recurring theme of chess is obvious in the game between Chandrashekhar and the unknown Mr. Z. Chandrashekhar understands that there is a mastermind behind Victor Rosetti and decides to play a queens gambit in his own words. Chandrasekhar finally manages to corner Victor on the rooftop of a theater, Victor commits suicide by falling off the rooftop to the pavement. Then, Chandrasekhar questions Dr. Jacob and says that he has evidence that he is the killer of the three women, in a ploy to mislead the real killer. He has his men protect his daughter, but lets his wife into the school where the play was held. The real murderer to comes in, attempting to strangle Deepthi with a red scarf. Fortunately, Chandrasekhar stops him and then finally corners the real murderer on the stage. The murder turns out to be none other than Mark himself, Paul Mathews younger brother.

Mark confesses that he has held a grudge against the women for his brothers death, which Chandrashekhar had already known. Then, Mark also confessed that, disguised as a priest, he manipulated Victor, a schizophrenic patient, into believing that he had committed the murders himself. He had planned for Victor to commit the final murder on Deepthi during the play, but attempted to use the scenery of the fight between Chandrasekhar and Victor to complete his plan. In a last-minute attempt to avenge his brother, Mark tries to jump from the stage to murder Deepthi with a dagger, but is shot to death by Chandrasekhar, thus saving Deepthis life. The film then ends with Chandrasekhar and Deepthi, along with their daughter, finally together again in their house.

==Cast==
* Mohanlal as Chandrasekhar IPS
* Priyamani as Adv. Deepthi
* Anoop Menon as Jacob Narain as Kishore
* Jagathy Sreekumar as Rashid
* Babu Antony as Victor Rosetti
* Arjun Nandakumar as Mark Roshan 
* Roma Asrani as Beena Thomson Siddique as Paul Matthew Devan as ADGP   Seetha as Chandrika
*Santosh Sleeba as Sub-Inspector
* Sreelekshmi as Dakshayini Chandrasekhar
* Riyaz Khan as Jerome Jacob
* Mithra Kurian as Bindya Matthew
* Rajshri Nair as Susan
* Manikuttan as Eby Kuriakose
* Ambika Mohan as Chandrasekhars mother
* Gayathri as Jeromes mother

==Production==
The producer said that its theme is very fresh and racy with lots of variety situations.  Grandmaster is the first production venture of Bollywood production company UTV Motion Pictures in Malayalam.  Priyamani, who had already performed in other acclaimed Malayalam films, was chosen to play the female lead after she expressed her desire to feature in a film alongside Mohanlal.     "This is an important role, but, from the looks of it, will not pair her romantically opposite him. Yet, she will be seen in many scenes opposite Mohanlal. This is going to be a role that requires someone who can set the screen on fire. So, the makers felt that she would be a good bet, considering her previous releases in Mollywood," says a source. 

Shooting of the film started December 2011 and was carried out primarily at Kochi and Ottapalam.

==Soundtrack==
{{Infobox album|  
| Name = ഗ്രാന്റ്മാസ്റ്റര്‍
| Longtype =
| Type = Soundtrack
| Artist = Deepak Dev
| Cover = Grandmaster film.jpg
| Released = 4 April 2012
| Recorded = 2012 Feature film soundtrack
| Length = 25:04 Malayalam
| UTV
| Producer = Deepak Dev
| Reviews =
| Last album = Padmasree Bharat Dr. Saroj Kumar (2012)
| This album = Grandmaster (2012)
| Next album = 101 Weddings (2012)
}}
The soundtrack of the film was composed by Deepak Dev. It was released on 4 April 2012 at Peoples Plaza in Kochi.

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 25:04
| lyrics_credits = yes
| title1 = Akaleyo Nee
| lyrics1 = Chitoor Gopi
| extra1 = Vijay Yesudas
| length1 = 4:38
| title2 = Aaranu Nee
| lyrics2 = Chandra Shekar
| extra2 = Suchithra
| length2 = 4:52
| title3 = Doore Engo Nee
| lyrics3 = Hari
| extra3 = Sanjeev
| length3 = 4:25
| title4 = Aaranu Nee (Club Mix)
| lyrics4 = Chandra Shekar
| extra4 = Suchithra
| length4 = 4:14
| title5 = My Moment
| lyrics5 = Deepak Dev
| extra5 = Krishna Kumar
| length5 = 2:02
| title6 = The Grandmaster Theme
| lyrics6 =
| extra6 = Instrumental
}}

==Release==
Grandmaster was expected to be released on 27 April 2012, but was postponed. The movie was released on 3 May 2012. Manorama Online and Indiavision TV also reported that the film was a Hit.   Boxofficeverdicts commented that Grandmaster may have lost the buzz around it during past couple of weeks, but shall prove to be a profitable venture for its distributors, with a bit of help from the satellite rights amount it fetched. According to reports, Surya TV purchased the rights for  ; however, in the initial stages it was widely believed that the price was much lesser.  The film earned   in its 60 day theatrical run and box office verdicts it above average as the movie, with the distributor share crossing  . In Vue it was released on 29 July.

==Critical reception==
Rediff.com rated the film 3.5 out of 5. Sifys critic rated the movie as "watchable" (3.5/5) and said, "Grandmaster easily turns out to be an engaging watch". 

==Box office== Diamond Necklace.

==Awards==
; Kerala State Film Award Best Male Singer - Vijay Yesudas

; Asianet Film Awards Best Actor - Mohanlal

; Asiavision Movie Awards
* Best Actor - Mohanlal

; 2nd South Indian International Movie Awards
* Best Debutant Producer - Siddharth Roy Kapur, Ronnie Screwvala

==References==
 

==External links==
*  
* Grandmaster at  

 

 
 
 

 