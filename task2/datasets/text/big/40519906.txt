The Violent Kind
{{Infobox film
| image          = 
| alt            = 
| caption        = 
| director       = The Butcher Brothers
| producer       = {{plainlist|
* Michael Ferris Gibson
* Jeffrey Allard
* Andy Gould
* Jeremy Platt
* Malek Akkad
}}
| screenplay     = {{plainlist|
* The Butcher Brothers
* Adam Weis
}}
| starring       = {{plainlist|
* Cory Knauf
* Taylor Cole
* Bret Roberts
* Christina Prousalis
* Tiffany Shepis
* Joseph McKelheer
* Samuel Child
* Joe Egender
}}
| music          = Joshua Myers
| cinematography = James Laxton
| editing        = Nic Hill
| studio         = San Francisco Independent Cinema
| distributor    = Image Entertainment
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Violent Kind is a 2010 American horror film directed by The Butcher Brothers (Mitchell Altieri and Phil Flores), written by The Butcher Brothers and Adam Weis, and starring Cory Knauf, Taylor Cole, Bret Roberts, Christina Prousalis, Tiffany Shepis, Joseph McKelheer, Samuel Child, and Joe Egender.  It is about a group of bikers who go to a remote cabin to party, only to have several people become possessed.  Reviews were generally negative.

== Plot ==
Cody, Q, and Elroy are second-generation bikers and drug dealers who plan to visit Codys family at a remote cabin for his mothers birthday party; Shade, Qs girlfriend and Codys cousin, joins them.  When they arrive, they meet Codys ex-girlfriend, Michelle, and her younger sister, Megan, who has a crush on Cody.  After the party, Michelle and her husband take off, which strands Megan at the cabin with Cody, Q, Elroy, and Shade.  However, despite everyone else having left the party, Megan sees people outside the cabin and tries to get a ride from them.  Having no luck locating the people she saw, she returns to the cabin and finds her sister bloody and near death.  Michelle begs for help and collapses into unconsciousness before they can get any answers from her.  At the same time, the cars and cell phones stop working.  As the others attempt to figure out a plan, Elroy sexually assaults Michelle, but she surprises him by suddenly waking up and responding positively to him.  As they begin to have sex, Michelle bites Elroy hard and tears away a piece of flesh from his neck.  Elroy screams for help and tries to defend himself from Michelle as she tears into him.  The others pull Michelle off Elroy, who is now heavily wounded, and, unsure what to do, they bind Michelle to the bed with tape.

Cody and Megan set off to find help, while Shade attempts to communicate with Michelle, who she believes is possessed.  During the conversation, Michelle loses control and attacks Shade.  Shade barely escapes, and Q shoots through the door with a rifle.  Michelle hides on the ceiling and attacks Q when he enters the room.  Meanwhile, Cody and Megan discover that their closest neighbor is dead, and his wall is littered with missing persons reports that date back to the 1950s.  They return to the cabin and discover the aftermath of Qs fight with Michelle: Michelle has disappeared, and Q wants to give up on finding her.  Megan is outraged that Q would try to kill her sister, and Cody insists that they stay to help Elroy and find Michelle.  Q and Cody come to blows, and Q leaves alone after unsuccessfully trying to persuade Shade to accompany him.  As Q walks down the road, he meets Michelles dead husband, who is now alive again and talking about hearing strange, beautiful music.  At the same time, Elroy, who is in the cabin, also mentions hearing music.  Elroy and Michelles husband both explode as their bodies are overcome in a blinding light.

Vernon, Jazz, and Murderball, psychopathic greaser (subculture)|greasers, appear at the cabin, take everyone hostage, and begin torturing them for information.  Vernon sadistically toys with Cody, demanding to know where Michelle is and hinting that she is critically important to plans that involve a cataclysmic end to humanity.  Vernon and Jazz leave the cabin momentarily to bring in Qs bound body, and Vernon proceeds to repeatedly stab Q with a switchblade.  When Shade protests, Vernon orders Jazz to kill her.  Outraged, Cody and Q overpower Vernon and shoot him with Qs rifle, but it has no effect.  Murderball kills Q, and Vernon reveals that he knew Michelles location the whole time; he just wanted to torture them for the fun of it.  Vernon claims to have been hiding in a human body for the past 60 years and to be originally from a void beyond time and space that was the inspiration for human myths about heaven and hell.  After he completes a ritual involving Michelle, Vernon releases Cody and Megan, saying that he likes them and pities their fate.  As Cody and Megan flee to a nearby town, they see the people around them dropping dead, and the sky darkens ominously.

== Cast ==
* Cory Knauf as Cody
* Taylor Cole as Shade
* Bret Roberts as Q
* Nick Tagas as Elroy
* Christina Prousalis as Megan
* Tiffany Shepis as Michelle
* Joseph McKelheer as Jazz
* Samuel Child as Murderball
* Joe Egender as Vernon

== Production ==
According to the directors, the film was meant to be a homage to 1970s films that they had grown up enjoying.  They decided to include the best elements of several different genres, including biker films and horror films.   The film was shot in 21 days, often filming all night. 

== Release ==
The Violent Kind premiered at the 2010 Sundance Film Festival.     Image Entertainment bought distribution rights to the film in January 2011, and it received a limited theatrical release.   It was released in the UK on July 22, 2011.   

== Reception == Time out London rated the film 1/5 stars and called it a "mess of half-digested influences".   Writing for Total Film, Matt Glasby rated the film 1/5 stars and criticized it as confusing and poorly scripted.   Geoffrey Macnab of The Independent rated the film 1/5 stars and called it incomprehensible mashup of biker film and torture porn.   Dread Centrals Andy Mauro rated it 2/5 stars and stated that the film has too much unintentional humor to be taken seriously.   Ryland Aldrich of Twitch Film called it fun and a "pinnacle of the WTF?! genre".   In a positive review for Brutal as Hell, Annie Riordan stated that the films lack of explanations is creative and keeps audiences guessing. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 