Foolish Heart (1998 film)
{{Infobox film
| name           = Foolish Heart
| image          = Corazon Iluminado1.jpg
| caption        = Theatrical release poster
| director       = Héctor Babenco
| producer       = Héctor Babenco Oscar Kramer
| writer         = Héctor Babenco Ricardo Piglia
| starring       = Miguel Ángel Solá Maria Luísa Mendonça
| music          = Zbigniew Preisner
| cinematography = Lauro Escorel
| editing        = Mauro Alice
| distributor    = Columbia TriStar Sony Pictures
| released       =  
| runtime        = 132 minutes
| country        = Argentina Brazil France
| language       = Spanish
| budget         =
}} French drama film directed by Héctor Babenco. The screenplay was written by Babenco and Ricardo Piglia. The film stars Miguel Ángel Solá, Maria Luísa Mendonça, and others. 

The picture is based on the story by Catulo Castillo.

==Plot==
The film tells of seventeen-year-old Juan (Walter Quiroz). He lives with his parents and spends time with several intellectuals who are interested in  photography.  The girlfriend of the groups money person is Ana (Maria Luísa Mendonça), and Juan is attracted to her.

Ana spent two years at a mental institution because she was considered "crazy", yet Juan sees Ana often.

Juan is training as a door-to-door salesman, but when a photographer gives him a viewfinder, it changes his life.  Hes put on the path to his later success as a Hollywood director.

==Cast==
* Miguel Ángel Solá as Juan (adult)
* Maria Luísa Mendonça as Ana
* Walter Quiroz as Juan (young)
* Xuxa Lopes as Lilith
* Norma Aleandro as Mother
* Villanueva Cosse as Father
* Oscar Ferrigno Jr. as Martin
* Alejandro Awada
* Luis Luque

==Distribution==
The film was first presented at the 1998 Cannes Film Festival in May.

===Release dates===
* Brazil: November 13, 1998
* Argentina: December 3, 1998
* France: November 17, 1999

==Awards==
Nominations
*  ; Héctor Babenco; 1998.   
* Argentine Film Critics Association Awards: Silver Condor; Best Actress, Maria Luísa Mendonça; 1999.
* Cinema Brazil, Petrópolis, Rio de Janeiro, Brazil: Cinema Brazil Grand Prize; Best Actress, Maria Luísa Mendonça; Best Cinematography, Lauro Escorel; Best Director, Héctor Babenco; 2000.

==References==
 

==External links==
*  
*   at the cinenacional.com   La Nación by Claudio España  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 