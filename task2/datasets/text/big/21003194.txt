Tales That Witness Madness
 
 
{{Infobox film
| name           = Tales That Witness Madness
| image          = Geschichten, die zum Wahnsinn führen Poster.jpg
| caption        =
| director       = Freddie Francis
| producer       = Norman Priggen
| writer         = Jennifer Jayne (as Jay Fairbank)
| narrator       =
| starring       = Donald Pleasence Joan Collins Kim Novak Jack Hawkins
| music          = Bernard Ebbinghouse
| cinematography = Norman Warwick
| editing        = Bernard Gribble
| studio         = World Film Services 
| distributor    = Paramount Pictures
| released       = 31 October 1973
| runtime        = 90 minutes
| country        = United Kingdom English
| budget         =
}} British horror film produced by Norman Priggen, directed by veteran horror director Freddie Francis, and written by actress Jennifer Jayne.
 Torture Garden Tales from the Crypt (1972), The Vault of Horror (film) (1973), From Beyond the Grave (1973) and The Monster Club (1980). These portmanteau horror films were all produced by Amicus Productions. Tales That Witness Madness is sometimes mistaken for an Amicus production, however it was actually produced by World Film Services. Ed. Allan Bryce, Amicus: The Studio That Dripped Blood, Stray Cat Publishing, 2000 p 93 

==Plot==
In the Framing Story, a psychiatrist in a high tech modern mental asylum, Dr. Tremayne (Donald Pleasence), discusses a bold new psychiatric theory with colleague Dr. Nicholas (Jack Hawkins). Tremayne uses four patients who went insane - Paul, Timothy, Brian, and Auriol - to illustrate the theory, presenting each in turn to Nicholas:
 David Wood as Pauls tutor Phillipe.

In Penny Farthing, Timothy (Peter McEnery) an antique store owner, stocks a strange portrait of "Uncle Albert" (Frank Forsyth) and a penny farthing bicycle. In a series of episodes, Uncle Albert compels Timothy to mount the bicycle, and he is transported to an earlier era where he observes the apparently mourning Beatrice (Suzy Kendall), who was young Alberts love interest. These travels place Timothys girlfriend Ann (also Suzy Kendall) in peril. Features Neil Kennedy and Richard Connaught as the removal men.

In Mel, Brian Thompson (Michael Jayston) brings home an old dead tree, which he lovingly calls Mel, mounting it in his modern home as a bizarre piece of found object art. He increasingly shows unusual attention to Mel, angering his jealous wife Bella (Joan Collins).

In Luau literary agent Auriol Pageant (  as Vera.

In the Epilogue, Nicholas has Tremayne himself declared insane, apparently for believing the patients bizarre accounts, but manifestations of the patients histories materialize, and "Mr. Tiger" kills Nicholas.

==Production== dubbed by Charles Gray in post-production.
(Cavett Binion, All Movie Guide)  This was Hawkins final film appearance. 

Kim Novak broke a four year hiatus from films with her appearance in this film. She replaced Rita Hayworth shortly after production started. 

==Evaluation==

The Encyclopedia of Horror says the film "avoids farce and develops a nicely deadpan style of humour which is ably sustained by the excellent cast in which only Novak appears unable to hit the right note." Milne, Tom. Willemin, Paul. Hardy, Phil. (Ed.) Encyclopedia of Horror, Octopus Books, 1986. ISBN 0-7064-2771-8 p 284 

==References==
 

==External links==
* 
* 
*http://www.rottentomatoes.com/m/tales_that_witness_madness at Rotten Tomatoes
 

 
 
 
 
 
 
 
 
 
 