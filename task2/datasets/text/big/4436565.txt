Gadjo dilo
{{Infobox film 
|  name           = Gadjo dilo 
|  image=Gadjo dilo.jpg|
|  producer       = Doru Mitran
|  writer         =  Tony Gatlif Jacques Maigre Kits Hilaire 
|  starring       = Romain Duris Rona Hartner Izidor Serban Ovidiu Balan Angela Serban Adrian Simionescu
|  director       = Tony Gatlif 
|  editing        = Monique Dartonne
|  music         = Tony Gatlif
| cinematography    = Eric Guichard CNC Princess Films Canal+ Lions Gate Films (USA)
|  released       =  
|  runtime        = 102 minutes  Romani Romanian Romanian French French 
|  country        = France 
|  budget         = 
| gross          = $673,153 (US) 
}}
 Gadjo  or crazy stranger" in Romani language|Romani. The Romanian title of the film is Străinul nebun ("The crazy foreigner").  
Most of the film was shot at the village of Creţuleşti some kilometers from Bucharest and some of the actors are local Romani people.

==Plot==

Stéphane, a young French man from Paris, travels to Romania. He is looking for the singer Nora Luca, whom his father had heard all the time before his death. Wandering along a frozen road, he meets old Izidor, a Rom (Gypsy) and tries tell him of Nora Luca. Drunken Izidor only hears the handful of Romani words and takes Stephane to his village, determined to teach the boy the Romani language. Stéphane believes that Izidor will take him to Nora Luca when the time has come, so he lives in the Roma village for several months in Izidors house, as Izidors son Adriani has been arrested. Izidor is happy to have him as a guest, calling him "his Frenchman" and fixing the young wanderers worn-out shoes. The other Roma dislike Stephane at first, insulting him in their language and believing him to be a lunatic, tricking him into saying rude words and even into entering a tent where women are bathing. Stéphane gradually wins them over by showing his respect for their music and culture and is rewarded with an intimate look into every aspect of Roma life, from a raucous wedding to a bittersweet funeral. The only person in the village who speaks any French is the tough Sabina, a divorced Romani dancer who is blatantly hostile towards him at first, but the pair eventually bond through a series of trips across the countryside to record traditional Romani music.

One day, just as the pair are beginning to make love for the first time, Adriani returns after many months in jail. The village rejoices, but the men soon have to leave to work at a performance and the two lovebirds sneak off to be together. While they are away, Adriani goes to the local bar, where he murders a man that he accuses of being responsible for his imprisonment. Adriani escapes, but a mob follows him and burns the village to the ground, killing Adriani. Sabina and Stephane return to find the smoldering ruins and are both devastated. They hurry to the concert venue and tell Izidor, who races outside and begs the earth to open and reunite him with his son.

Stephane leaves the village drives to the mile marker where the film opened. Grief-stricken, he smashes all the tapes he recorded during his travels with Sabina on the stone marker and buries them. He then drinks from a vodka bottle, spills some on the "grave" of the tapes, lays the bottle of the "grave" and then dances as Izidor did at his friends funeral. A shot of the back of the car reveals Sabina sleeping in the back seat. She wakes up, notices the impromptu "funeral" that Stephane is holding and smiles before the screen fades to black.

==Cast==
*Romain Duris as Stephane.
*Rona Hartner as Sabina.
*Izidor Serban as Isidor.
*Ovidiu Balan as Sami.
*Angela Şerban as Angela.
*Adrian Minune as the child prodigy.
*Mónika Juhász Miczura provides the voice of Nora Luca (unseen in the film).

==Themes==
Prejudice and racism is a major theme in Gadjo Dilo. The Romani people, so often accused of numerous crimes and demonized by outsiders, are shown as predominantly positive, yet wronged figures. They at first fear Stephane, accusing him much in the way they themselves are accused by the local non-Roma, fearing he will steal from them or kidnap the children (both are classic crimes associated with Romani,"Gypsy" people). Initially, their fears seem absurd, yet it becomes clear that these are the very prejudices that Romani people must endure every day.

Music plays another major role in the film, echoing Gatlifs magnum opus, Latcho Drom. Stephane cannot communicate with the Roma without help and vice versa, but his passion for their music helps the lack of communication and aids in healing their prejudices.

==Awards==
*  
* César Awards|César Award for the best Music at the 24th César Awards in 1999
* In   in 1998.

==Connections==
In 2006, Gatlif directed Transylvania (film)|Transylvania.
This time it is an Italian woman who travels from France to Romanian Transylvania to find her lover, a Gypsy musician.

==Notes==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 