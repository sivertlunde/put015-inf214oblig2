Affair in Havana
{{Infobox film
| name           = Affair in Havana
| image          = Affair in havana 1957 poster small.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Laslo Benedek	
| producer       = Richard Goldstone	
| screenplay     = Maurice Zimm
| narrator       = 
| starring       = John Cassavetes Raymond Burr Ernest Gold
| cinematography = Alan Stensvold
| editing        =  Allied Artists Pictures Dudley Productions
| distributor    = Allied Artists Pictures
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Affair in Havana (1957) is a crime film noir directed by Laszlo Benedek and written by Maurice Zimm. It stars Raymond Burr and John Cassavetes. 

The film is about a piano player who falls in love with a crippled mans wife.

==Plot==
Mallabee is a millionaire sugar-cane grower in Cuba who blames his wife, Lorna, for an accident that has left him in a wheelchair.

Lorna has been having an affair with Nick, a piano player in a Havana nightclub. Mallabee secretly is aware of this, having hired a private investigator to follow his wife.

The twisted mind of Mallabee has come up with a scheme in which Lorna kills him. She wont do it, but a trusted servant, Valdes, does cause his death by drowning. But the relationship between Nick and Lorna comes to an unhappy end.

==Cast==
* John Cassavetes as Nick
* Raymond Burr as Mallabee
* Sara Shane as Lorna
* Lilia Lazo as Fina
* Sergio Pena as Valdes
* Celia Cruz as Singer
* Jose Antonio Rivero as Rivero
* Miguel Angel Blanco as Captain of Police

==References==
 

==External links==
*  
*  
*  
*   featuring Celia Cruz

 

 
 
 
 
 
 
 
 
 


 