Romantic Warriors II: A Progressive Music Saga About Rock in Opposition
{{Infobox film
| name           = Romantic Warriors&nbsp;II: A Progressive Music Saga About Rock in Opposition
| image          = RomanticWarriorsII DVDcover.jpg
| image_size     = 240
| caption        = DVD cover
| director       = Adele Schmidt José Zegarra Holder 
| producer       = Mike Potter 
| writer         = Adele Schmidt José Zegarra Holder 
| narrator       = Roland Millman 
| starring       = {{Plainlist|
*Chris Cutler Bob Drake
*Franco Fabbri
*Giorgio Gomelsky
*Marc Hollander Mike Johnson
*Dave Kerman
*Carla Kihlstedt
*Marcello Marinone
*Ferdinand Richard Christian Vander
*Joris Vanvinckenroye
*Dave Willey
*Francesco Zago 
}}
| music          = {{Plainlist|
*Aranis
*Art Zoyd
*Etron Fou Leloublan Guapo
*Hamster Theatre
*Henry Cow Magma
*Miriodor
*Once Upon a Time in Belgium Present
*Ruins Ruins Alone
*Samla Mammas Manna
*Sleepytime Gorilla Museum
*Stormy Six
*Thinking Plague
*Univers Zero
*Yugen 
}}
| cinematography = Adele Schmidt José Zegarra Holder 
| editing        = 
| distributor    = Zeitgeist Media 
| released       =  
| runtime        = 98 min. 
| country        = United States 
| language       = English French (English subtitles)
| budget         =
}}

Romantic Warriors&nbsp;II: A Progressive Music Saga About Rock in Opposition is a 2012 feature length documentary film about the Rock in Opposition movement of the late 1970s, the music genre it spawned, and the influence it has on experimental groups across the world. The film was written and directed by Adele Schmidt and José Zegarra Holder, and was released in the United States by Zeitgeist Media.  It premiered in Washington, D.C. on September 28, 2012.  The film was generally well received by critics, with a reviewer at AllMusic saying that it "covers all the points an aficionado could possibly want". 

Romantic Warriors&nbsp;II is a sequel to Zeitgeist Medias 2010 documentary on  , comprising additional material filmed during the making of Romantic Warriors&nbsp;II.  A third installment in the series, Romantic Warriors III: Canterbury Tales was released in April 2015.

==Background==
Adele Schmidt and José Zegarra Holder are co-founders of Zeitgeist Media LLC, a video production company based in   and the 2009 Telly Award.  

Romantic Warriors&nbsp;II took two years to make, and was started by Schmidt and Holder soon after the release of its predecessor, Romantic Warriors in 2010. 

==Synopsis== Magma is also featured, and while never a member of RIO, the film shows how Magma operated outside the music industry and were a big influence on RIO.

In 1979 three more bands were added to RIO, Aksak Maboul (Belgium), Art Zoyd (France) and Art Bears (England, ex-Henry Cow members). By 1980 RIO had stopped functioning as an organization. Cutler adds that RIO had made its point, but that the support structures it had established continued to operate, and new bands were formed and functioned in the RIO tradition. Independent record labels sprang up to continue the work of RIO by promoting unknown and obscure musicians, including Recommended Records, formed by Cutler in England, Crammed Discs by Marc Hollander of Aksak Maboul in Belgium, and Cuneiform Records by Steve Feigenbaum in the United States.
 Mike Johnson of Thinking Plague both cite Henry Cow as having an influence on their music, and Carla Kihlstedt of Sleepytime Gorilla Museum explains the effect Fred Frith, Professor of Composition and former Henry Cow member, had on her music at Mills College, California. 
 Present and Aranis in Belgium. Present was started by some of the members of Univers Zero, and Aranis cited Univers Zero as an influence. Kerman later joined Present, and in 2011, Univers Zero, Present and Aranis joined forces to create a 17-member acoustic chamber rock group called Once Upon a Time in Belgium.
 Ruins Alone, Univers Zero, Sleepytime Gorilla Museum, Miriodor and Yugen. The film features performances by some of the bands at the 2011 festival, including the premiere of Once Upon a Time in Belgium.

==Reception==
{{Album ratings
| rev1 = AllMusic
| rev1Score =   
| rev2 = Babyblaue Seiten
| rev2Score =   
| rev3 = Educational Media Reviews Online
| rev3Score = highly recommended 
| rev4 = Prog-Sphere
| rev4Score = favorable 
| rev5 = The Rocktologist
| rev5Score = 9/10 
}}
Dave Lynch at AllMusic wrote that in this film Zeitgeist does a good job in explaining why interest in Rock in Opposition, and the music associated with it, has persisted to this day since its appearance in the late 1970s. Lynch said that while the documentary is "smartly paced and covers all the points an aficionado could possibly want", he lamented the fact that there was only space for short excerpts of the often complex music.  
 University of Maryland, wrote in a review at Educational Media Reviews Online that Romantic Warriors&nbsp;II is "far superior to its predecessor in terms of scope and focus".   He said that the "extensive and generous performance footage" gives the viewer a good feel for the music, and makes up for the sometimes "vague and ineffectual" descriptions by the narrator.  While Novara felt that the quality of some of the performance footage and interviews where Skype was used are lacking, he said that overall the film "is quite inspiring and informative for those interested in exploring the outer edges of rock music   certainly has a place in any library supporting popular music studies."  

A review in The Rocktologist magazine said that the film "does an excellent job in explaining the philosophy behind the movement", and that it is " efinitely a must see for any fan of innovative, forward-thinking and challenging music".  The Prog-Sphere website called Romantic Warriors&nbsp;II "an outstanding piece of filmmaking", and said that it will appeal to a wider audience than the first film because " he music’s very combination of the arty, the quirky and the academically austere will attract people who appreciate forms of non-mainstream music that do not necessarily fall under the "progressive rock" umbrella – including modern classical." 

==References==
{{reflist|2|refs=

  |publisher=AllMusic|accessdate=2013-03-15}} 

  |publisher=AllMusic|accessdate=2013-03-15}} 

  |publisher=AllMusic|accessdate=2013-03-19}} 

   

   

   

   

   

   

   

 Romantic Warriors&nbsp;II: A Progressive Music Saga About Rock in Opposition. Washington, D.C.: Zeitgeist Media (2012). 

   
}}

==External links==
* 

 
 
 
 