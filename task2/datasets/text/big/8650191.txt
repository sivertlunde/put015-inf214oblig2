Think of Me First as a Person
{{Infobox Film
| name           = Think of Me First as a Person
| image          = 
| image_size     = 
| caption        = 
| director       = George Ingmire
| producer       = 
| writer         = 
| narrator       = 
| starring       = Dwight Core (voice) Dwight Core Jr.
| music          = 
| cinematography = Dwight Core
| editing        = George Ingmire
| distributor    =  2006 (Originally shot till 1975)
| runtime        = 
| country        = U.S.A. English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} home movie about Dwight Core, Jr., a boy with Down syndrome. The footage was originally shot throughout the 1960s and 70s by Cores father, Dwight Core, Sr. The footage was later discovered and completed by the filmmakers grandson, George Ingmire.

The film was first shown at New Orleans 2006 Home Movie Day. Later that year, it was selected for preservation in the United States National Film Registry, an honor bestowed every year to twenty-five films deemed "culturally, historically, or aesthetically significant." The Library of Congresss statement announcing the 2006 additions to the Registry called the film a "loving portrait by a father of his son with Down syndrome" that represented "the creativity and craftsmanship of the American amateur filmmaker." 

The films title comes from the 1974 Rita Dranginis poem of the same name.

==Notes==
 

==External links==
* 
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 


 