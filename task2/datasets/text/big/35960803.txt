I (film)
 
{{Infobox film
| name           = I 
| alt            = Theatrical poster
| image          = I film poster.jpg
| caption        = Theatrical poster Shankar
| producer       = Viswanathan Ravichandran|V. Ravichandran D. Ramesh Babu Subha
| Vikram Amy Jackson Suresh Gopi
| music          = A.R. Rahman
| cinematography = P. C. Sreeram Anthony
| Aascar Film
| distributor    = Aascar Film
| released       = 14 January 2015
| country        = India
| runtime        = 188 minutes  Tamil
| budget         =     
| gross     =   {{cite web|url=http://andhraboxoffice.com/info.aspx?id=765&cid=6&fid=878 
}}
 Tamil romance romantic thriller thriller film Shankar and Vikram and Santhanam and soundtrack and film score were composed by A. R. Rahman. Production design was handled by T. Muthuraj. P. C. Sreeram was the films cinematographer and editing was done by Anthony (film editor)|Anthony. 
 Telugu and Hindi along with the Tamil version.  

The film, released on 14 January 2015, tells the story of a body builder turned super model, played by Vikram, who after being deformed, exacts revenge upon those who turned him into a hunchback.  The film received mixed reviews from critics, who praised Vikrams performance and the computer-generated imagery, but criticised the writing and the films excessive length.Despite mixed reviews from critics,The film become the first blockbuster film in India to cross 200 crore in 2015. 

==Plot== infatuated by Diya (Amy Jackson), a leading supermodel. Diya is soon blacklisted and all her advertisement film contracts are cancelled by popular model John (Upen Patel), her co-star in all her advertisements, after she had constantly refused to have sex with him. To save her career, Diya decides to replace John with Lingesan, whom she had earlier met at one of her shoots, as her co-star for her next advertisement. Lingesan agrees, sacrificing his Mr. India dreams in the process.

Lingesan is given a makeover by Diyas stylist, a transgender woman Osma Jasmine (Ojas Rajani). The shoot initially does not go well as Lingesan is shy and awkward around Diya. On her directors advice, Diya pretends to fall in love with Lingesan so that he would loosen up and perform better during the shoot. The plan proves successful and gradually both take the modelling world by storm as the lead pair. As time passes, Diya reciprocates Lingesans love for her and they soon get engaged.

In this journey from a bodybuilder to a top model, Lingesan makes many enemies, including John, whose modelling career was ruined due to Lingesans rising popularity, forcing him to appear in local advertisements to stay afloat; Osma, who was infatuated by Lingesan and proposed to him, only to be rejected; a wealthy industrialist Indra Kumar (Ramkumar Ganesan), whose company suffered losses when Lingesan refused to endorse their soft drink as it was reported to contain pesticides; and another bodybuilder Ravi (M. Kamaraj), who also competed for the Mr. Tamil Nadu title but lost to Lingesan. These four people conspire to take revenge on Lingesan and destroy his fame and modelling career.

Two days before his wedding, Lingesan slowly starts to experience hair and teeth loss and a skin disorder. He consults his friend Dr. Vasudevan (Suresh Gopi), who is also the guardian of Diyas family. Vasudevan informs him that he is suffering from a rare and incurable genetic disease which results in premature ageing. Lingesans condition worsens, with his face and body Deformity|deforming, and eventually he becomes a hunchback. In his current condition, Lingesan decides to disappear from public eye and also from Diyas life by faking his death in a car accident. Only his close friend Babu (N. Santhanam|Santhanam) and Vasudevan know that he is alive. Lingesan asks Vasudevan to marry Diya as he is the only person who will be able to understand Diyas past and present situation. Vasudevan reluctantly agrees and the wedding is fixed.
 insatiable lust for Diya since she was a teenager and was enraged when she chose Lingesan over him. Therefore, he sided with Lingesans enemies and planned the entire operation to get Lingesan injected with the virus. An enraged and betrayed Lingesan abducts Diya on her wedding day and keeps her safely in an old house without revealing his identity to her.
 sprout huge swell up. ayurvedic treatment for his condition and eventually returns to his normal self.

== Cast == Vikram as Lingesan (Lee)
* Amy Jackson as Diya Santhanam as Babu
* Suresh Gopi as Dr. Vasudevan
* Upen Patel as John
* Ramkumar Ganesan as Indra Kumar
* Ojas Rajani as Osma Jasmine
* M. Kamaraj as Ravi
* Azhagu as Lingesans father
* T. K. Kala as Lingesans mother
* Mohan Kapoor as an ad film-maker Srinivasan as Keerthivasan
* Sarath Kumar as himself (Cameo appearance)

==Production==
=== Origin === Sun TV, Vikram in the lead.

=== Development ===
  (pictured) for special effect and designing props for I.]]
 Suresh and Martial arts Anl Arasu. Richard Taylors special effects and prop company Weta Workshop.    costume designing of the films cast.  According to Shankar, Azhagan and Aanazhagan were the alternate titles that were kept in mind, but since they were already used, he decided that a single alphabet would be the films title. Since I also meant beauty and explained the character of the protagonist, the alphabet was finalised to be the films title. 

In September 2014, producer and distributor Aascar V Ravichandran, stated the project would cost about  ,    however by October 2014, he and director Shankar was widely quoted as stating the budget would be less than  .            

===Casting=== Vikram was Hollywood actor Arnold Schwarzenegger was reported to play an award presenter for a brief scene. Ravichandran, however, denied the news, but said that he would appear in the audio launch of the film. To prepare for his role in I, Vikram took some weight loss and gain tips from Arnold as well.  Make-up artiste Ojas M. Rajani was reported to play the role of a transgender make-up artiste in the film.  Mr. Asia Contest|Mr. Asia 2014 Syed Siddiq appeared in the action sequences featuring Vikram. 

===Character looks===
 Vikram plays hunchbacked man character was the most difficult one to sketch. Vikram was confirmed to sport his hunchbacked get-up in most parts of the film.  The bubbles observed on the characters face were reported to be designed by Weta Workshop|Weta.  Amy Jackson plays a model in the film. On casting and re-defining her looks Shankar was quoted saying, "I wanted someone who could authentically look like a model and then fixed Amy Jackson".    Vikram also went bald for the film to change his looks in the film easier.  Santhanam was also asked to lose some weight for his character.  Peter Ming used actual cyclists in China for a cycle fight scene that was shot there.   

===Filming=== Hunan province. Li river Prasad labs in Vadapalani, Chennai where a set was erected by Muthuraj.  On 24 September 2014, Jackson confirmed that the shooting of the film was completed. 

===Post-production===
In February 2014, Vikram started dubbing for the first half of the film.  In March 2014, Vikram had completed his dubbing portions for the first half of the film.  On 3 April 2014, film critic and journalist Sreedhar Pillai stated that the films trailer was in stages of editing.    Patchwork and dubbing for the film was completed on 11 September 2014.   

==Themes and influences== Apoorva Sagodharargal (1989) while Gauthaman Bhaskaran of Hindustan Times stated that film had similarities to the novel The Hunchback of Notre-Dame (1831) and the fairy tale Beauty and the Beast (1756).      

==Music==
 
 background score Nehru Indoor Stadium in Chennai on 15 September 2014.  Arnold Schwarzenegger was the Chief Guest.  The Hindi and Telugu versions of the soundtrack album were released on 29 and 30 December 2014, respectively.

== Release ==
=== Distribution === Chinese and English along with the Tamil and the dubbed Telugu versions.    The distribution rights for the dubbed Telugu version were purchased for   by Mega Super Good Films.   The theatrical rights in Tamil Nadu were sold to Sushma Cine Arts. 

=== Marketing ===
The first look production poster of the film was unveiled on 15 July 2012.  Stills featuring the lead pair were released in March 2014.   The official motion poster teaser of the film was released on YouTube on 11 September 2014.  The motion poster crossed 1 million views by the third day of its release.  Two posters released on 12 September 2014, with J. Hurtado of Twitch Film labelling them as "out of this world". 

The producer screened a sneak preview of the teaser and the song "Ennodu Nee Irundhaal" for the critics in late August 2014. The teaser carried a dummy music score that was not composed by A. R. Rahman.   
 leaked on the internet on 2 September 2014. Although the leaked teaser lacked decipherability, the person who had leaked the teaser openly informed the team about it and challenged them to take action against him. After further inquiries, it was revealed that the culprit broke into producer Ravichandrans office during the night and had copied the teaser. However, Ravichandran refused to make changes to the leaked teaser, and planned to release it as per the original schedule. 
 Indian film to do so, beating the record set by the teaser of the Bollywood film, Bang Bang! (2014), which crossed 1 million views in 13 hours.  It reached 7 million views in 2 weeks after its release, becoming the only Tamil film to cross the mark till date. 

The film was promoted by Vikram on Bigg Boss 8. 

=== Home media ===
The media rights of the film were purchased by Jaya TV for  . The media rights included satellite rights and ringtones.   

==Reception==

===Critical response===
I received mixed to positive reviews from film critics.  Subhash K. Jha gave 4 stars out of 5 describing I as the "most exceptional" film from Shankar and wrote that it "takes us beyond the imaginable and the conceivable, fusing with fabulous flamboyance the fantasy element with a level of heightened reality thats commercial cinemas forte".  Filmfare also gave it 4 stars out of 5, stating that "Shankar balances a social critique along with technical gimmickry and here the message centres about our obsession with physical perfection and beauty."  Komal Nahta felt that Shankars direction was "first rate" and wrote "His vision is unique and the translation of his vision on to the celluloid is brilliant. He has kept the narration so interesting that the viewers eyes remain glued to the screen. He has given the film a huge canvas and has made it an audio-visual treat.  Rajeev Masand from IBN Live gave 3 out of 5 and wrote, "I from visionary Tamil director Shankar is a work of staggering ambition, somewhat weighed down by the filmmakers own indulgence...(it) may be far from perfect, but for the most part its pretty entertaining stuff".  The Times of India gave it 3.5 stars out of 5 and wrote, "Shot mesmerisingly by PC Sreeram on virgin locales in China and India, with world class CG work, this spectacle works because at the core, its a romantic-thriller told simplistically....This is pure escapist fare but will resonate with those who read fairy tales at bedtime".  Rediff gave 3.5 stars out of 5 and wrote, "The narrative lacks the pace; we usually associate with a Shankars film. But he does tell a beautiful tale of love sullied by jealousies, greed and anger. The intriguing screenplay as the director alternates between the light-hearted past and the thrilling present keeps you engrossed".  Behindwoods.com rated the film 3 out of 5 and stated, "though the story is predictable beyond a point, its an amalgamation of Masters at play which works to a larger extent because of their huge efforts".  India Today rated the film 3 out of and wrote "Its not everyday you will get to watch a visually rich movie like this".  

The Hindu stated, "Vikrams terrific performance is let down by an uninspired, exhausting movie".  Daily News & Analysis gave the film 3 out of 5 stars, too, calling it a "great looking film but with shoddy writing and poorly sketched characters".  Haricharan Pudipeddi, writing for Indo-Asian News Service|IANS, rated the film 2 out of 5 and said, "All thats big may not necessarily be great. Hope Shankar realises that much better films can be made on a smaller canvas and much lower budget."  Sify wrote, "I is definitely not the best of Shankar and he has to take the blame for poor writing. His story is predictable and there are no twists or scenes which keeps you engaged. Barring few eye-popping stunt scenes, a slew of beautiful unseen locations, breathtaking camera by PC Sreeram and few hummable songs by AR Rahman, I is very ordinary".  Deccan Chronicle gave the film 2.5 stars out of 5 and said, "While the movie and the effort is good technically, its content is just average".  Oneindia also rated the film 2.5 out of 5 saying, "For a movie which had so much of anticipation, I doesnt feed its audience enough to fulfill their expectations. The story is plain and doesnt look like a normal Shankar movie which would otherwise have interesting twists and turns."  Gautaman Bhaskaran, writing for The Hindustan Times, rated the film two and a half stars, stating that Vikrams performance was the films "only high point"; he noted that the film had similarities to the novel The Hunchback of Notre-Dame (1831) and the fairy tale Beauty and the Beast (1756), "without infusing any novelty into these age-old yarns."  Deepanjana Pal from Firstpost wrote, "I is too long, too stupid and too regressive to be entertaining" and also called it "superficial and the least fun Shankar film ever". 

Variety (magazine)|Variety magazines Ronnie Scheib wrote, "Shankars visual ingenuity keeps things zippy for much of the hefty 188-minute running time, and star Chiyaan Vikram delivers a knockout three-pronged performance, but this cinematic bravura is offset by underdeveloped scripting, flatly one-dimensional villains and overdone lone-hero-vs.-swarms-of-murderous-attackers setpieces".  Rachel Saltz from The New York Times wrote, "I is exuberant and unselfconscious but too cartoonish to engage your emotions. The onslaught of images and music will engage your senses, though, even as youre left giggling at the too-muchness of it all". Saltzjan, Rachel (14 January 2015)
  . The New York Times  J Hurtado from Twitch Film wrote, "I strains the boundaries of self-indulgence and modern tolerance in a way that has become something of a plague among Indian blockbusters. The film is amazing to look at, features a number of amazing set pieces, and some appropriately over the top action sequences, but even with everything including a few kitchen sinks thrown in, it may go on a bit too long for its own good". 

===Box office===
According to Box Office India, I earned around   nett in South India on its first day,    setting records in Kerala and Nizam/Andhra Pradesh|AP. The film grossed over   worldwide on its opening day.  The film went on to collect   nett in India in three days from all its versions.  

In its extended five-day weekend, the film earned   nett in India from all three versions, with the Hindi version netting  .  

The film grossed around   worldwide from all versions twelve days after its release. 

==Notes==
 
 

==References==
 

== External links ==
 
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 