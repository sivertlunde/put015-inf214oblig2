Another Time, Another Place (1983 film)
Another Time, Another Place is a 1983 British drama film directed by Michael Radford and starring Phyllis Logan, Giovanni Mauriello and Denise Coffey. 

==Plot==
In Scotland in 1943 during World War II, Janie (Phyllis Logan) is a young Scottish housewife married to Dougal (Paul Young), who is 15 years older. Participating in a war rehabilation program, the couple take in three Italian prisoners of war to work on their farm. Janie soon falls in love with one of the three, Luigi (Giovanni Mauriello). She begins a secret relationship with Luigi that is doomed from the start. 

==Cast==
* Phyllis Logan - Janie
* Giovanni Mauriello - Luigi
* Denise Coffey - Meg Tom Watson - Finlay
* Gianluca Favilla - Umberto
* Gregor Fisher - Beel Paul Young - Dougal
* Claudio Rosini - Paolo
* Jennifer Piercey - Kirsty
* Yvonne Gilan - Jess
* Carol Ann Crawford - Else
* Ray Jeffries - Alick
* Scott Johnston - Jeems
* Nadio Fortune - Antonio
* David Mowat - Randy Bob
* Colin Campbell - Accordionist
* John Francis Lane - Farmer
* Corrado Sfogli - Raffaello
* Peter Finlay - Officer
* Stephen Gressieux - Prisoner of War

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 