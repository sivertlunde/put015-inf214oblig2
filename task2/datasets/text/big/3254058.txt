You Can't Stop the Murders
 
 
{{Infobox film
| name           = You Cant Stop the Murders
| image          = YouCantStopTheMurders.jpg
| image_size     = 200px
| caption        =
| director       = Anthony Mir
| producer       =
| writer         = Anthony Mir
| narrator       =
| starring       = Anthony Mir Gary Eck  Akmal Saleh
| music          =
| cinematography =
| editing        =
| distributor    = Miramax Films/ Buena Vista International (Australia)
| released       = 2003
| runtime        =
| country        = Australia
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}

You Cant Stop the Murders is a 2003 Australian  comedy film, directed by Anthony Mir, and written by and starring Mir, Gary Eck and Akmal Saleh. The plot revolves around a series of Village People-themed murders in a small town, and the police who investigate the crimes.  The title is a satirical reference to the 1980 film Cant Stop the Music, in which the Village People star.
 French male stripper.

Gary and Akmal soon discover that the murders have a Village People theme, with those murdered having been in one of the occupations of a Village Person, or resembling one.  They fearfully deduce that either a policeman or a dentist (Akmal is uncertain, as he doesnt clearly remember the  Village People, although Gary quickly deduces that it is, in fact, a policeman) will be next to die, as does Tony, who rushes back from the city.

The movie stars a number of Australian stand up comedians including Jimeoin, Bob Franklin, The Umbilical Brothers, Garry Who, Haskel Daniel, Richard Carter, The Dickster, Rash Ryder, Kenny Graham and Sandman.

==Box Office==
You Cant Stop the Murders grossed $254,871 at the box office in Australia. 

==See also==
*Cinema of Australia

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 
 