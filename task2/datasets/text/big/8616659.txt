Dog Treat
{{Infobox film
| name = Dog Treat
| image =
| caption =
| imdb_rating =
| director = Wan Laiming Wan Guchan
| producer = Wan Laiming Wan Guchan
| writer =
| starring =
| music =
| cinematography =
| editing =
| distributor =
| released = 1924
| runtime =  Unknown
| country = China
| language =
| budget =
| preceded_by =
| followed_by =
| mpaa_rating =
| tv_rating =
}}

Dog Treat (Chinese: 狗請客) is a black and white Chinese animation made in 1924 by Wan Laiming and Wan Guchan.

==Translations==
The translation comes out to "Dog treating guests" or "Dog entertaining guests".  It does not translate to "Dog food".

==History==
It is a cartoon short produced under the "Chinese film company" (中華影片公司).  Since no other productions were ever made by this company, it is unknown as to whether it is actually a legitimate corporation.   The purpose of this clip is also up for debate, since it can either be in the form of an advertisement or animation experiment.

==See also==
*History of Chinese Animation
*Chinese Animation

==References==
 

==External links==
*  

 
 
 
 
 
 

 