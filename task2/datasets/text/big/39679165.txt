Kink (film)
{{Infobox film
| name           =  Kink
|image=  
|caption=The logo of the pornography company, where Kink was filmed.
| writer         = 
| starring       = 
| director       = Christina Voros  
| producer       = James Franco
| distributor    = 
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Kink is a 2013 American documentary, produced by James Franco about the BDSM website Kink.com.   

==Early production==
While filming scenes of About Cherry at the Kink.com San Francisco Armory, Franco noticed the dynamic between actors and the production crew. He stated that this interested him, as in some respects, it was a similar dynamic to that of the production at Saturday Night Live. It was this that led to Franco developing an interest in this aspect of the BDSM culture. After coaxing director Christina Voros to an interview at the Armory, she agreed to do the film.  The other reported influence for Francos decision to make this documentary, was an unsuccessful sextape, with his girlfriend. 

== People featured/appearing ==
* Peter Acworth
* Maitresse Madeline
* Chris Norris
* Van Darkholme
* James Deen
* Mr. Marcus
* Zoe Holiday
* Jon Jon
* Jessie Lee
* Tomcat
* Princess Donna
* Five Star
* Francesca Le
* Phoenix Marie

==Reception==
The movie received positive reviews from The Hollywood Reporter and Variety (magazine)|Variety.  

==References==
 

==External links==
*  

 
 
 
 
 
 

 