An Innocent Magdalene
 
{{Infobox film
| name           = An Innocent Magdalene
| image          = Dwan-allan-an-innocent-magdalene-1916.jpg
| caption        =
| director       = Allan Dwan
| producer       =
| writer         = D. W. Griffith (as Granville Warwick) Roy Somerville
| starring       = Lillian Gish
| music          =
| cinematography = Victor Fleming
| editing        =
| distributor    = Triangle Film Corporation
| released       =  
| runtime        = 50 minutes; 5 reels
| country        = United States
| language       = Silent with English intertitles
| budget         =
}} silent drama film directed by  Allan Dwan. It is considered to be a lost film. 

==Cast==
* Lillian Gish - Dorothy Raleigh
* Spottiswoode Aitken - Col. Raleigh
* Sam De Grasse - Forbes Stewart
* Mary Alden - The Woman
* Seymour Hastings - The Preacher Jennie Lee - Mammy
* William De Vaull - Old Joe

==See also==
* Lillian Gish filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 