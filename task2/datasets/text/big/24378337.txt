Fairy in a Cage
{{Infobox film
| name = Fairy in a Cage
| image = Fairy in a Cage.jpg
| image_size = 
| caption = Theatrical poster for Fairy in a Cage (1977)
| director = Kōyū Ohara   
| producer = Yoshiki Yūki
| writer = Oniroku Dan (story) Seiji Matsuoka (screenplay)
| narrator = 
| starring = Naomi Tani Hirokazu Inoue Rei Okamoto
| music = Hajime Kaburagi
| cinematography = Nobumasa Mizunoo
| editing = Atsushi Nabeshima
| distributor = Nikkatsu
| released = June 4, 1977
| runtime = 70 min.
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1977 Japanese pink film in Nikkatsus Pink film#Second wave (The Nikkatsu Roman Porno era 1971–1982)|Roman porno series, directed by Kōyū Ohara and starring Naomi Tani.

==Synopsis== Japanese military police, uses his position to falsely accuse, capture, imprison and torture women in whom he is interested. Namiji Kikushima, a high-class business woman, is one such lady. Kikushima is accused of using her Ginza jewellery shop to finance an anti-government organization. Judge Murayama has Lady Kikushima arrested, and orders the new recruit Taoka to torture her. Taoka falls in love with  Kikushima, at first hating torturing her then growing to enjoy it. After a long series of tortures and humiliation, while the Judge espouses his twisted version of morality, Taoka sacrifices himself to enable Lady Kikushimas escape.       Lewis, Graham R. "The Films of Koyu Ohara" in Asian Cult Cinema, #27 (2nd quarter 2000), p. 25. 

==Cast==
* Naomi Tani: Namiji Kikushima  
* Kazuo Satake: Keita Taoka
* Minoru Ōkōchi: Baron Murayama
* Hirokazu Inoue: Lieutenant Nishizaki
* Tatsuya Hamaguchi: Inoue 
* Rei Okamoto
* Reika Maki

==Background==
Lead actresses   (1977), and Rope Hell (1978), which also reunited her with Hirokazu Inoue, one of her torturers from Fairy in a Cage.  In contrast to these torture-themed films, Ohara had been successful with light, youth-centered comedies, such as his later Pink Tush Girl series. 

  in a 1998 interview. The scene was not "faked" with a suspension brace. ]]
In a review of the films of Kōyū Ohara for the journal Asian Cult Cinema, Graham R. Lewis notes that Ohara uses conflict between social classes as a theme in Fairy in a Cage. Both Lady Kikushima and Judge Murayama are members of the upper class, while Taoka, the new recruit ordered to torture Kikushima, is from the lower class. Murayama tells Taoka, "Some think I am a sick person, but this form of entertainment has always been reserved for the rich."  Taoka is repulsed by this philosophy, and when he comes to enjoy the torturing, sacrifices himself to release the prisoners. In a 2000 interview, Ohara confirmed this class-conflict theme, stating, "I think S&M is noble peoples fantasy. In the Roman Empire, the nobility were the spectators at the coliseum as they watched slaves in life threatening games. This is the reason I used a fancy silver chalice to collect the heroines urine in Fairy In A Cage." 

In a 1998 interview, Tani recalled that one scene in Fairy in a Cage was more physically demanding than some of the more notorious ones in Lady Black Rose (1978), which were simple tricks. "The worst scenes," she said, "are the ones where Im hung up-side-down and tortured with my legs spread, like in Fairy in a Cage... If you try to fake it by using a suspension brace, the thigh muscles wont show tension. On the other hand, if my legs are tied too loosely, Ill fall. These scenes always demand full-shots, so no trick is ever used. When they tie my torso, the rope technicians-- professional people-- are on the set. These guys put extra effort into tying beautifully and painlessly for me." Tani, Naomi. Interviewed by Hamamoto, Maki. (1998). "Naomi Tani - An Interview with Nikkatsus Queen of SM" (Conducted in January 1998 in Kyushu, Japan) in Asian Cult Cinema Number 19, April 1998. p. 43. 

Jasper Sharp comments that one of the most interesting aspects of Fairy in a Cage, and films of its genre, are their use of "shameful and historically sensitive" aspects of the World War II-era in Japan as subjects for sexually-oriented entertainment. He notes that this era would have been alive in the memories of many audience members at the time.  For explanations of this phenomenon, he quotes pink film director Yutaka Ikejima who theorizes that the sexual fantasies of this generation had been warped by the war experience. Sharp also quotes Japanese film scholar Donald Richie, who asserts that these films are no more than an outlet for the frustrations that males everywhere sometimes feel towards females. 

==Critical appraisal==
The commercial database Allmovie judges Fairy in a Cage to be one of Nikkatsus best films in their violent genre. They note that in spite of his usual expertise with lightweight comedies, "Oharas direction is crisp, and torture fans will find much to admire in the Sadean eroticism he brings to the proceedings."    In their Japanese Cinema Encyclopedia: The Sex Films, the Weissers also rate the film highly, giving it three points out of four.
They rank Fairy in a Cage as one of Nikkatsus best in the genre, as well as one of author Oniroku Dans better efforts.  In his review of Oharas oeuvre, Graham R. Lewis writes of Fairy in a Cage, "Ohara really out-did himself with this one: the period detail is perfect and again all production values and acting are of the highest quality." 

Jasper Sharp, in his Behind the Pink Curtain: The Complete History of Japanese Sex Cinema, takes a negative view of Fairy in a Cage. He considers the film intellectually inferior to Tatsumi Kumashiros Woods Are Wet (1973), which had a similar theme. He writes that Ohara is not one of Nikkatsus top directors, and that the film, "merely wallows in its own depravity, with ever more vicious displays of abuse towards its leading lady." 

==Availability==
Fairy in a Cage was released theatrically in Japan on June 4, 1977.  It was released on VHS in Japan on December 5, 1997. 

==Bibliography==

===English===
*  
*  
*  
*  
*  

===Japanese===
*  
*  
*  
*  
*  

==Notes==
 

 

 
 
 
 
 
 