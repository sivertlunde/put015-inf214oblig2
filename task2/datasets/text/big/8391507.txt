Lefty (1964 film)
{{Infobox film name = Lefty image = caption = director = Ivan Ivanov-Vano Vladimir Danilevich writer = Ivan Ivanov-Vano starring = music = Aleksandr Aleksandrov cinematography = editor = Nina Mayorova narrator = Dmitriy Zhuravlyov distributor = released = 1964 (USSR) runtime = 42 min. 23 sec. country = USSR language = Russian
}} story of the same name by the 19th century Russian novelist Nikolai Leskov.  It was directed by the "Patriarch of Soviet animation", Ivan Ivanov-Vano, at the Soyuzmultfilm studio.

The score was performed by the Government Symphony Orchestra, conducted by Grigori Gamburg.

==Creators==
{| class="wikitable"
|-
! !! English !! Russian
|-
|Director-producer Ivan Ivanov-Vano
|Иван Иванов-Вано
|- Second Director (First Assistant) Vladimir Danilevich
|Владимир Данилевич
|- Scenario
|Ivan Ivanov-Vano
|Иван Иванов-Вано
|- Art Directors Arkadiy Tyurin Anatoliy Kuritsyn Marina Sokolova
|Аркадий Тюрин Анатолий Курицын Марина Соколова
|- Artists
|Vladimir Alisov G. Zuykova V. Rodzhero Vladimir Sobolev A. Volkov G. Nevzorova Aleksandr Gorbachyov
|Владимир Алисов Г. Зуйкова В. Роджеро Владимир Соболев А. Волков Г. Невзорова Александр Горбачёв
|- Animators
|Galina Zolotovskaya Yuriy Norshteyn Lev Zhdanov Kirill Malyantovich Mikhail Botov
|Галина Золотовская Юрий Норштейн Лев Жданов Кирилл Малянтович Михаил Ботов
|- Camera Operator Joseph Golomb
|Иосиф Голомб
|- Executive Producer Nathan Bitman
|Натан Битман
|- Composer
|Alexander Aleksandr Aleksandrov
|Александр Александров
|- Sound Operator Boris Filchikov
|Борис Фильчиков
|- Script Editor Natalya Abramova
|Наталья Абрамова
|- Puppets and decorations Vladimir Abbakumov N. Budylova Vera Cherkinskaya Svetlana Znamenskaya Liliana Lyutinskaya Y. Benkevich Oleg Masainov M. Spasskaya
|Владимир Аббакумов Н. Будылова Вера Черкинская Светлана Знаменская Лилиана Лютинская Ю. Бенкевич Олег Масаинов М. Спасская
|- Narrator
|Dmitriy Zhuravlyov
|Дмитрий Журавлёв
|- Editor
|Nina Mayorova
|Нина Майорова
|}

==Plot==
The screen version of the narration of Nikolay Leskov about the surprising master Lefty who grounded a "aglitskaya" (english) steel flea.

==Awards==
*1964 — the Honourable diploma at the VII International film festival short and documentaries in Leipzig.

==Video==
In 2008 was issued together with animated films "The Humpbacked Horse (film)" 1947 and 1975 on DVD the Krupnyy Plan company.

==Creation history==
Ivanov-Vano bore an animated film plan about the gifted master in Leskovs story about 30 years. Over time he arrived at idea that the originality of the narration of Leskov can be transferred, having created a graphic row with a support on an art system of the Russian popular print with "its characteristic generality of forms, specific expressiveness". For animation the idea to show evolution of character of the main character was innovative. Art directors at creation of the movie were inspired by ancient engravings (action in the imperial palace), english engravings (the foreign line), and the Tula episodes were solved in the stylistics of a popular print which is organically uniting all three lines. According to the proposal of the animator of "Lefty" Yury Norstein, the movie became in equipment of a turn.

==See also==
*History of Russian animation
*List of animated feature films
*List of stop-motion films

==External links==
*  at the Animator.ru
* 
*   

 
 

 
 
 
 
 
 
 
 


 
 