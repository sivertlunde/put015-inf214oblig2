Bhujangayyana Dashavathara
{{Infobox film name           = Bhujangayyana Dashavathara image          =  image_size     = caption        =  director       = Lokesh producer       = Girija Lokesh   Lata   Jayamma writer         = Lokesh screenplay     = Lokesh based on       =   narrator       = starring       = Lokesh   Pallavi Joshi   Girija Lokesh music          = Hamsalekha  cinematography = S. Ramachandra editing        = Suresh Urs distributor    = studio         = Belli There released       =   runtime        = 135 minutes country        = India language       = Kannada budget         = preceded_by    = followed_by    = website        =
}}
 Kannada political drama film directed and produced by Lokesh, based on the novel of the same name by Srikrishna Alanahalli.  Besides Lokesh as the protagonist, the film starred Pallavi Joshi and Girija Lokesh in the lead roles. 

The film opened on 4 January 1991 and won numerous awards including the Best Film in Karnataka State Film Awards besides for the story and Supporting acting. At the Filmfare Awards South, the film won best director award to Lokesh. However, the film performed poorly at the box-office and incurred huge financial losses to Lokesh. 

==Cast==
* Lokesh as Bhujangayya
* Pallavi Joshi
* Girija Lokesh
* Vadiraj
* Krishne Gowda
* Chandre Gowda
* Brahmavar
* G. V. Sharadha
* Prema Master Srujan
* Master Balaraj

==Soundtrack==
{{Infobox album	
| Name = Bhujangayyana Dashavathara
| Longtype = 
| Type = Soundtrack	
| Artist = Hamsalekha
| Cover = 
| Border = Yes
| Alt = Yes	
| Caption =	
| Released = 
| Recorded =	
| Length =  Kannada
| Label = 
| Producer = 
| Last album = 
| This album = 
| Next album = 
}}

The music of the film was composed by Hamsalekha.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Lyricist
|- 1 || Moretora Byaada  || C. Ashwath || Hamsalekha
|- 2 || Naramanushya || C. Ashwath || Hamsalekha
|- 3 || Ivanyaramma  || B. R. Chaya || Hamsalekha
|- 4 || Yappo Yappo Enu Janmavo || S. P. Balasubrahmanyam || Hamsalekha
|- 5 || Neenolidare ||  || Hamsalekha
|- 6 || Baare Baramma || K. J. Yesudas || Hamsalekha
|}

==Awards and honors==
* 1990-91: Karnataka State Film Awards Third Best Film Best Supporting Actress - Girija Lokesh Best Story - Srikrishna Alanahalli

* 1991 : Filmfare Awards South
#  

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 