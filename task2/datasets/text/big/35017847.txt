Adi Shankaracharya (film)
 
{{Infobox film
| name           = Adi Shankaracharya
| image          = Shakara Film Scene.JPG
| image size     = 220px
| border         = 
| alt            = Shankara, an Indian philosopher is giving a speech.
| caption        = A scene from the film Adi Shankaracharya
| director       = G. V. Iyer
| producer       = National Film Development Corporation of India
| writer         = 
| screenplay     = G. V. Iyer
| story          = 
| based on       = Life of Adi Shankaracharya
| narrator       = 
| starring       = Sarvadaman Banerjee  Sreenivasa Prabhu Bharat Bhushan
| music          = M. Balamurali Krishna
| cinematography = Madhu Ambat
| editing        = 
| studio         = 
| distributor    = 
| released       = 1983 
| runtime        = 136 minutes
| country        =  
| language       = Sanskrit
| budget         = 
| gross          = 
}}
  1983 Indian Best Film, Best Screenplay, Best Cinematography Best Audiography.      

==Cast and Crew==
===Cast===
<!--##########################################################
####### SPELLING ACCORDING TO FILM TITLE CARD
####### THERE IS MULTIPLE MISTAKES IN IMDb ENTRY. BE CAREFUL!
##########################################################-->
*Sarvadaman D. Banerjee as Adi Shankara.
*Sreenivasa Prabhu as Prajnaana.
*T.S. Nagabharana as Mrithyu.
*Bharat Bhuushan as Shivaguru.
*Manjunath Bhat as Padmapada.
*Gopal as Thotaka.
*V.R.K. Prasad as Hasthamalaka.
*M.V. Narayana Rao as Sureshwara.
*Gopinath Das as Govinda Bhagavathpada.
*L.V. Sharada Rao as Aryamba
*Leela Narayana Rao as Ubhaya Bharathi.
*Sreepathy Ballal as Kumarila Bhatta.
*G. V. Iyer as Veda Vyasa.
*Gopi as young Shankara.
*Radhakrishna as baby Shankara.
*Raghu Iyer as young Pranjana.
*Vijay Bharan as young Mrithyu.
*G.V.Shivanand as Guru.
*Gopalakrishna as Namboodiri
*Mallesh as Kapalika.
*Murgod as Chandala.
*Bala Subramanya as rich man.
*Manohar as Mama.
*G.S.Natraj as Vishnu.
*Ajay as Sathyakama.
*Mahesh Swamy as Boudha Guru.
*Purushotham as Boudha Bhikshu.
*Balu as Chiushka.
*Girish as Vidyananda.
*Veena Kamal as Jabali.
*Gayathri Balu as Amalaka Lady.

===Crew===
*Producer: G. V. Iyer.
*Director: G. V. Iyer.
*Music director: Dr. M. Balamurali Krishna.
*Art director: P. Krishna Murthy.
*Background score: B. V. Karanth.
*Screenplay: G. V. Iyer.
*Dialogue: Bannanje Govindacharya.
*Script (Sanskrit translation): N. Ranganatha Sharma.
*Film adviser: T.M.P Mahadevan.
*Production in charge: K. V. Sundarambai.

==Awards==
*  
**  
**   Best Cinematography (Colour): Madhu Ambatt Best Audiography: S.P. Ramanathan

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 