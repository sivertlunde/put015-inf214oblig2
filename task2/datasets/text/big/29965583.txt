Den forsvundne pølsemaker
{{Infobox film
| name           = Den forsvundne pølsemakere
| image          = 
| image size     =
| caption        = 
| director       = Toralf Sandø
| producer       = 
| writer         = Toralf Sandø
| narrator       =
| starring       = Leif Juster   Ernst Diesen
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 26 December 1941
| runtime        = 98 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Norwegian comedy film written and directed by Toralf Sandø, starring Leif Juster and Ernst Diesen. Private investigators Gløgg (Diesen) and Rask (Juster) have been hired to trace a butcher (or sausage-maker) who has disappeared. This leads the two into a number of adventures. The movie is today best known for Justers performance of the song "Pølsemaker, pølsemaker".

==External links==
*  
*  

 
 
 
 


 
 