Love Business
{{Infobox film
| name           = Love Business
| image          =Love business.JPEG
| image size     =
| caption        =
| director       = Robert F. McGowan
| producer       = Robert F. McGowan Hal Roach
| writer         = H. M. Walker
| narrator       =
| starring       =
| music          = Leroy Shield Marvin Hatley
| cinematography = Art Lloyd
| editing        = Richard C. Currier MGM
| released       =  
| runtime        = 20:12
| country        = United States English
| budget         =
}}
 short comedy film directed by Robert F. McGowan.    It was the 104th (16th talking episode) Our Gang short that was released.

==Plot==
Jackie is hopelessly in love with Miss Crabtree. At the same time, his sister Mary Ann tells their mother that Jackie is in love with Miss Crabtree. Jackie runs off to school without eating breakfast. Meanwhile, Miss Crabtree becomes a boarder in Jackies home and moves in later that day. Chubby also is in love with Miss Crabtree and practices kissing her on an oversized cardboard statue of Greta Garbo. Then at school, Wheezer tells Jackie, Mary Ann, Chubby, Farina, Donald, and Bonedust that Miss Crabtree was moving into their house. Jackie has mixed emotions about this.

That evening, Miss Crabtree has dinner with Jackie, Mary Ann, Wheezer, and their mother. Mothballs fell into the soup a bit earlier giving the soup a very bitter taste. Later, Chubby stops in to see Miss Crabtree and recites some very romantic poetry. Miss Crabtree asks Chubby where he got all this stuff. He says from Wheezer (who got them from his mothers old love letters). Mother hears this and is about to give Wheezer a spanking but decides not to in the end.

==Note==
Most of the schoolyard scenes were edited out of the Little Rascals television print in 1971 due to stereotyping of African-Americans but reinstated in 1990s home video and in 2001 AMC showings.

==Cast==
===The Gang===
* Jackie Cooper as Jackie Cooper
* Norman Chaney as  Norman Chubby Matthew Beard as Stymie
* Dorothy DeBorba as Dorothy Echo
* Allen Hoskins as Farina
* Bobby Hutchins as Wheezer Cooper
* Mary Ann Jackson as Mary Ann Cooper
* Shirley Jean Rickert as Shirley
* Donald Haines as Donald Bobby Young as Bonedust
* Pete the Pup as Himself

===Additional cast===
* June Marlowe as Miss June Crabtree
* May Wallace as May Wallace Cooper, Jackies mother
* Baldwin Cooke as Undetermined role

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 