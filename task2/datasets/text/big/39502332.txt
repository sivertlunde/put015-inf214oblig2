Shaun the Sheep Movie
 
{{Infobox film
| name           = Shaun the Sheep Movie
| image          = Shaun the Sheep MoviePoster.jpg
| caption        = Theatrical release poster Richard Starzak Mark Burton
| producer       = Paul Kewley Julie Lockhart
| story          = Mark Burton
| writer         = Richard Starzak Mark Burton
| based on       =  
| starring       = Justin Fletcher John Sparkes Omid Djalili   
| music          = Ilan Eshkeri
| cinematography = Charles Copping Dave Alex Riddett
| editing        = Sim Evan-Jones 
| studio         = Aardman Animations Anton Capital Entertainment
| distributor    = StudioCanal    United Kingdom|ref2=   }} 
| runtime        = 85 minutes 
| country        = United Kingdom   
| language       = English
| budget         = 
| gross          = $54 million 
}}
 Richard Starzak Mark Burton. Financed and distributed by StudioCanal,  it was released in the United Kingdom on 6 February 2015. 

==Plot==
The film follows Shaun, Bitzer and the flock into the big city to rescue the Farmer, who gets lost after Shauns mischief.

After a blow to the head, the Farmer is diagnosed with amnesia, yet retains certain memories (i.e. shearing the sheep), which proves crucial to the plot. Meanwhile, Shaun is captured by Animal Control, and finds himself in a cell with Bitzer. They later escape, and find the Farmer, who is now a world-famous hair stylist (he mistakenly shaved the hair off a famous celebrity, as if he was Shaun), for starting the new trend. However, the Farmer no longer recognises Shaun, and Shaun is left heartbroken after the encounter.

The flock devises a plan - they will send everyone to sleep, then abduct the farmer, and take him back to the countryside. However, with the enraged stray-catcher hot on their trail, the protagonists plan to safely make it back home wont be nearly as easy as they hope, but theyre determined to succeed.

==Voice cast==
*Justin Fletcher as Shaun/Timmy
*John Sparkes as Bitzer/Farmer
*Omid Djalili as Trumper 
*Kate Harbour as Timmys Mum/Meryl
*Richard Webber as Shirley
*Tim Hands as Slip 
*Simon Greenall as The Twins  Emma Tate as Hazel
*Henry Burton as Junior Doctor/Animal Containment Visitor 
*Dhimant Vyas as Hospital Consultant 
*Sophie Laughton as Animal Containment Visitor 
*Nia Medi James as Operatic Sheep
*Andy Nyman as Nuts
*Jack Paulson as Celebrity with Hair Trouble
*Nick Park as Himself

==Production== Richard Starzak Mark Burton, would be financed and distributed by StudioCanal.  The film will follow Shaun and his flock into the big city to rescue their farmer, who was forced by Shauns mischief to leave the farm.  On 24 September 2013, it was announced the film would be released on 20 March 2015,  but was later moved up to 6 February 2015.  Principal photography and production began on 30 January 2014. 

===Music===
Ilan Eshkeri composed the music for the film. 
 Kaiser Chief Nick Hodgson. 

==Release== Sundance Kid program on 24 January 2015.  The film was released in the United Kingdom on 6 February 2015.  It will be released in the United States on 7 August 2015 by Lionsgate Films. 

==Reception==
Review aggregator Rotten Tomatoes reports that List of films with a 100% rating on Rotten Tomatoes|100% of 33 critics have given the film a positive review, with a score average of 8/10. The sites consensus reads "Warm, funny, and brilliantly animated, Shaun the Sheep is yet another stop-motion jewel in Aardmans family-friendly crown."  On Metacritic, the film has a score of 77 out of 100, based on 6 critics, indicating "generally favorable reviews". 

==Potential sequels==
Aardman has stated that they are considering making a sequel to the film.     

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 