The Bodyguard from Beijing
 
 
{{Infobox film
| name           = The Bodyguard from Beijing
| image          = The-Bodyguard-from-Beijing.jpg
| film name = {{Film name| traditional    = 中南海保鑣
| simplified     = 中南海保镖
| pinyin         = Zhōngnánhǎi Bǎobiāo
| jyutping       = Zung1-naam4-hoi2 Bou2-biu1}}
| director       = Corey Yuen
| producer       = Jet Li
| writer         = Chan Kin-Chung Gordon Chan
| starring       = Jet Li Christy Chung Kent Cheng Collin Chou Ng Wai-kwok Joey Leung
| music          = William Hu
| editing        = Angie Lam Golden Harvest
| released       =  
| runtime        = 89 minutes
| country        = Hong Kong
| language       = Cantonese Mandarin
| budget         = 
}}
The Bodyguard from Beijing, also known as The Defender in the United States and the United Kingdom, is a 1994 Hong Kong action film directed by Corey Yuen, starring Jet Li, Christy Chung,  Collin Chou, and Kent Cheng. 

==Plot==
Allan is one of the most promising members of an elite corp of international bodyguards based in China. He is known for using highly unorthodox and anti-trust building methods,from helping a VIP escape assassination in a pool,to forcing a VIP to ride on a car with explosives and detonated near him, but his decisions are usually right. He is hired by James, a wealthy Hong Kong businessman, to protect his girlfriend Michelle, who is the only surviving witness to a mob murder. However, Michelle and Allan clash with one another almost immediately because she feels like she is under house arrest, while Allan is equally fed up because he is not able to join the Chinese Premiers bodyguard detail in China due to his duties to watch over Michelle. 

Michelle eventually storms out of the house and goes to the shopping mall. Unknown to Michelle, the mall is crawling with mob assassins under disguise. A massive shoot-out ensues when the assassins attempt to kill her, but Allan shows up and protects her while taking out the assassins one by one. One of the assassins who posed as a police officer and is killed by Allan during the shoot-out is the younger brother of Wong, a former Chinese soldier who fought together with his brother. Wong seizes his brothers dead body from the police morgue and burns his brothers body later, swearing vengeance on Allan. 

In the meantime, Michelle is grateful towards Allan for saving her and develops feelings for him. The two grow closer as they gain a better understanding towards one another. Though Michelle is straightforward with her feelings for him, Allan rejects her pursues because of his duties as her bodyguard.

Things come to a climax when Wong and a group of assassins storm the penthouse and start a gunfight.Ken was killed when he was shot by Wong himself. Allan eventually takes out all the other assailants until only Wong is left and engages Michelles younger brother was shot in a foot when Wong spots his shoes lighting. Michelle breaks cover to hold up a stumbling Allan while James enters the house just as Wong recovers his gun. Wong points his gun at Michelle and asks the two men who is willing to take her place. James attempts to dissuade Wong from shooting by offering to pay Wong, but Wong refuses. Allan shields Michelle with his body and takes two shots but he manages to pull out the bayonet from his chest and throw it towards Wongs neck, killing him. The scene ends with Michelle holding the convulsing Allan and crying inconsolably whilst James watches on.

Before the film ends, James drives Michelle to the border between Hong Kong and mainland China as she tries to see Allan a final time before he heads back to China but the guards at the checkpoint deny them entry into the mainland. Allan leaves Michelle with the box that held the watch she had given to him as a present and he had tried to refuse. However, when she opens it, the box contains his own watch that he was originally wearing, while "Fat Po" (Kent Cheng) receives Allans payment money to fund his sons school tuition. Michelle cries out Allans name just as his car drives away from the border back into the mainland.

==Cast==
* Jet Li as Allan Hui Ching-yeung  
* Christy Chung as Michelle Yeung
* Collin Chou as Wong  
* Kent Cheng as Charlie Leung Kam-po ("Fat Po")  
* Joey Leung as Keung  
* Ng Wai-kwok as James Song Sai-cheung
* William Chu as Billy
* Wong Kam-kong as Chiu Kwok-man (uncredited)
* Corey Yuen (uncredited)
* Sam Wong
* Gary Mak

 
 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 