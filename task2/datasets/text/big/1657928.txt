Monster a Go-Go
 
{{Infobox film
| name           = Monster a Go-Go!
| image          = Monsteragogo.jpg
| alt            = 
| director       = Bill Rebane Herschell Gordon Lewis  (uncredited) 
| producer       = Herschell Gordon Lewis  (as Sheldon S. Seymour)  Henry Marsh Bill Rebane
| writer         = Herschell Gordon Lewis  (as Sheldon Seymour) 
| screenplay     = Jeff Smith Dok Stanford Bill Rebane
| narrator       = Herschell Gordon Lewis  (uncredited)  Bill Rebane  (uncredited) 
| cinematography = Frank Pfeiffer
| studio         = B.I. & L. Releasing Corp.
| distributor    = B.I. & L. Releasing Corp. Something Weird Video
| released       = July 1965
| runtime        = 70 min.
| country        = United States
| language       = English
}}
 horror film one of the worst films ever.

The film was featured in an episode of movie-mocking television show Mystery Science Theater 3000 on Comedy Central. As it is an incredibly blatant deus ex machina, the phrase "but there was no monster!" became an often repeated riff for similar events in other films. The team felt that this was the worst film ever featured on the show. 

==Plot==
 
The plot concerns an American astronaut, Frank Douglas, who mysteriously disappears from his spacecraft as it parachutes to Earth. The vanished astronaut is apparently replaced by or turned into a large, radioactive humanoid monster. A team of scientists and military men attempt to capture the monster – and at one point succeed, only to have him escape again. Neither the capture nor the escape are ever shown, and are simply mentioned by the narrator.

At the end of the film, the scientists receive a telegram stating that Douglas is in fact alive and well, having been rescued in the North Atlantic, perhaps implying the monster was an alien impersonating Douglas. After the monster seemingly vanishes, the narrator provides the films closing dialogue:

 

== Cast==
*Henry Hite as Frank Douglas/The Monster
*June Travis as Ruth
*Phil Morton as Col. Steve Connors
*Peter M. Thompson as Dr. Chris Manning

== Production ==
 needed a second film to show with his own feature, Moonshine Mountain, bought the film, added a few extra scenes and some dialogue, and then released it, creating an odd, disjointed film with little continuity. Rebane had abandoned the film in 1961; Lewis did not finish the film until 1965 and so was unable to gather all of the original cast, resulting in almost half the characters disappearing midway through the film to be replaced by other characters who fill most of the same roles. One of the actors Lewis was able to get back had dramatically changed his look in the intervening years, necessitating his playing the brother of the original character.   

The actor playing the films monster, Henry Hite was 7 foot 6 but the director wanted the monster to be 10 feet tall, so Hite was filmed upward angle in every shot. 

==Reception==
 
Critical reception for the film has been predominantly negative.

Allmovie gave the film a negative review calling it "an incoherent concoction brewed solely to fill space on a double bill". 

The film has a very low rating of 2.4/10 on the Internet Movie Database and is placed number 84 on the IMDB Bottom 100.  The film also holds a 6% on Rotten Tomatoes. 

== DVD release ==

* Monster a Go-Go! was released with Psyched by the 4-D Witch as a DVD double feature by Something Weird Video. Rhino Home Video as part of the Collection, Volume 8 DVD set.
* The films original director, Bill Rebane, released a "Special Collectors Edition" with commentary and other extras on Synergy Entertainment on October 19, 2010.  

== See also ==

* List of films considered the worst

== References ==

 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 