The Climbers (1919 film)
{{infobox film
| name           = The Climbers
| image          = The Climbers - and - The Head Waiter - 1919 newspaperad.jpg
| caption        = A contemporary newspaper advertisement, featuring The Climbers as a double feature with The Head Waiter, another film from 1919.
| director       = Tom Terriss
| producer       = Vitagraph Company of America
| writer         = Lucien Hubbard (scenario)
| based on       =  
| starring       = Corinne Griffith
| cinematography = Tom Malloy
| editing        = George Randolph Chester
| distributor    = Vitagraph Company of America
| released       =  
| runtime        = 5 reel#Motion picture terminology|reels; 4,644 feet
| country        = United States
| language       = Silent (English intertitles)
}} Clyde Fitchs Broadway play. This film was directed by Tom Terriss and stars Corinne Griffith.  
 The Climbers The Climbers with Irene Rich.

A print is preserved in the Library of Congress. 

==Cast==
*Corinne Griffith &ndash; Blanche Sterlin
*Hugh Huntley &ndash; Richard Sterling
*Percy Marmont &ndash; Ned Warden
*Henry Hallam &ndash; George Hunter
*Josephine Whittell &ndash; Clara Hunter
*Jane Jennings &ndash; Aunt Ruth 
*James Spottswood &ndash; James Garfield Trotter
*Corinne Barker &ndash; Julia Godesby
*Emily Fitzroy &ndash; Mrs. Hunter
*Charles Halton &ndash; Jordan
*James A. Furey

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 


 