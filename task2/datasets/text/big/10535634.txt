You Can Thank Me Later
 
{{Infobox film
| name           = You Can Thank Me Later
| image          = Youcanthankmelaterdvd.jpg
| caption        = DVD cover
| director       = Shimon Dotan
| producer       = Netaya Anbar, Shimon Dotan
| writer         = Oren Safdie (play)
| narrator       =
| starring       = Ellen Burstyn
| music          = Walter Christian Rothe
| cinematography = Amnon Salomon
| editing        = Netaya Anbar
| distributor    = Cinequest Films
| released       = 1998
| runtime        = 110 minutes
| country        = Canada
| language       = English
| budget         =
}}
 1998 Canadian comedy drama film directed by Shimon Dotan. The film is based on a play titled Hyper-Allergenic written and adapted for the screen by Oren Safdie.

==Overview==
Conduct under pressure is the source of caustic humor and poignancy in "You Can Thank Me Later." Set primarily in a hospital room where a family awaits the results of the fathers operation, the emotional battlefield is a series of zingers that touch sensitive nerves and tickle the funnybone.

Shirley Cooperberg (Ellen Burstyn) is the strong-willed matriarch of a well-heeled Montreal Jewish family. While her husband is under the surgeons scalpel, her children arrive at the hospital. Eli (Ted Levine) is an oft-divorced, failed writer; Susan (Amanda Plummer) has been pouring her myriad neuroses onto canvas but has yet to find an appreciative audience; and Edward (Mark Blum) is a successful producer of touring Broadway plays. They are the picture-perfect embodiment of a dysfunctional family.

Expanding on Safdies play, director Shimon Dotan wades into Neil Simon territory. The recriminations, failures and barely contained bile hurled amongst principals are familiar in their wickedly funny, combative bent. Its a dry humor laden with modern-day angst and given a slightly novel spin when transplanted from New York to bilingual Quebec. Otherwise, the uptown group share the affluence and anxieties of Simons Manhattanites.

Also in the stew are a mistress (Geneviève Bujold) posing as a nun, Elis ex-wife (Mary McDonnell) and son (Jacob Tierney), a feisty nurse (Genevieve Brouillette), Edwards wife (Macha Grenon) and an ever-present TV repairman (Roch Lafortune) whos a virtually mute witness to the bad behavior.

==Cast==
*Ellen Burstyn ...  Shirley Cooperberg
*Amanda Plummer ...  Susan Cooperberg
*Ted Levine ...  Eli Cooperberg
*Mark Blum ...  Edward Cooperberg
*Mary McDonnell ...  Diane
*Geneviève Bujold ...  Joelle
*Jacob Tierney ...  Simon Cooperberg
*Roc LaFortune ...  TV Repairman
*Macha Grenon ...  Linda
*Geneviève Brouillette ...  Nurse
*Dorothée Berryman ...  Art Patron

==External links==
* 

 
 
 
 
 
 


 
 