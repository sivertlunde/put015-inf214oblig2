Turtle Diary
{{Infobox film
| name           = Turtle Diary
| image          = Turtle diary post.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = John Irvin Richard Johnson Bernard Sofronski (Executive Producer)
| writer         = Russell Hoban (novel) Harold Pinter (screenplay)
| narrator       = 
| starring = {{Plainlist|
* Glenda Jackson
* Ben Kingsley
}}
| music          = Geoffrey Burgon
| cinematography = Peter Hannan
| editing        = Peter Tanner
| distributor    = The Samuel Goldwyn Company
| released       = December 1985 (UK)
| runtime        = 97 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 1985 United British film drama and has been described as a romantic comedy. 

==Synopsis==
Two lonely Londoners - Neaera Duncan, a childrens author (Glenda Jackson), and William Snow, a bookstore assistant (Ben Kingsley) - find common ground when visiting the sea turtles at London Zoo; independently of each other, both perceive that the turtles are unnaturally confined, and they hatch a plan with the assistance of zookeeper George Fairbairn (Michael Gambon) to smuggle them out and release them into the sea, which they ultimately succeed in accomplishing.  Their release of the turtles represents metaphorically their release of themselves from their own inhibitions.   

==Main cast==
*Glenda Jackson, as Neaera Duncan, a "Popular childrens author … fearing her creative talents have evaporated,   escapes into the dreamy world of sea turtles seeking inspiration in their beauty and grace." 
*Ben Kingsley, as William Snow, "a humble assistant in a bookstore where he, too, dreams of the turtles."  Richard Johnson, as Mr. Johnson, a neighbor of Neaera Duncan
*Michael Gambon, as George Fairbairn, the zookeeper charged with caring for the turtles
*Jeroen Krabbé, as Mr. Sandor, a neighbor of William Snow
*Rosemary Leach, as Mrs. Charlie Inchcliff, another neighbor of Neaera Duncan
*Eleanor Bron, as Miss Neap, a neighbor of William Snow
*Harriet Walter, as Harriet Simms, a colleague of William Snow at the bookstore
*Nigel Hawthorne, as the Publisher of books by Neaera Snow
 cameo role as a man in the bookshop where William and Harriet work.

==Critical reception and analysis==
According to its description at Amazon.com, Turtle Diary has been "Critically hailed as a mini-masterpiece." 

In his 1985 Sunday Telegraph review of the film, Castell observes that Pinters screenplay concentrates on developing dialogue and plot, leaving clues for the actors to convey their characters subtle emotional and psychological development: "It is hard to think of two actors better matched to play Pinter than Glenda Jackson and Ben Kingsley. They milk every nuance, point up every missed beat and relish each irony and repetition in the script. … Turtle Diary is a fine film that charts movingly the unnoticed despair of everyday lives, the sufferings of those who endure loneliness in silence." 

The film grossed $2.2 million in its U.S. theatrical release.

==Home video ==

The film was released on videocassette in 1986 by Vestron Video.  The film has not yet been released on DVD.

==Notes==
 

==References==
  Sunday Telegraph 1 Dec. 1985.  Rpt. in HaroldPinter.org.  Harold Pinter, 2000–2003  . Accessed 22 March 2009.
 

==External links==
*  at HaroldPinter.org – The Official Website of International Playwright Harold Pinter (Includes production details and excerpt of film review by Castell.)
* 
 
 

 
 
 
 
 
 
 