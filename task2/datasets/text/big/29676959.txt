Vlyublyon po sobstvennomu zhelaniyu
{{Infobox film
| name           = Vlyublyon po sobstvennomu zhelaniyu
| image          = 
| caption        = 
| director       = Sergey Mikaelyan
| producer       = 
| writer         = Sergei Mikaelyan Aleksandr Vasinsky
| starring       = Oleg Yankovskiy
| music          = 
| cinematography = Sergei Astakhov
| editing        = I. Golovko
| distributor    = 
| released       = February 1983
| runtime        = 89 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}}

Vlyublyon po sobstvennomu zhelaniyu ( ) is a 1983 Soviet comedy film directed by Sergey Mikaelyan. It was entered into the 33rd Berlin International Film Festival, where Yevgeniya Glushenko won the Silver Bear for Best Actress.   

==Plot==
A handsome sportsman Igor Bragin and a plain but brainy librarian Vera Silkova make an unusual agreement: to fall in love with each other through will alone. In the background is late Soviet reality with a few of its unattractive features – ambition ridden careerists, black market, drunkenness, growing skepticism and disillusionment with ideals.

==Cast==
* Oleg Yankovskiy as Igor
* Yevgeniya Glushenko as Vera
* Vsevolod Shilovsky as Nikolai
* Irina Reznikova as Natasha Yuri Dubrovin as Petrushkin Vladimir Belousov as Gena
* Yu. Kopich as Michail Petrovich
* Kira Krejlis-Petrova
* Ivan Ufimtsev
* Svetlana Shershneva
* Natalya Yegorova Sergei Losev

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 