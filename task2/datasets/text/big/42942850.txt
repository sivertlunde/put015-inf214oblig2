Catherine the Last
{{Infobox film
| name =  Catherine the Last
| image =
| image_size =
| caption =
| director = Henry Koster
| producer = Joe Pasternak
| writer =   Alexander Hunyady (novel)   Károly Nóti   Felix Jackson
| narrator =
| starring = Franciska Gaal   Hans Holt   Hans Olden   Otto Wallburg
| music = Nicholas Brodszky   Hans J. Salter  
| cinematography = Theodore J. Pahle 
| editing = Viktor Gertler     
| studio = Universal Pictures
| distributor = Universal Pictures 
| released = 30 August 1936
| runtime = 
| country = Austria  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Catherine the Last (German:Katharina, die Letzte) is a 1936 Austrian romantic comedy film directed by Henry Koster and starring Franciska Gaal, Hans Holt and Hans Olden.  It was made by the Austrian subsidiary of Universal Pictures. After making the film Koster moved to Hollywood. In 1938 the film was remade in America as The Girl Downstairs with Gaal reprising her role.

==Synopsis== kitchen maid. While at first he simply uses her as a ruse, he eventually falls in love with her instead.

==Cast==
* Franciska Gaal as Katharina, Küchenmädchen  
* Hans Holt as Hans von Gerstikow  
* Hans Olden as Eduard, Hans Freund  
* Otto Wallburg as Sixtus Braun, Großindustrieller 
* Dorothy Poole as Sybill Braun  
* Eduard Linkers as Steinschneider, Brauns secretary  
* Ernő Verebes as Tobby, Hans servant  
* Adrienne Gessner as Berta, Köchin  
* Fritz Imhoff as Bubs, Autohädler  
* Adolf E. Licho as Exzellenz  
* Georg Schmieter as Feuerwehrmann  
* Sigurd Lohde as Kommissar  
* Comedian Harmonists as Themselves  Paul Morgan as Stephan, Diener

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 


 