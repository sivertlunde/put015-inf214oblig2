Pirates of the 20th Century
{{Infobox Film name = Pirates of the 20th Century image = Pirates_of__XX_century.jpg image_size =  caption = Soviet film poster director = Boris Durov producer =  writer = Boris Durov Stanislav Govorukhin narrator =  starring =  music = Yevgeni Gevorgyan cinematography = Aleksandr Rybin editing = G. Negelnitska studio =  distributor = Gorky Film Studio released = 1979 runtime = 81 min. country = Soviet Union language = Russian budget =  gross = 
}}
Pirates of the 20th Century ( , Transliteration|translit.&nbsp;Piraty XX veka) is a 1979 Soviet action film|action/adventure film about modern piracy. The film was directed by Boris Durov, the story was written by Boris Durov and Stanislav Govorukhin. 
 the leader of Soviet distribution in 1980 and had 87.6 million viewers. 

== Plot == Indian or Pacific Ocean and stopping near the pier where the Soviet cargo ship Nezjin is anchored. An agent of a local pharmaceutical company meets the captain of the Soviet vessel and discusses the cargo, medical opium, which is in critical demand by the hospitals of the USSR.  Soon after that the pharmaceutical company agent is seen inside a car, speaking to someone via walkie-talkie.  Later the MV Nezjin, with the opium on board, leaves port for Vladivostok.  

Some distance into the voyage, a watchman cries "man overboard" and the captain orders the engines stopped to rescue the stranded swimmer. The boat from Nezjin picks up an Asian man who identifies himself as Salikh, the only surviving sailor from a foreign merchant ship.  Salikh told the crew that his ship suddenly capsized during a heavy storm and his crewmates were fighting for places in rescue boats. Shortly after that the Soviet captain is informed of an unknown ship, drifting nearby. The ship, called the Mercury, is apparently abandoned, with no crew visible and no activity on deck.  The captain of the Nezjin decides to send four men to explore the ship and offer assistance to possible  survivors.  
 trap for the Soviets.  Occupied with the Mercury, none of the Russian crewmen pays any attention to Salikh, who takes an axe from the ships firefighting kit, enters the radio room of the Nezjin, and attacks a radio operator, killing him.  After Salikh destroys the ships radio equipment,  the Mercury starts her engines and approaches the Nezjin.  At that moment, the Nezjins crew see the bodies of the boarding party, floating in the water behind the Mercury.  The Soviet captain realizes that his ship is under attack by pirates. 

Their attempt to escape is foiled when the Mercury rams the ship and the pirates open fire with assault rifles and machine guns.  The pirates board the Nezjin, brutally killing Russian crewmembers who fight them.  Sergey, the chief-mate of the Nezjin, discovers the dead radio operator and decides to find Salikh.  Chasing Salikh through the corridors of the ship, Sergey makes an attempt to stop him.  Salikh shows impressive martial arts skills and quickly defeats the chief-mate.  Soon after, the pirates lock the remaining Russians into crew compartments and begin to offload the opium to the Mercury.  The pirate captain thanks Salikh for a successful mission and orders him to blow up the Nezjin together with her crew.  The Soviets, left to die on a sinking ship, manage to escape and must fight the pirates for survival.

==Cast==
* Nikolai Yeryomenko, Jr. as Sergej Sergejitsch (Chief Mate)
* Pyotr Velyaminov as  Iwan Iljitsch (Soviet Captain)
* Talgat Nigmatulin as  Salikh
* Rein Aren as   Captain of Pirates
* Dilorom Kambarova as Island girl 
* Natalya Kharakhorina as  Mascha
* Dzhigangir Shakhmuradov as Noah
* Yuri Kutyrev as Shweiggert
* Tadeush Kasyanov as Bosun
* Maya Eglite
* Viktor Zhiganov
* Georgi Martirosyan
* Leonid Trutnev
* Vladimir Yepiskoposyan
* K. Aminov

==See also==
*A Hijacking, a 2012 film on piracy in the Indian Ocean Captain Phillips, a 2013 American action thriller film directed by Paul Greengrass and starring Tom Hanks and Barkhad Abdi
*Survival film

== References ==
 

==External links==
* 
* 
* 

 
 
 
 
 