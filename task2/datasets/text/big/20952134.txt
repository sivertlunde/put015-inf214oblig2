The Sick Kitten
{{Infobox film
| name           = The Sick Kitten
| image          = TheSickKitten.jpg
| image_size     = 
| caption        = Screenshot from the film George Albert Smith
| producer       = George Albert Smith
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography = George Albert Smith
| editing        =
| studio         = G.A. Smith
| distributor    = Warwick Trading Company
| released       =  
| runtime        = 34 secs
| country        = United Kingdom Silent
| budget         =
}}
 1903 UK|British short  silent comedy George Albert Smith, featuring two young children tending to a sick kitten. The remake of the directors now lost The Little Doctor (1901), according to Michael Brooke of BFI Screenonline, "continues the editing technique that he first explored in Grandmas Reading Glass (1900) and As Seen Through a Telescope (1900)," but, "without the circular black mask to differentiate it," as presumably, "Smith believed that his audience would have grown more sophisticated and would be able to tell the difference between a medium shot and close-up without prompting."      

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 