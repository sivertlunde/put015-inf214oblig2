Subway Serial Rape: Lover Hunting
{{Infobox film
| name = Subway Serial Rape: Lover Hunting
| image = Subway Serial Rape - Lover Hunting.jpg
| image_size = 210px
| caption = Theatrical poster for Subway Serial Rape: Lover Hunting (1988)
| director = Shuji Kataoka|Shūji Kataoka 
| producer = 
| writer = Shūji Kataoka
| narrator = 
| starring = Kanako Kishi Megumi Takahashi Sayaka Hitomi
| music = 
| cinematography = Satoshi Shimomoto
| editing = Shōji Sakai
| studio = Shishi Productions
| distributor = Nikkatsu
| released = May 7, 1988
| runtime = 61 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1988 Japanese pink film written and directed by Shuji Kataoka|Shūji Kataoka. It was named the Best Film at the first Pink Grand Prix ceremony. 

==Synopsis==
Tomoko, a newscaster, decides to do a series of stories on rapes in subways. A gang which perpetrates these attacks targets the newscaster, and she is assaulted while broadcasting live.   

==Cast==
* Kanako Kishi ( ) as Tomoko (newscaster) 
* Megumi Takahashi ( )
* Sayaka Hitomi ( )
* Sayaka Kimura ( )
* Tsubasa Hayase ( )
* Shirō Shimomoto
* Yukijirō Hotaru ( )
* Ren Ōsugi ( )

==Background==
 s Shishi Productions and it was released theatrically in Japan by Nikkatsu on May 7, 1988.    It became available online through the Hokuto Corporations DMM service on December 17, 2004. 

Kataokas Subway Serial Rape series was inaugurated in 1985. The first film was intended as a single, but led to three loosely-related sequels. Kataoka had worked as assistant director to Yōjirō Takita in his Molesters Train series. Unlike the light tone of Takitas films, Kataokas Subway Serial Rape series is intentionally shocking and offensive. All the films in the series were released by Nikkatsu during the last years of its Roman porno series era. Subway Serial Rape: Lover Hunting was the fourth and last entry in the series, released in 1988, the year that Nikkatsu ceased production. In their Japanese Cinema Encyclopedia: The Sex Films, Thomas and Yuko Mihara Weisser write that these films may be "the most perfect portrayal of voyeurism ever captured on celluloid". 
 Ren Ōsugi had started his film career in the pink genre with Banmei Takahashis Tightly Bound Sacrifice (1980). Subway Serial Rape was his last appearance in a pink film. 

==Bibliography==

===English===
*  
*  
*  
*  

===Japanese===
*  
*  
*  
*  

==Notes==
 

 
 
 
 
 

 

 
 
 
 
 


 
 