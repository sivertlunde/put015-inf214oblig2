Afinidades (film)
{{Infobox film
| name           = Afinidades
| image          = Afinidades-film.jpg
| alt            =  
| caption        = Original film poster
| director       = Jorge Perugorría and Vladimir Cruz
| producer       = Antonio Gijon
| screenplay     = Vladimir Cruz
| based on       =  
| starring       = Jorge Perugorría, Vladimir Cruz, Cuca Escribano and Gabriela Griffith
| music          = Silvio Rodríguez
| cinematography = Luis Najmias Jr.
| editing        = 
| studio         = 
| distributor    = Hispafilms Cuban Film Institute (ICAIC)
| released       =  
| runtime        = 87 minutes
| country        = Spain Cuba
| language       = Spanish
| budget         = 
| gross          = 
}}
Afinidades ( ) is the debut feature film co-directed by Cuban director-actors Jorge Perugorría and Vladimir Cruz. They also both play the main roles in the film, a psychological drama. Cruz was also the screenwriter of the film, which is an adaptation of the Reynaldo Montero novel Música de cámara.

Afinidades was mostly filmed in Guamá, in Cubas Ciénaga de Zapata region, with some parts shot in a studio built in Santa Fe, Havana. It premiered on 4 December 2010 at the Festival internacional de cine de La Habana.

==Plot==
The plot is around two couples. One pair is Perugorría and Spanish actress Cuca Escribano; the other is Cruz and Gabriela Griffith. Film takes place inland on fresh water, a great opportunity to meditate about the human being and its complexities, facing emptiness and the lack of rational explanation for many of the problems of the contemporary world, sometimes appearing that the only way out is to take refuge in instincts and sex. But the result is ephemeral and the attempt has unforeseeable consequences. 

==Cast==
*Jorge Perugorría
*Vladimir Cruz		
*Cuca Escribano		
*Gabriela Griffith

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 

 
 