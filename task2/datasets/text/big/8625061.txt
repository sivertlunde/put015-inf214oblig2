Chitram
:This article is about the Telugu film. For the 1988 Malayalam film see Chithram

{{Infobox film
|  name     = Chitram
|  image          = 
|  imdb_id        = 
|  writer         = Teja
|  starring       = Uday Kiran Reema Sen Teja
|  producer       = Ramoji Rao
|  cinematography = Rasool Ellore
|  editing        = Shankar 
|  studio         = Ushakiran Movies
|  music          = R. P. Patnaik
|  country        = India
|  released       = 16th June 2000
|  language       = Telugu
|  budget         =  42 Lakhs
}} Telugu film Senthil were added. Chitram and Nuvve Kavali were the two movies which set a trend of college campus romance stories in the same year.  The movie was remade in Kannada as Chithra (film)|Chithra, starring Prabhu Devas brother Nagendra Prasad and Rekha Vedavyas in 2001 under the direction of Dinesh Baboo and the production of Ramoji Rao. The movie was a superhit in Kannada and marked the debut of the lead actors.

==Cast==
{|class="wikitable"
|-
! Actor/Actress !! Role
|- Uday Kiran Ramana
|- Reema Sen Janaki
|- Amitov Teja Cameo 
|- Chitram Srinu  Hero Friend
|}

==Plot==
Janaki (Reema Sen) and her sister are NRIs who want to join a PU college in AP. Janaki stays with her uncle and procures the admission in the same college as Ramana (Uday Kiran), a die-hard music fan. When Janaki first sees him in the music room practicing they get attracted to each other. When family members of Ramana are away, Janaki happens to come to Ramanas house wearing a saree. As she does not know how to wear saree, all she does is drape it around her body . Ramana offers to teach her how to wear a saree then accidentally he puts hand inside her saree skirt which makes them both feel shy and attracted. In the process they consummate their passion. After a few days Janaki informs Ramana that she is pregnant. Ramana, along with his friends, hires a nurse to perform abortion on Janaki. When Ramana asks Janaki to prepare for the abortion, she refuses to do so as she says she wants a company of a kid. Janaki tells him that her mother used to tell her that when she dies she will be reborn as Janakis child. Janaki is then told by the college Principal to take rest and write the exam following year. When Ramanas parents talk to Janakis uncle he blames Ramana. 

Soon Ramana and Janaki move into a new house and Ramana gets a job as a guitar player in a club. Janaki delivers a child in the hospital during Ramanas exams on which he cant concentrate and cant write anything. His lecturer tells him to study well as he has not done well in the previous tests also. But Janaki expects him to help her out in taking care of the baby. Ramana starts getting fed up of Janaki and the baby and starts refusing to change the babys diaper and even to take care of the baby for a minute when Janaki is in the kitchen. At this time Ramana loses his job in the club and Janaki questions him about his behaviour. Now angry, Ramana shouts and blames Janaki that she spoit his life, future and career. The next day when Ramana is writing his examination, Janaki brings the baby in a basket, approaches Ramana and leaves the baby in the exam hall and runs away to make Raman understand the difficulty of raising a baby alone. The invigilator holds the crying baby until Ramana has finished. Then Ramana takes the baby and goes home and feels bad about abusing Janaki and takes good care of the baby. One day when he is sleeping the baby disappears and Ramana goes searching for the child all over the city. He comes back home worried without finding the baby when Janaki returns home with the baby. Ramana apologises to her. Janaki tells him that she is pregnant again. In the ending the couple walk to college with the baby in the basket.

==References==
 
*http://www.thehindu.com/arts/cinema/article496752.ece

== External links ==
*  

 
 
 
 
 
 
 