Pie, Pie Blackbird
{{Infobox film
| name           = Pie, Pie Blackbird
| image          = 
| caption        =  Roy Mack
| producer       = 
| writer         = A. Dorian Otvos
| narrator       = 
| starring       = Nina Mae McKinney Nicholas Brothers Eubie Blake Noble Sissle
| music          = Eubie Blake Noble Sissle
| cinematography = Edwin B. DuPar
| editing        = 
| distributor    = Warner Brothers
| released       = 4 June 1932
| runtime        = 11 minutes
| country        = United States 
| language       = English
| budget         = 
}}
Pie, Pie Blackbird (1932 in film|1932) is a Vitaphone short comedy film released by Warner Brothers on June 4, 1932, starring African American musicians Nina Mae McKinney, the Nicholas Brothers, Eubie Blake, and Noble Sissle. 

A clip from the film is included in Thats Black Entertainment (1989) and the TV special Its Black Entertainment (2002). The full film is available on the DVD for Hallelujah! (1929 film)|Hallelujah! (1929) 

==References==
 

==See also==
*Vitaphone Varieties

==External links==
* 


 
 