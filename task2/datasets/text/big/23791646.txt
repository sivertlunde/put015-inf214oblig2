Katinka (film)
 
{{Infobox film
| name           = Katinka
| image          = Katinka (film).jpg
| caption        = Film poster
| director       = Max von Sydow
| producer       = Bo Christensen Katinka Faragó
| writer         = Klaus Rifbjerg
| based on       =  
| starring       = Tammi Øst
| music          = 
| cinematography = Claus Loof
| editing        = Janus Billeskov Jansen
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = Denmark Sweden
| language       = Danish
| budget         = 
}} Best Film Best Director.   

==Cast==
* Tammi Øst as Katinka
* Ole Ernst as Bai
* Kurt Ravn as Wilhelm Huus
* Erik Paaske as Linde
* Anne Grete Hilding as Mrs. Linde
* Tine Miehe-Renard as Agnes
* Ghita Nørby as Helene
* Birthe Backhausen as Mrs. Abel
* Bodil Lassen as Louise Abel
* Vibeke Hastrup as Ida Abel
* Henrik Koefoed as Bentsen
* Kim Harris as Togfører
* Kjeld Nørgaard as Kiær
* Birgitte Bruun as Emma
* Dick Kaysø as Andersen
* Paul Hüttel as Doktor
* Søren Sætter-Lassen as The Lieutenant

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 