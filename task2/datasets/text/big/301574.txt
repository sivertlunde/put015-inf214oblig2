The Little Mermaid (1989 film)
 
 
 
 
{{Infobox film
| name = The Little Mermaid
| image = Movie poster the little mermaid.jpg
| caption = Theatrical teaser poster
| director =  
| producer =  
| screenplay =  
| story = {{plain list|
*John Musker
*Ron Clements
*Howard Ashman
*Gerrit Graham
*Sam Graham
*Chris Hubbell
}}
| based on =  
| starring = {{plain list|
*Rene Auberjonois
*Christopher Daniel Barnes
*Jodi Benson Pat Carroll
*Paddi Edwards
*Buddy Hackett
*Jason Marin
*Kenneth Mars
*Edie McClurg
*Will Ryan Ben Wright
*Samuel E. Wright
}}
| music = Alan Menken
| studio =   Buena Vista Pictures
| released =  
| budget = $40 million Stewart, James B. (2005). DisneyWar, p. 104. ISBN 0-684-80993-1. Simon & Schuster. Retrieved June 4, 2007. 
| gross = $211.3 million 
| runtime = 82 minutes
| country = United States
| language = English
}}
 musical fantasy Danish The fairy tale Pat Carroll, Samuel E. Wright, Jason Marin, Kenneth Mars, Buddy Hackett, and René Auberjonois.
 Walt Disney Animated Classics series, The Little Mermaid was released to theaters on November 17, 1989, to largely positive reviews, garnering $84 million at the domestic box office during its initial release,  and $211 million in total lifetime gross worldwide.  After the success of the studios 1988 film Who Framed Roger Rabbit, The Little Mermaid is given credit for breathing life back into the art of Disney animated feature films after a string of critical or commercial failures produced by Disney that dated back to the early 1970s. It also marked the start of the era known as the Disney Renaissance.
 stage adaptation Broadway January 10, 2008.  

== Plot ==
  Scuttle the King Triton, Sebastian that contact between merpeople and humans is forbidden.
 Flotsam and Ursula the sea witch.

Ursula makes a deal with Ariel to transform her into a human for three days in exchange for Ariels voice, which Ursula puts in a nautilus shell. Within these three days, Ariel must receive the "kiss of true love" from Eric; otherwise, she will transform back into a mermaid and belong to Ursula. However, if Ariel gets Eric to kiss her, she will remain a human permanently. Ariel agrees and is then given human legs and taken to the surface by Flounder and Sebastian. Eric finds Ariel on the beach and takes her to his castle, unaware that she had saved his life earlier, he and his staff assuming her to be a mute shipwreck survivor. Ariel spends time with Eric, and at the end of the second day, they almost kiss but are thwarted by Flotsam and Jetsam. Angered at their narrow escape, Ursula disguises herself as a young woman named Vanessa and appears onshore singing with Ariels voice. Eric recognizes the song and, in her disguise, Ursula casts a hypnotic enchantment on Eric.

The next day, Ariel finds out that Eric will be married to the disguised Ursula. Scuttle discovers that Vanessa is actually Ursula in disguise, and informs Ariel who immediately pursues the wedding barge. Sebastian informs Triton, and Scuttle disrupts the wedding with the help of various animals. In the chaos, the nautilus shell around Ursulas neck is broken, restoring Ariels voice and breaking Ursulas enchantment over Eric. Realizing it was Ariel that saved his life, Eric rushes to kiss her, but the sun sets and Ariel transforms back into a mermaid. Ursula reveals herself and kidnaps Ariel. A furious Triton confronts Ursula and demands that she release Ariel, but the deal is inviolable. At Ursulas urging, the king agrees to take Ariels place as Ursulas prisoner, giving up his trident. Before Ursula can use the trident, Eric distracts her with a harpoon. Ursula tries to strike down Eric, but Ariel attacks Ursula, who inadvertently kills Flotsam and Jetsam. In her rage, Ursula uses the trident to grow into monstrous proportions.

Ariel and Eric reunite on the surface just before Ursula grows past and towers over the two. She then gains full control of the entire ocean, creating a storm and bringing sunken ships to the surface. Just as Ursula attempts to kill Ariel, Eric steers a wrecked ship towards Ursula, stabbing her to death with the ships splintered bowsprit. With Ursula destroyed, Triton and the other polyps in Ursulas garden revert into their original forms. Realizing that Ariel truly loves Eric, Triton willingly changes her from a mermaid into a human and blesses her marriage to Eric.

== Cast ==
  Princess Ariel and Vanessa, Ursulas human alter-ego.   
*Christopher Daniel Barnes as Prince Eric    Pat Carroll Ursula  Sebastian  Flounder 
*Kenneth Mars as King Triton 
*Buddy Hackett as Scuttle  Ben Wright as Grimsby 
*Paddi Edwards as Flotsam and Jetsam 
*Edie McClurg as Carlotta the maid 
*Kimmy Robertson and Caroline Vasicek as Ariels Sisters 
*Will Ryan as Harold the Seahorse 
*Frank Welker as Max the Sheepdog 
*René Auberjonois as Chef Louis 

== Production ==

=== Story development === package film Snow White and the Seven Dwarfs in the late 1930s, but was delayed due to various circumstances. 
 fairy tale treatment of "Mermaid" to Disney CEO Jeffrey Katzenberg at a "gong show" idea suggestion meeting. Katzenberg passed the project over, because at that time the studio was in development on a sequel to their live-action mermaid comedy Splash (film)|Splash (1984) and felt The Little Mermaid would be too similar a project.  The next day, however, Katzenberg approved of the idea for possible development, along with Oliver & Company. While in production in the 1980s, the staff found, by chance, original story and visual development work done by Kay Nielsen for Disneys proposed 1930s Anderson feature.  Many of the changes made by the staff in the 1930s to Hans Christian Andersens original story were coincidentally the same as the changes made by Disney writers in the 1980s. (2006) Treasures Untold: The Making of Disneys The Little Mermaid  . Bonus material from The Little Mermaid: Platinum Edition DVD. Walt Disney Home Entertainment. 
 Little Shop of Horrors, teamed up to compose the entire song score. In 1988, with Oliver out of the way, Mermaid was slated as the next major Disney release.   

=== Animation === Sherri Lynn Stoner, a former member of Los Angeles Groundlings improvisation comedy group, acted out Ariels key scenes. 
 The Fox and the Hound and Professor Ratigan in The Great Mouse Detective. Keane, however, was assigned as one of the two lead artists on the petite Ariel and oversaw the "Part of Your World" musical number. He jokingly stated that his wife looks exactly like Ariel "without the fins."  The characters body type and personality were based upon that of Alyssa Milano, then starring on TVs Whos the Boss? and the effect of her hair underwater was based on both footage of Sally Ride when she was in space,  and scenes of Stoner in a pool for guidance in animating Ariels swimming. 
 Pat Carroll Nancy Wilson, Rosanne Barr|Roseanne, Charlotte Rae, and Elaine Stritch were considered for the part.  Stritch was eventually cast as Ursula, but clashed with Howard Ashmans style of music production and was replaced by Carroll.    Various actors auditioned for additional roles in the film, including Jim Carrey for the role of Prince Eric, and comedians Bill Maher and Michael Richards for the role of Scuttle. 

The underwater setting required the most special effects animation for a Disney animated feature since Fantasia (1940 film)|Fantasia in 1940. Effects animation supervisor Mark Dindal estimated that over a million bubbles were drawn for this film, in addition to the use of other processes such as airbrushing,  backlighting, superimposition, and some computer animation. The artistic manpower needed for Mermaid required Disney to farm out most of the underwater bubble effects animation in the film to Pacific Rim Productions, a China-based firm with production facilities in Beijing.  An attempt to use Disneys famed multiplane camera for the first time in years for quality "depth" shots failed because the machine was reputedly in dilapidated condition. The multiplane shots were instead photographed at an outside animation camera facility. (2006) Audio Commentary by John Musker, Ron Clements, and Alan Menken Bonus material from The Little Mermaid: Platinum Edition  . Walt Disney Home Entertainment. 
 plotted as line art to cels and painted traditionally. 

=== Music ===
 
 The Wizard of Oz when appealing to Katzenberg. Keane pushed for the song to remain until the film was in a more finalized state. During a second test screening, the scene, now colorized and further developed, tested well with a separate child audience, and the musical number was kept. 

== Release ==
The film was originally released on November 17, 1989, followed by a November 14, 1997, reissue. After the success of the 3D re-release of   from September to October 2013.  On September 20, 2013, The Little Mermaid began playing in select theaters where audiences could bring iPads and use an app called Second Screen Live.  The film was also screened out of competition at the 1990 Cannes Film Festival.   

=== Home media ===
In a then atypical and controversial move for a new Disney animated film, The Little Mermaid was released as part of the Walt Disney Classics line of VHS and Laserdisc home video releases in May 1990, six months after the release of the film.   Before Mermaid, only a select number of Disneys catalog animated films had been released to home video, as the company was afraid of upsetting its profitable practice of theatrically reissuing each film every seven years.  Mermaid became that years top-selling title on home video, with over 10 million units sold (including 7 million in its first month).  This success led future Disney films to be released soon after the end of their theatrical runs, rather than delayed for several years. 
 Masterpiece Collection and included a bonus music video of Jodi Benson singing "Part of Your World" during the end credits, replacing "Under the Sea" as the end credit song.  The VHS sold 13 million units and ranked as the third best-selling video of the year.  
 Walt Disney Walt Disney Diamond Editions line.  

== Reception ==

=== Box office ===
Early in the production of The Little Mermaid, Jeffrey Katzenberg cautioned Ron Clements, John Musker, and their staff, reminding them that since Mermaid was a "girls film", it would make less money at the box office than Oliver & Company, which had been Disneys biggest animated box office success in a decade.   However, by the time the film was closer to completion, Katzenberg was convinced Mermaid would be a hit and the first animated feature to earn more than $100 million and become a "blockbuster (entertainment)|blockbuster" film.  

During its original 1989 theatrical release, Mermaid earned $84,355,863 at the North American box office,    falling just short of Katzenbergs expectations but earning 64% more than Oliver.  The Little Mermaid was reissued in theaters on November 14, 1997, on the same day as Anastasia (1997 film)|Anastasia, a Don Bluth animated feature for Fox Animation Studios. The reissue brought $27,187,616 in additional gross. The film also drew $99.8 million in box office earnings outside the United States and Canada between both releases, resulting in a total international box office figure of $211 million. 

=== Critical reception === Ursula in particular, as well as its animation, stating that the animation "proves lush and fluid, augmented by the use of shadow and light as elements like fire, sun and water illuminate the characters." They also praised was the musical collaboration between Howard Ashman and Alan Menken "whose songs frequently begin slowly but build in cleverness and intensity." 

Todd Gilchrist of IGN wrote a positive review of the film, stating that the film is "an almost perfect achievement." Gilchrist also praised how the film revived interest in animation as it was released at a time when interest in animation was at a lull. {{cite web| publisher=IGN|date=October 3, 2006| title=
Double Dip Digest: The Little Mermaid|url=http://dvd.ign.com/articles/737/737058p1.html| accessdate=December 23, 2009}}  Hal Hinson of The Washington Post wrote a mixed review of the film, referring to it as a "likably unspectacular adaptation of the Hans Christian Andersen classic." Hinson went on to write that the film is average even at its highest points. He wrote that while there is nothing wrong with the film, it would be difficult for children to identify with Ariel and that the characters seemed bland. Hinson concluded his review saying that the film is "accomplished but uninspiring, The Little Mermaid has enough to please any kid. All thats missing is the magic." {{cite news|newspaper=The Washington Post|date=November 17, 1997| title=
The Little Mermaid review|url=http://www.washingtonpost.com/wp-srv/style/longterm/movies/review97/littlemermaidhin.htm| accessdate=December 24, 2009}}  Empire (magazine)|Empire gave a positive review of the film, stating that "  a charmer of a movie, boasting all the ingredients that make a Disney experience something to treasure yet free of all the politically correct, formulaic elements that have bogged down the more recent productions." 
 Beauty and the Beast, and The Lion King, respectively- scored above it in the poll even after the update.)  In 2011, Richard Corliss of Time (magazine)|TIME Magazine named it one of "The 25 All-TIME Best Animated Films". 
 Sleeping Beauty Beauty and the Beast (1991), Aladdin (1992 Disney film)|Aladdin (1992), and The Lion King (1994). The staff increased from 300 members in 1988 to 2,200 in 1999 spread across three studios in Burbank, California; Lake Buena Vista, Florida; and Montreuil, Seine-Saint-Denis, France. {{Citation  | last = Moore  | first = Roger  | author-link =  | last2 = | first2 =
| author2-link =| title = AFTER THE MAGIC ; SCORES OF FORMER DISNEY ANIMATORS AND THEIR COLLEAGUES HAVE DISPERSED TO LAUNCH THEIR OWN STUDIOS, SEEK NEW CAREERS AND DISCOVER NEW IDENTITIES – DETERMINED TO LAND ON THEIR FEET.| newspaper = Orlando Sentinel| pages = F1| date = June 20, 2004| url = http://pqasb.pqarchiver.com/orlandosentinel/access/653286501.html?FMT=ABS&FMTS=ABS:FT&date=Jun+20%2C+2004&author=Roger+Moore%2C+Sentinel+Movie+Critic&pub=Orlando+Sentinel&edition=&startpage=F.1&desc=AFTER+THE+MAGIC+%3B+SCORES+OF+FORMER+DISNEY+ANIMATORS+AND+THEIR+COLLEAGUES+HAVE+DISPERSED+TO+LAUNCH+THEIR+OWN+STUDIOS%2C+SEEK+NEW+CAREERS+AND+DISCOVER+NEW+IDENTITIES+--+DETERMINED+TO+LAND+ON+THEIR+FEET.| accessdate = May 8, 2010 }} 

In addition, Mermaid signaled the re-establishment of the musical film format as a standard for Disney animated films. The majority of Disneys most popular animated films from the 1930s on had been musicals, though by the 1970s and 1980s the role of music had been de-emphasized in the films.  1988s Oliver and Company had served as a test of sorts to the success of the musical format before Disney committed to the Broadway-style structure of The Little Mermaid. 

=== Accolades === Best Song Best Score. Best Picture—Comedy or Musical, and won the awards for Best Song ("Under the Sea") and Best Score.   

In addition to the box office and critical success of the film itself, the Mermaid soundtrack album earned two awards at the   and the   in September 1990 for shipments of two million copies of the soundtrack album, an unheard of feat for an animated film at the time.  . RIAA.com.    To date, the soundtrack has been certified six times platinum. 
 Best Original Best Song for Alan Menken and Howard Ashmans "Under the Sea", sung by Samuel E. Wright in a memorable scene.    Another song from the film, "Kiss the Girl," was nominated but lost to "Under the Sea."  The film also won two Golden Globes for Best Original Score as well Best Original Song for "Under the Sea." It was also nominated in two other categories, Best Motion Picture and another Best Original Song. Alan Menken and Howard Ashman also won a Grammy Award in 1991 for "Under the Sea." 

;American Film Institute Lists
*AFIs 100 Years...100 Passions—Nominated 
*AFIs 100 Years...100 Heroes and Villains:
**Ursula—Nominated Villain 
*AFIs 100 Years...100 Songs:
**Under the Sea—Nominated 
*AFIs Greatest Movie Musicals—Nominated 
*AFIs 10 Top 10—Nominated Animated Film 

=== Controversy ===
Controversy arose regarding the artwork for the cover for the Classics VHS cassette when the film was first released on video when close examination of the artwork revealed an oddly shaped structure on the castle, closely resembling a human penis.       Disney and the cover designer insist it was an accident, resulting from a late night rush job to finish the cover artwork. The questionable object does not appear on the cover of the second releasing of the movie.  A second allegation is that a clergyman is seen with an erection during a scene late in the film.      The clergyman is a short man, dressed in Bishops clothing, and a small bulge is slightly noticeable in a few of the frames that are actually later shown to be the stubby-legged mans knees, but the image is small and is very difficult to distinguish. The combined incidents led an Arkansas woman to file suit against The Walt Disney Company in 1995, though she dropped the suit two months later.           

== See also ==
* Mermaids in popular culture
* List of Disney theatrical animated features
 

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 