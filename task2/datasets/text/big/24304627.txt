Angel (manga)
 
{{Infobox animanga/Header
| name            = Angel
| image           =  
| caption         = Angel #3 published by Shogakukan (1990)
| ja_kanji        =
| ja_romaji       =
| genre           = Erotic fiction|Erotic, Comedy
}}
{{Infobox animanga/Print
| type            = manga
| author          = U-Jin
| publisher       = Shogakukan / Cybele Publishing  While the manga started publishing in 1988, it was interrupted in 1991 after several controversies. As such, Shogakukan edition is incomplete and the manga was completed in tankōbon only. 
| demographic     = Shōnen manga|Shōnen, Seinen manga|Seinen While the manga was originally published in a shōnen magazine, it was later considered a seinen manga. 
| magazine        = Weekly Young Sunday 
| first           = 1988
| last            = 1993 
| volumes         = 7  Shogakukan version had only three volumes, but the complete version by Cybele had a total of 7, although with a different length than the other previous volumes 
| volume_list     =
}}
{{Infobox animanga/Video
| type            = ova
| director        =
| producer        =
| writer          =
| music           =
| studio          = Green Bunny
| released        = October 25, 1990
| runtime         = 45 minutes
}}
{{Infobox animanga/Game
| developer       =
| publisher       = Cocktail Soft
| genre           = Simulation
| ratings         =
| platforms       = NEC PC-9801
| released        = October 1, 1993
}}
{{Infobox animanga/Video
| type            = ova
| title           = Shin Angel
| director        = Kaoru Tooyoka (1, 2, 3, 4)
| producer        = Kotaro Ran   Saburo Omiya (3, 4)   Osamu Echuu (2)   Osamu Koshinaka (3)
| writer          = Koji Sakakibara (4)
| music           = Keisaku Irie Triple X
| licensor        =  
| first           = October 21, 1991
| last            = November 22, 1995
| runtime         = 46m, 29m, 29m, 33m ,31m
| episodes        = 5
| episode_list    =
}}
{{Infobox animanga/Video
| type            = live film
| title           = Angel: Ichiban Saisho wa Anata ni A.Ge.Ru
| director        =
| producer        =
| writer          =
| music           =
| studio          = dez
| licensor        =  
| released        = February 14, 1997
| runtime         = 66 minutes
}}
{{Infobox animanga/Video
| type            = live film
| title           = Angel: Shotai Fumei no Joo-sama!? (Hen)
| director        =
| producer        =
| writer          =
| music           =
| studio          = dez
| licensor        =  
| released        = April 4, 1997
| runtime         = 70 minutes
}}
{{Infobox animanga/Print
| type            = manga
| title           = Angel: the women whom delivery host Kosuke Atami healed
| author          = U-Jin
| publisher       = Nihon Bungeisha
| demographic     = Seinen manga|Seinen
| magazine        = Weekly Manga Goraku
| first           = 2006
| last            = 2008
| volumes         = 5
| volume_list     =
}}
{{Infobox animanga/Print
| type            = manga
| title           = Angel season 2: the women whom delivery host Kosuke Atami healed
| author          = U-Jin
| publisher       = Nihon Bungeisha
| demographic     = Seinen manga|Seinen
| magazine        = Weekly Manga Goraku
| first           = 2008
| last            = 2010
| volumes         = 5
| volume_list     =
}}
 

Angel is a hentai manga series written and illustrated by U-Jin. The original manga series met with controversy in 1990–1991 in Japan and was retired from its magazine serialization. It was adapted into an OVA of the same name and a sequel called Angel: the women whom delivery host Kosuke Atami healed. The manga was also succeeded by a manga called Angel: the women whom delivery host Kosuke Atami healed, and succeeded again by another manga called Angel: the women whom delivery host Kosuke Atami healed season 2.

==Plot==

===Story===
 
The story is about the adventures of sexually driven boy Kosuke Atami and his friend Shizuka. Its a gag comedy with strong touches of eroticism, where the main concept is how Kosuke helps people through the story.

===Characters===
 
* 
* 
* 

==Original manga publication==
Angel, also known as Angel: Highschool Sexual Bad Boys & Girls Story (prior to the Japanese controversy), Angel: Delight Slight Light Kiss Story (after it resumed serialization but before its complete ban) and currently Angel: Sexual boys and girls highschool story (in order to differentiate it from the sequels), started its publication in the magazine Weekly Young Sunday published by Shogakukan. Because of the controversy, its serialization was interrupted in 1991 and only three volumes were published by Shogakukan. It was later fully reprinted by Cybele Publishing (also known as Cybele Shuppan), which also included two new volumes for a total of 7 volumes, although Cybele volumes had in fact less pages than Shogakukan volumes. In the cover of the Cybele volumes, the legend "We came back!" can be read above the title, in reference to the incident that provoked its temporal ban. The manga was published in Taiwan by company Li-Yi, in France by Tonkam and in Spain by Norma Editorial in Cybeles edition.

===Volumes===
*Shogakukan (Young Sunday Comics, 1989–1990)
{| class="wikitable"
| Japanese release date || Vol. || ISBN
|-
|   || 1 ||  
|-
|   || 2 ||  
|-
|   || 3 ||  
|}

*Cybele Publishing (Cybele Comics, 1993) This version includes several autostereograms. Since foreign versions were based on this one, they also include the autostereograms.
{| class="wikitable"
| Japanese release date || Vol. || ISBN
|-
|   || 1 ||  
|-
|   || 2 ||  
|-
|   || 3 ||  
|-
|   || 4 ||  
|-
|   || 5 ||  
|-
|   || 6 ||  
|-
|   || 7 ||  
|}

*Cybele Publishing (Cybele Bunko, 1995–1996)
{| class="wikitable"
| Japanese release date || Vol. || ISBN
|-
|   || 1 ||  
|-
|   || 2 ||  
|-
|   || 3 ||  
|-
|   || 4 ||  
|-
|   || 5 ||  
|-
|   || 6 ||  
|}

*Ohzora Publishing (also known as Chu Shuppan) (Missy Comics, 2007–2008)
{| class="wikitable"
| Japanese release date || Vol. || ISBN
|-
|   || 1 ||  
|-
|   || 2 ||  
|-
|   || 3 ||  
|-
|   || 4 ||  
|-
|   || 5 ||  
|}

Along with this volumes, there is another collection which combines Angel with  Konai Shasei, another manga by U-Jin. The collection is simply called  . Instead of a regular numbering, each volume has a different subtitle.

*Ohzora Publishing (Missy Comics, 2006–2007)
{| class="wikitable"
| Japanese release date || Vol. || ISBN
|-
|   || Densha Bishōjo Hen ||  
|-
|   || Gakuen Bishōjo Hen ||  
|-
|   || Hokago Bishōjo Hen ||  
|-
|   || Koisuru Bishōjo Hen ||  
|-
|   || Imouto Kei Bishōjo Hen ||  
|-
|   || Junkoi Bishōjo Hen ||  
|-
|   || Shiroi Bishōjo Hen ||  
|}

The series is also available in ebook format by ebookjapan.   

==Media==

===OVAs===
 

==== Angel ==== LD by Pioneer LDCs brand Humming Bird.    It was later re-released by Tairiku Shobo.     A DVD of the anime was released in   by Happinet Pictures (a division of Namco Bandai) with standard number GBBH-1896.    

==== New Angel ==== Triple X.

{|class="wikitable" width="98%"
|- style="border-bottom: 3px solid #CCF"
! # !! Title !! Length !! Original release date
|-
{{Japanese episode list
 | EpisodeNumber = 1
 | EnglishTitle = New Angel
 | KanjiTitle = 新・エンジェル
 | RomajiTitle = shin enjeru
 | Aux1            = 46 mins.
 | OriginalAirDate =  
 | ShortSummary    = While taking obscene pictures, Kozuke and his classmate, Shinoyama meet a girl who smiles at Kozuke. Shinoyama snaps a photo and they later find out that she is supposedly a ghost. Shinoyama spreads the rumor that Kozuke is cursed and everyone at school avoids him, excepting Shizuka. She suggests that they go to a medium for help. While Shizuka waits, Kozuke finds that the medium is an attractive woman with whom he must make physical contact (sex) in order to remove the curse. But the curse is not removed. Later, a guy tells Kozuke that the ghost girl wants to meet with him. While meeting the girl Kozuke faints, waking up to find that the girl is giving him a blowjob and they have sex. Kozuke discovers that the girl is indeed a ghost. He meets Shizuka outside a temple and then goes after Shinoyama for spreading the false rumor.
}}
{{Japanese episode list
 | EpisodeNumber = 2
 | EnglishTitle = Flying Angel
 | KanjiTitle = 空飛ぶ天使
 | RomajiTitle = soratobu tenshi
 | Aux1            = 29 mins.
 | OriginalAirDate =  
 | ShortSummary    = While meeting with Yamada, Kozuke sees Ochiai Miki trying to commit suicide. Kozuke convinces her to have sex with him before that. He finds out that Kawamura Kunihiko threw out Mikis love note without reading it. With the help of Shizuka, Kozuke finds that Kawamura never got Mikis note. Kozuke later discovers that Kawamura also wrote his own love note. They try to find Miki, but the girl behind the problems stops Kozuke and takes off with Kawamura. She later has sex with Kozuke to obtain pictures for blackmailing. When Shizuka goes to find Kawamura while Kozuke goes to watch Miki, the mysterious girl tries to blackmail Shizuka. Shizuka instead beats her up to find Kawamuras location. As part of Kozukes plan, Miki falls of a plane with a parachute, coming to the conclusion that she doesnt want to die. Afterwards, Kawamura and Miki confess their feelings.
}}
{{Japanese episode list
 | EpisodeNumber = 3
 | EnglishTitle = Blue Experience
 | KanjiTitle = 青い体験
 | RomajiTitle = aoi taiken
 | Aux1            = 29 mins.
 | OriginalAirDate =  
}}
{{Japanese episode list
 | EpisodeNumber = 4
 | EnglishTitle = A Bride for a Week
 | KanjiTitle = 一週間の花嫁
 | RomajiTitle = isshuukan no hanayome
 | Aux1            = 33 mins.
 | OriginalAirDate =  
}}
{{Japanese episode list
 | EpisodeNumber = 5
 | EnglishTitle = The Last Night
 | KanjiTitle = 最後の夜
 | RomajiTitle = saigo no yoru
 | Aux1            = 31 mins.
 | OriginalAirDate =  
}}
|}
 SoftCel Pictures. It was originally released in VHS format in 1995, 1996 and 1998 and it was released in both uncut and edited versions for the first four episodes. {{cite web
| url         = http://softcelpics.com/newang.htm
| title       = New Angel
| accessdate  = 2009-09-10
| author      =
| date        = 1997-04-04
| work        =
| publisher   = SoftCel Pictures
| location    = USA
| archiveurl        = http://web.archive.org/web/19970404162512/http://softcelpics.com/newang.htm
| archivedate        = 1997-04-04
}}     It was also released in DVD format by the same company in two volumes, the first released in   and the second in  .

Shin Angel was also released in France, in VHS by Katsumi vidéo and in DVD by Anime Erotik,    and also in Spain in VHS and DVD by Manga Films    (although only the first 4 episodes were released by Manga Films)    and on TV by Arait Multimedia. 

===Video games===
A video game for the NEC PC-9801 based on Angel and with the same title was released in   by the Japanese company Cocktail Soft.      

===Live-action films===
Two adult live-action films based on the manga were produced. The first one,  , was released in   and the second one,  , was released in  . Both films have the participation of Japanese Adult Video actresses Yui Kawana and Mizuki Kanno and were released by the company dez.      

Both films were released in the US by Kitty Media in DVD format in   as a single release called Angel Collection.      

===Manga sequels===
Angel: the women whom delivery host Kosuke Atami healed
In 2006, a sequel of the original manga started in the magazine Weekly Manga Goraku published by Nihon Bungeisha, titled Angel: the women whom delivery host Kosuke Atami healed, also known as   and more commonly simply as Angel. Also created by U-Jin, this manga follows the new adventures of Kosuke Atami, now a divorced 34-year-old man who works as a host and helps people in a similar way as he did as a highschooler.

{{Graphic novel list/header
 | OneLanguage = yes
}}
{{Graphic novel list
 | VolumeNumber    = 1
 | RelDate         =  
 | ISBN            = 978-4-537-10555-1
}}
{{Graphic novel list
 | VolumeNumber    = 2
 | RelDate         =  
 | ISBN            = 978-4-537-10654-1
}}
{{Graphic novel list
 | VolumeNumber    = 3
 | RelDate         =  
 | ISBN            = 978-4-537-10708-1
}}
{{Graphic novel list
 | VolumeNumber    = 4
 | RelDate         =  
 | ISBN            = 978-4-537-10790-6
}}
{{Graphic novel list
 | VolumeNumber    = 5
 | RelDate         =  
 | ISBN            = 978-4-537-10839-2
}}
 

Following the previous sequel, in 2008, also in Nihon Bungeishas Weekly Manga Goraku, the manga titled Angel season 2: the women whom delivery host Kosuke Atami healed, more commonly known as Angel season 2, was released. Also done by U-Jin, the manga follows the same premise as the previous manga series. The manga is currently ongoing.

{{Graphic novel list/header
 | OneLanguage = yes
}}
{{Graphic novel list
 | VolumeNumber    = 1
 | RelDate         =  
 | ISBN            = 978-4-537-10912-2
}}
{{Graphic novel list
 | VolumeNumber    = 2
 | RelDate         =  
 | ISBN            = 978-4-537-10960-3
}}
{{Graphic novel list
 | VolumeNumber    = 3
 | RelDate         =  
 | ISBN            = 978-4-537-12538-2
}}
{{Graphic novel list
 | VolumeNumber    = 4
 | RelDate         =  
 | ISBN            = 978-4-537-12597-9
}}
 

==Reception==

===Controversy in Japan===
 Saitama serial PTAs managed to force the suspension of the manga for a while    and volume 3 became the last by Shogakukan    and the tankōbon became banned. The problem centered in housewives who believed that Angel was too sexually explicit for a shōnen publication and that the manga had become pretty popular.    The incident with Angel eventually lead to the creation of the  . 

While the manga was still being serialized in the magazine, U-Jin included a "message from the author" chapter as a form of protest. When it temporary resumed serialization, the sexual content was reduced and the subtitle changed to Delight Slight Light Kiss Story, until it was finally banned.

Eventually, publication resumed years later with Cybele Publishing, which re-published Angel since the beginning  to eventually publish the complete manga, now labeled as  .

===Controversy in France===
Upon the release of the third volume in France, Angel was banned from exhibition in stores. The argument of anti-manga people, relayed by the press at the time, was that the manga is dangerous for youth because of eroticism and violence it diffuses. Tonkam, however, for which Angel was the first erotic manga, finished the translation of the 7 volumes.

===New Angel===
Stig Høgset, writing for THEM Anime Reviews, found New Angel technically good, as the art and animation were good, but felt the story was "boring and stupid".   Chris Beveridge noted that the episodes were not scripted by U-Jin, thus lacking some of his distinctive style,  and feels that it is an example of a hentai series which merely "spices up" a "regular" anime series.   Bamboo Dong, writing for Anime News Network, felt that the writing of the series made it "fun to watch", with humour and interesting backgrounds for the female characters, and noted that the sex scenes are "pretty graphic".   Bamboo Dong felt the transition between non-sex scenes and sex scenes was not smooth in the second volume. 

==Notes==
 

==References==
 

==External links==
* 
* 
*    at ebookjapan
*  at Kitty Media

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 