The Cat in the Hat (film)
{{Infobox film
| name               = Dr. Seuss The Cat in the Hat
| image              = Cat in the hat.jpg
| image_size         = 
| alt                = 
| caption            = Theatrical release poster
| director           = Bo Welch
| producer           = Brian Grazer
| screenplay         = Alec Berg David Mandel Jeff Schaffer
| based on           =   Sean Hayes
| narrator           = Victor Brandt David Newman
| cinematography     = Emmanuel Lubezki Don Zimmerman
| studio             = Imagine Entertainment
| distributor        = Universal Pictures       DreamWorks Pictures    
| runtime            = 82 minutes
| released           =  
| country            = United States
| language           = English
| budget             = $109 million
| gross              = $134 million
}}
 fantasy comedy The Grinch.

The idea was originally conceived in 2003, when Tim Allen was initially cast as the Cat, but he dropped his role due to work on The Santa Clause 2, and the role was later given to Mike Myers. Filming took place in California for three months. While the basic plot parallels that of the book, the film filled out its 82 minutes by adding new subplots and characters quite different from those of the original story, similar to the feature film adaptation of How the Grinch Stole Christmas.
 book of the same name.

==Plot==
Conrad and Sally Walden live in the city of Anville with their single mother, Joan. Joan works for neat-freak Hank Humberfloob, and is hosting an office party at her house. One day, she is called back to the office, leaving the kids with Mrs. Kwan, a lethargic babysitter, and forbidding them to enter the living room, which is being kept pristine for the upcoming party. Joan is also dating their next-door neighbor, Lawrence Quinn, much to Conrads dismay. Lawrence is constantly on the lookout for any mischief Conrad may be up to, as he wants nothing more than to send him away to military school, as Conrad has earned the reputation of "Rebellion|troublemaker", while his sister is characterized as "perfect and well-behaved".

Once their mother leaves, and Mrs. Kwan has fallen asleep, Sally and Conrad discover a humanoid, oversized talking Cat in a Hat in their house. The Cat wants them to learn to have fun, but the childrens pet Fish doesnt want the cat around when Joan is away. In a series of antics, the Cat ruins Joans best dress, jumps on the living rooms couch, and bakes cupcakes that explode. In the process, he even releases two troublemaking Things from a crate that he explains is actually a portal from their world to his. The Cat tells Conrad that he only has one rule: never open the crate. The Cat tells the Things to fix Joans dress, but they end up wrecking the house instead. Despite the Cats warning, Conrad picks the lock anyway. When the crates lock attaches itself to the collar of the family dog, Nevins, which then escapes, the Cat and the kids go out to find it. They are almost discovered by some children from a nearby party, during which the Cat hides by pretending to be a Piñata|piñata and is subsequently beaten.

Meanwhile, Lawrence is revealed to be an unemployed slob with false teeth and is in financial ruin, having been showing off the impression that hes a successful businessman in the hopes of marrying to Joan and sponging off of her. Lawrence sees Nevins running across the street and sees this as an opportunity for Joan to send Conrad to military school and allow him to move in, so he takes the dog with him. The Cat and the kids are witness to this and, using the Cats super-powered car, they follow Lawrence into town but end up crashing the car. Lawrence goes to see Joan, but the Cat intervenes and tricks Lawrence into handing over the dog and he and the kids escape. They later see Lawrence driving home with Joan, but Conrad uses Things 1 and 2 to stall her by posing as police officers, giving them time to get back using Lawrences car. However, he sees the group drive past and races back to the house, telling Joan to meet him there.

During this time, "the mother of all messes" has emitted from the unlocked crate and enters the house. Lawrence catches the kids out the front and pushes them into the house, where they find it surprisingly immaculate. A hidden Cat then reveals himself to Lawrence who stumbles back in fear, tearing through a wall and falling off a cliff, revealing the Cats world. The trio navigate their way through the oversized house and find the crate sucking up things that disappears forever once gone through. Sally is nearly sucked up but Conrad manages to put the lock back on the crate to save her and the house. It returns to its normal proportions but immediately falls apart. The Cat then tells the kids that he had planned the whole day, including making not opening the crate his one rule, as he knew Conrad could not resist and also admits he never really lost his magic hat. The kids angrily tell the Cat to leave the house, and then brace themselves for their mothers arrival. However, the Cat happily returns to clean up his mess with a great cleaning contraption much to Conrad and Sallys surprise and delight. Afterwards, when all is restored, the Cat says goodbye to Conrad and Sally as they plead with him not to go but he departs just as Joan is coming in. Lawrence arrives, thinking he has busted the kids, but when Joan sees the clean house (and a messy Lawrence), she disbelieves him, and dumps him. When her party is successful, Joan and her kids play in the living room by jumping on the couch and having fun.

The film ends as the Cat and Thing 1 and Thing 2 walk off into the sunset.

==Cast== the 1971 TV special.
* Alec Baldwin as Lawrence "Larry" Quinn, the Waldens pompous, lazy, unemployed next-door neighbor. He is allergic to cats, steals food from the Waldens, and is determined to both marry Joan for her wealth and send Conrad to military school to straighten up his behavior. Larry sending Conrad to military school was cancelled after Joan dumped him.
* Spencer Breslin as Conrad Walden, Joans destructive and misbehaved son. As a running gag, the Cat constantly calls him names other than Conrad, due to his lack of name in the original book and TV special.
* Dakota Fanning as Sally Walden, Joans dull, well-behaved, and rule-obeying daughter.
* Kelly Preston as Joan Walden, Conrad and Sallys mother, and a real-estate agent.
* Amy Hill as Mrs. Kwan, an elderly Asian woman with a thick Chinese accent that gets hired to watch the kids, though she sleeps through her job. Her weight and sleep serves as a running gag. Sean Hayes as Mr. Hank Humberfloob, Joans boss. He is also the voice of the family Fish.
* Danielle Chuchran and Taylor Rice as Thing One, and Brittany Oaks and Talia-Lynn Prairie as Thing Two; two gibbering trouble-making creatures that the Cat brings in with him. Dan Castellaneta provided the voices for both Things.
* Steven Anthony Lawrence as Dumb Schweitzer
* Paris Hilton as a female club-goer
* Bugsy as Nevins, the Waldens pet dog. Frank Welker provided his voice. Candace Dean Brown as a secretary who works for the Humberfloob Real Estate.
* Daran Norris as the Announcer
* Clint Howard as Kate the Caterer; makes Chocolate brownie|brownies, cakes, and pies for parties, weddings, funerals, and graduations.
* Paige Hurd as Denise, who doesnt speak to Sally anymore, not long after she talked back to her. She never invited Sally to her birthday party either.
* Stephen Hibbert as Jim McFinnigan. Touching Humberfloobs hand, Jim gave him a handshake which results in being fired.
* Roger Morrisey as Mr. Vompatatat
* Victor Brandt as the Narrator, who tells the story; he is revealed to be the Cat using a voice-changer at the end.

==Production== Comedy film How the another Dr. Seuss book of the same name, commercial and critical success. Brian Grazer, who was the producer of How the Grinch Stole Christmas, stated, "Because we grew up with these books, and because they have such universal themes and the illustrations ignite such fantasy in your mind as a child — the aggregation of all those feelings — it leaves an indelible, positive memory. And so when I realized I had a chance to convert first The Grinch and then, The Cat in the Hat, into movies, I was willing to do anything to bring them to the screen."  Grazer contacted Bo Welch over the phone with the offer to direct the film, and he accepted. Welch, Bo. (2004). Commentary for The Cat in the Hat  . Universal Pictures. 

When production began, songs written by Randy Newman were dropped because they were deemed inferior. Although Welch and a publicist for Myers denied it, several people said Myers had considerable input into the films directing, telling some of the cast (the film co-stars Alec Baldwin and Kelly Preston) how to perform their scenes.   

Tim Allen was originally planned to play the role of the Cat. The script would be originally based on a story conceived by Allen, who admitted that as a child he was afraid of Seuss "mischievous feline" babysitter. Allen stated, "My dream is to give it the edge that scared me."  However, producers did not commission a screenplay until late February 2001, when Alec Berg, Jeff Schaffer, and Dave Mandel (who were also writers on Seinfeld) were hired to write the script (replacing the original draft of the film that was written a few years before),  so there was no way the film would be ready to shoot before the deadline. Allen was also committed to shooting Disneys The Santa Clause 2, which was also delayed because Allen wanted a script rewrite.  Due to a scheduling conflict with The Santa Clause 2,  he dropped out his role. 

In March 2002, the role of the Cat was given to Mike Myers,  even though he had an argument with Grazer about starring in a cancelled Saturday Night Live skit named Sprockets (Saturday Night Live)#Proposed film adaptation|Dieter.    Myers stated in an interview that he was a long-time fan of the original Dr. Seuss book, and that it was the first book he ever read. 
 Steve Johnson. angora and human hair and was fitted with a cooling system. To keep Myers cool during the outdoor shoots, a portable air conditioner was available that connected a hose to the suit between shots. The tail and ears were battery operated.

The Fish was considered somewhat of a unique character for Rhythm & Hues (responsible for some of the effects and animation in such films as   and Scooby-Doo (film)|Scooby-Doo), in that the character had no shoulders, hips or legs, so all of the physical performance had to emit from the eyes, head and fin motion. Sean Hayes, who provided the voice for the Fish, found the role significantly different from his usual on-camera jobs; he did not know how the final animation would look, and all of his work took place alone in a sound booth. 

Before filming began, giant props for the film were stolen from the set. Local police found the props vandalised in a mall car park in Pomona, California. The props were covered with graffiti. No arrests had been made, and filming was to start the next week.  The film was mainly shot in California from October 2002 until January 2003.
 Simi Valley, Pomona street where a number of antique and gift shops are located. The community decided not to redecorate after filming ended, so the surreal paint scheme and some of the signage could still be seen as it appears in the film. Because of so much smog in the area, the sky had to be digitally replaced with the cartoon-like sky and colours of the background had to be digitally fixed.

==Reception==

===Box office===
The film grossed $101,149,243 in the U.S. and an additional $32,811,298 from foreign countries brought the films total box office revenue to $133,960,541, against a production budget of $109 million. 

===Critical reaction===
Review aggregate website  s film website.  On Metacritic, the film has a score of 19/100 based on 37 reviews, indicating   "overwhelming dislike". 
 The Wizard How the PG rating Movie Guide gave it one and a half stars out of four saying that the "Brightly colored adaptation of the beloved rhyming book for young children is a betrayal of everything Dr. Seuss ever stood for, injecting potty humor and adult (wink-wink) jokes into a mixture of heavy-handed slapstick and silliness." Maltin also claimed that the films official title which included Dr Seuss The Cat in the Hat was "an official insult."  However, Jeffrey Lyons from the NBC-TV, enjoyed the film and considered it "enormously funny".

Actor Alec Baldwin addressed complaints the film received because of its dissimilarity to the source material. He expressed a belief that a movie is "an idea about something" and that because Dr. Seuss work is so unique, making a feature-length film out of one of his stories would entail taking liberties and making broad interpretations. Baldwin, Alec. (2004). Commentary for The Cat in the Hat  . Universal Pictures. 

===Awards and nominations===

{| class="wikitable"
|-  style="background:#b0c4de; text-align:center;"
! Award
! Subject
! Nominee
! Result
|- Broadcast Music BMI Film Awards Best Music David Newman David Newman
| 
|-Tom Bosley DFWFCA Awards Worst Film
|
| 
|- Nickelodeon Kids Choice Awards|Kids Choice Awards Favorite Movie Actor Mike Myers
| 
|-Andy Williams Golden Raspberry Awards Worst Actor of the Decade
| 
|- Golden Raspberry Worst Actor
| 
|- Golden Raspberry Worst Supporting Actor Alec Baldwin
| 
|- Golden Raspberry Worst Supporting Actress Kelly Preston
| 
|- Golden Raspberry Worst Picture
|
| 
|- Golden Raspberry Worst Director Bo Welch
| 
|- Golden Raspberry Worst Screenplay Alec Berg, the book by Dr. Seuss
| 
|- Golden Raspberry Worst Screen Couple Mike Myers and either Thing One or Thing Two
| 
|- Worst Excuse for an Actual Movie (All Concept/No Content)
|
| 
|- Worst "Comedy" of Our First 25 Years
|
| 
|- Stinkers Bad Movie Awards  Worst Picture
|
| 
|- Worst Director Bo Welch
| 
|- Worst Screenplay for a Film Grossing More Than $100 Million Worldwide Alec Berg, David Mandel and Jeff Schaffer, based on the book by Dr. Seuss
| 
|- Worst Actor Mike Myers
| 
|- Worst Fake Accent - Male
| 
|- Worst Supporting Actor Alec Baldwin
| 
|- Most Painfully Unfunny Comedy
|
| 
|- Worst Song
|"Fun, Fun, Fun"; music by Marc Shaiman, lyrics by Scott Wittman and Shaiman
| 
|- Most Annoying Non-Human Character Cat in the Hat
| 
|- Thing One and Thing Two (voices by Dan Castellaneta)
| 
|- The Spencer Breslin Award (Worst Performance by a Child Actor) Spencer Breslin
| 
|- Dakota Fanning
| 
|}

The film also received three nominations at the Hollywood Makeup & Hairstylists Guild Awards. 

==Cancelled sequel and planned CGI remake== The Cat in the Hat Comes Back was in development, a little more than a month before the films release.  However, following the failure of The Cat in the Hat, Dr. Seusss widow Audrey Geisel said she would never allow any further live action adaptations of her husbands works and the sequel was eventually canceled. 
 computer animated The Lorax.       

 

==Soundtrack==
{{Infobox album  
| Name = The Cat in the Hat
| Type = Film
| Longtype = / Soundtrack album David Newman
| Cover = 
| Length = 20:00
| Released = February 20, 2003
| Recorded = 2002
| Genre = Orchestral BMG Soundtracks}} David Newmans score, plus a song by Smash Mouth ("Getting Better") and ("Hang on") that makes it the third film in a row playing a song in a film starring Mike Myers, after Shrek (2001)  and Austin Powers in Goldmember (2002). The soundtrack also includes a couple of songs performed by Mike Myers (the role of the Cat).  Newmans score won a BMI Film Music Award.
{{tracklist
| collapsed       = Yes
| headline        =
| total_length    = 48:55 
| writing_credits = yes
| extra_column    = Producer(s)
| title1          = Main Title; The Kids	 David Newman
| length1         = 8:07 
| title2          = Getting Better
| note2           = Performed by Smash Mouth
| length2         = 2:24 	
| title3          = The Cat
| note3           = Composed by David Newman
| length3         = 3:50 	
| title4          = Two Things - Couch Jumping - Lea... 	
| note4           = Composed by David Newman
| length4         = 5:16 	
| title5          = Military Academy Seduction
| note5           = Composed by David Newman
| length5         = 3:02 	
| tittle6         = Mrs. Kwan - Mom Leaves
| note6           = Composed by David Newman
| length6         = 2:12 	
| title7          = Surfer Cat - the Phunometer
| note7           = Composed by David Newman
| length7         = 2:23 	
| title8          = Fun, Fun, Fun
| note8           = Performed by Mike Myers
| length8         = 2:38
| title9          = The Contract
| note9           = Composed by David Newman
| length9         = 1:53 	
| title10         = Oven Explodes - "Clean Up This Mess!"
| note10          = Composed by David Newman
| length10        = 1:36
| title11         = Things Wreck the House
| note11          = Composed by David Newman
| length11        = 2:52 	
| title12         = Larry the Slob
| note12          = Composed by David Newman
| length12        = 3:10
| title13         = Birthday Party
| note13          = Composed by David Newman
| length13        = 2:11 	

| title14         = S.L.O.W. Drive
| note14          = Composed by David Newman
| length14        = 2:32
| title15         = Rescuing Nevins
| note15          = Composed by David Newman
| length15        = 4:27 	
| title16         = Hang On
| note16          = Performed by Smash Mouth
| length16        = 2:53
}}

==Home media==
The Cat in the Hat was released for VHS and DVD on March 16, 2004.  It features 16 deleted scenes, 20 outtake scenes, almost a dozen featurettes and a “Dance with the Cat” tutorial to teach kids a Cat in the Hat dance.  On February 7, 2012, the film was released on Blu-ray. 

== Video game ==
{{Infobox video game
| title         = Dr. Seuss The Cat in the Hat
| italic title  = 
| collapsible   = 
| state         = 
| show image    = 
| image         = 
| caption       = 
| developer     = Magenta Software
| publisher     = Vivendi Universal Games
| series        = 
| engine        = 
| version       =  Xbox Game Boy Advance
| released      =  
| genre         = 
| modes         = 
| media         = 
| director      = 
| producer      = 
| designer      = 
| programmer    = 
| artist        = 
| writer        = 
| composer      = 
| cabinet       = 
| arcade system = 
| cpu           = 
| sound         = 
| display       = 
| latest release version = 
| latest release date    = 
| latest preview version = 
| latest preview date    = 
}}
{{Video game reviews
| width = 26m
| GR = (Xbox) 52.67%  (PS2) 50%  (GBA) 46.50%  (PC) 19.50% 
| MC = (Xbox) 56/100    (PS2) 56/100    (GBA) 40/100    (PC) 40/100   
| Fam = 27/40 
| GSpot = 3.8/10 
| GameZone = (PC) 8/10  (PS2) 7.2/10  (GBA) 5/10 
| IGN = 6/10    (GBA) 4/10 
| NP = 2.3/5 
| OPM =   
| OXM = 5.5/10 
| PCGUK = 9% 
| PCZone = 30% 
| XPlay =   
}} PC on November 9, 2003, shortly before the films theatrical release. A version for the Nintendo GameCube was canceled. The plot of the game is different from the movie; instead of Conrad unlocking the Cats Crate, Larry Quinn unlocks it and steals the Lock to it. Playing as the Cat, the player must go through thirteen levels through the transformed house and chase down Larry, who is collecting the magic released from the Crate for himself, and defeat him to get the Lock (called the "Crablock" in-game) back and re-lock the Crate before the childrens mother returns home.  The game received critically mixed reviews             (except for the PC version, which received negative reviews).     The video game was banned in Brazil due to copyright issues. 
 

==See also==
 
* List of films based on Dr. Seuss books
 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 