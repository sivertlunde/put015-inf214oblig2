Richard Cardinal: Cry from a Diary of a Métis Child
Richard Cardinal: Cry from a Diary of a Métis Child is a 1986 National Film Board of Canada documentary film by Alanis Obomsawin, about the suicide of Métis youth Richard Stanley Cardinal, who killed himself in 1984 at the age of 17.

Cardinal, who had been placed in 28 different homes during his 14 years in Albertas child welfare system, hanged himself from a cross bar he had nailed between two trees near his last foster home, northwest of Edmonton.   

The film makes use of Cardinals remembrances from his diary, and helped to change public perceptions in Canada around the treatment of children in the foster care system.   

Cardinal was born in Fort Chipewyan. Because of alcoholism in his family, the children were separated from their parents. The Royal Canadian Mounted Police moved the children to Fort McMurray where they were given different foster parents, with Richard and his older brother Charlie housed together for the first while—where a litany of physical and mental abuse began.   

Obomsawin offers Cardinals life story as an expression of the plight of "15,000 native children   are wards of the provinces." Cardinal recollections of abuse at his first foster home placement at age six, when he and his older brother were whipped with their pants down in front of the three daughters of his foster family. It quotes Cardinals brother as stating, "What Richard needed most was to go home. His funeral was the best social service that was provided for Richard, because it finally brought his family together." 

==Inquiries==
His death sparked a provincial government inquiry, which offered 22 recommendations for improving how courts, the provincial government, schools, hospitals, aboriginal organizations and media deal with foster children—including recommendations that have been repeated at subsequent inquiries following Aboriginal youth deaths. However, a January 2014 article in the Calgary Herald reported that such recommendations, which are neither binding on the government nor tracked for implementation, have largely been ignored. 

==Retrospectives==
In recent years, Richard Cardinal: Cry from a Diary of a Métis Child has been featured in retrospectives at the Museum of Modern Art (2008),  Hot Docs Canadian International Documentary Festival (2009),  and the Berlin International Film Festival (2013). 

==See also==
*Foster Child
*Sixties Scoop
*Kimelman Report

==References==
 

==External links==
* 
*Watch   at the National Film Board of Canada

 
 
 
 
 
 
 
 
 
 
 
 