Saint-Ex
 
:For the live stage musical production, see  .

Saint-Ex is a 1997 British film biography of French author-adventurer Antoine de Saint-Exupéry, filmed and distributed in the United Kingdom, and featuring Bruno Ganz, Eleanor Bron, and Miranda Richardson. The script was by Frank Cottrell Boyce, while the writers sons, Aidan and Joseph, portrayed the Saint-Exupery brothers, Francois and Antoine, as children. The film was director Anand Tuckers feature film debut, {{cite book
 | last = Allon
 | first =Yoram
 | authorlink =
 |author2=Del Cullen |author3=Hannah Patterson
  | title =Contemporary British and Irish Film Directors: A Wallflower Critical Guide
 | publisher =Wallflower
 | series =Wallflower Critical Guides
 | volume =
 | edition =
 | year =2001
 | location =London
 | pages =334
 | language =
 | url =
 | doi =
 | id =
 | isbn =1903364221
 | mr =
 | zbl =
 | jfm = }}  and combines elements of biography, documentary, and dramatic re-creation. It was not released theatrically. 

== References ==
 

==External links==
*  

 
 

 
 
 
 
 


 