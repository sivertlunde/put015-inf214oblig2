Fovou tous Ellines
 
{{Infobox film|
  name    = Fovou tous Ellines  (Beware of Greeks Bearing Guns)|
  image   = Beware Of Greeks Bearing Guns.JPG|
  caption = Australian movie poster|
  writer  = Tom Galbraith, Lakis Lazopoulos |
  starring = Lakis Lazopoulos, Zoe Carides,   Claudia Buttazzoni,   John Bluthal|
  director = John Tatoulis|
  producer =  |
  editing  =  |
  distributor = Palace Films, Odeon|
  released =  2000 ( Greece )  12 September 2002 ( Australia ) | min |
country = Australia Greece|
  language = English Greek |
  budget   = |
gross = A$92,142 (Australia) |
}}
Fovou tous Ellines (also known as Beware of Greeks Bearing Guns) is a film directed by John Tatoulis. Filmed in Australia and the island of Crete, Greece it is a romantic comedy / drama, that stars Lakis Lazopoulos, alongside Zoe Carides, Claudia Buttazzoni and John Bluthal.

==Summary==
Maria, a woman who is obsessed with tracking down the man who she claims killed her husband in Greece, wants her eldest grandson to enact revenge, as tradition demands.

Manos, a mild-mannered schoolteacher is sent to Australia to settle the long-standing vendetta but he is more interested in reuniting with his long lost love Niki than shooting anyone. Meanwhile, his layabout twin brother George decides to avenge the family name himself. When both brothers arrive in Melbourne, Victoria|Melbourne, they discover that the family secrets arent as secret as they thought. 

==Cast==
*Lakis Lazopoulos as Manos and George
*Zoe Carides as Niki
*John Bluthal as Stephanos
*Claudia Buttazzoni as Katerina
*Damien Fotiou as Jim
*Tasso Kavadia as Maria
*Osvaldo Maione as  Enzo
*Percy Sieff as Petros
*Ron Haddrick as Thomas
*Anastasia Malinof as Helen
*Noni Ioannidou as young Maria (1943–1971)
*Dimitris Kaperonis as young Manos (1971)
* Alexandros Kaperonis as young George (1971)
* Artemis Ionnides as young Nicki (1971)
* Nikos Psarras as young Vasilli (1943)
* Dimitris Kalantzis as young Stephanos (1943)

==Release==
The film was highly popular in Greece. 

==References==
 

== External links ==
*  
*  at Urban Cinefile

 
 
 
 
 
 
 