Welcome to Shama Town
 

{{Infobox film name       = Welcome to Shama Town image      = Welcome to Shama Town poster.jpg caption    =  traditional = 決戰剎馬鎮 simplified = 决战刹马镇 pinyin     = Juézhàn Shàmǎ Zhèn }} director   = Li Weiran producer   = Zhuo Shunguo Lu Yang
| writer    = Li Weiran Zhou Zhiyong
| based on  =  starring   = Sun Honglei Lin Chi-ling Li Liqun Gan Wei music      = Dong Dongdong cinematography = Zhao Xiaoding editing    =  studio     = Shanghai Media Group LeTV Investment Co. Ltd
|distributor= China Film Group Corporation Huaxia Film Distribution Company Dadi Time Film Distribution Co., LTD Shanghai Star Cultural Transmission Co., LTD.  released =   runtime = 104 minutes country = China language = Mandarin budget   =  gross    = ￥25 million 
}}

Welcome to Shama Town is a 2010 Chinese adventure film directed and written by Li Weiran,  starring Sun Honglei, Lin Chi-ling, Li Liqun, and Gan Wei.     It was distributed by Shanghai Media Group and LeTV Investment Co. Ltd and released by China Film Group Corporation, Huaxia Film Distribution Company, Dadi Time Film Distribution Co., LTD, and Shanghai Star Cultural Transmission Co., LTD. It was released in China on 22 June 2010.  

==Cast==
* Sun Honglei as Tang Gaopeng, the Village Chief of Shama Town.
* Lin Chi-ling as Chun Niang, Tang Gaopengs lover.
* Li Liqun as Zhou Dingbang, the tomb raider.
* Gan Wei as Tao Hua
 Ma Jian as Gui Zhong.
* Xie Yuan as Mian Bin.
* Ma Delin as Da Pao.
* Huang Haibo as Da Cui, the cultural relics dealer.
* Cao Bingkun as Chen Dili.
* Bao Beier as Lu Maoku.
* Zhao Ziqi as the newspaper woman.
* Cica Zhou as the female bodyguard.
* Ma Li as Mian Bins wife.
* Michael Stephen Kai as Xiao P.

==Production==
Sun Honglei filmed several scenes in Yongtai Ancient City, Yellow River Stone Forest, and Dunhuang Studio in Gansu, China.

==Release==
It was released in Mainland China on 22 June 2010.

The film was shown at the 47th Golden Horse Awards. 

==Box office==
It grossed ￥25 million on its opening weekend, it was a huge hit at the box office and flew straight to the top of the box office.    It grossed ￥40 million on its second weekend.  

==References==
 

==External links==
*  
*   
*  

 
 
 
 