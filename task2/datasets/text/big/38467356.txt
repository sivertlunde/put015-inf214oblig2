The Last: Naruto the Movie
 
{{Infobox film
| name           = The Last: Naruto the Movie
| image          = TheLastNarutomovie.jpg
| caption        = Theatrical release poster. Left caption reads,  
| director       = Tsuneo Kobayashi
| writer         = Kyozuka Maruo
| based on       =  
| starring             = {{plainlist|
* Junko Takeuchi
* Nana Mizuki
* Jun Fukuyama
* Chie Nakamura
* Satoshi Hino
* Showtaro Morikubo
* Kazuhiko Inoue
* Noriaki Sugiyama
* Akira Ishida
}}
| music          = Yasuharu Takanashi Studio Pierrot
| distributor    = Toho
| released       =    
| runtime        = 112 mins 
| country        = Japan
| language       = Japanese  
| gross          = Japanese yen|¥1.94 billion (Japan)
(US$16.5 million)
}}
The Last: Naruto the Movie is the tenth overall Naruto film and the seventh Naruto Shippūden film, made to commemorate the 15th anniversary of the franchise. This is also the first entry in the  , and the first film to be an official part of the canon Naruto storyline and is set between the last two chapters of the original manga series. The new movie is  also featuring the next generation of characters, that have yet to be trained.

It premiered in theaters on December 6, 2014.     The Last became the highest-grossing feature film in the franchise, earning Japanese yen|¥1.94 billion (US$16.5 million). 

==Plot== Kaguya Ōtsutsukis body begins to descend towards the Earth. With the moon now a meteor that would destroy everything on impact, Naruto must deal with this new threat. The crisis is caused by Toneri Ōtsutsuki, a descendant of Hamura Ōtsutsuki ill-determined to carry on his ancestors legacy. During the Konohagakure Winter Festival, Hinata hopes to give Naruto a personal gift of love — a red scarf — she knitted herself in remembrance of one Naruto used to wear back in the Academy, and Sakura offers to help. Toneri then infiltrates Konoha and kidnaps Hanabi Hyuga after failing to seize Hinata, and the scarf she made got damaged. Deployed for a mission, Naruto, Hinata, Sakura, Sai, and Shikamaru go to rescue Hanabi. Shikamaru is also given a special clock held only by the five Kage, which apparently is counting down to doomsday. On the journey, Naruto gradually began to realize the concept of love having to experience Hinatas memories. At some point, Toneri steals Hanabis Byakugan and implants them in his own empty eye sockets, awakening the Tenseigan sealed by Hamuras descendants over the last millennium. He also succeeds in capturing Hinata, proposing her to marry him; she "accepts" his offer, which causes Naruto to fall into a deep depression after being thoroughly beaten by Toneri. At the same time, in Narutos absence, Sasuke returns to Konoha to protect the village from meteorites breaking off from the moon.

After a three-day recovery process, Naruto wakes up and sees Sakura severely weakened. Naruto finally realizes that Hinata truly loves him. With new-found strength, Naruto leads the charge into Toneris moon base. Meanwhile, in Toneris palace, Toneri asks Hinata craft a red scarf for him. But in reality, Hinata only accepted Toneris proposal as she was contained by the spirit of Hamura to aid him as the "Byakugan Princess" to destroy the Tenseigan altar. Unfortunately, when Hinata attempts to destroy the altar, Toneri realized the deception and places her in a giant bird cage while destroying the scarf she had originally made for Naruto. Narutos team finally catches up and a massive attack on Toneris palace begins. The team splits, with Naruto rescuing Hinata from the ceremony, while the others save Hanabi. However, Toneris Tenseigan grants him enough power to slice the moon in half. Naruto enters Nine-Tails Mode, and a huge duel ensues. Near the end, Naruto grasps the last remaining shred of the scarf and channels his chakra shroud into the fist and delivers a strong punch enough to depower Toneri and pin him against a wall. Hinata uses this chance to retrieve Hanabis Byakugan. Eventually, Naruto defeats Toneri, stops the moon from falling. Toneri, realizing the error of his ways, remains on the moon as it returns to the orbit. Naruto then confesses his love for Hinata and the two kiss.

During the end credits, scenes show Naruto and Hinatas wedding, attended by their friends and family. In a post-credits scene, set during the time of the series epilogue, the couple play with their children, Boruto and Himawari.

==Voice cast==
{| class="wikitable"
|-
! Character
! Japanese voice
! English voice 
|-
| Naruto Uzumaki
| Junko Takeuchi
| Maile Flanagan
|-
| Hinata Hyuga
| Nana Mizuki
| Stephanie Sheh
|- Toneri Ōtsutsuki
| Jun Fukuyama
| TBA 
|-
| Sakura Haruno
| Chie Nakamura
| Kate Higgins
|- Sai
| Satoshi Hino
| Ben Diskin
|-
| Shikamaru Nara
| Showtaro Morikubo
| Tom Gibis 

|-
| Kakashi Hatake
| Kazuhiko Inoue
| Dave Wittenberg
|-
| Sasuke Uchiha
| Noriaki Sugiyama
| Yuri Lowenthal
|-
| Gaara
| Akira Ishida
| Liam OBrien 
|- Tsunade
| Masako Katsuki
| Debi Mae West 
|-
| Rock Lee
| Youichi Masukawa
| Brian Donovan 
|- Choji Akimichi
| Kentarō Itō
| Robbie Rist 
|- Ino Yamanaka
| Ryōka Yuzuki
| Colleen OShaughnessey
|-
| Hanabi Hyuga
| Kiyomi Asai
| Stephanie Sheh
|-
| Fourth Raikage, A
| Hideaki Tezuka
| Beau Billingslea 
|- Killer Bee
| Hisao Egawa
| Catero Colbert 
|-
| Onoki
| Tomomichi Nishimura
| Steven Blum 
|-
| Mei Terumi
| Yurika Hino
| Mary Elizabeth McGlynn 
|- Iruka Umino
| Toshihiko Seki
| Quinton Flynn 
|- Konohamaru Sarutobi
| Ikue Ōtani
| Colleen OShaughnessey
|-
| Hiashi Hyuga
| Eizō Tsuda
| John DeMita
|- Kurama
| Tesshō Genda
| Paul St. Peter 
|- Boruto Uzumaki
| Kokoro Kikuchi
| TBA 
|- Himawari Uzumaki
| Yūki Kuwahara
| TBA 
|}

==Production==
The film is directed by Tsuneo Kobayashi  while manga author Masashi Kishimoto is providing the story concept, character designs, and exercising complete editorial supervision.    The film was first announced at Jump Festa 2012.       It was premiered on December 6, 2014, which made it the first movie in the franchise to be released two years after the previous film.  Its first teaser was revealed on July 31, 2014. 
 data book was distributed with the movie, and was to contain a one-shot movie tie-in chapter by Kishimoto. Maruo Kyōzuka wrote a novelization published by Shueisha on December 8, 2014. 

The male duo Sukima Switch is performing the films theme, "Hoshi no Utsuwa" ("Star Vessel"); producer Takuyuki Hirobe had asked the duo to make a song that invokes a gentle yet powerful outlook of the world in the making of the movie.  The single is set to be released on December 3, 2014.   A character CD song for Hinata Hyuga has also been announced as "Fuyu no Owari ni" ("At the End of Winter") by Nana Mizuki. 

==Box office==
During its first weekend, The Last earned ¥515 million (US$4.35 million).  The film has grossed Japanese yen|¥1.29 billion after three weekends.  And by the end of December 2014, it had earned about  Japanese yen|¥1.75 billion (US$14.76 million) and became the top-grossing feature film in the franchise.   

==Sequel==
 
After the post-credits scene of The Last, it has shown a teaser trailer for a movie based on Naruto and Hinatas son Boruto set for an August 2015 release.  Masashi Kishimoto is confirmed to be involved as a supervising executive producer, co-writer and character designer, conceiving the story about the next generation. The sequel is tied into the New Era Project.

==References==
;Japanese text
 
;Specific 
 

==External links==
*  
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 