Stupid Teenagers Must Die!
{{Infobox film
| name           = Stupid Teenagers Must Die!
| image          = Stupid_Teenagers_Must_Die_Final_Poster.jpg
| caption        = Stupid Teenagers Must Die! movie poster
| director       = Jeff C. Smith
| producer       = Curtis Andersen Sara Parrell Wayne Watson
| writer         = Curtis Andersen Jeff C. Smith
| starring       = Jovan Meredith Ashley Schneider Devin Marble Lindsay Gareth Renee Dorian Cory Assink Jonathan Brett Will Deutsch Jamie Carson Christina DeRosa
| music          = Randy Catiller Chris Dingman John Draisey
| cinematography = Jeff C. Smith
| editing        = Jeff C. Smith
| studio         = Wiggy VonSchtick, Off Set Pictures
| distributor    = Vanguard Cinema, Singa Home Entertainment
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Stupid Teenagers Must Die! (early title Blood & Guts) is a 2006 spoof film directed by Jeff C. Smith and written by Smith and Curtis Andersen.       During production and initial festival screenings, the film was titled Blood & Guts, but before sending it to distributors, the change to the current title was made to better reflect the humor intended by the filmmakers, as they thought the original title implied more carnage than the film supplied.     

==Background==

In an early interview with Unbound, director Jeff C. Smith revealed that he and co-producers Sara Parrell and Curtis Andersen, as well as actor Jovan Meredith all at one time worked together in the Entertainment division at Disneyland in Anaheim, California. Smith shared that it was during their employment at the park, and when the group had collaborated on an earlier project, he learned that Jovan had skills he wished to include in a future project... the one which became Stupid Teenagers Must Die.     Even before the film had entered post-production, the crew had optimistically scheduled its premiere as Blood & Guts at Cinespace in Hollywood, California for July 13, 2006. However, the film was not ready and director Smith was compelled to screen the incomplete rough-cut he did have, as a "test screening". The audience panned the project. Taking the lesson of premature release to heart, Smith spent the next several months finishing the films editing in preparation for its subsequent screenings. 

==Plot==
 Night of the Demons, the Friday the 13th and Halloween films and Sleepaway Camp. 

A group of teens has decided to meet in a haunted house to hold a seance.  The characters are stereotype spoofs of other 80s film characters: the cool hero guy (Jovan Meredith), the naive girlfriend (Ashley Schneider / Aurora Sta. Maria), the goth girl (Renee Dorian), the tough guy (Devin Marble), the ditzy blonde girl (Lindsay Gareth), the shy geek in love with the blonde (Matt Blashaw), two big nerds (Cory Assink & Jonathan Brett), and a pair of lipstick lesbians (Jamie Carson & Christina DeRosa).  Expected before it even begins, and just as in 80s horror style, strange things begin to occur... outrageously corny deaths, gratuitous nudity, obvious pitfalls and traps, inane dialogue, and the teens themselves being one-by-one stabbed and sliced... with the cool hero working to save the day before every stupid teenager is dead.

==Partial cast==

{| class="wikitable"
|- bgcolor="#efefef"
! Actor !! Role
|-
| Jovan Meredith || Kane
|-
| Ashley Schneider || Julie
|-
| Devin Marble || Alfie
|-
| Lindsay Gareth || Tiffany
|-
| Renee Dorian || Madeline
|-
| Cory Assink || Geek One
|-
| Jonathan Brett || Geek Two
|-
| Will Deutsch || Ryan
|-
| Jamie Carson || Sissy
|-
| Christina DeRosa || Jamie
|-
| Matt Blashaw || Michael
|-
| Aurora Sta. Maria || Soup
|}

Full cast listing available at Piczo. 

==Reception==
Intended by the director to appeal to "movie geeks",  the film has received mixed response from genre reviewers. Dorkgasm Senior Staff Writer Kenneth Holm felt that with it being a low-budget film, he began his viewing with lowered expectations but opined that he should have lowered them even more. After panning it in his review, he concluded that he would recommend it only as an "exercise for other budding filmmakers to see what missteps to avoid when making their first movie."  Conversely, the reviewer for Dead Lantern found the film to be worth watching and great fun, remarking that he enjoyed the "back story" of why the house was haunted in the first place and feels that it would be a great prequel, and concludes by recommending it as a "comedy that will make even the most jaded horror elitists smile,"  and Fatally Yours called it a "film that knows how to have a good time!" in a review that acclaimed the film as "one of the most fun and enjoyable low-budget films I’ve seen in quite some time";  however, a reviewer for The Movies Made Me Do It, decided that even with touting itself as a low-budget spoof, the filmmakers lost track of the fact that they were making a spoof, resulting in them trying too hard at the wrong goals and taking the film too seriously, when it could have remained funny throughout, leading to them "basically creating a slasher film from the eighties minus the things that made those movies so appealing in the first place: massive body counts, T&A, and a neat villain for the heroes to contend with", concluding that "Its never even remotely scary", and "its simply not funny either save for a couple of one-liners."  The film had a major write-up in the October 2007 issue of Fangoria Magazine. 

==Release==
 US by Vanguard International Cinema   and internationally by Singa Home Entertainment on September 25, 2007.  Three weeks prior to the DVD release, director Smith shared that the distributor allowed them a maximum of 125 minutes on the DVD, so he had chosen to include a 30 minute "making of" documentary titled Movies Are Bullshit! The Making Of Stupid Teenagers Must Die!, with commentary by director Jeff C. Smith and actor Jovan Meredit, an audience reaction track from the premiere, and an interview with cast member Will Deutsch. 

== References ==
 
 

== External links ==
*  
*  

 
 
 
 
 