¡Qué hacer!
 
{{Infobox film
| name           = ¡Qué hacer!
| image          = 
| caption        = 
| director       = Raúl Ruiz (director)|Raúl Ruiz Saul Landau Nina Serrano
| producer       = James Becket
| writer         = Cristián Sánchez
| starring       = Sandra Archer
| music          = Country Joe McDonald
| cinematography = Richard Pearce
| editing        = Bill Yahraus
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Chile
| language       = Spanish
| budget         = 
}}

¡Qué hacer! is a 1970 Chilean drama film directed by Raúl Ruiz (director)|Raúl Ruiz,    Nina Serrano and Saul Landau.   

==Cast==
* Sandra Archer as Suzanne McCloud
* Aníbal Reyna as Simon Vallejo
* Richard Stahl as Martin Scott Bradford
* Luis Alarcón as Osvaldo Alarcón
* Pablo de la Barra as Hugo Alarcón
* Sergio Zorrilla as Himself
* Salvador Allende as Himself
* Country Joe McDonald as Country (as Joe McDonald)
* Jorge Yáñez as Padre Eduardo
* Sergio Bravo as Kidnapper
* Óscar Castro as Kidnapper
* Poli Délano as Old comrade
* Mónica Echeverría as Irene Alarcón
* Elizabeth Farnsworth as Margaret
* Saul Landau as Seymour Rosenberg
* Rodrigo Maturana as Old comrade
* Pablo Neruda as Himself
* Darío Pulgar as Darío Mesa, the drunk
* Donald Ramstead as Peace corps director

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 