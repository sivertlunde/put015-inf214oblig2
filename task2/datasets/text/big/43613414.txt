Kidnapping Freddy Heineken
{{Infobox film
| name           = Kidnapping Freddy Heineken
| image          = Kidnapping Freddy Heineken.jpg
| caption        =
| director       = Daniel Alfredson
| producer       = Judy Cairo Howard Meltzer Michael A. Simpson
| screenplay     = William Brookfield 
| source book    = Peter R. de Vries
| based on       =  
| starring       = Anthony Hopkins Sam Worthington Jim Sturgess Ryan Kwanten
| music          = Clay Duncan Lucas Vidal
| cinematography = Fredrik Bäckar
| editing        = Håkan Karlsson
| studio         = European Film Company Informant Europe SPRL Umedia Alchemy A Plus Films Signature Entertainment
| released       =    
| runtime        = 95 minutes
| country        = United Kingdom Netherlands
| language       = English
| budget         =
}} crime drama Frans Meijer.          

==Cast==
* Anthony Hopkins as Freddy Heineken
* Sam Worthington as Willem Holleeder
* Jim Sturgess as Cor van Hout
* Ryan Kwanten as Jan Boellard
* Jemima West as Sonja Holleeder
* Thomas Cocquerel as Martin Erkamps Frans Meijer

==Production==
Filming began in Belgium in October 2013. 

==Reception==
Kidnapping Mr. Heineken received generally unfavorable reviews from critics. The film holds a 33/100 score at  : "About as appealing as day-old beer littered with cigarette butts, the abysmal caper drama Kidnapping Mr. Heineken is one of those international co-productions produced for all the right tax-credit reasons and none of the right artistic ones."      of The New York Observer gave the film 3 out of 4 stars, and commented: "Anthony Hopkins plays the king of the hops, and he is excellent. So is the rest of the movie, a sober, no-frills account about the highest ransom ever collected up to that time—$10 million and counting."   

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 