Broken Vessels
{{Infobox film
| name           = Broken Vessels
| image          =
| caption        = 
| director       = Scott Ziehl
| producer       = Scott Ziehl Roxana Zal
| writer         = Scott Ziehl David Baer John McMahon   
| starring       = Todd Field Jason London Roxana Zal Susan Traylor James Hong
| music          = Martin Blasick Todd Field Brent David Fraser Bill Laswell 
| cinematography = Antonio Calvache   
| editing        = Chris Figler David Moritz 
| distributor    = Unapix Entertainment Productions
| released       = April 18, 1998
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $600,000
| gross          = $13,493
}}
 Los Angeles Independent Film Festival and marked Ziehls directorial debut.  It stars Todd Field, Jason London, Roxana Zal, Susan Traylor, and James Hong.  The film follows a rookie paramedic and his hardened drug-addicted partner as they take calls and cruise L.A. in their ambulance.  Although it shares the same name as the book, it has nothing to do with the Andre Dubus essay collection of the same name.

==Plot==
 traumatic effects of the job by heavy use of drugs and avoiding commitments. Before long Tom finds himself pulled into the same world and has to come to a decision about what direction he wants to take in his life.

==Cast==
* Todd Field as Jimmy Warzniak
* Jason London as Tom Meyer
* Roxana Zal as Elizabeth Capalino
* Susan Traylor as Susy 
* James Hong as Mr. Chen
* Patrick Cranshaw as Gramps
* Brent David Fraser as Jed
* Stephanie Feury as Jill

==Box office==
Made on a non-union shoestring budget of $600,000, it was nominated for several awards when it was shown at film festivals in 1998.,  Though it failed to find a legitimate theatrical distributor, eventually, the film was self-released in just two theaters over the holiday weekend of July 4, 1999 and brought in $3,722. 

{{Quote box
| quote  =" Movies dont get much more corrosive or gripping than Scott Ziehls high-energy first feature, "Broken Vessels." The actors, including the ever-reliable William Smith in a cameo, are all on the money in their portrayals in exceptionally well-drawn roles, crackling with pungent dialogue. In major, demanding roles London and Field are especially impressive.  "Broken Vessels" could take Ziehl far. It has that kind of kinetic energy that fuses style and theme, as Tom and Jimmy careen through L.A. streets both in answer to emergency calls and in pursuit of a fix."    Kevin Thomas, Los Angeles Times.
 | width  = 25%
 | align  = left
}}

{{Quote box
| quote  =" A vivid, embracing tale of life on the edge, "Broken Vessels" is an assured first feature with potent commercial appeal. Focused on a pair of paramedics behind the wheel of an ambulance, the film skillfully careens through the incidental and dark humor of their lives and plows forward into the bleak personal terrain that comes with the job. One of the few genuine artistic hits of the L.A. Indie Fest (the film received the fests best picture prize), "Vessels" has sufficient high- octane quality to overcome the noisy, overcrowded specialized scene and carve out a respectable theatrical niche. At the center of "Broken Vessels" are two exceptionally compelling performances by Field and London. Despite the outward flashiness of Jimmy, Field does nothing to pump up his role; its wonderfully nuanced work in which the most modest changes in shading wind up resonating as his dance on the edge of sanity and the law becomes complex and precarious. London works his boyish persona for all its worth and his slide from cute to sinister occurs with brilliant ease."   
 | source = Leonard Klady, Variety (magazine)|Variety.
 | width  = 30%
 | align  = right
}}

==Critical reception==
The film received no small amount of notice from major critics including Roger Ebert of the Chicago Sun-Times who gave it three stars out of four, saying "What makes the movie special is the way both lead actors find the right quiet notes for their performances." 

==Awards and nominations==
At the British Independent Film Awards, the film was nominated for Best Foreign Independent Film - English Language, at the Gijón International Film Festival director Scott Ziehl was nominated for the Grand Prix Asturias award in the category of Best Feature.  Ziehl and co-producer Roxana Zal won the Audience Award in the category of Best Feature Film at the Los Angeles Independent Film Festival in 1998. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 