The Big Broadcast of 1936
{{Infobox film
| name           = The Big Broadcast of 1936
| image          = Broadcast36.jpg 
| alt            = 
| caption        =
| director       = Norman Taurog
| producer       = Benjamin Glazer Francis Martin Ralph Spence Jack Mintz The Nicholas Brothers Lyda Roberti Wendy Barrie Mary Boland Charles Ruggles Akim Tamiroff
| music          = John Leipold
| cinematography = Leo Tover
| editing        = Ellsworth Hoagland
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
The Big Broadcast of 1936 is a 1935 American comedy film directed by Norman Taurog, and is the second in the series of Big Broadcast movies. 
 The Nicholas Bill "Bojangles" Ray Noble Orchestra. In Glenn Miller and His Orchestra (1974), George Thomas Simon noted that Glenn Miller was paid extra by Ray Noble "for working on The Big Broadcast of 1936, so that Glenns total weekly pay" was $356.  Uncredited roles include Jack Mulhall. 

The screenplay was by Walter DeLeon, Francis Martin, Ralph Spence, and Julius J. Epstein, who was uncredited. 
 Best Dance Direction by LeRoy Prinz for "Its the Animal in Me". 

The movie has not been released on DVD or VHS but is available on various websites.

==Plot==
Radio station W.H.Y. owner Spud Miller (Jack Oakie), also functions as the stations only announcer while his comic partner Smiley Goodwin (Henry Wadsworth) serves as the house singer, Lochinvar, The Great Lover, "the idol of millions of women." Both Spud and Smiley play the role of Lochinvar. Facing the prospect of bankruptcy, Spud welcomes the suggestions of George Burns and Gracie Allen, who attempt to sell an invention, The Radio Eye, invented by Gracie Allens uncle, a television device which can pick up and transmit any signal, any time, anywhere. Burns and Allen ask Miller for an advance of $5,000 for the invention. Spud decides to enter an international broadcast competition with a prize of $250,000.

Ysobel listens to the Lochinvar radio show and believes that he has sent her a letter. She finds out that he sends letters to listeners of the show. Outraged, she goes to the radio station to shoot Lochinvar. Spud and Smiley are able to win her over after her gun fails to shoot. They attempt to convince her to invest $5,000 in The Radio Eye invention which would allow them to win the competition. She takes Spud and Smiley to her Caribbean island, Clementi. She will decide to marry one of them before midnight. Gordoni (C. Henry Gordon), however, plans to murder them. Spud and Smiley are able to notify George Burns and Gracie Allen in New York and inform them that they are in grave danger. Burns and Allen then depart for the island on a boat. Gracie sets a fire on the boat. A Coast Guard cutter takes them on board and heads for the island. Gordoni has Drowzo put in the drinks to put Ysobel to sleep. Spud and Smiley turn on The Radio Eye to listen to the Vienna Boys Choir and the Ray Noble Orchestra from New York to distract Gordoni and his men. Spud and Smiley are able to escape on coaches with teams of horses. After a chase, during which Spud is separated from his horses in a bifurcation in the road, they reach the pier where the Coast Guard and Burns and Allen meet them. Gordoni jumps into the sea. Spud wins the international broadcast competition. Spud tells Ysobel that he may marry her after a period of observation. She tells him: "Let this be the start of a beautiful friendship."

==Production notes==
Paramounts Big Broadcast series began in 1932 and continued in late 1935 with The Big Broadcast of 1936 (1935), which was released on September 20, 1935. The movie was filmed at the Kaufman Astoria Studios on 3412 36th Street, Astoria, Queens, in New York City.

The movie is a precursor for the later Bob Hope and Bing Crosby Road to …|"Road" pictures, with Spud and Smiley finding themselves on an island ruled by Countess Ysobel de Nargila (Lyda Roberti).
 

Daily Variety reported that the scenes featuring Ray Noble and his Orchestra, the Amos n Andy sketches, and the scenes featuring Fox and Walters were filmed in New York. 

==Soundtrack==
* "Double Trouble"
::Music by Ralph Rainger, Richard A. Whiting
::Lyrics by Leo Robin
::Performed by Lydia Roberti, Jack Oakie, Henry Wadsworth and chorus
* "I Wished on the Moon"
::Music by Ralph Rainger
::Lyrics by Dorothy Parker
::Sung by Bing Crosby
::Violin accompaniment: George Stoll (uncredited)
* "Its the Animal in Me"
::Music by Harry Revel
::Lyrics by Mack Gordon
::Performed by Ethel Merman
* "Why Dream"
::Music by Ralph Rainger and Richard A. Whiting
::Lyrics by Leo Robin Kenny Baker)
* "Miss Brown To You"
::Music by Ralph Rainger and Richard A. Whiting
::Lyrics by Leo Robin
::Danced by Bill Robinson and The Nicholas Brothers
* "Amargura"
::Music by Carlos Gardel
::Lyrics by Alfredo Le Pera
::Performed by Carlos Gardel
* "Cheating Muchachita"
::Music by Carlos Gardel
::Lyrics by Alfredo Le Pera
::Performed by Carlos Gardel
* "Why Stars Come Out at Night"
::Words and Music by Ray Noble
::Performed by Ray Noble and His Orchestra, featuring Glenn Miller
* "On the Wings of a Waltz"
::Music by Mel Shauer
::Lyrics by Leo Robin
* "A Man, a Maid, a Moon"
::Music by Ralph Rainger
::Lyrics by Leo Robin
* "Is Love a Moon-Flower?"
::Music by Ralph Rainger
::Lyrics by Leo Robin
* "Through the Doorway of Dreams"
::Music by Ralph Rainger and Richard A. Whiting
::Lyrics by Leo Robin
* "Tales from the Vienna Woods"
::Music by Johann Strauss, Jr.
* "William Tell Overture"
::Music by Gioachino Rossini
* "Light Cavalry Overture"
::Music by Franz von Suppé

==Films in series==
* The Big Broadcast (1932)
* The Big Broadcast of 1937 (1936)
* The Big Broadcast of 1938 (1937)

==References==
 
==Sources==
* Hischak, Thomas S., ed. The Oxford Companion to the American Musical: Theatre, Film, and Television. Oxford University Press, 2008.
*Hark, Ina Rae. American Cinema of the 1930s: Themes and Variations. Rutgers University Press, 2007, p. 10.
*Simon, George Thomas. Glenn Miller and His Orchestra. Da Capo paperback reprint, 2000.

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 