Dans le silence, je sens rouler la terre
 

{{Infobox film
| name           = Dans le silence, je sens rouler la terre
| image          = 
| caption        = 
| director       = Mohamed Lakhdar Tati
| producer       = Stella Productions
| writer         = 
| starring       = 
| distributor    = 
| released       = 2010
| runtime        = 52 minutes
| country        = Algeria France
| language       = 
| budget         = 
| gross          = 
| screenplay     = Mohamed Lakhdar Tati
| cinematography = Sylvie Petit
| editing        = Julie Durré
| music          = Mohamed Zami
}}

Dans le silence, je sens rouler la terre is a 2010 documentary film.

== Synopsis ==
In 1939, the end of the Spanish Civil War forced thousands of men, women and children to flee Francoist Spain. The French administration in Algeria opened refugee camps to take them in. Seventy years later, a young Algerian investigates the past. Despite the absence of archives and files, the traces of these camps have survived the collective oblivion and still appear in current Algeria.

== References ==
 

 
 
 
 
 
 
 
 
 


 
 