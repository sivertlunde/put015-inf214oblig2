Graft (1931 film)
 
{{Infobox film
 | name = Graft
 | image = Graft VideoCover.jpeg
 | caption = Video cover
 | director = Christy Cabanne
 | producer = Samuel Bischoff
 | writer = Barry Barringer
 | starring = Regis Toomey Sue Carol
 | music = 
 | cinematography = Jerome Ash
 | editing = Maurice Pivar Universal Pictures
 | released =  
 | runtime = 54 minutes
 | country = United States
 | language = English
 | budget = 
}}

Graft is a 1931 American thriller film directed by Christy Cabanne, starring Regis Toomey and future talent agent Sue Carol, and featuring Boris Karloff, who appeared in Frankenstein (1931 film)|Frankenstein during the same year.

==Cast==
* Regis Toomey as Dusty Hotchkiss
* Sue Carol as Constance Hall
* Dorothy Revier as Pearl Vaughan
* Boris Karloff as Joe Terry George Irving as Robert Hall Richard Tucker as Carter Harrison
* William B. Davidson as M.H. Thomas
* Willard Robertson as Scudder Harold Goodwin as Speed Hansen
* Carmelita Geraghty as Secretary

==External links==
* 

 
 
 
 
 
 
 
 
 

 