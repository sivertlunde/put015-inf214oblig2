A Wonderful Night in Split
 
{{Infobox film|
  name     = Ta divna splitska noć |
  image          =  |
  writer         = Arsen Anton Ostojić |
  starring       = Dino Dvornik Marija Škaričić Coolio Mladen Vulić Nives Ivanković Marinko Prga Vicko Bilandžić Ivana Roščić |
  producer       = Jozo Patljak|
  director       = Arsen Anton Ostojić|
  cinematography = Mirko Pivčević| 
  editing        = Dubravko Slunjski |
  distributor    = |
  released   = July 21, 2004, Croatia |
  runtime        = 100 mins | English |
  budget         = |
  music          = Mate Matišić|
  awards         = |
  tagline        = |
}}

A Wonderful Night in Split ( ) is a 2004 Croatian drama film directed by Arsen Anton Ostojić.

== Synopsis == US Navy sailor called Franky (Coolio) in exchange for some heroin; the third one shows a young couple, Luka and Anđela (Vicko Bilandžić and Ivana Roščić) who spend the night desperately looking for a place to celebrate the New Year by having their first sexual experience. The plots are connected through a drug dealer Crni (Marinko Prga), through Dino Dvorniks concert, where all of them pass through at some point, and through the omnipresent fireworks that dot the night sky over the course of the film. 

==Cast==

* Mladen Vulić as Nike, drug dealer
* Nives Ivanković as Marija, young widow
* Marija Škaričić as Maja, drug addict Split
* Vicko Bilandžić as Luka, a young man
* Ivana Roščić as Anđela, a young woman
* Dino Dvornik as himself
* Marinko Prga as Crni, drug dealer

== Production==
Ostojić had considered a role for Dino Dvornik ever since he started writing the screenplay, seeing him as a person who best embodies the spirit of the city of Split. Dvornik readily accepted the offer, taking keen interest in the story, and even gave several suggestions that made it into the film.   

Since the film was designed as a visual commentary of social problems that Split is well known for in Croatia, depicted here as always involving drugs and awkward relationships, the whole film was shot in black-and-white photography. The resulting effect is an atmosphere of anxiety and depression, set to contrast the colorful mood of open-air New Year celebrations in Splits main square.

==Reception==
The film won the Golden Arena awards for Best Cinematography (Marko Pivčević) and Best Film Editing (Dubravko Slunjski) at the 2004 Pula Film Festival, as well as the Birch award for best directing debut and the Oktavijan critics choice award.

The films cinematographer, Mirko Pivčević, was nominated for the Golden Frog award at the Camerimage festival, the most renowned European festival dedicated to cinematography, and the film was also shown at the Sarajevo Film Festival as part of their regional program, where it won the Jury Prize for Best Film, and for Best Actress (Marija Škaričić). It has also earned the director, Arsen Ostojić, a nomination for the European Discovery of the Year at the European Film Awards, as well as the Best Feature Film award at the 2006 RiverRun International Film Festival.

==References==
 
* http://arhiv.slobodnadalmacija.hr/20060126/kultura01.asp
* http://www.jutarnji.hr/template/article/article-print.jsp?id=242707

== External links ==
*  
*  

 
 
 
 
 
 
 
 