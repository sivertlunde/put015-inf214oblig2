The Lighthorsemen (film)
 
{{Infobox film
| name           = The Lighthorsemen
| image          = The Lighthorsemen DVD.jpg
| caption        = DVD box art
| director       = Simon Wincer Ian Jones David Lee Simon Wincer Ian Jones John Walton Jon Blake
| music          = Mario Millo
| cinematography = Dean Semler
| editing        = Adrian Carr
| distributor    = Hoyts Columbia TriStar
| released       =   

| runtime        = 131 minutes
| country        = Australia
| language       = English
| budget         = A$10,489,320 "Australian Productions Top $175 million", Cinema Papers, March 1986 p64  David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p27-29 
| gross          = A$1,617,288 (Australia)
}} 
 Australian feature light horse Battle of Beersheeba.  The film is based on a true story and most of the characters in the film were based on real people.
 Breaker Morant (1980), Gallipoli (1981 film)|Gallipoli (1981), and the 5-part TV series Anzacs (1985).  Recurring themes of these films include the Australian identity, such as mateship and larrikinism, the loss of innocence in war, and also the continued coming of age of the Australian nation and its soldiers (the ANZAC spirit).

==Plot== Jon Blake) John Walton) in Palestine in 1917, part of the 4th Light Horse Brigade of the British and Commonwealth Dominion forces. When Frank is wounded and dies of his wounds, he is replaced by Dave (Peter Phelps).  Dave finds himself unable to fire his weapon in combat and is transferred to the Medical Corps, where he will not need to carry a weapon, but where he will still be exposed to the fighting.  

The British plan the capture of Beersheba.  During an attack by Turkish cavalry, Major Richard Meinertzhagen (Anthony Andrews) deliberately leaves behind documents indicating that the attack on Beersheba will only be a diversion.

The Australians leave for Beersheba, with limited water and supplies.  They bombard the town and the 4,000 Turkish-German defenders prepare for an assault. However, the German military advisor, Reichert (Shane Briant), believes it is a diversionary attack and advises the Turkish commander he does not need reinforcements.
 4th and 12th Light Horse Regiments are ordered to attack. Dave and the rest of the medical detachment prepare for casualties and are ordered in behind the Light Horse.
 sights on their rifles as the Light Horse get closer, eventually firing straight over the Australians heads.

During the charge, Tas is killed by an artillery shell.  The remaining Australians make it "under the guns" (advancing faster than the artillery can correct its aim for the reduced range) and reach the Turkish trenches.  

The Australians capture the first Turkish defences. Scotty and a few others take control of the guns. Chiller is wounded in the trench fight. Dave is struck by a grenade and is seriously wounded while protecting Chiller. Scotty continues to fight on into the town. When most of the remaining Turks surrender, Reichert tries to destroy the wells, but is captured by Scotty. 

Overall, the attack was a success and the Australians miraculously suffered only 31 dead and 36 wounded.

==Production==
The script was written by Ian Jones, who had long been interested in the Australian Light Horse ever since they featured in an episode of Matlock Police in 1971. He visited Beersheba in 1979 and had researched the period thoroughly. Simon Wincer came on board as director and he succeeded in helping secure a $6 million pre sale to RKO. Antony I. Ginnanes Film and General Holdings Company succeeded in raising the rest of the money.  Simon Wincer later claimed that Ginanne, Ian Jones and himself had to put in their own money at some stage when the film looked like falling over. 

Despite being set in Palestine and Egypt, the film was shot entirely on location in Victoria and Hawker, South Australia.
 Jon Blake Nectar Brook, South Australia. He suffered permanent paralysis and brain damage. 

The musical score was composed by Mario Millo. The original soundtrack recording was produced for compact disc release courtesy of Antony I Ginnane by Philip Powers and Mario Millo for Australian distribution in Australia by 1M1 Records and as a coupling with Shame on LP in America.

The movie was re-cut to a shorter length for the US release, which Wincer thought made the second half better, although he did not like the opening as much. Scott Murray, "Simon Wincer: Trusting His Instincts", Cinema Papers, November 1989 p79 

==Historical inaccuracies== General Kressensteins car is in the film red-white-black. In reality the flag of the German Empire was black-white-red (black-white stands for Prussia, white-red for the Hansa).Also the Australia flag depicted was incorrect it showed the stars on a blue back ground it was not until 1953 that the Australian flag was changed to this, prior to 1953 the stars were on a red background.

==Reception==
The film received positive to mixed views from critics. Rotten Tomatoes gives it an 80% approval rating, based on five reviews.   An unfavourable review came from The New York Times, who stated the film was "a sort of pacifist-aggressive war adventure" and that "None of the performances are really bad, but none are very good".    The Washington Post also gave the film a negative review, described it as "Mostly ... equine cinematography, a four-legged coffeetable movie about the Australian cavalry.". 

The Lighthorsemen is included in the Australian Film Commissions Top Australian films at the Australian box office list at number 83. The film grossed   in Australia after its release in 1987. It was also released in Canada, Sweden, the United Kingdom, and the United States in 1988. It was considered a commercial disappointment, yet Wincer claims its pre-sales and television sales were about $6 million or 60% of the budget.  
 AFI award in 1988 for Best Original Music Score and another for Best Achievement in Sound.   It was also nominated for Best Achievement in Cinematography.

==Box Office==
The Lighthorsemen grossed $1,617,288 at the box office in Australia,  which is equivalent to $8,250,749
in 2009 dollars.

==See also==
*Cinema of Australia

==References==
 

==External links==
*  
*  
 
 
 

 
 
 
 
 
 
 
 
 