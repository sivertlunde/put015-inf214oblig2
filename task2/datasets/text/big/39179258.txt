Irattai Roja
{{Infobox film
| name           = Irattai Roja
| image          = 
| image_size     =
| caption        =  Keyaar
| producer       = K. C. Sekar Babu
| writer         = N. Prasannakumar  (dialogues) 
| screenplay     = Keyaar
| story          = Bhoopathi Raja
| starring       =  
| music          = Ilaiyaraaja
| cinematography = J. A. Robert
| editing        = S. Ramesh
| distributor    = Devi Kamal Films
| studio         = Devi Kamal Films
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Tamil
}}
 1996 Tamil Tamil drama Urvashi and Kushboo in lead roles. The film, produced by  K. C. Sekar Babu, had musical score by Ilaiyaraaja and was released on 5 April 1996. The film is a remake of the Telugu film Shubhalagnam (1994).  

==Plot==

Uma (Urvashi (actress)|Urvashi) marries Balu (Ramki), an honest civil engineer. Uma thinks that he is rich and receives lot of bribes but turns out Balu is from the middle-class like her. Uma is a miser and dreams to become rich. After few years, they have two children. In the meantime, Priya (Kushboo (actress)|Kushboo), the daughter of Balus boss Rajasekhar (Rajasekhar), falls in love with Balu and asks him to marry her unaware of Balus marriage and his two children. Balu refuses to marry her as he was already married but she compels him. When Priya meets Uma, she offers her one crore rupees in exchange for marrying Balu. Uma immediately agrees and she starts forcing Balu for remarriage. Finally, Balu agrees with heavy heart as Uma threatens to commit suicide. Soon, Balu also starts falling in love with Priya. Uma slowly realizes her mistake and wants to get Balu back. The rest of the story is what happen to Balu, Uma and Priya.

==Cast==

 
*Ramki as Balu Urvashi as Uma Kushboo as Priya
*Suhasini Maniratnam as Geetha
*Srividya as an advocate
*Venniradai Moorthy as Umas father
*Kavitha as Umas mother
*Rajasekhar as Rajasekhar, Priyas father
*Chinni Jayanth
*Pandu Thyagu
*Anuja
*Shakeela
*Sharmili as Girija
*M. R. K.
*Oru Viral Krishna Rao as Doctor
*Junior Balaiah
*Swaminathan
*Disco Shanti
*Ajay Rathnam
*Baby Sridevi as Sumathi
*Master Prabhu
*Visu as an anchor (Guest appearance)
 

==Awards==

;1996 Cinema Express Awards
 Kushboo

;1996 Filmfare Awards South
 Urvashi

;1996 Screen Videocon Awards

*Won - Screen Videocon Award for Best Actress – Tamil - Urvashi

==Soundtrack==

{{Infobox Album |  
| Name        = Irattai Roja
| Type        = soundtrack
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1996
| Recorded    = 1996 Feature film soundtrack |
| Length      = 23:19
| Label       = 
| Producer    = Ilaiyaraaja
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1996, features 5 tracks with lyrics written by Vaali. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Notes
|- 1 || Ada Enna Samsaram || SP Balasubramaniam, Geetha || 
|- 2 || Arasana Nambi || Malaysia Vasudevan, K. S. Chithra || 
|- 3 || Pombalainga Kaiye || Ilayaraja, Arunmozhi || 
|- 4 || Siruvaani Aathu || Mano, Chitra || 
|- 5 || Unnai Padatha || SP Balasubramaniam|| 
|- 6 || Unnai Vidamaaten || SP Balasubramaniam, Mano, Bhavatharini || 
|}

==References==
 

==External links==
* 
 

 
 
 
 
 
 