Salome (1953 film)
{{Infobox film
| name           = Salome
| image          = 1953 Salome.jpg
| caption        = Original film poster
| director       = William Dieterle
| producer       = Buddy Adler
| writer         = Jesse Lasky Jr. (story)  Harry Kleiner (story)
| starring       = Rita Hayworth Stewart Granger Charles Laughton
| music          = Daniele Amfitheatrof George Duning
| cinematography = Charles Lang
| editing        = Viola Lawrence
| distributor    = Columbia Pictures Corporation
| released       =  
| runtime        = 103 min.
| country        = United States English
| budget         =
| gross          = $4.75 million (US)  3,0047,090 admissions (France) 
}}

: This article is about the 1953 film. For other uses see Salome (disambiguation) Biblical epic film made in Technicolor by Columbia Pictures. It was directed by William Dieterle and produced by Buddy Adler from a screenplay by Harry Kleiner and  Jesse Lasky Jr. The music score was by George Duning, the dance music by Daniele Amfitheatrof and the cinematography by Charles Lang. Hayworths costumes by Jean Louis. Hayworths dances for this film were choreographed by Valerie Bettis. This film was the last produced by Hayworths production company, the Beckworth Corporation.

The film starred Rita Hayworth as Salome, as well as Stewart Granger, Charles Laughton and Judith Anderson, with Cedric Hardwicke, Alan Badel and Basil Sydney.

==Plot==
Although based on the New Testament story, the film does not follow the biblical text. 
In Galilee, during the rule of Romes Tiberius Caesar, King Herod (Charles Laughton) and Queen Herodias (Judith Anderson) sit on the throne and are condemned by a prophet known as John the Baptist (Alan Badel). Herodias resents Johns denunciation of her marriage to the king, her former husbands brother, and the Baptists claim that she is an adulteress. The king is not pleased with the Baptist condemning his rule, but fears a slow and agonizing death that his father, the elder Herod, suffered after ordering the murder of firstborn males when Jesus was born. The prophesy states that if Herod kills the Messiah, he will suffer and die. The king believes John the Baptist is the Messiah because of the mistaken belief of some peasants.

After petitioning Caesar to marry Salome, Marcellus- nephew of Caesar- receives a message stating that he is forbidden to marry a "barbarian." Salome is also sent a message that she is banished from Rome and will be escorted back to Galilee, despite having lived in Rome most of her life. When Marcellus does nothing to protest Caesars decree, she declares that she shall never love another Roman.

On the boat escorting her home, Salome meets Claudius, a Roman soldier assigned to the palace of Herod. He is amused by her haughty behavior and thwarts her attempt to order him around when she demands to use drinking water instead of sea water for her bath aboard the ship. When he brings sea water instead, he interrupts her angry confrontation by stealing a long kiss, which earns him a slap across the face.

Queen Herodias greets her daughter warmly when she arrives at the palace, and becomes aware of the lecherous intentions of the king, who marvels at Princess Salomes beauty. The queen sends Salome away and consults with her advisor, who agrees that the queen can use the kings desire for Salome for her own benefit. It is revealed that the queen is trapped in a loveless and potentially deadly marriage to the king because she wishes to preserve the throne for her daughter. Meanwhile, Salome sneaks into the marketplace with several servants to hear John the Baptist speak. She reveals her identity by speaking out and is spared from the angry crowd by John the Baptist, who calms them and denounces violence. Salome returns, upset by what she has heard, and later beguiles Claudius with romantic kisses in an attempt to have him arrest John the Baptist to spare her mothers potential death as an adulteress. She exits the room angrily after he refuses her request.

Shortly after, the king decides to arrest John the Baptist to intimidate him and force him out of Galilee. At the trial, however, the kings attempts fail and the encounter ends with the king locking up John the Baptist. Salome hears that the prophet has been arrested; she thinks that Claudius did it for her, and apologizes to him for her behavior the night before. After she leaves, he learns that the king has imprisoned the Baptist and pleads for his release, but is unable to persuade the king. Claudius rushes off to Jerusalem on horseback to seek his release.

The king visits Salome, who bids Claudius farewell from the balcony, and is irritated that she pays him attention. Herod suggests after giving the princess a necklace that she "find pleasure in the moment," which disgusts her, and she subsequently rejects his gift, reminding him that his queen is her mother. Roman soldier Claudius (Granger) who converts to Christianity, and decides to help him.

Claudius and Salome both rush off to try to save the Baptists life. Claudius forcefully removes the Baptist from his cell and clashes with the palace guards; Salome dances a wild, enchanting dance and removes layers of clothing, thinking this will please the king, then she will ask him to set John free. But as she dances Herod tells Herodias that he will do anything for her because he likes the dance so much. Herodias asks him to kill John and John is beheaded before Salome finishes her dance. Horrified, she renounces her mother Herodias, who planned and ordered the execution, and also becomes a Christian convert. The last scene shows Hayworth and Granger listening to Christ (whose face is not shown) delivering the Sermon on the Mount.

== Dance of the seven veils ==
According to her biographers Hayworths erotic dance routine was "the most demanding of her entire career", necessitating "endless takes and retakes". 

==Cast==
* Rita Hayworth as Princess Salome
* Stewart Granger as Commander Claudius
* Charles Laughton as King Herod
* Judith Anderson as Queen Herodias
* Sir Cedric Hardwicke as Tiberius Caesar
* Alan Badel as John the Baptist
* Basil Sydney as Pontius Pilate
* Maurice Schwartz as Ezra the Kings Advisor
* Arnold Moss as Micha the Queens Advisor Asoka as Asian Dancer Sujata as Asian Dancer

==Reception==
The film was a big hit in France, with admissions of 3,047,090.   at Box Office Story 
== References ==
 

==External links==
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 