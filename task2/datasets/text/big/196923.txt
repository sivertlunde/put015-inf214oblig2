Napoléon (1927 film)
 
 
{{Infobox film name           =Napoléon image          =Napoleon 1927.jpg alt            =A monochrome photographic portrait of a handsome man in his late 20s wearing a French generals uniform from the 1790s and a cocked hat over stringy dark hair that reaches his shoulders caption  Napoleon
|director       =Abel Gance producer       =Abel Gance writer         =Abel Gance starring       =Albert Dieudonné Gina Manès Antonin Artaud Edmond Van Daële music          =Arthur Honegger (1927 in France) Werner Heymann (1927 in Germany) Carl Davis (1980 in the UK) Carmine Coppola (1980 in the US) cinematography =Jules Kruger editing        =Marguerite Beaugé (1927) various others at later times studio         = distributor  Gaumont (Europe) MGM (USA) released       =  runtime        =330 minutes (and various other lengths) country        =France language       =Silent film with intertitles budget         = gross          =
}} silent French split screen and mosaic shots, multi-screen projection, and other visual effects.   A revival of Napoléon in the mid-1950s influenced the filmmakers of the French New Wave. 

The film begins in Brienne-le-Château with youthful Napoleon attending military school where he manages a snowball fight like a military campaign, yet he suffers the insults of other boys. It continues a decade later with scenes of the French Revolution and Napoleons presence at the periphery as a young army lieutenant. He returns to visit his family home in Corsica but politics shift against him and put him in mortal danger. He flees, taking his family to France. Serving as an officer of artillery in the Siege of Toulon, Napoleons genius for leadership is rewarded with a promotion to brigadier general. Jealous revolutionaries imprison Napoleon but then the political tide turns against the Revolutions own leaders. Napoleon leaves prison, forming plans to invade Italy. He falls in love with the beautiful Joséphine de Beauharnais. The emergency government charges him with the task of protecting the National Assembly. Succeeding in this he is promoted to Commander-in-Chief of the Army of the Interior, and he marries Joséphine. He takes control of the army which protects the French–Italian border, and propels it to victory in an invasion of Italy.

Gance planned for Napoléon to be the first of six movies about Napoleons career, a chronology of great triumph and defeat ending in Napoleons death in exile on the island of Saint Helena. After the difficulties encountered in making the first film, Gance realised that the costs involved would make the full project impossible.

The film was first released in a gala at the Palais Garnier (then the home of the Paris Opera) on 7 April 1927. Napoléon had been screened in only eight European cities when Metro-Goldwyn-Mayer bought the rights to it, but after screening it in London, it was cut drastically in length, and only the central panel of the three-screen Polyvision sequences were retained before it was put on limited release in the US. There, the silent masterpiece was indifferently received at a time when talkies were just starting to appear. The film was restored in 1981 after twenty years work by silent film historian Kevin Brownlow.

==Plot== Librairie Plon. Much of the scenario describes scenes that were rejected during initial editing, and do not appear in any known version of the film. The following plot includes only those scenes that are known to have been included in some version of the film. Not every scene described below can be viewed today. 

===Part One===
====Brienne==== Minim Fathers in Brienne-le-Château, France. The boys at the school are holding a snowball fight organised as a battlefield. Two bullies—Philippeaux (Petit Vidal) and Peccaduc (Roblin)—schoolyard antagonists of Napoleon, are leading the larger side, outnumbering the side that Napoleon fights for. These two sneak up on Napoleon with snowballs enclosing stones. A hardened snowball draws blood on Napoleons face. Napoleon is warned of another rock-snowball by a shout from Tristan Fleuri (Nicolas Koline), the schools scullion  and a friend to Napoleon. Napoleon recovers himself and dashes alone to the enemy snowbank to engage the two bullies in close combat. The Minim Fathers, watching the snowball fight from windows and doorways, applaud the action. Napoleon returns to his troops and encourages them to attack ferociously. He watches keenly and calmly as this attack progresses, assessing the balance of the struggle and giving appropriate orders. He smiles as his troops turn the tide of battle. Carrying his sides flag, he leads his forces in a final charge and raises the flag at the enemy stronghold.

The monks come out of the school buildings to discover who led the victory. A young military instructor, Jean-Charles Pichegru (René Jeanne), asks Napoleon for his name. Napoleon responds "Nap-eye-ony" in Corsican-accented French, and is laughed at by the others. Pichegru tells him that he will go far.

In class, the boys study geography. Napoleon is angered by the condescending textbook description of Corsica. He is taunted by the other boys, and kicked by the two bullies who hold flanking seats. Another of the classs island examples is Saint Helena, which puts Napoleon into a pensive daydream.
 limber of a cannon, then he looks up to see the young eagle in a tree. He calls to the eagle which flies down to the cannon barrel. Napoleon caresses the eagle and smiles through his tears.

====The French Revolution====
In 1792, the great hall of the Club of the Cordeliers is filled with revolutionary zeal as hundreds of members wait for a meeting to begin. The leaders of the group, Georges Danton (Alexandre Koubitzky), Jean-Paul Marat (Antonin Artaud) and Maximilien Robespierre (Edmond Van Daële), are seen conferring. Camille Desmoulins (Robert Vidalin), Dantons secretary, interrupts Danton to tell of a new song that has been printed, called "La Marseillaise". A young army captain, Claude Joseph Rouget de Lisle (Harry Krimer) has written the words and brought the song to the club. Danton directs de Lisle to sing the song to the club. The sheet music is distributed and the club learns to sing the song, rising in fervor with each passage. At the edge of the crowd, Napoleon (Albert Dieudonné), now a young army lieutenant, thanks de Lisle as he leaves: "Your hymn will save many a cannon."
 Paul Barras Mademoiselle Lenormand (Carrie Carvalho), the fortune teller. Inside, Lenormand exclaims to Joséphine that she has the amazing fortune to be the future queen.
 National Assembly, Danton tells the crowd that they have cracked the monarchy. Napoleon senses a purpose rising within him, to bring order to the chaos. The mob violence has tempered his character.

Napoleon, on leave from the French Army, travels to Corsica with his sister,  ) interrupts the happy welcome to tell Napoleon the bad news that Corsicas president, Pasquale Paoli (Maurice Schutz) is planning to give the island to the British. Napoleon declares his intention to prevent this fate.
 Milelli gardens Pozzo di Buonaparte home. Lucien (Sylvio Joseph (Georges Calvi to see if French authorities can intervene. Napoleon faces the danger alone, walking into an inn where men are arguing politics, all of whom would like to see him dead. He confronts the men and says, "Our fatherland is France&nbsp;...with me!" His arguments subdue the crowd, but di Borgo enters the inn, accompanied by gendarmes. Napoleon evades capture and rides away on his horse, pursued by di Borgo and his men.
 French flag flies outside the window. Napoleon climbs up the balcony and takes down the flag, shouting to the council, "It is too great for you!" The men fire their pistols at Napoleon but miss as he rides away.

While chasing Napoleon, di Borgo stretches a rope across a road that Napoleon is likely to take. As expected, Napoleon rides toward the rope, but he draws his saber and cuts it down. Napoleon continues at high speed to the shore where he finds a small boat. He abandons the horse and gets into the boat, discovering that it has no oars or sail. He unfurls the French flag from Ajaccio and uses it as a sail. He is drawn out into the open sea.
 Girondists are losing to the The Mountain|Montagnards: Robespierre, Danton, Marat and their followers. Robespierre calls for all Girondists to be indicted. (Napoleons boat is tossed by increasing waves.) The Girondists seek to flee but are repulsed. (A storm throws Napoleon back and forth in his boat.) The assembly hall rolls with the struggle between Girondists and Montagnards. (Napoleon grimly bails water to prevent his violently rocking boat from sinking.)

Later, in calm water, the small boat is seen by Lucien and Joseph Buonaparte aboard a French ship, Le Hasard. The larger ship is steered to rescue the unknown boat, and as it is pulled close, Napoleon is recognised, lying unconscious at the bottom, gripping the French flag. Waking, Napoleon directs the ship to a cove in Corsica where the Buonaparte family is rescued. The ship sails for France carrying a future queen, three future kings, and the future Emperor of France. A British warship, the  ), asks his captain if he might be allowed to shoot at the enemy vessel and sink it. The captain denies the request, saying that the target is too unimportant to waste powder and shot. As Le Hasard sails away, an eagle flies to the Buonapartes and lands on the ships flag pole.

===Part Two===
  played Napoleon]] besieging the port of Toulon, held by 20,000 English, Spanish and Italian troops. Captain Napoleon is assigned to the artillery section and is dismayed by the obvious lack of French discipline. He confronts Carteaux in an inn run by Tristan Fleuri, formerly the scullion of Brienne. Napoleon advises Carteaux how best to engage the artillery against Toulon, but Carteaux is dismissive. An enemy artillery shot hits the inn and scatters the officers. Napoleon stays to study a map of Toulon while Fleuris young son Marcellin (Serge Freddy-Karl) mimes with Napoleons hat and sword. Fleuris beautiful daughter Violine Fleuri (Annabella (actress)|Annabella) admires Napoleon silently.

General Jacques François Dugommier (Alexandre Bernard) replaces Carteaux and asks Napoleon to join in war planning. Later, Napoleon sees a cannon being removed from a fortification and demands that it be returned. He fires a shot at the enemy, and establishes the position as the "Battery of Men Without Fear". French soldiers rally around Napoleon with heightened spirits. Dugommier advances Napoleon to the position of commander-in-chief of the artillery.
 drummer boy, Samuel Hood (W. Percy Day) orders the burning of the moored French fleet before French troops can recapture the ships. The next morning, Dugommier, seeking to promote Napoleon to the rank of brigadier general, finds him asleep, exhausted. An eagle beats its wings as it perches on a tree next to Napoleon.

===Part Three=== canal to Suez as Saliceti taunts him for not trying to form a legal defense.

In an archive room filled with the files of condemned prisoners, clerks Bonnet ( ) and La Bussière (Jean dYd) work secretly with Fleuri to destroy (by eating) some of the dossiers including those for Napoleon and Joséphine. Meanwhile, at the National Assembly, Violine with her little brother Marcellin, watches from the gallery. Voices are raised against Robespierre and Saint-Just. Jean-Lambert Tallien (Jean Gaudrey) threatens Robespierre with a knife. Violine decides not to shoot Saint-Just with a pistol she brought. Back at the archives, the prison clerks are given new dossiers on those to be executed by guillotine: Robespierre, Saint-Just and Couthon.
 Marmont (Pierre Junot (Jean Talma (Roger Blum). Napoleon and Junot see the contrast of cold, starving people outside of wealthy houses.

Joséphine convinces Barras to suggest to the National Assembly that Napoleon is the best man to quell a royalist uprising. On 3 October 1795 Napoleon accepts, and supplies 800 guns for defense. Directed by Napoleon, Major Joachim Murat (Genica Missirio) seizes a number of cannon to fight the royalists. Di Borgo shoots at Napoleon but misses; di Borgo is then wounded by Fleuris accidental musket discharge. Saliceti is prevented from escaping in disguise. Napoleon sets Saliceti and di Borgo free. Joseph Fouché (Guy Favière) tells Joséphine that the noise of the fighting is Napoleon "entering history again". Napoleon is made General in Chief of the Army of the Interior to great celebration.

A  ), and Napoleon is also fascinated. He plays chess with Hoche, beating him as Joséphine watches and entices Napoleon with her charms. The dancers at the ball become uninhibited; the young women begin to dance partially nude.

  played Joséphine de Beauharnais, Napoleons wife.]] French Army of Italy. Playing with Joséphines children, Napoleon narrowly misses seeing Barras in her home. Joséphine hires Violine as a servant.

Napoleon plans to invade Italy. He wishes to marry Joséphine as quickly as possible before he leaves. Hurried preparations go forward. However, on the wedding day, 9 March 1796, Napoleon is two hours late. He is found in his room planning the Italian campaign, and the wedding ceremony is rushed. That night, Violine and Joséphine both prepare for the wedding bed. Violine prays to a shrine of Napoleon. Joséphine and Napoleon embrace at the bed. In the next room, Violine kisses a shadowy figure of Napoleon that she has created from a doll.

Just before leaving Paris, Napoleon enters the empty National Assembly hall at night, and sees the spirits of those who had set the Revolution in motion. The ghostly figures of Danton and Saint-Just speak to Napoleon, and demand answers from him regarding his plan for France. All the spirits sing "La Marseillaise".

Only 48 hours after his wedding, Napoleon leaves Paris in a coach for Nice. He writes dispatches, and letters to Joséphine. Back in Paris, Joséphine and Violine pray at the little shrine to Napoleon.
 advances into Montenotte and takes the town. Further advances carry Napoleon to Montezemolo. As he gazes upon the Alps, visions appear to him of future armies, future battles, and the face of Joséphine. The French troops move forward triumphantly as the vision of an eagle fills their path, a vision of the red, white and blue French flag waving before them.

==Primary cast==
*Albert Dieudonné as Napoléon 
*Vladimir Roudenko as Napoléon Bonaparte (child)
*Edmond Van Daële as Maximilien Robespierre
*Alexandre Koubitzky as Georges Danton
*Antonin Artaud as Jean-Paul Marat
*Abel Gance as Louis de Saint-Just
*Gina Manès as Joséphine de Beauharnais
*Marguerite Gance as Charlotte Corday
*Yvette Dieudonné as Élisa Bonaparte
*Philippe Hériat  as Antoine Saliceti Annabella as Violine Fleuri
*Nicolas Koline as Tristan Fleuri

==Music==
The film features Gances interpretation of the birth of the song "  portrays the spirit of the song. "La Marseillaise" is played by the orchestra repeatedly during a scene at the Club of the Cordeliers, and again at other points in the plot. During the 1927 Paris Opera premiere, the song was sung live by Alexandre Koubitzky to accompany the Cordeliers scene. Koubitzky played Danton in the film but he was also a well-known singer. Gance had earlier asked Koubitzky and Damia to sing during the filming of the Cordeliers scene to inspire the cast and extras.  Kevin Brownlow wrote in 1983 that he thought it was "daring" of Gance "to make a song the highpoint of a silent film!" 

The majority of the film is accompanied by incidental music. For this material, the original score was composed by Arthur Honegger in 1927 in France. A separate score was written by Werner Heymann in Germany, also in 1927. In pace with Brownlows efforts to restore the movie to something close to its 1927 incarnation, two scores were prepared in 1979–1980; one by Carl Davis in the UK and one by Carmine Coppola in the US. Brownlow 1983, p. 236 
 La Carmagnole". Coppola returns to "La Marseillaise" as the finale. Coppolas score was heard first in New York at Radio City Music Hall performed for very nearly four hours, accompanying a film projected at 24 frames per second as suggested by producer Robert A. Harris.  Coppola included some sections of music carried solely by an organist to relieve the 60-piece orchestra. 
 Eroica Symphony David Gill and Liz Sutherland; the three had just completed the Thames Television documentary series Hollywood (1980 TV series)|Hollywood (1980), on the silent film era. To accompany a screening of 4 hours 50 minutes shown at 20 frames per second during the 24th London Film Festival, Davis conducted the Wren Orchestra.  Following this, work continued on restoration of the film with the goal of finding more footage to make a more complete version. In 2000, Davis lengthened his score and a new version of the movie was shown in London, projected at 20 frames per second for 5 hours 32 minutes. The 2000 score was performed in London in 2004 and 2013, and also in Oakland, California, in 2012, with Davis conducting local orchestras.

==Triptych sequence==
  wrote, produced, directed and acted in the film.]]
  widescreen (such as Fox Grandeur) in 1929, widescreen did not take off until CinemaScope was introduced in 1953. 

Polyvision was only used for the final reel of Napoleon, to create a climactic finale. Filming the whole story in Polyvision was impractical as Gance wished for a number of innovative shots, each requiring greater flexibility than was allowed by three interlocked cameras. When the film was greatly trimmed by the distributors early on during exhibition, the new version only retained the center strip in order to allow projection in standard single-projector cinemas. Gance was unable to eliminate the problem of the two seams dividing the three panels of film as shown on screen, so he avoided the problem by putting three completely different shots together in some of the Polyvision scenes. When Gance viewed Cinerama for the first time in 1955, he noticed that the widescreen image was still not seamless, that the problem was not entirely fixed. 

==Released versions and screenings==
{| class="sortable wikitable"
|-
! style="background:#ccf; width=9%"  | Date
! style="background:#ccf; width=15%" | Title
! style="background:#ccf; width=15%" | Length
! style="background:#ccf; width=15%" | Editor
! style="background:#ccf; width=15%" | Score
! style="background:#ccf; width=15%" | Venues
! style="background:#ccf; width=15%" | Triptych
! style="background:#ccf; width=9%" | Format

|- style="background:#fffff6"
| align="center" | April 1927
| Napoléon
| 5400 m (4:10)
|  
|  
| Paris Opera
| toned
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | May 1927
| Napoléon (version définitive)
| 12,800 m (9:22)
|  
|  
| Apollo Theatre, Paris
| none
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | October 1927
| Napoléon (UFA)
| under 3:00
| Universum Film AG
|  
| Germany and Central Europe
| toned
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | November 1927
| Napoléon
| total 4:10, shown in two seatings, some scenes repeated
|  
|  
| Marivaux Theatre, Paris
| toned, shown twice
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | Winter 1927–28
| Napoléon
| various
|
|
| French provinces
|
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | ca. 1928
| Napoleon (version définitive as sent to the U.S. in 29 reels)
|   (6:43)
|  
|
| none
| none
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | March–April 1928
| Napoléon (Gaumont)
| Shown in two parts totaling about 3:00
| Gaumont Film Company
|  
| Gaumont-Palace
| none
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | June 1928
| Napoleon (UK 1928)
| 11,400 m (7:20)
|  
|  
| UK
| toned
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | January 1929
| Napoleon (USA 1929)
|   (1:51)
| Metro-Goldwyn-Mayer
|
| USA
| none
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | ca. 1928
| Napoléon (Pathé-Rural)
| 17 reels
|
|
| Rural France
| none
| 17.5 mm
|-

|- style="background:#fffff6"
| align="center" | 1928
| Napoléon (Pathé-Baby)
| 9 reels
|
|
| French homes
| none
| 9.5 mm
|-

|- style="background:#fffff6"
| align="center" | 1929
| Napoléon (Pathescope)
| 6 reels
|
|
| UK homes
| none
| 9.5 mm
|-

|- style="background:#fffff6"
| align="center" | 1935
| Napoléon Bonaparte vu et entendu par Abel Gance
|  , later  
|  
|  
|
| none
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | 1935
| Napoléon Bonaparte (Film-Office version)
|  
|  
|  
|
| none
| 16 mm 9.5 mm 8 mm
|-

|- style="background:#fffff6"
| align="center" | 1935
| Napoléon Bonaparte (Studio 28 version)
|
|
|
|
| black and white
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | 1965
| Napoléon
|
|  
|
| Cinematheque Francaise
| none
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | 1970
| Bonaparte et la Révolution
| 4:45 at 20 fps (4:00 at 24 fps)
|  
|
|
| none
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | 1979
| Napoléon (Brownlow)
| 4:55 at 20 fps
|  
| None: ad lib accompaniment on electronic piano 
| Telluride Film Festival, Colorado
| black and white
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | 1980
| Napoléon (Brownlow 1980)
| 4:50 at 20 fps
|  
|  
| National Film Theatre, London
| black and white
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | 1980
| Napoleon (Coppola)
| 4:00 at 24 fps
|  
|  
| USA
| black and white
| 35 mm 70 mm
|-

|- style="background:#fffff6"
| align="center" | 1983
| Napoléon (Brownlow 1983)
| 5:13 at 20 fps
|  
|  
| Cinematheque Francaise
| black and white
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | 1983
| Napoléon (Brownlow 1983 TV cut)
| 4:50 at 20 fps
|  
|  
| Channel 4 (UK television)
| none
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | 1989
| Napoleon (Brownlow 1980)
| 4:50 at 20 fps
|  
|  
| Cité de la Musique, Paris
| none
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | 1989
| Napoléon (Brownlow 1989 TV cut)
| 4:50 at 20 fps
|  
|  
| Channel 4 (UK television)
| toned, letterboxed inside 4:3
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | 2000
| Napoléon (Brownlow 2000)
| 5:30 at 20 fps
|  
|  
| Royal Festival Hall
| toned
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | 2004
| Napoléon (Brownlow 2004)
| 5:32 at 20 fps
|  
|  
| Royal Festival Hall
| toned
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | 2012
| Napoléon (Brownlow 2004)
| 5:32 at 20 fps
|  
|  
| Paramount Theatre (Oakland, California)
| toned
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | 2013
| Napoléon (Brownlow 2004)
| 5:32 at 20 fps
|  
|  
| Royal Festival Hall, London
| toned
| 35 mm
|-

|- style="background:#fffff6"
| align="center" | 2014
| Napoléon (Brownlow 2004)
| 5:32 at 20 fps
|  
|  
| Ziggo Dome, Amsterdam
| toned
| 35 mm
|-

|}

==Restorations==
  (left) and composer Arthur Honegger in 1923]] Silver Medallion.   

  in 1981 to see Francis Ford Coppolas version of Napoléon]]
Brownlows 1980 reconstruction was re-edited and released in the United States by American Zoetrope (through Universal Pictures) with a score by Carmine Coppola performed live at the screenings. The restoration premiered in the United States at Radio City Music Hall in New York City on 23–25 January 1981; each performance showed to a standing room only house. Gance could not attend because of poor health. At the end of the 24 January screening, a telephone was brought onstage and the audience was told that Gance was listening on the other end and wished to know what they had thought of his film. The audience erupted in an ovation of applause and cheers that lasted several minutes. The acclaim surrounding the films revival brought Gance much-belated recognition as a master director before his death only 11 months later, in November 1981. 

Another restoration was made by Brownlow in 1983. When it was screened at the Barbican Centre in London, French actress Annabella (actress)|Annabella, who plays the fictional character Violine in the film (personifying France in her plight, beset by enemies from within and without), was in attendance. She was introduced to the audience prior to screenings and during one of the intervals sat alongside Kevin Brownlow, signing copies of the latters book about the history and restoration of the film.
 
Brownlow re-edited the film again in 2000, including previously missing footage rediscovered by the Cinémathèque Française in Paris. Altogether, 35 minutes of reclaimed film had been added, making the total film length of the 2000 restoration five and a half hours.

The film is properly screened in full restoration very rarely due to the expense of the orchestra and the difficult requirement of three synchronised projectors and three screens for the Polyvision section. One such screening was at the Royal Festival Hall in London in December 2004, including a live orchestral score of classical music extracts arranged and conducted by Carl Davis. The screening itself was the subject of hotly contested legal threats from Francis Ford Coppola via Universal Studios to the British Film Institute over whether the latter had the right to screen the film without the Coppola score. An understanding was reached and the film was screened for both days.  Coppolas single-screen version of the film was last projected for the public at the Los Angeles County Museum of Art in two showings in celebration of Bastille Day on 13–14 July 2007, using a 70&nbsp;mm print struck by Universal Studios in the early 1980s. 
 Paramount Theatre in Oakland, California, from 24 March to 1 April 2012. These, the first US screenings of his 5.5-hour-long restoration were described as requiring three intermissions, one of which was a dinner break. Score arranger Carl Davis led the 46-piece Oakland East Bay Symphony for the performances.       

At a screening of Napoleon on 30 November 2013, at the Royal Festival Hall in London, full to capacity, the film and orchestra received a standing ovation, without pause, from the front of the stalls to the rear of the balcony. Davis conducted the Philharmonia Orchestra in an outstanding performance that spanned a little over eight hours, including a 100-minute dinner break.  

==Reception==
Napoleon is widely considered to be one of the greatest and most innovative films of the silent era. Review aggregator Rotten Tomatoes reports that 92% of critics have given the film a positive review, based upon 12 reviews, with an average score of 8.7/10, making the film a "Certified Fresh" on the websites rating system.   

The 2012 screening has been acclaimed, with Mick LaSalle of the San Francisco Chronicle calling the film, "A rich feast of images and emotions." He also praised the triptych finale, calling it, "An overwhelming and surprisingly emotional experience." 

==Home media==
The Brownlow-edited film with a score by Davis has never been released for home viewing.   
 DVD Region 2 and Region 4, but no DVD is available in the US.  To suit home viewers watching on a single Standard-definition television|standard-width television screen, the triptych portion is letterboxed, such that image height is reduced to one-third for that portion of the film. 

==See also==
*List of biopics
*Napoleon in popular culture
*List of early colour feature films
*List of longest films by running time

==References==
;Notes
 

;Bibliography
 
* 
* 
* 
 

==External links==
* 
* 
* 
* , SilentEra.com
* , SilentEra.com
*  site based upon Napoleon on screen
* 
* 
* 
* 
* 
* 
* . Details of 2004 projection.
* , Adrian Curry in Notebook

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 