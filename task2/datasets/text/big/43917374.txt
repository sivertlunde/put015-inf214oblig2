Aayudham (2003 film)
{{Infobox film
| name = Ayudham
| image =
| caption =
| director = N. Shankar
| producer = Srinivasa Rao
| writer =
| screenplay = Rajasekhar Sangeetha Sangeetha Brahmanandam Gurleen Chopra
| music = Vandemataram Srinivas
| cinematography =
| editing =
| studio =
| distributor =
| released =  
| country = India Telugu
}} 2003 Cinema Indian Telugu Telugu film, directed by N. Shankar and produced by Srinivasa Rao. The film stars Rajasekhar (actor)|Rajasekhar, Sangeetha (actress)|Sangeetha, Brahmanandam and Gurleen Chopra in lead roles. The film had musical score by Vandemataram Srinivas.  

==Cast== Rajasekhar
* Sangeetha
* Brahmanandam
* Gurleen Chopra
* Rajan P. Dev
* Amanchi Venkata Subrahmanyam|A. V. S
* Jaya Prakash Reddy
* Kalabhavan Mani Venu Madhav

==Soundtrack==
The music was composed by Vandemataram Srinivas. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || "Why Raju" || Udit Narayanan, Usha || Bheems || 04.14
|-
| 2 || "Meghale Evela" || Shankar Mahadevan, Swarnalatha || Warangal Srinivas || 05.00
|-
| 3 || "Bangarubomma Rave" || Vandemataram Srinivas, Usha || Suddala Ashok Teja || 05.03
|-
| 4 || "Abba Yem" || Shankar Mahadevan, Anuradha Sriram || Srivare || 04.34
|-
| 5 || "Ranga Reddy Zilla" || Udit Narayanan, Kalpana || Padma Srinivas || 04.42
|}

==References==
 

==External links==
*  

 
 
 
 
 
 

 