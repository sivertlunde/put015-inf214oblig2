Let It Ride (film)
{{Infobox film
| name           = Let It Ride
| image          = Let It Ride (film).jpg 
| caption        = DVD cover
| image_size     = 250px
| director       = Joe Pytka
| producer       = Richard Stenta
| screenplay     = Nancy Dowd ( )
| based on       =  
| starring       = Richard Dreyfuss David Johansen Teri Garr Jennifer Tilly
| music          = Giorgio Moroder
| cinematography = Curtis Wehr Jim Miller
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       = August 18, 1989
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = $18,000,000
| gross          = $4,973,285
}} gambler who experiences a day in which he wins every bet he places, and focuses on the personality contrasts and the perpetually upbeat, hopeful attitudes of losers.

Let It Ride was primarily filmed at Hialeah Park Race Track, which was closed in 2001 and reopened on November 28, 2009. 

==Plot==
Jay Trotter drives a cab. His friend Looney, also a cab driver, has a secret microphone in his taxi to record his passengers conversations. Looney has a tape of two men talking about a horse race and how one of the horses, due to some unethical practice by its owner, is a sure thing to win big. Jay goes to the track to place a bet—despite the fact that the day before, he told his wife Pam that he would quit betting and be home to "start their marriage over" at noon. In the restroom of the bar next door, he prays to God, "Just one day, thats all Im asking for, one day, Im due." A man exiting the bathrooms says "Ya? Sos Jesus. Let it ride." Jay promptly places a $50 bet. The horse wins in a photo finish and pays $28.40 to win (earning him $710).

Armed with a newfound sense of confidence, Jay approaches the two men from Looneys cab and generously gives them the tape of their conversation. Out of gratitude, they give him a tip for the next race. He places a bet and wins again.

Sensing that this could be his "lucky day," Jay goes on picking winner after winner, letting it ride (betting all of his winnings each time). As he accumulates more money and uses his new friends membership in the tracks exclusive dining room, he starts coming into contact with other gamblers, including the wealthy Mrs. Davis and a sexy vixen named Vicki. He becomes a hero to the ticket seller (Robbie Coltrane) whose window he uses every time, and to the customers of the tracks bar.

However, he has totally neglected his wife Pam. Pam flies into a rage when she confronts her husband at the track. He cannot stop. He takes a survey of the track patrons and, eliminating any selection they give him, bets on the remaining horse—Fleet Dreams, which wins. Jay decides to call it a day and goes home to Pam, buying her a diamond necklace on the way.  At home he finds Pam intoxicated and passed out.

He heads back to the track to help the patrons of Martys bar across the street, but when he suggests sharing his luck by betting their money together, they balk at the idea. Disconcerted, he goes for a walk around the track. Vicki offers to "go to bed with him."  Jay "breaks the fourth wall" by saying to the audience, "Am I having a good day or what?" Ultimately, he turns Vicki down by professing his love for his wife.

Jay makes a final bet of $68,000 (his winnings for the day) after Looney advises him not to bet on Hot to Trot. As the race begins, Looney and Trotter argue over everything, and the main characters all make resolutions. In Vickis case, she vows to give up rich guys and consider a poor one, looking at Looney. The race comes down to a photo finish. While everyone awaits the result, Pam shows up to thank Jay for his lovely gift and to tell him not worry about the money, when the announcer reports the winner: Hot to Trot. The entire racetrack erupts in celebration, which causes Pam to ask, "why is everyone cheering?" Jay replies, "because Im having a very good day."

==Cast==
* Richard Dreyfuss as Jay Trotter
* David Johansen as Looney
* Teri Garr as Pam
* Jennifer Tilly as Vicki
* Allen Garfield as Greenberg
* Robbie Coltrane as Ticket Seller
* Michelle Phillips as Mrs. Davis
* Cynthia Nixon as Evangeline
* Richard Edson as Johnny Casino
* Trevor Denman as Race Track Announcer Edward Walsh as Marty (as Ed Walsh)

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 