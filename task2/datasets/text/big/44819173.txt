A Samurai Chronicle
{{Infobox film
| name           = A Samurai Chronicle
| image          = 
| alt            =
| caption        = 
| director       = Takashi Koizumi
| producer       =
| writer         = Takashi Koizumi Motomu Furuta
| based on       =  
| starring       = Kōji Yakusho Junichi Okada Maki Horikita Mieko Harada
| music          = Takashi Kako
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 129 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          = 
}}
  is a 2014 Japanese film directed by Takashi Koizumi.

==Cast==
*Kōji Yakusho as Shūkoku Toda
*Junichi Okada as Shōzaburō Danno
*Maki Horikita as Kaoru Toda
*Mieko Harada as Orie Toda
*Shinobu Terajima
*Hisashi Igawa
*Kenichi Yajima

==Plot==

A retired samurai must redeem himself for a crime that he committed earlier in his life. As squire is sent by the prime misiter of Japan to keep watch over him.   

==Development==

Teruyo Nogami, who was a longtime assistant of Akira Kurosawa, worked as a special adviser on the film, and jointed the director and star for a question and answer session about the film. 

The film was based on an award winning novel by Rin Hamuro. 

Koizumi claimed that he did not want to send any political messages with the film and instead intended to portray the real life events as accurately as possible. 

==Reception==

The film debuted at number two in the Japanese box office  and grossed a total of $8,804,424 in Japan. 

The Japanese Times awarded the film a score of five out of ten, saying that the film failed to invoke the film of Akira Kurosawa that it was influenced by. [Though Koizumi and his veteran staff try to channel that sensibility, they are not Kurosawa, and though “A Samurai Chronicle” echoes the master’s work, it lacks his vivifying presence. 

==References==
 

==External lists==
*http://www.thefilmcatalogue.com/catalog/FilmDetail.php?id=17808

 
 
 
 
 