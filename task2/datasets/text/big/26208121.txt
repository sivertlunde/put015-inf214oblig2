The Good Guy (film)
{{Infobox film
| name = The Good Guy
| image = Good guy.jpg
| caption = Theatrical release poster
| director = Julio DePietro
| producer = 
| writer = Julio DePietro
| starring = Alexis Bledel Scott Porter Bryan Greenberg Andrew McCarthy Aaron Yoo
| music = Kurt Oldman
| cinematography = 
| editing = 
| distributor = Belladonna Productions Movie Studio 
| released =  
| runtime = 91 minutes
| country = United States
| language = English
| followed by = 
| budget =$3,200,000  gross = $100,688
}}
The Good Guy is a 2009 romantic comedy film directed by Julio DePietro starring Alexis Bledel, Scott Porter, and Bryan Greenberg.

==Synopsis==
The narrator, Tommy (Scott Porter), is young, clever, charming, and attractive. Hes got a smart, pretty girlfriend, Beth (Alexis Bledel), a young Manhattanite and urban conservationist. Hes also very good at his investment broker job, impressing his ruthless, cynical boss, Cash (Andrew McCarthy). When a key member of Tommys sales team suddenly leaves for a competitor, Tommy needs to fill his spot quickly, and takes a chance on the bumbling Daniel (Bryan Greenberg), a former avionics engineer and computer geek who seems a bit naïve about high finance and a bit nervous around women. Tommy takes Daniel under his wing, showing him how to dress, where to socialize, and how to charm attractive women. Their relationship is threatened when Daniel begins spending more time with Beth, joining her book club, and becoming her confidant. Tommy begins to question his decision to share his wisdom with Daniel, while Daniel is forced to decide what success really means to him, and where his loyalties lie in life.

==Cast==
*Alexis Bledel as Beth Vest 
*Scott Porter as Tommy Fielding 
*Bryan Greenberg as Daniel Seaver 
*Anna Chlumsky as Lisa
*Andrew McCarthy as Cash
*Aaron Yoo as Steve-O
*Andrew Stewart-Jones as Shakespeare
*Denise Vasi as Suki
*Jessalyn Wanlim as Jordan
*Eric Thal as Stephens
*Kate Nauta as Cynthia
*Colin Egglesfield as Baker
*Adam LeFevre as Billy
*Jackie Stewart as Sofia
*Monica Steuer as Florist
*Darrin Baker as Bobby

==Soundtracks==
The Helio Sequence - Lately

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 