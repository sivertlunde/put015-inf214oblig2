Obaltan
{{Infobox film
| name            = Obaltan
| image           = Obaltan 1961 Poster.jpg
| caption         = Theatrical poster to Obaltan (1961)
| director        = Yu Hyun-mok
| producer        = Kim Seong-chun
| writer          = Lee Beom-seon  Kim Jin-kyu Moon Jeong-suk
| music           =
| cinematography  = Kim Hak-seong 
| editing         = Kim Hui-su
| distributor     = Cinema Epoch
| released        =  
| runtime         = 110 minutes
| country         = South Korea
| language        = Korean
| budget          = 
| film name = {{Film name
 | hangul          =  
 | hanja           =  
 | rr              = Obaltan
 | mr              = Obalt‘an }}
}}
Obaltan ( ), also known as The Aimless Bullet and Stray Bullet, is a 1960 South Korean film directed by Yu Hyun-mok. The plot is based on the same titled short novel written by Yi Beomseon. It has often been called the best Korean movie ever made.  

==Plot==
The films depicts a man, who lives a hard life together with his brother, his wife and his mentally ill mother. The film shows their poor lives Realism (arts)|realistically. The last, climactic part of the film portrays the impulsive crime that results from these circumstances.
==Cast== Kim Jin-kyu as Cheolho
*Choi Moo-ryong as Yeongho
*Seo Ae-ja as Myeongsuk
*Kim Hye-jeong
*Noh Jae-sin
*Moon Jung-suk
*Yoon Il-bong
*Yu Gye-seon
*Nam Chun-yeok
*Park Gyeong-hui

==Reception==
The government banned Obaltan because of its unremittingly downbeat depiction of life in post-armistice South Korea. An American consultant to the Korean National Film Production Center saw the film and persuaded the government to release it in Seoul so that it might qualify for entry in the San Francisco International Film Festival. Director Yu Hyun-mok attended the films premier in San Francisco in November 1963. Variety (magazine)|Variety called Obaltan a "remarkable film", and noted that its " rilliantly detailed camera work is matched by probing sympathy and rich characterizations." 

==Legacy==
The most-cited quote from the film, mentioned in the contemporary Variety review and in later texts on South Korean cinema, is "Lets get outta here! Lets get outta here!" 

==Availability== Region 0 DVD in South Korea with English subtitles,  but as of November 2007 is currently out of print.  Gregory Hatanakas Cinema Epoch released the film on Region 1 DVD on March 13, 2008. 

==Notes==
 

== Sources ==
*  
*  
*  
* 
*  
==Further reading==
* 

==External links==
*  
*  

 
 
 
 
 
 