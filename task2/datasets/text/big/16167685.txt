Banda Paramasivam
 
{{Infobox film
| name = Bandha Paramasivam
| image = Banda Paramasivam DVD Cover.jpg
| image size = 250px
| caption = DVD Cover
| director = T. P. Gajendran
| writer = Prabhu Abbas Abbas Rambha Rambha Kalabhavan Livingston P. Monica Manivannan Manobala
| producer =
| music = Sirpy
| editor =
| released =   
| runtime =
| country = India
| language = Tamil
}}

Banda Paramasivam is a Tamil film released in 2003.The movie is a remake of the 1998 Malayalam Movie Mattupetti Machan starring Mukesh, Baiju, Jagathy Sreekumar and Oduvil Unnikrishnan. It was remade in Telugu as Hungama (2005 film)|Hungama in 2005 and in Hindi as Housefull 2 in 2012.

==Plot==
Cheran (Manivannan) and Pandiyan( Vinu Chakravarthy) are stepbrothers who are constantly at loggerheads and their daughters Manju(Rambha)and Anju(Abinayasri) continue their enmity. Theyre always trying to prove to each other that one is better than the other and are
always bickering. When Pandiyan insults his daughters suitor(Livingston) who comes to see her in hopes of finding a future bride. Pandiyan insults him for not having as much money as he did and questioned his mother if she had other husbands. Hearing this Livingstons father gets a heart attack. In anger Livingstone, wanting to make Pandiyan lose his properties and come to the streets, arranges to have a petty thief  Paramu (Prabhu) pose as a rich man and wriggle his way in as Pandiyans son-in-law. Paramu and another thief Sivam are always competing against each other to be better than the other and to become a millionaire before the other. This leads to a lot of comic rivalries. But Paramu mistakenly enters Cherans house because the gate of Pandiyans house had  been recently been painted over and Manju falls for him. Meanwhile, Sivam (Kalabhavan Mani),Paramus friend, enters Pandiyans house posing as a cinema director. Madhavan (Abbas) helps
them both since he is in love with a poor girl and is pretty sure that his father(P.Vasu) will oppose his wedding. Paramu and Sivam make a deal with Abbas that if he helps them become
rich then Paramu would adopt the poor girl that Abbas loves so  they would be equal in prestige and stature. This way Abbass rich father would agree to their marriage. Paramu and Sivam pose
as the sons of Abbass dad. Things gone worse when Paramus Father in law (cheran) takes them to Madhavans house. The film revolves around how Paramu, Sivam & Madavan deals the situation & How everything comes to be known to both families and Abbass family forms the crux of the story.

==Cast== Prabhu as Paramu
*Kalabhavan Mani as Sivam Abbas as Madhavan Rambha as Manju
*Abhinayasree as Anju Monica as Shenbagam
*Manivannan as Cheran
*Vinu Chakravarthy as Pandiyan
*P. Vasu Livingston
*T. P. Gajendran
*Charle
*Manobala

==References==
 

 
 
 
 
 