Slow Dancing in the Big City
{{Infobox film
| name           = Slow Dancing in the Big City
| image          = 
| image_size     =
| caption        =Original theatrical poster
| director       = John G. Avildsen
| writer         = Barra Grant
| producer       = Lloyd Kaufman
| narrator       =
| starring       = Paul Sorvino Anne Ditchburn Nicolas Coster Anita Dangler Thaao Penghlis
| music          = Bill Conti
| cinematography = Ralph D. Bode
| editing        = John G. Avildsen
| studio         = CIP
| distributor    = United Artists
| released       =  
| runtime        = 110 mins.
| country        = United States
| language       = English
| budget         = $7 million
| gross          = $1,576,500 
| website        =
| amg_id         =
}}
Slow Dancing in the Big City is a 1978 film directed by John G. Avildsen. It stars Paul Sorvino and Anne Ditchburn. This was the first film made by Avildsen after 1976s Rocky captured Academy Awards for Best Picture and Best Director. It has never been released on video or DVD. 

==Plot==
Lou Friedlander(Paul Sorvino) is a popular columnist for the New York Daily News, writing about the little people of bustling New York City while befriending a street boy named Marty (Adam Gifford). His life changes dramatically upon falling in love with neighbor Sarah Gantz (Anne Ditchburn), a young ballerina who had just discovered she is stricken with a debilitating condition that will eventually force her to quit dancing.

==Cast==
* Paul Sorvino as Lou Friedlander
* Anne Ditchburn as Sarah Gantz
* Nicolas Coster as David Fillmore
* Anita Dangler as Franny
* Thaao Penghlis as Christopher
* Linda Selman as Barbara Bass
* Hector Mercado as Roger Lucas
* Dick Carballo as George Washington Monroe
* Jack Ramage as Doctor Foster
* Adam Gifford as Marty Olivera
* Brenda Starr as Punk
* Daniel Faraldo as T.C. Olivera 
* Michael Gorrin as Lester Edelman 
* Tara Mitton as Diana 
* Matt Russo as Jeck Guffy 
* Bill Conti as Rehearsal Pianist 
* Richard Jamieson as Joe Christy 
* Susan Doukas as Nurse 
* Ben Slack as Mort Hoffman 
* Danielle Brisebois as Ribi Ciano 
* Mimi Cecchini as Rose Ciano
* Lloyd Kaufman as Usher
* Barra Grant as Mildred

==Production==
Several cameos are made by the filmmakers: including John Avildsen|Avildsen, writer Barra Grant, producer Lloyd Kaufman, composer Bill Conti, as well as Avildsens sons Anthony and Rufus. 
 Cry Uncle. Metropolitan Opera Broadway productions. The male dancer was initially auditioned due to his affiliation with another United Artists picture Hair (musical)|Hair, which was filming at the same time as Slow Dancing as well as a Broadway production of Box (play)|Box. He later went on to say of his juggling of performances that "luckily the shooting of Hair was all before and after Slow Dancing, and the producers of Box allowed me to fit both of them in my schedule." 

In preparation for the film, Ditchburn appealed to a hairdresser to "do something with my hair". The various headbands and scarves the actress wears on her head through most of the film was used in order to hide the terrible results. She later commented that the move was a "big advantage" and that it "absorbs the perspiration". 

The film was shot over a course of eight weeks on location in New York City alongside the movies The Wiz (film)|The Wiz, Matilda (1978 film)|Matilda, Eyes of Laura Mars, and Hair, as well as the television productions of The Dain Curse, To Kill a Cop, Daddy I Dont Like It That Way, and Monkeys Uncle." 

The films many dance sequences were primarily set up by choreographer Robert North, though Ditchburn choreographed her own routine for a sequence in which she danced on a rooftop. 

Following poor reviews the film was re-editited and an additional ten minutes of cut footage was restored in order to further develop Sorvinos character.  The actor would later go on to say that he believed "the film is much improved" due to the additional scenes 

In its foreign release, the film was retitled as A Womans Quest (Denmark), A Big City With Heart (Finland), Small Steps to Big City (Greece), Married in New York (Portugal), and With You in a Big City (Austria and Germany). 

==Release==
The film was released to theatres November 8, 1978, to an opening weekend gross of $11,355.  It was then followed by a wide release on February 16, 1979.  Norman Dresser of the Toldeo Blade commented that the film "moved at the box office about as slowly as mollasses pours out of a jar after a week in the refrigerator", noting its nine week take of $335,436 was "dismal".   The movie was unsuccessful, earning $1,576,500 at the end of its theatrical run against a budget of $7,000,000. 

===Reaction=== Sneak Preview.  In a particularly scathing review, Dan Dinicola of The Gazette (Cedar Rapids)|The Gazette commented that "it seems the makers of this tinsel romance were so concerned with following patented formulas that they forgot to give it a heart", adding that Grants screenplay was "horrible; not so much the story idea, but the dialogue, the resolution, the subplots" and condemned Avildsens direction as "soppy and as sloppy as the story". 

In response to the mixed reception, star Anne Ditchburn stated that she was "not surprised by the adverse reviews. Not many critics are romantically minded and I must say that I am not romantic in judging my peers in the dance world. There is a razor-fine edge between romanticism and corn, and I think Slow Dancing worked against the corn." 

Ditchburn was nominated for a Golden Globe award for "Best Female Newcomer" for her performance,.  On her performance Charles Champlin of the Sarasota Herald-Tribune stated "  is not a professional actress, which may have been the luckiest thing in the world for her, because her performance has a kind of no-nonsense honesty that becomes a characterization. She is not an actress acting, but not an amateur trying to act either." 

==Merchandise==

===Soundtrack===
 
{{Infobox album  
| Name        = Slow Dancing in the Big City
| Type        = Soundtrack
| Artist      = Bill Conti
| Cover       = 
| Released    = 1978
| Recorded    = 
| Genre       = Film score
| Length      = 30:44
| Label       = United Artists
| Producer    = 
}}The soundtrack by Bill Conti was released on vinyl in 1978.  It was re-released by Varese Sarabande on August 31, 2005, in a limtied edition CD it shared with the Conti score for F.I.S.T.  
{{Track listing
| headline        = Slow Dancing in the Big City
| music_credits   = no
| title1 = Slow Dancing in the Big City
| length1 = 3:04
| title2 = Good Night
| length2 = 1:29
| title3 = You Cant Dance Again
| length3 = 2:24
| title4 = Alone in Lincoln Center
| length4 = 2:27
| title5 = Rooftop Dancing
| length5 = 2:13
| title6 = T.C. Salsa
| length6 = 3:52
| title7 = Balletto
| length7 = 7:04
| title8 = The Ovation
| length8 = 4:32
| title9 = Blue Evening
| length9 = 4:59
}}
Additionally the song "I Feel the Earth Move", written and sung by Carole King, was used in the film though did not appear on the soundtrack. 

===Novel===
A novelization of the film was released by Coronet Books and written by scriptwriter Barra Grant. 

===Home Video===
The film has not been officially released on home video in any format since the end of its theatrical run. According to Avildsen this is due to licensing issues with the music. 

==Legacy==
Slow Dancing in the Big City featured Anne Ditchburns first foray into film, leading her to leave the National Ballet of Canada in pursuit of a film career, in addition to a generally low morale.  The films release also marked a bloom in Sorvinos film career, as it was coupled with his appearances in The Brinks Job and Bloodbrothers (1978 film)|Bloodbrothers that same year. He considered Slow Dancing to be his "best shot" of the three after a string a negative reviews.  In addition, this was one of the first appearances of Golden Globe nominated actressDanielle Brisebois.  The film also featured the debut of director and scriptwriter Barra Grant. 
 The Formula, Neighbors (1981 film)|Neighbors, and A Night in Heaven|A Night in Heavan, though the director himself does not list it among those films as his failures. 

Bill Contis track "The Ovation" on the films soundtrack is frequently used by Conti during his composition of the music for the Academy Awards, used to back the footage that honors actors and actresses who had died during the year prior. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 