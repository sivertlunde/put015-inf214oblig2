Age of Consent (film)
 
 
 
{{Infobox film
| name           = Age of Consent
| image          = Age of Consent English film poster.jpg
| image_size     = 225px
| caption        = 1969–1970 cinema poster
| director       = Michael Powell
| producer       = Michael Powell James Mason
| writer         = Peter Yeldham
| based on       =  
| starring       = James Mason Helen Mirren Jack MacGowran
| music          = Peter Sculthorpe (original) Stanley Myers (UK & US release) Peter Sculthorpe (restored)
| cinematography = Hannes Staudinger
| editing        = Anthony Buckley
| studio         = Nautilus Productions Michael Powell Productions
| distributor    = Columbia Pictures Umbrella Entertainment (Aust, NZ)
| released       =  
| runtime        = 106 minutes (Australia)   Retrieved 13 December 2012   98 minutes (UK & US) 106 minutes (restored)
| country        = Australia
| language       = English
| budget         = AUD 1.2 million Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 242. 
| gross          = AUD 981,000 (Aust) 
}}
 romantic comedy-drama film|comedy-drama stars James Mason (co-producer with Powell), Helen Mirren in her first major film role, and veteran Irish character actor Jack MacGowran. The screenplay by Peter Yeldham was adapted from the 1935 semi-autobiographical novel of the same name by Norman Lindsay, who died the year this film was released.

==Plot==
Bradley Morahan (James Mason) is an Australian artist who feels he has become jaded by success and life in New York City. He decides that he needs to regain the edge he had as a young artist and returns to Australia.

He sets up in a shack on the shore of a small, sparsely inhabited island on the Great Barrier Reef. There he meets young Cora Ryan (Helen Mirren), who has grown up wild, with her only relative, her difficult, gin-guzzling grandmother Ma (Neva Carr Glyn). To earn money, Cora sells Bradley fish that she has caught in the sea. She later sells him a chicken which she has stolen from his spinster neighbour Isabel Marley (Andonia Katsaros). When Bradley is suspected of being the thief, he pays Isabel and gets Cora to promise not to steal any more. To help her save enough money to fulfill her dream of becoming a hairdresser in Brisbane, he pays her to be his model. She reinvigorates him, becoming his artistic muse.

Bradleys work is disrupted when his sponging longtime "friend" Nat Kelly (Jack MacGowran) shows up. Nat is hiding from the police over alimony he owes. When Bradley refuses to give him a loan, Nat invites himself to stay with Bradley. After several days, Bradleys patience becomes exhausted. Luckily, the problem is solved for him. Nat romances Isabel, hoping to get some money from her. Instead, she unexpectedly ravishes him. The next day, he hastily departs the island, but not before stealing Bradleys money and some of his drawings.

Then Ma catches Cora posing nude for Bradley and accuses him of carrying on with her underage granddaughter. Bradley protests that he has done nothing improper; finally, he gives her the little money he has left to get her to go away.

When Cora discovers that Ma has found her hidden cache of money, she chases after her. In the ensuing struggle, Ma falls down a hill, breaks her neck, and dies. The local policeman sees no reason to investigate further, since the old woman was known to be frequently drunk.

Later that night, Cora goes to Bradleys shack, but is disappointed when he seems to view her only as his model. When she runs out, Bradley follows her into the water. There, she finally gets him to see her as a desirable young woman.

==Cast==
 
* James Mason as Bradley Morahan
* Helen Mirren as Cora Ryan
* Jack MacGowran as Nat Kelly
* Neva Carr Glyn as Ma Ryan
* Andonia Katsaros as Isabel Marley
* Michael Boddy as Hendricks Harold Hopkins as Ted Farrell
* Slim DeGrey as Cooley
* Max Meldrum as TV Interviewer
* Frank Thring as Godfrey, the Art Dealer
* Clarissa Kaye as Meg
* Judith McGrath as Grace
 
;Cast notes
* Helen Mirren, who was a member of the Royal Shakespeare Company, and had played supporting roles in three films, was 22 at the time the filming of Age of Consent began. 
* James Mason met his future wife Clarissa Kaye on this film; she played the part of Meg, Bradleys ex-girlfriend in Australia. Their scene together was filmed in bed, and Kaye, who was recovering from pneumonia, had a temperature of  . After the filming, Mason began corresponding with Kaye, and the two were married in 1971, and remained so until Masons death in 1984. She was sometimes referred to as Clarissa Kaye-Mason.  

==Production==
Norman Lindsays novel had been published in 1938 and was banned in Australia. A film version was announced in 1961 by producer Oscar Nichols who said he wanted   to write the adaptation. 

Several changes were made from Lindsays novel including shifting the location from New South Wales to the Barrier Reef and making the artist a success instead of a failure. The bulk of the budget was provided by Columbia Pictures in London. 

Before filming began on Age of Consent, director  , but had not been able to come to an agreement on billing and Mason was unwilling to go on location to Scotland. After Age of Consent, Powell tried to recruit Mason for his version of Shakespeares The Tempest, a project which never came to fruition. 

It was originally intended to cast an unknown 17 year old Australian actress opposite Mason but in the end a young Helen Mirren was chosen. 

Filming began in March 1968 in Albion Park racecourse and elsewhere in Brisbane, with location filming on Dunk Island  and Purtaboi Island on the Great Barrier Reef off the coast of Queensland, and interiors shot at Ajax Film Centre in the Eastern Suburbs of Sydney.  TCM     

===Underwater photography=== The Blue Lagoon.)

==Censorship==
Although Age of Consent was released without cuts in Australia,   Retrieved 13 December 2012  and also passed the British Board of Film Classification without any demands for cuts,  the distributor, Columbia Pictures, decided to cut the opening bedroom scene between James Mason and Clarissa Kaye, and also some of Mirrens nude scenes, thus shortening the film from 106 minutes to 98 minutes before it was released to the UK and US audiences. The Columbia executives also didnt like Peter Sculthorpes original score, so it was replaced with one by Stanley Myers.  The original Sculthorpe score was restored when the film was restored in 2005.

==Reception==
===Box office===
Age of Consent was a huge success in Australia, where it received generally favourable reviews,  and drew sizeable audiences; it ran continuously for seven months at Sydneys Rapallo.  The film took AUD 981,000 at the box office in Australia,  which was equivalent to AUD 9,711,900 in 2009. It was the 13th most popular film in Australia in 1969. 

===Critical===
However, outside Australia critics were not very positive, with   writing: The film has plenty of corn, is sometimes too slow, repetitious and badly edited...Yet   has immense charm, and the photography and superb scenery make it a good travelog ad for the Great Barrier Reef.  

Michael Powell himself thought the film had turned out to be too comedic: "A sensual comedy. Not a big success, but interesting anyway." 

==Restored version==
At the 2005  .  The restored version is not yet available on DVD in Europe, but has been shown on TV. It premiered on Film Four in the UK in December 2012. 

Umbrella Entertainment released a DVD of the restored version in Australia in July 2012 with special features including Martin Scorsese on Age of Consent, audio commentary with historian Kent Jones, the making of Age of Consent, Helen Mirren: A Conversation with Cora, and Down Under with Ron and Valerie Taylor.   

== See also ==
* Cinema of Australia

==References==
 

==External links==
*   in the Australian National Film and Sound Archive
*  
*  
*  
*  
*  
*  
*   at Oz Movies
*   at  
*   at  

 

 
 
 
 
 
 
 
 
 
 