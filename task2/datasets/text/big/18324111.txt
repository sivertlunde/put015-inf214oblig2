Crooked Business
 
 
{{Infobox film
| name           = Crooked Business
| image          = Crooked_Business_poster.jpg
| caption        = Theatrical release poster
| director       = Chris Nyst
| producer       = Scott Corfield
| writer         = Christ Nyst
| released       =  
| runtime        = 95 minutes
| country        = Australia
| language       = English
| budget         = $1 million
}} Gold Coast. It opened in Australia on 15 October 2008.

==Plot==
The movie stars small time hustler Elmo, and his best friend Stand-Up Stevie, a used car dealer with questionable ethics. Stevie finds himself in trouble when he sells a counterfeit gold watch to biker Russian Tony. To buy his way out, Stevie agrees to do a job for local hood Bondi Bob McLean. All he has to do is fly to Melbourne to pick up some stolen jewels in a briefcase. 
 trade off with Cho.

==Cast==
*Teo Gebert - Elmo Anthony Wong - Peter Cho
* Firass Dirani - Stevie
* Kelly Atkinson - Cher
* Brad McMurray - Big Elvis
* Christopher Naismith - Pierre
* Chris Betts - Bondi Bob
* Bertrand Doeuk - Wang
* Anh Do - Benny Wing
* Jay Lagaaia - Pickaxe
* Hugh Parker - London Mick
* Scott Corfield - Maurice

==Production==
The movie was produced on a $1 million AU budget and shot entirely on location around Australias Gold Coast, over five weeks of shooting.  Many of the films extras were friends and clients of Nysts law firm, working as volunteers. Producer Scott Corfield said the lack of funds required calling in favours "from all over town". 

==Box Office==
Crooked Business grossed $27,804 at the box office in Australia. 

==External links==
*  
*  

==References==
 

 
 

 