Robby (film)
{{Infobox film
| name = Robby
| image = Robby poster.jpg
| caption = DVD cover
| director = Ralph C. Bluemke
| producer = Stacey Enyeart 
| writer = Ralph C. Bluemke
| narrator = 
| starring = Warren Raum Ryp Siani John Garces
| music = John Randolph Eaton Christopher Young
| cinematography = Al Mozell
| editing = Bill Buckley
| distributor = Award Films International Bluewood Films
| released =  
| runtime = 60 min
| country = United States
| language = English
}}
Robby is a 1968 family film written and directed by Ralph C. Bluemke. It is a modern-day retelling of the Daniel Defoe novel Robinson Crusoe in which the main characters are portrayed as children. The film deals with many themes, including friendship, homesickness, racial blindness and naturism. It is unusual for a family film in that it contains extensive, explicit nudity involving young boys.

==Plot==
The film opens with a lifeboat washing up onto shore of a tropical island. Inside the lifeboat is nine-year-old Robby. Robby explored the island but finds no signs of any other humans. He falls into a lagoon and almost drowns, but is rescued by a naked native boy, whom Robby befriends and names Friday.  Robby questions Friday about his naked state and lack of shame, but it soon becomes quite clear that Friday does not understand English.

It is not long before Robby himself abandons his own clothes and the two friends scamper around the island naked and free without adult supervision. Robby teaches Friday how to speak English, and in turn Friday teaches Robby how to swim. Together, the two boys survive on the island, with only each other to rely on. They build a shelter and fish for sustenance. They are faced with threats of poisonous snakes and cannibals, of which they find evidence in the form of human remains, as well as a stranger who mysteriously appears on their island one night.

==Cast==
*Warren Raum as Robby
* Ryp Siani as Friday
* John Garces as Harton Crandall
* Rita Elliot as Janet Woodruff
* Ralph Bluemke as Chauffeur

==Production==

Writer/director Ralph C. Bluemke originally conceived the idea of retelling the classic Robinson Crusoe story with children as the principals while working at a bank in 1960, seven years before filming would even begin.

In choosing his principal actors, Bluemke first cast ten-year-old Ryp Siani in the role of Friday. Ryp was already a seasoned child actor by this time, having been brought up in a showbiz family, and appearing in television commercials since early childhood. Casting the role of Robby was a bit more challenging. After looking at dozens of child actors, Bluemke finally settled on nine-year-old Warren Raum. For the role, Warrens hair was bleached platinum blond in order to symbolize the innocence of youth, and also to further contrast Ryps dark hair and complexion.
 Lord of the Flies was shot five years earlier. Incidentally, both films are about children marooned on a desert island. 

===Nudity===
 
Given the nature and location of the script, writer/director Ralph C. Bluemke knew from the beginning that the film would require a certain amount of nudity in order to give it a sense of realism and authenticity. Warren Raum and Ryp Siani, who played Robby and Friday, respectively, were both Bluemkes first choices for the leading roles.  Fortunately for him, both of the child actors and their parents consented to the extensive nude scenes featured in the script. On the first day of filming, both boys disrobed without the least bit of hesitation, and were delighted to be allowed to run and play naked on the beach and in the surf, uninhibited by clothes, especially in the hot Puerto Rican sun. 

When cinematographer Al Mozell asked Bluemke how far he wanted to go in showing the actors naked bodies, Bluemke told him to simply film the boys as if they were fully clothed. Mozell reluctantly agreed, saying, "Okay, but nudity is a no-no." 
 Lord of the Flies as an example, a film which also depicted naked boys in a similar setting, some of whom were even older than Raum and Siani.  However, Lord of the Flies depicted primarily incidental rear nudity, while Robby included lengthy shots of full frontal nudity of both boys. As a result, the film failed to attain a wide distribution deal, as prospective distributors were wary about the extensive nude scenes. 

When it came time to remove his pants and underwear, it was soon revealed that Warren had a sharp tan line in the shape of his shorts, his buttocks and groin pale white in strong contrast to his otherwise bronzed complexion. Ryps tan line, while present, was less noticeable due to his naturally darker complexion. Consequently, on the first day of shooting their nude scenes, both Warren and Ryp suffered severe sunburn to the groin and buttocks, as these sensitive areas were not accustomed to long term exposure to the sun. As a result, filming had to be postponed briefly while the actors recovered.  By the time filming ended, however, both boys had developed dark, full body tans. 

Before filming began, none of the cast and crew were nudists. However, since most of their scenes were filmed in the nude, Warren Raum and Ryp Siani quickly took to the habit of not getting dressed unless it was required for a particular scene. As a result, the rest of the cast and crew soon became accustomed to seeing the two boys wandering around the set stark naked, even when not filming. Much of the crew eventually joined in, occasionally skinny dipping with the actors during lunch breaks and downtime. 

==Reception==
===Release=== Broadway in New York City in August 1968. 

The film was released on VHS in 1983 by Award Films International. A Region Free DVD was also subsequently released, featuring bonus material including the films original trailer, a behind-the-scenes photo gallery, press quotes, illustrated Robby book and making-of text by writer/director Ralph C. Bluemke.  Both the VHS and DVD release versions feature a new score credited to composer Christopher Young, replacing the films original score by John Randolph Eaton.

===Critical response===
Upon its initial release, Robby received widely positive reviews from critics. Bob Salmaggi, of WINS (AM)|WINS, New York, called the film "Enchanting!...a heartwarming masterpiece." Howard Thompson of The New York Times called it "a genteel little drama, purposeful in content, perceptive in tone." Com-Collegiate News called Robby "Among the best movies of the year!...a thoughtful, artistic parable that is both heartwarming and timeless."

In addition, both actors Raum and Siani received praise for their performances in the film. Variety Magazine said "Warren Raum as Robby and Ryp Siani as Friday could put precocious Hollywood child actors to shame!" Com-Collegiate News said "Robby is played very well by young Warren Raum, and Ryp Siani displays some of the best acting a child has ever done on the screen!" Bob Salmaggi called Warren Raum "a little charmer as Robby, and Ryp Siani...is absolutely perfect as Friday." 

==Book adaptation==
After the films release, Robby was adapted into a childrens book by writer/director Ralph C. Bluemke, illustrated with screenshots from the film. The entire text of the book, along with the illustrations, is included as a bonus feature on the DVD release from Award Films International. 

==References==
 

==External links==
* 

 
 
 
 
 