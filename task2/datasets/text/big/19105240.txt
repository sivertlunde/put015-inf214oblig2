The Archangel
{{Infobox film
| name           = The Archangel
| image          = The Archangel.jpg
| caption        = Film poster
| director       = Giorgio Capitani
| producer       = 
| writer         = Adriano Baracco Giorgio Capitani Renato Castellani
| starring       = Vittorio Gassman
| music          = Piero Umiliani
| cinematography = Stelvio Massi
| editing        = Sergio Montanari
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}
The Archangel ( ) is a 1969 Italian comedy film directed by Giorgio Capitani and starring Vittorio Gassman.

==Cast==
* Vittorio Gassman - Fulvio Bertuccia
* Pamela Tiffin - Gloria Bianchi
* Irina Demick - Sig.ra Tarocchi Roda
* Adolfo Celi - Marco Tarocchi Roda
* Laura Antonelli
* Carlo Baccarini
* Carlo Delle Piane
* Mario De Rosa
* Gioia Desideri
* Tom Felleghy - Fabris
* Antonio Guidi
* Corrado Olmi Carlo Pisacane
* Gianni Pulone
* Jacob Stanislave
* Pippo Starnazza
* Juan Valejo

==External links==
* 

 
 
 
 
 
 
 
 
 