The Beggar (film)
{{Infobox film
| name           = The Beggar
| image          = Beggar 1983.jpg
| caption        = 
| director       = Ahmed Al-Sabaawi Armes, Roy.  , p. 30 (2008)(ISBN 978-0253351166) 
| producer       =
| writer         = Samir Abdelazim
| starring       = Adel Emam 
| music          = 
| cinematography = 
| distributor    = 
| released       = 1983
| runtime        = 120 minutes
| country        = Egypt
| language       = Arabic
| gross          = 
}}

The Beggar (Al-Motasawel) (Arabic: المتسول) is a 1983 Egyptian comedy film starring Adel Emam.

==Plot==
Imam plays Hasanin, an uneducated man who leaves his small village to live with his uncles family in the city.  After having no luck keeping a job, he must return to his village.  On the way, after getting kicked out of a mosque he tries to sleep at, he ends up in a homeless shelter that he discovers is actually run by a gang forcing people to beg in the streets after maiming them.  Hasanin is set out to pose as if he is a blind beggar. Behbehani, Ali I.  , Arab Times, Retrieved January 24, 2011 

==Reception==
The movies depiction of beggars spurred a lawsuit by peasants against Imam, in which Imam prevailed. Reid, Robert (6 September 1984).  , Leader-Post (Associated Press) 

==Primary cast includes==
*Adel Emam 
*Isaad Younis

==References==
 

==External links==
*  

 
 
 
 
 

 