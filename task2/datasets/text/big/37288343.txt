The Moment Before
{{infobox_film
| name           = The Moment Before
| image          =
| imagesize      =
| caption        =
| director       = Robert G. Vignola Hugh Ford (scenario)
| based on       =  
| cinematography = Edward Gheller
| studio         = Famous Players Film Company
| distributor    = Paramount Pictures
| released       = April 27, 1916
| runtime        = 50 mins.
| country        = United States
| language       = Silent English intertitles
}}
 1916 American silent drama film starring Pauline Frederick. It was produced by Famous Players Film Company and directed by Robert G. Vignola. The film is based on the play The Moment of Death, by Israel Zangwill.   

A 35mm nitrate copy of the film is housed at the Cineteca Nazionale film archive in Rome.  The print is missing one sequence described as "the opening scene before the flashback." 

==Cast==
* Pauline Frederick - Madge
* Thomas Holding - Harold
* Frank Losee - Duke of Maldon
* J. W. Johnston - John, the Gypsy  Edward Sturgis - Ojoe 
* Henry Hallam - The Bishop

==References==
 

==See also==
* List of rediscovered films

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 

 