Cannonball (film)
{{Infobox film
| name           = Cannonball
| image          = CannonballPoster.jpg
| image_size     =
| caption        = Film poster for Cannonball
| director       = Paul Bartel
| producer       = Samuel W. Gelfman Donald C. Simpson
| narrator       =
| starring       = David Carradine Bill McKinney Veronica Hamel Gerrit Graham Robert Carradine
| music          = David A. Axelrod
| cinematography = Tak Fujimoto
| editing        = Mort Tubor
| distributor    = New World
| released       =  
| runtime        = 93 min.
| country        = United States
| language       = English
| budget         = $780,000 Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 104 
| gross          = $1.5 million (rentals) 
}}
 The Cannonball Run, Cannonball Run II and Speed Zone!.  The film was written and directed by Paul Bartel, who also directed Death Race 2000.
 Erwin G. 55 MPH speed limit.

==Synopsis==
 Los Angeles (Santa Monica Pier) and New York City.  Recently released from jail, where he was serving a sentence for killing a girl while driving drunk, racing driver Coy "Cannonball" Buckman (David Carradine) hopes to win the race and get his career back on track. Racing team Modern Motors have promised a contract to either him or his arch-rival Cade Redman (Bill McKinney) who is also in the race - the contract will go to whichever of them wins. Coy is still on probation and when his parole officer, Linda Maxwell (Veronica Hamel), with whom he is having an elaborate affair, discovers he will be crossing state lines in violation of his parole, she attempts to stop him, only to have him force her to accompany him on the race.  

Redman also has company in the form of country singer Perman Waters (Gerrit Graham) and his manager Sharma Capri (Judy Canova) who have agreed to pay Redmans race expenses in return for his taking them with him to New York in his Dodge Charger. 
 Chevrolet Blazer, German driver Wolfe Messer (James Keach) in a De Tomaso Pantera, preppy African-American Beutell (Stanley Bennett Clay) in a Lincoln Continental; he has been hired by a wealthy elderly couple to transport to New York for them (unaware that he is using it to enter the race) and Buckmans best friend Zippo (Archie Hahn) in a Pontiac Trans Am identical to Coys. Unbeknown to Coy, his brother Bennie (Dick Miller) has bet heavily on the race and plans to use underhand methods to ensure Coy wins. 
 LAX to New Yorks LaGuardia Airport where he waits out the race with his mistress Louisa (Louisa Moritz). Beutells borrowed Lincoln gets progressively more damaged as the race goes on, while Jim and Maryann face engine trouble with a broken fan belt. The rivalry between Coy and the increasingly unstable Redman gets out of control as the two fight and attempt to force each other off the road, with Coy crashing his Trans Am after Redman breaks the headlights. Switching to a 1969 Ford Mustang he borrows from some local hot-rodders, Coy has a last showdown with Redman, who has kicked Perman and Sharma out of his car after arguing with them. A piece of Permans guitar, which Redman smashed in a rage after getting sick of Permans singing and on-the-road radio broadcasts, gets lodged behind the car pedals, causing Redman to lose control and crash over the side of an unfinished bridge. He dies when the car explodes. 

Bennie meanwhile, has sent a gunman to kill the driver of the "other" red Trans Am as it is beating Coy. He is unaware that the driver is Zippo or that Linda is now riding with him, as Coy thought it safer for her to do so since Redman was after him. While with Zippo, she has found out that it was Zippo who was driving the car in which the girl was killed, not Coy. Coy took the blame because he knew the weaker Zippo would never survive in jail. 

Bennies gunman shoots Zippo dead and the Trans Am crashes and explodes. Linda jumps clear, but is badly injured. Jim and Maryann see the wreck and pick up the comatose Linda, taking her to hospital. Behind them, the presence of the wrecked Trans Am on the freeway causes a multiple-car pileup. 

Terry McMillan and Louisa arrive first at the finish line, but Louisa lets slip that the Blazer was flown there and he is disqualified. The girls in the van and Coy are neck-and-neck as they cross into New York City (with Coy driving over the George Washington Bridge and the girls taking the Lincoln Tunnel until Sandy attempts to take a shortcut when the girls get lost and are stuck in traffic and the van crashes. Coy arrives at the finish line and is about to stamp his timecard, making him the official winner, when he is told about Zippo and Lindas accident and realizes Bennie caused it. He tears up his timecard so it cant be stamped and gives the pieces to Bennie, who is taken away by gangster Lester Marks (played by the films director Paul Bartel) to whom he owes all the money he bet on Coy, presumably to be killed. Assured of his racing contract, Coy is taken to the hospital to be reunited with Linda by the team manager. Having decided to finish the race in spite of believing they cannot win having lost so much time, Jim and Maryann are the next to arrive at the finish line. They are surprised and overjoyed to be told they are the winners of the $100,000 first place prize. 

At the hospital, Coy and Linda enjoy their reunion, while Beutell delivers the Lincoln - now completely wrecked - to its horrified owners in front of a hotel in the city.

==Cast==
* David Carradine - Coy "Cannonball" Buckman
* Bill McKinney - Cade Redman, Cannonballs nemesis
* Veronica Hamel - Linda Maxwell, Cannonballs parole officer and love interest
* Gerrit Graham - Perman Waters
* Robert Carradine - Jim Crandell
* Belinda Balaski - Maryann
* Mary Woronov - Sandy Harris (Girl in the Van #1)
* Diane Lee Hart - Wendy (Girl in the Van #2)
* Glynn Rubin - Ginny (Girl in the Van #3)
* James Keach - Wolf Messer, the German professional
* Dick Miller - Bennie Buckman, Cannonballs older brother
* Paul Bartel - Lester Marks
* Stanley Bennett Clay - Beutell Morris
* Judy Canova - Sharma Capri, Permans manager
* Archie Hahn - Zippo, Cannonballs friend
* Carl Gottlieb - Terry McMillan
* David Arkin - Dennis Caldwell
* Louisa Moritz - Louisa
* Patrick Wright - Brad Phillips, organizer of the Trans-American Grand Prix
* Joe Dante - Kid
* Allan Arkush - Panama
* Jonathan Kaplan - All-Night Gas Station Attendany
* Roger Corman - Los Angeles County District Attorney
* Don Simpson - L.A. County Assistant District Attorney
* Martin Scorsese - Mafioso #1 (uncredited)
* Sylvester Stallone - Mafioso #2 (uncredited)

The cameos by Martin Scorsese and Sylvester Stallone are uncredited, while Roger Corman and Don Simpson appear as district attorneys. Directors Joe Dante, Jonathan Kaplan and Allan Arkush have cameos, and former beach movie star and Transformers voice actor Aron Kincaid appears in a small role as one of two cops who pull over the girls in the van.

==References==
 

==External links==
*  
*  
* 
*   at the Internet Movie Cars Database (IMCDb)
*  

 

 
 
 
 
 
 