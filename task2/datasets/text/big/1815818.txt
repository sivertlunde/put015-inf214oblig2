Red Sorghum (film)
 
{{Infobox film
| name           = Red Sorghum
| image          = Red Sorghum movie poster.jpg
| caption        = Chinese movie poster
| writer         =  
| starring       = Gong Li Jiang Wen Teng Rujun
| director       = Zhang Yimou
| producer       = Wu Tianming
| cinematography = Gu Changwei
| music          = Zhao Jiping
| distributor    = United States: New Yorker Films
| studio         = Xian Film Studio
| released       = China: 1987 United States: October 10, 1988
| runtime        = 95 minutes
| language       = Mandarin
| country        = China
| budget         =
}}
 Chinese film sorghum liquor. It is based on the novel Red Sorghum Clan by Nobel laureate Mo Yan.
 Fifth Generation directors.

==Synopsis==
The film takes place in a rural village in Chinas eastern province of Shandong during the Second Sino-Japanese War. It is narrated from the point of view of the protagonist’s grandson, who reminisces about his grandmother, Jiuer (S: 九儿, T: 九兒, P: Jiǔér). She was a poor girl who was sent by her parents into a pre-arranged marriage with an old man, Li Datou, who owns a distillery.

As Jiuers wedding party crosses a field of sorghum, they are attacked by a bandit; however, one of the men hired to carry Jiuers sedan chair manages to fight off the assailant. After Jiuer safely reaches the distillery, her rescuer disappears, only to return on Jiuers trip back to her parents village. He jumps out of the sorghum field, and after chasing down Jiuer, carries her off into the sorghum stalks, where the two engage in sexual intercourse.

At the distillery, it is discovered that Li Datou has died of mysterious causes, leading many of the distillerys workers to suspect murder. Nothing is proven, however, and since Jiuers late husband was without heir, it is she who takes ownership of the distillery, which has recently fallen on hard times. She inspires the workers to take new pride in their wine, and once again meets her rescuer. He arrives, drunk, and tries to claim her, loudly insisting to the group of men accompanying him that he is going to share her bed. When he enters the bedroom, however, she angrily tosses him out. The other men on the scene carry him away, sticking him in a vat of liquor where he remains for the next three days. Meanwhile, a group of bandits kidnap Jiuer, forcing the distillery workers to pay a ransom for her freedom.

Jiuers rescuer finally emerges from his liquor vat and returns to the distillery, where the workers are making the first batch of liquor since Li Datous sudden death. The man takes four vats of the liquor and urinates in them as a prank to take revenge against Jiuer, but, to his surprise, the urine somehow makes the liquor taste better than ever. Its product newly improved, the distillery begins to see financial success.

The War begins and Imperial Japanese Army troops invade the area. The Japanese soldiers then torture and kill Jiuers friend Luohan, a respected distillery worker. Jiuer incites the workers to avenge his death. In the early dawn, they hide themselves in the sorghum field, prepared to ambush the Japanese military vehicles the moment they pass by. While waiting, however, the workers become distracted by hunger. When Jiuer is informed of this by her young child (the narrators father), she brings out some lunch for the workers. Arriving just as the Japanese soldiers do, Jiuer is shot and killed in the chaotic skirmish that ensues, and the explosive traps meant for the Japanese trucks end up killing almost everyone at the scene. Only Jiuers rescuer and the pairs young child manage to survive the encounter.

==Cast==
*Gong Li as "My Grandma"
*Jiang Wen as "My Grandpa"
*Ji Chunhua as Sanbao the bandit chieftain
*Teng Rujun as Uncle Luohan

==Style== The Road narrated by flashback framing device of The Road Home (the viewer never sees the narrator).

The cinematography by cinematographer Gu Changwei makes use of rich, intense colors. Zhang himself was a cinematographer prior to his directorial debut, and worked closely with Gu.

==Reception==
Upon its release, Red Sorghum garnered international acclaim, most notably winning the coveted Golden Bear at the 1988 Berlin International Film Festival.

Roger Ebert said, in his review and synopsis in Chicago Sun-Times, "There is a strength in the simplicity of this story, in the almost fairy-tale quality of its images and the shocking suddenness of its violence, that Hollywood in its sophistication has lost." 

===Awards=== 1988
**Golden Bear Award    1988
**Best Feature 1988
**Best Feature
**Best Cinematography - Gu Changwei
**Best Sound
**Best Music - Zhao Jiping
**Best Director - Zhang Yimou (nominated)
**Best Actor - Jiang Wen (nominated) Yang Gang (nominated) 1988
**Best Picture
**Best Director - Zhang Yimou
**Best Artistic Achievement 1988
**Film Critics Award 1989
**Belgian French Radio Young Jury Award for Best Picture 1989
**Silver Panda 1989
**Top Ten Chinese Language Films 1990
**Annual Award 1990
**Best Feature Film

==Further reading==
*Mo Yan. Red Sorghum: A Novel of China. ISBN 0-14-016854-0.

==References==
 

==External links==
 
* 
* 
* 
*  at the Chinese Movie Database

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 