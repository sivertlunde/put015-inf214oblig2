Valiant Is the Word for Carrie
{{Infobox film
| name           = Valiant Is the Word for Carrie
| image          = Carrietitle.jpg
| image_size     = 250px
| caption        = title card at the beginning of the film
| director       = Wesley Ruggles
| producer       = Wesley Ruggles
| writer         = Claude Binyon     Barry Benefield  
| narrator       = John Howard
| music          = Frederick Hollander
| cinematography = Leo Tover
| editing        = Otho Lovering
| distributor    = Paramount Pictures
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} John Howard. George was nominated for an Academy Award for Best Actress in 1936.  The movie was directed by Wesley Ruggles and  adapted by Claude Binyon from the novel of the same name by Barry Benefield.

==Plot== John Wray), John Howard). Harry Carey) Dudley Digges) that, "valiant is the word for Carrie".

==Cast==

*Gladys George as Carrie Snyder
*Arline Judge as Lady John Howard as Paul Darnley Dudley Digges as Dennis Ringrose Harry Carey as Phil Yonne
*Isabel Jewell as Lilli Eipper, sister of Franz Eipper
*Jackie Moran as Paul Darnley as a child
*Charlene Wyatt as Lady as a child John Wray as George Darnley, Paul Darnleys father
*William Collier, Sr. as Ed Moresby, a town councilman who convinces Carrie to leave town before she is forced out
*Hattie McDaniel as Ellen Belle, the Darnleys servant
*Lew Payton as Lon Olds, a horse-and-buggie driver in Crebillon
*Maude Eburne as Maggie Devlin, works with Paul
*Grady Sutton as Mat Burdon, a man Lady meets and hastily marries
*Janet Young as Mrs. Darnley, Paul Darnleys dying mother
*Adrienne DAmbricourt as Madame Odette Desolles, owner of the original Desolles laundry shop that Carrie buys
*Helen Lowell as Mrs. Wadsworth, a town councilwoman in Crebillon
*Bernard Suss as Franz Eipper
*George "Gabby" Hayes as Bearded Man (as George F. Hayes)
*Irving Bacon as Drug Store Clerk
*Olive Hatch as Girl
*Nick Lukats as Boy
*Don Zelaya as Nick Dorapopolos

==Literary antecedents==

The film was preceded by two literary versions by Barry Benefield - a short story and later a novel based on it.

===Short story===

Benefields original short story, entitled "With Banners Blowing", was published in the Womans Home Companion, and later appeared in two collections under the title "Carrie Snyder".

The original story focused entirely on events in the (fictional) town of Crebillion, Louisiana. Carrie Snyder is 31-year old, lives in a cottage at the edge of town and maintains herself as a prostitute, having a circle of regular customers. She has plenty of free time to cultivate her beloved flower garden, and is content with this life. However, though rather fond of such customers such as US Marshall Phil Yonne, who treat her "like gentlemen", she never felt love for anybody - until the seven-year-old Paul comes in to ask for a drink of water.

Carrie becomes instantly and deeply attached to the clever, sensitive, warm-hearted boy who comes again and again on secret visits, deposits with her his box of "treasures" which his father tried to confiscate and lets her take care of wounded creatures which he found - a tomcat and an owl. The African-American taxi driver Lon is Carrie and Pauls friend and confidant, keeping their secret. (Strangely for modern sensibilities, the word "nigger" is repeatedly used for this highly positive and sympathetic character, clearly without any hint of pejorative intent.)

Deeply jealous of Pauls mother, who can have him every day, Carrie is aware that this friendship would not last, and that the towns established society would cut it off once discovered. And so indeed it does come to pass, and even worse than Carrie feared. Hearing that Paul was severely beaten by his father, and witnessing him being chased and cruelly teased and hazed by a gang of other boys, Carrie realises that for Pauls sake she must leave the town, let her beloved garden deteriorate, and never come back. The original story ends poignantly with Carrie going into a self-imposed exile, with the clear implication that she would never see Paul again.

===Novel===
Barry Benefield later took up the story and made a revised version of it the first chapter of what became the 1936 novel Valiant Is the Word for Carrie.

Bar minor differences, the films plot, as described above, followed the novels plot up to the moment of the attempted jail break. From that point on, however, novel and film drastically diverge. In the original novel, the jailbreak succeeded without a hitch, and Lili and her lover were able to escape to Canada and start a new life there. Carrie returned unscathed to New York, her part in the jail break completely unknown. Later on, Lady divorced the Baltimore millionaire Mat Burdon whom she married to spite Paul; Lady and Paul then married and lived happily ever after; and at the end of the novel Carrie, who managed to pull her laundry business through the slump of 1929, is prepared to play loving foster grandmother to their first child.

However, the germ of the films ending - with the jail break going wrong and Carrie being arrested and facing trial - is present in the novel as a conversation about "what might have been" and "how things might have gone wrong".

==Reception==
Frank S. Nugent of The New York Times called it "more moral and uplifting than Pollyanna" and "irresistibly attractive".  He criticized the running time for being almost two hours long. He concluded that "The misfortune is that "valiant" is only one of the "words for "Carrie"; another would be "disproportionate." The picture takes too long, although doing it well, to introduce a little which is not well done at all." 

==Cultural references==
In 1938, the Three Stooges made a short called Violent Is the Word for Curly, a takeoff on the name of this then-popular film. 

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 