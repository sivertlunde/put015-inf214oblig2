The Voice from the Sky
 
{{Infobox film
| name           = The Voice from the Sky
| image          =
| caption        =
| director       = Ben F. Wilson
| producer       = Ben F. Wilson Robert Dillon
| starring       = Wally Wales Neva Gerber
| music          =
| cinematography = William Nobles
| editing        = Frederick Bain
| studio         = G.Y.B. Productions
| distributor    = Hollywood Pictures Corporation
| released       =  
| runtime        = 10 episodes
| country        = United States
| language       = English
| budget         =
}} science fiction film serial directed by Ben F. Wilson. The film was considered to be a lost film, but was recently rediscovered. This is the first serial film to have full sound. {{Cite book
 | first=Denise | last=Lowe | year=2004
 | title=An Encyclopedic Dictionary of Women in Early American Films, 1895-1930
 | publisher=Haworth Press | isbn=0-7890-1843-8
 | pages=234&ndash;236 }} 

==Cast==
* Wally Wales - Jack Deering, U. S. Secret Service
* Neva Gerber (as Jean Delores) - Jean Lovell Robert Walker - Edgar Ballin
* J. P. Lockney - Geoffrey Mentor
* Al Haskell - Henchman Patch-Eye Cliff Lyons - Henchman Humpy
* John C. McCallum - J. C. Gates
* Merle Farris - Mrs. Deering (Jacks mother)
* The Man from Nowhere (a mysterious black-cloaked figure)

==List of episodes==
# Doomed
# The Cave of Horrors
# The Man from Nowhere
# Danger Ahead
# Desperate Deeds
# Trail of Vengeance
# The Scarlet Scourge
# Trapped by Fate
# The Pit of Peril
# Hearts of Steel

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 