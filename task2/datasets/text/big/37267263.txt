The Bounty (2012 film)
 
 
{{Infobox film
| name           = The Bounty
| image          = The Bounty poster.jpg
| alt            = 
| caption        = 
| film name = {{Film name| traditional    = 懸紅
| simplified     = 悬紅
| pinyin         = Xuán Hóng
| jyutping       = Jyun4 Hung4}}
| director       = Fung Chi Keung
| producer       = Paco Wong
| writer         = Fung Chi Keung
| starring       = Chapman To Fiona Sit Alex Man
| music          = RubberBand Lui Pak Ka
| cinematography = Ko Chiu Lam
| editing        = Matthew Hui
| studio         = Sun Entertainment Culture Hong Kong Film Development Fund Beijing Dadi Century Iner
| distributor    = Sun Entertainment Culture
| released       =  
| runtime        = 102 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = HK$13,020,000
| gross          = HK$2,808,249 
}}
The Bounty is a 2012 action comedy film featuring the directorial debut of Fung Chi Keung   and starring Chapman To, Fiona Sit and Alex Man. In November, the film received the Hong Kong Film Development Funds "Film Development Foundation Film Production Finance Project" to provide HK$2,605,711 of finance.  The is the 13th film financed by The Hong Kong Film Development Fund. 

==Plot==
With an optimistic attitude towards life, Cho Sai Fung (Chapman To) is a neurotic man with extraordinary skills, who relies on bounty hunting to make a living. This time he comes to an island, a small hotel called LAZY Inn, to hunt down a fugitive robber Lee Kin Fai, with a HK$4000,000 reward from the police. Suen Long Ching (Fiona Sit) is the innkeepers (Alex Man) daughter. She has a rich imagination and curiosity, and is a little hyperactive. The nosy father and daughter have been monitoring the weird customer Cho, with the repeated wrong touch and misunderstanding, attracts a variety of funny jokes.

==Cast==
*Chapman To as Cho Sai Fung
*Fiona Sit as Suen Long Ching
*Alex Man as Boss Suen
*Zi Yi as Coconut Man / Lee Kin Fai
*Zhang Jin as Yip On
*Nai Mang @ RubberBand as Brother Cane
*Charmaine Fong as News vendor
*Wong Yik Lam
*Eric Kot as Sergeant Chung Raymond Wong as Mr. PTU
*Wan Chiu as Tony
*Sire Ma as Bride
*Stephanie Cheng
*6 Ho @ RubberBand
*Law Wing-cheung
*Michael Hui as Cho Sai Fungs bounty hunting mentor
*Ngai San Hei as Judy
*Jumbo Tsang as Belle
*Wang Wang Chi as Muscleman
*Lee Kin Hing 	  	 
*Crystal Lau as Twin girl
*Pang Chak Man as Twin girl
*Jackie Leung as Passer-by
*Szeto Lai Mui as Passer-by
*Lam Yiu Kei Passer-by
*Lee Chak Yuk as Passer-by
*Liu Wing Ha as Passer-by
*Chow Yun Cheung as Passer-by
*Ng Kwok Ming as Passer-by
*Koo Ming Kui as Passer-by
*Lam Siu Mei as Passer-by
*Chan Ho Ming as Passer-by
*Lau Ka Yee as Passer-by
*Leung Hing San as Passer-by

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 
 
 
 
 