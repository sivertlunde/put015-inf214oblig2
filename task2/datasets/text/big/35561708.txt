Bomb in the High Street
{{Infobox film
| name = Bomb in the High Street
| image = 
| caption = 
| director = Peter Bezencenet Terry Bishop
| producer = Theodore Zichy
| writer = Ben Simcoe Ronald Howard Terry Palmer Jack Allen
| music = Wilfred Josephs
| cinematography = Gordon Dines
| editing = John Trumper
| distributor = 
| released = 1961
| runtime = 60 mins
| country = United Kingdom
| language = English
| budget =
}} Ronald Howard, Terry Palmer and Suzanna Leigh.  

==Plot==
A gang of villains carrying out a bank robbery create a diversion using a fake bomb scare.

==Cast==   Ronald Howard as Captain Manning  Terry Palmer as Mike 
* Suzanna Leigh as Jackie  Jack Allen as Superintendent Haley 
* Peter Gilmore as Shorty 
* Russell Waters as Trent 
* Maurice Good as Feeney 
* Geoffrey Bayldon as Clay  Jack Lambert as Sergeant 
* Humphrey Lestocq as Reporter 
* A.J. Brown as Nightwatchman 
* Gerald Case as Ventry 
* Margaret Lacey as Woman at barrier 
* Leonard Sachs as Freeling 
* James Villiers as Stevens

==References==
 

==External links==
*  

 
 
 
 
 

 