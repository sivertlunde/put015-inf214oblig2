Cover Me Babe
{{Infobox Film
| name           = Cover Me Babe
| image          = Covermebabe.jpeg
| image_size     = 
| caption        = Theatrical release poster
| director       = Noel Black
| producer       = Lester Linsk George Wells
| narrator       = 
| starring       = Robert Forster Sondra Locke Susanne Benton Sam Waterston Ken Kercheval Sam Waterston Michael Margotta Floyd Mutrux Maggie Thrett Jeff Corey
| music          = Fred Karlin
| cinematography = Michel Hugo
| editing        = Harry W. Gerstad
| distributor    = 20th Century Fox
| released       = 1970
| runtime        = 89 min
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Cover Me Babe is a 1970 drama film about a young filmmaker who will do anything to get a studio contract. The film was directed by Noel Black, and stars Robert Forster and Sondra Locke. 
The title song was written by Fred Karlin and Randy Newman, and performed by BREAD. A second song by Bread (written by Karlin and band members James Griffin and Robb Royer, titled "So You Say") also appeared on the soundtrack.

==Plot==
Tony Hall is a film-school student who does not care to make conventional films. His first avant-garde effort features Melisse, who soon becomes Tonys lover and moves in with him.

Seeking a grant, Tony is steered to Paul, whos a Hollywood agent, but he continues to reject the notion of making movies that conform to the norm. Tony shoots realistic footage of a couple making love in a car, a derelict, a prostitute, even an argument between Melisse and a young student, Jerry, that nearly turns violent. He alienates all eventually, and is alone in the end.

==Cast==
* Robert Forster as Tony
* Sondra Locke as Melisse
* Ken Kercheval as Jerry
* Sam Waterston as Cameraman
* Jeff Corey as Paul
* Susanne Benton as Sybil
* Robert Fields as Will

==References==
 

==External links==
* 

 

 
 
 
 
 


 