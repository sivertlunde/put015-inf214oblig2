Dad Made Dirty Movies
{{Infobox film
| name           = Dad Made Dirty Movies |
| image          = Dad Made Dirty Movies theatrical poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Jordan Todorov |
| producer       = Martichka Bozhilova Thomas Tielsch |
| writer         = Jordan Todorov |
| narrator       = D.T. Andersen |
| starring       = Susan Apostolof Polly Apostolof Steve Apostolof Christopher D. Apostolof Shelley Apostolof Harvey Shane Greg Goodsell Rudolph Grey Ted V. Mikels Nadejda Klein David Ward
| music          = Debbie Wiseman
| cinematography = Boris Missirkov, Georgi Bogdanov |
| editing        = Kevork Aslanyan |
| studio         = AGITPROP
| distributor    = Autlook Filmsales |
| released       =   |
| runtime        = 58 minutes |
| country        = Bulgaria/Germany |
| language       = English |
| budget         = 
| gross          = 
}}
 German documentary film by Jordan Todorov following Stephen C. Apostolofs life and  career. The film premiered at Visions du Réel International Film Festival  , East European Picks at Visions du Réel  in April 2011 and consists of archive footage as well as interviews with Apostolofs family, friends and associates. 

The film also includes never before seen archive footage from making of Orgy of the Dead - Apostolofs most well known film. The title Dad Made Dirty Movies comes from a quote by Polly Apostolof, one of Apostolofs daughters.

==Synopsis==
Dad Made Dirty Movies chronicles Stephen C. Apostolofs rise, from the story of his escape as a refugee from the Soviet occupation of Eastern Europe, to producer and director of sexploitation films in the 1960s and 1970s, his turbulent relationship with infamous Ed Wood and his downfall in the late 1970s with advent of hardcore pornography.

==Production==
The documentary is produced by Bulgarian company AGITPROP in association with Filmtank(Hamburg), ZDF and Arte. Pre-production phase took about five years. Principal photography began in New York City in December 2009, and continued in California, Nevada and Arizona in May, 2010. 
 unreliable narrator based on authentic interviews with Apostolof, drawing heavily on his peculiar manner of speaking full of colloquialisms, puns and self irony.

==Release==
The film showed at the Franco-German television Arte on February 17, 2011, and had its official premiere on April 10, 2011 at  , Planet Doc Review Festival, Sydney Underground Film Festival, Kansas International Film Festival, Trieste Film Festival, Mumbai International Film Festival etc.  , 10th Transilvania Fest to Open in Cluj and Sibiu   , http://planetedocff.pl/index.php?page=film&i=162&lang=en   , Dad Made Dirty Movies?! Free tickets!!.. Not to the dirty movies   , http://www.kansasfilm.com/festival/guide.php   , 13th Mumbai Film Fest to open with Brad Pitts MONEYBALL  

The film often played as a double bill along with Orgy of the Dead - Apostolofs most popular film. Dad Made Dirty Movies was also aired on various TV channels as Arte, SBS One(Australia), Canvas(Belgium), Yle Teema(Finland)  and its going to be released on DVD in 2012 by Mindjazz Pictures (Germany). Dad Made Dirty Movies aired on HBO Central Europe in October 2012.

==Critical reception==
The film received favorable reviews by critics. Boyd Van Hoeij of  s Mark Adams who said that Dad Made Dirty Movies "does a great job in capturing the feel of the era," adding that "fans of cult exploitation films of the late 1960s and early 1970s will relish this engagingly balanced delve into the work of Stephen C. Apostolof."  , Adams, Mark (2011-06-23). "Review: Dad Made Dirty Movies. screendaily.com. Retrieved 2011-11-20.  Tim Elliott of The Age wrote that "Stephen C. Apostolofs life is one of the great postwar American stories, a rollicking tale of rebellion, adventure and female breasts" concluding that "this zesty tribute explores Apostolofs life with suitable scepticism and humour."  , Elliott, Tim (2011-09-09). "Review: Dad Made Dirty Movies. smh.com.au. Retrieved 2012-07-22. 

==Cast==
(in order of appearance)
* Susan Apostolof as Herself
* Polly Apostolof as Herself
* Steve Apostolof as Himself
* Christopher D. Apostolof as Himself
* Shelley Apostolof as Herself
* Harvey Shane as Himself
* Greg Goodsell as Himself
* Rudolph Grey as Himself
* Ted V. Mikels as Himself
* Nadejda Klein as Herself David Ward as Himself

* Ed Wood as Himself (archive footage)
* Kathleen OHara as Herself (archive footage)
* The Amazing Criswell as Himself (archive footage)
* Tor Johnson as Himself (archive footage)
* Marsha Jordan as Herself (archive footage)
* James E. Myers as Himself (archive footage)
* Rene Bond as Herself (archive footage)
* Pat Barrington as Herself (archive footage)
* Billy Graham as Himself (archive footage)
* Lyndon Johnson as Himself (archive footage)
* David F. Friedman as Himself (archive footage)
* Patricia J. Rudl as Herself (archive footage)
* Tim Burton as Himself (archive footage)
* Johnny Depp as Himself (archive footage)
* Kate Moss as Herself (archive footage) Lisa Marie as Herself (archive footage)
* Martin Landau as Himself (archive footage)
* Sarah Jessica Parker as Herself (archive footage)

==Movies referenced==
A list of movies referenced within Dad Made Dirty Movies.

*Journey to Freedom,			1957
*Orgy of the Dead,			1965
*Suburbia Confidential,			1966
*The Bachelors Dreams,			1967
*College Girls,			1968
*Motel Confidential,			1969
*Lady Godiva Rides,			1969
*The Divorcee,			1969
*Class Reunion,			1972
*The Snow Bunnies,			1972
*The Beach Bunnies,			1976 Hot Ice,			1978 Love Is a Four Letter Word,			1966
*Plan 9 from Outer Space,			1958
*Glen or Glenda,			1953 The Trip,			1967
*American Fever,			1979
*Record City,			1978
*Midwest Holiday,			1952
*In the Suburbs,			1957
*Topkapi (film)|Topkapi,			1964 Tom Jones,			1963 Deep Throat,			1972
*The Devil in Miss Jones,			1973
*The Green Slime,			1968
*Heaven with a Gun,			1969 Ed Wood,			1994

===Awards===
* 3rd Bulgarian Film Academy Awards 2012
** Best Television Documentary

* Sofia International Film Festival 2012
** The Young Jury Award for Best Documentary Film

====Nominations====
* Jihlava International Documentary Film Festival 2011 Silver Eye Award for Best Mid-length Documentary Film

* The London Archive Film Festival 2014
** FOCAL Award for Best Use of Footage in a Feature Length Documentary 

==References==
 

==External links==
*  
* 
* 
* 
*  

 
 
 
 
 
 
 
 
 
 