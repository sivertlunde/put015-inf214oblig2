Dimensions of Dialogue
{{Infobox film
| name           = Dimensions of Dialogue
| image          =
| caption        =
| director       = Jan Švankmajer
| writer         = Jan Švankmajer
| music          = Jan Klusák
| cinematography = Vladimír Malík
| editing        = Helena Lebdusková
| distributor    = Krátký Film Praha
| released       =  
| runtime        = 14 minutes
| country        = Czechoslovakia
| language       =
| budget         =
| gross          =
}}
Dimensions of Dialogue ( ) is a 1982 Czechoslovak animated short film directed by Jan Švankmajer. It is 14 minutes long and created with stop motion.

==Plot==
The animation is divided into three sections. "Eternal conversation" (Dialog věcný) shows Giuseppe Arcimboldo|Arcimboldo-like heads gradually reducing each other to bland copies; "Passionate discourse" (Dialog vášnivý) shows a clay man and woman who dissolve into one another sexually, then quarrel and reduce themselves to a frenzied, boiling pulp; and "Exhaustive discussion" (Dialogu vyčerpávajícím) consists of two elderly clay heads who extrude various objects on their tongues (toothbrush and toothpaste; shoe and shoelaces, etc.) and intertwine them in various combinations.  

==Accolades==
Dimensions of Dialogue won 3 major awards in 1983: the Grad Prix from the Annecy International Animated Film Festival, and the Golden Bear for Best Short Film, and the C.I.D.A.L.C. Award (Honorable Mention) from the Berlin International Film Festival. It was also selected by Terry Gilliam as one of the ten best animated films of all time.   

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 

 