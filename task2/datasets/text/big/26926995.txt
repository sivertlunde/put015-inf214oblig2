Paramanandayya Sishyula Katha
{{Infobox film
| name           =    (Telugu:     )
| image          = Paramanandayya Sishyula Katha.png
| image_size     =
| caption        =
| director       = C. Pullayya
| producer       = Thota Subba Rao
| writer         = Vempati Sadasivabrahmam
| narrator       = Raja Babu Chaya Devi Mukkamala
| music          = Ghantasala Venkateswara Rao
| cinematography = C. Nageshwara Rao
| editing        =
| studio         =
| distributor    =
| released       = 1966
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}

Paramanandayya Sishyula Katha  ( ) is a 1966 Telugu Comedy film directed by C. Pullayya. The story was written by Vempati Sadasivabrahmam. The film was remade in Kannada as Guru Shishyaru.

==Plot==
The plot is based on the story of Disciples (Sishya) of Paramanandayya, seven saints in number. They are actually the disciples of Aruna Keerti Mahamuni. Chitralekha (K.R. Vijaya) dances in the court of Lord Siva (Sobhan Babu). He gives her a Rudraksha mala to wear during the visits. Later, she visits the beautiful Earth with her companions and enjoys bathing in a pool. She finds these Sishiyas keenly looking at her and curses them to become idiots. After knowing the facts from Guruji and realizing her mistake, she pronounces the vimukti (free from curse) at the time of her marriage. The Guru, Aruna Keerti, in return curses Chitralekha that she will lose her celestial status if she comes into contact with a human. 

He advises the Sishyas to go to Rajagur(Principle Court Spiritual Adviser) Paramanandayya (Nagaiah) at Vijayadurgam ruled by Nandivardhana Maharaju (N.T. Ramarao). Paramanandayya accepts them as his disciples. Chitralekha forgets the Rudraksha mala while returning to the Heaven. The mala is accidentally found by Maharajah when he come to hunt in the forest. Chitralekha remembers her mala, and goes a snake to the Rajamahal (palace) as a snake and tries to steals the mala.  However Maharajah dreaming about the devakanya (a celestial maiden) touches Chitralekha, who loses her divine powers and past memory.  Maharajah calls Paramanandayya for advice.  He tell the Rajah to handover the mala to her, making her regain the sequence of events. He advises her to pray Lord Siva to attain divinity (so she can go back to Heaven).  Meanwhile, whatever the Sishiyas do out of ignorance turns out to be a good thing for Paramanandayya family.  They save them from thieves etc. Finally they decided to die as Guruji strongly scolded them for purchasing a dead cow. They eat Vishapu Undalu and go to the streets. Night patrolling Rajabhat takes to the Royal Court. They thought Maharaja as Indra and Ranjani (Vijayalakshmi) as Apsara. They reach the house of Ranjani and makes Maharajah know the criminal plans of Minister. The Sishiyas spoiled the marriage of daughter Girija with Tuberculous patient and save Paramanandayya from another family crisis. Ousted by Guruji, they reach RajaMahal and spoil an attempt to kill Maharaja by Jaggarayudu (Satyanarayana) and Minister (Mukkamala).  They convinced Chitralekha to marry Maharajah. During the marriage ceremony they are released from the curse and regain their sainthood.

==Cast==
{| class="wikitable"
|-
! Actor !! Character
|-
| Chittor V. Nagaiah || Paramanandaiah
|-
| Nandamuri Taraka Rama Rao || Nandivardhana Maharaju
|-
| K. R. Vijaya || Chitralekha
|-
| Sobhan Babu || Lord Shiva
|-
| B. Padmanabham || Nandi (Sishya)
|-
| Allu Ramalingaiah || (Sishya)
|- Raja Babu || Phani (Sishya)
|-
| Sarathi || (Sishya)
|-
| Boddapaati || (Sishya)
|-
| Mukkamala Krishna Murthy || Minister
|-
| Chaya Devi || Anandam, Wife of Paramanandaiah
|-
| L. Vijayalakshmi || Ranjani
|-
| Vangara Venkata Subbaiah || Parabrahma Sastry
|-
| Kaikala Satyanarayana || Jaggarayudu, the thief
|-
| R. Nageswara Rao
|-
| Dr. Sivaramakrishnaiah || Virupakshayya
|}

==Crew==
* Director: Chittajallu Pullayya
* Story, Dialogues and Lyrics: Vempati Sadasivabrahmam
* Producer: Thota Subba Rao
* Production Company: Sridevi Productions
* Original Music: Ghantasala Venkateswara Rao
* Cinematography: C. Nageshwara Rao
* Playback Singers: S. Janaki, J. V. Raghavulu, K. Appa Rao, Pithapuram Nageswara Rao, P. Leela, Ghantasala Venkateswara Rao, P. Susheela

==Soundtrack==
* Idigo Vachchiti Ratiraja Madhuve Techchiti Maharaja (Singer: S. Janaki)
* Enaleni Anandameereyi Manakinka Rabodu Ee hayi (Singer: S. Janaki)
* Kamini Madana Rara Ee Tharuna Kori Pilichera (Singer: P. Leela)
* Naaloni Ragameeve Nadayadu Teegaveeve (Lyrics:   and P. Susheela)
* O Mahadeva Nee Padaseva Bhavacharananiki Naava (Singer: P. Susheela)
* Parama Gurudu Cheppinavadu Peddamanishi Kaadura (Singers: Pithapuram Nageswara Rao, K. Appa Rao and J. V. Raghavulu)
* Om Namasivaya Namo Namasivaya
* Vanita Tanantata Tane Valachina Inta Niraadharana (Singers: P. Leela and A. P. Komala)

==1950 film==
* The film is first time made in 1950 produced and directed by famous actor Kasturi Siva Rao. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 