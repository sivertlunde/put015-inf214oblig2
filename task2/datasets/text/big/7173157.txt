Be Human (1936 film)
{{Infobox Hollywood cartoon
| cartoon_name = Be Human
| series = Betty Boop
| image =
| caption =
| director = Dave Fleischer
| story_artist = Lillian Friedman Myron Waldman
| voice_actor = Mae Questel
| musician =
| producer = Max Fleischer
| distributor = Paramount Pictures
| release_date = November 20, 1936
| color_process = Black-and-white
| runtime = 7 mins
| movie_language = English
}}
Be Human is a 1936 Fleischer Studios animated short film starring Betty Boop and Grampy. It is now public domain.

==Plot==

Betty Boop is incensed at her farmer neighbors cruelty to his animals. But the inventive Grampy knows how to teach him a lesson.

Be Human has been criticized for depicting the harsh punishment of the abusive farmer as being commendable. 

==Song==
The cartoon features the song Be Human sung by Betty Boop accompanying herself on piano.  Instrumental renditions of the song are also prominent throughout the cartoon.  When the animal-abusing farmer winds up on Grampys punishment treadmill, a phonograph recording of Grampys voice is heard singing the song.

===Lyrics===
 
Theres a very simple lesson, 
But its easy to forget; 
And I promise, youll regret it if you do.

That if youre always kind and gentle 
With your family pet, 
Hell be just as kind and gentle 
With you.

Be human, animals can cry; 
Be human, its easy if you try. 
Dont go around with a heart of stone, 
Or youll be sorry and all alone.

Be human, have a tender word, 
For every animal and bird!

If we would all be human, 
This world would be in rhyme; 
So be human all the time!

Share a tasty carrot with your parrot, 
Serve your chimpanzee a piece of cheese;  sarsaparilla with your camel or gorilla, 
With kind regards to all of them from me.

Be human, animals can cry; 
Be human, wont you even try? 
Dont think youre wonderful just because 
You werent born with a tail and claws.

Be human, have a tender word, 
For every animal and bird!

Its futile to be brutal, 
That wont get you a dime; 
So be human all the time!

A true cat cant be human, 
But you can!
 

== See also ==
 
*Cruelty to animals
*Animal welfare Vigilantism

==Notes==
 

==External links==
*   (public domain, MPEG4, 7.6MB)
*  
*   on YouTube
 

 
 
 
 
 
 
 
 
 


 