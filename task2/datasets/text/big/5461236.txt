Madonna: Truth or Dare
 
{{Infobox film
| name           = Madonna: Truth or Dare
| image          = Madonna truth or dare poster.jpg
| caption        = Theatrical release poster
| director       = Alek Keshishian Madonna Tim Clawson Steve Golin Lisa Hollingshead Daniel Radford Jay Roewe Sigurjón Sighvatsson
| narrator       = Madonna
| starring       = Madonna
| music          = Madonna Daniel Pearl Toby Phillips Marc Reshovsky
| editing        = Barry Alexander Brown
| studio         = Boy Toy, Inc. Propaganda Films Dino De Laurentiis Communications  (Europe) 
| released       =  
| runtime        = 122 minutes
| country        = United States
| language       = English
| budget         = $4.5 million
| gross          = $29,012,935
}} American singer-songwriter Madonna during her 1990 Blond Ambition World Tour. The film was generally well received by critics and was successful at the box office, at that point becoming the highest-grossing documentary of all time with a worldwide gross of $29,012,935. It was screened out of competition at the 1991 Cannes Film Festival.   
 A Kiss Before Dying.   It holds an 80% rating on Rotten Tomatoes. 

==Plot== Madonna cleans up her hotel room while, in a voice-over, she explains that she is not as emotional as the rest of her group over the end of the tour. She has already grieved, she says, but it will hit her later and she hopes she will be in a safe place when it happens.

In a flashback to April 1990, the tour is about to kick off in Japan. Everything is a mess; there are sound problems and Madonna did not realize that the tour is during the rainy season in Japan. Because it is cold and wet, Madonna and the dancers scrap their costumes for warmer attire. In a voice-over, Madonna confesses that the only thing keeping her from "slashing my wrists" is the thought of returning to North America and performing the show as it is meant to be. In America, Madonna meets the families of her dancers. One dancer, Oliver, sees his father for the first time in several years, while Madonna talks with her father, Silvio "Tony" Ciccone, on the phone.  Though she insists that she can get him tickets, he is reluctant to impose.
 plainclothes officers Like a Sean   was in jail.  I guess its my turn."  Freddy DeMann bets that the threat of arrest will only make Madonna go further and no one takes his bet.  According to a news report, Toronto police decide not to arrest the singer, claiming that no threats were made.

The Blond Ambition tour goes to Madonnas hometown of Detroit.  In a voice-over, Madonna expresses the difficulty she has going back home, especially since fame seems to change ones loved ones.  At the end of "Holiday (Madonna song)|Holiday", Madonna calls her father on stage and sings "Happy Birthday to You".  Backstage, Tony and his wife, Joan, compliment Madonna (and Christopher) on the show, though Tony expresses his displeasure at some of the more "burlesque" aspects. Madonna and Christopher wait for their older brother, Martin, to show up while they discuss his substance abuse problems.  By the time Martin arrives, Christopher has left and Madonna has gone to bed. Madonna reunites with her "childhood idol", Moira McFarland-Messana. Moira gives Madonna a painting she made entitled "Madonna and Child", and asks Madonna to be the godmother to her unborn child. Before leaving Detroit, Madonna visits her mothers grave as "Promise to Try" plays in the background.  It is the first time she has visited her mothers grave since she was a little girl.  Madonna lies down beside her mothers grave as Christopher watches and Madonna whispers that, "Im going to fit in right here.  Theyre going to bury me sideways."

As the tour continues, Madonnas throat problems worsen while Beatty becomes more fed up with the cameras. During a throat examination in Madonnas hotel room, Beatty chastises her for the documentary, telling her the atmosphere is driving everyone insane.  Madonna ignores Beatty, and when she declines to have the rest of her examination done off camera, Beatty starts laughing, saying, "She doesnt want to live off-camera, much less talk... What point is there of existing off-camera?"
 New York, sodomized while partying. In a prayer circle before the final show in New York, Madonna attempts to pull everyone back together.

The third leg of the tour kicks off in Europe with everyone in better spirits. Madonna and the dancers visit Chanel in Paris, fool around at Melissas birthday party and imitate Madonnas 1984 "Like a Virgin" music video. As the tour nears Italy, the Pope attempts to have it banned, forcing Madonna to cancel one show. Sandra Bernhard appears to cheer up Madonna. Sandra asks who would really blow her away if she could meet them and Madonna replies, "That guy whos in all of Pedro Almodóvars films—Antonio Banderas." When Almodóvar throws a party in Madrid and invites the future Evita (1996 film)|Evita co-stars, Madonna spends a week thinking up ways to seduce Banderas. It turns out that he is married, ending Madonnas two-year crush.
 last tour, Sandra Bernhard does not think that she takes enough time to enjoy her successes. Others describe her in unflattering terms. Luis Camacho says, "I just feel like shes a little girl lost in the storm sometimes. You know, theres just a whole whirlwind of things going on around her, and sometimes she gets caught up in it." As the tour winds down, the group plays a game of Truth or Dare? and Madonna is dared to perform Fellatio on a glass bottle. Madonna is asked, "Who has been the love of your life for your whole life??" Madonna replies without hesitation, "Sean Penn|Sean. Sean." Madonna invites her dancers, and backup singers, one-by-one, to join her in bed, where she imparts words of wisdom to each.
 Keep It Together", plays, a montage of Madonna saying good-bye to her dancers is shown. The film ends with a clip of Madonna telling director Alek Keshishian to go away and, "Cut it, Alek! Cut it, Goddamnit!"

==Cast== Madonna

; Uncredited
* Warren Beatty
* Antonio Banderas
* Sandra Bernhard
* Pedro Almodóvar
* Olivia Newton-John
* Matt Dillon
* Kevin Costner
* Lionel Richie
* Mandy Patinkin
* Al Pacino
* Freddy DeMann

==History==
The title of the documentary is in reference to the party game Truth or Dare?. Outside North America the documentary title was changed by Miramax Films to  In Bed with Madonna, due to the game being relatively unknown in other countries. Madonna has commented in various interviews that she hates the title of In Bed with Madonna and has described it as "stupid". The original working title of the documentary was "Truth or Dare: On the Road, Behind the Scenes and In Bed with Madonna".

During the summer of 1990, Madonna hired director Alek Keshishian to film backstage and onstage footage of her Blond Ambition World Tour. The entire documentary is filmed in black-and-white, except for onstage sequences which are in full color. There are appearances from celebrities such as Al Pacino, Mandy Patinkin, Olivia Newton-John, Antonio Banderas, Sandra Bernhard, Kevin Costner, and Warren Beatty, whom she was dating at the time.
 Express Yourself" concert sequence, Madonna says "All right, America! Do you believe in love?" This audio is overdubbed from a US date onto footage shot in Paris where she does not wear the ponytail as in the Japan and USA stops.

Christopher Ciccone stated in his biography Life with My Sister that he felt much of the documentary was "staged" and that he did not like the scene where Madonna visits their mothers grave, stating "All I know is that to Madonna nothing is sacred anymore. Not even our dead mother, whom she has relegated to the role of mere extra in her movie."

==Parodies==
 
To promote the just-released film, Madonna made a guest appearance in a pre-filmed segment on the May 11, 1991 episode of the sketch-comedy show Saturday Night Live.
 Mike Myers) and Garth (Dana Carvey) encounter a seductive Madonna lying on a bed in a hotel room during a dream fantasy sequence (filmed in black and white). After some back-and-forth banter, Wayne and Madonna play Truth or Dare. Wayne dares Madonna to make out with him. As they kiss, the music for her controversial 1990 video "Justify My Love" begins to play. Garth is then seen dancing in a parody of the video before being abducted by two women in fetish-wear outfits.
 Showtime television special, goes to great pains to recreate costumes, sets, and situations that occurred in the original documentary.

Rock group Medusa released the documentary "In Bed with Medusa" in 2013.   In Bed with Medusa (2013) IMDb IMDb.com. Retrieved 2013-04-01  The film uses the same black and white style along with many further references as it simultaneously parodies the film and documents the early life of the band as they release 2 albums and hit the headlines in the UK.

In addition, the popular British television comedy programme French & Saunders did a highly regarded parody of the film with both stars playing Madonna in tandem, a unique concept, which was called "In Bed with French & Saunders". Madonna herself was widely reported to be delighted with the long skit, saying that when French & Saunders did a parody of anything, it was proof that it had "arrived".

==Lawsuit==
On January 21, 1992, three of the Blond Ambition dancers, Oliver Crumes, Kevin Stea, and Gabriel Trupin, filed a lawsuit against Madonna. The suit claimed that the singer had invaded her dancers privacy during the filming of Truth or Dare, as well as charging her with fraud and deceit, intentional misrepresentation, suppression of fact, and intentional infliction of emotional distress for displaying the mens private lives in the documentary. In a commercial for MTVs Rock the Vote campaign later that year, Madonna joked about the lawsuit, saying, "Youre probably thinking thats not a very good reason to vote... So sue me!  Everybody else does."  In October 1994, after more than two years of litigation, the suit was withdrawn and an undisclosed settlement was reached.   Dancers Settle Suit Against Madonna, October 02, 1994 

==Video release==
{{Infobox album  
| Name        = Madonna: Truth or Dare
| Caption   = Blu-ray version of the film, released in 2012
| Type        = Video Madonna
| Cover       = 
| Border      = yes
| Cover size  =
| Released    = October 7, 1991
| Recorded    = 1990
| Genre       = Documentary/Live
| Length      = 122 minutes
| Label       = LIVE Home Video
| Chronology  = Justify My Love (1990)
| This album  = Madonna: Truth or Dare (1991)
| Next album  =   (1994)
| Misc        =
}} LIVE Entertainment in North America on October 9, 1991.     Outside of North America it was released as In Bed with Madonna. In the UK an additional VHS "British Board of Film Classification#Current certificates|15" certificate, edited version was released in November 1991, to allow younger teenagers to watch it.  The packaging was slightly changed to a blue format otherwise identical to the original. In 1992, it was re-released on VHS in the US with two additional live videos of "Like a Prayer" and "Hanky Panky", which played after the end credits.     The DVD version was released on August 26, 1997 by LIVE Entertainment in North America but did not get a worldwide DVD release until January 6, 2003 by MGM Entertainment. 

In March 2009, a new HD master of the film premiered on Palladia HD (MTVs new name).  It included the new Miramax logo at the beginning of the film and a Warner Bros. Television logo at its closing.  The blu-ray disc|Blu-ray version of the film, along with a new DVD release, was released by Lionsgate in North America on April 3, 2012.  The typography for the new release was changed from the original design and is similar to the fashion brand and perfume, Truth or Dare by Madonna which was released at the same time.

===Track listing===
;Concert footage includes  Express Yourself"
*"Oh Father" Like a Virgin"
*"Promise to Try" (montage of film, not concert performance)
*"Holiday (Madonna song)|Holiday"
*"Live to Tell"
*"Vogue (Madonna song)|Vogue" Keep It Together"

;plus excerpts from
*"Papa Dont Preach" (during soundcheck) Like a Prayer"
*"Causing a Commotion"

;US special edition VHS additional footage 
*"Like a Prayer" Hanky Panky"

;Madonna: Truth or Dare DVD extras
*Original theatrical trailer
*Production notes
*Cast and crew biographies

;In Bed with Madonna DVD extras/Madonna: Truth or Dare Blu-ray extras
*Original theatrical trailer

===Formats===
*VHS (1991) – International release
*VHS "British Board of Film Classification#Current certificates|15" certificate version (1991) – released only in the UK, running time was 114 minutes, edited to allow younger viewers to watch the film
*Laserdisc (1991) – released in the US
*Laserdisc (1993) – released outside the US
*VHS (1992) – re-released in the US with additional concert footage of "Like a Prayer" and "Hanky Panky" shown after the ending credits, duration is 133 minutes
*DVD (1997) – released in North America only
*VCD (1997) – released in Asian countries only
*DVD (2003) – released worldwide outside the US and Canada, this was the first DVD release internationally
*Blu-ray (2012) – released only in the US
*Blu-ray + DVD combo (2012) – released only in Canada, contains both discs, DVD contains the same 1997 release

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 