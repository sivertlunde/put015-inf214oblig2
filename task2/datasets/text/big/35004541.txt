Danyel Waro, fyer bâtard
 

{{Infobox film
| name           = Danyel Waro, fyer bâtard
| image          = 
| caption        = 
| director       = Thierry Hoarau
| producer       = Imago Productions
| writer         = 
| starring       = Danyel Waro
| distributor    = 
| released       = 2002
| runtime        = 54 minutes
| country        = France Réunion
| language       = 
| budget         = 
| gross          = 
| screenplay     = Thierry Hoarau
| cinematography = Thierry Hoarau
| editing        = Véronique Sanson
| music          = Danyel Waro
}}

Danyel Waro, fyer bâtard (in English, Danyel Waro, the Proud Bastard) is a 2002 documentary film.

== Synopsis ==
In Réunion, theres no need to introduce Danyel Waro, his rhythms and texts have left their mark on the cultural landscape of the past twenty years. Behind the public image, the singer, there’s a man with strong convictions, a poet, a craftsman. Unknown facets that Thierry Hoaraus film reveals. An intimate portrait of a strong, appealing personality.

== References ==
 

 
 
 
 
 
 


 