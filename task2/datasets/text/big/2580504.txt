The Limping Man (1953 film)
{{Infobox film
| name           = The Limping Man
| image          = The Limping Man (1953 film) poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Cy Endfield
| producer       = Donald Ginsberg
| writer         = Anthony Verney (novel) Ian Stuart Black Reginald Long
| narrator       =
| starring       = Lloyd Bridges Moira Lister Leslie Phillips Alan Wheatley
| music          = Arthur Wilkinson
| cinematography = Jonah Jones
| editing        = Stanley Willis
| studio         = Banner Films
| distributor    = Eros Films Lippert Films (US)
| released       =  
| runtime        = 76 minutes
| country        = United Kingdom English
| budget         =
}} 1953 British crime film directed by Cy Endfield (as Charles de Lautour) and starring Lloyd Bridges, Moira Lister and Leslie Phillips.  The film was based on Anthony Verneys novel Death on the Tideway and was released in the United States by Lippert Pictures.
 public domain.  

==Plot==
Former soldier Frank Prior arrives in London to visit a wartime girlfriend, whom he hasnt seen in six years.  His planes landing at the airport coincides with a fellow passenger being killed by a sniper.

Scotland Yard inspector Braddock and detective Cameron are assigned to investigate. The dead man, identified as Kendal Brown, is carrying forged documents as well as a photograph that leads them to Pauline French, an actress.

Pauline is the woman Frank has come to see. She also happens to be an expert marksman with a rifle. After they kiss, Pauline tells Frank that she had tried unsuccessfully to notify him to delay his visit.

An autographed picture of another actress, Helene Castle, is found in Kendal Browns flat. The detectives learn that Helene is the victims ex-wife. In the meantime, Frank spends a few hours with Pauline on her boat. When they later go to a pub, a limping man seems to menace and unnerve Pauline, who runs away.

Pauline confesses to Frank that she once let Kendal Brown use her boat for a smuggling operation. He began blackmailing her with letters she wrote, which Helene now possesses. At the theater, the limping man turns out to be George, the stage manager. But to everyones shock, the late Kendal Brown turns up very much alive. The victim on the plane was a man hed hired to impersonate him.

After knocking the limping man unconscious, Kendal Brown ends up in a fistfight with Frank in the theaters balcony. But as these events reach their climax, a huge surprise is revealed, one involving Frank and his fellow passengers from the plane.

== Cast ==
*Lloyd Bridges as Frank Prior
*Moira Lister as Pauline French
*Alan Wheatley as  Inspector Braddock
*Leslie Phillips as Detective Cameron
*Hélène Cordet as Helene Castle
*André van Gyseghem as George, stage doorman Tom Gill as Stage Manager
*Bruce Beeby as Kendal Brown Rachel Roberts as the Barmaid
*Lionel Blair as Dancer
*Robert Harbin as Harper LeStrade, magician
*Jean Marsh as the Landladys daughter

== External links ==
* 
*  

==References==
 

 

 
 
 
 
 
 
 
 
 
 
 


 