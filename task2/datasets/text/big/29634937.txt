Moner Manush
 
{{Infobox film
| name           = Moner Manush
| image          =  
| caption        = Theatrical Release Poster
| director       = Goutam Ghose
| producer       = Gautam Kundu Habibur Rahman Khan
| screenplay     = Goutam Ghose
| based on       =  
| starring       = Chanchal Chowdhury Prosenjit Raisul Islam Asad Shuvra Kundu Paoli Dam Priyanshu Chatterjee Tathoi Naufel Jisan Champa Hasan Imam
Shahed Ali
Golam Fakir
| distributor    = Impress Telefilm Ltd. Rosevalley Films Ltd. Vesctesh Films Pvt. Ltd.
| music          = Goutam Ghose
| cinematography = Goutam Ghose
| editing        = Moloy Banerjee
| lyricist       = Lalon Fakir, Shahzad Firdaus 
| released       =  
| runtime        = 150 minutes
| country        = Bangladesh India 
| language       = Bengali
}} biographical musical drama film based on the life and philosophy of Lalon, a noted spiritual leader, poet and folk singer of Bengal in the 19th century. Directed by Goutam Ghose, the film has Prosenjit as the lead actor portraying the character of Lalan Fakir. Paoli Dam plays the character of Komli, the key female disciple of Lalan. 

Moner Manush has been regarded as one of the best creations of recent times, based on the response received by people of Bengal. This effort from Goutom Ghosh comes in the time of growing Bengali movies, making it certain that the revival of golden period of Bengali cinema is not far away.
 National Film Award.

==Plot==
Rabindranath Tagore’s elder brother Jyotirindranath Tagore, a Western educated bright young man from the 19th century Bengal met the octogenarian Lalan Fakir and drew a portrait of the poet saint in the former houseboat afloat on the Padma river. Jyotirindranath, an urban intellectual exchange views with the man of native wisdom. Their exchange of ideas forms the cinematic narrative of this film. The narrative is a saga of the life and time of Lalan Fakir and his liberal sect who lived a life of high order in an otherwise superstitious 19th century Indian society. Lalan inherited the best of the liberal and enlightened tradition of Hinduism, Buddhism and Islam to develop a philosophy of life which is extremely secular and tolerant. Thus became an easy prey for the fundamentalists from the Hindu and the Muslim institutions. They were the parallel stream flowing freely in the heart of rural Bengal when men like Tagore were germinating ideas of the Bengal Renaissance. The love and compassion of Lalan is relevant more than ever in today’s world of intolerance and hate.

==Cast==
* Prosenjit Chatterjee as Lalon
* Priyanshu Chatterjee as Jyotirindranath Tagore
* Chanchal Chowdhury as Kaluah
* Raisul Islam Asad as Siraj Saain Gulshan Ara Champa as Padmabati, Lalons mother
* Tathoi as Golapi
* Paoli Dam as Komli
* Shuvra Kundu as Bhanumati
* Shantilal Mukherjee as kangal horinath
* Anirban Guha as Mir Mosharraf Hossain
* Shahed Ali as Duddu Shah

==Awards==
The movie has been awarded the Best Film at the 41st International Film Festival of India held at Goa from 22 Nov to 02 Dec 2010.

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 