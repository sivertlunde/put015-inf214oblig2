Farewell, My Lovely (1975 film)
{{Infobox film
| name           = Farewell, My Lovely
| image          = Poster Farewell My Lovely 1975.jpg
| image_size     =
| caption        = Theatrical release poster
| alt            =
| director       = Dick Richards
| producer       = Elliott Kastner Jerry Bruckheimer George Pappas
| screenplay     = David Zelag Goodman
| story          = Farewell, My Lovely Raymond Chandler John Ireland Sylvia Miles Anthony Zerbe
| music          = David Shire
| cinematography = John A. Alonzo Walter Thompson
| studio         = ITC Entertainment Avco Embassy Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $2.5 million The great movie money show. Michael Pye. The Sunday Times (London, England), Sunday, July 13, 1975; pg. 47; Issue 7935.  (966 words) 
| gross          = $2,000,000 (United States) 
}} novel of John Ireland, 1978 remake The Big Sleep, making him the only actor to portray Philip Marlowe more than once on the big screen.

==Plot==
Set in Los Angeles in 1941, against a seamy backdrop of police corruption, cheap hotel rooms, illegal gambling and jewel trafficking, private detective Philip Marlowe is holed up in a hotel room and growing more weary by the hour. As he explains to his police lieutenant friend Nulty: "Ive got a hat, a coat and a gun, thats it."

Marlowe has been hired by a huge and surly ex-convict, Moose Malloy, to find his old girlfriend Velma, whom he hasnt seen in years. At the same time, Marlowe is investigating the murder of a client named Marriott who was a victim of blackmail and a stolen necklace made of jade.

While encountering connections to both cases, Marlowe develops an attraction to the married and seductive Helen Grayle. As the body count mounts, Marlowe survives attempts on his life, which include being drugged and held captive by a psychotic brothel madam named Amthor and her thugs. The action comes to a head with a shootout on a gambling boat off the L.A. coast.

==Cast==
* Robert Mitchum as Philip Marlowe: Cynical Private Eye/Protagonist
*   John Ireland as Lt. Nulty: Skeptical LAPD Detective
* Sylvia Miles as Jessie Halstead Florian: Retired Showgirl/"Secret" Drinker
*  /Gambling Operator
*  
* Jack OHalloran as Moose Malloy: Huge Ex-Convict
* Joe Spinell as Nick: Hired Muscle
* Sylvester Stallone as Jonnie: Hired Muscle
*  /Drug Peddler (Believed to be based on Brenda Allen
* John OLeary as Lindsay Marriott: "Homosexual" Blackmailer/Finger man for Jewel Mob
* Walter McGinn as Tommy Ray: Second Rate Jazz Trumpeter Jim Thompson as Judge Baxter Wilson Grayle: Corrupt Law Official/Helens "sick" husband

==Production==
Sir Lew Grade had previously invested in Kastners film Dogpound Shuffle. The producer approached him to invest in Farewell My Lovely and Grade agreed, knowing the movie could be easily be pre-sold to TV. Grade later financed The Big Sleep. Lew Grade, Still Dancing: My Story, William Collins & Sons 1987 p 246 
 The Big Sleep, although that film was set in the present day and in England, rather than shot as a period piece in the detectives customary setting of Los Angeles.

Marlowes client, Moose Malloy, is played by Jack OHalloran, a former professional prizefighter. Sylvester Stallone, in an early role prior to Rocky, has a small role as an employee of the brothels sadistic madam (played by actress Kate Murtagh).
 Jim Thompson, The Getaway The Grifters, appears in the film as Judge Grayle.

The idea whether Anne Riordan should be included in the script, as she had been in Raymond Chandlers novel. Lew Grade thought "there are too many fucking dames on this lot as there is. Spinell, Stallone and Mitchum keep fucking most of them. So Anne Riordan will not be in this picture".

==Reception==
===Critical response=== The Long Goodbye. Richards doesnt hedge his bet. 

The staff at Variety (magazine)|Variety was more critical of the film, writing, "Farewell, My Lovely is a lethargic, vaguely campy tribute to Hollywoods private eye mellers of the 1940s and to writer Raymond Chandler, whose Philip Marlowe character has inspired a number of features. Despite an impressive production and some firstrate performances, this third version fails to generate much suspense or excitement." 

Film critic Dennis Schwartz believes that actor Robert Mitchum was well cast  and wrote, "The films success lies in Mitchums hard-boiled portrayal of Marlowe, its twisty plot and the moody atmosphere it creates through John A. Alonzos photography. Los Angeles looms as a nighttime playground for hoods, beautiful women and suckers ready to be taken by all the glitzy signs leading them astray." 

It maintains an 84% film rating on Rotten Tomatoes. 

===Box Office===
The film was profitable. TV rights were sold to NBC for $1.2 million. 

===Accolades===
Nomination
* Academy Awards: Best Actress in a Supporting Role, Sylvia Miles; 1976.
* Edgar Allan Poe Awards: Best Motion Picture, David Zelag Goodman; 1976.

==Previous adaptations==
:See: Farewell, My Lovely#Film adaptations|Farewell, My Lovely -- Film adaptations The Falcon in place of Philip Marlowe;  and in 1944, as Murder, My Sweet, featuring Dick Powell as Marlowe and directed by Edward Dmytryk. 
 The Big Robert Montgomery George Montgomery (1947), James Garner (1969) and Elliott Gould (1973).

==Soundtrack==
An original motion picture vinyl soundtrack album composed by David Shire was released in 1975 by United Artists Records. The album contained 11 tracks. 

Track listing
* 1. Main Title (Marlowes Theme)
* 2. Velma / Chinese Pool Hall / To the Mansion
* 3. Mrs. Grayles Theme
* 4. Amthors Place
* 5. Mrs. Florian Takes the Full Count
* 6. Marlowes Trip
* 7. Convalescence Montage
* 8. Take Me to Your Lido
* 9. Three Mile Limited
* 10. Moose Finds His Velma
* 11. End Title (Marlowes Theme)

===Note===
"Marlowes Theme" was for many years used as closing music to legendary Swedish radio jazz program Smoke Rings.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 