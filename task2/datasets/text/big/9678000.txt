Selva (film)
{{Infobox film
| name = Selva
| image =
| director = A. Venkatesh (director)|A. Venkatesh
| writer = Senthamizhan (dialogues)
| screenplay = A. Venkatesh (director)|A. Venkatesh
| story = Majeeth Vijay Swathi Swathi | Pavithran
| music = Sirpy
| cinematography = S. Saravanan
| editing = B. Lenin V. T. Vijayan
| studio = A. R. S. International
| distributor = A. R. S. International
| released = December 12, 1996
| runtime = 120 minutes
| country = India Tamil
| budget =  2.3 crore
}} Tamil movie Swathi and Raghuvaran.  The music is composed by Sirpy.  The film is directed by A. Venkatesh (director) |A. Venkatesh. 

== Plot ==
Selva (Vijay) lives in a colony.  He is the son of lawyer Varatharajan(Raghuvaran) but he lives separately.  Sangeetha is one of the girls in the colony. Meanwhile a ministers daughter moves into the colony.  She moves friendly with Selva.  Sumathi(Swathi) who is in love with Selva mistakes their friendship.  Vartharajan works for a terrorist group who kidnaps the ministers daughter.  Selva goes to the rescue of the girl.  However, Selva kills his father because Varatharajan was going to kill Sumathi.  Finally they will come against out of the clutch of the terrorists.

==Cast== Vijay as Selva Swathi as Sumathi Reeva as Kamini
*Raghuvaran as Varatharajan
*Rajan P. Dev Senthil 
*Manivannan
*Jayachithra
*Vichithra
*Shanmuga Sundari
*C. R. Saraswathi

==Soundtrack==
The music composed by Sirpy. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics
|- Vaali (poet) Vaali
|- Mano
|-
| 3 || Lappu Tappu || Suresh Peters, Swarnalatha, Deepika
|- Sujatha
|-
| 5 || Tharaiyil Natakkuthu || S. P. Balasubrahmanyam, Swarnalatha
|}

==References==
 

 

 
 
 
 
 


 