Tere Mere Phere
{{Infobox film
| name           = Tere Mere Phere
| image          = Tere mere phere key art.jpg
| alt            =  
| caption        = Theatrical Poster
| director       = Deepa Sahi
| producer       = Ketan Mehta Anup Jalota Sitara Productions
| story          = Deepa Sahi Jagrat Desai
| starring       = Vinay Pathak Riya Sen Jagrat Desai Sasha Goradia
| music          = Shivi R. Kashyap
| cinematography = Christo Bakalov
| editing        = Prateik Chitalia
| studio         = 
| distributor    = 
| released       =  
| runtime        = 125 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Tere Mere Phere  ( ) is an 2011 Hindi romantic comedy, Road film directed by well known and respected actress Deepa Sahi, and produced by the internationally acclaimed producer director Ketan Mehta and renowned singer Anup Jalota. Presented by Sitara Productions, it is a Maya Movies  Production. It stars Vinay Pathak. Riya Sen in the lead roles and introduces newcomers Jagrat Desai and Sasha Goradia. The story of the film has been written by Deepa Sahi and Jagrat Desai. The music is by Shivi R. Kashyap and the lyrics are penned by Manoj ‘Muntashir’ and Ketan Mehta. Most of the songs are choreographed by Bollywood choreographer Jeet Singh. It was released on 30 September 2011 to a mixed but mostly positive reception from critics.

== Plot ==
Tere Mere Phere is a story about a newly married couple, Pooja and Rahul, who have a perfect love affair and a perfect marriage and set out on a perfect dream honeymoon in a motor home, driving through the breath taking Himalayas …  but then can life ever be perfect?!  If “distance makes the heart grow fonder” then too much proximity should have the reverse effect! And it does ... As Pooja and Rahul get too close for comfort, the gilt of romance begins to rub off, a series of absurd fights follow and soon they get embroiled with  a local Himachali  small town  guy ( Vinay Pathak)  and his girl Muskaan, the  “Pahadi Mirch”  (Riya Sen).

== Cast ==
* Vinay Pathak as Jai Dhumal
* Riya Sen as Muskaan
* Jagrat Desai as Rahul Bhasin
* Sasha Goradia as Pooja Pahuja
* Darshan V. Jariwala as Rahul’s father
* Sushmita Mukherjee as Seema, Rahuls mother
* Rohan Shah as Raju, Rahul’s brother
* Anup Jalota as Pooja’s father
* Ketan Karande as Veeru

==Production==
The film has been shot in beautiful and unusual parts of Himachal and in the newly opened Kinnaur Valley. The Climax of the film has been shot at Hatu Peak and Jhamunda (near Tethys Resort, Narkanda).The opening shot of trailer in which the vehicle drives through a dangerous monolothic road is shot on National Highway 22, at the entrance of Kinnaur Valley. This steep descent is known as taranda descent.

==Reception==

===Critical response===
The film opened to generally mixed reviews but mostly positive.

Nikhat Kazmi, from the Times of India rated it 3 stars with this review - "Deepa Sahi makes a spirited debut as a director with Tere Mere Phere. No, shes not telling you a great story and doesnt pretend to do so either. All she wants to do is capture the quirks of the human psyche and make you chuckle while she builds her foursome of charming characters who fumble their way through the highs and lows of love and longing. While Pooja and Rahul slug it out on what-you-will -- personal habits, flirty asides with strangers -- Vinay and Riya make the most unusual pair with Riya determined to tie the knot with her reluctant beau."  

Subhash K Jha gives it 3.5 stars and says "What really works for the film is its tongue-in-cheek dialogues. The words ring true even when the couple raises its voice to be heard above the background music. That the two newcomers get into the thick of things without losing sleep over flattering camera angles, only adds to the charm of this cute and clever rom-com. Go for it. Tere Mere Phere is the funniest marital comedy in ages." 

Komal Nahta says, "Deepa Sahi’s debut at direction is promising. She seems to have a flair for comedy and makes even the exaggerated comic scenes humorous enough for people to laugh. She has extracted good work from out of her actors. Vinay Pathak does a fine job and plays the character if Jai with understanding and maturity.Jagrat Desai does not have the looks of a hero but the debut-making actor performs with effortless ease. Sasha Goradia looks nice and makes an appealing debut. She has a good sense of comic timing and does very well in comic scenes. Darshan Jariwala is natural. Sushmita Mukherjee does an excellent job. " 

Ibns Rajeev Masand called the film awful, loud and an exaggerated comedy. 

fullhyd.coms Nupur Barua said, "The only reason to watch Tere Mere Phere is to sit through the visuals of Himachal Pradesh," rating the film 3 out of 10. {{cite web|title=Tere Mere Phere Review|url=http://www.fullhyderabad.com/profile/movies/4512/2/tere-mere-phere-movie-review#tabs
|publisher=rediff.com|author=Preeti Arora|date=30 September 2011}} 

"Tere Mere Phere - You are warned. Do not miss this one !!***1/2 stars
Bhavikk Sangghvi - planetbollywood.com

RJ Jeeturaj ( Indias Hottest RJ ) says " Full2 entertainment honeymoon journey !!!"

== Music ==
The music is composed by Shivi R.Kashyap a young composer and singer from Himachal, a fusion of Himachali folk and contemporary world music. Most of the songs are choreographed by Bollywood choreographer Jeet Singh.

===Audio Listing===
{{Track listing
| extra_column = Singer(s)
| title1 = Bum Bhole | extra1 = Sukhwinder Singh | length1 = 3:28
| title2 = Ishq Khumari | extra2 = Baba Sehgal and Shivi R. Kashyap | length2 = 2:52
| title3 = Tere Mere Phere | extra3 = Shivi R. Kashyap and Subhash | length3 = 4:46
| title4 = Tere Bina | extra4 = Anup Jalota | length4 = 5:34
| title5 = Hey Dude I Dont Wanna Marry | extra5 = Shivi R. Kashyap and Rishi Singh | length5 = 2:07
}}

== References ==
 

==External links==
* 

 
 