The Naked Edge
 
 
{{Infobox film
| name           = The Naked Edge
| image          = Poster of the movie The Naked Edge.jpg
| caption        =  Michael Anderson
| producer       = George Glass Walter Seltzer
| writer         = Max Ehrlich Joseph Stefano
| starring       = Gary Cooper Deborah Kerr
| music          = William Alwyn Tony White
| editing        = Gordon Pilkington
| distributor    = United Artists
| released       =  
| runtime        = 97 minutes
| country        = United Kingdom United States
| language       = English
| budget         = 
}} American co-production Michael Anderson and produced by George Glass and Walter Seltzer with Marlon Brando Sr. as executive producer. The screenplay was by Joseph Stefano and Max Ehrlich, the music score by William Alwyn and the cinematography by Erwin Hillier and Tony White. The production design was by Carmen Dillon.

The film was shot in London and at Elstree Studios, Borehamwood, Hertfordshire, and was Gary Coopers last film.

== Plot ==

In the aftermath of a theft and murder, Martha Radcliffe (Kerr) increasingly suspects her husband George Radcliffe (Cooper), whose testimony in court convicted the main suspect, of being the real culprit.

Businessman Jason Root is stabbed to death on a night when George and a clerk named Donald Heath are the only other employees working at the office.  A mailbag full of money is stolen in the process.  George, who is seen sweating nervously both during the trial and later, insists that Heath must have been the murderer, and Heath is convicted.  Several months later, the mailbag is found, and the Radcliffes receive a letter that was in the bag.  The letter, which Martha reads, contains a blackmail threat from Jeremy Gray (Eric Portman) accusing George of the crime.  

As the story unfolds, clues pointing to George quickly accumulate.  These include a new business he started soon after the trial, using money that he claims to have made in the stock market; his own desperate desire for success; his lying to his wife in order to secretly search for Gray; some suspicious business with an unknown man; and Grays claim, when Martha finds him, that he was an eyewitness to the crime and George was the murderer.

George and Martha repeatedly have conversations in which she vacillates between questioning him and insisting she believes in his innocence, and he alternates between insisting that she believe in him and telling her to make up her own mind.  Tension is built by the repeated appearance of Georges old-style shaving razor, his insistence that she join him at the edge of a cliff, references to his masculine virility, and his warning that her investigation could threaten his business.

At the conclusion, a man tries to kill Martha after being seen sharpening Georges razor. The man turns out to be Gray.  George rescues his wife just in time and subdues Gray as the police arrive.

== Cast ==
* Gary Cooper as George Radcliffe 
* Deborah Kerr as Martha Radcliffe 
* Eric Portman as Jeremy Gray 
* Diane Cilento as Mrs. Heath 
* Hermione Gingold as Lilly Harris 
* Peter Cushing as Mr. Evan Wrack  Michael Wilding as Morris Brooke  Ronald Howard as Mr. Claridge 
* Ray McAnally as Donald Heath 
* Sandor Elès as Manfridi St John  Wilfrid Lawson as Mr. Pom 
* Helen Cherry as Miss Osborne 
* Joyce Carey as Victoria Hicks
* Diane Clare as Betty  
* Frederick Leister as Judge
* Martin Boddey as Jason Roote

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 