Raketeros
{{Infobox film
| name             = Raketeros
| image            = File:Raketeros, 2013 comedy film.jpg
| director         = Randy Santiago
| producer         = Harlene Bautista-Sarmenta Herbert Bautista
| music            = 
| cinematography   = 
| story            = 
| screenplay       = 
| writer           = 
| editing          = 
| starring         = Herbert Bautista Dennis Padilla Long Mejia Andrew E. Ogie Alcasid 
| studio           = Heavens Best Entertainment
| distributor      = Star Cinema
| released         = August 7, 2013
| country          = Philippines
| language         =  
| runtime          = 
| budget           =
| gross            = P4,616,940 
}}
 Filipino comedy film produced by Heavens Best Entertainment starring some of 90s comedy icons Herbert Bautista, Dennis Padilla, Long Mejia, Andrew E., and Ogie Alcasid. The film will open in theaters on August 7, 2013 as part of Star Cinemas 20th Anniversary presentation.      

== Synopsis ==
Raketeros shares the adventures and misadventures of a group of middle-aged male friends–-Berto (Herbert Bautista), the cable guy; Mando (Dennis Padilla), the billiards player; Mulong (Long Mejia), the driver; Julio (Ogie Alcasid), the all-around singer; and Andoy (Andrew) the ‘religious.’ The five are gathered to do a simple task of delivering an expensive gown to their rich friend’s mansion. However, a series of hilarious mishaps puts them and their task in jeopardy.

== Cast ==
*Herbert Bautista as Berto
*Dennis Padilla as Mando
*Long Mejia as Mulong
*Andrew E. as Andoy
*Ogie Alcasid as Julio
*Joey Marquez
*Mark Gil
*Sam Pinto
*Ryan Bang
*Wendy Valdez
*RR Enriquez
*Regine Angeles
*Tippy Dos Santos
*Rodjun Cruz
*Eula Caballero
*Karen Reyes
*Markki Stroem
*Ryan Boyce
*Ya Chang
*IC Mendoza
*Ahwel Paz
*Patricia Ismael 
*Lassy
*Rufa Mae Quinto
*Nova Villa
*Jaime Fabregas
*Dimples Romana
*Cynthia Patag
*Jay Manalo Efren Reyes
*DJ Durano
*Roi Vinzon
*Cherry Pie Picache
*Janice de Belen
*Regine Velasquez

==References==
 

==External links==
 

 
 
 