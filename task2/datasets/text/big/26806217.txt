Lost Patrol (1929 film)
{{Infobox film
| name           = Lost Patrol
| image          =
| caption        =
| director       = Walter Summers 
| producer       = Harry Bruce Woolfe
| writer         = Philip MacDonald (novel)   Walter Summers
| starring       = Cyril McLaglen  Sam Wilkinson   Terence Collier   Arthur B. Woods
| music          =
| cinematography =  
| editing        = 
| studio         = British Instructional Films
| distributor    = Fox Film Corporation
| released       = February 1929
| runtime        = 7,250 feet 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}}
  British silent silent war remade in 1934 by John Ford.

==Synopsis==
During the First World War, a hard-pressed British patrol in the deserts of Mesopotamia come under attack from the enemy. Gradually they are picked off one by one.

==Cast==
* Cyril McLaglen as The Sergeant
* Sam Wilkinson as Sanders
* Terence Collier as Corporal Bell
* Arthur B. Woods as Lieutenant Hawkins
* Hamilton Keene as Morelli
* Fred Dyer as Abelson
* Charles Emerald as Hale 
* Andrew McMaster as Brown 
* James Watts as Cook 
* John Valentine as Mackay

==References==
 

==Bibliography==
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.

==External links==
 

 

 
 
 
 
 
 
 
 
 
 
 

 