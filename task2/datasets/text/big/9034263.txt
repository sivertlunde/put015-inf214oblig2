Chhote Sarkar (1996 film)
{{Infobox film
| name           = Chhote Sarkar
| image          = Chhote_Sarkar_Movie_Poster.jpg
| image_size     = 
| caption        = Promotional Poster
| director       = Vimal Kumar
| producer       = 
| writer         =  S. Khan
| narrator       =  Govinda Shilpa Shetty
| music          = Anand-Milind
| lyrics         = Rani Malik
| cinematography = Anil Dhanda
| editing        = 
| distributor    = 
| released       = 22 November 1996
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Govinda and Shilpa Shetty. 

==Plot==

Amar Saxena (Govinda (actor)|Govinda) is a businessman, and the Managing Director of prestigious Khaitan Fans Ltd., India. One day while leaving from a board meeting, he meets a beggar named Jagmohan, who calls him Rohit, and asks him to return home to his ill wife, Seema. Amar denies being Rohit, but feels sorry for him and gives him some money, which Jagmohan declines to accept. Curious at his refusal, Amar offers to help. He visits the hospital and gets to meet Seema, and ends up falling in love with her. What he does not know that Jagmohan and Seema are plainclothes police officers, who have been assigned to arrest him for the murder of Ram Kumar Saxena, Amars uncle. Using his wits, Amar manages to convince the Court that he is of unsound mind, and thus escapes being imprisoned, and ends up in an Psychiatric hospital|asylum. From here Amar must figure out who has framed him for a murder he did not commit, and why Seema and Jagmohan were so intent to incriminate him.

==Cast== Govinda ....  Amar/Rohit
*Shilpa Shetty ....  Inspector Seema
*Aruna Irani....  Amars aunt
*Sadashiv Amrapurkar ....  Dr. Khanna
*Tej Sapru
*Kader Khan ....  Commissioner Jagmohan
*Divya Dutta ....  Meena
*Aashif Sheikh ....  Lobo (as Aasif Shiekh)
*Avtar Gill ....  Ram Kumar Saxena
*Dinesh Hingoo ....  Army Officer
*Guddi Maruti
*Yunus Parvez
*Mehmood Jr. ....  Asylum inmate (as Jr. Mehmood)
*Mahesh Gupta
*Gur Bachchan Singh (as Sarfaraz Ahmed)

==External links==
*  

 
 
 
 
 
 


 
 