Ivan & Ivana
Ivan & Ivana is a documentary directed by Jeff Daniel Silva in 2011.

== Synopsis ==
Ivan & Ivana follows a young couple fleeing the Kosovo War.  They arrive in California at the height of the housing boom.  The film follows their experience as new American immigrants through their economic, political and personal experiences. http://ivanandivana.com/thefilm.html   The film sets the characters among the events of the Kosovo War, while making no judgments about that war. 

== Production == Visions du Rèel Documentary Film Festival in Nyon, Switzerland.

== External links ==
* http://variety.com/2012/film/reviews/ivan-ivana-1117947195/
* http://www.hollywoodreporter.com/review/ivan-ivana-perm-festival-review-381498
* http://www.moma.org/visit/calendar/film_screenings/14560
* http://livinginnyon.com/ivan-and-ivana-living-the-american-dream/
* http://2012.doxafestival.ca/festival/films/ivan-ivana
* http://www.viennale.at/en/films/ivan-ivana
* http://www.iffboston.org/past/film.php?year=2011&film=678
* http://www.notcoming.com/reviews/ivanandivana
* http://www.thecrimson.com/article/2012/4/3/jeff-silva-documentary-portrait/
* http://www.lef-foundation.org/NewEngland/MovingImageFundGrantsDirectory/tabid/230/g/1186/Default.aspx
* http://cinema-scope.com/cinema-scope-online/endings-and-endings-recontres-internationales-du-documentaire-de-montreal-2011/
* http://blog.thephoenix.com/BLOGS/outsidetheframe/archive/2011/04/29/and-more.aspx
* http://suletomkinson.ca/uncategorized/ridm-november-11th/

== References ==
 

 
 
 
 
 


 