The Silent Partner (1978 film)
{{Infobox film
| name           = The Silent Partner
| image          = Silent partner.jpg
| image_size     =
| caption        = Movie cover for The Silent Partner
| director       = Daryl Duke Stephen Young
| writer         = Anders Bodelsen Curtis Hanson
| narrator       =
| starring       = Elliott Gould Christopher Plummer Susannah York
| music          = Oscar Peterson
| cinematography =
| editing        =
| distributor    = Carolco Pictures
| released       =  
| runtime        = 106 min.
| country        = Canada
| language       = English
| budget         = Canadian dollar|C$2,500,000 (estimated)
| gross          =
}}
 1978 Canadian heist film directed by Daryl Duke. It stars Elliott Gould, Christopher Plummer and Susannah York.

The film was the first to be produced by Carolco Pictures and one of the earliest films from Canada to take advantage of the Canadian governments "Capital Cost Allowance" plans. The Silent Partner is also notable for being one of the very few films to have a score composed by Oscar Peterson, and for featuring an early big-screen appearance by John Candy.
 Danish film Think of a Number (Tænk på et tal) from 1969 written and directed by Palle Kjærulff-Schmidt. Both are based on the novel Tænk på et tal by Danish writer Anders Bodelsen.

==Plot synopsis==
Miles Cullen (Elliott Gould), a bored teller at a small bank in a large Toronto shopping mall (The Eaton Centre), accidentally learns that his place of business is about to be robbed when he finds a discarded note on one of the banks counters one day. He also figures out who the would-be robber will be when he sees a mall Santa Claus hanging around outside the bank whose give to charity sign is similar to the handwriting on the discarded stick-up note.

Instead of informing his bosses or contacting the police, Miles devises a way of keeping the cash from most of his windows transactions in an old lunch box of his own inside his briefcase rather than in the banks till. As a result, the Santa Claus robber eventually arrives and holds up Miles at the tellers desk, and nets far less than the police and the bank think he has.

The thief, a psychopath named Harry Reikle (Christopher Plummer), figures out what happened, and makes a series of desperate and violent attempts to get the money (totaling CA$48,300) that Miles has kept for himself. Reikle starts following Miles around to and from his home, and making harassing phone calls to his place of residence.
 Michael Kirby). After escorting Julie to Mr. Packards Christmas party at his house, he tells Julie about his own attraction to her and disapproves of her dating a married man. In another subplot unrelated to the Miles-Reikle plot, Miles co-worker Simonson (John Candy) becomes infatuated with a young ditzy blonde named Louise, who is hired as a teller at the bank. However, though Miles discovers that Louise is cheating on Simonson with another co-worker, he never informs Simonson about Louises true nature, even after Simonson and the pregnant Louise get engaged and married, apparently to avoid hurting his feelings.

After Reikle breaks into Miles apartment and trashes it to look for the stolen bank money, Miles turns the tables on his stalker yet again by following Reikle back to his apartment and then sets him up to be arrested for the theft of a delivery truck. When Miles is brought to the police station to identify Reikle in a lineup, he does not point him out aware that Reikle would then implicate him in the bank robbery.

A few months later, during the spring, Miles invalid father, who has been residing in a nursing home, dies. At his fathers funeral, Miles meets and takes up with a flirtatious younger woman named Elaine (Celine Lomez), who tells him that she was a nurse who has been caring for his father. In fact, Elaine is secretly working with Reikle, who is still in jail awaiting trial for the delivery truck theft and unrelated assault and battery charges. For the past several months, Elaine has been visiting Reikle in jail to report her progress to acquire the stolen bank cash. But by the time Elaine discovers that Miles has stashed the holdup money in a vacant safety deposit box at the bank, Reikle no longer trusts her, for he correctly feels that Elaine has fallen in love with Miles.

Miles figures out that Elaine is not who she claims to be when she tells him about his father who had spoken to her about his life. Miles immediately catches her slip and informs Elaine that his invalid father had a stroke two years earlier and could not talk at all. Elaine reveals that she is working with Reikle and wants Miles to hand over the bank loot so they can divided it among themselves.

However, Miles loses the safety deposit key to the box where he has stashed the stolen bank cash. Elaine decides to help Miles and disguises herself as a customer to have copies of the safe deposit keys made in order to remove the money from the bank and she successfully evades Julie who has begun to suspect something off about Miles and his new girlfriend.

A few days later, Reikle is released from jail and confronts Elaine one night over where her loyalties lie. When Elaine admits that she has indeed fallen in love with Miles, an enraged Reikle murders and decapitates her at Miles apartment. Miles is forced to dispose of her body in the foundation for the new bank building to avoid being investigated by the police. Afterwards, Reikle approaches Miles and says that he intends to kill him too unless he agrees to get the money. Miles, however, refuses to part with it except in a public place where no harm can come to him.

The plan they agree to is that Reikle will come to the bank, again in disguise, and be handed the money at Miles window. The next day, Reikle arrives dressed as a woman. Once he is paid the money, he promises to kill Miles for the inconvenience that hes been caused. Miles, anticipating Reikle would say just that, immediately presses an alarm button. Reikle shoots Miles and flees into the mall, where he is shot dead by a bank security guard.

A wounded Miles is taken away by an ambulance. Julie tells Miles that she has figured out everything about him and his involvement in the bank robbery. He reveals to Julie that he still has the stolen bank money and that he only gave Reikle again a few thousand dollars of the banks money from the till. Both of them decide that the time might be right to quit their jobs and find some other line of work.

==Cast==
* Elliott Gould  - Miles Cullen
* Christopher Plummer - Harry Reikle
* Susannah York  - Julie Carver
* Celine Lomez  - Elaine
* John Candy  - Simonson Michael Kirby - Charles Packard
* Sean Sullivan - Frank the Security Guard
* Gail Dahms - Louise
* Michael Donaghue - Berg
* Ken Pogue - Detective Willard

==Reception== Canadian Film Academy Awards including Best Picture and Best Director.  Rist, 1995. pp.211  The film was a sleeper upon its US release, with Brendon Hanley of the film database Allmovie noting that the film"...stands out as one of the best sleepers of the late 70s". 

==Notes==
 

==References==
*{{cite book
 | last= Rist
 | first= Peter 
 | title= Guide to the Cinema(s) of Canada 
 |publisher= Greenwood Publishing Group 
 |year= 2001 
 |isbn= 0-313-29931-5
}}

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 