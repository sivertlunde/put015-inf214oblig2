Together (2002 film)
{{Infobox film
| name           = Together
| image          = Together One-Shee.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Chen Kaige Chen Hong
| writer         = Chen Kaige Xue Xiaolu Chen Hong Chen Kaige
| music          = Zhao Ling
| cinematography = Kim Hyljingkoo
| editing        = Zhou Ying
| distributor    = 
| released       = 10 September 2002 (Toronto International Film Festival) 20 September 2002 (China)
| runtime        = 119 minutes China
| Chinese (Mandarin)
| budget         = 
| gross          = $14,572,229 
}} 2002 Peoples Chinese drama drama film Chen Hong, and Wang Zhiwen. The film premiered on September 10, 2002 at the Toronto International Film Festival, and was commercially released in China ten days later.
 prodigy Liu Xiaochun and his father Liu Cheng who move to Beijing from a small southern town. It is Liu Chengs biggest dream that Xiaochun may find a good teacher in the city and rise to stardom. After studying under two teachers, however, the boy finds that he has learned not just music, but what is really important in life.

==Plot==

Liu Cheng (Liu Peiqi) is a widowed cook making his living in a small southern town in Peoples Republic of China|China. His thirteen-year-old son, Liu Xiaochun (Tang Yun), is a violin child prodigy|prodigy. In the hope that Xiaochun might find success as a violinist, Liu Cheng and Xiaochun travel to Beijing to participate in a competition organized by the Childrens Palace (China)|Childrens Palace, an arts institution for school children. Even though Xiaochun emerges fifth, he is denied admission into the conservatory as he does not have Beijing residency. Determined to realize his hopes for Xiaochun, Liu Cheng persuades Professor Jiang (Wang Zhiwen), a stubborn and eccentric teacher from the Childrens Palace, to take Xiaochun as a private student.
 Chen Hong), a young and attractive woman living upstairs. Lili does not have a regular job but lives off the rich men she dates. However, her heart lies with Hui (Cheng Qian), her smooth-talking, cheating boyfriend. After Lili finds out about Huis affairs in the presence of Xiaochun, Xiaochun sells his violin to a shop, buys a fur coat that Lili fancies, and tells Hui to give it to her as a consolation.

Meanwhile, Liu Cheng has learnt about a Professor Yu Shifeng (Chen Kaige), a high-profile professor from the Central Conservatory of Music. Believing that Professor Yu is able to bring Xiaochun to fame and success, Liu Cheng decides to switch teachers for Xiaochun. He pays Professor Yu a visit and confides in the latter the truth about Xiaochuns birth. It turns out that Liu Cheng has never been married. He found the infant Xiaochun abandoned in a train station, a violin placed next to the baby. Liu Cheng brought both home and raised the baby as his own. Xiaochun turned out to be a child prodigy, and Liu Cheng resolved to devote his life to cultivate his adopted sons talent. Professor Yu, apparently unmoved, nonetheless agrees to give Xiaochun an audition.

However, Liu Cheng arrives at Professor Yus home with Xiaochun only to find the violin case empty. Xiaochun, angry at his father for choosing commercial success over music, refuses to play even when offered another violin. Liu Cheng is so enraged that upon returning home, he tears up the award certificates that Xiaochun won in previous competitions. Lili, remorseful over the affair of the fur coat, looks up Professor Yu and persuades him to give Xiaochun a second chance. This time Xiaochun plays, and the professor agrees to take him without hesitation.

Professor Yu has another talented young student, Lin Yu (Zhang Jing), a highly jealous and ambitious girl. When a selection trial for an international competition is coming up, Lin Yu and Xiaochun are in a contest for the only spot. Liu Cheng decides to pack up and return home first, both to allow Xiaochun full concentration and to raise some money should Xiaochun be selected to participate in the international competition. In an effort to infuse passion and emotion into Xiaochuns playing, Professor Yu tells the boy the truth about his birth. However, this only strengthens Xiaochuns love for his father. Just before the selection trial, Xiaochun, who has been chosen to participate, relinquishes his spot to Lin Yu after she reveals to him that Professor Yu had secretly bought Xiaochuns original violin from the shop to stop Xiaochun from being distracted by thoughts of it. Xiaochun then takes the violin and runs after his father. The two finally reunite at the train station. In place of fame and success, Xiaochun chooses to be together with his father.

==Cast==
* Tang Yun as Liu Xiaochun, a thirteen-year-old violin child prodigy. Tang, a young violinist in real life, was found by director Chen Kaige at a violin competition in Shenyang. 
* Liu Peiqi as Liu Cheng, Xiaochuns father, a cook from a small southern town. Chen Hong as Lili, Xiaochuns friend, a young attractive woman who lives off the men she dates.
* Wang Zhiwen as Professor Jiang, a poor but proud teacher from the Childrens Palace (China)|Childrens Palace. 
* Chen Kaige as Professor Yu Shifeng, a high-profile professor in the Central Conservatory of Music.
* Cheng Qian as Hui, Lilis smooth-talking, cheating boyfriend
* Zhang Jing as Lin Yu, a young and talented student of Yus.
* Li Chuanyun (Babeli) as Tang Rong, a famous and former talented student of Yus. He also performed all of the violin solos for the film. Walters, Mark.  , BIGFANBOY.com, 2003-06-04. Retrieved on 2009-02-14. 

==Production==
Director  , 2003-05-30. Retrieved on 2007-04-13. 

==Awards and nominations==
* San Sebastian International Film Festival, 2002
** Best Director (Silver Seashell) — Chen Kaige
** Best Actor (Silver Seashell) — Liu Peiqi
* Golden Rooster Awards, 2002
** Best Director — Chen Kaige (in conjunction with Yang Yazhou for Pretty Big Feet)
** Best Supporting Actor — Wang Zhiwen
** Best Editing — Zhou Ying
** Best Film (nominated)
** Best Actor (nominated) — Liu Peiqi
* Huabiao Awards, 2003
** Outstanding Actor — Liu Peiqi
* Florida Film Festival, 2003
** Best International Feature Film
* Hong Kong Film Awards, 2004
** Best Asian Film (nominated)

==DVD release==
Together was released on DVD on November 18, 2003 and distributed by MGM in the United States. The DVD release features subtitles in English language|English.

==Notes==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 