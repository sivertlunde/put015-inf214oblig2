Love Sex aur Dhokha
 
 
{{Infobox film
| name           = Love Sex Aur Dhokha
| image          = Love_sex_aur_dhokha_Image.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Dibakar Banerjee
| producer       = Ekta Kapoor Shobha Kapoor  Priya Sreedharan

| writer         = Dibakar Banerjee Kanu Behl
| starring       = Anshuman Jha Nushrat Bharucha Rajkummar Rao
| music          = Sneha Khanwalkar
| cinematography = Nikos Andritsakis
| editing        = Namrata Rao
| studio         = 
| distributor    = ALT Entertainment Balaji Motion Pictures
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 1.5 crore
| gross          = 7 crore
}} anthology film found footage style. The film is a satire on the way television news media has been turning into cheap entertainment, feeding social voyeurism without taking any significant moral or ideological stance. 
 honour killings, MMS scandals, and sting operations,  which have become recurring phenomena of TV news in India, reported on in a lurid language, accompanied by flashing headlines, and dramatic music which simultaneously sensationalises and trivialises the very serious issues involved: caste-ism in nouveau riche India, sexual privacy and blackmail by media.

==Plot==
The movie begins, like Oye Lucky! Lucky Oye!, with an over-the-top parody of a quasi-news program which promises to bring you three exciting "stories", which are the inter-connected episodes of the film, namely, love, sex and deceit (dhokha).

===Love (Titled Superhit Pyaar)===
Rahul, a 20-something youngster infatuated with the candy-gloss of Bollywood romances, is a wannabe director who decides to shoot a small budget film to enter a contest hoping to meet his idol, Bollywood director Aditya Chopra. During auditions, he falls in love with the young woman he picks to be the leading actress, Shruti. She belongs to an orthodox, nouveau rich New Delhi family, and her father is a real estate magnate who is lampooned for his philistinism. The two bond, become closer, and define their relationship by confessing their love for each other. However, Shrutis overprotective and aggressive brother overhears their telephone conversation and attacks the movie set to find out the identity of the guy who loves Shruti to no avail. Shruti and Rahul elope. From their honeymoon suite, they call Shrutis family for their love and acceptance. Shrutis father and brother are initially furious but then approve and tell them they will be sending a car to pick them up from the hotel. During the car ride, the couple are ambushed by Shrutis brother and his hired goons, who beat them up in a graphic scene with hockey sticks, and then brutally cut their bodies using an axe, burying the dismembered parts under a railway culvert. The episode ends with the murderers casually sharing booze and joking with the accomplice saddled with the task of digging.

===Sex (Titled Paap ki Dukkaan)===
Shrutis friend Rashmi works night shifts in a local supermarket to support her family. Adarsh is a 20-something supermarket supervisor who has obtained the job due to his family connections with the stores owner and has outstanding debts to settle with the local committee. He makes a pact with one of his friends to make a sex tape with one of the employees to sell for a large amount of money to the media. Adarsh sets his sights on Rashmi, ultimately resulting in him developing genuine feelings for her. He decides to back out of the MMS plan, but his feelings for Rashmi are suppressed by his greed. Rashmi receives news regarding Shruti and Rahuls horrible death and is deeply saddened. Taking advantage of her vulnerability, he has sex with her and captures it in the shops security camera.

===Dhokha (Titled Badnaam Shohorat)===
Prabhat, an investigative reporter who uses spy cams to establish the sting operations is in desperate need of a groundbreaking story to get a bonus from the news company which employs him. He saves Naina (short for Mrignaina), an aspiring dancer, from drowning when she jumps from a bridge. Naina, who is initially furious at Prabhat, plans a sting operation with him to exact revenge on Loki Local, a music icon who asked her to trade sex for the position of leading dancer in his upcoming music video. The duo bring the sting footage to the media, who provoke them to plan another sting: Naina will supposedly blackmail Loki by threatening to reveal the initial footage in an attempt to catch him red-handed as he tries to bribe her &mdash; to render any false accusations by Loki of fabrication of footage. Naina meets Loki in the supermarket where Rashmi works to carry out the sting, while Prabhat watches closely. The plan goes awry when Loki tries to steal the camera and shoots Prabhat, who is immediately admitted into the hospital. Naina comes to meet Prabhat to hand over the footage so that he can get the bonus, but he decides to protect Nainas dignity and refuses to give his superiors the footage and resigns. However, it is revealed that Naina shockingly betrayed Prabhat by accepting the role as dancer in Lokis new music video, "Love, Sex Aur Dhokha," which is a parody on the events of the sting operation.

The end titles roll over the supposed music video footage.

==Cast==
* Anshuman Jha as Rahul
* Nushrat Bharucha as Shruti
* Sandeep Bose as Shrutis Dad
* Rajkummar Rao as Adarsh
* Neha Chauhan as Rashmi
* Arya Banerjee as Naina
* Herry Tangdi as Loki Local
* Amit Sial as Prabhat
* Atul Mongia as Atul
* Ashish Sharma as Shahid

==Reception==

===Critical reception===

The film opened to critical acclaim. Critics applauded the unique filming style, acting, realistic story and superb editing and received an aggregate rating of 7.5/10 at ReviewGang.   

Taran Adarsh of Bollywood Hungama reviewed positively saying, "On the whole, Love Sex aur Dhokha is original, innovative and ground-breaking cinema, which will shock and provoke you. The film is definitely not for the faint-hearted or those who swear by stereotypical fares, but for those who yearn for a change," and rated it 4 out of 5 stars.  

Rajeev Masand of CNN IBN rated it 4 out of 5, stating "Dibakar Banerjees Love, Sex aur Dhoka is the most riveting Hindi film in recent memory.   You will be shocked, you will be startled, but walking out of the theatre, you know you have just seen what is possibly the most important Hindi film since Satya and Dil Chahta Hai. Not only does it redefine the concept of "realistic cinema", it opens a world of possibilities in terms of how you can shoot films now."  

Subhash Jha of Bollywood Hungama commented on it, stating, "The film never belittles or sentimentalizes the characters lack of choices. While inventing a unique format of cinematic expression Dibakar Banerjee has not emotionally emasculated the characters.   In terms of technique this film gets as rough and jolting as any film can. The actors look like reality-show rejects making a last-bid attempt to prove their worth."  

Martin DSouza of glamsham.com rated it 4 out of 5 and said, "What is very smart is the way Banerjee weaves the lives of all three couples to make it one smart movie. At first, it appears as three separate issues but the end surprises. Intelligent cinema. Its real and its scary."  

Sarita Tanwar of Mid Day, gives it a 4-1/2 out of 5 and stated, "LSD has all the makings of a contemporary cult classic, this seasons best film. Its a must-watch."  

Gaurav Malani of India Times gives it a four out of five and stated, "At several levels, Love Sex aur Dhokha is at par with some of the best titles in world cinema in terms of its treatment."  

Nikhat Kazmi of Times of India gave a three-and-a-half to the film and stated, "Dont expect time-pass entertainment. Think beyond run-of-the-mill and see how Ekta Kapoor re-invents herself as the producer of contemporary Indian cinemas first full-blown experimental film."  

Raja Sen, of Rediff.com, gave it a perfect five and stated "Its bleak, bittersweet, funny and markedly unglamorous, and yet you come out humming the theme tune, your head blown clear off your shoulders."  

Mayank Shekhar of Hindustan Times gave a three-and-a-half and stated it as "truly an experiment" in Bollywood. 

===Commercial reception===
The films total budget was Rs.1 to 1.5 crore (INR 10 to 15 million). It grossed Rs.5.8 crore (INR 58 million) on first week and finished its run with Rs.7 crore (INR 70 million) making it a highly profitable venture. 

==Controversies==
Love Sex aur Dhokha has been censored: A seven-minute bare-back love-making scene was shortened and blurred.   

A reference to caste in the love story between a low-caste boy and a high-caste girl has been excised, as in the movie the boy is referred to a "special case" (SC) which is an allusion to a candidate of reservations. Originally, in place of "special case" was an explicit statement of caste which was subsequently censored.
 Film Censor Board of India.

==Soundtrack==
{{Infobox album
| Name = Love Sex aur Dhokha
| image= 
| Type = Soundtrack
| Artist = Sneha Khanwalkar
| Cover =
| Released =  
| Recorded = Feature film soundtrack
| Length = 
| Label = Sony Music
| Producer =
| Last album = Oye Lucky! Lucky Oye!   (2008)
| This album = Love Sex aur Dhokha
| Next album = Bheja Fry 2   (2011)
}}

The soundtrack was composed by Sneha Khanwalkar. The album has most of the songs sung by Kailash Kher.

{{track listing
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 
| title1 = I Cant Hold It
| lyrics1 =
| extra1 = Sneha Khanwalkar
| length1 = 2:49
| title2 = LSD Remix
| lyrics2 =
| extra2 = Kailash Kher
| length2 = 3:31
| title3 = LSD Title Track
| lyrics3 =
| extra3 = Kailash Kher
| length3 = 4:54
| title4 = Mohabbat Bollywood Style
| extra4 = Nihira Joshi, Amey Date
| length4 = 3:50
| title5 = Na Batati Tu
| extra5 = Kailash Kher
| length5 = 5:17
| title6 = Tainu TV Per Wekhya
| extra6 = Kailash Kher
| length6 = 2:53
| title7 = Tauba Tauba (Remix)
| extra7 = Kailash Kher
| length7 = 3:35
| title8 = Tu Gandi
| extra8 = Kailash Kher
| length8 = 3:09
}}

=== Reception ===
The soundtrack album was well received by critics and the public alike. Samir Dave of planetbollywood.com reviewed the album as "It’s just as wild ‘n’ crazy as unbridled kinky passion can be and most definitely worth multiple…listens".  Satyajit of Bollywood Trade News Network stated "LOVE SEX AUR DHOKHA makes bold musical statement with music that varies from shades from earthy folksy textures to groovy funky beats"  and rated it 2.5/5.

==Awards==
;56th Filmfare Awards 

;Won Best Editing - Namrata Rao Best Sound Design - Pritam Das
* R. D. Burman Music Award - Sneha Khanwalkar

;Nominated Best Screenplay - Dibakar Banerjee & Kanu Behl Best Cinematography - Nikos Andritsakis
    
;6th Apsara Film & Television Producers Guild Awards
* Won, Apsara Award for Best Sound Recording - Pritam Das
* Nominated, Apsara Award for Best Screenplay - Kanu Behl and Dibakar Banerjee
WON - Stardust Film Of the year - Ekta Kapoor

==See also==
* Found footage (genre)
* Anthology film

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 