1857 (film)
{{Infobox film
| name           =1857 
| image          = 1857_Surendra.jpg
| image_size     = 
| caption        = Surendra in 1857
| director       = Mohan Sinha
| producer       =
| writer         = M. Zahur
| narrator       =  Surendra Suraiya Wasti Madan Puri
| music          = Sajjad Hussain
| cinematography = V. N. Reddy
| editing        = Manhar Prabhu
| distributor    = 
| released       = 1946
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1946 Indian Bollywood film. It was the fifth highest grossing Indian film of 1946.  The film was directed by Mohan Sinha for Murari Pictures. The story was by M. Zahur with screenplay and dialogue by Safdar Ah. The film starred the singer-actor pair of Surendra and Suraiya,    along with Wasti, Nigar, Munshi Khanjar, Madan Puri.

1857 was a historical fiction drama set against the backdrop of the Indian Rebellion of 1857 also known as the First War of Indepence.    

==Cast== Surendra as Aslam
* Suraiya as Tasnim
* Wasti as Nawab Agha
* Benjamin as Jawahir Singh
* Shyam Sunder as Manzoor
* Nigar as mother of Manzoor
* Munshi Khanjar as father of Manzoor
* Madan Puri as Prince Mirza Mughal
* Menka Devi as Begum
* Laxmi as Queen of Jhansi
* Lila as sister of Manzoor

==Soundtrack==
The music composer was Sajjad Hussain with lyricists were Anjum Pilibhiti, Y. N. Joshi, Shewan Rizvi, Pandit Ankur.    The singers were Suraiya, Surendra and Shamshad Begum, Khan Mastana and Rajkumari.   

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer !! Lyricist
|- 
| 1
| "Meri Ho Gayi Unse Baat"
| Suraiya
| Pandit Ankur
|-
| 2
| "Dilli Tere Kile Par"
| Suraiya
| Shewan Rizvi
|-
| 3
| "Umido Ka Tara Kismat Pe"
| Suraiya
| Anjum Pilibhiti
|-
| 4
| "Gham-e-Aashiyana Satayega Kab Tak" 
| Suraiya
| Mohan Singh
|-
| 5
| "Teri Nazar Mein Main Rahun"
| Surendra, Suraiya
| Mohan Singh
|-
| 6
| "Jhamak Jhamak Liye"
| "Shamshad Begum"
|
|-
| 7
| "Chupke Hi Chupke Na Jaana"
| Rajkumari, Khan Mastana
| Anjum Pilibhiti
|-
| 8
| "Woh Pehli Mulaqat
| Surendra
| Pandit Ankur
|}

==References==
 

==External links==
*  
*   of 1857 At Surjit Singhs posted songs

 
 
 
 


 