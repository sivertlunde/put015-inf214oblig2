Shri Krishnavataram
{{Infobox film
| name           = Shri Krishnavataram
| image          = 
| image_size     =
| caption        =
| director       = Kamalakara Kameshwara Rao
| producer       = Atluri Pundarikakshaiah
| writer         = Tirupati Venkata Kavulu (poems) Samudrala Raghavacharya (dialogues)
| narrator       = Rajanala Chittor V. Nagaiah Mudigonda Lingamurthy
| music          = T. V. Raju Annayya
| editing        =
| studio         =
| distributor    =
| released       = 1967
| runtime        = 211 minutes
| country        = India
| language       = Telugu
| budget         =   10 lakhs
}}
Sri Krishnavataram ( . The magnum opus mythological film is based on Hindu epics Mahabharata and Bhagavata Purana covering major life events of Lord Krishna. 

==The story==
The movie covers all episodes of Lord Krishnas life. Krishnas birth, his captive parents and the prediction that Kamsa would be killed by Devakis eighth child makes the first episode. After defeating Kamsa, he marries Rukmini, fights with Jambavantha to procure the Samanthaka Mani and gets his daughter Jambavanthis hand in marriage. Satrajit, who initially proclaims that Krishna has the Mani regrets his mistake and gives his daughter Satyabhama in marriage to Krishna.

An example of Bhakti and devotion to friendship like Kuchelopakhyanam is also pictured beautifully.

After Sisupala completes his 100 sins, he defeats and kills him, thus regaining his entry back to Vishnus abode. Kuchelas episode is followed by Draupadis humiliation where Krishna saves her honour. The great war of Kurukshetra takes place after the negotiations (Rayabharam) between the cousins fails. Krishnas Geetopadesam to Arjuna is also shown. Finally he saves Bheema from Dhritarastras ire after Bheema kills Duryodhana. Balarama wades deep into the ocean after the gory war, while Lord Krishna ends his Avataram owing to a hunters mistake.

==Cast==
*Nandamuri Taraka Rama Rao  as  Lord Vishnu / Lord Krishna
*Devika  as  Lakshmi / Rukmini
*Krishnam Raju
*M. Prabhakar Reddy  as    Balarama
*Ramakrishna  as  Arjuna
*Kaikala Satyanarayana  as  Duryodhana Kuberudu
*Kanchana Kanchana  as  Satyabhama
*Master Harikrishna  as  Bala Krishna
*Dhulipala Seetharama Sastry  as  Satrajit
*Rajanala Kaleswara Rao  as  Sisupala
*Mukkamala Krishna Murthy  as  Kamsa
*Mudigonda Lingamurthy  as   Shakuni Krishna Kumari  as  Lakshana Dharmaraju
*S. Varalakshmi  as  Draupadi
*Arja Janardhana Rao  as  Bheema
*Chhaya Devi|   as  Mother of Sishupala
*Chittor V. Nagaiah  as  Dhritarashtra
*Sukanya  as  Jambavati Kunti Devi
*L. Vijayalaxmi  as  Kalindi
*Geetanjali  as  Nagnajiti
*Sandhyarani  as  Mitravinda

==Crew==
* Director : Kamalakara Kameshwara Rao
* Writer : Samudrala Raghavacharya
* Producer : Atluri Pundarikakshayya
* Production company : Tarakarama Pictures
* Cinematography : Annayya
* Music Director : T. V. Raju
* Playback singers : Ghantasala Venkateswara Rao and P. Susheela.

==Soundtrack==
There are 10 songs and 25 poems in the film. Music score provided by T. V. Raju. 
* Adigo Alladigo
* Ededo Autundi Egisi Egisi Padutundi
* Tanuvuto (Geetopadesam) (singer: Ghantasala Venkateswara Rao)
* Jagamulanele Gopalude Naa Sigalo Poovavune (singer: P. Susheela and Ghantasala; Cast: NTR and Kanchana)
* Jayahe Krishnavatara (Singer: Ghantasala Venkateswara Rao; Cast: Sobhan Babu)
* Neecharana Kamalana Needaye Chalu (singers: Ghantasala (singer)|Ghantasala, P. Leela, P. Susheela; Cast: NTR, Devika and Kanchana)

==Boxoffice==
The film was successful in Karnataka more than in Andhra Pradesh and celebrated Silver Jubilee. Even in the second and third releases, it ran for more than 100 days.

==References==
 

==External links==
*  

 
 
 
 
 
 
 