The Coachman (film)
{{Infobox film
| name           = The Coachman
| image          = The Coachman.jpg
| caption        = Poster to The Coachman
| director       = Kang Dae-jin 
| producer       = Lee Hwa-ryong
| writer         = Lim Hee-jae
| narrator       = 
| starring       = Kim Seung-ho Shin Young-kyun
| music          = 
| cinematography = Lee Mun-baek
| editing        = Kim Hee-su
| distributor    = Hwa Seong Films Co., Ltd.
| released       =  
| runtime        = 
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
| film name = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Mabu
 | mr             = Mabu
 | context        = }}
}} 1961 South Golden Bear Award and won the Silver Bear Extraordinary Jury Prize.    

==Synopsis==
An elderly single father living with his adult children makes a living with a horse-drawn cart. He finds companionship with a neighbors maid. His older daughter is a deaf-mute married to an abusive man, and his younger son is rebellious. His older son passes the bar exam, and, recognizing his fathers loneliness, tries to help him marry the neighbors maid. 

==References==
 

==Bibliography==
* 
* 
* 
* 

 
 
 
 
 