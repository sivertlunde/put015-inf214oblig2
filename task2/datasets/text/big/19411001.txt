Feed 'em and Weep
{{Infobox Film
| name           = Feed em and Weep
| image          = Feed em and weep TITLE.JPEG
| image_size     = 
| caption        = Title card Gordon Douglas
| producer       = Hal Roach
| writer         = 
| narrator       = 
| starring       = 
| music          = Leroy Shield Marvin Hatley
| cinematography = Norbert Brodine
| editing        = William H. Ziegler
| distributor    = Metro-Goldwyn-Mayer
| released       = 7 May 1938
| runtime        = 10 42"
| country        = United States 
| language       = English
| budget         = 
}} short comedy Gordon Douglas.  It was the 166th Our Gang short (167th episode, 78th talking short, and 79th talking episode) that was released. {{cite news 
|url=http://movies.nytimes.com/movie/225694/Feed-Em-and-Weep/overview |title=New York Times: Feed em and Weep |accessdate=2008-09-21|work=NY Times | first=Mordaunt | last=Hall}} 

==Plot==
Its Mr. Hoods birthday, and he has been eagerly anticipating a quiet dinner at home with his family. Alas, Darla has invited a "few friends" to the celebration: Alfalfa, Porky, and Philip. The well-meaning trio drive poor Mr. Hood to distraction with loud and interminable choruses of "Happy Birthday, Mr. Hood", but this is nothing compared to the presents theyve brought: a frog, a duck, and a cat, all of which get into a noisy confrontation with the family dog. When the kids arent arguing over their favorite comic-strip characters, theyre busily devouring Mr. Hoods birthday dinner; the poor fellow doesnt even get a slice of his own cake! That is given to Percy, when he stops in to tell Alfalfa that he needs to come home. Mr. Hood, disgusted over the whole affair, declares hes going out to get a bite to eat, and leaves. So does the Gang, as they resume arguing over whether Tarzan or Flash Gordon is the strongest...

==Cast==
===The Gang===
* Darla Hood as Darla Hood Eugene Lee as Porky
* Carl Switzer as Alfalfa
* Philip Hurlic as Our Gang member
* Gary Jasgur as Junior
* Leonard Landy as Percy

===Additional cast===
* Johnny Arthur as Johnny Hood
* Wilma Cox as Mrs. Hood

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 