Love Today (film)
{{Infobox film
| name = Love Today
| image = 
| director = Balasekaran
| writer = Balasekaran Vijay Suvalakshmi Manthra Raghuvaran
| producer = R. B. Choudary
| editing = V. Jaishanker
| cinematography = Vijay Gopal Super Good Films
| distributor = Super Good Films
| music = Shiva
| released = 9 May 1997 
| runtime = 151 minutes
| country = India
| language = Tamil
| budget =  3.2 crore
}}
 Tamil Romantic Vijay and Karan and Telugu as Suswagatham (1998) starring Pawan Kalyan and Devayani.

==Plot==
Ganesh (Vijay (actor)|Vijay) is a college graduate who lives with his father, Chadrasekhar (Raghuvaran), who is a doctor. Ganeshs father is easy going and pampers his son with whatever he wants. Sandhya (Suvalakshmi) is a daughter of a very strict police officer Vasudevan (Rajan P. Dev), who is always suspicious that his daughter would have a love affair. Sandhya makes up her mind that she would never fall in love and disappoint her father and her ambition is to get a gold medal in her college degree.

One day Ganesh spots Sandhya and falls in love at first sight. Even though Sandhya catches a good glimpse of him, she tells her friend Fathima she likes no one. Ganesh goes to the bus stop everyday to see Sandhya. He tries to get the advice his friends Ravi (Sriman) and Peter (Karan) to convince Sandhya to marry him. Their advices go in vain as she does not care about him. She confronts Ganesh one day and tells him not to waste his time on her.

Ganesh does not give up and gets hold of Sandhyas friend, Preethi (Manthra (actress)|Manthra) and she tries to talk to Sandhya, but she is unmovable. Vasudevan becomes suspicious of Ganesh and Sandhya and uses physical force on him. He also verbally abuses Sandhya, even though she tries to explain her innocence. She is forced to leave town because of this.

Ganesh finds her whereabouts and leaves his Father to go out of town and find her. As he is roaming around trying to find her, his Father has an accident and passes away. His friends desperately try to find his whereabouts, but they cannot trace him. Peter ends up performing the last rites for Ganeshs father. Ganesh arrives and gets heart broken that he could not even perform the cremation for his father.

At this point, Sandhya realizes her love for Ganesh. She reveals her feeling for him and tells him that she loves him and wants to spend the rest of her life with him. The next day, Sandhya is found to be standing at the bus stop, waiting for Ganesh to come. Ganesh meanwhile, takes the recommendation that his father had written for him before his death and goes for his first interview.

==Cast== Vijay as Ganesh
* Suvalakshmi as Sandhya Manthra as Preethi
* Raghuvaran as Chandrasekhar Karan as Peter
* Rajan P. Dev as Vasudevan
* Bindu Panicker
* Dhamu as Siva Sriman as Ravi
* Bobby Bedi Heros friend

==Production==
In an interview around the time of release, Vijay mentioned that he signed the film to give the audience what they want&nbsp;— fights, fast songs and a message, mentioning he had similar success from Poove Unakkaga. 

==Soundtrack==
Soundtrack is composed by newcomer Shiva. His realname was Omar and he earlier composed an album. The lyrics were written by Vairamuthu, Vaasan, Vaigarai Selvan, Pattukottai Shanmuga Sundaram.

{{Infobox album|  

   Name        = Love Today
|  Type        = Soundtrack
|  Artist      = Shiva
|  Cover       =  
|  Released    = 1996
|  Recorded    = 1996 Feature film soundtrack
|  Length      = 30.27
|  Label       = Star Music
|  Producer    = Shiva
|  Reviews     =
|  Last album  = 
|  This album  = Love Today (1997)
|  Next album  = 
}}

==Track list==
{{Tracklist
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 30:27
| lyrics_credits  = yes
| title1      = Yen Pennendru (Duet)
| lyrics1     = Vairamuthu
| extra1      =  Mohammed Aslam, Bombay Jayashri
| length1     =  05:22
| title2      = Yen Pennendru (Solo)
| lyrics2     = Vairamuthu
| extra2      = P. Unni Krishnan
| length2     = 04:05
| title3      = Enna Azhagu
| lyrics3     = Vairamuthu
| extra3      = S. P. Balasubrahmanyam
| length3     = 04:57
| title4      = Kuppayi Kuppayi
| lyrics4     = Selvan
| extra4      = Yugendran
| length4     = 02:37
| title5      = Aalai Ethi Pogum
| lyrics5     = Vairamuthu
| extra5      = Mano (singer)|Mano, Kalpana
| length5     = 04:53
| title6      = Salamiya
| lyrics6     = Vaasan
| extra6      = Mano, Malgudi Subha
| length6     = 05:12 title7       = Monica Monica
| lyrics7     = Pattukottai Shanmuga Sundaram.
| extra7      = Suresh Peters, Fepi Mani
| length7     = 04:41

}}

==Release== Sun TV. The film was given a "U" certificate by the Indian Censor Board.

==Critical Reception== Vijay and Suvalakshmi both sparkle in their respective roles .  The film became one of several successful films in the romantic genre with Vijay featured in throughout the late 1990s.  The success of the film prompted the director and actor to come together again immediately for a film titled Priyamudan, however the title was later used by Vijay for a different project. 

==Remakes== Telugu as Aftab and Amisha Patel which was released in 2002.  Dwarakish remade the film in Kannada as Majnu starring his son Giri in lead role. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" class=sortable
|- bgcolor="#CCCCCC" align="center"|-

! Year
! Film
! Language
! Cast
! Director
|- 1998
|Suswagatham Telugu
|Pawan Kalyan, Devayani Bhimaneni Srinivasa Rao
|- 2002
|Majnu Kannada
|Giri Dwarakish Dwarakish
|- 2002
|Kya Yehi Pyaar Hai Hindi
|Aftab, Amisha Patel
|K. Murali Mohan Rao
|-
|}

==References==
 

 
 
 
 
 
 
 