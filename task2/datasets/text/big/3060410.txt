Temporada de patos
{{Infobox film
| name        = Temporada de patos
| image       = Temporadadepatos poster.PNG
| caption     = Temporada de patos theatrical poster
| director    = Fernando Eimbcke
| producer    = Jaime Bernardo Ramos Frida Torresblanco
| writer      = Fernando Eimbcke Paula Markovitch
| starring    = Daniel Miranda Diego Cataño Danny Perea Enrique Arreola Carolina Politi
| music       = Alejandro Rosso Liquits
| distributor = Warner Independent Pictures
| released    =  
| runtime     = 85 min.
| country     = Mexico
| awards      = AFI Fest  Ariel Award  Guadalajara Film Festival  MTV Movie Awards Mexico  Thessaloniki Film Festival
| language    = Spanish
| budget      = 
}} Mexican film. It is the first feature film by writer/director, Fernando Eimbcke, a former MTV Awards videoclip director.

After being successfully featured in national and international film festivals such as the Cannes Film Festival it was sold to distributors in six European countries. The movie has been praised in all of these festivals as well as by directors such as Alfonso Cuarón (Gravity) and Guillermo del Toro (Pacific Rim).

The movie is filmed in black and white and mostly takes place in one location, an old apartment in Tlatelolco (Mexico City)|Tlatelolco.

==Cast==
*Daniel Miranda as Flama
*Diego Cataño as Moko
*Danny Perea as Rita
*Enrique Arreola as Ulises
*Carolina Politi as Flamas mother

==Plot==
Flama (Daniel Miranda) and Moko (Diego Cataño) are two 14-year-olds who have been friends since childhood. One Sunday afternoon, Flama invites Moko to play videogames while his mother is not home. There they have everything they need to survive such as videogames, pizza delivery, sodas, manga pornography and... no parents. But when the power goes out what seemed like a regular day becomes an adventure.

==Awards==
*2004 AFI Fest, Grand Jury Prize for Fernando Eimbcke
*2004 Guadalajara Film Festival, FIPRESCI Prize for Fernando Eimbcke
*2004 Guadalajara Film Festival, Mayahuel Award - Best Actor for Enrique Arreola (Tied with Roberto Espejo for Puños rosas)
*2004 Guadalajara Film Festival, Mayahuel Award - Best Actress for Danny Perea
*2004 Guadalajara Film Festival, Mayahuel Award - Best Director for Fernando Eimbcke
*2004 Guadalajara Film Festival, Mayahuel Award - Best Original Score for Alejandro Rosso
*2004 Guadalajara Film Festival, Mayahuel Award - Best Screenplay for Fernando Eimbcke, Paula Markovitch
*2004 Guadalajara Film Festival, Mayahuel Award - Best Sound Design for Lena Esquenazi
*2004 Thessaloniki Film Festival - Best Director for Fernando Eimbcke Ariel Award, Golden Ariel
*2005 Ariel Award, Silver Ariel - Best Actor for Enrique Arreola
*2005 Ariel Award, Silver Ariel - Best Actress for Danny Perea
*2005 Ariel Award, Silver Ariel - Best Art Direction for Diana Quiroz, Luisa Guala
*2005 Ariel Award, Silver Ariel - Best Cinematography for Alexis Zabe
*2005 Ariel Award, Silver Ariel - Best Direction for Fernando Eimbcke
*2005 Ariel Award, Silver Ariel - Best Editing for Mariana Rodríguez
*2005 Ariel Award, Silver Ariel - Best First Work - Fiction for Fernando Eimbcke
*2005 Ariel Award, Silver Ariel - Best Original Score for Alejandro Rosso, Liquits
*2005 Ariel Award, Silver Ariel - Best Screenplay Written Directly for the Screen for Fernando Eimbcke
*2005 Ariel Award, Silver Ariel - Best Sound for Lena Esquenazi, Miguel Hernández and Antonio Diego
*2005 MTV Movie Awards Mexico, Favorite Actress for Danny Perea
*2005 Paris Film Festival, Special Jury Prize for Fernando Eimbcke

==External links==
* 
* 
* 
*  on Yahoo! Movies

 
 
 
 
 
 
 
 