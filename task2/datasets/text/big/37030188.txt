Left for Dead (2007 Western film)
{{Infobox film
| name           = Left for Dead
| image          = Left for Dead DVD.jpg
| alt            = 
| caption        = US DVD Cover
| image_size     = 250px
| director       = Albert Pyun
| producer       = Michael Najjar Cynthia Curnan
| writer         = Chad Leslie
| starring       = Victoria Maurette
| music          = Anthony Riparetti
| cinematography = Alejandro Millán
| editing        = Ken Morrisey
| studio         = Sophia Productions Findling Films
| distributor    = Lions Gate Films
| released       =  
| runtime        = 84 minutes
| country        = United States Argentina
| language       = English
| budget         = 
| gross          = 
}} Argentine Weird horror western film directed by Albert Pyun and starring Victoria Maurette.

== Synopsis ==
In Mexico in 1895, Clementine Templeton is obsessively tracking the wanted man known as Sentenza for deserting her and their infant child. In her travels, she happens upon a gang of former prostitutes led by Mary Black, whose young daughter was also impregnated and abandoned by Sentenza. They eventually locate their prey and corner him in a remote ghost town called Amnesty; which is haunted by the vengeful ghost of slain preacher Mobius Lockhardt, who has made a pact with the devil to remain as an earthbound spirit unable to travel beyond the borders of the towns cemetery and slaughtering any who trespasses. But there are many secrets surrounding the group and the town of Amnesty, and not everyones motives are what they appear to be on the surface. As bloody betrayals and misdeeds come back to haunt them, they must confront their pasts if they hope to escape Amnesty and the vengeful wrath of Mobius Lockhardt alive.

== Cast ==
* Victoria Maurette ... Clementine Templeton  
* María Alche Dora
* Soledad Arocena ... Cota  
* Andres Bagg ... Mobius Lockhardt  
* Janet Barr ... Mary Black 
* Javier De la Vega ... Blake  
* Adnen Helali ... Garrett Flaherty  
* Oliver Kolker ... Frankie Flaherty  
* Brad Krupsaw ... Conner Flaherty  
* Mariana Seligmann ... Michelle Black

== Production ==
The film was shot in 12 days, entirely in Argentina.  

== Awards ==
* 2007 - Best Director (Albert Pyun) - Estepona XIII. Costa del Sol Fantasy Film Festival  Buenos Aires Rojo Sangre Film Festival

== References ==
 

== External links ==
*  

 

 
 
 