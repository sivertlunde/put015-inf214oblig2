Paycheck (film)
{{Infobox film
| name           = Paycheck
| image          = Paycheck filmposter.jpg
| caption        = Theatrical release poster
| alt            = 
| director       = John Woo
| producer       =  
| writer         = Dean Georgaris
| based on       =  
| starring       = {{Plain list| 
* Ben Affleck 
* Aaron Eckhart 
* Uma Thurman 
}} John Powell
| cinematography = Jeffrey L. Kimball Christopher Rouse
| studio         =  
| distributor    =  
| released       =  
| runtime        = 119 minutes
| country        = United States
| language       = English
| budget         = $60 million
| gross          = $96,269,812
}} thriller film of the same name by science fiction writer Philip K. Dick. The film was directed by John Woo and stars Ben Affleck, Uma Thurman and Aaron Eckhart. Paul Giamatti, Michael C. Hall, Joe Morton and Colm Feore also appear.

==Plot== reverse engineer; he analyzes his clients competitors technology and recreates it, often adding improvements beyond the original specifications. To protect his clients intellectual property and himself, Jennings, with the aid of his friend Shorty (Giamatti), undertakes a memory wipe to remove knowledge of his engineering.

Jennings is approached by his old college roommate, James Rethrick (Eckhart), the CEO of the successful Seattle technology company Allcom. Rethrick proposes a lengthy three-year reverse engineering job to Jennings, requiring him to live on Allcoms secured campus until its conclusion but rewarding him handsomely with company stock. Jennings agrees and, after arranging for his long-term absence, arrives at Allcom, turns in his personal possessions, and is given a brief tour of the facility where he meets and flirts with botanist Dr. Rachel Porter (Thurman). He is injected with a long-term memory marker for the post-job memory wipe.

Three years later (June 2007), at the conclusion of the memory wipe, Jennings is sitting in Rethricks office, being thanked for a successful job. On returning home, Jennings finds that, although Allcom stock has become quite valuable (around $92 million), he had signed away his share near the end of his tenure. Furthermore, he finds that his personal possessions have been replaced with an envelope containing a random assortment of everyday items. Soon after, the FBI capture him; and Agent Dodge (Morton) interrogates him on charges related to the death of physicist William Dekker. Jennings is able to escape custody, finding that the items in the envelope can be used at the right time to evade capture. After warning Shorty of his plight, he finds the items pointing him to a café meeting with Porter. Rethrick, who has been watching Jennings movements, discovers a message to Porter about this meeting and sends a body double to take her place to try to recover the envelope. The real Porter shows up and helps Jennings escape from both the FBI and Rethricks men.
 nuclear strike. Jennings realizes he must have built this device based on Dekkers invention and, on realizing the horrors that will come, prepared the envelope, using the forecasts from the machine to allow his future self to return to Allcom and destroy the unit. Furthermore, as Rethrick shortly discovers, Jennings rigged the device to malfunction, preventing Rethrick from anticipating Jennings actions.
 tableau Jennings previously saw. When Jennings watch, taken from the envelope, beeps, Jennings ducks in time to avoid the FBI Agents bullet that then kills Rethrick. The machine is destroyed, and Jennings and Porter escape the FBI in the chaos. When Agent Dodge and his men arrive and investigate, they take sympathy and report Jennings killed in the destruction.

In the films conclusion, Jennings, Porter, and Shorty have opened a greenhouse nursery. Jennings recalls a fortune cookie note from the envelope and discovers one last act he had done with the machine, foreseeing the results of a $90 million lottery and leaving the winning ticket in Porters birdcage.

==Cast==
 
* Ben Affleck as Michael Jennings, reverse engineer
* Aaron Eckhart as James Rethrick, billionaire CEO
* Colm Feore as John Wolfe, Rethricks right-hand man
* Uma Thurman as Doctor Rachel Porter, biologist and Jenningss lover
* Paul Giamatti as Shorty, Jenningss friend
* Michael C. Hall as Special Agent Klein
* Joe Morton as Special Agent Dodge
* Peter Friedman as Attorney General Brown
* Ivana Miličević as Maya, the double for Rachel
* Kathryn Morris as Rita Dunne, female executive
* Krista Allen as Holographic Woman
* Christopher Kennedy as Doctor Stevens

==Trademarks==
 
The film features several of director Woos trademarks, including two Mexican standoffs, the appearance of a dove, and a hollow birdcage similar to the opening tea house scene of Hard Boiled.

==Reception==
===Critical response===
 
Paycheck was met by mixed critical reception.
Rotten Tomatoes gives the film a score of 27% based on 154 reviews, with an average rating of 4.7 out of 10. {{cite web
| url = http://www.rottentomatoes.com/m/paycheck/
| title = Paycheck
| work = Rotten Tomatoes
| publisher = Flixster
| accessdate = 2012-01-21
}}
 
Metacritic holds a score of 43/100 based on reviews from 34 critics. {{cite web
| url = http://www.metacritic.com/film/titles/paycheck
| title = Paycheck
| work = Metacritic
| publisher = CBS
}}
 

Scott Tobias of The A.V. Club gave the film a positive review, calling it a "smart thriller" and praising "Woos wonderful sense of timing and rhythm." {{cite web
| date = 
| author = Scott Tobias
| url = http://www.avclub.com/content/node/17431/print
| title = Review by Scott Tobias
| work = The A.V. Club
| publisher = The Onion
}}
 
Chris Barsanti of Film Threat also praised Paycheck, calling it "one of the more competent and reassuring action movies to come out for quite some time." 

Roger Ebert gave the film two stars (out of four), saying that he "enjoyed the movie" but felt that it "exploits   for its action and plot potential, but never really develops it." 
K. J. Doughton of Film Threat called the film "John Woo lite," and a "neutered variation on his earlier, superior works." 
James Berardinelli gave the film one and a half stars out of four, calling it "a bad film, complete with lackluster acting, brainless writing, and uninspired direction." 

===Awards===
 
The film won a Golden Raspberry Award for Worst Actor - Ben Affleck, which also included Gigli (film)|Gigli and Daredevil (film)|Daredevil. After asking why he did not get his trophy, he was presented the Razzie live on Larry King Live a week later, which he promptly broke. The broken Razzie was sold on eBay for enough money to cover the hall rental for the following years ceremonies. While hosting Saturday Night Live, Ben Affleck joked that he would have walked out of the premiere and asked for his money back until he realized he was in the movie.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 