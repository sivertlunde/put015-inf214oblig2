By Hook or by Crook (2001 film)
{{Infobox film
| name           = By Hook or by Crook
| image          = By Hook or by Crook (2001 film).jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Harriet Dodge Silas Howard
| producer       = Steakhaus Productions
| writer         = Harriet Dodge Silas Howard
| contributing writer     = Stanya Kahn
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2001
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
}}

By Hook or by Crook is a 2001 queer buddy film by writers/directors/actors Harriet Dodge and Silas Howard and produced by Steakhaus Productions. Stanya Kahn was a contributing writer. The film premiered at the Sundance Film Festival. 

==Plot==
By Hook or by Crook chronicles the tale of two unlikely friends who commit petty crimes as they search for a path to understanding themselves and the outside world. Silas Howard plays Shy, (a Transgender man) who leaves his small town after the death of his father, and heads to the big city to live a life of crime. Along the way, he encounters Valentine, a quirky adoptee, in search of his birth mother. An immediate kinship is sparked between these men and they become partners in crime. Suffering money troubles, emotional problems, and physical confrontations, the duo face their issues head on and learn to trust each other and support each other in pursuit of their goals.

== Companies ==
*Steakhaus Productions
*NGB

== Cast ==
*Silas Howard
*Harriet Dodge
*Stanya Kahn
*Carina Gia
*James Cotner
*Joan Jett
*Kris Kovic
*Maia Lorian
*Aldo Pisano
*Nancy Stone

==Score==

Carla Bozulich of the Geraldine Fibbers wrote the score for the film. The soundtrack also features a song that Carla Bozulich co-wrote with the Geraldine Fibbers, "Lilybelle",  that was later covered by Kiki and Herb.

==Awards==

LA Outfest
2001 Winner of Audience Award: Outstanding Narrative Feature, Harriet Dodge
2001 Winners of Grand Jury Award: Outstanding Screenwriting, Silace Howard and Harriet Dodge

Paris Lesbian Film Festival
2002 Winner of Audience Award: Best Film, Silace Howard and Harriet Dodge

Philadelphia International Gay & Lesbian Film Festival
2002 Winner of Jury Prize: Best Feature - Lesbian, Silace Howard and Harriet Dodge

SXSW Film Festival
2002 Winner of Audience Award: Narrative Feature, Silace Howard and Harriet Dodge

Seattle Lesbian & Gay Film Festival
2001 Winner of Award for Excellence: Best Female Director, Silace Howard and Harriet Dodge
2001 Winner of Award for Excellence: Best Narrative Feature, Silace Howard and Harriet Dodge

==External links==
*   at the Internet Movie Database
*  

 
 
 
 
 
 