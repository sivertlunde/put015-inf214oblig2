A Very Natural Thing
{{Infobox film
| name = A Very Natural Thing
| image = A_Very_Natural_Thing.jpg
| caption = Theatrical release poster
| director = Christopher Larkin
| producer = Christopher Larkin 
| writer = Joseph Coencas Christopher Larkin
| starring = Robert Joel Curt Gareth Bo White Anthony McKay Marilyn Meyers
| music = Gordon Gottlieb Bert Lucarelli Samuel Barber
| cinematography = C.H. Douglass
| editing = Terry Manning
| distributor = New Line Cinema
| country = United States
| released =  
| runtime = 80 min. English
| budget = 
}} public school teacher by day, whilst looking for true love in a gay bar by night. It was one of the first films about gay relationships intended for mainstream, commercial distribution. The original title of the film was For as Long as Possible. It was directed by Christopher Larkin and was released to lukewarm reviews in 1973 and given an R rating by the Motion Picture Association of America.

==Plot== unabashedly declaring, monogamous relationship, and David moves in with Mark. But when Mark wants to have sex with other men, the relationship starts to break down. He rejects the idea of modeling a gay relationship on heterosexual marriage, and he is irritated that David wants to "keep pushing this romantic thing." Mark would rather have an understanding that either of them can have sex with other men when they feel like it, but this ends up alienating them from each other. Mark refuses to say, "I love you" until David playfully wrestles with him and tells him, "Say it...again...once more for good measure." After a year, though, David realizes that the two of them are just marking time. The two go to Fire Island for a weekend in an attempt to spice up their relationship, and although David tries to please Mark by entering an orgy, he cant go through with it. After a fight, David temporarily moves in with his friend Alan, who gives David an objective perspective on what happened. In a later encounter with Mark at Coney Island, David finally realizes that there cant be a reconciliation, as Mark is more interested in sex than a romantic relationship. 

After a season of loneliness, David meets a divorced photographer named Jason (Bo White) at the 1973 Gay Pride rally which began the film. David and Jason go to Jasons apartment and talk. In Jason, a divorced dad, we meet another member of the gay community, one who was living a heterosexual life prior to coming out. He still socializes with his ex-wife, who goes with him on photo shoots. On a parental visit with their toddler son (P.J.) Jason tells his ex-wife that he is now seeing someone with whom he would be spending the upcoming Labor Day holiday. It appears that in Jason, David has found someone willing to pursue a romantic, committed relationship with him. Jason takes pictures of David while telling him things to say other than "cheese, and the film ends by showing the two men together splashing naked in the surf on Cape Cod.

==Cast==
* Robert Joel as David
* Curt Gareth as Mark
* Bo White as Jason
* Anthony McKay as Gary, Davids roommate
* Marilyn Meyers as Valerie, Garys fiancée
* Jay Pierce as Alan, Davids friend
* Barnaby Rudge as Langley
* A. Bailey Chapin as the minister
* Scott Eisman as a student
* Michael Kell as father of boating family
* Sheila Rock as mother of boating family
* Linda Weitz as Linda, their daughter
* Robert Grillo as Edgar, Davids friend
* Kurt Brandt as Edgars lover Charles
* George Diaz as Miguel, one of Alans lovers
* Deborah Trowbridge as Jasons ex-wife
* Jesse Trowbridge as P.J., Jasons son
* Vito Russo

==Critical reaction== Love Story relationship films until then. Earlier films were dominated by tales of gays and lesbians being outcasts of society, mentally disturbed or committing suicide; later films were sadly dominated by the emergence of AIDS. A Very Natural Thing thus represents a short period in time where gay liberation flourished, and filmmakers could explore relationships in much the same way that films with heterosexual characters did.

The film was one of the first mainstream films to show homosexuality as a valid and normal act of love, i.e. "a very natural thing," as it attempted to explore the options for gay couples in 1973, including footage of an actual Gay Pride celebration. Many heterosexual film critics felt that the films depiction of love between two men as romantic made the film automatically "an argument rather than an entertainment" (New York Post). The film showed a young gay couple going through many of the same rituals and facing many of the same challenges as a straight couple; and, for many heterosexual Americans.
 sexual liberation, gay liberation movement.

Larkin responded to the criticism by saying, "I wanted to say that same-sex relationships are no more problematic but no easier than any other human relationships. They are in many ways the same and in several ways different from heterosexual relationships but in themselves are no less possible or worthwhile". (The Celluloid Closet pg. 208, 1987). Incidentally, Vito Russo, who wrote The Celluloid Closet appears in A Very Natural Thing.

The film was not financially successful, and the director, Christopher Larkin, moved to California, where in 1981 he published the book The Divine Androgyne According to Purusha. Larkin committed suicide on June 21, 1988. {{Citation
  | last = Thompson
  | first = Mark
  | author-link = Mark Thompson (author)
  | title = Leatherfolk
  | publisher = Daedalus Publishing
  | year = 2004
  | url = http://books.google.com/?id=GmLNPjjDD7sC&pg=PA284
  | isbn = 1-881943-20-8}} 

==Availability==
The film was released in VHS in 1996, it has recently been released on DVD: a 25th Anniversary edition in 1999 by Waterbearer Films, Inc.

==References==
 

==External links==
*  
*  

 
 
 
 