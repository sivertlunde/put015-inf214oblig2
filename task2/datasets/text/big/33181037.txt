Dangerous Liaisons (2012 film)
{{Infobox film name           = Dangerous Liaisons  image          = Dangerousliaisons.jpg director       = Hur Jin-ho  producer       = Chen Weiming  writer  Yan Geling  based on       =   starring       = Zhang Ziyi Jang Dong-gun Cecilia Cheung  music          = Jo Seong-woo  cinematography = Kim Byeong-seo editing        = Nam Na-yeong distributed    = Zonbo Media  released       =   runtime        = 110 minutes country        = China language       = Mandarin budget         =            gross          =
}} the same adapted numerous Les Liaisons eponymous Hollywood Valmont (1989), Cruel Intentions (1999), and Untold Scandal from South Korea (2003). 
 1930s Shanghai and stars South Korean actor Jang Dong-gun and Chinese actresses Zhang Ziyi and Cecilia Cheung.      
 2012 Busan International Film Festival.   

==Plot==
Shanghai, September 1931: Wealthy businessman and serial seducer Xie Yifan (Jang Dong-gun) is introduced to his uncles granddaughter, Du Fenyu (Zhang Ziyi), when his maternal grandmother, Du Ruixue (Lisa Lu), arrives at his apartment one day. Fenyu, a young widow whos just arrived from Northeast China (aka Manchuria) where the Japanese are making incursions, is staying at the country home of Madam Du, her grand-aunt.

At a glitzy fund-raiser for refugees thrown by Hudong Bank chairwoman Mo Jieyu (Cecilia Cheung) at Yifans nightclub, Jieyu, an old friend of Yifan whos never succumbed to his advances, asks him to rob Beibei (Candy Wang), the 16-year-old fiancee of tycoon Jin Zhihuan (Zhang Han), of her virginity. Jieyu wants revenge on Jin, for publicly dumping her in favor of a schoolgirl. Yifan turns down Jieyus request, partly because he has another quarry in his sights—the quiet and retiring Fenyu. Sensing an opportunity for some sport, Jieyu makes Yifan a wager: if he can seduce Fenyu without falling in love, she will finally agree to sleep with him; if he fails, he will sign over a valuable piece of land to her. Yifan accepts the challenge, but finds the virtuous Fenyu apparently immune to his charms.

Meanwhile, Jieyu employs a different strategy to get her revenge on Jin, encouraging an attraction between Beibei and her young drawing teacher, college student Dai Wenzhou (Shawn Dou). Despite Jieyus strenuous efforts, the relationship is never consummated; but when she finds out about it, Beibeis mother, Mrs. Zhu (Rong Rong), forbids her daughter to see Wenzhou anymore. With time running out, Jieyu suggests to Mrs. Zhu that Beibei should spend some quiet time at Madam Dus estate—and secretly arranges for Yifan to be there, to "comfort" Beibei. Mission finally accomplished, Yifan refocuses on seducing Fenyu, but finds himself in deeper emotional waters than hes ever experienced. 

==Cast==
*Zhang Ziyi as Du Fenyu
*Jang Dong-gun as Xie Yifan 
*Cecilia Cheung as Mo Jieyu
*Shawn Dou as Dai Wenzhou
*Lisa Lu as Madam Du Ruixue
*Rong Rong as Mrs. Zhu
*Candy Wang as Beibei, Mrs. Zhus daughter
*Ye Xiangming as Wu Shaopu, the demonstrator
*Xiao Shuli as Gui Zhen
*Zhang Yun as Wen, Mrs. Zhus maid
*Wu Fang as Hong, Jieyus maid
*Chen Guodong as Gen
*Zhang Han as Jin Zhihuan
*Xue Wei as young lady
*Hao Yifei as teacher
*Zong Xiaojun as police captain
*Yang Fan as policeman
*Gang Xiaoxi as dance girl
*Zhang Zichen as Cai Lu, Yifans driver
*Piao Yanni as make-up woman
*Yan Hongyu as photographer
*Jiang Yiyi as manicure maid
*Dong Hailong as reporter
*Son Seong-jae as saxophonist
*Leng Haiming as art director
*Yang Chen as MC
*Xiang Dong as lawyer
*Yin Yanbin as Japanese officer
*Jean Favie "Ji En" as the French tailor
*Li Shiping as young street beggar

== References ==
 

==External links==
*http://danger2012.kr/
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 