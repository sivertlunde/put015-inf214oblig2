Sing Sinner Sing
{{Infobox film
| name           = Sing Sinner Sing
| image          = SingSinnerSingDVDCover.jpg
| image_size     =
| caption        = DVD cover for film
| director       = Howard Christie
| producer       = Larry Darmour (executive producer) Phil Goldstone (producer)
| writer         = Wilson Collison (play Clip Joint) Edward T. Lowe Jr. (adaptation and screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Ira H. Morgan
| editing        = Otis Garrett
| distributor    =
| released       =
| runtime        = 74 minutes 66 minutes (American DVD)
| country        = USA
| language       =
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Sing Sinner Sing is a 1933 American film directed by Howard Christie.

==Plot summary==
 
Leila Hyams plays a singer who is accused of her husbands murder. At the time the movie was released, it was recognized as being loosely based on the 1932 Libby Holman-Zachary Smith Reynolds case.  

==Cast==
*Paul Lukas as Phil Carida
*Leila Hyams as Lela Larson
*Don Dillaway as Ted Rendon
*Ruth Donnelly as Margaret "Maggie" Flannigan
*George E. Stone as Spats
*Joyce Compton as Gwen
*Jill Dennett as Sadie
*Arthur Hoyt as Uncle Homer
*Paul McGrail as Louis
*Gladys Blake as Cecily Gordon
*Arthur Housman as Jerry
*Edgar Norton as Roberts the Butler
*John St. Polis as James Parks
*Stella Adams as Aunt Emily van Puyten Pat OMalley as Henchman Conley
*Walter Brennan as Henchman Riordan
*Walter Humphrey as Uncle Johann van Puyten

Various artists, such as Lionel Hampton and Marshal Royal, can be seen playing in bands in the film. 

==Soundtrack==
 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 