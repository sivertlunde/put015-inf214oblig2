Victoria No. 203
 
{{Infobox film
| name           = Victoria No. 203
| image          = Victoria No. 203.jpg
| image_size     = 
| caption        = 
| director       =Brij
| producer       =Brij
| writer         = 
| narrator       =  Pran Ashok Kumar music          = Kalyanji Anandji
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}} Anwar Hussain, Pran play key supporting roles and earned the Filmfare nominations for the film.  Telugu remake Laxmi of Julie (1975) fame and Shobhan Babu and also a Tamil remake called Vairam with Jaishankar and Jayalalithaa.    A Hindi remake of the film produced by Brijs son Kamal Sadanah was released in August 2007.

==Summary==

When Seth Durgadas steals some precious diamonds, one henchman turns traitor and decamps with the booty. The man is killed, but the diamonds are not found. A Victoria driver is wrongly sentenced to lifer since he was the last man to be seen with the dead man. Here, two crooks get the scent of the diamonds. The drivers daughter too is poised to prove her fathers innocence. Durgadass son Kumar too joins the forces after learning his true nature. Will this weird team find the diamonds? What surprises does everybody have in store for them?

==Plot==

Seth Durgadas is a rich businessman, revered by his son Kumar and society. In reality, Durgadas is the leader of a smuggling gang. As a trusted member of society, he gets all information, while his gang executes his plans. However, during one such robbery, a gang member turns greedy and flees with the diamonds. Durgadas sends another man to retrieve the diamonds and kill the traitor. The traitor is killed, but the killer too flees with the diamonds.
 Victoria driver of the eponymous Victoria, who was found near the dead body, is arrested based on circumstantial evidence. Somewhere else, two old golden-hearted crooks, Raja and Rana are to be released. Rana had an infant son who was kidnapped from a park. To date, Rana doesnt know who kidnapped him or whether his son is even alive.

The duo want to spend the rest of their lives as good, respected men. The plan is short lived when they find themselves on the trail of the diamonds. Soon, they learn about the Victoria and realise that no one has got a scent of the missing diamonds. To get closer to the Victoria, they pose as distant cousins of the Victoria driver, who are reluctantly admitted inside by the drivers elder daughter Rekha, who soon starts acting suspiciously.

During the day, Rekha rides the Victoria, posing as a man while during the night, when everyone is supposedly sleeping, she slips out of the home. One day, Raja and Rana follow her to the house of a man, who unknown to them, is Durgadass henchman. They are shocked to see Rekha seducing him. But when the henchman tries to rape her, the duo save her. On accosting her, she tells that the man was loitering suspiciously near the Victoria when her father was arrested. Before they can know anything from him, he is shot dead.

On learning the Victoria drivers story, the duo decide to do one good turn. They tell Rekha the whole truth, but a shocked Rekha doesnt have a clue about the location of diamonds. Even after tearing the Victoria apart, nothing is found. Meanwhile, Rekha falls in love with Kumar, whom she met while posing as a cab driver. She tries to woo him posing as a rich lady, a plan which works well. However, Kumar learns the truth and decides to marry her, despite knowing about her father.

Kumar also meets Raja and Rana, whereupon he gets suspicious of the duo. Here, Durgadas objects to Kumars relationship with Rekha. Later, Kumar learns of his fathers true nature and leaves him. After meeting Rekha, Raja and Rana tell the truth to him too. The quartet decide to find the diamonds. Here, Durgadas kidnaps Rekha on learning that she is the daughter of the Victoria driver. Later, Kumar, Raja and Rana are also kidnapped and tortured.

During this deal, Durgadass drinks are being served on an ornate tray with a fake Victoria headlamp attached to it. Suddenly, Rana realizes that the diamonds are hidden in the Victorias lamp. He buys time for himself and his old buddy, and they go to retrieve the diamonds. The duo turn up at Durgadass den, where they manage to create a confusion by luring the gang members to the diamonds. Durgadas is embarrassed to see that not one member of his gang is loyal to him.

During the melee, another twist occurs when an old henchman of Durgadas reveals that Kumar isnt Durgadass son, but Ranas. Durgadas had ordered the henchman to steal somebodys child because Durgadass father had threatened to disown him. Durgadas could have prevented the situation only by proving that Durgadas has fathered a child. In the end, Durgadas and his remaining cronies are arrested and Rekhas father is set free.

After being reunited with his son and his would be daughter-in-law, Rana decides to settle down. However, Raja decides that his destiny might have written something else for him and leaves. Soon, Raja spots a burkha clad woman and starts following her, only to find that the "woman" is actually Rana. Rana tells Raja that he will not let the latter spend the rest of his life without him. Raja relents and goes home with his friend.

==Cast==
*Navin Nischol as Kumar
*Saira Banu as Rekha Pran as Rana
*Ashok Kumar as Raja Anwar Hussain as Seth Durgadas
*Ranjeet as Bandit
*Mohan Choti as Hospital Wardboy
*Anoop Kumar as Havaldar Murli
*Chaman Puri as Rekhas Father
*Baby Gayatri as Munni, Rekhas Younger Sister
*Rajesh Khera as Karan

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Do Bechare Bin Sahare"
| Kishore Kumar, Mahendra Kapoor
|-
| 2
| "Tu Na Mile To Ham Jogi Ban Jayenge"
| Kishore Kumar
|-
| 3
| "Dekha Main Ne Dekha"
| Kishore Kumar
|-
| 4
| "Thoda Sa Thahro"
| Lata Mangeshkar
|}

==References==
 

==External links==
*  

 
 
 
 
 