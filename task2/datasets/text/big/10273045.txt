The Street Fighter's Last Revenge
{{Infobox film 
| name = The Street Fighters Last Revenge
| image = Streetfighterlastrevenge.jpg
| caption = 
| director = Shigehiro Ozawa
| producer = 
| writer = Kôji Takada Masahiro Shimura
| narrator = 
| starring = Sonny Chiba Reiko Ike Etsuko Shihomi
| music = 
| cinematography = Ken Tsukakoshi
| editing = 
| distributor = Toei Company Ltd. (Japan) New Line Cinema (USA, theatrical) Wizard Video (USA, home video)
| released = 
| runtime = 83 minutes
| country = Japan Japanese
| budget = 
}}
  is a 1974 martial arts film and the third in a series starting with The Street Fighter starring Sonny Chiba.  The film is currently in public domain.  

==Synopsis==

===Japanese version===
The film starts as Takuma Tsurugi retrieves Go Owada during a riot. After Tsurugi receives what is believed to be payment for bringing Go to them, he discovers that he had been paid in newspapers. Outraged, Tsurugi lashes out at the mobsters present and clashes with a young woman from Taiwan named Huo Feng. The Owada family is enraged that Tsurugi turned on them and decides to crush him.

Upon returning from a shareholders meeting at Tokyo Chemical, Seigan blackmails a politician, Mr. Iizuka, with a tape recording of a recent conversation just as Tsurugi ambushes them and steals the tape. Aya is inspired to hire a former Mafioso from Chicago, Frankie Black, after watching him tear apart a strong chain on live television. Upon hearing who her new partner is, Huo Feng turns on the Owada family and decides to align herself with Tsurugi. Meanwhile, Kunigami confronts Tsurugi after Aya outwits him following a dance at the local club, and he nabs the missing tape after defeating him at the local fairgrounds. Afterwards, Tsurugi turns to Master Masaoka to learn some Okinawa styles of martial arts and reclaims the tape after intercepting Kunigami as he returns home.

Tsurugi later encounters Huo Feng after she intercepts him in a tunnel. Unknown to them, Frankie Black and his gang had shown up to ambush them, and Black kills Huo Feng before retrieving the money Tsurugi had been paid for the blackmail tape. Kunigami then allies himself with Seigan to crush Tsurugi, who chases after some of the Owada familys goons, disguised as a motorcycle cop, and fights them at a car crushing plant. Seigan sends Aya after the money, which Tsurugi had hidden at Aoidani Funeral Home. She brings Black along to help with the retrieval. Tsurugi intercepts them, having cleverly hidden himself inside one of the twin retorts behind a conveniently-placed casket, and burns Black to death in the ensuing struggle. Aya then proceeds to seduce him.

Some time later, Go ambushes Tsurugi, and the two fight, giving Aya enough time to escape with the suitcases. Realizing he had been tricked again, Tsurugi follows Owada and his gang to a harbor as Kunigami confronts Seigan by himself, with Tsurugi taking out many of the Owada goons and Kunigami taking on one goon and Seigan. Unknown to the survivors, Tsurugi then puts a deadly surprise in the Owada family car. Aya is poisoned when Kunigami finds out she had tried to poison his drink, and Tsurugi and Kunigami have a final fight to the death which Tsurugi wins. The film ends as a critically wounded Tsurugi fails to save Aya from the booby trap he had placed earlier.

===American version===
The film starts as Terry Sugury retrieves Go Owada in the middle of a violent strike in which bitter workers at the largest chemical manufacturing plant in the East are engaged in combat against a riot police squadron. After Terry receives what is believed to be payment for the tape, he discovers that he had been paid in newspapers. Outraged, Terry lashes out at the mobsters present and clashes with a young assistant district attorney named Huo Feng who is also after the tape.

The tape turns out to be one half of a formula for synthetic heroin. The Owada family is enraged that Terry turned on them and decides to crush him. Aya is inspired to hire a Mexican mercenary, Frankie Black, after watching him tear apart a strong chain on live television. Meanwhile, Kunigami confronts Terry after Aya outwits him following a dance at the local club, and he nabs the missing tape after defeating him at the local fairgrounds. Afterwards, Terry turns to Master Masaoka for spiritual training and reclaims the tape after intercepting Kunigami as he returns home.

Terry later encounters Huo Feng after she intercepts him in a tunnel. Unknown to them, Frankie Black and his gang had shown up to ambush them, and Black kills Huo Feng before retrieving the money Terry had been paid for the tape. Terry chases after some of the Owada familys goons, disguised as a motorcycle cop, and fights them at a car crushing plant. Seigan sends Aya after the tape, which Terry had hidden at Aoidani Funeral Home. She brings Black along to help with the retrieval. Terry intercepts them, having cleverly hidden himself inside one of the twin retorts behind a conveniently-placed casket, and burns Black to death in the ensuing struggle. Aya then proceeds to seduce him.

Some time later, Go ambushes Terry, and the two fight, giving Aya enough time to escape with the suitcases. Realizing he had been tricked again, Terry follows Owada and his gang to a harbor as Kunigami confronts Seigan by himself, with Terry taking out many of the Owada goons and Kunigami taking on one goon and Seigan. Unknown to the survivors, Terry then puts a deadly surprise in the Owada family car. Aya is poisoned when Kunigami finds out she had tried to poison his drink, and Terry and Kunigami have a final fight to the death which Terry wins. The film ends as a critically wounded Terry fails to save Aya from the booby trap he had placed earlier.

==Alternate versions==
In addition to the plot changes, many scenes have been moved into different sequences. Much like the first U.S. release of The Street Fighter, some of the violence was cut to avoid the X-rating. One deleted scene involves Tsurugi ripping Kunigamis heart out of his chest, and another is Kunigamis finishing blow to Seigan Owadas head, which is shortened. Aside from a LaserDisc release in the U.S., the original cut in its entirety has been sold only in Japan, the United Kingdom and France.

== Cast ==
Note: English-translated names, if given or known, will be in parentheses.
 Shinichi (Sonny) Chiba - Takuma Tsurugi (Terry Sugury) 
* Reiko Ike - Aya Ōwada
* Eizō Kitamura - Seigen Ōwada
* Akira Shion - Gō Ōwada
* Kōji Wada - Takera Kunigami
* Masafumi Suzuki - Kendō Masaoka
* Etsuko Shihomi (Sue Shiomi) - Huǒ Fèng
* Dorian Howard - Frankie Black
* Willy Dosey - Wolf

==Notes==
 

== References ==
* http://movie.goo.ne.jp/movies/PMVWKPD28594/cast.html

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 