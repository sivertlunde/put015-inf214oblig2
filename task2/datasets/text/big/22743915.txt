Vinterland (film)
{{Infobox film |
 name =  Vinterland|
 image = |
 caption = Vinterland film poster |
 writer = Kjell Ola Dahl, Hisham Zaman|
 starring = Raouf Saraj, Shler Rahnoma|
 director = Hisham Zaman|
 producer = Turid Øversveen|
 composer = Øistein Boassen|
 released =  2007|
 runtime = 52 minutes |
 country = Norway |
 language = Kurdish language|Kurdish, Norwegian language|Norwegian|
 }}

  is a 2007 film directed by Hisham Zaman and produced in Norway.

==Plot==

Renas, a Kurdish refugee living in the northern part of Norway in a remote desolate place, is awaiting the arrival of his fiancée, Fermesk. The couple have never met before, and their first encounter at the airport does not meet up to their expectations.

==Awards==
#Best Actor (Årets mannlige skuespiller), Raouf Saraj, Amanda Awards, Norway, 2007.
#Grand Prix José Giovanni, Festival International Du Film De Montagne DAutrans, France, 2007. 

==See also==
*Cinema of Norway

==References==
 

==External links==
* 

 
 
 
 


 