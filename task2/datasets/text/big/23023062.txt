Apache Ambush
{{Infobox film
| name           = Apache Ambush
| image          = Apache Ambush.jpg
| image_size     =
| caption        =
| director       = Fred F. Sears
| producer       = Wallace MacDonald David Lang Bill Williams Richard Jaeckel
| cinematography = Fred Jackman, Jr.
| editing        = Jerome Thoms
| distributor    = Columbia Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
}} western film, Bill Williams and Richard Jaeckel.

==Plot== Civil War, Lincoln (James Don Harvey). Henry Repeating Rifles that Kingston has. Embittered ex-rebel leader Lee Parker (Richard Jaeckel) is also an obstacle to Kingstons mission, as he tries to get the cattle through Apache territory.

==Cast== Bill Williams - James Kingston  
*Richard Jaeckel - Lee Parker
*Alex Montoya - Joaquin Jironza
*Movita Castaneda - Rosita
*Adelle August - Ann Parker
*Tex Ritter - Traegar
*Ray "Crash" Corrigan - Hank Calvin
*Ray Teal - Sgt. Tim ORoarke Don Harvey - Maj. Donald Tex McGuire
*James Griffith - President Abraham Lincoln
*James Flavin - Col. Marshall
*George Chandler - Chandler
*Forrest Lewis - Sheriff Silas Parker
*George Keymas - Tweedy
*Clayton Moore - Townsman

==External links==
* 

 
 
 
 
 
 
 
 


 