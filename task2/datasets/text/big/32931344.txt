Mike and Stefani
 

{{Infobox film
| name           = Mike and Stefani
| image          =
| image size     =
| caption        =
| director       = Ron Maslyn Williams
| producer       = Stanley Hawes
| writer         = Ron Maslyn Williams Roland Loewe (dialogue)
| narrator       = Martin Royal Josephine ONeill
| starring       = Mycola Stefani Robert Hughes
| cinematography = Reginald G Pearse
| editing        = Inman Hunter R. Maslyn Williams Brereton Porter Dept of Immigration
| distributor    =
| released       =  
| runtime        = 64 minutes
| country        = Australia
| language       = English
| budget         = £5,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p212. 
}} Australian drama Film Division, Department of Ukrainian refugee couple who move to Australia. The film is in the style of the Italian neorealism movement. 

== Plot ==
 refugee camp and they live there for two and a half years. In 1949 the camps are being shut down so Mike and Stefani decide to immigrate to Australia. However, they first have to undertake a gruelling medical examination and interrogation by Australian immigration officers.

== Cast ==

* Mycola — himself
* Stefani — herself
* Ladu (Mycolas brother)
* Valerie Paling — herself
* Australian immigration official – Harold Grant

== Production ==
 forced labour in Germany.

Shooting took place over two months in the winter of 1949-50. The interview of Mike and Stefani by Australian Immigration official Harold Grant was real, with the family actually not yet assured of acceptance into Australia.  However they were successful and the crew followed them to Australia. Additional shots were taken at the Film Division headquarters at Burwood, New South Wales. 

== Release ==

Williams struggled to get the film commercially released.  It was mainly distributed through government film libraries although it did have a few commercial screenings and won a prize at a festival.   

== References ==
 

== External links ==

*  
*   at YouTube
*   at Senses of Cinema
*  
*   at ABC website
*  at Oz Movies

 
 
 