The Black Dahlia (film)
 
{{Infobox film
| name           = The Black Dahlia
| image          = Black dahlia ver264.jpg
| caption        = Theatrical Release Poster
| director       = Brian De Palma
| producer       = Art Linson Rudy Cohen Moshe Diamant
| writer         = Josh Friedman
| based on       =  
| starring       = Josh Hartnett Scarlett Johansson Aaron Eckhart Hilary Swank
| music          = Mark Isham
| cinematography = Vilmos Zsigmond
| editing        = Bill Pankow
| studio         = Millennium Films Nu Image Signature Pictures Universal Pictures
| released       =  
| runtime        = 122 minutes
| country        = United States
| language       = English
| budget         = $50 million http://www.boxofficemojo.com/movies/?id=blackdahlia.htm 
| gross          = $49,332,692 
}} crime thriller a novel Elizabeth Short Best Cinematography at the 79th Academy Awards but lost to Pans Labyrinth.

==Plot== LAPD officers Dwight Bucky Bleichert (Josh Hartnett) and Lee Blanchard (Aaron Eckhart), investigate the murder and dismemberment of Elizabeth Short (Mia Kirshner), soon dubbed The Black Dahlia by the press.
 John Kavanagh) and Ramona (Fiona Shaw).

Buckys partner, Lee, also becomes obsessed with Elizabeths murder. Lees obsession leads him to become erratic and abusive towards his long-time girlfriend Kay Lake (Scarlett Johansson), who is also one of Buckys close friends.  After Lee and Bucky have a nasty argument about a previous case, Bucky goes to Lee and Kays to apologize, only to learn from Kay that Lee was responding to a tip about a recently released convict, Bobby DeWitt.  Bucky goes to the location and gets into an altercation with DeWitt in the atrium of the building. DeWitt is gunned down by Lee, standing on the stairs across the atrium. Bucky sees a man sneak up behind Lee, wrapping a rope around Lees neck. Lee fights back while Bucky, paralyzed with shock, watches from across the atrium as a second shadowy figure steps out and slits Lees throat. Lee and the man holding the rope fall over the railing to their deaths several floors below.

Dealing with the grief of losing Lee propels Bucky and Kay into a sexual encounter.  The next morning, Bucky finds money from a bank robbery hidden in Lee/Kays bathroom.  Kay reveals that she had been DeWitts girlfriend, that DeWitt had mistreated her, and that DeWitt had done the bank robbery.  Lee had rescued Kay and stolen DeWitts bank robbery money.  Lee needed to kill DeWitt now that he was out of prison&mdash;leading to the encounter that resulted in Lees death.  Bucky leaves, furious with Lee and Kay for their actions and lies.  He returns to Madeleines family mansion and continues his intense relationship with her. Kay is furious when she discovers the relationship, especially with the fact that Madeleine bears a striking resemblance to the same girl Lee obsessed over before he was killed, and leaves the scene.
 Hollywoodland sign. In one of the empty houses, Bucky recognizes the set that was used to film Elizabeths pornographic movie. In a barn on the property, Bucky finds where Elizabeth was killed and her body butchered, as well as a drawing of a man with a Glasgow smile.  The drawing resembles a painting in Madeleines family home and matches the disfiguring smile carved into Elizabeths face during her murder.

Bucky confronts Madeleine and her father in their home, accusing them of murdering Elizabeth. Madeleines mother Ramona reveals that she was the one to kill Elizabeth, who looked so much like Madeleine.  She confesses first that Madeleine was not fathered by Emmett but rather by his best friend, George.  She further reveals that George had been on set when Elizabeths pornographic film was made, becoming infatuated with her.  Finally, she felt that Elizabeth looked too much like Madeleine, was bothered that George was going to have sex with someone who looked like his own daughter, and decided to kill Elizabeth first. Upon finishing her confession, Ramona kills herself.

A few days later, remembering something Lee had said during the investigation, Bucky visits Madeleines sister Martha with some questions.  He learns that Lee knew about the lesbian relationship between Madeleine and Elizabeth and was blackmailing Madeleines father to keep it secret.  Bucky finds Madeleine at a seedy motel, and she admits to being the shadowy figure who slit Lees throat.  Although she insists that Bucky wants to have sex with her rather than kill her, he tells her she is wrong and shoots her dead. Bucky later goes to Kays house.  Kay tells him to come in and closes the door as the film ends.

==Cast==
*Josh Hartnett as Police Officer Dwight Bucky Bleichert
*Scarlett Johansson as Katherine Kay Lake
*Aaron Eckhart as Police Officer Lee Blanchard
*Hilary Swank as Madeleine Linscott
*Mia Kirshner as Elizabeth Short Mike Starr as Detective Russ Millard
*Fiona Shaw as Ramona Linscott
*Patrick Fischler as Deputy DA Ellis Loew James Otis as Dolph Bleichert John Kavanagh as Emmett Linscott Troy Evans as Chief Ted Green
*Pepe Serna as Tomas Dos Santos
*Angus MacInnes as Captain John Tierney
*Rachel Miner as Martha Linscott
*Victor McGuire as Sgt. Bill Koenig
*Gregg Henry as Pete Lukins
*Jemima Rooper as Lorna Mertz
*Rose McGowan as Sheryl Saddon
*Steve Eastin as Detective
*Ian McNeice as Coroner
*Richard Brake as Bobby DeWitt William Finley as Georgie Tilden
*Joost Scholte as Madeleines GI
*Fatso-Fasano as Dealer

==Development and production== director and The Italian Job. Fincher originally envisioned "a five-hour, $80-million mini-series with movie stars." 

When De Palma became director, he replaced Wahlberg with Aaron Eckhart shortly before shooting began in April 2005. Black Angel is on the marquee.]] Pantages Theatre (and adjoining bar The Frolic Room) at Hollywood and Vine, and the Alto-Nido Apartments are perhaps the most recognizable landmarks. A standing set on the backlot of Nu Boyana Film Studios in Sofia, Bulgaria, was used to represent Leimert Park.

James Horner was originally on board the project to score the films music but in February, 2006 it was reported that Mark Isham had replaced him.
 The Man Who Laughs appear in this film.

==Reception==

===Critical response===
  shot for the film, showing a rainmaking rig, a sprinkler system used to create the appearance of rain on the set -- a commonly employed practical effect.]]
Highly anticipated by many after the success of L.A. Confidential (film)|L.A. Confidential, the film received mixed to negative reviews from film critic|critics. At Rotten Tomatoes, the film currently holds a 32% "Rotten" rating.

David Denby of The New Yorker described it as "a kind of fattened goose that’s been stuffed with goose-liver pâté. It’s overrich and fundamentally unsatisfying... There are scenes that display De Palma’s customary visual brilliance... (b)ut the movie is so complicated, the narrative so awkward, that when the pieces of the puzzle fall into place we get no tingle of satisfaction."  Peter Travers of Rolling Stone magazine commented "De Palma throws everything at the screen, but almost nothing sticks."   J. Hoberman of The Village Voice stated that the film "rarely achieves the rhapsodic (let alone the delirious)." 

However, Mia Kirshners performance as Elizabeth Short was praised by many critics.  Stephanie Zacharek of Salon.com, in a largely negative review, notes that the eponymous character was "played wonderfully by Mia Kirshner..."   Mick LaSalle wrote that Kirshner "makes a real impression of the Dahlia as a sad, lonely dreamer, a pathetic figure."  J. R. Jones described her performance as "haunting" and that the films fictional screen tests "deliver the emotional darkness so lacking in the rest of the movie." 

===Box office===
The film opened on September 15, 2006 in 2,226 theaters. It came in second place over its opening weekend (losing out to Gridiron Gang), with an estimated $10 million gross box office. It ended its theatrical run after domestically grossing $22,545,080, and grossing $26,787,612 in foreign theaters for a global total of $49,332,692. 

==References==
 

== External links ==
*  
*  
*  
*  
*  
*   - The New York Times, February 5, 2006

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 