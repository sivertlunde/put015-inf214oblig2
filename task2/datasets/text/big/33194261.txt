Sparkle (2012 film)
{{Infobox film
| name           = Sparkle
| image          = sparkle2012.png
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Salim Akil
| writer         = Mara Brock Akil
| based on       =  
| producer       = Debra Martin Chase TD Jakes Whitney Houston Curtis Wallace Salim Akil Mara Brock Akil
| writer         = Mara Brock Akil Howard Rosenman
| starring       = Jordin Sparks Whitney Houston Derek Luke Mike Epps Tamela Mann Carmen Ejogo Tika Sumpter Omari Hardwick Cee Lo Green
| music          = R. Kelly Curtis Mayfield Kier Lehman André DeJuan
| cinematography = Anastas N. Michos
| editing        = Terilyn A. Shropshire
| studio         = Stage 6 Films
| distributor    = TriStar Pictures
| released       =  
| runtime        = 116 minutes  
| country        = United States
| language       = English
| budget         = $14 million 
| gross          = $24,637,469  
}} remake of 1976 film of the same name, which centered on three singing teenage sisters from Harlem who form a girl group in the late 1950s. The remake takes place in Detroit, Michigan in the 1960s during the Motown era. 

The film stars Jordin Sparks, Derek Luke, Whitney Houston, Mike Epps, Cee Lo Green, Carmen Ejogo, Tika Sumpter, Tamela Mann, Cory Pritchett and Omari Hardwick. Sparkle features songs from the original film written by soul musician Curtis Mayfield as well as new compositions by R&B artist R. Kelly.   This film is the debut of Rhythm and blues|R&B/pop singer and American Idol winner Jordin Sparks as an actress. Sparkle also marks Whitney Houstons final feature film role before her death on February 11, 2012, three months after filming ended.  The film is dedicated to her memory.

==Plot summary==
Red introduces a local singer Black. While Black performs, Stix, an aspiring record label executive is watching in the club, meanwhile 19-year-old Sparkle Anderson talks with her eldest sister, 28-year-old Tammy "Sister" Anderson backstage or in "the kitchen". After performing Black encounters Sister and Sparkle backstage, attempting to flirt with Sister, but instead of accepting his advances she teases him by cutting the side of her dress and puts the neck of her dress in the inside of her dress.

As Sparkle rehearses for her show, Stix tells Sparkle a way to overcome her stage fright is by closing her eyes and sing. As Sparkle sings(Look into your heart) Stix brings Sparkle a piece of cake with a birthday candle on to celebrate her 20th birthday. Sparkle close her eyes makes a wish and tells Stix to ask her what she wish for, then she tells him that she wish that Stix propose to her better, stix then propose to Sparkle again by giving  her a candy ring and promises to give her a better ring depending on how their relationship goes. As a result they share a kiss and a hug. Emma goes to visit Sister in prison and the two have a talk about their relationship. Sparkle gets so excited her nose bleeds and Sparkle starts to panic hoping she can stop the bleeding to begin the show. Emma shows up with a new dress; while sparkle try on a red sparkly dress Emma to tell her that shes proud Sparkle has decided to follow her dreams, and wishes her good luck. Sparkle goes out on stage and performs "One Wing", a song which she dedicates to Sister.

==Cast==
* Jordin Sparks as Sparkle Anderson-  19 year old, youngest sibling in her family. She has a passion for singing but is unable to pursue a career due to her foreboding mothers rules against singing
* Whitney Houston as Emma Anderson- An overprotective, retired Professional singer. She forbids her 3 daughters from pursuing a career in music due to her own past experiences.
* Derek Luke as Stix- an ambitious manager of Sister and her sisters singing group and Sparkles love interest.
* Mike Epps as Satin Struthers- A comedian who becomes involved in an abusive relationship with Sister.  
* Carmen Ejogo as Tammy "Sister" Anderson- The oldest of the 3 sisters who displays more rebellion and a stronger desire for the fast life than either of her sisters.
* Tika Sumpter as Delores "Dee" Anderson- The middle child and second oldest sister in her family. Shes 24 years of age, shes confident, smart, and her ambition is to attend medical school to become a doctor. 
* Omari Hardwick as Levison "Levi" Robinson- Stixs Oldest Cousin whose in love with Sister and tries to prove it to her but he ends up getting back at Satin after she chooses him over Levi.
* Terrence J as Red - A MC at the Discovery Club 
* Cee Lo Green as Black- A chubby, flirty well-known male singer that sings at the Discovery Club.
* Tamela Mann as Mrs. Sarah Waters- Emmas honest close friend 
* Brely Evans as Tune Ann Waters
* Michael Beach as Reverend Bryce - The Reverend of their Church Kem L. Owens as Buddy - The host

==Background== The Robert Warner Bros. Pictures in 1976, starred Philip Michael Thomas, Irene Cara, Lonette McKee and Mary Alice, with songs and score composed and produced by Curtis Mayfield. One song from the Sparkle (Aretha Franklin album)|Sparkle soundtrack, "Something He Can Feel", became a hit single for both Aretha Franklin (in 1976) and En Vogue (in 1992).

===Production=== The Game The Game. 

Production began on October 10, 2011 in Detroit.  Filming wrapped up in early November 2011, with a release date scheduled for August 17, 2012.  The official trailer for Sparkle was released on April 2, 2012 and premiered on Today (NBC program)|Today   with the release of the trailer the films producer Debra Martin Chase said she had mixed emotions with the trailers release "On the one hand, Im so excited about the movie and were really happy with how it turned out," she said. "(But) just to have it said yet again that this is Whitneys last performance, its hard. Its hard."  

Howard Rosenman, the originals producer-writer and executive producer of the 2012 remake, said a novelization of the film will be released on August 7, 2012 by Simon & Schuster.  Rosenman said Houston was influenced by the original film as a girl and once went to see it every day for a week when she was a teenager.  It apparently helped plant the dream in her to become a singer. When the new version was coming together, Rosenman approached her to play the girls mother. She has two musical numbers in the film and on the soundtrack. Rosenman plans to team with director Joel Schumacher, one of the screenwriters of the original 1976 film, to add five new songs to those by Curtis Mayfield (in the original) and R. Kelly (in the 2012 version) for a Broadway musical of Sparkle. 

==Release== Sony announced the United States release date for Sparkle would be set for August 10, 2012.     This was later pushed back by a week to August 17, 2012 due to Whitney Houstons death.  


===Critical response===
 The movie received mixed to positive reviews.  Sparkle received an A on Cinemascore. Critic reviews for Sparkle so far have been generally mixed to positive, receiving a 57% on review aggregator  , another review aggregator, assigned the film a weighted average score of 55 out of 100 based on 25 reviews from mainstream critics, considered to be "mixed or average reviews". 
 Best Supporting Actress Oscar nomination. LaSalle comments: "...very few people will walk out of Sparkle talking about either Sparks or Houston, at least not at first. Instead they will be saying, "Who was that?" And theyll be referring to Carmen Ejogo...If there is any justice, Ejogo will become very famous very soon. As in, maybe today...Its rare to see someone become a movie star right before your eyes, but thats what happens with Ejogo in Sparkle....For an actress, this was the opportunity of a lifetime, and Ejogo plays it that way. She leaves nothing out, holds nothing back." 

===Box office===
In its opening weekend, the film grossed $11,643,342 in 2,244 theaters in the United States and Canada, ranking #5 at the box office.    Sparkle has grossed $24,397,469 domestically. 

===Awards===

====Black Reel Awards====
 
|- 2013
|Salim Akil Black Reel Best Director
|  
|- Twinkie Byrd Black Reel Best Cast (Ensemble)
|  
|- Salaam Remi  Best Original Score
|  
|- Mara Brock Akil  Black Reel Best Screenplay, Original or Adapted
|  
|- Whitney Houston, Jordin Sparks, R. Kelly - "Celebrate (Whitney Houston and Jordin Sparks song)|Celebrate" Black Reel Best Original Song
|  
|- Mike Epps  Black Reel Best Supporting Actor
|  
 

====BET Awards====
 
|- 2013
|Sparkle   BET Award Best Movie
|  
 

==Soundtrack==
 
The soundtracks first official lead single is the last song recorded by Houston before  ", debuted only one day after the premiere of "Celebrate". http://www.classichitsandoldies.com/v2/2012/05/24/whitney-houstons-his-eye-is-on-the-sparrow-released/  The official music video for "Celebrate" was filmed on May 30, 2012. https://twitter.com/iamtikasumpter/status/208056583874293760  It made its world premiere on   debuted on the Billboard 200|Billboard 200 at #26, #7 on the Billboard Hot R&B/Hip-Hop Songs, and #1 on the Billboard Soundtracks chart. 

==Home media==
Sparkle was released on DVD and Blu-ray Disc|Blu-ray on November 30, 2012. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 