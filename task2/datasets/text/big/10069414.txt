Bloodguilt (film)
{{Infobox Film |
  name         = Bloodguilt|
  image              = BloodguiltDVD.jpg|
  writer             = Yaky Yosha|
  starring           = Amos Lavie Yael Hadar Avital Dicker|
  director           = Yaky Yosha|
  producer           = Dorit Yosha|
  movie_music        = |
  distributor        = Yaky Yosha Ltd. |
  released           = 1996 (Israel) |
  runtime            = 85 minutes |
  country = Israel|
  language     = Hebrew/English |
  budget         = |
  music          = Richard Berger|
  awards         = |
}}
 Yaky Yoshas seventh feature film, is a dark thriller about a dangerous love triangle, inspired by the biblical romance of Jacob, Rachel and Leah.

==Plot==
Jake and Rachel are young, beautiful and married. Rachel comes from a wealthy family and Jake has a promising athletic career. 
When Rachel becomes pregnant, her foster sister, Leah, comes to help around the house. Rachel suspects that Leah and Jake have developed more than friendly relationship. One day, catching them red handed, Rachel loses control and attacks Jake with a kitchen knife. Jake now has a paralyzed arm, and a future turned to past. Rachel loses both her baby and her mind.
Leah and Jake retreat into an isolated rundown mountain cabin. 
One evening Rachel shows up at their door. Just released from a mental institution, she is fragile and helpless. Jake and Leah are troubled by her presence, but they take her in.
They all believe it may be possible to forgive, atone and heal past wounds. Rachel gradually gets better and prettier. Jake is attracted to her. The attraction seems mutual. Leah is now the odd man out, and she is about to lose control. 

A romantic triangle that began with crime ends with horrible punishment.

==External links==
*  

 

 
 
 
 
 
 


 
 