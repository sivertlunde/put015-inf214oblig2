The Duel (2000 film)
 
 
{{Infobox film name = The Duel image = The-Duel-2000-poster.jpg caption = Film poster  traditional = 決戰紫禁之巔 simplified = 决战紫禁之巅 pinyin = Juézhàn Zǐjìn Zhī Diān jyutping = Kyut3 Zin3 Zi2 Gam3 Zi1 Din1}} director = Andrew Lau producer = Wong Jing Manfred Wong story = Gu Long screenplay = Wong Jing Manfred Wong starring = Patrick Tam music = Chan Kwong-Wing cinematography = Andrew Lau editing = Marco Mak studio = Wins Entertainment Ltd. distributor = China Star Entertainment Group Wins Entertainment, Ltd. BoB and Partners Co. Ltd. released =   runtime = 106 minutes country = Hong Kong language = Cantonese budget = gross = HK$21,332,884.00 
}} Patrick Tam. The film is adapted from Juezhan Qianhou of Gu Longs Lu Xiaofeng novel series. It is known for its humorous take on the original story and its special effects. 

==Plot==
Dragon Nine, an imperial detective, is on his way back after concluding the case of the Thief Ghost. He encounters "Sword Saint" Yeh, who asks him to tell "Sword Deity" Simon to meet him for a duel on the night of the full moon at the highest rooftop of the Forbidden Palace. News of the upcoming duel between the two greatest swordsmen spread like wildfire and attract much attention, with people starting to place bets on the final outcome. The Emperor sends Dragon Nine and Princess Phoenix to stop the duel from taking place and investigate whether it is a mask for any sinister plots.

==Cast==
*Andy Lau as Yeh Cool Son (Ye Gucheng)
*Ekin Cheng as Snow Blower Simon (Ximen Chuixue)
*Nick Cheung as Dragon Nine (Lu Xiaofeng)
*Zhao Wei as Princess Phoenix
*Kristy Yang as Ye Ziqing (Sun Xiuqing)
*Tien Hsin as Jade Patrick Tam as Emperor
*Norman Chu as "Thief Ghost" Ling Wun-hok
*Elvis Tsui as Gold Moustache
*Jerry Lamb as Dragon Seven
*Frankie Ng as Dragon Five
*Ronald Wong Ban as Dragon Six
*Lee Sheung-man as Shek Tsi-lun
*Wong Yat-fei as Minister
*Liu Wei as Eunuch Lau Tong
*Jing Gangshan as Tong Fei
*Yin Xiaotian as Tong Ngo
*Geng Le as Yim Tsi-chun
*Yang Haiquan as Siu Tsi-chung
*Liu Hongmei
*Hong Yingying
*Li Li
*Zhan Xiaonan

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 

 