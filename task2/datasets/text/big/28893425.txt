Neuilly sa mère!
 
{{Infobox film
| name           = Neuilly sa mère !
| image          = Neuilly-sa-mere.jpg
| alt            = DVD cover depicting in the foreground an Arab boy with a duffel bag, and in the background a family standing in front of a fancy house. The words Neuilly sa mère ! are visible in front of the family.
| caption        = DVD cover
| director       = Gabriel Julien-Laferrière
| producer       = Djamel Bensalah  Isaac Sharry
| screenplay     = Philippe de Chauveron, Marc de Chauveron
| starring       = Samy Seghir Rachida Brakni Jérémy Denisty Joséphine Japy Mathieu Spinosi Josiane Balasko
| music          = 
| cinematography = Pascal Gennesseaux
| editing        = Jean-François Elie
| studio         = 
| distributor    = TFM Distribution
| released       =  
| runtime        = 97 minutes
| country        = France
| language       = French
| budget         = $4,900,000
| gross = $38,604,892 
}}
 French comedy film directed by Gabriel Julien-Laferrière. It stars Samy Seghir as a rebeu teenager who moves from the housing projects to the upscale neighborhood of Neuilly-sur-Seine. Because of its use of social inequality as a comedic device, it has been compared to the 1988 French comedy Life is a Long Quiet River (La vie est un long fleuve tranquille).            -->

A word-for-word translation of the films title is "Neuilly her mother!". The title is a play on the vulgar French insult nique ta mère ("Screw your mother"), in which Neuilly effectively serves as a euphemism.

==Plot== Burgundy region. When his widowed mother (Farida Khelfa) takes a job working on a boat, she sends Sami to live with her sister Djamila (Rachida Brakni), who is married to Frenchman Stanislas de Chazelle (Denis Podalydès). They live in the upscale neighborhood of Neuilly-sur-Seine, an affluent suburb of Paris, with Stanislas two children from his prior marriage: Charles (Jérémy Denisty), who aspires to be a politician someday, and Caroline (Chloé Coulloud). At first Charles resents Samis presence in his house, but they gradually become friends. Sami enrolls in classes at Saint-Exupéry, a private school there.

Sami is made fun of by many of his classmates at school, particularly Guilain Lambert (Mathieu Spinosi). He impresses the blonde violinist Marie (Joséphine Japy), however, with his tender side: during a music class one of the pieces moves him to tears by making him think of his friends back in Chalon. He and Marie become friends. Guilain, who also has his eye on Marie, gets revenge by tricking Sami into eating pork (which Sami cannot eat, as it is haraam), and Sami retaliates by beating up Guilain. Marie chides Sami, saying she will not tolerate violent boys, but they soon make up. Later, though, when Guilain and Charles end up running against one another in the class elections, Guilain hires a local thug Malik (Booder) to mug Sami and Charles. They manage to escape from Malik and his thugs, but Malik gives Sami a black eye in the process; when Marie sees the black eye, she becomes angry with Sami, thinking he got in a fight again.

Sami and Charles decide to throw a party in their house, to try to win votes for Charles for the class delegate election. Marie comes to the party and has forgiven Sami, but before they can talk Malik and his friends show up to trash the house. Some friends Sami invited from Chalon (Shaiko Dieng and Pierre Louis Bellet), however, soon arrive, and with their help the partygoers give Malik a beating and chase him and his thugs away. Malik finds Guilain and, thinking Guilain set him up, gives him a beating that sends him to the emergency room. Guilain and his mother encounter Samis aunt Djamila and uncle Stanislas at the hospital, and tell them that the beating was Samis fault . Djamila, enraged, returns home and tells Sami she is sending him back to his mother.

Marie finds Malik and pays him to tell the school headmistress (Josiane Balasko) that he is responsible for Guilains beating, however he refuses the cash and told her that he had already told the story to the headmistress. The Headmistress informs Djamila, and she forgives Sami.   Charles wins the election with the help of a well-written letter to the class, and goes on to meet all of Samis friends back in Chalon, urging them to remember his name and vote for him in the future. Sami stays in Neuilly-sur-Seine, and begins a romantic relationship with Marie.

==Cast==
* Samy Seghir: Sami Benboudaoud
* Rachida Brakni: Djamila, Samis aunt
* Denis Podalydès: Stanislas de Chazelle, Djamilas husband, owner of a pork company
* Jérémy Denisty: Charles de Chazelle, Samis cousin (Stanislas son)
* Chloé Coulloud: Caroline, Samis cousin (Stanislas daughter)
* Joséphine Japy: Marie, a girl at Saint-Exupéry school, and Samis love interest
* Mathieu Spinosi: Guilain Lambert, a student at Saint-Exupéry, Samis rival
* Farida Khelfa: Nadia, Samis mother
*   fan
*  
* Pierre Louis Bellet: Jason, Samis friend back home
* Anne Duverneuil: Sophie Bourgeois, a student at Saint-Exupéry who helps Sami adjust
* Josiane Balasko: the director of Saint-Exupéry
* François-Xavier Demaison: Father Dinaro
* Armelle: Mme Blanchet, history and geography teacher at Saint-Exupéry
* Olivier Baroux: M Boulegue, math teacher at Saint-Exupéry
* Booder: Abdelmalik ("Malik"), the boss of a street gang in the Picasso de Nanterre neighborhood
* Éric Judor: table tennis player at Maurice Ravel

==Soundtrack==
The films title track was recorded by Faf Larage and Magic System. 

==Reception==

The film holds an 86% rating on  ".  Marwan Chahine of Libération magazine, on the other hand, praised the films political jokes and allusions to political personalities. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 