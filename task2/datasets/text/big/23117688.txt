Kulir 100°
 
 

{{Infobox film
| name           = Kulir 100°
| image          =
| alt            =  
| caption        = 
| director       = Anita Udeep
| producer       = Anita Udeep
| writer         = Anita Udeep 
| screenplay     = 
| story          =   Sanjeev Riya Adithya Menon Rohit
| music          = Bobo Shashi
| cinematography = L. K. Vijay
| editing        = B. Lenin
| studio         = Mayajaal Entertainment
| distributor    = Mayajaal Entertainment
| released       =   
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}} Tamil film released in 2009 and directed by Anita Udeep.

==Synopsis== Adithya Menon), and studies at the Chengalpet Matriculation School. His mother who is separated from her rowdy husband wants him to grow up as a model citizen. Unfortunately he has more of his fathers genes and gets expelled for beating up a teacher.

Surya who is attached to his mother gives a promise that he will not take the path to violence again, and is admitted to the up market Lake View School in Ooty (the principal of the school Thalaivasal Vijay says it is the "best school in south Asia!!").

The Chengalpet guy is looked down by his snooty seniors led by Rohit (Rohit Rathod) and his gang. The only person who stands with him is Babloo (Bobo Sasi), and the principals daughter Tanya (Riya) who has a soft corner for him.

Under extreme provocation, Surya keeps his cool as he tries to live up to his mothers expectation. However he breaks down after Babloo is murdered, and goes on a revenge spree. The film ends on a tragic note, as the message of the film is conveyed.

==Cast==
* Sanjeev as Surya
* Riya as Tanya
* Karthik Sabesh as Babloo
* Rohit Rathod as Rohit

==Production==
Kulir 100 is the directorial debut of Anita Udeep. According to her, the title "indicates its both cold and hot which means that opposites attract, the basic framework of the film." The film was made on a budget of  . Shooting for the film took place in Switzerland, Kulu, Manali, Yercaud and Ooty 

==Reception==
Behindwoods said, "The title could be reminiscent of all things pleasant in this inexorably hot weather, but watching debutant director Anita Udeeps Kulir doesnt have the same effect. Anita, after probably having set out to deliver a message-movie, has lagged behind soon enough and ended up doing just the opposite" and concluded "Experience the cold!", while rating the film 0.5 out of 5 stars.  Sify said, "The film lacks the magic one associate with boarding school brat pack movie. Let us hope Anita Udeep, learns from her mistakes and finds a better script in her next outing."  Pavithra Srinivasan of Rediff said, "Kulir 100 Degrees tries very hard to simulate the lives of youngsters, but it seems to flit to the next scene before any scenario can be fully explored, leaving you with a sense of dissatisfaction. In the end, the result is rather half-baked. The cool visuals and neat songs are the only compensation." and rated the film 2 out of 5 stars. 

==Soundtrack==
{{Infobox album|  
| Name = Kulir 100°
| Type = Soundtrack
| Artist = Bobo Shashi
| Cover  = 
| Released = 13 November 2008 Feature film soundtrack
| Length = 24:57 Tamil
| Label = Sony Music
| Producer = Bobo Shashi
| Reviews =
| Last album =
| This album =Kulir 100° (2008)
| Next album =Bindaas (2010 film)|Bindaas (2010)
}}
The films score and soundtrack were composed by debutant Bobo Shashi. The soundtrack album features seven tracks, with lyrics written by Pa. Vijay, V. Ilango, the girl band Purple Patch, Boomerang X and the duo Thug Laws, Abhishek and Shivam. The album was released at Sathyam Cinemas on 13 November 2008 by director Gautham Menon. 

The soundtrack won critical acclaim, with a critic noting "it’s a definite must-try for its target group".  

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 24:57
| lyrics_credits = yes
| title1 = Boom
| lyrics1 = Pa. Vijay
| extra1 =  Benny Dayal, Abishek, Bobo Shashi & Nritya
| length1 = 04:22
| title2 = Hip Hop Hurray
| lyrics2 = Boomerang X
| extra2 =  Coco Nanda & MC Bullet
| length2 = 03:15
| title3 = Manasellam (Unplugged)
| lyrics3 = V. Ilango
| extra3 = Silambarasan
| length3 = 03:50
| title4 = Siragindri Parakkalam
| lyrics4 = Purple Patch
| extra4 =  Geetha, Rekha, Saritha, Archana, Benny Dayal, Swami, Lil-J & Bobo Shashi
| length4 = 03:47
| title5 = Un Uyir Nanban
| lyrics5 = Thug Laws Krish
| length5 = 02:22
| title6 = Nallavan Kettavan
| lyrics6 = Boomerang X
| extra6 =  Coco Nanda & MC Bullet
| length6 = 02:45
| title6 = Manasellam
| lyrics6 = V. Ilango Ranjith
| length6 = 04:36
}}

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 