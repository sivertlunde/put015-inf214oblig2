Escape (1948 film)
{{Infobox film
| name           = Escape
| image	         = Escape FilmPoster.jpeg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Joseph L. Mankiewicz
| producer       = William Perlberg Philip Dunne
| based on       =  
| starring       = Rex Harrison Peggy Cummins William Hartnell
| music          = William Alwyn
| cinematography = Freddie Young
| editing        = Alan L. Jaggs
| distributor    = 20th Century Fox
| released       =   
| runtime        = 78 minutes
| country        = United Kingdom United States
| language       = English
| budget         =
| gross          =
}} thriller film Philip Dunne filmed in 1930. 

==Plot==
A former RAF squadron leader, Matt Denant goes to Hendley, England to visit an airfield run by Titch, a friend. An employee there named Rodgers asks if he would kindly make a large wager for him on a horse. Denant does, but when the horse loses, Rodgers makes a promise to repay him.

While strolling through Hyde Park, a woman strikes up a conversation with Matt, only to be charged with unlawful soliciting by Penter, a detective. Matt intervenes on her behalf, but when the two men fight, Penters head strikes a park bench.

Matt is placed under arrest. He is then sentenced to three years in prison after Penter dies. Believing it an unjust punishment, Matt escapes. Inspector Harris of Scotland Yard is assigned to find him, while Matt takes refuge in the home of Sir James Winton, whose daughter Dora helps him hide.

An aeroplane is left for Matt by his friend, enabling him to flee to France, but he is betrayed by Rodgers once its learned the police are offering a reward. Matts aeroplane is caught in a heavy fog and crashes. He survives and takes refuge in a farm. Dora finds him and professes her love, also persuading Matt that he must turn himself in to the law.

==Cast==
* Rex Harrison as Matt Denant
* Peggy Cummins as Dora Winton
* William Hartnell as Inspector Harris
* Peter Croft as Titch
* Stuart Lindsell as Sir James Winton
* Norman Wooland as Minister
* Jill Esmond as Grace Winton
* Frederick Piper as Brownie - convict
* Marjorie Rhodes as Mrs. Pinkem
* Betty Ann Davies as Girl in Park
* Cyril Cusack as Rodgers John Slater as Salesman
* Frank Pettingell as Constable Beames Michael Golden as Detective Penter
* Frederick Leister as Judge
* Walter Hudd as Defence Counsel
* Maurice Denham as Crown Counsel

==Reception==
===Critical response===
Film critic A.H. Weiler of the New York Times wrote a positive review of the film, "As the harried convict, Rex Harrison gives a restrained but persuasive portrait of a man beset not only by physical but moral tribulations ... The pace of Escape, as set by director Joseph L. Mankiewicz, is, on occasion, slower than might be desired, and scenarist Philip Dunnes script is sometimes given to lengthy conversation. But these are minor flaws in an adult work, which unspectacularly and effectively does justice to a serious theme. 

Similarly, critic Craig Butler appreciated the film, writing, "Mankiewicz does a very good job of emphasizing the scripts strong points, and he uses a number of interesting visual touches to keep things lively during discussions of morality and other weighty issues ... For his part, Harrison is in top form, finding multiple levels to play in dialogue that could easily devolve into rants and providing the kind of solid performance that is crucial to anchoring a film of this type. Neither he nor Mankiewicz can overcome the limitations of the script to make Escape a classic, but they do make it fairly engrossing." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 