The Jungle (1914 film)
{{Infobox film
| name = The Jungle
| imagesize =
| image	= The Jungle FilmPoster.jpeg
| caption = George Irving John H. Pratt
| producer = Margaret Mayo Upton Sinclair (novel) George Nash Gail Kane
| cinematography =
| editing =
| distributor = All-Star Feature Corporation
| released =  
| runtime = 5 reels  Silent English English (intertitles)
| country = United States
| budget =
}} American drama George Nash. book of the same name by Upton Sinclair, the only one to date. Sinclair reportedly bought the negative of the film prior to 1916, hoping to market the film nationally after its initial release in 1914. Sinclair himself appears at the beginning and end of the movie, as a sort of endorsement of the film.

The film, from historical accounts at the time of release,  included the scenes Jurgis murdering the foreman who raped Jurgis wife by throwing him over a walkway into a "sea of frightful horns passing beneath him" (cattle). The film was commonly screened at socialist meetings across America at the time. 

It is now considered a lost film.   

==Cast== George Nash as Jurgis Rudkus
* Gail Kane as Ona Julia Hurley as Elzbieta
* Robert Cummings as Connor
* Alice Marc as Marija
* Robert Paton Gibbs as Antanas (as Robert Payton Gibbs)
* Clarence Handyside as John Durham
* E. P. Evers as Freddy Durham (as Ernest Evers) George Henry Irving
* Upton Sinclair as Himself

==See also==
* List of American films of 1914
* List of lost films
* The Jungle (1906) novel by Upton Sinclair

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 


 