Duel of Champions
 
{{Infobox film
| name           = Duel of Champions
| image          = Duel of Champions.jpg
| caption        = Terence Young
| producer       =
| writer         = Luciano Vincenzoni Carlo Lizzani Ennio De Concini Giuliano Montaldo
| starring       =
| music          = Angelo Francesco Lavagnino
| cinematography = Amerigo Gengarelli
| editing        = Renzo Lucidi
| distributor    =
| released       =  
| country        = Italy
}}
 Roman legend of the Horatii, triplet brothers from Rome who fought a duel against the Curiatii, triplet brothers from Alba Longa in order to determine the outcome of a war between their two nations.
 Terence Young.  The screenplay was written by Ennio De Concini, Carlo Lizzani, Giuliano Montaldo and Luciano Vincenzoni.

==Cast==
*Alan Ladd as "Horatii|Horatius"
*Franca Bettoia as "Marcia"
*Franco Fabrizi as "Curiatius" Robert Keith as "Tullus Hostilius"
*Jacqueline Derval as "Horatii|Horatia"
*Luciano Marin as "Eli"
*Andrea Aureli as "Gaius Cluilius"
*Mino Doro as "Caius"
*Osvaldo Ruggieri as "Warrior of Alba"
*Jacques Sernas as "Marcus"

==Production==
Tiberia Fillms had to cooperate with Lux Films in order to finance the venture. 

==Biography==
* 

==See also==
* List of historical drama films

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 