Caveman (film)
 
{{Infobox film
| name           = Caveman
| image          = Caveman Movie Poster.jpg
| caption        = Movie poster
| director       = Carl Gottlieb David Foster Lawrence Turman
| writer         = Rudy De Luca Carl Gottlieb
| starring       = Ringo Starr Dennis Quaid Shelley Long Barbara Bach
| music          = Lalo Schifrin
| cinematography = Alan Hume
| editing        = Gene Fowler, Jr.
| distributor    = United Artists
| released       =  
| runtime        = 91 min.
| country        = United States
| awards         = 
| language       = English
| gross          = $15,965,924
}}
 American slapstick slapstick comedy film written and directed by Carl Gottlieb and starring Ringo Starr, Dennis Quaid, Shelley Long and Barbara Bach. The film has also gained a cult following.

==Plot== bullying leader. abominable snowman. In the course of these adventures they discover sedative drugs, fire, cooking, music, and learn how to walk fully upright. Atouk uses these advancements to lead an attack on Tonda, overthrowing him and becoming the tribes new leader. He rejects Lana and takes Tala as his mate, and they live happily ever after.

==Cast==
* Ringo Starr as Atouk
* Dennis Quaid as Lar
* Shelley Long as Tala
* Jack Gilford as Gog
* Cork Hubbert as Ta
* Mark King as Ruck
* Paco Morayta as Flok
* Barbara Bach as Lana
* Evan C. Kim as Nook
* Ed Greenberg as Kalta
* Carl Lumbly as Bork
* Jack Scalici as Folg
* Erika Carlsson as Folgs Mate
* Gigi Vorgan as Folgs Daughter
* Sara López Sierra as Folgs Younger Daughter
* Esteban Valdez as Folgs Son
* Juan Ancona Figueroa as Folgs Younger Son
* Juan Omar Ortiz as Folgs Youngest Son
* Anaís de Melo as Meeka
* John Matuszak as Tonda
* Avery Schreiber as Ock
* Tere Álvarez as Ocks Mate
* Miguel Ángel Fuentes as Grot
* Ana De Sade as Grots Mate
* Gerardo Zepeda as Boola
* Hector Moreno	as Noota
* Pamela Gual as Nootas Mate
* Richard Moll as Abominable Snowman

==Production== Churubusco Studios in México City. The film features stop-motion animated dinosaurs constructed by Jim Danforth,  including a Tyrannosaurus Rex which in one scene becomes intoxicated by a Cannabis (drug)|Cannabis-type drug, animated by Randall W. Cook.  Danforth was a major participant in the special effects sequences, but left the film "about two-thirds of the way" (his words) through the work because the Directors Guild of America prohibited his contracted on-screen credit, co-direction with Carl Gottlieb. Consequently, Danforths name does not appear on the film. 

The films dialog is almost entirely in "caveman" language, such as:

*"alunda" &ndash; love
*"bobo" &ndash; friend
*"haraka" &ndash; fire
*"macha" &ndash; monster
*"aiyee" &ndash; help
*"ya" &ndash; yes
*"nya" &ndash; no/not
*"ool" &ndash; food
*"pooka" &ndash; broken/pain
*"ugh" &ndash; like
*"zug zug" &ndash; sex/mate
*"kuda" &ndash; come
*"caca" &ndash; shit
*"guwi" &ndash; out to get
*"gluglu" &ndash; drowned

At some showings audiences were issued a translation   caveman, by speaking English, he appears to be more advanced than the rest. At her audition, Long said she did not speak any English, but responded to everything with grunts. 

Barbara Bach and Ringo Starr first met on the set of Caveman, and they married just over a year later. 

== Release ==
The film was released on February 17, 2015 on Blu-ray Disc over Olive Films. 

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 