La Famille Bélier
{{Infobox film
| name           = La Famille Bélier 
| image          = La Famille Bélier (poster).jpg
| caption        =
| director       = Éric Lartigau 
| producer       = Philippe Rousselet Éric Jehelmann Stéphanie Bermann 
| writer         = Victoria Bedos Stanislas Carré de Malberg 
| starring       = Karin Viard François Damiens Éric Elmosnino Louane Emera
| music          = Evgueni Galperine Sacha Galperine 
| cinematography = Romain Winding
| editing        = Jennifer Augé 
| studio         = France 2 Cinéma Nexus Factory Jerico Mars Films Quarante 12 Films Vendôme Production uMedia
| distributor    = Mars Distribution 
| released       =  
| runtime        = 100 minutes
| country        = France
| language       = French French Sign Language
| budget         = $11 million
| gross          = $82,566,570 
}}

La Famille Bélier ( ) is a 2014 French comedy film directed by Éric Lartigau.  It won the Salamandre dor (Audience Award) at the Sarlat Film Festival in November 2014. 

== Plot == interpreter for deaf parents and brother on a daily basis, especially in the running of the family farm. One day, a music teacher discovers her gift for singing and encourages Paula to participate in a prestigious singing  contest in Paris, which will secure her a good career and a collegiate degree. However, this decision would mean leaving her family and taking her first steps towards adulthood.

The film shares strong plot themes with the 1996 Indian film   and the 1996 German film  

== Cast ==
 

* Karin Viard as Gigi Bélier 
* François Damiens as Rodolphe Bélier 
* Éric Elmosnino as M. Thomasson 
* Louane Emera as Paula Bélier 
* Roxane Duran as Mathilde 
* Ilian Bergala as Gabriel 
* Luca Gelberg as Quentin Bélier 
* Mar Sodupe as Mlle Dos Santos 
* Stéphan Wojtowicz as Lapidus
* Jérôme Kircher as Dr. Pugeot 
* Bruno Gomila as Rossigneux 
* Clémence Lassalas as Karène

==Accolades==
{| class="wikitable plainrowheaders sortable"
|- style="background:#ccc; text-align:center;" Award / Film Festival Category
! Recipients and nominees Result
|- 40th César Awards Best Film
|La Famille Bélier
|  
|- Best Actor
|François Damiens
|  
|- Best Actress Karin Viard
|  
|- Best Supporting Actor
|Éric Elmosnino
|  
|- Most Promising Actress Louane Emera
|  
|-
|César Award for Best Original Screenplay| Best Original Screenplay Victoria Bedos, Stanislas Carré de Malberg, Éric Lartigau and Thomas Bidegain
|  
|- Globes de Cristal Award Best Film
|La Famille Bélier
|  
|- Best Actor
|François Damiens
|  
|- 20th Lumières Awards Best Film
|La Famille Bélier
|  
|- Best Actress Karin Viard
|  
|- Most Promising Actress Louane Emera
|  
|- Best Screenplay Victoria Bedos, Stanislas Carré de Malberg, Éric Lartigau and Thomas Bidegain
|  
|- Sarlat Film Festival Salamandre dor 
|La Famille Bélier
|  
|}

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 


 