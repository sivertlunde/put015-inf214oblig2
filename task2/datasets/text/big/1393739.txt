The Brothers Grimm (film)
 
 
{{Infobox film
| name           = The Brothers Grimm
| image          = Brothers grimm movie poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Terry Gilliam
| producer       = Daniel Bobker Charles Roven
| writer         =  
| starring       = Matt Damon Heath Ledger Peter Stormare Lena Headey Jonathan Pryce
| music          = Dario Marianelli
| editing        = Lesley Walker
| cinematography = Newton Thomas Sigel
| studio         =   The Weinstein Company
| distributor    = Miramax Films  
| released       =  
| runtime        = 118 minutes
| country        = United States United Kingdom Czech Republic
| language       = English French German Italian
| budget         = $88 million
| gross          = $105,316,267
}} adventure fantasy film directed by Terry Gilliam. The film stars Matt Damon, Heath Ledger, and Lena Headey in an exaggerated and fictitious portrait of the Brothers Grimm as traveling Confidence trick|con-artists in French-occupied Germany during the early 19th century. However, the brothers eventually encounter a genuine fairy tale curse which requires real courage instead of their usual bogus exorcisms. Supporting characters are played by Peter Stormare,  Jonathan Pryce, and Monica Bellucci.

In February 2001, Ehren Kruger sold his spec script to Metro-Goldwyn-Mayer (MGM). With Gilliams hiring as director, the script was rewritten by Gilliam and Tony Grisoni, but the Writers Guild of America refused to credit them for their work, thus Kruger received sole credit. MGM eventually dropped out as distributor, but decided to co-finance The Brothers Grimm with Dimension Films and Summit Entertainment, while Dimension took over distribution duties.
 Bob and Harvey Weinstein, which caused the original theatrical release date to be delayed nearly ten months. The Brothers Grimm was finally released on 26 August 2005 with mixed reviews and a $105.3 million box office performance.

==Plot==
 n Queen (Monica Bellucci) stealing young girls to restore her own beauty. Will and Jake have a complicated relationship; Jake is the smaller, younger, more sensitive one that Will feels he needs to protect. Will is often very hard on Jake (dating all the way back to their childhood, when Jake spent their money that was to be used for medicine for their dying sister on "magic beans") and orders him around. Will is somewhat of a womanizer and wants to make money, whereas Jake is more interested in fairy tales and adventures. Jake feels that Will doesnt care about or believe in him; but Will is just frustrated about the way Jake acts so spontaneously, making it hard for Will to protect him.

Long ago, King Childeric I came to the forest to build a city while the Queen experimented with black magic to gain eternal life. A plague swept through the land, and she hid in her tower, while her husband and everyone below her perished. Her spell granted her immortal life, but not the youth and beauty to go along with it. Her youthful appearance now only exists in her mirror, the source of her life, as an illusion and nothing more. She needs to drink the blood of twelve young girls to regain her beauty; ten have already been reported missing. The Queen is working an enchantment to regain her beauty with the aid of her werewolf huntsman and his magic axe, crow familiars, and various creatures in the forest. The Grimms, with the help of Cavaldi and Angelika (Lena Headey), a knowing huntress from the village, intend to destroy the Mirror Queen. After another girl goes missing, Cavaldi takes the Grimms and Angelika back to Delatombe. Because they have failed, Cavaldi is ordered to kill both the Grimms; but, after convincing Delatombe that the magic in the forest is actually caused by German rebels, he sends them back. While Cavaldi stays behind with Angelika in the village, the brothers attempt to get into the tower. Jake succeeds and discovers the Queen and the power of her mirror. Meanwhile, another girl, named Sasha, is captured despite Angelika and Cavaldis efforts to save her.

Jake rides into the forest alone after a spat with Will, who follows him. After mistaking a dummy that is smashed into the tower for Jake, Will realizes that Jake needs him to believe in him and assists Jake in climbing up the tower. On the roof of the tower, Jake notices twelve crypts in which the twelve victims must lie. When Sashas body comes up from a well, the werewolf takes her to a tomb. After rescuing Sasha and taking the werewolfs magic axe, the Grimms return to the village. Delatombe captures the brothers and believes them to be frauds. French soldiers begin burning down the forest, and Cavaldi represses his sympathy to the brothers, but they are eventually saved by Angelika. The werewolf is revealed to be Angelikas father, who is under the Queens spell. It turns out that he is only able to keep on living due to an enchanted spike that is lodged into his chest and, without such, the spell is broken. Angelika is drowned by her father, becoming the 12th victim. The Brothers reach the tower while the Queen breathes an ice wind that puts out the forest fire. Delatombe notices that the Grimms have escaped and goes after them with Cavaldi. When Cavaldi refuses to kill the Grimms, Delatombe shoots him but is later impaled by Will.

Will and Jake enter the tower, where Will falls under the Queens spell when the Queen takes the enchanted spike from Angelikas father and thrusts it into Wills chest. Jake shatters the enchanted mirror in the tower, preventing the Queen from completing the spell that will restore her youth. With the last of his strength, Angelikas father destroys the rest of the mirror by jumping out of the window with it; and Will, attempting to save the Queen, tries to take back the mirror and falls with him, and both men are killed. Outside, Cavaldi survives, having donned the Grimms faux-magic armor. He finds Wills body and recites an Italian curse, and the tower falls apart. Jake escapes, and Cavaldi informs Jake that he can break the spell and awaken Angelika with a kiss, which in turn resurrects the other girls and Will. With the menace gone and their daughters returned to them, the villagers of Marbaden celebrate and give their heart-felt thanks to the brothers. Cavaldi stays in the village and joins the villagers for the feast. Angelika kisses both the Grimms and tells them that they are always welcome at the village. The Brothers Grimm decide to pursue a new profession, presumably recording fairy tales, although they are now wanted criminals of the state. One of the Queens crows is seen flying off with the last shard of her mirror, still holding the Queens watchful eye and presumably, her living soul.

==Cast== prosthetic nose, nose job ever". 
* Peter Stormare as Mercurio Cavaldi, Delatombes Italian associate. Cavaldi originally has a grudge against the brothers, but eventually has a change of heart. Robin Williams was cast in the role before dropping out. 
*   transformed into a werewolf by the Mirror Queens spell. Gilliams first choice for the role was Samantha Morton. 
* Jonathan Pryce as General Vavarin Delatombe, a cruel French military commander. Delatombe attempts to burn down the forest and kill the brothers.
* Tomáš Hanák as Woodsman
* Julian Bleach as Letorc
* Monica Bellucci as the Mirror Queen, a beautiful, evil queen who experimented with black magic before being struck by the Bubonic plague, her spell giving her eternal life but not the eternal youth she had expected. Nicole Kidman turned down the role over scheduling conflicts.   
*  s for the Grimms; they are eventually beheaded by French soldiers.
* Roger Ashton-Griffiths as the Mayor, who is duped into employing the Grimms to rid his village of its haunting.

==Production==
Ehren Krugers screenplay was written as a spec script; in February 2001, Metro-Goldwyn-Mayer (MGM) purchased the script, with Summit Entertainment to co-finance the film.  In October 2002, Terry Gilliam entered negotiations to direct,  and rewrote Krugers script alongside frequent collaborator Tony Grisoni. However, the Writers Guild of America refused to credit Gilliam and Grisoni for their rewrite work, and Kruger received sole credit.    After Gilliams hiring, production was put on fast track for a target November 2004 theatrical release date.    The budget, originally projected at $75 million, was to be Dimension Films most expensive film ever.    The studio had trouble financing the film, and dropped out as main distributor.  Weeks later, Bob Weinstein, under his Dimension Films production company, made a deal with MGM and Summit to co-finance The Brothers Grimm, and become the lead distributor. 

===Filming=== Alien vs. Van Helsing,  The Brothers Grimm provided work for hundreds of local jobs and contributed over $300 million into the Czech Republics economy.  Gilliam hired Guy Hendrix Dyas as production designer after he was impressed with Dyas work on X2 (film)|X2.    Gilliam often disputed with executive producers Bob and Harvey Weinstein during production.  The Weinstein Brothers fired cinematographer and regular Gilliam collaborator Nicola Pecorini after six weeks. Pecorini was then replaced by Newton Thomas Sigel. 

"Im used to riding roughshod over  , at the Weinsteins. You cant try and impose big compromises on a visionary director like him. If you try to force him to do what you want creatively, hell go nuclear."  The feud between Gilliam and the Weinsteins was eventually settled, although Bob Weinstein blamed the entire situation on yellow journalism.  Filming was scheduled to end in October, but due to various problems during filming, principal photography did not end until the following 27 November. 

Due to the tensions between the filmmaker and the producers during production, Gilliam said in retrospect about the film, " ts not the film they wanted and its not quite the film I wanted. Its the film that is a result of   two groups of people, who aren’t working well together." Peče, Maša (2009).  , Senses of Cinema, no. 53, 2009. Retrieved 25 April 2010  With regards to the Weinsteins also producing Martin Scorseses film Gangs of New York (2002), Gilliam stated: "Marty   said almost the exact same quote I said, without us knowing it: They took the joy out of filmmaking." 

===Visual effects===
Post-production was severely delayed when Gilliam disagreed with the Weinsteins over the final cut privilege. In the meantime, the conflict lasted so long that Gilliam had enough time to shoot another feature film, Tideland (film)|Tideland. To create the visual effects, Gilliam awarded the shots to Peerless Camera, the London-based effects studio he founded in the late-1970s with visual effects supervisor Kent Houston. However, two months into filming, Houston said that Peerless "ran into a number of major issues with The Brothers Grimm and with the Weinstein Brothers". He continued that "the main problem was the fact that the number of effects shots had dramatically increased, mainly because of issues that arose during shooting with the physical effects."  Meanwhile the Queens chamber inside the tower was actually built by the Art Department as 2 sets. One set was resplendent and new while the other was old and decrepit. The sets were joined to each other by the central mirror, a piece of transparent glass giving the illusion that a single set was reflected and used to create the effect.
 The Time computerized rendering, this could not happen, as the 3D volume of the body suddenly turns into 2D pieces of glass. The problem was eventually solved due to sudden advances that occurred with Softimage XSI software.   

==Release== 2005 event. 

===Box office===
The Brothers Grimm was released in the United States in 3,087 theaters, earning $15,092,079 in its opening weekend.  The film eventually grossed $37,916,267 in the United States and $67.4 million internationally, coming to a worldwide total of $105,316,267.    The Brothers Grimm was shown at the 62nd Venice International Film Festival on 4 September 2005, while in competition for the Golden Lion, but lost to Brokeback Mountain, also starring Ledger. 

===Critical reception===
The Brothers Grimm was released with mixed reviews from critics.       Based on 179 reviews collected by Rotten Tomatoes, 38% of the critics enjoyed the film, with an average score of 5.2/10.  By comparison, Metacritic collected a score of 51/100 based on 36 reviews.  The majority of critics believed Gilliam sacrificed the storyline in favor of the visual design.  Roger Ebert called the film "an invention without pattern, chasing itself around the screen without finding a plot. The movie seems like a style in search of a purpose, with a story we might not care about." 

Stephen Hunter of The Washington Post wrote that "The Brothers Grimm looks terrific, yet it remains essentially inert. You keep waiting for something to happen, and after a while your mind wanders from the hollow frenzy up there with all its filigrees and fretwork." {{cite news | author = Stephen Hunter | title = Gold into Dross | work = The Washington Post | url =
http://www.washingtonpost.com/wp-dyn/content/article/2005/08/25/AR2005082501834.html | date = 26 August 2005 | accessdate = 2008-12-10}}  Mick LaSalle from the San Francisco Chronicle felt "despite an appealing actor in each role, the entire cast comes across as repellent. Will and Jake Grimm are two guys in the woods, surrounded by computerized animals, putting audiences to sleep all over America." {{cite news | author = Mick LaSalle | title = The fakers Grimm, before they became famous storytellers | url = dark comedy". 

===Home media=== Disney own home video rights, while Metro-Goldwyn-Mayer holds television rights.  The DVD release of The Brothers Grimm in December 2005 includes audio commentary by Gilliam, two "making-of" featurettes, and deleted scenes.  The film was released on Blu-ray Disc format in October 2006. 
==TV Series==
Miramax has hired Ehren Kruger to adapt the film into a television series. 

==References==
 

;Further reading
*  

==External links==
 
 
*  
*  
*  
*  
*  
*  ;  
*  
*   filming locations at Movieloci.com

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 