Number Seventeen
 
 
 
{{Infobox film
| name           = Number Seventeen
| image          = Number17poster.PNG
| image_size     =
| caption        =
| producer       = John Maxwell 
| director       = Alfred Hitchcock
| screenplay     = Alfred Hitchcock Alma Reville Rodney Ackland
| story          = Jefferson J. Farjeon
| based on       = play Number Seventeen John Stuart Barry Jones Ann Casson
| music          = Adolph Hallis Jack Cox Bryan Langley
| editing        = A.C. Hammond
| studio         = Associated British Picture Corporation
| distributor    = Wardour Films
| released       = 18 July 1932	 (London)
| country        = United Kingdom
| language       = English
| budget         =
| runtime        = 64 minutes
}}
 John Stuart, Anne Grey and Leon M. Lion. The film is about a group of criminals who committed a jewel robbery and put their money in an old house over a railway leading to the English Channel, the films title being derived from the houses street number. An outsider stumbles onto this plot and intervenes with the help of a neighbour, a police officers daughter.

After being available only in poor-quality prints for decades, the film was released in high quality by French media company Canal+ in 2005. {{cite web |url=http://www.nytimes.com/2007/02/06/movies/homevideo/06dvd.html?n=Top/Reference/Times%20Topics/People/H/Hitchcock,%20Alfred
|title= New DVDs: Cinema Classics
|accessdate=30 July 2008
|last=Kehr
|first=Dave
|date=6 February 2007
|publisher=The New York Times}} 

==Plot==
Detective Barton is searching for a necklace stolen by a gang of thieves. In the beginning, the gang is in a house in London, before going on the run.
 John Stuart) arriving at a house marked for sale or rent. The door is unlocked and he wanders in. An unknown person with a candle is wandering about and a dead body is found. When confronted the mysterious person claims innocence of the murdered person. Barton (who introduces himself as Forsythe) asks the stranger what he has in his pockets (handkerchief, string, sausage, picture of a child, half a cigarette), before the shadow of a hand is shown reaching for a doorknob. The stranger (who later introduces himself as Ben) searches the body of the dead person and finds handcuffs and a gun which he takes.

The detective returns from investigating the weird sound and finds the handcuffs which the stranger left on the ground. A person is seen to be crawling on the roof through shadows, who then falls through the roof. This is a woman called Miss Akroyd (Ann Casson) who is revived and cries out for her father. She explains that her father went onto the roof and that they are next door in number 15.

The bell tolls half past midnight and the dead body has disappeared. Three people arrive at the windswept house, Mr. Ackroyd (Henry Caine), Nora (Anne Grey) (who is deaf and dumb) and a third person. Ben draws out the gun. Ben accidentally shoots the governor. Mr. Ackroyd draws out a gun and asks him to search the gentlemen, Ben and Miss Ackroyd. The telegram is revealed to Mr. Ackroyd. Sheldrake (Garry Marsh) gets the diamond necklace, which he has hidden in the upper portion of a toilet. Ben causes a commotion and is locked away with Sheldrake.

The two hands of Sheldrake reach out and appear to strangle Ben who is only pretending to be knocked out. More members of the gang arrive. They suggest tying up Miss Akroyd and Forsythe. The three thieves all have to catch a train. However, one of the "thieves" is Miss Akroyds father--a police officer--who locks away two of the thieves and frees Miss Akroyd and Doyle. He opens the door where Ben is locked away with Sheldrake and gets into a fist fight with Sheldrake.

The other man reveals himself as Sheldrake (the supposed corpse from earlier) and frees the others. Miss Akroyd and Forsythe are tied up again. Nora reveals herself to be able to speak and says "Im coming back". She comes back and frees Miss Akroyd and Doyle. Miss Akroyd faints but recovers. Nora returns to the basement to allay the suspicions of the other thieves and buy time for the rest to get away. They free Ben and Miss Akroyds father. The thieves arrive at the train yard, and board a freight train that is departing. The train says Deutsch-Englischer Fahrverkehr Ferry Service between Germany-Great Britain.

The train departs with Ben aboard and he stumbles onto crates of wine. The thieves, after dispatching the conductor, go to the front of the train, shoot the fireman, and catch the Driver as he faints. Forsythe failed to get on the train before it departed and commandeers a bus. Ben is revealed to have the necklace. Sheldrake discovers he doesnt have the diamond and the thieves fight each other. Sheldrake claims that Barton a detective posing as a thief. A chase scene occurs on the train as the thieves go after Barton. Barton escapes and handcuffs Nora. The bus that Forsythe is on races after the train. The thieves, realising the train is accelerating, try and find the brakes. They turn dials helplessly and notice the bus that Forsythe is on.

Pushing levers and turning dials does nothing, indeed, it only makes the train go faster, leaving the thieves unable to escape. At the dock, the ferry pulls up. As Forsythe watches, the train hurtles through the dock, crashes into the train currently on the ferry at full speed, and pushes it out to sea, dragging the remaining cars into the ocean. People are rescued from the water. Henry Doyle tells Forsythe that he is posing as Detective Barton. But Forsythe is actually Detective Barton, who says to Doyle, "You cant be Barton because I am." All of the thieves are apprehended by the police who are on the scene. Nora asks Barton, "What are you going to do about it?" Barton replied "You better come along with me." Nora says "Where?" "To breakfast." Barton says, and they laugh. Ben then reveals he has the diamond necklace.

==Cast==
* Leon M. Lion as Ben
* Anne Grey as Nora – The Deaf-Mute Girl John Stuart as Barton – The Detective
* Donald Calthrop as Brant – Noras Escort Barry Jones as Henry Doyle
* Ann Casson as Rose Ackroyd
* Henry Caine as Mr. Ackroyd
* Garry Marsh as Sheldrake

==Production== John Maxwell about it, but Maxwell said that Walter C. Mycroft had a different film for him to do, a filmed version of Joseph Farjeons play Number Seventeen. Hitchcock was unhappy with this, as he considered the story to be too full of cliches and he wanted to do a version of John Van Drutens London Wall. The director who eventually got to do London Wall at the time, wanted to direct Number Seventeen. Spoto, 1999. p. 129 

Hitchcock was assigned writer Rodney Ackland for the film, and decided to make the film as a comedy-oriented thriller. 

The film makes extensive use of miniature sets, including a model train, bus, and ferry.

Though the opening credits confirm the pictures title is Number Seventeen, much of the promotional material (as per graphic above) and many databases refer to Number 17.

In the book Hitchcock/Truffaut (Simon and Schuster, 1967), Hitchcock called the film "A disaster".

==Reception==
On its initial release, audiences reacted to Number Seventeen with confusion and disappointment.  The film is not often seen nowadays, but continues with generally negative reviews with critics from Rotten Tomatoes noting the film as, "highly entertaining but practically incomprehensible" and as an "unsatisfactory early tongue-in-cheek comedy/suspense yarn". {{cite web
|url= http://www.rottentomatoes.com/m/number_17/
|title=  MOVIES  / ON DVD / NUMBER 17
|accessdate=3 August 2008
|last=
|first=
|date=
|publisher=Rotten Tomatoes}} 
In the Hitchcock/Truffaut book (see above), François Truffaut has a similar verdict, telling Hitchcock he had found the film "quite funny, but the story was rather confusing".

==Notes==
 

==References==
*{{cite book
 | last= Spoto
 | first= Donald
 | coauthors=
 | title= The Dark Side of Genius: The Life Of Alfred Hitchcock
 |publisher= Da Capo Press
 |year= 1999
 |isbn= 0-306-80932-X
}}

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 