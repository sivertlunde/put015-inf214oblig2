Loin (film)
{{Infobox film
| name           = Loin
| image          = Loin , poster.jpg
| caption        = Theatrical release poster
| director       = André Téchiné
| studio         = Saïd Ben Saïd Alhena Films
| writer         = André Téchiné  Faouzi Bensaïdi
| starring       = Stephane Rideau Lubna Azabal  Mohamed Hamaidi  Gaël Morel
| music          = Juliette Garrigues
| cinematography = Germain Desmoulins
| editor         = Hervé de Luze
| distributor    =
| released       =  
| runtime        = 120 minutes
| country        = France
| language       = French
| budget         = 4.650.000 €  
}}

 Loin  ( ) is a 2001 French-Spanish drama film directed by André Téchiné, starring Stephane Rideau, Lubna Azabal  and Mohamed Hamaidi. The film, set in Tangier in a three-day period, tells the story of three young friends taking critical decisions about their uncertain future.

==Plot==
For the past few years, Serge, a young French man, has been working as a long distance truck driver, employed by a company that ships goods from Morocco to Europe. It is a demanding job that gives him plenty of time for reflection and boredom. In Algeciras, ready to make his next trip to Africa, Serge succumbs to the criminal subculture, dangerously agreeing to smuggle hashish from Morocco to Europe hiding the illegal drugs in his truck.

This time, the Moroccan stopover between transports of cargoes will last three days. In Tangier, while he waits for his truck to be loaded and pass customs, Serge is reunited with his friend Saïd. Saïd, a young Moroccan whose only possession is his bicycle, desperately seeks to escape his restricted background and avidly longs for the possibility and intrigue of life beyond Africa, but he has attempted his illegal immigration many times before with dire consequences. Serge who is eager to see his on and off girlfriend Sarah, Saïd’s friend and employer, strikes a deal with Saïd. If Saïd can convince Sarah to see him again, Serge promises to smuggle Saïd to Europe.

Saïd takes Serge to see his and Sara’s friends: Jack, a gay American émigré living happily in Morocco, and François, a young film director preparing a documentary on illegal immigration. François and Serge went to the same school eight years ago. Saïd entices Sarah, to go out that night in order to prepare and encounter with Serge. When she first see him again she runs away, but ultimately she yields and they get back together. Sarah, a beautiful, independent young Jewish woman mourning her recently dead mother operates a small hotel, where Saïd works. She is in a disjunctive of her own. Her successful brother, a Canadian émigré, wants her to leave Morocco and join him in Montreal. She agonizes over the decision, unsure about emigrating. Serges return and their complicated relationship just make harder to make a decision.  Emily, Sarah’s sister in law, a writer who recently has lost a son, arrives to help her close the hotel and resettle in Canada. The closing of the hotel leaves Saïd without a job and intensifies his need to try to pass to Europe. However, Serge backs off with his part of the plan to take Saïd in his truck to Spain. Angry, Saïd breaks his friendship with Serge and tries to indispose Sarah against Serge.

With money that Sarah gives Saïd for the lost of his job, he goes to a seedy neighborhood with François, who is attracted to Saïd,  but trying to changed the local currency to pesetas, he is robbed and loses his bicycle in the process. The River. At the end Serge ends empty handed neither he gets any money from his attempt at smuggling drugs nor he has certain in his relationship with Sarah who still can make out her mind of either leaving or staying in Tanger. Saïd, has little to lose and goes to the port and hides beneath Serge’s truck. He is found by Serge who sneaks him inside the truck’s cabin. Together they are on the road to Europe.

==Cast==
*Stéphane Rideau as Serge
*Lubna Azabal as Sara
*Mohamed Hamaidi as Saïd
*Gaël Morel as François Jack Taylor as James
* Yasmina Reza as Emily
*Rachida Brakni as Nezha
* Nabila Baraka as Farida

==Analysis==
Loin was shot on digital video using primarily natural light. The slightly patchy video image contributes to the sense of collapse and unease. The film is set in Tangier and is told in three "movements"; the sections marked by chapters. Téchiné renders the confusion and desperation occasioned by the three leads personal dilemmas into a larger canvas of cultural dislocation, identity and friendship.

In moving between the stories of his three principal actors, Téchiné establishes both an emotional immediacy and painful confusion that captures the intensity of feeling between them, addressing some of the directors trademark themes: family relations, irreconcilable sexual entanglements and the allure of criminal activity. Téchiné, examines the relationships between these three very independent twenty something, showing how their various desires in life conflict because of citizenship, employment, class differences, as well as personal outlook

The film generally concerns Moroccans with various relationships with the country: visiting staying, leaving and contemplating. Techinés captures the complexity of the emotions that join his star-crossed lovers. He balances their love story against the serious backdrop of smuggling and immigration.

==DVD release==
Loin is available in Region 2  DVD. Audio in French and dubbed Spanish. Spanish subtitles. There is no Region 1 DVD available.

==Notes==
 

==References==
*Marshall, Bill, André Téchiné, Manchester University Press, 2007, ISBN 0-7190-5831-7

==External links==
*  

 

 
 
 
 
 
 
 
 