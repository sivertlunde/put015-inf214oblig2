The Last Warhorse
{{multiple issues|
 
 
}}

{{Infobox film
| name           = The Last Warhorse
| image size     = 
| image	=	
| caption        = 
| director       = Bob Meillon
| producer       = Helen Boyd
| writer         = Colin Free
| based on       = 
| starring       = Graham Dow Robert Carlton
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| studio         = JNP
| released       = 1986
| runtime        = 93 mins
| country        = Australia
| language       = English
| budget         = $500,000   accessed 12 July 2013 
}}
The Last Warhorse is a 1986 Australian film about a Japanese businessman who tries to acquire a property belonging to a horse owning family. Ed. Scott Murray, Australia on the Small Screen 1970-1995, Oxford Uni Press, 1996 p99  It was shot in Wahroonga and Glebe, New South Wales|Glebe. 

==Plot==
Ishikawa, a Japanese businessman (Kazue Matsumoto), takes up residence in Sydney to direct the construction of a waterfront development. However, his employees secretly use his name to acquire the adjoining property to get access to horses.

==Cast==
*Graham Dow
*Ritchie Singer
*Kazue Matsumoto
*Robert Carlton
*Olivia Martin
*Kristin Veriga
*Kazue Matsumoto

==References==
 

==External links==
*  at Screen Australia
*  at BFI

 
 
 


 