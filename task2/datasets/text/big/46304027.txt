Ablations
{{Infobox film
| name           = Ablations
| image          = 
| caption        = 
| director       = Arnold de Parscau
| producer       = Benoît Delépine Jean-Pierre Guérin Serge de Poucques Sylvain Goldberg Adrian Politowski Gilles Waterkeyn Nadia Khamlichi
| writer         = Benoît Delépine
| based on       = 
| starring       = Denis Ménochet Virginie Ledoyen Florence Thomassin Philippe Nahon Yolande Moreau
| music          = Matthieu Gonet
| cinematography = François Catonné		
| editing        = Pascale Chavance
| distributor    = Ad Vitam Distribution
| studio         = JPG Films No Money Productions
| released       =  
| runtime        = 94 minutes
| country        = France Belgium
| language       = French
| budget         = 
| gross          = $49,147 
}}
Ablations is a 2014 French and Belgian drama directed by Arnold de Parscau. 

==Plot==
A man wakes up in a wasteland, with no memory of the night before, a lower back scar. A former mistress, surgeon, tells him that stole a kidney. Obsessed with this flight, he will sacrifice everything to find him: his family, his job ... to go crazy.

==Cast==
 
* Denis Ménochet as Pastor Cartalas
* Virginie Ledoyen as Léa Cartalas
* Florence Thomassin as Anna
* Philippe Nahon as Wortz
* Yolande Moreau as Wortzs Assistant
* Serge Riaboukine as Jean-Michel Poncreux
* Lily-Rose Miot as Juli
* Lika Minamoto as Mikako
* Antoine Coesens as Patrice
* Louane Gonçalves Santos as Miguel
* Luka Breidigan Campana as Bruno
 

==Production==
The movie was presented to the Festival international du film fantastique de Gérardmer, the Champs-Élysées Film Festival and to the USA with the Chicago International Film Festival.

==References==
 

==External links==
* 

 
 
 
 
 
 

 
 