Aloft (film)
 
{{Infobox film
| name           = Aloft
| image          = Aloft film poster.png
| caption        = Theatrical release poster
| director       = Claudia Llosa
| producer       = Ibon Cormenzana Phyllis Laing José María Morales 
| writer         = Claudia Llosa
| starring       = Jennifer Connelly Cillian Murphy Mélanie Laurent
| music          = Michael Brook 
| cinematography = Nicolas Bolduc
| editing        = Guillermo de la Cal 
| distributor    = Sony Pictures Classics 
| released       =  
| runtime        = 116 minutes 
| country        = Spain Canada France
| language       = English
| budget         =
}}

Aloft ( ) is a 2014 drama film written and directed by Claudia Llosa, starring Jennifer Connelly, Cillian Murphy and Mélanie Laurent.  The film premiered in competition at the 64th Berlin International Film Festival.   

==Plot==
Nana Kunning (Jennifer Connelly) was a struggling mother that abandoned her son Ivan (Cillian Murphy) 20 years ago. Now she is becoming a famous artist and healer when Jannia (Mélanie Laurent), a young journalist tracks down Ivan and sets up an encounter between Nana and Ivan that will make them question their lives.

== Cast ==
* Jennifer Connelly as Nana Kunning
* Oona Chaplin as Alice   
* Cillian Murphy as Ivan
** Zen McGrath as Young Ivan
* Mélanie Laurent as Jannia Ressmore
* Ian Tracey as Hans
* Peter McRobbie as Ike
* William Shimell as Newman 
* Nancy Drake as Old Woman
* Winta McGrath as Gully
* Owen Volk as Baby

==Production==
Filming took place in Manitoba, Canada. 

== References ==
{{Reflist|refs=
   
}}

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 

 