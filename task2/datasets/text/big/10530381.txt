The Cats of Mirikitani
{{Infobox Film |
  name     = The Cats of Mirikitani |
  image=The Cats of Mirikitani.jpg|
  writer         =  |
  starring       = Jimmy Mirikitani |
  director       =  Linda Hattendorf |
  producer       = Linda Hattendorf Masa Yoshikawa | Joel Goodman|
  distributor       = | 2006 |
  runtime        = 74 min. |
  language = English/Japanese |
  budget         = |
}}
The Cats of Mirikitani is a documentary film originally released in 2006 in film|2006. 
{{cite news
| url       = http://www.rte.ie/ten/2006/0509/tribeca.html
| title     = Tribeca honours war-themed films 
| publisher = RTE
| date      = 2006-05-09
}}
  
{{cite news
| url       = http://www.portlandmercury.com/portland/Content?oid=309651&category=22133
| title     = History on Repeat: Our Century in Racial Profiling
| publisher = Portland Mercury
| author    = Marjorie Skinner
| date      = 
}}
  
{{cite news
| url       = http://www.peninsuladailynews.com/article/20110817/NEWS/308179999/jennifer-jacksons-port-townsend-neighbor-column-quilcene-turns-out
| title     = Quilcene turns out for familys story
| publisher = Peninsula Daily News
| date      = Jennifer Jackson
| quote     = 
}}
 

==Synopsis==
In 2001 Japanese American painter, Jimmy Mirikitani (born Tsutomu Mirikitani), and over 80 years old, was living on the streets of lower Manhattan. Filmmaker, Linda Hattendorf, took an interest and began
to engage with him to create a documentary of his life. After the destruction of the World Trade Center  on September 11, 2001, the debris- and dust-choked streets were deserted. When Hattendorf "found" Mirikitani, in his usual spot along the wall of a Korean Market, near the intersection of MacDougal and Prince Street in Soho, she offered him shelter in her small apartment. During this period a beautiful and curious friendship flowered, as Ms. Hattendorf began the long process of re-integrating Mr. Mirikitani back into society, recovering, among other documents, his social security card and passport. Over the months they lived together, she uncovered his true identity and history. And ultimately she reunited him with his niece, poet Janice Mirikitani, and his surviving sister and helped him find his own apartment in an assisted living facility.

Over the course of the film, audiences learn about Mirikitanis past, including the injustice experienced by American-born Japanese during the Second World War, his career as an artist, his life among other artists, including Jackson Pollock. Ms. Hattendorf documents Mirikitanis epic journey, from California, to Hiroshima, back to California, to his imprisonment in an internment camp, to his sojourn across the country to Long Island and finally to New York City, where he was employed as a cook. When his employer died, Mirikitani became homeless, spending almost a decade in Washington Square Park. Later, he moved to the streets of Soho, where he created an "atelier" on the streets, and worked days and nights on his artwork.

Hattendorfs highly personal film about justice deferred, loss and redemption has won many awards in the United States and abroad, and in the end brought both Hattendorf and Mirikitani well-deserved and hard-won regard. (The "cats" in the title are featured in Mirikitanis artwork.)
 PBS series Independent Lens.

Mirikitani died October 21, 2012, at the age of 92. 

==Awards==
*Won the Audience Award at the 2006 Tribeca Film Festival. 
*Won the Best Picture Award in the Japanese Eyes section of the 2006 Tokyo International Film Festival 
*Won the audience award at the 2007   

==References==
 

==External links==
*  PBS
* 
*  
* 

 
 
 
 
 
 
 
 
 
 
 