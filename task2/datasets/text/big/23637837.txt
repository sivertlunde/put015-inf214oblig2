Angaaray (1998 film)
 
 
 
{{Infobox film
| name           = Angaaray
| image          = Angaaray (1998 film).jpg
| image size     =
| caption        =
| writer         = Robin Bhatt   Javed Siddiqui   Aakash Khurana
| producer       = Goldie Behl   Madhu Ramesh Behl  Shrishti Behl
| director       = Mahesh Bhatt Nagarjuna Pooja Bhatt Sonali Bendre
| music          = Anu Malik Aadesh Shrivastava
| cinematography = Sameer Arya
| editing        = Sanjay Sankla
| studio         = Rose Movies Combines
| distributor    =
| released       =   
| runtime        = 2:20:32
| country        =   India
| language       = Hindi
| budget         =  
| gross          =  
}}
 Hindi action film produced by Madhu Ramesh Behl on Rose Movies Combines banner, directed by Mahesh Bhatt. It stars Akshay Kumar, Akkineni Nagarjuna|Nagarjuna, Pooja Bhatt, Sonali Bendre in lead roles and music is composed by Anu Malik & Aadesh Shrivastava. It was a flop at the box office.
 State of Grace. Akshay Kumar did many dangerous stunts like jumping from one moving bus to another, hanging on to a speeding bus with only his legs, and jumping from a six-floor building to a three-floor building which are 45 feet apart. Mahesh Bhatt again used the same theme in his movie "Footpath" starring Aftab Shivdasani and directed by Vikram Bhatt.

==Plot==
Bangalore-based police inspector Amar (Akshay Kumar) is approached by Mumbais police commissioner, Vinod Talwar (Naresh Suri), and is assigned the task to go undercover and locate and kill the killer(s) of a businessman, Khanna. Amar soon finds out that the culprit is his childhood friend, Raja Lokhande (Akkineni Nagarjuna|Nagarjuna). Amars other childhood friends, Jaggu Lokhande (Paresh Rawal) and Surya (Irfan Kamal) are also working for Lala Roshanlal (Gulshan Grover), a major ganglord. He joins their gang as well using their friendship. 

Meanwhile, Amar rekindles an old flame with his childhood sweetheart, Pooja (Pooja Bhatt), who is Suryas sister. Pooja has distanced herself from her brother and friends as she wants to lead an honest life. Amar tells her that they can be reformed. Meanwhile, Lala starts to suspect Amar to be a mole in the gang after an encounter goes wrong. Jaggu confronts Amar who denies it, supported by Raja. Jaggu then tells him to kill a police inspector to prove himself. Amar agrees and Pooja, in anger, distances herself from him as well. Amar shoots the police inspector in front of the gang and has proved his worth. 
 rakhi on his wrist. At the same time, Lalas brother enters lusting after her. This angers Surya who beats him very hard. Lala hides at home, but when Amar and Raja go out to lookout for police, Jaggu calls him outside. It turns out that Jaggu betrayed Surya and gets him killed. 

Raja is now hungry for Lalas brothers blood and kills him. Amar reveals to Pooja that he is an undercover cop. Amar finds out that Raja has killed Lalas Roshanlal and confronts him. However, the police force enters at the exact moment to shoot Raja while Roma, Rajas girlfriend faints. Amar reveals that he is a cop and begs to take Roma to the hospital while Raja runs away. Very angry, Raja confronts Amar and is about to kill him. Amar manages to calm him down and Raja agrees to surrender after Roma is found pregnant. Jaggu meanwhile receives news from Lala that he can hide Raja but should bring Amar to him. Jaggu goes to their place and gives Raja tickets to escape Mumbai, but he insists to surrender. Jaggu angrily tells him that police cant be trusted, but Amar then reveals that he knew Jaggu was the one who got Surya killed. Jaggu realises what grave mistakes he has done and agrees to surrender as well. However, Lalas men start to attack their place with fire bombs so Amar and Raja go out to finish them. Raja gets shot in the knee and tells Amar to catch Lala at any cost. Amar chases Lala and finally catches him. He then kills Lala at the spot and Vinod and the police arrive. 

In the end, it is revealed that Raja and Jaggu will face 10 and 6 years of jail respectively after they agreed to be police informants. Meanwhile, Amar is taken to police custody for questioning.

==Cast==
 
* Akshay Kumar: Inspector Amar
* Akkineni Nagarjuna|Nagarjuna: Raja Lokhande 
* Pooja Bhatt: Pooja
* Sonali Bendre: Roma
* Gulshan Grover: Lala Roshanlal
* Paresh Rawal: Jaggu Lokhande
* Kulbhushan Kharbanda: Khanna
* Irfan Kamal: Surya
* Mohan Kapoor: Lala Roshanlals brother
* Naresh Suri: Police Commissioner Vinod Talwar
* Razak Khan: Jaggus friend
* Anant Jog: Police Inspector
* Chandu Parkhi: 
* Prabha Mathur: 
* Parmindar: 
* Upendar: Master Kunal Khemu: Young Amar
* Master Shaurya Mehta: Young Raja 
* Master Girish Prakash: Young Jaggu 
* Master Bunty: Young Surya
* Baby Gazala: Young Pooja
 

==Soundtrack==
{{Infobox album
| Name        = Angaaray
| Tagline     = 
| Type        = film
| Artist      = Anu Malik Aadesh Shrivastava
| Cover       = 
| Released    = 1998
| Recorded    = 
| Genre       = Soundtrack
| Length      = 41:31
| Label       = T-Series
| Producer    = 
| Reviews     =
| Last album  =  
| This album  = 
| Next album  = 
}}

The music was composed by Anu Malik and Aadesh Shrivastava. Lyrics were written by Javed Akhtar. The music was released by T-Series Audio Company. 
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 41:31
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =

| title1  = Tanha Tanha 
| extra1  = Abhijeet Bhattacharya, Alka Yagnik 
| length1 = 6:10

| title2  = Aande Aande
| extra2  = Udit Narayan, Aadesh Shrivastav, Alka Yagnik, Amit Kumar
| length2 = 6:00

| title3  = Yaaden Kitni Yaaden
| extra3  = Udit Narayan, Alka Yagnik 
| length3 = 7:28

| title4  = Hai Koi Meharban
| extra4  = Abhijeet Bhattacharya, Alka Yagnik
| length4 = 5:35

| title5  = Le Chalo Tum Jahan
| extra5  = Hariharan (singer)|Hariharan, Anuradha Paudwal  
| length5 = 4:53

| title6  = Rangila
| extra6  = Anu Malik, Suneeta Rao 
| length6 = 6:49

| title7  = Tum Hi Mere Humnashin
| extra7  = Hariharan (singer)|Hariharan, Kavita Paudwal 
| length7 = 4:36
}}

==External links==
*  
 

 
 
 
 