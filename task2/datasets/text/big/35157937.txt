Barrio (film)
{{multiple issues|
 
 
}}
 Spanish film directed by Fernando León de Aranoa. It won several awards including Best Director at the 13th Goya Awards, and was also nominated for Best Picture. 

== Principal actors ==

* Críspulo Cabezas (Rai)
* Timy Benito (Javi)
* Eloi Yebra (Manu)
* Marieta Orozco (Susi, Javis sister)
* Alicia Sánchez (Carmen, Javis mother)
* Enrique Villén (Ricardo, Javis father)
* Francisco Algora (Ángel, Manus father)
* Chete Lera (police inspector)

== Plot ==

Javi, Manu, and Rai are friends from school. They are teenagers, and they talk about girls a lot but seldom talk with them. They live together in the barrio (neighbourhood). It is a barrio of large social housing blocks. There is little to do, and in August even less. The centre of the city is far away, so the three friends spend most of their time in the barrio. The television news says that thousands of inhabitants of the city have gone to the coast for the summer. They wish they were among them. The windows of the travel agents are full of tempting offers. But their lives demonstrate how hard it is to leave the barrio and how hard it is to improve their situation. In the end, their lives take an unexpected turn.

==References==
 

==External links==
* 
* 
* 

 
 


 