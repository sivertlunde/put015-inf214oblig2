Hoje (film)
{{Infobox film
| name           = Hoje
| image          = Hoje Film Poster.jpg
| caption        = Theatrical release poster
| director       = Tata Amaral
| producer       = Caru Alves de Souza Tata Amaral
| writer         = Jean-Claude Bernardet Rubens Rewald Felipe Sholl
| starring       = Denise Fraga João Baldasserini Cláudia Assunção Lorena Lobato Pedro Abhull César Troncoso
| music          = Livio Trachtenberg
| cinematography = 
| editing        = 
| studio         = Tangerina Entretenimento Primo Filmes
| distributor    = H2O Films
| released       =  
| runtime        = 90 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| preceded_by    = 
| followed_by    = 
| mpaa_rating    = 
| tv_rating      = 
}}
 44th Festival de Brasília in 2011. 

Starring Denise Fraga, the film tells the story of Vera, a woman who fought against the military dictatorship in Brazil and began to receive a compensation from the Government for the disappearance of her husband, at the time of the crackdown. 

== Plot == Brazilian military government dictatorship. With the money, she can buy the so dreamed apartment and free herself of the condition of suspension in which she lived for decades, when she was not even officially recognized as widow. When moving to the new home, she receives a visit that will change her life. 

== Cast ==
* Denise Fraga	
* César Troncoso	
* João Baldasserini	
* Lorena Lobato	
* Cláudia Assunção	
* Pedro Abhull

==References==
 

== External links ==
*  

 
 
 
 
 

 
 