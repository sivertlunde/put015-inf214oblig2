The Cobweb (film)
{{Infobox film
| name           = The Cobweb
| image          = Thecobwebposter1955.jpg
| caption        = Theatrical release poster
| producer       = John Houseman Jud Kinberg
| director       = Vincente Minnelli
| based on       =  
| screenplay     = John Paxton
| starring       = Richard Widmark Lauren Bacall Charles Boyer Gloria Grahame
| music          = Leonard Rosenman
| cinematography = George Folsey
| editing        = Harold F. Kress
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 124 minutes English
| country        = United States
| budget         = $1,976,000  . 
| gross          = $1,978,000 
}}
 MGM Eastmancolor William Gibson. It was released on DVD as part of the Warner Archive Collection on January 18, 2011.

The film features an elite cast, revolving around the disturbed psyches of inmates and staff members at a posh psychiatric clinic. Stewart McIver (Richard Widmark) leads the way as the head of the clinic, while his wife Karen (Gloria Grahame) takes it upon herself to select new drapes for the hospitals library. These seemingly ordinary drapes set off a melodrama with an equal amount of love and lunacy. 

The opening credits are followed by the following onscreen words:  
"The trouble began."
  At the end of the film, the words appear onscreen. 
"The trouble was over"
 

==Plot==
Dr. Stewart McIver (Richard Widmark) is now in charge of a psychiatric facility, one run for many years by medical director Dr. Douglas Devanal (Charles Boyer).
 John Kerr), a possibly suicidal artist, and the self-loathing Mr. Capp (Oscar Levant). All of his responsibilities keep McIver so busy that his wife, Karen (Gloria Grahame), feels increasingly frustrated and ignored.

When new drapes are needed for the clinics library, the dour and penny-pinching Victoria Inch (Lillian Gish) orders unattractive ones. Karen McIver takes it upon herself to buy a more expensive and colorful set instead, gaining the approval of chairman of the board Regina Mitchell-Smythe (Mabel Albertson) but without the knowledge of her husband. What should be an insignificant matter is complicated further by Dr. McIver giving the patients, principally Stevie, permission to design and create the new drapes themselves.

Personalities clash. Dr. Devanal, who has a drinking problem, has been having an affair with his secretary Miss Cobb (Adele Jergens) and makes a clumsy pass at McIvers wife as well. McIver begins to fall in love with Meg Rinehart (Lauren Bacall), a member of his staff. Miss Inch privately schemes to expose the unseemly behavior of Devanal at the next meeting of the board and issues a veiled threat to do so to McIver as well, while Devanals wife, Edna (Fay Wray), mistakenly believes McIver to be behind the plot to discredit her husband. 

Having felt stable enough to go on a date with Sue Brett (Susan Strasberg), another patient, Stevie Holt is very upset to discover that new drapes have been installed, not the ones his artwork was meant to inspire. He disappears, causing a search party to look for him and McIver to fear a suicide. In the end, Stevie reappears, the McIvers agree to work on their marriage and Miss Inch decides not to disclose everyones actions. A grateful Dr. Devanal, his reputation intact, submits his resignation to the board.

==Cast==
* Richard Widmark as Dr. Stewart McIver
* Lauren Bacall as Meg Rinehart
* Charles Boyer as Dr. Devanal
* Gloria Grahame as Karen McIver
* Lillian Gish as Victoria Inch John Kerr as Steven W. Holte
* Susan Strasberg as Sue Brett
* Oscar Levant as Mr. Capp
* Tommy Rettig as Mark McIver Paul Stewart as Dr. Otto Wolff
* Adele Jergens as Miss Cobb
* Mabel Albertson as Regina Mitchell-Smythe
* Fay Wray as Edna Devanal
* Eve McVeagh as Mrs. Shirley Irwin

==Music==
The score was composed, conducted and orchestrated by Leonard Rosenman.  The music distinguishes itself by "having the first predominantly twelve-tone score ever written for a motion picture."   {{cite journal
| title      = The Cobweb/Edge of the City
| others    = Leonard Rosenman
| date      = 2003
| url       = 
| last      = Bond
| first     = Jeff
| author2 = Lukas Kendall
| pages     =12
| type    = CD insert notes
| journal = Film Score Monthly
| volume=6 |issue=14
| location  = Culver City, California, U.S.A.
| language  = 
}} 

The first release of portions of the score was on MGM Records on LP in 1957. The complete score in stereo was issued on cd in 2003, on Film Score Monthly records.

==Reception==
According to MGM records the film earned $1,385,000 in the US and Canada and $593,000 elsewhere resulting in a loss of $1,141,000. 

==See also==
* Lillian Gish filmography

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 