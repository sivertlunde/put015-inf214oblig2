Flame Top
{{Infobox film
| name           = Flame Top
| image          = 
| caption        = 
| director       = Pirjo Honkasalo Pekka Lehto
| producer       = Claes Olsson
| writer         = Pirjo Honkasalo Pekka Lehto Algot Untola
| starring       = Asko Sarkola
| music          = 
| cinematography = Kari Sohlberg
| editing        = Irma Taina
| distributor    = 
| released       =  
| runtime        = 155 minutes
| country        = Finland
| language       = Finnish
| budget         = 
}}
 Best Foreign Language Film at the 53rd Academy Awards, but was not accepted as a nominee. 

==Cast==
* Asko Sarkola - Writer
* Rea Mauranen - Olga Esempio
* Kari Franck - Gunnar Avanto
* Esko Salminen - Arwid
* Ritva Juhanto - Kurttuska
* Ari Suonsuu - Kalle
* Tuomas Railo - Sulo Esempio
* Heikki Alho - Pikku-Pouvali
* Yuri Rodionov - Rawitz
* Galina Galtseva - Magda
* Soli Labbart - Maria Lassila
* Pirkka Karskela - Young Algot
* Markku Blomqvist - Lundberg
* Esa Suvilehto - Pöntinen
* Matti Oravisto - Commandant Carl von Wendt

==See also==
* List of submissions to the 53rd Academy Awards for Best Foreign Language Film
* List of Finnish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 