The Passionate Demons
{{Infobox film
| name           = The Passionate Demons
| caption        = 
| image	         = The Passionate Demons FilmPoster.jpeg
| director       = Nils Reinhardt Christensen
| producer       = Sverre Gran
| writer         = Nils Reinhardt Christensen Axel Jensen
| starring       = Margarete Robsahm
| music          = Egil Monn-Iversen
| cinematography = Ragnar Sørensen
| editing        = Olaf Engebretsen
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
}}

The Passionate Demons ( ) is a 1961 Norwegian drama film directed by Nils Reinhardt Christensen. It was entered into the 1961 Cannes Film Festival.     

==Cast==
* Margarete Robsahm - Line
* Toralv Maurstad - Jacob
* Henki Kolstad - Gabriel Sand
* Sissel Juul - Hanne
* Elisabeth Bang - Jacobs Sister
* Rønnaug Alten - Jacobs Mother
* Truuk Doyer - A Passionate Demon
* Rolf Søder - Benna
* Rolf Christensen - Jacobs Father
* Atle Merton - Laffen
* Per Lillo-Stenberg - Jeno
* Ragnhild Hjorthoy - Ellen
* Per Christensen - Putte
* Odd Borg - Kalle
* Ulf Wengård - Pål
* Frithjof Fearnley - Lines father
* Wenche Medbøe - Veslemøy
* Olava Øverland - A passenger

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 