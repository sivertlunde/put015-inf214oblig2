Kevin & Perry Go Large
{{Infobox film
| name           = Kevin & Perry Go Large
| image          = KevinPerryGoLarge.jpg
| image_size     =
| caption        =
| director       = Ed Bye
| producer       = Peter Bennet-Jones Harry Enfield Jolyon Symonds Barnaby Thompson David Cummings Harry Enfield
| narrator       =
| starring       = Harry Enfield Kathy Burke Rhys Ifans
| music          = Cecily Fay Philip Pope
| cinematography = Alan Almond
| editing        = Mark Wybourn Tiger Aspect Pictures
| distributor    = Icon Film Distribution
| released       = 21 April 2000 (United Kingdom|UK)
| runtime        = 82 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          = GBP|£10,099,770
| preceded_by    =
| followed_by    =
}} British Teen teen comedy Harry Enfield Stephen Moore as Kevins father.

== Plot ==
 

The film begins at the beheading of Anne Boleyn, which turns out to be an erotic daydream, introducing protagonist Kevin, who is having sexual fantasies when hes supposed to be doing his homework about Boleyn. 
 pornographic magazine, which results in failure, as do all of Kevins attempts at sexual exploration.

The boys come up with the idea to go to Ibiza to become DJs and get "guaranteed Sex", but Kevins parents Ray and Sheila (James Fleet and Louisa Rix) forbid the trip due to Kevins bad grades on his school report. As a compromise however, they tell the boys they can make the trip on the condition that they have to get a job to pay for it.

The boys search fruitlessly for a job, ending up at a house party where Kevin again tries to succeed in finally having sex, and again fails.

Having failed miserably at all his attempts at success with women and gainful employment, Kevin is asked to sign for the delivery of his dads Credit card. Kevin then takes the card to the bank to steal the money for the flights from his dads bank account. At the bank Kevin and Perrys bumbling somehow results in the capture of a bank robber, and the manager awards Kevin and Perry with enough cash to make the trip. With that, they plan for their vacation in Ibiza. 

Once in Ibiza, the boys spot the girls of their dreams Candice (Laura Fraser) and Gemma (Tabitha Wady). They also meet Eye Ball Paul (whose nickname derives from his practice of vodka eyeballing). The boys spend the day with the girls and although they are unsuccessful at having sex with them, they do begin to bond with them as friends.

That night, the boys walk down the high street filming the events around themselves. Kevin then films a couple snogging which turns out to be Ray and Sheila. Kevin sulks while he and Perry hang out with Ray and Sheila, finally ending up at the club "Amnesia" where they dance the night away. Their new friends, Candice and Gemma are refused entry by the doorman (Paul Whitehouse), citing their appearance as a reason.

The next day, the boys go to Pauls where he makes them clean his house in return for his listening to their tapes. Leaving Pauls house, they spot Candice and Gemma, but again their clumsy attempts at wooing them end up in failure.

After the girls have a makeover, they are admitted to Amnesia with Kevin and Perry. However, events again conspire to ruin their chance at romance with the girls. Later, Kevin videotapes Ray having sex with Sheila. The following day, Paul listens to their music as the boys clean his kitchen. He stumbles across the taping of Ray and Sheila and shows it to everyone, resulting in Kevin ending his friendship with Perry after realizing he was responsible for filming them. 

After a day of Kevin and he sulking around unhappily, Perry runs into Paul, who tells him that he likes their song, and that he will play it in the club. Perry tells Kevin the good news and the friends reconcile. That night they and the girls go to the club again. Their song becomes an instant club favourite. Paul becomes unhappy at Kevin and Perrys success, and turns off the song and makes the crowd angry. Kevin and Perry the take over as DJs with the club dancing through the night. Later that night, Kevin and Perry finally lose their virginity to Candice and Gemma.

In the Epilogue, the boys are seen signing copies of their record in a music shop, whilst Kevins parents are seen signing copies of a video they have made about better mid-marriage sex.

== Cast ==
{| class="wikitable"
! Cast
! Characters
|- Harry Enfield Kevin the Kevin
|- Kathy Burke Perry
|- Rhys Ifans Eyeball Paul
|- James Fleet Dad (Ray)
|- Louisa Rix Mum (Sheila)
|- Laura Fraser Candice
|- Tabitha Wady Gemma
|- Steven ODonnell Steve ODonnell Big Baz
|- Natasha Little Anne Boleyn
|- Anna Shillinglaw Bikini Girl
|- Badi Uzzaman Norma Baxter
|- Kenneth Cranham Vicar
|- Sam Parks Police Officer
|- Frank Harper Robber
|- Mark Tonderai Music Store Boss
|- Amelia Curtis Sharon
|}

==Music production==
Enfield tried to use songs in the film that were top of the dance charts at the time.

DJ Judge Jules created the mix that Kevin and Perry create in the film, "Big Girl", which was subsequently released as a single credited to the Precocious Brats with Kevin And Perry. It was in the top 40 for 4 weeks in 2000, and reached #16 in the UK Singles Charts.
 Ford Focuses, and all of the people shown outside are wearing red baseball caps and using Flymo lawnmowers.

In the film, Eyeball Paul is a wealthy and very successful DJ, however, actor Rhys Ifans admits he is a terrible DJ, but thanks to "the magic of film" hell "look pretty sharp out there".

==Soundtrack==
 
This is a list of all the songs used for the soundtrack in the film (taken from the films credits).

Since being set largely in Ibiza, the film featured are large number of dance and techno tracks and DJs that were popular at the time
* Y:Traxx - "Mystery Land (Sickboys Courtyard Remix)"
* The Precocious Brats feat. Kevin & Perry - "Big Girl (All I Wanna Do is Do It!)"
* Fatboy Slim - "Love Island (4/4 Mix)"
* The Wiseguys - "Oh La La" Underworld - "King of Snake (Fatboy Slim Remix)"
* CRW - "I Feel Love (R.A.F. Zone Mix)"
* Jools Holland & His Rhythm & Blues Orchestra feat. Jamiroquai - "Im In the Mood for Love"
* Fragma - "Toca Me"
* Fragma - "Toca Me (In Petto Mix)" Ayla - "Ayla (DJ Taucher Remix)"
* Sunburst - "Eyeball (Eyeball Pauls Theme)"
* Yomanda - "Sunshine (Hi-Gate remix)" Oasis - "Wonderwall" (excerpts performed by Mr. & Mrs. Patterson)
* Mauro Picotto - "Lizard (Claxxix Mix)" The Look of Love" Jada - "Another Day (Perfecto Dub Mix)"
* Samuel Barber - "Barbers Adagio For Strings" Hybrid feat. Chrissie Hynde - "Kid 2000"
* The Precocious Brats feat. Kevin & Perry - "Big Girl (All I Wanna Do is Do It!) (Yomanda Remix)"
* Phil Pope & Los Lidos - "Mi Amour"
* Roger Sanchez - "The Partee"
* The Clash - "Straight To Hell" Lange feat. The Morrighan - "Follow Me" Tosca - "Fuck Dubs Parts 1&2"
* Groove Armada - "Chicago" The Birthday Party - "Release the Bats"
* Ver Vlads - "Crazy Ivan"
* Drew Milligan & Stewart Resiyn - "Elixir"
* Nick Bardon & Steve Warr - "Insanity" Martin Smith - "Onslaught"
* Laurie Johnson - "Galliard"
* Mike Hankinson - "Death & The Maiden"
* Nightmares on Wax - "Sweet Harry" / "Emotion"
* Nightmares on Wax - "Ethnic Majority"
* Southside Spinners - "Luvstruck"
* The Precosious Brats feat. Kevin & Perry - "Big Girl (All I Wanna Do is Do It!) (The Shaft Remix)" Signum feat. Scott Mac - "Coming in Strong"

==Venues featured in the film== Amnesia makes an appearance in the film. However, the front view of the club, as shown in the film, is not the same as the actual Amnesia.  The DJ booth that Eyeball Paul uses in the film is not the actual booth of the club, which is sited above the giant "spectrum analyser", seen in the film to the right of the fictitious booth. intu Watford and Charter Place in Watford was also used. Freemans Close in the small village of Stoke Poges in Buckinghamshire was used for the location of the Pattersons house.  Ibiza Airport was briefly used; however, the footage of a Virgin Sun Airlines plane taking off was actually filmed at El Altet airport in Alicante.

Kevin and Perry and Kevins parents apartment is in Santa Eulària des Riu in Ibiza.

The opening sequence where Kevin dreams of saving Anne Boleyn from execution was filmed at Dover Castle. 

==Viewing certificate==
Harry Enfield suspected that the "two uses" of the word "fuck" (there were actually three, all by Rhys Ifans) were the cause of the films BBFC rating being a 15, rather than a targeted 12. He revealed in the films audio commentary that the 15 rating was given because of a scene where a group of men took drugs.  According to BBFC guidelines, scenes of drug-taking often breach the rules of a 12-rated film.

==Box office==
Kevin & Perry Go Large was the top-grossing film at the British box office over the Easter weekend of 2000, taking £2 million and ranking as the number one film in the UK.    The next week the film was knocked off the top spot by Scream 3, but it regained its number one position the week after, in a week in which box office revenues were down due to warm weather.   

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 