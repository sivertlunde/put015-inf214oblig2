On the Double (film)
{{Infobox film
| name           = On the Double
| image          = On_the_Double_1961.jpg
| caption        = 1961 movie poster
| director       = Melville Shavelson Jack Rose
| writer         = Melville Shavelson Jack Rose
| starring       = Danny Kaye Dana Wynter Wilfrid Hyde-White Margaret Rutherford Diana Dors
| music          = Leith Stevens
| cinematography = Harry Stradling Geoffrey Unsworth
| editing        = Frank Bracht
| distributor    = Paramount Pictures
| released       =  
| runtime        = 92 min
| country        = United States
| awards         =
| language       = English
| budget         =
}}
 1961 film, Jack Rose. It stars Danny Kaye who plays, as in many of his films, two roles&nbsp;— in this case, an American soldier and a British General.

==Plot==
The year is 1944. The place is an Allied army camp in southeast England, full of soldiers waiting for the D-day invasion.

Enter Ernie Williams, a most reluctant American soldier. A hypochondriac and a coward, Ernie does have one talent; he is a superb mimic. One night in the mess, he is persuaded to impersonate the commander, General Sir Lawrence MacKenzie Smith. Using this persona, he attempts to go AWOL, but does not get further than the gate.

With a court-martial staring him in the face for impersonating an officer, Ernie is in big trouble. But Colonel Somerset (Wilfrid Hyde-White) of Military Intelligence recruits Ernie for "Operation Dead Pigeon". He is to impersonate the General (to whom he has a striking resemblance) for real, in order to flush out a suspected plot to kill the General, and unmask several officers known to be German agents.

Ernie is unable to fool the Generals wife, Lady Margaret (Dana Wynter), but he is able to deceive the officers at a regimental party. On her advice, and to further the conceit, he appears to get drunk and has a fight with his wife. His hard-drinking Scottish aunt, Lady Vivian (Margaret Rutherford) is highly suspicious, but is too drunk to take it further.

The General also has a mistress, Sergeant Stanhope (played by blonde bombshell Diana Dors), but Ernie finds out too late that she is a "plant" - a German agent. With her connivance, he is kidnapped and taken to Berlin. There, he manages to confuse the German interrogators, who want information about the projected Normandy landings, by talking arrant nonsense about non-existent operations and places. In the resultant confusion, he manages to escape and is chased through the streets of Berlin at night.
 The Blue Angel). Finally, disguised as a pilot, he boards a German bomber and manages to parachute out over England. Captured and about to be shot as a spy, he is rescued at the last moment by Somerset.

By the end of the movie, Ernie and Margaret are in love (the real General having been shot down in a plane).

==Cast==
*Danny Kaye as Private First Class Ernie Williams / General Sir Lawrence MacKenzie-Smith
*Dana Wynter as Lady Margaret MacKenzie-Smith
*Wilfrid Hyde-White as Colonel Somerset
*Margaret Rutherford as Lady Vivian
*Diana Dors as Sergeant Bridget Stanhope
*Allan Cuthbertson as Captain Patterson Jesse White as Corporal Joseph Praeger
*Gregory Walcott as Colonel Rock Houston
*Terence De Marney as Sergeant Colin Twickenham
*Rex Evans as General Carleton Brown Wiffingham
*Rudolph Anders as Oberkommandant
*Edgar Barrier as Blankmeister
*Ben Astar as General Zlinkov

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 