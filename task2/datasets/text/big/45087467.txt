Bharatamlo Bala Chandrudu
{{Infobox film
| name           = Bharatamlo Bala Chandrudu
| image          =
| caption        =
| writer         = Ganesh Patro  
| story          = Viyatnamveedu Sundaram
| screenplay     = Kodi Ramakrishna
| producer       = D.Kishore Murali Mohan  
| director       = Kodi Ramakrishna
| starring       = Nandamuri Balakrishna Bhanupriya Chakravarthy
| cinematography = K. S. Hari
| editing        = Suresh Tata
| studio         = Jayabheri Art Productions
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Telugu Action film produced by D. Kishore on Jayabheri Art Productions banner, presented by Murali Mohan, and directed by Kodi Ramakrishna. Starring Nandamuri Balakrishna and Bhanupriya in the lead roles, its music is composed by K. Chakravarthy|Chakravarthy.      

==Cast==
*Nandamuri Balakrishna as Balachandra
*Bhanupriya
*Rao Gopal Rao 
*Suresh Oberoi 
*Murali Mohan as Inspector Ranjeeth Kumar
*Giri Babu  Ranganath as DFO Chandra Shekar
*Jaya Sudha  Rajyalakshmi  Poornima
*Y.Vijaya

==Soundtrack==
{{Infobox album
| Name        = Bharatamlo Bala Chandrudu
| Tagline     = 
| Type        = film Chakravarthy
| Cover       = 
| Released    = 1988
| Recorded    = 
| Genre       = Soundtrack
| Length      = 25:14
| Label       = Cauvery Audio Chakravarthy
| Reviews     =
| Last album  = Tiragabadda Telugubidda   (1988) 
| This album  = Bharatamlo Bala Chandrudu   (1988) Ramudu Bheemudu   (1988)
}}

Music composed by K. Chakravarthy|Chakravarthy. Music released on Cauvery Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!lyrics !!Singers !!length
|- 1
|Porusham Naapallavi
|C. Narayana Reddy SP Balu
|4:11
|- 2
|Chilakamma Chettekki  Veturi Sundararama Murthy SP Balu, S. Janaki
|4:06
|- 3
|Ding Dong  Veturi Sundararama Murthy SP Balu,S. Janaki
|4:20
|- 4
|Jingidi Jingidi Siggullo Veturi Sundararama Murthy SP Balu,S. Janaki
|4:25
|- 5
|Tangumani Mogindi Ganata Veturi Sundararama Murthy SP Balu, P. Susheela
|4:08
|- 6
|Ye Laali Paadali Jonnavithhula Ramalingeswara Rao SP Balu,P. Susheela
|4:04
|}

==Others== Hyderabad

==References==
 

 
 
 


 