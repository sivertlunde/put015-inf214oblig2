Beowulf & Grendel
 
{{Infobox film
| name           = Beowulf & Grendel
| image          = beowulf2.jpg
| caption        =
| director       = Sturla Gunnarsson
| producer       = Michael Cowan Sturla Gunnarsson Eric Jordan Anna María Karlsdóttir Jason Piette Paul Stephens
| writer        = Andrew Rai Berzins
| starring       =  
| music          = Hilmar Örn Hilmarsson
| cinematography = Jan Kiesser
| editing        = Jeff Warren
| distributor    = Truly Indie
| released       =  
| runtime        = 102 minutes
| country        = Canada Iceland United Kingdom
| language       = English
| budget         =
}}
 epic poem Beowulf. Filmed in Iceland and directed by Sturla Gunnarsson, it stars Gerard Butler as Beowulf, Stellan Skarsgård as Hrothgar, Ingvar Eggert Sigurðsson as Grendel and Sarah Polley as the witch Selma. The film is a cooperative effort between Eurasia Motion Pictures (Canada), Spice Factory (UK), and Bjolfskvida (Iceland). The screenplay was written by Andrew Rai Berzins. The soundtrack was composed by Hilmar Örn Hilmarsson. The story takes place in the early half of the sixth century AD in what is now Denmark, but the filming of the movie in Iceland provided many panoramic views of that countrys landscape.

While some of the film remains true to the original poem, other plot elements deviate from the original poem: three new characters, Grendels father, the witch Selma, and Grendels son are introduced, and several related plot points were developed specifically for the film.

In 2006, a documentary of the making of Beowulf and Grendel, called Wrath of Gods, was released and went on to win six film awards in Europe and the U.S.

==Plot==
In 500 A.D., Hrothgar, king of Denmark, and a group of warriors chase a large and burly man, whom they consider a troll, and his young son, to the edge of deep cliff. The father directs his young son, Grendel, to hide from the attackers view; whereupon The Danes shoot the father dead, and his dead body plunges onto the beach far below. The Danish king sees the young Grendel, but spares him. Later, Grendel finds his fathers body and cuts the head off to take it home. Many years later, the severed (and mummified) head is inside a cave where the boy Grendel has become as large and powerful as his father, and plans revenge.

When Hrothgar finds twenty of his warriors killed inside his great hall, the Danish king falls into a depression. Beowulf (hero)|Beowulf, with the permission of Hygelac, king of Götaland|Geatland, sails to Denmark with thirteen Geats to slay Grendel for Hrothgar. The arrival of Beowulf and his warriors is welcomed by Hrothgar, but the kings village has fallen into a deep despair and many of the pagan villagers convert to Christianity at the urging of an Irish monk. While Grendel does raid Hrothgars village during the night, he flees rather than fight. Selma the witch tells Beowulf that Grendel will not fight him because Beowulf has committed no wrong against him. A villager, recently baptized and thus now unafraid of death, leads Beowulf and his men to the cliff above Grendels cave. When the villager is found dead, Beowulf and his men return with a rope and gain entry to Grendels secret cave, where Beowulfs men mutilates the mummified head of Grendels father. That night, Grendel invades Hrothgars great hall, kills the Geat who desecrated his fathers head, and leaps from the second story, but is caught in a trap by Beowulf. Grendel, refusing capture, escapes by severing his captive arm, and dies near the site of his fathers death, where his body is claimed by a mysterious webbed hand. Thereafter Hrothgar admits to Beowulf that he had killed Grendels father for stealing a fish but had spared the child Grendel out of pity. Grendels severed arm is kept by the Danes as a trophy. In revealing more about Grendel, Selma recounts that Grendel had once clumsily raped her and has protected her since that day; and Beowulf becomes her paramour. 

The Danes are later attacked by Grendels mother, the Sea Hag; and Beowulf slays her with a sword from among her treasure, but observes the battle watched by Grendel and Selmas child. Later Beowulf, with Grendels son watching, buries Grendel with ceremony. Shortly thereafter, Beowulf and his band of Geats leave Denmark by ship, and warn Selma that she must hide her son, lest the Danes destroy him.

==Cast== Beowulf
*Stellan Hrothgar
*Sarah Polley as Selma
*Ingvar Eggert Sigurðsson as Grendel
*Tony Curran as Hondscioh Wealhtheow
*Martin Martin Delaney as Thorfinn
*Jon Gustafsson as Warrior
*Ólafur Darri Ólafsson as Unferth

==Themes==
 
The film attempts to retell the classic tale as a historical epic. Andrew Rai Berzins, in his blog,  states that he intended that Grendel resemble a sasquatch. Other viewers feel that Grendel and his father are remnants of Neanderthals survived in isolated populations in the far north.  Beowulf as presented constantly doubts the Danes assertion (and later, that of his own men) that the troll is purely evil. Accordingly Beowulf deeply regrets the need to destroy Grendel.

Another theme of the film is that of Christianitys introduction into pagan civilization.   As Grendels reign of terror continues with no end in sight, the people of the village turn away from their Norse gods, which seem to offer no help, to the Christian Jesus, who they are told forgives all.

==Reception==
The film received generally mixed and average reviews from professional critics; it scored 53% at Metacritic and 48% at Rotten Tomatoes. Most critics praised the films cinematography, its brutal action sequences, and aspects of its revisionist script, but criticised the dialogue and some of the acting.

William Arnold of the Seattle Post-Intelligencer writes, "The films near-fatal flaw is its dialogue, which had to be invented wholesale from the Old English text. It alternates between sounding stagy and anachronistically hip with more overuse of the F-word than any two Samuel L. Jackson movies. Its a big mistake." Nevertheless, Arnold eventually recommends the film for "keeping its strain of rowdiness and violence in control, and lending the tale the kind of somber respect filmmakers tend to give adaptations of Shakespeare and Dickens." 
 Kingdom of Tristan & Isolde." 

The film has at least one major champion in  , and Leones Once Upon a Time in America." He hails the film for its reinterpretation of the poem as "a study of   sharp contrast   our ego-inflated perception versus the more humbling reality of our existence.... Gunnarsson and Berzins ultimate conclusion is that we are creatures of the world, not creatures above or below it, and for all of our theology and philosophy and courage and civility, there is Grendels severed arm nailed to our castle, and this trophy makes us feel good about ourselves. Its gory depravity representing our feelings of triumph transforms into one of the most revealing metaphors in all of literature." 

Other critics are less forgiving. Mick LaSalle of the San Francisco Chronicle says, "Imagine the worst Deadwood (TV series)|Deadwood episode ever, and youll get an idea of the general tone of Beowulf & Grendel, which is full of anachronistic cursing, tortured syntax, dark humor and lots of hairy, homely, filthy-looking people. The filmmakers get their point across in about 30 minutes, leaving 70 more for severed heads and period charm. Theres no charm." 

Todd McCarthy of Variety (magazine) agrees, writing, "Director Sturla Gunnarsson seems aware of the savagery intrinsic to the story, but is unable to mine it deeply, proving too genteel in the end to make a genuinely creepy or disturbing film."  Liam Lacey of The Globe and Mail concludes, "The movie is a lumbering and ludicrous mess." 

==References==
 

==External links==
* 
* 
*  
*   at Metacritic

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 