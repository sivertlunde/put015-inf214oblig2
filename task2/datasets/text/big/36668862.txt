The Yankles
{{Infobox film
| name           = The Yankles
| image          = The_Yankles.jpg
| caption        = Theatrical poster
| director       = David R. Brooks
| producer       = David R. Brooks Michael Morhaime
| writer         = David R. Brooks Zev Brooks
| starring       = Brian Wimmer   Michael Buster   Susanne Sutchy
| music          = Eddie Hernandez
| cinematography = Boris Price
| editing        = Lance Chapman
| studio         = 
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 115 minutes
| language       = English
| country        = United States
| budget         =
| gross          =
}} Orthodox Yeshiva.

Filmed in Utah with a "predominantly   for DVD release. 

==Plot and title== Orthodox Jewish yeshiva (theological school) decides that the school should have a baseball team to compete in the college league. Elliot (Michael Buster), one of the students with a background in minor league baseball, is able to convince Charlie Jones (Brian Wimmer), a former major league player who had been imprisoned as a result of DUI convictions—and now has to perform almost 200 hours of community service—to coach the team. As he coaches the team, he gives the players "a new measure of skill and confidence," as they help him regain his self-respect. 

The title is the name of the ball team, based on Yankle as the Yiddish language nickname for Jacob, used as a play-on-words with the famous ball team, the New York Yankees|Yankees.     An additional pun is reflected in the words painted on the school bus the yeshiva ball team uses: "In the Big Inning." 

==Themes==
According to publicity releases, the film is "not just a funny film about baseball; its about second chances, reconciliation and forgiveness....about making mistakes and moving on, about living by principles and not passions." 

==Production== The Chosen" brought to mind a movie scene with Orthodox and non-Orthodox students playing baseball.    They agreed the image was "visually entertaining" and built the film around that image. 

Once the script was completed, the brothers established "DZB productions" and began seeking out investors to fund the film. When it became clear that shooting a low-budget independent film in Los Angeles would be too expensive for them, they made the decision to film the movie in Utah, using many local men and women for cast and crew.   According to the films creators, the decision to film in Utah was made based on a combination of economic factors, the cooperation they would receive from the Utah Film Commission, and the large number of actors and film crew members who lived in that State.    Evidently because of the large number of Mormon actors and crew members involved in the production, the film (featured in many Jewish film festivals) was included in the "LDS Film Festival" as well. 

While the brothers co-wrote the film, David directed it and Zev produced it.  The movie was filmed on a tight schedule, and because the actors were not trained baseball players—and there was no time on the schedule to hold a training camp to improve their skills—the number of actual scenes of ball games were limited. 

The script was completed by the two brothers in 1996, and most of the film was completed by 2007, but a post-production lack of funding postponed the films completion until 2009. 

===DVD extras===
The DVD includes an 11-minute behind-the-scenes film, 11 minutes of extended musical numbers, and 16 minutes of deleted scenes. 

==Cast==
The cast includes: 
*Brian Wimmer (Charlie)
*Michael Buster (Elliot)
*Susanne Sutchy (Deborah)
*Don Most (Frankie)
*Bart Johnson (Sledge)

Brian Wimmer and Susanne Sutchy, two of the main stars of the movie, are married in real life. 

==Awards==
 
The awards this film has received include: 
*Golden Ace Award, Las Vegas Film Festival, 2010
*Best Comedy, International Family Film Festival, 2010
*Audience Choice: Best Feature, Palm Beach International Film Festival, 2010
*Audience Award: Best Feature, Los Angeles Jewish Film Festival, 2010
*Audience Choice: Best Feature, Anaheim International Film Festival, 2010
*Best Director (David R. Brooks), California Independent Film Festival, 2010
*Heartland Crystal Heart Award
*Best of Fest, New Jersey Jewish Film Festival, 2011
*Audience Choice Award, Best Feature Film, TriMedia Film Festival,2011

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 