New Movietone Follies of 1930
{{Infobox film
| title          = New Movietone Follies of 1930
| image          = New Movietone Follies of 1930.jpg
| caption        = Theatrical release poster
| director       = Benjamin Stoloff William Fox
| writer         = William K. Wells
| narrator       =
| starring       = El Brendel Marjorie White William Collier, Jr. Miriam Seegar Huntley Gordon Yola dAvril Joseph McCarthy George Lipschultz
| cinematography =
| editing        =
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

New Movietone Follies of 1930 is a 1930 American musical film released by Fox Film Corporation, directed by Benjamin Stoloff. The film stars El Brendel and Marjorie White who also costarred in Foxs Just Imagine in 1930.  

The film is a follow-up to Fox Movietone Follies of 1929 and has sequences filmed in Multicolor. An archival 35mm print of the film is in the collection of the UCLA Film and Television Archive. 

==Cast==
*El Brendel - Alex Svenson
*Marjorie White - Vera Fontaine
*Frank Richardson - George Randall
*Noel Francis - Gloria de Witt
*William Collier, Jr. - Conrad Sterling
*Miriam Seegar - Mary Mason
*Paul Nicholson - Lee Hubert
*Huntley Gordon - Marvin Kinsley
*Yola dAvril - Maid
*Betty Grable - Chorine (uncredited)

==See also==
*List of early color feature films

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 

 