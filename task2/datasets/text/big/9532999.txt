Wasabi Tuna
{{Infobox film
| name           = Wasabi Tuna
| image          = Wasabi Tuna.jpg
| caption        = Lee Friedlander
| writer         = Celia Fox
| starring       =
| producer       = Lee Friedlander Celia Fox
| music          = Raymond Jones
| cinematography = Ben Kufrin
| editing        = Larry Madaras Ken Morrisey
| distributor    = Cafe Entertainment Studios
| released       =  
| runtime        = 93 mins.
| language       = English
| budget         =
}} 2003 independent independent Cafe action comedy comedy film|motion Guillermo Díaz, Alexis Arquette, and Arturo Gil. Anna Nicole Smith is featured as herself.
 Directed and produced by Lee Friedlander, executive produced.

Wasabi Tuna premiered on August 20, 2003. It was issued on DVD on October 28, 2005, by Indican Pictures.

== Plot ==
 camp screwball screwball comedy of errors, which is full of over-the-top stereotypes, is set at Halloween against the backdrop of West Hollywood, California.
 spinning instructor Fredrico (played by Sabàto); and Emme (played by Ubach), a sassy young woman obsessed with style and classic movies, have all been anticipating the West Hollywood Halloween Parade on Santa Monica Boulevard, which is the biggest and brashest block party of the year. Their annual tradition of joining the festivities in outrageous costumes has become part of local legend.
 femme gay boyfriends and business partners, have come up with food-themed sushi costumes that everyone refers to as "wasabi tuna." Emme offers another suggestion &mdash; that they all dress up as gang members.

They all opt for the gang theme, but are unable to find the appropriate attire at the local stores. They then decide to seek out real life gang members in order to achieve the authentic, straight-from-the-hood look they want.
 Armenian woman WeHo Los Angeles County Sheriffs Department|sheriffs station with them attempting to talk their way out of trouble.
 East L.A., where he comes across a real gang member named Romeo (played by Díaz). He makes a deal with Romeo to loan him his Porsche in exchange for his lowrider gang car. A case of mistaken identity makes a vengeful, rival gang get after them. There is a comic drive-by shooting scene. Also, by driving Romeos lowrider they are unknowingly carrying an illegal cache of weapons.
 Reality TV actress Anna Nicole Smiths (appearing as herself) pet dog, Sugar-Pie, is kidnapped. All is havoc, as a group of drag queens who dress like Smith, Santa Ana Anna (played by Arquette), Eenie Anna (played by Gil, who is a dwarfism|dwarf), Brown Sugar Anna and Hot Spicy Anna, believe that Harvey stole their idols dog and band together to get Sugar-Pie back for Smith.
 Chinese man, street fight, DEA detectives, a trio of female martial arts gymnasts and the Santa Ana Annas, erupts and everyone battles it out with flying fists, feet and wigs.

Anna Nicole Smith herself arrives and saves the day. As she shamelessly mocks herself, Smith clears up the Sugar-Pie mess, hands the drugs over to the police and invites the group to her Halloween party, where they change into their "wasabi tuna" costumes.

==External links==
* 
*  at Rotten Tomatoes
*  at Yahoo! Movies
* 
* 
* 

 
 
 
 