Sabre Jet (film)
{{Infobox film
| name = Sabre Jet
| image = Sabjetpos.jpg 
| caption = Original film poster
| director = Louis King
| producer = Carl Kruger Edward Small (uncredited)
| writer = Story: Carl Kruger Screenplay: Dale Eunson Katherine Albert
| narrator =
| starring = Robert Stack
| music = Herschel Burke Gilbert
| cinematography = Charles Van Enger
| editing = 
| distributor = United Artists
| released = 1953
| runtime = 90 minutes
| country = United States English
| budget =
| preceded_by =
| followed_by =
| website =
}}
 1953 Korean War war film shot in Cinecolor and released by United Artists.  It stars Robert Stack and was directed by Louis King. It was based on a story by the producer Carl Kruger with the screenplay written by the husband and wife playwright and screenwriting team of Dale Eunson and Katherine Albert.  The film was mostly shot at Nellis Air Force Base near Las Vegas  and featured extensive use of actual combat film footage.

==Plot== squadron leaders, Colonel Gil Manton.  This is news to everyone.

Gil and Jane have been separated for two years.  Jane prefers life under her former name as a major journalist with frequent travel, while Gil prefers a wife who will stay home and have a family.  Gil is not only upset that Jane left their anniversary celebration to get a story from the wife of a Death Row prisoner about to be executed, but Gil feels Jane callously used and exploited the woman for a story.  Gil has kept their separation a secret as a divorce would hurt his career.

Jane meets the wives and learns their motivations and that though they are open with each other, they hide their fears from their husbands lest it affect their performance.
 synchronized combined airstrike of Boeing B-29 Superfortress bombers escorted by North American F-86 Sabre to deal with the enemy aircraft and F-80 Shooting Star  ground-attack aircraft to destroy the enemy’s Anti-aircraft warfare|anti-aircraft defenses. General Hale’s superiors are sympathetic but inform the General that approval for such a massive combined operation can take a long time to approve.  General Hale replies that the wet season in North Korea will begin in a week that would make the operation impossible after it has begun.   

General Hale disobeys orders by personally flying an F-86 Surveillance aircraft| reconnaissance aircraft without escort over the target.  After he is aloft, intelligence discovers the airbase is located elsewhere and that what the General is flying over is a trap full of anti-aircraft and enemy fighter planes.  

The General is shot down with Gil taking command of the wing to lead the operation. Jane is with the Generals wife Marge when Gil breaks the news of the loss of the General.  Marges incredible composure and courage brings Jane to tears that makes her reevaluate her marriage and behavior towards Gil.

==Cast==
*Robert Stack  ...  Col. Gil Manton 
*Coleen Gray  ...  Mrs. Gil Manton aka Jane Carter
*Richard Arlen  ...  Gen. Robert Hale  Julie Bishop  ...  Mrs. Marge Hale  Leon Ames  ...  Lt. Col. George Eckert 
*Amanda Blake  ...  Helen Daniel 
*Reed Sherman  ...  Lt. Ronnie Crane 
*Michael Moore  ...  Sgt. Klinger 
*Lucy Knoch  ...  Lee Crane
*Tom Irish  ...  Lt. Bill Crenshaw 
*Kathleen Crowley  ...  Susan Crenshaw 
*Jerry Paris  ...  Capt. Bert Flanagan
*Jan Shepard as Betty Flanagan

==Production==
The film used a number of flying aces as advisers. MOVIELAND BRIEFS
Los Angeles Times (1923-Current File)   17 Apr 1953: B8 
==Notes==
 

==External links==
* 
 
 
 
 
 
 
 