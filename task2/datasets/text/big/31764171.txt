Marianne (2011 film)
 
 
{{Infobox film
| name           = Marianne
| image          = MarianneMoviePoster.jpg
| alt            = Movie Poster with cast in it
| caption        = Teaser poster
| director       = Filip Tegstedt
| producer       = Filip Tegstedt Alexandra Malmqvist
| screenplay     = Filip Tegstedt
| starring       = Peter Stormare Thomas Hedengran Dylan M. Johansson Tintin Anderzon Viktoria Sätter Sandra Larsson 
| music          = Mikael Junehag
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   }} 
| runtime        = 
| country        = Sweden
| language       = Swedish
| budget         = $150,000 
| gross          =
}} 2011 Fantasia International Film Festival on 2 August 2011.   Thomas Hedengran plays a father who is plagued by guilt over the death of his wife.

== Plot ==
  Mare that is haunting him.

== Cast ==
*Thomas Hedengran as Krister, a teacher who begins to experience strange nightmares while trying to redeem his relationship with his daughter Sandra.
*Dylan M. Johansson as Stiff, the pothead older boyfriend of Sandra who has an intense interest in Swedish Folklore. 
*Peter Stormare as Sven, the therapist that Krister sees to help resolve his nightmares.
*Tintin Anderzon as Eva, the wife of Krister and the mother of Sandra.
*Viktoria Sätter as Marianne, the titular creature of Nordic Folklore whom Stiff believes is responsible for Kristers nightmares.
*Sandra Larsson as Sandra, the daughter of Krister and Eva who resents her father for cheating on her Mother.

== Production ==
Filip Tegstedt has commented in numerous interviews that he wanted to make a film that explores Swedish folklore surrounding the concept of the Mare, traditionally thought to be responsible for the contemporary medical phenomenon of sleep paralysis. This is a condition where patients report waking up paralyzed, unable to move, whilst feeling a presence in the room like someone is watching them. The paralysis often comes with the sense of a heavy weight on their chest that makes it difficult to breathe.  

Contributing to the development of this film was a reaction against the funding priorities of the Swedish Film Institute. Tegstedt has remarked that part of the reason why there are no good horror films in Sweden is that horror films are seen to be, culturally speaking, a lower class of film, less deserving of funding. In reaction to this perceived genre hierarchy, Tegstedt decided to liquidate all his assets in order to finance the production of Marianne and take a high interest loan. 

While other producers also offered to finance his debut feature, they were only willing to do this on the condition that Marianne was filmed in Stockholm. Filip felt that a key part of the story was the northern location of Östersund, and the fact that it was his hometown, and that he wanted the film to be as authentic to his vision as possible. 

== Release ==
Tegstedt has remarked that the marketing strategy depends on getting press and acceptance in film festivals. 

The film premiered at Fantasia International Film Festival on 2 August 2011. 

== Reception ==
 
Marianne has received positive reviews. Garry McConnachie of MCMBUZZ wrote that "director Tegstedt gets his character development spot on and his use of sound and visuals ensure viewers will be on the edge of their seats when its required", and that "Marianne is an extremely accomplished debut that will get under the skin".  Kurt Halfyard of Twitch Film described Marianne as "a film that subtly uses the language of domestic drama to craft a realistic and true horror film", and also stated that this  "is a brave thing to attempt with a first feature". Kurt Halfyard was so impressed that he added "Somebody, please, get Guillermo del Toro in contact with Filip Tegstedt because here is a young director with the chops to make a The Devils Backbone or a Pans Labyrinth if he were given the finances and freedom to do so.  In fact, he may well already have done the former with Marianne."    Scott Weinberg of Fearnet wrote, "Although probably not recommended for the hardcore gorehounds, the creepy, insightful and effectively melancholy Marianne is a quiet little winner."   Michael Gingold of Fangoria wrote, "Tegstedt develops a sense of mystery and creeping fear that balance the sadness and make MARIANNE an absorbing viewing experience." 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 