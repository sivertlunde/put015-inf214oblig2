The Reckless Driver
{{Infobox Hollywood cartoon|
| cartoon_name     = The Reckless Driver Woody Woodpecker 
| image            = Recklessdriver TITLE.jpg
| caption          =  James Culhane
| story_artist     = Ben Hardaway Milt Schaffer
| animator         = Terry Lind Grim Natwick Emery Hawkins Lester Kline
| voice_actor      = William Wright Ben Hardaway
| musician         = Darrell Calker
| producer         = Walter Lantz
| studio           = Walter Lantz Productions Universal Pictures
| release_date     = August 26, 1946 (United States|U.S.)
| color_process    = Technicolor
| runtime          = 6 43"
| movie_language   = English
| preceded_by      = Bathing Buddies
| followed_by      = Fair Weather Fiends
}}
 Universal Pictures.

==Plot==
 
Woody discovers that his drivers license is due for renewal, and quickly heads for the nearest Department of Motor Vehicles. Upon arrival, Woody  attempts to awaken sleeping Officer Wally Walrus (voiced by William Wright) so he can complete the test.

First up is the eye test, complete with an eye chart whose letters spell out "I CANT SEE A THING." Then comes the reflex test, in which Woody violently pecks at Wallys head whenever he gets his knee jabbed. Next, Wally tries to get a fingerprint from Woody, only to have the two stack their hands high into the air.

Finally, Woody must complete the actual driving test. The careless woodpecker accidentally backs up into a wall. A fire extinguisher then falls off the wall and onto the back of Woodys car, providing him with steam propulsion. With this new power, Woody starts zooming in the air and in and out of the office, driving Wally insane. At the conclusion of the driving test, Woody blurts out, "Say, Ive changed my mind. I want a pilots license".

==Notes==
*The normally Swedish Wally Walrus sports a New York accent in The Reckless Driver. 
*Many prints omit the alphabet soup-spitting scene.
*Woody sings "The Old Gray Mare" while driving through the countryside.

== External links ==
*  .
*  

==References==
*Cooke, Jon, Komorowski, Thad, Shakarian, Pietro, and Tatay, Jack. " ". The Walter Lantz Cartune Encyclopedia.

 
 
 
 
 
 