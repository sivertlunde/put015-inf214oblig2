Peter-No-Tail (1981 film)
{{Infobox film
| name           = Peter-No-Tail  (original title: ) 
| image          = Peter-No-Tail (film).jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Original Swedish film poster
| film name      = 
| director       = Stig Lasseby Jan Gissberg
| producer       = Stig Lasseby
| writer         = 
| screenplay     = Leif Krantz
| story          = 
| based on       =  
| narrator       = 
| starring       =  
| music          = Berndt Egerbladh
| cinematography = Eberhard Fehmers
| editing        = 
| studio         =   Sandrew
| released       =  
| runtime        = 81 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}

Peter-No-Tail ( ) is a 1981 Swedish film about a cat who does not have a tail. He is born on a farm in the Swedish countryside, but the farms owner is greedy and believes that more cats will cost too much money. He orders one of the workers to have Peter drowned, but the worker cant bring himself to do it. He puts Peter in the car of a family who are renting a house close to the farm for their summer vacation. The family, who live in Uppsala, take care of Peter. Without a tail, he becomes the talk of the town among the other cats, and especially Måns, who teases him cruelly. Måns rough and silly courting of Molly Silk-Nose does not impress her, since she becomes interested in the kind and gentle Peter. But Måns wont give up that easy.

== Plot ==

On a farm five kittens are born. However, the owner wants to keep only a cat, other kittens are adopted in the environment and in the end only a black and white male remains, which also has only a stubby tail. The foreman should drown the cat, but appears at the moment a vacationer from the city. The foreman smuggles the cat in the car. So he reached Uppsala and is received by the family of the holidaymaker at the end. Especially daughter Bridget has closed the cat in her heart and gives him the name of my body. Pelle receives a bow tie and a cap on the head. So Bridget takes him one day on a trip where Pelle of a bulldog is hunted. He takes refuge in a dilapidated backyard, where he met the common hangover Mike mice fright and his two henchmen Bill and Bull. Mike makes fun of Pelles missing tail.

The lovely cat Molly cream nose appear on the court and Pelle is fascinated by her. Mike wants to trick him and claims that Molly love songs like. In the evening Pelle appears in front of her window and begins to sing. He does not know that the holders of Molly in "caterwauling" to be angry. Molly but warns him in time and invites him to dinner. Mike, who believes Pelle had been punished for his singing, will appear before calling Mollys window and was expelled splashed by her irate owners. Mike is mad at Pelle and invokes a city wide gathering of cats. He claims that as a country Pelle make fun cat cats over the city, since he believed to be much more intelligent. The cats decide attention to Mikes initiative to give skin a lesson.

Pelle receives little later an exclusive invitation to the annual Spring Dance of the cats. While dancing he introduces himself clumsily and is mocked by Mike. He retreats sad and meets a snake that so knotted that threatens to suffocate. Pelle saves her and the serpent promises to help him once. A little later a quiz on the dance festival held where Mike reads computing tasks, the cats have to shape the result as a shadow play with her tail. Although Mike has informed all participants the solutions of simple tasks in advance, fail almost all cats. Pelle turn receives a very difficult computational task. He can solve though, they do not, however, show no tail. The snake rushes to his aid, forms the solution and Pelle wins. Mike is now trying to discredit Pelle with his family and destroyed a birthday cake that was meant for Bridgets brother. Pelle is held responsible for the damage and blocked for punishment in the laundry room. Here the family has started the iron; at night it starts to burn and Pelle rescues his meowing the family. He receives a medal of honor and an extra large piece of cake.

Mike seems to be hardly impressed with the medal count at the end but only a gold medal at the Olympics cats. Pelle is sad, but he has no idea that Mike, these Olympics has been forged. Mike organized the event in which he won by cheating in all disciplines. His deception is revealed at the end and Mike will be disqualified, while Pelle was awarded the gold medal final and win Mollys heart. During a furious speech to the cats Mike calls shortly after the expulsion of all land cats out of town. This was agreed hesitantly. The cats lurking in Pelle and Mike rushes the bulldog on it. Pelle but has noticed that the child of Frida cat climbed on scaffolding and in danger. Despite the attacks Bulldog succeeds Pelle risking their lives to save the kitten. The Bulldog chases after the end Mike, Bill and Bull. Pelle is celebrated by the cats and incorporated into the city cats Community. The summer months he is with his family but to spend in the country and the foreman was pleased to see him again.

==Origins==
The film is animated and is based on the novel by Gösta Knutsson. Class discrimination, bullying and racism play a big part of the story, much like in the books since Peter is at times looked down on for being a "country cat", who is accused by Måns of threatening the livelihoods of the town cats. In the original Swedish dub Måns is played by Ernst-Hugo Järegård.

== Cast ==
*Mats Åhlfeldt - Pelle Svanslös
*Ewa Fröling - Maja Gräddnos 
*Ernst-Hugo Järegård - Elake Måns 
*Carl Billquist - Bill
*Björn Gustafson - Bull 
*Wallis Grahn - Gammel-Maja i domkyrkotornet 
*Lena-Pia Bernhardsson - Gullan från Arkadien 
*Charlie Elvegård - Laban från Observatorielunden/En råtta 
*Åke Lagergren - Murre från Skogstibble/Kalle Huggorm 
*Nils Eklund - Rickard från Rickomberga 
*Jan Sjödin - Fritz 
*Gunilla Norling - Frida 
*Eddie Axberg - Den tjocka råttan 
*Gunnar Ernblad - Kråkan 
*Kajsa Bratt - Birgitta 
*Niklas Rygert - Olle 
*Helena Brodin - Mamma 
*Axel Düberg - Pappa 
*Sture Hovstadius - ladugårdsförmannen

== English Cast ==
* Steven Pacey
* Peter Woodthorpe
* Gareth Armstrong
* George Little
* Helen Worth

==Sequel==
A sequel called Peter-No-Tail in Americat ( ) was released in 1985.

==Reception==
 

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 


 
 