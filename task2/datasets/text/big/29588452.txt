Where Do We Go Now?
{{Infobox film
| name           = Where Do We Go Now?
| image          = Where do we go now - poster.jpg
| caption        = Poster used in Cannes 2011
| director       = Nadine Labaki
| producer       = Anne-Dominique Toussaint
| writer         =  
| starring       =  
| music          = Khaled Mouzanar 

Lyrics by Tania Saleh 
| cinematography = Christophe Offenstein

Stills by Sam Nessim
| studio         =
| distributor    = Les Films des Tournelles
| released       =  
| runtime        = 110 minutes
| language       = Arabic
| country        = Lebanon France Egypt Italy
| budget         = United States dollar|US$6.7 million }}
 Lebanese director Nadine Labaki. The film premiered during the 2011 Cannes Film Festival as part of Un Certain Regard .    The film was selected to represent Lebanon for the 84th Academy Awards,       but it did not make the final shortlist.    The film won the Peoples Choice Award at the 2011 Toronto International Film Festival. 

==Plot== Muslims and Lebanese Christians|Christians. The village is surrounded by land mines and only reachable by a small bridge. As civil strife engulfed the country, the women in the village learn of this fact and try, by various means and to varying success, to keep their men in the dark, sabotaging the village radio, then destroying the village TV.

The story begins with a boy named Roukoz, whose job – along with his cousin, Nassim – is to venture outside the village and bring back much-needed merchandise such as soap, utensils, newspapers, light bulbs. Roukoz lives with Nassims family, and it is made clear that Nassim has lost his father. Roukoz tries to fix the church speakers, and falls off his ladder, crashing into the cross and snapping it in half. Other characters include the village mayor and his wife Yvonne (Christians), the cafe-owner Amal (played by Nadine Labaki), Rabih (the village painter and Amals love interest) and his sister, Issam (Nassims brother) and his wife Aida, and the village priest and the village imam. 
The next day, the congregation is gathered in church to celebrate the Sunday mass; The Priest preaches about the need to fix the church, and blames the broken cross on the wind, telling churchgoers to keep their cool and that their fellow Muslims have nothing to do with it.
Some time later the Imam discovers that some goats have found their ways into the mosque, and urges the Muslims not to blame the Christians for what had happened. As people start to gather, however, a Muslim man blames the Christians for what has happened and a small fight ensues.
 pastries and remove their weapons from the village. This ensured that fighting would not resume in the village during or after Nassims funeral.

==Cast==
* Nadine Labaki as Amale
* Claude Baz Moussawbaa as Takla
* Layla Hakim as Afaf
* Antoinette Noufily as Saydeh
* Yvonne Maalouf as Yvonne
* Adel Karam as the bus driver
* Mustapha Sakka as Hammoudi
* Mustapha El Masri as Hanna

==Production==
The shooting of Where Do We Go Now? lasted for 2 months from 18 October until 18 December 2010.  Khaled Mouzanar, Labakis husband, composed the music for the film. Tania Saleh wrote the lyrics to all the songs in the film. The movie was released in Cannes in May 2011.

Sam Nessim, who helped co-write the film with Labaki, shot the initial stills for the film and was set to be the director of photography at only 18 years old but he was not available during the months of production due to school so Christophe Offenstein stepped in.  

The film was shot in Taybeh a village near Baalbek because the town contains a Church neighboring a mosque, other towns were used during the shooting like Meshmesh, Douma, Lebanon|Douma, and Jeitas Church Al-Saydeh.

==Release==
The film was part of the official selection at the 2011 Cannes Film Festival in the Un Certain Regard parallel competition.   The film was released on 14 September 2011 in France and 22 September 2011 in Lebanon, Syria and Jordan.

==Critical reception==
The New York Times compared the story to Aristophanes Lysistrata.  The Australian, however, said it did not go as far as Lysistrata. 
 Maronite upbringing to go beyond feminism and make allusions to the Blessed Virgin Mary as a rallying force for the women in the film. 

The Detroit News said it was disorienting and disjointed.  Similarly, The San Francisco Gate said the film was "undone by its ungainly mix of heavy-handed comedy and melodrama". 

==Awards and nominations==
*Un Certain Regard official selection during Cannes 2011.
* Prize of the Ecumenical Jury|Ecumenical Special Mentions during the 2011 Cannes Film Festival.  
*Won the François Chalais Prize at the 2011 Cannes Film Festival. 36th Toronto International Film Festival. 36th Toronto International Film Festival
*Selected at the 2011 San Sebastián International Film Festival
*Won the Award To The European Film, Audience Award at the 2011 San Sebastián International Film Festival. http://www.sansebastianfestival.com/in/pagina.php?ap=3&id=2042  Namur Film Festival http://www.rtbf.be/info/regions/detail_fiff-le-bayard-d-or-du-meilleur-film-est-decerne-a-et-maintenant-on-va-ou?id=6879213 
*Won the Audience Award at the Films from the South 2011 International film festival. 
*Won the Audience Award for the Best Narrative film at the 2011 Doha Tribeca Film Festival. 
* Awarded Honourable Mention at 7th Pomegranate Film Festival (Toronto, Canada)
* Awarded Audience Choice Award at 7th Pomegranate Film Festival (Toronto, Canada)

==Further reading==
*Weiss, Max. " ." Jadaliyya, January 9, 2012.

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Lebanese submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 