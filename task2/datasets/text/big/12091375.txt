I-See-You.Com
{{Infobox film
| name     = I-See-You.Com
| director = Eric Steven Stahl
| image	=	I-See-You.Com FilmPoster.jpeg
| writer   = Eric Steven Stahl Sean McLain
| starring = Beau Bridges Rosanna Arquette Shiri Appleby Dan Castellaneta Victor Alfieri Tracee Ellis Ross  Doris Roberts  Héctor Elizondo  Brittany Petros  Benton Jennings  Mary Hart  Lisa Joyner  Shea Curry  Garry Marshall  Don LaFontaine  Mark Anthony Parrish
| producer = Eric Steven Stahl Robert Egan Hector Elizondo Mirela Girardi Mark Anthony Parrish
| editing  = Alexander Egan Stefan Tellegino
| music    = Kevin Kiner
| cinematography = Ricardo Jacques Gale
| budget = $6,200,000
| runtime  = 90 min. English
}}
 comedy film directed and co-written by Eric Steven Stahl, starring Beau Bridges and Rosanna Arquette.

==Plot==
Harvey Bellinger (Beau Bridges), his wife Lydia (Rosanna Arquette), and their two teenage kids live a well-to-do life in suburbia. This changes, however, when their seventeen-year-old son puts video cameras around their house, and starts to broadcast the familys actions live on the internet. When Harvey finds about this, he is angry and appalled. But when he realizes that money can be made with the internet broadcasts, the Bellingers start acting crazier, eventually leading to Harvey blowing up the house to get rid of the cameras.

==Cast==
* Beau Bridges ...  Harvey Bellinger 
* Rosanna Arquette ...  Lydia Ann Layton 
* Mathew Botuchis ...  Colby Allen
* Shiri Appleby ...  Randi Sommers 
* Dan Castellaneta ...  Jim Orr 
* Baelyn Neff ...  Audrey Bellinger 
* Victor Alfieri ...  Ciro Menotti 
* Tracee Ellis Ross ...  Nancy Tanaka
* Doris Roberts ...  Doris Bellinger 
* Héctor Elizondo ...  Greg Rishwain
* Mark Anthony Parrish ...  Stud at talent agency
* Tiffany Baldwin ... Jessica
* William Dixon ... Todd
* Robert A. Egan ... Kløsen Executive #1
* Jeff Halbleib ... Kløsen Executive #2
* Brittany Petros ... Kløsen Executive #3
* Benton Jennings ... HR Executive
* Mary Hart ... Herself
* Lisa Joyner ... Herself
* Shea Curry ... Chloe
* Garry Marshall ... Himself
* Don LaFontaine ... Himself

== References ==
 
 

== External links ==
*  

 
 
 
 
 

 