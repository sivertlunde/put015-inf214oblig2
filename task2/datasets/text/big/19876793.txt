Killer's Carnival
 
{{Infobox film
| name           = Killers Carnival
| image          = 
| caption        =  Robert Lynn Sheldon Reynolds Louis Soulanes
| producer       = Karl Spiehs Rolf Olsen Sheldon Reynolds Vittorio Salerno
| starring       = Stewart Granger
| music          = 
| cinematography = Siegfried Hold
| editing        = 
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = France Austria Italy
| language       = French
| budget         =  gross = 129,757 admissions (France)   at Box Office Story 
}}

Killers Carnival ( ,  ,  ) is a 1966 crime film directed by Alberto Cardone and starring Stewart Granger.   

==Plot==
A murderer takes refuge in a doctors home, and the doctor tells him three stories in an attempt to convince him that crime doesnt pay.

==Cast==
* Stewart Granger - David Porter (Vienna segment)
* Lex Barker - Glenn Cassidy (San Francisco segment)
* Pierre Brice - Agent Brice (Rome segment)
* Karin Dor - Denise (San Francisco segment)
*   - Lotty (Vienna segment) Margaret Lee - Agent Linda (Rome segment)
* Walter Giller - Karl (Vienna segment)
* Johanna Matz - Monique Carrar (Vienna segment)
* Klaus Kinski - Gomez (San Francisco segment)
* Agnès Spaak - Nelly Small (San Francisco segment) Peter Vogel - Wendt, Suspected girls killer Richard Münch - Professor Alden (Frame story)
* Carmen G. Cervera - Joana (San Francisco segment) (as Tita Barker)
* Allen Pinson - Ray Runner (San Francisco segment) (as Alan Pinson)
* Herbert Fux - Ganove (Vienna segment)
* Roberto Miali - Pessana (San Francisco segment) (as Jerry Wilson)
* Carla Calò - Female boss (Rome segment) (as Carrol Brown)
* Fortunato Arena - Taxi driver (Rome segment)
* Pietro Ceccarelli - Gangster #3 (Rome segment)
* Luciano Pigozzi - Ivan (Rome segment)

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 