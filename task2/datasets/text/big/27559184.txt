Security Unlimited
 
 
{{Infobox film
| name           = Security Unlimited
| image          = 
| caption        = 
| film name = {{Film name| traditional    = 摩登保鑣
| simplified     = 摩登保镖
| pinyin         = Módēng Bǎobiāo
| jyutping       = Mo1 dang1 bou2 biu1}}
| director       = Michael Hui
| producer       = Raymond Chow
| writer         = Michael Hui Samuel Hui
| starring       = Samuel Hui Michael Hui Ricky Hui Marylinn Wong Stanley Fung
| music          = Samuel Hui
| cinematography = Cheung Yiu Cho Tom Lau
| editing        = Peter Cheung
| studio         =  Golden Harvest Hui Brothers Films Ltd.
| released       =  
| runtime        = 92 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK $17,769,048
}} 1981 Cinema Hong Kong Best Actor at the 1st Hong Kong Film Awards.

==Plot==
Chou Sai-Cheong (Michael Hui), a bitter supervisor of a Hong Kong private security company, teaches unusual guard tactics to new recruits such as electric mats, parachuting off burning buildings and counter-attacking gunfire. He was secretly observed by his new boss (Stanley Fung) and, unimpressed by his work, the new boss demotes Chou and promotes Chous assistant Sam (Sam Hui). Under the leadership of Sam, Chou and new recruit Bruce Tang (Ricky Hui) encounter a slew of misadventures, including pursuing stowaways on a party boat. Bruce ultimately falls in love with one of the stowaways. Finally, they all get entangled in a plot to steal one of Chinas most prized treasures on display in Hong Kong, and in a plot involving some missing government money that the security officers were guarding.

==Cast==
{| class="wikitable"
|-
! width=50% | Cast
! width=60% | Role
|-
| Samuel Hui || Lieutenant Sam
|-
| Michael Hui || Chow Sai Cheung
|-
| Ricky Hui || Bruce Tang
|-
| Marylinn Wong || Chun
|-
| Stanley Fung || Fan
|-
| Chan Sing || Robber gang thief
|-
| Lau Hak Suen || Taipan Law
|-
| Lee Hoi Sang || Robber
|-
| Feng Feng || driving instructor
|-
| Bill Tung || race course commentator
|-
| Yue Tau Wan || robber
|-
| Wong Man || Taipan Laws wife
|-
| Tsang Choh Lam || Fake corpse
|-
| Hui Sai Cheung || Wong Security Boss
|-
| Wong Chik Sam || Sergeant
|-
| Joe Junior || sniper with ice bullets
|-
|}

==Award==
1st Hong Kong Film Awards
*Won: Best Actor (Michael Hui)

==Album==
{{Infobox album  
| Name        = Security Unlimited 摩登保鑣
| Type        = Studio
| Artist      = Samuel Hui
| Released    =  
| Genre       = Cantopop
| Label       = PolyGram Cantonese
| Last album  = Nim Fei Giu 念奴嬌 (1980)
| This album  = Security Unlimited 摩登保鑣 (1981)
| Next album  = Cannot Forget You‧Paper Boat 難忘您‧紙船 (1982)
}}

Security Unlimited is Samuel Huis eighth Cantopop album with the title song being the films themesong.

===Track listing===
#"摩登保鑣"
#"印象"
#"扮野"
#"笑下就算"
#"長春不老"
#"我的心仍屬於您"
#"恭喜！恭喜！"
#"人辦"
#"陽光"
#"甜蜜的往事"
#"歌曲解困憂"
#"我問"

==External links==
* 
* 

 

 
 
 
 
 


 
 