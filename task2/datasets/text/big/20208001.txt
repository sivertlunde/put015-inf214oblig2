People of the Cumberland
{{Infobox Film
| name           = People of the Cumberland
| image          = PeopleCumberland.jpg
| image_size     = 
| caption        = Publicity still from People of the Cumberland
| director       = Sidney Meyers and Jay Leyda
| producer       = Frontier Films
| writer         = Erskine Caldwell
| narrator       =  
| starring       =
| music          = Alex North
| release        = 
| cinematography = Ralph Steiner
| editing        = 
| released       = June 4, 1938 (USA)
| runtime        = 18 minutes United States of America
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| distributor    = Frontier Films
}}
 1937 short film directed by Sidney Meyers and Jay Leyda and produced by Frontier Films. The film is designed to support the U.S. labor union movement and it mixes non-fiction filmmaking and dramatic re-enactions.

==Plot==
 Highlander Folk School in 1931 by educator Myles Horton and the movement to bring labor union representation to the region are shown as means of empowering the population. Efforts are made to stop the union activities with the murder of a local organizer, but eventually the union movement is able to take root with the local workforce.   

==Production== documentary filmmakers who focused on subjects relating to political and economic hardship. Frontier Films originally begin in 1931 as the New York Film and Photo League before changing its name in 1937. The collective focused on short films and disbanded in 1942 after producing its only feature-length production, Native Land. 

People of the Cumberland had two directors, Sidney Meyers and Jay Leyda, who used the pseudonyms “Robert Stebbins” and “Eugene Hill” for their screen credit,  and Elia Kazan served as assistant director.   

The film used actors to recreate the April 30, 1933, murder of Barney Graham, president of the local United Mine Workers.  Other events depicted in the film, including square dancing at the Highlander Folk School and a Fourth of July rally at La Follette, Tennessee, used the actual residents of the Cumberland region. 

==Reception==
When People of the Cumberland had its New York theatrical premiere in 1938, film critic Frank S. Nugent of The New York Times dismissed it by calling it “a propaganda film–rather than a documentary...it doesn’t carry much conviction as propaganda and not much weight as a film.” 

Film historian Russell Campbell, in his book Cinema Strikes Back: Radical Filmmaking in the United States 1930-1942, criticized the films Peoples Front ideology and argued that "square dancing and hog calling, delightful as they are, are no substitute for serious political thinking.” 

==References==
 

==External links==
* 

 
 
 
 
 
 
 