Candide ou l'optimisme au XXe siècle
 
{{Infobox film
| name           = Candide ou lOptimisme du XXe siècle
| image          =
| caption        =
| director       = Norbert Carbonnaux
| producer       = CLM, Société Nouvelle Pathé (France)
| writer         = Norbert Carbonnaux Albert Simonin
| starring       = Jean-Pierre Cassel Louis de Funès
| music          = Hubert Rostaing
| cinematography =
| editing        =
| distributor    = Consortium Pathé
| released       =  
| runtime        = 88 minutes
| country        = France
| awards         =
| language       = French
| budget         =
| preceded_by    =
| followed_by    =
}} comedy Drama drama film directed by Norbert Carbonnaux and written by Carbonnaux and Albert Simonin. It stars Jean-Pierre Cassel as Candide, Pierre Brasseur as Pangloss, Louis de Funès as the officer of the Gestapo, and Daliah Lavi as Cunégonde. The film was released under the titles Candide (alternative French title; USA), Candide oder der Optimismus im 20. Jahrhundert (West Germany), Candide, avagy a XX. század optimizmusa (Hungary), and Kandyd czyli optymizm XX wieku (Poland).  }} 

== Plot == firing squad, and settles down to write his memoirs.  

==Other film treatments==
In 1947 Marcel Carné intended to create a film based on Voltaires Candide, but production was abandoned.  The 1986 film Live from Lincoln Center: Candide was also based on the same novel. 

== Cast ==
* Jean-Pierre Cassel : Candide
* Louis de Funès : the officer of the Gestapo
* Pierre Brasseur : Pangloss
* Daliah Lavi : Cunégonde, the daughter of baron
* Nadia Gray : the live-in companion of Cunégonde
* Michel Simon : the colonel Nanar
* Jean Richard : the trafficker of the black market
* Darío Moreno : Don Fernando, the first dictator
* Luis Mariano : the second dictator South American
* Jean Tissier : the doctor Jacques
* Jacqueline Maillan : the puritanical mother
* Jean Poiret : a policeman
* Michel Serrault : a policeman
* Albert Simonin : the major Simpson
* Mathilde Casadesus : the baroness of Thunder-Ten-Trouck Robert Manuel : all German officers
* Jean Constantin : the king Fourak
* Don Ziegler : the papa gangster
* Odett : the baron Thunder-Ten-Trouck
* Michel Garland : the brother of Cunégonde
* Jacques Balutin : the prescription  of the colonel
* Gib Grossac : the leader of the Eunuches
* Michèle Verez : Paquerette, the maid of the baroness
* Sybil Saulnier : a lady from the harem
* Habib Benglia : the manhandled Black
* Mireille Alcon : a lady from the harem
* Danielle Tissier : a lady from the harem
* François Chalais : the commentator of the film
* Harold Kay : an American officer
* John William : the leader of "Oreillons"
* Pierre Repp : the priest
* Alice Sapritch : the sister of the baron
* Maurice Biraud : the Dutchman from Bornéo
* Michel Thomass : a Soviet driver

== References ==
 

== External links ==
*   movie review
*   at the Films de France

 

 
 
 
 
 
 
 
 
 