Backstreet Boys: Show 'Em What You're Made Of
 
 
{{Infobox film
| name = Backstreet Boys: ShowEm What Youre Made Of
| image = Backstreet Boys Doc Movie Poster.jpeg
| alt = 
| caption = Theatrical release poster
| director = Stephen Kijak
| producer = {{Plainlist|
* Mia Bays
* Sam Sniderman
* Jeff Kwatinetz
* Jennifer Sousa}}
| starring = {{Plainlist|
* Brian Littrell Nick Carter
* A.J. McLean Kevin Richardson
* Howie Dorough}}
| music = Backstreet Boys James Henry
| editing = {{Plainlist|
* Ben Stark
* Cinzia Baldessari}}
| studio = Pulse Films
| distributor = Gravitas Ventures & BBC Worldwide
| released =  
| runtime = 109 minutes    
| country = {{Plainlist|
* United States}}
| language = English
| budget = $1 million   
| gross = 
}}
Backstreet Boys: ShowEm What Youre Made Of is a 2015 American documentary film about the career of the American vocal group Backstreet Boys, released on January 30, 2015 in the U.S., and will be released February 26, 2015 in the UK and Europe, and March 28, 2015 for the rest of the world.    It was directed by Stephen Kijak.   

The film was shot over the span of two years and chronicles their entire career journey up to the making of their 2013 album In A World Like This, and throughout the subsequent world tour supporting it. 

==Plot==
  }}

The film opens with scenes of the group living together in a house in London, just the five of them without families, wives, or kids, as they work on their 20th anniversary album, In a World Like This. Shots of them working together, hanging out together, playing soccer, going shopping, hiking a mountain, were interwoven together with clips of individual members talking about their history. The film then shows Brian Littrell going to therapy for his ailment, he reveals that he was diagnosed with vocal cord dysphonia and neurological problem called dystonia.

Throughout the film, the entire group goes to each members hometown, visiting their old church, school, teachers. Kevin Richardson recounts the time leading up to his fathers death and shows his bandmates the church where he used to spend most of his time growing up. Howie Dorough brings his bandmates to his old house and tells them about the time their backyard was overrun with rabbits and he was ordered by his father to kill them, which he failed to do.

The group also visits Nick Carters old elementary school and dance school, where he breaks down and cries remembering how performing was a way for him to escape the negativity in his home, where his parents used to fight all the time. A.J. McLean also visits his old school and met with his teachers, and proceeds to recite the monologue he used to perform as a child. Littrell also visits his old school and met with his singing teacher, who was the first person to discover his talent. He also brings his bandmates to visit his church, where he proceeds to sit down at the altar and starts to sing, and admits that his voice problem is an ongoing struggle for him.

The group members also express their disappointment in Lou Pearlman, the man who put them together and subsequently created their rival NSYNC. They also talk about how he has robbed them of their money and how badly he treated them in the later years. They visit his mansion, now stripped bare by the IRS as hes in jail and had to pay back all the people he had scammed.

The film also features many old clips of the group rehearsing and performing in the early days, before they had gotten their recording contract in 1994. They had been touring schools across the US, and then found success in Europe and sold 14 million copies of their debut album, which was never released in the US.   

==Cast== Nick Carter as himself
* Brian Littrell as himself Kevin Richardson as himself
* A.J. Mclean as himself
* Howie Dorough as himself

==Production==
In 2012, as they were gearing up to record a new album, the group was looking to make a documentary of the whole recording process. However the idea quickly grew to make a movie about their whole career instead. "Weve been filming during the making of the record, the rehearsals for the tour, while weve been out on the road. Its kind of a making-of the record along with our story, how we got together and the ups and downs and the rollercoaster that weve been on," band member Kevin Richardson said. 

The group started filming when they moved into a house together all by themselves in London in July 2012. Filming process continued as the entire group visited each members hometown in September 2012 to learn more about one anothers lives before joining the group in 1993.
 No Distance Blur by director duo thirtytwo. 

==Promotion== world tour, they previewed the movie during the rest time.   Today Show. 

==Release==
The film was officially released on January 30, 2015 in the US, and will be released on February 26, 2015 in the UK and Europe, and March 28 in Latin America, Asia, Australia, and New Zealand.  The group will be present and perform at the UK premiere on February 26, 2015 in London. The performance will be broadcast live by satellite for UK and European countries, and will be recorded and played in the cinema for other countries where the release is not due until March 2015. 
 Arclight Cinemas in Los Angeles. The band was present and some celebrities attended the event, such as Lance Bass from NSync, Erik-Michael Estrada from O-Town, and Jeff Timmons from 98 Degrees.

===Commercial performance===
On January 30, 2015, the first day of US release, the movie topped the documentary chart on US iTunes Store and Google Play Store, and held the position for a week.   and No. 1 on iTunes Overall Movie Chart. 

===Home release===
The band has plans of releasing a DVD/Blu-ray Disc|Blu-ray combo pack in Summer 2015.  Band member A.J. McLean said that there will be deleted scenes and bonus footage in the DVD, along with an exclusive interview with the late John ‘Q’ Elgani, who was the head of their security. He had been with the group since 1996 and passed away in 2013.
On April 28, DVD was officially released in US and Canada, and topped the Music Videos & Concerts and Documentary chart consecutively. 

===Extended cut===
An extended version of the film, running 130 minutes, 20 minutes longer than the theatrical release, was screened at the sneak peek events. 

==Critical response==
 
Backstreet Boys: Show Em What Youre Made Of has received mixed reviews prior to release, currently holding a 69% critics on Rotten Tomatoes based on 16 reviews, with average rate of 6.8 out of 10.    and a 42/100 rating on Metacritic based on 6 reviews, signifying "mixed or average reviews".   Retrieved 30 January 2015 

Artistdirect gave it 5 out of 5 stars, noting that "One of the film’s most powerful aspects is its focus on the creation of the band’s latest offering of 2013’s In a World Like This. It’s the moment where A.J. McLean, Howie Dorough, Nick Carter, Kevin Richardson, and Brian Littrell really shine the most. Watching them jamming in the studio also feels strangely poignant as the purported image has always been of the quintet on stage in front of thousands dancing with no instruments. These intimate moments make Show ‘Em What You’re Made Of a true gem as far as music documentaries go."  Village Voice gave it an average score of 70, Amy Nicholson said "Show Em What Youre Made Of convincingly argues that these boy-men have something to say about the fickleness of fate — something they knew more about as young men than any of the cynics who dismissed them for dancing in unison. The hardest part will be convincing people to listen." 
 The New York Times, Neil Genzlinger felt "It is insight-free and cliché-heavy, with the five sharing obvious reminiscences about the thrill of superstardom, visiting haunts from their youth, shooting baskets and occasionally rehearsing." 

==See also==
* List of documentary films

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 