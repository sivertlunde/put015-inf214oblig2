Cat's Cradle (film)
{{Infobox film
| name           = Cats Cradle
| image          = 
| image size     = 
| caption        = 
| director       = Stan Brakhage
| producer       =
| writer         =
| narrator       =
| starring       = Stan Brakhage Jane Brakhage James Tenney Carolee Schneemann
| music          =
| cinematography =
| editing        =
| filmed         = 
| released       = 1959
| runtime        = 6 minutes
| country        = United States
| language       = English
| budget         =
}}
 experimental short film by Stan Brakhage, produced in 1959. The film was described by Brakhage as "sexual witchcraft involving two couples and a medium cat."   Canyon Cinema: Film, Accessed 13 February, 2011  By Brakhage: An Anthology, Volume 1, DVD menu 

==Production==
 
Cats Cradle was filmed in   and visual artist Carolee Schneemann. Banes, Sally (1993) Greenwich Village 1963: avant-garde performance and the effervescent body, Duke University Press, p90  Schneemann, who appeared in several Brakhage films, wore an apron at Brakhages insistence. "An Interview with Carolee Schneemann," Wide Angle, 20(1) (1998), p20-49  Despite her friendship with Brakhage, she later described the experience as "frightening," remarking that "whenever I collaborated, went into a male friends film, I always thought I would be able to hold my presence, maintain an authenticity. It was soon gone, lost in their celluloid dominance--a terrifying experience--experiences of true dissolution." 
 silent film was described by Brakhage as "sexual witchcraft involving two couples and a medium cat."   The film features shots of the naked bodies that are, according to writer Walter Metz, "edited in such a way that very little narrative sense can be immediately gleaned from them. As the film wears on, however, it becomes clear that the viewer is witnessing some form of domestic conflict and the intimacy that follows (or perhaps precedes) it." Metz, Walter (2001) ""What Went Wrong?": The American Avant-Garde Cinema of the 1960s", History of the American cinema: 1960-1969. The sixties, Volume 8 , University of California Press, p236  The editing style includes very brief "flash-frames" that interrupt longer shots, a technique that Brakhage would continue to use in such films as Tortured Dust (1984). Elder, R. Bruce (1998) The films of Stan Brakhage in the American tradition of Ezra Pound, Gertrude Stein, and Charles Olson, Wilfrid Laurier Univ. Press, p256 

==Reception==
Paul Arthur, in his essay for The Criterion Collection, wrote that Cats Cradle "does not entirely suppress our recourse to naming but rather floods our typical eye-brain loop with stimuli for which attached language cues are either less than automatic or, in cases of purely sensory appeal, non-existent."  , Paul Arthur, published June 09 2003, The Criterion Collection. Accessed 13 February, 2011  Fred Camper, in another essay for The Criterion Collection, remarked upon the mysteriousness of the four characters interactions, but was nevertheless "kept on edge by the very rapid intercutting... the viewer is at once encouraged to come up with his own interpretations and prevented from settling on any one idea."  , Fred Camper, published June 09 2003, The Criterion Collection. Accessed 13 February, 2011 

==References==
 

==External links==
*  

 
 
 
 
 