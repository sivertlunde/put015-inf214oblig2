Film Geek
{{Infobox film
| name     = Film Geek
| image    = Filmgeek.jpg
| director = James Westby
| writer   = James Westby Matt Morris John Breen Tara Walker Michelle Garner
| producer = Byrd McDonald
| editing  = James Westby
| music    = Jason Wells
| cinematography = Jason Hughes
| distributor = First Run Features
| released =  
| runtime  = 78 minutes
| language = English
}}
Film Geek is a 2005 independent film  written and directed by James Westby   and starring Melik Malkasian as Scotty Pelk. The story revolves around Pelks life as a super film geek and a love interest that develops with a girl named Niko (Tyler Gannon).

==Plot==
Scotty Pelk is a socially inept film geek who works at a video store called Video Connections in Portland, Oregon.  His biggest claim to fame is his website, www.scottysfilmpage.com (which has yet to receive a hit).  He is a hard worker, but eventually gets fired for being too much of an annoyance to the customers. In a funk, he visits several video stores around town to try to find a job, but none of the stores are hiring.
 MAX to the Portland Staffing Resources office, Scotty sees a young woman, Niko, reading a book called The Films of David Cronenberg.  He is infatuated and chases her down to talk to her about Cronenberg, but she essentially brushes him off.

At the Portland Staffing Resources office, Scotty finds a job working with auto parts. He is confused and lost during his first day at work, all the while continuing to talk about movies with his co-workers. On the way home, he sees Niko again and joins her for coffee, and she invites him to her art show later that night.  During the show, Niko kisses Scotty to make her ex-boyfriend, Brandon, jealous. Later, the two go out and sit in Nikos car to talk.  Scotty smokes marijuana with her, and they head to a party afterwards. At the party, Scotty gets drunk with two guys who are also film fans.

Scotty constantly calls Niko over the next few days, trying to ask her for a date.  She eventually says yes, and they go out to eat at a Mexican restaurant.  Afterwards, they watch a movie and then go to a bar.  Brandon shows up at the bar and upsets Niko, ending their date.

Scotty continues to obsess and call Niko frequently, but she does not answer her phone.  Eventually Niko picks up, but she brushes him off.  On the way to visit Niko with flowers and a balloon, Scotty sees Niko and Brandon kissing.  Niko then informs Scotty that she and Brandon have gotten back together, indicating that her interest in Scotty had been a ruse to make Brandon jealous.

Scotty goes home and, in a fit of rage and self-loathing, destroys the bouquet of flowers and balloon he had bought for Niko.

Scotty is shown masturbating in his bathroom (one of several such scenes), when he is interrupted by the phone ringing.  A Willamette Week newspaper editor is calling Scotty to do an article on him after hearing about his website from the two guys at the party.  Once the article is published, Scottys website becomes popular around the city and he becomes a local celebrity, and eventually gets his own office.  While Scotty is signing autographs at Video Connections, Niko walks in after having a fight with Brandon.  After talking a little, Niko and Scotty get back together and kiss. Finally, the film cuts back to Scotty masturbating, with the implication that the phone call and all that followed was only a masturbatory fantasy. Reviewer D.K. Holm compared the surprise ending to Vladimir Nabokov|Nabokovs An Affair of Honor and Ambrose Bierces An Occurrence at Owl Creek Bridge. 

==Cast==
* Melik Malkasian as Scotty Pelk
* Tyler Gannon as Niko Matt Morris as Brandon John Breen as Mr. Johnson
* Tara Walker as Kaitlin
* Michelle Garner as Cindi

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 