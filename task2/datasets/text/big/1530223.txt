Rock Star (2001 film)
 
{{Infobox film
| name           = Rock Star
| image          = Rock star ver1.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Stephen Herek
| producer       = George Clooney (executive) Robert Lawrence Toby Jaffe Mike Ockrent (executive) Steven Reuther (executive) John Stockwell
| starring       = Mark Wahlberg Jennifer Aniston Dominic West Timothy Spall Timothy Olyphant
| music          = Trevor Rabin
| cinematography = Ueli Steiger
| editing        = Trudy Ship
| studio         = Bel Air Entertainment Maysville
| distributor    = Warner Bros.
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $38   - $57   million
| gross          = $19,334,145
}} musical comedy-drama film directed by Stephen Herek and starring Mark Wahlberg and Jennifer Aniston. It tells the story of Chris "Izzy" Cole, a tribute band singer whose ascendance to the position of lead vocalist of his favorite band was inspired by the real-life story of Tim "Ripper" Owens, singer in a Judas Priest tribute band who was chosen to replace singer Rob Halford when he left the band.

==Plot== heavy metal band called Steel Dragon. By day, Chris is a photocopier technician and by night, he is the lead singer of a Steel Dragon tribute band called Blood Pollution (the name is taken from a Steel Dragon song).

Internal struggles among the actual Steel Dragon band members culminate with the firing of their lead singer, Bobby Beers (Jason Flemyng), and the starting of recruitment sessions to find a new vocalist. Chris experiences his own strife with his Blood Pollution bandmates, particularly guitarist Rob Malcolm (Timothy Olyphant). During a live performance, Robs playing fails to live up to Chris over-demanding standards regarding note-for-note accuracy to the original Steel Dragon recordings, and Chris sabotages Robs amplifier mid-song - a fight breaks out between the two onstage. The next day, Chris arrives at Blood Pollutions rehearsal space (the basement of an X-rated theater) to find that hes been fired and replaced with his arch-rival, the (now former) lead singer of another Steel Dragon tribute band. Rob also cites Chris inability to create his own musical style, preferring to remain the singer in a tribute band.
 closeted gay, and gives an outstanding performance of "We All Die Young" (a Steel Dragon song in the movie, but it is actually a song by Steelheart, whose lead vocalist (Miljenko Matijevic) provides Coles singing voice for the film). Chris joins the band as their new singer, adopting the stage name "Izzy". Following a successful debut concert with Steel Dragon, Izzy must come to grips with the pressures of his new-found fame and success. The band embarks on a lengthy tour and Izzy experiences the excesses of the lifestyle, with the groups manager, Mats (Timothy Spall), serving as a sympathetic mentor to Izzy.

His new lifestyle impacts his life both for better and worse, particularly with his relationship with his supportive girlfriend, Emily Poule (Jennifer Aniston), when she decides not to continue with him throughout the remainder of the tour as a rock star girlfriend, though Emily and Izzy agree to get back together when the tour reaches Seattle. Eventually, Steel Dragon stops in Seattle for a show, and Emily arrives at his hotel room as they had previously arranged, although Izzy had become so inebriated while on tour he forgot about the arrangement and did not even know what city he was in. Although taken aback by all the groupies, Emily still tries to reconnect with him, reminding him of their plans to meet up once he got to Seattle, however he is too intoxicated to really understand what she is saying, eventually suggesting they go to Seattle together. Heartbroken with his inconsiderate behavior, intoxication and the fact that he is sleeping with so many groupies, Emily leaves him.

After the end of the tour, Izzy reports to the next series of Steel Dragon recording sessions with song concepts for the bands next album. The rest of the band rejects Izzys ideas, with Kirk explaining that the band has to stay true to the "Steel Dragon thing" to fulfill fan expectations. Izzy is angered upon realizing that he was only recruited for his vocal abilities. After a heartfelt conversation with Mats about how he feared he had no control over the direction life has taken him, Izzy begins to reconsider his rock star lifestyle. On the next tour, in a scene directly paralleling one near the beginning of the film with their roles reversed, Izzy hears a fan (Myles Kennedy) singing along with him toward the end of a live concert. Impressed, Izzy pulls the fan, who introduces himself as Mike, onstage and hands him the microphone to finish the concert.  Backstage, Izzy realizes that what he wanted for so long was not what he thought, and he says goodbye to Mats, departing from the band while doing so.

Upon ditching his stage name, Izzy, Chris makes his way to Seattle and starts a new band with his old friend and former bandmate Rob. At the same time Steel Dragon, failing to evolve to changing tastes and styles, has its fame fizzle out. Chris finds Emily working in the coffee shop she and her roommate purchased a few years earlier, but is initially too ashamed to speak to her. While walking one evening, Emily sees a flyer for his band posted on the wall and takes it down. In the final scene, Chris is singing with his band in a bar and Emily walks in. Chris leaves the stage and speaks to her. They reconcile, ending the film with a kiss and the final note of Chris first original song "Colorful" (which is actually a song by The Verve Pipe).

==Cast== Foreigner bassist Foreigner and ex-Black Country Communion drummer Jason Bonham (the son of the late  John Bonham, drummer for Led Zeppelin). Myles Kennedy, who was at the time the lead vocalist of The Mayfield Four and now the frontman of Alter Bridge and Slash (musician)|Slashs solo project, makes a cameo appearance.

The singing voice for Wahlbergs character was provided by Steelheart frontman Miljenko Matijevic for the Steel Dragon Songs, the final number dubbed by Brian Vander Ark. Jeff Scott Soto (of Talisman (band)|Talisman, Yngwie Malmsteen, Soul Sirkus, and Journey (band)|Journey) provided the voice of the singer Wahlbergs character replaces.  Kennedy is the only actor whose actual voice is used. . Ralph Saenz (Steel Panther) also appears briefly, as the singer auditioning ahead of Chris at the studio.

Blood Pollution (Chris Coles former band and a Steel Dragon tribute band) is also made up of known musical artists, including guitarist Nick Catanese (Black Label Society), drummer Blas Elias (Slaughter (band)|Slaughter), and bassist Brian Vander Ark (The Verve Pipe, who also contributed a song to the films soundtrack). Actor Timothy Olyphant portrays Blood Pollutions guitarist, Rob Malcolm.  Bradley, the singer who replaces Chris in Blood Pollution, is played by Third Eye Blind frontman Stephan Jenkins.

{| class="toccolours" style="border-collapse: collapse;"
|-
|valign="top"|
Blood Pollution
* Mark Wahlberg as Chris "Izzy" Cole (Lead vocals)
* Jennifer Aniston as Emily Poule (Manager)
* Timothy Olyphant as Rob Malcolm (Guitar)
* Blas Elias as Donny Johnson (Drums)
* Nick Catanese as Xander Cummins (Guitar)
* Brian Vander Ark as Ricki Bell (Bass)
|width="20"|&nbsp;
|valign="top"|
Steel Dragon
* Jason Flemyng as Bobby Beers (Lead Vocals)
* Timothy Spall as Mats (Road Manager)
* Dominic West as Kirk Cuddy (Rhythm Guitar)
* Jason Bonham as A.C. (Drums)
* Zakk Wylde as Ghode (Lead Guitar)
* Jeff Pilson as Jörgen (Bass)
|width="20"|&nbsp;
|valign="top"|
Spouses
* Kara Zediker as Marci (Robs girlfriend)
* Heidi Mark as Kirks wife
* Rachel Hunter as A.C.s wife
* Carrie Stevens as Ghodes wife
* Amy Miller as Jörgens wife
|width="20"|&nbsp;
|valign="top"|
Others
* Dagmara Dominczyk as Tania Asher
* Stephan Jenkins as Bradley
* Michael Shamus Wiles as Joe Cole Sr
* Beth Grant as Tess Cole
* Matthew Glave as Joe Cole Jr
* Myles Kennedy as Mike / Thor
* Ralph Saenz as Ralph
* Carey Lessard as Nina
* Kristin Richardson as Samantha
|width="20"|&nbsp;
|valign="top"|
|}
 

==Reception==
The film opened at #4 at the U.S. box office raking in US$6,018,636 in its opening weekend,  and grossing a domestic total of $17,008,282 and $2,325,863 internationally for a worldwide gross of $19,334,145; based on a $57 million budget, Rock Star was a box office bomb. 

On the review aggregator website Rotten Tomatoes, the film holds a 52% "Rotten" score, with the consensus "Like its title, Rock Star is rather generic, being not so much about the heavy metal scene than about rock cliches and formula". 

Metacritic, which assigns a weighted average score from 1 to 100 to reviews from mainstream critics, gave the film a 54 based on 32 critics. 

==Soundtrack==
{{Infobox album
|  Name        = Rock Star
|  Type        =Soundtrack
|  Artist      = Various Artists
|  Cover = 
|  Background  = Orange
|  Released    = August 28, 2001
|  Recorded    = 
|  Genre       = 
|  Length      =  Priority
|  Producer    = 
|}}
{{Album ratings
| rev1      = AllMusic
| rev1Score =   
}}

A small number of the songs featured in the film and on the soundtrack were released after the dates given in the film. They are marked with an asterisk.

{{Tracklist
| headline        = 
| total_length    = 58:59
| extra_column    = Artist
| writing_credits = no

| title1 = Rock Star Everclear 
| note1 = Art Alexakis
| length1 = 3:30

| title2 = Livin the Life
| extra2 = Steel Dragon
| note2 = Steve Plunkett, Peter Beckett
| length2 = 3:14
 Wild Side*
| extra3           =Mötley Crüe
| note3         =Vince Neil, Nikki Sixx, Tommy Lee
| length3         =4:34

| title4          =We All Die Young
| extra4           =Steel Dragon
| note4         =Miljenko Matijevic, Kenny Kanowski
| length4         =4:01

| title5          =Blood Pollution
| extra5           =Steel Dragon
| note5         =Twiggy Ramirez
| length5         =3:59

| title6          =Livin on a Prayer*
| extra6           =Bon Jovi
| note6         =Jon Bon Jovi, Richie Sambora, Desmond Child
| length6         =4:08
 Stand Up
| extra7           =Steel Dragon
| note7         =Sammy Hagar
| length7         =4:18
 Stranglehold
| extra8           =Ted Nugent
| note8         =Ted Nugent
| length8         =8:23

| title9          =Wasted Generation
| extra9           =Steel Dragon
| note9         =Desmond Child, A.Allen (Ajay Popoff), J. Allen (Jeremy Popoff)
| length9         =2:54
 Lick It Up KISS
| note10        =Paul Stanley, Vinnie Vincent
| length10        =3:56

| title11         =Long Live Rock n Roll
| extra11          =Steel Dragon
| note11        =Ronnie James Dio, Ritchie Blackmore
| length11        =3:27
 Devil Inside*
| extra12          =INXS
| note12        =Andrew Farriss, Michael Hutchence
| length12        =5:13
 
| title13         =Colorful
| extra13          =The Verve Pipe
| note13        =Brian Vander Ark
| length13        =4:25

| title14         =Gotta Have It
| extra14          =Trevor Rabin
| note14        =Trevor Rabin
| length14        =2:57
}}

Partial list of songs that were featured in the movie but did not appear on the soundtrack CD: Rainbow - "Long Live Rock n Roll" Are You Ready"*
* Culture Club - "Karma Chameleon"
* Def Leppard - "Lets Get Rocked"*
* Def Leppard - "Pyromania (album)|Rock! Rock! (Till You Drop)" Chateau Lafitte 59 Boogie"
* Frankie Goes to Hollywood - "Relax (song)|Relax" Good Vibrations
* Ralph Saenz, Peter Beckett, and Steve Plunkett - "California Girls" (The Beach Boys cover) Once in a Lifetime" Phoenix Down cover)

==References==
 

==External links==
 
*  
*  
*  
*  
*   Includes after-party photos at House of Blues

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 