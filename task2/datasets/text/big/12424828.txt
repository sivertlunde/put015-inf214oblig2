My Brother (2006 film)
{{Infobox film
| name           = My Brother
| image          = My Brother dvd cover.jpg
| caption        = 
| director       = Anthony Lover
| producer       = Leslieann Fouche Gingi Rochelle
| writer         = Anthony Lover
| starring       = Vanessa L. Williams Tatum ONeal Nashawn Kearse Fredro Starr
| music          = John Califra
| cinematography = John Saywer
| editing        = Christian Baker
| distributor    = Codeblack Entertainment
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $65,797
}}
My Brother is a 2006 film directed by Academy Award nominee Anthony Lover. It stars Vanessa L. Williams, Tatum ONeal, Nashawn Kearse and Fredro Starr. It also stars two first time actors with Down syndrome, Christopher Scott and Donovan Jennings. Two developmentally disabled actors played leading roles, and also an African American actor with a developmental disability played a leading role.

The movie was produced by Gregory Segal for Angel Baby Entertainment and Liberty Artists. The executive producer was Michael Malagiero. The cinematographer was John Saywer.

The movie was released theatrically in March 2007 in 19 cities by CodeBlack Entertainment. The DVD was released in May 2007 by Universal.

==Special Olympics connection==
The movie was promoted in connection with the Special Olympics.  Vanessa Williams sits on the Board of Directors of Special Olympics International. A benefit for the Special Olympics was held in Washington D.C. for the release of the movie in that city. It was attended by cast, crew, Tim and Eunice Kennedy Shriver, Sargent Shriver, and Vanessa Williams.

==Awards==
The movie won the Grand Jury Prize at the American Black Film Festival in Miami, in July 2006. It has received 26 other nominations and awards, including Christopher Scotts Founders Award at the American Black Film Festival.

==External links==
* 
* 
* 

 
 
 
 
 
 


 