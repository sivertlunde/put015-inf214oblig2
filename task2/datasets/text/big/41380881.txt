Katherine of Alexandria (film)
 
 
{{Infobox film
| name = Katherine of Alexandria
| image = Katherine of Alexandria.jpg
| alt = 
| director = Michael Redwood
| producer = Michael Redwood
| writer = 
| starring = {{Plainlist|
* Nicole Madjarov
* Peter OToole
* Steven Berkoff
* Joss Ackland}}
| music = John Koutselinis
| cinematography = {{Plainlist|
* Ralph Messer
* Peter Taylor}}
| editing = Helen Ellene Michael
| distributor = {{Plainlist| Kaleidoscope   Lionsgate  }}
| released =  
| runtime = 110 minutes  
| country = United Kingdom
| language = English

}} biographical Historical historical drama film produced and directed by Michael Redwood about the life of Catherine of Alexandria. It is notable as the last film role of Peter OToole, who died before the film was released.

==Cast== Katherine
* Peter OToole as Cornelius Gallus
* Steven Berkoff as Liberius
* Joss Ackland as Rufus
* Jack Goddard as Constantine the Great
* Dudley Sutton as Marcellus Edward Fox Constantius
* Samantha Beckinsale as Vita

==Release==
The film was due for release in 2014  and was released Direct-to-video|direct-to-DVD in the United States on 12 August 2014. The film will have a theatrical release in the United Kingdom on 6 April 2015 as Fall of an Empire: The Story of Katherine of Alexandria.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 