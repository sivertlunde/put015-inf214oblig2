Forbidden Quest
 
{{Infobox film name        = Forbidden Quest image       = Forbidden quest.jpg caption     = Forbidden Quest movie poster director    = Kim Dae-woo producer    = Yun In-beom   Kim Su-jin   Im Jeong-ha writer      = Kim Dae-woo starring  Kim Min-jung cinematography = Kim Ji-yong editing     = Steve M. Choe   Kim Chang-ju music       = Mok Young-jin distributor = CJ Entertainment released    =   runtime     = 139 minutes country     = South Korea language    = Korean film name   = {{Film name hangul      =   hanja       =   rr          = Eumranseosaeng mr          = Ŭmransŏsaeng}} gross       =   
}}
Forbidden Quest ( ) is a 2006 South Korean period drama film about a scholar during the Joseon Dynasty who begins to write erotic novels, and becomes the lover of the Kings favorite concubine.     

The film has not been released in Europe or America, and is only available on region 3 DVDs since May 27, 2006.

==Plot==
At the start of the film, Kim Yoon-seo, an Inspections officer (police inspector) is seen at his home where his brother has been beaten almost to death. His family wants to submit a false appeal to the king accusing the family of the Deputy Prime Minister, the Lim family. The Lim family had by the past submitted a false appeal against Yoon-seos family. However Yoon-seo does not wish to submit such an appeal, much to the anger of his elders. 

He returns to the court where the King hands him an important assignment. The Kings concubine, Jung-bin, had a painting she had drawn mounted, but it was replaced by an exact copy, she only realized because of the lack of a scribble she had drawn on the back. The King would like Yoon-seo to find the culprits. Yoon-seo is impressed by the talent of the artist who made the fraudulent copy. Jung-bin is in turn impressed by Yoon-seos knowledge on the subject and invites him to talk to her some more about art some day. 

Yoon-seo teams up with Lee Gwang-hun, an inspector who likes to torture people in his dungeons, to try to hunt down the culprit. They enter a shop in their search for the criminal. In the basement Yoon-seo encounters an old man transcribing an erotic story book. Back upstairs a group of men from the village enter the shop and engages in a fight with Gwang-hun, who beats them away with his truncheon, however Yoon-seo is knocked out so Gwang-hun takes him back to his house with him. When Yoon-seo awakes he admires Gwang-huns painting artwork. Back at the shop the old man confesses to Yoon-seo that he does not wish to protect the mounter either, but that one of his friends is involved and he does not wish to betray him. After Yoon-seo has promised to protect his identity, the old man gives him an address he may go to. Out of curiosity before he leaves he reads few verses from the book the old man is reproducing. He is at first outraged by the content (pornography being illegal at the time), but when the old man says the whole city is waiting for the book he shows some interest and perhaps embarrassment about never having heard of the author. He reports later to the King that he found the culprits and had each flogged fifty times. 

Jung-bin invites him around for tea, to thank him. When a bee lands on her he violates all protocol risking his death, by going beyond the curtain and brushing it off her shoulder. Jung-bin however does not report his attitude, and thanks him instead. She also appears to be very fond of him. 

Yoon-seo writes an indecent book himself and returns to the shop to present to the shop keeper, asking him to read it. The shopkeeper, Hwang is impressed and they decide to copy and sell his work. His book becomes a best seller.

To get his books to sell better he asks Lee Gwang-hun to illustrate them for him. At first he is outraged but in the end accepts. Meanwhile Jung-bin sends him an embroidered kerchief, by means of her eunuch, who asks him to not accept the present, or else Jung-bin will be wanting to meet him. He does accept though, and later meets Jung-bin, in private. They start to cuddle, but Yoon-seo leaves when she attempts to kiss him. 

The illustrator has trouble understanding some of the things he is supposed to draw, so next time Yoon-seo meets Jung-bin, he has him hide behind a wall, and spy on them while they have sex. Yoon-seo writes about his affair with Jung-bin in his next book. When she forces the one of her maids to tell her, she goes to the shop and confronts Yoon-seo. She tells him to divulge the name of the illustrator Gwang-hun. However he refuses to betray him. 

Jung-bin has Yoon-seo sent to the town prison, the prison for which Gwang-hun the illustrator is responsible. In the presence of Jung-bin, Yoon-seo is tied to the ceiling and severely beaten with sticks. 

Gwang-hun wishes to stop but Jung-bin decides to call the matter to the Kings attention. They tie Yoon-seo to a contraption, and Gwang-hun turns the handle until the bones of his legs snap in half. The King is satisfied with the torture but Jung-bin wants to continue, after their meal. During meal time Gwang-hun removes him from jail, so he can escape with the help of the book publisher, the shopkeeper. However Jung-bins eunuch follows them and a fight ensues in between the people from the shop and a group of warriors the eunuch brought along. The shopkeepers are no match for the warriors, however Gwang-hun does put up a good fight. In the end he is beaten but is forced to leave with the people from the shop while the eunuch takes Yoon-seo back to his jail. Gwang-hun is left alive because Yoon-seo threatens to claim the eunuch as the illustrator, which would be possible because he did come with Jung-bin to all their meetings. On the way back the eunuch becomes more worried about what Yoon-seo may say and attempts to kill him. One of his warriors however chops off his hand to avoid him stabbing Yoon-seo. The eunuch is shocked and the warrior asks him why he disobeyed an order. The eunuch tells him he had an order from his heart also (he is secretly in love with Jung-bin, and became a eunuch to stay with her); at which point the warriors shoves his dagger through the top of his shoulder and down through his heart.

Yoon-seo is brought back to the palace and to Jung-bin and the King. Jung-bin decides she has had enough. The King asks her about her sudden change of mind. Then he tells her he knows that what she had wanted Yoon-seo to say all along was not the name of the illustrator, but that he loved her. The King can not understand why he did not lie and say he loved her. 

The King tells the servant to castrate him and make him a eunuch so he may be with Jung-bin for the rest of his life, even though the operation may kill him due to his age. Jung-bin intervenes and takes all the blame saying she used her influence to seduce him. Yoon-seo then confesses his love to her. Jung-bin says she will wait till they meet again in the next life. The King is overcome by anger and grief, as he was most in love with her, and at first wants to personally kill both Jung-bin and Yoon-seo. He decides against this though, as he realizes that it would be useless as they would just be reunited in the afterlife, and leaves them both alive. 

At the end Yoon-seo is sent to live in exile on an island, where he continues to write. Jung-bin remains at the palace "to rot." Later it is revealed that the words "indecent" were branded on Yoon-seos forehead as part of his punishment.

The film ends with Yoon-seo, Gwang-hun and the shopkeeper walking along the shore, talking about future works, which include their first homosexual male erotic story, and even the discovery by Yoon-seo of the basic principles of animation or "moving pictures."

==Cast==
*Han Suk-kyu - Yoon-seo
*Lee Beom-soo - Gwang-hun Kim Min-jung - Jung-bin
*Oh Dal-su - copier
*Woo Hyeon - transcriber
*Ahn Nae-sang - King
*Kim Roi-ha - eunuch
*Kim Byung-ok - deputy prime minister
*Choi Jong-ryul - old man 1
*Kwon Oh-jin - old man 2
*Geum Dong-hyun - old man 3
*Oh Tae-kyung - Jung-seo
*Jung In-gi - colleague
*Park Hyuk-kwon - Oh Na-jang
*Lee Soon-jae - Yoon-seos father
*Jin Kyung - Yoon-seos wife
*Gu Bon-im - Gwang-huns wife
*Choi Moo-sung - trader 1
*Ra Mi-ran - housewife 1
*Kang Hyun-joong - black mask 1
*Lee Ha-eun - gisaeng 4
*Choi Gyo-sik - servant
*Lee Marg-eum - servant
*Jo Seok-hyun - passerby
*Jang Nam-yeol - public officer
*Ha Sang-won - administrator 1

==Awards and nominations==
;2006 Baeksang Arts Awards 
* Best New Director - Kim Dae-woo
* Nomination - Best Screenplay - Kim Dae-woo

;2006 Grand Bell Awards
* Best Costume Design
* Nomination - Best Screenplay - Kim Dae-woo
* Nomination - Best Art Direction - Cho Geun-hyeon
* Nomination - Best New Director - Kim Dae-woo

;2006 Blue Dragon Film Awards
* Best Art Direction - Cho Geun-hyeon and Hong Ju-hee
* Nomination - Best Supporting Actor - Oh Dal-su
* Nomination - Best Screenplay - Kim Dae-woo
* Nomination - Best Cinematography - Kim Ji-yong
* Nomination - Best Visual Effects
* Nomination - Best New Director - Kim Dae-woo

;2006 Korean Film Awards
* Best Art Direction - Cho Geun-hyeon
* Nomination - Best Film
* Nomination - Best Actor - Han Suk-kyu Kim Min-jung
* Nomination - Best Screenplay - Kim Dae-woo
* Nomination - Best New Director - Kim Dae-woo

==See also==
* List of Korean language films
* Korean cinema
* List of Korea-related topics

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 