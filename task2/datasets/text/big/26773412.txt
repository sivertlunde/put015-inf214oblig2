Nallavan (2010 film)
{{Infobox film
| name           = Nallavan
| image          = Nallavan.jpg
| alt            =  
| caption        = 
| director       = Aji John
| producer       = Anil Mathew  S. Murukan
| writer         =  
| starring       =  
| music          = Mohan Sithara
| cinematography = Manoj Pillai   Pradip Nair
| editing        = 
| studio         = 
| distributor    = Anil mathew S Murugan for Bethestha Productions
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} 
Nallavan is a 2010 Malayalam film written and directed by Aji John starring Jayasurya and Mythili in title roles.

== Plot ==

The movie starts from the Kerala high court, when the police brings Kocherukkan (Jayasurya), a man who had done two murders and jumped the jail four times but is again caught by the police. But he escapes again with the help of his friend and checks out his colourful memory.

Kocherukkan was a happy youth who lived an ordinary life in a quiet village somewhere in the border of Tamil Nadu and Kerala. He is in love with Malli (Mythili), an agile, adventurous, cheerful girl of 17. Both of them are orphans, and Malli has a landlord.

The tumultuous life of Kocherukkan and Malli takes a sudden change when Mallis landlord Chandrasheghara Vazhunnor (Saikumar (Malayalam actor)|Saikumar)and his driver Devarajan (Vijayakumar) decides to get  her married-off to his relative Chitharanjan(Suraj Venjarammood). Malli and Kocherukkan elopes with the help of Shaji (Bijukkuttan) and Shangu (Anoop Chandran), Kocherukkan’s friends. They meet a guy named Kumareshan(Siddique (actor)|Siddique)on the way who tries to kidnap Malli, but Kocherukkan hits him with a stone and they flees. They reach Pollachi and starts living with Murukan(Sudheesh),Kocherukkans friend. Kocherukkan and Malli goes to the registrar office to get married but since Malli was only 17 and Kocherukkan was 20,the registrar (Kochupreman) asks them to come after a year when they are legally entitled to get married.

A  year later, Kocherukkan was spotted by Kumareshan, who was the police sub-inspector of the area and he arrests Kocherukkan for his personal enmity.He puts false charges on Kocherukkan including immoral trafficking and robbery. But he runs off from the court to meet Malli and they get married. But he reports back to Police the same day since police arrests Murukan instead of Kocherukkan and tortures him.

Kocherukkan is sent to the jail again.This time Malli gets to know from newspaper that Kocherukkan is a wanted criminal now. She returns to the land lord and dismisses her relation. Kocherukkan escapes from jail again and finds Malli murdered in a desert. He is arrested again with one more crime of murder. Kumareshan also traps him in another murder, this time that of Vazhunnor. In the court Kocherukkan escapes again for the fifth time, questions Mallis stepfather Narayanan (Manianpilla Raju) and understands that Kumareshan is behind his wifes murder and takes his revenge on him.

== Cast ==
* Jayasurya as Kocherukkan
* Mythili as Malli Siddique as Kumareshan police
* Sudheesh as Murukan
* Bijukuttan as Shaji
* Suraj Venjaramood as Chitharanjan
* Vijayakumar as Devarajan
* Manianpilla Raju as Narayanan
* Anoop Chandran as Shangu Saikumar as Chandrashekhara Vazhunnor,mallis landlord
* Bindu Panicker as Murukans mother
* Jayan Cherthala as a prisoner
* Sona Nair as Prosecutor Rajalakshmi
* Kochu Preman as The registrar of the marriage office
* Ambika Mohan as Chandrashekhara Vazhunnors wife
* Baby Ester as Malli Jr

==Soundtrack==
{{Infobox album
| Name       = Nallavan
| Type       = soundtrack
| Artist     = Mohan Sithara
| Cover      = 
| Released   =  
| Recorded   =
| Genre      = World Music
| Label      = 
| Producer   =
}} romantic malayalam song and Maayakkanavu which was an emotional malayalam song.

{{Track listing
| extra_column = Performer(s)
| title1 = Pudichachu | extra1 = Anwar Sadath, Jisha, Sangeetha | length1 = 5:08
| title4 = Maayakkanavu | extra4 = Arun Gopan | length4 = 4:40
| title5 = Thoomallike Allithen Mallike | extra5 = Latha Krishna, Santhosh Raj | length5 = 4:15
| title6 = Nallavan | extra6 = Prameela, Sanandh George(vocals) | length6 = 2:46
| title7 = Theme Music  | extra7 =  | length7 = 0:47
}}

== External links ==
* http://www.nowrunning.com/movie/7266/malayalam/nallavan/index.htm
* http://www.indiaglitz.com/channels/malayalam/preview/11798.html
* http://popcorn.oneindia.in/title/8129/nallavan.html
* http://thebollywoodactress.com/nallavan-release-centres-by-the-24th-of-this-month/

 
 
 