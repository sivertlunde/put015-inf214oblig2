Lady Scarface
{{Infobox film
| name           = Lady Scarface
| image          = 
| alt            =
| caption        = Judith Anderson as Lady Scarface
| director       = Frank Woodruff
| producer       = Cliff Reid Richard Collins
| starring       = Dennis OKeefe Judith Anderson Frances E. Neal Mildred Coles
| music          = Dave Dreyer
| cinematography = Nicholas Musuraca
| editing        = Harry Marker
| costumes       = Renié
| distributor    = RKO Pictures
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
}}
Lady Scarface is a 1941 black and white crime drama film.

== Plot ==
The evil Chicago gangster Slade is on the loose.  Lt. Mason and Ann Rogers are both vying to catch the criminal.  The gang has come up with a scheme of contacting each other involving want ads and dogs as code names.  It also involves leaving money at a hotel for a "Mary Jordan".  The only problem occurs when someone who really is named Mary Jordan gets the money by mistake.  A trap is set and villains are caught.

== Cast ==
* Dennis OKeefe as Lt. Bill Mason
* Judith Anderson as Slade
* Frances E. Neal as Ann Rogers
* Mildred Coles as Mary Jordan Powell
* Eric Blore as Mr. Hartford
* Marc Lawrence as Lefty Landers
* Damian OFlynn as Lt. Onslow
* Andrew Tombes as Art Seidel (hotel detective)
* Marion Martin as Ruby, aka Mary Jordan
* Arthur Shields as Matt Willis
* Rand Brooks as James Jimmy Powell
* Lee Bonnell as George Atkins Harry Burns as Big Sem Semenoff

 
==External links==
*  
*  

 
 
 
 
 


 