Majnu (1987 film)
{{Infobox film
| name           = Majnu
| image          =
| caption        =
| director       = Dasari Narayana Rao
| producer       = Dasari Padma
| writer         = Dasari Narayana Rao  
| starring       = Akkineni Nagarjuna Rajani Moon Moon Sen
| music          = Laxmikant-Pyarelal
| cinematography = P. Sarath Babu
| editing        = B. Krishnam Raju
| studio         = Taraka Prabhu Films
| distributor    =
| released       =  
| runtime        = 141 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
| website        =
}}
 Telugu Romance film tragic love tale produced & directed by Dasari Narayana Rao on Taraka Prabha Films banner. Starring Akkineni Nagarjuna, Rajani, Moon Moon Sen in lead roles and music is composed by Laxmikant-Pyarelal. The film recorded as Super Hit at box-office.  The film was remade in Tamil as Anand (1987 film)|Anand with Prabhu Ganesan|Prabhu.

==Cast==
 
*Akkineni Nagarjuna as Rajesh 
*Rajani as Aleykya
*Moon Moon Sen Satyanarayana
*Betha Sudhakar 
*J. V. Somayajulu as Gopala Krishna Gummadi 
*Suthi Velu 
*Sowcar Janaki
*K. R. Vijaya 
*Rama Prabha 
 

==Soundtrack==
{{Infobox album
| Name        = Majnu
| Tagline     = 
| Type        = film
| Artist      = Laxmikant-Pyarelal
| Cover       = 
| Released    = 1987
| Recorded    = 
| Genre       = Soundtrack
| Length      = 36:12
| Label       = Lahari Music
| Producer    = 
| Reviews     =
| Last album  =  
| This album  = 
| Next album  = 
}}

The music was composed by Laxmikant-Pyarelal. Lyrics written by Dasari Narayana Rao. All songs are hit tracks. Music released on LAHARI Audio Company.
{{tracklist
| collapsed       = no
| all_music       = 
| all_lyrics      = 
| extra_column    = Artist(s)
| total_length    = 36:12

| title1          = Nene Nene Hero  SP Balu
| length1         = 6:14

| title2          = Kadhalaku Kannu SP Balu, P. Susheela
| length2         = 5:15

| title3          = Idhi Tholi Rathri SP Balu
| length3         = 6:32

| title4          = Porabadithivo Twarabadithivo SP Balu
| length4         = 5:53

| title5          = I Love U SP Balu, P. Susheela
| length5         = 6:33

| title6          = Galidebba Thattukona SP Balu, P. Susheela
| length6         = 5:45
}}

==References==
 

==External links==
* 
*http://imovies4you.com/majnu-1987-watch-blockbuster-telugu-movie-online-and-download/

 
 
 
 
 
 
 