Socrates (film)
{{Infobox film
| name           = Socrates
| image          =Socrates (film).jpg
| image_size     =
| caption        =
| director       = Roberto Rossellini Renzo Rossellini                Renzo Rossellini  Roberto Rossellini 
| narrator       =
| starring       = See below
| music          = Mario Nascimbene
| cinematography = Jorge H. Martín
| editing        = Alfredo Muschietti
| distributor    =
| released       =
| runtime        = 120 minutes
| country        = Spain, Italy, France
| language       = Italian
| budget         =
| gross          =
}}
 French film directed by Roberto Rossellini.

==Plot==
After Athens fell under the government of the Thirty Tyrants, the lives of citizens are no longer safe. The philosopher Socrates, meanwhile, continues his so-called philosophical "preaching", gathering more and more young disciples. Among them is Plato, who notes the speeches of his master, not knowing about the conflict and what apparently seems so real as to be considered a principle. The youth of Athens like Socrates, although the conservatives, as the comedian Aristophanes, ridicule him, believing him to be one the sophists.

When Socrates claims to have received a vision of the gods at the shrine of Delphi, his conspirators accuse him in court, arguing that the philosopher preaches crazy doctrines to the youth, and does not believe in the gods, but demons. Socrates makes his "apology" (i.e., legal defense), but the citizens are against him; so Socrates is sentenced to death, and held in prison, awaiting execution. His disciples are desperate, and one of them, Crito, tries to help him by encouraging him to flee. Socrates rejects this idea, saying he must obey the rulers of the city. Socrates then resolves to die, and dies as soon as he is forced to drink hemlock.

As the philosopher dies, the people of Athens are aware of the serious mistake of the court.

==Cast==
*Jean Sylvère as Socrates
*Anne Caprile as Santippe
*Giuseppe Mannajuolo as Apollodoro
*Ricardo Palacios as Critone Antonio Medina as Platone Julio Morales as Antistene
*Emilio Miguel Hernández as Meleto
*Emilio Hernández Blanco as Ipperide
*Manuel Angel Egea as Cebete
*Jesús Fernández (actor)|Jesús Fernández as Cristobolo
*Eduardo Puceiro as Simmia
*José Renovales as Fedo
*Gonzalo Tejel as Anito
*Antonio Requena as Ermete
*Roberto Cruz as Un vecchio
*Francisco Sanz as Un attore
*Antonio Alfonso as Eutifrone
*Juan Margallo as Crizia
*Román Ariznavarreta as Calicle
*Francisco Calalá as Lisia
*Adolfo Thous as Ippio
*Bernardo Ballester as Teofrasto
*Jean-Dominique de La Rochefoucauld as Fedro
*César Bonet as Prete
*Jerzy Radlowsky as Giullare
*Pedro Estecha as Focione
*Rafael de la Rosa as Trasibulo
*Simón Arriaga as Servitore della cicuta
*Iván Almagro as Ermogene
*Constant Rodriguez as Aristefo
*Stefano Charelli as Efigene
*Luis Alonso Gulias as Echino
*Jesus A. Gordon as Lamprocle
*José Luis Ortega as Giovane figlio di Socrate
*Elio Seraffini as Prete

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 