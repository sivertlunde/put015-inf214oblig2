Affengeil
{{Infobox film
| name           =Affengeil
| image          = Affengeil film poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Rosa von Praunheim 
| producer       = Rosa von Praunheim
| writer         = 
| screenplay     = Eva Ebner   Lotti Huber   Rosa von Praunheim	 	
| story          = 
| based on       =  
| narrator       = 
| starring       = Lotti Huber   Rosa von Praunheim   Helga Sloop   Gertrud Mischwitky 
| music          = Marran Gosov    Thomas Marquard
| cinematography = Klaus Janschewsky   Mike Kuchar
| editing        = Mike Shephard 	
| studio         = Exportfilm Bischoff 
| distributor    = First Run Features
| released       = 27 October 1990 
| runtime        = 87 minutes
| country        = West Germany
| language       = German
| budget         = 
| gross          = 
}}
Affengeil  (    and Our Corpses Are Still Alive.  Murray, Images in the Dark, p. 109 

The film consists of extended interviews with Huber using photographs, film clips and field trips to recreate her life. The ten year professional relationship between the film director and his subject is explored. Rosa von Praunheim appears arguing about the control of the direction of the film as Hubert insist to be portrayed "Just as I am" Murray, Images in the Dark, p. 109 
==Plot==
Lotti Huber, in her late seventies at the time of filming, is a short, chubby flamboyant old lady of remarkable vivacity. Appearing wearing large hoop earrings and dramatic makeup, she recounts vividly important events and successes of her life. At the beginning of the film, she demands from Praunheim to make a documentary about her.

Huber was born in Kiel in northwestern Germany and grew up in Weimar. Her mother inflected in her a sense of independence and a love for studies. At age 16 she fell in love with the son of the major of her town. As she was Jewish and he was Aryan, their relationship ran against racial Nazi ideology. Her boyfriend was arrested and shot by the Nazis while she was sent to a concentration camp from which she escaped. The details of her flight are so fabulously incredible, she claims, that she refrains from telling that story. 

After the war she traveled extensively: Palestine, Egypt, Cyprus and London. By age 27 she was living in Israel, where she developed a successful career as an exotic dancer. She later opened and operated a hotel and restaurant in Cyprus, something unusual for a woman to do in that time and place. She did not return to Germany until the 1960s when, she established a charm school in Berlin. She later spent several years demonstrating products in a department store. In one scene, she is shown teaching belly-dancing to middle-aged women. While she had several marriages, she worked as cabaret performer, dancer, restaurateur, teacher, model, movie extra and finally actress starring in two films directed by Rosa von Prauunheim. Their ten year professional relationship is shown as bickering. Praunehim question the veracity of some of Hubert’s incredible tales and Lotti appears asking the director’s mother about her son’s homosexuality.

==Notes==
 

== References ==
*Murray, Raymond. Images in the Dark: An Encyclopedia of Gay and Lesbian Film and Video Guide. TLA Publications, 1994, ISBN 1880707012

==External links==
* 

 
 
 
 