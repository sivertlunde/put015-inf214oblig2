Open Season (1974 film)
 
{{Infobox film
| name           = Open Season
| image_size     = 
| image          = Open Season FilmPoster.jpeg
| caption        =  Peter Collinson
| writer         = 
| narrator       = 
| starring       = Peter Fonda
| music          = 
| cinematography = 
| editing        = 
| studio         = Arpa Productions Impala
| distributor    = Columbia Pictures
| released       = August 1974
| runtime        = 104 min.
| country        = Spain
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Peter Collinson. It stars Peter Fonda, John Phillip Law, William Holden and Cornelia Sharpe.  The film was shot in both Spain and England, with parts of those countries used to portray the American backwoods. The screenplay was by David Osborn and Liz Charles Williams, based on Osborns novel.

==Story==
The film follows three former Vietnam veterans who are accustomed to violence. They decide to go on a spree of brutality by kidnapping a girl and a middle-aged man. After an orgy of rape, beatings, and humiliation, the two victims plan an escape and one plans revenge.

==Cast==
*Peter Fonda as Ken
*Cornelia Sharpe as Nancy Stillman
*John Phillip Law as Greg Richard Lynch as Art
*Alberto de Mendoza as Martin
*William Holden as Hal Wolkowski 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 