Poor Jake's Demise
 
 
{{Infobox film
| name           = Poor Jakes Demise
| image          = 
| caption        = 
| director       = Allen Curtis
| producer       = 
| writer         =  Max Asher Lon Chaney
| music          = 
| cinematography = 
| editing        =  Universal Film Manufacturing Company
| released       =  
| runtime        = 1 reel
| country        = United States 
| language       = Silent English intertitles
| budget         = 
}}
 silent Short short slapstick Lon Chaney. compromising position Universal at the start of his career and is also his first credited screen role. The film was presumed lost film|lost, but a fragment of the film was discovered in England in May 2006.

== Plot and cast ==
Jake comes home and finds his wife and Willy Mollycoddle in a compromising position. Enraged, Jake throws Willy out of the house and scolds his wife and threatens to kill himself. Fearful that Jake will commit suicide, the wife calls the police and three officers are sent out to find Jake. Stopping at a bar before he commits suicide, Jake finds Willy who is drowning his sorrows. Jake takes his revenge on Willy with a seltzer bottle.    

== Cast == Max Asher as Jake Schultz 
* Daisy Small  as Mrs. Schultz 
* Lon Chaney  as The Dude 
* Louise Fazenda  as Servant 

== Production ==
Poor Jakes Demise was directed by Allen Curtis and produced by Independent Moving Pictures Company (IMP) and distributed by Universal Film Manufacturing Company. The screenplay author was not credited. The film is notable for having been the first billed appearance of Lon Chaney and perhaps the debut of Louise Fazenda.    

== Release and reception ==
The film was released on August 16, 1913 and had viewings in Texas,  North Carolina,  Pennsylvania,  Wisconsin,   and Illinois.  A surviving contemporary review of the film in Moving Picture World described it as "simply horse play without any special appeal, though it is harmless and lacks vulgarity." 

The film was presumed lost film|lost, but a fragment of the film was discovered in England in May 2006. It has since been restored by the Haghefilm Laboratory of Amsterdam and Lobster Films, Paris.    The restored fragment is 7 minutes and 52 seconds long.    In 2006, the film was shown at the Pordenone Silent Film Festival.  The film would also be shown at the 31st Cinéfest Sudbury International Film Festival on the evening of March 19, 2011. 

In 1957, an article by Jim Neal of the Denton Record-Chronicle cited this as the first of Lon Chaneys films.   Don G. Smiths book, Lon Chaney, Jr.: Horror Film Star, 1906-1973 also claims this film as Chaneys first.  Rosemary Guiley would also refer to this claim in The Encyclopedia of Vampires, Werewolves, and Other Monsters.  Chronologically, this is the first released film with a confirmed credit to Lon Chaney and also the first billed release. According to Blake, Lon Chaney did not appear in The Honor of the Family or The Ways of Fate, two earlier films sometimes credited to feature Chaney.  It is possible that other films with unbilled appearances have yet to be discovered.

==See also==
*List of rediscovered films

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 