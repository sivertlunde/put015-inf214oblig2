Sex & Drugs & Rock & Roll (film)
{{Infobox film
| name           = Sex & Drugs & Rock & Roll
| image          = Sexdrugsrockrollmovie.jpg
| caption        = Theatrical release poster
| director       = Mat Whitecross
| producer       = Damian Jones
| writer         = Paul Viragh
| starring       =  
| cinematography = Christopher Ross
| music          = Chaz Jankel
| studio         = 
| distributor    = 
| released       =  
| runtime        = 113 minutes
| country        = United Kingdom
| language       = English 
| budget         = 
| gross          =
| followed_by    = 
}} new wave musician Ian Dury (12 May 1942 – 27 March 2000), starring Andy Serkis as Dury. The film follows Durys rise to fame and documents his personal battle with the disability caused by having contracted polio during childhood. The effect that his disability and his lifestyle have upon his relationships is also a focal point of the film.    The title of the film is derived from Durys 1977 7" single, "Sex & Drugs & Rock & Roll".

Principal photography began on 29 April 2009 in Egham, Surrey  and theatre footage was shot 1–3 June at Watford.

==Cast==
*Andy Serkis as Ian Dury
*Naomie Harris as Denise
*Ray Winstone as Bill Dury
*Olivia Williams as Betty Dury
*Noel Clarke as Desmond
*Toby Jones as Hargreaves
*Ralph Ineson as The Sulphate Strangler
*Mackenzie Crook as Russell Hardy
*Bill Milner as Baxter Dury
*Michael Maloney as Graham
*Arthur Darvill as Mick Gallagher Luke Evans as Clive Richards John Turnbull Tom Hughes as Chaz Jankel
*Clifford Samuel as Charley Charles
*Jennifer Carswell as Ruby
*Stephanie Carswell as Mia
*Joseph Kennedy as Davey Payne
*Catherine Balavage as Crazy drug girl (uncredited)

==Awards==
{| class="wikitable"
|-
! Award
! Category
! Nominee
! Result
|-
| British Independent Film Awards
| Best Actor
| Andy Serkis
| Nominated
|- British Academy British Academy Film Award Best Actor
| Andy Serkis
| Nominated
|-
| Music
| Chaz Jankel
| Nominated
|-
| Evening Standard Film Awards
| Best Actor
| Andy Serkis
| Won
|}

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 