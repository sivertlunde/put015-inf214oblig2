Rescued from an Eagle's Nest
 
{{Infobox film
| name           = Rescued from an Eagles Nest
| image          = Rescued from an eagles nest still.jpg
| caption        = 
| director       = J. Searle Dawley
| producer       = 
| writer         = 
| starring       = Henry B. Walthall
| cinematography = Edwin S. Porter
| editing        = 
| distributor    = 
| released       =  
| runtime        = 6 minutes 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
  silent action-drama co-directed by Edwin S Porter and J. Searle Dawley for the Edison film studios.

This film is significant for both being an early example of the motion picture craft and the film acting debut of the future director D. W. Griffith. In the opening scene, he exits a cabin. This appearance was prompted by being both broke and stranded in New York after the failure of a play he authored. He responded to an Edison studios offer of $15 per treatment with a script idea which was based on the Puccini opera, Tosca. Porter rejected Griffiths treatment then offered him the lead role in this production. Six months later he directed his first film for Biograph. Only one other film in which Griffith appears as an actor survives. A print of Rescue survives in the Museum of Modern Art film archive.   

==Plot==
A woodsman leaves a hut followed by a woman with their baby. Nearby some men chop down a tree. The baby is left outside the hut, but an eagle flies away with it. The mother comes outside and sees what has happened. She picks up a gun and aims, but decides against it. She tells the woodmen and they get to the cliff where the eagles nest is. One of the men is let down on a rope to the nest. However the eagle attacks, but he kills it and kicks it of the cliff. He then picks up the baby, is hoisted up the cliff, and returns the baby to its mother.

==Cast==
* D. W. Griffith as Father
* Henry B. Walthall as Woodsman (as H. B. Walthall)
* Miss Earle
* Jinnie Frazer as Baby

==See also==
* List of American films of 1908
* 1908 in film

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 
 