The Last of the Knucklemen
 
 
{{Infobox film name           = The Last of the Knucklemen image          = caption        = producer       = Tim Burstall Byron Kennedy director       = Tim Burstall  writer         = Tim Burstall based on       = play by John Power starring  Gerard Kennedy Michael Preston Peter Hehir music          = Bruce Smeaton cinematography = Dan Burstall editing        = Edward McQueen-Mason studio         = Hexagon Productions distributor    = Umbrella Entertainment released       =   runtime        = 93 minutes country        = Australia language       = English  budget         = A$460,000 David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p38  gross          = A$180,000 (Australia)
}}
The Last of the Knucklemen is a 1979 Australian film directed by Tim Burstall.

==Plot== Gerard Kennedy) before the big fight.

==Cast== Gerard Kennedy as Tarzan
*Michael Preston as Pansy
*Peter Hehir as Tom
*Dennis Miller as Horse
*Michael Caton as Monk
*Steve Rackman as Carl
*Michael Duffield as Methuselah
*Steve Bisley as Mad Dog
*Stewart Faichney as Tassie
*Gerry Duggan as Old Arthur

==Original play==
{{Infobox play
| name       = The Last of the Knucklemen
| image      = 
| image_size = 
| caption    = 
| writer     = John Powers
| characters = 
| setting    = A mining camp, north-west Australia
| premiere   = 19 November 1973
| place      = Russell St Theatre, Melbourne
| orig_lang  = English
| subject    = 
| genre      =
}}
John Powers play had been produced in 1973. 

Leslie Rees described it as "a sequence of sketches using the same basic characters but without much development or thematic resolution". Leslie Rees, Australian Drama in th 1970s, Angus & Robertson, 1978 p 207 

==Production==
Before Tim Burstall started on Eliza Fraser he thought Hexagon Productions should make a male bonding film, and considered Rusty Bugles, The Odd Angry Shot and Last of the Knucklemen. He eventually decided on the latter. He had to wait to get the rights because the Melbourne Theatre Company were negotiating to sell the rights to the US but this fell through. Scott Murray, Tim Burstall, Cinema Papers Sept-Oct 1979 p577 

Burstall did the adaptation himself, which was largely faithful to the play. He felt that the film was weak in the first half setting up characters.  Burstall:
 I was trying to take the ocker stuff and cross it, as I think John Powers play was, with anthropology. Before I rehearsed the cast, I got them to read The Territorial Imparity of the Native Aid. I wanted it to be seen not just as ockerism but as anthropology. But the only people who got that were the French. It was bought in France and its done terribly well there – much better than it ever did in Australia.   accessed 14 October 2012  

The movie was shot over six weeks in September and October 1978 mostly on sets at Melbournes Cambridge Studios. 

==Reception==
The Last of the Knucklemen grossed $180,000 at the box office in Australia,  which is equivalent to $703,800 in 2009 dollars. Reviews however were strong.  Burstall:
 I dont think they knew how to market it. A lot of women said to me, Id never go to a picture that had the title The Last of the Knucklemen. But nobody ever looked at it as an analysis of the way men work. Its a right-wing view of unionism.  

==Home media==
The Last of the Knucklemen was released on DVD by Umbrella Entertainment in January 2012. The DVD is compatible with region codes 2 and 4 and includes special features such as the trailer, photo gallery and interviews with John Powers, Gerard Kennedy, Dan Burstall, Steve Bisley and Michael Caton.   

==See also==
*Cinema of Australia

==References==
 

==Further reading==
* 

==External links==
* 
*  at Oz Movies
 

 
 
 
 
 
 
 
 