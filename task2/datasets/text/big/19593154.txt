Babu (1985 film)
 
{{Infobox film
| name           = Babu
| image          =
| image size     =
| caption        =
| director       = A. C. Tirulokchandar
| producer       = V.R. Parameshwaram
| writer         = A. C. Tirulokchandar
| based on = Odeyil Ninnu by P. Kesavadev
| starring       = Rajesh Khanna Hema Malini Rati Agnihotri Madan Puri Rajendranath
| music          = Rajesh Roshan
| cinematography =
| editing        =
| distributor    =
| released       =  1985
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
}} Odayil Ninnu, novel of the same title. 

==Plot==
Babu is a young man who works as a rickshaw puller and he has fallen in love with Kammo.His only true friend is Shambu Nath.One day he helps Shankerlal and his family and in return Shankerlal invites him to his house.Instantly the whole family including Shankerlals wife and Pinky both begin to shower love on Babu. Babu having led a tough childhood feels very ecstatic and feels grateful to Shankerlal on having provided him food,clothes and more importantly respect given to him. Meanwhile a goon in the village rapes Kammo and Babu kills him and thereby lands in jail. After his release he finds Pinky begging in the streets and then he gets shocked as to how a rich girl has been forced to beg. Then he realises that now Pinkys mother is a widow. His sole aim now becomes to help this widow and her child. He gives up his personal life, drives a hand-drawn rickshaw, saves some money, so that he can buy provisions for them, as well as send the child, Pinky, to a decent school. Pinky then grows up and later starts disliking Babu.The rest of the story is how Pinky realises her folly later on, how Pinkys mother feels indebted to Babu for having helped them in return for one nights shower of affection on Babu by Shankerlal. 

==Cast==
*Rajesh Khanna as Babu
*Hema Malini as Kammo
*Rati Agnihotri as Pinky
*Mala Sinha as Widow
*Madan Puri as Shambu Nath
*Navin Nischol as Shankarlal

==Music==
*Aise Rang De Piya - Kishore Kumar Lata Mangeshkar
*Main Kuwari Albeli - Kishore Kumar Asha Bhosle
*Yeh Mera Jeevan - Kishore Kumar
*Ae Hawa - Lata Mangeshkar

==References==

 

==External links==
* 

 

 
 
 
 
 
 
 