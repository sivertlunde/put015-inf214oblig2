Victory and Peace
{{Infobox film
| name           = Victory and Peace
| image          =
| caption        =
| director       = Herbert Brenon
| producer       = 
| writer         = Hall Caine
| starring       = Matheson Lang Marie Lohr James Carew   Ellen Terry
| music          =
| cinematography = 
| editing        = 
| studio         = National War Aims Committee
| distributor    = National War Aims Committee
| released       = December 1918
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} silent war film directed by Herbert Brenon and starring Matheson Lang, Marie Lohr and James Carew. The film was produced by the National War Aims Committee for propaganda reasons during the First World War. The screenplay was written by the novelist Hall Caine, who was personally invited to do so by the Prime Minister David Lloyd George. Because the war ended, the film never went on general release. It is a partially lost film, with only around 1,000 feet of film still surviving.  Its alternative title is The Invasion of Britain.

==Cast==
* Matheson Lang as Edward Arkwright
* Marie Lohr as Barbara Rowntree
* James Carew as Karl Hoffman
* Ellen Terry as Widow Weaver
* Renee Mayer as Jenny Banks
* Hayford Hobbs as Charlie Caine
* Frederick Kerr as Sir Richard Arkwright
* Jose Collins as  Madge Brierley
* Sam Livesey as Capt. Schiff
* Edith Craig as Mary Rountree
* Bertram Wallis as Bob Brierley
* Ben Greet as Mayor of Castleton
* Harding Thomas as Jim Banks
* Arthur Applin as Capt, von Lindheimer
* Henry Vibart as Bishop
* Sydney Lewis Ransome as Rev. Paul Brayton
* Joyce Templeton as Joyce Brierley
* Helena Millais as Liz Lowery
* Rolf Leslie as Abraham Lincoln

==References==
 

==Bibliography==
* Allen, Vivien. Hall Caine: Portrait of a Victorian Romancer. Continuum, 1997.
* Low, Rachael. History of the British Film, 1914-1918. Routledge, 2005.

==External links==
* 

 

 
 
 
 
 
 
 


 
 