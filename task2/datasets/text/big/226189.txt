The Black Watch
 
{{Infobox film
| name           = The Black Watch
| image          = Poster of the movie The Black Watch.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = John Ford
| producer       = Winfield R. Sheehan 
| writer         = {{Plainlist|
* James Kevin McGuinness
* John Stone
}}
| based on       =  
| starring       = {{Plainlist|
* Victor McLaglen
* Myrna Loy
* David Torrence
}}
| music          = William Kernell
| cinematography = Joseph H. August
| editing        = Alex Troffey
| studio         = Fox Film Corporation
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} adventure epic Black Watch regiment assigned to a secret mission in India just as his company is called to France at the outbreak of war. His covert assignment results in his being considered a coward by his fellows, a suspicion confirmed when he becomes involved in a drunken brawl in India that results in the death of another officer. The film features an uncredited 21-year-old John Wayne working as an extra; he also worked in the arts and costume department for the film.    

==Cast==
* Victor McLaglen as Capt. Donald Gordon King
* Myrna Loy as Yasmani
* David Torrence as Field Marshal
* David Rollins as Lt. Malcolm King
* Cyril Chadwick as  Maj. Twynes
* Lumsden Hare as Colonel of the Black Watch
* Roy DArcy as  Rewa Ghunga
* David Percy as Soloist, Black Watch Officer
* Mitchell Lewis as  Mohammed Khan Claude King as General in India Walter Long as Harrim Bey Francis Ford as Maj. MacGregor Frederick Sullivan as Generals Aide
* Richard Travers as Adjutant
* Pat Somerset as OConnor, Black Watch Officer
* Joseph Diskay as Muezzin
* Joyzelle Joyner as Dancer
* Gregory Gaye as a 42nd Highlander (uncredited) Mary Gordon as Sandys Wife (uncredited)
* Bob Kortman as a 42nd Highlander (uncredited)
* Tom London as a 42nd Highlander (uncredited)
* Jack Pennick as a 42nd Highlander (uncredited)
* Randolph Scott as a 42nd Highlander (uncredited)
* Phillips Smalley as the Doctor (uncredited)
* Lupita Tovar in a Bit Part (uncredited)
* John Wayne as a 42nd Highlander (uncredited)   

==See also==
* List of American films of 1929

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 