My Name Is Alan and I Paint Pictures
{{Infobox film
| name = My Name Is Alan and I Paint Pictures
| image = 
| caption = 
| director = Johnny Boston
| producer = Colleen Ryan
| writer = Johnny Boston
| starring = Alan Russell-Cowan
| music = 
| cinematography = 
| editing = Todd Drezner
| distributor = 
| released =  
| runtime = 
| country = United States
| language = English
| budget = 
| gross = 
}}
My Name is Alan, and I Paint Pictures is a 2007 documentary film directed by Johnny Boston.  The film stars and is materially about Alan Russell-Cowan, an artist diagnosed with schizophrenia.

The documentary My Name is Alan, and I Paint Pictures focuses on Alan Russel-Cowan, a street painter diagnosed with paranoid schizophrenia, as he works to break his way into the professional art world.

The film also addresses larger issues which directly or indirectly affect Alan. Subjects addressed include the treatment and diagnosis of Paranoid Schizophrenia; the therapeutic benefits of art for mental illness; and the path to success for artists and the politics of the art establishment.

The film had an art-house theatrical run in New York and Los Angeles in 2007 and 2008.

Most recently My Name is Alan... was shown on the Ovation Arts Network in February and May 2009.

==Reception==
The film has received sparse and varied reviews.  The Village Voice gives a generally negative review,  while the New York Times a positive one. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 