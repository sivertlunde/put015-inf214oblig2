Mr. Wrong
 
{{Infobox film
| name           = Mr. Wrong
| image          = Mr wrong.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Nick Castle
| producer       = Marty Katz
| writer         = Chris Matheson Kerry Ehrin Craig Munson
| narrator       =
| starring       = Ellen DeGeneres Bill Pullman Joan Cusack Dean Stockwell Joan Plowright
| music          = Craig Safan
| cinematography = John Schwartzman
| editing        = Patrick Kennedy
| studio         = Touchstone Pictures Buena Vista Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = $19 million
| gross          = $12.3 million
| preceded_by    =
| followed_by    =
}}
 1996 American Romance film|romantic/black comedy film starring Ellen DeGeneres and Bill Pullman. Ellen DeGeneres still mentions this film occasionally in her talk show, The Ellen DeGeneres Show. The film was a critical failure and a box office bomb.

==Plot summary== incarcerated in Mexican prison. wedding day.

Some time ago, at her younger sisters wedding, Martha is pestered by her family about when shell get married. At work, she rejects a date with a younger co-worker, Walter (John Livingston). Disappointed by her dull  , Martha goes home to   in front of the TV, where, inundated by romantic imagery, she is prompted to get out of the house.
 quarter in front of the jukebox. She bends down to get it, then a man shows up and selects the same song Martha would have chosen. The man is Whitman Crawford (Pullman), and they instantly hit it off. They go back to his house and have sex. Whitman says hes a poet and an investor. He reads Martha one of his poems.
 harass Martha. Inga refuses to believe that Martha has dumped Whitman. Whitman refuses to believe it either, and begins stalking Martha and trying to woo her back in increasingly ridiculous ways. Whitman even buys off Marthas private investigator (Dean Stockwell) and abducts Martha to Mexico to marry her there.

Walter shows up to rescue Martha from the wedding, but trips. His gun falls into Marthas hand and she shoots Whitman. Then shes arrested, and the investigators conclude that she murdered Whitman. Walter springs her out of jail and explains that it was Inga who shot Whitman.

Walter and Martha ride a horse West towards the sunset. Title cards over this scene explain that they eventually turned North towards the American border, that Inga and Bob get married and open a pet store in Albuquerque, and that Whitman continues his search for love.

==Cast==
* Ellen DeGeneres as Martha Alston
* Bill Pullman as Whitman Crawford
* Joan Cusack as Inga Gunther
* Dean Stockwell as Jack Tramonte
* Joan Plowright as Mrs. Crawford
* Hope Davis as Annie
* Ellen Cleghorne as Jane
* Robert Goulet as Braxton
* John Livingston as Walter

==Reception==
The film received extremely negative reviews, garnering a score of 4% on Rotten Tomatoes. 

Mick LaSalle, writing for the San Francisco Chronicle, found this film "dreadful" and "inherently unfunny" after Martha dumps Whitman and he begins stalking her. 
 Looking for Mr. Goodbar" and laments that "Ellen DeGeneres, a comedian and sitcom star in her film debut,   is ostensibly the protagonist here" does not control the action, but her character "merely reacts to   twists and turn-ons". 

Not every critic was disparaging of the film, however. Martin & Porter gave it three stars, and while acknowledging that "the script is predictable and Nick Castles direction is only adequate", they found that "DeGeneress personal charm and a few inspired gags make it all worthwhile". 
 Barb Wire.

===Box office===
The movie debuted at No. 6. 

==References==
 

==External links==
 
* 
* 
* 

 

 
 
 
 
 
 
 