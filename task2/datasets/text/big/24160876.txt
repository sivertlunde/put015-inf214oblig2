Sphinx (film)
 
{{Infobox film
| name           = Sphinx 
| image          = Sphinx-film-poster.jpg
| image_size     = 
| alt            = 
| caption        = Original poster
| director       = Franklin J. Schaffner
| producer       = Stanley OToole
| screenplay     = John Byrum Robin Cook 
| starring       = Lesley-Anne Down Frank Langella
| music          = Michael J. Lewis
| cinematography = Ernest Day 
| editing        = Robert Swink Michael F. Anderson 
| studio         = Orion Pictures
| distributor    = Warner Bros.
| released       = February 11, 1981
| runtime        = 118 minutes
| country        = United States
| language       = English
| budget         = $14 million
| gross          = $2,022,771
}}
 novel of Robin Cook.

==Plot== Pharaoh Seti. Soon after her arrival in Cairo, she witnesses the brutal murder of unscrupulous art dealer Abdu-Hamdi, meets a French journalist named Yeon, and is befriended by Akmed Khazzan, who heads the antiquities division of the United Nations. When she journeys to the Valley of the Kings in Luxor to search a tomb reportedly filled with treasures, she finds herself the target of black marketeers determined to keep the riches themselves. 

==Production==
Film rights were purchased by Orion Pictures for $1 million. Orion: A Humanistic Production
Kilday, Gregg. Los Angeles Times (1923-Current File)   05 Jan 1979: f13.  
 Winter Palace Hotel in Luxor, and Thebes, Egypt|Thebes.

==Cast==
*Lesley-Anne Down as Erica Baron 
*Frank Langella as Akmed Khazzan 
*Maurice Ronet as Yeon 
*John Gielgud as Abdu-Hamdi
*Vic Tablian as Khalifa  Martin Benson as Mohammed 
*John Rhys-Davies as Stephanos Markoulis
*Nadim Sawalha as Gamal
*Tutte Lemkow as Tewfik 
*Saeed Jaffrey as Selim
*Eileen Way as Aida 
*William Hootkins as  Don 
*James Cossins as Lord Carnarvon 
*Victoria Tennant as Lady Carnarvon

==Critical reception== Nile Hilton." In conclusion, he called the film "total, absolute, utter confusion."  
 
Variety (magazine)|Variety described the film as a contemporary version of The Perils of Pauline and called it "an embarrassment," adding "Franklin J. Schaffners steady and sober style is helpless in the face of the mounting implausibilities."  

Time Out New York thought the film made "striking use of locations" but criticized the "lousy script, uneasy heroine, and weak material." It called it a "clear case of a lame project that only a best selling (ie. heavily pre-sold) novel could have financed" and warned audiences to "avoid" it.  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 