My Dearest Enemy
{{Infobox Film
| name           = My Dearest Enemy
| image          = Dearestenemy.jpg  
| image_size     =
| caption        = 
| director       = Bernard Dufour Francois Rabatth
| producer       = Nelka Films
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        =
| distributor    = 2004 
| runtime        = 60 min.
| country        =  English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} documentary that Palestinian Hani El Hassan—both powerful leaders in opposite camps.

==Summary==
The film follows a heated conference between two men who have the power to instigate change. Amnon was the chief-of-staff of Israel’s military, and Hani was one of Yasser Arafat’s right-hand men. The filmmakers worked exhaustingly to organize this unofficial meeting between two powerful people who have more in common than might be expected.

“We’ve worked on this handshake for more than a year,” the filmmaker says as Amnon enters the room and greets Hani. 
 Palestinian guerrilla soldiers led by Hani, and in 1973 both men were in Beirut during Israel’s raid on Lebanon. Now, instead of interacting from behind guns, the two sit comfortably in an air-conditioned room, talking over finely prepared meals and red wine.

But instead of staying fixed in the conference room, the film offers a glimpse into Amnon and Hani’s personal lives to expose the roots of their political beliefs. Breaking from his discussions with Amnon, Hani goes to visit his childhood home in Haifa, which has now become the holy site of Elija’s tomb. The day trip stirs up resentment and frustration in Hani, who is forced to remember his bitter, violent departure from the home that had been the peaceful sanctuary of his youth. Walking beside the stone walls, he remembers how Israeli Haganah soldiers spoke disrespectfully to his mother and abused him before forcing the family to leave their home. “Amnon always talks of trust,” Hani says, “but how can I trust someone who stole my land and who takes a little bit more of it every day?”

==See also==
Other Israeli Documentaries about the conflict between Israelis and Palestinians:
*The Land of the Settlers
*All Hell Broke Loose
*On the Green Line
*Nadias Friend

==References==
*{{cite web
  | title = My Dearest Enemy
  | publisher = Europa Images
  | url = http://www.europeimages.com/en/programmes/3935-my-dearest-enemy/
  | accessdate =  2008-10-20 }}
*{{cite web
  | title = My Dearest Enemy
  | publisher = The Jewish Channel
  | url = http://www.tjctv.com/movies/my-dearest-enemy/
  | accessdate =  2008-10-20 }}

==External links==
* 
* 

 
 
 