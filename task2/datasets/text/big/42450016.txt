Marmato
{{Infobox film
| name           = Marmato
| image          = Marmato poster.jpg
| caption        = Promotional poster
| director       = Mark Grieco
| producer       = Mark Grieco Stuart Reid Harry Swartz-Turfle Izabel Hoyos	
| writer         = Mark Grieco Stuart Reid
| starring       = 
| music          = Todd Boekelheide
| cinematography = Mark Grieco
| editing        = Ricardo Acosta Mark Grieco
| studio         = Calle Films
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = Colombia United States
| language       = Spanish English
| budget         = 
}} Candescent Award. Pamela McClintock,   The Hollywood Reporter, January 18, 2014.  

==Synopsis==
The film narrates 6-years long struggle of townspeople of Marmato, Caldas with Canadian mining company that wants the $20 billion in gold beneath their homes.

==Production==
Director, Mark Grieco lived and filmed in the town of Marmato over the course of 5 1/2 years from 2008-2013. The entirety of the film takes place in the town except for two scenes in the nearby city of Medellín. Grieco worked alone filming with one camera, the Canon XH-A1. Production was forced to end when it became too dangerous to continue to film in the town.  

==Reception==
The film received positive response from critics. Kenneth Turan in his preview of Sundance 2014 for the Los Angeles Times said that it was among the festivals most memorable films and is "made with exceptional artistry."    

Guy Lodge in his review for Variety (magazine)|Variety said that "Mark Griecos detailed, patient chronicle of a Colombian gold rush is equal parts passion and compassion project."  

Mark Adams of Screen International said it is "a striking and vivid story" and "a fascinating glimpse into a tough and resilient community"  

Daniel Feinberg in his Sundance review for HitFix called it "one of the best films Ive seen in this years U.S. Documentary Competition" with "compelling and fully realized characters". He goes on to say, "The whole thing is like a Werner Herzog film, mans base desires, juxtaposed with sublime and unconquerable nature"  

Jordan M. Smith from Ioncinema gave the film a positive review by saying that "Grieco’s film acts as an advocate for cultural preservation, but in doing he’s unfortunately woven a story as muddled as the conflict it documents." 

However, Justin Lowe of The Hollywood Reporter said, "Sympathetic account of beleaguered small-town residents doesn’t muster enough material to prove entirely persuasive." 

==Accolades==
  
 
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan="12"| 2014 Sundance Film Festival
| U.S. Grand Jury Prize: Documentary
| Mark Grieco
|  
|-
| Candescent Award
| Mark Grieco
|   
|-
| rowspan="3"| Cartagena Film Festival
| Best Documentary Film
| Mark Grieco
|     
|-
| Best Colombian Film
| Mark Grieco 
|   
|-
| Audience Award for Best Documentary
| Mark Grieco 
|   
|-
| Ashland Independent Film Festival
| Best Editing Feature Documentary
| Mark Grieco and Ricardo Acosta
|   
|-
| Environmental Film Festival at Yale
| Best Environmental Storytelling
| Mark Grieco
|     
|-
| Seattle International Film Festival
| Grand Jury Prize: Best Documentary
| Mark Grieco 
|   
|-
| Astra Film Festival
| Eco Cinematography Award
| Mark Grieco 
|   
|-
| Santa Fe Independent Film Festival
| Audience Choice Best Documentary Feature
| Mark Grieco 
|   
|- Habana International Film Festival
| Special Jury Prize
| Mark Grieco 
|   
|}
 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 