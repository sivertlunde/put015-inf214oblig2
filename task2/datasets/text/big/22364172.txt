The Cohens and Kellys
{{Infobox film
| name           = The Cohens and Kellys
| image          = Poster of the movie The Cohens and Kellys.jpg
| image_size     =
| caption        =
| director       = Harry A. Pollard
| producer       = E.M. Asher Edward Small
| writer         = Alfred A. Cohn Aaron Hoffman (play Two Blocks Away) Harry A. Pollard Charles Murray Kate Price Jason Robards Sr.
| music          =
| cinematography = Charles Stumar
| editing        = Universal Pictures
| released       =  
| runtime        = 80 mins.
| country        = United States
| language       = English
}}
 silent comedy Charles Murray, Kate Price film serials. copyright infringement case, in which Judge Learned Hand articulated the doctrine that copyright protection does not cover the characteristics of stock characters in a story. 

==Plot==
As articulated in the Nichols case, 

{{Quote|The Cohens and The   Kellys presents two families, Jewish and Irish, living side by side in the poorer quarters of New York in a state of perpetual enmity.  The wives in both cases are still living, and share in the mutual animosity, as do two small sons, and even the respective dogs.  The Jews have a daughter, the Irish a son; the Jewish father is in the clothing business; the Irishman is a policeman.  The children are in love with each other, and secretly marry, apparently after the play opens.  The Jew, being in great financial straits, learns from a lawyer that he has fallen heir to a large fortune from a great-aunt, and moves into a great house, fitted luxuriously.  Here he and his family live in vulgar ostentation, and here the Irish boy seeks out his Jewish bride, and is chased away by the angry father.  The Jew then abuses the Irishman over the telephone, and both become hysterically excited.  The extremity of his feelings makes the Jew sick, so that he must go to Florida for a rest, just before which the daughter discloses her marriage to her mother.

On his return, the Jew finds that his daughter has borne a child; at first he suspects the lawyer, but eventually learns the truth and is overcome with anger at such a low alliance.  Meanwhile, the Irish family who have been forbidden to see the grandchild, go to the Jews house, and after a violent scene between the two fathers in which the Jew disowns his daughter, who decides to go back with her husband, the Irishman takes her back with her baby to his own poor lodgings.   The lawyer, who had hoped to marry the Jews daughter, seeing his plan foiled, tells the Jew that his fortune really belongs to the Irishman, who was also related to the dead woman, but offers to conceal his knowledge, if the Jew will share the loot.  This the Jew repudiates, and, leaving the astonished lawyer, walks through the rain to his enemys house to surrender the property.  He arrives in great dejection, tells the truth, and abjectly turns to leave.  A reconciliation ensues, the Irishman agreeing to share with him equally.  The Jew shows some interest in his grandchild, though this is at most a minor motive in the reconciliation, and the curtain falls while the two are in their cups, the Jew insisting that in the firm name for the business, which they are to carry on jointly, his name shall stand first.}}

==Cast== Charles Murray - Patrick Kelly
*George Sidney - Jacob Cohen
*Vera Gordon - Mrs. Cohen Kate Price - Mrs. Kelly
*Jason Robards Sr. - Tim Kelly 
*Olive Hasbrouck - Nannie Cohen
*Nat Carr - Milton J. Katz Robert Gordon - Sammy Cohen
*Mickey Bennett - Terence Kelly

==Related films==
The Cohens and Kellys was the first in a series of films featuring characters from the Cohen and Kelly families.  It was followed by The Cohens and the Kellys in Paris (1928), The Cohens and Kellys in Atlantic City (1929),  The Cohens and the Kellys in Scotland (1930),  The Cohens and the Kellys in Africa (1930),  and The Cohens and Kellys in Trouble (1933).

It was an early production of Edward Small. 

==References==
 

==External links==
*  
*   
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 


 