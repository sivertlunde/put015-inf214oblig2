The Betrayed (2008 film)
{{Infobox film
| name           = The Betrayed
| image          = The Betrayed (2008 film) dvd.jpg
| alt            = 
| caption        = DVD cover
| border         = yes
| director       = Amanda Gusack
| producer       = 
| writer         = Amanda Gusack
| starring       = Melissa George Oded Fehr Christian Campbell Alice Krige
| music          = Deborah Lurie
| cinematography = Roger Vernon
| editing        = Matthew Booth
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} thriller film, directed by Amanda Gusack from her own screenplay and starring Melissa George, Oded Fehr and Christian Campbell.

==Plot==
The story follows a young woman as shes put through a psychological journey under the thumb of a mysterious figure who suspects her husband of stealing millions from a crime syndicate. 

==Cast==
*Melissa George ... as Jamie 
*Oded Fehr ... as Voice / Alek 
*Christian Campbell ... as Kevin 
*Alice Krige ... as Falco 
*Donald Adams  ... as Shuffle / Rathe 
*Scott Heindl ... as Chase 
*Kevan Kase  ... as Officer Gene 
*Andrew Wheeler ... as Officer Davis 
*Blaine Anderson  ... as Officer Wild

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 


 