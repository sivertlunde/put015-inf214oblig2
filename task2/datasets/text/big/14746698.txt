Income Tax Sappy
{{Infobox Film |
  | name           = Income Tax Sappy
  | image          = Incometaxsappy54onesheet.jpg
  | caption        = 
  | director       = Jules White Felix Adler
  | starring       = Moe Howard Larry Fine Shemp Howard Benny Rubin Margie Liszt Nanette Bordeaux Vernon Dent Joe Palma
  | cinematography = Ray Cory  Edwin Bryant
  | producer       = Jules White
  | distributor    = Columbia Pictures
  | released       =  
  | runtime        = 16 28"
  | country        = United States
  | language       = English
}}
Income Tax Sappy is the 153rd short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The boys are set to do their income taxes, which are due the next day.  As they do, Moe mentions on how easy it is for people to "gip" on their tax returns and not get caught, so the three of them get the idea to create their own fake deductions to ensure a hefty refund, and throughout their endeavors, Larry and Shemp come up with an idea to come up with creative deductions for other peoples tax returns and charging them for it. Moe finds this idea grand, and the three of them become "tax experts."

Not long afterward, the Stooges are enjoying a life of luxury, having made lots of money from profits and their own tax returns.  On one particular day, they host a dinner party for one of their clients, a German man who obsessively strokes his beard, but as the meal commences, strange happenings and the boys own incompetence anger their guest. He then removes his beard (which Larry had cut off), exposes himself as a sleeper agent for the IRS and calls in his fellow agents to arrest the Stooges for tax fraud. With nothing left to lose, the boys make a mad dash away and attempt to fight off the agents, but end up being bested and sent away in handcuffs and were taken to Alcatraz.

==Production notes==
The title Income Tax Sappy is a homonym for "Income Tax Happy." Solomon, Jon. (2002) The Complete Three Stooges: The Official Filmography and Three Stooges Companion, p. 435; Comedy III Productions, Inc., ISBN 0-9711868-0-4  

This is one of only two shorts released in 1954 containing all new footage, the other being Shot in the Frontier. Shemp Howard did not slick back his long hair in either film. This was because he had begun dying his hair by this time, and initially could not use pomade. 

Income Tax Sappy features a recurring gag of "Man Vs. Soup," wherein one of the Stooges is about to eat a soup that, at first unbeknownst to them, contains a live crustacean that continually eats all the crackers the Stooge drops in it, and a battle between the two parties ensues. In the episode Dutiful But Dumb, Curly tries to defeat a stubborn oyster in his stew; In Shivering Sherlocks, Moe is having problems with clam chowder; in this episode, And Larry struggles against lobster gumbo. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 