The Great Waldo Pepper
{{Infobox film
| name = The Great Waldo Pepper
| image = Waldo Pepper.jpg
| caption = Theatrical release poster by Gary Meyer
| director = George Roy Hill
| producer = George Roy Hill
| writer = George Roy Hill William Goldman
| starring = Robert Redford Bo Svenson Margot Kidder Bo Brundin Susan Sarandon
| music = Henry Mancini Robert Surtees
| editing = William H. Reynolds
| distributor = Universal Pictures
| released =  
| runtime = 107 minutes
| country = United States English
| budget = $5 million
| gross = $20,642,922 
}}
 -1 biplane used in the movie.]]
 
 pilot who aviation regulations by the Air Commerce Act.

==Plot==
World War I veteran Waldo Pepper (fictitiously 1895-1931, played by Robert Redford) feels he has missed out on the glory of aerial combat after being made a flight instructor. After the war, Waldo had taken up barnstorming to make a living. He soon tangles with rival barnstormer (and fellow war veteran) Axel Olsson (Bo Svenson).

Antagonistic at first, Waldo and Axel become partners and try out various stunts. One of these stunts, a car-to-aircraft transfer, goes wrong and Waldo is nearly killed after Axel is unable to climb high enough to clear a barn, slamming Waldo into it.  Waldo then goes home to Kansas to be with on-again, off-again girlfriend Maude (Kidder) and her family. Maude, however, is not happy to see Waldo at first; because every time she does, Waldo is injured in some way. Eventually, however, they make up and become lovers once again. Meanwhile, Maudes brother Ezra (Ed Herrmann), a long-time friend of Waldos since boyhood, promises to build Waldo a high-performance monoplane as soon as he is well enough to fly it. Waldos goal is to become the first pilot in history to successfully perform an outside loop, and Ezra feels Waldo can do it with the monoplane.
 flying circus Air Commerce Geoffrey Lewis), a man from Waldos war past.

Soon after, at the Muncie Fair, another tragedy occurs with the Dillhoefer Circus when Ezra (flying in place of the grounded Waldo) attempts the outside loop in the monoplane.  He crashes on his third attempt, and the crowd rushes out of the stands to see the wreckage.  Some of the spectators are smoking as they watch Waldo struggle to free Ezra.  One of the cigarettes is flicked into gas leaking from the aircraft, igniting it and eventually burning Ezra to death. Because no one helped Waldo try to save him, Waldo goes on a rampage, jumps in one of Dillhoefers aircraft and begins buzzing the crowd away from the wreckage and ends up crashing into a carnival area, which leads to his permanent grounding.
 Hollywood where alias so Fokker Dr. I replica in the film. 

During filming of a famous wartime duel; Waldo in a Sopwith Camel and Kessler in the Fokker—although their aircraft are disarmed—begin dogfighting in deadly earnest, using their aircraft as weapons, repeatedly playing chicken and colliding with each other. Eventually, Waldo damages Kesslers aircraft so bad that its no longer airworthy, and Kessler surrenders to Waldo. Waldo and Kessler then salute each other and fly their separate ways. 

==Cast==
 
* Robert Redford as Waldo Pepper
* Bo Svenson as Axel Olsson
* Bo Brundin as Ernst Kessler
* Susan Sarandon as Mary Beth Geoffrey Lewis as Newt Potts
* Edward Herrmann as Ezra Stiles
* Philip Bruns as Dillhoefer
* Roderick Cook as Werfel
* Kelly Jean Peters as Patsy
* Margot Kidder as Maude Stiles
* Roderick Cook as Werfel
* Scott Newman as Duke
* James S. Appleby as Ace
* Patrick W. Henderson as Scooter
* James N. Harrell  as Farmer
* Elma Aicklen as Farmers Wife
* Deborah Knapp as Farmers Daughter
* John A. Zee as Director Western Set
* John C. Reilly as Western Star
* Jack Manning as Director Spanish Set
* Joe Billings as Policeman
* Robert W. Winn as Theater Manager
*  Lawrence P. Casey as German Star
* Greg Martin  as Assistant Director
 

==Production==
The Great Waldo Pepper was a "passion project" for director George Roy Hill, who was himself a pilot. He and William Goldman had what Goldman described as "a huge falling out" during the middle of Goldmans writing the screenplay. Nevertheless, they managed to complete the project. 

Frank Tallman flew the air sequences using actual aircraft – not models. Waldo flew a Standard J-1 biplane. A lot of the other aircraft in the film, including Axels and those of Dillhoefers, were Curtiss JN-4 biplanes. A number of de Havilland Tiger Moth biplanes, modified to look like Curtiss JN-4s, were used for the crash scenes.
 Le Mans-style endurance car race). Hill, who flew as a U.S. Marine Corps cargo pilot in World War II, made sure stars Bo Svenson and Robert Redford did each sequence with no parachutes or safety harnesses. He wanted them to feel what it was like to fly vintage aircraft. Fortunately, no one was hurt during the air scenes.

==Reception==
The Great Waldo Pepper opened to mixed to good reviews, with the biggest praise going to the films aerial sequences. Vincent Canby of The New York Times wrote, "The Great Waldo Pepper is a most appealing movie. Its moods dont quite mesh and its aerial sequences are so vivid— sometimes literally breathtaking— that they upstage the human drama, but the total effect is healthily romantic."  Leonard Maltin noted that the film disappointed at the box office, and, although compared to earlier efforts such as The Sting (1973), it was director George Roy Hills "more personal" account that "... wavers uncomfortably between slapstick and drama." 

The aerial sequences staged by Frank Tallman included the climactic fight between Waldo Pepper and Kessler. The scene featuring a replica Sopwith Camel and a replica Fokker Triplane, was loosely patterned after a real dogfight between German ace Werner Voss and a flight of aircraft led by British ace James McCudden.  

Due to the attention to period details and the use of actual aircraft in the flying scenes, The Great Waldo Pepper is well-regarded among aviation films, receiving a "four-star" rating by film and aviation historians Jack Hardwick and Ed Schnepf.  Released in a number of home media formats, there are no extra features in the latest DVD.
 silent screen Ormer "Lock" Locklear, a daring aviator, military veteran and budding film star, reputed to be the prototype for the character of Waldo Pepper. Locklear died when his aircraft crashed on August 2, 1920 during a nighttime film shoot for the Fox Studios feature, The Skywayman.   

==References==
Notes
 

Citations
 

Bibliography
 
* de Haro, Roberto. Twist of Fate: Love, Intrigue, and the Great War. Bloomington, Indiana: iUniverse, 2012. ISBN 978-1-46207-027-5.
* Farmer, James H. Celluloid Wings: The Impact of Movies on Aviation. Blue Ridge Summit, Pennsylvania: Tab Books Inc., 1984. ISBN 978-0-83062-374-7.
* Goldman, William. Which Lie Did I Tell?. London: Bloomsbury, 2000. ISBN 978-0-74755-317-5.
* Hardwick, Jack and Ed Schnepf. "A Viewers Guide to Aviation Movies". The Making of the Great Aviation Films, General Aviation Series, Volume 2, 1989.
* Maltin, Leonard. Leonard Maltins Movie Guide 2009. New York: New American Library, 2009 (originally published as TV Movies, then Leonard Maltin’s Movie & Video Guide), First edition 1969, published annually since 1988. ISBN 978-0-451-22468-2.
 

==External links==
 
*  
*  
*  
*  , rottentomatoes.com

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 