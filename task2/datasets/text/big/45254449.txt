Tangerine (film)
{{Infobox film
| name           = Tangerine
| image          = Tangerine (film) POSTER.jpg
| caption        = Theatrical release poster
| director       = Sean S. Baker
| writer         = {{plainlist|
* Sean S. Baker
* Chris Bergoch
}}
| producer       = {{plainlist|
* Karrie Cox
* Marcus Cox
* Darren Dean
* Shih-Ching Tsou
* Sean S. Baker
}} 
| starring       = {{plainlist|
* Kiki Kitana Rodriguez
* Mya Taylor
* Karren Karagulian
* Mickey OHagan
* James Ransone
}}
| editing        = Sean S. Baker
| cinematography = {{plainlist|
* Sean S. Baker
* Radium Cheung
}}
| studio         = {{plainlist|
* Duplass Brothers Productions
* Through Films
* Cre Film
}}
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
}}  independent comedy-drama film  directed by Sean S. Baker and written by Baker and Chris Bergoch. Baker was inspired to make the film by a donut shop near his home in Hollywood, CA. 

The film will be released in theaters on July 10, 2015 courtsey of Magnolia Pictures. 

==Plot==
On Christmas Eve, transgender prostitute Sin-Dee Rella, who has just finished a prison sentence, meets her friend Alexandra, who is also a transgender prostitute, at Donut Time. Alexandra informs Sin-Dee Rella that her boyfriend and pimp Chester has been cheating on her with a white cisgender woman. Outraged, Sin-Dee storms out of Donut Time to search the neighborhood for Chester and Dinah, the "fish" he has been sleeping with.

==Cast==
*Kitana Kiki Rodriguez as Sin-Dee
*Mya Taylor as Alexandra
*James Ransone as Chester
*Mickey OHagan as Dinah
*Karren Karagulian as Razmik 
*Alla Tumanian as Ashken 
*Luiza Nersisyan as Yeva

==Production==
Baker  and  Bergoch collaborated on the screenplay for Tangerine from September through December 2013.  Baker and Bergoch met transgender actresses Mya Taylor and Kitana Kiki Rodriguez, who had no major acting experience, at a Los Angeles LGBT Center in 2013.   The film was shot with three iPhone 5s phones by Baker and Radium Cheung.  It was executive produced by the Duplass Brothers, and produced by Through Films, Darren Dean, and Shih-Ching Tsou.  Tangerine began production on Christmas Eve 2013 and wrapped the following month. The film was shot entirely in Hollywood, CA.

==Release==
Tangerine made its world premiere January 23, 2015 at the Sundance Film Festival as part of their NEXT program.   Magnolia Pictures bought world rights to the film on January 27, 2015 and will release the film later in 2015.  The film will be released in a limited release in the United States on July 10, 2015. 
==Reception==
The Hollywood Reporter described the film as "a singularly delightful girlfriend movie with an attitude".  Indiewire gave the film an A- grade, describing it as "a breath of fresh air in an indie landscape that often tends to focus on #WhitePeopleProblems."  Variety s Justin Chang wrote that Tangerine is "an exuberantly raw and up-close portrait of one of Los Angeles more distinctive sex-trade subcultures." 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 