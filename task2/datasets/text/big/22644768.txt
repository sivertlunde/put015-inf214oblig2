De Kassière
{{Infobox film
| name           = De Kassière
| image          = De Kassière.jpg
| image_size     = 
| caption        = 
| director       = Ben Verbong
| producer       = Chris Brouwer,  Haig Balian
| writer         = Ben Verbong, Sytze van der Laan, Willem Jan Otten
| narrator       = 
| starring       = 
| music          = David A. Stewart
| cinematography = 
| editing        = 
| distributor    = Movies film production
| released       = 10 June 1989
| runtime        = 112 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
}}
 1989 Netherlands|Dutch Dave Stewart, was later released as a single--"Lily Was Here."

==Plot==
Teenager Lily works as a checkout girl at the local supermarket. She becomes pregnant, but before the child is born, the black father is attacked by guys and killed. Following his death, she flees to the city, where she soon finds herself under the wings of a pimp, Ted. Escaping Ted, she commences a one-woman spree of thefts, culminating in running from the police and the press. In the end, Lily is forced to choose between freedom and her baby.

==Cast==
* Marion van Thijn ...Lily
* Thom Hoffman ... Arend
* Coen van Vrijberghe de Coningh ... Ted
* Truus te Selle ... Lilys Mother
* Con Meyer ...Sjaak
* Monique van de Ven ...Midwife Conny
* Hans Kesting ... Piccolo
* Kees Hulst ... Emile

==Background==

* The film was largely recorded in Rotterdam.
* Marion van Thijn is the daughter of former mayor Ed van Thijn of Amsterdam.
* The music for the film was composed by David A. Stewart, former member of the band Eurythmics.
* The title song, "Lily Was Here," reached first place in the Dutch charts and was a hit internationally. Candy Dulfer played the major saxophone pieces in the song.

== External links ==
*  

 

 
 
 
 
 