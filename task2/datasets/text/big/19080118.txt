The Woman for Joe
{{Infobox film
| name = The Woman for Joe
| image = "The_Woman_for_Joe"_(1955).jpg
| caption = British poster
| director = George More OFerrall
| producer = Leslie Parkyn Neil Paterson
| starring = 
| music = Malcolm Arnold
| cinematography = Georges Périnal
| editing = Alfred Roome
| studio = Group Film Productions Limited
| distributor = J. Arthur Rank Film Distributors (UK)
| released = 25 August 1955	(London) (UK)
| runtime = 91 minutes
| country = UK English
| budget =
| gross = 
| preceded_by =
| followed_by =
}} 1955 British George Baker, Jimmy Karoubi and David Kossoff.  The owner of a circus sideshow and his prize attraction (a midget) become romantically involved with the same woman. The film was made at Pinewood Studios. 

==Plot==
Midget George Wilson pulls strings to obtain a job in the circus for Mary, a Hungarian lady hes fallen madly in love with. Mary is happy to have the job, singing to the lions, but although she likes George, her feelings for circus owner Joe Harrop are stronger. The jealousy and tensions caused affect the running of the circus.

==Cast==
* Diane Cilento as Mary   George Baker as Joe Harrop  
* Jimmy Karoubi as George Wilson  
* David Kossoff as Max  
* Violet Farebrother as Ma Gollatz   Earl Cameron as Lemmie  
* Sydney Tafler as Butch  
* Alf Dean as Vendini 
* Patrick Westwood as Freddie the Kid  
* Derek Sydney as Harry the Spice  
* Verna Gilmore as Princess Circassy   Martin Miller as Iggy Pulitzer  
* Meier Tzelniker as Sol Goldstein  
* Miriam Karlin as Gladys  
* Terence Longdon as Doctor at the Circus (uncredited) 
* Arthur Lowe as Georges Agent (uncredited)
* Philip Stainton as Sullivan

==Critical reception==
TV Guide wrote, "the highlight of this picture is the elaborate circus set, but this does little to benefit the unfolding of the plot, which is predictable" ;   while AllMovie noted "an excellent showcase for leading lady Diane Cilento (later better known as Mrs. Sean Connery)... What could have been an exercise in tawdriness is redeemed by the colorful camerawork of Georges Perinal."  

==External links==
* 

==References==
 

 
 
 
 
 
 
 


 