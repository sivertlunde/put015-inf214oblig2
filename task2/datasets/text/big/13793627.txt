Love (1919 film)
 
{{Infobox film
| name           = Love
| image          = Love 1919.jpg
| caption        = Theatrical poster for Love (1919) Fatty Arbuckle
| producer       = Joseph M. Schenck
| writer         = Fatty Arbuckle Vincent Bryan
| starring       = Fatty Arbuckle
| cinematography =
| editing        =
| studio         = Comique Film Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 23 minutes
| country        = United States Silent (English intertitles)
| budget         =
}}
 short comedy Fatty Arbuckle. Prints of the film survive in collections.   

==Plot==
As summarized in a magazine,  Fatty (Arbuckle) meets Winnie (Westover) after rescuing her father Frank (St. John) from a well at their farm and is smitten with her. Fatty is dismissed and leaves, however, as Frank wants Winnie to marry Al Clove (St. John). Fatty returns to the farm in the disguise of a hired girl so that he can be near his beloved, but finds he must fend off the flirtations of her father Frank. Winnies marriage is all arranged, but at the dress rehearsal the groom is missing, so the "hired girl" takes his place and goes through the practice ceremony, word for word, with the bride. When the wedding day arrives, the ceremony is broken up when Fatty and Winnie announce that they have already been married as the rehearsal was the real thing.

==Cast== Roscoe Fatty Arbuckle as Fatty Al St. John as Al Clove, Fattys rival
* Winifred Westover as Winnie Frank Hayes as Frank, Winnies father
* Monty Banks as Farmhand (as Mario Bianchi) Kate Price as the Cook

==See also==
* Fatty Arbuckle filmography

==References==
 

==External links==
 
* 

 
 
 
 
 
 
 
 
 

 