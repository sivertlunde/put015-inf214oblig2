Unforgettable (2014 film)
{{Infobox film
| name=Unforgettable
| image=
| caption=Theatrical release poster
| director=Arshad Yusuf Pathan
| producer=Arshad Shah Zarah Shah Arshad Yusuf Pathan
| writer=Arshad Yusuf Pathan
| starring=Mohammed Iqbal Khan Alka Verma Hazel Crowney
| music=Sujeet Shetty
| released= 
| country=India
| language=Hindi
}}
Unforgettable is a 2014 Bollywood drama film directed by Arshad Yusuf Pathan. The film stars Mohammed Iqbal Khan, Alka Verma and Hazel Crowney in lead roles. The film is based and shot in Dubai. It is a love story of Mohammed Iqbal Khan who is a car racer who loses his eyesight. Sujeet Shetty has scored the films music. The film was released in India on 13 June 2014 

==Cast==
*Mohammed Iqbal Khan 
*Alka Verma 
*Hazel Crowney 
*Kiran Kumar
*Shahbaz Khan
*Usha Bachani Niall OBrien
*William Porterfield

==Original soundtrack==

Unforgettables music is composed by Sujeet Shetty.
{{track listing
| headline = Unforgettable Soundtrack
| extra_column = Singer(s)
| collapsed = no
| title1 = Lafzo Mein Dhalke
| extra1 = Sujeet Shetty
| length1 = 03:02
| title2 = Mai Kahu Na Kahu
| extra2 = Sujeet Shetty, Khushboo Jain
| length2 = 03:12
| title3 = Lagan Agan Lagi Re
| extra3 = Sahid Malya
| length3 = 04:22
| title4 = Abhi Se Teri
| extra4 = Sachin Gupta
| length4 = 02:18
}}

==References==
 

==External links==
*  
* 
* 
* 

 
 