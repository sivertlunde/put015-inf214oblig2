October Baby
{{Infobox film
| name = October Baby
| image = Theatrical_release_poster_for_film_"October_Baby".jpg
| alt = 
| caption = Theatrical release poster
| director = Andrew Erwin Jon Erwin
| producer = Dan Atchison Justin Tolley Jon Erwin Andy Erwin
| screenplay = Cecil Stokes Jon Erwin
| based on =  John Schneider Jasmine Guy
| music = Paul Mills
| cinematography = Jon Erwin
| editing = Andrew Erwin Gravitas Provident Films
| distributor = Provident Films Samuel Goldwyn Films
| released =  
| runtime = 107 minutes
| country = United States
| language = 
| budget = $1,000,000 
| gross = $Lifetime Gross: $5,357,328   
}}
October Baby is a 2011 American Christian-themed dramatic film directed by Andrew and Jon Erwin and starring Rachel Hendrix in her film debut. It is the story of a woman named Hannah, who learns as a young adult that she survived a failed abortion attempt. She then embarks upon a road trip to understand the circumstances of her birth.   was inspired by a YouTube video chronicling the life experiences of abortion survivor Gianna Jessen. 

==Plot== John Schneider abort her.

Hannah experiences a range of emotions from confusion, to anger, to desperation and seeks out her best friend, Jason (Jason Burkey), for advice. After sorting through her feelings and her options with Jason, she decides to find her birth mother.  Jason invites her to go on a trip for spring break with a group of his friends who are going to New Orleans,  Louisiana.  Hannahs father, Jacob Lawson forbids her to go, because of her illness. He says he trusts her to make the right decision, regardless of his opinion. Hannah decides to go because she says she wants "answers to all these questions about who  ." She sets out on a journey that leads her to her birthplace, Mobile, Alabama. While staying at a hotel overnight, Jasons girlfriend Alanna (Colleen Trusler) says some hurtful things to Hannah, who eventually leaves when told that Jason didnt want her to come on the trip. Jason decides to help her find the hospital where she was born. Upon arriving, they find it has been vacant for years and is locked up. Hannah pries the back door open, and she and Jason are arrested. The sheriff, however, allows them to go when she tells him her reason for trying to get in. He gives her the name and address of the nurse who signed her birth certificate. She locates the nurse (Jasmine Guy) who assisted in the abortion, and they have an emotional encounter while the nurse describes the circumstances behind not only her birth, but that of her twin brother, whom she knew nothing about. She leaves the nurses apartment knowing not only the new (changed) name of her birth mother but where she works.

When she finally meets her biological mother (Shari Rigby), she is overwhelmed by anger and hatred because of her mothers rejection. Just then, her father arrives to take her home, having found out she lied to him about being with others beside Jason, and also about checking in with her doctor while traveling. He also discovers their arrest for breaking and entering. He tells Jason he is not permitted to visit, call, or interact with Hannah because he had lied about what they were doing. Jason returns to the hotel where the rest of the college friends are staying. He breaks up with his girlfriend, Alanna, after she apologizes for her cruel actions, and returns home. He phones Hannahs father and apologizes for lying.

Hannahs adopted parents also go through their own pain and suffering, deciding to tell Hannah the details of their choice to adopt both her and her brother, who died months later. Her mother relates to her that she had been pregnant with twins and lost them at 24 weeks. Theyd then seen an adoption request for Hannah and her brother at a pregnancy crisis center where she had volunteered.

Hannah wanders aimlessly until she sees a   and finds she is able to forgive her biological mother and forget about the botched abortion.

Jason takes Hannah back to the theater where shed collapsed and says they should finish the play together. Hannah begins to realize he wants to be more than a friend and starts to fall in love. The movie ends with them leaving to go to their college dorms. Hannah hugs both her parents and thanks them for wanting her when no one else did. She is last seen smiling proudly at her father and holding Jasons hand.

==Cast==
*Rachel Hendrix as Hannah
*Jason Burkey as Jason John Schneider as Jacob
*Jasmine Guy as Nurse Mary
*Robert Amaya as Beach Cop
*Maria Atchison as Secretary Pat
*Joy Brunson as Danielle Rodney Clark as Priest
*Brian Gall as Rent-a-Cop
*Carl Maguire as Lance
*Tracy Miller as Officer Mitchell
*Lance E. Nichols as Doctor
*Jennifer Price as Grace
*Shari Rigby as Cynthia (as Shari Wiedmann)
*Don Sandley as Psychiatrist
*Chris Sligh as B-Mac
*Austin Johnson as Trueman
*Colleen Trusler as Alanna

==Production and release==
The film was inspired by a YouTube video chronicling the experiences of Gianna Jessen.  At a preview of the film at the Heritage Foundation, director Jon Erwin stated that prior to making the film he "never knew there was such a thing as an abortion survivor." He decided to make the film after Christian filmmaker Alex Kendrick challenged him: "What is your purpose?"    
 Measure 25, The Hunger Games was expected to release at the same time on ten times as many screens. When asked how the film would fare against this daunting competition, Rachel Hendrix said, "there will be all these teenage girls waiting in line to see Hunger Games, and theyll see the poster for October Baby, and theyll want to go see our movie, too."   
 Eric Wilson wrote a novel based on the film which was released September 2012. 

==Reception==
The film received poor reviews from critics, scoring a 22% favorable rating on   described the film as a "virulent pro-life tract" and "revenge fantasy" that, rather than being a film, is "propaganda for the already converted." She particularly criticized the films medical claims, saying that it depicted late-term abortion and poor conditions in abortion clinics as the norm.  

The films pro-life message  was received well by pro-life movement|pro-life organizations. Endorsements include Ron Anger, Alex Kendrick, Stephen Kendrick, Dennis Rainey, Richard Land, Ted Baehr and Charmaine Yoest.  Joni Hannihan of the Florida Baptist Witness wrote, "the movie sends strong messages about the beauty of life, the importance of each life—but it’s not preachy" and found the film "young" and "refreshing."  World Magazine|World magazine observed that the film is "polished" and "a more-than-worthy viewing experience." However, the romance between Hannah and Jason was found to be lacking in depth and one of the characters on the road trip unbelievable. But the highlight of the film was portraying "how liberating and joyous forgiveness is—both giving it and receiving it—without putting implausible, sermonizing dialogue into their characters mouths." 

The October Baby filmmakers believe the gulf between the reviewer and the ticket buyer scores dramatizes a rift between critics and conservative moviegoers. "What it tells me is that there’s a gap in values", Jon Erwin said. "Theres a large group of people who don’t see their values reflected in most movies."  In selecting the film for its "Worst of films of 2012" list, staff of the The A.V. Club explained that "what makes the film so insidious and upsetting is the way in which it’s understood that the reluctant birth mother deserves what she gets" and "It’s a cinematic encapsulation of a worldview in which a woman’s rights are widely understood to be secondary to those of her offspring, both in the womb and years later."  , The A.V. Club, December 20, 2012, accessed December 20, 2012. 

==Awards==
The film won the Grand Jury Prize as the Best Fiction Feature at the 2011 Red Rock Film Festival. At the festival Rachel Hendrix won the Special Achievement Award for Acting. 

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 
 
 