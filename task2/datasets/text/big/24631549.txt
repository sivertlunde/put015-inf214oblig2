Crossing Hennessy
 
 
{{Infobox film
| name               = Crossing Hennessy
| image              = Crossing hennessy.jpg
| film name = {{Film name| simplified         = 月满轩尼诗
| traditional        = 月滿軒尼詩
| pinyin             = yuè mǎn xuānníshī}}
| caption            = 
| director           = Ivy Ho
| producer           = William Kong Nansun Shi
| writer             = Ivy Ho
| starring           = Jacky Cheung Tang Wei Danny Lee Sau-Yin Paw Hee-Ching Maggie Cheung Ho-yee Andy On
| music              = 
| cinematography     = 
| editing            = 
| studio             = 
| distributor        =
| released           =  
| runtime            = 
| country            = Hong Kong
| language           = Cantonese
| gross              = $439,766
}}
Crossing Hennessy ( ) is a 2010 Hong Kong film directed by Ivy Ho. It was filmed from March to May 2009, and stars Jacky Cheung and Tang Wei. It is a remake of Crossing Delancey (1988).

It is a return to film both for Cheung, who has not appeared in a film for several years, and for Tang, who was banned from acting in mainland China after appearing in Ang Lees 2007 Lust, Caution (film)|Lust, Caution. It is the second film directed by Ho (who began her career as a screenwriter). Other actors in the film include Danny Lee Sau-Yin, Paw Hee-Ching, Maggie Cheung Ho-yee, and Andy On.

The story mainly takes place around Hennessy Road, in Hong Kong Islands Causeway Bay and Wan Chai districts.

In May 2009 it was announced that the film would be shown at the Cannes Film Festival.  It will also be shown at the 2010 Hong Kong International Film Festival alongside Clara Laws "Like a Dream" as a kick start for the opening event. 

==Plot==
Loy (Jacky Cheung) is a 41-year-old bachelor who lives with his widowed mother (Paw Hee-Ching) and aunt (Chu Mi-Mi) in Wan Chai. Loys mother, disappointed by his bachelorhood, often sets up match-making lunches for him with various girls. On the other side of Hennessy, orphaned Oi Lin (Tang Wei) lives with and works for her uncle and aunt in a bathroom appliances store on Lockhart Road. She is in a relationship with Xu (Andy On), who is in prison. Her uncle, who disapproves of Xu, sets up a match-making lunch for her and Loy. Neither Loy or Oi Lin are attracted to one another, but the families continue to set up meetings for the two. After a few meetings and dates, the two eventually strike up a friendship after discovering a mutual interest in detective stories.

Both Loy and Oi Lins families misunderstand the newfound friendship as romance and begin discussing wedding banquet arrangements. Loy explains to his mother this is not the case, and that Oi Lin has a boyfriend. Annoyed, Loys mother calls Oi Lins aunt and yells at her, calling her and her husband cheats. Oi Lin becomes upset with Loy, and breaks off their friendship. During this time she shares a flat with her boyfriend Xu, who has just been released from prison, and Loy gets back together with his ex-girlfriend Man Yu (Maggie Cheung Ho-yee).

As time passes, Loy and Oi Lin grow increasingly uneasy about their relationships with Man Yu and Xu respectively. Loy tells Man Yu that he is finally in love with someone, but does not know if he is good enough for her. In the meantime, Oi Lin breaks up with Xu. Furious, Xu tracks down Loy and beats him up. Loy, though severely injured and no match for Xu, claims he is the better man because he can make Oi Lin smile. Xu finally understands, and leaves Oi Lin to get on with her life.

Loys mother finally marries her accountant at the Cotton Tree Drive Marriage Registry to everyones delight. After the wedding, Loy goes to the Cha Chaan Teng (where hed previously had a date with Oi Lin) and finds Oi Lin. He joins her there, and the two enjoy the afternoon together.

==Reception==
Critics gave generally positive reviews, and Tang Wei in particularly received praise for her work. Perry Lam of Muse (Hong Kong magazine)|Muse Magazine, for example, comments that Tang exudes charisma and star appeal, even in jeans and sneakers, but I have no doubt that she could have stretched her role a lot further if the script had allowed her. 

==Awards and nominations==
47th Golden Horse Awards
* Nominated: Best Actress (Tang Wei)
17th Hong Kong Film Critics Society Awards
*Won: Film of Merit
*Won: Best Screenplay (Ivy Ho)
* Nominated: Best Film
* Nominated: Best Actress (Tang Wei)
30th Hong Kong Film Awards
*Nominated: Best Screenplay (Ivy Ho)
*Nominated: Best Actor (Jacky Cheung)
*Nominated: Best Actress (Tang Wei)
*Nominated: Best Supporting Actress (Mimi Chu)
*Nominated: Best Supporting Actress (Paw Hee-ching)
*Nominated: Best New Director (Ivy Ho)

==References==
 

==External links==
* 

 
 
 