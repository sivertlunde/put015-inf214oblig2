Dedication of the Great Buddha
 
{{Infobox film
| name           = Dedication of the Great Buddha
| image          = 
| caption        = 
| director       = Teinosuke Kinugasa
| producer       = 
| writer         = Teinosuke Kinugasa Ryuichiro Yagi
| starring       = Shinobu Araki
| music          = 
| cinematography = Kôhei Sugiyama
| editing        = 
| distributor    = 
| released       =  
| runtime        = 129 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}

  is a 1952 Japanese film directed by Teinosuke Kinugasa. It was entered into the 1953 Cannes Film Festival.   

==Cast==
* Shinobu Araki - Ryōben
* Kōtarō Bandō
* Kazuo Hasegawa - Kunihito Tateto
* Sumiko Hidaka - Morime Ōmiya
* Tatsuya Ishiguro
* Ryōsuke Kagawa
* Toshiaki Konoe
* Kanji Koshiba
* Yataro Kurokawa - Nakamaro Fujiwara
* Machiko Kyō - Mayame
* Mitsuko Mito - Sakuyako Tachibana
* Shozo Nanbu
* Shintarō Nanjō
* Joji Oka - Naramaro Tachibana
* Denjirō Ōkōchi - Gyōki
* Sakae Ozawa - Kimimaro Kuninaka
* Mitsusaburō Ramon - Sakamaro
* Taiji Tonoyama
* Kenjiro Uemura - Shōnan Shinjō

==See also==
*Depictions of Gautama Buddha in film

==References==
 

==External links==
* 

 
 

 
 
 
 
 


 