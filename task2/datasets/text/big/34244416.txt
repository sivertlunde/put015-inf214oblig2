Father of a Soldier
{{Infobox film
| name           = Father of a Soldier
| image          = 
| caption        = 
| director       = Rezo Chkheidze
| producer       = 
| writer         = Suliko Jgenti
| starring       = Sergo Zaqariadze
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 83 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}} Georgian black-and-white World War II-themed drama film directed by Revaz Chkheidze based on a script by Suliko Jghenti. The leading role was played by Sergo Zakariadze. The film was entered into the 4th Moscow International Film Festival.   

==Cast==
* Sergo Zaqariadze as Giorgi Makharashvili
* Vladimir Privaltsev as Nikiforov (as V. Privaltsev)
* Aleksandr Nazarov as Arkadi (as A. Nazarov)
* Aleksandr Lebedev (as A. Lebedev)
* Yuri Drozdov as Vova (as Yu. Drozdov)
* Vladimir Kolokoltsev as Grisha (as V. Kolokoltsev)
* Viktor Uralsky as Pachua (as V. Uralsky)
* Qetevan Bochorishvili as Tamari, wife of Giorgi (as K. Bochorishvili)
* Vladimir Pitsek (as V. Pitsek)
* Pyotr Lyubeshkin (as P. Lyubeshkin)
* Ivan Kosykh as Akhmed (as I. Kosykh)

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 

 
 