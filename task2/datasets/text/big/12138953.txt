Mulberry Street (film)
{{Infobox film
| name           = Mulberry Street
| image          = Mulberry street 07.jpg
| caption        = Promotional film poster
| director       = Jim Mickle
| producer       = Victor Assante Rene Bastian Adam Folk Tim House Linda Moran
| writer         = Nick Damici Jim Mickle
| starring       = Nick Damici Kim Blair Ron Brice Bo Corre Tim House Larry Fleishman Larry Medich Javier Picayo Antone Pagan Lou Torres John Hoyt 
| music          = Andreas Kapsalis
| cinematography = Ryan Samul
| editing        = Jim Mickle
| distributor    = After Dark Films 
| released       =   }}
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = United States|$60,000   
| gross          = 
}}
Mulberry Street is a 2006 American horror film directed by Jim Mickle, written by Nick Damici and Jim Mickle, and starring Nick Damici. It was released by After Dark Films as a part of their 8 Films to Die For 2007.

== Plot == Mulberry Street in downtown Manhattan, causing humans to devolve into blood-thirsty monstrosities. Six recently evicted tenants must survive the night and protect their downtown apartment building as the city quickly spirals out of control.

Initially emergency services and city authorities attempt to contain the spread by shutting down public transportation, and closing roads, but soon hospitals are inundated with the wounded, and the virus begins to spread island wide. By the time the characters realise the severity of the situation, the infected have overrun much of the city and the streets are highly dangerous, with police seemingly overwhelmed and unable to respond. The survivors barricade themselves in their apartments as the news of the outbreak and subsequent quarantine of Manhattan breaks on TV and radio, waiting on promised rescue from the military, which the government promises will begin to restore order in Manhattan soon.

== Cast ==
* Nick Damici as Clutch
* Kim Blair as Casey
* Ron Brice as Coco
* Bo Corre as Kay
* Tim House as Ross
* Larry Fleishman as Charlie
* Larry Medich as Frank
* Javier Picayo as Otto
* Antone Pagan as Peter Pace
* John Hoyt as Big Vic
* Lou Torres as Larry the Bartender
* Larry Fessenden as Man behind the gate

== Production ==
Jim Mickle and Nick Damici met while working on a student thesis film.  They came up with the idea of a back-to-basics zombie film called Dead of Night, but the estimated budget was too high for them.  Tim House, who played the super, offered to put up $10,000, and the concept and style were reworked to fit around that budget.  Eventually, the film grew from there and morphed into something less traditional.  Many of the characters were based on people they knew from the real apartment building, and the actors were made up of friends and family.  The film was shot in three and a half weeks. 

== Release ==
Damici and Mickle had to fight to keep the title, as their representatives wanted to title it something more evocative of the current horror boom.  Damici and Mikle, however, wanted to emphasize the theme of gentrification. 

Mulberry Street premiered at the Stockholm International Film Festival on November 16, 2006.   It got a theatrical release through After Dark Films on November 9, 2007.   It was included in the After Dark Horrorfest 2007 DVD box set, released March 18, 2008. 

== Reception ==
Rotten Tomatoes, a review aggregator, reports that 70% of ten surveyed critics gave the film a positive review; the average rating was 6.9/10.   Jay Weissberg of Variety (magazine)|Variety called it "a cut above most zero-budget horrors" that may become a cult film.   Bloody Disgusting rated it 2/5 stars and wrote, "The concept is solid but the execution is rife with problems."   Joshua Siebalt of Dread Central rated it 4/5 stars and recommended it to people who want "a tense and terrifying claustrophobic heart attack".   Jeremy Knox of Film Threat rated it 4.5/5 stars and wrote, "Off the top of my head, I can’t really think of another recent example of filmmaking in its purest form that tops Mulberry Street."   Scott Collura of IGN wrote that film rises above its limitations by its expertly realized characters and subtle themes.    Steve Biodrowski of Cinefantastique called it "half-brilliant and half-okay", criticizing the films lighting and cinematography.   Ian Jane of DVD Talk rated it 3.5/5 stars and wrote, "Mulberry St. was a surprisingly engaging horror film. It moves at a great pace, it features some very strong performances, and while it might borrow from a few other films a little too much, at least it does so well."   Paul Pritchard of DVD Verdict called it "a surprisingly effective little shocker that, though far from perfect, is never dull and well worth 84 minutes of anybodys time." 

== References ==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 