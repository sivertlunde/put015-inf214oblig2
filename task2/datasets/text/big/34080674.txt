The Big Fix (1947 film)
 
{{Infobox film
| name           = The Big Fix
| image          =
| caption        =
| director       = James Flood
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = Producers Releasing Corporation
| released       =   
| runtime        = 63 minutes
| country        = United States English
| budget         =
}}
 James Brown and Regis Toomey.

The story concerns the efforts of a crooked gambling ring to fix college basketball games.  This film was the final production of Producers Releasing Corporation.

== Plot ==
After retiring from the service, Ken Williams goes back to his old school, Norton University, where he used to be an appreciated student with an expected future as a basketball player. Together with fellow veteran Andy Rawlins, Ken registers to continue and complete his studies. They are welcomed back by the student liaison for veterans, Ann Taylor, and Andy is a bit surprised by the warm welcome, since he is in the dark about Kens previous accolades. Ken also refuses to join the basketball team, which confuses Andy even more.

Soon Ken is romantically involved with Ann, but he cant be persuaded even by her to reveal his reasons for not participating in the university basketball. He claims he has to focus on his engineering studies, and this disappoints coach Ambrose, who is eager to have him back on the team.

One night a woman turns up at the house where Ken resides, and demand to see him. Ken interrupts his date with Ann to talk to the woman, who turn out to be his kid sister Lillian. She is involved with gangster Del Cassini, who is responsible for making Ken fix basketball games before the war. That is the reason why he quite the basketball team in the first place and also interrupted his education.

Lillian convinces Ken that she is no longer involved with Del Cassini, and that Ken can safely return to the team. It turns out she is lying, and after her meeting with Ken she goes dorwctly to Del Cassini to inform him that their plan worked out fine.

Unaware of this, Ken rejoins the basketball team and is very happy with his decision. Soon one of Del cassinis goons approach him and tells him to lose their opening game of the season. Ken is furious and hits the man, but is threatened that Lillian will go to jail if he doesnt comply. Apparently she was involved in a hit-and-run car accident a while ago, and got saved by Del Cassini feom the law. Ken still refuses to cooperate.

Ken doesnt know that the real leader of Del Cassinis operation is police lieutenant Brenner. When Brenner hears that Ken refuses to cooperate, heinstead tricks Ken into participating in an undercover operation to uncover the organized crime in the area, focusing on illegal gambling in college sports.

Brenner asks Ken to pretend to cooperate with Del Cassini to get inside the gangster ring and find evidence. Ken agrees to this and becomes Del Cassinis marionette. He tells Ann about the operation, and seeing how unhappy Ken is with the current situation, she decides to help him out as best she can.

Ann finds out that there was no accident where Lillian was involved. She meets with Lillian in a club, and Lillian confesses everything was made up to get Ken to fix the games. Ann in turn tells Ken what she has found out, and Ken quits his undercover duty with immediate effect.

Brenner has put a lot of money on Norton losing their next game and is quite worried with Kens decision. He also punishes Lillian for squealing, and shoots her. The he goes on to capture Ken. Lillian dies from her wounds, but before she does she manages to reveal to Andy that Brenner is the real crook and runs the crime syndicate.

Andy gathers the basketball team and brings them to Kens rescue. Ken is held hostage at a hotel. When Del Cassini arrives at the hotel and finds out Brenner has killed Lillian, he is furious and shoots Brenner as revenge. The basketball team then runs into the hotel and rescues Ken. The others explain to Ken that it was Lillian who saved him in the end. Ken plays with the Norton team and they win the championship tournament. After the win, he and Ann are married. 

== Cast ==
 James Brown as Ken Williams
* Sheila Ryan as Lillian
* Noreen Nash as Ann Taylor
* Regis Toomey as Lieutenant Brenner
* Tommy Noonan as Andy Rawlins John Shelton as Del Cassini
* Charles McGraw as Armiston
* Grantland Rice as himself

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 