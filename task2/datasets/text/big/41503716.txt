Jump (2012 film)
{{Infobox film
| name           = Jump
| image          = Jump film.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Kieron J. Walsh
| producer       = Brendan J. Byrne Katie Holly
| writer         = 
| screenplay     = Steve Brookes Kieron J. Walsh
| story          = 
| based on       =  
| narrator       =  Martin McCann Charlene McKenna Ciarán McMenamin Valene Kane
| music          = 
| cinematography = David Rom
| editing        = Emer Reynolds
| studio         = Blinder Films Cyprus Avenue Films
| distributor    = Breaking Glass Pictures (USA) 
| released       =  
| runtime        = 81 minutes
| country        = Northern Ireland 
| language       = English
| budget         = £1,300,000 (estimated)
| gross          = 
}} mystery drama stage play of the same name by Lisa McGee.

==Plot==
 
The plot revolves around Greta Feeney (Burley), who intends to commit suicide on New Years Eve, and the interplay of her friends, and her gangster father. The story is framed by a voice-over narrative by Greta.

==Cast==
* Nichola Burley as Greta Feeney Martin McCann as Pearse Kelly
* Richard Dormer as Johnny Moyes
* Ciarán McMenamin as Ross
* Charlene McKenna as Marie
* Valene Kane as Dara
* Lalor Roddy as Frank Feeney
* Packy Lee as Jack
* Kelly Gough as Lucy
* Jonathan Harden as Richie

==Awards==
• Best Feature at Irish Film New York 2012
• Bridging the Borders Award at the Palm Springs International Film Festival 2013 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 


 
 