Indian (1996 film)
 
 

{{Infobox film
| name = Indian
| image = Indian (1996 film) poster.jpg
| caption = Poster Shankar
| Sujatha  (Dialogue)  Shankar
| Shankar
| Sukanya
| producer = A. M. Rathnam Sri Surya Movies
| music = A. R. Rahman Jeeva
| editing = B. Lenin V. T. Vijayan
| distributor = Sri Surya Movies
| released =  
| runtime = 185 minutes
| language = Tamil
| country = India
| budget =  
| gross =   
}}
 Shankar and Sukanya and Goundamani appearing in other pivotal roles. The films score and soundtrack are composed by A. R. Rahman, while cinematography was handled by Jeeva (director)|Jeeva. The film focuses on an ex-freedom fighter turned vigilante bent on rooting out corruption and his son, who is at the other end of the spectrum by facilitating corrupt practices leading to unfortunate events..
 India as Best Foreign National Film Filmfare Awards Telugu as Bharatheeyudu. It was also the highest grossing Tamil film upon its release, until surpassed by Padayappa, three years later.

==Plot== CBI officer is hell bent on tracking down Indian.

Chandra Bose alias Chandru (Kamal Haasan),(whom we later on find out to be Senapathys son) is a small-time bribe-broker stationed outside a Regional Transport Office, who aids people in greasing the right officials inside the RTO for getting permits and licenses. His assistant Subbiah (Goundamani) and Paneerselvam (Senthil), a RTO official, are engaged regular tiffs while Aishwarya (Manisha Koirala), Chandrus love interest and an avid animal rights activist also battle it out with Sapna (Urmila Matondkar), the daughter of a RTO official Chandru is trying to hobnob to secure a job for himself at the RTO. Aishwarya is irked by the fact that Sapna (as well her mom) is exploiting Chandrus situation and gets him to do grocery shopping and even the laundry.
 British officials and the Indian Freedome Movement Rebellions against the British, atrocities etc. are shown, culminating in Senapathy marrying Amirthavalli but leaving for Singapore to join Subhas Chandra Bose to be part of the INA. He returns after independence. Krishnaswamy tries to arrest Senapathy, but his attempt is foiled, and Senapathy escapes with his expertise in the art of hitting marmam or pressure points.

Senapathy then goes on to commit a murder in front of television audiences by killing a corrupt doctor(Nizhalgal Ravi) who refused to attend immediately to Senapathys daughter (Kasthuri (actress)|Kasthuri), who was suffering from third degree burns because he insisted on a bribe, which Senapathy refused. Chandru parts ways with his father because of his excessive insistence on honesty etc. and considers these values to be dead and worthless. How Senapathi evades the police and escapes arrest forms a major portion of the remaining part of the story. Public support is very high for the Indian because he exposes so many corrupt people. Senapathy does not do his son any favours either. Chandru had earlier taken a bribe and given a safety certificate to a bus with faulty brakes. The bus has an accident, killing forty school children it was carrying and thus Chandru is held responsible. Senapathy is bent on giving Chandru the same punishment as he gives others, i.e.death. After a few chase sequences, in the climax sequence in an airport Senapathy kills Chandru and apparently dies in an explosion. Krishnaswamy discovers through a video that Senapathi escaped moments before the jeep is exploded killing his son in the explosion.

The epilogue shows Senapathy calling Krishnaswamy from a foreign land (Hong Kong), indicating that he will be back should the need for him arise.

==Cast==
* Kamal Haasan as Senapathy and Chandra Bose
* Manisha Koirala as Aishwarya (voice dubbed by Rohini (actress)|Rohini)
* Urmila Matondkar as Sapna (voice dubbed by Bhanupriya) Suganya as Amrithavalli
* Goundamani as Subbaiya Senthil as Panneerselvam
* Nedumudi Venu as Krishnaswamy (voice dubbed by Nassar) Kasturi as Kasthuri
* Crazy Mohan as Parthasarathy
* Omakuchi Narasimhan as Lorry driver Manorama
* Aruna Irani (Hindi version only)
* Nizhalgal Ravi
* Bala Singh Ponnambalam

==Production==
  Raadhika to Urvashi subsequently replaced her, only for Shankar to throw her out for missing a days schedule to attend her sisters wedding. The role was finally handed to Sukanya, who had previously appeared alongside Kamal Haasan in Mahanadhi.  Hindi actress Urmila Matondkar was signed on to play a role in the film after the producers were impressed with her performance and the success of her 1995 Hindi film, Rangeela (film)|Rangeela. The producers roped in Hollywood make-up artistes Michael Westmore and Michael Jones to work on the designs for the senior Kamal Haasans and Sukanyas look in the film.   
 Las Vegas Jeeva and music director A. R. Rahman to location hunt and to compose tunes.  The films unit were given strict order to maintain privacy, with Hindi actor Jackie Shroff being notably turned away from visiting the shooting spot. A song for the film was shot at Prasad Studios featuring Kamal Haasan and Urmila Matondkar alongside 70 Bombay models.  This led to a protest from the Cine Dancers Union who argued that Tamil dancers should have been utilised instead, with Shankar opting to pay them off to avoid further hassle. Another duet between Kamal Haasan and Manisha Koirala was shot near the Sydney Opera House in Sydney and Canberra for fifteen days.  A flashback song was canned with four hundred dancers and a thousand extras at Gingee with Kamal Haasan and Sukanya, while another song featured shooting in Jodhpur, Rajasthan.   Graphic designer Venky noted that Indian was his most difficult project to date (in 1997) with a scene constructed to feature Kamal Haasans character alongside freedom fighter, Subhas Chandra Bose. Venky had to remove blemishes on the film reel of Bose provided by the Film Divisions archive before merging Kamal Haasan on to the shot to make it appear that the pair were marching in tandem. 

==Release==
The film opened in May 1996 to predominantly positive reviews from critics and went on to become a blockbuster at the South Indian box office.  The film ran packed houses for several months in Tamil Nadu and was dubbed and released in Telugu as Bharatheeyudu. Prior to the release of the film, the team also planned a Hindi version of the film, with a few re-shot scenes including Aruna Irani in place of Manorama. The Hindi version also fared well after its release in June 1996.
 National Film Best Actor Best Art Best Special Indian submission for the Academy Award for Best Foreign Language Film in 1997, though eventually did not make the short list.

In 2011, producer A. M. Rathnam discussed the idea of a sequel to this project as anti-corruption leaders like Anna Hazare were becoming active.  Shankars press relations team dismissed reports of a sequel in 2013, after news emerged that he was considering a follow up featuring Kamal Haasan and Ajith Kumar. 

===Awards=== National Film Awards (India) Silver Lotus Award for Best Actor – Kamal Haasan Silver Lotus Award for Best Art Direction – Thotta Tharani Silver Lotus Award for Best Special Effects – Venky

;1997 Filmfare Awards South (India) Best Actor Award (Tamil) – Kamal Haasan
* Won – Filmfare Award for Best Film – Tamil – A. M. Rathnam

;Tamil Nadu State Film Awards Tamil Nadu State Film Award for Best Film (First prize) – A. M. Rathnam
* Won – Tamil Nadu State Film Award for Best Actor – Kamal Haasan

;1997 Academy Awards (United States) Indian submission for the Academy Award for Best Foreign Language Film

==Soundtrack==
 
{{Infobox album |  
 Name = Indian
| Type = soundtrack
| Artist = A. R. Rahman
| Cover =
| Released = 1996
| Recorded = Panchathan Record Inn Feature film soundtrack
| Length = 30:05 Pyramid
| Producer = A. R. Rahman
| Reviews = Love Birds  (1996)
| This album = Indian (1996)
| Next album = Kadhal Desam   (1996)
}}

The soundtrack album includes five tracks composed by A. R. Rahman  and was released on 1996 by Pyramid Saimira|Pyramid.    Hindi as TIPS  Telugu as Vaali and Vairamuthu for the original version, P. K. Mishra for Hindustani and Bhuvanachandra for Bharateeyudu.

The soundtrack was also magnificent in sales and sold about 600,000 records within days of release. 

{{tracklist
| headline = Track listing
| extra_column = Singer(s)
| all_music =
| title1 = Akadanu Naanga
| extra1 = Swarnalatha
| length1 = 5:52 Vaali
| title2 = Maya Machindra
| extra2 = S. P. Balasubramaniam, Swarnalatha
| length2 = 5:37 Vaali

| title3 = Pachai Kiligal
| extra3 = K. J. Yesudas
| length3 = 5:50
| note3 = Lyrics: Vairamuthu
| title4 = Telephone Manipol
| extra4 =  Hariharan (singer)|Hariharan, Harini
| length4 = 6:15
| note4 = Lyrics: Vairamuthu

| title5 = Kappaleri Poyaachu
| extra5 = S. P. Balasubramaniam, P. Susheela
| length5 = 6:28 Vaali

}}

==References==
 

==External links==
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 