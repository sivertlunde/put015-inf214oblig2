301, 302
{{Infobox film 
| name         = 301, 302 
| image        = Korean film-301.302-01.jpg
| caption      = Theatrical poster 
| film name      = {{Film name
 | hangul       =  ,  
 | hanja        =  ,  
 | rr           = Samgongil, Samgonge
 | mr           = Samgongil, Samgongi}}
| writer       = Lee Seo-gun 
| starring     = Bang Eun-jin Hwang Shin-hye 
| director     = Park Chul-soo 
| producer     =
| music        = 
| editing      = Park Gok-ji
| distributor  = 
| released     =  
| runtime      = 100 minutes
| country      = South Korea
| language     = Korean
| budget       = 
}}
301, 302 or 301/302 is a 1995 South Korean film directed by Park Chul-soo.  It tells the story of two South Korean women, neighbors in the same apartment building, who take very different approaches to the difficulties of modern life; one indulges in food, sex, and spending while the other lives in self-imposed austerity. 

Compulsion (2013 film)|Compulsion, a 2013 Canadian psychological thriller directed by Egidio Coccimiglio, is based on this movie.  

==Plot==
The film details the relationship between two neighbours, Song-Hee a chef, and Yun-Hee an anorexic writer. After her divorce, Song-Hee moves in next door to Yun-Hee. Realising she is an anorexic, Song-Hee begins to torment Yun-Hee by offering her food, eventually forcing it upon her violently. 

Both women’s pasts are explored: Yun-Hee’s sexual abuse at the hands of her step-father, butcher by trade; Song-Hee’s dysfunctional marriage, her obesity, and finally the moment she cooks her husband’s dog and feeds it to him as an act of revenge. 

The film ends as Yun-Hee offers her own body to be cooked and eaten. After her death and consumption, Yun-Hee is shown living happily in Song-Hee’s apartment as a spirit. The film is told in flashback, as Song-Hee is interviewed by a detective who is investigating Yun-Hee’s disappearance.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 


 
 