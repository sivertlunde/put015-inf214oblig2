Mozhi (film)
 
{{Infobox film
| name           = Mozhi
| image          = Mozhi.jpg
| caption        =
| director       = Radha Mohan
| producer       = Prakash Raj
| eproducer      = Duet Movies
| aproducer      =
| writer         = Radha Mohan Prithviraj Prakash Raj Swarnamalya Vidyasagar
| cinematography = K. V. Guhan
| editing        = Mu. Kasivishwanathan
| distributor    = Oscar Films
| released       = 23 February 2007
| runtime        = 155 minutes
| country        = India
| awards         =
| language       = Tamil
| budget         = 2.5 crores
| gross          = 10 crores
}}
 Prithviraj in lead roles while Prakash Raj and Swarnamalya portray supporting roles. The films plot is about Karthik (Prithviraj) who works as a musician in films falls in love with a deaf and dumb girl Archana (Jyothika) but she considers him as his friend and disapproves his love. Rest of the story revolves around Archana accepting Karthiks love.
 Vidyasagar scored the music while lyrics were written by Vairamuthu. Mu. Kasivishwanathan handled the editing and K. V. Guhan worked as the cinematographer. The dialogues were written by Viji. The film was released on 23 February 2007 and emerged a critical and commercial success, despite facing stiff competition from Paruthiveeran at the box-office. The film, notably, was the last film that Jyothika signed up for, as she retired from acting after marriage. Mozhi became a milestone in the career of Jyothika. The film was dubbed in Telugu as Maatarani Mounamidi.

Mozhi was the first Tamil film receiving the fastest launch in the home video market. The producers had tied up with optical disc maker Moser Baer for the release. The film was premiered in the Non prize section section of 2007 Cannes Film Festival.  

==Plot==
Karthik (Prithviraj Sukumaran|Prithviraj) and Viji (Prakash Raj) are close buds, who are keyboard players in music director Vidyasagar (music director)|Vidyasagar’s team. They are brilliant in their work of re-recording and background scores. They are fun-loving, witty and share a great rapport. They come to live in an apartment complex where they meet a handful of interesting people. Their neighbour and flat secretary, Ananthakrishnan (Brahmanandam) is not too happy about Karthik occupying the flat and asks him to vacate as bachelors are not allowed to live there. Preethi (Neelima Rani), a girl from one of those apartments, is head over heels in love with Karthik.

One day, Karthik sees a girl (Jyothika) on the road and he is quite impressed by her attitude and guts. Later, he finds out that the girl is Archana, a deaf and mute girl who lives in the same apartment. Karthik falls in love with her, and along with Viji, try to find out more about her from her best friend Sheela (Swarnamalya). Karthik learns sign language from Sheela so that he can communicate with Archana. He wants to have her as his life partner, though she considers him as a good friend.

Archana gets angry with him eventually. She thinks that her baby will be deaf and mute just like her. Her father abandoned her after he found out about her handicap, and she is afraid that Karthik will leave her as well if their baby is like her. Karthik tries to reason with her that her father was unaware of her condition, so it took him by surprise. However, he is ready to accept her and their future children because he knows and understands them. However, his pleas are to no avail. Archana begins avoiding Karthik, and Karthik is heartbroken.

At the same time, Viji also falls in love with Sheela, who is a widow. Sheelas parents agree to the marriage, and their marriage date is fixed. On the day of Vijis wedding, Archana confesses to Karthik that she does indeed love him. The story thus concludes.

==Cast== Prithviraj as Karthik
* Prakash Raj as Vijayakumar (Viji)
* Jyothika as Archana
* Swarnamalya as Angeline Sheela
* Vatsala Rajagopal as Archanas grandmother
* Brahmanandam as Ananthakrishnan
* M. S. Bhaskar as Professor Sriranjini as Ananthakrishnans wife
* Neelima Rani as Preeti
* Geetha Ravishankar as Professors wife
* E. V. Ganesh Babu as Pichumani
* Ramya Subramanian as Pannaiyars daughter

==Production==
{{quote box
| quote = "As a boy, I had seen such a girl living in my colony. She looked cute and moved about cheerfully. I would wonder what `sound meant to her. So later I felt it would be interesting to have a musician opposite this role".
| source =&nbsp;– Radhamohan, in an interview with The Hindu in July 2007. 
| align = right
| width = 30%
}}

Radhamohan revealed that idea of the film came from a little girl who stayed in his neighbourhood. He also told that he had Jyothika in his mind while writing down her character,  she initially refused to act but later agreed after hearing the script. http://www.thehindu.com/todays-paper/tp-features/tp-fridayreview/making-a-point-silent-and-strong/article2271636.ece 

Many actresses refused to play the character of Jyothikas friend,  finally Swarnamalya was selected to play the role, Swarnamalya agreed after hearing the script which gave equal importance to women characters.   Elango Kumaravel who earlier appeared in directors previous films did not acted in this film instead he worked as a dubbing artist lending his voice for Brahmanandam.  Prithviraj was selected after director was impressed with his performance in Paarijatham.  Prasanna was originally selected for the role which was eventually portrayed by Prakashraj. 

Brahmanandam, Neelima Rani and M. S. Bhaskar were selected to portray supporting roles.   

 
The films climax was shot at the church called "Church of Our Lady of Angels" situated at Pondicherry. 

==Music==
{{Infobox album |  
| Name       = Mozhi
| Type       = Soundtrack Vidyasagar
| Cover      = 
| Released   = 2007
| Recorded   = 
| Genres     = World Music
| Length     =
| Label      = Sa Re Ga Ma Vidyasagar
| Last album = Aathi (2006)
| This album = Mozhi (2007)
| Next album = Arai En 305-il Kadavul (2007)
}}
 Vidyasagar gave the music for the Malayalam movie too. All the songs were well received especially "Kaatrin Mozhi" and "Sevvanam". 

A segment of Harry Belafontes vesion of the popular Jewish folk song Hava Nagila is played during the comedy scene between Prakash Raj and Brahmanandam.

The audio was released on February 2007   and received positive reviews from critics.   Indiaglitz praised the soundtrack stating that: "To put it simply, Vidyasagar has given music as much for your soul as he has done for your ears". 

{| class="wikitable" width="70%"
! Song Title || Singers || Duration
|-
| Kannal Pesum Penne || S. P. Balasubramanyam || 04:43
|-
| Kaatrin Mozhi || Balram || 05:52
|-
| Sevvanam Selai Katti || Jassie Gift || 04:45
|- Srinivas || 01:16
|- Karthik || 00:56
|-
| Pesa Marandhaayae || Madhu Balakrishnan || 04:44
|- Tippu || 01:09
|- Sujatha || 05:53
|-
|}

==Remake==
Boney Kapoor has acquired the Hindi remake rights of the film in 2008. According to initial reports Abhishek Bachchan is considered to do the lead role that Prithviraj had done with considerable ease while Kareena Kapoor is earmarked to do the potential dumb and mute act of the female lead, which was done by Jyothika in the original. Radhamohan himself would direct the remake, however it failed to materialised. 

==Release==
The satellite rights of the film were sold to Kalaignar TV|Kalaignar. The film was given a "U" certificate by the Indian Censor Board. The film was released on 23 February 2007 and it was premiered on channels launch date September 15, 2007.  The films official website was created by entertainment portal Indiaglitz. 

N Achyutha Kantha Rao of Indus Inspirations purchased the theatrical rights of Telugu dubbed version. The film was released in Telugu under the title Maataranai Mounamidi. 

==Reception==

=== Critical Reception ===
Mozhi received positive critical acclaim for its clean content, performance and music. Sify wrote, "Director Radha Mohan has once again proved that he can make a pucca family entertainer with characters that you can relate to. Mozhi is indeed gutsy, feel-good, real solid movie within the framework of commercial cinema."  Now running wrote, "Mozhi is not a regular masala flick but a straightforward, heartbreaking and refined tale of love. The power of this film resides in its story, strong and believable narration, genuine sadness and tour-de-force performance by all central characters - Prithviraj, Jyothika, and Prakash Raj".  Indiaglitz praised the film stating that: "Mozhi is a clean entertainer that justifies the tagline that is found in the promotional materials of the film. "It is not the expression, but the emotion".  Behindwoods wrote: "Radha Mohan does it once again. His uncompromising attitude in delivering a neat and family entertainer is remarkable".  Rediff wrote: "Mozhi is a groundbreaking movie for Tamil cinema in many ways. It does not over-sentimentalise physical handicaps   The story is no-nonsense and without any unnecessary frills. Strong characterisation and excellent performances from the cast make this truly a remarkable film".  Hindu wrote:"Mozhi is surely a film its producer can be proud of   Conceiving a simple yet poignant line, creating a screenplay that takes you along smoothly, and helming it, director Radha Mohan presents `Mozhi with panache". 

Rajinikanth praised the film stating that such films make him feel proud and happy that he is part of the Tamil film fraternity. 

===Box-office===
{{quote box
| quote = "The success of `Mozhi is very heartening.   All of us knew what we were doing and it was an opportunity for us to say, "Hey, were capable of this" and the audience have in turn said, "Guys, we love you." They wont forgive us if we do a clichéd movie". 
| source =&nbsp;– Prakashraj, in an interview with The Hindu in April 2007.
| align = right
| width = 30%
}}
The film was produced on a budget of $500,000 and it became a surprise hit grossing $2 million.     The  positive response of the film has led to the generation of additional prints.  The film completed a theatrical run of 100 days. The films 100 day function was held in the open air club house attached to Mayajaal multiplex on the ECR. Anjali Arora, a visually challenged lawyer, working as a legal advisor to the Ministry of Civil Aviation, Balu Mahendra, Ameer, Lingusamy and Sundar.C attended the function. 

==Home Media==
Mozhi has been released in VCDs and DVDs by Moser Baer Home Video. 

==Legacy==
Mozhi became an important film in the career of Prithviraj and Jyothika. The film proved that the people would accept stories based on disability if the film was presented in a new and innovative way.   The film continued the trend of films with different themes that focused on realism.  K. Jeshi of The Hindu placed the film in the category of films that propagate social issues along with other films like Sethu...|Sethu (1999), Kaadhal (2004), Veyil (2006), Imsai Arasan 23am Pulikesi (2006) and Paruthiveeran (2007).  Behindwoods stated that humour helped the film "to become one of the biggest money-spinners in Kollywood" and also added "Prakash Raj’s witty dialogues ensured a merry go ride". 

Director   revealed that: "I’m looking for a role on the lines of the one played by Jyotika in Mozhi and Sridevi in Moondram Pirai".  P. B. Ramasamy, head of Big FM, Chennai told to Hindu that: "Mozhi was a wholesome entertainer and I never got bored even for a minute. The movie made me smile throughout and for the first time while watching a film, I felt the urge to watch it all over again".  Dancer Shweta Prachande who portrayed deaf and dumb character in a short film called Notes of Silence said she was inspired by Jothikas performance from the film to portray the character. 

Scenes from the film has been parodied in Thamizh Padam (2010). Shivas love signals, Disha Pandeys introduction scene and her friend character has been based on the film. 

==Accolades==
Jyothika won many awards for her excellent performance in this film. They are listed below:

1. Tamil Nadu State Film Award for Best Actress  
2. ITFA Award  
3. Benze Award for Best Actress  
4. Nominated for Vijay Award for Best Actress  
5. Ritz Award for Best Actress  
6. Meera Tamil Music Award for Best Actress in a Song Sequence  
7. 58th Film Fans Association Award for Best Actress 2007  
8. Nominated for Film Fare Best Actress Award

==References==
 

==External links==
*http://www.starmusiq.com/tamil_movie_songs_free_download.asp?movieid=405
* 
* 

 
 

 
 
 
 
 
 
 