Evolution: The Musical!
{{Infobox film
| name           = Evolution: The Musical!
| image          = Evolutionposter.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Kenny Taylor
| producer       = Kenny Taylor Andrew Bancroft
| writer         = Andrew Bancroft
| starring       = Tonya Glanz Jon Wolanske Melanie Case Andrew Bancroft
| music          = Olive Mitra Andrew Bancroft
| cinematography = Brian Relph
| editing        = Eric Schultz
| studio         = Mothership Pictures illBilly productions
| distributor    = 
| released       =  
| runtime        = 36 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} musical comedy film depicting the struggle between Evolution and Creationism, through two groups of fictional characters: "The Beasties" and "The Blesseds." As the film explores the struggle between these two groups, a love affair develops between two of the characters. 

==Plot== lineage of The Blesseds (Creationism).

 The focus of the narrative then turns to "The Beasties," who dwell in the forest near the church.  A "Beastie" named Flippy asks another "Beastie," Wog Wog, how the Beasties came into existence. Wog Wog sings a song that depicts the history of their race (Evolution).  At the conclusion of the song, Queen Bonobo, queen of "The Beasties," is informed that her husband, Chief Cranius, has been killed by "The Blesseds."  "The Beasties" exit the forest in search of revenge.
 retribution for killing their chief.
 Sean Hayes), Charles Darwin, appears and stops "The Beasties" in a similar fashion.  The two groups make amends momentarily, but resume fighting when they see Jesus and Darwin fighting.  Wog Wog and Mary flee to the ocean.

==Cast==
* Andrew Bancroft as Wog Wog
* Tonya Glanz as Mary Catheter
* Jon Wolanske as Father Catheter
* Melanie Case as Queen Bonobo
* Shaye Troha as Mother Grizzard
* Paco Romane as Flippy Sean Hayes as Jesus
* Andy Alabran as Chimpy
* Joe Higgins as Charles Darwin
* Olive Mitra as a "Beastie"
* Cameo appearance by Jamie DeWolf

==Production==
  Andrew Bancroft wrote the screenplay, including seven original songs, helped compose the music, and played the "Beastie" character Wog Wog.  Kenny Taylor directed the film and made a cameo as the "Blessed" character Eziekiel.
 Marin County Headlands, and the indoor scenes were shot in Oakland California|Oakland, California.

==Film Festivals==
* San Francisco International Film Festival 2008 
* Digital Video and High Definition Film Festival 2008
* Atheist Film Festival 2009 
* Black Rock City Film Festival 2010

==References==
 

==External links==
*  
*  
* Interview with the Filmmakers on  

;Reviews
* Review of Evolution: The Musical by the  
* Review of Evolution: The Musical! by  
* Review of the San Francisco International Film Festival by  

 
 