Blue Juice
 
{{Infobox film
| name           = Blue Juice
| image          = Bluejuiceposter.jpg
| caption        = Promotional film poster
| director       = Carl Prechezer
| producer       = Simon Relph
| writer         = Carl Prechezer Peter Salmi Tim Veglio
| starring       = Sean Pertwee Catherine Zeta-Jones Ewan McGregor Steven Mackintosh
| music          = Simon Davison
| cinematography = Richard Greatrex
| editing        = Michael Ellis
| choreography   = Clive Clarke
| distributor    = 
| released       = September 15, 1995
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 FilmFour productions.

==Plot==
JC seems to have it all figured out. By day he runs a surf school, at night he lies down next to his beautiful girlfriend Chloe, his lifelong dream is to travel the world surfing. However, when old mates arrive from London unannounced it releases tensions which have long been simmering under the surface of JC and Chloes seemingly perfect relationship. Chole decides to buy the local surfer cafe and settle down, His friends, especially drug-dealer Dean are intent on causing mischief and sucking JC back into surfing a dangerous reef, which he had attempted before, seriously injuring his back.
It turns out that Dean had a job as a journalist, and setting up JC was to get a story.
As a requirement for keeping his job, he had to get a big story, preferably with a life or death situation involved. JC refuses to surf the boneyard which prompts Dean to try it himself as he had already arranged media coverage, and his boss had decided to watch. Dean fails to surf the reef, hitting his head when smashed under by a huge wave. JC then dives in to rescue Dean and in doing so, successfully surfs the boneyard therefore saving Deans life and job at the same time. However Deans boss gets knocked out by the local guru for his highly offensive attitude and remarks.
JCs friend Terry, having been given drugs by Dean, has radically rethought his life, and buys JCs round the world tickets for him and his fiancé. JC then uses the money to buy a cafe for him and Chole, deciding that his relationship with Chole is more important than impressing his friends.

==Cast==
* Catherine Zeta-Jones as Chloe
* Sean Pertwee as J.C.
* Ewan McGregor as Dean Raymond
* Steven MacKintosh as Josh Tambini Peter Gunn as Terry Colcott

==Wardrobe and props==
Many of the films characters are dressed in clothing from the pressure group Surfers against Sewage. Many characters also wear the Australian surfwear label Mambo Graphics and Stussy hats. Wetsuits used in the film and in publicity shots were manufactured by the Cornish Surf brand Gul. Other surf brands seen throughout the film, in the form of stickers or clothing, include Body Glove and Quiksilver.

Surfers Against Sewage stickers and posters are widely used throughout the film, visible in the Aqua Shack scenes and applied to the blue Bedford CF van driven by JC and Chloe for the surf school.

Pertwee used toupée tape to hold a sock in place, in the scene where JC appears nude apart from a black sock. "...I came up with this ingenious ploy - wrapping toupée tape on my chap. You wont believe how difficult it is to remove..."  In a 1995 interview with FHM magazine|FHM, Zeta-Jones recalled the filming of the sock scene; "It was a  brown Marks & Spencer one, though. If it was a Stussy  one, or something like that, it might have been more interesting." 

Professional surfer Steve England was a body double for Peter Gunns character, Terry. To replicate Gunns look and larger build he had to have his long hair cut and wear two wetsuits with towels packed around his stomach. 

A reference is made in the film to the 1960s comic book character the Silver Surfer. On the way to find the rave, JC, Josh, Dean and Terry pass a man painted entirely in silver carrying a silver surfboard who waves at them. Terry, under the influence of drugs supplied by Dean, then copies this by painting himself silver.

There are a couple of errors in the story. Early on when the radio DJ says the cows will be coming in for milking it cuts to a herd of Hereford cattle blocking a road. Herefords are farmed for beef not milk. In the scene where Wigan Casino is discussed, the order given of Three before Eight records is wrong.

The red and yellow ‘TASTY’ surfboard that Ewan McGregor used in the film, was sold at a film memorabilia auction in 2001 at Sotheby’s in New York, to raise money for children born with Aids in Africa. 

==Supporting cast==
Blue Juice features an appearance from Jenny Agutter as a retired actress turned hotel proprietor Mary Fenton, who is famous for playing Guinevere in a fictional television show called "Arthurs Knights".
 soul singer Paul Reynolds, whose largest role before Blue Juice was as Christopher Craig, the accomplice of Derek Bentley in the film Let Him Have It.
 Keith Allen had a small part as Mike, a tabloid newspaper editor, who pays the Ewan McGregor character, Dean, for stories about record producer Josh.

The role of Shaper, played by Heathcote Williams, was also offered to Nigel Terry, best known for his portrayal of King Arthur in the 1981  John Boorman film, Excalibur.
 ITV soap Echo Beach, kite surfer and set the world distance record for kite surfing, traveling from Cornwall to Ireland, in September 2005.  Another surfer was played by Martin Dorey, author of The Campervan Cookbook and presenter of the BBC2 series One Man and His Campervan.

Chloe and JCs baby is played by Astrid Weguelin, the daughter of the films make-up artist Kirstin Weguelin.

==Filming locations==
Most of the film is set in Cornwall and filmed there, many scenes are actually cut showing several different beaches and villages as if they were all adjoining, allowing the main parts of Cornwall to be incorporated into a single place. St Ives for some of the street scenes. The scene filmed at the train station was in St. Erth, just outside Hayle. The map examined by JC, Josh and Dean whilst trying to work out where Terry has gone on the bike shows Godrevey Point and the B3301 road, a few miles north of Hayle. The scene where JC and Chloe argue through the windscreen of the van have is at Trevellas Porth Valley, also known as Blue Hills, between St Agnes, Cornwall|St. Agnes and Perranporth. The location of the second Aqua Shack is Kynance Cove Cafe, at Kynance Cove on Lizard Point. The hotel where Josh and Dean take Terry for the cream tea is the Tregenna Castle Hotel, on the outskirts of St Ives, whilst the village hall scene was filmed at Rose Hill, just off Bunkers Hill in the Downalong area of St Ives.

The large wave and surfing sequences were filmed in Famara, Lanzarote, while a few close ups were filmed in a specialist wave pool, as shown in the documentary that accompanies the DVD release.

Additional shots were filmed in London and at Pinewood Studios.

In a 2008 interview with The Independent, Sean Pertwee stated that his favourite place in the British Isles was St. Agnes in Cornwall. "I spent a lot of time there when filming Blue Juice, which was about surfers. The walk along the top of the cliffs is beautiful." 

==Soundtrack==
* "Movin On Up" performed by Edwin Starr (a cover of the Primal Scream song)
* "The Price of Pain" - performed by Edwin Starr
* "Freedom Bug" - performed by Heavy Stereo, written by Gem Archer
* "Get It On" - performed by Marc Bolan and T.Rex Ride
* "Half the Man" - written and performed by Jamiroquai
* "Duel" - performed by Swervedriver
* "Lonely for You Baby" - performed by Sam Dees
* "I Need Something Stronger" - performed by Apollo 440
* "Youre the One" - performed by Gillian Wisdom
* "You Were the Dream" - Roscoe Shelton
 score in the film was written by Simon Davison.

The soundtrack has never been commercially available.

==DVD artwork==
For the 2000  ." 

A later release on DVD in 2004 reverted to the original artwork featuring Pertwee and 
Zeta-Jones, while a further release in 2008 saw the artwork entirely redesigned with FilmFour branding and a fresh image of Pertwee and Zeta-Jones.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 