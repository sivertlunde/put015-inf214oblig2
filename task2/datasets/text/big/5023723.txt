Fando y Lis
{{Infobox film
| name           = Fando & Lis
| image          = Fando y Lis.jpg
| caption        = 
| director       = Alejandro Jodorowsky
| producer       = 
| writer         = Fernando Arrabal 
| starring       = Diana Mariscal Sergio Kleiner
| music          = Pepe Avila
| cinematography = Rafael Corkidi
| editing        =  Abkco Records
| released       = 1968
| runtime        = 96 minutes
| country        = Mexico
| awards         = 
| language       = Spanish
| budget         = 
}}

Fando y Lis is a film adaptation of a Fernando Arrabal play by the same name, and it is Alejandro Jodorowskys first feature length film. Arrabal was working with Jodorowsky on performance art at the time. The film was done in black and white on the weekends with a small budget and was first shown at the Acapulco Film Festival in 1968.

==Plot==
The film follows Fando (Sergio Klainer) and his paraplegic girlfriend Lis (Diana Mariscal) through a barren, post-apocalyptic wasteland in search of the mythical city of Tar, where legend has it all wishes come true.

==Reception==

When the film premièred at the 1968 Acapulco film festival, a full-scale riot broke out. Rosenbaum, 1992. p.92  The film was later banned in Mexico. Rosenbaum, 1992. p.93  Roman Polanski (who was with his wife Sharon Tate to promote his film Rosemarys Baby (film)|Rosemarys Baby) defended the film, stating that he defends any auteurs right of expressing himself with complete liberty and that censorship in art and culture was just not acceptable (like what happened to him in his motherland).

The film with thirteen minutes cut, was released in New York to generally negative reviews, with many critics comparing it unfavorably to Fellini Satyricon which had recently opened. 

It currently holds a 67% "fresh" score on Rotten Tomatoes.

==References==
 

;Bibliography
* {{cite book
| last      = Rosenbaum
| first     = Jonathan 
| title     = Midnight movies
| publisher = Da Capo Press
| year      = 1991
| isbn      = 0-306-80433-6
| url       = http://books.google.ca/books?id=yiRH-NdN_cAC
}}

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 

 