The Proud Twins (film)
 
 
{{Infobox film name = The Proud Twins image = The Proud Twins (1979 film).jpg caption = DVD cover art traditional = 絕代雙驕 simplified =　绝代双骄 pinyin = Jué Dài Shuāng Jiāo}} director = Chor Yuen producer = Run Run Shaw story = Gu Long screenplay = Chor Yuen starring = Alexander Fu Ng Wai-kwok music = Eddie H. Wang cinematography = Wong Chit editing = Chiang Hsing-lung Yu Siu-fung studio = Shaw Brothers Studio distributor = Shaw Brothers Studio released =   runtime = 105 minutes country = Hong Kong language = Mandarin budget = gross = 
}}
The Proud Twins is a 1979 Hong Kong film adapted from Gu Longs novel Juedai Shuangjiao. The film was directed by Chor Yuen, produced by the Shaw Brothers Studio, and starred Alexander Fu and Ng Wai-kwok as the lead characters.

==Cast==
 
*Alexander Fu as Jiang Xiaoyu (Xiaoyuer)
*Ng Wai-kwok as Hua Wuque
*Wong Yung as Yan Nantian
*Wen Hsueh-erh as Tie Xinlan
*Susanna Au-yeung as Murong Jing
*Kitty Meng as Yaoyue
*Tang Ching as Jiang Biehe / Jiang Qin
*Ku Kuan-chung as Jiang Yulang
*Cheng Miu as Wan Chuliu
*Chan Shen as Ten Villains chief
*Lau Wai-ling as Xiao Mimi
*Jamie Luk
*Yuen Bun
*Lun Ga-chun
*Lam Fai-wong
*Lee Ging-fan
*Yeung Chi-hing
*Ngai Fei
*Wong Chi-wa
*Wong Ching-ho
*Ting Tung
*Chai Lam
*Tam Bo
*Wong Chi-ming
*Sek Gong
*Wong Pau-gei
*Ho Hon-chau
*Hung Ling-ling
*Gam Tin-chue
*Sai Gwa-pau
*To Wing-leung
*Kwan Yan
*Fung Ming
*Cheung Chok-chow
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 

 