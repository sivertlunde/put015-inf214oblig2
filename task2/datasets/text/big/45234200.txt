Tillie (film)
{{Infobox film
| name           = Tillie
| image          =
| alt            = 
| caption        =
| director       = Frank Urson
| producer       = 
| screenplay     = Alice Eyton Helen Reimensnyder Martin
| starring       = Mary Miles Minter Noah Beery, Sr. Allan Forrest Lucien Littlefield Lillian Leighton Marie Trebaol
| music          = 
| cinematography = Allen M. Davey
| editing        = 
| studio         = Realart Pictures Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Frank Urson and written by Alice Eyton and Helen Reimensnyder Martin. The film stars Mary Miles Minter, Noah Beery, Sr., Allan Forrest, Lucien Littlefield, Lillian Leighton and Marie Trebaol. The film was released on January 29, 1922, by Paramount Pictures.   

==Plot==
 

== Cast ==
*Mary Miles Minter as Tillie Getz
*Noah Beery, Sr. as Jacob Getz
*Allan Forrest as Jack Fairchild
*Lucien Littlefield as Doc Weaver
*Lillian Leighton as Sarah Oberholtzzer
*Marie Trebaol as Sallie Getz
*Virginia Adair as Louisa Robert Anderson as Absalom Puntz Edward Cooper as Lawyer

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 