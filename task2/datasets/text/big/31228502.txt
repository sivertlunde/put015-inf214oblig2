Ghost: Mouichido Dakishimetai
 
 
{{Infobox film
| name           = Ghost: Mouichido Dakishimetai
| image          = Ghost2010poster.jpg
| caption        = Theatrical release poster
| director       = Taro Otani
| producer       = Takashige Ichise
| writer         = Shimako Sato, Miho Nakazono
| screenplay     = Miho Nakazono, Shimako Sato
| story          = Bruce Joel Rubin
| based on       = 
| starring       =  
| music          = Michiru Oshima
| cinematography = Takuro Ishizaka
| editing        = Yoshifumi Fukasawa
| studio         = CJ Entertainment Paramount Pictures Shochiku Miyagi Television Broadcasting D.N. Dream Partners Pivot Plus Music 
| distributor    =  
| released       =  
| runtime        = 116 minutes
| country        = Japan South Korea
| language       = Japanese
| budget         = 
| gross          = United States dollar|US$9,833,553   
}}

  is a 2010 Japanese remake of the 1990 American film Ghost (1990 film)|Ghost.    It is directed by Taro Otani and it stars Nanako Matsushima, Song Seung-heon, Mana Ashida and Kirin Kiki.   

==Plot==
 
  Korean potter Kim Jun-ho, and they both live a seemingly happy life. Then, one month after their marriage, Nanami is killed by a biker on her way home. This tragedy leaves Jun-ho completely devastated. At the hospital, Nanamis ghost arises from her body, and upon meeting a ghost child, she realizes that she is a ghost whose presence cannot be seen. She then realizes that her death was no coincidence and Jun-ho is in imminent danger. Unable to communicate with normal humans, Nanami seeks help from the elderly psychic Unten in hopes of saving Jun-hos life.

==Cast==
* Nanako Matsushima as Nanami Hoshino
* Song Seung-heon as Kim Jun-ho
* Kirin Kiki as Unten
* Mana Ashida as the kid Ghost
* Satoshi Hashimoto
* Sawa Suzuki
* Kazuko Kurosawa
* Daisuke Miyagawa
* Yoichi Nukumizu
* Kyusaku Shimada

==Production==

===Theme song===
Eiga.com reported on 1 September 2010 that the theme song of the film Ghost: Mouichido Dakishimetai will be the song  , which is sung by singer Ken Hirai.    This song was released as his 33rd single on 10 November 2010.  Ken Hirai had previously provided theme songs for films like I Give My First Love to You and Ano Sora o Oboeteru. 

==References==
 

==External links==
*   
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 