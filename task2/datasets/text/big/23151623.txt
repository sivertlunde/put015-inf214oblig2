Pirates in Callao
{{Infobox Film
| name = Pirates in Callao
| image =
| image_size =
| caption =
| director = Eduardo Schuldt
| producer =
| writer = Book: Hernán Garrido Lecca Screenplay: Pipo Gallo
| narrator =
| starring = Diego Bertie Stephanie Cayo Salvador del Solar Alberto Ísola Gonzalo Torres
| music = Diego Rivera
| cinematography =
| editing =
| studio = Alpamayo Entertainment
| distributor = United International Pictures (Peru) 2005 United States: October 28
| runtime = 78 minutes
| country = Peru
| language = Spanish
| budget = $500,000 (estimated)
| gross =
| preceded_by =
| followed_by =
}}
 2005 Peruvian CGI animated film directed by Eduardo Schuldt based on the childrens book of the same name written by Hernán Garrido Lecca. It tells the story of Alberto, a Peruvian kid who gets lost during a school trip to the Real Felipe Fortress in the seaport of Callao. The nine-year-old Alberto falls through a hole and travels back in time to the 17th century when the port is about to be attacked by a group of Dutch pirates led by Jacques LHermite. There he meets children who have also travel back in time and together they fight and defeat LHermite. The film is notable for being one of the first computer-animated film made in Latin America.

==References==
* David Cornelius.  . eFilmCritic. Retrieved on June 8, 2009.

==External links==
*    
*  

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 