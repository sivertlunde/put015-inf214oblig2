Sir (film)
{{Infobox film
| name           = Sir
| image          = Sir.jpg
| caption        = Poster
| director       = Mahesh Bhatt
| producer       = Mukesh Bhatt
| writer         = Jay Dixit
| narrator       = 
| starring       = Naseeruddin Shah Pooja Bhatt Atul Agnihotri Paresh Rawal Avtar Gill Soni Razdan Sushmita Mukherjee Gulshan Grover
| music          = Anu Malik
| cinematography = Pravin Bhatt
| editing        = 
| distributor    = 
| released       = 9 July 1993
| runtime        =  
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
|}}

Sir is a 1993 Bollywood film directed by Mahesh Bhatt starring Naseeruddin Shah, Pooja Bhatt, Atul Agnihotri and Paresh Rawal. The film was remade in Telugu as Gangmaster (film)|Gangmaster with Rajasekhar (actor)|Rajasekhar.

It was inspired by the film Raajkumar starer Bulundi 

==Plot==
Mumbai-based college teacher Amar Verma (Naseeruddin Shah) lives with his wife, Shobha (Soni Razdan), and 6 year old son, Kunal. When a war breaks out between ganglords Veljibhai (Paresh Rawal) and Chhapan Tikli (Gulshan Grover) alias Jimmy, a number of innocent bystanders fall victim, one of whom is Kunal. This devastates the Verma family. Shobha walks out on Amar, who then devotes his life to his students. Many years later, when he finds out that a student, Pooja (Pooja Bhatt), has a problem of stammering, he decides to help her. He finds out that she is the only child of gangster Veljibhai. Amar meets with Veljibhai, discusses the gang-war scenario, then meets with Chhapan Tikli and does the same. Amar gets the gangsters to agree on a truce so that he can take Pooja and the rest of the students on a field trip to Bangalore. Amar will soon find out that neither Veljibhai nor Chhapan Tikli has any intention of keeping any truce. As the two dons engage in a fight to the finish, it seems that Amar may well have jeopardized his own life as well as the lives of Pooja and the other students. An additional complication arises with Pooja falling in love with her classmate Karan (Atul Agnihotri) and wishing to marry him against the wishes of her father.

==Cast==
* Naseeruddin Shah:  Amar Verma
* Pooja Bhatt: Pooja
* Atul Agnihotri: Karan
* Paresh Rawal: Velji
* Avtar Gill: Arjun Das
* Soni Razdan: Shobha
* Sushmita Mukherjee: Sweety (Jimmys Sister) (as Sushmita Mukherjee)
* Gulshan Grover: Chhappan Tikli a.k.a. Jimmy
* Abha Ranjan: Veljis wife
* Suhas Bhalekar
* Mushtaq Khan: Kaluba (as Mushtaque Khan)
* Mahesh Anand: Jimmys Henchman
* Makrand Deshpande: Mak
* Anang Desai: Doctor
* Anant Jog
* Viplove Rai
* Vikas Anand
* G.P. Singh
* Gopal Poojari
* Kamal Malik
* Deepak Sinha
* Anjana
* Banjara
* Master Kunal Khemu (as child artist): Kunal
* Brownie Parasher: Last man killed by Kaluba

==Track list==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
|"Jis Din Suraj Ki" Kumar Sanu
|-
| 2
| "Aaj Humne Dil Ka"
| Kumar Sanu, Kavita Krishnamurthy
|-
| 3
|"Sun Sun Barasat Ki Dhun"
| Kumar Sanu
|-
| 4
| "Yeh Ujali Chandani"
| Kumar Sanu, Alka Yagnik
|-
| 5
|"Hum Se Badal Gaya"
| Kumar Sanu
|-
| 6
|"Ode Ke Andhera "
| Alka Yagnik
|-
| 7
| "Band Hoton Se (Male)"
| Kumar Sanu
|-
| 8
|"Band Hoton Se (Female)"
| Kavita Krishnamurthy
|}

==External links==
*  

 

 
 
 
 
 

 