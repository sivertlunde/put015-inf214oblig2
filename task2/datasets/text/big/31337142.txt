Command Performance (1937 film)
{{Infobox film
| name =  Command Performance 
| image =
| image_size =
| caption =
| director = Sinclair Hill
| producer =Harcourt Templeman      George Pearson   Sinclair Hill 
| narrator = Mark Daly   Finlay Currie
| music = Louis Levy 
| cinematography = Cyril Bristow 
| editing = Michael Hankinson 
| studio = Grosvenor Films 
| distributor = General Film Distributors 
| released = 19 August 1937
| runtime = 84 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} musical drama Mark Daly.  It was based on a play by Stafford Dickens. Like The Street Singer which was released the same year, it was designed as a vehicle for Tracy who performs a number of songs during the film. It was made at Pinewood Studios. 

==Plot== manhunt is whipped by the press to find him so that he can shoot the final scenes of his latest film.

==Cast==
* Arthur Tracy as Street Singer 
* Lilli Palmer as Susan  Mark Daly as Joe 
* Rae Collett as Betty 
* Finlay Currie as Manager 
* Jack Melford as Reporter 
* Stafford Hilliard as Sam 
* Julian Vedey as Toni 
* Phyllis Stanley as Olga

==References==
 

==Bibliography==
*Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 