Her Own People
{{Infobox film
| name           = Her Own People
| image          = 
| alt            = 
| caption        =
| director       = Scott Sidney
| producer       = 
| screenplay     = Gardner Hunting Julia Crawford Ivers
| starring       = Lenore Ulric Colin Chase Howard Davies Adelaide Woods Jack Stark Gail Brooks
| music          = 
| cinematography = James Van Trees	
| editing        = 
| studio         = Pallas Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Scott Sidney and written by Gardner Hunting and Julia Crawford Ivers. The film stars Lenore Ulric, Colin Chase, Howard Davies, Adelaide Woods, Jack Stark and Gail Brooks. The film was released on February 8, 1917, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Lenore Ulric as Alona 
*Colin Chase as Frank Colvin
*Howard Davies as John Kemp
*Adelaide Woods as Eleanor Dutton
*Jack Stark as Jimmie Pope
*Gail Brooks as Morning Star
*Joy Lewis as Myra Agnew
*William Jefferson as Blinn Agnew 
*Ada Lewis as Mrs. Colvin
*Mary Mersch as Katherine Colvin William Steele as Polsa Kar

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 