Jungle Boy (1998 film)
{{Infobox Film |
name      =  Jungle Boy | John Lawson and Damian Lee (based on characters created by Ashok Amritraj and Damian Lee) | David Fox Asif Mohammed Seth Jeremy Roberts Lea Moreno Chippy Gangjee  Ratan Thakore Grant |
director    = Allan Goldstein |
producer    = Ashok Amritraj and Damian Lee | David and Eric Wurst|
distributor  = Moonstone Entertainment |
released    = 1998 |
runtime    =  88 minutes |
language    = English |
}} family adventure John Lawson, and directed by Allan Goldstein.

==Plot summary==
  elephant Bono and the monkey Mantoo, and he is given the ability to communicate with the animals. Bono informs the animals present that Sabre has hunted on the Night of Deva and for Sabre to be brought to him. When Sabre appears and Bono scolds him for violating the no hunting rule that occurs on the Night of Deva, Sabre states that he will come for the boy when he is old enough to defend himself. Suresh is named "Manling" and raised by the forest creatures where Bono ends up training the Manling to defend himself when it comes to the day when he has to fight Sabre.

As an adult, Manling (now called Krishna) must battle Sabre. At the same time, Professor John Gellar has picked up his niece Anna from the airport at the time when he was looking for a valuable statue associated with the Nāga (with the statue enabling anyone pure of heart to understand all the worlds languages) and the scroll that goes with it. Rajah Singh is also seeking to obtain the statue where he claims that the treasure belongs to his family and hires treasure hunter Joshua Hook to find it. Hook will use any means necessary, even killing, to make sure he gets not only the treasure, but also his share of the money from finding it. Rajah Singh has his servant Sanjay work with Joshua Hook who is instructed to act as Rajah Singhs game warden. Joshua Hook has an encounter with Professor Gellar when he tries to pursue Krishna as Professor Gellar points to the opposite direction. Later that evening, Professor Gellar tells Anna about the jungle boy stating that he was a kid who wandered into the jungle and was raised by animals. Professor Gellar also tells Anna that the mother of the boy died of a broken heart.

Krishna arrives at the Valley of Death where he must fight Sabre. But when he defeats Sabre in a manner that violates the rules involving defeating him near a village by using fire against him, he is told by Bono to go out on his own much to the objection of Mantoo. Under advice from Deva the next day, Krishna is told that he must return to the humans. Geller and his pretty niece Anna find him and take him in, teaching him English and how to be human. He learns fast because he knew human ways at one time. Professor Geller also mentioned in the narration that Sabre left the jungle and wasnt heard from again.

Anna and Krishna become romantically involved. After Mantoo visits Krishna, Bono is shown to have some regrets of sending Krishna away. Gellers servant Ravi tells Joshua Hook and his cousin Sanjay about Gellar also looking for the statue. The next day, Geller finds his map missing as Joshua Hook and his men arrives with the map as Joshua Hook captures Anna and hands her over to Rajah Singh. Geller is now forced to work with Joshua Hook to obtain the statue from the sacred jungle. Disappointed that Mr. Hook had tricked him, Ravi tells Krishna what happened and Krishna starts to rescue Anna from Rajah Singhs palace

After Krishna frees Anna from Rajah Singh, they go after Joshua Hook as Deva tells Krishna that he must enlist Bonos help and tell him that its his orders to help Krisha. Krishna finds Bono and Mantoo where Krishna tells Bono what Deva wanted Krishna to tell Bono. Arriving near the spot where the statue is located, Krishna, Anna, Bono, and Mantoo await nearby as Deva tries to prevent Joshua Hook from obtaining the statue. This doesnt work as Deva teleports away when Joshua Hook tries to shoot him which ends up breaking open the rock the contains the statue. After wounding Professor Gellar as Anna runs to his side, Joshua Hook orders Sanjay to reach into the rock and grab the statue and scroll. Sanjay does so and Joshua Hook makes off with the statue, the scroll, and Anna. Deva tells Krishna to go after Mr. Hook while he heals Professor Gellar.

Moments later at Rajah Singhs palace, Joshua Hook and Rajah Singh make their way to the Alter Room with Anna as their prisoner. Krishna and Mantoo enter the palace and make their way to the Alter Room. After Sanjay leaves upon tying Anna to a column, Joshua Hook has Rajah Singh recite the prayer that would summon Nāga to them. When the giant cobra Nāga arrives upon Krisha and Mantoos arrival, it deems Joshua Hook unworthy to receive the special gift. Joshua Hook fires his gun at Nāga which doesnt phase Nāga who then devours Joshua Hook. When Rajah Singh states to Nāga that he is Nāgas disciple, Nāga tells Rajah Singh that he doesnt deserve the title and turns him into a pig. Upon Nāga turning his attention towards Krishna and Anna, Krishna tells Nāga not to harm Anna. Nāga agrees with Krishnas demands declaring them worthy of the special gift. Before regressing back into statue form, Nāga tells Krishna to hide his statue and scroll someplace where nobody will ever find it. After Naga leaves, Krishna frees Anna who can now understand Mantoo.

With Professor Gellar recovered and Ravi is the servant of Professor Gellar again, Krishna prepares to leave as he considers Anna part of his family and tells her to come visit him someday. Krishna leaves and reunites with Bono and Mantoo. A voice-over by Professor Gellar states that Krishna has returned to the jungle and placed Nāgas statue and scroll someplace where nobody will ever find it. Professor Gellar also mentions about the day when Mantoo stumbled upon a lost temple in the jungle, but that will be for another story.

==Cast== David Fox - Professor John Geller
* Asif Mohammed Seth - Krishna
* Lea Moreno - Anna Geller
* Jeremy Roberts - Joshua Hook Patrick Grant - Ravi
* Chippy Gangjee - Rajah Singh Avinash - Sanjay
* Premlal - Deva
* Nihar Kiran Shembekar - Suresh

===Voices===
* Robert Quarry - Bono the Elephant
* Jenni Pulos - Mantoo the Monkey
* Ross Hagen - Sabre the Leopard    

==Reception==
* Richard Scheib  describes this film as "a distinctly B movie|B-budget effort" and "a blatant copy of Rudyard Kipling’s The Jungle Book stories". 

==References==
 

==External links==
*  

 
 
 
 
 