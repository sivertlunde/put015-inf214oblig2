When the Cat's Away (1996 film)
{{Infobox film
| name           = When the Cats Away
| image          = Chacun cherche son chat.jpg
| image size     =
| caption        =
| director       = Cédric Klapisch
| producer       =
| writer         = Cédric Klapisch
| narrator       =
| starring       = Garance Clavel Zinedine Soualem Renée Le Calm
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1996
| runtime        = 91 min.
| country        = France French
| budget         =
| preceded by    =
| followed by    =
}}
 French drama directed by Cédric Klapisch. The movie is set in Paris and stars Garance Clavel, Zinedine Soualem, Renée Le Calm, Olivier Py, Romain Duris, Hélène de Fougerolles, Hiam Abbass and others.

== Plot ==
The story is about a young woman living in Paris, in the Bastille neighborhood, who has lost her cat. Through the pursuit of the cat, the film lets the spectator discover the contrasts of the district, which is evolving from a typical Parisian district to a modern and young district and the contrast between its inhabitants, among them a nice simpleton and the old women who are bored. In fact it is the story of a village in a large city, Klapisch shows once again the behaviour of an individual inside a group, as he did in Le péril jeune or Riens du tout. This time he has succeeded in joining together actors, theatre actors and residents of the district, creating a credible atmosphere that is probably the reason for the success of this film outside France.

== Cast ==
* Garance Clavel as Chloé
* Zinedine Soualem as Djamel
* Renée Le Calm as Madame Renée
* Olivier Py as Michel
* Simon Abkarian as Carlos
* Jacqueline Jehanneuf as Madame Henriette
* Romain Duris as The battery player
* Hélène de Fougerolles as a Model
* Marine Delterme as a Model
* Eriq Ebouaney as The Cabinetmaker worker
* Hiam Abbass as a Woman

== Soundtrack ==
The main title "Food for Love" was composed and performed by Cqmd. 

==References==
 

== External links ==
*  
 
 
 
 
 
 
 
 

 
 