Thanks for Every New Morning
{{Infobox film
| name           = Díky za každé nové ráno
| image          = Diky za kazde nove rano.jpg
| border         = 
| alt            = 
| caption        = 
| director       = Milan Šteindler
| producer       = Tomáš Chadím  Miloš Fedaš  Karel Czaban
| writer         = Halina Pawlowská
| screenplay     = 
| story          = 
| based on       = 
| starring       = Ivana Chýlková  Franciszek Pieczka  Barbora Hrzánová  Alena Vránová  Dagmar Edwards
| music          = Petr Ulrych
| cinematography = Jiří Krejčík Jr
| editing        = Věra Flaková
| studio         = 
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Czech Republic Ukrainian
| budget         = 
| gross          = 
}}
 Czech film directed by Milan Šteindler. It was the Czech Republics submission to the 68th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.   

==Cast==
* Ivana Chýlková as Olga
* Franciszek Pieczka as father
* Barbora Hrzánová as Lenka
* Alena Vránová as mother
* Halina Pawlowská as Vasilina
* Milan Šteindler as StB officer
* Jiří Langmajer as Honza
* Karel Heřmánek as Orest
* Tomáš Hanák as Lesik
* Petr Čepek as famous writer
* Miroslav Etzler as Mirek
* Szidi Tobias as Romka

==Awards ==
{| class="wikitable"
|- Year
!width="147"|Nominated work
!width="186"|Award(s)  Category
!width="70"|Result
|- 1995
|rowspan="2"|Díky za každé nové ráno Febiofest - Czech Critics Award Best Film
| 
|- Czech Lions
| 
|- Halina Pawlowská Best Screenplay
| 
|- Franciszek Pieczka Best Actor in Leading Role
| 
|- Ivana Chýlková Best Actress in Leading Role
| 
|- Barbora Hrzánová Best Supporting Actress
| 
|- Milan Šteindler Best Director 
| 
|- 19th Moscow Moscow IFF - Silver Saint George|St. George
|    
|}

==See also==
*List of Czech submissions for Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 

 
 
 


 
 