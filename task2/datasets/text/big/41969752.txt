Scarlet Road
{{Infobox film
| name           = Scarlet Road
| image          =
| caption        =
| director       = Catherine Scott
| producer       = Pat Fiske
| writer         = 
| starring       = Rachel Wotton
| music          = David McCormack, Antony Partos, Michael Lira
| cinematography = 
| editing        = Andrea Lang
| distributor    = Paradigm Pictures
| released       = 2011
| runtime        = 106 minutes
| country        = Australia
| language       = English
| budget         = AUD|$390,000 (estimated)
| gross          =
}} Australian Rachel Walkley Documentary Award finalist.   

== Background ==
Wotton is a member of Scarlet Alliance, the Australian Sex Workers Association.  In late 2000, Wotton collaborated with other sex workers and related organisations, such as People with Disability Australia Inc., to form the "Touching Base Committee". The committee explored the concept of providing sexual services for people with special needs and the corresponding training that would be required for participating sex workers. As of February 2014, Touching Base is an incorporated charitable organisation based in New South Wales and provides information, education and support for clients with disabilities, sex workers and disability service providers. 

Filmmaker Catherine Scott had known Wotton for 11 years when she proposed the idea for Scarlet Road; however, Wotton was initially reluctant, due to the sensitive nature of the subject matter. They continued to discuss the idea and the documentary was eventually completed over a three-year period. Wotton later commented on the documentary approach saying: "This film was a collaboration thing. Cathy worked closely with me and listened not just to my voice but to those of other sex workers."     Wotton explained her motivation for making the film in greater depth after its launch in 2011:

 
Part of my reason for doing the film was to wipe away the us and them mentality. Were all one car accident away from being in the same position as these guys. Tomorrow we could all wake up out of coma and not be able to eat let alone have sex or touch ourselves. What I say to people is imagine the next time you go to have sex or masturbate having to call your mum and have her organise it all for you ... People with disabilities want to be viewed as whole beings. Think about how important your sexuality is to how you are perceived. These people arent seen like that, so you can imagine how that makes them feel.    

Following the release of the film, Wotton stated in an interview with the Sydney Morning Herald: "I am a sex worker and I make my money from clients seeing me. Some clients just happen to have a disability."   

== Reception ==
The film garnered a variety of positive reviews. In a 2012 article for The Age on whether movies can help overturn stereotypes surrounding sex and disabilities, Shane Green described the film as moving and noted that it: "continues to win international acclaim".   

Following 2012 screenings at the   noted that some of the most uplifting moments featured mothers discussing their sons desire to have an active sex life.    Writing for Bitch Flicks, Erin Tatum, a reviewer with cerebral palsy, said: "Ultimately, the audience can recognize that there’s a great deal of intersectionality in the way that both sex workers and disabled people are policed and shamed about their sexual expression. Rachel reminds us that the two groups can work together to lessen collective stigma."  

Notably, the film has also been shown at the both the Australian Capital Territory Legislative Assembly, Canberra and Parliament House, Sydney.     Wotton also briefed MPs in Adelaide before a March 2012 screening of the film as part of a broader debate about the decriminalisation of sex work in states beyond New South Wales, and a specific call by South Australia Dignity for Disability MP Kelly Vincent for disability services funding to pay for access to sex therapy or a sex worker. 

== Selected screenings ==
*Sydney International Film Festival, Sydney, Australia (2011) 
*Art Gallery of New South Wales – cast and crew screening, Sydney, Australia (2011)    SBS TV – Australian television premiere (2011) 
* SXSW, Austin, US (2012)   
*Sheffield Doc/Fest, Sheffield, UK (2012)   
* 14th Annual Thessaloniki Documentary Festival, Greece (2012)   
*ACT Legislative Assembly – public screening and reception, Canberra, Australia (2012) 
*Documentary Edge Festival – screened as part of human rights programme, Auckland and Wellington, New Zealand (2013)   
*Hot Docs Canadian International Documentary Festival, Toronto, Canada (2013) 
*Clinton Street Theater Sex Workers Film Series, Portland, US (2014) 
*Sexuality and Disability Expo, Sydney, Australia (2014) 
*Australian Centre for the Moving Image (ACMI) – with Q&A session, Melbourne, Australia (2014) 

== Accolades ==
The film received a 2011 Walkley Documentary Award nomination,  was a finalist for the Foxtel Australian Documentary Prize  and won the peoples choice award at the 10th Oceania Film Festival (FIFO) in Tahiti in 2013. 

== References ==
 

== External links ==
* 
* 

 
 
 
 