Non-Stop (film)
{{Infobox film
| name           = Non-Stop
| image          = Non-Stop2014Poster.jpg
| alt            = A man falling back along an airplane, firing a gun.
| caption        = Theatrical release poster
| director       = Jaume Collet-Serra
| producer       = {{Plain list |
* Joel Silver
* Alex Heineman
* Steve Richards
* Andrew Rona
}}
| screenplay     = {{Plain list |
* John W. Richardson
* Chris Roach
* Ryan Engle
}}
| story          = {{Plain list |
* John W. Richardson
* Chris Roach
}}
| starring       = {{Plain list |
* Liam Neeson
* Julianne Moore
* Scoot McNairy
* Michelle Dockery
* Lupita Nyongo
* Nate Parker
* Jason Butler Harner
* Anson Mount
* Corey Stoll
 
}}
| music          = John Ottman
| cinematography = Flavio Martínez Labiano
| editing        = Jim May
| studio         = {{Plain list |
* StudioCanal
* Silver Pictures
* Anton Capital Entertainment
}}
| distributor    = Universal Pictures
| released       =   
| runtime        = 106 minutes 
| country        = {{Plain list |
* France
* United States
}}
| language       = English
| budget         = $50 million  
| gross          = $222.8 million 
}} action thriller Weird Science. The film received mixed to positive reviews from critics.

==Plot==
 
Bill Marks (Liam Neeson) is an alcoholic Federal Air Marshal Service|U.S. Air Marshal, he enrolled in the Air Marshal Service after he was discharged from the New York City Police Department. On a Boeing 767 non-stop flight from New York to London aboard British Aqualantic Flight 10, midway over the Atlantic Ocean, Marks receives text messages on his secure phone stating that someone on the plane will die every 20 minutes unless $150 million is transferred into a specific bank account.

Breaking protocol, Marks consults with Jack Hammond, the other Air Marshal on the flight. Hammond is revealed to be smuggling cocaine in a briefcase. Marks confronts Hammond and the two get into an argument that results in an altercation. Marks ends up killing Hammond during the fight in a lavatory, justifying it as self-defense. This occurs exactly at the 20 minute mark, resulting in the first death. As Marks attempts to stall for time with the texter, he works with Nancy Hoffman (Michelle Dockery), a flight attendant, and Jen Summers (Julianne Moore), a passenger seated next to Marks, to discover the texters identity. When the next 20 minutes expires, the Captain (Linus Roache) suddenly dies, presumably of poisoning.

Back in the U.S., the media and the public becomes convinced that Marks is  , as the bank account is in his name and a passenger uploads video footage of Marks treating passengers aggressively and that video is broadcast on television. Co-pilot Kyle Rice (Jason Butler Harner) has been instructed by the Transportation Security Administration (TSA) to ignore Marks and land in Iceland, the closest destination; he diverts the plane but continues to cautiously trust Marks. Cell phone programmer Zack White, a passenger on the plane,  is asked by Marks to design a hack which will cause the texters cell phone to ring. It is discovered in the pocket of passenger Charles Wheeler, who claims to have never seen the phone before. After being physically subdued by Marks during the interrogation, Wheeler dies in a similar fashion to the Captain (with symptoms of poisoning.)

In the lavatory, Marks finds a hole in the wall that allowed someone to shoot a poison dart at the Captain; he finds that Wheeler was struck with a dart as well. While Marks and Summers try to gain access to the texters phone, it suddenly activates, sending automated messages to the   implying that Marks is suicidal and is going to detonate a bomb on the plane.

Marks finds the bomb hidden in the cocaine smuggled by Hammond.  Passengers attempt to disable Marks, convinced he is a terrorist. They overpower Marks, but passenger Tom Bowen uses Marks gun to make them move away. Marks finally explains the situation, and they agree to work with him.
 air pressure, placing the bomb in the rear of the plane, covering it with baggage and moving the passengers to the front to contain the explosion, and minimizing casualties. As the protocol goes into effect, a fighter jet escort joins the airliner and warns that if it descends into civilian airspace, it will be shot down.
 lack of security at U.S. airports after 9/11, Bowen believes framing an air marshal as a terrorist will lead to drastically increased security. Bowen is prepared to die with the plane and shoots White, who planned to parachute off with the money, after Marks persuaded White to disarm the bomb. As Bowen prepares to shoot Marks, Rice disregards orders from his fighter jet escort and descends, giving an advantage to Marks in the following fight where he kills Bowen with a head shot. Still alive from Bowens shot, White then attacks Marks but is also defeated. Immediately afterwards, Marks escapes from the blast radius of the bomb just in time, while White is killed by the detonation. 

Rice manages an emergency landing at an air base in Iceland after the bomb explodes. The plane is damaged in the landing, but no one else dies. Marks is hailed as a hero in the media, and he and Summers begin a friendship.

==Cast==
  New York to London
* Julianne Moore as Jen Summers, a passenger that Bill befriends on the flight
* Michelle Dockery as Nancy Hoffman, a flight attendant who knows Marks
* Nate Parker as Zack White, a programmer who helps Bill find the messenger but is later revealed to be the latters accomplice captain of the flight
* Scoot McNairy as Tom Bowen, the mystery messenger and mastermind behind the plot to frame Marks
* Corey Stoll as Austin Reilly, an NYPD police officer
* Lupita Nyongo as Gwen Lloyd, a flight attendant
* Anson Mount as Jack Hammond, another Air Marshal on board the flight
* Omar Metwally as Dr. Fahim Nasir, a doctor on board the flight
* Jason Butler Harner as First Officer Kyle Rice, the co-pilot of the flight
* Corey Hawkins as Travis Mitchell
* Frank Deal as Charles Wheeler TSA agent
* Bar Paly as Iris Marianne
* Jon Abrahams as David Norton
* Quinn McColgan as Becca, a little girl that Bill befriends
 

 

==Filming== JFK Airport on December 7, 2012, and at Long Island MacArthur Airport. This was the inaugural movie filmed at York Studios.   

==Reception==

===Critical response===
Non-Stop received mixed to positive  reviews. Review aggregation website Rotten Tomatoes gives it a rating of 60%, based on 199 reviews, with an average score of 5.8/10. The sites consensus states, "While Liam Neeson is undoubtedly an asset, Non-Stop wastes its cast—not to mention its solid premise and tense setup—on a poorly conceived story that hinges on a thoroughly unbelievable final act."  On another aggregation website, Metacritic, it holds a score of 56 out of 100, based on 41 critics, indicating "mixed or average reviews". 

Chris Nashawaty, writing for  , for The New Yorker, was ambivalent on the films overall scope, but praised Neeson, writing, "Neeson, who brings enormous conviction to these late-career action roles, moves his big body through confined spaces (virtually the entire movie takes place in the airplane) with so much power that you expect him to rip out the seats." 

 , maintained a similar tone in his review, saying of Neeson, "Hes at his best striding up and down the aisles of the aircraft with that big, rolling gait of his, carving out great wads of air with his hands, barking orders, his face in Rodin-ish profile, his destiny, like Mitchums, enlivened by a nobility far greater than the film he finds himself in – the true sign of a B-movie king.", and of Moore "...Neeson enjoys a nice, relaxed rapport with Moore, whose looser, Keaton-esque side seems to come out when cast opposite noble hunks."  

===Box office=== Son of God. 

The film earned $92.1 million at the North American box office. In other markets it took in an additional $130.6 million, for a total of $222.8 million worldwide. Its budget for making the film was $50 million.   

==Soundtrack==
{{Infobox album  
| Name        = Non-Stop
| Cover       = 
| Caption     = 
| Type        = soundtrack
| Artist      = John Ottman
| Released    = April 3, 2014 
| Recorded    = 
| Genre       = 
| Length      = 53:10
| Label       = Varèse Sarabande  302 067 251 8  
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

The original motion picture soundtrack was composed by John Ottman. The record was released on April 3, 2014 via Varèse Sarabande label.

{{Track listing
| collapsed       = no
| total_length    = 53:10 

| title1          = Non-Stop
| length1         = 3:13
| title2          = Damaged Goods
| length2         = 3:43
| title3          = Usual Suspects
| length3         = 1:20
| title4          = Welcome to Aqualantic
| length4         = 1:04
| title5          = First Text
| length5         = 3:16
| title6          = Random Search
| length6         = 1:41
| title7          = Do Something for Me
| length7         = 2:43
| title8          = Circling Passengers
| length8         = 3:12
| title9          = Interrogations
| length9         = 3:24
| title10         = What Happened to Amsterdam?
| length10        = 3:46
| title11         = Death Number One
| length11        = 2:08
| title12         = Reluctant Passenger/Blue Ribbon
| length12        = 2:09
| title13         = Fuck It
| length13        = 3:43
| title14         = Explosions Protocol
| length14        = 1:56
| title15         = Ambush
| length15        = 1:40
| title16         = Message Received
| length16        = 3:21
| title17         = Bathroom Discovery
| length17        = 1:49
| title18         = 8000 Feet
| length18        = 2:11
| title19         = Unloaded Weapon
| length19        = 1:31
| title20         = Crash Landing
| length20        = 1:27
| title21         = Epilogue
| length21        = 3:53
}}

==Home media==
Non-Stop was released on Blu-ray Disc and DVD on June 10, 2014. 

==Sequel==
On June 11, 2014, Entertainment Weekly reported that in an interview with producer Joel Silver, he talked about the possibility of a sequel, and stated that it will not be happening on a plane again. "I need to think of a way to put them in an equal situation. But when I make a sequel I like to replicate the experience, not replicate the movie. Im not going to put them on a plane again, of course. He has a touch of Sherlock Holmes in that he has to figure out whats going on and then he has to figure out how to solve it. I think that characters a great character and well try to figure something else to do. I havent thought about it yet. But I have to, sooner or later." 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 