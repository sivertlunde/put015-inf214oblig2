Rene the Cane
{{Infobox film
 | name =  Rene the Cane
 | image =   600full-rene-the-cane-poster.jpg
 | caption =
 | director =  Francis Girod
 | writer = Francis Girod   Jacques Rouffio
 | starring =Gérard Depardieu
 | music =  Ennio Morricone
 | cinematography =  Aldo Tonti
 | editing =     
 | producer =   
 | released =  
 | country = 
 | language = French
 }}
Rene the Cane ( ,  ) is a 1977 French-Italian crime film directed by  Francis Girod and starring Sylvia Kristel and Gérard Depardieu.

It was released in France in 1977 and recorded admissions of 534,714. 

==Cast==

* Gérard Depardieu : René Bornier 
* Sylvia Kristel : Krista 
* Michel Piccoli : Inspector Marchand 
* Stefano Patrizi : Gino  Riccardo Garrone : Karl 
* Jacques Jouanneau : Fourgue 
* Jean Rigaux : Vieuchêne 
* Orchidea De Santis : Kim 
* Venantino Venantini : Carlo 
* Valérie Mairesse : Martine 
* Jean Carmet : Cannes Police Commissioner  
* Maria van der Meer : Kristas Mother
* Annie Walther : Martha 
* Doris Walther : Bretzel 
* Georges Conchon   
* Évelyne Bouix   
* Catherine Castel
* Marie-Pierre Castel

==References==
 

==External links==
* 

 
 
 