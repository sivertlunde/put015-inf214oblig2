Field of Honor (1986 film)
{{Infobox film
| name           = Field of Honor
| image          = Field of Honor.jpg
| caption        = Part of movie poster
| director       = Kim Dae-hie Hans Scheepmaker
| producer       = Yoram Globus, Menahem Golan, Hyeon-suk Han Henk Bos Kim Dae-hie
| starring       = Everett McGill Ron Brandsteder Bart Römer
| music          = Roy Budd
| cinematography = Hein Groot Eung-Hwi Heo	 
| editing        = Victorine Habets
| distributor    = Cannon Films
| released       = 26 September 1986 (South-Korea) 19 March 1987 (Netherlands)
| runtime        = 110 min
| country        = Netherlands South Korea
| language       = English Korean
| budget         = 
}}
Field of Honor ( ) is a 1986 Dutch/South Korean war film set during the Korean war, directed by Kim Dae-hie and Hans Scheepmaker.

==Plot summary== Chinese troops Dutch  sergeant (whose troops committed atrocities against the local population) finds himself alone in the field. He meets a young Korean woman who tries to save her little brother who is shell shocked. This changes the sergeants outlook on the war.

==Cast==
* Everett McGill – Sergeant Sire De Koning
* Ron Brandsteder – Tiny
* Bart Römer – Lieutenant
* Anis de Jong – Taihutu (as Annies De Jong) 
* Lee Hye-young – Sun Yi (as Hey Young Lee)
* Kim Dong-hyeon – Applesan (as Dong Hyum Kim) 
* Min Yu – Kim 
* Mark Van Eeghem – Brammetje
* Frank Schaafsma – Wiel 
* Guus van der Made – Leen 
* Choi Jae-ho – Chinese Medic (as Jae Ho Choi) Mike Mooney – Journalist Jon Bluming – Platoon Sergeant 
* Fritz Homann – Truck Driver 
* David Hartung – Radioman

==Reception==
The film got a negative review in the Dutch communist newspaper De Waarheid.   

==References==
 

==External links==
* 
* 

 
 
 
 
 
 

 
 