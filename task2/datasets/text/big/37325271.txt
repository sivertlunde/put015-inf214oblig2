More Deadly Than the Male
{{infobox film
| name           = More Deadly Than The Male
| image          = Ethel Clayton More Deadly than the Male Film Daily 1919.png|
| imagesize      =
| caption        =
| director       = Robert G. Vignola
| producer       = Jesse L. Lasky
| writer         = Joseph Gollomb (story "The Female of the Species" in Saturday Evening Post) Julia Crawford Ivers (scenario)
| starring       = Ethel Clayton
| music          =
| cinematography = James Van Trees
| editing        =
| studio         = Famous Players-Lasky
| distributor    = Paramount Pictures
| released       = December 7, 1919
| runtime        = 50 minutes (5 reels)
| country        = USA
| language       = Silent film(English intertitles)

}}
More Deadly Than The Male is a 1919 silent film comedy adventure produced by Famous Players-Lasky and released by Paramount Pictures. Robert G. Vignola directed and Ethel Clayton stars. 

==Cast==
*Ethel Clayton - Helen OHara
*Edward Coxen - Richard Carlin
*Herbert Heyes - Terry OHara
*Hallam Cooley - Jimmy Keen
*Peggy Pearce -

==Preservation status==
This is now considered a lost film.

==References==
 

==External links==
 
* 
* (Univ of Washington, Sayre collection)

 
 
 
 
 
 
 
 


 