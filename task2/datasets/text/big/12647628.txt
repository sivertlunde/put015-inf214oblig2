Nuts in May (film)
 

{{Infobox film
| name           = Nuts in May
| image          =
| caption        =
| director       = Robin Williamson
| producer       = Isadore Bernstein
| writer         =
| starring       = Stan Laurel
| cinematography = Harry M. Fowler
| editing        =
| distributor    =
| released       =  
| runtime        = 30 minutes
| country        = United States English intertitles
| budget         =
}} Robin Williamson, produced by Isadore Bernstein, and featuring Stan Laurel, billed as Stan Jefferson, in his onscreen debut.    

The short was filmed at Bernstein Studios, in Hollywood, California. Very little of the film survives (a little over 60 seconds).  

==Plot==
Stan plays a resident of "Home for the Weak-Minded", apparently a lunatic asylum. Stans particular delusion is that he thinks hes Napoleon.  Stan walks the grounds of the cuckoo-hatch sticking his right hand into his shirt and wearing a Napoleon hat. He thinks hes Napoleon, but he gives the salute of the British army.

Stan has his own personal keeper in the asylum: a taller moustached man who wears a kepi so that Stan will think hes a French officer. 

Stan gets out and finds some local boys, who eagerly join him in playing soldier. Stans kepi-wearing keeper pursues him through the film. Stan hijacks a steamroller, and Stan nearly runs down some workers in a road crew.

The surviving footage consists of Stan in various scrapes with a steamroller, ending with him in a straw boater being dragged off to the asylum.

==Cast and crew==
* Stan Laurel (as Stan Jefferson)
* Mae Dahlberg
* Lucille Arnold Owen Evans
* Charles Arling

==See also==
* Stan Laurel filmography
* List of incomplete or partially lost films Mixed Nuts (1922) film using footage from Nuts in May

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 