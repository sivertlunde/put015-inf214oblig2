Dreyfus (1931 film)
{{Infobox film
| name           = Dreyfus
| image          = 
| image_size     = 
| caption        = 
| director       = F.W. Kraemer  Milton Rosmer
| producer       = F.W. Kraemer Reginald Berkeley and Walter C. Mycroft (writers)  Wilhelm Herzog and Hans Rehfisch (play) Arthur Hardy
| music          = John Reynders
| cinematography = Walter J. Harvey Horace Wheddon Willy Winterstein
| editing        = Langford Reed  Betty Spiers
| distributor    = 
| released       = 1931
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Dreyfus is a 1931 British film on the Dreyfus affair, translated from the play by Wilhelm Herzog and Hans Rehfisch and the 1930 German film Dreyfus (1930 film)|Dreyfus.

==Cast==
*Cedric Hardwicke -	Capt. Alfred Dreyfus Charles Carson Picquart
*George George Merritt - Émile Zola Labori
*Beatrix Thomson - Lucille Dreyfus
*Garry Marsh - Ferdinand Walsin Esterhazy|Maj. Esterhazy
*Randle Ayrton - Court-martial president
*Henry Caine - Col. Hubert-Joseph Henry
*Reginald Dance - President, Zola trial
*George Skillan - Maj. Armand du Paty de Clam
*Leonard Shepherd - Georges Clemenceau Arthur Hardy - Gen. Auguste Mercier
*Alexander Sarner - Mathieu Dreyfus
*Frederick Leister - Edgar Demange
*J. Fisher White - Georges-Gabriel de Pellieux
*Abraham Sofaer - Dubois
*J. Leslie Frith - Alphonse Bertillon
*George Zucco - Jacques Marie Eugène Godefroy Cavaignac
*Nigel Barrie - Jules Lauth
*Violet Howard - Marguerite

==External links==
* 

 

 
 
 
 
 
 
 

 