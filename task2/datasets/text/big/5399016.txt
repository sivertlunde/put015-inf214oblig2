An American Crime
 American Crime}}
 
{{Infobox film
| name = An American Crime
| image = American crimemp.jpg
| alt = Against a black background, a tightly cropped image showing only Catherine Keeners glaring eyes appears above the title "An American Crime" in white. A similarly cropped image of Ellen Pages tear-filled eyes appears below the title, and just above the tagline "The true story of a shocking crime and a secret that wouldnt keep". The two actresss names appear above the two images.
| caption = Theatrical release poster
| director = Tommy OHaver
| producer = Christine Vachon Jocelyn Hayes Henry Winterstern Kevin Turen Hans C. Ritter
| writer = Tommy OHaver Irene Turner
| starring = Ellen Page Catherine Keener Hayley McFarland Ari Graynor Scout Taylor-Compton
| music = Alan Ari Lazar
| cinematography = Byron Shah
| editing = Melissa Kent First Look Showtime
| released =  
| runtime = 98 minutes
| country = United States
| language = English
| budget = 
| gross = 
}} Indianapolis housewife Gertrude Baniszewski. It premiered at the 2007 Sundance Film Festival. 
 Showtime television network officially premiered An American Crime on May 10, 2008. 

The film was nominated for a Golden Globe, a Primetime Emmy (both for Keeners performance), and a Writers Guild of America Award.

==Plot== flashbacks to the events of the previous year described by the witnesses.

Sixteen-year-old   (Ari Graynor), who confesses to Sylvia that she is pregnant by her married boyfriend&mdash;Sylvia swears not to tell. Soon after the girls arrival, Gertrude is upset that Lesters weekly payment is late, and punishes the girls by whipping them with a belt. The payment, along with a letter from the parents, arrives shortly thereafter, but Gertrude throws the letter in the trash and says nothing to the Likens girls.

While Paula and Sylvia are out with friends, Paulas boyfriend tells her that he can no longer see her, then hits Paula when she pleads for him to stay with her. To stop the boyfriends attack, Sylvia tells him that Paula is pregnant. Paula, furious, vows that Sylvia will pay for revealing her secret. Ricky Hobbs (Evan Peters), a neighbor of the Baniszewskis who is attracted to Sylvia, observes the scene. Paula tearfully tells Gertrude that Sylvia has been spreading lies about her. Gertrude forces Sylvia to apologize, and has her son Johnny (Tristan Jarred) hold Sylvia so that Paula can beat her. Rumors spread around their school that Paula is pregnant. Jenny finds the letter from her parents in the garbage, prompting Sylvia to phone them. The call is cut short when they see the Baniszewski children, who tell their mother about it. Although the girls had traded bottles in for money to make the call, Gertrude believes they stole the money from her and burns Sylvias hand with a cigarette for "stealing".

At a church lunch, Andy (James Franco), the father of Gertrudes infant son, tells her that Sylvia has been spreading rumors that Paula is pregnant. When Sylvia returns home, Gertrude accuses her of flirting with a boy at the lunch and telling more lies about her daughter. Intending to make an example of her to her children, Gertrude forces Sylvia to insert a Coke bottle into her vagina in front of them. Gertrude then orders Johnny and Stephanies boyfriend, Coy Hubbard (Jeremy Sumpter), to throw her down the basement stairs, and lock her in. When Jenny asks how long Sylvia will be down there, Gertrude replies "until she learns her lesson", then tells everyone to maintain the fiction that Sylvia was sent to a juvenile detention facility.

Johnny begins to bring neighborhood children into the basement so they can join in the ongoing torture and humiliation of Sylvia begun by Gertrude and her children, assuring them that his mother said it was all right. The children visit regularly, with Gertrudes knowledge and approval, to beat Sylvia and burn her with cigarettes. Paula, feeling guilty, tells her mother she has been praying and thinks Sylvia has been punished enough, but Gertrude ignores her pleas. Their pastor visits the Baniszewski home, informing Gertrude that Paula told him she was pregnant and hinted to him about Sylvias abuse. Gertrude reassures him that Sylvia was sent away for being a bad influence, and as the pastor leaves, she orders everyone into the basement. Straddling Sylvia on the basement floor, Gertrude begins to carve the words "IM A PROSTITUTE AND PROUD OF IT!" on Sylvias abdomen with a hot needle, but is unable to continue and orders Ricky to finish the branding.

Late one night, Paula helps Sylvia escape by carrying her up the basement stairs and out of the house. Awakened by one of the Baniszewski girls, Gertrude tries to prevent the escape, but Paula stops her. Ricky finds Sylvia and drives her to her parents, who are horrified at their daughters condition. Sylvia and her parents go immediately to the Baniszewski household to make sure that Jenny is safe. Sylvia walks into the house and sees her own lifeless body lying on the floor with Stephanie leaning over it screaming that she is not breathing. As she stands watching this, Sylvia closes her eyes and vanishes, revealing her escape and return to be only a hallucination just before dying. While Gertrude continues to insist that Sylvia is faking, Ricky calls the police. As they arrive, Jenny approaches one of the officers, saying "Get me out of here and Ill tell you everything."

On the witness stand, Jenny tells the prosecutor that Sylvia never did anything to Gertrude, Paula, or anyone else, and adds that Gertrude had threatened her with the same treatment her sister received if she did not keep quiet about it. After denying that she did anything wrong, and blaming the torture and death of Sylvia on her children and their friends, Gertrude is found guilty of first degree murder and sentenced to life in prison. As Sylvias narration details the punishment of some of her abusers, Gertrude sits alone in her prison cell. In her imagination she sees a vision of Sylvia silently looking back at her. Gertrude tries to speak, but is unable to get the words out, and the image vanishes.

==Cast==
* Ellen Page as Sylvia Likens
* Catherine Keener as Gertrude Baniszewski
* Hayley McFarland as Jennifer Faye "Jennie" Likens Paula Baniszewski
* Scout Taylor-Compton as Stephanie Baniszewski
* Nick Searcy as Lester Likens
* Romy Rosemont as Betty Likens
* Brian Geraghty as Bradley Michael Welch as Teddy Lewis
* Evan Peters as Ricky Hobbs
* James Franco as Andy Gordon
* Jeremy Sumpter as Coy Hubbard
* Tristan Jarred as Johnny Baniszewski
* Hannah Leigh Dworkin as Shirley Baniszewski
* Carlie Westerman as Marie Baniszewski
* Bradley Whitford as Prosecutor Leroy K. New
* Michael OKeefe as Reverend Bill Collier

==Production==
Principal photography took place in 2006. Most of the cast were completely unaware of the real Likens murder until after they read the script, which was based largely on actual court transcripts from the case. Catherine Keener originally turned down the role of Gertrude Baniszewski; however, after she could not get the story out of her head, she met with director Tommy OHaver and agreed to do the film.    Ellen Page was the only choice to play Sylvia Likens.

==Critical reception==
The film received generally negative reviews, currently holding a 23% rating on Rotten Tomatoes, based on 13 reviews.    Ginia Bellafante of The New York Times called the film, "...one of the best television movies to appear in years" and praised Catherine Keeners portrayal of Gertrude Baniszewski. 

==Likens-based films== The Girl novel of the same name by Jack Ketchum) was also produced in 2007. Directed by Gregory Wilson, it stars Blanche Baker as the torturer and Blythe Auffarth as the tortured girl.  The film was released in December 2007 to mostly positive reviews.

==References==
 

==External links==
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 