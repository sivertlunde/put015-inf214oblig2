The Treasure of the Sierra Madre (film)
 
{{Infobox film
| name           = The Treasure of the Sierra Madre
| image          = Treasuremadre.jpg
| caption        = Theatrical poster
| director       = John Huston
| producer       = Henry Blanke
| screenplay     = John Huston
| based on       =  
| starring       = Humphrey Bogart Walter Huston Tim Holt Bruce Bennett
| music          = Max Steiner
| cinematography = Ted D. McCord
| editing        = Owen Marks
| distributor    = Warner Bros. Warner Bros.-First National Picture
| released       =  
| runtime        = 126 minutes
| country        = United States
| language       = English
| budget         = $3 million 
| gross          = $4,307,000  (rentals) 
}} American Drama dramatic adventure adventurous neo-western western written novel of the same name, about two financially desperate American people|Americans, Fred C. Dobbs (Humphrey Bogart) and Bob Curtin (Tim Holt), who in the 1920s join initially reluctant old-timer Howard (Walter Huston, the directors father) in Mexico to prospect for gold.  
 on location outside the United States (in the state of Durango and street scenes in Tampico, Mexico), although many scenes were filmed back in the studio and elsewhere in the US. The film is quite faithful to the source novel. In 1990, the film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".   

== Background ==
By the 1920s in Mexico the violence of the Mexican Revolution had largely subsided, although scattered gangs of bandits continued to terrorize the countryside. The newly established post-revolution government relied on the effective, but ruthless, Federal Police, commonly known as the Federales, to patrol remote areas and dispose of the bandits.
 bandits if their paths crossed. The bandits suffered a similar fate if captured by the Mexican Federales or army units. On-the-spot, bandidos were forced to dig their own graves and given a "last cigarette" before the death sentence was carried out.

== Plot == Sierra Madre mountains.

They ride a train into the hinterlands, surviving a bandit attack en route. In the desert, Howard proves to be the toughest and most knowledgeable; he is the one to discover the gold they seek. A mine is dug, and much gold is extracted. Greed soon sets in, and Dobbs begins to lose both his trust and his sanity, lusting to possess the entire treasure. Dobbs is also unreasonably afraid that he will be killed by his partners.

A fourth American named James Cody (Bruce Bennett) appears, which sets up a moral debate about what to do with the new stranger. The men decide to kill Cody, but just as the three confront him with pistols and prepare to kill him, the bandits reappear, crudely pretending to be Federal Police (Mexico)|Federales.  (This results in a now-famous exchange between Dobbs and the bandits about not needing to show any "stinking badges.")  After a gunfight with the bandits, in which Cody is killed, a real troop of Federales appears and chases the bandits away.

Howard is called away to assist local villagers to save the life of a seriously ill little boy.  When the boy recovers, the next day, the villagers insist that Howard return to the village to be honored.  However, he leaves his goods with Dobbs and Curtin.  Dobbs, whose paranoia continues, and Curtin constantly argue, until one night when Curtin falls asleep, Dobbs holds him at gunpoint, takes him behind the camp, shoots him, grabs all three shares of the gold, and leaves him for dead.  However, the wounded Curtin survives and manages to crawl away during the night.
 indios and taken to Howards village, where he recovers. The bandits try to sell the packing donkeys but a child recognizes the donkeys and Dobbs clothes and reports them to the police. The bandits are captured, sentenced to death and forced to dig their own graves before being executed. Curtin and Howard miss witnessing the bandits execution by Federales by only a few minutes as they arrive back in town, and learn that the gold is gone.
 indio village, where the natives have offered him a permanent home and position of honour, and Curtin returning home to the United States.

== Cast ==
 
* Humphrey Bogart as Fred C. Dobbs
* Walter Huston as Howard
* Tim Holt as Bob Curtin
* Bruce Bennett as James Cody
* Barton MacLane as Pat McCormick
* Alfonso Bedoya as Gold Hat
* Arturo Soto Rangel as El Presidente
* Manuel Dondé as El Jefe
* José Torvay as Pablo
* Margarito Luna as Pancho

==Production== Robert Blake also appears as a young boy selling lottery tickets. However, the most controversial cameo is the rumored one by Ann Sheridan. Sheridan allegedly did a cameo as a streetwalker. After Dobbs leaves the barbershop in Tampico (actually a set on a studio soundstage), he spies a passing prostitute who returns his look. Seconds later, the woman is picked up again by the camera, but this time in the distance. Some filmgoers and critics feel the woman looks nothing like Sheridan, but the DVD commentary for the film contains a statement that it is her. A photograph included in the documentary accompanying the DVD release shows Sheridan in streetwalker costume, with Bogart and Huston on the set. However, single frames of the film show a different woman in a different dress and different hairstyle, raising the possibility that Sheridan filmed the sequence but that it was reshot with another woman for undetermined reasons.  Many film-history sources credit Sheridan for the part.
 Jack Holt, a star of silent and early sound Westerns and action films, makes a one-line appearance at the beginning of the film as one of the men down on their luck.

Significant portions of the films dialog are in Spanish without sub-titles.

The opening scenes, filmed in longshot on the Plaza de la Libertad in Tampico, show modern (i.e. of the 1940s) cars and buses, even though the story opens in 1925, as evidenced by the lottery numbers poster.

==Themes==
The film is often described as a story about the corrupting influence of greed.   Film critic Roger Ebert enlarged upon this idea, saying that "The movie has never really been about gold but about character."   In addition, reviewers have noted the importance not just of greed and gold, but also of nature and its desolateness as an influence on the actions of the men.  However, the ability of the film to comment on human nature generally has been questioned, in view of the fact that Dobbs character is so evidently flawed from the beginning. 

== Quotation ==

 

The film is the origin of a famous line, often misquoted as "We dont need no stinking badges!" (homaged in Mel Brooks Blazing Saddles, also a Warner Bros. film). The correct dialogue is:

: Gold Hat (Alfonso Bedoya): "We are Federales... you know, the mounted police."
: Dobbs (Bogart): "If youre the police, where are your badges?"
: Gold Hat (Bedoya): "Badges? We aint got no badges. We dont need no badges! I dont have to show you any stinkin badges!"

In 2005, the quotation was chosen as No. 36 on the American Film Institute list, AFIs 100 Years...100 Movie Quotes.

== Awards and honors == Best Picture award, but lost to Laurence Oliviers film adaptation of Hamlet (1948 film)|Hamlet.

In 1990, this film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". The film was among the first 100 films to be selected.   

Director Stanley Kubrick listed The Treasure of the Sierra Madre as his 4th favorite film of all time in his list of his top ten favorite films in a 1963 edition of Cinema magazine.  Director Sam Raimi ranked it as his favorite film of all time in an interview with Rotten Tomatoes and director Paul Thomas Anderson watched it at night before bed while writing his film There Will Be Blood. {{cite news 

| url = http://www.nytimes.com/2007/11/11/magazine/11daylewis-t2.html?_r=1&oref=slogin
| title = The New Frontiers Man
| author = Lynn Hirschberg
|work=The New York Times
| accessdate = November 10, 2007
| date=November 11, 2007
}} 

; American Film Institute recognition
*AFIs 100 Years... 100 Movies – No. 30
*AFIs 100 Years... 100 Thrills – No. 67
*AFIs 100 Years... 100 Heroes and Villains:
**Fred C. Dobbs – Nominated Villain 
*AFIs 100 Years... 100 Movie Quotes:
**"Badges? We aint got no badges! We dont need no badges! I dont have to show you any stinking badges!" – No. 36
*AFIs 100 Years of Film Scores – Nominated 
*AFIs 100 Years... 100 Movies (10th Anniversary Edition) – No. 38

Breaking Bad creator Vince Gilligan has also cited the film as one of his personal favorites. A key scene from the film was emulated in "Buyout", the sixth episode of the fifth season of Breaking Bad.

== References ==
 

;Bibliography
* 

== External links ==
 
 
*  
*  
*  
*  
*  
*  on Lux Radio Theater: April 18, 1949 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 