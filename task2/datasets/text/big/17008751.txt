Amelia (film)
 
{{Infobox film
| name           = Amelia
| image          = Ameliaposter09.jpg
| caption        = Theatrical release poster
| alt            = Hilary Swank as Amelia Earhart standing alone on the runway with her back turned wearing a flight suit and an aircraft filling the background
| director       = Mira Nair
| producer       = Ted Waitt  Kevin Hyman  Lydia Dean Pilcher
| writer         = Ronald Bass  Anna Hamilton Phelan
| based on       = East to the Dawn by Susan Butler and The Sound of Wings by Mary S. Lovell
| starring       = Hilary Swank  Richard Gere  Ewan McGregor  Christopher Eccleston
| music          = Gabriel Yared
| cinematography = Stuart Dryburgh
| editing        = Allyson C. Johnson Lee Percy 2S Films Avalon Pictures AE Electra Productions
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 111 minutes  
| country        = United States  Canada
| language       = English
| budget         = $40 million
| gross          = $19,642,013 	 
}}

Amelia is a 2009  . Siegel, Tatiana.    Variety, May 26, 2008. Retrieved: October 8, 2008.  The film was written by Ronald Bass and Anna Hamilton Phelan, using research from sources including East to the Dawn by Susan Butler and The Sound of Wings by Mary S. Lovell. Fleming, Michael.   Variety, February 7, 2008. Retrieved: October 8, 2008.  The film has garnered predominantly negative reviews.

==Plot== George Putnam (Richard Gere) to become the first woman to cross the Atlantic Ocean, albeit as a passenger. Taking command of the flight results in a success and she is thrust into the limelight as the most famous woman pilot of her time. Putnam helps Earhart write a book chronicling the flight, much like his earlier triumph with Charles Lindberghs We. Earhart gradually falls in love with Putnam and they eventually marry, although she enacts a "cruel" pledge as her wedding contract.
 Federal Aviation administrator Gene Vidal (Ewan McGregor). In a display of romantic jealousy, Putnam quietly tells Amelia that he does not want Vidal in his house. Earhart is annoyed by the seemingly endless agenda of celebrity appearances and endorsements but Putnam reminds his wife that it funds her flying. Earhart returns to her husband on the eve of her last momentous flight. Earharts last flight was her biggest and most dangerous adventure to date. Her plan was to fly around the world. Earharts first attempt ends in a runway crash in Hawaii, due to collapsed landing gear. Earhart shuts off the fuel supply but her aircraft requires extensive repairs before the flight can be attempted again. Eventually, she takes the repaired Lockheed Model 10 Electra Purdue University|"Flying Laboratory" in a reverse direction, leaving the lengthy transpacific crossing at the end of her flight.

Setting out to refuel at tiny Howland Island, radio transmissions between USCGC  , a Coast Guard picket ship, and Earharts aircraft reveal a rising crisis. Earhart radios to Itasca that the sky has become cloudy and overcast. When Itasca attempts to radio her back, however, all Earhart gets is static. For the rest of the approach, Earhart cannot hear Itascas transmissions, although they can hear hers. The Coast Guard radio operators realize that they do not have sufficient length to provide a "fix". Itasca has a directional finder with a dead battery, and weak radio communications prevent Earhart and USCG Itasca from making contact.  Running low on fuel, Earhart and Noonan continue to fly on over empty ocean, as Earhart informs the Itasca that she is on position line 157-337, running north and south. She is not heard from again. A massive search effort is unsuccessful, but solidifies Earhart as an aviation icon.

==Cast==
 
* Hilary Swank as Amelia Earhart
* Richard Gere as George P. Putnam Gene Vidal
* Christopher Eccleston as Fred Noonan Joe Anderson as Bill Stutz
* William Cuddy as  Gore Vidal
* Mia Wasikowska as Elinor Smith
* Cherry Jones as Eleanor Roosevelt
* Divine Brown as the "Torch singer". 
* Ron Smerczak as Interviewer
 
 

==Production== CGI effects Fokker F.VIIb/3m Tri-motor Friendship (with limited ability to run up engines and taxi). 
The Lockheed 12A Electra Junior "Hazy Lily" (F-AZLL) used alongside another Electra Junior, filled in for the much rarer Lockheed Electra 10E that Earhart used.  Despite the efforts to faithfully replicate the period, numerous historical inaccuracies were evident, as chronicled in some reviews. Gillespie, Ric.   tighar.org, October 23, 2009. Retrieved: October 24, 2009. 
 reenactor Kathie Brosemer recounting the story of Earharts flight in 1928. 

===Writing=== Gateway founder Ted Waitt, who has funded expeditions to search for Earharts aircraft, and was prepared to finance the film himself. Thompson, Anne.   Thompson on Hollywood, October 23, 2009. Retrieved: December 13, 2011.  Bass used research from books on Earhart such as biographies by Susan Butler, East to the Dawn and Mary S. Lovells  The Sound of the Wings as well as Elgen and Mary Longs Amelia Earhart: The Mystery Solved.  Although the film was not intended to be a documentary, Bass incorporated many of Earharts actual words into key scenes.  Oscar-nominated screenwriter Anna Hamilton Phelan did a re-write, taking a different approach from the original screenplay. 

==Reception==
===Critical response===
Amelia received negative reviews from film critics, with a 20% "rotten" rating on the Rotten Tomatoes website based on 159 reviews with an average score of 4.4/10.  Another review aggretator, Metacritic, which assigns rating of 100 reviews from mainstream critics, gave the film a score of 37 based on 34 reviews. 

Echoing the majority view, Martin Morrows review on the   wrote, "The actors don’t make a persuasive fit, despite all their long stares and infernal smiling. ...the movie is a more effective testament to the triumphs of American dentistry than to Earhart or aviation."  Ric Gillespie, author of Finding Amelia, wrote that "Swank, under Nair’s direction, accomplishes the amazing feat of making one of the most complex, passionate, ferociously ambitious, and successful women of the 20th century seem shallow, weepy, and rather dull." 

A small number of positive reviews included Ray Bennett of the   of the  , October 21, 2009. Retrieved: October 25, 2009.  In pre-release publicity, Hilary Swank had been touted as a candidate for a third Oscar, but later that prospect was viewed as distant. Coles 2009, p. 172.  Carrie Rickey of the Philadelphia Inquirer, however, awarded the film 3 stars, praising Swanks performance in her review stating that "like Maggie in Million Dollar Baby,   is unwavering in her gaze, ambition, and drive," and "in Nairs evocatively art-directed (and sensationally costumed) film, Earhart comes alive." 

==Home media release==
On February 2, 2010, Fox Home Entertainment released Amelia in DVD and Blu-ray versions. Extras on the DVD include deleted scenes and "The Power of Amelia Earhart", "Making Amelia" and "Movietone News" featurettes. The Blu-ray release also has two additional featurettes: "The Plane Behind the Legend" and "Re-constructing the Planes of Amelia" along with a digital copy of the film. 

==References==
Notes
 

Citations
 

Bibliography
 
* Butler, Susan. East to the Dawn: The Life of Amelia Earhart. Reading, Massachusetts: Addison-Wesley, 1997. ISBN 0-306-80887-0.
* Coles, Joanna. " Hilary Swank is Ready for Takeoff." Marie Claire, November 2009.
* Goldstein, Donald M. and Katherine V. Dillon. Amelia: The Centennial Biography of an Aviation Pioneer. Washington, D.C.: Brasseys, 1997. ISBN 1-57488-134-5.
* Long, Elgen M. and Marie K. Amelia Earhart: The Mystery Solved. New York: Simon & Schuster, 1999. ISBN 0-684-86005-8.
* Lovell, Mary S. The Sound of Wings. New York: St. Martins Press, 1989. ISBN 0-312-03431-8.
* OLeary, Michael, ed. "Amelia on the Silver Screen."  Air Classics, Volume 45, No. 11, November 2009.
* Rich, Doris L. Amelia Earhart: A Biography. Washington, DC: Smithsonian Institution Press, 1989. ISBN 1-56098-725-1.
* Zohn, Patricia. "Oh So Swank." Town and Country, October 2009.
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 