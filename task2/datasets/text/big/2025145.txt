The Revolution Will Not Be Televised (film)
 
 
 
{{Infobox film
| name           = The Revolution Will Not Be Televised
| image          = The Revolution will not be Televised.gif
| alt            =
| caption        = Theatrical release poster
| director       = Kim Bartley  Donnacha Ó Briain
| producer       = David Power
| narrator       = Donnacha Ó Briain
| starring       = Hugo Chávez Pedro Carmona Jesse Helms Colin Powell George Tenet
| cinematography = Kim Bartley  Donnacha Ó&nbsp;Briain
| editing        = Ángel Hernández Zoido
| studio         = Power Pictures Vitagraph Films (US)
| released       =  
| runtime        = Chavez: Inside the Coup 52 minutes The Revolution Will Not Be Televised 74 minutes
| country        = Ireland
| language       = English Spanish
| budget         = €200,000
| gross          = $200,000 (€171,000) 
}}

The Revolution Will Not Be Televised (Spanish:  ), also known as Chávez: Inside the Coup, is a 2003 documentary focusing on events in  ; and the Carmona administrations collapse, which paved the way for Chávezs return. The Revolution Will Not Be Televised was directed by Irish filmmakers Kim Bartley and Donnacha Ó&nbsp;Briain. Given direct access to Chávez, the filmmakers intended to make a Reality television#Documentary-style|fly-on-the-wall biography of the president. They spent seven months filming in Venezuela, following Chávez and his staff and interviewing ordinary citizens. As the coup unfolded on 11&nbsp;April, Bartley and Ó&nbsp;Briain filmed on the streets of the capital, Caracas, capturing footage of protesters and the erupting violence. Later, they filmed many of the political upheavals inside Miraflores Palace|Miraflores, the presidential palace.

Bartley and Ó&nbsp;Briain conceived of the film after Bartley returned from documenting the aftermath of the 1999 Vargas mudslides for an Irish charity. Following a visit to Venezuela to determine the feasibility of a film project, the pair formed a production company and applied to Irelands film board, Bord Scannán na hÉireann (BSÉ), for a development grant. At BSÉs request, the filmmakers partnered with a more experienced producer and shot a short pilot to show to potential investors. Funding for the €200,000 production was provided by BSÉ and several European broadcasters. Bartley and Ó&nbsp;Briain shot more than 200&nbsp;hours of material; editing focused on identifying footage that would make the film entertaining and drive the plot. It was at this stage that the films coverage narrowed to concentrate more on the coup attempt.

The film was positively received by mainstream film critics and won several awards. Reviewers cited the filmmakers unprecedented proximity to key events and praised the film for its "riveting narrative"; Schiller (2009), p. 488.  criticism focused on its lack of context and pro-Chávez bias. First shown on television in Europe and Venezuela in 2003, The Revolution Will Not Be Televised later appeared at film festivals and secured a limited theatrical release on the art house circuit. Independent activists held unofficial screenings, and Venezuelan government officials encouraged its circulation to build support for Chávezs administration. The film is regularly shown on Venezuelan television, and in the capital it is often broadcast during "contentious political conjunctures".  The Revolution Will Not Be Televised paints Chávez in a favorable light, which has led to disputes over its neutrality and accuracy; particular attention is paid to its framing of the violence of 11–13&nbsp;April, the filmmakers editing of the timeline, and the alleged omission of incidents and personnel. The film is variously cited as an accurate portrayal or a misrepresentation of the events of April 2002.

==Background==
 
Throughout much of the twentieth century, Venezuela was beset by political, civil and military unrest. After Juan Vicente Gómezs long reign as president ended in 1935, a series of military rulers followed, concluding with Marcos Pérez Jiménezs overthrow by general uprising in 1958. Although the military remained influential, Venezuelas government has since been chosen by civilians through democratic processes. Stoneman (2008), p. 5.  Until 1998, the dominant political parties were Acción Democrática and COPEI, who shared seven presidencies between them. In 1989, during the second term in office for Acción Democráticas Carlos Andrés Pérez, Venezuela was hit by a severe economic crisis. A wave of protests known as the Caracazo engulfed the country and dozens were killed in rioting. Stoneman (2008), p. 6. 

  Lieutenant Colonel unsuccessful military socialist political struggle for 1998 presidential election, Chávez won 56.2% of the vote, on a promise to "end the corruption of several decades" and institute a new Bolivarian Revolution that he felt would secure Latin Americas true independence from the outside world. 

Chávez strengthened his support among the poor with a series of social initiatives known as the Bolivarian Missions, and created a network of grass-roots workers councils, the Bolivarian Circles.  Nevertheless, by early 2002, Venezuela was "embroiled&nbsp;... in a severe political crisis" as Chávez sought to bring more of the countrys vast oil wealth under state control.  Although the state-owned radio and television stations remained staunch advocates of Chávezs stated policies—to redistribute the nations wealth to the poorest—the private media was more hostile.  The crisis reached a head when Chávez attempted to remove the management of the state oil company, Petróleos de Venezuela (PDVSA), provoking a showdown. "Oil managers, business leaders, and large segments of organized labor" called a general strike.  The strike was backed by a large segment of the population, "particularly the countrys increasingly impoverished middle class" and army officers upset at the increasing politicization of the military. 

On 11&nbsp;April 2002, hundreds of thousands of people marched in protest against the government. Abandoning their planned route, the marchers advanced towards the palace, a path that took them close to government supporters who had come out in opposition to the protest. Journalist Phil Gunson wrote, "Shooting broke out on all sides.  A score of civilians died and more than 150 suffered gunshot wounds. The military high command called for Chávez to resign, and at 3:20 the next morning they announced he had agreed to do so. The presidency was assumed by a business leader, Pedro Carmona, but his government collapsed in less than forty-eight hours and Chávez returned to power." 

==Synopsis==
The Revolution Will Not Be Televised opens in 2001  McKay, Alastair (Winter 2008). "The Revolution Will Not Be Televised, But The Coup Attempt May Be Sexed Up". Product Magazine (Red Herring Arts and Media): 10.  ism and the international communitys attacks on his character.  The film outlines Chávezs rise to power, before covering his day-to-day routine and appearances on his television show, Aló Presidente, which includes a phone-in for citizens to speak with the president.  Chávez outlines his aspiration to be seen as a modern-day Bolívar.  Clips from Venezuelan and United States news reports demonstrate a "relentless campaign" against the president. Stoneman (2008), p. 30. 
 the State Department express concern about Chávezs rule and stress the importance of Venezuelas oil. A Venezuelan general appears on private television to voice similar disquiet. Carmona appeals for a public protest at the offices of PDVSA. Bartley & Ó&nbsp;Briain (2003), chapter 7. 

On 11&nbsp;April, opposition protesters begin their march outside PDVSAs headquarters in Caracas; Chávezs supporters gather outside the presidential palace. The protest route is changed to take it to the palace; shots ring out and civilians are killed. The private media blames Chávezs supporters, citing footage that shows them shooting at opposition protesters from a bridge.  The narration states, "What the TV stations didn’t broadcast was  , which clearly shows that the streets below were empty. The opposition march had never taken that route." Bartley & Ó&nbsp;Briain (2003), chapter 8.  Later, the state television signal is cut; rumors circulate that the opposition has taken over the studio. Bartley & Ó&nbsp;Briain (2003), chapter 10.  At the palace, members of the military high command demand Chávezs resignation, threatening to bomb the building. The president refuses to resign, but submits to their custody.  He is led away, and Carmona announces on television that a transitional government will be established. 

On 12&nbsp;April, opposition leaders appear on private television, where they disclose their plan to unseat Chávez.  Carmona is sworn in as president while images play of unrest on the streets. Defying media censorship, Chávezs supporters disseminate the story that the president did not resign. Bartley & Ó&nbsp;Briain (2003), chapter 11.  On 13&nbsp;April, they gather to protest outside Miraflores, Stoneman (2008), p. 31.  while palace guards plot to retake the building. The guards take up key positions and, at a prearranged signal, take members of the new government prisoner. Bartley & Ó&nbsp;Briain (2003), chapter 12.  The state television channel is relaunched and urges the army to back Chávez. Bartley & Ó&nbsp;Briain (2003), chapter 13.  "Full military control" is returned to the Chávez administration and the president arrives at the palace amid celebratory scenes.  Chávez makes an address in which he says it is fine to oppose him, but not the Constitution of Venezuela. The closing titles say Carmona fled to Miami while under house arrest, and that Ortega went into hiding, only reappearing to help lead the opposition after Chávez said there would be no repercussions. Most of the dissident generals, after being expelled from the army, fled to the US. Others remained as part of the opposition. Bartley & Ó&nbsp;Briain (2003), chapter 15. 

==Production==

===Development=== Bord Scannán na hÉireann (BSÉ), for a development grant. Stoneman (2008), p. 12.  Bartley and Ó&nbsp;Briain proposed a fly-on-the-wall documentary,  a "personal profile and intimate portrait" of Chávez  that would be "broadly supportive" of him. Stoneman (2008), p. 13.  During their 2000 visit, the filmmakers had sensed that "something genuinely was happening" in Caracas,  and felt an urgency to get the project underway; even so, it wasnt until April 2001 that BSÉ approved the £6000 (€9500) grant. 

The project was at this point named Aló Presidente, a working title taken from Chávezs weekly television and radio program.  BSÉ set about exploring avenues of funding;  the organization persuaded Bartley and Ó&nbsp;Briain to make a short pilot to show to potential investors, Stoneman (2008), p. 15.  but refused the filmmakers application for a €60,000 grant towards their €131,000 production budget. BSÉ felt the pair needed to partner with a production company that had experience in the field, and which could help raise the remaining funds. Bartley and Ó&nbsp;Briain approached Power Pictures and, with the addition of David Power as a producer, reapplied for the grant.  Even as filming began, the full budget—now at €200,000 —had not been secured. David Power pitched the project at several documentary festivals and markets. At Dublins Stranger Than Fiction festival in September 2001, the British Broadcasting Corporation|BBC, S4C and Channel 4 declined to invest. Raidió Teilifís Éireann (RTÉ) expressed interest in providing development funds; no such deal was made, but RTÉ did offer €10,000 (subsequently €20,000) for the Irish broadcast rights. In October, the Dutch broadcaster Nederlandse Programma Stichting also committed €10,000. At a November market in Amsterdam, Power once again approached the BBC and was turned down, as the organization believed the films subject was "too far away to be relevant to   lives".  However, RTÉs Kevin Dawson pushed the film at a European Broadcasting Union pitching session, securing the interest of German television channel ZDF, which subsequently provided funds. Stoneman (2008), p. 16.  In late 2001, BSÉ finally approved a production grant of €63,000. Stoneman (2008), p. 17. 

===Filming===
 
In 2000, Bartley and Ó&nbsp;Briain had been promised "exclusive access" to Chávez by the governments Minister of Communications.  They arrived in Venezuela in September 2001.  While filming the pilot they met the president,  after which they reconsidered their approach.  Bartley explained, "We had&nbsp;... this notion of investigating Chávez—was he a demagogue? Was the media persona just that? What makes him tick? My sense had changed as we got closer; what were seeing here is a guy who is motivated, driven, not the demagogue with another side, drinking, carousing. I began to see him as more transparent—what you see is what you get."  Bartley and Ó&nbsp;Briain began by attempting to build a relationship with Chávez that would allow them the access they required. At first, the presidents staff treated the filmmakers with suspicion and made filming difficult. After numerous delays, Bartley and Ó&nbsp;Briain finally got through to Chávez. They calculated that they needed to "press the right buttons" to gain his support, so they presented  him with an old edition of the memoirs of Daniel Florence OLeary, who had fought alongside Simón Bolívar.  Inside, they had written a quote from the Irish socialist playwright Seán OCasey.  Slowly, Bartley and Ó&nbsp;Briain gained their subjects trust, "dissolving any self-consciousness as a result of their cameras". Stoneman (2008), p. 20. 

Although ensconced with Chávez and his entourage, Bartley and Ó&nbsp;Briain felt a disconnect from the events of the outside world. During a "chaotic" road-trip with Chávez, they "knew something was coming", and divined that Chávezs trip was intended to bolster his support and "get people used to being on the streets".  Chávez had recently "upped the ante" with the introduction of the Land Law.   Increasing tensions further, in February 2002, Chávez took control of PDVSA;  the private media stepped up its criticism of Chávez, which for Bartley and Ó&nbsp;Briain "marked the beginning of an exciting phase".  By April 2002, Bartley and Ó&nbsp;Briain were in Caracas and spent much of their time filming at   was closed—the filmmakers instead took to the streets, "to document the repression   were witnessing".  At the same time, a press office cameraman was in the palace, "reluctant to lose his job despite the change in government".  He filmed the formation of the interim government. When Bartley and Ó&nbsp;Briain returned to the palace on 13&nbsp;April, the cameraman let them have his footage.  They remained filming in Venezuela until July 2002, interviewing residents Stoneman (2008), p. 22.  and recording "witness" accounts from those who had been present during the coup—ministers, security guards and journalists. Stoneman (2008), p. 26. 

===Editing===
{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; color:black; width:40em; max-width: 35%;" cellspacing="5"
|style="text-align: left;"|"While Im editing a film, I never forget that it is entertainment&nbsp;... Something that people are going to pay for to watch and thus make them think, laugh, or learn things they didnt know before. So you must never ever let them get bored.&nbsp;... There are always hundreds of stories sleeping inside the material and you have to find them and wake them up. But you have to organise them in a way that they become entertainment."
|-
|style="text-align: left;"|—Editor Ángel Hernández Zoido 
|}
Using two   with Bartley and Ó&nbsp;Briain, Zoido asked them, "What do we want to tell in this scene?"  He focused on two factors: how much information would be necessary to drive the plot, and provide an emotional core. Preferring to work alone, Zoido would then send the filmmakers from the edit suite. After Zoido completed the scene, the trio discussed whether those aims had been realized. Stoneman (2008), p. 24. 

The large amount of footage, and the fact that the filmmakers were "in a sense&nbsp;... looking at it for the first time", meant that the films concept only became clear during editing.  The events of 11–13&nbsp;April gave it "a new dramatic centre",  although Bartley and Ó&nbsp;Briain were keen that the film did not concentrate entirely on the coup. They had intended to include more historical and political context;  it wasnt until late in post-production that the focus narrowed  to include more emphasis on the medias role in Venezuelan politics.  In October 2002, a two-hour "rough cut" was shown to Stoneman and Brendan McCarthy, BSÉs head of Production and Development.  This version included the "witness" accounts that Bartley and Ó&nbsp;Briain had captured after April 2002. Stoneman felt that these sequences reinforced the films claims, but "diluted its originality".  He argued that they be cut, and that to compensate, Bartley and Ó&nbsp;Briain should record a voice-over and place themselves more in the frame as witness-protagonists, ideas that the filmmakers initially resisted. 

As editing progressed, budget shortfalls prevented BSÉ from finalizing contracts. The organization also restricted post-production funds to "limit   exposure" in the event the project was not completed.  Stoneman contacted a former colleague at the BBC, Nick Fraser, Stoneman (2008), p. 27.  who had declined to help finance the film in 2001.  Fraser was commissioning editor for the BBCs Storyville (television series)|Storyville documentaries series. According to Stoneman, Fraser was "still undecided", having been told by the head of BBC Two that "weve done Chávez".  Nevertheless, the BBC pre-purchased the film and in December 2002 received a rough cut.  At the BBCs behest, the opening of the television version was made more dramatic. Fraser was unsure about the voice-over; he asked for a more "opinionated" narration that, according to Bartley, would "get the boot in".  Although the BBC did not ask outright, Bartleys impression was that the organization wanted the film to be "against Chávez".  Fraser later said he had asked for the filmmakers to "include   with someone not a Chávez supporter".  He also suggested that a more experienced director be employed to help edit the film. The filmmakers "made a few minor concessions", but resisted major edits.  Ó&nbsp;Briains voice-over in the final cut was "polished up but not significantly changed". 
 of the same name —is 74&nbsp;minutes long. 

==Release==

===Television=== El Universal. 2004 recall 2006 presidential election, and in 2007 to "help build support" for the governments controversial attempt to not renew the license of private television network RCTV. 

===Festivals and theatrical run===
  in November 2003. ]]
The Revolution Will Not Be Televised screened at several film festivals in 2003, winning numerous awards.  Beginning with the  , the Galway Film Fleadh and the Los Angeles Wine & Country Festival.  In March 2003, a VHS copy of the film screened to under 100 people as part of an American Cinematheque Irish film festival in Los Angeles. Among the viewers was the president of Vitagraph Films, David Schultz, who bought the rights for theatrical distribution and paid for the film to be converted from video. Schultz initially struggled to secure the support of exhibitors; they were skeptical of the films commercial prospects, and believed "the environment was not hospitable" for a film critical of the US so close to the start of the 2003 invasion of Iraq.  They only became receptive a few months later, when political perceptions shifted and the public became more aware of Venezuela because of its oil wealth. Schiller (2009), pp. 486–487.  One such exhibitor was the Film Forum in New York City. Mike Maggiore, a programmer at the theater, worked to market the film and raise its profile with film critics. He created press kits and circulated information to appeal to "a particular audience". 

The Revolution Will Not Be Televised premièred to the public at the Film Forum in November 2003. The showing was accompanied by protests outside the theater from supporters and detractors of the film, both of whom "attempted to influence audience reception". Schiller (2009), pp. 488–489.  A few weeks previously, the film had been withdrawn from an  .  The films success at the 2003 Grierson Awards was also overshadowed by a letter to the Grierson jury from London-based Venezuelan filmmakers, who disputed its version of events.  Opposition demonstrators at the Film Forum première attempted to throw doubt on the films "impartiality, precision, veracity, editorial integrity, and ideological independence", while supporters "encouraged theatergoers to denounce censorship" and sign a petition.  Opposition protests also greeted showings in Canada, Australia and France.  The run at the Film Forum earned $26,495 (€22,600)—several thousand above Maggiores expectations.  After a limited run in theaters in six cities,  the film had earned over $200,000 (€171,000),   not quite profitable, but still considered "a significant sum for a documentary". 

===Informal distribution=== Bolivarian Circle El Universal Lincoln Center, where 250 people paid $35 (€30) each to see the film and take part in a "question-and-answer session" with guests such as Leonor Granado, the Venezuelan Consul General.  The consulate office made DVDs of the film available to "anyone who wanted a copy", as Granado said the film was vital to "building support in   for the Venezuelan government". 

Journalist Michael McCaughan invited a group of people who held anti-Chávez views to a screening of the film. He said some among the audience changed their opinion of Chávez after seeing it, although many remained hostile. McCaughan said the consensus opinion was that the film was  excellent and reasonably objective", but that "Chávez remained a dictator leading the country to a totalitarian grave". Stoneman (2008), p. 37.  As of 2006, groups such as Global Exchange were arranging tours to Venezuela that included a screening of the film. 

==Analysis==

===Disputed accuracy===
In Venezuela, debate about The Revolution Will Not Be Televised is "often acrimonious".  The film has become key to framing peoples understanding of the events of April 2002.  The previously accepted international view was that Chávezs ousting came from a "spontaneous popular response" to the repression of his regime; the film "directly contradicts" this position,  and since its release it has rapidly become "the prevailing interpretation of  ". Gunson, Phil (May–June 2004). "Directors cut: did an acclaimed documentary about the 2002 coup in Venezuela tell the whole story?".  , says that most of the film critics who embraced the film ignored "the complex, messy reality" of the situation.  He charges that the filmmakers "omit key facts, invent others, twist the sequence of events to support their case, and replace inconvenient images with others dredged from archives".  Bartley and Ó&nbsp;Briain argue that Gunsons points are "issues of dispute" that "continue to divide opinion" in Venezuela. Bartley, Kim; Ó Briain, Donnacha (May–June 2004). "Whos Right? The Filmmakers Respond". Columbia Journalism Review 43 (1): 62–63.  Author Brian A. Nelson says that Bartley and Ó&nbsp;Briain—in their initial meeting with Chávez—did more than merely invoke Daniel OLeary to gain the presidents support for filming; Nelson alleges that they offered to portray the president positively in return for open access, with a "you scratch my back if I scratch yours" understanding that he says was ultimately reflected in the films "unabashed pro-Chavismo." Nelson (2009), p. 337. 

===BBC and Ofcom investigations===
{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; color:black; width:40em; max-width: 35%;" cellspacing="5"
|style="text-align: left;"|"Controversy and contention began to build around  , with a high level of complaints and responses reaching organisers, distributors and curators, culminating in the publication of articles, a petition and formal complaints.&nbsp;... From the point of view of those who opposed Chávez, The Revolution Will Not Be Televised constitutes the main weapon of the Venezuelan government to disseminate internationally a biased, manipulated and lying version of what happens  . 
|-
|style="text-align: left;"|—Rod Stoneman, former CEO of BSÉ 
|}
Soon after the films October 2003 broadcast on BBC Two, Venezuelan filmmaker Wolfgang Schalk began a campaign against The Revolution Will Not Be Televised, representing El Gusano de Luz ("The Worm of Light"), an organization associated with the Venezuelan opposition. In July, Schalk had complained to RTÉ about its broadcast of the film.  On 21&nbsp;October, El Gusano de Luz published a "detailed critique" as part of an internet petition Stoneman (2008), p. 47.  that attracted 11,000 signatories, 85% of whom self-identified as Venezuelan. Stoneman (2008), pp. 39–40.  Directed at the European broadcasters that financed and aired the film,  the petition said in part, "The Revolution Will Not Be Televised is being presented as an authors film, as an objective journalistic research film, while it is really a very well plotted and accomplished propaganda operation, supported logistically by the Venezuelan government, with the aim of misleading unprepared spectators of countries who do not know the totality of events."  The petition submitted 18 specific points of contention with the film.  Venezuelan private television soon aired two programs "dissecting and denouncing" the film, and similar newspaper articles followed. Stoneman (2008), p. 38. 
 David Kelly; the BBC had been criticized for reporting that intelligence dossiers had been "sexed up" by the UK government to justify the 2003 invasion of Iraq. 
 West Wing—biased, of course, but highly entertaining." 

===Responsibility for violence===
One of the films key contentions is that the private media aired footage selectively to make it look like the violence of 11&nbsp;April was caused by Chávezs supporters, portraying them as an "irrational and uncivilized mob". Schiller (2009), p. 479.  Private television repeatedly showed Chávezs supporters on Puente Llaguno bridge as they shot at Baralt Avenue below, an area purportedly full of opposition marchers.  The film says this footage was edited to show the gunmen but not the people near them who were ducking to avoid being shot. It follows with images taken from above the bridge showing an empty Baralt Avenue, claiming that "the opposition march had never taken that route" and that Chávezs supporters were only returning fire.  Gunson charges that this edit is itself a misrepresentation, stating that the film does not mention that both sets of marchers were fired upon, and taking issue with the implication that "coup plotters" were the shooters.  In response, the filmmakers say, "Nowhere in the film did we say that only   were shot&nbsp;... Nobody can say with certainty who orchestrated the shootings."  Gunson also asserts that the footage of the empty street was taken earlier that day, citing an "analysis of the shadows" by Schalk,  who created a counter-documentary,  , also supported Bartley and Ó&nbsp;Briains view. 

===Timeline and media depictions===
Other issues of contention include the lack of historical context; the film does not cover some of the events leading up to Chávezs ousting, including the long-running political crisis and the general strike. Gunson also criticizes the filmmakers for showing events out of order. In June 2002, they filmed an opposition community group as its members considered "how to defend themselves against possible&nbsp;... attacks" from Chávezs supporters.  In the film, this sequence is placed before the march. Bartley justified the action, saying that the residents opinions were representative of those held "long before" the events of April 2002.  Responding to the critique, the BBC added a date stamp to the sequence for the films repeat broadcast.  Gunson also cites footage of Caracas mayor Freddy Bernal as he sings to a happy group of Chávez supporters in front of the palace. Later images of a "differently dressed Bernal" reveal that the footage was from another day.  Similarly, Gunson says that until shot at, "The opposition march was entirely peaceful."  The film presents footage of its "violent finale"—including an image from another day—as if it occurred during the protests approach to the palace, accompanied by the narrated claim that "some in the vanguard looked ready for a fight".  Bartley and Ó&nbsp;Briain admit that they included a "limited" amount of archive footage,  but say it was a "legitimate reconstruction"  to build context "before the core narrative of the coup   off" as they "could not be everywhere filming at all times". 

The Revolution Will Not Be Televised claims that state television was "the only channel to which   had access", but does not mention that during the violence he requisitioned "all radio and TV frequencies" to broadcast his two-hour address.  Private television circumvented the rules allowing this action by splitting the screen, showing Chávezs address on one side and footage of the violence on the other.  Chávez subsequently took television stations   corroborated their claim that opposition forces took over VTV.  The film also presents footage of armored vehicles around the palace, which Gunson says were there at the request of the president, not the opposition. He also challenges the film for presenting Chávezs supporters as "invariably poor, brown-skinned, and cheerful" and the opposition as "rich, white, racist, and violent".  He says that the opposition protests were multiracial and that armed government supporters "made the center of Caracas a no-go area".  Bartley and Ó&nbsp;Briain cite several commentators who uphold the claim that Chávezs supporters "were broadly poor and dark-skinned and the opposition broadly white and middle class", including Gunson himself in an April 2002 article in The Christian Science Monitor.  Gunson does agree that the film was right to point out that the private media "behaved disgracefully" by "systematically   viewpoint from print, radio, and TV" during the period of the coup. 

===Military involvement=== Lucas Rincón (who announced Chávezs resignation on television), was not part of the coup and remained in the government after April 2002.  The petition draws the conclusion, "(1) either General Rincón stated a truth that was accepted throughout the whole country&nbsp;... or (2) General Rincón lied, because he was an accomplice&nbsp;... that seems not to be the truth because he  ." Stoneman (2008), p. 56.  Only one of the high command joined Carmonas interim administration before contributing to its downfall by withdrawing his support. The military leaders shown withdrawing their support for Chávez were not the high command, and Vice-Admiral Hector Ramirez Perez was not the head of the navy, as the film claims. Gunson says, "With one solitary exception, these generals and admirals had not fled abroad after the Carmona government collapsed."  Although Bartley and Ó&nbsp;Briain accept that Rincón said Chávez "had agreed to resign",  they reiterate that "elements in the military   force in the effort to make Chávez resign"; the filmmakers say it is "irrelevant" that the whole military did not join the coup, as this "is the case with most coups".  General Rincóns announcement was omitted because they felt it was "supplementary to the main, key fact of the story",  that no documentary evidence of the resignation exists. 

===X-Ray of a Lie===
Schalk investigated The Revolution Will Not Be Televised for five months.  Clark, AC (2009). The Revolutionary Has No Clothes: Hugo Chavezs Bolivarian Farce. Encounter Books. p.&nbsp;91. ISBN 9781594032592.  and in 2004 they created the documentary  . --> In 2004, he and producer Thaelman Urguelles responded to the film with their own documentary,  .  Schalk said the film "presented a distorted version of events&nbsp;... to fit a story that appeals to audiences". Schalk is associated with the Venezuelan opposition;  Bartley and Ó&nbsp;Briain say that it is "not insignificant that Schalk has led the well-resourced campaign, linked to  , to discredit and suppress  ". 

===Chavez: The Revolution Will Not Be Televised===
In 2008, Stoneman published Chavez: The Revolution Will Not Be Televised – A Case Study of Politics and the Media. A book "of film studies rather than politics", it nevertheless looks in detail at the petitions arguments.  Stoneman "broadly absolves" the filmmakers; he concludes, "There were some relatively small examples of slippage in the grammar of the piece, but overall the film was made with honesty and integrity. Of the 18 objections made, 15, if not 17, were wrong. The filmmakers spent a long time assembling evidence to show why they’d done what they’d done in the film and mostly it’s true."  Stoneman conceded that the filmmakers cinéma vérité approach meant that for wider historical and political context, viewers should look elsewhere. Stoneman received an "Executive Producer" credit on the film, which he explains as an unasked-for gratuity that came by virtue of his position as head of BSÉ. 

==Reception==

===Critical response=== weighted average out of 100 to critics reviews, the film received a score of 82 based on 24 reviews.  Almost all local and national film critics in the United States said the film presented a "riveting narrative", but conceded that it was a biased account of the events. 

{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; color:black; width:40em; max-width: 35%;" cellspacing="5"
|style="text-align: left;"|"The Revolution Will Not Be Televised gets viewers inside these tense, emotional and occasionally terrifying events with immediacy and, given the confusion of the time, remarkable clarity. Bartley and OBriain are clearly Chavez supporters—their glowing portrait of this controversial leader is never punctuated by critical questions about his policies or methods. But the filmmakers biases dont stop The Revolution Will Not Be Televised from being riveting drama."
|-
|style="text-align: left;"|—Ann Hornaday, writing in The Washington Post 
|}
Frank Scheck, writing in  .  Both critics said the film was made so through the filmmakers unique inside access to the events at the palace with Ebert calling that aspect "unique in film history".   Although Ebert was generally very praising of the film, he criticised the way in which Chávezs opponents were portrayed,  while Shenk faulted the lack of historical context; however, he said this was balanced by the films "brevity and succinctness".  In Variety (magazine)|Variety, Scott Foundas wrote that the film was a "superior example of fearless filmmakers in exactly the right place at the right time", and likely the best of a string of documentaries that have shone the light on US involvement in South America.  He had praise for the camera work and editing, and said the film was a "startling record" that reached "another level" when events shifted to the presidential palace.  He cited these scenes—along with those of the protesters clashing—as ones that "spark with a vibrant tension and uncertainty". Foundas, Scott (10 July 2003).  . Variety. 

  in  . 

Desson Thomson of  , Brett Sokol agreed that the film was "never less than thrilling", but said that as history, it was "strictly agitprop". Sokol, Brett (27 November 2003).  . Miami New Times.  Similarly, Mark Jenkins wrote in the Washington City Paper that the film was "unapologetically polemical", but "notable foremost as a gripping you-are-there account". 

===Accolades===
The film won several awards in 2003–04. It was also nominated for Best Documentary and Best Irish Film at the  . Retrieved 3&nbsp;March 2010. "Het is een van de films uit de Top 10 van filmmaker en journalist Maziar Bahari (Teheran, 1967), sinds 2000 vaste IDFA-gast&nbsp;... The Revolution will not be Televised, Kim Bartley and Donnacha O’Brian (Ierland, 2003)." 
{| align="center" width=94% style="text-align:center; font-size:87%; clear:both"
! style="background-color: #DBDABA; font-weight: normal; line-height: normal;" |  Film organization  
! style="background-color: #F5F5EC; font-weight: normal; line-height: normal;" |  Award won  
|- Banff World Television Festival
|style="background-color: #F5F5EC;"  |
Best Information and Current Affairs Program 
Grand Prize 
|- Chicago International Film Festival
|style="background-color: #F5F5EC;"  |
Silver Hugo 
|- ESB Media Awards
|style="background-color: #F5F5EC;"  |
Best Documentary 
Journalist of the Year 
|- European Broadcasting Union
|style="background-color: #F5F5EC;"  |
Golden Link Award (Best Co-Production) Staff (7 December 2007).  . RTÉ. Retrieved 25&nbsp;March 2010. 
|- Galway Film Fleadh
|style="background-color: #F5F5EC;"  |
Best Documentary 
|- Grierson Awards
|style="background-color: #F5F5EC;"  |
Best International Feature Documentary  . ScreenWest. Retrieved 25&nbsp;March 2010. 
|- International Documentary Association
|style="background-color: #F5F5EC;"  |
Best Feature Documentary (shared with Balseros (film)|Balseros) 
|- Leeds International Film Festival
|style="background-color: #F5F5EC;"  |
Audience Award 
|- Los Angeles Wine & Country Festival
|style="background-color: #F5F5EC;"  |
Best Documentary 
|- Marseille Festival of Documentary Film
|style="background-color: #F5F5EC;"  |
Best International Feature Documentary 
|- Monaco International Film Festival
|style="background-color: #F5F5EC;"  |
Golden Nymph Award (Best European Current Affairs Documentary) 
|- Peabody Award
|style="background-color: #F5F5EC;"  |
Excellence in Television Broadcasting 
|- Prix Italia
|style="background-color: #F5F5EC;"  |
Television Documentary 
|- Seattle International Film Festival
|style="background-color: #F5F5EC;"  |
Best Documentary 
|}

==References==
 
;Annotations
 

;Notes
 

;Bibliography
 
*Bartley, Kim; Ó&nbsp;Briain, Donnacha (2003). The Revolution Will Not Be Televised.  . (Galway, Ireland: Power Pictures).
*Nelson, Brian A. (2009). The Silence and the Scorpion: The Coup Against Chavez and the Making of Modern Venezuela (New York: Nation Books). ISBN 978-1568584188.
*Schiller, Naomi (October 2009). "Framing the Revolution: Circulation and Meaning of The Revolution Will Not Be Televised". Mass Communication and Society (Philadelphia, PA:    .
*Stoneman, Rod (2008). Chavez: The Revolution Will Not be Televised – A Case Study of Politics and the Media (London: Wallflower Press). ISBN 9781905674749.
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 