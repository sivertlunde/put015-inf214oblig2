The Perfect Student
{{Infobox film
| name           = The Perfect Student
| image          = The-perfect-student-dvd.png
| image size     = 
| caption        = DVD cover
| border         = 
| alt            = 
| director       = Michael Feifer
| producer       =   Peter Sullivan Jeffrey Schenck
| based on       = 
| starring       = Natasha Henstridge Josie Davis Brea Grant Jay Pickett
| music          = Marc Jovani
| cinematography = Denis Maloney
| editing        = Sean Olson
| studio         = Renegade Pictures, Hybrid
| distributor    = Eagle Films (2011) (Non-US), Starz!
| released       =  		
| runtime        = 1 hr 28 min
| country        = US
| language       = English
| budget         = 
| gross          = 
}}

The Perfect Student is a 2011 American thriller movie directed by Michael Feifer and starring Natasha Henstridge, Josie Davis, Brea Grant, and Jay Pickett. It was premiered on TV in France on March 14, 2011, and released on DVD on January 17, 2012. 

==Plot==
A student is brutally murdered on campus, and her body is thrown to the ocean. The police suspects her roommate Jordan, but Nicole Johnson, a well-respected criminology professor, understands that the police is on the wrong track, trying to frame the innocent. She begins his own investigation to prove the innocence of Jordan. But doing so is not as easy as it seems to be at first glance...  

==Cast==
*Natasha Henstridge as Nicole Johnson
*Josie Davis as Tara
*Brea Grant as Jordan
*Jay Pickett as John Michael Bowen as Detective Walker
*Carlson Young as Laura
*Alexander DiPersia as Jake (as Alexander John)
*Wilson Bethel as Trent 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 