A Princesa Xuxa e os Trapalhões
{{Infobox film
| name           = A Princesa Xuxa e os Trapalhões
| image          = 
| caption        = 
| director       = José Alvarenga Jr.	
| producer       = Renato Aragão
| writer         = Mauro Wilson Paulo de Andrade Carlos Alberto Diniz Roberto Silveira Zacarias Xuxa Meneghel Paulo Reis
| music          = 
| cinematography = Nonato Estrela
| studio         = 
| distributor    = Columbia Pictures Art Films
| released       = 1989
| runtime        = 112 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = 
}}
A Princesa Xuxa e os Trapalhões is a 1989 Brazilian film, directed by José Alvarenga Jr. and starring Xuxa Meneghel and Os Trapalhões.

== Plot ==
The story takes place on another planet, in full outer space. In the right arm of the Milky Way there is a planet called Antar, the terrible Ratan decides to conquer it alone. For this, it makes use of all the military power of the kingdom, destroying it almost completely. 

== Cast ==
*Renato Aragão .... Diron, o Cavaleiro Sem Nome
*Mussum .... Mussaim
*Dedé Santana .... Dedeon Zacarias .... Zacaling
*Xuxa Meneghel .... Princesa Xeron
*Paulo Reis .... Imperador Ratan
*Trem da Alegria

== See also ==
* List of Brazilian films of the 1980s

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 


 
 