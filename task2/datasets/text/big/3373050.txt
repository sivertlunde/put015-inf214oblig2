OffOn
OffOn is an experimental film created by Scott Bartlett made in 1967 and released in 1972.   It is most notable for being one of the first examples in which film and video technologies were combined.  The nine minute film combines a number of video loops which have been altered through re-photography or video colorization, and utilizes an electronic sound track to create its unique effect.

In 2004, the film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==References==
 

==External links==
*  
*  
*  

 
 


 