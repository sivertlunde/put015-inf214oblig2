Bullets over Broadway
{{Infobox film
| name = Bullets over Broadway
| image = Bullets over Broadway movie poster.jpg
| alt = 
| caption = Theatrical release poster
| director = Woody Allen Jack Rollins
| writer = Woody Allen Douglas McGrath
| starring = John Cusack Dianne Wiest Jennifer Tilly Chazz Palminteri Mary-Louise Parker Jack Warden Joe Viterelli Rob Reiner Tracey Ullman Jim Broadbent Harvey Fierstein Carlo DiPalma
| editing = Susan E. Morse
| distributor = Miramax Films
| released =  
| runtime = 98 minutes
| country = United States
| language = English
| budget = $20 million
| gross = $13,383,747
}}
Bullets over Broadway is a 1994 American Crime film|crime-comedy film written by Woody Allen and Douglas McGrath and directed by Woody Allen. It stars an ensemble cast including John Cusack, Dianne Wiest, Chazz Palminteri, and Jennifer Tilly.
 Original Screenplay, Director and Supporting Actress Actor respectively. Wiest won Best Supporting Actress for her performance, the second time Allen directed her to an Academy Award.
 Broadway Bullets musical theatre version opened in previews on March 11, 2014.

==Plot==
In 1928, David Shayne (John Cusack) is an idealistic young playwright newly arrived on Broadway theatre|Broadway. In order to gain financing for his play, God of Our Fathers, he agrees to hire Olive Neal (Jennifer Tilly), the actress/girlfriend of a gangster. She is demanding and talentless, but her gangster escort Cheech (Chazz Palminteri) turns out to be a genius, who constantly comes up with excellent ideas for revising the play.
 partner Ellen alcoholic leading lady Helen Sinclair (Dianne Wiest), and facing his leading man, a compulsive eater (Jim Broadbent), beginning an affair with Olive.

==Cast==
* John Cusack as David Shayne
* Dianne Wiest as Helen Sinclair
* Jennifer Tilly as Olive Neal
* Chazz Palminteri as Cheech
* Mary-Louise Parker as Ellen
* Jack Warden as Julian Marx
* Joe Viterelli as Nick Valenti
* Rob Reiner as Sheldon Flender
* Tracey Ullman as Eden Brent
* Jim Broadbent as Warner Purcell
* Harvey Fierstein as Sid Loomis
* Stacey Nelkin as Rita
* Edie Falco as Lorna
* Benay Venuta as Adoring Theatre Patron
* Debi Mazar as Violet
* Małgorzata Zajączkowska as Lili
* Tony Sirico as Rocco
* Tony Darrow as Aldo

==Production==
The films locales include the duplex co-op on the 22nd floor of 5 Tudor City Place in Manhattan. 

The films title may have been an homage to a lengthy sketch of the same title from the 1950s television show Caesars Hour; one of Allens first jobs in television was writing for Sid Caesar specials after the initial run of the show.
The film featured the last screen appearance of Benay Venuta. Allen cast her in a cameo role as a well-wishing wealthy theatre patron. She died of lung cancer months after the film opened.

==Stage musical==
Allen adapted the film as a stage musical, titled   as David Shayne, Brooks Ashmanskas, Betsy Wolfe, Lenny Wolpe, and Vincent Pastore.  Marin Mazzie stars as Helen Sinclair,  and Karen Ziemba appears as "Eden Brent."  Musical supervisor Glen Kelly has adapted and written additional lyrics for songs including "Taint Nobodys Busness," "Running Wild," "Lets Misbehave" and "I Found A New Baby".  The musical closed on August 24, 2014, after 156 performances and 33 previews. 

==Reception==
The review-aggregate website Rotten Tomatoes reports 96% positive reviews, with the consensus "A gleefully entertaining backstage comedy, Bullets Over Broadway features some of Woody Allens sharpest, most inspired late-period writing and direction." 

== Awards and nominations ==
=== Won ===
* Academy Award for Best Supporting Actress – Dianne Wiest American Comedy Award for Funniest Supporting Actress in a Motion Picture – Dianne Wiest
* Chicago Film Critics Association Award for Best Actress – Dianne Wiest
* Dallas-Fort Worth Film Critics Association Award for Best Supporting Actress – Dianne Wiest
* Golden Globe Award for Best Supporting Actress – Motion Picture – Dianne Wiest
* Independent Spirit Award for Best Supporting Female – Dianne Wiest
* Independent Spirit Award for Best Supporting Male – Chazz Palminteri
* Kansas City Film Critics Circle Award for Best Supporting Actress – Dianne Wiest
* Los Angeles Film Critics Association Award for Best Supporting Actress – Dianne Wiest
* National Society of Film Critics Award for Best Supporting Actress – Dianne Wiest
* New York Film Critics Circle Award for Best Supporting Actress – Dianne Wiest
* Sant Jordi Award for Best Foreign Actor – Chazz Palminteri
* Screen Actors Guild Award for Outstanding Performance by a Female Actor in a Supporting Role – Dianne Wiest Society of Texas Film Critics Award for Best Supporting Actress – Dianne Wiest
* Southeastern Film Critics Association Award for Best Supporting Actress – Dianne Wiest

=== Nominated ===
* Academy Award for Best Supporting Actor – Chazz Palminteri
* Academy Award for Best Supporting Actress – Jennifer Tilly
* Academy Award for Best Director – Woody Allen
* Academy Award for Best Original Screenplay – Woody Allen and Douglas McGrath
* Academy Award for Best Production Design – Santo Loquasto and Susan Bode
* Academy Award for Best Costume Design – Jeffrey Kurland
* AFIs 100 Years...100 Laughs 
* AFIs 10 Top 10 – Gangster Film 
* BAFTA Award for Best Original Screenplay – Woody Allen and Douglas McGrath
* Boston Society of Film Critics Award for Best Supporting Actress – Dianne Wiest
* Chicago Film Critics Association Award for Best Supporting Actor – Chazz Palminteri
* Laurel Award for Screenwriting Achievement – Woody Allen and Douglas McGrath
* Screen Actors Guild Award for Outstanding Performance by a Male Actor in a Supporting Role – Chazz Palminteri

==References==
 

==External links==
* 
* 
* 
* 
* : article discussing the Nietzschean influences in Bullets Over Broadway

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 