Return to Bolivia
{{Infobox Film
| name           = Return to Bolivia
| image          = Returntobolivia.jpg
| caption        = Theatrical poster
| director       = Mariano Raffo
| producer       = Marina Boolls  Felicitas Raffo
| writer         = Mariano Raffo
| starring       = Janeth Cuiza Brian Quispe Cuiza Camila Quispe Cuiza Jhoselyn Quispe Cuiza David Quispe
| music          = Zelmar Garín
| cinematography = Mariano Raffo
| editing        = Marina Boolls
| distributor    = Pelela cine-CEPA
| released       =  
| runtime        = 96 minutes
| country        = Argentina Spanish Aymara Aymara
| budget         =
}}
Return to Bolivia is a 2008 Argentine film directed by Mariano Raffo,  with the script in collaboration with Marina Boolls.

The film was nominated for twenty-two awards and won four of them.

==Plot== Oruro and La Paz.
: Return to Bolivia is an auteur documentary film that tells the story of a Bolivian family in Buenos Aires that decides to return to Bolivia. The documentary is filmed in a verite style and it gives a personal account of the subject of immigration, allowing the characters to lead the story. The narrative is based on universal values using a clear style that brings the story very close to fiction. 

==Production==
Shot with limited resources and a minimal film crew that included the director and the films producer and soundman, Mariana Boolls. This production won several international awards, including the award for Best Foreign Film Festival Icaro XI of Guatemala and the Best Documentary Festival Gualeguaychú, Entre Ríos|Gualeguaychú. It received positive reviews, like the Buenos Aires Herald s critic:

: (...)the intelligent narrative and the skilful editing make Return to Bolivia a remarkable documentary, not just from a technical point of view. Indeed, Return to Bolivia, more than just a camera following the travails of a group of people, is about life and death, death and life, the exploration of which is never easy. 

==Cast==
* Janeth Cuiza as Janeth
* Brian Quispe Cuiza as Brian
* Camila Quispe Cuiza as Camila
* Jhoselyn Quispe Cuiza as Jhoselyn
* David Quispe as David

==Awards==
{|  class="wikitable sortable"
! Country
! Year
! Festival
|- Argentina || 2009 || Best Documentary del Festival de Gualeguaychú, Entre Ríos|Gualeguaychú
|- Guatemala || 2008 || Best Documentary Extranjero del XI Festival Icaro de Guatemala
|- Argentina || Tucuman Cine.
|- Argentina || 2009 || First Prize Zona Centro of Festival ARAN de Neuquen.
|- Argentina || 2009 || Grand Prize Federal Amonite (compartido) del Festival ARAN de Neuquen
|}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 