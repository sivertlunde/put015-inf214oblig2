The Falcon (film)
{{Infobox Film
| name = The Falcon
| image =Banovic Strahinja (film).jpg
| image_size =
| caption =
| director = Vatroslav Mimica
| producer = Aleksandar Petrović (screenplay),  Vatroslav Mimica (screenplay)
| starring =
| music = Alfi Kabiljo
| cinematography = 
| editing =
| distributor = Soundfilm München (1982)
| released = 30 March 1983 (West Germany)
| runtime =
| country = Yugoslavia
| language = Serbo-Croatian
| budget =
| gross =
| preceded_by =
}}

Banović Strahinja (  Yugoslavian adventure film written and directed by Vatroslav Mimica based on Strahinja Banović, a hero of Serbian epic poetry.  It entered the section "Officina Veneziana" at the 38th Venice International Film Festival. 

==Plot==
During the late 14th century Serbia becomes the target of the Ottoman Empire. While the respected Serbian noble Strahinja Banović is out hunting, a Turkish renegade gang burns his castle, kills all of his servants, and takes the young wife of Banović Strahinja. Strahinja begins a long quest to rescue his wife despite everybody elses doubts in her fidelity. Strahinja gathers a posse of scoundrels and goes after the bandits. In the meantime, the Turkish bandit Alija tries to seduce Strahinjas wife Anđa, but she refuses him. However, over a period of time she begins to weaken.

==Cast==
* Franco Nero as Banović Strahinja
* Gert Fröbe as Jug Bogdan
* Dragan Nikolić as Alija
* Sanja Vejnović as	Andja
* Rade Šerbedžija as Abdulah
* Nikola-Kole Angelovski as Timotije
* Stole Aranđelović	as Pop Gradislav
* Neda Spasojević as Luda
* Janez Vrhovec as Vladika
* Rados Bajic as Bosko Jugovic

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 


 
 