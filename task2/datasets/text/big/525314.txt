Shrek 2
 
 
{{Infobox film
| image          = Shrek 2 poster.jpg
| caption        = Theatrical release poster
| alt            = 
| director       = Andrew Adamson Kelly Asbury Conrad Vernon
| producer       = Aron Warner John H. Williams David Lipman
| screenplay     = Andrew Adamson Joe Stillman J. David Stem David N. Weiss
| story          = Andrew Adamson William Steig
| based on       =  
| narrator       = Rupert Everett
| starring       = Mike Myers Eddie Murphy Cameron Diaz Julie Andrews Antonio Banderas John Cleese Rupert Everett Jennifer Saunders
| music          = Harry Gregson-Williams
| editing        = Michael Andrews Sim Evan-Jones
| studio         = DreamWorks Animation Pacific Data Images DreamWorks Pictures
| released       =  
| runtime        = 93 minutes  
| country        = United States
| language       = English
| budget         = $150 million
| gross          = $919.8 million 
}}
  fantasy comedy film produced by DreamWorks Animation and directed by Andrew Adamson, Kelly Asbury and Conrad Vernon. It is the second installment in the Shrek (franchise)|Shrek series, the sequel to 2001s Shrek, and features the voices of Mike Myers, Eddie Murphy, Cameron Diaz, Antonio Banderas, Julie Andrews, John Cleese, Rupert Everett, and Jennifer Saunders.
 sixth highest-grossing animated film of all time.

==Plot== Puss in Boots.

When Fiona realizes that Shrek left, she asks her father for help, but he replies that he always wanted the best for her and that she should think about that too. Puss is unable to defeat Shrek and reveals that he was paid by Harold. He asks to tag along as a way to make amends. Shrek decides to go to the Fairy Godmother for help. However, the Fairy Godmother states that ogres do not live "happily ever after" and refuses to assist him. Nonetheless, the three sneak into the Fairy Godmothers potion factory and steal a "Happily Ever After" potion that Shrek believes will ensure a happy ending for his marriage. Shrek and Donkey both drink the potion, which doesnt appear to work. They wait out the storm in a barn and Shrek and Donkey become dizzy and fall over into a deep sleep. The next morning the potion has taken effect: Shrek is now a handsome human, while Donkey has turned into a white stallion. To make the change permanent, Shrek must kiss Fiona by midnight. Shrek, Donkey, and Puss return to the castle to discover that the potion has also transformed Fiona back into her former human self. However, the Fairy Godmother, having learned of the potions theft, sends Charming to the castle, where he poses as Shrek to win Fionas love. Although Harold recognizes Charming for who he really is, he doesnt speak up. Shrek, heartbroken, lets Fiona go, believing she will be happier with Charming.
 Frog Prince. Harold apologizes to Shrek and Fiona for everything he has done, and now realizes what is best for Fiona, giving them his blessing. Shrek forgives him, and Lillian tells Harold that she still loves him no matter what. The castle clock strikes midnight and Shrek, reminded of the potions deadline, tells Fiona that if they kiss now, then their change will be permanent. But Fiona says that she wants what any other princess wants: to live happily ever after with the ogre she married. The clock chimes as the potions effects wear off, with Donkey changing back as well, much to his chagrin. He cheers up as Shrek reassures him he still is a noble steed in his eyes. Finally, Puss and Donkey sing as the royal ball resumes celebrating the true newlyweds.

In a post-credits scene, Donkey complains to Puss about missing Dragon, and rejects his invitation to a night-cap at the "Kit-Kat Club." Suddenly, Dragon flies in, along with six "List of Shrek characters#Dronkeys|Dronkeys" that Donkey happily embraces as his "mutant babies," and Donkey and Dragon reconcile. Having a larger family, Donkey realizes, "Ive gotta get a job!"

==Cast==
  Shrek
* Donkey
* Cameron Diaz as Princess Fiona Puss in Boots King Harold Queen Lillian Prince Charming and the Narrator Fairy Godmother Red Carpet Announcer
** Kate Thornton (UK version) as Red Carpet Announcer Doris the Ugly Stepsister
** Jonathan Ross (UK version) as Doris the Ugly Stepsister Big Bad Wolf The Three Little Pigs Three Blind Mice Gingerbread Man; Muffin Man; Mongo (Shrek)|Mongo; Cedric; Announcer Chris Miller as Magic Mirror Mark Moseley as Dresser
* Kelly Cooney as Fast Food Clerk
* Kelly Asbury as Page; Elf; Nobleman; Noblemans son
* Andrew Adamson as Captain of the Guard
 

; Special guest stars
*Joan Rivers cameo marked the first time that a real person had been represented on screen by the Shrek animation team. Her part (though retaining her visual representation) was redubbed by presenter Kate Thornton for the United Kingdom release.
*On the DVD Special Features and in the U.S. edition VHS (just before the credits), Simon Cowell appears as himself on Far Far Away Idol, a parody of American Idol. (see Home Media)

==Production==
In July 2001, it was reported that the main cast of the original Shrek were set for huge paychecks for voicing a sequel to the film.  Following a successful collaboration with the original film, Eddie Murphy had signed a two-year, first-look production deal with DreamWorks, where he also signed writer-director Todd Field to a two-year deal.  The film was produced with a US$70 million budget.  
 Ted Elliott and Terry Rossio, insisted the film to be a traditional fairytale, but after disagreements with the producers, they left the project and was taken over by director Andrew Adamson. His writing of the film was inspired from Guess Whos Coming to Dinner, with the help of the co-directors for the film, who had spent most of the films production in Northern California while Adamson spent most of the time with the voice actors in Glendale, California. {{cite book|author=Alex Ben Block, Lucy Autrey Wilson|title=George Lucass Blockbusting: A Decade-by-Decade Survey of Timeless Movies Including Untold Secrets of Their Financial and Cultural Success|date=March 30, 2010|publisher=HarperCollins 
|oclc=310398975|lccn=2010279574|isbn=0061778893|pages=976}} 

DreamWorks began production in 2001,    which was actually before the first film was even completed.  DreamWorks made sure there was something new to see in Shrek 2 by putting more human characters in the film than there were in its predecessor and improving their appearance, with the use of a few systems that dealt with hair and fur to improve its appearance and movement.  The set up for all the characters was done in the first 3 years of production. 
 Puss in Boots required a whole new set of tools in the film to handle his fur, belt and feather plume in his hat. The character also required an upgrade in the fur shader for his introduction in the film. 

According to production designer Guillaume Aretos, Shrek 2 appeared to be a lot darker than the original film; "There are a lot of medieval paintings and illustrations that we used quite a bit also. Other than that there are my own influences, which are classical paintings from the 15th and 16th centuries, but those are not as direct. In fact, nothing was absolutely direct. The design of Shrek is always a twist on reality anyway, so we tried to   as much detail and interest as we could in the imagery." 

==Soundtrack==
  John Powell had been left out to compose the score for the film with Williams due to a conflict. 

==Cultural references==
Like its predecessor, Shrek 2 also acts as somewhat of a parody film, targeting adapted childrens fantasies (mainly those adapted by Disney); and like other DreamWorks animated films, also features references to American popular culture: Ariel from The Little Mermaid.   
* Elements and landmarks in the fictional kingdom of Far Far Away bear reference to elements and landmarks of Southern California, particularly those of the Los Angeles area. For example, the kingdom features a "Far Far Away" sign obviously modeled after the famous Hollywood Sign; and the "Friars Fat Boy" restaurant which King Harold, Fairy Godmother and Charming "drive-thru" references the Southern California restaurant chain, Bobs Big Boy. 
* The dinner scene where the camera cuts to different characters saying each others names references a scene from The Rocky Horror Picture Show.
* The character Puss In Boots is based on Zorro, a character played by Banderas, who also voices Puss. His behavior references Zorro as he appeared in the 1998 film, The Mask of Zorro.  The Wizard of Oz. 
* When Shrek, Donkey, and Puss are having drinks at The Poison Apple, Puss laments "I hate Mondays" in reference to the cartoon cat Garfields catchphrase.
* When Mongo first enters Far Far Away, several people run out of a "Farbucks" in fear, only to run to another location across the street. 
* When the fairy tale creatures rescue Shrek, Donkey and Puss, Pinocchio dives in the prison tower attached to puppet strings, a reference to  . The theme music can be heard in the background as well.   
* Several parodies of well-known businesses exist in Far Far Away, such as "Farbucks", a parody of Starbucks, "Baskin Robbinhood", a parody of Baskin Robbins, "Burger Prince", a parody of Burger King, "Abercrombie & Witch", a parody of Abercrombie & Fitch, "Versarchery," a play on the designer label Versace, and "Old Knavery", a parody of Old Navy. 
* When Mongo sinks into the moat in front of the castle, he says "Be good." to Gingy, referencing E.T. (film)|E.T. (Steven Spielberg, director of E.T., was a co-founder of Dreamworks). 
* When the Fairy Godmother appears to Fiona on her balcony when she sheds a tear due to the fight at dinner, the gold dress in which she makes Fiona wear, blows upward in a reference to the Marilyn Monroe film The Seven Year Itch. 
* When Puss is attacking Shrek and crawls through his shirt, he bursts out of the front, a reference to the chestburster scene from the 1979 film Alien (film)|Alien 

==Release==
In April 2004, the film was selected for competition at the 2004 Cannes Film Festival.   

Shrek 2 was originally going to release on June 18, 2004.  The film was then moved forward to May 21, 2004, however, due to "fan demand," it was released two days earlier, on May 19, 2004.  A day before the film went to theaters, the first five minutes were shown on Nickelodeons U-Pick Live. 

Playing in 4,163 theaters over its first weekend in the United States, Shrek 2 was the first film with over 4,000 theaters in overall count; over 3,700 theaters was its count for an opening day. 

==Home media==
Shrek 2 was released on VHS and DVD on November 5, 2004    and on Game Boy Advance Video on November 17, 2005.  A 3D-converted version of the film was released exclusively with select Samsung television sets on Blu-ray on December 1, 2010, along with the other three films of the series.  A non-3D version was released on December 7, 2010, as part of Shrek: The Whole Story,  and a stand-alone Blu-ray/DVD combo pack was released individually on August 30, 2011, along with the other two films of the series.  A stand-alone 3D Blu-ray version of the film was released on November 1, 2011. 

===Far Far Away Idol===
Far Far Away Idol is a special feature on the DVD and VHS release based on American Idol and guest starring Simon Cowell. Taking place right after Shrek 2 ends, the characters from Shrek compete in singing popular songs while being judged by Shrek, Fiona, and Cowell. 
 My Way". At the end of the VHS release, it gives a link to a website where the viewer can vote for their favorite to determine the ultimate winner.  DreamWorks Animation announced on November 8, 2004, three days after the DVD and VHS release, that after over 750 thousand votes cast, the winner of the competition was Doris. 

 

==Reception==

===Box office===
The film opened at #1 with a Friday-to-Sunday total of $108,037,878, and a total of $128,983,060 since its Wednesday launch, from a then-record 4,163 theaters, for an average of $25,952 per theater over the weekend. At the time Shrek 2 s Friday-to-Sunday total was the second-highest opening weekend trailing only Spider-Man (2002 film)|Spider-Man s $114,844,116. In addition, Saturday alone managed to obtain $44,797,042, making it the highest single day gross at the time, beating Spider-Man s first Saturday gross of $43,622,264.  It remained at #1 in its second weekend, grossing another $95,578,365 over the 4-day Memorial Day weekend, narrowly beating out the $85,807,341 4-day tally of new opener The Day After Tomorrow. The film spent a total of 10 weeks in the weekly top 10 remaining there until July 29, and stayed in theaters for 149 days (roughly 21 weeks), closing on November 25, 2004.
 8th on 30th on the worldwide box office list. 

The film also took away the highest worldwide gross made by an animated feature, which was before held by Finding Nemo,  although the latter still had a higher overseas-only gross.  With DVD sales and Shrek 2 merchandise are estimated to total almost $800 million, the film (which was produced with a budget of $150 million)  is DreamWorks most profitable film to date.
 Disney and 6th highest-grossing animated film of all time.

===Critical response  ===
 
The film was well received by a number of critics, many rating it as good as its predecessor,   and some rated it even better.  Based on reviews collected from 232 critics by the film review aggregator  |archiveurl=//web.archive.org/web/20140423212814/http://www.rottentomatoes.com/m/shrek_2/ 
|archivedate=April 23, 2014}}  On Metacritic, the film has a weighted average rating of 75 out of 100 based on 40 professional reviews published in newspapers, magazines and in highly regarded Internet sites, which indicates "generally favorable reviews." {{cite web|title= 
Shrek 2 reviews|url=http://www.metacritic.com/movie/shrek-2|work=Metacritic|accessdate=July 10, 2010|archivedate=October 4, 2013|archiveurl=//web.archive.org/web/20131004233220/http://www.metacritic.com/movie/shrek-2}} 

Roger Ebert gave it three out of four stars saying its "bright, lively, and entertaining,"  while Robert Denerstein of Denver Rocky Mountain News called it "Sharply funny."  James Kendrick praised the plot, calling it "familiar, but funny." 
 New York magazine stated the film "manages to undo much of what made its predecessor such a computer-generated joy ride." 

===Accolades  ===
Shrek 2 was nominated for the  ), Favorite Movie Comedy, and Favorite Movie Villain for "Fairy Godmother" ( , but lost to The Incredibles. One of the films songs, "Accidentally in Love" received nominations for the Academy Award for Best Original Song, Golden Globe Award for Best Original Song, and the Grammy Award for Best Song Written for a Motion Picture, Television or Other Visual Media.
 Top 10 Animated Films list. 

==Other media==

===Video games===
  Shrek 2 (Shrek 2: Team Action) (2004)
* Shrek 2 Activity Center: Twisted Fairy Tale Fun (2004) 
* Shrek 2: Beg for Mercy (2004) 
* Shrek Super Slam (2005)
* Shrek Smash n Crash Racing (2006)

==Sequels and spin-offs==
  Puss in Puss in Boots, who was introduced in this film.

==See also==
 
* List of animated feature-length films
* List of computer-animated films
 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 