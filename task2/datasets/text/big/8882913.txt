B.S. I Love You
 
{{Infobox Film name          = B.S. I Love You image         = Bs-i-love-you-movie-poster-1971.jpg caption       = Movie poster director      = Steven Hilliard Stern producer      = Arthur M. Broidy writer        = Steven Hilliard Stern starring      = Peter Kastner Joanna Cameron Louise Sorel Gary Burghoff Richard B. Shull Joanna Barnes                distributor   = 20th Century Fox released      = March 31, 1971 USA runtime       = 99 Min language      = English
}}

B.S. I Love You is an American comedy-drama film from 1971. It was directed and written by Steven Hilliard Stern, and starred Peter Kastner. The supporting cast included Gary Burghoff, Louise Sorel, Joanna Cameron and Joanna Barnes. The style of the film is like many others of its era, taking its cues from The Graduate and the raunchiness of the early 1970s, as Kastner plays a youthful TV commercials producer whose quest in life is to bed as many women as possible, while trying to remain faithful to his childhood sweetheart who remains in tow, awaiting the day they will marry.   

The film was released to little or no fanfare, and remains today a curious relic from the early 1970s. It is extremely hard to find, as it was never released on VHS and had not been released on DVD as of October 2010.  It runs on the Fox Movie Channel from time to time, but it is an edited version (it runs 89 minutes, excising 10 minutes from the original theater release print).

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 

 