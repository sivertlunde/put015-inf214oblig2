Shirley Thompson vs. the Aliens
 
{{Infobox film
| name           = Shirley Thompson vs. the Aliens
| image          =
| caption        =
| director       = Jim Sharman Matt Carroll Jim Sharman
| writer         = Jim Sharman Helmut Bakaitis
| narrator       =
| starring       = Jane Harders Helmut Bakaitis
| music          = Ralph Tyrell
| cinematography = David Sanderson
| editing        = Malcolm Smith
| studio         = Kolossal Piktures
| distributor    =
| released       =  
| runtime        = 104 minutes (original cut) 79 mins (1976 re-edit)
| country        = Australia English
| budget         = A$50,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998 p263  or $17,000 
| gross          =
}}

Shirley Thompson vs. the Aliens is a 1972 film directed by Jim Sharman. It is the first feature-length film from Sharman, who subsequently directed The Rocky Horror Picture Show.

==Plot==
In 1950s Sydney, Shirley and her gang discover that aliens have attacked Australia but no one believes them. Shirley is assumed to be insane and is committed to a lunatic asylum.

==Cast==
*Jane Harders as Shirley Thompson
*June Collis as Dr Leslie Smith
*Tim Elliot as Dr George Talbot
*Marion Johns as Rita Thompson
*John Llewellyn as Reg Thompson
*Marie Nicholas as Narelle Thompson
*Helmut Bakaitis as Harold
*John Ivkovitch as Bruce
*Bruce Gould as Blake
*Kate Fitzpatrick as nurse
*Alexander Hay as Alien
*Ron Haddrick as replica of Prince Philip
*Phil Kitamura as gang member
*Candy Raymond as gang member
*Julie Rodgers as gang member
*Georgina West as gang member
*Max Hess as gang member
*Sue Moir as gang member

==Production==
The film was shot on 16mm.  Sharman paid for the movie with his own money. It was written as a tribute to old B movies. David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p160-161 

Sharman later said "it was made quite impulsively and not without passion". 

==Release==
The film was previewed at the National Film Theatre in London in March 1972 and had its premiere at the Sydney Film Festival in June and received limited release. In 1976 Sharman re-edited it substantially. 

==References==
 

==External links==
*  
*  
*  at Australian Screen Online
*  at Oz Movies

 

 
 