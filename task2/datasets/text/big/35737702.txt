Chor Aur Chaand
{{Infobox film
| name           = Chor Aur Chaand
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Pavan Kaul	 	
| producer       = Aditya Pancholi
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Aditya Pancholi Pooja Bhatt
| music          = Nikhil-Vinay
| Lyric          = Yogesh
| cinematography = Promod Pradhan
| editing        = Afaque Husain	 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by Pavan Kaul and produced by Aditya Pancholi. It stars Aditya Pancholi and Pooja Bhatt in pivotal roles.

It is a love story with the back drop of Drug and its effect on youth of the nation.Both Aditya Pancholi and Raghuvir Yadav were brilliant along with Kiran Kumar and Pooja Bhatt.It had some great soulful music composed by Yogesh who had given hits like Anand, Mili , Manzil, Rajinigandha and Chhoti Se Baat etc.

==Plot==

Reema lives a wealthy lifestyle with her widowed businessman dad, Dinkar, and her grandma. Her dad wants her to marry Vicky, but she dislikes him. On the day of the marriage, while fully dressed as a Hindu bride, Reema plans to run away. It is at this time that she sees a stranger in her house, thinking it is her dads employee, she tries to run from him, but he catches up with her. He tells her that his name is Suraj alias Surya, an ex-convict, who has just been released from prison. As he had no money, he as unable to eat anything, and hence had broken into their house to steal money and jewelery. While on the run, they are also joined by Hero, who had attempted to rob a bank so that he could go to Bombay and become a movie star. His attempt at robbing the bank had failed and he is now on run from the police. When Dinkar is told that Reema has run away, he contacts a local gangster named Lala and asks him to locate her. Lala, in his turn asks Ranga, one of his hoodlums, to find Reema. Ranga finds out where Reema, Hero and Suraj are holed up. At the same time, Inspector Naik also finds out their whereabouts. But Naik has a different reason for locating them - he has a score to settle with Suraj, and wants Reema for himself. There is no doubt at all that Reema, Suraj and Hero are headed for nothing but trouble, as the gangster and their men on one hand, and Inspector Naik on the other, close in on them, making their escapade virtually impossible. 

==Cast==
* Aditya Pancholi...Suraj "Surya"
* Pooja Bhatt...Reema
* Aruna Irani...Surajs mom
* Alok Nath...Dinkar Seth
* Raghuvir Yadav...	Hero
* Avtar Gill...Inspector Naik
* Kiran Kumar...Inspector Vivek

==Soundtrack==

Nikhil Vinay had composed 10 beautiful tracks penned by Yogesh and the title track along Sapno Mein Aana Dil Mein Samana,Lagne Laga Hai Mujhe Aajkal,Baat Kya Hai Kaise Kehde Tumse Raaz Ki and  Sharma Ke Baadalon Mein Chanda Kyon Chhup Raha being the best.
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Sapno Mein Aana Dil Mein Samana"
| S. P. Balasubrahmanyam, Anuradha Paudwal
|-
| 2
| "Lagne Laga Hai Mujhe Aajkal"
| S. P. Balasubrahmanyam, Anuradha Paudwal
|-
| 3
| "Mohabbat Mein Duniya Se Ham Na Darenge"
| Manu Chitra, Anuradha Paudwal
|-
| 4
| "Tere Bin Kahin Jiyara Lage Na"
| Kumar Sanu, Anuradha Paudwal
|-
| 5
| "Saanson Ka Kya Pata"
| S. P. Balasubrahmanyam, Anuradha Paudwal
|-
| 6
| "Baat Kya Hai Kaise Kehde Tumse Raaz Ki"
| S. P. Balasubrahmanyam, Anuradha Paudwal
|-
| 7
| "Sau Baar Hamne Tumse Kah"
| Manu Chitra, Anuradha Paudwal
|-
| 8
| "Sharma Ke Baadalon Mein Chanda Kyon Chhup Raha"
| S. P. Balasubrahmanyam, Anuradha Paudwal
|-
| 9
| "Tere Bina Main Na Rahun"
| Kumar Kancha, Anuradha Paudwal
|-
| 10
| "Janam Janam Tumko Sanam Chaha Teri Kasam"
| Udit Narayan, Anuradha Paudwal
|}
==External links==
* 

 
 
 

 