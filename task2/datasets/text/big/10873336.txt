Blind Mountain
{{Infobox film
| name           = Blind Mountain
| image          = Blind_Mountain_Poster.jpg
| caption        =  Li Yang
| writer         = Li Yang
| starring       = Huang Lu
| producer       = Li Yang
| distributor    = StudioCanal Tang Splendour Films
| cinematography = Lin Jong
| editing        = Li Yang Mary Stephen
| released       =   
| runtime        = 95 minutes
| country        = China
| language       = Shaanxi Chinese Dialect Sichuan Dialect
| budget         = €600,000 ($809,000)   
}} Chinese film Li Yang and is Lis first feature film since his 2003 debut Blind Shaft. It is also known as Road Home. 

Like Lis previous film, Blind Shaft, which dealt with the notoriously dangerous mining industry, Blind Mountain turns a sharply critical eye towards another one of Chinas continuing social problems, this time selling women for marriage in modern day china.

==Plot==
Blind Mountain follows a young woman, Bai Xuemei, in the early 1990s who recently graduated from college and attempts to find work to help pay for her brothers education. In the process, she is drugged, kidnapped and sold as a bride to a villager in the Qin Mountains of Chinas Shaanxi province. Trapped in the fiercely traditional town, the young woman finds that her avenues of escape are all blocked. As she searches for allies, including a young boy, a school teacher and a mailman, she suffers from being raped by her "husband" and continued beatings at the hands of the villagers, her husband, and her husbands parents.

==Cast==
* Huang Lu as Bai Xuemei, the heroine, a young college student
* Yang Youan as Huang Degui, a villager who purchases Bai Xuemei
* Zhang Yuling as Ding Xiuying, Huang Deguis mother
* He Yunie as Huang Decheng, a local teacher and Huang Deguis cousin
* Jia Yinggao as Huang Changyi, Huang Deguis father
* Zhang Youping as Li Qingshan, a local boy who befriends Bai Xuemei

==Production history==
The film was primarily funded by private, overseas Chinese donors.  The cast was primarily non-professional actors with the notable exception of the lead, Huang Lu, who was cast from the Beijing Film Academy before she had even graduated. 
 Chinese Film Bureau policy also led to Li filming several alternate versions of the films ending including a more upbeat one specifically for a possible video or DVD release in China. 

==Reception== Night Train, Wang Bing California Dreamin.

While the audience at Cannes gave a standing ovation to the film when it was screened, {{cite web | url =
http://www.hollywoodreporter.com/hr/awards_festivals/fest_reviews/article_display.jsp?&rid=9234 cinematographer Lin Jong.   Other critics expressed sentiments similar to Elley, but nevertheless found the performances effective, particularly by the lead actress.     Reception was more positive with the Chinese press. Writing in Hong Kong cultural magazine, Muse (Hong Kong magazine)|Muse, Perry Lam observed, What makes this film so heartbreaking and so hard to watch sometimes is its determination to let us experience without varnish the depths of the heroines despair and meet all the wretched characters who have a role to play in her tragedy. 

On April 19, 2008, Blind Mountain won the Film Award of the Council of Europe (FACE), which is presented at the Istanbul International Film Festival by the Council of Europe to a film that raises public awareness and interest in human rights issues and promotes a better understanding of their significance. 

===Awards and nominations===
* 2007 International Film Festival Bratislava
** Grand Prix (describing the film as "masterful") 
** Special Mention of the Ecumenical Jury

==Alternate version==
The movie was filmed with several endings to satisfy the Chinese film censorship board. The rather short and abrupt ending in the international DVD has the main character of Bai Xuemei stabbing the "husband" who forcefully married and raped her, during a personal brawl with Xuemeis father after the police leaves the village. The ending that appears on the Chinese DVD is more of a "happy" yet ironic ending when the police shows up at Bai Xuemeis house days later, drugs and fends off the husband and rescues her. At the same time, other girls who were sold into marriage in the village are also rescued, but her best friend (who was also sold into marriage) suddenly decides to stay for her child and gets out of the police car. Bai Xuemeis child is left without a mother, and the family of her "husband" is left with no daughter-in-law. The ending concludes with text about the ongoing problem of human trafficking in modern-day China. More than 30 cuts were required by Chinese censors for it be approved for exhibition.

==Notes==
 

==External links==
 
*  
*  
*  
*  
*  
*   at the Chinese Movie Database
*   at schrift-chinesisch

 
 
 
 
 