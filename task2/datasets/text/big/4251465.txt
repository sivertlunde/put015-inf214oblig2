Fido (film)
{{Infobox film
| name           = Fido
| image          = Fido newposter.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Andrew Currie
| producer       = Trent Carlson Blake Corbet Mary Anne Waterhouse Kevin Eastwood
| writer         = Robert Chomiak Andrew Currie Dennis Heaton
| story          = Dennis Heaton
| starring       = Carrie-Anne Moss Billy Connolly Dylan Baker KSun Ray Henry Czerny Tim Blake Nelson
| music          = Don Macdonald
| cinematography = Jan Kiesser
| editing        = Roger Mattiussi
| studio         = Anagram Pictures British Columbia Film Commission Telefilm Canada Lions Gate Films Roadside Attractions TVA Films
| released       =   |2007|03|9|Canada|ref2=   }}
| runtime        = 91 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          =  $426,224   
}}
Fido is a 2006 Canadian zombie comedy film directed by Andrew Currie and written by Robert Chomiak, Currie, and Dennis Heaton from an original story by Heaton. It was produced by Blake Corbet, Mary Anne Waterhouse, Trent Carlson and Kevin Eastwood of Anagram Pictures, and released in the United States by Lions Gate Entertainment.

== Plot == alternate universe where radiation from space has turned the dead into zombies. This resulted in the "Zombie Wars", where humanity battled zombies to prevent a zombie apocalypse, with humanity the ultimate victor. The radiation still plagues humanity, as all those who die after the original contamination turn into the undead, unless the dead body is disposed of by decapitation or cremation. In order to continue living normal lives, communities are fenced with the help of a governing corporation named Zomcon. Zomcon provides collars with accompanying remote controls to control the zombies hunger for flesh so as to use them as slaves or servants.

In the town of Willard housewife Helen Robinson (Carrie-Anne Moss) buys a zombie in spite of her husband Bills (Dylan Baker) zombie phobia, as Bill has had bad experiences with zombies having been a veteran of the Zombie Wars. Their son, Timmy (KSun Ray), befriends the zombie, naming him "Fido" (his true name is never revealed, and little is revealed of his "pre-zombie" life, except that he died of myocardial infarction, and at one point Helen wishes she had met him before she got married and when he was still alive). One day, Fidos collar malfunctions and he kills their next door neighbor, who turns into a zombie. Timmy "kills" the zombified neighbor later, but not before she kills and infects another person, causing a small outbreak. Zomcom security forces quell the situation and then investigate what caused the outbreak.

When a pair of local bullies are blamed for the missing neighbor, they capture Fido and Timmy. Fido escapes and runs to find Helen, who comes and rescues Timmy from the bullies (who, through misadventure and Fidos hunger for human flesh, are now zombies), and they try to forget about the whole thing. Several days later, the neighbors body is found and the murder is traced back to Fido, who is taken away to Zomcon where the public is told he will be destroyed. Timmy learns through Cindy Bottoms (Alexia Fast), daughter of Jonathan Bottoms (Henry Czerny), Zomcons abusive security chief, that Fido is simply working in a factory at Zomcon. Timmy sets out to rescue him with the help of Mr. Theopolis (Tim Blake Nelson), previous security chief of Zomcon who was forced into early retirement when it was discovered he was found guilty of fraternization with his female zombie, whom he has remarkably preserved well to retard her decaying process, thus giving her a relatively attractive appearance.

Meanwhile, Timmy locates Fido, but is captured by Mr. Bottoms, who attempts to throw Timmy into the zombie-infested "wild zone" that exists outside of the fenced communities. Bill comes to the rescue and is killed by Mr. Bottoms, who in turn is killed by Fido. Timmy is set free and the news media propagandizes that the Zomcom security breach was the fault of rednecks who venture out into the wild zone to hunt zombies for fun. Helen finally learns not to belittle Bills bad experiences from the Zombie Wars by paying for a headless funeral in order to prevent his zombification. The film ends with Fido as a surrogate father and husband, Timmy, Helen and Helens newborn baby by Bill as a new family. They, along with a few neighbors happily enjoy their new domestic lives together, including the zombified Jonathan Bottoms who is now more attentive to his daughter.

==Cast==
* KSun Ray as Timmy Robinson
* Billy Connolly as Fido
* Carrie-Anne Moss as Helen Robinson
* Tim Blake Nelson as Mr. Theopolis
* Dylan Baker as Bill Robinson
* Henry Czerny as Jonathan Bottoms
* Sonja Bennett as Tammy
* Alexia Fast as Cindy Bottoms
* Aaron Brown as Roy Fraser
* Brandon Olds as Stan Fraser
* Jennifer Clement as Dee Dee Bottoms
* Rob LaBelle as Frank Murphy
* Tiffany Lyndall-Knight as Miss Mills
* Mary Black as Mrs. Henderson
* Raymond E. Bailey as Floyd

== Production == The Night Peyton Place, and George A. Romeros zombie films. The script was originally written in 1994, but creative differences kept it tied up. Eventually, Currie optioned it through his production company and completed the picture. The film was completely storyboarded prior to filming. 

== Release == premiered at 2006 Vancouver 2007 Gérardmer Fantasy Filmfest in France. The theatrical release was March 9, 2007. 

=== Box office ===
The film grossed $304,533 in the US and a total of $426,224 worldwide.  Domestic DVD sales were $2.95 million. 

=== Home media ===
The DVD was released on October 23, 2007. 

== Reception == Douglas Sirkian Time Out New York rated the film 2/5 stars and called its satire tiresome and overdone.   J. R. Jones of the Chicago Reader called it a "tired spoof" based on a throwaway gag from Shaun of the Dead.   Rob Nelson of The Village Voice called it an unfunny zombie parody that pales in comparison to 28 Weeks Later. 

== References ==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 