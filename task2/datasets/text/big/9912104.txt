El Hijo del crack
{{Infobox film
| name           =El Hijo del crack
| image          =El Hijo del crack.jpg
| image_size     =
| caption        =
| director       =Leopoldo Torre Nilsson and Leopoldo Torres Ríos
| producer       =Armando Bo
| writer         =
| narrator       =
| starring       =Armando Bo, Óscar Rovito, Miriam Sucre, Francisco Pablo Donadio 
| music          =Alberto Gnecco and José Rodríguez Faure
| cinematography =Enrique Wallfisch
| editor       =Rosalino Caterbeti
| studio    =Sociedad Independiente Filmadora Argentina 
| released       =December 15 1953
| runtime        =77 minutes
| country        = Argentina Spanish
| budget         =
}} 1953 Argentina|Argentine football drama film co-directed by Leopoldo Torre Nilsson and Leopoldo Torres Ríos    and starring Armando Bo and Oscar Rovito. The film, a tale of a dwindling professional football star and his son was released on December 15, 1953     in Normandie cinema in Buenos Aires.  The cast involved major professional football players of the time as  Mario Boyé, Tucho Méndez and Ángel Labruna and journalists such as Fioravanti. It is the last film in which Leopoldo Torres Ríos and Leopoldo Torre Nilsson (father and son      ) worked together. The 77 minute film  was produced by Sociedad Independiente Filmadora Argentina  (SIFA). 

==Plot==
 
Mario Lopez (Oscar Rovito) is a child, the son of an aging footballer (Armando Bo) .  On the one hand, while his father is disowned by supporters for being no longer physically able to play it,  he tries to convince himself  that this is a temporary decline and he will return to his former star status. On the other hand, his mother and his maternal grandfather, reject the world of football and the street, arguing that it is a primitive world and inadequate, isolating him. Only his son remains a major fan. Dying from a serious illness, he tries to please his fans once more and regain his legendary status.
 
 
 
 
 
 
 
==Cast==
*Armando Bo as Héctor Balazo López
*Óscar Rovito as Mario López
*Miriam Sucre as María del Carmen de López
*Francisco Pablo Donadio as Alvarado
*Pedro Laxalt 		
*Héctor Armendáriz 
*Alberto Rinaldi 		
*Rolando Dumas 			
*Nelson de la Fuente 			
*Carlos Benso 	

==Reception==
  on set during filming]]
The International Film Guide described the film as a "purely commercial work", unlike many of Torre Nilsons other films such as   highlighted the strong presence of the father and son in the film and noted its neorealist elements and charge.   

==References==
 

==External links==
 
*  

 
 
 
 
 
 
 