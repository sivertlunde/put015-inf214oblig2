The Devil-Ship Pirates
 
 
{{Infobox film
  | name =The Devil-Ship Pirates
  | image    = "The_Devil-Ship_Pirates"_(1964).jpg
  | caption  = UK theatrical poster
  | director = Don Sharp
  | producer = Anthony Nelson Keys	
  | starring =Christopher Lee John Cairney Barry Warren Andrew Keir Philip Latham
  | music = Gary Hughes Michael Reed
  | editing =	James Needs
  | studio =        Hammer Film Productions
  | distributor = Associated British Picture Corporation|Warner-Pathé Distributors (UK) Columbia Pictures (US)
  | released =  1964
  | runtime = 86 min
  | country = United Kingdom English
  }}
The Devil-Ship Pirates is a 1964 British-made pirate adventure film directed by Don Sharp.

It concerned pirates from a vessel from the defeated Spanish Armada terrorizing citizens on the English coast. All goes well until the villagers realize the Spaniards have been defeated and revolt. It had some spectacular swordplay but was a largely land-locked pirate movie.

==Plot Summary==
A pirate ship, fighting in 1588 on the side of the Spanish Armada, suffers extensive damage and must put into a village on the British coast for repairs. The village is small and isolated and the Spanish convince the villagers that the English fleet has been defeated and that they, the Spanish, are now their masters. This results in the villagers sullen cooperation, but rumors and unrest begin to spread and soon the Spanish pirates find themselves facing a revolt.

 
==Cast==
* Christopher Lee as Captain Robeles
* Barry Warren as Don Manuel Rodriguez de Savilla 
* John Cairney as Harry 
* Andrew Keir as Tom, Harrys Father
* Duncan Lamont as The Bosum 
* Michael Ripper as Pepe
* Suzan Farmer as Angela Smeeton

==Production==
The outdoor sets were previously utilised for Hammers The Scarlet Blade, made the previous year. Ripper, Lamont and Farmer appeared in both films.

According to Christopher Lee, Hammer Studios had built a full-sized galleon in some sand pits on a steel structure under the water. Although warned not to have too many people on board at once, one day the tea boat was lifted onto a platform level with the water and too many people rushed over to get a cup of tea. The ship capsized, throwing most of the cast and crew in the water. Lee was on the poop deck and luckily managed to hold on to the rail. No one was drowned or seriously hurt. Yoram Allon, et al (eds.) Contemporary British and Irish Film Directors, London: Wallflower, 2001, p.310 

==Reception==
The Devil-Ship Pirates is a "lacklustre pirate yarn with not much action and some elements of Hammer horror" according to Leslie Halliwell|Halliwells Film and Video Guide.  Richard Harland-Smith it is a "spirited romp", but notes that the films "diet of floggings, hangings and swordplay pushed its U certificate to the limits." 

==References==
 

== External links ==
*  
*  at Trailers from Hell
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 