Miss Lulu Bett (film)
{{Infobox film
| name           = Miss Lulu Bett
| image          = Miss Lulu Bett 1921 lobby card.jpg
| image_size     = 
| caption        = 1921 lobby card
| producer       = Adolph Zukor
| director       = William C. deMille
| writer         = Clara Beranger
| based on       =   Lois Wilson Charles Ogle Peaches Jackson Carrie Clark Ward
| music          = 
| cinematography = L. Guy Wilky
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       = December 25, 1921(premiere) 1922(nationwide)
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
  silent comedy play and novel of the same name by Zona Gale. The screenplay was written by Clara Beranger, and the film was directed by William C. deMille. 

In 2001, this film was deemed "culturally, historically, or aesthetically significant" by the United States Library of Congress and selected for preservation in the National Film Registry.

==Cast== Lois Wilson - Lulu Bett
*Milton Sills - Neil Cornish
*Theodore Roberts - Dwight Deacon
*Helen Ferguson - Diana Deacon
*Mabel Van Buren - Ina Deacon
*Mae Giraci - Monona Deacon (billed May Giraci)
*Clarence Burton - Ninian Deacon
*Ethel Wales - Grandma Bett
*Taylor Graves - Bobby Larkin Charles Ogle - Station Agent

uncredited
*Peaches Jackson - Child
*Carrie Clark Ward - Gossip

==References==
 

==External links==
 
*  
*   at AllMovie
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 