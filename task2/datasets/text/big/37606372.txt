Angry Video Game Nerd: The Movie
 
{{Infobox film
| name = Angry Video Game Nerd: The Movie
| image = File:Film_Poster_for_AVGN_The_Movie.jpg
| caption = Theatrical release poster James Rolfe
| producer = Sean Keegan Executive: April Rolfe James Rolfe
| writer = Kevin Finn James Rolfe
| based on =  
| starring = James Rolfe Sarah Glendening Stephen Mendel Helena Barrett Time Winters Eddie Pepitone Bobby Charles Reed Jeremy Suarez
| music = Bear McCreary 
| cinematography = Jason Brewer
| editing = Paul Fontaine Michael Licisyn James Rolfe (final editing)
| studio = Cinemassacre Productions Skinny Ugly Pilgrim
| distributor = Devolver Digital   
| released =  
| runtime = 115 minutes
| language = English
| country = United States
| budget = $325,327
| gross =
}}
 independent Comic science fiction James Rolfe. web series of the same name, also created by Rolfe, with himself as the title role. The film premiered July 21, 2014 at Graumans Egyptian Theatre in Hollywood, and was released online via video-on-demand on September 2, 2014. The Blu-Ray version of the film was released on December 14, 2014 through Amazon.com, with the DVD version coming in the future. The films budget of over   came entirely from Internet crowdfunding.   
 mass burial of millions copies of the Atari 2600 video game E.T. the Extra-Terrestrial (video game)|E.T. the Extra-Terrestrial, proclaimed as the "worst video game of all time". After a longstanding refusal to address the game in his web series, the Nerd succumbs to pressure by fans to review the video game, embarking on a quest to prove that there is nothing buried there. The crew is pursued by federal authorities, led by the villainous General Dark Onward, who believes he is investigating Area 51 and the crash of an unidentified flying object.

==Plot== burial of 2 million copies of the "worst video game of all time", E.T. the Extra-Terrestrial (video game)|Eee Tee for the Atari 2600. In the present day, game executive Mandi (Sarah Glendening) of Cockburn Industries, Inc. proposes to her bosses creating an intentionally badly made sequel, "Eee Tee 2". Thanks to the massive popularity and success of the Angry Video Game Nerd, the sales of poorly made video games has increased dramatically, and a review of Eee Tee 2 by the Nerd would cause his fans to buy the game and make profits for Cockburn.
 James Rolfe) and Cooper Folly (Jeremy Suarez) are working on the formers latest video game review. The Nerd has become increasingly disheartened over the years, as his fans continue to buy and play the video games he reviews and warns people to stay away from. On top of this, the Nerd is forced to promote and sell bad video games as part of his job at GameCops, and when he discovers marketing for Eee Tee 2, his fans encourage him to review E.T., something the Nerd has stood against for years because the game is so bad that it scarred him as a child. However, after a bad nightmare and some personal thought, the Nerd decides to go to Alamagordo, New Mexico in order to ultimately debunk the conspiracy surrounding the buried cartridges. He is accompanied by Cooper and Mandi, and the trip is completely funded by Cockburn Industries.

While filming their expedition, Cooper reveals that he believes in a super-being known as Death Mwauthzyx, who has the power to collapse all of the dimensions in the universe and destroy all existence. Suddenly, the legless General Dark Onward (Stephen Mendel), thinking the trio is looking for extraterrestrials, brings along Sergeant McButter (Helena Barrett) in an attempt to capture them. In the process, Onward accidentally blows up his right arm with a grenade, giving the trio enough time to escape.

The Nerd, Cooper, and Mandi decide to search for the creator of Eee Tee, Howard Scott Warshaw, for answers. They instead stumble across the home of Dr. Zandor (Time Winters), who tells them that "Eee Tee"s level design is actually an exact map of Area 51. Dr. Zandor, a worker of Area 51 at the time, gave the code to Warshaw to help him meet the five-week deadline Atari set for "Eee Tee"s completion, and also to exact revenge on the government for kidnapping and holding hostage an alien he was attempting to free. The government then ordered the burial of the cartridges, supervised by General Onward himself, while Zandor escaped with the metallic material Area 51 was researching at the time in an attempt to reassemble the aliens spaceship. Dr. Zandor manages to keep the trio safe from the government for the night, but Mandi is captured by McButter while outside of the house. The Nerd and Cooper, believing she is a double agent, do not go after her.

Going back to the Alamagordo site, the Nerd and Cooper discover a large crowd of fans and the head of Cockburn Industries promoting the release of Eee Tee 2 with the promise of digging out a copy of the original Eee Tee from the site itself. The Nerd tells his fans that there are no cartridges buried out there, but Howard Scott Warshaw himself appears and tells fans the opposite. Annoyed, the Nerd decides to break into Area 51. He successfully does so, but is captured by General Onward. Having discovered who the Nerd really is, Onward attempts to force him to play E.T., but that fails, and the Nerd tells him that when Zandor escaped with the spaceship metal, he replaced it with tin foil. Onward launches a missile at Mount Fuji, the basis for the Atari logo design, and while angrily leaving the room, gets his left arm cut off in the door, thus leaving him with no arms. During the launch countdown, an alien resembling the one in E.T. grabs the Nerd and pulls him to safety.
 Eiffel Tower in Las Vegas.

The Nerd and the alien escape in a fighter jet similar to one in the NES Top Gun (video game)#Gameplay|Top Gun video game, while the alien reveals Death Mwauthzyxs can collapse the dimensions and destroy all existence by completely turning the satellite dish on his head. Cooper is captured by Death Mwauthzyx and brought to Las Vegas, where Mandi manages to knock McButter off the Eiffel Tower to her death. Mandi is also captured by Death Mwauthzyx. The Nerd and the alien crash-land at the Alamagordo site, where a captured Dr. Zandor shouts to them that he hid the alien spaceship metal inside the millions of E.T. game cartridges. The alien summons every single copy of the game to form the spaceship. The Nerd and the alien leave for Las Vegas to stop Death Mwauthzyx, and the armless General Onward is killed when attempting to stop them.

In Las Vegas, the Nerd and the alien save Cooper and Mandi. The Nerd then fires a laser at Death Mwauthzyxs satellite dish, causing Death Mwauthzys to fly away and disappear forever. They return to the Alamagordo site and reunite with Dr. Zandor and the Nerds fans. Cooper and Mandi share a kiss, while the Nerd decides to finally review E.T. for his fans before the alien leaves for good though he reviews the sequel first declaring that while it is worse, true wretchedness was too unique to duplicate and that the original stood the test of infamy.  Everyone, including Mandi, throw the copies of the sequel into the landfill and the Nerd finally reviews E.T. 

During the credits, the Nerd declares that E.T. is not the worst game that he has ever played, calling it cryptic and challenging as well as addicting, although it is still a bad game.

==Cast==
{{columns|colwidth=30em
|col1= James Rolfe the Nerd
*Jeremy Suarez as Cooper
*Sarah Glendening as Mandi
*Stephen Mendel as General Dark Onward
*Helena Barrett as Sergeant McButter
*Time Winters as Dr. Zandor
*Bobby Charles Reed as Bernie Cockburn
*Eddie Pepitone as Mr. Swann
*Robbie Rist as Alien (voice)
*Matt Brewer as Young Zandor
*Nathan Barnatt as Keith Apicary
|col2=
;Cameos
*Andre Meadows as Himself (store customer)
*Howard Scott Warshaw as Himself
*Kyle Justin as Guitar Guy
*Mike Matei as Himself
*Pat Contri as Disappointed Gamer #1
*Malcolm Critchell as British Guy
*Bear McCreary as Zombie Doug Walker as the Nostalgia Critic
*Lloyd Kaufman as Himself
*Jason Janes as Misc. Role / Extra
*Justin Carmical as Misc. Role / Extra
}}

==Development== The Angry GamesTM magazine that he would be playing himself in the movie.  Production of the film was delayed for several years due to the busy production schedule of Rolfes AVGN web series, wherein Rolfe was continuously filming two episodes per month.
 burial of over 2 million copies of the proclaimed "worst video game of all time", E.T. the Extra-Terrestrial (video game)|E.T. the Extra-Terrestrial for the Atari 2600, named Eee Tee in the film itself. Finally rescinding his longstanding refusal to address the game in the web series, the Nerd succumbs to pressure by fans to review the video game, embarking on a quest to prove that there is nothing buried there. The crew is pursued by federal authorities, led by the villainous General Dark Onward, who believes he is investigating Area 51 and the crash of an unidentified flying object.    The film was said to be in the vein of Waynes World (film)|Waynes World in the sense that it will be a movie about a person that reviews games, rather than a feature length review or multiple reviews adding up to a feature length.   

The films budget of more than   was secured entirely via Internet crowdfunding, Indiegogo. Filming in California started April 1, 2012, and wrapped on May 11, 2012. Additional scenes were being filmed in the actors spare time, mainly in Philadelphia. Production officially ended December 2013.

Rolfe consistently utilized online articles and videos to document the movies development and to solicit talent for casting and crew. Open casting calls were held, including one hosted by Channel Awesome held in Chicago, with live auditions held by one of the films actors, Douglas Walker, also the actor of the Nostalgia Critic.    Rolfe asked for his fanbase to provide fictional webcam footage of themselves reacting to the Nerds webseries to be used in a sequence at the beginning of the film which introduces the Angry Video Game Nerd character.   
 B movie feel.   

==Soundtrack== How The Nerd Stole Christmas". McCreary utilized rock-and-roll music, heavy metal music, a symphonic orchestra, and synthesized elements from Nintendo Entertainment System|NES, Super Nintendo Entertainment System|SNES, and SEGA Genesis hardware to compose the score.  The album features two remixes by McCreary, as well as two songs written by Brendan McCreary and performed by the band Young Beautiful in a Hurry.
The album was released on the iTunes Store on September 2, 2014.   

All music was composed by McCreary, except where otherwise noted.
{{Infobox album  
| Name = Angry Video Game Nerd: The Movie (Original Motion Picture Soundtrack)
| Type = Film
| Artist = Bear McCreary
| Cover = 
| Border = yes
| Alt =
| Released =  
| Recorded = 2014
| Genre = Various
| Length =  
| Label = 
| Chronology = 
| Producer =
}}

{{Track listing
| collapsed = no
| extra_column = Performer
| total_length = 1:19:00

| title1 = Theme from Angry Video Game Nerd: The Movie
| length1 = 4:41

| title2 = Nerds Before Birds
| extra2 = Young Beautiful in a Hurry
| length2 = 3:55

| title3 = Nerd Nightmares
| length3         = 3:42

| title4          = The Landfill
| length4         = 1:54

| title5          = Humvee Chase
| length5         = 2:43

| title6          = Barcade
| extra6          = Young Beautiful in a Hurry
| length6         = 2:44

| title7          = The Story of Death Mwauthzyx
| length7         = 1:54

| title8          = Save the Fans
| length8         = 3:31

| title9          = Zandors Tale
| length9         = 4:07

| title10         = Howard Scott Warshaw
| length10        = 4:43

| title11         = Sacred Ground of the Golden Turd (Bear McCreary Remix)
| extra11         = Kyle Justin
| length11        = 2:16

| title12         = General Dark Onward
| length12        = 5:50

| title13         = Unidentified Flying Nerd
| length13        = 2:56

| title14         = Killer Robots
| length14        = 2:25

| title15         = Death Mwauthzyx Rises
| length15        = 5:44

| title16         = The Nerdy Hero
| length16        = 10:15

| title17         = Birds Before Nerds
| length17        = 2:28

| title18         = Source Music Medley
| length18        = 5:31

| title19         = Maverick Regeneration
| length19        = 4:11

| title20         = The Angry Video Game Nerd Theme Song (Bear McCreary Remix)
| extra20         = Kyle Justin
| length20        = 2:54
}}

==Reception==
The Hollywood Reporter called it an "overly long and almost obsessively self-indulgent" and "aspiring cult film" with production value which "hovers above home video|home-video quality by a few admirable notches", noting that the "filmmakers manage to capably anchor these disparate storylines to their central plot concerning crusading gamers".   

The Michigan Daily student newspaper of the University of Michigan gave this film a mostly negative review, describing it as unfunny, poorly edited, badly paced, and too long. This review argued that the soundtrack by Bear McCreary was good and the best aspect of the film. The reviewer noted that The Angry Video Game Nerd was "the pioneering internet gamer show", which he had enjoyed greatly, so the film was a "disappointing failure".   

==References==
 

==Further reading==
* {{Cite web
| author=Gerard, Peter
| date=April 9, 2015
| url=https://vimeo.com/blog/post:704
| title=Its all about the karma: how Angry Video Game Nerd delighted its fans and made more sales
| publisher=Vimeo
| archiveurl=https://web.archive.org/web/20150409222202/https://vimeo.com/blog/post:704
| archivedate=April 9, 2015
}} 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 