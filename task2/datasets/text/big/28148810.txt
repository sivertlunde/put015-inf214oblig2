The Heroic Captain Korkorán
{{Infobox film
| name =The Heroic Captain Korkorán
| image =Hrdinný kapitán Korkorán.jpg
| image size =
| alt =
| caption =
| director = Miroslav Cikán
| producer = Vladimír Kabelík
| writer = Miroslav Cikán Emil Artur Longen Jaroslav Mottl
| narrator =
| starring = Vlasta Burian
| music =
| cinematography = Václav Vích
| editing = Antonín Zelenka
| studio =
| distributor =
| released = 24 August 1934
| runtime =
| country =Czechoslovakia
| language =Czech
| budget =
| gross =
| preceded by =
| followed by =
}} Czech comedy film directed by Miroslav Cikán. It stars Vlasta Burian, Jirina Stepnicková and Milada Smolíková. The film is about a Prague skipper who dreams of commanding ships on the high seas but is stuck with running a small steamboat on the Vltava river. 

==Cast==
* Vlasta Burian - Adam Korkorán
* Jirina Stepnicková - Irena Svobodová
* Milada Smolíková - Mrs. Broniková Theodor Pištěk - Mr. Dlouhý
* Jaroslav Marvan - Inspektor paroplavební spolecnosti
* Svetla Svozilová - Chambermaid
* Cenek Slégl - Emeritus admiral Piacci
* Ladislav Hemmer - Secretary Eman Fiala - Stoker
* Josef Kotalík - Director of the steam-navigation
* Bohumil Mottl - Vláda
* Karel Nemec - Servant
* Karel Postranecký - Balvan, greaser
* Jan W. Speerger - Rating
* Josef Waltner - Hotel manager

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 
 