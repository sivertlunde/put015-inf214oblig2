7 Grandmasters
{{Infobox film
| name           = 7 Grandmasters
| image          = 
| alt            =  
| caption        = 
| director       = Joseph Kuo
| producer       = Joseph Kuo
| writer         = Yao Ching Kang, Joseph Kuo, Raymond To
| starring       = Mark Long, Jack Long, Corey Yuen, Lee Yi Min
| music          = Chow Fook Leung
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 84 minutes
| country        = Taiwan Mandarin
| budget         = 
| gross          = 
}} kung fu film directed by Joseph Kuo, starring Mark Long, Jack Long, Corey Yuen and Lee Yi Min. It was filmed in Standard Chinese|Mandarin, as opposed to Cantonese which is more common due to Hong Kongs enormous output on the genre.

==Plot==
Sang Kuan Chun is an old kung fu master who is getting ready to retire from martial arts.  But just as he is about to put up the kings signboard and call it quits, he receives a note alleging that hes not the best. Thus begins his journey for one last challenge with each of the Seven Grandmasters to prove his superiority. As Sang Kuan Chun and his three students travel from one challenge to the next, the foursome acquires a fifth—a young man named Siu Ying who wants desperately to train under master Sang Kuan Chun to avenge his fathers death. So he tags along, despite the masters insistence that he will not accept any more students. Eventually we learn more about the masters past.
 Pai Mei Twelve Strikes. However a masked man soon stole several pages of the book, leaving only nine strikes. So, somewhere out there, is this unknown man, and he has the final three strikes of Pai Mei, which are the most deadly and can beat even the other nine strikes. Sang Kuan Chun soon accepts the seemingly devout Siu Ying and teaches him the nine known strikes of Pai Mei. Siu Ying ends up learning from his “uncle” that Sang Kuan Chun (who was set up) killed his father during a friendly tournament. Siu Ying is taught the final 3 strikes from a mysterious figure and almost kills Sang Kuan Chun until he being a loyal good student couldnt break his teachers rule of "Never kill anyone if it can be avoided". This all leads up to an exciting climax, where we learn the identity of the masked man who stole the Pai Mei final strikes and the identity of the man who killed Siu Yings father.

==Cast==
*Li Yi Min as Siu Ying
*Jack Long as Sang Kuan Chun
*Mark Long as Sangs elder student
*Corey Yuen as Sun Hung
*Nancy Yen as Sangs daughter

== External links ==
*  
*  

 
 
 
 



 
 