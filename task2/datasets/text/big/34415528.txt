Café de Flore (film)
{{Infobox film
| name           = Café de Flore
| image          = CafeDeFlore2011Poster.jpg
| alt            = A woman holding a child in her arms, and another set of arms hugging around her waist. 
| caption        = Film poster
| director       = Jean-Marc Vallée
| producer       = {{Plainlist|
* Nicolas Coppermann Pierre Even}}
| writer         = Jean-Marc Vallée
| starring       = {{Plainlist|
* Vanessa Paradis
* Kevin Parent
* Hélène Florent
* Evelyne Brochu}}
| music          = 
| cinematography = Pierre Cottereau
| editing        = Jean-Marc Vallée 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = Canada
| language       = French
| budget         = 
| gross          = 
}}
Café de Flore is a Canadian drama film, released in 2011. Directed, written, and edited by  , January 17, 2012. 

==Plot==
The film cuts between two seemingly unrelated stories. One, set in present-day  s.

==Cast==
*Vanessa Paradis as Jacqueline
*Kevin Parent as Antoine
*Évelyne Brochu as Rose
*Hélène Florent as Carole
*Marin Gerrier as Laurent
*Alice Dubois as Véro

== Reception ==

=== Critical response ===
Rotten Tomatoes gives the film a score of 63% based on reviews from 49 critics, with an average rating of 6.5 out of 10. 

===Accolades===
The films Genie Award nominations included Best Picture, Best Actress in a Leading Role (Paradis), Best Actor in a Supporting Role (Gerrier), Best Actress in a Supporting Role (Florent), Best Director and Best Original Screenplay.  Paradis won the Genie for Best Actress; the film also picked up a Genie for Achievement in Make-Up.

==References==
 

==External links==
 
*  

 

 
 
 
 
 
 
 
 
 
 
 

 