Under the Roofs of Paris
 

{{Infobox film
| name           = Under the Roofs of Paris
| image          = Under the Roofs of Paris poster.jpg
| caption        = Theatrical release poster
| director       = René Clair
| producer       = Films Sonores Tobis
| writer         = René Clair
| starring       = Albert Préjean Pola Illéry
| music          = Armand Bernard Raoul Moretti (songs) René Nazelles (songs)
| cinematography = Georges Périnal Georges Raulet
| editing        = René Le Hénaff
| distributor    =
| released       = 2 May 1930 (France)
| runtime        = 96 minutes
| country        = France
| language       = French Romanian
| budget         =
}}

Under the Roofs of Paris ( ) is a 1930 French film directed by René Clair. The film was probably the earliest French example of a filmed musical-comedy, although its often dark tone differentiates it from other instances of the genre. This was an early example of sound film in France, along with Prix de Beauté and LAge dOr. However, Under the Roofs of Paris was the first French production of the sound film era to achieve great international success.

==Plot==
In a working-class district of Paris, Albert, an impecunious street singer, lives in an attic room. He meets a beautiful Romanian girl, Pola, and falls in love with her; but he is not the only one, since his best friend Louis and the gangster Fred are also under her spell. One evening Pola dares not return home because Fred has stolen her key and she does not feel safe. She spends the night with Albert who, reluctantly remaining the gentleman, sleeps on the floor and leaves his bed to Pola.  They soon decide to get married, but fate prevents them when Émile, a thief, deposits with Albert a bag full of stolen goods.  It is discovered by the police, and Albert is sent to prison. Pola finds consolation with Louis.  Later Émile is caught in his turn and admits that Albert was not his accomplice, which earns Albert his freedom. Fred has just got back together with Pola who has fallen out with Louis, and in a jealous fury at Alberts return Fred decides to provoke a knife fight with him.  Louis rushes to Alberts rescue and the two comrades are re-united, but their friendship is clouded by the realisation that each of them is in love with Pola. Finally Albert decides to give up Pola to Louis.

==Cast==
*Albert Préjean as Albert
*Pola Illéry as Pola
*Edmond T. Gréville as Louis
*Bill Bocket as Bill
*Gaston Modot as Fred
*Raymond Aimos as "un gars du milieu"
*Thomy Bourdelle as François
*Paul Ollivier as the drunken customer in the café
*Jane Pierson as the fat woman with a purse

==Background==
The arrival of synchronised sound in the cinema in the late 1920s provoked mixed reactions among French film-makers, and some of the masters of silent film technique were pessimistic about the impact it would have. In 1927, even before The Jazz Singer had been shown in Paris, René Clair wrote: "It is not without a shudder that one learns that some American manufacturers, among the most dangerous, see in the talking picture the entertainment of the future, and that they are already working to bring about this dreadful prophecy".  Elsewhere he described the talking picture as "a redoubtable monster, an unnatural creation, thanks to which the screen would become poor theatre, the theatre of the poor".  It was therefore an irony that it was Clair who would produce the French cinemas first big international success with a sound picture in Sous les toits de Paris. 

Clair accepted the inevitability of the talking picture but at first retained very specific views about the way that sound should be integrated into film. He was reluctant to use dialogue or sound effects naturalistically, and maintained that the alternate use of the image of the subject and of the sound produced by it - not their simultaneous use - created the best effect. 

In 1929, the German film company Tobis Klangfilm (Tobis Sound-Film) established a studio at Épinay near Paris which was equipped for sound production. This studio inaugurated a policy of making French-speaking films in France rather than importing French performers to make French versions of films in Germany. The company concentrated on prestigious productions, and they recruited René Clair to undertake one of their first French projects with Sous les toits de Paris. 

==Production==
René Clair filmed Sous les toits de Paris at Épinay between 2 January and 21 March 1930.   The setting of the film was defined by the elaborately realistic yet evocative set which  ) which starts among the rooftops and then descends along the street closing in on a group of people gathered around a singer, whose song (the title-song) gradually swells up on the soundtrack. (A reversal of this shot ends the film.) This is the first of many ways in which Clair affirms his loyalty to the style and techniques of silent cinema while creating a distinctive role for the new element of sound. Elsewhere, a conversation is cut off by the closing of a glass door and then has to be followed in dumb-show; the hour of midnight is indicated by the sound of a mere three chimes - and the superimposition of a clockface; and a knife-fight is first seen but not heard as a passing train drowns out all else, and then the fights continuation in darkness is conveyed only by its sounds until the headlights of a car illuminate the scene. Such devices are not only imaginative but amount almost to a satire of the sound film. 

Among the other members of Clairs team on the film were   appeared as an actor in the role of Alberts friend Louis.

When the film first came out, it began with a five-minute sequence outlining the relationships of the main characters, before the spectacular travelling shot that descends from the rooftops.  In later versions this introduction disappeared, perhaps reflecting Clairs second thoughts, and the symmetry of the films beginning and end was allowed to stand out. 

==Reception==
The film was first presented at the Moulin Rouge cinema in Paris from 2 May 1930, advertised as "100% talking and singing in French",  but it did not at first have more than a modest success in its own country. In fact only about one quarter of the film could be described as talking, and this may have contributed to the disappointment with which it was greeted by many Parisians, eager to experience the new medium.   Among the other criticisms which were made by French reviewers were the slowness of the narrative, the conventionality of the characters, and the systematic emphasis on the Paris of hoodlums and the underworld. 

The director of the French branch of Tobis, Dr Henckel, had given Clair complete freedom to make the film, but after the Paris opening he told Clair that it was now clear what others thought of his methods, and that in future he would have to resign himself to giving the audience what they wanted - talking pictures that really talked. 

However a gala screening of the film, attended by Clair, was arranged in Berlin in August 1930, and there it was greeted as a triumph. Its run in German cinemas continued for several months. This success was repeated when the film appeared in New York and in London (both in December 1930), and it was also well received in Tokyo, Shanghai, Moscow and Buenos Aires. 
 Jacques Brunius and Henri-Georges Clouzot,  found greater support, and the originality of the approach to sound was better appreciated. René Clair later recalled that the profits were such that the cost of the film, which was considerable, was covered by the returns from a single cinema. 
 La Belle Le Crime de Monsieur Lange (1936). 

Modern judgments of the film, while acknowledging its importance for its time, have tended to find it limited by its nostalgic portrayal of the "little people" of Paris and by its "studio artifice"; in the words of one critic, it tends to "smother cinematic interest with the sheer cleverness of the conception and the technical mastery of the execution".  There are hesitations in its continuity and pacing, and uncertainty in some of the performances as they try to adapt to the spoken word.   On the other hand, questions which Clair was addressing about the appropriate use of sound in an essentially visual medium continue to be valid, and his film remains a witty exploration of some of the possible answers. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 