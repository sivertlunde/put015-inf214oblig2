Falcon Rising
{{Infobox film
| name           = Falcon Rising
| image          = FalconRisingPoster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Ernie Barbarash
| producer       = Shahar Stroh Etchie Stroh
| writer         = Y.T. Parazi
| casting        = Ferne Cassel, C.S.A
| starring       = Michael Jai White Neal McDonough Laila Ali
| music          = Neal Acree
| cinematography = Yaron Levy
| editing        = Peter Devaney Flanagan
| studio         = Moonstone Entertainment Strohberry Films
| distributor    = Freestyle Releasing
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $4.5 million (estimated) 
| gross          = $11,774 
}}
 American action film|action/adventure film directed by Ernie Barbarash and starring Michael Jai White, Neal McDonough, Laila Ali and Masashi Odate. Formerly titled Favela, Falcon Rising is the first installment of Moonstone Entertainments "CODENAME: FALCON" action franchise which will revolve around former Marine character John "Falcon" Chapman, "a dark anti-hero driven by guilt, who will destroy himself unless given something else to destroy - a useful weapon-of-last-resort for the foreign ministry."  Falcon Rising had a limited theatrical release on September 5, 2014.

==Synopsis==
John "Falcon" Chapman (Michael Jai White|White), is an ex-Marine anti-hero plagued with a terrible secret consuming him with guilt. On the self-destructive edge, he learns his sister Cindy (Laila Ali|Ali) has been brutally beaten in the slums, or “favelas”, of Brazil and travels there to hunt down her attackers. In the process he discovers an underground world of drugs, prostitution, and police corruption ruled by the Japanese mafia and protected by the powerful Hirimoto (Odate). 

==Cast==
* Michael Jai White as John Falcon Chapman
* Neal McDonough as Manny Ridley
* Laila Ali as Cindy Chapman
* Jimmy Navarro as Thiago Santo
* Millie Ruperto as Katarina Da Silva Lateef Crowder as Carlo Bororo
* Masashi Odate as Hirimoto
* Hazuki Kato as Tomoe

==Release==
Falcon Rising was released on Video on Demand (VOD) platforms on September 4, 2014 and had a limited release in ten North American theaters on September 5, 2014.  The film grossed $8,691 in its opening weekend and grossed $11,774 by the end of its theatrical run.  Falcon Rising was released on DVD on October 27, 2014. 

==Sequel==
Moonstone Entertainment has announced that it has begun pre-production of the second installment in the "CODENAME: FALCON" franchise, titled Flight of the Falcon. 

==References==
 

== External links ==
*  
*  
*  

 

 
 
 


 