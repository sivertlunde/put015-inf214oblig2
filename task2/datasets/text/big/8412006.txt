The Cry Baby Killer
{{Infobox Film
| name           = The Cry Baby Killer
| image          = Cry Baby Killer.jpg
| border         = yes
| caption        = Theatrical release poster Joe Addis
| producer       = Roger Corman David Kramarsky David March
| writer         = Leo Gordon Melvin Levy
| starring       = Harry Lauter Jack Nicholson Carolyn Mitchell
| music          = Gerald Fried
| cinematography = Floyd Crosby
| editing        = Irene Morra
| distributor    = Allied Artists Pictures Corporation
| released       = August 17, 1958
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
}}
The Cry Baby Killer is a 1958 cult film produced by Roger Corman. It was the feature film debut of Jack Nicholson. Until recently, the film was out of print and hard to find. In 2006, it was issued on DVD for the first time by Buena Vista Home Entertainment as part of their Roger Corman Classics series.

==Plot==
 
17 year old juvenile delinquent, Jimmy Walker panics after he thinks he has committed manslaughter while fighting with a couple of teenage hoodlums. Walker then takes several people hostage, one a small infant, and threatens them if they try to escape. All the while police have Walker surrounded and prepare to rescue the hostages.  

==Production==
Corman later claimed the film was the first movie he produced which did not make money, although he said it earned its costs back off television rights. He also says he was out of the country during pre-production and much of the script was changed by the producer. Corman returned to Hollywood two days before filming began and tried to put the things back in the film but only managed to get some of them in. Ed. J. Philip di Franco, The Movie World of Roger Corman, Chelsea House Publishers, 1979, page 16–17. 

==Cast==
*Harry Lauter as Police Lt. Porter 
*Jack Nicholson as Jimmy Walker
*Carolyn Mitchell as Carole Fields 
*Brett Halsey as Manny Cole
*Lynn Cartwright as Julie
*Barbara Knudson as Mrs. Maxton 
*William A. Forester as Carl Maxton 
*John Shay as Police Officer Gannon 
*Ralph Reed as Joey
*Bill Erwin as Mr. Wallace
*Ed Nelson as Rick Connor
*Smoki Whitfield as Sam

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 


 