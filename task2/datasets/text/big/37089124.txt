Journey to the West: Conquering the Demons
{{Infobox film
| name           = Journey to the West:  Conquering the Demons
| image          = JourneytotheWestConqueringtheDemons.jpg
| alt            = 
| caption        = 
| director       = Stephen Chow
| producer       = {{Plainlist|
* Stephen Chow
* Wang Zhonglei
}}
| writer         = {{Plainlist|
* Stephen Chow
* Derek Kwok
* Xin Huo
* Yun Wang
* Fung Chih Chiang
* Lu Zheng Yu
* Lee Sheung Shing
* Y.Y. Kong
}}
| starring       = {{Plainlist|
* Shu Qi
* Wen Zhang
* Huang Bo Show Lo
* Chrissie Chau
}} Raymond Wong
| cinematography = Sung Fai Choi
| editing        = Chi Wai Chan
| production companies = {{Plainlist|
* Bingo Movie Development Village Roadshow Pictures Asia Chinavision Media Group
* Edko Films
* Huayi Brothers China Film Group
}}
| distributor    = Edko Films
| released       =  
| runtime        = 110 minutes
| country        = China  Hong Kong Mandarin 
| budget         = 
| gross          =  US$215 million   
}} fantasy comedy Chinese literary classic written by Wu Chengen. 

==Plot== Tang Sanzang got his disciples and embarked on the Journey to the West.
 mysterious underwater humanist approach and to use nursery rhymes to coax goodness out of demons, a tactic Duan scoffs at. Disillusioned, Sanzang meets back up with his master and bemoans his lack of capabilities in comparison to more aggressive demon-hunters such as Duan. His master reaffirms his philosophy of trying to reform evil demons and sends him off again, telling him to find "enlightenment."
 pig demon reanimated corpses of the demons victims, as well as the demons iconic nine-pronged weapon. Sanzang gets into a battle with one of the corpses but Duan arrives and destroys all of them. Duan then battles the demon. In the ensuing battle, the building collapses and Sanzang and Duan retreat as the demon is momentarily injured. Duan then develops a strong limerence towards Sanzang after being impressed by his selfless ideals. She wishes to kiss him, but he quickly flees, not wishing to deal with romantic love in his quest for nirvana.

Sanzangs master advises him to tame the Monkey King demon Sun Wukong (trapped by Buddha) to subdue the pig demon. That night, he is captured by a gang that had also subdued Duan. It is later revealed to be a ploy orchestrated by Duan to trick Sanzang into having sex. After Sanzang rejects her again, she has him imprisoned. The pig demon reappears and injures Duan but is chased off by a trio of rivaling demon-hunters. Duan views Sanzangs concern for her injuries as a romantic attraction. After Sanzang refuses her advances again, she destroys his book of nursery rhymes and he promptly leaves.

After days of traveling, Sanzang finally discovers the hole where Monkey King was trapped in for five-hundred years. Monkey King informs him to use a dancer under the full moon as bait. Duan appears and volunteers to dance. The pig demon appears and falls into Monkey Kings hole, turning into a miniature pig, which Duan turns into a puppet. Duan then gives both the fish and pig puppets to Sanzang and offers her golden ringed weapon as an engagement band, but he rejects her again. She leaves after returning his nursery rhyme book, which she had pieced back together, although at random due to being bad at reading.

Monkey King successfully tricks Sanzang into removing the seal on his prison. The three demon hunters appear to catch Monkey King but he kills them all after a brutal fight. Injured, Sanzang then begins to pray to Buddha, and in retaliation Monkey King rips the hair from his head. Duan arrives to save Sanzang, enraged and attacks but the Monkey King mortally injures her. Sanzang finally tells Duan he loves her. Monkey King proceeds to destroy her body, but Sanzang is able to summon Buddha, when it is revealed that Duan accidentally reassembled the words of his book into those of the Buddha Sutra. Buddha, in vast form, defeats Monkey King with the palm of his hand. Sanzang then places Duans golden ring atop Monkey King as his iconic restrictive headband.

Sanzang tells his master that his suffering due to Duans loss has helped him to enlightenment. Sanzang is then instructed to journey west(India) for the Buddhist sutras of Leiyin Temple, and it is shown that the Water Demon, Pig Demon, and Monkey King have been tamed and turned into humans named, respectively, Sandy, Pigsy, and Sun Wukong. As they hike across the desert, Sanzang looks across the sand and sees an image of Duan.

==Cast==
*Shu Qi as Duan Tang Sanzang
*Huang Bo as Sun Wukong (Monkey King)
*Show Luo as Prince Important
*Lee Sheung Ching as Sand Monk
*Chen Bing Qiang as KL Hog
*Cheng Si Han as Master Nameless
*Xing Yu as Fist of the North Star
*Lu Zheng Yu as Killer One
*Chiu Chi Ling as Killer Two
*Yang Di as Killer Three
*Chrissie Chau as Killer Four
*Ge Hang Yu as Killer Five and Short Monkey King
*Min-hun Fung as Taoist Priest
*Yeung Lun as Mayor
*Zhang Chao Li as Almight Foot
*He Wun Hui as Maple
*Tina Tang as Blossom
*Chen Yichun and Liu Zhan Ling as Gao Family Inn Managers
*Huang Xiao Chuan as Leader of the Sand People
*Zhang Yu Wen as Sheng
*Xu Min as Mrs. Gen
*Li Jing as Gen
*Zhang Wei Fu as Grandpa Gen
*Fan Fu Lin as Muscleman
*Dai Qu Hua as Lan
*Zhong Kai Jie as Lans baby
*Xie Jing Jing as Fat Lady
*Yu Qian Wen as Fat Ladys Husband
*Kong Wu Shuang as Singing Girl
*Li Gao Ji as Taoist Priest Fook
*Wen Fei Fei as Monk Lu
*Huang Hai Seng as Monk Shou
*Zhang Wan Ku, Xu Wen Qiang, Chen Jian Feng, Li Nin Cai, Li Jing, Li Gui Suan, Han Xiao Chuang, Yu Ping, Li Yong Bo, Gong Meng Ying, Ge Hui Lei, Zhang Hong Di, Chen Xing Xiang, Zhang Cheng Long, and Wang Ya Bing as villagers
*  Min Hun Fung 

==Box office==
The film set several records at the Chinese box-office.  The film was released on February 10, 2013 in China and opened to 78 million Yuan ($12.5 million) on its first day, thus overtaking the 70 million yuan ($11.2 million) opening-day record set by   on June 28, 2012 as the biggest opening-day gross for a Chinese film.     On February 14, 2013, the film grossed 122 million yuan ($19.6 million) and thus overtook the record of 112 million yuan by   as the biggest single-day gross by a film in Chinas box-office history.   The film set an opening record in China with $92.46 million. 

To date, the film has grossed US$205 million in China,    US$3.6 million in Hong Kong,  US$3.2 million in Malaysia,  and US$1.8 million in Singapore. http://www.boxofficemojo.com/movies/?page=intl&id=journeytothewest.htm 

Journey to the West: Conquering the Demons grossed a total of US$215 million worldwide, making it highest grossing Chinese-language film ever. 

==Critical reception== average score of 68, based on 13 reviews. 

Edmund Lee of Screen International describes the film as "a thoroughly entertaining action comedy."  Andrew Chan gave the film 9/10 and writes, "Stephen Chow latest revisit to “Journey to the West: Conquering the Demons” is a highly entertaining affair. From the get go, the audience is treated with Chow famed exaggerated style of comedy." 

==Possible sequel==
Derek Kwok reported in March 2013 that there were ongoing discussions about a script for a sequel with Stephen Chow, who may appear in it himself.  The film has a reported budget of around US$64 million. Production could start in August 2015 with a possible release during the 2017 Chinese New Year. 

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 