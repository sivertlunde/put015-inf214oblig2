Band Baaja Baaraat
 
 

{{Infobox film
| name           = Band Baaja Baaraat
| image          = Band Baaja Baaraat poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Maneesh Sharma
| producer       = Aditya Chopra
| screenplay     = Habib Faisal
| story          = Maneesh Sharma
| starring       = Anushka Sharma Ranveer Singh
| music          = Salim-Sulaiman
| cinematography = Aseem Mishra
| editing        = Namrata Rao
| studio         = Yash Raj Studios
| distributor    = Yash Raj Films
| released       =  
| runtime        = 140 Minutes
| country        = India
| language       = Hindi English
| budget         =  
| gross          =   (domestic net gross)   
}} romantic comedy wedding planning. No Problem Nani and Vaani Kapoor in the lead roles. The film was released on 21 February 2014. 
 Nandini Reddy is an unofficial remake of BBB.

==Plot==
Bittoo Sharma (Ranveer Singh) is street-smart and fun-loving. He gatecrashes a wedding for the free food, and Shruti Kakkar (Anushka Sharma), an intelligent and quirky girl who is assisting the wedding coordinator, confronts him. Bittoo pretends that he is part of the film crew and makes a video of Shrutis performance when he sees her dancing at the wedding. The next day, he tries to impress Shruti with a DVD he compiled of her dance at the wedding. Shruti is not interested in flirting, and reveals that her main interest is starting her own wedding planner business.

Bittoo is under pressure from his parents to come back to the village and work on the family sugarcane fields, and Shruti is being coaxed into getting married as soon as possible. After exams are over, Shruti makes a deal with her parents that she has five years to get her business up and running before they arrange a marriage for her. When Bittoos father comes to take him back to his village, Bittoo lies that he cannot come back because he is starting a wedding planner business. He goes to Shruti with this idea but she refuses because she is worried that a romantic complication will rise between the two of them. She tells Bittoo that the number one rule of business is not to let love get in the way.

Shruti, with Bittoo tagging along, goes to meet Chanda, a famous wedding planner, in the hopes that Chanda will hire her. Chanda shows no interest in Shruti, but loses one of her male workers at that very moment and offers Bittoo the job. Bittoo accepts on the condition that both him and Shruti are hired together. Shruti learns that Chanda is cheating her clients. When a client confronts Chanda, she blames Shruti. Bittoo angrily defends Shruti and the two of them quit and walk out together. They begin their own company, starting with small, low-budget projects and gradually working their way up. They finally get their first big client by stealing them away from Chanda. The wedding is a huge success and that night, they celebrate, ending up drunk and making love. Afterwards, Bittoo lies awake all night, worried, since Shruti had warned against this.

Bittoo starts behaving awkwardly around her, while Shruti realizes she has fallen in love with him. Shruti tries to reassure Bitto that shes not like other women. Bittoo, misunderstanding, believes that shes telling him the night didnt mean anything to her. He is relieved and cites her own rule that business and love dont mix. Shruti pretends to agree but cries after he leaves. This creates a tense atmosphere between the two, eventually leading to a huge fight. Shruti breaks the partnership, forcing Bittoo to leave the company. Bittoo starts his own wedding planning business . However, they dont do as well by themselves and both end up suffering huge losses, plunging them into debt.

They finally get a big contract, but it is contingent upon them working together as a team. Facing debt collectors, they agree to partner up again for the sake of recovering their losses. They divvy up the workload, eventually falling back into their old rhythm. The next day, Bittoo tells Shruti that they should be partners again. Shruti refuses his offer, telling him that she is getting married to her fiance, Chetan, and will move to Dubai. Bittoo is stunned and pesters her during the rest of the wedding preparations, making various excuses why she shouldnt marry. As a last resort, he accuses her of her getting married to make him jealous. Shruti angrily answers that she is doing it for her parents, and admits that she did fall in love with him, but since he didnt feel the same, has moved on.

Bittoo realizes that he has always been in love with Shruti, but was too scared to acknowledge it. Desperate to win her back, Bittoo calls Chetan, telling him to back off. Hearing this, Shruti angrily confronts Bittoo. He tells Shruti that he was a fool to have run away from her love. Shruti calls off her engagement and the two share a kiss.

The film ends with both of them dancing at their own wedding with friends and family.

==Cast==
* Anushka Sharma as Shruti Kakkar
* Ranveer Singh as Bittoo Sharma
* Manu Rishi as Inspector (Special Appearance)
* Puru Chibber as Bittoos best friend (Mika)
* Revant Shergill as Santy (Musician)
* Manmeet Singh as Rajinder Singh (Caterer)
* Neeraj Sood as Maqsood (Florist)
* Vinod Verma as Shrutis father
* Nirupama Chopra as Shrutis mother
* Pushvinder Rathore as Shrutis sister
* Shena Gamat as Chanda Narang, the famous wedding planner
* Govind Pandey as Bittoos father Manish Choudhary as Sidhwani

==Production==

===Casting===
{{Quote box quote  = "  and I have been friends for a long time, and we have a very comfortable love-hate relationship – more hate and less love from her side!" source = —Maneesh Sharma Chandna Arora  , The Times of India, 27 October 2010.  width  = 30%}} Punjabi girl, which called for her to "talk fast, sometimes mix words and even omit words completely".  Director Maneesh Sharma, with whom shes been great friends since her first film, said that the model-turned-actress was a very feisty person, and a natural actress who didnt like doing multiple takes.  She described the film as "very much a young love story set in Delhi". 

The male lead was given to   prior to shooting said about his casting "I’m the first solo hero Yash Raj is launching. It’s a huge deal for me. I don’t know how I got here. I guess I happened to be in the right place at the right time".  As such, this marks the first time a film rests mainly on Anushka Sharmas shoulders, as opposed to her prior films Rab Ne Bana Di Jodi and Badmaash Company where she shared the screen with more experienced co-stars, respectively Shahrukh Khan and Shahid Kapoor, a fact the actress remarked upon, stating "some people will refer to Band Baaja Baraat as Anushka Sharma’s film because they have seen me in another film before". Shweta Mehta  , Hindustan Times, 26 October 2010. 

===Filming===
Filming started in Delhi on 4 February 2010, the same day the production company announcement was made.  Ranveer Singh was very nervous about his first day but was eventually proud that his first scene only took three takes to shoot.  An 18 February article by Minakshi Saini for the Hindustan Times entertainment supplement HT City reported that during the previous days early morning shoot in West Delhi’s Subhash Nagar, Ranveer Singhs newcomer status led many to speculate on whether he was Ranbir Kapoor, Ranvir Shorey or even Ritesh Deshmukh. Police officers were eventually called in to secure the set from curious onlookers, however some expressed discontentment at having to be out in the cold despite both lead actors being virtual unknowns.  The film features a kiss between the lead actors, which only necessitated a single take.  Reportedly, Singh accidentally hit Sharma while filming an undisclosed intense scene.  Other than Subhash Nagar DDA Market, locations in Delhi include Janakpuri, Delhi University, North and West Delhi, Ring Road, Mehrauli Farms and Akbar Road.     Some scenes were also shot in director Maneesh Sharmas almamater, Hans Raj College in University campus.  The film was additionally shot in Mumbai in March and Rajasthan in April. 
 New York and Once Upon a Time in Mumbaai. The various dance sequences were choreographed by Vaibhavi Merchant, who has previously worked on countless films including such hits as Lagaan, Swades, Devdas (2002 Hindi film)|Devdas and Veer-Zaara. Sonal Choudhry and T.P. Abid were the films production designers while Niharika Khan was the costume designer.

Several rumours surrounded the films shoot, such as speculation that the lead pair were more than just friends.    Early media reports alternatively titled the film Shaadi Mubarak or Shaadi Mubarak Ho, however Yash Raj Films eventually issued a press release in late April claiming that this was not the films title, and that film was actually still untitled.  The title was eventually revealed to be Band Baaja Baaraat in late July. 

Lead actors Ranveer Singh and Anushka Sharma got along well during the shoot, and are strongly believed to have dated and broken up before the films release. Speaking about his co-star, Singh declared "She’s intelligent, well read and great to hang out with. Actually, she’s pretty close to my dream girl".  Later he joked "She is the best co-star I have ever worked with! Its also because she is the only co-star I have ever worked with!". 

===Post-production=== edited the film.

==Soundtrack==
{{Infobox album  
| Name        = Band Baaja Baaraat
| Type        = soundtrack
| Artist      = Salim-Sulaiman
| Cover       = Band Baaja Baaraat soundtrack.jpg
| Alt         =
| Released    =  
| Recorded    = Feature film soundtrack
| Length      =   YRF Music
| Producer    =
| Last album  = Aashayein (2010)
| This album  = Band Baaja Baaraat (2010)
| Next album  = Anaganaga O Dheerudu (2011)
}}
The score and songs of the film were composed by the duo Salim-Sulaiman whove composed music for many other Yash Raj Films productions before, including Rab Ne Bana Di Jodi, which had already united actress Anushka Sharma with director Maneesh Sharma, who was then still an assistant director. The lyrics to the various songs were written by Amitabh Bhattacharya and playback singers include Sunidhi Chauhan, Benny Dayal, Shreya Ghoshal, Natalie Di Luccio, Himani Kapoor, Harshdeep Kaur, Labh Janjua, Shrraddha Pandit, Master Saleem, Sukhwinder Singh, Amitabh Bhattacharya and Salim Merchant.
 Punjabi wedding song and Band Baaja Baaraat (Theme) as "a pulsating track which perfectly represents the spirit of the movie".

{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    = 37:59
| all_writing     =
| all_lyrics      =
| all_music       =
| title1          = Ainvayi Ainvayi
| extra1          = Salim Merchant & Sunidhi Chauhan
| length1         = 4:27
| title2          = Tarkeebein
| extra2          = Benny Dayal & Salim Merchant
| length2         = 4:49
| title3          = Aadha Ishq
| extra3          = Shreya Ghoshal & (additional vocals by Natalie Di Luccio)
| length3         = 4:43
| title4          = Dum Dum
| extra4          = Benny Dayal & Himani Kapoor
| length4         = 5:10
| title5          = Mitra
| extra5          = Amitabh Bhattacharya & Salim Merchant
| length5         = 4:02
| title6          = Baari Barsi
| extra6          = Harshdeep Kaur, Labh Janjua & Salim Merchant
| length6         = 4:42
| title7          = Band Baaja Baaraat (Theme)
| extra7          = Salim Merchant & Shrraddha Pandit
| length7         = 1:52
| title8          = Ainvayi Ainvayi (Dilli Club Mix)
| note8           = Remix by Abhijit Vaghani
| extra8          = Master Saleem & Sunidhi Chauhan
| length8         = 3:45
| title9          = Dum Dum (Sufi Mix)
| note9           = Remix by Abhijit Vaghani
| extra9          = Sukhwinder Singh & Himani Kapoor
| length9         = 4:29
}}

===Reception=== Aaja Nachle. Rab Ne Bana Di Jodi, it was "far more engaging".  One of the albums most enthusiastic reviews was written by Joginder Tuteja for Bollywood Hungama. Tuteja described the music as "one of the better soundtracks that one has heard from the house of Yash Raj Films in last couple of years", "one of the best works of Salim-Sulaiman" and "much more than just a regular fun album". He selected Ainvayi Ainvayi, Aadha Ishq and Dum Dum (Sufi Mix) as the discs best tracks.  Moviehattans Gurpreet Bhuller also liked the soundtrack, which he described as a "quality album" with "a good collection of songs". 

==Release==

===Marketing===
{{Quote box quote  = "We’re like a tiny paan shop if you compare us with the other releases in the same week or month, but this paan shop is the best of the lot" source =  — Anushka Sharma  width  = 30%}}
As with all films from the studio since Mohabbatein, publicity design was handled by Fayyaz Badruddin. Stills were taken by Abhay Singh and Zahir Abbas Khan.

  at a Band Baaja Baaraat press event, 2010]] trailer and wallpapers and a press kit for visitors to download. The number of available wallpapers later grew to twenty-five and the website eventually allowed visitors to send e-cards to their acquaintances, with the virtual cards dubbed "Band Baaj-O-Grams". A number of contests were organised by Yash Raj Films, including one where the company, along with partners Radio Mirchi and BIG Cinemas, offered the winning couple a free wedding in December, in time for the films release and supposedly planned the films heroes, and another in which a couple would win a trip to Switzerland and visit the filming locations of the various Yash Raj Films productions to have been shot there. In addition to the website, Yash Raj Films also had regularly updated official pages on Facebook and Twitter and a Blogspot blog in an effort to reach the widest audience possible. The company finally uploaded a number of videos on their YouTube account, including the trailer but also several videos promoting the songs Tarkeebein and Ainvayi Ainvayi.

  at a promotional event for Band Baaja Baaraat|alt=Singh looking towards his left and smiling, wearing a black t-shirt]] IAS officer. The ensuing argument grew so heated that the cabin crew had to intervene and both parties went to the police station upon arrival and stayed there for over an hour-and-a-half, eventually sorting the matter out amicably without lodging any complaints.   
 pundits have star and the fact that the female lead, Anushka Sharma, was by then an "almost-forgotten" actress.  Some have even speculated that the 22-year-olds career might not thrive for much longer with her three film contract with Yash Raj Films being over. 

===Theatrical run===
On 24 September 2010, Yash Raj Films announced that the film would release worldwide on 10 December of that year,  almost two years since the release of Anushka Sharmas debut Rab Ne Bana Di Jodi. Some people have speculated that this was not coincidence but an attempt to repeat the success of her first film. 

==Critical reception==
The film received unanimously positive reviews from critics.   of NDTV awarded it 3/5 stars and commented,"Band Baaja Baaraat is reasonably entertaining. Its definitely the most fun youll have in a theater this weekend." Rajeev Masand gave it 3/5 stars writing,"Band Baaja Baaraat works because it’s invested with an earnestness that’s become increasingly rare to find at the movies I’m going with three out of five for director Maneesh Sharma’s Band Baaja Baaraat. It’s a romantic comedy done correctly. Fun, but with warmth at its heart. Don’t miss it." Nikhat Kazmi of The Times of India gave the film 3/5 stars and opined,"As long as you view Band Baaja Baaraat as a loving, heartfelt take on what makes Delhi go dhak-dhak, the film holds your attention.Band Baaja Baaraat engages you with its fond look at fun-loving Dilliwalas." Sonal Dedhia of Rediff gave it 3/5 commenting,"On the whole, Band Baaja Baaraat is a refreshing film -- very different from the usual romantic comedy movies were so used to. It is a well-made film that should connect with the audience. Give this one a chance, you wont regret it."

===Box office===
 
The movie had an opening of below   10&nbsp;million. It grossed   95.0&nbsp;million in the first week. The movie grossed   75.0&nbsp;million in week 2, taking 2 weeks collection to   170&nbsp;million. It earned   214.4&nbsp;million. The film was declared a "Super Hit" by Box Office India. It earned approx   233.1&nbsp;million in full theatrical run.

== Awards and nominations ==
 

===2011 IIFA Awards===
;Won
* Hot Pair – Anushka Sharma and Ranveer Singh
* Best Debut (Male) – Ranveer Singh
* Best Actress – Anushka Sharma
* Best Costume Designing – Niharika Khan
* Best Editing – Harshitha Kakani
* Best Song Recording – Vijay Dayal, Ainvayi Ainvayi

===2011 Star Screen Awards===

;Won
* Star Screen Award for Most Promising Debut Director – Maneesh Sharma
* Star Screen Award for Most Promising Newcomer - Male – Ranveer Singh
* Star Screen Award for Best Dialogue – Habib Faisal
* Star Screen Award for Best Editing – Namrata Rao

Nominated
* Star Screen Award for Best Actress – Anushka Sharma
* Star Screen Award for Best Director – Maneesh Sharma
* Star Screen Award for Best Film – Band Baaja Baaraat
* Star Screen Award for Best Screenplay – Habib Faisal
* Star Screen Award for Best Choreography – Vaibhavi Merchant "Ainvayi Ainvayi"

===6th Apsara Film & Television Producers Guild Awards===

;Won
* Apsara Award for Best Editing – Namrata Rao
* Apsara Award for Best Costume Designer – Niharika Khan
* Apsara Award for Best Debut Male – Ranveer Singh
* Apsara Award for Best Debutante Director – Maneesh Sharma
* Apsara Award for Best Art Direction – Sonal Choudhry and T.P. Abid
* Apsara Award for Best Actress in a leading role – Anushka Sharma

Nominated
* Apsara Award for Best Film – Band Baaja Baaraat
* Apsara Award for Best Female Singer – Sunidhi Chauhan "Ainvayi Ainvayi"
* Apsara Award for Best Choreography  – Vaibhavi Merchant "Ainvayi Ainvayi"
* Apsara Award for Best Dialogue – Habib Faisal
* Apsara Award for Best Screenplay – Habib Faisal
 2011 Zee Cine Awards===

;Won  Best Male Debut – Ranveer Singh

Nominated  Best Actress – Anushka Sharma Best Story – Maneesh Sharma Best Debutante Director – Maneesh Sharma

===2011 Filmfare Awards===

;Won Best Male Debut – Ranveer Singh
* Filmfare Award for Best Debut Director – Maneesh Sharma

Nominated
* Filmfare Award for Best Movie  – Band Baaja Baaraat
* Filmfare Best Scene of the Year Award
* Filmfare Award for Best Director – Maneesh Sharma
* Filmfare Award for Best Actress – Anushka Sharma

===2011 17th Lions Gold Award===

;Won
* Lions Favourite Debutant Actor – Ranveer Singh
* Lions Favourite Jodi – Ranveer Singh and Anushka Sharma

===2011 BIG Star Entertainment Awards===

Nominated
* Most Entertaining Film Actor Male– Ranveer Singh
* Most Entertaining Film Actor Female– Anushka Sharma

===2011 Stardust Awards===

;Won
* Superstar of Tomorrow (Male) – Ranveer Singh

Nominated
* Best Actress – Comedy / Romance – Anushka Sharma
* Best Actor – Comedy / Romance – Ranveer Singh
* Hottest New Director – Maneesh Sharma
* New Musical Sensation – Himani Kapoor for Dum Dum
* Best Film – Comedy / Romance – Band Baaja Baaraat
* Best Director – Comedy / Romance – Maneesh Sharma

===2011 Dadasaheb Phalke Academy Awards ===

;Won
* Excellent Performance in a Film – Anushka Sharma
* Best Debut Male – Ranveer Singh

==See also==
 
* Aaha Kalyanam, the Tamil / Telugu remake of the film in production.
* Jamuna Paar, an Indian TV serial inspired by Band Baaja Baaraat
* Jabardasth (film)|Jabardasth, a Telugu film inspired from Band Baaja Baaraat
* Bollywood films of 2010

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 