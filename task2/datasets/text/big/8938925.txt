Black Gold (2006 film)
{{Infobox film
| name           = Black Gold
| image          = Black Gold (2006) Poster.jpg
| director       = Marc Francis, Nick Francis
| editing        = Hugh Williams
| music       = Andreas Kapsalis
| producer       = Christopher Hird, Marc Francis, Nick Francis
| writer         = Marc Francis, Nick Francis
| starring       = Tadesse Meskela
| released       = 2006 (United States|USA)
| runtime        = 78 min.
| language       = Amharic, Oromiffa and English with English subtitles 
| studio         = Speakit Films
| budget          = 760,000 USD 
| website         =  
}}

Black Gold is a 2006 feature-length documentary film. The story follows the efforts of an Ethiopian Coffee Union manager as he travels the world to obtain a better price for his workers coffee beans.

The film is directed and produced by Marc Francis and Nick Francis from Speakit Films, and co-produced by Christopher Hird. It premiered at Sundance in 2006. 

==Synopsis== Sidamo and Abaya woreda of the Borena Zone. The Ethiopian coffee farmers speak about their lives, with one explaining that he is cutting down his coffee plants and planting khat (a  plant containing cathinone, an amphetamine-like stimulant) instead, due to the low price he is getting for coffee because of the explosion in the number of coffee farmers across the globe, and the comparatively higher price he can get for khat.
 commodity trading floor in New York City, where the "C" international benchmark price of coffee is set each business day based on supply and demand, and explores the effects that these international prices (which by 2006 were at an all-time low) have on Ethiopian coffee growers. Other footage was shot at the first Starbucks and the World Barista Championship at the 2005 Specialty Coffee Association of America conference in Seattle; and at a café and the Illy coffee company in Trieste, Italy. These scenes stand in stark contrast to the footage of the impoverished conditions faced by the Ethiopian coffee farmers and their families.

==Production==
In July 2003, Nick Francis  and Marc Francis travelled to Ethiopia and started making the film.

The meeting with Tadesse Meskela, the manager of Oromia Coffee Farmers Co-operative Union, was a decisive moment in the production of the film.    The inspiring passion and determination of Tadesse, along with his continuous efforts to improve the lives of the farmers he represents, determined Nick Francis and Marc Francis much further in the production process to centre the film narrative around Tadesse’s journey, his character functioning as a guide into the coffee world.

The Ethiopian footage was filmed on two occasions (in 2003 and 2005), for six weeks each time. 

After the first shoot in Ethiopia, the crew flew to Cancun to document the meetings of the World Trade Organization, where pivotal decisions that affect Africa’s economy and development were being made in setting the trade rules and where the struggle for fairer international trade rules was played out. Unprecedented, that year developing countries refused to be forced into signing agreements that were against their interests - and the WTO talks completely collapsed.

Following the team’s return to the UK, they spending the following months developing a rough cut of Black Gold. During these months, the direction of the film shaped, along with the rest of necessary shoots meant to develop the thrust of the narrative. In 2005 the team returned to Ethiopia for further shoots.
 Sara Lee, Procter & Gamble, Kraft Foods|Kraft, and Nestlé - the worlds largest sellers of coffee - are mentioned in the film, all five companies declined invitations to be interviewed for the film. 

By the end of 2005, the final cut of Black Gold was done and officially premiered during the 2006‘s edition of Sundance Film Festival.

==Music==
Andreas Kapsalis, whom Marc Francis and Nick Francis met during the Sundance Documentary Composers Lab, wrote the final soundtrack. In doing so, he used in part Mathew Coldrick’s Ethiopian instrumental samples that he recorded from Ethiopia. Other tracks include contributions from Joe Henson & Kunja Chatterton and Mathew Herbert. 

==Awards and nominations==
* WINNER: British Independent Film Awards -  
* WINNER: Contemporary Issues -     
* NOMINEE: Best Documentary -   
* NOMINEE: Best Debut Director -  
* NOMINEE:  
* NOMINEE: Pare Lorentz Award   (2006)
* NOMINEE: Index On Censorship Film Awards  (2008)
* Official Selection: Melbourne International Film Festival (2006)   
* Official Selection: Rio de Janeiro International Film Festival (2006)   
* Official Selection: London Film Festival (2006)  
* Official Selection: FESPACO Pan-African Film Festival 
* Official Selection: Rome Film Festival   
* Official Selection: Hot Docs  
* Official Selection: True/False Film Festival (2006)  
* Official Selection: Human Rights Watch Film Festival (2006)   
* Official Selection: New Zealand International Film Festival (2006)  

==Distribution==
Black Gold has been released in the cinema and on DVD in over 40 countries: in the UK (Dogwoof), US (California Newsreel), Canada (Mongral Media), Australia (Madman), Germany, Spain, Netherlands, Scandinavia and Japan and has been broadcast on TV stations around the world including US (ITVS: Independent Lens), UK (Channel 4), Japan (NHK), Israel (DBS/YES), New Zealand (Documentary Channel), South Africa (SABC), South Korea (EBS) and the Middle East (Al-jazeera). It continues to be screened around the world.

Black Gold went on to be seen in over 60 international film festivals including London, Rome, Berlin, Melbourne, Hong Kong and Rio de Janeiro and has secured major broadcast deals around the world including   (UK),   (US), Documentary Channel (Canada), NHK (Japan) and Al-jazeera (Middle East). 

Black Gold has attracted wide coverage in the media including features on CNN,  BBC World, BBC News 24,  Sky News,  Bloomberg, The Observer,  The Times,    The Daily Telegraph,  New York Times,  LA Times,    The Washington Post  and South China Morning Post.      

==Reception and impact==
In the first few weeks after the premiere, the film helped to generate donations approximately up to $25,000. The dilapidated school featured in the film was built.

Tadesse Meskela, the main character in Black Gold was able to increase the market price for his cooperative union’s coffee. Tadesse Meskela was invited to 10 Downing Street where he took his message directly to Tony Blair.

After seeing the film, the UN have invited Tadesse to New York to do a presentation about his co-operatives at the 46th Session of the Commission of Social Development.

Black Gold was screened at the World Bank, European Union, as part of debates about trade policy and development. The Ethiopian ambassadors based at Berlin, Washington, D.C. and London attended screenings.

Black Gold was the first film to receive funding from BritDoc.  

==Corporate response to Black Gold==
Ever since the first showing of Black Gold during Sundance, Starbucks sent people to screenings of the film in what has been called by one journalist "going on a charm offensive".    As the film became more and more popular, Starbucks flew Tadesse, along with four other African coffee producers to their Seattle headquarters for a weekend conference, which was seen by many as a PR stunt   
Further, just before the film premiered at the London Film Festival in October 2006, a memo received by Starbucks staff from the headquarters leaked to the Black Gold forum. The internal memo was sent out to inform all Starbucks employees that Black Gold was "incomplete and inaccurate"  

==References==
 

==External links==
* 
* 
* 
 
 

 
 
 
 
 
 
 
 