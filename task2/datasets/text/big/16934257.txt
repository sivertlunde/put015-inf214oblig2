Where Do We Go from Here? (film)
{{Infobox film
| name           = Where Do We Go from Here?
| image          = Where Do We Go from Heret - 1945 Poster.png
| image_size     =
| caption        = 1945 Theatrical Poster
| director       = Gregory Ratoff
| producer       = William Perlberg
| writer         = Morrie Ryskind Sig Herzig
| narrator       =
| starring       = Fred MacMurray Joan Leslie June Haver
| music          = Kurt Weill Ira Gershwin
| cinematography = Leon Shamroy
| editing        = J. Watson Webb Jr.
| distributor    = Twentieth Century-Fox
| released       =  
| runtime        = 78 minutes
| country        = United States English
| budget         = $2.4 million Charles Tranberg, Fred MacMurray: A Biography, Bear Manor Media, 2014 
| gross          = $1.75 million 
}}

  romantic musical musical comedy film|comedy-fantasy film produced by Twentieth Century-Fox, and starring Fred MacMurray, Joan Leslie, June Haver, Gene Sheldon, Anthony Quinn and Fortunio Bonanova. Joan Leslies singing voice was dubbed by Sally Sweetland.
 score was composed by Kurt Weill with lyrics by Ira Gershwin. Gregory Ratoff directed and Morrie Ryskind wrote the screenplay from a story by Sig Herzig and Ryskind.   

The film is notable as Weills only musical written directly for the screen and for its anachronistic blend of history and contemporary (1940s) slang. At the time, the mock-operatic sequence, "The Nina, the Pinta, the Santa Maria," was one of the longest musical sequences ever created for a screen musical.

==Synopsis== 4F status prevents him from enlisting. Bill does his bit for the war effort by collecting scrap metal. Among the discarded junk he discovers a mysterious brass bottle which he rubs to clean off the grime.  
 Hessian soldiers, Bill escapes by wishing himself into the Navy. Once again the Genie transfers him, but this time to the crew of Christopher Columbuss ship on his maiden voyage to the new world. Once on shore, he agrees to buy Manhattan Island from a local native (Anthony Quinn).  

Bill next finds himself whisked forward in time to New Amsterdam in the mid-17th century. When he claims that he owns the Island, he is thrown in jail. Ali finally gets it right and Bill finds himself in the right time and place by the end of the film. June Haver plays the girl Bill thinks he loves and Joan Leslie plays the girl who loves (and ultimately lands) him.

==Cast==
* Fred MacMurray as Bill Morgan
* Joan Leslie as Sally Smith / Prudence / Katrina
* June Haver as Lucilla Powell / Gretchen / Indian
* Gene Sheldon as Ali the Genie
* Anthony Quinn as Chief Badger Carlos Ramírez as Benito
* Alan Mowbray as General George Washington
* Fortunio Bonanova as Christopher Columbus
* Herman Bing as Hessian Col. / Von Heisel
* Howard Freeman as Kreiger

==Production==
Kurt Weill had not been well served by Hollywood. His scores for the Broadway shows Knickerbocker Holiday, Lady in the Dark, and One Touch of Venus had been drastically cut for their film adaptations. Although a few cuts were made in his proposed score for Where Do We Go from Here, most of his work with Ira Gershwin remains, including a lengthy mock-opera bouffé aboard Columbus ship during which the crew threatens to mutiny.

Co-stars June Haver and Fred MacMurray met while working on this film, and were later married.
 
Where Do We Go from Here? has never been released on home video. A recording taken directly from the soundtrack of the film was released on LP (Ariel KWH 10)  and a set of rehearsal recordings performed by Kurt Weill and Ira Gershwin is currently available on the CD Tryout  (DRG Records) including extended versions of the songs "Nina, the Pinta, the Santa Maria", "Song of the Rhineland", and "Manhattan (Indian Song)."

==Musical numbers==
* All at Once - Sung by Fred MacMurray
* Morale - Sung and Danced by June Haver and Chorus
* If Love Remains - Sung and Danced by Fred MacMurray, Joan Leslie (dubbed by Sally Sweetland) and Chorus
* Song of the Rhineland - Sung and Danced by June Haver, Howard Freeman and Chorus
* Columbus (The Nina, the Pinta, the Santa Maria) - Sung by Carlos Ramírez, Fred MacMurray, Fortunio Bonanova and Chorus
* All at Once (reprise) - Sung by Fred MacMurray and Joan Leslie (dubbed by Sally Sweetland)
* Morale (reprise) - Sung by Fred MacMurray, Joan Leslie (dubbed by Sally Sweetland), June Haver, Gene Sheldon and Chorus

==Cut Songs==
* It Could Have Happened to Anyone
* Woo, Woo, Woo, Woo, Manhattan

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 