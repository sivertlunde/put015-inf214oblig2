Phish: Walnut Creek
{{Infobox film
| name        = Phish: Walnut Creek
| image       = PhishWalnut.jpg
| caption     =
| writer      =
| starring    = Phish
| director    =
| producer    = JEMP
| released    =  
| runtime     = 136 minutes
| language    =
| music       = Phish
| awards      =
| budget      =
}}

Walnut Creek is a concert DVD released on August 8, 2008 by the rock band Phish. It was performed on July 22, 1997 at Walnut Creek Amphitheatre in Raleigh, North Carolina during a stormy night, the second show of their 1997 U.S. summer tour.

During the first set, lightning struck the stage three different times, hitting the stage during the bands rendition of "Taste," forcing the group to cut the set short and leave the stage. After a one hour intermission (purposely extended in order to let the storm pass), the group returned to the stage for an additional 90 minutes of music. 
 
The DVD footage is taken from the venue jumbo screen video feed displayed in concert during the show.

This concert is also available as a download in FLAC and MP3 formats at LivePhish.com.  As a bonus, the download includes You Enjoy Myself recorded at Walnut Creek on June 16, 1995 with guest Boyd Tinsley of the Dave Matthews Band on violin.

==Track listing==
===Disc one===
# "Runaway Jim"  (Abrahams, Trey Anastasio|Anastasio)  - 10:10 ->
# "My Soul"  (Clifton Chenier|Chenier)  - 5:36
# "Water in the Sky"  (Anastasio, Tom Marshall (singer)|Marshall)  - 2:46
# "Stash"  (Anastasio, Marshall)  - 12:33
# "Bouncing Around the Room"  (Anastasio, Marshall)  - 3:44
# "Vultures"  (Anastasio, Herman, Marshall)  - 7:09
# "Bye Bye Foot"  (Jon Fishman|Fishman)  - 3:59
# "Taste"  (Anastasio, Fishman, Mike Gordon|Gordon, Marshall, Page McConnell|McConnell)  - 10:19

===Disc two===
# "Down With Disease"  (Anastasio, Marshall)  - 20:17 ->
# "Mikes Song"  (Gordon)  - 14:35 ->
# "Simple"  (Gordon)  - 9:30 ->
# "I Am Hydrogen"  (Anastasio, Marc Daubert|Daubert, Marshall)  - 3:02
# "Weekapaug Groove"  (Anastasio, Fishman, Gordon, McConnell)  - 11:16 Hello My Baby"  (Emerson, Howard, Singer)  - 2:02
# "When the Circus Comes"  (David Hidalgo|Hidalgo, Louie Pérez|Pérez)  - 5:04 Harry Hood"  (Anastasio, Fishman, Gordon, Long, McConnell)  - 11:32

===Left Nuts Bonus Disc===
All songs recorded June 16, 1995 except "Reba," which was recorded June 29, 1994.
# "Runaway Jim"  (Abrahams, Anastasio)  - 31:00 ->
# "Free"  (Anastasio, Marshall)  - 8:35
# "Reba"  (Anastasio)  - 15:36
# "Dog Faced Boy"  (Anastasio, Fishman, Marshall, McConnell)  - 2:14 ->
# "Catapult"  (Gordon)  - 0:48 ->
# "Split Open and Melt"  (Anastasio)  - 15:11
# "Carolina"  (Traditional)  - 1:42

==Personnel==
Phish
:Trey Anastasio - guitars, vocals
:Page McConnell - keyboards, vocals
:Mike Gordon - bass, vocals
:Jon Fishman - drums, vocals

 

 
 
 
 
 