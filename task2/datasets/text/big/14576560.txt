Dodsworth (film)
{{Infobox film
| name           = Dodsworth
| image          = Dodsworth poster.jpg
| caption        = Theatrical release poster
| producer       = Samuel Goldwyn Merritt Hulburd
| director       = William Wyler
| based on       =  
| writer         = Based on Dodsworth (novel)|Dodsworth novel (1929) by Sinclair Lewis
| starring       = Walter Huston Ruth Chatterton Paul Lukas Mary Astor Alfred Newman
| cinematography = Rudolph Maté
| editing        = Daniel Mandell
| studio         = Samuel Goldwyn Productions
| distributor    = United Artists
| released       =  
| runtime        = 101 minutes
| language       = English
| country        = United States
| gross          = $1.6 million    accessed 19 April 2014 
}}
 1934 stage 1929 novel of the same name by Sinclair Lewis. Huston reprised his stage role.

The center of the film is a study of a marriage in crisis. Recently retired auto magnate Samuel Dodsworth and his wife Fran, while on a grand European tour, discover that they want very different things out of life, straining their marriage.
 1997  and AFIs 100 Years...100 Movies (10th Anniversary Edition)|2007. 

==Plot==
Samuel "Sam" Dodsworth (Walter Huston) is the successful, self-made and unsophisticated head of Dodsworth Motor Company, an American automobile parts manufacturing firm, based in the small Midwestern town of Zenith (also the setting for Lewis Babbitt (novel)|Babbitt). His wife Fran (Ruth Chatterton), feeling trapped by the boring social life of their small-town existence, convinces her spouse to sell his interest in the company and take her to Europe. Sam disregards the warning of Tubby Pearson, his banker and friend, that men like them are only happy when they are working.

While on the luxury cruise to England, Sam meets Edith Cortright (Mary Astor), an American divorcee now living in Italy, who is sympathetic to his eagerness to expand his horizons and learn new things. Meanwhile, Fran indulges in a light flirtation with a handsome officer (David Niven), only to hastily retreat when he suggests it become more serious.

Once they reach Paris, Fran begins to view herself as a sophisticated world traveler and Sam, with his apparent interest only in seeing the usual tourist sights and inspecting foreign auto works, as increasingly boring and unimaginative. Becoming bolder, Fran pretends to be much younger than she actually is and begins spending time on her own with other men. In the process, she becomes infatuated with cultured playboy Arnold Iselin (Paul Lukas). She suggests Sam return home and allow her to spend the summer in Europe, and, feeling rather out of place in the urbane Old World, he consents.
 John Payne), who have moved into her parents mansion. Before long, though, Sam realizes that life back home has left him behind—and he is tormented by the idea that Fran might have, as well. He has a Dodsworth manager in Europe confirm that she is in fact seeing Iselin, and he returns to Europe immediately to put a stop to it.

Fran tries to deny the affair, but breaks down when Sam reveals he has summoned Iselin to confirm everything. She immediately begs for forgiveness, and he still loves her and their shared past, so they decide to patch up their marriage. However, it is soon evident that they have grown far apart.  When news of the birth of their first grandchild arrives, although initially excited, Fran is displeased with the idea of being a grandmother. She eventually informs Sam that she wants a divorce after all, especially after the poor, but charming young Baron Kurt von Obersdorf (Gregory Gaye) tells her he would marry her if she were free. Sam agrees.

Traveling aimlessly throughout the Continent while the divorce is being arranged, in Naples, Sam encounters Edith by chance. She invites him out to stay at her peaceful, charming Italian villa. The two rapidly fall in love. Sam feels so rejuvenated he decides to investigate starting a new business: a Moscow-to-Seattle airline. He asks Edith to marry him and come with him to Samarkand and other exotic locales on his new venture. She gladly accepts.

Meanwhile, Frans idyllic plans are shattered when Kurts mother (Maria Ouspenskaya) refuses to give her blessing to Fran marrying her son. In addition to divorce being against their religion, she tells Fran that Kurt must have children to carry on the family line, and Fran would be, in the Baronesss words, an "old wife of a young husband." Kurt asks Fran to postpone their wedding until he can get his mothers approval. Fran sees that it is hopeless, so she telephones Sam to call off the divorce. He reluctantly decides to leave Edith and to sail home with Fran in a sense of duty. However, after only a short time in Frans critical and demanding company, Sam realizes their marriage is irrevocably over. "Love has to stop somewhere short of suicide," he tells her. He gets off the ship at the last moment to rejoin Edith.

==Cast==
  
* Walter Huston - Sam Dodsworth
* Ruth Chatterton - Fran Dodsworth
* Paul Lukas - Arnold Iselin
* Mary Astor - Mrs. Edith Cortright
* Kathryn Marlowe - Emily Dodsworth McKee
* David Niven - Captain Clyde Lockert
 
* Gregory Gaye - Baron Kurt Von Obersdorf
* Maria Ouspenskaya - Baroness Von Obersdorf
* Odette Myrtil - Renée De Penable John Payne - Harry McKee
* Spring Byington - Matey Pearson
* Harlan Briggs - Tubby Pearson
 

==Production== Broadway production, which co-starred Fay Bainter as Fran. Huston recreated his role again for a Lux Radio Theatre broadcast in October 1937. 

==Reception==
In his review in the New York Times, Frank S. Nugent described it as "admirable" and added, "William Wyler . . . has had the skill to execute it in cinematic terms, and a gifted cast has been able to bring the whole alive to our complete satisfaction . . .   has done more than justice to Mr. Howards play, converting a necessarily episodic tale . . . into a smooth-flowing narrative of sustained interest, well-defined performance and good talk." 

Time said it was "directed with a proper understanding of its values by William Wyler, splendidly cast and brilliantly played." 

The film was named one of the years ten best by The New York Times and was one of the top twenty box office films of the year.

In 1990, Dodsworth was selected for preservation in the United States National Film Registry. In 2005, Time (magazine)|Time named it one of the 100 best movies of the past 80 years. 
 Academy Awards==
;Wins   
*  
;Nominations
*   Best Director: William Wyler Best Actor: Walter Huston Actress in a Supporting Role: Maria Ouspenskaya
*  
*  

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 