An Egyptian Story
{{Infobox film
| name           = An Egyptian Story
| image          = An Egyptian Story.jpg
| image size     = 
| caption        = 
| director       = Youssef Chahine
| producer       = Youssef Chahine
| writer         = Youssef Chahine
| starring       = Oussama Nadir Mohsen Mohieddin Nour El-Sherif
| music          = 
| cinematography = Mohsen Nasr
| editing        = Rashida Abdel Salam
| distributor    = 
| released       =  
| runtime        = 115 minutes
| country        = Egypt
| language       = Arabic
| budget         = 
}}

An Egyptian Story ( , Transliteration|translit.&nbsp;Hadduta misrija) is a 1982 Egyptian drama film directed by Youssef Chahine.

==Plot==
During his open heart surgery, an Egyptian filmmaker named Yehia has flashbacks and remembers his life. "Yehia" is the same character from the 1979 film Alexandria... Why?

The film was daring for implying a homosexual affair between the character "Yehia" and a taxi driver. Homoerotic tension was created by knowing glances that are exchanged between Yehia and the taxi driver. 

==Cast==
* Oussama Nadir - Yehia as Child
* Mohsen Mohieddin - Yehia as Young Man
* Nour El-Sherif - Yehia 
* Ahmed Mehrez
* Mohamed Mounir
* Ragaa Hussein
* Seif El Dine
* Yousra - Amal
* Hanan - Nadia child
* Leila Hamada - Nadia young girl 
* Magda El-Khatib - Nadia
* Raga El Geddaoui
* Soheir El Monasterli
* Andrew Dinwoodie
* Abdel Hadi Anwar

==References==
 

==External links==
* 
 
 

 
 
 
 
 
 
 


 
 