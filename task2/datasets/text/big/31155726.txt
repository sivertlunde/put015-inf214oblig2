Swapnabhoomi
{{Infobox film 
| name           = Swapnabhoomi
| image          =
| caption        =
| director       = SR Puttanna
| producer       = Rangarajan
| writer         = Miss. Thriveni S. L. Puram Sadanandan (dialogues)
| screenplay     = S. L. Puram Sadanandan Sathyan Sheela Kaviyoor Ponnamma
| music          = G. Devarajan
| cinematography = RNK Prasad
| editing        = VP Krishnan
| studio         = Sujatha Pictures
| distributor    = Sujatha Pictures
| released       =  
| country        = India Malayalam
}}
 1967 Cinema Indian Malayalam Malayalam film, directed by SR Puttannaand produced by Rangarajan. The film stars Prem Nazir, Sathyan (actor)|Sathyan, Sheela and Kaviyoor Ponnamma in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Prem Nazir Sathyan
*Sheela
*Kaviyoor Ponnamma
*Adoor Bhasi
*BN Nambiar
*Lakshmi

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aa Kayyil Ee Kayyil || P Susheela || Vayalar Ramavarma || 
|-
| 2 || Ezhilam Poomarakkaattil || P Susheela || Vayalar Ramavarma || 
|-
| 3 || Madhumathi || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 4 || Premasarvaswame || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 5 || Vellichirakulla || P Susheela || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 