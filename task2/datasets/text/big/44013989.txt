Tholkan Enikku Manassilla
{{Infobox film
| name = Tholkan Enikku Manassilla
| image =
| caption = Hariharan
| producer = GP Balan
| writer = S. L. Puram Sadanandan
| screenplay =
| starring = Prem Nazir Jayabharathi Bahadoor Junior Sheela
| music = Shankar Ganesh
| cinematography =
| editing =
| studio = GP Films
| distributor = GP Films
| released =  
| country = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, Hariharan and produced by GP Balan. The film stars Prem Nazir, Jayabharathi, Bahadoor and Junior Sheela in lead roles. The film had musical score by Shankar Ganesh.   
 
==Cast==
  
*Prem Nazir 
*Jayabharathi 
*Bahadoor 
*Junior Sheela
*K. P. Ummer 
*MG Soman  Master Raghu 
 
 
==Soundtrack==
The music was composed by Shankar Ganesh. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ponvilayum Kaadu || Jayachandran, Ambili || Mankombu Gopalakrishnan || 
|- 
| 2 || Subhamangalodayam || K. J. Yesudas || Mankombu Gopalakrishnan || 
|- 
| 3 || Tholkkaan Orikkalum || K. J. Yesudas || Mankombu Gopalakrishnan || 
|-  Susheela || Mankombu Gopalakrishnan || 
|- 
| 5 || Vayanaadin Maanam || Vani Jayaram || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 
 


 