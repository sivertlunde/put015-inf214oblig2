Surviving Progress
{{Infobox film
| name           = Surviving Progress
| image          = 
| alt            = 
| caption        = 
| director       =  Mathieu Roy, Harold Crooks (co-director)
| producer       = Daniel Louis. Denise Robert, Gerry Flahive
| writer         =  Harold Crooks, Mathieu Roy
| based on       =  
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = First Run Features (USA) 
| released       =  
| runtime        = 86 minutes 
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}} Massey Lecture series by Ronald Wright about societal collapse. The film was produced by Daniel Louis, Denise Robert, and Gerry Flahive.

==Subject matter== ancient civilizations, the film focuses on the present-day impact of civilization, including the impact of concentrated wealth.  The underlying message here is that current models and strategies of economic growth have no practical connections with the real world. That is to say, the lack of an ethical underpinning in modern global economic practices is directly responsible for the overconsumption and exploitation of natural resources to the extent that the increasingly more probable future population collapse would take modern society right along with it.

==Interviewees==
 
*Ronald Wright ... Author, A Short History of Progress
*  ... Group Leader, China Energy Group
*  Brazilian Minister of the Environment
*Kambale Musavuli ... Friends of the Congo
*Vaclav Smil ... Population Scientist/author, Global Catastrophies and Trends (ISBN  0262195860)
*Colin Beavan	... Engineer/author Michael Hudson ... Economic historian
*Jane Goodall ... Primatologist
*Ming Chen ... Self Driving Club Tour Guide
*Changnian Chen	... Professor/father of Ming Chen
*Craig Venter ... Biologist/CEO of Synthetic Genomics
*Raquel Taitson-Queiroz ... Environmental police officer, Brazilian Institute of Environment and Renewable Natural Resources
*Gary Marcus ... Cognitive psychologist/author, The Birth of the Mind (ISBN 0465044050)
*Daniel Povinelli ... Behavioral scientist China National Association of International Studies
*  Jim Thomas ETC Group Simon Johnson ... Former Chief Economist, International Monetary Fund
*Enio Beata ... Sawmill owner
*David Suzuki... Geneticist/activist
 

==Production== The Corporation 

==Release== 2011 Toronto International Film Festival. It was also shown as part of Festival Atmospheres on March 31, 2012 in Paris.

==Reception==
Calling it a "bone-chilling new documentary", Roger Ebert gave it   (3½ stars out of 4) and called it "bright, entertaining"  According to Macleans:  The Corporation, Inside Job, 2008 financial meltdown. The popularity of these films (the last two won Oscars) underscores a genuine appetite for global analysis that the fragmented vision of the news media fails to provide....The latest example is Surviving Progress, a Canadian documentary about the increasing weight of the human footprint of the planet. It’s a high-level lesson that is enlightening, engrossing and beautiful to look at. 

Kenneth Turan calls it "brainy and light on its feet, bristling with provocative insights and probing questions"; according to Turan, "though it features lively editing and a wide variety of involving visuals, Surviving Progress depends for its impact on the intelligence and eloquence of the numerous people interviewed." 

According to Rotten Tomatoes, 27 of the 36 reviewers rate the film "fresh." 

The film received a Green Cross Italia Special Mention at the CinemAmbiente Environmental Film Festival in Torino, Italy, in June 2012.   

==References==
{{reflist|refs=
   
   
}}

==External links==
* 
*  at the National Film Board of Canada
* 

 
 
 
 
 
 
 
 