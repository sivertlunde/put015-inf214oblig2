Bells from the Deep
 
{{Infobox film
| name           = Bells from the Deep
| image          =
| caption        = 
| director       = Werner Herzog
| producer       = Lucki Stipetic Ira Barmak Alessandro Cecconi
| writer         = Werner Herzog
| narrator       = Werner Herzog
| starring       = 
| music          = 
| cinematography = Jörg Schmidt-Reitwein
| editing        = Rainer Standke
| studio         = Werner Herzog Filmproduktion Momentous Events (co-production)
| distributor    =  1993
| runtime        = 60 min.
| country        = Germany United States English German German Russian Russian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Bells from the Deep: Faith and Superstition in Russia, is a 1993 documentary film written and directed by Werner Herzog, produced by Werner Herzog Filmproduktion.

==Summary== German director Werner Herzogs documentary investigation of Russian mysticism. The first half of the film is concerned primarily with a Russian faith healer and a man claiming to be the reincarnation of God as was Jesus. Herzog uses primarily interviews with Russians and scenes from the religious services of the two Holy men. Herzog also has several segments on the religion of Siberian nomads.
 prayed for rescue. Hearing their prayers, God placed the city at the bottom of a deep lake, where it resides to this day. Some even say that one can hear the bells from the citys church. The story is recounted by a local priest and pilgrims visiting the lake.

Throughout the movie a character claiming to be the second coming of Jesus appears. Towards the very end of the film he blesses the viewers of the film. This man is Sergey Anatolyevitch Torop who has later received much attention as the religious leader Vissarion (Russian mystic)|Vissarion.

== Embellishments==
Herzog, as he often does, embellished the story of the Lost City considerably, acknowledging his fabrications fully:
:"I wanted to get shots of pilgrims crawling around on the ice trying to catch a glimpse of the lost city, but as there were no pilgrims around I hired two drunks from the next town and put them on the ice.  One of them has his face right on the ice and looks like he is in very deep meditation.  The accountant’s truth: he was completely drunk and fell asleep, and we had to wake him at the end of the take." "Herzog on Herzog", by Paul Cronin (London: Faber and Faber Ltd., 2002) 
Herzog defends the fabrication as reaching a greater truth:
:"I think the scene explains the fate and soul of Russia more than anything else."

This is in keeping with Herzogs beliefs about truth in film.

The film also contains shots of pilgrims, which are in fact people ice fishing. The chanting Siberians are only performing religious services in one of their two major scenes. In the other they are simply singing a love song.

== References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 