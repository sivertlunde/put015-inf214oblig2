Unnecessary Fuss
{{Infobox film name        = Unnecessary Fuss image       = UnnecessaryFuss.jpg caption     = Cover of the 1984 PETA film based on footage removed from the University of Pennsylvania by the Animal Liberation Front. director    = producer  Alex Pacheco (PETA) writer      = starring    = distributor = released    = runtime     = 26 minutes language    = English country     = United States budget      =
}}
 Alex Pacheco of People for the Ethical Treatment of Animals (PETA), showing footage shot inside the University of Pennsylvanias Head Injury Clinic in Philadelphia.

The footage was shot in 1983–1984 by the researchers themselves as they inflicted brain damage on baboons  with a hydraulic device. The video footage released by PETA can be viewed at:
*  *  *  *  *   The experiments were conducted as part of a research project into head injuries caused by vehicle and sports accidents. The footage shows the researchers laughing at the baboons as the brain damage is inflicted. 

Sixty hours of audio- and videotape were removed from the laboratory during a raid in May 1984 by the Animal Liberation Front, who handed it over to PETA. It was subsequently edited down to 26 minutes with a voice-over commentary by Newkirk, before being distributed to the media and Congress. Charles McCarthy, director of the Office for Protection from Research Risks (OPRR) wrote that the film had overstated the deficiencies in the clinic, but that the OPRR had found serious violations of the Guide for Care and Use of Laboratory Animals.  As a result of the publicity, the lab was closed, the chief veterinarian fired, and the university placed on probation.

The title of the film comes from a statement made to The Globe and Mail by the head of the clinic, neurosurgeon Thomas Gennarelli before the raid.  He declined to describe his research to the newspaper because, he said, it had "the potential to stir up all sorts of unnecessary fuss ..." 

==Contents of the film==
 
 , V40(1) 1999. 

After the injury is sustained, the baboons head is dislodged from the helmet using a hammer and screwdriver. One sequence shows part of the baboons ear being torn off along with the helmet. After pulling the baboons head from the helmet, the researcher is heard to laugh, saying: "Its a boy," then, "Looks like I left a little ear behind." Ingrid Newkirk|Newkirk, Ingrid. Free the Animals. Lantern Books, 2000, pp. 194–195. 
 electrocautery on an apparently conscious baboon, smoking cigarettes and pipes during surgery, and playing loud music as the animals are injured. A researcher is seen holding a seriously injured baboon up to the camera, while others speak to the animal: "Dont be shy now, sir, nothing to be afraid of," followed by laughter, and "He says, youre gonna rescue me from this, arent you? Arent you?," followed by more laughter.  

While one baboon was being injured on the operating table by the hydraulic device, the camera panned to a brain-damaged, drooling monkey strapped into a high chair in a corner of the room, with the words "Cheerleading in the corner, we have B-10. B-10 wishes his counterpart well. As you can see, B-10 is still alive. B-10 is hoping for a good result," followed by laughter. In another sequence, one researcher is heard to say: "You better hope the ... anti-vivisection people dont get a hold of this film."  Ingrid Newkirk|Newkirk, Ingrid. Free the Animals. Lantern Books, 2000, p. 196. 

===OPRR investigation===
An investigation was conducted by 18 veterinarians from the American College of Laboratory Animal Medicine, commissioned by the Office for Protection from Research Risks (OPRR). Charles R. McCarthy, director of the OPRR at the time, wrote that " espite the fact that Unnecessary Fuss grossly overstated the deficiencies in the Head Injury Clinic, OPRR found many extraordinarily serious violations of the Guide for Care and Use of Laboratory Animals ... Furthermore, OPRR found deficiencies in the procedures for care of animals in many other laboratories operated under the auspices of the university." 

The violations included that the depth of anaesthetic coma was questionable; that most of the animals were not seen by a veterinarian either before or after surgery, survival surgical techniques were not carried out in the required aseptic manner; that the operating theater was not properly cleaned; and that smoking was allowed in the operating theater despite the presence of oxygen tanks.  

When PETA made its 26-minute film available, the OPRR initially refused to investigate because the film had been edited from 60 hours of videotape. For over a year PETA refused to release the original footage. When they eventually handed over the unedited material, the OPRR discovered that the footage of the brain damage being inflicted involved just one baboon out of the 150 who had received the Penn 2 injuries. The film gave the impression that the brain-damage scenes involved several animals.   

The OPRR identified 25 errors in Newkirks voice-over commentary. One example was where an accidental water spill over a conscious baboon during a surgical procedure was identified, incorrectly, by one of the Head Injury Clinics researchers, and subsequently by Newkirk, as "perhaps acid."  *Ingrid Newkirk|Newkirk, Ingrid & Alex Pacheco (animal rights)|Pacheco, Alex.  , video, 26 minutes, People for the Ethical Treatment of Animals. 1984. 

===Sit-in and clinics closure===
After a three-day sit-in by animal-rights activists at the  , undated, retrieved February 26, 2006.  The OPRR also found deficiencies in other laboratories operated by the university. The universitys chief veterinarian was fired, new training programs were initiated, and the university was placed on probation, with quarterly progress reports to OPRR required.

== See also ==
* Pit of despair
* Testure, a music video by the industrial rock band Skinny Puppy

==Notes==
 

==References==
 
 
* ,  ,  ,  ,  , the video composed of footage taken from the lab by the Animal Liberation Front
*McCarthy, Charles. R.  , The Online Ethics Center for Engineering and Science at Case Western Reserve University, undated, retrieved February 26, 2006
*Ingrid Newkirk|Newkirk, Ingrid. Free the Animals. Lantern Books, 2000. ISBN 1-930051-22-0
*Newkirk, Ingrid & Alex Pacheco (animal rights)|Pacheco, Alex. Unnecessary Fuss, video, 26 minutes, available from PETA.  
*Sideris, Lisa, McCarthy, Charles, & Smith, David H.  , Institute for Laboratory Animal Research Journal, V40(1) 1999.
* 
 

 
 

 
 
 
 
 
 
 
 
 