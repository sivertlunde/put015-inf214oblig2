Scream and Scream Again
{{Infobox film
| name           = Scream and Scream Again
| image_size     =
| image	         = Scream and Scream Again FilmPoster.jpeg
| caption        =
| director       = Gordon Hessler
| producer       = Max Rosenberg Milton Subotsky Louis M. Heyward
| writer         = Christopher Wicking based on = the novel The Disorientated Man by Peter Saxon
| narrator       =
| starring       = Alfred Marks   Vincent Price Christopher Lee Peter Cushing Michael Gothard David Whitaker
| cinematography = John Coquillon
| editing        = Peter Elliot
| studio    = American International Pictures distributor = AIP (USA)
| released       =  
| runtime        = 95 minutes
| country        = United Kingdom
| language       = English
| budget         = $350,000 Ed. Allan Bryce, Amicus: The Studio That Dripped Blood, Stray Cat Publishing, 2000 p 56-61 
| gross          = $1,217,000 (US/ Canada rentals) 
}}
Scream and Scream Again is a 1970 conspiracy-thriller / science fiction film.
 The Oblong Box, of actors Vincent Price and Christopher Lee with director Gordon Hessler. Price and Lee only share a brief scene in the films climax, however.

The movies title, and association with stars Price, Lee and Peter Cushing have given it an undeserved reputation as a violent horror film, but the violence in the film is mostly understated and/or off-screen, while the plot owes more to films like Invasion of the Body Snatchers or 1970s era conspiracy thrillers like The Parallax View.

The Overlook Film Guide: Science-Fiction, acknowledges it as: "one of the best science-fiction films made in Britain."

== Plot ==
The movies structure is fragmented, as it alternates between three distinguishable plot threads.

A man jogging through suburban London grabs his heart, and collapses. He wakes up in a hospital bed. The nurse tending him give him water. She leaves. He pulls down the bed covers to discover both his legs have been amputated. He screams.

An official (Marshall Jones) from an unidentified Eastern European totalitarian state arrives back at his home country. Upon being debriefed by a superior officer, the man steps around the table, places a hand on the mans shoulder, paralyzes him, and kills him.

A Metropolitan London detective (Alfred Marks) and his unit, investigate the deaths of several young women in the city. The women, picked up at nightclubs by Keith (Michael 
Gothard), have apparently been killed by the same individual, and some of the bodies have 
been drained of blood. 

The centerpiece of the movie is near 15 min. long police - murder suspect car-chase/foot-chase sequence through suburban London.

Vincent Price plays a doctor whose clinic specializes in limb and organ transplantation.

Christopher Lee plays the head of Britains -unnamed- intelligence services.

Peter Cushing -third-billed- plays an unidentified official in the Eastern European country; a very brief cameo role.

The three plot lines converge in a chilling -and unexpected- climax.

==Cast==
* Alfred Marks as Detective Superintendent Bellaver
* Vincent Price as Dr. Browning
* Christopher Lee as Fremont
* Peter Cushing as Major Heinrich Benedek
* Michael Gothard as Keith
* Christopher Matthews as Dr. David Sorel
* Judy Huxtable as Sylvia
* Anthony Newlands as Ludwig
* Kenneth Benda as Professor Kingsmill
* Marshall Jones as Konratz
* Uta Levka as Jane
* Yutte Stensgaard as Erika
* Julian Holloway as Detective Constable Griffin
* Judy Bloom as Helen Bradford
* Peter Sallis as Schweitz Amen Corner as themselves

==Production==
The movie is based on Peter Saxons science fiction novel "The Disoriented Man". For the most part, the movie follows the novel quite closely.

In the novel, the antagonists turned out to be aliens. According to an interview with Christopher Lee, the characters were indeed going to be revealed as aliens in the movies climax, but all connections to that fact were cut out of the movie before it was released, leaving the enigmatic villains backgrounds unexplained.

Rights to the novel were bought by Milton Subotsky of Amicus Productions who got financing from Louis Heyward head of European operations for American International Pictures|AIP. 

There was a script but Gordon Hessler says he got Chris Wickling to heavily rewrite it:
 That was really a pulp book, a throwaway book that you read on a train. There was nothing in it, just empty pieces of action. But it was Chris who gave it a whole new level by using it as a political process of what might happen in the future. That is what made the picture, hes the one that came up with all those ideas, yet he still managed to keep the nuances of the sort of pulp fiction novel.   Amen Corner, who appeared in the film singing it. This was one of their last appearances before Andy Fairweather-Low departed for a solo career after a brief career as Fair Weather.

This marked the first time Peter Cushing, Vincent Price and Christopher Lee appeared in the same film. The three, however, do not share screen-space. Cushing does not appear with either Lee or Price - only appearing in a cameo. Lee and Price share a brief scene towards the films climax.

The opening credits list the copyright as 1969, and Judy Huxtable is billed as a "guest star".

==References==
 

==External links==
* 

 
 

 

 
 
 
 
 
 
 
 
 