Flickan från tredje raden
{{Infobox film
| name=Flickan från tredje raden
| image= 
| caption= 
| director=Hasse Ekman
| producer=Lorens Marmstedt, Terrafilm
| writer=Hasse Ekman
| starring=Hasse Ekman Eva Henning Hilda Borgström
| music=Sune Waldimir
| released=29 August 1949
| runtime=82 min
| country=Sweden Swedish
}}
 Swedish comedy comedy film directed by Hasse Ekman.

==Plot==
An extraordinary ring is being handed down amongst many people. They find it and lose it or give it away, but the ring always gives each owner good fortune or hope in some way. An angel watches over the ring and the people wearing it.

==Cast==
* Hasse Ekman as Sture Anker, theatre manager
* Eva Henning as the angel
* Hilda Borgström as Vilma Andersson
* Maj-Britt Nilsson as Birgit
* Sven Lindberg as Göte Gunnar Olsson as jeweller Lilja
* Sigge Fürst as Gusten Örjevall
* Siv Thulin as Sonja Örjevall
* Stig Olin as Kalle
* Ingrid Backlin as nurse Maj
* Gunnar Björnstrand as Edvin Burelius
* Hilding Gavle as Fredrik Antonsson
* Barbro Hiort af Ornäs as Dagmar Antonsson
* Francisca Lindberg as the little girl Charlotte, daughter of Birgit and Göte

==External links==
* 

 
 

 
 
 
 
 