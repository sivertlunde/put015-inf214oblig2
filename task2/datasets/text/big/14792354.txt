Life on a String (film)
{{Infobox film
| name           = Life on a String
| image          = Life On A String DVD.jpg
| image_size     = 
| caption        = DVD release cover
| director       = Chen Kaige
| producer       = Karl Baumgartner, Cai Rubin Donald Ranvaud
| writer         =  
| starring       = Liu Zhongyuan Huang Lei Xu Qing
| music          = Qu Xiaosong
| cinematography = Gu Changwei
| editing        = Pei Xiaonan Kino International
| released       = 1991
| runtime        = 110 minutes
| country        = China
| language       = Mandarin
| budget         = 
}}
 1991 Cinema Chinese film by acclaimed film director Chen Kaige. Made before his international breakthrough Farewell My Concubine (film)|Farewell, My Concubine, Life on a String is a more intimate and philosophical affair, telling the story of a blind sanxian player and his young disciple. The film was based on the novel by Shi Tiesheng.  The film was entered into the 1991 Cannes Film Festival.   

== Cast ==
*Liu Zhongyuan as the Old Master, a master blind sanxian player, as a child he was told that upon wearing out his 1000th string, his sight would be restored.
*Huang Lei as Shitou, his young disciple. 
*Xu Qing as Lanxiu

==References==
 

==External links==
* 
* 
* 
*  at the Chinese Movie Database

 

 
 
 
 
 
 
 
 

 