Page One: Inside the New York Times
{{Infobox film
| name           = Page One: Inside the New York Times
| image          = Page One Poster.jpg
| caption        = Theatrical release poster
| director       = Andrew Rossi
| producer       = Josh Braun David Hand Kate Novack Alan Oxman Adam Schlesinger
| writer         = Kate Novack Andrew Rossi David Carr Bruce Headlam Richard Perez-Pena Tim Arango Bill Keller Brian Stelter
| music          = Paul Brill
| cinematography = Andrew Rossi
| editing        = Chad Beck Christopher Branca Sarah Devorkin
| studio         = Participant Media
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| gross       = $1,067,028  
}}

Page One: Inside the New York Times is an American documentary film by Andrew Rossi, which premiered at the 2011 Sundance Film Festival. Magnolia Pictures and Participant Media jointly acquired the U.S. distribution rights and released the film theatrically in Summer 2011.    The film grossed over one million dollars at the US box office and has been nominated for two News & Documentary Emmy Awards as well as a Critics Choice Movie Award|Critics Choice Award for Best Documentary Feature.  

==Synopsis==
From the Sundance Program Description: David Carr track print journalism’s metamorphosis even as their own paper struggles to stay vital and solvent, publishing material from WikiLeaks and encouraging writers to connect more directly with their audience. Meanwhile, rigorous journalism—including vibrant cross-cubicle debate and collaboration, tenacious jockeying for on-record quotes, and skillful page-one pitching—is alive and well. The resources, intellectual capital, stamina, and self-awareness mobilized when it counts attest there are no shortcuts when analyzing and reporting complex truths."  

===Stories and issues=== Publication of Afghan war logs by WikiLeaks
* Release of the iPad
* Bankruptcy of the Tribune Company
* NBC Universal merger with Comcast
* The Jayson Blair scandal Judith Miller
* Gawker and its "Big Board"
* Pro Publica and new models for investigative reporting Charging for news online
* Watergate and the Pentagon Papers
* Staff cuts in Network News and coverage of the White House
* The purported end of U.S. combat operations in Iraq Vice

==Cast==
 
; New York Times Media Desk David Carr: Media columnist
* Bruce Headlam: Media editor
* Richard Pérez-Peña: Media reporter
* Tim Arango: Former media reporter, Baghdad bureau chief
* Brian Stelter: Media reporter

; New York Times Business Desk
* Andrew Ross Sorkin: Financial columnist
* Larry Ingrassia: Business editor

; New York Times Foreign Desk
* Susan Chira: Foreign editor Ian Fisher: Deputy foreign editor Joseph Kahn: Deputy foreign editor

; New York Times Masthead
* Bill Keller: Executive editor
* Jill Abramson: Managing editor
* Dean Baquet: Assistant managing editor/Washington bureau chief
 
; Featured interviews
* Sarah Ellison
* Clay Shirky
* Carl Bernstein
* David Remnick
* Nick Denton
* Jeff Jarvis
* Gay Talese Alex Jones
* Katrina vanden Heuvel
* Jimmy Wales
* Nicholas Lemann
* Seth Mnookin
* Michael Hirschorn
* James McQuivey
; Others featured
* Julian Assange (In phone interview with Brian Stelter)
* Brian Lam
* Markos Moulitsas
* Noam Cohen: Special columnist
 

==Critical reception==
The film was nominated for a 2011 Critics Choice Movie Award|Critics Choice Award for Best Documentary and has also been nominated for two News & Documentary Emmy Awards.
 Vanity Fair called it "slick, fun, and surprisingly sexy."   Somewhat less positively, Justin Chang of Variety (magazine)|Variety says of the film, "Rossis coverage of daily news meetings and interviews with editorial staffers arent as juicy as one might have hoped or expected, but for journos (who will likely rep the films most appreciative audience), simply being a fly on these hallowed walls will offer much to savor,"  but Eric Kohn of Indiewire counters, "Rossi captures the minutiae of the newsroom, from the rapid transcription of interviews to the rush of deadlines, as if observing an Olympic sport."   Regarding David Carr, Tim Wu of Slate (magazine)|Slate describes him as "a sympathetic hero for what turns out to be a riveting film,"  and David Fear of Time Out Chicago adds, "its his H.L. Mencken–like attitude toward old-school reporting that offers the best example for why traditional news-gathering won’t ever truly die."   Sebastain Doggart of the UKs Telegraph describes Carr as the "Keith Richards of the Fourth Estate," and adds that the film is "enthralling" and "inspiring." 

A notable departure from the positive reception was Michael Kinsleys review in the Times itself.

The film is currently the seventh most popular newspaper movie on Netflix streaming. 
 

==Awards== Denver Film Critics Society - Best Documentary 
*Phoenix Film Critics Society - Best Documentary 
*Oklahoma Film Critics Circle - Best Documentary 
*San Diego Film Critics Society - Best Documentary nomination 
*Dallas-Fort Worth Film Critics Association - Best Documentary nomination 
*Broadcast Film Critics Association - Best Documentary nomination 
*News & Documentary Emmy Awards - Outstanding Informational Programming - Long Form nomination, Outstanding Individual Achievement in a Craft: Editing – Documentary and Long Form nomination 

==References==
 

==External links==
*  
*  
*   at Sundance Film Festival
*  

 
 
 
 
 
 
 
 
 
 
 