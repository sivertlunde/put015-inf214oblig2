Awaaz
 
{{Infobox film
| name           = Awaaz
| image          = Awaaz.jpg
| image size     =
| caption        = Promotional Poster
| director       = Shakti Samanta
| producer       = Shakti Samanta
| Dialogue      = Kamleshwar
| narrator       =
| starring       = Rajesh Khanna Jaya Prada Rakesh Roshan Amrish Puri Suresh Oberoi Madan Puri Prem Chopra Suresh Oberoi
| music          = R D Burman Anand Bakshi (Lyrics)
| cinematography =  Alokedas Gupta
| editing        = Bijoy Chowdhary
| distributor    =
| released       =  November 23, 1984
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

Awaaz is a 1984 Hindi movie directed by Shakti Samanta with Rajesh Khanna  in the lead role and supported by Jaya Prada, Rakesh Roshan, Suresh Oberoi, Supriya Pathak, Iftekar, Prem Chopra, Madan Puri and Amrish Puri.      
The music is by R.D.Burman.  The songs are sung by Kishore Kumar for Rajesh, and by Asha Bhonsle for the females.
This film marked the first return of the Shakti-Rajesh team after Anurodh in 1977.

==Plot==
Advocate Jayant (Rajesh Khanna) a criminal lawyer believes that a lawyers duty is to do service to his client alone and to bail him out of his trouble even if his client is in reality a criminal. His close friend Amit is an honest police inspector who brings many of the goons that are part of gang of smugglers led by Mulchand Malhotra but they are let off by the court when he uses his skills and knowledge about the loopholes in law. Jayant helps Mulchand in setting his workmen free from jail. Amit Gupta asks Jayant to be more responsible when he defends criminals, who are repeat offenders. Later Amit is killed and this motivates Amits brother Vijay to join the police force. Meanwhile Priya (Jayants sister) falls in love with Vijay and both decide to marry. But one day while going on a picnic Anu (Jayants wife) and Priya get into trouble as their car tire gets punctured on a road near a jungle. There a drunkard who happens to be Mulchands son rapes Anu and injures Priya. On being humiliated Anu commits suicide. Jayant also gets a clue that the death of his friend Amit was not accidental but was a deliberate attempt by some goons and that the rapist is a person known to Mulchand and his gang members. Jayant tries enlisting the help of his clients Mulchand and Meerchandani in finding the real culprits but realises that they are not responsive. Jayant vows to find the assailants, but before he can attempt anything his daughter, Nandita, is kidnapped. The kidnappers want Jayant to represent Mulchands son in a criminal case, and get him acquitted. Mulchand says his daughter is with them and would be left once his son is let off in a court of law. Jayant gets his client out on bail. Jayant does not know that his client is the one who had raped his wife and sister, and when Jayant does find out there is noting he can do since his daughter still under the control of her kidnappers. The rest of the story is how he single handedly brings all of them to justice.

==Reception==
Awaaz was well appreciated by audience and critics alike. It received three stars in the Bollywood guide Collections.  Awaaz fetched Rs.3.60&nbsp;crores in the year 1984 at the box office.   

==Cast==
*Rajesh Khanna as Advocate Jayant  
*Jaya Prada as Anu
*Amrish Puri as Mulchand Malhotra
*Suresh Oberoi as Inspector Amit Gupta
*Rakesh Roshan as Vijay Gupta
*Prem Chopra as Gagan
*Madan Puri  as Mirchandani
*Supriya Pathak Priya
* Mac Mohan as Rustam
* Sudhir   as Saymond Parerra
* Beena Banerjee as Beena Gupta
* Monty
* Iftikhar  
* Guruvinder

==Music==
*"Zindagi Sau Baras Ki" – Kishore Kumar, Asha Bhosle
*"Bolo Pyar Karogi" – Kishore Kumar, Asha Bhosle
*"Aa Jaaneman Aaj Tujhe" – Kishore Kumar
*"Ankhon Ki Zuban Ne" –  Kishore Kumar, Asha Bhosle
*"Zindagi Awaaz Deti Hai" – Asha Bhosle
*"Zindagi Sau Baras Ki" – Kishore Kumar
*" Theme Music" – Manohari Singh

==References==
 

==External links==
* 
* 

 
 
 
 
 