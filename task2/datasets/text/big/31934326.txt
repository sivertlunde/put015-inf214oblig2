Behind the Mask (1992 film)
{{Infobox film
| name           = Behind the Mask
| image          = Mask_Poster.jpg
| alt            =
| caption        = Poster
| director       = Warren Chaney Darlene Staffa (Assistant Director)
| producer       = Warren Chaney Beverly Wilson (Executive) David Sanders (Assistant)
| screenplay     = Warren Chaney
| starring       = Roy Alan Wilson Deborah Winters Steve Word Luis Lemus
| music          = Ted Mason
| cinematography = Craig Bailey
| editing        = Rick Christie
| studio         = Sandpiper Productions Warren Chaney Productions, Inc.
| distributor    = Intercontinental Releasing Corporation Metro-Goldwyn-Meyer
| released       =  
| runtime        = 180 minutes
| country        = United States
| language       = English
| budget         = $3.6 million
| gross          = $32.3 million 
}} 1992 film, released by Intercontinental Releasing Corporation (IRC) in 1992 and by Metro-Goldwyn-Meyer in 1998.     The production utilized a new emerging 3-D film technology for television at the time of its filming.     The film was written and directed by Warren Chaney. Beverly Wilson was the executive producer, Warren Chaney, the producer and David Sanders, the associate producer. It starred Roy Alan Wilson, Deborah Winters, Steve Word and Luis Lemus.   The motion picture was originally released in separate chapters with each chapter attached to a film.    Following its theatrical run, it  was released to television as a broadcast miniseries.   This represented the first movie serial produced since 1965. After acquisition by Metro-Goldwyn-Meyer, the film was reedited into a film and re-released. In 1998, it was released to home video on laser disc.    

Behind the Mask is considered by some film historians to be historic in that the serial genre was updated by following a 1940s and 1950s serial format while using a new 3-D television technology.    Reviews were favorable upon release and it has since developed somewhat of a cult following. 

==Plot==
  stars as The Mask]]
 thriller film, first released in six separate chapters and later as a film.  The films adversary, an unidentified masked entity known only as The Octopus, terrorizes a large American city. He engages in industrial espionage and terror in order to secure valuable technological secrets that will lead to America’s decline as a world power. The Octopus is on the verge of success when another unknown figure appears. The Mask, as he becomes known, thwarts the Octopus and his organized crime syndicate in the villain’s one failed attempt after another. 

  stars as Annie Strayton]]
Noted FBI Agent, Annie Strayton (Deborah Winters) is assigned to the case from Washington, D.C. At first, she views the Mask as an adversary but soon joins forces with him in an effort to bring the Octopus and his mob to justice.

Identities of the Mask and Octopus are unspecified throughout most of the film. However, both masked figures are identified as members of a vital government funded secret alliance known as Research Associates. The seven members of the team are each considered a suspect but as the film progresses; they are killed off until only two remain. One is the Mask and the other, the Octopus.
 

Chapter after chapter follow with each ending with a cliffhanger finale. In Chapter One, the Mask and Agent Strayton are embroiled in a gun battle atop a 75-story skyscraper.   As the chapter closes, the Mask is thrown from the top and plunges toward the city streets. In Chapter Two, the audience observes that a section of the crime fighter’s jacket opens into a small stunt parachute permitting the Mask to land safely on the streets below. Chapter after chapter follow, each leaving one or more of the main characters in danger, only to be saved in a subsequent chapter.

The film’s storyline combines action and mystery. Both the Mask and Octopus are adept at disguise, leaving the hanging question, "Who is who?" In one chapter – the film heroine Agent Strayton, is discovered to have been substituted by a member of the Octopus’ gang. In another scene, the Mask dons the identity of a missing member from Research Associates.
 ) search for the Octopus in the citys dark underground]]

The city’s dark underground serves as the backdrop for Behind the Mask’s closing scenes as the two adversaries face off in a life or death struggle. Agent Strayton is captured and nearly killed before being rescued by the Mask. Strayton and the Mask expose the Octopus’ identity before he dies in a final combat scene. Strayton knows that the remaining scientist must be the Mask.

 
The Mask removes his disguise revealing his identity as the remaining scientist, Dr. Lane Elliott. Elliott replaces his mask and exits as law enforcement agents and officers arrive to remove the remaining members of the Octopus’ gang. Annies boss congratulates her on the capture of the criminals. As Strayton walks away, he says, “This was a good day. We’ve learned the identity of the Octopus and youve learned the identity of the Mask.”

Strayton pauses as if considering the truth of her supervisor’s statement.

The Mask sits in a dark dimly lit lair. As he removes his mask once again, he is seen to be Dr. Lane Elliott. Elliott smiles lightly as he lifts a corner of his face revealing yet another mask. His face remains unseen as black-gloved hands remove the “Elliott” mask to a table on which many facemasks reside. Footsteps pass into the distance as the camera pushes in on a collection of masks in the center of which is the black cowl belonging to the mysterious and still unknown, crime fighter. Series of lights switch off until only one light illuminates the remaining disguise. The final light fades and the screen goes to black leaving unanswered, “Who is behind the mask?”

==Cast==
 
 ]]

Many members of the cast were used in multiple roles though the use of extensive prosthetic makeup applied by the brother/sister team of Phillip and Melissa Nichols.  This appeared to be in keeping with the film’s title and general theme of who is “behind the mask.” 

* Roy Alan Wilson as Dr. Lane Elliott Swanson and The Mask
* Deborah Winters as Agent Annie Strayton
* Steve Word as Dr. Alex Raymond and The Octopus
* Kevin Hickman as Dr. Lee Falk
* James Keany as Dr. Bill Whitney
* Luis Lemus as Deatherage
* John Swasey as Agent John Reed
* Woodrow Thomas as Jack Shepherd
* Rene’ Gatica as Dr. FredrickLouis
* Gage Tarrant as Lynn Garnett
* Joshua Blyden as Irish Thug
* John Hervey as Matt Williams
* Ed Mixon as James Andrews
* Brian Castillo as Tony Louis
* Barbara Jenkins as Foster Mother
* Kim I. Morrell as Dr. Barry Carson
* Dave Ward as TV Newscaster
* Stacey Cortez as Carson’s Daughter
* Karen Cook as Linda Clark
* Dyna Steele as Policewoman
* Warren Chaney as FBI Agent
* Beverly Wilson as FBI Agent
* Joe West as Door Man
* Scott Andrews as Lab Assistant

==Production==

After the screenplay was written, Beverly Wilson, President of Sandpiper Productions and the film’s Executive Producer, gave company approval to begin production. She made the decision to film the movie using a newly developed 3-D technology, which would enable it to be broadcast in 3-D for later television distribution. Warren Chaney, the projects screenwriter was then hired to produce and direct. Chaney had just completed an earlier production for Sandpiper, a western titled, The Broken Spur (1992). 
  discusses a scene with actors John Swasey (center) and Gage Tarrant (seated)]]

Chaney employed stunt specialists and practical effects for the majority of the production instead of using digital compositing that was still in its infancy. The filming was complex because of the 800-pound weight of the camera system and long setup times for each shot.  In an interview with a fan magazine, Chaney said, “It was very time consuming to light and set up the scenes because you had to make certain no disembodied heads were floating through the theatres in 3-D. The camera weight was difficult because it made movement troublesome. Still it forced me to tell a story in a different way than I ever had before.” 

The vehicle driven by the Mask was a high-performance vehicle built for the production by car designer Charlie Van Natter. Van Natter took his task seriously, actually equipping the vehicle with the automatic weaponry and rocket launchers used in the film. The car could reach speeds up to 160&nbsp;mph and had such sophisticated mechanics that special stunt drivers were required to man the production during high-speed chases. 

The special effects prosthetic makeup brother/sister team of Phillip and Melissa L. Nichols were brought into the production to develop the effects makeup, costuming and the many facial disguises required for the film.     Roy Alan Wilson who portrayed the Mask, Steve Word as the Octopus and Deborah Winters as FBI Special Agent Annie Strayton, were required to assume multiple roles. In an interview for America Film Review, actress Deborah Winters commented, “The Nichols’ team makeup was so good that I once stood off camera carrying on a conversation with Roy (Alan Wilson). I must have been doing most of the talking because when he finally said something I realized that it wasn’t him but another of our actors “made-up” to look like him. It gave me the chills.”  

Veteran Stuntman, David Sanders developed and coordinated the film’s stunts. Behind the Mask employing more than 80 stuntmen with various specialties.  In one scene, the Mask falls from atop a 75-story building and escapes by pulling a parachute after a 40-story drop.   In another, the Mask high dives from one skyscraper to another, requiring a stuntman whose specialty was long leaps and high falls. Still others handled the large fight scenes, pyrotechnics and high-performance gymnastics. Early on, Chaney had made the decision to film the stunts as action sequence devoid of effects graphics. In an interview with a local CBS Houston affiliate, Chaney commented, “We were shooting the first serial feature since 1954 and while updating the genre, I wanted to stay with the spirit of what the filmmakers used to do.”  

Executive producers are seldom involved in the technical process of film making but Beverly Wilson appears to be the exception. She makes a character appearance on screen  as well as being actively involved in all facets of the production. 

From start to finish, Behind the Mask required 2 years (1990–1992) of production. 

==Distribution==
Behind the Mask was distributed by Intercontinental Releasing Corporation (IRC) in 1992. It was picked up for continuing distribution by Metro-Goldwyn-Meyer in 1998 and was released to home video on laser disc in 1996.    However, as of 2011 the movie has not been released on DVD or Blu-ray.

==Box Office and Budget==
Behind the Mask was produced for $3.5 million in 1990-1992.  The film earned $32.4 million from total distribution in and out of the United States. 

==Awards|Accolades==
Warren Chaney won the Best Director and Best Screenplay CineCon ’92 Award and a Best Screenplay American Cinema Award for Behind the Mask. The picture received the Silver Award in the WorldFest-Houston International Film Festival ’93 and a Bronze Medal Award in the New York Film Festival – 1993. Deborah Winters was nominated for Best Actress and Luis Lemas as Best Supporting Actor for the CineCon ’92 Awards. 
 Bob Lanier of Houston, Texas declared the date as Behind the Mask Day.
However on IMDB the film was given a 2.7.

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 