Kaakan
{{Infobox film
| name           = Kaakan
| image          = Kaakan-Marathi-Movie-Poster.jpg
| alt            = Kaakan promotional poster
| caption        = 
| film name      =  
| director       = Kranti Redkar
| producer       =  
| writer         =  
| screenplay     = 
| story          = 
| based on       =  
| starring       = Jitendra Joshi   Urmila Kanitkar 
| narrator       =  
| music          = 
| cinematography = 
| editing        = 
| studio         =  
| distributor    =  
| released       =   
| runtime        = 140 minutes
| country        = 
| language       = Marathi
| budget         = 
| gross          =  
}}

Kaakan is a Marathi language movie released in 2015 directed by Kranti Redkar starring Jitendra Joshi and Urmila Kanitkar on lead roles. 

== Cast ==
* Jitendra Joshi
* Urmila Kanitkar
* Ashutosh Gaikwad
* Ashok Shinde 
* Madhavi Juvekar

== Release and reception ==
Kaakan was released on 10 April 2015. Pune Mirror rated the movie with two and a half star saying that Kaakan would be only appealing to a specific type of audience, which prefers a quick dose of emotional melodrama. 

== Music ==
Soundtracks for Kaakan is composed by debut music director Ajay Singha.
 
{{tracklist
| headline        = 
| extra_column    = Artist(s)
| total_length    = 
| note         = 
| title1          = Kaakan
| extra1          = Shankar Mahadevan & Neha Rajpal
| length1         = 4:48
| note1         = 
| title2          = Wedyancha Ghar Unnhat
| extra2          = Swapnil Bandodkar
| length2         = 4:32
| note2         = 
| title3          = Athya Pathya
| extra3         = Shriram Iyer Anish
| length3         = 3:16
| note3        = 
| title4          = Kaakan (Reprise)
| extra4        = Raman Mahadevan & Neha Rajpal
| length4         = 5:26
| note4        = 
| title5          = Suki Poli
| extra5        = Shriram Iyer Anish
| length5         = 3:56
| note5        = 
| title6          = Saajana
| extra6        = Hamsika Iyer
| length6         = 3:57
| note6        = 
}}

== References ==
 


== External links ==
*  

 