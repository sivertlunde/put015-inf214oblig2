Transported
 
{{Infobox film
  | name     = Transported
  | image    = Transported_still.jpg
  | caption  = Still from the film
  | director = W. J. Lincoln		
  | producer = 
  | writer   = W. J. Lincoln  Godfrey Cass
  | starring = Roy Redgrave
  | music    = 
  | cinematography = Maurice Bertel
  | editing  = 
  | studio = Lincoln-Cass Films
  | released = 29 September 1913 (Melbourne) 
  | runtime  = 2,500 feet
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}

Transported is an Australian convict melodrama film directed by W. J. Lincoln. It is considered a lost film. 

==Plot==
In England, Jessie Grey is about to marry Leonard Lincoln but the evil Harold Hawk tries to force her to marry him and she wounds him with a gun. Hawk is arrested and sentenced to imprisonment in Australia. Leonard and Jessie get married and move to Australia. Hawk escapes from gaol and tries to get his revenge by kidnapping Jessie. 

==Cast==
*Roy Redgrave
*Godfrey Cass George Bryant

==References==
 

==External links==
*  at IMDB
*  at AustLit
*  at National Film and Sound Archive

 

 
 
 
 


 