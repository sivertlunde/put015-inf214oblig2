I Am Curious (Yellow)
 
{{Infobox film
| name = I Am Curious (Yellow)
| image = CuriousYellowPoster.jpg
| alt = 
| caption = North American release poster
| director = Vilgot Sjöman
| producer = Göran Lindgren   Lena Malmsjö
| writer = Vilgot Sjöman  
| starring = Vilgot Sjöman Lena Nyman Börje Ahlstedt
| music = Bengt Ernryd  
| cinematography = Peter Wester  
| editing = Wic Kjellin  
| studio = Janus Films
| distributor = Grove Press
| released =  
| runtime = 122 minutes
| country = Sweden
| language = Swedish
| budget = 
| gross = $27.7 million  
}}
I Am Curious (Yellow) ( , meaning "I Am Curious: A Film in Yellow") is a 1967 Swedish drama film written and directed by Vilgot Sjöman, starring Sjöman and Lena Nyman. It is a companion film to 1968s I Am Curious (Blue); the two were initially intended to be one 3½ hour film.  The films are named after the colours of the Swedish flag.

== Plot ==

Director Vilgot Sjöman plans to make a social film starring his lover Lena Nyman, a young theater student who has a strong interest in social issues. 

Nymans character, also named Lena, lives with her father in a small apartment in Stockholm and is driven by a burning passion for social justice and a need to understand the world, people and relationships. Her little room is filled with books, papers, and boxes full of clippings on topics such as "religion" and "men", and files on each of the 23 men with whom she has had sex. The walls are covered with pictures of concentration camps and a portrait of Francisco Franco, reminders of the crimes being perpetrated against humanity. She walks around Stockholm and interviews people about social classes in society, conscientious objection, gender equality, and the morality of vacationing in Francos Spain. She and her friends also picket embassies and travel agencies. Lenas relationship with her father, who briefly went to Spain to fight Franco, is problematic, and she is distressed by the fact that he returned from Spain for unknown reasons after only a short period. 

Through her father Lena meets the slick Bill (Börje in the original Swedish), who works at a menswear shop and voted for the Rightist Party. They begin a love affair, but Lena soon finds out from her father that Bill has another woman, Marie, and a young daughter. Lena is furious that Bill has not been open with her, and goes to the country on a bicycle holiday. Alone in a cabin in the woods, she attempts an ascetic lifestyle, meditating, studying nonviolence and practicing yoga. Bill soon comes looking for her in his new car. She greets him with a shotgun, but they soon start to make love. Lena confronts Bill about Marie, and finds out about another of his lovers, Madeleine. They begin to fight and Bill leaves. Lena has strange dreams, in which she ties two teams of soccer players – she notes that they number 23 – to a tree, shoots Bill and cuts his penis off. She also dreams of being taunted by passing drivers as she cycles down a road, until finally Martin Luther King Jr. drives up. She apologizes to him for not being strong enough to practice nonviolence. 

Lena returns home, destroys her room, and goes to the car showroom where Bill works to tell him she has scabies. They are treated at a clinic, and then go their separate ways. As the embedded story of Lena and Bill begins to resolve, the film crew and director Sjöman are featured more. The relationship between Lena the actress and Bill the actor has become intimate during the production of Vilgots film, and Vilgot is jealous and clashes with Bill. The film concludes with Lena returning Vilgots keys as he meets with another young female theater student. 

The movie also includes an interview with Martin Luther King Jr., who happened to be visiting Stockholm when the film was being made. In addition to the footage of King, the film also includes an interview with Minister of Transportation Olof Palme, who talks about the existence of class structure in Swedish society, and footage of Russian poet Yevgeny Yevtushenko.

== Cast ==

 
* Vilgot Sjöman as Vilgot Sjöman
* Lena Nyman as Lena
* Börje Ahlstedt as Börje Peter Lindgren as Lenas father
* Chris Wahlström as Runes woman
* Marie Göranzon as Marie
* Magnus Nilsson as Magnus
* Ulla Lyttkens as Ulla
* Anders Ek as Exercise leader
* Martin Luther King Jr. as himself
* Olof Palme as himself
* Yevgeny Yevtushenko as himself

;Uncredited roles
* Holger Löwenadler as The King
* Bertil Norström as Factory worker
* Dora Söderberg Old lady in elevator
* Öllegård Wellton as Yevtushenkos Interpreter
* Sven Wollter as Captain
 

==Style==
  together with actress Lena Nyman.]] New Wave. Yellow was the only film of the wave of left wing films that made any money and the movement had little significance to the Swedish public. The interview with Martin Luther King was filmed in March 1966, when Dr. King and Harry Belafonte were in Stockholm to start a new initiative for Swedish support of African Americans. 

==Censorship==
The film includes numerous and frank scenes of nudity and staged sexual intercourse. One particularly controversial scene features Lena kissing her lovers flaccid human penis|penis. In 1969, the film was banned in Massachusetts for being Pornography|pornographic. After proceedings in the United States District Court for the District of Massachusetts (Karalexis v. Byrne, 306 F. Supp. 1363 (D. Mass. 1969)), the United States Court of Appeals for the Second Circuit, and the Supreme Court of the United States (Byrne v. Karalexis,   and  ), the Second Circuit found the film not to be obscene. 

==Release==
===Critical reception===
Initial reception to the films were hostile with Vincent Canby of The New York Times stating that, "Im not very fond of this sort of moviemaking, which tries to disarm conventional criticism by exploiting formlessness as meaningful itself." 

An arsonist torched the Heights Theater in Houston during the films run there. 

In 1969, Boston police seized the film reels for I Am Curious (Yellow) from the Symphony Cinemas after a Massachusetts Superior Court judge ruled the film obscene. The U.S. Supreme Court overturned the ban in 1971. 

===Box office===
The film was very popular at the box office, earning an estimated $6.6 million in North American rentals.  It was the twelfth most popular film in the US in 1969. 

One of the main reasons that it was a box office smash was that it was the first film with sexual acts performed onscreen that was not confined to the porn theaters on 42nd Street, New York City.  Millions of people who had never seen a pornographic film flocked to safe neighborhood theaters to see what it was all about. Another reason was that it became popular among "stars" to be seen going to this movie, and that made the general public even more interested. News of Johnny Carson seeing the film legitimized going to see it.  The film ushered in a wave of nudity and sex never before shown to the general public, and it was the first shot in the war that was to begin in pushing the limits of films for the general public. 

===Accolades===
  (who played himself in an uncredited role in the movie) and Lena Nyman, taken at the Guldbagge Award ceremony. Nyman won the 1967 award for Best Actress in a leading role.]] Best Actress at the 5th Guldbagge Awards for her role in this film and I Am Curious (Blue).   

==In popular culture==
* The Get Smart episode "I Am Curiously Yellow", which was the series finale
* The Simpsons episode "I Am Furious (Yellow)"
* The Supermans Girl Friend, Lois Lane issue "I Am Curious (Black)" wherein Lois Lane becomes a Black woman for a day to experience racism.
* The That Girl episode "I Am Curious Lemon" The Falls 1988 album Lee and The Curious Orange", portrayed by Paul Putner)
* In Mad Men (season 7)|Mad Men season 7, episode 6 ("The Strategy"), Don Draper references having just seen this movie in a theater. 
* In "Death in the Family", an episode of the BBC comedy series Steptoe and Son, Harold raises his fathers spirits after the death of their horse by promising to take him to see I Am Curious (Yellow).

==References==
 

==External links==
*  
*    
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 