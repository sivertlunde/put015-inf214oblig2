Amityville 4: The Evil Escapes
 
{{Infobox film
| name           = Amityville 4: The Evil Escapes
| image          = Amityville IV.jpg
| caption        =
| director       = Sandor Stern Steve White, Barry Bernardi, Kenneth Atchity  (NBC) 
| writer         = John G. Jones  (novel) , Sandor Stern
| starring       = Patty Duke, Jane Wyatt, Fredric Lehne
| music          = Rick Conrad Tom Richmond
| editing        = Skip Schoolnik National Broadcasting Company (NBC)  (TV) 
| released       = May 12, 1989
| runtime        = 95 min.
| country        = United States
| language       = English
}}

Amityville 4: The Evil Escapes (or simply Amityville: The Evil Escapes; the onscreen title is Amityville Horror: The Evil Escapes) was released in 1989 as a television film by   was also going to air on NBC but the film was never made.

==Summary== Amityville Horror Norman Lloyd) floor lamp. As he begins to chant, a burst of energy emerges from the outlet, through the cord, and into the lamp. A demonic face appears in the large round bulb. Kibbler is knocked across the room and is unconscious.

A few days later, the real estate agency decides to have a yard sale by selling the items left in the house by the previous owners. Father Manfred believes that the evil spirits are finally gone from the house. Meanwhile, at the yard sale, a woman named Helen Royce (Peggy McCay) and her friend Rhona (Gloria Cromwell) are looking through the items when Helen finds the lamp. At only $100.00, Helen decides to buy the lamp. She even decides to send it to her sister as a birthday present, explaining that she and her sister send each other rather ugly gifts as a long-running joke. While checking the lamp, Helen cuts her finger on a brass collar around the bulb. Ignoring the cut on her finger, Helen buys the lamp. As the day goes on, Helens finger begins to get infected and discolored.

One week later, the lamp arrives at Helens sister, Alice Leacocks (Jane Wyatt), house, a large, three story home over a beach in a small town called Dancott, California. That day, Alices daughter, Nancy Evans (Patty Duke) and her three children Amanda (Zoe Trilling), Brian (Aron Eisenberg), and the youngest child, quiet, mysterious Jessica (Brandy Gold) move in with Alice. Once they arrive, Alice decides to open the package containing the lamp. Nancy thinks the lamp is hideous, while Alice finds it to be interesting. Once the lamp is turned on, Alices parrot, Fred, begins to act crazy, and her cat, Pepper, scratches Amanda. While the rest of the family pays little to no attention to the lamp, Jessica seems to be drawn towards it.

==Cast==
* Patty Duke as Nancy Evans
* Jane Wyatt as Alice Leacock
* Fredric Lehne as Father Kibbler
* Lou Hancock as Peggy
* Brandy Gold as Jessica Evans
* Zoe Trilling as Amanda Evans (as Geri Betzler)
* Aron Eisenberg as Brian Evans Norman Lloyd as Father Manfred
* Robert Alan Browne as Donald McTear
* Gloria Cromwell as Rhoda
* James Stern as Danny Read
* Peggy McCay as Helen Royce

==Continuity==
Despite the destruction of the house in the conclusion of the previous film it manages to make an appearance in this film. The films after 3-D usually do not pick up where the previous film left off, so no explanation has ever been given as to how the house is still standing. It is possible this film may take place before Amityville 3-D due to the house already being empty in that film, and after the yard sale in this one. The furniture that was left in the house in the beginning of the film is believed to be owned by the Lutz family in the first movie. The film also makes a reference to  .

==Reception==
On the  ". 

==Filming locations==
The replica of the original Amityville house was located at 402 East M St., Wilmington, Los Angeles|Wilmington, California. A facade was added to the side of the house to give it the Amityville appearance. The interiors of this house were filmed also.

The exteriors for Alice Leacocks house were filmed at the James Sharp House, 11840 W. Telegraph Rd, Santa Paula, California. The house is an historical Italian Villa style home built in 1890. In reality, it does not back out onto an oceanside cliff as depicted in the movie. Instead, it backs out onto the Santa Paula Freeway, about a quarter of a mile behind the house. For a second, you can see the tower of the house in the distance while driving on the Freeway.

The interiors of the grandmothers house were filmed at the Woodbury-Story House in Altadena, California.
 John Marshall Los Angeles, California was also used.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 