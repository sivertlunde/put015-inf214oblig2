Sweeney Todd: The Demon Barber of Fleet Street (2007 film)
 
 
{{Infobox film
| name = Sweeney Todd: The Demon Barber of Fleet Street
| image = Sweeneylarge.jpg
| image_size = 220px
| border = yes
| alt = 
| caption = Theatrical release poster
| director = Tim Burton John Logan Richard D. Zanuck
| screenplay = John Logan
| based on =  &nbsp;by    
| starring = Johnny Depp Helena Bonham Carter Alan Rickman Timothy Spall Sacha Baron Cohen
| music = Stephen Sondheim
| cinematography = Dariusz Wolski
| editing = Chris Lebenzon The Zanuck Company Neal Street Productions  Warner Bros. Pictures  
| released =  
| runtime = 116 minutes  
| country = United Kingdom United States

| language = English
| budget = $50 million  
| gross = $152.5 million 
}} Victorian melodramatic English barber and serial killer who murders his customers with a straight razor and, with the help of his accomplice, Mrs. Lovett, processes their corpses into meat pies.

Having been struck by the cinematic qualities of Sondheims musical while still a student, Burton had entertained the notion of a film version since the early 1980s. However, it was not until 2006 that he had the opportunity to realize this ambition, when DreamWorks announced his appointment as replacement for director Sam Mendes, who had been working on such an adaptation. Sondheim, although not directly involved, was extensively consulted during the films production.
 Music section below).
 Best Actor. Although not an outstanding financial success in the U.S., it performed well worldwide, and has spawned a soundtrack album and various DVD releases.

==Plot== Benjamin Barker Lucy ("The Barber and His Wife"). Barker adopts the alias "Sweeney Todd" and returns to his old Fleet Street shop, situated above Mrs. Lovett|Mrs. Nellie Lovetts meat pie shop ("The Worst Pies in London"). From her, he learns that Turpin raped Lucy, who then poisoned herself with arsenic. The couples daughter, Johanna (character)|Johanna, is now Turpins Ward (law)|ward, and, like her mother before her, the object of Turpins lust ("Poor Thing"). Todd vows revenge, and re-opens his barber shop after Mrs. Lovett returns his straight razors to him ("My Friends"). While roaming London, Anthony becomes enamored of Johanna, but is caught by Turpin and driven away by his corrupt associate, Beadle Bamford ("Green Finch and Linnet Bird", "Alms! Alms!", "Johanna").
 Italian barber Adolfo Pirellis hair tonic as a fraud and humiliates him in a public shaving contest ("Pirellis Miracle Elixir", "The Contest"). A few days later, Pirelli arrives at Todds shop, with his boy assistant Tobias Ragg. Mrs. Lovett keeps Toby occupied while Pirelli identifies himself as Todds former assistant, Davy Collins, and threatens to reveal Todds secret unless Todd gives him half his earnings. Todd kills Collins to protect his secret, and hides his body in a trunk.

After receiving some advice from Bamford, Turpin, intending marriage to Johanna, visits Todds shop for grooming ("Ladies in Their Sensitivities"). Todd shaves Turpin, preparing to slit his throat; they are interrupted by Anthony, who quickly reveals his plan to elope with Johanna before noticing Turpin ("Pretty Women"). Turpin leaves enraged, vowing never to return. Infuriated at having missed his chance, Todd vents his rage by killing his customers while waiting for another chance to kill Turpin, and Mrs. Lovett bakes his victims into pies ("Epiphany"). Todd rigs his barbers chair with a pedal-operated mechanism that deposits his victims through a trap door into Mrs. Lovetts basement bake-house. Meanwhile, Anthony searches for Johanna, whom Turpin has sent to an insane asylum upon discovering her plans to elope with Anthony ("Johanna  ").

The barbering and pie-making businesses prosper, and Mrs. Lovett takes Toby as her assistant ("God, Thats Good"). Elated by success, Mrs. Lovett tells an uninterested Todd of her plans to marry him and move to the seaside ("By the Sea"). Anthony discovers Johannas whereabouts and, at Todds suggestion, poses as a wig-makers apprentice to rescue her. Meanwhile, Todd has Toby deliver a letter to Turpin, telling him where Johanna will be brought when Anthony frees her. Toby has become wary of Todd and tells Mrs. Lovett of his suspicion ("Not While Im Around").

Bamford arrives at the pie shop, informing Mrs. Lovett that neighbors have been complaining of the stink from her chimney. He is distracted by Todds offer of a free grooming and is murdered by Todd. Mrs. Lovett informs Todd of Tobys suspicions, and the pair search for Toby, whom Mrs. Lovett has locked in the bake-house. He has hidden himself in the sewers after seeing Bamfords body drop into the room from the trap door above, as well as finding a human toe in a pie. Anthony brings Johanna, disguised as a sailor, to the shop, and has her wait there while he leaves to find a coach.

A beggar woman enters the shop in search of Bamford. She recognizes Todd when he enters, but upon hearing Turpins voice, Todd kills her and sends her through the trap door. As Turpin enters, Todd explains to him that Johanna had repented and offers a free shave; when Turpin finally recognizes Todd as Benjamin Barker, Todd stabs him several times before cutting his throat. Upon seeing Johanna, Todd prepares to slit her throat as well, not recognizing her as his daughter. Hearing Mrs. Lovett scream in horror as a dying Turpin grabs her dress, Todd instead tells Johanna "Forget my face", and spares her life.

Todd discovers that the beggar woman Mrs. Lovett hurriedly tries to dispose of is his wife Lucy, whom he had believed to be dead, and that Mrs. Lovett misled him about her death. Todd pretends to forgive her and dances with her before hurling her into the bake-house oven, then cradles his wifes dead body in his arms, singing bitterly to himself. Unseen by Todd, Toby climbs from the sewers and slits Todds throat with his own razor. He then leaves the basement as Todd bleeds to death ("Final Scene").

==Cast== Benjamin Barker / Sweeney Todd
* Helena Bonham Carter as Mrs. Lovett
* Alan Rickman as Judge Turpin
* Timothy Spall as Beadle Bamford Johanna Barker
* Sacha Baron Cohen as Adolfo Pirelli Lucy Barker / Beggar Woman
* Jamie Campbell Bower as Anthony Hope Ed Sanders as Tobias Ragg

==Production==

===Development=== CalArts student directing career took off in the late 1980s, Burton approached Sondheim with a view to making a cinematic adaptation, but nothing came of it. In Sondheims words, "  went off and did other things." 

Meanwhile, director  ) had previously collaborated with Parkes on Gladiator (2000 film)|Gladiator, and claimed his biggest challenge in adapting the Sondheim stage play "was taking a sprawling, magnificent Broadway musical and making it cinematic, and an emotionally honest film. Onstage, you can have a chorus sing as the people of London, but I think that would be alienating in a movie."    Mendes left to direct the 2005 film Jarhead (film)|Jarhead, and Burton leaped at taking over the direction after his project, Ripleys Believe It or Not!, fell apart due to its excessive budget.  
 Amicus movies".  Turning a three-hour stage musical into a two-hour film required some changes. Some songs were shortened, while others were completely removed.    Burton said "In terms of the show, it was three hours long, but we werent out to film the Broadway show, we were out to make a movie, so we tried to keep the pace like those old melodramas. Sondheim himself is not a real big fan of movie musicals, so he was really open to honing it down to a more pacey shape.""  Burton and Logan also reduced the prominence of other secondary elements, such as the romance between Todds daughter Johanna and Anthony, to allow them to focus on the triangular relationship between Todd, Mrs. Lovett, and Toby.  

===Casting===
DreamWorks announced Burtons appointment in August 2006, and Johnny Depp was cast as Todd.  Christopher Lee, Peter Bowles, Anthony Head, and five other actors were set to play the ghost narrators, but their roles were cut. According to Lee, these deletions were due to time constraints caused by a break in filming during March 2007, while Depps daughter recovered from an illness.  Burtons domestic partner Helena Bonham Carter was cast in October 2006, as well as Sacha Baron Cohen.   In December of 2006, Alan Rickman was cast.  In January of 2007, Laura Michelle Kelly was cast as Lucy Barker.  Timothy Spall was added to the cast, and said he was urged to audition by his daughter, who wanted him to work with Depp. He recalled, "I really wanted this one – I knew Tim was directing and that Johnny Depp was going to be in it. My daughter, my youngest daughter, really wanted me to do it for that reason – Johnny Depp was in it. (She came on set to meet Depp) and he was really delightful to her, she had a great time. Then, I took her to the junket – and (Depp) greeted her like an old pal when he saw her. Ive got plenty of brownie points at the moment." 
 Ed Sanders was cast as Toby, Jayne Wisener as Johanna, and Jamie Campbell Bower, who auditioned, and after four days got the part of Anthony said "I think I weed myself. I was out shopping at the time and I got this call on my mobile. I was just like, OH MY GOD! Honestly, I was like a little girl running around this shop like oh-my-god-oh-my-god-oh-my-god." 

===Filming=== Filming began green screen, but decided against it, stating that physical sets helped actors get into a musical frame of mind: "Just having people singing in front of a green screen seemed more disconnected". 

Depp created his own image of Todd. Heavy purple and brown make-up was applied around his eyes to suggest fatigue and rage, as if "hes never slept".    Burton said of the character Sweeney Todd, "We always saw him as a sad character, not a tragic villain or anything. Hes basically a dead person when you meet him; the only thing thats keeping him going is the one single minded thing which is tragic. You dont see anything else around him."    Depp said of the character, "He makes Sid Vicious look like the innocent paper boy. Hes beyond dark. Hes already dead. Hes been dead for years."  Depp also commented on the streak of white in Todds hair, saying, "The idea was that hed had this hideous trauma, from being sent away, locked away. That streak of white hair became the shock of that rage. It represented his rage over what had happened. Its certainly not the first time anyones used it. But its effective. It tells a story all by itself. My brother had a white spot growing up, and his son has this kind of shock of white in his hair." 
 desaturated color bin liners Paramount had slasher movie categories."   

After the filming, Burton said of the cast, "All I can say is this is one of the best casts Ive ever worked with. These people are not professional singers, so to do a musical like this which I think is one of the most difficult musicals, they all went for it. Every day on the set was a very, very special thing for me. Hearing all these guys sing, I dont know if I can ever have an experience like that again.""  Burton said of the singing, "You cant just lip synch, youd see the throat and the breath, every take they all had to belt it out. It was very enjoyable for me to see, with music on the set everybody just moved differently. Id seen Johnny (Depp) act in a way Id never seen before, walking across the room or sitting in the chair, picking up a razor or making a pie, whatever. They all did it in a way that you could sense." 

Depp said of working with Baron Cohen, when asked what he was like in real life (meaning, not doing one of his trademark characters), "Hes not what I expected. I didnt look at those characters and think, This will be the sweetest guy in the world. Hes incredibly nice. A real gentleman, kind of elegant. I was impressed with him. Hes kind of todays equivalent of Peter Sellers." 

==Music==
 
Burton wanted to avoid the traditional approach of patches of dialogue interrupted by song, "We didnt want it to be what Id say was a traditional musical with a lot of dialogue and then singing. Thats why we cut out a lot of choruses and extras singing and dancing down the street. Each of the characters, because a lot of them are repressed and have their emotions inside, the music was a way to let them express their feelings." 

He cut the shows famous opening number, "The Ballad of Sweeney Todd", explaining, "Why have a chorus singing about attending the tale of Sweeney Todd when you could just go ahead and attend it?" Sondheim acknowledged that, in adapting a musical to film, the plot has to be kept moving, and was sent MP3 files of his shortened songs by Mike Higham, the films music producer, for approval. Several other songs were also cut, and Sondheim noted that there were "many changes, additions and deletions...  ... if you just go along with it, I think youll have a spectacular time."  To create a larger, more cinematic feel, the score was re-orchestrated by the stage musicals original orchestrator, Jonathan Tunick, who increased the orchestra from 27 musicians to 78. 
 New York pitch is dead-on accurate... Beyond his good pitch and Phrase (music)|phrasing, the expressive colorings of his singing are crucial to the portrayal. Beneath this Sweeney’s vacant, sullen exterior is a man consumed with a murderous rage that threatens to burst forth every time he slowly takes a breath and is poised to speak. Yet when he sings, his voice crackles and breaks with sadness." 

==Marketing== Advertising Standards Authority and Trading Standards agency.   The studios involved opted for a low-key approach to their marketing. Producer Walter Parkes stated, "All these things that could be described as difficulties could also be the movies greatest strengths." Warner Bros. felt it should take a similar approach to marketing as with The Departed, with little early exposure and discouraging talk of awards. 

==Release== Marcus Theaters Corporation was not initially planning to screen the film following its premiere, because it was unable to reach a pricing agreement with Paramount.  However, the dispute was resolved in time for the official release. 

===Critical reception===
Although Sondheim was cautious of a cinematic adaptation of his musical, he was largely impressed by the results.  The film received critical acclaim, with Depps performance receiving acclaim from critics — the review aggregator Rotten Tomatoes reports that 86% of critics gave the film positive reviews, based on 221 reviews,  and Metacritic gave the film an average score of 83 out of 100, based on 39 reviews.  Sweeney Todd appeared on many critics top ten lists of the best films of 2007. 

Of the reviewers, Time (magazine)|Time rated it an A-minus and added, "Burton and Depp infuse the brilliant cold steel of Stephen Sondheims score with a burning passion. Helena Bonham Carter and a superb supporting cast bring focused fury to this musical nightmare. Its bloody great." Time s  Richard Corliss named the film one of its top ten movies of 2007, placing it fifth. {{Cite news| last = Corliss| first = Richard| author-link = | title = The 10 Best Movies| newspaper = Time magazine| page = 40| year =
| date = 2007-12-24| url =| postscript =   }}  Roger Ebert of the Chicago Sun-Times gave it four stars out of four, lauding Burtons visual style.  In his review in Variety (magazine)|Variety, Todd McCarthy called it "both sharp and fleet" and "a satisfying screen version of Stephen Sondheims landmark 1979 theatrical musical ... things have turned out uniformly right thanks to highly focused direction by Tim Burton, expert screw-tightening by scenarist John Logan, and haunted and musically adept lead performances from Johnny Depp and Helena Bonham Carter. Assembled artistic combo assures the film will reap by far the biggest audience to see a pure Sondheim musical, although just how big depends on the upscale crowd’s tolerance for buckets of blood, and the degree to which the masses stay away due to the whiff of the highbrow."  Lisa Schwarzbaum of Entertainment Weekly gave the film a B-plus in its Movie Reviews section and stated, "To stage a proper Sweeney Todd, necks must be slit, human flesh must be squished into pastries, and blood ought to spurt in fountains and rivers of death. Enter Tim Burton, who&nbsp;... has tenderly art-directed soup-thick, tomato-red, fake-gore blood with the zest of a Hollywood-funded Jackson Pollock." She went on to refer to the piece as "opulent, attentive&nbsp;... so finely minced a mixture of Sondheims original melodrama and Burtons signature spicing that its difficult to think of any other filmmaker so naturally suited for the job." 

In its DVD Reviews section, EW  s Chris Nashawaty gave the film an A-minus, stating, "Depps soaring voice makes you wonder what other tricks hes been hiding... Watching Depps barber wield his razors... its hard not to be reminded of   said, "The blood juxtaposed to the music is highly unsettling. It runs contrary to expectations. Burton pushes this gore into his audiences faces so as to feel the madness and the destructive fury of Sweeneys obsession. Teaming with Depp, his long-time alter ego, Burton makes Sweeney a smoldering dark pit of fury and hate that consumes itself. With his sturdy acting and surprisingly good voice, Depp is a Sweeney Todd for the ages."  Harry Knowles gave the film a highly positive review, calling it Burtons best film since Ed Wood, his favorite Burton film, and said it was possibly superior. He praised all of the cast and the cinematography, but noted it would probably not appeal to non-musical fans due to the dominance of music in the film. 

==Awards and nominations== Spike TVs 2008 Scream Awards (filmed on October 18, 2008, and aired three days later), the film won two awards: Best Horror Movie, and Best Actor in a Horror Movie or TV Show (Depp). 

{| class="wikitable" style="width:100%;"
|-
! style="width:5%;"| Year
! style="width:25%;"| Award
! style="width:35%;"| Category
! style="width:25%;"| Nominee
! style="width:10%;"| Result
|-
| style="text-align:center;"| 2007 London Film Critics Circle Award
| British Actress of the Year
| Helena Bonham Carter
|  
|-
| rowspan="36" style="text-align:center;"| 2008
| rowspan="3"| Academy Award Best Performance by an Actor in a Leading Role
| Johnny Depp
|  
|- Best Costume Design
| Colleen Atwood
|  
|- Best Art Direction
| Dante Ferretti and Francesca Lo Schiavo
|  
|-
| Evening Standard British Film Award
| Best Actress
| Helena Bonham Carter
|  
|-
| American Cinema Editors
| colspan="2"| Best Edited Feature Film - Comedy or Musical
|  
|-
| rowspan="2"| British Academy Film Award Best Costume Design
| Colleen Atwood
|  
|- Best Makeup and Hair
| Ivana Primorac
|  
|-
| rowspan="5"| Broadcast Film Critics Association Award Best Film
|  
|- Best Cast
|  
|- Best Actor
| Johnny Depp
|  
|- Best Young Performer Ed Sanders
|  
|- Best Director
| Tim Burton
|  
|-
| rowspan="4"| Golden Globe Award Best Motion Picture - Musical or Comedy
|  
|- Best Actor - Motion Picture Musical or Comedy
| Johnny Depp
|  
|- Best Actress - Motion Picture Musical or Comedy
| Helena Bonham Carter
|  
|- Best Director
| Tim Burton
|  
|-
| rowspan="5"| Italian Online Movie Award
| Best Actor in a Leading Role
| Johnny Depp
|  
|-
| Best Actress in a Supporting Role
| Helena Bonham Carter
|  
|-
| Best Art Direction
| Dante Ferretti and Francesca Lo Schiavo
|  
|-
| Best Costume Design
| Colleen Atwood
|  
|-
| Best Make-up
| Ivana Primorac
|  
|-
| MTV Movie Award
| Best Villain
| Johnny Depp
|  
|-
| rowspan="3"| National Movie Award
| colspan="2"| Best Musical
|  
|-
| Best Performance (Male)
| Johnny Depp
|  
|-
| Best Performance (Female)
| Helena Bonham Carter
|  
|-
| rowspan="8"| Saturn Award Best Horror Film
|  
|- Best Actor
| Johnny Depp
|  
|- Best Actress
| Helena Bonham Carter
|  
|- Best Supporting Actor
| Alan Rickman
|  
|- Best Director
| Tim Burton
|  
|- Best Costume
| Colleen Atwood
|  
|- Best Make-up
| Peter Owen and Ivana Primorac
|  
|- Best Writing John Logan
|  
|-
| rowspan="3"| Scream Award
| Best Horror Actor
| Johnny Depp
|  
|-
| Best Horror Actress
| Helena Bonham Carter
|  
|-
| Best Director
| Tim Burton
|  
|-
|
| Teen Choice Award
| Choice Movie Villain
| Johnny Depp
|  
|-
| rowspan="4" style="text-align:center;"| 2009
| rowspan="4"| Empire Award Best Horror
|  
|- Best Actor
| Johnny Depp
|  
|- Best Actress
| Helena Bonham Carter
|  
|- Best Director
| Tim Burton
|  
|}

==Home release==
Sweeney Todd: The Demon Barber of Fleet Street was released on DVD in North America on April 1, 2008, and the UK on May 19. A Blu-ray Disc|Blu-ray was released on October 21, 2008.  An HD DVD release was announced for the same date, but due to the discontinuation of the format, Paramount canceled this version in preference for international distribution of the Blu-ray release. 

The DVD was released on April 1, 2008 and has thus far sold approximately 1,892,489 units, bringing in more than $38 million in revenue. 

Sweeney Todd officially premiered on television on BBC America in October 2010. An edited version of the film appeared as part of "Shocktober", a Halloween themed month event for This TV network in October 2013.

The film premiered on British television on Channel 4 on 27 November 2010.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 