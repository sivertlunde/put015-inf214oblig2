Down to the Dirt
 
 
{{Infobox film
| name           = Down to the Dirt
| image          = DowntotheDirt20082c150.jpg
| caption        = Film poster
| director       = Justin Simms
| producer       =
| writer         = Joel Hynes Justin Simms Sherry White Mary Lewis
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    = Mongrel Media
| released       =  
| runtime        = 115 minutes
| country        = Canada
| language       = English
}} Newfoundland author Joel Hynes first novel of the same name. The movie has been shot largely in Newfoundland. The film won two awards at the Atlantic Film Festival, one for the best feature film and other for the best screenplay.

== Plot ==
Keith Kavanagh is a rowdy hooligan known for his hard drinking throughout the Southern shore. He also has a shattered relationship with his father.  But when he meets Natasha (Mylène Savoie), his life changes. Both fall in love. But soon Natasha leaves Keith, fed up of his ways. Later Keith realizes that he himself is responsible for all his failures in life and thus embarks on a mission to find Natasha, where he not only finds her, but also himself and realizes he needs to make a change for the better.

== Release ==
Mongrel Media provided Canadian distribution for the film.   14A rating - coarse language, sexual content, violence.  Running time is 115–116 minutes.   The public debut was at the Toronto International Film Festival on 9 September 2008. 

== Reception == Sun Medias Liz Braun had a more favourable assessment ( ) describing the production as "uneven" yet possessing "a strange appeal, like some kind of Newfie version of a Charles Bukowski story".  Film review aggregator Rotten Tomatoes lists the feature at a 50% approval level on its tomatometer with an average 5.4/10 rating. 

Film classification boards in Ontario and Quebec issued content advisories regarding the productions strong language and violence.    13+ rating - vulgar language (langage vulgaire), violence. 

== Awards ==
Down to the Dirt received two awards at the Atlantic Film Festival.

* Justin Simms won the Atlantic Canadian Award for the best feature film
* Justin Simms and Sherry White won the Atlantic Canadian Award for the best original screenplay

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 