Gagana (film)
{{Infobox film
| name           = Gagana
| image          =
| image_size     =
| caption        =
| director       = Dhorairaj Bhagavan
| producer       = G.R. Krishnan
| writer         =
| narrator       = Leelavathi
| music          = Rajan Nagendra, Udayashankar
| cinematography =
| editing        =
| distributor    =
| released       = 1989
| runtime        =
| country        =   India
| language       = Kannada
| budget         = 32 lakhs (disputed)
| preceded_by    =
| followed_by    =
| website        =
}}

Gagana ( , meaning Sky) is a 1989 Kannada movie starring Anant Nag and Kushboo Sundar|Kushboo.

==Plot==
This is the love story of a textiles manufacturer owner, Raju(Anant Nag), and his secretary, Asha/Gagana(Kushboo Sundar). It was originally billed as a romantic comedy.

==Cast==
* Anant Nag Kushboo
* Leelavathi
* Mahalakshmi

==Crew==
* Music - Rajan-Nagendra, Udayashankar
* Producer - G.R. Krishnan
* Director - Dhorairaj Bhagavan

==Soundtrack==
{| class="wikitable"
|-
!Track #
!Song
!Performed by
|- 1
|Yenaithu Nanna Nalla
|K.S. Chitra
|- 2
|Ellu Kaanenu Nanintha Hennanu
|S.P. Balasubrahmanyam, K.S. Chitra
|- 3
|Baagilu Teredhu Kayya Hididhu
|K.S. Chitra
|- 4
|Naanu Happy Neenu Happy
|S.P. Balasubrahmanyam, K.S. Chitra
|-
|}

==External links==
* 
* 

 
 
 
 
 


 