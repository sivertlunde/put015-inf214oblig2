Kalyana Parisu
 
{{Infobox film
| name           = Kalyana Parisu
| image          = Kalyana Parisu poster.jpg
| image_size     =
| caption        = Theatrical poster
| director       = C. V. Sridhar
| producer       = S. Krishnamoorthy T. Govindarajan C. V. Sridhar
| writer         = C. V. Sridhar
| narrator       =
| starring       = Gemini Ganesan B. Saroja Devi C. R. Vijaykumari K. A. Thangavelu M. Saroja
| music          = A. M. Rajah
| cinematography = A. Vincent
| editing        = N. M. Shankar
| studio         = Venus Pictures
| distributor    = Venus Pictures
| released       = 9 April 1959 
| runtime        = 167 minutes
| country        = India
| language       = Tamil
| budget         =
}}
 Indian Tamil Tamil romantic drama film produced and directed by C. V. Sridhar in his debut. The film stars Gemini Ganesan, B. Saroja Devi and C. R. Vijaykumari in the lead roles, while K. A. Thangavelu, M. Saroja, Akkineni Nageshwara Rao and M. N. Nambiar play supporting roles. A triangular love story, the film is about a young man who rents a house near his girlfriend, but the girls elder sister is not aware of their romance. She subsequently falls for the man and wishes to marry him, so her younger sister decides to sacrifice her love, unknown to the former.
 Telugu remake Hindi as Nazrana (1961 film)|Nazrana (1961) which starred Raj Kapoor and Vyjayanthimala in the lead, along with Gemini Ganesan in a different role. A second Telugu remake titled Devatha was released in 1982, and a second Hindi remake in Hindi titled Tohfa in 1984; both directed by K. Raghavendra Rao.

==Plot==
Baskar (Gemini Ganesan) and Vasanthi (Saroja Devi) are college mates. A naughty girl creates misunderstanding between them, and an enraged Vasanthi complains to the college dean, who immediately dismisses Baskar. Now left homeless, Baskar however manages to get employed at a Tea Company, and gets to stay with his close friend Sampath (K. A. Thangavelu). Vasanthi approaches Baskar and apologises for her cruel act. Baskar forgives her, and they both fall in love. Vasanthi lives with her mother and her elder sister Geetha (Vijaykumari) who is unmarried. Baskar rents the vacant portion of their house. Over time, Vasanthi passes her exams and gets employed.
 Nageshwara Rao) falls in love with her and expresses his desire to marry her, but she is unable to respond to his feelings.

Baskar is not able to lead a happy life in Coimbatore as he often thinks about his disappointment in love. He receives a letter from Vasanthi who advices him to forget about the past and lead a happy life with Geetha, and he relents to this. Shortly, Geetha becomes pregnant and returns to her original home where she delivers a son. Raju again meets Vasanthi and proposes to her, but she tells him her past history and expresses her inability to respond to his love. Heartbroken, Raju leaves her and resigns from his job. Vasanthi’s mother dies and Vasanthi joins Geetha at their house in Coimbatore. When Geetha falls sick, Vasanthi attends to all the household work. Baskar spends more time with Vasanthi than his own wife, and Geetha, suspecting them of being in a relation, abuses them both. Due to this, Vasanthi leaves them.

Geetha, having realised that Baskar and Vasanthi loved each other, dies in guilt, leaving Baskar alone to bring up their child, making him promise that he will find Vasanthi and make her the childs mother. He searches for Vasanthi all over the city, with no success. Meanwhile, Vasanthi meets with an accident, but is saved by a wealthy old man (M. N. Nambiar) who allows her to stay in his house. The mans son arrives and is revealed to be Raju, who Vasanthi agrees to marry. Through Sampath, Baskar learns about Vasanthis impending marriage, and rushes to the marriage hall with his son. However by the time they arrive, Vasanthi is already married. Baskar then hands over his child to her as a wedding gift, and walks away.

==Cast==
* Gemini Ganesan as Baskar
* B. Saroja Devi as Vasanthi
* C. R. Vijayakumari as Geetha
* K. A. Thangavelu as Sampath
* M. Saroja as Sampaths wife
* Akkineni Nageshwara Rao as Raju
* M. N. Nambiar as Rajus father

==Production==
The film marked the directorial debut for C. V. Sridhar, who was previously a partner in the production unit Venus Pictures, where he was an associate of S. Krishnamurthi and T. Govindarajan,     since 1956. In 1959, he created the story of Kalyana Parisu and narrated it to Krishnamurthy and Govindarajan, expressing his desire to direct the film. Though his partners liked the story, they were sceptical about his directorial capabilities and discouraged him. Sridhar requested them to give him a chance and offered to direct a few scenes for their approval. If they approved of his work, he would continue, otherwise some other director could be selected. He directed a few scenes, everyone was pleased with the outcome and he continued. 
 Nadodi Mannan. The role of the male lead was given to Gemini Ganesan, while K. A. Thangavelu and M. Saroja were signed for the comedy sequences.  Even though Ganesan had contracted typhoid, Sridhar felt that only he could do "justice to the role" and had to wait for him to get better before casting him. 

==Soundtrack==
The films soundtrack was composed by A. M. Rajah in his debut in Tamil cinema,  while the lyrics were penned by Pattukkottai Kalyanasundaram.  All the songs were acclaimed, and contributed to the films success.  The success of Kalyana Parisus songs made P. Susheela a leading female playback singer of Tamil cinema. 

The soundtrack received positive response. Tamil film historian S. Theodore Baskaran said, "Its songs were stupendous hits. It owed its phenomenal success in large part to its music composed by A.M. Raja, who was at his peak as a playback singer. the filmic convention of singing the same song twice, once in joy and once in sorrow, is followed. in fact, there are two such happy-sad songs."    V. Balasubramanian of The Hindu said, "A. M. Rajahs tryst with composing includes super duper hit songs from the films Kalyana Parisu, Aadi Perukku and Then Nilavu." 

;Tracklist 
{{Track listing
| collapsed       = 
| headline        = 
| extra_column    = Singer(s)
| total_length    =

| all_writing     = 
| all_lyrics      = 
| all_music       =

| writing_credits = 
| lyrics_credits  = 
| music_credits   =

| title1            = Vaadikkai Maranthathum
| note1           = 
| writer1         = 
| lyrics1          = Pattukkottai Kalyanasundaram
| music1        = 
| extra1          = A. M. Rajah, P. Susheela
| length1         = 4:24

| title2            = Aasaiyinaale Manam
| note2           = 
| writer2         = 
| lyrics2          = Pattukkottai Kalyanasundaram
| music2         = 
| extra2          = A. M. Rajah, P. Susheela
| length2         = 3:28

| title3            = Unnaikkandu Naan Vaada (Pathos)
| note3           = 
| writer3         = 
| lyrics3          = Pattukkottai Kalyanasundaram
| music3         = 
| extra3          = A. M. Rajah
| length3         = 4:13

| title4            = Thullatha Manamum
| note4           = 
| writer4         = 
| lyrics4           = Pattukkottai Kalyanasundaram
| music4          = 
| extra4           =  Jikki
| length4         = 3:57

| title5          = Kaathalile Tholviyutraal
| note5           = 
| writer5         = 
| lyrics5         = Pattukkottai Kalyanasundaram
| music5          = 
| extra5          = P. Susheela
| length5         = 1:46

| title6            = Mangayar Mugathil (Akkalukku)
| note6           = 
| writer6         = 
| lyrics6          = Pattukkottai Kalyanasundaram
| music6         = 
| extra6          = P. Susheela, K. Jamuna Rani, Chorus
| length6         =

| title7            = Kathalile Tholviyutraan
| note7           = 
| writer7         = 
| lyrics7          = Pattukkottai Kalyanasundaram
| music7          = 
| extra7          = P. Susheela
| length7         = 3:27

| title8             = Unnaikandu Nanada (Happy)
| note8           = 
| writer8         = 
| lyrics8          = Pattukkottai Kalyanasundaram
| music8         = 
| extra8           = P. Susheela
| length8         = 3:20
}}

==Release==
Kalyana Parisu turned out very profitable at the box office and "brought in a great deal of money".  It had a theatrical run of over 100 days,  and became one of C. V. Sridhars biggest box-office hits.   Kalyana Parisu, which created a major impact on the Tamil film industry, was the breakthrough for B. Saroja Devi and music director A. M. Rajah.  It is also considered the "most important" film for actress M. Saroja, who played K. A. Thangavelus wife in the film. On the films 100th day celebrations, both were married at a temple in Madurai. The duo went on to act in over 200 films together.   Actor Sivaji Ganesan, after seeing the preview of the film, highly appreciated it and predicted that it would become a box office hit. 
 Telugu and Kannada cinema Certificate of Merit for Best Feature Film in Tamil.   

===Critical response===
Kalyana Parisu received positive critical response. Ananda Vikatan (26 April 1959) said, "We are happy to see a good story in a Tamil film for the first time... The film does not have any villain and everyone has a good heart... In totality, the film deserves a prize for its story, a prize for its acting and a prize for its dialogues and that is Kalyana Parisu."    IndiaGlitz called it "immortal". 
 Thangavelu and M. Saroja|Saroja."  Malathi Rangarajan of The Hindu called the film " A love story with well etched characters", and praised the actors performances as "excellent".  In 2005, India Today called Ganesans performance as "memorable". 

==Legacy== Parthiban Kanavu same name Sun TV. 

==Remakes==
The success of Kalyana Parisu led to it being remade several times with different actors in the lead:
{| class="wikitable"
|-
! Kalyana Parisu (Tamil language|Tamil) (1959)
! Pelli Kanuka (Telugu language|Telugu) (1960)   
! Nazrana (1961 film)|Nazrana (Hindi) (1961) 
! Sammanam (1975 film)|Sammanam (Malayalam language|Malayalam) (1975) 
! Premanubandha (Kannada) (1981) 
! Devatha (Telugu language|Telugu) (1982) 
! Tohfa (Hindi) (1984) 
|-
| Baskar (Gemini Ganesan)
| Baskar (Akkineni Nageswara Rao)
| Rajesh (Raj Kapoor)
| (Prem Nazir)
| (Srinath)
| (Sobhan Babu)
| Ramu (Jeetendra)
|-
| Vasanthi (B. Saroja Devi)
| Vasanthi (B. Saroja Devi)
| Vasanthi (Vyjayanthimala)
| Jayabharathi
| Manjula
| (Sridevi)
| Lalita (Sridevi)
|-
| Geetha (Vijayakumari) Krishna Kumari)
| Geetha (Usha Kiran)
| Sujatha
| K. Vijaya
| (Jaya Prada)
| Janki (Jaya Prada)
|-
| Raghu (Akkineni Nageswara Rao)
| Raghu (Kongara Jaggayya)
| Shyam (Gemini Ganesan)
| Madhu
| (Dinesh)
| Ramudu (Mohan Babu)
| 
|}

==References==
 

==External links==
*  
*   at Upperstall.com

 
 

 
 
 
 
 
 
 
 
 