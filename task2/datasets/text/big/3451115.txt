You Nazty Spy!
{{Infobox Film
 | name           = You Nazty Spy!
 | image          = Naztyspy lobby.jpeg
 | caption        = 
 | director       = Jules White
 | writer         =  
 | starring       =  
 | producer       = Jules White
 | distributor    = Columbia Pictures
 | released       =  
 | runtime        = 17 59"
 | country        = United States
 | language       = English
}}
 

You Nazty Spy! is the 44th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot== wallpaper hangers simultaneously working in his dining room—the Stooges.
 leader (the Adolf Hitler role), with Curly as Field Marshal "Gallstone" (representing Hermann Göring while also mimicking Benito Mussolini), and Larry as Minister of Propaganda Pebble (a representation of Joseph Goebbels). After his takeover, Hailstone proceeds to give a speech to the masses, cueing Larry to display signs reading "CHEERS", "APPLAUSE" and, accidentally, "HISS".

However, the daughter (Lorna Gray) of the overthrown king pays Hailstone a visit, going by the name Mattie Herring (a spoof of World War I spy Mata Hari). The Stooges suspect she is a spy after and attempt a failed execution of her. 

Larry then cuts a round table while a dancer arrives and tells them the delegates have arrived for the round table meeting. The meeting goes wrong when Curly knocks the first two delegates unconscious. Later the kings daughter gathers a huge mob to storm Hailstones palace. The trio quickly abdicate, inadvertently flee into a lions den, and are eaten.

==Significance==
The film satirized the   film The Great Dictator, which began filming in September 1939.

The Hays code discouraged or prohibited many types of political and satirical messages in films, requiring that the history and prominent people of other countries must be portrayed "fairly". Short films such as those released by the Stooges were subject to less attention than feature films.

==Notes==
*The title is supposedly a parody of comedian Joe Penners catchphrase "You Nasty Man!" 
*Moe Howard, as "Moe Hailstone", became the first American actor to portray/imitate Adolf Hitler in a film.
*Both Moe Howard and Larry Fine cited You Nazty Spy! as their favorite Three Stooges short.   
*You Nazty Spy! was followed by a sequel, Ill Never Heil Again, in 1941 in film|1941.  Moronika would also appear in Dizzy Pilots
*There is a historical pun when Larry says, "If I take Mickey Finlen, I better be rushin." Curly replies, "Then quit stallin." This is a reference to Finland, the Soviet Union, and Joseph Stalin, who was the leader of the Soviet Union.
*Larry Fine injured his leg shortly before filming, and can be seen with a limp throughout the short. Fortunately, this was appropriate for his role as a parody of Joseph Goebbels, who walked with a limp due to a club foot.
*The names of the munitions manufacturers are Pig Latin for "Nix" (a slang term of that era), "No", and "Scram", which in turn were known by the audience as slang in their Pig-Latin form.
*The parody of the Nazi banner with two snakes in the form of a swastika is captioned with the phrase "Moronika for Morons" which is a play on the Nazi slogan "Deutschland den Deutschen" (Germany for Germans).
*The Stooges—all Ashkenazi Jews—occasionally worked a word or phrase of Yiddish into their dialogue. In particular here, the Stooges make several overt Jewish and Yiddish cultural references:
**The exclamation "Beblach!" used several times in the film is a Yiddish word meaning "beans".
**"Shalom aleichem!", literally "Peace unto you" is a standard Hebrew greeting meaning "hello, pleased to meet you".
** Moe: "Well start a Blintzkrieg (  especially with sour krieg." This is a reference to the Ashkenazi Jewish dish blintzes with sour cream.
**In Moes imitation of a Hitler speech, he says "in pupik gehabt haben" (the semi-obscene "Ive had it in the bellybutton" in Yiddish). These references to the Nazi leadership and Hitler speaking Yiddish were particularly ironic inside jokes for the Yiddish-speaking Jewish audience. 
**In addition to the "Mata Hari" reference, the name of the female spy Mati Herring is a play on the Yiddish and German name of soused herring, soused herring|matjeshering.
*When Mr. Ixnay informs the Stooges of how to overthrow Moronikas monarchy, and suggests that the takeover of Moronika start with a "putsch", it refers to the historical Beer Hall Putsch, the real-world Nazi party attempt at a power grab in the Weimar Republic of 1923. Curlys humorous response to Mr Ixnays suggestion, to explain it to Moe and Larry, was that "You putsch your beer down, and wait for the pretzels". bra size) and the unread "Tessie oomph 2 69 (sex position)|69" which were ignored by the Motion Picture Production Code|censors. This was a key dig at the attempt to censor The Great Dictator then in production by Charlie Chaplin. (Curly was also noted in his personal life for being a womanizer.)
*Curly Gallstone says to Mati Herring when he takes her out to shoot her "Lets go shoot the works." Hermann Göring was known to be a morphine addict; this was a slang allusion to the intravenous injection of morphine. colorized version of this film was released in 2004. It was part of the DVD collection entitled Stooged & Confoosed. 
*You Nazty Spy was also the first Stooges short to bear a new opening title sequence, with the "Torch Lady" on the left-hand corner, standing on a pedestal where each step has printed out "Columbia," "Short Subject" and "Presentation," and the opening titles and credits are inside a box with rounded edges.  This format will remain in effect through Booby Dupes. 

==References==
 

==External links==
*  
*  
*  at StoogeWorld.com
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 