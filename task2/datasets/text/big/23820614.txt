Trenchcoat in Paradise
 
 
 
}}

{{Infobox film
| name           = Trenchcoat in Paradise
| director       = Martha Coolidge
| writer         = Tom Dempsey
| starring       = Dirk Benedict Sydney Walshl Bruce Dern
| distributor    = Finnegan/Pinchuk Productions 
| released       = October 18, 1989
| runtime        = 100 min
| country        = United States English 
}}

==Plot==
The main character, Eddie Mazda (Benedict), is a hard nosed private investigator originally from Jersey City, New Jersey.  After working a job for a widow named Nan Thompson (played by Amy Yasbeck), he soon after is confronted by mob boss Dom Gellatti (played by Ralph Drischell), the man who killed Mrs. Thompsons husband.   Having already ransacked Mazdas film studio for any incriminating pictures against him for fear of federal prosecutions, Gellatti gives Eddie the chance to back down by forcing him to leave Jersey City and never come back... or else.  After gathering some of his possessions, calls and leaves a phone message to his ex-wife Vicky, and leaving his pet goldfish with the next-door neighbor and her cat, Mazda is escorted by two of Gellattis goons to the airport and given a plane ticket to Chicago and some money; instead, he decides to go to Hawaii, taking with him film negatives that he managed to hide from the mobsters.

 
 


 