Not like Others
{{Infobox film
| name           = Not Like Others
| director       = Peter Pontikis
| producer       = Patrick Sobieski
| screenplay     = Peter Pontikis
| starring       = Jenny Lampa Ruth Vega Fernandez David Dencik
| music          = Svante Fjaestad
| cinematography = Erik Persson
| editing        = Hanna Lejonqvist
| released       =  
| runtime        = 75 minutes
| country        = Sweden
| language       = Swedish
| studio         = Impasse Films Patrick Sobieski
| distributor    = NonStop Entertainment
| budget         = 
| gross          = 
}} Swedish vampire film|vampire-drama film written and directed by Peter Pontikis.
 Let The Right One In the second.

== Plot ==
Two vampire sisters, Vera (Jenny Lampa) and Vanja (Ruth Vega Fernandez), attend an illegal club. At the club, Vera is sexually harassed by a biker (Peter Järn). She drags him into the bathroom, pretending to accept having sex with him. Vera kills the biker with her pocket knife and drinks his blood. Vera and Vanja escapes the scene. The bikers gang gives them chase wanting revenge. 

The narration intercuts with events from previous nights. Vera and Vanja belong to a breed of vampires, who are human-like but can only survive on blood. Both of the sisters are homeless. Vanja plans to try to live with her secret human boyfriend and pass off as a regular human, having heard a rumor that other vampires have done this. Vanja tries to avoid killing humans and steals blood from hospitals to feed her cravings.

After having been found by the bikers Vera and Vanja escape through the Stockholm Ghost Park and are separated. Vera is picked up by a Taxi Driver, representing humanity (David Dencik). The driver seem friendly at first but when he finds out she does not have any money to pay him he asks for oral sex. Vera attacks and kills him.

Vera and Vanja are reunited at a midnight theatre showing Night of the Living Dead and Vanja tells Vera about her plan. Vera panics with the idea of not living with her sister and betrays their location to one of the bikers (Jörgen Persson) as he is talking to his friend (Omid Khansari). The bikers chase the sisters down to a warehouse. Vera has a change of heart and confronts the bikers as Vanja escapes. Vera admits to the bikers killing their friend and meets her end at their hands.

The film ends with Vanja meeting her boyfriend (Marcus Ovnell) at Stockholm Central Station.

== Cast ==
* Jenny Lampa ... Vera
* Ruth Vega Fernandez ... Vanja
* David Dencik ... Taxi Driver
* Peter Järn ... Biker 1
* Jörgen Persson ... Biker 2
* Omid Khansari ... Filmbuff
* Marcus Ovnell ... Vanjas Boyfriend
* Isabella Sobieski ... Nurse
* Björn Ekdahl ... DJ
* Ellen Fjæstad ... Party girl

David Dencik is the only human character to have any (audible) dialogue. According to director Portikis, he is supposed to represent humanity. Marcus Ovnell, who plays Vanjas human boyfriend, went on and married Jenny Lampa.

== Reception ==
The film met with mostly negative reviews.  Swedish critic and writer of the first Swedish vampire film (2006s Frostbite (2006 film)|Frostbite) Pidde Andersson has mentioned in several reviews and interviews that he views it as the worst vampire film of all times. 

==See also==
*Vampire film

== References ==
 

== External links ==
*  

 
 
 
 
 
 