Rhythm in the Clouds
{{Infobox film
| name           = Rhythm in the Clouds
| image          = Richard Carle in Rhythm in the Clouds.jpg
| image_size     = 150px
| caption        = Richard Carle in the film
| director       = John H. Auer
| producer       = Albert E. Levoy (producer)
| writer         = Ray Bond (story) Olive Cooper (screenplay) George Mence (story) Nathanael West (screenplay)
| narrator       =
| starring       =
| music          = Alberto Colombo Ernest Miller Edward Mann
| distributor    = Republic Pictures
| released       = 1937
| runtime        = 62 minutes 53 minutes (edited version)
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Rhythm in the Clouds is a 1937 American film directed by John H. Auer.

== Plot summary ==
A homeless and hungry music writer, Judy Walker, inhabits the apartment of music writer ... while he is away.

== Cast ==
*Patricia Ellis as Judy Walker
*Warren Hull as Bob McKay William Newell as Clyde Lyons
*Richard Carle as J.C. Boswell
*Zeffie Tilbury as Maggie Conway, the Duchess de Lovely
*Charles Judels as Luigi Fernando
*Robert Paige as Phil Hale
*Joyce Compton as Amy Lou
*Suzanne Kaaren as Dorothy Day
*Esther Howard as Mrs. Madigan
*Eddie Parker as Baxter
*James C. Morton as Cop
*Rolfe Sedan as Victor
*Richard Beach as Ben Graham
*Ranny Weeks as Radio Announcer

== Soundtrack ==
* Warren Hull - "Dont Ever Change" (Written by Walter Hirsch and Lou Handman)
* Suzanne Kaaren - "Hawaiian Hospitality" (Written by Harry Owens and Ray Kinney)
* Suzanne Kaaren and Warren Hull - "Two Hearts are Dancing" (Written by Walter Hirsch and Lou Handman)
* Hull and Patricia Ellis - "Two Hearts are Dancing"
* Patricia Ellis - "Mad Symphony"

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 

 