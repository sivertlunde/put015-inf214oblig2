The Mystery of the Hooded Horsemen
{{Infobox film
| name           = The Mystery of the Hooded Horsemen
| image          =DVD cover of the movie The Mystery of the Hooded Horsemen.jpg
| image_size     =
| caption        =DVD cover Ray Taylor
| producer       = Edward Finney (producer) Lindsley Parsons (supervising producer)
| writer         = Edmond Kelso (story) Edmond Kelso (screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Gus Peterson
| editing        = Frederick Bain
| distributor    = Grand National Pictures
| released       = 6 August 1937
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 Ray Taylor. It was singing cowboy Tex Ritters eighth film for Grand National Pictures.

==Plot==
Tex and Stubby keep their promise to a dying friend by helping his partner hang on to his gold mine and to avenge his death by taking on and wiping out a horde of masked riders.

== Cast ==
*Tex Ritter as Tex Martin
*White Flash as Texs horse
*Iris Meredith as Nancy Wilson
*Horace Murphy as Stubby Charles King as Blackie Devlin
*Earl Dwire as Sheriff Walker
*Forrest Taylor as Norton
*Joseph W. Girard as Dan Farley
*Lafe McKee as Tom Wilson
*Hank Worden as Deputy
*Ray Whitley as Band Leader
*The Range Ramblers as Saloon musicians

== Soundtrack == Fred Rose and Michael David)
*Tex Ritter - "Im a Texas Cowboy"
*Tex Ritter - "Ride Around Little Dogies"
*Tex Ritter - "Rosita"
*Tex Ritter - "ARidin Old Paint"

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 