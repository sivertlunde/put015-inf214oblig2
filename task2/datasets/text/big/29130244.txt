Makeup Man
{{Infobox film
| name           = Makeup Man
| image          = Makeupman.jpg
| caption        = Makeup Man theatrical poster Shafi
| producer       = Rejaputhra Renjith
| writer         = Sachi-Sethu Sheela Suraj Siddique Janardhanan Janardhanan
| Vidyasagar
| cinematography =
| editing        = 
| studio         = Rejaputhra Films
| distributor    = Rejaputhra Films
| released       =   
| runtime        = 150 minutes
| country        = India
| language       = Malayalam
| budget = 
| gross=
}} Siddique in Prithviraj appearing Vidyasagar composed the music, with lyrics penned by Kaithapram.The tumultuous life of a couple, after the wife becomes a heroine just by chance and the husband is forced to disguise himself as her makeup man, is being told here. 

==Story==
Balachandran (Jayaram) is arrested by police by mistaking him to be a house breaker, while entering his own home in middle of a night. He explains his pathetic financial situation to the police inspector (Shammi Thilakan), including his huge debts and love affair with Soorya (Sheela (Tamil actress)|Sheela). After seeing him reluctant to take up her calls, the police inspector advises him to approach life positively by accepting her into his life. But Balu explains him that its her marriage on the next morning and he is unable to bear the pain by talking to her. upon the request of the inspector, Balu takes up her call, and comes to know that she is now waiting for him at the railway station. Balu arrives at the railway station and meets her. With the help of Kitchu Manjaly (Suraj Venjarammoodu), a production controller in film industry, Balu marries Soorya at a temple in the same morning. Kitchu arranges a room for them to spend a night in the same hotel where a film crew was residing. Paul (Janardhanan (actor)|Janardhanan), the film producer mistakes her to be the new heroine for the film and Kitchu to play things safe makes him believe so. Paul calls up Sidharth (Siddique (actor)|Siddique), the film director and cameraman to conduct a screen test on the same night. Though reluctant, Soorya and Balu has no option left. Soorya appears for the screen test and is successfully selected as the new heroine. Balu plays it safe by claiming her personal make up man and appears on the set the next day. Soorya is re christianed as Anamika. during the shoot, Siddharth develops a soft feeling towards her, and finds Balus interference as a nuisance. He tries all ways to belittle him and asks Anamika to dump him away. Slowly, the fever of stardom catches up Anamika, who is also cast for the next film by Siddharth, which Balu objects. To make things worse, her father arrives on the set along with her fiancee to patch up things. Things get worsened between Balu and Anamika, and he quits from the set. Up on the release of the film, Anamika tries to patch up Balu, but he publicly humiliates her. Upon realizing his mistake, in a bid to sort out issues, he lands up in Hyderabad where Anamika is acting as the heroine of Prithviraj Sukumaran. There  Balu is the make up man of Chandra (Kamna Jethmalani), the second heroine. Sidharth is again disturbed by seeing him and tries all ways to kick him out, but all in vain. Prithviraj understanding Balus issues with Anamika from Kitchu decides to help him with the support of Chandra. After the pack up, on reaching back in Kochi, Balu is arrested by police for financial forgery. Siddharth, by realizing the relation between Balu and Soorya decides to play a dirty game to own her. He files a case on her behalf against her for which Balu files back another one. The legal fight and how Balu patches up with Soorya forms the rest of the story.

==Cast==
* Jayaram as Balu Sheela as Surya / Anamika(voice dubbed by sreeja) Siddique as Sidharth
* Suraj Venjaramoodu as Kitchu Janardhanan as Paul
* Kamna Jethmalani as Chandra 
* Jagadish as Advocate Mathai
* Salim Kumar as Lawrance
* Jagathi Sreekumar as Vareeth Mappilai
* Shammi Thilakan as Police Inspector
* Kunchacko Boban in guest appearance
* Prithviraj Sukumaran in guest appearance
* Babu Namboothiri as Sooryas Father
* Saiju Kuruppu as Sorryas fiance
* Appahaja as police inspector Devan as John Kuruvila Krishna Kumar as Adv. Krishna prasad

==Soundtrack==
{{Infobox album
| Name        = Makeup Man
| Type        = soundtrack Vidyasagar
| Cover       = 
| Released    = 
| Recorded    = 
| Genre       = Film soundtrack
| Length      = 
| Label       = 
| Producer    = Vishal-Shekhar| 
| Last album  = 
| This album  = Makeup Man (2011)
| Next album  = 
}}
 Vidyasagar in 2011. All lyrics were written by Kaithapram Damodaran Namboothiri.

{| class="wikitable"
|-
! Song !! Singer!! Length !! Picturization
|-
| Moolipaattum Paadi || Karthik, Kalyani Menon Jr || 4:24 || with Jayaram, Sheela, Prithviraj, Kamna
|-
| Aarutharum || Madhu Balakrishnan || 4:14 || with Kunchacko Boban, Sheela
|-
| Karimukil || Afsal ||  || 
|}

==References==
 

==External links==
* 
*  
*  
*  
*  
*  

 
 
 
 