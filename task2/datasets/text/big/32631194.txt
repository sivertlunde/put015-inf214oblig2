My Way (2011 film)
{{Infobox film
| name           = My Way 
| image          = My Way (2011 film).jpg
| caption        = South Korean poster
| director       = Kang Je-gyu 
| producer       = Kang Je-gyu   Kim Yong-hwa   James Choi
| writer         = Kim Byung-in   Na Hyun   Kang Je-gyu
| starring       = Jang Dong-gun Joe Odagiri Fan Bingbing
| music          = Lee Dong-jun
| cinematography = Lee Mo-gae
| editing        = Park Gok-ji
| distributor    = CJ Entertainment
| released       =  
| runtime        = 119 minutes
| country        = South Korea
| language       = Korean Japanese Russian German Chinese English
| budget         =  
| gross          =   
| film name = {{Film name
 | hangul =   
 | rr = Maiwei 
 | mr = Maiwei}}
}} Chinese actress Fan Bingbing.

This film is inspired by the true story of a Korean named Yang Kyoungjong who was captured by the Americans on D-Day. Yang Kyoungjong was conscripted into the Japanese Imperial Army, the Red Army, and the Wehrmacht. 

==Plot==
The year is 1928 in Names of Seoul#Gyeongseong|Gyeong-seong (modern-day Seoul), Korea. Young Kim Jun-shik (Shin Sang-yeob), his father (Chun Ho-jin) and sister Eun-soo (Jo Min-ah) work on the farm of the Hasegawa family (Sano Shiro, Nakamura Kumi) in Korea under Japanese rule|Japanese-occupied Korea. Both Jun-shik and young Hasegawa Tatsuo (Sung Yoo-bin) are interested in running; by the time they are teenagers (Do Ji-han, Kobayashi Yukichi), they have become fierce competitors. Tatsuos grandfather (Natsuyagi Isao) is killed in a bomb attack by a Korean freedom fighter and subsequently a Korean runner, Sohn Kee-chung (Yoon Hee-won), wins a marathon race against Japanese competitors, further inflaming Korean-Japanese tensions.
 Japanese army, including Jun-shik and his friend Lee Jong-dae (Kim In-kwon), who has a crush on Eun-soo (Lee Yeon-hee).
 battle at River Khalkhin. Jun-shik, seeing the tanks on the horizon, attempts to return to base to warn the Japanese forces. During his return, he is attacked by a Soviet Polikarpov I-16|I-16 Ishak, and is saved by Shirai, who dies after shooting down the plane. Jun-shik returns to the base and manages to warn the Japanese forces that a large-scale Soviet tank attack is coming but Tatsuo refuses to order a retreat, and both Tatsuo and Jun-shik are seen flying through the air, unconscious.
 POW camp, Soviet army. Johng-dae has saved their lives. They all fight in a bloody battle against the German army at Hedosk in December 1941. Jong-dae dies, while leading the Soviets to battle, but Tatsuo and Jun-shik find themselves alive at the end of the battle and Jun-shik convinces Tatsuo to don German military apparel taken from bodies and travel over the mountains. As they travel, it becomes clear that Tatsuo has been injured. They come upon a town where Jun-shik goes to find medicine to give to Tatsuo. During his search, Jun-shik is found by German soldiers, who, unable to understand him, capture him. Meanwhile, the dying Tatsuo is found by Nazi soldiers searching the house he was placed in.

Three years later, Tatsuo is part of the German Army. He finds himself on the beaches of Normandy, France, just prior to the Normandy landings|D-Day Allied invasion. As the army fortifies the beaches, Tatsuo sees a man running on the beach. He catches up to him and see that it is Jun-shik. It is apparent that they have not seen each other since right after the battle in Hedosk. They decide to run away from Normandy to go home, back to Korea. As they attempt to leave, the Normandy Landings begin. Jun-shik and Tatsuo are locked into a machine gun nest by a German officer. The two force open the door and emerge to a scene of chaos, with American soldiers taking over the beach. They run inland, but Jun-shik is wounded by a bomb fragment in the chest. A dying Jun-shik tells Tatsuo that "He is now Jun-shik," as Tatsuo is Japanese and considered an enemy of the Americans. Tatsuo is later seen running and winning the 1948 Olympic games then a flashback appears of their first encounter. 

==Cast==
 
* Jang Dong-gun - Kim Jun-shik 
* Joe Odagiri - Tatsuo Hasegawa
* Fan Bingbing - Shirai
* Kim In-kwon - Lee Jong-dae
* Lee Yeon-hee - Kim Eun-soo
* Oh Tae-kyung - Kwang-choon
* Kwak Jung-wook - Min-woo
* Kim Hee-won - Choon-bok
* Nicole Jung - press conference guide
* Yang Jin-suk - official at banquet
* Taro Yamamoto - Noda
* Manabu Hamada - Mukai
* Shingo Tsurumi - Takakura
* Kim Shi-hoo - Tsukamoto
* Yoon Hee-won - Sohn Kee-chung
* Chun Ho-jin - Jun-shiks father
* Isao Natsuyagi - Tatsuos grandfather
* Shiro Sano - Tatsuos father
* Kumi Nakamura - Tatsuos mother
* Shin Sang-yeob - Jun-shik (child)
* Sung Yoo-bin - Tatsuo (child)
* Jo Min-ah - Eun-soo (child)
* Do Ji-han - Jun-shik (teen)
* Yukichi Kobayashi - Tatsuo (teen)
* Ko Joo-yeon - Eun-soo (teen)
* Hong Young-geun - Joseon conscripted soldier
* Hakuryu - governor of Athletic Federation (cameo)
* Kim Su-ro - mic man on a truck (cameo)
* Han Seung-hyun
 

==Soundtrack==
#Andrea Bocelli - To Find My Way

==Production== Gangwon Province, Saemangeum Seawall).  
The Soviet BT-5 and BT-7 tanks in the film were copies built on the chassis of old, but functional  American light tank M24 Chaffee.

==Reception==
Despite being one of the most expensive Korean films ever made with a budget of   ( ),  the film flopped at the box office.  It encountered stronger than expected competition from  , released on December 15, and it also received a lukewarm response from viewers. From its release December 21 to the end of the year, My Way sold 1.58 million tickets - only a small fraction of what it would have needed in order to break even. 

The film was nominated in the category of "Best International Film" for the 39th Saturn Awards but lost against Headhunters (film)|Headhunters.

==References==
 

==External links==
*    
*  
*  

 
 
 
 
 
 
 
 
 