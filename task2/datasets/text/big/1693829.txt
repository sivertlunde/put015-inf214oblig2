Twisted Obsession
{{Infobox film
| name           = El sueño del mono loco
| image          = El sueño del mono loco.jpg
| image size     = 250px
| alt            = 
| caption        = Theatrical release poster
| director       = Fernando Trueba
| producer       = 
| writer         = Fernando Trueba Manuel Matji Menno Meyjes
| based on       =  
| starring       = Jeff Goldblum Miranda Richardson Anémone Daniel Ceccaldi Dexter Fletcher Liza Walker
| music          = Antoine Duhamel
| cinematography = José Luis Alcaine
| editing        = 
| studio         = 
| distributor    = 
| released       = 1989
| runtime        = 108 min.
| country        = Spain France
| language       = English Spanish
| budget         = 
| gross          =
}}    
Twisted Obsession (original   erotic thriller directed by Fernando Trueba, starring Jeff Goldblum and Miranda Richardson. It was written by Trueba, Manuel Matji (also known as Manolo Matji) and Menno Meyjes (uncredited), originally based on the novel The Dream of the Mad Monkey by Christopher Frank.

Goldblum stars as a screenwriter who becomes involved with a young incestuous brother and sister. Liza Walker and Dexter Fletcher are also featured in the cast.

==Cast==
*Jeff Goldblum... Dan Gillis
*Miranda Richardson... Marilyn 
*Anémone... Marianne
*Daniel Ceccaldi... Julien Legrand
*Dexter Fletcher... Malcolm Greene 
*Liza Walker... Jenny Greene 
*Jerome Natali... Danny
*Arielle Dombasle... Marion Derain
*Asunción Balaguer... Juana

== External links ==
*  

 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 