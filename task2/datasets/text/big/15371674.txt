Free Woman
{{Infobox film name           = Free Woman image          = Free Woman.jpg caption        = Poster for Free Woman (1982) director       = Kim Ki-young  producer       = Jeong Do-hwan writer         = Lee Mun-ung starring       = Ahn So-young Shin Seong-il music          = Han Sang-ki cinematography = Jung Pil-si editing        = Hyeon Dong-chun distributor    = Shin Han Films Co., Ltd. released       =   runtime        =  country        = South Korea language       = Korean
| film name      = {{Film name hangul         =   hanja          =   rr             = Jayucheonyeo mr             = Chayuch‘ŏnyŏ }} budget         =  gross          = 
}}

Free Woman (hangul|자유처녀 - Jayu cheonyeo) is a 1982 South Korean film directed by Kim Ki-young.

==Synopsis==
A melodrama about a Korean woman who has lived in Spain pursuing her Ph.D. in Korea. 

==Cast==
* Ahn So-young 
* Shin Seong-il
* Kim Won-seop
* Han Seong-gyeong
* Kim Chung-chul
* Cho Ju-mi
* Han U-ri
* Kim Seong-geun
* Lee Yeong-ho
* Yoo Myeong-sun

==Notes==
 

==Bibliography==
*  
*  
*  

 

 
 
 
 


 