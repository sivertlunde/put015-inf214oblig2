Lovers and Other Strangers
 
{{Infobox film
| name = Lovers and Other Strangers
| image = Lovers and other strangers.jpg
| image_size =
| caption = Movie poster
| director = Cy Howard
| producer = David Susskind
| writer = Joseph Bologna David Zelag Goodman Renée Taylor
| narrator =
| starring = Beatrice Arthur Richard Castellano Bonnie Bedelia
| music = Fred Karlin
| cinematography = Andrew Laszlo
| editing = David Bretherton Sidney Katz
| distributor = Cinerama Releasing Corporation
| released =  
| runtime = 104 minutes
| country = United States
| language = English
| budget =$2,550,000 
| gross = $7,700,000 "ABCs 5 Years of Film Production Profits & Losses", Variety, 31 May 1973 p 3 
| preceded_by =
| followed_by =
}}
Lovers and Other Strangers is a 1970 comedy film based on the play by Renée Taylor and Joseph Bologna. The cast includes Richard Castellano, Gig Young, Cloris Leachman, Anne Jackson, Beatrice Arthur, Bonnie Bedelia, Michael Brandon, Harry Guardino, Anne Meara, Bob Dishy, Marian Hailey, Joseph Hindy, and, in her film debut, Diane Keaton. Sylvester Stallone was an extra in this movie. The film was nominated for three Academy Awards (it won the Academy Award for Best Original Song), and was one of the top box office performers of 1970. It established Richard Castellano as a star (receiving an Oscar nomination for his performance) and he, along with Diane Keaton, was subsequently cast in The Godfather. The song For All We Know was composed by Fred Karlin with lyrics by Robb Royer and Jimmy Griffin.

Lovers and Other Strangers was released by ABC Pictures. It was released on VHS in 1980 by Magnetic Video, but soon went out of print. The Magnetic Video release was a collectors item for many years, but the film was eventually re-released on VHS by CBS/Fox Video in the 1990s. It is now available on DVD by MGM Home Entertainment.
 Richard Carpenter For All We Know", with his sister Karen Carpenter|Karen. "Karen and I were in Toronto to open the show for Engelbert Humperdinck. We had one night off before opening and our manager Sherwin Bash suggested we see the film Lovers and Other Strangers. We enjoyed the film and noticed the song For All We Know which we recorded upon our return home." (It subsequently won an Oscar for Best Song of 1970.) 
 Made for Each Other in which they also starred.

==Synopsis==
Lovers and Other Strangers revolves around the wedding of Mike (Michael Brandon) and Susan (Bonnie Bedelia), intercutting their story with those of other couples among their families and friends. As the movie opens, Mike wants to call off the wedding, arguing that it would be hypocritical for them to get married when theyve already been living together for a year and a half. He only relents when Susans father Hal (Gig Young) tells him that Susan went to her first Halloween party dressed as a bride.

Over the course of the movie, we meet:
* Susans White Anglo-Saxon Protestant|WASP-ish parents, Hal (Gig Young) and Bernice (Cloris Leachman). Hal has been having an extramarital affair with Bernices sister Kathy (Anne Jackson), who is afraid of ending up a  spinster and is using the wedding to get some commitment from Hal.
* Susans sister Wilma (Anne Meara) and her husband Johnny (Harry Guardino). As parents of two, Wilma is feeling her age and misses the passion they had at the beginning of their marriage, while Johnny is more interested in watching Spellbound (1945 film)|Spellbound on TV than giving his wife attention.
* Mikes brother Richie (Joseph Hindy) and his wife Joan (Diane Keaton), who have grown "incompatible" and are considering divorce. Nebbishy self-imagined playboy Jerry spends most of the weekend trying to "score" with Brenda. Broadway play, received a Best Supporting Actor nomination for his role.)

These plotlines all play out through the rehearsal, wedding, and reception.

==Awards==
*Nominee Best Supporting Actor - Academy Awards (Richard Castellano)
*Nominee Best Adapted Screenplay - Academy Awards (Joseph Bologna, David Zelag Goodman)
*Winner Best Original Song (For All We Know) - Academy Awards (Richard Karlin, Robb Royer, Jimmy Griffin)

==Reception==
The film was very popular, earning rentals of $7 million in North America. It recorded an overall profit of $790,000. 
==Footnotes==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 