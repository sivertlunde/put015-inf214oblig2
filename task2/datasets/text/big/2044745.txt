Went the Day Well?
{{Infobox film
| name = Went the Day Well?
| image = Went the Day Well Poster.jpg
| caption = Theatrical movie poster Cavalcanti
| producer = Michael Balcon Diana Morgan Valerie Taylor David Farrar
| music = William Walton
| cinematography = Wilkie Cooper
| editing = Sidney Cole
| distributor = Ealing Studios
| released =  7 December 1942 (UK)
| runtime = 92 minutes
| country = United Kingdom
| language = English
| budget =
}} 1942 United British war film adapted from a story by Graham Greene and directed by Alberto Cavalcanti. It was produced by Michael Balcon of Ealing Studios and served as unofficial propaganda for the war effort. It tells of how an English village is taken over by Nazi paratroopers. It reflects the greatest potential nightmare of many Britons of the time, although the threat of German invasion had largely receded by that point.    (Germanys planned invasion, Operation Sea Lion, had been indefinitely postponed.) It includes the first major role of Thora Hird, and one of the last of C. V. France. The village location for some scenes was Turville in Buckinghamshire. 

==Plot== English village German soldiers intended to form the vanguard of an invasion of Britain, they round up the residents and hold them captive in the local church. The vicar is shot after sounding the church bell in alarm.
 Home Guard, village squire, who is revealed to be collaborating with the Germans. Members of the local Home Guard are ambushed and shot by the Germans. Several attempts are made to escape or contact the outside world, all of which fail until eventually a young boy succeeds in escaping; despite being shot in the leg, he alerts the army. British soldiers arrive, and - aided by some of the villagers, including a group of Womens Land Army girls, who have managed to escape, barricade themselves in, and arm themselves - defeat the Germans after a short battle. The squire is shot dead by the vicars daughter, who had discovered his treachery, as he attempts to let the Germans into the barricaded house.

The villager retelling the story to the camera shows the Germans grave in the churchyard and explains proudly that "this is the only bit of England they got".

==Cast==
 
*Leslie Banks as Oliver Wilsford, the treachorous squire
*C.V. France as the Reverend Ashton, the vicar Valerie Taylor as Nora Ashton, the vicars daughter
*Marie Lohr as Mrs Fraser
*Basil Sydney as Kommandant Orlter, alias Major Hammond
*Harry Fowler as George Truscott
*Elizabeth Allan as Peggy Pryde (a Land Army Girl)
*Frank Lawton as Tom Sturry
*Thora Hird as Ivy Dawking (a Land Army Girl)
*Muriel George as Mrs Collins, the postmistress
*Patricia Hayes as Daisy
*Mervyn Johns as Charles Sims
*Norman Pierce as Jim Sturry
*Hilda Bayley as Cousin Maude
*Edward Rigby as Bill Purvis, the poacher
*Ellis Irving as Harry Drew
*Grace Arnold as Mrs Owen David Farrar as Lt. Jung, alias Lt. Maxwell John Slater as German sergeant
*James Donald as German corporal
* Men of the Gloucestershire Regiment
 

==Title quotation==
:Went the day well?
:We died and never knew.
:But, well or ill,
:Freedom, we died for you.

This epitaph was written by the classical scholar John Maxwell Edmonds, and originally appeared in The Times on 6 February 1918, page 7, under the heading Four Epitaphs.
"Went the day well" also appeared in an unidentified newspaper cutting in a scrapbook now held in the RAF Museum (AC97/127/50), and in a collection of First World War poems collated by Vivian Noakes. 

==Reception==
The film reinforced the message that civilians should be vigilant against fifth columnists and that "careless talk costs lives". It was based on a short story by the author Graham Greene entitled The Lieutenant Died Last.  By the time the film was released the threat of invasion had subsided somewhat, but it was still seen as an effective piece of propaganda, and its reputation has grown over the years. It has been noted that by opening and closing in a predicted future where not only had the war been won but a (fictitious) full-scale German invasion of Britain defeated, and by presenting a scenario where all echelons of British society unite for the common good (the lady of the manor sacrifices herself without hesitation, for example), the films message was morale-boosting and positive rather than scaremongering.     In 2010 an article in The Independent (London) commented, "It subtly captures an immemorial quality of English rural life—the church, the local gossip, the sense of community—and that streak of native pluck that people believed would see off Adolf Hitler|Hitler". 

In 2005 it was named as one of the "100 Greatest War Films" in a Channel 4 poll in Britain. The 1975 book, and later film, The Eagle Has Landed uses some of the same ideas. {{Cite journal|url=http://www.encyclopedia.com/doc/1G1-107041433.html|title=
Nazis into Germans: Went the Day Well? (1942) and The Eagle Has Landed (1976; Critical Essay)|journal=Journal of Popular Film and Television |publisher=encyclopedia.com |date=22 June 2003 |accessdate=2 January 2010}}  
 Time Out (London) termed it "jawdroppingly subversive. Cavalcanti establishes, with loving care and the occasional wry wink, the ultimate bucolic English scene, then takes an almost sadistic delight in tearing it to bloody shreds in an orgy of shockingly blunt, matter-of-fact violence."  When the restored film opened at Film Forum in New York City in 2011, A.O. Scott of The New York Times called it "undeservedly forgotten...  ome-front propaganda has rarely seemed so cutthroat or so cunning."  

==See also== film based on it with a similar premise.

==References==
 

==Sources==
* Houston, Penelope. Went the Day Well? London: BFI, 1992 ISBN 0-85170-218-6

==External links==
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 