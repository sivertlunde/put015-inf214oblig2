A Proper Scandal
{{Infobox film
| name =    A Proper Scandal
| image =  Uno scandalo perbene.jpg
| director = Pasquale Festa Campanile
| writer =
| starring =  Ben Gazzara
| music = Riz Ortolani
| cinematography = Alfio Contini
| editing =
| producer =
| distributor =
| released =  
| runtime =
| awards =
| country = Italy
| language = Italian
| budget =
}} 1984 Cinema Italian drama film. It represents the last film written and directed by Pasquale Festa Campanile.   It is a true dramatization of the Bruneri-Canella case.     

The film entered the competition at the 41° Venice International Film Festival. 

== Cast ==
* Ben Gazzara: Bruneri/Canella
* Giuliana De Sio: Giulia Canella
* Vittorio Caprioli: Renzo 
* Franco Fabrizi: Count Guarienti 
* Valeria DObici: Camilla Ghidini 
* Giuliana Calandra: Maria Gastaldelli 
* Vincenzo Crocitti: The Journalist
* Enzo Robutti:  The Professor 
* Carlos de Carvalho : Count De Besi
* Clara Colosimo : Tenutaria

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 