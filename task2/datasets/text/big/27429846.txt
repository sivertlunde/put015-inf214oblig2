Confessions of a Cheat
{{Infobox film
| name           = Confessions of a Cheat
| image          = Confessions-of-a-cheat-poster.jpg
| caption        = 
| director       = Sacha Guitry
| producer       = Serge Sandberg
| writer         = Sacha Guitry
| starring       = Sacha Guitry
| narrator       = Sacha Guitry
| music          = Adolphe Borchard
| cinematography = Marcel Lucien
| editing        = Myriam Borsoutsky
| studio         = Films Sonores Tobis; Cinéas Gaumont (2007)
| released       =  
| runtime        = 81 minutes
| country        = France French
}}

Le Roman dun tricheur (sometimes known in English as The Story of a Cheat, but also as Confessions of a Cheat, The Story of a Trickster, or The Cheat) is a 1936 film starring, written and directed by Sacha Guitry.  It was adapted from Guitrys only novel, Les Mémoires dun tricheur, published in 1935. 

==Plot==
The 54-year-old Cheat writes his memoirs in a café. As the age of 12, he is caught stealing money from the family grocery shop. As punishment, he is not allowed to enjoy a treat with the rest of the family: mushrooms which turn out to be poisonous.  His parents, siblings, uncle and grandparents all die. His mothers unscrupulous cousin takes charge of him, and uses his inheritance for his own benefit. Thus, it appears to the youngster that dishonesty pays.

He runs away and works at various jobs, such as doorman and hotel bellhop. In Paris, he is unwillingly drawn into a plot to assassinate the visiting Czar Nicholas II of Russia by fellow restaurant worker Serge Abramich. However, an anonymous letter (which the Cheat implies he wrote) leads to the arrest of Abramich and the other plotters.

As an elevator operator in the Hotel de Paris in Monaco, the Cheat catches the eye of the much older Countess. They have a brief fling. (By chance, they meet again in the present at the café, though the now elderly Countess does not recognize her former paramour, much to his relief.)

After a stint in the army, the Cheat decides to take up a profession that rewards honesty: croupier in a casino in Monaco.  However, he is taken back into the French Army at the start of World War I. He is wounded almost immediately, but is rescued by Charbonnier, a fellow soldier who loses his right arm as a result. The army loses track of the Cheat, allowing him to spend the war reading books.

Once more a civilian, he is picked up by a beautiful woman in a restaurant. After spending the night together, she confesses that she is a professional thief. She enlists him in robbing a jeweler of a ring through trickery. Though they are successful, he slips away afterward and returns to work as a croupier. 

An attractive regular at his roulette wheel believes he can control where the ball falls. Indeed, over the next few days, she wins consistently. They become partners; to ensure he will get his share of the winnings, he gets her to agree to a marriage of convenience. However, she loses all of the money she had won before; all of the other gamblers win so much, he is fired, ironically for being unable to cheat. The couple quickly obtain a divorce.

He then becomes a professional card cheat. One day, while in disguise, he spots two women from his past, his former wife and the Jewel Thief together at a gambling table, evidently friends. He gallantly invites them to share half his bet, but only if he wins. He does, and they accept his dinner invitation. Both indicate they are willing to go to bed with him, still unaware of his true identity; he chooses his ex-wife because he had not slept with her before.

Later, he cheats Charbonnier at baccarat before he recognizes his benefactor. Fortunately, they tie, but he is so filled with shame, he quits cheating. Charbonnier, having unwittingly cured the Cheat of one vice, infects him with another: the love of gambling. The Cheat loses all of the ill-gotten gains of years in a matter of months. 

Returning to the present, the Countess finally recognizes her former lover and tries to recruit him to help her rob the house across the street. The Cheat declines, explaining first that it was his former home and second that he has since embarked on the only honest job that utilizes his skills, that of security officer.

==Cast==
*Sacha Guitry as The Cheat (as an old man)
*Marguerite Moreno as The Countess (as an old woman)
*Jacqueline Delubac as Henriette, the Cheats wife
*Roger Duchesne as Serge Abramich
*Rosine Deréan as The Jewel Thief
*Elmire Vautier as The Countess (as a younger woman)
*Serge Grave as The Cheat (as a boy)
*Pauline Carton as Madame Moriot, the cousins wife
*Fréhel as The Singer
*Pierre Labry as Cousin Moriot
*Pierre Assy as The Cheat (as a young man)
*Henri Pfeifer as Monsieur Charbonnier
*Gaston Dupray as Waiter

==Production== The Magnificent Ambersons.   Guitry also used the spoken word to present the actors and technicians as they are introduced on-camera at the start of the film, instead of the conventional printed credits. 

==Reception==
On its release in France, the film met with a mixed reception, but it subsequently maintained a reputation among critics and film-makers as one of Guitrys best films; in 2008 it was included in Cahiers du Cinémas list of 100 most important films. 

The film was released in the UK and USA, and enjoyed significant success.  In London a reviewer for The Times commented on its elaborate narrative structure: "As always when the difficulties and disadvantages of a constricting technique are overcome, there is an effect of unity and precision which freedom cannot often give".   When the film opened at the Academy Cinema in London in September 1937, it ran there for four months. Around the UK it was shown in 400 cinemas.   In the USA, the reviewer for The New York Times described it as "witty, impudent, morally subversive" and commented, "Only a man of Guitrys impudence could have succeeded in making The Story of a Cheat the clever picture that it is". 

==References==
 

== External links ==
* 
* 
* 

 
 
 
 
 
 
 
 
 