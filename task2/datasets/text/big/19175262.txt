Bewafa Sanam
{{Infobox film
| name           = Bewafa Sanam
| image          = Bewafa Sanam DVD cover.jpg
| caption        =
| director       = Gulshan Kumar
| producer       = Gulshan Kumar
| writer         = Sachin Bhowmick Madan Joshi
| story          = Gulshan Kumar
| starring       = Krishan Kumar Aruna Irani Shilpa Shirodkar Shakti Kapoor Kiran Kumar
| music          = Gopes
| cinematography = Thomas Xavier
| editing        = Omkar Bhakri
| distributor    = Super Cassettes Industries Limited (T-Series)
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| Budget         =
}}
Bewafa Sanam is a 1995 Hindi film directed by Gulshan Kumar. It stars Krishan Kumar(Brother of Gulshan Kumar), Shilpa Shirodkar, Aruna Irani, Shakti Kapoor and Kiran Kumar. The screenplay was written by Sachin Bhowmick, dialogue by Madan Joshi, and story by Gulshan Kumar. It was produced by Gulshan Kumar.

== Plot ==
Sundar and Sheetal love each other and hope to get married soon. His mother approves of this match, Sundar makes his living by playing cricket and has become a very popular cricketer and this has brought about many enemies in his life. Among them are Gautam, who conspires with Sheetal to bring about the destruction of Sundar, to which Sheetal agrees because Gautam has Sheetals mother captive and threatens to kill her if Sheetal does not agree to all his schemes and they send goons after him and after trashing the goons he lands himself in jail and here is where he will find out that Sheetal is not what she claims to be. Frustrated at this, on Sheetal and Gautam wedding, Sundar picks up a pistol and shoots Sheetal in public and is charged for this crime. A charge that carries the death sentence. Later in the movie a journalist tries to help him and finds out everything that happened. Once Sundar realises that Sheetal was forced to do everything and she actually loved him, he requests the authority to let him visit Sheetals grave one last time before he is hanged for his crime and they let him. Sundar dies on her grave after kissing her grave stone.

== Reception ==

Bewafa Sanam was an unexpected hit at the Box-Office, the only certified hit of Krishan Kumar.

== External links ==
*  

 
 
 


 