Boomi Malayalam
{{Infobox film
| name           = Bhoomi Malayalam (The Mother Earth)
| image          = Boomi Malayalam.jpg
| caption        = 
| director       = T. V. Chandran
| producer       = Revathi Chandran V. P. Abeesh
| writer         = T. V. Chandran
| starring       = Suresh Gopi Nedumudi Venu Samvrutha Sunil
| music          = Isaac Thomas Kottukapally
| cinematography = K. G. Jayan
| editing        = Venugopal
| associate director = Yadavan Chandran
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Bhoomi Malayalam ( ) is a 2009 Malayalam film directed by T. V. Chandran. The film features Suresh Gopi in a dual role along with Nedumudi Venu, Samvrutha Sunil and other actors.

Bhoomi Malayalam depicts the plight of seven different women, each of whom represent different periods of time. Starting from the early 1980s, the film moves down to the present era.

== Plot ==
The movie tells an incident that took place in Thillenkeri in Kannur district in 1948 April 15. Communist party was struggling against the rule of Jawaharlal Nehru at that period. In Thillenkeri, comrade  Ananthan Master (Suresh Gopi) was the leader of communist party.

He was shot dead  while he was leading a demonstration against feudals . His wife Meenakshi was pregnant at that time and later gave birth to a baby girl Janaki Amma. Janaki Ammas son is Narayanan kutty (Suresh Gopi). From the first generation Ananthan to third generation Narayanan Kutty, the film attempts to capture the fear that has been pervading the life of women living in different periods of time, at different places.

== Cast ==
* Suresh Gopi as Ananthan, Narayanan Kutty
* Nedumudi Venu
* Samvrutha Sunil as Nirmala
* Padmapriya as Fousia
* Priyanka Nair as Annie
* Lakshmi Sharma
* Manikandan Pattambi
* Kripa as Sathi
* Nanda
* Jasna
* Lakshmipriya

==See also==
* Malayalam films of 2009

==External links==
*  
*   at Nowrunning.com
*   at Oneindia.in

 

 
 
 
 
 
 
 


 