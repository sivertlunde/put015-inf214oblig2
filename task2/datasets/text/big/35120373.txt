The Story of Short Stack
 
{{Infobox film
| name           = The Story of Short Stack
| image          = Story of Short Stack cover.jpg
| director       = Andy Clemmensen
| producer       = Andy Clemmensen Shaun Diviney  (exec.)  Bradie Webb  (exec.) 
| starring       = Shaun Diviney Andy Clemmensen Bradie Webb
| music          = Short Stack Bradie Webb  (add.) 
| editing        = Andy Clemmensen
| released       =    
| runtime        = 67 minutes
| country        = Australia
| language       = English
}} many people, notably the band itself (Shaun Diviney, Andy Clemmensen and Bradie Webb). The film was premiered on 27 October 2011  and officially released on 4 November 2011.   

The documentary spent years being made. It included the bands entire back catalogue of film clips and highlights of their career from their debut album Stack Is the New Black through to the cover of Rolling Stone on their 2010 album This is Bat Country and their single "Bang Bang Sexy".   
 Art Vandelay. Art Vandelay was released on Friday 18 October 2013, however with a slightly altered tracklisting. 

==Trailer==
A trailer was released on Short Stacks official YouTube channel, featuring excerpts from some of the interviews and featured clips from the movie and the new music video for "Bang Bang Sexy", which was also played in the background throughout the trailer. 

==Premiere==
The film was premiered live via satellite on 27 October 2011 at Event Cinemas around Australia. Hosted by Hot 30s Matty Acton and Maude Garrett, the cinema event included a live acoustic set by the band, a live Q&A with fans in cinema and the first screening of their documentary. Regardless of which cinema fans go to across the country, they were able to share in the same experience live.
{| class="wikitable"
|-
! Location
! Time  (AEST) 
|-
| Queensland
| align=center| 6:00PM
|-
| South Australia
| align=center| 6:30PM
|-
| New South Wales
| align=center rowspan=2| 7:00PM
|-
| Western Australia
|}
The Perth screening time was shifted for the audiences enjoyment.

Diviney, Clemmensen and Webb also walked the red carpet at Event Cinemas George Street in Sydney at the Official World Premiere. There were only a limited number of tickets available to the event. 

==Reception==
On the purchase page for the DVD on the bands official website, the film has received all positive reviews. The total is 5 out of 5 stars, excluding the spam comments. 

==Credits==

===Starring===
{| class="wikitable"
|-
! Name
! Affiliation
|-
| Shaun Diviney Members of Short Stack
|-
| Andy Clemmensen
|-
| Bradie Webb
|-
| Trevor Steel Sunday Morning Records
|-
| Chris Johns
|-
| Sonny Joe Flanagan
| Friend
|-
| Matty Acton
| Radio host
|-
| Lee Groves
| Music producer
|-
| Dan Reisinger
| Music video director
|-
| Lewis Usher
| Touring Assistant
|-
| Luke Lukess
| Touring guitarist
|-
| Sinj Clarke
| Touring keyboardist
|-
| Shaun Jennings
| Touring Manager
|-
| Matt Covte
| Rolling Stone Magazine
|-
| Brad Smith
|rowspan=4| Members of Heroes for Hire
|-
| Duane Hazell
|-
| Alex Bonic Potter
|-
| Lee McGarrity
|-
| Jay Taplin
|rowspan=6| Members of For Our Hero
|-
| Beau Taplin
|-
| Dave Tran
|-
| Geoffrey Mark Taylor
|-
| Nax Vee
|-
| Leon Blair
|}

===Other===
{| class="wikitable"
|-
! Credited for
! Name
|-
| Directed by
| rowspan=2| Andy Clemmensen
|-
| Produced by
|-
| rowspan=2| Executive Producers
| Shaun Diviney
|-
| Bradie Webb
|-
| Edited by
| Andy Clemmensen
|-
| Additional Music by
| Bradie Webb
|-
| Post Production by
| rowspan=2| Gerald Webb
|-
| Live footage edited by
|-
| rowspan=2| Live footage filmed by
| Walter Loveridge
|-
| Gerald Webb
|-
| rowspan=3| Cover and Menu Artwork
| Goran Momircevski
|-
| Justin Smith
|-
| End of Work
|-
| Live Photography
| Walter Loveridge
|-
| Tour Photography
| Jimmy Machan
|-
| rowspan=2| Sunday Morning Records/Artist Management
| Trevor Steel
|-
| Chris Johns
|-
| rowspan=3| Artist Booking
| Tony Grace Guarrera
|-
| Jeremy Sharp
|-
| The Harbour Agency
|}

===Featured songs===
{| class="wikitable"
|-
! Song
! Artist
|-
| "Wild Wild West"
| The Escape Club
|-
| "All of Me"
| My Future Lies
|-
| "Dont You Dare"
| For Our Hero
|-
| "Secrets, Lies and Sins"
| Heroes for Hire
|}

====by Short Stack====
{| class="wikitable"
|-
! Song
! Album
|-
| "Black Tears"
|rowspan=2| One Size Fits All
|-
| "Boopacha"
|-
| "Before Angels Fall"
|rowspan=4| One Size Fits All and Stack Is the New Black
|-
| "We All Know"
|-
| "Back of My Head"
|-
| "Its 4 You"
|-
| "Sway Sway Baby"
|rowspan=6| Stack Is the New Black
|-
| "Shimmy a Go Go"
|-
| "Thick as Thieves"
|-
| "Princess"
|-
| "In This Place"
|-
| "Drop Dead Gorgeous"
|-
| "Nothing at All"
|rowspan=5| This Is Bat Country
|-
| "Planets"
|-
| "Are You Afraid of the Dark?"
|-
| "Sweet Emergency"
|-
| "Heartbreak Made Me a Killer"  (uncredited) 
|-
| "In My Hands"
|rowspan=2| Planets (EP)  (credited from This is Bat Country) 
|-
| "Electric Romantics"
|-
| "New York City Ballet" Art Vandelay  (unreleased) 
|-
| "Bang Bang Sexy"  (uncredited) 
|-
| "S.O.U.L."  (uncredited) 
|-
| "Tonight, Tonight, Tonight"  (uncredited) 
|-
| "Saturday Night"
|}

==References==
 

 

 
 
 
 