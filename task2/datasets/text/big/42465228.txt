Naa Mechida Huduga
{{Infobox film|
| name = Naa Mechida Huduga
| image = 
| caption =
| director = R. N. Jayagopal
| writer = P. S. Vaidyanathan
| based on =  Kalpana   K. S. Ashwath
| producer = Harini
| music = Vijaya Bhaskar
| cinematography = T. G. Shekar
| editing = N. C. Rajan
| studio = Vijaya Bharati
| released =  
| runtime = 154 minutes
| language = Kannada
| country = India
| budget =
}} Kannada romantic drama film directed and scripted by R. N. Jayagopal. It is based on the story "Divorce In Indian Style" written by P. S. Vaidyanathan.
 Leelavathi in lead roles.  The music composed by Vijaya Bhaskar was widely appreciated and declared chart-busters.

== Cast ==
* Srinath  Kalpana 
* K. S. Ashwath  Leelavathi
* Vadiraj
* Ramesh
* Shivaram
* Advani Lakshmi Devi
* Master Arun

== Soundtrack ==
The music was composed by Vijaya Bhaskar with lyrics by R. N. Jayagopal.  All the songs composed for the film were received extremely well and considered as evergreen songs .

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Beladingalina Nore Haalu
| extra1 = P. B. Sreenivas, S. Janaki
| lyrics1 = R. N. Jayagopal
| length1 =
| title2 = Appa Amma Jagaladali
| extra2 = Vadiraj, B. K. Sumithra, L. R. Anjali
| lyrics2 = R. N. Jayagopal
| length2 = 
| title3 = Cheluve Oh Cheluve
| extra3 = P. B. Sreenivas, A. L. Raghavan
| lyrics3 = R. N. Jayagopal
| length3 = 
| title4 = Mangalada Ee Sudina
| extra4 = S. Janaki
| lyrics4 = R. N. Jayagopal
| length4 = 
| title5 = Naa Mechida Huduganige
| extra5 = S. Janaki
| lyrics5 = R. N. Jayagopal
| length5 = 
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 


 