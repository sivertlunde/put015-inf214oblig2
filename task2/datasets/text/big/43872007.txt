Sakkarai Devan
{{Infobox film
| name           = Sakkarai Devan
| image          =
| image_size     =
| caption        =
| director       = J. Panneer
| producer       = A. S. Ibrahim Rawuthar
| writer         = R. Selvaraj Liyakath Ali Khan (dialogues)
| screenplay     = Liyakath Ali Khan Sukanya Kanaka Kanaka M. N. Nambiar
| music          = Ilayaraja
| cinematography = Rajarajan
| editing        = G. Jayachandran
| studio         = I. V. Cini Productions
| distributor    = I. V. Cini Productions
| released       = 1993
| country        = India Tamil
}}
 1993 Cinema Indian Tamil Tamil film, Kanaka and M. N. Nambiar in lead roles. The film had musical score by Ilayaraja.  

==Cast==
 
*Vijayakanth Sukanya
*Kanaka Kanaka
*M. N. Nambiar
*Nassar
*Mohan Natarajan
*Senthil
*Thyagu
*R. Sundarrajan
*Vadivelu
*Gandhimathi
*C. K. Saraswathi
*Baby Monisha
 

==Soundtrack==
The music was composed by Ilaiyaraaja.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Janaki || Vaali || 04.50 
|-  Janaki || Vaali || 04.58 
|-  Vaali || 04.54 
|-  Vaali || 05.54 
|-  Janaki || Vaali || 04.43 
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 


 