Roller Boogie
{{Infobox film
| name           = Roller Boogie
| image          = Rollerboogie.jpg
| image_size     = 215px
| alt            = 
| caption        = Promotional film poster
| director       = Mark L. Lester
| producer       = Bruce Cohn Curtis
| screenplay     = Barry Schneider
| story          = Irwin Yablans
| starring       = Linda Blair Jim Bray Albert Insinnia Jimmy Van Patten Kimberly Beck Stoney Jackson Beverly Garland Mark Goddard
| music          = Craig Safan
| cinematography = Dean Cundey
| editing        = Byron "Buzz" Brandt Edilberto Cruz Edward Salier Compass International
| distributor    = United Artists
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $13,253,715
}}
Roller Boogie is a 1979 musical film starring Linda Blair and introducing Jim Bray, a former competitive artistic skater from California. The film also stars Beverly Garland, Mark Goddard, and Kimberly Beck, and is directed by Mark L. Lester.

The film is set in the Venice, Los Angeles, California area at the height of the roller skating fad. Two characters, Bobby James (Bray) and Terry Barkley (Blair), fall in love while boogie skating to disco music. Along the way they must thwart a powerful mobster who wants the land their favorite roller rink sits on and compete in the Boogie Contest.

The film received mostly negative reviews.

==Plot==
Bobby James and his friends (Phones, Hoppy, Gordo, and several others) skate to work on the Venice, California boardwalk. Meanwhile in Beverly Hills, Terry Barkley, genius flautist is also heading towards the beach in her Excalibur Phateon car. She also is joined by her stuck-up girlfriend Lana.

Bobby is skating on the boardwalk with a female friend when he encounters Terry. But she remains aloof and spurns his advance. Later they meet at a local skating rink called Jammers. During a near catastrophic skating incident where Bobby saves the day, she gives in. Terry wants to pay him to teach her how to skate for the Roller Disco contest. Even though they share a flirty, romantic couples skate, later on she rebuffs him yet again.

The next day has both Terry and Bobby getting flack from their respected friends and family. She has had enough and goes to the beach. She finds Bobby there, practicing a jump and turns on the charm. He shares with her his dream to become an Olympic Roller Skater. They end up making out on the beach. Bobby asks her if shes going to pay him for sex as well, which garners a mighty slap in return and she takes off.

Terry goes home and has a row with her mother. She wants to give up her dreams of playing Classical Flute at Juilliard School and win a roller disco contest at the beach. Her mom is shocked, enough so she needs a Valium. Terry decided to run away.

The next morning, she calls and invites Bobby to breakfast where she apologizes. He wants to skate with her, but on his terms: no money; he calls the shots. Through a series of outdoor scenes we see them work together to form a routine.

Unfortunately, Jammers is about to be sold to a ruthless mob developer. Bobby and Terry are clued into this plot and try to get her father, a lawyer, to help. But he refuses. While Terry is performing at a lush outdoor party, some of the young men sneak up, causing chaos. As a result, a group of distinguished guests falls into the swimming pool.  This ruins the concert, as well as the party and its ceremonial cake. Terry gets reprimanded and punched by her father for her running away, as well as for hanging out with her radical friends. The skaters find evidence, in the form of a cassette tape recording of the invalid ordeal, to kill the deal. Through a wild chase on the streets near the canal zone of Venice, they race to get it to the cops on time. They do, the mobsters are hauled off and the Boogie Contest is on. Terry and Bobby skate their routine and win.

Later on, back at the beach Terry and Bobby share a sad goodbye. Both promise to write each other as she heads off to New York City and him to the Olympics.

==Development== Tourist Trap, the latter the most successful of the three.

Made before the roller-skating fad of the late-1970s and early-1980s ended, the film was originally led by Linda Blair and Canadian actor David Kennedy, with whom Blair was dating at the time. However, when Kennedy and Blair ended their relationship Kennedy was dropped from the picture, rumors  stating at Blairs request.  Several other actors were considered for the character Bobby James, including Peter Galagher and Albert Insinnia; who would actually play the character Gordo in the finished movie. Jim Bray, an American amateur roller skating champion who had won over 270 awards for his skating talents by the age of eighteen, was originally cast as the stuntman for the uncast male lead. When producers had difficulty finding someone to fill the role, Bray was given acting lessons and cast in the role. 

In an earlier version of the script, Bobby James primary interest was song-writing. In the movie, Bobby and friends were trying to write a new song by humming into a tape recorder. Once he meets Terry she assists him in scoring it using her musical abilities. By the end of the movie, Terry leaves for her music scholarship whilst Bobby pursues his musical career. There is no mention of the Olympics in the earlier version. Another scene that does not feature in the movie that was present in the script comes where Bobby helps Terry escape from her bedroom after she is grounded by her parents. This scene was shot, however, since there are photos in various publicity items with Jim Bray peeking through a window on a ladder.
 David Winters and were instrumental in the success of the movie. Prior to the production of the picture, skating trainer Barbara Guedel tested over 300 young skaters, finally selecting fifty that would make up the skating crowds in the picture - many of whom would also feature in another skating-influenced picture, Xanadu (film)|Xanadu (1980). The ensemble were then given three weeks of training before the photography began, and, at the behest of their managers/producers, the principal actors were only on roller-skates for short periods of time. However, Blair did much of her own skating for the picture. Two stunt doubles were used, one for the skating chase around the streets of Venice - Barbara Guedel would perform the trickier dancing stunts in the competition sequence. Blair would develop bursitis in her hip during the making of the picture. 

The film was shot in eight weeks through the Summer of 1979, mostly on the Venice boardwalks but also in Beverly Hills and, for the final competition sequence, at The Stardust Ballroom in Hollywood. At the end of principal photography, Blair had to return to Florida for a court appearance (from her arrest, in 1977, for possessing cocaine). 

==Soundtrack==
The soundtrack of the film largely draws on the disco sound that was popular in the late 1970s. A double-LP soundtrack was issued by Casablanca Records in 1979.
 Take Me Hell on Wheels", used in the opening sequence. The track originally featured on the Prisoner (Cher album)|Prisoner album, and a rare accompanying video clip featuring Cher roller-skating also appeared around the same time as the release of the movie.

Interestingly, the "Hell on Wheels" Japanese single includes another Prisoner album track, the 12" version of "Git Down (Guitar Groupie)", which is advertised as "Theme from Roller Boogie". However, the song does not appear in the movie. This single features a still of Linda Blair and Jim Bray on the cover.

The song "Lord Is It Mine", performed by Bob Esty, was originally written by Supertramps Roger Hodgson for their Breakfast in America LP. He also performed the tracks "Summer Love", "Rollin Up a Storm", and "Roller Boogie". The segued opening tracks of side two of the double LP, "Electronix (Roller Dancin)", and the Latin-disco instrumental "Cunga", are credited to Bob Esty and Cheeks. Craig Safan composed cues for the films original score, however, the film tends to rely on actual songs as opposed to instrumental pieces.

Along with "Hell on Wheels", the other song on the soundtrack that was previously available prior to the soundtrack release is Earth, Wind & Fires well-known disco single, "Boogie Wonderland", featuring the female group, the Emotions. Only one song featured in the film does not feature on the soundtrack, Jean Shys "Night Dancer", which appears in the movie when Terry first visits the roller-disco rink.

{{Infobox album
| Name        = Roller Boogie
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = 
| Released    = 1979
| Recorded    =
| Genre       = Disco
| Length      = 41:20
| Label       = Casablanca Records
| Producer    = Bob Esty
}}

Side A: Hell on Wheels" - Cher (5:32)
# "Good Girls" - Johnnie Coolrock (3:38)
# "All for One, One for All" - Mavis Vegas Davis (4:20)
# "Boogie Wonderland" - Earth Wind & Fire (4:48)

Side B:
# "Weve Got the Power" - Ron Green (5:15)
# "Top Jammer" - Cheecks (4:12)
# "Summer Love" - Bob Esty (3:53)
# "Takin My Life in My Own Hands" - Ron Green  (5:25)

Side C:
# "Electronix (Roller Dancin)" - Bob Esty & Cheeks (5:00)
# "Cunga" - Bob Esty (4:54)
# "Evil Man" - Mavis Vegas Davis (4:17)
# "Lord is it Mine" - Bob Esty (4:26)

Side D:
# "Rollin up a Storm (The Eye of the Hurricane)" - Bob Esty (6:30)
# "The Roller Boogie" - Bob Esty (6:09)
# "Love Fire" - Bob Esty & Michelle Aller (4:54)

==Release==
The film was promoted with a lengthy trailer in the Autumn of 1979, before the premiere in New York City in early December. The film was released nationally on December 23. The film grossed $13.2 million at the box office, proving popular with teen audiences.  Initially, Compass International Pictures planned on a sequel (to be set in Mexico—Acapulco Roller Boogie); however, probably due to the end of the disco fad the idea was scrapped. Linda Blair, according to a Teen Beat article published in 1980, intended on moving away from the horror genre in favor of more light-hearted pictures such as Roller Boogie, but returned to the genre the following year in another Compass International Pictures produced movie, Hell Night (1981). Jim Bray did not appear in any other films after Roller Boogie, despite achieving something of pin-up status in teen magazines largely based on his appearance in the film. Stoney Jackson would appear in the third and final season of The White Shadow as Jesse B. Mitchell before its cancellation in 1981, and in Michael Jacksons pop video "Beat It", in 1983.

In August 2006, fashion brand American Apparel played the movie in store windows in New York City. Matthew Swenson, the companys fashion media director, stated; We became obsessed with that movie. On a whim, we also bought lamé fabric and turned them into leggings, and the gold and silver took off.".   Despite a weighted average vote of 3.7 / 10 on the Internet Movie Database (and also a rare 0% on Rotten Tomatoes ), the film has developed a loyal following and is seen as something of a time-capsule of the late 1970s and the disco era. The film is considered a cult classic. 

The film is listed in Golden Raspberry Award founder John Wilsons book The Official Razzie Movie Guide as one of the The 100 Most Enjoyably Bad Movies Ever Made. 

==See also==
;Other films released during the late 1970s disco craze:
* Saturday Night Fever (1977)
* Thank God Its Friday (1978)
* Skatetown, U.S.A. (1979) The Apple (1980)
* Xanadu (film)|Xanadu (1980)
* Cant Stop the Music (1980)
* Fame (1980 film)|Fame (1980)

==References==
 

==External links==
*  
*  
*  
*  
*   - Unofficial Fan Site
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 