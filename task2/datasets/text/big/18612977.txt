Doraemon: Nobita and the Windmasters
{{Infobox film
| name           = Nobita and the Windmasters
| image          = Nobita and the Wind Wizard.jpg
| caption        = Theatrical release poster
| director       = Tsutomu Shibayama
| producer       = Junichi Kimura Toshihide Yamada
| writer         =
| narrator       =
| starring       = Nobuyo Ōyama Noriko Ohara
| music          = Katsumi Horii
| cinematography = Toshiyuki Umeda
| editing        = Hajime Okayasu
| studio         = Asatsu
| distributor    = Toho Company
| released       =  
| runtime        = 84 mins.
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = $23,428,491/2,423,023,317.51 yen 
}}
  is a feature-length Doraemon film which premiered on March 8, 2003.

== Plot ==

When a mysterious tomb in a faraway land is opened, two strange spheres and the spirit of an ancient wizard are set free. The ancient wizard goes and takes the body of wolf. One of the spheres ends up in Nobitas neighborhood. Next one goes and ends up in wind village. As first one breaks open, an odd cyclone creature is set free. First, Suneo had got it and he tried to catch it and make his pet but he was unsuccessful. Meanwhile Nobita was eating ice cream to his way back home but unfortunately that odd cyclone snatched it. Nobita followed it and then the cyclone threw the ice cream at the head of Gian. Due to this he becomes angry and he tries to beat Nobita but he ends up with a crash in a trash box. then the cyclone followed Nobita and went inside home. It created a disaster in his home and also troubled Nobitas mom. Then he liked it so much that he wanted to keep it. While trying to take care of it, Nobita tried to hide it with his mom. So he made its appearance like doll in manga (a soft toy machine camera to make it as a toy. Then he takes it to Shizuka to show her. But Fuuko runs away. Then at last Nobita found it. Then he gives a name Fuko to it. He declared that Fuuko is a girl( female). As there was no space in the Nobitas neighbourhood, he and his friends with Doraemon go somewhere at Anywhere door but they are accidentally taken to a location hidden from the rest of the world: the Wind Village. There they discover different types of creatures and meet some incredible people. They enjoy there very much  and in evening they return from Anywhere door. They left Fuuko at Wind Village. Seeing the Anywhere door, the ancient wizard also comes through it. Then it takes the body of Suneo. Meanwhile, one of the villager from Wind Village discovers next sphere. But it flies away and goes to wizard. Next day Doraemon and his friends again visit Wind Village. They meet Storm Villagers and also the wizard in the appearance of Suneo. They get surprised. Then they attack them and in this process they capture Fuko and also steal Doraemons pocket. They trap Fuuko in a prison and they have enjoyment. Meanwhile Doraemon and his friends go to prison and free Fuko. But D gets back Doraemon and his pocket. There caused massive typhoons throughout the land. Doraemon and the others then join forces with the Wind Village to prevent the worst from happening. At this process, Fuuko loses her power and goes somewhere far. Nobita fell into a trap and reunited with Fuuko when he tried to help Shizuka when she fell. A yak advised Nobita to take care of Fuuko.One of the person of storm village was most wanted criminal of twenty-second century.He was an archaeologist.He controlled the dragon Mafuuku and trapped the wizard in a spirit capturing cage. Nobita took a heavy sword to thrash him but in vain. Then Fuuko went to the volcano to make herself to fire and went to the opposite direction (swirling herself.) She gave up her life, leaving the soft toy she wore. Nobita was in tears. He vowed that he wouldn’t forget her till his last breath. Then time patrol came to capture the crook. So he was captured by time patrol and Doraemon and his friends returned home.

== Cast ==
{| class="wikitable"
|-
! Character
! Voice
|-
| Doraemon
| Nobuyo Ōyama
|-
| Nobita Nobi
| Noriko Ohara
|-
| Shizuka Minamoto
| Michiko Nomura
|-
| Takeshi "Gian" Goda
| Kazuya Tatekabe
|-
| Suneo Honekawa
| Kaneta Kimotsuki
|-
| Fuko
| Mika Kanai
|-
| Temujin
| Rikako Aikawa
|-
| Sun
| Kumiko Nishihara
|-
| Temujin and Suns Mother
| Nana Yamaguchi
|-
| Tomujin
| Yūko Satō
|-
| Yamujin
| Yūjin Kitagawa
|-
| Kunjin
| Kōji Iwasawa
|-
| Kanjin
| Yōsuke Akimoto
|-
| Elder
| Takanobu Hozumi
|-
| Storm
| Yusaku Yara
|-
| Storms underlings
| Kazunari Tanaka Yasuhiro Takato Yukitoshi Hori Masashi Hirose Hajime Koseki Minoru Inaba
|-
| Uranda
| Kiyoshi Kobayashi
|-
| Yaku Osamu Kobayashi
|-
| Captain of Future Time Patrol
| Yōsuke Naka
|-
| TV Announcer
| Noritsugu Watanabe
|}

==References==
 

== External links ==
*    
*  

 
 

 
 
 
 
 