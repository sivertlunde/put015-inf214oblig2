The Trouble with the Truth (film)
The romantic drama film written and directed by Jim Hemphill. Starring John Shea and Lea Thompson as two divorcees meeting after their daughters (Danielle Harris) engagement, the film follows their conversation over dinner as they reassess their life and relationship. The film has drawn comparison with Richard Linklaters Before Sunrise and Before Sunset, and Louis Malles My Dinner with Andre for its minimalist plot, with Roger Ebert noting that "it is a very small movie with very deep feelings".  

==Cast==
*Lea Thompson as Emily
*John Shea as Robert
*Danielle Harris as Jenny
*Keri Lynn Pratt as Heather
*Rainy Kerwin as Staci
*Ira Heiden as Restaurant Host

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 


 