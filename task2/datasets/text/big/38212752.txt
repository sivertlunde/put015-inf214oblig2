Taxi! Taxi!
 

{{Infobox film
| name           = Taxi! Taxi!
| image          = Taxi! Taxi! Poster.jpg
| image_size     = 215px
| border         = Yes
| alt            = 
| caption        = Theatrical release poster
| director       = Kelvin Sng
| producer       = Chan Pui Yin
| writer         = Boris Boo Rebecca Leow Chan Pui Yin Kelvin Sng
| screenplay     = Boris Boo Lee Chee Tian Violet Lai
| story          = 
| based on       =   
| narrator       =  Mark Lee Gurmit Singh Jazreel Low Gan Mei Yan Lai Meng Chua Jin Sen (Pseudonym|a.k.a. Dr Jia Jia) Royston Ong
| music          = Alex Oh
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Golden Village Pictures
| released       =   
| runtime        = 93 minutes
| country        = Singapore Hokkien
| budget         = Singapore dollar|S$1 million 
| gross          = S$1.45 million 
}}
 Mark Lee, Gurmit Singh and YouTube personality Chua Jin Sen,  better known by his online handle "Dr. Jia Jia".  It is Chuas professional film debut. The film follows two fellow taxi-drivers (Lee and Singh) quest for self-discovery.  Distributed by Golden Village Pictures, the film was commercially released in Singapore on January 3, 2013    and slated for a January 24, 2013 release in Malaysia. 

==Plot== Mark Lee) mistaken him for him attempting suicide. This made Ah Tau to receive a fine for illegal parking.

After continuous attempts on finding a new job, Professor Chua See Kiat had no choice but to resort into cab driving. Ah Tau volunteered to help him and became his first passenger. Soon, Ah Tau was then fined again for another illegal parking.

Whilst Professor Chua is picking up his mother-in-law from the airport, he decides to use the taxi to go back home. Coincidentally the taxi driver was Ah Tau who brought his son, Jiajia, along. On the way, Ah Tau nags about Professor Chuas new career. Annoyed, Professor Chua shouted at him to shut up unwilling to allow his wife and his mother-in-law to know about his new career. This deteriorates Ah Tau and Professor Chuas friendship.

Professor Chua had encountered some hard time whilst being a cab driver. Once his passenger was a scientist working in his former lab. After dropping him, he encounters his former colleagues. Reluctant to let them know about his new job, he went of with another taxi and had his own taxi wheel clamped. He soon encountered some party-goers as for being his passengers when they soon refuse to pay their taxi fees and stole his phone. Furious, he chased off his passengers and ended up into getting more troubles with gangsters. Ah Tau helped him by recording the scene and got himself punched by the gangsters. The gangsters are then caught and Professor Chua gave Ah Tau a mobile phone to thank him. However he is yet to face his wife who was crying. He then explains the situation to her as she promised not to tell anyone about his new job.

Despite having the job mummed, Chuas son realised about it as Chua once picked he and his girlfriend up as one of their passengers. Soon the family had a brief scuffle and his son decided not to talk to him. Ah Tau had a similar situation for showing Jiajia edited pictures of his long lost mother when Jiajia realised that Ah Tau and Regina had both edited it. This made Jiajia not to talk to his father. The two shared their grievances and decided to help each other in the future. The next day, Chua realised that his mother-in-law left the house unattended. This allow him to understand the taxi culture. Ah Tau asked all taxi drivers to find her while Chua gave descriptions of her. It was successful. Chua also helped Ah Tau by developing his sons broken English and tried to find a suitable school for him.

While Ah Tau was set on a date with Regina, he was caught by a policeman (Chua En Lai) who gave a long, memorised sppech for doing an illegal U-turn). His taxi ran out of fuel and he was forced to take another taxi before being caught by the same policeman as he repeats the speech. Regina felt that Ah Tau isnt worth a wait and stomped her off on the streets, leading into a car accident. While she was in hospital, she found out that her eyesight is blurred and she is unable to fulfil her dream to be a fashion designer. This made her to almost attempt suicide on the hospitals roof. Chua explains the situation Regina is in to Ah Tau, who was in the middle of his sons performance. While he was on his way, Chua explains to Regina about his story on being  a cab driver, which then calms her down and lose her thoughts on committing suicide. It was recorded as his son watches it live.

Few years later, Chuas son began idolising his father again. While Chua regains his job as a professor, he still wants to stick into taxi driving. Regina became a DJ as she falls in love with Ah Tau. The film ends with Chua and Ah Tau driving off the roads.

==Cast== Mark Lee as Ah Tau, a veteran "Ah Beng" taxi driver
* Gurmit Singh as Professor Chua See Kiat,   a retrenched microbiologist 
* Royston Ong as Chuas son 
* Jazreel Low as Chuas wife.  Director Kelvin Sng stated that he chose her for the role as:

 

* Lai Meng as Chuas grandmother
* Chua Jin Sen as Jia Jia, Ah Taus son.  It is his film debut.  Sng said of him:

 

* Chua En Lai as a policeman
* Gan Mei Yan as Ah Taus tenant, Regina. 

==Production==

===Development===
Inspiration was largely drawn from real life blogger Cai Mingjies personal recounts in his 2010 best selling work, Diary of a Taxi Driver: True Stories From Singapores Most Educated Cabdriver.   Boris Boo, Lee Chee Tian and Violet Lai served as screenwriters. Boo, Rebecca Leow, Chan and Sng were credited with writing the story. Chua Jin Sen (better known as Dr Jia Jia)s involvement in the project kindled public interest. His participation in the film was first mentioned by his mother in June 2012, who told Yahoo! Singapore:

   }}

She also gave some details about his role, but a confidentiality agreement prevented her from stating more at that time.  Greater details about the film were disclosed at a July 2012 press conference, where director Kelvin Sng said  :

   }}

 

Taxi! Taxi! is the first on-screen pairing of Gurmit Singh and Mark Lee since their 2001 collaboration, One Leg Kicking.   Singh attributed this to the "lack of suitable scripts".  It is Singhs first feature-length film since Phua Chu Kang The Movie (2010).    Taxi! Taxi! also marks the comeback of former actress Jazreel Low. 

===Financing and filming===
Financers for Taxi! Taxi! included SIMF Management, Galaxy Entertainment, sglanded.net, Widescreen Media, RAM Entertainment and PMP Entertainment. With a budget of Singapore dollar|S$1 million,  production commenced on July 12, 2012.  Actual filming begun on July 16, 2012.  A particular scene required Chua to be shot from various angles, resulting in many takes. This reportedly made Chua feel frustrated. His mother said of the scenario:

   }}

==Reception==

===Box office===
Commercially released in Singaporean cinemas on January 3, 2013,  Taxi! Taxi! grossed S$592,000 in its opening weekend.  In total it took in $1.45 million, making it the second-highest grossing Singaporean film of 2013, after Ah Boys to Men 2.   

===Critical response===
Film magazine F ***   Raphael Lim dubbed the film as "run of the mill", giving it only 2 out of 5 stars.  Writing for my paper was Boon Chan, who gave the film a rating of 2.5. He wrote that "their   much-vaunted chemistry has been overhyped."   

==Awards and nominations==
Taxi! Taxi! was chosen as Singapores first official entry for the 1st Asean International Film Festival and Awards ( , Sarawak, Malaysia, from March 28–30, 2013. 

==See also==
 

==References==
 

 
 
 
 