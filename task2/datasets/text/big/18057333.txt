Beauty for the Asking
{{Infobox film
| name           = Beauty for the Asking
| image          =
| caption        =
| director       = Glenn Tryon
| producer       = B. P. Fineman
| writer         = Doris Anderson and Paul Jarrico (screenplay) Edmund L. Hartmann (story) Grace Norton and Adele Buffington (idea) Donald Woods Frieda Inescort
| music          = Frank Tours
| cinematography = Frank Redman
| editing        = George Crone
| distributor    = RKO Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Beauty for the Asking is a 1939 film drama produced by RKO Pictures, and starring Lucille Ball and Patric Knowles.

It tells the story of Jean Russell (Ball), who becomes romantically involved with a wealthy married man (Knowles) who later spurns her.    She later invents a new facial cream, and with the financial backing of her former lovers wife (Frieda Inescort), starts a business that makes her a millionaire.

==Reception==
RKOs pre-release publicity claimed that the film was to be an "exposé of the beauty racket" but reviewers of the day concluded that it was a standard "romantic love triangle". 

Recently Leonard Maltin has written favorably of the film, suggesting that the film offered an unusual feminist viewpoint for its time, and acknowledging that Ball delivered a strong performance. 

==Cast==
*Lucille Ball as Jean Russell
*Patric Knowles as Denny Williams Donald Woods as Jeffrey Martin
*Frieda Inescort as Flora Barton-Williams
*Inez Courtney as Gwen Morrison
*Leona Maricle as Eve Harrington
*Frances Mercer as Patricia Wharton
*Whitney Bourne as Peggy Ponsby
*George Beranger as Cyril (as George Andre Beranger)
*Kay Sutton as Miss Whitman, Jeans Secretary
*Ann Evers as Lois Peabody

==Notes==
 

==References==
*Jewell, Richard B. and Harbin, Vernon, The RKO Story, Octopus Books, London, 1982. ISBN 0-7064-1285-0
*Maltin, Leonard, Leonard Maltins 1998 Movie & Video Guide, Signet Books, 1997. ISBN 0-451-19288-5

== External links ==
*  

 
 
 
 
 
 