Singing Lovebirds
{{Infobox Film
| name           = Singing Lovebirds
| image          = Singing Lovebirds 1.jpg
| caption        = screenshot from the film
| director       = Masahiro Makino
| studio         = Nikkatsu
| writer         =
| starring       = Chiezō Kataoka Takashi Shimura
| music          = Tokujirō Ōkubo
| cinematography = Kazuo Miyagawa
| editing        = 
| distributor    = 
| released       = December 14, 1939
| runtime        = 69 min.
| country        = Japan
| awards         =  Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
| 
}}
  is a 1939 Japanese film directed by Masahiro Makino. It is a musical comedy. Fulfilling his reputation as a fast worker, Makino made the film in only two weeks when an opening was created in the production schedule of another film, Yaji Kita Dōchūki, after its star, Chiezō Kataoka, came down with appendicitis (Kataokas scenes in Singing Lovebirds were filmed in only a few hours).    The film, however, has become "the most frequently revived Japanese prewar musical film," featuring music ranging from jazz to joruri (music)|jōruri, and popular music stars like Dick Mine.   Makino made other musicals like Hanako-san (1943) and was known for his rhythmic style. Singing Lovebirds also features Takashi Shimura, most famous as the lead samurai in Akira Kurosawas Seven Samurai, in a singing role.

==Plot==
 
Oharu is the daughter of Kyōsai Shimura, a rōnin who now makes his living making umbrellas. She is in love with another rōnin, Reisaburō Asai, who lives next door, but he is being pursued by two of the town beauties, Otomi and Fujio. To make things even more difficult for Oharu, her father is obsessed with antiques, buying them even though he has little money and even when most of them eventually turn out to be fakes. A mistake, however, puts him deeply in debt to the local lord, Tanbanokami Minezawa, and he is confronted with having to sell Oharu in order to pay it off.

==Cast==
* Chiezō Kataoka as Reisaburō Asai, a rōnin
* Takashi Shimura as Kyōsai Shimura
* Haruyo Ichikawa as Oharu, Kyōsais daughter
* Dick Mine as Tanbanokami Minezawa
* Tomiko Hattori as Otomi
* Fujiko Fukamizu as Fujio

== References ==
 

== External links ==
 
*  

 

 
 
 
 
 
 


 