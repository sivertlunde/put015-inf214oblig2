The Adventures of Kathlyn
{{Infobox film
| name           = The Adventures of Kathlyn
| image          = The Adventures of Kathlyn.JPG
| alt            =  
| caption        = The Adventures of Kathlyn (1914 book)
| director       = Francis J. Grandon
| producer       = 
| writer         = Harold MacGrath Gilson Willets
| starring       = Kathlyn Williams
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Selig Polyscope Company
| released       =   
| runtime        = 
| country        = United States
| language       = silent film   English intertitles
| budget         = 
| gross          = 
}}
 serial released adventure serial filmed in Chicago, Illinois, its thirteen episodes were directed by Francis J. Grandon from a story by Harold MacGrath and Gilson Willets and starred Kathlyn Williams as the heroine. Harold MacGraths novel of the same title was released a few days later in January 1914 so as to be in book stores at the same time as the serial was playing in theatres.

The Adventures of Kathlyn was only the second serial ever made by an American film studio and considered the first of the cliffhanger serials that became enormously popular during the next decade.

The success of the serial spawned a 1916 feature-length film of the same title with basically the same crew and cast.

==Production== nickelodeons only 5 years previously, Tribune editor James Keeley agreed and the serial was released as a promotional project. The chapters of the film were released biweekly and the story was also printed as a newspaper serial in the Tribune and other newspapers including the Los Angeles Times. {{cite book
 | last = Stedman
 | first = Raymond William
 | title = Serials: Suspense and Drama By Installment
 | year = 1971
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-0927-5 
 | pages = 6–9
 | chapter = 1. Drama by Instalment
 }}  {{cite book
 | last = Lahue
 | first = Kalten C.
 | title = Continued Next Week 
 | year = 
 | publisher = 
 | isbn = 
 | pages = 6–8
 | chapter = 1. A Bolt From The Blue
 }} 
 

Although the first American film serial was What Happened to Mary, The Adventures of Kathlyn is a more important piece of film history, being the first serial to use cliffhangers as the ending of its chapters, and so became the first recognizable film serial.  Frank Leon Smith, in a letter to Films in Review (February 1958), wrote that the cliffhanger ending of chapter one "was a situation ending, but other episodes wound up with sensational action or stunts, broken for holdover suspense...gave the serial both the key to its success and the assurance of its doom." 

The Adventures of Kathlyn used animals from the Selig Zoo and had more action than What Happened to Mary?  The Tribune announced a 10% increase in circulation as a result of the film serials success.  

==Preservation status==
The film is now considered to be a lost film.  According to silentera.com, however, the La Cineteca del Friuli film archive has the first  episode and the EYE Film Institute Netherlands possesses print fragments. 

==Chapter titles==
# The Adventures of Kathlyn
# The Two Orphans
# The Temple of the Lion
# A Royal Slave
# A Colonel in Chains
# Three Bags of Silver
# The Garden of Brides
# The Cruel Crown
# The Spellbound Multitude
# A Warrior Maid
# The Forged Parchment
# The Kings Will
# The Court of Death

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 