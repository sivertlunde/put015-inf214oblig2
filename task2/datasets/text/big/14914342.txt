Vaana (film)
{{Infobox film
| name = Vaana
| image = vaana_poster.jpg
| writer = M. S. Raju Parachuri Brothers
| story = Yogaraj Bhat Suman Jayasudha Seetha Naresh Diganth
| director = M. S. Raju
| choreographer = M. S. Raju
| producer = M. S. Raju
| distributor =
| released =  
| runtime =
| cinematography = Sekhar V. Joseph
| language = Telugu
| music = Mano Murthy & Kamalakar
| country = India
| budget = 
}} Telugu film Suman plays a supporting role. M.S. Raju produces and directs this film while debutant, Kamalakar composed music for the project.  It is a remake of Kannada language film, Mungaru Male.  The film was dubbed and released in Tamil as Gnabagangal Thaalattum.

==Plot==
Abhiram (Vinay Rai) is a naughty and active guy. His parents (Jayasudha and Naresh (actor)|Naresh) enjoy his mischievous deeds. On a visit to a mall amidst a heavy wind, he spots a pretty girl, Nandini. Whilst staring at her, he inadvertently falls into a manhole. Nandini rescues him from the pit, but in the process loses her heart-shaped watch she had just bought.

While accompanying his mother to Araku, Abhi confronts a man named Jaanu (Ajay). Jaanu, who has been following Nandini, beats up Abhiram thinking that he is in love with Nandini. Abhiram, unaware that Jaanu has vowed not to allow any one near Nandini, trashes Jaanu and his gang in return.

In Araku, Abhiram meets Nandini unexpectedly. He identifies himself and expresses his love and offers to tie the watch as an indication of his intention to marry her. Nandini, who is already engaged, rejects his proposal. Still, Abhiram vows to marry Nandini if she meets him again. Abhis mother takes him to her friend Bharathi’s (Seetha (actress)|Seetha) house. Abhi discovers that his host in Araku, Col. Choudary (Suman (actor)|Suman) is Nandinis father is deaf and Nandinis marriage is just a week away. Dejected, Abhiram throws Nandinis heart-shaped watch away. But Nandini calls him over the phone and taunts him to return. Delighted, Abhiram goes in search of her watch and brings it back. While searching for it, he spots a rabbit, which he calls as Devadas and brings it along with him.

Since Nandinis friends are due to arrive from Mumbai for marriage, Abhiram takes Nandini to the railway station. The train from Mumbai is delayed by five hours, so Nandini and Abhiram decide to visit a nearby temple. While returning from the temple, Abhiram and Nandini are caught in rain. An old couple offers them shelter inside their hut. Abhiram, still in two minds about expressing his love to Nandini, grabs few toddy bottles, goes out in rain and starts drinking. However, when Nandini walks towards him, offering an umbrella, he is under a state of intoxication and tells Nandini that hed better stay away from Nandini to remain a decent boy, rather than to propose to or elope with her.

Nandini is now in love with Abhiram and is in a dilemma as her wedding is due in a few days. She requests him to take her to the top of a waterfall and expresses her love to Abhiram, standing at the edge of the waterfall.

Abhiram, intent on marrying Nandini, takes her father for a morning jog to discuss his marriage with Nandini. But Choudary, a heart patient, tells Abhiram that hes expected to die anytime and his only wish is to get Nandini married off to Gautam (Diganth). On the night before the marriage, Abhiram drives away from the house without taking Devadas. He then starts drinking in a bar. He finds Gautam, asking the bar-owner for directions to Choudarys home. When Jaanu tries to kill Gautam, Abhiram saves Gautam and convinces jolly that only Gautam is the best person to marry Nandini.

Next day, he drops Gautam at the marriage venue, just in time for the marriage. He then declines to attend Gautams marriage. Gautam asks for the heart-shaped watch as a remembrance but Abhiram does not agree to give it. Abhiram then leaves. Meanwhile, on the wedding day everyone is searching for Abhiram, but he is nowhere to be found. His mother is the only one who knows about his love and is worried of his whereabouts, but does not show her worry.

With a tear in his eyes, Abhiram watches the arch proclaiming "Gautham weds Nandini". As he is trying to leave, Nandini writes a letter and puts the heart shaped watch into it. She asks Abhiram whether he told their story to her father. But Abhiram  tells her that he doubted her love. Hurt, Nandini tells him not to show his face again. With tears in her eyes, she gets married to Gautam, thinking that she has been cheated by Abhiram.

Abhiram Leaves, taking her memories and Devadas with him. On the way, Abhiram goes to the waterfall where Nandini proposed to him. There Devadas dies. The story ends with Abhiram throwing him into the waterfall and returning to his house with all memories of his Love.

==Cast==
* Vinay Rai - Abhiram
* Meera Chopra - Nandhini Choudary Suman - Col. Choudary
* Jayasudha - Abhirams Mother Seetha - Bharathi Choudary Ajay
* Naresh - Abhirams Father
* Diganth - Gautham
* Dharmavarapu Subramanyam
* M. S. Narayana
* Krishnudu - Servant

==Credits==
* Rock Prabhu - action director

==See also==
* Mungaru Male
* Premer Kahini

==References==
 
 

 
 
 
 
 
 