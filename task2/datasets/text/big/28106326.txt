Creature (miniseries)
{{Infobox television film
| name           = Creature
| director       = Stuart Gillard
| producer       = Peter Benchley
| writer         = Peter Benchley (novel)  Rockne S. OBannon (teleplay)
| starring       = Craig T. Nelson Kim Cattrall Matthew Carey
| released       = May 17, 1998 - May 18, 1998
| runtime        = 240 minutes
| country        = United States
| language       = English
}}
 White Shark (re-published as Creature in 1997 concurrent with the film) by Jaws (novel)|Jaws author Peter Benchley.  The film is about an amphibious shark-like monster terrorizing an abandoned secret military base and the people who live on the island where it is located.

==Plot==
 
The film starts with Richland (Feore), who was sent to visit a government facility on a remote island. He follows Peniston (Esposito) into the facility. The head scientist Dr.Bishop (Reineke) shows them the projects he is working on, including a shark and dolphin hybrid with the ability to adapt to any environment, as well as a human and shark hybrid (the creature). It breaks free from its tank and kills Dr. Bishop. Richland orders Peniston to kill it, but Peniston instead traps the Creature in a containment unit and dumps it in the sea.

25 years later...

Dr. Chase (Nelson), a marine biologist, and his assistant, Tall Man (Williams), find a pregnant female Great White Shark trapped in Ben Medieras (Alyward) nets. Dr. Chase sets it free. Meanwhile, a local fisherman, Puckett (Burke), retrieves the containment unit and accidentally releases the creature. Mediera is later eaten by the creature. Dr. Chase & Tall Man meet Dr. Macy (Cattrall), her sea lion named Robin, and Dr. Chases son, Max (Carey), at the airport. As Dr. Chase & Dr. Macy prepare to out on the ocean, Max goes into town. He meets the native kids including Elizabeth (Echikunwoke), the chiefs daughter. The local boys tell him hell have to take the test of bravery and jump from a tall cliff. 

Medieras body is found and police Chief Gibson asks Dr. Chase to inspect the body. Chase says even though there is a great white out there it is not the culprit and tells the chief not to let people go after the innocent shark. Dr. Chase and Dr. Macy leave and prepare to go out and find the real culprit. Tall Man arrives, escorted by his girlfriend, Tauna (Michele). Peniston, who has gone crazy and is called Werewolf by the locals, sees the empty containment unit after Puckett brings it ashore. He realizes Mediera was not killed by a shark. Dr. Chase and Dr. Macy are unable to find the Creature and return to Dr. Chases headquarters (formerly the research facility where the creature was born). Dr. Macy reveals Dr. Chase studies why sharks do not get cancer, which his best friend died from. 

Meanwhile the kids take turns jumping of the cliff, unaware the creature is lurking right below them. Max and another boy named Kimo jump in. The creature mauls Kimo to death, but Max escapes. When Kimos body is found, the chief is furious and, no longer listening to Dr. Chase, orders all the fisherman to hunt for the great white. The islanders, including Puckett, accuse Chase and Macy of performing witchcraft on the shark. Dr.Chase, Dr. Macy, Max and Tall Man search for the Creature again; this time with success. The creature attacks their boat, but they escape. By this time, Puckett has killed the great white and the islanders celebrate, believing the danger is past. Dr. Chase tells the chief that they are not safe and that he has seen the Creature. However, the chief refuses to believe him. They do research at Dr. Chases headquarters, and are soon put into contact with Richland. Richland tells Dr. Chase not to do anything until he arrives. Dr. Chase argues that more people could die if they do not take action but Richland does not care. Richland and a troop of NAVY SEALs board a helicopter and set off for the island. Dr. Chase ignores Richlands warning and goes back out into his boat. Peniston uses a horn to attract the creature, which is attracted to noise. The creature attacks him, but Dr. Chase and friends save him. They bring him back to their headquarters. 

The next day Dr. Chase discovers claw marks on his boat (proof of his theory of the Great Whites innocence). Later, Peniston shows them a secret tunnel leading to the actual research facility. Meanwhile, Richland and the SEALs approach the island in their helicopter. Dr. Chase and Dr. Macy go down the tunnel and discover it to be half flooded, most likely connected to the ocean. They also discover equipment and notes. They find some of Pucketts traps and wonder how they got there. By the time they realize who brought them, it is too late: the creature ambushes them. They run and the creature gives pursuit, but is only able to go so far until it cant catch up because its arms and legs are short and weak out of water. It stops and evolves its arms and legs until they are like that of a human. The creature resumes chase. The doctor and her friends manage to escape and reseal the tunnel blocking the creature. They leave the research facility by boat. The creature attacks them when they reach the dock. It is about to kill Max, but then lets him live. 

Dr. Macy goes to the town doctor and says that she has claw wounds. Dr. Chase discusses the encounter with Chief Gibson, but he still refuses to believe. They decide to send Max back to the States. Elizabeth stops him and says that she cant find her cousin. Elizabeth claims the only place she could be is at the old rum factory (which is now used as a club and place for teenagers to go). They then leave to find it. Puckett goes diving and is killed by the creature. Dr. Chase, Dr. Macy, and Peniston return to headquarters only to be ambushed by Richland and his thugs. He sends a few of his cronies to explore the tunnels. Meanwhile, Max & Elizabeth find the old rum factory out in the swamp. They enter, but dont find her cousin, only Tauna. They head outside to look in the swamp when suddenly a mans head floats to the surface. Max recognizes it as Pucketts. They run away, but the creature jumps out of a tree and chases them. 

Dr. Chase examines the stuff in the tunnel but Richland destroys it and threatens Chase. The kids make it to Taunas house and hide and contact Dr. Chase. Before he can make it back, the creature attacks the house. When he arrives, he finds the house in ruins and believes the children are dead, but they come out of their hiding places. Chief Gibons soon arrives and Elizabeth tells him that the creature exists. He finally accepts the beasts existence. The Chief and Richland lead a hunt for the creature in the swamp. Dr. Chase goes with them and exposes Richland as one of the people present at the original event. He says how Richlands cover-up put people at risk. Richland threatens to kill him, but the Chief stops him. The creature attacks and kills Richlands men. The Chief falls into the water. But instead of saving him, Richland just watches hoping the creature will attack the Chief so he can kill it and finally settle his score. Dr. Chase rescues the Chief. The creature obliterates the bridge where Richland is standing. He falls into the water where the creature eats him. Dr. Chase and the Chief escape and make it back to town the next day. 

There, the chief is reunited with Elizabeth and finally understands and accepts Dr. Chase as a good man. Dr. Chase, Tall Man, Max, and Dr. Macy return to headquarters where Peniston is waiting. Dr. Macy realizes he was there. She angrily asks him why he did not kill the creature. Peniston replies that it was because they used his blood to help create it and that the creature is a part of him. They use Robin with a camera attached to her back to locate the creature. It locates the beasts lair, but before they can retrieve her, the creature attacks and the camera goes blank. Tall Man & Peniston enter the tunnel shortly after. Tall Man shoots at the creature but Peniston interferes. The creature injures Tall Man. 

Dr. Chase arrives and helps Tall Man escape, but are pursued by the creature. The creature quickly gains the upper hand and almost kills Chase. Max uses Penistons sound device to distract the creature. It attacks him, but he uses a zipline to escape. Peniston takes the sound device and lures the creature into the pressure chamber. Dr. Chase urges Peniston not to stay in the pressure chamber with the creature, but Peniston refuses to leave saying he cant let the creature die alone. Dr. Chase locks them in and activates it, building greater and greater pressure. The creature kills Peniston. Chase allows the pressure to build up very high then smashes the window causing all the air to come out. This causes explosive decompression and the creature explodes, finally killing it. Later, the survivors walk into the sun set ready to board the boat, presumably to go back to town and never return to the island.

==Cast and characters==
*Craig T. Nelson as Dr. Simon Chase. He is a scientist who studies sharks. He is the first person to realize the creature is not an average shark.
*Kim Cattrall as Dr. Amanda Macy, ex-wife/friend of Dr. Chase
*Colm Feore as Adm. Aaron Richland, a government agent who attempts to kill the creature. He was there when the creature escaped and was attacked. He has a personal vendetta against the creature. He is eaten by the creature.
*Cress Williams as Tall Man, Dr. Chases assistant and close friend.
*Michael Reilly Burke as Adamowicz "Adam" Puckett, an ambitious fisherman. He has a rivalry with Dr. Chase. He found the containment unit and unkowingly released the creature. He was killed by the creature while scuba diving. They later find his head floating in the water at a nearby swamp.
*Blu Mankuma as Chief Rollie Gibson, the chief of police on the island. He is Elizabeths father and dislikes Dr. Chase.
*Michael Michele as Tauna, Tall Mans girlfriend.
*Matthew Carey as Max Chase, Dr. Chases son.
*Megalyn Echikunwoke as Elizabeth Gibson, the chiefs daughter and Maxs girlfriend.
*John Aylward as Benjamin "Ben" Madiera, a mean fisherman on the island. He is eaten by the creature.
*Giancarlo Esposito as Lt. Thomas Peniston, he was the assistant to Dr. Bishop. He did not kill the creature when he was supposed to. The guilt of letting it live and the fear of it returning while yet feeling a connection to it drives him insane and calls himself "Werewolf." In the end, he sacrifices his life to save the others, while luring the creature to its death. 
*Gary Reineke as Dr. Ernest Bishop, the man who created the creature. It eats him in the beginning of the film.

==References==
 
 

==External links==
* 
 
 

 
 
 
 
 
 
 
 
 
 