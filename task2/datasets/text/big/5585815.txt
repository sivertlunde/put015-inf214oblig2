Mili (1975 film)
{{Infobox film 
| name           = Mili 
| image          = Mili75.jpg
| director       = Hrishikesh Mukherjee
| producer       = Hrishikesh Mukherjee N.C. Sippy
| writer         = Bimal Dutta Dr. Rahi Masoom Reza Mohini N. Sippy
| narrator       = 
| starring       = Ashok Kumar Amitabh Bachchan Jaya Bachchan Asrani Aruna Irani Parikshat Sahni
| music          = Sachin Dev Burman
| cinematography = Jaywant Pathare
| editing        = Hrishikesh Mukherjee
| studio         = Mohan Studios
| distributor    = 
| released       = 1975
| runtime        = 124 minutes
| country        = India
| language       = Hindi
| awards =
| budget = 
}} 1975 Bollywood Telugu in 1976 as Jyothi (1976 film)|Jyothi starring Jayasudha in the title role. 

==Plot==
Mili (Jaya Bhaduri) is a story about a girl who suffers from pernicious anemia, a disease considered untreatable during the period the film was produced. Her lively, inquisitive and cheerful demeanour spreads happiness in everyones life. She becomes an inspiration to her new neighbour Shekhar (Amitabh Bachchan) who is a drunkard and always depressed. With her cheerful ways she changes Shekhar and he falls in love with her, unaware of her ailment. When he comes to know about it, he thinks of going away as he cannot bear to see her die. A reproach from a neighbour makes him reconsider his decision. As he loves the girl, he offers to marry her and take her abroad for her treatment. The film begins and ends with a scene of a jet aircraft taking off, ostensibly carrying the couple to Switzerland where they hope to find a cure.

==Cast==
*Amitabh Bachchan  as  Shekhar Dayal 
*Jaya Bhaduri  as  Mili Khanna 
*Ashok Kumar  as  Mr. Khanna, Milis father
*Usha Kiran  as  Sharda Khanna 
*Suresh Chatwal  as  Ranjeet Khanna
*Shubha Khote  as  Neighbour 
*Asrani  as  Drunkard, guest Appearance
*Aruna Irani  as  Runa Singh 
*Rajnath 
*Naina Apte 
* Aarti
*Shahana 
*Chandra
*Arabind 
*Nandita Aras

==Music==
{{Track listing
| headline     = Songs
| extra_column = Singer(s)
| all_music    = S.D. Burman
| all_lyrics   = Yogesh (lyricist)|Yogesh.
| title1 = Aaye Tum Yaad Mujhe | extra1 = Kishore Kumar | length1 = 06:10
| title2 = Badi Sooni Sooni Hai | extra2 = Kishore Kumar | length2 = 05:07
| title3 = Maine Kaha Phoolon Se | extra3 = Lata Mangeshkar | length3 = 04:41
}}

==Awards==

 

==Awards==
 
|- 1976
| Amitabh Bachchan 
| BFJA Awards for Best Actor (Hindi)
|  
|-
| Jaya Bachchan
| Filmfare Award for Best Actress
|  
|}

==References==
 

== External links ==
*  

 

 
 
 
 
 