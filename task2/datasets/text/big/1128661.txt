Tears of the Sun
 
{{Infobox film
| name           = Tears of the Sun
| image          = Tears of the Sun movie.jpg
| image_size     = 230px
| caption        = Theatrical release poster
| director       = Antoine Fuqua
| producer       = Ian Bryce Mike Lobell Arnold Rifkin
| writer         = Alex Lasker Patrick Cirillo
| starring       = Bruce Willis Monica Bellucci Cole Hauser  Peter Mensah
| music          = Hans Zimmer
| cinematography = Mauro Fiore
| editing        = Conrad Buff
| studio         = Revolution Studios Cheyenne Enterprises
| distributor    = Columbia Pictures
| released       = March 7, 2003
| runtime        = 121 minutes 142 minutes  
| country        = United States
| language       = English
| budget         = $75 million 
| gross          = $86.5 million    
}}
 action War war drama civil war in Nigeria. LT A.K. Waters (Bruce Willis) commands the team sent to rescue United States|U.S. citizen Dr. Lena Fiore Kendricks (Monica Bellucci) from the civil war en route to her jungle hospital. The film was directed by Antoine Fuqua.

Willis produced Tears of the Sun through Cheyenne Enterprises, his production company, and took the title from an early sub–title for Live Free or Die Hard, the fourth film in the Die Hard series. He filmed the sequel on the condition that he could use its sub-title for his SEALs war film.  The cast of Tears of the Sun features refugees portrayed by true African refugees living in the United States.

==Plot==
  Johnny Messner), Captain Bill Rhodes (Tom Skerritt) to Nigeria to extract a "critical persona", one Dr. Lena Fiore Kendricks (Monica Bellucci), a U.S. citizen by marriage. Their secondary mission is to extract the mission priest and two nuns.

The mission begins as planned. Waters tells Dr. Kendricks of the company of rebel soldiers closing on her hospital and the mission, and that the teams orders are to extract U.S. personnel; however, Kendricks refuses to leave without the patients. Waters calls Captain Rhodes for options; after their short and ambiguous conversation, he concedes to Dr. Kendricks that they will take those refugees able to walk. She begins assembling the able-bodied for the   hike; the priest and the nuns stay behind to take care of the injured. Irritated and behind schedule, the team and the refugees leave the hospital mission after daybreak.

At nightfall they take a short break. Guerrilla rebels rapidly approach their position, and Waters stealthily kills a straggling rebel. Dr. Kendricks warns Waters that the rebels are going to the mission, but he is determined to carry out his orders, and they continue to the extraction point. When they arrive, Waters initial plan becomes clear: the SEALs suddenly turn away the refugees from the waiting helicopter. Waters forces Dr. Kendricks into the helicopter, leaving the refugees stranded in the jungle, unprotected against the rebels. En route to the aircraft carrier, they fly over the original mission compound, seeing it destroyed and all its occupants murdered, as Dr. Kendricks had predicted. Remorseful, Waters orders the pilot to return to the refugees. He then loads as many refugees as he can into the helicopter instead and decides to escort the remaining refugees to the Cameroon border.

During the hike to the border, using satellite scans, they discover the rebels are somehow tracking them. As they escape and evade the rebels, the team enters a village whose inhabitants are being raped, tortured, and massacred by rebel soldiers. Aware of having the opportunity to stop it, Waters orders the team to take down the rebels. The team is emotionally affected by the atrocities they see the rebels have committed against the villagers.

Again en route, Slo determines that a refugee is transmitting a signal allowing the rebels to locate them. The search for the transmitter reveals the presence of Arthur Azuka (Sammi Rotibi), surviving son of deposed President Samuel Azuka, which they realize is the reason the rebels are hunting them. Samuel Azuka was not only the president of the country, but also the tribal king of the Igbo. As the only surviving member of this royal bloodline, Arthur is the only person left with a legitimate claim to the leadership of Nigeria. A newer refugee picked up during the trek is discovered with the transmitter on his person. He attempts to run but is shot. Waters is angry at Dr. Kendricks, because she knew all along about Arthur, yet never informed him.

The team decides to continue escorting the refugees to Cameroon, regardless of the cost. A fire fight ensues when the rebels finally catch up with the SEALs, who decide to stay behind as rearguard to buy the refugees enough time to reach the border safely. Zee calls the   for air support; two Boeing F/A-18 Hornet|F/A-18A Hornets take off and head for the fire fight. The rebels kill Slo, Flea, Lake, and Silk. Waters, Red, Zee and Doc are wounded, but direct the fighter pilots on where to attack. Arthur and Dr. Kendricks are scrambling to the Cameroon border gate when they hear the fighter jets approach and bomb the entire rebel force.

Waters, Zee, Doc, and Red rise from the grass as U.S. Navy helicopters land in Cameroon, opposite the Nigerian border fence gate. Captain Rhodes arrives and orders the gate open, letting in the SEALs and the refugees. A detail of United States Marine Corps|U.S. Marines then escort the SEALs to some helicopters. Captain Rhodes promises Waters that he will recover the bodies of his men. Dr. Kendricks says farewell to friends and flies away in the same helicopter with Waters.

The finale shows the refugees recognizing Arthur Azuka as tribal chief and bearer of his fathers democratic dreams for Nigeria. He raises his arm exclaiming "Freedom!" as everyone celebrates around him.  Edmund Burkes quote "The only thing necessary for the triumph of evil is for good men to do nothing" serves as the films epilogue.

==Cast==
* Bruce Willis as Lieutenant A.K. Waters: Team Commander/Rifleman
* Monica Bellucci as Dr. Lena Kendricks
*   Machine Gunner
*  /Grenadier  Johnny Messner as GMC(SEAL/SW) Kelly Lake: Lead Scout/Rifleman
* Nick Chinlund as OS1(SEAL) Michael "Slo" Slowenski: Automatic Rifleman (SAW)
* Charles Ingram  as MA2(SEAL) Demetrius "Silk" Owens: Sniper
* Paul Francis as HM2(SEAL/SFC Saucedo, Christian) Danny "Doc" Kelley: Corpsman/Grenadier
* Chad Smith as STG2(SEAL) Jason "Flea" Mabry: Designated Marksman
* Peter Mensah as Terwase
* Tom Skerritt as Captain Bill Rhodes
* Malick Bowens as Colonel Idris Sadick
* Akosua Busia as Patience
* Sammi Rotibi as Arthur Azuka
* Lloyde Del Mundo as General David Norton

==Production== USS Harry Navy Reserve Naval Air Station New Orleans.

The shooting was touched by misfortune when actor  . He decided to walk about the Central China Television film studio grounds, and climbed a prop tower in a set of another film, lost his footing, and fell approximately three stories, severely injuring his head. He was taken to a hospital, then transferred to Beijing. He lapsed into a coma and was on life support for ten days, until it was discontinued. He died on February 15, 2002, without regaining consciousness.

==Reception==
Tears of the Sun received mixed to negative reviews; the movie review aggregation websites Rotten Tomatoes and Metacritic record average favorable review ratings of 33% and 45% respectively.        Roger Ebert, however, gave the film three stars out of four and said "Tears of the Sun is a film constructed out of rain, cinematography and the face of Bruce Willis. These materials are sufficient to build a film almost as good as if there had been a better screenplay." 

==See also==

* List of films featuring the United States Navy SEALs

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 