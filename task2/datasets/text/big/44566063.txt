Three of a Kind (1944 film)
 
{{Infobox film
| name           = Three of a Kind
| image          = 
| caption        = 
| director       = D. Ross Lederman
| producer       = 
| writer         = Earle Snell Arthur Caesar
| starring       = Billy Gilbert
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
}}

Three of a Kind is a 1944 American comedy film directed by D. Ross Lederman.   

==Cast==
* Billy Gilbert as Billy
* Shemp Howard as Shemp Howard
* Max Rosenbloom as Maxie (as Maxie Rosenbloom)
* Helen Gilbert as Belle Collins
* June Lang as Delores OToole Robert Henry as Jimmy Collins (as Buzzy Henry) Paul Phillips as Paul Collins
* Wheeler Oakman as Oliver
* Franklin Parker as McGinty
* Marie Austin as Short Singer Sheila Roberts as Gilberts Stooge Robert McKenzie as Pawnbroker (as Bob McKenzie)
* Syd Saylor as Customer
* Harris Ashburn as Radio Rascal
* Jimie Haine as Radio Rascal (as Jimmie Haine)
* Dick Carlton as Radio Rascal
* Frank Jaquet as Judge
* Milton Kibbee as Welfare Worker

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 