The Zoo Gang (film)
{{Infobox film|
| name           = The Zoo Gang
| image          =  John Watson  John Watson
| producer       = Pen Densham Jason Watson
| starring       = Robert Jayne Tiffany Helm Jason Gedrick Jackie Earle Haley
| music          = Patrick Gleeson
| cinematography = Robert C. New
| editing        = Martin Nicholson James R.Symons 
| studio = Hersch  Insight Film Group
| distributor    = New World Pictures(1985)(USA)(theatrical) Starmaker Video(1996)(USA)(VHS) Highlight Video Transvídeo(Brazil)(video)
| released       = May 15, 1985
| runtime        = 96 mins.
| country        =   English
| awards         =
}} American teen film.

==Summary==
Tired of having nowhere to go for fun, Kate (Tiffany Helm), her brother Ricky (Robert Jayne), and friends Bobbi (Gina Battist), Danny (Eric Gurry) and Val (Marc Price) lease a dilapidated nightclub called The Zoo from an old drunk named Leatherface (Ben Vereen). The Zoo becomes their home away from home and an overnight success until the Donnelly Clan catches wind of it. 

Little Joe Donnelly (Jackie Earle Haley) and his brawny, but dimwitted twin brothers are a walking, talking trio of nuisance; where they go, trouble is sure to follow. And when they enter The Zoo ripe for a fight, a fight is just what they get. They have pushed The Zoo Gang too far and the battle for the club is on. Will the Gang be able to save their new found club from the greedy hands of the Donnelly clan? 
== Cast ==
* Tiffany Helm as Kate Haskell
* Jason Gedrick as Hardin
* Eric Gurry as Danny
* Marc Price as Val
* Gina Battist as Bobbi
* Robert Jayne as Ricky Haskell
* Ben Vereen as The Winch
* Jackie Earle Haley as Little Joe
* Ramon Bieri as Pa Donnely
* Darwyn Swalve as Goose
* Glen Mauro as Twin #1
* Gary Mauro as Twin #2
* Ty Hardin as Dean Haskell
* Tiny Wells as Hank
* Ramon Chavez as Cop
* William Reynolds as Fire Chief

==Rating==
The Zoo Gang was the first film to earn a PG-13 rating (though Red Dawn   would be the first released).

==Awards==
{|class="wikitable" style="font-size: 90%;" border="2" cellpadding="4" background: #f9f9f9;
|- align="center"
! style="background:#B0C4DE;" | Year
! style="background:#B0C4DE;" | Group
! style="background:#B0C4DE;" | Award
! style="background:#B0C4DE;" | Nominee
|- 1987
|Exceptional Performance by a Young Actor Starring in a Feature Film - Comedy or Drama Young Artist Award Robert Jayne-Nominated
|}

==References==
 

==External links==
 

 
 
 
 
 
 
 


 