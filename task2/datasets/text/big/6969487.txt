Marlowe (film)
{{Infobox Film
| name           = Marlowe
| image          = MarlowePoster943.jpg
| image_size     = 225px
| caption        = Theatrical release poster
| alt            = 
| director       = Paul Bogart
| producer       = Sidney Beckerman Gabriel Katzka
| screenplay     = Stirling Silliphant
| story          = Novel  
| narrator       = 
| starring       = James Garner Gayle Hunnicutt Carroll OConnor Rita Moreno Bruce Lee
| music          = Peter Matz
| cinematography = William H. Daniels
| editing        = Gene Ruggiero
| distributor    = Metro-Goldwyn-Mayer
| released       =  : October 22, 1969
| runtime        = 96 minutes
| country        = United States English
| budget         = 
}}
Marlowe (1969) is a neo-noir movie starring James Garner as Raymond Chandlers private detective Philip Marlowe. Directed by Paul Bogart, the mystery film was written by Stirling Silliphant based on Chandlers 1949 novel The Little Sister.

The supporting cast includes Bruce Lee, Gayle Hunnicutt, Rita Moreno, Sharon Farrell, Carroll OConnor and Jackie Coogan. 
 Jim Rockford in The Rockford Files.
 In the Route 66 Naked City.

This movie introduced martial arts legend Bruce Lee to many American film viewers.

The films title song "Little Sister" (named after the novel from which the film is derived) is provided by the group Orpheus (band)|Orpheus.

==Plot==
Los Angeles private-eye Philip Marlowe is trying to locate the brother of his new client, a woman named Orfamay Quest. The trail leads to two men who deny any knowledge of the brothers existence. Both are soon killed by an ice pick, so Marlowe deduces that there is much more to this than a simple missing-person case.

Marlowes path crosses that of a blackmailed movie star, Mavis Wald, and her friend, exotic dancer Dolores ("With an O"). A mobster sends Kung Fu expert Winslow Wong to bust up Marlowes office and warn him off the case, while Lieutenant French also cautions the detective to stay out of the polices way.

Hand-to-hand combat between the martial artist and detective leads to Wongs plummeting to his death off a balcony. Several more die along the way in a case that leads to a final shootout during a striptease.

==Cast==
* James Garner as Philip Marlowe
* Gayle Hunnicutt as Mavis Wald
* Carroll OConnor as Lt. Christy French
* Rita Moreno as Dolores Gonzáles
* Sharon Farrell as Orfamay Quest
* William Daniels as Mr. Crowell
* H.M. Wynant as Sonny Steelgrave
* Jackie Coogan as Grant W. Hicks
* Kenneth Tobey as Sgt. Fred Beifus
* Bruce Lee as Winslow Wong
* Christopher Cary as Chuck
* George Tyne as Oliver Hady
* Corinne Camacho as Julie Paul Stevens as Dr. Vincent Lagardie
* Roger Newman as Orrin Quest
* Read Morgan as Gumpshaw

==Critical reception==
The staff at Variety (magazine)|Variety magazine gave the film a mixed review and wrote,
 

Critic Roger Ebert criticized parts of the film in his review, writing,
 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 