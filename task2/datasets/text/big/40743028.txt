Unmanned: America's Drone Wars
 
Unmanned: Americas Drone Wars is a feature-length documentary film released by Robert Greenwald and Brave New Films in October, 2013,       investigating the impact of U.S. drone strikes in Pakistan and elsewhere.    

== Synopsis ==
 drone in 2011;      and school teacher, Rafiq ur Rehamn, whose mother was killed and children hospitalized due to a drone strike in 2012.     
Unmanned includes more than seventy interviews.   Prominent among these are a former American drone operator;     Pakistani families of drone victims who are seeking legal redress;  high ranking politicians and some of the military’s top brass,   warning against blowback from the loss of innocent life.   
With the intent of humanizing those who have been impacted by US drone policy, Unmanned intersperses these interviews with never-before-seen footage from tribal regions in Pakistan.     
The film claims that covert military drone strikes are often imprecise, and result in creating more enemies for the American people, who have little knowledge of how drone victims are targeted and killed. 

==Release==

Unmanned premiered theatrically in New York City on October 30, 2013.   In keeping with Brave New Films unique distribution model, it was also made available online for free.  

==Congressional Briefing==
 Reprieve brought survivors of US drone strikes to Capitol Hill to testify at a Congressional briefing.  
The initial response to Unmanned was positive.  Michael Moores website said, "It should be required viewing in all schools and homes in the United States.   The Front Page Online calls it "ardently compelling." 

==Interviewees==

Interviewees include:
*Lawrence Wilkerson, former Chief of State to Secretary of State Colin Powell
*David Kilcullen, former advisor to NATO and to General David Petraeus
*Medea Benjamin, Co-Founder, Code Pink
*Jemima Khan, Associate Editor, New Statesman 
*Imran Khan, Chair of the Pakistan Tehreek-e-Insaf Reprieve
*Pratap Chatterjee, Investigative Journalist
*Andrew Bacevich, Professor of International Relations and History at Boston University
*Mark Mazzetti, National Security Correspondent The New York Times and Author, The Way of the Knife
*Scott Shane, National Security Reporter, The New York Times
*Glenn Greenwald, National Security Columnist, The Guardian
*Karen DeYoung, Senior National Security Correspondent, The Washington Post 
*USAF Col. Morris Davis (Ret.), Former Lead Prosecutor For Guantanamo Bay
*David D. Cole, Professor, Georgetown University Law
*James Cavallaro, Stanford Law Professor
*Akbar Ahmed, Former Pakistani Ambassador to the UK, and Chair of Islamic Studies at American University
*Pir Zubair Shah, Council on Foreign Relations (Former)
*Philip Alston, United Nations, Former Special Rapporteur on Extrajudicial Executions
*23 more scholars, journalists, lawyers, public figures and activists
*Dozens of Pakistani survivors of drone strikes, and family members of those who did not survive

== See also ==

* List of films featuring drones

== References ==

 

== External links ==

*  
 

 
 