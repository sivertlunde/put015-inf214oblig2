5 to 7
{{Infobox film
| name           = 5 to 7
| image          = 5 to 7 film poster.jpg
| caption        = Film poster
| director       = Victor Levin
| producer       =  
| writer         = Victor Levin
| narrator       = 
| starring       =  
| music          = Danny Bensi Saunder Jurriaans 
| cinematography = Arnaud Potier
| editing        = Matt Maddox
| studio         = Demarest Films Mockingbird Pictures
| distributor    = IFC Films
| released       =  
| runtime        = 95 minutes
| country        = U.S.
| language       = English French
| budget         = 
| gross          = 
}}

5 to 7 is a 2015 American film directed by Victor Levin and starring Olivia Thirlby, Anton Yelchin, Bérénice Marlohe, Eric Stoltz, Glenn Close, Lambert Wilson, and Frank Langella.

== Cast ==
* Anton Yelchin as Brian Bloom
* Glenn Close
* Frank Langella
* Bérénice Marlohe as Arielle
* Lambert Wilson
* Olivia Thirlby
* David Shannon as Jim Sheehy

==Plot==
A writer (Yelchin) meets an older woman (Marlohe). Her open marriage with a diplomat (Wilson) only allows them to meet in the late afternoon.

== Production ==
On June 17, 2014, IFC Films acquired the US rights to the film. 

=== Filming ===
Filming began on May 20, 2013 in New York City. 

== References ==
 

== External links ==
*  
* 

 
 
 
 
 


 