Chanthupottu
{{Infobox film
| name           = Chanthupottu
| image          = chanthupottu2.jpg |caption=The music CD cover Dileep Gopika Lal Bhavana Bhavana Indrajith Indrajith
| director       = Lal Jose
| writer         = Benny P Nayarambalam Lal
| distributor    = Lal Release & PJ Entertainments
| editing        = Ranjan Abraham
| cinematography = Azhagappan
| released       =  
| country        = India
| language       = Malayalam Vidyasagar
| budget         =
| gross          =
}}

Chanthupottu is a 2005 Malayalam film directed by Lal Jose, written by Benny P. Nayarambalam, and produced by Lal (actor)|Lal. The story is about a man named Radhakrishnan (Dileep (actor)|Dileep) who was brought up like a girl by his grandmother. This film was a super hit at the box office.

==Plot==

Radhakrishnan (Dileep (actor)|Dileep) is brought up like a girl by his grandmother (Sukumari) who wanted a granddaughter. There by, he becomes an effeminate. She calls him Radha, which becomes his nickname. Radhas father Divakaran (Lal (actor)|Lal) goes to jail for a murder that he accidentally commits. Radha is ridiculed among the people in the village but he is not worried and spends time with the girls singing and teaches dancing. His best friend is Malu (Gopika) who is wooed by Kumaran (Indrajith Sukumaran|Indrajith), a local money lender and the son of the man whom Radha’s father had killed.

Divakaran comes back from jail and is shocked to see that his only son is a good for nothing transvestite, but can do nothing about it. Slowly Radha’s liking for Malu turns into love and when Kumaran sees it, he beats up Radha with the help of her father (Rajan P. Dev), a local astrologer and dumps him in deep sea. But he is saved by Freddy (Biju Menon), a restaurant owner, in some distant shore. Freddy takes him to the formers native where he is living with his sister Rosie (Bhavana (actress)|Bhavana) and his grandmother (Valsala Menon), who is a mental patient due to the shock of the sudden death of Freddys other sibling, Jonfy. He soon becomes a part of their family, as the grandmother begins to identify him as the late Jonfy.  He also discovers the manly element in him and starts to transform from his female mannerisms slowly.

Once, he gets involved in a fight with Cleetus (Sreejith Ravi), an old enemy of Freddy, after Cleetus tries to molest Rosie. During the fight, Cleetus gets severely injured on the head. Radha is forcibly, but sadly circumcised to escape from the police on Freddys shore.

On reaching his native shore, he discovers that his family, along with his house was brutally burned down by Kumaran. He also learns that Malu is pregnant with Radhas child. His arrival follows a fight with Kumaran. Towards the end of the fight, Radha defeats Kumaran and is about to kill him. But he is reminded of how his  father had to suffer in jail due to murder charges, and so he spares Kumaran. In the meantime, Malu  prematurely gives birth to Radhas son. However, when Radha sees the child, he vows to raise him as a boy, ripping off the ribbon tied to his hair.

==Characterisation of Radha==
  feminine name. His parents dont intervene, probably fearing the wrath of the grandmother. This induces a kind of femininity in him, even his mannerisms closely resembles women. To add to the situation, his father goes to jail leaving Radha to grow up with his femininity and transvestism. He, instead of going fishing like other men, teaches dance to girls. He is constantly in the company of girls; he plays their games and also gets teased by people of the village. He discovers that his deep affection for his childhood friend Malu, is actually love. This is preceded by an attempt by some hooligans (oolanmar, in Radhas terms) to molest him and his mothers advice that loving and marrying a girl would prove his manhood. Malu also is fond of Radha, and they engage in sexual activities. Even that doesnt change his feminine behaviour. Under special circumstances, he reaches another beach, which is more developed and there he is exposed to the outer world. He lives with Freddy and Rosy, who consider him as their lost brother. After several attempts by them, he sheds his transvestism but his femininity still hangs on. The film closes with shots of Radha going for fishing, his ancestral job. It is notable that even in the closing shots, Radha keeps his feminine mannerisms, though to a milder range.
 Dileep arguably presents the character in the most inimitable way. This role which won the Special Jury Award at the Kerala State Film Awards is another testimonial to his versatility.

==Cast==
 Dileep as Radhakrishnan
*Gopika as Malu Indrajith as Kumaran
*Lal as Divakaran (Father of Radhakrishnan)
*Biju Menon as Freddy
*Bhavana as Rosie (Sister of Freddy)
*Rajan P. Dev as Aasan (Father of Malu)
*Mala Aravindan
*Sukumari (Grand mother of Radhakrishnan)
*Salim Kumar as Vareed, colloquially known as Paradooshanam Vareed, due to his gossiping nature
*Anil Murali as Father of Kumaran
*Sreejith Ravi as Cleetus
*Valsala Menon as Freddys and Rosies grandmother
* Joju George
*Koottikal Jayachadran(actor) as Lawrence

==Crew==
*Direction: Lal Jose Lal
*Writer: Benny P Nayarambalam Vidyasagar
*Lyrics: Vayalar Sarath Chandra Varma
*Editing: Ranjan Abraham
*Director of Photography: Azhakappan
*Choreography: Sujatha
*Stunt Director: Thyagarajan
*Makeup: Pattanam Shah
*Costume Designer: Manoj Alappuzha
*Creative Support: Walter Jose
*Director of Art: Nemom Pushparaj

==Music==
{{Infobox album
|  Name        = Chanthupottu |
  Type        = Soundtrack |
  Artist      = Vidhyasagar |
  Cover       =  |
  Released    =  2005 |
  Recorded    =  | Feature film soundtrack |
  Length      =  |
  Reviews     =  |
  Last album = Muddula Koduku |
  This album = Chanthupottu| Made in USA|
}}

# "Azhakadalinte" - S. Janaki
# "Omanapuzha" - Vineeth Sreenivasan
# "Chanthu Kudanjoru" - Shahabas Aman, Sujatha Mohan
# "Kana Ponnum" - Franco

==References==
 
 

==External links==
*  
*   at the Malayalam Movie Database

 

 
 
 
 
 
 
 
 
 
 
 
 
 