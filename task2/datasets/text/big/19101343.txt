10 Rillington Place
 
 
{{Infobox film
| name           = 10 Rillington Place
| image          =File:10 rillington place poster.jpg
| image_size     =
| caption        =US theatrical release poster
| director       = Richard Fleischer
| producer       = Leslie Linder Martin Ransohoff
| screenplay     = Clive Exton
| based on       =  
| starring       = Richard Attenborough Judy Geeson John Hurt
| music          = John Dankworth
| cinematography = Denys Coop
| editing        = Ernest Walter
| studio         = Filmways Genesis Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 111 min
| country        = United Kingdom
| language       = English
}} crime drama film, directed by Richard Fleischer. It stars Richard Attenborough, John Hurt and Judy Geeson and was adapted by Clive Exton from the book Ten Rillington Place by Ludovic Kennedy (who also acted as technical advisor to the production).
 John Christie, Best Supporting Actor for his portrayal of Evans.

==Plot== John Christie peeks through the curtains of his bay window to inspect her. He greets her at the door with his soft voice and offers her a cup of tea. He prepares a remedy for her bronchitis by pouring some Tincture_of_benzoin|Friars Balsam into a squat jar. He runs a rubber hose through a hole in the lid. On the other end is a cardboard respirator, which he instructs Muriel to hold to her face. He inserts a second rubber hose into another hole in the jars lid, explaining that it is simply an exhaust tube. While Muriel is distracted with the respirator, Christie reaches behind her head and removes a bulldog clip from the "exhaust" line, which is attached to the domestic gas line. The Friars Balsam begins to bubble. Muriel complains of the smell, but Christie forces her to keep inhaling. When she finally passes out from the carbon monoxide in the gas, Christie produces a rope that he uses to strangle her. He buries her body in the garden behind his house. As he digs, he uncovers the body of one of his previous victims.

In 1949, Timothy and Beryl Evans move into 10 Rillington Place, London, with their infant daughter Geraldine. Tim boasts of having a high-powered job, but the couple is indigent. Beryl is pregnant again and attempts an abortion by taking some pills. When she informs Tim, they have an enormous fight which Christie breaks up. Soon after, Christie offers to help Beryl terminate the pregnancy, claiming a friendship with an eminent doctor who can help. He pretends to be reading a medical textbook one day in an effort to convince Tim of his expertise. Tim is essentially illiterate and cannot tell that Christie is lying. Christie tells Tim that 1 in 10 women die from the abortion procedure, but he assures them that fatalities are usually due to the errors of lesser doctors than himself. The Evans agree to let Christie do the procedure.

Christie occupies his wife, Ethel, by sending her to his office with some paperwork. He grabs his killing tools, makes a cup of tea, and hurries upstairs to Beryl. He is interrupted by a team of builders who are there to renovate the bathroom. He lets them in, and when he sees they are well-occupied, he pours a new cup of tea and heads back upstairs. Beryls anxiety about the procedure only deepens as she sees Christies crude setup. She has a violent reaction to the gas, and Christie has to punch her in the face to knock her out. 

When Tim returns, Christie explains that Beryl did not survive the procedure and blames the pills she had previously taken for the complications that led to her death. Tim wants to go to the police, but Christie convinces him that he will be seen as an accessory before the fact. Christie suggests that Tim leave that night on a train, while Christie disposes of Beryls body. He promised that he will place the baby in the care of a childless couple from East Acton. Tim reluctantly agrees. After he has left the house in the middle of the night, Christie leaves the bed, grabs one of his neckties, goes upstairs and murders the baby. 

Tim hides out with his aunt and uncle in Merthyr Tydfil, pretending that he is in town on business. He claims that Beryl and the baby are visiting her family in Brighton. Tims relatives send a letter to Beryls father, who telegrams in response to say that he has not seen Beryl in months. When confronted by his relatives, Tims guilt becomes overwhelming, and he visits the local police. He confesses to disposing Beryls body in the sewer after the botched abortion. Three London police officers lift the manhole, and do not find Beryls body. A search of 10 Rillington Place eventually uncovers the bodies of Beryl and the baby in the bathroom, where Christie hid them.
 criminal activity. During the trial, Ethel begins to fear her husband. She starts sleeping on the settee. Tim is found guilty and executed by hanging. 

After the trial, Christie and Ethels estrangement deepens, partially because of their poverty. Ethel informs Christie she will move out to stay with relatives, since they cannot afford their flat any more. When he begs her not to leave him, Ethel implies that he should be in prison. Christie murders her that night and hides her body under the floorboards. Later, he meets a woman suffering from a migraine in a restaurant. He pretends to be a medical expert and promises her a cure. He is next seen putting fresh wallpaper on a wall in his living room.

Several years later, Christie is living in a hostel. He brags to a fellow tenant about being a key witness in Tims trial, and he is frustrated that the man is uninterested in seeing Christies news clippings. Meanwhile, new tenants are moving into the Christies flat. They complain about the awful smell, and one of them peels off the wallpaper to find a space behind the wall. He gets a torch and finds three bodies behind the wall. Soon after, Christie is noticed by a police officer and arrested. The film ends with some cards explaining that Tim Evans was posthumously pardoned and reinterred on consecrated ground.

==Cast== John Reginald Christie
*Judy Geeson as Beryl Evans
*John Hurt as Timothy John Evans
*Pat Heywood as Ethel Christie
*Isobel Black as Alice
*Phyllis MacMahon as Muriel Eady
*Ray Barron as Workman Willis
*Douglas Blackwell as Workman Jones
*Gabrielle Daye as Mrs. Lynch Jimmy Gardner as Mr. Lynch Edward Evans as Detective Inspector
*Tenniel Evans as Detective Sergeant David Jackson as Police Constable
*George Lee as Police Constable
*Richard Coleman as Police Constable who arrests Christie
*André Morell as Judge Lewis
*Robert Hardy as Malcolm Morris
*Geoffrey Chater as Christmas Humphreys
*Basil Dignam as Member of the Medical Board
*Edward Burnham as Member of the Medical Board Edwin Brown as Hangman
*Sam Kydd as Furniture Dealer
*Rudolph Walker as West Indian man 1
*Tommy Ansah as West Indian man  2
*Reg Lye as Tramp
*Norma Shebbeare as woman in cafe

==Production==
The film was adapted by Clive Exton from the book Ten Rillington Place by Ludovic Kennedy.  The film relies on the same argument advanced by Kennedy that Evans was innocent of the murders and was framed by Christie. That argument was accepted by the Crown and Evans was officially pardoned by Home Secretary Roy Jenkins in 1966. The case is one of the first major miscarriages of justice known to have occurred in the immediate postwar period. Most of the script, narrative and character development of it was drawn up in the 1960s.   

In 1954, the year after Christies execution, Rillington Place in Notting Hill, west London was renamed Ruston Close, but number 10 continued to be occupied. The three families living there in 1970 refused to move out for the shooting of the film, so filming was moved to the empty number 7. The house and street were demolished later, and the area has changed beyond all recognition. Filming also took place at Merthyr Vale railway station; Merthyr Vale was Timothy Evanss hometown and where he made his confession to the police. The pub scenes were filmed at the Victoria Hotel on Burdett Road in east London. The pub was subsequently demolished as part of the redevelopment of the area in 1972–73.

  and Martin Ransohoff.    Hangman Albert Pierrepoint, who had hanged both Evans and Christie, served as an uncredited technical advisor on the film to ensure the authenticity of the hanging scene. 

==Reception==
At the time of the films release, reviews were mixed. Variety (magazine)|Varietys critic wrote "Richard Fleischer has turned out an authenticated documentary-feature which is an absorbing and disturbing picture. But the film has the serious flaw of not even attempting to probe the reasons that turned a man into a monstrous pervert." Praise went to John Hurt for his "remarkably subtle and fascinating performance as the bewildered young man who plays into the hands of both the murderer and the police."  Vincent Canby of The New York Times described 10 Rillington Place as "a solemn, earnest polemic of a movie, one with very little vulgar suspense ... The problem with the film is very much the problem with the actual case, which involved small, unimaginative people." 
 Time Out gave the film a 5-star review and described it as an "underseen gem". 

In an interview with Robert K. Elder in his book The Best Film Youve Never Seen, director Sean Durkin states that 10 Rillington Place "depicts this story the way that a piece of journalism might, as opposed to worrying about preconceived notions of what a film should achieve."    Tom Hardy of the British Film Institute has noted Attenboroughs ability at "getting into the flesh of the paranoid and the distressed", describing the film as a "detailed account of life under the shadow of World War II   is powerful and compelling".   
 Best Supporting Actor.

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 