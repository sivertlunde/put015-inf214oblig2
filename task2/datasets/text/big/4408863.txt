Renaissance (film)
{{Infobox film
| name           = Renaissance
| image          = Renaissance poster.jpg
| caption        = French theatrical release poster
| director       = Christian Volckman
| producer       = Roch Lener Aton Soumache Alexis Vonarb
| screenplay     = Mathieu Delaporte Alexandre de la Patellière Patrick Raynal Jean-Bernard Pouy
| story          = Alexandre de la Patellière Mathieu Delaporte Pascal Tosi
| music          = Nicholas Dodd
| distributor    = Pathé (France) Miramax Films (USA)
| released       =  
| runtime        = 105 minutes
| country        = France
| language       = French
| budget         = Euro|€14 million
| gross          = $1,831,348 (worldwide)
}}

Renaissance is a 2006 (English Language) French black-and-white animated science fiction film by French director Christian Volckman. It was co-produced in France, United Kingdom and Luxembourg and released on 15 March 2006 in France and 28 July 2006 in the UK by Miramax Films. Renaissance features a rare visual style in which almost all images are exclusively black and white, with only occasional colour used for detail. The film centers on a policeman investigating the kidnapping of a scientist who holds the key to eternal life in a futuristic Paris.

==Plot==
The film opens in a future Paris with scenes establishing the kidnapping of 22-year-old scientist Ilona Tasuiev, who works for the megacorporation Avalon. The focus transitions to police captain Barthélémy Karas, as he defuses a hostage situation by killing the hostage-takers. Afterwards Karas is given the job of solving the mystery surrounding Ilonas disappearance. Karas begins by contacting Dr. Jonas Muller, a former Avalon scientist familiar with her.

Muller had been working to cure progeria, a genetic condition which affected his brother. Muller worked for Avalon as their top scientist but left after he failed to find a cure and his brother died. He took up new work at a free clinic. Muller tells Karas that "No one ever leaves Avalon", throwing the corporation under suspicion. Karas visits Paul Dellenbach, one of Avalons CEOs and questions him about Ilona. On suggesting he may have been sleeping with her, Dellenbach replies "I sleep with my wife, I sleep with my secretary, I even sleep with my sister-in-law but I would never sleep with one of my researchers".

After following a series of dead ends, control tells Karas they are tailing Illonas car through Paris. Eventually he captures the driver after a chase which ends at the Eiffel Tower. The man turns out to be a henchmen of Farfella, an Arab Muslim mobster and a childhood friend of Karas. The police captain returns the criminal to Farfella who in return gives him security footage of Illonas kidnapping; it shows her car being stolen by an incredibly old man.

Karas asks Ilonas sister, Bislane, who works for Avalon to break into the companys Archives to discover what Muller was researching. The sister discovers that a Dr. Nakata worked with Muller in a quest to find a cure for progeria. But they destroyed all evidence of their work when some of the children they were testing on started to mutate. Karas and Bislane then escape because accessing the closed file has alerted Avalon security.

Later Karas opens up to Bislane and tells her that he and Farfella were raised in the casbah where they worked with gangs. After a drugs run went wrong, they ended up in a holding cell. Farfella escaped but Karas was left to the mercy of the other gang. Karas puts Bislane under false arrest to protect her from Avalon. Meanwhile Ilona is shown confined in a cyber ball which is being controlled by the old man.

Eventually Karas tracks down Muller. He explains that he took Ilona because through her research she has discovered the secret to eternal life (as he himself did 40 years ago); but knowing what the consequences would be if Avalon acquired such knowledge, he kidnapped her. Karas tries to encourage the old scientist to hand himself in but Muller is mistakenly shot by a police marksman. Karas then deduces that the mysterious old man is Mullers younger brother: now immortal but trapped in an elderly body.

Karas calls on Farfella who hides Bislane from Avalon while also getting a fake passport for Ilona. However the mega-corporations security are also closing in on the Parisian sewer where Ilona is being held captive. After a short battle, Karas is mortally wounded rescuing Ilona. However she refuses to take the fake passport to start a new life. Instead she tells Karas she wants to live forever with her discovery turning around to walk back to Avalon security. Reluctantly Karas shoots her in the back as CEO Dellenbach watches live through one of his mens helmet camera.

As Karas lies mortally injured from a gunshot wound, he imagines himself apologizing to Bislane for killing her sister for which she forgives him. The film closes with Mullers little brother living as a tramp, throwing his picture of him and his brother into a burning bin. The final scene shows an advert for Avalon with an old woman becoming young again saying, "With Avalon, I know Im beautiful and Im going to stay that way."

==Cast==
 
 

===French ===
* Patrick Floersheim as Barthélémy Karas
* Laura Blanc as Bislane Tasuiev
* Virginie Mery as Ilona Tasuiev
* Gabriel Le Doze as Paul Dellenbach
* Marc Cassot as Jonas Muller
* Rick Warden as Amiel
* Bruno Choel as Pierre Amiel
* Marc Alfos as Nustrat Farfella
* Chris Bearne as Multiple
* Lachele Carl as Nora
* Radica Jovicic as Woman Hostage
* Breffni McKenna as Dmitri
 

===English ===
* Daniel Craig as Barthélémy Karas
* Catherine McCormack as Bislane Tasuiev
* Romola Garai as Ilona Tasuiev
* Jonathan Pryce as Paul Dellenbach
* Ian Holm as Jonas Muller
* Jerome Causse as Amiel & Dmitri
* Kevork Malikyan as Nusrat Farfella
* Pax Baldwin as Farfella Boy
* Wayne Forrester as Administrator
* Julian Nest as Parisien
* Sean Pertwee as Montoya
* Jessica Reavis as Multiple
* Nina Sosanya as Reparez
* Leslie Woodhall as Elderly Man
 

==Production==
 
 blue screen. Computer animators translated these animations to digital models used for the characters. The animated characters were placed in three-dimensional computer backdrops, with post-process effects added to achieve the films final look.

French automaker Citroën designed a car specially for the film, imagining what a Citroën might look like in 2054. Volckman initially wanted Karas to drive a Citroën DS and approached the company for permission to use it in the film. Citroën suggested the filmmakers work with their designers to design a new car. The final design was produced after three months. 
 Disney with US$3 million provided from Miramax.

==Critical reception==
The film received mixed reviews from critics. The review aggregator Rotten Tomatoes reported that 47% of critics gave the film positive reviews, based on 73 reviews.  Metacritic reported the film had an average score of 57 out of 100, based on 17 reviews. 

The film was honored at the 5th Festival of European Animated Feature Films and TV Specials where it was awarded the prize for Best Feature Film. 

==Box office==
The film grossed a total of $1,831,348 worldwide – $70,644 in North America and $1,760,704 in other territories  – including $1,520,587 in Algeria, France, Monaco, Morocco and Tunisia. 

==See also==
 
* List of animated feature-length films
* List of post-1960s films in black-and-white

==References==
 

==External links==
*  
*  
*  
*  
*  

;Interviews  
*    
*    
*    

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 