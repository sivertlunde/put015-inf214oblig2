Devil Dog: The Hound of Hell
{{Infobox television film
| name = Devil Dog: The Hound of Hell
| image = Devil Dog- The Hound of Hell FilmPoster.jpeg
| image_size = 
| caption = Horror Thriller Thriller
| creator = 
| director = Curtis Harrington
| producer = Hal Landers (executive producer) Lou Morheim (producer) Jerome M. Zeitman (executive producer)
| starring = Richard Crenna Yvette Mimieux Kim Richards Ike Eisenmann
| cinematography = Gerald Perry Finnerman
| music = Artie Kane
| writer = Elinor Karpf Steven Karpf	 	 
| editing = Margo Anderson Ronald J. Fagan	
| runtime = 95 minutes
| studio = Wizan Productions CBS
| country   = United States
| language  = English
| network   = CBS
| released  = October 31, 1978
}} Witch Mountain series, but were not intentionally cast based on that fact, just on that they looked believable as siblings.

==Plot summary==
 satanist who bred the dog during an evil ceremony, causing it to be Demonic possession|possessed. They sell the dog to wreak havoc in the hope Satan will overcome good once and for all. The dog acts very strangely when they bring it home, leading the father and the family maid to believe there is something wrong with the dog. The dad believes so after the maid is killed in a fire while she was watching the dog, and the dad being forced to stick his arm into a lawnmower while the dog is present. The father barely avoids having his arm chopped off and soon, the dog begins to exhibit mental control that allows him to kill, injure, or control many victims. Eventually, the familys souls are possessed by the hound causing them to act strangely, including the son framing a student by stealing a watch and planting it in a classmates locker, allowing him to win the student election. Dad decides the dog has overstayed his welcome when he finds a secret shrine to Satan in the attic. Dad tries to shoot the demonic beast to put an end to the hardships, but it is unharmed.

Realizing the dog is possessed, the father finally makes a special trip to Ecuador to determine how to destroy the animal. Unfortunately, there is no way to kill it, but if you hold a holy symbol to its eye, you can imprison it in Hell for 1,000 years. He takes it to a showdown at his work plant, but there the dog turns into a demon and begins to wreak havoc. When he is cornered by the monster, he holds the sign he made on his hand right up to the beasts eye. This causes the beast to be engulfed in fire and be imprisoned, getting his familys souls back. The final scene shows the family loading the family car for a vacation, and one of the children mentioning there were several more puppies the vendor was selling, suggesting there are more satanic dogs and that it is not over yet.

==Cast==
* Richard Crenna as Mike Barry
* Yvette Mimieux as Betty Barry
* Kim Richards as Bonnie Barry
* Ike Eisenmann as Charlie Barry (as Ike Eisenman)
* Victor Jory as Shaman
* Lou Frizzell as George
* Ken Kercheval as Miles Amory
* R. G. Armstrong as Dunworth
* Martine Beswick as Red Haired Lady
* Bob Navarro as Newscaster
* Lois Ursone as Gloria Hadley
* Jerry Fogel as Doctor Norm
* Warren Munson as Superintendent

==Home Video Release== Shriek Show Entertainment released the film on DVD in 2005 and Blu-ray in 2011.

==External links==
* 
*  

 

 
 
 
 
 
 
 
 

 