Elemental (film)
{{Infobox film
| name = Elemental
| caption = Theatrical release poster
| director = Emmanuel Vaughan-Lee Gayatri Roshan
| producer = Emmanuel Vaughan-Lee Gayatri Roshan
| music = H. Scott Salinas Emmanuel Vaughan-Lee
| editing = Pedro Kos
| released =  
| runtime = 93 minutes
| country = United States
| language = English  Hindi
}}
Elemental is a 2012 documentary film directed by Emmanuel Vaughan-Lee and Gayatri Roshan.  The film premiered at the Mill Valley Film Festival on October 9, 2012 in Mill Valley, California.   

==Synopsis==
"Elemental" tells the story of three individuals united by their deep connection with nature and driven to confront some of the most pressing ecological challenges of our time.
 Tar Sands, Keystone XL Indigenous communities and threatening an entire continent.

And in Australia, inventor and entrepreneur Jay Harman searches for investors willing to risk millions on his conviction that natures own systems hold the key to our worlds ecological problems. Harman finds his inspiration in the natural worlds profound architecture and creates a revolutionary device that he believes can slow down global warming.
 postmodern heroes for whom stemming the tide of environmental destruction fades in and out of view.

==Release==
The film premiered at the 2012   in the  ,     .   

==Reception==
During its festival run, "Elemental" was reviewed by online and local publications including Variety (magazine)|Variety, The Film Stage, Austin American-Statesman, Off to the Films, St. Louis Post-Dispatch, and Lost in Reviews.

Dennis Harvey wrote in Variety (magazine)|Variety, that the film is "an interesting...view of eco-warriors at work" and "their efforts are duly inspiring and the related issues imposing".  
 Harvey, Dennis.  Variety October 29, 2012.  The Film Stage noted the films "alarming intimacy," and stated, "Elemental is a rare, fresh look at environmental issues and sustainability that does not shy away from the personal impact the decisions to dedicate ones life to a cause entails."   

Off to the Films called the documentary "beautifully shot", "heartbreaking, informative, and compelling".   Off to the Films. October 23, 2012.  Lost in Reviews called the film "truly moving and engaging" and "one of the better environmental documentaries out there".   Lost in Reviews. October 30, 2012. 

The St. Louis Post-Dispatch listed Elemental as one of the highlights of the 21st St. Louis International Film Festival, and called the film "inspirational
", concluding: "The pro-active protagonists lift it from the muck of feel-bad documentaries."   St. Louis Post-Dispatch. November 7, 2012. 
 San Francisco magazine highlighted the film as one of the best five documentaries at Mill Valley Film Festival,  and the Austin American-Statesman called it one of the top documentaries at Austin Film Festival. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 