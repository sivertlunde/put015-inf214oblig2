Simha Swapnam
{{Infobox film
| name           = Simha Swapnam
| image          =
| caption        =
| writer         = D. Prabhakar  
| story          = M. D. Sundar
| screenplay     = V. Madhusudhan Rao 
| producer       = V. B. Rajendra Prasad
| director       = V. Madhusudhan Rao Shantipriya
| Chakravarthy
| cinematography = D. Prasad Babu
| editing        = A. Sreekar Prasad
| studio         = Jagapathi Art Pictures
| released       =  
| runtime        = 2:21:14
| country        =   India
| language       = Telugu
| budget         =
| preceded_by    = 
| followed_by    = 
}}
 Shantipriya in Hindi Movie Khatron Ke Neelam in lead roles. This film is first debut of Jagapathi Babu as Hero with double role performance, both the roles of Sanjay Dutt & Chunky Pandey is done by Jagapathi Babu in Telugu.      

==Plot== Ranganath & Ahuti Prasad). Bala Rams brother comes to know about the gold smuggling, so, Raghupathi kills him and frames Bala Ram, because of which he ends up in jail. Annapoorna (Jayasudha), wife of Bala Ram gives birth to twins, Rajesh & Harish (Jagapathi Babu). She leaves one of her twins with a trusted friend and gives him the responsibility of bringing up the child. As the kids grow up, Bala Ram becomes a vigilante killing the criminals. How Bala Ram takes his revenge and how the family unites is what the movie is all about.

==Cast==
 
*Krishnam Raju as Balarama Krishnarjun
*Jayasudha as Annapurna
*Jagapathi Babu as Rajesh & Harish (Duelrole) 
*Vani Viswanath  Shantipriya
*Gummadi Gummadi
*Gollapudi Maruthi Rao Ranganath as Rangapathi
*Giri Babu
*Ahuti Prasad as Raghupati
*Chalapathi Rao
*Thyagaraju Ramana Murthy
*Pradeep Shakthi
*Vinod 
*Jaya Bhaskar
*KK Sarma
*Anitha
*Sri Lakshmi
*Phani
*Vijayalakshmi
*Shyamala
 

==Soundtrack==
{{Infobox album
| Name        = Simha Swapnam
| Tagline     = 
| Type        = film Chakravarthy
| Cover       = 
| Released    = 1989
| Recorded    = 
| Genre       = Soundtrack
| Length      = 25:47
| Label       = AMC Audio Chakravarthy
| Reviews     =
| Last album  = Mamathala Kovela  (1989) 
| This album  = Simha Swapnam   (1989) 
| Next album  = Ontari Poratam  (1989) 
}}
 Aachari Aatreya. Music released on AMC Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!length
|- 1
|Kallalona Neeve Gundelona Neeve SP Balu, P. Susheela
|3:29
|- 2
|Chaliki Vaniki SP Balu,P. Susheela
|4:15
|- 3
|Jigi Jigi  SP Balu,P. Susheela
|4:26
|- 4
|Tholi Kougilintha SP Balu,P. Susheela
|4:22
|- 5
|Urumi Urumi SP Balu,P. Susheela
|4:52
|- 6
|Kallalona Neeve (Sad)  SP Balu,P. Susheela
|4:23
|}

==References==
 

==External links==
 

 
 
 
 
 
 
 