Sahara (1919 film)
:For other films with the same name, see Sahara_(disambiguation)#Film_and_television|Sahara.
{{Infobox film
| name           = Sahara
| image          = Sahara (1919) - Ad 2.jpg
| image_size     = 200px
| caption        = Ad for film
| director       = Arthur Rosson
| producer       = J. Parker Read, Jr. (producer) Allan Dwan (supervisor)
| writer         = C. Gardner Sullivan Matt Moore
| music          = Hugo Riesenfeld Victor Schertzinger
| cinematography = Charles J. Stumar
| editing        =
| distributor    = William Wadsworth Hodkinson|W. W. Hodkinson Corp. through Pathé Exchange, Inc.
| released       =  
| runtime        = 70 minutes
| country        = United States Silent (English intertitles)
| budget         =
| gross          =
}}
 American dramatic film written by C. Gardner Sullivan and directed by Arthur Rosson.  The film starred  Louise Glaum and told a story of love and betrayal in the Egyptian desert. 

A print survives at FilmmuseumNederlands , Amsterdam. 

==Plot== Matt Moore. Cairo with the wealthy Baron Alexis, portrayed by Edwin Stevens. Mignon lives in Baron Alexis palace while Stanley goes blind and becomes addicted to the drug hasheesh. Mignon later encounters Stanley and her son, who have become beggars in the streets of Cairo.   Mignon returns to the desert to care for her husband, and the two are reconciled.

==Cast==
*Louise Glaum ..... Mignon Matt Moore ..... John Stanley
*Edwin Stevens ..... Baron Alexis
*Pat Moore ..... The Boy
*Nigel De Brulier ..... Mustapha

==Critical reception==
The film received generally positive reviews upon its release in the summer of 1919. The Washington Post wrote: "Sahara, one of the most impressive film dramas ever screened at Moores Rialto Theater, was acclaimed by large audiences at the first showings yesterday. Lavish in scenic embellishment ... Sahara features a most entertaining bill." 

The Morning Telegraph of New York wrote: "It is a most elaborate production, in which are combined taste and lavish expenditure.  The scenery -- both Miss Glaums and that of the play, is unusually effective, the stars costumes being sufficient of themselves to repay women patrons. Moreover, the story holds interest unflaggingly, and a jewel like this in a setting of the sort here revealed makes for real value."  

The New York Times offered the following comments: "It is another Wild East melodrama.  Desert sand and wind storms, picturesque Arabs, dashing horses, camels, beggars, turbans, flowing robes, bloomers and streets with the atmosphere of the Arabian Nights -- these are the materials substituted for the long-familiar two-gun man and his well-known properties.  And theres no denying that these materials make good pictures, especially in the hands of Mr. Dwan.  There are some effective scenes in Sahara.  The story is purely mechanical, but Matt Moore does creditable work as the hero, and little Pat Moore is as appealing as a child can be."  

The Los Angeles Times reported: "This brilliant author   is found at his best, it is said, in his newest drama, Sahara ...  Press notices in the East indicate that Sahara is unquestionably one of the most luxurious motion pictures of the year."   It was also described as "a masterpiece" and a film "as brilliant and gorgeous as the colorful Egyptian desert." 

A syndicated review published in several newspapers called it the "Biggest Picture of the Year" and praised it as one of the great dramas of recent years: "All who see Louise Glaum in Sahara, a powerful emotional drama of Paris and Cairo by C. Gardner Sullivan and supervised by Allan Dwan will regret the days that she has spent in other pictures which failed to give her the opportunities of Sahara ... This big J. Parker Read, Jr., production distributed by W.W. Hodkinson is entitled to rank as one of the really great dramas produced in motion pictures in recent years. ... The achievement of a picture like Sahara represents the triumphs of brains of an author, star, director and producer over the mountain high stupidities that have become involved in motion picture making. It reveals, too, that there can be a Belasco of the screen as there is one in the theatre; that great artists can perpetuate their art and their intelligence via photography.  Sahara is a picture that will live long in the memory.   

A newspaper in Cedar Rapids, Iowa urged every woman in the city to see the film and decide for themselves on the morality of Mignons actions: "Sahara is a picture that every woman in Cedar Rapids should see. The story is one that they will admire and absorb but whether they will agree with the woman Mignon is a question they alone can decide. Undoubtedly some of them will say that Mignon was right in leaving her husband and child to seek happiness in the care-free city of Cairo while others who believe in the hide-bound rule of modern society will condemn her."    
A Canadian reviewer called it "lavish, riotous in action and compelling in its alluring charm of cast and situation. 

An Alabama reviewer focused on the lighting and cinematography: "So beautiful are the lighting and tinting effects in Sahara starring the magnetic Louise Glaum, that even the morbid scene described above is made an artistic picture that pleases the eye as much as the scenes of splendor in the Palace of the Dawn.  ... It is a magnificent Hodkinson picture ..." 

==References==
 

==External links==
 
*  

 
 
 