The Road to Freedom (film)
{{Infobox film
| name           = The Road to Freedom 
| image          = TheRoadToFreedom2010Poster.jpg
| caption        = Film poster
| director       = Brendan Moriarty
| screenplay     = Thomas shade  Margie Rogers
| producer       = Brendan Moriarty  Patrick Moriarty
| starring       = Joshua Fredric Smith Scott Maguire
| music          = Austin Creek
| cinematography = David Mun
| editing        = Sean Halloran Margie Rogers	 	
| studio         = Endocom 
| distributor    = Creative Freedom
| released       =  
| runtime        = 
| country        = United States / Cambodia
| language       = English / Khmer
| budget         = $1M
| gross          = 
}} Sean Flynn, the son of Errol Flynn, who disappeared with fellow photojournalist Dana Stone in Cambodia in 1970. Joshua Fredric Smith portrays Sean while Scott Maguire portrays Dana.

The world premiere at the Cannes Film Market was on the April 27, 2010.  In July 2011, Creative Freedom acquired the United States distribution rights and released the film on September 30, 2011.     

==Plot==
Two photojournalists, Sean (Joshua Fredric Smith) and  Dana (Scott Maguire), brave the jungles to get their story of war-torn Cambodia in 1970 when they are captured by Khmer Rouge guerrillas. 

==Cast==
*Joshua Fredric Smith as Sean 
*Scott Maguire as Dana
*Tom Proctor as Francias
*Nhem Sokun as Lim Po
*Nhem Sokunthol as General
*Kanilen Kang as Mean 
 
==External links==
*  

==References==
 

 
 
 
 
 