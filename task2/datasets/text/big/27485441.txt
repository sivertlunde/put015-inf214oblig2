Killer Elite (film)
 
 
{{Infobox film
| name           = Killer Elite
| image          = Killer Elite Poster.jpg
| director       = Gary McKendry
| producer       = Michael Boughen Steve Chasman Sigurjón Sighvatsson Tony Winley 
| screenplay     = Matt Sherring
| based on       =  
| starring       =  Clive Owen Jason Statham Robert De Niro
| music          = Reinhold Heil Johnny Klimek
| cinematography = Simon Duggan John Gilbert
| studio         = Omnilab Media Ambience Entertainment Current Entertainment Sighvatsson Films Film Victoria Wales Creative IP Fund Agora Films International Traders Mascot Pictures Wales
| distributor    = Entertainment Film Distributors  (United Kingdom)  Open Road Films  (United States)  Walt Disney Studios Motion Pictures  (Australia) 

| released       =  
| runtime        = 114 minutes 
| country        = United Kingdom Australia
| language       = English Arabic French Spanish
| budget         = $70 million   
| gross          = $56,383,756 
}}
 action Thriller thriller film Sir Ranulph Fiennes and is directed by Gary McKendry.

==Plot== mercenaries Danny Bryce (Jason Statham), Hunter (Robert De Niro), Davies (Dominic Purcell), and Meier (Aden Young) are in Mexico to assassinate a man. Danny unwittingly kills him in front of his young child, then is injured during the getaway. Affected by this outcome, Danny retires and returns to his native Australia.

One year later in 1981, Danny is summoned to Oman where Hunter is being held captive. He meets with the agent (Adewale Akinnuoye-Agbaje) who arranges missions for assassins and learns that Hunter accepted a $6 million job but failed to accomplish it. If Danny doesnt complete Hunters mission, Hunter will be executed.
 SAS troopers—Steven Harris (Lachy Hulme), Steven Cregg (Grant Bowler), and Simon McCann (Daniel Roberts)—for killing his three eldest sons during the Dhofar Rebellion. Danny must videotape their confessions and make their deaths look like accidents, and he must do it before the terminally ill Sheikh dies. This will allow the Sheikhs fourth son, Bakhait (Firass Dirani), to regain control of the desert region his father had ruled. If Danny fails, Hunter will be killed. Danny reunites with Davies and Meier. They agree to help him in exchange for a share of the money. 

As Danny and Meier sneak into the house of their first target, Steven Harris, in Oman, Davies questions local bar patrons about former SAS members. This is reported to the Feathermen, a secret society of former operatives protecting their own. Their head enforcer, Spike Logan (Clive Owen), is sent to investigate.

After Harris has confessed on videotape, Danny and Meier take him to the bathroom. They plan to kill him by hitting him over the head with a hammer made of bathroom tile, then leave the body to look like Harris slipped and hit his head.  In the process, Harris girlfriend arrives, knocking on the door.  While Harris and Meier are distracted waiting for her to leave, Harris attempts to break free, Causing Meier to kill him hastily in a struggle.

Back in London, Davies discovers the second target, Steven Cregg, preparing for a long nighttime march in wintry weather at the Brecon Beacons mountain range. Davies creates a diversion; posing as a civilian having car problems outside the bases fence, allowing Danny to infiltrate the base. Inside, he drugs Creggs coffee, causing him to go into shock and die of hypothermia during the march. Danny, in uniform, follows Cregg on the march, and makes him confess on videotape before he dies.

Their last target, Simon McCann, currently works as a mercenary, after being discharged from the SAS following a car bombing that made him mentally unstable. Their plan is to rig a truck to respond to remote control, and crash it into McCanns car while he is driving, making his death look like a traffic accident. With the help of a new and inexperienced team member, Jake (Michael Dorman), Meier successfully kills McCann, however, Logan and his men were watching over McCann. A gun fight in the docks ensues, and Meier is accidentally killed by the inexperienced Jake. Danny and Davies decide that the case is over, and they part ways. Soon after, Davies is tracked down by Logans men, and is hit by a truck and killed while trying to escape them.

Danny returns to Oman and gives the Sheikh the last taped confession, which he has faked. Hunter is released and returns to his family, while Danny heads back to Australia and reunites with Anne (Yvonne Strahovski), a childhood acquaintance. Soon, he is informed by the Agent that there is one last man who participated in the Sheikhs sons murders and that this man, Ranulph Fiennes, is about to release a book about his experiences as a member of the SAS.

Danny tells Anne to go to France with Hunter to protect her while he carries out the last job. The Sheikh’s son confirms that Harris was an innocent man. Logan, meanwhile, traces Danny through the Agent and sends a team to protect the author, but Jake distracts them, allowing Danny to infiltrate the building and shoot the author. He chooses to only wound the author, however, but takes pictures that appear to show him dead. Logan chases and captures Danny, taking him to an abandoned warehouse, but he is interrupted when an agent from the British government arrives and reveals that the British government is behind the events because of the Sheikhs valuable oil reserves. A three-way battle ensues, with Danny escaping and Logan shooting the government agent. 

In Paris, Anne goes to take the subway. The Agent and a henchman are waiting for her because the Agent plans to kidnap her for ransom. But Hunter follows her, beats the henchman and shoots the Agent in the leg. The Agent then tries to explain that it is just for business. Hunter seems threatening at first, but then spares his life.

Danny and Hunter head to Oman to give the Sheikh the pictures. However, Logan arrives first, confronts the Sheikh, telling him that the pictures are fake and then stabs him to death. The Sheikhs son does not care and gives over the money, which was intended for Danny and Hunter, to Logan. Hunter spots Logan leaving, and they chase after him, along with the Sheikhs men.

After stopping the Sheikhs men, Danny and Hunter confront Logan on a desert road. Danny says that Logan can keep the money (though Hunter takes some of the money for his expenses and his family). They give Logan the remainder, telling him that hell need it to start a new life away from the government after killing the government agent and acting against the wishes of the Feathermen and the British government. Danny says that its over for him and that Logan must make up his own mind. They leave him there, saying theyll send a cab for him from the airport. Danny meets with Anne in France to start a new life.

== Cast ==
 , Jason Statham, Yvonne Strahovski and Adewale Akinnuoye-Agbaje.]]
 
* Jason Statham as mercenary Danny Bryce SAS officer Spike Logan
* Yvonne Strahovski as Anne Frazier
* Robert De Niro as Hunter
* Lachy Hulme as Harris
* Dominic Purcell as Davies 
* Aden Young as Meier
* Adewale Akinnuoye-Agbaje as The Agent
* Ben Mendelsohn as Martin
* Grant Bowler as Captain James Cregg 
* Matthew Nable as Pennock
* Michael Dorman as Jake
* Jamie McDowell as Diane
* Chris Anderson as Finn
* George Murphy as ADR Voice
* Gillie McKenzie as Shooter and stunt double
* Firass Dirani as Bakhait
* Rodney Afif as Sheikh Amr
* Andrew Stehlin as Dutchy
* Daniel Roberts as McCann
 

==Production== Spring Street set in 1970s Paris.  The scene of McCanns death by tanker truck was filmed on Dynon Road, Melbourne.

Some London scenes were filmed in Cardiff—in July 2010, De Niro and Statham were seen filming outside The Promised Land Bar on Windsor Place. Other scenes shot in Cardiff were also on Windsor Place, showing the City United Reformed Church, Buffalo bar and various small business buildings. Agents several meetings with other characters at a stone, columned monument were shot at the Welsh National War Memorial in Alexandra Gardens, Cardiff. A scene where The Welshman leaves a building were shot on Kings Road, Pontcanna, showing Kings Road Doctors Surgery and residential buildings. East Aberthaw, Vale of Glamorgan.   

In July 2010, filming took place near the Storey Arms outdoor centre, in the Brecon Beacons. A number of 1970s period cars were in evidence, particularly a bright orange Austin Maxi.

==Reception==
The film, which had a gala premiere at the 36th Toronto International Film Festival on 10 September 2011,    has received negative reviews from critics.   gave it 3 stars out of 4, calling it director Gary McKendrys "impressive debut", noting he "understands that action is better when its structured around character and plot, and doesnt rely on simple sensation." 

==The Feather Men==
The plot for the movie is based on the novel The Feather Men by Sir Ranulph Fiennes and is "based on a true story" (since Fiennes insists that it is true). Several elements from the book were altered to make the movie seem more believable to a movie-going audience. three feathers of British Army regiments. Their job was to protect SAS personnel and their families and avenge wrongs or harm done to them. 
* The mastermind behind the plot was changed from a Soviet-trained terrorist group called The  Clinic to an Omani sheik who is inexplicably sent into exile after his three eldest sons die on jihad. The sheik hires a group of highly-skilled Western mercenaries, gives them a limitless budget, and holds one of their friends as a hostage so they will complete the task he sets for them.    
* The targets are three SAS troopers (one who is still serving and is a decorated war hero) who served in Oman in the 1970s. Their deaths must appear accidental to avoid reprisals. In the film, an added complication is that they must confess to being murderers before they are killed.  Firqas under training and 30 paramilitary askars (armed police) held off a force of 250 insurgents, is mentioned in passing in the film but never explained. One of the sheiks three sons was supposed to have been killed in action there. 
* In the movie, the British Foreign Office is supposed to be in collusion with the sheik in order to guarantee oil leases on the sheiks land. They even force the "feather men" to back off with threats of imprisonment. However, the wealthy sheik is in exile and his son, a westernized playboy, shows no interest in returning to his homeland to claim his title. Therefore even if the sheiks plot is successful the British government will not have gained any leverage with the original landholders (and probably have a current relationship with the usurper who replaced him). The book has no such subplot.
* Sir Ranulph Fiennes claims that the "feather men" saved his life from an assassination attempt by The Clinic. In the book The Clinic tried to ambush him at his farmhouse in Exmoor, but the "feather men" ran them off. In the movie, he is a minor character who only survives because the assassin feels regret and only maims him. In fact, in the movie Fiennes inclusion in the death list is an afterthought by the patron because Fiennes falsely claims in a tell-all book that he was at Mirbat.

== See also ==
*Battle of Mirbat

==References==
 

==External links==
 
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 