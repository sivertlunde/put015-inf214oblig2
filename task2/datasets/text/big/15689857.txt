Day of the Idiots
{{Infobox film
| name           = Day of the Idiots
| image          = DayoftheIdiots.jpg
| image_size     = 
| caption        = Screenshot of Carole Bouquet
| director       = Werner Schroeter 
| producer       = Karel Dirka, Peter Genée, Harald Kügler   
| writer         = Dana Horaková. Werner Schroeter 
| narrator       = 
| starring       = Carole Bouquet, Ingrid Caven, Christine Kaufmann
| music          = Peer Raben
| cinematography = Ivan Slapeta
| editing        = Catherine Brasier-Snopko
| distributor    = OKO-Film
| released       = West Germany: March 26, 1982
| runtime        = 110 min.
| country        = West Germany
| language       = German
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1981 West West German psychological fantasy film drama directed and written by Werner Schroeter. It stars Carole Bouquet as a disturbed mental patient with an inclination to remove her clothes and Ingrid Caven as Dr. Laura and Christine Kaufmann as Ruth.

The film was nominated for a Golden Palm Award at the 1982 Cannes Film Festival    and won best film at the 1982 German Film Awards going to director Werner Schroeter.

==Cast==
*Carole Bouquet as  Carole 
*Ingrid Caven as  Dr. Laura 
*Christine Kaufmann as  Ruth 
*Ida Di Benedetto as  Elisabet 
*Carola Regnier as  Ninon 
*Mostefa Djadjam as Alexander 
*Hermann Killmeyer as Markus 
*Marie-Luise Marjan as  Schwester Marjan 
*Magdalena Montezuma as  Zigeunerin 
*George Stamkoski as himself
*Tamara Kafka   
*Dana Medřická
*Fritz Schediwy   
*Jana Plichtová

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 