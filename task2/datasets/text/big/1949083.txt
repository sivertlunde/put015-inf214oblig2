The Cincinnati Kid
 
{{Infobox film name = The Cincinnati Kid image  = Ckfilm.jpg
| image_size     = 225px border = yes caption = theatrical release poster producer = Martin Ransohoff director = Norman Jewison writer  = Richard Jessup (novel) Ring Lardner Jr. and Terry Southern (screenplay) starring = Steve McQueen Edward G. Robinson Ann-Margret Karl Malden Tuesday Weld music  = Lalo Schifrin cinematography = Philip H. Lathrop editing  = Hal Ashby studio   = Filmways Solar Productions  distributor  = Metro-Goldwyn-Mayer (1965,original) Warner Bros. (2005, DVD and 2011, Blu-Ray DVD) released =   runtime = 102 minutes language = English
| gross =$7,000,000 (US/ Canada rentals) 
}}

The Cincinnati Kid is a 1965 American drama film. It tells the story of Eric "The Kid" Stoner, a young Great Depression|Depression-era poker player, as he seeks to establish his reputation as the best. This quest leads him to challenge Lancey "The Man" Howard, an older player widely considered to be the best, culminating in a climactic final poker hand between the two.
 1947 blacklisting The Hollywood Ten. {{cite web
  | first =Steven | last =Hartman | title =Film Notes: Cincinnati Kid
  | work = New York State Writers Institute Film Notes  | publisher = University at Albany
  | url =http://www.albany.edu/writers-inst/webpages4/filmnotes/fnf98n8.html
  | date=1998 (?) | accessdate =2007-07-30}}    The film was directed by Norman Jewison and stars Steve McQueen in the title role and Edward G. Robinson as Howard. Jewison, who replaced original director Sam Peckinpah shortly after filming began,  describes The Cincinnati Kid as his "ugly duckling" film. He considers it the film that allowed him to transition from the lighter comedic films he had previously been making and take on more serious films and subjects. {{cite video
  | people =Jewison, Norman
  | title =The Cincinnati Kid director commentary
  | medium =DVD
  | publisher =Turner Entertainment Co
  | date =2005}} 
 garnered mixed reviews from critics on its initial release; supporting actors Robinson and Joan Blondell earned award nominations for their performances.

==Plot==
Eric Stoner, nicknamed "The Kid," is an up-and-coming poker player. He hears that Lancey Howard, a longtime master of the game nicknamed "The Man," is in town, and decides to take him on. The Kids friend Shooter cautions him, reminding the Kid how he thought he was the best five-card stud player in the world, until Howard "gutted" him when they played.
 markers worth $12,000, and blackmails him by threatening to reveal damaging information about his wife, Melba. When Shooter asks why, Slade tells him that he wants to see Howard gutted the way Howard gutted him. Shooter agonizes over his decision, having spent the last 25 years building a reputation for integrity.

With the Kids girl Christian visiting her parents, Melba tries to seduce him. Out of respect for Shooter, he rebuffs her, and spends the day before the game with Christian and her family.

The big game starts with six players, including Howard, the Kid and Shooter playing as he deals. In the first big confrontation between the Kid and Howard, the Kid is short $2,000 and Slade steps in to stake him. Several hours later, Howard busts one player, perhaps with a bluff, and the remaining players take a break. Following the break, Lady Fingers who has been delighting in needling Howard all evening, takes over as dealer and continues to needle him.

As the game wears on, Shooter only deals, and then after another hand when Howard outplays them, two more players drop out, leaving just Howard and the Kid. After a few unlikely wins, the Kid calls for a break and confronts Shooter, who admits to being forced into cheating by Slade. The Kid insists he can win on his own and tells Shooter to deal straight or he will blow the whistle, destroying Shooters reputation. Before the game resumes, Melba succeeds in seducing the Kid. Christian makes a surprise visit to the room, catches them after the fact and walks out on The Kid.

After another break in the game, Slade tells the Kid that Shooter will continue to cheat for him. Despite Slades threats, the Kid tells him he will not allow Shooter to cheat, insisting he will beat Howard without help. Back at the game, The Kid maneuvers to have Shooter replaced by Lady Fingers and wins several major pots from Howard, who is visibly losing confidence.

===The final hand===
The Kid is on the Button (poker)|button.  

Lady Fingers deals Howard the  8♦  and the Kid the 10♣. The Kid bets $500 and Howard calls. Howard gets the  Q♦  and the Kid the 10♠. 

The Kid bets $1,000 and Howard raises $1,000. The Kid calls. 

Lady Fingers deals Howard the  10♦  and the Kid gets the A♣. The Kid bets $3,000 and the Man calls. 
 all in. Howard reaches into his wallet and raises another $5,000. The Man agrees to take his marker and the Kid calls the bet. 

Howard turns over the  J♦  for a queen-high straight flush. The Kid turns over the  A♥ , to show his bad beat with a full house, Aces full of tens.

Following the game, the Kid leaves the hotel and loses a penny pitch to a shoe shine boy he had beaten at the same game at the films opening. Around the corner, he runs into Christian and they embrace.

===Alternate versions===
In some cuts, the film ends with a Freeze frame shot|freeze-frame on Steve McQueens face following his penny-pitching loss. Turner Classic Movies and the DVD feature the ending with Christian. Jewison wanted to end the film with the freeze-frame but was overruled by the producer. 

The cockfight scene was cut by British censors. {{cite web
  | first =BBC4
  | title =The Cincinnati Kid Review
  | work =Channel4.com
  | url =http://www.channel4.com/film/reviews/film.jsp?id=102072&section=censor
  | accessdate =2007-07-30}} 

==Cast==
  
* Steve McQueen as Eric "The Kid" Stoner
* Edward G. Robinson as Lancey "The Man" Howard  
* Karl Malden as Shooter
* Ann-Margret as Melba
* Tuesday Weld as Christian
* Joan Blondell as Lady Fingers
* Rip Torn as Slade
* Theodore Marcuse as Felix
* Midge Ware as Mrs. Slade
 
* Jack Weston as Pig
* Cab Calloway as Yeller
* Jeff Corey as Hoban
* Milton Selzer as Sokal
* Karl Swenson as Mr. Rudd
* Émile Genest as Cajun
* Ron Soble as Danny
* Dub Taylor as the first dealer
* Sweet Emma Barrett as the blues singer
 

==Production==
The Cincinnati Kid was filmed on location in New Orleans, Louisiana, a change from the original St. Louis, Missouri, setting of the novel. Spencer Tracy was originally cast as Lancey Howard, but ill health forced him to withdraw from the film. {{cite book  
| last =Deschner | first =David 
| title =The Complete Films of Spencer Tracy 
| publisher = Citadel Press 
| year =1993 
| isbn = 
| page =57 }}  Sam Peckinpah was originally hired to direct;  producer Martin Ransohoff fired him shortly after filming began  for "vulgarizing the picture." {{Cite news
  | last =Carroll| first =E. Jean
  | title =Last of the Desperadoes: Dueling with Sam Peckinpah
  | newspaper =Rocky Mountain Magazine
  | date =March 1982
  | postscript = }}  Peckinpahs version was to be shot in black-and-white to give the film a 1930s period feel. Jewison scrapped the black-and-white footage, feeling it was a mistake to shoot a film with the red and black of playing cards in greyscale. He did mute the colors throughout, both to evoke the period and to help pop the card colors when they appeared. 

The film features a theme song performed by Ray Charles  and a brief appearance during the film by The Preservation Hall Jazz Band, with Emma Barrett as vocalist and pianist.

===Notes on the game=== string bets," though players (including Howard) go on to make string bets during the game. open stakes. This is unusual in modern times and almost never allowed in casinos, but permissible in home games and was common for the time period of the film. {{cite web
  | last =Ciaffone | first =Robert
  | title =Roberts Rules of Poker &mdash; Version 6
  | publisher =Pokercoach.us
  | url =http://pokercoach.us/RobsPkrRules6.htm
  | pages =
  | accessdate =  2007-08-04}}  
* The unlikely nature of the final hand is discussed by  , "the odds against any full house losing to any straight flush, in a two-handed game, are 45,102,781 to 1," with Holden continuing that the odds against the particular final hand in the movie are astronomical (as both hands include 10s). Holden states that the chances of both such hands appearing in one deal are "a laughable" 332,220,508,619 to 1 (more than 332 billion to 1 against) and goes on: "If these two played 50 hands of stud an hour, eight hours a day, five days a week, the situation would arise about once every 443 years."
* Notwithstanding the previous sources credentials, the actual odds of any straight flush being dealt in the same deal as a full house in a heads up game are calculated to be much less than stated by the previous source. It can be shown to be calculated as actually 1 in 23,461,900. Further, even narrowing it down specifically to a queen high flush vs. aces full of tens still only has a 1 in 41,527,563,578 probability which is much less than the 332 billion number cited above. 
* In any case, the odds against such a scenario are irrelevant. There have certainly been millions upon millions of poker hands played throughout history, and the film can simply be seen as the depiction of a rare yet possible event.

==Soundtrack==
 

==Reception==
Upon its 1965 release, The Cincinnati Kid was favorably reviewed by Variety (magazine)|Variety which wrote "Martin Ransohoff has constructed a taut, well-turned-out production. In Steve McQueen he has the near-perfect delineator of the title role. Edward G. Robinson is at his best in some years as the aging, ruthless Lancey Howard...." {{cite news 
  | author =Variety staff
  | title =Review| url =http://www.variety.com/review/VE1117789909.html?categoryid=31&cs=1&p=0 Variety
  | accessdate = 2007-07-30 Howard Thompson The Hustler, to which it bears a striking similarity of theme and characterization."   Time (magazine)|Time magazine also noted the similarities to The Hustler, saying "nearly everything about Cincinnati Kid is reminiscent" of that film, but falls short in the comparison, in part because of the subject matter: 
:Director Jewison can put his cards on the table, let his camera cut suspensefully to the players intent faces, but a pool shark sinking a tricky shot into a side pocket undoubtedly offers more range. Kid also has a less compelling subplot. Away from the table, McQueen gambles on a blonde (Tuesday Weld) and on the integrity of his dealer pal, Karl Malden. Pressure comes from a conventionally vicious Southern gentleman (Rip Torn), whose pleasures include a Negro mistress, a pistol range adjacent to his parlor, and fixed card games. As Maldens wife, Ann-Margret spells trouble of another kind, though her naive impersonation of a wicked, wicked woman recalls the era when the femme fatale wore breastplates lashed together with spider web. By the time all the bets are in, Cincinnati Kid appears to hold a losing hand.

A retrospective review published by the New York State Writers Institute of the University at Albany also noted the similarities the film had to The Hustler, but in contrast said The Cincinnati Kid s  "stylized realism, dreamlike color, and detailed subplots give   a dramatic complexity and self-awareness that The Hustler lacks. 
 Golden Globe nomination for Best Supporting Actress. Motion Picture Exhibitor magazine nominated Robinson for its Best Supporting Actor Laurel Award.

==Home media==
The Cincinnati Kid was released on Region 1 DVD on May 31, 2005. The DVD features a commentary track by director Norman Jewison, commentary on selected scenes from Celebrity Poker Showdown hosts Phil Gordon and Dave Foley and The Cincinnati Kid Plays According to Hoyle, a promotional short featuring magician Jay Ose. A Blu-Ray DVD came out on June 14th, 2011. {{cite web
  | publisher = Barnes & Noble
  | title =Cincinnati Kid, The (DVD)
  | work =
  | date =
  | url =http://video.barnesandnoble.com/search/product.asp?ean=012569698628&z=y&displayOnly=Menu
  | accessdate = 2007-09-13}} 

With the release of the film on DVD, one modern reviewer said the film "is as hip now as when it was released in 1965" {{cite web
  | last =Cullum | first =Brett
  | title =DVD Verdict Review: The Cincinnati Kid
  | publisher =DVD Verdict
  | date =June 13, 2005
  | url =http://www.dvdverdict.com/reviews/cincinnatikid.php
  | accessdate = 2007-09-11 }}  and another cited McQueen as "effortlessly watchable as the Kid, providing a masterclass in the power of natural screen presence over dialogue" and Robinson "simply fantastic." {{cite web
  | last =Sutton| first =Mike
  | title =The Cincinnati Kid
  | publisher =DVD Times
  | date =June 20, 2005
  | url =http://www.dvdtimes.co.uk/content.php?contentid=57514
  | accessdate = 2007-09-11 }}    Poker author Michael Wiesenberg calls The Cincinnati Kid " ne of the greatest poker movies of all time." {{Cite news
  | last = Weisenberg  | first =Michael
  | title =Implausible Play in The Cincinnati Kid? A play-by-play analysis of a highly unlikely poker hand
  | newspaper =Card Player Magazine
  | date =August 23, 2005
  | url =http://www.cardplayer.com/cardplayer-poker-magazines/65570-jennifer-tilly-18-16/articles/14928-implausible-play-in-the-cincinnati-kid-a-play-by-play-analysis-of-a-highly-unlikely-poker-hand
  | postscript =   }} 

==References==
Notes
 

==External links==
*  
*  
*  

 

 

 

 
 
 
 
 
 
 
 
 
 
 
 