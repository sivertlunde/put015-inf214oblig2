Cyborg 2
{{Infobox film
| name           = Cyborg 2
| image          = Cyborg2poster.jpg
| caption        = Video release poster Michael Schroeder
| producer       = Raju Patel Alain Silver Michael Schroeder Mark Geldman Ron Yanover
| starring       = Elias Koteas Angelina Jolie Jack Palance Billy Drago Karen Sheperd Allen Garfield Renee Griffin
| music          = Peter Allen
| cinematography = 
| distributor    = Trimark Pictures
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Cyborg 2, released in some countries as Glass Shadow, is a 1993  .

== Plot ==
In the year 2074, the cybernetics market is dominated by two rival companies: USAs Pinwheel Robotics and Japans Kobayashi Electronics. Cyborgs are commonplace, used for anything from soldiers to prostitutes. Casella "Cash" Reese (Jolie) is a prototype cyborg developed for corporate espionage and assassination. She is filled with a liquid explosive called "Glass Shadow". Pinwheel plans to eliminate the entire Kobayashi board of directors using Casella to precipitate a hostile takeover and obtain a monopoly over the cyborg market. She is programmed to mimic human senses and emotions such as fear, love, pain and hate. Guided by Mercy (Palance), a renegade prototype cyborg who can communicate through any electronic device, she and her combat trainer Colton Ricks (Koteas) escape the Pinwheel facility so she can avoid self-destruction, something that most corporate espionage cyborgs face. Theyre relentlessly pursued by Pinwheels hired killer or "wiretapper", Daniel Bench (Drago).

==Cast==
* Elias Koteas as Colton "Colt 45" Ricks
* Angelina Jolie as Casella "Cash" Reese
* Jack Palance as Mercy
* Jean-Claude Van Damme as Gibson Rickenbacker (flashback)
* Billy Drago as Danny Bench
* Karen Sheperd as Chen
* Allen Garfield as Martin Dunn
* Renee Griffin as Dreena
* Vincent Klyn as Fender Tremolo (flashback)

== Reception ==
Alan Jones of the Radio Times rated the film 3 out of 5 stars and called it a "Blade Runner-inspired violent fantasy".   Entertainment Weekly selected Cyborg 2 as Jolies worst film. 

Jolie has said that after she saw the film, she "went home and got sick". 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 

 