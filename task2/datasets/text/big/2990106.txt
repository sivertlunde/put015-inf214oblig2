Tarzan's New York Adventure
{{Infobox film
| name           = Tarzans New York Adventure
| image          = Tarzans New York Adventure movie poster.jpg image size      = 200px Theatrical poster
| director       = Richard Thorpe 
| producer       = Frederick Stephani
| writer         = William R. Lipman Myles Connolly
| based on       =  
| starring       = Johnny Weissmuller Maureen OSullivan Johnny Sheffield David Snell
| cinematography = Sidney Wagner
| editing        = Gene Ruggiero
| studio         =Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer|Loews Inc.
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         = $707,000 "The Eddie Mannix Ledger". Margaret Herrick Library, Center for Motion Picture Study (Los Angeles).  gross = $2,729,000 
}}

Tarzans New York Adventure (aka Tarzan Against the World ) is a 1942 film, the sixth Tarzan film to feature actors Johnny Weissmuller and Maureen OSullivan. This film was the sixth and final film in MGMs Tarzan series and was the studios last Tarzan film until their 1958 release, Tarzans Fight for Life, Tarzans New York Adventure was directed by Richard Thorpe, and although it included New York scenes, as well as the customary jungle sequences, it was another Tarzan production primarily shot on MGM backlots.  

==Plot==
Circus workers land an aircraft in the jungles of Africa in search of lions for their show. While trapping lions, the three men meet up with Tarzan (Johnny Weissmuller), Jane (Maureen OSullivan) and their adopted son Boy (Johnny Sheffield). Watching Boys tricks with the elephants, the head of the circus, Buck Rand (Charles Bickford), realizes that Boy would be a great act for the circus. The group is attacked by natives, and it appears that Tarzan and Jane have perished in a jungle fire. The men take Boy on an aircraft back to the United States. Tarzans loyal chimp Cheeta wakes Tarzan and Jane before they are burned by the fire. Then Cheeta tells Tarzan that Boy has left with the men on the plane. 

Tarzan, Jane and the chimp track across the jungle and flying across the Atlantic, eventually end up in New York City where Tarzan is befuddled by the lifestyle and gadgetry of "civilization". Tarzan displays his quaint, "noble savage" ways by complaining about the necessity of wearing clothing, commenting that an opera singer that he hears on a "noisy box" is "Woman sick! Scream for witch doctor!", and expressing his childlike wonderment at taxi cabs. It is noteworthy that Tarzan comments that various African-Americans he sees making a living throughout New York City are from this or that tribe back in their jungle home. 
 Charles Lane), who tricks Jane into admitting that Boy was not born in the jungle and is not her actual child, provoking Tarzan into attacking him in the courtroom. Tarzan makes a daring escape out the courtroom windows and after a rooftop chase by the police ends up doing a high dive off the Brooklyn Bridge into the East River. 

Tarzan somehow finds the circus where Boy is being held and enlists the aid of circus elephants who are chained to stakes. He calls to them with his "jungle speak" and they take their revenge on their tormentors by tearing free from the chains and destroying the circus. In the ensuing bedlam, Tarzan is able to rescue Boy, and before the family returns to Africa,  the judge grants Tarzan and Jane full custody of Boy.

==Cast==
 
* Johnny Weissmuller as Tarzan
* Maureen OSullivan as Jane
* Johnny Sheffield as Boy
* Virginia Grey as Connie Beach
* Charles Bickford as Buck Rand, Circus owner Paul Kelly as Jimmie Shields, Pilot
* Elmo Lincoln as Circus Roustabout
* Chill Wills as Manchester Montford 
* Cy Kendall as Colonel Ralph Sergeant  Russell Hicks as Judge Abbotson 
* Howard C. Hickman as Blake Norton, Tarzans Lawyer (credited as Howard Hickman)  Charles Lane as Gould Beaton, Sargents Lawyer 
* Miles Mander as Portmaster 
* Anne Jeffreys as Young woman  William Forrest as Inspector at airport
* Willie Fung as Sun Lee, the Chinese tailor  
* Marjorie Deanne as Cigarette girl 
* Eddie Kane as Eddie, the Headwaiter 
* Mantan Moreland as Sam, the Nightclub janitor  
* Dorothy Morris as Hat check girl
 

==Production==
Production with the working title of Tarzan Against the World, began on December 17 1941, continuing to January 28, 1942, mainly on the MGM backlot/ranch. Additional scenes were shot in early February 1942.  

Although popular mythology claims that Johnny Weismuller did his own stunt in Tarzans New York Adventure and, as an escaping Tarzan, actually jumped 250 feet from the Brooklyn Bridge. According to the ERBzine research on Edgar Rice Burroughs, the shot was filmed by cameraman Jack Smith on top of the MGM scenic tower on lot 3 and used a dummy plunging into a tank.  

Tarzans New York Adventure was the last in the series for MGM, and Maureen OSullivans last picture until 1948. She wanted to devote more time to her seven children. Of interest is the uncredited appearance as a circus roustabout by Elmo Lincoln who in 1918 was the first actor to star as Tarzan.   
 Lockheed 12A with a single tail, that is used to fly in the Africa scenes and a Boeing 314 Clipper that the characters of Tarzan and Jane use to cross the Atlantic Ocean.   

==Reception==
By 1942, the Tarzan series had become tired and the contemporary reviews certainly echoed this view. Film critic Theodore Strauss at The New York Times said the change of outfit did nothing to change the obvious. "With an African yodel and a tailor-made suit, our old jungle friend is back in Tarzans New York Adventure, currently chilling the veins of reviewers and 12-year-olds at the Capitol. Although were not quite certain that the small-fry approved of Tarzans temporary conversion to decidedly dapper duds of the sort more commonly seen at the corner of Hollywood and Vine, he probably will be forgiven. In Tarzans case, clothes do not make the man." 

In a recent appraisal of Tarzans New York Adventure, Leonard Maltin noted some redeeming factors; "... an amusing entry. Tarzans first encounter with indoor plumbing is truly memorable." 

Tarzans New York Adventure earned $1,404,000 in the US and Canada and $1,315,000 elsewhere during its initial theatrical run, making MGM a profit of $985,000. 

==References==
===Notes===
 	

===Bibliography===
 
* Maltin, Leonard. Leonard Maltins Movie Guide 2009. New York: New American Library, 2009 (originally published as TV Movies, then Leonard Maltin’s Movie & Video Guide), First edition 1969, published annually since 1988. ISBN 978-0-451-22468-2.
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 