Dating Do's and Don'ts
 
{{Infobox film name = Dating Dos and Donts image = caption = director = Ted Peshak   Gilbert Altschul producer = Coronet Instructional Media writer = starring = Jackie Gleason   John Lindsay music = cinematography = editing = distributor = Coronet Instructional Media released = 1949 runtime = 13 min country = USA language = English budget = gross =
}} American high adolescents basic University of North Carolina. In this film, the boy is the sole initiator of any contact with the girl, and all arrangements are made under the warm supervision of the family, particularly a mother who is a matriarchal homemaker|housewife.  This film, as many of the other educational shorts of the post World War II era, denotes the traditional socially conservative values that were common in the early to mid 20th century.  His mother, as the film was released in the late 1940s,  was raised during the end of the Victorian Era, in the 1910s or 1920s decade, supposing she is middle aged,   where the custom of "courtship" was commonplace and is new to the "dating" concept, but she accepted it.

The film is one of many public domain films in the Prelinger archives. 

There is a similarly named but unrelated Looney Tunes webtoon.

== Woodys first date ==
The film follows a young adolescent boy, Woody, who receives tickets for "one couple" to the Hi Teen Carnival. At different stages, the film offers options on how Woody might respond to various situations:
# What kind of girl should he date?
# How should he ask her out?
# How should he say good night after the date is over? first date. Woody is cautioned not to ask a girl out based on her looks as she could be aloof or boring. Instead he should ask a girl who is "fun". He is similarly told to be straightforward and not to insist that his potential date give up some other activity for him. Finally, the film depicts the perceived danger of immediately kissing the girl good night, or of just leaving her at her door, and instead urges the viewers to say a friendly goodbye, ending with a promise to call next week.

As Woody prepares for his date with Anne, he receives hints from his older brother, who is already an expert at dating; for instance, Woodys brother tells Woody to act like his "natural, talkative self" while on the phone, and says that Woody does not have to bring Anne flowers on her first date. He also convinces their mother to allow Woody to go on his first date even though he is young, with her adding that it would be acceptable provided that Woody only dates on weekends and comes home at a reasonable hour. As Woody prepares for his date, his mother and father reflect on their own first dates to remind Woody how important it is for him to show up on time. His mother adds that any girl who is not ready for him on time is not worthy of going out with "my boy".

The film ends with Woody leaving the door outside Annes home, whistling happily as he contemplates his next date.

==Looney Tunes webtoon==
A Looney Tunes webtoon, titled Dating Dos and Donts: How to Be a "Mr. Good-Date", starred Bugs Bunny and, for the first and only time in a Looney Tunes short, Lola Bunny. The cartoon begins in black-and-white with a narrator telling the audience how to be a "Mr. Gooddate," the way Reggie Gerandevu (Bugs Bunny) does it. Bugs calls Lola (who doesnt have any quotes in dialogue and responds only by giggling) and asks if she wants go out that night for carrot juice. Lola giggles in agreement, which makes Bugs break the fourth wall by saying that she is a good conversationalist. After Bugs hangs up, Lola gets ready for the date, but her disapproving dad has overheard the phone call. Meanwhile, Bugs is trying to get to Lolas house but cant tell if her address number is an eight or a three. The narrator says its a three and also says theres a perfect shortcut. Bugs thanks the narrator, but he doesnt know that the narrator is actually Lolas dad, trying to lure him away from Lola. The hapless Bugs does not know that has been directed to Elmer Fudds house. Elmer, peeking out the window, tells Bugs to go away. Bugs goes over the fence, triggering Elmers alarm system. A robotic hand picks Bugs up and a robotic shoe kicks him out and he falls onto the street, making him hope that the Warner Bros. Medical Plant cures him. Elmer praises the success of his alarm. Then, Bugs enters his house, dressed up as a scientist saying hes here to recalibrate Elmers alarm system. Bugs triggers the alarm system to hurt and toss Elmer around until finally Bugs adjusts the system to "GOOOAAAL!" which makes Elmer get kicked into space. At Lolas house, Lolas dad says he gave it "the old college try". But then Bugs spots Lola in her room. Bugs gets in her room by using a pole vault. However she is gone. Bugs finds a note reading "Gone to France, so long sucker. - Lolas Dad". Meanwhile, Lola and her dad are sitting at a table in a French restaurant. Pepe Le Pew brings in a serving dish. He removes the lid, to reveal Bugs. An excited Lola kisses Bugs. Lolas dad is steaming about this. Bugs pulls out the remote to Elmers alarm system, adjusting it to "Heads Up!", which makes Elmer get punched back to Earth and land right on top of Lolas dad. Lola then kisses Bugs multiple times.

==See also==
 
*Sex education
*Sexual morality
* 
*Teenage pregnancy
*Sexual abstinence

==References==
 

==External links==
* 
* 

 
 
 
 