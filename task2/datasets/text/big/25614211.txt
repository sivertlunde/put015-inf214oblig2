Stevnemøte med glemte år
{{Infobox Film
| name           = Stevnemøte med glemte år
| image          = 
| image size     = 
| caption        = 
| director       = Jon Lennart Mjøen
| producer       = Bjørn Bergh-Pedersen
| writer         = Jon Lennart Mjøen Bjørn Bergh-Pedersen Olav Dalgard Sigurd Hoel
| narrator       = 
| starring       = Mona Hofland
| music          = 
| cinematography = Gunnar F. Syvertsen
| editing        = 
| distributor    = 
| released       = 22 April 1957
| runtime        = 89 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
| preceded by    = 
| followed by    = 
}}

Stevnemøte med glemte år is a 1957 Norwegian drama film directed by Jon Lennart Mjøen. It was entered into the 7th Berlin International Film Festival.     

==Cast==
* Mona Hofland
* Espen Skjønberg
* Henki Kolstad
* Inger Marie Andersen
* Jon Lennart Mjøen
* Rolf Christensen
* Pål Skjønberg
* Bab Christensen
* Arne Reitan
* Willy Kramer Johansen
* Eva Steen
* Inger Teien
* Haakon Arnold
* Erling Lindahl

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 