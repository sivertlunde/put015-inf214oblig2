El Mar de Lucas
 
{{Infobox film
| name           = El mar de Lucas
| image          = El Mar de Lucas.jpg
| image size     =
| caption        = DVD Cover
| director       = Víctor Laplace
| producer       = Víctor Laplace
| writer         = Víctor Laplace Martín Salinas
| narrator       =
| starring       = Víctor Laplace Pablo Rago
| music          = Damián Laplace
| cinematography = Fabián Giacometti
| editing        = César Custodio Miguel Pérez
| distributor    = Primer Plano Film Group
| released       =  
| runtime        = 97 minutes
| country        = Argentina Spanish
| budget         =
}} Argentine film, directed by Víctor Laplace, and written by Laplace and Martín Salinas.  The story features Víctor Laplace, Pablo Rago, Virginia Innocenti, and others.

==Plot==
The films tells of father, Juan Denevi (Victor Laplace), who gets the surprise of his life on his fiftieth birthday when he finds out that hes become a grandfather.  The movie opens in Buenos Aires, Argentina, and Juan is cooking individual personal favorite dishes for his friends, the patrons of his restaurant.

A young woman named Manuela (Virginia Innocentti) appears with a 3-year-old boy (Lucas) and announces that Lucas is Juans grandson. Juan hasnt seen his son Facundo (Pablo Rago) for many years, and during this time, Facundo has married and had a son.

Facundos wife and son find Juan at his birthday party and beg him to come help Facundo with a family crisis.

==Cast==
* Víctor Laplace as Juan Denevi
* Pablo Rago as Facundo Denevi
* Virginia Innocenti as Manuela
* Ana María Picchio as Clara
* Betiana Blum as Ana
* Rodolfo Ranni as Rolo
* Ulises Dumont as Nacho
* Lautaro Penella as Lucas Denevi
* Norberto Díaz as Secretario del intendente
* Adolfo Yanelli as Carlitos
* David Di Napoli as Martillero
* Pía Uribelarrea as Silvia
* Óscar Guzmán as Lalo
* Antonio Bax as Pedro
* Eugenia Tobal as Marisa

==Awards==
Wins
* Mar del Plata Film Festival: Special Mention, Víctor Laplace, for a first work; 1999.
* Cartagena Film Festival: Golden India Catalina, Best Screenplay, Víctor Laplace; 2000.

Nominations
* Cartagena Film Festival: Golden India Catalina, Best Film, Víctor Laplace; 2000.
* Argentine Film Critics Association Awards: Silver Condor, Best First Film, Víctor Laplace; Best Supporting Actress, Virginia Innocenti; 2001.

==References==
 

==External links==
*  
*   at the cinenacional.com  

 
 
 
 
 
 