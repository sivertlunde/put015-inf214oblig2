Little Men (1934 film)
{{Infobox film
| name           = Little Men
| image          = Little_Men_(1934_film)_poster.jpg
| caption        = Theatrical release poster
| director       = Phil Rosen
| producer       = Nat Levine
| writer         = Louisa May Alcott (novel) Gertrude Orr Ken Goldsmith
| narrator       = Dickie Moore Frankie Darro Tad Alexander
| music          = Hugo Riesenfeld Ernest Miller William Nobles
| editing        =
| distributor    = Mascot Pictures
| released       =  
| runtime        = 72 minutes
| country        = United States English intertitles
| budget         =
| gross          =
| awards         =
}}
Little Men (1934 in film|1934) is an American feature film based on Louisa May Alcotts novel Little Men (1871). The film stars Ralph Morgan, Erin OBrien-Moore, Richard Quine, Frankie Darro, and Tad Alexander, was directed by Phil Rosen, and released by Mascot Pictures. 

==Plot==
The former Jo March (OBrien-Moore), now married to Prof. Bhaer (Morgan), opens a boarding school for wayward boys.

==Cast==
*Ralph Morgan as Prof. Bhaer Dickie Moore as Demi
*Erin OBrien-Moore as Jo March Bhaer
*Junior Durkin as Franz
*Cora Sue Collins as Daisy
*Phyllis Fraser as Mary Anne
*Frankie Darro as Dan
*David Duran as Nat Blake
*Buster Phelps as Dick
*Ronnie Cosby as Rob Bhaer
*Tad Alexander as Jack 
*Tommy Bupp as Tommy Bangs
*Bobby Cox (actor) as Stuffy
*Dickie Jones as Dolly
*Richard Quine as Ned
*Hattie McDaniel as Asia
*Elsie the Wonder Cow

==References==
 

==External links==
* 

 
 
 
 
 