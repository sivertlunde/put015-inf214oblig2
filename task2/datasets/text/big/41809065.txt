Gallant Lady (1942 film)
{{Infobox film
| name           = Gallant Lady
| image          =Gallant Lady 1942.jpg
| imagesize      =
| caption        =
| director       = William Beaudine
| producer       =Lester Cutler, Sam Efrus, Leon Fromkess 
| writer         = Octavus Roy Cohen, Arthur St. Claire
| based on       = 
| starring       = 
| music =
| cinematography =Marcel Le Picard 	
| editing        = Frederick Bain
| studio      =Motion Picture Associates, Inc
| distributor    = 
| released       =   
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Gallant Lady is a 1942 American drama film directed by William Beaudine. It stars Rose Hobart, Sidney Blackmer, Claire Rochelle, and Lynn Starr. 

==Cast==
*Rose Hobart as Rosemary Walsh
*Sidney Blackmer as Steve Carey
*Claire Rochelle as Nellie
*Lynn Starr as Linda
*Jane Novak as Lucy Walker
*Vince Barnett as Baldy
*Jack Baxley as Sheriff Verner
*Crane Whitley as Pete Saunders John Ince as Judge Stevens
*Frank Brownlee as Luke Walker Richard Clarke as Nick Morelli
*Spec ODonnell 	as Ben Walker
*Inez Cole as Jane
*Pat McKee as Jed Hicks
*Ruby Dandridge as Sarah

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 