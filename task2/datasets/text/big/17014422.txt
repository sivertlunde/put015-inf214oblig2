Exit the Dragon, Enter the Tiger
{{Infobox film
| name           = Exit the Dragon, Enter the Tiger
| film name = {{Film name| traditional    = 天皇巨星
| simplified     = 天皇巨星}}
| image          = Exitthedragonposter.jpg
| caption        = 
| director       = Tso Nam Lee
| producer       = Wong Fung
| writer         = Hsin Yee Chang
| starring       = Bruce Li
| music          = Fu Liang Chou
| cinematography = Yip Cheng-Biu
| editing        = 
| studio         = Hong Kong Alpha Motion Picture Company Dimension Pictures
| released       =  
| runtime        = 90 minutes
| country        = Hong Kong
| language       = Mandarin
| budget         = 
| gross          = 
}}
Exit the Dragon, Enter the Tiger (Chinese title: 天皇巨星; Cantonese: Tian huang ju xing), also released as Bruce Lee: The Star of Stars, is a 1976 Bruceploitation film starring Bruce Li as David Lee. The title is a play on the Bruce Lee film Enter the Dragon and is one of the most well-known films in the Bruceploitation genre. 

==Plot==
The film tells the story of Tiger, a student of Bruce Lee, who comes to Hong Kong in search of answers regarding the mysterious death of his master. The character Suzy Yung represents Betty Ting Pei. She and Tiger team up and take on the Hong Kong mafia in search for the truth regarding the death of the martial arts legend. 

==Cast==
* Bruce Li as David Lee/Tiger
* Chang Yi as The Baron
* Lung Fei as Lung Fei
* Shan Mao as Sam
* Kam Kong as Wa

==Music== John Barrys The Man with the Golden Gun, Pink Floyds "Shine On You Crazy Diamond", and Isaac Hayes theme to Three Tough Guys, among others.

==References==
 

==External links==
*  .
*  

 
 
 
 
 

 