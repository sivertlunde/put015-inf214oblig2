Blown Away (1992 film)
 
For the 1994 film of the same name starring Jeff Bridges and Tommy Lee Jones, see Blown Away (1994 film).
{{Infobox film
| name           = Blown Away
| image          = Blown_Away_Corey_Haim.jpg
| caption        =
| alt            =
| writer         = Robert C. Cooper Jean LeClerc Kathleen Robertson
| director       = Brenton Spencer
| producer       = Peter R. Simpson
| distributor    = Norstar Entertainment Inc.
| released       =  
| runtime        = 92 minutes
| music          = Paul Zaza
| country        = Canada
| language       = English
| budget         =
}}
Blown Away is a 1992 erotic thriller film starring Corey Haim, Nicole Eggert and Corey Feldman.

== Plot == Jean LeClerc). Grateful, she invites him to her party, where she thanks him again by having sex with him in her fathers bed. The next morning, Cy comes home and almost catches them. Even though he does not, he angers Megan by forbidding her to see any guys. Unlike Cy, Richs girlfriend Darla (Kathleen Robertson) does find out about the affair, and dumps him. Rich wastes no time and enters into a passionate relationship with Megan. She soon introduces him to her father, but he disapproves of him, and Megan tells Rich that they cannot see each other any longer.

Devastated, Rich turns to his womanizing half-brother Wes (Corey Feldman) for comfort, who encourages him to do everything to get Megan back. He follows her to a bar, where she is seen giving a large sum of money to a criminal-looking man. Rich catches her getting intimate with the guy, and knocks him down as a response. Megan then apologizes to Rich, and claims that she did not think that he really loved her and was only testing him how much he would be willing to do for her. He immediately takes her back and they accompany each other to a bar, where Wes always hangs out with his friends. While Rich is arguing with Darla, who accuses him of going out with her only because of her money, Megan is seen having an argument with Wes. As they go to their home together, they run into Cy, who calls her daughter a slut for bringing a boy home, resulting into a huge fight. Rich decides to go home, where he finds Wes sleeping with Darla. Enraged, he tries to beat up Wes, but Darla stops them by attending Rich that he does not own her. Rich decides to return to Megans place, where she - fed up with fighting with her father - convinces him that her father killed her mother and that they should kill him, and run off with the money. Rich, blinded by the potently sexual relationship, is in two minds about what to do.

The next day, Rich and Wes are shocked to find out that Darla has been killed in a horse riding accident. Meanwhile, Megan turns out hospitalized for whom she claims to Rich is her father to blame. Rich, seeing how severely beaten up she is, fears of losing her someday to Cys abuse, and promises her to help her. As they return home, Megan tells Rich that she has placed a bomb in his bike, and that it will all be over soon. The next morning, Rich is invited by Cy to accompany him on a bike ride, causing Rich to witness the explosion that throws him almost off a cliff. As Cy falls to his death, he tells Rich that he did not kill his wife. Rich starts to suspect that Megan may not be who he thinks she is, and meanwhile, he becomes the prime suspect in Cys death in the investigation of Detective Anderson (Gary Farmer). Despite Andersons attempts to make him turn in Megan, Rich denies any involvement in the entire ordeal, though evidence points against him. Wes is shocked that his brother would have killed anyone, and is mad at him for not having killed their own abusing father.

Shortly later, Megan bails out Rich, and gives him her car to skip town with. She promises him that she will follow him soon after collecting her fathers money. Rich drives off, but, not trusting her anymore, checks the car and finds a bomb in it. He is able to get away just in time for the explosion and immediately rushes to her house. There, it is revealed that Megan and Wes were lovers all along and planned the murders and schemes together in order to be together. Rich confronts his half-brother, and Wes informs him that he tried to frame him because he has always hated him. As Wes is about to shoot Rich, Megan kills Wes. She tries to put the entire blame on Wes, but Rich does not believe her, prompting her to reveal that she was the mastermind behind it all. As she tries to shoot Rich, the police arrive, killing Megan immediately in self-defense. Rich was wired throughout the final scene, which clears him from all charges, though he has nobody important in his life anymore, leaving him empty inside.

== Cast ==
* Corey Haim as Rich Gardner
* Nicole Eggert as Megan
* Corey Feldman as Wes Jean LeClerc as Cy
* Kathleen Robertson as Darla Hawkes
* Gary Farmer as Detective Anderson
* Jason Hopley as Rody
* Alessandro Bandiera as Davis
* Rob Stefaniuk as Dave
* Rex Hagon as Marshall
* Lindsay Jamieson as Megans Mother

== External links ==
*  

 
 
 
 
 
 
 
 
 