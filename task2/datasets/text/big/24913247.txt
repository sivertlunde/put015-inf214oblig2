The Box of Life
{{Infobox film
| name           = The Box of Life
| image          = 
| caption        = 
| director       = Usama Muhammad
| producer       = Muhammad al-Ahmad Xavier Carniaux Elisabeth Marliangeas
| writer         = Usama Muhammad
| starring       = Meerna Ghannam
| music          = 
| cinematography = Elso Roque
| editing        = Martine Barraqué
| distributor    = 
| released       = May 2002
| runtime        = 112 minutes
| country        = Syria France
| language       = Arabic
| budget         = 
}}

The Box of Life ( ,  ) is a 2002 Syrian-French drama film directed by Usama Muhammad. It was screened in the Un Certain Regard section at the 2002 Cannes Film Festival.   

==Cast==
* Zuhair Abdulkarim - 3rd Man
* Caresse Bashar - 2nd Woman
* Elias Ghannam - 3rd Boy
* Meerna Ghannam - Fairouza
* Muhammad Hamad - 2nd Boy
* Fares Helou - 2nd Man
* Nihal Khateeb - Kubra
* Bassam Kousa - 1st Man
* Ali Muhammad - Akhdar
* Amal Omran - 1st Woman
* Hala Omran - 3rd Woman
* Maha Saleh - Khadra
* Mustafa Sbeih - 1st Boy
* Rafeeq Subaiee - Akbar

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 