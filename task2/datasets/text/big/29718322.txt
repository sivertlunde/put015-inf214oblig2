Marykkundoru Kunjaadu
 
 
{{Infobox film
| name           = Marykkundoru Kunjaadu
| image          = MarykkundoruKunjaadu.jpg
| caption        = Marykkundoru Kunjaadu theatrical poster
| alt            = Shafi
| producer       = Vaishakh Movies and Ansil James
| writer         = Benny P Nayarambalam Dileep  Bhavana Biju Vijayaraghavan Vinaya Prasad Jagathy Sreekumar
| music          = Songs:  
| cinematography = Shamdat
| editing        = Manoj Bhai
| studio         =
| distributor    = Vysakh Cinemas & PJ Entertainments
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Malayalam
| budget         =   http://www.sanscinema.com/2011/01/marykkundoru-kunjaadu-5-2-crores-share-in-5-weeks/ 
| gross          =   
}} Malayalam comedy Bhavana and Biju Menon in the lead roles. The film was released on 25 December 2010 which went on to become a blockbuster completing 150 days and eventually became the highest grossing Malayalam film of 2010.

A Sequel for the movie is announced.

==Synopsis==
The story takes place in a remote village where the majority of the people are Christians. Solomon (Dileep (actor)|Dileep), the son of GeeVarghese Kapyaar (Vijayaraghavan (actor)|Vijayaraghavan), a simple soul who is quiet in nature and fears virtually everything around, thus earning the nickname Kunjaadu (little lamb). His mother, Mary (Vinaya Prasad),was Ittichans (Innocent (actor)|Innocent) (a rich landlord) crush. He named his daughter Mary (Bhavana) after Solomons mother.  He wanted Solomon to love his daughter dearly and then he would marry her to some other person so that he gets to know the pain that he felt when he lost his lover. He promises the church that he will give another cross made out of gold to the church if Solomon marries some other girl but if he wants to marry his daughter then Solomon must give a cross to church. Mary (Bhavana) has been in love with Solomon right from their childhood days. Meanwhile, a man (Biju Menon) with rugged looks comes to Solomons house and Solomon, who is overjoyed at getting some fearless company, gets the man accepted by his whole family under the guise that he is his long-lost brother Jose. He does so much for the family and gets involved in agriculture and develops the families living. Later, Solomon finds that there is more to his so-called brother than he thought initially,where he has killed a girl and was imprisoned for 7 years. Finally he finds that Jose has stolen the cross and hid it in the place where Solomons house was 7 years ago and came again to take it. Solomon warns his family that even if is their brother, he should be watched or else hell burn down the house. their parents dont take it seriouly but later when the family is away, Jose burns the house down and everyone thinks it is Solomon who did it. Solomon later finds out that Joses old friend who also wants the golden cross was the one who killed the girl, who was joses wife. A fight occurs between Solomon and both the thieves. Joses friend get smashed down and rings the church bell telling that he got cross and saves jose by telling that he was the one who help him capture the thief, and because Solomon always considered him as a brother. everyone becomes happy and Ittichan gives his daughters hand to Solomon.

==Cast== Dileep as Solomon
* Biju Menon as Jose, whose actual name is Vishnu
* Bhavana as Mary, Solomons love interest Vijayaraghavan as GeeeVarghese Kapyaar, Solomons father
* Vinaya Prasad as Marychedathy, Solomons mother
* Jagathy Sreekumar as Kundukuzhy Achan, the priest Innocent as Ittichan, Marys father
* Salim Kumar as Lolappan, the coffin dealer and Solomons friend.
* Sreelatha Namboothiri
* Kochu Preman as Marys uncle
* Nimisha Suresh as Sisily, Solomons sister
* Ambika Mohan as Marys mother
* Ponnamma Babu as Chanda Mariya
* Chali Pala ... Thoma
* T. P. Madhavan .... Dr Pisharady
* Jai as Solomons sister Anand as Johnykutty, Marys brother
* Subbalakshmi
* Kalabhavan Haneef as Bus conductor
* Appa Haja as Marys brother
* Sajitha Betti as Zelin
* Renjusha Menon as Lolappans wife
* Vijayan

{{Infobox album
| Name        = Marykkundoru Kunjaadu
| Artist      = Berny-Ignatius
| Type        = soundtrack
| Image       =
| Cover       = Marykkundorukunjaadu01.jpg
| Released    =  
| Recorded    = 2010 Feature film soundtrack
| Length      = 20:75
| Label       =  
| Music       = Berny-Ignatius
| Producer    = Vyshaka Rajan
}}
The soundtrack of the film was released on 20 December 2010. It had music scored by composers Berny-Ignatius". Lyrics were provided by Anil Panachooran. There was no special launch for the audio release and the music were given straight through the audio sellers.
{{Track listing
| all_lyrics = Anil Panachooran
| extra_column = Singer(s)
| title1 = Enttadukke | extra1 = Shankar Mahadevan, Rimi Tomy, Pappukutty Bhagavathar, Subhalaxmi | length1 = 4:35
| title2 = Pancharachiri | extra2 = Franco, Sithara Krishnakumar | length2 = 4:33
| title3 = Kunjaade Kurumbanade | extra3 = Madhu Balakrishnan | length3 = 4:29
| title4 = Changathi Kuyile | extra4 = Master Anuragh, Kumari Yogini V Prabhu | length4 = 3:29 Sithara | length5 = 4:29
}}

==Critical reception==
The film is one of the blockbusters of 2010.  IMDB gave the film a 7.1 rating, which indicates high performance.  Nowrunning.com gave the film 2/5 stars stating that "Shafis new film might definitely appeal to people who are on the lookout for a few nontoxic laughs. And for those who wish to see Dileep in roles that offer some substance, this is it."    The movie is a commercial success. The film ran for more than 150 days. This movie turned out to be one of the biggest hits for Dillep. It also was the Biggest Hit of 2010.

== References ==
 
 

== External links ==
*  

 
 
 
 