The Pied Piper of Cleveland
{{Infobox film
| name           = The Pied Piper of AJ The Glo gang
| image          =
| image_size     =
| caption        = Arthur Cohen 
| producer       = Bill Randle
| writer         =
| narrator       =
| starring       = Bill Randle
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
}}
 American musical Arthur Cohen directed the film, which was produced by Bill Randle himself.

Included in the film was live footage shot at several live shows at local high schools and auditoriums on and around October 20, 1955. Performers featured included Elvis Presley, Bill Haley and His Comets, Pat Boone, LaVern Baker, Roy Hamilton, Johnnie Ray and others. This was the first film Presley ever appeared in, and is the "movie short" referred to by Randle when he introduced Presley on his first national TV appearance on Stage Show in early 1956. It was Bill Haleys second film appearance after his group appeared in the 1954 short film, Round Up of Rhythm.

A plaque commemorating one of the filmed performances is located at Brooklyn High School in the Cleveland area of Ohio, and was installed by the Rock and Roll Hall of Fame.

The original forty-eight minute film was supposed to be cut down to a twenty minute "short" for national distribution, but never made it that far. As of 2005, 50 years after it was produced, the movie remains unreleased. There is some dispute over whether or not this film actually exists, although it was shown publicly, albeit only once in Cleveland, and excerpts were also aired on a Cleveland television station in 1956.

According to music historian Jim Dawson, Randle, before his death, sold the rights to the film to PolyGram, although it has been reported that Universal Studios has the negatives of the film in its vaults.

==The Alien Autopsy connection== Alien Autopsy" film circulated by producer Ray Santilli. According to Santilli, he was attempting to obtain a copy of Pied Piper from a man who claimed to have been the cinematographer on the film. Instead of the rare Elvis film, Santilli claims, the cameraman offered him footage he claimed to have shot in the late 1940s of an alleged autopsy of an alien recovered from one of the UFO crash sites.

The   centered around this footage, which was later debunked (by FOX) as being a hoax. This has led to some claims that Pied Piper is likewise a hoax, however documentation supporting the films existence predates the Alien Autopsy affair by many years.

==References==
* Jim Dawson, Rock Around the Clock: The Record That Started the Rock Revolution (San Francisco: Backbeat Books, 2005)
* Roger Lee Hall, Shake, Rattle and Roll: Electric Elvis and Bill Randle (Stoughton: PineTree Productions, 2010).
* Bert L. Worth and Steve D. Tamerius, Elvis: His Life from A to Z (Chicago: Contemporary Books, 1990).

==External links==
*  
*  

 

 
 
 
 