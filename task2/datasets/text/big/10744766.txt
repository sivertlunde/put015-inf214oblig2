Asambhav
 
 

{{Infobox film  name     = Asambhav image    = Asambhav.jpg caption  = Movie poster for Asambhav director = Rajiv Rai producer = Gulshan Rai cinematography = Sukumar Jatania writer   = Rajiv Rai starring = Arjun Rampal Priyanka Chopra music    = Viju Shah released =  23 July 2004 distributor = Trimurti Films  runtime  = 149 minutes country = India Switzerland language = Hindi  budget   = gross =   
}} action Thriller thriller directed by Rajiv Rai and produced by Gulshan Rai under Trimurti Films banner. The film was released on 23 July 2004, starring Arjun Rampal and Priyanka Chopra in the lead roles. Naseeruddin Shah is featured as the villain with Dipannita Sharma, Tom Alter, Milind Gunaji and Sharat Saxena playing supporting roles among others.  The film was shot entirely in Switzerland. 

Rajiv Rai had previously worked with Arjun Rampal in Pyaar Ishq Aur Mohabbat (2001) .Rai signed him for a second time for Asambhav in 2002. The work on the movie had begun in 2002 when he started to create his new project and titled it Asambhav. Aishwarya Rai was initially signed to act as Rampals heroine but was replaced by Priyanka Chopra. Legendary actor Naseeruddin Shah was cast as the villain. The shooting of the film began in June 2003 and it took two months to complete the shooting. The shooting was completed in September 2003.    It was the first Indian film to be graded digitally on Luster.   

The film was launched in March 2004. The soundtrack was composed by Viju Shah and it was released on 26 May 2004. The film was released on 23 July 2004. Through an extensive post-production work and huge promotion, Asambhav was an average grosser at the box office, collecting   nett. It grossed   in India and was average in overseas as well with a collection of US$19,504 in United States and Pound (currency)|£16,894 in United Kingdom.   

==Plot==
Captain Adit Arya (Arjun Rampal) is a special agent of the Indian army. When the Indian president Veer Pratap Singh (Mohan Agashe) is kidnapped by Rafiq Mabroz (Shawar Ali) in exchange for cash rewarded by renowned terrorist Youssan Baksh (Mukesh Rishi) and General Ansari (Milind Gunaji), Adit is sent undercover to rescue the president and his daughter, Kinjal (Dipannita Sharma). The mission is coded "Asambhav". Adit poses as a journalist for Indiatimes, and manages to make his way to Switzerland with the help of a RAW agent called Atul Bhatnagar (Jameel Khan). On the flight, Adit meets Alisha (Priyanka Chopra). Alisha is a singer, and has come to Switzerland under a contract in order to do a show with Sam Hans (Naseeruddin Shah). Her friend Shilpa (Chitrita Sharma) accompanies her. However the people who Alisha and Shilpa have come to Switzerland with, are drug smugglers. Shilpa is murdered when she finds this out, and Alisha pretends she knows nothing. She meets up with Adit, and together they help each other, during which Adit and Alisha fall in love with each other.
 Yashpal Sharma) is a member of the Indian embassy. He is investigating the files of the Indians in Switzerland, during which he reads the file of Rafiq Mabroz and comes to know that he is a terrorist. He informs Hans about the situation and Hans decides to fight ISI. However, he signs a deal with ISI by kidnapping the president from Youssan Baksh and returning the president to Baksh in exchange of 50 Million Pounds.

Parmar suspects that someone of the Indian embassy is a traitor and is helping the terrorists. He finds out that Ms. Brar (Tora Khasgir) is a traitor. He informs the ambassador G.L. Sarin (Sharat Saxena) about the whole situation and is attacked by Mabroz. Parmar is unconscious. Upon regaining consciousness, he discovers that Ms. Brar and Sarin are both traitors and are associates of Mabroz. Mabroz kills Parmar and Sarin leaves a suicide note beside Parmar. Capt. Arya and Bhatnagar visit Parmar in his room and are surprised to see him dead. They read the suicide note and inform the embassy about it, during which Arya reveals that the ambassador G.L. Sarin is the traitor because of his head-stamp on the letter. Arya tries to finish Sarin but Brar calls the police to get Arya and the ambassadors arrested. Arya shows the police his card that he is an army member while Sarin and Brar flee. Arya chases them and Gazi (Chetan Hansraj) tries to attack Arya. Arya runs after Gazi and is attacked by ISI agents and Youssans men in a fort. They try to kill Arya but Arya fires back and kills all of them including Gazi.

Adit reaches Youssans headquarters, where the president is kept captive. He kills General Ansari and all of Youssans men. Youssan takes the president and flees. Adit chases him. On the other hand, Sam has captured Alisha and Kinjal, but is stopped by Ambassador Sarin, who has Hanss friend Brian as captive. Hans gives them the money in exchange for Brians safety but Sarin kills Brian. Hans kills Mabrozs men. Ms. Brar and Volga (Anupama Verma) chase Alisha and Kinjal until they trap them near a helicopter. Brar and Volga kill each other while firing at Alisha and Kinjal, who dodge the attack. Sarin tries to finish Hans but Hans kills him to avenge the death of Brian.

Youssan tries to finish the president but Adit kills Youssan and saves the president. Alisha and Kinjal join Adit and the president. Mabroz tries to fire Adit but Adit kills him. In the end, Adit meets Sam and Sam confesses all the sins that he committed throughout his life and promises to improve himself. Adit and Sam make friends after being the arch rivals throughout the Mission Asambhav. Adit goes back to India with his girlfriend Alisha, Kinjal, President and Atul Bhatnagar.

==Cast==

===Lead cast===
*Arjun Rampal as Captain Adit Arya
:He is the protagonist of the movie. He is an Indian Commando. The objective of Captain Aryas is to save the president from the Al-Amaz terrorist group led by Youssan Baksh.
*Priyanka Chopra as Alisha
:She is the female protagonist. She is the love interest of Adit. Alisha is a struggling singer from India. She is trapped by Dabral, Sam Hans and Brian in a drug smuggling plan and she seeks Adit for help.
*Naseeruddin Shah as Sameer "Sam" Hans
:He is very important for the movie. Sam is a rich drug dealer. He is originally from Delhi, but he resides in Switzerland.

===Supporting cast===
*Dipannita Sharma as Kinjal
:She is the bright daughter of the president. Kinjal studies at a university in the USA.
*Mohan Agashe as president Veer Pratap Singh
:He is a man with wisdom. He is supposed to be relaxing in Switzerland, but is kidnapped by Mabroz. He is freed from Mabroz, but then kidnapped by Sam Hans, who sells the president to Youssan, before finally being saved by Captain Adit Arya. Yashpal Sharma as Ranjit Parmar
:He is a member of the Indian Embassy in Switzerland. He has unintentionally passed the tender to Rafiq Mabroz for the vacations of the Indian president, for which he is upset. He informs his friend Sam Hans about the entire situation as well as the Indian Embassy. However, Embassy members G.L. Sarin and Ms. Brar are revealed to be Mabrozs allies and traitors of the country, during which Mabroz kills Parmar.
*Tom Alter as Brian
:He is the influence of Sam Hans. Brian is the only Christian in the film. Brian is responsible for involving Alisha in the drugs smuggling. He is killed by G.L. Sarin, after being held at ransom for Sam Hans money.
*Jameel Khan as Atul Bhatnagar RAW agent, who is sent by the Indian army to fulfill the needs of Captain Adit during the mission.
*Chitrita Sharma as Shilpa
:She is Alishas friend. She is murdered by Gazi.

===Villains===
*Milind Gunaji as General Ansari ISI agent from Pakistan. He is a powerful man with contacts and he, arranged an attempt on Sam Hanss life, and he has contacts within Al Jazeera. He is a clever man, and keeps a cool head for most of the time. He is shot and killed by Adit.
*Tej Sapru as Hashmi
:He is an ISI agent. Hashmi is first of the two henchmen of General Ansari. He is killed as he attempts to apprehend Bhatnager.
*Chetan Hansraj as Gazi
:He is an ISI agent. Gazi is second of the two henchmen of General Ansari. He kills Alishas friend Shilpa, and lures Adit into a trap created by Ansari. He is killed by Adit.
*Sharat Saxena as G.L. Sarin
:He is the Indian ambassador. A seemingly proud Indian at first, until turning out to be a traitor. He kills Sam Hanss friend Brian, and is killed by Hans in return.
*Tora Khasgir as Ms. Brar
:She is an associate to the Indian Ambassador G.L. Sarin. Ms. Brar reveals to be a traitor, working for Rafiq Mabroz and is actually working alongside Sarin for the huge fee paid by the ISI. She is accidentally killed by Volga, after both women try to kill Alisha and Kinjal, who dodge the attack.
*Mukesh Rishi as Youssan Baksh
:He is the leader of the Al Amaz insurgency. Baksh is a clever man, as he discovers an Indian spy within his group, but as well as smarts, he has strength. He is killed by Captain Adit Arya in the final scenes of Asambhav.
*Shawar Ali as Rafiq Mabroz
:He is an international terrorist whose purpose is similar to a mercenary. Whilst Youssan and Ansari are patriots, Mabroz cares only for the money. He is responsible for the death of Embassy worker Parmar.
*Arif Zakaria as Dabral
:Dabral is an associate of Brians. He discovers Alisha and is one of the few people who actually know who Aditya is, and what relationship he has to Alisha. Dabral is killed by Zorba.
*Anupama Verma as Volga
:She is a lady in Rafiq Mabrozs gang. She is the love interest of Mabroz.
*Shabhir as Zorba
:He is a man in Rafiq Mabrozs gang.
*Mumaith Khan as Dancer in song "Mashuqa Rubiya" (Special Appearance)

==Production==

===Development=== action thriller genre including Yudh (film)|Yudh (1985), Tridev (1989), Vishwatma (1992), Mohra (1994) and Gupt (1997) under the banner of his father Gulshan Rais production company Trimurti Films. All the films did well at the box office and were huge successes, thus establishing Rai as a well acclaimed director.  After his repeated five successes in action movies, Rai directed a romance film for the first time titled Pyaar Ishq Aur Mohabbat and the newcomer Arjun Rampal was cast in the movie, who had already filmed Ashok Mehtas Moksha (2001 film)|Moksha. However, Pyaar Ishq Aur Mohabbat was released before Moksha and was billed as Rampals debut movie.  Rampal received critical acclaim for his performance. 
 Filmfare Online, Rai said "I listened to several peoples opinions. I made the film (PIAM) not on intuition and belief but on marketing strategies and counsels from trade persons insisting that love stories were the trend of the day."  In that same year, Rai started his new project with a comeback to action genre and titled it Asambhav. 

===Casting===
 
After planning Asambhav, Rai signed Arjun Rampal as the hero of the movie as Rampal had worked as the hero of Rais previous directorial venture Pyaar Ishq Aur Mohabbat. Aishwarya Rai was signed as the heroine.  Rampal and Rai had already worked in Dil Ka Rishta that released on 17 January 2003 and Asambhav marked their second pairing. However, Rai backed out of the film in 2003 and Priyanka Chopra was cast in as the female lead instead of Rai.  Naseeruddin Shah was roped in to essay the role of the villain as he had worked with Rai as a hero in Tridev and as a villain in Mohra. Shah had earned critical acclaim for his villainous performance in Rais Mohra and had earned a nomination for the prestigious Filmfare Award for Best Performance in a Negative Role at the 40th Filmfare Awards.   Noticing on Shahs previous success in a negative role, Rai cast him as the villain in his latest action thriller.   The total cast was of 55 artists including model turned actress Dipannita Sharma, Tom Alter and Sharat Saxena among others. 

During the music launch of the film, Rajiv Rai was interviewed on casting models in important roles. Rai replied "I wanted a very western look to the film; I wanted it to look stylish. I have roped in many models because I wanted my film to look fresh and stylish. Models have a lot of exposure, they are very confident in front of the camera. There are a lot of fresh faces coming into acting. I wanted fresh faces for my film. The modeling industry can act. Its not fair to pinpoint. Arjun (Rampal) himself was a model."   

===Filming===
After the casting was finalised, Rajiv Rai flew to Switzerland and the entire film was filmed in Ticino, Switzerland and began filming in June 2003 with a schedule of fifty-seven days. It was filmed in multi-camera setup. While most scenes were shot with four cameras, the action sequences consisted of seven camera units. It was the first Indian action film to feature no stunts. The action sequences were digitally shot. Asambhav was the first Indian film to be graded digitally on Luster.  The filming was completed in September 2003.  It was the first Indian film to be shot in that location, in the longest filming schedule of any Indian film unit in Switzerland.  The entire film was digitised and no duplicates were used in the development of the movie. 

===Post-production===
The movie was launched in March 2004. The soundtrack was composed by Viju Shah and it was launched in Mumbai on 26 May 2004.  

==Release==
Asambhav was released on 23 July 2004 to limited cinema houses but became a average grosser. 

===Critical reception===
{| class="wikitable infobox plain" style="float:right; width:23em; font-size: 80%; text-align: center; margin:0.5em 0 0.5em 1em; padding:0;" cellpadding="0" cellspacing="0"
|+ Professional reviews
|-
! colspan="3" style="background:#d1dbdf; font-size:120%;" | Review scores
|-
! Source
! Rating
! Reference
|-  Bollywood Hungama
| 
|    
|- Planet Bollywood
| 
| 
|- Apun Ka Choice
| 
| 
|}

Asambhav received mixed reviews from critics upon release. Ronjita Kulkarni of Rediff wrote "Asambhav, starring Arjun Rampal and Priyanka Chopra, is a good film. It packs in the done-to-death Pak-bashing content, and reduces Pakistanis and Afghans to caricatures."  Prema K. of Bollyvista wrote "The film grip the audience even in a single reel. The songs flow freely .Viju Shah too seems to have lost his touch! The cinematography is good, in fact, the only real good thing about the film. Arjun Rampal is good but the role does not offer him much scope. Priyanka Chopra is reduced to a glamour piece. The model brigade serves as mere sidekicks. Talented actors like Naseeruddin Shah, and Arif Zakaria have been positive."  Taran Adarsh of Bollywood Hungama gave a rating of 3.5 out of 5 stars and gave positive reviews for the film. He wrote "On the whole, ASAMBHAV is a medium film in all respects. At the box-office, its chances of hit movie asambhav  ."     Shahid Khan of Planet Bollywood gave a rating of 8 out of 10 stars. He wrote "Rajiv Rais direction is uninspired. Therefore, his "Asambhav" ends up looking tired and dragged out."    
 IANS wrote "Asambhav is arguably the only mainstream film ever with no mother-figure on the hectic horizon. But absence of melodrama cannot in itself be a virtue unless it is compounded by a moist quality in the overall design of storytelling."  However, Pankaj Shukla of SmasHits was the critic who praised the movie but criticised the casting. He wrote "The film in totality is not boring. It not only entertains but also makes value for the money, as the labor done by the director is quite visible on the screen. It is only the casting of the film that has gone haywire.". 
However the movie was average at the box office.

==Box office==

===India===
Created on a very high budget, Asambhav became an average grosser at the box office. It collected  80,632,076 rupees in the opening weekend from all over the country. The earning in the following week was it  100,507,694 in the second week and  220,731,569 in the third week. It grossed a total of  500,113,339.  As a result, it was declared an average grosser.  Asambhav grossed   from India in 2004 with a nett of  . The estimated net rate was   and the estimated gross was   

===Overseas===
Asambhav was an average in overseas as well. The film collected US$19,504 in United States. In United Kingdom, the film hit fifteen screens in the first weekend of 23 July and collected Pound (currency)|£16,894. In the second weekend of 30 July, the screens were reduced to fourteen. Asambhav collected £2,142 and grossed a total of £30,311 in the first two weekends. As a result, it was declared an average grosser.   

==Soundtrack==
{{Infobox album  
| Name       = Asambhav
| Type       = soundtrack
| Artist     = Viju Shah
| Cover      =
| Alt        = 
| Released   = 26 May 2004
| Recorded   = 2003 Feature film soundtrack
| Length     =  Tips 
| Producer   = Gulshan Rai
| Director   = Rajiv Rai
}}
{{Album ratings
| rev1 = Planet Bollywood
| rev1Score =   
}}
 Tips Music Sameer and Remo DSouza was the choreographer of the music sequences. The soundtrack was released on 26 May 2004.   

The soundtrack consisted of eleven tracks including five songs, five instrumental versions and a special Asambhav Theme. The director of the movie Rajiv Rai and the supporting actor Naseeruddin Shah also sang in the movie. They performed the song Raatein Badi Hain. 

The soundtrack received mixed to negative reviews. Shahid Khan of Planet Bollywood rated 7 out of 10 stars. He wrote "Viju Shah´s "Asambhav" might not exactly win the award for the best soundtrack of the year but there are some pretty darn good tunes here."  Runaq Kotecha of Glamsham wrote "The music of Asambhav once again confirms that Viju Shah is certainly one of the best composers that we have when it comes to delivering expeditious racy tracks with ultra modern electronic beats. However, in the midst of all the upbeat technology and heavy duty musical instruments, the tunes seem to be taking a back seat."    Aira of SmasHits wrote "Music of Asambhav is only mediocre. If heard in isolation the music lovers will probably be interested in the techno bit of the entire album." 

 

==Awards and nominations==
*Zee Cine Awards
**Zee Cine Award for Best Song Recording–Tanay Gajjar (Nominated)   

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 