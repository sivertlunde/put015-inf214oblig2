Dis-moi que tu m'aimes
 
 
Dis-moi que tu maimes is a French film directed by Michel Boisrond, released in 1974 in film|1974.

== Synopsis ==

Two wives (one a housewife and the other a decorator) throw their husbands (businessmen Daniel Ceccaldi and Jean-Pierre Marielle) out. The men are initially delighted to return to bachelorhood. 

One man leaves the city to breed sheep in the countryside, the other finds love with another man.

== Details ==

* Title : Dis-moi que tu maimes
* Director : Michel Boisrond
* Writers : Annette Wademant and Michel Boisrond
* Cinematography : Daniel Gaudry
* Cameraman : Jean-Paul Cornu
* Editor : Renée Lichtig
* Music : Claude Bolling
* Sound : Bernard Ortion
* Produce : Gérard Beytout (co-production Franco Film - S.N. - Mannic Film)
* Distributor : S.N.C.
* Length : 115 minutes
* Release date : 12 December 1974

== Starring ==

* Mireille Darc : Victoire Danois
* Marie-José Nat : Charlotte Le Royer
* Jean-Pierre Marielle : Richard le Royer
* Daniel Ceccaldi : Bertrand Danois
* Georges Descrières : Maître Olivier
* Geneviève Fontanel : Pascaline Dorgeval
* Jean-Pierre Darras : Lucien Dorgeval
* Jean Topart
* Monique Delaroche
* Erik Colin : Charles Tabard
* Lisbeth Hummel : Christiana
* Jacqueline Fogt : la mère de Christiana

== External links ==

*  

 

 
 
 

 