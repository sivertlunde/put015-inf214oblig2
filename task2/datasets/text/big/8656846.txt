Love and Pain and the Whole Damn Thing
{{Infobox film
| name           = Love and Pain and the Whole Damn Thing
| image          =
| caption        =
| director       = Alan J. Pakula
| producer       = Alan J. Pakula
| writer         = Alvin Sargent
| narrator       =
| starring       = Maggie Smith Timothy Bottoms
| music          = Michael Small
| cinematography = Geoffrey Unsworth Russell Lloyd
| distributor    = Columbia Pictures
| released       =  
| runtime        = 110 mins
| country        = United States English
| budget         =
}} American film directed by Alan J. Pakula. Variety Film Reviews|Variety film review; April 18, 1973, page 32.  It is often categorized as a drama, but contains many comic elements. Maggie Smith and Timothy Bottoms star.

==Plot==
Walter Elbertson (Timothy Bottoms) is a young, shy asthmatic who lacks direction in his life and the confidence to tackle his future.  His father, in an effort to instill some spirit into his son, sends him on a biking holiday in Spain.  Walter goes to Spain but finds the bike riding torturous due to his asthma and lags behind the rest of the group.

Lila Fisher (Maggie Smith), meanwhile, is touring Spain by bus. She too is awkward with people and keeps to herself, and looks uncomfortable when a Spaniard tries to woo her with bird noises.

Soon the two tours coincide. Seeing the bus about to depart, Walter decides he has had enough of the bike and joins the bus tour.  He ends up alongside Lila on the rear bus seat, wheezing terribly from having run for the bus.

The two begin spending time together out of necessity, but neither seems particularly confident in the growing relationship, Lila particularly.  However, their similar dispositions soon bring them closer and they consummate their relationship. Not all goes smoothly, both expressing doubt of the others loyalty.  They make pledges of commitment to one another, increase their intimacy, and strengthen their bond.

Walter and Lila eventually decide to leave the bus tour behind. Walter organises a small caravan to take them around the country. At one point, they meet The Duke (Don Jaime de Mora y Aragon), who lives in a large Spanish castle, and seems to be very taken with Lila.  This awakens jealousy in Walter, and for the first time he acts with strength and resolve to keep her with him.

Lila, who has shown signs of illness at various points along the way, confesses to Walter that she has not long to live.  The two determine, with Walter as the main instigator, to spend her remaining days traveling together and following their hearts.

==Alternate title==
Also known as The Widower (a working title in the U.S.)

==Cast==
*Lila Fisher -          Maggie Smith
*Walter Elbertson -     Timothy Bottoms
*The Duke -               Don Jaime de Mora y Aragon
*Spanish Gentleman -      Emiliano Redondo Charles Baxter
*Mrs Elbertson -          Margaret Modlin
*Melanie Elbertson -      May Heatherly  (aka May Heatherley)
*Carl -                   Lloyd Brimhall
*Dr Edelheidt -           Elmer Modling
*Tourist Guide -          Andrés Monreal 

==References==
 

==External links==

 

 
 
 
 
 
 
 
 
 