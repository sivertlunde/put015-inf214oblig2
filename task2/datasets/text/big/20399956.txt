Invasion (1966 film)
  British sci-fi film, directed by Alan Bridges for producer Jack Greenwood of Merton Park Studios.
 Robert Holmes. Holmes later re-used elements of his storyline in a 1970 Doctor Who serial entitled Spearhead from Space, starring Jon Pertwee. 

It tells the story of an alien spacecraft which crash-lands on Earth, near a secluded hospital in the Home counties. The aliens, who are humanoid and resemble Asian people|Asians, are taken to a rural hospital where they cause a forcefield to be raised around the building.

== Availablility ==
The film was theatrically released by Anglo-Amalgamated in the UK, and by American International Pictures in the USA.

A very brief video release by Warner Home Video was available in the UK in 1992.
The film has a November 2014 DVD Release Date. Networkonair.

A re-release on DVD had been scheduled for November 2014.

== Cast ==
* Edward Judd as Dr. Mike Vernon
* Yoko Tani as Leader of the Lystrians
* Valerie Gearon as Dr. Claire Harland
* Lyndon Brook as Brian Carter Eric Young as The Lystrian Tsai Chin as Nurse Lin
* Barrie Ingham as Major Muncaster
* Anthony Sharp as Lawrence Blackburn
* Glyn Houston as Police Sergeant Draycott
* Ann Castle as Sister Evans John Tate as Dundy
* Jean Lodge as Barbara Gough

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 

 