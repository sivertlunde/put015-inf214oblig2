Supergator
{{Infobox film
| name           = Supergator
| image          = Supergator.jpg
| caption        = DVD Cover
| director       = Brian Clyde
| producer       = Roger Corman
| writer         = Wayne Gates Brian Clyde Brad Johnson Kelly McGillis
| music          = Damon Ebner
| cinematography = Robert Shoemaker Andrea V. Rossotto
| editing        = 
| studio         = New Horizons Picture Corp.
| distributor    = Rodeo Productions
| released       =  
| runtime        = 96 mins.
| country        = United States
| language       = English 
| budget         = 
}}
 Brad Johnson and Kelly McGillis. The music was by Damon Ebner.

==Background==
After Corman produced Dinocroc in 2004, he wanted to create a sequel to be named Dinocroc 2.  However, Sci-Fi Channel turned down the project after claiming that sequels did not do well for them.     Corman decided to go ahead with the project, but under the name Supergator.   

== Plot ==
Professor Scott Kinney is an American geologist monitoring a local volcano when the Supergator, a Phobosuchus recreated from fossilized, preserved DNA escapes from a secret bio-engineering research center/laboratory. Along the way, it eats thirteen people, including two lovers, three drunken teens, three models, two tourists and a fisherman. It also eats Alexandra Stevens and Ryan Houston.

Kinney joins forces with another scientist, Kim Taft, and a Texan alligator hunter named Jake. They pursue the monster as it heads down river intent on destroying a luxurious resort packed with hundreds of tourists. Conventional weapons have no effect on it at all and Kim is eaten, so they, with the help of Carla, plan a trap for it using a fake volcano. They lure him on to it, with Jake using himself as live bait. The Supergator devours him and Kinney kills it by shooting at the fake volcano, which then blows up the Supergator.

== Cast == Brad Johnson - Professor Scott Kinney
*Kelly McGillis - Kim Taft
*Bianca Lawson - Carla Masters
*Mary Alexandra Stiefvater - Alexandra Stevens Josh Kelly - Ryan Houston John Colton - Jake Kilpatrick
*Holly Weber - Lorissa
*Tamara Witmer -  Gigi
*Nic Nac - Jeremy
*Meg Cionni - Wendy
*Greg Cipes - Shaun
*Matt Clendenin - The Driver
*Gene DeFrancis - Lance
*Sarah DuBois - Betty
*Elizabeth J. Carlisle - Brenda
*Sol Kahn - Jason
*Ikaika Kahoano - Rob
*Justin Loeb - Guy Running From Supergator
*Dave Ruskjer - Max
*Joanna Shewfelt - Tracy Charles Solomon - Joe
*Charles Solomon Jr. - Joe
*Jennifer Titus - Zoe
*Traci Toguchi - Melinda

==Reception==
The film has received good and bad reviews from critics.

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 


 