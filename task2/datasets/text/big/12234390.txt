That's My Baby! (1984 film)
{{Infobox Film
  | name = Thats My Baby!
  | image = 
  | caption = 
  | director = John Bradshaw Edie Yolles 
  | producer = Edie Yolles
  | writer = John Bradshaw Edie Yolles 
  | starring = Timothy Webber Sonja Smits Lenore Zann Matt Craven Joann McIntyre Eric N. Robertson
  | cinematography = W.W. Reeve
  | editing = John Bradshaw Stephen Withrow Edie Yolles
  | distributor = Troma Entertainment
  | released = 1984 
  | runtime = 97 minutes
  | language = English
  | budget =  
  | preceded_by = 
  | followed_by = 
  }}
 Canadian comedy film directed by John Bradshaw and Edie Yolles. It was distributed for a brief time by Troma Entertainment.

The film follows a couple, Lewis and Suzanne: Lewis takes care of the house and works part-time, while Suzanne works full-time. Lewis decides he wants a child, but Suzanne worries that it might interfere with her career.

==External links==
* 

 
 
 
 
 
 
 
 