Sanmanassullavarkku Samadhanam
{{Infobox film
| name           = Sanmanassullavarkku Samadhanam
| image          = 
| alt            = 
| caption        = Promotional Poster
| director       = Sathyan Anthikkad
| producer       = Siyad Koker Sreenivasan (dialogues) Sreenivasan 
| story          =  Sreenivasan
| music          = Jerry Amaldev
| cinematography = Vipin Mohan
| editing        = K Rajagopal
| studio         = Kokers Films
| distributor    = Kokers Films
| released       =  
| country        = India Malayalam
}} 
 1986 Cinema Indian feature directed by Sreenivasan and M.G. Soman.     
==Plot==

It is about a house owner, Gopalakrishna Panikkar, who is  struggling with debt incurred during the wedding of his two sisters. He needs to force his tenants to leave so that the house can be sold and the debts repaid. The tenants do not want to leave for reasons of their own. How the various situations unfold and what happens to Gopalakrishna Panikker and the tenants is the rest of the story.

==Summary==
The only way Gopalakrishnan can escape from certain bankruptcy is to sell the property he owns in the city and use the proceeds to settle his debts. To do that, he has to evict his tenants from there first. Unfortunate for him, his tenant is the feisty Meera and her family, who absolutely refuses to vacate the house. Gopalakrishnan uses an obscure clause in the contract to take up residence at the house, and tries his best to smoke his tenants out and takes help of his friend Sub Inspector Rajendran(Sreenivasan) to evict them out. But Rajendran falls in love with Meera and refuses to evict her.  Gopalakrishnan plans Rajendran to get married with Meera inorder to evict her with her family but it flops as when it is revealed by Meeras mother that she was a mental patient due to the death of her father. Much hilarity ensues later and Meera atlast when  knowing about Gopalakrishnans problems, vacates the house full heartedly and leaves. But Gopalakrishnan calls her back and calls it as their house.

==Cast==
*Mohanlal as Gopalakrishna Panikkar Karthika as Meera Sreenivasan as Sub Inspector K. Rajendran
*Thilakan as Damodaran/Damodar Bhai Innocent as Kunji Kannan Nair
*Sankaradi as Adv. P. Sreedharan Nair
*Mammukoya as Ummer (as Mamu)
*K.P.A.C. Lalitha as Karthyayani (as Lalitha)
*Sukumari as Panikkars Mother
*Thodupuzha Vasanthi as Ayisha
*Yadu Krishnan as Monukuttan
*M.G. Soman as Rajendrans Uncle(Poomkinavu Ammavan)

==Soundtrack==
The music was composed by Jerry Amaldev and lyrics was written by Mullanezhi.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kanninu Ponkani || K. J. Yesudas || Mullanezhi ||
|-
| 2 || Pavizhamalli Poothulanja || K. J. Yesudas || Mullanezhi ||
|}

==Reception & Remakes==
The film was a critical and commercial success.  It was later remade in Telugu as Donga Kollu with Rajendra Prasad and Sumalatha, in Tamil as Illam_(film)|Illam with Sivakumar and Amala (actress)|Amala, in Kannada as Yardo Duddu Yallamanna Jathre with Jaggesh and in Hindi as Yeh Teraa Ghar Yeh Meraa Ghar directed by Priyadarshan and starring Sunil Shetty in the lead role.

==References==
 

==External links==
*  

 
 
 
 
 
 
 