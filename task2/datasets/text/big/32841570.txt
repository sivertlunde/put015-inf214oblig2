New Mexico (film)
{{Infobox film
| name           = New Mexico
| image_size     =
| image	=	New Mexico FilmPoster.jpeg
| caption        =
| director       = Irving Reis
| producer       = Irving Allen (producer) Joseph Justman (producer)
| writer         = Max Trell (original screenplay)
| narrator       =
| starring       = See below
| music          = René Garriguenc Lucien Moraweck William E. Snyder Lester White
| editing        = Louis Sackin
| studio         =
| distributor    =
| released       = 1951
| runtime        = 76 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

New Mexico is a 1951 American western film directed by Irving Reis, starring Lew Ayres and shot in Ansco Color.

== Plot ==
Abraham Lincoln himself comes to New Mexico to discuss living together in peace with Acoma, a feared and respective Indian chief. He presents the chief with a cane as a gift and symbol of their friendship.

Lt. Hunt is promoted due to his personal assistance to Lincoln in arranging the truce. Unhappily, a bigoted superior officer, Col. McComb, and the dastardly Judge Wilcox are opposed to any such treaty, and when Hunt states his objection, McComb has him placed under arrest alongside Acoma and a number of Indian braves, also breaking the cane.

Other members of the tribe break them out of jail, killing McComb and others in the process. Hunt takes command and cancels all travel in the region, angering a woman named Cherry who is planning a trip to Nevada. She arrogantly elects to leave anyway, as does Judge Wilcox, so a company of men led by Hunt goes along as escorts.

Indians attack, frightening the woman and burying the judge in the sand. Hunt is disgusted with Cherrys selfish attitude and tells her so. She comes to know one of Acomas sons, and when another uprising has fatal consequence for Indian warriors as well as Hunt, she and Acomas son are lucky to have their lives spared.

== Cast ==
*Lew Ayres as Captain Hunt
*Marilyn Maxwell as Cherry
*Robert Hutton as Lieutenant Vermont
*Andy Devine as Sergeant Garrity
*Raymond Burr as Private Anderson
*Jeff Corey as Coyote
*Lloyd Corrigan as Judge Wilcox
*Verna Felton as Mrs. Fenway
*Ted de Corsia as Acoma - Indian Chief
*John Hoyt as Sergeant Harrison
*Donald Buka as Private Van Vechton
*Robert Osterloh as Private Parsons Ian MacDonald as Private Daniels
*Ralph Volkie as Rider
*William Tannen as Private Cheever
*Arthur M. Loew Jr. as Private Finnegan Bob Duncan as Corporal Mack Jack Kelly as Private Clifton
*Allen Mathews as Private Vale Jack Briggs as Private Lindley Peter Price as Chia-Kong
*Walter Greaza as Colonel McComb
*Hans Conried
*Bud Ray as Stagecoach Driver

== Soundtrack ==
 

== External links ==
* 
* 
 
 
 
 
 
 
 
 
 
 

 