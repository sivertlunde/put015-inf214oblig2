Turn the River
{{Infobox film
| name     = Turn the River
| image    = Turn The River.jpg
| caption  = 
| writer   = Chris Eigeman Rip Torn Matt Ross Lois Smith Marin Hinkle Terry Kinney Jordan Bridges Ari Graynor
| director = Chris Eigeman 
| producer = Ami Armstrong
| released =  
| runtime  = 92 minutes
| language = English
| budget   = 
}}
Turn the River is a film that was written and directed by Chris Eigeman.  The film debuted at the Hamptons International Film Festival on October 17, 2007.

==Cast==
*Famke Janssen as Kailey Sullivan
*Jaymie Dornan as Gulley
*Rip Torn as Teddy Quinette
*Matt Ross as David
*Lois Smith as Abigail
*Marin Hinkle as Ellen
*Terry Kinney as Markus
*Jordan Bridges as Brad
*Ari Graynor as Charlotte
*John Juback as Duncan
*Tony Robles as Ralphie
*Santo DAsaro as Scott
*Zoe Lister Jones as Kat
*Elizabeth Atkeson as Sally
*Joseph Siravo as Warren

==Plot==
Turn the River stars Famke Janssen as Kailey Sullivan, a woman rough around the edges and schooled in hard knocks. Divorced from her sons father and without visitation rights, she and her son write letters to each other and meet surreptitiously. 

Kailey learns that her son, Gulley, is receiving abuse from his father and is being bullied in school. She decides that the best thing for her and her child would be to leave the country and start a new life in Canada. She puts a plan into motion to organize fake passports and papers, which require that she raise a fair amount of money. Kailey starts off by using her exceptional skills at pool to win small bets. After a nasty encounter with a player unhappy at losing, Kailey reveals to her friend Teddy Quinette that she met her ex-husband, David, while she was running an illegal card game room. David left his studies in a seminary to marry a then pregnant Kailey. Davids mother, staunchly religious and unhappy at this turn of events, reported Kaileys illegal activities and then struck a deal with her to make her life easier in return for Kailey getting a divorce and giving away custody of Gulley. With Teddys help, Kailey begins to organise high-stakes games against skilled players at the pool bar Teddy owns. Kailey eventually collects enough money together and prepares to flee with her son in tow. On the morning of departure, she drops by Davids house to speak to him for the first time in eleven years. She tells him that she is leaving with Gulley and that he will be better off with her. Kailey then handcuffs David inside the house with the aid of a replica handgun. Davids wife returns to find him, and they call the police.

After driving to the Canadian border, Kailey and Gulley have only to catch an early morning bus over the border. The police manage to track them down before they can leave, and Kailey is shot by police who believed her to be reaching for a firearm in her bag. It is left to the viewer to decide if she died or not. The movie ends with a flashback of Gulley walking to Quinettes pool bar for the first time to receive a postcard from his mother.

==Themes==
Entertainment website, Variety, offered the following analysis:

:  In casting a woman in a traditionally male role, Eigeman subtly shifts both genre and gender. His heroine adopts the iconography of the hustler movie but feminizes it: The image of a woman camping out on a pool table reads less as rugged than vulnerable. When Kailey gets beaten up, its not for being a hustler but for being a woman, as male rage explodes on the barest pretext.  

==Critical reception==
As of April 11, 2009, the review aggregator Rotten Tomatoes reported that 67% of critics gave the film positive reviews, based on 18 reviews. 

==Awards==
Eigeman won the Zicherman Family Foundation Award For Best Screenwriter "for its unflinching realism, pitch perfect dialogue and three-dimensional characters in a world that is so often given to stereotypes."   

Janssen was awarded a Special Jury Best Actress Prize for her work in the film "for a brave and touching portrayal of a woman fighting for her child and her life." 

The film, Chris Eigeman and Ami Armstrong were nominated for Film Independents John Cassavetes Award.

==Soundtrack==
The music on the soundtrack of the movie is composed by Clogs (band)|Clogs.

==References==
 

==External links==
*  
*  
*  
*  

 
 