When Eight Bells Toll (film)
{{Infobox Film
| name           = When Eight Bells Toll  
| image          =When Eight Bells Toll.jpg
| image_size     =
| caption        = When Eight Bells Toll Etienne Perier
| producer       = Elliott Kastner Jerry Gershwin
| writer         = Alistair MacLean
| narrator       = 
| starring       = Anthony Hopkins Jack Hawkins Robert Morley Nathalie Delon
| music          = Angela Morley (credited as Walter Stott)
| cinematography = Arthur Ibbetson
| editing        = John Shirley
| distributor    = J. Arthur Rank
| released       = 9 March 1971
| runtime        = 94 min.
| country        = United Kingdom
| language       = English
| budget         = $7 million (approx.)
| gross          = 
}} 1971 action Scottish author 1965 novel of the same name. Producer Elliott Kastner planned to produce a string of realistic gritty espionage thrillers to rival the James Bond series, but the films poor box office receipts ended his plans.

==Plot== British Treasury marine biologists, Cypriot tycoon and shipping magnate Sir Anthony Skouras (Jack Hawkins), whose luxury yacht Shangri-La is anchored off the coast, may be behind the pirating of the gold bullion. While searching the surrounding area in a Royal Navy Helicopter, Calvert makes contact with a group of remote shark fishermen who appear more friendly than Torbays locals. Calvert also meets the occupants of a Castle, Lord Kirkside and his teenage daughter, who behave strangely as well as being hostile. As the helicopter brings Calvert back to Torbay it comes under attack from the shore and the Royal Navy pilot is killed. The helicopter crashes, explodes and plummets into the sea. Calvert escapes from the helicopter after it sinks to the bottom. When he returns to his research yacht Firecrest he finds Hunslett is missing.

Operating out of his yacht, Calvert is joined by his boss Sir Arthur Arnford-Jones, known as "Uncle Arthur" (Robert Morley). Together, they combat boarders and make ready for sea. On raising the anchor they find the dead body of Hunslett tied to it.

They are joined by Skourass wife, Charlotte (Nathalie Delon), and when a pirate speedboat approaches, Calvert rams it, shoots the occupants and blows up the boat in vengeance for Hunsletts death.

Calvert recruits the shark fishermen to deal with Skouras and his modern day pirates. Guessing that the missing bullion ships are being sunk to allow the gold to be offloaded invisibly, Calvert dives in the bay and finds the Nantesville. He fights and kills one of the divers, whom he has previously encountered and who he suspects killed Hunslett.

He then secretly enters Kirksides castle and questions the Lords daughter, discovering that Skouras is an innocent victim whose real wife is being held hostage along with other locals down in the castles dungeons. He then sneaks into the underground dock of the castle where the gold is being offloaded.

At midnight (eight bells) the shark fishermen ram the gates of the underground dock with their boat. The pirates are expecting them because Charlotte has been transmitting Calverts plans to them by secret radio. A fire fight ensues in which the pirates are wiped out, after which Calvert lets Charlotte escape with a single bar of gold in her possession.

==Production==
The story is very close to the novel, and features some of the same witty dialogue—not surprising since MacLean elected to adapt this film himself. Some of the twists in the ending have been changed, however, and an explosive shootout replaces MacLeans original Agatha Christie-style summation.
 Bob Simmons helped him slim down to become a convincing Royal Naval officer trained as a commando and frogman. 
 Westland Widgeon. There is also an Aérospatiale Alouette II.
 Diamonds Are Forever, and the Bond series producers decided to continue the series when Connery left. The projected Phillip Calvert series was cancelled.

==Cast==
*Anthony Hopkins as Philip Calvert
*Robert Morley as Uncle Arthur
*Nathalie Delon as Charlotte
*Jack Hawkins as Sir Anthony Skouras
*Corin Redgrave as Hunslett
*Derek Bond as Lord Charnley
*Ferdy Mayne as Lavorski
*Maurice Roëves as Helicopter Pilot Leon Collins as Tim Hutchinson
*Wendy Allnutt as Sue Kirkside
*Peter Arne as Imrie
*Oliver MacGreevy as Quinn
*Tom Chatto as Lord Kirkside
*Edward Burnham as Macullum Charles Gray uncredited voice of Sir Anthony Skouras

==Notes==
 

== External links ==
* 
* 
* 
* 
*  
 

 
 
 
 
 
 
 
 
 
 