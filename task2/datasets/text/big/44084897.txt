Pathivritha
{{Infobox film 
| name           = Pathivritha
| image          =
| caption        =
| director       = MS Chakravarty
| producer       =
| writer         = MS Chakravarthi Pappanamkodu Lakshmanan (dialogues)
| screenplay     = MS Chakravarthi Madhu Sheela MG Soman Padmapriya
| music          = M. S. Viswanathan
| cinematography = Ashok Gunjal
| editing        = VP Krishnan
| studio         = Meghalaya Films
| distributor    = Meghalaya Films
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by MS Chakravarty. The film stars Madhu (actor)|Madhu, Sheela, MG Soman and Padmapriya in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast== Madhu
*Sheela
*MG Soman
*Padmapriya
*Ravi Menon Reena
*Seema Seema

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aa janmasoubhaagyame || K. J. Yesudas || Bichu Thirumala || 
|-
| 2 || Iniyoru naalil || P Susheela, P Jayachandran || Bichu Thirumala || 
|-
| 3 || Kalam kalam malarmelam || S Janaki || Bichu Thirumala || 
|-
| 4 || Shankhumukham Kadappurathoru || Vani Jairam, Jolly Abraham || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 