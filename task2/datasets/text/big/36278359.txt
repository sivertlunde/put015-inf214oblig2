Thy Womb
{{Infobox film
| name = Thy Womb
| image = Thy-Womb-poster.jpeg
| caption = Theatrical movie poster for the 2012 Metro Manila Film Festival
| director = Brillante Mendoza
| starring = Nora Aunor Bembol Roco Lovi Poe
| producers = Melvin Mangada Brillant Mendoza Jaime Santiago
| writer = Henry Burgos
| cinematography = Odyssey Flores
| editing = Kats Serraon
| studio = Center Stage Productions Solar Films
| released =  
| country = Philippines
| language =  
| budget   = P 2 million
| gross = P 5.1 million
}} Filipino drama film starring Nora Aunor, Bembol Roco, Mercedes Cabral, and Lovi Poe.  Produced by Center Stage Productions and the Film Development Council of the Philippines (FDCP), Melvin Mangada and Jaime Santiago, the film is written by Henry Burgos and directed by Brillante Mendoza. The film is one of the 8 official entries to the 2012 Metro Manila Film Festival.
 37th Toronto International Film Festival in September and the 17th Busan International Film Festival in October.  , Thy Womb, film info and screening schedules at TIFF.    

It is also cited as the single most important achievement in Philippine cinema for 2012 as it gets listed in the BRITANNICA BOOK OF THE YEAR 2013 (A Review of 2012) by Encyclopædia Britannica. 

==Cast==
*Nora Aunor as Shaleha
*Bembol Roco as Bangas-An
*Lovi Poe as Mersila
*Mercedes Cabral as Aisha

==Reviews==
*Aunor’s softly crinkled face beautifully registers the internal pain of her every decision in this curious process. – Guy Lodge, Variety, Sept. 2012

*Aunor’s moving portrayal of a woman determined to provide her husband with a child – Steve Gravestock, Toronto programmer

*Her elfin features, so powerfully expressive of both happiness and sorrow help make Shahela (Nora) an engaging, unlikely heroin - Neil Young, The Hollywood Reporter, Sept. 2012

*One of the most poignant and intimate films at this year’s Venice fest …a moving and visually captivating movie with two commanding yet understated central performances – Jo-Ann Titmarsh, CineVue, September 2012

*Nora Aunor’s acting is immense…Filipino diva Nora Aunor progressively conveys humiliation and pride, pain and hope, frustration and dignity. Emotions fruit of the extreme consequences, of a choice of love that destroys and upsets. Emotions that Thy Womb is able to convey with great intensity and maximum modesty. – Italian Libre Cinema website

== List of Film Festival Competed or Exhibited ==
 
 
* 69th Venice International Film Festival (COMPETITION), Venice, Italy;  37th Toronto Intl Film Festival (World Cinema), Toronto, Canada; 
* 17th Busan International Film Festival (Window on Asian Cinema) Busan, Korea; 
* 6th Ferra de Tutti Film Festival (Exhibition), Bologna, Italy; 
* 50th Vienna International Film Festival (Exhibition), Vienna, Austria; 
* 2012 Brisbane International Film Festival (Exhibition), Brisbane, Australia;  
* 43rd International Film Festival of India (COMPETITION), Goa, India; 
* 2012 Golden Horse Film Festival and Awards (Exhibition), Taipei, Taiwan;   
* 9th Dubai International Film Festival (COMPETITION);  
* 8TH Movies Festival World Edition, Poland (Exhibition) 
* 14th Bratislava International Film Festival (Panorama Section),Slovakia; 
* 20th Plus Camerimage Intl Film Festival of D Art of Cinematography (Exhibition), Bydgoszcs, Poland; 
* 6th Asia Pacific Screen Awards (COMPETITION)  Brisbane, Australia, November 2012 
* 16th Tertio Millenio Film Festival (Exhibition), Rome, Italy;   
* 12th International Film Festival of Marrakech, Morocco (Exhibition)  
* 24th Forum Des Images Festival, Paris, France (Exhibition) 
* 55th Asia Pacific Film Festival (COMPETITION )Macau, China, December 2012 
* The Wadsworth Atenaeum Museum of Arts, CT, USA (Jan. 24-27 2013) (Exhibition) 
* Perth Film Festival, Perth, Australia (Jan. 2013) (Exhibition) 
* 7th Asian Film Awards, Hong Kong (COMPETITION) 
* 15th Deuville Asian Film Festival, France (Exhibition) 
* 37th Hong Kong Intl. Film Festival (Exhibition) 
* 20th Prague Intl Film Festival, Czech Republic (Exhibition) 
* 1st ASEAN Intl. Film Festival Awards, Malaysia (Exhibition) March 2013; 
* 18th Vilniaus Intl. Film Festival, Lithuania (Critics Choice Section) March 2013 
* 11th Reggio Emilia Asian Film Festival,Italy (COMPETITION) April 6–11, 2013 
* 5th CPH PIX (Exhibition), Copenhagen, Denmark April 11–24, 2013 (Exhibition)
* CINEMANOVO International Film Festival, Belgium April 17–28 ( Exhibition)
* 10th International Independent Film Festival, Lisbon Portugal, April 18–30, 2013 (Exhibition)
* Australian Centre for the Moving Image (ACMI)  May 10 to 13, 2013, (Exhibition)
* ASIAN Film Festival ROMA 2013, Rome, Italy (Exhibition) May 10–31, 2013 
* 12th Transilvania International Film Festival in Romania from May 31 to June 9, 2013 (Exhibition)
 
*7th Granada Film Festival Cines del Sur. Imágenes del milenio. Granada, Spain(COMPETITION) 
*Munich International Film Festival, Germany, June 28- July 6, 2013 (COMPETITION) 
*4th World Cinema Amsterdam Festival from August 7–18, 2013 (COMPETITION) 
*3rd Sakhalin International Film Festival, Russia from Aug. 23-30, 2013 (COMPETITION) 
* Southeast Asian Film Festival (Regional Intersections 2013), Canberra, Australia, Sept. 5-19, 2013 (Exhibition)
* 3rd Saint Petersburg International Film Festival, Russia, September 13 to 22 2013 (Exhibition).
* 9th Eurasia International Film Festival in Almaty, Kazakhstan,September 16 to 21, 2013 (Exhibition) ) 
*OzAsia Festival, Adelaide, Australia  September 13–29, 2013 (Exhibition)
*Rio de Janeiro International Film Festival,Brazil, Sept 29 - Oct 9, 2013)   
* 33rd Hawaii International Film Festival (Official Selection) October 10–20, 2013 
* 57th London International Film FestivalOctober 9–20, 2013 (Exhibition)
* 13th Film Festival Womens Worlds, (TERRE DES FEMMES Film Festival) Tuebingen, Germany, November 20–27, 2013 (Exhibition) 
* Around the World in 14 Films - The Berlin Festival of Festivals, Berlin, Germany,  Nov. 29 to Dec. 7, 2014 (Exhibition)
* Luang Prabang Film Festival (LPFF)- Laos. Dec. 7 - 11  
*The 20th Festival International des Cinémas dAsie | Festival du Film Asiatique de Vesoul "Cinemas dAsie" (11-18 Feb 2014), France  
*37th Portland International Film Festival, Oregon, USA, Feb 15 - 20, 2014  
* Philippine Entry, Best Foreign Language Film, 71st Golden Globe Awards (shorlisted)
* 7th Asian Consul General Club (ACGC) Film Festival,JEDDAH, Saudi Arabia - March 2014  
* Lake Como Film Festival edizione 2014 
*Festival Internacional de Cine de Gijon, Spain  Nov. 21-29, 2014
*Les Forum des Images, Paris, France  October 13-19, 2014
* 2014 Gijón International Film Festival in Spain (Nov. 21-29, 2014) (Brillante Mendoza Retrospective Tribute) 
 

== Notable Citations and Awards ==

===International Awards and Recognition===

{| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Group
! width="25%"| Category
! width="25%"| Nominee
! width="10%"| Result
|-
| rowspan="10" align="center"| 2013
| rowspan="6" align="left"| 3rd Sakhalin International Film festival
| align="left"| Best Actress
| align="center"| Nora Aunor
|  
|-
| align="left"| Best Film
| align="center"| 
|  
|-
| align="left"| Best Director
| align="center"| Brillante Mendoza
|  
|-
| align="left"| Best Actor
| align="center"| Bembol Roco
|  
|-
| align="left"| Best Cast
| align="center"| 
|  
|-
| align="left"| Grand Prix of the audience
| align="center"| 
|  
|-
| 7th Asian Film Awards    Best Actress
| align="center"| Nora Aunor
|  
|-
| 7th Cines del Sur - Festival de Granada - Spain 
| Jury Special Mention
| align="center"| 
|  
|-
| 10th International Independent Film Festival - Lisbon, Portugal
| Audience Award - International Competition, Observatory, Emerging Cinema, World Pulse, IndieJunior and Bran New.
Feature Film
| align="center"| 
| style="text-align:center; background:#4cbb17;"| 2nd Place
|-
| 11th Asian Film Festival - Reggio Emilia, Italy
| Premio Del Pubblico
| align="center"| 
| style="text-align:center; background:#4cbb17;"| 2nd Place
|-
| rowspan="10" align="center"| 2012
| rowspan="2" align="left"| 6th Asia Pacific Screen Awards   Best Performance by an Actress
| align="center"| Nora Aunor
|  
|- Best Director
| align="center"| Brillante Mendoza
|  
|-
| rowspan="5" align="left"| 69th Venice International Film Festival
| align="left"| Bisato dOro: Premio Della Critica Indipendiente for Best Actress
| align="center"| Nora Aunor
|  
|-
| align="left"| La Navicella Award for Best Director
| align="center"| Brillante Mendoza
|  
|-
| align="left"| P. Nazareno Taddei Award Special Mention
| align="center"| Brillante Mendoza
|  
|-
| align="left"| Golden Lion for Best Film
| align="center"| 
|  
|-
| align="left"| Volpi Cup for Best Actress
| align="center"| Nora Aunor
|  
|-
| rowspan="2" align="left"| 55th Asia Pacific Film Festival
| align="left"|  Best Actress
| align="center"| Nora Aunor
|  
|-
| align="left"|  Best Director
| align="center"| Brillante Mendoza
|  
|}

===Philippine Awards===
{| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Group
! width="25%"| Category
! width="25%"| Category
! width="10%"| Result
|-
| rowspan="8" align="center"| 2012 2012 Metro Manila Film Festival Best Actress
| align="center"| Nora Aunor
|  
|- Best Director
| align="center"| Brillante Mendoza
|  
|- Best Original Story
| align="center"| Henry Burgos
|  
|- Best Cinematography
| align="center"| Odyssey Flores
|  
|-
| align="center"| Gatpuno Antonio J. Villegas Cultural Awards
| align="center"|
|  
|- Most Gender Sensitive Film
| align="center"| 
|  
|- Female Star of the Night
| align="center"| Nora Aunor
|  
|-
| BALATCA (Batangas Laguna Teachers Association for Culture and the Arts)
|  Pambansang Dangal ng Sining sa Pagganap
| align="center"| Nora Aunor
|  
|-
| rowspan="65" align="center"| 2013 61st FAMAS Awards  
| align="center"| Presidential Award for Cinematic Excellence
| align="center"| Nora Aunor
|  
|- 36th Gawad Urian Awards  
| align="center"| Best Actress
| align="center"| Nora Aunor
|  
|-
| align="center"| Best Production Design
| align="center"| Dante Mendoza
|  
|-
| align="center"| Best Film
| align="center"| 
|  
|-
| align="center"| Director
| align="center"| Brillante Mendoza
|  
|-
| align="center"| Best Actor
| align="center"| Bembol Roco
|  
|-
| align="center"| Best Screenplay
| align="center"| Henry Burgos
|  
|-
| align="center"| Best Editing
| align="center"| Kats Seraon
|  
|-
| align="center"| Best Cinematography
| align="center"| Odessey Flores
|  
|-
| align="center"| Best Music
| align="center"| Teresa Barrozo
|  
|- 15th Gawad Pasado (Pampelikulang Samahan ng mga Dalubguro)  
| align="center"| Pinakapasadong Pelikula 
| align="center"| 
|  
|-
| align="center"| Pinakapasadong Direktor
| align="center"| Brillante Mendoza
|  
|-
| align="center"| Pinakapasadong Aktres
| align="center"| Nora Aunor
|  
|-
| align="center"| Pinakapasadong Aktor
| align="center"| Bembol Roco
|  
|-
| align="center"| Pinakapasadong Katuwang Aktres
| align="center"| Lovi Poe
|  
|-
| align="center"| Pinakapasadong Istorya
| align="center"| Henry Burgos
|  
|-
| align="center"| Pinakapasadong Pelikula sa Paggamit ng Wika
| align="center"| 
|  
|-
| align="center"| Pinakapasadong Sinematograpiya
| align="center"| Odessey Flores
|  
|-
| align="center"| Pinakapasadong Editing
| align="center"| Kats Seraon
|  
|-
| align="center"| Pinakapasadong Dulang Pampelikula
| align="center"| Henry Burgos
|  
|-
| align="center"| Pinakapasadong Disenyong Pamproduksyon
| align="center"| Dante Mendoza
|  
|-
| align="center"| Pinakapasadong Paglalapat ng Tunog
| align="center"| Albert Michael Idioma at Addiss Tabong
|  
|-
| align="center"| Pinakapasadong Musika
| align="center"| Teresa Barrazo 
|  
|- 2012 Young Young Critics Circle  
| align="center"| Best Performance by Male or Female, Adult or Child, Individual or Ensemble in Leading or Supporting Role 
| align="center"| Nora Aunor
|  
|-
| align="center"| Best Screenplay
| align="center"| Henry Burgos
|  
|-
| align="center"| Best Editing
| align="center"| Kats Seraon
|  
|-
| align="center"| Best Achievement in Cinematography and Visual Design
| align="center"| Odyssey Flores (Cinematography) Dante Mendoza (Production Design)
|  
|- 11th Gawad Tanglaw
| align="center"| Best Actress
| align="center"| Nora Aunor
|  
|- 29th Star Awards for Movies 
| align="center"| Indie Movie Cinematographer of the Year
| align="center"| Odyssey Flores
|  
|-
| align="center"| Indie Movie Production Designer of the Year
| align="center"| Dante Mendoza
|  
|-
| align="center"| Indie movie of the Year
| align="center"| 
|  
|-
| align="center"| Indie Movie Director of the year
| align="center"| Brillante Mendoza
|  
|-
| align="center"| Movie Actress of the Year
| align="center"| Nora Aunor
|  
|-
| align="center"| New movie Actress of the Year
| align="center"| Glenda Kennedy
|  
|-
| align="center"| Indie Movie Screenwriter of the Year
| align="center"| Henry Burgos
|  
|-
| align="center"| Indie Movie Editor of the Year
| align="center"| Kats Seraon
|  
|-
| align="center"| Indie Movie Musical Scorer of the Year
| align="center"| Teresa Barrozo
|  
|-
| align="center"| Indie Movie Sound Engineer of the Year
| align="center"| Albert Michael Idioma and Addiss Tabong
|  
|- 10th Golden Screen Awards  
| align="center"| Best Motion Picture-Drama
| align="center"| 
|  
|-
| align="center"| Best Direction
| align="center"| Brillante Mendoza
|  
|-
| align="center"| Best Performance by an Actress in a Lead Role-Drama
| align="center"| Nora Aunor
|  
|-
| align="center"| Best Original Screenplay
| align="center"| Henry Burgos
|  
|-
| align="center"| Best Cinematography
| align="center"| Odessey Flores
|  
|-
| align="center"| Best Production Design
| align="center"| Dante Mendoza
|  
|-
| align="center"| Best Sound Design
| align="center"| 
|  
|-
| align="center"| Best Visual/Special Effects
| align="center"| Keep Me Posted Production Outfit 
|  
|- 31st Film Academy of the Philippines (Luna Awards)
| align="center"| Best Picture
| align="center"| 
|  
|-
| align="center"| Best Actress
| align="center"| Nora Aunor
|  
|-
| align="center"| Best Screenplay
| align="center"| Henry Burgos
|  
|-
| align="center"| Best Production Design
| align="center"| Dante Mendoza
|  
|-
| rowspan="10" | Gawad Tangi (Kritiko ng Pelikula, Telebisyon at Musikang Pilipino)  
| align="center"| Best Lead Actress
| align="center"| Nora Aunor
|  
|-
| align="center"| Best Art Direction
| align="center"| Brillante Mendoza
|  
|-
| align="center"| Best Picture 
| align="center"| 
|  
|-
| align="center"| Best Director
| align="center"| Brillante Mendoza
|  
|-
| align="center"| Best Screenplay
| align="center"| Henry Burgos
|  
|-
| align="center"| Best Cinematography
| align="center"| Odessey Flores
|  
|-
| align="center"| Best Original score
| align="center"| Teresa Barrazo
|  
|-
| align="center"| Best Editing
| align="center"| Kats Seraon
|  
|-
| align="center"| Best Ensemble
| align="center"| 
|  
|-
| align="center"| Best Language
| align="center"| 
|  
|-
| rowspan="5" | 1st Philippine Edition Movie Awards 
| align="center"| Best Drama Movie 
| align="center"| 
|  
|-
| align="center"| Favorite Actress - Drama
| align="center"| Nora Aunor
|  
|-
| align="center"| Favorite Actor - Drama
| align="center"| Bembol Roco
|  
|-
| align="center"| Favorite Director
| align="center"| Brillante Mendoza
|  
|-
| align="center"| Favorite Actress - Drama
| align="center"| Lovi Poe
|  
|-
|align="center"| 2014
| PPA Sisa Media Awards  
| align="center"| Outstanding Female Character in a Mainstream Filipino Film 
| align="center"| Nora Aunor
|  
|}

==References==
 

==External links==
* 

 

 
 
 
 
 