Orders Are Orders
{{Infobox Film
| name           = Orders Are Orders
 
| image_size     =
|  image         = "Orders_are_Orders"_(1954).jpg
| caption        = 
| director       = David Paltenghi
| producer       = Donald Taylor
| writer         = Geoffrey Orme Donald Taylor  Eric Sykes  (additional dialogue) Anthony Armstrong Ian Hay
| narrator       = 
| starring       = Peter Sellers Tony Hancock Sid James Arthur Grant
| music          = Stanley Black
| editor         = Joseph Sterling
| distributor    = British Lion Film Corporation (UK) Distributors Corporation of America (US)
| released       = October 1954
| runtime        = 78 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross = £84,148 (UK) 
| preceded_by    = 
| followed_by    = 
}}

Orders Are Orders is a 1954 British comedy film directed by David Paltenghi, and featuring Peter Sellers, Sid James, Tony Hancock, Raymond Huntley, Donald Pleasence and Eric Sykes.  It was a remake of the 1933 film Orders Is Orders. 

==Synopsis==
A film production company decides to make a new science fiction film in an army barracks, using the soldiers as extras. This does not go down well with the commanding officer, who attempts to make life as difficult as possible for the film crew.

==Cast==
* Brian Reece&nbsp;&mdash; Captain Harper
* Margot Grahame&nbsp;&mdash; Wanda Sinclair
* Raymond Huntley&nbsp;&mdash; Colonel Bellamy
* Sid James&nbsp;&mdash; Ed Waggermeyer
* Tony Hancock&nbsp;&mdash; Lieutenant Wilfred Cartroad
* Peter Sellers&nbsp;&mdash; Private Goffin
* Clive Morton&nbsp;&mdash; General Sir Cuthbert Grahame-Foxe
* June Thorburn&nbsp;&mdash; Veronica Bellamy
* Maureen Swanson&nbsp;&mdash; Joanne Delamere Peter Martyn&nbsp;&mdash; Lieutenant Broke
* Bill Fraser&nbsp;&mdash; Private Slee
* Edward Lexy&nbsp;&mdash; Captain Ledger Barry MacKay&nbsp;&mdash; RSM Benson
* Donald Pleasence&nbsp;&mdash; Corporal Martin
* Eric Sykes&nbsp;&mdash; Private Waterhouse Leonard Williams &nbsp;&mdash; Corporal Smithers

==Media releases== region two DVD in 2007. 

==Critical reception==
*TV Guide wrote, "except for a couple of decent comic performances, the good cast, including both Peter Sellers and Donald Pleasence in early roles, are wasted by the films haphazard construction."   Time Out wrote, "just about worth suffering to see Tony Hancock in his film debut as the harassed bandmaster."  

==References==
 

== External links ==
*  

 
 
 
 
 


 
 