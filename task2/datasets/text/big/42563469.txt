Naples Will Never Die
{{Infobox film
| name = Naples Will Never Die
| image =
| image_size =
| caption =
| director = Amleto Palermi
| producer = Giulio Manenti
| writer = Jean-Georges Auriol   Ernesto Murolo   Cesare Giulio Viola   Amleto Palermi 
| narrator =
| starring = Fosco Giachetti   Marie Glory   Paola Barbara   Cesare Bettarini
| music = Alessandro Cicognini 
| cinematography = Anchise Brizzi 
| editing = Fernando Tropea 
| studio = Manenti Film 
| distributor = Manenti Film 
| released = 18 February 1939 
| runtime = 88 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Naples Will Never Die (Italian:Napoli che non muore) is a 1939 Italian comedy film directed by Amleto Palermi and starring Fosco Giachetti, Marie Glory and Paola Barbara.  A young French tourist on holiday in Naples meets and falls in love with an engineer. She marries him, but finds his family overbearing and traditional while they condsider her to be too extrovert. She leaves him and returns to France, but the couple are eventually re-united.

==Cast==
* Fosco Giachetti as Mario Fusco 
* Marie Glory as Annie Fusco 
* Paola Barbara as Teresa 
* Bella Starace Sainati as Donna Amalia Fusco 
* Cesare Bettarini as Pietro 
* Carla Sveva as Lia 
* Ennio Cerlesi as Enrico 
* Armando Migliari as Daspuro 
* Clelia Matania as Rosinella 
* Giuseppe Porelli as Il maestro Califano 
* Gianni Agus as Bebè  Vittorio Parisi as Il cantate

== References ==
 

== Bibliography ==
* Gundle, Stephen. Mussolinis Dream Factory: Film Stardom in Fascist Italy. Berghahn Books, 2013.
* Liehm, Mira. Passion and Defiance: Film in Italy from 1942 to the Present. University of California Press, 1984.
 
== External links ==
*  

 
 
 
 
 
 
 
 
 
 


 