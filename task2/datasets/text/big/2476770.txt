God of Gamblers II
 
 
{{Infobox film
| name           = God of Gamblers II
| image          = GodofGamblersII.jpg
| caption        = Film poster
| film name = {{Film name| traditional    = 賭俠
 | simplified     = 赌侠
 | pinyin         = Dǔ Xiá
 | jyutping       = Dou2 Hap6}}
| director       = Wong Jing
| producer       = Jimmy Heung
| writer         = Wong Jing
| starring       = Andy Lau Stephen Chow Monica Chan Ng Man-tat Sharla Cheung Charles Heung
| music          = Lowell Lo
| cinematography = David Chung
| editing        = Poon Hung
| studio         = Wins Entertainment, Ltd.|Wins Movie Production & I/E Co. Ltd. Golden Harvest
| released       =  
| runtime        = 99 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         =
| gross          = HK$40,342,758
}} action comedy film written and directed by Wong Jing.  It stars Andy Lau as Little Knife, Stephen Chow as the Saint of Gamblers, and Ng Man-Tat as Blackie Tat. This movie is a sequel to both God of Gamblers, which included Laus character, and to All for the Winner, which starred Chow and Ng. This film should not be confused with God of Gamblers Returns, also released as God of Gamblers 2, and in essence the true sequel to the original God of Gamblers.

==Summary==
In this movie, down on his luck, Sing (Stephen Chow), seeks out the God of Gamblers, in hopes of becoming his disciple. Unfortunately, the God of Gamblers is unreachable and out of the country, last heard to be in Brazil, and Michael Chan, the Knight of Gamblers (Andy Lau) replaces the God of Gamblers. When a phony attempts to impersonate the Knight of Gamblers in a huge scam, its up to the real Knight and Saint of Gamblers to team up and defeat their formidable challenger. The film combines intricate action sequences with sharp comedic timing.

==Cast==
*Andy Lau as Michael Chan / Little Knife / Knight of Gamblers
*Stephen Chow as Cho Chung-sing / Saint of Gamblers
*Monica Chan as Long Kau / Kowloon
*Ng Man-tat as Uncle Three / Blackie Tat
*Sharla Cheung as Dream Lo
*Charles Heung as Lung Ng
*Pal Sinn as Hussein
*Blackie Ko as Black Panther
*John Ching as Tai Kwan
*Ronald Wong as Raven
*Shing Fui-On as Brother Kau
*Pau Hon Lam as Chan Kam-sing /Beast of Gamblers
*Luk Chuen as Mr. Yama
*Kirk Wong as Dai Anfen
*Wong Jing as Ham Ka Chan (uncredited cameo)
*Chow Yun-fat as Ko Chun / God of Gamblers (uncredited cameo; archive footage)

==Title==
Even though the international English title is God of Gamblers II, this is not the direct sequel to the 1989 film starring Chow Yun-fat.  This films title literally translates to The Knight of Gamblers, the name of Andy Laus character.

==Significance of the film==
Wong Jing was so impressed with All for the Winner that he contacted Stephen Chow to star in two sequels made during 1991. This combo went on to make several money making films turning Stephen Chow into an Asian Comedy star and helping boost Jing Wongs status as one of the top film makers in Hong Kong.

This movie is followed by  , which does not have Andy Lau, but features actress Gong Li in two roles.

==See also==
*God of Gamblers (1989)
*All for the Winner (1990)
*  (1991)
*God of Gamblers Returns (1994)
*  (1997)
*Andy Lau filmography
*Wong Jing filmography

==External links==
* 
* 


 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 