The Jewel of the Nile
{{Infobox film
| name           = The Jewel of the Nile
| image          = The Jewel of the Nile (1985) film poster.jpg
| caption        = Promotional film poster
| director       = Lewis Teague
| producer       = Michael Douglas
| writer         = Mark Rosenthal Lawrence Konner
| based on        =  
| narrator       = 
| starring = {{Plainlist|
* Michael Douglas
* Kathleen Turner
* Danny DeVito
}}
| music          = Jack Nitzsche
| cinematography = Jan De Bont
| editing        = Peter Boita Michael Ellis
| studio         = 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 107 mins
| country        = France Morocco United States
| language       = English Arabic
| budget         = $25 million 
| gross          = $96.7 million 
| website        =
}} adventure romantic romantic comedy and a sequel to the 1984 film Romancing the Stone, with Michael Douglas, Kathleen Turner, and Danny DeVito reprising their roles. Directed by Lewis Teague, the film sends its characters off on a new adventure in a fictional African desert, in an effort to find the precious "Jewel of the Nile."
The film was a box office success.

==Plot==
Six months after the events in Romancing the Stone, Joan Wilder (Kathleen Turner) is having trouble writing her next romantic novel while living with Jack Colton (Michael Douglas) on his boat, the Angelina, which is moored in a South of France port, and she refuses to discuss marriage. Later that afternoon at a book signing engagement held by her publisher, Gloria (Holland Taylor), Joan meets a charming Arab ruler named Omar (Spiros Focás) who has managed to persuade the world that he is the firm, but fair, ruler of Kadir. Omar offers Joan the opportunity to live like a queen at his palace, while she writes a fluff piece about him. However, as soon as Joan leaves with Omar, Jack runs into Ralph (Danny DeVito). Ralph, a comical swindler from Jacks past, recently out of prison, plans on killing Jack for abandoning him to the authorities in Colombia, but is stopped by an Arab named Tarak (Paul David Magid), who tells Jack of Omars true intentions. This includes the fact that he has their greatest treasure, the Jewel of the Nile, in his possession. Ralph, immediately interested at the prospect, agrees to help find the Jewel. Jack, however, is less than convinced. But seconds later, the Angelina explodes, having been sabotaged under Omars orders. He then agrees to team up with Ralph and Tarak in order to track down the lost jewel in Omars kingdom.
 Arabic for Avner Eisenberg), Sufi tribe led by Tarak that has sworn to protect the Jewel so he can fulfill his destiny.
 British rock & roll technicians (Daniel Peacock) smoke and mirrors special effects at an upcoming festival, planned by Omar, to convince the Arab world that he is in fact a prophet that will unite the Arab world under his rule. Jack, Joan and Al-Julhara decide to crash the festival in Kadir and unmask Omar as the fraud that he is. However, they are all captured and Omar sets up an elaborate and fiendish trap from The Savage Secret, Joans most popular novel. Jack and Joan are suspended over a deep pit; the ropes holding up Jack are soaked with goats blood and being rapidly chewed away by rats, while those supporting Joan are slowly being dissolved by drops of acid. Al-Julhara, however, is simply locked up in stocks. As Omar leaves them to their fate, they are found later and inadvertently saved by Ralph, who along with Tarak and his Sufi followers have come to rescue Al-Julhara.

As Omar takes center stage to address the Arab people, Jack and Joan disrupt the ceremony while Tarak and the Sufi battle Omars guards below. A fire breaks out when part of Omars platform apparatus crashes into the stage, engulfing it in flames. Jack and Joan are separated in the chaos and Omar corners her at the top of the burning scaffolding surrounding the stage. With help from Ralph, Jack rides a crane to the top of the scaffolding and knocks Omar over the side and down into the flames below just as he is about to kill Joan. Once Omar is killed, Al-Julhara rises as the real spiritual leader (fulfilling the prophecy by walking through fire unharmed) and Jack and Joan are finally married by Al-Julhara himself the following day. While he is genuinely happy for Jack and Joan, Ralph laments sadly that once again, he has nothing to show for his efforts. But he is then acknowledged as being a true Sufi by Tarak, signified by being presented with a priceless jeweled dagger. Ralph is genuinely touched and happily accepts the gift. The film ends with Jack and Joan sailing down the Nile as Al-Julhara and his people, along with Ralph, Tarak, the Sufi and Gloria wave goodbye from the rivers dock.

==Cast==
*Michael Douglas as Jack Colton 
*Kathleen Turner as Joan Wilder 
*Danny DeVito as Ralph 
*Spiros Focás as Omar  Avner Eisenberg as Jewel 
* The Flying Karamazov Brothers
:* Paul David Magid as Tarak 
:* Howard Jay Patterson as Barak 
:* Randall Edwin Nelson as Karak 
:* Samuel Ross Williams as Arak 
:* Timothy Daniel Furst as Sarak 
*Hamid Fillali as Rachid 
*Daniel Peacock as Rock Promoter
*Holland Taylor as Gloria 
*Guy Cuevas as Le Vasseur 
*Peter DePalma as Missionary (as Peter De Palma) 
*Mark Daly Richards as Pirate

==Response== The War of the Roses.

Critics felt the film was loaded with numerous plot holes and that it lacked the first films original charm. The New York Times opened its review by writing, "Theres nothing in The Jewel of the Nile that wasnt funnier or more fanciful in Romancing the Stone."  Roger Ebert agreed that "it is not quite the equal of Romancing the Stone," but praised the interplay between Douglas and Turner. "It seems clear," he wrote, "that they like each other and are having fun during the parade of ludicrous situations in the movie, and their chemistry is sometimes more entertaining than the contrivances of the plot." 

==Soundtrack== When the Going Gets Tough, the Tough Get Going", performed by Billy Ocean, plays during the films end credits. Douglas, Turner, and DeVito also co-starred with Ocean in the MTV music video of the same name. The soundtrack features 1980s rap group Whodini and their single "Freaks Come Out at Night" as Michael Douglas and company make their way through the desert on camel back  as well as "Party (No Sheep Is Safe Tonight)" by The Willesden Dodgers during the campfire party scene.
 Arista released a soundtrack album on record, cassette and compact disc.
 When the Going Gets Tough, the Tough Get Going - Billy Ocean (5:43)
# Im In Love - Ruby Turner (3:30)
# African Breeze - Hugh Masekela and Jonathan Butler (6:00) 
# Party (No Sheep Is Safe Tonight) - The Willesden Dodgers (5:10)
# Freaks Come Out At Night - Whodini (4:45)
# The Jewel Of The Nile - Precious Wilson (4:18) 
# Legion (Here I Come) - Mark Shreeve (4:49)
# Nubian Dance - The Nubians (3:35) 
# Love Theme - Jack Nitzsche (2:26) 
# The Plot Thickens - Jack Nitzsche (4:15)

==Production notes== ghost written by Catherine Lanigan. 

Approximately two weeks before principal photography began, a plane carrying Richard Dawking (production designer) and Brian Coates (production manager) crashed during location scouting over the countryside of Morocco, killing all on board.
The film is dedicated to the memory of Dawking and Coates, as well as Diane Thomas.
 RCA SelectaVision CED video format.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 