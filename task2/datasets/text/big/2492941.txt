Shakes the Clown
{{Infobox film
| name           = Shakes the Clown
| image          = Shakes the Clown.jpg
| caption        = Theatrical release poster
| director       = Bobcat Goldthwait Ann Luly-Goldthwait
| writer         = Bobcat Goldthwait
| starring       = Bobcat Goldthwait Julie Brown Tom Kenny Blake Clark Adam Sandler Robin Williams Tom Scott Elliot Davis
| editing        = J. Kathleen Gibson IRS Media (Theatrical) Sony Pictures Home Entertainment|Columbia-Tristar Home Video (Home Video)
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = $1.4 million 
| box office     = $115,103
}}
 Jack Gallagher, and a cameo by Robin Williams as Mime Jerry (using the pseudonym "Marty Fromage", an homage to an earlier film they worked in together called Tapeheads in which Goldthwait used the pseudonym "Jack Cheese"). 
 mimes and other performers are depicted as clannish, rivalrous subcultures obsessed with precedence and status. This was Goldthwaits bitter satire of the dysfunctional standup comedy circuit he knew as a performer. 

==Cast==
*Bobcat Goldthwait as Shakes the Clown
*Julie Brown as Judy
*Blake Clark as Stenchy the Clown
*Paul Dooley as Owen Cheese
*Kathy Griffin as Lucy
*Tom Kenny as Binky the Clown
*Adam Sandler as Dink the Clown Jack Gallagher as Officer Crony
*Florence Henderson as The Unknown Woman
*Scott Herriott as Floor Director
*LaWanda Page as Female Clown Barfly
*Robin Williams as Mime Jerry
*Johnny Silver as Clown Tailor

==Reception==
Shakes the Clown was not a financial success, earning only about $115,000 in ticket sales against an estimated budget of $1.4 million.  

Critical reaction to the movie was mixed: Leonard Maltin gave it his lowest rating, while Betsy Sherman of The Boston Globe called it "the Citizen Kane of alcoholic clown movies".     Roger Ebert gave Shakes 2-out-of-4 stars, writing that while some isolated scenes were "very funny" the plot was scattered and the performances often seemed under-rehearsed.  As of mid-2011, the movie has a 37% rating on Rotten Tomatoes, based on 19 reviews. 

In an interview with Conan OBrien, Goldthwait revealed that no less than Martin Scorsese had defended the movie from detractors. When a film critic derided the movie in order to make a point about good and bad movies, Scorsese revealed, "I like Shakes the Clown. Havent you heard? Its the Citizen Kane of Alcoholic Clown Movies!"
 

==References==
 

==External links==
 
* 
* 
 

 
 
 
 
 
 
 
 
 
 
 
 