The Pretenders (1916 film)
{{infobox film
| title          = The Pretenders
| image          = The Pretenders.jpg
| imagesize      =
| director       = George D. Baker
| producer       = B. A. Rolfe(Rolfe Photoplays) Channing Pollock(story) George D. Baker(scenario)
| starring       = Emmy Wehlen
| music          =
| cinematography = William Wagner
| editing        =
| distributor    = Metro Pictures
| released       = August 21, 1916
| runtime        = 5 reels
| country        = USA
| language       = Silent..(English intertitles)
}} Channing Pollock.  Stage actress Emmy Wehlen starred. 

==Cast==
*Emmy Wehlen - Helen Pettingill
*Paul Gordon - Hubert Stanwood
*Charles Eldridge - Silas T. Pettengill
*Kate Blancke - Maria Pettingill
*Edwin Holt - Inspector Burke
*William B. Davidson - Joseph Bailey (*as William Davidson)
*Howard Truesdale - John Stafford (*as Howard Truesdell)
*Jerome N. Wilson - Joseph Bailey (as Jerome Wilson)
*Ilean Hume - Rita
*Harry Neville - Dugan
*Hugh Jeffrey - Andrews
*George Stevens -

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 