Champagnegaloppen
 
{{Infobox film
| name           = Champagnegaloppen
| image          = Champagnegaloppen.jpg
| caption        = DVD
| director       = George Schnéevoigt 
| producer       = 
| writer         = Hans Christian Lumbye (musical) Paul Holck-Hofmann (play), Fleming Lynge (writer)
| narrator       = 
| starring       = Svend Methling Valdemar Møller Agnes Rehni
| music          = Viggo Barfoed Alfred Kjerulf
| cinematography = Valdemar Christensen
| editing        = Valdemar Christensen Carl H. Petersen
| distributor    = Nordisk Film
| released       =  
| runtime        = 93 minutes
| country        = Denmark
| language       = Danish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Champagnegaloppen is a 1938 Danish musical film directed by George Schnéevoigt. The film based on a musical by Hans Christian Lumbye and play by Paul Holck-Hofmann and stars Svend Methling and Valdemar Møller. It is named after the light classical piece composed by Hans Christian Lumbye, who is a character in the film.

==Cast==
*Svend Methling as  H.C. Lumbye 
*Valdemar Møller as  Tobias Hambroe 
*Agnes Rehni as Abelone Hambroe 
*Annie Jessen as Amelie Hambroe 
*Marius Jacobsen as Musiker Jens Werning
*Victor Montell as Musiker Køster  John Price as Musiker Lindemann 
*Johannes Poulsen as  Koncertmester Salomon Bierbaum 
*Eigil Reimers as Baron von Listow 
*Torkil Lauritzen. as Løjtnant Seefeld 
*Gunnar Helsengreenas  Opvartersken Lene 
*Else Højgaard as  Danserinden Arabella 
*Helga Frier as  Frk. Züberlein

==External links==
*  

 
 
 
 
 
 


 
 