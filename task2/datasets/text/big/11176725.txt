Summer Time Machine Blues
{{Infobox film |
  name     = Summer Time Machine Blues |
  image          = Summer time machine blues.jpg |
  caption        =  |
  director       = Katsuyuki Motohiro | Makoto Ueda | Yoko Maki  Tsuyoshi Muro  Riki Honda  Kuranosuke Sasaki |
  cinematography = Kazunari Kawagoe |
  producer       = Chikahiro Ando  Masaki Koide |
  distributor    = Toshiba Entertainment |
  released   =   |
  runtime        = 107 minutes |
  language = Japanese  |
  country = Japan |
  music          =  |
  budget          =  |
}} Japanese film Yoko Maki as part of the neighboring photography club.

==Plot==

Summer Time Machine Blues is based around a group of friends in a science-fiction club who spend their days fooling around at the clubhouse.  The boys play a game of baseball as their friend Yui Ito takes pictures of them nearby.  After the game, the boys return to the clubhouse and head off to the bathhouse leaving the girls, Yui and Haruka to develop their pictures.  While they bathe, Niimi gets angry because he thinks someone stole his Vidal Sassoon shampoo.  On their way back, Komoto decides to sneak off to buy tickets at the theater for a sci-fi B movie.  He hopes that he can ask Haruka out.  Haruka says that she does not want to go out with him since he apparently has a girlfriend, but Komoto has no idea what she is talking about. But when he returns to the clubhouse, his friends act very strangely as to where he has been.  They make him explain what happened, but through a series of chain events, Ishimatsu accidentally spills Coke all over the air-conditioner remote control after being hit in the face.  Everyone rushes to try to clean the remote off, but its too late.  The remote is broken and they cannot turn on the air-conditioner.  This immediately becomes a problem as the day becomes hotter.

The remote apparently cannot be repaired as it is too old, so the boys give it to their club advisor to try to fix. They look for a fan in the meantime but only find broken ones.  Back at the clubhouse, the boys discover a strange boy in the room who hurriedly escapes, leaving behind some sort of machine.  They put Soga on the time machine thinking it is a practical joke on the sci-fi club, but to their surprise it actually works. Soga appears in a picture of their baseball game that they held yesterday. They fool around discovering that it is a time machine.  The boys decide to travel to the past to steal the remote control before it was broken and bring it back to the present.  But as soon as they step back one day, they run into problems beyond their control.

While Ishimatsu, Niimi and Koizumi go back in time to retrieve the remote. The strange guy is revealed to be Tamura who is a member of the sci-fi 25 years in the future. Tamura gets sent to the past the same way Soga was coerced onto the machine. The remaining members, Haruka, Soga, Ito, and Komoto take Tamura on a tour of the past. While on the tour they encounter their club adviser, Professor Kohtaro Hozumi, referred to as Hose, who explains that changing the past results in the grandfather paradox and would cause the universe to vanish. Realizing this the remaining club members rush back to ensure that the Ishimatsu, Niimi, and Koizumi do not change the past. Ishimatsu and Koizumi get sent back to the present but they return shortly with Tamura after being informed about the situation. They find the time machine waiting for them when they get back to the club room. Soga and Takuma go to the past and prevent the trio from changing the past. In the process Soga wraps the remote in tape and gets transported to 99 years into the past to prevent the caretaker from discovering the time machine. The school being originally a swamp, Soga almost drowns before returning to the present. The past villagers mistaken him as a Kappa and Kappas becomes integral to the town culture. Soga also loses the remote 99 years in the past.

Komoto finds Niimi at the bathhouse and in a twist it is revealed that Niimi himself took the shampoo. With the remote missing Tamura goes into his future to retrieve the futures remote so that the past wont be altered. Everyone gets on the time machine but the machine is too small and Komoto gets knocked off. Komoto deceives the past club members who mistakenly believe that he has a girlfriend due to a voice message Komoto sent to the "present Soga" which "past Soga" received. Komoto distracts the club members but before he can leave his past self enters the club room. With no other options Komoto hides in the club room locker. The present club members return and notice that Komoto is not with them, but Komoto is already in the present having spent the whole day in the locker. A dog digs up a remote which is revealed to have survived for 99 years in mud and the remote still works. Tamura returns to his present but forgets his camera. When he returns for it he says that it belonged to his mother and Komoto and Ito note that Haruka also possess a similar camera. When they met, Tamura notes that his mother also attended this college. Komoto and Ito deduce that Tamura is the son of future Haruka.

Professor Kohtaro Hozumi is inspired by these events to build a time machine which might explain the time machines origins since Tamura and his club members also did not know the machines origins.

Since Takumas girlfriend was revealed a lie, Haruka agrees to go on a date with Komoto. Komoto wonders how he can change his name (presumably to Tamura).

==External links==
* 
* 
* 
* 
*  

 

 
 
 
 
 
 
 