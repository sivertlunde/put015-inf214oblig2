The President's Plane Is Missing (film)
 
{{Infobox film
| name           = The Presidents Plane Is Missing
| image          = The Presidents Plane is Missing VHS cover.jpg
| alt            =
| caption        = VHS box coverart
| film name      = 
| director       = Daryl Duke
| producer       =  
| screenplay     =  
| based on       =  
| starring       =  
| narrator       = 
| music          = Gil Melle
| cinematography = Richard C. Glouner
| editing        = John F. Link ABC Circle Films
| distributor    = American Broadcasting Company
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
 same name. Vice President Secretary Of National Security President Jeremy Haines.        
 diplomatic relationship visit to China.     The film was subsequently released on October 23, 1973,    as a period piece.   

==Plot== Acting President. Washington political community doubts his competence. In dealing with growing tensions and conflicting advice, Madigan struggles to avoid a nuclear war with the Chinese. Meanwhile, it develops that President Haines was not aboard the crashed plane after all.

==Cast==
 
* Buddy Ebsen as Vice President Kermit Madigan
* Peter Graves as Mark Jones Arthur Kennedy as Gunther Damon
* Raymond Massey as Secretary of State Freeman Sharkey
* Mercedes McCambridge as Hester Madigan
* Rip Torn as National Security Advisor George Oldenburg
* Louise Sorel as Joanna Spencer
* Dabney Coleman as Senator Bert Haines
* Joseph Campanella as Colonel Doug Henderson
* Richard Eastham as General Colton
* Byron Morrow as Admiral Phillips Bill Walker as Thomas Richard Bull as First Controller
* Richard Stahl as Dentist
* Gil Peterson as Tower Controller Barry Cahill as Ground Crew Chief
* Lillian Lehman as Genesse
* James Sikking as Aide to Dunbar
* Barbara Leigh as WAF
* George Barrows as Mr. Meyers
* John Amos as Marine Corporal John Ward as Major DAndrea
* Tod Andrews as President Jeremy Haines James Wainwright as General Ben Dunbar James B. Smith as Major Earl Foster  
 

==Reception==
In "Cinema and Nation" (ISBN 1134618840) when comparing this film to such as JFK (film)|JFK (1991) and The Manchurian Candidate (1962 film)|The Manchurian Candidate (1962) it was pointed out that while many films use a premise that actual democracy is an illusion, this one was rare in that it turned the President into an action hero.     In 1988, Sydney Morning Herald wrote that it was a "dull film despite an excellent cast."   

==Novel==
 
Robert J. Serlings 1967 novel spent multiple weeks on the New York Times Bestseller List and its success enabled Serling to become a full-time writer.  Serling later penned a sequel to the novel entitled Air Force One Is Haunted, which centered around former President Franklin Delano Roosevelt haunting the current President whenever he boards Air Force One.   Filming the project had been completed in 1971, but Peter Graves reported that the film did not air until 1973 due to the films villains being Chinese and President Richard Nixon being involved in negotiations with China.   

==References==
 

==External links==
*   at the Internet Movie Database

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 