A Killer Within
{{Infobox film
| name           = A Killer Within
| image          = AKillerWithin2004Poster.jpg
| alt            =  
| caption        = Film poster
| director       = Brad Keller
| producer       = Brad Keller Joey Stewart
| writer         = Chris Peirson William Peirson
| screenplay     = 
| story          = 
| based on       =  
| starring       = C. Thomas Howell Sean Young Ben Browder Dedee Pfeiffer Giancarlo Esposito
| music          = William Richter
| cinematography = Mike Redding
| editing        = Russ G. Gregg
| studio         = 
| distributor    = 
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

A Killer Within is a 2004 film featuring C. Thomas Howell as Addison Terrill, Sean Young as Rebecca Terrill, Ben Browder as Sam Moss, Dedee Pfeiffer as Sarah Moss and Giancarlo Esposito as Vargas.  The movie was directed by Brad Keller.

==Plot==
When Addison Terrills wife Becky is brutally murdered suspicion automatically falls on him. The fact the words "Were even now" were written on the bedroom wall next to her body seem to the police to make this a water tight case. But Addison claims he is innocent and if that is the case then everyone around him, including his best friend and partner Sam, Sams loving wife Sarah (who babysits the Terrills young son), even a disgraced cop he successfully prosecuted in the past, are potentially guilty. With time and evidence against him Addison and his unusual ally must race to clear his name.

There will be answers but some of them come at a heavy price...

==Cast==
* C. Thomas Howell as Addison Terrill  
* Sean Young as Rebecca Becky Terrill  
* Ben Browder as Sam Moss  
* Dedee Pfeiffer as Sarah Moss  
* Giancarlo Esposito as Vargas   Bill Flynn as Dr. Mark Shultz

==External links==
* 
* 

 
 
 
 
 
 


 