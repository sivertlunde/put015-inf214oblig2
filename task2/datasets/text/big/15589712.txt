New Year's Evil (film)
{{Infobox film
| name           = New Years Evil
| image          = New-years-evil.jpg
| alt            = 
| caption        = Film poster
| film name      = 
| director       = Emmett Alston
| producer       = Yoram Globus   Menahem Golan
| writer         = 
| screenplay     = Leonard Neubauer
| story          = Emmett Alston   Leonard Neubauer
| based on       = 
| starring       = Roz Kelly   Kip Niven   Grant Cramer   Chris Wallace
| narrator       = 
| music          = Laurin Rinder   W. Michael Lewis
| cinematography = Thomas E. Ackerman
| editing        = Richard S. Brummer
| studio         = The Cannon Group|Golan-Globus Productions Cannon Film Distributors
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 American horror new wave show host who receives a series of phone calls during a televised New Years Eve bash from a killer warning of impending murders that he plans to exact as the New Year dawns on each time zone.

== Plot ==

As the film begins New Years Eve is on its way and televisions most famous punk rock lady icon, Diane Sullivan or Blaze as her fans call her, is holding a late night countdown celebration of music and partying. All is going well until Diane receives a phone call from an odd-sounding stranger claiming his name is Evil, who announces on live television that when the clock strikes twelve in each time zone, a Naughty Girl will be punished (murdered), then the killer signs off with a threat claiming that Diane will be the last Naughty Girl to be punished.
 crazed fan, a religious psychotic, or maybe its someone much closer to Diane than anyone could have ever expected.

The killer eventually gets caught trying to kill Diane and flees from the scene. He races toward the rooftop, where he commits suicide by jumping. The survivor is loaded into an ambulance, while her son is seen wearing the killers old mask in the ambulance with the corpse of the medic at the front.

== Cast ==

* Roz Kelly as Diane Sullivan
* Kip Niven as Richard Sullivan
* Chris Wallace as Lieutenant Ed Clayton
* Grant Cramer as Derek Sullivan
* Louisa Moritz as Sally
* Jed Mills as Ernie Moffet
* Taaffe OConnell as Jane
* Jon Greene as Sergeant Greene
* Teri Copley as Teenage Girl
* Anita Crane as Lisa
* Jennie Anderson as Nurse Robbie
* Alicia Dhanifu as Yvonne
* Wendy-Sue Rosloff as Make-up Girl
* John London as Floor Manager
* John Alderman as Doctor Reed
* Michael Frost as Larry

== Soundtrack ==

Composer(s):
:W. Michael Lewis
:Laurin Rinder

# "New Years Evil" - Written by Roxanne Seeman and Eduardo del Barrio
# "When I Wake Up" - Written by John Pakalenka
# "Simon Bar Sinister" - Written by Clifford White and Ray Leonard
# "Temper Tantrum" - Written by Ray Leonard
# "Headwind" - Written by Clifford White
# "Cold Hearted Lover" - Written by Clifford White
# "Auld Lang Syne"
# "Dumb Blondes" - Written by Tony Fried 
# "The Cooler" - Written by Tony Fried url = title =  last = first = publisher = date = website = accessdate = 13 July 2014}}  

== Reception ==
 before 1978."   Eric Vespe of Aint It Cool News said, "New Years Evil falls into that didnt love it, didnt hate it gray area of mediocrity that doesn’t exactly inspire any kind of passion one way or the other. On the one hand its too goofy and amateurish to really be creeped out by and on the other its not fun enough to rally behind."   Dread Centrals Matt Serafini concluded, "This isnt worth your time if youre looking for a horror film to deliver in scares or suspense, but as a late night horror fix, its ideal. What New Years Evil lacks in scares it makes up for in pure entertainment. And really, thats all you can ask for."   The film was labeled "another routine mad-slasher film" and a "strictly paint-by-numbers effort" by TV Guide.  

== References ==

 

== External links ==

*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 