Dead Birds (1963 film)
 
{{Infobox film
| name           = Dead Birds
| image          =
| image_size     =
| caption        = Robert Gardner
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = Documentary Educational Resources
| released       =  
| runtime        = 84 min.
| country        = United States
| language       = English
| budget         =
}} Robert Gardner Peabody Expedition to study the highlands of New Guinea, at that time one of the few remaining areas in the world uncolonized by European ethnic groups|Europeans.   

The premiere of Dead Birds took place at the Loeb Drama Center at Harvard University in October 1963.
In 1998, this film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==Synopsis==
The films title is borrowed from a Dani fable that Gardner recounts in voice-over. The Dani people, whom Gardner identifies mysteriously as "a mountain people," believe that there was once a great race between a bird and a snake, which was to determine the lives of human beings. Should men shed their skins and live forever like snakes, or die like birds? The bird won the race, dictating that man must die.

The films plot revolves around two characters, Weyak and Pua. Weyak is a warrior who guards the frontier between the land of his tribe and that of the neighboring tribe. Pua is a young boy whom Gardner depicts as weak and inept.

==Criticism==
Though stylistically impressive, Dead Birds has been criticized with respect to its authenticity. The characters who speak in the film are never subtitled, and even then the voice itself is not always what it seems. What the audience perceives as Weyaks voice is actually a post-filming dub of Karl G. Heider speaking Dani. Gardner himself did not speak Dani, and so all his interpretations of events are second-hand. The battle sequences are made up of many shots taken during different battles and stitched together to give the appearance of temporal unity. The apparent continuity stems from the post-synchronized sound, and in fact all the sound in the film is post-synched. Heider, himself, admits in his book Ethnographic Film, that some of the battle films were edited out of sequence, intercut with scene from the women at the salt pool, which was also taken at a different time. 

==Notes==
 

==References==

*Ruby, Jay. Picturing Culture: Explorations of Film and Anthropology. Chicago: U. of Chicago Press, 2000.
*Heider, Karl. Ethnographic Film. Austin: University of Texas Press, 1978.
*Kirsch, Stuart. "Ethnographic Representation and the Politics of Violence in West Papua". Critique of Anthropology 30(1):3–22, 2010.

==External links==
*  
*  

 
 
 
 
 
 
 
 
 