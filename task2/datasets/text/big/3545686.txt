Life at the End of the Rainbow
{{Infobox film
| name           = Life at the End of the Rainbow
| image          = Rainbow PosterA4.jpg
| alt            = 
| caption        = Life at the End of the Rainbow
| director       = Wayne Coles-Janess
| producer       = 
| writer         = Wayne Coles-Janess
| starring       = 
| music          = Helen Mountfort
| cinematography = Steven Williams Wayne Coles-Janess
| editing        = Wayne Coles-Janess
| studio         = 
| distributor    = Ipso Facto Productions Pty. Ltd.
| released       =  
| runtime        = 55 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = 
}} documentary by Big Desert, North Western Victoria, Australia.

==Synopsis== German settlers. first and second World Wars. Rainbow and its people have struggled to eke out an existence for more than three generations, with global economics and government policy compounding the difficulties of marginal farming. The film uses home movies from the 1940s.

== See also ==
*In the Shadow of the Palms
*On The Border Of Hopetown
*Bougainville – Our Island Our Fight

==References==
* 

==External links==
*  
* 

 
 
 
 
 

 