The Lone Wolf's Daughter
{{Infobox film
|name= The Lone Wolfs Daughter
|image= Louise Glaum The Lone Wolfs Daughter 1 Film Daily 1919.png
|image_size=
|caption= Ad for film
|director= William P. S. Earle
|producer= J. Parker Read, Jr.
|writer= Louis Joseph Vance Edwin Stevens Thomas Holding
|music=
|cinematography= Charles J. Stumar
|editing= Ralph Dixon Thomas H. Ince
|distributor= Pathe Exchange
|released=  
|runtime= 70 min. (7-reel#Motion picture terminology|reels) Silent with English intertitles
|country= United States
|budget=
}} 1919 silent silent era thriller film|motion Edwin Stevens, and Thomas Holding. 
 Directed by produced by adapted by Lone Wolf, private detective. 
 Thomas H. Culver City, The Lone Wolf (1917 in film|1917) and The False Faces (1919). The movie premiered in Chicago. It was not exhibited in Los Angeles until January 12, 1920.

Glaum was acknowledged as a fashion plate for "wearing at least fifty different and striking Gown#Womens dress|gowns." "Many Thrills On The Screen --- Exciting Picture Plays Abound this Week --- Tallys Broadway." Los Angeles Times. Jan. 11, 1920. p. III 17. 

==Synopsis== set in Russian nobleman Corot Landscape landscape that has incriminating letters she wrote hidden inside. The painting is purchased by Michael Lanyard (played by Grassby), who is suspected of being the mysterious international thief the "Lone Wolf."

Lanyard gives the letters to Princess Sonia. She then divorces Prince Victor and marries Lanyard. With malevolent hatred, Victor threatens to follow Lanyard "to the very gates of Hell." Lanyard replies, "If you do, then Ill push you inside." Princess Sonia dies after giving birth to their daughter, Sonia. Lanyard is unaware that he has a daughter.
 Oriental criminals and Bolsheviks. Telling her that he is her father, he brings her to his home in the hope it will entice Lanyard to make an appearance. She falls in love with Roger Karslake (played by Holding), who is Victors secretary.
 Houses of Parliament, the homes of Downing Street and of the nobility, even Buckingham Palace, in order to clear the way for Victor to become Englands dictator, she tells Karslake.

Unbeknownst to Sonia or the gang, Lanyard has actually been working in the household, posing as Victors Oriental butler, and he and Karslake are both Scotland Yard agents. Lanyard learns that she is, in fact, his daughter. Following Sonias recognition of her father, the Lone Wolf, he and Karslake capture the gang amidst a blazing house fire and a huge fight. Victor makes his way to the roof pursued by Lanyard, who shoves the evil prince down into the flames.

==Cast==
*Bertram Grassby as Michael Lanyard, the Lone Wolf
*Louise Glaum as Princess Sonia and as her daughter, Sonia Edwin Stevens as Prince Victor
*Thomas Holding as Roger Karslake
*Fred L. Wilson in an undetermined role

==Reviews== review of Saturday, January 11, 1920, reads:

 
"Louise Glaums now starring feature, "The Lone Wolfs Daughter," comes to Tallys Broadway Theater, tomorrow, Louis Joseph Vance was the author of the original story and J. Parker Read, Jr., the producer. The supporting cast is notable, including Edwin Stevens, Thomas Holding, Bertram Grassby, and many others.

The scene of the story is London with a panorama of coloring, ranging from the magnificence of Buckingham Palace to the mysterious depths of the shadowy Limehouse district. The author personally arranged the scenario for Mr. Read, the producer.

Much of the action takes place in Soho, the French quarter of London, where Sonia (Louise Glaum) who knows nothing of her parentage or past, is the attraction for the curious slumming parties.

The plot centers about Sonias captivity in the house of a celebrated crook, her discovery of mysterious maneuverings to poison all London, and the intense climax which follows directly after her recognition of her father, "The Lone Wolf," who has been working, unknown to her or by her, in the same household.

In the play Miss Glaum incidentally reveals her talents as a fashion plate, wearing at least fifty different and striking gowns." "Many Thrills On The Screen --- Exciting Picture Plays Abound this Week --- Tallys Broadway." Los Angeles Times. Jan. 11, 1920. p. III 17. 
 

==See also==
*List of American films of 1919

==References==
 
 

==External links==
*  at the Internet Movie Database AFI Catalog of Feature Films

 
 
 
 
 
 
 
 
 
 
 