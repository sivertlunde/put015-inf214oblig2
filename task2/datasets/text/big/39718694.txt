The Rebel (1932 film)
{{Infobox film
| name           = The Rebel
| image          = Der Rebell 1932 Poster.jpg
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = {{Plainlist|
* Curtis Bernhardt
* Edwin H. Knopf
* Luis Trenker
}}
| producer       = {{Plainlist|
* Paul Kohner
* Joe Pasternak
* Alfred Stern
}}
| writer         = {{Plainlist|
* Walter Schmidtkunz
* Robert A. Stemmle
* Henry Koster  
}}
| story          = Luis Trenker
| starring       = {{Plainlist|
* Luis Trenker
* Luise Ullrich
* Victor Varconi
}}
| music          = Giuseppe Becce
| editing        = {{Plainlist| Hermann Haller
* Andrew Marton
}}
| cinematography = {{Plainlist|
* Sepp Allgeier
* Albert Benitz
* Willy Goldberger
* Reimar Kuntze
}}
| studio         = Deutsche Universal-Film
| distributor    = Deutsche Universal-Film
| released       =  
| runtime        = 82 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}}
 historical drama The Rebel was released the following year. The film is part of the Mountain film genre.
 Tyrolean mountaineer Tirolean Patriotism|patriot Andreas Hofer was a proto-type of "Severin Anderlan" !.. Trenker was designed to mirror what was happening in contemporary Germany as it rejected the terms of the Treaty of Versailles.  In 1933 there was published Luis Trenkers novel, Der Rebell. Ein Freiheitsroman aus den Bergen Tirols.

==Cast==
* Luis Trenker as Severin Anderlan 
* Luise Ullrichas Erika Rieder 
* Victor Varconi as Kapitän Leroy, Oberkommandant von St. Vigil 
* Ludwig Stössel as Riederer, Amtshauptmann von St. Vigil 
* Olga Engl as  Severins Mutter 
* Erika Dannhoff as Gerrud Anderlan 
* Fritz Kampers as Offizier Raidthofer 
* Arthur Grosse as General Drouet 
* Amanda Lindner as Frau Drouet 
* Albert Schultes as Jakob Harrasser 
* Reinhold Bernt as Krahvogel 
* Otto Kronburger as Pater Mdardus 
* Emmerich Albert as Bergführer Hagspiel 
* Hans Jamnig as Bergführer Klotz 
* Hugo Lehner as Französischer Soldat 
* Luis Gerold as Bergführer Rabenstein 
* Inge Conradi as Kathrin, Hagspiels Freundin

==References==
;Notes
 
;Bibliography
 
* Prawer, S.S. (2005) Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910–1933. Berghahn Books.
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 