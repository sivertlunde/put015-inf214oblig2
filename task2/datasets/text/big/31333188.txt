La cruz del diablo
{{Infobox film
| name           = La cruz del diablo
| image          =
| alt            =
| caption        =
| director       = John Gilling
| producer       =
| screenplay     = {{plainlist|
* Juan Jose Porto
* Jacinto Molina
}}
| based on       =  
| starring       = {{plainlist|
* Ramiro Oliveros
* Carmen Sevilla
* Adolfo Marsillach
* Eduardo Fajardo
* Emma Cohen
* Mónica Randall
}}
| music          =
| cinematography = Fernando Arribas
| editing        = Alfonso Santacana
| studio         = Bulnes
| distributor    = United International Pictures
| released       =  
| runtime        =
| country        = Spain
| language       = Spanish
| budget         =
| gross          =
}}
La cruz del diablo is a 1975 Spanish horror film directed by John Gilling and starring Carmen Sevilla, Adolfo Marsillach and Emma Cohen.  Its plot concerns a British writer who travels to Spain to visit his sister only to discover she has been killed by a sinister cult. It was based on a short story by Gustavo Adolfo Becquer.

==Cast==
* Carmen Sevilla – Maria
* Adolfo Marsillach – Cesar del Rio
* Emma Cohen – Beatriz
* Ramiro Oliveros – Alfred Dawson
* Eduardo Fajardo – Enrique Carrillo
* Mónica Randall – Justine Carrillo
* Tony Isbert – Iñigo de Ataíde
* Fernando Sancho – Ignacio
* Silvia Vivó – Ines
* Eduardo Calvo – Director Prisión
* Pascual Hernández – Guardia Civil
* Antonio Ramis – Criado

==Reception==
Writing in The Zombie Movie Encyclopedia, academic Peter Dendle called it "a disappointing final film for ex-Hammer Studios director John Gilling".   Glenn Kay, who wrote Zombie Movies: The Ultimate Guide, described it as an out of print film that is considered "uninspired and unimpressive" by those who have seen it. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 