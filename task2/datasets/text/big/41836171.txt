Minugurulu
{{Infobox film
| name           =Minugurulu
| image          =Minugurulu poster.jpg
| director       =AyodhyaKumar
| producer       =AyodhyaKumar Krishnamsetty
| writer         = AyodhyaKumar Krishnamsetty N.V.B. Chowdary Mahesh Kathi
| starring       = Ashish Vidyarthi Suhasini Maniratnam Raghubir Yadav Deepak Saroj
| cinematography = David Edward Fuller Paulius Kontijevas
| editing        = Kiran Ganti
| distributor    = Respect Creations
| music          = Josyabhatla Rajsekhar Sarma
| released       = January 24, 2014 
| runtime        = 109 min
| country        = India Telugu
| budget         = 3 Crores
| box Office     =  _ 
}}
 Telugu social problem film written and directed by Ayodhya Kumar Krishnamsetty.   The film was selected as Best Indian Film at the 9th India International Childrens Film Festival, held at Bangalore.  The film has also garnered the Akkineni Vamsee International Award.  The film was also showcased official selection, Golden Elephant, at the International Childrens Film Festival in Hyderabad, India. 
The film was also showcased at the Rafi Peers 3rd International Film Festival held at Lahore in December 2013.  On 26th December 2013, the film was screened at the Kolkata International Film Festival and International Childrens Film Festival of Kolkata.  The film garnered official selection at the Fifth International Disability Film Festival - Ability Fest held between September 23-26 2013 

== Plot ==
Raju’, a mother-less child, is orphaned by his father, as he is blinded while editing a video that he has made in a local photo studio. He is admitted to a residential blind orphanage run by a depraved man ‘Narayana’, who battens on the money meant for the welfare of the blind and keeps the children in deplorable conditions. As visually impaired boy, he learns to see life as a challenge. In the process of coping with the fresh blindness, Raju finds four friends Mynah, Sukumar, Bhaskar and Anand among the children of the centre. All the children, decide to send a complaint to the District Collector with the help of Raju. Unfortunately the whole effort goes in vain. This humiliation and pain strengthens the resolve of the children to expose Narayana’s delinquency. The mission “Divya-Drushti” starts with Raju’s astounding idea of portraying the brutality of Narayana with all evidences and proofs to the District Collector and makes an identity and creates independence not only to him but also to others.

==References==
 

 
 
 
 
 
 