My Old Kentucky Home (film)
{{Infobox film
| name           = My Old Kentucky Home
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Dave Fleischer
| producer       = Max Fleischer
| writer         = 
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = 
| music          = song "My Old Kentucky Home" by Stephen Foster
| cinematography = 
| editing        =  Out of the Inkwell Studios
| distributor    = Red Seal Pictures
| released       = June 1926 
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Max and Dave Fleischer of Fleischer Studios as one of the Song Car-Tunes series. The series, between May 1924 and September 1926, eventually totaled 36 films, of which 19 were made with sound. This cartoon features the original lyrics of "My Old Kentucky Home" (1853) by Stephen Foster, and was recorded in the Lee DeForest Phonofilm sound-on-film system.
 follow the bouncing ball gimmick in their Song Car-Tune My Bonnie Lies Over the Ocean (released 15 September 1925).
 Paul Terrys Dinner Time (September 1928) and The Walt Disney Company|Disneys Steamboat Willie (November 1928).

A clip from the film is used in the opening credits of the Futurama episode "Why Must I Be a Crustacean in Love?".

==External links==
*  
*  

 
 
 
 
 
 
 
 
 

 