Premaku Velayera
{{Infobox film
| name           = Premaku Velayara
| image          =
| caption        =
| director       = S. V. Krishna Reddy
| producer       = Taranga Subramanyam
| writer         = Diwakar Babu  (Dialogue)  S. V. Krishna Reddy  (story) 
| narrator       =
| starring       = J. D. Chakravarthy Soundarya Prakash Raj Brahmanandam Ravi Teja, Sri Hari Suriya
| music          = S. V. Krishna Reddy
| cinematography = Sarat
| editing        = K.Ramgopal Reddy
| distributor    = Taranga Films
| released       = 6 August 1999
| runtime        = 140 min.
| country        = India Telugu
| budget         = 7 crores
| preceded_by    =
| followed_by    =
| website        =
}}
Premaku Velayara is a musical hit Telugu film, directed by S. V. Krishna Reddy. The film was released in 1999.

==Plot==
Venkata Narayana (Prakash Raj) is one of the biggest business tycoons in India having a personal net worth of Rs 2,500 crores. His motto of life is Dare to dream and care to achieve. His only daughter is Madhavi (Soundarya). She is arriving from  the USA. after doing her MBA. He wanted to make her the MD for his companies. But Madhavi tells him that she wants to join as a clerk in his firm and learn the tricks of trade and slowly take over Venkata Narayana post when she is ready. Venkata Narayana likes her idea and he appoints her as a clerk in his office and she changes her name to Malati.

Manohar (J. D. Chakravarthy) just finished his B.Com. Manohar comes to know that Venkata Narayana is none but his own maternal uncle. Then he chooses the easy way out to become a filth rich millionaire. That is to marry the daughter of Venkata Narayara, Madhavi.

On his way to Hyderabad, Manohar skillfully rescues a pregnant, who happened to be the friend of Madhavi. As expected Manohar nonchalantly leaves the train without making any advances to Madhavi though she attracted him with her charms.

Manohar joins his uncles office as dispatch clerk. Manohar gets surprised by knowing that the beautiful girl he met in train is working at the same office. He meets his college time bum chum Ravi (Ravi Teja) as the canteen owner for the same office. He puts up his stay with Ravi. Obviously Malati is staying in the ground floor of the same building in which these guys are staying.

When Manohar and Ravi overhear the telephonic conversation by Malati to her father, they come to know that Malati is none other than Madhavi. Then the inevitable thing happens and Manohar falls in love with Madhavi. Madhavi confesses to Venkata Narayana that she is in love with Manohar. Then Venkata Narayana tells her that Manohar is her brother-in-law and Manohar is interested in her money than herself. Madhavi refuses to accept the allegation by her father and she tells him that she will prove that Manohar is not after money.

She performs a drama to lead Manohar and Ravi into believing that she is Malati, a poor fishermans daughter. Manohar fails to acknowledge his love for her after he realizes that she is a daughter of a poor man. He confesses to Malati that he is here to marry the daughter of Venkata Narayana, but not Malati. He expresses his love and obsession for money.

Madhavi has two choices now. One is to go back to her father and accept her defeat. Second one is to change Manohar. She decides to do both. She meets Venkata Narayana and tells him that Manohar is a money minded man. She asks him to give her a chance to change Manohar. They fix December 31, 1999 as the deadline.

Meanwhile, Manohar loses his job in Venkata Narayanas company. Inspired by a spirited speech by Malati, he decides to do his own consultancy by giving solutions (ideas) to the CEOs who are facing labour and marketing problems. But he is still interested in money and the charm of Madhavi, whom he thinks he did not find. Manohar in turn upsets the apple cart of Venkata Narayana by making the strategic marketing deals with Venkata Narayanas business rival Rama (Ranganath (actor)|Ranganath). Now, Venkata Narayana decides to come even with Manohar and promises him that he will give his daughter to Manohar and that grand announcement of this news would be done at 31 December night bash.
 Surya is the younger brother of Venkata Narayanas wife (Y. Vijaya). Surya is willing to marry Madhavi. This film crazy and director aspirant Surya follows Venkata Narayana and weaves a hypothetical story around the incidents and concludes that Manohar is the villain of the entire episode.

Its 31 December 1999. Mid night is fast approaching. As he is the hero, Surya kidnaps Manohar and keeps him in his dungeon and goes to attend the mid night bash thrown by Venkata Narayana. Manohar attends the bash by bashing the guys who kidnapped him.  Venkata Narayana announces that he will marry off his daughater, Madhavi to Manohar. But, Manohar says that he will marry Malati not knowing that she is Madhavi. Venkata Narayana announces Malati is Madhavi. Manohar tells her that he wants only her and not her weatlh.

==Cast==
*J. D. Chakravarthy ... Manohar
*Soundarya ... Madhavi/Malati
*Prakash Raj ... Venkata Narayana
*Ravi Teja ... Ravi
*Brahmanandam ... Anandam
*Sri Hari ... Appa Rao Ranganath ... Rama
*Gowtham Raju ... Anandams brother-in-law Surya ... Venkata Narayanas brother-in-law Annapoorna ... Manohars mother
* Y. Vijaya as Venkata Narayanas wife
* Subbaraya Sharma ... Manohars father
* M. S. Narayana ... Narayana
* Babu Mohan ... Drama artist Jenny as Ravis father
* Bandla Ganesh ... Manohars friend
* Narra Venkateswara Rao ... Union leader
* Pavala Syamala ... Drama artist
*Ramya Krishna ... Item number

==Crew==
*Banner: Taranga Films
*Film producer|Producer: Taranga Subramanyam
* 
* 
* 
*Dialogues: Divakar Babu
*Cinematography: Sarat
*Editing: Ram Gopal Reddy
*Theatrical Release Date :6 August 1999

==External links==
* 
* 
 

 

 
 
 