Desperate But Not Serious
 
{{Infobox Film name           = Desperate But Not Serious image          = DesperateButNotSerious.jpg image_size     =  caption        = US DVD cover director       = Bill Fishman producer       = Jim Fishman, Mark McGarry, Jason Villard writer         = Nicole Coady, Halle Eaton, Abbe Wool narrator       =  starring  John Corbett music          =  cinematography =  editing        =  studio         =  distributor    =  released       = 1999 runtime        = 94 minutes country        = USA language       = English budget         =  gross          =  preceded_by    =  followed_by    = 
}}

Desperate But Not Serious is a 1999 movie directed by Bill Fishman.    It was released in the USA under the title Reckless + Wild.

==Plot==
Out-of-towner Lily (Taylor) arrives in Los Angeles to attend a wedding reception with the man of her dreams, Jonathan (Corbett). Aided by party-girl Frances (Brewster), they embark on a night of adventure after the wedding invitation is lost. Their wild romp through the streets of Hollywood in search of the reception, takes them to club after club - including the trendy "Vapor" Room and even into the home of famous actor Darby Tipp. After being thrown out of parties, terrorized by a psycho bartender, and chased by police it seems Lily will never find her man - or will she?

==Cast==
* Christine Taylor as Lili
* Paget Brewster as Frances
* Claudia Schiffer as Gigi
* Judy Greer as Molly John Corbett as Jonathan
* Max Perlich as Todd
* Joseph Lawrence as Darby 
* Toledo Diamond as Himself 
* Wendie Jo Sperber as Landlady
* Brent Bolthouse as Steve
* Stacy Sanches as Shauna John Fleck as Landon Liebowitz
* Zach Tiegan as Justin
* Henry Rollins as Bartender
* Matthew Porretta as Gene
* Sara Melson as Patrice
* Duffy Taylor as Jimmy
* Agnieszka Musiala as Stacy

==Production==
Claudia Schiffer sang all of the songs performed by her character herself. 
 Black & White, Claudia Schiffer had the cartilage in both ears pierced multiple times for her part in this movie. She also had her nose and belly button pierced especially for this movie. Following the end of filming, she removed all of the piercings (including her earlobes) and allowed them to close up again. 

==References==
 

==External links==
* 

 

 
 
 
 
 


 