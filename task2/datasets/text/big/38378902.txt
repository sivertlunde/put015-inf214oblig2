Hard Boiled Sweets
 
 
 
{{Infobox film| name = Hard Boiled Sweets
  | image = 
  | caption =
  | director = David LG Hughes   
  | producer = Lara Greenway Demelza Jones David LG Hughes
  | writer = David LG Hughes
  | starring = Adrian Bower Philip Barantini Elizabeth Berrington
  | music = Tom Morrison 
  | cinematography = Sara Deane 
  | editing = Lloyd George 
  | studio = Fatal Black Films
  | distributor =
  | released = 9 March 2012
  | runtime = 84 minutes 
  | language = English
  | gross = $6,462
  }} British crime Paul Freeman and Ian Hart) that had appeared in Hughes earlier short film A Girl and a Gun from which this, his first feature, was developed.   

==Plot==
London crime boss Jimmy the Gent travels to Southend in Essex to collect some monies owed to him by local gangster Shrewd Eddie. There, various assorted gangsters, corrupt police and petty criminals attempt to steal from Jimmy a case containing £1 million in cash.

==Main cast==
{| class="wikitable"
|- "
! Actor !! Role
|-
| Philip Barantini || Dean
|-
| Elizabeth Berrington || Jackie
|-
| Adrian Bower || Gerry
|-
| Liz May Brice || Jenna
|- Paul Freeman || Shrewd Eddie
|-
| Ty Glaser || Porsche
|-
| Ian Hart || Joyce
|-
| Nathaniel Martello-White || Jermaine
|-
| Danny Sapani || Leroy
|- Peter Wight || Jimmy the Gent
|-
| Scot Williams || Johnny
|-
| René Zagger || Fred
|}

==Critical reception== Time Out also found it clichéd – "more Cockernee crime by numbers" – but also suggested that it had "a fistful of decent throwaway gags and enough plot surprises to just about carry it through the rough patches".  ScreenDaily praised its technical aspects including Anders Bundgaards opening credit sequence and Sara Deanes cinematography, describing it overall as "an intriguing debut". 

==Box office==
The film received a limited release and according to Box Office Mojo took only $6,462 in two weeks in cinemas.  It was released on DVD and Blu-ray Disc on 30 April 2012.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 