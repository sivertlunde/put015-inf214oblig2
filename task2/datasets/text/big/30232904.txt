Lonesome
 
{{Infobox film
| name           = Lonesome
| image          = Lonesome poster.jpg
| image size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Paul Fejös
| producer       = Carl Laemmle Carl Laemmle, Jr. Oskar Schubert-Stevens
| writer         = 
| screenplay     =  
| story          = Mann Page
| based on       =   
| starring       =  Barbara Kent Glenn Tryon Fay Holderness 
| music          =  
| cinematography = Gilbert Warrenton Frank Atkinson
| studio         = 
| distributor    = Universal Pictures
| released       = June 20, 1928 (silent version) Sept 30, 1928 (sound version)
| runtime        = 69 min (silent version) 75 min (sound version)
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}   1928 silent silent film by Hungarian-born American director Paul Fejös. It was produced and distributed by Universal Pictures. In 2010, it was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".  The film was released on Blu-ray Disc and DVD on August 28, 2012 as part of The Criterion Collection. 

==Synopsis==
In New York, Mary is a telephone operator who lives alone and is lonely. Jim is a factory worker who lives alone and is lonely. Each decides to go to the beach (presumably Coney Island) and both are captivated with each other, eventually realizing, moreover, that they really do like each other. Having gone from loneliness to finding love, enjoying the company of each other and having fun, they are – due to exigent circumstances and hundreds of visitors to the beach that day – both separated from the other. Only knowing each others first name, and having only a small photo of each other, Jim and Mary are desperate to find each other. Will these two lonesome individuals who have discovered their love for each other...lose it all that same day?

==Cast==
*Barbara Kent as Mary
*Glenn Tryon as Jim
*Fay Holderness as Overdressed woman Gustav Partos as Romantic gentleman Eddie Phillips as The sport

==Production==
 mono versions. colored with stencils.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 