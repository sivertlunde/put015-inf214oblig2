Hush Hush Baby
{{Infobox film
| name           = Hush Hush Baby
| image          = Film poster Shouf Shouf Habibi.jpg
| caption        = Film poster
| director       = Albert ter Heerdt
| producer       = Albert ter Heerdt
| writer         = Albert ter Heerdt
| narrator       = 
| starring       = Mimoun Oaïssa Salah Eddine Benmoussa
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 2004
| runtime        = 84 minutes
| country        = Netherlands Dutch
| budget         = € 680.000
| preceded_by    = 
| followed_by    = 
}}
 Dutch comedy comedy film, written and directed by Albert ter Heerdt.

==Story==
  Moroccan family that tries to find their way in Dutch society.

Abdullah Ap Bentarek is a young Moroccan man, about 20 years old. He is happy that, unlike his Uncle Yusuf who stayed in the ancestral Moroccan mountain village, his own father, Ali, moved to the Netherlands. His mother knows only one sentence in Dutch, which she utters every time, and in a strong Moroccan accent: "Is goed" ("Its OK"). His father doesnt speak or understand Dutch at all.
 Dutch lifestyle, playing some pool with his lousy friends and lead a life like his sister Leila does--Leila refuses to be married off and takes off her hijab often when attending school--or he can do what his father wants him to do: become serious, find a job and choose a bride in Morocco.

His older brother Sam is a policeman and is very well integrated in Dutch society. He gets Ap an officejob, at a bank he and his friends have dreamt of robbing, but Ap lasts only a day there. He then chooses to join his friends in the attempt to break into the bank. The attempt, however, fails miserably.

Meanwhile, his little brother Driss blackmails their sister Leila and other Muslim girls at his school, by taking pictures of them when they have taken off their hijab in school. His grades are low, but his parents dont know of that. When his father has to come over to discuss his grades, Driss accompanies him as his interpreter. Everything the teacher tells about his grades and his behaviour is not translated by him: instead, he tells his father his teacher thinks he is a brilliant student. The scheme fails when the conversation is overheard by a Moroccan cleaner who intervenes.

When Ap is heading for trouble, he decides to follow his fathers advice and become serious. He sets off for Morocco to find a bride, but if that is the right choice for him is uncertain.

==Reception==
The movie became one of the most successful Dutch comedy productions ever. Reactions from the Moroccan community were mixed: on one side, Moroccan youths claimed the movie, comparing themselves to the main characters; on the other side, the movie was criticised for being stereotypical and giving an overall negative impression of Moroccan immigrant families.

==Cast==
  
*Mimoun Oaïssa - Ap
*Salah Eddine Benmoussa - Ali
*Zohra Flifla Slimani - Khadija
*Najib Amhali - Sam
*Iliass Ojja - Driss
*Tanja Jess - Maja
*Frank Lammers - Chris
*Touriya Haoud - Leila
*Mimoun Ouled Radi - Rachid
*Mohammed Chaara - Mussi
*Leo Alkemade - Robbie
*Winston Gerschtanowitz - Daan
*Tara Elders - Britt
*Bridget Maasland - Carlie

== Production ==
 
Since January 2006, Dutch broadcasting organisation VARA broadcasts Shouf Shouf! de serie.  In this series, most of the main cast is the same as in the film.  The series is directed by Tim Oliehoek.

== References ==
 
 

== External links ==
* 
*  

 
 
 
 
 