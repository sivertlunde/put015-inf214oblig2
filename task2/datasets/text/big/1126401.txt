Van Wilder
 
 
{{Infobox film
| image = National Lampoons Van Wilder Poster.png
| caption = Theatrical release poster
| film name = National Lampoons Van Wilder
| director = Walt Becker Robert L. Levy
| writer = Brent Goldberg David T. Wagner
| starring = Ryan Reynolds Tara Reid Kal Penn Tim Matheson
| music = David Lawrence
| cinematography = James Bagdonas
| editing = Dennis M. Hill
| studio = Myriad Pictures Tapestry Films
| distributor = Lions Gate Films   Artisan Entertainment  
| released =  
| runtime = 92 minutes 94 minutes  
| country = Germany United States
| language = English
| budget = $5 million 
| gross = $38,275,483   
}}

National Lampoons Van Wilder (released internationally as Van Wilder: Party Liaison) is a 2002 comedy film directed by Walt Becker that stars Ryan Reynolds as the title character. The film also stars Kal Penn, Tara Reid, and Daniel Cosgrove. It features Sophia Bushs acting debut. 

==Plot==
Van Wilder (Ryan Reynolds) is an extremely popular student at Coolidge College who has been in school for seven years. For the past six semesters, he has made no effort to graduate, instead spending his time organizing parties and fundraisers, doing charity work, helping other students, and posing for figure drawing classes. When Vans father (Tim Matheson) finds out Van is still in college, however, he decides to stop paying Vans tuition.

Meanwhile, Gwen (Tara Reid), a star reporter for the student newspaper, is asked to do an article on Van. It proves to be so popular that she is asked to write a follow-up article for the front page of the graduation issue. Although that would be a big win, she is rather put off when Van interprets her attempts to schedule interviews as romantic advances&mdash;when she already has a boyfriend, Richard "Dick" Bagg (Daniel Cosgrove). Van and Richard dislike each other immediately, and perform increasingly disgusting pranks on each other. In one infamous scene, Van and his friends replace the cream inside some pastries with semen from Vans pet bulldog, Colossus, and send them to Richards fraternity, where the frat brothers do not realize what they are eating until it is too late.

At one point, Richard has sex with another woman from a sister sorority, which would be the final straw for Gwen later on. Richard also sets up Van at one of his own parties in which there are some (very) underage drinkers and reports it to the police, which leads to Van almost being expelled. However, Van proposes an alternative punishment: that the school force him to complete a semesters worth of work in the six days remaining, and then graduate. The committee agrees to this by a three-to-two decision.
 MCAT and Northwestern Medical School, Gwen laces his ritualistic protein shake with a powerful laxative. Since there are no bathroom breaks allowed during the test, Richard rushes through the 2-hour exam in 20 minutes, "dialing down the middle" while disgusting the other test-takers with frequent flatulence. Afterwards, just as hes about to reach the bathroom, he unexpectedly runs into the medical school interview committee. They pull him aside into an office to begin the interview and Richard ends up defecating explosively into a trash can while in their presence. He is briefly seen again in the film, reading Gwens article in the school paper in the bathroom (presumably, his medical school dreams are ruined, as he mentions to Van in a deleted scene). Meanwhile, Van does well on his finals and celebrates his graduation with a party, where his father and Gwen congratulate him.

A subplot depicts the saga of Taj Mahal Badalandabad (Kal Penn), an Indian foreign exchange student who is accepted (out of many "talented" applicants) to be Vans personal assistant. The main reason for his application for this position, Taj explains, is he wants to have sex with an American girl before he goes home. He meets a girl named Naomi (Ivana Bozilovic), and Van pushes Taj to go for her, but Taj accidentally sets himself on fire with massage oil. At the end of the film, Taj meets an attractive Indian girl (Teesha Lobo), and it seems like they are going to hit it off.

==Cast==
 
* Ryan Reynolds as Van Wilder
* Tara Reid as Gwen Pearson
* Kal Penn as Taj Mahal Badalandabad
* Tim Matheson as Vance Wilder, Sr.
* Paul Gleason as Professor McDougal
* Daniel Cosgrove as Richard "Dick" Bagg
* Teck Holmes as Hutch
* Emily Rutherfurd as Jeannie
* Curtis Armstrong as Campus Cop
* Deon Richmond as "Mini Cochran" Alex Burns as Gordon Chris Owen as Timmy
* Ivana Božilović as Naomi
* Sophia Bush as Sally
* Simon Helberg as Vernon
* Quentin Richardson as Q
* Michael Olowokandi as Leron
* Darius Miles as Darius
* Erik Estrada as Himself
* Edie McClurg as Campus Tour Guide
* Michael Waltman as Coach Ken Massey
* Aaron Paul as Wasted Guy
* Teresa Hill as Hot Female Doctor
* Tom Everett Scott (uncredited) as Elliot Grebb
* Walt Becker (uncredited) as Fireman
* Lamar Odom  ( uncredited) as Coolidge Chickandee Player

==Production==

 

==Soundtrack==
{{Infobox album   Name        = National Lampoons Van Wilder Type        = soundtrack Artist      = Various Artists Cover       = Released    = 26 March 2002 Recorded    = 2000, 2001 Genre       = Soundtrack Length      = 45:42 Label       = Ultimatum Records, Artemis Records Producer    = Gwen Bethel Riley and Chris Violette Reviews     =  Last album  =  This album  = Next album  = 
}}

The soundtrack album was released on March 26, 2002. It omits the song "Hello" by Sugarbomb, "Authority Song" by Jimmy Eat World, and "Stuck In America" by Sugarcult. Other artists with songs omitted from the soundtrack included Atomic Kitten, Michelle Branch, Sprung Monkey, Bird 3, Spymob, Mint Royale, and Tahiti 80.

===Track listing=== Roll On" - The Living End (Chris Cheney) Bleed American" - Jimmy Eat World (Jimmy Eat World)
# "Hit The Ground" - 6gig (6gig)
# "Bouncing Off The Walls" - Sugarcult (Sugarcult)
# "Im a Fool" - American Hi-Fi (Stacy Jones) David Mead
# "Things Are Getting Better" - N.E.R.D.|N*E*R*D (Chad Hugo, Pharrell)
# "Okay" - Swirl 360 (Kenny & Denny Scott, Tonio K)
# "Blind Spot" - Transmatic (Transmatic)
# "Makes No Difference" - Sum 41 (Sum 41) Fuzz Townshend (Fuzz Townshend, Matt Machin, Roger Charlery) Sia (Sia Furler, Sam Frank)
# "Start Over" - Abandoned Pools (Tommy Walter, Pete Pagonis)
# "You Get Me" - Michelle Branch (Michelle Branch)

==Release==

===Reception===
The film was panned by critics. Rotten Tomatoes gives the film a score of 18% based on 88 reviews. The critics describe the movie as being "A derivative gross-out comedy thats short on laughs." 

===Box office===
* Budget - $5 million 
* Marketing cost - $15 million
* Opening Weekend Gross (Domestic) - $7,302,913
* Total Domestic Grosses - $21,305,259
* Total Overseas Grosses - $16,970,224
* Total Worldwide Grosses - $38,275,483 

===Home media===
  trailers and other promotional material like television ads and poster art.

On November 28, 2006, in a way of promoting the sequel to Van Wilder,  , Lions Gate Home Entertainment released a 2-disc special edition DVD with new bonus features including a "Drunken Idiot Audio commentary|Kommentary" (featuring National Lampoon editors Steven Brykman and Mason Brown), behind-the-scenes footage, and interviews with the cast and crew. The re-release did not include the topless menus from the previous set.

The film was also released on Blu-ray Disc|Blu-ray on August 21, 2007 which had almost the same features as the 2-disc special edititon DVD. Also included (and exclusive to the Blu-ray edition) is the "Blu-Book Exam", an interactive game that focuses on Van Wilder trivia questions, plus a series of "Blu-line" options including a pop-up film-progression menu that allows you to set bookmarks and skip around the feature film.

==Legacy==

===Sequel===
 
A  ); the film was released theatrically in 2006.

===Prequel===
  Jonathan Bennett) as he deals with his freshman year of college.

==References==
 

==External links==
*  
*  
*  
*   Questions, answers and other music information.

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 