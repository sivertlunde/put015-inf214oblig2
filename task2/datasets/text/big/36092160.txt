Sex Positive
 
 
{{Infobox film
| name           = Sex Positive
| image          = Sex-positive-film-by-daryl-wein.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Daryl Wein
| producer       = David Oliver Cohen, Daryl Wein (producers), Zoe Lister Jones (associate producer)
| writer         =
| narrator       =
| starring       = Richard Berkowitz
| music          = Michael Tremante
| cinematography = Alex Bergman
| editing        = Daryl Wein
| distributor    =
| released       =  
| runtime        =
| country        = United States English
| budget         =
| gross          =
}}
Sex Positive is a 2008 documentary film directed by Daryl Wein about Richard Berkowitz. The film explores Berkowitz life, presenting him as a revolutionary gay activist whose incomparable contribution to the invention of safe sex has never been aptly credited.

The documentary had footages from Berkowitz, as well as Don Adler, Dotty Berkowitz (his mother), Susan Brown, Demetre Daskalakis, Richard Dworkin, William A. Haseltine, Larry Kramer, Ardele Lister, Michael Lucas, Francisco Roque, Gabriel Rotello, Joseph Sonnabend, Bill Stackhouse, Krishna Stone, Sean Strub and Edmund White.

In 2008, the film won the Grand Jury Award at the Los Angeles Outfest for "Best Documentary Feature". 

== References ==
 
*{{cite web
|url=http://movies.msn.com/movies/movie/sex-positive/
|title=Sex Positive:Overview
|publisher=MSN Entertainment
|accessdate=2012-09-06
}}
*{{cite news
|url=http://movies.nytimes.com/2009/06/12/movies/12posi.html
|title=Sex Positive (2008)
|newspaper=The New York Times
|date=11 June 2009
|last=Holden
|first=Stephen
|accessdate=2012-09-06
}}

==External links==
* 

 
 
 
 
 
 
 
 


 
 