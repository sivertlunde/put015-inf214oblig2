Pelle the Conqueror
 
{{Infobox film
| name = Pelle the Conqueror
| image = Pelle_original.jpg
| alt = 
| caption = Original Swedish poster
| director = Bille August
| producer = Per Holst
| screenplay = Bille August Per Olov Enquist Bjarne Reuter Max Lundgren
| based on =  
| starring = Max von Sydow Pelle Hvenegaard Erik Paaske Björn Granath|Bjørn Granath Stefan Nilsson
| cinematography = Jörgen Persson
| editing = Janus Billeskov Jansen Per Holst Filmproduktion
| distributor = Svensk Filmindustri   Kærne Film  
| released =  
| runtime = 150 minutes  
| country = Denmark Sweden 
| language = Scanian Danish Swedish
| budget = 
| gross = $2,053,931 
}} Swedish immigrants to Denmark, a father and son, who try to build a new life for themselves. It stars Pelle Hvenegaard as the young Pelle, with Max von Sydow as his father.

Critically acclaimed, it won the Palme dOr at the 1988 Cannes Film Festival, the 1988 Golden Globe Award for Best Foreign Language Film, and the 1988 Academy Award for Best Foreign Language Film.

==Plot== Danish writer Danish that discriminated against as a foreigner. But neither boy nor father is willing to give up their dream of finding a better life than that which they left in Sweden.

==Cast==
* Max von Sydow as Lassefar "Lasse" Karlsson
* Pelle Hvenegaard as Pelle Karlsson
* Erik Paaske as Foreman
* Björn Granath as Erik
* Astrid Villaume as Mrs. Kongstrup
* Axel Strøbye as Kongstrup
* Troels Asmussen as Rud
* Kristina Törnqvist as Anna
* Karen Wegener as Mrs. Olsen
* Sofie Gråbøl as Miss Sine
* Lars Simonsen as Niels Køller
* Buster Larsen as Ole Køller
* John Wittig as Schoolteacher
* Troels Munk as Doctor
* Nis Bank-Mikkelsen as Clergyman
* Lena Pia Bernhardsson as The Sow
* Anna Lise Hirsch Bjerrum as Karna
* Thure Lindhardt as Nilen

==Production== Stefan Nilsson.

==Reception==
The film received very positive reviews; it currently holds a 100% rating on Rotten Tomatoes. 

==Accolades==
Pelle the Conqueror won the Academy Award for Best Foreign Language Film, 1988; it was submitted to the Academy by the Danish government, giving Denmark its second consecutive win after Babettes Feast. Max von Sydow was nominated for the Academy Award for Best Actor, but lost to Dustin Hoffman for Rain Man.
 Best Film Best Actor.   

 
==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 