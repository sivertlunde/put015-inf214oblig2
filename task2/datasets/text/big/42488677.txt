The Cabin in the Mountains
{{Infobox film name       = The Cabin in the Mountains image      = The Cabin in the Mountains poster.jpg caption    = The Cabin in the Mountains poster traditional = 林中小屋 simplified = 林中小屋 pinyin     = Línzhōng Xiǎowū }} director   = Gary Tang producer   = Chen Kewen screenplay = Gary Tang
| based on  =  starring   = Gregory Wong Sienna Li Mark Du Ava Yu A.Lin Pei Sukie Wing-Lee Shek Wei Yuhai Gu Shangwei music      = Kang Wai Ho Corrnd Wang cinematography = Liang Baoquan editing    = Guo Xuanyu studio     = Zhujiang Film Group Co.,Ltd Beijing East Light Film Co.,Ltd Mei Ah Entertainment
|distributor= Mei Ah Entertainment released =   runtime = 95 minutes country = China language = Mandarin Cantonese budget   =  gross    = ￥4 million 
}} suspense crime film written and directed by Gary Tang. The film stars Gregory Wong, Sienna Li, Mark Du, Ava Yu, A.Lin Pei, Sukie Wing-Lee Shek, Wei Yuhai, and Gu Shangwei.    It was released in China on Valentines Day. 

==Cast==
* Gregory Wong as Jian Ming.
* Sienna Li as Na Na.
* Mark Du as Zhen Bang.
* Ava Yu as Mei Jin.
* A.Lin Pei as Bai Zhi.
* Sukie Wing-Lee Shek as Hai Di.
* Wei Yuhai as Jia Jie.
* Gu Shangwei as Ren Bao.

==Released==
Mei Ah Entertainment released a trailer for The Cabin in the Mountains in Guangzhou on January 22, 2014. 

The film grossed ￥4 million on its first three days and received positive reviews. 

==References==
 

 
 
 
 
 
 
 

 