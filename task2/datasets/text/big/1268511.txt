Kwaidan (film)
 
 

{{Infobox film
| name           = Kwaidan
| image          = Kwaidanposterjapanese.jpg
| director       = Masaki Kobayashi
| writer         = Yôko Mizuki
| starring       = Rentarō Mikuni Keiko Kishi Michiyo Aratama Misako Watanabe Tatsuya Nakadai
| producer       = Shigeru Wakatsuki
| music          = Toru Takemitsu
| cinematography = Yoshio Miyajima   
| distributor    = Toho Company Ltd.
| released       =  
| runtime        = 183 minutes
| country        = Japan
| language       = Japanese
| based on = stories by Lafcadio Hearn
}} Special Jury Best Foreign Language Film.   

==Plot==
 

===The Black Hair===
  was adapted from "The Reconciliation", which appeared in Hearns collection Shadowings (1900). An impoverished swordsman living in Kyoto divorces his wife, a weaver, for a woman of a wealthy family to attain greater social status. He takes his new wife to his new position as a district governor. However, the swordsmans second marriage proved to be unhappy. His second wife being callous and shallow, the swordsman regrets leaving his more devoted ex-wife.

One night while he sleeps, the woman has found out that the swordsman only married her to obtain her familys wealth and is furious. She slaps him for making her a fool to think that he loved her, when it was really his ex-wife he keeps thinking about. The second wife returns to her marriage chambers in humiliation. When he is told to go into the marriage chambers to reconcile with her by a lady-in-waiting, the swordsman refuses, stating his intent to return home and reconcile with his true wife. He tells her that it is his foolish youth in being impoverish that made him marry his second wife to obtain greater social status and wealth. Admitting that he didnt love her, the swordsman tells his second wifes lady-in-waiting to inform her that their marriage is over and she can return to her family.

After a few years, the swordsman able to return to Kyoto and finds the house in a mess. Nevertheless, he is able to find his way to his ex-wife thanks to a lighting in one room and reconcile with her. The first wife refuses to let him punish himself as he didnt have a choice in the matter. They were impoverished and the wife understands he only divorced her so he can better support her. Before going to bed in their marriage chamber, the swordsman promises her that they wont have to worry about poverty anymore and will never leave her side again. He wakes up the following day and finding that he was sleeping next to a rotted corpse that was once his wife. Realizing this, the swordsman tries to escape only to find himself falling under the floor. His clothes being a mess and hes losing his hair(possibly inferring that the ghost of his devoted ex-wife intends to make the swordsman uphold his promise in not abandoning her again).

===The Woman of the Snow===
  is an adaptation from Hearns   (1903). In the Musashi Providence, a woodcutter named Minokichi takes refuge in a fishermans hut during a snow storm alongside his mentor Mosaku. When Minokichi comes to, he finds Mosaku killed by a Yuki-onna before she sees him. Although she spares Minokichi because of his youth, Yuki-onna warns him to never mention what happened that day or she will kill him. Keeping his word and on his way home from cutting wood, Minokichi later falls meets a young woman named Yuki who resembles the ghost he encountered. She reveals that she is on her way to Edo for she lost her family and that her relatives had got her a position as a lady-in-waiting. Minokichi takes Yuki to his home he shares with his mother. She takes a liking to Yuki and asks her to stay here. Yuki never leaves for Edo and Minokichi fall in love. The two marry and have children together, living happily for eighteen years. The female villagers are in awe of Yukis youth for after having three children, she still looks the same. One night, during a snowstorm, Minokichi tells her that her appearance reminds him of the Yuki-onna he met, telling her of the strange event. It was then that Yuki reveals herself to be the Yuki-onna while telling him that he broke his word, yet refrains from killing him because of their children. Yuki then leaves Minokichi with the children, warning to treat them well or she will return and kill him. She then disappears into the snowstorm, and leaving Minokichi heartbroken.

===Hoichi the Earless===
  is also adapted from Hearns Kwaidan (though it incorporates aspects of The Tale of the Heike that are mentioned, but never translated, in Hearns book).  It depicts the folkloric tale of Hoichi the Earless, a blind musician, or biwa hoshi, whose specialty is singing The Tale of the Heike, about the Battle of Dan-no-ura, fought between the Taira and Minamoto clans during the last phase of the Genpei War. He is subsequently called in to sing for a royal family. His friends and priests grows concerned that he may be singing for ghosts as soon as he answered the call. To protect Hoichi, a priest and his acolyte write the text of The Heart Sutra on his body, and instruct him to go outside and sit still, as if in meditation.

===In a Cup of Tea===
  is adapted from Hearns Kottō: Being Japanese Curios, with Sundry Cobwebs (1902). A writer who is anticipating a visit from the publisher, keeps seeing faces in a cup of tea.

==Cast==
* Michiyo Aratama as First wife (segment "Kurokami")
* Misako Watanabe as Second Wife (segment "Kurokami")
* Rentaro Mikuni as Husband (segment "Kurokami")
* Kenjiro Ishiyama as Father (segment "Kurokami") (as Kenjirô Ishiyama)
* Ranko Akagi as Mother (segment "Kurokami")
* Fumie Kitahara as (segment "Kurokami")
* Kappei Matsumoto as (segment "Kurokami")
* Yoshiko Ieda as (segment "Kurokami")
* Otome Tsukimiya as (segment "Kurokami")
* Kenzo Tanaka as (segment "Kurokami")
* Tatsuya Nakadai as Minokichi (segment "Yuki-Onna")
* Keiko Kishi as the Yuki-Onna (segment "Yuki-Onna")
* Yūko Mochizuki as Minokichis mother (segment "Yuki-Onna")
* Kin Sugai as Village woman (segment "Yuki-Onna")
* Noriko Sengoku as Village woman (segment "Yuki-Onna")
* Katsuo Nakamura as Hoichi (segment "Hoichi the Earless")
* Tetsurō Tamba as Warrior (segment "Hoichi the Earless")
* Takashi Shimura as Head priest (segment "Hoichi the Earless")
* Haruko Sugimura as Madame (segment "Chawan no naka")
* Osamu Takizawa as Author / Narrator (segment "Chawan no naka")
* Ganjirō Nakamura as Publisher (segment "Chawan no naka")
* Noboru Nakaya as Shikibu Heinai (segment "Chawan no naka")
* Seiji Miyaguchi as Old man (segment "Chawan no naka")
* Kei Satō as Ghost samurai (segment "Chawan no naka")

==Production==
 

==Release==
 

==Reception==
In his Harakiri (1962 film)|Harakiri review, Roger Ebert described Kwaidan as "an assembly of ghost stories that is among the most beautiful films Ive seen". 

==Legacy==
 

==See also==
* List of ghost films
* List of submissions to the 38th Academy Awards for Best Foreign Language Film
* List of Japanese submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  
*  
;Text of Lafcadio Hearn stories that were adapted for Kwaidan
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 