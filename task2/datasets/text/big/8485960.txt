A Different Loyalty
{{Infobox Film
| name           = A Different Loyalty
| image          = A_Different_Loyalty_DVD.jpg
| caption        = 
| director       = Marek Kanievska
| producer       = Michael Cowan Richard Lalonde Jason Piette Jan H. Vocke
| writer         = Jim Piddock
| starring       = Sharon Stone Rupert Everett Julian Wadham
| music          = Normand Corbeil
| cinematography = Jean Lépine
| editing        = Yvann Thibaudeau
| distributor    = Lions Gate Entertainment
| released       =  
| country        = Canada United Kingdom United States
| language       = English
| runtime        = 96 minutes
}}
A Different Loyalty is a 2004 drama film inspired by the story of British traitor Kim Philbys love affair and marriage to Eleanor Brewer in Beirut and his eventual defection to the Soviet Union.  The story takes place in the 1960s and stars Sharon Stone and Rupert Everett.  In the film, the characters have fictitious names.  The film was entered into the 26th Moscow International Film Festival.   

Though not credited, the story is based on Eleanor Brewer Philbys 1967 book Kim Philby: The Spy I Loved, published in 1967. The screenplay was written by Jim Piddock. It was a Canada/UK/United States co-production. A Different Loyalty was not released theatrically in the United States.

==Cast==
* Sharon Stone as Sally Tyler Cauffield
* Rupert Everett as Leo Cauffield
* Julian Wadham as Andrew Darcy
* Michael Cochrane as Dick Madsen
* Anne Lambton as Cynthia Cauffield (as Ann Lambton)
* Jim Piddock as George Quennell
* Richard McMillan as Angus Petherbridge
* Mimi Kuzyk as Leslie Quennell
* Emily VanCamp as Jen Tyler
* Tamara Hope as Lucy Cauffield
* Mark Rendall as Oliver Cauffield

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 