A Cuckoo in the Nest (film)
A same title. 

==Synopsis==
Peter and Barbara Wyckham plan to travel by railway from London to a country house in Somerset, but Peter misses the train. Another intending traveller in a similar plight is Marguerite Hickett, an old friend of Peters from the days before their marriages. They decide to hire a motor car and drive to Somerset, but the car breaks down and they seek refuge at the local inn. Only one bedroom is available, and as it is very clear that the landlady, Mrs Spoker, will not admit an unmarried couple, Peter and Marguerite check in as husband and wife.

Barbara jumps to the conclusion that Peter and Marguerite have run away together. First her parents, Major and Mrs Bone, and then Marguerites husband and finally Barbara descend on the inn. It becomes clear to everyone that Peter and Marguerite are blameless, and both couples are reconciled.

==Cast==
*Major Bone – Tom Walls*
*Peter Wyckham – Ralph Lynn*
*Marguerite Hickett – Yvonne Arnaud*
*Mrs Spoker – Mary Brough*
*The Rev. Cathcart Sloley-Jones – Robertson Hare*
*Noony  – Gordon James*
*Barbara Wyckham – Veronica Rose
*Mrs Bone  – Grace Edwin* Mark Daly
*Claude Hickett  – Cecil Parker
*Alfred  – Roger Livesey*
*Gladys  – Norah Howard
*Landlord  – Frank Pettingell
*Kate – Joan Brierley
::Source :  British Film Institute  , British Film Institute, accessed 14 February 2013 
Cast members marked * were the creators of the roles in the original stage production. 

==Notes==
 

 

 
 
 
 
 
 
 
 
 
 


 