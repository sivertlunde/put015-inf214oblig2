The Chinese Botanist's Daughters
{{Infobox Film
| name           = The Chinese Botanists Daughters
| image          = Affiche 02 ok2.jpg
| image_size     = 
| caption        = French-language film poster
| director       = Dai Sijie
| producer       = Lise Fayolles
| writer         = Dai Sijie Nadine Perront
| starring       = Mylène Jampanoï Li Xiaoran
| music          = Eric Lévi
| cinematography = Guy Dufaux
| editing        = Dominique Fortin
| distributor    = EuropaCorp
| released       =  
| runtime        = 105 minutes
| language       = Mandarin
| budget         = 
| country        = France/Canada
}}
Les filles du botaniste (Chinese: 植物园, Botanic Garden) is a French and Canadian film, with the background set as in China. It was released in 2006. Its English title is The Chinese Botanists Daughters.

==Plot==
Set in China in the 1980s or 1990s, the film tells the story of Li Ming, a young orphan of the Tangshan earthquake, who leaves to study at the home of a renowned botanist. A secretive man and commanding father, he lives on an island that he has transformed into a luxurious garden. Anxious to share this solitary life, his daughter, An, welcomes with joy the arrival of the female student. Soon their friendship develops into a sensual, but forbidden attraction. Incapable of separating themselves, Ming and An create a dangerous arrangement to be able to continue spending their lives together: Ming marries Ans brother, who is a Peoples Liberation Army, PLA soldier and cannot bring his wife with him. However, An and Mings relationship is discovered by the botanist who has a heart attack when he finds out. Before he dies, he tells police that it was his daughter and daughter-in-laws homosexuality "disease" that killed him. Thus, An and Ming are sentenced to death by a court and executed.

==Casting==
* Mylène Jampanoï &mdash; Li Ming
* Li Xiaoran &mdash; An
* Dongfu Lin &mdash; Botanist

==Production details==
*Year: 2005
*Date of release: 2006
*Languages: Chinese
*Filmed in Vietnam
*Company: Europa

==Controversies==
Due to the sensitivity of the topic of homosexuality, this movie was not allowed to be shot in China. It was shot in northern Vietnam instead (mainly in Ba Vì and Hà Tây) to create a similar environment.

==Awards and nominations==
Montréal World Film Festival:
*Best Artistic Contribution (Guy Dufaux, won)
*Peoples Choice Award (Sijie Dai, won)
*Grand Prix des Amériques (Sijie Dai, nominated)

Toronto Inside Out Lesbian and Gay Film and Video Festival:
*Best Canadian Film or Video (Sijie Dai, won)

==References==
 

==External links==
* 
*  Japanese
* 
* 
*   at ELMS
*  
*  in Tokyo Wrestling

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 