Bear Ye One Another's Burden
{{Infobox film
| name           = Bear Ye One Anothers Burden
| image          = 
| image size     = 
| caption        = 
| director       = Lothar Warneke
| producer       = 
| writer         = Wolfgang Held Lothar Warneke
| starring       = Jörg Pose Manfred Möck
| music          = 
| cinematography = Peter Ziesche
| editing        = Erika Lehmphul
| released       =  
| runtime        = 115 minutes
| country        = East Germany
| language       = German
| budget         = 
}}

Bear Ye One Anothers Burden ( ) is a 1988 East German drama film directed by Lothar Warneke. It was entered into the 38th Berlin International Film Festival, where Manfred Möck and Jörg Pose won the Silver Bear for Best Actor.   

==Cast==
* Jörg Pose as Josef Heiliger
* Manfred Möck as Hubertus Koschenz
* Karin Gregorek as Oberschwester Walburga
* Heinz-Dieter Knaup as Dr. Stülpmann
* Susanne Lüning as Sonja Kubanek
* Johanna Clas as Frau Grottenbast
* Doris Thalmer as Schwester Inka
* Hermann Stövesand as Sibius
* Peter Hölzel as Herr Trufelknecht
* Gert Gütschow as Dr. Sabrocki
* Monika Lennartz as Heiligers Mutter
* Hans Jochen Röhrig as Jochen
* Ute Lubosch as Sittichs Freundin
* Wilfried Pucher as Sittich

==References==
 

==External links==
* 

 
 
 
 
 
 
 