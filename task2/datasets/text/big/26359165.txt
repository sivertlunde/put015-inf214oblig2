Indian Summer (1973 film)
{{Infobox film
| name = Сиромашко Лято ( )
| image =
| imagesize =
| caption = the original poster
| director = Milen Nikolov
| writer = Mormarevi Brothers
| starring = Georgi Partsalev Tatyana Lolova Ivan Kondov Itzhak Fintzi Leda Taseva
| music = Boris Karadimchev
| cinematography = Rumen Georgiev
| studio = Studio of Featured Films (SFF)|SFF, a Film Unite Mladost
| released = 1973
| country = Bulgaria
| runtime = 79 min. Bulgarian
}}

Indian Summer (  / Siromashko Lyato) is a 1973 Bulgarian comedy-drama film directed by Milen Nikolov and written by Mormarevi Brothers. The film stars Georgi Partsalev, Tatyana Lolova, Ivan Kondov, Itzhak Fintzi and Leda Taseva.

==Cast==

{| class="wikitable"
|-
! Character
! Actor
! Role
|-
| Metodi Rashkov
| Georgi Partsalev
| the grandfather just retired on a pension
|-
| Neychev
| Ivan Kondov
| Rashkovs friend, retired too
|-
| Toteva
| Tatyana Lolova
| a social figure
|-
| Tsvetarski
| Itzhak Fintzi
| Totevas suitor
|-
| Radka
| Leda Taseva
| Rashkovs neighbour, retired too
|-
|
| Milen Penev
| Rashkovs son
|-
|
| Emilia Dragostinova
| Rashkovs daughter-in-law
|-
| Tedi
| Kircho Petrov
| Rashkovs grandson
|-
| Dobrev
| Evstati Stratev
| police inspector
|}

==Response==
A reported 947,814 admissions were recorded for the film in cinemas throughout Bulgaria. 

The film was subsumed among the 50 golden Bulgarian films in the book by the journalist Pencho Kovachev. The book was published in 2008 by "Zahariy Stoyanov" publishing house.

==Notes==
 
==References==
*   
*Pencho Kovachev, 50 Golden Bulgarian Films, Zahariy Stoyanov 2008

==External links==
*  

 
 
 
 