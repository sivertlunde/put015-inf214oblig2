The Lorax (film)
 
 
{{Infobox film
| name = Dr. Seuss The Lorax
| image = Lorax teaser poster.jpg
| image_size =
| alt =
| caption = Theatrical release poster  Chris Renaud Kyle Balda  
| producer = Chris Meledandri Janet Healy Cinco Paul Ken Daurio
| based on =  
| starring = Danny DeVito Ed Helms Zac Efron Taylor Swift Betty White Rob Riggle Jenny Slate John Powell 
| editing = Claire Dodgson Steven Liu Ken Schretzmann
| studio = Illumination Entertainment Universal Pictures
| released =  
| runtime = 86 minutes  
| country = United States
| language = English
| budget = $70 million 
| gross = $348,840,316 
}} 3D Musical musical fantasy fantasy comedy of the Universal Pictures on March 2, 2012, the 108th birthday of Dr. Seuss.
 adaptation of 1972 animated musical television special. It builds on the book by expanding the story of Ted, the previously unnamed boy who visits the Once-ler. The cast includes  Danny DeVito as the Lorax,  Ed Helms as the Once-ler and Zac Efron as Ted. New characters introduced in the film are Audrey, who is voiced by Taylor Swift, Aloysius OHare, voiced by Rob Riggle, and Grammy Norma, voiced by Betty White. The film was a  box office success, although it received mixed reviews.

==Plot==
Ted Wiggins is an idealistic 12-year-old boy, who lives in "Thneedville," a walled city that, aside from the human citizens, is completely artificial; everything is made of plastic, metal, or synthetics. Ted has a crush on local environmentalist Audrey, who wants to see a "real tree" more than anything in the world, and decides to find one in order to impress her. His energetic Grammy Norma secretly tells Ted the legend of the Once-ler, who will tell anyone about trees if brought fifteen cents, a nail, and a shell of a great-great-great grandfather snail. When Ted leaves Thneedville in search of the Once-ler, he discovers that the outside world is a contaminated, empty wasteland. Once the boy finds him, the Once-ler agrees to tell Ted about the trees on the condition that he listens to the story over multiple visits. Ted agrees, but on his way home, he encounters the mayor of Thneedville, Aloysius OHare, who is also the proprietor of a company that sells bottled oxygen to Thneedville residents. OHare explains to Ted that trees produce oxygen free of charge, and for that reason, he considers it a threat to his business whenever he hears people talking about them.  After revealing that he has "security camera eyes" all over the city, OHare pressures Ted to stay in town.  However, Ted continues to sneak out of OHares sight (with his grandmothers encouragement) and learns more of the trees history.

Over Teds various visits, the Once-ler recounts the story of how he departed his family to make his fortune. After stumbling upon a lush Truffula Tree forest, the Once-ler meets the guardian of the forest, the Lorax, after cutting down a Truffula Tree. The Lorax urges Once-ler to leave the forest, but Once-ler refuses. Eventually, the Once-ler promises not to chop another tree down, and the two seem to begin a friendship of sorts. Then, the young businessmans Thneed invention becomes a major success and Once-lers family arrives to participate in the business. At first keeping his promise, the Once-ler continues Thneed production by harvesting the Truffula Tree tufts in a slow, but sustainable manner. However, soon his greedy and lazy relatives convince him to resume chopping down the trees. Over time, the Once-lers deforestation spirals into a mass overproduction. Flushed with wealth, the Once-ler rationalizes his short-sighted needs into arrogant self-righteousness, and the Loraxs helpless protests do not stop him. The Once-ler pollutes the sky, river, and landscape, until the last Truffula Tree falls. With no further chance of business, he is left broken and abandoned by his family, and with the region uninhabitable because of his businesss pollution, The Lorax sends the animals off to find a new place to live before he departs into the sky, leaving only a stone-cut word: "Unless". Distraught and ruined, the Once-ler became a recluse.

After he finishes telling his story, the Once-ler finally understands the meaning behind the Loraxs last message, and gives Ted the last Truffula seed in hopes that he can plant it and make others care about real trees once more. Teds desire to impress Audrey is now a personal mission to remind his town of the importance of nature. OHare, still determined not to have trees takeover his business, takes heavy-handed steps such as covering Audreys nature paintings, closing off the door that Ted uses to see the Once-ler, and forcibly searching Teds room for the seed. Ted enlists his family and Audrey to help plant the seed, which has begun to germinate after coming into contact with water. OHare and his employees pursue the dissidents until they manage to elude him and reach the town center. When Ted finally attempts to plant the seed, he is interrupted by OHare, who rallies the population to stop them by telling the people that trees are dangerous and filth. To convince them otherwise, Ted takes an earthmover and rams down a section of the city wall to reveal the environmental destruction outside, thereby showing them what OHare is encouraging. Horrified at the sight and inspired by Teds conviction, the crowd defies OHare, with his own henchmen expelling him from the town. The seed is finally planted, and Audrey kisses Ted on the cheek. Time passes and the land starts to recover; new trees sprout, the animals begin to return, and the redeemed Once-ler is happily reunited with the Lorax.

===The Once-ler===
Both the book and TV special never reveal the Once-lers face, but instead throughout the book, the Once-ler is pictured by what appear to be green arms and yellow eyes. But the line "...his secret strange hole in his gruvvulous glove," suggests that the Once-ler may not actually have hands or arms of this color.   The filmmakers used that as the basis for the Once-lers character design. They interpreted the Once-ler as a human, and also featured his green gloves and showed his face for the first time in this film.

==Voice cast==
* Danny DeVito as the Lorax.
* Ed Helms as the Once-ler.
* Zac Efron as Ted Wiggins.  He is named after the author of the book, Dr. Seuss (Theodor Geisel).   
* Taylor Swift as Audrey, Teds love interest.  She is named after Audrey Geisel, Dr. Seuss wife. 
* Betty White as Grammy Norma, Teds grandmother   
* Rob Riggle as Aloysius OHare, the mayor of Thneedville and head of the "OHare Air" company that supplies fresh air to Thneedville residents.
* Jenny Slate as Mrs. Wiggins, Teds mother 
* Nasim Pedrad as the Once-lers mother.
* Stephen Tobolowsky as Uncle Ubb, the Once-lers uncle.
* Elmarie Wendel as Aunt Grizelda, the Once-lers aunt.
* Danny Cooksey as Brett and Chet, the Once-lers brothers. Jim Ward.

==Production== Horton Hears Horton Hears Chris Renaud and Kyle Balda were announced as the director and co-director of the film, while Cinco Paul and Ken Daurio, the duo who wrote the script for Horton Hears a Who!, were set to write the screenplay.  In 2010, it was announced that Danny DeVito would be voicing the titular character. 

The film was fully produced at the French studio "Illumination Mac Guff", which was the animation department of Mac Guff which was acquired by Illumination Entertainment in the summer of 2011.  DeVito reprised his role in five different languages, including the original English audio, and also for the Spanish language|Spanish, Italian language|Italian, German language|German, and Russian language dub editions.  Universal added an environmental message to the films website after a fourth-grade class in Brookline, Massachusetts launched a successful petition through Change.org. 

==Release==
The film was released on March 2, 2012 in the United States and Canada and was released on July 27, 2012 in the United Kingdom.

===Marketing controversy===
Mazda used the likeness of The Lorax s setting and characters in an advertisement for their Mazda CX-5|CX-5 SUV.  This was seen by some as the complete opposite of the works original meaning.  In response, Stephanie Sperber, president of Universal partnerships and licensing, said Universal chose to partner with the Mazda CX-5 because it is "a really good choice for consumers to make who may not have the luxury or the money to buy electric or buy hybrid. Its a way to take the better environmental choice to everyone." 
 Seventh Generation disposable diapers.  In total, Illumination Entertainment struck more than 70 different product integration deals for the film. 

===Home media===
The film was released on DVD and Blu-ray Disc|Blu-ray on August 7, 2012. The Blu-ray/DVD Combo Pack also includes three new short films based on the main feature: Serenade, Wagon Ho!, and Forces of Nature.   

====Mini-movies====
Three mini-movies were released on the Lorax Blu-ray/DVD Combo Pack on August 7, 2012: Serenade, Wagon Ho!, and Forces of Nature. 

;Serenade
Lou wants to impress a girl Barboloot, but he has some competition.

;Wagon Ho!
The Once-ler arrives with his wagon and Melvin. Then Pipsqueak and Lou arrive. The Once-ler tells them not to have Joyride (crime)|joyrides. When the Once-ler goes inside his house, Pipsqueak and Lou start cranking up the wagon, but Melvin refuses to pull it because he is angry that they whipped the reins. So Pipsqueak hangs a truffula fruit in front of Melvin as an incentive. However, Melvin gets exhausted going up a steep hill. Then the wagon gets unhooked from Melvin, and it rolls downhill backwards while Pipsqueak and Lou scream in terror. Melvin chases the runaway wagon. Lou tries to stop the wagon by stabbing a stick in one of its wheels, but he gets stuck on the wheel instead. Finally, he lands back on the seat next to Pipsqueak, who hugs him happily. The wagon hits a rock and is propelled downhill even faster. When it approaches a cliff, Pipsqueak panics and grabs the wagon canopy, which flies off—only to be held in place by Lou. The wagon flies off the cliff, but the canopy acts as a parachute and they make their descent calmly, landing in the same place they left. Melvin appears shortly after. The Once-ler comes out of the house and is surprised to see them. He expresses surprise that they listened to instructions, but just when it seems like they fooled him, Lou sneezes and the wagon falls apart. Melvin hides beneath his hooves and Lou and Pipsqueak curl up into a ball. The Once-ler looks annoyed and makes a small, disgruntled "Grr" sound, causing Lou and Pipsqueak to run away into the trees.

;Forces of Nature
The Lorax makes Pipsqueak an Honorary Lorax and team up to scare the Once-ler by using two sticks that looks like a monsters hand to freak him out. The Once-ler finds out its fake and sprays water on the Lorax, which makes his fur puff-up. When the Lorax tells Pipsqueak that hes going to turn out like him, Pipsqueak gets scared and runs away. The short ends with the Lorax telling Pipsqueak that he lost his Honorary Lorax.

===Video game=== Apple iOS Android platforms. 

==Reception==

===Critical response===
 
The film received mixed reviews from critics, with criticism directed towards the film and its marketing as betraying the original message of the book. The film earned a "rotten" rating of 54% on Rotten Tomatoes based on 146 reviews and an average rating of 5.9/10, with the critical consensus saying, "Dr. Seuss The Lorax is cute and funny enough, but the moral simplicity of the book gets lost with the zany Hollywood production values."  It also has a score of 46 on Metacritic based on 30 reviews, indicating "mixed or average reviews". 
 New York magazine film critic David Edelstein on NPRs All Things Considered strongly objected to the movie, arguing that the Hollywood animation and writing formulas washed out the spirit of the book.  "This kind of studio 3-D feature animation is all wrong for the material," he wrote. Demonstrating the poor way the books text was used in the movie—how modern cultural styles were pasted over the text—in this excerpt from the review, Edelstein shows Audrey describing the truffula trees to Ted:

:"the touch of their tufts was much softer than silk and they had the sweet smell of fresh butterfly milk" -- and   Ted says, "Wow, what does that even mean?" and Audrey says, "I know, right?" So one of the only lines that is from the book, that does have Dr. Seuss sublime whimsy, is basically made fun of, or at least, dragged down to Earth."

Lou Dobbs, the host of Lou Dobbs Tonight on the Fox Business Network, has criticized the film as being "insidious nonsense from Hollywood," and accused "Hollywood of trying to indoctrinate children." 

The film also garnered some positive reviews, from critics such as Richard Roeper who called it a "solid piece of family entertainment".     Roger Moore of the Pittsburgh Tribune called the film "a feast of bright, Seuss colors, and wonderful Seuss design", and supported its environmentalist message.   

===Box office===
The film has grossed $214,030,500 in North America, and $134,809,816 in other countries, for a worldwide total of $348,840,316.   
 Project X John Carter (second place).  On April 11, 2012, it became the first animated film in nearly a year to gross more than $200 million in North America, since Walt Disney Animation Studios|Disneys Tangled.  

==Music==
{{Infobox album  
|Name=Dr. Seuss The Lorax: Original Motion Picture Score
|Type=Film score John Powell
|Cover=
|Released=March 6, 2012
|Recorded= Score
|Length=45:51
|Label=Varèse Sarabande
|Producer=
|Chronology= Last album= This album= Next album=
}}
{{tracklist all_writing = John Powell 
|title1=Ted, Audrey and the Trees
|length1=2:36
|title2=Granny to the Edge
|length2=2:33
|title3=Wasteland
|length3=2:17
|title4=Truffula Valley Fantasy (featuring The Lorax Humming Fish)
|length4=5:00
|title5=Once-ler & Lorax Meet
|length5=2:35
|title6=O’Hare Warns Ted
|length6=3:21
|title7=The River Bed
|length7=4:03
|title8=Houseguests
|length8=3:12
|title9=Valley Exodus
|length9=4:54
|title10=The Last Seed
|length10=4:54
|title11=Thneedville Chase
|length11=5:04
|title12=At the Park
|length12=3:12
|title13=Funeral For a Tree
|length13=2:10
|total_length=45:51
}}

 
{{Infobox album  
|Name=Dr. Seuss The Lorax: Original Songs from the Motion Picture
|Type=film John Powell and Cinco Paul
|Cover=
|Released=February 28, 2012
|Recorded= Soundtrack
|Length=34:00
|Label=Interscope Records
|Producer=
|Chronology= Last album= This album= Next album=
}}
{{tracklist John Powell and Cinco Paul 
|music_credits=yes
|title1=Let It Grow (Celebrate the World)
|music1=Ester Dean
|length1=3:39
|title2=Thneedville Beth Anderson, Oliver Powell, Edie Lehmann Boddicker, Missi Hale, and Rob Riggle
|length2=2:44
|title3=This is the Place
|music3=Ed Helms
|length3=2:24
|title4=Everybody Needs a Thneed
|music4=Ed Helms, Randy Crenshaw, Fletcher Sheridan, Edie Lehmann Boddicker, Monique Donnelly, Ty Taylor, and The 88
|length4=1:31
|title5=How Bad Can I Be?
|music5=Ed Helms and Kool Kojak
|length5=2:52
|title6=Let It Grow
|music6=Fletcher Sheridan, Dan Navarro, Edie Lehmann Boddicker, Jenny Slate, Claira Titman, Betty White, Rob Riggle, and Ed Helms
|length6=3:17
|title7=Let It Grow Gospel Ending (Original Demo)
|music7=Jenny Slate
|length7=0:52
|title8=Thneedville (Original Demo)
|music8=Fletcher Sheridan
|length8=3:58
|title9=The Once-lers Traveling Madness (Original Demo)
|music9=Ed Helms
|length9=1:35
|title10=I Love Nature (Original Demo)
|music10=Randy Crenshaw
|length10=2:43
|title11=You Need a Thneed (Original Demo)
|music11=Keith Slettedahl and The 88 featuring Antonio Sol, Fletcher Sheridan, and Taylor Graves
|length11=1:32
|title12=Nobody Needs a Thneed (Original Demo)
|music12=Fletcher Sheridan and Randy Crenshaw
|length12=1:52
|title13=Biggering (Original Demo)
|music13=Gabriel Mann, Randy Crenshaw, and The 88
|length13=5:01
|total_length=34:00
}}

==See also==
* List of films based on Dr. Seuss books

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 