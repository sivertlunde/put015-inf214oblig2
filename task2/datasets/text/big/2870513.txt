Dungeons & Dragons: Wrath of the Dragon God
{{Infobox film
| name           = Dungeons & Dragons: Wrath of the Dragon God
| image          = D&d2.jpg
| caption        = DVD cover for the film
| director       = Gerry Lively
| producer       =
| writer         = Robert Kimmel Brian Rudnick
| starring       = Mark Dymond Clemency Burton-Hill|Clemency&nbsp;Burton-Hill Bruce Payne Ellie Chidzey
| music          = David Julyan
| cinematography = Igor Meglic
| editing        = Rodney Holland Sci Fi Pictures original films Warner Home Video (DVD)
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English USD
}}
 Dungeons & Dragons, which in turn was based on the popular role-playing game Dungeons & Dragons (or D&D). The only returning actor is Bruce Payne reprising his role as the evil Damodar.  The film was released in theaters in Europe and some parts of North America, and released on DVD on February 7, 2006.

Dungeons & Dragons: Wrath of the Dragon God is followed by the third film of the series,  , shot in 2011  and Direct-to-video|direct-to-DVD released in the United Kingdom on August 9, 2012.

==Story==
 
Approximately one hundred years after the events of the first film, Damodar (Bruce Payne) has survived his defeat by Ridley Freeborn, having been cursed by his former master, Profion, to walk the earth as an undead entity.  Driven insane by the pain of the curse, he seeks revenge against the kingdom of Izmir, and the descendants of those who defeated him.
 artifact linked to the power of Faluzure, a dragon god imprisoned under Saragassos mountains.  With the Orbs power, he heals himself of the curse, and prepares to awaken the dragon to destroy Izmir.
 fighter and former captain of the kings guard, now a bored and unsatisfied lord of the King, and Melora (Clemency Burton-Hill), his wife, a "gifted" young Wizard (Dungeons & Dragons)|mage, investigate reports of poison gas emanating from Saragassos caves and find the still-slumbering dragon.  Researching the threat in Izmirs library, Melora excitedly reports to Oberon (Roy Marsden), the head of the Mages Council, that Faluzure was imprisoned three thousand years ago by a powerful ancient civilization called the Turanians, who also created the Orb.  While trying to locate the Orb through magic, Melora is inadvertently cursed by the much-more powerful Damodar, and begins dying slowly.
 Cleric of master thief. Together they resolve to locate the vault of the warlock Malek, a worshiper of the demon Juiblex who was gifted a magical scrying pool known as the Pool of Sight; Berek believes the pool will allow them to penetrate Damodars defenses and reveal the location of the orb.

The party sets out to locate Maleks Vault, while Oberon and the other mages try to decipher the tomes of Turanian magic in their library, to find a way to defeat the dragon.

While traveling through a haunted forest, Bereks party catches the attention of the powerful lich Klaxx the Maligned, who offers his services to Damodar. Damodar does not trust him, but is confident that the Orb makes him more powerful than Klaxx.

After making through several obstacles and riddles, losing Dorian in the process, Bereks party finds its way to Damodars castle.  Confronting him, Berek manages to take advantage of his overconfidence and steal the Orb, though Ormaline and Nim are badly wounded before the wizard teleports them to the Temple of Obad-Hai. While Ormaline and Nim are treated by the clerics, Berek rides back to Izmir, Lux staying behind to delay demons summoned by Damodar.

Using his shape-changing abilities, Klaxx infiltrates Izmirs castle, kills Oberon in his bath, and assumes his shape.  When Berek returns with the Orb, Melora uses it to unlock a vault discovered beneath the castle, where the Turanians hid the secrets of their magic.  However, at that moment, Klaxx reveals himself, stealing the Orb back and killing the King, the new captain of the guard, and many of the castles inhabitants, before returning the Orb to Damodar. Falazure awakens and destroys the Orb, regaining his godly power.

While Berek rides in pursuit, Melora, who is near death, manages to decipher the Turanians secret and rally the remaining mages in a magical attack that defeats the dragon.

Berek and Lux meet up and confront Damodar, who no longer has the Orbs power at his disposal. They force him to cancel Meloras curse. Klaxx, who has no interest in helping Damodar any further, disappears with a laugh.

In the aftermath, Izmir is rebuilt, with Berek immersed in his ministerial duties, and Melora appointed as the new head of the Council of Mages.  Lux, Ormaline and Nim are shown to have fully recovered from their wounds.

Damodar is last seen being imprisoned in a dark dungeon beneath Izmir, but smiling to himself as if he is fully prepared to wait another hundred years to have his revenge.

==Influences from the D&D game== vorpal sword, spells cast by wizards are accurate renditions of the ones from the roleplaying game, namely that spells are not cast at will, but must first be prepared and in limited amounts.
 white dragon, drow hanging from the ceiling and whose blood was used for Damodars "dinner". Juiblex was also mentioned, but the demon lord did not actually appear in the film.

There are also a number of references to classic List of Dungeons & Dragons modules|D&D modules (The Ghost Tower of Inverness, Expedition to the Barrier Peaks, etc.) in the film.
 iconic characters from the third edition D&D ruleset) quipping upon the action on the screen.

From quick glimpses in the DVDs Interview with Gary Gygax, the heroes are shown to have the following stats in the D&D game:

{| class="wikitable" style="text-align:center"
|-
| Berek || Lawful Good male human fighter 7
|-
| Lux || Chaotic Good female human barbarian 7
|-
| Nim || Chaotic Good male human rogue 7
|-
| Dorian || Neutral male human cleric 7 of Obad-Hai
|-
| Ormaline || Neutral female elf wizard 9
|-
| Melora || Neutral Good female human cleric 1 of Obad-Hai/wizard 4
|}

There is also a reference to RuneQuest, another roleplaying game: the runes on the Libram and on the Turanian sealed portal are those that illustrate the RuneQuest books.

==Reception==
IGN scored it 3 out of 10,  stating that only hardcore D&D fans should check it out, though mostly due to references to the game itself. Monsters and Critics awarded it 2 out of 5, stating, "If Lord of the Rings showed us how the fantasy genre can be done right, Dungeons and Dragons - Wrath of the Dragon God shows us how it can be done horribly wrong."  One reviewer stated that Bruce Paynes performance is still the highlight of this one.  Another reviewer stated that Bruce Payne steals the show. 

==Sequel==

A sequel,  , was announced in 2011,  and was released Direct-to-video|Direct-to-DVD in the United Kingdom on August 29, 2012.

== References ==
 

== External links ==
* 
* 
* 

 

 

 
 
 
 
 
 
 
 
 
 
 
 