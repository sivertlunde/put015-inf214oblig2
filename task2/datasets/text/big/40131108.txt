Not My Life
 
 
 
{{Infobox film
| name           = Not My Life
| image          = Not My Life poster.jpg
| image_size     =
| border         =
| alt            = The words "NOT MY  LIFE" in light yellow superimposed on a darker yellow background with the face of a child peering over the words
| caption        =
| film name      =
| director       = {{plainlist|
* Robert Bilheimer Richard Young}}
| producer       = {{plainlist|
* Robert Bilheimer
* Heidi Ostertag
* Jim Greenbaum
* Amy Detweiler
* Devon Higby
* Johan Weijers}}
| writer         = Robert Bilheimer
| screenplay     =
| story          =
| based on       =  
| narrator       = Glenn Close
| starring       =
| music          = {{plainlist|
* Chris Brubeck
* Dave Brubeck
* Susan Tedeschi
* Derek Trucks}}
| cinematography = {{plainlist|
* Richard Young
* Craig Braden
* Gerardo Puglia}}
| editing        = {{plainlist|
* Anthony DeLuca
* JD Marlow}}
| studio         =
| distributor    = Worldwide Documentaries
| released       =  
| runtime        = 83 minutes (2011 version) 56 minutes (2014 version)
| country        = United States
| language       = English
| budget         =
| gross          =
}} independent documentary film about human trafficking and contemporary slavery. The film was written, produced, and directed by Robert Bilheimer, who had been asked to make the film by Antonio Maria Costa, executive director of the United Nations Office on Drugs and Crime. Bilheimer planned Not My Life as the second installment in a trilogy, the first being A Closer Walk and the third being the unproduced Take Me Home. The title Not My Life came from a June 2009 interview with Molly Melching, founder of Tostan, who said that many people deny the reality of contemporary slavery because it is an uncomfortable truth, saying, "No, this is not my life."
 United States. The first and last scenes of the film take place in Ghana, and show children who are forced to fish in Lake Volta for 14 hours a day. The film also depicts sex trafficking victims, some of whom are only five or six years old.
 Richard Young, its cinematographer and co-director, after he died in December 2010. It had its premiere the following month at the Lincoln Center for the Performing Arts in New York City. The narration was completely rerecorded in 2011, replacing Ashley Judds voice with that of Glenn Close. The version of the film that was aired by CNN International as part of the CNN Freedom Project was shorter than the version shown at the premiere. In 2014, a re-edited version of the film was released.

Not My Life addresses many forms of slavery, including the military use of children in Uganda, involuntary servitude in the United States, forced begging and garbage picking in India, sex trafficking in Europe and Southeast Asia, and other kinds of child abuse. The film also focuses on the people and organizations engaged in working against human trafficking. The film asserts that most victims of human trafficking are children. Actress Lucy Liu said that people who watch Not My Life "will be shocked to find   is happening in America." Lucy Popescu of CineVue criticized the film for focusing on the victims, arguing that the perpetrators of trafficking should have been dealt with more prominently. Not My Life was named Best World Documentary at the Harlem International Film Festival in September 2012.

==Themes== unfree labor in Ghana, forced begging and garbage picking in India, sex trafficking in Europe and Southeast Asia, and other kinds of child abuse. The focus of the film is on trafficking victims, especially women and children, the latter of whom are often betrayed by adults that they trust.  The film also focuses on the people and organizations engaged in working against human trafficking,  including members of the Federal Bureau of Investigation (FBI), Free the Slaves, Girls Educational and Mentoring Services (GEMS), International Justice Mission (IJM), the Somaly Mam Foundation, Terre des hommes, Tostan, UNICEF, United Nations Office on Drugs and Crime (UNODC), and the United States Department of State (US DoS).  Not My Life has been called "a cautionary tale".  It depicts the commodification of millions of people and identifies the practices of traffickers as undermining international economics, security, sustainability and health. 

 , including forced child begging.]]
 human trafficking drug trafficking.  The film indicates a relationship between contemporary slavery and globalization.  It asserts that most human trafficking victims are children,  although the filmmakers have recognized the fact that millions of adults are also trafficked.    The film depicts human trafficking as a matter of good and evil, provides interviews with survivors of human trafficking, and presents analysis from anti-trafficking advocates. Throughout the film, Robert Bilheimer encourages viewers to personally combat human trafficking.  Bilheimer was sparing in his use of statistics in the film, feeling that overloading viewers with figures might numb them to the issues. 
 Black Snake Moan (2007).  The Academy Award-winning Hustle & Flow portrays a pimp as the hero, while Black Snake Moan features Christina Ricci as a young Hypersexuality|nymphomaniac; the marketing for Black Snake Moan centered on evocative, sexualized slave imagery, including a scantily-clad Ricci in chains. Rhodes, p. 12.  According to Rhodes, Bilheimer "rescue  modern slaves from representation as exotic creatures, to restore their humanity" and allow audiences to relate to them. For this purpose, Bilheimer tells stories of individuals in the context of their communities and families. Rhodes, p. 14.  While Bilheimer had previously done extensive social justice work with religious organizations, he did not proselytize in the film,  despite the many opportunities the film afforded him to do so. Rhodes, p. 16.  

==Contents==

Fifty people are interviewed in Not My Life,    including Katherine Chon of the Polaris Project,  investigative journalist Paul Radu of Bucharest, Vincent Tournecuillert of Terre des hommes, Iana Matei of Reaching Out Romania, Rhodes, p. 6.  UNICEF Director of Programmes Nicholas Alipui,  Susan Bissell of UNICEFs Child Protection Section, Antonio Maria Costa of UNODC, Somaly Mam of the Somaly Mam Foundation, Molly Melching of Tostan in Senegal, and Suzanne Mubarak, who was First Lady of Egypt at the time.  The sex trafficking victims shown in the film include children as young as five and six years old. 

 s, Quranic schoolboys, in Senegal. Not My Life documents forced child begging in Senegal, where around 50,000 talibes beg on the streets under the threat of being beaten if they do not meet set quotas.]]
 skin and spoiled food—one Romani families child prostitutes. The narrator says that the profits of human trafficking "are built on the backs and in the beds of our planets youth." 

In Zoha Prison in Romania,  there are interviews with traffickers serving prison sentences that the film suggested were too short in light of the severity of the crime of human trafficking.  The typical sentence for this crime is six or seven years, while the sentence for trafficking in drugs is normally twenty years.  Two Romanian traffickers, Traian and Ovidiu, attest to having starved,  punched, and kicked the girls they trafficked.  Ovidiu recounts a story, in an interview filmed in February 2007,  about kidnapping a prostitute and selling her for sex when he was 14. He expresses no remorse for these actions.  The sentences served by Traian and Ovidiu were short enough that, by the time the film was released, they were no longer in prison.  Ana, a girl they trafficked, is also interviewed in the film, saying that she lost a tooth in one of her beatings. She describes being pregnant at the time, but not telling this to her captors because of fears for the unborn childs safety.   
 Richard Young (center) and Robert Bilheimer (right). Radu is one of fifty people interviewed in Not My Life.]]

Radu is interviewed in this portion of the film, as is Tournecuillert, who speaks about his experiences in Albania, where he heard about the sex trafficking of girls and how some of the girls would be shot or burned to death as a warning to the other girls.  He describes how Albanian girls are often rounded up to be sexually trafficked in Italy. He further explains that, normally, before they leave Albania, the traffickers kill one of the girls in front of the others—usually by burning or shooting—to demonstrate what will happen to others who try to escape.  Matei adds that, for the sake of amusement, some of the girls would be buried alive with only their heads remaining above ground.  Eugenia Bonetti, a nun, speaks about her work helping girls escape from slavery in Italy. Rhodes, p. 15. 
 American Midwest Mike Beaver K Street, changing into prostitutes attire. 

Angie was rescued during Operation Stormy Nights, an anti-human-trafficking operation carried out by the FBI, in 2004.  Bilheimer said that, while there is no way of being certain how many girls like Angie are being sexually trafficked in the U.S., "diligent people out there have arrived at a bare minimum figure of&nbsp;... one hundred thousand girls, eight to fifteen   ten sex acts a day" adding up to "a billion unpunished crimes of sexual violence on an annual basis." Rhodes, p. 46. 
 Sheila White Clinton Global Initiative. White is interviewed in Not My Life.]]
 Sheila White, Clinton Global Initiative. 
 madam reacts furiously, perceiving the raid as taking away her livelihood.  Then, the trafficking of children into the sex industry is depicted in Cambodia.  Some scenes take place in Svay Pak, Phnom Penh, one of the cheapest sex tourism destinations in the Mekong Delta.  Women of the Somaly Mam Foundation are depicted working with girls who have been sexually trafficked. A large number of these girls are pictured one by one, each child fading into the next against the backdrop of a doorway.  An interview with one of the Somaly Mam Foundation workers, Sophea Chhun, reveals that her daughter, Sokny, was kidnapped in 2008 at age 23. Rhodes, p. 9.  "Most likely Sokny too was sold," Chhun says, claiming that "the police treated it like she wasnt important"—perhaps, she suggests, because Sokny was an adopted child. Rhodes, p. 10.  Don Brewster of Agape International Missions is interviewed, and says that all of the girls they have rescued from child sex tourism in Cambodia identify Americans as the clients who were the most abusive to them. Rhodes, p. 5.  Bilheimer agreed with this assertion in an interview outside the film.     
 Hillary Clinton Trafficking in Persons (TIP) Hero. Haugen appears in Not My Life.]]
 TIP Report. 
 abducted by Joseph Kony to be used as a child soldier  in the Lords Resistance Army (LRA),  is interviewed, saying that "this kind of evil must be stopped."  She was forced to kill another girl as part of her initiation into the LRA, a very common practice among armies that employ child soldiers. The film states that she was ultimately rehabilitated and became a mother. 

Bishop Desmond Tutu, who Bilheimer had previously interviewed for The Cry of Reason, appears towards the end of the film,  saying, "Each of us has the capacity to be a saint." Rhodes, p. 3.  Bilheimer included Tutu in Not My Life because he felt that audiences might be in need of pastoral counseling after watching the film.  The final scene of Not My Life returns to the boy holding his breath underwater in Ghana. His name is revealed to be Etse, and it is stated that he and six other trafficking victims shown in the film have been rescued.  Some of the last words in the film are spoken by Brazilian human rights advocate Leo Sakomoto: "I cant see a good life while there are people living like animals. Not because Im a good person, not because its my duty, but because they are human—like me." 

==Production==

===Background===
 , executive director of the United Nations Office on Drugs and Crime, asked Robert Bilheimer to make Not My Life.]]

The project that became Not My Life was initiated by the executive director of the UNODC,  , which documents internal resistance to South African apartheid by way of Beyers Naudés story; and A Closer Walk, which is about the epidemiology of HIV/AIDS. Costa e-mailed Bilheimer,  Director of Worldwide Documentaries,  asking him to create the film he envisioned. Costa said that he choose Bilheimer because the director had developed a "solid reputation   addressing difficult topics... combining artistic talent, a philosophical view about development and a humanitarian sentiment about what to do about the issues." 

Bilheimer accepted Costas proposition,  and subsequently wrote, produced, and directed Not My Life    as an independent film.    Bilheimer, who had received an Academy Award nomination for The Cry of Reason,  said that "the unrelenting, unpunished, and craven exploitation of millions of human beings for labor, sex, and hundreds of sub-categories thereof is simply the most appalling and damaging expression of so-called human civilization we have ever seen."  Bilheimers wife, Heidi Ostertag, is Worldwide Documentaries Senior Producer, and she co-produced Not My Life with him. She said that she found making a film about human trafficking difficult because "people do not want to talk about this issue." Bilheimer found that the connections he had made during the production of A Closer Walk were also useful when producing Not My Life because the poor and the outcast are at the greatest risk of both HIV/AIDS and human trafficking; there is, for this reason, much overlap between the groups victimized by these two afflictions.  Bilheimer attempted to fashion the film in such a way that every part of it would illustrate a statement made by Abraham Lincoln: "If slavery is not wrong, nothing is wrong."   
 contemporary abolitionist movement did not yet exist. He described his purpose in creating the film as to raise awareness and initiate such a movement.  He also wished to communicate to his audiences that not all human trafficking is sexual.  Traffickers "commit unspeakable, wanton acts of violence against their fellow human beings," he said, "and are rarely punished for their crimes."  Production of Not My Life was supported by the United Nations Global Initiative to Fight Human Trafficking (UN.GIFT), UNICEF, and UNODC,    providing Worldwide Documentaries with $1 million in funding secured by Costa. 

===Filming===

 

Bilheimer said that the level of cruelty he saw in shooting Not My Life was greater than anything he had seen when documenting apartheid in South Africa for The Cry of Reason.  Bilheimer attested to becoming more aware of the global extent of human trafficking as he went about making Not My Life.  The films title came from a June 2009 interview with Molly Melching, founder of Tostan, an organization dedicated to human rights education operating in ten African countries. As Bilheimer and Melching spoke in Thiès, Senegal, discussing how people often deny the reality of contemporary slavery because it is an uncomfortable truth, Melching said, "People can say, No, this is not my life. But my life can change. Lets change together." 
 washboard roads in Land Rovers and did not sleep.  Filming in Svay Pak took place in March 2010,  and shooting in Abusir (Lake Mariout)|Abusir, Egypt took place the following month. Rhodes, p. 2. 

In Guatemala, Bilheimer facilitated the arrest of trafficker Ortiz by renting a car for the police to use, in order to film the arrest as part of Not My Life. Rhodes, p. 7.  Bilheimer said that, during the making of the film, he and his crew were surprised to discover that traffickers employ similar methods of intimidation across the globe, "almost as if there were&nbsp;... unwritten bylaws and tactics&nbsp;... The lies are the same." 

===Editing===
  in Egypt. Bilheimer removed much of the related content from the film after the Arab Spring made the information outdated.]]
Susan Tedeschi, Derek Trucks, and Dave Brubeck performed the theme song for Not My Life, Bob Dylans "Lord Protect My Child", which was produced by Chris Brubeck.  After the initial screenings in early 2011, the film went through a series of revisions, taking into account information gathered from more than thirty screenings for focus groups. Later that year, the narration was completely rerecorded;  Bilheimer replaced Ashley Judds voice with that of Glenn Close, Rhodes, p. 55.  who had previously worked with him on A Closer Walk.  The version of the film that was aired by CNN International was shorter than the version shown at the premiere.  An even shorter version, only 30 minutes long, was created with school audiences in mind. 
 redeemed and become saints. 

 

As had occurred with Bilheimers previous film, A Closer Walk, Not My Life had several preview screenings before its official release. Rhodes, p. 25.  The United States Agency for International Development (USAID) hosted a preview screening at the Willard InterContinental Washington in September 2009 as part of a day-long symposium on human trafficking.  A preview screening in Egypt, including the material shot in that country that was later removed,  took place in December 2010 at the International Forum against Human Trafficking in Luxor.   
 Richard Young died. Not My Life was subsequently dedicated to him. Rhodes, p. 43.  Bilheimer said that Young had believed in the film far more than he himself had. Rhodes, p. 52. 

==Release==
 Crown Princess Victoria.  Bilheimer recognized that people combatting human trafficking are in need of resources, so he considered various options with respect to the intellectual property of Not My Life, ultimately deciding to release the film at charge Rhodes, p. 38.  in addition to selling licenses for unlimited private screenings. Rhodes, p. 39.  On November 1,  an 83-minute  version of the film was released on DVD by Worldwide Documentaries,  which also began digitally distributing the film and selling the unlimited licenses. LexisNexis, the governments of Arizona and Minnesota, and the U.S. Fund for UNICEF all purchased licenses. The latter organization planned to use the film as part of an anti-human-trafficking campaign. 

  and contemporary slavery.]]

Not My Life was screened at the 2012 UNIS-UN Conference in New York City, the theme of which was human exploitation.  Segments from the film were included in "Can You Walk Away?",  a 2012 exhibition on contemporary slavery at President Lincolns Cottage at the Soldiers Home in Washington, DC. Rhodes, p. 58.  A hotel chain presented the film to its staff in London in preparation for the 2012 Summer Olympics to raise awareness about the types of human trafficking that might take place in conjunction with the events.  Bilheimer initiated an Indiegogo crowdfunding campaign in 2012 to allow local organizations opposing human trafficking to screen the film.  That same year, he expressed a willingness to release fifteen-minute excerpts from the film to help its message reach more people. Rhodes, p. 40. 
 environmental film Iraq and War in Afghanistan (2001–present)|Afghanistan.  Bilheimer said in 2013 that Not My Life "is not a perfect film by any means, but it is having an impact." He said that he would like to be moving on to a new film project, but that he would continue promoting Not My Life because he thought it could help combat human trafficking. 
 Carlson Family Foundation.  That same year, the Swedish International Development Cooperation Agency gave Worldwide Documentaries a grant to do anti-human trafficking work over a three-year period.    Not My Life was screened at the Mexican film festival Oaxaca FilmFest in November 2012;  BORDEReview in Warsaw, Poland, in May 2013;  and the Pasadena International Film & New Media Festival in February 2014. 

  resigned from the Somaly Mam Foundation, Bilheimer re-edited Not My Life to remove her from the film.]]

In May 2014, the Somaly Mam Foundation released a statement that Somaly had resigned from her leadership of the organization as a result of investigations regarding allegations about her personal history.  The following month, Bilheimer released a statement in response, saying that he had re-edited the film in order to remove the scenes depicting Somaly and that the new version would be available shortly. Bilheimer wrote that "the storytelling in the Cambodia segment of Not My Life remains intact and is still very moving, with an even sharper focus, now, on the girls themselves."    In this statement, Bilheimer requested that people screening previous versions of the film tell their audiences that the presence of Somaly in the film is understandably a distraction, that the film is not primarily about Somaly but rather about the millions of children in slavery in the world, and that this focus is what is most important about the film. 
 child labor.  This content emphasizes that there are more human trafficking victims in India than in any other country in the world.  The new version of the film, which was co-produced with the Delhi-based Riverbank Studios,  was 56 minutes long and premiered at the India International Centre in New Delhi on June 26. Satyarthi was one of the panelists in a panel discussion accompanying the screening, as was Indian filmmaker Mike Pandey,  who had managed Riverbank Studios side of the co-production.  The film was scheduled to air on Doordarshan (DD) in Hindi three days later.    In July, Bilheimer called his continued work on the film "a labor of love" and said that "far too much silence still surrounds the issue" of human trafficking.   

==Reception==
  spoke at the United States Agency for International Development preview screening of Not My Life, saying that 80,000 women are victims of sexual assault every day.]]

At the USAID preview screening, actress Lucy Liu, who has worked with MTV EXIT and produced the documentary film Redlight (film)|Redlight, said that people who watch Not My Life "will be shocked to find   is happening in America"; she said that there were 80,000 women being sexually assaulted daily and she called human trafficking the "cannibalization of the planets youth."  According to UN.GIFT, before Not My Life, there was "no single communication tool that effectively depict  the problem as a whole for a mass audience."  Susan Bissell, UNICEFs Child Protection Section chief, agreed with this assertion,  and said that the film "takes a close look at the underlying causality that so many other filmmakers have missed   it will change the way we see our lives, in some very fundamental ways."    She also said that Not My Life is an important documentary because it brings attention to underreported forms of abuse. A reviewer from Medical News Today praised the film for "raising awareness and speaking about taboo subjects," arguing that these activities "are critical to empower families, communities, and governments to speak out honestly and take action against abuses." 

Lucy Popescu of CineVue called the film "a powerful indictment of the global trade in human beings and the abuse of vulnerable people," but criticized the film for focusing on human trafficking victims, arguing that the perpetrators should have been dealt with more prominently. She commended Bilheimer on the few interviews with traffickers that he did include, but she condemned as inadequate the "only passing reference to the thousands of men who engage in sexual tourism, like those who travel to Cambodia to buy traumatized children who they can then abuse for weeks at a time." Popescu also called the film "simplistic", arguing that it should have more clearly expressed that sex trafficking victims are not able to provide legitimate consent for sexual activity because they are afraid that their lives might be in danger if they do not comply.  John Rash of the Star Tribune called the film "a cacophony of concerned voices speaking about a modern-day scourge." Rash praised the film for its global scope, but suggested that this geographical breadth allows American audiences to ignore the fact that the trafficking of children is prevalent in the United States and not just in other countries.   

Not My Life was named Best World Documentary at the Harlem International Film Festival in September 2012.  Nancy Keefe Rhodes of Stone Canoe called it a "highly-distilled&nbsp;... remarkable film," describing Bilheimer as "committed to strong story-telling and film-as-craft."  She commends Bilheimer on alternating between American sequences and scenes in other countries, allowing "the experiences of young women with whom an American audience may more readily identify   become one among many woven into the fabric of global trafficking."  Tripurari Sharan, Director General of DD, said that his organization was pleased to air the film and hoped that doing so would bring about greater awareness across India about human trafficking in the country. He called the film "both an eye-opener and a profoundly moving call to action". 

==Notes==
 

==References==
 

==Bibliography==
*  

==External links==
 
*  
*  , the distributor
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 