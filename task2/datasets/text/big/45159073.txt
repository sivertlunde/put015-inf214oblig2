Homer Comes Home
{{Infobox film
| name           = Homer Comes Home
| image          = Homer Comes Home (1920) 1.jpg
| alt            = 
| caption        = Film still with Priscilla Bonner, Charles Ray, and Otto Hoffman
| director       = Jerome Storm
| producer       = Thomas H. Ince
| screenplay     = Alexander Hull Agnes Christine Johnston Charles Ray Otto Hoffman Priscilla Bonner Ralph McCullough Walter Higby John Elliott Harry Hyde
| music          = 
| cinematography = Chester A. Lyons 
| editor         = Harry L. Decker 
| studio         = Thomas H. Ince Productions Artcraft Pictures Corporation Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent comedy Charles Ray, Otto Hoffman, Priscilla Bonner, Ralph McCullough, Walter Higby, John Elliott, and Harry Hyde. The film was released on June 27, 1920, by Paramount Pictures.   A copy of the film exists in a collection or archive. 

==Plot==
 

==Cast== Charles Ray as Homer Cavender
*Otto Hoffman as Silas Prouty 
*Priscilla Bonner as Rachel Prouty
*Ralph McCullough as Arthur Machim
*Walter Higby as Old Machim
*John Elliott as Mr. Bailly
*Harry Hyde as Mr. Kort
*Gus Leonard as The Grocer
*Joseph Hazelton as The Shoe Store Man 
*Bert Woodruff as Farmer Higgins
*Louis Morrison as Old Tracey

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 