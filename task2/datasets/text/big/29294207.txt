The Sitter
 
{{Infobox film
| name = The Sitter
| image = The Sitter Poster.jpg
| caption = Teaser poster
| director = David Gordon Green
| producer = Michael De Luca
| writer = Brian Gatewood Alessandro Tanaka
| starring = Jonah Hill Max Records Ari Graynor J. B. Smoove Sam Rockwell
| music = Jeff McIlwain David Wingo
| cinematography = Tim Orr
| editing = Craig Alpert
| studio = Michael De Luca Productions
| distributor = 20th Century Fox
| released =  
| runtime = 81 minutes  87 minutes (unrated extended version)
| country = United States
| language = English
| budget = $25 million 
| gross = $34.5 million 
}} film directed by David Gordon Green and produced by Michael De Luca. The film follows a slacker college student who, after being suspended, is forced by his mother to fill in for a babysitter that called in sick. During this time, he takes his charges along for his extensive criminal escapades.

The film is a Michael De Luca Productions and 20th Century Fox joint venture, distributed by 20th Century Fox. The film was originally scheduled to be released in theaters on August 5, 2011, but was pushed back to December 9, 2011.

== Plot ==
Noah ( ), pop culture obsessed wild child Blithe (Landry Bender) and pyromaniac adopted son Rodrigo (Kevin Hernandez).

== Cast ==
* Jonah Hill as Noah Griffith, a suspended college student. friend with benefits.
* Sam Rockwell as Karl, a drug lord.
* J. B. Smoove as Julio, Karls right-hand man.
* Max Records as Slater Pedulla, the neurotic oldest son.
* Landry Bender as Blithe Pedulla, the vain, celebrity-obsessed daughter.
* Kevin Hernandez as Rodrigo Pedulla, the unpredictable adopted son.
* Kylie Bunbury as Roxanne, Noahs college classmate.
* Method Man as Jacolby
* Erin Daniels as Mrs. Pedulla
* D. W. Moffett as Doctor Pedulla
* Jessica Hecht as Sandy Griffith
* Bruce Altman as Bruce Griffith
* Nicky Katt as Officer Petite

== Reception ==
The Sitter has received mostly negative reviews from critics. It currently holds a 21% rating on   listed it as the #1 worst movie of 2011.

== Home media ==
The Sitter was released on DVD and Blu-ray on March 20, 2012.

== References ==
 

== External links ==
*  
*  
*  

 

 

 
 
 
 
 
 
 