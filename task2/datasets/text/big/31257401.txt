Petualangan Sherina
{{Infobox Film
| name           = Petualangan Sherina
| image          = Petualangan Sherina.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Riri Riza
| producer       = Mira Lesmana
| writer         = Jujur Prananto
| starring       = Sherina Munaf Didi Petet Mathias Muchus Ratna Riantiarno Butet Kertaradjasa Henidar Amru Djaduk Ferianto Dewi Hughes Ucy Nurul Derby Romero
| music          = Elfa Secioria
| cinematography = Yadi Sugandhy
| editing        = Sentot Sahid
| distributor    =  
| released       = 14 June 2000
| runtime        = 114 minutes
| country        = Indonesia
| language       = Bahasa Indonesia
| budget         = Rp 2 billion
| gross          = Rp 10 billion
}}

Petualangan Sherina (English: Sherina’s Adventure) is an Indonesian musical film released in 2000. It was directed by Riri Riza and starred Didi Petet, Mathias Muchus, Ratna Riantiarno, and Butet Kertaradjasa. The screenplay was written by Jujur Prananto, with music arranged by Elfa Secioria.
<!--One of the inclusions of promotional preparation is Sherina’s name in the title. The original title of this film was The Adventures of Vera and Elmo. They replaced the title because of the popularity of a young artist named Sherina Munaf, who was being heightened at the moment because of her album.
-->

==Plot==
Sherina is a little girl who is smart, energetic, and loves to sing. She lives in Jakarta with her parents, Mr. and Mrs. Darmawan (Mathias Muchus and Ucy Nurul). However, Sherina has to leave her friends when her father accepts a job as an agronomist, at a plantation in Lembang owned by Ardiwilaga (Didi Petet). In her new environment, she quickly adapts and acquires some new friends. However, she becomes the target of the class bullies, consisting of a boy named Sadam (Derby Romero) and his two friends. They often bother children who are weaker. Sherina cannot accept such treatment and she gathers her classmates in a struggle against Sadam. When she accompanies her father to visit the plantation, she discovers that Sadam is Ardiwilagas son.
 boot telling her to get away. She escapes and, when the four kidnappers leave, she follows them. She discovers that they have been tasked with kidnapping Sadam by a cunning businessman named Kertarajasa (Djaduk Ferianto), who wishes to buy the plantation to complete their development project.

When the kidnappers try to call Ardiwilaga, they reach his wife (Ratna Riantiarno) and tell her to prepare a 3&nbsp;billion rupiah ransom. She agrees to this condition and convinces her husband to sell the land Natasya (Henidar Amroe), who had offered them 2&nbsp;billion the day before. Later, at the kidnappers hideout, Sherina is able to rescue Sadam while the guards are sleeping. They take documentation connecting the thugs to Kertarajasa escape to a nearby observatory, where they spend the night. In Jakarta, Kertarajasa dances with his wife, revealed to be Natasya, and states that he will launch his project the following day once the plantation is legally his.

In the morning Sadam is desperately ill. Sherina tries to leave the observatory through the front door only to discover that it had been locked and the kidnappers were outside. With Sadams help she rappels down the observatory wall and outruns two of the kidnappers, making her way to a nearby village and catching a ride to Ardiwilagas home; Sadam, however, is captured. Sherina arrives just as he is preparing to sign the land deed over to Natasya and reveals Kertarajasas plans. Police quickly arrest Natasya and Kertarajasa, and when the kidnappers try to collect their ransom they too are arrested. When Sherina and Sadam return to school after the holiday they are friends, and Sadam agrees to stop bullying his peers.

==Production== Rp 1.3 billion (US$ 180,000),  but went over budget and eventually cost Rp 2 billion (US$ 250,000).  The filming was conducted from November to December 1999.    In this film, Elfa Secioria composed the films soundtrack Lihatlah Lebih Dekat (Take a Closer Look), which is also Sherinas second album,  while Lesmana wrote the lyrics for eight songs.  Lesmana said that ideas and inspirations came from The Sound of Music and Grease (film)|Grease. 

==Style and themes==
Sherina deals with themes of friendship, children, environment and family relationships, as well as coping with the entertainment business.  According to a review by The Jakarta Post the storyline is easily understood by all, including children from all levels of society. The review states that the film boasts fine graphic quality, smooth camerawork, neatly constructed scenes and attractive cinematography, especially shots of mountains, forests and landscapes. This was reminiscent of several Indonesian films from the 1950s to 1970s, including Si Pintjang (The Lame; 1951), Jenderal Kancil (General Deer-mouse; 1958), Bintang Kecil (Little Star; 1963), and Rio Anakku (Rio, My Son; 1973).   

==Release and reception== Tempo Magazine, wrote that the music for the film was interesting and easy to listen to, with brilliant visualization, but the editing is rather chaotic, and the childrens choreography is weak. 

The film was watched by over one million people,  and earned Rp 10 billion (US$ 1.2 million). 
<!--
==Album==
Sherinas Adventure is an album of music by Sherina Munaf. The album was released in the year 2000. This album was also the theme song of the movie The Adventures of Shrek.

===List of songs===
# Anak Mami (Mommys Boy)
# Bintang (Stars)
# Jagoan (Hero)
# Kertarejasa
# Lihatlah Lebih Dekat (Look Closer)
# Menikmati Hari (Enjoying The Day)
# Persahabatan (Friendship)
# Petualangan Sherina (Sherinas Adventure)
-->

==Awards==
Petualangan Sherina received a special jury prize at the 2000 Asia Pacific Film Festival. The following year it won Best Childrens Film at the Bandung Film Festival. At the 2004 Indonesian Film Festival, the first in over a decade, the film received three nominations for a Citra Award but saw no wins. 

{| class="wikitable plainrowheaders" style="font-size: 95%;"
|-
! scope="col" | Award Year
! scope="col" | Category
! scope="col" | Recipient
! scope="col" | Result
|-
! scope="row" | Asia Pacific Film Festival
| 2000 Special Jury Prize
|–
| 
|-
! scope="row" | Bandung Film Festival
| 2001
| Best Childrens Film
|–
| 
|-
! scope="row" rowspan="3" | Indonesian Film Festival
| rowspan="3" | 2004 Best Leading Actor
| Derby Romero 
|  
|-
| Best Supporting Actor Djaduk Ferianto
|  
|-
| Best Musical Direction
| Elfa Secioria
|  
|}

 

==References==
 

==External links==
* 

 
 
 