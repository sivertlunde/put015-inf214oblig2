Advice to the Lovelorn
{{Infobox film
| name           = Advice to the Lovelorn
| image          = 
| alt            = 
| caption        =
| director       = Alfred L. Werker
| producer       = Darryl F. Zanuck
| writer         = Leonard Praskins 
| based on       =  
| starring       = Lee Tracy Sally Blane Paul Harvey Sterling Holloway C. Henry Gordon Isabel Jewell Alfred Newman
| cinematography = James Van Trees
| editing        = Allen McNeil 
| studio         = 20th Century Pictures
| distributor    = United Artists
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Advice to the Lovelorn is a 1933 American drama film directed by Alfred L. Werker and written by Leonard Praskins. The film stars Lee Tracy, Sally Blane, Paul Harvey, Sterling Holloway, C. Henry Gordon and Isabel Jewell. The film was released on December 1, 1933, by United Artists.   

==Plot==
 

== Cast ==
*Lee Tracy as Toby Prentiss
*Sally Blane as Louise
*Paul Harvey as Gaskell
*Sterling Holloway as Benny
*C. Henry Gordon as Kane
*Isabel Jewell as Rose 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 