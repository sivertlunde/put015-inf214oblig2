Carlos (film)
 
{{Infobox film
| name           = Carlos
| image          = 
| caption        = 
| director       = Hans W. Geißendörfer
| producer       = Ernst Liesenhoff Hellmut Haffner Günther Witte
| screenplay     = Hans W. Geißendörfer Bernd Fiedler
| based on       =   
| starring       = Gottfried John Anna Karina
| music          = Ernst Brandner
| cinematography = Robby Müller
| editing        = Wolfgang Hedinger BR Westdeutscher WDR
| distributor    = 
| released       =  
| runtime        = 107 minutes
| country        = West Germany
| language       = German
| budget         = 
}}

Carlos is a 1971 West German  , who also wrote the screenplay, and stars Gottfried John and Anna Karina.

The film was shot in Eilat in Israel, with several Israelis participating in smaller roles and as extras.

==Cast==
* Bernhard Wicki as Philipp
* Gottfried John as Carlos
* Anna Karina as Clara
* Geraldine Chaplin as Lisa
* Horst Frank as Ligo Thomas Hunter as Pedro
*   as Enrico (as Sabi Dor)
* Ebba Kaiser
* Lorenza Colville as Roswitha
* Shaike Ophir as Domingo
* Reuven Shefer as Tassos
* Shmuel Wolf as Mönch
* Joseph Shiloach as Mario (as Yossi Shiloah)
* Leon Charney as Col

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 
 