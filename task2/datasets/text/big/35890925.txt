Maad Dad
{{Infobox film
| name           = Maad Dad
| image          = Maad_dad.jpg
| image_size     = 
| caption        = 
| director       = Revathy S Varmha
| producer       = P. N. Venugopal
| story          = Revathy S Varmha
| screenplay     = Revathy S Varmha
| writer         = Revathy S Varmha Lal Nazriya Nazim Meghana Raj Lalu Alex Padmapriya Pooja Gandhi
| music          = Alex Paul
| cinematography = Pradeep Nair
| editing        = Johnkutty
| studio         = P N V Associates
| distributor    = 
| runtime        = 
| released       =  
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}

Maad Dad is a 2013 Malayalam film directed by Revathy S Varmha, starring Lal (actor)|Lal, Nazriya Nazim, Meghana Raj, Lalu Alex, Padmapriya and Pooja Gandhi.  

==Cast== Lal as Palachottil Geevargees Kuriyakose Isow
* Nazriya Nazim as Mariya
* Meghana Raj as Anuradha/Annamma (Wife)
* Lalu Alex as Soda Mathan (Bony’s Dad)
* Padmapriya as Dr. Rasiya
* Pooja Gandhi as Lisa Vijayaraghavan as Krishnettan
* Balu R Nair as Ansari Janardanan as Mamachan
* Aiswarya as Sosamma (Mrs. Mathan)
* Sreejith Vijay as Bonnie (Love interest of Mariya)
* Kovai Sarala as Dawini D’Zosa
* Salim Kumar as Giridhar Shari as Sarala
* Balu as Young Easho
* Lakshmipriya as Omana

==Production==
Mohanlal was to play the title role first,  but he was replaced by Lal (actor)|Lal.  Also Shweta Menon was first signed for a role but had to opt out due to pregnancy. She was replaced by Padmapriya.  Archana Kavi, who was to play an important role, has been replaced by Nazriya Nazim. 

==Critical Reception==
Malayalam Manorama called it  a good movie. 
 

==References==
 

 
 


 