Cheap Kisses
{{Infobox film
| name           = Cheap Kisses
| image          =
| caption        = John Ince Cullen Tate
| producer       = C. Gardner Sullivan
| writer         = C. Gardner Sullivan
| starring       = Jean Hersholt
| cinematography = Jules Cronjager
| editing        = Barbara Hunter
| distributor    =
| released       =  
| runtime        =
| country        = United States Silent English intertitles
| budget         =
}}
 silent drama film starring Jean Hersholt as a famous sculptor.  This was the first film made by screenwriter C. Gardner Sullivan through his new production company, C. Gardner Sullivan Productions.  Sullivan also wrote the screenplay.

==Plot==
The plot focuses on a famous sculptor, Gustaf Borgstrom (played by Jean Hersholt).  He hires a chorus girl, Ardell Kendall (played by Lillian Rich).  When Donald Dillingham (played by Cullen Landis) marries the chorus girl, his wealthy parents disown him.

==Cast==
* Jean Hersholt - Gustaf Borgstrom
* Michael Dark - Butterworth Little
* Sidney DeGray - Henry Dillingham
* Louise Dresser - Jane Dillingham
* Bessie Eyton - Maybelle Wescott
* Cullen Landis - Donald Dillingham
* Kathleen Myers - Mignon De Lisle
* Vera Reynolds - Kitty Dillingham
* Lillian Rich - Ardell Kendall
* Tom Ricketts - The Old Man
* Phillips Smalley - George Wescott
* Lincoln Stedman - Bill Kendall

==References==
 

==External links==
* 

 
 
 
 
 
 


 