It May Be You
 
{{Infobox film
| name           = It May Be You 
| image          = 
| caption        = 
| director       = Will Louis
| producer       = 
| writer         = Lawrence Corey
| starring       = Arthur Housman
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent film English intertitles
| budget         = 
}}
 silent comedy film featuring Oliver Hardy.

==Cast==
* Arthur Housman - Jack Kenwood
* Andy Clark - The Office Boy (as Andrew J. Clark)
* Dallas Welford - Hi Jinks
* Mabel Dwight - Mrs. Jinks
* Maxine Brown - Jinkss Stenographer
* Charles Ascot - William Hall (as Charles Ascott)
* Caroline Rankin - Stenographer
* Oliver Hardy - Paul Simmons (as O.N. Hardy)
* Jessie Stevens - Stenographer
* Harry Eytinge - A Banker
* Gladys Leslie - His secretary
* Julian Reed - James Redfield
* Jean Dumar - Lillian

==See also==
* List of American films of 1915
* Oliver Hardy filmography

==External links==
* 

 
 
 
 
 
 
 

 