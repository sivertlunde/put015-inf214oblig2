Crawl (film)
 
{{Infobox film
| name           = Crawl
| image          = Film poster for the 2011 Australian horror suspense; Crawl.jpg
| alt            =  
| director       = Paul China
| producer       = {{Plain list|
*Benjamin China
*Brian J. Breheny}}
| writer         = Paul China
| starring       = {{Plain list|
* George Shevtsov
* Georgina Haig
* Paul Holmes}} Christopher Gordon Brian J. Breheny
| editing        = {{Plain list|
*Bin Li
*John Scott}}
| distributor    = {{Plain list|
*Arrow Films (UK)
*Bloody Disgusting (US)}}
| released       =  
| runtime        = 80 minutes
| country        = Australia
| language       = English
}}
Crawl is a 2011 Australian suspense-thriller written, produced and directed by Paul China and Benjamin China. The film is the China Brothers debut feature, and stars George Shevtsov, Georgina Haig and Paul Holmes.

== Plot ==
Slim Walding (Paul Holmes), a pompous bar owner, hires a laconic, nameless Croatian (George Shevtsov) to kill an acquaintance over an unpaid debt. The crime is carried out, but the culprit intentionally leaves behind the murder weapon; an antique pistol belonging to Slim (thus framing him for the homicide). However, as the Croatian makes his getaway, he accidentally runs down a stranded motorist. Now burdened with a bloodied corpse and a wrecked vehicle, the murderer seeks refuge at a nearby, isolated house. Here, he holds a young woman, Marilyn Burns (Georgina Haig), hostage.

The Croatian then learns that Marilyns fiancee is the motorist he has killed. In order to make an escape on a motorcycle (the only vehicle at hand), he ventures back to the crash site to retrieve the mans keys. There, the Croatian murders the fiancee, after discovering he is in fact still alive. Later, Slim realises that his prized pistol has been stolen and that he has been set up. Upon learning the Croatians whereabouts, he heads off looking for revenge. At the house, however, after freeing a gagged and bound Marilyn, Slim is killed when the Croatian unexpectedly attacks him with an axe.

Marilyn, terrified, attempts to escape the house, and soon finds herself involved in a cat-and-mouse chase with the mysterious intruder. Eventually, a fight erupts between the two of them, and the Croatian is subsequently shot dead by the hostage he underestimated.

== Cast ==
*George Shevtsov as The Stranger
*Georgina Haig as Marilyn Burns
*Paul Holmes as Slim Walding
*Lauren Dillon as Holly
*Lynda Stoner as Eileen
*Catherine Miller as Annie
*Andy Barclay as Travis
*Bob Newman as Rusty Sapp
*Paul Bryant as Sergeant Byrd
*John Rees-Osbourne as Constable Rolly

== Production ==
The film was shot on location in Queensland, Australia in late 2010. 

== Soundtrack == Christopher Gordon scored the film. 

== Release ==
Crawl played at over thirty international film festivals, including   won Best Director, Georgina Haig won Best Actress, and Brian J. Breheny won Best Cinematography.    Bloody Disgusting released the film in the US on September 1, 2012,   and Arrow Films released the film theatrically in the UK on February 22, 2013.  

== Reception ==
Crawl has received positive reviews from critics, scoring a 71% fresh rating on Rotten Tomatoes.  Sean Decker of Dread Central said the film ″rivals No Country For Old Men in gritty style and suspense″ and was an ″assured Hitchockian slow-burn.″   Journalist Alan Jones awarded the film four stars saying ″not since the Coen Brothers Blood Simple has there been a more exciting thriller debut...a nail-bitingly effective study in slow-burning terror...scary, sly and sexy...a twisted and skillful homage to film noir.″ 

== References ==
 

== External links ==
* 
*  at Rotten Tomatoes

 
 
 
 
 