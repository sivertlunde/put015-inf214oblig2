Daredevil (film)
{{Infobox film
| name           = Daredevil
| image          = Daredevil poster.JPG
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Mark Steven Johnson
| producer       = {{Plainlist|
* Avi Arad
* Gary Foster
* Arnon Milchan}}
| screenplay     = Mark Steven Johnson
| based on       =  
| starring       = {{Plainlist| 
* Ben Affleck
* Jennifer Garner
* Michael Clarke Duncan
* Colin Farrell
* Joe Pantoliano
* Jon Favreau
* David Keith}}
| music          = Graeme Revell
| cinematography = Ericson Core
| editing        = {{Plainlist|
* Armen Minasian
* Dennis Virkler}}
| studio         = {{Plainlist|
* Regency Enterprises Marvel Enterprises
* Horseshoe Bay Productions}}
| distributor    = 20th Century Fox
| released       =  
| runtime        = 103 minutes   133 minutes  
| country        = United States
| language       = English
| budget         = $78 million
| gross          = $179.1 million
}} same name, love interest Elektra Natchios; Wilson Fisk, also known as the crime lord Kingpin.

The film began development in 1997 at  , was released in February.

Reviews of the film were mixed, praising the performances of Affleck, Farrell, and Duncan, but criticizing the unoriginality and lack of character development. The film was still a profitable theatrical run and became Februarys second biggest release: it was successful enough for a Spin-off (media)|spin-off film, Elektra (2005 film)|Elektra, which was released in 2005. In 2004, an R-rated directors cut of Daredevil was released, reincorporating approximately 30 minutes of the film, including an entire sub-plot involving a character played by Coolio. The directors cut was intended as an improvement over the theatrical version.

==Plot==
  Matt Murdock Franklin "Foggy" Nelson. As a child, Matt was blinded by a toxic waste spill while walking home from school after discovering that his father, former boxer Jack "The Devil" Murdock, had become an enforcer for a local mobster. The accident enhanced Matts other senses and gave him sonar to "see" via sonic vibrations. Matt trained himself in martial arts. Blaming himself for Matts disability, Jack stopped being an enforcer, returned to boxing, and was killed after refusing to turn in a fixed fight for the mobster who had employed him earlier. To avenge his fathers death, Matt used his abilities to become the costumed crime fighter, Daredevil.
 Elektra Natchios, Wilson Fisk, a rich executive who is also the criminal leader known as the Kingpin. When Nikolas tries to bail on his dealings with Fisk, the mobster hires the Irish hitman Bullseye (comics)|Bullseye, who has preternatural aim, to kill him. Matt tries to stop Bullseye, but Bullseye ultimately succeeds in killing Nikolas and framing Matt in the process. Elektra vows to exact revenge, while reporter Ben Urich discovers his secret identity. Believing Matt to have done good things for Hells Kitchen, Urich tells Matt that Bullseye is going after Elektra next.

Matt tracks Bullseye, but is attacked by Elektra, who plans to use her own extensive training in martial arts to avenge her fathers death. After wounding him, she removes his mask, and discovers Matts secret identity and innocence. Forced to fight Bullseye alone, Elektra is overpowered and killed by the hitman. A wounded Matt makes his way to a church, where he is looked after by his confidante, Father Everett, who knows his secret identity. After recovering slightly, Matt fights Bullseye, who had followed him to the church. Bullseye discovers that loud noise is Matts weakness and prepares to kill him with a spiked piece of wood after incapacitating him. Matt blocks the attack and hears an FBI sniper stationed on the neighboring building preparing to fire. As the bullet is fired, Matt moves out of the bullets path of the bullet and pulls Bullseyes hands into it. When Bullseye pleads for mercy, Matt throws him from the churchs top floor. Bullseye lands on the hood of Urichs car, wounded but alive.

Upon discovering Fisk is the Kingpin and Bullseyes employer, Matt makes his way to Fisks office. Fisk proves to be a surprisingly powerful combatant due to his size and brute strength, overwhelming Matt for most of the fight. Lying on the floor, Matt questions Fisk as to why he killed the people Murdock loved, including Jack Murdock years before; Fisk replies it was just business. Angered, Matt regains his strength and breaks Fisks legs, but refrains from killing him, instead allowing him to be arrested by the police, who have discovered his criminal connections. Before being taken away, Fisk — who had also discovered Matts secret identity after overpowering him — swears revenge on Matt, who points out that Fisk cannot reveal Matts secret identity because he will be humiliated by  being beaten by a blind man.

Having gained closure over his fathers murder, Matt returns to his day-to-day routine, though brokenhearted over the loss of Elektra. He learns she might still be alive after he goes to the rooftop where they had their first kiss and finds a necklace like the one her mother gave her, except with a Braille inscription. Matt meets Urich one last time, in which Urich assures him he will not publish his article about Matts true identity.

In a mid-credits scene, Bullseye, having been moved to a prison hospital and severely bandaged after his confrontation with Matt, is shown to still have his perfect aim after he impales a fly with a syringe needle.

==Cast==
*   with a   series, they had modeled it on Affleck.    Affleck said Daredevil was his favorite comic book as a kid,  and explained why he took the role by saying "Everybody has that one thing from childhood that they remember and that sticks with them. This story was that for me."  He also stated another reason, being "I didnt want someone else to do it, because I was afraid that they would go out and do it different from the comic and screw it up."   
** Scott Terra as Young Matt Murdock: As a youth, he has trouble with local bullies and a close bond with his father. This changes in different ways after the accident. Terra was officially announced as a part of the cast in March 2002.   
*  . For the role of Elektra, many actresses were looked into with considerations including Penélope Cruz, Salma Hayek, Natalie Portman, Lucy Liu, Jessica Alba, and Katie Holmes.    A short-list was eventually made, giving the choices of Jennifer Garner, Jolene Blalock, Mía Maestro and Rhona Mitra,  with Garner finally becoming the actress to land the role.  Garner said of the character, "I think shes strong and cool and beautiful and smart. Shed be a good role model."  Garner noted that the costume would be different as Elektra often wears red satin, but in the film she wears black leather. Garner explained "the red would never have worked for hiding a harness, and I know this sounds ridiculous, but you have to protect your skin a little bit. They throw me around so much on the rooftop   I got cut through the leather, so imagine if I hadnt had anything."  Frank Millers Daredevil comics to understand Bullseye "because the expression on the characters faces in the comic books, and just the way they move sometimes, and the exaggerations of the character Im playing   hes so over-the-top that you do draw from that. But its not exactly a character you can do method acting for... you know, running around New York killing people with paper clips." 
*   with one or two reps a day, as well as eating whatever he wanted. Despite this, Duncans biggest concern was that he is black, while Kingpin has always been portrayed as white. Also, Duncan provided the voice for Kingpin in  .  He spoke on the fans loyalty to the source material by saying "they watch movies to say, Hey, thats not like the comic book. But I want them to get past that and just see the movie for what it is and see me for what I am—an actor."  Iron Man and its sequel, Iron Man 2 while also appearing as Happy Hogan.
* Joe Pantoliano as Ben Urich: An investigative journalist whose articles notably relate to Daredevil, and the Kingpin. During the film, he goes on to uncover a lot of information about the two. Pantoliano was cast in March 2002. 
* David Keith as Jack Murdock: Father of Matt Murdock and a boxer, he is murdered by the Kingpin when he does not take a fall for a fight. Keith was officially announced as a part of the cast in March 2002. 
*  .
* Lennie Loftin as Detective Nick Manolis: A NYPD cop who considers Daredevil an urban legend.
* Erick Avari as Nikolas Natchios: The father of Elektra, who is murdered by Bullseye on behalf of the Kingpin.
* Ellen Pompeo as Karen Page: The secretary at Matt Murdock and Franklin Nelsons law firm.
* Derrick OConnor as Father Everett: A priest of Matt Murdocks local church, where he sometimes goes to find solace.

Stan Lee, Frank Miller, and Kevin Smith, each notable for their work on the Daredevil comics, also have cameo roles throughout the film.  The directors cut version also features Jude Ciccolella and Coolio in a sub-plot removed from the theatrical version. Tanoai Reed appears uncredited as a thug in Josies Bar.

==Production==

===Development=== Marvel Enterprises, Chris Columbus Disney began negotiations in order to acquire the rights. In 1999, the negotiations failed to work out so Marvel set the project up with Sonys Columbia Pictures.    During this time, Chris Columbus and Carlo Carlei co-wrote a script together,  before Mark Steven Johnson got signed to write the screenplay. By 2000, Sony decided to cancel the project,  as the two companies reportedly could not come to an agreement over Internet rights. 
 New Regency entered negotiations with a more satisfying offer, attaining the character rights from Marvel in 2000 to produce the film, with 20th Century Fox handling the distribution.  Mark Steven Johnson was rehired and his script was turned in during 2001, which was praised by Aint It Cool News Harry Knowles.   Prior to shooting, producer Gary Foster said that in comparison to other comic book-based films before it, this film would be "more character-driven ... darker ... edgier,"  while Marvel Studios executive Kevin Feige felt the script was one of the strongest comic scripts Marvel had received. 

===Filming=== The Sum of All Fears) they were able to change the studios mind. 

  graphic novel, and the film. Several scenes were taken shot-for-shot of comic books.]]
When the look of the film was being decided, Mark Steven Johnson opted to use direct scenes from the Daredevil comics. Joe Quesadas artistic take in   (Daredevil vol. 2, #1-#8) was an influence on the film, with Johnson noting that they would "literally take out a scene from the comic book that Joe did   Heres Daredevil on the cross, you know, its that scene from Guardian Devil. You just shoot that."  Throughout the film, Ben Affleck had to wear "cataract milky-blue" contacts, which would effectively make him blind. This was considered great by Johnson, as it would aid his performance.   
 CGI except in a scene where Elektra and Matt Murdock are in the rain, which was done using CGI over film. 

===Music=== score to gothic and motifs but Incubus collaborated with Revell for any additional rock elements. Revell thoroughly enjoyed working with Johnson, describing the director as "positive" and "responsive" when it came to experimenting (as opposed to feeling "locked in a box of preconceptions") which he felt lead to "cool stuff". Varèse Sarabande put together the score record. 

==Release==

===Marketing===
Aside from expected TV commercials during such TV hits as  , was released in February 2003. 

===Box office=== Old School by $639,093.  By the third weekend release, Daredevil saw a further 38.5% drop in sales, and so fell to third place at the box office.  The film grossed over $102 million in North America, and over $76 million in the rest of the world, totalling the films worldwide takings at just over $179 million, grossing over double its budget of $78 million.  Avi Arad addressed the top spot success by saying "we are five for five with record-breaking box office successes   and have two more Marvel releases slated for this summer  . Its a testament to the broad appeal of these characters before mainstream audiences outside of the core comic fans. These super heroes have been successful within the Marvel pantheon for decades; it only makes sense that their translations to the big screen are just as fruitful." 

Due to the films violent scenes, Daredevil was banned in   for "having scenes containing violence using weapons and hands," a direct quotation of which the article cited as being another reason why Daredevil was banned. 

===Reception=== Film critic Roger Ebert gave the film 3 out of a possible 4 stars and called the film good, despite noting the almost typical superhero background. Of the actors, he stated that Ben Affleck and Jennifer Garner were suitable for their roles, while Michael Clarke Duncans presence alone was menacing. He finished by saying "I am getting a little worn out describing the origin stories and powers of superheroes   Some of their movies, like this one, are better than others."  Houston Chronicles Bruce Westbrook considered it "the best Marvel movie to date, its as well-written and character-driven as some of todays Oscar contenders, and its story doesnt stall with hollow flamboyance."  The Austin Chronicles Kimberly Jones praised the film, the actors, and felt that though an unproven director, "Johnson has just signed his meal ticket with this marriage of big brains, big brawn, and–most happily–big heart." 

Empire (film magazine)|Empires Kim Newman gave the film 3 out of 5 stars, and felt people "will like the characters more than the film," before adding that there are enough strong moments to guarantee a good viewing.  The Guardian s Peter Bradshaw stated that the film held "unconvincing touches" but was more enjoyable than Spider-Man (2002 film)|Spider-Man and as dark as Tim Burtons Batman (1989 film)|Batman.  BBC film critic Nev Pierce believed the film had spectacular set-pieces, but felt there was no strong narrative arc to keep the viewer interested.  The Wall Street Journals Joe Morgenstern considered the film to be neither original nor great but felt it maintained "many grace notes and interesting oddities."  The Globe and Mails Rick Groen said the film was "not woeful, not wonderful, merely watchable."  The Philadelphia Inquirers Steven Rea thought the film brought a variance of humour and violence, but felt it didnt work as well as it could have.  James Berardinelli felt the film was merely a satisfactory superhero film.  TV Guides Frank Lovece noted that for a superhero film the characters are more grounded to reality, respectively, but also felt it was "a movie for grown-ups, not kids." 

Particularly negative reviews included that of  s Michael Wilmington thought the film grabs the attention, but felt it does not reward it.  The New York Posts Lou Lumenick panned the film, describing it as a "mind-numbing, would-be comic-book franchise, which often seems as blind as its hero -- not to mention deaf and dumb."  Character co-creator Stan Lee himself felt, " hey just wrote the whole thing wrong. They made him too tragic."  Ben Affleck won the Golden Raspberry Award for Worst Actor for his work in the movie, as well as for Gigli and Paycheck (film)|Paycheck.

===Directors cut=== DTS and Dolby Digital sound. The new version of the film has newly recorded commentary to accompany it, featuring Mark Steven Johnson and Avi Arad. A "Making of Directors Cut" featurette also accompanied the film. The release date of the DVD was later pushed back to November 30, 2004.     On September 30, 2008 the directors cut was released on Blu-ray Disc|Blu-ray.  The DVD release of the directors cut removed the wealth of bonus material included on the theatrical cuts DVD release, but it was restored for the Blu-ray release (although the Blu-ray release only contains the directors cut).
 Greg Cox, published in 2003.
 Frank Miller Catholic upbringing. On the whole they felt the film would be far more pleasing to the fans, and overall better than the theatrical release. 

==Reboot==
  Born Again storyline, as well as suggesting Mister Fear|Mr. Fear as a possible villain. 

During 2004,  , set to be released in 2016. 

In July 2006, Michael Clarke Duncan showed interest in returning for the role of the Kingpin, but stated that he would not be willing to gain weight as he felt "comfortable" being down to 270 pounds. However, he jokingly showed willingness to change his mind if he was offered $20 million. Duncan suggested that the character is portrayed to have been training a lot in jail in order to become faster in combat against Daredevil, also working as a way to fit his weight loss into the story. 
Duncan would later go on to reprise his role as the Kingpin in an episode of the animated series:  .
 Frank Miller Tom Rothman reboot is Chris Nolan Deadline Hollywood New Regency News Corp, with Vice President Peter Chernin producing and David Scarpa writing the script. Intentions were said to move forward so that the film rights to the character would not revert to Marvel Entertainment|Marvel.  On March 15, 2011, it was announced that filmmaker David Slade would be directing the reboot,   but he later had to drop out due to other obligations. Fringe (TV series)|Fringe writer and producer Brad Caleb Kane was hired to pen the Slade-directed film.  On September 3, 2012, Duncan died, precluding him from further reprising his role as Kingpin.    

Later, it was announced that should a sequel or reboot not start filming by 10 October 2012, the rights to the Daredevil franchise would revert from Fox back to Marvel. In early August 2012, Fox scrambled to find a replacement for David Slade, who dropped out of the directors chair due to scheduling conflicts. The studio briefly met with Joe Carnahan, for the job—however, Carnahan said on Twitter that his pitch, described as a hard-boiled 70s thriller, had gone up in smoke. Several sources commented that Fox had given up on the reboot, and were prepared to let the rights revert to Marvel and their parent company, The Walt Disney Company.  On April 23, 2013, Kevin Feige confirmed that the rights for Daredevil returned to Marvel Studios and Disney, opening the possibility of including the character into the Marvel Cinematic Universe.  That speculation was confirmed with the announcement of an original Netflix Daredevil (TV series)|Daredevil television series, which premiered on the streaming service in April 2015. 

==References==
 
*  

==External links==
 
*  
*  
*  
*  
*  
*  
*   at Marvel Entertainment|Marvel.com

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 