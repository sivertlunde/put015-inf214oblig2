Midnight Crossing
{{Infobox film
| name           =  Midnight Crossing
| image          = 
| caption        = 
| writer         = 
| starring       = Faye Dunaway Kim Cattrall
| director       = Roger Holzberg
| producer       = 
| distributor    = 
| released       =  
| runtime        = 114 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Midnight Crossing is a 1988 drama film starring Faye Dunaway.

==Plot==
What begins as a pleasure cruise turns out to be a treasure hunt for two couples, sight-impaired Helen Barton and her husband, Morely, whos a former naval officer, and Jeff and Alexa Schubb.

A million dollars supposedly is buried on a small isle between Florida and Cuba, so the four of them decide to go after it. Their lives become in danger from natives and pirates, as well as from the greed that overwhelms their group.

==Cast==
* Faye Dunaway as Helen Barton
* Daniel J. Travanti as Morely Barton
* Kim Cattrall as Alexa Schubb John Laughlin as Jeff Schubb
* Ned Beatty as Ellis

==Reception==
The movie received mixed reviews.   

==Box Office==
Midnight Crossing was not a success. In fact, the producers owed money to cinema companies because of the films commercial failure. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 

 