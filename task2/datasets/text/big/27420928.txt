Dr. O'Dowd
 
 
{{Infobox film
| name           = Dr. ODowd
| image          = 
| image_size     =
| border         =
| alt            =
| caption        =
| director       = Herbert Mason
| producer       = Sam Sax
| writer         =
| screenplay     = Austin Melford Derek Twist
| story          =
| based on       =  
| narrator       =
| starring       = Shaun Glenville Peggy Cummins Felix Aylmer Irene Handl
| music          =
| cinematography = Basil Emmott
| editing        =
| studio         = Warner Bros.
| distributor    =
| released       =  
| runtime        = 76 mins
| country        =
| language       = English
| budget         =
| gross          =
}}
 British drama 75 Most Wanted" lost films.

==Plot==
Marius ODowd (Shaun Glenville) is an Irish doctor who is often drunk. His daughter-in-law Moira (Pamela Wood) dies during a serious operation which ODowd is performing. Although ODowd is not to blame, his son Stephen (Liam Gaffney) suspects that Moira died due to ODowd operating while under the influence of alcohol, and accuses him of criminal neglect. ODowd consequently has his license to practice medicine taken away. Stephen also does not tell his daughter Pat (Peggy Cummins) that Marius is her grandfather, although several years later she becomes friends with Marius and works this out. Marius eventually manages to redeem himself by saving Stephens life during an outbreak of diphtheria. 

==Cast==
* Shaun Glenville – Marius ODowd
* Peggy Cummins – Pat ODowd
* Mary Merrall – Constantia
* Liam Gaffney – Stephen ODowd
* Patricia Roc – Rosemary
* James Carney – OHara
* Felix Aylmer – President
* Irene Handl – Sarah
* Walter Hudd – Doctor Crowther
* Pat Noonan – Mulvaney
* Maire ONeill – Mrs Mulvaney
* Charles Victor – Dooley
* Pamela Wood – Moira

==Production==
Most of the filming for Dr. ODowd took place at the Warner Bros. studios in Teddington, with outdoor sequences shot in Cumberland in north west England and in County Wicklow, Ireland. The filming was undertaken in the summer of 1939, concluding just after the start of World War II. Warner Bros. employed a number of different experts as advisers to ensure the film was realistic, including a doctor, a nurse, an angler and a billiards player. 

Dr. ODowd was the film debut for 13-year-old Peggy Cummins, who later starred in films such as Gun Crazy and The Night of the Demon.     Cummins had caught the attention of film executives after appearing in a 1938 production named Lets Pretend in London, and this film was the beginning of a deal signed with Warner Bros.    As part of an agreement with the London County Council, Cummins was limited to five house of filming per day and had to be supervised by a governess.  The film was also a debut for her co-star Shaun Glenville, a music hall performer.    This was only one of two film roles Glenville played in his career. 

==Release and later history==
Dr. ODowd was distributed by the production studio Warner Bros. in 1940, with an initial trade showing in London on 16 January 1940. It had a public release in UK and Ireland in the same year. It was also distributed in America  and Australia.   
 75 Most Wanted" lost films.    No sequences of film are known to survive, although the BFI does possess a collection of stills from the production. 

==Reception==
The film was received very positively by critics. One Irish newspaper described it as "one of the best films about Ireland ever made."    described it as "one of the surprise hits of the year".  Praise was particularly given to the two main actors, Shaun Glenville and Peggy Cummins. Despite the latters young age, Kinematograph Weekly predicted that "Given the chance, shell go far". 

Halliwells Film Guide describes it as a "somewhat woebegone tearjerker with an interesting cast". 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 