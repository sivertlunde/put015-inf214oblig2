Men Must Fight
{{Infobox film
| name           = Men Must Fight
| image          = MMFight2.JPG
| image_size     =
| caption        =
| director       = Edgar Selwyn
| producer       =
| writer         = Reginald Lawrence (play) S. K. Lauren (play) C. Gardner Sullivan
| narrator       =
| starring       = Diana Wynyard Lewis Stone Phillips Holmes
| music          =
| cinematography = William S. Gray
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Eurasian States in 1940", that belief is put to the test.    


==Plot== Robert Young) fall in love after only knowing each other for a few days. Tragically, he is brought to her hospital and, by chance, put under her care after being fatally wounded on his very first mission. After he dies, Laura realizes she is pregnant. Edward Seward (Lewis Stone) loves her and persuades her to marry him. As far as anyone knows, the child will be his.

By 1940, Lauras son Bob has grown into a young man, newly engaged to Peggy Chase. Laura has raised Bob to embrace pacifism. Meanwhile, Edward Seward, now United States Secretary of State, flies home after having negotiated the Seward Peace Treaty, which he claims will make it impossible for any country to go to war again. However, when the U.S. ambassador to the state of "Eurasia" is assassinated while en route to the Eurasian State Department to discuss an earlier diplomatic incident, the President sends the navy across the Atlantic to underscore the U.S. demand for a formal apology. Eurasia refuses to comply, and another world war becomes inevitable despite the treaty.

Laura speaks at a large peace rally, over her husbands strong objection. The rally is broken up a group of angry men. A mob then gathers at the Seward home and starts pelting the place. Edward manages to disperse the crowd by first reminding the mob of each Americans right to voice his or her own opinion in peacetime, and pledging himself wholeheartedly to the struggle once war is declared. When a news reporter interviews him, he insists his son will enlist. Bob categorically denies this, causing Peggy to break off their engagement. Unable to get his son to change his mind, Edward tells him that he at least has no right to sully the Seward name, revealing that he is not Bobs father. Laura confirms it, and tells Bob of his real father and how he died.

War breaks out. Privately, Edward informs his wife that the war is going badly because America fell behind during the years of peace; the "Panama Canal|Canal" has been captured by the enemy, and 12,000 U.S. troops killed in two days by enemy gas bombs. When Eurasia launches an air raid on New York City, destroying such landmarks as the Empire State Building and the Brooklyn Bridge, hundreds are killed and Laura is injured, though not seriously. Bob changes his stance and enlists, not in the chemical division as a trained chemist as Edward had suggested, but as an aviator like his real father. Bob and Peggy marry, then he departs with his squadron. As she watches Bobs squadron fly over the city, Laura now understands that freedom is not free; that we must always be prepared to safeguard it; and we all have a responsibility to defend it.

==Cast==
As appearing in screen credits (main roles identified): 
* Diana Wynyard as Laura Mattson Seward
* Lewis Stone as Edward "Ned" Seward
* Phillips Holmes as Bob Seward
* May Robson as Maman Seward
* Ruth Selwyn as Peggy Chase, Bobs fiancée Robert Young as Lt. Geoffrey Aiken
* Robert Greig as Albert, the Sewards butler
* Hedda Hopper as Mrs. Chase, Peggys mother
* Don Dillaway as Lt. Steve Chase, Peggys brother (as Donald Dilloway)
* Mary Carlisle as Evelyn, Steves fiancée
* Luis Alberni as Soto, the Sewards cook, who quits so he can go home and fight for the other side

==Production==
 
Men Must Fight, a.k.a. "What Women Give" in its working title, was in production from mid-December 1932, and reflected the unstable period between the wars. In creating a realistic scenario for a war in 1940, the future included television and video telephones. Eames 1982, p. 91.  Based on a Broadway production that premiered on October 14, 1932, some of the staginess of the play was reflected in its set. Carr, Jay.   Turner Classic Movies. Retrieved: May 28, 2013. 

==Reception==
In reviewing Men Must Fight, Mordaunt Hall, the film critic of The New York Times, praised the performances of two of the leads, writing that "Miss Wynyard gives an affecting and very earnest interpretation of a mother who protests vociferously against war ... Mr. Stone does splendidly in his part", while "Phillips Holmes is not especially effective".  In a later review, Leonard Maltin noted the prophetic nature of the plotline, "This powder keg of a film is fascinating on many levels, particularly as it looks into the future and uncannily depicts the world at war and the mainstream popularity of television."   American film critic Wheeler Winston Dixon noted that the film "predicted with eerie accuracy the outbreak of war in 1940", and that it depicted men as being "unreasonable aggressors by reason of their gender alone."  Wheeler Winston Dixon, 2003, Wallflower Press, London and New York, Visions of the Apocalypse: Spectacles of Destruction in American Cinema, Retrieved November 28, 2014, ISBN 1-903364-74-4 (paperback) ISBN 1-903364-38-8 (hardcover), see page 122, bottom paragraph 

==References==
===Notes===
 
===Bibliography===
 
* Eames, John Douglas. The MGM Story: The Complete History of Fifty Roaring Years. London: Octopus Books Limited, 1982, First edition 1979. ISBN 978-0-51752-389-6.
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
   
 
 