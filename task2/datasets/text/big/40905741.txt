Last Weekend (film)
{{Infobox film
| name           = Last Weekend
| image          =
| caption        =
| director       = Tom Dolby Tom Williams
| producer       = Mike S. Ryan
| writer         = Tom Dolby
| starring       = Patricia Clarkson
| music          = Stephen Barton
| cinematography = Paula Huidobro
| editing        = Michael R. Miller David Gray
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $26,111 
}} Joseph Cross. The film premiered at the San Francisco International Film Festival on May 2, 2014. In May, the film was acquired for theatrical release and iTunes/VOD on August 29, 2014 by IFC/Sundance Selects.  The film will also open the Provincetown International Film Festival on June 18, 2014.  The movie was filmed entirely on location in Lake Tahoe, California. It was the first feature film in thirteen years to be shot entirely in the area. 

== Synopsis ==
When an affluent matriarch (Patricia Clarkson) gathers her dysfunctional family for a holiday at their Northern California lake house, her carefully constructed weekend begins to come apart at the seams, leading her to question her own role in the family. 

== Cast ==
{| class="wikitable"
|-
! Actor
! Role
|- Patricia Clarkson || Celia Green
|- Zachary Booth || Theo Green
|- Joseph Cross Joseph Cross || Roger Green
|- Chris Mulkey || Malcolm Green
|- Devon Graye || Luke Caswell
|- Alexia Rasmussen || Vanessa Sanford
|- Rutina Wesley || Nora Finley-Perkins
|- Jayma Mays || Blake Curtis
|- Judith Light || Veronika Goss
|- Julio Oscar Mechoso || Hector Castillo
|- Mary Kay Place || Jeannie
|- Sheila Kelley Sheila Kelley || Vivian
|- Julie Carmen || Maria Castillo
|- Fran Kranz || Sean Oakes
|- Ray OBrien || Paramedic
|} 

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 


 