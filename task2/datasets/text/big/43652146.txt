Jusqu'au bout de la nuit
{{Infobox film
| name           = Jusquau bout de la nuit
| image          = 
| caption        = 
| director       = Gérard Blain
| producer       = Frédéric Marboeuf
| writer         = Gérard Blain Marie-Hélène Bauret
| starring       = Gérard Blain Anicée Alvina
| cinematography = Daniel Gaudry
| music          = Tarik Benouarka
| editing        = Catherine Taieb
| studio         = DB Films
| distributor    = Pathé|Pathé Distribution
| released       =  
| runtime        = 80 minutes
| country        = France
| language       = French
| budget         = 
}}
Jusquau bout de la nuit ("until the end of the night") is a 1995 French drama film directed by Gérard Blain, starring Blain and Anicée Alvina. It tells the story of a man in his sixties who is released after a long prison sentence, and tries to have a relationship with a younger woman, while still determined to live outside the law. Blain described the film as "not a gangster film, but a love film and a relentless tragedy".    Sylvia Jeanjacquot, the widow of the criminal Jacques Mesrine, appears in a minor role.

The film was shot in December 1994 in Lyon, Villeurbane, Vénissieux and Sathonay. It was shown at the Montreal World Film Festival and Cairo International Film Festival in 1995. It was released on France on 13 September 1995.  

==Cast==
* Gérard Blain as François 
* Anicée Alvina as Maria 
* Gamil Ratib as Rousseau 
* Paul Blain as Christian 
* Frédéric Marboeuf as Serge
* Pierre Blain as Benoît
* Sylvia Jeanjacquot as the lawyer

==Reception==
 , admittedly referenced by Blain throughout his whole filmography." Père continued: "With its rigorous aesthetics and moral, Jusqu’au bout de la nuit distinguishes itself from all and sundry of the French cinematic output of its time. But it is also a (politically) incorrect and disturbing film, charged with a restrained and subversive violence." 

==References==
 

 
 
 
 
 