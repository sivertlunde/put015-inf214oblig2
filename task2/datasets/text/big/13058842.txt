Violanchelo
{{Infobox film
| name = Violanchelo
| image = Violanchelo.jpg
| caption =  
| director = Alfonso Pineda Ulloa
| writer = Alex Marino (screenplay)   Blas Valdez (story) 
| starring = Bárbara Mori Leonardo Sbaraglia Tony Dalton Irene Azuela
| cinematography = 
| distributor = Panamax (United States) Videocine  (Mexico)
| language = Spanish
| released =  
| country = Mexico
| budget =  
}} 2008 drama film starring Bárbara Mori, Leonardo Sbaraglia and Tony Dalton. The film is based on a romantic story.

==Synopsis==
In this thriller, Consuelo (Bárbara Mori) is in love with the man of her dreams.  There is one problem... he only exists in her dreams.  But she knows he is real and to prove it, she goes to the police claiming rape.   A police sketch artist creates a portrait of him.  The next day she gets a call.  They have found him.  She goes to the station and there he is, in a police line-up:  the man of her dreams—in the flesh.  But she soon learns how quickly dreams can turn into nightmares.

== External links ==
*  
*    
*  

 
 
 
 


 
 