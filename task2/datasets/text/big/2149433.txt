On the Ropes
 
{{Infobox film
| name           = On the Ropes
| image          =
| image_size     =
| caption        =
| director       = Nanette Burstein Brett Morgen
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography = Brett Morgen
| editing        = Nancy Baker Nanette Burstein
| distributor    =
| released       = 1999
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
 American 1999 documentary film directed by Nanette Burstein and Brett Morgen. This film follows the career of three young boxers and their coach.

==Awards==
On the Ropes won many awards:
* Jury Prize, Urbanworld Film Festival, Best Documentary, 1999
* Special Jury Prize, Sundance Film Festival, Documentary, 1999
* Silver Spire, San Francisco International Film Festival,  Film & Video - Society and Culture-U.S., 1999
* IDA Award, International Documentary Association,  Feature Documentaries, 1999
* DGA Award, Directors Guild of America,  Outstanding Directorial Achievement in Documentary, 2000 SFFCC Awards, Santa Fe Film Critics Circle Awards, Best Documentary, 2000

The movie was also nominated for:
* Grand Jury Prize, Sundance Film Festival, Documentary, 1999
* Oscar, Academy Awards, Best Documentary, Features, 2000    

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 