Old Mother Riley in Business
{{Infobox film
| name           = Old Mother Riley in Business
| image          = "Old_Mother_Riley_in_Business"_(1941).jpg
| image_size     =
| caption        = Trade ad poster John Baxter  John Baxter
| writers        = Geoffrey Orme  Con West
| story          = Roger Clegg Barbara K. Emary
| narrator       =
| starring       = Arthur Lucan   Kitty McShane   Charles Victor Cyril Chamberlain 
| music          = Kennedy Russell James Wilson
| editing        = Michael C. Chorlton
| studio         = British National Films
| distributor    = Butchers Film Service
| released       = 12 April 1941 (UK)	
| runtime        = 
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} British comedy John Baxter and starring Arthur Lucan, Kitty McShane and Cyril Chamberlain.  It was the sixth in the long-running Old Mother Riley series of films. Old Mother Rileys pub faces competition from a large chain store nearby, causing her to declare war on it.

==Plot==
Old Mother Riley comes to the rescue of local shopkeepers after a ruthless chain, "Golden Stores" makes its aggressive presence felt. The boisterous Irish washerwoman gives the chain stores boss a push into the river and soon finds herself a wanted woman, donning a nurses outfit to escape from the hospital in which she is hiding. 

==Cast==
* Arthur Lucan ...  Mrs. Riley
* Kitty McShane ...  Kitty Riley
* Cyril Chamberlain ...  John Halliwell
* Ernest Butcher
* O. B. Clarence
* Edgar Driver
* Morris Harvey
* Roddy Hughes
* Ruth Maitland
* Edie Martin
* Wally Patch
* Ernest Sefton
* Charles Victor

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 