Sarah's Key
 
{{Infobox film
| name           = Sarahs Key
| image          = Sarahs key movie poster 300x400.jpeg
| alt            =  
| caption        = Theatrical release poster
| director       = Gilles Paquet-Brenner
| producer       = Stéphane Marsil
| writer         = Serge Joncour Gilles Paquet-Brenner
| starring       = Kristin Scott Thomas Mélusine Mayance Niels Arestrup Frédéric Pierrot
| music          = Max Richter
| cinematography = Pascal Ridao
| editing        = Hervé Schneid France 2 Cinema Canal+ TPS Star France Televisions Kinology Ile de France
| distributor    = Anchor Bay Entertainment The Weinstein Company UGC Madman Entertainment StudioCanal UK
| released       =  
| runtime        = 111 minutes
| country        = France
| language       = French English
| budget         = EU10,000,000  
| gross          = $21,118,093
}}
 novel with the same title by Tatiana de Rosnay. 
 French bureaucracy as well as French citizens hiding and protecting Sarah from the French authorities. 

The film alternates between Sarahs life in 1942 and the journalist researching the story in 2009.

==Plot==
In 1942, 10-year-old Sarah Starzynski (Mélusine Mayance) hides her younger brother from French police by locking him in a secret closet and telling him to stay there until she returns. She takes the key with her when she and her parents are transported to the Vélodrome dHiver, where they are held in inhuman conditions by the Paris Police and French Secret Service.

The deportees are transferred to the French-run Beaune-la-Rolande internment camp. The adults are deported to Auschwitz, leaving the children in the camp. When Sarah tries to escape with a friend, Rachel, a sympathetic Paris police guard spots them.  When Sarah begs him to let them go so she can save her brother, he hesitates then lifts the barbed wire to let them out.

Sarah and Rachel fall asleep in a dog house at a farm where they are discovered by the farmers, Jules and Genevieve Dufaure. Despite knowing who they are and the associated danger, the Dufaures decide to help the girls. Rachel is dying, and when they call attention to themselves by calling in a doctor, a skeptical German officer asks them if they know anything about a second Jew child.  The officer begins a search for the second child, only to be interrupted when the French physician carries out the dead body of Rachel.  Days later, the Dufaures take Sarah back to her familys apartment building in Paris. Sarah runs up to her apartment, knocking on the door furiously. A boy, twelve years old, answers. She rushes in to her old room and unlocks the cupboard. Horrified by what she finds, she starts screaming hysterically.

After the war, Sarah continues to live as a family member with the Dufaures and their two grandsons.  When she turns 18, she moves to the United States, hoping to put everything that happened behind her.  She stops corresponding with the Dufaures when she gets married and has a son, William. When her son is nine, Sarah – still despondent and blaming herself for her brothers death – commits suicide by driving her car into the path of an on-coming truck. Its explained to her son that her death was an accident.

In the present, the French husband of journalist Julia (Kristin Scott Thomas) inherits the apartment of his grandparents (his elderly father was the boy who opened the door to Sarah in August 1942). Having previously done an article on the Vel dHiv Roundup, Julia finds her interest piqued when she learns that the apartment came into her husbands family at about the time of the Roundup, and she begins to investigate what happened nearly 70 years earlier. Her father-in-law tells Julia what he knows so she will quit prying.

Julia begins an obsessive quest to find any trace of Sarah, eventually learning of her life in Brooklyn and finally locating William in Italy. She meets with him and asks him for information about his mother, but learns to her surprise that William does not know his mothers history or even that she was a Jew, believing only that she had been a French farm girl. Listening in amazement, William rejects the story and dismisses Julia. Later, everything is confirmed to William by his dying father, Richard, including Sarahs suicide. He gives William Sarahs journals and notes, telling him Sarah had immediately had William baptized right after his birth, fearing that being Jewish was a threat to him and explaining that "... were all a product of our history". The key to the cupboard is among the items handed to him by his father.

Julia, having given up hope of having another child after years of unsuccessful attempts to conceive, discovers shes pregnant. Her husband loves their life with their 12-year-old daughter, Zoe, as it is, and does not want to have another child at this point in life. Julia ultimately decides against an abortion, has another daughter, divorces her husband, and eventually moves with Zoe to New York City.

Two years later, William, having contacted Julia, meets her for a late lunch in a restaurant favored by Sarah, and gives her additional information about his mother that the Dafaures had. Julia is amazed and happy for him, and has brought her young daughter along to the meeting. William breaks into tears when Julia tells him her daughters name is Sarah. Julia comforts him as they both look at little Sarah.

==Cast==
* Kristin Scott Thomas – Julia Jarmond
* Natasha Mashkevich – Mrs Starzynski
* Arben Bajraktaraj – Mr Starzynski
* Mélusine Mayance – young Sarah Starzynski
* Charlotte Poutrel – Adult Sarah
* Niels Arestrup – Jules Dufaure
*   – Geneviève Dufaure
* Frédéric Pierrot – Bertrand Tezac
* Michel Duchaussoy – Édouard Tezac
*   – Mamé Tezac
* Aidan Quinn – William Rainsferd
* George Birt – Richard Rainsferd

==Release==
The film had a preview at the Toronto International Film Festival on 16 September 2010, then it had a wide release in France on 13 October 2010 and in Italy on 13 January 2012.

==Reception==
 

The film has been critically well-received,   currently holding a 73% rating on the film review aggregate site Rotten Tomatoes.  The film recorded 1,635,278 admissions in Europe. 

Although British, Scott-Thomas delivers her English dialogue in an American accent, but for most of the film she speaks fluent French as she has lived in France for many years. She has done many Anglo-French movies in French and received a César Awards 2011|César Award nomination for this performance.

==Home video==
The film was released in the US on DVD and Blu-ray Disc on 22 November 2011.

==See also==
* The Holocaust in France

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 