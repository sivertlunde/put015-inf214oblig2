Prom Wars
{{Infobox Film
| name = Prom Wars
| image = PromWars.jpg
| caption = Theatrical release poster
| director = Phil Price
| producer =  Brandi-Ann Milbradt
| writer = Myles Hainsworth 
| starring = Raviv Ullman Alia Shawkat Rachelle Lefèvre Nicolas Wright
| distributor = 
| released =  
| Status = Completed
| runtime =
| country = Canada
| awards = English
| budget = $3.5 million CDN 
}}

Prom Wars is a comedy featuring Raviv Ullman, Alia Shawkat, Rachelle Lefèvre, and Nicolas Wright.

== Cast ==
*Raviv Ullman as Percy Collins
*Alia Shawkat as Diana Riggs
*Rachelle Lefèvre as Sabina
*Nicolas Wright as Joseph
*Kevin Coughlin as Geoffrey
*Chad Connell as Rupert
*Jesse Rath as Francis
*Yann Bernaquez as Hamish
*Cory Hogan as Jasper
*Noah Bernett as Kyle
*Alexandra Cohen as Maggie
*Meaghan Rath as Jen L.
*Mélanie St-Pierre as Jen Bergman
*Daniel Rindress-Kay as Northrop
*Keenan MacWilliam as Meg

==Plot==
The graduating class at Miss Aversham and Miss Cronstalls School for Girls find that they have - in defiance of the natural laws of probability - all blossomed simultaneously. Capitalizing on their unique status, and intent on teaching high school boys to NEVER take girls for granted, they issue a challenge to the boys of Easthills rival private schools, Selby and Lancaster. The winner in a series of designated competitions will be awarded exclusive rights to the girls as prom dates. Like the capricious and meddlesome gods of Greek mythology, the ACS girls pit the boys schools against each other in a (secret) Prom War.

==References==
 

==External links==
*  

 
 
 
 
 