Lady in a Cage
{{Infobox Film
| name           = Lady in a Cage
| image          = Lady in a Cage - 1964- poster.png
| caption        = 1964 Theatrical poster
| director       = Walter Grauman 
| producer       = Luther Davis
| writer         = Luther Davis James Caan
| music          = Paul Glass      
| cinematography = Lee Garmes
| editing        = Leon Barsha	
| distributor    = Paramount Pictures
| released       =  
| runtime        = 94  min
| country        = United States English
| budget         = 
| gross = $1,650,000 (US/ Canada) 
| preceded_by    = 
| followed_by    = 
}}
  American psychological James Caan in his first substantial film role.

==Plot== alcoholic derelict, George, who enters the home, ignores her pleas and steals some small items.  The wino sells them to a fence, then visits his prostitute friend, Sade, and tells her of the treasure trove he has stumbled upon.  The expensive goods George fenced attracts the attention of three young hoodlums, Randall, Elaine and Essie.  The trio follows George and Sade back to the Hilyard home, where they conduct an orgy of violence, killing George the wino and locking Sade in a closet.  Randall then pulls himself up to the elevator and taunts Mrs. Hilyard with a note left behind by her son Malcolm, in which he threatens suicide because of her domineering manner.  Shocked by the revelation, Mrs. Hilyard struggles with Randall, escapes the elevator, and crawls out of the house.  Randall follows and, as he is attempting to drag her back inside, Mrs. Hilyard gouges his eyes and escapes his clutches.  The blinded assailant stumbles into the street and is run over by a passing automobile, whereupon police arrive to arrest the surviving intruders and comfort the victim.

==Cast==
*Olivia de Havilland as Mrs. Cornelia Hilyard James Caan as Randall Simpson OConnell
*Jennifer Billingsley as Elaine
*Jeff Corey as George L. Brady Jr. aka Repent
*Ann Sothern as Sade
*Rafael Campos as Essie
*William Swan as Malcolm Hilyard
*Charles Seel as Mr. Paul (Junkyard Proprietor)
*Scatman Crothers as Junkyard Proprietors Assistant
*Richard Kiel as Pawn shop strongman (uncredited)
*Ron Nyman as Neighbor (uncredited)

==See also==
* List of American films of 1964
* List of films featuring home invasions
* Psycho-biddy

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 