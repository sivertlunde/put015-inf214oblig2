Purple Haze (film)
{{Infobox film
| name           = Purple Haze 
| image          = 
| caption        = 
| director       = David Burton Morris
| writer         = David Burton Morris Victoria Wozniak 
| producer       = Thomas A. Fucci (producer) (as Thomas Anthony Fucci) Victoria Wozniak (executive producer) Peter Nelson Chuck McQuary Bernard Baldan Susanna Lack Bob Breuler
| music          = 
| cinematography = Richard Gibb
| editing        = Dusty Dennison
| studio         = Triumph Films 
| distributor    = Purple Haze
| released       = October, 1982
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}} 1982 dramedy Vietnam in the summer of 1968.

== Plot summary ==
The film opens in Princeton University, 1968, where Matt Caulfield and his friends are watching television. There, they witness President Lyndon B. Johnson inform us of his plans not to rerun in the upcoming election. Upon hearing of his plans, Matt and the rest of the students celebrate by smoking marijuana. An uncool student from next door is disturbed by the boys racket, and upon being pelted with junkfood by the boys for telling them to be quiet, he calls the police. Within minutes, Matt and his roommates are caught smoking, and are banned from college campus. Matt returns home to his family, where he is faced with various issues before being shipped off to Vietnam.

== Reception ==
Despite receiving positive reviews in some mainstream publishings, Purple Haze was a huge flop, and faded into obscurity shortly after its release. The director, David Burton Morris, offers DVD copies of the film from his e-mail address, DGA123@AOL.com.

== Soundtrack == Heavy Metal faced similar issues that were resolved in the late 1990s when the film finally received a proper DVD release.
*"When I Was Young" by The Animals For What Its Worth" by Buffalo Springfield Expecting to Fly" by Buffalo Springfield Cream
*"So You Want to Be a Rock n Roll Star" by The Byrds
*"The "Fish" Cheer/I-Feel-Like-Im-Fixin-To-Die Rag|I-Feel-Like-Im-Fixin-to-Die Rag" by Country Joe and the Fish
*"Eight Miles High" by The Byrds Magic Carpet Steppenwolf
*"White White Rabbit" by Jefferson Airplane Embryonic Journey" by Jefferson Airplane
*"A Whiter Shade of Pale" by Procol Harum
*"A Salty Dog" by Procol Harum
*"Runaway (Del Shannon song)|Runaway" by Del Shannon Everyday People" by Sly and the Family Stone
*"Darkness, Darkness" by The Youngbloods Get Together" by The Youngbloods
*"Purple Haze" by The Jimi Hendrix Experience
*"Foxy Lady" by The Jimi Hendrix Experience
*"Are You Experienced?" by The Jimi Hendrix Experience The Star-Spangled Banner" by The Jimi Hendrix Experience

==References==
 
 

==External links==
* 
* 

 
 
 