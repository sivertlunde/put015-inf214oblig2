Dushmani
{{Multiple issues|
 
 
}}

{{Infobox film
| name           = Dushmani
| image          = MithunDushmani.jpg
| alt            =  
| caption        = DVD Cover
| director       = Imran Khalid
| producer       = N Kumar
| writer         = Meeraq Mirza
| starring       = Mithun Chakraborty Armaan Kohli Faisal Khan Puru Raajkumar Raj Babbar Hemant Birje
| music          = Nikhil-Vinay
| cinematography = Venkatesh
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 125 minutes
| country        = India
| language       = Hindi Rs 5 Crores
| gross          = 
}}
Dushmani is a 2009 Hindi language|Hindi-language Indian film directed by Imran Khalid, starring Mithun Chakraborty, Armaan Kohli, Faisal Khan and Puru Raajkumar

==Plot==
A revenge tale, and Mithun heading the cast with his Juniors, Armaan Kohli, Faisal Khan and Puru Rajkumar. This film was shelved/delayed and yet to release.

==Cast==
* Mithun Chakraborty
* Armaan Kohli
* Faisal Khan
* Puru Raajkumar
* Vikranta Sandhu

==Soundtrack==
{{Infobox album Name     = Dushmani Type     = film Cover    =  Released =  Artist   = Nikhil Vinay Genre  Feature film soundtrack Length   = 21:45 Label    = 
}}

{| class="wikitable"
| No. || Song || Singers ||Ref 
|-
| 1 || Deewangi Hamari... || Anuradha Paudwal, Abhijeet Bhattacharya ||  
|-
| 2 || Tu Mere Sale Ki Sister... || Khamosh, Pamela Jain, Udit Narayan ||  
|-
| 3 || Teri Aashiqui Mein Dil... || Anuradha Paudwal, Udit Narayan ||  
|-
| 4 || Main Mast Malang... || Sunidhi Chauhan, Sukhwinder Singh ||  
|-
| 5 || Lehnge Se Lage Hawa... || Poornima ||  
|}

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 