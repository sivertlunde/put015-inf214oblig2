High Treason (1951 film)
 
 
 
{{Infobox Film |
  name     = High Treason |
  image          = |
  caption = | Frank Harvey |
  starring       = Liam Redmond Anthony Bushell André Morell |
  director       = Roy Boulting |
  editing        = Max Benedict |
  producer       = Paul Soskin |
  cinematography = Gilbert Taylor |
  distributor       = Peacemaker Pictures |
  released   = 20 May 1952 |
  runtime        = 90 min. |
  language = English |
  music          = John Addison |
  awards         = |
}} Frank Harvey, Superintendent Folland Scotland Yards Special Branch from the first film, though in High Treason he is subordinate to the head of Special Branch, Commander Robert "Robbie" Brennan, played by Liam Redmond.

==Plot==
Enemy saboteurs infiltrate the industrial suburbs of London, intending to plant high-powered bombs at several factory sites. Their motivation is to cripple the British economy and enable subversive forces to insinuate themselves in the government. The saboteurs are thwarted not by the traditional counterintelligence agents but by workaday London police officers.

==Cast==
 
*Liam Redmond as Commander Robert Brennan
*André Morell as Superintendent Folland
*Anthony Bushell as Major Elliott
*Kenneth Griffith as Jimmy Ellis
*Patric Doonan as George Ellis
*Joan Hickson as Mrs. Ellis Anthony Nicholls as Grant Mansfield
*Mary Morris as Anna Braun
*Geoffrey Keen as Morgan Williams
*Stuart Lindsell as Commissioner John Bailey as Stringer
*Dora Bryan as Mrs. Bowers
*Charles Lloyd-Pack as Percy Ward
*Laurence Naismith as Reginald Gordon-Wells
 

==External links==
*  

 

 
 
 
 
 
 