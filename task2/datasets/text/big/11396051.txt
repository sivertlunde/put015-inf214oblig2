L'Atlantide (1921 film)
 
{{Infobox film
| name           = LAtlantide
| image          = Manuel Orazi - LAtlantide.jpg
| caption        = Poster by Manuel Orazi
| director       = Jacques Feyder
| producer       =
| screenplay     = Jacques Feyder
| based on       =  
| starring       = Jean Angelo Georges Melchior Stacia Napierkowska
| music          = M. Jemain (original score)
| cinematography = Georges Specht Victor Morin Amédée Morrin
| editing        =
| studio         = Thalman & Cie
| distributor    = Louis Aubert
| released       =  
| runtime        = original reported as 196 minutes; DVD of restored copy from Nederlands Filmmuseum 163 minutes
| country        = France
| language       = Silent film French intertitles
| budget         =
}} Pierre Benoit.

==Plot==
In 1911, two French officers, Capitaine Morhange and Lieutenant Saint-Avit, become lost in the Sahara desert and discover the legendary kingdom of Atlantis, ruled by its ageless queen Antinéa. They become the latest in a line of captives whom she has taken as lovers, and who are killed and embalmed in gold after she has tired of them.  Morhange however, already grieving for a lost love and planning to take holy orders, is indifferent to Antinéas advances and rejects her. Angered and humiliated, she exploits the jealousy of his friend Saint-Avit and incites him to kill Morhange. Appalled by what he has done, Saint-Avit is helped to escape by Antinéas secretary Tanit-Zerga, and after nearly dying in the desert from thirst and exhaustion, he is found by a patrol of soldiers.  Saint-Avit returns to Paris and tries to resume his life, but he is unable to forget Antinéa. Three years later he returns to the desert and sets out to find her kingdom again, accompanied by another officer to whom he has told his story.

Much of the narrative is contained within a long flashback as Saint-Avit recounts his first visit to Antinéa; other shorter flashbacks are used within this framework, creating a fairly complex narrative structure. 

==Cast==
  
*Jean Angelo as Capt. Morhange 
*Stacia Napierkowska as Queen Antinea 
*Georges Melchior as Lt. de Saint-Avit 
*Marie-Louise Iribe as Tanit-Zerga 
*Abd-el-Kader Ben Ali as Cegheir ben Cheik 
*Mohamed Ben Noui as Guide Bou-Djema 
*Paul Franceschi as Archivist 
*André Roanne as Segheïr ben Cheïkh 
*René Lorsay as Lt. Olivier Ferrières

==Production and distribution==
When Jacques Feyder obtained the rights to film Benoits novel, he took the radical step of insisting that the film should be made on location in the Sahara, a strategy which no film-maker had previously used for a project on this scale.  His whole cast and crew were taken to Algeria, first to the Aurès Mountains and then Djidjelli on the coast, for 8 months of filming.  Even the interiors were filmed in an improvised studio in a tent outside Algiers, with sets by the painter Manuel Orazi.  

Feyder initially borrowed production money from his cousin who was a director of  Banque Thalmann. By the time of the films release in October 1921, the costs had escalated to an unprecedented figure of nearly 2 million francs, and its financial backers rapidly sold their rights to the distributor Louis Aubert.  The film soon became a huge success however and earned a great deal of money for Aubert; it ran at a Paris cinema for over one year and was widely sold abroad.  Aubert re-released the film in 1928 and it had a renewed success. 

==Reception==
The celebrity of the source novel as well as the much-reported circumstances of the production ensured that the film received plenty of attention on its release. Despite the 3-hour running time and its sometimes slow pace, it proved enormously popular with the public and put Jacques Feyder into the front rank of French film-makers.  The critical reception of the film was more mixed, with particular objections made against the central performance by Stacia Napierkowska; she had been a dancer and well-known film actress for many years, but was now past her prime, and Feyder regretted engaging her to portray the captivating Antinéa, especially when he found that she had gained an inappropriate amount of weight.  However the undoubted success of the film was the grandeur of its locations and the photography of the desert landscapes. A much-quoted remark by Louis Delluc was not wholly sarcastic: "There is one great actor in this film, that is the sand". 
 Le Grand La Bandera (1935).

==Preservation status==
A DVD version of the film was released by Lobster Films/MK2 in 2004, based on a restored copy at the Nederlands Filmmuseum in Amsterdam. This reveals the very high quality of the films photography, and it includes a detailed scheme of colour tinting throughout the print. Its running time is about 30 minutes shorter than the reported length of the original.  It has a new musical soundtrack by Eric Le Guen.  It was released on DVD by Home Vision Entertainment in 2006. 

==Alternative titles==
*Lost Atlantis (USA)
*Missing Husbands (USA)
*Queen of Atlantis (USA)
*Die Loreley der Sahara (Germany)

==References==
 

==External links==
 
*  
* : analysis of the film by Françoise Marchand  
* : pictures and notes  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 