Lal Patthar
 
{{Infobox film
| name =Lal Patthar
| image = 
| image_size = 
| caption = 
| director =Sushil Majumdar
| producer =F. C. Mehra (Eagle Films)
| writer = Screenplay: Nabendu Ghosh Dialogue: Vrajendra Gaur Story: Prasanta Chowdhary
| narrator = 
| starring = Raaj Kumar Hema Malini music = Shankar Jaikishan
| cinematography = Dwarka Divecha	
| editing = Pran Mehra
| distributor = 
| released = 31 December 1971
| runtime = 2 hr 28 min
| country = India
| language = Hindi
| budget = 
}}
 1971 Hindi film, produced by F. C. Mehra, and directed by Sushil Majumdar. The film stars Raaj Kumar, Vinod Mehra, Hema Malini, Raakhee, Ajit Khan|Ajit, Asit Sen and Paintal (comedian)|Paintal. The film is one of rare films where lead actress Hema Malini played negative role, of jealous mistress of a zamindar, and tries to frame his young wife as an adultress, and her performance received accolades.  The music of the film was composed by Shankar-Jaikishan with award winning songs.

The film was shot on some minor outdoor locations and at Mehboob Studios, and Natraj Studios Bombay.

==Plot==
Raja Kumar Bahadur alias Gyan Shankar Rai (Raaj Kumar) has been a total abstainer all his life, never touching a drop of alcohol, and keeping away from women and all known vices all his life. Then one day he sees a young woman named Saudamani (Hema Malini), and instantly falls in love with her. He finds out about her background, and virtually buys her, and brings her to his palatial home. This is when he takes to drinking, and wooing her, and renaming her Madhuri, but refrains from marrying her.

Years later, he sees another beautiful woman, about half his age, named Sumita (Raakhee), meets with her parents, pays off their debts, and marries her in the bargain. He brings Sumita home, but makes it clear that Madhuris word is law here. Then Kumar finds out that Sumita had a childhood sweetheart in Shekhar (Vinod Mehra), who has now returned from abroad. Kumar meets with Shekhar and finds out that both still have feelings for each other. An insecure and jealous Kumar now schemes a plot against them on the historical grounds of Fatehpur Sikri   It is here Kumars plot unfolds to entrap the young lovers, thus giving the title "Lal Patthar".

==Cast==
* Raaj Kumar - Kumar Bahadur Gyan Shankar Rai
* Hema Malini - Saudamani / Madhuri
* Raakhee - Sumita
* Vinod Mehra - Shekhar Ajit - Raja Raghav Shankar Rai
* D.K. Sapru - Raja Ram Shankar Rai Asit Sen - Haricharan Paintal - Chhotu Dulari  - Mrs. Madhu Chakraborty
* Leela Mishra - Gokuls Mother
* Padma Khanna - Courtesan

==Critical Reception==

Lal Patthar was one of the films featured in Avijit Ghoshs book, 40 Retakes: Bollywood Classics You May Have Missed

==Soundtrack==
{{Infobox album |
| Name = Lal Patthar
| Type = Album
| Artist = Shankar Jaikishan
| Cover =
| Released =   1971 (India)
| Recorded = Feature film soundtrack
| Length =
| Label =  Sa Re Ga Ma- HMV
| Producer = Shankar Jaikishan
| Reviews =
| Last album = Nadaan  (1971)
| This album = Anuraag (1972)
| Next album = Rivaaj (1972)
}}
Film Annual 1972 Best Female Playback Singer in 1973, for her rendition of "Suni Suni Sans Ki Sitar Par". Another memorable number in the film is a ghazal, Unke Khayal Aaye To, Aake Chale Gaye, a composition based on Bageshwari/Gara ably sung by Mohammed Rafi.  The song "Re Man Sur Mein Ga" by Manna Dey won the "Sur Singar" Award for the year 1971.
{| class="wikitable"
|-
! Songs
! Singer/s
! Lyricist
|-
| Aa Aaja, Dikhaoon Tujhe Jannat Ki Shyam Asha Bhonsle Hasrat Jaipuri
|-
| Suni Suni Sans Ki Sitar Par
| Asha Bhosle
| Neeraj
|-
| Unke Khayal Aaye To, Aake Chale Gaye
| Mohammed Rafi
| Hasrat Jaipuri
|-
| Geet Gata Hoon Main
| Kishore Kumar
| Dev Kohli
|-
| Phoolon Se Meri Sez Saja Do
| Asha Bhonsle
| Neeraj
|-
| Re Man Sur Mein Ga
| Asha Bhosle and Manna Dey
| Neeraj
|}

==Notes==
 

==References==
*  
*  

==External links==
*  
*   at Eagle Films

 
 
 
 
 