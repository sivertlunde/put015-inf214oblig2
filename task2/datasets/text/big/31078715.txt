K3 en het Magische Medaillon
 
{{Infobox Film
| name           = K3 en het Magisch Medaillon
| image          = 
| image_size     = 
| caption        = 
| director       = Indra Sira
| producer       = 
| writer         = Hans Bourlon, Danny Verbiest
| narrator       = 
| starring       = Kathleen Aerts   Karen Damen   kristel Verbeke K3
| cinematography = 
| editing        =  Universal
| released       =  
| runtime        = 75 minutes (including aftertitles)
| country        = Belgium Netherlands
| language       = Dutch
| budget         = 
}}
 girl band K3 – Kathleen Aerts, Karen Damen and Kristel Verbeke – in their first full-length theater film.

==Plot==
K3, the popular hot-pants and miniskirt wearing trio of Belgian singers whose first names all start with the letter K (Kathleen, Karen and Kristel), star in their first motion picture. Since their music is aimed solely at the smallest, girliest of consumers (read: their parents) and written by their producers Samson en Gert (Verbiest & Verhulst), who may collectively be remembered as the Belgian Walt Disney in years to come (they already have a theme park), this movie was a sure-fire hit.  Like the Monkees, K3 go by their own names, live and sleep together and perform songs while remaining a struggling band (at least in the world this movie creates). Unlike the Spice Girls, this film knows where its audience lies and has no trouble reaching it. The girls do not perform much music at all: only three numbers, all of which are used as background music.

Phoney Italian crook Frederico Gazpacho (Peter Rouffaer) wants to get his hands on an ancient Egyptian Washabi Medallion, which just happens to be hidden away in the house of the late adventurer and architect who found the amulet, and I bet you  can guess which threesome are just about to move into that faithful house. Kristel (the dark-haired one) picks up a "Mega Macy" comic book, which not only foreshadows things to come but is also an excuse to give the entire film a comic book look, complete with stylish black and white panels substituting for scene changes (did I mention Samson & Gert publish comic books of all the childrens entertainers in their stable, including K3). Naturally the three of them find the amulet hidden in a wall almost immediately and, unaware of its powers, Kathleen (the blond one) decides to wear it around her neck.

Gazpacho sends his two silly henchmen to get the hidden treasure: Rode Tijger (Eddy Vereycken) is the dumb fat one who never shuts up, Zwarte Panter (Daisy Ip) is the Chinese girl who never speaks and whos Kung Fu is, shall we  say, not all that impressive. The magic amulet grants wishes, and each time one of the girls happens to make one, little animated gold stars do their handy work. About half way through, Dutch loud mouth and incredibly gay comedian Paul de Leeuw makes his first appearance as the Genie of the Medaillon, painted gold and spitting out thought balloons and animated letters all over the place. You can see right through him, and it looks like all of his scenes were filmed separately. This makes me wonder if he ever met K3 during filming at all. De Leeuw is even more unbelievably loud and annoying than usual, because for some reason every European childrens movie has to include at least one of these hyperactive character. Luckily, his scenes are kept to a bare minimum.

Kathleen wishes to be a princess. Karin (the redheaded one) wants to fly and Kristel asks for a mega-bike like Mega Macey. Soon they have spent 98 of their 100 wishes, and thats the moment the Genie chooses to explain the 100 wish limit. Its also the moment the nasties choose to reveal their dastardly intentions to the girls, forcing Kristel to use the penultimate wish to turn all three of them into Mega Macey (Kristel in Yellow, Kathleen in red and Karen in blue). The special effects, cinematography and production design are quite impressive by Studio 100 standards,  even the script by Samson & Gert is funnier than their usual material, patented for the K3 audience of pre-teenager girls. Although the running time of the film is less than an hour (including credits), I  believe a short film starring another Verhulst/Verbiest franchise was featured as an appetizer: Piet Piraat, whose movie resume at the time of this writing is a tie with the K3 girls: two each. Theres also a superhero spin-off television series called Mega Mindy.

== Cast ==

=== Main ===
* Kathleen Aerts...Kathleen
* Karen Damen...Karen
* Kristel Verbeke...Kristel

=== Supporting ===
* Paul de Leeuw...The Ghost (from the Medaillon)
* Eddy Vereycken...Red Tiger
* Daisy Ip...Black Panter
* Peter Rouffaer...Gazpacho
* Tom De Hoog...Police officer
* Angele De Backer...La Mama (Gazapachos mother)
* Nicolas Vander Biest...Halewijn Van Helewout

=== Minor ===
* Nadja Schrauwen
* Ilke Siera
* André van Cleemput-Wils
* Annie van Cleemput-Wils

== DVD ==
The Video DVD was released in early 2005.

== External7 links ==
*  

 

 
 
 
 