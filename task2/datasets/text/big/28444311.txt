Aathmakatha
{{Infobox film
| name           = Aathmakatha
| image          = Athma Kadha.jpg
| image_size     = 
| caption        = 
| director       = Premlal
| producer       = Santhosh Pavithram Shafeer Sait Vidya Santhosh  (co-producer) 
| writer         = Premlal 
| narrator       =  Sreenivasan Shafna Sharbani Mukherjee Jagathy Sreekumar
| music          = Songs:  
| cinematography = Sameer Haq
| editing        = Mahesh Narayanan
| studio         = Pavithram Creation
| distributor    = Pavithram Creations
| released       =  
| runtime        = 134 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Aathmakatha (English:Autobiography) is a 2010 Malayalam film written and directed by debutant Premlal starring Sreenivasan (actor)|Sreenivasan, Shafna, Sharbani Mukherjee and Jagathy Sreekumar in pivotal roles. Sreenivasan is playing a blind man named Kochubaby in this film while Sharbani Mukherjee plays Mary, his wife. The film talks about the positivism in the life of visually impaired Kochubaby.The film was selected to various international film festivals and was screened at the IFFI,Goa in the Indian Panorama section.Premlal received many awards for Athmakadha including a special jury award at the Kerala State Film Awards  for direction and Ramu Kariat award. 
 Kaithapram and Engandiyoor Chandrasekharan.

== Synopsis ==
"Athmakadha" belongs to a classic genre of Malayalam films that include the works of master directors like Bharathan and Padmarajan.Kochu baby (Sreenivasan)is a blind man working in a Candle factory run by the local church for handicapped, in a remote hillside  village. Although he is blind, he can do almost everything that other people do.Kochu is able to smile at the world and is a satisfied man.He shares a warm friendship with the local priest,Fr.Punnoose(Jagathy Sreekumar). A new employee and a blind,Mary,(Sharbani mukherji) joins the factory.   Slowly, the duo falls in love and marries.

They become blessed with a girl child soon and they names her Lilly. One day, as  they were returning from the hospital after taking polio vaccination to the child,  Mary meets with an accident while crossing the road and dies on the spot. Kochu then brings up the child.Years later,While Lilly(Shafna) was appearing her 10th board exams, she also feels that her vision is going blurry. Doctor confirms that she has an incurable disease in her nerves and she will go blind sooner or later.

Lilly is heart broken hearing this. She does not appear for the rest of the exams. Later she decides to commit suicide and gets herself a bottle of poison. But Kochu finds out the bottle and hides it from her. In the next scenes,kochu tries hard to make his daughter believe that it is not the end of the road and in the world,there is space for the handicapped  also.  She becomes more courageous and starts doing things with her eyes shut. In the last scene of the movie, we see Lilly losing her sight completely, she laughs at it saying that she is not afraid of blindness anymore.

== Cast == Sreenivasan as Kochubaby
* Shafna as Lillykutty
* Sharbani Mukherjee as Mary
* Jagathy Sreekumar as Father Punnoos
* Sreelatha Namboothiri as Paili
* Bindu Panicker as Saramma
* Munshi Venu as Eenashu
* Chempil Asokan as Kumaran
* Nisha as Rosie
* Babu Namboothiri
* Ambika Mohan as Amina
* Shalu Kurian

==External links==
 http://www.imdb.com/title/tt1723004/
*  
*  
*  

 
 
 
 
 

 