Ek Nai Paheli
{{Infobox film
| name           = Ek Nai Paheli
| image          = Ek Nai Paheli.jpg
| image_size     =
| caption        = Promotional Poster
| director       = K. Balachander
| producer       = Subba Rao
| writer         = K. Balachander
| narrator       =
| starring       = Kamal Haasan Raaj Kumar Hema Malini Padmini Kohlapure
| music          = Laxmikant-Pyarelal
| cinematography =
| editing        =
| distributor    =
| released       = June 29, 1984
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 Indian feature directed by K. Balachander, starring Kamal Haasan, Raaj Kumar, Hema Malini,  Padmini Kohlapure and Suresh Oberoi

==Plot==
 Tamil classic Apoorva Raagangal directed by K. Balachander, incidentally called the shots for Hindi version too.

==Summary==

 ]]
Upendranath, a widower lives a wealthy life and his only son, Sandeep is a headstrong and stubborn young man. He leaves his dads house to make his own life. He meets a beautiful older woman Bhairavi who is a singer. Both Bhairavi and Sandeep fall in love with each other and decide to marry, but a man,  Avinash now arrives and claims as the husband of Bhairavi, moreover he meets Sandeep as well and tells his story. He also speak about their daughter, Kajri. Climax reveals that, Kajri lives with the much older Upendranath and plan to marry as well. What would be the fate of Bhairavi, Sandeep and Avinash??

==Cast==
*Kamal Hassan ...  Sandeep
*Raaj Kumar ...  Upendranath
*Hema Malini ...  M.K. Bhairavi
*Padmini Kolhapure ...  Kajri
*Suresh Oberoi ...  Avinash Mehmood ...  Dr. Suri
*C.S. Dubey ...  Dubey - Upendranaths friend
*Rajkishore Rana ...  Upendranaths friend Anand
*N.S. Bedi
*Asha Sachdev ...  Jeet Kumari Chitra
*Rajee
*Rakesh Bedi ...
*Johnny Lever ...


==References==		
 
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Ek+Nai+Paheli

==External links==
* 		

 

 
 
 
 
 
 


 