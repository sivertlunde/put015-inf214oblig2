Sweeney 2
 
{{Infobox Film
| name           = Sweeney 2
| image          = "Sweeney_2"_(1978).jpg
| image_size     = 
| caption        = Original British quad poster Tom Clegg
| producer       = Ted Childs
| writer         = Ian Kennedy Martin Troy Kennedy Martin
| narrator       = 
| starring       = John Thaw Dennis Waterman Denholm Elliott
| music          = Tony Hatch
| cinematography = Dusty Miller
| editing        = Chris Burt
| distributor    = EMI
| released       =  
| runtime        =  104 minutes
| country        =  United Kingdom
| language       = English
| budget         = 
}}
Sweeney 2 is a 1978 film made following the success of the 1977 film Sweeney! which was a spin-off from the popular British TV show The Sweeney. Some of the action is transferred from the usual London setting to Malta. Denholm Elliott appears as a corrupt ex-officer, who asks his former subordinates to take down a gang of armed bank robbers. Nigel Hawthorne appears as a bureaucratic superior officer, taking a role similar to that usually played by Garfield Morgan in the television episodes.

== Plot ==
The plot concerns a group of bank robbers, who are both violent and successful, strangely getting away each time with an amount around the £60,000 mark, and often leaving behind cash in excess of this sum. 
 Purdey shotguns, they evade Regan and the Flying Squad for quite some time, before Regan finds encouragement from his former Detective Chief Superintendent who was sent down for corruption because Jack wouldnt testify in court for him.

== Cast ==
* John Thaw as Detective Inspector Jack Regan
* Dennis Waterman as Detective Sergeant George Carter
* Denholm Elliott as ex-Detective Chief Superintendent Jupp
* Ken Hutchison as Hill Anna Gaël as Mrs. Hill
* Lewis Fiander as Gorran
* Nigel Hawthorne as Detective Chief Inspector Dilke Frederick Treves as McKyle
* John Alkin as Dectective Sergeant Tom Daniels  
* James Warrior as Dectetive Constable Jellyneck

== External links ==
*  

 
 
 
   
   
 
 
 
 


 