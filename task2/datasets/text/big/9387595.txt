I Am Josh Polonski's Brother
{{Infobox film
| name           = I Am Josh Polonskis Brother
| image          = Joshpolonski.jpg
| image_size     = 
| caption        = I Am Josh Polonskis Brother poster
| director       = Raphael Nadjari
| producer       = Caroline Bonmarchand Tom Donahue Francesca Feder Anne Feinsilber Geoffroy Grison
| writer         = Raphaël Nadjari
| First Assistant Director       =  Fred Bellaiche Jeff Ware Meg Hartig Arnold Barkus Ivan Martin
| music          = Vincent Segal Jean-Pierre Sluys
| cinematography = Laurent Brunet
| editing        = Tom Donahue
| distributor    = MK2 Diffusion
| released       =  
| runtime        = 87 minutes
| country        = France United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

I Am Josh Polonskis Brother (2001) is Raphael Nadjaris second feature film. It was shot in New York City in 1999 and early 2000, in S8mm 200 ASA. The film was selected in the Berlin Film Festival at the Forum.

==Awards and nominations==
Berlin Film Festival (2001) - Forum of Young Cinema

==Plot==
The Polonski brothers, Abe, Ben and Josh, work together in their familys fabric store on the lower east side of Manhattan. Like any other Jewish family they go to their mothers to spend the Sabbath together. But one day, Josh is shot to death in the middle of the street in front of Abes eyes.. For Ben the tragic situation has an explanation - the nightlife of Josh - but Abe wants to understand what happened.  Following the path of his brother, he walks in the same foosteps, finding more and more of himself.

==Shooting - format and release== MK2 and more than 50,000 people came to see it in theaters even though it was shot in S8. Kodak supported the film to prove that it was doable to approach a true narrative feature in such a format in the midst of the digital age. Nadjari not only proved it but demonstrated that the digital age, instead of reducing media to its own format, is in fact a great system of expanding the possibilities of most film and video formats.

Nadjaris idea is that the digital age is an archiving system that enables any other format to fit, not technically, but more to integrate with the story we want to tell.  It is thus possible to use almost any recording device and have it fit to a narrative integration. Here the film was about family and nostalgia for film of the 70s, Super-8 film was the best format to fuse the format with its content.

== References ==
 
 

== External links ==
* 
*  Variety
*  Interview Richard Edson and Raphael Nadjari
*  Trailer

 
 
 
 
 
 
 
 