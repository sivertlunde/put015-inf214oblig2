H. G. Wells' The Shape of Things to Come
{{Infobox Film
| name           = H. G. Wells The Shape of Things to Come
| image          = Hgwellsshape.jpg
| image_size     =
| caption        = Beyond the earth... Beyond the moon... Beyond your wildest imagination!
| director       = George McCowan
| producer       = John Danylkiw (associate producer) 
William Davidson (producer) Harry Alan Towers (as Peter Welbeck) (executive producer) 
| writer         = Mike Cheda (adaptation) Joseph Glazner (adaptation) Martin Lager (writer) H.G. Wells (novel) John Ireland Anne-Marie Martin
| music          = Paul Hoffert
| cinematography = Reginald H. Morris	 	 
| editing        = Stan Cole	 
| distributor    = Film Ventures International (FVI) (1979) (USA) (theatrical) Astral Video (Canada) Blue Underground (2003) (USA) (DVD) International Film Distributors (Canada)
| released       = Canada May 4, 1979 UK May 24, 1979 USA August 1979 
| runtime        = 98 min.
| country        =  
| language       = English
}}
 Battlestar Galactica, although the film had only a fraction of the production budget of any of these. 

==Plot==
 
Sometime in the future, Earth is recovering from "The Robot Wars" that devastated the planet seven years earlier. Most of humanity now lives on the Moon within a domed city called New Washington, but their survival depends on an anti-radiation drug called Raddic-Q2 which is manufactured on the distant planet Delta 3. 
 John Ireland), and science advisor Dr. John Caball (Barry Morse), try to contact Nikki (Carol Lynley), the leader of Delta 3, but instead hear from Omus (Jack Palance), the "Robot Master," Caballs former apprentice, and the newly self-proclaimed Emperor of that world. Omus states that the crash was a deliberate attack and he demands the people of New Washington recognize his authority as their leader, or else he will send more ships with an invasion force of robots under his control. 

Smedley refuses to give into Omus threats and Caball suggests launching the Starstreak against him – an advanced starship designed for both space exploration and defense of the Moon colony, but Smedley goes against the plan since the ship has yet to be fully tested. Caball boards the ship anyway, and prepares it for launch, during which he accidentally exposes himself to a dose of deadly radiation while in the reactor room. 
 Eddie Benton), and "Sparks," a teleporting pilot robot that Kim had salvaged from the wreck of the cargo ship and repaired. When they arrive, Caball convinces them of the urgency to stop Omus at all costs. They agree to help steal the Starstreak and set course to Delta 3.

Shortly after launch, a malfunction forces the Starstreak to stop at Earth, and the crew separate from the ship to land the forward saucer section on the planet. While Caball conducts repairs, Jason and Kim explore the area hoping to locate an old friend of Caballs named Charley who mans a nearby refueling depot. They are unaware however, of small figures that stalk them in the woods. Jason eventually finds Charley dead and then notices that Kim has disappeared. He and Sparks eventually find her with a group of harmless children who are survivors of the Robot Wars, but with more pressing matters to attend to, Jason decides to leave the kids behind with some food supplies, but promises to come back for them once their mission is complete. 

Meanwhile on Delta 3, Nikki has formed a resistance force and tries to take back the Citadel – a massive tower controlled by Omus and his robot minions. Her infiltration attempt fails and Nikki can only pray that help arrives soon. Elsewhere, the Starstreak has left Earth and achieved light speed, but enters a gravity vortex that threatens to destroy the ship. The crew eventually manages to escape the storm with Delta 3 conveniently appearing before them. Upon landing, the crew finds Nikki and her people, but soon a group of Omus robots surround them. The party is then greeted by a hologram of Omus and Caball demands to meet face-to-face. Omus agrees and has Caball brought before him while Jason and the others plan to sneak inside the Citadel. 

Omus shows off his latest achievements to his old mentor, and how he was able to turn the mining robots into shock troopers that easily took control of the planet. Caball remains unimpressed and tries to talk Omus into giving up his plan to control humanity. Omus refuses to listen and then dons a transparent helmet where he shows Caball another creation – a spinning disco ball-like device that drives Caball mad with pain and eventually kills him.

Once the others finally reach Omus chambers, Jason finds his father murdered, but Kim reveals that Caball had severe radiation sickness and was about to die soon anyway. A furious Jason then confronts Omus, but Omus robots take him prisoner. Thanks to Sparks, all the robots suddenly turn on their master and run out of control, allowing Jason and the others to flee the control room. Jason hears from Sparks who has teleported to one Omus cargo ships and taken over the main computer system. The robot frenzy however, overloads critical systems and explosions begin to rip through the Citadel.  Sparks escapes in the cargo ship, while the others make it back to the Starstreak and lift off. They leave Omus sitting in his control room while everything explodes around him. The destruction of the Citadel eventually causes the whole planet to explode.

The last scene shows the two ships returning to Earth, with the cargo ship hauling a supply of Raddic-Q2.

==Critical reception==
G. Noel Gross of DVD Talk said, "Released in 1979, the sets and FX look more akin to nineteen FIFTY-nine with the aptly named Sparks and his robo-ilk who precariously teeter along like oversized popcorn poppers with great big salad tongs for arms." 

Nicholas Sylvain of DVD Verdict wrote, "Embarrassing acting, mysterious scriptwriting, and ultra-cheesy effects are potentially forgivable, but the worst sin of all is that The Shape Of Things To Come takes itself seriously. If insomnia rules your world or you have a need for an emergency coaster, then try The Shape Of Things To Come. Otherwise, put it down, and pull slowly away. How far? How about Cleveland?" 

J.C. Maçek III of WorldsGreatestCritic.com said, "Trust me folks, this film is NOT good and if this is, in fact, The Shape of Things to Come, Id much prefer to live in the past, man!" 

The film was released on DVD in 2004.

==See also==
*Things to Come, a 1936 film scripted by Wells, based on his novel.

==References==
 

==External links==
* 
* 
 
 
 
 
 
 