Husband's Holiday
{{Infobox film
| name           = Husbands Holiday
| image          = 
| alt            = 
| caption        = 
| director       = Robert Milton 
| producer       = 
| screenplay     = Ernest Pascal Viola Brothers Shore
| starring       = Clive Brook Vivienne Osborne Charlie Ruggles Juliette Compton Harry Bannister Dorothy Tree Adrienne Ames
| music          = John Leipold
| cinematography = Charles Rosher 
| editing        = 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

Husbands Holiday is a 1931 American drama film directed by Robert Milton and written by Ernest Pascal and Viola Brothers Shore. The film stars Clive Brook, Vivienne Osborne, Charlie Ruggles, Juliette Compton, Harry Bannister, Dorothy Tree and Adrienne Ames. The film was released on December 19, 1931, by Paramount Pictures. {{cite web|url=http://www.nytimes.com/movie/review?res=9A03EEDF133AEE3ABC4D51DFB467838A629EDE|title=Movie Review -
  Husband s Holiday - THE SCREEN; A Burlesque of Broadway Stage Producers Is Hollywood Reply to "Once in a Lifetime." Marriage and Infatuation. - NYTimes.com|work=nytimes.com|accessdate=21 February 2015}}  
 
==Plot==
 

== Cast ==
*Clive Brook as George Boyd
*Vivienne Osborne as Mary Boyd
*Charlie Ruggles as Clyde Saunders
*Juliette Compton as Christine Kennedy
*Harry Bannister as Andrew Trask
*Dorothy Tree as Cecily Reid
*Adrienne Ames as Myrtle
*Charles Winninger as Mr. Reid Elizabeth Patterson as Mrs. Caroline Reid
*Leni Stengel as Molly Saunders
*Dickie Moore as Philip Boyd
*Marilyn Knowlden as Anne Boyd
*Berton Churchill as Gerald Burgess 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 