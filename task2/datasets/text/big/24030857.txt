The Reluctant Widow (film)
{{Infobox film
| name           = The Reluctant Widow
| caption        =
| image         = "The_Reluctant_Widow"_(1950_film).jpg	
| director       = Bernard Knowles
| producer       = Gordon Wellesley
| writer         = Basil Boothroyd	(as J.B. Boothroyd) Gordon Wellesley Georgette Heyer (novel)
| narrator       =
| starring       = Jean Kent  Guy Rolfe
| music          = Allan Gray
| cinematography = Jack Hildyard
| editing        =  John D. Guthridge
| studio         = Two Cities Films
| distributor    = General Film Distributors   (UK)
| released       = 1 May 1950	(UK)
| runtime        = 91 minutes 
| country        = United Kingdom
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}} British drama French governess marries a British aristocracy (class)|aristocrat, and inherits his country house when he dies. The ongoing Napoleonic Wars see her become embroiled with a spy ring. 

==Cast==
* Jean Kent as Helena  
* Guy Rolfe as Lord Carlyon  
* Paul Dupuis as Lord Nivelle  
* Lana Morris as Becky  
* Kathleen Byron as Mme. Annette de Chevreaux  
* Scott Forbes as Francois Cheviot  
* Anthony Tancred as Nicky   Peter Hammond as Eustace Cheviot  
* Jean Cadell as Mrs. Barrows  
* Andrew Cruickshank as Lord Bedlington  
* George Thorpe as Colonel Strong  
* Ralph Truman as Scowler  
* James Carney as Major Forbes  
* Allan Jeayes as Colonel  
* Hector MacGregor as Sir Malcolm Torrens

==Critical reception==
Bosley Crowther wrote in The New York Times, "except for the rather fine surroundings and some nice eighteenth century costumes, there is no more in The Reluctant Widow than a genteel invitation to doze" ;  while Robin Karney in the Radio Times found, "a good-looking but rather muddled and unsatisfactory adaptation of a novel by Georgette Heyer, which entertains in fits and starts."  

==References==
 

 

 
 
 
 
 
 
 
 
 
 
 
 


 