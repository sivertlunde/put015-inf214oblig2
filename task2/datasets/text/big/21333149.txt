Fiebre de Amor (film)
{{Infobox Film
| name           = Fiebre de Amor 
| image          = Luceroluis.jpg
| caption        = Poster of the movie
| director       = René Cardona Jr.
| producer       = René Cardona Jr.
| writer         = 
| narrator       =  Lucerito Luis Miguel Guillermo Murray Lorena Velázquez
| music          = 
| cinematography = 
| editing        = 
| distributor    = Televicine Metro-Goldwyn-Mayer 1985
| runtime        = 
| country        = Mexico Spanish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Mexican motion musical and drama released in 1985. This film won the Diosa Award for best picture. 

== Synopsis  ==

A teenage girl with a crush on a young popular singing star sneaks into his house to fulfill her dreams of meeting him. Together they accidentally run into a group of diamond smugglers and she becomes the victim of a kidnapping. The young star comes to her rescue in this romantic comedy with lots of songs and the Acapulco scenery as a backdrop. .

== Cast == Lucero as Lucerito
* Luis Miguel as himself
* Lorena Velázquez
* Guillermo Murray
* Maribel Fernández
* Carlos Monden
* Monica Sanchez Navarro

==Soundtrack== soundtrack was Lucerito as the only performers.

==References==
 

== External links ==
*  

 
 
 
 