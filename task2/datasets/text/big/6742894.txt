Super Speedway
{{Infobox Film
| name           = Super Speedway
| image          = Superspeedwaydvd.jpg
| caption        = 
| director       = Stephen Low
| producer       =
| writer         = 
| narrator       = Paul Newman
| starring       = Mario Andretti Michael Andretti Christian Fittipaldi
| music          = 
| cinematography = 
| editing        =  IMAX Corporation
| released       = 1997
| runtime        = 50 min. USA
| English 
| budget         =
| preceded_by    = 
| followed_by    = 
}}
 documentary racing Lola chassis up to speed, and anothers goal of rebuilding an old 1964 roadster once driven by the legendary Mario Andretti.  The film was directed by renowned IMAX director Stephen Low and produced by Pietro Serapiglia. It was narrated by Paul Newman (who was himself an avid racer and co-owner of Newman/Haas Racing).  It first premiered at IMAX theaters nationwide.

==Production==

 

The entire process was a complicated one, taking well over four years to film.  Overall, the car was equipped with an IMAX camera and driven by Mario Andretti.  Michael Andretti drove his teammate, Christian Fittipaldis car, in the film.  Mario was able to get his car as fast as 240&nbsp;mph at the Michigan International Speedway at director Lows request.  Newman/Haas Racing was the only team interested in the project in the technical world of Indy Car racing. The effort was spearheaded by Neil Richter, the teams director of finances.

The movie features Mario and Michael Andretti during the 1996 season driving for Newman/Haas Racing.  During the season, Michael Andretti won a season-high five races and finished 2nd in the championship to Jimmy Vasser.  At the end of the film, Mario is reunited with his old roadster for a once-in-a-lifetime experience.

==External links==
*  
*  

 
 
 
 

 