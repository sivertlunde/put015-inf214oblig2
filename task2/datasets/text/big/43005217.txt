Aaj (film)
{{Infobox film
| name = Aaj
| image =
| image_size =
| border =
| alt =
| caption =
| director = Mahesh Bhatt
| producer =
| writer =
| screenplay =
| story =
| based on =  
| narrator =
| starring = {{Plainlist|
* Kumar Gaurav
* Smita Patil
* Raj Babbar Raj Kiran
}}
| music =
| cinematography =
| editing =
| studio =
| distributor =
| released =  
| runtime =
| country = India Hindi
| budget =
| gross =
}}

Aaj is a 1987 Hindi film, directed by Mahesh Bhatt and produced by Kuljeet Pal   

It stars Kumar Gaurav,Smita Patil, Raj Babbar, Marc Zuber and Anamika Pal.  The film featured songs written by the Hindi poet Sudarshan Faakir.

== Story ==
Akshay (Kumar Gaurav) is a youngster whose sister has been missing for a long time. Akshay often wanders around on streets looking for her. One day he meets and befriends a girl (Anamika Pal) who helps him to search his sister with him. The movie traces their search, and the twists that their own lives take during this search. 

==Cast==
* Kumar Gaurav...Akshay
* Smita Patil...Kavita
* Raj Babbar...
* Haidar Ali...
* Ila Arun...Bharti Raj Kiran...
* Anamika Pal...
* Suresh Chatwal...Inspector Ghorpade
* Yusuf...Hitman
* Haidar Ali...
* Akshay Kumar...Karate Instructor

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" st#le="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer#s#
|-
| 1
| "Phir Aaj Mujhe"
| ##Jagjit Singh##
|-
| 2
| "Rishta Yeh kaisa Hai"
| ##Chitra Singh##
|-
| 3
| "Woh Kagaz Ki Kashti #Part 1#"
| ##Jagjit Singh##
|-
| 4
| "Zindagi Roz Naye Rang Main"
| ##Jagjit Singh##, ##Chitra Singh##
|}

== References ==
 

==External links==
* 

 
 
 


 