Nanobba Kalla
 
{{Infobox film
| name           = Nanobba Kalla
| image          = 
| image size     = 
| caption        = 
| director       = Dorai-Bhagawan
| producer       = S. Ramanathan   S. Shivaram
| story          = Shankar-Sundar
| screenplay     = Shankar-Sundar
| narrator       =  Rajkumar Lakshmi Lakshmi Kanchana Kanchana Vajramuni
| music          = Rajan-Nagendra
| cinematography = Chittibabu
| editing        = P. Bhaktavatsalam
| studio         = Rashi Brothers
| released       =  
| runtime        = 
| country        = India
| language       = Kannada
}}  Kannada film Rajkumar in Lakshmi in lead roles.  It is based on the story written by Shankar-Sundar.

The film was received positively upon release and the music composed by Rajan-Nagendra met with highly critical appreciation. The film was an inspiration to the later releases such as Shakti (1982 film)|Shakti in Hindi and Kadamba (2004 film)|Kadamba in Kannada.  

==Cast== Rajkumar in dual roles Lakshmi
* Kanchana
* Thoogudeepa Srinivas
* Tiger Prabhakar
* Shivaram
* Dheerendra Gopal
* Vajramuni
* Rajanand
* Shakti Prasad
* Papamma

==Soundtrack==
The music of the film was composed by the duo Rajan-Nagendra with lyrics penned by Chi. Udaya Shankar. 
{{Infobox album	
| Name = Nanobba Kalla
| Longtype = 
| Type = Soundtrack	
| Artist = Rajan-Nagendra	
| Cover = 
| Border = Yes	
| Alt = Yes	
| Caption = 
| Released = 1979
| Recorded = 
| Length =  Kannada 
| Label = 
| Producer = 
| Last album = 
| This album = 
| Next album = 
}}

===Tracklist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Kopavethake Nannali" Rajkumar
|-
| 2
| "Araluthide Moha"
| Rajkumar, S. Janaki
|-
| 3
| "Aaseyu Kaigoodithu"
| Rajkumar, S. Janaki
|-
| 4
| "Nanobba Kallanu"
| Rajkumar
|-
|}

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 