Let's Spend the Night Together (film)
 
{{Infobox film
| name           = Lets Spend the Night Together
| image          = Lets Spend the Night Together.jpg
| image_size     = 
| caption        = 
| director       = Hal Ashby
| producer       = 
| writer         = 
| narrator       = 
| starring       = The Rolling Stones
| music          = The Rolling Stones
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1983
| runtime        = 
| country        = United States
| language       = English
| budget         = 
}} 1981 North American Tour. It was directed by Hal Ashby, and released to cinemas in 1983, then subsequently released on VHS and CED Videodisc. It was released in New Zealand & Australia with the alternative title Time Is On Our Side on VHS and is currently available on DVD in Japan, Australia and New Zealand (as L.S.T.N.T from STUDIO CANAL/UNIVERSAL).

It was filmed at the Meadowlands Sports Complex in East Rutherford, New Jersey (5–6 November 1981) and at Sun Devil Stadium in Tempe, Arizona (13 December 1981).

The film was released as Rocks Off in Germany in 1982 with slightly different footage and the additional song "When the Whip Comes Down" (following "Under My Thumb") from Sun Devil Stadium.

See also the live album "Still Life" (American Concert 1981), released in 1982, from the same tour.

Lions Gate Entertainment released the film on DVD in the United States on November 2, 2010.

==Track listing==
# "Under My Thumb" -  (Tempe, Arizona, 13 December 1981 (outdoor stadium show)) 
# "Lets Spend the Night Together" -  (Tempe, 13 December 1981) 
# "Shattered (song)|Shattered" -  (Tempe, 13 December 1981) 
# "Neighbours (song)|Neighbours" -  (Tempe, 13 December 1981) 
# "Black Limousine" -  (Tempe, 13 December 1981) 
# "Just My Imagination (Running Away with Me)" -  (Tempe, 13 December 1981) 
# "Twenty Flight Rock" -  (Tempe, 13 December 1981)  Let Me Go" -  (Tempe, 13 December 1981) 
# "Time Is on My Side" -  (Tempe, 13 December 1981)  Beast of Burden" -  (Tempe, 13 December 1981) 
# "Waiting on a Friend" -  (Tempe, 13 December 1981)  Going to a Go-Go" -  (East Rutherford, New Jersey, 6 November 1981 (indoor arena show) 
# "You Cant Always Get What You Want" -  (East Rutherford, 6 November 1981) 
# "Little T&A" -  (East Rutherford, New Jersey, 5 November 1981 (indoor arena show) 
# "Tumbling Dice" -  (East Rutherford, 5 November 1981) 
# "Shes So Cold" -  (East Rutherford, 5 November 1981) 
# "All Down the Line" -  (East Rutherford, 5 November 1981) 
# "Hang Fire" -  (East Rutherford, 5 November 1981)  Miss You" -  (East Rutherford, 6 November 1981)  Let It Bleed" -  (East Rutherford, 5 November 1981) 
# "Start Me Up" -  (East Rutherford, 5 November 1981) 
# "Honky Tonk Women" -  (Tempe, 13 December 1981)  Brown Sugar" -  (East Rutherford, 5 November 1981) 
# "Jumpin Jack Flash" -  (Tempe, 13 December 1981) 
# "(I Cant Get No) Satisfaction|Satisfaction" -  (East Rutherford, 6 November 1981) 

==Personnel==
;The Rolling Stones
*Mick Jagger&nbsp;– lead vocals, guitar
*Keith Richards&nbsp;– guitar, vocals
*Ronnie Wood&nbsp;– guitar, backing vocals drums
*Bill Wyman&nbsp;– bass guitar

;Additional musicians organ
*Ian Ian Stewart&nbsp;– piano
*Ernie Watts&nbsp;– saxophone, tambourine

==External links==
*  

==References==
 
 

==See also==
*"Still Life" (American Concert 1981)

 
 
 

 
 
 
 
 
 
 
 
 
 