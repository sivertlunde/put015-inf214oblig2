The Great Waltz (film)
{{Infobox film
| name           = The Great Waltz
| image          = Poster of the movie The Great Waltz.jpg image size      = 250px
| caption        = 
| director       = Julien Duvivier Victor Fleming (uncredited) Josef von Sternberg (uncredited)
| producer       = Bernard H. Hyman
| writer         = Gottfried Reinhardt (story) Samuel Hoffenstein Walter Reisch Vicki Baum (story, uncredited)
| narrator       = 
| starring       = Fernand Gravet Luise Rainer Miliza Korjus
| music          = 
| cinematography = 
| editing        = Tom Held
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = $2,260,000  . 
| gross          = $2,422,000 
}} Fernand Gravet (Gravey) and Miliza Korjus. Rainer received top billing at the producers insistence, but her role is comparatively minor as Strauss wife, Poldi Vogelhuber. It was the only starring role for Korjus, who was a famous opera soprano and played one in the film.
 Supporting Actress Film Editing.
The film was popular in Australia and was distributed largely throughout Sydney and Melbourne for two years after its initial release.

==Plot summary==
The highly fictionalised story sees Schani dismissed from his job in a bank. He puts together a group of unemployed musicians who wangle a performance at Dommayers cafe. The audience is minimal, but when two opera singers, Carla Donner and Fritz Schiller, visit whilst their carriage is being repaired, the music attracts a wider audience.

Strauss is caught up in a student protest; he and Carla Donner avoid arrest and escape to the Vienna Woods, where he is inspired to create the waltz Tales from the Vienna Woods.

Carla asks Strauss for some music to sing at an aristocratic soiree and this leads to the composer receiving a publishing contract. Hes on his way, and he can now marry Poldi Vogelhuber, his sweetheart. But the closeness of Strauss and Carla Donner during rehearsals of operettas, atrracts comment, not least from Count Hohenfried, Donners admirer. 

Poldi remains loyal to Strauss and the marriage is a long one. He is received by the Kaiser Franz Josef (whom he unknowingly insulted in the aftermath of the student protests) and the two stand before cheering crowds on the balcony of Schőnbrunn.
==Box Office==
According to MGM records the film earned $918,000 in the US and Canada and $1,504,000 elsewhere, resulting in a loss of $724,000. 
==Re-make==
 
The film was re-made in 1972 with Horst Buchholz playing Strauss alongside Mary Costa, Nigel Patrick and Yvonne Mitchell.

== References ==
 

==External links==
* 
*  
*  
* 1972 remake:  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
   
 
 
  
 