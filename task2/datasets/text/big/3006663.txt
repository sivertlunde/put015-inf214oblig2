Jabe Babe – A Heightened Life
{{Infobox film
| name           = Jabe Babe - A Heightened Life
| image          =
| alt            =
| caption        =
| director       = Janet Merewether
| producer       =
| writer         =
| starring       = Jabe Babe
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = Australia
| language       = English
| budget         =
| gross          =
}} 
Jabe Babe – A Heightened Life is a 2005 Australian documentary film directed by Janet Merewether.

The subject of the film is Jabe Babe, a woman diagnosed with marfan syndrome, a hereditary disorder which affects organs including the skeleton, lungs, eyes, heart, and blood vessels. The disorder has resulted in Jabe Babe growing to 188 centimetres in height. After being told she only had a short time to live, Jabe Babe embarks on living life as though her time is nearly through, which includes working as a dominatrix. The film follows Jabe as she strives for a more "normal" life by pursuing a career in the funeral industry.

The film won the award for Best Documentary Directing Award at the 2005 Australian Film Institute Awards, Best Australian Documentary at the 2005 Inside Film Awards and the Merit Award TIDF Taiwan International Documentary Festival.

Jabe died on 6 April 2008 from heart complications

==External links==
* 
* 
* 
*  

 
 
 
 
 
 


 
 