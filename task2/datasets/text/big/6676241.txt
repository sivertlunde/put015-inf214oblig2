Bloodlust!
{{Infobox film
| name           =Bloodlust! 
| image          = Bloodlustposter.jpg
| caption        = A promotional film poster for "Bloodlust!"
| director       = Ralph Brooke
| producer       = Robert H. Bagley Ralph Brooke
| writer         =Richard Connell Ralph Brooke
| starring       = Wilton Graff Robert Reed June Kenney  Gene Persson
| music          = Michael Terr
| cinematography = Richard E. Cunha
| editing        = Harold V. McKenzie
| distributor    = Crown International Pictures
| released       = 13 September 1961
| runtime        = 68 min
| country        = United States
| awards         =  English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 thriller film about a group of young adults who visit a tropical island only to become prey for a sadistic hunter.  Filmed in 1959 and released in 1961, the feature was picked up for cinema release by Crown International Pictures. Crown released the film in 1970 as a double feature with Blood Mania. 

The film was featured on Mystery Science Theater 3000 in September 1994, during season 6, and featured the first appearance of Pearl Forrester.

==Plot==
Two couples (Robert Reed, June Kenney, Joan Lora, and Eugene Persson) are on a boating trip when they come across an uncharted island.  The four investigate and find themselves in the clutches of Dr. Albert Balleau (Wilton Graff), whose hobby is hunting both animals and humans.

After learning about the terrible secrets of the island from the doctors wife (Lilyan Chauvin) and her boyfriend (Walter Brooke) as well as an investigation, the group tries to escape only to be thwarted by Dr. Balleau and his henchmen.  Balleaus wife and her lover are slain and taxidermy|stuffed, while the men are forced to participate in a The Most Dangerous Game-style hunt, with their girlfriends soon joining them.  The hunt includes Balleaus sailor henchmen and hidden traps, as well as Balleaus deadly skill. At the end, Balleu becomes the last one to be hunted when killed by his assistant Jondor.

==Home video releases==
*The movie was released on DVD as a double feature with Atom Age Vampire on March 20, 2001, and later released on DVD as another double feature in 2002 with The Amazing Transparent Man, another film used on Mst3k. 
*The Mst3k version was released on VHS in March 2000 by Rhino Home Video, then released on DVD by Rhino as part of the Volume 1 collection, with the uncut film as an extra.

==See also==
* List of American films of 1961
* Taxidermy
* The Most Dangerous Game

==References==
 
  SUBTITULADA AL ESPAÑOL ONLINE

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 