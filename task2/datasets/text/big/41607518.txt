Amara Kaaviyam
{{Infobox film
| name           = Amara Kaaviyam
| image          = Amara Kaaviyam.jpg
| alt            =  
| caption        = 
| director       = Jeeva Shankar Arya
| writer         = Jeeva Shankar Sathya Miya Miya
| music          = M. Ghibran
| cinematography = Jeeva Shankar
| editing        = Suriya
| studio         = The Show People Vignesh Pictures
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         =  
| gross          =
}} Sathya and Miya in the lead roles. The film, produced by actor Arya (actor)|Arya, features a successful soundtrack composed by M. Ghibran. It opened to mixed reviews in September 2014.

==Plot==
The movie begins with Jeeva (Sathya (actor)|Sathya) being taken to court, where he recalls his past. In 1989, at the beginning of his 12th standard, his friend Balaji (Ananth Nag) falls in love with Kartika (Miya George|Miya) their classmate. Jeeva talks to Kartika on Balaji’s behalf but he is shocked to know that she is actually in love with him and not his friend.
The next day he accepts her love and both grow close to each other. Jeevas father died when he was young, and his mother married again. Once when Jeeva and Kartika are out they are caught by police and both their families are informed about this. Later, when Jeeva goes to apologize to Kartikas parents, he is hit by her father. In anger he sets their bike on fire. He is handed to the police but the court releases him and asks him to consult a psychiatrist. Meanwhile Kartikas father applies for a transfer. They both promise that they won’t meet until their last exam. but on the last day she leaves without informing him directly but gives a letter to Balaji asking him to pass it on to Jeeva, but Balaji does not give the letter to him instead tries to convince Jeeva that Kartika doesnt love him anymore.
Jeeva gets disturbed and is hospitalized; both try to meet each other but in vain.
Finally Jeeva learns the whereabouts of Kartika, after lots of misunderstanding they finally meet. When he meets her he lands up in a fight with her friend.
At last he again goes to her home and asks her if she still loves him. With no option to make him leave she says NO but later feels she should talk to him and marry him the way he asked her as a proof, but fate has other plans. The next day when she meets him, he stabs her before she can say anything and later realizes that she is still in love with him. He tries to save her but she dies in his arms. The scene returns to present where Jeeva escapes from police just to find Kartikas grave and weeps for his mistake. Then he runs and jumps off a cliff. The movie ends with Jeeva and Kartika united in heaven and their names on the bark of tree written by Jeeva when they first met.

==Cast== Sathya as Jeeva Miya as Karthika
*Ananth Nag as Balaji
*Thambi Ramaiah as Gnanam
*Aroul Jody as Chandrasekhar, Jeevas step-father
*Rindhuravi as Jeevas mother
*Vaidyanathan as Karthikas father
*Elizabeth as Karthikas mother
*Sudeepa Pinky as Karthikas sister
*R. Subramanian as Petti Kadai owner

==Production==
Following the critical and commercial success of his previous project Naan (film)|Naan (2012), director Jeeva Shankar wrote a new script titled Amara Kaaviyam, a college love story set in the 1980s.  Madhan of Escape Artistes Motion Pictures was to produce the film, while Yuvan Shankar Raja was announced as the music composer.  Adharvaa was announced to play the lead role but consequently opted out during pre-production stages, and the production house dropped the film.  
 Sathya in Arya producing the film under his production house, The Show People.   Malayalam actress Miya George was signed to play the female lead role, making her debut in Tamil films. The film began production in October 2012 in Ooty, with costume designer Sai preparing clothes for the films 1980s backdrop.  Mia said that she played a school girl named Karthika in the film, a "very cute character", going on to add that she had to act in a lot of emotional scenes and that she would be seen mostly in the school uniform costume and the half saree. She had shot for 45 days in Ooty, with most of her scenes being shot in a set.  Sathya played a school boy named Jeeva, a character that required him to shed more than 10 kg, with the actor stating that the film was an "intense love story...on the lines of Kaadhal Kondein (2003), 7G Rainbow Colony (2004), and Kaadhal (2004)". 

==Soundtrack==
{{Infobox album|  
| Name = Amara Kaaviyam
| Type = Soundtrack
| Artist = M. Ghibran
| Cover  = 
| Released = 28 June 2014 Feature film soundtrack
| Length = 27:32 Tamil
| Label = Sony Music India
| Producer = M. Ghibran
| Reviews =
| Last album =Run Raja Run (2014)
| This album =Amara Kaaviyam (2014)
| Next album =Uttama Villain (2014)
}} Bala and actresses Pooja Umashankar, Lekha Washington and, Rupa Manjari were also present. 
 music bridges as the director had not placed any restriction on the length of the songs.  Barring K. S. Chitra, no 80s singers sang the songs as the voices had to match the two young leads, with Ghibran adding that, since the film was about the journey of the two characters, the voices "mature as the film progresses". 

The album received high praise from critics. The Times of India wrote, "This is an album that you will instantly take to and it is only going to grow on you even better, especially if you are in love. It is sure to figure in the Top 5 of the years best film album".  Behindwoods have 3.25 out of 5 and wrote, "Ghibran showcases poise, proficiency and pure melodies in this soul-stirring album".  Indiaglitz.com wrote, "Ghibran is on a roll. Whats so good to see is, the tunes let the lyrics to dominate and the freedom he gives to the singers to be imaginative. With this album he has gone a step further from being a bankable music director to a star composer. Like the title, the album will be remembered as an epic in the years to come".  musicaloud.com gave it a score of 8.5 out of 10 and wrote, "His Telugu debut may not have been up to his usual standards, but with Amara Kaaviyam, Ghibran continues to churn out top quality work in Tamil".  Sify wrote, "Ghibran shoulders the movie by delivering his career best soundtrack and his re-recording is very refreshing too". 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 27:32 
| lyrics_credits = yes
| title1 = Saridhaana Saridhaana
| lyrics1 = Madhan Karky
| extra1 =  Yazin Nizar, Thomson Andrews & M. Ghibran
| length1 = 04:28
| title2 = Edhedho Ennamvandhu
| lyrics2 = Parvathy
| extra2 =  Haricharan & Padmalatha
| length2 = 04:51
| title3 = Mounam Pesum
| lyrics3 = P. Vetriselvan
| extra3 = K. S. Chithra & Sowmya Mahadevan
| length3 = 04:41
| title4 = Dheva Dhevadhai
| lyrics4 = P. Vetriselvan Ranjith & Madhu Iyer
| length4 = 04:18
| title5 = Thaagam Theerea
| lyrics5 = Asmin
| extra5 =  Yazin Nizar & Padmalatha
| title6 = Edhedho Ennamvandhu (Solo)
| lyrics6 = Parvathy
| extra6 =  Sundar Narayana Rao
| length6 = 04:41
}}

== References ==
 
 

==External links==
*  

 
 
 
 