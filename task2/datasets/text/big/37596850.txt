Balukabela.com
 
{{Infobox film
| name           = Balukabela.com
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Partha Sen
| producer       = Kunal Ghosh
| writer         = 
| screenplay     = Padmanava Dasgupta, Rudranil Ghosh
| story          = Pracheta Gupta
| based on       =  
| narrator       = 
| starring       = 
| music          = Raja Narayan Deb
| cinematography = Premendu Bikash Chaki
| editing        = 
| studio         = 
| distributor    = 
| released       = 14 December 2012
| runtime        = 
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}}
Balukabela.com is an upcoming Bengali language film directed by Partha Sen.    The film was world-premiered in London in May 2012.      

== Cast ==
* Parambrata Chattopadhyay
* Locket Chatterjee
* Payel Sarkar
* Ambalika
* Taniya
* Indranil
* Abhiraj
* Rudranil Ghosh
* Rahul
* Bratya Basu
* Saswata Chatterjee

== See also ==
* Aborto
* Goynar Baksho

== References ==
 
* http://www.gomolo.com/balukabelay-dot-com-movie/43930

 
 
 


 