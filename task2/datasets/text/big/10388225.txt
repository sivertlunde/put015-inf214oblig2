Up for Grabs (film)
{{Infobox Film
| name           = Up for Grabs
| image          =Up for Grabs (film).jpg
| caption        = 
| director       = Michael Wranovics
| producer       = Michael Wranovics
| writer         = 
| starring       = Marty Appel Barry Bonds Patrick Hayashi
| music          = 
| cinematography = Josh Keppel
| editing        = Dave Ciaccio
| distributor  =   Laemmle/Zeller Films
| released       = 2004
| runtime        = 88 minutes
| country        = USA English
}}
 2004 comedic documentary about two men who fought over custody of a baseball. It was directed and produced by Michael Wranovics, co-produced by Michael Lindenberger and Josh Keppel, co-edited by Dave Ciaccio; the executive producers were Dr. Roger Petrie, Helen Woo, and Christopher Parry.

==Synopsis==
The ball happened to be the one hit by San Francisco Giants slugger Barry Bonds for his record-setting 73rd home run at the end of the 2001 MLB season.  When the ball landed in the right-field bleachers at what was then Pac Bell Park (San Francisco), there was a mad scramble for the precious ball, bodies piled up on the walkway above McCovey Cove. Patrick Hayashi, who stood quietly with a sheepish grin on his face as the scrum continued, eventually held the historic ball up for a TV camera to reveal that he had possession of it. MLB and Giants security grabbed Mr. Hayashi and escorted him down to the bowels of the ballpark and authenticated his baseball as the true #73.  

As Hayashi prepared to be the next Bay Area millionaire, a man named Alex Popov, owner of Smart Alecs restaurant in Berkeley, CA, was complaining loudly that he had caught the ball on the fly and that Patrick had stolen the ball from him at the bottom of the pile. Video footage shot by KNTV news cameraman Josh Keppel did actually show the ball land in Popovs glove, providing the key evidence that led to a trial in San Francisco Superior Court.
 Best in Show, etc.), Up for Grabs focuses on the characters involved rather than the event itself.

==Awards==
Up for Grabs won the Audience Award for Best Documentary at the 2004 Los Angeles Film Festival, plus Best Documentary at the Gen Art Film Festival (New York) and the Phoenix Film Festival. Its Rotten Tomatoes critical "Fresh" rating is 93%. {{cite web | url=
http://www.rottentomatoes.com/m/up_for_grabs | title=Rotten Tomatoes | accessdate=2009-09-16 | last= | first= | coauthors= | date= | work= | publisher=}} 

MLB Advanced Media released the DVD during the 2007 MLB season - which coincides with Barry Bonds successful pursuit of Hank Aarons lifetime home run record. Bonds hit his 756th career HR on August 7, 2007.
 Rodrigo Garcías Nine Lives, and Opie Gets Laid. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 