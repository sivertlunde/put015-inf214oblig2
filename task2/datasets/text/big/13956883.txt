Annie Oakley (film)
{{Infobox film
| name           = Annie Oakley
| image          = Annie Oakley (poster).jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = George Stevens
| producer       = Cliff Reid
| screenplay     = {{Plainlist|
* Joel Sayre
* John Twist
}}
| story          = {{Plainlist|
* Joseph Fields
* Ewart Adamson
}}
| starring       = {{Plainlist|
* Barbara Stanwyck
* Preston Foster
* Melvyn Douglas
* Moroni Olsen
}}
| music          = Alberto Colombo
| cinematography = J. Roy Hunt
| editing        = Jack Hively
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $354,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p56  
| gross          = $620,000 
}}

Annie Oakley is a 1935 American biographical film directed by George Stevens and starring Barbara Stanwyck, Preston Foster, Melvyn Douglas, and Moroni Olsen. The film is based on the life of Annie Oakley.

==Plot==
In late 1800s Ohio, a young woman from the backwoods, Annie Oakley (Stanwyck) delivers six dozen quail she has shot to the owner of the general store. He sends them to the MacIvor hotel in Cincinnati, where the mayor is holding a large banquet in honor of Toby Walker (Foster), the "greatest shot in the whole world". Walker is particular about what he eats&ndash;the hotel owner (James MacIvor, played by Andy Clyde) bought Oakleys quail because she shoots the quail cleanly through the head, leaving no buckshot elsewhere.
 sudden death. The two sharpshooters continue hitting their targets. Following a comment from Oakleys mother (Margaret Armstrong) Oakley  deliberately misses her next shot. Walker is a gracious, though unsuspecting winner; Hogarth knows exactly what happened.

 
When the Oakleys return home, Annie promises to pay back all those who bet on her. Hogarth follows and tells Annie that he never bet the money she gave to him. He also invites her to join the Wild West Show. Oakley, having developed a crush on Walker, accepts. Hogarth introduces her to Buffalo Bill (Moroni Olsen) and the other members of the show.

When Walker overhears Buffalo Bill telling Hogarth that he might have to fire Oakley because she lacks showmanship (performing)|showmanship, he teaches her some fancy shootin and tricks. 

At the first show, Chief Sitting Bull (Chief Thunderbird) is in the audience with Iron Eyes Cody as his translator. Ned Buntline (an uncredited Dick Elliott), Buffalo Bills publicist, tries to sign him up for the show, but the chief is bored with the acts until he sees Annie shoot five targets thrown in the air. He is so impressed, he changes his mind and joins the show.

A romance blossoms between Oakley and Walker, despite Hogarths attempts to win Oakleys affections for himself. They also become good friends with Sitting Bull. 

One day, a man with a grudge tries to shoot Sitting Bull. Walker grabs the mans gun just as it goes off, saving his friends life. However, his eyes are affected by the closeness of the shot.  While Oakleys fortunes rise, Walkers decline. He hides his injury, but ends up shooting Oakley in the hand and is dismissed from the show. However, Oakley cannot forget him. After a triumphant tour of Europe, the show next plays New York City, Walkers home town. When Walker attends the show, Sitting Bull spots him and reunites the loving couple.

==Cast==
  as Annie]]
*Barbara Stanwyck — Annie Oakley
*Preston Foster — Toby Walker
*Melvyn Douglas — Jeff Hogarth
*Moroni Olsen — Col. William F. Buffalo Bill Cody
*Pert Kelton — Vera Delmar
*Andy Clyde — James MacIvor
*Chief Thunderbird — Sitting Bull (as Chief Thunder Bird)
*Margaret Armstrong — Mrs. Oakley
*Delmar Watson — Wesley Oakley
*Adeline Craig — Susan Oakley

==Production== Western for both Stevens and Stanwyck.    While based on the real life of Annie Oakley, it took some liberties with the details: 
 Rather than focusing on her career, the 1935 production centered on the love story between Annie and "Toby Walker," the films stand-in for Oakleys husband Frank Butler. In the film, Oakley throws the couples famous Thanksgiving Day shooting match so that Walker wont lose his job, a point that may have resonated with the films Great Depression in the United States|Depression-era audiences. Oakley also spends much of the film pining away for Walker&mdash;they are separated while she tours in Buffalo Bill Codys Wild West show, but fortuitously reunited by Sitting Bull just in time for a happy ending. In this first Hollywood version of Oakleys life, the facts of the Butlers long and happy marriage are pushed to the side, and Frank Butlers deliberate ceding of the spotlight to his wife is ignored. 

==Reception==
The film was released less than 10 years after the death of the real-life Oakley.  It made a profit of $48,000. 

Andre Sennwald of The New York Times called the film a "gaudy and pungent motion picture, smacking healthily of that obscure commodity known as tanbark"; Sennwald rave about the performances: 
 Barbara Stanwyck is splendid in the title rôle; this is her most striking performance in a long time. Preston Foster plays persuasively, too, in the unrealized Toby Walker rôle, and Moroni Olsen is excellently bluff as Buffalo Bill. Chief Thunderbird, though, is the star of the picture. One scene, by the way, ought to give you a start. That is when the Wilhelm II, German Emperor|Kaiser, then only a Prince, gallantly, holds a cigarette in his mouth for Annie to shoot at. What might have been the course of history, you find yourself wondering, if Annie had missed.  

Decades later, Pauline Kael called Stanwyck "consistently fresh and believable" and said Stevens "makes some of the points about race he made later in Giant (1956 film)|Giant... but here theyre lighter and better. They seem to grow casually out of the American material; the movie feels almost improvised." 

==References==
{{reflist|refs=
   
   
   
}}

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 