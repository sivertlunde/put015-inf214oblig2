Concussion (2013 film)
 
{{Infobox film
| name           = Concussion
| image          = Concussion Movie Poster 2013.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Film poster
| director       = Stacie Passon
| producer       = Cliff Chenfield Anthony Cupo Rose Troche
| writer         = Stacie Passon
| starring       = Robin Weigert Maggie Siff Johnathan Tchaikovsky Ben Shenkman Janel Moloney Emily Kinney
| music          = Barb Morrison
| cinematography = David Kruta
| editing        = Anthony Cupo
| studio         = 93 Films Razorwire Films
| distributor    = The Weinstein Company|RADiUS-TWC
| released       =  
| runtime        = 96 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = $42,606 
}}
Concussion is a 2013 American  , February 20, 2013. 

==Plot==
Abby Ableman is a lesbian who becomes disillusioned with her domestic life and career after suffering a mild concussion when her son, Jake, accidentally hits her in the head with a thrown baseball. She then begins working as a prostitute for other women. 

==Cast==
 
* Robin Weigert as Abby Ableman / Eleanor
* Maggie Siff as Sam Bennet
* Johnathan Tchaikovsky as Justin
* Ben Shenkman as Graham Bennet
* Janel Moloney as Pru
* Emily Kinney as The Girl
* Daniel London as Evan
* Kate Rogal as Gretchen
* Julie Fain Lawrence as Kate Ableman
* Maren Shapero as Mayer Ableman
* Micah Shapero as Jake Ableman
* Anna George as Dr. Jofar
* Anthony Cupo as Walter
* Funda Duval as Sarah
* Claudine Ohayon as Lisa
* Daria Rae Feneis as Woman #1
* Tracee Chimo as Woman #2
* Laila Robins as Woman #3
* Mimi Ferraro as Woman #4
* Erika Latta as Woman #5
 

==Release== premiered at the 2013 Sundance Film Festival and the 2013 Berlin International Film Festival. At Sundance, it secured a deal with The Weinstein Company for general release later in 2013.  At Berlin, the film won a Teddy Award Jury Prize as an outstanding film about LGBT themes. 

==Critical reception==
Concussion received generally positive reviews, currently holding a 75% "fresh" rating on  , based on 18 critics, the film has a 56/100 rating, signifying "mixed to average reviews". 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 