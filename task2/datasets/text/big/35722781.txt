Mexican Spitfire
 
{{Infobox film
| name           = Mexican Spitfire
| image          = Poster - Mexican Spitfire.jpg 
| image_size     =
| caption        = Promotional poster of the film James Anderson (assistant)
| producer       = Robert Sisk
| writer         = Charles E. Roberts and Joseph Fields
| narrator       = Donald Woods
| music          = Paul Sawtell
| cinematography = Jack MacKenzie
| editing        =
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = $106,000 Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p144 
| gross          = $102,000 
}}

Mexican Spitfire is a 1940 American comedy film starring Lupe Vélez.  She plays a hot-headed, fast-talking Mexican singer taken to New York for a radio gig, who decides she wants the ad agency man for herself. The film was the sequel of the film The Girl from Mexico (1939) and was succeeded by another 6 more films with the same title and main characters.

==Story== Donald Woods) and Carmelita (Lupe Vélez) have several obstacles to deal with in their new marriage: Carmelitas fiery Latin temper, a meddling aunt and a conniving ex-fiancee who is determined to break up their marriage.

==Cast==
* Lupe Vélez as Carmelita Fuentes Donald Woods as Dennis Denny Lindsay
* Leon Errol as Uncle Matthew Matt Lindsay Linda Hayes as Elizabeth Price
* Cecil Kellaway as Mr. Chummley

==Notes==
First official entry in the series is a retread of The Girl from Mexico, but shifts focus from bland leading man Woods to hilarious Errol in dual role of Uncle Matt and the tipsy Lord Epping. The film was succeeded by another 6 films:

*Mexican Spitfire Out West (1940) 
*Mexican Spitfires Baby (1941)
*Mexican Spitfire at Sea (1942)
*Mexican Spitfire Sees a Ghost (1942)
*Mexican Spitfires Elephant (1942)
*Mexican Spitfires Blessed Event (1943)

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 


 