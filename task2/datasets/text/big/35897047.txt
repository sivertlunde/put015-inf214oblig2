Veendum Kannur
{{Infobox film 
| name           = Veendum Kannur 
| image          = Veendum_Kannur.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster Haridas
 | producer      = Latheef Thirur 
| writer         = Robin Thirumala
 | screenplay    = Robin Thirumala 
| story          = Robin Thirumala 
| based on       =   
| narrator       =  Sandhya Rajeev Pillai
| music          =  Robin Thirumala 
| cinematography =  Jithu 
| editing        = 
| studio         =  Golden Wings International 
| distributor    = 
| released       =   
| runtime        = 
| country        = India 
| language       = Malayalam 
| budget         = 
| gross          = 
}}
 Kannur although the characters do not resemble.  Veendum Kannur is about how one man sets out to bring about a revolution in the communist party.

==Plot==
The film is set in Kannur, a veritable killing field for political rivals of different hues. Jayakrishnan (Anoop Menon) is the son of the Communist Party Secretary Madayi Surendran (Shivaji Guruvayoor) who is averse to any progress funded by multinational corporations. Jayakrishnan, a Jawaharlal Nehru University product who believes in non violent political ideology, dont like the way politics is practised and had once left the state disheartened with the developments. He is them goes to Agra and works as a guide for the Taj Mahal archaeological department. He returns to Kannur and falls in love with Radhika (Sandhya), who happens to be the daughter of Divakaran (Riza Bava), Surendrans biggest political rival. Jayakrishnans political views and this love affair leads to direct confrontation between the father and his more modern son.

Jayakrishnan, on his return, is given high security because of being the son of the party secretary, but is physically attacked by members of the opposition party on the very day of his arrival, which result in a child losing her limbs in a bomb blast. This flare up the fights between different party members of the district. Jayakrishnan finds his social sensibilities awakened all on a sudden, and he comes up with a unique plan that leads to the birth of the New Communist. Within no time, followers to the New Communist turn up in thousands, and Jayakrishnan faces stringent opposition as well. As the movement gain detractors and supporters from far and wide with his pages in Facebook and Twitter getting tons of hits, Jayakrishnan also opens up his development agenda as he starts to support the fabric corridor project proposed by an young industrialist named Mohit Nambiar (Rajeev Pillai) which is expected to give a fresh development initiatives for the men of Kannur. The Party fights it while Jayakrishnan offers it all support.

==Cast==
* Anoop Menon as Jayakrishnan Sandhya as Radhika
* Shivaji Guruvayoor as Madayi Surendran
* Rajeev Pillai as Mohit Nambiar
* Mithun Ramesh Voice For Rajeev pillai
* Riza Bava as Divakaran
* Tini Tom as Sugunan
* Sajitha Beti as Tara
* Arun
* Ambika Mohan Madayi Surendrans wife
* Manuraj
* Sadiq
* Zeenath
* Sajitha Betti

==Production==
The film was shot in Kannur, Ernakulam and Ottappallam and completed in about 25 days.    Haridas says the message of the film is that success could be achieved only through forgiveness and truth.    

==Reviews== City Journal said, "If Veendum Kannur is a thriller, it is a lacklustre one, which will neither excite nor move the audience."  Aswin J. Kumar of The Times of India said, "Even with the abundance of thriller elements, the movie fails to grip the audience for it relies heavily on Anoop Menon, who is heavily loaded with dialogues which he renders at a rapid pace and with vigour."  Paresh C Palicha of Rediff.com gave the film a   and said "it is better to watch a re-run of an old film on TV than Veendum Kannur."  Veeyen of Nowrunning.com gave the movie a   rating and commented, "I thought we were already done with such stale political capers that ruled the roost long, very long back! Looks like thats not the case, unfortunately!"  Contrastingly, IBN Live gave the movie an "average" rating and said, "Veendum Kannur has some lack luster moments, but manages to qualify for a one time watch." 

The film released in about 38 theatres across Kerala on 8 June 2012. In spite of poor reviews from the audiences and critics, the director says the film is doing well and he is planning to release it in another 22 theatres on 15 June. He added that a special screening of the film would soon be conducted for the members of the legislative assembly soon. 

==References==
 

==External links==
*  

 
 
 
 
 
 