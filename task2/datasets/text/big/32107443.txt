Umrao Jaan Ada (film)
{{multiple issues|

 
 
}}

Umrao Jaan Ada is about the 1972 Pakistani film directed by the Hassan Tariq. Umrao Jaan Ada is based on the 1905 novel Umrao Jaan Ada written by the Mirza Hadi Ruswa.  The film stars Rani, Shahid, Zamurrad and Aasiya.

== Plot ==
  classical music and Indian classical dance|dance. Together with the other apprentice tawaif and Gauhar Mirza, the mischievous illegitimate son of a local Nawab, she is taught to read and write in both Urdu and Persian language|Persian. As Umrao grows up, she is surrounded by a culture of luxury, music and poetry. She eventually gains her first client, (earning her the suffix of jaan) but prefers the impoverished Gauhar Mirza, her friend.

Umrao Jaan attracts the handsome and wealthy Nawab Sultan. The couple fall in love, but after a jealous customer tries to start a fight with Nawab Sultan, he shoots him and the jealous customer Zabardast Khan dies. He no longer comes to the kotha and Umrao Jaan must meet him secretly, by the help of Gauhar Mirza. As Umrao Jaan continues to see Nawab Sultan and also serve other clients, she supports Gauhar Mirza with her earnings. A new client, the mysterious Faiz Ali, showers Umrao Jaan with jewels and gold, but warns her not to tell anyone about his gifts. When he invites her to travel to Farrukhabad, Khanum Jaan refuses so Umrao Jaan must run away. On the way to Farrukhabad, they are attacked by soldiers and Umrao Jaan discovers that Faiz is a dacoit and all of his gifts have been stolen goods. Faiz Ali escapes with his brother Fazl Ali and she is imprisoned, but luckily one of the tawaif from Khanum Jaans kotha is in the service of the Raja whose soldiers arrested her so Umrao Jaan is freed. As soon as she leaves the Rajas court, Faiz Ali finds her and gets her to come with him. He is soon captured and Umrao Jaan, reluctant to return to Khanum Jaan, sets up as a tawaif in Kanpur. While she is performing in the house of a kindly Begum, armed bandits led by Fazl Ali try to rob the house, but leave when they see that Umrao Jaan is there. Then Gauhar Mirza comes to Kanpur and she decides to return to the kotha.
 mutiny is over. She meets the Begum from Kanpur again in Lucknow and discovers that she is actually Ram Dai. By a strange twist of fate Ram Dai was sold to the mother of Nawab Sultan and the two are now married. Another ghost of Umrao Jaans past is put to rest when Dilawar Khan is arrested and hung for robbery. With her earnings and the gold that Faiz Ali gave her, she is able to live comfortably and eventually retires from her life as a tawaif. 

== Cast ==
* Rani (actress)
* Shahid (actor)
* Zamurrad
* Aasiya
* Mumtaz (actress)
* Saeed Khan Rangeela
* Lehri (actor)
* Ladla
* Imdad Hussain
* Nayyar

== References ==
 

== External links ==
*  

 
 
 
 

 