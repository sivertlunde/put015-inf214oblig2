The Night My Number Came Up
 
 
{{Infobox film
|  name           = The Night My Number Came Up
|  image          = Nightmynumbercame_up.jpg
|  caption        = UK release poster Leslie Norman
|  producer       = Michael Balcon
|  writer         = R. C. Sherriff
|  starring       = Michael Redgrave Sheila Sim Alexander Knox Denholm Elliott
|  music          = Malcolm Arnold |
|  cinematography = Lionel Banes
|  editing        = Peter Tanner
|  studio         = Ealing Studios
|  distributor    = General Film Distributors (UK) Continental Film Distributors (US)
|  released       = 1955 (United Kingdom|UK)
|  runtime        = 94 min.
|  country        = United Kingdom
|  language       = English
|  budget         =
|}} Leslie Norman Air Marshal Sir Victor Goddard.  It was made by Ealing Studios.

==Plot summary== Dakota which crashes on a rocky shore. The Air Marshal is due to fly to Tokyo the following day, but is not disturbed because many of the details differ from his planned voyage.

However, as the flight proceeds circumstances change so that eventually all the details correspond to the dream. Moreover the plane does actually crash on a rocky shore in Japan.

==Cast==
*Michael Redgrave as Air Marshal Hardie
*Sheila Sim as Mary Campbell
*Alexander Knox as Robertson
*Denholm Elliott as Mackenzie
*Ursula Jeans as Mrs Robertson
*Ralph Truman as Lord Wainwright
*Michael Hordern as Commander Lindsay Nigel Stock as Pilot
*Bill Kerr as Soldier
*Alfie Bass as Soldier George Rose as Businessman
*Victor Maddern as Engineer

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 

 