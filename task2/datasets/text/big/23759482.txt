The Insomniac
 Insomniac}}
{{multiple issues|
 
 
}}

{{Infobox film
| name           = The Insomniac
| image          = The_insomniac_poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Madhav Mathur
| producer       = Madhav Mathur Varun Viswanath
| writer         = Madhav Mathur Jacob Thomas Lishi Lee
| music          = Siddesh Mukundan
| cinematography = K.M. Lo
| editing        = Varun Viswanath
| studio         = 
| distributor    = Bad Alliteration Films
| released       =  
| runtime        = 88 minutes
| country        = Singapore
| language       = English
| budget         = 
| gross          = 
}}
The Insomniac is a 2009 Singaporean film noir written and directed by Madhav Mathur. It is the debut feature length film under the Bad Alliteration Films independent production banner. The Insomniac premiered at the Sinema Old School Singapore-based independent distribution house, opening in the 100-seater venue on August 13, 2009, in Singapore.

The films narrative centers on Ali, a workaholic writer who is struggling to complete his latest work of fiction. His sleepless efforts slowly force him to lose his mind. The film was shot and edited in Film-noir style, relying on invoking surrealistic atmosphere and dark, ironic plot-ends.

==Synopsis==
 
Ali (Madhav Mathur), a possessed workaholic writer, is spiralling downwards into a permanent state of mental corrosion, in his ill-conceived insomniac efforts to complete his latest book of fiction. On one fateful night, his overactive imagination gets the better of him.

Characters and episodes from his unfinished book come to life in front of his weary eyes. They get strangely intertwined with people and experiences from his own life, resulting in absurd, tragic comedy. Tormented and chased by seductive assassins, loony psychiatrists, bullies and army men, he has only one person who can actually save him, and she too seems reluctant.
 

==Cast==
* Madhav Mathur as Ali
* Neeti Warrier as Rui
* Navneet Jagannathan as Nas
* Ipshita Bhattacharya as Lady in the robe Jacob Thomas as Zola
* Lishi Lee as Sharon

==Production==

===Timeline===
The film was shot in ten shooting days in May 2008. Editing, Visual Effects, Colour Grading, Background Score and Original Soundtrack were completed by May 2009.

===Resources===
Camera: Panasonic HPX502 (HD) with Fujinon Lens 
Recording Medium: P2 cards

Edited on: Adobe Premiere Pro CS3 
Special Effects on: Adobe Premiere Pro CS3 and Adobe After Effects CS3

Sound Mixing and Music Production: Sony ACID Pro 4.0

==Score and soundtrack==
The Background Score and Original Soundtrack were composed and produced by Siddesh Mukundan.

===Soundtrack===

OST Performed by 
Guitars, Synth, Percussion & Vocals:    Siddesh Mukundan                            
Drums & Percussion:    Sammy Arvis

"Man Who Does Not Sleep" 
"Zola" 
"Pagan" 
"Im losing" 
"Flogging" 
"Fat Pig Chase" 
"The Bobbies" 
"Darling" 
"Acid Trip" 
"Wake"

==External links==
 
*  
*   at Sinema Old School
*   at The Arts House, Singapore
*  

 
 
 
 
 
 
 
 
 