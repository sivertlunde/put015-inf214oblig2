Hexing a Hurricane
{{Infobox Film
| name           = Hexing a Hurricane
| image          = 
| image_size     = 
| caption        =  Jeremy Campbell
| producer       = ten18films
| writer         = 
| narrator       =  Chris Rose Angela Hill Garland Robinette Harry Anderson Irvin Mayfield Sallie Ann Glassman
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2006
| runtime        = 45 min.
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Jeremy Campbell and  distributed by the National Film Network. The films score was orchestrated by New Orleans artist Eric Laws.
 Voodoo Ceremony at the start of Hurricane Season asking spirits for protection from dangerous storms. After the ominous hurricane strikes a few weeks following the VooDoo service, the film follows locals affected.

==Cast == Chris Rose (The Times-Picayune|Times-Picayune columnist), Angela Hill (WWL-TV Channel 4 news anchor), Garland Robinette, (WWL (AM) radio talk show host), Harry Anderson (actor, former resident, former local club owner), Irvin Mayfield (musician), Sallie Ann Glassman (artist, Voodoo priestess), along with various people of New Orleans.

==Scenes==
Scenes in the film include:

* Members of La Source Ancienne Temple perform a voodoo ceremony asking powerful spirits to protect New Orleans from dangerous hurricanes. Lakeview to view their devastated home for the first time.
* New Orleans Cultural Ambassador (and Grammy nominee) Irvin Mayfield issues a call to action from beneath the unharmed statue of jazz great Louis Armstrong in the French Quarter.
* Acclaimed Times-Picayune columnist Chris Rose provides amusing and unfiltered commentary on the local media and regarding day-to-day life after the storm.
* Former sitcom star and longtime New Orleans resident Harry Anderson (Night Court, Daves World) trades in his Wednesday night comedy act at his French Quarter nightclub for a new role as "town hall" leader.
* Locally famous broadcasters (and former husband and wife co-anchors) Garland Robinette (the Big 870) and Angela Hill (WWL Channel 4) reunite to share a candid conversation together in Jackson Square about personal experiences during a disaster of global interest.
* Voodoo Priestess Sallie Ann Glassman explains why she believed in her protective ceremony for years before the storm hit. She openly admits "what happened" with last years service and tells a possible message from the spirits regarding the disaster. second line parade dedicated to the memory of "all brothers and sisters lost in Hurricane Katrina."
* The film also debuts Below Zero, a ballad by street musician "Willow" of the Willow Family Band. (William Kennedy)

==Awards==
* "Best American Documentary" nominee - Rome International Film Festival

 
 
 
 
 