Hounddog (film)
 
{{Infobox film
| name           = Hounddog
| image          = Hounddog poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Deborah Kampmeier Robin Wright Penn Raye Dowell Jen Gatien Terry Leonard
| writer         = Deborah Kampmeier
| starring       = {{Plainlist | 
* Dakota Fanning
* Isabelle Fuhrman
* Robin Wright Penn
* Piper Laurie David Morse
}}
| music          = Gisburg
| cinematography = Jim Denault
| editing        = Sabine Hoffmann
| studio         = Empire Film Group Hannover House
| distributor    = 
| released       =   
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = $3.75 million
| gross          = $131,961 
}} Robin Wright Penn, and Piper Laurie, among others. It is also Isabelle Fuhrmans debut film. Penn also serves as an executive producer. The film was produced by Raye Dowell, Jen Gatien, and Terry Leonard. It premiered in competition at the 2007 Sundance Film Festival, and was given a limited release in 11 North American theaters on September 19, 2008.
 abusive life through music of Elvis Presley." 

The film was panned by critics, due in part to a controversial scene in which Fannings character is raped.  The film was also a box office failure, grossing only $131,961, against an estimated $3.75 million production budget. 

==Plot==
Lewellen lives with her stern, religious grandmother, Grannie, who has taken it upon herself to raise the girl, as neither of Lewellens parents can provide her a stable home. Her father, Lou, loves her and tries to please her, giving her gifts such as Elvis Presley recordings. Although he battles with alcoholism, he tries his best to give Lewellen a stable home. He even tries to provide a motherly figure in Lewellens life by dating a mysterious girlfriend, Ellen, who promised one night to rescue Lewellen from life in the rural South should the relationship falter.  We later learn that Ellen is in fact Lewellens aunt, her mothers sister.

Lewellen is able to maintain her innocence by finding consolation in playing with her best friend Buddy, idling away her last pre-teen summer with typical outdoor rural pastimes such as swimming in the pond and exploring the woods, meeting a new friend, Grasshopper, while spending the summer with her grandparents. Lewellen is enchanted by her idol, Elvis Presley, who is making a homecoming tour in the South. Her town is one of the venue stops. Lewellen finds that singing Elvis music is a way to channel her trauma into something constructive and creative. Charles (Afemo Omilami) acts as a mentor, imparting wisdom of his snake handler religion to explain this emotional channelling to her — in other words, how to create something positive out of something venomous and deadly.

Lewellen is challenged by many problems besides living in a "broken home".  Ellen leaves one day and breaks Lewellens heart, burdening her with the responsibility to be a "mother" despite not having one herself. Her father suffers a terrible accident, and is handicapped to the point of infantile Mental retardation|retardation, but the thought of Elvis coming to town gives her the resolve to carry on despite this newest of many traumatic circumstances. Buddy tells Lewellen that Woodens Boy has an Elvis ticket and is willing to give it to her if she does her Elvis dance for him naked. When she finds out the deal, she questions doing such an act for a moment. She then agrees to do so, Woodens Boy then unzips his trousers, she asks for her Ticket, but Woodens Boy then rapes her.
 Hound Dog". He nurses her back to health. Ellen soon returns to the town to keep her promise to Lewellen. Lewellen bids farewell to her father and departs for a better life with her new mother.

==Cast==
* Dakota Fanning as Lewellen
* Isabelle Fuhrman as Wendy "Grasshopper"
* Piper Laurie as Grannie
* Jill Scott as Big Mama Thornton David Morse as Lou Robin Wright Penn as Ellen
* Christoph Sanders as Woodens Boy
* Cody Hanford as Buddy
* Afemo Omilami as Charles
* Ryan Pelton as Elvis Presley
* Sean A. Wallace as Boy

==Reception==
   trauma of the act,   it became known as the "Dakota Fanning rape movie" at the Sundance Film Festival.  Fanning expressed ire towards the attacks against her family, most of which she said were directed toward her mother. 
 North Carolina State Senator and minority leader Phil Berger called for all future films made in North Carolina to have their scripts approved in advance if they are to get the normal production subsidy from the state. Berger says that he has not seen the film but is acting in response to what he has read about it. 
 Review aggregation website Rotten Tomatoes gives the film a score of 15% based on reviews from 54 critics.  . 

Fanning was praised for her performance by Roger Ebert, who compared it to Jodie Fosters in Taxi Driver. 

===Box office===
In its opening weekend of September 19–21, 2008, the film took in $13,744 in 11 theaters. It grossed $131,961 in its entire run.   

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 