Vendetta (1986 film)
 
{{Infobox film
| name = Vendetta
| image =
| caption =
| writer = John Adams John K. Adams
| director = Bruce Logan
| producer = Jeff Begum Ken Dalton Ken Solomon
| starring = Karen Chase Sandy Martin Roberta Collins Marshall R. Teague Greg Bradford Will Hare
| cinematography = Robert C. New David Newman
| editing = Glenn Morgan
| ditributor = Concorde Pictures
| released = September 1986
| runtime = 89 min.
| country = United States English
| budget =
| gross =
| preceded_by =
| followed_by =
}}
Vendetta is a 1986 prison action movie starring Karen Chase and Sandy Martin.  It was released in September 1986.

==Plot==
Laurie Collins (Chase) is an attractive and highly successful stuntwoman in the motion picture industry.  Her job on one film takes her to a small town not far from where her younger sister Bonnie Cusack (Michelle Newkirk) lives.  Because of the close proximity, Laurie invites Bonnie to come visit her on the set, as she likely wont have time to visit her on her own.

Bonnie readily agrees, and during a night of partying, shes picked up by a man in a bar who then tries to rape her at gunpoint.  Having been taught some self-defense moves by her sister, Bonnie manages to overpower her attacker and kills him with his own gun.

Though the case seems like a sure open-to-shut one of self-defense, Bonnie is shocked when shes charged with murder, as she learns that her attacker was the son of an influential local politician.  Because Bonnie is from out of town, shes seen as the troublemaker and convicted on her charges.  The judge sentencing her states that while she may have seemed justified by her actions, she cannot take the law into her own hands, and is sentenced to a term in the state prison.

Bonnie learns the realities of prison life when she draws the sexual interest of Kay (Martin), an influential trustee inmate who soon shows herself to be capable of making life miserable for the youngster.  One night Bonnie is touched inappropriately by Kay, and fights back.  Bonnies victory is short-lived, however, as Kay and her gang assault her and inject her with heroin before dumping her over a railing several floors up and to the floor below, resulting in her death.

The next day, Laurie is called to claim and identify her sisters body.  She sees on the report that her sister had died of a drug overdose, but when she sees her sisters dead and visibly bruised body in the morgue, Laurie concludes that a cover-up is being engineered by the authorities, and is determined to find out the truth.

Laurie decides to get arrested and inside the prison herself by intentionally driving drunk.  Her efforts succeed, and she gets the same sentencing judge her sister had.  However, she is shocked when the judge gives her a suspended sentence because she is a first-time offender.  Outraged, Laurie proceeds to insult the judge and attempts to attack him.  She is dragged off to jail as the judge revokes her suspension.

It doesnt take long for Laurie to realize whos behind her sisters death once on the inside.  However, Kay doesnt make the same sexual advances on her as she had her sister.  Laurie finally gets Kays attention by killing Bobo, the first inmate whos part of Kays gang.

Throughout all of this is a subplot involving Miss Dice (Roberta Collins), a firm but benevolent guard at the prison who is aware of the corrupt circumstances at the prison and in the judicial system, but has little power to do anything about it.  When China, another one of Kays henchwomen is killed after Laurie attacks her at the prison pool, and then Star, who dies from a head injury after Laurie fights her in a prison shower, Dice takes an interest in the pattern and asks Kay whats going on.  An anguished Kay, who is unaware of Lauries relationship to Bonnie, claims not to know.  Later on, two more of her people die at Lauries hands, ultimately leaving Kay to face Laurie in a showdown of hand-to-hand combat.

Kay is stunned to discover Laurie is the one responsible for the killings and even more so when she reveals that Bonnie was her sister. Kay tells Laurie that Bonnies death was merely an accident and that she and her cohorts just roughed her up a little. Enraged, Laurie attacks Kay and proceeds to beat her into submission. While the older and heavier Kay is physically stronger and has considerable endurance for her age, she proves to be no match for Lauries stuntwoman training. Laurie quickly gains the upper hand and proceeds to try and choke Kay to death. But then, Laurie suddenly has an attack of conscience and cant bring herself to kill her. She leaves Kay on the floor and walks away, but Kay recovers and proceeds to chase Laurie through the prison. Kay taunts her the entire time, daring her to stand and fight. This proves to be a mistake as Laurie finally stops running and once again, Kay is soundly thrashed. Just as Laurie defeats Kay, leaving her battered and bloody on the floor, Miss Dice appears, armed with a shotgun and orders them both to stand down. Laurie does so, but then Kay rises up off the floor with a Wrench, posed to kill Laurie. Miss Dice quickly fires on Kay, gunning her down and saving Laurie.

Laurie stares in shock at Kays body and then back at Miss Dice, who looks back at her with sympathy. 
"Did it bring Bonnie back?" Miss Dice asks Laurie rhetorically, revealing she knew about what Kay and her people had done to her and the reason Laurie had gotten herself imprisoned.  "You have the rest of your life to think about that."  The scene then cuts to the movies final shot, showing Laurie being picked up from prison by her boyfriend and now living life as a free woman.

==External links==
*  

 
 
 
 
 
 
 