Peau d'Ange
 
{{Infobox film
| name           = Peau dAnge   Once Upon an Angel
| image          = 
| caption        = 
| director       = Vincent Perez
| producer       = Virginie Silla
| writer         = Karine Silla  Vincent Perez   Jérôme Tonnerre
| based on       = 
| starring       = Morgane Moré Guillaume Depardieu  Karine Silla
| music          = Alexandre Azaria     (as "Replicant")
| cinematography = Philippe Pavans de Ceccatty
| editing        = Laurence Briaud
| distributor    = EuropaCorp
| released       =   
| runtime        = 85 minutes
| country        = France 
| awards         = Swan dOr 2003
| language       = French   (English version subtitled)
| budget         =  €4,250,000
}}

Peau dAnge is a 2002 drama film directed by Vincent Perez.

==Synopsis==
Grégoire Guillaume Depardieu returns reluctantly to the countryside in order to oversee his mothers funeral. Being back in the small town where he was raised upsets him. He is not ready yet to accept the fact his mother died. While looking for some comfort and solace he meets a maid called Angèle Morgane Moré. He achieves to impress her by making up he was an important manager although he only has a minor position in a cosmetics company. After theyve spent the night in a hotel he just leaves her but she cannot forget him, in particular because he was her first lover at all. Subsequently she follows him and even manages to get hired as the maid of one of his colleagues. Unfortunately she then has to realise that Grégoire is about to become the son-in-law of his boss. Angèle observes how he is going to marry another woman for other reasons than love.

== Cast ==
* Morgane Moré : Angèle
* Guillaume Depardieu : Grégoire
* Karine Silla : Laure
* Magali Woch : Josiane
* Dominique Blanc : Catherine
* Valeria Bruni-Tedeschi : Angèles solicitor
* Olivier Gourmet :  Faivre
* Marine Delterme : Faivres wife
* Michel Vuillermoz : Grégoires uncle
* Esse Lawson : Amira
* Hélène de Saint-Père : Ms Artaud
* Laurent Terzieff : Mr Grenier
* Maryline Even : Jocelyne
* André Marcon : Angèles father
* Stéphane Boucher : Doctor Artaud

==Release==
The film was presented at the Montreal World Film Festival (24 August 2002) and the Festival International du Film Francophone de Namur (28 September 2002) and then released throughout Continental Europe. An English subtitled Direct-to-video version including a making-of, a soundtrack, deleted scenes, a film poster and two short films came out the following year (28 May, 2003) as "Once Upon an Angel".

==Accolades==
At the Cabourg Romantic Film Festival 2003 Morgane Moré was awarded a "Swan dOr" as best actress.

==Reviews==
 
 

==References==
 

==External links==
* 
* 
*  on Vincent Perez Official Homepage

 
 
 
 
 
 
 