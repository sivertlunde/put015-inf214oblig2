Rock and Roll's Greatest Failure: Otway the Movie
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Rock and Rolls Greatest Failure: Otway the Movie
| image          = RockandRollsGreatestFailure.jpg
| alt            = 
| caption        = 
| director       = Steve Barker
| producer       = Richard Cotton 
Karen Lawrence
| writer         =  Bob Harris Richard Holgarth Janey Lee Grace Alistair Mclean Jon Morter Dennis Munday Humphrey Ocean Nigel Reveler Mark Thomas Murray Torkildsen Barry Upton Johnnie Walker
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Rock and Rolls Greatest Failure: Otway the Movie is a feature-length documentary directed by Steve Barker about English singer-songwriter John Otway.
 

== Story ==
 The film starts with Otway on stage performing Beware of the Flowers (Cos Im Sure Theyre Gonna Get You, Yeah in front of over 20,000 people at an outdoor gig in his hometown of Aylesbury in 1978.  before fast-forwarding to 2012 where Otway is giving a talk to students at his old school on how to survive in the music industry. Full of contemporary footage and interviews with key figures throughout Otways career (including some footage of his mum explaining why she thought Otways career choice was an odd one to take given his lack of talent), the film charts Otways many - mostly failed - attempts to get chart success and the fame and fortune that comes with it and how his ever-loyal fanbase helped to get him what he most desired - a second hit.

Explaining how he first achieved success, Otway talks the audience through his performance on the BBCs Old Grey Whistle Test where, keen to make an impression, he jumped onto the amplifier of his guitarist Wild Willy Barrett only for the amp to fall with Otway crashing down onto the box raising the amp with his legs either side crushing his testicles in the process.  This act of careless disregard for his own safety led to Otways single Cor Baby Thats Really Free reaching number 27 in the UK Singles Chart {{cite book
| first= David
| last= Roberts
| year= 2006
| title= British Hit Singles & Albums
| edition= 19th
| publisher= Guinness World Records Limited
| location= London
| isbn= 1-904994-10-5
| page= 412}}  and a £250,000 recording contract with Polydor Records.  Polydor A&R man Dennis Munday explains that Polydor also signed The Jam around the same time but only paid £6,000. 

Following the hit, Otway and various contemporaries explain how he becomes increasingly desperate for a follow-up hit and the ways in which he tried and failed to get back into the charts. From releasing three copies of his single Frightened and Scared without any vocals to only allowing people into a gig if they had purchased a copy of DK50/80 and how on both these occasions he was thwarted by the Musicians Union.  The film then goes on to explain how failure after failure leads to Otway writing his first autobiography Cor Baby Thats Really Me: Rock and Rolls Greatest Failure {{cite book
| first= John
| last= Otway
| year= 1990
| title= Cor Baby Thats Really Me: Rock and Rolls Greatest Failure
| edition= 4th, Revised
| publisher= Karen Lawrence Glass
| location= London
| isbn= 978-0-956434-302 
| page= 192}}  and how this sees a resurrection of his career.

After years in the wilderness, Otway finds himself creeping back into the national consciousness with increasing press coverage, TV appearances and publicity stunts designed to return Otway to the top of the music industry. The film tells how he teams up with guitarist Richard Holgarth, who then gets a band together for Otways 2,000th gig at the London Astoria. Promoter Paul Clerehugh explains how the band gives Otway all his credibility back whilst Otway goes on to reveal his next big scheme - playing the Royal Albert Hall which is only "twice as big" as the Astoria. Here the film highlights just how limited Otways musical talent is as he struggles to sing along to the specially commissioned orchestration with his first band - the Aylesbury Youth Orchestra. The only time he gets it right is on the night in front of a sellout crowd - much to his relief. It was also around this time that Otway and his fans embarked on one of their biggest publicity stunts to date. Coming up to the millennium, the BBC conducted a poll to find out what the nation thought were the greatest lyrics of all-time. Otway fan Ali Mclean explains how it occurred to him that people voting were likely to vote for a number of different songs by various more successful artists, but if the Otway fans focussed their attention on just one song, they may just have a chance of getting into the top 10. The plan worked, with Beware of the Flowers, Cos Im Sure Theyre Gonna Get You, Yeah reaching number seven in the chart 
 Woolworths still refuse to stock Otways single and place their own choice of number nine in their displays.  The audience also find out how Otways Hit campaign inspires Jon Morter to try to get Rage Against the Machine the coveted Christmas number one in 2009. 
 Las Vegas, Tahiti and Sydney.  He also explains how he puts a £10,000 deposit down on hiring the large concert hall at the Sydney Opera House, which although expensive is still "significantly cheaper than hiring a jet". He also explains how contemporary artists such as Glenn Tilbrook and Steve Harley agree to come on the tour with him. Sadly, after initial enthusiasm for the world tour subsides, Otway is left with little choice but to cancel the tour as costs spiral and only half the plane is filled. 

Coming up to his 60th birthday in 2012, Otway announces he intends to produce and show a movie of career. The film thus ends on a high with Otway triumphantly walking along the red carpet in scenes shot on the morning of the producers premiere. He explains how the movie has been paid for by his fans snapping up tickets for a screening at Londons Odeon Leicester Square all of whom are credtted as producers in the films closing credits.

==Producers premiere==
To celebrate his 60th birthday, Otway booked the Odeon Leicester Square to screen the story of his career to his fans. The screening created cinematic history with the final scenes being filmed from the red carpet on the morning of the Producers Premiere and cut into the movie whilst the producers were watching the film.  Over 1,000 fans packed into the Odeon to watch the screening of their idol each of whom were credited as a producer in the films end credits.  The movie was hailed a great success by those who attended the Premiere. 

==Cannes Film Festival==
 In May 2013, after further editing, Otway took the film and a group of his fans to the Cannes Film Festival for the films international debut.  In order to promote the screening of the movie, Otways fans dressed up as their hero, donned Otway masks and marched down the Croisette before gathering on the red carpet.  

==Theatrical release==
Following the films success in Cannes, the movie got its theatrical release in June 2013 at Glastonbury Festival  and is being shown in selected UK cinemas throughout the summer. The film has received positive reviews and has been hailed by critics as warm, engaging and an enjoyable watch.   

==References==
 

==External links==
*  
*  

 
 
 