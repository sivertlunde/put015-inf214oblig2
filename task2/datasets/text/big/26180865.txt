Musical Varieties
 

{{Infobox Film
| name           = Musical Varieties
| image          = 
| image_size     = 
| caption        = 
| director       = 
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1948
| runtime        = 10 minutes
| country        =  English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 Rosemary Lane and Johnny Downs.

== Plot summary ==
Farm workers harvest the crop whilst singing. At night they meet at the barn dance for more singing and dancing. Later, a man and a woman declare their love for each other, that "you could have knocked me over with a feather". The pairs song number is imitated by a male trio, one impersonating the woman.

== Cast == Rosemary Lane
* Johnny Downs
* Radio Rogues
* Eddie Le Barons Orchestra

== External links ==
* 

 
 
 
 
 


 