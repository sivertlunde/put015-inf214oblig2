The Pale Light of Sorrow
{{Infobox film
| name           = The Pale Light of Sorrow
| image          = 
| caption        = 
| director       = Iulian Mihu
| producer       = 
| writer         = George Macovescu
| starring       = Violeta Andrei
| music          = 
| cinematography = Gábor Tarko
| editing        = 
| distributor    = 
| released       =  
| runtime        = 135 minutes
| country        = Romania
| language       = Romanian
| budget         = 
}}

The Pale Light of Sorrow ( ) is a 1981 Romanian drama film directed by Iulian Mihu. It was entered into the 12th Moscow International Film Festival where it won a Special Diploma.   

==Cast==
* Violeta Andrei
* Andrei Finti
* Florina Luican
* Gheorghe Marin
* Rodica Muresan
* Emanoil Petrut
* Alexandru Racoviceanu
* Geo Saizescu
* Siegfried Siegmund
* Savel Stiopul
* Liliana Tudor

==References==
 

==External links==
*  

 
 
 
 
 
 