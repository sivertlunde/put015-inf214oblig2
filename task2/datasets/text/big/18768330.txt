Not Now, Darling (film)
 
{{Infobox Film
| name           = Not Now, Darling
| image          = Not Now, Darling (film).jpg
| image_size     = 
| caption        =  David Croft
| producer       =  John Chapman Ray Cooney
| starring       = Leslie Phillips Moira Lister Julie Ege Jack Hulbert
| music          = Cyril Ornadel
| cinematography = Alan Hume John Rook
| editing        = Peter Thornton Dimension Pictures
| released       = 1973
| runtime        = 97 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}
 play of John Chapman and Ray Cooney. The plot is a farce centered on a fur coat shop in central London. A loosely releated sequel Not Now, Comrade was released in 1976.

It was the last film to feature appearances by Cicely Courtneidge and Jack Hulbert who had been a leading celebrity couple in the 1930s and 1940s.

==Cast & Characters==

* Leslie Phillips – Gilbert Bodley
* Julie Ege – Janie McMichael
* Joan Sims – Miss Tipdale
* Ray Cooney – Arnold Crouch
* Bill Fraser – Commissionaire
* Jack Hulbert – Commander Frencham
* Cicely Courtneidge – Mrs. Frencham
* Derren Nesbitt – Harry McMichael
* Barbara Windsor – Sue Lawson
* Moira Lister – Maude Bodley
* Trudi Van Doorn - Miss Whittington
* Jackie Pallo – Mr. Lawson

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 


 
 