Batang PX
 
{{Infobox film
| name           = Batang PX
| image          = 
| alt            =  
| caption        = 
| director       = Jose Javier Reyes
| producer       = Mhalouh Crisologo Trina N. Dayrit
| writer         = Jose Javier Reyes
| starring       = Patrick Garcia Zsa Zsa Padilla Edu Manzano Anna Larrucea
| music          = Nonong Buencamino
| cinematography = Ding Achacoso
| editing        = Danny Gloria
| studio         = 
| distributor    = 
| released       =  
| runtime        = 113 minutes
| country        = Philippines English Filipino Filipino
| budget         = 
| gross          = 
}}
Batang PX or "Fil-American kid" in English, is a 1997 dramatic film produced by Star Cinema. This award-winning movie was directed by an acclaimed writer and director, Jose Javier Reyes. Batang PX earned several awards and nominations from different award-giving bodies and was also responsible for bringing back Patrick Garcia to stardom.

==Synopsis==
The Philippines is considered as one of the biggest U.S. military bases in the world. It is not surprising that the population of Filipino-American children has risen. Most of them have fathers who abandoned them as soon as these fathers are re-stationed. Amboy, played by Patrick Garcia, is one of the Fil-Am kids patiently anticipating his fathers homecoming and yearning to meet him. Stories from Amboys mom are the only things he knows about his father. His wish is granted when he finally meets his father. However, things dont turn out the way they were planned.

==Casts==
* Patrick Garcia - Amboy / Christopher 
* Zsa Zsa Padilla - Tessie
* Edu Manzano - Danny
* Anna Larrucea - Angela
* Nida Blanca - Cedes
* Cherry Pie Picache - Sarah
* Laura James - Young Tessie
* Eula Valdez - Maribeth
* Piolo Pascual - Jessie
* Chubi del Rosario - Macoy
* J.R. Herrera - Chok
* Gilleth Sandico - Laura
* Joshua Spafford -Michael Dahoff
* Don Laurel
* Mon Confiado

==Awards==
*Patrick Garcia
** Movie Actor of the Year - Star Awards
** Best Performer - Young Critics Circle Awards
** Best Actor Nominee - FAP Awards
** Best Actor Nominee - Gawad Urian Awards
** Teenage Actor of the Year - Guillermo Mendoza Memorial
** Best Young Performer - Parangal ng Bayan
** German Moreno Youth Achievers Awardee - FAMAS Awards
** Favorite Actor of the Year - Peoples Choice Awards 
*Others
** Young Critics Circle Awards - Best Film
** Star Awards Actress of the Year: Zsa Zsa Padilla
** Star Awards Editor of the Year: Danny Gloria
** Star Awards Original Screenplay of the Year: Jose Javier Reyes
** Gawad Urian Best Actress (Pinakamahusay na Pangunahing Aktres): Zsa Zsa Padilla

==External links==
 

 
 
 
 
 
 

 