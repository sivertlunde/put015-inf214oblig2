The House I Live In (2012 film)
 
 
{{Infobox film
| name           = The House I Live In
| italic title   = no
| image          = TheHouseILiveIn poster.jpg
| image_size     = 
| caption        = 
| director       = Eugene Jarecki
| producer       = Eugene Jarecki   Melinda Shopsin
| writer         = Eugene Jarecki
| narrator       = 
| starring       = Nannie Jeter David Simon
| music          = Robert Miller
| cinematography = Sam Cullman   Derek Hallquist
| editing        = 
| distributor    = 
| released       =  
| runtime        = 108 minutes
| country        = United States   International   
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
The House I Live In, directed by Eugene Jarecki, is a 2012 documentary film about the War on Drugs in the United States. 

==Participants==
*Michelle Alexander (civil rights litigator and the author of "The New Jim Crow")
*Shanequa Benitez (resident of Cromwell Towers housing project, Yonkers, New York)
*The Honorable Mark W. Bennett (U.S. District Court Judge. Sioux City, Iowa)
*Charles Bowden (journalist covering drug war-caused violence on the U.S.-Mexico border)
*Mike Carpenter (Chief of Security, Lexington Corrections Center, Lexington, Oklahoma)
*Marshal Larry Cearley (Police Department, Magdalena, New Mexico)
*Eric Franklin (Warden, Lexington Corrections Center, Lexington, Oklahoma)
*Maurice Haltiwanger (sentenced to 20 years for crack cocaine distribution)
*Carl Hart|Dr. Carl Hart  (Professor of Clinical Neuroscience, Columbia University, New York)
*Nannie Jeter (resident of New Haven, Connecticut)
*Anthony Johnson (former, small-time drug dealer, Yonkers, New York) Gabor Maté (Hungarian-born physician specializing in the treatment of addiction - has been working in Vancouver, Canada for several decades)
*Mark Mauer  (Director, The Sentencing Project)
*Richard Lawrence Miller  (American historian and expert on the history of U.S. drug laws)
*Charles Ogletree (Jesse Climenko Professor of Law at Harvard Law School, and a former academic advisor to Barack and Michelle Obama)
*Kevin Ott (serving life without parole on drug charges, Lexington Correctional Center, Lexington, Oklahoma)
*Susan Randall  (Private investigator in Vermont - formerly a journalist/producer for National Public Radio, All Things Considered, Morning Edition, Vermont Public Radio, and a researcher and associate producer for the A&E series Biography)
*David Simon (creator of The Wire on HBO)
*Julie Stewart (President and founder of Families Against Mandatory Minimums aka FAMM)
*Dennis Whidbee (former drug dealer, and the father of Anthony Johnson)
*Officer Fabio Zuena (Providence Police Department, Rhode Island) David Kennedy (Professor of Criminal Justice at John Jay College of Criminal Justice in NYC)

==Reception==
The documentary has been well received. Among the review aggregators, Rotten Tomatoes gave it 95% based on 55 reviews  and Metacritic gave it 77/100 based on 24 reviews.  Roger Ebert says The House I Live In "makes a shattering case against the War on Drugs."  Peter Bradshaw  reviewed the film for The Guardian and summed it up as an "angry and personal attack on Americas war on drugs   contends it is a grotesquely wasteful public-works scheme". 

==Awards==
* January 2012: Won the   at the Sundance Film Festival.
* April 2014: Won a Peabody Award. 

==See also==
* List of American films of 2012
* Prison–industrial complex

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
{{Succession box
| title= 
| years=2012
| before=How to Die in Oregon
| after=Blood Brother}}
 

 

 
 
 
 
 
 
 