The Blood of Hussain
 
{{Infobox film
| name           = The Blood of Hussain
| image          =
| caption        =
| director       = Jamil Dehlavi
| producer       = Jamil Dehlavi
| writer         = Rafiq Abdullah Jamil Dehlavi
| cinematography = Walter Lassally
| distributor    = Parindah Films Ltd Cinegate Ltd.
| runtime        = 112 minutes
| released       =  
| country        = Pakistan English
| awards         =
}}
The Blood of Hussain is a 1980 Pakistani film directed by Jamil Dehlavi and released in 1980 (released in February 1981 in the UK). The film was banned throughout Pakistan as the military junta led by General Zia-ul-Haq toppled the government of Zulfiqar Ali Bhutto.

==Plot==
The film is about the life and death of Hussain, the youngest son of a Pakistani family. He meets a holy soothsayer who foretells his destiny, which is to liberate the poor and oppressed against a tyrannical government. The struggle parallels that of Imam Hussain of the 7th century who was martyred by Yazid I, the Umayyad Caliph.

==Cast==
*Aliya Begam
*Durriya Kazi
*Fauzia Zareen
*Kabuli Baba
*Khayyam Sarhadi
*Imran Peerzada
*Kika Markham
*Jamil Dehlavi
*Mirza Ghazanfar Baig
*Mubila
*Salmaan Peerzada
*Samina Peerzada Saqi
*Shoaib Hashmi
*Zil-e-Subhan

==Alternative titles==
The Blood of Hussain is known as To Aima tou agonisti in Greece, Husseins Herzblut in Germany and Le Sang dHussain in France.

==Banning of the movie==
The film was banned by the Pakistani military ruler General Zia ul-Haq, after he seized power in a coup de état and became President of Pakistan in 1977, as the film portrays a fictional military coup in a less than favourable light. The ban on the film has not been lifted, and the director later moved to the United Kingdom. The film was eventually released and shown on British television.

==Credits==
*Director: Jamil Dehlavi
*Production Company: Parindah Films Ltd
*Producer: Jamil Dehlavi
*Associate Producer: Sharira Masood, Sandra Marsh
*Production Accountant: Gerry Wheatley
*Production Manager: Kaiser Keg
*Assistant Director: Imran Peerzada
*Continuity: Omar Norman, Marian Nicholson
*Screenplay: Jamil Dehlavi
*Dialogue Writer: Raficq Abdulla
*Director of Photography: Walter Lassally, Jamil Dehlavi
*Assistant Camera: Ashiq Rahi
*Focus Puller: Tony Garratt
*Key Grip: Afzal Akhtar
*Electrician: Barkat Joseph, Mohammed Saeed
*Special Effects: Colin Arthur Sue Collins, Jamil Dehlavi
*cost: Rashida Masood
*Make-up: Colin Arthur
*Titles/Opticals: Les Latimer Film Associates
*Music Supervisor: Jamil Dehlavi
*Playback singer: Reshma
*Sound: Christian Wangler, Shahid Rasool
*Boom Operator: Javed Khan
*Dubbing Mixer: Paul Carr
*Sound Editor: Martin Evans
*Studio: Mark One Films Studios (London)

==Release==
*Release date: 1980
*Country: Pakistan
*System: N/A
*Format: 35mm Film
*Length: 10068 Feet
*Run time: 112.0 mins.
*Colour/Black and White: Colour
*Colour systems: N/A
*Sound/Silent: Sound
*Sound systems: N/A
*Language: Urdu, English, Punjabi and Seraiki

==TV Transmission==
*First Transmission date: February 16, 1983
*Country: Great Britain
*Series/Slot: N/A
*Start time: 22:00
*Stop time: 00:00
*Duration: 120 mins.
*Company: N/A
*Channel: Channel Four

*Second Transmission date: October 2, 1988
*Country: Great Britain
*Series/Slot: N/A
*Start time: N/A
*Stop time: N/A
*Duration: N/A
*Company: Channel Four
*Channel: N/A

==See also==
*Jamil Dehlavi

==External links==
* 
* 
* 

 
 
 
 
 