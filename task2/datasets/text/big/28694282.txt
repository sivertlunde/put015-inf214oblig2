Night Birds (film)
 
 
{{Infobox film
| name           = Night Birds
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Eichberg
| producer       = Richard Eichberg
| writer         = Victor Kendall Miles Malleson
| narrator       = 
| starring       = Jack Raine Muriel Angelus Jameson Thomas Eve Gray
| music          = John Reynders Heinrich Gärtner Bruno Mondi
| editing        = 
| studio         = British International Pictures (UK) Richard Eichberg-Film GmbH (Germany)
| distributor    = Wardour Films (UK)
| released       = 1930
| runtime        = 97 minutes
| country        = Weimar Republic United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Night Birds is a 1930 British-German thriller film directed by Richard Eichberg and starring Jack Raine, Muriel Angelus and Jameson Thomas.  A separate German language version Der Greifer was made at the same time.

==Cast==
* Jack Raine ...  Sgt. Harry Cross 
* Muriel Angelus ...  Dolly Mooreland 
* Jameson Thomas ...  Deacon Lake 
* Eve Gray ...  Mary Cross 
* Franklyn Bellamy ...  Charlo Bianci 
* Garry Marsh ...  Archibald Bunny 
* Frank Perfitt ...  Inspector Warrington 
* Hay Petrie ...  Scotty  
* Harry Terry ...  Toothpick Jeff 
* Margaret Yarde ...  Mrs. Hallick 
* Ellen Pollock ...  Flossie 
* Cyril Butcher ...  Dancer Johnny 
* Barbara Kilner ...  Mabel

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 



 
 