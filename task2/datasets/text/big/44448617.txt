ToryBoy The Movie
{{Infobox film
 | name           = ToryBoy The Movie
 | image          =  
 | caption        = Theatrical release poster John Walsh
 | studio         = Walsh Bros
 | distributor    = Fremantle Media 3DD Productions
 | released       = May 11, 2011
 | runtime        = 100 min.
 | country        = United Kingdom
 | language       = English
}} John Walsh. Conservative Party Labour Party 2010 General Election,  John Walsh decided to do just that and document his experiences on camera. His film made the case that Stuart Bell should not be the Member of Parliament (MP) of Middlesbrough. As part of his campaign, he showed the finished film in a free showing a year after the election.  He came in third in the election; Stuart Bell remained the MP of Middelsbrough.  but increased the Conservative vote in the to its highest in thirty years with an 18.8% share of the vote. 

In the film, Walsh claims that Sir Stuart Bell, the Labour Partys MP in Middlesbrough, is absent so often from Middlesbrough that he is an unsuitable candidate. According to Richard Moss, a BBC film critic, "some silliness aside, it is a thought-provoking insight into the way our political system works or doesnt work on the ground."  

==Awards==
In 2011 the film was nominated for the Grierson Awards for "Best Documentary on a Contemporary Theme". 

==Controversy== Sir Stuart Bell to face formal party disciplinary procedures.  The Labour Party decided to have an official investigation into the allegations.  Other political public figures joined the debate. Lord Michael Ashcroft “I’d defy anyone with an interest in politics not to enjoy this film”.  Sir Stuart Bell did have a right of reply which appeared in the short film ToryBoy The AfterMath.  The films expose nearly forced a By Election.  
The films re-release in 2015 caused more controversy after it was banned from being shown in Middlesbrough.  
 
   

==Re Release==
The film received a re-release in cinemas in 2015 in the lead up to the 2015 UK General Election.   The Radio Times film review by Jeremy Aspinall on the films re-release says "“A revealing and droll fly-on-the-wall documentary. Its a fascinating odyssey illuminated by pithily informative animations. Who would have thought politics could be so entertaining?”  

==References==
 

==External links==
*  
*  

 
 
 
 
 