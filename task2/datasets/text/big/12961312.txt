Love and Duty (1916 film)
 
{{Infobox film
| name           = Love and Duty
| image          =
| caption        =
| director       = Will Louis
| producer       = Louis Burstein
| writer         =
| starring       = Oliver Hardy
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 11 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
}}
 silent comedy film featuring Oliver Hardy.

==Cast==
* Oliver Hardy - Pvt. Plump (as Babe Hardy)
* Billy Ruge - Lt. Runt
* Bert Tracy - Col. Tracy
* Florence McLaughlin - His daughter (as Florence McLoughlin)
* Ray Godfrey - The sweetheart of the regiment

==See also==
* List of American films of 1916
* Oliver Hardy filmography

==External links==
* 

 
 
 
 
 
 
 
 

 