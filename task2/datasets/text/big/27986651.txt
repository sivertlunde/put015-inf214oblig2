Doraemon: Nobita and the New Steel Troops—Winged Angels
{{Infobox film
| italic title   = force
| name           = Nobita and the New Steel Troops: ~Winged Angels~
| image          = Doraemon_Nobita_and_the_New_Steel_Troops.jpg
| alt            =
| caption        = Theatrical Poster
| director       = Yukiyo Teramoto
| producer       = Maiko Sumida Momoko Kawakita Rika Tsuruaki Shunsuke Okura Takumi Fujimori
| writer         = Yuichi Shinbo Chiaki   Miyuki Sawashiro   Yumiko Kobayashi   Kouji Katou   Masaharu Fukuyama   Kotono Mitsuishi   Yasunori Matsumoto
| music          = Kan Sawada
| cinematography = Katsuyoshi Kishi
| editing        = Keiki Miyake Riko Fujimoto Toshihiko Kojima
| studio         = Shin-Ei Animation
| distributor    = Toho
| released       =  
| runtime        = 108 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          = US$ 42,260,430
}}
  is an anime  , and is the first Doraemon film ever to be released in 3-D film|3-D. It was also released in India. This movie was ranked 16th highest grossing animated movie of Japan.
==Plot== RC toy to upstage Suneo, who was showing off his own toy. Nobitas fit irritates Doraemon as he uses his Anywhere Door to get anyway from the summer heat cool off at the North Pole. Sometime later, Nobita follows and a discovers a strange bowling ball-like orb which starts blinking with a pulsating light, and summons what looks like a giant robot´s foot. After Nobita uses the foot to sled down, crashing into his room through the Anywhere Door, the bowling ball follows him home through the door and another robot piece falls into his backyard. A frozen Doraemon follows soon after, covered in ice before being thawed out and with a cold. Learning of the robot parts, Doraemon admits to Nobita that he has nothing to do with it as the two use the Opposite World Entrance Oil and the Roll-Up Fishing Hole to enter the World Inside the Mirror, an alternate mirror world without people. There, they built the robot which Nobita christens "Zanda Claus" as he believed the sphere summoning the parts is from Santa Claus.
 brain wave telepathic messages to a mysterious girl named Lilulu. The actual owner of Zanda Claus, Lilulu seeks out Nobita when he accidentally lets slip all that he knows about the robot. After Lilulu proceeds to force him into showing her where it is, Nobita borrows the Roll-Up Fishing Hole from the spare pocket Doraemon keeps in the closet to take her to the World Inside the Mirror. She reclaims Zanda Claus while getting Nobita to let her borrow the Roll-Up Fishing Hole for a while.
 shooting stars in a row, he uses the Bamboo Copter to investigate the forest at Mt. Ura. There, Nobita follows another shooting star through the unrolled Roll-Up Fishing Hole and finds Lilulu building a massive robot army. Doraemon, having been suspicious of Nobitas peculiar behavior at home and having followed him there, uses a long-range Paper Cup Phone to listen in on Lilulu as she is revealed to be a human-hating robot .When Lilulu discovers them, Nobita and Doraemon escape to their own world with the portal accidentally destroyed by Zanda Claus.
 incubator and puts the sphere with the Translation Jelly in it, causing it to hatch into a yellow chick that is named "Pippo" (one of the onomatopoeia s to describe the sound a peep makes in Japanese). Nobita becomes good friends with Pippo, and Shizuka takes care of fixing Lilulu when she becomes very badly damaged. Lilulu had some traumatic experiences as a child, and she holds a very deep distrust and resentment towards humans. The only individual with whom she feels connected to is Pippo, whom she fixed "on a whim" after he was broken by the other robots. Despite everything Nobita and the others do for her, Lilulu shoots Nobita with a laser beam from her finger. Pippo jumps in front of Nobita and becomes very badly injured, which gives Lilulu a wake-up call as she decides to help the humans while not disclosing the reason for the lack of people. However, the commander and the rest of the robot army chain her up for her heresy and proceed to torture her for what she knows. Luckily, Nobita, Doraemon, and the others arrive with Zanda Claus and rescue her. Back in the real world, Lilulu still feels conflicted as her willing allows Doraemon lock her up in a birdcage using the Small Light.
 satellite image of the world and comparing the image to another image of the current world, and seeing how they are reversed. They return to the lake where they first entered the fake world, which they believe is the connection doorway.  Doraemon and the group intercept the army at the lake.

Lilulu and Shizuka remain at Shizukas house to have a talk, which gives Shizuka a brilliant idea to save the world. She re-enlarges Lilulu, using the Enlarging Torch Light, and they both use the Walk-In Mirror to return to the real world.  Using the time machine, they return to 30,000 years ago on Mekatopia, attempting to talk to the professor who created the robots from which the robot army is descended. The professor plans to redo everything by removing the competition instinct from his robots, replacing those instincts with instincts of humanity and love. He collapses before he can finish his job. Lilulu, in order to complete the salvation, disregards the fact that she and Pippo will disappear after they redo history, and she continues the reprogramming with instructions from the professor.

Back on Earth in the present time, the robot army, larger in number, has taken the upper hand.  Zanda Claus is heavily damaged in the process of destroying the leading ship of the robot army. Lilulu, back at Mekatopia, has nearly finished her job when the professor dies. At first, she doesnt know what to do, then she figures out that she needs only to add her feelings, her love from Nobita and his friends, including Pippo. The job is completed just in time.  The robot army is reinforced and attacks en masse. The reprogramming is successfully completed, and the robot army is completely erased, as are Lilulu and Pippo. Shizuka uses the Anywhere Door to return to Earth, rejoining her friends with sorrow.

The next day, the group is back in the real world, and Nobita is back in school. This time, he chooses to stay, and Doraemon arrives to talk to him before going to the baseball field. While Nobita wonders if Lilulu and Pippo may ever be resurrected, a shadow crosses his eyes and Lilulu appears with wings on her back.  The wings take the appearance of Pippo in the form of a giant Phoenix (mythology)|Phoenix. They visit Nobita with cheers, then vanish once again into the air. Nobita believes it was Lilulu and Pippo who appeared, and he runs to the field to tell his friends.

== Cast ==
*Wasabi Mizuta
*Megumi Oohara
*Yumi Kakazu
*Subaru Kimura
*Tomokazu Seki
*Chiaki
*Miyuki Sawashiro
*Yumiko Kobayashi
*Kouji Katou
*Masaharu Fukuyama
*Kotono Mitsuishi
*Yasunori Matsumoto

== Characters ==
*   - a robot with similarities to the Hyaku Shiki, from Yoshiyuki Tominos Mobile Suit Zeta Gundam.
* Pippo&nbsp;— Zanda Claus brain part, resembling a chick. He and Lilulu change sides and help the humans. In the end, he is shown in the form of a giant bird.
*Lilulu&nbsp;— A mysterious gynoid built with the appearance of a teenage girl. She is sent as a spy by the soldiers of Mechatopia. She changes sides and helps the humans defeat the robot army. She and Pippo are later shown to be reborn. Her name is spelt as Lilulu (in original 1986 movie and subtitles), Riruru (in translation and audio) and Lillele (in Doko Demo Doa Manga Scanlation).

== Release ==
This film was released in Indian theaters on 6 October 2011, and was aired on   on 3 May 2013 as Doraemon in Nobita and The Steel Troops - The New Age.

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 