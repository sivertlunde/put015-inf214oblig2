Poesaka Terpendam
{{Infobox film
| name           =Poesaka Terpendam
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = 
| producer       = Tan Khoen Yauw
| screenplay     =
| narrator       = 
| starring       ={{plain list|
*Roekiah
*Djoemala
}}
| music          = 
| cinematography = 
| editing        = 
| studio         = Tans Film
| distributor    = 
| released       =  
| runtime        = 
| country        = Dutch East Indies
| language       = Indonesian
| budget         = 
| gross          = 
}}
Poesaka Terpendam (  produced by Tans Film and starring Roekiah, Djoemala, and Kartolo.

==Plot==
Two young men, Agoes and Badjoel, travel from Palembang to West Java and are told of the beauty of two sisters, Zaenab and Djoeleha, who live in Cicadas village with their father. The friends decide to visit Cicadas. Meanwhile, a local bandit leader named Ramelan has asked for Zaenabs hand in marriage. She and her father Ardi refuse, and Ramelan is forced to leave.

Through an old letter found on Ardi, Ramelan learns of a buried treasure containing some 30,000 Netherlands Indies gulden|gulden. He decides to dig it up, kidnapping Zaenab and Djoeleha in the process. Seeing this, Agoes and Badjoel follow Ramelan to his gangs hideout in a cave and fight to save the treasure and girls.

After rescuing the girls and defeating the bandits, Agoes and Badjoel are allowed to marry Zaenab and Djoeleha. The treasure, meanwhile, is divided amongst them. 

==Production== Rd Djoemala. It also featured Titing, Kartolo, Ismail, Ramli, and Wolly Sutinah.  Shooting for the film, originally titled Poesaka Pendeman, began in June 1941.  Scenes for this black-and-white film were shot in Banten, including Lake Tasikardi in Serang. The film featured several kroncong songs, some sung by Roekiah. 

==Release and reception==
Poesaka Terpendam was released in October 1941, near the Eid ul-Fitr holiday.  Pertjatoeran Doenia dan Film wrote that it was the only film to date in which Djoemala seemed out of place; the reviewer wrote that Djoemala seemed overly stiff and unnatural during the fight scenes.  A review in De Indische Courant praised the films scenery. 

The film is likely lost film|lost, as are all Indonesian films from before 1950 according to American visual anthropologist Karl G. Heider.  Movies produced in the Indies were shot on highly flammable nitrate film, and after a fire destroyed much of Produksi Film Negaras warehouse in 1952, old films shot on nitrate were deliberately destroyed.  However, Kristanto records several as having survived at Sinematek Indonesias archives, and film historian Misbach Yusa Biran writes that several Japanese propaganda films have survived at the Netherlands Government Information Service. 

==References==
 

==Works cited==
 
*{{cite book
 |title= 
 |trans_title=History of Film 1900–1950: Making Films in Java
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |author-link=Misbach Yusa Biran
 |publisher=Komunitas Bamboo working with the Jakarta Art Council
 |year=2009
 |isbn=978-979-3731-58-2
 |ref=harv
}}
*{{cite book
 |title=Indonesia dalam Arus Sejarah: Masa Pergerakan Kebangsaan
 |trans_title=Indonesia in the Flow of Time: The Nationalist Movement
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |chapter=Film di Masa Kolonial
 |trans_chapter=Film in the Colonial Period
 |author-link=Misbach Yusa Biran
 |publisher=Ministry of Education and Culture
 |year=2012
 |volume=V
 |pages=268–93
 |isbn=978-979-9226-97-6
 |ref=harv
}}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G.
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
*{{cite journal
 |title=Ismail Djoemala: Dari Doenia Dagang ke Doenia Film
 |trans_title=Ismail Djoemala: From Enterprise to Film
 |language=Indonesian
 |work=Pertjatoeran Doenia dan Film
 |issue=9
 |location=Batavia
 |volume=1
 |date=February 1942
 |pages=7–8
 |ref= 
}}
*{{cite journal
 |title=Poesaka Terpendam
 |trans_title=Buried Treasure
 |language=Indonesian
 |work=Pertjatoeran Doenia dan Film
 |issue=4
 |location=Batavia
 |volume=1
 |date=September 1941
 |pages=40
 |ref= 
}}
* {{cite web
  | title = Poesaka Terpendam
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-p016-41-849090_poesaka-terpendam
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 27 July 2012
  | archiveurl = http://www.webcitation.org/69ShkS0Ao
  | archivedate = 27 July 2012
  | ref =  
  }}
*{{cite news
 |title=Sampoerna: „Poesaka terpendam”
 |trans_title=Sampoerna: Poesaka Terpendam
 |language=Dutch
 |work=De Indische Courant
 |location=Surabaya
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011176339%3Ampeg21%3Ap002%3Aa0062
 |date=10 October 1941
 |page=2
 |ref= 
}}
*{{cite journal
 |title=Warta dari Studio
 |trans_title=Reports from the Studios
 |language=Indonesian
 |work=Pertjatoeran Doenia dan Film
 |issue=2
 |location=Batavia
 |volume=1
 |date=July 1941
 |page=27
 |ref= 
}}
 

 
 
 