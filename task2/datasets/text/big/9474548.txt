Shalako (film)
{{Infobox film
| name           = Shalako
| image          = Shalako68.jpg
| image_size     =
| caption        = original film poster by Tom Chantrell
| director       = Edward Dmytryk
| producer       = Euan Lloyd
| writer         = J.J. Griffith Hal Hopper
| starring       = Sean Connery Brigitte Bardot Stephen Boyd Jack Hawkins Honor Blackman
| music          = Robert Farnon
| cinematography = Ted Moore
| editing        = John D. Guthridge Bill Blunden Palomar Pictures CCC  MGM (2004 and 2009, DVD)
| released       = 1968
| runtime        = 113 minutes
| country        = United Kingdom
| language       = English
| budget         = $1,455,000 
| gross          = $1,310,000 "ABCs 5 Years of Film Production Profits & Losses", Variety, 31 May 1973 p 3 
}} 1968 Western film directed by American Edward Dmytryk, starring Sean Connery and Brigitte Bardot. The British production was filmed in Almería, Spain.

The cast also includes Stephen Boyd, Jack Hawkins and Honor Blackman, Connerys co-star in Goldfinger (film)|Goldfinger. It is based on a novel by Louis LAmour.

== Plot summary ==
In 1880 in New Mexico, guide Bosky Fulton leads a hunting party composed of European aristocrats into Apache territory. When a French countess, Irina Lazaar, wanders off alone, she is confronted by Apache men on horseback. She is rescued by Shalako, a former cavalry officer in the American Civil War, sent by the Army to guide the party off Indian land.

The leader of the party, Frederick von Hallstadt, refuses to go, and the Apache retaliate by raiding the party. Although their lives are gravely threatened, Fulton makes off with the stage coach, ammunition and supplies, as well as Sir Charles Daggetts unfaithful wife, Lady Julia. With the party stranded, Shalako tries to lead them on foot to safety at an Army fort. 

The chiefs son, Chato, attacks the stage coach. The Apache make Lady Julia swallow her jewels, killing her. Fulton flees, but when he reaches the hunting party, Daggett shoots him to death. 

After catching up with the hunting party, Chato accepts Shalakos challenge to a one-on-one fight using spears. He is about to be defeated when his father, the Apache chief, intervenes. He offers safe passage to Shalako and the others if his sons life is spared. With the party safe, Shalako rides off alone into the Western landscape, soon accompanied by Countess Irina.

== Production ==
Producer Euan Lloyd was introduced to Louis LAmour, author of numerous Western adventure novels, by his actor friend Alan Ladd. Over the years as Lloyd dreamed of becoming an independent producer, he kept in touch with LAmour. He wanted to film his 1962 novel Shalako (novel)|Shalako. At one time Lloyd had lined up Henry Fonda and Senta Berger to star in the film, planning to shoot it in Mexico. Lloyd recounted that, at the time, many film distributors were reluctant to back a film starring Fonda, and increases in the cost of filming in Mexico made it impossible to pursue. 
 New York On Her Bob Simmons.
 The Gunfighter, which was believed to have discouraged some of the public from attending. They feared the same might happen with Shalako.

Almería province was a favoured location for filming spaghetti Westerns. But, when Shalako was in production, Harry Saltzmans Second World War film, Play Dirty, set in the Libyan Desert, was being filmed on the same locations. One film crew had to wipe out the tyre tracks in the sand before filming the Old West, whilst the other crew had to pick up the horse droppings before they shooting for the Second World War battles. Once the gypsy Apaches, mounted on horseback, rode by mistake headlong into an attack on a Long Range Desert Group.   
 Don Barry.

==Cast==
* Sean Connery as Moses Zebulon Shalako Carlin  
* Brigitte Bardot as Countess Irina Lazaar  
* Stephen Boyd as Bosky Fulton  
* Jack Hawkins as Sir Charles Daggett  
* Peter van Eyck as Baron Frederick von Hallstadt  
* Honor Blackman as Lady Julia Daggett  
* Woody Strode as Chato (Apache chief)  
* Eric Sykes as Mako  
* Alexander Knox as Sen. Henry Clarke   Valerie French as Elena Clarke
* Julián Mateos as Rojas  
* Don Red Barry as Buffalo  
* Rodd Redwing as Chatos Father

== Critical response ==
The film premiered in late 1968 to mixed reviews. Some critics thought the film was not as good as the other Westerns being made in Europe, in particular, the Italian westerns (known as "spaghetti Westerns") by which Sergio Leone, Lee Van Cleef and
Clint Eastwood were building their reputations. 
===Box office===
Shalako was the 18th most popular film of 1969.  It recorded admissions of 1,385,466 in France.  However, according to Variety (magazine)|Variety, because of its high costs the film recorded a loss of $1,275,000.  

Later, Shalako became a cult classic; it has been regarded as having the hallmarks of a European Western.
 Diamonds Are Forever.

==DVD== A Bridge Too Far.

== Notes ==
 
 

== External links ==
*  

 

 

 
 
 
 
 
 
 
 
 
 