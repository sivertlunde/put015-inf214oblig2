The Incredible Kung Fu Master
 
 
 
{{Infobox film
| name           = The Incredible Kung Fu Master
| image          = TheIncredibleKungFuMaster.jpg
| alt            = 
| caption        = Film poster
| film name = {{Film name| traditional    = 肥龍功夫精
| simplified     = 肥龙功夫精
| pinyin         = Féi Lóng Gōng Fū Jīng
| jyutping       = Fei4 Lung4 Gung1 Fu1 Zing1}}
| director       = Joe Cheung
| producer       = Joe Cheung
| writer         = Joe Cheung
| starring       = Sammo Hung Stephen Tung Cecilia Wong
| music          = Chow Fuk Leung
| cinematography = Ricky Lau Lau Koon Chiu
| editing        = Marco Mak
| studio         = First Films Hong Kong Wai Kuen Films
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$2,072,922.70
}} 1979 Martial martial arts action comedy film directed by Joe Cheung and starring Sammo Hung and Stephen Tung, the latter in his first leading role. This film features an action direction from a number of famous action directors including Sammo Hung Stunt Team, Lam Ching-ying, Billy Chan, Bryan Leung and Yuen Biao.

==Plot==
Two brothers Lee Chun Fei (Wong Ha), a Wing Chun stylist, and Lee Chun Pang (Peter Chan), a Five Animals stylist, encounters a town bully Yeung Wai (Lee Hoi Sang) harassing a civilian. Together, with their own distinctive kung fu style, the brothers defeat Yeung and are praised by the townsmen. Later, when the townsmen debate on who is the better martial artist of the two, the brothers become bitter rivals and start their own martial arts schools. Then a rich man Chin Fung (Phillip Ko) hires the two brothers to teach his sons, the crooked-eyed Big Dog (Addy Sung), and Little Dog (Chung Fat). Big Dog becomes Feis disciple and Little Dog becomes Pangs disciple. Meanwhile, a local young kung fu fanatic Sei Leng Chai (Stephen Tung) is trying to create his own kung fu style and befriends a wandering winemaker Fei Chai (Sammo Hung), who is an expert martial artist. Fei Chai advises Sei Leng Chai to learn every style of kung fu he can and he starts studying from Fei Chai, Fei and Pang. Later, it is revealed that Yeung is actually Chins kung fu senior and he sent his fake sons to study under them to understand their styles more.  Chin wants to cripple Fei and Pang to avenge Yeung. As they fight, Sei Leng Chai comes to help and the brothers eventually reconcile their brotherhood. Sei Leng kills Chin and Yeung arrives to defeat and capture Sei Leng Chai while Fei Chai comes to the rescue and kills Yeung.

==Cast==
*Sammo Hung as Fei Chai
*Stephen Tung as Sei Leng Chai
*Cecilia Wong as Lee Ching Ching
*Philip Ko as Chin Fung
*Lee Hoi Sang as Yeung Wai
*Mang Hoi as Hoi
*Adam Sung as Big Dog
*Chung Fat as Little Dog
*Wong Ha as Lee Chun Fei
*Peter Chan as Lee Chun Pang
*Austin Wai as Invincible
*Ho Pak Kwong as Grain store boss
*Fung King Man as Three shells man
*Lam Ching-ying as kung fu student
*Tsang Cho Lam as gambler Mars as one of Yeung Wais men
*Billy Chan as Snake Fist stylist
*Yuen Lung Kui as one of Yeung Wais men
*Wellson Chin as student
*Lai Kim Hung as student
*To Wai Wu as student
*Cheung Chok Chow as villager
*Yeung Sai Kwan as one of Yeung Wais men
*Lung Ying
*Cheng Sek Au as gambler
*Chow Kam Kong
*Ka Lee as student
*Chan Ming Wai as student
*Leung Hung as gambler
*Ho Lai Lam as student
*Tu Chia Cheng
*Johnny Cheung

==Box office==
The film grossed HK$2,072,922.70 at the Hong Kong box office during its theatrical run from 26 January to 7 February 1979 in Hong Kong.

==See also==
*Sammo Hung filmography
*Yuen Biao filmography

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 
 
 
 