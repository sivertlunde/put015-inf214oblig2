Laughing Gravy
 
{{Infobox Film
  | name = Laughing Gravy
  | image =Laughinggravytitlecard.jpg
  | caption = 
  | director =  James W. Horne
  | producer = Hal Roach
  | writer = H.M. Walker Stan laurel  (uncredited) 
  | starring = Stan Laurel   Oliver Hardy
  | music = 
  | cinematography = 	
  | editing = 
  | distributor = Metro-Goldwyn-Mayer
  | released =  
  | country = United States
  | runtime = 31 38" (English) 62 29" (Spanish) 64 45" (French)
  | language = English Spanish French
  | budget = 
}}
Laughing Gravy is a 1931 short film comedy starring Laurel and Hardy. It was directed by James W. Horne, produced by Hal Roach and distributed by Metro-Goldwyn-Mayer.

== Synopsis ==
On a snowy winters night, Laurel and Hardy try to keep their pet dog Laughing Gravy hidden from their landlord, mostly without success.  The landlord eventually orders them to leave, but fate takes a hand.

== Cast ==
* Stan Laurel
* Oliver Hardy Charlie Hall
* Harry Bernard

== Remakes ==
The film is a remake of Laurel and Hardys final silent film Angora Love, made in 1929. Charlie Hall played a tenant in the original; he plays the landlord in the remake. Harry Bernard also played the policeman in Angora Love as he does here.

The Chimp, released the following year, is also a partial remake.

== Alternate versions ==
The film has three versions. A two-reel black-and-white version lasting approximately 20 minutes, a three-reel black-and-white version lasting approximately 30 minutes, and a three-reel colourised version.

There is also a "feature" version joining this film and Be Big!, by a title card stating that Laurel & Hardy were divorced by their wives for what happened and wind up in the dingy rooming house.

The three-reel version was the original one, as Hal Roach had tried to switch to the three-reel format for Laurel & Hardy shorts, starting with The Laurel-Hardy Murder Case. Just after it was completed, MGM asked Roach to limit short films to two reels, and an alternate ending was shot instead. The three-reel version had already been previewed, and a work print was rediscovered in 1985.

The two-reel version and the first two reels of the three-reel version are identical except that the two-reel version ends with the landlord committing suicide when, due to his residence being quarantined, he finds he is unable to evict Stan and Ollie. In the three-reel version, Stan receives a telegram just before they are evicted that says he is to inherit a vast fortune, but only if he leaves Ollie behind forever.

== Foreign versions ==
Laughing Gravy was filmed in two extended foreign language versions immediately upon completion of its English incarnation. These foreign versions combined the story of the English original with that of Be Big!, another short from the same year.

Les Carottiers was the extended French version; it replaced Isabelle Keith with Germaine de Neel as Mrs. Hardy and Jean De Briac in Baldwin Cookes role of "Cookie." The Spanish version, Los Calaveras, featured Linda Loredo as Mrs. Hardy.

Laurel and Hardy delivered their French and Spanish lines phonetically from cue cards in both foreign versions. In the preceding Be Big! Anita Garvin played Mrs. Laurel in all three films; she mouthed her foreign lines phonetically, on-camera but off-mic, while a voice actress just off-camera spoke into a "hot" mic. Randy Skretvedt|Skretvedt, Randy (1996). Laurel & Hardy: Magic Behind the Movies. Beverly Hills, CA: Past Times Publishing. ISBN 0-940410-29-X, p. 211. 

== References ==
 

== External links ==
*   – describing the Spanish version, which, following the expulsion of Stan and Olliie from their home in Be Big! has a plot largely identical to the shorter English version; the description also covers the preceding Be Big!
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 