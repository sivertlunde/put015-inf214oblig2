When I Saw You (film)
 
{{Infobox film
| name           = When I Saw You
| image          = When I Saw You (film).jpg
| caption        = Film poster
| director       = Annemarie Jacir
| producer       = Ossama Bawardi
| writer         = Annemarie Jacir
| starring       = Mahmoud Asfa
| music          = 
| cinematography = Helene Louvart
| editing        = 
| distributor    =  The Match Factory
| released       =  
| runtime        = 93 minutes
| country        = Palestine/Jordan
| language       = Arabic
| budget         = 
}} Best Foreign Language Oscar at the 85th Academy Awards, and won Best Asian Film at the 63rd Berlin International Film Festival, NETPAC award.     When I Saw You won Best Arab Film at the Abu Dhabi International Film Festival, Special Jury Prizes at the Oran Film festival of Arab Cinema and the Cairo International Film Festival, the Film Critics Don Quixote Award of the Carthage International Film Festival, and Audience Choice Award as well as the SIGNIS Award for Best Film at the Amiens International Film Festival. 

==Cast==
* Mahmoud Asfa as Tarek
* Ruba Blal as Ghaydaa
* Saleh Bakri as Layth
* Anas Algaralleh as Mr. Nasser
* Ali Elayan as Abu Akram
* Ruba Shamshoum as Zain
* Ahmad Srour as Touissant
* Firas W. Taybeh as Majed

==Accolades==
{| class="wikitable" rowspan=5; style="text-align: center; background:#ffffff;"
!Year!!Award!!Category!!Recipient!!Result!!Ref.
|- 2013
|Asia Pacific Screen Awards Asia Pacific Best Childrens Feature Film
|
| 
| 
|- 34th Young 2013
|Young Artist Award 34th Young Best Performance in an International Feature Film - Young Actor Mahmoud Asfa
| 
|   
|- 63rd Berlin 2013
|Best Asian Film 63rd Berlin Berlin International Film Festival - NETPAC Award
|
| 
|   
|}

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Palestinian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 