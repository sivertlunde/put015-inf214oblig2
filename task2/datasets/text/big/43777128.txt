The Uninvited Guest (2004 film)
 
 
 
The Uninvited Guest ( ) is a 2004 Spanish mystery thriller directed by Guillem Morales, best known for Julias Eyes. It stars Andoni Gracia, Mónica López (actress)|Mónica López and Francesc Garrido.

==Plot==


Felix (Gracia) is an architect who lives in a large house. He appears to be sensitive about visitors since being separated from his wife Vera (López). One night, a man asks to come in and use the phone. Felix allows him to do so, leaves the room for a few minutes, and then discovers the man is nowhere to be seen. For the next few days, Felix hears strange noises in the house, and suspects that the man never left. Felix calls the police who are unable to find anything. He then calls Vera and asks her to visit him, eventually resulting in them having sex. But he becomes paranoid when he thinks he hears her talking to someone in the kitchen and accidentally injures her with a knife.

Felixs suspicions are heightened when his neighbours dog enters the house and appears to hear noises from upstairs too. His neighbour, Mrs Mueller, runs after the dog and appears to be thrown down the stairs, killing her and her dog in the process. The police determine that she merely slipped. Eventually, Felix encounters someone in his house and shoots them, leaving them locked in a room. Felix locks down his house and leaves. He naps in his car until he is awakened by two children who are looking at a picture that Felix drew of the strange man. They identify him as "Martin" and points Felix towards Martins house.

Felix sneaks into Martins house and sees Martins wife; Claudia (also played by López) who is paralysed from the waist down. Felix stays hidden in her house for the next few days and discovers that Martin is also an architect who was away on a business trip and has been unkind to Claudia since her accident. Felix becomes infatuated with Claudia and manages to speak to her by hiding in plain sight at a surprise birthday party. 

Eventually, Felix alerts Claudia and Martins friend Bruno to his presence by breaking a vase and finding a key to the locked basement. Claudia reveals to Bruno that she actually locked Martin in the basement. Felix escapes the house through the basement (in the process taking an injury from Bruno and possibly injuring/killing Bruno and Claudia in turn), which leads to a tunnel where he finds Martins corpse. The tunnel leads to Felixs own basement. Felix climbs upstairs and discovers that the "man" he shot a few nights ago is actually Vera, who as she lays dying on the floor, reveals she is pregnant.

== External links ==
*  
*  

 
 
 
 


 