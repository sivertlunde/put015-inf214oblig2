Bey Yaar
 
{{Infobox film
| name           = Bey Yaar
| image          = Bey Yaar poster.jpg
| caption        = Official poster
| director       = Abhishek Jain
| producer       = Nayan Jain
| writer         =Bhavesh Mandalia Niren Bhatt
| starring       = {{Plainlist|
* Darshan Jariwala Manoj Joshi
* Amit Mistry
* Kavin Dave
* Aarti Patel
* Divyang Thakkar
* Pratik Gandhi
* Samvedna Suwalka
}}
| music          = Sachin–Jigar
| lyrics         = Niren Bhatt
| cinematography = Pushkar Singh
| editing        = Satchit Puranik Nirav Panchal
| studio         = CineMan productions
| distributor    = CineMan productions
| released       =  
| runtime        = 
| country        = India
| language       = Gujarati
| budget         =     
| gross          =   
}}
 Gujarati film Manoj Joshi, Darshan Jariwala, Divyang Thakkar, Pratik Gandhi, Amit Mistry, Samvedna Suwalka. The film was released on 29 August 2014  to positive reviews and box-office success as it completed silver jubilee (25 weeks) in theatres.    The film will be screened at New York Indian Film Festival, the first ever Gujarati film to do so.   

==Plot==
The film begins with Uday (Kavin Dave), an artist, narrating a story of his two friends, Tapan (Tino) and Chintan (Chako). 

Tapan (Pratik Gandhi) and Chintan (Divyang Thakkar) are close friends working as Pharmaceutical sales representative|MR. In the greed of making quick money, they are scammed by a fraud, posing as a Godman (India)|Godman, who promises to triple the money. Chintans father, J J Bhatt (Darshan Jariwala) runs a tea stall, in which hangs a painting of a prominent artist M. F. Hassan. The painting was gifted to his father by the painter as a sign of friendship between them. Tapan and Chintan ask Uday to copy the painting and decide to mortgage the original to an art dealer. After several nights, Uday successfully copies the painting, they place the duplicate in place of the original. They mortgage the original painting with the art dealer, Y. B. Gandhi (Manoj Joshi).  The next day Gandhi calls Chintan  informing that the painting is a fake. Chintan immediately meets him. Gandhi informs him that someone else had tried to sell the same painting to another dealer and that its a fake, implying that Tapan had double crossed him or that his father must have been lying and in actuality it must have been sold a long time back. 
Chintan angrily confronts both and out of anger his father throws him out of the house. Later Chintan reconciles with other friends and understands that Gandhi has cheated them. Gandhi publicly humiliates Chintans father and announces in media that the painting and the story of the friendship behind it are fake. The friends then hatch an elaborate plan to get back the painting and pride of Chintans father.

==Cast==
* Darshan Jariwala as Jeetu J Bhatt Manoj Joshi as Y B Gandhi
* Amit Mistry as Prabodh Gupta and Pranav Doshi
* Kavin Dave as Uday Faujdar
* Divyang Thakkar as Chintan
* Pratik Gandhi as Tapan
* Samvedna Suwalka as Jigisha
* Abhishek Jain as Dev

==Production==

===Development=== national award Manoj Joshi and Kavin Dave will be making their debuts in Gujarati movies.  

===Filming===
The film shot in Ahmedabad. The film was shot at 50 different locations in 35 days.   Talking about filming locations, Abhishek Jain said, "We have shot in the most rustic locales of Ahmedabad to bring out the real essence of the city in every scene."  In March 2014, it was announced that the film is almost ready.    Several notable Gujaratis appeared as the guests in the film including columnist Jay Vasavada, author Tushar Shukla, actor Archan Trivedi, art promoter Sonal Ambani and critic S D Desai.   

==Soundtrack==
{{Infobox album
| Name        = Bey Yaar
| Type        = Soundtrack
| Artist      = Sachin-Jigar
| Cover       = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 16:45
| Label       = Cineman Productions Limited
| Producer    = 
| Last album  = Entertainment (film)|Entertainment (2014)
| This album  = Bey Yaar (2014)
| Next album  = Finding Fanny (2014)
}}

Music for the film is composed by Sachin–Jigar.  The director Abhishek Jain said, about the music of the film, "With Bey Yaars music we are trying to create a friendship anthem"  Music review site Milliblog reviewed the soundtrack as "short but competent affair". 

{{tracklist
| total_length = 16:45
| headline        = Tracklist
| all_music       = 
| music_credits   = 
| lyrics_credits = yes
| extra_column    = Artist(s)
| title1          = Bey Yaar Sapna Nava
| lyrics1        = Niren Bhatt
| extra1        =  
| length1      = 3:53
| title2          = Bey Yaar Tara Vina
| lyrics2        = Niren Bhatt
| extra2        = Sachin Singhavi
| length2      = 4:55
| title3          = Rakhad Rakhad
| lyrics3        = Niren Bhatt
| extra3        = Keerthi Sagathia
| length3      = 4:03
| title4          = Peechha Raja
| lyrics4       = Niren Bhatt
| extra4        = Divya Kumar
| length4      = 3:54
}}

==Release==

In July 2014, the producers released the teaser  and poster  and was well-received. The trailer was released online on 1 August 2014. The theatrical trailer was launched on 4 August 2014 at Alpha One Mall in Ahmedabad.  The film was released on 29 August 2014.
 Gujarati film to do so.  

==Reception==

===Critical Reception===
Critical reception to the film has been mostly positive. Divya Bhaskar praised the film and rated it 3 out of 5 stars.  BuddyBits.com rated it 4.5 out of 5 stars praising it to be one of the best films coming out from Gujarati cinema.  Sandesh (Indian newspaper)|Sandesh praised it and called it a mature movie.  Times of India reviewed it positively saying, "Wonderfully executed, beautifully crafted and crisply edited, the movie makes the popcorn and the hot cuppa more enjoyable." and rated it 4 stars out of 5.  Jay Vasavada called it, "Brilliant, Brave, Bright".  The film will be screened at New York Indian Film Festival and will be the first ever Gujarati film to do so. 

===Boxoffice===
The film had a limited release across cinemas in Gujarat and Maharashtra, but was running with packed houses on the day of the release  and continued the success in the first weekend.  The film completed 100 days in many cities.  and eventually completed 25 weeks.  The film collected   on the box office. 

==Awards==

=== 14th Annual Transmedia Gujarati Screen & Stage awards ===

The film was nominated for 14 out of 16 categories and won total 9 awards, including the best film.  

* Best film – Bey Yaar
* Best director – Abhishek Jain
* Best scriptwriter (Story, Screenplay & Dialogue) – Bhavesh Mandalia, Niren H Bhatt
* Best actor (male) – Divyang Thakkar
* Best actor (female) – Samwedna Suwalka
* Best supporting actor (male) – Darshan Jariwala
* Best supporting actor (male) – Kavin Dave
* Best supporting actor (female) – Arati Patel Manoj Joshi
* Best cinematographer – Pushkar Singh
* Best Editor – Satchit Puranik, Nirav Panchal
* Best music – Sachin-Jigar
* Best singer (male) – Darshan Raval
* Best lyrics – Niren H Bhatt

==References==
 

==External links==
*  
*  
*   on Facebook

 
 
 
 
 
 
 
 