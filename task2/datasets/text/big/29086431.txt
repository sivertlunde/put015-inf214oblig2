I Melt with You (film)
{{Infobox film
| name           = I Melt with You
| image          = I_melt_with_you.jpg
| caption        = Theatrical poster
| director       = Mark Pellington
| producer       = Rob Cowan Norman Reiss Mark Pellington Thomas Jane Neil LaBute OShea Read


| screenplay     = Glenn Porter
| starring       = Thomas Jane Jeremy Piven Rob Lowe Christian McKay
| music          = tomandandy
| cinematography = Eric Schmidt
| editing        = Don Broida
| studio         = Media House Capital Raw Entertainment
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
I Melt with You is a 2011 American arthouse thriller film directed by Mark Pellington. It completed filming in September 2010. It premiered at the Sundance Film Festival in January 2011.  The film was critically panned. 

==Plot==
Former college friends—Ron (  for embezzlement. Jonathan runs a successful medical practice, but all of his patients are wealthy drug addicts, he and his wife are divorced, and their young son identifies more with his mothers new husband than with Jonathan. Richard is a published author, but he has only written one book and now teaches high school English. Tim, an open bisexual, was until 5 years ago living in a happy relationship with a man, until accidentally causing the fatal car crash that killed his boyfriend and his sister, Jill.

The four friends party for several days at a beach side mansion, during which the men consume massive quantities of drugs provided to them by Jonathan, including oxycodone, ativan, dilaudid, adderall, ketamine, medicinal marijuanna, hydrocodone cocaine and morphine. After reminiscent conversation they head into town for food and to pick up women. Richard convinces a young waitress to bring her friends back to the house. Tim engages in a three-way with two of the revelers, during which they role play the parts of Tims dead boyfriend and sister. Early in the morning, Tim hangs himself in the shower. Richard, Ron, and Jonathan find him, along with a note he left behind. The note contains the text of a suicide pact the men made in 1986, promising that they would kill themselves together if they found life unfulfilling in middle age. Afraid that the police will find the note and blame them for Tims death, they bury him on the beach behind the house. Ron disagrees with continuing the fulfillment of the suicide pact, which results in Richard and Jonathan mocking him as a coward and a liar. Ron goes to an airport in an attempt to return home, but cannot bring himself to board the plane. He returns to the mansion and the three friends reunite.

Going into town for lunch, the men eat at a restaurant where an elderly man goes into cardiac arrest. Jonathan saves his life, drawing the attention of police Officer Boyde (Carla Gugino). Coming to the house to thank Jonathan, Boyde encounters an inebriated and agitated Ron. Believing that Boyde knows something is wrong, Ron attempts to get her to leave, raising Boydes suspicions. 

Later, Richard finds Ron in his room, unwilling to return home to the waiting federal agents. He tells Richard that he is frustrated with where his life has ended up, cannot face his wife and family, and no longer wants to live. Richard agrees to help him end his suffering and smothers him to death with a pillow. The next morning, Richard and Jonathan bury Ron beside Tim. 

Richard and Jonathan go into town to party more, where Richard provokes two young men into beating him up. Jonathan, having decided to end his life, calls his son, asking the boy to promise him to remember who his real father was. After Jonathans ex-wife interrupts, he uses a stethoscope to make a tourniquet and gives himself a fatal overdose of intravenous sedative, most likely a barbiturate. 

The next morning Boyde arrives at the house to check in on the men, the bar fight and Rons suspicious behavior leading her to believe that something criminal is occurring. Entering the mansion, she finds that Richard has covered the inside of the house in artistic recreations of the text of the suicide pact. Richard tells that his friends are dead and then flees in a sports car, leading Boyde on a high speed chase to the Point Sur Lighthouse. When Boyde arrives, she finds Richards car with the suicide pact on the front seat and Richard near the edge of the cliffs above the ocean. Boyde attempts to talk Richard away from the edge, but Richard states he would "miss his friends too much" and jumps while Boyde looks on in shock. The film ends on a close up of the suicide pact and the four friends each narrating a line.

==Cast==
*Jeremy Piven as Ron
*Rob Lowe as Jonathan 
*Thomas Jane as Richard
*Carla Gugino as Laura Boyde
*Arielle Kebbel as Randi
*Sasha Grey as Raven
*Christian McKay as Tim Tom Bower as Captain Bob
*Joe Reegan as Cole
*August Emerson as Ethan
*Victoria Bruno as Carrie
*David Lowe as Officer Stevens

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 