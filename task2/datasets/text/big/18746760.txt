Morning Light
{{Infobox film
| name = Morning Light
| image = Morning-light-theatrical-poster.jpg
| image_size = 
| caption = Theatrical poster
| director = Mark Monroe
| producer = Roy E. Disney Leslie DeMeuse Morgan Sackett Paul Crowder
| studio = Walt Disney Pictures Walt Disney Studios Motion Pictures
| released =  
| runtime =  100 minutes
| country = United States
| language = English
| gross = $275,776 
}} 2008 film directed by Mark Monroe and executive produced by Roy E. Disney. The film was released on October 17, 2008 {{cite web
  | title = The Morning Light Project
  | publisher = Pacific High Productions
  | url = http://www.pacifichighproductions.com/ TP52 class sailing yacht, Morning Light.

== Plot ==
Morning Light  is a documentary that follows the youngest crew (by average age)  to compete in the Transpac. All crew members were between 18 and 23 at the time. The film follows the formation of the Morning Light sailing team, their six months of training in advance of the yacht race, and finally the weeklong Los Angeles to Honolulu race itself.

The crew numbered 15 young sailors of varied experience: Chris Branning, Graham Brant-Zawadzki, Chris Clark, Charlie Enright, Jesse Fielding, Robbie Kane, Steve Manson, Chris Schubert, Kate Theisen, Mark Towill (at 18, the youngest crew member), Genny Tulloch, navigator Piet van Os, Chris Welch, Kit Will and the 21-year-old skipper, Jeremy Wilmot. {{cite news 
  | last = Roberts
  | first = Rich
  | title = Jeremy Wilmot chosen as Morning Light skipper
  | publisher = Sail World
  | date = April 26, 2007
  | url = http://www.sail-world.com/pda.cfm?Nid=33026&RequestTimeOut=180
  | accessdate = 2008-08-06}} 

== Production ==
Executive producer Roy Disney was a sailing enthusiast and Transpac competitor who held several sailing speed records including the Los Angeles to Honolulu monohull time record, which he set on his boat Pyewacket in July 1999. {{cite news 
  | last = McCormick
  | first = Herb
  | title = THE BOATING REPORT; Transpac Is Amusing to Disney 
  | publisher = The New York Times
  | date = 2001-07-01
  | url = http://query.nytimes.com/gst/fullpage.html?res=9D03EFDA1239F932A35754C0A9679C8B63
  | accessdate = 2008-08-06}}  The concept for the film came from TP52 Class Association executive director Tom Pollack, who passed it on to former ESPN producer Leslie DeMeuse, who has worked with Disney on other sailing-related film projects.

In early 2006, Roy E. Disney, longtime sailing master Robbie Haines and DeMeuse considered 538 applications and picked 30 finalists, from which 15 were chosen in a week of selection trials in Long Beach, California. {{cite news 
  | last = Streuli
  | first = Stuart
  | title = Roy Disney Sounds Off About Morning Light
  | publisher = Sailing World
  | date = 2008-08-04
  | url = http://www.sailingworld.com/racing/racing-news/roy-disney-sounds-off-about-morning-light-43542.html
  | accessdate = 2008-08-06}}  Training began in Honolulu, Hawaii in January 2007, for two weeks at a time through late June, with time off in May.  The 
team trained aboard the Morning Light, which Disney purchased from software executive Philippe Kahn. Filming coincided with training and the race itself, which started July 15, 2007 and concluded ten days later.

== Release ==
The film was released on October 17, 2008. Prior to theatrical release, private screenings were held for yacht racing enthusiasts, including one hosted by Roy  E. Disney for U.S. Sailing on March 14, 2008 in Newport, Rhode Island. {{cite web
  | title = JOIN ROY E. DISNEY AND FELLOW US SAILING MEMBERS FOR A SPECIAL PREVIEW OF THE MORNING LIGHT MOVIE
  | publisher = U.S. Sailing
  | url = http://www.ussailing.org/donations/Morninglight.asp
  | accessdate = 2008-08-06}} 

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 