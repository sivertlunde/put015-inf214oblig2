Aakhri Khat
{{Infobox film
| name           = Aakhri Khat
| image          = AakhriKhat.jpg
| caption        =  Chetan Anand
| producer       = Himalaya Films Chetan Anand
| narrator       = 
| starring       = Rajesh Khanna Indrani Mukherjee Khayyam Kaifi Azmi (lyrics)
| cinematography = Jal Mistry
| editing        = Jadav Rao
| distributor    = Prabha Pictures 
| released       = 1966
| runtime        = 153 minutes
| country        = India
| language       = Hindi
| budget         = 
}}
 Chetan Anand. Khayyam and lyrics by Kaifi Azmi; it includes the song "Baharon Mera Jeevan Bhi Sanwaro", sung by Lata Mangeshkar. The film was given 5 stars in Bollywood Guide Collections.
 Best Foreign Language Film at the 40th Academy Awards in 1967, but was not accepted as a nominee.  

== Plot ==
Govind (Rajesh Khanna) is a young sculptor, while vacationing near Kullu, sees Lajjo (Indrani Mukherjee) and falls in love. Subsequently, they get married secretly in a village temple. He then has to leave for the city to pursue further education. Meanwhile, the girl finds that she is pregnant. On finding this her step mother sells her off for Rs. 500, where she is beaten, some time later she gives birth to a little boy, Buntu. Later Lajjo comes to Mumbai to meet Govind, carrying her one-year-old son. She leaves a letter for him at his doorsteps, and want to leaves the child as well, is unable to go through it, and takes him along. They keep wandering, and feed off whatever comes their way, soon she dies leaving her son alone.

The rest of the film is a story of the little child, wandering around the city.  He goes out of the house, eating whatever he finds, including a pill, which makes him doze off.  On waking up, he wanders even more and more into the city. Meanwhile Govind, comes know all through a letter she has left behind, Aakhri Khat (Last Letter), he soon realizes his mistake and with the help of police tries to find his wife and son, though only finds his wifes body. Later, he shows the Police inspector Naik, (Manvendra Chitnis), the statue of Lajjo he has kept in his studio.

The child is then rescued by a man who is a staff member of an orphanage nearby. He escapes from that place at night.  And after a long time of wandering here and there, and after the help of some people, he ultimately reaches home to find a statue of his lost mother, and a new lady, who is now his new mother. 

==Cast==
* Rajesh Khanna - Govind
* Indrani Mukherjee - Lajjo	
* Master Bunty	- Buntu
* Nana Palsikar		
* Manvendra Chitnis - Inspector Naik
* Mohan Choti - Moti (Servant)
* Tun Tun		
* Maruti Rao		
* Naqi Jehan - Wealthy young woman

==Production==
The film Rajesh Khannas debut film as an actor, and he was shy while face the camera, after facing difficult in first three shots, director Ravindra Dave helped him understand his scenes and movements, and corrected his  way of walking.  

==Quotes on the film==
Rajesh Khanna disclosed in an interview,"I consider “Aakhri Khat” a memorable film of my initial days. It was out and out a directors project and Chetan Anand, highly imaginative and sensitive director handled the film with expertise. I still clearly remember how cinematographer Jal Mistry shot the song sequence, “Ab Na Ja” on me and Indrani Mukherjee in five to six close ups also picturising the natural panorama of the Himalayas with rare aesthetics and perfection. My most challenging scene in “Aakhri Khat” was the last one where I am in a pensive mood in silence till I recognise my son, Bunty. Chetan Anand used to wake me up with late night phone calls so that my face had the ideal pathos oriented look."   

== Soundtrack == Bhupinder Singh as a solo playback singer. 
{{Infobox album  
| Name        = Aakhri Khat
| Type        = Soundtrack Khayyam
| Cover       = 
| Released    = 1966 (India)
| Recorded    =  
| Genre       = Film soundtrack|
| Length      = 
| Label       = EMI  Khayyam
| Reviews     = 
| Last album  = Mohabbat Isko Kahete Hain   (1965) 
| This album  = Aakhri Khat  (1966)
| Next album  = Shagun    (1964)      
|}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer (s)
|-
| Aur Kuchh Der Thahar 
| Mohd. Rafi 
|-
| Baharon Mera Jeevan Bhi Sanwaro Lata Mangeshkar
|-
| Mere Chanda Mere Nanhe  Lata Mangeshkar
|-
| O My Darling 
| Manna Dey
|-
| Rut Jawan Jawan Raat Mehrbaan Bhupinder Singh
|}

==See also==
* List of submissions to the 40th Academy Awards for Best Foreign Language Film
* List of Indian submissions for the Academy Award for Best Foreign Language Film

==References==
 

== External links ==
*  

 

 
 
 
 
 
 