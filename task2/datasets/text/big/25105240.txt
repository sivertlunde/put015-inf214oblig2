The Proud and the Damned
 
{{Infobox film
| name           = The Proud and the Damned
| image          = DVD cover of The Proud and the Damned.jpg
| image_size     =
| caption        =
| director       = Ferde Grofé Jr. George Montgomery (co-producer) Stanford Tischler (associate producer)
| writer         = Ferde Grofé Jr.
| narrator       =
| starring       =
| music          = Gene Kauer Douglas M. Lackey
| cinematography = Remegio Young
| editing        = Philip Innes
| distributor    =
| released       =
| runtime        =
| country        = United States Colombia
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

The Proud and the Damned is a 1972 American-Colombian co-production western film directed by Ferde Grofé Jr.. The film is also known as Proud, Damned and Dead.

== Plot == Peter Ford) have just emigrated from their home state of Texas to Colombia in South America. After their arrival, they are ambushed by Colombian soldiers who force them to come meet General Martinez, the evil, cold-hearted dictator of their country. Martinez sends them to San Carlos, a town where rebel forces are preparing to start a civil war against Martinezs army.  The Texans are instructed to live among the rebels, and report back to Martinez what they know within the next couple of days.  Martinez warns them that theyll be severely punished if they fail him.
 gypsy family who are on their way to the same town. Will escorts them after he and the gypsies daughter, Mila, fall in love at first sight. They all ride into San Carlos, meet the governor, and rent a cabin outside of town. Will and Mila sneak out that night and make love.  Milas father is angry when he hears of this, and slices her cheek. When Will finds out about this, he shoots Milas father.

Being from famously rebellious Texas, Wills gang finds itself in sympathy with the San Carlos rebels. Will and his gang disobey Martinezs orders and dont report back to him, so Martinez kidnaps Will and hangs him. Wills friends find his body, give him a funeral, and vow to avenge his murder. They join the rebels in a battle with Martinezs army and drive them back. They later ambush Martinez and the rest of his surviving soldiers in a canyon, joined by the rebel armys captain. They manage to kill Martinez, but all are gunned down by his soldiers except for Billy, who was unconscious after falling from his horse. The film ends with Billy riding off into the sunset.

== Cast ==
*Chuck Connors as Will Hansen
*Aron Kincaid as Ike
*Cesar Romero as San Carlos Mayor
*José Greco as Ramon (the gypsy)
*Smokey Roberds as Jeb
*Henry Capps as Hank Peter Ford as Billy
*Andres Marquis as Gen. Alehandro Martinez
*Conrad Parham as Capt. Juan Hernandez (the mayors nephew)
*Maria Grimm as Maria Vargas
*Nana Lorca as Carmela (the dancer)
*Anita Quinn as Mila (Hansens gypsy girlfriend)
*Álvaro Ruiz as Chico Pacheco as Lieutenant Ignacio Gómez as Padre
*Ernesto Uribe as Aide
*Rey Vásquez as Innkeeper
*Bernardo Herrera as Rollo

== External links ==
* 
* 

 
 
 
 
 
 
 
 