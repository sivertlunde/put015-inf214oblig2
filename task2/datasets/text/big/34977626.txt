Mandhira Punnagai (1986 film)
{{Infobox film|
| name = Mandhira Punnagai
| banner = Sathya Movies
| image = Manthirappunagai86.jpg
| caption = 
| director = V Tamizhalagan
| dialog =  A L Narayanan Baby Sujitha
| producer = G Thyagarajan
| music = Ilaiyaraaja
| photography = Ramachandra Babu
| editor = K R Krishnan
| screenplay = P L Sunderrajan
| released = 11 December 1986  
| runtime = Tamil
| budget =
}}

Mandhira Punnagai ( ),( ) is a 1986 Tamil film produced by Sathya Movies and directed by V Thamizhalagan. The film casts Sathyaraj in dual role- one as a Doctor and the other as a Police Inspector. 
The film with moderate reviews from the critics was moderately successful at the box office. 
. 

==Plot==
The film has an intuitive story line with Sathyaraj enacting dual role for the first time in his career. The Doctor Sathyaraj appeared with moustache while the Police Office Sathyaraj appeared without moustache.

==Cast==
* Sathyaraj Baby Sujitha
* Nadia Moidu Suresh
* Raghuvaran Senthil

==Reception==
The film received mostly mixed reviews from the critics but due to its promotion and intuitive story line managed to become successful at the box office. Behindwoods reported that this film gave Sathyaraj a strong foothold as a solo hero in Tamil industry. Earlier he was doing supportive and villain roles but with this film, he established himself as a solo hero at the same time continued to shine in villain roles too. Raghuvarans powerful performance as the villain Daniel Miranda helped the film a lot.

==References==
 

==External links==
*  

 
 
 
 
 


 