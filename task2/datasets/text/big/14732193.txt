Triumph (1924 film)
 
{{Infobox film
| name           = Triumph
| image          = Triumph -1924.jpg
| caption        = 1924 theatrical poster
| director       = Cecil B. DeMille
| producer       = Cecil B. DeMille
| writer         = May Edginton Jeanie MacPherson
| starring       = Leatrice Joy
| music          = James C. Bradford
| cinematography = Bert Glennon
| editing        = Anne Bauchens
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 silent drama film directed by Cecil B. DeMille.   

==Cast==
* Leatrice Joy - Ann Land
* Rod La Rocque - King Garnet
* Victor Varconi - William Silver Charles Ogle - James Martin
* Theodore Kosloff - Varinoff
* Robert Edeson - Smauel Overton
* Julia Faye - Countess Rika
* George Fawcett - David Garnet
* Spottiswoode Aitken - Torrini
* ZaSu Pitts - A Factory Girl
* Raymond Hatton - A Tramp
* Alma Bennett - A Flower Girl
* Jimmie Adams - A Painter William Boyd - (uncredited)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 


 