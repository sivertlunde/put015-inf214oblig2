Eine Symphonie des Kampfwillens
{{Infobox film
| name           = Eine Symphonie des Kampfwillens
| image          =
| image_size     =
| caption        =
| director       = Julius Lippert
| producer       =
| writer         =
| starring       =
| music          =
| cinematography = Robert Eibig
| editing        =
| distributor    =
| released       = 1927
| runtime        = ca. 20 minutes
| country        = Weimar Republic German intertitles
| budget         =
| gross          =
}}

Eine Symphonie des Kampfwillens ( ) is the first film documentary of a Nuremberg Rally.

Made soon after the establishment of the Nazi Party film office, the film is a short record of the highlights of the conference, interspersed with newspaper descriptions of the rally. It begins with the arrival at Nuremberg of the various contingents of Nazism|Nazis, some on train, others in trucks or on foot. The main gatherings are held in parks, rather than the stadiums that would be used later.

An important moment in the film is the sequence when Adolf Hitler greets the delegations from areas of Germany that have been occupied or cut off from the Reich, such as the Ruhr, or Austria, and vows that the foreigners will be expelled and German people reunited. The film ends, like others in this series, with a parade of the Sturmabteilung before the Führer.

==External links==
* 

 
 
 
 
 
 


 
 