Dharma Kshetram
{{Infobox film
| name           = Dharmakshetram
| image          =
| caption        =
| writer         = Paruchuri Brothers  
| screenplay     = A. Kodandarami Reddy
| producer       = K. C. Reddy
| director       = A. Kodandarami Reddy
| starring       = Nandamuri Balakrishna Divya Bharti
| music          = Ilaiyaraaja
| cinematography = A. Vincent A. Venkat
| editing        = D. Venkataratnam
| studio         = Sri Rajeev Productions
| released       =  
| runtime        = 147 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}

Dharmakshetram  ( , Action film produced by on Sri Rajeev Productions banner, directed by A. Kodandarami Reddy. Starring Nandamuri Balakrishna, Divya Bharti in the lead roles and music composed by Ilaiyaraaja.    The film recorded as flop at box office.      

==Cast==
 
*Nandamuri Balakrishna
*Divya Bharti Jaggayya 
*Nassar 
*Rami Reddy  Devan  Srihari 
*Brahmaji 
*Sakshi Ranga Rao  
*Prasanna Kumar 
*Posani Krishna Murali 
*Jayalalita 
*Jyothi 
*Sudha Rani 
*Radha Bai
 

==Soundtrack==
{{Infobox album
| Name        = Dharmakshetram
| Tagline     = 
| Type        = film
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1992 
| Recorded    = 
| Genre       = Soundtrack
| Length      = 30:25
| Label       = LEO Audio
| Producer    = Ilaiyaraaja
| Reviews     =
| Last album  = Aditya 369   (1991)  
| This album  = Dharmakshetram   (1992)
| Next album  = Aswamedham   (1992)
}}

Music composed by Ilaiyaraaja. All songs are hit tracks. Music released on LEO Audio Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 30:25
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Enno Ratrulu  Sirivennela Sitarama Sastry SP Balu, Chitra
| length1 = 5:01

| title2  = Cheli Nadume Andam
| lyrics2 = Veturi Sundararama Murthy
| extra2  = SP Balu,Chitra
| length2 = 5:07

| title3  = Mudduto Srungara Beetu
| lyrics3 = Veturi Sundararama Murthy
| extra3  = SP Balu, S. Janaki
| length3 = 4:41

| title4  = Are Inka Janka
| lyrics4 = Veturi Sundararama Murthy
| extra4  = SP Balu,S. Janaki 
| length4 = 5:42

| title5  = Pelliki Munde Okka Sari
| lyrics5 = Veturi Sundararama Murthy
| extra5  = SP Balu,S. Janaki
| length5 = 4:59

| title6  = Kora Meenu Komalam  
| lyrics6 = Sirivennela Sitarama Sastry  
| extra6  = Mano (singer)|Mano,Chitra
| length6 = 5:05
}}
   

==Others==
* VCDs and DVDs on - Moser Baer Home Videos, Hyderabad

==References==
 

 
 
 
 
 

 