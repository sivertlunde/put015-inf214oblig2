Cheech & Chong's Animated Movie
{{Infobox film
| name           = Cheech & Chongs Animated Movie
| image          = File:The_Image_is_the_Cover_Art_for_"Cheech_and_Chongs_Animated_Movie.jpg
| alt            = 
| caption        = 
| director       = Branden Chambers Eric D. Chambers
| producer       = Branden Chambers  Eric Chambers  Lou Adler
| writer         = Tommy Chong Cheech Marin 
| starring       = Cheech Marin  Tommy Chong
| music          = Dominic Kelly 
| cinematography = 
| editing        = Eric D. Chambers Kevin C. Murray 
| studio         = Big Vision Entertainment Chamber Bros. Entertainment
| distributor    = 20th Century Fox
| released       = April 18, 2013
| runtime        = 120 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 The Corsican Brothers, and the first to feature them as animated characters. The film features several of their original comedy bits  such as "Sister Mary Elephant", "Sgt. Stedanko", "Ralph and Herbie", "Lets Make a Dope Deal", "Earache My Eye", and the classic "Daves not here, man". It was screened on April 18, 2013 and was released on DVD/Blu-ray on April 23, 2013. This currently the only Cheech & Chong movie available on Blu-ray.

The trailer claims it to be an adventure, but scenes shown throughout are mostly animated interpretations of their segments from their comedy albums. It also features a wraparound story of a genital crab named Buster the Body Crab trying to get high from Chongs weed scented head.

==Plot== Ice Age; a marajuana obsessed pubic lice named Buster the Body Crab (from their comedy skit "Les Morpions") tries to go after Tommy Chongs weed-scented head while following and putting up through the duos adventures from there famous comedy skits.

==Cast==
*Cheech Marin as Cheech
*Tommy Chong as Chong

==Famous skits used==
*Les Morpios
*Cruisin with Pedro De Pacas (simply titled Cruisin St.)
*Pedro and Man at the Drive In (simply titled Drive In)
*Ralph & Herbie
*Sign Ze Papers Old Man (simply titled Tortured Old Man)
*Big Bambu (simply titled Empire Hancock)
*Lets Make a Dope Deal
*Un-American Bandstand
*Up His Nose
*Acapulo Gold
*The Bust!
*Afghanistan
*Trippin in Court
*Daves not here, man! (simply titled Dave)
*Wake Up America
*Pedros Request
*Sister Mary Elephant
*Sgt. Stedanko
*Earache My Eye

18 skits of the duos albums have been animated in this film.

==Soundtrack==
The soundtrack for the film was released on April 9, 2013. 

==Releases==
The movie debuted in theaters April 18, 2013. The DVD and Blu-ray release followed shortly after on April 23, 2013. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 


 