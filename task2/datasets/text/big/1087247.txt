The Mark of Zorro (1940 film)
{{Infobox film
| name           = The Mark of Zorro
| image          = Mark_of_Zorro_1940.jpg
| caption        = 1940 US Theatrical Poster
| director       = Rouben Mamoulian
| producer       = Darryl F. Zanuck
| screenplay     = John Taintor Foote
| story          = Garrett Fort Bess Meredyth
| based on       =  
| starring       = Tyrone Power Linda Darnell Basil Rathbone  Alfred Newman
| cinematography = Arthur C. Miller
| editing        = Robert Bischoff
| distributor    = 20th Century Fox
| released       =  
| runtime        = 94 minutes
| language       = English Spanish
| country        = United States
| budget         = $1 million Solomon, Aubrey (1989). Twentieth Century Fox: A Corporate and Financial History. Lanham, Maryland: Scarecrow Press, p. 240, ISBN 978-0-8108-4244-1. 
| gross         = $2 million   
}}
  love interest, Robert Lowery, and Chris-Pin Martin. Diegos mute servant, Bernardo is absent in this film adaptation.  
 Best original 1941 Academy Awards.  In 2009, it was named to the National Film Registry by the Library of Congress for being "culturally, historically or aesthetically" significant and will be preserved for all time. 

==Plot==
The film is based on the story The Curse of Capistrano written by Johnston McCulley, originally published in 1919, which introduced the masked hero Zorro.  The story is set in Southern California during the early 19th century. The plot deals with Don Diego Vega (Tyrone Power), the apparently foppish son of wealthy ranchero Don Alejandro Vega (Montagu Love), who returns to California after his education in Spain.  He is horrified at the way the common people are mistreated by Alcalde Luis Quintero (J. Edward Bromberg). Don Diego adopts the guise of El Zorro ("the fox"), a masked outlaw who becomes the defender of the common people. Meanwhile, he romances the Alcaldes beautiful niece, Lolita (Linda Darnell).  He simultaneously flirts with the Alcaldes wife Inez (Gale Sondergaard), filling her head with tales of Madrid fashion and culture and raising her desire to move there with her corrupt husband. In both his guises, Don Diego has to contend with the governors ablest henchman, the malevolent Captain Esteban Pasquale (Basil Rathbone).

==Original version== lavish 1920 Douglas Fairbanks, Sr. and Noah Beery, Sr..  The 1920 film introduced Zorros iconic all-black costume, subsequently incorporated into Johnston McCulleys later Zorro stories. The 1920 film was the first in a popular array of swashbuckler action pictures starring the acrobatic Fairbanks, who had previously appeared mainly in comedies.

==Cast==
* Tyrone Power as Don Diego Vega/Zorro
* Linda Darnell as Lolita Quintero
* Basil Rathbone as Captain Esteban Pasquale
* Gale Sondergaard as Inez Quintero
* Eugene Pallette as Fray Felipe
* J. Edward Bromberg as Don Luis Quintero
* Montagu Love as Don Alejandro Vega
* Janet Beecher as Senora Isabella Vega
* George Regas as Sergeant Gonzales
* Chris-Pin Martin as Turnkey Robert Lowery as Rodrigo
* Belle Mitchell as Maria
* John Bleifer as Pedro
* Frank Puglia as Proprietor
* Eugene Borden as Officer of the Day Pedro de Cordoba as Don Miguel
* Guy DEnnery as Don Jose

==Batman connection==
In the   claims it was the Tyrone Power version, whereas a story by  , 
 Gray Ghost—a pulp fiction hero inspired by The Shadow—is the inspiration to young Bruce Wayne.

==Home media==
The film has been released twice on DVD. The first was released on October 7, 2003 and featured the movie in its original   on the A&E Network, and a commentary by film critic Richard Schickel.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 