Fifa e arena
 
{{Infobox film
| name           = Fifa e arena
| image          = 
| caption        = 
| director       = Mario Mattoli
| producer       = Metropa Film Steno Marcello Marchesi
| starring       = Totò Isa Barzizza
| music          = Pippo Barzizza
| cinematography = Vincenzo Seratrice
| editing        = Giuliana Attenni
| distributor    = Titanus
| released       = 1948
| runtime        = 80 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Fifa e arena is a 1948 Italian comedy film directed by Mario Mattoli and starring Totò.   

==Plot==
Nicolino is a kitchen boy who works in a small pharmacy in the country, run by a woman unbearably rude. So Nicolino really wants to leave his job, when he discovers that you are looking for a murderess with the same face to his. Nicolino so disguises himself as a woman and flees with the first plane is: leave for Sevilla. In Spain Nicolino is always found involved in misunderstandings and terrible mess because it is always considered a murderess until he runs into some people who mistake him for a famous bullfighter ready for his next battle against the bull to be held in bullfight in a few rounds. Nicolino in spite of being trained and prepared for the race and also falls in love with the beautiful Patricia, who encourages him to fight. Nicolino is wittily nicknamed "Nicolete" and is faced with the bull but it breaks down. In the hospital Nicolete prove their identity and will marry Patricia.

==Cast==
* Totò - Nicolino Capece
* Isa Barzizza - Patricia Cotten
* Mario Castellani - Cast
* Franca Marzi - Carmen
* Giulio Marchetti - Paquito
* Cesare Polacco - Banderillero
* Vinicio Sofia - Paquitos manager
* Ada Dondini - Mrs. Adele
* Luigi Pavese - Doctor
* Galeazzo Benti - George
* Raimondo Vianello - Maître
* Alda Mangini - Lady on train
* Ughetto Bertucci - Chaffeur
* Enzo Turco - Shoeshiners customer

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 