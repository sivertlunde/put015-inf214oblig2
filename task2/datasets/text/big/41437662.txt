NH 47 (film)
{{Infobox film 
| name           = NH 47
| image          =
| caption        = Baby
| producer       = Sajan
| writer         = Saj Movies Pappanamkodu Lakshmanan (dialogues)
| screenplay     = Pappanamkodu Lakshmanan Jose Shubha Shubha Prathapachandran Shyam
| cinematography = KB Dayalan
| editing        = K Sankunni
| studio         = Saj Productions
| distributor    = Saj Productions
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, Baby and Shubha and Prathapachandran in lead roles. The film had musical score by Shyam (composer)|Shyam.    Sukumaran as the victim Chacko of the notorious Chacko Murder Case that shook Kerala and T. G. Ravi played the character of Sukumara Kurup who plotted the murder.

Eventhough in real life, Sukumara Kurup still remain untraced, the film showed he is getting caught by the Police. 

==Cast==
 
*Jagathy Sreekumar as Pushpangathan Jose as Alikunju Shubha as Ramani 
*Prathapachandran as Advocate
*Sukumaran as Rahim Baby Shalini as Mini
*Balan K Nair as Thankappan
*C. I. Paul as Bhargavan Pilla
*Harippad Soman
*Jalaja as Nazeera
*Kunjandi as Sunnys father
*Lalu Alex as SI Johnson
*Mala Aravindan as PC Velu Pilla
*Nanditha Bose as Sumathi
*Nellikode Bhaskaran as Khaderikka
*Philomina as Sudhakarans mother
*Sathyakala as Elsy
*Sreenath as Sunny
*TG Ravi as Sudhakaran Pilla
*Vallathol Unnikrishnan as PC Purushu
*Radhadevi as Gauriyamma/Thankappans mother
 

==Soundtrack== Shyam and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Allah Allah Allah || K. J. Yesudas, Poovachal Khader, CA Aboobacker || Poovachal Khader || 
|-
| 2 || Kannukondu kessezhuthum || K. J. Yesudas, Chorus || Poovachal Khader || 
|-
| 3 || Punnarapoomanimaran || S Janaki || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 