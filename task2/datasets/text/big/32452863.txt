Emmanuelle in Soho
  David Hughes and starring Angie Quick, Julie Lee and John M. East. 

==Cast==
* Angie Quick - Emmanuelle of Soho Julie Lee - Kate Benson
* John M. East - Bill Anderson
* Kevin Fraser - Paul Benson
* Gavin Clare - Adie Timothy Blackstone - Derek
* Geraldine Hooper - Jill
* Anita Desmarais - Sheila
* Georges Waser - Tom Poluski
* Erika Lea - Judy
* Kathy Green - Sammy
* Suzanne Richens - Suzie
* John Roach - Albert
* Vicki Scott - Showgirl
* Louise London - Showgirl
* Natalie Newport - Showgirl
* Linzi Drew - Showgirl
* Maria Harper - Showgirl
* Samantha Devonshire - Showgirl
* Carla Lawrence - Showgirl
* Ruth Chapman - Showgirl
* Kalla Ryan - Showgirl

==Bibliography==
* Simon Sheridan Keeping the British End Up: Four Decades of Saucy Cinema 2011 (fourth edition) (Titan Publishing, London) ISBN 0857682792
* Simon Sheridan Come Play with Me: The Life and Films of Mary Millington 1999 (FAB Press, Guildford)
* Sweet, Matthew. Shepperton Babylon: The Lost Worlds of British Cinema. Faber and Faber, 2005.

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 