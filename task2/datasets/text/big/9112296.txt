Shaadi Karke Phas Gaya Yaar
{{Infobox film
| name = Shaadi Karke Phas Gaya Yaar शादी करके फँस गया यार  
| image = SKPGY.jpg
| writer = K.S. Adiyaman Rumi Jaffery
| starring = Salman Khan Shilpa Shetty
| director = K. S. Adhiyaman
| producer = Bubby Kent
| distributor = Sahara One
| cinematography = W.B. Rao
| released =  
| country = India
| runtime =
| language = Hindi
| music = Daboo Malik
| awards =
| budget =
}}
 Madhavan starrer, Priyasakhi.

Shooting took place in London, United Kingdom and in India. The film was earlier titled Dil Chura Ke Chal Diye. 

==Synopsis==
Ayaan lives a very wealthy lifestyle with his married brother, Karan and his wife, Anju; a younger sister, Yamini, and brother, Rahul; his mom, and grandma; and runs a garage. One day he meets with beautiful model, Ahana, and falls head over heels in love with her. She forgets her personal diary at his garage, and through this Ayaan woos, wins her heart, and both get married. Ahana soon finds out that despite of Ayaans wealth, his family is very conservative and tradition-bound. This causes some bitterness between the newly-weds, which gets worse when she becomes pregnant and wants to abort the child. While visiting her mom on their dogs birthday, Ahana has an accident which results in a miscarriage. Ayaan blames her for losing the child, but his mother convinces him and he apologizes. Few months later, Ahana becomes pregnant again, and thats when she finds out that Ayaan had pulled wool over her eyes through her personal diary. Watch what happens when Ahana decides to get drunk on her birthday and expose Ayaan publicly.

==Cast==
* Salman Khan ... Ayaan
* Shilpa Shetty ... Ahana Kapoor
* Reema Lagoo ... Ayaans Mother
* Amneek Sandhu ... Ayaans Sister
* Mohnish Behl ... Karan
* Shakti Kapoor ... Mr. Kapoor
* Supriya Karnik ... Mrs. Kapoor
* Aashif Sheikh ... Bunty
* Neena Kulkarni ... Ayaans Grandmother
* Kunika Lal ... Mrs. Kapoors Friend

==Soundtrack==
{{Infobox album |  
 Name = Shaadi Karke Phas Gaya Yaar |
 Type = Soundtrack |
 Artist = Sajid-Wajid |
 Cover = | 2006 |
 Recorded = | Feature film soundtrack |
 Length = |
 Label = Tips Industries|Tips|
 Producer = Sajid-Wajid |
 Reviews = |
 The Killer   (2006) |
 This album = Shaadi Karke Phas Gaya Yaar (2006) |
 Next album = Jaane Hoga Kya  (2006) |
}}

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
! # !! Title !! Singer(s) !! Length

|-
| 1
| Dil Yeh Bahalta Nahin
| Sonu Nigam, Sunidhi Chauhan
| 04:47
|-
| 2
| Deewane Dil Ko Jaane Na
| Alka Yagnik, Sonu Nigam, Sunidhi Chauhan
| 04:16
|-
| 3
| Taaron Ko Mohabbat Amber Se
| Alka Yagnik, Udit Narayan
| 04:42
|-
| 4
| Shadi Kar Ke Abhijeet
| 02:00
|-
| 5
| Tujh Se
| Sunidhi Chauhan
| 05:06
|-
| 6
| Shaadi Kar Ke Phas Gaya Yaar
| Sonu Nigam, Sunidhi Chauhan
| 05:11
|-
| 7
| Kuch Bhi Nahi Tha Hariharan (singer)|Hariharan, Alka Yagnik
| 05:11
|}

==References==
 

==External links==
*  
 

 
 
 
 
 