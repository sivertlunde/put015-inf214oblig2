The Squeaker (1931 film)
 
 
{{Infobox film
| name           = The Squeaker
| image          = 
| caption        = 
| director       = Martin Frič Karel Lamač
| producer       = Karel Lamač
| writer         = Edgar Wallace Rudolf Katscher
| starring       = Lissy Arna
| music          = 
| cinematography = Otto Heller
| editing        = 
| distributor    = 
| released       =  
| runtime        = 64 minutes
| country        = Germany
| language       = German
| budget         = 
}}
The Squeaker ( ,  ) is a 1931 German drama film directed by Martin Frič and Karel Lamač.   

==Cast==
* Lissy Arna as Lillie / Millie Trent (as Lissi Arna)
* Karl Ludwig Diehl as Captain Leslie (as Carl Ludwig Diehl)
* Fritz Rasp as Frank Sutton
* Peggy Norman as Beryl Stedman, seine Nichte (as Peggy Normann)
* Paul Hörbiger as Josuah Harras, Reporter
* S. Z. Sakall as Bill "Billy" Anerley (as Szöke Szakall)
* Robert Thoeren as Charles "Charly" Tillmann
* John Mylong as Harry "Juwelen Harry" Webber (as Jack Mylong-Münz)
* Ernest Reicher as Inspektor Elford, Scotland Yard
* Karl Forest as Sergeant Miller
* Fritz Greiner as Falschspieler
* Marianne Kupfer as Zena
* Antonie Jaeckel as Garderobiere im Leopard-Club

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 