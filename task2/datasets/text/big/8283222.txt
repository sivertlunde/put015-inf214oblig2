Jungle Drums of Africa
{{Infobox film
| name = Jungle Drums of Africa
| image = Jungledrumsofafricaposter.JPG
| caption = Original poster for the serial
| director =Fred C. Brannon
| producer =Franklin Adreon
| writer =Ronald Davidson
| starring =Clayton Moore Phyllis Coates Johnny Spencer John Cason Roy Glenn
| cinematography =John MacBurnie
| distributor =Republic Pictures
| released       =   21 January 1953 (serial) {{cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement
 | year = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 132–133
 | chapter =
 }}    1966 (TV) 
| runtime         = 12 chapters / 167 minutes (serial)  100 minutes (TV) 
| language = English
| budget          = $172,840 (negative cost: $167,758) 
| awards =
}}
 American Movie serial film, shot in black-and-white, which was an original commissioned screenplay by Ronald Davidson produced by Franklin Adreon and directed by Fred C. Brannon for Republic Pictures.  The story is set in Kenya, and involves the efforts of an American uranium processing companys representative and a woman medical missionary, to thwart the efforts of agents of a "foreign power", abetted by a disaffected native witchdoctor, to gain control of a large uranium deposit on lands owned by the latters tribe.  This serial features black American actors in major roles, including that of a college-educated chieftain.

==Plot==
The daughter of a medical missionary in Africa carries on her fathers work after he dies. She later befriends two adventurers prospecting for uranium. But it isnt long before long she finds herself in danger from crooks trying to get the uranium for themselves and a local witch doctor, who sees her as a threat to his power...

==Cast==
* Clayton Moore as Alan King
* Phyllis Coates as Carol Bryant
* Johnny Spencer as Bert Hadley Henry Rowland as Kurgan
* John Cason as Regas
* Roy Glenn as Naganto, the witch doctor Bill Walker as Chief Douanga Steve Mitchell as Gauss, the store clerk henchman
* Don Blackman as Naganto

==Production==
Jungle Drums of Darkest Africa was budgeted at $172,840 although the final negative cost was $167,758 (a $5,082, or 2.9%, under spend).  It was the most expensive Republic serial of 1953. 

It was filmed between 29 September and 18 October 1952 under the working title Robin Hood of Darkest Africa.   The serials production number was 1935. 

===Stunts=== Tom Steele as Alan King/Third Constable (doubling Clayton Moore)

===Special Effects===
Special effects created by the Lydecker brothers.

==Release==
===Theatrical===
Jungle Drums of Darkest Africas official release date is 21 January 1953, although this is actually the date the sixth chapter was made available to film exchanges. 

This was followed by a re-release of Adventures of Captain Marvel, re-titled as Return of Captain Marvel, instead of a new serial.  The next new serial, Canadian Mounties vs. Atomic Invaders, followed in the summer. 

===Television===
Jungle Drums of Darkest Africa was one of twenty-six Republic serials re-released as a film on television in 1966.  The title of the film was changed to U-238 and the Witch Doctor.  This version was cut down to 100-minutes in length. 

==Critical reception==
Harmon and Glut describes this serial as "an uninteresting arrangement of stock footage and clichés." {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut 
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury
 | year = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | chapter = 1. The Girls "Who Is That Girl in the Buzz Saw?"
 | page = 16
 }} 

==Chapter titles==
# Jungle Ambush (20min)
# Savage Strategy (13min 20s)
# The Beast Fiend (13min 20s)
# Voodoo Vengeance (13min 20s)
# The Lion Pit (13min 20s)
# Underground Tornado (13min 20s)
# Cavern of Doom (13min 20s)
# The Water Trap (13min 20s)
# Trail to Destruction (13min 20s)
# The Flaming Ring (13min 20s) - a clipshow|re-cap chapter
# Bridge of Death (13min 20s)
# The Avenging River (13min 20s)
 Source:   {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = Filmography
 | page = 254
 }} 

==See also==
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
*  

 
 
{{s-bef
| rows=2
| before=Zombies of the Stratosphere (1952 in film|1952}}
{{s-ttl
| rows=2 Republic Serial Serial
| years=Jungle Drums of Africa (1953 in film|1953)}}
{{s-aft
| after=Canadian Mounties vs. Atomic Invaders (1953 in film|1953) (Serial)}}
|-
{{s-aft
| after=  (1953 in television|1953) (Intended to be a TV series)}}
 

 
 

 
 
 
 
 
 
 
 
 