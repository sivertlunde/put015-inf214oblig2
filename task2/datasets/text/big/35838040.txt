Faun (film)
{{Infobox film
| name           = Faun
| image          = 
| image_size     = 
| caption        = 
| director       = Alexander Korda
| producer       = Alexander Korda
| writer         = Eduard Knoblauch (play)   Richárd Falk   László Vajda   
| narrator       = 
| starring       = Gábor Rajnay Dezsõ Gyárfás Artúr Somlay   Ica von Lenkeffy
| music          = 
| editing        = 
| cinematography = 
| studio         = Corvin Film
| distributor    = 
| released       = 1918
| runtime        = 
| country        = Hungary Silent  Hungarian intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Hungarian silent silent drama film directed by Alexander Korda and starring Gábor Rajnay, Dezsõ Gyárfás and Artúr Somlay. It was based on a play by Eduard Knoblauch. 

==Cast==
* Gábor Rajnay - a Faun 
* Dezsõ Gyárfás   
* Artúr Somlay   
* Ica von Lenkeffy  
* Paula Horváth   
* Erzsi Ághy   
* János Ducret   
* Jenõ Horváth   
* Gyula Bartos   
* József Hajdú  

==References==
 

==Bibliography==
* Kulik, Karol. Alexander Korda: The Man Who Could Work Miracles. Virgin Books, 1990.

==External links==
* 

 

 

 
 
 
 
 
 
 