BMX Bandits (film)
 
 
{{Infobox film
| name           = BMX Bandits
| image          = Bmxbanditsposter83.jpg
| caption        = Theatrical release poster
| director       = Brian Trenchard-Smith
| producer       = Tom Broadbridge Paul F. Davies
| screenplay     = Russell Hagg
| story          = Patrick Edgeworth
| narrator       = Angelo DAngelo James Lugton David Argue
| music          = Colin Stead Frank Strangio
| cinematography = John Seale
| editing        = Alan Lake
| studio         = Nilsen Premiere
| distributor    = Filmways Australasian Distributor Umbrella Entertainment
| released       = 22 December 1983
| runtime        = 88 minutes
| country        = Australia
| language       = English
| budget         = just over $1 million 
| preceded_by    =
| followed_by    =
}}
 adventure crime crime drama film starring Nicole Kidman.

==Plot==
After a successful Sydney bank robbery at the "Montgomery Street Bank" where they grabbed $101,586 (with the robbers wearing pig masks and brandishing shotguns), the man in charge, The Boss (Bryan Marshall), plans a further and larger payroll robbery for two days later worth at least $1.5 million, hoping that he can trust his less-than-competent gang headed by Whitey (David Argue) and Moustache (John Ley) to do the job properly, with anyone who doesnt answering to him.
 Warringah Mall during the school holidays in order to be able to buy her own BMX bike, and accidentally get Judy fired from her job when they crash into trolleys pushed away by the local "Creep" (Brian Sloman). The three go out in Gooses dads runabout on the harbor searching for cockels to sell in order to fix their own crashed bikes, as well as getting Judy her own, and stumble onto and steal a box of police-band walkie talkies (all the way from the US of A according to "The Boss") that the bank robbers were hoping to use to monitor on police traffic. Ironically, after stealing the box, the kids pass Whitey and Moustache who are on their way in their high powered motorboat to pick it up.
 Bayside Police are able to hear the kids using the walkie talkies and the Sergeant (Bill Brady) warns them not to use restricted police wavelength and tells them to turn them in to the nearest police station (causing confusion for his not so bright constable (Peter Browne) who is on point duty at an accident site). Judy, P.J. and Goose are also unaware that the robbers know who stole the box and that they are selling them to other kids in the area. After they are spotted and chased late at night through a cemetery by Whitey and Moustache wearing monster masks (going formal according to Whitey), they manage to escape. Judy is caught the next day by Whitey and Moustache while getting a second walkie talkie for The Creep, but escapes with the help of P.J. and Goose. The trio are finally arrested but escape police custody and, with the help of the local kids, launch their own plan to foil the planned payroll robbery.

The film boils down to a cartoonish chase across opportunistic sites around Sydney, including a memorable escape down the Manly Waterworks water slides, complete with BMX bikes.

The film was also known as Short Wave for American bootleg distribution.   

==Filming locations==
* Northern Beaches Warringah Mall
* Manly Cemetery
* Manly Oval
* Manly Waterworks
* The Corso
* Manly Beach Sydney Harbour

==Production==
Brian Trenchard-Smith was hired after the producers had been impressed by his handling of action in Turkey Shoot. He says the script was originally set in Melbourne but he persuaded them to re-set it in Sydney to take advantage of that citys locations. He set it on the northern beaches and wrote action sequences based on "my concept for the BMX action being putting BMX bikes where BMX bikes aren’t meant to be."   accessed 8 February 2013  The movie was shot over 41 days, a longer than normal shoot because of the labour restrictions caused by the fact many of the cast were under 16. Trenchard-Smith:

 I wanted to capture the spirit of the Ealing comedies and British films of the ‘50s and ‘60s that were clearly aimed at children and delivered action and fun in a largely cartoonish way. If you look at the basic premise of the plot, the crooks clearly want to or intend to kill the children at some point, so how do you disguise that and make that palatable to an audience of kids and parents? You make the crooks buffoonish, the gang that couldn’t shoot straight, so that takes the curse  off the underlying purpose.  

Nicole Kidman sprained her ankle during filming. No female stunt double that looked like her could be found so her bike stunts were performed by an 18-year-old man in a wig. 

==Awards==
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards  (1983 Australian Film Institute Awards)  AACTA Award Best Adapted Screenplay Patrick Edgeworth
| 
|- AACTA Award Best Supporting Actor David Argue
| 
|- AACTA Award Best Editing Alan Lake
| 
|- AACTA Award Best Sound Andrew Steuart
| 
|- John Patterson
| 
|- Robin Judge
| 
|- Phil Judd
| 
|- Gethin Creagh
| 
|-
|}

==Box office==
BMX Bandits grossed $124,649 at the box office in Australia. 
Kidmans performance led to her being cast in the TV series Five Mile Creek where she was directed by Trenchard Smith in some episodes. 

==Critical reception==
The film was release in the UK. The Guardian said "theres a girl called Nicole Kidman whos rather good". Heavens above
Malcolm, Derek. The Guardian (1959-2003)   19 July 1984: 11.  

==Home media==
BMX Bandits was released on DVD by Umbrella Entertainment in August 2010. The DVD is compatible with all region codes and includes special features such as the trailer and audio commentary with Brian Trenchard-Smith, Eric Trenchard-Smith & Chalet Trenchard-Smith. A bonus disc includes a photo gallery, press clippings, Nicole Kidman discussing the film on Young Talent Time, and a featurette titled BMX Buddies with Brian Trenchard-Smith, Tom Broadbridge, Patrick Edgeworth, Russell Hagg and James Lugton.

2013 region free DVD and Blu-Ray was released as an updated version which includes all footage and bonus material from previous versions on 1 disc.    

A regular edition was released on DVD by Umbrella Entertainment in January 2012 without the bonus disc.   

==References in popular culture==
American rock band Wheatus has a song titled "BMX Bandits" on their album TooSoonMonsoon. The song has been dedicated to Nicole Kidman, and includes the lyrics "Hey Nicole" in the chorus. In the songs animated music video, there is an animated caricature of Kidman.

The show That Mitchell and Webb Look parodied the film in the recurrent adventures titled "Angel Summoner and BMX Bandit". 

==See also==
* Cinema of Australia

==References==
 

==External links==
*  
*  
*  at Oz Movies
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 