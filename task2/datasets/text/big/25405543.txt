What If... (2010 film)
:Not to be confused with The F Word (2013 film)
{{Infobox film
| name           = What If...
| image          = What If (film).jpg
| alt            = 
| caption        = 
| director       = Dallas Jenkins
| producer       = 
| writer         = Chuck Konzelman Andrea Gyertson Nasfell Cary Solomon
| starring       = Kevin Sorbo Kristy Swanson Debby Ryan Caeden Lovato John Ratzenberger Kristin Minter Toni Trucks Stelio Savante
| music          = Jeehun Hwang
| cinematography = Todd Barron
| editing        = Frank Reynolds
| studio         = Jenkins Entertainment
| distributor    = Pure Flix Entertainment
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
What If... is a 2010 Christian/drama film directed by Dallas Jenkins. It stars Kevin Sorbo, Kristy Swanson, Debby Ryan and John Ratzenberger. The film was released to theaters on August 20, 2010. It is the first film in a two-movie partnership between Jenkins Entertainment and Pure Flix Entertainment.

== Plot == doctorate in Theology with a minor in Biblical Archaeology from Moody Bible Institute.
 alternate reality married to Wendy with two daughters - rebellious teen Kimberly (Debby Ryan), and Kimberlys little sister, Megan (portrayed by newcomer Taylor Groothuis) - getting ready for church on a Sunday morning, where he’s scheduled to give his first sermon as the new pastor. The mechanic explains that this is an experience God has put in place to show Ben what would have happened if hed accepted his other calling, the great "What If".

If Ben wants to get back to his old life, he must first learn to appreciate the value of faith and family, and perhaps rediscover the love of his life. In the tradition of Its a Wonderful Life and The Family Man, What If... tells the story of a man whose glimpse into what he’s missing reminds him of what he truly wants.

== Production ==
What If... was filmed June 28–July 14, 2009, mostly in Manistee, Michigan at 10 West Studios, with some filming done in Grand Rapids, Michigan.    Jenkins Entertainment is owned by Jerry B. Jenkins and operated by his son Dallas Jenkins.  Jerry will serve as executive producer and Dallas will direct both films. Pure Flix Entertainment will produce and supervise marketing and distribution for the films. 

== Release ==
What If...  was screened at the Christian Writers Guild conference in Denver|Denver, Colorado. The audience, which included Jerry B. Jenkins and his wife, gave the film a standing ovation. 

== References ==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 