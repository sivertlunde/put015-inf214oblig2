The Moon's Our Home
{{Infobox film
| name           = The Moons Our Home
| image          = The Moons Our Home.jpg
| image_size     = 
| caption        = 
| director       = William A. Seiter
| producer       = Walter Wanger Alan Campbell Isabel Dawn Boyce DeGaw Dorothy Parker
| narrator       = 
| starring       = Henry Fonda Margaret Sullavan Walter Brennan Henrietta Crosman
| music          = 
| cinematography = 
| editing        = 
| distributor    = Paramount Pictures
| released       =  
| country        = United States
| runtime        = 80 minutes
| language       = English
| budget         = $402,573 Matthew Bernstein, Walter Wagner: Hollywood Independent, Minnesota Press, 2000 p436 
| gross = $417,663 
| preceded_by    = 
| followed_by    = 
}} 1936 film directed by William A. Seiter.

==Plot summary==
A comedy about marriage and everything relating to it. New York novelist Henry Fonda meets up with an actress, Margaret Sullavan, and the two date and later marry, though neither knows of the others fame. The real adventure begins on the honeymoon, when this screwball comedy really heats up with insults and arguments.

==Reception==
The film recorded a loss of $111,845. 

==References==
 

==External links==
*  

==On Radio==
The Moons Our Home was produced as part of the Lux Radio Theater on February 10, 1941 with Jimmy Stewart and Carole Lombard in the leading roles.

 
 

 
 
 
 
 
 
 
 
 


 