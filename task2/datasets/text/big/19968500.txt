Monsieur Beaucaire (1946 film)
{{Infobox film
| name           = Monsieur Beaucaire
| image          = Monsieur Beaucaire Poster.jpg
| image_size     =
| caption        = George Marshall Paul Jones
| writer         = Melvin Frank Norman Panama Booth Tarkington (novel) Frank Tashlin (uncredited)
| starring       = Bob Hope Joan Caulfield Patric Knowles
| music          =
| cinematography = Lionel Lindon
| editing        = Arthur P. Schmidt
| distributor    = Paramount Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} 1946 comedy novel of Monsieur Beaucaire.

==Plot==
King Louis XV of France is invited by his rival King Philip V of Spain to choose a suitable husband for Philips daughter, Princess Maria, as a gesture of unity between their two nations. Louiss choice is Duc le Chandre, but the duke fancies Madame Pompadour, as does the king.

Louis barber, Beaucaire, becomes tangled in a web of deceit along with Mimi, a chambermaid he loves. Both end up exiled from France, and after Beaucaire assists the duke in hiding Madame Pompadour, all must ward off General Don Francisco, who is planning to overthrow Philip so that he can rule Spain.

After a series of mistakes and misadventures, Beaucaire shows his bravery in a swordfight with Don Francisco, and is rewarded by the duke coming to his rescue. 

==Cast==
*Bob Hope as Monsieur Beaucaire
*Joan Caulfield as Mimi
*Patric Knowles as Duc le Chandre
*Marjorie Reynolds as Princess Maria of Spain
*Cecil Kellaway as Count DArmand
*Joseph Schildkraut as Don Francisco
*Reginald Owen as King Louis XV
*Constance Collier as The Queen of France Madame Pompadour
*Fortunio Bonanova as Don Carlos
*Douglass Dumbrille as George Washington
*Mary Nash as The Duenna
*Leonid Kinskey as Rene King Philip II
*Lewis Russell as Chief Justice

==External links==
* 
*  
*  

 
 

 
 
 
 
 
 
 


 