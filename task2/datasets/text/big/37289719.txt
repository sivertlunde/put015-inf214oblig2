Third Person Plural
 
{{Infobox film
| name           = Third Person Plural
| image          = 
| image size     =
| caption        = 
| director       = James Ricketson
| producer       =
| writer         = James Ricketson
| based on = 
| narrator       =
| starring       = Bryan Brown
| music          = 
| cinematography = 
| editing        = 
| studio = 
| distributor    = 
| released       = 1978
| runtime        = 92 mins
| country        = Australia English
| budget         = AU$30,000 
| gross = 
| preceded by    =
| followed by    =
}}
Third Person Plural is a 1978 film directed by James Ricketson. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p136 

The script was devised by the actors and director in a workshop. 

James Ricketson has since called the film an "experiment":
 I just wanted to see whether it would be possible to make a film on that small budget, shoot the whole thing with a hand-held camera, integrate improvised dialogue with scripted dialogue, work with a small core of actors on a character-based piece, which is what I did, and then to approach the editing of the film in an innovative way. Now, I happen not to like the film myself. Having done the film - it was fun to do it - I decided that I didnt like it, and it certainly wasnt the direction that I wanted to go in.  

==References==
 

==External links==
*  at IMDB
*  at Australian Screen Online
*  at Oz Movies
 
 
 


 