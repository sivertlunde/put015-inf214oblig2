Gunman in the Streets
{{Infobox film
| name           = Gunman in the Streets
| image          = Gunmaninthestreets.jpg
| image_size     =
| caption        = Cover of DVD release
| director       = Frank Tuttle (English version) Borys Lewin (French version)
| producer       = Victor Pahlen
| writer         = Victor Pahlen (English version) Jacques Companéez André Tabet (French version)
| narrator       =
| starring       = Dane Clark Simone Signoret Fernand Gravey Robert Duke
| music          = Joe Hajos
| cinematography = Claude Renoir Eugen Schüfftan
| editing        = Steve Previn
| distributor    = DisCina International Film Distributors (Canada)
| released       = 1950
| runtime        = 86 min
| country        = France
| language       = English / French
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} television syndication from 1963.

The low-budget B-movie was shot on location in Paris, France. A French-language version of the film (directed by Borys Lewin) was released in France under the title Le Traque ("The Hunt").

==Plot==
American army deserter turned criminal on the run Eddy Roback is being chased through the streets of Paris. The fugitive finds his old girlfriend, Denise Vernon (Signoret) and tries to get money from her in an attempt to get across the border to Belgium. The girlfriends friend, an American crime reporter (Duke), as well as a country-wide man hunt become obstacles Roback must get past in order to escape. While trying to raise him money, Denise finds him a hiding place in the studio of a lecherous photographer Max Salva, who may have turned Roback in.

== Cast ==

*Dane Clark as Eddy Roback
*Simone Signoret as Denise Vernon
*Fernand Gravey as Commissioner Dufresne (as Fernand Gravet)
*Robert Duke as Frank Clinton
*Michel André as Max Salva

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 