Miss Beatty's Children
{{Infobox film
| name           = Miss Beattys Children
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Pamela Rooks  NFDC Doordarshan Rooks AV
| writer         =
| screenplay     =  Pamela Rooks James Killough
| story          = 
| based on       =   
| narrator       = 
| starring       =  Jenny Seagrove  Faith Brook  Protima Bedi Zakir Hussain Venu
| editing        = Renu Saluja
| studio         = 
| distributor    = 
| released       = 1992 
| runtime        = 112 min.
| country        = India
| language       = English
| budget         = 
| gross          =
}}

Miss Beattys Children is a 1992 English drama film directed by Pamela Rooks in a directorial debut, with Jenny Seagrove, Faith Brook and Protima Bedi in lead roles. {{cite web |url=http://www.chaosmag.in/pamela.html |title=Pamela Rooks |website=Chaosmag Database 
|accessdate=2014-04-25}}   The film set in 1936 in South India, was based on Pamela Rooks own novel by the same name. 
 Best First Best Cinematography for Venu (cinematographer)|Venu. 

==Plot==
The film is set in 1936 in South India, when Jane, an English schoolteacher comes to Trippuvur to work with Mabel Forster, a Christian missionary, who runs a mission school, and works towards young girls from being sold into temple prostitution. She has Kamla Devi, a senior temple woman as her opponent. Once when Mabel has gone away Jane finds herself trapped in a local controversy. She rescues an ANglo-Indian girl and takes her to Ooty, but back in the town she is accused of kidnapping. However she manages to find help with an American doctor, Alan Chandler. Eventually she adopts Amber, and rescues several more children.

==Cast==
* Jenny Seagrove as Jane Beatty
* Faith Brook as  Mabel Forster
* Protima Bedi as Kamla Devi
* D. W. Moffett as Alan Chandler Barry John
* Emma Sanderson Rituraj Singh
* Catherine Stevens
* Cecil Qadir

==Bibliography==
*  

==References==
 

==External links==
*  
*   at CinemasofIndia, NFDC

 

 
 
 
 
 
 
 
 
 
 
 