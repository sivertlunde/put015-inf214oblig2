Swapnam Kondu Thulabharam
{{Infobox film
| name           =Swapnam Kondu Thulabharam
| image          = 
| image size     =
| caption        =
| director       = Rajasenan
| producer       = Khader Hassan
| writer         = Reghunath Paleri
| narrator       = Nandana
| music          = Ouseppachan 
| cinematography = Ramachandra Babu 
| editing        = Raja Mohammad
| distributor    = 
| released       = 2003
| runtime        =
| country        = India Malayalam
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
Swapnam Kondu Thulabharam is a Malayalam language film. It was released in 2003. 

==Plot==
The story of the movie was based on a strong relationship between two brothers (Suresh Gopi and Kunchacko Boban) in a family . But due to some misunderstandings they are separated. The movie deals with the later happenings in the story.

==Cast==
* Suresh Gopi as Vishnu
* Kunchacko Boban as Aniyankuttan
* Shrutika as Ammu Nandana as Kalyani Janardhanan as Madhavan Thampi, Aniyankuttans father
* Srividya as Prabhavathi, Aniyankuttans mother
* Jagathi Sreekumar as Sivan Kutti
* Bindu Paniker as Hostel Matron
* Mithun Ramesh as Aniyankuttans friend
* Kalamandalam Kesavan as Vishnus real father

==References==
 

 
 
 

 