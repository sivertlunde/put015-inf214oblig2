Umbrage (film)
{{Infobox film
| name           = Umbrage
| image          = umbrage film poster.jpg
| caption        = theatrical poster
| director       = Drew Cullingham
| producer       = Drew Cullingham Charlie Falconer
| writer         = Drew Cullingham James Fisher
| music          = Captain Bliss Huskie Jack
| cinematography = James Friend
| editing        = Drew Cullingham
| studio         = Motion Picture House Grindstone Entertainment Lionsgate & Umbrella Entertainment  (DVD release) 
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          = 
}} James Fisher.     The film had its first public screening on October 31, 2009, at the London FrightFest Film Festival,  and in 2010 was picked up by Lionsgate for 2011 and 2012 DVD distribution.      

==Plot== Irish cowboy Phelan (Jonnie Hurn) hanging out in their barn.  Phelan is a vampire and himself a former vampire hunter.  He tells Rachel that the real danger to everyone is Lilith, and explains how Lilith had been Adams willful first wife who, since having been exiled from the Garden of Eden, had been spawning demonic offspring in the world of man. Considered the mother to the Succubi, ruler of shadows, slaughterer of children, and consort to the devil himself, she had been re-awakened by, and drawn to the location of, the occult power inherent in the mirror carried by Jacob. Phelan has been harboring a centuries-long grudge against her for she having "turned" him 120 years earlier, and he had been tracking her for decades, equipped with the one relic that might finally kill her... a bone splinter from the rib of Adam himself.

==Cast==
* Doug Bradley as Jacob
* Rita Ramnani as Rachel James Fisher as Stanley Adam in Garden
* Scott North as Travis (as Scott Thomas)
* Jonnie Hurn as Phelan
* Natalie Celino as Lilith
* Grace Vallorani as Lauren
* Yoram Halberstam as older Adam Sammaelson
* Victoria Broom as Voices

==Release== Abertoir Horror film festivals through 2009 and 2010.   The film became available on DVD in the UK in October 2011, with a United States and Canada DVD release to be through Lionsgate in 2012.  
 Grindstone Entertainment, with release through Lionsgate Films expected.     Upon the films UK DVD release in October 2011, Fangoria revisited the topic to share an exclusive video excerpt and photos.   

===DVD===
DVD extras include a 20-minute "making of" documentary, a music video for Hokie Joint, a video interview the films lead actor Doug Bradley, and an audio interview of Bradley and director Drew Cullingham.    

==Critical reception==
Dread Central described the film, Drew Cullinghams first directorial effort, as "ambitious", and that the film was an "eclectic mix of family melodrama, vampire action, supernatural horror and Western styling". The review stated that these diverse elements were its downfall, "leaving a wildly inconsistent yet occasionally promising piece of work,"  that it was not lacking in well-conceived individual elements, and the backgrounds of the characters are decently explored.  But the major problem was in the directors inability to deliver on the potential of those elements, with his efforts "cack-handed at best, and laughable at worst."  The reviewer appreciated his "eye for cinema" and his ability to make the film look like it cost more to produce than one might have expected from its small budget. Further, the cast "for the most part" performed admirably; Bradley "seems to be having a lot of fun in a normal role" as opposed to his usual genre fare, and carried his character of a family man with aplomb.  His co-star Rita Ramnani was "effectively annoying and self-centered as Rachel", but was let down by the screenplay, not by any failing on her part. The review considered Jonnie Hurns Phelan to be "the most interesting character here, both on page and screen," and a well-nuanced and suitably impressive anti-hero.  Natalie Celino as Lilith was faulted for her "constantly off-kilter, disinterested line delivery and a Scottish accent so thick it proves almost impenetrable."    
 Buffy the Abertoir Horror film festivals, the film was more of a "supernatural/fantasy type of film, featuring plenty of banter and a playing against type performance from the legendary Doug Bradley," and it runs "against a lot of the horror genres conventions, and also a lot of film conventions in general."  The director had explained to the audience that the film was "never really intended as a vampire film and stressed that neither is the word vampire used anywhere throughout the films duration", and that his intention was to "take the monster out of the genre and turn this particular type of horror creature into a proper, well developed character", creating a likable anti-hero.  Of Hurns Phelan, the review stated that he gave "an outstanding, multi-dimensional performance" which "makes the character appear fascinating, entertaining and sexy", in a manner missing from Edward Cullen in the The Twilight Saga (film series)|Twilight series, and presents him with "the same kind of Clint Eastwood style presence on screen".    

MJ Simpson panned the film for not meeting its potential, stating that the films trailer had him anticipating release of the feature, but the film itself was "disappointing, frustrating and ultimately unsatisfying," despite feeling that it did well in trying "to do something different".
Simpson wrote that it was a "good-looking movie", due to the cinematography of James Friend, the production design by Charlie Falconer, and the gore effects by Scott Orr, but the main problem with the film is its script. Noting that Drew Cullingham was writer, director, producer, and editor, Simpson stated, "When a single individual has that much input into a film, there’s potential for a pure, unsullied, personal vision to shine through - but there’s equal potential for major problems to be glossed over or ignored."  Simpson described the "threat" in the film as "ill-defined", a serious flaw for any horror film. He wrote that none of the films characters have a clear motivation for their actions. As examples, Simpson gave the lack of any logical reason why a husband and wife expecting their first child together would decide that the middle of winter would be a good time to move to an "isolated 17th century farmhouse 20 miles from the nearest town which has no telephone connection and no mobile signal"; why two adult men would decide to camp together in a wood in the middle of nowhere; why one would become so immediately enamored by a total stranger that he would follow her into the woods for a tryst; or why, after that friend is horribly maimed, the second man would abandon him and hike away with that same woman looking for help. Simpson noted that several more incongruous scenes served a purpose in progressing the plot, but had "no purpose within the reality of the film".  The repeated flaw, he stated, is that " ime after time, characters behave in a way which is illogical, inexplicable or just downright nonsensical - but expedient. ... Umbrage is a mishmash of ideas that hasn’t been properly worked out."
Summarizing, Simpson stated, "If you want a British vampire western, this is probably the best you can pick. Doug Bradley buckles down to his usual reliable performance, some of the other acting is also good (some less so) and there are some clever moments. But Umbrage could have been so much better and its problems stem not from any unexpected snowfall or malfunctioning generator but from a poor script which was nowhere near ready to shoot."  

Umbrage received mixed reviews from critics which praised the acting talent of Jonnie Hurn. At Horrortalk.com, Charlotte Stear wrote, "Basically, this film tried too much to do lots all at once and with its obviously small budget, its main failure is how ambitious it is for what it can realistically do."  Horrorphilia gave the film 5 stars out of 10 and referred its "unfortunate wasted potential." 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 