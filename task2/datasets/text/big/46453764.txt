Premonition (2006 film)
{{Infobox film
| name           = Premonition 
| image          = 
| caption        =
| director       = Jean-Pierre Darroussin
| producer       = Patrick Sobelman 
| screenplay     = Jean-Pierre Darroussin Valérie Stroh 
| based on       =  
| starring       = Jean-Pierre Darroussin
| music          = Albert Marcœur
| cinematography = Bernard Cavalié 
| editing        = Nelly Quettier 
| studio         = Agat Films & Cie   France 2 Cinéma Bac Films
| distributor    = Bac Films
| released       =  
| runtime        = 97 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
 Louis Delluc Prize for Best First Film in 2006.

== Cast ==
 
* Jean-Pierre Darroussin as Charles Benesteau
* Valérie Stroh as Isabelle Chevasse
* Amandine Jannin as Sabrina Jozic
* Hippolyte Girardot as Marc Bénesteau
* Nathalie Richard as Gabrielle Charmes-Aicquart
* Natalia Dontcheva as Helena Jozic
* Ivan Franek as Thomas Jozic
* Anne Canovas as Alice Benesteau
* Laurence Roy as Edith Benesteau
* Jonathan Altman as Ferdinand Benesteau
* Aristide Demonico as Monsieur Serrurier
* Michele Ernou as Madame Serrurier
* Mbembo as Eugénie
* Didier Bezace as Albert Testat
* Maurice Chevit as An old man
* Patrick Bonnel as Jean
* Vittoria Scognamiglio as Farida Garibaldi
* Thibault de Montalembert as The police inspector
* Lou-Nil Font as Victor Chevasse
* Alain Libolt as Edouard Benesteau
* François Monnié as Monsieur Garibaldi
* Antoine Valli as The neighbour
 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 