Argo (2012 film)
 
{{Infobox film
| name           = Argo
| image          = Argo2012Poster.jpg
| alt            =  
| caption        = Theatrical release poster
| tagline        = "The movie was fake. The mission was real."
| director       = Ben Affleck
| producer       = Ben Affleck George Clooney Grant Heslov
| based on       =    
| screenplay     = Chris Terrio
| starring       = Ben Affleck Bryan Cranston Alan Arkin John Goodman
| music          = Alexandre Desplat
| cinematography = Rodrigo Prieto
| editing        = William Goldenberg GK Films Smokehouse Pictures
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 120 minutes 
| country        = United States
| language       = English
| budget         = $44.5 million   
| gross          = $232.3 million 
}}
Argo is a 2012 American  ,"  in which Mendez led the rescue of six U.S. diplomats from Tehran, Iran, during the 1979-1981 Iran hostage crisis.   

The film stars Affleck as Mendez with  , directed by Lamont Johnson.  
 Best Picture, Best Adapted Best Film Best Motion Best Director, Best Supporting Outstanding Performance Outstanding Performance Best Film, Best Editing, Best Director at the 66th British Academy Film Awards.

Argo has been criticized for its portrayal of events; especially for minimizing the role of the Canadian embassy in the rescue, for falsely showing that the Americans were turned away by the British and New Zealand embassies, and for exaggerating the danger that the group faced during events preceding their escape from the country.

==Plot== United States Ken Taylor. exfiltrating them from Iran. Tony Mendez, a U.S. Central Intelligence Agency exfiltration specialist, is brought in for consultation. He criticizes the proposals, but is at a loss when asked for an alternative. While on the phone with his son, he is inspired by watching Battle for the Planet of the Apes and begins plans for creating a cover story for the escapees:  that they are Canadian filmmakers who happened to be in Iran scouting exotic locations for a similar science-fiction film.
 John Chambers, Star Wars, shredded before the takeover and learn that some personnel have escaped.
 bazaar to maintain their cover story takes a bad turn, but their Iranian culture contact gets them away from the hostile crowd.
 planned military rescue of the hostages. He pushes ahead, forcing his boss Jack ODonnell to hastily re-obtain authorization for the mission to get tickets on a Swissair flight. Tension rises at the airport, where the escapees flight reservations are confirmed at the last minute, and a guards call to the supposed production company in Hollywood is answered at the last second. The group boards the plane, which takes off just as the Revolutionary Guards at the airport uncover the ruse and try to stop them.

To protect the hostages remaining in Tehran from retaliation, all U.S. involvement in the rescue is suppressed, giving full credit to the Canadian government and its ambassador (who left Iran with his wife under their own credentials as the operation was underway; their Iranian housekeeper, who had known about the Americans and lied to the revolutionaries to protect them, escaped to Baathist Iraq|Iraq). Mendez is awarded the Intelligence Star, but due to the missions classified nature, he would not be able to officially receive the medal until the details were publicized in 1997. All the hostages were freed on January 20, 1981. The film ends with Carters speech about the crisis and the Canadian Caper.

==Cast==
 
 
* Ben Affleck as Tony Mendez
* Bryan Cranston as Jack ODonnell
* Alan Arkin as Lester Siegel John Chambers
* Tate Donovan as Robert Anders
* Clea DuVall as Cora Lijek
* Christopher Denham as Mark Lijek
* Scoot McNairy as Joe Stafford
* Kerry Bishé as Kathy Stafford
* Rory Cochrane as Lee Schatz Ken Taylor
* Kyle Chandler as Hamilton Jordan (White House Chief of Staff)
* Chris Messina as Malinov
* Željko Ivanek as Robert Pender
* Titus Welliver as Jon Bates
* Bob Gunton as Cyrus Vance (United States Secretary of State)
* Philip Baker Hall as Stansfield Turner (Director of Central Intelligence)  
* Richard Kind as Max Klein
* Richard Dillane as Peter Nicholls
* Keith Szarabajka as Adam Engell
* Michael Parks as Jack Kirby
* Tom Lenk as Rodd
* Christopher Stanley as Tom Ahern
* Page Leong as Pat Taylor
* Taylor Schilling as Christine Mendez
* Ashley Wood as Beauty
* Barry Livingston as David Marmor, CIA official
* Sheila Vand as Sahar
* Mina Kavani as Sahar (Voice)
* Nikka Far as Tehran Mary
* Omid Abtahi as Reza
* Karina Logue as Elizabeth Ann Swift
* Adrienne Barbeau as Nina
* Fouad Hajji as Komiteh
 

==Production==
  before going to Iran.]]
Argo is based on the "Canadian Caper" that took place during the Iran hostage crisis in 1979 and 1980. Chris Terrio wrote the screenplay based on Joshuah Bearmans 2007 article in Wired (magazine)|Wired: "How the CIA Used a Fake Sci-Fi Flick to Rescue Americans from Tehran."  The article was written after the records were declassified.

In 2007, the producers George Clooney, Grant Heslov and David Klawans set up a project based on the article. Afflecks participation was announced in February 2011.  The following June, Alan Arkin was the first person cast in the film.    After the rest of the roles were cast, filming began in Los Angeles  in August 2011. Additional filming took place in McLean, Virginia; Washington, D.C.; and Istanbul.    The scene in which Mendez drives up to and walks into the CIA headquarters lobby was filmed with permission at the CIAs original headquarters building in Virginia; all other scenes set at the CIA were filmed in the basement of the Los Angeles Times Building. 
 Dance the Night Away" by Van Halen and "When the Levee Breaks" by Led Zeppelin.  For its part, Warner Bros. used its 1972–1984 title featuring the "Big W" logo designed by Saul Bass for Warner Communications to open the film and painted on its studio lots famed water tower the logo of The Burbank Studios (the facilitys name during the 1970s and 1980s when Warner shared it with Columbia Pictures). 

The screenplay used by the CIA to create their cover story was an adaptation of Roger Zelaznys 1967 novel Lord of Light.  Producer Barry Gellar had spearheaded an earlier attempt to produce the film with the books original title.  After that production attempt had failed, the screenplay was renamed Argo and used by the CIA.  Higgins, Bill; Kit, Borys. "Argos odd Hollywood history." Hollywood Reporter. October 5, 2012: 64. eLibrary. March 1, 2013. 

According to Tony Mendez, Studio Six—the phony Hollywood production office he helped create at the core of the CIA plan—proved so convincing that even weeks after the rescue was complete and the office had folded, 26 scripts were delivered to its address, including one from Steven Spielberg.   

==Soundtrack==
{{Infobox album  
| Name        = Argo (Original Motion Picture Soundtrack)
| Type        = film
| Artist      = Alexandre Desplat
| Cover       = Argo_soundtrack.jpg
| Released    = October 9, 2012
| Recorded    = Classical
| Length      = 58:37
| Label       = WaterTower Music
| Producer    = Alexandre Desplat
}}

;Track listing
{{Track listing
| all_writing     = Alexandre Desplat, except where noted
| total_length    = 58:37
| title1          = Argo
| length1         = 3:38
| title2          = A Spy in Tehran
| length2         = 4:18
| title3          = Scent of Death
| length3         = 3:25
| title4          = The Mission
| length4         = 2:07
| title5          = Hotel Messages
| length5         = 2:03
| title6          = Held Up By Guards
| length6         = 5:31
| title7          = The Business Card
| length7         = 2:55
| title8          = Breaking Through the Gates
| length8         = 3:50
| title9          = Tony Grills the Six
| length9         = 3:30
| title10         = The Six Are Missing
| length10        = 3:21
| title11         = Sweatshop
| length11        = 1:31
| title12         = Drive to the Airport
| length12        = 3:45
| title13         = Missing Home
| length13        = 3:00
| title14         = Istanbul (The Blue Mosque)
| length14        = 2:18
| title15         = Bazaar
| length15        = 3:45
| title16         = Cleared Iranian Airspace
| length16        = 6:01
| title17         = Hace Tuto Guagua
| note17          = Familion
| length17        = 3:39
 
}}

==Release and reception== Academy Award Best Picture, Diplomatic Room of the White House, February 24, 2013.]]

===Critical response===
Argo was acclaimed by American critics, praising Ben Afflecks direction, the cast (especially Alan Arkin and John Goodman) and the editing.  , which assigns a weighted average rating out of 100 to reviews from mainstream critics, the film has received an average score of 86, considered to be "universal acclaim," based on 45 reviews.   

Naming Argo one of the best 11 films of 2012, critic   said it felt "like a movie from an earlier era — less frenetic, less showy, more focused on narrative than sensation," but that the script included "too many characters that he doesn’t quite develop." 

Writing in the Chicago Sun-Times, Roger Ebert said, 
 The craft in this film is rare. It is so easy to manufacture a thriller from chases and gunfire, and so very hard to fine-tune it out of exquisite timing and a plot thats so clear to us we wonder why it isnt obvious to the Iranians. After all, who in their right mind would believe a space opera was being filmed in Iran during the hostage crisis?  Ebert gave the film 4/4 stars, calling it "spellbinding" and "surprisingly funny," and chose it as the best film of the year. Ebert, Roger (10 October 2012).  . Chicago Sun-Times. Retrieved January 16, 2013. 

Literary critic Stanley Fish says that the film is a standard caper film in which "some improbable task has to be pulled off by a combination of ingenuity, training, deception and luck." He goes on to describe the films structure: "(1) the presentation of the scheme to reluctant and unimaginative superiors, (2) the transformation of a ragtag bunch of neer-do-wells and wackos into a coherent, coordinated unit and (3) the carrying out of the task." 

Although he thinks the film is good at building and sustaining suspense, he concludes,
 This is one of those movies that depend on your not thinking much about it; for as soon as you reflect on whats happening rather than being swept up in the narrative flow, there doesnt seem much to it aside from the skill with which suspense is maintained despite the fact that you know in advance how its going to turn out. ... Once the deed is successfully done, theres really nothing much to say, and anything that is said seems contrived. That is the virtue of an entertainment like this; it doesnt linger in the memory and provoke afterthoughts.    

===Reaction by Iranians=== the cabinet members advocated freeing all the American personnel quickly.  Jian Ghomeshi, a Canadian writer and radio figure of Iranian descent, thought the film had a "deeply troubling portrayal of the Iranian people." Ghomeshi asserted "among all the rave reviews, virtually no one in the mainstream media has called out   unbalanced depiction of an entire ethnic national group, and the broader implications of the portrait." He also suggested that the timing of the film was poor, as American and Iranian political relations were at a low point.  University of Michigan history professor Juan Cole had a similar assessment, writing that the films narrative fails to provide adequate historical context for the events it portrays, and such errors of omission lead all of the Iranian characters in the film to be depicted as ethnic stereotypes.  A November 3, 2012 article in the Los Angeles Times claimed that the film had received very little attention in Tehran, though Masoumeh Ebtekar, who was the spokesperson of the students who took the hostages and called only "Tehran Mary" in the films credits, said that the film did not show "the real reasons behind the event." 

Bootleg DVDs have become popular and are estimated at "several hundreds of thousands" of copies. Interpretations of the films popularity in Iran have varied, ranging from the fact that the movie portrays the excesses of the revolution and the hostage crisis, which had been long glorified in Iran, to regular Iranians viewing it as a somber reminder of what caused the poor relations with America and the ensuing cost to Iran, decades after the embassy takeover.    The high DVD sales are suggested as a form of silent protest against the governments ongoing hostility to relations with America.  

===Top ten lists===
Professional reviewers ranked the film with other releases for 2012, as follows:
{| class="wikitable sortable"
! Rank
! Reviewer
! Publication
|-
| rowspan=3 align="center" | 1st
| Christy Lemire || Associated Press
|-
| Joe Neumaier and Elizabeth Weitzman || New York Daily News
|-
| Roger Ebert || Chicago Sun-Times
|-
| rowspan=3 align="center" | 2nd
| Lisa Kennedy || Denver Post
|-
| Lou Lumenick || New York Post
|-
| Richard Roeper || Richardroeper.com
|-
| rowspan=2 align="center" | 4th
| Betsy Sharkey || Los Angeles Times
|-
| Kyle Smith || New York Post
|-
| rowspan=3 align="center" | 5th
| Lisa Schwarzbaum || Entertainment Weekly
|-
| Peter Travers || Rolling Stone
|-
| Stephen Holden || The New York Times
|-
| rowspan=2 align="center" | 6th
| Mary Pols || Time (magazine)|Time
|-
| Mick LaSalle || San Francisco Chronicle
|-
| rowspan=3 align="center" | 7th
| Ann Hornaday || Washington Post
|- Anne Thompson || Indiewire
|-
| Ty Burr || Boston Globe
|-
| rowspan=1 align="center" | 8th
| Owen Gleiberman || Entertainment Weekly
|-
| rowspan=1 align="center" | 9th
| Peter Bradshaw || The Guardian
|-
| rowspan=7 align="center" | Top 10  (ranked alphabetically)
| Bob Mondello || NPR
|-
| Calvin Wilson || St. Louis Post-Dispatch
|-
| Claudia Puig || USA Today
|-
| David Denby || The New Yorker
|-
| Joe Morgenstern || The Wall Street Journal
|- Joe Williams || St. Louis Post-Dispatch
|-
| Kenneth Turan || Los Angeles Times
|}

===Box office===
The film earned $136,024,128 in North America, and $96,300,000 in other countries, for a worldwide total of $232,324,128. 

===Home media=== UltraViolet digital copy. 

===Accolades===
 
The film was nominated for seven    and Quentin Tarantino, whose film Django Unchained was nominated in several categories. 

Entertainment Weekly wrote about this controversy:

 

==Historical inaccuracies==

===Canadian versus CIA roles===
After the film was previewed at the   wrote, "Even that hardly does Canada justice." 
 

In a CNN interview, former U.S. president Jimmy Carter addressed the controversy: 
 90% of the contributions to the ideas and the consummation of the plan was Canadian. And the movie gives almost full credit to the American CIA. And with that exception, the movie is very good. But Ben Afflecks character in the film was... only in Tehran a day and a half. And the main hero, in my opinion, was Ken Taylor, who was the Canadian ambassador who orchestrated the entire process.  Taylor noted, "In reality, Canada was responsible for the six and the CIA was a junior partner. But I realize this is a movie and you have to keep the audience on the edge of their seats."  In the film, Taylor is shown threatening to close the Canadian embassy. This did not happen and the Canadians never considered abandoning the six Americans who had taken refuge under their protection. 

Affleck asserted: 
 Because we say its based on a true story, rather than this is a true story, were allowed to take some dramatic license. Theres a spirit of truth .... the kinds of things that are really important to be true are—for example, the relationship between the U.S. and Canada. The U.S. stood up collectively as a nation and said, "We like you, we appreciate you, we respect you, and were in your debt."... There were folks who didnt want to stick their necks out and the Canadians did. They said, "Well risk our diplomatic standing, our lives, by harbouring six Americans because its the right thing to do."  Because of that, their lives were saved."  

===British and New Zealand roles===
Upon its release in October 2012, the film was criticized for its suggestion that  ar agent played in the film by Tate Donovan, said, "They put their lives on the line for us. We were all at risk. I hope no one in Britain will be offended by whats said in the film. The British were good to us and were forever grateful."   
 Sir John British ambassador to Iran, said,  Martin Williams, secretary to Sir John Graham in Iran at the time, was the one who found the Americans, after searching for them in his own British car (the only Austin Maxi in Iran) and first sheltered them in his own house.  The sequence in the film when a housekeeper confronts a truckload of Iranian revolutionary guards at the Canadian ambassadors home bears a striking resemblance to Williams story. He has told how a brave guard, Iskander Khan, confronted heavily armed revolutionary guards and convinced them that no-one was in when they tried to search Williams house during a blackout. Williams said, "They went away. We and the Americans had a very lucky escape." The fugitives later moved to the residence of the Canadian ambassador.   

Affleck is quoted as saying to The Sunday Telegraph: "I struggled with this long and hard, because it casts Britain and New Zealand in a way that is not totally fair. But I was setting up a situation where you needed to get a sense that these six people had nowhere else to go. It does not mean to diminish anyone." 

<!-- This section does not meet WP:WEIGHT for an article about the film itself === "I rescued Argo hostages in my orange Austin Maxi" ===
First Secretary Martin Williams, who was in the British Embassy in Iran in November 1979 when the US Embassy was under siege, recounted:
"It was on a cold evening in January that my wife Sue and I set off from our home in Kent to watch Argo at the Odeon in Earls Court, West London. We had arranged to meet several colleagues who had also served in Tehran and were, naturally, looking forward to seeing how the film would depict what had been a significant and dramatic time in our lives...Not only had the film-makers airbrushed British involvement, but they claimed we had actually turned our back on our closest  ally at a desperate time of need.  It was gratuitous, insensitive and  completely inaccurate. My wife and I both bristled. You might wonder why it matters. It’s just a film, after all. Well, it matters because not many people know the truth about what happened. Argo has been touted as a ‘true story’, but it has been given such a Hollywood spin that it is actually historically damaging to the reputation of the British diplomatic service." 

I had been working in Tehran for nearly two years, during the time the revolution had thrown the country into a state of turmoil. There had been mass public anti-Shah and anti-Western demonstrations on the streets, incidents when soldiers fired into the crowds and the British Embassy had been invaded by angry students and set alight in 1978, while we were inside – fortunately without casualties.
We continued to get regular anonymous threats, for while the Iranians considered America to be enemy No 1, or the Great Satan, Britain was the Little Satan.  So it was not entirely surprising when a mob stormed the American Embassy on November 4. We didn’t know then how long it would last. But we had no hesitation in helping when, at about 5pm the following day, I was told that several people had evaded capture and I should go and find them. I set off in my dusty orange Austin Maxi, which Sue and I had driven all the way from England in late 1977. It was pretty distinct and the only one in Iran; it also had a prominent GB sticker on the back." 

"They got into our cars in a state of great nervousness and anxiety, bringing just small bags with them. I think I took two and Gordon had three with him. We were all very anxious and the Americans initially wanted to duck down in the vehicles, but I argued that was unwise. Both the car and Land Rover were relatively small, so there was a good chance that someone could have  spotted them trying to hide. It would have been pretty tricky too, if we were stopped at a roadblock. We decided the safest thing to do was to take the Americans to my home in Gulhak, which was the British Embassy’s residential compound, north of the city." 

"The compound was not completely secure. The large number of Iranian staff meant there were many opportunities for them to be discovered, but we would never have turned them away, as the film claims.  That night, armed Iranian militants came to search the compound, probably looking  for the missing Americans, but were turned away at the gates by Iskander Khan, the chief guard and a former member of the British Indian army. He told them there was nothing to search for and refused to let them in. They went away, but we suspected they might come back.  It was therefore decided, the next day, that the Americans should move on. They had the address of a Thai cook who worked for someone in the US Embassy. Just after dark we made another risky journey to drive them there.  That was not, however, the end of our involvement. On November 10, we were asked once more to move the Americans, this time to the home of John Sheardown, a senior diplomat at the Canadian Embassy...Let me say that, although I was disappointed by the inaccuracies,  I thought Argo was a great piece of entertainment. I can see why it won the Oscar for Best Film, but it is a semi-fictional account only...The truth is very different, and I think it only right to get the correct information out to the public." -->
On March 12, 2013, the New Zealand House of Representatives censured Affleck by  unanimously agreeing to the following motion, initiated by New Zealand First leader Winston Peters:
 ... this House acknowledge  with gratitude the efforts of former New Zealand diplomats Chris Beeby and Richard Sewell in assisting American hostages in Tehran during the hostage crisis in 1979, and express  its regret that the director of the movie Argo saw fit to mislead the world about what actually happened during that crisis when, in reality, our courageous diplomats’ inspirational actions were of significant help to the American hostages and deserve the factual and historical record to be corrected.   

===Imminent danger to the group===
In the film, the diplomats face suspicious glances from Iranians whenever they go out in public, and appear close to being caught at many steps along the way to their freedom. In reality, the diplomats never appeared to be in imminent danger.    Rather than the team having to withstand scrutiny while buying tickets, Taylors wife bought three sets of plane tickets from three different airlines ahead of time, without any issues.  
* The film depicts a dramatic last-minute cancellation of the mission by the Carter administration and Mendez declaring he will proceed with the mission. Carter delayed authorization by only 30 minutes, and that was before Mendez had left Europe for Iran.   
* The film portrays a tense situation when the crew tries to board the plane, and their identities are nearly discovered. No such confrontation with security officials took place at the departure gate.    
* The film has a dramatic chase sequence as the plane takes off; this did not occur.  As Mark Lijek described it, "Fortunately for us, there were very few Revolutionary Guards in the area. It is why we turned up for a flight at 5.30 in the morning; even they werent zealous enough to be there that early. The truth is the immigration officers barely looked at us and we were processed out in the regular way. We got on the flight to Zurich and then we were taken to the US ambassadors residence in Bern. It was that straightforward."   

===Other inaccuracies===
The film contains other historical inaccuracies:
* The screenplay has the escapees—Mark and Cora Lijek, Bob Anders, Lee Schatz, and Joe and Kathy Stafford—settling down to enforced cohabitation at the residence of the Canadian ambassador Ken Taylor. In reality, after several nights, the group was split between the Taylor house and the home of another Canadian official, John Sheardown.  
* "Its not true we could never go outside. John Sheardowns house had an interior courtyard with a garden and we could walk there freely," Mark Lijek says. 
* The major role of producer Lester Siegel, played by Alan Arkin, is fictional. 
* In the depiction of a frantic effort by CIA headquarters, in Langley, to get President Jimmy Carter to re-authorize the mission so that previously purchased airline tickets would still be valid, a CIA officer is portrayed as getting the White House telephone operator to connect him to Chief of Staff Hamilton Jordan by impersonating a representative of the school attended by Jordans children. In reality, Jordan was unmarried and had no children at the time.  book of the same name by Roger Zelazny. The CIA changed the title to Argo.  
* Comic book illustrator Jack Kirby did not do his storyboard work for the fabricated CIA film production. He created these when there was an attempt to produce Lord of Light a few years before the Iranian hostage situation.  
* The Hollywood Sign is shown dilapidated as it had been in the 1970s. The sign had been repaired in 1978, two years prior to the events described in the film. 
* There is a modern day flag of the Democratic Republic of the Congo visible in one of the windows of the CIA headquarters building. In 1979/1980 the country was known as Zaire and flew a completely different flag. registration of HB-ISO. Although Swissair has the aircraft with that registration, the registration belongs to the McDonnell Douglas DC-9-51. 

==Casting controversy==
Some Hispanics and film critics criticized the casting of Ben Affleck in the role of Mendez, who is of Mexican ancestry on his fathers side.     Mexican-American actor and director Edward James Olmos considered Afflecks casting as Mendez a "mistake," and that the actor "had no sense of the cultural dynamic of the character he was playing." 

However, Mendez has said that he was unconcerned about the casting, and noted he does not identify as Hispanic. 

==See also==
* On Wings of Eagles - a near factual account of Ross Perot organizing a successful attempt to free several of his employees from Iran just after their revolution.

 

== References ==
 

;Further reading
 
* " , The Guardian, 13 November 2012.
 

==External links==
 
*  
*  
*  
*  
*  

{{Navboxes
|list= 
 
 
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 