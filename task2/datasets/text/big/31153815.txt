Ponnapuram Kotta
{{Infobox film
| name           = Ponnapuram Kotta
| image          =
| caption        =
| director       = Kunchacko
| producer       = Kunchacko
| writer         = N. Govindankutty
| screenplay     =
| starring       = Prem Nazir Kaviyoor Ponnamma KPAC Lalitha Adoor Bhasi  Rajasree
| music          = G. Devarajan
| cinematography =
| editing        = Veerappan
| studio         = Udaya
| distributor    = Udaya
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film,  directed and produced by Kunchacko. The film stars Prem Nazir, Kaviyoor Ponnamma, KPAC Lalitha and Adoor Bhasi in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir
*Kaviyoor Ponnamma
*KPAC Lalitha
*Adoor Bhasi
*Thikkurissi Sukumaran Nair
*Manavalan Joseph
*Adoor Pankajam
*Alummoodan
*Aranmula Ponnamma GK Pillai
*Rajasree
*K. P. Ummer
*N. Govindankutty
*Premji
*S. P. Pillai
*Sabnam
*Vijayanirmala
*Vijayasree
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar and AP Gopalan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aadiparaashakthi || K. J. Yesudas, P Susheela, P. Leela, P. Madhuri, PB Sreenivas || Vayalar ||
|-
| 2 || Chaamundeshwari || K. J. Yesudas || Vayalar ||
|-
| 3 || Manthramothiram || K. J. Yesudas || Vayalar ||
|-
| 4 || Nalacharithathile || P Susheela || Vayalar ||
|-
| 5 || Roopavathi Ruchiraangi || K. J. Yesudas || Vayalar ||
|-
| 6 || Valliyoorkkaavile || P Jayachandran || Vayalar ||
|-
| 7 || Vayanaadan Keloonte || K. J. Yesudas, P. Madhuri || AP Gopalan ||
|}

==References==
 

==External links==
*  

 
 
 


 