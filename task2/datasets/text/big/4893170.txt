The Dogs of War (film)
 
{{Infobox film
| name           = The Dogs of War
| image          = Dogs of war ver2.jpg
| caption        = Theatrical release poster by Tom Jung
| image_size     =
| director       = John Irvin 
| producer       = Larry DeWaay Norman Jewison Patrick J. Palmer
| writer         =     George Malko
| starring       = Christopher Walken Tom Berenger Colin Blakely
| music          = Geoffrey Burgon
| cinematography = Jack Cardiff
| editing        = Antony Gibbs
| released       = 17 December 1980
| distributor    = United Artists (1980, original) Metro-Goldwyn-Mayer|MGM(2001, DVD)
| runtime        = 102 minutes
| country        = United States United Kingdom
| language       = English
| budget         =
| gross          =$5,484,132 
}}
 of the same name by Frederick Forsyth, directed by John Irvin. It stars Christopher Walken and Tom Berenger as part of a small, international unit of mercenary soldiers privately hired to depose President Kimba of a fictional "Republic of Zangaro", in Africa, so that a British tycoon can gain mining access to a huge platinum deposit.  This movie was filmed on location in Belize.
 Julius Caesar written in 1599; the full quotation is  "Cry, Havoc!, and let slip the dogs of war."

==Plot==

As the film opens, mercenaries Jamie Shannon, Drew, Derek, Michel, Terry and Richard are making a hasty exit from a war-torn Central American country by forcing their way onto a government civilian DC-3 airplane. When a Central American officer demands Richards body to be removed to make room for others, one of the mercenaries shows he is still able to safely grasp the spoon of a hand grenade and Drew insists his friend "goes home". After Shannon returns home, he gets an offer from a British businessman named Endean who is interested in "certain resources" of a small African nation named Zangaro. Endean pays Shannon $15,000 to go on a reconnaissance mission in Zangaro which is run by a paranoid and brutal dictator named General Kimba. 

Shannon arrives in Zangaros capital of Clarence and meets a British documentary filmmaker named North who tells him Zangaros history and scouts out the defences of the military garrison. However, his activities arouse the suspicions of Zangaros police and he is arrested, severely beaten and thrown in jail. His multiple wounds are treated by Dr. Okoye, a physician who was formerly a moderate political leader and the only local politician of whom North approved but who was imprisoned by General Kimba four years ago. North agitates for his release and Shannon is deported after two days of torture. His physician tells him that all the damage he has sustained has taken years off his lifespan.  

After Shannon tells Endean that there is no chance of an internal coup, Endean offers Shannon $100,000 to overthrow Kimba by invading Zangaro with a mercenary army. Endean intends to install a puppet government led by Colonel Bobi, Kimbas brutal and greedy former ally. This would allow Endean to exploit the countrys newly discovered platinum resources as Colonel Bobi has already signed away the mineral rights. Shannon refuses the offer and decides to leave his mercenary life behind. He meets his estranged wife and proposes that they start a new life in Colorado or Montana. She turns him down, noting that she does not think that he has changed. Shannon then accepts Endeans offer to organize an attack on Zangaro with the condition that he will have complete control of the operation.
 rocket launchers, mines and other weapons from arms dealers.

By chance he encounters North, who was expelled from Zangaro shortly after Shannon. North believes Shannon is a CIA agent heading back to Zangaro and tries to tail him. Shannon asks Drew to scare North away without hurting him but instead North is killed by someone who had been hired by Endean to follow Shannon and his crew. A furious Shannon kills him in turn and leaves his body at Endeans house during a dinner party held for Colonel Bobi.

He hires a small freighter and crew to transport the team to the coast of Zangaro and purchases a variety of other equipment that will be used in the attack such as Zodiac Group|Zodiac-style motorboats. 

At sea, the team is joined by a group of Zangaron exiles trained as soldiers by a former mercenary colleague, Jinja. Once ashore in Zangaro, the mercenaries use their entire array of weapons to attack the military garrison where Kimba lives. Drew enters a shack in the barracks courtyard and is killed by a seemingly helpless young woman who shoots him in the back with a pistol. After the mercenaries storm the burning, bullet-scarred ruins of the garrison, Shannon makes his way inside Kimbas mansion. He blasts his way upstairs to find Kimba frantically stuffing bricks of pristine bills into a briefcase. Kimba desperately offers Shannon a fortune for his life but is killed anyway.
 helicopter with Colonel Bobi and they enter the presidential residence where they find Shannon and Dr. Okoye quietly awaiting their overdue arrival. Shannon introduces Dr. Okoye as Zangaros new president. Endean and Bobi protest, but Shannon had already warned Endean about the consequences of not showing up on time. Shannon casually kills Bobi, at which point Endean stops complaining. Shannon, Derek and Michel load the body of Drew onto a Landrover and leave. The story ends with the mercenaries driving through the deserted streets of Clarence.

==Cast==
  
* Christopher Walken as Jamie Shannon
* Tom Berenger as Drew Blakeley
* Colin Blakely as Alan North
* Hugh Millais as Roy Endean Paul Freeman as Derek Godwin
* Jean-Francois Stevenin as Michel Claude  
* JoBeth Williams as Jessie Shannon Robert Urquhart as Capt. Lockhart
 
* Winston Ntshona as Dr. Okoye
* Pedro Armendáriz Jr. as The Captain George Harris as Col. Sekou Bobi
* Alan Beckwith as The mercenary
* Eddie Tagoe as Jinja
* Ed ONeill as Terry
* Harlan Cary Poe as Richard
* Olu Jacobs as Customs Officer
 

==Production==

The opening Central American scene was filmed at the Miami Glider Port southwest of Miami, Florida. Later African country scenes were filmed in Belize City, Belize, and the surrounding area. The manually-turned swinging bridge shown during the attack is one of the largest of its kind in the world. The film features several weapons that were prominent in popular culture during the 1980s. The Uzi submachine guns used in the movie (changed from the German Second World War vintage MP 40s of the novel) were actually a mix of real Uzis and set-dressed Ingram Mac-10|MAC-10s.    Shannons grenade launcher, depicted in the promotional poster, dubbed the "XM-18" in the film, is a Manville gun - a design later used by the MM-1 grenade launcher. 
 Raw Deal), Don Cheadle (Hamburger Hill) and Michael Caine (Shiner (2000 film)|Shiner).

Cinematographer Jack Cardiff had previously directed an account of mercenaries in Africa entitled Dark of the Sun.  Composer Geoffrey Burgon concludes the film with A. E. Housmans Epitaph for an Army of Mercenaries sung over the end titles by Gillian McPherson.

Michael Cimino also did a rewrite of The Dogs of War but wasnt credited.

==DVD==
The Dogs of War was released to DVD by MGM Home Video on November 20th, 2001 as a Region 1 widescreen DVD and later on by Twilight Time (under license from MGM) as a multiregion widescreen Blu-Ray DVD.

==References==
 

==External links==
*  
*   at Internet Movie Firearms Database

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 