The Hot Angel
{{Infobox film
| name           = The Hot Angel
| image          = The Hot Angel poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Joe Parker 
| producer       = Stanley Kallis 
| screenplay     = Stanley Kallis 
| starring       = Jackie Loughery Ed Kemmer Mason Alan Dinehart Emory Parnell Lyle Talbot Boyd Santell
| music          = Richard Markowitz 	
| cinematography = Karl Struss
| editing        = Leon Selditz Eda Warren
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The Hot Angel is a 1958 American drama film directed by Joe Parker and written by Stanley Kallis. The film stars Jackie Loughery, Ed Kemmer, Mason Alan Dinehart, Emory Parnell, Lyle Talbot and Boyd Santell. The film was released on December 1958, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Jackie Loughery as Mandy Wilson
*Ed Kemmer as Chuck Lawson
*Mason Alan Dinehart as Joe Wilson
*Emory Parnell as Judd Pfeifer
*Lyle Talbot as Van Richards
*Boyd Santell as Mick Pfeiffer
*Heather Ames as Lynn Conners
*Steffi Sidney as Myrna John Nolan as Ray
*Richard Stauffer as Monk
*Kathi Thornton as Liz
*Harold Mallet as Pilot

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 