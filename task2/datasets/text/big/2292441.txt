Tokyo Drifter
 

{{Infobox film
| name           = Tokyo Drifter
| image          = Tokyo Drifter poster.jpg
| caption        = Theatrical poster
| director       = Seijun Suzuki
| producer       = Tetsuro Nakagawa
| writer         = Yasunori Kawauchi
| starring       = Tetsuya Watari Chieko Matsubara Hideaki Nitani
| music          = Hajime Kaburagi
| cinematography = Shigeyoshi Mine
| editing        = Shinya Inoue
| studio    = Nikkatsu
| released       = April 10, 1966
| runtime        = 83 minutes
| country        = Japan
| language       = Japanese
| budget         = USD|US$20 million
}} hitman "Phoenix" Tetsu who is forced to roam Japan avoiding execution by rival gangs.

==Plot== real estate scam. Looking to profit from the scheme himself and fearing that his group is threatened by his presence, Kurata asks Tetsu to leave and live the life of a Vagabond (person)|drifter. 

Otsuka and Kurata join forces, and assign the successful hitman, "Viper" Tatsuzo, to kill Tetsu. Tetsu evades Viper and his hit squad a number of times and arrives at the establishment of Umetani, an ally of boss Kurata. Tetsu returns to Tokyo, confronting his boss who betrayed him. He kills everyone in the room besides his boss and former girlfriend. At the end of the movie, Kurata kills himself, and Tetsu rejects his former girlfriend Chiharus plea to allow her to accompany him on his travels. He exits down a pure white hallway, explaining that he has a new allegiance to the wanderer lifestyle, and cannot abandon it for the company of another. 

==Cast==
* Tetsuya Watari as Tetsuya "Phoenix Tetsu" Hondo
* Chieko Matsubara as Chiharu
* Hideaki Nitani as Kenji Aizawa
* Tamio Kawaji as Tatsuzo The Viper
* Tsuyoshi Yoshida as Keiichi
* Ryuji Kita as Kurata
* Hideaki Esumi as Otsuka
* Eiji Go as Tanaka

==Production==
Nikkatsu bosses had been warning Suzuki to tone down his bizarre visual style for years and drastically reduced Tokyo Drifters budget in hopes of getting results. This had the opposite effect in that Suzuki and art director Takeo Kimura pushed themselves to new heights of surrealism and absurdity. The studios next move was to impose the further restriction of filming in black and white on his next two films, which again Suzuki met with even greater bizarreness culminating in his dismissal for "incomprehensibility". 

Because of budget limitations, Suzuki had to cut connecting shots out of many fights, leading to a need for more creative camera work. 
 absurdist comedy, surrealist film. 

==Themes==
Suzuki displays common themes found in Yakuza films, particularly the theme of loyalty, to parody the message and presentation of traditional Yakuza films. He uses his depictions of Yakuza relationships to show the inherent weakness of the archetype, particularly the possible abuses of power that can arise from unquestioning allegiance. Standish (2005), 300.  Further, the common theme of corporate corruption is also parodied in through exaggeration when the main character becomes an expendable retainer. Standish (2005), 301.  The conventions in the film further parody the conformity of theme and structure apparent in all Japanese film, but most notably in Yakuza films of the time,  particularly its excesses. Bleiler (2004), 632. 

==Style==
The mise en scène of Tokyo Drifter is highly stylized. Standish (2005), 304.  Film reviewer Nikolaos Vryzidis claims that the film crosses over into a number of different genres, but most resembles the avant-garde films occurring in the 1960s. Vryzidis (2010), 282. 
 saloon joins in the brawl against United States Navy sailors, and comical violence is used where no one is permanently injured, despite the large-scale violence of the scene. 

The majority of the film takes place in Tokyo, but portrays the city in a highly stylized manner. Barber (2005), 124.  The opening sequence consists of a mash of images from metropolitan Tokyo, meant to condense the feeling of the city into one sequence. Barber (2005), 125. 

The film opens in stylized black and white, which becomes vibrant color in all subsequent scenes. Vryzidis (2010), 283.  This served to represent Tokyo post-1964 Summer Olympics. 

==Reception==
Vryzidis claims that Suzukis later films, once the studio gave him more freedom, never reached the same level of artistic quality as Tokyo Drifter, where the studio attempted to impose a large amount of control over the project. 

Tetsu, the main character of the film, has also been well received. One reviewer commented that he always looks "cool", even when he is not the toughest guy in the room. 

Stephen Barber called the visualization in Tokyo Drifter "bizarre and individual".  Douglass Pratt praised the film for its quirkiness and character. Pratt (2005), 1246.  He further stated that the plot of the film does not matter so much as "the gorgeous Pop Art sets, the bizarre musical sequences, the confusing but ballistic action scenes and the films gunbutt attitude." 

==Legacy== Ninkyo eiga Jitsuroku eiga Yakuza films, which disavowed the romantic and nostalgic views of the Yakuza in favor of social criticism. 

==Home video==
The Criterion Collection released the film outside of Japan in DVD format in 1999.   Criterion also released a Blu-ray version in 2013.

==Soundtrack==

 

==Footnotes==
 

==References==
 
* 
* 
* 
* 
* 
* 
* 
* 
 

==External links==
*  .
*  
*  
*   essay at the Criterion Collection by Manohla Dargis.
*   by Howard Hampton
*   at the Japanese Movie Database  .

 

 
 
 
 
 
 
 
 
 
 