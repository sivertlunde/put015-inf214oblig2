Far and Away
 
{{Infobox film
| name = Far and Away
| image = Far and away ver1.jpg
| border = yes
| caption = Theatrical release poster
| director = Ron Howard
| producer = Ron Howard Brian Grazer Bob Dolman
| screenplay = Bob Dolman
| story = Ron Howard Bob Dolman
| starring = Tom Cruise Nicole Kidman
| music = John Williams
| cinematography = Mikael Salomon Mike Hill
| studio = Imagine Entertainment
| distributor = Universal Pictures
| released =  
| runtime = 142 minutes
| country = United States
| language = English
| budget = $60 million 
| gross = $137,783,840
}} romance film John Williams. It was screened out of competition at the 1992 Cannes Film Festival.   
 Irish immigrants seeking their fortune in 1890s United States|America, eventually taking part in the Land Run of 1893.  

The film was advertised as being the first movie to be filmed in 70mm since David Leans 1970 film Ryans Daughter, although the film was not shot entirely in 70mm; that distinction would go to Kenneth Branaghs Hamlet (1996 film)|Hamlet.

This was Cyril Cusacks final acting role before his death the following year.

==Plot==
In 1893 Ireland, Joseph Donnellys family home is burned down by his landlords men because of unpaid rent. Vowing revenge, Joseph attempts to kill the landlord, the landlords daughter Shannon has rebelled against family tradition and made plans to claim free land in America. She offers to take Joseph with her as her "servant" so she, a single woman, can travel without scandal. Joseph agrees, convinced he can also stake a land claim.

On a ship bound for America, Shannon meets McGuire, who warns her that the free land is very far away in Oklahoma. She explains that her collection of valuable silver spoons will cover all expenses, and he offers to help her find a shop to sell them to. On arriving in Boston, McGuire is shot, and Shannons spoons fall out of his clothing and get snatched up by passersby. Joseph rescues her but not the spoons. A worker for Ward Boss Mike Kelly, a leader of the Irish immigrant community, introduces them to him. Kelly finds them lodging and jobs, but only one room, which they must share. To avoid scandal, Joseph says shes his sister.

Joseph and Shannon become attracted to each other, but both keep up a front of indifference. One night Joseph rushes out to Boss Kellys club, where a bare knuckle boxing match is underway. Joseph challenges the winner, knocks him out, and soon becomes a regular at the club. Back in Ireland, Donnellys landlords house is burned down by angry tenants in the Irish Land War, so the Christies decide to emigrate to America, hoping also to find their daughter.

Joseph is told Shannon is at Kellys club. Rushing to the club, he discovers Shannon on stage as a burlesque dancer. He tries to cover her with his jacket, demanding that she stop dancing. The Irish men surrounding the couple beg him to fight and offer him a small fortune ($200). Shannon, who previously scorned boxing, urges him to fight, since the money would get them to Oklahoma. Joseph agrees and is winning until he notices one of his backers (a member of the city council) groping Shannon on his lap. Joseph pushes through the crowd to free her, but is pushed back into the ring, where his foot accidentally "toes" the line, falsely signaling he is ready to begin fighting. But he isnt ready, and the Italian lands a sucker punch, after which hes beaten.

In retaliation for the hundreds of dollars Josephs boxing loss has cost Boss Kelly and his friends, Joseph is thrown out into the street outside the club and he meets a policeman who shows him a picture of Shannon asking if hes seen her. He then comes back to the room to find Kelly and his thugs searching their room for the money he and Shannon saved. With their valuables having been stolen by Kellys thugs, theyre both then thrown out into the streets. Joseph and Shannon are left homeless.

Cold and famished, the pair enter a seemingly abandoned luxurious house. Joseph encourages Shannon to pretend the house is hers and he is her servant, but she begs him to pretend they are married and the house is theirs. During that tender moment, the owners of the house return and chase them away, shooting Shannon in the back. Joseph brings Shannon to the Christies, newly arrived from Ireland. He decides Shannon will be better cared for by them, and leaves despite his obvious feelings for her.

Joseph finds work laying track on a railroad, seemingly abandoning his dream of owning land. Told a wagon train he sees out the door of his boxcar is heading for the Oklahoma land rush, Joseph abandons the railroad and joins the wagon train, arriving in Oklahoma Territory just in time for the Land Run of 1893.

Joseph finds Shannon, Stephen, and the Christies already in Oklahoma. Stephen, having seen Joseph talking to Shannon, threatens him that he will kill him if he goes near Shannon again. Joseph buys a horse for the land rush but the horse dies in a few hours and he is forced to ride an unruly horse he manages to tame. On this horse, he quickly outpaces everybody and catches up with Shannon and Stephen, having discovered that Stephen cheated by illegally inspecting the territory before the race and is headed for extremely desirable land he found. 

Joseph is ready to plant his claim flag. Shannon rushes to his side and rejects Stephen when he questions her actions. Joseph professes his love for Shannon. They drive their flag into the ground and claim the land together.

==Cast==
* Tom Cruise as Joseph Donnelly
* Nicole Kidman as Shannon Christie
* Thomas Gibson as Stephen Chase
* Robert Prosky as Daniel Christie
* Barbara Babcock as Nora Christie
* Cyril Cusack as Danty Duff
* Eileen Pollock as Molly Kay
* Colm Meaney as Kelly
* Douglas Gillison as Dermody Michelle Johnson as Grace
* Clint Howard as Flynn
* Rance Howard as Tomlin
* Niall Tóibín as Joseph Donnellys father
* Jamie Gerrard as Child running in background
* Chris Cattan as Extremely Old Man

==Soundtrack==
{{Infobox album|  
| Name        = Far and Away
| Type        = Film score
| Artist      = John Williams
| Cover       = Far and Away - soundtrack cover.jpg
| Border      = yes
| Recorded    = 1992
| Released    = 26 May 1992
| Genre       = Soundtrack
| Length      = 67:12
| Reviews     = 
}}

{{Album ratings Filmtracks
| rev1Score =    
| rev2      = 
| rev2Score = 
}}
 Book of Days" composed and performed by Enya.  The soundtrack was released 26 May 1992 through MCA Records and features 19 tracks of music at a running time just over sixty-seven minutes. 

# "County Galway, June 1892" (1:55)
# "The Fighting Donellys" (2:18) – featured performance by The Chieftains
# "Joe Sr.s Passing/The Duel Scene" (4:41)
# "Leaving Home" (1:55)
# "Burning the Manor House" (2:43)
# "Blowing Off Steam" (1:31)
# "Fighting for Dough" (2:02) – featured performance by The Chieftains
# "Am I Beautiful?" (3:38)
# "The Big Match" (5:56)
# "Inside the Mansion" (4:24)
# "Shannon is Shot" (4:06)
# "Josephs Dream" (3:08)
# "The Reunion" (3:50)
# "Oklahoma Territory" (2:12)
# "The Land Race" (4:56)
# "Settling with Steven/The Race to the River" (4:08)
# "Joseph and Shannon" (3:14)
# " 
# "End Credits" (6:35) – featured performance by The Chieftains

==Reception== Tony Parsons later called it "a stinker of a picture called Far And Away, which was far and away the worst film I have ever seen."  

The film is rated M in Australia, however the rating was later changed to PG in New Zealand.

==References==
 

==External links==
*  
*  
*   at TomCruise.com
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 