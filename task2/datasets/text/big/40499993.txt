The Cardboard Lover
{{infobox film
| title          = The Cardboard Lover
| image          =
| imagesize      =
| caption        =
| director       = Robert Z. Leonard
| producer       = William Randolph Hearst (for Cosmopolitan) Harry Rapf Marion Davies (exec. producer)
| writer         = F. Hugh Herbert Carey Wilson Lucille Newmark (intertitles)
| based on       =  
| starring       = Marion Davies Nils Asther Jetta Goudal
| music          =
| cinematography = John Arnold
| editing        = Basil Wrangell
| distributor    = Metro-Goldwyn-Mayer
| studio         = Cosmopolitan Productions
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent film English intertitles

}} silent film comedy produced by Cosmopolitan Productions and distributed by Metro Goldwyn Mayer. It was directed by Robert Z. Leonard and stars Marion Davies.

The film is based on the play Dans sa candeur naive by Jacques Deval.      (Turner Classic Movies states it is based on Devals novel of the same name.) In London, Tallulah Bankhead played the lead female in the  cast. On Broadway, Jeanne Eagels played the female lead. 

The film survives at the Library of Congress and in the Turner library, but is not available on video, DVD or the Blu-ray.  

It was remade as The Passionate Plumber in 1932 and Her Cardboard Lover in

==Cast==
*Marion Davies - Sally
*Jetta Goudal - Simone
*Nils Asther - Andre
*Andres de Segurola - The Baritone
*Tenen Holtz - Argine
*Pepe Lederer - Argine

uncredited
*Carrie Daumery - Chaperon

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 