La Provocation
{{Infobox film
| name           = La Provocation
| image          = 
| caption        = 
| director       = André Charpak
| producer       = Israel Motion Picture Studios,  Jeune Cinema, Paris
| writer         = André Charpak Jean Verdun
| starring       = Jean Marais Maria Schell
| music          = Dov Seltzer
| cinematography = 
| editing        = 
| distributor    = 
| released       = 11 February 1970 (France)
| runtime        = 90 minutes
| country        = Israel France  West Germany
| awards         = 
| language       = German
| budget         =
}}
 drama film from 1970. It was directed by André Charpak, written by André Charpak and Jean Verdun, starring Jean Marais and Maria Schell. 

== Cast ==
* Jean Marais: Christian
* Maria Schell: Jeanne
* Corinne Le Poulain: Isabelle
* Veit Relin: Bertrand
* André Charpak: André
* Evelyne Dassas: Françoise
* Gerald Robar

== References ==
 

== External links ==
*  
*   at Films de France
*  at Uni France

 
 
 
 
 
 
 
 

 