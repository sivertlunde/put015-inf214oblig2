Bhat De
{{Infobox film
| name   = Bhat De
| image    = 
| caption     = DVD Cover
| director      = Amjal Hossain   
| producer    = Abu Jafor Khan
| writer         = Amjad Hossain Alamgir  Shabana  Anwar Hussain
| music          = Amjad Hossain   Alauddin Ali
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Bangladesh
| language       = Bengali
| budget         = 
| gross          = 
}} Anwar Hossain.

== Cast == Alamgir
* Shabana – Jori
* Rajiv Anwar Hossain
* Anwara
* Akhi Alamgir – child actor 

== Crew ==
* Producer – Abu Zafar Khan
* Director – Amjad Hossain
* Screenplay – Amjad Hossain
* Image Editor – Mujibur Rahman Dulu
* Art Directors – Anjan Bhowmik
* Sound – MA Baset
* Music – Alauddin Ali

== Accolades ==

=== National Film Awards ===
 
In 1984 the film "Bhalt De" has got best film award and including total 9 awards. 
* Won Best Actress –  Shabana 1984 
* Won Best child actor – Akhi Alamgir 1984   
* Won Best Director: Amjad Hossain 1984 
* Won Best Producer: Abu Jafar Khan 1984 
* Won Best Screenwriter Abu Zafar Khan 1984 
* Won Best dialogue writer Abu Zafar Khan 1984 
* Won Best sound customer MA Baset, 1984 
* Won Best Image Editor Mujibur Rahman Dulu 1984 
* Won Best Art Instruction Anjan Bhowmik, 1984 

== International awards ==
The Bengali film "Bhat De" was the part of the Cannes International Film Festival. 

== Music ==

=== Sound track ===
{| border="3" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Songs !! Singer !! Note
|-
| 1
|Koto kanlam koto daklam ailona Tapon Chowdhury
|
|- 2
|Gasher Ekta Pata Jhorle, Kaser Ekta Manush Morle Khobor To Key Rakhena
| Syed Abdul Hadi
|
|- 3
|Chineshi Tomara, Akare Prokare, Diner Alo Rater Adhare Sabina Yasmin
|
|-
|}

==See also==
* Rani Kuthir Baki Itihash
* Biyer Phul

==References==
 

==External links==
*  

 
 
 
 
 
 