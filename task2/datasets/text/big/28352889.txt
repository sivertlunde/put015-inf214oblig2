A Doll's Dream
{{Infobox film
| name           = A Dolls Dream
| image          = 
| image size     =
| caption        = 
| director       = Ivo Caprino
| producer       = 
| writer         = Ivo Caprino
| narrator       =
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = January 1950
| runtime        = 18 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Norwegian animation film directed by Ivo Caprino. It was the first 35mm colour film made in Norway. 

The film is about Lars and Lotte, two children whose father owns a music shop. The father wants them to learn to play, but instead of playing the instruments they destroy them. One night Sandman comes and makes them dream that they go up in the attic where their fathers old instruments are. The instruments come alive, and play melodies. After this, Lars and Lotte no longer want to destroy the instruments, and start playing them instead.

==References==
 

==External links==
*  
*  
*   at Filmweb.no (Norwegian)

 
 
 
 
 
 
 
 
 


 
 