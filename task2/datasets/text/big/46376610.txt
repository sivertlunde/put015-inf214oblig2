Family on Fire
 
{{Infobox film
| name           = Family on Fire
| image          = 
| alt            = 
| caption        = 
| director       = Tade Ogidan
| producer       =Tade Ogidan
| writer         =   Tade Ogidan 
| starring       = Saheed Balogun Segun Arinze  Sola Fosudo
| music          = 
| cinematography = 
| editing        = 
| studio         =  OGD Pictures
| distributor    =  OGD Pictures
| released       = 2012
| runtime        = 145 minutes
| country        = Nigeria
| language       =  Yoruba and subtitled in English
| budget         = 
| gross          = 
}}

Family of Fire is a 2011 Nigerian film produced and directed by Tade Ogidan.
The film stars Saheed Balogun, Segun Arinze, Sola Fosudo and Sola Sobowale.   
The film was premiered on November 4, 2011 at The Lighthouse Hall, Camberwell road, London.   

Prominent actors and actress present includes; Kunle Afolayan, Richard Mofe Damijo,  Ramsey Nouah, Teju Babyface, Saheed Balogun, Segun Arinze and Bimbo Akintola.    Chief of the Defence Staff.   

==Plot==
A young man, Kunle (Saheed Balogun) commits a grievous act that leaves his family members in anguish. In Lagos, Kunle hides cocaine in his mothers (Lanre Hassan) baggage before she pays a visit to his siblings, including the eldest brother Femi (Sola Fosudo) in London. His mother narrowly escapes from the British immigration officers.
A rescheduled flight to the UK foils Kunle’s plans to retrieve the illegal drug. Before his arrival, Femi’s wife (Sola Sobowale) incidentally sees it while unpacking the food items stuffed along with the drug. A student in London, Moyo who is sponsored by Femi, gets remunerated by the drug deals after stealing it from where it is hidden, but the enraged drug barons, led by the Don (Segun Arinze) and his gang, unleash terror on Kunle’s family. Suspense fills the air when Femi or nobody makes any attempt to find Moyo at his school since he is assumed to be in danger. Contrary to expectations, he is on a celebration spree in London before his inevitable arrest. 

==Accolades== Best Nigerian Film and Best Film in an African Language. Both awards were won by Adesuwa and State Of Violence respectively.   

==Cast==
*Saheed Balogun
*Segun Arinze
*Sola Fosudo
*Sola Sobowale
*Bukky Amos
*Lanre Hassan (Iya awero)


==References==
 

 
 
 
 
 
 
 
 
 