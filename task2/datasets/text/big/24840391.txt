Romance of the Western Chamber (1927 film)
{{Infobox film
| name           = Romance of the Western Chamber
| image          = Romance of the Western Chamber poster.jpg
| caption        = 1927 Chinese movie poster
| director       = Hou Yao
| producer       = 
| writer         =  
| cinematography = Liang Linguang Lai Man-Wai
| studio         = Minxin Film Company (Shanghai)
| released       = 8 September 1927 (Hangzhou Theater, China)  2007 (USA)
| runtime        = 42 min. (orig.) 45 min. (2007 DVD release) China
| French intertitles
}}
  silent Cinema Chinese film drama directed by Hou Yao.

The film is an adaption of the classic Chinese dramatic work Romance of the West Chamber by Wang Shifu.

Originally consisting of ten film reels, only 5 reels have survived. 

The 2007 USA DVD release by Cinema Epoch has an additional original musical score composed by Toshiyuki Hiraoka.

== Cast ==
  . accessed 26 October 2009. 
* Lin Chuchu - as Ying Ying, the daughter of the late Prime Minister
* Li Dandan - as Hung Niang, a servant of Ying Ying
* He Minzhuang (M. C. Noo) - as madame Cui, the mother of Ying Ying
* T. K. Kar - as wise student Chang Kung
* Tsao Yao Dein - as an old monk Fa Pen
* Lee Wha Ming/Li Huamin - as Sung Fei Fu (Tiger Sun), the bandit king
* Lu Ying Lang/Li Yinlan - as monk Wei Hing, the messenger with bō staff
* Ge Cijiang - as Zhang Gong
* Wang Longxi - as clever monk
* Hu Chichang - as the White Horse General
* Zhu Yaoting - as the stupid monk
* Huang Ke - as the boy servant

== References ==
 

== Further reading == Zhang Y. (ed.) Cinema and Urban Culture in Shanghai, 1922-1943. Stanford, CA: Stanford University Press, 1999. pp.&nbsp;51–73.

== External links ==
*  
* http://www.dvdtalk.com/silentdvd/romance_of_the.html

 
 
 
 
 
 
 


 
 