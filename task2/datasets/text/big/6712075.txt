George and the Dragon (film)
{{Infobox film
  | name           = George and the Dragon (Dragonsword)
  | image          = GeorgedragonDVD.jpg
  | caption        = DVD cover
  | director       = Tom Reeve   
  | producer       = Steven Saxton Michael Burks Frank Huebner Jeremy Saunders
  | writer         = Tom Reeve Michael Burks  
  | starring       = Piper Perabo James Purefoy Patrick Swayze Michael Clarke Duncan
  | music          = Gast Waltzing
  | cinematography = Joost van Starrenburg
  | editing        = Misch Bervard Jonathan P. Shaw
  | distributor    = American World Pictures (AWP)
  | released       = 2004
  | runtime        = 93 min 
  | country        = United States
  | language       = English 
  | budget         = 
}}

George and the Dragon, (alternative title: Dragon Sword) is a 2004 Sci Fi Channel (United States)|Sci-Fi Channel Historical fantasy film based in a medieval England, and loosely based on the legend of Saint George and the Dragon.

==Synopsis== betrothed of a title which passes through different men, the first of which is played by an uncredited Val Kilmer.
 Paul Freeman), a previous friend of King Edgaars and an amputee following his own battle with the mother dragon, gives his son George a "dragon horn", which "sounds a note only a dragon can hear".

When George encounters the princess, he attempts to destroy the egg, but she knocks him unconscious each time he tries. In company with their companions, they transport the egg by wagon back to her father. Along the way they stop at a convent; Lunnas cousin is a nun there, and one of the friars is an old friend of George. The princess betrothed, Garth, catches up with them at the convent, and she says she will not marry him because she does not love him. Garth kidnaps her to force her to marry him; she is part of his plan to take over the kingdom.
 Moor who had been a close friend of George during the Crusade. El Cabillos men revolt against him, wishing to capture the Princess and claim the reward themselves. While they are fighting, the baby dragon hatches, the monk Elmendorf is killed saving the Princess from a flying spear, and King Edgaars men and Sir Roberts men arrive to join the fray. During the fight, Garth and George are forced to collaborate against a mutual enemy: the former second-in-command of El Cabillo, the leader of the mutiny. They fight him off together, but occasionally strike at each other. The confused melee is interrupted when the wall of the keep explodes. The mother dragon has returned.

The combatants flee. Debris prevents Georges escape. In the castle courtyard the other combatants listen in silence to the very loud noises of the unseen dragon inside the keep. Princess Lunna fears the worst for both George and the dragon.

Within the keep, the mother dragon is preoccupied with her child. George remains still to avoid detection by the dragon. George notices that a lance protrudes from the mother dragons side. It is his fathers lance. George slowly approaches the lance and takes hold. He asks Gods forgiveness for what he must do and promises to make this as painless as possible for the dragon.

A roar is heard by the listeners in the courtyard. George emerges from the keep with a bloodied lance. The men are overjoyed, believing that George has slain the dragon. Princess Lunna is not. Overcome with sorrow for the dragons death and angered by Georges betrayal, she flees on horseback. King Edgaar gives George his blessing to marry the princess, and George pursues her on the kings horse. As they race beside a large body of water, they are joined by Garth. Garth knocks George from his steed and they fight. Garth has the advantage, and raises his sword for the killing blow. The mother dragon leaps from the water and swallows Garth whole.

The Princess Lunna realizes that George did not kill the dragons.  They kiss and live happily ever after.

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| James Purefoy || George
|-
| Piper Perabo || Princess Lunna
|-
| Patrick Swayze || Garth
|-
| Michael Clarke Duncan || Tarik
|-
| Bill Treacher || Elmendorf
|-
| Jean-Pierre Castaldi || Bernard
|-
| Rollo Weeks || Wryn
|- Paul Freeman || Sir Robert
|-
| Stefan Jürgens || Bulchar
|-
| Phil McKee || McNally
|- Caroline Carver || Sister Angela
|-
| Joan Plowright || Mother Superior
|-
| Simon Callow || King Edgaar
|-
| Carl Chase || Arrd
|-
| Bill Oddie || Odo 
|}

==References==
 
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 