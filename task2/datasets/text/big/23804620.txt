The Four Troublesome Heads
{{Infobox film
| name           =  Un homme de têtes
| image          = Méliès, Un homme de têtes (Star Film 167 1898).jpg
| image_size     =
| caption        =
| director       = Georges Méliès
| producer       =
| writer         = Georges Méliès
| narrator       =
| starring       = Georges Méliès
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 20 meters/65 feet   
| country        = France
| language       = Silent
| budget         =
| website        =
}}

The Four Troublesome Heads ( ) is an 1898 French silent film directed by Georges Méliès. It was released by Mélièss company Star Film and is numbered 167 in its catalogues.  An illegal print of the film, copied without authorization from Méliès, was released in America in 1903 by Siegmund Lubin under the title Four Heads Are Better Than One.   

The film features the first known use of multiple exposure of objects on a black background on film, a special effect Méliès went on to use prolifically. Frazer, p. 70.  

== Summary ==
Georges Méliès enters the frame and stands between two tables. He removes his own head and puts it on one of the tables, where it starts talking and looking around. Méliès repeats the action twice, with a new head appearing on his shoulders each time, until four identical Méliès heads are presented at once. Méliès then plays a banjo, and all four heads sing along.

== References ==
 

== External links ==
*  
*   on YouTube

 
 
 
 
 
 

 
 