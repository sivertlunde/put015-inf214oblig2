The Atomic City
{{Infobox film
| name = The Atomic City
| image =Poster of The Atomic City.jpg
| caption =
| director = Jerry Hopper
| producer = Joseph Sistrom
| writer =  Sydney Boehm
| starring = Gene Barry Lydia Clarke
| music = Leith Stevens
| cinematography =  Charles Lang (as Charles B. Lang Jr)
| editing = Archie Marshek
| distributor = Paramount Pictures
| released = May 1, 1952
| runtime = 85 minutes
| country = United States
| language = English
| budget =
}}
The Atomic City is a 1952 drama film directed by Jerry Hopper, starring Gene Barry and Lydia Clarke.

The story takes place at Los Alamos, New Mexico, where a nuclear physicist (Barry) lives and works. Terrorists kidnap his son and demand that the physicist turn over the H-bomb formula.

The film was nominated for an Academy Award for Best Writing (Story and Screenplay), Sydney Boehm being the nominee.

==Plot== Los Alamos, Santa Fe for a carnival, where teacher Ellen Haskell cant find him when Tommys winning ticket in a raffle is announced.

The Addisons receive a telegram telling them Tommy has been kidnapped. The teacher also gets in touch about their boy being missing, but Frank, ordered to keep quiet, lies that he left work early and picked up his son.

Ellens boyfriend is an FBI agent, Russ Farley, and she passes along her concerns. Farley and partner Harold Mann begin tailing the Addisons. When a kidnapper instructs Frank to steal a file from the atomic lab and mail it to a Los Angeles hotel, he wants to inform the authorities, but Martha fears for their boy.

A small-time thief, David Rogers, picks up the file and takes it to a baseball game, followed by the FBIs agents and cameras. His car explodes, killing him, but Rogers first passed the file to someone at the game. FBI film spots a hot-dog vendor who is actually Donald Clark, a man with Communist ties.

Tommy is moved by kidnappers to the site of an Indian ruin in New Mexico, where they briefly encounter the Fentons, a family of tourists. The mastermind turns out to be Dr. Rassett, a physicist. He studies the file Addison mailed and determines it to be a fake. Rassett orders the boy killed, but Tommy has escaped and is hiding in a cave.

The son of the Fentons has the raffle ticket, which he found by the ruins. FBI agents rush to the site, where Rassett is arrested after killing his accomplices, and Tommy is saved.

==Cast==
As appearing in screen credits (main roles identified):   
{| class="wikitable" width="60%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Gene Barry || Dr. Frank Addison
|-
| Lydia Clarke || Martha Addison
|-
| Michael Moore || Russ Farley (Michael Moore, who had a brief U.S. career as an actor after being named to the Paramount Studios Golden Circle with Gene Barry (1951-2), and was "killed" at the beginning of Stalag 17, is not the child actor and director Michael D. Moore).
|-
| Nancy Gates || Ellen Haskell
|-
| Lee Aaker || Tommy Addison
|-
| Milburn Stone || Insp. Harold Mann
|-
| Bert Freed || Emil Jablons
|-
| Frank Cady || F.B.I. Agent George Weinberg
|-
| Houseley Stevenson Jr. || Greg Gregson
|- Leonard Strong || Donald Clark
|-
| Jerry Hausner || John Pattiz
|-
| John Damler || Dr. Peter Rassett
|- George Lynn ||  Robert Kalnick (as George M. Lynn)
|-
| Olan Soule || Mortie Fenton
|-
| Anthony Warde || Arnie Molter
|}

A full cast and production crew list is too lengthy to include, see: IMDb profile. 

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 