Blush (film)
{{Infobox Film 
  | name = Blush
  | image = 
  | caption =
  | director = Li Shaohong 
  | producer = Chen Kunming Yi Liu Jimmy Tan
  | writer =   Wang Ji He Saifei Wang Zhiwen
  | music = Guo Wenjing
  | cinematography = Zeng Nianping
  | editing = Zhou Xinxia
  | distributor = United States: First Run Features
  | studio = Beijing Film Studio
  | released =  
  | runtime = 115 minutes
  | country = China
  | language = Mandarin
  | budget = 
}}

Blush ( ) is a 1995 Chinese film about the experience of two women during the  .   

Blush won the Silver Bear for Outstanding Single Achievement at the 45th Berlin International Film Festival in 1995. 

== Plot ==
Blush takes place in the 1950s during a campaign by the new Communist government in China designed to "re-educate" prostitutes to become contributing members of society. Two such prostitutes, Xiaoe (He Saifei) and Qiuyi (Wang Ji), have recently been sent to a re-education camp by the Peoples Liberation Army. Rebelling against her new life of uniforms and forced re-education, Qiuyi escapes and becomes a kept woman for Laopu (Wang Zhiwen) When Qiuyi becomes pregnant, she seeks refuge in a Buddhist temple but is cast out when the nuns discover her pregnancy - soon after, the baby miscarriage|miscarries. 

Left in the training camp, Xiaoe undergoes her ideological re-education and emerges from her ordeal as a factory worker. Detesting physical labor, she goes on to marry Laopu and has a child with him, forcing him to steal money from work. When Laopu is caught, he is sentenced to death, and Xiaoe abandons him and her child to remarry. As the film ends, XiaoEs child is adopted by Qiuyi, who had gone on to marry a simple old teahouse owner.

== Reception ==
Blush was well received by most critics in the West, with Jonathan Rosenbaum of the Chicago Reader calling it the "most emotionally complex picture Ive seen from mainland China about the effect of the communist revolution on the lives of ordinary people." 

== References ==
 

== External links ==
*  
*  
*   at the Chinese Movie Database

 
 
 
 
 
 
 