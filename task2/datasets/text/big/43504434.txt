Cuori nella tormenta
{{Infobox film
 | name = Cuori nella tormenta
 | image = Cuori nella tormenta.jpg
 | caption =
 | director =Enrico Oldoini
 | writer =  Furio Scarpelli  Carlo Verdone  Ettore Scola Enrico Oldoini 
 | starring =  Carlo Verdone Lello Arena Marina Suma
 | music =Manuel De Sica  Fiorenzo Carpi
 | cinematography =  Alessandro DEva 
 | editing =  Alberto Gallitti 
| released = 1984
 | country = Italy
 | runtime = 103 min
 | language = Italian 
 }} 1984 Italian romantic comedy film. It marked the directorial debut of Enrico Oldoini.    

== Plot ==
Walter is a non-commissioned officer of the Navy, while Raf is a ships cook; they become great friends but unfortunately they fall in love, without knowing it, with the same girl, Sonia, who clearly prefers the most brilliant Walter. The girl does not have the courage to reveal it to Raf, who believes instead that she loves him to death.

== Cast ==

*Carlo Verdone: Walter Migliorini 
*Lello Arena: Raffaele aka "Raf"
*Marina Suma: Sonia 
*Rossana Di Lorenzo:  Walters Mother

==References==
 

==External links==
* 

 
 
 
 
 


 
 