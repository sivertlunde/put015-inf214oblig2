The Recourse to the Method
 
{{Infobox film
| name           = The Recourse to the Method
| image          = 
| caption        = 
| director       = Miguel Littín
| producer       = Héctor López (producer)|Héctor López Michèle Ray-Gavras Vicente Silva
| writer         = Alejo Carpentier Régis Debray Miguel Littín
| starring       = Nelson Villagra
| music          = 
| cinematography = Ricardo Aronovich
| editing        = Ramón Aupart
| distributor    = 
| released       =  
| runtime        = 164 minutes
| country        = Mexico, Cuba
| language       = Spanish
| budget         = 
}}
 Best Foreign Language Film at the 51st Academy Awards, but was not accepted as a nominee. 

==Cast==
* Nelson Villagra - El Primer Magistrado
* Ernesto Gómez Cruz - Cholo Salvador Sánchez - Peralta
* Reynaldo Miravalles - Oberst Hoffmann
* Raúl Pomares - General Galván
* Katy Jurado - La Mayorala
* Alain Cuny - El Académico
* Gabriel Retes - El Estudiante
* María Adelina Vera - La Hija del Presidente
* Roger Cudney - Army captain
* Didier Flamand
* Denis Perrot
* Monique Perrot
* Jacques Rispal
* Idelfonso Tamayo - Miguel Estatua

==See also==
* List of submissions to the 51st Academy Awards for Best Foreign Language Film
* List of Cuban submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 