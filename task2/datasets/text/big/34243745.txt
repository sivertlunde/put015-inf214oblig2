Aggiramudu
{{Infobox film
| name           = Aggiramudu
| image          =
| caption        =
| director       = S.S. Ravichandra
| producer       = M.Krishna & Shekar
| writer         = Paruchuri Brothers
| screenplay     = Venkatesh Gouthami Amala
| Chakravarthy
| cinematography = S. Gopal Reddy
| editing        = Kotagiri Venkateswara Rao
| studio         = Sri Raghavendra Pictures
| distributor    =
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Tollywood film Amala played the lead roles and music composed by K. Chakravarthy|Chakravarthy.  The film was flop at the box office. 

==Cast==
  Venkatesh as Aggi Ramudu & Vijay (Duelrole)
*Gouthami as Malli Amala as Manasa Sharada as Justice Janaki Devi Satyanarayana as Sarpabhushan Rao Allu Ramalingaiyah as Tatarao
*Kota Srinivasa Rao as Mental Doctor
*Babu Mohan as Bhuvagiri Babji
*Sarath Babu as Ram Mohan
*Narra Venkateswara Rao as Inspector
*Chalapathi Rao as Phanibhushan Rao
*Pradeep Shakti as Sasibhushan Rao
*Bhimeswara Rao as Bank Manager Jayalalitha as Saroja Hema as Manasas friend 
*Nirmalamma
 

==Soundtrack==
{{Infobox album
| Name        = Aggiramudu 
| Tagline     = 
| Type        = soundtrack Chakravarthy
| Cover       = 
| Released    = 1989
| Recorded    = 
| Genre       = Soundtrack
| Length      = 21:34
| Label       = Cauvery Audio Chakravarthy
| Reviews     =
| Last album  = Ayudham   (1990)
| This album  = Aggiramudu   (1990) Lorry Driver   (1990)
}}

Music Composed by K. Chakravarthy|Chakravarthy. Music released on Cauvery Audio Company.

{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 21:34
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Thagilithe Kopam 
| lyrics1 = Veturi Sundararama Murthy SP Balu, S. Janaki
| length1 = 4:00

| title2  = Haayile Haayile Sirivennela Sitarama Sastry
| extra2  = Mano (singer)|Mano, S. Janaki
| length2 = 4:50

| title3  = Mallesha 
| lyrics3 = Veturi Sundararama Murthy
| extra3  = SP Balu,S. Janaki
| length3 = 4:36

| title4  = Savaalu Chestaava 
| lyrics4 = Sirivennela Sitarama Sastry
| extra4  = Mano,S. Janaki
| length4 = 4:00

| title5  = Srungaara Thailaala 
| lyrics5 = Veturi Sundararama Murthy 
| extra5  = SP Balu,S. Janaki
| length5 = 4:08
}}

== References ==
 

==External links==
* 

 
 
 
 


 