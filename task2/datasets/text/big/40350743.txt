Shudhu Tumi
 
{{Infobox film
| name           = Shudhu Tumi
| image          = Shudhu Tumi poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Abhijit Guha Sudeshna Roy
| producer       = Sagar Bhora Siddhartha Bhora
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Prosenjit Chatterjee Koel Mallick
| music          = Zubeen Garg
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2004
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali film directed by Abhijit Guha and Sudeshna Roy, produced by Sagar Bhora and Siddhartha Bhora, and starring Prosenjit Chatterjee and Koel Mallick in the lead roles. Zubeen Garg composed the music.   

== Cast ==
* Prosenjit Chatterjee
* Koel Mallick
* Kalyani Mondal
* Arijit Chowdhury
* Debnath Chattopadhyay
* Dwijen Bandyopadhyay
* Gargi Raychowdhury
* Joyjit Banerjee
* Parthasarathi Deb
* Payel Sarkar

== Soundtrack ==
{{Infobox album  
| Name       = Shudhu Tumi
| Type       = Soundtrack
| Artist     = Zubeen Garg
| Cover      = 
| Alt        = 
| Released   = 2004
| Recorded   =  Feature film soundtrack
| Length     =  
| Label      = Fontana India
| Producer   = 
| Last album = 
| This album = Shudhu Tumi (2004)
| Next album = Mon Niye (2010)
}}

Music composed by Zubeen Garg. 

=== Track listing ===
{{Track listing
| collapsed       = 
| headline        = 
| extra_column    = Singer(s)
| total_length    = 36:52
| writing_credits = no
| lyrics_credits  = no
| music_credits   = no
| title1          = Gun Gun Gun Gunjare, Antare
| extra1          = Shreya Ghoshal
| length1         = 4:32
| title2          = Shure Shure Gaan Holo
| extra2          = Shaan (singer)|Shaan, Shaswati
| length2         = 4:06
| title3          = Piriti Kathaler Aatha
| extra3          = Joy, Zubeen Garg, Shaswati
| length3         = 4:56
| title4          = E Chokher Kachhete
| extra4          = Shaan (singer)|Shaan, Hilsa, Pritha Majumder
| length4         = 5:27
| title5          = Ektuku Chhoan Lage
| extra5          = Babul Supriyo, Shreya Ghoshal
| length5         = 4:28
| title6          = Bheja Bheja Smritir Pathor
| extra6          = Zubeen Garg
| length6         = 4:35
| title7          = Sajbe Ebar Koner Saaje
| extra7          = Udit Narayan, Pamela Jain, Ritika Sahni
| length7         = 4:17
| title8          = Ektuku Chhoan Lage
| extra8          = Zubeen Garg
| length8         = 4:31
}}

== References ==
 
* 

 
 