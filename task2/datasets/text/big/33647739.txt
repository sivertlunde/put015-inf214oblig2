Schwarzwaldmelodie
{{Infobox film
| name           = Schwarzwaldmelodie
| image          = 
| image_size     = 
| caption        = 
| director       = Géza von Bolváry
| producer       = Kurt Ulrich (producer)
| writer         = Werner P. Zibaso
| narrator       = 
| starring       = See below Gerhard Winkler Kurt Schulz
| editing        = Ingrid Wacker
| studio         = 
| distributor    = 
| released       = 1956
| runtime        = 100 minutes
| country        = West Germany
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Schwarzwaldmelodie is a 1956 West German film directed by Géza von Bolváry. The film was produced in de Ufa-Studios Berlin and in the Circus Roland.

== Plot summary ==
 

== Cast ==
*Erica Beer as Harriet Morton
*Claus Biederstaedt as Hans Homann
*Siegfried Breuer Jr. as Fredy
*Erich Fiedler
*Gerd Frickhöffer
*Willy Fritsch as Herbert Olberg
*Walter Giller as Luggi
*Gardy Granass as Susanne
*Carla Hagen as Kuni
*Hans Leibelt as Mr. Morton
*Maria Leininger
*Ralph Lothar as Albert
*Kurt Reimann as Lerche Hans Richter as Aribert
*Marina Ried as Uschi
*Alexa von Porembsky Fritz Wagner as Stallmeister
*Herbert Weissbach
*Carl Wery as Stettner
*Trude Wilke-Roßwog
*Kurt Zehe as Balthasar

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 


 
 