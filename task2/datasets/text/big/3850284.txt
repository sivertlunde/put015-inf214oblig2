The Guinea Pig (film)
 
{{Infobox Film
| name           = The Guinea Pig
| image          = "The_Guinea_Pig"_(1948).jpg
| director       = Roy Boulting
| producer       = John Boulting
| writer         = Roy Boulting Warren Chetham-Strode  (play)  Bernard Miles
| starring       = Richard Attenborough
| music          = John Wooldridge
| cinematography = Gilbert Taylor Richard Best
| studio         = Boulting Brothers Pilgrim Pictures
| distributor    = Pathé Pictures International (UK)
| released       = 27 October 1948 (UK)
| runtime        = 97 min.
| country        = United Kingdom
| language       = English
| gross = £224,694 (UK) 
}} British film play of the same name by Warren Chetham-Strode. 

==Plot summary==
 public school. The school used in the film was Sherborne School in Dorset.

Only after the changes wrought by World War II could such a scenario be imagined. Of course, Reads uncouth behaviour causes him difficulties in fitting in to the school. The film was controversial as it contains the first screen use of the word "Buttocks|arse". 

==Cast==
*Richard Attenborough as Jack Read
*Sheila Sim as Lynne Hartley
*Bernard Miles as Mr. Read
*Cecil Trouncer as Lloyd Hartley
*Robert Flemyng as Nigel Lorraine
*Edith Sharpe as Mrs. Hartley
*Joan Hickson as Mrs. Read
*Timothy Bateson as Tracey Herbert Lomas as Sir James Corfield
*Anthony Newley as Miles Minor Anthony Nicholls as Mr. Stringer
*Wally Patch as Uncle Percy
*Hay Petrie as Peck
*Oscar Quitak as David Tracey
*Kynaston Reeves as The Bishop
*Olive Sloane as Aunt Mabel Peter Reynolds as Grimmett

==Critical reception==
*The New York Times wrote, "the details are highly parochial, the attitudes of the characters are strangely stiff, the accents and idioms are hard to fathom—and the exposition is involved and tedious."   Time Out called it, "solid entertainment, even if barely convincing". 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 

 