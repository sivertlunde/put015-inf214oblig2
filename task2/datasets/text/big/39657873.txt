The Adventure of Mr. Philip Collins
{{Infobox film
| name           = The Adventure of Mr. Philip Collins
| image          = 
| image_size     = 
| caption        = 
| director       = Johannes Guter 
| producer       = Erich Pommer
| writer         = Frank Heller (novel)   Robert Liebmann
| narrator       = 
| starring       = Georg Alexander   Ossi Oswalda   Elisabeth Pinajeff   Alexander Murski
| music          = 
| editing        =
| cinematography = Günther Krampf UFA
| UFA
| released       = 1925
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent comedy The Tower of Silence. 

The films art direction was by Rudi Feld.

==Cast==
* Georg Alexander as Filip Collin 
* Ossi Oswalda as Daisy Cuffler 
* Elisabeth Pinajeff as Alice Walters 
* Adolf E. Licho as President Cuffler, Daisys Vater  
* Alexander Murski as Reeder John Walters, ALices Onkel 
* Erich Kaiser-Titz as Austin Bateson 
* Paul Biensfeldt as Austins Bruder 
* Karl Victor Plagge    Hans Junkermann   
* Karl Platen

==References==
 

==Bibliography==
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918-1945. University of California Press, 1999.

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 