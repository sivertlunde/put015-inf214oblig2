Chekavar (film)
{{Infobox film
| name = Chekavar
| image = Chekavar.in-wall_11.jpg
| alt =  
| caption =
| director = Sajeevann
| producer = Pentagon Creations
| writer = Sajeevann Indrajith Kalabhavan Sarayu Samvrutha Sunil
| music = Rahul Raj
| cinematography = Sujith Vasudev
| editing =
| studio =
| distributor =
| released =  
| runtime =
| country = India
| language = Malayalam
| budget =
| gross =
}} Sarayu and Rahul Raj with lyrics penned by Anil Panachooran. The film released on 8 October 2010. Indrajith plays the role of a police officer while Mani plays an underworld don. The story progresses as both of them fight each other, for their siblings.

==Cast== Indrajith .... Kashinathan
* Samvrutha Sunil .... Jyoti
* Kalabhavan Mani .... Garudan Raghavan Sarayu .... Gauri
* Jagathy Sreekumar
* Suraj Venjaramood
* Bijukuttan
* Sreejith Ravi
* Chali Pala 
* Kalashala Babu
* Krishnaprasad
* Subair
* Unni Shivapal
* Punnapra Prashanth (Baiju) Janardhanan
* Ajith Kollam
* Saju Kodiyan
*T. P. Madhavan
* Majeed Edavanakkadu
* Baiju Ezhupunna
* Raheem
* Kaimal
* Lakshmipriya as Kasinathans sister
* Jannet James
* Deepa Nair
* Ponnamma Babu
* Sreelakshmi Nair
* Ambika Mohan ... Jyotis mother
* Deepika Mohan

==Soundtrack==
{{Infobox album
| Name = Chekavar
| Type = Soundtrack
| Artist = Rahul Raj
| Cover =
| Background =
| Released = 2010
| Recorded =
| Genre = Soundtrack
| Length =
| Label = Tune 4 Records
| Producer =
| Reviews =
| Last album = Ritu (film)|Ritu (2009)
| This album = Chekavar (2010)
| Next album = Oh My Friend (2011)
}} Tune4 Records. The audio was launched on August 19, in a function held at The Wyte Fort Hotel, Cochin, in the presence of actor Prithviraj Sukumaran|Prithviraj. The audio recorded excellent sales on the first day, and has received generally positive reviews from both critics and Rahul Raj fans.  The Audio contains a total of thirteen tracks, out of which 3 are songs, and the rest are extracts from the background score of the film. This is the first time in the Malayalam industry, extracts from the score are being released as a motion picture soundtrack.

{{Track listing
| extra_column = Performer(s)
| title1 = Poonchillayil | extra1 = Vijay Yesudas, KS Chithra
| title2 = Poril Theyyaram | extra2 = Indrajith, Ragged Skull
| title3 = 24 Hours | extra3 = Rahul Raj
| title4 = Gauri: The Village Girl | extra4 = Rahul Raj, Gayatri Suresh, Josy
| title5 = Heart of God | extra5 = Rahul Raj, Chorus
| title6 = In Search of Love | extra6 = Rahul Raj, Sangeetha Prabhu, Kamalakar, Josy
| title7 = Metal Pooram | extra7 = Rahul Raj, Ragged Skull
| title8 = The Final Invasion | extra8 = Rahul Raj, Chorus
| title9 = Love | extra9 = Rahul Raj, Josy
| title10 = Helpless | extra10 = Rahul Raj, Josy
| title11 = Forces of the Dark | extra11 = Rahul Raj
| title12 = Poonchillayil | note12 = Karaoke
| title13 = Poril Theyyaram | note13 = Karaoke
}}

==Production==
The Pooja of the film was held in February 2010 and the shooting began in Kerala at the end of February 2010. 

==References==
 

==External links==
*  

 
 
 
 