Shun Li and the Poet
{{Infobox film
| name           = Shun Li and the Poet
| image          = 
| caption        = 
| director       = Andrea Segre
| producer       = 
| writer         = Andrea Segre
| starring       = Zhao Tao Rade Šerbedžija Marco Paolini
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Italy
| language       = Italian and Mandarin
| budget         = 
| gross          = 
}}
Shun Li and the Poet ( ) is a 2011 Italian drama film directed by Andrea Segre.  It won the European Parliament Lux Prize in 2012. Zhao Tao won the 2012 David di Donatello for Best Actress for her performance as Shun Li. It also took out the 2014 Whitehead Award at the 13th annual international film festival.  

== Plot ==
Shun Li (Zhao Tao) has been working in a textile workshop in the outskirts of Rome for eight years trying to obtain the documents to bring her son to Italy. She is suddenly transferred to Chioggia, a small island town situated in the Venetian lagoon to work as a bartender in a tavern. Bepi (Rade Šerbedžija), a fisherman of Slavic origin, nicknamed by friends as "the Poet", has been visiting the little inn for years. Their meeting is a poetic escape from loneliness, a silent dialogue between different cultures. It is a journey into the heart of a deep lagoon, known to be a mother and birthplace of identity. But the friendship between Shun Li and Bepi disturbs both communities, Chinese and Chioggia, which hinders this new journey, which perhaps is simply still too scary.

== Cast ==
*Zhao Tao: Shun Li
*Rade Šerbedžija: Bepi
*Marco Paolini: Coppe
*Roberto Citran: Avvocato
*Giuseppe Battiston: Devis

==References==
 

== See also ==
* Movies about immigration to Italy

== External links ==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 


 
 