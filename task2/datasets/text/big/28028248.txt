The House That Drips Blood on Alex
{{Infobox film
| name           = The House That Drips Blood on Alex
| image          = 
| alt            =  
| caption        = 
| director       = Brock LaBorde Jared Richard
| producer       = Brock LaBorde
| writer         = Brock LaBorde Jared Richard Justine Ezarik Brooke Brodack Rory Windhorst Arlando Smith Chris Trew Brock LaBorde
| music          = David McKeever
| cinematography = Yaque Silva-Doyle
| editing        = Jared Richard
| studio         = 
| distributor    = Atom.com
| released       =  
| runtime        = 13 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 short starring Tommy Wiseau.  The film was written by sketch comedy group Studio8.  It first aired October 14, 2010 on Comedy Central   and was released online at atom.com. It was also included as an extra on the DVD compilation of The Tommy Wi-Show, a web series also created by Studio8 and starring Wiseau.

The title is an homage to the Amicus film The House That Dripped Blood.

==Plot==
In a movie theater, a mysterious stranger sits next to two girls and starts to tell them a scary story that he claims is true:

Alex moves into an old house at 3 Blood Street. He is excited about moving in, until his friend Thomas points out that the house is dripping blood onto him. Alex is nonchalant about this, saying that it is just the houses paint job, although Thomas observes that nothing is painted this color. They ignore this, but after more blood drips on Alex later, the pair agree to approach the landlord to report the "leak".

The next day, they discover that the landlords office is vacant, appearing to have not been used in years. Alex finds this odd, saying that the office was open yesterday. Alex hopes the landlord will call him and explain the dripping blood, but starts to worry as the problem continues.

Thomas explains the whole story to his friend Bobby the next day. When Alex meets up with them, Thomas offers Alex a place to stay until the problem stops. Alex declines, saying that he is not going to let some "leak" make him move. Later, Alex goes up to the attic to investigate the source of the blood and discovers his own dead body on the attic floor.

Back at the theater, the stranger finishes his story. The previews start, and the first trailer is for a horror film called "3 Blood Street" that has the same plot as the strangers story. When the girls look at the stranger, he has become a skeleton, causing the girls to scream.

==Cast==
* Tommy Wiseau as Alex and Mysterious Stranger
* Joey Greco as Landlord Justine Ezarik as Melissa
* Brooke Brodack as Girl 1
* Rory Windhorst as Thomas
* Arlando Smith as Bobby
* Brock LaBorde as Landlord 2
* Chris Trew as Alan

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 