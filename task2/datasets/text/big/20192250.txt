Jungle Jim (film)
{{Infobox film
| name           = Jungle Jim
| image          = Jungle_jim_poster.jpg
| image size     = 
| alt            = 
| caption        = Film Poster
| director       = William Berke
| producer       = Sam Katzman
| writer         = Carroll Young
| screenplay     = 
| story          = Carroll Young
| narrator       = 
| starring       = Johnny Weissmuller Virginia Grey George Reeves
| music          = Mischa Bakaleinikoff
| cinematography = Lester White
| editing        = Aaron Stell
| studio         = Esskay Pictures Corporation
| distributor    = Columbia Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

Jungle Jim is a black and white 1948 Adventure film directed by William Berke and written by Carroll Young, was based on Alex Raymonds Jungle Jim comic strip and distributed by Columbia Pictures. This was the first picture in the "Jungle Jim" series that consisted of sixteen films spanning a period from 1948 to 1955. In 1954, Columbia turned over its Jungle Jim rights to its television subsidiary, and in the last three films of the series Weissmullers character is named "Johnny Weissmuller" instead of Jungle Jim. Devil Goddess (1955) was the last entry in the series, as well as being Weissmullers last feature film. 

==Plot summary==
This is the first adventure of Jungle Jim, an African guide and hunter.
 

==Main cast==
* Johnny Weissmuller – Jungle Jim
* Virginia Grey – Dr. Hilary Parker
* Rick Vallin – Kolu
* George Reeves – Bruce Edwards
* Lita Baron – Zia
* Holmes Herbert – Commissioner Geoffrey Marsden
* Tex Mooney – Chief Devil Doctor
* Steve Calvert – Gorilla
* Al Kikume – Native Bearer / Tribesman

==References==
 	

==See also==
* Jungle Jim (disambiguation)
* Jungle Jim (serial)

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 

 