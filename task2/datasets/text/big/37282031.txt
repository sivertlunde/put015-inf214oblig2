Siti Akbari
{{Infobox film
| name           = Siti Akbari
| image          = Poster siti akbari.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical poster Joshua and Othniel Wong
| producer       = Tan Khoen Yauw
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{plainlist|
*Roekiah
*Rd Mochtar
*Kartolo
*Annie Landouw
*Titing
}}
| music          = 
| cinematography = Joshua and Othniel Wong
| editing        = 
| studio         = Tans Film
| distributor    = 
| released       =  
| runtime        = 
| country        = Dutch East Indies Malay
| budget         = 
| gross          = 
}} Joshua and Othniel Wong and produced by Tan Khoen Yauw. Starring Roekiah and Rd Mochtar, it follows a couple while the husband commits adultery.

==Plot==
Siti Akbari (Roekiah) is living happily with her husband. When he begins to wander she stays faithful and he eventually comes back to her. 

==Production== Joshua and Othniel Wong,  ethnic Chinese brothers who had received film training in the United States and been active in the film industry of the Dutch East Indies since 1929s Lily van Java.  The films producer, Tan Khoen Yauw, was co-owner of the production studio that made the work, Tans Film. 

The black-and-white talkie featured vocals by the actresses Annie Landouw and Titing, with the keroncong group Lief Java providing background music.  Several further cast members, including the stars Mochtar, Roekiah, and her husband Kartolo, had migrated to Tans after the success of Albert Balinks Terang Boelan (1937); the earlier films formula of beautiful scenery, music, and action, which Tans had already utilised in 1938s Fatima (1938 film)|Fatima, was present in Siti Akbari as well. 

The title Siti Akbari is reminiscent of a syair, or traditional Malay poem, written by Lie Kim Hok in 1884;   his Sair Tjerita Siti Akbari had previously been adapted for the stage.  However, the influence Lims work exerted on the film is unknown: it may have simply lent the title, or it may have been adapted in its entirety. 

==Release== Rd Djoemala for Roekihati in 1940. 

The production is likely a lost film. The American visual anthropologist Karl G. Heider writes that all Indonesian films from before 1950 are lost.  However, JB Kristantos Katalog Film Indonesia (Indonesian Film Catalogue) records several as having survived at Sinematek Indonesias archives, and Biran writes that several Japanese propaganda films have survived at the Netherlands Government Information Service. 

==References==
Footnotes
 

Bibliography
 
*{{cite book
 |title= 
 |trans_title=History of Film 1900–1950: Making Films in Java
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |author-link=Misbach Yusa Biran
 |publisher=Komunitas Bamboo working with the Jakarta Art Council
 |year=2009
 |isbn=978-979-3731-58-2
 |ref=harv
}}
*{{cite news
 |title=Cinema: Siti Akbari
 |language=Dutch
 |newspaper=Bataviaasch Nieuwsblad
 |location=Batavia
 |date=1 May 1940
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011123090%3Ampeg21%3Ap003%3Aa0057
 |ref= 
 |publisher=Kolff & Co.
}}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
*{{cite web
 |title=Othniel Wong
 |url=http://filmindonesia.or.id/movie/name/nmp4b99bd523f3ef_Othniel-Wong
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=29 August 2012
 |archiveurl=http://www.webcitation.org/6AHIOJqsm
 |archivedate=29 August 2012
 |ref= 
}}
* {{cite web
  | title = Siti Akbari
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-s010-39-092079_siti-akbari
  | work = filmindonesia.or.id
  | publisher = Konfidan Foundation
  | location = Jakarta
  | accessdate = 24 July 2012
  | archiveurl = http://www.webcitation.org/69ORNyFQG
  | archivedate = 24 July 2012
  | ref =  
  }}
* {{cite web
  | title = Siti Akbari, Sair Cerita
  | language = Indonesian
  | url = http://www.jakarta.go.id/jakv1/encyclopedia/detail/2900
  | work = Encyclopedia of Jakarta
  | publisher = Jakarta City Government
  | location = Jakarta
  | accessdate = 10 October 2012
  | archiveurl = http://www.webcitation.org/6BJ7eDI0N
  | archivedate = 10 October 2012
  | ref =  
  }}
 

==External links==
* 
 
 

 
 