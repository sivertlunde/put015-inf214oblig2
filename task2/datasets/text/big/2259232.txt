Solarbabies
{{Infobox film
| name        = Solarbabies
| image       = solarbabies.jpg
| caption     = DVD box cover Alan Johnson
| producer    = Jack Frost Sanders Irene Walzer
| writer      = Walon Green Douglas Anthony Metrov
| starring    = Richard Jordan Jami Gertz Jason Patric Lukas Haas Peter DeLuise Charles Durning Sarah Douglas Bruce Payne Alexei Sayle
| music       = Maurice Jarre Peter MacDonald
| editing     = Conrad Buff
| distributor = Metro-Goldwyn-Mayer
| released    =  
| runtime     = 94 min.
| country     = United States
| language    = English
| budget      = $7,500,000
| gross       = $1,579,260
| awards      =
}} Brooksfilms and Alan Johnson. {{cite news
 | first=Vincent | last=Canby | date=November 26, 1986
 | title=Screen: Solarbabies | work=The New York Times
 | url=http://www.nytimes.com/1986/11/26/movies/screen-solarbabies.html | accessdate=2009-11-06 }}  It was released on DVD on March 6, 2007. 

The movie was the second and final film directed by Alan Johnson, who is better known for his work as a choreographer.

== Synopsis ==
 apocalyptic future, most of Earths water has been controlled. The Eco Protectorate, a para-military organization, governs the planets new order. Orphan children, mostly teenagers, live in orphanages created by the Protectorate, designed to indoctrinate new recruits into their service.  The orphans play a rough sport which is a hybrid of lacrosse and roller-hockey. Playing is the only thing that unites them other than the futile attempts of the Protectorate to control them. These orphans are Jason, the groups leader (Jason Patric), Terra (Jami Gertz), Tug ( Peter DeLuise), Rabbit (Claude Brooks), Metron (James LeGros), and a young deaf boy named Daniel (Lukas Haas).

While hiding in a cave, Daniel finds a mysterious orb with special powers.  The orb is an alien intelligence called Bohdai, who miraculously restores Daniels hearing and has other powers, such as creating rain indoors. Another orphan, Darstar (Adrian Pasdar), takes the orb, hoping that he will be able to use it. He leaves the orphanage on rollerskates and Daniel soon follows. The rest of the group chase after Daniel. The E-police learn of Bohdai while chasing the teens and catch Darstar with the sphere. The teens are eventually rescued by a band of older outlaws called the Eco Warriors. They have retired from fighting and are led by Terras long-lost father. The teens leave the Eco Warriors and using their rollerskating skills, break into the Protectorates high security Water Storage Building. The teens discover the E-Police are trying to destroy Bohdai and they manage to recover the alien, but as soon as they do the sphere dematerializes and destroys the facility, releasing the water back to where it belongs as they rush out.  As they all gather on a nearby hillside, Bohdai sparks the first thunderstorm the teens have ever seen and returns to space, but not without leaving a bit of himself behind in each of them. 

Ultimately, the orphans are seen swimming together in the newly restored ocean, Darstar being fully accepted into the group and Jason and Terra sharing a kiss.

==Cast==

*Richard Jordan as Grock
*Jami Gertz as Terra
*Jason Patric as Jason
*Lukas Haas as Daniel
*James LeGros as Metron
*Claude Brooks as Rabbit
*Peter DeLuise as Tug
*Peter Kowanko as Gavial
*Adrian Pasdar as Darstar
*Sarah Douglas as Shandray
*Charles Durning as The Warden
*Frank Converse as Greentree
*Terrence Mann as Ivor
*Alexei Sayle as Malice
*Bruce Payne as Dogger

==Critical response==
Reviews for Solarbabies were very poor, with film historian Leonard Maltin describing it thus: "An appalling stinker; the 1980s teen jargon doesnt exactly capture the futuristic mood of this junk."

  rip-off, working from a script which must have been scrawled in Crayola, with every futuristic cliche you could possibly imagine. Lacking in originality, but rich in brain-dead dialogue; when Jami Gertz snarls, Get out, you creature of filth!, consider that a subliminal message."

Mike Clark, reviewing the film for  ), and added "...we see in a couple of scenes that movies still exist. Id have thought both civilization and the movies would have been wiped out by 41  , thanks to atrocities like Solarbabies."

Gene Siskel, on his syndicated film review show Siskel & Ebert, called the film "trash." 

The film presently holds a score of 4.8 on Internet Movie Database.

==See also==
* List of American films of 1986

==References==
 

==External links==
*   at MGM.com
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 