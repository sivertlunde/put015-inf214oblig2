Naalvar
{{Infobox film
| name           = Naalvar நால்வர்
| image          = Naalvar.jpg
| image size     = 
| caption        = 
| director       = V. Krishnan
| producer       = M. A. Venu
| writer         = A. P. Nagarajan
| narrator       =
| starring       =  
| music          = K. V. Mahadevan
| cinematography =
| editing        =
| distributor    = Sangeetha Pictures
| released       = 5 November 1953
| runtime        = 
| country        = India Tamil
| budget         =
| preceded by    =
| followed by    =
}} Tamil Family family drama film directed by V. Krishnan. The film stars A. P. Nagarajan, Kumari Thangam, N. N. Kannappa and Muthulakshmi in major roles. A. P. Nagarajan who played the main lead was also the writer of the film.    The film revolves around a family consisting of four siblings.

==Plot==
A. P. Nagarajan, a sincere cop is the eldest of four siblings in a family whose father works as a secretary to a mill owner. The second of the four siblings is a lawyer, third is employed in the same mill where their father works as a supervisor, and the youngest of all is a social activist. Nagarajan happens to meet Kumari Thangam, not knowing the fact that she is the daughter of his fathers boss charges her for an offence. He is very sincere in his duty to such an extent that he even arrests his own brother when the latter commits a crime, and even takes the blame when his fathers enemies try to make that his father was involved in smuggling of gold and printing of forfeited currency notes, in order to protect his father and the image of the family.

==Cast==
* A. P. Nagarajan Malayalam
* N. N. Kannappa
* M. N. Krishnan
* S. R. Janaki
* R. Balasubramaniam
* C. R. Vijayakumari
* T. P. Muthulakshmi
* V. M. Ezhumalai
* E. R. Sahadevan
* S. S. Majid
* Ratnakumari
* N. S. Narayana Pillai
* V. N. Natesan
* A. R. Damodaram
* T. K. Swaminathan
* V. R. Natarajan
* T. V. Satyamurthi
* Gauthamdas
* K. N. Kanakasabai

==Production==
Naalvar was shot at the Central Studios, Coimbatore and was produced by M. A. Venu under the banner Sangeetha Pictures. V. Krishnan was the director and the script was provided by A. P. Nagarajan.  Nagarajan later became a successful film-maker as he went on to direct many successful films such as Thillana Mohanambal, Thiruvilayadal (1965 film)|Thiruvilayadal, Kandan Karunai and Thirumal Perumai in Tamil cinema within a span of two decades. These films earned a cult status in Tamil Nadu.  Thillana Mohanambal and Thiruvilayadal won National Film Award for Best Feature Film in Tamil  and Certificate of Merit for the Third Best Feature Film in Tamil respectively.  Vijayakumari, who later became a popular actor in her own right in Tamil films, played a small role in the film. 

The background and soundtrack was provided by K. V. Mahadevan. Nagarajan also lent his voice for a song in the film along with the composer. The film fared well at the box-office and Nagarajan came to be known as "Naalvar Nagarajan". 

==See also==
*  
* Tamil films of 1953

==References==
 

==Further reading==
* {{cite book
 | last = Sundararaj
 | first =  Theodore Baskaran
 | title = The eye of the serpent: an introduction to Tamil cinema
 | publisher = East West Books
 | location = Madras
 | year = 1996}}
* {{cite book
 | last = M. S. S.
 | first =  Pandian
 | title = The image trap: M.G. Ramachandran in film and politics
 | publisher = Sage
 | year = 1992
 | isbn = 9780803994034}}
* {{cite book
 | last1 = Rajadhyaksha
 | first1 = Ashish
 | last2 = Willemen
 | first2 = Paul
 | title = Encyclopaedia of Indian cinema
 | publisher = British Film Institute
 | year = 1999}}
 

 
 
 
 
 