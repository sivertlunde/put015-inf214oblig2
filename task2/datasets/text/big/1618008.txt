Dark Water (2002 film)
{{Infobox film
| name = Dark Water   仄暗い水の底から
| image = Darkwater2002movieposter.jpg
| alt =  
| caption = English-language DVD art
| director = Hideo Nakata Taka Ichise
| writer = Hideo Nakata   Takashige Ichise   Yoshihiro Nakamura
| starring = Hitomi Kuroki   Rio Kanno   Mirei Oguchi   Fumiyo Kohinata   Asami Mizukawa
| music = Kenji Kawai Shikao Suga
| cinematography =
| editing =
| studio = Toho Company Ltd.
| released =  
| runtime = 101 minutes
| country = Japan
| language = Japanese
| budget = $4,000,000  . tohokingdom. Retrieved 2011-12-24. 
| gross = $3,565,363     
}}

Dark Water (  directed by Hideo Nakata, the director of Ring (film)|Ring and Ring 2. Dark Water is based on Dark Water (book)|Floating Water, a short story by Koji Suzuki. The plot follows a divorced mother who moves into a rundown apartment with her daughter, only to experience supernatural occurrences and a mysterious water leak from the floor above which is eventually traced back to the former tenants. Released in Japan in January 2002, the film went on to premiere at festivals in Europe and the United States.
 remade in 2005 under the same title, directed by Walter Salles and starring Jennifer Connelly.

== Plot ==
Yoshimi Matsubara (松原 淑美 Matsubara Yoshimi; Hitomi Kuroki), in the midst of a divorce, moves to a run-down apartment with her daughter, Ikuko (松原 郁子 Matsubara Ikuko; Rio Kanno). She enrolls her daughter in a nearby kindergarten and in order to win custody of her daughter, starts working as a proofreader, a job she held years ago before she was married. The ceiling of the apartment has a leak, which worsens on a daily basis. Matsubara complains to the janitor of the apartment, an old man, but the janitor does nothing to fix the leak. She then tries to go to the floor just above her apartment to find out the root of the leak, and discovers that the apartment is locked.

Strange events then happen repeatedly: a red bag with a bunny on the front reappears no matter how often Yoshimi tries to dispose of it. Hair is found in tap water. Yoshimi gets glimpses of a mysterious long-haired girl who is of similar age to her daughter. Yoshimi discovers that the upstairs apartment, the source of the leak, was formerly the home of a girl named Mitsuko Kawai (河合 美津子 Kawai Mitsuko; Mirei Oguchi), who was of similar age to her daughter. She had attended the same kindergarten Ikuko now attends. Mitsuko was abandoned by her mother and vanished more than a year ago.

Yoshimi finds her missing daughter one day in the apartment upstairs, which has walls pouring with water with the entire apartment flooded ankle-deep. Convinced something eerie is happening, she decides to move, but her lawyer convinces her that her eyes may be playing tricks on her and that moving now would weaken her position greatly in her divorce.

One evening, after yet another strange occurrence involving the red bag, Yoshimi is drawn to the roof of the building, and while examining the huge water tank she notices that it was last inspected – and thus opened – over a year ago, on the day Mitsuko was last reported seen. She comes to the horrific realization via a vision that Mitsuko had fallen into the tank while trying to retrieve her red bag, and was thus drowned.

Meanwhile, Ikuko, left alone in the apartment, attempts to turn off the bath tap, which has started to spurt filthy water. Mitsukos spirit emerges from the flooded bathtub and attempts to drown her.

Yoshimi finds Ikuko unconscious on the bathroom floor. Intending to escape, she rushes into the elevator, fleeing apparently from the apparition of Mitsuko. But as the elevator door closes she sees that the figure pursuing her is in fact her own daughter – with short hair – and realizes she is carrying Mitsuko, who, gripping her neck, claims Yoshimi as mother in a torrent of water. Yoshimi realizes that Mitsuko wont let her go and with Ikuko looking on in tears, Yoshimi sacrifices herself by staying on in the elevator to appease Mitsukos spirit and pretending to be Mitsukos mother. The elevator ascends and Ikuko follows, but when the doors open, a flood of murky brown water rushes out and nobody emerges.

The end of the film shows Ikuko, now sixteen (Asami Mizukawa), re-visiting the abandoned block. She notices that her old apartment looks oddly clean and seems occupied. She then sees her mother, and they have a conversation. Her mother affirms that as long as Ikuko is all right, she is happy. Ikuko then pleads to stay with her mother, whom she thinks is alive, and though Yoshimi smiles, she tells Ikuko that that would be impossible. Sensing someone behind her, Ikuko warily turns, but sees no one (the audience though sees Mitsuko for a split second). When she turns back, Yoshimi has also disappeared. As she leaves, Ikuko realizes that her mothers spirit has been watching over her.

== Cast ==
*Hitomi Kuroki – Yoshimi Matsubara
*Rio Kanno – Ikuko Matsubara (6 years old)
*Mirei Oguchi – Mitsuko Kawai
*Asami Mizukawa – Ikuko Hamada (16 years old)
*Fumiyo Kohinata – Kunio Hamada
*Yu Tokui – Ohta (real-estate agent)
*Isao Yatsu – Kamiya (apartment manager)
*Shigemitsu Ogi – Kishida (Yoshimis lawyer)
*Maiko Asano – Young Yoshimis Teacher
*Yukiko Ikari – Young Yoshimi

== Related work == the horror anthology by Koji Suzuki and the manga adaptation, authored by Koji Suzuki and illustrated by MEIMU, under Kadokawa Shoten in 2002. The English manga version, translated by Javier Lopez, was published as Dark Water by ADV Manga in 2004.

A Dark Water (2005 film)|U.S. remake of the film, directed by Walter Salles and starring Jennifer Connelly, was released on July 8, 2005.

== Reception ==
The movie received positive reviews upon its release. This film currently holds a 77% "fresh" rating at Rotten Tomatoes.

==See also==
*List of ghost films

== References ==
 

== External links ==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
  