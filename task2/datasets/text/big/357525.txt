Bedknobs and Broomsticks
{{Infobox film
| name        = Bedknobs and Broomsticks
| image       = Bedknobs and Broomsticks poster.jpg
| caption     = Theatrical poster Robert Stevenson Bill Walsh
| screenplay  = Bill Walsh Don DaGradi
| based on    =  
| starring    = Angela Lansbury David Tomlinson John Ericson Ian Weighill Cindy OCallaghan Roy Snart
| music       = Songs:    
| editing     = Cotton Warburton
| cinematography = Frank Phillips Walt Disney Productions Buena Vista Distribution
| released    =  
| runtime     = 119 minutes (1971 original version) 96 minutes (1979 reissue version) 139 minutes (2001 reconstruction version)
| country     = United States
| language    = English
| budget      = $20 million
| gross       = $17,871,174 
}} Walt Disney Mary Norton. The film, which combines live action and animation, stars Angela Lansbury and David Tomlinson.
 Mary Poppins Robert Stevenson, art director Peter Ellenshaw, and music director Irwin Kostal.  

The film received generally positive reviews from critics, scoring 66% on Rotten Tomatoes, and won the Academy Award for Best Visual Effects. This was the last film released prior to the death of Walt Disneys surviving brother, Roy O. Disney, who died one week later.

==Plot==
In England 1940, the Rawlins children, Charlie, Carrie, and Paul are evacuated from London to the Dorset village of Pepperinge Eye. They are placed in the care of the reclusive Eglantine Price, who is in fact an amateur witch and student of a college course run by one Professor Emelius Browne, hoping to use her magic to end World War II. She receives a broomstick from the course, but her first attempted flight ends with her crashlanding. The children witness her flight whilst trying to run away, and inform her the next day. Miss Price makes an agreement with them to keep her status a secret in exchange for giving them a transportation spell, enchanting a knob from her father’s antique bed to activate it.

Miss Price receives a letter from Browne informing her the college has closed due to the war, inspiring her and the children to use the bedknob to travel to London on her father’s bed and find Browne. They discover Browne is in fact a charismatic showman who does not believe in magic until Miss Price transforms him into a rabbit. The elated Browne admits he created the course from an old spellbook he found, and takes Miss Price and the children to an abandoned mansion where he is living. Miss Price receives the spellbook but discovers the last pages containing the most vital spell “Substitutiary Locomotion” are missing, explaining why Mr. Browne ended the course. They go to Portobello Road to find the book’s other half, and are approached by the spiv Swinburne, who takes them to his associate, the Bookman, who possesses the other half of the book.

Miss Price and the Bookman trade the book’s halves but still find the words for the spell missing. The Bookman explains that the book’s author, a sorcerer named Astaroth, used his magic to give wild animals anthropomorphism, but they killed him, and stole the source of his power, a medallion called the Star of Astaroth. The Bookman believes their home, the Isle of Naboombu, does not exist until Paul confirms its existence via a storybook he owns. The group escape on the bed and land in the animated world of Naboombu, eventually meeting King Leonidas, who wears the Star of Astaroth. Leonidas invites Mr. Browne to act as a referee in a soccer match. The chaotic match ends in Leonidas’ self-proclaimed victory, but Mr. Browne swipes the medallion. Leonidas attacks the travellers, but Miss Price transforms him into a rabbit before returning home, only to find the medallion has vanished.

Paul reveals the words to use Substituitary Locomotion were in his storybook the whole time, and Miss Price tries the spell on Mr. Browne’s shoes, but causes a variety of clothes to come alive and run amok. That night, Mrs. Hobday, supervisor for the children’s refuge, informs Miss Price that the children could move, but Miss Price declines the offer. Mr. Browne gets cold feet when the children refer to him as a father figure, and departs to return to London. A platoon of Nazi commandos sneak onto the coast and invade Miss Price’s house, imprisoning her and the children in the local museum. Mr. Browne comes to the rescue after witnessing more Nazis disabling phone lines, inspiring Miss Price to use Substitutiary Locomotion to enchant the museum’s exhibits into an army. The army of knights armour and military uniforms chase the Nazis away, but not before they destroy Miss Price’s workshop, ending her career as a witch.

Though disappointed her career is over, she is happy she played a small part in the war effort. Mr. Browne enlists in the army and departs with the local Home Guard escorting him, while Paul reveals he still owns the enchanted bedknob for potential adventures.

==Cast==
* Angela Lansbury as Miss Eglantine Price. Miss Price is initially a somewhat reclusive woman, reluctant to take in children from London as she believes they will get in the way of her witchcraft, which she prefers to keep secret but hopes to use to bring the nascent World War II to an end. Though at first refusing to take the children in to her house, she quickly warms up to them.
* David Tomlinson as Mr. Emelius Browne. Introduced as "Professor Browne," the title by which Miss Price knows him, he is running a Correspondence College of Witchcraft based on what he believes to be "nonsense words" found in an old book. When Miss Price and the children find him in London, he is revealed to be a street performer and con artist, and not a very good one.  He is, however, a smooth talker, which proves useful on the groups adventures, and believes in doing everything "with a flair."  As the adventures unfold, he finds himself developing an attachment to Miss Price and the children, a feeling he struggles with.
* Ian Weighill as Charles "Charlie" Rawlins. Charlie is the eldest of the orphaned Rawlins children; eleven, going on twelve, according to Carrie, an age which Miss Price calls "The Age of Not Believing."  Accordingly, he is initially cynical and disbelieving of Miss Prices magical efforts, but comes around as time goes on; it is at his initial suggestion that Ms. Price uses the Substitutiary Locomotion spell on the museum artifacts.
* Cindy OCallaghan as Carrie Rawlins.  Slightly younger than Charlie, she takes on a motherly attitude toward her brothers, especially Paul. She is the first to encourage a friendly relationship between Miss Price and the children.
* Roy Snart as Paul Rawlins.  Paul is about six; his possession of the bedknob and the Isle of Naboombu childrens book lead to the groups adventures as well as the eventual solution to the quest for the Substitutiary Locomotion spell.  Paul is prone to blurting out whatever is on his mind, which occasionally leads to trouble.
* Roddy McDowall as Mr. Rowan Jelk, the local clergyman.  Deleted scenes reveal Mr. Jelk to be interested in marrying Miss Price, largely for her property.
* Sam Jaffe as the Bookman, a mysterious criminal also in pursuit of the Substitutiary Locomotion spell.  It is implied that there is some history and bad blood between him and Mr. Browne.
* Bruce Forsyth as Swinburne, a spiv and associate of the Bookmans who acts as his muscle.
* Tessie OShea as Mrs. Jessica Hobday, the local postmistress of Pepperinge Eye and chairwoman of the War Activities Committee.
* John Ericson as Colonel Heller, leader of the German raiding party which comes ashore at Pepperinge Eye.
* Reginald Owen as Major General Sir Brian Teagler, commander of the local Home Guard.
* Arthur Gould-Porter as Captain Greer, a British Army captain who becomes lost in the area. He is constantly running into locals who suspect him of being a Nazi in disguise.
* Hank Worden as Old Home Guard Soldier (uncredited) 
* Cyril Delevanti as Elderly farmer

===Voices===
* Lennie Weinrib as King Leonidas and Secretary Bird.  The king is a lion, and a devoted soccer player with a fearsome temper, as well as a notorious cheater who is known to make up the rules of soccer as he goes along - according to Pauls book.  His Secretary Bird is a prim and proper type who often bears the brunt of the kings temper.
* Dallas McKennon as Bear.  The Bear is a sailor and fisherman on the Isle of Naboombu who pulls the bed, with Miss Prices group on it, out of the lagoon with his fishing pole, and takes them to see the King after warning them of his temper. Bob Holt as Codfish, a denizen of the Naboombu lagoon.

Leslie Caron, Lynn Redgrave, Judy Carne, and Julie Andrews were all considered for the role of Eglantine Price before the Disney studio decided on Angela Lansbury.  David Tomlinson replaced Ron Moody as Emelius Brown due to Moodys busy schedule.

==Production== Walt Disney Studios in Burbank, California. The castle scenes were shot on location at Corfe Castle, Dorset, England. 

The armor used for the climactic battle against the Nazis had been assembled in Spain for the movie El Cid (film)|El Cid and was later shipped to Burbank for use in the movie Camelot (film)|Camelot before being rented for this film.

==Theatre releases== Mary Poppins, but after its premiere, it was shortened from its two and a half-hour length (while the liner notes on the soundtrack reissue in 2002 claims it was closer to three hours) to a more manageable (to movie theatres) two hours. Along with a minor subplot involving Roddy McDowalls character, three songs were removed entirely, and the central dance number "Portobello Road" was shortened by more than six minutes.

Although the musical score was recorded in stereo, and the soundtrack album was presented that way, the film was released in mono sound.

The movie was reissued theatrically in 1979, with a lower time of 96 minutes, with all songs, excluding "Portobello Road" and "Beautiful Briny Sea", being muted out. 

==Home media releases==
The film has been released for home several times on VHS and DVD. Upon rediscovering the removed song "A Step in the Right Direction" on the original soundtrack album, Disney decided to reconstruct the films original running length. Most of the film material was found, but some segments of "Portobello Road" had to be reconstructed from work prints with digital re-coloration to match the film quality of the main content. The footage for "A Step in the Right Direction" was never located. As of 2009, it remains lost, and it is believed that the footage was possibly destroyed. A reconstruction of "A Step in the Right Direction", using the original music track linked up to existing production stills, was included on the DVD as an extra to convey an idea of what the lost sequence would have looked like. The edit included several newly discovered songs, including "Nobodys Problems", performed by Lansbury. The number had been cut before the premiere of the film. Lansbury had only made a demo recording, singing with a solo piano because the orchestrations would have been added when the picture was scored. When the song was cut, the orchestrations had not yet been added; therefore, it was finally orchestrated and put together when it was placed back into the film.
 ADR dubs for those who were unavailable. Even though David Tomlinson was still alive when the film was being reconstructed, he was in ill-health, and unavailable to provide ADR for Emelius Browne.

Some of the alternate actors that re-dubbed the newly inserted scenes had questionable likenesses to that of the original voices (the postmistress, for example, had a British regional accented voice that changed from Welsh to Scottish and back again on the reconstructed scenes). Elements of the underscoring were either moved or extended when it was necessary to benefit the new material. The extended version of the film was released on VHS and DVD on March 20, 2001, to commemorate the 30th anniversary of the film. The reconstruction additionally marks the first time the film was presented in   Special Effects featurette and a The Suite Life of Zack & Cody Blu-ray infomercial.  The Sherman Brothers Featurette, the lost song "A Step in the Right Direction" and most of the other bonus features are retained from the previous edition.   

The movie was released on Special Edition Blu-ray, DVD, and Digital HD on August 12, 2014, in its 117-minute General Release Version, with the deleted scenes used in the previous reconstructed version presented in a separate section on the disc.

==Awards and nominations==
The film received five Academy Award nominations and won one.   

{| class="wikitable sortable"
|- Year
!align="left"|Ceremony Category
!align="left"|Recipients Result
|- 1971
|29th Golden Globe Awards Best Actress - Musical or Comedy Angela Lansbury
| 
|- 44th Academy Awards Best Visual Effects Alan Maley, Eustace Lycett, Danny Lee
| 
|- Best Costume Design Bill Thomas Bill Thomas
| 
|- Best Art Direction John B. Mansbridge, Peter Ellenshaw, Emile Kuri, Hal Gausman
| 
|- Best Original Song Richard M. Sherman, Robert B. Sherman
| 
|- Best Original Score Richard M. Sherman, Robert B. Sherman, Irwin Kostal
| 
|}

==Soundtrack==
{{Infobox album  
| Name        = Bedknobs and Broomsticks
| Type        = soundtrack
| Artist      = Richard M. Sherman, Robert B. Sherman, and Irwin Kostal
| Cover       =
| Released    = 1971
| Recorded    =
| Genre       =
| Length      =
| Label       = Buena Vista
| Producer    =
| Reviews     =
}}
Although the film is in mono sound recording, the songs for the film were recorded in stereo.
These songs include:

*"The Old Home Guard" 
*"The Age of Not Believing" (received an Oscar nomination for Best Original Song)
*"With a Flair" (only in the 2001 reconstruction)
*"Eglantine (song)|Eglantine"
*"Dont Let Me Down (1971 song)|Dont Let Me Down" (only in the 2001 reconstruction) Portobello Road"
*"The Beautiful Briny"
*"Substitutiary Locomotion"
*"A Step in the Right Direction"
*"Nobodys Problems" (only in the 2001 reconstruction)
*"Solid Citizen" (replaced by the soccer match)
*"Fundamental Element" (sections were incorporated into "Dont Let Me Down")
It was also released on CD in 2002.

==See also== Mary Poppins

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*   at  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 