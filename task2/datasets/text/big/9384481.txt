Count Your Blessings (film)
{{Infobox film
| name           = Count Your Blessings
| image_size     = Original film poster 
| caption        =
| director       = Jean Negulesco
| producer       = Karl Tunberg
| writer         = Karl Tunberg
| based on =  
| starring       = Deborah Kerr
| music          = Franz Waxman
| cinematography = George J. Folsey Milton R. Krasner
| editing        = Harold F. Kress
| distributor    = Metro-Goldwyn-Mayer
| released       = April 23, 1959 (US)
| runtime        = 102 minutes
| country        = United States English
| budget         = $2,311,000  . 
| gross          = $1,710,000 
| preceded_by    =
| followed_by    =
}} The Blessing by Nancy Mitford. The music score was by Franz Waxman and the cinematography by George J. Folsey and Milton R. Krasner. The costume design was by Helen Rose.
 Martin Stephens, Ronald Squire, Patricia Medina and Mona Washbourne.

The film was shot in London and Paris.

==Plot==

While visiting Grace Allingham in wartime London at the behest of Hugh Palgrave, his friend, Charles is charmed by her and abruptly proposes marriage. They marry, but during the honeymoon Charles reports back to military duty.

He reportedly is shot and taken prisoner. Grace waits for his return while raising their young son, Sigi. Charles returns after eight years, but over time, Grace comes to learn that during his long absence he has been seeing other women. She turns for comfort to her old love, Hugh.

A divorce seems imminent while 8-year-old Sigi is torn between the two parents and their very different ways of life. Because of their commitment to him, Grace and Charles ultimately reconcile.

==Box Office==
According to MGM records the film earned $810,000 in the US and Canada and $900,000 elsewhere resulting in a loss of $1,688,000. 
==References==
 
==External links==
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 