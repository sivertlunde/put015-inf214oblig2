Vice Versa (1988 film)
{{Infobox film
| name = Vice Versa
| image = Vice Versa (1988 movie poster).jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster Brian Gilbert
| producer = Dick Clement Ian La Frenais
| writer = Ian La Frenais Dick Clement
| based on =  
| starring = Judge Reinhold Fred Savage Swoosie Kurtz
| music = David Shire King Baggot
| editing = David Garfield
| distributor = Columbia Pictures
| released =  
| runtime = 98 minutes
| country = United States
| language = English
| budget =
| gross = $13,664,060   
}} 1882 novel of the same name by F. Anstey. Three previous adaptations were released in the UK in 1916, 1937 and 1948.
 Like Father Like Son, it was released three months before a similar age-changing 1980s comedy, Big (film)|Big.

==Plot==
In Thailand, a pair of thieves steal an ancient skull from a Buddhist monastery.

Marshall Seymour (Judge Reinhold) is Vice President of a Chicago department store in charge of buying. He is divorced and has a son named Charlie (Fred Savage) whom he has no time for. He, along with his girlfriend Sam (Corinne Bohrer), are on a trip to Thailand to purchase exotic merchandise. At the same time, an art thief named Turk (David Proval), tries to purchase the skull but has to find a way to smuggle it out of the country so he puts it with one of Marshalls purchases, so he and his accomplice Tina Brooks (Swoosie Kurtz), arrange to make a switch with a ginger jar that Marshall purchased that was replaced with the skull.

When Marshall returns, he takes Charlie for a few days while his mother, Robyn (Jane Kaczmarek), and step father, Cliff, are vacationing. Tensions run high in this family since Charlie cant understand why his father cant be more involved in his life. While Charlie is holding the skull, they get into an argument about how they wish they could be in each others bodies. It is revealed that the skull possesses magical powers, and after they both express a wish and touch the skull, Charlie grows up into his fathers body, and Marshall shrinks into his sons body. After the initial shock, they each realize they must live out their lives as each other, and Marshall heads off to school to deal with tests, bullies and hockey practice, while Charlie resumes his role as a Vice President from an 11-year-olds viewpoint.

During one moment, Charlie goes out with Sam and takes her to see the rock band, Malice (metal band)|Malice, which Marshall promised to take him, but changed his mind at the last second. This helps Marshalls relationship with Sam even more.

Marshall and Charlie go to the museum and talk with a Professor Kerschner (Elya Baskin), who explains the true nature behind the skull, he wishes to show it to a (Llama) before giving it back to them. Robyn comes home earlier than expected and is furious when she sees Charlie drinking. (Not knowing he is Marshall.)

After failing to get back the skull by asking nicely, the thieves embark on mission to steal it. Charlie learns from Marshalls boss, Avery, that he has called a meeting to pull the plug on Marshalls business, so he comes and picks up Marshall and after purchasing a device that will allow then to communicate together, Charlie listens in on what Marshall says, Turk ends up kidnapping Marshall as ransom, at the last minute, Charlie, who does not know what to say, lashes out and stands up for his father when he finds out he might lose his job. Turk and Tina hold Marshall for Ransom and Charlie tries to get the skull back from the (Llama). During this time, Marshall explains to them that he is not himself, and his father is not himself, that they have switched, due to the skull. Turk seriously considers what Marshall is saying, but Tina is just concerned about getting the skull back so they can be rich. Eventually they get the skull, and Marshall is returned. However Marshall and Charlie rush to reacquire the skull so they themselves can switch back. The last we see them, the two have touched the skull and literally change genders right as Charlie steals the skull back from them, leaving them in their new bodies as punishment.

The police arrest Charlie for possible kidnapping and Cliff bails him out, who says Robyn is not aware of what happened. Sam shows up and says that Marshall still has a job. He asks Sam to take him to his house so that he can give Charlie a present. While they are riding, Charlie proposes to Sam.

Charlie climbs up to his bedroom window and Marshall and Charlie switch back once they realize how they did it in the first place. Marshall goes to see Sam and Charlie listens in to their conversation about his proposal saying that Charlie is excited about it.

==Cast==
* Judge Reinhold as Marshall Seymour / Charlie Seymour
* Fred Savage as Charlie Seymour / Marshall Seymour
* Corinne Bohrer as Sam
* Swoosie Kurtz as Tina Brooks / Turk
* David Proval as Turk / Tina Brooks
* Jane Kaczmarek as Robyn Seymour William Prince as Stratford Avery
* Gloria Gifford as Marcie
* Beverly Archer as Jane Luttrell
* Harry Murphy as Larry
* Kevin ORourke as Brad
* Richard Kind as Floyd
* Elya Baskin as Professor Kerschner
* James Hong as Kwo
* Ajay Naidu as Dale Ferriera
* Jane Lynch as Ms. Lindstrom

==Critical and box office reaction==
The film received mixed reviews,    and flopped at the box-office.  The film was given a score of 43% on Rotten Tomatoes.  It grossed $13,664,060 in the USA on its theatrical run. 

==In popular culture==
The film was mentioned in an episode of   by Randal Graves as an attempted bribery of being Dante Hicks lawyer by telling the Judge Reinhold|"honorable" Judge Reinhold that he   has seen all of his movies including Zandalee and Vice Versa.
 Freaky Friday (film) and all other movies with the same basic premise, including Vice Versa, the DVD of which Abed immediately throws away.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 