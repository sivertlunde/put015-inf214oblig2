The Road to Home
{{Infobox film
| name           = The Road to Home
| image          = Screen Shot 2015-04-12 at 21.34.02.png
| director       = Dominic Brown
| released       = 2015
| producer    = Dancing Turtle Films
| runtime        = 53 minutes
| country        = United Kingdom
| language       = English
}}
The Road to Home is a 2015 feature-length documentary  about Benny Wenda, the West Papuan independence leader. Since his dramatic escape from an Indonesian prison in 2002,  where he was held in isolation and tortured as a political prisoner,  Benny has been an unceasing crusader on the international scene, campaigning to bring about an end to the suffering of his people at the hands Indonesia’s colonial regime. 

==Background==
Granted political asylum in the UK in 2004, Wenda’s freedom of movement was restricted in 2011 when, at the behest of the Indonesian government, Interpol issued a ‘red notice’ putting him at extreme risk of extradition should he travel.  The following year the ‘red notice’ was lifted after being deemed “predominantly political in nature”.  Wenda then embarked on a world-wide “Freedom Tour”  to build support and awareness for the West Papuan Independence movement. To date Benny has performed and spoken in such diverse places as New Zealand, Australia, Switzerland, Poland, the United Kingdom, Papua New Guinea, the Solomon Islands, the United States of America, and many others.

==Production==
The film was produced and directed by British filmmaker Dominic Brown, and shot over a two year period,  with the filmmaker joining Benny Wenda on his overseas trips including his appearance at the Oslo Freedom Forum,  and his first official overseas tour to the US, Australia, New Zealand, Papua New Guinea and Vanuatu.

==See also==
Benny Wenda 
Free Papua Movement

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 