Womanlight
{{Infobox film
| name     = Womanlight
| film name =  
| image    = Clair de femme (film).jpg
| director = Costa-Gavras
| producer = Georges-Alain Vuille
| based on =  
| writer = 
| starring = Yves Montand Romy Schneider
| cinematography = Ricardo Aronovich
| music = Jean Musy
| country = France
| language = French
| runtime = 
| released =  
}}
Womanlight ( ) is a 1979 film by Costa-Gavras based on the 1977 novel Clair de femme by Romain Gary.  

==Cast (alphabetical)==
* Roberto Benigni as Le barman
* Heinz Bennent as Georges
* Jacques Dynam
* Lila Kedrova as Sonia
* Daniel Mesguich as Lofficier de police
* Yves Montand as Michel
* Jean Reno as Lagent de la circulation
* Michel Robin
* Romy Schneider as Lydia
* Romolo Valli as Galba

==References==
 
 
 


 