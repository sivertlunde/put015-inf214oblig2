The Man by the Shore
 
{{Infobox film
| name           = The Man by the Shore
| image          =
| image size     =
| caption        =
| director       = Raoul Peck
| producer       = Pascal Verroust
| writer         = Raoul Peck André Graill
| narrator       =
| starring       = Jennifer Zubar
| music          =
| cinematography = Armand Marco
| editing        = Jacques Comets
| distributor    =
| released       =  
| runtime        = 106 minutes
| country        = Haiti France
| language       = Haitian, French
| budget         =
}}

The Man by the Shore ( ) is a 1993 Haitian-French drama film directed by Raoul Peck. It was entered into the 1993 Cannes Film Festival.   

==Cast==
* Jennifer Zubar – Sarah
* Toto Bissainthe – Camille Desrouillere
* Patrick Rameau – Gracieux Sorel
* Jean-Michel Martial – Janvier
* Mireille Metellus – Aunt Elide
* Magaly Berdy – Mirabelle
* Johanne Degand – Jeanne
* Douveline Saint-Louis – Sabine
* François Latour – François Jansson
* Aïlo Auguste-Judith – Gisèle Jeansson
* Albert Delpy – Assad
* Michèle Marcelin – Madame Janvier
* Norah Moriceau – Annie Sarah
* Fritzner Cedon – Nazaire
* Michèle Léger – Mère Suzanne
* Anne Mejia – Mère Séverine

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 
 