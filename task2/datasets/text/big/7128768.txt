Spring Breakdown
{{Infobox film
| name           = Spring Breakdown David A. Keeps (January 22, 2009).  . LA Times. Accessed 2010-01-30. 
| image          = SpringBreakdown_.JPEG
| image_size     = 
| caption        = DVD cover
| director       = Ryan Shiraki
| producer       = Rick Berg Larry Kennar
| writer         = Ryan Shiraki (screenplay) Ryan Shiraki & Rachel Dratch (story)
| starring       = Amy Poehler Parker Posey Rachel Dratch Amber Tamblyn Sophie Monk Jonathan Sadowski Missi Pyle Jane Lynch Mae Whitman Sarah Hagan
| music          = Deborah Lurie
| cinematography = Frank G. DeMarco
| editing        = Tom Lewis
| distributor    = Warner Premiere 
| released       =  
| runtime        = 84 minutes 
| country        = United States
| language       = English
| budget         = $12.3 million 
| gross          = 
}} its parent company, it was released direct-to-video in 2009.

==Plot==
The film begins with Gayle, Becky and Judi performing in McCormick State Colleges Senior Talent Show in 1992. The three of them were the losers and geeks in college but were always hopeful about their future. The next scene then jumps to 15 years later. Gayle is now a guide dog trainer for the visually handicapped. She asks her client out on a date but gets rejected after he touches her face. Becky is an office manager for Senator Hartmann. Judi and her fiance, William, go for relationship counseling and insist that there are no secrets between them. Becky returns home only to find her cat, Honey, dead. The three of them hold their usual make your own pizza party and play their usual movie game. They decided to go on a trip to Tempe, Arizona to the Wimmins Music Festival.

Senator Hartmann announces to her team that she is the potential next vice president. In order to ensure success, she has to make sure that she has a good reputation and background. Ashley, her daughter, just broke up with her boyfriend because she is not slutty enough. In order to win him back and not disappoint her mother, she decides that she will be going to South Padre for her spring break. She wants her to think that she is just like her mother was back in the days - the most popular girl in her sorority and always up for a good time. In order to make sure that Ashley does not act out, Senator Hartmann sends Becky to go to South Padre to keep an eye on her. Throughout the trip, Gayle becomes very close to a group of girls called The Sevens who are Ashleys nemesis.

Judi returns home and discovers that William is actually gay and he ends up breaking off with her. Judi meets up with Becky and Gayle and the three of them decide to go to South Padre to relive the college days that they never had. Even though the girls are appalled with the state of the place, Gayle and Judi fit into the crowd easily. They spend the next few days getting wasted while Becky keeps to the main reason of her being there. Gayle and Judi eventually persuade Becky into relaxing.

One night at a foam party, Ashley finds out that Becky was actually sent by her mother and feels betrayed because she thought they were friends. They engage in a cat fight and end up in jail. That night, William goes to find Judi and asks for a second chance. Then Judi bails Becky and Ashley out of jail. She announces to everyone that she is going to marry William. Gayle declares that shes going to be in the All Girl Talent Show with The Sevens because she is finally going to win. This leads to an argument, and Judi leaves to get married, while Becky and Gayle prepare separately for the talent contest.

At the airport, Judi finally admits to herself that William is gay and tells him she cant marry him. Gayle falls out with Mason, the leader of The Sevens, just before they go on stage. Senator Hartmann appears backstage and wants to bring Ashley back home by force. They have a confrontation and Ashley begs her mother to let her compete in the show, and her mother relents. As the group begins to perform, the pianist passes out (drunk) and Judi returns just in time to replace her. They perform, with begrudged success. The film ends with the three of them back home, at their usual make your own pizza party playing their usual movie game.

==Cast==
*Amy Poehler as Gayle OBrien
*Parker Posey as Becky St. Germaine
*Rachel Dratch as Judi Joskow (now Cody)
*Amber Tamblyn as Ashley Hartmann 
*Seth Meyers as William Rushfield
*Sophie Monk as Mason Masters
*Jonathan Sadowski as Doug
*Missi Pyle as Charlene
*Jane Lynch as Senator "Kay Bee" Hartmann
*Mae Whitman as Lydia 
*Sarah Hagan as Truvy
*Jana Kramer as Seven #2
*Kristin Cavallari as Seven #3 
*Justin Hartley as Todd

==Production and release==
Spring Breakdown was filmed in 2006 and was initially created by Rogue Pictures as an "R-rated spring break|spring-break farce"; it was then sold to Warner Independent Pictures as a PG-13 film and underwent a long post-production period.

The score to Spring Breakdown was composed by Deborah Lurie who recorded her score with the Hollywood Studio Symphony conducted by Blake Neely and recorded by Greg Dennen at the Eastwood Scoring Stage at Warner Brothers.   

In April 2008, co-star Missi Pyle believed the box-office performance of Baby Mama (film)|Baby Mama would determine whether Warner Bros. released this film theatrically. 
 Park City at Midnight before going direct-to-video.

==Reception==
Sundance called Spring Breakdown an "outlandish, quick-witted romp that jubilantly leaves none immune to ridicule" and a film that "chews up our geeky gals and spits them out triumphant powerhouses—confident that being who they truly are is way cooler than fitting in." Variety (magazine)|Variety magazine called the film "energetic but uninspired" with a "party-boatload of comedic talent   is fairly wasted" and notes:   tropes &mdash; wet T-shirt contest, drunken puke-outs, a climactic talent show triumph &mdash; and the rote Girl Power|girl-power message rings unironically hollow. 

Ray Greene of Boxoffice (magazine)|Boxoffice magazine, after seeing the film at Sundance, gave the film " " (no stars), saying "The annual Sundance “What the f---” moment has arrived in the form of Spring Breakdown, a very bad genre exercise starring some very good comedic actresses."  

As of February 2010, 1459 IMDb users gave the film a weighted average vote of 4.9 / 10, with female users across all age demographics rating it higher than the websites mostly male users. 

==References==
 

==External links==
* 

 
 
 
 
 
 