Twenty Something (film)
 
 
{{Infobox film
| name           = Twenty Something
| image          = 
| caption        = 
| film name = {{Film name| traditional    = 晚九朝五
| simplified     = 晚九朝五
| pinyin         = Wǎn Jiǔ Jiāo Wú
| jyutping       = Maan5 Gau2 Ziu1 Ng5}}
| director       = Teddy Chan
| producer       = Peter Chan
| writer         = James Yuen
| starring       = Moses Chan Farini Cheung Jordan Chan Valerie Chow Hui
| cinematography = Kwan Pak Suen Herman Yau Cheung Ka Fai
| studio         = United Filmmakers Organisation (UFO)
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK $10,468,360
}} 1994 Cinema Hong Kong film directed by Teddy Chan Tak-Sum (陳德森). It is the sole Category-III film in UFOs (United Filmmakers Organization) list of credits, and features a popular theme song "Wish" (願) sung by Sandy Lam. Then newcomer Jordan Chan won Best Supporting Actor at the 14th Hong Kong Film Awards for his role in the film.

Chan was the producer to the films sequel, Twenty Something Taipei (台北晚九朝五) in 2002, which was directed by Leon Dai and starred an entirely different cast. The original theme song is sung in Mandarin by Elva Hsiao.

==Cast and roles==
* Moses Chan – Tom
* Jordan Chan – Bo
* Valerie Chow – Alice
* Karsin Bak - Pat
* Farini Cheung – Jennifer
* Selena Khoo – Sue

==External links==
*  

 

 
 
 
 