National Lampoon's 301: The Legend of Awesomest Maximus
 
{{Infobox film
| name           = The Legend of Awesomest Maximus
| director       = Jeff Kanew
| writer         = Jason Burinescu
| starring       = {{Plainlist|
* Will Sasso
* Kristanna Loken
* Sophie Monk }}
| studio         = {{Plainlist|
* National Lampoon Productions
* Farah Films & Management
* Vision Entertainment }}
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
}}

National Lampoons 301: The Legend of Awesomest Maximus is a 2011 comedy film directed by Jeff Kanew, starring Will Sasso. It was released on August 5, 2011. It was released on DVD on March 20, 2012.

==Cast==
* Will Sasso as Awesomest Maximus
* Kristanna Loken as Hottessa
* Sophie Monk as Princess Ellen
* Khary Payton as King Erotic
* Ian Ziering as Testiclees
* Gary Lundy as Orlando
* Rip Torn as King Looney
* Nelson Frazier, Jr. as Ginormous
* Tony Cox as Minorities

==Production==

 

==Reception==
The Legend of Awesomest Maximus earned mixed reviews on Netflix, receiving an average score of 3.2 stars out of 5 from almost 140,000 reviews.

==References==

 

==External links==
*   at Internet Movie Database
*   at Rotten Tomatoes

 

 
 


 