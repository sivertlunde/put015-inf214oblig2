Jigariyaa
{{Infobox film
| name           = Jigariyaa
| image          = Jigariyaa the Film.jpg
| caption        = Theatrical release poster
| director       = Raj Purohit 
| producer       = Raju Chadha Vinod Bachchan
| Co-Producer    = Dinesh Madan
| screenplay     = Raj Purohit Apratim Khare 
| story          = Vinod Bachchan
| starring       = Harshvardhan Deo Cherry Mardia Virendar Saxena K.K. Raina Navni Parihar Natasha Rastogi Vineeta Malik Deepak Chadha Ketan Singh Sneha Deori
| music          = Agnel- Faizan Raj-Prakash
| Lyrics         = Arun Kumar Faraaz Ahmed
| cinematography = Sriram Ganapathy
| editing        = Ranjeet Bahadur, Karan Varia, Mahesh Allala
| studio         = Wave Cinemas Ponty Chadha A Soundarya Production
| distributor    = Wave Cinemas Distribution
| released       =  
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Jigariyaa is an Bollywood romantic film.  The film is directed by Raj Purohit introducing Harshvardhan Deo and Cherry Mardia in lead roles.  The first look of the film was unveiled by Rohit Shetty in Mumbai on August 26, 2014.  The film is inspired by true events released on 10 October 2014. 

==Plot==
Inspired from true events, Jigariyaa tells the story of Shyamlal Gupta (Shaamu) and Radhika Sharma (Raadha). Shaamu, the only son of halwaai Ramlal Gupta is a happy go lucky boy based in Agra. He spends his days writing sheyr-o-shayari admired by his motley group of good-for-nothing friends. Radhika, the only daughter of Pandit Shankar Dayal Sharma is a well educated and caring girl who helps her father in his endeavours as a social do gooder and a man of high reputation in Mathura. Shaamu falls in love with Raadha at first sight, who is visiting her Nani’s house in Agra, thus begins his quest to find this elusive girl in the streets of Agra. As they grow fond and close to each other, the destiny takes another turn and the two lovers break apart. 

==Cast==

* Harshvardhan Deo as Shaamu
* Cherry Mardia as Raadha
* Virendra Saxena as Shaamus father
* K. K. Raina as Raadhas father
* Navni Parihar as Raadhas mother
* Natasha Rastogi as Shaamus mother
* Vineeta Malik as Raadhas maternal grandmother
* Deepak Chadha as Raadhas maternal uncle
* Ketan Singh
* Sneha Deori

===Original soundtrack===

{{track listing
| headline = Jigariyaa Soundtrack
| extra_column = Singer(s)
| total_length = 31:55
| collapsed = no
| title1 = Arziyaan
| extra1 = Vikrant Bhartiya, Aishwarya Majmudar
| length1 = 04:27
| title2 = Ishq Hai (Reprise)
| extra2 = Javed Ali, Yashika Sikka
| length2 = 05:06
| title3 = Ishq Hai 
| extra3 = Javed Ali 
| length3 = 04:26
| title4 = Jigariyaa
| extra4 = Javed Bashir
| length4 = 05:12
| title5 = Mora Rangddar Saiyyaan
| extra5 = Prajakta Shukre, Roop Kumar Rathod
| length5 = 05:01
| title6 = Phurr Phurr
| extra6 = Aishwarya Majmudar, Manjira Ganguly, Agnel Roman
| length6 = 06:13
| title7 = Rang Rang De
| extra7 = Suchi, Jatinder Pal Singh, Yashika Sikka
| length7 = 05:57
}}

== References ==
 
 

== External links ==
*  
* 
* 
* 
* 

 
 
 
 