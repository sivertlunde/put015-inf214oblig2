April Maze
{{Infobox film
| name= April Maze
| image= april-maze-copley.jpg
| image size= 
| alt=  Inky and Winky Cat in April Maze Pat Sullivan Pat Sullivan
| writer= Pat Sullivan
| starring= Felix the cat, Inky, Winky
| studio= Copley Studios
| distributor= Copley Studios
| released= 1930 
| runtime= 7 minutes
| country= United States
| language= English}}
April Maze is a 1930 short animated cartoon featuring Felix the Cat.
It was produced by Pat Sullivan.

==Plot==
Felix brings his nephews Inky and Winky to the park for a picnic. However, they always seem to be delayed. While they say their grace, a rabbit gets a snake to steal their meal. When they try to get it back, it rains so they needed to go back home. Inky and Winky are upset and Felix does his best to carry them out on the picnic. Fortunately, the rain stops and the three continue to the picnic. It rains again for a short while, but then it gets better. They say their grace again before eating, and someone else steals their meal. Felix runs after the culprit however, he cant catch him. At the end of the story, a stork comes with a picnic basket. Hoping the stork has found his meal, Felix opens the basket with joy, but he finds out it is just more kittens.

==Inky and Winky==
Inky and Winky make their most famous theatrical appearance in this short, which is occasionally mistaken for their only appearance. They actually debuted in Felix the Cat Weathers the Weather (1926), returning in several more 1920s shorts before April Maze. Sometimes three kittens were used, in which case the third was called Dinky. In Eskimotive (1928), Inky appeared solo.
In the 1950s, two nephews appeared with Felix in their own comic books. There Inky and Dinky were used. The most common pairing of Inky and Winky began several years later.
Although Felixs nephews do not appear in the early-1960s and mid-1990s TV series, they have a considerable role in the online comics, making them among the principal characters in the Felix media.

==External links==
* 
*  at the Big Cartoon Database
 

 
 
 
 
 


 