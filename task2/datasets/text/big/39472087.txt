Compulsion (2013 film)
{{Infobox film
| name           = Compulsion 
| image          = Compulsion 2013 movie poster.jpg
| caption        = Theatrical release poster
| border         = yes
| alt            = 
| director       = Egidio Coccimiglio 
| producer       = Gary Howsam, Bill Marks  
| writer         = Floyd Byars 
| based on       =  
| starring       = Heather Graham   Carrie-Anne Moss   Kevin Dillon   Joe Mantegna   Jonathan Goldsmith 
| cinematography = Vilmos Zsigmond 
| editing        = D. Gillian Truster 
| studio         = Phase 4 Films
| distributor    = Dimension Films
| released       =  
| runtime        = 88 minutes
| country        = Canada
| language       = English
| budget         = $4 million (estimated) 
| gross          = 
}}
Compulsion is a 2013 Canadian independent  psychological thriller film directed by Egidio Coccimiglio and starring Heather Graham, Carrie-Anne Moss, Kevin Dillon, and Joe Mantegna.  The movie is based on the South Korean film 301, 302 directed by Park Chul-soo, which also serves as a remake. It focuses on two women occupying neighboring apartments, each one grappling with psychological disorders that begin to overtake their lives. 

The movie opened for limited release on June 21, 2013. 

==Plot==
Amy (Heather Graham), an obsessive chef, befriends an anorexic former child star Saffron (Carrie-Anne Moss) living in the apartment next door, sparking a fiery battle of wills. Unstable Amy wields a spatula like a samurai, and dreams of the day shell have her own cooking show. As a young girl, Amy was infatuated with a television show starring Saffron (Carrie-Anne Moss), who subsequently vanished into obscurity. The damaged daughter of a tyrannical stage mother, Saffron has struggled with an eating disorder ever since she was a little girl. Plagued by deep-rooted intimacy issues, she spends most of her time alone until the day she crosses paths with Amy. At first, Amy cant believe her luck at having her childhood idol living right next door, and at first the two women even sense an ethereal connection that bonds them. Later, as their true colors begin to emerge, both Amy and Saffron discover just how toxic their chemistry may be.

==Cast==
*Heather Graham as Amy 
*Carrie-Anne Moss as Saffron 
*Kevin Dillon as Fred 
*Joe Mantegna as Detective Reynolds  James McGowan as Bob  Natalie Brown as Rebecca 
*Jean Yoon as Leslie

==Production==
Although the film takes place in New York City, principal photography took place in  Sault Ste. Marie and Toronto, Ontario.

==Critical reception==
Compulsion holds one rotten review on Rotten Tomatoes but has a 61% vote of viewers who would like to watch the movie. 

==See also==
*301, 302, the original, released in 1995.
*Girl, Interrupted (film)|Girl, Interrupted, a 1999 drama film.
*Single White Female, a similarly themed film released in 1992.

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 