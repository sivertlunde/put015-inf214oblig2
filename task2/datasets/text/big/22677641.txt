Daniel – Der Zauberer
{{Infobox film
| name           = Daniel – Der Zauberer
| image          = Danielzauber.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Ulli Lommel
| producer       = Peter Schamoni
| writer         = Ulli Lommel
| cinematography = Manuel Lommel
| editing        = Angelika Steinbock
| music      =  Robert Schulze
| studio         = Peter Schamoni Film
| distributor    = Stella / Rekord-Film
| released       = 12 August 2004
| runtime        = 81 min.
| country        = Germany
| language       = German
| budget         = low budget   
}}

Daniel – Der Zauberer (translated: Daniel – The Wizard) is a German grotesque    experimental film  about the pop singer Daniel Küblböck, starring as himself. This movie is often considered as one of the worst movies ever made.

==Plot==
The successful singer Daniel Küblböck is "loved by millions, hated by many" (as the subtitle says). The teenagers Rike and Tom decide to kill Daniel. They are morally but not directly supported by Baltazar. Whereas Daniel gets support from his dead grandfather Johnny who mostly carries a baritone horn and a wand on his person, has sometimes just one arm and wears a cap under his top hat.

The first attempt on Daniels life fails, because the teenagers are discovered and scared away by Daniels vocal coach. Daniel shall take part in a screen test for Hollywood, Los Angeles, California|Hollywood. Meanwhile Johnny and Baltazar talk with each other. Suddenly Johnny changes Baltazar into a cockroach. He assumes human shape again as he says the words: "I’m a celebrity, get me out of here!"

Later the girl Petra visits Daniel. She is a fan of Daniel and wrote him a letter before. Petra works at the café of her grandfather (Grandpa Winter) in Daniels hometown Eggenfelden. Grandpa Winter cant stand Daniels music and the guests of his café agree with him. Daniel samples some tortes with his finger, buys them and gives Petra two cost-free tickets for his last concert of this year that takes place in Passau.

Rike and Tom plan to shoot Daniel on the stage at this concert. But instead Rike just kidnaps Daniel and brings him to her house. Soon after that Tom arrives. Beforehand Baltazar encouraged Tom to kill Daniel once more. Tom and Rike want to film Daniels execution, in order to become superstars themselves. When Daniel is alone in the room, his grandfather appears and emboldens Daniel to take the hardest test. Afterwards Rike and Tom dont bring off Daniels murder. Instead of it they talk about their unhappy childhoods. Tom confesses that Daniels latest hit song isnt bad in his opinion. Rike goes away and Tom makes a deal with Daniel. He releases Daniel and in return Daniel doesnt report the kidnap. Finally, Daniel returns to his concert. Grandpa Winter who is a concert attendee becomes convinced of Daniel and after it, hes a fan of him who behaves like a teenager.

Johnny appears again in the evening. As Daniel asked for his Christmas gift, Johnny says to him that he forgot Rike and Tom and that he shall give them his new guitar. At first, Daniel disagrees with him, but then he does it anyway, whereupon the three of them become best friends.

After all, Daniel finds a wand under the Christmas tree, with the note: "By the One-Armed". Daniels grandma tells that her husband had just one arm. He was a musician at the Oktoberfest. She shows a picture of him and Daniel says that he appeared several times to him. His grandma says that Daniel is only allowed to use the wand in order to help people.

Johnny and Baltazar meet again. Baltazar says to him that Johnny won the fight, but the war isnt over yet and the new wizard doesnt know how to use the wand rightly.

==Cast==
* Daniel Küblböck as himself
* Ulli Lommel as Johannes "Johnny" Küblböck (Daniels grandpa)
* Peter Schamoni as Grandpa Winter
*   as Baltazar
* Günther Küblböck as himself (Daniels father)
* Marina Lommel as Petra
* Manolito Lommel as Daniel at age six
*   as Grandma Küblböck
* Adele Eden as Rike
* Oliver Möller as Tom
* Roger Fritz as film producer from Hollywood
* Günther Ziegler as vocal coach

==Style==
Director Ulli Lommel stated that he wanted to make a film that shows young people how to deal with frustration and hate without getting into the eternal cycle of hate and counterhate. In his opinion the understanding between the old and the young people and the mutual trust play an important role in it and so he decided to shoot the film with an intergenerational cast.  The cast features experienced actors like Lommel, Schamoni, Brem and Rupé; young actors like Eden and Möller; and also lay actors like Küblböck. All scenes were filmed spontaneously. 
 reality TV show "Ich bin ein Star – Holt mich hier raus!" is displayed, in which he had to lie in a coffin full of cockroaches. Headlines about Daniel Küblböck in the tabloid newspaper Bild are also shown (one refers to Küblböcks car accident with a truck).

Ulli Lommel tried to find an explanation for the controversy about Daniel Küblböck in Germany:
 

The film contains four songs by Daniel Küblböck:
* "Teenage Tears"  (peaked at #16 on the German singles chart) 
* "My Life Is Magic"
* "Man in the Moon"
* "The Skin Im In"

==Distribution==
Daniel – Der Zauberer was released in Germany on 12 August 2004, and was watched by 13,834 viewers in total.  Most cinemas removed the film from their programme after the first week, because of the low number of attendees. On 30 September 2005 the film was released on DVD (available in Germany and Austria only).

==Reception== one of lowbrow through to the camp (style)|camp, hed present the beautiful sentimentality and the great kitsch.  

The film became the lowest ranked film on the IMDb Bottom 100, where it remained for a considerable amount of time, and wieistderfilm.de stated it was fair to call it the worst German film ever made.   It appeared on Total Films list of the 66 worst films of all time.      In an interview conducted several years after its release Daniel Küblböck admitted that in retrospect "You have to say this is the worst movie of all time really." 

==See also==
* List of films considered the worst

==References==
 

 
 
 
 
 
 
 
 