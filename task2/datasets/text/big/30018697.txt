July 4 (film)
{{Infobox film
| name           = July 4
| image          = July4_film.jpg
| caption        = July 4 DVD cover
| alt            = Joshi
| producer       = Alwin Antony  Suku Nair Udayakrishna Siby K. Thomas Dileep  Roma
| music          = Ouseppachan
| cinematography = Shaji
| editing        = Ranjan Abraham
| studio         =
| distributor    = Varnachitra Big Screen & PJ Entertainments 
| released       =  
| runtime        =  150 minutes
| country        = India
| language       =  Malayalam
| budget         =
| gross          =
}}
 Malayalam film Dileep and Roma in the lead roles.

==Plot==
The story begins in a jail. Ramachandran (Siddique (actor)|Siddique), a very agile police officer, takes charge as the superintendent. He is given a brief about the jail and the prisoners by Narayanan Potti (Innocent (actor)|Innocent), who is the jailer.

One day Ramachandran comes face to face with Gokul Das (Dileep (actor)|Dileep), one of the prisoners who is to be released soon, on July 4. It is then revealed that the very reason Ramachandran had taken charge of the jail is to meet Gokul Das, with whom he has some old scores to settle. From here unfurls, in flashback sequences, the story of Gokul Das.

As a child, Gokul Das had no one but his mother (Sona Nair) to call his own; he lived in a street and worked in a cycle shop. When he is picked up by the police along with other boys from the street for a crime he has not committed, things go berserk in his life. His mother comes to get him released but is raped and killed by the police officer (Chali Pala) in charge of the station.

When Gokul comes out, he teams up with some other boys and takes revenge on the police officer, strangling him. Then they flee to Mumbai, where they become criminals, constantly clashing with the cops. Things get too hot for them when a daring police officer takes charge, and they break up, with Gokul taking refuge in a colony at the house of Gopalan (Vijayaraghavan (actor)|Vijayaraghavan), who is a kind-hearted taxi driver. He gets close with Gopalan and his family, but soon the cops reach there, too. In the tussle that follows, Gopalan is killed. Gokul, who escapes from the cops, is filled with remorse. Later he takes Gopalans family and leaves Mumbai, deciding to lead a good life. He works as a taxi driver at the Coimbatore airport. One day he meets a young girl woman (Roma Asrani|Roma) there. She later travels in his car. When Sreepriya urges him to speed, offering him more money, he obliges, only to hit an auto rickshaw carrying school children. Sreepriya catches another taxi and continues on her way. Gokul Das later turns up at Sreepriyas house asking for compensation. Viswanathan, her father (Devan (actor)|Devan), offers him a job a driver, and things take a new turn.

After the engagement of Sreepriya with Unni (Sarath), the son of Ramachandran, Das takes Sripriya for an examination in Coimbatore. But on the way back, they are chased by a gang of three toughies, who during the previous night had tried to break into her bedroom.   Gokul Das and, Gokul hides her in an old house. And says her that to open the door when he says. Then when he out he hugs the toughies.    And in the flashback it is seen that they were his friends who helped him to kill the inspector. As they are forced to spend a few days together, they develop a liking for each other.

==Cast== Dileep as Gokuldas Roma as Sri
* Devan Siddique
* Innocent
* Cochin Haneefa
* Salim Kumar
* Lakshmy Ramakrishnan
* Sona Nair
* Sarath
* Mangala
* Riaz Khan
* Santhosh Jogi
* Anil Murali Pavan
* Chali Pala 

==Release date==
Some films in which Dileep was cast in the lead role and released on July 4 were big successes. The list includes Ee Parakkum Thalika (2001),    Meesha Madhavan (2002), C.I.D. Moosa (2003),    Pandippada (2005),    Chess (2006),.    This film too was planned to release on July 4, 2007 and had to change to July 5, 2007, due to a court order on a complaint that the story was copied without permissions from the author.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 