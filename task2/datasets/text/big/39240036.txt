Two Lies
{{Infobox film
| name = Two Lies
| image = 
| director = Pamela Tom
| producer = Pamela Tom
| associate producer = Jack Kuramoto
| writer = Pamela Tom
| starring = Dian Kobayashi   Sala Iwamatsu   Marie Nakano
| music = James Leary
| director of photography = Gary Tieche
| editing = Pamela Tom
| distributor = Women Make Movies
| released =  
| runtime = 25 minutes
| country = United States
| language = English
}}

Two Lies is a 1990 short film directed, produced and written by Pamela Tom. The film was shot in black and white and discusses issues like identity, Orientalism, and Blepharoplasty.

==Plot==

The plot of the film focuses on the life of a Chinese American family who struggles with identity. The mother, Doris Chu, has undergone blepharoplasty, or "double eyelid" surgery, and the oldest of two daughters, Mei, has trouble understanding her mothers actions. From her perspective, her mothers two new eyes are equivalent to two lies. On a trip to Cabots Pueblo Museum off a dusty highway in Southern California, a series of confrontations occur and Mei finally realizes her mothers reasoning. Between the conflicts of Chinese tradition and American ways, the family tries to adapt to their new life in Southern California. 


"Fresh, contemporary. The sharp sense of cultural disorientation depends on Toms atmospheric style. Clearly the work of a filmmaker who is fearless."
:Caryn James
:New York Times

==Cast==
* Dian Kobayashi - Doris Chu (mother)
* Sala Iwamatsu - Mei Chu (oldest daughter)
* Marie Nakano - Esther Chu (youngest daughter)

==Extended Cast==
* Joe Marinalli - Nick
* William Merzenich - Pueblo Tour Guide
* Bob McCarthy - Martin
* Melba Yale - Accordion Teacher
* Walter Looell, Jr - Teenage Boy at Pool
* Carolyn Stanek - Little Girl in Pool

==Awards, Festivals, & Screenings==
* New Directors/New Films
* Hawaii International Film Festival
* USA Film Festival
* NY Asian American Intl Film Festival
* Sundance Film Festival
* The Smithsonian Institution

==External Links==
 

==References==

 

 

 