Nidra (2012 film)
{{Infobox film
| name = Nidra
| image = Nidra_film.jpg
| caption = Film poster
| alt =
| director = Siddharth Bharathan
| producer = Sadanandan Rangorath Debobrath Mondal
| based on = 
| story = Ananthu
| screenplay = Santhosh Echikkanam  Siddharth Bharathan Jishnu K. P. A. C. Lalitha  Vijay Menon
| music = Songs:  
| cinematography = Sameer Thahir
| editing = Bavan Sreekumar
| studio = Lucsam Creations
| distributor = Remya Movies
| released =   
| runtime = 104 minutes
| country = India
| language = Malayalam
| budget = 
| gross = 
}} Jishnu in the main roles. Siddharth co-wrote the adapted screenplay with noted author Santhosh Echikkanam. The songs were composed by Jassie Gift and the background score was by Prashant Pillai. The cinematography was handled by Sameer Thahir.

Considered to be a part of the new-wave in  :  . Retrieved 11 November 2012. 

==Plot==
Raju (Siddharth Bharathan) is the younger son from a wealthy family. Raju, who was in Germany to do space research, was not informed of his mothers demise and has a nervous breakdown when he gets the news. He takes a couple of years to recover. After that he marries his childhood sweetheart Ashwathy (Rima Kallingal). She is the daughter of their former family chauffeur and the marriage took place much against her mothers wishes. Ashwathy barely has an idea as to what lies in store as Raju has had a history of mental unsteadiness. She has heard about his illness, that he has been mentally disturbed after his mothers death but now, she has to face the situation with virtually no support.

Raju lives in a world of his own, away from all the business deals and money, unlike his elder brother Vishwan (Jishnu (actor)|Jishnu). But his near and dear ones look at every action of his through the prism of his past illness, which frustrates him. It does not help that he has a rather short temper. To add to the oddities, he has converted his bedroom into a space research lab and dreams of building an organically self-sustainable world for himself and his wife in the middle of the familys rubber estate. Vishwan has plans to build a resort catering to foreign clients. All this makes Raju very angry and violent, and people begin to think that his illness has resurfaced. Ashwathy is determined to get him back to life with her love. She almost succeeds, but the people around them were not so bothered about compassion, tender human emotions and selfless love.

==Cast==
* Siddharth Bharathan as Raju
* Rima Kallingal as Ashwathy Jishnu as Vishwan Sarayu as Priya, Vishwans wife
* Thalaivasal Vijay as Madhava Menon, Rajus father
* KPAC Lalitha as Bhargavy Amma, Ashwathys mother
* Vijay Menon as Dr. Roy Peter 
* Shivaji Guruvayoor as Bhaskara Menon
* Rajeev Parameswar as Rajeev
* Ajmal as Gopu
* Kavitha as Maya
* Manikandan Pattambi
* Ambika Mohan as Priyas mother
* Baby Maria Biju as Ammu
* Malavika as Revathi
* Preethi as Nirmala

==Production==

===Adaptation===
Siddharth wanted to make his directorial debut by adapting the 1964 horror classic Bhargavi Nilayam which was written by noted author Vaikom Muhammad Basheer and directed by A. Vincent.  The project was dropped in pre-production stages. Siddharth then began directing Mithram but the project had to shelved mid-way due to production problems.  Siddharth decided to remake the 1981 film Nidra, which was directed by his father Bharathan. The original, which starred Shanthi Krishna and Vijay Menon in the lead roles, was a critical success although is not usually recognised as one of Bharathans better creations. The new version is set in modern times and Siddharth considers it as a tribute to his father.  

Siddharth says he chose Nidra of all his fathers films because its story is relevant even now. Siddharth says, "If I have to remake a film, there should be something that I can convey through it. This story can happen even today, that too, during these times of mobile phones and Facebook. It has been set in a rural place like Chalakudy." Vijay G (20 February 2012).  .  . Retrieved 11 November 2012.  Some reinterpretations of the major elements and scenes of Nidra have also been done.  The adapted screenplay was written by noted author Santhosh Echikkanam and Siddharth.

===Pre-production===
Nidra was announced in December 2010  . Sify.com. 2 December 2010. Retrieved 12 November 2012.  and was originally titled Suhruthu.  Kuttamath Films, a newly launched production house, was the producer.  The production was to begin on 5 January 2011 and the film was slated to release coinciding with the festival of Vishu in April 2011.    However, the project was long delayed due to technical reasons. Later the crew decided to retain the original title and the film, with the new title, was officially launched in November 2011 with production set to begin in December. 

When the film was announced in 2010, Manu and Rima Kallingal were cast to play the lead roles, with the former making his film debut.    Rima Kallingal, who had been the busiest actress of the year,  was chosen as the female lead much earlier.  When Manu opted out citing personal reasons (he made his debut later in 2011 through  . Retrieved 11 November 2012.  Siddharth says: "I have been working on this script for quite some time now and I could easily relate to the psyche of the character as I used to emote out the characters nature at various stages of development. We had cast another actor in the lead role. But when he couldnt do it due to certain personal issues, it was relatively easy for me to step into the shoes of the character."  

Another important role was given to Jishnu (actor)|Jishnu, who had acted with Siddharth in Nammal.  Nidra marks Jishnus comeback, who took a brief hiatus to pursue projects outside filmdom.  KPAC Lalitha, Bharathans wife and Siddharths mother, reprises the role she donned in the original film. Lalitha had worked with Siddharth in the short film Kaathu Kaathu which was directed by him.  Vijay Menon, who played the male lead in the original, appears as the psychiatrist in this version. 

===Filming===
The film was jointly produced by  : Kochi, India. Retrieved 11 November 2012.  Principal photography started in December 2011 in Chalakudy, Kerala. Aranumula was initially selected as the major locale but was later changed.  Besides Chalakudy where the film is mainly set, it was shot from different parts of Kerala.    The climax scene of the film was shot from a pond in Chalakudy, which housed crocodiles. Siddharth and Rima Kallingal performed a swimming stunt in the pond, with the latter not even knowing how to swim. Rima was told about this scene a year before the shoot by Siddharth. "I didnt learn swimming, but I was mentally preparing myself to jump into the river," says Rima.  Sameer Thahir wielded camera for the film.  

About the cinematography, Siddharth says, "The cameraman is the directors eye and Sameer and I tried to capture interesting frames out of the sheer love for cinema. There was no ulterior motive of receiving accolades or anything; it all just fell into place." Nidra completed its production in January 2012, at a budget about 20 per cent less than the initially fixed amount.  The films producer Sadanandan says, "I have come across budget overshoots but have decided that if am not satisfied with a particular person, I won’t do another project with that person. As producers, that is our right, as it is our blood and toil that go into making a movie! Nidra is a case in point, where careful and astute budgeting has reduced the costs because of control from our production team. If a producer remains vigilant and reaps profits that can mean more movies in the market and no producer is here to do only one film." 

===Music===
The background score was composed by Prashant Pillai, a former associate of A. R. Rahman who has carved a niche for himself by scoring critically acclaimed soundtracks for all of Lijo Jose Pellisserys films.  The songs are composed by Jassie Gift who has included slow-paced and melodious compositions.  Poet and lyricist Rafeeq Ahamed penned the lyrics for the songs.

==Reception==

===Critical===
The film received positive reviews. Veeyen of Nowrunning.com rated it   and stated" "Sidharth Bharathans Nidra could very well qualify as a complicated mood piece that is passionately charged with raw emotions. It tells a daymare of a story in which the viewer is forced to indulge in a free fall plunge into a world where madness and mayhem collaborate with the most central of all human emotions - love." The critic concluded the review saying, "As a tribute, this remake is perhaps the best that a son can offer to his legendary filmmaker dad! Bharathan, Im sure would have been proud."  Paresh C Palicha of Rediff.com rated the film   and commented that it "gives you a  contemporary feel but it lacks the emotional tug of the films of this nature made in the past." However, he appreciated Siddharth for providing a contemporary feeling. The critic wrote, "Siddharath makes this story contemporary by introducing a philosophical angledebating the difference between sanity and insanity, the concept of reality, and even touching on environmental concerns."  Sify.coms critic gave a verdict of "Above Average" and said, "Its one film that should be appreciated for its inherent sincerity." The critic, however, notes that Nidra "gives the feeling that it is a bit too fast paced than one would have liked and events happen, at times in an undisciplined manner." Siddharths and Rimas performances have been labelled as "the best performances in their careers", Sameer Thahirs visuals as "topnotch", Prashant Pillai’s background score as "excellent" and Jassie Gifts tunes as melodious. The performances by the supporting cast including Jishnu, KPAC Lalitha, Thalaivasal Vijay and Sarayu were described as "impressive".  

Neil Xavier of Yentha.com gave a rating of   and said, Nidra is a "well-crafted movie" and "is a visual treat and comes out as an emotionally rich and touching love story". Siddharths acting is described as "one of the best performances of recent times" while the performances by Rima and Jishnu were also lauded.  N. P. Sajish, in his review for Madhyamam, was mostly critical and stated that the new version is much inferior compared to the original which he regards as a "classic". He adds that Siddharth fails to bring any reinterpretation to the original film. 

===Commercial===
Nidra released on 24 February 2012,  along with the films Ee Adutha Kalathu and Ideal Couple.  It failed to make a mark at the box office despite largely garnering positive reviews. Nidra even had to face holdover threats due to lack of viewer turnout. There were posts on social networking sites asking for media support, but the film turned out to be a commercial failure.  This failure accounts mainly to the lack of promotion and the limited release it had. Sidharth says: "I received a standing ovation throughout Kerala in the first three days from the ardent film-loving junta. Also, I have still been getting praise from the handful who has watched the film. The failure of the film could be due to the films mature subject of derangement, the impression that its a remake and also its non-superstar cast." Siddharth adds that the team had run into few hurdles during the post-production phase due to which they had only limited time to promote the film. 

==See also==
* Mental illness in films

==References==
 

==External links==
*  
*   (in Malayalam)

 
 
 
 
 
 
 