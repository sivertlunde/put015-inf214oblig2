Ju-on: The Grudge 2
{{Infobox film
| name           = Ju-on: The Grudge 2
| image          = Juon2 poster.jpg
| border         = 
| caption        = Theatrical release poster
| film name      = {{Film name
| kanji          = 呪怨2
| kana           = じゅおん2
| romaji         = Juon 2}}
| director       = Takashi Shimizu
| producer       = Shinya Egawa Takashige Ichise Kunio Kawakami Yoshinori Kumazawa Haruhiko Matsushita Hiroki Numata
| writer         = Takashi Shimizu
| starring       = Noriko Sakai Chiharu Niiyama Kei Horie Yui Ichikawa Shingo Katsurayama Emi Yamamoto
| music          = Shiro Sato
| cinematography = Tokusho Kikumura
| editing        = Nobuyuki Takahashi
| studio         = 
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          =
}}
Ju-on: The Grudge 2 is a 2003 Japanese horror film and the fourth installment in the Ju-on (franchise)|Ju-on series. The film was written and directed by Takashi Shimizu. It was released in Japan on August 23, 2003.

The series follows a curse created by a murdered housewife in a house in Nerima, Tokyo|Nerima. The curse falls on anyone who enters the house where the murders took place. Everyone connected to the house since has met a terrible fate. In the film, Keisuke, the director of a popular TV horror show, casts scream queen, Kyoko Harase, as a special guest to an episode set in the Nerima house. The curse begins to set on everyone involved in the filming, including Kyoko herself. Like all films in the series, the plot is told in anachronistic order, with each parts frequently overlapping each other.

== Plot ==
Like the rest of the Ju-on series, the film takes place over a period of time, and is told in a non-linear order as six overlapping vignettes. The overarching plot involves the haunted house of the deceased Saeki family, whose brutal murders caused by Kayako Saeki’s crush on another man led to the creation of a curse. Anyone who enters the house will be cursed and eventually consumed by the ghosts of the Saekis. The vignettes are presented in the following order: Kyoko (京子), Tomoka (朋香), Megumi (恵), Keisuke (圭介), Chiharu (千春), and Kayako (伽椰子).

Film actress Kyoko Harase and her fiancé Masashi Ishikura drive home after Kyoko starred in a paranormal television show called “Heart Stopping Backgrounds”. Kyoko is pregnant and expecting soon. Masashi stops the car after running over a black cat but they continue on, only for the car to crash when the ghost of Toshio Saeki appears. Masashi falls into a coma and Kyoko has a miscarriage. Kyoko encounters Toshio again who touches her stomach before disappearing, and she tells her mother that he must have been the spirit of her lost child. Three months later, Kyoko has returned to acting but is shocked when a doctor announces she is pregnant again. Her mother also suddenly dies.

The cause of her pregnancy is revealed in the other vignettes. Kyoko joins the film crew to record the episode of Heart Stopping Backgrounds at the Saeki house in Nerima, Tokyo. The crew includes director Keisuke Okuni, ambitious host Tomoka Miura, and hair stylist Megumi Obayashi. The filming goes well aside from some technical glitches caused by Kayako, and a strange black stain in one of the rooms which grows bigger when Megumi revisits the room. The crew return to their office, where Keisuke fails to notice some supernatural phenomena on the recordd footage. Megumi attends to several wigs in a dressing room but finds one astray in a changing area. Megumi is suddenly pinned to the floor unable to move, and a flashback reveals she is mirroring a near-dead Kayako who created the stain in the house. The wig transforms into Kayako and kills Megumi.

Before and after filming the episode, Tomoka hears disturbing noises in her apartment, namely something banging hard against the wall every night at 12:27am. Her boyfriend Noritaka Yamashita also hears the noises and goes to Tomoka’s apartment on the night of the filming. When Tomoka arrives home, she finds he has been hung from the ceiling by a curtain of black hair and is being pushed against the wall by Toshio. Kayako descends and hangs Tomoka too, who dies exactly at 12:27am. In the present, Masashi comes out of his coma, though confined to a wheelchair, mute, and seems to react badly at Kyoko’s new pregnancy. Keisuke appears and tells Kyoko all of the film crew save them have died or disappeared.

Keisuke drives Kyoko home but they spot Megumi disappearing into the latter’s house. They explore inside, Keisuke encountering Megumi’s ghost who offers him Kayakos diary before vanishing. Keisuke suspects Kyoko surviving the car crash was deliberate and makes plans to return to the Nerima house the next day. Kyoko and Keisuke are haunted by Kayako and Megumi’s ghosts, until Kyoko returns to the house. There, she encounters a teenage girl Chiharu trying to escape the house, before Kyoko goes into contractions when Kayako’s ghost appears to claim Chiharu. In her own vignette, Chiharu, who appeared in Ju-on: The Grudge, keeps phasing in and out of time back and from the house until she dies in the arms of her friend Hiromi, and claimed by the curse.

Keisuke arrives at the house, witnessing Chiharu being trapped inside, but only finds an unconscious Kyoko inside. She is rushed to hospital and goes into delivery, giving birth, only for pandemonium to occur – Masashi is implied to throw himself off the hospital roof, Toshio appears during the delivery and repeats “Mum!” over and over, and the delivery team all die of fright. Keisuke enters the delivery room, only to witness Kayako crawl her way out of Kyoko’s body and kill him. Kyoko awakens some time later and finds her newborn baby waiting for her, which she embraces.

A few years later, a young boy in Nerima encounters a worn out Kyoko with her daughter, who resembles Kayako. Kyoko is pushed off the bridge by her malevolent daughter, who departs with Kayakos diary in tow, leaving Kyoko to die.

==Cast==
* Noriko Sakai as Kyoko Harase
* Chiharu Niiyama as Tomoka Miura
* Kei Horie as Noritaka Yamashita
* Yui Ichikawa as Chiharu
* Ayumu Saito as Masashi Ishikura
* Emi Yamamoto as Megumi Obayashi
* Erika Kuroishi as Hiromi
* Kaoru Mizuki as Aki Harase
* Shinobu Yuki as Kaoru Ishikura
* Takako Fuji as Kayako Saeki
* Yuya Ozeki as Toshio Saeki
* Shingo Katsurayama as Keisuke Okuni

==DVD==
The DVD was released on January 18, 2005.

The PAL version is a two-disc affair, featuring many extras, such as trailers, premiere footage, the making of, behind-the-scenes footage, interviews, deleted/extended scenes, an audio commentary, and a DVD-ROM game (a flash version of this game is also available).

==References==
 
 
*  

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 