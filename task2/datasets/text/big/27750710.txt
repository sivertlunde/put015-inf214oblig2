H6: Diary of a Serial Killer
{{Infobox film
| name           = H6: Diary of a Serial Killer
| image          = H6- Diario de un asesino.jpg
| caption        = 
| director       = Martín Garrido Barón	 
| producer       = Mark Albela Denise ODell
| writer         = Martín Garrido 
| starring       = Raquel Arenas Alejo Sauras Antonio Mayans
| music          = Gaby Jamieson José Sánchez-Sanz
| cinematography = Sergio Delgado
| editing        = Samuel Gómez
| studio         = Kanzaman
| distributor    = Aurum Producciones Kanzaman
| released       = 29 April 2005 
| runtime        = 92 minutes
| country        = Spain Spanish
| budget         =  € 1.2 Million
| gross         =
| preceded_by    =
| followed_by    =
}}
H6: Diary of a Serial Killer  is a 2006 Spanish horror film directed by Martín Garrido Barón and was written by Martín Garrido. 

==Plot==
H6 tells the story of Antonio Frau, a serial killer set free after serving 25 years in jail for the violent murder of his girlfriend. After inheriting an old motel from a relative he never knew, he sees this as a signal and takes to his holy task of relieving the grief of those who have lost the will to live. He takes his victims to room Number 6 in the motel where he purifies them, while, at the same time, continues his everyday life next to his wife. A mistake leads to his arrest, and his plan to become rich and famous takes relevance.

==Cast==
* Fernando Acaso as Antonio Frau
* Ángel Alarcón	as Franciscas Father
* Raquel Arenas as Rosa
* Ruperto Ares as Pablo
* María José Bausá as Francisca
* Ramón Del Pomar as Curro
* Miquel Fernandez as Antonio Frau
* Martín Garrido as Miguel Oliver
* Antonio Mayans as Dr. Planas
* Sonia Moreno as Tina
* Xènia Reguant as Marisa
* Mark San Juan	as Peralta
* Alejo Sauras as Cristóbal
* Elena Seguí as Soledad Mendez
* Miquel Sitjar as Flores

==Release==
The film premiered on 29 April 2005 as part of the Málaga Film Festival.  It was on 3 November 2005 part of the San Sebastián Horror and Fantasy Film Festival and the renomated Festival de Cine Negro de Manresa on 19 November 2005.  H6 had a theatrical release in Spain on 7 July 2006 and was in the United States released as direct to video production on 21 November 2006. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 