The Magnificent Concubine
 
 
{{Infobox film
| name           = The Magnificent Concubine
| image          =
| caption        =
| film name = {{Film name|traditional=楊貴妃
 |simplified=杨贵妃 pinyin = Yáng Guìfēi jyutping = Joeng 4  Gwai 3 fei 1 }}
| director       = Li Han Hsiang
| producer       = Doven Chow Runme Shaw
| writer         =
| starring       = Hsin Yen Chao
| music          =
| cinematography = Tadashi Nishimoto
| editing        = Hsing-lung Chiang
| studio         = Shaw Brothers Buena Vista Distribution  (United States) 
| released       =  
| runtime        =
| country        = Hong Kong
| language       = Mandarin
| budget         =
}}

The Magnificent Concubine ( ) is a 1962 Hong Kong drama film in color directed by Li Han Hsiang and is a remake of the Kenji Mizoguchi film Princess Yang Kwei-Fei|Yōkihi (1955). The Magnificent Concubine was entered into the 1962 Cannes Film Festival.    It was the first Chinese-language film to win the Grand Prix for Best Interior Photography and Color.  

==Cast==
* Hsin Yen Chao
* Wen Shin Chen Bin He
* Wen Chung Ku
* Hsiang Chun Li
* Li Hua Li
* Ching Lin
* Ying Li
* Ti Tang
* Chih-Ching Yang
* Chun Yen
* Lei Zhao
* Mu Zhu as Rebellious captain

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 