Daughters of the Dust
{{Infobox film
| name           = Daughters of the Dust
| image          = DaughtersOfTheDustDVD.jpg
| image_size     =
| caption        = DVD cover
| director       = Julie Dash
| producer       = Lindsay Law Julie Dash Arthur Jafa Steven Jones
| writer         = Julie Dash
| narrator       =
| starring       = Cora Lee Day Barbara O Alva Rogers Trula Hoosier Umar Abdurrahamn Adisa Anderson Kaycee Moore
| music          = John Barnes
| cinematography = Arthur Jafa
| editing        = Kino International
| released       =  
| runtime        =
| country        = United States English
| budget         =
}}
Daughters of the Dust is a 1991 independent film written, directed and produced by Julie Dash; it is the first feature film by an African-American woman distributed theatrically in the United States. It tells the story of three generations of Gullah women in the Peazant family on Saint Helena Island (South Carolina)|St. Helena Island in 1902, as they prepare to migrate to the North.
 narrative device, the film is told by the Unborn Child. Ancestors are part of the movie, as the Peazant family has lived on the island since their first people were brought as slaves centuries before. The movie gained critical praise, for its rich language and use of song, and lyrical use of visual imagery. It won awards at the Sundance Film Festival and others.

The film features Cora Lee Day, Alva Rogers, Barbara-O, Trula Hoosier, Vertamae Grosvenor, and Kaycee Moore. It was filmed on Saint Helena Island in South Carolina.

In 2004, Daughters of the Dust was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

Dash has published two books related to the film: Daughters of the Dust: The Making of an African-American Womans Film (1992), which includes the screenplay; and Daughters of the Dust: A Novel (1997), set 20 years after the events in the film.

==Development== Sea Island home to a new life in the North. It was inspired by her fathers family, who were Gullah and had migrated to New York. As she developed it over 10 years, she added layers of meaning and clarified her artistic vision. Together with Arthur Jafa, her cinematographer and co-producer, she put together a short film to use for marketing.

She was initially rejected by Hollywood executives, as this was her first full-length film. Dash said they thought it was "too different". She thought their reaction was part of a systematic exclusion of black women from Hollywood. Persisting, Dash finally got financing from PBS American Showcase. Her work is the first feature film by an African-American woman to be distributed theatrically in the United States. 

==Synopsis==
Daughters of the Dust is set in 1902 among the members of the Peazant family, Gullah who live at  , Yoruba people|Yoruba, Kikongo, Mende people|Mende, and Twi origin.  Developed in their relative isolation of large plantations on the islands, the slaves unique culture and language have endured in areas of the Low Country. The Peazant family, including a couple of contrasting daughters who have come back for a last dinner on the island, is meeting before most leave for the North. The film is narrated by the Unborn Child, and is influenced by accounts of ancestors, represented especially by Nana Peazant, the matriarch. She says, "We are two people in one body. The last of the old and the first of the new." Lyrical visual images convey much of the story. The dialogue is in Gullah creole. 

==Characters in Daughters of the Dust==

* Nana Peazant (played by Cora Lee Day): Matriarch of the Peazant family, determined to stay on the island.
* Eli Peazant (Adisa Anderson): Nanas grandson, torn between traveling north and staying on the island.
* Eula Peazant (Alva Rogers): Elis wife, who was raped by a Caucasian on the mainland and is now pregnant.
* Unborn Child (Kay-Lynn Warren): The spirit of Eulas unborn child, who is in fact Elis daughter, narrates much of the film and magically appears in some scenes before her birth, as a young girl.
* Haagar Peazant (Kaycee Moore): Nanas strong-willed daughter-in-law, who is leading the migration north.
* Viola Peazant (Cheryl Lynn Bruce): Another of Nanas daughters, she has already moved to Philadelphia and become a fervent Christian.
* Mr. Snead (Tommy Hicks): A photographer from Philadelphia, engaged by Viola to document the familys life on the island before they leave it for the north.
* Iona Peazant (Bahni Turpin): Haagars daughter, in love with a Native American who will not leave the island, St. Julien Lastchild (M. Cochise Anderson).
* Yellow Mary (Barbara-O): Another of Nanas daughters, she returns from the city with her lover, Trula (Trula Hoosier) for a final visit to the island and her family.
* Bilal Muhammad (Umar Abdurrahman): A practicing Muslim, and a pillar of the island community.
* "Daddy Mac" Peazant (Cornell Royal): Patriarch of the family. 

==Production==
In 1988, Dash secured funding from the PBS series, American Playhouse, and could begin work. She cast a number of veterans of black independent cinema in various roles, as a tribute to the work they had done and the sacrifices they had made, along with a mainly African-American crew. For the sake of authenticity and poetry, she used Gullah dialect in the film. Ronald Daise, author of Reminiscences of Sea Island Heritage (1987), was the dialect coach for her actors (none of whom knew Gullah). She chose not to use subtitles, preferring to have audiences be immersed in the language.

The film was shot in 28 days. Considering she had cast principal actors who were union members, as well as hired union technicians, her budget of $800,000 was very small. They filmed on St. Helena Island and Hunting Island, off the South Carolina coast.  Post-production began in January 1990 and took nearly through the end of 1991.  , Note for Cascade African Film Festival, 2005, faculty spot hosted at Portland Community College  

==Reception==
Daughters of the Dust opened in January 1992 to critical acclaim. The Boston Globe called it "mesmerizing"; the Atlanta Constitution described it as "poetry in motion"; and the Village Voice said that it was "an unprecedented achievement."  At a 2005 festival showing, Michael Dembrows program notes said  that it "explores the strands of West African and African-American experience and ties them into a cultural and spiritual knot, at once graceful, sturdy, and persevering." 

The critic Roger Ebert wrote of the use of Gullah creole, 
 "The fact that some of the dialogue is deliberately difficult is not frustrating, but comforting; we relax like children at a family picnic, not understanding everything, but feeling at home with the expression of it."  

Audiences were moved by the film; one woman told New York Magazine, 
 "Its hard to explain. It makes you feel connected to all those before you that you never knew, to parents and grandparents and great-grandparents. Im a different person now from seeing this movie. Its a rejuvenation, a catharsis. Whatever color you are, people want to feel that sense of belonging."  

==Awards==
*1991, Sundance Film Festival - it won the Cinematography Award and was nominated for the Grand Jury Prize. 
*2004, selected for the National Film Registry of the Library of Congress
*2005, Excellence in Cinematography Award of the 15th Cascade Festival of African Films, Portland, Oregon. 

==Related books==
*With Toni Cade Bambara and bell hooks, Dash wrote Daughters of the Dust: The Making of an African American Womans Film (1992). The book includes the screenplay.
 Charleston County Public Librarys "One Book Program".  , Charleston City Paper, 14 September 2011 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 