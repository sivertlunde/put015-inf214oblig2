The Pirate Movie
 
 
 
{{Infobox film
| name = The Pirate Movie
| image = Pirate_Movie.jpg
| alt =
| caption = Theatrical release poster
| director = Ken Annakin
| producer = David Joseph
| writer = Trevor Farrant
| based on =  
| starring = {{plainlist|
* Kristy McNichol
* Christopher Atkins
* Ted Hamilton
* Bill Kerr
* Garry McDonald
* Maggie Kirkpatrick
}}
| music = {{Plainlist| Mike Brady
* Songs:
* Terry Britten
* Sue Shifrin}}
| cinematography = Robin Copping
| editing = Kenneth W. Zemke
| studio = Joseph Hamilton International Productions
| distributor = 20th Century Fox
| released =  
| runtime = 104 minutes  
| country = Australia
| language = English
| budget = Australian dollar|A$6 million 
| gross = $9 million
}} musical romantic music score Mike Brady and Peter Sullivan (no relation to Pirates of Penzance composer Arthur Sullivan).

The film performed far below expectations when first released and is generally reviewed very poorly.  

==Plot==
Mabel Stanley (Kristy McNichol) is an introverted teenage girl yearning for popularity in a seaside community in Australia. She attends a local pirate festival featuring a swordplay demonstration led by a young curly-haired instructor (Christopher Atkins), who then invites her for a ride on his boat. She is duped by her acquaintances, Edith (Kate Ferguson), Kate (Rhonda Burchmore), and Isabel (Catherine Lynch), into missing the launch, so she rents a small sailboat to give chase. A sudden storm throws her overboard, and she washes up on a beach. She subsequently dreams an adventure that takes place a century before. In this fantasy sequence, the swordplay instructor is now named Frederic, a young apprentice of the Pirates of Penzance, celebrating his 21st birthday on a pirate vessel. Frederic refuses an invitation from the Pirate King (Ted Hamilton), his adoptive father, to become a full pirate, as his birth parents were murdered by their contemporaries. Frederic swears to avenge their deaths and is forced off of the ship on a small boat.

Adrift, Frederic spies Mabel and her older sisters on a nearby island and swims to shore to greet them. In a reversal of roles, Mabel is a confident, assertive, and courageous young woman, while her sisters are prim, proper and conservative. Frederic quickly falls for Mabel and proposes marriage, but local custom requires the elder sisters to marry first. Soon, Frederics old mates come ashore, also looking for women and kidnap Mabels sisters. Major-General Stanley (Bill Kerr), Mabels father, arrives and convinces the Pirate King to free his daughters and leave in peace. The pirates anchor their ship just outside the harbour instead of actually leaving. Mabel wants Frederic to gain favour with her father so they can marry, so she plots to recover the family treasure stolen years earlier by the pirates. Unfortunately, the treasure was lost at sea, but the location where it lies was tattooed as a map on the Pirate Kings back. Mabel successfully tricks the Pirate King into revealing his tattoo while Frederic sketches a copy.
 29 February, and he is dismayed to see that the contract specifies his twenty-first birthday, rather than his twenty-first year. As his birthday occurs every four years, Frederic has celebrated only five birthdays and is still bound by contract to remain with the pirates.

That night, the pirates raid the Stanley estate, and the Pirate King orders their execution. Mabel demands a "happy ending" – admitting for the first time that she believes this all to be a dream. Everyone — even the pirates — cheers their approval, leaving the Pirate King disappointed and shocked. Mabel then confronts her father, but the Major-General is steadfast that the marriage custom remains in effect. Mabel quickly pairs each of her older sisters with a pirate, and she also pairs the Pirate King to Ruth, the ship nurse. With Mabel and Frederic now free to marry, the fantasy sequence ends in song and dance.

Mabel awakens back on the beach to discover that she is wearing the wedding ring that Frederic had given her in her dream. At that moment, the handsome swordplay instructor arrives and lifts her to her feet. He passionately kisses Mabel, who is still shaken by her dream. She asks if his name is Frederic. He assures her that he isnt who she imagines him to be, but then carries her off to marry her, thus giving Mabel her happy ending in reality as well.

==Cast==
* Christopher Atkins as Swordplay instructor/Frederic
* Kristy McNichol as Mabel Stanley
* Ted Hamilton as The Pirate King
* Bill Kerr as Major-General Stanley
* Maggie Kirkpatrick as Ruth, the ship nurse
* Garry McDonald as Sergeant/Inspector
* Chuck McKinney as Samuel
* Kate Ferguson as Edith
* Rhonda Burchmore as Kate
* Catherine Lynch as Isabel

==Production== film version of his Broadway production of The Pirates of Penzance.

The film was shot at the Polly Woodside at South Melbourne wharf, the Farm and Mansion at Werribee Park and Loch Ard on the Great Ocean Road Port Campbell.

==Soundtrack==
Polydor Records released a soundtrack album and singles from the film.
: A1 – "Victory" – The Pirates (2:37)
: A2 – "First Love" – Kristy McNichol and Christopher Atkins (4:13)
: A3 – "How Can I Live Without Her" – Christopher Atkins (3:08)
: A4 – "Hold On" – Kristy McNichol (3:14)
: A5 – "We Are the Pirates" – Ian Mason (3:36)
: B1 – "Pumpin and Blowin" – Kristy McNichol  (3:05)
: B2 – "Stand Up and Sing" – Kool & The Gang  (4:32)
: B3 – "Happy Ending" – The Peter Cupples Band (4:58)
: B4 – "The Chase" – Peter Sullivan and The Orchestra  (1:33)
: B5 – "I Am a Pirate King" – Ted Hamilton and The Pirates (2:03)
: C1 – "Happy Ending" – The Cast of The Pirate Movie (4:18)
: C2 – "The Chinese Battle" – Peter Sullivan and The Orchestra (2:36)
: C3 – "The Modern Major Generals Song" – Bill Kerr and The Cast of The Pirate Movie (2:00)
: C4 – "We Are the Pirates" – The Pirates (2:18)
: C5 – "Medley" – Peter Sullivan and The Orchestra (4:03)
: D1 – "Tarantara" – Gary McDonald and The Policemen  (1:53)
: D2 – "The Duel" – Peter Sullivan and The Orchestra (4:04)
: D3 – "The Sisters Song" – The Sisters (2:42)
: D4 – "Pirates, Police and Pizza" – Peter Sullivan and The Orchestra (3:32)
: D5 – "Come Friends Who Plough the Sea" – Ted Hamilton and The Pirates (2:00)

==Release==
The Pirate Movie was made soon after the 1980 New York City Central Park and 1981 Broadway theatre production of The Pirates of Penzance produced by Joseph Papp, which re-popularized swashbuckling pirates as a theatrical genre.

===Box office===
The film earned A$1,013,000 at the Australian box office.  In the United States, the film grossed $7,983,086. 

===Critical reception=== Michael and Harry Medveds book Son of Golden Turkey Awards includes The Pirate Movies "First Love" on its list of "Worst Rock N Roll Lyrics in a Movie".  Australian film critic Michael Adams later included The Pirate Movie on his list of the worst ever Australian films, along with Phantom Gold,The Glenrowan Affair,Houseboat Horror, Welcome to Woop Woop,Les Patterson Saves the World and the 1987 film Pandemonium. 

===Accolades===
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards  (24th Australian Film Institute Awards)  AACTA Award Best Supporting Actor Garry McDonald
| 
|- AACTA Award Best Costume Design Aphrodite Kondos
| 
|- Golden Raspberry Razzie Award Golden Raspberry Worst Actor Christopher Atkins
| 
|- Golden Raspberry Worst Actress Kristy McNichol
| 
|- Golden Raspberry Worst Supporting Actor Ted Hamilton
| 
|- Golden Raspberry Worst Original Song  ("Pumpin and Blowin")  Terry Britten, B.A. Robertson & Sue Shifrin
| 
|- Worst Original Song  ("Happy Endings") 
| 
|- Golden Raspberry Worst Musical Score Kit Hain
| 
|- Golden Raspberry Worst Screenplay Trevor Farrant
| 
|- Golden Raspberry Worst Director Ken Annakin
| 
|- Golden Raspberry Worst Picture David Joseph
| 
|- Stinkers Bad Stinkers Bad Movie Award Worst Picture 
| 
|}

The film is listed in Golden Raspberry Award founder John Wilsons book The Official Razzie Movie Guide as one of the The 100 Most Enjoyably Bad Movies Ever Made. 

==References==
 

==External links==
 
*  
*  at Oz Movies
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 