Snow Flower and the Secret Fan (film)
{{Infobox film
| name           = Snow Flower and the Secret Fan
| image          = Snow flower and the secret fan poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Wayne Wang
| producer       = Wendi Murdoch  Florence Sloan
| screenplay     = Angela Workman Ronald Bass Michael K. Ray
| based on       =  
| starring       = Jun Ji-hyun Li Bingbing Vivian Wu Wu Jiang Russell Wong Coco Chiang Hu Jingyun Archie Kao 
| music          = Rachel Portman
| cinematography = Richard Wong
| editing        = Deirdre Slevin
| studio         = IDG China Creative Media Limited Big Feet
| distributor    = Fox Searchlight Pictures   Pinnacle Films  
| released       =  
| runtime        = 104 minutes 
| country        = China United States
| language       = Mandarin Chinese, English, Korean
| budget         = $6 million   
| gross          = $11,348,205 	  
}} historical drama novel of the same name by the author  Lisa See. Directed by Wayne Wang, the film stars Jun Ji-hyun, Li Bingbing, Archie Kao, Vivian Wu, and Hugh Jackman.

Rupert Murdoch personally arranged for the film to be released by Fox Searchlight Pictures,  which opened the film in North America on July 15, 2011.   

==Plot==
In nineteenth century China, two girls both named Snow Flower (Gianna Jun) and Lily (Li Bing Bing) are forever bonded together as sworn sisters. They are paired as laotong by a matchmaker who is also responsible for arranging their marriages. They are isolated by their families and communicate by writing in a secret sisterly language, Nu shu (a historical practice in China in that period) on a unique Chinese fan that Snow Flower possesses. 

Meanwhile, in the present day Shanghai, their descendants Sophia Liao and Nina Wei struggle with the intimacy of their own pure and intense childhood friendship. As teenagers, Sophia and Nina were introduced to the idea of laotong, and they signed a traditional laotong contract on the cover of Canto-pop Faye Wongs album Fu Zao (Restless in English). Faye Wong was their favorite singer and their liberated dancing to the "degenerate" sounds of the cheerful refrain "la cha bor" was one of the reasons Sophias stepmother attempted to separate them. Eventually they are separated but come together again when Sophia falls into a coma after being struck by a taxi while cycling. Reunited at long last, they must come to understand the story of the strong and close ancestral connection hidden from them in the folds of the antique white silk fan or lose one another forever in the process.

==Cast==
* Li Bingbing    as Nina/Lily
* Jun Ji-hyun as Sophia/Snow Flower
* Vivian Wu as Aunt
* Hugh Jackman as Arthur
* Archie Kao as Sebastian
* Wu Jiang as Butcher
* Angela Evans as Ballroom guest
* Jennifer Lim (voice) as Snow Flower
* Christina Jun (voice) as Sophie

==Production==
The film was produced by IDG China Media. The filming locations were Hengdian World Studios, Heng Dian, China, and Shanghai, China with many scenes at The Peninsula Hotel on the The Bund (Shanghai)|Bund.

==Reception==
The film received generally negative reviews from critics. On Rotten Tomatoes, it has a 21% approval rating, based on 87 reviews.    On Metacritic, it has a score of 42 out of a possible 100, based on 31 reviews.   

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 