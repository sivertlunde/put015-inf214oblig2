Ration Bored
{{Infobox Hollywood cartoon|
| cartoon_name        = Ration Bored Woody Woodpecker
| image               = Ration01.jpg
| caption             =
| director         = Emery Hawkins Milt Schaffer (both co-directed)
| story_artist        = Ben Hardaway Bob Bentley Paul J. Smith (uncredited)
| designer  = Emery Hawkins (uncredited)
| background_artist   = Fred Brunish (uncredited)
| voice_actor         = Kent Rogers (uncredited)
| musician            = Darrell Calker
| producer            = Walter Lantz
| studio              = Walter Lantz Productions Universal Pictures
| release_date        = July 26, 1943
| color_process       = Technicolor
| runtime             = 6 49"
| movie_language      = English
| preceded_by         = The Dizzy Acrobat The Barber of Seville
}}
 Universal Pictures.

==Plot==
While driving his car, Woody sees a sign that reads: "Conserve gas & tires. Is this trip really necessary?" Woody refers to himself as a "necessary evil" while changing his appearance briefly into a demonic version of itself with deranged eyes  and speeds down the road after changing back again. While cresting a hill, he runs out of gas and rolls to the gas station below.

The gas attendant asks to see Woodys "ABC" book (see below), and Woody hands him an alphabet book. Insulted, the attendant grabs a hammer and knocks Woodys car into a salvage yard. Woody decides to steal gasoline from the wrecked vehicles in the lot. He unknowingly siphons gas from a parked police car.

A cop chases Woody around the salvage yard. They get caught up in stacks of tires, and Woody ends up riding the cop like an automobile out of the yard and into a large storage unit of gasoline.
 

In heaven, the cop leaves the "Wing Rationing Board" with a small pair of wings. He starts chasing Woody again when he realizes that the wings Woody has received are much larger.

==Woodys appearance== The Barber of Seville, Woodys appearance would get a complete makeover, making Ration Bored the last Woody Woodpecker film featuring the original manic design.

==Production notes== The Barber James "Shamus" Culhane as series director. 

==Cultural references== army rationing. During World War II United States citizens were asked to conserve gasoline and rubber, as well as other items and food supplies. Decisions on rationing were made by a Ration Board, hence the punning title.

The gas station attendant refers to a ration book as an "ABC book". During the 1940s war ration, American automobiles were classified as either A, B, C, T, or X.

The ending title card asks viewers to buy war bonds.

==References==
 

 
 
 
 