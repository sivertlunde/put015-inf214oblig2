For the First Time (1967 film)
{{Infobox film
| name           = For the First Time
| image          = 
| alt            = 
| caption        = 
| director       = Octavio Cortazar
| producer       = El Estudio Cubano del Arte
| writer         = 
| narrator       = 
| starring       = 
| music          = Raul Gomèz
| cinematography = Lopito
| editing        = Caìta Villalon
| distributor    = El Estudio Cubano del Arte
| released       =  
| runtime        = 10 minutes
| country        = Cuba
| language       = Spanish
| budget         = 
}}
 documentary short film.  It chronicles the events of April 12, 1967 in the village of Los Munos, in the Baracoa municipality of Cuba, where a mobile cinema truck shows the villagers moving pictures for the first time.
 Chaplin feature, Modern Times, 16 mm projector in the back of the truck. As it happens, the film screening is well received.
 MK2 DVD release of Modern Times, distributed by Warner Brothers.

==References==
 

==External links==
*  

 
 
 
 
 


 
 