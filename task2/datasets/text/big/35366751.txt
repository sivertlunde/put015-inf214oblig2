Vi hade i alla fall tur med vädret
 
{{Infobox film
| name           = Vi hade i alla fall tur med vädret
| image          = 
| image size     = 
| director       = Kjell Sundvall
| producer       = Sveriges Television
| writer         = Kjell Sundvall   Börje Hansson   Kjell-Åke Andersson
| narrator       = 
| starring       = 
| music          = Örjan Fahlström
| cinematography = Kjell-Åke Andersson
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 57 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}} Swedish TV comedy film directed by Kjell Sundvall. It had premiere in Sveriges Television on 27 February 1980.   In 2008 it was followed by the film Vi hade i alla fall tur med vädret &ndash; igen.

==Plot==
The family Backlund are going on travel trailer vacation. But Gösta doesnt like that Rudolf, the childrens grandpa, has 
been promised to follow them. During the vacation there is always an incident after the other.

==Selected cast==
*Rolf Skoglund as Gösta Backlund
*Claire Wikholm as Gun Backlund
*Charlotte Thomsen as Lotta Backlund
*Johan Öhman as Johan Backlund
*Gunnar Lindkvist as Rudolf, grandpa (Guns father)
*Rune Pettersson as Motorcycle-policeman

==References==
 

==External links==
*  
*  

 
 
 
 
 
 

 