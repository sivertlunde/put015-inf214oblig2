Crying... Silicon Tears
{{Infobox Film
 | name=Crying... Silicon Tears
 | image= 
 | caption=Australian DVD Cover for Crying... Silicon Tears
 | director=Thanasis Papathanasiou   Michalis Reppas
 | writer=Thanasis Papathanasiou   Michalis Reppas 
 | producer=Helena Hatzialexandrou
 | starring=Anna Panayiotopoulou Mirka Papakonstantinou Tasos Halkias Maria Kavoyanni 	Joys Evidi Michalis Reppas Mina Adamaki Alexandros Antonopoulos Mimis Chrisomalis Kostas Evripiotis Sofia Filippidou Takis Hrisikakos Antonis Kafetzopoulos Trifon Karatzas Krateros Katsoulis Vladimiros Kiriakidis Kostas Koklas Elissavet Konstantinidou Anna Kyriakou Renia Louizidou Nena Menti Arietta Moutousi Yorgos Partsalakis Dimitris Piatas Hristos Valavanidis Yannis Zouganelis
 | music=Afrodite Manou
 | cinematography=Kostas Gkikas
 | editing=Ioanna Speliopoulos
 | distributor=Warner Roadshow Distributrors
 | runtime= 101 Greek
 | budget= €1,32 million
 | released=Greece: 26 October 2001
Australia: May 1, 2003
 |
 }}

Crying...Silicon Tears (Orig. To Klama vgike apo ton paradeiso) is a 2001 Greek language film, directed by Thanasis Papathanasiou and Michalis Reppas.

== Plot ==
The movie is divided into three clear timelines, the 1960s, the World War II period and the bucolic period (in order from newest to oldest). In the 1960s, the rich Della Franca family start a fight with the poor Bisbikis family over a man that romances with women from both families. The Della Francas manage to put the mother of the Bisbikis, Lavrentia, out of business, which triggers the World War II flashback, where Lavrentia worked with the Greek Resistance. In that period, another event triggers the bucolic period in the form of Lavrentia talking about her ancestors, a man and a woman that lived in a small village in the countryside. No clear main plot existent in any period, the film mainly revolves around Lavrentias history and the tragic events that stained her life. In the end of the film, it is revealed that the Bisbikis are actually blood-related to the Della Francas and they all live happily ever after.

Although the film features mostly tragic events, they are presented in a way that parodies older movies of the Greek cinema, rendering them hilarious.  It is both a tribute and a satire of old Greek dramas, war movies and bucolic-style films.

== Cast ==
*Anna Panayiotopoulou as Lavrentia Bisbiki
*Mirka Papakonstantinou as Tzella Della Franca
*Mina Adamaki as Billio
*Mimis Chrisomalis as Parnassos
*Joys Evidi as Jenny Della Franca
*Sofia Filippidou as Sirmo
*Tasos Halkias as Mr. Baras
*Antonis Kafetzopoulos as contact
*Trifon Karatzas as Mr. Della Franca
*Krateros Katsoulis as doctor
*Maria Kavoyianni as Martha
*Vladimiros Kiriakidis as German soldier 1
*Kostas Koklas as German soldier 2
*Ketty Konstadinou as Jennys friend
*Anna Kyriakou as Mrs. Baras
*Renia Louizidou as party guest 1
*Nena Menti as party guest 2
*Yorgos Partsalakis as general of the Greek army
*Michalis Reppas as Yiakoumis
*Hristos Simardanis as Denis
*Thodoros Siriotis as judge
*Mary Stavrakeli as Zabeta
*Hristos Valavanidis as Heinrich Von Snitchell
*Yannis Zouganelis as Kavouras

==Quotes==
* Ήξερες πάρα πολύ καλά ότι το πλοίο αυτό δεν έπρεπε να ταξιδέψει! Το Τζέλλα Δελτα είχε πρόβλημα: Δεν είχε φουγάρα! (You knew very well that this ship shouldnt have sailed! The Jella Delta had a problem: She didnt have smokestacks!)

* "I taught her well; until marriage, your man wont grab anything... Alright, some tit, but only on Saturday after the movies."

==External links==
*  
* 

 
 
 
 


 
 