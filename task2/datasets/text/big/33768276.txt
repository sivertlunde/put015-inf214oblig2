Meherjaan
{{Infobox film
| name           = Meherjaan
| image    = Meherjaan film poster.jpg
| alt            =  
| caption        = Theatrical Release Poster
| director       = Rubaiyat Hossain
| producer       = Ashique Mostafa 
| writer         = Rubaiyat Hossain
| screenplay     = Rubaiyat Hossain Ebadur Rahman
| starring       = Jaya Bachchan Victor Banerjee Humayun Faridi
| music          = Neil Mukherjee
| cinematography = Samiran Datta
| editing        = Sujan Mahmud Mita Chakraborty
| studio         = 
| distributor    = Habibur Rahman Khan
| released       =   
| runtime        = 119 minutes
| country        = Bangladesh Bengali English English Urdu Urdu
| budget         = 
| gross          = 
}}
Meherjaan ( ) is the feature-length début film of Bangladeshi director Rubaiyat Hossain. The film was pulled from theatres due to the hostile response of some segments of the audience after its release in January, 2011. Meherjaan claims to be a womens "feminine" re-visiting of the Bangladesh Independence War with Pakistan in 1971, while many feel discomfort with the deconstructive representation of the 71 conflict.

==Cast==
* Jaya Bachchan as Meher
* Victor Banerjee as Khwaja Saheb (Grandfather)
* Omar Rahim as Wasim Khan (Pakistani Soldier)
* Shaina Amin as Young Meher
* Reetu A Sattar as Neela (Birangona/Freedom fighter)
* Azad Abul Kalam Pavel as Shumon (Communist Party worker)
* Humayun Faridi as Khonker (Razakar)
* Sharmili Ahmed as Mehers Mother
* Khairul Alam Sabuj as Mehers Father
* Monira Mithu as Mehers Aunt
* Nasima Selim as Sarah (Warchild)
* Rubaiyat Hossain as Salma
* Ashique Mostafa as Shimul (Freedom fighter)
* Shatabdi Wadud as Khalil 
* Iqbal Sultan as Major Baset (Pakistani Major)
* Rifat Chowdhury as Arup
* Arup Rahee as Rahee
* Rajeev Ahmed as Sami
* Tansina Shawan as Joba (Freedom Fighter)

==Festivals and awards== Festival International de Films de Fribourg,  Festival de Cine de Bogotá,  31 º Festival Cinematográfico Internacional del Uruguay,  London Asian Film Festival,  Osians Cinefan Festival of Asian and Arab Cinema|Osians Cinefan Festival of Asian & Arab Cinema,  Jaipur International Film Festival,  Cyprus International Film Festival,  Fort Lauderdale International Film Festival,  Belize International Film Festival,  Portobello Film Festival,  Baghdad International Film Festival,  Indian Film Festival The Hague,  Arizona International Film Festival,  Kenya International Film Festival,  Oaxaca Film Fest,  Ladakh International Film Festival, Seattle South Asian Film Festival,  International Film Festival Antigua Barbuda,  Bahamas International Film Festival  and others.
 Bare Bones Moondance Film AOF Int. NJ Independent New Jersey Int. Film Festival) and others

==Suspension==

The film was withdrawn from movie theatres in Bangladesh due to the objections of different groups of people. "The film Meherjaan, which was released in Dhaka in January 2011, was quickly pulled out of theatres after it created a furore among audiences. The hostile responses to the film from across generations highlight the discomfort about the portrayal of a raped woman, and its depiction of female and multiple sexualities during the Bangladesh Liberation War of 1971.  The films stance against Bangladeshi nationalism also created a stir among audiences." 

On November 3, 2011, there was a special film event and a panel discussion at Harvard University sponsored by the  University of Massachusetts Boston, South Asia Initiative at Harvard University, Consortium on Gender, Security and Human Rights at UMass, and the CARR Center for Human Rights, Harvard Kennedy School. The film was screened in advance of a panel discussion by Cambridge/Boston academics.  

==Reviews==
Meherjaan has received mixed reviews.          

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 