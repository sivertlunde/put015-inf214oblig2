Splinterheads
{{Infobox Film
| name           = Splinterheads
| image          = Splinterheads.jpg
| caption        = Theatrical release poster
| director       = Brant Sersen
| producer       = Darren Goldberg Christopher Marsh Anisa Qureshi
| writer         = Brant Sersen
| starring       = Thomas Middleditch Rachel Taylor Christopher McDonald Lea Thompson Dean Winters Frankie Faison Jason Rogel
| music          = John Swihart
| cinematography = Michael Simmonds
| editing        = Chris Lechler
| distributor    = Paladin
| released       = November 6, 2009
| runtime        = 94 minutes
| country        = United States
| language       = English
}}
Splinterheads is a 2009 romantic comedy film written and directed by Brant Sersen.  It stars Thomas Middleditch, Rachael Taylor, Christopher McDonald and Lea Thompson.  The film opened in limited release in the United States on November 6, 2009.

The film is about a young man, Justin Frost (Middleditch), who falls in love with Galaxy (Taylor), a splinterhead (someone who works at a carnival but is not a carny). The two go on their share of adventures and in the end are a couple.

Schuylar Croom of He Is Legend makes a cameo in the film as the Might As Well Jump guy.

 
 
 


 