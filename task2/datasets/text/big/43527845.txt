Beyond Iconic: Photographer Dennis Stock
{{Infobox film
| name           = Beyond Iconic: Photographer Dennis Stock
| image          = Beyond Iconic film poster.jpg
| alt            = 
| caption        = Film poster
| director       = Hanna Sawka Hamaguchi 
| producer       = Hanna Sawka Hamaguchi 	
| writer         = Hanna Sawka Hamaguchi 
| screenplay     = 
| starring       =
| music          = John Menegon Teri Roiger
| cinematography = Mateusz Broughton
| editing        = Hanna Sawka Hamaguchi 
| studio         = Peppercat Productions
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
}}
Beyond Iconic: Photographer Dennis Stock is a 2011 American biographical documentary film, written, produced and directed by Hanna Sawka Hamaguchi.    The film chronicles the personal and professional life of photographer Dennis Stock. It is narrated by Stock himself and was completed before his death in 2010. The film shone light on Stocks different aspects of life including his classroom at the Omega Institute, where he taught a master class of photography before his death. 
 34th Denver Film Festival and 2014 Visions du Réel. 

==Synopsis==
The film explore the work and life of one the late 20th century most influential photographer in his own words and through hundreds of his famous photographs.

==Reception==
The film received mostly positive review from the critics. Lauren Wissot in her review for   praised the direction by saying that "Hamaguchi smartly cuts from Stock’s master class to direct interviews with the no-nonsense product of The Bronx (“The more you rationalize bad pictures the further you get from taking good pictures,” he advises his students), juxtaposed with Stock narrating the stories behind many of his legendary stills, including the shot of a pre-iconic James Dean navigating Times Square in the rain. Stock comes off as a brilliant teacher – both brutally honest and sensitive, and able to clearly articulate what works in a photo, what doesn’t, and why."  

Colorado Photographic Arts Center in their review praised Hamaguchi that "Her film balances Stock’s quiet, contemplative life with his wife and pets against his worldly, vibrant, and yet somehow equally intimate portraits of actors, musicians, and a generation of iconoclasts struggling to find meaning in an increasingly pop culture–driven America." 

Ann Hutton of Almanac gave film a positive review by saying that "Stock’s artistic philosophy, his commitment to integrity of purpose and his ability to see the story in a picture are expertly depicted in this unnarrated treatment." She further added that "Stock’s work is much more than just a bio in photographs, as Sawka, who grew up in High Falls and has four award-winning short films to her credit, gets inside the artist, goes beyond the “iconic” and finds the man behind the beautiful and evocative pictures." 

Mark Antonation, in his review for the Starz Denver Film Festival said that "The director balances Stock’s quiet, contemplative life with his wife and pets against his worldly, vibrant, and yet somehow equally intimate portraits of actors, musicians, and a generation of iconoclasts struggling to find meaning in an increasingly pop culture–driven America. While his pictures need no explanation, Hamaguchi allows Stock’s belief in his work to echo with the same eloquence, clarity, and confidence they emanate." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 