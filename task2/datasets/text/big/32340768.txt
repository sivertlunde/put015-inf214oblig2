Comic Costume Race
{{Infobox film
| name           = Comic Costume Race
| image          = ComicCostumeRace.jpg
| image_size     = 
| caption        = Screenshot from the film
| director       = Robert W. Paul
| producer       = Robert W. Paul
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography = Robert W. Paul
| editing        =
| studio         = Pauls Animatograph Works
| distributor    =
| released       =  
| runtime        = 43 secs
| country        = United Kingdom Silent
| budget         =
}}
 1896 UK|British short black-and-white silent actuality film, directed by Robert W. Paul, featuring comic costume scramble at the Music Hall Sports on 14 July 1896 at Herne Hill, London. The music hall sports day was an annual charity event consisting of other events such as egg and spoon races and three-legged races. The film is the best surviving pictorial record of the Music Hall Sports.    It is not known who the race participants are.   

The film was, according to Michael Brooke of BFI Screenonline, "presented at Windsor Castle on 23 November 1896," which, "enabled Paul to add a royal seal of approval to his advertisements." It is included on the BFI DVD R.W. Paul: The Collected Films 1895-1908.      

Another film of the costume race at the Music Hall Sports was produced in 1898.  In 1899, Cecil Hepworth produced a film on a similar race, entitled Comic Costume Race for Cyclists. This film depicted a group of cyclists racing to a pile of clothing, containing costumes such as those of a policeman and a clown, before remounting and racing to the finishing line. 

==References==
 

==External links==
* 

 
 
 

 
 