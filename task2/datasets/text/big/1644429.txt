Masculin Féminin
{{Infobox film
| name           = Masculin Féminin
| image          = Masculinféminin.jpg
| caption        =
| director       = Jean-Luc Godard
| writer         = Jean-Luc Godard
| starring       = Jean-Pierre Léaud Chantal Goya Marlène Jobert Michel Debord
| producer       = Anatole Dauman
| music          = Jean-Jacques Debout
| cinematography = Willy Kurant
| editing        = Agnès Guillemot
| studio         = Anouchka Films-Argos Films Sandrews-Svenskfilmindustri
| distributor    = Columbia Films S.A.
| released       =  
| runtime        = 103 minutes
| country        = France French
|gross = 427,430 admissions (France) 
}}
Masculin Féminin ( ,  , " . It stars Jean-Pierre Léaud, Chantal Goya, Marlène Jobert, Catherine-Isabelle Duport and Michel Debord. 
 LeRoi Jones’ Marx and Coca-Cola", which is actually an intertitle between chapters.

==Plot==
The film stars Jean-Pierre Léaud as Paul, a romantic young idealist and literary lion-wannabe who chases budding pop star, Madeleine (Chantal Goya, a real life Yé-yé girl). Despite markedly different musical tastes and political leanings, the two soon become romantically involved and begin a ménage à quatre with Madeleines two roommates, Catherine (Catherine-Isabelle Duport) and Elisabeth (Marlène Jobert). The camera probes the young actors in a series of Cinéma vérité|vérité-style interviews about love, love-making, and politics. 

==Cast==
* Jean-Pierre Léaud as Paul, a young idealist 
* Chantal Goya as Madeleine Zimmer, a young singer
* Marlène Jobert as Elisabeth Choquet, Madeleines roommate
* Michel Debord as Robert Packard, a journalist
* Catherine-Isabelle Duport as Catherine-Isabelle
* Brigitte Bardot as herself (cameo)
* Antoine Bourseiller as himself (cameo)

==Background==
In 1965 Anatole Dauman, the head of Argos Films, wanted to re-edit and re-release Alexandre Astrucs 1952 44 minute film The Crimson Curtain. He decided that he also wanted another medium length film to accompany Astrucs film and offered the project to Godard, suggesting that Godard adapt Guy de Maupassants short story The Signal. Godard had been interested in filming The Signal for several years and agreed to the project. Eventually Dauman suggested that Godard also adapt Maupassants short story Pauls Mistress and secured the rights to both short stories. When filming began, Godard discarded both Maupassant short stories and Maupassants publishers later agreed that the film was in no way an adaptation of the authors work. The only parts of either short stories that appear in the film is the fact that the main characters name is Paul and the "film within the film" that the main characters go to see at a movie theater was initially inspired by "The Signal". 

Godard did not have a shooting script and instead relied on a hand written spiral notebook filled with ideas, sketches and dialogue written the night before.  Godard was interested in working with singer Chantal Goya because she was neither a film or stage actress and was introduced to her by Daniel Filipacchi on November 7, 1965.   Shooting began on November 22, 1965. Godard used natural lighting and a minimal crew throughout the production. 

==Reception==
Due to the portrayal of youth and sex, the film was prohibited to persons under 18 in France — "the very audience it was meant for," griped Godard.   

Reviews were mixed in both France and in the U.S.   called it "the film of the season."  Judith Crist said that it had "flashes of original wit and contemporary perceptions."  Bosley Crowther disliked the film and called it "entertainment of only the most loose and spotty sort."  Gene Moskowitz called it "naive and knowing, irratating and engaging." 

==Awards==
Jean-Pierre Léaud won the Silver Bear for Best Actor at the 16th Berlin International Film Festival.   
 Berlin Film Festival the film won an award for the years best film for young people. 

== See also ==
*List of French language films
*Yé-yé

==References==
 

==External links==
*  
*  
*  
*  
* 

 

 
 
 
 
 
 
 