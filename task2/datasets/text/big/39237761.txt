When the Girls Take Over
{{Infobox film
| name           = When the Girls Take Over
| image          = 
| caption        = Original film poster
| director       = Russell Hayden
| producer       = Russell Hayden
| writer         = Samuel Roeca  Marvin Miller Robert Lowery John Conte Howard Jackson (conductor)
| cinematography = Arthur E. Arling
| editing        = 
| studio         = Trans-Oceanic Productions
| distributor    = Parade Releasing Organization 
| released       = May 1962
| runtime        = 79 min.
| country        = United States English
}}
 James Ellison who partnered with Hayden in a series of Westerns.  It was filmed in Puerto Rico in 1960 under the working title Caribe and released in 1962.   The film was shot in colour but the public domain copy is in black and white.

==Plot==
The film begins with shots of the United Nations Headquarters in New York City as a narrator quotes Genesis Chapter 11 about the Tower of Babel. The scene switches inside the UN with the delegate from the Republic of Hondo Rico being interrupted by Nikita Khrushchev recreating his shoe-banging incident. The Hondo Rican delegate revenges himself by stalking Khrushchev to an empty hallway and knocking him out with a shoe.

The easy going Republic of Hondo Rico is a former French colony in the Caribbean menaced by an insurgency led by a Fidel Castro type bearded revolutionary general named Maximo Toro (translation: a lot of bull). The main impediment to Maximos plan is lack of money and weapons. 
 Catalina flying boat named "Caribe" and a local mechanic named Razmo.
 Kadet M1903 drill rifles). Henri and Toussaint beat the slow flying Catalina back to Hondo Rico and steal several crates of weapons from the Hondo Rican army.  Taking the weapons by boat they panic when they arrive at Maximos basecamp dumping the crates into the ocean just offshore of the guerilla base.

Knowing the two American soldiers of fortune have diving gear, he enlists their aide to recover the weapons.  In the meantime Longhorn Gates has arrived in the nation and decides to recruit every beautiful woman on the island to launch a sexy assault on Maximos guerillas.

==Cast== Marvin Miller   ...  Henri Degiere   Robert Lowery  ...  Maximo Toro  
*Jackie Coogan   ...  Captain Toussaint   James Ellison   ...  Axel Longhorn Gates  
*Ingeborg Kjeldsen   ...  Francoise Degiere  
*Jeffrey Stone   ...  Steve Harding 
*Donald Durrell   ...  Stoney Jackson  Tommy Cook   ...  Razmo  
*True Ellison   ...  Melesa  
*Gabriel Dell   ...  "Major" Henderson 
* Paul Bailey ...  Clutch 
* Luis de Tejada ...  Jeep Driver 
* Yanka Mann ...  Chiquita 
* Fabiola Brown ...  Bonita

==Notes==
 

==External links==
*  

 
 
 
 
 
 
 
 
 