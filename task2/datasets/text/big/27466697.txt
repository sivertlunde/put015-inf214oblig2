The Old Army Game
{{Infobox Hollywood cartoon|
|cartoon_name=The Old Army Game
|series=Donald Duck Jack King
|story_artist=Carl Barks, Jack Hannah
|image=
|caption=
|animator=
|layout_artist=
|background_artist=
|voice_actor=
|musician=
|producer=Walt Disney
|studio=Disney
|distributor=RKO
|release_date= November 5, 1943
|color_process=Technicolor
|runtime=7 minutes
|movie_language=English
|preceded_by=Fall Out Fall In
|followed_by=Home Defense
}}
 RKO Radio Pictures.

==Plot== Black Pete walks through the camp as he inspects it and tells himself "Hmm, the camp sure is peaceful tonight" and goes through the cabins to see the soldiers sleeping, where he checks one of the cabins that is sleeping peacefully to discover that the snoring was a record and the things in bed were "dummies" (including Donald Duck.) Speaking of which, Donald Duck sneakingly arrives at the Army Camp,after some unauthorized leave. Knowing Donald Ducks coming, Black Pete goes into Donalds bed to surprise him. Donald Duck goes to his bed, and sleeps in it, knowing that he "put it over on the sarge", not realizing that Black Pete is in. But when he does, Donald runs for it. Pete chases Donald and Donald hides under one of three boxes, Donald switches the boxes repeatably to fool Black Pete. But when Pete finds him, and when Donald is still hiding in the box, Black Pete kicks Donalds box which flies through the bladed fence and the box cracks in half as Donalds lower half falls in a hole. But Donald thinks that his lower half was cut off, when Pete comes, he realizes that he killed donald and he is crying over Donalds tragedy. Donald feels so upset, he grabs Black Petes gun and attempts suicide. But Black Pete tells him to do it "Over there, behind the bushes", to which Donald comes out of the hole realizing that his lower half wasnt cut off. Donald feels relieved, but Black Pete on the other hand feels angry. So Pete chases Donald to a sign which says National Speed Limit: 35&nbsp;mi, so Donald and Black Pete chase more slowly (and even the music slows down) as the clip ends.

==External links==
* 

 
 
 
 
 
 
 
 
 

 