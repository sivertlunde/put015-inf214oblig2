The Lucky One (film)
 
{{Infobox film
| name           = The Lucky One
| image          = The Lucky One Poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Scott Hicks Kevin McCormick
| screenplay     = Will Fetters
| based on       =  
| starring       = Zac Efron Taylor Schilling Jay R. Ferguson Blythe Danner
| music          = Mark Isham
| cinematography = Alar Kivilo
| editing        = Scott Gray
| studio         = Village Roadshow Pictures Di Novi Pictures Warner Bros. Pictures Roadshow Entertainment   
| released       =  
| runtime        = 100 minutes 
| country        = United States
| language       = English
| budget         = $25 million 
| gross          = $99,357,138   
}}
 romantic drama novel of Nicholas Sparks.

The film stars Zac Efron as Logan Thibault, a U.S. Marine who finds a photograph of a smiling young woman while serving in Iraq, carries it around as a good luck charm, and later tracks down the woman, with whom he begins a relationship.

The Lucky One received generally negative reviews from critics, many of whom found its writing generic and criticized Efrons casting.

==Plot== Marine serving tour of mortar attack destroys where he had been sitting before he saw the picture, killing some of those around him, but leaving him with minor injuries. He later tries to find the owner of the picture, but is unsuccessful. Logan keeps the picture, and has a run of close calls but escapes them, though a number of his comrades do not. On his final mission Logan and a squadmate are discussing the upcoming end of their tour, and thoughts of returning home. Logans squad mate is confident Logan will get back home, declaring the woman in the picture is Logans "guardian angel". The conversation ends abruptly when an explosion destroys the Humvee they were riding in.
 PTSD and survivor guilt, he finds it difficult to adjust to life back home. He decides it is best to leave, and he departs to find some peace from his memories, and to search for the woman in the picture to thank her for her unknown role in helping to bring him through.

Logan walks with his dog Zeus to Louisiana, where a lighthouse in the background of the picture of the unknown woman has provided a clue to her location. Once there he asks around if anyone could recognize the woman in the picture, and a local resident Logan finds in a bar recognizes the woman, but warns Logan that she used to be married to a friend of his, a local deputy sheriff.

Logan finds the woman, Beth Green (Taylor Schilling), but has difficulty explaining why he is there. She assumes he wants to apply for a job, and becomes wary of him. She looks to send him off, but her grandmother Ellie (Blythe Danner) is less quick to judge, and decides to give him the job. At first, Beth is irritated by Logans presence, but begins to warm to him as he proves to be more solid than she had at first supposed. Logans calm, steady presence and willingness to work are appreciated, as is his competence in repairing machinery. Through it all he develops a supportive relationship with Beths son, Ben, who is without a strong, positive male presence in his life since the death of Beths brother, Drake.

Beths former husband, Sheriff Deputy Keith Clayton, the son of the towns judge and mayoral hopeful, is immediately suspicious and jealous of Logan. He is brusque and overbearing with the former Marine. Insecure, he discourages Ben from playing the violin around him, something which causes Ben to practice in his tree house by himself. When Ben returns bloodied from a charity baseball game, Beth and Keith have an argument, and Keith threatens to use his connections to take full custody of Ben. Beth is anxious over Keiths short temper, and is fearful of losing her son to him.

On the anniversary of Drakes death, Beth becomes distraught and Logan comforts her. Beth opens up about the life she had growing up with her brother, and she takes Logan to a boat her parents owned. The boat no longer runs, but she is fond of it, and she reminisces about how she and Drake used to ride together on it when they were young.

One day Logan overhears Ben playing the violin in his tree house. Later, Ben discovers that Logan is musical as well, and has a talent for playing the piano. Logan convinces Ben to team up with him for a church service performance, and with coaxing and encouragement, Ben agrees. Keith tries to do something about the budding relationship between Beth and Logan, but Beth stands up to Keith, showing that she is not intimidated by him anymore.

Logan and Ben perform for the church. Afterwards the man who Logan talked to when he first arrived in town finds Keith to tell him about Logan asking around with the photograph. Keith breaks into Logans home and steals the photograph, and then tells Beth that Logan has been stalking her. Her trust destroyed, Beth is distraught and sends Logan away. As he walks from the house young Ben comes out and gives him his chess book. Ellie tries to soften Beth, explaining to her that it isnt Logans fault he survived and Drake did not.

An intoxicated Keith sees Logan walking with his dog and angrily confronts him. When Zeus starts barking Keith draws his weapon, aiming briefly at Zeus before aiming at Logan, while people around them on the street begin to panic and run. Logan manages to disarm Keith, turning his gun over to another officer. Logan heads home to pack up his belongings. While doing so he flips through the chess book and finds a picture of Beths brother, Drake. The tattoo on Drakes forearm says "Aces". Realizing now that he knows what became of Beths brother, he heads back to Beths house to tell her.

Keith is at Judge Claytons, shocked and embarrassed over what he had done. The judge continues to see events only as they relate to himself, and tells Keith the other sheriff will look the other way, and it will all blow over before the election.  Keith walks out into a gathering storm, leaving his badge behind. He heads for Beths to plead to get back together again. Beth gently but firmly tells him it cannot be, angering Keith, who threatens to take Ben away. Hearing this the boy runs out into the storm with Keith following. Beth goes after them, just as Logan arrives. Ellie urges Logan to follow.

Ben heads for his tree house, but has difficulty crossing the river because of the high water and storm. The rope bridge gives way and father and son are dropped into the water, just as Beth and Logan arrive. Keith calls to Logan for help and he immediately comes to assist, as the tree house sways in the wind. Keith is caught in the rope of the bridge, and gives Ben over to Logan. Logan says he will return for Keith, but as Ben is handed up to Beth the tree house falls and Keith disappears under it. The river sweeps the debris away. Paramedics arrive in the background. Judge Clayton thanks Logan for saving Ben, to which Logan replies it was Keith that had saved him, and conveys his condolences.

Back home, Beth goes to thank Logan for saving her son. Logan states he is there to tell her he knew what happened to her brother Drake. He tells Beth how he saw one of Drakes men injured, and how Drake went to rescue him, paying for it with his life. Logan assures Beth that her brother didnt die in vain. As Logan walks out, Beth runs after him and asks him to stay, saying "You belong here with us."

The final scene shows Logan, Beth, Zeus and Ben, on Bens 9th birthday, steering the boat as they make their way through the beauty of the Louisiana delta.

==Cast==
* Zac Efron as Logan Thibault
* Taylor Schilling as Beth Green
* Blythe Danner as Ellie 
* Jay R. Ferguson as Keith Clayton
* Riley Thomas Stewart as Ben Clayton
* Adam LeFevre as Judge Clayton
* Joe Chrest as Deputy Moore
* Ann McKenzie as Charlotte Clayton
* Kendal Tuttle as Drake "Aces" Green
* Robert Terrell Hayes as Victor
* Russ Comegys as Roger Lyle
* Sharon Morris as Principal Miller

==Reception==
The Lucky One received mixed reviews from critics. At Rotten Tomatoes, the film holds a "negative" rating of 20&nbsp;percent, based on 134 reviews and an average rating of 4.2/10, with the critical consensus saying, "While it provides the requisite amount of escapist melodrama, The Lucky One ultimately relies on too many schmaltzy clichés to appeal to anyone not already familiar with the Nicholas Sparks formula".    It also has a score of 39 on Metacritic based on 35 reviews, indicating "generally unfavorable reviews".   

The Lucky One grossed $22,515,358 over its opening weekend, landing in number 2 at the box office behind Think Like a Man  . The film grossed $60.4 million in North America and $38.9 million in other countries, for a worldwide total of $99,357,138. 

===Accolades===
{| class="wikitable"
|-
! Award !! Category !! Recipient(s) !! Result
|- Golden Trailer Awards 2012
| Best Romance
| Find You
| 
|-
| Best Romance Poster
| One Sheet
| 
|-
| Best Romance TV Spot
| Reveal
| 
|- Teen Choice Awards 2012 
| Choice Movie Actor: Drama
|rowspan="2"| Zac Efron
|rowspan="2"  
|-
| Choice Movie Actor: Romance
|-
| Choice Movie Actress: Romance
| Taylor Schilling
| 
|-
|  
| The Lucky One
| 
|-
| Choice Movie: Liplock
| Zac Efron & Taylor Schilling
|rowspan="2"  
|-
| Choice Movie: Romance
|rowspan="2"| The Lucky One
|-
|rowspan="2"| 39th Peoples Choice Awards|Peoples Choice Awards 2013
| Favorite Dramatic Movie
| 
|-
| Favorite Dramatic Movie Actor
| Zac Efron
| 
|- 34th Young Young Artist Awards 2013    34th Young Best Performance in a Feature Film - Leading Young Actor Ten and Under
| Riley Thomas Stewart
| 
|}

==Home media==
The Lucky One was released on DVD on August 28, 2012.

==References==
 

==External links==
 
*  
*  
*  
*  
*   at The Numbers

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 