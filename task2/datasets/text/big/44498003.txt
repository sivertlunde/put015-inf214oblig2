Eeramana Rojave
{{Infobox film 
| name           = Eeramana Rojave 
| image          = https://www.google.co.in/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0CAcQjRw&url=http%3A%2F%2Fshakthi.fm%2Fta%2Falbum%2Fshow%2F7f5d33bf&ei=YQ-AVP_zFIyQuQTWyIHgCw&bvm=bv.80642063,d.c2E&psig=AFQjCNGH3ARVIZXctYtoSEV14YGRetyknQ&ust=1417765084245385
| caption        =  Keyaar
| Keyaar
| writer         = N. Prasannakumar (dialogues)
| screenplay     = Keyaar
| story          = Keyaar Shiva Mohini Mohini Srividya Nassar
| music          = Ilayaraja
| cinematography = Nambi
| editing        = B. Lenin V. T. Vijayan
| studio         = K.R.Enterprises
| distributor    = K.R.Enterprises
| released       =  
| runtime        = 139 minutes
| country        = India  Tamil
}}
 1991 Cinema Indian Tamil Tamil film, directed and produced by Keyaar. The film stars Shiva (Tamil actor)|Shiva, Mohini (Tamil actress)|Mohini, Srividya and Nassar in lead roles. The film had musical score by Ilayaraja. 

==Cast==
  Shiva as Shiva (debut) Mohini as Shanthi (debut)
*Srividya as Shanthis Mother
*Nassar
*Venniradai Moorthy
*Chinni Jayanth
*Kuladeivam Rajagopal
*Kumarimuthu Thyagu
*Disco Shanthi
 

==Soundtrack==
The music was composed by Ilaiyaraaja.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Adho Mega Oorvalam || Mano (singer)|Mano, Sunandha || Pulamaipithan || 05.05 
|-  Janaki || Pirai Sudan || 04.49 
|-  Janaki || Vaali || 04.52 
|-  Janaki || Vaali || 05.04 
|-  Vaali || 04.22 
|-  Chithra || Muthulingam || 04.49 
|- 
| 7 || Kalyana Tharagare || Malaysia Vasudevan || Gangai Amaran || 04.45 
|}

==References==
 

==External links==
*   
*   
 

 
 
 
 
 
 


 