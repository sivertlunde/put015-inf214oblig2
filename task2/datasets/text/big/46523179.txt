Love Watches
{{Infobox film
| name           = Love Watches
| image          =
| caption        =
| director       = Henry Houry Albert E. Smith Graham Baker
| based on       = play, Lamour veille by Gaston Arman de Caillavet and Robert de Flers
| starring       = Corinne Griffith
| music          =
| cinematography = Arthur Ross
| editing        =
| distributor    = Vitagraph Company of America
| released       = July 15, 1918
| runtime        = 5 reels
| country        = USA
| language       = Silent.. English language titles
}} lost  1918 silent film feature comedy-drama directed by Henry Houry and starring Corinne Griffith. It was produced and distributed by the Vitagraph Company of America.  A Broadway play produced by Charles Frohman starred Billie Burke in 1908.  

==Cast==
* Corinne Griffith - Jacqueline Cartaret
* Denton Vane - Ernest Augarde
* Florence Deshon - Lucia de Morfontaine
* Edmund Burns - Count Andre de Juvigny
* Julia Swayne Gordon - Marquise de Javigny
* Alice Terry - Charlotte Bernier
* Nellie Parker Spaulding - Sophie
* Charles A. Stevenson - Cartaret
* Carola Carson - Baroness
* Alice Nash - Christine
* Edna Nash - Solange

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 

 