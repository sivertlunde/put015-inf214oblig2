Malliswari (2004 film)
 
 
{{Infobox film
|  name           = Malliswari
|  image          = Malliswari_poster.jpg
|  writer         = Trivikram Srinivas  
|  screenplay     = K. Vijaya Bhaskar Venkatesh Katrina Naresh
|  director       = K. Vijaya Bhaskar
|  producer       = Daggubati Suresh Babu|D.Suresh Babu D. Rama Naidu  
|  studio         = Suresh Productions
|  released       = 18 February 2004
|  runtime        = 155 minutes
|  editing        = Sreekar Prasad
|  cinematography = Sameer Reddy
|  language       = Telugu Koti
|  country        = India
|  awards         = 
|  budget         =  
}} Telugu romantic comedy film produced by Daggubati Suresh Babu|D.Suresh Babu on Suresh Productions banner, directed by K. Vijaya Bhaskar. Starring Daggubati Venkatesh|Venkatesh, Katrina Kaif in lead roles and music composed by Saluri Koteswara Rao|Koti, written by Trivikram Srinivas  The film was later dubbed into Malayalam as Malliswari, the Princess.  The film recorded as Super Hit at box office.

== Plot ==
Malliswari (Katrina Kaif) is the heiress of Raja of Mirzapur (M. Balaiah). Her deceased father wrote in his will that she would inherit the property worth 750 crores after she becomes major at the age of 21. The entire property is under the care-taking of Bhavani Shankar (Kota Srinivasa Rao), who wants to inherit the entire property by killing her. As he hatches the plans to eliminate Malliswari, she is sent to Visakhapatnam to a relatives place as a normal girl so that she can live with anonymity. Prasad (Daggubati Venkatesh|Venkatesh) works as bank accountant in Andhra Bank there. He is a bachelor who has been desperately seeking for marriage alliance for the past seven years. He would be looking at every unmarried girl with dreams in his eyes. He accidentally meets Malliswari and falls deeply in love with her. Malliswari is traced by the goons there and they start running after her. Prasad escorts her and drops her safely in Hyderabad. The rest of the story describes what happens next and the eventual conclusion of the plot.

==Cast==
{{columns-list|3| Venkatesh as Pellikani Prasad
* Katrina Kaif as Malliswari (Princess of Mirzapur)
* Gajala as Monalisa (Cameo)
* Kota Srinivasa Rao as Bhavani Shankar
* Brahmanandam as Balu Sunil as Paddu / Padmanabham Naresh as Prasads elder brother
* Tanikella Bharani as Murthy
* M. S. Narayana as Krishnavenis husband (Cameo)
* Dharmavarapu Subrahmanyam as Bank Manager Shankar Rao Mallikarjuna Rao as Prasads paternal uncle
* M. Balaiah as Ram Mohan Rao (Malliswaris grandfather)
* Ahuti Prasad as Malliswaris paternal uncle
* Chalapathi Rao
* Raghunath Reddy as Police Officer
* CVL Narasimha Rao as Lawyer
* Devdas Kanakala as Lawyer
* Benarjee as Balaji (Killer)
* Prabhu as Bobby (Bhavani Shankars son)
* Lakshmipathi as Ice Cream vendor
* Chitram Seenu as Peon Seenu
* Sarika Ramachandra Rao as Auto Driver
* Gundu Sudarshan as Marriage Broker
* Gowtham Raju as Cashier Subba Rao Chitti Babu as Vegetable Vendor
* Bandla Ganesh as Drunker 
* Smita as Killer Hema as Krishnaveni
* Rajitha as Murthys wife
* Sana as Malliswaris paternal aunty Rajya Lakshmi as Prasads sister-in-law
* Uma as Lakshmi
* Padma Jayanth as Bhavani Shankars wife
* Priyanka
* Master Rohit as Siddharth (boy in the library)
* Baby Greeshma as Dolly
}}

== Crew ==
* Director: K. Vijaya Bhaskar
* Screenplay: K. Vijaya Bhaskar
* Story: Trivikram Srinivas
* Dialogue: Trivikram Srinivas
* Producer: Daggubati Suresh Babu Koti
* Cinematography: Sameer Reddy
* Editor: Sreekar Prasad

==Soundtrack==
{{Infobox album
| Name        = Malliswari
| Tagline     = 
| Type        = film Koti
| Cover       =
| Released    = 2004
| Recorded    =
| Genre       = Soundtrack
| Length      = 26:43
| Label       = Aditya Music Koti
| Reviews     = Anandama Andamaye   (2004)  
| This album  = Malliswari   (2004)
| Next album  = Swetha Naagu   (2004)
}}
The music was composed by Saluri Koteswara Rao|Koti. All songs are hit tracks. The music was released on Aditya Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 26:43
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Nuvventa Andagattevaina Sirivennela Sitarama Sastry Karthik
| length1 = 4:48

| title2  = Nee Navvule Vennelani
| lyrics2 = Sirivennela Sitarama Sastry Sunitha
| length2 = 4:11

| title3  = Gundello Gulabila
| lyrics3 = Sirivennela Sitarama Sastry Chithra
| length3 = 4:16

| title4  = Cheli Soku
| lyrics4 = Sirivennela Sitarama Sastry
| extra4  = KK (singer)|KK, Chithra
| length4 = 4:02

| title5  = Janma Janmala
| lyrics5 = Bhuvanachandra SP Balu, Shreya Ghoshal
| length5 = 5:06

| title6  = Nuvu Evvari Edalo 
| lyrics6 = Sirivennela Sitarama Sastry 
| extra6  = SP Balu, Shreya Ghoshal
| length6 = 4:06
}}

==Awards==
Nandi Award for Akkineni Award for Best Home-viewing Feature Film - Daggubati Suresh Babu|D. Suresh Babu

==References==
 

==External links==
*  

 
 

 
 
 
 