Naanayam (1983 film)
{{Infobox film
| name           = Naanayam
| image          =
| caption        =
| director       = IV Sasi
| producer       =
| writer         = T. Damodaran
| screenplay     = T. Damodaran Madhu Mammootty Mohanlal Srividya Shyam
| cinematography = CE Babu
| editing        = K Narayanan
| studio         = CS Productions
| distributor    = CS Productions
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, directed by IV Sasi. The film stars Madhu (actor)|Madhu, Mammootty, Mohanlal and Srividya in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast== Madhu
*Mammootty
*Mohanlal
*Srividya
*Poornima Jayaram Seema
*Adoor Bhasi
*Janardanan
*K. P. Ummer
*Paravoor Bharathan

==Soundtrack== Shyam and lyrics was written by Yusufali Kechery and Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ghanashyaama varnnaa kanna || Vani Jairam || Yusufali Kechery || 
|-
| 2 || Hai Murari   || Vani Jairam || Yusufali Kechery || 
|-
| 3 || Maankidaave vaa || K. J. Yesudas, P Susheela || Yusufali Kechery || 
|-
| 4 || Pom Pom   || Vani Jairam, Unni Menon || Yusufali Kechery || 
|-
| 5 || Pom Pom Ee Jeeppinu Madamilaki || K. J. Yesudas, P Jayachandran || Yusufali Kechery || 
|-
| 6 || Pranaya swaram hrudaya swaram || P Jayachandran, Krishnachandran || Poovachal Khader || 
|-
| 7 || Pranayaswaram Hridayaswaram   || P Susheela, P Jayachandran || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 