Drive Angry
 
{{Infobox film
| name           = Drive Angry
| image          = Drive Angry Poster.jpg
| caption        = Theatrical release poster
| director       = Patrick Lussier
| producer       = Michael De Luca René Besson
| writer         = Todd Farmer Patrick Lussier Billy Burke Tom Atkins
| music          = Michael Wandmacher
| cinematography = Brian Pearson
| editing        = Patrick Lussier Devin C. Lussier
| studio         = Millennium Films Nu Image Saturn Films
| distributor    = Summit Entertainment  }} Lionsgate    Metropolitan Filmexport  
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $45–55 million   
| gross          = $28.9 million 
}}

Drive Angry is a 2011 American supernatural action film starring Nicolas Cage and Amber Heard, and directed by Patrick Lussier. It was released on February 25, 2011. Shot in 3-D, the film was met with a mixed reception and grossed almost $30 million.

== Plot ==
  Billy Burke), a cult leader who tricked Miltons daughter into joining his followers in the wake of Miltons death, only to kill her and her husband and steal their daughter -Miltons granddaughter- to be sacrificed in a Satanist ritual.
 Dodge Charger R/T and follows her to fix it in exchange for a ride on the way to Stillwater.

Entering her room, Piper walks in on her boyfriend, Frank (Todd Farmer), having sex with another woman. Piper beats up the woman and assaults Frank, getting knocked to the ground. Milton, in a phone booth nearby, hears the commotion and comes to Pipers aid, kicking Frank in the face as he is about to continue assaulting Piper, and punching Frank repeatedly. Milton steals his car, taking Piper along with him driving to Stillwater. Meanwhile, a supernatural operative of Satan, The Accountant (William Fichtner), arrives on Earth with the mission to bring Milton back to Hell. After interrogating Frank, he discovers that Milton and Piper are heading to Louisiana and tricks a pair of state troopers into helping him by impersonating an FBI agent.

At a shady hotel, Milton is attacked by King and his men, who heard about his return, but he kills most of them. The Accountant appears with the police and chases after Milton and Piper, who are chasing after Kings van. Milton uses a gun labeled "The Godkiller" to shoot the Accountant out of the road. They then follow King to a church, only to find it filled with Kings followers. They are ambushed and captured. Piper is kidnapped and Milton is shot in the eye and left for dead, but he awakens and kills Kings men before pursuing the RV once again. Inside, Piper breaks free and fights King before jumping out of the RV and onto Miltons car. King then disables the car by repeatedly shooting its engine.
 David Morse), Chevrolet Chevelle Tom Atkins) and finally arrive at Stillwater. The Accountant captures Piper and forces Milton to give up the Godkiller before he can engage King, but he allows Milton to go into battle against King and his followers to save his granddaughter, noting that Satan is more of a warden for a very large prison rather than the master of evil and is actually against the sacrifice of innocents in his name.

While Milton slaughters Kings men before they can sacrifice the child, Piper escapes The Accountants clutches with the Godkiller. King eventually gets the upper hand on Milton and savagely beats him. Piper fires the Godkiller at King, but misses and hits one of his few surviving men instead. She is knocked out by the guns recoil. King orders one of his female servants to murder the child. However, the woman, who had been caring for the baby ever since King stole her, finds herself unable to carry out the deed, making King angry. The Accountant attracts Kings attention, allowing Milton to grab the Godkiller and shoot King, destroying his soul. The Accountant retrieves the baby. He allows Milton to say goodbye to her and Milton gives her to Piper, who promises to care for and protect her. Webster arrives and looks on as Milton "dies."
 1957 Chevrolet before throwing the keys to Milton. They then drive off into the gates of Hell.

== Cast ==
* Nicolas Cage as John Milton 
* Amber Heard as Piper 
* William Fichtner as The Accountant  Billy Burke as Jonah King  David Morse as Webster 
* Katy Mixon as Norma Jean 
* Charlotte Ross as Candy 
* Christa Campbell as Mona Elkins 
* Pruitt Taylor Vince as Roy

== Production == Season of the Witch, he had wanted to have such a scene but producers rejected the idea. 

The film was shot in 3D, and special effects were created by Gary Tunnicliffe.  The cameras were rented from Paradise FX.  One of the reasons why Cage chose this movie is that he wanted to play in a 3D movie to be part of the new technology  

The three cars driven by Cage in the film are a 1964 Buick Riviera, a 1969 Dodge Charger R/T (440 Engine) and a 1971 Chevrolet Chevelle SS 454. 
 Plain Dealing and Shreveport, Louisiana. 

== Release ==
The film was released in the US on February 25, 2011.  Footage premiered on July 23, 2010 as part of the San Diego Comic-Con International. 

=== Critical reception === New York Jack and Jill and Just Go with It.

=== Box office ===
The film opened at ninth place within the box office rankings at an underperforming $1.6 million on Friday, with a lower than expected $5 million weekend.    Drive Angry  box office performance made it the lowest-grossing opening of a 3D film released in over 2,000 US theaters.  The film was slightly more successful in international markets, earning $18,210,368 according to Box Office Mojo. 

=== Home media ===
Drive Angry was released on DVD, Blu-ray and 3D Blu-ray on May 31, 2011.

== See also ==
*   (2012)—films with a similar theme, also starring Nicolas Cage

== References ==
 

== External links ==
*  
*  
*  
*  
*   at Metacritic
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 