The Lady Gambles
{{Infobox film
| name           = The Lady Gambles
| image          = The Lady Gambles 1949.jpg
| image_size     =
| caption        = Michael Gordon
| producer       = Michael Kraike
| writer         = Lewis Meltzer (story) Oscar Saul (story) Roy Huggins Halstead Welles
| narrator       = Robert Preston Frank Skinner
| cinematography = Russell Metty
| editing        = Milton Carruth
| distributor    = Universal Pictures
| released       =  
| runtime        = 99 minutes
| country        = United States English
| budget         =
| gross          =
}} 1949 drama Robert Preston. Las Vegas and becomes addicted to gambling.

==Plot==
When his estranged wife Joan is found badly beaten after using loaded dice in a back-alley game, David Boothe looks back on how she came to this sorry state.

A reporter from Chicago, he had been on an assignment at Hoover Dam, so he and his wife stayed at a hotel in Las Vegas. There the casinos owner, Corrigan, introduces her to gambling with a few chips on the house.

Joan eventually loses $600 of Davids expense money, as well as pawning a camera, before winning it back. Gambling excites her. When her sister Ruth joins them in Nevada, her husband decides to leave. Ruth has always been a divisive presence, manipulative and neurotic.

Corrigan flirts with Joan and persuades her to be his proxy in a poker game. She wins $4,000 and is given a share. But the game lasts till dawn, and when David phones from the road, Ruth tells him Joan has not slept in her bed all night.

He returns to Vegas and can see how gambling has her hooked. David quits his job and takes her to Mexico, where he intends to write a book. A couple Joan met in Vegas get her into a dice game, where she loses all of their life savings. David leaves, intending to file for a divorce.

Not knowing where else to go, Joan returns to Vegas, where she is hired by Corrigan to front a horse-racing operation. But she double-crosses his partners just to cash in on a long-shot bet. On her own again, Joan descends into a world of disreputable characters, partnering in Shreveport with a crooked gambler named Frenchy.

By the time David rejoins her after the beating, Joan is hospitalized and suicidal. He must fight to convince her that it is not too late to kick her gambling habit and save their marriage.

==Cast==
*Barbara Stanwyck as Joan Phillips Boothe Robert Preston as David Boothe
*Stephen McNally as Horace Corrigan
*Edith Barrett as Ruth Phillips
*John Hoyt as Dr. Rojac
*Elliott Sullivan as Barky John Harmon as Frenchy
*Philip Van Zandt as Chuck Benson Leif Erickson as Tony
*Curt Conway as Bank Clerk
*Houseley Stevenson as Pawnbroker
*Don Beddoe as Mr. Dennis Sutherland
*Nana Bryant as Mrs. Dennis Sutherland
*Tony Curtis as Bellboy (as Anthony Curtis)
*Peter Leeds as Jack Harrison, Hotel Clerk
*Frank Moran as Murphy
*Esther Howard as Gross Lady
*John Indrisano as Bert

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 