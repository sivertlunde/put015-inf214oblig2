Remote (film)
{{Infobox film
| name          =Remote!
| image         =
| caption       =
| imdb_id       =
| writer        =Mike Farros John Diehl
| director      =Ted Nicolaou
| producer      =Albert Band
| studio         = Moonbeam Entertainment
| distributor   = Paramount Pictures
| released      = 
| runtime       =80 min
| country       =United States English
}}

Remote (Moonbeam Entertainment, 1993) is a comedy film that was released on September 22, 1993, starring Chris Carrara, Jessica Bowman, and John Diehl. Ted Nicolaou directed the film and it was written by Mike Farros. The movies premise is similar to that of Home Alone. It is the second film to be released by Moonbeam Entertainment.

==Plot==
Randy Mason (Chris Carrara) is a teenage tech whiz who lives in a suburban neighborhood located somewhere in the state of California with his mother Marti (Derya Ruggles) and his father Brent (whos away for the duration of the film on a business trip). Randy designs and uses remote controlled models as a hobby, as well as using the modified controllers for other purposes as well. After being set up by a bully Ben (Jordan Belfi) who uses one of Randys models to wreak havoc on the school project of his friend Jamaal (Kenneth A. Brown) and getting the blame for it, Randy arrives home to hear Marti saying on the answering machine that she is going to confiscate all of his models. Randy then decides to stash them at the model home which serves as his secret hideout. While hiding out there, He stumbles across three store robbers named Delbert McCoy (John Diehl), Louis (Tony Longo), and Louis cousin Ritchie Marinelli (Stuart Fratkin). With the use of his remote control toys, as well as a little help from his captive friend and love interest Judy Riley (Jessica Bowman) an avid baseball player, Randy manages to apprehend the three fugitives.

==DVD release==
As of May 11, 2013, neither Paramount Pictures and Moonbeam Entertainment has announced any plans for a DVD release of the film.

==External links==
* 

 
 
 
 
 
 


 