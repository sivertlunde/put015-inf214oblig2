Breakfast on Pluto (film)
 
{{Infobox film
| name = Breakfast on Pluto
| image = Breakfast on pluto poster.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Neil Jordan
| producer = Alan Moloney Neil Jordan Stephen Woolley
| screenplay = Neil Jordan
| based on =  
| starring = Cillian Murphy Ruth Negga Liam Neeson Stephen Rea Brendan Gleeson
| music = Anna Jordan
| cinematography = Declan Quinn
| editing = Tony Lawson
| distributor = Sony Pictures Classics Pathé
| released =  
| runtime = 129 minutes
| country = United Kingdom Ireland
| language = English
| budget = 
| gross = $3,942,254
}} novel of Patrick McCabe, foundling searching for love and her long-lost mother in small town Ireland and London in the 1970s.

==Plot==
 
The film is divided over more than 30 chapters. In the fictional Irish town of Tyrellin, near the border of Northern Ireland in the late 1940s, cartoon robins narrate as Patrick Bradens mother, Eily Bergin, abandons him on the doorstep of the local parochial house where his father, Father Liam, lives. He is then placed with an unloving foster mother. Biologically male, a young Patrick is later shown donning a dress and lipstick, which angers his foster family. Patrick is accepted by his close friends Charlie, Irwin, and Lawrence, as well as by Lawrences father, who tells Patrick Eily looked like blonde American movie star Mitzi Gaynor.

The story moves to Patricks late teen years. Patrick gets into trouble in school by writing explicit fiction imagining how he was conceived by his parents and by inquiring about where to get a sex change. Patrick comes out as transgender and renames himself Kitten, also using the name Patricia. She approaches Father Liam in Confession (religion)|confession, asking about Eily, but is rebuffed. Kitten soon runs away from home, catching a ride with a glam rock band, Billy Hatchet and the Mohawks, and striking up a flirtation with leader Billy. Billy installs the lovestruck, homeless Kitten in a trailer home where she discovers hes hiding guns smuggled for the Irish Republican Army. Meanwhile, Irwin has begun to work with the IRA, much to the dismay of his now-girlfriend Charlie. Kitten dismisses Irwins politics as "serious, serious, serious," but after Lawrence is killed by police detonating a suspected IRA car bomb, she tosses the IRA gun cache into a lake. Billy abandons Kitten to flee the IRA, while Kitten plays crazy, so that she wont be shot.

Kitten next journeys to London to search for Eily, but initial inquiries prove fruitless. Penniless, she finds shelter in a tiny cottage in a park, only to find that its a childrens entertainment park for The Wombles. Kitten gets a job as a singing, dancing Womble, but immediately loses it when her sponsor and co-worker punches their boss. Forced into prostitution, she is violently attacked by her first client, saving herself from strangulation by spraying him in the eyes with Chanel No. 5 perfume. At a diner, magician Bertie Vaughan asks her what she is writing in her notebook. She explains that its the story of "The Phantom Lady" who was "swallowed up" by the big city, then reveals its about the mother she is seeking. Bertie hires her to be his magicians assistant, turning her life story in a hypnosis act. The two take a romantic day trip, but Kitten explains that shes not biologically female when Bertie tries to kiss her. Bertie says that he already knew this. Soon, Charlie finds Berties show and takes Kitten away.

 
Kitten goes to a club frequented by British soldiers and dances with a soldier, only to be injured when the club is bombed by the IRA. When police discover that Kitten is biologically male and Irish, she is arrested as a suspected terrorist. Beaten and prevented from sleeping, she writes a hyperbolic statement, shown in a fantasy spy film spoof sequence. The polices attitude soften, realizing she is innocent, and they release her. With no place to go, Kitten begs to stay in the police station, but is tossed to the street. Kitten is again forced to turn tricks, but is saved by one of the cops who interrogated her. He brings her to a peep show where she transforms herself into a blonde. Her repentant father finds her and in a scene that mirrors their confessional scene, professes his love and tells Kitten where to find Eily. She goes to her house posing as a telephone company market researcher and discovers a younger half-brother whose name is also Patrick. She faints upon meeting Eily, but after reviving does not reveal her identity.

When Irwin is killed by the IRA, Kitten goes home to tend to a pregnant Charlie. The town reacts against the unwed mother and her transgender friend by firebombing the parish house. Kitten and Charlie flee to London. In the final scene, they run into pregnant Eily and little Patrick at the doctors office, where Charlie is getting post-natal care. Kitten is friendly, but still doesnt reveal who she is.

==Cast==
* Cillian Murphy as Patrick/Patricia "Kitten" Braden
* Conor McEvoy as 10-year-old Patrick
* Stephen Rea as Bertie Vaughan
* Brendan Gleeson as John Joe Kenny
* Liam Neeson as Father Liam
* Gavin Friday as Billy Hatchett
* Ruth Negga as Charlie
* Laurence Kinlan as Irwin
* Seamus Reilly as Lawrence
* Eva Birthistle as Eily Bergin
* Ian Hart as PC Wallis
* Steven Waddington as Inspector Routledge
* Ruth McCabe as Ma Braden Liam Cunningham as 1st Biker (who introduces song "Breakfast on Pluto") Patrick McCabe as Schoolmaster Peepers Egan
* Bryan Ferry as Mr. Silky String
* Dominic Cooper as Squaddie at Disco

==Production==
To prepare for the lead role of Kitten, Murphy studied womens body language and for a few weeks met with a drag queen who instructed him and took him out clubbing with friends. Kaufman, Anthony.   , Time Out New York, 10 November 2005. Accessed 19 July 2007. 
 kinky situations as well as a few long-term relationships, Kitten doesnt even kiss another character on the lips. One sexual encounter for hire is strongly implied, but Kitten is not shown being overtly sexual with anyone on screen. Kittens flirtatious relationships with the series of male characters she meets throughout the film are never shown or strongly implied to have been consummated, leaving the yearning main character unrequited.

The seaside scene between Kitten and Bertie was considered by some to be an allusion to director Jordans earlier film The Crying Game, Stein, Ruthe.  , The San Francisco Chronicle, 23 December 2005. Accessed 18 July 2007.  which also involved a transgender major character, the IRA, and actor Stephen Rea. In The Crying Game, Reas character doesnt realize that the woman he has fallen for and become sexually involved with is biologically male. In Breakfast on Pluto, Kitten confesses that shes "not a girl" before Reas character can kiss her, and he says kindly that he already knew, but does not follow through with the kiss.
 Patrick McCabe, has a cameo in the film as Kittens creative writing teacher.   IMDb  Declan Burke also played his part as the leading extra.

==Reception==
Rotten Tomatoes gives the film a score of 57% based on 115 reviews. 

==Awards and nominations==
For his portrayal of Kitten, Murphy won the 2007   for Best Performance by an Actor in a Motion Picture - Musical or Comedy.

Jordan also won the 2007 IFTA for Best Director and Jordan and McCabe took home the Best Script IFTA. 

{| class="wikitable" style="border:2px #aaa solid; font-size:90%;"
|-  style="background:#ccc; text-align:center;"
! colspan="4" style="background: LightSteelBlue;" | Awards
|-  style="background:#ccc; text-align:center;"
! Award
! Category
! Name
! Outcome
|- European Film Awards Best Actor Cillian Murphy
| 
|- 63rd Golden Golden Globe Awards Golden Globe Best Actor – Motion Picture Musical or Comedy Cillian Murphy
| 
|- Golden Trailer Awards Best Foreign Dramatic Trailer
|
| 
|- Best Voice Over
|
| 
|- 4th Irish Irish Film and Television Awards Best Actor in a Lead Role in a Feature Film Cillian Murphy
| 
|- Best Director Neil Jordan
| 
|- Best Hair & Make-Up for Film
|Glynn, Lorraine, Lynn Johnson
| 
|- Best Script for Film Neil Jordan, Patrick McCabe
| 
|- Best Irish Film (Audience Award)
|
| 
|- Best Actor in a Supporting Role in a Feature Film Stephen Rea
| 
|- Best Actress in a Supporting Role in a Feature Film Ruth Negga
| 
|- Best Cinematography Declan Quinn
| 
|- Best Costume Design for Film Eimer Ni Mhaoldomhnaigh
| 
|- Best Film Alan Moloney, Neil Jordan, Stephen Woolley
| 
|- Best Production Design for Film Tom Conroy
| 
|- Ljubljana International Film Festival Audience Award Neil Jordan
| 
|- National Board of Review Special Recognition For excellence in filmmaking
|
| 
|- Satellite Awards Satellite Awards Satellite Award Best Actor – Motion Picture Musical or Comedy Cillian Murphy
| 
|}

==See also==
* Cross-dressing in film and television
* Don Partridge

==References==
 

== External links ==
*  
*  
*  
*  
*  
*   (January 2006)

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 