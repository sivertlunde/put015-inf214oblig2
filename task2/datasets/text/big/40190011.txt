Ida (film)
 
 
{{Infobox film
| name           = Ida
| image          = Ida (2013 film).jpg
| alt            = 
| caption        = Film poster
| director       = Paweł Pawlikowski
| producer       = {{Plainlist|
* Eric Abraham
* Piotr Dzięcioł
* Ewa Puszczyńska}}
| writer         = {{Plainlist|
* Rebecca Lenkiewicz
* Paweł Pawlikowski}}
| starring       = {{Plainlist|
* Agata Kulesza
* Agata Trzebuchowska
* Dawid Ogrodnik}}
| music          = Kristian Eidnes Andersen
| cinematography = {{Plainlist|
* Łukasz Żal
* Ryszard Lenczewski}}
| editing        = Jarosław Kamiński
| production companies = {{Plainlist|
* Canal+|Canal+ Polska
* Danish Film Institute
* Eurimages}}
| distributor    = {{Plainlist|
* Solopan  
* Memento Films  
* Artificial Eye  }}
| released       =  
| runtime        = 82 minutes  
| country        = {{Plainlist|
* Poland
* Denmark
* France
* United Kingdom}}
| language       = {{Plainlist|
* Polish
* French
* Latin}}
| budget         = €2 million 
| gross          = $10.7 million   
}} German occupation German occupation Holocaust and History of Poland (1945–89)#Stalinist era (1948–56)|Stalinism) remain unsaid: "none of this is stated, but all of it is built, so to speak, into the atmosphere: the country feels dead, the population sparse".   
 Best Foreign Best Film Best Film Not in the English Language of 2014 by the British Academy of Film and Television Arts (BAFTA).  

==Plot== Jews who German occupation Communist resistance political show trials of the early 1950s, when Poland’s Communist government used judicial terror (among other methods) to consolidate its power and eliminate its enemies."   

Wanda tells Ida that she should try some worldly sins and pleasures before she decides to take her vows. She picks up a hitchhiker, Lis, who turns out to be a alto saxophone player who is going to a gig in the same town. Wanda tries to get Ida interested in Lis, and brings Ida with him to his show, but Ida resists.

Ida wants to find the bodies of her parents. Wanda takes her to the house they were born in and used to own, which is now occupied by a Pole, Feliks Skiba and his family. Wanda had left her family with Feliks family during the war; the Skibas had hidden the Lebensteins from the German authorities. Wanda, a former prosecutor, demands that Feliks and his father tell her what happened to the Lebensteins. Finally, Feliks agrees to tell them—if Ida promises that they will leave the Skibas alone and give up any claim to the house.

Feliks admits that he took three of the Lebensteins into the woods and killed them. Feliks says that because Ida was very small and able to pass for a Christian, he was able to give her to a convent. But Wandas small son, whom shed also left behind, was "dark and circumcised". He couldnt pass for a Christian child, and Feliks had killed him along with Idas parents. Jeremy Hicks describes some of the possible motivations for Feliks murders: "The implication is that he killed them for fear that he and his family might be discovered by the Nazis to be hiding Jews, and themselves be killed. But there is so much left unsaid here that the motivations for murder are left obscure. An understanding of Polish wartime history might equally push us towards explaining the murder through Polish anti-Semitism. The perception that Jews had money, and that killing them would enable the murderers to acquire their property, is a motive that is hinted at too."   

Feliks takes the women to the burial place in the woods and digs up the bones of their family. Wanda and Ida take the bones to their family burial plot, in an abandoned, overgrown Jewish cemetery in Lublin, and bury them.   
 stilettos and evening gown, tries smoking and drinking, and then goes to Lis gig, where he later teaches her to dance. 

After the show Ida and Lis sleep together. The next morning Lis suggests they get married, have children, and live "life as usual." Without waking Lis, Ida dons her convent habit and leaves, seemingly to return to the convent, or continue being caught between two worlds.

==Cast==
* Agata Trzebuchowska as Ida Lebenstein/Anna
* Agata Kulesza as Wanda Gruz
* Joanna Kulig as a singer
* Dawid Ogrodnik as Lis, a saxophonist
* Adam Szyszkowski as Feliks Skiba
* Jerzy Trela as Szymon Skiba

==Production==
The director of Ida, Paweł Pawlikowski, was born in Poland and lived his first fourteen years there. In 1971 his mother abruptly emigrated with him to England, where he ultimately became a prominent filmmaker. Ida is his first Polish film; in an interview he said that the film "is an attempt to recover the Poland of my childhood, among many things".  Ida was filmed in Poland with a cast and crew that was drawn primarily from the Polish film industry. The film received crucial early funding from the Polish Film Institute based on a screenplay by Pawlikowski and Rebecca Lenkiewicz, who is well known as an English playwright. Once the support from the Polish Film Institute had been secured, producer Eric Abraham underwrote production of the film.   

The first version of the screenplay was written in English by Lenkiewicz and Pawlikowski, when it had the working title Sister of Mercy. Pawlikowski then translated the screenplay into Polish and further adapted it for filming.   
 General ‘Nil’ Fieldorf, a famed resistance fighter. While Wolińska-Brus may have been involved, she was not the actual prosecutor for that trial.  Pawlikowski met her in the 1980s in England, where shed emigrated in 1971; hes said of her that "I couldnt square the warm, ironic woman I knew with the ruthless fanatic and Stalinist hangman.  This paradox has haunted me for years.  I even tried to write a film about her, but couldnʼt get my head around or into someone so contradictory."   

Pawlikowski had difficulty in casting the role of Anna/Ida. After hed interviewed more than 400 actresses, Agata Trzebuchowska was discovered by a friend of Pawlikowskis, who saw her sitting in a cafe in Warsaw reading a book. She had no acting experience or plans to pursue an acting career. She agreed to meet with Pawlikowski because she was a fan of his film My Summer of Love (2004).   
 Last Resort (2000); unlike Pawlikowski, Lenczewski had worked in Poland as well as England  prior to Ida. Ida is filmed in black and white, and uses the now uncommon 4:3 ratio of the horizontal and vertical sizes of the image. When Pawlikowski told the films producers of these decisions about filming, they reportedly commented, "Paul, you are no longer a student, dont be silly."  Lenczewski has commented that, "We chose black and white and the 1.33 frame because it was evocative of Polish films of that era, the early 1960s. We designed the unusual compositions to make the audience feel uncertain, to watch in a different way." The original plan had been for Żal to assist Lenczewski. Lenczewski became ill, and Żal took over the project.    

Production on Ida was interrupted mid-filming by an early snowstorm. Pawlikowski took advantage of the two-week hiatus to refine the script, find new locations, and rehearse. He credits the break for "making the film cohere ... in a certain, particular way." 

Ida was edited by Jarosław Kamiński, a veteran of Polish cinema.  Pawlikowskis previous English language feature films were edited by David Charap. Excepting the final scene of the film, there is no background musical score; as Dana Stevens explains, "the soundtrack contains no extradiegetic music—that is, music the characters aren’t listening to themselves—but all the music that’s there is significant and carefully chosen, from Wanda’s treasured collection of classical LPs to the tinny Polish pop that plays on the car radio as the women drive toward their grim destination."  As for the final scene, Pawlikowski has said, "The only piece of music that is non-ambient (from outside the world of the film – that is not on the radio or played by a band) is the piece of Bach at the end.  I was a bit desperate with the final scene, and I tried it out in the mix.  It’s in a minor key, but it seems serene and to recognize the world and its complexities."   

==Reception==
Ida received critical acclaim with most of its praise going towards its script, atmosphere, and cinematography. A. O. Scott of the New York Times writes that "with breathtaking concision and clarity—80 minutes of austere, carefully framed black and white—Mr. Pawlikowski penetrates the darkest, thorniest thickets of Polish history, reckoning with the crimes of Stalinism and the Holocaust."    He concludes that "Mr. Pawlikowski has made one of the finest European films (and one of most insightful films about Europe, past and present) in recent memory."  David Denby of The New Yorker has called Ida a "compact masterpiece", and he discusses the films reticence concerning the history in which it is embedded: "Between 1939 and 1945, Poland lost a fifth of its population, including three million Jews. In the two years after the war, Communists took over the government under the eyes of the Red Army and the Soviet secret police, the N.K.V.D.. Many Poles who were prominent in resisting the Nazis were accused of preposterous crimes; the independent-minded were shot or hanged. In the movie, none of this is stated, but all of it is built, so to speak, into the atmosphere ..."    Denby considered Ida to be "by far the best movie of the year".  Peter Debruge was more reserved about the films success, writing in Variety that "...dialing things back as much as this film does risks losing the vast majority of viewers along the way, offering an intellectual exercise in lieu of an emotional experience to all but the most rarefied cineastes." 

Ida is partially a " , who always implied much more than she said."  Dana Stevens writes that "As played, stupendously, by the veteran Polish TV, stage, and film actress Agata Kulesza, Wanda is a vortex of a character, as fascinating to spend time with as she is bottomlessly sad." 

Ida has a 96% rating based on 115 reviews on Rotten Tomatoes,  and a "universal acclaim" score of 90 out of 100 based on 35 reviews on Metacritic. 

===Controversy and criticism===
The film was criticized by some  for its perspective on Christian–Jewish relations in Poland.     Some  have argued that the Christian Poles in the film are portrayed negatively as being anti-Semitic and sharing responsibility for the Holocaust with the German occupiers.    A letter of complaint has been sent by the Polish Anti-Defamation League to the Polish Film Institute, which provided significant funding for the film. A petition calling for the addition of explanatory title cards has apparently been signed by about 40,000 Poles; the film does not explicitly note that thousands of Poles were executed by the German occupiers for hiding or helping Jewish Poles.    Eric Abraham, one of the producers of Ida, responded "Are they really suggesting that all films loosely based on historical events should come with contextual captions? Tell that to Mr. Stone and Mr. Spielberg and Mr. von Donnersmarck," which refers to the directors of JFK, Lincoln, and The Lives of Others. 

Conversely, others  have argued that the character of Wanda Gruz, who participated in the persecution of those who threatened the Soviet-sponsored postwar regime, perpetuates a stereotype about Polish Jews as collaborators with the regime.      

===Influences=== David Thomson writes enthusiastically that seeing Ida is "like seeing Carl Dreyer’s The Passion of Joan of Arc for the first time" and that the relationship of Ida and her aunt Wanda is "worthy of the Bresson of Diary of a Country Priest."    The Passion of Joan of Arc (1928) is a silent film that is noted as one of the greatest films.   M. Leary has expanded on the influence on Ida: "The actress that plays Ida was apparently noticed at a cafe and drafted in as a blank canvas for this character, who becomes a mute witness in the film to the terror of Jewish genocide and the Soviet aftermath. She is a bit like Dreyers Joan in that her character is more about a violent march of history than her Catholic subtext."    Dana Stevens writes that Ida is "set in the early 1960s, and its stylistic austerity and interest in theological questions often recall the work of Robert Bresson (though Pawlikowski lacks—I think—Bresson’s deeply held faith in salvation)." 
 New Wave films such as the definitive French film The 400 Blows (directed by François Truffaut-1959) and the Polish film Innocent Sorcerers (directed by Andrzej Wajda-1960).       There are also similarities with Luis Buñuels Viridiana (1961). 

==Box office==
Grossing more than $3.6 million at the North American box office, the film has been described as a "crossover hit", especially for a foreign language film.  Nearly 500,000 people watched the film in France, making it one of the most successful Polish-language films ever screened there.  The film earned nearly as much in France, $3.2 million, as it did in the US. The film earned $0.3 million in Poland, and less than $0.1 million in Germany. 

==Accolades==
 
Ida was screened in the Special Presentation section at the 2013 Toronto International Film Festival    where it won the FIPRESCI Special Presentations award.    Among other festivals Ida won Best Film at Gdynia Film Festival|Gdynia, Warsaw International Film Festival|Warsaw, BFI London Film Festival|London, Camerimage|Bydgoszcz, Listapad|Minsk, Gijón International Film Festival|Gijón, GoEast|Wiesbaden, International Festival of Independent Cinema Off Plus Camera|Kraków. The film is also widely recognized for Agata Kuleszas and Agata Trzebuchowskas performances, and for the cinematography by Ryszard Lenczewski and Łukasz Żal.
 Polish Film Best Film Best European Spanish Academy Best Film Best Foreign Language Film. 
 Best Foreign Language Film category.
 31st Robert 7th Gaudí Awards).

The film has been selected by the European Parliament for the Lux Prize.

==Home media==
Ida has been released to DVD in both region 1 and region 2 with English subtitles.   It has also been released with subtitles in several other languages. In December 2014 the film was awarded the Lux Prize by the European Parliament; this prize supports subtitling of films into all of the 23 official languages of the European Union. 

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Polish submissions for the Academy Award for Best Foreign Language Film
* Reverse (film)|Reverse (2009) (an earlier Polish film portraying Communist Poland in the 1950s and 1960s)
* Aftermath (2012 Polish film)|Aftermath (2012) (a Polish thriller about reckoning with the destruction of a community of Polish Jews during World War II). Graham Fuller remarked, "Filmed in long shot, Anna and Wanda are dwarfed by the godless landscape, which inevitably evokes some of the woods and fields in Claude Lanzmanns Shoah (film)|Shoah and Władysław Pasikowski’s Aftermath." 

==References==
 

==External links==
*   at Music Box Films
*  
*  
*  
*  

 
{{Navboxes title = Awards for Ida list =
 
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 