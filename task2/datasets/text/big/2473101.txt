Showtime (film)
{{Infobox film
| name           = Showtime
| image          = Showtime movie eddie robert.jpg
| caption        = Theatrical poster
| director       = Tom Dey
| producer       = James Lassiter Will Smith Jane Rosenthal Jorge Saralegui
| writer         = Jorge Saralegui Keith Sharon
| starring       = Robert De Niro Eddie Murphy Rene Russo William Shatner
| music          = Alan Silvestri
| editing        = Billy Weber
| studio         = Village Roadshow Pictures
| distributor    = Warner Bros.
| released       =  
| runtime        = 92 minutes
| language       = English
| budget         = $85 million
| gross          = $77.7 million   
}}
Showtime is a 2002 action comedy film directed by Tom Dey and starring Robert De Niro and Eddie Murphy.

==Plot== Harry Callahan intentionally breaks a news camera and is subject to favors for the news channel as a result. In this film, Mitch breaks a news camera after a failed confrontation with a drug lord, who escapes by using a custom-built gun. Maxxis Television, the network that employed the cameraman, decides to sue the police department for $10 million, but will drop the lawsuit if Mitch agrees to star in a reality cop television show, which Trey later calls Showtime!.

Trey enters the picture shortly after, as an LAPD officer who actually wants to be an actor while also trying to become a detective. He pays a friend to snatch the purse of the shows producer, Chase Renzi (Rene Russo), and then retrieves it after a staged fight scene. Even though the deception is embarrassingly revealed, Chase is impressed and signs Trey on anyway. It is quickly revealed that the shows producers have little interest in filming an actual police officers existence; they build a mini-movie set in the middle of the station, and replace Mitchs nondescript personal car with a Humvee. They also hire William Shatner (who once played T. J. Hooker) to give both men tips on how to act. Trey is eager to learn, Mitch is merely annoyed.

Despite all this, Mitch tries to investigate the mysterious supergun, which is subsequently used by arms dealer Caesar Vargas to kill the drug dealer and his girlfriend. Through a clever ruse by Trey, they are able to get the arms dealers name from the dead dealers henchman. However, Vargas is less than cooperative, which causes a brawl at his nightclub. Trey and Mitch are able to defeat him and his henchmen, and subsequently have a relatively friendly conversation on their way home. Mitchs good humor evaporates when he finds that, in his absence, the Showtime producers have drastically remodeled his house and given him a retired K-9 dog as a pet. 

Vargas and his crew assault an armored car and kill the crew, then devastate the police who respond. Trey and Mitch arrive and are pulled into the shootout. When the attackers flee in a garbage truck, Mitch gives chase in a police car. In the ensuing mayhem, the car is rammed by the garbage truck, which winds up crashing into a construction site. Mitch, ironically, survives by jumping from the police car to Treys sports car (he had previously denounced "hood-jumping" as a useless skill). In the wake of the disaster, the police chief pulls the plug on the show, suspends Mitch from duty and demotes Trey back to patrol. 

With the show ended, Mitchs car is returned and his apartment restored (but he refuses to return the dog, of which he has grown fond). While watching the final episode, Mitch sees one of his police colleagues at Vargass nightclub. He and Trey investigate, finding that Vargas is selling the weapons at a gun show at the Bonaventure Hotel. Vargas flees with one of the weapons, taking Chase hostage in the process. The duo is able to rescue her, via a pocket pistol concealed in a Maxxis camera, but the ceiling of the room is shot. It is located just below the pool, so it floods, and Vargas is washed out the window, Trey manages to save himself and Mitch by handcuffing them together. They wind up suspended from a broken beam outside the hotel.

The movie ends with Trey promoted to detective, he and Mitch still working together with a new case, and there are hints of a romance between Chase and Mitch. Showtime is in its second season, this time with two female officers, who are just as antagonistic as Mitch and Trey were.

==Cast==
*Robert De Niro as Detective Mitch Preston
*Eddie Murphy as Officer Trey Sellars
*Rene Russo as Chase Renzi
*Pedro Damián as Cesar Vargas
*Drena De Niro as Annie
*William Shatner as Himself
*Mos Def as Lazy Boy

==Soundtrack==
{{Infobox album Name = Showtime: From And Inspired by The Motion Picture Type = soundtrack Artist =  Various Longtype = Digital download / Audio CD) Cover = Showtime - From And Inspired by The Motion Picture.jpg Released = March 15, 2002 Length = Label = MCA  Reviews =
}}
;Track list:
# "Caramel" – performed by Alias Project  
# "Why" – performed by Rude   Shaggy  
# "My Bad" – performed by Rayvon  
# "Lie Till I Die" – performed by Marsha Morrison  
# "Man Ah Bad Man" – performed by T.O.K.  
# "Money Jane" – performed by Baby Blue Soundcrew  
# "Your Eyes" – performed by Rik Rok  
# "Fly Away" – performed by Gordon Dukes  
# "Swingin" – performed by Shaggy  
# "Get the Cash" – performed by Howzing  
# "Still the One" – performed by Prince Mydas  
# "Showtime" – performed by Shaggy  

==Reception==
Showtime received negative reviews from critics. On Rotten Tomatoes the film has a rating of 25%. The sites consensus reads, "Showtime starts out as a promising satire of the buddy cop genre. Unfortunately, it ends up becoming the type of movies it is satirizing." On Metacritic the film has a score of 32 out of 100, based on 34 critics, indicating "generally unfavorable reviews".
 Razzie Awards for Worst Actor and Worst Screen Couple.

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 