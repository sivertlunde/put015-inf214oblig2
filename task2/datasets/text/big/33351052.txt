Mozart's Sister
 
{{Infobox film
| name = Mozarts Sister
| image = MozartsSister2010Poster.jpg
| alt = 
| caption = Film poster
| director = René Féret
| producer = René Féret Fabienne Féret
| writer = René Féret
| starring = Marie Féret
| music = Marie-Jeanne Serrero
| cinematography = Benjamín Echazarreta
| editing = Fabienne Féret
| studio = Les Films Alyne
| distributor = JML Productions
| released =  
| runtime = 120 minutes
| country = France
| language = French
| budget = United States dollar|$4.5 million    (Euro sign|€3.4 million)
| gross = $839,654 
}}

Mozarts Sister (French title: Nannerl, la sœur de Mozart) is a 2010 French drama film written and directed by René Féret and starring two of his daughters. It presents a fictional account of the early life of Maria Anna Mozart, nicknamed Nannerl, who was the sister of Wolfgang Amadeus Mozart and his only sibling to survive infancy.

==Plot== Princess Louise Versailles with King Louis XV. A bizarre final encounter with the Dauphin and his new wife ensues. Nannerl and  Princess Louise reflect on how their fates would have differed had they been born male.

==Cast== Nannerl Mozart
*   as Leopold Mozart
*   as Anna Maria Mozart Wolfgang Mozart Le Dauphin Louise de France Victoire de France
* Dominique Marcas as Abbess
* Salomé Stévenin as Isabelle dAubusson
* Nicolas Giraud  as Master of Music at Versailles

==Reception==
Mozarts Sister received generally positive reviews, holding a 75% rating on Rotten Tomatoes.  On Metacritic, which uses an average of critics reviews, the film has 71/100, indicating "generally favorable reviews". 

==Home video==
In the United States, Mozarts Sister was released on DVD and Blu-ray Disc by Music Box Films. Each DVD and Blu-ray Disc includes a music CD with the films soundtrack, composed by Marie-Jeanna Serero. The music CDs tracks are:
 
#Le Voyage
#Louise de France
#Concert a labbaye
#Le livre maudit
#Tendresse
#Le Violon du Dauphin
#Le Do Magique
#Versailles
#Le Voyage
#La Gifle
#Chant Versailles
#Improvisation
#Dauphin Lettre 1
#La Mer
#Nannerl compose
#Concert Nannerl - 1st movement
#Concert Nannerl - 2nd movement
#Concert Nannerl - 3rd movement
#Dauphin Lettre 2
#Comptine Leopold
#Le Catafalque
#Dernier repas
#Nannerl brule ses partitions
#Generique de fin
 

==See also==
* Nannerl Notenbuch
* Cross-dressing in film and television

==References==
 

==External links==
*    
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 