Suvarna Sundari
{{Infobox film
  | name = Suvarna Sundari
  | image = Suvarna Sundari.jpg |
  | caption =
  | director = Vedantam Raghavaiah
  | producer = P. Adinarayana Rao
  | writer = Malladi Ramakrishna Sastry   (Dialogues )  Vedantam Raghavaiah   (screenplay)  Rajanala
  | music = P. Adinarayana Rao
  | cinematography =
  | editing =
  | distributor =
  | released = 10 May 1957
  | runtime = 209 min.
  | language =  Telugu
  | budget =
  | preceded_by =
  | followed_by =
  }}
 Telugu film produced in 1957 in the golden era of Telugu film industry. 
 Relangi and Gummadi in the lead roles. It was later made in Hindi starring Ranjan and Anjali Devi in lead roles. The songs "Piluvakura Alugakura" & its Tamil Version "Azhaikadhe" (P Susheela) in Raga Hindolam are still very popular semi classical songs. 

The shooting of the film was held at Venus Studios, Madras and the outdoor shooting was held at Shimsa Falls, Mysore.

The film was released through Chamria Talkie Distributors headed by Sundar Lal Nahata, set a record at the box-office by celebrating 50 days in 48 centres and completed 100 days in 18 centres.

The film was remade in Hindi and Tamil languages. The music was a great success and P. Adinarayana Rao won the best music director in Hindi. 

==Crew==
* Art: Vaali.
* Choreography: Vempati Satyam.
* Cinematography: M.A. Rahman.
* Costumes: B.Narayana Murthy.
* Dialogues: Malladi Ramakrishna Murthy.
* Editing: N.S. Prakash.
* Lyrics: Samudrala Raghavacharya, Samudrala Ramanujacharya, Kosaraju.
* Make-up: K.Haribabu, A.Nageswara Rao, Potharaju.
* Music: P. Adinarayana Rao.
* Processing: Vijaya Laboratories.
* Re-recording: Swaminathan, Vahini Studios.
* Sound: J. Suryanarayana.
* Sound Recording: Vahini and Revathi Studios.
* Story: Sadasiva Brahmam and Adinarayana Rao.

==Synopsis==
This is a Folklore story of a Gandharva kanya loving a human prince. Jayanth (ANR) gets ousted from the kingdom. He meets Suvarna Sundari (Anjali Devi) who comes to earth on every Kaarteeka Pournami day. They love each other. Lord Indra comes to know about their love and curses Suvarna Sundari. As a result Jayanth forgets her. She loses her child near the river. How the two protagonists reunite forms the story.

==Songs==
* "Ammaa, Ammaa! Ani Adilevu Baabu!"
** Lyric: Samudrala Ramanujacharya
** Playback: Ghantasala Venkateswara Rao
** Cast: ANR, Master Babji
* "Bangaaru Vannela Rangaaru Sanjala Rangeli Yetenchene"
** Lyric: Samudrala Raghavacharya
** Playback: P. Leela
** Cast: Suryakala
* "Bommaalammaa Bommalu"
** Lyric: Samudrala Raghavacharya
** Playback: P. Susheela
** Cast: Anjali Devi
* "Eraa, Manatoti Geliche Veerulevvaruraa"
** Lyric: Kosaraju Raghavayya Chowdary
** Playback: Pithapuram Nageswara Rao, Chorus
** Cast: Relangi Venkataramayya, Ramana Reddy, Balakrishna
* "Haayi Haayigaa Aamani Saage"
** Lyric: Samudrala Raghavacharya
** Raagam: ragamalika (sOhiNi/ hamsanandi, bahaar jaunpuri, yaman)
** Playback: P. Jikki Krishnaveni, Ghantasala Venkateswara Rao
** Cast: ANR, Anjali Devi
* "Jagadeeswaraa! Paahi, Parameswaraa!"
** Lyric: Samudrala Raghavacharya
** Raagam: sindhubhairavi
** Playback: P. Susheela or Jikki (two versions)
** Playback : P.Jikki Krishnaveni
** Cast: Anjali Devi
* "Lakshmeem Ksheerasamurdraraajatanayaam" (Slokam)
** Playback: Ghantasala Venkateswara Rao
** Cast: Akkineni Nageswara Rao
* "Naa Chitti Paapaa, Naa Kanna Paapaa!"
** Lyric: Samudrala Raghavacharya
** Playback: P. Susheela, M. S. Ramarao
** Cast: Anjali Devi, Gummadi Venkateswara Rao
* "Naa Nomu Pande Yeenaade"
** Cast: Raja Sulochana
* "Nee Needalona Nilichenuraa"
** Lyric: Samudrala Raghavacharya
** Raagam: jayajayavanti (dwijaavanti)
** Playback: P. Susheela
** Cast: Anjali Devi
* "Piluvakuraa Alugakuraa Nalugurilo Nanu, O Raajaa!"
** Lyric: Samudrala Raghavacharya hindOLam
** Playback: P. Susheela
** Cast: Anjali Devi
* "Poobaala Pelli Cheddaamu Randi"
** Playback: P.Leela
** Cast: Girija and others
* "Sambho, Naa Mora Vinavaa!"
** Playback: P. Susheela
** Cast: Anjali Devi
* "Tadheem Nanana Thom Tillaanaa"
** Cast: Raja Sulochana, Girija

==References==
 

==External links==
*  

 
 
 