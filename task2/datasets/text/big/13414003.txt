Sringaram
{{Infobox film
| name           = Sringaram
| image          = Sringaram Poster.jpg
| caption        = Film poster
| director       = Sharada Ramanathan
| producer       = Golden Square Films
| story          = Sharada Ramanathan
| screenplay     = Indra Soundar Rajan
| starring       =  
| music          = Lalgudi Jayaraman
| cinematography = Madhu Ambat
| editing        = A. Sreekar Prasad
| studio         = Golden Square Films
| distributor    = GV Films 
| released       =  
| runtime        = 117 minutes
| country        = India Tamil
| budget         = 
| gross          = 
| preceded_by    =
| followed_by    =
}} Tamil period piece|period-drama film directed by debutante Sharada Ramanathan, a cultural activist. Co-written by novelist Indra Soundar Rajan and produced by Padmini Ravi, herself a dancer,   it stars Aditi Rao Hydari, in her feature film debut, in the lead role alongside Manoj K. Jayan, Hamsa Moily and Y. Gee. Mahendra. The film depicts the life of a Devadasi during the 1920s.  Sringaram became highly critically acclaimed, winning three prizes at the 53rd National Film Awards and two Tamil Nadu State Film Awards,   besides several screenings at various international film festivals, much before its theatrical release on 5 October 2007. 

==Plot==

 

==Cast==
* Aditi Rao Hydari as Madhura and Varshini
* Manoj K. Jayan as Sukumar, the Mirasu
* Hamsa Moily as   Kama
* Shashikumar as Kasi
* Manju Bhargavi as Ponammal
* Y. Gee. Mahendra as the Gurukkal
* Aishwarya Sivachandran as Mirasu’s wife
* Vagai Chandrasekhar as Kangani
* Bharat Kalyan as Manisundaram
* Sindhu as Saroja
* Junior Balaiya as Koilpillai
* Lakshmi Ravi as Ambujam

==Accolades==
; 53rd National Film Awards  Best Cinematography - Madhu Ambat Best Music Direction - Lalgudi Jayaraman Best Choreography - Saroj Khan

; Tamil Nadu State Film Awards 2005  Best Art Direction - Thotta Tharani Best Costume Design - Rukmini Krishnan

The film was screened at following film festivals:
* 37th International Film Festival of India (IFFI) 
* 8th Dubai International Film Festival  
* Adelaide OzAsia Festival 2008  
* 4th Indian Film Festival of Los Angeles   
* Kerala Film Festival  
* Thrissur International Film Festival  
* 2nd India International Women Film Festival  
* Dance on Camera Festival 2006   
* 3rd Indo-German Film Festival 

== Soundtrack==

The films score and soundtrack were composed by renowned violinist Padma Bhushan Lalgudi Jayaraman. The soundtrack album features 14 tracks, most of them sung by carnatic musicians. Lyrics were penned by Swati VAR. Jayaraman eventually won a National Film Award for Best Music Direction for the films score.  Sringaram is notably his first and till date only work for a feature film. 

# Title Music
# Mallari - Injukkudi Brothers
# Nattu Purappadal (Folk Song) - T. L. Maharajan & O. S. Arun Bombay Jayashri Ramnath
# Mudal Mariyadai (Salutation) - Swati Srikrishna & Hamsi
# Mamara Thopila - O. S. Arun
# Ninaival Yennai - Lalgudi Vijayalakshmi
# Three Seasons - GJR Krishnan, Lalgudi Vijayalakshmi, Swati, Revathy Meera & Orchestra
# Yen Indha Mayamo (Hamir Kalyani - Solo) - S. Sowmya
# Akaram- Swati Srikrishna & Hamsi
# Mudal Mariyadai (Salutation) - Swati Srikrishna & Hamsi
# Nattu Purappadal (Folk Song) - T. L. Maharajan & O. S. Arun
# Harathi - Swati Srikrishna, Meera & Hamsi
# Three Seasons - GJR Krishnan, Lalgudi Vijayalakshmi, Swati, Revathy Meera & Orchestra

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 