Daddy's Deadly Darling
 
{{Infobox Film
  | name = Daddys Deadly Darling
  | image = TromaPigs.jpg
  | caption = Poster for Pigs A.K.A. Daddys Deadly Darling
  | director = Marc Lawrence 
  | producer = Marc Lawrence Donald Reynolds
  | writer = Marc Lawrence
  | starring = Toni Lawrence Marc Lawrence Jesse Vint Paul Hickey Katharine Ross
  | music = Charles Bernstein
  | cinematography = Irv Goodnoff Glenn Roland 
  | editing = Irv Goodnoff
  | distributor = Troma Entertainment
  | released = August 16, 1972 
  | runtime = 80 minutes
  | language = English
  | budget =  
  | preceded_by = 
  | followed_by = 
  }}

Daddys Deadly Darling (also titled Pigs, Daddys Girl, The Strange Love Exorcist and Roadside Torture Chamber) is a 1972 horror film directed by Marc Lawrence and currently distributed by Troma Entertainment. 

Toni Lawrence plays Lynn Webster, an escapee from a mental hospital who takes refuge in a local farm inhabited by Mr. Zambrini and his pack of flesh-eating pigs. When Lynn starts killing people who remind her of her abusive father, Zambrini feeds the remains to his sadistic sows. However, law enforcement starts to catch up with the two of them. 

Tagline: Once the pigs tasted blood...no one could control their hunger!!

==External links==
* 
*  – at the Troma Entertainment movie database

 
 
 
 

 