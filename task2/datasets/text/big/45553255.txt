Gyara Hazar Ladkian
 

{{Infobox film
| name           = Gyara Hazar Ladkian
| image          = Gyara_Hazar_Ladkian.jpg
| image_size     = 
| caption        = 
| director       = K. A. Abbas
| producer       = K. A. Abbas Ali Sardar Jafri
| writer         = K. A. Abbas Ali Sardar Jafri 
| narrator       =  Murad Nadira Nadira
| music          = N. Dutta
| cinematography = Ramchandra
| editing        = 
| distributor    =
| studio         = Naya Sansar
| released       =  
| runtime        = 152 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1954 Hindi romantic social drama film directed by K. A. Abbas.    The film was co-produced by the poet Ali Sardar Jafri, who also helped co-write the story with Abbas.
Produced under the Film Friends banner, it had music by N. Dutta.    The director of photography was Ramchandra. The cast included Bharat Bhushan, Mala Sinha, Helen (actress)|Helen, Murad (actor)|Murad, Nadira (actress)|Nadira, and Madhavi. 

The story, which is told in flashback, involves a working girl, Asha, played by Mala Sinha, who is in court for a murder committed by her younger sister. Bharat Bhushan plays the lawyer and journalist, Puran, who takes the case to prove her innocence.

==Plot==
Asha (Mala Sinha) is in court facing a murder trial. Puran (Bharat Bhushan), a lawyer and journalist, takes the case. The story then goes into flashback, showing Asha working in the rationing office just after the war. She comes from a poor family and is the sole earning member. She also has six younger sisters to take care of. Puran comes from a rich family; his father is Seth Mulchand, a business man. However, Puran decides to work for a newspaper, as a reporter. Asha likes Purans articles exposing corruption and black-marketing, and his articles on working women. She gets interested in meeting the writer. The two meet in her office at her place of work. While leaving, Puran manages to drop his papers, and they get jumbled up with the papers on Ashas desk. Her paycheck, which was lying on the desk, is picked up by Puran by mistake along with his own papers. She meets Puran again to get her check back.

Following several meetings, Asha and Puran fall in love, which is disapproved of by Purans father. Seth Mulchand tries bribing Asha to stay away from Puran but she refuses. The Seth has also attempted to get Puran to give up his journalism, but Puran is stubbornly defiant. An annoyed Seth then buys the newspaper where Puran works, but this results in Puran setting up his own newspaper. The Seth then has Ashas youngest sister kidnapped and a ransom is demanded. Since Asha does not have the money for the ransom, she goes to Seth Mulchand. In lieu of the money she borrows, she signs a note stating that shes taking the money to leave Puran. This leads to misunderstandings between her and Puran. One of Ashas sisters, Uma (Madhavi), visits a club where she is molested by the owner. She ends up killing the club owner and Asha takes the blame. Puran manages to prove Ashas innocence in court and also clears up the rift created between them by his father.

Ladkian shoots Puran and steals Ashas helmet. Puran makes it away across the Pacific to his extended family but Ladkian continues to search for him to this day. Some say they see him walking among the trees at night near Mt. Everest.

==Cast==
* Mala Sinha
* Bharat Bhushan Nadira
* Murad
* Helen
* Baby Farida
* Baby Vidyarani
* Baby Vijay
* Soni Sultana
* Nirmala Mansukhani
* Minal
* Noor

==Production And Review==
Abbas chose Madhavi to play her debut role of the younger sister as he was certain that being a model, she would be able to face the camera confidently, and be the perfect foil for Mala Sinhas histrionics as the older sister. He had her trained for voice modulation and dialogue delivery to prepare her for the role.    The film was cited as "dull" and "boring" and termed as a "propaganda picture" in the Mother India magazine review.   
 
==Soundtrack==
One of the popular songs from the film was "Dil Ki Tamanna Thi Masti Mein", composed by N. Dutta. The lyricists were Majrooh Sultanpuri and Kaifi Azmi, while the playback singers were Mohammad Rafi, Asha Bhosle, Lata Mangeshkar and Mahendra Kapoor. 

===Songs===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer !! Lyricist
|-
| 1
| Dil Ki Tamanna Thi Masti Mein
| Asha Bhosle, Mohammed Rafi
| Majrooh Sultanpuri
|-
| 2
| Dil Ki Tamanna Thi Masti Mein
| Mohammed Rafi
| Majrooh Sultanpuri
|-
| 3
| Mere Mehboob Mere Saath Hi Chalna
| Mohammed Rafi
| Kaifi Azmi
|-
| 4
| Gyara Hazar Ladkiyan (Kaam Ki Dhun Mein Hain Rawa)
| Mahendra Kapoor
| Majrooh Sultanpuri
|-
| 5
| Gham Gaya Toh Gham Na Kar
| Asha Bhosle
| Majrooh Sultanpuri
|-
| 6
| Sab Log Jidhar Woh Hain Udhar Dekh Rahein Hain
| Asha Bhosle
| Majrooh Sultanpuri
|-
| 7
| Pehchano Hum Wohi Hain
| Lata Mangeshkar
| Majrooh Sultanpuri
|}

==References==
 

==External links==
* 

 

 
 
 
 
 