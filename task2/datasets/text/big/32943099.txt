The Hordern Mystery
 
 
{{Infobox film
  | name     = The Hordern Mystery
  | image    = 
  | caption  = 
  | director = Harry Southwell		
  | producer = Harry Southwell
  | writer   = M.F. Gatwood
| based on = novel by Edmund Finn
  | starring = Claude Turton Flo Little
  | music    = 
  | cinematography = Tasman Higgins
  | editing  = 
| studio = Southwell Screen Plays
  | distributor = 
  | released = 23 October 1920 
  | runtime  = 5,600 feet
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}

The Hordern Mystery is an Australian feature length film directed by Harry Southwell based on an 1889 novel by Edmund Finn (son of Edmund Finn).

Unlike many Australian silent films, it still survives today.

==Plot==
Money-hungry Gilbert Hordern is married to an adoring wife and has a child. He pretends to be his own evil twin brother so he can marry a millionaires daughter. He succeeds but is wracked with guilt and confesses. He wakes up and realises it was all a dream.

==Cast==
*Claude Turton as Gilbert Hordern
*Flo Little as Midge Hordern
*Floris St George as Laura Yellaboyce
*Godfrey Cass as Dan Yellaboyce
*Thomas Sinclair as Peter Mull
*Beatrice Hamilton as Mrs Mull
*David Edelsten

==Production==
The film was shot in suburban Sydney in mid 1920 under the title The Golden Flame. Commercial reception appears to have been poor. 

==References==
 

==External links==
*  
*  at National Film and Sound Archive

 

 
 
 
 
 


 