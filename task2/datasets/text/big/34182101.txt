Bettada Jeeva
{{Infobox film |
  name           = Bettada Jeeva |
  image          = |
  alt            =   |
  caption        = Dattatreya & Rameshwari Varma, in Movie poster |
  director       = P. Sheshadri |
  producer       = Basanta Kumar Patil |
  writer         = K. Shivaram Karanth |
  based on       =   |
  starring       = Suchendra Prasad Rameshwari Varma H. G. Dattatreya |
  music          = V. Manohar |
  cinematography = Anant Urs |
  editing        = B. S. Kemparaju |
  distributor    = Basanth Productions |
  released       =   |
  language       = Kannada|
  country        = India |
}}
Bettada Jeeva is a National Award winning (2011) Kannada film directed by P. Sheshadri starring Suchendra Prasad, Rameshwari Varma and H. G. Dattatreya. The story is based on Jnanpith awardee K. Shivaram Karanths novel. It depicts the tradition of people living in remote areas of Western Ghats during pre-Independence days in India.

==Story==
The story happens in around early forties. A young freedom fighter Shivaramu, lost in jungle while he travels to a place near subramanya, meets two villagers called Deranna and Bhatya. They advice Shivaramu to stay in the house of Gopalayya, a Brahmin living with his wife Shankaramma in Kelabailu. The couple treats Shivaramu well and asks him to stay for couple of more days. They express that their son Shambhu had left them years back. They treat Shivaramu as their own son.

During the stay, Gopalayya narrates the story of his past. Recalls his hardworking days of making a beautiful crop field beside mountain. Shivaramu slowly gets clear introduction to the legendary personality of Gopalayya. Shivaramu gets more curious at his attitude of fighting against nature to build his dreams in a place called Katumule. Meanwhile Shivaramu meets Narayana, his wife Laxmi and their kids, living in Katumule. They feel that Shivaramu could be a person sent by Shambhu. Narayana also express that Shambhu’s return may cause them to leave Katumule. So, Narayana plans to leave before that critical moment come and thinks of having his own land. Couple of day’s later Gopalayya says about his decision to transfer the property of Katumule to Narayana.

Later while talking to Shivaramu, everyone gives their own explanation for Shambhu’s disappearance. Laxmi talks about an incident which could be the reason for leaving. Narayana describes Shambhu as a crooked minded. Gopalayya describes that opposing Shambhu’s decision to join freedom movement may be the reason for him to leave the place. At the same time Shankaramma also blames herself to be the reason for his riddance. Each character explores their hidden feelings in their own way with Shivaramu.

Once Shivaramu finds Shambhu’s photo and identifies him that he was also a comrade with him in freedom fighting, says he would try to find out Shambhu. When Shivaramu comes across an incident of villagers hunting a tiger, he requests and stops them by killing it. Finally he leaves the place Kelabailu, with a promise of finding Shambhu.

==Awards==
*          

==References==
 

== External links==
*  
*  

 

 
 
 
 
 
 


 