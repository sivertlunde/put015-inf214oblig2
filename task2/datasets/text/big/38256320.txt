Militiaman Bruggler
{{Infobox film
| name           = Home Guardsman Bruggler
| image          =
| image_size     =
| caption        =
| director       = Werner Klingler 
| producer       = Peter Ostermayr
| writer         =  Anton Graf Bossi-Fedrigotti (novel)   Joseph Dalman
| narrator       =
| starring       = Ludwig Kerscher  Franziska Kinz   Rolf Pinegger   Eduard Köck
| music          = Herbert Windt   
| cinematography = Alexander von Lagorio   Karl Attenberger   Sepp Allgeier
| editing        = Paul May
| studio         = Universum Film AG
| distributor    = Universum Film AG
| released       = 28 August 1936
| runtime        = 
| country        = Germany German
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} Tyrol during the First World War and depicts the actitivies of the local home guard unit.  It was based on a novel by Anton Graf Bossi-Fedrigotti,  itself based on the diary of the titular Anton Bruggler. 

==Partial cast==
* Ludwig Kerscher as Toni Bruggler 
* Franziska Kinz as Mother Bruggler 
* Rolf Pinegger as Grandfather Bruggler
* Eduard Köck as Anderl Theissbacher 
* Viktor Gehring as Waldemar 
* Friedrich Ulmer as Hans Oberwexer 
* Beppo Brem as Jorgl Trimml 
* Lola Chlud as Countess von Teuff 
* Gustl Gstettenbaur as Bartl Theissbacher 
* Hans Hanauer as Sepp Thaler 
* Fritz Hofbauer as Sebastian Mutschechner
* Willy Schultes as Hannes Baumgartner

==References==
 

==Bibliography==
* Richards, Jeffrey. Visions of Yesterday. Routledge, 1973.
* 
* 

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 