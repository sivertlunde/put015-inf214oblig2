Pattampoochi
 
{{Infobox film
| name           = Pattampoochi
| image          = 
| caption        = 
| director       = A. S. Pragasam
| producer       = P. Sreenivasan
| writer         = A. S. Pragasam
| starring       = Kamal Haasan Jayachitra Nagesh V. K. Ramasamy (actor)|V. K. Ramasamy
| music          = P. Sreenivasan
| cinematography = J. G. Vijayam
| editing        = K. Narayanan
| studio         = Sri Prakash Productions
| distributor    = Sri Prakash Productions
| released       =  
| runtime        = 148 mins
| country        =   India Tamil
| budget         = 
}}
 1975 Tamil language drama film directed by A. S. Pragasam. The film features Kamal Haasan and Jayachitra in lead roles. The movie was a box office success. 

==Cast==
* Kamal Haasan
* Jayachitra
* Nagesh
* V. K. Ramasamy (actor)|V. K. Ramasamy Manorama  
* Senthamarai

==Soundtrack==
The music composed by P. Sreenivasan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics
|-
| 1 || Ethanai Malargal || T. M. Soundararajan, S. Janaki || Kannadasan
|- Pulamaipithan
|-
| 3 || Madhana Kamaraja || P. Susheela
|-
| 4 || Pasi Edukkum Neram || S. P. Balasubrahmanyam, S. Janaki
|-
| 5 || Sakkarai Pandhalil || T. M. Soundararajan, P. Susheela || Kannadasan
|}

==References==
 

 
 
 
 


 