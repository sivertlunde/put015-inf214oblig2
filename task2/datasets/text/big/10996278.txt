Le bambole
{{Infobox film
| name           = Le bambole
| image          =Le bambole.jpg
| image_size     =
| caption        =  Franco Rossi
| producer       = Gianni Hecht Lucari
| writer         = Rodolfo Sonego
| narrator       = 
| starring       = Nino Manfredi Monica Vitti 
| music          = Armando Trovajoli
| cinematography = Leonida Barboni Ennio Guarnieri
| editing        = 
| distributor    = Royal Films International 
| released       = 1965
| runtime        = 107 mins.
| country        =  Italian
}} 1965 Italy|Italian comedy film in four segments; cast includes Virna Lisi, Nino Manfredi, Gina Lollobrigida, Elke Sommer and Monica Vitti.  

The four vignettes—The Telephone Call (La Telefonata), Treatise on Eugenics (Il Trattato di eugenetica), The Soup (La Minestra) and Monsignor Cupid (Monsignor Cupido)—concern secrets of love and secret lovers.    The fourth segment is based on a tale of Boccaccios The Decameron.   

==Cast==
*Nino Manfredi: Giorgio   
*Monica Vitti: Giovanna   
*Elke Sommer: Ulla   
*Gina Lollobrigida: Beatrice   
*Virna Lisi: Luisa   
*Orazio Orlando: Richetto   
*Maurizio Arena: Massimo   
*Jean Sorel: Vincenzo   
*Piero Focaccia: Valerio   
*Gianni Rizzo: Il direttore dalbergo   
*Akim Tamiroff: Monsignor Arcudi

==Notes==
 

== External links ==
*  
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 


 
 