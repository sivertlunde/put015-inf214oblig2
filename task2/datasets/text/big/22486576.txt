Adata Vediya Heta Hondai
 
{{Infobox Film | name = Adata Wadiya Heta Hondai
 | image = 
 | imagesize = 
 | caption =
 | director = M. Masthan
 | producer = K. Gunaratnam
 | Screenplay = K. Hugo Fernando
 | starring = Gamini Fonseka, Jeevarani Kurukulasooriya, Ananda Jayaratne, Sandhya Kumari, Vijitha Mallika
 | music = Karunaratne Abeysekera (lyrics) M. K. Rocksamy (direction) W. D. Amaradeva (composition)
 | cinematography = 
 | editing = 
 | distributor = 
 | country    = Sri Lanka
 | released =   March 26, 1963 
| runtime = Sinhala
 | budget = 
 | followed_by =
}}

Adata Vediya Heta Hondai (Sinhala, Tomorrow Is Better Than Today) is a 1963 Sri Lankan film starring Gamini Fonseka and Jeevarani Kurukulasooriya. It was a box office success in the country.   

== Plot ==
Love story.

Cast

*Gamini Fonseka
*Jeevarani Kurukulasoorya
*Ananda Jayaratne
*Sandhya Kumari
*Vijitha Mallika
*Nelson Karunagama
*Ignatius Gunaratne

== Songs ==

*"Sandun Gase" &ndash; J. A. Milton Perera and Mallika Kahawita
*Sethsiri Sethsiri" &ndash; Latha and chorus
*"Soka Susum Marathe" &ndash; Latha Walpola
*"Baloli Loli" &ndash; Indrani Wijebandara
*"Baloli Loli" &ndash; Latha Walpola
*"Rasadun Netha Di" &ndash; Latha Walpola
*"Amma" &ndash; Latha Walpola and chorus

==Notes==
 

 
 


 