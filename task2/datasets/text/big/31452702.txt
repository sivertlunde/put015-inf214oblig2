The Double (2011 film)
 
{{Infobox film
| name           = The Double
| image          = The Double Poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Michael Brandt
| producer       = Ashok Amritraj
| writer         = Derek Haas Michael Brandt
| starring       = Richard Gere   Topher Grace   Stephen Moyer   Odette Yustman   Stana Katic   Chris Marquette   Tamer Hassan   Martin Sheen
| music          = John Debney
| cinematography = Jeffrey L. Kimball
| editing        = Steve Mirkovich
| studio         = Hyde Park Entertainment Imagenation Abu Dhabi
| distributor    = Image Entertainment
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  $3,696,232  	 
}}

The Double is a 2011 spy film, directed by Michael Brandt and starring Richard Gere and Topher Grace. It was released on October 28, 2011.

==Plot==
Two FBI agents are surveilling a warehouse. As a U.S. senator Dennis Darden (Ed Kelly) walks out of the door, he is approached by an assassin from behind who slits his throat and escapes. The agents rush to the scene to find the man dead. However, they could not identify the assassin as he committed the murder in darkness. Later, CIA officers arrive on the scene and take charge. 

Retired operative Paul Shepherdson (Richard Gere) is summoned by CIA director Tom Highland (Martin Sheen) to look into the murder. He is introduced to a young FBI agent, Ben Geary (Topher Grace), who is an expert on a former Soviet operative known as Cassius, whom Geary predicts to have committed the murder due to his signature throat-slitting method. 

Paul and Ben visit Brutus (Stephen Moyer), one of Cassiuss proteges, who had been locked up in prison, to learn the whereabouts of Cassius. They provide him with a radio and leave. The prisoner then swallows the batteries from the radio and fakes an upset stomach. Upon arriving at a hospital, he spits out the batteries, overpowers the medical staff, and escapes. In the basements garage, he is attacked by Paul, who reveals himself to be Cassius. Cassius slits his throat. Upon investigating the crime scene, Ben grows suspicious of Paul. Meanwhile, a Russian terrorist and murderer, Bozlovski (Tamer Hassan), has entered the U.S. 

As the investigation deepens, Paul warns Ben to pull out, due to the possibility of harm to his family. Ben, who has become obsessed with the idea that Paul is Cassius, starts his own parallel investigation. Meanwhile, Paul tries to contact Bozlovski in a factory where he escapes after an intense firefight. Ben examines another throat-slitting murder of Bozlovskis associate at the same site and is now convinced Paul is Cassius.

Ben pieces together the events of Pauls life and determines that not only is Paul actually Cassius, but also that he is systematically murdering the people involved in the death of his wife and child, who were assassinated by Bozlovski.

Paul has now tracked down Bozlovski to a shipyard warehouse. A while later, Ben also arrives at the plant. After being confronted with the evidence, Paul confesses everything. Paul then confronts Ben with the fact that Ben is a Russian spy, which Paul learned at one of Bens informant drop-offs.  He is able to convince Ben that Bozlovski is the actual threat. When Ben reveals that he has plans to return to Russia after this is over, Paul tries to convince him to stay in the FBI and with the family he has grown to love.

Together they hunt down Bozlovski inside the shipyards warehouse. Bozlovski attacks Paul and Ben, and in the ensuing struggle, a mortally wounded Paul slits Bozlovskis throat using his garrote-watch.  However, Paul himself later succumbs to his own injuries. As the only witness, FBI agent Ben relays the incident to his superiors and claims that Bozlovski was Cassius, thereby securing Pauls reputation and recognizing his heroism. As Ben departs, the CIA director Highland asks him whether he would ever consider working at the CIA.

The film ends with Ben returning to his home.

==Cast==
* Richard Gere as Paul Shepherdson
* Topher Grace as Ben Geary
* Martin Sheen as Tom Highland
* Tamer Hassan as Bozlovski 
* Stephen Moyer as Brutus
* Chris Marquette as Oliver Odette Yustman as Natalie Geary 
* Stana Katic as Amber
* Jeffrey Pierce as Agent Pierce 
* Nicole Forester as Molly
* Ed Kelly as Senator Dennis Darden Lawrence Gilliard Jr. as Agent Burton
* Randy Flagler as Martin Miller
* Yuri Sardarov (as Yuriy Sardarov) as Leo

==Reception==
The film received mostly negative reviews from critics, earning it a 20% on Rotten Tomatoes based on 43 reviews. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 