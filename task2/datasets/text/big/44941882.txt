Pirate's Passage
 
 
{{Infobox film
| name           = Pirates Passage
| image          = 
| alt            = 
| caption        = 
| film name      =  
| director       = Mike Barth   Jamie Gallant
| producer       = Brad Peyton   Donald Sutherland
| writer         =  
| screenplay     = Brad Peyton   Donald Sutherland
| story          = 
| based on       =  
| starring       = Donald Sutherland   Gage Munroe   Carrie-Anne Moss
| narrator       =  
| music          = Andrew Lockington
| cinematography = 
| editing        = Jef Harris   Paul Neumann
| studio         = Martins River Ink   PiP Animation Services
| distributor    = Tandem Communications
| released       =  
| runtime        = 90 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          =  
}}
 Canadian animation|animated adventure film based on a novel by William Gilkerson. It premiered on CBC Television on 4 January 2015.

==Plot==
The story is set in the fictional community of Grey Rocks, Nova Scotia. where Jim (Gage Munroe), a 12-year-old boy, faces bullying at school. His widowed mother (Carrie-Anne Moss) runs a local inn which is facing a takeover by a wealthy family. Sea captain Charles Johnson (Donald Sutherland) appears to assist Jim and his mother through their struggles, however Johnson appears to be a pirate who supposedly died two centuries ago.  

The story is based on William Gilkersons book Pirates Passage which won the 2006 Governor Generals Award for English-language childrens literature.   

==Voice cast==
* Donald Sutherland as Captain Charles Johnson
* Gage Munroe as Jim Hawkins
* Carrie-Anne Moss as Kerstin Hawkins
* Megan Follows as Meg O’Leary
* Kim Coates as Roy Moehner
* Colm Feore as Corporal Robin Hawkins
* Gordon Pinsent as Harry the Barber
* Paul Gross as Calico Jack
* Rossif Sutherland as Klaus Moehner
* Terry Haig as Mr. Herkes

==Production==
Pirates Passage was produced and written by Sutherland and Brad Peyton through Sutherlands production company Martins River Ink and the studios of PiP Animation Services.       

Tandem Communications distributes the film outside of Canada.   

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 