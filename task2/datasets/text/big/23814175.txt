Among Grey Stones
 
{{Infobox film
| name           = Among Grey Stones
| image          =
| caption        =
| director       = Kira Muratova
| producer       =
| writer         = Vladimir Korolenko
| starring       = Igor Sharapov
| music          =
| cinematography = Aleksei Rodionov
| editing        =
| distributor    =
| released       = 1983
| runtime        = 88 minutes
| country        = Soviet Union
| language       = Russian
| budget         =
}}

Among Grey Stones ( , Transliteration|translit.&nbsp;Sredi serykh kamney) is a 1983 Soviet drama film directed by Kira Muratova. The film suffered a lot from the Soviet censorship and was edited without the acceptance of Muratova, so she refused to release it under her name - it was attributed to "Ivan Sidorov" (a common Russian name and surname).
It was screened in the Un Certain Regard section at the 1988 Cannes Film Festival.   

==Cast==
* Igor Sharapov
* Oksana Shlapak
* Stanislav Govorukhin - Judge
* Roman Levchenko Sergei Popov Viktor Aristov
* Viktor Gogolev
* Fyodor Nikitin
* Vladimir Pozhidayev
* Nina Ruslanova

==References==
 

==External links==
* 

 
 
 
 
 

 