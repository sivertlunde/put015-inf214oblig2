The Unjust
{{Infobox film name           = The Unjust  image          = The Unjust film poster.jpg director       = Ryoo Seung-wan producer       = Koo Bon-han   Kim Yun-ho   Ryoo Seung-wan   Kang Hye-jung    Han Jae-duk  writer         = Park Hoon-jung  starring       = Hwang Jung-min   Ryoo Seung-bum   Yoo Hae-jin music          = Jo Yeong-wook cinematography = Chung Chung-hoon  editing        = Kim Sang-bum   Kim Jae-beom distributor    = CJ Entertainment released       =   runtime        = 119 minutes country        = South Korea language       = Korean budget         =  gross          =   
| film name = {{Film name hangul         =    hanja          =   rr             = Budang georae mr             = Pudang kŏrae}}
}}
The Unjust ( ; lit. "Bad deal" or "Unfair trade") is a 2010 South Korean crime film by Ryoo Seung-wan.    It is a dark and bitter denunciation of corruption in the South Korean justice system.  
 2011 Blue Dragon Film Awards.    

This is director Ryoo Seung-wans fifth collaboration with his younger brother, actor Ryoo Seung-bum.  Lead actors Hwang Jung-min and Ryoo Seung-bum previously worked together in Bloody Tie (2006). 

==Plot==
After the rape and murder of five elementary schoolgirls, the police have still failed to apprehend the serial killer. After one suspect, Yu Min-cheol (Kim Seung-hun), is shot dead — but with no conclusive proof he was guilty — the countrys president becomes involved and adds to the pressure on the police to solve the case. Choi Cheol-gi (Hwang Jung-min), a brilliant but sidelined detective at the Metropolitan Investigation Services who has just brought down corrupt property developer Kim Yang-su (Jo Yeong-jin), is suddenly taken off the case and Kim, thanks to his powerful connections, is released and his case closed. Choi is assigned instead to the serial murders and finally promised a promotion if he can get the police force off the hook by bringing the case to a satisfying conclusion. Choi re-examines the whole case and, with the help of Kims opportunistic rival, Jang Seok-gu (Yoo Hae-jin), decides to stitch up one of the other suspects as the killer. He chooses Lee Dong-seok (Woo Don-gi), a school bus driver with a retarded wife (Lee Mi-do) and young daughter (Park Ha-yeong) who has a past criminal record that includes child molestation. However, Choi and his team are secretly monitored by Joo Yang (Ryoo Seung-bum), a Seoul District public prosecutor in the pocket of Kim, who is looking for payback on Choi for bringing him down and losing a construction project to Jang. One evening, however, Kim is stabbed to death while playing golf with Joo, and Joo receives embarrassing photos of himself and Kim together. Suspecting that Choi has fitted up Lee in the serial-murder case, Joo makes his suspicions known to his superiors but cannot present any hard evidence. After Joo is again made to look stupid when Lee mysteriously hangs himself in his holding cell, he launches a blitzkrieg investigation into Chois whole career and family, and a deadly war breaks out between them, with Jang playing both sides off against each other.        

==Cast==
 
*Hwang Jung-min ... Police captain Choi Cheol-gi
*Ryoo Seung-bum ... Public prosecutor (district attorney|D.A.) Joo Yang
*Yoo Hae-jin  ... Gangster/businessman Jang Seok-gu
*Chun Ho-jin ... Police bureau chief Kang
*Ma Dong-seok ... Police lieutenant Ma Dae-ho
*Woo Don-gi ... Lee Dong-seok, the scapegoat
*Jo Yeong-jin  ... TK chairman Kim Yang-su
*Jung Man-sik ... Assistant D.A. Gong Lee Sung-min ... Chief prosecutor/D.A.
*Kim Su-hyeon ... Soo-il
*Gu Bon-woong ... Yoon-jjang
*Kim-jae ... Detective Lee
*Lee Hee-joon ... Detective Nam
*Oh Jung-se ... Reporter Kim
*Lee Jong-ju ... Representative Goh
*Baek Seung-ik ... killer
*Song Sae-byeok ... Cheol-gis brother-in-law
*Go Seo-hee ... Cheol-gis younger sister
*Kwak Ja-hyeong ... Detective Kwak
*Jo Jong-geun ... Detective Jo
*Kim Gi-cheon ... old inspector
*Lee Do-hyeon ... young inspector
*Hwang Byeong-guk ... defense counsel
*Lee Kyoung-mi ... forensic examiner
*Kim Weon-beom ... squad leader Park
*Kang Hyeon-joong ... squad member
*Jo Ha-seok ... squad members
*Lee Mi-do ... Dong-seoks wife
*Park Ha-yeong ... Dong-seoks daughter
*Kim Seung-hun ... Yu Min-cheol
*Jung Jin-gak ... President of South Korea
*Kim Hye-ji ... Joo Yangs wife
*Park Seo-yeon ... Joo Yangs hostess
*Kang Hae-in ... Reporter Kims hostess
*Ahn Gil-kang ... team leader
*Lee Chun-yeon ... National Police Agency head
*Lee Joon-ik ... President Jeong
*Jo Cheol-hyeon ... Haedong investor
*Oh Seung-hyeon ... Haedong investor
 

==Film festivals== Panorama section Vladivostok International Film Festival - Pacific Meridian,  the Sitges Film Festival,  the London Korean Film Festival,  and the Udine Far East Film Festival. 

==Awards and nominations==
{| class="wikitable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| 2010
|   13th Directors Cut Awards   
| Best Director
| Ryoo Seung-wan
|  
|- 2011
| 5th Asian Film Awards   Best Supporting Actor
| Ryoo Seung-bum
|  
|-
| Best Screenplay
| Park Hoon-jung
|  
|-
|   2nd Seoul Art and Culture Awards   
| Best Film Director
| Ryoo Seung-wan
|  
|- 47th Baeksang Arts Awards 
| Best Film
| The Unjust
|  
|-
| Best Director
| Ryoo Seung-wan
|  
|-
| Best Actor
| Ryoo Seung-bum
|  
|-
| Best Screenplay
| Park Hoon-jung
|  
|-
| rowspan=3|  15th Fantasia International Film Festival    Best Actor
| Hwang Jung-min
|  
|-
| Ryoo Seung-bum
|  
|-
| Best Screenplay
| Park Hoon-jung
|  
|-
| rowspan=3|  20th Buil Film Awards   

| Best Film
| The Unjust
|  
|-
| Best Director
| Ryoo Seung-wan
|  
|-
| Best Actor
| Ryoo Seung-bum
|  
|-
|   44th Sitges Film Festival      
| Best Film (Casa Asia section)
| The Unjust
|  
|-
| rowspan=6|  48th Grand Bell Awards 
| Best Film
| The Unjust
|  
|-
| Best Director
| Ryoo Seung-wan
|  
|-
| Best Supporting Actor
| Yoo Hae-jin
|  
|-
| Best Screenplay
| Park Hoon-jung
|  
|-
| Best Editing
| Kim Sang-bum, Kim Jae-beom
|  
|-
| Best Production Design
| Choi Ji-yeon
|  
|- 32nd Blue Dragon Film Awards            
| Best Film
| The Unjust
|  
|-
| Best Director
| Ryoo Seung-wan
|  
|-
| Best Supporting Actor
| Yoo Hae-jin
|  
|-
| Best Screenplay
| Park Hoon-jung
|  
|-
| Best Cinematography
| Chung Chung-hoon
|  
|-
| Best Lighting
| Bae Il-hyeok
|  
|-
|}

==References==
 

==External links==
*    
*   at Naver  
*   at CJ Entertainment
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 