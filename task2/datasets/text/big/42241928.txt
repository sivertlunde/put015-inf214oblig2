Me and the Mosque
 
Me and the Mosque is a 2005 Canadian documentary film by Zarqa Nawaz about the efforts of Muslim women in North America to pray in mosques, and the use of partitions to conceal women from male worshippers.       

In the documentary, Nawaz speaks with women from Canadian Islamic communities in British Columbia, Saskatchewan, Manitoba and Ontario. In the U.S., she attends an Islamic Society of North America conference in Chicago, and speaks with journalist and activist Asra Nomani, activist Aminah Assilmi as well as author and speaker Tareq Al-Suwaidan about the role of women in Islam. Nawaz often takes a humorous approach to her subject matter; the film also incorporates animated sequences.      

The film was produced as part of a National Film Board of Canada competition for emerging filmmakers of colour, in partnership with CBC Newsworld.

==References==
 

==External links==
*Watch   at the National Film Board of Canada
* 

 

 
 
 
 
 
 
 
 


 
 
 