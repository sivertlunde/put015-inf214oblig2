They Got Me Covered
{{Infobox film
| name           = They Got Me Covered
| image_size     = 
| image	         = They Got Me Covered FilmPoster.jpeg
| caption        =  David Butler
| producer       = Samuel Goldwyn
| writer         = Leonard Spigelgass Leo Rosten Lynn Root
| based on       =  Frank Fenton
| narrator       = 
| starring       = Bob Hope Dorothy Lamour Otto Preminger Lenore Aubert
| music          = Harold Arlen Leigh Harline
| cinematography = Rudolph Maté
| editing        = Daniel Mandell
| studio         = Samuel Goldwyn Productions
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 95 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 David Butler.  Otto Preminger appears in a supporting role. It is also known as Washington Story and The Washington Angle.

==Plot summary==
In the middle of WWII, acclaimed Pulitzer Prize-winning newspaper reporter Robert "Kit" Kittredge returns to the U.S. from his position as Moscow correspondent. He is fired for his incompetency by his editor, Norman Mason, the minute he comes back, since he has neglected to report that Germany recently has invaded Russia.

Kit goes to see his girlfriend, stenographer Christina Hill, at work in another newspaper in Washington D.C. Their meeting is stopped when one of Kits Romanian informers, Gregory Vanescu, claims to have a big scoop for Kit.

Before he can tell Kit his story, he is shot by Nazi spies and runs into hiding, telling Kit to send Christina to the Lincoln Memorial by midnight that night, wearing a red purse and a green umbrella. To get the required accessories, Christina asks her friend Sally Branch to meet her at the monument by midnight, but by mistake Kit drives Christina to the wrong monument that night.

Sally shows up at the correct memorial and when Vanescu sees her, he believes she is Christina. Sally gets to transcribe the extensive information Vanescu has to offer, which tells of a plan to perform terror attacks on the city. Sally jots down all the information in Christinas monogrammed notebook.

Just as the meeting finishes, Nazi agents appear and steal the notebook. Realizing their mistake, Kit and Christina go to Sallys apartment and waits for her there together with her roommates and her boyfriend, Red, who is a marine.

Sally soon returns and tells the others what happened. Meanwhile, the Nazi agents realize they cant interpret Sallys handwriting. The Nazis come to Sallys apartment and "steal" her away. Christina wants them to call the FBI, but Kit is anxious to get his scoop and his job back, so he only pretends to call them.

Kit goes to look for Vanescu at a nightclub where he knows the man has been hiding before. There he finds a gypsy woman who shows him to a private room on the second floor, where two spies await - Olga and Otto Fauscheim. Olga poses as Vanescus wife and persuades Kit to help her find her missing husband.

Olga tries to scare Kit off by leading him to an old house of an old delusional veteran, but Kit strokes the man the right way, and is instead able to discover Vanescus dead body.

Otto decides they instead try to break Kit by ruining his reputation and career. They manage to drug Kit with a doped cigarette and when he is knocked out makes him marry one of the showgirls, Gloria the Glow Girl, at the night club.

The plan fails, as Christina realizes Kit has been set up. Kit has managed to keep a handkerchief from Olga, and Christina and her friends start tracing the perfume on it. Before Gloria can disclose the plan she was involved in to trick him, she is stabbed and killed by the spies.

Kit is quickly suspected of murdering Gloria. He flees but is captured by another agent working for the enemy, Baldanacco. Christina finds out that the perfume on the handkerchief was purchased at a particular beauty salon, and she goes there to find clues. The salon is owned by the night club owner, and she suspects them of working with the spies.

It turns out Olga runs the salon, and she recognizes the monogramme on Christinas bag, which matches the one on Sallys notebook. Kit is held hostage in another room at the salon, but manages to escape his bonds and run into hiding. By accident he hears the Axis agents speak in the showroom.

Kit hears all about the spy rings plan to blow up the city. Kit takes on the agents and manage to hold them off until the rest of his friends arrive, including Ted the Marine and his soldier buddies. The spies are defeated and the police arrive. Christina and Kit reunite in a kiss, and his friends discuss the possibility of another Pulitzer Prize because of this new scoop. 

==Cast==
* Bob Hope as Robert Kittredge
* Dorothy Lamour as Christina Hill
* Lenore Aubert as Mrs. Olga Venescu
* Otto Preminger as Otto Feischum
* Eduardo Ciannelli as Baldanacco
* Marion Martin as Gloria
* Donald Meek as Little Old Man
* Phyllis Ruth as Sally
* Philip Ahn as Nichimuro
* Donald MacBride as Mason
* Mary Treen as Helen
* Bettye Avery as Mildred
* Margaret Hayes as Lucille
* Mary Byrne as Laura
* William Yetter Sr. as Holtz (as William Yetter)
* Gino Corrado as Bartender at Cafe (uncredited)
* Bing Crosby as The Music Box (voice) (uncredited)
* Doris Day as Beautiful Girl in Sheet (uncredited)
* Wolfgang Zilzer as Cross (uncredited)

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 