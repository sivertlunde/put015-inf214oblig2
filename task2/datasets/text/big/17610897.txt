The Red Lantern
 Red Lantern}}
{{Infobox film
| name           = The Red Lantern
| image          = The Red Lantern.jpg
| image_size     =
| caption        = Sheet music cover to the films theme song
| director       = Albert Capellani   
| producer       =
| writer         = June Mathis Edith Wherry (novel)
| starring       = Alla Nazimova Noah Beery
| cinematography = Tony Gaudio
| editing        =
| distributor    = Metro Pictures Corporation
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         =
| gross          =
| admissions     =
}} 1919 American silent drama film starring Alla Nazimova, who plays dual roles, and directed by Albert Capellani. It is notable today for Anna May Wongs screen debut. A single print survives in Europe with rumors of a copy at Gosfilmofond, Moscow.  

==Plot== Peking where Boxer leader. Dr. Wang loves Mahlee but she spurns his advances. One day Blanche Sackville (Nazimova) visits the mission, and Mahlee realizes that she is the daughter of the Englishman her grandmother told her about and is her half-sister. Although Mahlee initially feels an attachment to Blanche, she soon becomes jealous when she realizes that Sir Philip Sackville (Currier) favors a suite between Blanche and Andrew Templeton. Capitalizing on the disdain in Mahlees heart, Sam Wang convinces her to join him and impersonate the Goddess of the Red Lantern, a mystic personage that presides over the Chinese New Year, to convince the superstitious revolutionaries that victory is near if they follow Wang. While she serves the Boxer cause, she still cannot bear to have those she loves hurt, so she goes to the mission to warn them of an attack. She meets Philip Sackville and pleads for him to acknowledge her as his daughter and take her away from China, but Sackville refuses. She returns to Wang and celebrates the Feast of the Lanterns. The Boxers are suspicious of her, but Wang saves her. Armed conflict between the Boxers and Allies results in the rout of the Chinese. Mehlee goes to the Boxer Palace and while on her throne, she drinks the poison Wang gives her. Philip Sackville, Blanche, and Andrew come to the palace and discover Mehlee dead.

==Cast==
 ]]
* Alla Nazimova ... Mahlee & Blanche Sackville 
* Noah Beery ... Dr. Sam Wang Charles Bryant
* Edward Connelly ... General Jung-Lu
* Frank Currier ... Sir Philip Sackville Reginald Denny
* Darrell Foss ... Andrew Templeton
* Dagmar Godowsky
* Winter Hall ... Reverend Alex Templeton
* Henry Kolker
* Harry Mann ... Chung
* Virginia Ross ... Luang-Ma
* Mary Van Ness ... Mrs. Templeton
* Anna May Wong ... (uncredited)

==Production==
  flu pandemic, was the debut of Anna May Wong, who played a lantern bearer.    To meet the casting requirements which required 300 extras for the film, the Chinese American extras were paid $7.50 per day ($101.32 in 2015 dollars), which was $1.50 more than the other extras. 

==Restoration==
A restored print of this film is available on a region 2 DVD.

==Notes==
 

==Bibliography==
*  

== External links ==
 
*  
*  
*  , Alla Nazimova Society
*  , Alla Nazimova Society
*   by Edith Wherry, with illustrations from the film (Google Books)

 
 
 
 
 
 
 