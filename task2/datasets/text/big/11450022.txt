The Star Boarder (1914 film)
 
{{Infobox film
| name = The Star Boarder
| image = The Star Boarder.jpg
| imagesize= 180px
| caption = scene from film George Nichols
| producer = Mack Sennett
| writer = Craig Hutchinson
| starring = Charles Chaplin Minta Durfee Edgar Kennedy Gordon Griffith Alice Davenport Frank D. Williams
| editing =
| distributor = Keystone Studios
| released =  
| runtime = 16 minutes English (Original titles)
| country = United States
| budget =
}}
  1914 United States|American-made motion picture starring Charlie Chaplin.

The film is also known as The Landladys Pet.

==Synopsis==
Chaplin is the favorite of his landlady, and the other boarders are jealous.

==Cast==
* Charles Chaplin - The Star Boarder
* Minta Durfee  - Landlady
* Edgar Kennedy - Landladys husband
* Gordon Griffith - Their son
* Alice Davenport - Landladys friend

==See also==
* List of American films of 1914
* Charlie Chaplin filmography

==External links==
* 
* 
*  

 
 
 
 
 
 
 
 
 
 


 