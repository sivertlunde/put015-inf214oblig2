Il mostro (1977 film)
 
{{Infobox film
| name           = Il mostro
| image          = Il mostro (1977 film).jpg
| caption        = Film poster
| director       = Luigi Zampa
| producer       = Claudio Mancini
| writer         = Sergio Donati
| starring       = Johnny Dorelli
| music          = Ennio Morricone
| cinematography = Mario Vulpiani
| editing        = Franco Fraticelli
| distributor    = 
| released       =  
| runtime        = 
| country        = Italy
| language       = Italian
| budget         = 
}}

Il mostro is a 1977 Italian thriller film directed by Luigi Zampa and starring Johnny Dorelli.

==Cast==
*Johnny Dorelli as Valerio Barigozzi
*Sidney Rome as Dina 
*Orazio Orlando as Commissioner Pisani
*Renzo Palmer as Baruffi
*Enzo Santaniello as Luca Barigozzi
*Renato Scarpa as Livraghi
*Yves Beneyton as Giorgio Mesca
*Gianrico Tedeschi as Vittorio Santi, "Grandpa Gustavo"
*Clara Colosimo as Donatella Domenica Donati
*Angelica Ippolito as Anna

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 