The Hangman (2010 film)
 
{{Infobox film
| name           = The Hangman
| image          = hangmannew.jpg
| caption        = Theatrical poster
| producer       = Bipin Patel
| Co-Producer    = Anoli Patel
| Exec. Producer = Piyu Sheth
| director       = Vishal Bhandari
| writer         =  
| starring       =  
| music          = Vishal-Shekhar
| country        = 
| cinematography = V. Manikandan
| editing        = Deepa Bhatia
| distributor    = B4U Productions
| released       =  
| runtime        = 162 minutes
| language       = Hindi
| budget         =
| gross          = 
}} English was also released.

== Plot==
The Hangman, starring internationally acclaimed actor Om Puri (East is East, Jewel in the Crown, City of Joy and Gandhi), is a story about one mans quest to attain redemption. Puri, who gives a compelling performance as the aged and tired executioner Shiva, has been forced into his forefathers profession.

Shivas overwhelming desire is to create a better life for his son Ganesh (Shreyas Talpade). Shiva seeks help from his friend, the prison jailor (Gulshan Grover). Ganesh is taken to the city under the jailors guardianship where he pursues his fathers ultimate dream of becoming a police officer. But a series of ill-fated events result in a startling tragedy.

Based in a culture full of ancient customs and traditions, this bittersweet tale follows Shiva as he struggles between the mandates of his profession and his desire to achieve salvation and happiness.

== Cast ==
*Om Puri as Shiva
*Gulshan Grover as Jail Superintendent
*Shreyas Talpade as Ganesh
*Smita Jaykar as Parvati
*Tom Alter as Father Mathew
*Anita Kanwar as Madam at the brothel
*Yatin Karyekar as Billa
*Nazneen Ghani as Gauri
*Amrita Bedi as Amrita

==External links==
* 
*  
*  at Bollywood Hungama
* 

 
 
 
 


 