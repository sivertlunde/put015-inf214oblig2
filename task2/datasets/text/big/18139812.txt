Mehbooba (2008 film)
{{Infobox film
| name           = Mehbooba
| image =
| caption        = Movie poster Afzal Khan
| producer       = Shabina Khan
| writer         = Rumi Jaffrey
| starring       = Sanjay Dutt Ajay Devgn Manisha Koirala
| music          = Ismail Darbar
| music release  = T-Series
| country        = India
| cinematography =
| editing        =
| distributor    = Eros Labs
| released       =  
| runtime        = 162 minutes
| language       = Hindi
| budget         =
| gross          =
}} Afzal Khan. The story of the movie is based on a love-triangle, lead protagonists being Ajay Devgn, Sanjay Dutt and Manisha Koirala. Music of the film was composed by Ismail Darbar. The film was released in India on July 11, 2008. The film was shot in 2000, but got delayed and released in 2008, 8 years after it was shot.

== Synopsis ==
The story follows rich businessman Shravan Dhariwal (Sanjay Dutt). Shravan has everything one can dream of having and what he doesn’t have, he buys it. One day, he meets Varsha (Manisha Koirala) and believes her to be his true love. He proposes to her, and she eventually agrees. The couple get engaged, and go on a small holiday. However, Varsha’s beautiful dream comes to a shocking end when Shravan tells her that his love for her was just a drama to get engaged so he could sleep with her. Heart broken and shattered, Varsha’s whole world collapses around her. She leaves the country and goes as far away as possible from Shravan Dhariwal to start a new life. Shravan goes back home, and says things didnt go well with Varsha.

After this intro, the story begins with Karan (Ajay Devgn) a young artist, who has many girls after him, however awaits his true love and his dream girl because he is an honest lover. Every night, he dreams of a girl running in the fields and believes she is his true love. One day, Karan sees a young woman named Payal (also Manisha Koirala), and she is the extreme lookalike of his dream girl. Payal is actually Varsha who changed her name so Shravan could never find her. Karan begins to follow her, and explains she is the woman he sees in his dreams but Payal thinks he is flirting with her. Eventually, when Payal realizes that her uncle is Karans lawyer, the two make friendship, and soon enough, they fall in love. Karan phones his mother, and tells her he is getting married to an Indian girl named Payal.

His mother is overly excited, and tells the whole village such as Karans brother; cousins and rest of his family. She tells him that the marriage would take place in their own village, and Karan sets out for his hometown with Payal. Once they arrive, Karans mother is totally smitten with Payal. Everything is going well, and a day before the marriage, Karans elder brother arrives home to see his brothers future wife, and Payals world turns upside down. It is revealed that Karans brother is actually Shravan Dhaliwal. Payal begins to hide the shock from Karan, though refuses to get married to Karan. Karan, who then finds out the truth behind her rejection, goes to commit suicide. Shravan is unaware of this. Karan takes his car on full speed in front of a running train, but is soon pushed out of the car by Shravan.

Shravan is hurt and struggles. He makes Karan promise to take care of Varsha/Payal and also not to tell the family that Payal is Varsha. He tries to ask forgiveness but dies in the arms of Karan.

== Cast ==
* Sanjay Dutt as Shravan Dhariwal
* Ajay Devgn as Karan Dhariwal
* Munja kale
* Manisha Koirala as Varsha/Payal
* Kader Khan as Lawyer
* Sanober Kabir in song "Babuji Bahut Dukhta Ha"

== Box office ==
Mehbooba had a good first-week response at the box office. The film collected Rs. 35.45 crore in its first week. It did well since it was concepted to be filmed so old and took quite a longtime for release, and was filmed in 2000, although it was released in 2008. The films did business of 68 crores in India and 90.32 crores worldwide. The film was a failure, and was demoted as a disaster at the box office by Box Office India. 

== Music ==
The old-fashioned still soothing music of the film was composed by Ismail Darbar. Lyrics by Anand Bakshi
  The tracks are listed down.
{| class="wikitable"
|-
! Song
! Singer
! Picturised on/Notes
|-
| Tu Meri Mehbooba
| Udit Narayan
| Sanjay Dutt, Manisha Koirala
|-
| Khwabon Ki Rani Hai
| Udit Narayan
| Ajay Devgn, Manisha Koirala
|-
| Dilruba
| Udit Narayan
| Ajay Devgn
|-
| Yaar Tera Shukriya
| Alka Yagnik, Udit Narayan
| Manisha Koirala, Ajay Devgn
|-
| Deewana
| Sukhwinder Singh, Sonu Nigam
| Sanjay Dutt, Ajay Devgn
|-
| Babuji Bahut Dukhta Hai
| Alka Yagnik
| Sanjay Dutt, Ajay Devgn, Sanober Kabir
|-
| Kuch Kar Lo
| Sonu Nigam
| Sanjay Dutt
|-
| Kuch Kar Lo
| Shankar Mahadevan, Kavita Krishnamurthy
| Sanjay Dutt, Manisha Koirala
|-
| Kuch Kar Lo Kuch
| Mahalaxmi Iyer
| Manisha Koirala
|-
| Achcha To Ab Main Chalta Hoon
| Sonu Nigam
|
|}

== References ==
 

== External links ==
*  

 
 
 
 