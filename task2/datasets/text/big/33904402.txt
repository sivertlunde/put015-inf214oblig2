Deadroom
{{Infobox film
| name = Deadroom
| image =
| caption = David Lowery  Nick Prendergast Yen Tan
| producer = James M. Johnston  David Lowery  Nick Prendergast Yen Tan
| writer = James M. Johnston  David Lowery  Nick Prendergast Yen Tan
| narrator =
| starring =
| music = Daniel Huffman Jim McMahon
| editing = David Lowery
| distributor =
| released =  
| runtime =
| country =
| language =
| budget =
| gross =
}} David Lowery, Nick Prendergast and Yen Tan.  Film is divided into 4 sub-plots with each plot taken by a director.

==Synopsis==
"Four Conversation Between the Living and the Dead: If you had the chance, what would you say? says the film caption.

Deadroom centers around four encounters, each confined to a single room. The four intertwined stories weave a tale that is shocking, humorous, tragic and uplifting. In Deadroom, secrets are shared, truths are revealed, and each of the eight characters comes to an understanding they could not have gained without the touch of death. Each of these conversations could happen any day, every day, except that the conversations are happening after death of the individual concerned.
  
The characters: 
*A man helps a young woman remember her past after the young woman has been raped and murdered
*A husband and his wife confront each other about their infidelities, after the husband has perished in a terrible automobile accident
*A journalist interviews a famous author about the mysteries hidden in his novel, after the novelist died of old age
*A secretary visits her former employer to tell him how much she always loved him., after the employer died in a fire

==Cast==
*Rebecca Bustamante as Mother
*Mark Forte as Man with Dog
*Harry Goaz as Layton
*Jeff Griffin as Man
*Grant James as Percy
*Renée Kelly (Kelly Grandjean) as Kate 
*Alana Macias as Woman
*Lydia Miller as Julie
*Sue Roberts-Birch as Joan
*Bill Sebastian as Tim
*Paul T. Taylor as Trevor

==Screenings==
The film was an official selection for the 2005 Philadelphia International Film Festival, the 2005 SXSW Film Festival and the 2006 Cleveland International Film Festival.

==Awards==
*Directors James M. Johnston, David Lowery, Nick Prendergast and Yen Tan were co-winners of Directors Award at the Texas Film Festival for the film

==External links==
* 
* 

 

 
 
 