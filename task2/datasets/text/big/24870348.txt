Stan Helsing
{{multiple issues|
 
 
}}
{{Infobox film
| name = Stan Helsing
| image = Stan_helsing_Movie_Post.jpg
| caption = 
| director = Bo Zenga
| producer = Scott Steindorff Bo Zenga  Kirk Shaw
| writer = Bo Zenga Steve Howey Diora Baird Kenan Thompson Desi Lydic Leslie Nielsen
| music = Ryan Shore
| studio = Stone Village Pictures
| distributor = Anchor Bay Entertainment   
| released = October 23, 2009
| runtime = 81 min.
| country = United States Canada
| language = English
| preceded_by = 
| followed_by = 
| budget = $14 million
| gross = $11,014,125
}} Van Helsing.

== Plot == Steve Howey) is an underachieving employee at a video rental store named Schlockbuster whose personal mottos are "Dont get involved" and "Dont talk about it". His teen-aged boss Sully orders him to drop off a bag of movies to the mother of the stores owner or risk not having a job Monday morning. Despite his arguments, he agrees to the request and manages to get his friend Teddy (Kenan Thompson), his ex-girlfriend Nadine (Diora Baird) and ditsy blond massage therapist Mia (Desi Lydic) to take him there before they attend a Halloween party, even though its on the other side of town. 
 MILF next Ring of Fire.
 Michael Meyers. altar boy who is so very close to becoming a priest who informs Stan of his destiny and that he is Van Helsings descendant, arming them with various weapons before kicking them out of his church. The townsfolk offer a competition in which Stan and his friends compete against the monsters in Karaoke. The humans are unanimously voted as the winners, but the monsters refuse to leave town. Disobeying his policy of never getting involved, Stan turns each of the monsters weaknesses against them, soundly defeating them and feeding them to Sammy who had been brought back to vicious life (ala Pet Sematary). The group calls a cab and they leave the town as heroes. During the trip home, Nadine discovers how much she cares for Stan and kisses him. Teddy suggest kissing Mia, but she rebuffs with a lap dance offer instead which he gladly accepts. The movie ends with the taxi cab driving away and the sound of moaning.

During the film many posters relating to Southern New Jersey are shown such as Visit Atlantic City and Visit Palasades Park. The town the videos are being delivered to is called Linwood (a real town in Southern New Jersey). Neighboring cities Ventnor and Margate are mentioned on a news broadcast towards the end of the movie.

== Cast== Steve Howey as Stan Helsing
* Diora Baird as Nadine
* Kenan Thompson as Teddy
* Desi Lydic as Mia
* Leslie Nielsen as Kay
* Travis MacDonald as Hitcher
* Chad Krowchuk as Sully
* Darren Moore as Crazy
* Jeremy Crittenden as Altar Boy Chucky from Childs Play (film series)|Childs Play) Friday the 13th) A Nightmare on Elm Street) Michael Myers from Halloween (franchise)|Halloween) The Texas Cahinsaw Massacre) Pinhead from Hellraiser (franchise)|Hellraiser)
* Hilary Strang as Hippie Lady
* Ray G. Thunderchild as Husband
* John DeSantis as Frankensteins Monster
* Ryan Steele as the Wolfman
* Jeremiah Sird as Idiot Indian

==Soundtrack==
Although no soundtrack album was released the film includes songs by Pluto (New Zealand band)|Pluto, The Saturday Knights, Davernoise and covers of songs by Johnny Cash, Patsy Cline and Village People. 

The music was composed by Ryan Shore.

== Reception ==
Stan Helsing was panned by critics. It hold a rating of 17% "rotten" on Rotten Tomatoes. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 