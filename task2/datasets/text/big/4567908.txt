Phool Aur Patthar
{{Infobox film
| name           = Phool Aur Patthar 
| image          = PhoolAurPatthar.jpg
| image_size     = 
| caption        = 
| director       = O. P. Ralhan 
| producer       = O. P. Ralhan
| writer         = O. P. Ralhan, Ehsan Rizvi (dialogue)
| narrator       = 
| starring       = Dharmendra Meena Kumari Shashikala O. P. Ralhan Ravi
| cinematography = Nariman Irani
| editing        = Vasant Borkar
| distributor    = 
| released       = 
| runtime        = 
| country        =   India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Phool Aur Patthar was O. P. Ralhans 1966 Hindi film that made Dharmendra a star in the Hindi Film Industry. It starred Meena Kumari along with Dharmendra who played a villainous character (or Patthar, literally a Stone) whose inner good being (or Phool, literally a Flower) is drawn out by Meena Kumari. The movie also starred Shashikala, Lalita Pawar, Madan Puri and Iftekhar.

This was the movie which went on to become a golden jubilee hit catapulting Dharmendra to stardom. The movie was highest grossing for the year 1966.  Due to his rugged physique, he was also acknowledged as the He-man of the Indian film Industry. In fact, a scene in the movie where he takes off his shirt to cover the ailing Meena Kumari was one of the highlights of the movie. His performance in the movie ensured him a nomination in the Best Actor category in the Filmfare awards for that year. However, it was won by Dev Anand for his performance in Guide. He made his presence strongly felt despite the fact that he did not lip sync for any songs in the film. In the 1960s, it was very unusual for the leading man not to sing any songs in a movie.
 Baharon Ki Manzil after this. 

Before shooting for the film, O. P. Ralhan had wanted Sunil Dutt to don the leading role but it did not work out. 

During shooting, at one point of time, Dharmendra had a show-down with the films director O. P. Ralhan since he felt that the director had an arrogant attitude and he contemplated quitting the film mid-way. However, sense prevailed and he resumed shooting.

The film was remade in Tamil as Oli Vilakku with M. G. Ramachandran.

==Plot==
Circumstances have made Shaka a career criminal. When plague empties a town of its inhabitants, he takes the opportunity to burgle a house. He finds nothing except Shanti, a widowed daughter-in-law who has been left to die by her cruel relatives. Shaka nurses her back to health. When her relatives return they are not pleased to find her alive and even less pleased to discover that someone has tried to rob them. Shanti gets the blame and a beating. Shaka saves her from worse, at the hands of brother-in-law, and the pair flee. They set up home in Shakas house, much to the displeasure of the respectable neighbours who are all too ready to think the worst. Shantis relatives are dismayed when a lawyer arrives to announce that Shanti has been left a legacy. They hatch a plot to get her back. Meanwhile, Shakas rehabilitation is proceeding - much to the chagrin of his former criminal associates. Fire and redemption for some, death and handcuffs for others is what fate has in store.

==Cast==

* Meena Kumari  	 ...	Shanti Devi
* Dharmendra	         ...	Shakti Singh / Shaaka
* Shashikala    	 ...	Rita
* Lalita Pawar	 ...	Mrs. Jeevan Ram Sunder    	 ...	Dr. Alopinath - Vaidraj
* Jeevan        	 ...	Jeevan Ram
* Ram Mohan	         ...	Kalicharan (as Rammohan)
* Manmohan Krishna	 ...	Police Inspector
* Madan Puri         ...	John - Boss (as Madanpuri)
* Tun Tun       	 ...	Mrs. Alopinath (Guddki) (as Tuntun)
* Leela Chitnis      ...	Blind Beggar
* Iftekhar       	 ...	Babu (as Iftikhar)
* D.K. Sapru         ...	Judge (as Sapru)
* Brahm Bhardwaj	 ...	Public Prosecutor (as Brahm Bharadwaj)
* Baby Farida      	 ...	Jumni
* Master Aziz        ...	Bablu
* Ram Avtar        	 ...	Fat Pickpocket Bhairon        	 ...	Daboo - the dog (as Famous Dog Bhairon)
* O. P. Ralhan	         ...	Sadakram

==Soundtrack==

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)!! Notes
|-
| Sheeshe Se Pee Ya
| Asha Bhosle 
| Picturized on Shashikala
|-
| Sun Le Pukar
| Asha Bhosle
|  
|-
| Zindagi Mein Pyar Karna
| Asha Bhosle
| Picturized on Shashikala
|- 
| Mere Dil Ke Andar
|  Mohammad Rafi
| This song is a qawwali.
|-
| Layi Hai Hazaron Rang
| Asha Bhosle
|  Future item queen Laxmi Chhaya is one of the dancers featured in this song.
|-
| Tum Kaun? Mamul
| Mohammad Rafi
|
|}

==Awards==

;Filmfare Awards
* Best Art Director - Color - Shanti Dass (Won)
* Best Editor - Vasant Borkar (Won)
* Best Actor - Dharmendra (Nominated)
* Best Actress - Meena Kumari (Nominated)
* Best Supporting Actress - Shashikala (Nominated)

== External links ==

*  

==References==
 

 
 
 
 