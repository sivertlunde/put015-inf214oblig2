Lum and Abner Abroad
{{Infobox film
| name           = Lum and Abner Abroad
| image          = Lum and Abner Abroad poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = James V. Kern
| producer       = James V. Kern 
| screenplay     = Carl Herzinger 
| story          = Carl Herzinger James V. Kern
| starring       = Chester Lauck Norris Goff Jill Alis Lila Audres Gene Gary Chris Peters
| music          = 
| cinematography = Kreso Grcevic Oktavijan Miletic 
| editing        = Blanche Jens Maurice Wright 
| studio         = Nasbro Pictures Inc.
| distributor    = Nasbro Pictures Inc.
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Lum and Abner Abroad is a 1956 American comedy film directed by James V. Kern and written by Carl Herzinger. The film stars Chester Lauck, Norris Goff, Jill Alis, Lila Audres, Gene Gary and Chris Peters. The film was released on January 1, 1956.   

==Plot==
 

== Cast ==
*Chester Lauck as Lum Edwards
*Norris Goff as Abner Peabody
*Jill Alis as Marianne Passavetz
*Lila Audres as Collette Bleu
*Gene Gary as Nikolai Brasnovich
*Chris Peters as Croupier 
*Nada Nuchich as Lisa Dubroc
*Branko Spoljar as Papa Passavetz
*Jim Kiley as Tommy Ellis
*Steven Voyt as Frankenshpinin
*Vera Misita as Duchess Dubroc
*Vlado Stefancic as Mischa Dramascu 
*Josip Batistic as Dignitary

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 