Donato and Daughter
{{Infobox television film
| name         = Donato and Daughter
| image        = 
| caption      = 
| genre        = Action Drama Crime
| writer       = Jack Early (novel) Robert Roy Pool (teleplay)  
| producer     = Marian Brayton (producer) Anne Carlucci (producer) Brenda Miao (co-executive producer) Neil Russell (executive producer) 
| director     = Rod Holcomb
| starring     = Dana Delany, Charles Bronson
| cinematography = Thomas Del Ruth
| editing      = Christopher Nelson
| music        = Sylvester Levay
| distributor  = CBS
| studio       = Multimedia Motion Pictures ARD Degeto Film
| released     = September 21, 1993
| runtime      = 90 minutes
| network      = CBS
| preceded_by  = 
| followed_by  = 
| country      = United States
| language     = English
| budget       = 
}}
Donato and Daughter is a 1993 American crime drama film. It stars Charles Bronson and Dana Delany.

==Cast==
* Charles Bronson – Sgt. Mike Donato
* Dana Delany – Lt. Dena Donato
* Xander Berkeley – Russ Loring
* Jenette Goldstein – Det. Judy McCartney
* Louis Giambalvo – Chief Hugh Halliday
* Marc Alaimo – Det. Petsky
* Tom Verica – Bobby Keegan
* Robert Gossett – Det. Bobbins
* Bonnie Bartlett – Renata Donato
* Julianna McCarthy – Adele Loring
* Patti Yasutake – Dr. Stewart
* Richard Kuss - Chief Stone Michael Cavanaugh - Vinnie Stellino
* Julianna McCarthy - Adele Loring 
==External links==
* 

 
 
 
 
 


 