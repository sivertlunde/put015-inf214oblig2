The Loves of Mary, Queen of Scots
{{Infobox film
| name           = The Loves of Mary, Queen of Scots
| image          =
| caption        =
| director       = Denison Clift
| producer       =
| writer         = Denison Clift John Stuart
| music          =
| cinematography =
| editing        =
| studio         = Ideal Film Company
| distributor    = Ideal Film Company
| released       = October 1923
| runtime        =
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British silent silent historical film directed by Denison Clift and starring Fay Compton, Gerald Ames and Ivan Samson.  The film depicts the life of Mary, Queen of Scots and her eventual execution. It was one of the final films made by Ideal Film Company|Ideal, one of the leading British studios, before they were hit by the Slump of 1924.

==Cast==
* Fay Compton - Mary Stuart
* Gerald Ames - Bothwell
* Ivan Samson - Lord Dudley John Stuart - George Douglas
* Ellen Compton - Queen Elizabeth
* Lionel dAragon - Moray
* Harvey Braban - Ruthven
* Irene Rooke - Catherine de Medici
* Donald Macardle - Francis II
* René Maupré - Rizzio
* Ernest A. Douglas - Cardinal
* Sydney Seaward - Lord Douglas
* Edward Sorley - John Knox
* Betty Faire - Mary Beaton Dorothy Fane - Mary Beaton
* Nancy Kenyon - Mary Fleming
* Julie Hartley-Milburn - Mary Seaton
* Basil Rathbone (uncredited)
* Jack Cardiff (uncredited)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 