The Great Impersonation (1921 film)
{{Infobox film
| name           = The Great Impersonation
| image          = The Great Impersonation (1921) - Kirkwood.jpg
| alt            = 
| caption        = Still with James Kirkwood, who played both a German spy and English gentleman with similar appearances
| director       = George Melford
| producer       = Jesse L. Lasky
| screenplay     = Monte M. Katterjohn E. Phillips Oppenheim
| starring       = James Kirkwood, Sr. Ann Forrest Winter Hall Truly Shattuck Fontaine La Rue Alan Hale, Sr. Bertram Johns
| music          = 
| cinematography = William Marshall 	
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent drama film directed by George Melford and written by Monte M. Katterjohn and E. Phillips Oppenheim. The film stars James Kirkwood, Sr., Ann Forrest, Winter Hall, Truly Shattuck, Fontaine La Rue, Alan Hale, Sr., and Bertram Johns. The film was released on October 9, 1921, by Paramount Pictures.   Its survival status is classified as unknown,  which suggests that it is a lost film.

==Plot==
 

== Cast ==
*James Kirkwood, Sr. as Sir Everard Dominey / Leopold von Ragastein
*Ann Forrest as Rosamond Dominey
*Winter Hall as Duke of Oxford
*Truly Shattuck as Duchess of Oxford
*Fontaine La Rue as Princess Eiderstrom
*Alan Hale, Sr. as Gustave Seimann
*Bertram Johns as Dr. Eddy Pelham
*William Burress as Dr. Hugo Schmidt
*Cecil Holland as Roger Unthank
*Tempe Pigott as Mrs. Unthank
*Lawrence Grant as Emperor William of Germany
*Louis Dumar as Prince Eiderstrom
*Frederick Vroom as	Prince Terniloff
*Florence Midgley as Princess Terniloff

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 


 