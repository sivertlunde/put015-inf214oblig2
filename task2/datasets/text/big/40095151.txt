Wake Up, Girls!
{{Infobox animanga/Header
| name            = Wake Up, Girls!
| image           = Wake Up, Girls! Volume 1 Blu-Ray cover.jpg
| caption         = 
| ja_kanji        = 
| ja_romaji       = 
| genre           = Music, Slice of Life
}}
{{Infobox animanga/Movie
| type            = film
| title           = Wake Up, Girls! - Seven Idols
| director        = Yutaka Yamamoto
| writer          = Tōko Machida
| music           = Satoru Kōsaki Ordet
| licensee        = 
| released        =  
| runtime         = 53 minutes
}}
{{Infobox animanga/Video
| type            = TV series
| director        = Yutaka Yamamoto
| writer          = Tōko Machida
| music           = Satoru Kōsaki Ordet
| licensor        = 
| network         = TV Tokyo, Television Aichi Broadcasting|TVA, Television Osaka|TVO, Sendai Television|OX, AT-X (company)|AT-X
| first           = January 10, 2014
| last            = March 28, 2014
| episodes        = 12
| episode_list    = #Episode list
}}
{{Infobox animanga/Video
| type            = ona
| title           = Wake Up, Girl ZOO
| director        = Kenshirō Morii
| writer          = 
| music           = 
| studio          = Ordet (company)|Ordet, Studio Moriken
| licensor        = 
| network         = 
| first           = September 5, 2014
| last            = 
| episodes        = Ongoing
| episode_list    = 
}}
{{Infobox animanga/Movie
| type            = Film
| title            = 
| director        = Yutaka Yamamoto
| writer          = Tōko Machida
| music           = Satoru Kōsaki Ordet 
| licensee      = 
| released      =  
| runtime      = 
}}
  Ordet and Tatsunoko Production and directed by Yutaka Yamamoto.  A film titled   opened in Japan on January 10, 2014, and a television series aired in Japan between January and March 2014.     The film was streamed online in 108 countries upon its release. Both the series and film are being streamed with English subtitles by Crunchyroll. 

==Plot==

Green Leaves Entertainment is a tiny production company on the verge of going out of business in Sendai, the biggest city in Japans northeastern Tohoku region. The agency once managed the careers of magicians, photo idols, fortune-tellers, and other entertainers, but its last remaining client finally quit. In danger of having zero talent (literally), the president Tange hatches an idea of producing an idol group. On the brash presidents orders, the dissatisfied manager Matsuda heads out to scout raw talent.

==Characters==

=== Green Leaves ===

==== Wake Up, Girls! ====
; 
:    Yamakan Reveals Wake Up, Girls! Anime Characters & Cast |date=July 28, 2013|publisher=Anime News Network|accessdate=July 28, 2013}} 
:A high school freshman who used to be center in the famous idol group I-1 Club. She was one of the earliest members of the group, auditioning at age 12. Her popularity as the I-1 Clubs center helped the group became more well known, but tensions between her and the groups manager and president Tōru Shiraki, when Mayu protested about the firing of her friend from I-1, led to her being fired after her next single did not beat that of rival center Shiho Iwazaki in sales records. Her firing also led to her parents divorce and an estranged relationship with her mother. In the movie, she is the 7th and last member to join the Wake Up Girls, her reason being seeking her own happiness by becoming an idol once again. She later makes up with her mother after showing the latter how much she enjoys her job and, with her mothers blessing, becomes properly contracted with WUG and Green Leaves (as she joined the group without auditioning). Nicknamed "Mayushi" by both her peers and fans. Like the I-1 Club, Mayu is the center for Wake Up Girls.

; 
:  
:A 15-year-old high school freshman and Mayus best friend. Shes a simple girl with no particular abilities, but with a lot of drive and is a hard worker. Her family owns a sweets store. She was almost fired from WUG when Hayasaka saw her lack of improvement but Nanase (Yoppi) and Mayu convinced her to stay in the group while the other girls convinced Hayasaka not to fire her. She continues to practice and later becomes just as good as the others during their performances.

; 
: 
:A 14-year-old girl who enjoys eating various kinds of food and has a big appetite. She is recruited after Junko saw her winning a local folk song singing competition. She is also known to sing at a local home for senior citizens who give her their support during her concerts. She and Miyu are usually the mood makers of the group due to their bubbly and cheerful personalities.

; 
: 
:16-years-old. Nicknamed "Yoppi" by her peers, she is well known in Sendai as a former child actress and a model before joining the group. She and Kaya are among the most mature members, often questioning the antics of Kohei and Junko. Because of her having the most experience in the Entertainment Industry, shes cited as the de facto leader by the group. She is also a very determined person as shown when sprained her foot during a rehearsal for the Idol Festival finals yet continued to practice and perform properly albeit with a slight mishap caused by her injury.

; 
: 
:At 13-years-old, Nanami is the youngest member of the group. She constantly practices singing, acting, and playing the piano in hopes of becoming a successful idol. Her dream is to one day perform at a Hikarizuka Theater, a type of Takarazuka play. After an overnight trip to Kesennuma with the group she decides to devote herself to WUG after hearing about Mayus history.

; 
: 
:At 18-years-old, she is the oldest member of the group. Kaya was originally from Kesennuma, living with her aunt after her parents died when she was young. Kaya then left for Sendai working in various part-time jobs after the fishing vessel "Shotomaru", in which her childhood friend was among the passengers, went missing 3 years prior to the start of the series. She joined the Wake Up Girls on a whim and originally planned to leave when she gets bored, but has since dedicated herself to be with the group. She acts as the groups second-in-command after Yoppi. Nicknamed "Kayatan" by her peers.

; 
: 
:A 17-year-old girl with a bubbly personality. She has a job at a maid cafe in Sendai and is self described as "twin tailed and incompetent". She is willing to participate in almost anything and enjoys being the center of attention whilst respecting others in the group. Miyu is the first person to be successfully recruited by Kohei as a member of the Wake Up Girls. She and Minami are often the mood makers of the group.

==== Staff ====
; 
: 
:The groups manager. While having a good heart, his timid persona and inexperience as an idol manager makes it difficult for him to effectively make decisions that help boost the groups career. He nevertheless cares for the girls and would take action if he sees the girls in an uncomfortable situation. In the movie, it is revealed that he used to play guitar.

; 
: 
:President of Green Leaves Entertainment and Koheis superior. A chain-smoker with a brash personality, she often harasses Kohei both physically and verbally on the job. She came up with the name "Wake Up Girls" (WUG for short) from a local motel that bears the same name, as well as inspiration from the group Wham!.

=== I-1 Club ===
; 
: 
:17-years-old and at her second year of High School. One of the 1st Generation members of the I-1 Club. She and Mayu competed for the center spot as part of president Shirakis stipulation and won the spot after defeating Mayu in sales records. Despite her center spot and among the most popular members of the I-1 Club, she continues to view Mayu as a rival. Nicknamed "Shihocchi".

; 
: 
:One of the most popular members of the I-1 Club. 16-years-old. She constantly sends text messages to her friend Mayu on the groups progress. She is also a 1st Generation member of the group nicknamed "Yoshimegu". She openly supports Mayu, and WUG as a whole, despite their two groups being rivals.

; 
: 
:A 1st Generation member and one of the oldest members of the I-1 Club at 20-years-old. She serves as the groups team captain and is very strict in maintaining their top performances and activities. Nicknamed "Maimai".

; 
: 
:19-years-old. She is a 2nd Generation member and one of the most popular members of the group in part because of her wearing glasses throughout her performances and promotions. Nicknamed "Nanokasu".

; 
: 
:13-years-old. She is the 4th Generation Member of the I-1 Club. Her goal is to one day become the clubs future center.

; 
: 
:20-years-old. She is the 2nd Generation Member of the I-1 Club and Mokas older sister.

; 
: 
:18-years-old. The only member of the group who is half-Japanese (her mother is English whereas her father is Japanese).

; 
: 
:He is the I-1 Clubs General Manager, Producer, Company President. His goal is to push the I-1 Club to be the most popular Entertainment Group in Japan. He shows little to no emotion and does not change his stern expression even when publicly giving a speech. Because of his ambitions, he imposes strict guidelines towards the group, in which among those guidelines was that members are forbidden to have any personal relationships and would frequently remove any member of the I-1 Club that does not meet his expectations during training drills, citing that it would tarnish the groups reputation. These led to tensions between him and former center idol Mayu Shimada, which led to her firing when Mayus next single did not beat rival Shiho in sales records.

:His belief of forbidding the members to have personal relationships are based on real life instances where idols who are known or even suggested to have such relationships are either demoted or removed from their respective groups. A recent example of such is that of Minami Minegishi.

; 
: 
:Introduced in Episode 6. An A-list music producer of which the I-1 Club is one of his clients. After seeing the Wake Up Girls dismal performance, he decides to offer his services to Green Leaves Entertainment at no charge in exchange for total control as manager, without intervention from Junko or Kohei. Under Tasukus control, the girls engage in a series of harsh training and a grueling performance schedule. Tasuku has gone far as to consider removing Airi from the group or disband the group entirely if they choose to keep Airi as a means of testing the girls commitment and unity. He is also the composer of the songs both WUG and I-1 perform during the Idol festival. He appears to really care for WUG as he was shown to be happy with their performance during said festival (and the crowds reaction to it) as well as disappointed when they didnt win.

=== Other characters ===
;Twinkle
:A successful singer–songwriter duo composed of Karina (Tomatsu Haruka) and Anna (Hanazawa Kana), whom Junko helped in boosting their careers. Theyre responsible for writing some of the songs for the Wake Up Girls to sing.

; 
: 
:A long-time fan of the I-1 Club. He attended the Wake Up Girls first concert and discovered Mayu as one of the members, reporting it later in social media. He continues to watch the girls performances as well as comments made in social media. He later assembles a small group to support WUG as their popularity grew and often holds loud meetings in restaurants much to the disturbance of the other customers and waitresses.

== Media ==

=== Social game === social game titled   launched in January 2014. In the game, the player takes the role of manager and must train the girls on their way to stardom. The game features unique idols not present in the anime.

The game was discontinued in December 15, 2014.

=== Anime ===
 
The film, titled  , was released in theaters on January 10, 2014.  Box office is under ¥ 10 million. The television series aired between January 10, 2014 and March 28, 2014. Last Blue Ray Disc Volume 6 was released on August 22, 2014, and sold 2,193 copies. The opening theme of the first two episodes is  , and from episode 3 onwards is 7 Girls War, both credited by the cast of Wake Up, Girls!. The ending theme is  , also performed by Wake Up, Girls!. An original net animation spin-off produced by Studio Moriken, titled  , began streaming on Avexs YouTube channel from September 5, 2014. The main theme is "WUG Zoo Zoo" by Wake Up, Girls!. 

====Episode list====
{|class="wikitable" style="width:98%; margin:auto; background:#FFF; table-layout:fixed;"
|- style="border-bottom: 3px solid #CCF"
! style="width:3em;"  | No.
! Title
! style="width:12em;" | Original airdate
|-
{{Japanese episode list EpisodeNumber   = 1 KanjiTitle      = 静かなる始動 RomajiTitle     = Shizukanaru Shidō EnglishTitle    = A Quiet Beginning OriginalAirDate = January 10, 2014 ShortSummary    = On Christmas 2013, the new idol group "Wake Up, Girls!" performed their debut concert on an outdoor stage in a small park in Sendai. However, their agencys president Tange disappeared with the companys money. The remaining manager Matsuda was at a loss, and the seven members were uncertain of their groups future.
}}
{{Japanese episode list EpisodeNumber   = 2 KanjiTitle      = ステージを踏む少女たち RomajiTitle     = Sutēji o Fumu Shōjo-tachi EnglishTitle    = Girls Standing Onstage OriginalAirDate = January 17, 2014 ShortSummary    = WUG resumes activity thanks to a Producer Sudo brought in by Matsuda. However, the job involved wearing risque swimsuits at a health spa. Despite their doubts about the obviously shady Sudo, the girls resolve to stand onstage, but the requirements continue to escalate.
}}
{{Japanese episode list EpisodeNumber   = 3 KanjiTitle      = 一番優しく RomajiTitle     = Ichiban Yasashiku EnglishTitle    = Kindest OriginalAirDate = January 24, 2014 ShortSummary    = The missing President Tange returns to Green Leaves, and WUG resumes activity in earnest. The president secures a 3 minute weather report and gourmet report mini-corner on a variety show for the girls. The members split into two groups to work on their segments, and Minamis eating habits attract attention.
}}
{{Japanese episode list EpisodeNumber   = 4 KanjiTitle      = スキャンダル RomajiTitle     = Sukyandaru EnglishTitle    = Scandal OriginalAirDate = January 31, 2014 ShortSummary    = Following their TV program, WUG makes smooth progress on the road to idoldom by appearing on a radio program and beginning work on their second song. However, as word of their activity spreads, people begin sharing less than kind words about former I-1 Club member Mayu.
}}
{{Japanese episode list EpisodeNumber   = 5 KanjiTitle      = 天国か地獄か RomajiTitle     = Tengoku ka Jigoku ka EnglishTitle    = Heaven or Hell OriginalAirDate = February 7, 2014 ShortSummary    = With their second song complete, WUG prepares for their second concert. However, WUGs concert falls on the same day as the I-1 Clubs Sendai Theater opening concert. The town is painted in the colors of the I-1 Club and WUG is overwhelmed by the idol groups power, but the girls put their best efforts into their lessons and advertise their own concert.
}}
{{Japanese episode list EpisodeNumber   = 6 KanjiTitle      = まだまだだよ RomajiTitle     = Madamada dayo EnglishTitle    = Not Yet OriginalAirDate = February 14, 2014 ShortSummary    = Music Producer Hayasaka suddenly reaches out to Green Leaves. While he was one of the people responsible for raising the I-1 Club to stardom and continues working with them, he now wants to produce WUG!
}}
{{Japanese episode list EpisodeNumber   = 7 KanjiTitle      = 素晴らしき仲間たち RomajiTitle     = Subarashiki Nakama-tachi EnglishTitle    = Wonderful Friends OriginalAirDate = February 21, 2014 ShortSummary    = Hayasaka announces his decision to remove Airi from WUG! and threatens to fire the remaining members if they do not agree.
}}
{{Japanese episode list EpisodeNumber   = 8 KanjiTitle      = 波乱 RomajiTitle     = Haran EnglishTitle    = Commotion OriginalAirDate = February 28, 2014 ShortSummary    = WUG! enters the annual Idol Festival in which idol groups from across the country compete to be crowned number one. When they learn the winning team will receive a contract with a mainstream record label, the members feel motivated.
}}
{{Japanese episode list EpisodeNumber   = 9 KanjiTitle      = ここで生きる RomajiTitle     = Koko de Ikiru EnglishTitle    = Living Here OriginalAirDate = March 7, 2014 ShortSummary    = When Yoshinos reservations towards Mayus resistance to talking about her past with I-1 boils over and a dark cloud hangs over the group, WUG takes a trip at Kayas suggestion.
}}
{{Japanese episode list EpisodeNumber   = 10 KanjiTitle      = 登竜門 RomajiTitle     = Tōryūmon EnglishTitle    = Gateway to Success OriginalAirDate = March 14, 2014 ShortSummary    = The Tohoku preliminaries of the Idol Festival approach. As everything from orthodox to unique idol groups enter the competition, President Tange asks the girls what it means to be WUG.
}}
{{Japanese episode list EpisodeNumber   = 11 KanjiTitle      = アイドル狂詩曲(ラプソディー) RomajiTitle     = Aidoru Rapusodī EnglishTitle    = Idol Rhapsody OriginalAirDate = March 21, 2014 ShortSummary    = Do to unexpected events, WUG is forced to learn a new song with no time remaining. Not only that, but the song and choreography are far more difficult than the songs theyve done before.
}}
{{Japanese episode list EpisodeNumber   = 12 KanjiTitle      = この一瞬に悔いなし RomajiTitle     = Kono Isshun ni Kui Nashi EnglishTitle    = No Regrets in This Moment OriginalAirDate = March 28, 2014 ShortSummary    = As their performance approaches, Mayu and the others notice something is wrong with Yoshino. WUG cant conceal their panic when a problem develops just before they go onstage.
}}
|}

==Notes==
 

==References==
 

==External links==
*    
*   at TV Tokyo  
*    
*  

 
 
 
 
 
 
 
 
 
 
 