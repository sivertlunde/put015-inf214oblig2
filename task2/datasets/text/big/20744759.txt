Chouchou (film)
 
{{Infobox film
| name = Chouchou
| image = 2003 Chouchou.jpg
| producer = Christian Fechner Hervé Truffaut
| director = Merzak Allouache
| writer = Merzak Allouache Gad Elmaleh
| starring = Gad Elmaleh Alain Chabat Claude Brasseur
| music = Gilles Tinayre Germinal Tenas
| cinematography = Laurent Machuel
| editing = Sylvie Gadmer
| distributor = Warner Bros.
| released =  
| country = France
| runtime = 105 minutes
| language = French
| budget =
| gross = 32 950 862 €
}}
 2003 French comedy film about a maghrebin transvestite who settles in Paris to find his nephew. The film stars Gad Elmaleh as the title character Chouchou, for which he was nominated the César Award for Best Actor.

==Cast==
*Gad Elmaleh : Chouchou 
*Alain Chabat : Stanislas 
*Claude Brasseur : Père Léon
*Julien Courbey : Yekea 
*Catherine Frot : Nicole Milovavovich 
*Micheline Presle : Stanislass Mother
*Jacques Sereys: Stanislass Father
*Roschdy Zem :  Frère Jean
*Arié Elmaleh : Vanessa 
*Yacine Mesbah : Djamila 
*Jean-Paul Comart : Le commissaire Molino 
*Anne Marivin: La vendeuse supermarché

== External links ==
*  
*  
*   on AlloCiné
 

 
 
 
 
 
 
 
 
 


 
 