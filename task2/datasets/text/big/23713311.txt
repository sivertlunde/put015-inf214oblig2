Man in the Vault
{{Infobox film
| name           = Man in the Vault
| image          = ManInTheVault1956Poster.jpg
| caption        = Film poster
| director       = Andrew V. McLaglen
| producer       = Robert E. Morrison John Wayne (uncredited)
| screenplay     = Burt Kennedy
| based on       =   William Campbell Karen Sharpe Anita Ekberg Henry Vars
| cinematography = William H. Clothier
| editing        = 
| studio         = Batjac Productions
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} William Campbell, Frank Gruber. The film was the directorial debut of Andrew V. McLaglen.

==Plot==
The criminal Willis Trent wants to rob the safety deposit box of a crooked Los Angeles businessman, Paul De Camp. He has lawyer Earl Farraday smooth-talk the guys girlfriend, the two-timing Flo Randall, into revealing the bank boxs number.

Now they need a locksmith. A henchman called Herbie is sent to find one. He settles on Tommy Dancer, who works in a bowling alley. Tommy is quickly smitten with Earls girl Betty Turner but is a law-abiding citizen and rejects an offer of $5,000.

Tommy falls for Betty, taking her to the Hollywood Bowl and learning she comes from a wealthy family. Tommys attentions to her get him a beating from Louie, another big thug. He is told Bettys face will be disfigured if he refuses to cooperate.

Breaking into the box is no problem, but Tommy thinks hes been double-crossed by Betty and decides to keep the $200,000. He stashes the cash in a locker at the bowling alley. Flo confesses her part in the scheme to De Camp, who goes after Tommy, even hurling bowling balls at him before the cops show up.

Tommy races to save Betty, realizing shes on the level. Trent ends up dead, and Tommys future is a lock.

==Cast== William Campbell as Tommy Dancer
*Karen Sharpe as Betty Turner
*Anita Ekberg as Flo Randall
*Berry Kroeger as Willis Trent (as Berry Kroger)
*Paul Fix as Herbie
*James Seay as Paul De Camp
*Mike Mazurki as Louie
*Robert Keys as Earl Farraday
*Nancy Duke as Trents girlfriend
*Pedro Gonzalez-Gonzalez as Pedro (as Gonzales Gonzales)
*John Mitchum as Andy (uncredited)

== References ==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 