How to Train Your Dragon 2
 
{{Infobox film
| name = How to Train Your Dragon 2
| image = How to Train Your Dragon 2 poster.jpg
| border = yes
| alt = A dark haired boy, holding a helmet by his side, his friends and a black dragon behind him. Dragons are flying overhead.
| caption = Theatrical release poster
| director = Dean DeBlois
| producer = Bonnie Arnold
| screenplay = Dean DeBlois
| based on =  
| starring = {{Plain list |
* Jay Baruchel
* Cate Blanchett
* Gerard Butler
* Craig Ferguson
* America Ferrera
* Jonah Hill
* Christopher Mintz-Plasse
* T. J. Miller|T.J. Miller
* Kristen Wiig
* Djimon Hounsou
* Kit Harington
}} John Powell
| cinematography = Roger Deakins  
| editing = John K. Carr
| studio = DreamWorks Animation
| distributor = 20th Century Fox
| released =  
| runtime = 102 minutes  
| country = United States
| language = English
| budget = $145 million   
| gross = $618.9 million   
}} 3D Computer-animated action fantasy book series How to Train Your Dragon and the second in the How to Train Your Dragon (franchise)|trilogy. The film is written and directed by Dean DeBlois, and stars the voices of Jay Baruchel, Gerard Butler, Craig Ferguson, America Ferrera, Jonah Hill, Christopher Mintz-Plasse, T. J. Miller|T.J. Miller and Kristen Wiig with the addition of Cate Blanchett, Djimon Hounsou and Kit Harington. The film takes place five years after the first film, featuring Hiccup and his friends as young adults as they meet Valka, Hiccups long-lost mother, and Drago Bludvist, a madman who wants to conquer the world. 
 John Powell returned to score the film. How to Train Your Dragon 2 benefited from advances in animation technology and was DreamWorks first film to use scalable multi-core processing and the studios new animation and lighting software.

The film was released in the United States on June 13, 2014, and like its predecessor, received wide acclaim. Critics praised the film for its animation, voice acting, action scenes, emotional depth, and darker, more serious tone compared to its predecessor. The film grossed over $618 million worldwide, making it the twelfth highest-grossing film of 2014. It earned less than its predecessor at the US box office, but performed better internationally.  The third and final film in the trilogy, titled How to Train Your Dragon 3, is scheduled to be released on June 29, 2018.
 Best Animated Feature and Best Director.

==Plot==
 
 Viking village of Berk made peace with the dragons, they now live among the villagers as helpful companions. Hiccup goes on adventures with his dragon, Toothless, as they discover and map unexplored lands. Having come of age, he is being pressed by his father, Stoick the Vast, to succeed him as chieftain, although Hiccup remains unsure if he is ready for this responsibility.

While investigating a burnt forest, Hiccup and Astrid discover the remains of a fort encased in ice and encounter a group of dragon trappers led by Eret, who blames them for his forts destruction and attempts to capture their dragons for an insane conqueror named Drago Bludvist. The two dragon riders escape and return to Berk to warn Stoick about the dragon army that Drago is amassing. Stoick orders the villagers to fortify the island and prepare for battle. Hiccup, however, refuses to believe that war is inevitable. After Stoick interrupts Hiccups plan to get Eret to take him to Drago, Stoick explains that he once met Drago at a gathering of chiefs, where Drago, mocked after offering the chiefs his service in return for their servitude, murdered them all, with Stoick as the only survivor. Undeterred, Hiccup flies off with Toothless in search of Drago to try to reason with him.
 Alpha dragon called a "Bewilderbeast", which controls smaller dragons. Stoick tracks Hiccup to the nest, where he discovers that his wife is still alive. Simultaneously, Astrid and the other riders kidnap Eret to find Drago, but Drago captures them and learns of Berks dragons.

Drago and his army lay siege to the nest, where he reveals that he has his own Bewilderbeast to challenge the Alpha. A battle ensues between the two colossal dragons, which ends with Dragos Bewilderbeast killing the nests Bewilderbeast and becoming the new Alpha. Dragos Bewilderbeast then seizes control of all the adult dragons, who hypnotically obey. Hiccup tries to persuade Drago to end the violence, but Drago orders him killed. Toothless, under the Bewilderbeasts influence, approaches Hiccup and launches a blast, but Stoick pushes Hiccup out of the way and is hit instead, killing him. The Bewilderbeast momentarily relinquishes control of Toothless, but Hiccup drives him away in a fit of despair. Drago maroons Hiccup and the others on the island and rides Toothless, again under the control of the Bewilderbeast, to lead his army to conquer Berk. Stoick is given a Viking funeral and Hiccup, now having lost both his father and dragon, is unsure what to do. Valka encourages him by telling him that he alone can unite humans and dragons, and inspired by her words and his fathers, Hiccup decides to return to Berk to stop Drago.

The dragon riders fly baby dragons back to Berk, as they are immune to the Bewilderbeasts control. They find that Drago has already attacked the village and taken control of its dragons. Hiccup confronts Drago and a brainwashed Toothless while the other riders work to distract the Bewilderbeast. Hiccup succeeds in freeing Toothless from the Bewilderbeasts control, much to Dragos surprise. Hiccup and Toothless briefly separate Drago from the Bewilderbeast and confront Drago on the ground, but the Bewilderbeast attacks them, encasing them in ice. However, Toothless blasts away the ice, revealing that both he and Hiccup are unharmed. He then challenges the Bewilderbeast, firing at it repeatedly, which breaks its control over the other dragons, who now side with Toothless as the new Alpha dragon. All the dragons repeatedly fire at the Bewilderbeast until Toothless fires a final massive blast, breaking its left tusk. Defeated, the Bewilderbeast retreats under the sea with Drago on its back. The Vikings and dragons celebrate their victory and Hiccup is made chieftain of Berk.

Afterwards, Berk undergoes repairs while feeling secure knowing that its dragons can defend it.

==Voice cast==
{{multiple image
| direction = horizontal
| image1    = Cate Blanchett Cannes 2014.jpg
| alt1      = Cate Blanchett
| width1    = 144
| image2    = Djimon Hounsou Cannes 2014.jpg
| alt2      = Djimon Hounsou
| width2    = 175
| footer            = Cate Blanchett and Djimon Hounsou, who have joined the cast as Valka and Drago Bludvist respectively, attending the films premiere at the 2014 Cannes Film Festival.
| footer_align      =  
}}
* Jay Baruchel as Hiccup Horrendous Haddock III, son of the Viking chief Stoick the Vast and Valka. His best friend and dragon is Toothless, a Night Fury.
* Cate Blanchett as Valka, Stoicks wife and Hiccups long-lost mother.    She rides the dragon Cloudjumper, a Stormcutter, who carried her off during an earlier dragon attack on Berk.
* Gerard Butler as Stoick the Vast, the chieftain of the Viking tribe of Berk, Hiccups father and Valkas husband. He rides the dragon Skullcrusher, a Rumblehorn.
* Craig Ferguson as Gobber the Belch, Stoicks closest friend and a seasoned warrior. He rides the dragon Grump, a Hotburple.
* America Ferrera as Astrid Hofferson, Hiccups girlfriend.  She rides the dragon Stormfly, a Deadly Nadder.
* Christopher Mintz-Plasse as Fishlegs Ingerman. He rides the dragon Meatlug, a Gronckle.
* Jonah Hill as Snotlout Jorgenson. He rides the dragon Hookfang, a Monstrous Nightmare.
* T.J. Miller and Kristen Wiig as Tuffnut and Ruffnut Thorston, fraternal twins. They share a two-headed Hideous Zippleback named Barf and Belch.
* Djimon Hounsou as Drago Bludvist, an evil warlord and dragon hunter who seeks to take over the world with a dragon army.    He has a prosthetic left arm, since he lost his real arm during an earlier encounter with dragons, and wears a dragon-hide cape immune to dragon fire attacks.
* Kit Harington as Eret, son of Eret, a dragon trapper who sells captured dragons to Drago, and Ruffnuts love interest.     He sports a scar on his chest given to him by Drago following the last time he showed up with no dragons.

==Production==

===Development===
After the success of the first film, the sequel was announced on April 27, 2010.     "How to Train Your Dragon ... has become DreamWorks Animations next franchise. We plan to release the sequel theatrically in 2013," said Jeffrey Katzenberg, DreamWorks Animations CEO.  It was later revealed that DeBlois had started drafting the outline for a sequel in February 2010 at Skywalker Ranch, during the final sound mix of the first film.      The film was originally scheduled for release on June 20, 2014,    but in August 2013 the release date was moved forward one week to June 13, 2014. 

 .]]
The film was written, directed, and executive produced by   and My Neighbor Totoro    having the pivotal inspirations for the film.  "What I loved especially about Empire is that it expanded Star Wars in every direction: emotionally, its scope, characters, fun. It felt like an embellishment and thats the goal."   

The entire original voice cast&nbsp;– Baruchel, Butler, Ferguson, Ferrera, Hill, Mintz-Plasse, Miller and Wiig&nbsp;– returned for the sequel.  On June 19, 2012, it was announced that Kit Harington, of Game of Thrones fame, was cast as one of the films antagonists.    At the 2013 San Diego Comic-Con International, it was announced that Cate Blanchett and Djimon Hounsou had joined the cast; they lent their voices to Valka and Drago Bludvist, respectively. 

While the first film was set in a generic North Sea environment, the creative team decided to focus on Norway this time around.  Early in the sequels development, about a dozen of them traveled there for a week-long research trip, where they toured Oslo, Bergen, and the fjords.   DeBlois, together with Gregg Taylor (DreamWorks head of feature development) and Roger Deakins (a cinematographer who served as visual consultant), then broke off from the group to visit Svalbard and see polar bears in the wild with the assistance of armed guides.  

DeBlois explained that he had learned from directing Lilo and Stitch (2002) that "if you set an animated film in a place you want to visit, theres a chance you might get to go there."   He had wanted to visit Svalbard for some time, after learning of its stark beauty from a couple of backpackers he met during earlier visits to Iceland to work with post-rock band Sigur Rós on the 2007 documentary film Heima.  

===Animation===
  Barack Obama tried a motion capture camera of the kind used to capture live-action reference performance for the film. ]]
Over the five years before the films release,    DreamWorks Animation had substantially over-hauled its production workflow and animation software. How to Train Your Dragon 2 was the first DreamWorks Animation film that used "scalable multi-core processing", developed together with Hewlett-Packard. Called by Katzenberg as "the next revolution in filmmaking," it enabled artists for the first time to work on rich complex images in real time, instead of waiting eight hours to see the results next day.    The film was also the studios first film to use its new animation and lighting software through the entire production. Programs named Premo  and Torch allowed much more subtlety, improving facial animation and enabling "the sense of fat, jiggle, loose skin, the sensation of skin moving over muscle instead of masses moving together."   
 Glendale as Redwood City and DreamWorks India in Bangalore.  

==Release==
 . ]] Regency Village IMAX 3D and released to international theaters on June 13, 2014. 

===Home media===
How to Train Your Dragon 2 was released digitally on October 21, 2014, and was released on DVD and Blu-ray on November 11, 2014.    The Blu-ray combo pack and digital release is accompanied by a new animated short film Dawn of the Dragon Racers, in which Hiccup and friends compete to become the first Dragon Racing Champion of Berk.  A double DVD pack with the film and Dawn of the Dragon Racers was released exclusively at Walmart stores.  As of February 2015, 7.5 million home entertainment units have been sold worldwide. 

==Reception==

===Critical response===
  gave the film a score of 76/100 based on reviews from 39 critics, indicating "generally favorable reviews".  Audiences surveyed by CinemaScore during the opening weekend gave the film a grade A.  Audiences were a mix of 53% female and 47% male. Children responded most strongly, with those aged under 25 giving a grade A+. 

At the   has prevailed, serving up DreamWorks Animations strongest sequel yet&nbsp;— one that breathes fresh fire into the franchise, instead of merely rehashing the original. Braver than Brave (2012 film)|Brave, more fun than Frozen (2013 film)|Frozen, and more emotionally satisfying than so many of its live-action counterparts, Dragon delivers. And good thing, too, since DWA desperately needs another toon to cross the half-billion-dollar threshold."  Elizabeth Weitzman of the New York Daily News gave the film three out of five stars, saying Its the unflinching edge that gives the film its unexpected depth."  Jocelyn Noveck of the Associated Press gave the film three out of four stars, saying "How to Train Your Dragon 2 doesnt play it safe, and thats why its the rare sequel that doesnt feel somewhat stale."  Lou Lumenick of The New York Post gave the film three out of four stars, saying "Dragon 2 really soars when our hero is aloft, imparting some important lessons about family, ecology and war for young audiences. It should also do very healthy business for hit-starved DreamWorks Animation."  Joe McGovern of Entertainment Weekly gave the film a B, saying "The flight path needs straightening, but this is still a franchise that knows how to fly."  Jody Mitori of the St. Louis Post-Dispatch gave the film three out of four stars, saying "For audiences who want a sweet story, they cant beat the first film of a boy finding his best friend. For those who are ready for the next stage, try this one about a boy becoming a man." 

Bill Goodykoontz of The Arizona Republic gave the film four out of five stars, saying "It seemed as if there was nowhere new to go after the first film, but this is a richer story that dares to go darker and is thus more rewarding."  Peter Travers of Rolling Stone gave the film three and a half stars out of four, saying "Dragon 2, like The Empire Strikes Back, takes sequels to a new level of imagination and innovation. It truly is a high-flying, depth-charging wonder to behold."  Peter Hartlaub of the San Francisco Chronicle gave the film three out of four stars, saying "DeBlois, who also wrote the script, successfully juggles the multiple story lines, shifting allegiances and uncharted lands."  Rafer Guzman of Newsday gave the film three out of four stars, saying "Gruesome? A little. Scary? You bet. But thats exactly what makes the "Dragon" films so different, and so much better, than the average childrens fare."  Michael Phillips of the Chicago Tribune gave the film three and a half stars out of four, saying "For once, we have an animated sequel free of the committee-job vibe so common at every animation house, no matter the track record."  Stephen Holden of The New York Times gave the film a negative review, saying "The story seems to be going somewhere until it comes to a halt with the inevitable showdown between the forces of darkness and the forces of light."  Peter Howell of the Toronto Star gave the film three out of four stars, saying "Taking its cues as much from Star Wars and Game of Thrones as from its own storybook narrative, How to Train Your Dragon 2 breathes fire into a franchise sequel." 

Claudia Puig of USA Today gave the film three out of four stars, saying "Nearly as exuberant as the original, How to Train Your Dragon 2 nimbly avoids sequel-itis."  Colin Covert of the Star Tribune gave the film four out of four stars, saying "The impressive part is the storytelling confidence of writer/director Dean DeBlois. He has created a thoughtful tale as meaningful for grown-ups as it is pleasurable for its young primary audience."  Stephanie Merry of The Washington Post gave the film three and a half stars out of four, saying "This may be the first and last time anyone says this, but if How to Train Your Dragon 2 is this good, why stop at 3 and 4?"  Moira MacDonald of The Seattle Times gave the film three and a half stars out of four, saying "Young and old fans of the first movie will be lining up for the wit, for the inventiveness of the characters, for the breathtaking visuals&nbsp;— and just the sheer fun of it all."  Tirdad Derakhshani of The Philadelphia Inquirer gave the film three and a half stars out of four, saying "One of this years true surprises, the superior animated sequel not only is infused with the same independent spirit and off-kilter aesthetic that enriched the original, it also deepens the first films major themes."  Stephen Whitty of the Newark Star-Ledger gave the film two and a half stars out of four, saying "This was not a sequel that anybody needed, outside of the accountants. And theres another already planned."  John Semley of The Globe and Mail gave the film four out of four stars, saying "More than just teaching kids what to think about the world theyre coming into, its a rare film that encourages them to think for themselves." 

Rene Rodriguez of the Miami Herald gave the film three and a half stars out of four, saying "How to Train Your Dragon 2 is its own standalone picture, with a surprising range of emotions that surpasses the original and a brisk pace and manner of storytelling that give it purpose and direction. The fact that its also so much fun, no matter what your age, almost feels like a bonus."  Bill Zwecker of the Chicago Sun-Times gave the film four out of four stars, saying "Not only does this second movie match the charm, wit, animation skill and intelligent storytelling of the original, I think it even exceeds it."  Lisa Kennedy of The Denver Post gave the film a positive review, saying "How to Train Your Dragon 2 is soaring, emotionally swooping, utterly satisfying fun."  Bob Mondello of NPR gave the film an 8.5 out of 10, saying "Its clear that   took inspiration from the first Star Wars trilogy&nbsp;— not a bad model for breathing new life, and yes, a bit of fire, into one of Hollywoods more nuanced animated franchises."  Inkoo Kang of The Wrap gave the film a mixed review, saying "If there isnt enough to feel, at least theres a lot to look at. Thanks to the superb 3-D direction by DeBlois, we swoop through the air, whoosh down dragons tails, and juuust baaaarely   squeeze into small crevices, but still, those experiences are only like being on a really great rollercoaster&nbsp;— they dont mean anything."  A.A. Dowd of The A.V. Club gave the film a B-, saying "There arent just more dragons, but more characters, more plot, more everything. The trade-off is that the charm of the original gets a little lost, a casualty of rapid-franchise expansion." 

===Box office===
How to Train Your Dragon 2 grossed $177 million in North America, and $441.9 million in other countries, for a worldwide total of $618.9 million.  The film is the second highest-grossing animated film of 2014, behind Big Hero 6 (film)|Big Hero 6, and the twelfth highest-grossing film of the year in any genre.        While How to Train Your Dragon 2 only earned $177 million at the US box office, compared to $217 million for its predecessor,  it performed much better at the international box office, earning $438 million to How to Train Your Dragons $277 million. Calculating in all expenses, Deadline.com estimated that the film made a profit of $107.3 million. 

In the United States and Canada, the film earned $18.5 million on its opening day,  and opened at number two in its first weekend, with $49,451,322.  In its second weekend, the film dropped to number three, grossing an additional $24,719,312.  In its third weekend, the film stayed at number three, grossing $13,237,697.  In its fourth weekend, the film dropped to number five, grossing $8,961,088. 

Its $25.9 million opening weekend in China was the biggest ever for an animated film in the country, surpassing the record previously held by Kung Fu Panda 2.   

===Accolades===
{| class="wikitable"
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | List of awards and nominations
|- style="text-align:center;"
!  style="background:#ccc; width:40%;"| Award / Film Festival
!  style="background:#ccc; width:30%;"| Category
!  style="background:#ccc; width:25%;"| Recipient(s)
!  style="background:#ccc; width:15%;"| Result
|- 42nd Annie Annie Awards  Annie Award Best Animated Feature
|How to Train Your Dragon 2
| 
|- Animated Effects in an Animated Production James Jackson, Lucas Janin, Tobin Jones, Baptiste Van Opstal, Jason Mayer
| 
|- Character Animation in a Feature Production Fabio Lignini 
| 
|- Steven "Shaggy" Hornby
| 
|- Thomas Grummt 
| 
|- Annie Award Directing in a Feature Production Dean DeBlois
| 
|- Annie Award Music in a Feature Production John Powell, Jónsi
| 
|- Storyboarding in an Animated Feature Production Truong "Tron" Son Mai
| 
|- Annie Award Writing in a Feature Production Dean DeBlois
| 
|-
| Editorial in an Animated Feature Production John K. Carr
| 
|- 87th Academy Academy Awards Best Animated Feature Dean DeBlois and Bonnie Arnold
| 
|- British Academy Childrens Awards 
| Kids Vote - Film in 2014
|How to Train Your Dragon 2
| 
|-
|Childrens Feature Film in 2014
|How to Train Your Dragon 2
| 
|-
|Critics Choice Movie Award 	 Best Animated Feature
|How to Train Your Dragon 2
| 
|- 72nd Golden Golden Globe Awards  Golden Globe Best Animated Feature Film
|How to Train Your Dragon 2
| 
|- Hollywood Film Awards  Best Hollywood Animation Award 
|How to Train Your Dragon 2
| 
|- National Board of Review  Best Animated Feature
|How to Train Your Dragon 2
| 
|- 2015 Kids Nickelodeon Kids Choice Awards  Favorite Animated Movie
|How to Train Your Dragon 2
| 
|- Online Film Critics Society  Best Animated Feature
|How to Train Your Dragon 2
| 
|-
|Peoples Choice Awards  Favorite Family Movie
|How to Train Your Dragon 2
| 
|- Phoenix Film Critics Society  Best Animated Film
|How to Train Your Dragon 2
| 
|- Producers Guild of America
| Best Outstanding Producer of Animated Theatrical Motion Pictures
| Bonnie Arnold
|  
|- San Francisco San Francisco Film Critics Circle Award  San Francisco Best Animated Feature
|How to Train Your Dragon 2
| 
|- Satellite Awards  Best Motion Picture Animated or Mixed Media
|How to Train Your Dragon 2
| 
|- Washington D.C. Area Film Critics Association  Best Animated Feature
|How to Train Your Dragon 2
| 
|- 41st Saturn Saturn Awards  Saturn Award for Best Animated Film
|How to Train Your Dragon 2
| 
|- Saturn Award for Best Music
|John Powell
| 
|}

==Soundtrack==
 
{{Infobox album
| Name       = How to Train Your Dragon 2
| Type       = Soundtrack John Powell
| Cover      =
| Released   = June 17, 2014
| Recorded   = 2014
| Genre      = Film score
| Length     = 60:07
| Label      = Relativity Music Group
| Producer   =
| Chronology = John Powell film scores
| Last album = Rio 2  (2014)
| This album = How to Train Your Dragon 2  (2014)
| Next album =
}}

Composer  , returned to score the sequel. Powell described the project "a maturation story" and stated that he too tried to achieve the same maturation in the structure of his music by developing further every aspect of his compositions from the original film.   
 The Red Hot Chilli Pipers.    The ensemble was conducted by the composers usual collaborator Gavin Greenaway.   

 , provided two new original songs for the sequel in collaboration with Powell.    Belarusian-Norwegian artist Alexander Rybak, who voices Hiccup in Norwegian, also wrote and performed the song "Into a Fantasy". The latter song is featured only in the European versions of the film. 

A soundtrack album for the film was released on June 17, 2014 by Relativity Music Group.   The album features over an hour of score by Powell, as well as the two original songs written by Powell and Jónsi. Rybaks song, "Into a Fantasy", was released separately as a single.

{{Track listing
| headline        = Track listing
| all_music       =
| total_length    =
| title1          = Dragon Racing
| length1         = 4:34

| title2          = Together We Map the World
| length2         = 2:19

| title3          = Hiccup the Chief/Dragos Coming
| length3         = 4:44

| title4          = Toothless Lost
| length4         = 3:28

| title5          = Should I Know You?
| length5         = 1:56

| title6          = Valkas Dragon Sanctuary
| length6         = 3:19

| title7          = Losing Mom/Meet the Good Alpha
| length7         = 3:24

| title8          = Meet Drago
| length8         = 4:26

| title9          = Stoick Finds Beauty
| length9         = 2:33

| title10         = Flying with Mother
| length10        = 2:49

| title11         = For the Dancing and the Dreaming
| note11          = performed by Gerard Butler, Craig Ferguson & Mary Jane Wells
| length11        = 3:06

| title12         = Battle of the Bewilderbeast
| length12        = 6:26

| title13         = Hiccup Confronts Drago
| length13        = 4:06

| title14         = Stoick Saves Hiccup
| length14        = 2:23

| title15         = Stoicks Ship
| length15        = 3:48

| title16         = Alpha Comes to Berk
| length16        = 2:20

| title17         = Toothless Found
| length17        = 3:46

| title18         = Two New Alphas
| length18        = 6:06

| title19         = Where No One Goes
| note19          = performed by Jónsi
| length19        = 2:44
}}
{{Track listing
| headline        = European/Slavic bonus track
| collapsed       = no

| title20       = Into A Fantasy
| note20        = performed by Alexander Rybak
| length20      = 3:32
}}

==Video game==
A video game based on the film, titled How to Train Your Dragon 2, was released in June 2014 by Little Orbit.  Developed by Torus Games, the game is available for Xbox 360, Nintendo 3DS, Wii, Wii U and PlayStation 3.  It allows players to choose various riders and dragons, and enter a dragon flight school, participating in trainings, challenges and tournaments.   

==Sequel==
 
 
The third and final film, titled How to Train Your Dragon 3,    was originally scheduled for release on June 17, 2016,    but in September 2014, DreamWorks Animation moved the release date to June 9, 2017.   In January 2015, in the wake of the closure of Pacific Data Images and massive lay-offs, the release date was pushed back to June 29, 2018.  Dean DeBlois, the co-screenwriter/co-director of the first and writer-director of the second film, will return, along with producer Bonnie Arnold and all the main cast,    while composer John Powell, who scored the first two films, will also be returning.   Cate Blanchett will also reprise her role as Valka from the second film.    

==See also==
 
* List of animated feature-length films
* List of computer-animated films

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 