Razor Eaters
{{Infobox film
| name           = Razor Eaters
| image	         = Razor_Eaters_Poster.jpg
| image size     =
| alt            = 
| caption        = 
| director       = Shannon Young
| producer       = Nick Levy Paul Moder
| writer         = Shannon Young
| screenplay     = 
| story          =
| based on       = 
| narrator       = 
| starring       = Richard Cawthorne Angus Sampson Fletcher Humphrys Julie Eckersley Teague Rook
| music          = 
| cinematography = Karl Siemon
| editing        = 
| studio         = Hybrid Films
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = Australia English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Australian crime crime thriller anarchist street gang (based on the real-life Hedge-Burners) who terrorize those whom they believe deserve punishment.  

Razor Eaters was met with some controversy, as the Melbourne police department labeled it as “Extremely Violent” and “Obscene” when it was first released in 2003.  

==Synopsis==
Unhappy with the modern day legal system, five men have decided to band together to hand out vigilante justice to anyone they think deserves it. This list is rather loose, as they include tailgaters along with drug dealers and other serious criminals. The band, which calls themselves "Razor Eaters" and videotapes all of their activities, has attracted the attention of Detective Danny Berdan (Paul Moder), who finds himself captivated by the gangs footage.

==Cast==
*Paul Moder as Danny Berdan
*Richard Cawthorne as Zach
*Teague Rook as Orville
*Fletcher Humphrys as Roger
*Campbell Usher as Anthony
*Shannon Young as Rob
*Matt Robertson as Chris
*Julie Eckersley as Jenny
*David Bradshaw as Hersch
*Vincent Gil as Lonnie Evans (as Vince Gil)
*Peter Hosking as Hurstleigh
*Angus Sampson as Syksey
*Craig Madden as Garth
*David Serafin as Parking Inspector
*Chris McLean as Dean

==Production==
Young loosely based the films concept on the "Hedge Burners", a group of vandals that videotaped themselves vandalizing property.    He chose to escalate the actions to become more violent for the film, as Young believed that having the Razor Eaters only commit vandalism would "get boring in about half an hour" and that they needed "to have a purpose, a goal".  Young decided to make the gangs goal be infamy and stated that "I think we set a really dangerous standard by the amount of publicity that we give to criminals."  In order to flesh out the characters, Young had the actors improvise various scenarios such as "how they met, how they came together as a group".   

==Reception==
Critical reception for Razor Eatershas been predominantly positive.  Film Threat wrote a positive review and gave Razor Eaters five stars while stating it was "classy, intrepid and totally refined".  JoBlo.com gave a positive review for the film but a negative review for the DVD release, criticizing its picture and audio quality.  

===Awards===
*Best Use of the Guerilla Aesthetic at the Melbourne Underground Film Festival (2003, won)
*Special Jury Award at the Melbourne Underground Film Festival (2003, won)
*Festival Prize for Best Actor (Richard Cawthorne) at Shriekfest (2005, won)    
*Best Thriller Feature at Shriekfest (2005, won) 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 

 
 