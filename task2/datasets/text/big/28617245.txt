The Moonstone (1934 film)
{{Infobox film
| name           = The Moonstone
| image          =
| image_size     =
| caption        =
| director       = Reginald Barker
| producer       = Paul Malvern
| writer         = Adele Buffington
| based on = The Moonstone by Wilkie Collins
| narrator       =
| starring       = See below
| music          =
| cinematography = Robert H. Planck
| editing        = Jack Ogilvie Carl Pierson
| distributor    = Monogram Pictures
| released       =
| runtime        = 62 minutes 46 minutes (DVD)
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 1915 and The Moonstone (1909 film)|1909, although a number of television and radio adaptations have been made.

==Production==
The film was made by Monogram Studios, one of the smaller Hollywood outfits often known collectively as Poverty Row. The adaptation of a prestigious British Victorian novel marked a break from their usual films which were generally cheaply made American-set Westerns. To fit the story into a limited running time, large amounts of the original novel are dropped from the adaptation. 

== Plot summary ==
As with the book, the film is based around the Herncastle Moonstone, a valuable diamond from India.

== Cast ==
*David Manners as Franklyn Blake
*Phyllis Barry as Ann Verinder (misspelled Anne in opening credits)
*Gustav von Seyffertitz as Carl Von Lucker
*Jameson Thomas as Godfrey Ablewhite
*Herbert Bunston as Sir John Verinder Charles Irwin as Inspector Cuff
*Elspeth Dudgeon as Betteredge, Housekeeper John Davidson as Yandoo Claude King as Sir Basil Wynard
*Olaf Hytten as Dr. Ezra Jennings
*Evalyn Bostock as Roseanna Spearman, Maid
*Fred Walton as Henry the Butler
*John Power as The Driver
*Harold Entwistle as Sutter
*A.C. Henderson as Robbin

==References==
 

==Bibliography==
* Reid, John H. B Movies, Bad Movies, Good Movies. Lulu Press, 2004.

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 