Magic Wonderland
{{Infobox film
| name           = Magic Wonderland
| image          = Magic Wonderland poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Wu Jianrong Fang Lei
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Yi Yi Yang Ying Zhang Lu
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 90 minutes
| country        = China
| language       = Mandarin
| budget         = 
| gross          = US$3.06 million
}}
 Chinese animated fantasy adventure film directed by Wu Jianrong and Fang Lei.  

==Plot==
A baby princess named Ocean is sent away from the North Pole by her mother because of the tyranny of the magma dragon who controls the volcano. The next twelve years, Princess Ocean lives on a local island and encounters a transforming polar bear whom she names Roly Poly. And she talks to a duo of little fish. While encountering the pirates, she meets an inventor named Momo. Their plan is to subdue and defeat the magma dragon.

==Cast==
===English version===
*Lucy Liu as Princess Ocean
*Grey DeLisle as Little Roly Poly
*Denis Leary as Big Roly Poly

===Chinese version=== Yi Yi
*Ge Yuying Yang Ying
*Ling Juan Zhang Lu

==Related TV series==
The first episode is called "The Birth of the Magic Globe".

==Reception==
The film has grossed US$3.06 million at the Chinese box office. 

==References==
 

 
 
 
 

 
 
 
 
 
 
 
 