Mercenary for Justice
{{Infobox film
| name           = Mercenary for Justice
| image          = Mercenary for Justice.jpg
| caption        = 
| director       = Don E. Fauntleroy
| producer       = Randall Emmett George Furla
| writer         = Steve Collins
| starring       = Steven Seagal Jacqueline Lord Roger Guenveur Smith Stephen Edwards
| cinematography = Don E. FauntLeRoy
| editing        = Robert A. Ferretti
| studio         = Emmett/Furla/Oasis Films|Emmett/Furla Films
| distributor    = Millennium Films
| released       =  
| runtime        = 91 minutess
| country        = Aruba United States South Africa
| language       = English
| budget         = $15,000,000 
}}
Mercenary for Justice is an action film starring Steven Seagal, and also starring Luke Goss and Roger Guenveur Smith. It was released direct-to-video on April 18, 2006. Principal photography was on location in Cape Town, South Africa.

== Plot ==
CIA dirty deeds man John Dresham (Luke Goss) and Black Ops organiser Anthony Chapel (Roger Guenveur Smith) hire John Seeger (Steven Seagal) and his crew for a mission in the French-controlled Galmoral Island in Southern Africa. They tell them they are helping the locals when in reality they just want to get rich on petrol and diamonds.

John gets steamed when the mission goes wrong. Some of his soldiers take the French Ambassador (Rudiger Eberle) and his family hostage for leverage and later blow them all up. The French close in on them, his best friend Radio Jones (Zaa Nkweta) is killed, and Maxine Barnol, his spy posing as a journalist, suggests CIA involvement.

John heads back to the U.S.A. and goes to the home of Radios wife Shondra (Faye Peters), tells her the news, and then promises her that hell take care of her and her young son Eddie (Tumi Mogoje). While there he kills two of Dreshams men sent to kill him and discovers Dreshams implication.
 Peter Butler), who has been arrested and thrown into the Randveld Prison outside of Cape Town, South Africa, and is due to be transferred to the States.

Dresham discovers the job but not its object and when he bumps into Maxine he forces her to work for him instead of Chapel. Maxine leads him to believe that the target is the safe of the bank of South Africa and Dresham uses his CIA influence to be shown round the security installation. Maxine listens attentively and takes photos.

John leads Dresham in circles but when the mercenaries break into the prison Kamal isnt there any more and the bad elements of the team get killed. Next stop the bank. In the safe John persuades the Greeks to arrest Kamals father, then escapes making sure Dresham will be arrested too.

Finally, with a few faithful members of his team, John rescues Shondra and Eddie and kills Chapel and his guards.

== Cast ==
* Steven Seagal....John Seeger
* Jacqueline Lord....Maxine Barnol
* Roger Guenveur Smith....Chapel
* Luke Goss....Dresham
* Adrian Galley....Bulldog
* Michael Kenneth Williams....Samuel
* Langley Kirkwood....Kreuger
* Jeannie de Gouveia... Bank assistant
* Director: Don E. FauntLeRoy

==Sequel==

A sequel titled: "The Mercenary: Absolution" has been announced with a planned release of 2014-15. 

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 