La Vendetta (film)
{{Infobox film
| name           = La Vendetta
| image          = 
| caption        = 
| director       = Jean Chérasse
| producer       = Le Trident, S.I.F.E.C (Paris)   M.E.C Cinematografica (Rome)
| writer         = Albert Valentin Pascal Bastia
| starring       = Francis Blanche Louis de Funès
| music          = Derry Hall
| cinematography = 
| editing        = 
| distributor    = 
| released       = 18 April 1962 (France)
| runtime        = 90 minutes
| country        = Italy/France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy film from 1962, directed by Jean Chérasse, written by Albert Valentin, starring Francis Blanche and Louis de Funès. The film was known under the titles: "Bandito sì... ma donore" (Italy), "The Vendetta" (international English title). 

== Cast ==
* Francis Blanche : Le capitaine Bartoli, a candidate for the town hall
* Louis de Funès : Valentino Amoretti, the robber of honour and father of Antonia
* Marisa Merlini : the postal worker
* Olivier Hussenot : Mr Lauriston, the gentle rentier
* Jean Lefebvre : Colombo, a supporter of Bartoli
* Rosy Varte : Mrs Marthe Lauriston, wife of the rentier
* Jean Hoube : Michel Lauriston, the nephew
* Christian Mery : the teacher, supporter of Bartoli
* Charles Blavette : Sosthène, a supporter of Corti
* Noël Rochiccioli : Colonna
* Juan Vilato : the partisan singer of Mr Lauriston
* Geneviève Galéa : Antonia Amoretti, the daughter
* Jacqueline Pierreux : the tourist, friend of captain
* Elisa Mainardi : daughter of Bastia
* Mario Carotenuto : Corti, a candidate for the town hall
* Jacqueline Doyen : (uncredited)
* Tintin pasqualini : (uncredited)

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 


 