Life Is a Dream (1917 film)
{{Infobox film
| name           = Life Is a Dream 
| image          = 
| image_size     = 
| caption        = 
| director       = Robert Wiene
| producer       = Oskar Messter
| writer         = Richard Wurmfeld   Robert Wiene   
| narrator       = 
| starring       = Emil Jannings   Bruno Decarli   Maria Fein
| music          =  
| editing        = 
| cinematography = 
| studio         = Messter Film
| distributor    = Hansa Film
| released       =  
| runtime        = 
| country        = German Empire Silent  German intertitles
| budget         = 
| gross          = 
}} German silent silent drama film directed by Robert Wiene and starring Emil Jannings, Bruno Decarli and Maria Fein. A young aristocrat meets a man and marries him, but soon discovers he is a monster. After his death she grows increasingly mad, until a revolutionary new cure is attempted which makes her believe that the whole episode was simply a dream. 

==Cast==
* Emil Jannings   
* Bruno Decarli   
* Maria Fein   
* Alexander Antalffy   
* Emil Rameau

==References==
 

==Bibliography==
* Jung, Uli & Schatzberg, Walter. Beyond Caligari: The Films of Robert Wiene. Berghahn Books, 1999.

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 