Night of the Juggler
{{Infobox film
| name           = Night of the Juggler
| image          =Night of the Juggler.jpg
| image_size     =
| caption        = Robert Butler
| producer       = Jay Weston
| writer         = William P. McGivern (novel) Rick Natkin William W. Norton (as Bill Norton Sr.)
| narrator       = Linda Miller Cliff Gorman Barton Heyman Mandy Patinkin Dan Hedaya Sully Boyar
| music          = Artie Kane
| cinematography = Victor J. Kemper
| editing        = Argyle Nelson Jr.
| studio         = Columbia Pictures Corporation GCC Productions
| distributor    = Columbia Pictures
| released       = June 6, 1980
| runtime        = 101 min.
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
}}

Night of the Juggler is a 1980 action-thriller film, starring James Brolin.

==Plot summary==

A former cop, played by James Brolin, aligns with a street smart young clerk from the New York City dog pound (Julie Carmen) on a harrowing search for his daughter who is kidnapped by a psychopath (Cliff Gorman) after being mistaken for a wealthy mans daughter. His search is met with obstacles as he runs afoul of the police in his pursuit, including a former corrupt colleague bent on revenge against him. Meanwhile, the kidnapper is just as prepared to kill anybody, including his young hostage, unless his colossal ransom demands are met.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 


 