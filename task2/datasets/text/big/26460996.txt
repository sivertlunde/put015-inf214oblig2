Blonde Dynamite
{{Infobox film
| name           = Blonde Dynamite
| image          = Blonde Dynamite.jpg
| image_size     =
| caption        = Theatrical poster to Blonde Dynamite
| director       = William Beaudine
| producer       = Jan Grippo
| writer         = Charles Marion
| narrator       =
| starring       = Leo Gorcey Huntz Hall Gabriel Dell David Gorcey William Benedict
| music          = Edward J. Kay
| cinematography = Marcel LePicard William Austin
| distributor    = Monogram Pictures
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         =
}}
Blonde Dynamite is a 1950 comedy film starring The Bowery Boys. The film was released on February 12, 1950 by Monogram Pictures and is the seventeenth film in the series.

==Production==
Beginning with this film, Buddy Gorman temporarily takes over for the role of Butch in the absence of Bennie Bartlett.

==Plot==
Slip suggests that Louie take a vacation and hand over the responsibilities of the sweet shop to the boys.  Louie reluctantly agrees and takes his wife to Coney Island.  The boys decide to turn his sweet shop into an escort service and give the place a makeover.  Meanwhile Gabe, who works as a messenger for a bank, has gotten himself into trouble.  He had $5,000 of the banks money stolen from him and local gangsters threaten to frame him for the theft unless he agrees to get the bank vault combination for them.  He gives them the combination and the gangsters take over the sweet shop from Sach, who is minding it while Slip and the rest of the boys are out serving as escorts.  Their goal is to dig from the sweet shop to the bank, and Sach allows them to after they convince him that they are government men looking for uranium.  Eventually Slip, Louie, and the rest of the boys wind up back at the sweet shop and are pressed into service.  Gabe goes to the police to tell the whole story, and as soon as he does, the gangsters dig through the police station floor (which is situated behind the bank) and are captured.  Louie is temporarily pleased as Sach did find uranium under his store, but in the end it is discovered that he only owns the land...not the mineral rights!  He faints and a doctor tells him all he needs is a vacation to feel better!

==Cast==

===The Bowery Boys===
*Leo Gorcey as Terrance Aloysius Slip Mahoney
*Huntz Hall as Horace Debussy Sach Jones
*William Benedict as Whitmore Whitey
*David Gorcey as Cedric Chuck
*Buddy Gorman as Bartholemew Butch

===Remaining cast===
*Gabriel Dell as Gabe Moreno
*Bernard Gorcey as Louie Dumbrowski
*Adele Jergens as Joan Marshall Harry Lewis as Champ
*Stanley Andrews as Mr. Jennings

==Home media== Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume One" on November 23, 2012.

==References==
 

==External links==
* 

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958
| before=Master Minds 1949
| after=Lucky Losers 1950}}
 

 
 

 
 
 
 
 
 
 
 
 