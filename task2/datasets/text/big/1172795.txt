The Way of All Flesh (film)
{{Infobox film
| name           = The Way of All Flesh
| image          = The-way-of-all-flesh-1927.jpg
| caption        = Film poster
| director       = Victor Fleming
| producer       = Adolph Zukor Jesse L. Lasky
| writer         = Perley Poore Sheehan (original story) Lajos Biro (adaptation) Frederica Sagor (adaptation) Jules Furthman (scenario)
| starring       = Emil Jannings
| cinematography = Victor Milner
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 9 reel#Motion picture terminology|reels; 8,486 feet
| country        = United States Silent English intertitles
}} Samuel Butlers novel The Way of All Flesh, and is now considered a lost film. 

==Plot== securities to Chicago, he meets a blond seductress on the train, who sees what he is carrying. She flirts with him, convinces him to buy her a bottle of champagne, and takes him to a saloon run by a crook. The next morning he awakes alone in a dilapidated bedroom, without the securities. He finds the woman, and at first pleads with her, then intimidates her to return the stolen securities. He is knocked unconscious by the saloon owner and dragged to a nearby railroad track.

As the crook strips him of everything that might lead to his identification, Schiller recovers consciousness, and in a struggle the crook is thrown into the path of an oncoming train and killed. Schiller flees, and in despair is about to take is own life, when he sees in a newspaper that he is supposedly dead, the crooks mangled body having been identified as Schillers. The time passes to twenty years later. Schiller is aged and unkempt, employed to pick up trash in a park. He sees his own family go to a cemetery and place a wreath on his grave. Following other scenes in a Christmas snowstorm, Schiller makes his way to his former home, where he sees that the son whom he had taught to play violin is now a successful musician. He walks away, carrying in his pocket a dollar that his son has given him, not recognizing that the old tramp is his father.

==Cast==
*Emil Jannings - August Schilling
*Belle Bennett - Mrs. Schilling
*Phyllis Haver - The Temptress Donald Keith - August, junior
*Fred Kohler - The Tough
*Philippe De Lacy - August, as a child
*Mickey McBan - Evald
*Betsy Ann Hisle - Charlotte
*Carmencita Johnson - Elizabeth
*Gordon Thorpe - Karl
*Jackie Combs - Heinrich
*Dean Harrell -
*Anne Sheridan - (*not to be confused with Ann Sheridan)
*Nancy Drexel -
*Dorothy Kitchen -
*Philip Sleeman - ?

==Production== Academy Award The Last Oscars were awarded for multiple performances).

==Preservation status==
Only two fragments survive, both from the ending, making Jannings the only Academy Award-winning performance with no known complete copy of the film preserved. This is one of Victor Flemings many lost silent films of the 1920s.

==Disputed provenance==
In her 1999 autobiography, Frederica Maas claimed that the idea for the movie had been stolen from her and her husband, Ernest Maas.  She said the story was based on the life of her husbands father, who had similarly abandoned his family after making horrible mistakes in his personal life.  They named the script "Beefsteak Joe" and presented it to Emil Jannings.  As a fellow German-American, Ernest had thought Jannings would help them get the script made into a movie.  Instead they were shocked to learn that it had been stolen and produced without any credit or remuneration.  The fact that it became an award-winning film only aggravated the situation. 

==Remake== William Henry. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 