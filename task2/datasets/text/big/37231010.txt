Patrick: Evil Awakens
 
 
{{Infobox film
| name           = Patrick: Evil Awakens
| image          = File:Patrick Evil Awakens 2013 movie poster.jpg
| caption        = Theatrical film poster
| director       = Mark Hartley
| producer       = Antony I. Ginnane
| writer         = Justin King
| starring       = Rachel Griffiths Sharni Vinson Charles Dance
| music          = Pino Donaggio
| cinematography = Garry Richards
| editing        = Jane Moran
| studio         = 
| released       =  
| distributor    = Umbrella Entertainment.
| budget         = 
| runtime        = 96 minutes
| country        = Australia
| language       = English
}} 1978 film of the same name.     It had its world premiere on 27 July 2013 at the Melbourne International Film Festival and received a limited theatrical release on 14 March 2014, followed by a DVD release the following month. Its Canadian theatrical premiere was at the Lost Episode Festival Toronto on 5 July 2014.

The movie stars Jackson Gallagher as the titular Patrick, a comatose young man that uses his psychic powers to stalk a nurse caring for him.

==Synopsis==
Kathy (Sharni Vinson) is a young nurse that is eager to prove herself in her new job in an isolated psychiatric clinic. Shes intrigued by Patrick (Jackson Gallagher), a comatose patient that her boss Dr. Roget (Charles Dance) assures her is incapable of truly responding to any external stimuli. Kathy is horrified by the experiments that Roget and his nurse Matron Cassidy (Rachel Griffiths) inflict upon him, and shes initially pleased when she finds a way to communicate with him. This quickly turns to horror when Patrick uses his psychic abilities to interfere with her life outside of the hospital, as Patrick has grown obsessed with Kathy and will harm anyone that he deems to be interfering with his relationship with her.

==Cast==
*Sharni Vinson as Kathy Jacquar
*Rachel Griffiths as Matron Cassidy
*Charles Dance as Doctor Roget
*Peta Sergeant as Nurse Williams
*Eliza Taylor as Nurse Panicale
*Martin Crewes as Brian Wright
*Damon Gameau as Ed Penhaligon
*Jackson Gallagher as Patrick
*Rod Mullinar as Morris
*Simone Buchanan as Patricks Mother

==Production==
Richard E. Grant was originally cast as the doctor but had to drop out because of a scheduling conflict. 

==Reception==
Critical reception for Patrick: Evil Awakens has been predominantly positive and the film holds a rating of 83% on Rotten Tomatoes, based on 18 reviews.  The Hollywood Reporter rated it favorably, summing it up with the tagline "This Ozploitation remake is a spookily effective fright-fest."  The Guardian gave a predominantly favorable but mixed review, praising the casts acting overall while noting that the film erred in overdoing the films shocks and doing them too early. 

The film is rated R16 in New Zealand for horror, violence, sex scenes, and offensive language.

==References==
 

==External links==
*  

 

 
 
 


 