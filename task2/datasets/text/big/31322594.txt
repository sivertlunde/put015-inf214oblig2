52 Weeks Make A Year
{{Infobox film
| name           = 52 Wochen sind ein Jahr
| image          = 
| image size     =
| caption        =
| director       = Richard Groschopp
| producer       =Hans-Joachim Schoeppe
| writer         = Richard Groschopp Jurij Brezan 
| starring       = Hans Wehrl
| music          = Hans Hendrik Wehding
| cinematography = Joachim Hasler, Erich Gusko
| editing        =Waltraut von Zehmen-Heinicke  studio = DEFA
| distributor    = PROGRESS-Film Verleih
| released       =  
| runtime        =88 minutes
| country        = East Germany
| language       = German
| budget         =
| gross          =
}}

52 Weeks Make A Year (  film directed by Richard Groschopp. It was released in List of East German films|1955.

==Plot== Sorbian village collectivize all the farms, Krestan is reluctant to hand over his property, and his neighbors share his sentiments. But when they realize the great advantages of collective ownership, they happily join in.

==Cast==
* Hans Wehrl as Krestan Serbin
* Lotte Loebinger as Serbinowa
* Irene Korb as Lena
* Kurt Oligmüller as Peter
* Erich Franz as Gessner
* Fritz Schlegel as Ladusch
* Lore Frisch as Sonja
* Fabian Wander as Herbert
* Johannes Arpe as Müller
* Dorothea Thiesing as Marta
* Aribert Grimmer as Baumann
* Hans Joachim Schölermann as Professor
* William Gade as Kubank
* Heinz Kammer as Rinke

==Production==
The film was adapted from Jurij Brězans novel by the same name, published in 1953. 52 Weeks Make One Year was the first East German film about the collectivization of agriculture in the country. 

==Reception== Homeland films, 52 Weeks "is not entirely irreconcilable with the belief in progress" and is an example to the East German agrarian pictures that focused on the benefits of the collectivization.  The Catholic Film Service called it "formalistic, but with good acting." 

==References==
 

==External links==
* 
*  on ostfilm.de.

 
 
 
 