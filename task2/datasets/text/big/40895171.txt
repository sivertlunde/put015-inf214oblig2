Raqt
 
 
{{Infobox film
| name           = Raqt
| image          = 
| caption        = DVD Cover
| director       = Adi Irani Shiva Rindan
| producer       = Amit Mishra Ashish Mishra
| writer         = Adi Irani Shiva Rindan  Ranjiv Verma
| starring       = Gulshan Grover Shweta Bhardwaj Adi Irani Farida Jalal Arjun Mahajan Sheena Shahabadi
| music          = Daboo Malik
| cinematography = Nirmal Jani
| editing        = pritam
| studio         =
| distributor    = Multimedia Combines 
| released       =  
| runtime        = 127 Min
| country        = India
| language       = Hindi
| budget         =
| gross          = 
}}

Raqt is a Hindi feature Film released on 27 September 2013,Produced by Ashish Mishra & Amit Mishra,Directed by Adi Irani & Shiva Rindan.  

==Plot==
The film revolves around Sonia (played by Sweta Bharadwaj) and Suhani (played by Sheena Shahabadi). The film is about the fatal obsession of an adopted daughter for her mother’s affection.

Sonia is a single mother who adopts her sister’s daughter when her sister and brother-in-law die in an accident due to her carelessness. Sonia, along with her maid Maria (Farida Jalal), pampers Suhani to not let her miss her parents.
 But this has an opposite effect with Suhani, with her developing an obsessive relationship with Sonia. She will let nothing come between her and her mother.

Unaware of her child’s problem, Sonia becomes a career-woman not focusing on her personal life beyond Suhani. She becomes a successful heroine. One day, Suhani sees Sonia and a friend arguing. She kills him, and Sonia takes the blame and is jailed for seven years.

Performance wise, the film belongs entirely to Sheena Shahabadi. Yes, Sheena is the same actress who debuted with Satish Kaushik’s long forgotten Terre Sang (2009), which discusses teen pregnancy.

In Raqt, Sheena comes with a different avatar and fits the role well. Sweta Bharadwaj is good along with Farida Jalal and the other cast. Gulshan Grover plays a psychiatrist and he does full justice to his part.

The film is directed by Adi Irani and Shiva Ridnani who are actor-turned-directors, serving Bollywood with some good and remembered performances in the past.

This time they have decided to make a film which is both honest and decent. The film offers lots of twist and turns in the second half, especially when Sonia is imprisoned.

The songs in the film however look out of place, but the background score gels well with the film. The cinematography and production values of the film are good despite a restrained budget. The editing is little weak causing the film to drag at a few points.

On a frank note, as I mentioned earlier, such good films fail to create an impact and that is only because of the lack of valuable promotions. Raqt is a good thriller, which strongly deserves publicity through word of mouth.
 
==Cast==
* Gulshan Grover,
* Shakti Kapoor, 
* Shweta Bhardwaj, 
* Sheena Shahabadi, 
* Farida Jalal, 
* Suhani Tak, 
* Suresh Menon

==References==
 

 
 
 


 