Jimmy Neutron: Boy Genius
 
 
{{Infobox film
| name           = Jimmy Neutron: Boy Genius 
| image          = Jimmy Neutron Boy Genius film.jpg
| caption        = Theatrical release poster
| director       = John A. Davis
| producer       = Steve Oedekerk John A. Davis Albie Hecht
| screenplay     = John A. Davis David N. Weiss J. David Stem Steve Oedekerk
| story          = John A. Davis Steve Oedekerk
| starring       = Debi Derryberry Patrick Stewart Martin Short Rob Paulsen Jeffrey Garcia Frank Welker Carolyn Lawrence Crystal Scales
| cinematography = Steve Kolbe Chris Sherrod
| editing        = Gregory Perler Jon Price
| music          = John Debney
| studio         = Nickelodeon Movies O Entertainment DNA Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 82 minutes 
| country        = United States
| language       = English
| budget         = $30 million
| gross          = $102,992,536 
}}
Jimmy Neutron: Boy Genius is a 2001   and LightWave 6) by DNA Productions. The film was released December 21, 2001.   

It was nominated for the first Academy Award for Best Animated Feature, but lost to Shrek. It was the only animated Nickelodeon film to ever be nominated in that category until Rango (2011 film)|Rango was nominated in 2011 and won.

The success of the film led to a spin-off television series,  , which premiered on July 20, 2002 and ended on November 25, 2006. A second spin-off, Planet Sheen, premiered on October 2, 2010, but was cancelled on February 15, 2013. 

==Plot== Jimmy Neutron Carl Wheezer Goddard (Frank Welker). Jimmy is attempting to launch a communications satellite made out of a toaster, hoping to communicate with an alien species he believes exists somewhere out in the universe. Unfortunately, the pulse rockets fail just after Jimmy launches the probe, causing the rocket to crash land on his roof. He is reprimanded by his parents List of Jimmy Neutron characters#Hugh Neutron|Mr. Hugh Neutron (Mark DeCarlo) and List of Jimmy Neutron characters#Judy Neutron|Mrs. Judy Neutron (Megan Cavanagh), and misses the bus to school as a result. Later, Jimmy, Carl, and Sheen spot a poster for an amusement park called "Retroland." However, Judy Neutron isnt allowed to let Jimmy go because its a school night. 
 King Goobot Ooblar (Martin Short), watch a pre-recorded message from Jimmy, featuring him introducing himself and explaining about life on Earth. He sets a course for Earth and kidnaps all the parents in the city, leaving fake notes on the refrigerators to tell the kids theyve gone to Florida for an "extended vacation".
 Sheen (Jeffrey Nick (Candi Milo) then says that Jimmy will take them to the aliens, and hell take it from there. He organizes the other children in town to build spaceships from the Retroland rides to travel there and get their parents back.

They reach the planet Yolkus, home of the Yolkian race. Eventually they are captured by Goobot, who tells them the parents are to be sacrificed to their goddess, List of Jimmy Neutron characters#Poultra|Poultra. He shows the kids Jimmys video, thanking him for helping him find a suitable species for their ritual. After an unusual ceremony (in which the parents under mind control do the Chicken Dance), Poultra, a colossal three-eyed reptilian chicken, hatches from her egg. 

Everyone escapes with the adults in tow, but Goobot follows them in his ship at the head of the Yolkus fleet, while Poultra is not far behind. Jimmy reunites with his parents again, and they make it home.

==Cast==
  James Issac Neutron/Jimmy Neutron Girl Eating Plant/Oyster Carl Wheezer/List of Jimmy Neutron characters#Mr. and Mrs. Wheezer|Mr. and Mrs. Wheezer/Kid in Classroom Cindy Vortex Cindy Vortexs mom Sheen Estevez Libby Folfax Nick Dean/List of Jimmy Neutron characters#Britney Tenelli|Britney/PJ King Goobot V Ooblar
* David L. Lander as Yolkian Guard/Gus VOX
* Pilot II
* Carlos Alazraqui as List of Jimmy Neutron characters#Mr. Estevez|Sheens Dad Mission Control/General Bob
* Keith Alcorn as Bobby/Kid/Control Yokian
* Kimberly Brooks as Zachery/Reporter/Angie/List of Jimmy Neutron characters#Mrs. Folfax|Libbys Mom
* Andrea Martin as List of Jimmy Neutron characters#Miss Winfred Fowl|Ms. Winfred Fowl Billy West as Bobbys Twin Brother/Butch/Jailbreak Cop/Old Man Johnson/Robobarber/Flurp Announcer/Yokian Officer/Anchor Boy/Guard
* Bob Goen and Mary Hart as Yokian newscasters
Additional voices were provided by: Jack Angel, Jeannie Elias, Bill Farmer, Philip Proctor, Rodger Bumpass, Mary Kay Bergman, Toran Caudell, Erik von Detten, Mickie McGowan, Mona Marshall, Hynden Walch, Bill Striglos and Gregg Berger.

==Reception==
Jimmy Neutron: Boy Genius received generally positive reviews from critics and audiences. The film currently holds a 75% "Certified Fresh" rating on  , the film also holds a score of 65/100, indicating "generally favorable reviews". 

Rita Kempley of Washington Post praised the film, saying that "this little charmer both celebrates and kids the corny conventions of family sitcoms".

Nell Minow of Common Sense Media enjoyed the "stylish 3-D computer animation, good characters", giving the film 3 out of 5 stars. 

Owen Gleiberman of Entertainment Weekly gave this film a B+, calling it "
a lickety-split, madly packed, roller-coaster entertainment that might almost have been designed to make you scared of how much smarter your kids are than you". 

Paul Tatara of CNN.com called this film "the most delightfully original childrens film of 2001". 

Roger Ebert of Chicago Sun-Times gave this film a 3/4 score, saying that "it doesnt have the little in-jokes that make Shrek and Monsters, Inc. fun for grown-ups. But adults who appreciate the art of animation may enjoy the look of the picture". 

===Box office===
The film was financially successful, bringing in $13,833,228 on its opening weekend for an average of $4,407 from 3,139 theaters, and ended up with a total of $80,936,232 domestically, and the film did better overseas bringing in $22,056,304 which made a total of $102,992,536 worldwide. It had a budget of roughly $30 million.  It is one of only twelve feature films to be released in over 3,000 theaters and still improve on its box office performance in its second weekend, increasing 8.7% from $13,832,786 to $15,035,649. 

===Awards===
Jimmy Neutron: Boy Genius was nominated for the first Academy Award for Best Animated Feature, but lost to Shrek (film)|Shrek, released by DreamWorks. It was the first release from Nickelodeon Movies to receive an Academy Award nomination.

==Home media==
Jimmy Neutron: Boy Genius was released on VHS and DVD on July 2, 2002. It was re-released by Warner Home Video on DVD on January 1, 2013. A Blu-ray release has not been announced yet.

==Soundtrack==
===Official Edit=== 2001 after the movie was released, by Jive Records and Nick Records.    
{{tracklist
| extra_column = Artist Leave It Up to Me
| extra1   = Aaron Carter
| length1  = 2:59 Pop
| note2    = Deep Dish Cha-Ching Remix
| extra2   = *NSYNC
| length2  = 4:13 Parents Just Dont Understand
| extra3   = Lil Romeo, 3LW, and Nick Cannon
| length3  = 3:55 Intimidated 
| extra4   = Britney Spears
| length4  = 3:17 He Blinded Me with Science The Matrix
| length5  = 3:15
| title6   = A.C.s Alien Nation 
| extra6   = Aaron Carter
| length6  = 3:23 Kids in America No Secrets
| length7  = 3:06 The Answer to Our Life
| extra8   = Backstreet Boys
| length8  = 3:17
| title9   = The Chicken Dance
| extra9   = Stupid
| length9  = 1:32
| title10  = I Can Count on You
| extra10  = True Vibe
| length10 = 3:46
| title11  = We Got the Beat
| extra11  = The Go-Gos
| length11 = 2:31
| title12  = Go Jimmy Jimmy
| extra12  = Aaron Carter
| length12 = 2:38
| title13  = Parents Just Dont Understand (Bonux Mix)
| extra13  = Lil Romeo, 3LW, and Nick Cannon
| length13 = 
| title14  = Blitzkrieg Bop
| extra14  = The Ramones
| length14 = 2:12
| title15  = Jimmy Neutron Theme 
| extra15  = Bowling for Soup
| length15 = 2:08
}}
===Original Score===
Additionally, a promotional CD containing the score by John Debney was released for Academy Award consideration.
{{tracklist
| extra_column = Artist
| total_length = 82:58
| title1  = Jimmy Neutron Theme 
| extra1  = Bowling for Soup
| length1 = 2:08 Leave It Up to Me
| extra2   = Aaron Carter
| length2  = 2:59 Pop
| note3    = Deep Dish Cha-Ching Remix
| extra3   = *NSYNC
| length3  = 4:13 Parents Just Dont Understand
| extra4   = Lil Romeo, 3LW, and Nick Cannon
| length4  = 3:55 Intimidated 
| extra5   = Britney Spears
| length5  = 3:17 He Blinded Me with Science The Matrix
| length6  = 3:15
| title7   = A.C.s Alien Nation 
| extra7   = Aaron Carter
| length7  = 3:23 Kids in America No Secrets
| length8  = 3:06 The Answer to Our Life
| extra9   = Backstreet Boys
| length9  = 3:17
| title10   = The Chicken Dance
| extra10   = Stupid
| length10  = 1:32
| title11  = I Can Count on You
| extra11  = True Vibe
| length11 = 3:46
| title12  = We Got the Beat
| extra12  = The Go-Gos
| length12 = 2:31
| title13  = Go Jimmy Jimmy
| extra13  = Aaron Carter
| length13 = 2:38
| title14  = Parents Just Dont Understand (Bonux Mix)
| extra14  = Lil Romeo, 3LW, and Nick Cannon
| length14 = 
| title15  = Blitzkrieg Bop
| extra15  = The Ramones
| length15 = 2:12
| title16   = Nickelodeon Logo
| length16   = 0:14
| title17   = Air Force
| length17   = 1:00
| title18   = Jimmys Rocket Machine
| length18   = 1:20
| title19   = Parents
| length19   = 1:17
| title20   = Ready-To-Go-To-School Machine
| length20   = 1:49
| title21   = The Plan (Part 1)
| length21   = 0:37
| title22   = The Plan (Part 2)
| length22   = 0:17
| title23   = Nick
| length23   = 0:50
| title24   = The Worm
| length24   = 0:20
| title25   = RetroLand Theme Park!
| length25   = 0:40
| title26   = Oyster & Diamond
| length26   = 0:34
| title27   = Alien Space Craft/Jimmys Message
| length27   = 3:02
| title28   = Options
| length28   = 0:49
| title29   = Sneak Out
| length29   = 1:09
| title30   = Invasion Alert
| length30   = 0:34
| title31   = RetroLand Main
| length31   = 0:14
| title32   = Good Night
| length32   = 0:58
| title33   = Alien Abduction
| length33   = 1:13
| title34   = The Wish
| length34   = 0:47
| title35   = Say Goodbye/Angry Mob & 75/Launch
| length35   = 7:07
| title36   = Beauty Of Space/Meteor
| length36   = 2:25
| title37   = The Alien Planet
| length37   = 1:12
| title38   = Flying Jimmy
| length38   = 0:50
| title39   = King Goobots Shock
| length39   = 0:20
| title40   = Poultra: God Of Wrath (Part 1)
| length40   = 0:10
| title41   = Poultra: God Of Wrath (Part 2)
| length41   = 0:20
| title42   = Prisoners
| length42   = 1:10
| title43   = Cindy & Jimmy
| length43   = 1:34
| title44   = Ooblars Danger/Cell Dog Phone/Rescue
| length44   = 3:09
| title45   = Stadium
| length45   = 0:23
| title46   = Bring On The Humans
| length46   = 0:47
| title47   = The Incubation
| length47   = 0:48
| title48   = Sacrifice
| length48   = 0:29
| title49   = The Plan
| length49   = 1:40
| title50   = Jimmy To The Rescue
| length50   = 2:02
| title51   = Escape From The Planet/The Big Chase
| length51   = 2:42
| title52  = Jimmy Is The Winner/Apologise
| length52   = 2:15
| title53   = The End
| length53   = 0:13
| title54   = End Credits
| length54   = 7:27
}}

==Genius, Sheenius or Inbetweenius==
An event that aired on May 25, 2007, Nickelodeon rehired the original voice actors of Jimmy, Sheen and Carl to return for a special audio commentary version of the film that features their animated counterparts silhouettes, spoofing Mystery Science Theater 3000.

==Film promotion==
These shorts were used to promote the film. They have all been released on the official Jimmy Neutron: Boy Genius DVD release of the film. All of the inventions in each short was seen again at some point in the TV series (except for the Pain-Transference helmet). Clips from similar versions of these shorts, along with clips from the unaired "Runaway Rocketboy" pilot appeared in the teaser trailer for Jimmy Neutron: Boy Genius. The biggest difference between the clips seen in the trailer and the original shorts is that Jimmy wears the white and red striped shirt he wore in the pilot, rather than his trademark shirt.

{| class="wikitable"
|- style="background:#EFEFEF"
!  Short
!  Overview
|-
| Carl Squared
| Carl asks Jimmy lots of questions when he clones himself. The cloning machine is seen again in "Send in the Clones"and "The Trouble with Clones."
|-
| Calling All Aliens
| Jimmy receives a message, thinking that it is from aliens. But when he says "school goddard", he gets teleported to school. And tries several attempts to try to communicate with the aliens. Aliens are also mentioned in Jimmy Neutron: Boy Genius (Movie)
|-
| Cookie Time
| Jimmy has a remote control that controls time. He asks his mom for a cookie and gets it. He keeps rewinding but Goddard bites the remote and sends them back to the prehistoric era. The remote is seen again in "Sorry, Wrong Era".
|-
| Hyper Corn
| Jimmy invents his Hyper Cube, to store infinite items in one small place. But, its dinner time and they are having creamed corn, which Jimmy hides in his Hyper Cube. After Jimmys dad thinks its a brain teaser puzzle and breaks it, Jimmy finds out he likes it after all. The Hyper Cube makes appearances in the episodes "Hypno Birthday to You" and "Holly Jolly Jimmy", although it looks different from in the original short.
|-
| New Dog, Old Tricks
| Jimmy introduces his robotic dog, Goddard to Cindy and her dog, Humphrey who Cindy says is the best dog in Retroville. However after showing each other new tricks, Goddard wins the argument. Humphrey makes brief cameos in other episodes. NOTE: A clip from a slightly different version of this short appeared in the original theatrical trailer for Jimmy Neutron: Boy Genius. This version ends with Goddard putting himself back together after Jimmy says, "I can fix that".
|-
| Pain Pain Go Away
| Jimmy visits the dentist, using his Pain-Transference Helmet to transfer the pain to Cindy. He gets in trouble, though, when Cindy snatches the helmet the next day and retaliates by injuring herself and sending the pain to Jimmy.
|-
| Sea Minus
| Jimmy accidentally uses his Matter Transporter to move the Neutrons House underwater! The Matter Transporter is seen again in "My Son, the Hamster".
|-
| Ultralord vs. The Squirrels Sheen gets his new Ultralord Action Figure in a tree, Jimmy must get it back with his Hypno Ray invention to keep it away from the squirrels. The Hypno Ray is seen again in "Hypno Birthday To You". NOTE: A clip from a slightly different version of this short appeared in the original theatrical trailer for Jimmy Neutron: Boy Genius. In the trailer version, the scenes take place in the park, rather than in Jimmys backyard, and Sheen is replaced by Nick Dean.
|}

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 