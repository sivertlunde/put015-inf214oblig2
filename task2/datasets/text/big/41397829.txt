Aham Premasmi
{{Infobox film
| name = Aham Premasmi
| image =
| caption = 
| director = V. Ravichandran
| writer = V. Ravichandran
| producer = V. Ravichandran
| starring = Balaji, Aarti Chhabria
| music = V. Ravichandran
| cinematography = G S V Seetharam
| editing = V. Ravichandran
| studio  = Eeshwari Entertainments
| released =  May 27, 2005
| runtime = 150 min
| language = Kannada  
| country = India
}}

Aham Premasmi ( ) is a 2005 Indian Kannada-language romantic action film starring newcomers, Balaji and Aarti Chhabria in the lead roles. Besides acting, V. Ravichandran has directed, written, produced, edited and composed music for the film. 

==Cast==
*Balaji as Eeshwar
*Aarti Chhabria as Apsara
*V. Ravichandran  Sharan
*Ganesh Ganesh
*Ramakrishna Ramakrishna
*Chitra Shenoy
*Bullet Prakash
*Bank Janardhan
*Shankar Ashwath
*Sarigama Viji

==Release==
The film was released on May 27, 2005 all over Karnataka. The film met moderate and average response at the box-office.

==Soundtrack==
All the songs are composed, written and scored by V. Ravichandran. 

{|class="wikitable"
! Sl No !! Song Title !! Singer(s) 
|-
| 1 || "Yadhaa Yadaahi" || L. N. Shastry 
|- Anupama 
|-
| 3 || "Kannalley" || Sonu Nigam, Sunidhi Chauhan 
|-
| 4 || "Eeshwar" || S. P. Balasubramanyam, L. N. Shastry, Raju Ananthaswamy
|-
| 5 || "Condition" || Suresh Peters, Sunidhi Chauhan 
|- Anupama 
|-
| 7 || "O Preethiye" || S. P. Balasubramanyam, Suma. S
|-
| 8 || "O Premave Na" || L. N. Shastry, Suma. S
|-
| 9 || "Praya Praya" || Rajesh Krishnan, Anuradha Sriram
|-
|}

==Awards== Udaya Film Award for best Supporting actor - V. Ravichandran
* Karnataka State Film Award for Best Art Direction - Ismail, Shivakumar

==References==
 

== External links ==
* 
* 
* 
* 

 

 
 
 
 
 


 