Encounter (2013 film)
{{Infobox film
| name           = Encounter
| image          = Poster of Encounter 2013.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Suman Mukhopadhyay
| producer       = Kamini Tewari
| writer         = Suman Mukhopadhyay
| starring = {{Plain list|
*Dheeraj Pandit
*Koel Das
*Ferdous Ahmed Rajesh Sharma
*Biswajit Chakraborty}}
| music          = Shanku Mitra
| cinematography = Suman Dutta
| editing        = Atanu Ghose
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}} action Thriller thriller Cinema Bengali film directed by Suman Mukhopadhyay and produced by Kamini Tewari. The film features newcomers Dhiraj and Koel Das in the lead roles. Music of the film has been composed by Sanku Mitra.   

== Plot ==
The story of the film revolves around the affinity between administration, police and underworld. It depicts that how our administrators make use of the underworld to fulfill their own wishes. The film is about four people who later become involved with the underworld. 

== Cast ==
* Dheeraj Pandit
* Koel Das
* Ferdous Ahmed Rajesh Sharma
* Shantilal Mukherjee
* Pallabi Chattopadhyay
* Biswajit Chakraborty
* Rita Koyral
* Soumen Mahapatra

== Soundtrack ==
{{Infobox album  
| Name       = Encounter
| Type       = Soundtrack
| Artist     = Shanku Mitra
| Cover      = 
| Alt        = 
| Released   =  
| Recorded   =  Feature film soundtrack
| Length     =  
| Label      = 
| Producer   = 
| Last album = Shudhu Tomar Jonyo (2007)
| This album = Encounter (2013)
| Next album = 
}}
Shanku Mitra composed the music for Encounter. Lyrics are penned by Goutam Sushmit. The music launch was held at The Park on September 2, 2013. The soundtrack consists of six tracks. The playback singers include Usha Uthup, Kumar Sanu, Rima, Mohammed Aziz, Archan, Sujoy Bhowmick, Raghab Chatterjee and Sanchita Bhattyacharya.   

== References ==
 

 
 
 
 


 
 