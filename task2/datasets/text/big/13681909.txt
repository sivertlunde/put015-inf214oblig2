N.U. (film)
{{Infobox film
| name           = N.U.
| image          =
| image_size     =
| caption        =
| director       = Michelangelo Antonioni
| producer       = Vieri Bigazzi (production manager)
| music          = Giovanni Fusco (music consultant)
| cinematography = Giovanni Ventimiglia
| distributor    = Lux Film (presented by) Icet, Milan
| released       = 1948
| runtime        = 11 min
| country        = Italy Italian
}}

N.U. (short for Nettezza urbana, Italian municipal cleansing service) is a short 1948 documentary film directed by Michelangelo Antonioni. The film examines a weekday morning of Italian janitors, captured at work on the streets of post-WWII Rome.
 neorealistic in its style, this documentary does foreshadow some of Antonionis own future trademarks, such as slow-moving camera that is enamoured with alienated and deserted urban landscapes and inanimate objects of architecture, nature, etc. and seemingly less than concerned with sweepers, washers and cleaners (almost indistinguishable from bums, and sometimes behaving much in the same way). The films narration consists of barely several phrases; musically soundtrack alternates classical-like piano improvisations with jazzy pieces.

In 1948 N.U. won Antonioni Nastro dArgento Best Documentary award.  So did his Lamorosa menzogna next year.

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 


 