Too Hot to Handle (1938 film)
{{Infobox film
| name           = Too Hot to Handle
| image          = Too Hot to Handle (1938).jpg
| image_size    = 225px
| caption        = Theatrical release poster Jack Conway
| producer       = Lawrence Weingarten
| writer         = Laurence Stallings John Lee Mahin Buster Keaton (uncredited)
| narrator       =
| starring       = Clark Gable Myrna Loy Walter Pidgeon
| music          = Franz Waxman
| cinematography = Harold Rosson Clyde De Vinna (in Dutch Guiana)
| editing        = Frank Sullivan
| studio         =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = $1,564,000  . 
| gross          = $2,396,000 
}}

Too Hot to Handle is a 1938 film about a newsreel reporter, the woman he is attracted to, and his fierce competitor, played by Clark Gable, Myrna Loy, and Walter Pidgeon respectively. Many of the gags in this sequence were devised by an uncredited Buster Keaton.

==Plot==
Union Newsreel reporter Chris Hunter (Clark Gable) is sneakier and has fewer scruples than his rivals in Second Sino-Japanese War|war-torn China. When the Japanese do not oblige with a convenient aerial attack to film, Chris fakes one with a model airplane with his cameraman José Estanza (Leo Carrillo). 

Outraged when he finds out, Chriss main competitor, Atlas Newsreels Bill Dennis (Walter Pidgeon) decides to do the same, having his aviator friend Alma Harding (Myrna Loy) fly in "serum" for an imaginary cholera outbreak. Chris finds out and swoops in to film her landing. José, however, drives too close to the plane, causing it to crash and burst into flame. Chris rescues Alma, but when he starts to go back for the serum, she has to admit the truth.

Chris piles on lie after lie to romance Alma, even pretending to get fired by his boss, "Gabby" MacArthur (Walter Connolly), for burning the footage. Chris convinces her to work for Union. She reveals that she needs the money to mount a search for her brother Harry, lost in the Amazon jungle and given up for dead by everyone  else. They travel to New York (where Gabby is eagerly awaiting the landing footage Chris is secretly bringing). Bill follows to protect the woman he has loved for years from his unscrupulous competitor.

However, the whole charade is eventually revealed, discrediting Chris, Bill and Alma. Both reporters are fired, and people begin to question whether Almas brother is really missing. Chriss budding romance with Alma is quashed when she learns of his numerous lies. Ashamed, Chris and Bill hock their equipment and have José pretend to be a generous, kind-hearted South American plantation owner. He presents Alma with nearly $8000 and a compass supposedly from Harrys airplane. He tells her one of his workers brought it to him. In reality, Chris etched a fake serial number on it.
 voodoo and means them no good, Alma is convinced when the native produces Harrys watch. 

To protect Alma, Chris and José set out on their own with their guide in a canoe. As they near the village, the native escapes, though José shoots and wounds him. Chris spots an ill white man through his binoculars. José suspects the natives intend to sacrifice him that night, so, using their camera equipment, Chris makes the frightened natives believe he is a powerful magician or god. He and José tend to the unconscious man. Despite a tense moment when their former guide shows up and denounces them, Chris maintains a tenuous control of the situation. When he hears Alma flying by, he has the natives show the wreckage of Harrys plane. She and Bill land nearby. Chris disguises himself and his cameraman as witch doctors, and film Alma and Bill without their knowledge. The natives finally turn hostile.  Alma and Bill get Harry into their airplane, but when Chris and José try to board it (still in disguise), Bill hits Chris. The plane takes off, leaving Chris and José to paddle for their lives.

When Alma, Bill and Harry return to New York, they are welcomed by reporters. However, "Pearly" Todd (Henry Kolker), Bills annoyed boss, wants to know how Chris got footage of Harrys dramatic rescue and he did not. Realizing that Chris must have been the helpful witch doctor, Alma reconciles with Chris (in the midst of a dangerous police shootout).

==Cast==
*Clark Gable as Christopher "Chris" Hunter
*Myrna Loy as Alma Harding
*Walter Pidgeon as William O. "Bill" Dennis
* Walter Connolly as Arthur "Gabby" MacArthur
* Leo Carrillo as Joselito "José" Estanza
* Johnny Hines as Mr. Parsons
* Virginia Weidler as Hulda Harding
* Betty Ross Clarke as Mrs. Ruth Harding
* Henry Kolker as "Pearly" Todd
* Marjorie Main as Miss Kitty Wayne
* Gregory Gaye as Popoff
* Al Shean as "Gumpy" Gumpert
* Willie Fung as Willie
*Lillie Mui as "Tootsie"
* Patsy OConnor as Fake Hulda
==Box Office==
According to MGM records the film earned $1,627,000 in the US and Canada and $769,000 elsewhere resulting in a small loss of $31,000. 
==References==
 
== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 