Oru Raagam Pala Thaalam
{{Infobox film 
| name           = Oru Raagam Pala Thaalam
| image          = Oru Ragam Pala Thalam.jpg
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       = Srividya George Thomas
| writer         = Dr Pavithran
| screenplay     = Dr Pavithran Madhu Jayan Srividya Jagathy Sreekumar
| music          = M. S. Viswanathan
| cinematography = NA Thara
| editing        = G Venkittaraman
| studio         = TV Movies
| distributor    = TV Movies
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by M. Krishnan Nair (director)|M. Krishnan Nair and produced by Srividya and George Thomas. The film stars Madhu (actor)|Madhu, Jayan, Srividya and Jagathy Sreekumar in lead roles. The film had musical score by M. S. Viswanathan.    

==Cast==
  Madhu
*Jayan
*Srividya
*Jagathy Sreekumar
*Sankaradi
*Aranmula Ponnamma
*Balan K Nair
*P. K. Abraham Reena
*T. P. Madhavan
*Vazhoor Rajan
 

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Janikkumpol nammal || P Jayachandran || Sreekumaran Thampi || 
|-
| 2 || Kanakachilankachaarthum || Vani Jairam || Sreekumaran Thampi || 
|-
| 3 || Thedivanna vasanthame || P Jayachandran || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 