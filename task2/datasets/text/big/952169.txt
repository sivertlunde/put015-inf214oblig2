Izzy and Moe
 
{{Infobox film
| name = Izzy and Moe
| image= Izzy and Moe VideoCover.png
| writer = Steven Patrick Bell Robert Boris
| starring = Jackie Gleason Art Carney
| director = Jackie Cooper
| producer = CBS Television (USA)
| released = September 23, 1985
| runtime =
| language = English
| budget =
}}
Izzy and Moe is a 1985 Television movie|made-for-TV  crime/comedy film, starring Jackie Gleason and Art Carney. It is a fictional account of two actual Prohibition-era policemen, Izzy Einstein and Moe Smith, and their adventures in tracking down illegal bars and gangsters.

==Plot details==
During the Prohibition era of the 1920s, a gangster named John Vanderhoff, alias "The Dutchman", was killing off the competition and setting up his own speakeasys. To fight the  crime, the Prohibition Bureau needed to get some extra men. Izzy Einstein (Jackie Gleason) volunteers; he is desperate to have a steady paycheck to support his wife, mother-in-law and four daughters. Einstein wants to disprove his mother-in-laws claim that he is just a "bum".

When the agent in charge of the local Prohibition Unit office tells him there is no job, Einstein makes a speech: "This is America. And Im proud to be an American..." When the chief tells Einstein that he is "too old" for this kind of job, Izzy quickly suggests taking a partner.  Moe Smith (Art Carney) recently had his underground bar discovered by the police and is spending too much time alone and drinking. Einstein meets with Smith and asks for his help. The idea of a steady paycheck convinces him.

When they first try to raid one of Dutchs bars, they find the gangster has converted it into a "reading hall". Izzy and Moe decide to use different tactics. After spotting a local baseball team, they ask their boss for 10 more men. They all dress as baseball players, and tell the gatekeeper at the bar they want to celebrate a win. They gain entry and enjoy it - then whip out their badges and arrest everyone in the bar.

Soon, Izzy and Moe have their own division within the unit; they work alone as a pair, get various costumes, and offer no explanations of their tactics.  Izzy and Moe soon successfully raid underground bars all over the city, and arrest such high-profile people as the district attorney. After their boss threatens to fire them, Izzy and Moe meet the press, and Izzy gives his "This is America" speech.  They are reinstated.
 bourbon is being kept. Izzy and Moe evade the trap, but Moe gets shot in the arm, and one of the agents is killed. The agents death affects Moe, who again considers quitting, but he decides to stay, giving his own version of the "This is America" speech to reporters. Izzy and Moe then proceeded to rob the Dutchs big shipment of Bourbon that he is expecting, with help from Dallas, and also take the truck and its treasure to a police impound site.

The Dutchman finds out about Dallas tipping off the shipment, takes her hostage and tells Izzy and Moe to bring the bourbon for an exchange.  They go to his estate, dodging the bullets and capturing the corrupt police chief. When Moe confronts Dutch, the gangster is pointing a gun at Dallas head. Moe offers himself; with Dutch about to shoot him, a gun blast knocks the gun away. Izzy emerges from hiding with a rifle. Moe says, "How could you chance taking a shot like that?" Izzy says, "I just pretended he had an apple on your head."

==Cast==
  
*Jackie Gleason as Izzy Einstein
*Art Carney as Moe Smith
*Cynthia Harris as Dallas Carter
*Zohra Lampert as Esther Einstein
*Dick Latessa as Lieut. Murphy

==References==
 
 

==External links==
 
* 
 

 
 
 