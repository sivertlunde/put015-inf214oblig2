You Never Can Tell (1920 film)
{{Infobox film
| name           = You Never Can Tell 
| image          = You Never Can Tell (1920) - Ad 1.jpg
| image size     = 190px
| border         = 
| alt            = 
| caption        = Ad for film Chester M. Franklin
| producer       = Adolph Zukor
| writer         = Thomas J. Geraghty Helmer Walton Bergman
| screenplay     = 
| story          = Grace Lovell Bryan
| based on       = 
| narrator       = 
| starring       = Bebe Daniels Jack Mulhall
| music          = 
| cinematography = Victor L. Ackland
| editing        = 
| studio         = 
| distributor    = Realart Pictures
| released       =   
| runtime        = 50 mins.
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 Chester M. Franklin directed and Bebe Daniels starred in the film. The film is based on several short stories You Never Can Tell and Class by Grace Lovell Bryan. A surviving print of the film is housed in the Library of Congress.    

==Cast==
*Bebe Daniels - Rowena Patricia Jones
*Jack Mulhall - Prince
*Edward Martindel - William Vaughn
*Helen Dunbar - Mrs. Vaughn Harold Goodwin - Jimmy Flannery
*Neely Edwards - Mysterious Sport
*Leo White - Mr. Renan
*Milla Davenport - Mrs. Jones
*Graham Pettie - Wilberforce Jones
*Gertrude Short - Vera

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 


 
 