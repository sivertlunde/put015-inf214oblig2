Bravo My Life
 
 
{{Infobox film
| name = Bravo My Life
| image = Bravo My Life (2007 film) poster.jpg
| caption = Theatrical poster
| director = Park Yeong-hoon
| producer = Lee Jae-hyuk Na Won-kyoon
| writer = Park Yeoung-hoon Lee So-yeon
| music = Choi Man-shik
| cinematography = Kim Jong-yoon
| editing =  Kyung Min-ho
| distributor = Showbox
| released =  
| runtime = 120 minutes
| country = South Korea
| language = Korean
| budget =
}} 2007 South Korean film directed by Park Yeong-hoon.

It is a  , a 1988 Japanese film written and directed by Jun Ichikawa. 

==Plot==
"Chief" Cho Min-hyuk (Baek Yoon-sik) is a middle-aged corporate manager nearing his retirement after working for 30 years in the same company. Cho and other colleagues in the office secretly have held dreams of being rock musicians but they suppressed those ambitions in order to get stable office jobs and support their families. During the film Cho and others resume their interest in playing musical instruments and form a band together. However Cho struggles to deal with office politics that result from his upcoming retirement as well as the anxiety for his and his familys future post-retirement.

==Cast==
* Baek Yoon-sik as Cho Min-hyuk Lee So-yeon as Kim Yoo-ri
* Park Jun-gyu as Son Seung-jae
* Im Ha-ryong as Choi Seok-won

==References==
 

==External links==
*  
*  

 
 
 
 


 