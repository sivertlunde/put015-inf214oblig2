Hot Stuff (1971 film)
Hot Stuff is a 1971 animated short directed and animated by Zlatko Grgic and written by Don Arioli. Produced by the National Film Board of Canada for the Dominion Fire Commission, a department of Public Works Canada, the nine minute short on fire safety offers a humorous look at the origins, benefits and dangers of fire.    The film garnered seven international awards, including Best Educational Film Award at the World Festival of Animated Films in Croatia and a Canadian Film Award for Arioli for best non-feature screenplay.      

==Production==
Grgic was recruited by for the NFB by producers  , who was himself an animator, ad-libbed voices for two of the characters, a snake and a cat. Arioli had been annoyed with Budner’s banter, but Koenig insisted on retaining these asides. Grgic was also given freedom to improvise by the producers.   

==Release==
Hot Stuff was one of seven NFB animated shorts acquired by the American Broadcasting Company, marking the first time NFB films had been sold to a major American television network. It aired on ABC in the fall of 1971 as part of the children’s television show Curiosity Shop, executive produced by Chuck Jones.   

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 


 
 