Namma Makkalu
{{Infobox film 
| name           = Namma Makkalu
| image          =  
| caption        = 
| director       = R. Nagendra Rao
| producer       = Harini
| writer         = V. D. Gopalakrishna
| screenplay     = 
| starring       = K. S. Ashwath Pandari Bai Chandrashekar Amarnath
| music          = Vijaya Bhaskar
| cinematography = V Manohar
| editing        = P S Murthy
| studio         = Vijaya Bharathi
| distributor    = Vijaya Bharathi
| released       =  
| runtime        = 122 min
| country        = India Kannada
}}
 1969 Cinema Indian Kannada Kannada film, directed by R. Nagendra Rao and produced by Harini. The film stars K. S. Ashwath, Pandari Bai, Chandrashekar and Amarnath in lead roles. The film had musical score by Vijaya Bhaskar.  

==Cast==
 
*K. S. Ashwath
*Pandari Bai
*Chandrashekar
*Amarnath
*Rohini Balakrishna in Guest Appearance
*Adavani Lakshmidevi Kalpana in Guest Appearance
*Nagaraj
*Rajaram
*Saroja
*Sarvamangala
*R. Nagendra Rao
*Ramadevi
*Vadiraj
*Indrani
*Rajashekar
*Shivaram in Guest Appearance
*Lilitha
*Nagesh
*B. Jaya (actress)|B. Jaya
*Niyogi
*Anuradha
*Govinda
*N. S. Vaman in Guest Appearance
*Rajananda
*Bhaskar
*Ramakrishnaraju
 

==Soundtrack==
The music was composed by Vijaya Bhaskar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Janaki || R. N. Jayagopal || 03.02
|- Janaki || R. N. Jayagopal || 03.21
|-
| 3 || Nagu Nee Nagu || Vani Jayaram || R. N. Jayagopal || 03.24
|- Janaki || R. N. Jayagopal || 03.33
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 


 