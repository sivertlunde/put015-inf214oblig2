The Wise Kids
{{Infobox film
| name           = The Wise Kids
| image          = 
| caption        = 
| director       = Stephen Cone
| producer       = Stephen Cone Laura Klein Sue Redman
| writer         = Stephen Cone
| starring       = Molly Kunz Tyler Ross Allison Torem
| music          = Mikhail Fiksel
| cinematography = Stephanie Dufford
| editing        = Stephen Cone (uncredited)
| distributor    = Wolfe Video
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Wise Kids is a 2011 drama film written and directed by Stephen Cone.

==Plot==
An ensemble, coming-of-age piece, the film follows a group of young members of a South Carolina Baptist church as they confront issues of homosexuality and a crisis of faith.

==Cast==
*Molly Kunz as Brea
*Tyler Ross as Tim
*Allison Torem as Laura
*Matt DeCaro as Jerry
*Sadieh Rifai as Elizabeth
*Stephen Cone as Austin
*Cliff Chamberlain as Dylan
*Sadie Rogers as Cheryl
*Ann Whitney as Ms. Powell
*Rodney Lee Rogers as Pastor Jim
*Lee Armstrong as Harry
*Laurel Schroeder as Erin
*Sharon Graci as Kathy
*Sullivan Hamilton as Haley
*Jonathan Jones as Josh
*Jacob Leinbach as Brad
*Tessa Nicole as Savannah
*Porter Spicer as Matthew
*Eric Hulsebos as Ryan
*Alyssa Puckett as April

==Production==
To raise money for the film, Cone ran a Kickstarter campaign with a goal of $17,500.  On July 18, 2010, the campaign closed, having successfully raised $17,830.   The film was shot on location in Cones hometown of Charleston, South Carolina. 

==Release== New York, New York on July 23, 2011.   The film also opened the 30th annual Reeling Gay and Lesbian Film Festival. 

===Home media===
The film was distributed by Wolfe Video who released the film on DVD January 8, 2013. 

==Reception==
===Critical response===
The Wise Kids received a very positive response from critics and currently holds a 100% "Fresh" rating on the review aggregator Rotten Tomatoes.   

Roger Ebert of the Chicago Sun-Times gave the film 3 out of 4 stars and described its handling of the subject matter as "honest, observant, and subtle."     Robert Koehler of Variety (magazine)|Variety called the performers "a brilliant cast of young actors" and said of the film, "Most impressively, this is an ensemble piece in which no boogeymen are permitted, everyone is observed in shades of gray, and the easy out of making fun of true believers is simply not in the cards."   Stephen Holden of The New York Times praised the film as "... a guileless exploration of the growing pains of sheltered innocents whose reticence and sincerity evoke 1950s small-town values" and added, "The performances all capture the perplexity of sexually repressed people who are trying to do the proper Christian thing while coping with unruly desires that they recognize as challenges to their way of life. In its unassuming way, this tiny, low-budget film is a universal reflection on issues of personal identity and choice for which there are no easy answers."   

Melissa Anderson of The Village Voice was slightly more critical, commenting, "The Wise Kids suffers from a theater workshop-y tendency to rest too long on pauses and silences to convey dramatic heft. But the blunder is ultimately overshadowed by Cones excellent young actors, particularly Torem, burrowing deeply into her characters zealotry and anguish about being left behind." 

===Accolades===
The Wise Kids was selected as a Critics Pick for The New York Times.   The film won the Audience Award for Best Narrative Feature at NewFest and won the Jury Awards for Best Narrative Feature and Best Ensemble at the Out on Film film festival.     It also won the Grand Jury awards for Outstanding U.S. Screenwriting and Outstanding U.S. Dramatic Feature at the Outfest film festival. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 