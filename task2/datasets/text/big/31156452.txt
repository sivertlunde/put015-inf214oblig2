Mazhakaaru
{{Infobox film 
| name           = Mazhakaaru
| image          =
| caption        =
| director       = P. N. Menon (director)|P. N. Menon
| producer       = SK Nair
| writer         = G Vivekanandan Thoppil Bhasi (dialogues)
| screenplay     = Thoppil Bhasi Madhu Kanakadurga Roja Ramani KPAC Lalitha
| music          = G. Devarajan Ashok Kumar
| editing        = Ravi
| studio         = New India Films
| distributor    = New India Films
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed by P. N. Menon (director)|P. N. Menon and produced by SK Nair. The film stars Madhu (actor)|Madhu, Kanakadurga, Roja Ramani and KPAC Lalitha in lead roles. The film had musical score by G. Devarajan.   

==Cast==
  Madhu as Prabhakaran
*Kanakadurga as Malathi
*Roja Ramani as Shantha
*KPAC Lalitha as Meenakshi
*Mahendran
*Sankaradi as Swami Raghavan
*Adoor Bhavani as Malathis Mother
*Baby Sheela
*Balan K Nair
*Janardanan as Soman
*Kottarakkara Sreedharan Nair
*Kuthiravattam Pappu as Maniyan
*MG Soman
*Madhubala
*PK Venukkuttan Nair
*PO Thomas
*PR Menon
*Radhadevi
*Xavier
*Valsala
*Meera
*Anitha
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Anasooye Priyamvade || P. Madhuri || Vayalar Ramavarma || 
|-
| 2 || Maninaagathirunaaga || P Jayachandran, P. Madhuri || Vayalar Ramavarma || 
|-
| 3 || Pralayapayodhiyil || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 4 || Vaikkathappanum Sivaraathri || MG Radhakrishnan, Chorus || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 