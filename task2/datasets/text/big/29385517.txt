The Flemish Farm (film)
 
{{Infobox film 
  | name = The Flemish Farm
  | image =  
  | caption = 
  | director = Jeffrey Dell
  | producer = Sydney Box
  | writer = Jeffrey Dell, Jill Craigie
  | starring = Clive Brook, Clifford Evans, Jane Baxter
  | music = Ralph Vaughan Williams
  | cinematography =  Eric Cross
  | editing =
  | distributor = Two Cities Films
  | released = 6 September 1943
  | runtime =  82 minutes 
  | language = English
  | budget =  
  | preceded_by =  
  | followed_by = 
  }}

The Flemish Farm is a 1943 British war film, based on an actual war-time incident. Released during the war, and used as a propaganda tool to support the allied war effort, the film begins with the caption:
 
:The following story is based on an actual incident, but for security reasons real names have not been used. The co-operation of the Belgian Government and of the Air Ministry is gratefully acknowledged.   

The film score, by Ralph Vaughan Williams, is an orchestral piece in 7 movements, entitled The Story of a Flemish Farm and was recorded by the London Symphony Orchestra, conducted by Muir Mathieson. 

==Plot==
In May 1940, as German forces sweep across France and Belgium, the remains of the Belgian Air Force are bottled up near the Flemish coast, and billeted at a farm in the Flemish countryside. Ordered by their government to surrender, the commander gives orders that the regimental colours be honorably buried, rather than surrendered to the invaders. The few pilots with serviceable aeroplanes fly to England to join the Allied airforces, while those remaining are forced to surrender. 

Six months later, after fighting in the Battle of Britain, Jean Duclos, now a squadron leader, is persuaded by a fellow officer to return with him to retrieve the colours. The latter is killed before he can leave and Duclos persuades the authorities to parachute him into Belgium. He contacts his former commanding officer, now living as a civilian in Brussels and secretly operating a resistance group feeding intelligence to the Allies. Duclos then returns incognito to the farm, where his late colleagues wife and child still live. She is initially unwilling to reveal where the colours are buried, believing that they arent worth dying for. But she relents and the colours are retrieved.

Duclos must now travel through several hundred miles of dangerous and heavily guarded country to reach neutral Spain, from where he returns to England. On his return, the colours are paraded and formally re-presented to the Belgian Air Force.

==Cast==
*Clive Brook - Major Lessart 
*Clifford Evans - Squadron Leader Jean Duclos 
*Jane Baxter  - Tresha 
*Wylie Watson - Flemish farmer 
*Philip Friend - Fernand Matagne 
*Ronald Squire - Hardwicke 
*Brefni ORorke - Minister 
*Mary Jerrold - Mme Duclos 
*Charles Compton - Ledoux
*Irene Handl - Frau

==Locations==
One scene was filmed on Chelfham Viaduct, formerly of the Lynton and Barnstaple Railway in North Devon

==Notes==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 