Breathless (1982 film)
{{Infobox film
| name           = Breathless
| image          = 
| caption        = 
| director       = Mady Saks
| producer       = Roeland Kerbosch
| writer         = Mady Saks
| starring       = Coen Bennink
| music          = 
| cinematography = Cees Samson
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
}}

Breathless ( ) is a 1982 Dutch drama film directed by Mady Saks. It was entered into the 13th Moscow International Film Festival.   

==Cast==
* Coen Bennink as Verpleger
* Bobby Boermans as Bobo (as Bobo Boermans)
* Theu Boermans as Dokter
* Ursul de Geer as Van Poppel
* Frouke Fokkema as Verpleegster
* Pieter Groenier as Leo
* Cora Hollema as Patiente
* Maarten Kouwenhoven as Therapeut
* Marie Louise Stheins as Pauline
* Monique van de Ven as Anneke
* Ina van der Molen as Agaath
* Linda van Dyck as Martha
* Olga Zuiderhoek as Moeder

==References==
 

==External links==
*  

 
 
 
 
 
 
 