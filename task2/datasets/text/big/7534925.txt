Johns (film)
{{Infobox Film
| name           = Johns
| image          = Johns-scott-silver.jpg
| caption        = DVD cover
| director       = Scott Silver
| producer       = Paul Brown Beau Flynn Stefan Simchowitz
| writer         = Scott Silver
| starring       = David Arquette Lukas Haas
| music          = Charles Brown Danny Caron Tom Richmond
| editing        = Dorian Harris
| distributor    = First Look International
| released       = January 31, 1997
| runtime        = 96 min.
| country        = United States
| language       = English
| budget         = 
| gross          = $50,789
}} hustlers who work Santa Monica Boulevard.  

==Plot== Los Angeles park. He awakens as someone is stealing his shoes, in which he keeps his money. He chases the thief but cant catch him. John is angered not only because those are his "lucky" sneakers but because hes trying to accumulate enough money for an overnight stay in a fancy hotel to celebrate his birthday, which is also Christmas. Each time John puts any money together, either by turning a trick, robbing the house of one of his regular "dates" or stealing from potential clients, its taken from him either by robbery or in payback for a drug deal where he burned the dealer.

Meanwhile, Donner (Lukas Haas), a fellow hustler whos new to the streets and has fallen for John, tries to convince John to go with him to Branson, Missouri. Donner has a relative who runs a theme park there who can get them jobs. John is initially resistant to the idea but, after some particularly bad experiences, agrees to go.

John and Donner have enough money for two bus tickets to Branson but John takes one last "date" to earn money for expenses. After their sexual encounter at a motel, however, Johns "date" turns violent, beating John unmercifully.

Donner goes in search of John and finds him at the motel. Donner drags Johns lifeless body from the bathroom to the bed and tearfully confesses that hes the one who stole Johns sneakers and money in a desperate attempt to persuade John to leave town with him.

==Cast==
* David Arquette as John
* Lukas Haas as Donner
* Wilson Cruz as Mikey
* Keith David as Homeless John
* Christopher Gartin as Eli
* Elliott Gould as Manny Gold Terrence Dashon Howard as Jimmy the Warlock
* Nicky Katt as Mix
* Richard Kind as Paul Truman
* John C. McGinley as Danny Cohen Richard Timothy Jones as Mr. Popper
* Alanna Ubach as Nikki
* Arliss Howard as John Cardoza
* Nina Siemaszko as Tiffany the Prostitute
* Craig Bierko as Christmas Radio Preacher

==Awards and nominations==
* San Sebastián International Film Festival Best New Director - Scott Silver (1996)

==DVD release==
Johns was released on Region 1 DVD on February 29, 2000.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 