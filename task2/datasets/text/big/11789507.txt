Fear (1954 film)
{{Infobox film
| name           = Fear
| image          = Angst film poster.jpg
| image_size     = 
| caption        = Belgian theatrical release poster
| director       = Roberto Rossellini
| producer       = Herman Millakowsky
| writer         = Roberto Rossellini Sergio Amidei Franz von Treuberg
| based on       =  
| starring       = Ingrid Bergman Mathias Wieman Renate Mannhardt Kurt Kreuger  Renzo Rossellini
| cinematography = Carlo Carlini Heinz Schnackertz 
| editing        = Jolanda Benvenuti Walter Boos
| distributor    = Minerva Film SpA
| released       =  
| runtime        = 83 minutes
| country        = Germany Italy
| language       = German Italian
| budget         = 
}} German and noirish with Hitchcock and German Expressionism.

==Plot==
Irene Wagner (Bergman), the wife of prominent German scientist Professor Albert Wagner (Wieman), had been having an affair with Erich Baumann (Kreuger). She does not disclose this to her husband, hoping to preserve his innocence and their "perfect marriage". This fills her with anxiety and guilt. However, Johann Schultze (Mannhardt), Erichs jealous ex-girlfriend, learns about the affair and begins to blackmail Irene, turning Irenes psychological torture into a harsh reality. When Irene finds out that the extortion plot is truly an experiment in fear, she is driven into a homicidal/suicidal rage. But she is saved from suicide by her husband at the last minute, both sorry for what they did.

==Cast==
* Ingrid Bergman as Irene Wagner
* Mathias Wieman as Professor Albert Wagner
* Renate Mannhardt as Luisa Vidor, alias Johann Schultze
* Kurt Kreuger as Erich Baumann
* Elise Aulinger as Haushaelterin
* Edith Schultze-Westrum
* Steffi Stroux
* Annelore Wied
* Klaus Kinski as Cabaret Performer (uncredited)

==Reception==
The film did not do well when it was released in Italy and Germany. Consequently, the Italian distributor edited the film and re-released it as Non credo più allamore. In this edited version, a fishing scene is shortened and an explanatory narration is added to two silent scenes. In addition, the ending was changed from a scene showing Bergman attempting suicide to a scene showing her family in the countryside, after Bergman had left her husband, living on for the sake of her children.

==The Rossellini Project==
This initiative involves 10 films by Roberto Rossellini that are being digitally restored and will then be promoted internationally. Carrying out the restoration work are Cinecittà Luce-Filmitalia, the Cineteca di Bologna,
the Centro Sperimentale di Cinematografia, and the  .
Fear is one of the ten films being restored. The others are:
 , and Interview with Salvador Allende (Intervista a Salvador Allende: La forza e la ragione).
 

==Turner Classic Movies (TCM) North American TV Premiere==
On March 15, 2013, Turner Classic Movies broadcast Fear for the first time on TV in North America. 
 

==Footnotes==
 

== External links ==
*  
*  

*  
*  
*  
*   - Harvard University Film Archive screened Fear on July 21 and 23, 2005 as part of its summer exhibition of films. Read a brief synopsis of the film at this site.
*   - Review of Fear that also includes movie stills.
*  

 

 
 
 
 
 
 
 
 