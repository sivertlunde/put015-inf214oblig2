Pharaoh (film)
{{Infobox film
| name = Pharaoh
| image = Faraon.jpg
| caption = Publicity still
| director = Jerzy Kawalerowicz
| producer =
| writer =  
| starring =  
| music = Adam Walaciński
| cinematography = Jerzy Wójcik
| editing = Wiesława Otocka
| distributor =
| released =  
| runtime = 175 minutes
| country = Poland
| language = Polish
| budget = 
| gross =
}} eponymous novel by the Polish writer Bolesław Prus. In 1967 it was nominated for an Academy Award for Best Foreign Language Film.    It was also entered into the 1966 Cannes Film Festival.   

==Cast==
*Ramses XIII; and his  
*Herhor (High Priest of Amon): Piotr Pawłowski
*Pentuer (priest, Herhors assistant): Leszek Herdegen
*Thutmose (Ramses XIIIs cousin): Emir Buczacki Egyptian army officer): Ryszard Ronczewski
*Fellah: Jerzy Block
*Sara (Ramses XIIIs mistress, mother of his son Seti): Krystyna Mikołajewska
*Ramses XII (father of Ramses XIII): Andrzej Girtler
*Nitager (Egyptian general): Wiktor Grotowicz
*Queen Nikotris (mother of Ramses XIII): Wiesława Mazurkiewicz
*Berossus (Chaldean priest): Kazimierz Opaliński Egyptian high priest): Stanisław Milski 
*Mentezufis (Egyptian priest): Józef Czerniawski
*Dagon (Phoenician merchant): Edward Rączkowski
*Rabsun (Phoenician merchant): Marian Nosek Tyrian prince): Alfred Łodziński
*Kama (Phoenician priestess): Barbara Brylska
*Sargon (Assyrian envoy): Jarosław Skulski Libyan commander): Leonard Andrzejewski mummification of Ramses XII: Lucyna Winnicka
*Keeper of the Labyrinth#Herodotus Egyptian labyrinth|Labyrinth: Bohdan Janiszewski
*Samentu (High Priest of Set (mythology)|Set): Mieczysław Voit
*Hebron (Ramses XIIIs last mistress): Ewa Krzyżewska
*Other principal actors: Bronisław Dardziński, Jerzy Fidler, Jerzy Kozłowski 

==Novel== The Shade Night Train power in Pharaoh is incredibly topical and contemporary.  The mechanics dont change all that much."  
 reasons of Leon Schiller State School, "Faraon." 

==Film== Labyrinth were Lake Kirsajty, palms and lotus for the scene involving Ramses row on the Nile with Sara. 
 vipers and venomous spiders that launched themselves at people from a couple of yards distance. 

Some scenes were filmed at authentic Egyptian locales.  For example, the scene in which Prince Ramses learns that his father Pharaoh Ramses XII has died and that he has now become Pharaoh Ramses XIII, takes place against the backdrop of the pyramids of Gizeh; but the crowds of tourists and the present-day appearance of the area made it near-impossible to find good takes.  One of the many consultants on the film was Polands Professor Kazimierz Michałowski, a world authority in Egyptology.  Another was Shadi Abdel Salam, an Egyptian film director and costume designer, who had consulted on the 1963 Cleopatra (1963 film)|Cleopatra.  Abdel Salam was the costume designer for Pharaoh. 
 Labyrinth not with a chloroform-like substance, but with a rope looped around his neck and pulled tight by its ends, several yards apart.
Pharaoh is among 21 digitally restored classic Polish films chosen for Martin Scorsese Presents: Masterpieces of Polish Cinema.  

==See also==
*Pharaoh (novel)|Pharaoh (the novel)
*National Film School in Łódź
*List of historical drama films
*List of submissions to the 39th Academy Awards for Best Foreign Language Film
*List of Polish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  Leon Schiller State School of Film, Television and Theater, "Faraon."
*   at  

 

 
 
 
 
 
 
 
 
 
 
 