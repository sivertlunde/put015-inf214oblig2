Coriolanus: Hero without a Country
 
{{Infobox film
| name           = Coriolanus: Hero without a Country 
| image          = Coriolanus_Hero_without_a_Country.jpg
| image size     =
| caption        =
| director       = Giorgio Ferroni
| producer       =  
| writer         = 
| starring       = Gordon Scott
| music          = Carlo Rustichelli
| cinematography = Augusto Tiezzi
| editing        =  
| distributor    =
| released       = 1963
| runtime        =  
| country        =  
| language       = Italian
| budget         =
}}
 historical drama Rome in general who won great victories for the Romans over their enemies the Volscians, but was then forced into exile by his political enemies at home.

The film was directed by Giorgio Ferroni.

==Cast==
* Gordon Scott as "Gaius Marcius Coriolanus|Coriolanus"
* Alberto Lupo as "Escinius"
* Lilla Brignone as "Volumnia"
* Philippe Hersent as "Cominius"
* Rosalba Neri as "Virginia"
* Aldo Bufi Landi as "Marco" Menenius Agripa"
* Piero Pastore
* Tullio Altamura
* Nello Pazzafini

==See also==
* List of historical drama films
* List of films set in ancient Rome

==External links==
*  

 
 
 
 
 
 
 


 
 