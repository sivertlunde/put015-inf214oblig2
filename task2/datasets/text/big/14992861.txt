Snow Buddies
 
{{Infobox Film
| name = Snow Buddies
| image = snow_buddies_poster.jpg
| caption = Sales poster
| director = Robert Vince
| producer = Robert Vince Anna McRoberts
| writer = Robert Vince Anna McRoberts
| based on =  
| starring = Jimmy Bennett Mike Dopud John Kapelos Charles C. Stevenson Mary Birdsong-Covey Wendi McLendon-Covey Kris Kristofferson
| music = Brahm Wenger Walt Disney Studios Home Entertainment
| released =  
| runtime = 87 minutes
| country = United States English
| budget = $80 million
}}

Snow Buddies is a 2008 direct to video in the Air Bud (series)|Air Bud series. It was released on DVD on February 5, 2008. The movie was shot on location in Canada at Mount Seymour, North Vancouver, and the town of Ladner, British Columbia.

==Plot==
  Washington who become trapped in a truck heading to Ferntiuktuk, Alaska.  Upon arrival, the Buddies meet Shasta, a young husky whose 11-year-old owner is determined to win the Alaskan sled dog race. The puppies decide to help Shasta pursue his dreams. Unfortunately, as Shasta has no parents, this puts the puppies in a predicament as there is nobody to teach them how to become snow dogs. Shasta manages to persuade a dog named Talon into teaching the puppies into becoming his new protégés.

When Shasta introduces his owner, Adam, to his new sleigh team, the child is delighted at the prospect of his dreams coming true and the team pursue vigorous training routines.  Adam begins building a new sleigh with his team of hard-working puppies. Talon proudly watches as the teams efforts come to fruition and it seems as though they are cooperating as a team. The older town huskies, however, are not impressed and begin to plan their downfall. They reveal to the Buddies that Shastas parents died when the ice beneath them shattered to dishearten them.
 Northern Lights before he departs telling Shasta that he knows all he needs to know and that he can become the great leader that his father was. The following morning, the puppies enter the race with Adam. After being reminded how treacherous the race is by the sheriff, the puppies begin their trek.

Meanwhile, the puppies parents follow the puppies to Alaska where the rescue dog Bernie informs them of their participation in the race.  A dangerous snowstorm forces Adam and the team take shelter in an igloo provided by an Inuit until the storm subsides. They come head to head with their opponent, Jean George, and Adam gets injured.

Adam then recovers. Jean Georges dogs get into trouble when the ice beneath them shatters. Jean George continues and abandons his dogs while Adam and the puppies begin a rescue operation despite Shastas fears of his parents deaths. The puppies pull the dogs out of the icy waters and Jean George continues the race without any gratitude and abandons their rescuers.  Jean Georges dogs realize they owe nothing to their owner and everything to Shasta and the Buddies, and so, slow down and "go on strike" causing Jean George to lose the race.

Adam is victorious and the Buddies reunite with Buddy and Molly. Jean George gives out to his dogs and they respond in kind by chasing him around the Arctic. All seven Buddies return home via airplane.

==Cast==
*Skyler Gisondo as B-Dawg
*Jimmy Bennett as Buddha
*Josh Flitter as Budderball
*Henry Hodges as Mudbud
*Liliana Mumy as Rosebud, the oldest female puppy
*Mary Birdsong-Covey as Peatty, a Siberian Husky
*Wendi McLendon-Covey as Truffles, a Prologue Dog 
*Tom Everett Scott as Buddy
*Karley Scott Collins as Lily, Rosebuds younger sister and the youngest of all the puppies
*Molly Shannon as Molly
*Kris Kristofferson as Talon, an Alaskan Malamute
*James Belushi as Bernie, a St. Bernard dog
*Paul Rae as Phillipe
*Lothaire Bluteau as Francois
*Whoopi Goldberg as Miss Mittens, a Himalayan cat
*Christian Pikes as Henry
*John Kapelos as Jean George
*Tom Woodruff, Jr. as Slim Wolf (uncredited)

==Controversy== Washington State. euthanized for intussusception before the parvo outbreak, one for suspected parvo (but not confirmed by necropsy) and one puppy who was returned to the breeder without being used reportedly died of parvo. The American Humane Association enforced the removal of the first two sets of puppies, and after a four-week delay, during which time all puppies were quarantined until they were healthy, Disney hired 8 older puppies to continue filming. These puppies were joined by puppies from the second group who did not get ill from parvovirus and filming was completed. In total, five puppies died during the making of the film. 

==Home Media==
Walt Disney Studios Home Entertainment released the film on Blu-ray in January 2012.

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 