Arjun (1985 film)
 Arjun}}
 
{{Infobox film
| name           = Arjun
| image          = Arjun Movie 1985.JPG
| image_size     = 
| caption        = Original Promotional Poster
| director       = Rahul Rawail
| producer       = 
| writer         = Javed Akhtar
| narrator       =  Raj Kiran   Anupam Kher
| music          = Rahul Dev Burman
| cinematography = 
| editing        = 
| studio      = Cineyug Productions
| distributor    = Eros Entertainment
| released       = 1985
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1985 Indian Venkatesh and in Kannada as Sangrama with Ravichandran.

The film is about a  person named Arjun (Sunny Deol) who is in need of a job. He has a group of friends, who unknowingly get involved in small problems faced by poor people and come to be recognized as Robin Hood types. Arjuns father (A.K. Hangal) is a simple man and has surrendered to living a poor mans life without confronting the problems faced. His wife Shashi Kala is his second wife who does not like Arjun. He gets used by political people for their selfish purposes.

==Plot==

Unemployed but tolerant and kind hearted Arjun Malvankar stays in the suburbs of Bombay with his father and step mother and step sister. Mr. Malvankar (A.K. Hangal) still works in old age to make both ends meet. One day Arjun beats up a group of ruffians who are thrashing a poor man for not paying extortion taxes.

With this incident, Arjuns life changes. Arjun invokes the wrath of the local goon, who orders more people to go and take him, but this s time, assisted by his five friends, Arjun beats them badly and send them back. The goon warns Arjuns parents to advise him against his activities, lest he get himself killed, at the infuriated Arjun not only goes and thrashes him, but also destroys his bar and all its activities.
 Raj Kiran) notes that Arjun is actually doing the right thing. he also begins to fall in love with Arjuns sister. Arjun is also approached by Geeta (Dimple Kapadia), a journalist who asks him for a favour which he obliges, and after which they both get close to each other.
Soon Arjun begins to grab attention. The chief minister Deen Dayal Trivedi (Prem Chopra), who is behind all illegal activities in the city, and who is also supporting the don, gets to know of Arjun. He thinks that Arjun is working for his rival Shiv Kumar Chowgule (Anupam Kher) whose party is trying to get nominated for the upcoming elections, and orders that he and his friends be eliminated. The gang attack and kill one of Arjuns friends, Mohan (Satyajeet) in the public. Though Arjun tries his best, no one comes forward to give witness to the murder out of fear, because of which the killers are left free. Arjun goes and beats up the killers till he is stopped by police.
 Matka den operated by his trusted aides, Anoop (Paresh Rawal). He also manages to lay hold of Anand Patkar (Shafi Inamdar), a wealthy alcoholic who is part of the nomination committee for elections and on Trivedis side. Arjun gets to know that a blackmailer has a few illicit photos of Anand with which he blackmails him, and he uses force to get the photos and negatives from the blackmailer. Pleased by this, Anand now decides to shift sides and instead of supporting Trivedi, begins to support Chowgule on Ajays request. The enraged Trivedi orders Anoop to murder Anand, which he does but unfortunately gets caught red handed by police.

Arjun now gets his sister married to Inspector Ravi at her request, and also handles the shop owner where his father is working for ill treating the latter.
Finally, Chowgule tells Arjun to get some secret files and documents form Trivedi, which can be used against the latter to expose him in public. Arjun gets this information form Trivedis right-hand man and manages to flick the papers and hand them to Chowgule. The pleased politician congratulates him and promises him that Trivedi will now be exposed.

The film takes a twist when Arjun sees that Chowgule has joined hands with Trivedi and that none of the evidence he collected has really been published anywhere, as promised by Chowgule. He realizes that the politician is a corrupt person who was using him for his own gains. The car and house given to Arjun are also confiscated. The corrupt MLAs now turn to another one of Arjuns friends, Chander (Raja Bundela) and promising him money, ask him to kill the arrested Anoop in court, which he does. In jail, Chander tells Arjun that his mother is ill and sister unmarried, and he committed the murder for money. Arjun rushes to his house, only to find that his mother has died, and sister has eloped with somebody. On returning to visit him in prison, he sees that his friend has hung himself.

Frustrated, angered, Arjun goes to fight the politicians at their speech rally but is simply thrown out. At home with Geeta, Arjun is visited by his father, who tells him that all his life he has borne wrongdoings and injustice. He blesses his son, telling him that he is with him in the fight against injustice.

The film then reaches its climax, where Arjun breaks into Trivedis house and beats hm up, then proceeds to Chowgules mansion, where he beats up the politician and takes away all the files and evidence of both Chowgules and Trivedis bad activities. But the wounded Chowgule orders his men to chase Arjun and kill him.
Arjun is now on the run in the empty streets of Bombay at night, with goons hot on his heels. They manage to shoot him a couple of times, but Arjun still runs. He manages to evade them, causing their vehicles to crash, until finally he kills the last goon and arrives at Inspector Ravis Police station, where he collapses and gives him the evidence. Chowgule and Trivedi are finally arrested.

The film ends with Ravi giving Arjun a thumbs up in hospital.

==Cast==

*Sunny Deol                             as  Arjun Malvankar
*Dimple Kapadia                         as  Geeta Sahani Satyajeet               as  Mohan Raj Kiran  as  Insp. Ravi Rane
*Anupam Kher                            as  Shivkumar Chowgule
*Supriya Pathak                         as  Sudha Malvankar
*A.K. Hangal                            as  Mr. Malvankar
*Shashikala                             as  Mrs. Rukhmini Malvankar
*Prem Chopra                            as  Chief Minister Deen Dayal Trivedi 
*Raja Bundela                           as  Chander
*Paresh Rawal                           as  Anup Lal
*Goga Kapoor                            as  Ranga
*Annu Kapoor                            as  Babu Ram
*Shafi Inamdar                          as  Anand Patkar
*Ghanshyam Rohera                       as  Ashok Babu

==Music==

This film has good music by Rahul Dev Burman. Javed Akhtar has written the lyrics. All the four songs are hummable and melodious.
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Mammaya Kero Kero Kero Mama" Shailendra Singh
|-
| 2
| "Dhadkan Pal Pal"
| Asha Bhosle
|-
| 3
| "Munni Pappu aur Chunmun"
| Asha Bhosle
|-
| 4
| "Bhoore Bhoore Aankhonwala"
| Lata Mangeshkar
|}

== External links ==
*  

 
 
 
 
 
 
 