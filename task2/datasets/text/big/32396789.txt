Squatter's Rights (film)
 
{{Infobox Hollywood cartoon cartoon name=Squatters Rights
|series=Mickey Mouse
|image=Squatters Rights.jpg
|caption=Theatrical release poster
|director=Jack Hannah
|producer=Walt Disney story artist=Rex Cox Harry Reeves
|narrator= voice actor=Pinto Jimmy MacDonald
|musician=Oliver Wallace
|animator=Bob Carlson Rex Cox Hugh Fraser Blaine Gibson Murray McClellan layout artist= background artist= Walt Disney Productions RKO Radio Pictures release date=  (USA) color process=Technicolor
|runtime=7 minutes
|country=United States
|language=English preceded by=In Dutch (1946) followed by=The Purloined Pup (1946)
}} animated short Walt Disney RKO Radio Pluto and Chip and taken up residence in Mickey Mouses hunting shack. It was nominated for an Academy Award for Best Animated Short Film in 19th Academy Awards|1947, but ultimately lost to The Cat Concerto, an MGM Tom and Jerry film.
 Jimmy MacDonald making this the debut of MacDonald as Mickey. He would go on to provide Mickeys voice for over 30 years.  It was also Mickeys first post-war appearance. With the exception of a very brief cameo in The Three Caballeros (1945), Mickey had not appeared in a theatrical film since Pluto and the Armadillo in 1943.   

==Synopsis== wood stove they have made their home. The stove is located in Mickey Mouses hunting shack (called "Mickeys Hydout") which appears to have been unoccupied for a while. Soon Mickey and Pluto arrive for the hunting season.

Pluto soon discovers that the stove is occupied by the chipmunks and helps Mickey build a fire to smoke them out. Chip and Dale realize what is happening and blow out Mickeys matches and roll of newspaper before they can catch the wood on fire. Finally Mickey is about to use a can of kerosene which the chipmunks cant blow out. From a hiding place underneath the stove, the chipmunks burn a hole in Mickeys boot with a match. Mickey assumes that Pluto is to blame and scolds him. Mickey remains unaware of the chipmunks throughout the film.

After Mickey leaves to get more wood, Pluto chases Chip and Dale across the room. They lead Pluto across a table and mantle above the fireplace. Pluto accidentally gets his nose stuck in the muzzle of Mickeys rifle which is hanging over the fireplace. As Pluto tries to pull is nose free, he realizes that one of the hooks on which the rifle is mounted is directly in front of the trigger; the more Pluto pulls the more he will cause the gun to go off in his face. Gradually the table which Pluto is standing on with his hind paws starts to slide back. Pluto falls and makes the rifle fire, but rifles delay is just long enough that Pluto narrowly avoids the bullet. Pluto lands on the floor where the rifle hits him on the head momentarily knocking him unconscious. From the table above Chip and Dale pour ketchup over the dog to make it look as if he is seriously wounded.

Suddenly Mickey returns having heard the gunshot, and when he sees Pluto he thinks he is dead. Pluto comes to and first starts to comfort Mickey. But when he sees the ketchup he starts to panic. Mickey hurriedly carries him off to find help. Thus Chip and Dale regain working ownership over the property. 

==Releases==
*1946 &ndash; theatrical release
*1955 &ndash; Walt Disney anthology television series|Disneyland, episode #2.5: "Adventures of Mickey Mouse" (TV)
*1975 &ndash; "Walt Disneys Cartoon Carousel" (TV)
*1984 &ndash; "Cartoon Classics: More of Disneys Best 1932-1946" (VHS)
*c. 1992 &ndash; Mickeys Mouse Tracks, episode #72 (TV) The Ink and Paint Club, episode #1.5: "Chip n Dale" (TV)
*2004 &ndash; " " (DVD)
*2010 &ndash; iTunes (digital download)

==Notes==
 
 
 
 
 