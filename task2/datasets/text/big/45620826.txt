Currito of the Cross (1965 film)
{{Infobox film
| name =   Currito of the Cross 
| image =
| image_size =
| caption =
| director = Rafael Gil
| producer = 
| writer = Alejandro Pérez Lugín (novel)   José López Rubio   Luis de Diego   Antonio Abad Ojuel   Rafael Gil
| narrator = Arturo Fernandez   El Pireo   Soledad Miranda
| music = Manuel Parada  
| cinematography = José F. Aguayo  
| editing = José Luis Matesanz
| studio = Coral Producciones Cinematográficas
| distributor = 
| released = 1 July 1965
| runtime = 109 minutes
| country = Spain Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
}} Arturo Fernandez novel of the same title by Alejandro Pérez Lugín. 

==Cast==
*    Francisco Rabal as Manuel Carmona  Arturo Fernández as Ángel Romera Romerita  
* El Pireo as Currito de la Cruz  
* Soledad Miranda as Rocío Carmona  
* José Marco Davó as Don Emilio  
* Adrián Ortega as Don Ismael  
* Luis Ferrín as Gazuza  
* Yelena Samarina as Teresa  
* Ángel Álvarez as Don Antonio  
* Julia Gutiérrez Caba as Madre María 
* Mercedes Vecino as Doña Manuela Alonso, viuda de Varela  
* Manolo Morán as Copita 
* Juan Cortés as Comisario  
* Rafael Durán as Carmona 
* Carlos Mendy as Pintao  

== References ==
 
 
==Bibliography==
* Labanyi, Jo & Pavlović, Tatjana. A Companion to Spanish Cinema. John Wiley & Sons, 2012.

== External links ==
* 

 
 
 
 
 
 
 

 