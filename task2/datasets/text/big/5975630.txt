Young Man with a Horn (film)
{{Infobox film 
| name           = Young Man with a Horn
| image          = Young Man with a Horn (film).jpg
| caption        = Theatrical release poster
| director       = Michael Curtiz
| producer       = Jerry Wald
| screenplay     = Carl Foreman Edmund H. North
| based on       =  
| starring       = Kirk Douglas Lauren Bacall Doris Day Hoagy Carmichael
| music          = Lauren Kirk
| cinematography = Ted D. McCord
| editing        = Alan Crosland Jr.
| distributor    = Warner Bros.
| released       =  
| runtime        = 112 minutes
| country        = United States
| language       = English
}} 1950 musical musical drama novel of the same name by Dorothy Baker inspired by the life of
Bix Beiderbecke, the jazz cornet|cornetist.  The movie stars Kirk Douglas, Lauren Bacall, Doris Day, and Hoagy Carmichael, and was directed by Michael Curtiz and produced by Jerry Wald. The screenplay was written by Carl Foreman and Edmund H. North.

==Plot==
As a young boy, after his mother dies, Rick Martin sees a trumpet in the window of a pawn shop. He works in a bowling alley to save up enough money to buy it. Rick grows up to be an outstanding musician, tutored by jazzman Art Hazzard. He lands a job playing for the big band of Jack Chandler, getting to know the piano player Smoke Willoughby and the beautiful singer Jo Jordan.

Chandler orders him to always play the music exactly as written. Rick prefers to improvise, and one night, during a break with Chandlers band, he leads an impromptu jam session, which gets him fired.

Jo has fallen for Rick and finds him a job in New York with a dance orchestra. One night, her friend Amy North accompanies her to hear Rick play. Amy, studying to be a psychiatrist, is a complicated young woman still disturbed by her own mothers suicide.

She claims to be incapable of feeling love, but she and Rick begin an affair and eventually are married. Rarely together at the same time because of their demanding schedules, Rick and Amy constantly quarrel. She fails in her attempts to become a doctor and takes it out on Rick, demanding a divorce. He begins drinking and his mood deteriorates to the point that one day he even takes it out on Art Hazzard, a man who had done so much for him. Before Rick can apologize, Art is hit by a car and killed.

Rick becomes an alcoholic who neglects his music and even destroys his horn. He disappears, until one day Smoke finds him in a drunk tank. Jo is contacted and rushes to Ricks side, helping him to recover his love of music and of her—a happy ending 
found neither in the novel nor in the life of Bix Beiderbecke.

==Cast==
  in background.]]
 
* Kirk Douglas as Rick Martin
* Lauren Bacall as Amy North
* Doris Day as Jo Jordan
* Hoagy Carmichael as Willie Smoke Willoughby
* Juano Hernández as Art Hazzard
* Jerome Cowan as Phil Morrison
* Mary Beth Hughes as Marge Martin
* Nestor Paiva as Louis Galba Walter Reed as Jack Chandler
 

==Production notes==
Composer-pianist Hoagy Carmichael, playing the sidekick role, added realism to the film and gave Douglas insight into the role, being a friend of the real Beiderbecke.   Famed trumpeter Harry James performed the music Kirk Douglas is shown playing on screen. 

In her authorized biography, Doris Day described her experience making the film as "utterly joyless", as she had not found working with Douglas to be pleasant. In the book, Douglas said that he felt her ever-cheerful persona was only a "mask" and he had never been able to get to know the real person underneath. Day countered that while Douglas had been "civil", he was too self-centered to make any real attempt to get to know either her or anyone else. 

==Reception==
According to The New York Times, "banalities of the script are quite effectively glossed over in the slick pictorial smoothness of Michael Curtizs direction and the exciting quality of the score. The result is that there is considerable good entertainment in Young Man With a Horn despite the productions lack of balance."  

In spite of the screenplay, the Times praised the performances of Douglas, Day and Hoagy Carmichael, but noted "the unseen star of the picture is Harry James, the old maestro himself, who supplies the tingling music which flows wildly, searchingly and forlornly from Rick Martins beloved horn. This is an instance where the soundtrack is more than a complementary force. It is the very soul of the picture because if it were less provocative and compelling the staleness of the drama could be stultifying." 

==See also==
* List of American films of 1950

==References==
{{reflist|refs=
   }}

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 