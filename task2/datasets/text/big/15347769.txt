Mr. Wu
 
{{Infobox film
| image = File:Mr. Wu lobby card.JPG
|caption=Lobby card
| name = Mr. Wu
| director = William Nigh
| producer = William Nigh
| writer = Play:   Titles: Lotta Woods Lon Chaney Louise Dresser Renée Adorée Holmes Herbert Ralph Forbes Gertrude Olmstead Anna May Wong
| cinematography = John Arnold
| editing = Ben Lewis
| distributor = Metro-Goldwyn-Mayer Pictures
| released =  
| runtime = 90 minutes
| country = United States 
| language = Silent film English intertitles
}} silent movie Lon Chaney as a Chinese patriarch who tries to exact revenge on the Englishman who seduced his daughter.  The supporting cast includes Anna May Wong.

==Cast== Lon Chaney - Mr. Wu/Grandfather Wu
*Louise Dresser - Mrs. Gregory
*Renée Adorée - Wu Nang Ping
*Holmes Herbert - Mr. Gregory
*Ralph Forbes - Basil Gregory
*Gertrude Olmstead - Hilda Gregory
*Mrs. Wong Wing - Ah Wong Claude King - Mr. James Muir
*Sonny Loy - Little Wu
*Anna May Wong - Loo Song
*Toshia Mori - Nang Pings friend (*billed as Toshia Ichioka)

==Synopsis==
In the prologue, Chaney plays Grandfather Wu, entrusting his grandsons education to a trusted English associate, stating "The West is coming to the East. He   must be prepared for both." Chaney also plays the part of the grandson as a young man who enters an arranged marriage with a delicate girl who dies after giving birth to a daughter. Wu swears he will raise the child as a daughter and a son. 

As Mr. Wus daughter, Nang Ping, emerges into womanhood, he arranges a marriage for her with a mandarin. A lively, spirited girl, she has been educated by her fathers old tutor. Despite the seclusion of her fathers palace, she meets and falls in love with Basil Gregory, a young Englishman. His father is a diplomat who does not respect Chinese ways and insults the "damned Chinks" to their faces, although the rest of his family is courteous. 

Basil later informs Nang Ping that he must return to Britain with his family, but she surprises him with the revelation that she carries his child. Later, Basil indicates he indeed wants to marry her, but she then claims she was merely trying to establish if he was an "honorable man", and reveals she is already engaged in an arranged marriage. Via a nosy gardener, Nang Pings father discovers the relationship.  

While Wu agonizingly peruses his cultures ancient writings, he comes to the conclusion that a dishonored daughter must "die by the hand of her father."  Despite his great tenderness and love for her, he interprets customary Chinese law as grimly necessitating Nang Pings execution. Father and daughter bid each other a tearful goodbye, she asks him to spare her lover, indicating that would make death less painful and less meaningless for her. She then obediently retreats to the homes central shrine. The last scene of that act concludes as the curtain dramatically drops over the profile of Wu lifting his sword high above his daughters prostrate form. 

Wu then goes about exacting revenge on the Gregory family, whom he feels is responsible for the "dishonor" and subsequent death of his daughter. He invites Mrs. Gregory and her daughter to his home and tricks the daughter into going into Nang Pings room, immediately locking her in, imprisoning her on the rooms raised outdoor terrace. He has Basil tied up in an adjacent garden, flanked by an axe-wielding servant. By sunset, Mrs. Gregory has the choice of either condemning her son to death or her daughter to prostitution. She says there is another way and offers her own life. Wu explains that this is not the custom in China; the parents are obliged to live on and bear the shame. Nonetheless, Mrs. Gregory continues to insist Wu take her life instead of those of her children, and in an ensuing struggle, Mrs. Gregory stabs Wu with a knife from a nearby desk, thus freeing herself, and ultimately, her daughter and Basil. Wu staggers to strike the gong to signal that Basil should be killed anyway, but a vision of Nang Ping appears, shaking her head and imploring her father with outstretched arms. Pursuing the ghostly image into the hall, Wu dies and is found by his old friend and tutor, who picks up Wus prayer beads and says "Thus passes the House of Wu."

==Background==
  New York on October 14, 1914 with stage star Walker Whiteside playing Wu.  The actor Frank Morgan was in the original Broadway cast, appearing under his original name Frank Wupperman.

The first actor to portray Mr. Wu (in the 1913 West End production) was Canadian-born English actor Matheson Lang, who became so popular in the role that he starred in a 1919 film version. (The better-known Lon Chaney production is therefore a remake.) Lang continued to play Asian roles (although not exclusively), and his autobiography was titled Mr. Wu Looks Back (1940).

In his film version, for the hundred-year-old look, Chaney build up his cheekbones and lips with cotton and rigid collodion. His nostrils were stuffed with cotton and the ends of cigar holders, and his long fingernails were constructed from strips of painted film stock. Fish skin was used to affect an epicanthal fold to his eyes and grey crepe hair was used to create his mustache and goatee. The make-up procedures took from four to six hours to apply.

Wu Li Chang, a Spanish-language version of Mr. Wu, was produced in 1930.

In 2000, Turner Classic Movies presented the television premiere with a music soundtrack composed, produced, edited and mixed by Maria Newman, who also conducted the Viklarbo Chamber Symphony.
 George Formbys panto comedies, which often rely on material familiar to the audience from previous productions, a perennially popular joke occurs when the comedian is confronted by a large fearsome villain of flamboyant appearance. Instead of being frightened, the comedian greets the villain familiarly: "Mister WU! How DO you DO?"

==References==
 

==References==
 

==External links==
*  
*  
*   (for the Spanish remake)

 

 
 
 
 
 
 
 