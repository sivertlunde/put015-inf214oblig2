Scooby-Doo! and the Beach Beastie
{{Infobox television film
| name           = Scooby-Doo! and the Beach Beastie
| image          = 
| caption        = 
| director       = 
| producer       = Jason Wyatt (line producer)  . LinkedIn.com. 
| previousfilm= Scooby-Doo! Ghastly Goals!
| writer         = 
| starring       =  
| music          = 
| cinematography = 
| editing        = 
| distributor    = Warner Home Video
| studio         = Warner Bros. Animation
| released       =  
| first_aired    = 
| runtime        = 22 minutes
| country        = United States
| language       = English
| budget         = 
}}
Scooby-Doo! and the Beach Beastie is the sixth Direct-to-video|direct-to-DVD special produced by Warner Bros. Animation, based upon the Scooby-Doo Saturday morning cartoons. It will be released on May 5, 2015, only through the Scooby-Doo! 13 Spooky Tales: Surfs Up Scooby-Doo DVD. 

==Plot==
 

==Cast== Fred Jones
* Matthew Lillard as Shaggy Rogers
* Mindy Cohn as Velma Dinkley
* Grey Griffin as Daphne Blake

==Trivia==
* Almost two years before its scheduled release date, Matthew Lillard made a post on Twitter and Instagram about it having his favourite line of Shaggy Rogers|Shaggys, with a shot of the line from the script itself. Other details visible on the script include a monster called "Aquazilla".
* Lillard also referred to it as Scooby-Doo! Beach Blanket Beastie, which seems to have been its name at the time.

==References==
 

 

 
 
 
 
 