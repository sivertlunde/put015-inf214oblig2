The Jazz Singer (1980 film)
{{Infobox film
| name = The Jazz Singer
| image = Jazz_singer.jpg
| caption = Theatrical release poster
| director =  
| producer = Jerry Leider Herbert Baker
| starring = Neil Diamond Laurence Olivier Lucie Arnaz Catlin Adams Franklyn Ajaye Paul Nicholas Richard Bennett Alan E. Lindgren
| cinematography = Isidore Mankofsky
| editing = Frank J. Urioste Maury Winetrobe
| studio = EMI Films
| distributor = Associated Film Distribution
| released =  
| runtime = 115 minutes
| country = United States
| language = English
| budget = 
| gross = $27,118,000
}} directed by Richard Fleischer and Sidney J. Furie.
 soundtrack was Love on Hello Again".

==Plot summary== synagogue of his imperious father Cantor Rabinovitch (Laurence Olivier). Yussel is married to his childhood sweetheart, Rivka (Catlin Adams), and has settled down to a life of religious devotion to the teaching of his faith. But on the side, he writes songs for a black singing group, and when a member of the quartet gets in trouble with the law, Yussel is forced to cover for him at one of their engagements by wearing blackface. The nightclub engagement (where the selection "You Baby" is performed) is a success, but a patron at the nightclub (an unknown Ernie Hudson, four years before his Ghostbusters role) notices that Yussels hands are not black and incites a riot. A fight ensues and the band is arrested. Cantor Rabinovitch comes to the jail to bail them out, but finds there is not a Yussel Rabinovitch there, only a Jess Robin. His son explains it is just a stage name he uses when performing. Cantor informs him that his singing voice was to be used for Gods purposes, not his own. Yussel/Jess decides to relent and do whatever his father wants...at least for now.
 Love on the Rocks". Rivka notices him writing and composing the song in his free time and senses that Yussel yearns for a bigger stage for his voice, but her values keep her grounded to the home life they have built together. Bubba calls from Los Angeles to inform him that Keith Lennox really loved "Love on the Rocks" and wants to record it, but they need Yussel to come out for two weeks to oversee the recording session. This is the opportunity he has been waiting for, but Yussels wife and father are opposed to him going. At a party at their synagogue, his father relents and bids him a tearful goodbye. In his professional identity as "Jess", he is met in L.A. by music agent Molly Bell (Lucie Arnaz). She takes him to the studio where Keith Lennox is recording, and Jess is shocked to find out that his ballad is being recorded as a hard rock song. During a break in recording, Jess asks the producer and Lennox if he can perform the song as intended. They allow him to do so and while recording, Molly realizes that Jesss performance is the proper way. Not easily swayed, Lennox dismisses the group--and eventually Molly as well.
 Mike Pasternak) new television special. Gibbs is not amused. He says he cannot book anyone sight unseen and promptly throws Molly out of his car. However, she persuades Eddie to come to a club where Jess has managed to obtain an engagement, thanks to Bubba who was working there as a waiter. Eddie watches for only a few moments and leaves. Jess thinks he has blown it. Molly tells him, "He hates loud music. You he loved! You open for Zany Gray next week." Meanwhile, back in New York City, Cantor Rabinovich confronts Rivka about Jess going to California and reminds her that her place is by her husbands side. If she travels to California, then maybe she can bring him home. She relents and goes.

On Jesss opening night, he begins with a small harmonica intro and is heckled by someone in the crowd who yells, "Is that all you can play, turkey?!?" But he endures and performs "Summer Love". The audience applauds and Jess decides to perform a song everyone can get in on, "Hey Louise". Rivka shows up and meets Molly. Rivka tries to explain that their Jewish values are so tightly adhered to that Jess cannot possibly stay. She also senses Mollys attraction to her husband. Molly tells Rivka that she is not the problem; the audience is. Jess ends his set and is given a standing ovation. He heads backstage and is reunited with Rivka. Molly makes herself scarce. At the aftershow party in Jesss dressing room, Jess is given a recording contract. However, he and Rivka have an argument outside his dressing room where his wife warns him that it is either her or his new life. He tries to convince her to stay, but Rivka feels she has lost him permanently and runs off.
 Hello Again" for her. His father comes to see Jess where he is living with Molly in Venice, California to get Jess his son to come back to New York where he belongs. Jess tells him he and Rivka will be divorced. They are interrupted when Molly enters and Jess introduces her. Anguished, his father then performs Bereavement in Judaism|keriah, the tearing of a piece of clothing over the heart, and says, "I have no son." Molly asks Jess why his father tore his clothing and Jess explains that the act is one of mourning someone who has just died. Molly asks, "Whos dead?" Bitterly, Jess replies, "I am."

In the recording studio, Jess, along with Bubba and the Four Brothers, are recording the selection "Jerusalem". But Jess, distracted by the incident with his father, takes his anger out on the band. During a break, Bubba finds out from Molly she is pregnant. He warns her not to tell Jess right now. The recording session goes from bad to worse. Jess grows so upset that he leaves the studio and drives off in anger. Bubba tries to follow, but Molly tells him to just let Jess work things out on his own. Jess keeps driving until his car runs out of fuel. He hitchhikes and winds up spending the next several months on the road. He ends up at a small country bar, where he is hired to sing. Bubba tracks him down and tells Jess of the birth of his son, Charlie Parker (Chaim) Rabinovitch. Jess returns to Venice Beach and to Molly. Hoping to revive Jesss career, Molly again sneaks into Eddies car. He is not easily swayed, even though Jesss debut album did go gold and "probably paid for this car", Molly reminds him. Eddie gives in and allows Jess to perform one number at "Zany Grays Autumn in New York" concert.

In New York City at the rehearsals, Jess is met by a friend of his fathers, Leo (Mike Kellin), who explains that the doctors refuse to allow Cantor Rabinovitch to sing the Kol Nidre on Yom Kippur due to his high blood pressure. He asks if Yussel could come to the service to be cantor, in hopes that it will breach the distance between father and son. Jess initially says no, but is convinced by Molly that he should. On Yom Kippur, a time for atonement, Jess steps in aside the synagogue cantor to sing the Kol Nidre. His father looks up in shock, but seems still hardened by past events. After the service, Jess confronts his father and shows him a picture of his grandson. With that, Cantor Rabinovitch forgives his son and the two embrace. At the concert, Jess opens the show with "America (Neil Diamond song)|America" accompanied by a full orchestra. He basks in the glow of a standing ovation he receives - including from Cantor Rabinovitch, who is sitting next to Molly.

This latest version changes the ending considerably by allowing Oliviers character to live rather than die of a broken heart, and allowing a genuine face-to-face reconciliation between Jess and his father.

==Cast==
*Neil Diamond as Yussel Rabinovitch/Jess Robin
*Laurence Olivier as Cantor Rabinovitch
*Lucie Arnaz as Molly Bell
*Catlin Adams as Rivka Rabinovitch
*Franklyn Ajaye as Bubba
*Paul Nicholas as Keith Lennox
*Sully Boyar as Eddie Gibbs
*Mike Kellin as Leo
*James Booth as Paul Rossini

==Reception==
===Box office===
Lew Grade who invested in the film said the box office "results were disappointing and we werent able to recoup our prints and advertising costs". However, since the movie had been presold to American television for $4 million the losses were minimized. Also, the album was very successful and made more money than the film itself. Lew Grade, Still Dancing: My Story, William Collins & Sons 1987 p 252 

===Critical===
Unlike the original, the film received mostly negative reviews. Based on 20 reviews,   called the appearance of Neil Diamond "the most cautious soft-rock superstar movie debut youll ever get to see." The only top critic to give a positive review of the film (according to Rotten Tomatoes) was Dave Kehr of the Chicago Reader. He wrote that "Richard Fleischers direction is appropriately close-in and small, and Diamond himself, while no actor, proves to be a commandingly intense, brooding presence." The film is listed in Golden Raspberry Award founder John J. B. Wilsons book The Official Razzie Movie Guide as one of the The 100 Most Enjoyably Bad Movies Ever Made. 

===Awards and nominations===
{| class="wikitable"
|-  style="background:#b0c4de; text-align:center;"
! Award
! Subject
! Nominee
! Result
|- Grammy Awards Best Album Neil Diamond, Richard Bennett and Doug Rhone
| 
|- American Society ASCAP Awards Most Performed Feature Film Standards Neil Diamond for "America (Neil Diamond song)|America"
| 
|- Golden Globe Awards Golden Globe Best Original Song Neil Diamond Love on the Rocks"
| 
|- Golden Globe Best Supporting Actress - Musical/Comedy Lucie Arnaz
| 
|- Golden Globe Best Actor - Musical/Comedy Neil Diamond
| 
|- Golden Raspberry Awards Golden Raspberry Worst Actor
| 
|- Golden Raspberry Worst Supporting Actor Laurence Olivier
| 
|- Golden Raspberry Worst Picture
|
| 
|- Golden Raspberry Worst Director Sidney J. Furie and Richard Fleischer
| 
|- Golden Raspberry Worst Original Song Neil Diamond for "You Baby"
| 
|-
|}

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 