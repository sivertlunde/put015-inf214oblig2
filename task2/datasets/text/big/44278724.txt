Courting Chaos
 
 
{{Infobox film
| name           = Courting Chaos
| image          = Courting_Chaos_film_poster.jpg
| caption        = VOD release poster
| director       = Alan Clay
| writer         = Alan Clay
| starring       = Rachelle DiMaria Alastair Bayardo Nancy la Scala Jessica Howell Tristan Cunnigham Jeff Bee
| cinematography = Dan Kneece
| editing        = Rachel Ann Pearl
| studio         = Artmedia
| released       =  
| runtime        = 124 minutes
| country        = United States
| language       = English
}}
Courting Chaos is a 2014 film written and directed by New Zealand filmmaker Alan Clay. The film is adapted from his book Angels Can Fly, a Modern Clown User Guide. The story is about a Beverly Hills girl who falls for a Venice Beach street clown called Chaos and she must overcome her inhibitions and become a clown herself for the relationship to survive.

Courting Chaos won the Best Comedy Film award at its premiere at the Hollywood Reel Independent Film Festival in February 2014 and went on to win the Special Jury Award for Romantic Comedy from WorldFest-Houston International Film Festival, one of the top awards from the oldest independent film festival in the world, where Lead actor, Rachelle DiMaria, was also nominated for Best Actor. The film also received Awards of Merit for Best Director and Best Feature Film from the Accolade Competition.

==Cast==
*Rachelle DiMaria as Ginger
*Alastair Bayardo as Chaos
*Nancy la Scala as Darlene
*Jessica Howell as Madonna
*Tristan Cunningham as Dancer
*Jeff Bee as TC

==Production==
 
Courting Chaos is notable for being shot and having finished the complete post production process, with a live music score, in just six weeks. This was done to capture the spontaneity inherent in the films subject matter and to achieve this, composer Sherri Chung was composing the films performance tracks and score from the script prior to the shoot and editor Ranchel Ann Pearl was working on set at Venice Beach from the second day. 

A team of 7 camera operators under DP Dan Kneece captured the street theatre scenes live, complete with real audience interactions and performer perspective shots from cameras mounted on the clowns. By the end of the 4 week shoot there was a rough cut of the film. One week later it was a fine cut and by the end of the following week, music was recorded and sound mixed. The result is a delightfully light and intimate story about the lead characters journey of liberation as she allows the spontaneity of becoming a clown in her life.

==Accolades==
* Best Comedy Film award at the Hollywood Reel Independent Film Festival in February 2014. 
* Special Jury Award for Romantic Comedy at Worldfest, Houston, 2014 where, Rachelle DiMaria, was also nominated for Best Actor. 
* Awards of Merit for Best Director and Best Feature Film from the Accolade Awards, 2014. 

==Release==
The film was released in the United States in 2014 to good reviews. 

== References ==
 

== External links ==
*  

 
 
 