In the Name of Buddha

{{Infobox film
| name           = In the Name of Buddha
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Rajesh Touchriver
| producer       =  K. Shanmughathas   Sai George
| writer         = Sai George
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = Jain Joseph   Raja Ratnam
| editing        = Ranjan Abraham   Sanjeev Mirajkar
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
}}
 Tamil doctor Siva. The film was screened in the Spotlight on India section at the 2003 Cannes Film Festival.  

==Synopsis==
Siva fled to Britain in 1993, flushing his false passport down the toilet during his flight. Its plot revolves around the conflict between minority Tamil rebels and the Sri Lankan army and Indian peacekeeping forces. The film portrays the agony of a people who only wanted peace of mind and a decent life as a protagonist puts it. The film upholds the value of compassion and abjures all forms of retribution.
 Oslo International Film Festival, the film proved controversial and garnered critical acclaim for its subject matter.  It also won the Best Foreign Film at the Beverly Hills Film Festival and the Best Film Award at the Newport Beach Film Festival
==Critical reception==
*“A Fine Balance”- India Today.  “A Courageous Treatise” – Empire.  “Terrific” – Film Review UK
“Very worthy” – BBCi Films. “Brave film” - The Guardian

==Crew==
{{columns-list|3|
*Screenplay & Direction - Rajesh Touchriver
*Producers - K. Shanmughathas & Sai George
*Executive Producers - Mariyamma Antony & Edison Lawrence
*Story - Sai George
*Cinematography - Jain Joseph & Raja Ratnam
*Editor - Ranjan Abraham & Sanjeev Mirajkar
*Production Design - Sunil Babu Music
*Background Score- Rajamani
*Lyrics - Raajaa Chandrashekhar
*Dialogue - Raajaa Chandrashekhar Elangai Tamil - Devakanthan Sinhalese - Sundara Pandi English - Ken Davenport & Sherry Roy
*Sound Effects - C Murukesh
*Audiography - Ajit Abraham George Stills - K.R. Vinayan
*Special Effects Make-up - N G Roshan
*Costumes - Ashokan Oliyakkal & Ullasan
*Production Executive - Praveen Flatmarket
}}
==References==
 

==External links==
* 
* 

 

 
 