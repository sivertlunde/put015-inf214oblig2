Raksha (film)
{{Infobox film
| name           = Raksha
| image          = Raksha.jpg
| image_size     = 
| caption        = Film Poster
| director       = Ravikant Nagaich
| producer       = P. Mallikharjuna Rao	
| starring       = Jeetendra Parveen Babi Prem Chopra Moushumi Chatterjee Suresh Oberoi
| music          = Rahul Dev Burman 
| released       = 5 Feb 1982
| country        = India
}}

Raksha ( ;   heads the cast with Parveen Babi, Prem Chopra, Moushumi Chatterjee and Suresh Oberoi, and also Agha (actor)|Agha, Birbal and Iftekhar.

==Cast==

*Parveen Babi as Chanda / Bijli
*Jeetendra as Goplakishan Pandey - Agent 116
*Moushumi Chatterjee as Asha
*Prem Chopra as Big Hardy
*Suresh Oberoi as Dr. Sinha
*Iftekhar as Chief of Secret Services
*Manik Irani as Francis
*Goga Kapoor as Khurana
*Satyendra Kapoor as Dr. Srivastava
*Narendra Nath as Jagat Baba
*Paintal as Goga
*Ranjeet as Daulatram
*Sajjan as Kedar
*Polson as Chef  Agha as Chef
*Birbal as Chef

== Soundtrack ==

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)!! Duration
|-
| "Tere Liye Mere Liye"
| Kishore Kumar
| 3.58
|-
| "Mil Gaye Dil Mil Gaye"
| Asha Bhosle & Kishore Kumar
| 5.08
|-
| "Jani Dilbar Jani"
| Asha Bhosle
| 6.26
|-
| "Main Chalta Hoon Mujhe Jane Do"
| Asha Bhosle & Mohd. Rafi
| 3.47
|-
| "Naye Purane Saal Me"
| Asha Bhosle & Kishore Kumar
| 6.03
|}



 