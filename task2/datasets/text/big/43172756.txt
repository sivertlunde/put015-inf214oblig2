Thaikku Thalaimagan
 
{{Infobox film
| name           = THAIKKU THALAIMAGAN
| image          = 
| caption        = 
| director       = M.A.Thirumugham 
| producer       = « Sandow » M.M.A.Chinnappa Devar
| writer         = « Sandow » M.M.A.Chinnappa Devar 
| story          = « Sandow » M.M.A.Chinnappa Devar 
| starring       = M. G. Ramachandran Jayalalitha S. V. Ranga Rao S. A. Asokan R. S. Manohar Nagesh
| music          = K. V. Mahadevan
| cinematography = N.S.Varma
| editing        = M.A.Thirumugham
| studio         = Devar Films
| distributor    = Devar Films
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Tamil
}} 1967 Tamil language drama film directed by M.A.Thirumugham.

The film features M. G. Ramachandran and S. A. Ashokan in lead roles.

==Plot==

Within a peaceful family garage, The Marudhachalam Murthi Automobiles, a drama is slowly but surely spirit to be formed.

Indeed, Somaiya (S. A. Asokan), the elder son of this small company aims, aspires to unreal perspectives.

Whereas Marudhu (MGR) the younger brother behaves in a way reflexive, of clearly more responsible.

But contrary to all expectations, their mother (S. N. Lakshmi) sides with her first-born, blinded by her love, she crosses him everything, while making feel guilty, ceaselessly, her youngest son.

As if it was not enough, Somaiya became infatuated with a beautiful bitchy girl, Nalini (Rajasree) who wants more in his money than in the beautiful eyes.

Another version

Because he cannot remain distant, too long, from his unique daughter, Maruthi (J. Jayalalitha), bride recently, with good Marudhu (M. G. Ramachandran) (younger son of modest one family), which holds with his older brother, Somaiya (S. A. Ashokan), a garage inherited from their deceased father, Maruthachalamurthi Automobiles, generous Pannaiyar Dharmalingham (S. V. Ranga Rao) develops then a plan with colorful sound driver Kuppu (Nagesh), in all innocence, without any defers without any back thoughts, to get closer to his child, as often as possible.

But that he (S. V. Ranga Rao) does not suspect, it is that he is going to break so, unintentionally, the happiness of the in-laws of Maruthi.

Indeed, Somaiya (S. A. Ashokan) , the elder brother of Marudhu (MGR), feeds for a long time desires for sizes and the fortune of Pannaiyar Dharmalingham (S. V. Ranga Rao) goes up to him to the head, gives him ideas.

Informed intentions of the disconsolate father, Somaiya takes advantage of its distress to propose him another kind of calculation which Dharmalingham hurries to accept, without guessing of the fact that he brews in reality.

Worse, Somaiya, some times later, falls, in his turn, in the claws of a couple of swindlers, venal Nalini (Rajasree) and fatal Ranga (R. S. Manohar).

In fact, Ranga quenches a vengeance. In the past, he received a monumental correction administered by Marudhu, to have tried to dupe his older brother.

Ranga had sworn itself, since then, to return blow for blow and so held his revenge.

The first one (Nalini), thus, took care to count him floweret, whereas the second (Ranga) robbed him shamelessly, enormous sums, which borrowed Somaiya with the father-in-law of Marudhu.

Somaiya turned away, consequently of his affectionate wife (Sowcar Janaki), but also her mother (S. N. Lakshmi) who crossed him everything, for years, whether he is the elder son, his preference.

The forfeiture of Somaiya was an announced column even inevitable in spite of the efforts deployed by his younger brother Marudhu to prevent him from sinking...

==Cast==
{| class="wikitable" width="100%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran || as Marudhu (alias Marudhur), the younger brother
|-
| S. V. Ranga Rao || as Cholaiya Pannaiyar Dharmalingham, Maruthis father
|-
| S. A. Ashokan || as Somu (alias Somaiya), the elder son
|-
| Nagesh || as Kuppu, Pannaiyar Dharmalinghams driver
|-
| R. S. Manohar || as Ranga, the building contractor (also a procurer)
|-
| Sandow M. M. A. Chinnappa Thevar || as a close friend of Meenakshis family, (the man splashed by Marudhus motorcycle, at 0.10.22)
|-
| J. Jayalalitha || as Maruthi
|-
| Sowcar Janaki || as Gauwri, Somaiyas wife
|-
| Manorama (Tamil actress)|Manorama|| as Munimma, Kuppus lover
|-
| S. N. Lakshmi || as Meenakshi, the mother of Somu and Marudhu
|-
| Baby R. Geetha || as Mani, Somaiyas son
|-
| Rajasree (Guest-star) || as Nalini
|-
|}

The casting is established according to the original order of the credits of opening of the movie, except those not mentioned

==Around the movie==

The film, produced by Sandow M. M. A. Chinnappa Thevar under Devar Films, had musical score by K. V. Mahadevan.

Thaikku Thalaimagan was almost darkened at its exit, on January 13, 1967, for Pongal of this year.

The day before the release of this film, MGR was assaulted at him by the actor M. R. Radha.
The film took very big opening due to the attack on mgr. 

 .

Sandow, the big Tamil storyteller, borrows the main weft to include its elements which make his trademark if characteristic of its film universe.

The voice of MGR is particularly very beautiful in this movie.

The last 15 minutes of this drama are very poignant !

Thaikku Thalaimagan marks the twelfth MGR-Chinnappa Devar association.

==Soundtrack==
The music composed by K. V. Mahadevan

==External links==
*  

 
 
 
 
 