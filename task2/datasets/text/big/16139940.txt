Kentucky Pride
 
 
{{Infobox film
| name           = Kentucky Pride
| image          = 
| caption        = 
| director       = John Ford
| producer       =  Elizabeth Pickett (titles)
| starring       = Henry B. Walthall Gertrude Astor Peaches Jackson
| music          = 
| cinematography = George Schneiderman
| editing        = 
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 70 minutes 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}} silent drama Fox Film about the life of a horse breeder and horse racing|racer, directed by the famed film director John Ford and starring Henry B. Walthall (who had previously played the Little Colonel in D. W. Griffiths controversial 1915 film The Birth of a Nation).  It is among Fords lesser-known works, but has been praised for sweetness and charm and its beautiful depiction of the life of horses and the relationship between the protagonist and his daughter.  Several well-known thoroughbred racehorses appear in the film, including the legendary Man o War.  

== Plot ==
The plot concerns Beaumont, a horse breeder with a penchant for gambling, who is down on his luck.  After losing at poker and being forced to give up several of his horses to cover his losses, Beaumont bets it all and loses again when his horse, Virginias Future, suddenly falls and breaks a leg while leading the pack in a critical race. Wollstein, Hans J., "Kentucky Pride (1925)", Rovi Corporation|Rovi, New York Times web site.  Accessed January 9, 2015.   Beaumonts selfish wife tells the horses trainer, Mike Donovan, to kill the injured horse, and abandons Beaumont for Greve Carter, a well-to-do neighbor. Beaumont also loses his relationship with Virginia,  his daughter from his previous marriage. Beaumont and Donovan manage to save Virginias Future, and she births a colt  (or a filly ) named Confederacy, but his financial troubles force him to sell off both the colt and the mare. Confederacy is mistreated by his new owner, a foreign junk dealer, and Virginias Future is forced into hard labor as a pack horse. But when Confederacy is later entered to run in the Kentucky Futurity|Futurity, ridden by Mike Donovans son Danny,   Beaumont gathers everything he can and bets it all again. This time he wins. He is reunited with his daughter and buys back the colt, giving it a good life in the pasture.  

== Critical reviews ==
The   web site, 2009. Accessed January 9, 2015.  Scott Eyman said "Kentucky Pride remains a shameless – shamelessly effective – film". 

== Film archive ==
A print of the film exists at the Museum of Modern Art film archive.      

==Cast==
The cast included the following actors
* Gertrude Astor as Mrs. Beaumont, the selfish second wife of Mr. Beaumont
* Peaches Jackson as Beaumonts daughter Virginia
* J. Farrell MacDonald as Mike Donovan, horse trainer
* Winston Miller as Mike Donovans son Danny
* Belle Stoddard as Mrs. Donovan
* Malcolm Waite as the neighbor Greve Carter
* Henry B. Walthall as Mr. Beaumont, the protagonist horse breeder

Several notable horses appeared in the film, including " " at Silentera.com. 
* Man o War, widely considered one of the greatest racehorses of all time, winner of the Preakness Stakes and Belmont Stakes and many other prominent races (not entered in the Kentucky Derby) Fair Play, Mad Hatter, Chance Shot, Fairmount
* Vito
* Zev and Flying Ebony
* Morvich, winner of the 1922 Kentucky Derby (the first California-bred racehorse to win the Derby)

==Other credits==
* John Ford, director William Fox, producer
* Edward OFearna, assistant director
* George Schneiderman, cinematographer
* Dorothy Yost, screenwriter Elizabeth Pickett, editing and titles

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 