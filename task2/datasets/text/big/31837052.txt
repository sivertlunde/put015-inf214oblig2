Mr. Nookayya
{{Infobox film
| name           = Mr. Nookayya
| image          = Mr Nookayya.JPG
| caption        = 
| director       = Ani Kanneganti
| producer       = D. S. Rao
| story          = Ani Kanneganti
| screenplay     = Ani Kanneganti
| writer         = Paruchuri Brothers  (dialogues) 
| starring       = Manoj Manchu Kriti Kharbanda Sana Khan
| music          = Yuvan Shankar Raja Chinna  (Film score|BGM) 
| cinematography = B. Rajasekar
| editing        = 
| studio         = Shri Shailendra Cinemas
| distributor    = 
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
Mr. Nookayya (previously known as Mr. Nokia and Mr. No Keyia) is a 2012 Indian Telugu language|Telugu-action film written and directed by Ani Kanneganti, produced by D. S. Rao under Shri Shailendra Cinemas banner and starring Manoj Manchu, Kriti Kharbanda and Sana Khan in lead roles. The film score and soundtrack is composed by Yuvan Shankar Raja, while cinematography is handled by B. Rajasekar. Dialogues of the film were written by popular writer duo Paruchuri Brothers and screenplay was handled by Ani Kanneganti. The film was released worldwide on 8 March 2012.  Upon release, the film ran into trouble as the telecommunications company Nokia filed a suit against the use of its registered trademark.   The film was then re-edited and re-released nine days later as Mr. Nookayya Reloaded.  

==Plot==
Nookayya (Manoj Manchu), who calls himself Nokia, is an expert cell phone thief. He has a good heart though and together with his friends Nampally (Paruchuri Venkateswara Rao) and Charger (Vennela Kishore), he takes care of orphans and abandoned kids. He is deeply in love with Shilpa (Sana Khan), who is a waitress in a pub. Shilpa wants Nokia to settle down in life with a nice car, lots of cash and a house in order to marry. A desperate Nokia starts hunting for ways to achieve these things in life.

On a separate note, Anu (Kriti Kharbanda) and Kiran (Raja Abel) are a newly married couple. On a trip to Bangalore, Kiran is kidnapped by a gang headed by Shajahan Bismil (Murali Sharma) and they demand a ransom of 2 Crores. In a curious twist of fate, the paths of both Nokia and Anu cross. They team up to save themselves and Kiran. How they are betrayed by the people they trust forms the rest of the story.

==Cast==
* Manoj Manchu as Nookayya / Nokia
* Kriti Kharbanda as Anuradha
* Sana Khan as Shilpa
* Raja Abel as Kiran
* Murali Sharma as Shajahan Bismil
* Brahmanandam as Rahul
* Raghubabu
* Vennela Kishore as Charger
* Paruchuri Venkateswara Rao as Nampally
* Ahuti Prasad
* Bhavana
* Allari Subhashini

==Production== muhurat was Hyderabad and shooting began.  The chief guest V. V. Vinayak tapped the clapperboard while Ravindranath switched on the camera.   On 25 April 2011, the title Mr. Nokia was registered for the film at the Film Chamber.  Manoj Manchu claimed that the director and producer had planned for 75 days film shooting, but since they were following a bound script, the shooting schedule was reduced to 45 days.  Producer of the film, D.S. Rao announced that the title of the film was changed from Mr. Nokia to Mr. Nookayya due to undisclosed reasons.    A reason to this is that the Finnish multinational communications corporation Nokia Communications contacted the producers concerning the use of their registered trademark in the films name and got a court order that forbade the producers to use the title Nokia or identical or deceptively similar title.   

===Casting=== Pranitha of Baava (film)|Baava fame was reported to play another lead female character.  In April 2011, Shriya walked out of the project and the producer roped in Kriti Kharbanda, who shot into fame after the release of Theenmaar.  According to sources, the producers dropped her on the third day of the shoot,  since she "threw starry tantrums".  Shriya however clarified that she walked out of the project as the negotiations on remuneration had failed.  Pranitha too was later replaced by Sana Khan. 

===Filming===
Major part of the filming took place in Hyderabad. In June 2011, the climax action sequences along with few important scenes on Manoj and Raja   were canned in Bangkok. In October 2011, Producer D.S. Rao announced that filming of the talkie part was completed with three songs left to be shot.   In November 2011, final schedule of filming started with 3 songs being shot in Hyderabad.   On 25 December 2011, filming of the song "No. Keyia" began in Ramoji Film City and was completed there by bringing an end to the filming part of the film.  

==Soundtrack==

{{Infobox album
| Name = Mr. Nookayya
| Longtype = to Mr. No Keyia
| Type = Soundtrack
| Artist = Yuvan Shankar Raja
| Cover = Mr No Keyia audio cover.jpg
| Released = 19 January 2012
| Recorded = 2011 Feature film soundtrack
| Length = 32:19 Telugu
| Label = Aditya Music
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album = Rajapattai (2011)
| This album = Mr. Nookayya (2012)
| Next album = Billa II (2012)
}}

The soundtrack of the film was composed by Yuvan Shankar Raja, which happens to be his second Manoj Manchu project after Raju Bhai.  During the launch of the film, Yuvan Shankar had already tuned two of the songs,  while the lead actor Manoj Manchu has also written lyrics for one of the songs.  The album consists of seven songs from Yuvan Shankar Rajas earlier Tamil releases, with varied orchestrations. The soundtrack album was released on 19 January 2012 under its tentative title Mr. No Keyia at Rock Heights, HITEC City, Madhapur in Hyderabad, India|Hyderabad,   with several prominent film personalities being present. 

{{Tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 32:19
| writing_credits = 
| lyrics_credits  = yes
| music_credits   = 
| title1          = No. Keyia
| note1           = Who Am I from Vaanam
| lyrics1         = Ramajogayya Shastry Ranjith
| length1         = 03:59
| title2          = Oke Oka Jeevitham
| note2           = Oru Naalil from Pudhupettai
| lyrics2         = Ramajogayya Shastry
| extra2          = Haricharan 
| length2         = 06:04
| title3          = Pista Pista
| note3           = Evandi Unna Pethan from Vaanam
| lyrics3         = Manoj Manchu
| extra3          = Karthik (singer)|Karthik, Yuvan Shankar Raja
| length3         = 05:42
| title4          = Pranam Poye Badha  Kalvanin Kadhali
| lyrics4         = Manoj Manchu
| extra4          = Yuvan Shankar Raja
| length4         = 02:44
| title5          = No Money No Honey
| note5           = No Money No Honey from Vaanam
| lyrics5         = Ramajogayya Shastry
| extra5          = Karthik, Premgi Amaren
| length5         = 04:49
| title6          = Ye Janma Bandhamo
| note6           = Nenjodu song from Kaadhal Kondein
| lyrics6         = Lakshmi Bhupal
| extra6          = Ranjith, Priya Hemesh
| length6         = 05:29
| title7          = Theme of No. Keyia 
| note7           = "Cable Raja" Theme from Vaanam
| lyrics7         = Lakshmi Bhupal
| extra7          = Blaaze
| length7         = 03:30
}}

==References==
 

 
 
 
 
 