The Silent War (film)
 
{{Infobox film
| name           = The Silent War
| image          = The Silent War film poster.jpg
| alt            = 
| caption        = 
| film name = {{Film name| traditional    = 聽風者
| simplified     = 听风者}}
| director       = Alan Mak Felix Chong
| producer       = Ronald Wong Charley Zhuo
| writer         = Alan Mak Felix Chong
| based on       =  
| starring       = Tony Leung Chiu-Wai Zhou Xun Mavis Fan Wang Xue Bing
| music          = Chan Kwong-wing
| cinematography = Fletcher Poon
| editing        = Curran Pang
| studio         = Mei Ah Film Production Pop Movies
| distributor    = Mei Ah Entertainment
| released       =  
| runtime        = 120 minutes
| country        = China Hong Kong Mandarin Cantonese
| budget         = 
| gross          = 
}}
The Silent War is a 2012 Chinese-Hong Kong thriller film directed by Alan Mak and Felix Chong.   

The film won Best Cinematography at the 32nd Hong Kong Film Award. 

==Cast==
*Tony Leung Chiu-Wai as He Bing 
*Zhou Xun as Zhang Xue Ning
*Mavis Fan as  Shen Jing 
*Wang Xue Bing as Guo Xingzhong
*Dong Yong as Wu Chang
*Carrie Ng as Mrs Li
*Pal Sinn as Luo Saner
*Lam Wai as Captain Yang
*Tang Qun as Mother He
*Jacob Cheung as Sparrow
*Zhang Haiyan as Mrs. Ma

==Awards and Nominations==
32nd Hong Kong Film Awards
* Nominated: Best Screenplay (Alan Mak & Felix Chong)
* Nominated: Best Actor (Tony Leung Chiu-Wai)
* Nominated: Best Actress (Zhou Xun)
* Nominated: Best Supporting Actress (Mavis Fan)
* Won: Best Cinematography (Anthony Pun Yiu Ming)
* Nominated: Best Art Direction (Man Lim Chung)
* Nominated: Best Costume & Make Up Design (Man Lim Chung)
* Nominated: Best Original Film Score (Chan Kwong Wing)

Golden Horse Awards
* Nominated: Best Art Direction (Man Lim Chung)
* Nominated: Best Makeup & Costume Design (Man Lim Chung)
* Nominated: Best Sound Effects (Traithep Wongpaiboon, Nopawat Likitwong)
* Nominated: Best Supporting Actress (Mavis Fan)

Asian Film Awards
* Won: Best Costume Designer (Man Lim Chung)
* Nominated: Best Production Designer (Man Lim Chung)

Asia Pacific Film Awards
* Won: Best Sound Effects (Traithep Wongpaiboon, Nopawat Likitwong)

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 

 
 