The Swarm (film)
 
{{Infobox film
| name = The Swarm
| image = The Swarm.jpg
| alt =
| caption = Theatrical release poster
| director = Irwin Allen
| producer = Irwin Allen
| screenplay = Stirling Silliphant
| based on =   Richard Chamberlain Ben Johnson Cameron Mitchell
| music = Jerry Goldsmith
| cinematography = Fred J. Koenkamp
| editing = Harold F. Kress
| distributor = Warner Bros.
| released =  
| runtime = 116 minutes 156 minutes (extended cut)
| country = United States
| language = English
| budget = $21 million Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 150-151  or $11.5 million THE OVERSEAS CONNECTION: TAKING STARS TO MARKET
Wilson, John M. Los Angeles Times (1923-Current File)   18 Mar 1979: o3.  
| gross = $7.6 million Richard Nowell, Blood Money: A History of the First Teen Slasher Film Cycle Continuum, 2011 p 257 
}} monster horror killer bee invasion of Texas. It was adapted from a novel of the same name by Arthur Herzog.
 Richard Chamberlain, Ben Johnson, edited by Harold F. Kress.

==Plot==
A group of soldiers led by Maj. Baker (Bradford Dillman) is ordered to investigate a basement level station which they believed was attacked. After Baker contacts his commander, Gen. Slater (Richard Widmark), they begin to investigate who drove a civilian van into the base. It is revealed to be owned by a scientist named Dr. Bradford Crane (Michael Caine), the only survivor of the attack. Slater orders two helicopters to check for a black mass (revealed to be bees), but the two helicopters are swarmed by the bees and crash, killing the pilots inside. Crane insists to Slater that the base was attacked by the same African killer bees that destroyed the helicopters. Helena Anderson (Katharine Ross), one of the bases doctors, supports Cranes story.
 Marysville town square, where the citizens are preparing for the annual flower festival. The boy is brought into the hands of military personnel, where he hallucinates a vision of giant bees attacking him, due to the aftereffects of the bee sting. Wheelchair bound Dr. Walter Krim (Henry Fonda) confirms to Crane that the very war they have feared for a long time has started against the bees. At the gates of the base, Slater must confront angry country bumpkin Jed Hawkins (Slim Pickens) who demands to see the dead body of his son, who was killed by the bees. Hawkins takes the body bag and departs, leaving the entire watching crowd silent over the loss. Slater suggests airdropping poison on the swarm, but Crane considers the ecological possibilities of the situation.
 Marysville and Ben Johnson), and town Mayor Clarence Tuttle (Fred MacMurray), who also runs the towns drug store.
 Marysville dies after once more visiting the hospital, which sends Helena into a rage about why the children have to die. Dr. Krim self-injects an experimental bee venom antidote to keep track of the results, although the trial proves fatal, and Krim dies from the effects of the venom. Meanwhile, nuclear power plant manager Dr. Andrews (Jose Ferrer) is convinced that his plant can withstand the attacks of the bees, ignoring the warnings of Dr. Hubbard (Richard Chamberlain). However, at that moment, the alarm sounds and the bees invade the plant, killing both Andrews and Hubbard, as well as completely destroying the plant and wiping out an entire town.

Crane and Slater analyze tapes from the original bee invasion and come to the possible conclusion that their alarm system attracted the swarm into the base. The bees invade once more, so Slater uses a flame-thrower to allow Crane and Helena to escape. Sonically altered helicopters successfully manage to lure the bees out to sea, where they douse the water with oil and set the swarm ablaze. Crane wonders if their victory was overall successful or just temporary, then decides that "if we use our time wisely, the world just might survive."

==Cast==
{{columns-list|2|
* Michael Caine as Dr. Bradford Crane
* Katharine Ross as Helena Anderson
* Richard Widmark as General Thaddeus Slater Richard Chamberlain as Dr. Hubbard
* Olivia de Havilland as Maureen Schuester Ben Johnson as Felix Austin
* Lee Grant as Anne MacGregor Jose Ferrer as Dr. Andrews
* Patty Duke as Rita
* Slim Pickens as Jud Hawkins
* Bradford Dillman as Major Baker
* Fred MacMurray as Clarence Tuttle
* Henry Fonda as Dr. Walter Krim Cameron Mitchell as General Thompson
* Morgan Paull as Dr. Newman
* Alejandro Rey as Dr. Martinez
* Don "Red" Barry as Pete Harris}}

==Reception== Time Out magazine called The Swarm a "risibly inadequate disaster movie".  Leslie Halliwell called
The Swarm a "very obvious disaster movie with risible dialogue", and suggested its commercial failure was partly due
to the fact that prior to its release, several American television movies with similar plots had been broadcast.  Wilmington Morning Star stated "The Swarm may not be the worst movie ever made. Id have to see them all to be sure. Its certainly as bad as any Ive seen." Velt also stated "All the actors involved in this fiasco should
be ashamed".  
""Swarm" Not Recommended". July 21, 1978. Retrieved April 6 2014. 
 The Magus and his later film Ashanti (1979 film)|Ashanti): "It wasnt just me, Hank Fonda was in it too, but I got the blame for it" he claimed in an interview with Michael Parkinson. 

==Score== musical score was composed by Academy Award winner Jerry Goldsmith and used French horns and such to sound like the humming of bees.

The score was originally released on LP on Warner Bros. Records in 1978 at the same time of the films release, but has long since gone out of print. An expanded, remastered score was released in 2002 in a limited edition by Prometheus Records and contained over 40 minutes of previously unreleased material. It has also gone out of print.

==Alternate versions==
The film was released initially at 116 minutes. When released on laserdisc in the 1980s, it was expanded to 156 minutes, and this version is what is used on the DVD release.

==Remake==
The film is currently in negotiations for a remake. Roy Lee of Vertigo Entertainment is producing. 

==See also== List of killer bee films

==References==
 

==External links==
 
*  
*  
*  
*  
*   (The Irwin Allen News Networks Swarm page)
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 