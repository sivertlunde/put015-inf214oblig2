Sky Commando
 {{Infobox film
| name           = Sky Commando
| image          = Sky Commando.jpg
| caption        = Theatrical poster
| director       = Fred F. Sears
| producer       = Sam Katzman
| writer         = screenplay by Samuel Newman story by Samuel Newman, William Sackheim, Arthur Orlof
| narrator       =  Mike Connors as Touch Conners
| music          = Ross DiMaggio as Ross Di Maggio
| cinematography = Lester H. White
| editing        = Edwin Bryant
| distributor    = Columbia Pictures
| released       = August 21, 1953
| runtime        = 67/69 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 American adventure film released by Columbia Pictures, directed by Fred F. Sears and starring Dan Duryea, Frances Gifford and Mike Connors (credited as "Touch Conners").  The Cold War period provides the background, although the plot concerns a flashback to World War II aerial action.  

Sky Commando was the last major film feature for Frances Gifford as her postwar career was affected by serious personal problems. The film was one of Connors first, having made his debut in Sudden Fear (1952); he had previously appeared in Sears The 49th Man, earlier in 1953.

==Plot== William Bryant) Michael Fox) stops him and relates a story about Wyatts career during World War II.
 8th Air Force, Wyatt commanded a bomber group whose reconnaissance missions provided valuable information for future bombing raids. War correspondent Jo McWethy (Frances Gifford) who was assigned to cover Wyatts group, wanted to know the truth about his reputation for being a hard-driving and unsympathetic commanding officer. His pilots held Wyatt responsible for the death of his co-pilot during a dangerous mission and new co-pilot, Lt. Hobson "Hobbie" Lee (Mike Connors), becomes the replacement. During a "milk-run", Wyatt changed the mission to photograph heavily-defended Bremen. German anti-aircraft batteries shot down several fighter escorts and badly damaged Wyatts aircraft, killing three of his crew. Nursing the stricken bomber back home, Wyatt made the decision to dump everything to save the aircraft and its important film. Hobbie and waist-gunner Danny Nelson (Freeman Morse) had to ditch the bodies of the dead crew and parachute out. Wyatt then flew back with the injured navigator aboard. 
 North Africa, Romanian oil fields, Wyatts bomber was shot down. Although five of the crew were saved, gunner Danny Nelson was killed. With the aid of partisans, Hobbie reconciled with the badly wounded Wyatt, and managed to convey the crucial roll of film safely back to England.

When Scott concludes his story, Lt. Willard realizes that Wyatt does care for his men. A wire the next day informs him that his brother is still alive and has been rescued.

==Cast==
As credited, with screen roles identified: 
* Dan Duryea as Colonel Ed (E.D.) Wyatt
* Frances Gifford as Jo McWethy  
* Mike Connors as Lieutenant Hobson "Hobbie" Lee (credited as "Touch Conners") Michael Fox as Major Scott William Bryant as Lieutenant John "Johnny" Willand (listed as Will R. Klein) 
* Freeman Morse as Danny Nelson
* Dick Paxton as Captain Frank Willard
* Selmer Jackson as General Carson
* Dick Lerner as "Jorgy"
* Morris Ankrum as General W.R. Combs

Notable actors in uncredited roles include Tommy Farrell as a pilot, Bert Stevens as an air force officer and Paul McGuire as Major Daly, albeit uncredited.

 

==Production== USAF and USAAF action. Lockheed F-80 Shooting Star footage was, at times, jarringly spliced together, with a view of a bomber changing dramatically to a different aircraft. Most of the action in the cockpit between Conners and Duryea was filmed in a B-25 bomber cockpit. 

==Reception==
Like most of Fred F. Sears work, with its poor production values and stagey plot, Sky Commando  was not well received.  Reviewer Hal Erickson, in a later day critique, observed that the film was notable in presenting Duryea in a sympathetic role and former headliner Frances Gifford appearing in the film. 

==References==
===Notes===
 

===Bibliography===
  
* Carlson, Mark. Flying on Film: A Century of Aviation in the Movies, 1912–2012. Duncan, Oklahoma: BearManor Media, 2012. ISBN 978-1-59393-219-0.
* Orriss, Bruce. When Hollywood Ruled the Skies: The Aviation Film Classics of World War II. Hawthorne, California: Aero Associates Inc., 1984. ISBN 0-9613088-0-X.

* Wheeler, Winston Dixon. Lost in the Fifties: Recovering Phantom Hollywood. Carbondale, Illinois: Southern Illinois University Press, 2005. ISBN  978-0-80932-653-2
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 