The Lost Chord (1925 film)
{{Infobox film
| name           = The Lost Chord
| image          =
| caption        =
| director       = Wilfred Noy
| producer       = 
| writer         = Wilfred Noy 
| starring       = David Powell   Alice Lake   Dagmar Godowsky
| music          = 
| cinematography = 
| editing        = 
| studio         = Chord Pictures 
| distributor    = Arrow Film Corporation
| released       = January 15, 1925
| runtime        = 
| country        = United States
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama made the film in Britain and this remake marked his American debut.

==Cast==
*   David Powell as Arnold Grahme  
* Alice Lake as Countess Zara  
* Dagmar Godowsky as Pauline Zara  
* Henry Sedley as Count Zara  
* Faire Binney as Joan  
* Louise Carter as Phyllis  
* Charles W. Mack as Jack Brown 
* Dorothy Kingdon as Helene Brown  
* Samuel E. Hines as Arthur Ames  
* Signor N. Salerno as Levina  
* Rita Maurice as Baby Joan  

==References==
 

==Bibliography==
* Munden, Kenneth White. The American Film Institute Catalog of Motion Pictures Produced in the United States, Part 1. University of California Press, 1997.

==External links==
*  

 

 
 
 
 
 
 
 
 

 