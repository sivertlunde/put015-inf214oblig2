Pranayamay
{{Infobox film
| name           =Pranayamay
| image          = 
| image size     =
| caption        =
| director       =Trivikram Srinivas
| producer       =Sravanthi Ravi Kishore
| writer         = Trivikram Srinivas
| narrator       =
| starring       = Shreya, Tarun, Prakash Raj
| music          = 
| cinematography = 
| editing        =
| distributor    = 
| released       = 2004
| runtime        =
| country        = India Malayalam
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
Pranayamay is a Malayalam language film. It was released in 2004.  It is Malayalam dubbed version of Telugu film Nuvve Nuvve, directed by ace director Trivikram Srinivas. It is first movie of Trivikram as a director.

==Plot==
Vishwanath (Prakash Raj) is a millionaire whose daughter is Anjali (Shriya Saran). Vishwanath loves his daughter very much. So much that he even buys her an Icecream Parlor when his daughter asks him for an ice cream. What Vishwanath expects is a son-in-law who will never oppose him in any matter. Rishi (Tarun) hails from a middle-class family. His father, played by Chandramohan, owns a department store. He is a good-hearted but happy-go-lucky guy. Anjali happens to join the college in which Rishi is a senior. Soon, they fall in love with each other. On one occasion, Rishi takes Anjali to Mumbai for dinner. Anjali tries to shake off the issue with her father, saying that she was with one of her friends. But unfortunately, Vishwanath calls that friend up when Anjali was missing. He tells him that Anjali went to Mumbai with someone called Rishi. This is enough for Vishwanaths suspicion to rise that Anjali has fallen in love. He asks Rishi to prove his worth and asks him in some way to earn any money so that he can support Anjali, who he has brought up in riches. When Rishi refuses to comply, he gives Rishi one crore rupees (approximately $185,000 USD) to forget his daughter. Rishi takes it and then later gives it back, insulting Viswanath, and tries to prove that his love is greater than everything and money cant buy his love. To retaliate, Vishwanath insults Rishis family by getting a beggar for marriage with Rishis sister to try to convey to them the pain he must feel. Rishi then creates a scene in Viswanaths office beating up security guy and destroying computers in the office, Rishi clarifies that in the proposal that beggar be married to Rishis sister there is no love between the two, but Rishi and Anjali love each other. Anjali starts to show more feelings for Rishi. Worried, Vishwanath tries to set Anjalis marriage with another guy. Anjali meets Rishi at his home and requests him to marry her right at the moment, but Rishi convinces Anjali that this is not the right way. Rishi takes her back to her home, there Rishi challenges Vishwanath that if it is correct that Anjali getting married to some one else then that marriage cant be stopped, but if it is wrong then that marriage cannot happen. In the end, the marriage fails and Anjali marries Rishi..

==References==
 

 
 
 


 