The Great Impersonation (1935 film)
{{Infobox film
| name           = The Great Impersonation
| image          =
| caption        =
| director       = Alan Crosland
| producer       = Edmund Grainger
| writer         = E. Phillips Oppenheim (novel) Eve Greene Frank Wead
| narrator       =
| starring       = Edmund Lowe Valerie Hobson Wera Engels Murray Kinnell
| music          = Heinz Roemheld Clifford Vaughan
| editing        = Philip Cahn
| cinematography = Milton R. Krasner
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} American drama The Great Universal Horror 1942 respectively.

==Plot==
Before the First World War, Sir Everard Dominey, a drunken upper-class Englishman, encounters an old acquaintance the sinister German arms dealer Baron Leopold von Ragostein in Africa. The two men are identical, and von Ragostein plans to kill his doppelganger and take his place in British high society where he will be able to further his arms business and spy on Britain for the German Empire. He arranges the murder with his various associates.

When "Dominey" returns to London shortly afterwards, he encounters the German aristocrat Stephanie Elderstrom who is certain she recognises him as her former lover, von Ragostein. von Ragosteins associates attempt to buy her off but she remains convinced something untoward is going on. When he reaches Donimey Hall, Domineys wife is equally certain that it is her genuine husband returning from Africa at long-last. Gradually, doubts begin to emerge whether it is the real Dominey who has come home.

==Cast==
* Edmund Lowe as Sir Everard Dominey/Baron Leopold von Ragostein 
* Valerie Hobson as Eleanor Dominey 
* Wera Engels as Princess Stephanie Elderstrom 
* Murray Kinnell as Seaman 
* Henry Mollison as Eddie Pelham 
* Esther Dale as Mrs. Unthank 
* Brandon Hurst as Middleton 
* Ivan F. Simpson as Doctor Harrison 
* Spring Byington as Duchess Caroline 
* Lumsden Hare as Duke Henry 
* Charles Waldron as Sir Ivan Brunn 
* Leonard Mudie as Mangan  Claude King as Sir Gerald Hume 
* Frank Reicher as Doctor Trenk  Harry Allen as Perkins
* Lowden Adams as Waiter  Frank Benson as English Farmer 
* Robert Bolder as Villager 
* Willy Castello as Duval  Edward Cooper as Butler  David Dunbar as English Farmer 
* Dwight Frye as Roger Unthank 
* Nan Grey as Middletons Daughter  
* Virginia Hammond as Lady Hume 
* Henry Kolker as Doctor Schmidt 
* Priscilla Lawson as Maid 
* Adolph Milar as German 
* Thomas R. Mills as Bartender  Pat OHara as Chauffeur  John Powers as Policeman 
* Tom Ricketts as Villager 
* Violet Seaton as Nurse 
* Leonid Snegoff as Wolff 
* Larry Steers as Army Officer  
* Frank Terry as Villager  Douglas Wood as Nobleman   Harry Worth as Hugo

==References==
 

==Bibliography==
* Weaver, Tom & Brunas, Michael & Brunas, John. Universal Horrors: The Studios Classic Films, 1931-1946. McFarland & Company, 2007.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 