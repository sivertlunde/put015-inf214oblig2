November Rain (2007 film)
{{Infobox film
| name           = November Rain
| image          =
| image_size     =
| caption        =
| director       = Vinu Joseph
| producer       =
| writer         = Vinu Joseph
| screenplay     = Santhosh Echikkanam Arun Sajeevan Geetha
| music          = Anup S Nair
| cinematography = Viswamangal Kitsu
| editing        = B Ajith Kumar
| studio         = Touch Screen Corporation
| distributor    = Touch Screen Corporation
| released       =  
| country        = India Malayalam
}}
 2007 Cinema Indian Malayalam Malayalam film, Geetha in lead roles. The film had musical score by Anup S Nair.   

==Cast==
  Arun
*Sajeevan
*Aniyappan Geetha
*Lalu Alex
*Nimisha Suresh
*Niyaz Musaliyar
*Reji Nair
*Sadiq
*Sajitha Beti
*Samad
*Spadikam George
*Sreejith Ravi
*Vinod Kovoor
 

==Soundtrack==
The music was composed by Anup S Nair.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aarumaarum || Sreenivas || Brajesh Ramachandran, Sachithanandan Puzhankara, Suresh ||
|-
| 2 || Aarumaarum || Jyotsna, Sreenivas || Brajesh Ramachandran, Sachithanandan Puzhankara, Suresh ||
|-
| 3 || Bad company || Anup S Nair || Brajesh Ramachandran, Sachithanandan Puzhankara, Suresh ||
|-
| 4 || Chembakappoo || G. Venugopal || Brajesh Ramachandran, Sachithanandan Puzhankara, Suresh ||
|-
| 5 || Chembakappoo   || Sujatha Mohan || Sachithanandan Puzhankara ||
|-
| 6 || Dham || Jyotsna, Franco || Brajesh Ramachandran, Sachithanandan Puzhankara, Suresh ||
|-
| 7 || Gaanamayidaam || Tippu || Brajesh Ramachandran, Sachithanandan Puzhankara, Suresh ||
|-
| 8 || November rain || Brajesh Ramachandran, Sachithanandan Puzhankara, Suresh || Brajesh Ramachandran, Sachithanandan Puzhankara, Suresh ||
|-
| 9 || Raavin nenjil || Balu, Arup. S. Nair || Brajesh Ramachandran, Sachithanandan Puzhankara, Suresh ||
|}

==References==
 

==External links==
*  

 
 
 

 