Four Wives
{{Infobox film
| name           = Four Wives
| image          = Priscilla Lane in Four Daughters trailer.jpg
| image size     = 
| caption        = Priscilla Lane as Anne Lemp Deitz
| director       = Michael Curtiz
| producer       = 
| writer         =  
| narrator       =  Rosemary Lane Claude Rains Eddie Albert John Garfield
| music          = Max Steiner
| cinematography = Sol Polito
| editing        = Ralph Dawson
| distributor    = Warner Bros.
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Four Wives  is a 1939 film starring Priscilla Lane and two of her sisters, features Gale Page, Claude Rains, Eddie Albert, and John Garfield, and was directed by Michael Curtiz. The movie is a sequel to Four Daughters (1938), and was followed by Four Mothers (1941).

==Cast==
 
*Priscilla Lane as Ann Lemp Dietz Rosemary Lane as Kay Lemp
*Lola Lane as Thea Lemp Crowley
*Gale Page as Emma Lemp Talbot
*Claude Rains as Adam Lemp
*Jeffrey Lynn as Felix Dietz
*Eddie Albert as Clint Forrest, Jr.
*May Robson as Aunt Etta
*Frank McHugh as Ben Crowley
*Dick Foran as Ernest Talbot
*Henry ONeill as Clinton Forrest, Sr.
*John Garfield as Mickey Borden
*Vera Lewis as Mrs. Ridgefield
*John Qualen as Frank

==External links==
* 
* 

 

 
 
 
 
 
 
 