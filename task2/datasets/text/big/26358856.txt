Judaai (1980 film)
{{Infobox film
| name = Judaai
| image =Judaai80.jpg
| image_size =
| caption = DVD cover
| director = Tatineni Rama Rao
| producer = A.V. Subba Rao
| writer = Rahi Masoom Reza
| screenplay = Tatineni Rama Rao
| story    = M. Balamurugan
| dialogue = Rahi Masoom Reza
| narrator =
| starring =Ashok Kumar Jeetendra Rekha
| music = Laxmikant-Pyarelal Anand Bakshi   (Lyrics)  
| cinematography =
| editing = V. Balasubramaniam J. Krishnaswamy
| studio  = Annapoorna Studios
| distributor = Prasad Art Pictures
| released = 
| runtime =
| country = India
| language = Hindi
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
 Indian Bollywood|Hindi film directed by T. Rama Rao, released on 26 September 1980. The film stars Ashok Kumar, Rekha and Jeetendra in lead roles.

==Plot==
Retired and widowed judge Umakant Verma (Ashok Kumar) lives a wealthy lifestyle with his only son, Dr. Shashikant (Jeetendra). Gauri (Rekha), the daughter of Umakants childhood friend, Narayan Singh (A.K. Hangal), is the lead servant in the house, but she has never been treated as a servant but rather as a family member. When Umakant makes his last will and testament, he wants Shashikant to marry Gauri. And indeed, after Umakants death, Shashikant and Gauri get married. Soon Gauri gives birth to a boy, Ravikant (Sachin (actor)|Sachin), and a few months later gets pregnant again. However, before she gives birth, she finds out that Shashikant has been seeing his former girlfriend Krishna (Asha Sachdev), not understanding that he is treating her sick daughter. One night, Krishna calls the house seeking Sashikants help for her daughter. Gauri, who receives the call and suspects an affair, refuses to relay the message to Sashikant. Consequently, Krishnas daughter dies. An intense rift happens between Gauri and Shashikant, and Gauri leaves the house. Angry about how Gauri was responsible for Krishnas daughters death, he does not let her take their son with her. Ravikant is brought up by his father, while Gauri gives birth to another son, Umakant (Arun Govil), and does her best to bring him up. The story follows the separate individual lives of Shashikant and Gauri and their two children.

==Cast==
* Ashok Kumar  as   Retired Justice Umakant Verma
* Jeetendra  as   Dr. Shashikant U. Verma
* Rekha  as   Gauri Singh/Gauri S. Verma Sachin  as   Ravikant S. Ravi Verma
* Arun Govil  as   Umakant S. Verma
* A.K. Hangal  as   Narayan Singh (Gauris Father) Asit Sen  as   Constable Dinanath Tiwari
* Deven Varma  as   Ram Narayan R.N.
* Madan Puri  as   Dubey/Mamaji (Ram Narayans Uncle)
* Asha Sachdev  as   Krishna
* Shoma Anand  as   Manisha R. Narayan
* Tamanna  as   Monica D. Tiwari
* Helena Luke  as   Madhvika R. Narayan
* Aruna Irani  as   Miss Lily (Dancer)

==Crew==
*Direction &ndash; Tatineni Rama Rao
*Story &ndash; M. Balamurugan
*Screenplay &ndash; Tatineni Rama Rao
*Dialogue &ndash; Rahi Masoom Raza|Dr. Rahi Masoom Reza
*Production &ndash; Anumolu Venkata Subba Rao
*Production Company &ndash; Prasad Art Pictures
*Editing &ndash; J. Krishnaswamy, V. Balasubramaniam
*Art Direction &ndash; G.V. Subba Rao
*Costume Design &ndash; Akbar Gabbana, Leena Daru
*Choreography &ndash; Hiralal, Manohar Naidu, Suresh Bhatt
*Music Direction &ndash; Laxmikant-Pyarelal
*Lyrics &ndash; Anand Bakshi Shailendra Singh

==Soundtrack==
{{Tracklisting
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =

| all_writing     =
| all_lyrics      = Anand Bakshi
| all_music       = Laxmikant-Pyarelal

| writing_credits =
| lyrics_credits  =
| music_credits   =

| title1          = Apnon Ko Jo Thukraayega
| note1           =
| writer1         =
| lyrics1         =
| music1          =
| extra1          = Mohammad Rafi
| length1         =

| title2          = Bansi Bajaao Bansi Bajaiya
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = Kishore Kumar
| length2         =

| title3          = Maar Gayee Mujhe Teri Judaai
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = Kishore Kumar, Asha Bhosle
| length3         =

| title4          = Mausam Yeh Mausam Suhaanee Aa Gayee
| note4           =
| writer4         =
| lyrics4         =
| music4          =
| extra4          = Asha Bhosle, Mohammad Rafi
| length4         =

| title5          = Saamne Aa Dekhe Zamana Saara
| note5           =
| writer5         =
| lyrics5         =
| music5          =
| extra5          = Kishore Kumar, Asha Bhosle
| length5         =

| title6          = Tere Naam Ke Hum Deewane
| note6           =
| writer6         =
| lyrics6         =
| music6          =
| extra6          = Amit Kumar, Shailendra Singh, Anuradha Paudwal, Chandrani Mukherjee
| length6         =
}}

==Awards==
*1981 Filmfare Awards - Nominated  Best Actress - Rekha Best Comedian - Deven Verma

==References==
 

==External links==
* 

 
 
 
 
 