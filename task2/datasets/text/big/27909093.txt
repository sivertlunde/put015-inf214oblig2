Nambugun
 
{{Infobox film
| name           = Nambugun
| image          = Nambugun.JPG
| caption        = Theatrical poster
| film name = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Nambugun
 | mr             = Nambugun }}
| director       = Chung Ji-young
| producer       = Chung Ji-young
| writer         = Lee Tae (novel) Jang Sun-woo
| starring       = Ahn Sung-ki  Choi Jin-sil  Choi Min-soo  Lee Hye-young
| music = Shin Bung-ha
| cinematography = You Yong-kil
| editing = Kim Hyeon
| released       =  
| runtime = 158 minutes
| country        = South Korea
| language       = Korean
}} South Korean war drama film directed by Chung Ji-young. It is based upon the experiences of Lee Tae, a war correspondent during the Korean War.

== Cast ==
* Ahn Sung-ki ... Lee Tae
* Choi Jin-sil ... Park Min-ja
* Choi Min-soo...Kim Young
* Lee Hye-young...Kim Hee-suk

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 


 