Rhapsody in Blue (film)
{{Infobox film
| name           = Rhapsody in Blue
| image          = Poster of Rhapsody in Blue (film).jpg
| caption        = 
| director       = Irving Rapper
| producer       =  Howard Koch Elliot Paul Clifford Odets (uncredited) Harry Chandlee Robert Rossen
| story          = Sonya Levien
| starring       = Robert Alda Joan Leslie Alexis Smith Hazel Scott Anne Brown
| featuring      = Hazel Scott Anne Brown
| music          = George Gershwin Max Steiner Ray Heindorf
| cinematography = Merritt B. Gerstad Ernest Haller Sol Polito
| editing        = Folmar Blangsted
| distributor    = Warner Brothers
| released       = 22 September 1945
| runtime        = 141 minutes
| country        = United States
| language       = English
| budget         = 
}}

Rhapsody in Blue (1945 in film|1945) is a fictionalized screen biography of the American composer and musician George Gershwin (September 26, 1898 &ndash; July 11, 1937) released by Warner Brothers. 

==Production background==
Starring Robert Alda as Gershwin, the film features a few of Gershwins acquaintances (including Paul Whiteman, Al Jolson, and Oscar Levant) playing themselves. Alexis Smith and Joan Leslie play fictional women in Gershwins life, Morris Carnovsky and Rosemary De Camp play Gershwins parents, and Herbert Rudley portrays Ira Gershwin. Oscar Levant also recorded most of the piano playing in the movie, and also dubbed Aldas piano playing. Both the Rhapsody in Blue and An American in Paris are performed nearly completely, the "Rhapsody..." debut of 1924 conducted, as it was originally, by Whiteman himself.

The film introduces two fictional romances into the story, one with a woman named Julie Adams (played by Joan Leslie) and the other a near-romance with a rich society woman played by Alexis Smith.

The film notably features performances of Gershwin music by two talented and accomplished African-American musician/singers, Anne Brown (1916-2009) and Hazel Scott (1920-1981). Both were child prodigies whose training included study at the Juilliard School.

Anne Brown, a soprano, created the role of "Bess" in the original production of George Gershwins opera Porgy and Bess in 1935. In the film, Brown sings the aria  Summertime (song)|Summertime from Porgy and Bess. But in the film, the song is completely rearranged, with the first verse sung by chorus only.  William Gillespie, an African-American bass-baritone, appeared uncredited as "Porgy" in the Porgy and Bess sequence, but did not sing.

Born in Trinidad and Tobago, Hazel Scott was raised in New York City and became known as a jazz and classical pianist and singer. Like Lena Horne, Scott was one of the first African-American women to have a career in Hollywood as well as television. Scott plays herself in the film, performing in a Paris nightclub.

==Cast==
* Robert Alda as George Gershwin
* Joan Leslie as Julie Adams
* Alexis Smith as Christine Gilbert
* Charles Coburn as Max Dreyfus Julie Bishop as Lee Gershwin
* Albert Bassermann as Prof. Franck
* Morris Carnovsky as Poppa Morris Gershwin
* Rosemary DeCamp as Momma Rose Gershwin
* Oscar Levant as Himself
* Paul Whiteman as Himself
* Al Jolson as Himself George White as Himself
* Hazel Scott as Herself
* Anne Brown as Herself as Bess in Porgy and Bess scene
* Herbert Rudley as Ira Gershwin

==Awards and nominations== Grand Prize Best Sound (Nathan Levinson).   

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 