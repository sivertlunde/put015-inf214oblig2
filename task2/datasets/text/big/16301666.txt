The Unborn (2009 film)
{{Infobox Film
| name           = The Unborn 
| image          = The Unborn poster.jpg
| alt            = A woman standing in front of a mirror 
| caption        = International release poster
| director       = David S. Goyer
| producer       = {{Plain list | 
* Michael Bay
* Andrew Form Brad Fuller
}}
| writer         = David S. Goyer
| music          = Ramin Djawadi
| editing        = Jeff Betancourt
| cinematography = James Hawkinson
| starring       = {{Plain list | 
* Odette Yustman
* Gary Oldman
* Meagan Good
* Cam Gigandet
* James Remar
* Jane Alexander
* Idris Elba
* Carla Gugino
}}
| studio         = Platinum Dunes
| distributor    = Rogue Pictures Universal Pictures
| released       = January 9, 2009 (US)
| runtime        = 88 minutes 89 minutes  
| language       = English
| country        = United States
| budget         = $16 million   
| gross          = $76,514,050
}} horror film written and directed by David S. Goyer. The film stars Odette Yustman as a young woman who is tormented by a dybbuk and seeks help from a rabbi (Gary Oldman). The dybbuk seeks to use her death as a gateway to physical existence.   The film is produced by Michael Bay and Platinum Dunes. It was released in American theaters on January 9, 2009, by Rogue Pictures.

==Plot==
Casey Beldon has nightmarish hallucinations of strange-looking dogs in the neighbourhood and an evil child with bright blue eyes following her around. While babysitting Matty, her neighbors son, she finds him showing his infant sibling its reflection in a mirror. Matty attacks Casey, smashing the mirror on her head, and tells her: "Jumby wants to be born now". She puts him to bed and leaves in shock.
 superstition that tetragametic chimerism and heterochromia, and that is completely normal. Her neighbors infant dies, supporting the superstition.
 her umbilical cord strangled him, and whom he and Caseys mother had nicknamed "Jumby". She begins to suspect that the spirit is haunting her and that is the soul of her dead twin wanting to be born so it can enter the world of the living as evil. 
 Nazi experiments Auschwitz during World War II. A dybbuk brought the brother back to life to use as a portal into the world of the living. Kozma killed her twin to stop the spirit, and now it haunts her family for revenge, which is why Caseys mother became insane and committed suicide.

Kozma gives Casey a hamsa amulet for protection; instructs her to destroy all mirrors and burn the shards; and refers her to Rabbi Joseph Sendak, who can perform a Jewish exorcism to remove the dybbuk out of her soul. Sendak does not believe Caseys story until he sees a dog with its head twisted upside down in his synagogue. The dybbuk kills Kozma and, soon after, Romy. Casey and her boyfriend Mark&mdash;who both see the spirit after it kills Romy&mdash;realize that it is getting stronger. 

Sendak, Mark, Episcopal priest Arthur Wyndham, and other volunteers begin the exorcism, but the dybbuk attacks them and several are wounded or killed. The spirit, having possessed the priest, chases Casey and Mark. Mark knocks Wyndham unconscious but gets possessed. Casey stabs Mark in the neck with the amulet; Sendak arrives and he and Casey complete the exorcism. The rite draws the dybbuk out of the human world, but Mark falls and dies during the separation.

Casey mourns her boyfriend but still wonders why the dybbuk became suddenly active in her life now, and why it didnt attack her earlier. She takes a pregnancy test, and learns that she is pregnant by Mark, with twins.

==Cast== Odette Yustman as Casey Beldon
* Cam Gigandet as Mark Hardigan
* Meagan Good as Romy
* Jane Alexander as Sofi Kozma
* Gary Oldman as Rabbi Joseph Sendak
* Idris Elba as Arthur Wyndham
* James Remar as Gordon Beldon
* Carla Gugino as Janet Beldon
* Atticus Shaffer as Matty Newton
* Ethan Cutkosky as Barto
* Rhys Coiro as Mr. Shields
* Michael Sassone as Eli Walker
* Rachel Brosnahan as Lisa

==Reception==
=== Box office===
In the U.S., the film opened at the third position, grossing $19,810,585 averaging $8,405 at 2,357 sites.  It spent only eight weeks in release, and had a final gross of $42,670,410.  Worldwide, the film grossed $76,710,644.

===Critical response===
The Unborn was released to overwhelming negative reviews. Review aggregation website   gave the film a 30 out of 100, based on reviews from 16 critics, indicating "generally unfavorable reviews". 

  

==Soundtrack==
  
{{Infobox album
| Name        = The Unborn: Original Motion Picture Soundtrack
| Type        = Film score
| Artist      = Ramin Djawadi
| Cover       = The_Unborn_Soundtrack.gif
| Caption     =
| Released    = February 24, 2009
| Recorded    =
| Genre       = Soundtrack
| Length      =
| Label       = Lakeshore Records  LKS 340652  
| Producer    = 
| Last album  =
| This album  =
| Next album  =
}}

The Unborn: Original Motion Picture Soundtrack was composed by Ramin Djawadi. The record was released on February 24, 2009 via Lakeshore Records label.

{{Track listing
| title1          = The Unborn
| length1         = 4:17
| title2          = The Glove
| length2         = 2:07
| title3          = Jumby Wants to Be Born Now
| length3         = 1:24
| title4          = Twins
| length4         = 1:55
| title5          = Moms Room
| length5         = 2:22
| title6          = Barto
| length6         = 2:12
| title7          = Possessed
| length7         = 3:15
| title8          = Experiments
| length8         = 3:34
| title9          = Breakin Mirrors
| length9         = 2:18
| title10         = Dybuk
| length10        = 1:12
| title11         = The Doorways Open
| length11        = 2:38
| title12         = Sophies Letter
| length12        = 2:18
| title13         = Medicine Cabinet
| length13        = 1:59
| title14         = Bugs
| length14        = 2:01
| title15         = Book of Mirrors
| length15        = 2:27
| title16         = Circle of Trust
| length16        = 2:47
| title17         = Hex or Schism
| length17        = 4:43
| title18         = Inhabit the Helpless
| length18        = 1:13
| title19         = Sefer Ha-Marot
| length19        = 2:49
| title20         = Casey
| length20        = 1:22
}}

==Home media== BD Live features.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 