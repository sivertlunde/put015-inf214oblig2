Crook (film)
 
 

{{Infobox film
| name           = Crook
| image          = Crook2.jpg
| caption        = Theatrical release poster
| director       = Mohit Suri
| producer       = Mukesh Bhatt
| starring       =Emraan Hashmi Neha Sharma Arjan Bajwa
| music          = Pritam Babbu Maan
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Vishesh Films
| released       =  
| runtime        = 121 minutes
| country        = India
| language       = Hindi
| budget = 
| gross = 
 
  
}} allegedly racial attacks on Indian students in Australia between 2007 and 2010.    

==Story==
 pirated
DVDs]]. One day, his uncle Joseph (Gulshan Grover) caught him and changed his
personality completely, he changed his name
to Suraj Bhardwaj and sent him off to
Australia. At the airport, he meets Romi
Latti, a teenager who got a scholarship to a
University College. He also meets Suhani
(Neha Sharma), a young girl who has come
to pick Romi up. Suraj gets attracted to
Suhani, and therefore he pretends to be
Romi and leaves with Suhani. When Suhani
finds out that he is not the real Romi, Suraj
makes a run for it. Suraj then stays with
Goldie (Mashhoor Amrohi), a responsible adult living with his
brothers. While Suraj is at a grocery store,
on phone with his uncle Joseph, he finds
that Australians are attacking the
shopkeeper because he is a Muslim, so
Suraj finds a gun and comes out. He has the
Australians on the gunpoint as the police
come. Suraj remembers that his uncle told
him not to get in any type of trouble with the
police, so Suraj runs away. Suraj hides in
Nicoles car, although he finds out Nicole is
the younger sister of the Attackers. Nicole intimate with Nicole. When Suraj has to pick between Suhani and
Nicole, he picks Suhani and takes the duty to
be Suhanis brother, Samarths (Arjan Bajwa) driver. When Samarths car breaks
down, Suraj has to get help, but instead he
tells Romi to go and fix his car so Suhani
and Suraj can have a beautiful night
together. But when they are about to kiss,
Samarth shows up, and concepts that Romi
has been badly beaten up by Australians on
the highway and Romi and Samarth are
about to protests against the Australians.
When Samarth is attacked, he loses his
temper and kidnaps Nicole, when Suraj goes
to save Nicole, it turns out that Samarth is
planning to murder Nicole and blame the
murder on Suraj. When Suraj comes to know about Samarths plan, Samarth beats Suraj and tells him that he is doing all this
because his sister Sheena (Smiley Suri) was also
murdered by the Australians once. But when
he accidentally shoots Suraj, Romi comes up
behind him with a shovel and hits it
on Samarths head and he dies. The film
ends with Suhani and Suraj getting back
together.

==Cast==
*Emraan Hashmi as Jai Dixit / Suraj Bhardwaj
*Neha Sharma as Suhani
*Arjan Bajwa as Samarth
*Mashhoor Amrohi as Golde
*Kavin Dave as Romi Latti
*Shella Alan as Nicole Nikki
*Francis Chouler as Russell (as Francis Michael Chouler)
*J. Brandon Hill as Sgt. Damien Stern
*Jaideep Suri 
*Jeffrey Tangreo Tsoutsos
*Aman Hora
*Jaideep Suri
*Darmendra Singh
*Shabnam as Girl at Airport
*Vineet Singh
*Glen David Short as Glen David Short
*Smiley Suri as Sheena (Special Appearance)
*Elan Davidson as David

==Soundtrack==
The films soundtrack has been composed by Pritam & Babbu Mann and was released on 6 September 2010. Lyrics are penned by Kumaar."Challa" song lyrics are Penned By Babbal Rai.

===Track listing===
{{Track listing
| extra_column = Performer(s)
| title1 = Challa | extra1 = Babbu Mann, Suzanne DMello | length1 = 3:45
| title2 = Mere Bina | extra2 = Nikhil DSouza | length2 = 4:49
| title3 = Kya | extra3 = Neeraj Shridhar, Dominique Cerejo | length3 = 3:49 KK | length4 = 5:00
| title5 = Tujhko Jo Paaya | extra5 = KK | length5 = 3:04
| title6 = Challa (Remix) | extra6 = Babbu Mann, Suzanne DMello | length6 = 4:26
| title7 = Mere Bina (Unplugged) | extra7 = Mohit Chauhan | length7 = 4:49 KK | length8 = 4:40
}}

==Critical reception==

===India===
Reception of the film in India has been mixed. One critic writes praises the music, writing: "this along with its already popular songs makes Crook a full on entertainment package that should not be missed when it releases all over on 8 October".  Movie critic Taran Adarsh, criticized the film as a "half-hearted effort", but praises Mohit Suris handling of the subject during the second hour of the film.  Another critic praised the film for presenting "an altogether different approach to the situation and (the director) takes both the sides and speaks in favour of Indians and as well as the Australians. 

Among negative reviews, a critic at India Today complained that the film racially vilifies Australians as:

 

Also that "it is badly directed and doesnt even have that one redeeming feature of all" 

A critic at Rediff.com complained of a weak script and story line.  A reviewer at BollySpice|bollyspice.com said that the film was "too insensitive" and "superficial". 

===Australia===
One media outlet in Australia voiced concern about the film, repeating Indian newspaper reviews that Crook portrayed Australia as  "A country of ex-convicts. A country where they sleep with each other without marrying. A country where they dont take care of their families. Yes thats the sort of venom thats spewed against the Australians in Crook." 
 Federation of Indian Students criticised the piece, saying: "They have performed their research so badly, its shocking." He also complained that, far from helping the situation, that the film could help inflame tensions.   

Director Mohit Suri responded to these charges, rejecting the claims of bias. The Hindustan Times quoted Suri saying:
 

Responding to allegations that the film is "poorly researched", Suri says, "I have just made a film. At 28, don’t expect me to have a cure to racism worldwide. I have just expressed my opinion." 

Suri also complained that during the production of the film:
 

==Awards and nominations==

;6th Apsara Film & Television Producers Guild Awards

Nominated
* Apsara Award for Best Music - Pritam
* Apsara Award for Best Performance in a Negative Role - Arjan Bajwa

==References==
 

==External links==
* 

 

 
 
 
 