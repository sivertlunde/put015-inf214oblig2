Child's Pose
 
{{Infobox film
| name           = Childs Pose
| image          = Childs Pose poster.jpg
| caption        = Film poster
| director       = Călin Peter Netzer
| producer       = Ada Solomon
| writer         = Răzvan Rădulescu Călin Peter Netzer
| starring       = Luminița Gheorghiu Bogdan Dumitrache Vlad Ivanov
| music          = 
| cinematography = Andrei Butică
| editing        = 
| distributor    = 
| released       =  
| runtime        =112 minutes 
| country        = Romania
| language       = Romanian
| budget         = 
}}
 Best Foreign Language Film at the 86th Academy Awards,    but it was not nominated.

==Cast==
* Luminița Gheorghiu as Cornelia Keneres, Mother
* Vlad Ivanov as Dinu Laurențiu
* Florin Zamfirescu as Domnul Făgărășanu
* Bogdan Dumitrache as Barbu, Son
* Ilinca Goia as Carmen
* Nataşa Raab as Olga Cerchez
* Adrian Titieni as Father

==Awards==
*2013: Golden Bear, Berlinale
*2013: Telia Film Award, Stockholm International Film Festival

==See also==
* Romanian New Wave
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of Romanian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 