Atrocious (film)
{{Infobox film
| name           = Atrocious
| image          = Atrocious.jpg
| caption        = 
| director       = Fernando Barreda Luna
| producer       = Jessica Villegas David Sanz
| writer         = Fernando Barreda Luna
| starring       = Cristian Valencia Clara Moraleda Rafael Amaya
| studio         = Programa Ibermedia Nabu Films Silencio Rodamos
| distributor    = Bloody Disgusting The Collective
| released       =  
| runtime        = 75 minutes
| country        = Spain
| language       = Spanish
}} Spanish horror film, written and directed by Fernando Barreda Luna, that was released in the United States (US) on August 17, 2011. 

The film relays the story of two siblings during an Easter holiday at their familys country house in Sitges; the brother decides to investigate a local urban legend involving the ghost of a missing girl named Melinda.

== Plot ==

Cristian Quintanilla (Cristian Valencia) and his sister July (Clara Moraleda)  have a web-show of paranormal investigations covering urban legends. When they are invited to spend the Easter holidays at the familys summer home, they intend to investigate the story of Melinda, a girl that went missing in 1940 and supposedly haunts the woods. Cristian and July spend a great deal of time wandering around the hedge maze there with their video cameras. Their father reveals that their mother once knew the maze quite well in her youth.

Upon finding a well in the maze, Cristian leans into it and calls Melindas name. July chastises him for being disrespectful. The morning after, their dog goes missing and Cristian and July eventually find him dead at the bottom of the well. That night, the mother enters their room, screaming that their younger brother José has gone missing. When she and the two children head for the maze to search for him, they are separated. Eventually, Cristian finds July, tied to the pillars of a small gazebo, bleeding profusely. He leads her back to the house, where they find Josés burnt body in the fireplace. Hearing noises outside, July hides in a kitchen cupboard, while Cristian goes to the front door. An axe blade bursts through the door and Cristian runs upstairs to hide.

The next morning, Cristian goes downstairs to find the kitchen cupboard empty and drenched in blood. Down in the basement he finds an old video recorder playing. It is a tape of his mother being interviewed in a mental institution by her doctors. They mention that schizophrenic episodes in adolescent women have a tendency to reoccur, and that night can bring them on. It is established that Cristian and July once had a baby sister named Michelle and that their mother had a psychotic break as the result of postpartum depression and killed her. The mother blames the episodes on a woman named Elvira, about whom the doctors question her.

As Cristian watches this tape, his mother comes up behind him and slaughters him with an axe.
Five days later, the police find the bodies of the Quintanilla family and recover the video footage shot by Cristian and July.

== Reception ==
The film has received mixed reviews, with Rotten Tomatoes listing sixteen reviews, 56% of which are favorable.  Bill Gibron of PopMatters gave it seven of ten stars, concluding: "Thankfully, writer/director Fernando Barreda Luna knows enough about building suspense to overcome the movies many shortcomings."   

== See also ==
*Found footage (genre)

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 