In the Bosom of the Enemy
{{Infobox film
| name           = In the Bosom of the Enemy ( )
| image          =
| image size     =
| alt            =
| caption        =
| director       = Gil Portes
| producer       =
| writer         =
| screenplay     =
| story          =
| based on       =  
| narrator       =
| starring       =Mylene Dizon
Jomari Illana
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 2001 
| runtime        = 101 minutes
| country        = Philippines
| language       =  Filipino language  Japanese language  Tagalog language
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 Filipino war war drama film  directed by Gil Portes.

==Plot==
After her husband was arrested from being a Guerilla warrior, Pilar agreed to be a wet nurse to the Japanese Generals infant son whose Filipina wife died from giving birth. While attending and taking care of the baby, an unintentional love affair developed between Pilar and the General Hiroshi. She begins to be isolated from her husband and her townspeople as she refused to help the Guerillas to conspire the Generals administration following the battle between the Guerillas and the Japanese Soldiers.

==Cast==
Mylene Dizon as Pilar

Jomari Yllana as Diego

Kenji Motoki as General Hiroshi

Ynez Veneracion as Soledad

==Academy Award submission for nominee consideration==
It was the Philippines submission to the 74th Academy Awards for the 2001 Academy Award for Best Foreign Language Film, but was not accepted as a nominee.      

==See also==
 
*2001 in film
*List of submissions to the 74th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 


 
 