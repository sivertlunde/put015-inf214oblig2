The Detonator
{{Infobox Film
| name = The Detonator
| image = The detonator dvd cover.jpg
| caption = DVD cover Po Chih Leong
| producer = Donald Kushner Pierre Spengler Andrew Stevens
| writer = Martin Wheeler William Hope Matthew Leitch Bogdan Uritescu Warren Derosa Michael Brandon
| music = Barry Taylor
| cinematography = Richard Greatrex
| editing = Patrick Dodd
| studio = Andrew Stevens Entertainment Donald Kushner Entertainment
| distributor = Sony Pictures Home Entertainment
| released =  
| runtime = 91 minutes
| language = English
| country = United States
| budget = $15 million
}} Po Chih William Hope. The film was released on direct-to-video|direct-to-DVD in the United States on April 25, 2006.

==Plot==
Undercover C.I.A. agent Sonni Griffith (Wesley Snipes) travels alone to Romania to expose an arms dealer and stop the sale of a nuclear weapon. When the arms dealer is tipped off to Griffiths identity, he lands himself in prison...but is quickly released by the C.I.A. only to be given a new mission: to escort a beautiful Russian woman named Nadia (Silvia Colloca) back to the United States.

Griffith soon learns that strong-willed Nadia is being hunted by the very arms dealer that he intended to destroy, but this evil dealer will stop at nothing to get the information out of Nadia that he needs the location of the $30 million she has hidden that will buy him a nuclear bomb. As the leak within the C.I.A. continues to expose the location and identity of Griffith and Nadia, they must fight the arms dealers to the death to save themselves and the world!

==Cast==
* Wesley Snipes as Sonni Griffith
* Silvia Colloca as Nadia Cominski
* Tim Dutton as Jozef Bostanescu William Hope as Michael Shepard
* Matthew Leitch as Dimitru Ilinca
* Bogdan Uritescu as Pavel
* Warren Derosa as Mitchel
* Michael Brandon as Flint
* Vincenzo Nicoli as Yuri Mishalov
* Stuart Milligan as Greenfield
* Evangelos Grecos as Kravchenko
* Mihnea Paun as Alex
* Florian Ghimpu as Nita
* Valentin Teodosiu as Czeslaw
* Tania Popa as Ana
* Bogdan Farkas as Stankiewcz

==Production==
It is set and filmed at Bucharest Romania, in 49 days, between June 30 and August 18, 2005.

==Home media== Region 1 in the United States on April 25, 2006, and also Region 2 in the United Kingdom on 18 September 2006, it was distributed by Sony Pictures Home Entertainment.

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 