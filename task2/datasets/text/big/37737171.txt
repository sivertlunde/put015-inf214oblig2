Santo contra el cerebro del mal
 
{{Infobox film
| name           = Santo contra el cerebro del mal
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Joselito Rodríguez
| producer       = Jesús Alvariño Jorge García Besné Carlos Garduño G. Enrique Zambrano	
| writer         = Fernando Osés Enrique Zambrano
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Salvador Espinosa	 	
| cinematography = 
| editing        = 
| studio         = Agrupación de Técnicos Cinematografistas de Cuba Azteca Films
| distributor    = Columbia Pictures (within the United States)
| released       = July 1961  (Mexico City) 
| runtime        = 70 minutes
| country        = Mexico Cuba Spanish
| budget         = 
| gross          = 
}}
 action film directed by Joselito Rodríguez and starring Joaquín Cordero, Norma Suárez, Enrique Zambrano and Rodolfo Guzmán Huerta (who plays the titular character, Santo).       It is the first Santo film chronologically. Released in 1961, it was produced around 1958.   

==Plot==
The film opens with three   of a Doctor Lowel and carry off the scientist. He is later given the brainwashing treatment at Campos lab. The bodyguards tell Zambrano that Santo, an undercover agent for the police force, was one of the kidnapers. Meanwhile, another undercover agent, El Incógnito (Fernando Osés) uses a special device to locate the hideout of Doctor Campos.

Elisa (Norma Suárez), Campos secretary, meets Gerardo (Alberto Inzúa), her boyfriend and the assistant of Campos (in relation to his good scientific work only) outside a bank. Elisa tries to speak to the bank manager, but gets an empty response; he has actually been brainwashed by Campos. The brainwashed manager then proceeds to rob the bank, as Campos monitors the happenings. It later unfolds that Campos has a liking for Elisa; he then asks his henchmen to kidnap her. The crime is reported by Gerardo. Zambrano deduces that the reason for Elisas kidnapping was that she was a key witness to the bank robbery. Meanwhile, Doctor Campos hands an agent a few classified papers, promising to sell him a formula for a "cell disintegrator" the following day.

Incógnito returns to the laboratory and overcomes Santo after a tough and hard-won fight. The black-masked Incógnito injects Santo with the antidote to the brainwashing serum, but tells Santo to pretend that he is still brainwashed.

Doctor Campos hands Gerardo a drugged drink and flees to his lab via a secret passageway. He arrives at the hideout; the foreign agent arrives with another man to check the plans for the cell disintegrator. However, Zambrano, having been tipped off by El Incógnito, orders a raid on the lab. Santo and Incógnito battle Campos henchmen as the police arrive, but Campos manages to escape with Elisa. Returning to his house, Campos holds Elisa hostage. Incógnito distracts him while Santo climbs up over the roof to sneak in through the back way; Campos shoots El Incógnito, but Gerardo wakes up from his drugged stupor and struggles with him. Santo bursts in and fights with Campos, who seizes a knife and is ready to kill the masked wrestler until Zambrano shoots him in the nick of time. With his dying breath, Campos apologizes to Elisa.

==Cast==
* Joaquín Cordero as Doctor Campos 
* Norma Suárez as Elisa 
* Enrique Zambrano as Lieutenant Zambrano
* Alberto Inzúa	as Gerardo 
* Rodolfo Guzmán Huerta as Santo (known as El Enmascarado within the film)
* Fernando Osés as El Incognito

==See also==
  Santo filmography

==Notes==
 

==References==
 

==External links==
*  
*  
*  

 
 