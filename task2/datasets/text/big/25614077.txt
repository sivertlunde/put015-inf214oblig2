Tower Bawher
Tower abstract animated Russian composer Georgy Sviridov. 
 Soviet era artists as Dziga Vertov, the Stenberg brothers, Alexander Rodchenko, El Lissitzky and Lyubov Popova.  

The 3 minute 46 second film was made over a five-week period in Montreal, beginning in April 2005, when Ushev ran into problems working on was to have been his first animated short with the National Film Board of Canada, the childrens film Tzaritza. The filmmaker had a strong connection with the Sviridov musical piece, having heard it as a child in Bulgaria as the theme music for the Soviet state TV evening news. 

Despite difficulties in securing the music rights to the Sviridov composition, which had been in dispute in Russian civil court at the time, Tower Bawher was completed in time for its world premiere at the Ottawa International Animation Festival in September 2005.    Awards for the film included the prize for best non-narrative film at I Castelli Animati festival and the award for best abstract film at the London International Animation Festival.   

The film is similar in style to Ushevs 2008  film, Drux Flux, both utilizing Soviet constructivist imagery and Russian classical music score.    

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 

 