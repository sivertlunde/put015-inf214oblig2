All the Boys Love Mandy Lane
 
{{Infobox film
| name = All the Boys Love Mandy Lane
| image = Boys love lane.jpg
| image_size = 215px
| border = yes
| alt = 
| caption = Promotional poster Jonathan Levine
| producer = Chad Feehan Joe Neurauter Felipe Marino
| writer = Jacob Forman Michael Welch Whitney Able Anson Mount
| music = Mark Schulz
| cinematography = Darren Genet
| editing = Josh Noyes
| distributor = Senator Entertainment The Weinstein Company|RADiUS-TWC   Occupant Films Optimum Releasing  
| released =  
| runtime = 90 minutes
| country = United States
| language = English
| budget = $750,000 
| gross = $1,893,697     
}} Michael Welch, Whitney Able and Anson Mount. The plot centers on a group of rich populars who invite an outsider, Mandy Lane, who developed into a "hot chick" over the summer, to spend the weekend at a secluded ranch house because the boys in their group want to "get with her".

Originally completed in 2006, the film premiered at a number of film festivals throughout 2006 and 2007, including the Toronto Film Festival, Sitges Film Festival, South by Southwest, and London FrightFest Film Festival. It received a theatrical release in the United Kingdom on February 15, 2008.  All the Boys Love Mandy Lane received extremely divided reviews from critics, with some dismissing the film as "bogus and compromised", and others praising its "grindhouse" aesthetic and likening its cinematography to the early work of Terrence Malick   and Tobe Hooper. 

Despite its international attention, the film went unreleased in the United States for over seven years after it was completed; this was due to complications with its distributor, Senator Entertainment, who went bankrupt shortly after purchasing the film from The Weinstein Company. On March 8, 2013, it was announced that The Weinstein Company had re-acquired the rights to theatrically release the film in the United States. The film became available through video on demand in September 2013, and was given a limited release on October 11, 2013, through a joint contract between Senator Entertainment and Weinsteins subsidiary label The Weinstein Company|RADiUS-TWC.   

==Plot==
 
At a Texas high school, Mandy Lane is an outsider who becomes a "hot chick" over the summer. She starts getting a great deal of attention from her male classmates. One of those classmates, Dylan, invites Mandy to a pool party at his house and she accepts with the provision that her best friend, Emmet (another outsider and bullying victim), can come along with her.

At the party, Dylan makes passes at Mandy and puts his hand on Mandys thigh. Emmet sprays Dylan with a supersoaker. Dylan attacks Emmett, throws him into the pool, and holds his head under water until Mandy intercedes. Emmet retreats to a roof overlooking the pool. Dylan comes to get him down but Emmet tricks him into jumping from the roof into the pool. When Dylan jumps he hits his head on cement and dies.

Nine months later, popular stoner Red is having a small party at his fathers cattle ranch and has invited Mandy along. She obtains permission from her aunt and agrees to go. Since Dylans death, she has been befriended by many of Dylans friends. Conversely, Emmet has been almost completely ostracized and is subjected to even more intense bullying.

When they arrive at the ranch, Chloe refuses to drive her car over the cattle grid and so, with a shortage of seats in the car still being used, Mandy and Bird elect to walk to the ranch. While walking, Bird tries to prove he is a "gentleman", but before he can do more than kiss Mandys cheek, he is interrupted by Garth, the ranch hand, who drives Mandy and Bird the rest of the way to the ranch.

The kids go swimming in a nearby lake, then return to the house to drink and play games. After a disparaging remark, Jake walks out, followed by Marlin. They stop in a cattle shed where they engage in oral sex. Jake leaves after an argument. When Jake is far from the shed, Marlin gets knocked out with the butt of a double-barrel shotgun by a hooded figure. Marlin awakens to find the double-barrel shotgun rammed down her throat, almost killing her and breaking her jaw.

When Jake returns to the house, the power goes out. The group separates: Red goes to help Chloe who is upstairs alone, Bird goes out to start the generator, and Mandy starts lighting candles in the kitchen. Mandy is confronted by Jake, who confesses to turning off the power in order to spend time alone with her; when he tries to kiss her, she refuses, and he storms out. The power comes back on.

Jake goes looking for Marlin after being rejected by both Mandy and Chloe, and takes Reds car and gun. Jake drives around in the darkness and eventually finds Marlin sitting by the lake. Just as he realizes she is injured he is pushed into the lake and then shot in the head; the hooded killer, who is revealed to be Emmet, then breaks Marlins neck with the butt of the shotgun.

Soon after, Bird, Mandy, Red and Chloe are joined by Garth at the house after a stranger in Reds car fires fireworks at them on the porch. Bird chases after the car, believing that Jake is the driver, but encounters Emmet. During their fight, Emmet blinds Bird by slashing him across his eyes and stabs him to death. At the ranch, Mandy falls asleep in the kitchen, and Red and Chloe fall asleep on the couch.

In the morning, Emmet enters the house and approaches the sleeping Mandy, stroking her hair. Garth hears noise in the kitchen and suspects someone is in the house, but upon rushing downstairs finds Mandy still asleep with blood on her hair; "wake up" is spelled out in bloody alphabet magnets on the refrigerator. Garth and Mandy realize that they need to leave and go to wake up Red and Chloe.

As they open the door to leave, Emmet shoots Garth. Red and Chloe escape out the back door of the house and run to Chloes car, where they discover the bodies of Jake and Marlin dangling from a barbed-wire fence. Distraught, Chloe and Red kiss one another, when Red is shot from afar; paralyzed, he is killed off-screen by Emmet. Chloe flees, heading back to the ranch, and finds Birds body in the hay fields. As she runs, she is chased by Emmet, who is driving her car.
 knife that Emmet used to kill Bird. She sees Chloe running towards the ranch, screaming, pursued by Emmet. Chloe runs to Mandy, but upon embracing her, Mandy stabs her in the stomach. While Chloe bleeds to death, Emmet and Mandy discuss their suicide pact; Mandy will take pills before shooting Emmet in the heart. Mandy then refuses, never having intended to kill herself. She and Emmet get into an argument and begin fighting. Garth hears Mandy yell for help and shoots Emmet, who attacks him.

Mandy runs into the fields, chased by Emmet, and falls into a ditch full of cattle carcasses, where the two fight. Mandy eventually gets hold of Emmets machete, and hacks him to death. She then helps the wounded Garth into his Bronco. They drive away, and Garth thanks Mandy for saving him, assuming her to be a victim in Emmets murder plot.
 flashback then shows the group back at a railroad track, where they took a break from their drive. They are all goofing off, and Mandy is seen balancing on the tracks, watching her future victims.

==Cast and characters==
* Amber Heard as Mandy Lane, a shy and pensive teenager at her rural Texas high school. She is both a shy misfit and a popular outsider, who keeps distance from her peers, particularly the males who find her an object of extreme sexual desire; the exception is her best friend, Emmet. Mandys parents died during her childhood. In conceiving her character, Heard stated that Mandy Lane "  many, many real girls. Many real teenagers, especially in America. There are a lot of incidents of this kind of violence in school with the perpetrators being cute teenagers against their classmates. Their victims are their classmates and theyre often their bullies....  s a great representation of all those girls who are insecure and uncomfortable with their sexuality and their power and yet theyre strangely intrigued by it and tempted by it."    Michael Welch Columbine massacre. A notable similarity is that in one scene he is portrayed to be wearing a shirt similar to that worn by Eric Harris on the day of the massacre.   
* Anson Mount as Garth, the ranch hand at Reds parents cattle ranch where the teenagers carry out their weekend party. He is years older than them, and the females, particularly Chloe and Marlin, are voraciously attracted to him; it is revealed that he is a war veteran, and that his wife had died before he obtained his job at the ranch. Garths role is to oversee the property, and his presence is that of a parental figure, as well as an object of affection for the girls, much like Mandy is to the boys. insecurity issues as well as body image problems, and continuously belittles her friend Marlin, and asserts Mandy Lanes superior beauty over both of them.
* Edwin Hodge as Bird, also a member of the high schools select popular elite, is slightly more reserved, and thus more perceptive to Mandy Lanes quiet and fleeting nature. He warns her prior to the party that all the boys there invited her for the sole purpose of trying to bed her, and insists that hes "not like that". 
* Aaron Himelstein as Red, the funny and overall nice guy of the popular group, and the one whose familys ranch house the festivities take place at. In the beginning of the film, he gives a brief monologue educating the underclassmen about Mandy Lane and the failed attempts from multiple boys to sleep with her; while astutely aware of Mandy Lanes sexuality, Red is not overtly aggressive or flirtatious toward Mandy. He also makes his best attempts to maintain peace in the group, which is evident when a fight breaks out concerning Jake.
* Luke Grimes as Jake, a hot-headed and overtly sexual teenager who is the most forward in his pursuit of Mandy Lane, although all of his attempts at wooing her are either ignored or received with disgust or silent ambivalence. Jake also makes sexual advances on Chloe.
* Melissa Price as Marlin, Jakes girlfriend and Chloes best friend who is constantly belittled and criticized by the insecure Chloe. Throughout the film, Chloe makes jokes about Marlin being "fat", and the jokes are only reinforced for Marlin after Jake refuses to have sex with her after she performs oral sex on him.

==Production==

===Development=== compositions and photography of landscapes ]]
The film had initially been conceived in 2003 when writer Jacob Forman, producer Chad Feehan, and production designer Tom Hammock were all students at the American Film Institute.     "I actually started it as my thesis at AFI," Feehan told Twitch Film.  "The writer, Jacob Forman and the production designer Tom Hammock and I did it as our thesis together at AFI. We started working on it in 2003, then graduated and got it financed and were able to hire our friends that we graduated with to make the movie. It was obviously quite a journey from 2003 to 2006 when we sold it to the Weinstein Company, and after that its been pretty trying." 
 Friday Night John Hughes The Virgin Dazed and Confused (1993) when developing the films depiction of teenagers. Levine, Jonathan. All the Boys Love Mandy Lane (audio commentary). DVD. Anchor Bay Entertainment. Released 2013-12-03. 

Lead performer Amber Heard said that when she received the script for the film in Los Angeles, she felt it was noticeably "different".  In an interview, she said, "There are so many   you get where it feels like youre reading the same girl over and over again. And then I read this script and I thought it was truly different and that it could be done well. This was a movie that was really under the radar; no one was really talking about it. It didnt have much money and subsequently it didnt get much attention right off the bat." 

===Filming=== Bastrop in 2006, on a budget of $750,000.  According to Amber Heard, she spent little time with the rest of the cast when filming wasnt taking place in order to maintain a distance necessary to her character.  She also said that the shoot was very low-maintenance, saying, "Everyone has these expectations, whether theyre subconscious or not, of the glamour and how much fun that you can have in L.A. and I went with those same expectations. This was my first shoot, my first leading role. I fly to my hometown, funnily enough, to film and I stand out in this field waiting for my hair and make-up. Instead of the chair, instead of the lights, I stand in the middle of a field and have, literally, a bucket of freshly-dug mud dumped on my head." 

==Release== premiered at IFI Horrorthon, French Cinemathèque.

===Distribution issues=== Fantasia Film Festival, and received a theatrical release in the United Kingdom on February 15, 2008.
 on demand on September 6 and in theaters October 11, 2013 in the United States— over seven years after its premiere at the Toronto International Film Festival.  

===Critical reception=== The Virgin Suicides (1999). 

The film continued to receive mixed reviews upon its theatrical release in the United States in October 2013.  The New York Times praised the film, noting that "cinematographer Darren Genet draws from long shots of pursuits and a vaguely 1970s look, which wasn’t cutting-edge during the film’s making but suits the real-time nostalgia of high school activities, even murderous ones,"  and Scott Weinberg of FEARnet said the film "  a quietly artistic taste of teen-aged sexual politics to a subgenre thats generally disinterested in anything resembling brains, wit, or subtext."  The Los Angeles Times gave the film a positive review as well, calling it "a small, tightly coiled spellbinder," and praised Heards performance, referring to it as her most "definitive   to date." 

 , however, praised the films acting and thematics, writing: "Thoughtful viewers may detect thematic whiffs of Columbine, blended with "Carrie," that darken and complicate the film’s aroma of stale blood. Thoughtful viewers? What kind of teen slasher movie is this? Too dumb for the arthouse, but too smart for the mall multiplex, the movie satisfies, paradoxically, precisely because it doesn’t deliver on expectations." 

===Box office===
The films worldwide box office totals $1,893,697,  more than covering its $750,000 budget.   

===Home media=== Region 2 format in 2008. It is scheduled for a North American Blu-ray and DVD release in Region 1 format on December 3, 2013 through Anchor Bay Entertainment. 

==Soundtrack==
Although an official soundtrack was not released, the film features the following songs: 

 
* "In Anticipation of Your Suicide" by Bedroom Walls Nude
* "Slowly, Just Breathe" by Dead Waves Kunek
* "Our Lips Are Sealed" by The Go-Gos
* "Thin Air" by Brian Jennings
* "Sister Golden Hair" by Gerry Beckley, covered by Juliette Commagere Peaches
* "Piano Concerto No. 5 in E Flat Major, Second Movement" by Ludwig van Beethoven
* "Oh Molly Dear" by B. F. Shelton

* "You Take the Fall" by The Sunday Drivers
* "Free Stress Test" by Professor Murder
* "Dreadful Selfish Crime" by Robert Earl Keen
* "Yup Yes Yeah" by Buffalo Roam
* "Green Zone" by Mark Schulz
* "One of Us Is Dead" by The Earlies
* "The Rundown" by S.W.E.A.T.
* "Sealed with a Kiss" by Bobby Vinton
 

==See also==
 
* 2006 Toronto International Film Festival
* Slasher film
* School violence
* Delayed release (film)

==Notes==
 
 
#  Optimum Releasing was only distributor in the United Kingdom, but it is uncertain that Optimum is the official distributor of the film even though it was filmed in America. 
 

==References==
 

==External links==
*   (US)
*   (UK)
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 