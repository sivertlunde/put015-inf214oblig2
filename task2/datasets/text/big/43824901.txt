The Pilgrim (2014 film)
{{Infobox film
| name           = The Pilgrim
| image          = Não_Pare_na_Pista_Poster.jpg
| caption        = Theatrical release poster
| director       = Daniel Augusto
| writer         = Carolina Kotscho
| producer       = Iôna de Macêdo  Carolina Kotscho
| starring       = Júlio Andrade  Ravel Andrade  Fabíula Nascimento  Fabiana Guglielmetti   Lucci Ferreira
| music          = Pascal Gaigne
| cinematography = Jacob Solitrenick
| studio         = Babel Films S.L.  Dama Filmes
| distributor    = Columbia Pictures
| released       =  
| runtime        = 112 minutes
| country        = Brazil  Spain
| language       = Portuguese  Spanish
| gross          = 
| budget         = 
}}
 biographical drama film about the Brazilian lyricist and novelist Paulo Coelho. Directed by Daniel Augusto, it stars Júlio Andrade, Ravel Andrade, Fabíula Nascimento, Fabiana Guglielmetti and Lucci Ferreira. {{cite web | url=http://variety.com/2014/film/news/picture-tree-acquires-paulo-coelho-biopic-the-pilgrim-exclusive-1201273511/ | title=Picture Tree Acquires Paulo Coelho Biopic ‘The Pilgrim’ (Exclusive)
| date=August 5, 2014 | publisher=Variety | accessdate=September 14, 2014}} 

The film focuses on three different moments of the writers career &mdash; his youth in the 60s (period in which is lived by the actor Ravel Andrade); adulthood in the 80s (Julio Andrade); and maturity, in 2013, when he visits once again Santiago de Compostela. 

Using as a basis Paulo Coelhos own statements, the history pervades the most striking moments of the life of the author, as the traumas, the relationship with drugs and religion, sexuality and the partnership with musician Raul Seixas. 

==Cast==
*Júlio Andrade as Paulo Coelho
*Ravel Andrade as Paulo Coelho (young)
*Fabiana Gugli as Christina Oiticicca
*Fabiula Nascimento as Lygia Souza
*Enrique Diaz as Pedro Souza
*Lucci Ferreira as Raul Seixas
*Nancho Novo as Jay Vaquer

== References ==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 

 