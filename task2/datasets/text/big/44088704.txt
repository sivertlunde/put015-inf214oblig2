Vijayam Nammude Senani
{{Infobox film 
| name           = Vijayam Nammude Senani
| image          =
| caption        =
| director       = K. G. Rajasekharan
| producer       = GP Balan
| writer         = KS Rajasekharan
| screenplay     = Joseph Madappally Jose Jose Prakash Pattom Sadan Balan K Nair
| music          = Shankar Ganesh
| cinematography = Kanniyappan
| editing        = VP Krishnan
| studio         = Chanthamani Films
| distributor    = Chanthamani Films
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by K. G. Rajasekharan and produced by GP Balan. The film stars Jose (actor)|Jose, Jose Prakash, Pattom Sadan and Balan K Nair in lead roles. The film had musical score by Shankar Ganesh.   

==Cast== Jose
*Jose Prakash
*Pattom Sadan
*Balan K Nair
*Janardanan
*K. P. Ummer
*Vijayalalitha

==Soundtrack==
The music was composed by Shankar Ganesh and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Oh Poojaari Oru Raavil || Ambili || Bichu Thirumala || 
|-
| 2 || Pralayaagni pole || KP Brahmanandan, Chorus || Bichu Thirumala || 
|-
| 3 || Thumbappookkunnumele || Ambili || Bichu Thirumala || 
|-
| 4 || Vijayam nammude senaani || K. J. Yesudas, Ambili || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 
 

 