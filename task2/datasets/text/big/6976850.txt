Battlefield Earth (film)
 
{{Infobox film
| name           = Battlefield Earth
| image          = Battlefield earth poster.jpg
| alt            = Movie poster which reads: John Travolta / Battlefield Earth / Take Back The Planet. There is a picture of a man with a goatee beard in the background, and alien spaceships in the foreground.
| caption        = Theatrical release poster Roger Christian
| producer       = {{Plainlist|
* Jonathan Krane
* Elie Samaha
* John Travolta}}
| screenplay     = {{Plainlist|
* Corey Mandell
* J. David Shapiro|J. D. Shapiro}}
| based on       =  
| starring       = {{Plainlist|
* John Travolta
* Barry Pepper
* Forest Whitaker
* Kim Coates
* Sabine Karsenti
* Richard Tyson}}
| music          = Elia Cmiral
| cinematography = Giles Nuttgens
| editing        = Robin Russell
| studio         = {{Plainlist|
* Morgan Creek Productions
* Franchise Pictures}} Warner Bros. Pictures
| released       =  
| runtime        = 117 minutes  
| country        = United States
| language       = English
| budget         = $73 million 
| gross          = $29.7 million 
}} science fiction 1982 novel Roger Christian gold miners.   
 bankrupted in 2004 after it emerged that it had fraudulently overstated the films budget by $31&nbsp;million.   
 commercial failure worst movies until 2012 was the most Razzie Awards given to a single film.

Travolta originally envisioned Battlefield Earth as the first of two adapted from the book, as the screenplay only covered the first half of the novel. However, the films poor performance at the box office, as well as the collapse of Franchise Pictures, ended plans for a sequel. 

==Plot==
In the year 3000, Earth has been ruled for 1,000 years by the Psychlos, a brutal race of giant humanoid aliens. The remnants of humanity are either enslaved by the Psychlos and used for manual labor or survive in primitive tribes living in remote areas outside Psychlo control. Jonnie Goodboy Tyler (Barry Pepper), a member of one such tribe, leaves his home in the Rocky Mountains on a journey of exploration. He joins forces with Carlo (Kim Coates), a hunter, but both men are captured by a Psychlo raiding party and transported to a slave camp at the Psychlos main base on Earth, a giant dome built over the ruins of Denver|Denver, Colorado.

Terl (John Travolta), the Psychlo security chief on Earth, has been condemned by his superiors to remain indefinitely at his post on Earth as punishment for an unclear incident involving "the Senators daughter". Aided by his deputy, Ker (Forest Whitaker), Terl devises a plan to buy his way off the planet by making a fortune using human slaves to mine gold in radioactive areas. Psychlos are unable to visit such areas due to the explosive interaction of the gas that they breathe with radionuclide particles. Terl selects Jonnie as his "foreman" for the project and gives him a Psychlo education using a rapid-learning machine. Terl gives Jonnie a party of slaves and a Psychlo flying shuttle and orders him to go out and find gold.
 Fort Knox to satisfy Terls demands, and uses the free time to plot a revolution. Jonnie and his followers find an abandoned underground U.S. military base with working aircraft, weapons, fuel and nuclear weapons. They use the bases flight simulators to train themselves in aerial combat. When Terl returns, he orders more gold to be extracted, and tells how in the 21st century the Psychlos conquered Earth in a matter of minutes. Jonnie warns Terl that he will be overthrown and the humans will retake Earth, and cites the United States Declaration of Independence as inspiration, to which Terl scoffs.
 Harrier jump-jets and other weapons. Carlo sacrifices himself to destroy the dome over Denver, and the Psychlos inside suffocate in Earths atmosphere, which they are unable to breathe. Jonnie captures a Psychlo teleportation device and uses it to teleport an atomic bomb to the Psychlo home world. The ensuing detonation causes the entire Psychlo atmosphere to explode, wiping out the Psychlo world. The film ends with the humans in control of Earth but facing an uncertain future, along with Terl and Ker who survived the base destruction. Terl is now imprisoned inside Fort Knox in a makeshift cell of gold bars, while Ker sides with the victorious humans, and helps in their hard effort to rebuild their civilization. 

==Cast==
 
* John Travolta as Terl
* Barry Pepper as Jonnie Goodboy Tyler
* Forest Whitaker as Ker
* Kim Coates as Carlo
* Sabine Karsenti as Chrissy
* Richard Tyson as Robert the Fox
* Kelly Preston as Chirk
* Michael MacRae as District Manager Zete
* Shaun Austin-Olsen as Planetship Numph
* Tim Post as the Assistant Planetship Michael Byrne as Parson staffer
* Christian Tessier as Mickey
* Sylvain Landry as Sammy
* Earl Pastko as the Bartender
* Noël Burton as the Clinko Learning instructor
 

==Production==
===Initial deals=== film version Hollywood one of these days and probably on location in the Denver area for Battlefield Earth when they film it."   
 Santa Monica public relations firm Dateline Communications announced a nationwide contest to promote the film.  First and second place prizes were an all-expense paid trip to the films production location and a paid bit part|walk-on part in the film, and other announced prizes included a trip to Los Angeles for the world premiere, records, cassettes, and hardcover and paperback copies of the novel.  A 30-foot (10&nbsp;m) high inflatable figure of the films villain, Terl, was erected by Scientology officials on Hollywood Boulevard in 1984 in an effort to promote the production, and auditions were held in Denver. The low-budget project soon collapsed.   
 Pulp Fiction, Best Actor.   He had not forgotten Hubbards wishes to see the book on the big screen and took on the task of making Battlefield Earth into a movie.  Travolta described the book in interviews as "like Pulp Fiction for the year 3000" and "like Star Wars, only better".    
He lobbied influential figures in Hollywood to fund the project and was reported to have recruited the aid of fellow Scientologists in promoting it. According to Bill Mechanic, the former head of Twentieth Century Fox, "John wanted me to make Battlefield Earth. He had Scientologists all over me. They come up to you and they know who you are. And they go, Were really excited about Battlefield Earth ." This did not impress Mechanic: "Do you think in any way, shape, or form that weirding me out is going to make me want to make this movie?"   

 
Travoltas involvement in Battlefield Earth was first publicized in late 1995.  He told the New York Daily News that "Battlefield Earth is the pinnacle of using my power for something. I told my manager, If we cant do the things now that we want to do, what good is the power? Lets test it and try to get the things done that we believe in. "  It was assumed from the start that Travolta would star in and produce the film, which would be distributed by MGM; J. David Shapiro|J. D. Shapiro would write the screenplay.  Shapiro was eventually fired because he refused to accept some suggestions from the studio producers that changed the tone of his script, including removing key scenes and characters.    In 1997 Travoltas long-time manager Jonathan Krane signed a two-year deal with Twentieth Century Fox under which that studio would release Battlefield Earth instead of MGM, but the deal with Fox also fell through.   James Robert Parish, author of Fiasco: A History of Hollywoods Iconic Flops, comments that both studios regarded the project as too risky on several grounds.  Its heavy reliance on special effects would be very expensive, pushing the budget up to as much as $100&nbsp;million; Hubbards narrative was seen as naïve and outdated; and the "Scientology factor" could work against the film, negating Travoltas star power.  As one studio executive put it, "On any film there are ten variables that can kill you. On this film there was an eleventh: Scientology. It just wasnt something anyone really wanted to get involved with."   

===Franchise Pictures=== William Morris, and approached Travolta.  A deal was soon struck and financing was arranged; Travolta significantly reduced his normal fee of $20&nbsp;million, lowering the films cost from the $100&nbsp;million that had previously been forecast, and costs were reduced further by using Canadian locations and facilities. 

The film was set up as an independent production for Morgan Creek Productions which would release the film through Warner Bros. in the U.S. under an existing distribution agreement. Travoltas company JTP Films was also involved, and Travolta invested $5&nbsp;million of his own money in the production.   Warner Bros. allocated $20&nbsp;million for the films marketing and distribution.  Franchise retained the foreign rights, licensing the European distribution rights to the German group Intertainment AG in exchange for 47% of the production costs which were set at $75&nbsp;million. The Intertainment deal later became the focus of a legal action that bankrupted Franchise.  Samaha forecast that the film would be a hit: "My projected numbers on Battlefield Earth are really conservative. Im already covered internationally, and theres no way Im going to lose if the movie does $35&nbsp;million domestically. And Travolta has never had an action movie do under $35&nbsp;million." 

  }} Planet of William Morris was also said to be unenthusiastic, reportedly leading to Travolta threatening to leave them if they did not help him to set up the film. Fellow Scientologist Tom Cruise was said to have warned Warner Bros. that he thought the movie was a bad idea. This was later denied by his spokesperson. 

===Author Services Inc. and Church of Scientology===
In 1999, Author Services Inc. said that it was "donating its share of the profits from the film to charitable organizations that direct drug education and drug rehabilitation programs around the world".  It was reported that the merchandising revenues would be passed on to the Scientology-linked groups Narconon, a drug rehabilitation program, and Applied Scholastics, which promotes Hubbards Study tech, with movie-related sales of the book funding the marketing of Hubbards fiction books and the L. Ron Hubbard Writers of the Future contest.    The size of the revenue deal was not disclosed by the parties; Trendmasters, the makers of the Battlefield Earth line of toys, stated that its deal was strictly with Franchise Pictures, which declined to comment, and Warner Bros. stated that its role was limited to distribution and had nothing to do with the associated merchandising deals. 

In February 2000, Church of Scientology spokesman Mike Rinder told Tribune Media Services that any spinoff deals based on Hubbards novel would benefit Author Services Inc. while another church spokesman, Aron Mason, stated, "The church has no financial interest in Battlefield Earth. Author Services is not part of the Church of Scientology. They are a literary agency without any connection to the church."   

Travoltas manager Jonathan Krane denied that the Church of Scientology was playing any part in the production: "Ive never even dealt with or talked to the church on this. This is an action-adventure, science-fiction story. Period. The movie has nothing to do with Scientology."    Krane stated that the film had been financed "without a dollar coming from the Scientologists".  Some people in Hollywood feared that Travolta was using his box office draw to promote Scientology teachings, and one film producer stated, "This film could encourage kids to embrace the whole strange world of Scientology."    Travolta stated, "Im doing it because its a great piece of science fiction. This is not about Hubbard. Im very interested in Scientology, but thats personal. This is different."  In a separate interview Travolta commented on the perceived similarities between Battlefield Earth and Scientology: "Well, they are kind of synonymous ... L. Ron Hubbard is very famous for Scientology and Dianetics. On the other hand, hes equally as famous in the science fiction world. So for people to think that ... look, I dont want everybody to try Scientology. I dont really care if somebody thinks that. Im not worried about it. You cant be. The truth of why Im doing it is because its a great piece of science fiction. Im going to be the wickedest 9-foot alien youve ever seen in your life."  
 

===Pre-production===
Travolta and his manager, Jonathan Krane, took the lead in hiring the on-set personnel. They initially approached  .   Patrick Tatopoulos was signed to develop the production design and costumes, including the design of the alien Psychlos, and Czech-American composer Elia Cmiral was signed to provide the films score.       Travolta and Krane also signed the cinematographer and most of the principal actors.  Corey Mandell, a screenwriter who had previously worked with Ridley Scott on Blade Runner, signed on to write the script for the film, which had previously gone through 10 revisions.  Mandell stated in an interview, "I am not a Scientologist ... I came on board because John asked me to read the book and said, Its not a religious book. Its a science-fiction story. Theres nothing sacred about the story, nothing of the religious philosophy. I was given this to read purely as science fiction – to see whether it was intriguing as a movie. And it was." 

===Filming===
 
Filmed in Canada, principal photography took place in Montreal and several other Quebec locations during the summer and autumn of 1999.     In January 1999, Travolta flew his private Boeing 727 on a secret visit to Montreal to scout out locations for shooting.  The film was reported to have been the most expensive production shot in Canada up to that point.  It was also reported that the production costs would have been twice as high had the film been shot in the United States.  Almost every shot in the film is at a dutch angle, because, according to Roger Christian, he wanted the film to look like a comic book. 
 Standing Room Only. 

The film was "plagued by bad buzz" before release with the media speculating about the possible influence of Scientology and commenting on the productions tight security.      As the film was entering post-production, the alternative newspaper Mean Magazine obtained a copy of the screenplay. Means staffers changed the scripts title to "Dark Forces" by "Desmond Finch" and circulated it to readers at major Hollywood film production companies.  The comments that came back were unfavorable: "a thoroughly silly plotline is made all the more ludicrous by its hamfisted dialog and ridiculously shallow characterizations", "a completely predictable story that just isnt written well enough to make up for its lack of originality".  One reviewer labeled the screenplay "as entertaining as watching a fly breathe".   

==Release== Scientology holiday.  Its premiere was held on May 10, 2000 at Graumans Chinese Theater on Hollywood Boulevard in Los Angeles. 

===Box office===
 
The films scathingly bad reviews and poor word-of-mouth led to a precipitous falling-off in its grosses. Having earned $11,548,898 from 3,307 screens on its opening weekend, its take collapsed by 67 percent to $3,924,921 the following weekend, giving an average take of $1,158 per screen.    The film made 95 percent of its entire domestic gross in the first two weekends and flatlined thereafter, with earnings dropping a further 75 percent by the end of its third week to $1&nbsp;million. The following week, facing earnings of just $205,745, Warner Bros. attempted to cut its losses by slashing the number of screens at which the film was being shown. The number was reduced from 2,587 to 641.  By its sixth weekend on release, the film was showing on 95&nbsp;screens and had made $18,993 in a week – less than $200 per screen.   

The film ultimately earned $21,471,685 in the United States and Canada and $8,253,978 internationally for a total of $29,725,663 worldwide, falling well short of its reported $73 million production budget and $20 million in estimated marketing costs.  . Retrieved 2008-01-13.  Financially, it is regarded as one of the most expensive box office bombs in film history.   

===Merchandising===
A limited range of merchandising was produced for the film, including posters, a soundtrack CD by Elia Cmíral recorded by the Seattle Symphony, and a re-released version of the novel with a new cover based on the films poster.   Trendmasters also produced a range of action figures of the main characters, including an   figure of Travolta as Terl voicing lines from the film such as "Exterminate all man-animals at will!", "You wouldnt last one day at the academy", "Man is an endangered species", and "Ratbastard!".        In Hubbards novel the term "Ratbastard" is never used, and Terl instead refers to Jonnie Goodboy Tyler as "rat brain". 

===Home media=== Sci Fi Weekly wrote that "... the Battlefield Earth Special Edition DVD is packed with information, offering an enlightening glimpse into the creative process behind this imperfect but entertaining picture".  Randy Salas of the Star Tribune described it as the "Best DVD for a bad movie."    A review of the DVD release in the Los Angeles Times was more critical: "A dated visual style, patched-together special effects and ludicrous dialogue combine in a film that is a wholly miserable experience." 

==Reception==
===Critical reception===
  spoof poster for Battlefield Earth quoting the films many negative reviews   ]]
Upon release, Battlefield Earth was heavily panned, and is often considered as one of  , the film had an average score of 9 out of 100, based on 33 critics indicating "Overwhelming dislike". 

  mocked the film on his television program The Daily Show, describing it as "a cross between Star Wars and the smell of ass". 

Rita Kempley of   wrote: "It may be a bit early to make such judgments, but Battlefield Earth may well turn out to be the worst movie of this century" and called it "  summarized the film as being "a flat-out mess, by golly, with massive narrative sinkholes, leading to moments of outstanding disbelief in the muddled writing and shockingly chaotic mise en scène thats accompanied by ear-pummeling sound and bombastic music". 

  and luridly-tinted scenes.]] angled camera shots (which, according to the director himself in different reports, are used in all but one frame of the film or even in every single frame), derivative special effects and unbelievable plotting.        The Providence Journal highlighted the films unusual color scheme: "Battlefield Earths primary colors are blue and gray, adding to the misery. Whenever we glimpse sunlight, the screen goes all stale yellow, as though someone had urinated on the print. This, by the way, is not such a bad idea." 

The film is profiled in Better Living Through Bad Movies by Scott Clevenger and Sheri Zollinger, who comment: "So what new truths have we gleaned from Battlefield Earth? First, we have learned that spirituality is a fine thing, but its probably best to avoid joining denominations that make action movies."  In 2010, screenwriter J.D. Shapiro wrote an apology letter in The New York Post, saying that his draft was completely different from the final product, and he was very ashamed of the poor quality of Battlefield Earth – " The only time I saw the movie was at the premiere, which was one too many times." 

The film was reported to have been greeted with widespread derision in preview screenings for the public and critics. An audience of Los Angeles entertainment journalists, critics and others greeted the film with guffaws and hoots at a screening in Century City, while other viewers in Washington, D.C. and Baltimore responded with derisive laughter or simply walked out.  At a post-launch publicity event, Travolta, on asking assembled journalists if they had enjoyed it, received no reply.    He later asserted that other filmmakers had enjoyed the movie: "When I felt better about everything was when George Lucas and Quentin Tarantino, and a lot of people that I felt knew what they were doing, saw it and thought it was a great piece of science fiction."  Christian also spoke of an initially positive reception, mentioning an enthusiastic response from both the audience and Tarantino. 

Responding to the criticism, one of the films producers, Elie Samaha, complained: "  critics were waiting for us to ... chop our heads off. Everybody hated Scientology for some reason. I didnt know people were so prejudiced." He argued that despite the films poor performance it would cover its costs in due course: "Maybe   the second cycle with Internet, and HBO, and DVD, you always make your money ... so Im not going to lose sleep over one movie that did not perform for us." 

The reviews were not uniformly negative. Bob Graham of the   without the ponderous build-up or self-importance. Imagine how much more enjoyable the other blockbuster-of-the-moment,  ." 
 worst film lists,     and is included on Rotten Tomatoes "100 Worst of the Worst Movies" list.  Rotten Tomatoes ranked the film 27th in the 100 worst reviewed films of the first decade of the 21st century.   The Arizona Republic listed it as the worst film of 2000, and called it a "monumentally bad sci-fi flick".  Richard Roeper placed the film at number five on his list of "40 movies that linger in the back chambers of my memory vault like a plate of cheese left behind a radiator in a fleabag hotel".    In 2001 the film received the "Worst Picture" award from the Dallas-Fort Worth Film Critics Association.  James Franklin of McClatchy-Tribune News Service put the film as the worst of his "summer blockbuster bombs" list, giving it a rating of four stars for "traumatic" on his scale of how the films "generate a perverse sense of nostalgia".  Christopher Null of Filmcritic.com listed the films villain Terl at number 8 of his "10 Least Effective Movie Villains", writing: "we still cant imagine how anyone would go face to face with one of these creatures and react with anything other than simple laughter". 
 parodied the film at the 2000 MTV Movie Awards.       The MTV short was the first time South Park had satirized Scientology, in a piece entitled "The Gauntlet". The short was primarily a Gladiator (2000 film)|Gladiator parody, with the characters fighting Russell Crowe in the Roman Colosseum; it included "John Travolta and the Church of Scientology" arriving in a spaceship to defeat Crowe and attempting to recruit the boys into Scientology. Travolta, along with his fellow Scientologists, was depicted as a Psychlo, as he appeared in the film. 

===Golden Raspberry Awards=== Jack and Jill won ten awards.

As Travolta did not attend to collect his trophies, an action figure of Terl, his character, accepted them in his place. Travolta responded a week later to the awards: "I didnt even know there were such awards. I have people around me whose job it is to not tell me about such things. Theyre obviously doing their job. Not every film can be a critical and box office success. It would have been awful only if Battlefield Earth was neither. Thats not the case. It is edging toward the $100m mark which means it has found an audience even if it didnt impress critics. Id rather my films connect with audiences than with critics because it gives you more longevity as a performer."  He later insisted that he still felt "really good about it. Here I was taking big chances, breaking a new genre." 

Pepper said that he regretted not having been invited to the Razzies and blamed the films failure on "a weak script and poor production values".  Writer J. D. Shapiro received his Worst Screenplay award from Razzies founder John J. B. Wilson during a radio program; he commented that Travolta had called the script "the Schindlers List of science fiction".    Shapiro also appeared to pick up the Worst Picture of the Decade award at the 30th Golden Raspberry Awards, giving a speech quoting negative reviews, and thanking both the studio for firing him and Corey Mandell for "rewriting my script in a way I never, ever, ever — could have imagined or conceived of myself." 

The films producer, Elie Samaha, declared that he welcomed the "free publicity", as "the more the critics hit Battlefield Earth, the more DVDs it sells. It is the kind of film that makes a movie legend and we feel we have enough staying power to last long after the critics have quieted down." 
{| class="wikitable"
|-
!Year !! Award !! Category !! Nominee !! Result
|- 21st Golden 2000    Golden Raspberry Razzie Award Razzie Award Worst Picture Warner Bros.
| 
|- Razzie Award Worst Director Roger Christian Roger Christian
| 
|- Razzie Award Worst Screenplay Corey Mandell and J.D. Shapiro,  based on the novel by L. Ron Hubbard
| 
|- Razzie Award Worst Actor John Travolta
| 
|- Razzie Award Worst Supporting Actor Barry Pepper
| 
|- Forest Whitaker
| 
|- Razzie Award Worst Supporting Actress Kelly Preston
| 
|- Razzie Award Worst Screen Couple John Travolta and anyone sharing the screen with him
| 
|- 25th Golden 2005  Worst "Drama" of Our First 25 Years
|Battlefield Earth
| 
|- 30th Golden 2010
|Worst Actor of the Decade John Travolta
|    
|- Worst Picture of the Decade
|Battlefield Earth
|    
|}

==Controversies==
===Scientology influence===
  characterized the film as a recruitment tactic for the Church of Scientology, stating, "Its designed to introduce L. Ron Hubbard to a whole new generation of kids. Its there to plant a favorable seed in childrens minds."    Bunker criticized the promotional methods of the film—instead of granting interviews about the film to the press, John Travolta went on a book tour and signed copies of L. Ron Hubbards novel.  Bunker stated, "When   and then suddenly find themselves wanting to explore Christianity." 
 Duncan Campbell put it, "the only subliminal voice I could detect came about 10&nbsp;minutes into this 121-minute film and it seemed to be saying Leeeaaave thisssss cinemmmaaa nooow".  When asked about the similarities between the film and Scientology beliefs in intergalactic travel and aliens, church spokesman Aron Mason stated, "Thats a pretty crude parallel ... Youd have to make some serious leaps of logic to make that comparison."  John Travolta also stated that the film was not inspired by Scientology tenets. 

===Fraud by Franchise Pictures=== Franchise Pictures, The Wall Street Journal reported that the FBI was probing "the question of whether some independent motion picture companies have vastly inflated the budget of films in an effort to scam investors".  In December 2000 the German-based Intertainment AG filed a lawsuit alleging that Franchise Pictures had fraudulently inflated budgets in films including Battlefield Earth, which Intertainment had helped to finance.    Intertainment had agreed to pay 47% of the production costs of several films in exchange for European distribution rights, but ended up paying for between 60 and 90% of the costs instead. The company alleged that Franchise had defrauded it to the tune of over $75 million by systematically submitting "grossly fraudulent and inflated budgets". 
 The Art The Whole Nine Yards.  Baeres testified that "Mr. Samaha said, If you want the other two pictures, you have to take Battlefield Earth — its called packaging". Baeres commented: "We would have been quite happy if he had killed  ". 
 trebled the damages if Franchise had been convicted on that charge.  The judgment forced Franchise into bankruptcy a few months later.  The failure of the film was also reported to have led in 2002 to Travolta firing his manager Jonathan Krane, who had set up the deal with Franchise in the first place. 

==Follow-ups and sequels==
Battlefield Earth is significantly shorter than its source novel, covering only the first 436 pages of the 1,050-page book.  A sequel covering the remainder of the book was originally planned at the outset.   When asked during promotion of the film if there would be a Battlefield Earth 2, Travolta responded, "Sure. Yeah."       Travolta asserted that the first film would become a  .    

Despite Travoltas earlier statements, plans for a sequel never came to fruition. According to James Robert Parishs Fiasco: A History of Hollywoods Iconic Flops, the disastrous performance of Battlefield Earth and the collapse of Franchise Pictures made it very unlikely that a live-action sequel will be made.  In a 2001 interview, Travolta stated that a sequel was not planned: "Ultimately the movie did $100&nbsp;million when you count box office, DVD sales, video and pay per view ... But I dont know what kind of number it would have to do to justify filming the second part of the book. And I dont want to push any buttons in the press and stir anybody up about it now."  Author Services announced in 2001 that Pine Com International, a Tokyo-based animation studio, would produce 13&nbsp;one-hour animated television segments based on the book and rendered in a manga style.  The plans appear to have fallen through, and according to Parish, "little has been heard of the series since". 

==See also==
 
* 21st Golden Raspberry Awards
* Box office bomb
* List of films considered the worst
 

==References==
 

;Further reading
*  
*  

==External links==
*  
*  
*  
*  
*  

   
{{Succession box
| title=Golden Raspberry Award for Worst Picture
| years=21st Golden Raspberry Awards
| before=Wild Wild West
| after=Freddy Got Fingered
}}
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 