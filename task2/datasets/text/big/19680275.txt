Once Upon a Dream (1949 film)
Once 1949 British comedy romance Griffith Jones, Guy Middleton, and Maurice Denham

Time: 1 hour 30 minutes

==Plot==
The film is a comedy and a social commentary on the period and is set just after World War II.
 man (servant) and comes to believe it is true. Meanwhile the husband has asked his servant to help him, after the war, to suggest ways to ignite the romance he and his wife had before the war, as well as find a way to make money in a post-war economy. Misdirection and misunderstandings ensue.

It was a J. Arthur Rank presentation, a Sydney Box production and was released through General Film Distributors Ltd.

==Cast==
* Googie Withers as Carol Gilbert Griffith Jones as Jackson
* Guy Middleton as Major Gilbert
* Betty Lynne as Mlle Louise David Horne as Registrar
* Geoffrey Morris as Registrars Clerk
* Raymond Lovell as Mr. Trout
* Noel Howlett as Solicitor
* Agnes Lauchlan as Aunt Agnes
* Mirren Wood as Conductress
* Hubert Gregg as Captain Williams
* Maurice Denham as Vicar
* Mona Washbourne as Vicars Wife
* Nora Nicholson as 1st W.V.S.
* Dora Bryan as Barmaid
* Hal Osmond as Bailiff
* Arthur Denton as Janitor
* Eric Messiter as Pontefact
* Gibb McLaughlin as Pontefact
* Cecil Bevan as Wright
* Wilfred Caithness as Pontefact

==Production==
*Screenplay by Patrick Kirwan and Victor Katona
*Based on an original story by Val Guest
*Music composed by Arthur Wilkinson, played by London Symphony Orchestra, conducted by John Hollingsworth
*"Time & Time Again" by Eric Maschwitz, music by Manning Sherwin
*Produced by Antony Darnborough
*Production controller - Arthur Alcott
*Associate producer - Alfred Roome Jack Cox
*Camera operator - Len Harris
*Supervising art director - George Provis
*Art director - Cedric Dawe
*Production manager - H. Attwooll
*Editor - Jean Barker
*Assistant director - George Mills
*Make-up - John Wilcox
*Dress designer - Julie Harris
*Recordists - Desmond Dew and Chas. Knott (Western Electric Recording)

 

==External links==
* 

 
 
 
 
 
 
 
 


 
 