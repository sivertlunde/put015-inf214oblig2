The Americanization of Emily
{{Infobox film 
| name           = The Americanization of Emily 
| image          = Americanization of Emily poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster by Reynold Brown
| director       = Arthur Hiller
| producer       = Martin Ransohoff
| screenplay     = Paddy Chayefsky
| based on       =  
| starring       = {{Plainlist|
* James Garner
* Julie Andrews
* Melvyn Douglas
* James Coburn
* Joyce Grenfell
* Keenan Wynn
* William Windom
}}
| music          = Johnny Mandel 
| cinematography = Philip H. Lathrop
| editing        = Tom McAdoo
| studio         = Metro-Goldwyn-Mayer  
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| gross          = $4,000,000 (US/ Canada rentals) 
}}
The Americanization of Emily (1964) is an American comedy-drama film|comedy-drama war film written by Paddy Chayefsky, directed by Arthur Hiller, starring James Garner, Julie Andrews, Melvyn Douglas and James Coburn, and loosely adapted from the novel of the same name by William Bradford Huie, who had been a SeaBee officer on Normandy Invasion|D-Day.  Both Garner     and Andrews   consider it their personal favorite of their films.

Set in London in 1944 during World War II, in the weeks leading up to D-Day, the black-and-white film also features Joyce Grenfell, Keenan Wynn and William Windom.

==Plot== Lieutenant Commander Rear Admiral motor pool, wartime rationing both fascinates and disgusts Emily, but she does not want to lose another loved one to war and finds the "practicing coward" Madison irresistible.
 US Army Air Corps overshadowing the Tomb of the Unknown Sailor."
 combat engineers who will be the first on shore. When Madison tries to retreat to safety, Cummings forces him forward with a pistol. A German shell lands near Madison, making him the first American to die on Omaha Beach. Hundreds of newspaper and magazine covers reprint a photograph of Madison on the shore, making him a hero. Jessup, having recovered from his breakdown, regrets his part in Madisons death but plans to use it in support of the Navy when testifying before a Senate committee in Washington. Losing another man she loves to the war devastates Emily.
 imprisoned for cowardice. Emily persuades him to choose, instead, happiness with her by keeping quiet and accepting his heroic role.

==Main cast==
 
*James Garner as Lt. Cmdr. Charles E. "Charlie" Madison
*Julie Andrews as Emily Barham
*Melvyn Douglas as Admiral William Jessup
*James Coburn as Lt. Cmdr. Paul "Bus" Cummings
*Joyce Grenfell as Mrs. Barham
*Keenan Wynn as Old Sailor
*Edward Binns as Admiral Thomas Healy
*Liz Fraser as Sheila William Windom as Captain Harry Spaulding John Crawford as Chief Petty Officer Paul Adams Douglas Henderson as Captain Marvin Ellender
*Edmon Ryan as Admiral Hoyle
*Steve Franken as Young Sailor
*Alan Sues as Petty Officer Enright
*Judy Carne as "2nd Nameless Broad"
*Sharon Tate as "Beautiful Girl" (uncredited)
*Red West as Soldier (uncredited)
 

==Production==

===Casting===
According to James Garner, William Holden was originally meant to play the lead role of "Charlie" Madison. Garner was originally selected to play the character "Bus" Cummings. When Holden backed out of the project, Garner took the lead role, and James Coburn was brought in to play "Bus".  The resulting movie was one of Garners favourites. 

===Soundtrack===
The film introduced the song "Emily (1964 song)|Emily" which was composed by Johnny Mandel with lyrics by Johnny Mercer. The song was recorded by Frank Sinatra with Nelson Riddle arranging and conducting on October 3, 1964 and included in the Reprise LP, Softly, as I Leave You (album)|Softly, as I Leave You. It was later recorded by Barbra Streisand for The Movie Album (2003).

==Comparison with the novel==
  John Barry was announced. 
Chayefskys adaptation, while retaining the title, characters, situation, background and many specific plot incidents, nevertheless told a very different story. "I found the book, which is serious in tone, essentially a funny satire, and thats how Im treating it." 

The screenplays theme of cowardice as a virtue has no parallel in the novel; in fact, the novel does not mention cowardice at all.

The screenplay implies, but never explicitly explains what is meant by the term "Americanization." The novel uses "Americanized" to refer to a woman who accepts, as a normal condition of wartime, the exchange of her sexual favors for gifts of rare wartime commodities. Thus, in reply to the question "has Pat been Americanized", a character answers:
 

This theme runs throughout the novel. Another character says, "We operate just like a whorehouse ... except we dont sell it for cash. We swap it for Camels and nylons and steak and eggs and lipstick ... this dress ... came from Saks Fifth Avenue in the diplomatic pouch." Emily asks Jimmy, "Am I behaving like a whore?" Jimmy replies, "Whoring is a peacetime activity." 

The screenplay uses Hershey bars to symbolize the luxuries enjoyed by Americans and their "Americanized" companions; the novel uses strawberries rather than chocolate bars, in a parallel way. In his first dinner with Emily, he orders the waiter to bring strawberries. "She protested that they were too forbidden, too expensive." Jimmy convinces her to accept them by arguing, "If you dont eat them, theyll be eaten by one of these expense-account correspondents." Later, she asks Jimmy, "If I fall in love with you, how can I know whether I love you for yourself or for the strawberries?" 

The novel briefly mentions that Emilys mother, Mrs. Barham, has been mentally affected by wartime stress, but she is not a major character. There is no mention of her self-deception or pretense that her husband and son are still alive. The film contains a long scene between Charlie and Mrs. Barham, full of eloquent antiwar rhetoric, in which Charlie breaks down Mrs. Barhams denial and reduces her to tears while nevertheless insisting that he has performed an act of kindness. The novel has no parallel to this scene.

In the film, Charlie is comically unprepared to make the documentary film demanded by Admiral Jessup, and is assisted only by a bumbling and drunken serviceman played by Keenan Wynn. In the book, Charlie has, in fact, been a PR professional in civilian life, takes the assignment seriously, and leads a team of competent cinematographers.

==Reception==

===Award nominations===
 Academy Awards in 1965, for Best Art Direction and Best Cinematography,    and in 1966 Julie Andrews portrayal of Emily earned her a nomination for a BAFTA Award for Best British Actress. 

==DVD==
The Americanization of Emily was released as a Region 1 Blu-ray DVD by Warner Home Video on March 11, 2014 via Warner Archive.

==References==
 

==External links==
 
*  
*  
*  
* 
*  
*   at Archive of American Television

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 