Other Men's Wives
{{Infobox film
| name           = Other Mens Wives
| image          = Other Mens Wives (1919) - 1.jpg
| alt            = 
| caption        = Still with Holmes Herbert and Dorothy Dalton
| director       = Victor Schertzinger
| producer       = Thomas H. Ince
| screenplay     = C. Gardner Sullivan
| starring       = Dorothy Dalton Forrest Stanley Holmes Herbert Dell Boone Elsa Lorimer Hal Clements
| music          = Victor Schertzinger 	
| cinematography = John Stumar
| editor         =	
| studio         = Thomas H. Ince Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent Drama drama film directed by Victor Schertzinger and written by C. Gardner Sullivan. The film stars Dorothy Dalton, Forrest Stanley, Holmes Herbert, Dell Boone, Elsa Lorimer and Hal Clements.   The film was released on June 15, 1919, by Paramount Pictures.

==Plot==
 

==Cast==
*Dorothy Dalton as Cynthia Brock
*Forrest Stanley as James Gordon
*Holmes Herbert as Fenwick Flint 
*Dell Boone as Viola Gordon
*Elsa Lorimer as Mrs. Peyton-Andrews 
*Hal Clements as Mr. Peyton-Andrews 

== References ==
 

== External links ==
 
*  
 
 
 
 
 
 
 
 
 