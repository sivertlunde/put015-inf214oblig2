Yoshiwara (film)
{{Infobox film
| name           = Yoshiwara 
| image          = 
| image_size     = 
| caption        = 
| director       = Max Ophüls Samuel Epstein   Herman Millakowsky Wolfgang Wilhelm   Max Ophüls
| narrator       = 
| starring       = Pierre Richard-Willm Sessue Hayakawa Michiko Tanaka   Roland Toutain
| music          = Paul Dessau 
| editing        = Pierre Méguérian
| cinematography = Eugen Schüfftan 
| studio         = Milo Film
| distributor    = Compagnie Cinématographique de France
| released       = 22 October 1937
| runtime        = 102 minutes
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    =  French drama film directed by Max Ophüls and starring Pierre Richard-Willm, Sessue Hayakawa and Michiko Tanaka. It is based on a novel by Maurice Dekobra. The film is set in the Yoshiwara, the red-light district of Tokyo, in the nineteenth century. It depicts a love triangle between a high-class prostitute, a Russian naval officer and a rickshaw man. 

==Reception==
The film was Ophüls greatest pre-war French financial success.  Yoshiwara proved controversial in Japan where the government objected to the depiction of Japanese brothels and banned it. There was a negative reaction against the two Japanese actors who had starred in the film, and they were labelled as traitors. 

==Cast==
* Pierre Richard-Willm - Lieutenant Serge Polenoff 
* Sessue Hayakawa - Ysamo, Kuli 
* Michiko Tanaka - Kohana 
* Roland Toutain - Pawlik 
* Lucienne Le Marchand - Namo  
* André Gabriello - Pô
* Camille Bert - Le commandant 
* Foun-Sen - Geisha 
* Philippe Richard - Lattaché russe 
* Ky Duyen - Lagent secret 
* Georges Saillard - Le médecin

==References==
 

==Bibliography==
* Bacher, Lutz. Max Ophuls in the Hollywood Studios. Rutgers University Press, 1996.
* Seigle, Cecilia Segawa. Yoshiwara: The Glittering World of the Japanese Courtesan. University of Hawaii Press, 1993.

==External links==
* 

 

 
 
 
 
 
 
 


 