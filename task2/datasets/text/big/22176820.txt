Hail the Artist
 
{{Infobox film
| name           = Hail the Artist
| image          = HailTheArtist1973Poster.jpg
| caption        = French poster
| director       = Yves Robert
| producer       = 
| writer         = Jean-Loup Dabadie Yves Robert
| screenplay     = 
| story          = 
| based on       =  
| starring       = Marcello Mastroianni
| music          = 
| cinematography = Jean Penzer
| editing        = Ghislaine Desjonquères
| studio         = 
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = France Italy
| language       = French
| budget         = 
| gross          = $4,340,265  
}}

Hail the Artist ( ,  ) is a 1973 French-Italian comedy film directed by Yves Robert.   

==Cast==
* Marcello Mastroianni - Nicolas
* Françoise Fabian - Peggy
* Jean Rochefort - Clément
* Evelyne Buyle - Bérénice
* Henri-Jacques Huet - Le metteur en scène au bord de leau
* Lise Delamare - Lady Rosemond
* Sylvie Joly - La femme du photographe
* Bernadette Robert
* Hélène Vallier - La script-girl à Versailles
* Betty Beckers
* Lucienne Legrand
* Simone Paris - La directrice du théâtre
* Elizabeth Teissier (as Elisabeth Teissier)
* Maurice Barrier - Al Capone
* Dominique De Keuchel - Rodrigue

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 