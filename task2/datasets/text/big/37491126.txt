The Truth (1988 film)
 
 
 
{{Infobox film
| name           = The Truth
| image          = TheTruth1988.jpg
| alt            = 
| caption        = DVD cover
| film name = {{Film name| traditional    = 法內情
| simplified     = 法内情
| pinyin         = Fǎ Nèi Qíng
| jyutping       = Faat3 Noi6 Cing4}}
| director       = Taylor Wong
| producer       = Stephen Shiu Johnny Mak
| writer         = Stephen Shiu Taylor Mak Paul Chun
| music          = Joseph Chan
| cinematography = Herman Yau
| editing        = Poon Hung
| studio         = Movie Impact Johnny Mak Production
| distributor    = 
| released       =  
| runtime        = 116 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$18,831,625
}} The Unwritten Law. It is followed by a sequel The Truth Final Episode released the following year and is the last film of the film series.

==Plot==
Raymond Lau (Andy Lau) is a young lawyer who grew up in an orphanage who fought a lawsuit for a prostitute Lau Wai Lan (Deanie Ip), but he still did not that Wai Lan is his mother.

Lau Wai Lan still lives a harsh life, often being extorted by CID officers, she leads a quite miserable life. Unfortunately, his sons birth certificate falls into the hands of the harsh police officer Wong Fat (Kirk Wong), who extorts a large sum of money from Wai Lan, and forces her to cheat in a mahjong game, and was caught and beaten.  With help from her prostitute sisters, they raise a large sum of money, however, Wong Fat has gone back on its words, and out of desperation, Wai Lan killed Wong Fat and was arrested for pending trial. When Sister Mary of the orphanage was  aware of the matter, she finds Raymond again to defend Wai Lan. Wai Lan does not want to let his son know about their relationship, she rather commit suicide to end it. Fortunately, she was rescued. In the hospital, Sister Mary could not help but to tell Raymond the secret, telling him Wai Lan is the unknown uncle who paid for his studies abroad, and that she is his mother.
 Paul Chun), and with profound grievances, it was a disadvantage for him to defend his mother. For the sake of his mother, Raymond bribed witnesses and his acts of instigating false statements brought to light and is on the verge of facing penalties for knowingly violating the law. Raymond tells of his real identity in court, tells the greatness of his mother Wai Lan, which the final jury agreed to Wai Lan not guilty for murder, while accusing the judge for his bad attitude and bring unjust, saying that if the judge can not give a lighter sentence, he clearly has personality problems. Finally, the judge ruled out that Wai Lan is sent to three years incarceration, suspended for five years, and was released immediately.

==Cast==
*Andy Lau as Raymond Lau
*Deanie Ip as Lau Wai Lan
*Kathy Chow as Eve
*Lau Siu Ming as Old Kwan Paul Chun as Prosecutor Chun
*Wai Kei Shun as Eves father
*Shing Fui-On as Madly
*Kirk Wong as Wong Fat
*Ng Man Ling
*Sita Yeung
*Ng Hoi Tin as Judge
*Chan Ging
*Lee Ying Kit as Inspector Chan
*Mantic Yiu
*Tsang Cho Lam as Pings father
*Wong Hung as Madlys thug
*Ho Chi Moon as Juror

==Box office==
The film grossed HK$18,831,625 at the Hong Kong box office in its theatrical run from 28 April to 25 May 1988 in Hong Kong.

==See also==
*Andy Lau filmography

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 
 
 
 