The Blue Man (film)
{{multiple issues|
 
 
}}

{{Infobox film
| name           = The Blue Man
| image_size     =
| image	=	The Blue Man FilmPoster.jpeg
| caption        =A poster bearing the films most commonly used title: Eternal Evil
| director       = George Mihalka
| producer       = Nicolas Clermont (executive producer) Buck Houghton (producer) Pieter Kroonenburg (producer) David J. Patterson (executive producer)
| writer         = Robert Geoffrion (writer)
| narrator       =
| starring       = See below
| music          = Marvin Dolgay
| cinematography = Paul Van der Linden
| editing        = Yves Langlois Nick Rotundo
| distributor    = New Century Productions
| released       = 1985
| runtime        = 85 minutes
| country        = Canada, USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

The Blue Man, also known as Eternal Evil, is a 1985 Canadian film directed by George Mihalka.

==Plot==
A dissatisfied Montreal director of TV commercials is taught to astrally project himself by a mysterious woman. But soon he finds that he does it against his will when he sleeps, and while he does it, he commits savage acts against those in his life.

==Release==
The film was released on VHS by Lightning Video in 1987. The film is assumed to be public domain as it has received numerous DVD releases from different distributors since then, some of which can be found at the dollar store.

==Cast==
*Winston Rekert as Paul Sharpe
*Karen Black as Janus
*John Novak as Kauffman
*Patty Talbot as Jennifer Sharpe
*Vlasta Vrána as Scott
*Andrew Bednarski as Matthew Sharpe
*Bronwen Booth as Isis
*Tom Rack as Dr. Meister
*Joanne Côté as Helen
*Philip Spensley as Bill Pearson
*Ron Lea as Mick
*Len Watt as Dr. Morton
*Michael Sinelnikoff as William Duval
*Lois Maxwell as Monica Duval
*Anthony Sherwood as Jensen Walter Massey as John Westmore
*Dean Hagopian as Guard
*Adriana Roach as Meisters Secretary
*Arthur Grosser as Professor Crombie
*Lois Dellar as Housewife Rob Roy as 1st Assistant Director
*Roland Nincheri as Client

==Soundtrack==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 