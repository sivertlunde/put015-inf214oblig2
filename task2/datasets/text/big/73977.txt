Hell's Hinges
 
 
{{Infobox film
| name           = Hells Hinges
| image          = Hells Hinges.jpg
| caption        = Theatrical poster to Hells Hinges
| director       = {{plainlist|
* Charles Swickard
* William S. Hart (uncredited)
* Clifford Smith (uncredited)
}}
| producer       = Thomas H. Ince
| writer         = C. Gardner Sullivan
| starring       = William S. Hart Clara Williams
| music          = Victor Schertzinger (uncredited)
| cinematography = Joseph H. August
| editing        = Triangle Distributing Corporation
| released       =  
| runtime        = 64 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
| gross          =
}} Western silent film starring William S. Hart and Clara Williams.  Directed by Charles Swickard, William S. Hart and Clifford Smith, and produced by Thomas H. Ince, the screenplay was written by C. Gardner Sullivan.

==Plot==
 
Hells Hinges tells the story of a weak-willed minister, Rev. Bob Henley (played by Standing), who comes to a wild and debauched frontier town with his sister, Faith (played by Williams). The owner of the saloon, Silk Miller (played by Hollingsworth), and his accomplices sense trouble and encourage the local rowdies to disrupt the attempts to evangelize the community. Hard-bitten gunman Blaze Tracy (played by Hart), the most dangerous man around, is, however, won over by the sincerity of Faith. He intervenes to expel the rowdies from the newly built church.

Silk adopts a new approach. He encourages the dance-hall girl, Dolly (played by Glaum), to seduce Rev. Henley. She gets him drunk, and he spends the night in her room. The following morning the whole town learns of his fall from grace. Blaze rides out to find a doctor for the now near-demented minister. 

The disgraced minister, having rapidly descended into alcoholism, is goaded into helping the rowdy element to burn down the church. The church-goers try to defend the church, and a gunfight erupts in which the minister is killed and the church set ablaze. Blaze returns too late to stop the destruction. In revenge, Blaze kills Silk and burns down the whole town, beginning with the saloon. He and Faith leave to start a new life.

==Cast==
*William S. Hart as Blaze Tracy
*Clara Williams as Faith Henley
*Jack Standing as Rev. Robert Henley
*Alfred Hollingsworth as Silk Miller Robert McKim as A clergyman
*J. Frank Burke as  Zeb Taylor
*Louise Glaum as Dolly  John Gilbert as A rowdy cowboy (uncredited)
*Jean Hersholt as A rowdy townsman (uncredited)

==Production==
The production companies were Kay-Bee Pictures and New-York Motion Picture.

==Critical reception==
===Initial reviews in 1916===
When Hells Hinges was released, the reception of the film among New York critics was so positive that the producer bought space in newspapers around the country to reprint the reviews.   The following are excerpts from those reviews:
* New York Telegraph: "Dramatic suspense and punch, coupled with artistic treatment, are the most conspicuous characteristics of Hells Hinges ...   swaggering, hard-drinking, fast-shooting, all-round bad man, with good stuff under a rough exterior, furnished Mr. Hart with a vehicle in which his talents show to best advantage."
* New York American: "A well-balanced supporting cast, a lavish production and marked finesse in treatment combines to make Hells Hinges an unusual offering."
* New York Press: "Gunplay and religion lubricate Hells Hinges ...  It is a film drama that combines all the elements that make for success ... Reckless riding, double-handed shooting from the hip, a dance hall of the Bret Harte description and, finally a conflagration that gives a truly Gehenna-like finish to the place known as Hells Hinges ... No actor before the screen has been able to give as sincere and true a touch to the Westerner as Hart. He rides in a manner indigenous to the soil, he shoots with the real knack and he acts with that sense of artistry that hides the acting." 
* New York Sun: "It depicts strikingly the storm and stress of existence in a Western town with a final scene of the shooting up of a gambling den, which aroused the spectators to a high degree of approval."
* New York Herald: "William S. Hart is beginning to typify certain things in the film world. He is ever stoical, slow to anger, but possessed of the powers of a hundred men when aroused. He is a big, bluff, wholesome fellow, whose ideas are frequently a little peculiar, and he goes about matters in exclusively his own way. But when the showdown arrives, depend upon it, William S. Hart will be found lined up on the side of righteousness. This week, for example, Hart is appearing at the Knickerbocker Theatre in Hells Hinges.  Hart has the opportunity to do some good riding, to carry a drunken minister on his back, to shoot the villain and some sub-villains, to set the town afire and to marry the ministers sister. The Kaiser himself has appeared in pictures and done less."
* New York Herald: "Hells Hinges, one of those traditional places on the frontier of the Wild West, where there aint no Ten Commandments and a man can get a thirst, was pictured in the most lurid manner."

Grace Kingsley of the Los Angeles Times gave the actors high marks.  She credited Hart with doing his "usual excellent work" and found Glaum to be "a really fascinating vampire."   Kingsley paid special note to Standings performance as the reverend, calling it "one of the most subtle, but at the same time of the most sincere bits of film acting of his entire career," a performance exhibiting "intelligence and imagination ... in the very highest degree."   Kingsley found the film to be "marvelously well done" but took exception to the would-be folksy western dialect in the title cards: "C. Gardner Sullivan appears to have written Hells Hinges for the purpose of allowing us to look our fill on fire and fights.  Certain it is the thing is marvelously well done.  There is a burning dance hall with men and women entrapped, which fairly makes you gasp, and there is a beau-oo-tiful free-for-all fight between the sheep and the goats of Hells Hinges.  All this is lovely enough in its way to make for forgiveness of the dialect of the subtitles, a dialect which never was on land or sea."    

The title cards includes lines such as "When women like her say there is a God, there is one, and he sure must be worth trailin with". 
 Moving Picture World gave the film as a whole a positive review: "Brilliant in subtitle, strong in treatment with occasional notes of true pathos, the marks of creative ability and sure craftmanship are there .... the cast is without flaw."      The publication further noted that Ince "is at his best when holding close to revelations of the human mind and heart." 

===Later historical assessments===
In September 1994, Hells Hinges experienced a revival following a screening at the Film Center of the School of the Chicago Art Institute.   At the time, the Chicago Tribunes movie critic, Michael Wilmington, called Hells Hinges "Harts acknowledged masterpiece," "perhaps the finest movie Western made before John Fords 1939 Stagecoach (1939 film)|Stagecoach," and "as emotionally powerful as any American film of the teens, except for the masterpieces of D.W. Griffith and Erich Von Stroheim."   

Two months after the showing in Chicago and Wilmingtons review, the film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant." 

In 1997,   goes up in smoke, the real feel of the old, dusty, unglamorised West, all should have earned Hart a reputation as one of the great directors. ... For the most part, Hells Hinges offers highpowered drama rather than traditional western action. ... Fine camerawork utilising long panoramic shots, excellent cutting and a sure control over the masses of extras fuse this into an episode of astonishing vigor. Hart, his assistant Cliff Smith, his writer Gardner Sullivan and cameraman Joe August were one of the sturdiest (and least appreciated) teams of craftsmen the cinema ever produced."  

==In popular culture==
In 2009, the band Caledonia Mission wrote a song for Esopus magazine|Esopus magazine inspired by the movie. The song is named "The Ballad of Blaze Tracy." 

==References==
 

==External links==
*  at the National Film Preservation Foundation
* 
*  available for free download at  

 
 
 
 
 
 
 
 