Odd Little Man
Odd Little Man (Norwegian title: Da jeg traff Jesus... med sprettert) is a Norwegian family movie from 2000, directed by Stein Leikanger, based on one of the biographic memoirs of Odd Børretzen. The title of the movie is a Norwegian pun: the first part of the Norwegian title means "When I met Jesus", and the second part means "...with a slingshot", thus changing the meaning of the title to "When I hit Jesus ... with a slingshot", as the word traff can mean hit or met depending on the context. The English titles makes use of the coincidence that Børretzens Norwegian first name has another meaning in English.

This movie was the Norwegian entry to the Academy Award for Best Foreign Language Film in 2001.

== External links ==
*  

 
 
 
 


 