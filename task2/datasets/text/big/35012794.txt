Ko Bongisa Mutu
{{Infobox film
| name           = Ko Bongisa Mutu
| image          = 
| caption        = 
| director       = Claude Haffner
| producer       = 
| writer         = 
| starring       = 
| distributor    = 
| released       = 2002
| runtime        = 15 minutes
| country        = Democratic Republic of the Congo
| language       = 
| budget         = 
| gross          = 
| screenplay     = 
| cinematography = Mahongue Mbait Jongue Nicolas Conan Claude Haffner
| editing        = Chantal Piquet
| music          = 
}}

Ko Bongisa Mutu is a 2002 documentary film.

== Synopsis ==
Plagued by the stress of Paris, a young woman from Congo-Kinshasa takes shelter in a hair salon in the Strasbourg-Saint-Denis neighborhood in Paris, where numerous African hairdressers have opened up for business. She spends a very enjoyable day watching the clients being taken care of, eating, singing and even dancing. She recovers memories from her childhood and a certain amount of serenity.

== References ==
 

 
 
 
 
 
 


 
 