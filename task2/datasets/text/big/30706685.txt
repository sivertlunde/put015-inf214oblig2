Nothing to Declare (film)

{{Infobox film
| name           = Nothing to Declare
| image          = Nothing to Declare.jpg
| caption        = 
| director       = Dany Boon
| producer       = Les Productions du Chicon  Éric Hubert
| writer         = Dany Boon
| starring       = Benoît Poelvoorde Dany Boon Chritel Pedrinelli
| music          = Stéphane Reichart
| cinematography = Pierre Aïm
| editing        = Luc Barnier
| studio         =
| distributor    = Pathé
| released       =  
| runtime        = 108 minutes
| country        = France Belgium
| language       = French
| budget         = € 20 million
| gross          = € 88 million }}
 French comedy film, written and directed by Dany Boon. {{cite web|url=http://en.unifrance.org/movie/31217/nothing-to-declare|title=Nothing to Declare
|publisher=unifrance.org |accessdate=2015-03-24}}  

==Plot==
On 1 January 1993, two customs officers, one Belgian and the other French, have to deal with closure of their small customs post situated between Courquain in France and Koorkin in Belgium.
 francophobe and an over-zealous Belgian customs officer, Ruben Vandevoorde is forced to join the first Franco-Belgian mobile squad. The first French volunteer for the squad is Mathias Ducatel, Vandervoordes personal bête noire. Ducatel also has the misfortune to have fallen in love with Vandervoordes sister Louise, who is afraid to unveil their love because of the trouble she knows it will cause within her family.

==Cast==
* Benoît Poelvoorde: Ruben Vandevoorde
* Dany Boon: Mathias Ducatel
* Karin Viard: Irène Janus
* Éric Godon: Chief Willems
* Zinedine Soualem: Lucas
* François Damiens: Jacques Janus
* Philippe Magnan: Mercier
* Guy Lecluyse: Grégory Brioul
* Laurent Gamelon: Duval
* Bruno Lochet: Tiburce
* Nadège Beausson-Diagne: Nadia
* David Coudyser: the country driver
* Jérôme Commandeur: the French driver
* Bouli Lanners: Bruno Vanuxem
* Olivier Gourmet: the priest from Chimay
* Bruno Moynot: the estate agent
* Jean-Luc Couchard: Brother Vanuxem
* Jean-Paul Dermont: Father Vandevoorde
* Julie Bernard: Louise Vandevoorde
* Laurent Cappeluto: the Russian
* Chritel Pedrinelli: Olivia Vandevoorde
* Joachim Ledeganck: Léopold Vandevoorde

==Box office==
According to Pathé the film sold   tickets in the Nord-Pas-de-Calais on the opening night, the biggest film success in the region since Bienvenue chez les Chtis. 

==References==
 

==External links==
 
*  
*   {en}

 

 
 
 
 
 
 
 
 
 
 
 


 
 