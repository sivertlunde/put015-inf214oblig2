Malvaloca (1926 film)
{{Infobox film
| name =   Malvaloca
| image =
| image_size =
| caption =
| director = Benito Perojo
| producer =
| writer = Joaquín Álvarez Quintero (play)   Serafín Álvarez Quintero (play)   Benito Perojo
| narrator =
| starring = Lidia Gutiérrez   Manuel San Germán   Javier de Rivera
| music = 
| cinematography = Georges Asselin  
| editing = 
| studio = Goya Producciones Cinematográficas 
| distributor = 
| released = 1926
| runtime = 
| country = Spain Spanish intertitles
| budget =
| gross =
| preceded_by =
| followed_by =
}} silent drama play of the same title. 

==Cast==
*    Lidia Gutiérrez as Rosa Malvaloca  
* Manuel San Germán as Leonardo  
* Javier de Rivera as Salvador  
* Joaquín Carrasco as Jeromo  
* Florencia Bécquer as Juanela  
* Lina Moreno as Hermana Piedad 
* Juan Manuel Figuera as Padre de la hija de Malvaloca 
* Carlos Verger as Lobito 
* Amalia Molina   
* Alfredo Hurtado

== References ==
 
 
==Bibliography==
* Peiró, Eva Woods. White Gypsies: Race and Stardom in Spanish Musical Films. University of Minnesota Press, 2012. 

== External links ==
* 

 

 
 
 
 
 
 
 
 
 

 