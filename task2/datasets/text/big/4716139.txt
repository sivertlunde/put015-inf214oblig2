Bharathi Kannamma
 
{{Infobox film
| name           = Bharathi Kannamma
| image          = Cheran
| Cheran
| Meena Vijayakumar Vijayakumar Vadivelu Indu
| producer       = Henry
| studio         = Pankaj Productions
| cinematography = Kichas
| editing        = K. Thanikachalam
| distributor    = Pankaj Productions
| released       =  
| runtime        = 168 minutes
| country        = India
| language       = Tamil Deva
}}
 Tamil film directed by Cheran (director)|Cheran.  The film marks Cherans debut as a director and screenwriter. It was well received critically and commercially upon release. Prabhu Solomon remade this film in Kannada as Usire (film)|Usire (Kannada) (2001) with V. Ravichandran.

==Plot==
The story revolves around Bharathi (R. Parthiepan), a worker for a rich Vellaisamy Thevar Zamindar  (Vijayakumar (actor)|Vijayakumar). He comes from a lower caste and falls in love with the rich Zamindars daughter Kannamma (Meena Durairaj|Meena). Kannamma feels the same way, however their differing castes become a barrier, and as Kannammas marriage is fixed to a wealthy man, Bharathi doesnt have the courage to express his disapproval or start a relationship with Kannamma. His allegiance to the Zamindar, along with his fear of the caste system make him unwilling to stop the marriage. Bharathis sister, however, also falls in love with a rich young man from a neighbouring town, and their differing circumstances do not hinder them from forming a relationship. In the end, Kannamma commits suicide and Bharathi jumps into her funeral pyre kills himself (in reference to the past Hindu practice of Sati (practice)|sati).

==Cast==
*R. Parthiepan as Bharathi Meena as Kannamma Vijayakumar as Vellaisamy Thevar Zamindar
*Vadivelu as Eenamuthu Raja
*Indhu as Pechi Ranjith as Maayan
*Anwar Ali Khan
*Pasi Narayanan
*Bayilvan Ranganathan
*M. Rathnakumar
*Crane Manohar
*Ramyasri
*Vijayamma
*Anitha
*Bonda Mani

==Soundtrack==
The music composed by Deva (music director)|Deva. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Arivumathi || 05:16
|-
| 2 || Naalethu Padhuchavare || Swarnalatha || 01:54
|-
| 3 || Poonkatre Poonkatre || K. J. Yesudas || 05:28
|- Chandrabose || 04:34
|-
| 5 || Retakili Rekkai || Mano (singer)|Mano, S. P. Sailaja || 05:04
|-
| 6 || Thendralukku Theriyuma || Arun Mozhi, K. S. Chithra || 05:14
|-
| 7 || Vaady Patti Melamada || Gangai Amaran || 02:39
|}

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 


 