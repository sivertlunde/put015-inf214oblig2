Yolanda (film)
{{infobox film
| name           = Yolanda
| image          =Flickr - …trialsanderrors - Marion Davies, Ziegfeld girl, by Alfred Cheney Johnston, 1924.jpg
| imagesize      =180px
| caption        =Davies photograph taken by Alfred Cheney Johnston for the film
| director       = Robert G. Vignola
| producer       = William Randolph Hearst
| based on       =  
| writer         = Luther Reed (screenplay)
| starring       = Marion Davies
| music          = William Frederick Peters George Barnes Ira H. Morgan
| editing        =
| distributor    = Metro-Goldwyn (Metro-Goldwyn-Mayer as of May 1924)
| released       = February 19, 1924 (premiere) September 15, 1924 (nationwide)
| runtime        = 11 reels (10,700 feet)
| country        = United States
| language       = Silent film (English intertitles)
}}
Yolanda is a 1924 silent film historical film drama produced by William Randolph Hearst (through his Cosmopolitan Productions) and starring Marion Davies.  Robert G. Vignola directed as he had Enchantment (1921 film)|Enchantment (1921) and several other Davies costume films. The film is extant at Cinematheque de Belgique and the Museum of Modern Art and a trailer survives at the Library of Congress.   The film began production as a Metro-Goldwyn film, with the company becoming Metro-Goldwyn-Mayer in May 1924.  
 When Knighthood Was in Flower in 1922. Unlike Knighthood, Yolanda was not financially successful. 

==Cast==
*Marion Davies - Princess Mary/Yolanda
*Lyn Harding - Charles the Bold, Duke of Burgundy
*Holbrook Blinn - King Louis XI of France
*Maclyn Arbuckle - Bishop La Balue
*Johnny Dooley - The Dauphin Arthur Donaldson - Lord Bishop
*Ralph Graves - Maximillian of Styria
*Ian Maclaren - Campo Basse
*Gustav von Seyffertitz - Oliver de Daim
*Theresa Maxwell Conover - Queen Margaret (*Teresa Maxwell-Conover) Martin Faust - Count Galli
*Thomas Findley - Antoinettes father
*Paul McAllister - Jules dHumbercourt
*Leon Errol - Innkeeper
*Mary Kennedy - Antoinette Castleman

unbilled
*Roy Applegate - Sir Karl Pitti
*Arthur Tovey - 
*Kit Wain - Peasant boy

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 


 