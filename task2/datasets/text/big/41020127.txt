Action (1980 film)
{{Infobox film
 | name = Action
 | image = Action Brass.jpg
 | caption =
 | director = Tinto Brass
 | writer = Tinto Brass Gian Carlo Fusco Roberto Lerici Vincenzo Maria Siniscalchi
 | starring = Luc Merenda
 | music =  Riccardo Giovannini Blue Malbeix Band
 | cinematography = Silvano Ippoliti
 | editing =  Tinto Brass
 | producer = Ars Cinematografica
 | distributor =
 | released = 4 January 1980 	
 | runtime = 120 mins
 | awards =
 | country = Italy
 | language = Italian
 | budget =
 }} Italian black comedy directed by Tinto Brass. The film is reminiscent of the directors earlier avant-garde low-budget works such as The Howl and Nerosubianco.

Brass faced many difficulties in Italy due to lawsuits concerning the production of Caligula (film)|Caligula and he could film Action in London in 1979.   

==Plot==
Bruno Martel (Luc Merenda) is a "Z movie" actor with hero syndrome, often quarrelling with directors. He meets Doris (Susanna Javicoli) an actress who is obsessed with Ophelia but cannot get any Shakespearean bookings. One day, during a nervous breakdown, Bruno "rescues" Doris from the set, leaving the town to encounter various absurd situations. They meet an old anarchist (Alberto Sorrentino) who thinks he is Giuseppe Garibaldi and the three are locked at a mental asylum where Doris commits suicide. Bruno and "Garibaldi" escape and take refuge at an awkward petrol station run by Florence (Adriana Asti) and her invalid husband Joe (Alberto Lupo).

==Cast==
*Luc Merenda: Bruno Martel
*Alberto Sorrentino: "Garibaldi"
*Susanna Javicoli: Doris
*Adriana Asti: Florence
*Alberto Lupo: Joe
*Paola Senatore: Ann Shimpton
*John Steiner: manager
*Franco Fabrizi: producer
*Tinto Brass: director (cameo)

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 