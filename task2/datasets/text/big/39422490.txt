Commando Duck
 
{{Infobox Hollywood cartoon|
|cartoon_name= Commando Duck
|series=Donald Duck Jack King
|story_artist=
|image=
|caption=
|animator=
|layout_artist=
|background_artist=
|voice_actor=
|musician=
|producer= Walt Disney Productions RKO Radio Pictures
|release_date= May 5, 1944
|color_process=Technicolor
|runtime=7 minutes
|movie_language=English
|preceded_by=Contrary Condor
|followed_by=The Plastics Inventor
}}
 RKO Radio Pictures.

==Plot==
Donald Duck parachutes into the jungle of a remote Pacific island to wipe out a Japanese airfield undetected. Shull, Wilt (2004), p. 166  
 Akita, Kenney (2013), p. 53  He loses most of his equipment in the process of landing. He uses a rubber raft to travel down the river. He is located by Japanese snipers, including one disguised as a rock and one disguised as a slant-eyed and buck-toothed tree.  He initially mistakes their bullets for mosquitoes and presses onwards. 

His raft is caught beneath a waterfall and starts inflating. He makes sure the raft hits nothing that would pop it. When he gets to the edge of a cliff, he sees the airfield. The raft has already exploded, causing water to flow. This large amount of water splashes onto the airfield, wiping the whole thing clean, but leaving disfigured airplanes. Upon seeing the ruins of the airfield, a proud Donald declares his mission accomplished ("Contacted enemy, washed out same"). 

==Analysis==
The cartoon expresses blatant Anti-Japanese sentiment in the United States|anti-Japanese sentiment. However, the focus is mostly on Donald and his efforts and less on the racial aspects of the enemy. This has allowed the short to be broadcast to modern audiences with most of the Japanese references removed. 

There are Japanese caricatures and depictions of the Imperial Japanese Army. There is also a reference to Hirohito.  The Japanese soldiers speak in stereotypical dialect and advocate firing the first shot at a mans back. 

The gag with the enemy soldier disguised as a tree can be traced back to Shoulder Arms (1918). It was also used in A Lecture on Camouflage (1944). Shull, Wilt (2004), p. 193-194 

This is the only film which depicts a regular Disney character engaging with the enemy at war. Leskosky (2011), p. 60 

==Sources==
*  
*  
*  
*  

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 