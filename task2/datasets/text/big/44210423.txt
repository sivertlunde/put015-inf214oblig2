The War Symphonies: Shostakovich Against Stalin
{{Infobox film
| name           = The War Symphonies: Shostakovich Against Stalin
| image          = 
| alt            =
| caption        = 
| director       = Larry Weinstein
| producer       = Niv Fichman
| screenplay     =  
| starring       =  
| music          = Dmitri Shostakovich
| cinematography = Horst Zeidler
| studio         = Rhombus Media
| distributor    = Bullfrog Films
| released       = 1997
}}
 Ninth Symphonies, Lady Macbeth of the Mtsensk District.

==Production== revisionist view of Shostakovich put forward by Solomon Volkov in his book Testimony (book)|Testimony (which is quoted extensively in the film without attribution).    This view holds that Shostakovich was strongly opposed to the leadership of Josef Stalin, and that he included anti-government messages in his compositions under the Soviet regime. However, the extent to which this interpretation of his music is true is hotly disputed by musicologists. 
 Leningrad première of the Seventh Symphony during the Siege of Leningrad.     The list of interviewees has considerable overlap with those quoted in Elizabeth Wilsons Shostakovich: A Life Remembered, published two years before the films production; Weinstein drew heavily on this revisionist work in his scripting.       Extensive archival footage from the Soviet era is combined with contemporary cinematography, and "the editing together of music, image, and voice-over is often masterly". 

The War Symphonies was rereleased by Philips as a DVD in 2005. 

==Reception==
Musicologist Ian Macdonald, an adherent of the revisionist viewpoint, noted that "those diehards who refuse to admit Shostakovichs moral anti-communism, or even that his music is anything but purely abstract, may well squirm at Weinsteins approach. Let them. Their seemingly incurable historical ignorance is the cause of their discomfort: they deserve to be annoyed by this programme".  Anti-revisionist Royal Brown, on the other hand, suggested that "the composer himself would have been horrified at the mickey-mousings to which some of his music is subjected". 

John McCannon noted the films "slight (if understandable) tendency to over-identify with its subject" but praised its "considerable emotional power, as well as a piquant sense of irony".  Brad Eden recommended the work for screening in university musicology and history courses, lauding its "intense historical perspective".  David Haas criticized the narrator of the film as an unrealistic portrayal of Shostakovich but commended the "technical ingenuity and artistry of the films editing". 

===Awards=== Best Arts Documentary
*American Historical Association Film of the Year
*Gemini Award for Best Performing Arts Program 

==References==
 

 

 
 
 
 
 
 