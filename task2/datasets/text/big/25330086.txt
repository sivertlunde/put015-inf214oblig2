Liberty & Bash
 
{{Infobox film
| name           = Liberty & Bash
| image          = 
| image_size     =
| caption        = 
| director       = Myrl A. Schreibman
| producer       = Douglas Forsmith
| writer         = Tina Plackinger, Myrl A. Schreibman
| narrator       = Richard Eden Cheryl Paris
| music          = Sasha Matson
| cinematography = Thomas F. Denove
| editing        = Lee Dragu
| distributor    = Shapiro-Glickenhaus Entertainment
| released       = October 1989
| runtime        = 92 min
| country        =   English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Liberty & Bash is an 1989 Action film starring Miles OKeeffe and Lou Ferrigno as Vietnam war buddies who team up to rid their community of drugs.

==Plot==
Miles OKeeffe and Lou Ferrigno star as Vietnam war buddies who team up to rid their community of drugs. When Jesse is murdered
Liberty hunts them down with Bash to get back what happen to Jesse. Their friendship began in a time of war, when Liberty and Bash stood united against one enemy in Central America. Now, once again, the battle lines have been drawn and another war rages. Its up to them to save their oldest friends life.

==DVD release==
It is unknown whether or not it will be released on DVD.

 
 