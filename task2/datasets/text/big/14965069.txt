Doubt (2008 film)
{{Infobox film
| name           = Doubt
| image          = Doubtposter08.jpg
| image_size     = 
| caption        = US Theatrical release poster
| border         = yes
| director       = John Patrick Shanley
| producer       = Scott Rudin
| screenplay     = John Patrick Shanley
| based on       =  
| starring       =  
| music          = Howard Shore
| cinematography = Roger Deakins
| editing        = Dylan Tichenor
| distributor    = Miramax Films
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = $20 million
| gross          = $50,907,234   
}}
Doubt is a 2008 American  . Written and directed by Shanley and produced by Scott Rudin, the film stars Meryl Streep, Philip Seymour Hoffman, Amy Adams, and Viola Davis. It premiered October 30, 2008 at the AFI Fest before being distributed by Miramax Films in limited release on December 12 and in wide release on December 25.
 Oscars at the 81st Academy Awards.

==Plot==
Set in 1964 at a Catholic church in The Bronx, New York, the film opens with the jovial Father Flynn (Philip Seymour Hoffman) giving a sermon on the nature of doubt, noting that like faith, doubt can be a unifying force. The next evening, Sister Aloysius Beauvier (Meryl Streep), the strict principal of the parish school, discusses the sermon with her fellow nuns, the Sisters of Charity of New York. She asks if anyone has observed unusual behavior that would inspire Father Flynn to preach about doubt, and instructs them to keep their eyes open should any such behavior occur in the future.

Sister James (Amy Adams), a young and naive teacher, observes the closeness between Father Flynn and Donald Miller, an altar boy and the schools only black student. One day during class, Sister James receives a call asking for Donald Miller to meet Father Flynn in the rectory. When he returns, Donald is distraught and Sister James notices the smell of alcohol on his breath. Later, while her students are learning a dance in the gymnasium, she sees Father Flynn placing a white shirt in Donalds locker. She decides to report her suspicions to Sister Aloysius.

Under the pretext of discussing the upcoming school Christmas pageant, Sisters Aloysius and (to a lesser extent) James voice their suspicions that Father Flynns relationship with Donald may be inappropriate. Several times, Father Flynn asks them to leave the matter alone as a private issue between the boy and himself, but Sister Aloysius persists. Finally, he is pressured into admitting that Donald had been caught drinking altar wine, and he had promised Donald not to tell anyone about the incident. Having now been forced to break that promise and reveal the truth, he will need to dismiss Donald as an altar boy, which he had been trying to avoid. Before leaving, Father Flynn tells Sister Aloysius he is displeased with her handling of the situation. His next sermon regards gossip and how it is easily spread.

Initially, Sister James is relieved and convinced of Father Flynns innocence, but Sister Aloysius belief Flynn has behaved inappropriately with Donald is unshakable. Later, Sister James asks Father Flynn about the shirt he put in Donalds locker, an observation she had hidden from Sister Aloysius. Flynn discusses his relationship with the boy. Flynn offers a winding explanation about love and Sister James who has just received a letter from her sick brother appears to interpret Flynns love for Donald as fraternal.

Sister Aloysius meets with Donald Millers mother regarding her suspicions. Mrs. Miller (Viola Davis) shocks Sister Aloysius with her lack of interest in the alleged misconduct on Father Flynns part. As far as she is concerned, Donald need only last to the end of the school year, as graduation from a prestigious church school would increase his chances of going to a decent high school. It is hinted that Donald is homosexual and revealed that his father is abusive, with the dialogue between them indicating that the fathers anger is fueled by his sons effeminacy. Mrs. Miller begs that Sister Aloysius drop the matter, feeling that Father Flynn is a source of inspiration to Donald and a shield from the abuse he receives at home. She becomes angry when Sister Aloysius refuses to compromise and threatens to throw Donald out of the school. Mrs. Miller believes that Donald would be punished for a thing of which he was not the cause.

Despite having no evidence and no support from anyone, Sister Aloysius again confronts Father Flynn and demands that he tell her the truth. Otherwise, she will go to the Bishop. Father Flynn is adamant that there is no illicit relationship, but Sister Aloysius claims that she has learned that he has a history of problems, having moved to three different parishes in the last five years. She tells him that she has contacted a nun from one of his prior churches (she refuses to say whom), who corroborated her suspicions. Father Flynn is furious that she has contacted a nun rather than the church pastor, which is proper church protocol. Sister Aloysius tells him he doesnt deserve to wear the collar, and asks for his resignation. Unable to stand up to her, he succumbs to her demands.

Following his final sermon, Father Flynn steps down from the pulpit and shakes hands with the members of the congregation. Some time later, Sisters Aloysius and James are sitting together in the church garden. Sister Aloysius tells Sister James that although Father Flynn resigned, the bishop has appointed him to pastor at a larger church with a parochial school, in essence promoting him to a more prestigious position and perpetuating the same issue with Father Flynn. She then admits she lied about speaking to a nun at Father Flynns former church, and thus drove him out with no more than her suspicions. Her justification is that if Father Flynn truly were innocent of wrongdoing, he would not have resigned. Repeating a line from earlier in the film, Sister Aloysius says that "in the pursuit of wrongdoing, one steps away from God."

Sister Aloysius concludes that she has paid a price in pursuing the wrongdoing of Father Flynn. While discussing her inability to fully expose Father Flynn and have him dismissed from the diocese as a whole, she reflects upon her larger faith in the Church as she breaks down in tears and says to Sister James: "I have doubts...I have such doubts." The film ends with Sister James comforting Sister Aloysius.

==Cast==
*Meryl Streep as Sister Aloysius Beauvier
*Philip Seymour Hoffman as Father Brendan Flynn
*Amy Adams as Sister James
*Viola Davis as Mrs. Miller
* Joseph Foster as Donald Miller
*Alice Drummond as Sister Veronica
*Paulie Litt as Tommy Conroy

==Production==
Production began on December 1, 2007.    The film, which concentrates on a Bronx Catholic school, was filmed in various areas of the Bronx, including Parkchester, St. Anthonys Church (Bronx, New York)#St. Anthonys School|St. Anthonys Catholic School, and the College of Mount Saint Vincent, as well as Bedford-Stuyvesant, Brooklyn.   
The "garden" exterior scenes were shot at the historic Episcopal Church St. Luke in the Fields on Hudson Street in New Yorks Greenwich Village. The associated St. Lukes School (Manhattan)|St. Lukes School was also heavily featured.
The film is dedicated to Sister Margaret McEntee, a Sister of Charity nun who was Shanleys first-grade teacher and who served as a technical adviser for the movie, after whom Shanley modeled the character of Sister James.

==Reception==
Based on 203 reviews collected by Rotten Tomatoes, the film has a 78% approval rating. The site reported in a consensus that "Doubt succeeds on the strength of its top-notch cast, who successfully guide the film through the occasional narrative lull."    Another review aggregator, Metacritic, gave the film a 68/100 approval rating based on 36 reviews.  Critic Manohla Dargis of The New York Times concluded that "the air is thick with paranoia in Doubt, but nowhere as thick, juicy, sustained or sustaining as Meryl Streeps performance."  Meryl Streeps performance as the stern, intimidating and bold principal Sister Aloysius Beauvier gave her critical acclaim. Philip Seymour Hoffman and Amy Adams performances also received universal acclaim.

Viola Daviss performance as Mrs. Miller drew her critical raves. Salon magazine declared that Daviss character Mrs. Miller was acted with "a near-miraculous level of believability&nbsp;... Davis, in her small, one-scene role, is incredibly moving—I can barely remember a Davis performance where I havent been moved&nbsp;...   plays her character, an anxious, hardworking woman whos just trying to hold her life and family together, by holding everything close. Shes not a fountain of emotion, dispensing broad expression or movement; instead, she keeps it all inside and lets us in." 

National Public Radio called Daviss acting in the movie "the films most wrenching performance&nbsp;... the other   argue strenuously and occasionally even eloquently, to ever-diminishing effect; Davis speaks plainly and quietly, and leaves   doubt that the moral high ground is a treacherous place to occupy in the real world." 

Roger Ebert, who thought Daviss performance worthy of an Academy Award, gave the film four stars, his highest rating, and praised its "exact and merciless writing, powerful performances and timeless relevance. It causes us to start thinking with the first shot," he continued, "and we never stop."  Ebert goes on to say, "The conflict between Aloysius and Flynn is the conflict between old and new, between status and change, between infallibility and uncertainty. And Shanley leaves us doubting." 
 Best Actress Best Adapted Screenplay for Shanley.

The scholar Daniel S. Cutrara, in his book on sex and religion in cinema, has commented that the film works as a metaphor for worldwide uncertainty over priests accused of pedophilia—specifically through Father Flynns resignation as an indication of guilt and then Sister Aloysius subsequent doubt. 

===Top ten lists===
The film appeared on several critics top ten lists of the best films of 2008.     

*2nd – James Berardinelli, ReelViews  New York Daily News 
*8th – Kyle Smith, New York Post 
*8th – Peter Travers, Rolling Stone 
*9th – David Edelstein, New York (magazine)|New York magazine 
*10th – Michael Rechtshaffen, The Hollywood Reporter  Shawn Levy, The Oregonian 

=== Awards === Academy Awards nominations on January 22, 2009, for its four lead actors and for Shanleys script.

{| class="wikitable"
|-
! Award
! Category
! Recipient(s)
! Outcome
|- Academy Awards
| Best Actress
| Meryl Streep
|  
|-
| Best Supporting Actor
| Philip Seymour Hoffman
|  
|- Best Supporting Actress
| Amy Adams
|  
|-
| Viola Davis
|  
|-
| Best Adapted Screenplay
| John Patrick Shanley
|  
|- BAFTA Awards
| Best Leading Actress
| Meryl Streep
|  
|-
| Best Supporting Actor
| Philip Seymour Hoffman
|  
|-
| Best Supporting Actress
| Amy Adams
|  
|- Chicago Film Critics Association Awards
| Best Actress
| Meryl Streep
|  
|-
| Best Supporting Actor
| Philip Seymour Hoffman
|  
|- Best Supporting Actress
| Amy Adams
|  
|-
| Viola Davis
|  
|-
| Best Adapted Screenplay
| John Patrick Shanley
|  
|-
| rowspan="6" | 14th Critics Choice Awards|Critics Choice Awards
| Best Picture
| 
|  
|-
| Best Actress
| Meryl Streep
|  
|-
| Best Supporting Actor
| Philip Seymour Hoffman
|  
|-
| Best Supporting Actress
| Viola Davis
|  
|-
| Best Acting Ensemble
| 
|  
|-
| Best Writer
| John Patrick Shanley
|  
|-
|| Dallas-Fort Worth Film Critics Association Awards 2008|Dallas-Fort Worth Film Critics Association Awards
| Best Supporting Actress
| Viola Davis
|  
|- Detroit Film Critics Society Awards
| Best Actress
| Meryl Streep
|  
|-
| Best Supporting Actress
| Amy Adams
|  
|- Golden Globe Awards
| Best Performance by an Actress in a Motion Picture&nbsp;– Drama
| Meryl Streep
|  
|-
| Best Performance by an Actor in a Supporting Role in a Motion Picture
| Philip Seymour Hoffman
|  
|- Best Performance by an Actress in a Supporting Role in a Motion Picture
| Amy Adams
|  
|-
| Viola Davis
|  
|-
| Best Screenplay&nbsp;– Motion Picture
| John Patrick Shanley
|  
|- Houston Film Critics Society Awards
| Best Supporting Actress
| Viola Davis
|  
|-
| Best Cast
|
|  
|- National Board of Review Awards
| Breakthrough Performance by an Actress
| Viola Davis
|  
|-
| Best Cast
|
|  
|-
|| Palm Springs International Film Festival
| Spotlight Award
| Amy Adams
|  
|-
|| Phoenix Film Critics Society Awards
| Best Actress
| Meryl Streep
|  
|- Satellite Awards
| Best Actress in a Motion Picture&nbsp;– Drama
| Meryl Streep
|  
|-
| Best Actor in a Supporting Role
| Philip Seymour Hoffman
|  
|-
| Best Actress in a Supporting Role
| Viola Davis
|  
|-
| Best Screenplay&nbsp;– Adapted
| John Patrick Shanley
|  
|- Screen Actors Guild Awards
| Outstanding Performance by a Female Actor in a Leading Role
| Meryl Streep
|  
|-
| Outstanding Performance by a Male Actor in a Supporting Role
| Philip Seymour Hoffman
|  
|- Outstanding Performance by a Female Actor in a Supporting Role
| Amy Adams
|  
|-
| Viola Davis
|  
|-
| Outstanding Performance by a Cast in a Motion Picture
| 
|  
|-
| rowspan="2" | St. Louis Gateway Film Critics Association Awards 2008|St. Louis Gateway Film Critics Association Awards Best Supporting Actress
| Amy Adams
|  
|-
| Viola Davis
|  
|- Washington D.C. Area Film Critics Association Awards
| Best Actress
| Meryl Streep
|  
|-
| Best Cast
| 
|  
|}

==References==
 

== External links ==
*  
* French, Philip. " " (film review). The Observer. Saturday February 7, 2009. Observer/Guardian Film Review
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 