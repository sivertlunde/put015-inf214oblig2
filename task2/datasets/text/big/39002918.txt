Rajanadai
{{Infobox film
| name           = Rajanadai
| image          = Rajanadai DVD cover.jpg
| image_size     =
| caption        = DVD cover
| director       = S. A. Chandrasekhar
| producer       = Shoba Chandrasekhar
| screenplay     = S. A. Chandrasekhar
| story          = Shoba Chandrasekhar
| starring       =  
| music          = M. S. Viswanathan
| cinematography = Indhu Chakravarthy
| editing        = Shyam Mukarji
| distributor    =
| studio         = V. V. Creations
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 1989 Tamil Tamil crime Seetha in lead roles. The film, produced by Shoba Chandrasekhar, had musical score by M. S. Viswanathan and was released on 28 October 1989. The film was later remade in Hindi as Jeevan Ki Shatranj.  

==Plot==

Vijayakanth (Vijayakanth), an honest C.I.D inspector, is married to Seetha (Seetha (actress)|Seetha) and they has a daughter Shamili (Shamili). Vijayakanth has enough evidences to arrest the dangerous criminal Tiger (Charan Raj). Vijayakanth befriends Rekha, a C.I.D inspector, without knowing that she is Seethas friend. Seetha compels Rekha to live with them. Seetha has the blood cancer but she hides to her husband. When her husband and her friend know this news, they decide to go in U.S.A for the treatment but Tiger manages to kill Seetha and erases the proofs. Vijayakanth is now more determinated to catch Tiger.

==Cast==

*Vijayakanth as Vijay Seetha as Seetha
*Gouthami as Rekha Suresh as Pandian
*Charan Raj as Tiger
*Radha Ravi as Chakravarthy
*Senthamarai
*Shamili as Shamili
*S. S. Chandran
*Kovai Sarala Kuyili

==Soundtrack==

{{Infobox album |  
| Name        = Rajanadai
| Type        = soundtrack
| Artist      = M. S. Viswanathan
| Cover       = 
| Released    = 1989
| Recorded    = 1989 Feature film soundtrack |
| Length      = 24:16
| Label       = Lahari Music
| Producer    = M. S. Viswanathan
| Reviews     = 
}}
 Vaali and Pulamaipithan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Kasthuri Maankutti  (solo) || K. S. Chithra || 4:37
|- 2 || Kasthuri Maankutti  (duet)  || Jayachandran, K. S. Chithra || 4:32
|- 3 || Onnum Rendum || Vani Jairam || 4:46
|- 4 || Thendralukku || S. P. Balasubrahmanyam || 5:02
|- 5 || Mano || 5:19
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 