Blood Oath (1990 Chinese film)
{{Infobox film
|name=Blood Oath
|image=bloodoathchinese.jpg
|caption=a Hong Kong video recording cover film name={{Infobox name module
|Chinese=  pinyin = Xuě Shì}} based on=Tsui Suks Wife-Killing Records
|director=Gao Tianhong
|producer=Huang Yingchun
|editing=Wang Daru
|writer=Chen Boqing Gao Tianhong
|starring=Su Ying Fei Anqi Li Yanqiu Wang Guofu Cui Yong Zhao Chengshun Huang Guilan Yan Bingyu
|music=Wu Daming
|cinematography=Xu Hongliang
|studio=Xiaoxiang Film Studio
|released=
|runtime=95 minutes
|country=China
|language=Mandarin
}}

Blood Oath is a 1990 Chinese drama film directed by Gao Tianhong and produced by Xiaoxiang Film Studio. It was based on Hong Kong author Tsui Suks novel The Wife-Killing Records (殺妻記).

It is set in China during the Second Sino-Japanese War, in a fictional infantry regiment under general Zhang Zhizhongs commands resisting the Japanese army in northern Hubei.

==Plot==
During the Second Sino-Japanese War, Yu Chongwen (Su Ying), a Chinese college student with no knowledge or experience in warfare, was assigned to the 2801th Infantry Regiment of the Republic of China Armed Forces to direct its political training office. The day after his arrival, the regiment leaders discuss what to do with an adulterous couple caught in the army. The regiment commander Ma Muyuan (Fei Anqi) is adamant that the pair be executed in order to discipline his troops, but Yu Chongwen, finding the order excessive and unacceptable, opposes. Ma assigns Yu to interrogate the two.

Its clear to Yu that the woman Ma Yuee (Li Yanqiu), the wife of Adjutant Zhang (Zhao Chengshun), and Yang Baozhu (Wang Guofu), an orderly, are deeply in love with each other. They request that they be executed together, and Ma Yuee explains that she betrayed Adjutant Zhang with his permission, because he is impotent. To help save their lives, Yu begs for forgiveness with Ma Muyuan, but is frustrated by Ma Muyuans arrogance and lack of compassion. He attempts to write a letter to his superior to resign. 

Ma Muyuan finds Yu and explains, to Yus surprise, that he is close with both Yang Baozhu and Ma Yuee, who is his adopted daughter. He further explains that his wife many years ago also cheated on him with an orderly, after he had been away for 2 years. He did not kill her or the orderly, but was almost killed by the orderly from behind in a battle. He also lists other examples in the army where adultery and lust leads to discordance and betrayal, and justifies his order on that there could be no lenience during a must-win war when national interest is at stake. Yu is convinced and supports the execution.

Right before the battle against the Japanese, Ma Muyuan explains to his soldiers that he must execute Yang and his adopted daughter to stop such behaviors within the army and their families. Yang and Ma Yuee tearfully bid farewell to Ma Muyuan, and are taken away by Yu to the execution ground. Wu Renhe (Cui Yong), hated by the pair for exposing their adultery, shoots and apparently kills them. He is left behind with 2 soldiers friendly with Yang to bury their corpses.

With morale high, the Japanese are defeated and retreated. One day, Chinese soldiers discover the tombs of Yang and Ma Yuee have been raided and that a "ghost" had appeared nearby. Yu finally resigns from the army and begins to trip to Gansu for a teaching position. Along the way he stops in a restaurant and discovers in shock that the waitress is none other than the executed Ma Yuee, and that Yang and Wu, who had also "died" in a car accident, are both there...

==External links==
* 

 
 
 
 
 