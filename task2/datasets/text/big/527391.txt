A Trip to the Moon
 
 
{{Infobox film
| name           = A Trip to the Moon
| image          = Voyage dans la lune title card.png
| caption        = Title card
| director       = Georges Méliès
| producer       = Georges Méliès
| writer         = Georges Méliès Inspirations section below)
| starring       =  
| cinematography =  
| studio         = Star Film Company
| released       =   18 minutes 16 minutes 9 minutes   }}
| country        = France
| language       = Silent
| budget         =  10,000
}}
A Trip to the Moon ( )   Similarly, though the film was first sold in France without an initial article in the title,  it has subsequently been commonly known as Le Voyage dans la Lune.  }} is a 1902 French silent film directed by Georges Méliès. Inspired by a wide variety of sources, including Jules Vernes novels From the Earth to the Moon and Around the Moon, the film follows a group of astronomers who travel to the Moon in a cannon-propelled capsule, explore the Moons surface, escape from an underground group of Selenites (lunar inhabitants), and return with a splashdown to Earth with a captive Selenite. It features an ensemble cast of French theatrical performers, led by Méliès himself in the main role of Professor Barbenfouillis, and is filmed in the overtly theatrical style for which Méliès became famous.
 pirated by other studios, especially in the United States. Its unusual length, lavish production values, innovative special effects, and emphasis on storytelling were markedly influential on other film-makers and ultimately on the development of narrative film as a whole.  Scholars have commented upon the films extensive use of pataphysical and anti-imperialist satire, as well as on its wide influence on later film-makers and its artistic significance within the French theatrical féerie tradition. Though the film disappeared into obscurity after Mélièss retirement from the film industry, it was rediscovered in the late 1920s, when Mélièss importance to the history of cinema was first recognized by film devotees. An original Film colorization|hand-colored print was discovered in 1993 and restored in 2011.

A Trip to the Moon was named one of the 100 greatest films of the 20th century by The Village Voice, ranked 84th.  The film remains the best-known of the hundreds of films made by Méliès, and the moment in which the capsule lands in the Moons eye remains one of the most iconic and frequently referenced images in the history of cinema. It is widely regarded as the earliest example of the science fiction film genre and, more generally, as one of the most influential films in cinema history.

==Plot==
 
At a meeting of the Astronomic Club, its president, Professor Barbenfouillis, .}}   It has been suggested that the name parodies President Impey Barbicane, the hero of Jules Vernes From the Earth to the Moon,  although Méliès had previously used the name in a non-Verne context in 1891, for a stage magic act. }} proposes a trip to the Moon. After addressing some dissent, five other brave astronomers—Nostradamus, Alcofrisbas, Omega, Micromegas and Parafaragaramus—agree to the plan. They build a space capsule in the shape of a bullet, and a huge cannon to shoot it into space. The astronomers embark and their capsule is fired from the cannon with the help of "marines", most of whom are played by a bevy of young women in sailors outfits. The Man in the Moon watches the capsule as it approaches, and it hits him in the eye. 
 Saturn leans out of a window in his ringed planet, and Phoebe (mythology)|Phoebe, goddess of the Moon, appears seated in a crescent-moon swing. Phoebe causes a snowfall that awakens the astronomers, and they seek shelter in a cavern where they discover giant mushrooms. One astronomer opens his umbrella; it promptly takes root and turns into a giant mushroom itself.
 alien inhabitant of the Moon, named after one of the Greek moon goddesses, Selene) appears, but it is killed easily by an astronomer, as the creatures explode if they are hit with force. More Selenites appear and it becomes increasingly difficult for the astronomers to destroy them as they are surrounded. The Selenites capture the astronomers and take them to the palace of their king. An astronomer lifts the Selenite King off his throne and throws him to the ground, causing him to explode.

The astronomers run back to their capsule while continuing to hit the pursuing Selenites, and five get inside. The sixth astronomer, Barbenfouillis himself, uses a rope to tip the capsule over a ledge on the Moon and into space. A Selenite tries to seize the capsule at the last minute. Astronomer, capsule, and Selenite fall through space and land in an ocean on Earth, where they are rescued by a ship and towed ashore. The final sequence (missing from some prints of the film) depicts a celebratory parade in honor of the travelers return, including a display of the captive Selenite and the unveiling of a commemorative statue bearing the motto "Labor omnia vincit".  }}

==Cast==
  opening and closing credits in films was a later innovation.    Nonetheless, the following cast details can be reconstructed from available evidence: Joan of Arc (1900).  His extensive involvement in all of his films as director, producer, writer, designer, technician, publicist, editor, and often actor makes him one of the first cinematic auteurs.  Speaking about his work late in life, Méliès commented: "The greatest difficulty in realising my own ideas forced me to sometimes play the leading role in my films&nbsp;... I was a star without knowing I was one, since the term did not yet exist."  All told, Méliès took an acting role in at least 300 of his 520 films.    Phoebe (the woman on the crescent moon). Méliès discovered Bernon in the 1890s, when she was performing as a singer at the cabaret LEnfer. She also appeared in his 1899 adaption of Cinderella.   
*François Lallement as the officer of the marines. Lallement was one of the salaried camera operators for the Star Film Company. 
*Henri Delannoy as the captain of the rocket 
*Jules-Eugène Legris as the parade leader. Legris was a magician who performed at Mélièss theater of stage illusions, the Théâtre Robert-Houdin in Paris.   
*Victor André, Delpierre, Farjaux, Kelm, and Brunnet as the astronomers. André worked at the  ; the others were singers in French music halls.  : "I remember that in "Trip to the Moon," the Moon (the woman in a crescent,) was Bleuette Bernon, music hall singer, the Stars were ballet girls, from theatre du Châtelet—and the men (principal ones) Victor André, of Cluny theatre, Delpierre, Farjaux—Kelm—Brunnet, music-hall singers, and myself—the Sélenites were acrobats from Folies Bergère."  
*Ballet of the Théâtre du Châtelet as stars 
*Acrobats of the Folies Bergère as Selenites 

==Production==

===Inspiration=== Le voyage dans la lune]]
When asked in 1930 what inspired him for A Trip to the Moon, Méliès credited Jules Vernes novels From the Earth to the Moon and Around the Moon. Cinema historians, the mid-20th-century French writer Georges Sadoul first among them, have frequently suggested H. G. Wellss The First Men in the Moon, a French translation of which was published a few months before Méliès made the film, as another likely influence, with Sadoul arguing that the first half of the film (up to the shooting of the projectile) is derived from Verne and that the second half (the travelers adventures on and in the moon) is derived from Wells. 
 parodic tone of the film, from the Offenbach operetta. 

===Filming===
 
 Ron Miller notes, A Trip to the Moon was one of the most complex films that Méliès had made, and employed "every trick he had learned or invented".  It was his longest film at the time;   Méliès went on to make longer films; his longest, The Conquest of the Pole, runs to 650 meters  or about 44 minutes. }} both the budget and filming duration were unusually lavish, costing  10,000 to make    and taking three months to complete.    The camera operators were Théophile Michault and Lucien Tainguy, who worked on a daily basis with Méliès as salaried employees for the Star Film Company. In addition to their work as cameramen, Mélièss operators also did odd jobs for the company such as developing film and helping to set up scenery, and another salaried operator, François Lallement, appeared onscreen as the marine officer.  By contrast, Méliès hired his actors on a film-by-film basis, drawing from talented individuals in the Parisian theatrical world, with which he had many connections. They were paid one Louis dor per day, a considerably higher salary than that offered by competitors, and had a full free meal at noon with Méliès.   

Mélièss film studio, which he had built in Montreuil, Seine-Saint-Denis in 1897,  was a greenhouse-like building with glass walls and a glass ceiling to let in as much sunlight as possible, a concept used by most still photography studios from the 1860s onward; it was built with the same dimensions as Mélièss own Théâtre Robert-Houdin (13.5×6.6meters|m).  Throughout his film career, Méliès worked on a strict schedule of planning films in the morning, filming scenes during the brightest hours of the day, tending to the film laboratory and the Théâtre Robert-Houdin in the late afternoon, and attending performances at Parisian theaters in the evening. 

 
 molds for them; a specialist in mask-making used these molds to produce cardboard versions for the actors to wear.    One of the backdrops for the film, showing the inside of the glass-roofed workshop in which the space capsule is built, was painted to look like the actual glass-roofed studio in which the film was made. 

Many of the special effects in A Trip to the Moon, as in numerous other Méliès films, were created using the stop trick technique (also known as substitution splicing), in which the camera operator stopped filming long enough for something onscreen to be altered, added, or taken away. Méliès carefully spliced the resulting shots together to create apparently magical effects, such as the transformation of the astronomers telescopes into stools  or the disappearance of the exploding Selenites in puffs of smoke.  The use of special effects in the film as a result, as Barbara Creed puts it, "present the trip to the moon as pure fantasy rather than as a scientific event". 

The pseudo-tracking shot in which the camera appears to approach the Man in the Moon, was accomplished using an effect Méliès had invented the previous year for the film The Man with the Rubber Head.    Rather than attempting to move his weighty camera toward an actor, he set a pulley-operated chair upon a rail-fitted ramp, placed the actor (covered up to the neck in black velvet) on the chair, and pulled him toward the camera.    In addition to its technical practicality, this technique also allowed Méliès to control the placement of the face within the frame to a much greater degree of specificity than moving his camera allowed.  A substitution splice allowed a model capsule to suddenly appear in the eye of the actor playing the Moon, completing the shot.  Another notable sequence in the film, the plunge of the capsule into real ocean waves filmed on location, was created through multiple exposure, with a shot of the capsule falling in front of a black background superimposed upon the footage of the ocean. The shot is followed by an underwater glimpse of the capsule floating back to the surface, created by combining a moving cardboard cutout of the capsule with an aquarium containing tadpoles and air jets.  The descent of the rocket from the Moon was covered in four shots, taking up only about twenty seconds of film time. 

===Coloring=== The Barber of Seville), some prints of A Trip to the Moon were individually Film colorization|hand-colored by Elisabeth Thuilliers coloring lab in Paris.    Thuillier, a former colorist of glass and celluloid products, directed a studio of two hundred people painting directly on film stock with brushes, in the colors she chose and specified; each worker was assigned a different color in assembly line style, with more than twenty separate colors often used for a single film. On average, Thuilliers lab produced about sixty hand-colored copies of a film. 

===Music=== The Barber Olympia music hall in Paris in 1902, an original film score was reportedly written for it. 
 Air (for Around the World in 80 Days). 

==Style==
{{multiple image
| align     = right
| direction = vertical
| width     = 250

| image1    = Voyage dans la Lune cliff still.jpg
| alt1      = 
| caption1  = Uncropped production still from the film, showing the edges of the backdrop and the floor of the studio

| image2    = Trip to the Moon Selenite on Shell.jpg
| alt2      = 
| caption2  = The scene as it appears in the hand-colored print of the film
}}
The films style, like that of most of Mélièss films, is deliberately theatrical, with a stylized mise en scéne recalling the traditions of the 19th-century stage, and filmed by a stationary camera placed to evoke the perspective of an audience member sitting in a theatre.   This stylistic choice was one of Mélièss first and biggest innovations. Although he had initially followed the popular trend of the time by making mainly actuality films (short "slice of life" documentary films capturing actual scenes and events for the camera), in his first few years of filming Méliès gradually moved into the far less common genre of fictional narrative films, which he called his scènes composées or "artificially arranged scenes."  The new genre was extensively influenced by Mélièss experience in theatre and magic, especially his familiarity with the popular French féerie stage tradition, and in an advertisement he proudly described the difference between his innovative films and the actualities still being made by his contemporaries: "these fantastic and artistic films reproduce stage scenes and create a new genre entirely different from the ordinary cinematographic views of real people and real streets."   

Because A Trip to the Moon preceded the development of narrative film editing by filmmakers such as  , in which time is treated as repeatable and flexible rather than linear and causal, is highly unconventional by the standards of Griffith and his followers, but before the development of continuity editing it was not uncommon for filmmakers to make similar experiments with time; Porter, for instance, used temporal discontinuity and repetition extensively in his 1902 film Life of an American Fireman.   Later in the twentieth century, with sports televisions development of the instant replay, temporal repetition again became a familiar device to screen audiences. 

Because Méliès does not use a modern cinematic vocabulary, some film scholars have created other frameworks of thought with which to assess his films; for example, some recent academicians, while not necessarily denying Mélièss influence on film, have argued that his works are better understood as spectacular theatrical creations rooted in the 19th-century stage tradition of the féerie.  Similarly, Tom Gunning has argued that to fault Méliès for not inventing a more intimate and cinematic storytelling style is to misunderstand the purpose of his films; in Gunnings view, the first decade of film history may be considered a "cinema of attractions," in which filmmakers experimented with a presentational style based on spectacle and direct address rather than on intricate editing. Though the attraction style of filmmaking declined in popularity in favor of a more integrated "story film" approach, it remains an important component of certain types of cinema, including science fiction films, musical film|musicals, and avant-garde films. 

==Themes==
 
 Release and Richard Abel describes the film as belonging to the féerie genre,  as does Frank Kessler. }} David Seed also considers the spaceship in the film to be one of the "key icons of science fiction, with its sleek rocket design, promising freedom and escape". 

However, A Trip to the Moon is also highly satirical in tone, poking fun at nineteenth-century science by exaggerating it in the format of an adventure story.  The film scholar Alison McMahan calls it one of the earliest examples of pataphysical film, saying it "aims to show the illogicality of logical thinking" with its satirically portrayed inept scientists, anthropomorphic moon face, and impossible transgressions of laws of physics.    The film historian Richard Abel believes Méliès aimed in the film to "invert the hierarchal values of modern French society and hold them up to ridicule in a riot of the carnivalesque".  Similarly, the literary and film scholar Edward Wagenknecht described the film as a work "satirizing the pretensions of professors and scientific societies while simultaneously appealing to mans sense of wonder in the face of an unexplored universe."   

There is also a strong anti-imperialist vein in the films satire.       The film scholar Matthew Solomon notes that the last part of the film (the parade and commemoration sequence missing in some prints) is especially forceful in this regard. He argues that Méliès, who had previously worked as an anti-Boulangist political cartoonist, mocks imperialistic domination in the film by presenting his colonial conquerors as bumbling pedants who mercilessly attack the alien lifeforms they meet and return with a mistreated captive amid fanfares of self-congratulation. The statue of Barbenfouillis shown in the films final shot even resembles the pompous, bullying colonialists in Mélièss political cartoons.  The film scholar Elizabeth Ezra agrees that "Méliès mocks the pretensions of colonialist accounts of the conquest of one culture by another," and adds that "his film also thematizes social differentiation on the home front, as the hierarchical patterns on the moon are shown to bear a curious resemblance to those on earth." 

==Release==
 
Méliès, who had begun A Trip to the Moon in May 1902, finished the film in August of that year and began selling prints to French distributors in the same month.  From September through December 1902, a hand-colored print of A Trip to the Moon was screened at Mélièss Théâtre Robert-Houdin in Paris. The film was shown after Saturday and Thursday matinee performances by Mélièss colleague and fellow magician, Jules-Eugène Legris, who appeared as the leader of the parade in the two final scenes.  Méliès sold black-and-white and color prints of the film through his Star Film Company,  where the film was assigned the catalogue number 399–411   and given the descriptive subtitle Pièce à grand spectacle en 30 tableaux.   In France, black-and-white prints sold for  560, and hand-colored prints for  1,000.  Méliès also sold the film indirectly through Charles Urbans Warwick Trading Company in London. 

Many circumstances surrounding the film—including its unusual budget, length, and production time, as well as its similarities to the 1901 New York attraction—indicate that Méliès was especially keen to release the film in the United States.   Because of rampant film piracy, Méliès never received most of the profits of the popular film.    One account reports that Méliès sold a print of the film to the Paris photographer Charles Gerschel for use in an Algiers theatre, under strict stipulation that the print only be shown in Algeria. Gerschel sold the print, and various other Méliès films, to the Edison Manufacturing Company employee Alfred C. Abadie, who sent them directly to Edisons laboratories to be duplicated and sold by Vitagraph. Copies of the print spread to other firms, and by 1904 Siegmund Lubin, the Selig Polyscope Company, and Edison were all redistributing it.  Edisons print of the film was even offered in a hand-colored version available at a higher price, just as Méliès had done.  Méliès was often uncredited altogether; for the first six months of the films distribution, the only American exhibitor to credit Méliès in advertisements for the film was Thomas Lincoln Tally,  who chose the film as the inaugural presentation of his Electric Theater. 
 an American branch of the Star Film Company, directed by his brother Gaston Méliès, in New York in 1903. The office was designed to sell Mélièss films directly and to protect them by registering them under United States copyright.    The introduction to the English-language edition of the Star Film Company catalog announced: "In opening a factory and office in New York we are prepared and determined energetically to pursue all counterfeiters and pirates. We will not speak twice, we will act!" 

In addition to the opening of the American branch, various trade arrangements were made with other film companies, including American Mutoscope and Biograph, the Warwick Trading Company, the Charles Urban Trading Co., Robert W. Pauls studio, and Gaumont Film Company|Gaumont.  In these negotiations, a print sale price of  0.15 per foot was standardized across the American market, which proved useful to Méliès; later price standardizations by the Motion Picture Patents Company in 1908 hastened Mélièss financial ruin, as his films were impractically expensive under the new standards. In addition, in the years following 1908 his films suffered from the fashions of the time, as the fanciful magic films he made were no longer in vogue. 

==Reception== Olympia music hall in Paris for several months.   

A Trip to the Moon was met with especially large enthusiasm in the United States, where (to Mélièss chagrin) its piracy by Lubin, Selig, Edison and others gave it wide distribution. Exhibitors in New York City, Washington D.C., Cleveland, Detroit, New Orleans, and Kansas City reported on the films great success in their theaters.    The film also did well in other countries, including Germany, Canada, and Italy, where it was featured as a headline attraction through 1904. 

A Trip to the Moon was one of the most popular films of the first few years of the twentieth century, rivaled only by a small handful of others (similarly spectacular Méliès films such as The Kingdom of the Fairies and The Impossible Voyage among them).    Late in life, Méliès remarked that A Trip to the Moon was "surely not one of my best," but acknowledged that it was widely considered his masterpiece and that "it left an indelible trace because it was the first of its kind."  The film which Méliès was proudest of was Humanity Through the Ages, a serious historical drama now presumed lost film|lost. 

==Rediscovery==

===Black-and-white print===
 
After Mélièss financial difficulties and decline, most copies of his prints were lost. In 1917, his offices were occupied by the French military, who melted down many of Mélièss films to gather the traces of silver from the film stock and make boot heels from the celluloid. When the Théâtre Robert-Houdin was demolished in 1923, the prints kept there were sold by weight to a vendor of second-hand film. Finally, in that same year, Méliès had a moment of anger and burned all his remaining negatives in his garden in Montreuil.  In 1925, he began selling toys and candy from a stand in the Gare Montparnasse in Paris.  A Trip to the Moon was largely forgotten to history and went unseen for years. 

Thanks to the efforts of film history devotées, especially  , and Paul Gilson, Méliès and his work were rediscovered in the late 1920s. A "Gala Méliès" was held at the Salle Pleyel in Paris on 16 December 1929 in celebration of the filmmaker, and he was awarded the Legion of Honor in 1931.  During this renaissance of interest in Méliès, the cinema manager Jean Mauclaire and the early film experimenter Jean Acme LeRoy both set out independently to locate a surviving print of A Trip to the Moon. Mauclaire obtained a copy from Paris in October 1929, and LeRoy one from London in 1930, though both prints were incomplete; Mauclaires lacked the first and last scenes, and LeRoys was missing the entire final sequence featuring the parade and commemorative statue. These prints were occasionally screened at retrospectives (including the Gala Méliès), avant-garde cinema showings, and other special occasions, sometimes in presentations by Méliès himself.   

Following LeRoys death in 1932, his film collection was bought by the Museum of Modern Art in 1936; the museums acquisition and subsequent screenings of A Trip to the Moon, under the direction of MoMAs film curator Iris Barry, opened the film up once again to a wide audience of Americans and Canadians  and established it definitively as a landmark in the history of cinema.    LeRoys incomplete print became the most commonly seen version of the film and the source print for most other copies, including the Cinémathèque françaises print.  A complete version of the film, including the entire celebration sequence, was finally reconstructed in 1997 from various sources by the Cinémathèque Méliès, a foundation set up by the Méliès family. 

===Hand-colored print===
 
No hand-colored prints of A Trip to the Moon were known to survive until 1993, when one was given to the Filmoteca de Catalunya by an anonymous donor as part of a collection of two hundred silent films.    It is unknown whether this version, a hand-colored print struck from a second-generation negative, was colored by Elisabeth Thuilliers lab, but the perforations used imply that the copy was made before 1906. The flag waved during the launching scene in this copy is colored to resemble the flag of Spain, indicating that the hand-colored copy was made for a Spanish exhibitor. 

In 1999, Anton Gimenez of the Filmoteca de Catalunya mentioned the existence of this print, which he believed to be in a state of total decomposition, to Serge Bromberg and Eric Lange of the French film company Lobster Films. Bromberg and Lange offered to trade a recently rediscovered film by Segundo de Chomón for the hand-colored print, and Gimenez accepted. Bromberg and Lange consulted various specialist laboratories in an attempt to restore the film, but because the reel of film had apparently decomposed into a rigid mass, none believed restoration to be possible. Consequently, Bromberg and Lange themselves set to work separating the film frames, discovering that only the edges of the film stock had decomposed and congealed together, and thus that many of the frames themselves were still salvageable.    Between 2002 and 2005, various digitization efforts allowed 13,375 fragments of images from the print to be saved.  In 2010, a complete restoration of the hand-colored print was launched by Lobster Films, the Groupama Gan Foundation for Cinema, and the Technicolor Foundation for Cinema Heritage.  The digitized fragments of the hand-colored print were reassembled and restored, with missing frames recreated with the help of a black-and-white print in the possession of the Méliès family, and time-converted to run at an authentic silent-film speed, 14 frames per second. The restoration was completed in 2011    at Technicolors laboratories in Los Angeles. 

The restored version premiered on 11 May 2011, eighteen years after its discovery and 109 years after its original release, at the 2011 Cannes Film Festival, with a new soundtrack by the French band Air (French band)|Air.    The restoration was released by Flicker Alley in a 2-disc Blu-Ray and DVD edition also including The Extraordinary Voyage, a feature-length documentary by Bromberg and Lange about the films restoration, in 2012.  In The New York Times, A. O. Scott called the restoration "surely a cinematic highlight of the year, maybe the century." 

==Legacy==
 
 

As A Short History of Film notes, A Trip to the Moon combined "spectacle, sensation, and technical wizardry to create a cosmic fantasy that was an international sensation."  It was profoundly influential on later filmmakers, bringing creativity to the cinematic medium and offering fantasy for pure entertainment, a rare goal in film at the time. In addition, Mélièss innovative editing and special effects techniques were widely imitated and became important elements of the medium.    The film also spurred on the development of cinematic science fiction and fantasy by demonstrating that scientific themes worked on the screen and that reality could be transformed by the camera.     In a 1940 interview, Edwin S. Porter said that it was by seeing A Trip to the Moon and other Méliès films that he "came to the conclusion that a picture telling a story might draw the customers back to the theatres, and set to work in this direction."  Similarly, D. W. Griffith said simply of Méliès: "I owe him everything."    Since these American directors are widely credited with developing modern film narrative technique, the literary and film scholar Edward Wagenknecht once summed up Mélièss importance to film history by commenting that Méliès "profoundly influenced both Porter and Griffith and through them the whole course of American film-making." 

It remains Mélièss most famous film as well as a classic example of early cinema, with the image of the capsule stuck in the Man in the Moons eye particularly well-known.    The film has been evoked in other creative works many times,  ranging from Segundo de Chomóns 1908 unauthorized remake Excursion to the Moon  through the extensive tribute to Méliès and the film in the Brian Selznick novel The Invention of Hugo Cabret and its 2011 Martin Scorsese film adaptation Hugo (film)|Hugo.  Film scholar Andrew J. Rausch includes A Trip to the Moon among the "32 most pivotal moments in the history of  ," saying it "changed the way movies were produced."  Chiara Ferraris essay on the film in 1001 Movies You Must See Before You Die, which places A Trip to the Moon as the first entry, argues that the film "directly reflects the histrionic personality its director", and that the film "deserves a legitimate place among the milestones in world cinema history." 

==References==

===Notes===
 

===Citations===
 

===Bibliography===
 
* 
* 
*  
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
 

==External links==
 
*  
*  
*  
*   — A letter to NASA
*  
*  
*   on Hulu
*   complete film on YouTube
*   complete film on YouTube

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 