Bicentennial Man (film)
{{Infobox film 
| name           = Bicentennial Man
| image          = Bicentennial_man_film_poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Promotional poster Chris Columbus
| producer       = Chris Columbus Wolfgang Petersen Gail Katz Laurence Mark Neal Miller Mark Radcliffe Michael Barnathan
| screenplay     = Nicholas Kazan
| narrator       = 
| based on       =    
| starring       = Robin Williams Sam Neill Embeth Davidtz Wendy Crewson Oliver Platt
| music          = James Horner
| cinematography = Phil Méheux
| editing        = Neil Travis
| studio         = Touchstone Pictures Columbia Pictures 1492 Pictures Laurence Mark Productions   Radiant Productions Buena Vista Columbia TriStar Film Distributors International        
| released       =  
| runtime        = 132 minutes
| country        = United States
| language       = English
| budget         = $100 million
| gross          = $87,423,861
}}
 science fiction family comedy-drama Chris Columbus. the year that the United States|U.S. had its United States Bicentennial|bicentennial.

==Plot== android "Andrew" (Robin Williams) joins the Martin family home to perform housekeeping and maintenance duties. The familys reactions range from acceptance and curiosity to outright rejection and deliberate vandalism by the eldest child, Grace (Lindze Letherman), which leads to the discovery that Andrew can both identify emotions and reciprocate in kind. When Andrew accidentally breaks a figurine belonging to Graces sister, "Little Miss" Amanda (Hallie Kate Eisenberg), he carves a replacement out of wood. The family is astonished by this creativity and “Sir” Richard Martin (Sam Neill) takes Andrew to NorthAm Robotics to inquire if all the robots are like him. The companys CEO (Stephen Root) sees this development as a problem and wishes to scrap Andrew. Instead, "Sir" takes Andrew home and allows him to pursue his own development.

In 2025, Andrew has an accident in which his thumb is accidentally cut off, and Sir again takes him to NorthAm for repairs. Andrew requests that, while he is being repaired, his face be upgraded to allow him to convey the emotions he feels but cannot express. The CEO informs them that upgrade modification will be very expensive, but the price is well within the Martin familys means. Andrew is upgraded just in time for the wedding of Little Miss (Embeth Davidtz).

In 2037, Andrew realizes there are no more orders for him to run, so he asks for his freedom, much to Sirs dismay. His elderly owner grants the request but banishes Andrew so he can be "completely" free. Andrew eventually builds himself a home at the beach and lives alone. In 2053, Andrew sees Sir one last time. On his deathbed, Sir apologizes for banishing Andrew, knowing that letting him have his freedom was the right thing.

After reluctant help from Lloyd Charney (Bradley Whitford), Little Misss son, Andrew attempts to locate more NDR series robots to discover if others have also developed sentience. After more than a decade of futility, he finds Galatea (Kiersten Warren), an NDR robot that has been given feminine attributes and personality as part of her programming. Galatea is owned by Rupert Burns (Oliver Platt), the son of the original NDR robot designer. Rupert works to create a more human look for robots but is unable to attract funding. Andrew agrees to finance the research and the two work to give Andrew a superficial human appearance.

In 2073, Andrew comes back to visit but finds Little Miss has aged significantly and meets Portia Charney (Embeth Davidtz), her granddaughter (and Lloyds daughter) who looks almost exactly like a younger version of Little Miss due to a genetic likeness. As Andrew gets to know Portia, Little Miss is hospitalized after suffering a stroke. Andrew and Portia visit her, noticing that she is clutching the wooden horse Andrew carved for her when she was young. After Little Miss passes away, Andrew feels the pain of not being able to cry and realizes that every human being he cares for will eventually die.
 tactile sensations and taste. Meanwhile, his friendship with Portia evolves into romance. When Andrew and Portia realize that their relationship would never be socially accepted, Andrew petitions the World Congress to recognize him as a human being, which would allow him and Portia to be legally married. The Congress Speaker rejects the proposal, however, arguing that while society can tolerate an everlasting machine, an immortal human would create too much jealousy and resentment, thus leaving Andrew and Portia unable to be married at the very least.

Years later, Andrews medical breakthroughs allow Portia to age more gradually, but she decides that she doesnt want to have her life prolonged forever. Realizing that he wouldnt want to live without her, Andrew asks Rupert to introduce blood into his system which will cause his brain to gradually decay and allow him to age. Decades later, Andrew and Portia are physically elderly. Andrew meets with the World Congress a second time to once again petition to be declared human with Portia watching. This time, the Congress President decides to review the case before making a final determination.

Sometime afterwards, Andrew and Portia reside in a nursing home with a human-looking Galatea as their nurse. As they listen to a broadcast, the Congress President finally acknowledges Andrews humanity by declaring that the 200 year old is ("with the exception of Methuselah and other biblical figures") the oldest human being in recorded history and validates his marriage to Portia. Despite his life support machine, Andrew unfortunately dies peacefully while listening to the broadcast. Afterwards, Portia asks Galatea to unplug her from life support and then dies hand-in-hand with Andrew after saying "Ill see you soon".

==Cast==
* Robin Williams as Andrew Martin
* Sam Neill as Richard "Sir" Martin
* Embeth Davidtz as Amanda "Little Miss" Martin (adult) and Portia Charney
* Wendy Crewson as Rachel "Ma´am" Martin
* Hallie Kate Eisenberg as Amanda "Little Miss" Martin (age 7) 
* Kiersten Warren as Galatea
* Oliver Platt as Rupert Burns
* Stephen Root as Dennis Mansky
* Angela Landis as Grace "Miss" Martin (adult) 
* Bradley Whitford as Lloyd Charney (adult) 
* John Michael Higgins as Bill Feingold
* Lindze Letherman as Grace "Miss" Martin (age 9)
* Igor Hiller as Lloyd Charney (age 10) 
* George D. Wallace as the first President/Speaker of the World Congress
* Lynne Thigpen as Marjorie Bota, the second President/Speaker of the World Congress

==Reception==
Bicentennial Man received mixed reviews; the film holds a 37% approval rating on Rotten Tomatoes with 35 out of 93 critics giving it a positive review, with an average rating of 4.8 out of 10.  Its consensus states that Bicentennial Man is ruined by a bad script and ends up being dull and mawkish, while the review aggregator Metacritic  gives it a score of 42. 

Roger Ebert gave it two out of four stars, saying, "Bicentennial Man begins with promise, proceeds in fits and starts, and finally sinks into a cornball drone of greeting-card sentiment. Robin Williams spends the first half of the film encased in a metallic robot suit, and when he emerges, the script turns robotic instead. What a letdown."  William Arnold of Seattle Post-Intelligencer said the film "Becomes a somber, sentimental and rather profound romantic fantasy that is more true to the spirit of the Golden Age of science-fiction writing than possibly any other movie of the 90s." Todd McCarthy of Variety (magazine)|Variety summed it up as "An ambitious tale handled in a dawdling, sentimental way".

==Accolades==
* Academy Awards — Best Makeup (lost to Topsy-Turvy) Big Daddy)   
* Blockbuster Entertainment Award — Favorite Actress — Comedy (Embeth Davidtz) (lost to Drew Barrymore in Never Been Kissed)  Sleepy Hollow)
* Nickelodeon Kids Choice Awards — Favorite Movie Actor (Robin Williams) (lost to Adam Sandler in Big Daddy)
* Razzie Award — Worst Actor (Robin Williams) (lost to Adam Sandler in Big Daddy) Where the Heart Is)

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 