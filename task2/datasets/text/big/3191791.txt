Max, 13
{{Infobox Film
| name           = Max, 13
| image          = 
| image_size     = 
| caption        = 
| director       = Abe Levy
| producer       = Abe Levy
| writer         = Abe Levy
| narrator       = 
| starring       = Max Hurwitz Jorja Dwyer Christopher Jaymes 
| music          = Jeff Grove
| cinematography = Ruben OMalley
| editing        = Jacob Bricca Jennifer Matson
| distributor    = Colorfast Pictures
| released       = 
| runtime        = 
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1999  director by Tomales native Abe Levy, revolving around a 13 year old boy coming of age in rural Northern California.   This film featured Marshall resident, Max Hurwitz, who continues to reside in Marshall as well as Petaluma. 

==Cast==
*Max Hurwitz ...  Max 
*Jorja Dwyer ...  Claire 
*Christopher Jaymes ...  Daniel 
*April Daniels ...  Valerie  Josh Faure-Brac ...  Camera Store Clerk 
*Daedalus Howell ...  Ray 
*Kandis Kozolanka ...  Mrs. Eggles 
*Aerielle Levy ...  Florida 
*Greg Marquardt ...  Harvey 
*Christine Renaudin ...  Frida  Devon Rumrill ...  Floyd 
*Nick Scott ...  Dallas 
*Shaina Solomon ...  Sasha  Josh Staples ...  Ronnie  Sienna SZell ...  Marianne 
*Robert Vandermaaten ...  Jonathan

==Additional sources==
* , (about Levy) "North Bay filmmakers forge a new cinematic scene: Abe Levy and Silver Tree ...Lifelong Sonoma County resident Abe Levy has proven himself a filmmaker to the degree that he actually has a second home in Hollywood...."
* : "09.15.99, FINAL CUT: Tomales-borne director Abe Levy unthaws the freeze-frame on his coming-of-age opus "Max, 13 this weekend at the Phoenix Theater"
* : "Ever since 1973, Petaluma has served as a location for many major films, including: ...Max, 13 (1997) 4 day shoot in Petaluma and 10 day shoot in Tomales..."
* : "Some of the movies in which you can find scenes of Petaluma are: ...Max, 13 (1997)..."
* : (about Levy) "Abe Levy has directed a handful of feature films"
* : (about the editor) "Jeni Matson edited Abe Levys acclaimed debut feature, MAX, 13"

==References==
 

==External links==
 

 
 
 


 