The Medicine Show
{{Infobox Hollywood cartoon|
| cartoon_name = The Medicine Show
| series = Krazy Kat
| image = 
| caption = 
| director = Manny Gould Ben Harrison
| story_artist = 
| animator = Allen Rose Preston Blair
| voice_actor = 
| musician = Joe de Nat
| producer = Charles Mintz
| studio = 
| distributor = Columbia Pictures 
| release_date = February 7, 1933
| color_process = Black and white
| runtime = 6:40 English
| preceded_by = Wedding Bells
| followed_by = Wooden Shoes
}}

The Medicine Show is a short animated film by Columbia Pictures. The film is part of a long-running short film series featuring Krazy Kat.

==Plot== python dances. His second act features a lady wolf being inside a wooden box which Krazy saws into four pieces. But instead of his assistant being divided, four small lady wolves pop out of each piece. When the parts of the wooden box are placed back together, the lady wolf comes out back in her one self.

After doing his acts, Krazy starts retailing his bottles of liquid medicine. One of his customers is an old beagle. The old beagle, after consuming a bottle, suddenly becomes very able and dances very fluently but also transforms into a small puppy who merrily leaves in a scooter.

Krazys second customer is a pet dog with a flea problem. Krazy pours liquid from one of his bottles onto the pet dog. The fleas start to jump out and leave their host.

As more customers flock to Krazys wagon, a rat can be seen drinking one of his bottles. The rat, for some reason, transforms into a vicious lion. The lion scares everybody, including Krazy, away from the scene.

The lion continues to chase Krazy for many yards. On the way, the lion comes across a pack of dachshunds. The dachshunds try to outrun the lion but the big cat swallows them. The dachshunds are able to exit the lions mouth. But as they come out, the little dogs have been transformed into little lions. Despite their size, the little lions manage to scare away their swallower. The little lions then turn to and confront Krazy. But as they jump forth, Krazy traps the tiny beasts in a bottle.

==See also==
*Medicine show

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 

 