The Count of Luxemburg (1957 film)
The German musical musical comedy film directed by Werner Jacobs and starring Gerhard Riedmann, Renate Holm and Gunther Philipp.  It is based on the operetta Der Graf von Luxemburg by Franz Lehár.

==Cast==
* Gerhard Riedmann ...  René, Graf von Luxemburg 
* Renate Holm ...  Angèle, Sängerin 
* Gunther Philipp ...  Brissard, Maler 
* Susi Nicoletti ...  Caroline, Erbgräfin von Luxemburg 
* Gustav Knuth ...  Fürst Basil Basilowitsch 
* Alice Kessler ...  Fritzi 
* Ellen Kessler ...  Franzi 
* Hans Olden ...  Paul Pawlowitsch, Gesandter 
* Erika von Thellmann ...  Frau Pawlowitsch
* Theodor Danegger ...  Großfürst Michail Michailowitsch 
* Hugo Lindinger ...  van Megeren, Gerichtsvollzieher 
* Herbert Weissbach ...  Hassling, Gerichtsvollzieher 
* Gerold Wanke ...  Robert, Bildhauer 
* Gustl Weishappel ...  Albert, Maler 
* Clarissa Stolz ...  Sophia Laurentia

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 


 
 