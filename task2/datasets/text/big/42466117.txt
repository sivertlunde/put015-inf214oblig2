Balkan Rhapsodies: 78 Measures of War
Balkan Rhapsodies: 78 Measures of War is a 2007 documentary by Jeff Daniel Silva about the Kosovo War.

== Synopsis ==
Balkan Rhapsodies is a "ruminative experimental mosaic",  exploring the Kosovo War through encounters with recovering civilians, observational moments, and Silvas own reflections on his various trips through war-torn Serbia and Kosovo.

== About the Filmmaker ==
Jeff Daniel Silva was the first United States Citizen granted access into the territory that was formerly known as Yugoslavia. His access was granted just weeks after the North Atlantic Treaty Organization (NATO) launched a military operation that dropped bombs over the Federal Republic of Yugoslavia. 

== Commentary ==
Lisa Stevenson, Professor of Anthropology, McGill University says, 
"Balkan Rhapsodies: 78 Measures of War deftly juxtaposes the everyday and the exceptional, the sublime and the horrific, into a film that challenges us to reconsider what we thought we knew about war, about humanitarianism and about our own good intentions. It challenges us as students and scholars of the humanities and social sciences to interrogate our own process of meaning making - how we come to know what we do about war, and especially how we come to know and consume the pain and suffering of others. Balkan Rhapsodies is a beautiful and disturbing film, a film whose afterimage should provoke us to think about war and humanitarianism in a more sophisticated, and ultimately more compassionate way." 

 
Branka Bogdanov, Director of Film and Video, Institute of Contemporary Art, Boston says, 
"Its become a cliché that every documentary film/video tells a story, but what and whose story does it tell? Experimental filmmaker Jeff Daniel Silva, with his documentary Balkan Rhapsodies, exercises an exceptional talent to capture a kaleidoscopy of life in the post-war Serbia, after the NATO bombing in 1999. With this film Silva strives to crackle beneath the façade of ordinary lives, opening up a whole landscape of disillusionment and hope, humor and pain, and above all else succeeds in vividly capturing the bruised acceptance of a people in the aftermath of war. With its unique multilayered structure Balkan Rhapsodies remains open to interpretation and most importantly, each of the seventy-eight Rhapsodies is its own reality. Kudos for a filmmaker who exercises a profound commitment to the proposition that film is a vital manifestation and a document of the political volatile time we live in."

== Screenings and Awards ==
* Basil Wright Prize RAI Film Festival, 2009
* "Visual Representation of Crisis through Ethnographic Film," EASA Biennial Conference, Ireland, 2010
* AAA/Society for Visual Anthropology Film, Video & Multimedia Festival, 2009
* 11th RAI International Festival of Ethnographic Film Leeds, 2009
* Harvard Film Archive, Cambridge, MA, 2008
* Göttingen International Ethnographic Film Festival, Göttingen, Germany, 2008
* Best Documentary, Festival Cinemateca Uruguaya, Montevideo, Uruguay, 2008
* Museum of Modern Art (MoMA), NY - Doc Fortnights, 2008
* OVNI Arxius de lObservatori, Barcelona, Spain, 2008
* DocHouse Brussels, Belgium, 2007
* ForumDocBH, Belo Horizonte, Brazil, 2007
* Valdivia International Film Festival, Chile, 2007
* DokumentART Festival, Neubrandenburg, Germany, 2007

== External links ==
*  
*  
*  

== References ==
 

 
 
 
 