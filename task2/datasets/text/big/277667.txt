Scary Movie
 
{{Infobox film
| name           = Scary Movie
| image          = Movie poster for "Scary Movie".jpg
| caption        = Theatrical release poster
| alt            =
| director       = Keenen Ivory Wayans
| producer       = Eric L. Gold Lee R. Mayes Jason Friedberg Aaron Seltzer Dave Sheridan Kurt Fuller Carmen Electra 
| music          = David Kitay
| cinematography = Francis Kenny Mark Helfrich
| studio         = Dimension Films Wayans Bros. Entertainment Gold/Miller Productions Brad Grey Pictures Miramax Films
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $19 million
| gross          = $278 million
}} horror comedy spoof film American Black dark comedy parodies the mystery genres. Several mid- and late-90s films and TV shows are spoofed, especially Scream (1996 film)|Scream, along with I Know What You Did Last Summer, Buffy the Vampire Slayer, The Sixth Sense, The Usual Suspects, The Matrix, The Blair Witch Project, and Dawsons Creek.

The tagline reads "No mercy. No shame. No sequel.", the last reference being an ironic nod towards the tendency of popular horror movies becoming cash cow franchises. 2001 saw the release of Scary Movie 2, with the appropriate tagline "We lied". Later video covers of the first film frequently drop the taglines third statement. The film was originally titled "Last Summer I Screamed Because Halloween Fell on Friday the 13th".  Scary Movie was followed by four more sequels Scary Movie 2 (2001), Scary Movie 3 (2003), Scary Movie 4 (2006) and Scary Movie 5 (2013).  Its title serves as a homage to the production title of Scream (1996 film)|Scream, which was also released through Dimension Films.

==Plot==
An 18-year-old girl named Drew Decker (Carmen Electra) receives a threatening phone call while home alone one night. Drew is chased outside by Ghostface (Scream)|Ghostface, who wounds her with a stab to the breast that removes her implant. She is hit by a vehicle driven by her father, who was distracted by oral sex from his wife, and is then killed by Ghostface.
 Dave Sheridan) hoping to milk the facts out of him.

While Cindy is in class, she receives a note reading: "I KNOW WHAT YOU DID LAST HALLOWEEN!" She then realizes that Drew was murdered exactly one year after she and her friends accidentally killed a man during a wild car ride. The next day a series of increasingly bizarre events take place. Various members of the group receive threatening notes from Ghostface and are rapidly dispatched.

Greg is killed by Ghostface in plain view during Buffys beauty pageant, with the audience mistaking Buffys screams as being part of her act. Buffy, high on the success brought along by her win, ignores Cindys warnings about the killer and does not realize she is being killed as it happens, even after she is decapitated (and the killer tosses her head in a Lost and Found bin, though her head keeps talking). Ray and Brenda go to a showing of Shakespeare In Love, where Ray is stabbed in the ear through a bathroom stall.  Ghostface then goes after Brenda.  Unfortunately, angry movie patrons, fed up with Brendas rude behavior during the movie, kill her before Ghostface can.

Meanwhile, Cindy throws a house party, hoping for safety in numbers. During the party, Bobby and Cindy go upstairs and they have sex, no longer making Cindy a virgin.  Ghostface unexpectedly appears and stabs Bobby, before disappearing quickly. Cindy gets a gun from a drawer near the entrance, Bobby follows and she tends to his wounds. Brendas stoner brother Shorty (Marlon Wayans) comes up from the basement and informs them that the killer has murdered everybody in the house.

Bobby takes the gun and shoots Shorty, revealing that his wound was an elaborate ruse. Ray arrives on the scene, alive; he and Bobby announce their plan to Cindy to kill her and her father (despite the fact that they are not actually the killers). Ray and Bobby also plan to make themselves look like heroes by giving each other stab wounds to indicate they fought back, but the plan backfires when Ray stabs Bobby repeatedly and nearly kills him, furious because his favorite show, The Wayans Bros., has been cancelled. Ghostface abruptly arrives and attacks Cindy during a karate fight after stabbing Ray, but she successfully subdues him by employing moves copied from The Matrix (and by doing a brief Irish jig while levitating) and kicks him through a window. Nonetheless, Ghostface vanishes before the police arrive.

At the police station, Cindy and the local sheriff (Kurt Fuller) realize that Doofy, the only one who knew about the car accident, was actually faking his disability and is the true killer. Unfortunately, Doofy has already escaped with Gail Hailstorm. Upon finding his discarded disguise in the street, Cindy begins screaming, only to be hit by a car.

In a post credit scene, Shorty appears and breaks the fourth wall by giving tips on how to survive a horror movie. The ways tips he gives is you got to be quick, dont fall down and the most important one is to never look back. After that he yells "Snatch and run yall!" then him and his friends turn out to be at a gas station store and steal the food and drinks from the gas station as the man at the cash register yells after them to stop.

==Cast==
  Cindy Campbell Brenda Meeks Ray Wilkins Shorty Meeks
*Jon Abrahams as Bobby Prinze
*Shannon Elizabeth as Buffy Gilmore
*Lochlyn Munro as Greg Phillipe Dave Sheridan Ghostface
*Cheri Oteri as Gail Hailstorm
*Kurt Fuller as The Sheriff
*Carmen Electra as Drew Decker
*Rick Ducommun as Mr. Campbell
*Jayne Trcka as Miss Mann
*Kelly Coffield Park as the History Teacher David L. Lander as Principal "Squiggy" Squiggman
*Marissa Jaret Winokur as Tina
*Keenen Ivory Wayans as Slave
*Robert Jacks as Rowdy (uncredited)
*James Van Der Beek as Dawson Leery (uncredited) Prince
 

==Parodies== Oscar nominee The Shining, before he kills Buffy. The line "We all go a little crazy sometimes" is also used, which is taken from Scream quoting Psycho (1960 film)|Psycho.
 American Pie.  A trailer for a fictitious sequel to Amistad (film)|Amistad titled Amistad II with elements of Titanic (1997 film)|Titanic appears in the movie theater scene. 

The film also makes other pop culture references beyond the scope of film, including a brief send-up of Dawsons Creek    and a parody of the Whassup? ad campaign by Budweiser (Anheuser-Busch)|Budweiser.  Drew (Carmen Electra)s boyfriend at the beginning of the movie, the one she "slept with but didnt date" was Prince (musician)|Prince, who Electra dated in real life.

{| class="wikitable sortable"
|-
! Subjects parodied or referenced
|-
| I Know What You Did Last Summer, I Still Know What You Did Last Summer
|-
| Scream (1996 film)|Scream, Scream 2, Scream 3
|-
| The Sixth Sense
|-
| The Blair Witch Project
|-
| The Exorcist
|-
| Election (1999 film)|Election
|-
| Kazaam
|- The Shining
|-
| Halloween (1978 film)|Halloween
|-
| Shakespeare in Love
|-
| Amistad (film)|Amistad
|-
| Thelma & Louise
|- American Pie
|-
| The Matrix
|- The Fugitive
|-
| Big Mommas House
|-
| Titanic (1997 film)|Titanic
|-
| Crouching Tiger, Hidden Dragon
|-
| The Usual Suspects
|-
| Schindlers List
|-
| Boogie Nights
|-
| The Wayans Bros.
|-
| Psycho (1960 film)|Psycho
|-
| Whassup?
|-
| Dawsons Creek
|-
| Jackie Chan filmography
|-
|}

==Rating== provincial FCO, but was re-rated on appeal by the Motion Picture and Liquor Appeal Board to a 14A. This resulted in a record number of complaints to the British Columbia Film Classification Office from parents who felt the film should have been rated 18A. Many parents wrote letters to their local newspaper warning others that the film may be inappropriate for their fourteen-year-olds. Theatre owners complained about the inappropriate rating as well. 

==Reception   ==

The film received mixed reviews from critics. On Rotten Tomatoes, 54% of critics gave the film a positive review based on 112 reviews. 

Joe Leydon of Entertainment Weekly gave the film a positive review, remarking that the film was "unbounded by taste, inhibition or political correctness" and that "the outer limits of R-rated respectability are stretched, if not shredded" by the movie.  By contrast, Roger Ebert did not find the film as innovative, saying that the film lacked "the shocking impact of Airplane!, which had the advantage of breaking new ground."  However, Ebert did give the film 3 stars out of 4, saying it "delivers the goods," calling the film a "raucous, satirical attack on slasher movies."

Bob Longino of The Atlanta Journal-Constitution felt that the films crude humor detracted from the film, saying that Scary Movie "dives so deep into tasteless humor that its a wonder it landed an R rating instead of an NC-17."    Other reviewers, such as A.O. Scott of The New York Times, argued that the jokes were "annoying less for their vulgarity than for their tiredness."    Scott remarked in his review, "Couch-bound pot smokers, prison sex, mannish female gym teachers, those Whassssup Budweiser commercials -- hasnt it all been done to death?". 

==Soundtrack==
{{Infobox album  
| Name        = Scary Movie
| Type        = soundtrack
| Artist      = Various artists
| Cover       =
| Released    = July 4, 2000
| Recorded    = 1999-2000 Hip hop, Alternative rock
| Length      = 55:15 TVT
| Producer    =  
| Last album  = 
| This album  = 
| Next album  = 
}}

{{Album ratings
| rev1      = AllMusic
| rev1Score =   
}}

The soundtrack to Scary Movie was released on July 4, 2000 through TVT Records and consists of a blend of hip hop and rock music.

;Track listing
#" )  
#" ) 
#"Stay"- 3:56 (Radford (band)|Radford) 
#"The Only Way to Be"- 3:20 (Save Ferris)  
#"My Bad"- 3:22 (Oleander (band)|Oleander) 
#"Punk Song #2"- 2:46 (Silverchair) 
#" ) 
#"Superfly"- 2:55 (Bender (band)|Bender) 
#" ) 
#"Scary Movies (Sequel)"- 3:56 (Bad Meets Evil) Yaki Kadafi, Hussein Fatal, Nate Dogg & Dru Down)  Black Eyed Peas)  Public Enemy)   Rampage & Rock (rapper)|Rock) 
#"Im the Killer"- 3:57 (Lifelong & Incident)

==See also==
 
* Shriek If You Know What I Did Last Friday the Thirteenth — A parody of horror movies
* Student Bodies — A parody of horror movies
* Stan Helsing — A parody of horror movies
 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 