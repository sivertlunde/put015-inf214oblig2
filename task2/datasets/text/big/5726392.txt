The Glass Shield
 
{{Infobox film
| name           = The Glass Shield
| image          = Glass shield movie poster.jpg
| image_size     =
| caption        = Theatrical releaseposter Charles Burnett
| producer       = Thomas S. Byrnes Carolyn Schroeder
| writer         = Charles Burnett John Eddie Johnson Ned Welsh
| narrator       =
| starring       = Ice Cube Elliott Gould Michael Boatman Lori Petty
| music          = Stephen James Taylor
| cinematography = Elliot Davis
| editing        = Curtiss Clayton
| distributor    = Miramax Films
| released       = June 2, 1995
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = $8 million
| gross          = $3,291,163
}} Charles Burnett. 

==Plot==
Deputy John Johnson (J.J.) (Michael Boatman) is a rookie in the Los Angeles Sheriffs Department and the first black deputy at the station to which he is assigned. Racial tensions run high in the department as some of J.J.s fellow officers resent his presence. His only real friend is the other new deputy (Lori Petty), the first female officer to work there, who also suffers similar discrimination in the otherwise all-white-male work environment. When J.J. becomes increasingly aware of police corruption during the murder trial of Teddy Woods (Ice Cube), who he helped to arrest, he faces difficult 
decisions and puts himself into grave personal danger in the service of justice.

==Cast==
* Elliott Gould as Greenspan
* Ice Cube as Teddy Woods
* Michael Ironside as Detective Gene Baker
* Lori Petty as Deputy Deborah Fields
* M. Emmet Walsh as Detective Jesse Hall
* Erich Anderson as District Attorney Ira Kern
* Richard Anderson as Watch Commander Clarence Massey
* Thomas Babson as U.S. Marshall
* Monty Bane as Coroner
* Ernie Lee Banks as Mr. Woods
* Michael Boatman Deputy J.J. Johnson
* Jean Hubbard-Boone as Mrs. Woods
* James Boyce as Paramedic #1
* Gaye Shannon-Burnett as Woman Driver
* Bernie Casey as James Locket
* Linden Chiles as Sergeant Berry Foster
* Janet Claire as Juror
* Victor Contreras as Mr. Cruz
* Wanda De Jesus as Carmen Munoz
* Bill Dearth as Guard

==Awards==

===Nominations===
* Locarno International Film Festival: Golden Leopard, Charles Burnett; 1994.

==References==
 

==External links==
*  
*  
*  
*   film review by Hal Hinson at The Washington Post

 
 
 
 
 
 
 
 
 


 