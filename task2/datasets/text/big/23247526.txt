Panchalankurichi (film)
{{Infobox film
| name = Panchalankurichi 
| image =
| caption = Official DVD Box Cover Seeman
| producer = K. Balu Seeman
| narrator = Prabhu Madhoo|Madhubala Chandrasekar Prasanna Vijayakumar Ilavarasi Maheswari Deva
| cinematography = Ilavarasu
| editing = K. Pazhanivel
| studio = K. B. Films
| distributor = K. B. Films
| released = 10 November 1996
| runtime =
| country = India Tamil
| budget =
| gross = $1 million
| preceded_by =
| followed_by =
}}
 1996 Indian Seeman starring Prabhu Ganesan, Madhoo|Madhubala, Vagai Chandrasekhar|Chandrasekar, Prasanna, Thulukanam, Vadivelu, Vijayakumar (actor)|Vijayakumar, Ilavarasi, Maheswari. 

==Soundtrack==

{|class="wikitable"
! Song !! Singer !! Duration
|----
|Ana Aavanna
|s.b.b
|4:55
|---
|Asai Vaithen Swarnalatha
|6:13
|---
|Chinna Chinna
|n/a
|5:08
|---
|Chinnavale
|n/a
|5:41
|---
|Katrai Niruthi
|n/a
|2:37
|---
|Oru Pakkam Then
|n/a
|2:23
|---
|Un Uthattora Hariharan (singer)|Hariharan, Anuradha Sriram
|6:00
|---
|Vantheyalla Suresh Peters, Anuradha Sriram
|5:15
|---
|}

== References ==
 

 

 
 
 
 


 