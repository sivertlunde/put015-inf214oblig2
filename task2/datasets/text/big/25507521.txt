Made for Each Other (2009 film)
{{Infobox film
| name           = Made for Each Other
| image          = 
| alt            =  
| caption        = 
| director       = Daryl Goldberg
| producer       = Sam Hamadeh
| writer         = Eric Lord
| starring       = Danny Masterson Patrick Warburton Christopher Masterson Bijou Phillips Samm Levine George Segal Lauren German Leslie Hendrix Kelsey Fowler Kyle Howard Andrew van den Houten
| music          = Ryan Shore
| cinematography = 
| editing        = 
| studio         = 
| distributor    = IFC Films
| released       =   
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Made for Each Other is a 2009 romantic comedy, which was released straight to DVD. This is the first film in which both of the Masterson brothers have featured.

==Plot==
Dan (Christopher Masterson) has a problem; hes been married to the beautiful Marcie (Bijou Phillips) for three months, but they still havent consummated their union. When Dans sex-crazed boss Catherine (Lauren German) comes on to him during a marathon work session, he crumbles under temptation. Immediately regretting his actions but unable to simply admit his indiscretion, Dan schemes with his best pal Mike (Samm Levine) to get Marcie to wander astray, too. If Dan isnt the only one who cheated, he and Mike surmise, then Marcie cant be mad at him for doing so. But hatching the plan is one thing, and finding a guy whos willing to go along with his plan is an altogether different challenge. Later, as the plan finally starts to come together, Dan discovers the truth about why Marcie has yet to sleep with him, and comes to realize that they may actually be the perfect couple after all.

==External links==
* 
* http://www.reelz.com/movie/275806/made-for-each-other/
* http://www.moviefone.com/movie/made-for-each-other/50026/main

 
 
 
 
 

 