Crazy Old Woman
{{Infobox Film name        = Crazy Old Woman image       = COW_festival_poster.jpg director    = Zhou “Joe” Fang producer    = Kathrin Krückeberg writer      = H. Scott Hughes starring    = Dan Richardson Nicki Burke Jacqlyn Atkins music       = distributor = released   = 2007 (Canada)  2008 (United States) runtime    = 10 min. language   = English budget     = country    = Canada 
}} short form paranormal Thriller (genre)|thriller, written by H. Scott Hughes and directed by Joe Fang.   It plays in the classic vein of shows such as The Twilight Zone and Alfred Hitchcock Presents, and films such as The Sixth Sense.

==Plot== attorney annoyed by the presence of his wifes grandmother, Crystal Colburn, who has moved in—and seems determined to deliberately disrupt his life and marriage. Crystal begins driving Stewart insane as she speaks to dead people on an unconnected rotary phone. As Stewart is pushed over the edge from forces beyond, his paranoia could lead to murder. 

==Reception==
Crazy Old Woman won the Audience Favorite Drama Award at the Route 66 Film Festival in 2008.   It had premiered the year before at the Vancouver International Film Centre.  

==Cast==
Dan Richardson as Stewart Hanson  
Nicki Burke as Terri Hanson  
Jacqlyn Atkins as Crystal Colburn  
Sean Gilchrist as Mover #1  
Marlon Hopeton as Mover #2

==Continuity with Food for the Gods==
 
This film takes place in the same fictional universe as H. Scott Hughess science fiction film Food for the Gods, set fifty years in the future.  In that film, Food for the Gods#Main cast|Dr. Denise Hanson is presumed to be granddaughter of Stewart and Terri Hanson, the characters featured in Crazy Old Woman.  Denise Hanson, in Food for the Gods, also meets a fate somewhat similar to her phone dialing ancestor, Crystal.  Writer, H. Scott Hughes has referred to this as the Hanson Family Curse. The plots are, otherwise, unrelated.  

== References ==
 

 
 
 
 
 


 
 
 