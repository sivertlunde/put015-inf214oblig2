Among Giants
{{Infobox film
| name           = Among Giants
| image          = Among giants.jpg
| alt            = 
| caption        = Original film poster
| director       = Sam Miller
| producer       = Stephen Garrett
| writer         = Simon Beaufoy James Thornton
| music          = Tim Atack
| cinematography = Witold Stok Paul Green Elen Pierce Lewis
| studio         = Capitol Films BBC Films Arts Council of England
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 97 minutes
| country        = United Kingdom
| language       = English
| budget         = ₤2,479,000 Alexander Walker, Icons in the Fire: The Rise and Fall of Practically Everyone in the British Film Industry 1984–2000, Orion Books, 2005 p. 303 
| gross          = 
}} James Thornton. The plot came about after Beaufoy was refused permission to make a documentary on electricity pylon painters in Pembrokeshire, and converted the idea into fiction.    The script predates that of The Full Monty, but only found a producer in the wake of the earlier films success. 

The practicalities of shooting atop electricity pylons, not to mention insurance difficulties, meant that a safer mock-up pylon was made for the actors. This proved unconvincing: Postlethwaite remarked that "I dont believe we are up there, and if you dont sell that shot you dont sell the rest of the pylons," and so some material was re-shot on real pylons after training from Electricity Board climbing experts. 
 Bedford van converted for use as a camper van—dubbed "the shagging wagon" in the film—was stolen and burnt out in Sheffield. 

== Plot ==
Ray (played by Postlethwaite) is a middle-aged Sheffield father of two, down on his luck. Separated from his wife, his life revolves around his close friendship with his much younger flatmate Steve (Thornton) and a passion for climbing.

One summer, the pair and their loose gang of workers gain illicit, cash-in-hand employment painting the electricity pylons of the Yorkshire Moors. Their deadline is tight and their terms of employment precarious, but both are grateful for money and the opportunity to climb daily.

The pattern of life is interrupted by the arrival of footloose Australian backpacker Gerry (Rachel Griffiths). Attractive, and a talented climber, she and Ray fall in love and become a couple despite Steves apparent interest.

Ray harbours some reluctance at Gerrys wild ways, but manages to overcome these to propose marriage. Gerry too is doubtful that her wandering days are over, and wonders if she can live the staid existence on offer, despite her love for Ray. Things come to a head when she and Steve, both clearly affected by the backpacking bug, are caught drinking vodka atop a tall pylon. 

Abseiling recklessly to face an angry Ray, she and Steve are fired on the spot.

In a confrontation, Gerry tells Ray that she cannot commit to the relationship, nor be tied down. Meanwhile, Rays friendship with an increasingly jealous Steve is also in trouble, and the younger man moves out of the flat, planning his own travels to India. He and Gerry have a brief sexual encounter, but back out when they recognise the wrongs of their actions.

Gerry waits on Rays doorstep, hoping for some form of reconciliation, but is rejected. Upset, and undertaking a solo rock climb, she falls and is hospitalised with serious injuries. As Ray is being told this by an emotional Steve, the pylons are electrified as Rays gang are still at work, and the crew are lucky to escape without electrocution. 

This heralds the end of the summers work, and their elusive paymaster Derek, the electricity company official, arrives on the scene. He is apologetic at the mistake, but cannot say when the workers will receive their pay.

The film ends on an uncertain note, with Steve departing for India, a recovered Gerry deciding to return to Australia, and Ray left standing on the Moors contemplating his scant options.

== Cast ==
*Pete Postlethwaite as Ray
*Rachel Griffiths as Gerry James Thornton as Steve
*Lennie James as Shovel
*Andy Serkis as Bob
*Rob Jarvis as Weasel
*Alan Williams as Frank

== Reception ==
Film critic Roger Ebert praised the film with a positive review and three stars, particularly pleased by the performances of Postlethwaite and Griffiths—"when you look at them, you always know how their characters are feeling"—and writing that the film was "thick with atmosphere." 

David Stratton of Variety (magazine)|Variety was less complimentary, writing that the film was not "in the same league" as The Full Monty, was predictable, and despite the efforts of the cast, suffered from "less attractive characters, and a rather dull plot."  It has a 58% rating on Rottentomatoes.com. 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 