Stewardess School
{{Infobox film
| name           = Stewardess School
| image          = Stewardess-school.jpg
| caption        = Stewardess School VHS cover
| director       = Ken Blancato
| producer       = Phil Feldman
| writer         = Ken Blancato
| starring       = Brett Cullen Don Most Judy Landers Mary Cadorette Alan Rosenberg Wendie Jo Sperber Tim Hoskins
| music          = Robert Folk
| cinematography = Fred J. Koenekamp Lou Lombardo Kenneth C. Paonessa	
| distributor    = Columbia Pictures
| released       = August 1986
| runtime        = 89  minutes
| country        = United States
| language       = English
| budget         = 8,000,000 (estimated)
| gross          =  $136,158 
}}
 voice veteran Rob Paulsens very few onscreen roles, and up until the early 2000s, one of the most played films on the American cable channel Comedy Central.

==Plot==
When frat boys Philo (Cullen) and George (Most) fail at becoming pilots, the duo decides to join Weidermeyer Academy, one of the top stewardess schools in the country. During training, the guys encounter a group of misfits who run the gamut of quirkiness. The group end up bonding and theyre all sent to the seedy Stromboli Airlines for their final test.

During the cross country flight, which is filled with blind passengers, the group encounters a Mad Bomber (Rosenberg) who intends to blow up the plane. After the pilots get gassed by the bomber, its up to the students to save the day.

==Cast==
*Brett Cullen as Philo Henderson
*Don Most as George Bunkle
*Mary Cadorette as Kelly Johnson
*Judy Landers as Sugar Dubois
*Sandahl Bergman as Wanda Polanski
*Wendie Jo Sperber as Jolean Winters
*Julia Montgomery as Pimmie Polk
*Dennis Burkley as Snake
*Corinne Bohrer as Cindy Adams
*Rob Paulsen as Larry Falkwell
*Vito Scotti as Carl Stromboli
*William Bogert as Roger Weidermeyer
*Vicki Frederick as Miss Grummet
*Alan Rosenberg as Mad Bomber
*Sherman Hemsley as Mr. Buttersworth
*Tim Hoskins as Boy
*Richard Erdman as Attorney
*Lenore Woodward as Passenger

== References ==
 

==External links==
* 
* 
* 

 
 
 
 
 


 