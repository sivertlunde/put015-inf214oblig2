Two Men in Town (2014 film)
{{Infobox film
| name           = Two Men in Town
| image          = Two Men in Town.PNG
| alt            = 
| caption        = 
| director       = Rachid Bouchareb
| producer       = Allen Bain   Jean Bréhat   Jérôme Seydoux   Rachid Bouchareb
| writer         = Olivier Lorelle   Rachid Bouchareb   Yasmina Khadra
| starring       = Forest Whitaker   Harvey Keitel   Ellen Burstyn   Luis Guzmán   Brenda Blethyn
| music          = 
| cinematography = Yves Cape
| editing        = 
| studio         = Artists & Co   Tessalit Productions
| distributor    = 
| released       =  
| runtime        = 
| country        = France United States
| language       = English
| budget         = 
| gross          = 
}} Deux hommes dans la ville. 

The film had its premiere in the competition section of the 64th Berlin International Film Festival.       It was released in the United States by Cohen Media Group to theaters and iTunes on March 6, 2015.

==Cast==
* Forest Whitaker as William Garnett
* Harvey Keitel as Bill Agati
* Ellen Burstyn as Garnetts mother
* Luis Guzmán as Terence
* Brenda Blethyn as Emily Smith
* Tim Guinee as Rod
* Sarah Minnich as Reporter
* Matthew Page as Prison Guard
* Lionel Archuleta as Cafe Patron
* Chris Ranney as Parolee
* Cesar Miramontes as Attorney
* Stan Carp as Richard Wayne
* Patrick Juarez as Parole Security Officer
* Michael Stone as Sheriff Deputy
* Kristin Hansen as Maria Perez
* Earl Ortiz as Dancer and Bar Patron
* Joe Cabezuela as Lieutenant Joe Corrales

==Production==

===Filming===
The shooting of the film began in April 2013 in New Mexico. 

==References==
 

==External links==
 
*  

 
 
 
 
 
 
 
 
 
 
 
 

 
 