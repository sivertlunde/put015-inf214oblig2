Salt of this Sea
Salt Palestinian film directed by Annemarie Jacir and was an Official Selection of the Cannes International Film Festival in 2008. It is Palestines submission to the 81st Academy Awards for the Academy Award for Best Foreign Language Film.  The film stars Palestinian-American poet Suheir Hammad as Soraya, an American-born Palestinian woman,  who heads to Israel and Palestine on a quest to reclaim her familys home and money that were taken during the 1948 Arab-Israeli War. Palestinian actor, Saleh Bakri also stars in the film.

==See also==
*List of submissions to the 81st Academy Awards for Best Foreign Language Film
*List of Palestinian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==Awards==
* FIPRESCI PRIZE - INTERNATIONAL CRITICS AWARD
International Federation of Film Critics, 2008
* FIRST PRIZE - BEST FILM
 , Italy 2009
* SPECIAL JURY PRIZE
Osians Asian & Arab Film Festival, 2008
* BEST FIRST FILM
Traverse City Film Festival, 2009
* SPECIAL JURY PRIZE
Oran International Festival of Arab Cinema, 2009
* RANDA CHAHAL PRIZE
Journées cinématographiques de Carthage, 2008
* BEST SCREENPLAY
Dubai International Film Festival, 2008
* BEST OF FEST SELECT
Minneapolis St. Paul International Film Festival 2009
* AUDIENCE CHOICE AWARD
Houston Palestine Film Festival, 2009
* AUDIENCE CHOICE AWARD - BEST FEATURE
Chicago Palestine Film Festival, 2009
* HONORABLE MENTION
Cairo Refugee Film Festival, 2009
* AUDIENCE CHOICE RUNNER UP
Toronto Palestine Film Festival, 2008
* PALESTINES OFFICIAL OSCAR ENTRY
FOR BEST FOREIGN-LANGUAGE FILM, 2008
* SOPADIN FINALIST
Grand Prix Best Screenplay, 2007
* CINEMA IN MOTION AWARDS
San Sebastian Film Festival, 2007

==External links==
*  
*   The National, Sep. 9, 2008
*  , Nov. 16, 2008
*  Cannes 2008
*Trigon:  
*  April 22, 2009, CNN
* 

 
 
 