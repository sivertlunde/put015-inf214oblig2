We're All Christs
{{Infobox film
| name           = Were All Christs
| image          =Were All Christs.jpg
| image size     =
| caption        =
| director       = Marek Koterski	 	
| producer       = Wlodzimierz Otulak
| writer         = Marek Koterski
| narrator       =
| starring       = 
| music          =
| cinematography = 
| editing        = Ewa Smal
| distributor    =
| released       = 
| runtime        = 107 min 
| country        = Poland Polish
| budget         =
| preceded by    =
| followed by    =
}}
 Polish 2006 2006 film directed by Marek Koterski. It won Best Film at the 2007 Polish Film Awards.

The films depicts the story of a Polish intellectual Adaś Miauczyński. It focuses on his alcoholism problems and his relations with son Sylwek.

== Cast ==
* Marek Kondrat, as Adaś Miauczyński (age 55)
* Andrzej Chyra, as Adaś Miauczyński (age 33)
* Michał Koterski, as Sylwek, son of Adaś
* Janina Traczykówna, mother of Adaś
* Małgorzata Bogdańska, wife of Adaś
* Tomasz Sapryk, as "angel guard"
* Marcin Dorociński, as evil angel
* Andrzej Grabowski, as friend of Adaś
* Marian Dziędziel, as friend of Adaś
* Jan Frycz, as friend of Adaś Artur Żmijewski, as friend of Adaś
* Paweł Królikowski, as friend of Adaś Andrzej Zieliński, as friend of Adaś
* Ewa Ziętek, as teacher of Sylwek
* Patrycja Soliman, as journalist

== External links ==
*  

 
 
 

 
 