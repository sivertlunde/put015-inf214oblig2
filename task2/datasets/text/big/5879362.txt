Pori (film)
 
{{Infobox film
| name = Pori

| image = 
| director = Subramaniam Siva
| writer = Subramaniam Siva Pooja Seeman Seeman Karunas 
Sampath Raj
| producer = Karthick
| music = Dhina
| distributor = Nivi-Pavi Creations
| released =  
| runtime =
| language = Tamil
| country = India
| budget =
}}
Pori ( ) is a 2007 Tamil film directed by Subramaniam Siva. Subramaniam Siva, who rendered his debut Thiruda Thirudi, a 2004 runaway blockbuster. This film has Jiiva in the lead and Pooja as his lady love. It has been dubbed in Hindi as Naya Zalzala.The film met with negative reviews and turned out to be a box office bomb. {{Cite web
| title = Pori Tamil Movie (2007) - Venpura
| accessdate = 2013-12-09
| url = http://www.venpura.com/movie/Pori-2007
}} 

==Plot==
Hari (Jiiva) is an unemployed youth who runs a pavement book shop in Tripiclane area. His father (Nagesh) an upright retired school teacher buys him a shop in a building with his PF money. He names it “Periyar Bookshop” and runs it with friend Karunas. Meanwhile he constantly bumps into Pooja (Pooja), a television reporter and a love-hate relationship happens!

However, one day a Malaysian based businessman (Seeman (director)|Seeman) comes and evicts him from the shop claiming that the place belongs to him. Hari discovers that his father was cheated by a land mafia gang which specializes in forged documents and is hand-in-glove with revenue officials. Hari decides to take the gauntlet and unveil the evil forces behind those property grabbers.

Soon he discovers that Nama Shivayam (Sampath Raj), Vinayagam real estates owner, is the brain behind the entire operations. How Hari becomes a one-man army and brings Nama Shivayam to justice forms the rest of the story.

==Reception==

Pori got negative reviews from critics. A critic from Nowrunning.com rated the film 1/5, stating that "If you have all the time in the world to do nothing else, go watch Pori. A seriously depressing movie with absolute lack of comedy, story line, good music and everything which goes into making a good movie." 

==Soundtrack==
{{tracklist
| headline     = Track-list
| extra_column = Singer(s)
| title1       = Eppadiyellam 
| extra1       = Shankar Mahadevan
| title2       = Jigina Nadanthu 
| extra2       = Grace Karunas Jasey Gift 
| title3       = Perunthil Nee
| extra3       = Madhu Balakrishnan Madhushree Dhina
| title4       = Pookkalellam 
| extra4       = Hariharan
| title5       = Vedhala Dhevathaiye  
| extra5       = Shankar Mahadevan Malathi 
| title6       = Yetta Uyarathil 
| extra6       = Jeeva Dhina Subramaniam Siva
}}

==References==
 
 

 
 
 
 
 


 