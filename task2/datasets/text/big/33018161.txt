To Have and to Hold (1922 film)
{{infobox film
| name           = To Have and to Hold
| image          =To Have and to Hold (1922) 1.jpg
| imagesize      =150px
| caption        =Film still with Betty Compson and Bert Lytell
| director       = George Fitzmaurice
| producer       = Adolph Zukor Jesse Lasky
| screenplay     = Ouida Bergere 
| based on       =  
| starring       = Bert Lytell Betty Compson
| cinematography = Arthur C. Miller
| editing        =
| distributor    = Famous Players-Lasky Corporation Paramount Pictures
| released       =  
| runtime        =
| country        = United States
| language       = Silent English intertitles
}}
 silent drama novel of the same name, the film was directed by George Fitzmaurice and starred Bert Lytell and Betty Compson.

To Have and to Hold is now considered lost film|lost.   

==Cast==
* Betty Compson - Lady Jocelyn Leigh
* Bert Lytell - Captain Ralph Percy
* Theodore Kosloff - Lord Carnal
* William J. Ferguson - Jeremy Sparrow
* Raymond Hatton - King James I
* Claire Du Brey - Patience Worth Walter Long - Red Gill
* Anne Cornwall - Lady Jane Carr
* Fred Huntley - Paradise
* Arthur Rankin - Lord Cecil
* Lucien Littlefield - Duke of Buckingham

==Other== in 1916. The 1916 version starred Mae Murray and Wallace Reid, and is also considered lost.

==See also==
*List of lost films

==References==
 

==External links==
 
*  
*  
*   at Virtual History

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 