Liz: The Elizabeth Taylor Story
{{Infobox film
 | name = Liz: The Elizabeth Taylor Story
 | image =
 | caption = Kevin Connor
 | producer = Lester Persky Hugh Benson
 | writer = Novel: &nbsp;&nbsp;  Kevin McCarthy Corey Parker Ray Wise Casey Ahern
 | music = Ken Thorne
 | cinematography = Douglas Milsome Barry Peters Corky Ehlers
 | distributor = NBC
 | released = May 21, 1995
 | runtime =
 | country = United States
 | language = English
 | budget = $14 million
 }} Kevin Connor. The film stars Sherilyn Fenn (as Elizabeth Taylor), Katherine Helmond, Nigel Havers, Angus Macfadyen, William McNamara and Ray Wise.

==Cast==
{|Class="wikitable"
|-
! Actor
! Role
|-
| Sherilyn Fenn || Elizabeth Taylor
|- Michael Wilding
|-
| Katherine Helmond || Hedda Hopper
|-
| Angus Macfadyen || Richard Burton
|-
| William McNamara || Montgomery Clift
|- Corey Parker Eddie Fisher
|-
| Ray Wise || Mike Todd
|-
| Daniel McVicar || Rock Hudson
|-
| Charles Frank || John Warner
|-
| Michael McGrady || Larry Fortensky
|-
|}

==External links==
* 

==See also==
Liz & Dick — a 2012 Lifetime biopic.

 
 

 
 
 
 
 


 