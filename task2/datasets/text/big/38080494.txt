Tadeo Jones
 
{{Infobox film
| name           = Tadeo Jones
| image          = Tadeo Jones poster.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Enrique Gato
| producer       = 
| writer         = 
| screenplay     = José Ángel Esteban Carlos López Manolo Matji Enrique Gato 
| story          = 
| based on       =  
| starring       = 
| narrator       = 
| music          = Zacarías Martínez de la Riva 
| cinematography = 
| editing        = Enrique Gato 
| studio         = La Fiesta Producciones 
| distributor    = Lolita Peliculitas Visual Arts
| released       =  
| runtime        = 8 minutes   
| country        = 
| language       = 
| budget         = €43,000
| gross          = 
}}
Tadeo Jones is a 2004 Spanish short animation film directed by Enrique Gato. It tells the story of Tadeo Jones, an adventurer who enters a pyramid where he finds a family of mummies. The project of this film was born when Gato wanted to make a short film containing more action and humour than his previous projects, so he decided to make a parody of the adventure genre, whose representative figure is Indiana Jones. The story contains many elements from the Raiders of the Lost Ark (Indiana Jones) series of movies. The synopsis, in the directors words, is "adventurer Tadeo Jones is a little foolish but stubborn to explore an ancient pyramid and will run into a family of mummies."  See   and  .}}   

The film won sixty-five awards including a British Academy of Film and Television Arts (BAFTA). Two years later, there was a sequel to the film called Tadeo Jones and the Basement of Doom.    A feature-length animated film, Tad, The Lost Explorer, featuring Tadeo Jones, was released in 2012. 

==Plot==
Tadeo Jones is in a maze inside a pyramid looking for the exit and gets access to a room with a statue, which needs one euro to activate the exit mechanism. Instead he gets through using a knife. In the next room Tadeo discovers a crank to turn, opening a door leading to treasure, but if he stops moving it abruptly closes. What he does not know is that turning the crank opens the coffins of a family of mummies.   When going to take the treasure, Tadeo hears the sound of the door and discovers a mummy father. Tadeo flees but meets a river that prevents him from passing. He continues to run away and gets on a train where six children laugh at him. Eventually, Tadeo discovers that it is a carnival ride called "Terror en la cripta" (literally "Terror in the Crypt").  

==Characters== Mortadelo (Mort & Phil).  The family of mummies consists of a father mummy who is the head of it, who is upset when Tadeo breaks in. The father mummies forehead, jaw and eyes resemble Victor Frankenstein. The mother (who is the caretaker of the family) was influenced by Lara Croft and the baby (who becomes the main enemy) was influenced from Who Framed Roger Rabbit and the eyes came from Dash from The Incredibles.    Appearing at the end of the film, there are six children wearing explorer clothing who participated in the attraction ("Terror en la cripta"). The creators did not give much importance to them as they appeared on the screen for a few seconds, so one was copied six times and only the clothes changed. 

==Production==
The character Tadeo Jones was created in 2001  as a parody of Indiana Jones. However, the project did not start until two years later when Gato begun to write the script.    In September 2003, negotiations begun with La Fiesta production  and four other scripts were written. 

==Reception== Goya Award.  Tadeo Jones was budgeted to cost 43,000 euros,  but no final figure has been released.   

==Notes==
 

==References==
 

==Bibliography==
* 
* 

==External links==
*  
*  
 
 
 