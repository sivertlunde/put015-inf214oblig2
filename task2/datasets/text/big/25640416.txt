Khelein Hum Jee Jaan Sey
 
 

{{Infobox film
| name           = Khelein Hum Jee Jaan Sey
| image          = Khjjs1.jpg
| caption        = Theatrical release poster
| director       = Ashutosh Gowarikar Ajay Bijli Sanjeev Bijli  Sunita A. Gowariker
| screenplay     = Ashutosh Gowariker Raoul V Randolf
| based on       =  
| starring       = Abhishek Bachchan Deepika Padukone Vishakha Singh Sikandar Kher Ram Sethi
| music          = Sohail Sen
| cinematography = Kiran Deohans Seetha Sandhiri
| editing        = Dilip Deo
| studio         = 
| distributor    = PVR Pictures
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =    
| gross          =   
}}
 Chittagong Uprising of 1930. The film was shot mostly in Goa along with portions in Mumbai. {{cite web|title=Ashutosh Gowarikers Khelein Hum Jee Jaan Sey to be released on December 3.
|url=http://www.dnaindia.com/entertainment/report_ashutosh-gowariker-s-khelein-hum-jee-jaan-sey-to-be-released-on-December 3_1374400|publisher=Daily News & Analysis|date=22 April 2010|accessdate=11 October 2011}}  The first promo of the film was unveiled on 12 October 2010, on the films official Facebook page. 
The film was released on 3 December 2010. The film depicts the journey of the Chittagong Uprising, from the rise to its aftermath.

==Plot==
Khelein Hum Jee Jaan Sey is about a teacher Surya Sen a.k.a. Masterda (played by Abhishek Bachchan), who is a well-known revolutionary leader of the Indian Republican Army Chittagong, who had an instinctual flair for Swaraj and Independence. The film opens up with 16 teenagers playing football in an open field, when the army convoy steps up and orders them to evacuate the field for setting up a base there. When the children oppose their unauthorized evacuation, the soldiers threaten them and then leave, grinning and planning to meet Masterda Surya Sen.
 Ambika Chakraborty, Nirmal Sen and Ananta Singh (played by Maninder Singh)]. Nirmal Sen (played by Sikander Kher) after being released from jail meets Masterda and asks him for next action. Masterda assures him of next action soon and Nirmal goes on to meet Pritilata Waddedar (played by Vishakha Singh), when she expresses her eagerness to join the Indian Republican Army along with his friend Kalpana Datta (played by Deepika Padukone). Together they meet Masterda who asks them to prove their skills by assigning them the task of collecting information about the Cantonment. Dressed as sweeping girls they both draw successful sketches of the Cantonment.

Meanwhile, those teenagers express their will to join Surya Sen to drive the British out of the country. Surya Sen enlists them and trains them in martial arts, shooting and bomb-making. Masterda devises the plan to rock the British Empire by planning a raid on police lines Armoury, Cantonment, Telegraph office and Railway lines. Apart from Surya Sen, the group included Ganesh Ghosh, Lokenath Baul, Nirmal Sen, Ambika Chakraborty, Naresh Roy, Sasanka Datta, Ardhendu Dastidar, Harigopal Baul, Tarakeswar Dastidar, Ananta Singh, Jiban Ghoshal, Anand Gupta, Pritilata Waddedar, Kalpana Datta and the group of teenagers. The plan was put into action at 10 oclock on 18 April 1930. As per plan, the armoury of the police was captured by a group of revolutionaries led by Ganesh Ghosh and another group of ten, led by Lokenath Baul took over the Auxiliary Force armoury. Unfortunately the guns were found but bullets were not there. So they had to burn the arms there only. The revolutionaries also succeeded in dislocating telephone and telegraph communications and disrupting the movement of the trains. After the successful raids, all the revolutionary groups gathered outside the police armoury where Surya Sen took a military salute, hoisted the National Flag and proclaimed a Provisional Revolutionary Government. The revolutionaries left Chittagong town before dawn and marched towards the Chittagong hill ranges, looking for a safe place, but the police by then with the support of British Army had them surrounded, but the British Armys first wave had been demolished by Masterdas Teenagers, this enraged the British and they bought machine guns and fired casualties in Masterdas side. Harigopal Baul (Tegra) was first to be shot dead along with twelve others and the group had to flee leaving Ambika Chakraborty wounded, the Britishers found the dead bodies and burnt them with Petroleum. {{multiple image
   | width     = 200
   | footer    = image1    = HangplatformLeft.jpg
   | alt1      =
   | caption1  =  The gallows in Chittagong Central Jail, Bangladesh, where Surya Sen was hanged. The Government of Bangladesh designated it a monument.
   | image2    = Hangplatformstone.jpg
   | alt2      =
   | caption2  =
  }}

Meanwhile, Masterda had split the group into four and asked them to separate. While living with her sister in Patiya village the police under Major Cameron surrounded Masterda and tried to capture him but he was shot dead by Nirmal sen. While Nirmal got wounded, he asked Masterda to get out of there and henceforth sacrificing his own life fighting. Later eight young rebels led by Pritilata Waddedar attacked the European Club. After killing the officials the rebels fled and Pritilata committed suicide by swallowing Cyanide. Masterda escaped and retired to another house with Kalpana and other young rebels. Meanwhile the Police SP of Chittagong and other parts press hard crackdown on escaping rebel members and search for Masterda like anything. While, Masterda escaped, other members got arrested or killed in encounters with police while some members shot themselves like heroes to evade brutal torture by police. After few months, Haripada a young rebel decides to take revenge as he assassinates the SP who had an upper hand while suppressing the rebels, during an army football match. Finally, one night Masterda was encircled and was captured by police troopers he was trialled before the court and was sentenced to death by hanging, Surya Sen happily accepts the verdict. Before the death sentence, Surya Sen was brutally tortured by British executioners who break all his teeth, limbs and joints with hammer. He was dragged to the rope unconscious and was hanged, thus ending the glorious life of a revolutionary who became one of the living legends, who laid their lives fighting for Indias independence.

==Cast==
* Abhishek Bachchan as Surya Sen/Surjyoda/Masterda
* Deepika Padukone as Kalpana Datta
* Sikander Kher as Nirmal Sen
* Vishakha Singh as Pritilata Waddedar
* Maninder Singh as Ananta Singh Ambika Chakraborty
* Samrat Mukerji as Ganesh Ghosh Loknath Bal
* Vikramjeet Virk as AssanUllah
* Monty Munford as Major J.R. Johnson
* Amin Gazi
* Ram Sethi as Rehman Chacha
* Nitin Prabhat as tegra
* Shubham Patekar as naresh
* Parth Mandal as fakir sen
* Palash Muchhal as jhunkoo
* Mohsin Khan as himangshu
* Aamir Khan as deba gupto
* Rohan Painter as anando gupto
* Abbas Khandwawala as bidhu
* Narendra Bindra as rajat sen

==Reception==

===Critical response===
The film opened to generally positive reviews. Critics reactions to the film have been mostly generous, although several said it was much too long.   

Filmfare gave the movie a three star rating stating Khelein Hum Jee Jaan Sey is a bumpy ride as far as the narrative goes. It jolts and jumps and sometimes doesnt move at all. But that is all in the first half, which just seems longer than it is. There is some amazing art direction in the film not to mention the costumes; Gowariker shows his forte yet again in making a picture perfect period film. This is his thing. He does it well. Movies like KHJJS reiterate our faith that there are still filmmakers out there who will not compromise on their vision and conviction. Trade fads be damned. And as long as there are stories like this waiting to be told, we hope people like Ashutosh Gowariker will tell them. 

The Times of India, which gave a three and half rating, mentioned that the film unfolds like a relentless thriller with loads of action involving the band of revolutionaries as they go about their bloody business. Yet no one hollers the national anthem at you or grows hysterical with patriotic pulp. Instead, the director gently salutes the spirit of nationalism in a seminal scene where the bunch of teenage revolutionaries discover the hypnotic allure of a hymn like vande mataram while resting under the shade of the trees in their village.  Nikhat Kazmi (The Times of India) felt that the filmmaker was able to "combine high-octane drama with a high degree of restraint". She writes, "The film unfolds like a relentless thriller with loads of action involving the band of revolutionaries as they go about their bloody business." 

NDTV gave 2.5 out of 5 rating and noted, Ashutosh Gowariker is Bollywoods most earnest historian and Khelein Hum Jee Jaan Sey is a noble attempt at restoring glory to long-forgotten heroes. 
Critic Taran Adarsh of Bollywood Hungama rated four out of five who opined recreating the bygone era is indeed demanding, laborious and strenuous. Its a challenge to present the era convincingly. Besides extensive detailing to lend authenticity, the director carries the responsibility of making the characters come alive to the present-day generation. Gowariker has successfully done that in the past and does it successfully yet again in Khelein Hum Jee Jaan Sey. The expectations from Khelein Hum Jee Jaan Sey are minimal, but you cant overlook the fact that its a genuinely honest effort that needs to be encouraged and appreciated. 

Sify movies, which also gave a two and a half rating, said that would be a film one would wholeheartedly and unabashedly recommend. 

Rajeev Masand of CNN IBN gave it a two star rating explaining Its a noble effort that gives you a glimpse of forgotten history. 

Reuters trashed that the Gowarikers biggest strength is that he chooses a story worth telling. For that reason alone, and to get a glimpse into a much-ignored part of our history, this film is worth a watch. 

In the newspaper Daily News and Analysis, Aniruddha Guha says that it is the story of the film that made it watchable. "The strong points of the film are the novel storyline – that of kids fighting for independence – and the performances of most actors, especially the children. Also, Gowariker manages to recreate the era well (with art director Nitin Chandrakant Desai), and draws you in the plot slowly and steadily," wrote Mr. Guha. 

MovieTalkies rated 4.5 out of 5 and noted "Gowariker this time manages to keep a running subtext that underlines the drama with a national emotional resonance right till the heart-wrenching denouement that will bring a lump to the throat of even the most hardened hearts, while the story actually unfolds as a thriller with the planning and execution of five attacks on the British in a single night and its aftermath, a first for Indian cinema. At a time when our cinema needs true calibre to distinguish itself as an art form rather than a medium of lowest common denominator entertainment, Khelein Hum Jee Jaan Sey stands as a lighthouse, shining high". 

===Box office===
In spite of highly positive reviews, KHJJS had a very poor opening and didnt pick up due to word of mouth even. Eventually, the film was a Box Office disaster and proved to be a very huge loss for the PVR Production House. The GM of the PVR group mentioned later that they couldnt even get back 10% of the total films budget. Yet, he mentioned that his production house was happy that they lost only on giving an excellent film, not a poor one. Similarly, Amitabh Bachchan and Director Anurag Kashyap also expressed their views later and said that films like KHJJS are assets to Bollywood, but the failure of such films is a very bad sign and it doesnt motivate young directors to come up with quality products. The Box Office failure of KHJJS was indeed a very big shock and disappointment among the critics as well the industry. Yet, at the same time the film was expected to do poorly due to the lead actor, Abhishek Bachchans past box office track record.

==Soundtrack==
 
The films songs and film score were composed by Sohail Sen, who had worked on Ashutoshs previous film Whats Your Raashee?. The lyrics are penned by Javed Akhtar.  The music was launched on 27 October 2010.  The complete track listing of the soundtrack was also released on the same day of the music launch, on the films official Facebook page.  The official soundtrack consists of twelve tracks; including five songs and seven background score cues, some of which have been arranged and programmed by Simaab Sen. The title track has been sung by the pupils from Suresh Wadkars music academy. Malayalam playback singer Ranjini Jose made her Bollywood debut through the song "Naiyn Tere". {{cite web
|url=http://sify.com/movies/ranjini-jose-to-rock-bollywood-news-malayalam-klmlrzdefcc.html
|title=Ranjini Jose to rock Bollywood
|publisher=Sify
|accessdate=12 November 2010
}} 

==Awards and nominations==
 2011 Zee Cine Awards

Nominated  Best Story - Ashutosh Gowariker

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 