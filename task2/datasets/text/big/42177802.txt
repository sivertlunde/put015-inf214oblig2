Stripped (film)
{{Infobox film
| name           = Stripped
| image          = Stripped_poster_by_Bill_Watterson.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Bill Watterson created the poster for Stripped.
| director       = Dave Kellett Frederick Schroeder 
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Sequential Films 
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = 
| language       = English
| budget         = 
| gross          = 
}}
 failing newspaper industry to the webcomic|web. {{cite web
| url          = http://mashable.com/2014/02/27/bill-waterson-calvin-hobbes/
| title        = Calvin and Hobbes Creator Bill Watterson Publishes First Cartoon in 19 Years
| last1        = Green
| first1       = Dennis
| date         = 2014-02-27
| website      = Mashable
| archiveurl   = http://www.webcitation.org/6NvYQ8ciK
| archivedate  = 2014-03-08
| accessdate   = 2014-03-08
}}   Work on Stripped began in 2010.  The films original concept was to make a documentary about cartoonists in their studios. 

Stripped features interviews with over 70 comic creators, {{cite news
| last1                 = Gustines
| first1                = George Gene
| date                  = 2014-02-26
| title                 = Some New Art From ‘Calvin and Hobbes’ Creator
| url                   = http://artsbeat.blogs.nytimes.com/2014/02/26/some-new-art-from-calvin-hobbes-creator/
| newspaper             = The New York Times
| issn                  = 0362-4331
| oclc                  = 1645522
| archiveurl            = http://www.webcitation.org/6O0Nha2Hr
| archivedate           = 2014-03-11
| accessdate            = 2014-03-11
}}  who discuss their trade and its prospects in the 21st century. {{cite journal
| last1                 = Grow
| first1                = Kory
| date                  = 2014-02-28
| title                 = Calvin and Hobbes Creator Draws First Comic in 19 Years
| url                   = http://www.rollingstone.com/culture/news/calvin-and-hobbes-creator-draws-first-cartoon-in-nearly-two-decades-20140228
| journal               = Rolling Stone
| issn                  = 0035-791X
| archiveurl            = http://www.webcitation.org/6NvWPOLKL
| archivedate           = 2014-03-08
| accessdate            = 2014-03-08 crowdfunded through Kickstarter, {{cite news
| last1                 = Runcie
| first1                = Charlotte
| date                  = 2014-02-27
| title                 = Calvin and Hobbes creator releases rare new artwork
| url                   = http://www.telegraph.co.uk/culture/books/booknews/10664948/Calvin-and-Hobbes-creator-releases-rare-new-artwork.html
| newspaper             = The Daily Telegraph
| location              = London, England
| publisher             = Telegraph Media Group
| issn                  = 0307-1235
| oclc                  = 49632006
| archiveurl            = http://www.webcitation.org/6NvXLpYWE
| archivedate           = 2014-03-08
| accessdate            = 2014-03-08
| quote                 = Bill Watterson, the artist behind the beloved Calvin and Hobbes cartoons, has released his first new artwork in 19 years as part of a new film about cartoons
}}  and was released on the iTunes Store on April 1, 2014. {{cite web
 | url          = https://itunes.apple.com/us/movie/stripped/id816065098?ls=1
 | title        = iTunes - Movies - Stripped
 | website      = iTunes Store
 | publisher    = Apple Inc.
 | archiveurl   = http://www.webcitation.org/6NvZHHBGS
 | archivedate  = 2014-03-08
 | accessdate   = 2014-03-08
}} 

==Interviewees==
  
 
* Jean Schulz, widow of Charles M. Schulz (Peanuts, 1950&ndash;2000) 
* Mort Walker (Beetle Bailey, 1950–) 
* Jeff Keane (The Family Circus, 1960–) {{cite news
| last1                 = Cohen
| first1                = Nicole
| date                  = 2014-02-27
| title                 = Calvin & Hobbes Creator Pens His First Public Comic In 18 Years
| url                   = http://www.npr.org/blogs/thetwo-way/2014/02/27/283437272/calvin-hobbes-creator-pens-his-first-public-comic-in-18-years
| archiveurl            = http://www.webcitation.org/6O0M0LpgP
| publisher             = NPR
| archivedate           = 2014-03-11
| accessdate            = 2014-03-11
}} 
* Cathy Guisewite (Cathy, 1976–2010)  Jim Davis (Garfield, 1978–) 
* Bill Watterson (Calvin and Hobbes, 1985–1995)  Greg Evans (Luann (comic strip)|Luann, 1985–) {{cite news
| date                  = 2014-03-10
| title                 = ‘Stripped’ shines spotlight on comic strips
| url                   = http://www.msnbc.com/the-cycle/watch/stripped-shines-spotlight-on-comic-strips-190390339821 The Cycle
| publisher             = MSNBC
| accessdate            = 2014-03-11
| quote                 = Filmmakers Dave Kellett and Frederick Schroeder talk about their documentary “Stripped,” which takes a look at the history of comic strips and the future of the funnies.
}} 
* Bill Amend (FoxTrot, 1988–)  
 
* Jerry Holkins (Penny Arcade, 1998–) 
* Mike Krahulik (Penny Arcade, 1998–) 
* Jeph Jacques (Questionable Content, 2003–)  Richard Thompson Cul de Sac, 2004–2012) 
* Karl Kerschl (The Abominable Charles Christopher, 2007–) 
* Kate Beaton (Hark! A Vagrant, 2008–) 
* Matthew Inman (The Oatmeal, 2009–) 
 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 