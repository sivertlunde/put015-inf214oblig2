Renegade Trail
{{Infobox film
| name           = Renegade Trail
| image          = Renegade Trail poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Lesley Selander
| producer       = Harry Sherman 
| screenplay     = John Rathmell Harrison Jacobs  William Boyd George "Gabby" Hayes Russell Hayden Charlotte Wynters Russell Hopton Roy Barcroft John Merton
| music          = Gerard Carbonara John Leipold
| cinematography = Russell Harlan
| editing        = Sherman A. Rose 	
| studio         = Harry Sherman Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 58 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film William Boyd, George "Gabby" Hayes, Russell Hayden, Charlotte Wynters, Russell Hopton, Roy Barcroft and John Merton. The film was released on August 18, 1939, by Paramount Pictures.  

==Plot==
 

== Cast ==		 William Boyd as Hopalong Cassidy
*George "Gabby" Hayes as Windy Halliday 
*Russell Hayden as Lucky Jenkins
*Charlotte Wynters as Mary Joyce
*Russell Hopton as Bob Smoky Joslin
*Roy Barcroft as Stiff-Hat Bailey
*John Merton as Henchman Tex Traynor
*Sonny Bupp as Joey Joyce Eddie Dean as Singing Cowhand Red
*The Kings Men as Singing Cowhands 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 