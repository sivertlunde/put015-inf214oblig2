Voyage in Time
{{Infobox film
| name           = Voyage in Time
| image          = Voyage in Time DVD.jpg
| image_size     = 
| caption        = 
| director       = Tonino Guerra Andrei Tarkovsky
| producer       = 
| writer         = Tonino Guerra Andrei Tarkovsky
| narrator       = 
| starring       = Tonino Guerra Andrei Tarkovsky
| music          = 
| cinematography = Luciano Tovoli
| editing        = 
| distributor    = Facets Video
| released       = 1983
| runtime        = 63 minutes
| country        = Italy Italian / Russian
| budget         = 
| preceded_by    = 
| followed_by    = 
}} feature documentary documentary that documents the travels in Italy of the director Andrei Tarkovsky with the script writer Tonino Guerra in preparation for the making of his film Nostalghia. In addition to the preparation of Nostalghia, their conversations cover a wide range of matters, filmmaking or not. Notably, Tarkovsky reveals his filmmaking philosophy and his admiration of films by, among others, Robert Bresson, Jean Vigo, Michelangelo Antonioni, Federico Fellini, Ingmar Bergman.

The film was screened in the Un Certain Regard section at the 1995 Cannes Film Festival.   

==Similar documentaries==
This is the only documentary about Andrei Tarkovsky which is also co-directed by Tarkovsky, although several dozen other documentaries about him have been produced. Most notable are One Day in the Life of Andrei Arsenevich by Chris Marker, Moscow Elegy by Alexander Sokurov, The Recall by Tarkovskys son Andrei Jr., and Regi Andrej Tarkovskij (Directed by Andrei Tarkovsky) by Michal Leszczylowski, the editor of Tarkovskys The Sacrifice. Tarkovsky has also been featured in numerous documentaries about the history of cinema or the craft and art of filmmaking. 

==References==
 

==External links==
*  
*   at Nostalghia.com

 

 
 
 
 
 
 
 
 
 
 
 

 