Race with the Devil
 
{{Infobox film
| name           = Race with the Devil
| image          = RaceWithTheDevil.jpg
| caption        = Promotional movie poster for the film
| director       = Jack Starrett
| producer       = Paul Maslansky Wes Bishop
| writer         = Wes Bishop Lee Frost
| starring       = Peter Fonda Warren Oates Loretta Swit Lara Parker R.G. Armstrong
| music          = Leonard Rosenman
| cinematography = Robert Jessop
| editing        = John F. Link
| distributor    = 20th Century Fox 
| released       =  
| runtime        = 88 min
| country        = United States English
| budget         = $1,745,000 
| gross          = $12,000,000 
}}
 1975 occult thriller and 92 in the Shade was their third). The film was a hybrid of the horror film|horror, action and car chase genres. Wes Bishop scripted the film with Lee Frost. The two had previously teamed up in 1972 for the camp classic The Thing With Two Heads.

==Plot==
Roger Marsh (Peter Fonda) and Frank Stewart (Warren Oates) own a successful motorcycle dealership in San Antonio, Texas. Together, with their wives Kelly and Alice (Lara Parker, Loretta Swit), along with Roger and Kelly’s small dog, they leave San Antonio in a recreational vehicle (RV) for a much anticipated ski vacation in Aspen, Colorado. 
 Satanic ritual human sacrifice a short distance from their campsite across a river. 

After being caught by the Satanists and barely escaping with their lives, they report the incident to the local sheriff (R.G. Armstrong), who investigates their report but attempts to convince them that they probably only saw hippies killing an animal. Unbeknownst to the sheriff, Roger steals a sample of dirt stained with the murder victims blood, intent on delivering it to the authorities in Amarillo. At the same time, the wives find a cryptic message-a rune- pinned to the broken back window while cleaning, and steal books about occultism from the local library to further research the incident, one such book reveals the ritual was what Satanists often perform to gain magical powers. As the foursome leaves town, the sheriff knowingly sees a truck begin to follow them, making it clear that he is either aware or part of the Satanic cult. 

When the couples stay at a trailer park, Kelly is stared at by its residents and wants to return home. A couple at the park invites them to dinner, When they return from dinner, they discover that Kelly’s dog has been hanged, causing them to immediately leave the park in a panic. Shortly afterwards, they must fight off two rattlesnakes planted in their camper by the cultists. The next day, Roger and Frank purchase a shotgun and head towards Amarillo, spied on by a steadily increasing number of cultists who seem to be networked throughout numerous small Texas towns. When Roger tries to use payphones to call the highway patrol, he is told of a "bad connection" and that the long distance lines are down throughout their town by a "big wind from up north".

The couple leave for Amarillo and stage a showdown with the cult members during a high speed chase that pits their RV against a convoy of trucks and cars. Roger and Frank kill or wound all of the attackers and escape. 

The foursome stop in a field at nightfall, as they cannot continue until morning since the RV’s headlights were damaged during the chase. They begin to celebrate when they pick up a radio signal coming from Amarillo. In the middle of their celebration, the foursome hears chanting outside the RV and find themselves surrounded by cult members wearing black robes with hoods, including the sheriff and the couple they had dinner with at the trailer park. The film ends as the cultists light a ring of fire around the RV, trapping the couples inside.

==Production== San Antonio, Bandera and Leakey, Texas. Director Jack Starrett later claimed he hired actual Satanists to serve as cult-member extras, though he probably stated this for publicity purposes.   Starrett directed the similar B-movie action film A Small Town in Texas the following year. 

==Release==
The film was released theatrically in the United States by 20th Century Fox in June 1975.  It earned North American rentals of $5.8 million. 

The film was released on DVD by Anchor Bay Entertainment in 2005.  This version is currently out of print. 

On April 12, 2011 the film was released on DVD through Shout! Factory, packaged as a double feature with another Peter Fonda film, Dirty Mary, Crazy Larry. As of June 4, 2013, this
Peter Fonda double feature set is available on Blu-ray.

==Remakes announced== Chris Moore Red State. 

The film was also the basis for the Tamil language film Kazhugu (1981 film)|Kazhugu (Eagle) which was released in 1981, and Drive Angry, starring Nicolas Cage, in 2011.

==References==
 

==External links==
*  
*  
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 