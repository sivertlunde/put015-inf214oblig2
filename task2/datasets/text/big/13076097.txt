Encounters at the End of the World
{{Infobox film
| name = Encounters at the End of the World
| image = End of the world post.jpg
| caption = Theatrical release poster
| director = Werner Herzog
| writer = Werner Herzog
| narrator = Werner Herzog
| producer =    Phil Fairclough Dave Harding Julian Hobbs Andrea Meditch Erik Nelson
| cinematography = Peter Zeitlinger 
| editing = Joe Bini  Henry Kaiser David Lindley 
| released =  
| runtime = 99 minutes Discovery Films Discovery Films (USA)  THINKFilm (USA)  Image Entertainment Revolver Entertainment (UK)
| country = United States
| language = English
}}
Encounters at the End of the World is a 2007 American documentary film by Werner Herzog. The film studies people and places in Antarctica. It was released in North America on June 11, 2008, and distributed by THINKFilm. 

==Synopsis== seal camp Henry Kaiser Samuel Bowser and zoologist Jan Pawlowski. Kaiser and Bowser stage a rooftop guitar concert.

Herzog and Zeitlinger return to McMurdo for some more interviews, and visit the preserved original base of Ernest Shackleton. After some brief footage at the South Pole, Herzog interviews penguin scientist David Ainley. This footage includes a shot of a penguin marching in the wrong direction, walking to a certain death in the barren interior of the continent.

Herzog and Zeitlinger next visit Mount Erebus, and interview volcanologists. A strange sequence follows which was shot in tunnels deep below South Pole station carved from snow and ice. Various trinkets and mementos, including a can of Russian caviar and a whole frozen sturgeon, are placed in carved-out shelves in the ice walls, and preserved by the extremely cold and dry air. On the slope of the volcano, Herzog and Zeitlinger explore inside ice caves formed by fumaroles.
 neutrino detection project (ANtarctic Impulse Transient Antenna|ANITA) and features an interview with physicist Peter Gorham. The film concludes with some philosophical words from a maintenance worker, and more footage from the fumarole ice caves and Kaisers dives.

==Production== Henry Kaiser. Kaiser was working on music for Herzogs Grizzly Man and was showing the footage to a friend when Herzog noticed it. Herzog, Kaiser, Zeitlinger, DVD audio commentary for Encounters at the End of the World  Kaiser had been to Antarctica on scientific diving expeditions, as well as with the National Science Foundations Antarctic Artists and Writers Program for his "Slide Guitar Around the World" project.  Within two years, Herzog had released The Wild Blue Yonder, which made prominent use of Kaisers footage.

The film was shot in Antarctica as part of the National Science Foundations Antarctic Artists and Writers Program.  The entire film crew consisted of Herzog, who recorded all production sound, and cinematographer Peter Zeitlinger. The two went to Antarctica without any opportunity to plan filming locations or interview subjects, and had only seven weeks to conceive and shoot their footage. Herzog often met his interview subjects only minutes before he began shooting them. 

Filming in Antarctica is usually overseen by the National Science Foundations media office, which approves and supervises all film productions. Because of Herzogs grant from the Artists and Writers Program, he was allowed to film with no minders or oversight from the NSF. This allowed them to film the "seal-bagging" footage, which is not typically deemed suitable for public release. 

The sound recordings of the seals were produced by Douglas Quin, a renowned Sound expert and professor at the S.I. Newhouse School of Public Communications at Syracuse University, another recipient of the Antarctic Artists and Writers grant. 

The film is dedicated to American critic Roger Ebert. 

==Release==
The film showed at the 2007 Telluride Film Festival,  and had its official premiere  one week later on September 10 at the Toronto International Film Festival.  It also showed in 2008 at the International Documentary Festival in Amsterdam; the Edinburgh International Film Festival; the Cambridge Film Festival and the Melbourne International Film Festival.  Revolver Entertainment released the film theatrically in the UK from 24 April 2009. 

The film   at 28 March 2008, Hong Kong Cultural Centre, for 2008 Hong Kong International Film Festival; and on April 30 at the Minneapolis-St. Paul International Film Festival.

==Critical reception==
The film received very positive reviews from critics. As of September 7, 2013, the review aggregator Rotten Tomatoes reported that 94% of critics gave the film positive reviews, based on 72 reviews — with the consensus that the film "offers a poignant study of the human psyche amid haunting landscapes."  Metacritic reported the film had an average score of 80 out of 100, based on 25 reviews.  On January 22, 2009, it was nominated for an Academy Award for Documentary Feature.

===Top ten lists===
The film appeared on many critics top ten lists of the best films of 2008.     

 
 
*1st - Carrie Rickey, The Philadelphia Inquirer 
*1st - Dennis Harvey, Variety (magazine)|Variety 
*2nd - David Ansen, Newsweek 
*4th - Andrew OHehir, Salon.com|Salon  Dana Stevens, Slate (magazine)|Slate 
*4th - Peter Rainer, The Christian Science Monitor 
 
*8th - Manohla Dargis, The New York Times 
*9th - Michael Rechtshaffen, The Hollywood Reporter 
*10th - Andrea Gronvall, Chicago Reader 
*10th - Richard Corliss, Time (magazine)|TIME magazine 
*10th - Sheri Linden, The Hollywood Reporter 
 

==Awards==
Encounters at the End of the World won the award for Best Documentary at the Edinburgh International Film Festival and Special Prize at Planet Doc Review Festival in Warsaw (both in 2008).

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*   by interviewee Bill Jirsa

 

 
 
 
 
 
 
 
 