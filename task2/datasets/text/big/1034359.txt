Fahrenhype 9/11
{{Infobox film
| name = FahrenHYPE 9/11
| image = FahrenHype 911.jpg
| caption = A promotional poster. Alan Peterson
| producer = Michael R. Fox Steve Haugen Jeff Hays Dave Sapp Lee Troxler
| writer = Eileen McGann Dick Morris Lee Troxler
| narrator = Ron Silver
| starring = Ron Silver Dick Morris
| music =
| cinematography = Doug Monroe Dave Sapp
| editing = Michael R. Fox Lee Troxler
| distributor = Trinity Home Entertainment Overstock.com
| released =  
| runtime = 80 min.
| country = United States
| language = English
| budget =
}}
 documentary Fahrenheit Senator Zell Democratic New York City mayor Ed Koch.

The movie was released with a companion book, on October 5, 2004, the same day that Fahrenheit 9/11 was released on home video. 

==Reviews and response==
The documentary received four reviews at Rotten Tomatoes.  Film reviewer Robert Koehler described the film in Variety (magazine)|Variety as "a broadside," but noted that "its vastly superior in content and style to most of the recent anti-Moore focus."  Jim Emerson reviewed the video as well, saying that the documentary "isnt exactly a thorough or level-headed piece of reasoning or investigative journalism; its every bit the hot-headed political advertisement that Moores film was, just a lot less funny." 

After receiving complaints about fairness, a library in San Diego postponed and moved a free showing of Moores Fahrenheit 9/11 film to a location where it could be shown along with this video.  Controversy was generated in Chicago when a student from the Northwestern University Young Republicans had publicized an election night party and showing of the documentary in the bar area of a theater, after the offer was rescinded by theater management.  An after school showing of Moores Fahrenheit 9/11 requested by North Sutton, New Hampshire high school students was cancelled after several conservative parents complained. The high school superintendent told the teachers to, "in the interests of balance", not show Fahrenheit 9/11 without also showing Fahrenhype. 

==References==
 

==See also==
*Fahrenheit 9/11 controversies|Fahrenheit 9/11 controversies

==External links==
* 

 
 
 
 
 
 
 
 