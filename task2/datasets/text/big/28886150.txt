Man About Town (1939 film)
{{Infobox film
| name           = Man About Town
| image          = Man-About-Town-1939.jpg
| image_size     =
| caption        = Film poster
| director       = Mark Sandrich
| producer       = Arthur Hornblow, Jr. Allan Scott (story) Morrie Ryskind (story and screenplay)
| narrator       =
| starring       = Jack Benny Dorothy Lamour
| music          =
| cinematography =
| editing        =
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Man About Town is a 1939 musical comedy film starring Jack Benny and Dorothy Lamour. A producer tries to get his leading lady take him seriously romantically by pursuing other women.

==Cast==
*Jack Benny as Bob Temple
*Dorothy Lamour as Diana Wilson Edward Arnold as Sir John Arlington
*Binnie Barnes as Lady Arlington
*Monty Woolley as Henri Dubois
*Isabel Jeans as Mme. Dubois
*Phil Harris as Ted Nash
*Betty Grable as Susan Hayes
*Edward E. Clive|E. E. Clive as Hotchkiss Eddie Anderson as Rochester
*The Merriel Abbott Dancers as Themselves
*Matty Malneck and His Orchestra as Themselves
*The Pina Troupe as Themselves

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 


 