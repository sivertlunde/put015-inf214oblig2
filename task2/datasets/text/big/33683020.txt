Zila Ghaziabad
{{EngvarB|date=September 2014
}}
 
{{Infobox film
| name = Zila Ghaziabad
| image = Zilla Ghaziabad.jpg
| caption = Theatrical release poster Anand Kumar Vinod Bachan
| writer = Anand Kumar
| starring = Sanjay Dutt Arshad Warsi Vivek Oberoi Charmy Kaur Ravi Kishan Paresh Rawal Sunil Grover
| music = Amjad Nadeem
| cinematography =
| editing =
| studio = Soundrya Production
| distributor = Showman International
| released =   
| runtime = 143 minutes
| country = India
| language = Hindi
| budget =    
| gross =  
}} political action Anand Kumar. It is produced by Vinod Bachan, and presented by Mohammed Fasih & Bharat Shah. The film features Arshad Warsi, Vivek Oberoi, Sanjay Dutt, Charmy Kaur, Ravi Kishan, Paresh Rawal and Sunil Grover in lead roles amongst others. It was released on 22 February 2013.    Upon release, the film received mixed reviews, with a poor response at the box office. Zila Ghaziabad was described by critics as "a disaster", and ended up becoming one of the biggest flops of the year. 

==Premise==
The movie is based on the true story of the gang war that lasted between two groups in the 80s and 90s in Ghaziabad, India. In the village of Ghaziabad, jagmal Singh Choudhary (Paresh Rawal) is a corrupt chairman, and has connections with the gangster Fauji (Arshad Warsi), who is willing to do anything to make a quick buck. When jagmal refuses to pay up to 2&nbsp;million for Faujis sisters wedding, Fauji breaks their ties and leaves him. At the same time, Brahampal befriends a polite teacher named Satbir (Vivek Oberoi) who also happens to be in a relationship with jagmals daughter Sanika (Charmy Kaur).

Thinking that jagmal has replaced him with Satbir, Fauji becomes furious and joins forces with Rashid (Ravi Kishan), a politician and Brahampals enemy. Rashid deliberately turns Fauji against Satbir, making him think that he wants him dead. When Fauji tries to attack Satbir, an elderly village spokesman is shot, leading Satbir to fight back against him. But before Satbir is able to do anything about it, Satbirs older brother Karamvir (Chandrachur Singh) is kidnapped, and soon killed by Fauji. Seeking revenge, Satbir turns to the crime world to finish off his brothers murderer. With riots taking place all over, the police department shifts a crazy, tough, feared and solid officer, Thakur Pritam Singh (Sanjay Dutt) to enter the city bring a stop to this gang-war. However, Pritam Singh ends up making it all worse.

Pritam Singh initially visits Fauji and Rashid and later on, invites Satbir to the Police Station. Having gauged their attitudes, Pritam Singh decides to side with Satbir in the war against Fauji and Rashid. With elections approaching, both parties eventually decide to contest with Rashid and Satbirs brother contesting. Rashid wins the election seeing which Fauji goes on a killing spree beginning his attack starting from Satbirs house and accidentally ends up killing Satbirs now wife Sanika. Enraged at this act of Fauji, Satbirs brother kidnaps his girlfriend – an act that is looked down upon by Satbir. Before she could be returned safely, Fauji invades the place and kills Satbirs brother prompting a furious Satbir to go on a revenge spree.

In the meanwhile, a tape is discovered which not only reveals that Rashid and Faqeera have secretly made a deal with Pritam Singh to murder Fauji, thereby betraying him, but also shows Faqeera confessing to having ordered the attack on Faujis house at the start of the movie which is what triggered the whole gang-war in the first place. Furious at this betrayal, Fauji kills Faqeera and then heads out to kill Rashid. Pritam Singh also joins in, apparently fulfilling his promise to kill Fauji for Rashid but ends up killing Rashid instead. This results in an action-packed finale that sees Satbir killing Fauji and being arrested by Pritam Singh. His fate then is left unknown.

==Cast==
* Sanjay Dutt as Thakur Pritam Singh
* Arshad Warsi as Mahendra Fauji Baisla
* Vivek Oberoi as Satbir Gurjar
* Ravi Kishan as Rashid Ali
* Charmy Kaur as Suman
* Sunil Grover as Faqeera
* Paresh Rawal as Jagmal Singh Choudhary (chairman)
* Minissha Lamba as Faujis girlfriend
* Chandrachur Singh as Karamvir
* Ashutosh Rana as Sangram Singh
* Ashutosh Kaushik as Pandit
* Divya Dutta as Mahenderi 
* Geeta Basra in an Item number (special appearance) 
* Shriya Saran in an Item number (special appearance) 

==Soundtrack==
The soundtrack of Zila Ghaziabad is composed by Amjad Nadeem, with Bappi Lahiri composing "Chamiya No. 1." The album contains five tracks. Lyrics by Shabbir Ahmed.

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track # !! Song !! Length !! Singer(s)
|-
| 1
| "Ye Hai Zila Ghaziabad"
| 5:22
| Sukhwinder Singh
|-
| 2
| "Ranjha Jogi"
| 4:11
| Sonu Nigam & Shreya Ghoshal
|-
| 3
|"Baap Ka Maal"
| 5:22
| Sukhwinder Singh, Mika Singh, Mamta Sharma 
|-
| 4
| "Tu Hai Rab Mera"
| 4:49
| Mohit Chauhan & Tulsi Kumar
|-
| 5
| "Chhamiya No 1"
| 4:32
| Sunidhi Chauhan
|-
| 6
| "Baap Ka Maal  "
| 5:33
| Mika Singh & Anupama Raag
|-
|}

==References==

 

==External links==
*  

 
 
 