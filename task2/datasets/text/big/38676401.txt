Things I Never Told You
{{Infobox film
| name           = Things I Never Told You
| image          =
| caption        =
| director       = Isabel Coixet
| producer       = Coproducción España-USA; Carbo Films
| writer         = Isabel Coixet
| based on       =
| starring       = Lili Taylor Andrew McCarthy Peggy West Leslie Mann Sherilyn Lawson Linda Ruth Goertz Kathryn Hurd Chanda Watts Kathleen Edwards Alexis Arquette Seymour Cassel
| music          = Alfonso Vilallonga
| cinematography =
| editing        = Teresa Medina
| studio         =
| distributor    = Sony Pictures Classics
| released       = 1996
| runtime        = 93 minutes
| country        = Spain Oregon
| language       = English
| budget         =
| gross          =
}}

Things I never told you  is a 1996 Spanish/American romantic comedy-drama film directed by Isabel Coixet and starring Lili Taylor and  Andrew McCarthy.

== Plot ==
The film opens with a voice-over by Don who states that in life and relationships, "anything can happen."

Ann works at a camera shop in a small town in the Pacific Northwest.  One day, she gets called by her boyfriend who is in Prague and gets dumped by him.  Ann makes a halfhearted suicide attempt by drinking a bottle of nail polish remover.  After she leaves the hospital, her psychiatrist gives her the number to a crisis help line.  One evening, still distraught over being abandoned by her boyfriend, Ann calls the number and speaks with Don, who volunteers there because he finds it less depressing to talk with other depressed people about their problems than to stay at home and feel sorry for himself.  They make an emotional connection, but Ann hangs up on him when Don admits that he doesnt really know what love is. 

The same time this is happening Ann begins making a series of emotional video tapes addressing her ex-boyfriend and how desolated she is by him leaving her.  She gives the tapes to a neighbor who works at a package delivery service to send to Prague, but the neighbor secretly opens the packages, watches the tapes, and becomes infatuated with Ann.

In an apparent coincidence some time after they spoke on the phone, Don walks into the camera shop where Ann works and buys a camera from her for his work as a real estate agent.  While they talk, Ann makes a comment about happiness being "unfair" that is similar to something she said when she called the help line and spoke with Don then, and he apparently realizes who she is but does not let that on to her.  

Apparently attracted to Ann, Don hangs around in a coffee shop waiting for her to get off from work.  He approaches her on the street, and she invites him to help her do her wash at a local laundromat.  They talk for a while in the laundromat, and then she invites him to come to her house in a couple of days.

Don comes over to Anns house, and they go inside and have sex.  Don is unaware that Ann has taped this encounter with the apparent intent of sending the tape to her ex-boyfriend (but once again, the neighbor holds onto and watches the tape).

After their encounter, Ann ducks Dons calls.  After a couple of days, Don gets called to help the police persuade a man who he had been counseling to come out of a hotel room where he is apparently hiding with a gun.  Don talks to the man and prevents him from killing himself but is badly injured when the gun goes off.  When Ann hears about what has happened, she rushes to the hospital and waits until Don is out of danger but leaves when it becomes clear he will recover.

While this is happening, Anns ex-boyfriend calls her and tells her he wants to get back together with her.  Ann hastily packs her belongings and leaves the town without leaving word to Don or her ex-boyfriend.  Don goes home from the hospital and begins traveling frequently in his job as a real estate agent.  The film ends with Don musing about what he would tell Ann if he saw her while he sits on a park bench.  A woman walks by, looks at him, sits down, and smiles at him.  He looks up and sees it is Ann.  Before the credits roll, we hear a voice-over of Don saying again, "Anything can happen."

== Cast ==
* Lili Taylor as Ann.
* Andrew McCarthy as Don Henderson.
* Peggy West as Woman with Camera.
* Leslie Mann as Laurie.
* Sherilyn Lawson as Ice cream woman.
* Linda Ruth Goertz as Aurora.
* Kathryn Hurd as Muriel.
* Chanda Watts as Anns nurse.
* Kathleen Edwards as Dr. Lewis
* Alexis Arquette as Paul
* Seymour Cassel as Frank.

== Reception ==
Things I never told you  received generally positive reviews from film critics. As of March 2013, review aggregator Rotten Tomatoes has scored a 79% rating, with an average rating of 3.9 out of 5, based on 322 reviews.

This movie has won 8 awards: ADIRCAE Award for best director, Isabel Coixet; CEC Award for Best Screenplay Original, Isabel Coixet; Fotogramas de Plata for best film, Isabel Coixet; Film Award for best director, Isabel Coixet; Sant Jordi for Best Spanish film, Isabel Coixet; Best Actress for Lili Taylor and Silver Alexander award for Isabel Coixet; Audience Award for Best Spanish film, Isabel Coixet.

==External links==
*  
*  
*  

 
 
 