Ajami (film)
{{Infobox film
| name           = Ajami
| image          = Ajami Arabic.jpg
| image_size     = 
| alt            = 
| caption        = Arabic-language Theatrical poster
| director       = Scandar Copti Yaron Shani
| producer       = Moshe Danon Thanassis Karathanos Talia Kleinhendler
| writer         = Scandar Copti Yaron Shani
| starring       = Fouad Habash Ibrahim Frege Scandar Copti Shahir Kabaha Eran Naim
| music          = Rabih Boukhari
| cinematography = Boaz Yehonatan Yaacov
| editing        = Scandar Copti Yaron Shani
| studio         = 
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = Israel Arabic Israeli Hebrew
| budget         = $1 million
| gross          = $2.2 million
}} Ajami neighborhood of Jaffa, Israel.

==Plot==
The film contains five story lines, each of which is presented in a non-chronological fashion. Some events are shown multiple times from varying perspectives. A young Israeli Arab boy, Nasri, who lives in the Ajami neighborhood of Jaffa, narrates the film.

In the first story, Nasris neighbor—a teenage boy—is shot to death by a well-known Bedouin clan in a drive-by shooting while working on his car. Nasri explains that the intended target was his older brother Omar, who had previously sold the car to the neighbor. The botched hit was revenge for a loss of one of Bedouin clan members, who was shot and paralyzed by Nasris uncle in a dispute. Nasri and his younger sister are sent to Jerusalem, while Omar, his mother, and grandfather stay behind. Fearing for his familys safety, Omar seeks protection and guidance from Abu Elias, an affluent restaurant owner, and well known and respected member of the Jaffa community. Abu Elias arranges for a three day cease fire, and hires a lawyer to represent Omar in tribal court. During this time, Nasri and his sister return home. At the conclusion of the court session, the judge declares that Omar must pay tens of thousands of dinars—the equivalent of tens of thousands of US dollars—so peace can be restored. Omar is given three weeks to make good on his payment. Omar and his friend Shaata attempt petty crime in order to come up with the finances, but are unsuccessful at bringing in enough money. Omars mother attempts to persuade him to escape with the family, but Omar refuses to leave, believing that there is no place to run to.

The second story introduces a young teenaged boy named Malek who lives in the Palestinian territory of Nablus. Malek is illegally employed in Abu Eliass restaurant, and works out of desperation to make enough money for his ailing mothers bone marrow transplant surgery. Malek is friends with Omar, who has also become a recent employee at the restaurant. It is also revealed that Omar, a Muslim, is in love with Abu Eliass daughter Hadir, a Christian. Abu Elias, once discovering the secret couple later in the film by catching them in the surreptitious act of flirtation, does not approve of their relationship, and angrily fires Omar, warning him to stay away from his daughter.

The third story shows a brief, but violent encounter between an older Jewish man and his three young drug dealing Arab neighbors. The dispute begins when the Jewish man complains to the young men that he has not been able to sleep, due to the fact that their bleating sheep keep him up all night. The disagreement soon escalates, and one of the young men mortally stabs the Jewish man. The three young men go into hiding before the police arrive. Amongst the policemen who arrive at the scene is an Israeli officer named Dan, nicknamed Dando by his friends. Viewers learn that Dandos younger brother Yoni has gone missing during his service in the Israeli Defense Forces. While rumors circulate that Yoni may have run away and became very religious, Dandos family—mother and father specifically—suspect that he may have been kidnapped or murdered by a Palestinian terrorist organization. Dando—a family man with a wife and kids—has remained strong for his emotionally broken family, as they make attempts at locating his brother. Later in the story, Dando is informed that the army has discovered the remains of what is believed to be a murdered Israeli soldier in the Palestinian territories. It is soon thereafter confirmed that the remains are Yonis, and Dando—emotionally traumatized—vows to find the murderer and bring him to justice.

In the fourth story, viewers learn of the character Binj (who is played by co-director Scandar Copti) an eccentric cook who works in Abu Eliass restaurant. He is also close friends with Omar, Shaata and Malek. Binj is in love with a Jewish girl from Tel Aviv, and is thinking of moving in with her, much to the dismay of his group of friends. It is revealed that Binjs brother was one of the three involved in the stabbing of the Jewish man in Jaffa. Both Binj and his father are taken in and interrogated by the police. After his release, Binj reluctantly agrees to accept a great deal of drugs that belongs to his brother who is still on the run. Early one morning, after a social gathering in his house, Binj awakes Malek so he wont be late to the restaurants opening and hides in his presence the brick of drugs. Just when Malek is leaving he sees three Hebrew speaking men enter Binjs house. When Binj is found dead in his apartment not long afterward, Malek and Omar initially suspect that he was murdered by a group of Israeli drug dealers. It is later revealed that those three men were actually policemen who came to search Binjs house and intimidate him into revealing his brothers location. Having to leave after he told them nothing the police promises Binj to return. Binj, tired from the situation and annoyed by the ongoing harresment of the police, discarded the majority of the drugs, put sugar powder inside packages mimicking drug bricks and hid them around the house in an attempt to mock the police, should they ever return. Binj then proceeds to snort the remainder of drugs he did not discard and accidentally dies of a drug overdose. All of this is unbeknownst to Malek and Omar who, after Binjs death, takes one of the hidden mock-drug packages and, thinking it is the drugs, decides to sell it to a drug dealer in an attempt to pay off their respective debts. Abu Elias learns of their plans and tips off the police, thinking Omar will be caught, thus ending the relationship between Omar and his daughter. Initially, Abu Elias fires Malek after learning of his involvement with drugs, but after Maleks pleading and after learning that Omar will not go alone to the exchange he changes his mind, and instructs Malek to meet the dealers with Omar, but warns him not to carry the drugs on his person. He assures Malek that once Omar is taken into custody by the police, Malek can return to the restaurant and that his sick mother will be taken care of. Like Omar and Malek however, Abu Elias does not realize that the drugs are fake.

The fifth story shows the encounter between Omar, Malek, and the drug dealers. Toward the beginning of the film, viewers are shown the scene, and initially led to believe that Malek was shot to death by the drug dealers, once they discovered the drugs were fake. It is revealed later however, that the dealers were actually policemen executing a sting operation. It is also revealed that Omars younger brother Nasri insisted on accompanying Omar and Malek to the meeting, afraid that something bad would happen to his brother. Upon arrival, Omar tells Nasri to stay in the car, and at Maleks urging, leaves his gun behind as well. At the meeting, the police tackle and beat Omar and Malek after they discover the drugs are fake. Dando, who is a part of the sting, sees Malek with the pocket watch he planned to give Abu Elias as a present. Dando believes that the watch belonged to Yoni and in a fit of rage beats Malek and aims his gun at him with the intent to murder him. However Nasri, who hadnt stayed in the car as ordered, sees the gun pointed at Malek, and shoots Dando with Omars gun. He is then shot and killed by another officer. The film ends with Omar escaping down an alleyway and getting back to the car, only to discover that Nasri is missing.

==Production== German and Israeli companies – Inosan Productions, Twenty Twenty Vision, Israel Film Fund, Medienboard Berlin-Brandenburg, ZDF, Arte, World Cinema Fund. 

==Reception==
The film currently holds a 97% Certified Fresh rating at Rotten Tomatoes, based on 64 reviews with an average rating of 7.7/10.
 City of God and Gomorra (film)|Gomorra.

Ajami was the first predominantly Arabic-language film submitted by Israel for the Academy Award for Best Foreign Language Film, and it was nominated for the award. {{Citation
 | title = ‘Ajami’ nominated for Oscar 
 | url = http://www.jpost.com/ArtsAndCulture/Entertainment/Article.aspx?id=167582
 | date = 2010-02-02
 | author = Brown, Hannah
 | journal = The Jerusalem Post
}}  It lost to El secreto de sus ojos (Argentina). It was the third year in a row that an Israeli film was nominated for an Academy Award.

===Awards=== Cannes Film Festival:
** Caméra dOr - Special Mention (Scandar Copti and Yaron Shani) (won)
* European Film Awards:
** European Film Academy Discovery (nominated)
* Jerusalem Film Festival:
** Best Full-Length Feature (won)
* London Film Festival:
** Sutherland Trophy
*  Ophir Award:
** Best Film (won)
** Best Director (Scandar Copti and Yaron Shani) (won)
** Best Screenplay (Scandar Copti and Yaron Shani)(won)
** Best Music (Rabih Boukhari) (won)
** Best Editing (Scandar Copti and Yaron Shani) (won)
* Tallinn Black Nights Film Festival:
** Best Eurasian Film (won)
* Thessaloniki International Film Festival
** Golden Alexander (won)
* 82nd Academy Awards:
** Foreign Language Film (nominated)

==See also==
* 2009 in Israeli film

==Notes==
 

==References==
*  on Free TV Movie Database

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 