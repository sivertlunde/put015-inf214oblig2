Home for Christmas (film)
{{Infobox film
| name           = Home for Christmas
| image          = 
| caption        = 
| director       = Jaakko Pakkasvirta
| producer       = Jaakko Pakkasvirta
| writer         = Väinö Pennanen Jaakko Pakkasvirta
| starring       = Paavo Pentikäinen
| music          = 
| cinematography = Esa Vuorinen
| editing        = 
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = Finland
| language       = Finnish
| budget         = 
}}

Home for Christmas ( ) is a 1975 Finnish drama film directed by Jaakko Pakkasvirta. It was entered into the 9th Moscow International Film Festival.   

==Cast==
* Paavo Pentikäinen as Urho Suomalainen
* Irma Martinkauppi as Sirkka Suomalainen
* Kaisa Martinkauppi as Leena Suomalainen
* Jari Erkkilä as Jari Suomalainen
* Selma Miettinen as Urhos mother
* Aapo Vilhunen as Sisters husband
* Tuija Ahvonen as Sirkkas sister
* Timo Martinkauppi as Alaja

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 