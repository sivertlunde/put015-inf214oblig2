Mister Scarface
{{Infobox Film
  | name = Mister Scarface
  | image = MrScarface.jpg
  | caption = DVD cover for Mister Scarface
  | director = Fernando Di Leo 
  | producer = Armando Novelli
  | writer = Peter Berling Fernando Di Leo
  | starring = Jack Palance 
  | music = Luis Enríquez Bacalov    
  | cinematography = Erico Menczer
  | editing = Amedeo Giomini
  | distributor = Troma Entertainment
  | released = 1976 
  | runtime = 90 minutes
  | language = English
  | budget =  
  | preceded_by = 
  | followed_by = 
  }}
 Italian poliziottesco film directed by Fernando Di Leo and starring Jack Palance.

== Plot ==
Tony (Harry Baer) is a mob loan collector who is unsatisfied with his position in life, and constantly dreams of living it rich in Brazil with his brother. To make some quick cash, Tony joins the forces of organized crime, making his way up the ladder. Together with Napoli, another mob enforcer, Tony hatches a plan to con mob boss Manzari (Palance) out of a fortune, but Manzari isnt about to let that happen.

== Cast ==

*Harry Baer: Tony
*Al Cliver: Ric  
*Jack Palance: Manzari aka "Mr. Scarface"
*Vittorio Caprioli: Vincenzo Napoli
*Gisela Hahn: Clara
*Edmund Purdom: Luigi Cerchio
*Peter Berling: Valentino

==External links==
* 
* 

 
 
 
 
 
 
 


 
 