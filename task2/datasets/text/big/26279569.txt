Cyrus (2010 comedy-drama film)
{{Infobox film
| name = Cyrus
| image = Cyrus poster.jpg
| alt =
| caption = Theatrical release poster
| director = {{Plainlist|
* Jay Duplass
* Mark Duplass}} Michael Costigan 
| writer = {{Plainlist|
* Jay Duplass
* Mark Duplass}}
| starring = {{Plainlist|
* John C. Reilly
* Jonah Hill
* Marisa Tomei
* Catherine Keener}} Michael Andrews
| cinematography = Jas Shelton
| editing = Jay Deuby
| studio = Scott Free Productions
| distributor = Fox Searchlight Pictures
| released =  
| runtime = 91 minutes
| country = United States
| language = English
| budget = $7 million   
| gross = $9,931,520 
}} Jay and Mark Duplass and starring John C. Reilly, Jonah Hill, Marisa Tomei, and Catherine Keener.

==Plot==
The film opens as Jamie (Catherine Keener) walks in on her ex-husband John (John C. Reilly) as he is masturbating. She had come to his house to tell him that she is getting married. Even though they have been apart for seven years, the news still devastates John, who is already in a depression. Jamie insists that John accompany her to a party the following night and try to cheer up. At the party, John tries various conversational tactics with different women, failing to spark a connection each time. He gets progressively drunker until he ends up urinating in the bushes, where Molly (Marisa Tomei) compliments him on his penis and strikes up a conversation. Molly goes back to Johns house and leaves during the night, after they have had sex.

Molly returns for a second date the next night, and once again, she leaves after they have had sex. John follows her to her house and falls asleep in his car. The next morning, he warily approaches the house, where he is surprised to meet Mollys son Cyrus (Jonah Hill). Cyrus invites John inside and makes friendly conversation with him. Molly is startled to see John in her house when she returns, but the trio have dinner together. John is slightly unnerved by evidence that Molly and Cyrus are closer than normal for a mother and son.

The next morning, John cannot find his shoes, which he had left in the living room. As the day wears on, he is increasingly disturbed by their disappearance and starts to worry that Cyrus has been messing with him the whole time. He ropes Jamie into meeting Molly and Cyrus, in order to appraise his paranoia. Jamie finds Cyrus sweet, if a little overly intimate with his mother. Relieved, John returns for another night at Mollys home. As they begin to have sex for the first time in her house, Cyrus screams in his room, and Molly runs to comfort him. She does not return to John, who goes out looking for her in the middle of the night. Instead, he encounters Cyrus who is holding a large kitchen knife. Cyrus explains that he had a night terror, and that Molly has gone to sleep. He then counsels John to back off on the relationship, because he is scaring off Molly. John leaves a note for her and returns home.

In the morning, Cyrus sits Molly down and tells her that John had confessed to him that she was coming on too strong. When she presses Cyrus for more information, he explodes in a tantrum and storms off, checking through the window to make sure he has upset his mother. Molly calls John and begs him to come over, while she waits for Cyrus to return. When Cyrus finally comes home, he explains that he has rented a room and will be moving out. After a few days alone together, John decides that he wants to move in with Molly.

After one date, they return home and begin to have sex, while Cyrus sits in the darkened kitchen. He surprises them both and explains that he has had another panic attack and wants to return home. In private, John finally confronts Cyrus about everything, and Cyrus admits that he has been deliberately sabotaging his mothers relationship. He moves back home, and John remains wary of him. The night before Jamies wedding, he warns Cyrus not to screw up the day, because it means a lot to him. At the wedding, however, Cyrus is hurt when he sees how the event stirs romantic feelings between John and his mother. He confronts John in the bathroom and attacks him, yelling that John will not take his mother away from him. As John defends himself, the two spill out of the bathroom, into the view of the other guests. Cyrus makes it look like John attacked him. John advises Molly to open her eyes, then he storms off, furious at Cyrus.

Later, Molly believes Johns explanation, but John will not consider continuing the relationship, convinced that Cyrus will only continue to sabotage it and that he will end up alone again in a few years. He moves into a dumpy apartment. Meanwhile, Molly confronts Cyrus about his behavior, and she explains that she is deeply unhappy at the loss of her relationship. Cyrus reconsiders his position and visits John, begging him to come back. John insists that he will not, despite his love for Molly. He is certain that Cyrus will only ruin things again. Unsure if Cyrus has left, John opens the door to see him sitting on the steps with tears in his eyes. They exchange some dialogue that denotes a truce between them. Cyrus then asks if he can get a ride home. John inquires as to whether or not Molly is home and Cyrus says that she is not. Once home, Cyrus and John hug to show that the truce is genuine. Once Cyrus goes into the house, Molly comes out alluding to the prospects of Cyrus now using his manipulative power to keep their relationship together instead of trying to rip it apart. John and Molly lock eyes and it looks like the relationship is back on track. The movie ends with John starting to get out of the car.

==Cast==
* John C. Reilly as John Kilpatrick
* Jonah Hill as Cyrus Fawcett
* Marisa Tomei as Molly Fawcett
* Catherine Keener as Jamie Matt Walsh as Tim
* Diane Mizota as Thermostat Girl
* Kathy Ann Wittes as Ashley
* Katie Aselton as Pretty Girl
* Jamie Donnelly as Pastor
* Tim Guinee as Roger
* Charlie Brewer as Stranger at Reception
* Steve Zissis as Rusty
* Carrie McJane as Tricky

==Reception==
The film has received generally positive reviews. Review aggregate   ideals of directors Jay and Mark Duplass. 
Critics reacting negatively include Manohla Dargis of The New York Times, who criticized the Duplass brothers for displaying an "almost aggressive lack of ambition",  and Damien Magee of 702 ABC Sydney, who identified Cyrus as "the sort of film that many people, including a number of well-respected critics, have started to confuse with good cinema" going on to call it "a checklist of indie-chic clichés", and concluding with the suggestion "If youre really stuck for something to see, stick this on the maybe pile, otherwise marked Juno (film)|Juno."  Peter Travers added it to his top ten movies of 2010 as one of the movies that tied for eleventh place.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 