Diary of a Lost Girl
{{Infobox film
| name           = Diary of a Lost Girl
| image          = Diary323.JPG
| caption        = Original German poster
| director       = G.W. Pabst
| producer       = G.W. Pabst
| writer         = Margarete Böhme Rudolf Leonhardt
| starring       = Louise Brooks Fritz Rasp André Roanne Josef Ravensky Franziska Kinz
| music          = 
| cinematography = Sepp Allgeier Fritz Arno Wagner
| editing        = 
| released       =  
| runtime        = 
| country        = Weimar Republic
| language       = Silent film German intertitles
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 black and Tagebuch einer Verlorenen (1905) by Margarete Böhme. A previous version of the novel, directed in 1918 by Richard Oswald, is now considered a lost film.

==Plot==
Thymian Henning (Louise Brooks), the innocent, naive daughter of pharmacist Robert Henning (Josef Rovenský), is puzzled when their housekeeper, Elisabeth (Sybille Schmitz), leaves suddenly on the day of Thymians confirmation. It turns out that her father got Elisabeth pregnant. Elisabeths body is brought to the pharmacy later that day, an apparent suicide by drowning, upsetting Thymian. 

Thymians fathers assistant Meinert (Fritz Rasp), promises to explain it all to her late that night, but instead takes advantage of her; she gives birth to an illegitimate child.   Though Thymian refuses to name the babys father, the relatives find out from her diary. They decide that the best solution is for her to marry Meinert. When she refuses because she does not love him, they give the baby to a midwife and send her to a strict reformatory for wayward girls run by a tyrannical woman (Valeska Gert) and her tall, bald assistant (Andrews Engelmann).

Meanwhile, Thymians friend, Count Osdorff (André Roanne), is cast off and left penniless by his rich uncle, also Count Osdorff (Arnold Korff), after he proves unsuccessful at every school and trade. Thymian begs her friend to persuade her father to take her back, but Thymians father has married his new housekeeper, Meta (Franziska Kinz), and Meta wants no rivals. 

Rebelling against the reformatorys rigid discipline, Thymian and her friend Erika (Edith Meinhard) escape with Osdorffs help. When Thymian goes to see her baby, she is told the child has just died. After despondently wandering the streets, she re-unites with Erika, who is working in a small, upper-class brothel. With no skills, Thymian also becomes a prostitute. 

By chance, Thymian runs into her father, Meta and Meinert in a nightclub. Her father is shocked when he realizes what she has become, but Meta and Meinhert take him away.

Three years later, her father dies. With the expectation of inheriting a large amount of money, she decides to start a new life. The others suggest she obtain a new identity by marrying Osdorff. After thinking about it, he agrees. At the lawyers office, Meinhert buys Thymians interest in the pharmacy, making her rich. However, when she learns that Meinhert is throwing the widow and her two children out on the street, Thymian gives Meta the money so that her young half-sister will not suffer her fate.  

Osdorff, who had been counting on the money to rebuild a life for himself too, throws himself out the window to his death when she tells him what she has done.  The uncle, grief-stricken, decides to make amends by taking care of Thymian. He introduces her to his cousin as his niece, Countess Osdorff.

In a strange twist of fate, she is invited to become a director of the same reformatory where she herself was once held.  When an old friend is brought before the directors as an "especially difficult case", Thymian denounces the school and its "blessings" and takes her away. Count Osdorff has the last word as he is leaving: "A little more love and no-one would be lost in this world."

== See also ==
*List of films made in Weimar Germany

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 