Aiyyaa
 
 
{{Infobox film
| name           = Aiyyaa
| image          = Aiyyaa poster.jpg
| alt            = Film Poster
| caption        = Theatrical release poster
| director       = Sachin Kundalkar
| producer       = Anurag Kashyap Guneet Monga Viacom 18 Meraj Shaikh
| writer         = Sachin Kundalkar Prithviraj Subodh Bhave Nirmiti Sawant
| music          = Amit Trivedi
| lyrics         = Amitabh Bhattacharya
| cinematography = Amalendu Choudhary
| editing        = Abhijeet Deshpande
| studio         = Viacom 18 ShowMaker Pictures
| distributor    = ShowMaker Pictures
| released       =  
| mpaa rating    = PG-13
| country        = India
| language       = Hindi
| Budget         =  
| Box office     =    
}} Viacom 18.    The film stars Rani Mukerji and Prithviraj in the lead roles. This film is the debut Bollywood film of Malayalam actor Prithviraj Sukumaran. The trailer for the film was released on 6 September 2012. The movie was released on 12 October 2012. Aiyyaa was an average grosser.

== Plot ==
The story is about a  ) who is blind and has gold teeth, her father who smokes four cigarettes together, her mother ( ), her colleague, an eccentric woman who dresses up in weird ensembles inspired by pop star Lady Gaga.
Meenakshis family is looking for a suitable groom but Meenakshi, who doesnt believe in arranged marriages, is waiting for her prince and wants her dream wedding. Thats when Surya (Prithviraj Sukumaran|Prithviraj) enters. Surya is an art student, and the moment Meenakshi looks at him she falls in love with his tanned skin and a mysterious fragrance emanating from him. By this time her family has found the right guy Maadhav (Subodh Bhave) for her, and are rushing with her wedding. The rest of the film involves Madhav running around after Meenakshi, and Meenakshi following Surya.Nana gets engaged to Maina under bizarre circumstances when Meenakshi goes missing on her engagement date when she actually was following Surya and ends up in his incense sticks factory. Meenakshi learns that Suryas fragrance, that she got enthralled to, was actually because of his involvement in the factory. In the end, Meenakshi eventually succeeds in winning over Suryas heart and they get engaged in a traditional Maharashtrian ceremony.

== Production ==
Pre-production started in August 2011 and the film went into shooting by the first week of October 2011. The shooting ended in April 2012, and the film was released on 12 October 2012. 

== Certifications ==
Aiyyaa was Given a UA by the CBFC despite the sexual scenes.   

The BBFC gave Aiyya a 12a for Infrequent moderate sex and drug references. 

The MPAA gave the film an R and then PG-13 due to demand for Infrequent comic uses for drugs. 

== Soundtrack ==
{{Infobox album
| Name = Aiyyaa
| Longtype = to Aiyyaa
| Type = Soundtrack
| Artist = Amit Trivedi
| Cover = 
| Border = 
| Alt = 
| Caption = 
| Released = 12 September 2012
| Recorded =  Feature film soundtrack  World Music
| Length =  Hindi
| Label = T-Series
| Producer = Amit Trivedi
| Last album = English Vinglish (2012)
| This album = Aiyyaa (2012)
| Next album = Luv Shuv Tey Chicken Khurana (2012)
}}

{{Track listing
| extra_column = Singer(s)
| all_lyrics      = Amitabh Bhattacharya
| all_music       = Amit Trivedi 
| title1 = Dreamum Wakeuppam
| extra1 = Sowmya Raoh, Rupesh Ubh
| length1 = 3:31
| title2 = Sava Dollar (Lavani)
| extra2 = Sunidhi Chauhan
| length2 = 4:47
| title3 = Aga Bai
| extra3 = Shalmali Kholgade, Monali Thakur
| length3 = 4:25
| title4 = Mahek Bhi
| extra4 = Shreya Ghoshal
| length4 = 5:18
| title5 = What To Do
| extra5 = Sneha Khanwalkar, Amitabh Bhattacharya
| length5 = 5:26
| title6 = Wakda
| extra6 = Amit Trivedi
| length6 = 4:06
}}

=== Reception ===

The soundtrack has received positive reviews on release. Music Alouds review rated it 8/10.  Musicperk.com rated the album 8/10 quoting "Aga Bhai, Mahek Bhi, What To Do and Dreamum Wakeupum are the picks of the album".  Shresht Poddar, of Score Magazine, gave the album 3 out of 5 stars saying,"Melody-wise, the album is just above average. Innovation-wise, it scores full marks from me. Amit Trivedi dares to be experimental when his contemporaries are staying safe by sticking to tried-and-tested methods.".,  while Suhail mir of Gomolo, gave the soundtrack 4 out of 5 stars mentioning, "Aiyyaa is a truimphant album all the way. Dares to be quality entertainer with experimental tracks that are promising. Amit trivedi shows his real – class with Aiyyaa, a mix of styles, tastes and generes. An intelligently packaged complication." 
Rumnique Nannar said "Aiyyaa is one of the most spirited and hilarious albums in such a long time that lives up to its trailer and wacky style. Each of the songs has something to celebrate and enjoy, and it is often so rare to hear an album so joyous and downright fun." and gave 4 on 5. 

== Commercial reception ==
Both Aga Bai and Dreamum Wakupum became super hits and chartbusters, both getting over 1 million views on YouTube in less than 1 week of being released. Both charted in the top 5 of the Indias Airplay Top 100 and have been promoted strongly on TV and Radio broadcasts.

== Critical reception ==
{|class="wikitable infobox plain" style="float:right; width:20em; font-size: 80%; text-align: centr; margin:0.5em 0 0.5em 1em; padding:0;" cellpadding="0" cellspacing="0"
! colspan="2" style="font-size:120%;" | Professional reviews
|-
! colspan="2" style="background:#d1dbdf; font-size:120%;" | Review Scores
|-
! Source
! Rating
|-
| Koimoi
| 
|- DNA India
| 
|-
|The Times of India
| 
|- YAHOO
| 
|- India Today
| 
|-
| Hindustan Times
| 
|- Bollywood Hungama
| 
|-
| Kerala Films
| 
|- Rediff
| 
|- IMDb
| 
|- India Masala
| 
|}

The film was released worldwide on 12 October 2012. The film garnered mixed to negative reviews. While the performance by Rani was praised and admired by the critics, the inadequacy of the plot led to its disappointing outcome. Bravos, a movie review aggregator website specifically for Indian movies, assigned the film an average score of 38 (out of 100) based on 7 reviews from mainstream critics, which is considered to be an average score. Madhureeta Mukherjee of Times of India gave it 2.5 stars. "Even with such a talented ensemble, this one turns into a cultural showpiece, and gets lost in translation." said ToI.  "Aiyyaa is let down by its weak script" writes Prasanna D Zore of rediff.  Roshni Devi of Koimoi gave it 3 stars. " Watch Aiyyaa for a quirkily different film with very good performances but be warned that it drags." wrote Roshni Devi.  Social Movie Rating site MOZVO gave it 2.9 putting Aiyyaa in Average category.  Taran Adarsh of Bollywood Hungama gave it 3 stars.  Kanika Sikka of DNA gave it 2.5 stars. "Aiyyaa is an average entertainer" said DNA. 
Kerala Films gave the movie 2 stars and added "Aiyya is let down by a confused script.".  Anupama Chopra of Hindustan Times rated Aiyya 2 Stars and added "Whackiness cant carry a film.".  Reviewers on IMDb gave Aiyya an overall all mark of 4.1 out of 10.  Shilpa Jamkhandikar of India Masala rated Aiyaa 3 stars praising the cast and the stories, however saying What it doesnt have is something that binds all of this together. Kundalkar makes a bizarre mash-up of several genres and ends up with a film that doesnt do too much justice to any one of them.  Raghavendra Singh of FilmFare praised the movie and says It takes courage to present something never-done-before on the larger-than-life canvas of the big screen. And surprisingly debut director (at least in Hindi films) Sachin Kundalkar shows this trait with great effect in his film Aiyyaa. Hats off to an established star, Rani Mukerji, for showing such conviction in Kundalkars experimental vision.. 

== Box office ==

=== Overseas ===
Aiyyaa was released overseas in a very limited amount of theatres (30 in the United Kingdom) and as a result did poorly in overseas markets with its opening collecting around $125 500 in overseas, however averaging $4,000 in each theatre over its opening weekend. However, it dropped 90% the following weekend.  Subsequently it was declared a Flop by Box Office India for its overseas performance.

=== India ===
Aiyya had many high expectations at the box office from both filmmakers and box office predictors due to high advertisement and raving reviews from the trailers and promos alike. They were expecting 80%–100% occupancy throughout the country on its opening day and would moved onto collecting 15–200&nbsp;million on the first weekend. However in fact, Aiyyaa opened to 20–25% occupancy and gained a disappointing 30&nbsp;million. It did better in the maharashtra ranging from 40 to 50% in the region. On its second day, Aiyyaa went on to collect 35.0&nbsp;million and on Sunday it collected 32.0&nbsp;million.

In the middle of eight competing films, Aiyyaa was the only one which came out with a respectable number, albeit very low. With Marathi flavour the film was helped of content as film remained poor in other parts.

Total Collections is 160.2&nbsp;million

In total the film got very low collections, nevertheless, became an average success at box office of India.

The under-performance of the film was perceived due to the limited overseas release, criticism of the length of the film by audiences, lack of high-profile actors/actresess (except Rani Mukherji), lackluster reviews and only song promos to promote the entire movie.

== References ==
 

== External links ==
*  
*  
* 

 

 
 
 
 