Red Shadow (film)
 
{{Infobox film
| name = Red Shadow
| image = Redshadow.jpg
| director = Hiroyuki Nakano
| producer =
| writer = Story: Mitsuteru Yokoyama Screenplay: Hiroshi Saito Masatoshi Kimura
| starring = Masanobu Ando Megumi Okina Kumiko Aso Jun Murakami Naoto Takenaka
| music = Toshiyuki Kishi
| cinematography = Hideo Yamamoto
| editing =
| distributor = Toei Company
| released =  
| runtime = 108 min. Japanese
| budget =
| country = Japan
}}

  is a 2001 Japanese action comedy-drama film by directed by Hiroyuki Nakano. It is a feature film remake of the 1967 TV series  , created by Mitsuteru Yokoyama. The film stars Masanobu Ando in the title role and features a guest star appearance of Tomoyasu Hotei returning from the 1998 spin-off and virtual prequel Samurai Fiction.

== Cast ==
*Masanobu Ando - Akakage
*Megumi Okina - Koto-Hime
*Kumiko Aso - Asuka
*Jun Murakami - Aokage
*Naoto Takenaka - Shirokage
*Fumiya Fujii - Ranmaru
*Shuhei Mainoumi - Rikimaru
*Kei Tani - Roushi
*Ryoko Shinohara - O-Rin
*Kitarō - Roushi, Mura no chourou
*Denden - Bonnobei
*Masahiko Tsugawa - Tougou Hidenobu, Sengoku Daimyou
*Yutaka Matsushige - Kamijou Takatora

== External links ==
* 

 
 
 
 
 
 
 
 


 
 