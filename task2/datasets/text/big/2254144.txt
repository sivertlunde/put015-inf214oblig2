The Palm Beach Story
{{Infobox film
| name           = The Palm Beach Story
| image          = The Palm Beach Story postr.jpg
| image_size     = 225px
| caption        = theatrical release poster
| director       = Preston Sturges Paul Jones (assoc. producer)
| writer         = Preston Sturges
| starring       = Claudette Colbert Joel McCrea Mary Astor Rudy Vallee
| music          = Victor Young
| cinematography = Victor Milner
| editing        = Stuart Gilmore
| distributor    = Paramount Pictures
| released       = November 2, 1942  (NYC)  November 7  (US general) 
| runtime        = 88 minutes
| language       = English
| country        = United States
| budget         = $950,000 (approx) 
| gross          =
}}
 romantic screwball comedy film written and directed by Preston Sturges, and starring Claudette Colbert, Joel McCrea, Mary Astor and Rudy Vallée. Victor Young contributed the lively musical score, including a fast-paced variation of the William Tell Overture for the opening scenes. Typical for a Sturges movie, the pacing and dialogue of The Palm Beach Story are very fast.

==Plot==
Tom and Gerry Jeffers (Joel McCrea and Claudette Colbert) are a married couple in New York City who are down on their luck financially, which is pushing the marriage to an end.  But there is another, deeper problem with their relationship, one that is hinted at in the prologue of the movie as the opening credits roll and then explained near the movies end.

In the prologue Claudette Colbert appears bound and gagged in a closet, but then a second later in a wedding dress, seen by a maid who faints at every disturbance. The movie reveals much later that Colbert is playing identical twins, both of whom are in love with the intended groom played by Joel McCrea. The sister of the bride has just tied her up in an attempt to steal the wedding for herself. The pantomime is cross-cut with action showing McCrea hurriedly changing from one formal suit to another in the car as he rushes to the church. McCrea also is playing twins and the sibling is likewise in love with the tied up sister. He too is trying to steal the wedding. The end result is that the two siblings, not the original bride or groom, are married, and those two were not in love with each other.
 Robert Dudley), a strange but rich little man who is thinking of renting the Jeffers apartment; and boards a train for Palm Beach, Florida.  There she plans to get a divorce and meet a wealthy second husband who can help Tom. On the train, she meets the eccentric John D. Hackensacker III (Rudy Vallée), one of the richest men in the world.  

  and Claudette Colbert, stars of The Palm Beach Story, from the trailer for the film]] 
Because of an encounter with the wild and drunken millionaire members of the Ale and Quail hunting club, Gerry loses all her luggage; after making do with clothing scrounged from other passengers, she is forced to accept Hackensackers extravagant charity. They leave the train and go on a shopping spree for everything from lingerie to jewelry &ndash; Hackensacker minutely noting the cost of everything in a little notebook, which he never bothers to add up &ndash; and make the remainder of the trip to Palm Beach on Hackensackers yacht named The Erl King (a Sturges joke on the Hackensacker family business, oil).

Tom follows Gerry to Palm Beach by air, also with the impromptu financial assistance of the Wienie King. When Tom meets Hackensacker, Gerry introduces him as her brother, Captain McGlue. Soon, Hackensacker falls for Gerry, while his often-married, man-hungry sister, Princess Centimillia (Mary Astor), chases Tom, although her last lover, Toto (Sig Arno), is still following her around.  To help further his suit with Gerry, Hackensacker agrees to invest in Toms scheme to build an airport suspended over a city by wires.

Tom finally persuades Gerry to give their marriage another chance, and they confess their masquerade to their disappointed suitors. Even though he is disappointed, Hackensacker intends to go through with his investment in the suspended airport, since he thinks it is a good business deal and he never lets anything get in the way of business. Then, when Tom and Gerry reveal that they met because they are both identical twins &ndash; a fact which explains the opening sequence of the film &ndash; Hackensacker and his sister are elated. The final scene shows Hackensacker and Gerrys sister, and the Princess and Toms brother, getting married.

The film ends where it began after the prologue, with the words "And they lived happily ever after...or did they?" on title cards.

== Cast ==
  and Joel McCrea]]
  and Claudette Colbert]]
  
*Claudette Colbert as Geraldine "Gerry" Jeffers
*Joel McCrea as Tom Jeffers (alias "Captain McGlue")
*Mary Astor as The Princess Centimillia (Maud)
*Rudy Vallée as John D. Hackensacker III
*Sig Arno as Toto Robert Dudley as Wienie King
*Esther Howard as Wife of Wienie King
*Franklin Pangborn as Apartment Manager
*Arthur Hoyt as Pullman Conductor
*Al Bridge as Conductor Fred "Snowflake" Toones as George, Club Car Bartender &nbsp;&nbsp;&nbsp;&nbsp;
*Charles R. Moore as Train Porter
*Frank Moran as Brakeman
*Harry Rosenthal as Orchestra Leader
*J. Farrell MacDonald as Officer ODonnell
 
The Ale and Quail Club:
*Robert Warwick as Mr. Hinch
*Arthur Stuart Hull as Mr. Osmond
*Torben Meyer as Dr. Kluck
*Victor Potel as Mr. McKeewie
*Jimmy Conlin as Mr. Asweld
*Unnamed members played by:
**William Demarest
**Jack Norton Robert Greig
**Roscoe Ates
**Dewey Robinson
**Chester Conklin
**Sheldon Jett
 

===Cast notes=== The Great Imitation of Life, The Palm Beach Story was the only time they worked together on a movie Sturges wrote and directed. Unfaithfully Yours and The Beautiful Blonde from Bashful Bend.
*Many members of Sturges unofficial "stock company" of character actors appear in The Palm Beach Story, among them Al Bridge, Chester Conklin, Jimmy Conlin, William Demarest, Robert Dudley, Byron Foulger, Robert Greig, Harry Hayden, Arthur Hoyt, Torben Meyer, Frank Moran, Charles R. Moore, Jack Norton, Franklin Pangborn, Victor Potel, Dewey Robinson, Harry Rosenthal, Julius Tannen and Robert Warwick.
*This was the seventh of ten films written by Preston Sturges in which William Demarest appeared. 

==Production==
At least part of the initial inspiration for The Palm Beach Story may have come to Preston Sturges from close to home, since his ex-wife, Eleanor Hutton, was an heiress who moved among the European aristocracy, and was once wooed by Prince Jerome Rospigliosi-Gioeni, among others, and Sturges himself had shuttled back and forth between Europe and America as a young man.  Indeed one incident in the film is based on something which happened to Sturges and his mother while traveling by train to Paris, when the car with their compartment was uncoupled while they ate dinner two cars away. Stafford, Jeff     
 Paramount submitted to them because of its "sex suggestive situations...and dialogue."  Changes were made, but the Hays Office continued to reject the script because of its "light treatment of marriage and divorce" and because of similarities between the John D. Hackensacker III character and John D. Rockefeller. More changes were made, including reducing the number of Princess Centimillias previous marriages from eight to three (plus two annulments), before the script finally was approved. TCM   

Claudette Colbert received $150,000 for her role, and Joel McCrea was paid $60,000. 
 Penn Station in Manhattan.  The film premiered in New York City on 2 November 1942 and went into general release on 7 November.   The film was released on video in the U.S. on 12 July 1990 and re-released on 30 June 1993. 

==Awards and honors==
American Film Institute recognition
* 2000: AFIs 100 Years... 100 Laughs #77

==References==
Notes
 

==External links==
 
 
*  
*  
*  
*   Archival NYTimes Review By Bosley Crowther, December 11, 1942
*   Critique and thorough plot description/analysis.
*   at metroactive
*   at Portico
*  on Screen Guild Theater: March 15, 1943

 

 
 
 
 
 
 
 
 
 
 
 