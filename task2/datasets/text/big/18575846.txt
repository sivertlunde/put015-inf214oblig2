13 Hours by Air
{{Infobox film
|name=13 Hours by Air
|image=13 HOURS BY AIR.jpg
| image_size     = 200px
| caption        = Theatrical poster
|director=Mitchell Leisen
|producer=E. Lloyd Sheldon
| based on       =  
|writer=Kenyon Nicholson Bogart Rogers
|starring=Fred MacMurray Joan Bennett
|music=Heinz Roemheld (composer) Irvin Talbot (conductor)
|cinematography=Theodor Sparkuhl
|editing=Doane Harrison
|country=United States
|language=English
|studio=Paramount Pictures
|distributor=Paramount Pictures
|runtime=77 min.
|released= 
}}
13 Hours by Air (aka 20 Hours by Air) is a 1936 drama film made by Paramount Pictures and directed by Mitchell Leisen. The film stars Fred MacMurray and Joan Bennett. The screenplay was written by Kenyon Nicholson and Bogart Rogers, based on story Wild Wings by Bogart Rogers and Frank Mitchell Dazey. 13 Hours by Air was also the forerunner of the disaster film, a genre featuring a complex, heavily character-driven ensemble cast film, exploring the personal dramas and interactions that develop among the passengers and crew as they deal with a deadly onboard emergency.

==Plot== Alan Baxter, both of whom, seem to be harboring a secret.
 Alan Baxter), his co-pilot are persuaded to fly on but are eventually forced to make an emergency landing. Dr. Evarts tells Jack he is a federal agent pursuing Palmer, a notorious criminal, who now takes the opportunity to shoot Freddie and Dr. Everts, commandeering the aircraft. Jack manages to overcome Palmer, and with the help of Felice, is able to take off and fly to San Francisco. When the flight lands, he is able to have his dinner with Felice, collecting his bet, knowing that he will need the money for a marriage licence. 

==Cast==
   
* Fred MacMurray as  Jack Gordon
* Joan Bennett as  Felice Rollins
* Zasu Pitts as  Miss Harkins John Howard as Freddie Scott
* Benny Bartlett as Waldemar Pitt III (as Bennie Bartlett)
* Grace Bradley as Trixie La Brey  Alan Baxter as  Curtis Palmer
* Brian Donlevy as  Dr. Evarts
* Ruth Donnelly as  Vi Johnson
* Fred Keating as  Count Gregore Stephani
 
* Adrienne Marden as Ann McKenna
* Dean Jagger as Hap Waller
* Mildred Stone as Ruth Bradford
* Jack Mulhall as Horace Lander
* Clyde Dilson as Fat Richhauser
* Dennis OKeefe as Baker (as Bud Flannagan)
* Granville Bates as Pop Andrews
* Bruce Warren as Tex Doyle
* Marie Prevost as Waitress in Omaha 
 

==Production==
  Alhambra Airport, California, in Cleveland, Ohio, and Beaver Dam, Wisconsin, using United Air Lines Boeing 247 airliners. Second unit filming involved a flight from Newark to Los Angeles to obtain actual footage to be used in the film. An aircraft assigned to the production was also involved in a minor accident.   

The pairing of MacMurray and Bennett brought together two dependable leads who worked as Paramount Studios contract players. They were sometimes loaned out to other concerns, but steadily climbed up from programmers such as 13 Hours by Air to more prestigious fare. 

==Reception==
Film reviewer Frank S. Nugent, in his review for The New York Times, called 13 Hours by Air "pleasant". "... there is no disputing the liveliness of the melodrama. The device of tossing a miscellany of humans and motives together on a bus, plane, train or airliner and letting them work out their destiny is as formular as the Bartenders Guide and has been used as often, but Bogart Rogerss and Frank Mitchell Dazeys story has been screened with a shrewd sense of pace, with a purposeful preservation of suspense and a knack for comic interlude." 

13 Hours by Air is one of over 700 Paramount Productions, filmed between 1929 and 1949, which were sold to Universal Studios|MCA/Universal in 1958. The Paramount catalogue was destined for television distribution, and have been owned and controlled by Universal ever since.

==References==
===Notes===
 
===Bibliography===
 
* Maltin, Leonard.  Leonard Maltins Movie Encyclopedia. New York: Dutton, 1994. ISBN 0-525-93635-1.
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 