Mommo the Bogeyman
{{Infobox film
| name           = Mommo the Bogeyman 
| image          = BogeymanFilmPoster.jpg
| alt            = 
| caption        = Theatrical poster
| director       = Atalay Taşdiken
| producer       = Atalay Taşdiken
| writer         = Atalay Taşdiken
| starring       = Elif Bülbül   Mehmet Bülbül   Mete Dönmezer
| music          = Erkan Oğur
| cinematography = Ali Özel
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = 
}}
Mommo the Bogeyman ( ) is a 2009 Turkish drama film, written, produced and directed by Atalay Taşdiken, about two young siblings who find refuge with their grandfather after their mother dies and their father’s new wife rejects them. The film, which went on nationwide general release across Turkey on  , won the Audience Award at the Würzburg International Filmweekend and was nominated in several categories at the 3rd Yeşilçam Awards.

==Production==
The film was shot on location in Konya, Turkey.   

== Plot ==
Two young siblings, Ahmet and Ayse, find refuge with their grandfather after their mother dies and their father’s new wife rejects them. The old man however is not able to care for them and finally has to take the children apart. This simple, poignant tale of a village, and the relationship between a brother and sister, portrays very gritty living circumstances in rural Anatolia.

== Reception ==

=== Awards ===
* Würzburg International Filmweekend - Audience Award (Won)

== See also ==
* 2009 in film
* Turkish films of 2009

==External links==
*   for the film
*  
*  

==References==
 

 
 
 
 

 