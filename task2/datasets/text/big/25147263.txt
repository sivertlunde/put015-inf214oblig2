The Ghosts of Christmas Eve
{{Infobox film
| name           = The Ghosts of Christmas Eve
| image          =
| director       = Hart Perry Paul ONeill, Taro Meyer, Michael Owen Paul ONeill 
| starring       = Paul ONeill, Robert Kinkel, Jon Oliva
| cinematography =
| editing        = Fox Family
| released       =
| runtime        = 46 minutes   
}}

The Ghosts of Christmas Eve is a 1999 Television movie|made-for-TV movie showcasing a Christmas music performance by Trans-Siberian Orchestra, starring Ossie Davis and Allie Sheridan with guest performers Michael Crawford and Jewel (singer)|Jewel.    Other performers include Bob Kinkel, Al Pitrelli, Chris Caffery, Johnny Lee Middleton, Jeff Plate, Tony Gaynor, Daryl Pediford and Tommy Farese. 

==Plot==
The songs are presented in such a way as to form a storyline about a runaway who takes refuge in an abandoned theatre on Christmas Eve,    and experiences the musical performances as ghostly visions from the theatres past.   in Jersey City, New Jersey.
 Fox Family on December 14, 1999 as part of their 25 Days of Christmas programming block. On November 13, 2001, it was released on DVD  with the Timeless Version of Christmas Eve/Sarajevo 12/24 included as a bonus track.

The lost child, Allie Sheridan, would go on to perform with TSO during their 2003 tour.

==List of Songs==
# O Come All Ye Faithful / O Holy Night
# Good King Joy
# Hark! The Herald Angels Sing (featuring Jewel)
# Christmas Eve/Sarajevo 12/24
# Christmas Canon
# O Holy Night (featuring Michael Crawford) 
# Music Box Blues
# Promises To Keep
# This Christmas Day
# First Snow

==Reception==
The New York Times reviewed said, "the Trans-Siberian Orchestra has carried out a mission to create powerful artistry in music through the webbing of songs together by means of storytelling...artists like Jewel and Michael Crawford bring their own unique twists to a musical exploration of art and the Christmas spirit." 

==References==
 

==External links==
*  

 
 

 
 
 
 
 