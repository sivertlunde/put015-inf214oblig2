School Wars: Hero
{{Infobox film
| name           = School Wars: Hero
| image          =
| caption        =
| imdb_rating    =
| director       = Ikuo Sekimoto
| producer       =Kazunari Hashiguchi
| writer         =Yoshiharu Yamaguchi (novel) Toshimichi Saeki Ritsu Yamada (screenplay)
| starring       = Shōei, Emi Wakui, Sayaka Kanda, Kōtarō Satomi, Tomohisa Yuge, Asahi Uchida, Katsuya Kobayashi
| music          =Yûsuke Honma
| cinematography =Masami Inomoto
| editing        =Yoshiyuki Okuhara
| distributor    =
| released       = 18 September 2004
| runtime        = 118 min. Japanese
| budget         =
| preceded_by    =
| followed_by    =
| mpaa_rating    =
| tv_rating      =
}}
 Japanese film director Ikuo Sekimoto.
 high school in Kyoto, giving special attention to the rugby team, which included the most disruptive students among its members.

==Plot== Japanese rugby union team in the 1960s.  The principal of Fushimi Daiichi Kogyo High School (Kōtarō Satomi), which suffers from severe discipline problems, persuades him to join his staff, in the hope that he has the strength to turn the school around.

At first, the students shout and physically attack him and the other teachers, and he becomes very depressed.  But after receiving encouragement from his wife (Emi Wakui), he refuses to give up on the students, and re-doubles his efforts, winning over previously skeptical staff members.  The state of the school gradually improves.

The principal appoints him the coach of the rugby team.  He tries hard to persuade delinquent but strong students to join the team, first winning over Arai (Tomohisa Yuge), and later Shingo (Katsuya Kobayashi), known as the worst trouble maker in Kyoto.

The rugby team lose their first match 112-0, but this experience motivates them to commit themselves fully to the team, and doing whatever it takes to become the best team.  After intense training, during which the team members bond and quit their gangs, they win the 1976 Kyoto Prefecture rugby tournament.

==See also==
* Rugby union in Japan

==External links==
*   in Japanese
*   (in Japanese)
* 
 
 
 
 
 


 