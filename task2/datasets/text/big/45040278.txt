Kranthiyogi Basavanna
{{Infobox film 
| name           = Kranthiyogi Basavanna
| image          =  
| caption        = 
| director       = K. S. L. Swamy (Ravee)
| producer       = 
| writer         = Jagadguru Mathe Mahadevi
| screenplay     = Jagadguru Mathe Mahadevi Ashok Aarathi Manjula
| music          = M. Ranga Rao
| cinematography = Purushottham
| editing        = Yadav Victor
| studio         = Vishwa Kalyana Movies
| distributor    = Vishwa Kalyana Movies
| released       =  
| country        = India Kannada
}}
 1983 Cinema Indian Kannada Kannada film, Manjula in lead roles. The film had musical score by M. Ranga Rao.  

==Cast==
  Ashok
*Aarathi
*Srinivasa Murthy Manjula
*Prabhakar
*Vajramuni
*Sudheer
*Hema Choudhary
*Shashikala
*Vasudeva Rao
*Baby Rekha
*Chandrashekar
*Shivaram
*Sadashiva
*Shakthi Prasad
*Seetharam
*Saikumar
*K. B. S. Achar
*N. N. Vaman
*B. Jayashree
*Kaminidharan
*Doddanna
*Mamatha
*Thimmayya
*Bhatti Mahadevappa
*H. T. Urs
*Master Nagesh
*Master Mahesh
*Bharathi in Guest Appearance
*Rajesh in Guest Appearance
*Manju Bhargavi in Guest Appearance
*Musuri Krishnamurthy in Guest Appearance
 

==Soundtrack==
The music was composed by M. Ranga Rao. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Janaki || Maate Mahadevi || 04.50
|- Janaki || Maate Mahadevi || 04.28
|-
| 3 || Ella Ballidanaiah || S. P. Balasubrahmanyam || Maate Mahadevi || 04.15
|-
| 4 || Odhagite Namagintha Bhagya || PB. Srinivas, Vani Jayaram || Maate Mahadevi || 04.37
|-
| 5 || Neera Kandalli || S. P. Balasubrahmanyam || Maate Mahadevi || 01.02
|-
| 6 || Kalalli Kattida Gundu || S. P. Balasubrahmanyam || Maate Mahadevi || 01.27
|-
| 7 || Jagadagala Mugilagala || S. P. Balasubrahmanyam || Maate Mahadevi || 01.21
|-
| 8 || Vachanadalli Namamruta || S. P. Balasubrahmanyam || Maate Mahadevi || 01.24
|-
| 9 || Ullavaru Shivalayava || S. P. Balasubrahmanyam || Maate Mahadevi || 01.53
|-
| 10 || Ba Yemballi Yenna || Vani Jayaram || Maate Mahadevi || 01.18
|-
| 11 || Suryanudaya Tavarege || Vani Jayaram || Maate Mahadevi || 01.21
|-
| 12 || Kalabeda Kolabeda || S. P. Balasubrahmanyam || Maate Mahadevi || 01.43
|-
| 13 || Bhakthi Yemba Pruthvi || S. P. Balasubrahmanyam || Maate Mahadevi || 01.37
|}

==References==
 

==External links==
*  

 

 
 
 
 


 