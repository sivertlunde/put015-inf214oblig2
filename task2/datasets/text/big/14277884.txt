Luke, the Gladiator
{{Infobox film
| name           = Luke, the Gladiator
| image          =
| image size     =
| caption        =
| director       = Hal Roach
| producer       = Hal Roach
| writer         =
| narrator       =
| starring       = Harold Lloyd
| music          =
| cinematography =
| editing        =
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        =
| country        = United States Silent English intertitles
| budget         =
| preceded by    =
| followed by    =
}}
 1916 short short comedy film starring Harold Lloyd.   

==Cast==
* Harold Lloyd - Lonesome Lukius, Gladiator
* Bebe Daniels
* Snub Pollard Charles Stevenson - (as Charles E. Stevenson)
* Billy Fay
* Fred C. Newmeyer
* Sammy Brooks
* Harry Todd
* Bud Jamison
* Margaret Joslin - (as Mrs. Harry Todd)
* Earl Mohan
* Haika Carle
* Capitola Holmes
* Gene Hershaw
* H. Saunders
* J. Stevenson
* J. Jenks
* C.H. Butter
* Thelma Daniels
* Beatrice Peskett
* Esther Bennett
* Aleen Ware
* Robert Wooley
* Minna Browne

==See also==
* Harold Lloyd filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 