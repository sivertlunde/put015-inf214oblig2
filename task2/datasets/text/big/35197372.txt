Hit and Run (2012 film)
 
{{Infobox film
| name           = Hit and Run
| image          = Hit and Run Poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = David Palmer Dax Shepard
| producer       = Andrew Panay Nate Tuck Kim Waltrip Jim Casey Dax Shepard
| screenplay     = Dax Shepard Tom Arnold Bradley Cooper
| music          = Robert Mervak Julian Wass
| cinematography = Bradley Stonesifer
| editing        = Keith Croket Dax Shepard
| studio         = Primate Pictures Kim and Jim Productions
| distributor    = Open Road Films 
| released       =  
| runtime        = 100 minutes  
| country        = United States
| language       = English
| budget         = $2 million 
| gross          = $14.5 million 
}} Tom Arnold, and Bradley Cooper. It was released on August 22, 2012.

==Plot== Witness Protection Tom Arnold).  Charlies girlfriend Annie Bean (Kristen Bell) is a professor at Milton Valley College and has a doctorate in Non-Violent Conflict Resolution from Stanford University, a major she created herself.  Annies supervisor Debbie Kreeger (Kristin Chenoweth) calls Annie in for a meeting, where she tells her that the University of California is starting a Conflict Resolution program and is interested in interviewing her.  The interview is scheduled for Wednesday in Los Angeles at 4:00; Annie balks at the idea saying she needs to talk to her boyfriend about it first, until Debbie tells her to live for herself instead of boyfriends, and that she will be fired if she does not make it to the interview.

A perplexed Annie returns home and tells Charlie of the job interview, upsetting him since Los Angeles is the area he lived in prior to enrolling in Witness Protection and cant return to.  Charlie insists Annie interview for the job for her own sake, even though he would be unable to follow her, but Annie instead returns to the college the next day to beg to keep her job.  While she is gone, Charlie decides he would return to L.A. after all, and picks up Annie in his souped-up, restored Lincoln Continental, promising to take her to her interview.
 getaway driver who testified in an ultimately unsuccessful bank robbery case against his accomplices, one of whom shot the bank guard.  Gil finds the Facebook page of one of the defendants, Alexander Dmitri (Bradley Cooper), and leaves a message saying he knows where Yul Perkins is for the next 24 hours.

Meanwhile, Randy calls Charlie after discovering he is not home.  Charlie tells him he is returning to L.A., and Randy insists on accompanying him per Marshals Service policy, leaving Milton in order to pursue Charlie.  A short time later Charlie and Annie discover Gil following them in his vehicle. Charlie pulls over, intending to beat up Gil, but instead tries to non-violently resolve the situation at Annies insistence.  Gil is unmoved, and reveals that he both knows Charlies real name and has Alex Dmitri as a "Facebook friend".  Charlie and Annie then flee from Gil in the Continental, in the process running Randy off the road as he arrives, but ultimately losing Gil.  Elsewhere, Alex sees Gils Facebook message, gathers his fellow bank robbers Neve (Joy Bryant) and Alan (Ryan Hansen) and heads to meet Gil.

Annie and Charlie gas up the Continental, where the vehicles engine is admired by a redneck named Sanders (David Koechner).  The two then make their way to a motel, where they are unknowingly followed by Sanders.  In the morning, Charlie tries to start the vehicle, only to discover that the engine has been stolen in the night.  Gil arrives shortly after, ambushing Charlie with a golf club, but Charlie distracts him and knocks him out, placing him in his vehicle.  He quickly discovers that Gil was also accompanied by Alexs crew, who are at the front desk.  Charlie grabs the VIN Number of a Corvette in the parking lot, makes a duplicate keyless entry for the vehicle using the former tools of his trade, and then leaves with Annie, Gil and Alexs crew in hot pursuit, with Randy joining the chase.  During the chase Annie and Charlie argue over his past, where he reveals that he was a getaway driver who participated in 13 bank robberies, and that Neve was once his fiancee.  The two ultimately escape their pursuers again.

Afterward, Annie demands Charlie pull over, where she confronts him for lying to her about his past.  She decides to proceed to L.A. without Charlie; Gil arrives shortly after, and agrees to take Annie the rest of the way.  A short time later they are run off the road by Alex, who takes Annie hostage and calls Charlie, telling him to meet at a nearby diner.  Charlie arrives and Alex demands money in exchange for Annie, then argues about Charlies betrayal, cut short when Alex reveals that he was raped in jail and blames Charlie for it.  Charlie agrees to take him to a hidden stash of bank robbery money located at the home of his estranged father Clint (Beau Bridges).  While in transit he surreptitiously places a call to Randy, now in the company of Terry (whos attracted to Randy) and his partner Angela Roth (Carly Hatter), and gives Randy his fathers address.  The three pick up Gil along the way.

At Clints house, Charlie digs up a bag of money he hid in a pasture with his father, at the same time reconciling with him.  His father carefully mentions he owns a Class 1 Off-Road racing vehicle; shortly after he knocks Alex down with a shovel, then fights with Alan as Charlie and Annie make their escape.  The two get in the racer and flee just as Gil, Randy, Terry and Angela arrive.  Alex and Neve attempt to follow, but Randy manages to shoot Alex as the latter fires at Charlie, forcing them to stop and placing the two under arrest.  Two Marshals (Jason Bateman and Nate Tuck) later arrive and take Alex and his crew into custody, complimenting Randy and Terry on their work.

After their escape, Charlie tells Annie he is committed to getting her to the interview still, wanting to keep his word despite the fact that she no longer loves him.  Annie responds that she still loves him, and the two reconcile before continuing the trip.  Charlie makes it to the University of California campus in time for Annie to make her interview.  Before she leaves, Charlie offers to spend the rest of his life with her, which Annie accepts.  The final scene cuts some months in the future, showing Randy and Terry, now in a relationship, giving each other a brief pep talk before heading to take the Marshals exam.
 Sean Hayes), interrupting him as he is smoking from a bong.  After a rough start due to Ostermans embarrassment at hotboxing his office and confusion at him not being a woman as Debbie had described, Osterman reveals that Debbie is his sister and she has jokingly called him a girl since he was 9.  Annie expresses sympathy for how this must make Sandy feel, earning his approval and an immediate job offer, which she accepts.

==Cast==
* Dax Shepard as Charlie Bronson/Yul Perkins
* Kristen Bell as Annie Bean
* Kristin Chenoweth as Debby Kreeger Tom Arnold as U.S. Marshal Randy Anderson 
* Bradley Cooper as Alex Dimitri
*Jess Rowland as Terry
* Ryan Hansen as Alan
* Beau Bridges as Clint Perkins
* Michael Rosenbaum as Gil Rathbinn
* Steve Agee as Dude #1
* David Koechner as Sanders
* Joy Bryant as Neve Tatum Sean Hayes as Sandy Osterman
* Jason Bateman as U.S. Marshal Keith Yert

==Production==
On December 21, 2011, Open Road Films picked up the U.S distribution rights.    Open Road changed the name of the film from Outrun to Hit and Run. 

==Filming locations==
One of the chase scenes was filmed at the former MCAS Tustin base in Tustin, California, with the distinctive blimp hangers prominently visible.

This film was also filmed in Fillmore, California and Piru, California.

==Release== trailer was released on May 16, 2012. 

==Reception==
Hit and Run received mixed reviews from critics. Rotten Tomatoes gives it a score of 49%, based on 129 reviews, with an average rating rating of 5.2/10. The sites consensus states, "Though Hit & Run has some surprisingly oft-kilter filmmaking, the action doesnt add to much and the writings a bit smug."  Metacritic gives the film a weighted average score of 50 out of 100, based on 31 critics, indicating "mixed or average reviews". 

Roger Ebert gave it 3.5 of 4 stars, writing, "With its off-the-shelf title, I had worked up less than a white-hot enthusiasm to see "Hit & Run," but its a lot more fun than the title suggests. How many chase comedies have you seen where the heros sexy girlfriend has a doctorate in nonviolent conflict resolution? Her counseling would have been invaluable to the U.S. marshal (Tom Arnold) in an early scene where he attempts to shoot his own van." 
Film critic Richard Roeper gave the film an "F", calling it an unfunny comedy movie from start to finish. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 