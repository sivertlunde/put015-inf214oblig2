Bollywood Queen
 
{{Infobox film
| name = Bollywood Queen
| director = Jeremy Wooding
| image	= Bollywood Queen FilmPoster.jpeg
| producer = Jeremy Wooding
| written by = William Shakespeare   Neil Spencer   Jeremy Wooding
| starring = Preeya Kalidas   James McAvoy   Ray Panthaki   Ciarán McMenamin   Amerjit Deu   Saraj Chaudhry   Ronny Jhutti
| music = Steve Beresford
| cinematography = Jono Smith
| editing = Ben Yeates
| studio = Win Media
| distributor =
| released =  
| country = United Kingdom
| language = English Hindi
| length = 89 minutes
| gross =
}}
Bollywood Queen is a British Indian take on the William Shakespeare play Romeo and Juliet, directed by Jeremy Wooding and starring Preeya Kalidas and James McAvoy in the lead roles. Produced by Jeremy Wooding, the film was released in 2003.

==Plot== Gujarati family. She has finished school, and is at university doing a business studies course. She is dating Dilip (Ronny Jhutti), an ambitious computer whizz, but wants her love life to be like it is in the movies. However, Dilip only wants sex. Geenas family owns Ganesh Global, a clothing company which imports materials and saris from India. Geena has a part-time job working in the shop. She is also in a secret band with two other girls, Anjali (Kat Bhathena) and Neeta (Karen David).

Jay (James McAvoy) is a young Scottish boy, who joins his brother Dean (Ciarán McMenamin) in London, who also works in the clothing industry, for someone called Frank (Ian McShane). He owns a guitar, of which he is extremely protective.

One day, when Geena is walking along the road, Jay and his brother happen to be managing some poles. One of the poles nearly falls on Geena, but Jay lunges at her and pushes her out of the way before it can crush her. Geena, disgusted at this, hurries along, leaving her phone behind. Jay sees the phone and takes it, but his brother Dean repossesses it. Jay then takes the phone back and gives it to Geena, who then thanks him for saving her life. She then gives him her phone number, and they begin to meet up regularly.

The two start to fall in love, meeting up in secret regularly and hiding from Geenas family. However, Dean comes to know of the affair and launches an attack on Ganesh Global. One of Geenas brothers also sees the two together, and Geena gets into trouble with her family. Her brother then takes away her phone. Geenas brothers then beat Jay up, leaving him bleeding. Jay and Geena then run away together and escape their family. Dean is in hospital, and Jay visits him and takes away all his money.

Eventually, Jay and Geena return to London and Jay settles his dispute with his father and brother. Geena then turns up for her relatives wedding, dressed in Indian attire, and sings with her band. She then attempts to reunite with her family, and it is revealed her brother, Sanjay (Amerjit Deu) is a criminal, and is handling illegal suits, and that her brother, Tariq (Saraj Chaudhry) is gay. Jay and Geena then leave in the bride and grooms carriage, and her mother says to her father, "Shell be back."

==Cast==
* Preeya Kalidas as Geena
* James McAvoy as Jay
* Ray Panthaki as Anil
* Ciarán McMenamin as Dean
* Kat Bhathena as Anjali
* Ian McShane as Frank
* Amerjit Deu as Sanjay
* Karen David as Neeta
* Lalita Ahmed as Geenas mother
* Andy Beckwith as Johnny
* Jo Cameron Brown as Anita
* Saraj Chaudhry as Tariq
* Giada Dobrzenska as Club Kid
* Ronny Jhutti as Dilip
* Tajpal Rathore as Family Member
* Ranu Setna as Uncle Ganesh
* Faizaan Shurai as Factory Worker

==Reception==
The film was not very well received by the public, both English and Asian communities. It received mostly negative reviews from critics. 

==Awards and nominations==
British Independent Film Awards
*Jeremy Wooding- Douglas Hickox Award- Nominated

==External links==
* 

 

 
 
 
 
 
 
 