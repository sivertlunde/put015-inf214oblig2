I Love Hong Kong 2013
 
 
{{Infobox film
| name           = I Love Hong Kong 2013
| image          = ILoveHK2013.jpg
| alt            = 
| caption        = 
| film name = {{Film name| traditional    = 2013我愛HK 恭喜發財
| simplified     = 2013我爱HK 恭喜发财
| pinyin         = Èr Líng Yī Sān Wǒ Ài HK Gōng Xǐ Fā Cái
| jyutping       = Ji6 Ling4 Jat1 Saam1 Ngo5 Ngoi3 HK Gung1 Hei2 Faat3 Coi4}}
| director       = Chung Shu Kai
| producer       = Eric Tsang Peter Chik
| writer         = Peter Chik Kwok Kin Lok Yan Pak Wing Chiu Sin Hang
| starring       = Alan Tam Veronica Yip Natalis Chan Eric Tsang Stanley Fung Bosco Wong Michael Tse Kate Tsui Joyce Cheng
| music          = Tang Chi Wah Benedict Chong
| cinematography = Ko Chiu Lam
| editing        = Wenders Li Mok Man Ho Television Broadcasts Limited
| distributor    = Intercontinental Film Distributors (H.K.)
| released       =  
| runtime        = 110 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$16,894,784.
}}
 2013 Cinema Hong Kong comedy film and the third film of the I Love Hong Kong film series. Film stars Alan Tam, Veronica Yip, Natalis Chan and Eric Tsang, who also served as producer. This is also Veronica Yips first film role since 1996s Hong Kong Showgirls. The film was released on 7 February 2013 to celebrate Chinese New Year.

==Cast==

===Main cast===
{|class="wikitable" width="65%"
|- style="background:cornflowerblue; color:white" align=center
|style="width:25%"|Cast||style="width:25%"|Role||Description
|- 
| Alan Tam || rowspan="2"| Sung Chi Hung 宋池雄 || rowspan="2"| Tea house owner Mei Yeung Yeungs husband Ha Sek Sams good friend and love rival Helped by Angel
|-
| Bosco Wong  
|-
| Veronica Yip || rowspan="2"| Mei Yeung Yeung 美洋洋 || rowspan="2"| Mei Lung Kwats daughter Sung Chi Hungs wife Chu Yuk Yuens good friend
|-
| Kate Tsui  
|-
| Natalis Chan || rowspan="2"| Ha Sek Sam 夏石森 || rowspan="2"| Tim Sam Corporation Chairman Sung Chi Hungs good friend and love rival Pursues Mei Yeung Yeung Chu Yuk Yuens love interest
|-
| Michael Tse  
|-
| Eric Tsang || Angel 天使 || Second tier angel Promoted to first tier angel after helping Sung Chi Hung
|-
|}

===Other cast===
{|class="wikitable" width="65%"
|- style="background:cornflowerblue; color:white" align=center
|style="width:25%"|Cast||style="width:25%"|Role||Description
|- 
| Stanley Fung || 
|-
| Joyce Cheng || Chu Yuk Yuen 朱玉圓 || Mei Yeung Yeungs good friend Loves Ha Sek Sam
|-
| Wong Cho-lam || ||
|-
| Benz Hui || Mei Lung Kwat 美龍骨|| Mei Yeung Yeungs father
|- Anthony Chan || ||
|-
|  ) || ||
|-
|  ) || ||
|-
| Tin Kai-Man || || 70s tea house cook
|-
| Alfred Cheung || ||
|-
|  ) || || Tea house customer
|-
|  ) || So Pik 蘇碧 || Dim sum girl
|-
| Elvina Kong || ||
|-
| Evergreen Mak Cheung-ching || ||
|-
|  ) || || 70s tea house team leader
|-
| Jacqueline Chong || || 70s tea house team leader
|-
| Lai Lok-yi || || 70s tea house team leader
|-
|  ) || Caustic 哥士的 || 70s tea house electrician Restroom department manager
|-
|  ) || || 70s tea house second chef
|-
| Ngo Ka-nin || Fadan Wong 花旦王 || Cantonese opera fadan
|-
| Samantha Ko || Cho Cho 楚楚 || 70s porn star Has a son
|-
|  ) || || 70s tea house manager
|-
|  ) || || 70s tea house manager
|-
| Kayi Cheung || ||
|-
| Koni Lui || ||
|-
|  ) || ||
|-
|  ) || ||
|-
|  ) || ||
|-
|  ) || ||
|-
|  ) || ||
|-
| Siu Yam-yam || ||
|-
| Gill Mohindepaul Singh || ||
|-
|  ) || ||
|-
|  ) || ||
|-
|  ) || ||
|-
|  ) || ||
|-
|  ) || ||
|-
|  ) || ||
|-
|  ) || ||
|-
|  ) || ||
|-
| Terence Siufay || ||
|-
| Joe Junior || ||
|-
| Susan Tse || ||
|-
|  ) || ||
|-
| William Chak || ||
|-
|  ) || ||
|-
|  ) || ||
|-
|}

==Reception==
Andrew Chan of the Film Critics Circle of Australia writes, "One of the reasons why “I Love Hong Kong 2013″ works, is because it touches upon relevant Hong Kong people concerns and the sentimental value that people place on long lost Hong Kong culture in the rapidly changing territory. " 

I Love Hong Kong 2013 earned HK$16,894,784 at the Hong Kong box office. 

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 


 
 