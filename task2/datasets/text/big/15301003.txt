The Adventures of Food Boy
{{Infobox film
| name = The Adventures of Food Boy
| image = The Adventures of Food Boy.jpg
| caption = Poster
| director = Dane Cannon
| producer = Marc Mangum Sam Mangum Melissa Brady
| writer = Marc Mangum
| starring = Lucas Grabeel Brittany Curran Noah Bastian Ryne Sanborn Joyce Cohen
| music = 
| cinematography = Ryan Cannon
| editing = Sam Mangum
| distributor = Cold Spark Films
| released =  
| runtime = 90 minutes
| country = United States
| language = English
}} independent comedy film that was released in 2008 by Cold Spark Films. It is based on the 2007 short film Food Boy. {{cite web | url=
http://www.coldsparkfilms.com/foodboytheshortfilm.html | title=Cold Spark Films website | publisher=Cold Spark Films | accessdate=2008-01-18 }}  It stars Lucas Grabeel in his first lead role as Ezra, who becomes a superhero known as "Food Boy".    Brittany Curran plays the lead female role of Shelby.

The film was filmed in Utah at Timpview High School during summer 2007.  An early teaser trailer of The Adventures of Food Boy was posted on the Internet in November 2007. 

The Adventures of Food Boy premiered at the Newport Beach Film Festival in April 2008 where it won the award for "Best Family Film".  It saw a limited theatrical release in the United States on August 22, 2008, and saw a DVD release on October 7, 2008.

==Plot==
The Adventures of Food Boy takes a different approach to the superhero genre. The story centers around a teenage boy who discovers he has the superpower to generate food out of his hands.

Ezra (Lucas Grabeel) is a top high school student who is determined to make his life extraordinary. He is usually challenged into eating disgusting things such as puke and manages to eat them without hassle. However, his goals get more complicated when food spontaneously begins shooting from his hands at inconvenient times. Chaos usually ensues.

After discovering that he has this "gift" (which only a few people possesses), Ezra starts to use it to improve his status at school. Ezras friends Shelby (Brittany Curran), Joel (Kunal Sharma) and Dylan (Jeff Braine) also start to get more popular. But as this new superpower begins to take over his life, Ezra has to learn how best to use it. He eventually is forced to decide if he agrees with his Grandma’s (Joyce Cohen) advice that, "Not all superheroes fight crime". 

==Cast==
*Lucas Grabeel as Ezra
*Brittany Curran as Shelby
*Kunal Sharma as Joel
*Jeff Braine as Dylan
*Noah Bastian as Garrett
*Ryne Sanborn as Mike
*Joyce Cohen as Grandma
*Craig Everett as Montagu
*McCall Clark as Justine
*Brooklyn McKnight as Elle
*Bailey McKnight as Julie

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 