Youthful Folly
{{Infobox film
| name = Youthful Folly
| image =
| image_size =
| caption =
| director = Miles Mander
| producer = Norman Loudon
| writer =  Josephine Tey  (play)   Heinrich Fraenkel 
| narrator = Jane Carr Mary Lawson   Arthur Chesney
| music = 
| cinematography = 
| editing = 
| studio = Sound City Films
| distributor = Columbia Pictures 
| released = December 1934
| runtime = 74 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Jane Carr Mary Lawson. It was a quota quickie made at Shepperton Studios for release by Columbia Pictures.   It portrays the love lives of the son of daughter of an aristocratic lady. 

It is also known by the alternative title Intermezzo.

==Cast==
*   Irene Vanbrugh as Lady Wilmington  Jane Carr as Ursula Wilmington  Mary Lawson as Susan Grierson 
* Grey Blake as Larry Wilmington 
* Arthur Chesney as Lord Wilmington 
* Eric Maturin as Tim Gierson  Fanny Wright as Mrs. Grierson 
* Betty Ann Davies   
* Merle Tottenham   
* Belle Chrystall     
* Kenneth Kove 

==References==
 

==Bibliography==
*Chibnall, Steve. Quota Quickies: The Birth of the British B Film. British Film Institute, 2007.
*Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
 


 

 
 
 
 
 
 
 
 
 
 

 