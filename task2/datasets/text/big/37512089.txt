Brahmachari (1938 film)
{{Infobox film
| name           = Brahmachari
| image          = 
| alt            =  
| caption        = 
| director       = Master Vinayak
| producer       = 
| writer         = P. K. Atre    Pandit Indra  
| based on       =  
| starring       = Master Vinayak   Meenakshi Shirodkar   Damuanna Malvankar
| music          = Dada Chandekar
| cinematography = Pandurang Naik
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Marathi Hindi
| budget         = 
| gross          = 
}}
 Marathi film. The film was directed by Master Vinayak and written by Pralhad Keshav Atre and starred Master Vinayak himself along with Meenakshi Shirodkar in lead roles. The film was a political satire targeted on the Hindu nationalist organization Rashtriya Swayamsevak Sangh. The film was also made in Hindi language.

Atre paired with Master Vinayak for the second time for this film after the 1937 film Dharmaveer. Apart from the witty dialogues and satirical theme, the film became popular for Shirodkars seductive song sequence wearing a swimsuit.

==Plot==
Audumbar is a young and ordinary man. On one occasion he happens to listen to the lecture given by Deshbhakta Jatashankar. Jatashankar in his speech emphasizes on discipline and celibacy, the principals of the Rashtriya Swayamsevak Sangh. This speech inspires Audumbar and he decides to join the institute of Acharya Chandiram. He renounces his sexual desires and also throws away his collection of posters of film actors. 

But at the institute, he comes across Kishori, a young woman and all his vows to Brahmacharya are tested.

==Cast==
 
* Master Vinayak as Audumbar / Kanhaiya (in Hindi version)
* Meenakshi Shirodkar as Kishori
* V.G. Jog		
* Salvi		
* Damuanna Malvankar as Acharya Chandiram	
* Javdekar as Deshbhakta Jatashankar
* Vasant Eric

==Production==
Pralhad Keshav Atre, who had previously published many novels, poems and also written stories and dialogues for films, paired with Master Vinayak for the second time for this film. Previously they had worked together on the 1937 Marathi film Dharmaveer, which was a religious satire.   

The film was actress Meenakshi Shirodkars debut film.  She had previously worked in theatre. The film became notable for the song sequence "Yamuna Jali Khelu Khel" where Shirodkar wore a swimsuit. The scene involved the character Kishori trying to seduce Audumbar while bathing on a ghat.

==Reception and legacy==
The film had dialogues written by Atre, who is known for his sense of humour.  His satirical handling of political nationalist party Rashtriya Swayamsevak Sanghs ideologies in the Marathi medium and Pandit Indras writing for the Hindi medium brought in audiences. Critics have also credited the success of the film to these strong dialogue writers.   Apart from the dialogues, the films main attraction was Shirodkars song sequence with the swimsuit that brought repeat audience and also brought fame to Shirodkar.       The film was a hit at box-office and ran for twenty five weeks in Mumbai and for fifty in Pune.  The then critics had also criticized the bold song sequence.   
 Marathi audience and despite the criticism for the bold scene, the film brought popularity for Shirodkar.   Her Braid|twin-plait hairstyle also became popular and trendy in teenage girls. The pair of Master Vinayak and Shirodkar was well received by audiences. The two went on to play successful lead roles in various film like Brandichi Batli (1939), Ardhangi / Ghar Ki Rani (1940), Amrut (1941), Mazhe Bal (1943) and more. 
 Sachin Pilgaonkar, who then launched her in films through Gammat Jammat.  

The song "Yamuna Jali Khelu Khel" still remains popular. After its use in Kenkres play, the song was recently also used in the film Pratibimb (2011). Sung by new comer Sampada Hire, the music is composed by Avadhoot Gupte and is picturised on Ankush Choudhary and Sonali Kulkarni. 

== References ==
 

== External links ==
*  

<!--- Categories 
 --->

 
 
 