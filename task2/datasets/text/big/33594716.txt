Inspecteur Lavardin
{{Infobox film
| name           = Inspecteur Lavardin
| image          = Inspecteur Lavardin.jpg
| image_size     =
| alt            =
| caption        = DVD cover
| director       = Claude Chabrol
| producer       = Marin Karmitz
| writer         = Claude Chabrol Dominique Roulet
| narrator       =
| starring       = Jean Poiret Jean-Claude Brialy
| music          = Matthieu Chabrol
| cinematography = Jean Rabier
| editing        = Monique Fardoulis
| studio         = CAB Productions Films A2 MK2 Productions Télévision Suisse-Romande (TSR)
| distributor    =
| released       =  
| runtime        = 100 min.
| country        = France Switzerland
| language       = French
| budget         =
| gross          =
}} Cop au vin.

==Synopsis== titular inspector Roman Catholic skeletons in the closet.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Jean Poiret || Inspecteur Jean Lavardin
|-
| Jean-Claude Brialy || Claude Alvarez
|-
| Bernadette Lafont ||Hélène Mons
|- Max Charnet
|- Raoul Mons
|-
| Hermine Clair||Véronique Manguin
|- Marcel Vigoroux
|}

==Critical reception==
From Caryn James of The New York Times:
 

From Fred Camper of The Chicago Reader:
 

==Availability==
The film can be found to buy on DVD on sites such as Amazon.com 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 

 