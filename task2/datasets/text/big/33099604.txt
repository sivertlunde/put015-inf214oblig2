The Unpardonable Sin
{{infobox film
| name           = The Unpardonable Sin
| image          = Blanche Sweet card 5.jpg
| imagesize      =
| caption        = Blanche Sweet coming attraction shot
| director       = Marshall Neilan Alfred E. Green (assistant director)
| producer       = Harry Garson Blanche Sweet
| writer         = Kathryn Stuart (scenario)
| based on       =  
| starring       = Blanche Sweet Mary Alden
| music          =
| cinematography = Tony Gaudio Henry Cronjager
| editing        =
| distributor    = World Pictures
| released       =   reels (2,700 meters)
| country        = United States Silent (English intertitles)
}}

The Unpardonable Sin is a 1919 American  .

==Plot==
The film follows two American sisters: Alice and Dimny Parcot (Sweet in a dual role). Alice and their mother (Mary Alden) get stranded in Belgium when World War I breaks out. Both are raped by German soldiers and die. Dimny, who is still in the United States, is unaware of the fates of her sister and mother. She sets out to Belgium to find them. During her quest, she too is attacked by the same German soldier who raped her mother and sister.

==Cast==
*Blanche Sweet - Alice Parcot/Dimny Parcot
*Edwin Stevens - Stephen Parcot
*Mary Alden - Mrs. Parcot Matt Moore - Nol Windsor
*Wesley Barry - George Washington Sticker
*Wallace Beery - Colonel Klemm
*Bull Montana - The Brute
*Bobby Connelly - Boy Scout
*Dick Curtis (uncredited)
*John De Lacey (uncredited)

==Release==
Upon its release, the film was banned by the Kansas Board of Review due to its depiction of rape. 

==See also==
*War rape

==References==
 

==External links==
 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 