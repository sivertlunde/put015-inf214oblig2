Vemulawada Bheemakavi (film)
{{Infobox film
| name           = Vemulawada Bheemakavi 
| image          =
| caption        =
| writer         = 
| story          = N. T. Rama Rao
| screenplay     = N. T. Rama Rao 
| producer       = N. T. Rama Rao
| director       = Dasari Yoganand|D. Yoganand
| starring       = N. T. Rama Rao   Nandamuri Balakrishna 
| music          = Pendyala Nageswara Rao
| cinematography = 
| editing        = 
| studio         = Ramakrishna Studios   
| released       =  
| runtime        = 
| country        =   India
| language       = Telugu
| budget         =
| gross          = 
}}
 hagiographical film produced by N. T. Rama Rao on his Ramakrishna Cine Studios banner & directed by Dasari Yoganand|D. Yoganand. Starring N. T. Rama Rao, Nandamuri Balakrishna in the lead roles and music composed by Pendyala Nageswara Rao.      

==Plot==
The story is based on the life of Vemulawada Bheemakavi famous telugu poet of 9th century.

==Cast==
*N. T. Rama Rao
*Nandamuri Balakrishna Satyanarayana 
*Thyagaraju  Rajanala  Shavukaru Janaki  Vijayalalitha  Girija

==Soundtrack==
{{Infobox album
| Name        = Vemulawada Bheemakavi
| Tagline     = 
| Type        = film
| Artist      = Pendyala Nageswara Rao
| Cover       = 
| Released    = 1975
| Recorded    = 
| Genre       = Soundtrack
| Length      = 28:45
| Label       = AVM Audio
| Producer    = Pendyala Nageswara Rao
| Reviews     =
| Last album  = 
| This album  = 
| Next album  = 
}}

Music composed by Pendyala Nageswara Rao. Lyrics written by C. Narayana Reddy. Music released on AVM Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!length
|- 1
|Anukuntunnanu
|
|3:03
|- 2
|Saira Magada
|
|3:20
|- 3
|Rajuvedale Sabhaku
|
|6:40
|- 4
|Lera Lera (Burrakatha)
|
|3:23
|- 5
|Jagadeesa Madhapeddi Ramesh 
|2:45
|- 6
|Eesaana Nenu Needana
|
|3:12
|- 7
|Chilakala Kolikira
|
|3:25
|- 8
|Chandamama Neethoti
|
|2:57
|}

==References==
 

 
 


 