Stranger on Horseback
{{Infobox film
| name           = Stranger on Horseback
| image          = Stranger on Horseback film poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Jacques Tourneur
| producer       = Robert Goldstein
| writer         = Herb Meadow Don Martin
| story          = Louis LAmour
| starring       = Joel McCrea
| music          = Paul Dunlap
| cinematography = Ray Rennahan
| editing        = William B. Murphy
| distributor    = United Artists
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Stranger on Horseback is a 1955 American Western film directed by Jacques Tourneur starring Joel McCrea.  The screenplay is based on a story by Louis LAmour.

==Plot==
 

==Cast==
* Joel McCrea as Judge Richard Rick Thorne Miroslava as Amy Lee Bannerman Kevin McCarthy as Tom Bannerman
* John McIntire as Josiah Bannerman
* John Carradine as Col. Buck Streeter
* Nancy Gates as Caroline Webb
* Emile Meyer as Sheriff Nat Bell Robert Cornthwaite as Arnold Hammer
* Jaclynne Greene as Paula Morrison
* Walter Baldwin as Vince Webb
* Emmett Lynn as Barfly
* Roy Roberts as Sam Kettering
* George Keymas as Bannermans Henchman

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 

 