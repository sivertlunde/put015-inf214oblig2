Figaro-ci, Figaro-là
{{Infobox Film
| name           = Figaro-ci, Figaro-là
| image          = 
| image size     = 
| caption        = 
| director       = Hervé Bromberger
| producer       = 
| writer         = Hervé Bromberger Frédéric Grendel
| narrator       = 
| starring       = Jean-François Poron
| music          = 
| cinematography = André Lecoeuvre
| editing        = 
| distributor    = 
| released       = 16 June 1972
| runtime        = 
| country        = France
| language       = French
| budget         = 
| preceded by    = 
| followed by    = 
}}

Figaro-ci, Figaro-là is a 1972 French film directed by Hervé Bromberger.   

==Cast==
* Jean-François Poron - Beaumarchais
* Marie-Christine Barrault - Julie
* Yves Rénier - Gudin
* Isabelle Huppert - Pauline
* Alexandre Rignault - Caron, le père de Beaumarchais
* Henri-Jacques Huet - Le duc de Chaulnes
* Edmond Beauchamp - Paris-Duverney
* Jacques Jansen - Le prince de Conti
* André Oumansky - Sartines Jacques Bernard - La Blache
* Michèle André (actor)|Michèle André - Mme Franquet
* Pierre Negre - Franquet (as Pierre Nègre)
* Hubert de Lapparent - Goetzman
* Michèle Moretti - Mme Goetzman
* Fernand Guiot - Lepautre

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
* 

 
 
 
 
 
 