Suzhou River (film)
 
{{Infobox film 
|  name = Suzhou River
|  image = SUZHOU RIVER POSTER.JPG
|  caption = 
|  writer = Lou Ye 
|  starring = Zhou Xun Jia Hongsheng 
|  producer = Philippe Bober Nai An
|  director = Lou Ye 
|  music = Jörg Lemberg  Wang Yu
|  editing = Karl Riedl 
|  distributor =  
|  released =  
|  runtime = 83 minutes
|  country = China
|  language = Mandarin
|  budget =  
| film name = {{Film name|  jianti =苏州河
|  fanti =蘇州河
|  pinyin = Sūzhōu hé}}
}} Sixth Generation" Dream Factory. 
 Suzhou River, rather than the glitzy new face of Shanghai.

Though well-received abroad, Suzhou River was not screened in its native China, as Lou Ye was banned from filmmaking for two years after screening his film at the International Film Festival Rotterdam without permission from Chinese authorities. 

The movie is now authorized in China. 

==Plot== Suzhou River and love. As the films narrator, the videographer is nevertheless never seen aside from his hands, as the camera serves as his first-person perspective for the audience. He starts by telling the story of a romance between himself and Meimei (Zhou Xun), a performer at a dive bar in Shanghai. He discusses his love of Meimei, who makes a living putting on a blond wig and mermaid costume and swimming in a large water tank at the "Happy Tavern," and who disappears days at a time, leaving him heartbroken each time.
 Suzhou River, apparently to her death. Mardar is then arrested and imprisoned. Moudans body, however, is never found.

Years later, Mardar returns to Shanghai and resumes his work as a courier, all the while still looking for Moudan. One night, he happens upon the Happy Tavern where he meets Meimei, the videographers elusive girlfriend. Mardar, convinced shes his lost love, first spies on her in her dressing room and then seeks out the videographer to tell his story. Later, Mardar tells Meimei herself about his affair with Moudan. Humoring him at first, she eventually becomes enamored with his story and takes him to bed. After being beaten one night by the owner of the Happy Tavern, Mardar heads out to the suburbs, where he discovers the real Moudan working in a convenience store. The film ends with the ambiguous death of both Mardar and Moudan, leaving the viewer to decide if it was an accident or suicide. The videographer and Meimei spend one more night together, before Meimei disappears again, leaving a note telling the videographer that if he truly loves her, as Mardar loved Moudan, he will find her.

==Cast==
*Jia Hongsheng as Mardar (马达 Mǎdá), a small-time crook and motorcycle courier. Jia had previously starred in Lou Yes Weekend Lover.
*Zhou Xun as Moudan (牡丹 Mǔdan)/Meimei (美美 Měiměi). The dual role for Zhou Xun required her to play both the demure and girlish Moudan, and the more aggressive but no less romantic Meimei.
*Hua Zhongkai as the Videographer, Lao B (老/B Lǎo B). The audience never sees Hua and only hears his voice as the films primary narrator, with the camera serving as his subjective point of view. 
*Nai An as Xiao Hong (萧红 Xiāo Hóng), Mardars former lover and conspirator in the plot to kidnap Moudan for ransom. Nai An also served as one of Suzhou Rivers producers. 
*Yao Anlian as Boss (酒吧老板 Jiǔbā Lǎobǎn "Bar Boss"), another conspirator. A throwaway reference in the film notes that he is killed off-camera during a police chase.

==Influences== Wang Yus use of the hand-held camera as being similar to Wongs Chungking Express.      

Critics also saw the film as an homage to Alfred Hitchcocks tale of obsessive love, Vertigo (film)|Vertigo.        Suzhou Rivers thematic use of water (in this case the titular river) and its plot of men obsessed with a woman/women who may not be who she/they say they refer back to Hitchcocks earlier film.   Even the films score is said to echo Bernard Herrmanns classic soundtrack to Vertigo.   Some critics also saw in Suzhou River elements of another Hitchcock classic, Rear Window, particularly in the character of the Videographer, the films voyeuristic narrator.  

The Chinese film scholar, Shelly Kraicer, saw the film as an homage to the writer Wang Shuo, the so-called "bad boy" of Chinese literature, particularly in the films noir-like characters on the margins of society, and the playing with genre conventions.   

==Reception==
The release of Suzhou River in the international film festival circuit announced a major new voice in the sixth generation movement.  One critic boldly stated that the films take on film noir and urban life in Shanghai could potentially breathe new life into the movement.  Other western critics were captivated by the films atmosphere and its echoes of other directors like   praised the films style: "Shot with a jostling, nervous camera, Suzhou River looks great—the showy jump cuts and off-kilter close-ups belie an extremely well edited, even supple, piece of work."    At the same time, however, he made sure to announce that "Suzhou Rivers narrative is more than a bit cornball and not overly convincing—which is to say the movies conviction is to be found in its formal values."  A similar review came from The New York Times A. O. Scott, who found the film emotionally distant. Even Scott, however, could not deny the allure of the films atmospherics, describing the film as providing "impeccable attitude and captivating atmosphere." 

Ultimately, two review aggregators reflect the films general reception by western critics. Rotten Tomatoes records that 89% (25 out of 28 reviews) for Suzhou River were positive in content, while Metacritic gave a score of 76 (out of 100, based on 18 reviews), indicating "generally favorable reviews." 

===Awards & nominations===
*Belgian Syndicate of Cinema Critics, 2001 Grand Prix (nominated)
*International Film Festival Rotterdam, 2000
**Tiger Award 
*Viennale, 2000
**FIPRESCI Award, "for its realistic and documentary approach to thriller conventions, and its expressive use of narrative and cinematic structure". 
*Paris Film Festival, 2000
**Grand Prix   
**Best Actress - Zhou Xun 
*Fantasporto, 2002
**Critics Award

==Notes==
 
*Silbergeld, Jerome (2004). Hitchcock With a Chinese Face: Cinematic Doubles, Oedipal Triangles, and Chinas Moral Voice. Seattle and London: University of Washington Press.

==External links==
 
*  
*  
*  
*  
*   at the Chinese Movie Database

 

 
 
 
 
 
 
 
 
 
 