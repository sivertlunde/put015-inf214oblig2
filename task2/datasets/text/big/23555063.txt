Amreeka
 
{{Infobox film
| name           = Amreeka
| image          = Amreeka.jpg
| caption        = Theatrical release poster
| director       = Cherien Dabis
| producer       = Christina Piovesan Paul Barkin
| writer         = Cherien Dabis
| starring       = Nisreen Faour Melkar Muallem Hiam Abbass Alia Shawkat Yussuf Abu-Warda Joseph Ziegler Miriam Smith
| music          = Doug Bernheim
| editing        = Keith Reamer National Geographic Entertainment Imagenation
| released       =  
| runtime        = 96 minutes
| country        = United States Canada Kuwait
| language       = English Arabic
| budget         = 
| gross          = $2,147,715 
}}
Amreeka is a 2009 independent film written and directed by first-time director Cherien Dabis. It stars Nisreen Faour, Melkar Muallem, Hiam Abbass, Alia Shawkat, Yussuf Abu-Warda, Joseph Ziegler, and Miriam Smith.
 National Geographic Entertainment bought all theatrical and home entertainment rights to Amreeka after its debut at Sundance.     

==Plot== Palestinian Christian American green card through the lottery.     Although she initially considered declining the offer, Muna reconsiders after she and Fadi are harassed at the checkpoint by Israeli soldiers.

They arrive in the United States shortly after the  . Too ashamed to tell her family the truth, she pretends to have been hired by the bank next door to White Castle. She maintains the facade through the help of an employee of the bank next door to White Castle (Miriam Smith) and her blue-haired high school drop-out co-worker, Matt (Brodie Sanderson).

Meanwhile, Muna begins to discover that her sisters family has been experiencing difficulties in the Post-9/11 and Iraq war atmosphere of the United States. The family receives anonymous threats in the mail and Nabeel is continually losing patients from his medical practice. They are also behind on their mortgage and risk losing their home. The strain of living in this atmosphere becomes so severe that Raghda and Nabeel temporarily "separate" and Nabeel moves into the basement of the family home.
 American Jew Polish Jews. She is surprised to learn that he is Jewish. Muna asks him to drop her off at the bank but forgets her purse, an act which leads him to discover that she works secretly at White Castle. Deciding to have a meal there, they discover that they are both divorced.

On another day, local high school students make discriminatory remarks about Fadi to Muna while she is working in White Castle. She chases them out, only to slip on a drink one of the boys poured on the floor and falls flat on her back. Matt immediately calls her family who then discovers her secret. Furious over the incident, Fadi gets into a fight with one of the boys and is subsequently arrested. In addition to assault, ambiguous charges are also leveled towards him that are serious enough to prevent Muna from getting him released. Muna contacts Mr. Novatski who rushes to the police station and tells the officers that the accusations are without merit and that he will assume responsibility for Fadi. Fadi is thus released from jail. These events also lead Raghda and Nabeel to reconcile.
 Middle Eastern restaurant for dinner. While leaving she bumps into Mr. Novatski and invites him to join them for dinner. Raghda teases Muna when he enters the car and the evening ends with music and dancing.

==Cast==
* Nisreen Faour as Muna Farah
* Melkar Muallem as Fadi Farah
* Hiam Abbass as Raghda Halaby
* Yussuf Abu-Warda as Nabeel Halaby
* Alia Shawkat as Salma Halaby
* Jenna Kawar as Rana Halaby
* Selena Haddad as Lamis Halaby
* Joseph Ziegler as Mr. Novatski
* Brodie Sanderson as Matt
* Miriam Smith as the Bank Employee (Illinois)

==Production==
 

Dabis began working on the screenplay for Amreeka while a graduate student in film at Columbia University School of the Arts in 2003.    Christina Piovesan, a producer in Toronto, read about Dabis in Filmmaker Magazine, thus leading the two of them to collaborate on "Amreeka." Over the next three years, Piovesan gathered financing from a number of sources in the Middle East and Canada. Dabis also traveled for months in order to hold casting auditions in New York, Chicago, Los Angeles, Dearborn, Michigan|Dearborn, Toronto, Winnipeg, Paris, Amman, Beirut, Haifa, Jerusalem, Bethlehem and Ramallah.    In choosing Nisreen Faour for the lead role of Muna, Dabis commented that she "had a sweetness about her   a kindness and a childlike sense of wonder. There was something about her that was so youthful, and yet, I could still see in her eyes the depth of sadness that her life experience had given her.”   On the choice of Melkar Muallem as Fadi, Dabis commented that he was, "the son of a Palestinian woman who helped cast the film. He wanted nothing to do with acting—both parents are actors, and he’s only interested in computer science. I begged him to audition, and after he did, he wanted the part."    Muallem is from Ramallah, one of the locations for the film. Dabis also chose to work with film and television actors Hiam Abbass and Alia Shawkat and a theater actor from Haifa, Yussef Abu Warda.  Dabis modeled the family after her own, making them Christian Palestinians.    Events surrounding the inclusion of a kind high school principal were taken from Dabis own life. While in high school, serious accusations were leveled at her sister, who was saved by the intervention of the school principal. Dabis chose to make her fictional version of this principal, Mr. Novatski (Joseph Ziegler), Jewish-American in order to create parallel experiences related to "immigration and displacement."    In another interview Dabis stated that she included this because in the United States Arabs and Jews are friends and "we dont get to see that enough."   
 White Castle was also "an enthusiastic supporter of the film" and contributed "a truckload of real White Castle supplies." 

==Music==
Music in the film the closing song "Jawaz al-safar" ("Passport"), music by Marcel Khalife, based on a poem by Mahmoud Darwish.

==Critical reception==
As of June 7, 2013, Amreeka has received an overall rating of 87% from Rotten Tomatoes (61 fresh and 9 rotten reviews).  On Metacritic, the film had an average score of 73 out of 100, based on 23 reviews, indicating "Generally Favorable" reviews.  American critic Roger Ebert gave Amreeka three and a half out of four stars and described it as "Cherien Dabiss heart-warming and funny first feature." 

==Awards==

===2011===
2011 Intersections Film Festival
*Official Selection: Opening Night 

===2010===
2010 Heartland Film Festival
*Won:  One of Top Ten Truly Moving Pictures For 2009    

2010 Independent Spirit Awards
*Nominated: Independent Spirit Award for Best Film
*Nominated: Independent Spirit Award for Best First Screenplay
*Nominated: Independent Spirit Award for Best Female Lead – Nisreen Faour 

 2010 NAACP Image Awards
*Nominated:Outstanding Independent Motion Picture 

===2009===
2009 Cairo International Film Festival
* Won: Best Arabic Film – Cherien Dabis (director), Christina Piovesan (producer)
* Won: Best Arabic Screenplay – Cherien Dabis (director) 

2009 Cannes Film Festival Fipresci Prize – Cherien Dabis
* Official Selection: Directors Fortnight 

2009 Dubai International Film Festival
* Won: Muhr Award, Best Actress: Nisreen Faour 

 Gotham Independent Film Awards 2009 Best Feature 

2009 National Board of Review of Motion Pictures
*Won: One of Top Ten Independent Films. 

2009 New Directors/New Films
* Official Selection: Opening Night 

2009 Sundance Film Festival
* Official Selection: Dramatic Competition Sundance 

===2007===
 Tribeca All Access L’Oréal Paris Women of Worth Vision Award.

==DVD== Make a Wish, which premiered at the  2007 Sundance Film Festival and received a number of awards at other festivals. 

==See also==
 
* List of cultural references to the September 11 attacks

==References==
 

==Further reading==
* Khayat, Rasha. " ." 2010.
* Legel, Laremy. " ." September 27, 2009.
* Matchan, Linda. " ." September 25, 2009.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 