Naanum Rowdydhaan
{{Infobox film
| name           = Naanum Rowdy Dhaan
| image          = Naanum_Rowdydhaan_Promotional_Poster.jpg
| caption        = Naanum Rowdydhaan Title Design
| director       = Vignesh Shivan
| producer       = Dhanush
| writer         = Vignesh Shivan
| starring       = Vijay Sethupathi Nayanthara
| music          = Anirudh Ravichander George C. Williams
| editing        = Sreekar Prasad
| Extra          = Cika
| studio         = Wunderbar Films
| distributor    = Elred Kumar|R. S. Infotainment
| released       =  
| country        = India
| language       = Tamil
| budget         = 
}}
 Indian Tamil Tamil film written and directed by Vignesh Shivan, in his second venture after Podaa Podi (2012). The film features Vijay Sethupathi and Nayantara in the lead roles. Produced by actor Dhanush under his banner Wunderbar Films, the film features music composed by Anirudh Ravichander. 

==Cast==
* Vijay Sethupathi
* Nayantara
* RJ Balaji
* R. Parthiepan
* Raadhika Sarathkumar
* Anandaraj

==Production== Samantha for the female lead role.    In a turn of events in the following month, Anirudh chose not to make an acting debut and the film was briefly shelved as the team looked for a replacement lead actor.   
 Raja Rani, while Dilip Subbarayan was announced as the films action director.    Vignesh Shivan revealed that the film would be based on a 19 year old youngster caught in the midst of a gangster setting and noted he hoped to shoot the film in Mumbai, Chennai and Puducherry.    Lavanya Tripathi was signed on to play the female lead role of a deaf girl and noted that she was impressed by Vignesh Shivans narration of the script.   

On 29 August 2014, Vinayagar Chathurthi, in a turn of events, Dhanush announced via Twitter that he would be producing the film under his Wunderbar Film banner and also posted details about the cast and crew and the title design. While the principal crew was retained, the lead roles went to Vijay Sethupathi and Nayantara.    The team began its shoot in December 2014, with the first leg of the venture held in Pondicherry for a span of 40 days. Scenes were shot at the Thirukameeswarar Temple in Villianur, a famous temple town 9&nbsp;km away from Pondicherry bus stand.  RJ Balaji joined the teams cast during the first schedule.  Director Vignesh Shivan revealed that Vijay Sethupathi had put on weight for some of his recent roles, so he worked hard for the past two months to shed those pounds. He worked out and went on an extensive diet regime as well — he even went without eating at times.   Anandaraj joined the team in December 2014 and shot for the film in Pondicherry, and revealed he would play a gangster with negative shades. 

==Release== Sun TV.

==References==
 

==External links==
*  
*  

 
 
 
 