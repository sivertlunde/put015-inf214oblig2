Get Rich Quick Porky
{{multiple issues|
 
 
 
}}
{{Infobox Hollywood cartoon|
| cartoon_name = Get Rich Quick Porky
| series = Looney Tunes (Porky Pig)
| image = 
| caption = 
| director = Robert Clampett
| story_artist =  Charles Jones
| voice_actor = Mel Blanc
| musician = Carl W. Stalling
| producer = Leon Schlesinger Leon Schlesinger Productions
| distributor = Warner Bros., The Vitaphone Corporation
| release_date = August 28, 1937 (United States of America|U.S.A.) Black & White
| runtime = 7 min. English
| followed_by =
}}
 American animation|animated Gabby Goat.

==Summary== two per cent." Just as Porky would enter the building, Mr. Gusher blocks his path, barely introduces himself by way of a hastily drawn and withdrawn business card, and points out the plot just across the street. With Gabby egging him on, Porky signs the wayward oilmans deed and turns over his sack in exchange for the field.

The two friends, having gathered some tools in the meanwhile, begin their excavation. Gabby, by means of a pickaxe, unearths a can of oil. A dog wanders onto the property and attempts to bury a bone, only to have it spat back at him by a small gusher; the dog has some further difficulties restraining the spouts of crude. Gabby rides a   of his money back!

By this point Gabby, still astride his jackhammer, is far beneath the earth. Just as Porky is about to accept Mr. Gushers offer, Gabby hits a large vein of oil, which liquid then bursts through the surface and carries all of the major characters high into the air. Porky realizes his new wealth and proclaims it as he tries to pull the deed away from the con artist, who, following upon Porkys initial acceptance of the offer of one dollar, had grabbed it. Gabby unintentionally strikes Gusher with the jackhammer, leading the crook to shout in pain as Gabby falls backwards onto Porky and the two friends fall off of the spout and to the ground. Sitting up, Porky gleefully announces that he has the deed in his hand, but he soon realizes that, in fact, he is holding the dogs bone. A despondent Porky turns away, but his sulking is interrupted by the same gopher who earlier so annoyed our canine friend; silently requesting the useless bone, the cheerful gopher, exercising a variation on a now familiar magic trick, transmutes the thing into the deed, but withholds it from Porky, promising to release it once Porky makes him half-owner of the oil field!

==References==
 

 

 
 
 
 
 
 