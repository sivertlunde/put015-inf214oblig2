Lucky Di Unlucky Story
{{Infobox film
| name           = Lucky Di Unluky Story
| image          = Lucky Di Unlucky Story.jpg
| alt            = 
| director       = Smeep Kang
| writer         = Smeep Kang, Naresh Kathuria, Vaibhav Suman Samiksha Singh Binnu Dhillon Gurpreet Ghuggi Jaswinder Bhalla
| music          = Jatinder Singh-Shah & Gurmeet Singh
| released        =  
| studio          = Gurfateh Films Sippy Grewal Productions
| language       = Punjabi
| country        = India
| budget         =  5.25 crores
| gross          =  14.75 crores

}} Punjabi comedy Tamil blockbuster Panchathanthiram starring Kamal Haasan & Simran. Panchathanthirams story was written by Kamal Haasan & Crazy Mohan.

==Plot==
After Lucky (Gippy Grewal) is taken on a vacation for his birthday by his three friends, Dimpy (Binnu Dhillon), Brar (Jaswinder Bhalla) and Sidhu (Gurpret Ghuggi), a murder takes place and the four friends are the main suspects in their own eyes. Then a funny sequence of events take place and finally they find out about the real killer and the real victim.

==Cast==
* Gippy Grewal as Lucky
* Jaswinder Bhalla as Mr. Brar
* Gurpreet Ghuggi as Sidhu
* Binnu Dhillon as Dimpi
* Surveen Chawla as Seerat
* Karamjit Anmol as Dimpis brother in law
* Samiksha as Shilpy sikka
* Jackie Shroff as Fateh

==Box Office==
Lucky Di Unlucky Story had a good opening week collections at the time of release for a Punjabi film in the state of Punjab apparently. The opening 1st week collections were Rs 11.35 cr. The film was declared a blockbuster  at the box office in India.

==External links==
*  
*  
*  
*  

 
 
 
 