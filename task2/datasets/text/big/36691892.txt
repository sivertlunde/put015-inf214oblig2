My Autograph
{{Infobox film
| name        = My Autograph
| image       = My Autograph Movie.jpg Cheran
| Deepu
| director    = Sudeep
| producer    = Sudeep
| distributor =
| released    =  
| runtime     =
| music       = Bharathwaj Rajesh Ramanath (BGM)
| language    = Kannada
| country     = India
| budget      =   
}} Kannada film which was directed, produced and enacted by Sudeep. This was his first directorial venture. Along with him, this film starred Meena Durairaj|Meena, Sridevika, Deepu, Rashmi Kulkarni and more. 
 Tamil film Cheran and Sneha in lead roles. The songs of the movie became very popular amongst the masses, who found them very inspirational.

==Plot==

The movie begins with Shankar(Sudeep) , who runs an advertising agency, boarding a train to visit his native village to invite all his friends for his wedding.The journey to his childhood days begins there. The happenings in the school, his tussle with his friends and his first love with his classmate, Kamala (Deepa Bhaskar|Deepu), are all pictured realistically. Shankar reaches the village and invites all including Kamala, who promises to come to the wedding, with her husband and three children.Then, he goes to Kerala where he had his college education. His major crush at that time was Lathika (Sreedevika), a Malayalee girl, with whom he falls in love but later the affair proves to be short- lived as her parents marry her off to her cousin, Madhavan. On reaching Kerala to invite her, Shankar is shattered to see his lover as a widow.Meanwhile, he is dejected at the failure of his love affair then and he comes across a trusted friend Divya (Meena), who instils confidence, unearths his hidden talents and teaches him the lesson that one has to go ahead in life without looking back. However, she does not reveal the tragedy that occurred in her past.

But as time passes by, she reveals that her mother is a paralytic patient and that she now has to work for survival. While she and Shankar travel on a bus, she reveals that she was in love with someone and believed that he was a good man but got cheated. A poetic narration on the need for a good friend has been stressed.Towards the end, Shankar gets married to a girl of his parents choice, Rashmi (Rashmi Kulkarni), and all the three girls, who had played a part in his life, and many college friends attend the wedding. Also he sets a very nice ending to the main story.

==Cast==
* Sudeep as Shankar Meena as Divya
* Sridevika as Lathika Deepu as Kamala
* Rashmi Kulkarni
* Srinivasa Murthy as Shankars father
* Yathiraj as Sathya
* Harish
* Laxmi Narayan
* Vishwanath

==Reception==
This film got good response from critics, it completed 175 days and it is declared as superhit.

==Soundtrack==
{|class="wikitable"
! Sl No. !! Song Title !! Singer(s) !! Lyrics
|-
| 1 || "Nannavalu" || Rajesh Krishnan || K. Kalyan
|-
| 2 || "Araluva Hoovugale" || K. S. Chitra || K. Kalyan
|-
| 3 || "Malle Hudugi" || Rajesh Krishnan, Rashmi || K. Kalyan
|-
| 4 || "Jagadodharana" || Rashmi, Srividya || Purandara Dasa
|-
| 5 || "Kila Kila" || Chetan Sosca || K. Kalyan
|- Hariharan || K. Kalyan
|-
|}
 

==Review==
* chitraloka 

==Awards==
{| class="infobox" style="width: 22.7em; text-align: left; font-size: 85%; vertical-align: middle; background-color: #eef;"
|-
| colspan="3" |
{| class="collapsible collapsed" width="100%"
! colspan="3" style="background-color: #d9e8ff; text-align: center;" | Awards and nominations
|- style="background-color:#d9e8ff; text-align:center;"
!style="vertical-align: middle;" | Award
| style="background:#cceecc; font-size:8pt;" width="60px" | Wins
| style="background:#eecccc; font-size:8pt;" width="60px" | Nominations
|-
|align="center"|
;Filmfare Awards South
| 
| 
|}
|- style="background-color:#d9e8ff"
| colspan="3" style="text-align:center;" | Totals
|-
|  
| colspan="2" width=50  
|-
|  
| colspan="2" width=50  
|}
Filmfare Awards South :- Best Female Playback Singer – Kannada - K. S. Chitra 

==External links==
*  
*  
*  

==References==
 

 
 

 
 
 
 
 