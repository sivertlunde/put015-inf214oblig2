Fantômas (1913 serial)
 
{{Infobox film
| name           = Fantômas
| image          = Fantomas early film poster.jpg
| caption        = Film poster
| director       = Louis Feuillade
| producer       = Romeo Bosetti
| writer         = Marcel Allain Louis Feuillade Pierre Souvestre
| starring       = René Navarre Edmond Bréon Georges Melchior Renée Carl
| cinematography = Georges Guérin
| editing        =  Gaumont
| released       =  
| runtime        = 337 minutes  
| country        = France 
| language       = Silent with French intertitles
}} silent Crime crime film novel of the same name.    The five episodes, initially released throughout 1913-14, were restored under the direction of Jacques Champreaux and released in this new form in 2006.

The series consists of five episodes, each an hour to an hour and a half in length, which end in cliffhangers, i.e., episodes one and three end with Fantômas making a last-minute escape, the end of the second entry has Fantômas blowing up Lady Belthams manor house with Juve and Fandor, the two heroes, still inside. The subsequent episodes begin with a recap of the story that has gone before. Each film is further divided into three or more chapters that do not end in cliffhangers.

==Films==
 
 
# Fantômas I: À lombre de la guillotine (Fantômas: In the Shadow of the Guillotine) (1913)
## Le Vol du Royal Palace Hotel (The Theft at the Royal Palace Hotel)
## La Disparition de Lord Beltham (The Disappearance of Lord Beltham)
## Autour de léchafaud (By the Guillotine)
# Fantômas II: Juve contre Fantômas (Juve vs. Fantômas) (1913)
## La Catastrophe du Simplon-Express (Disaster on the Simplon Express)
## Au "Crocodile" (At the Crocodile)
## La Villa hantée  (The Haunted Villa)
## LHomme noir (The Man in Black)
# Fantômas III: Le Mort Qui Tue (The Murderous Corpse) (1913)
## Le Drame rue Novins (The Tragedy in Rue Novins)
## LEnquête de Fandor (Fandors Investigation)
## Le Collier de la princesse (The Princesss Necklace)
## Le Banquier Nanteul (The Banker Nanteul)
## Elizabeth Dollon
## Les Gants de peau humaine (The Human Skin Gloves)
# Fantômas IV: Fantômas contre Fantômas (Fantômas vs. Fantômas) (1914)
## Fantômas et lopinion publique (Fantômas and Public Opinion)
## Le Mur qui saigne (The Wall that Bleeds)
## Fantômas contre Fantômas (Fantômas vs. Fantômas)
## Règlement de comptes (Getting Even)
# Fantômas V: Le Faux Magistrat (The False Magistrate) (1914)
## Prologue (The Theft at the Château des Loges)
## Le Prisonnier de Louvain (The Prisoner of Louvain)
## Monsieur Charles Pradier, juge dinstruction (Charles Pradier, Examining Magistrate)
## Le Magistrat cambrioleur ( The Burglar Judge)
## LExtradé de Louvain (The Extradited Man)

==Cast==
* René Navarre as Fantômas aka Gurn, Tom Bob and many other aliases
* Edmund Breon as Inspector Juve
* Georges Melchior as Jérôme Fandor, reporter for the Capital newspaper and Juves collaborator
* Renée Carl as Lady Beltham, Fantômas mistress
* Jane Faber as Princesse Danidoff
* Volbert as Valgrand
* Naudier as Nibet
* Maillard as Valgrands dresser
* Yvette Andréyor as Josephine

==Reception==
Fantômas was enormously popular upon its release in France, and made Navarre, who played Fantômas, an overnight celebrity.  In a rave review from a 1914 issue of the French journal Chronique cinématographique, critic Maurice Raynal wrote that "There is nothing in this involved, compact, and concentrated film but explosive genius." 

In his contemporary critical review of the Fantômas serial, Peter Schofer notes that contrary to some modern understandings of the series, Fantômas was not interpreted by its audience as a suspense film. Based on a previously published and widely read newspaper serial, audiences of the time were already extensively familiar with the plot, characters, and outcome of the story, making the film much more about how the story might develop as opposed to what might happen next. 

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
 
*  

 

 
 
 
 
 
 
 
 
 
 
 