Waking Life
 
{{Infobox film
| name           = Waking Life
| image          = Waking-Life-Poster.jpg
| image_size     = 220px
| alt            =
| caption        = Theatrical release poster
| director       = Richard Linklater
| producer       = Tommy Pallotta Jonah Smith Anne Walker-McBay Palmer West
| writer         = Richard Linklater
| starring       = Wiley Wiggins
| music          = Glover Gill
| cinematography = Richard Linklater Tommy Pallotta
| editing        = Sandra Adair
| studio         = Thousand Words
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 101 minutes  
| country        = United States
| language       = English
| budget         =
| gross          = R$10,00 
}} adult animated docufiction/drama film, directed by Richard Linklater. Distributed by Fox Searchlight Pictures, it is the first (and currently only) animated film released by Fox Searchlight Pictures. The film explores a wide range of philosophical issues including the nature of reality, dreams, consciousness, the meaning of life, free will, and existentialism.  Waking Life is centered on a young man who wanders through a variety of dream-like realities wherein he encounters numerous individuals who willingly engage in insightful philosophical discussions. Many questions are posed by the film: How can a person distinguish their dream life from their waking life? Do dreams have any sort of hidden significance or purpose?
 live actors with a team of artists drawing stylized lines and colors over each frame with computers, rather than being filmed and traced onto cells on a light box. The film contains several parallels to Linklaters 1991 film Slacker (film)|Slacker. Ethan Hawke and Julie Delpy reprise their characters from Before Sunrise in one scene.   Waking Life premiered at the 2001 Sundance Film Festival. 

==Plot== situationist politics, posthumanity, the film theory of André Bazin, and lucid dreaming, and makes references to various celebrated intellectual and literary figures by name.

Gradually, the protagonist begins to realize that he is living out a perpetual dream, broken up only by occasional false awakenings. So far he is mostly a passive onlooker, though this changes during a chat with a passing woman who suddenly approaches him. After she greets him and shares her creative ideas with him, he reminds himself that she is a figment of his own dreaming imagination. Afterwards, he starts to converse more openly with other dream characters, but he begins to despair about being trapped in a dream.

The protagonists final talk is with a character who looks somewhat similar to the protagonist himself and whom he briefly encountered previously, earlier in the film. This last conversation reveals this other characters understanding that reality may be only a single instant that the individual interprets falsely as time (and, thus, life); that living is simply the individuals constant negation of Gods invitation to become one with the universe; that dreams offer a glimpse into the infinite nature of reality; and that in order to be free from the illusion called life, the individual need only to accept Gods invitation.

The protagonist is last seen walking into a driveway when he suddenly begins to levitate, paralleling a scene at the start of the film of a floating child in the same driveway. The protagonist uncertainly reaches toward the cars handle, but is too swiftly lifted above the vehicle and over the trees. He rises into the endless blue expanse of the sky until he disappears from view.

==Cast==
* Wiley Wiggins plays the protagonist.

The film features appearances from a wide range of actors and non-actors, including:
* Eamonn Healy
* Timothy "Speed" Levitch
* Adam Goldberg
* Nicky Katt
* Steven Soderbergh
* Ethan Hawke
* Julie Delpy Steven Prince
* Caveh Zahedi Otto Hofmann
* Richard Linklater
* Aklilu Gebrewold
* David Martinez (filmmaker) American philosophers and writers, including:
* Louis H. Mackey
* David Sosa Alex Jones
* Robert C. Solomon
* Kim Krizan

==Production==
Adding to the dream-like effect, the film used an animation technique based on rotoscoping.  Animators overlaid live action footage (shot by Linklater) with animation that roughly approximates the images actually filmed.   This technique is similar in some respects to the rotoscope style of 1970s filmmaker Ralph Bakshi. Rotoscoping itself, however, was not Bakshis invention, but that of experimental silent film maker Max Fleischer, who patented the process in 1917.  A variety of artists were employed, so the feel of the movie continually changes, and gets stranger as time goes on. The result is a surrealism|surreal, shifting dreamscape.
 A Scanner Darkly.

==Release== premiered at the Sundance Film Festival in January 2001, and was given a limited release in the United States on October 19, 2001.

===Reception===
Critical reaction has been mostly positive. It holds a rating of 80% across 137 reviews on review aggregator Rotten Tomatoes — with critical consensus that "the talky, animated Waking Life is a unique, cerebral experience" — and an average score of 82/100 ("universal acclaim") on Metacritic, based on 31 reviews.   Roger Ebert of the Chicago Sun-Times gave the film four stars out of four, describing it as "a cold shower of bracing, clarifying ideas."  Ebert later included the film on his list of "Great Movies".  Lisa Schwarzbaum of Entertainment Weekly awarded the film an "A" rating, calling it "a work of cinematic art in which form and structure pursues the logic-defying (parallel) subjects of dreaming and moviegoing,"  while Stephen Holden of The New York Times said it was "so verbally dexterous and visually innovative that you cant absorb it unless you have all your wits about you."  Dave Kehr of The New York Times found the film to be "lovely, fluid, funny" and stated that it "never feels heavy or over-ambitious." 

Conversely, J. Hoberman of The Village Voice felt that Waking Life "doesnt leave you in a dream&nbsp;... so much as it traps you in an endless bull session."  Frank Lovece felt the film was "beautifully drawn" but called its content "pedantic navel-gazing." 

Nominated for numerous awards, mainly for its technical achievements, Waking Life won the National Society of Film Critics award for "Best Experimental Film", the New York Film Critics Circle award for "Best Animated Film", and the "CinemAvvenire" award at the Venice Film Festival for "Best Film". It was also nominated for the Golden Lion, the festivals main award.

===Home media=== in Region 2 in February 2003.

==Soundtrack==
The Waking Life OST was performed and written by Glover Gill and the Tosca Tango Orchestra, except for Frédéric Chopins Nocturne in E-flat major, Op. 9, No. 2. The soundtrack was relatively successful. Featuring the nuevo tango style, it bills itself "the 21st Century Tango."  The tango contributions were influenced by the music of the Argentine "father of new tango" Ástor Piazzolla.

==See also==
* Dream argument
* Dream art
* Oneironaut
* Simulated reality

==References==
*  
*  

;Notes
{{reflist|30em|refs=
    -->
   
   
   
   
<!--
   
-->
 {{cite web
|url=http://www.washingtonpost.com/ac2/wp-dyn/A50955-2001Oct25?language=printer
|title=Aroused by Waking Life
|work=The Washington Post
|publisher=The Washington Post Company
|accessdate=May 26, 2009
|last=Howe
|first=Desson
|date=October 26, 2001
}} 
   
 
{{cite web
|url=http://www.rottentomatoes.com/m/waking_life
|title=Waking Life
|work=Rotten Tomatoes
|publisher=IGN Entertainment
|accessdate=May 26, 2009
}}
  
{{cite web
|url=http://www.metacritic.com/video/titles/wakinglife
|title=Waking Life
|work=Metacritic
|publisher=CBS Interactive
|accessdate=May 26, 2009
}}
 
 
{{cite news
|url=http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20011019/REVIEWS/110190306/1023
|title=Waking Life
|work=Chicago Sun-Times
|publisher=Sun-Times Media Group
|date=October 19, 2001
|accessdate=May 26, 2009
|last=Ebert
|first=Roger
}} 
 
{{cite news
|url=http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20090211/REVIEWS08/902119997/-1/RSS
|title=Waking Life
|work=Chicago Sun-Times
|publisher=Sun-Times Media Group
|date=February 11, 2009
|accessdate=May 26, 2009
|last=Ebert
|first=Roger
}}
 
 
{{cite journal
|url=http://www.ew.com/ew/article/0,,180109~1~0~wakinglife,00.html
|title=Waking Life
|journal=Entertainment Weekly
|publisher=Time Inc.
|accessdate=May 26, 2009
|last=Schwarzbaum
|first=Lisa
|date=October 18, 2001
}}
 
 
{{cite news
|url=http://www.nytimes.com/2001/10/12/movies/12WAKI.html
|title=Surreal Adventures Somewhere Near the Land of Nod
|work=The New York Times
|publisher=The New York Times Company
|date=October 12, 2001
|accessdate=May 26, 2009
|last=Holden
|first=Stephen
}}  
 
 
{{cite journal
|url=http://www.villagevoice.com/2001-10-16/film/sleep-with-me/1
|title=New York Movies – Sleep With Me
|publisher=Village Voice Media
|journal=The Village Voice
|accessdate=May 26, 2009
|last=Hoberman
|first=J.
|date=October 16, 2001
}}
 
 
{{cite web
|url=http://movies.tvguide.com/waking-life/review/135385
|title=Waking Life Review
|publisher=TVGuide.com
|accessdate=May 26, 2009
|last=Lovece
|first=Frank
}}
 
}}

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 