Bilitis (film)
{{Infobox film
| name           = Bilitis
| image          = Bilitis1977Poster.jpg
| alt            =  
| caption        =  David Hamilton
| producer       = Sylvio Tabet Malcolm J. Thomson
| writer         = Robert Boussinot  Catherine Breillat Jacques Nahum Pierre Louÿs (poem cycle)
| starring       = Patti DArbanville
| music          = Francis Lai
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}} David Hamilton with film score by composer Francis Lai. It stars Patti DArbanville and Mona Kristensen, Hamiltons first wife, as the title characters Bilitis and Melissa respectively.

The film is loosely based on a poem cycle by Pierre Louÿs entitled The Songs of Bilitis set in ancient Greece, although the film is set in modern Europe. The poems are meant to be autobiographical poems by the title character. 

==Synopsis==
A teenage schoolgirl spends the summer with a couple whose marriage is on the rocks and develops a crush on the wife. Meanwhile, she pursues a local teenage boy and tries to find a "suitable male lover" for the wife. 
 
Although Bilitis can be best described as a coming of age film, the title character, Bilitis, ends up returning to school at the end of the film, realizing she is not yet ready for adulthood.

==Production== David Hamiltons photography and his other films.

==Companion book==
In 1977, Hamilton released a photobook, Bilitis, which includes the most memorable images from the film.

==Cast==
* Patti DArbanville as Bilitis
* Mona Kristensen as Melissa
* Bernard Giraudeau as Lucas
* Mathieu Carrière as Nikias
* Gilles Kohler as Pierre
* Irka Bochenko as Prudence
* Jacqueline Fontaine as Head Mistress
* Marie-Thérèse Caumont as Sub-Principal
* Germaine Delbat as Principal
* Madeleine Damien as Nanny
* Camille Larivière as Susy
* Catherine Leprince as Helen

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 

 
 