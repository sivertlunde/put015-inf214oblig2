Little Golden Book Land
 
{{Infobox Film
| name           =  Little Golden Book Land
| image          =  
| image_size     = 
| caption        =  
| director       =   Al Zegler
| producer       =   Richard Raynis
| writer         =   Phil Harnage
| narrator       =   
| starring       =   Tony Ali Graham Andrews Tony Balshaw Emily Perkins
| music          =   Shuki Levy Haim Saban
| cinematography =   
| editing        =   
| studio         =   Western Publishing Company
| distributor    =   
| released       =   
| runtime        =   
| country        =  USA
| language       =  English
| budget         =   
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Little Golden Book Land is an animated syndicated special produced by DIC Entertainment and Western Publishing Company in 1989 in film|1989.  The special stars the characters that made Little Golden Books so popular among many readers, and was probably a pilot episode (because of the theme song at the beginning) for an animated series that never materialized.  On video and coloring books, the working title was Little Golden Book Land: The Great Harbor Rescue.

==Plot==
Scuffy the Tugboat and his friends race against time to help fix the hole in the breakwater at Harbortown before the next storm arrives.

==Storyline==
On a beautiful summers day at Harbortown in Little Golden Book Land, Beamer the Old Lighthouse is worried because of the breakwater.  Previous storms have battered the breakwater so much that the last storm broke a hole in it.  As Beamer knows what consequences the next storm will bring, he calls a meeting with the big ships.  Scuffy the Tugboat overhears the meeting, and after learning whats wrong, he promises to help fix the breakwater.  The big ships laugh at him for such a thought.  Beamer orders them to stop it, but Scuffy is determined to prove that a little tugboat like him can do big things.
 Poky Little Puppy, and Shy Little Kitten, and explains the problem.  Despite their fears about getting into trouble, they agree to help Scuffy fix the breakwater.  Tootle suggests logs and the Shy Little Kitten suggests sand, but after Katy Caboose comments on Scuffy having rocks in his head, Scuffy sees that a big rock from Cavetown is just what they need.

The only way to get to Cavetown quickly is to take a shortcut through Jolly Jungle.  The friends reach Jolly Jungle and after getting through some vine covered tracks, they make it to Crocodile Bridge.  Tootle tries sneaking quietly across, but the crocodiles spot them and make Scuffy fall overboard into the river.

Scuffy outraces the hungry crocodiles, but goes over a waterfall and meets up with Saggy Baggy Elephant.  Saggy Baggy Elephant also finds Poky Little Puppy (who went out to look for Scuffy) hiding in a pile of leaves.

Meanwhile, Tootle, Katy Caboose, and Shy Little Kitten have come to the Rabbits house where Tawny Scrawny Lion is hanging out with his rabbit friends.  After getting tossed out of the hammock, Tawny Scrawny Lion agrees to help.  After Katy Caboose points out they need a big rock and not a carrot, Tawny Scrawny Lion offers to come with his friends to Cavetown to protect them.  But from what?

Meanwhile, Poky Little Puppy and Scuffy have told Saggy Baggy Elephant about the breakwater, and Saggy Baggy Elephant also agrees to help.  He offers them his coconuts, but after Scuffy points out they need a rock, Saggy Baggy Elephant takes his friends along a path to Cavetown.

Back at Harbortown, Beamer looks out at the storm clouds on the horizon, and along with the big ships, is starting to worry whether Scuffy and his friends will make it back in time.

Up in the mountains, Tootle takes his friends into Cavetown Tunnel, where he becomes frightened by the bats, but nevertheless makes it through.  Outside, they meet up with Poky Little Puppy, Saggy Baggy Elephant, and Scuffy, only to find Shy Little Kitten has gone missing.  In order to find Shy Little Kitten, Poky Little Puppy follows her footprints and eventually finds her and her new friend, Baby Brown Bear.

After learning about the breakwater, Baby Brown Bear helps his new friends by leading them to a big round rock perched on a mountaintop.  Just as the storm starts, all friends with arms and legs push the rock off the mountaintop right down to Tootle.

Tootle and Katy Caboose escape getting flattened, but the rock pushing down on the track holds them back.  After everybody gets back on board does Tootle start making a run for it.  The rock then chases the friends down the track, creating a trail of destruction behind it.  Almost to Harbortown, the rock bounces and lands in Scuffys bathtub car (Scuffy is safely out of the way in Saggy Baggy Elephants car).

At Harbortown, just as the storm is starting to reach its fury and batter the breakwater even further, Beamer catches sight of Tootle and orders the cargo ship to take action.  The cargo ship takes out some mattresses from his hold and puts them on the ground.  Tootle then slams on his brakes and sends everything flying out of his train.  Poky Little Puppy, Shy Little Kitten, Tawny Scrawny Lion, Saggy Baggy Elephant, and Baby Brown Bear land on the mattresses, and Scuffy lands back in the water.  The big rock lands squarely in the hole in the breakwater, plugging it up and preventing Harbortown from being flooded.

Eventually, the storm passes and the Golden Sun shines down again.  Then everybody holds a party for Scuffy, hosted by Beamer.  Beamer congratulates Scuffy for saving the day, but Scuffy gives the credit to all his friends.  After Tawny Scrawny Lion makes a toast to Scuffy, Shy Little Kitten catches everybodys attention by spotting a rainbow in the sky.

==Themes==
This special contained two themes.  The first one was that even little people can do big things.  Scuffy was little, but he did a big thing by saving Harbortown from getting flooded and Beamer from getting washed away.  The second theme was the power of friendship.  Scuffy knew he couldnt save the day alone, so he called on the help of his friends to help him put things right.

==Advertisement==

On every VHS release of Little Golden Book Land, an advertisement for the bath toy Water Pets is included in the middle of the story.

==Cast and characters==
(in order of appearance)
*Beamer the Old Lighthouse...Imbert Orchard
*Scuffy the Tugboat...Tony Ail
*Tootle the Train...Dillian Bouey
*Poky Little Puppy...Chiara Zanni
*Shy Little Kitten...Tony Balshaw
*Katy Caboose...Emily Perkins
*Tawny Scrawny Lion...Graham Andrews
*Saggy Baggy Elephant...Lelani Marrell
*Baby Brown Bear...Tony Dakota

==Facts==
Fans of Thomas the Tank Engine & Friends will notice Scuffys whistle sounds like Trevor The Traction Engines whistle.

The rock chase sequence was a parody of the boulder chase sequence in the 1981 Indiana Jones film Raiders of the Lost Ark.  This same scene probably inspired the Thomas the Tank Engine & Friends episode "Rusty and the Boulder".

Animation error: Tootle is a steam locomotive with a 4-4-2 wheel arrangement and a four-wheeled tender, but during the scene where hes taking Katy Caboose, Poky Little Puppy, Shy Little Kitten, and Scuffy up a hill and over a bridge on the way to Jolly Jungle, he seems to be missing the first pair of wheels under his cowcatcher, making him look like a 2-4-2 steam locomotive.

==Crew==
* Story: Phil Harnage
* Animation Director: Shingo Kaneko
* Producer:  Andy Heyward
* Music: Haim Saban and Shuki Levy
* Lyrics: Phil Harnage
* Director: Al Zegler

==Songs==
This special featured three songs as listed below:
*"Little Golden Book Land theme"
*"Get Up And Go"
*"Friends To The End"

==Aftermath==
Little Golden Book Land was released on video in the 1990s. Shortly following the special, a series of new Little Golden Books featuring the characters were written, based on the film:

* Welcome to Little Golden Book Land
* Tootle and Katy Caboose: A Special Treasure
* Poky Little Puppys Special Day
* Shy Little Kittens Secret Place
* Tawny Scrawny Lion Saves the Day
* Saggy Baggy Elephant: No Place for Me
* Scuffys Underground Adventure
* Baby Brown Bears Big Bellyache

==External links==
* 

 
 
 
 
 