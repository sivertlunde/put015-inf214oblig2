Night of the Living Dead: 30th Anniversary Edition
 
{{Infobox film
|name=Night of the Living Dead: 30th Anniversary Edition
|image=Night of the Living Dead 30th Anniversary Edition.jpg
|border=yes
|caption=DVD Cover
|director=John A. Russo
|producer=John A. Russo Bill Hinzman Karl Hardman Russell Streiner
|writer=George A. Romero John A. Russo
|starring={{Plain list |
Bill Hinzman 
Scott Vladimir Licina 
Grant Cramer 
Adam Knox 
Debbie Rochon 
Heidi Hinzman 
Scott Kerschbaumer 
George Drennen
}}
|cinematography=Bill Hinzman
|music=Scott Vladimir Licina
|editing=Bill Hinzman John A. Russo
|studio=Market Square Productions Westwood Artists International Inc.
|distributor=Anchor Bay Entertainment
|released= 
|runtime=93 minutes
|country=United States
|language=English
|budget=no info
|gross=no info
}}
Night of the Living Dead: 30th Anniversary Edition is a 1999 recompiled version of George A. Romeros Night of the Living Dead with all new features by John A. Russo such as a new story, footage and soundtrack. Night of the Living Dead: 30th Anniversary Edition (DVD, 1999),  .          

==Plot==
 
Following the execution of a convicted child rapist and murderer, drivers Dan and Mike are in the process of delivering the corpse to the county cemetery for anonymous burial.  After brief words by Reverend Hicks, Dan and Mike notice that the corpse appears to be twitching and possibly even moving.  They are suddenly attacked by the reanimated corpse, prompting them to flee to Beekmans Diner.  Meanwhile,  Barbra (Judith ODea) and Johnny (Russell Streiner) drive to rural Pennsylvania for an annual visit to their fathers grave, at their mothers request. Noticing Barbras discomfort, Johnny teases, "Theyre coming to get you, Barbra", before she is attacked by a strange man (Bill Hinzman), the same reanimated corpse that Dan and Mike were in the process of burying. Johnny tries to rescue his sister, but falls and bashes his head on a gravestone, killing himself. Barbra flees by car but almost instantly crashes into a tree. With the man in pursuit, she flees and eventually arrives at a seemingly abandoned farmhouse where, to her horror, she discovers a womans mangled corpse. Running out of the house, she is confronted by strange menacing figures like the man in the graveyard. Ben (Duane Jones) arrives in a truck and takes her back inside the house. Barbra slowly descends into shock as Ben drives the monsters from the house and begins boarding up the doors and windows.

Ben and Barbra are unaware that the farmhouse has a cellar, which is housing angry married couple Harry (Karl Hardman) and Helen Cooper (Marilyn Eastman) and their daughter Karen (Kyra Schon), who sought refuge after a group of zombies overturned their car; and teenage couple Tom (Keith Wayne) and Judy (Judith Ridley), who arrived after hearing an emergency broadcast about a series of brutal murders. Karen has fallen seriously ill after being bitten on the arm by one of the zombies. They venture upstairs and tensions immediately rise between Ben and Harry.  Meanwhile, the situation outside worsens, as we see hordes of hungry reanimated corpses wandering the roads and fields.  Among the recently dead are a family of 4, all of whom perished in a car wreck.  The mother and two daughters reanimate, and descend upon the corpse of the father (whose head was crushed in the initial impact, thus preventing reanimation) and devour him.

Radio reports explain that a state of mass murder is sweeping across the eastern United States. When Ben finds a television, the emergency broadcaster reports that the recently deceased have become reanimated and are consuming the flesh of the living. Experts, scientists, and the United States military fail to discover the cause, though one scientist suspects radioactive contamination from a space probe returning from Venus, which was deliberately exploded in the Earths atmosphere when the radiation was detected.  When the reports list local rescue centers offering refuge and safety, Ben plans to leave and obtain medical care for Karen. Tom states that the closest center is in the town of Willard, several miles away. Ben and Tom venture outside to refuel Bens truck, while Harry hurls Molotov cocktails from an upper window to keep the ghouls at bay. Fearing for Toms safety, Judy follows him. At the pump, Tom accidentally spills gasoline on the truck, setting it ablaze. Tom and Judy try to drive the truck away from the pump, but Judy gets her jacket caught in the car door and is unable to free herself. The truck explodes, instantly killing both Tom and Judy and setting their corpses on fire.

Ben flees back to the house, but finds himself locked out by Harry. He pounds on the door and shouts without result, finally kicking the door in. Angered by Harrys cowardice, Ben issues him a sound beating, while the ghouls feed on the remains of Tom and Judy. In the house, a news report reveals that, aside from setting the "reactivated bodies" on fire, a gunshot or heavy blow to the head will stop them, and that posses of armed men are patrolling the countryside to restore order.

Moments later, the lights go out and the ghouls begin to break through the barricades. Harry grabs Bens rifle and threatens to shoot him, but Ben wrests the gun away and fires. Mortally wounded, Harry stumbles into the cellar and collapses next to Karen, who has also died from her illness. The ghouls try to pull Helen and Barbra through the windows, but Helen frees herself and goes down into the cellar, only to find a reanimated Karen eating Harry. Helen, paralyzed by shock, falls as Karen stabs her to death with a masonry trowel. Barbra, seeing Johnny among the ghouls, is carried away by the horde and likely devoured. The undead overrun the house, and Ben fights off Karen as he seals himself inside the cellar. He finds Harry and Helen starting to reanimate and shoots them.

The next morning, Ben is awakened by gunshots as sheriffs department deputies move through the fields, shooting all the zombies they find. We see that Reverend Hicks has encountered the "original zombie" and as he tries to condemn it by reciting scripture, the zombie falls upon him, biting his face before it is killed by the armed posse.  Meanwhile, venturing upstairs, Ben is killed by a member of the posse, who seemingly mistook him for a ghoul.

In the final scenes of the film, a year passes and the crisis is averted.  We find Reverend Hicks is being kept in solitary quarters under constant observation, still alive.  He reveals that the parents of the little girl that was murdered by the "cemetery zombie"(prior to his execution), found Reverend Hicks and took him in.  Between caring for the wound and reciting endless prayer, Reverend Hicks manages to survive the zombie bite.  Declaring it to be divine intervention by God, the reverend delivers an ominous statement to the reporter who had come to interview him, and seems to disgust her to the point of storming out when he says that the only reason he is allowed to keep his pet dog is because it will provide a distraction for him should he turn into a ghoul, allowing his captors to dispose of him without risking human life.

==Cast==
*Bill Hinzman|S. William Hinzman as Zombie
*Scott Vladimir Licina as Reverend Hicks
*Grant Cramer as Dan
*Adam Knox as Mike
*Debbie Rochon as Darlene Davis
*Heidi Hinzman as Rosie
*Scott Kerschbaumer as Prison Guard
*George Drennen as Arthur Krantz
*Julie Wallace Deklavon as Hilda Krantz Night of the Living Dead (1968))

==New Features==
15 minutes of new footage produced and shot by three original creators John A. Russo, Bill Hinzman and Russell Streiner. This edition also features an all new musical score by Scott Vladimir Licina and a scene from Bill Hinzmans feature film Flesheater at the films conclusion. 

==Release==
On the morning of 24 August 1999 Night of the Living Dead: 30th Anniversary Edition was released in the United States for VHS and DVD.  

==Reception==
 
Critical reception to the 30th anniversary treatment has largely been negative. While the picture quality is often praised, the newly added sequences and characters are met mostly with displeasure by fans.  The character of Reverend Hicks, portrayed by the composer of the films new score Scott Vladimir Licina, has been considered by some to be one of the worst horror movie characters of all time.  Acting in the new sequences is considered to be terrible, and the pacing of the original film is damaged by the superfluous added footage.   Others view the inclusion of Bill Hinzman reprising his role as the Cemetery Zombie to be awkward, as the clear age difference, makeup and costume design between his original portrayal and the new footage is blatant and obvious. 

Contrast to criticism, others  praise the film for its new score and definite conclusion, which shows that the original outbreak was in fact contained and eradicated.  Additionally, the documentary content and extra features were all well received by fans of the original film. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 