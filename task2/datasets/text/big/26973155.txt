Ummadi Kutumbam
{{Infobox film
| name           = Ummadi Kutumbam
| image          =
| image_size     =
| caption        =
| director       = D. Yoganand
| producer       = N. Trivikrama Rao
| writer         =
| narrator       = Krishna Kumari Savitri
| music          = T. V. Raju
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 1967
| runtime        =
| country        = India Telugu
| budget         =
}}

Ummadi Kutumbam (  directed by D. Yoganand, and produced by National art theater, starring N. T. Rama Rao, the film was selected by Film Federation of India as one of its entries to the 1968 Moscow Film Festival. Eenadu Daily, Eenadu cinema – 17 July 2013, National art theater, Page 10  

==Cast==
* Nandamuri Taraka Rama Rao
* Kaikala Satyanarayana Krishna Kumari
* M. Prabhakar Reddy Savitri
* Vanisree
* Relangi Venkata Ramaiah
* Mukkamala Krishna Murthy Raja Babu
* Allu Rama Lingaiah

==Soundtrack==

* Bhale Mojuga Tayaraina (Singers: P. Susheela and Ghantasala)
* Cheppalani Undi Devathaye Digivachi Manushullo Kalisina Katha (Singer: Ghantasala Venkateswara Rao and P. Susheela; Cast: N. T. Rama Rao and Krishna Kumari)
* Hello My Dear Chalo (Singers: Ghantasala and L. R. Eswari)
* Jigi Jigi Jigelumannadi Chinnadi (Singer: L. R. Eswari)
* Kutumbam Ummadi Kutumbam (Singers: Ghantasala and P. Leela)
* Sadivinodikanna Oranna (Singers: L. R. Eshwari and Madhavapeddi Satyam group)
* Tassadiyya Tassadiyya (Singer: Ghantasala)
* Lanka Dahanam drama (Singers: Ghantasala, N.T. Rama Rao and T. Tilakam)
* Sati Savitri drama (Singers: Ghantasala, N.T. Rama Rao and T. Tilakam)

==Box office==
* The film celebrated Silver Jubilee and ran for 197 days at Durga Kala Mandir, Vijayawada. 

==References==
 

==External links==
* 

 
 
 
 


 