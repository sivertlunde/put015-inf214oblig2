Yaariyan (2014 film)
 
 
{{Infobox film
| name              = Yaariyan
| image             = Yaariyan (2014 film) poster.jpg
| caption           = Theatrical release poster
| director          = Divya Khosla Kumar
| producer          = Bhushan Kumar Krishan Kumar
| writer            = Sanjeev Dutta
| starring          = Himansh Kohli Nicole Faria Rakul Preet Singh Shreyas Porus Pardiwalla Ankit Sharma 
| music             = Pritam Mithoon Anupam Amod Honey Singh Arko Pravo Mukherjee
| cinematography    = Sameer Arya
| editing          = Aarif Sheikh
| studio            = T-Series
| released          =  
| country           = India Hindi 
| budget            =   
| gross             =  
}}
 romance adventure film directed by Divya Khosla Kumar,  starring Himansh Kohli, Rakul Preet Singh and Nicole Faria in the lead roles.  It marks the debut of the director  as well as the lead star cast. The film was produced by Bhushan Kumar and Krishan Kumar under the banner of T-Series Super Cassettes Industries.

Yaariyan tells the story of 5 close friends who are  in college, experiencing different relationships and learning new values. The response to the promotional material on YouTube led the distributor to release the film two weeks earlier than initially planned.  The film released on 10 January 2014 with 1200 screens release in India.   

==Plot==
Living in a university campus set in Sikkim, Laksh (Himansh Kohli) is the son of a martyred army officer but does not appreciate his fathers sacrifice for the nation. Lakshya, Jiya (Nicole Faria) - a college bombshell, Saloni (Rakul Preet Singh) - a geek, Pardy - a drummer, and Neil - a biker, are five close friends who are exploring the best moments of their lives in college, experiencing different relationships yet living an aimless life. They come across a challenge when their college land has been bought by an Australian businessman who plans to build a casino instead but is ready to lease out the land to the college for 100&nbsp;years only if theyre able to defeat a team of Australian students in a five-round competition.

For the first three rounds they are sent to Australia where Lakshya meets his best friend and cousin Debu. During the first round of rock concert, the Australian band steals Lakshya and his bands song, and on protest they brutally beat Debu. While Debu is rushed to hospital, Lakshya and the band perform a Hindi song, eventually losing the first round. Saloni then wins the second round which is a chess competition. Unable to recover from his injuries, Debu succumbs to death in the hospital. Disturbed by his death, Saloni and Neil lose the third round of bike race, giving Australia a 2-1 lead.

The competition is then shifted to India with two more rounds to go. Lakshya discovers that Neil was the traitor who gave their song to the Australian band and intentionally lost the bike race motivated by a greed of getting an Australian emigration as promised by an Australian team member. Neil is now planning to lose the cycle race as well, but in order to fail his plan, Lakshya befriends Jannet (Evelyn Sharma) and makes Neil believe that the Australians are double crossing him. Neil realizes his fault and decides to win the race, but fatefully falls from stairs breaking his leg. Lakshya takes his place and beats Australia in round four leading to a tie between both teams. In the final round of rock climbing, a team member of both teams has to collect his respective countrys flag from the hill top and race back to college in order to win the competition. After crossing all the hurdles, Lakshya manages to win the race and the college celebrates their victory. The story ends with Lakshya and Saloni, Jiya and Pardy getting together while Neil repents his deeds.

==Cast==
*Himansh Kohli as Lakshya
*Rakul Preet Singh as Saloni
*Nicole Faria as Jiya
*Shreyas Porus Pardiwalla as Pardy
*Dev Sharma  as Neil
*Jatin Suri as Debu
*Serah Singh as Jenny
*Evelyn Sharma as Jannet
*Sayali Bhagat as Nikki
*Deepti Naval as Girl Hostel Warden
*Smita Jaykar as Lakshyas mother
*Gulshan Grover as College Principal
*Hemant Pandey as Salonis father
*Manish Kumar as Jennys boyfriend
* Yo Yo Honey Singh as Batbelly

==Production==

===Development===
Early 2012, Bhushan Kumar announced making a film in college romance genre and that Divya Khosla Kumar would be the director.  Soon, an announcement was released that the cast of the film will include new comers Himansh Kohli    Rakul Preet Singh     Nicole Faria  and Evelyn Sharma  as well as Gulshan Grover    and Deepti Naval.   

The title of the movie was registered with Saif Ali Khan   

The production budget of film is     

===Filming===
The shoot began in Mumbai    followed by shoots in Australia and then Sikkim and Darjeeling (St Joseph’s school, Darjeeling).   
The film was also partially shot in Cape Town.

==Critical reception==
The film received mixed reviews. Madhureeta Mukherjee from The Times of India gave the movie 3 stars stating "Divyas debut film Yaariyan is mounted on a large canvas with impressive production value." 
Prithvi KC of Vellore Times has stated that the movie wasnt upto the hype created.Saibal Chatterjee for NDTV Movies gave the film 1 star out of 5, stating that Yaariyan "makes no sense at all".  Paloma Sharma for Rediff gave the movie 0.5 out of 5 stars and wrote: "Yaariyan may be targeted at a young audience but every single teenager who chooses to watch it will be insulting their own intelligence". 
Madhureeta Mukherjee from Times of India  gave the film 2 stars out of 5. She stated that "Yaariyan is nothing to gush about, but the teenies can watch this one for a lark...and some yo-yo beats!". 
Taran Adarsh for Bollywood Hungama gave the film 4 stars out of 5 and stated that Yaariyan is "A treat for youngsters and young at heart". "On the whole, Yaariyan  has a gripping second half, smash hit musical score and the youthful romance that should lure and entice its target audience.".  Tushar Joshi for DNA India praised the music of the film, and gave the film 2 stars out of 5 stating that the film is "strictly for those who want to enjoy the songs on the big screen".    Mohar Basu for Koimoi gave the film 0 stars out of 5 stating that "there is no valid reason for you to watch Divya Khosla Kumar’s Yaariyan. It is dull, boring, pathetic and doesn’t seem to get over it". 

==Box Office==

===India===
Yaariyan had a good first weekend  of around Rs 16.50&nbsp;crore nett    Yaariyan grossed around Rs  2.50&nbsp;crore nett on first Monday taking its four day total to 19&nbsp;crore nett.    Yaariyan had the same figure as Monday as it collected 2.50&nbsp;crore nett taking its five day total to Rs 21.50&nbsp;crore nett     Yaariyan collected around 2.75&nbsp;crore nett in second weekend.    Made on a budget of 10 crores. It collected 40 crores.   

===Overseas===
Yaariyan was screened in the UAE and had a limited release.     

==Soundtrack==
{{Infobox album  
| Name       = Yaariyan
| Type       = Soundtrack
| Artist     =  
| Cover      = Original Soundtrack - Yaariyan.jpg
| Alt        = 
| Released   =  
| Recorded   = 2013 Feature Film Soundtrack
| Length     =  
| Label      = T-Series
| Producer   = Bhushan Kumar
| Chronology = Pritam Chakraborty
| Last album = Dhoom 3 (2013)
| This album = Yaariyan   (2014)
| Next album = Shaadi Ke Side Effects   (2014)
 {{Extra chronology
 | Artist        = Mithoon
 | Type          = Soundtrack
 | Last album    = Aashiqui 2 (2013)
 | This album    = Yaariyan (2014)
 | Next album    = Samrat & Co. (2014)
 }}
 {{Extra chronology
 | Artist        = Yo Yo Honey Singh
 | Type          = Soundtrack
 | Last album    = Boss (2013 Hindi film)|Boss (2013)
 | This album    = Yaariyan (2014)
 | Next album    = Ragini MMS 2 (2014)
 }}
 {{Extra chronology
 | Artist        = Arko Pravo Mukherjee
 | Type          = Soundtrack
 | Last album    = Jism 2 (2012)
 | This album    = Yaariyan (2014)
 | Next album    = Hate Story 2 (2014)
 }}
 {{Extra chronology
 | Artist        = Anupam Amod
 | Type          = Soundtrack
 | Last album    = Once Upon Ay Time in Mumbai Dobaara! (2013)
 | This album    = Yaariyan (2014)
 | Next album    = 
 }}
 {{Singles
 | Name = Yaariyan
 | Type = Soundtrack Singles
 | Single 1 = ABCD - Yaariyan
 | Single 1 date =  
 | Single 2 = Baarish - Yaariyan
 | Single 2 date =  
 | Single 3 = Sunny Sunny
 | Single 3 date =  
 }}
}}
The soundtrack for the film was composed by Pritam. There is also a special number by Honey Singh titled Sunny Sunny. Mithoon rendered tracks for this film as well. Arko Pravo Mukherjee also composed a track. First full track ABCD was released on 25 November 2013. Baarish was released on 29 November 2013. It crossed 1&nbsp;million views on YouTube in 2&nbsp;days. Audio album of the movie was released on 9 December 2013 on internet. Song Allah Waariyan was released on 11 December 2013. The song is sung by Shafqat Amanat Ali, and written as well as composed by Arko Pravo Mukherjee. 

===Track listing===
{{Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| music_credits = yes
| total_length =  
| title1 = ABCD - Yaariyan 
| extra1 = Yo Yo Honey Singh, Benny Dayal, Shefali Alvares
| music1 = Pritam
| lyrics1 = Amitabh Bhattacharya
| length1 = 3:25
| title2 = Baarish - Yaariyan  Mohammed Irfan, Gajendra Verma
| music2 = Mithoon
| lyrics2 = Mithoon
| length2 = 6:14
| title3 = Sunny Sunny 
| extra3 = Yo Yo Honey Singh, Neha Kakkar
| music3 = Yo Yo Honey Singh
| lyrics3 = Yo Yo Honey Singh
| length3 = 4:03
| title4 = Allah Waariyan
| extra4 = Shafqat Amanat Ali
| music4 = Arko Pravo Mukherjee
| lyrics4 = Arko Pravo Mukherjee
| length4 = 5:14
| title5 = Love Me Thoda Aur 
| extra5 = Arijit Singh, Monali Thakur
| music5 = Anupam Amod
| lyrics5 = Irshad Kamil
| length5 = 4:25
| title6 = Meri Maa  KK
| music6 = Pritam
| lyrics6 = Irshad Kamil
| length6 = 5:17
| title7 = Zor Lagaake Haisha 
| extra7 = Vishal Dadlani
| music7 = Arko Pravo Mukherjee
| lyrics7 = Irshad Kamil
| length7 = 5:59
| title8 = Mujhe Ishq Se
| extra8 = Gajendra Verma, Tulsi Kumar
| music8 = Mithoon
| lyrics8 = Mithoon
| length8 = 5:44
| title9 = Baarish - Yaariyan (Remix)
| note9 = Remixed by: DJ Shiva
| extra9 = Mohammed Irfan, Gajendra Verma
| music9 = Mithoon
| lyrics9 = Mithoon
| length9 = 5:07
| title10 = Meri Maa (Reprise)
| extra10 = KK
| music10 = Pritam
| lyrics10 Irshad Kamil
| length10 = 4:43
| title11 = ABCD - Yaariyan (Remix)
| note11 = Remixed by: Kiran Kamath
| extra11 = Yo Yo Honey Singh, Benny Dayal, Shefali Alvares
| music11 = Pritam
| lyrics11 = Amitabh Bhattacharya
| length11 = 3:19
| title12 = Meri Maa (Unplugged)
| extra12 = Anupam Amod
| music12 = Pritam

| lyrics12 = Irshad Kamil
| length12 = 5:17
| title13 = Yaariyan Mashup
| note13 = Remixed by: Kiran Kamath
| extra13 = Benny Dayal, Shefali Alvares, Mohammed Irfan, Gajendra Verma, Yo Yo Honey Singh, Neha Kakkar, Shafqat Amanat Ali, Arijit Singh, Monali Thakur, KK, Vishal Dadlani, Tulsi Kumar, Anupam Amod
| music13 = Pritam, Mithoon, Yo Yo Honey Singh, Arko Pravo Mukherjee, Anupam Amod
| lyrics13 = Amitabh Bhattacharya, Mithoon, Yo Yo Honey Singh, Arko Pravo Mukherjee, Irshad Kamil
| length13 = 3:43
}}

==See also==
*Bollywood films of 2014

==References==
 

==External links==
* 

 
 
 
 
 
 