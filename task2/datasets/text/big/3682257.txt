Murder on Flight 502
{{Infobox film
| name           = Murder on Flight 502
| image          =Murder on Flight 502.JPG
| image_size     =200px
| caption        =Theatrical poster
| director       = George McCowan
| producer       = David Chasman (producer) J. Bret Garwood (associate producer) Leonard Goldberg (executive producer) Aaron Spelling (executive producer)
| writer         = David P. Harmon  Farrah Fawcett-Majors
| music          = Laurence Rosenthal
| cinematography = Archie R. Dalzell
| editing        = Allan Jacobs
| studio         =  Spelling-Goldberg Productions
| distributor    = American Broadcasting Company (ABC)
| released       =   (television broadcast premiere)
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Farrah Fawcett-Majors, along with an all-star ensemble television cast in supporting roles.

==Plot==
Flight 502 takes off from New York City to London. At the airport, a fake bomb threat leads to Head of Security Robert Davenport (George Maharis) finding a letter he would not have received until the next morning. The letter explains a series of murders will take place on Flight 502 before it lands. He and his team go over the backgrounds of all the passengers to find possible suspects. In the air, Captain Larkin (Robert Stack), off duty Police Officer Daniel Myerson (Hugh OBrian), and flight attendant Karen White (Farrah Fawcett-Majors) look for suspicious passengers. Relationships develop on board between elderly singles Charlie Parkins (Walter Pidgeon) and Ida Goldman (Molly Picon), rock star Jack Marshall (Sonny Bono) and Dorothy Saunders (Rosemarie Stack), and mystery writer Mona Briarly (Polly Bergen) and suave passenger Paul Barons (Fernando Lamas).  Briarly suspects Barons is actually a criminal who got away with stealing seven million dollars from a bank, but Barons denies it. 

The investigations on the ground and in the air produce several leads. It is discovered that wife of passenger Otto Gruenwaldt (Theodore Bikel) died because fellow passenger Dr. Kenyon Walker (Ralph Bellamy) was not available to help. But Gruenwaldt suffers a heart attack on board and is saved by Dr. Walker. Ray Garwood (Dane Clark) attacks Marshall, blaming him for the death of his daughter due to an overdose. Garwood denies leaving the note, and Captain Larkin and Myerson believe him.

A break in the investigation comes when Briarly tells the captain that a priest on board may be an imposter, because he did nothing when it appeared Gruenwaldt was near death. Donaldson checks the priest out and discovers he is indeed an imposter and a known thief. Myerson looks for the priest, but finds him dead in the dumbwaiter. At this point the notes contents are revealed to the passengers. Briarly again notices that Barons seems the most fazed by the priests death, and wonders if the two men knew each other. Soon a second murder occurs; flight attendant Vera Franklin is found dead by the co-pilot. At this point Barons confesses to Myerson that he committed the bank robbery, and that the priest and Franklin were both involved in smuggling the money out of the country on the aircraft. Barons says he is the next target. Myerson agrees and pulls out a gun, proving he is the killer, having snapped when Barons escaped justice for his crime after no proof was found. 

Myerson takes the passengers hostage and explains he murdered the priest and looked through the luggage of the crew and found the money in Franklins bag. Captain Larkin makes a drastic move to distract Myerson by releasing the oxygen masks and going for the gun. In the ensuing struggle, Barons is killed, the cabin catches on fire, and Myerson is badly burned. The passengers extinguish the fire and the aircraft lands safely in London. Just as the crisis ends, Donaldson calls to warn Larkin about Myerson. 

On the ground, Garwood apologizes to Marshall, Dr. Walker and Gruenwaldt reconcile, and Charlie Perkins and Ida Goldman decide to share a hotel room. Flight attendant White thanks Captain Larkin for saving the passengers but Larkin reveals that before the aircraft took off Franklin had dropped her bag, which had no money inside. Therefore the money was put in the bag by the real guilty smuggler, Karen White. Larkin hands White and Myerson to the British police. Myerson is taken away claiming he brought a thief to justice when the law would not, and will be commended for protecting the people from criminals.

==Cast==
Principal cast listed alphabetically:
   
* Ralph Bellamy as Dr. Kenyon Walker
* Polly Bergen as Mona Briarly
* Theodore Bikel as Otto Gruenwaldt
* Sonny Bono as Jack Marshall
* Dane Clark as Ray Garwood
* Laraine Day as Claire Garwood
* Fernando Lamas as Paul Barons
* George Maharis as Robert Davenport Farrah Fawcett-Majors as Karen White
* Hugh OBrian as Detective Daniel Myerson
* Molly Picon as Ida Goldman
* Walter Pidgeon as Charlie Parkins
* Robert Stack as Captain Larkin Brooke Adams as Vera Franklin
 
* Danny Bonaduce as Millard Kensington
* Vincent Baggetta as Fred Connors Rosemarie Stack as Dorothy Saunders  
* Elizabeth Stack as Marilyn Stonehurst  
* Steve Franken as Donald Goldman
* Philip Sterling as Benny Cummings
* Pepper Martin as Bomb Man
* Yolanda Galardo as Alice Quincy
* Bob Hachman as Operations Man
* Don Hanmer as Priest
* Glorie Haufman as Ticket Taker
* Byron Morrow as Diplomat
* George O. Petrie as Ferguson
* Dave Shelley as Harold
 

==Production==
  New York, but the opening title credits and first scenes are shot at Los Angeles International Airport. The "tacky production values" were evident throughout.  

Robert Stack, then mainly shooting in Europe, was still a television icon and was in demand for the movie-of-the-week features that were common in the 1970s.  In later interviews, Stack revealed that one of the prime incentives to sign on for Murder on Flight 502 was the opportunity to work with his wife, Rosemarie and daughter Elizabeth. 

==Reception==
Reviewer Keith Bailey considered Murder on Flight 502 as typical of the 1970s disaster film. "The 70s was the era of the 90-minute (including commercials) TV movie, unlike this one; had this movie been cut down to fit a 90-minute slot, I am sure it would have been a definite improvement." Bailey considered that the film didnt work as a murder mystery but could have worked as a character study.  Bailey, Keith.   The Unknown Movies,  2014. Retrieved: December 9, 2014. 

==References==
===Notes===
 

===Citations===
 

===Bibliography===
 
* Maltin, Leonard.  Leonard Maltins Movie Encyclopedia. New York: Dutton, 1994. ISBN 978-0-45227-058-9.
* Stack, Robert and Mark Evans. Straight Shooting. Amsterdam, New York: Xs Books, 1980. ISBN 978-0-02613-320-3.
* Weaver, Tom. A Sci-Fi Swarm and Horror Horde: Interviews with 62 Filmmakers.  Jefferson, North Carolina: McFarland & Company, 2010. ISBN 978-0-78644-658-2.
 
==External links==
*  
*  

 
 
 
 
 
 
 
 