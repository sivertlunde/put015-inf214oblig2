What Would Jesus Buy?
{{Infobox film
| name           = What Would Jesus Buy?
| image          = What would jesus buy ver2.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Rob VanAlkemade
| producer       = Morgan Spurlock, Peter Hutchison, Stacey Offman
| writer         =
| narrator       =
| starring       = Bill Talen
| music          = Steve Horowitz, William Moses
| cinematography =
| editing        =
| distributor    = Warrior Poets 2007
| runtime        = 90 min.
| country        = United States English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
What Would Jesus Buy?  is 2007 a documentary film produced by Morgan Spurlock and directed by  Rob VanAlkemade. The title is a take-off on the phrase "What would Jesus do?" The film debuted on the festival circuit on March 11, 2007, at the South By Southwest (SXSW) conference in Austin, Texas. It went into general US release on November 16, 2007. 

== Plot ==
The film focuses on the issues of the commercialization of Christmas, economic materialism|materialism, the over-consumption in American culture, globalization, and the business practices of large corporations, as well as their economic and cultural effects on American society, as seen through the prism of activist/performance artist Bill Talen, who goes by the alias of "Reverend Billy", and his troupe of activists, whose street theater performances take the form of a church choir called "The Church of Stop Shopping," that sings anti-shopping and anti-corporate songs.  The film follows Billy and his choir as they take a cross-country trip in the month prior to Christmas 2005, and spread their message against what they perceive as the evils of patronizing the retail outlets of several different large corporate chains.

== Crew ==
The film was produced by Morgan Spurlock, with cinematography by Alan Deutsch, Daniel Marracino, Martin Palafox, Alex Stikich and Rob VanAlkemade. Jeremy Osbern was an additional cinematographer and Michael Moore, Dietmar Post, Jon Shenk, and Martin Taylor were camera operators. The film was edited by Gavin Coleman and Stela Georgieva.

==Cast==
*  Adetola Abiade	...	Alto
*  Paul Allen	...	Tenor
*  Paul Norman Allen	...	Paul
*  Paul Norman Allen	...	Tenor
*  Shannon Baxter	...	Soprano
*  Rick Becker	...	Trombone
*  James Solomon Benn	...	Choir Director / Choreographer
*  Reverend Billy	...	Reverend Billy
*  Ben Cerf	...	Bass
*  Misun Choi	...	Soprano
*  Ben Dubin-Thaler	...	Bass
*  Savitri Durkee	...	Church Director
*  Leah Farrell	...	Tenor
*  Gina Figueroa	...	Alto
*  Mike Flthye	...	Drums

==See also==
*Buy Nothing Christmas
*Buy Nothing Day
*Festivus

== References ==
 

== External links ==
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 