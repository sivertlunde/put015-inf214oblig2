Maine Gandhi Ko Nahin Mara
{{Infobox film
| name           = Maine Gandhi Ko Nahin Mara
| image          = MGKNM.jpg
| caption        = DVD cover
| director       = Jahnu Barua
| producer       = Anupam Kher Sanjay Chauhan 
| starring       = Anupam Kher Urmila Matondkar Rajit Kapur Parvin Dabas
| music          = Bappi Lahiri
| cinematography = Raaj Chakravarti
| editing        = Deepa Bhatia
| distributor    =
| released       = 30 September 2005
| runtime        =
| language       = Hindi
| budget         = Rs. 20 million
| gross          = Rs. 3.9 million
| preceded by    =
}}

Maine Gandhi Ko Nahin Mara (translated as I Did Not Kill Gandhi) is a 2005 Indian film, directed by Jahnu Barua and produced by Anupam Kher. The film stars Anupam Kher and Urmila Matondkar in lead roles.

In the film, a poem by noted Hindi poet, Harivansh Rai Bachchan|Sr. Bachchan, Himmat Karne Walon Ki Har Nahi Hoti (Those who have courage never fail) was depicted as the favorite poem of the main character  

==Plot summary==
The film explores the downward spiral of a retired Hindi professor,  Uttam Chaudhary, (portrayed by Anupam Kher) as he falls victim to dementia. After he sees someone carelessly place an ash tray on a newspaper photograph of Mohandas Gandhi, his senility increases. One night his daughter Trisha, played by Urmila Matondkar, and son Karan discover his room on fire. Trisha takes him to a doctor who says nothing can be done.

Then Uttam believes he killed Gandhi by accidentally playing with a toy gun which had real bullets and shooting Gandhi during his walk in Birla House. So they go see Uttams brother for details. Uttams brother says that when they were young, they played darts by filling ballons with red dye and placing it on someones picture.

One day someone found Gandhis picture and Uttam popped a ballon while their father saw who believed he killed Gandhi, with Uttam replying "Maine Gandhi Ko Nahin Mara" while his father hit him. Later they go to another doctor named Siddharth (Parvin Dabas) who helps Uttam when he thinks that his house is jail and people poisoned his food because he killed Gandhi. Siddharth eats the food so Uttam knows the food is not poisoned. Later,in court, a gun expert says that a toy gun (which Uttam believes he killed Gandhi with) cannot kill anyone.

==Response== Special Jury National Film Awards. He also received awards at several international film festivals.   Matondkar was also much appreciated for playing the caring daughter of Kher and won the Bollywood Movie Award - Best Actress for her performance. 

==Cast==
*Anupam Kher as Professor Uttam Chaudhary
*Urmila Matondkar as Trisha Chaudhary
*Rajit Kapur as Ronu Chaudhary
*Parvin Dabas as Dr. Siddarth Katari
*Prem Chopra
*Waheeda Rahman
*Boman Irani
*Sudhir Joshi
*Raju Kher
*Vishwaas Paandya
*Nazneen Ghaani
* Danica Apolline as Charity Worker

==See also==
*List of artistic depictions of Mohandas Karamchand Gandhi

==References==
 

==External links==
* 
* 
* 
 

 
 
 
 