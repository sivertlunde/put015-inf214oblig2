Lonesome Dove (miniseries)
{{Infobox television 
  | name           = Lonesome Dove
  | image          =  
  | caption        = 2001 DVD cover
  | director       = Simon Wincer
  | producer       = Dyson Lovell Suzanne De Passe
  | writer         = Larry McMurtry (novel) William D. Wittliff (teleplay)
  | narrator       = 
  | starring       = Robert Duvall Tommy Lee Jones Danny Glover Diane Lane Anjelica Huston
  | composer       = Basil Poledouris
  | cinematography = Douglas Milsome
  | editing        = Corky Ehlers
  | distributor    = Motown Productions Pangaea Qintex Entertainment
  | country        = USA
  | language       = English
  | network        = CBS
  | first_aired =  
  | last_aired =  
  | num_episodes = 4
  | runtime        = 384 min. 
  | budget         = 
  | gross          =  Streets of Comanche Moon
}}
 epic Western Western adventure adventure television adaptation of novel of the same name by Larry McMurtry and is the first installment in the Lonesome Dove series|Lonesome Dove franchise. The series stars Robert Duvall and Tommy Lee Jones. The series was originally broadcast by CBS over four nights in February 1989, drawing a huge viewing audience, earning numerous awards, and reviving both the television western and the miniseries.
 War and Remembrance, won the Emmy for Outstanding Miniseries. Yet, Lonesome Dove found success later, when it won two Golden Globes, for Best Miniseries and Best Actor in a Miniseries (Robert Duvall).

==Plot== Texas Rangers, livery in Tim Scott), another former Ranger who works hard but isnt very bright, and Bolívar (León Singer), a retired Mexican bandit who is their cook. Also living with them is Newt Dobbs (Ricky Schroder), a 17-year-old whose mother was a prostitute named Maggie and whose father may be Call.

===Part I: Leaving=== Texas Ranger and comrade of Gus and Calls, shows up after an absence of more than ten years. He reveals that he is a man on the run, a fugitive, after having accidentally shot the dentist and mayor of Fort Smith, Arkansas in a bar-room gunfight. The dentist/mayors brother happens to be the sheriff, July Johnson (Chris Cooper).

Reunited with Gus and Call, Jakes glowing description of Montana inspires Call to gather a herd of cattle and drive them there, attracted by the notion of settling pristine country. Gus is less enthusiastic, pointing out that they are getting old and that they are Rangers and traders, not cowboys. But he changes his mind when he realises Lonesome Dove has little left to offer him by way of excitement, now that much of the land has been "civilized".

At the continued insistence of the dentists widow, Sheriff Johnson sets off in pursuit of Spoon, accompanied by his young stepson Joe who travels at the request of Joes mother and Sheriff Johnsons wife Elmira (Glenne Headly). Once her son and husband have left for Texas the pregnant Elmira leaves Fort Smith for Ogallala, Nebraska to meet up with her first husband and Joes father, Dee Boot. En route to Ogallala upstream, she soon falls in with a group of buffalo hunters.

Meanwhile, the men of Lonesome Dove make preparations for their adventure north, including stealing 2,500 horses and cattle from across the Rio Grande in Mexico, befriending two lost Irish immigrants, Allan and Sean OBrien, and being joined by nearly all of the male citizens of the town.  Before leaving, Gus returns to fetch his livery sign and say farewell to his pigs, who end up following him anyway.  Jake decides not to travel with the herd, mainly because he promises to take the towns only prostitute, Lorena "Lorie" Wood (Diane Lane), to San Francisco via Denver.  Some time later the group survive a huge dust-storm, but Sean (Bradley Gregg), one of the Irishmen is bitten by water moccasins while crossing the Nueces River.

===Part II: On the Trail=== Kansas Plains with two hunters she has befriended.

Meanwhile, the camps cook refuses to continue, and Gus and Call head into San Antonio in search of a new cook, Po Campo (Jorge Martínez de Hoyos). On the way back, they catch up with Lorie, who Jake has abandoned in order to go gambling in Austin, Texas|Austin. Before he returns, Gus and Lorie encounter Blue Duck (Frederic Forrest), a notorious Mexican/Indian bandit from Gus and Calls Ranger days. Blue Duck knocks Newt unconscious, kidnaps Lorie, and attempts to sell/barter her to a gang of Comanchero bandits camped on the Llano Estacado.

Knowing that Gus is in pursuit, Blue Duck asks the Comanchero bandit gang to kill Gus when he arrives - the reward of doing so being Lorie. Gus and the bandits engage in a brief gun battle that quickly turns into a stalemate. Gus, having killed his horse, is pinned down by the bandits gunfire until nightfall, when Sheriff Johnsons party arrives and scares them off. Sheriff Johnson, despite Gus protests, joins Gus in the rescue of Lorie. The pair then ride to a hilltop above the Comancheros camp. After a brief one-sided gunfight, in which Gus kills all of Blue Ducks gang, Lorie is rescued.

But while Gus and Johnson are away, Blue Duck uses his knife to kill deputy Roscoe, Janey and stepson Joe. He then steals their horses and escapes. The tearful Sheriff, with the help of Gus, buries them all. Gus and Lorie ride north to rejoin Call and the herd. After being severely traumatized by her capture, Lorie now regards Gus as her primary protector. Meanwhile, in a saloon in Fort Worth, and oblivious of Lories ordeal, Jake Spoon falls in with a gang that are headed north to rob banks in Kansas.

===Part III: The Plains=== horse wranglers. posse to search for the thieves.  Gus and Call quickly capture the robbers and prepare to hang them. With his last words, Jake Spoon admits that it is better to be hanged by his friends than by strangers.
 Ogallala in search of Dee Boot. She finds Dee Boot in jail, where he is shortly hanged for a murder. Two weeks later, Sheriff Johnson also arrives at Claras house and sees his abandoned son.  Later in Ogallala Sheriff Johnson encounters Elmira, who is still recovering from childbirth. That night, Elmira secretly departs east for St. Louis with the two buffalo hunters, but all three are soon killed by the Sioux. Sheriff Johnson returns to Claras house and is offered a job. Clara, having lost her own three sons to pneumonia, is quite fond of Johnsons newborn son, and names him Martin.

Gus and Calls cattle drive also arrives at Ogallala, where they relax and enjoy the town. Some U.S. cavalry soldiers attempt to commandeer the groups horses, and beat Newt when he resists, sending Call into a murderous rage.  In the aftermath, Newt learns that Call is his father.  Clara, although happy to see Gus, and with her husband gravely ill, makes it clear that she will not marry Gus.  Instead, she invites Gus to settle and ranch on a piece of nearby property.  Further, she invites Lorie to remain with Clara and her daughters, and Gus promises he will return one day to them.

Continuing their journey, Gus and Call lead their cattle drive north through the badlands of Wyoming and into Montana Territory.  Impoverished Indians soon steal a dozen of their horses for food.  Gus, Call, and Deets ride after the horse thieves to retrieve the horses. Call frightens the Indians away with a gunshot.  Deets takes pity on a blind Indian child left behind, and goes to assist him.  Another Indian mistakes his intentions and impales Deets with a spear.  He is mortally wounded and dies in Guss and Calls arms a few moments later.

===Part IV: Return=== Powder River, Montana Territory.  Meanwhile, Claras husband finally dies and is buried as well.  Leaving the main group to scout ahead with Pea Eye Parker, Gus decides to pursue some buffalo, but is chased by mounted Indians and is badly wounded in his right leg. While trying to get back to the herd for help, an exhausted Pea Eye is guided by the ghost of Deets, whereas Gus is found by a stranger and taken 40 miles away to Miles City, Montana.

There a doctor amputates Gus right leg.  Gus knows that his left leg is septic and he is likely to die, but refuses to let the doctor remove it too.  Gus tells Call (who has come in search of him) to give his money to Lorie, to bury him in Texas, and to admit that Call is Newts father.  Call arranges to store Gus body in the town over the winter.  He then leads the cattle drive to a wilderness lake where the party raises a cabin and a corral.

Call honors Gus wish to be returned to Texas.  Just before departing, Call gives Newt a pocket-watch that belonged to his own father and states that Newt will run the ranch in his absence. The moment is filled with anticipation, but he is incapable of actually calling Newt his son out loud.  Call soon returns to Ogallala, Nebraska|Ogallala.

Sheriff Johnson, Clara, Lorie, and the ranch hand Dish live happily together.  Dish is enamored with Lorie, but she does not return his affections.  When Call brings Gus body, she stays and mourns by the coffin all night long.  Clara asks Call to bury Gus at her home, but Call declines.  Clara then berates Call for the bad effect he and Gus had on each other, blaming their adventures as the reason neither of them could find happiness.

After a long journey, Call arrives at Santa Rosa, New Mexico Territory, the town where Blue Duck has finally been captured.  Call visits Blue Duck in jail, where Blue Duck mocks Calls failure to capture him.  While being led to the gallows, Blue Duck grabs Robert Hofer and throws himself out a window, choosing a murder-suicide rather than allow himself to be hanged.

Despite blizzards, a broken wagon, and the loss of the coffin, Call finally succeeds in burying Gus after a journey of some three thousand miles.  Call weeps for his friend after burying him, the first display of emotion he has allowed himself since Deets death.  After the burial, Call tours Lonesome Dove and discovers that the saloon owner who once employed Lorie was so heartbroken by her departure that he burned down the saloon and killed himself.  As Call walks out of town, a reporter recognizes him and tries to interview him about his remarkable feats.  Call ignores the reporters questions, aside from ironically agreeing with him that he was a man of vision.  Call then walks away as the sun sets on Lonesome Dove.

==Cast==
{{columns-list|2|
* Robert Duvall as Captain Augustus "Gus" McCrae
* Tommy Lee Jones as Captain Woodrow F. Call
* Danny Glover as Joshua Deets
* Diane Lane as Lorena Wood
* Robert Urich as Jake Spoon
* Frederic Forrest as Blue Duck
* D. B. Sweeney as Dishwater "Dish" Boggett
* Ricky Schroder as Newt Dobbs
* Anjelica Huston as Clara Allen
* Chris Cooper as July Johnson Timothy Scott as Pea Eye Parker (credited as Tim Scott)
* Glenne Headly as Elmira Johnson
* Barry Corbin as Roscoe Brown
* William Sanderson as Lippy Jones
* Barry Tubb as Jasper Fant
* Gavin OHerlihy as Dan Suggs
* Steve Buscemi as Luke
* Frederick Coffin as Big Zwey
* Travis Swords as Allan OBrien
* Kevin OMorrison as Doctor
* Ron Weyand as Old Hugh
* Lanny Flaherty as Soupy Jones David Carpenter as Needle Nelson
* James McMurtry as Jimmy Rainey
* Charlie Haynie as Ben Rainey
* James Terry McIlvain as Cowboy
* Sonny Carl Davis as Bert Borum
* Helena Humann as Peach Johnson
}}

==Episodes==
# "Leaving" – first aired Sunday 5 February 1989
# "On the Trail" – aired Monday 6 February 1989
# "The Plains" – aired Tuesday 7 February 1989
# "Return" – aired Wednesday 8 February 1989

==Production==
Larry McMurtrys original novel was based upon a screenplay that he had co-written with Peter Bogdanovich for a movie that was intended to star John Wayne as Call, James Stewart as Gus, and Henry Fonda as Jake Spoon, but the project collapsed when John Ford advised Wayne to reject the script.

Most Hollywood studios were at first not interested in the rights to the novel, which ended up being bought by Motown Productions, headed by Suzanne de Passe. Motown made the mini series for CBS with Robert Halmi Productions as deficit financiers. Halmi at that time was being taken over by Qintex whose head of American operations, David Evans, suggested Simon Wincer as director. CBS also suggested him on the basis of The Last Frontier and Bluegrass. Robert Duvall, who had director approval, watched some of Wincers films and approved him. Scott Murray, "Simon Wincer: Trusting His Instincts", Cinema Papers, November 1989 p9-12, 78-79 

Four other actors (Charles Bronson, Robert Duvall, James Garner, and Jon Voight) were offered the role of Woodrow Call but declined for various reasons before the role fell to Tommy Lee Jones. Garner said that he was originally set to play one of the lead roles but had to drop out for ill health.  Duvall turned down the part of Call on the grounds he had already played that type of character, and asked to play Gus. Bronson agreed to play Blue Duck but he was under contract to Cannon Films who said he was required to make a movie for them instead. 
 The Alamo. 
 
The majority of the miniseries was filmed at the Moody Ranch located seven miles south of Del Rio, Texas. Other locations used for filming, were ranches, in Texas and New Mexico, and the series was shot over 90 days. Real ranch horses were used for authenticity during the filming of the movie. Tommy Lee Jones and Robert Duvall did their own stunts in the film, except for one brief scene that required Duvall to ride in the center of a herd of bison.  

The majority of the ION Television has shown a digitally remastered version of the miniseries starting the weekend of June 30, 2007 during the "RHI Movie Weekend" (RHI Entertainment are the current owners of the   Lonesome Dove miniseries).

==Reception==
Lonesome Dove received an overwhelmingly positive reception with critics and audience alike.

The New York Times comments that: 

:This six-hour miniseries, based on a Pulitzer Prize-winning novel by Larry McMurtry, revitalized both the miniseries and Western genres, both of which had been considered dead for several years. Robert Duvall and Tommy Lee Jones star as fun-loving Gus MacRae and taciturn Woodrow Call, respectively, a pair of longtime friends and former Texas Rangers who crave one last adventure before they bow to their advancing years. Convinced that animals will thrive on the lush grasslands of Montana, Woodrow persuades Gus to undertake the arduous, 3,000-mile cattle drive there. ... Storms, hostile natives, poisonous snakes, and rustlers take their toll on the company before Montana is reached in an adventure that is equal parts Greek tragedy and classic, John Ford-style oater. Originally developed in the 1970s as a script by McMurtry for director Peter Bogdanovich and stars Henry Fonda, John Wayne, and James Stewart, Lonesome Dove earned 18 Emmy nominations and inspired a pair of miniseries sequel as well as two attempts at an ongoing television series.

==Awards== TRIO ranked Lonesome Dove third in a list of ten outstanding miniseries, beginning from the time the format was created 

{| class="wikitable"
|+ 1989 Emmy Awards
|-
! style="width:50em" | Category
! style="width:3em" | Won
! style="width:50em" | Winner
|-
|  Outstanding Achievement in Casting for a Miniseries or a Special  ||   || Lynn Kressel
|-
|  Outstanding Achievement in Makeup for a Miniseries or a Special  ||   || Manlio Rocchetti  (makeup supervisor) , Carla Palmer  (makeup artist) , and Jean Ann Black  (makeup artist)    For Part 4 ("The Return") 
|-
|  Outstanding Achievement in Music Composition for a Miniseries or a Special (Dramatic Underscore)  ||   || Basil Poledouris  (composer)    For Part 4 ("The Return") 
|-
|  Outstanding Costume Design for a Miniseries or a Special  ||    || Van Broughton Ramsey   For Part 2 ("On the Trail") 
|-
|  Outstanding Directing in a Miniseries or a Special  ||   || Simon Wincer  (director)    For Part 1 ("Leaving") and Part 4 ("The Return") 
|-
|  Outstanding Sound Editing for a Miniseries or a Special  ||   || Dave McMoyler (supervising sound editor); Joseph Melody  (co-supervising editor) ; Mark Steele, Richard S. Steele, Michael J. Wright, Gary Macheel, Stephen Grubbs, Mark Friedgen, Charles R. Beith Jr., Scott A. Tinsley, Karla Caldwell, George B. Bell, and G. Michael Graham  (sound editors) ; Kristi Johns  (supervising adr editor) ; Tom Villano  (supervising music editor) ; and Jamie Forester  (supervising music editor)    For Part 3 ("The Plains") 
|- Kevin OConnell  (sound effects mixer)    For Part 4 ("The Return") 
|-
|  Outstanding Achievement in Hairstyling for a Miniseries or a Special  ||  || Philip Leto  (hairstylist)  and Manlio Rocchetti  (hair supervisor)    For Part 2 ("On the Trail") 
|-
|  Outstanding Art Direction for a Miniseries or a Special  ||  || Cary White  (production designer)  and Michael J. Sullivan  (set decorator)    For Part 4 ("The Return") 
|-
|  Outstanding Cinematography for a Miniseries or a Special  ||  || Douglas Milsome  (director of photography)    For Part 4 ("The Return") 
|-
|  Outstanding Editing for a Miniseries or a Special - Single Camera Production  ||  || Corky Ehlers  (editor)    For Part 3 ("The Plains") 
|-
|  Outstanding Lead Actor in a Miniseries or a Special  ||  || Robert Duvall   For Part 2 ("On the Trail") 
|-
|  Outstanding Lead Actor in a Miniseries or a Special  ||  || Tommy Lee Jones   For Part 4 ("The Return") 
|-
|  Outstanding Lead Actress in a Miniseries or a Special  ||  || Anjelica Huston   For Part 3 ("The Plains") 
|-
|  Outstanding Lead Actress in a Miniseries or a Special  ||  || Diane Lane   For Part 3 ("The Plains") and Part 4 ("The Return") 
|-
|  Outstanding Miniseries  ||  || Suzanne de Passe  (executive producer) , Bill Wittliff  (executive producer) , Robert Halmi Jr.  (co-executive producer) , Dyson Lovell  (producer) , and Michael Weisbarth  (supervising producer) 
|-
|  Outstanding Supporting Actor in a Miniseries or a Special  ||  || Danny Glover   For Part 1 ("Leaving"), Part 2 ("On the Trail"), and Part 3 ("The Plains") 
|-
|  Outstanding Supporting Actress in a Miniseries or a Special  ||  || Glenne Headly   For Part 1 ("Leaving"), Part 2 ("On the Trail"), and Part 3 ("The Plains") 
|-
|  Outstanding Writing in a Miniseries or a Special  ||  || Bill Wittliff  (teleplay)    For Part 1 ("Leaving") and Part 4 ("The Return") 
|}
{| class="wikitable" Golden Globes
|-
! Category
! style="width:3em" | Won
! Winner
|-
|  Best Mini-Series or Motion Picture Made for TV  ||   || 
|-
|  Best Performance by an Actor in a Mini-Series or Motion Picture Made for TV  ||   || Robert Duvall
|-
|   Best Performance by an Actor in a Supporting Role in a Series, Mini-Series or Motion Picture Made for TV  ||  || Tommy Lee Jones
|-
|   Best Performance by an Actress in a Supporting Role in a Series, Mini-Series or Motion Picture Made for TV  ||  || Anjelica Huston
|}
{| class="wikitable"
|+Other
|-
! Awards
! Category
! style="width:3em" | Won
! Winner
|-
|  American Cinema Editors (1990)  ||  Best Edited Episode from a Television Mini-Series  ||  || Corky Ehlers  For Part 3 ("The Plains") 
|-
|  BMI Film & TV Awards (1990)  ||  ||   || Basil Poledouris
|-
|  Casting Society of America (1989)  ||  Best Casting for TV Miniseries  ||   || Lynn Kressel
|-
|  Directors Guild of America (1990)  ||  Outstanding Directorial Achievement in Dramatic Specials  ||  || Simon Wincer 
|-
|  TV Land Awards (2007)  ||  Miniseries You Didnt Miss a Moment Of  ||  || 
|- Television Critics Association Awards (1989)  ||  Outstanding Achievement in Drama  ||   || 
|- Television Critics Association Awards (1989)  ||  Program of the Year  ||   || 
|-
|  Western Heritage Awards (1990)  ||  Television Feature Film  ||    || William D. Wittliff  (writer/executive producer) , Suzanne de Passe  (executive producer) , Robert Duvall  (star) , Tommy Lee Jones  (star) , and Anjelica Huston  (star) 
|-
|  Writers Guild of America Awards (1990)  ||  Adapted Long Form  ||   || William D. Wittliff   For Part 1 ("Leaving")
|}

===Release history===
Lonesome Dove, Return to Lonesome Dove, Streets of Laredo and Dead Mans Walk are all available on DVD in the United Kingdom (distributed by Acorn Media UK) and the United States. Both seasons of the TV series have also been released in the U.S.

Lonesome Dove was filmed in a soft matte 1.78:1 (16:9) aspect ratio, allowing it to be cropped from the 4:3 negative. It was then released on Blu-ray Disc on August 5, 2008 just months before the films 20th anniversary.  

==Soundtrack==

The score for Lonesome Dove was composed and conducted by Basil Poledouris, his first western and the first of his five scores for director Simon Wincer - Poledouris subsequently scored the directors next three theatrical films (Quigley Down Under, Harley Davidson and the Marlboro Man and Free Willy) and 2006s Crocodile Dundee in Los Angeles).

In 1993 Cabin Fever Music released an album of selections from his score; Sonic Images issued an expansion in 1998, the tracklisting of which is below with cues premiered on the 1998 album in bold.

# Theme from "Lonesome Dove" 5:13 
# Jakes Fate 2:15 
# Night Mares (Deets, Newt) 3:56 
# Cowboys Down The Street 2:16 
# Statue/Deets Dies 3:04 
# Arkansas Pilgrims (Clara, July, Lorena) 4:30 
# Sunny Slopes of Yesterday 1:58 
# The Leaving 3:30 
# On The Trail 6:46 
# Murdering Horse Thieves 1:16 
# Gus & P-Eye - The Search 5:27 
# Gus Dies 2:34 
# Captain Calls Journey 7:18 
# Farewell Ladies - Finale 5:44

==References==
 

==Further reading==
*A Book of Photographs from Lonesome Dove, by Bill Wittliff.  Foreword by Larry McMurtry. The University of Texas Press, 2007. 188 pp., 112 color photos. ISBN 978-0-292-71311-6 .  
* A Book on the Making of Lonesome Dove, Interviews by John Spong, studio archives photographs by Jeff Wilson, and on-set photographs by Bill Wittliff.12 x 12 inches, 168 pp., 137 color and 2 b&w photos. The Wittliff Collections at Texas State University-San Marcos. http://uweb.txstate.edu/gao/wittliff/collections/book_0047.html

==External links==
*  
*  at TSU-San Marcos

 
 
 
 
 

 
 
 
 
 
 
 
 