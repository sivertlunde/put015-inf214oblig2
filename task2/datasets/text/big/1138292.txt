5x2
{{Infobox film
| name           = 5x2
| image          = 5x2 movie.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = François Ozon
| producer       = Olivier Delbosc Marc Missonnier
| writer         = François Ozon Emmanuèle Bernheim
| screenplay     = 
| story          = 
| based on       =  
| starring       = Valeria Bruni Tedeschi Stéphane Freiss
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = ThinkFilm (USA)
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = $5,250,000
| gross          = $4,111,034  
}} French film directed by François Ozon, which uncovers the back story to the gradual disintegration of a middle class marriage by depicting five key moments in the relationship, but in reverse order.   

==Plot==
A young married couple, Gilles and Marion, sit in an office while they listen to a lawyer read out the formal terms of their separation, after which they book a hotel room together.    The plot then travels backwards chronologically, with the following chapter focusing on a tense dinner party the couple hosted for Gilles’ brother and his boyfriend some time previously, at which Gilles appears to admit to infidelity, before moving back again to the point at the birth of their son, which Gilles manages to miss by several hours, leaving Marion’s parents as the only family with her in the hospital. The film then reverts to their wedding day, before ending with scenes at the Italian beach resort where, already acquaintances from work, they ran into each other by chance and first began their relationship.   
 Two Friends, and that it allowed for “a true, lucid reading of a couple’s story”. 

==Cast==
*Valeria Bruni Tedeschi as Marion
*Stéphane Freiss as Gilles
*Géraldine Pailhas as Valérie
*Françoise Fabian as Monique
*Michael Lonsdale as Bernard

==References==
 

==External links==
* 
* 

 

 

 
 
 
 
 
 