I Was a Teenage Werewolf
{{Infobox film
| name           = I Was a Teenage Werewolf
| image          = I Was A Teenage Werewolf-poster.jpg
| image size     = 190px
| caption        = film poster by Reynold Brown
| alt            =
| director       = Gene Fowler Jr.
| producer       = Herman Cohen
| writer         = Herman Cohen Aben Kandel
| starring       = Michael Landon Whit Bissell
| music          =
| cinematography =
| editing        =
| country        = United States
| distributor    = American International Pictures
| released       =  
| runtime        = 76 minutes
| language       = English
| budget         = $82,000 Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p89  or $123,000 
| gross          = $2,000,000 
}}

I Was a Teenage Werewolf is a 1957 horror film starring Michael Landon as a troubled teenager and Whit Bissell as the primary adult. It was co-written and produced by cult film producer Herman Cohen, and was one of the most successful films released by American International Pictures (AIP). Arkoff, pp. 61–75  It was originally released as a double feature with Invasion of the Saucer Men. 

== Plot ==
Tony Rivers (Michael Landon), a troubled teenager at Rockdale High, is known for losing his temper and overreacting. A campus fight between Tony and classmate Jimmy (Tony Marshall) gets the attention of the local police, Det. Donovan (Barney Phillips) in particular. Donovan breaks up the fight and advises Tony to talk with a “psychologist” that works at the local aircraft plant, Dr. Alfred Brandon (Whit Bissell), a practitioner of hypnotherapy.

Tony declines, but his girlfriend Arlene (Yvonne Lime) &ndash; as well as his widowed father (Malcolm Atterbury) - show concern about his violent behavior. Later, at a Halloween party at “the Haunted House” – an old house at which several of the teenagers hang out – Tony attacks his friend Vic (Kenny Miller) after being surprised from behind. After seeing the shocked expressions on his friends’ faces, he realizes he needs help and goes to see Dr. Brandon.

On Tony’s first visit, however, Brandon makes it clear that he has his own agenda while the teenager lies on the psychiatrist’s couch: Tony will be an excellent subject for his experiments with a scopolamine serum he’s developed that regresses personalities to their primitive instincts. Brandon believes that the only future mankind has is to hurl him back to his primitive state. Although Brandons assistant, Dr. Hugo Wagner (Joseph Mell), protests that the experiment might kill Tony, Brandon continues and within two sessions suggests to Tony that he was once a werewolf.

That night, after a small party at the haunted house, Tony drives Arlene home; and one of their buddies, Frank (Michael Rougas), is attacked and killed as he is walking home through the woods. While Donovan and Police Chief Baker (Robert Griffin) review photographs of the victim and await an autopsy, Pepi (Vladimir Sokoloff), the police stations janitor, persuades officer Chris Stanley (Guy Williams) to let him see the photos. Pepi, a native of the Carpathian Mountains, where werewolves, “human beings possessed by wolves” are common, immediately recognizes the marks on Franks body, much to the disbelief of Chris, who balks at the idea of a werewolf.

The next day, after another session with Brandon, during which Tony tells the doctor that he feels that there is something very wrong with him, Tony reports to Miss Ferguson (Louise Lewis), the principal of Rockdale High. Miss Ferguson tells Tony that she is pleased with him; Brandon has given him a positive report regarding his behavior; and that she intends to recommend Tony for entry into State College. As Tony leaves the principals office happy with the good news, he passes the gymnasium where Theresa (Dawn Richard) is practicing by herself. A school bell behind his head suddenly rings, triggering his transformation into a werewolf, and he attacks and kills Theresa. Tony flees the highschool and despite the changes in his facial appearance, witnesses identify him by his clothing, causing Baker to issue an all-points bulletin for his arrest.

A local reporter, Doyle (Eddie Marr), interviews Tonys father as well as Arlene and her parents, in the hope of locating Tony and getting a scoop. Baker and Donovan attempt to trap Tony in the woods where they think he may be hiding. Still in the form of a werewolf, Tony watches as the dragnet looks for him, but is surprised by a dog and ends up killing it.

In the morning, Tony awakens and sees he has reverted to his normal appearance and walks into the town. After phoning Arlene (who answers, but refuses to tell the police who is on the line), Tony heads to Brandons office and begs for his help. Brandon wants to witness Tonys transformation, and capture it on film in order to advance himself in the scientific community. Brandon tells Tony he will help him and after telling him to lie on the couch, and injects him with the serum again. Immediately following the transformation, a nearby ringing telephone triggers Tonys instincts and he leaps up and kills both Brandon and Wagner, breaking open the film camera in the process, ruining the film. Alerted that Tony has been seen nearby, Donovan and Chris break in and are forced to shoot several times as Tony advances toward them. Upon dying, Tony’s normal features return, leaving Donovan to speculate on Brandons involvement – and on the mistake of man interfering in the realms of God.

==Production notes==
Samuel Z. Arkoff wrote in his memoirs that he got a lot of resistance for producing a film portraying a teenager becoming a monster, an idea that had never been exploited in film before. Arkoff, pp. 61–75 
 Dawn Richard, who plays a teenaged gymnast in the film, was actually a 22-year-old Playboy centerfold model at the time, appearing in the magazine’s May 1957 issue, which hit the newsstands a couple months ahead of the movie.
 Romanian janitor at the police station, was played by the Russians|Russian-born Vladimir Sokoloff, a character actor who appeared as ethnic types in over 100 productions, his most famous being the Old Mexican Man in The Magnificent Seven three years later.

Tony Marshall is the only other male actor to receive billing in the trailer for I Was a Teenage Werewolf, in addition to Landon and Bissell. However, he made only one other motion picture, the obscure Rockabilly Baby for Twentieth Century-Fox, which was released in October of the same year.

Shooting began 13 February 1957. MOVIELAND EVENTS: Edith Atwater Signs for Lancaster Film
Los Angeles Times (1923-Current File)   01 Jan 1957: B37.  The movie was shot in seven days. 
 How to Make a Monster, released in 1958, features two young actors being hypnotized to kill while in make-up as the monster characters "Teenage Werewolf" and "Teenage Frankenstein" of the 1957 films.

==Cast==
* Michael Landon as Tony Rivers Yvonne Lime as Arlene Logan
* Whit Bissell as Dr. Alfred Brandon
* Malcolm Atterbury as Charles Rivers
* Barney Phillips as Detective Sgt. Donovan Robert Griffin as Police Chief Baker
* Joseph Mell as Dr. Hugo Wagner
* Louise Lewis as Principal Ferguson Guy Williams as Officer Chris Stanley
* Tony Marshall as Jimmy
* Vladimir Sokoloff as Pepe, the Janitor
* Kenny Miller as Vic Cindy Robbins as Pearl
* Michael Rougas as Frank Dawn Richard as Theresa

== Release and reception ==
Variety reported: "Another in the cycle of regression themes is a combo teenager and science-fiction yarn which should do okay in the exploitation market   Only thing new about this Herman Cohen production is a psychiatrists use of a problem teenager   but its handled well enough to meet the requirements of this type film.   good performances help overcome deficiencies. Final reels, where the lad turns into a hairy-headed monster with drooling fangs, are inclined to be played too heavily." Variety went on to say that Landon delivers "a first-class characterization as the high school boy constantly in trouble." 

According to Tim Dirks, the film was one of a wave of "cheap teen movies" released for the Drive-in theater|drive-in market. They consisted of "exploitative, cheap fare created especially for them   in a newly-established teen/drive-in genre."  
 US $2,000,000, compared to its $82,000 budget. How to Make a Monster in July 1958. Arkoff, pp. 61–75 

===AIPs female "teenage vampire" companion piece===

Less than four months after the release of I Was a Teenage Werewolf, and coinciding with the release of I Was a Teenage Frankenstein, AIP released   and Paul Dunlap accompanied by a choreographed "ad-lib" dance number, hypnosis as scientific medical treatment, drug injections, specific references to Carpathia , hairy transformation scenes, and even some of the same dialogue. In addition, two prominent actors from I Was a Teenage Werewolf are also featured in Blood of Dracula, Malcolm Atterbury and Louise Lewis, with Lewiss villain, Miss Branding a practically perfect female version of Whit Bissels Dr. Brandon. However, few critics have drawn a connection between the two films, and while most reference works consider I Was a Teenage Frankenstein and How to Make a Monster as direct follow-ups to I Was a Teenage Werewolf, not even cinema guru Leonard Maltin speaks of Blood of Dracula as even being related to the trilogy. 

==Legacy==
I Was a Teenage Werewolf helped launch Landons career, as he became a regular on  , followed by playing Professor John Robinson on the TV show  .
 The Shaggy Dog. The film betrays its successful forebear with Murrays classic bit of dialogue: "Don’t be ridiculous — my son isn’t any werewolf! He’s just a big, baggy, stupid-looking, shaggy dog!" Arkoff, pp. 61–75 

=== Pop culture impact === Police Gazette-style title (which had already been used by Hollywood previously with pictures such as 1949s I Was a Male War Bride and 1951s I Was a Communist for the FBI) with the inclusion of the adjective "teenage", was used again by AIP for their sequel, I Was a Teenage Frankenstein, and the original working title for their 1958 sci-fi film Attack of the Puppet People was I Was a Teenage Doll. Due to the success of I Was a Teenage Werewolf, this convention was constantly mocked in the late 1950s and early 1960s. Many sitcom television series in particular had characters going to movies titled I Was a Teenage Dinosaur, Monster, etc., and it was often referenced in monologues by comedians and bits by disc jockeys.   Examples include Stan Frebergs 1957 radio series, which featured a Madison-Avenue/horror-movie spoof titled "Gray Flannel Hat Full of Teenage Werewolves", {{cite web
  | url = http://www.imdb.com/find?s=all&q=i+was+a+teenage
  | title = IMDb Search: "i was a teenage"
  | format = 
  | work = Internet Movie Database
  | accessdate = 2007-01-04
}}  and the 1959 The Many Loves of Dobie Gillis|"Dobie Gillis" novel I Was a Teenage Dwarf by Max Shulman. 

====Film====
Over the years, the "I Was a Teenage..." title was played on  by several unrelated films — usually comedies — wishing to make a connection with the cult AIP hit, including Teenage Caveman, the 1963 Warner Bros. cartoon, I Was a Teenage Thumb, 1987s I Was a Teenage Zombie, 1992s I Was a Teenage Mummy, 1993s I Was a Teenage Serial Killer, and 1999s I Was a Teenage Intellectual.

The script title for 1985s Just One of the Guys was "I Was a Teenage Boy", a title that used a year later as an alternate for 1986s Willy/Milly. An alternate title for the 1995 hit Clueless was "I Was a Teenage Teenager". 
 Let the Good Times Roll, featuring Madison Square Garden performances from Chuck Berry and Bill Haley and the Comets.

====Television====
Episode 2.19 (1963) of The Dick Van Dyke Show was entitled "I Was a Teenage Head-Writer". {{cite episode
  | title = I Was a Teenage Head Writer
  | series = The Dick Van Dyke Show
  | serieslink = The Dick Van Dyke Show
  | network = CBS
  | airdate = 1963-01-30
  | season = 2
  | number = 19 The Monkees was entitled "I Was a Teenage Monster". {{cite episode
  | title = I Was a Teenage Monster
  | series = The Monkees
  | serieslink = The Monkees
  | network = NBC
  | airdate = 1967-01-16
  | season = 1
  | number = 18
}} 

The July 16, 1982 episode of Second City Television|SCTV ("Battle of the PBS Stars") featured a comedy skit of the movie called "I was a Teenage Communist," mixing horror with the politics of Red Baiting|red-baiting during the 1950s. The host segments, meanwhile, parody the film Alien (film)|Alien.

In 1987, the NBC-TV series Highway to Heaven featured "I Was a Middle-Aged Werewolf" (episode 4.5), written and directed by Michael Landon. Landon, as angel Jonathan Smith,  transforms himself into a werewolf, initially to scare off some teenage bullies. During the earlier scenes, Jonathans buddy watches the original film, remarking: "You know, the guy in this movie reminds me a lot of you," adding, "when hes a regular guy, not when hes got fuzz all over his face."

In April 1997, the movie was mocked directly when it was featured in episode 809 of Mystery Science Theater 3000. {{cite episode
  | title = I Was a Teenage Werewolf
  | series = Mystery Science Theater 30000
  | serieslink = Mystery Science Theater 3000
  | credits = Best Brains Sci Fi Channel
  | airdate = 1997-04-19
  | season = 8
  | number = 9
}} 

The October 28, 1999 episode of SpongeBob SquarePants is titled "I Was a Teenage Gary," and features SpongeBob transforming into a snail after a hypodermic injection.

A Phineas and Ferb episode in 2010 was titled "I Was a Middle Aged Robot". It involves Phineas dad, Mr. Fletcher having his imagination sucked away and the O.W.C.A. (Organization Without a Cool Acronym) replaced him with a robot controlled by Phineas & Ferbs pet platypus, Perry, also known as Agent P.

====Music==== Faker released a song in 2005 entitled "Teenage Werewolf."  Punk band Against Me! released a song in 2010 titled, "I Was a Teenage Anarchist."  Pop punk artist Lil CamRons debut album "I Was a Teeange Cameron" also references the title.

American Musical Theater Composer Joe Iconis wrote a song titled  

The Brazilian rock band Legião Urbana has a song called "Eu era um lobisomem juvenil", title of the movie in Portuguese.

====Publishing====
In Stephen Kings 1986 novel It (novel)|It, and its It (1990 film)|made-for-TV film adaptation several of the characters watch this movie. Afterwards, Pennywise takes the form of a real teenage werewolf to frighten them, particularly Richie. When the Losers Club first attacks Pennywise, it takes the form of the werewolf. In 2002, Last Gasp published I Was a Teenage Dominatrix, a memoir by Shawna Kenney.

== References ==
;Notes
 

;Bibliography
*  

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 