Maragadham
{{Infobox film
| name           = Maragadham மரகதம்
| image          =
| image_size     =
| caption        =
| director       = S. M. Sriramulu Naidu
| producer       = K. R. Seenivasan N. Naga Subramaniyam
| writer         = Murasoli Maran
| screenplay     = S. M. Sriramulu Naidu
| based on       = Karunkuyil Kunrathu Kolai by T. S. D. Sami Padmini Sundaram Balachander Sandhya
| music          = S. M. Subbaiah Naidu
| cinematography = Shailan Bose
| editing        = Velusamy
| studio         = Pakshiraja Studios
| distributor    = Pakshiraja Studios
| released       =  
| country        = India Tamil
}}
 1959 Cinema Indian Tamil Tamil crime-thriller Balachander and Sandhya in lead roles. The film had musical score by S. M. Subbaiah Naidu.  

==Plot==
 

==Cast==
*Sivaji Ganesan as Varendran Padmini as Maragatham / Alamu Balachander as Maranamarthanda Zamindar / Anandar
*Sandhya as Karpagavalli
*T. S. Balaiah
*T. S. Durairaj Chandrababu as Gundan
*O. A. K. Thevar
*P. S. Gnanam
*T. P. Muthulakshmi
*Lakshmirajam
*Lakshmiprabha
*M. R. Santhanam
*Ennatha Kannaiya

==Soundtrack== Playback singers are T. M. Soundararajan, Radha Jayalakshmi, P. Leela & K. Jamuna Rani.

Kunguma Poovey Konjum Puraavey, becoming a hit — the song is popular even after half a century.

{| class="wikitable"
|- 
! No.
! Song
! Singers
! Lyrics
! Length (m:ss) 
|- 
| 1 || Kunguma Poove Konjum Puraave || J. P. Chandrababu & K. Jamuna Rani || Ku. Ma. Balasubramaniam || 03.27 
|- 
| 2 || Aadinaal Nadanam Aadinaal|| Radha Jayalakshmi || Ra. Balu || 03.12 
|- 
| 3 || Kannukkulle Unnai Paaru || T. M. Soundararajan & Radha Jayalakshmi || Ra. Balu || 03.32 
|- 
| 4 || Kaviri Paayum || T. M. Soundararajan || Ra. Balu || 03.27 
|- 
| 5 || Maalai Mayangukindra Neram || Radha Jayalakshmi || Shuddhananda Bharati || 04.08 
|- 
| 6 || Punnagai Thavazhum Madhimugamo|| T. M. Soundararajan & Radha Jayalakshmi || Papanasam Sivan || 03.54 
|- 
| 7 || Pachchai Kili Pola || P. Leela & K. Jamuna Rani ||  ||  
|}

==References==
 

==External links==
*  

 
 
 
 

 