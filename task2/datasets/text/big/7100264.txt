The Karnival Kid
 
{{Infobox Hollywood cartoon
| cartoon name      = The Karnival Kid
| series            = Mickey Mouse
| image             = The Karnival Kid.png
| image size        = 
| alt               = 
| caption           = "Hot dogs! Hot dogs!"
| director          = Walt Disney Ub Iwerks
| producer          = Walt Disney
| story artist      = 
| narrator          = 
| voice actor       = Carl Stalling Walt Disney
| musician          = Carl Stalling
| animator          = Ub Iwerks
| layout artist     = 
| background artist =  The Walt Disney Studio
| distributor       = Celebrity Productions
| release date      =   (USA)
| color process     = Black and white
| runtime           = 7:40 minutes
| country           = United States
| language          = English
| preceded by       = The Plow Boy
| followed by       = Mickeys Follies
}} animated short The Walt Disney Studio and released to theaters by Celebrity Productions. It is the ninth film in the List of Mickey Mouse cartoons|Mickey Mouse film series, and the first in which Mickey speaks.  (During his first eight appearances Mickey whistled, laughed, cried and otherwise vocally expressed himself.)  Mickeys first spoken words were in this cartoon, "Hot Dogs!" "Hot Dogs!", the vocal effects being provided by composer Carl Stalling instead of Walt Disney. 

Besides Mickey, three other recurring characters of the series also appear. The first is Clarabelle Cow in a cameo. The second is Kat Nipp, making his third and last appearance.  The third is Mickeys recurring love interest, Minnie Mouse.

==Synopsis==
The Karnival Kid is broken into two distinct segments. The first segment features Mickey selling hot dogs at a carnival. The second segment is set later that night and features Mickey, accompanied by two cats, in a moonlight serenade. 

The short opens to the scene of a bustling carnival. After a few initial sight gags, the action quickly focuses on Kat Nipp, a barker at the carnival who is enticing a crowd to see Minnie, "the Shimmy Dancer."  Mickey stands nearby, selling hot dogs and heckling Nipp. Nipp briefly gets into a dispute with Mickey over a dancing doll scam.  However, Minnie soon notices Mickey and calls him over to order a hot dog.  She takes a coin out of her stocking to pay, but Mickey, who is clearly attracted to her, refuses to accept the coin and gives it to her for free.  When she bites into the hot dog, it screams and runs away.  Mickey catches it and spanks it, concluding the first segment.  Much of the humor in this segment comes from the interaction between Mickey and his hot dogs, with the latter tending to act like actual dogs in relation to their owner/trainer. 

In the second segment, Mickey attempts to draw Minnies attention by playing guitar outside her window.  He is joined by two alley cats who noisily sing along.  The sound delights Minnie but awakens an irate Kat Nipp, who had been resting in a nearby trailer.  Nipp starts throwing things at the three annoyances in an attempt to silence them.  The short ends as Mickey is hit with an entire bed and knocked dizzy.
 The Streets Sweet Adeline.

==Legacy and influence==
In the Toontown Central location of the Disney video game Toontown Online, a building is named after The Karnival Kid.

Mickeys first words may have inspired the "Hot Dog" song from Mickey Mouse Clubhouse.
 Roy Williams to invent the Mickey Mouse ears hat.

Ub Iwerks reused elements of the plot and many of the visual gags from The Karnival Kid in his 1932 cartoon, Circus. The part of the hot dog vendor is played by Flip the Frog. 

New York Weenie, an episode from the 2013 Mickey Mouse (2013 TV series)|Mickey Mouse shorts series, shares similar themes and gags with The Karnival Kid. The episode was directed by Aaron Springer, most notable for his work on SpongeBob SquarePants.

==External links==
* 

==References==
  

 

 
 
 
 
 
 
 
 
 
 