Megaforce
 

 
{{Infobox film name           = MegaForce image          = Megaforceposter.jpg caption        = MegaForce movie poster director       = Hal Needham producer       = Andre Morgan Albert S. Ruddy story          = Robert S. Kachler  screenplay     = James Whittaker Albert S. Ruddy Hal Needham André Morgan
| starring = {{plainlist|
* Barry Bostwick
* Michael Beck
* Persis Khambatta
* Edward Mulhare
* George Furth
* Henry Silva
}} cinematography = Michael C. Butler editing        = Patrick Roark Skip Schoolnik studio  Golden Harvest Northshore Investments Ltd. distributor    = 20th Century Fox (USA) Toho-Towa (Japan)  released       =   runtime        = 99 min. country        = United States Hong Kong language       = English music          = Jerrold Immel awards         = budget         = $20 million  gross          = $5,675,599 tagline        = Deeds Not Words
}}
 1982 directed Ralph Wilcox, Robert Fuller (who, years later, admitted to being less than fond of the picture) and Henry Silva.
 dune buggies launching missiles that proved lethal for main battle tanks. The dune buggies, "megadestroyers" or "megacruisers", also had lasers that could destroy a tank in a single shot. The vehicles were coated with a photo-sensitive paint that was a white, tan, and black lightning-bolt scheme during the day and darkened to a solid black camouflage at night. In the film finale, the main characters motorcycle activates small (~2&nbsp;ft or 0.6 m) fold-out wings and flies.

The movie was made into a computer game, most notably for the Atari 2600.

The film was a critical and commercial failure on its release and was nominated for three Razzie Awards, Worst Picture, Worst Director and Worst Supporting Actor (Michael Beck).
 Delta Force, the 1986 Chuck Norris blockbuster, had a plot very similar to MegaForce, and in the movie, Norris rides a motorbike that fires missiles.

==Plot synopsis==
The story involves two fictional countries, the peaceful Republic of Sardun and their aggressive neighbor Gamibia. Unable to defend themselves from a Gamibian incursion, Sardun sends Major Zara (Persis Khambatta) and General Byrne-White (Edward Mulhare) to ask the help of MegaForce – a secret mercenary army composed of international soldiers of fortune, equipped with advanced weapons and vehicles. The MegaForce leader, Commander Ace Hunter (Barry Bostwick), accepts the peacekeeping mission when he learns that his rival, and former military academy friend, Duke Gurerra (Henry Silva) is leading the Gamibian invasion.

While Hunter composes an elaborate battle plan to destroy Gurerras forces, Zara tries out to become a member of MegaForce. Although she passes the tests, Hunters growing feelings of affection toward her prevent him from accepting her on for such a dangerous mission.

Eventually, MegaForce successfully para-drops its attack vehicles into Gamibia and Hunter mounts his sneak attack against Gurerras forces. Although they manage to destroy his base, Gurerra has set a trap for them at the teams only means of escape – a dry lake bed where the cargo planes will pick them up. Gurerra sends his tanks to secure the lake bed while Hunter comes up with a plan to attack Gurerra from behind by crossing over a mountain range the enemy tanks had turned their backs toward.
 thumbs up.

==Soundtrack==
An album was released on Boardwalk Records, with the theme song done by the group 707 (band)|707, was released as a single reaching the mid level of the charts in the U.S.A. and Canada.  The soundtrack was released on Compact Disc in 2011 by BSX Records. 

==Merchandising==
Mattel also produced a Vertibird and Hot Wheels play sets based on the MegaForce theme.

A video game based on the film was released in 1982 on the Atari 2600.

==In popular culture==
In the   includes a number of apparent references to the film, including similar scenes of a flying motorcycle and an underground base where the hero meets various specialists.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 