Hasyaratna Ramakrishna
{{Infobox film
| name           = Hasyaratna Ramakrishna
| image          = 
| image size     = 
| caption        = 
| director       = B. S. Ranga
| producer       = B. R. Vasanth A. Ramesh Rao
| story          = B. S. Ranga
| screenplay     = B. S. Ranga
| narrator       = Manjula
| music          = T. G. Lingappa
| cinematography = B. N. Haridas
| editing        = P. G. Mohan
| studio         = Varna Productions
| distributor    = Varna Productions
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Kannada
}}
 Manjula and Srinath in the pivotal roles. 

==Cast==
* Anant Nag as Ramakrishna
* Aarathi as Kamala Manjula
* Srinath
* K. S. Ashwath
* Shivaram
* Pramila Joshai
* Sripriya
* Dinesh
* Ashwath Narayan
* Anantharam Maccheri
* Mandeep Roy

==Soundtrack==
{{Infobox album
| Name        = Hasyaratna Ramakrishna
| Type        = Soundtrack
| Artist      = T. G. Lingappa
| Cover       = 
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 1982
| Recorded    =  Feature film soundtrack
| Length      = 32:15
| Label       = Saregama
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

T. G. Lingappa composed the soundtrack, and lyrics were written by Chi. Udaya Shankar. The album consists of eight soundtracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 32:15
| lyrics_credits = yes
| title1 = Anjaneya Swami Banda
| lyrics1 = Chi. Udaya Shankar
| extra1 = S. P. Balasubrahmanyam
| length1 = 4:27
| title2 = Chikkavanene Ivanu
| lyrics2 = Chi. Udaya Shankar
| extra2 = Vani Jairam, K. J. Yesudas
| length2 = 5:17
| title3 = Iddhu Hogu
| lyrics3 = Chi. Udaya Shankar
| extra3 = K. J. Yesudas, S. Janaki
| length3 = 4:01
| title4 = Krishna Karedaaga
| lyrics4 = Chi. Udaya Shankar
| extra4 = S. Janaki
| length4 = 4:06
| title5 = Mellage Kai Kotthu
| lyrics5 = Chi. Udaya Shankar
| extra5 = S. Janaki
| length5 = 4:03
| title6 = Nanu Neenu Iniya
| lyrics6 = Chi. Udaya Shankar
| extra6 = S. Janaki
| length6 = 4:01
| title7 = Enu Chenna
| lyrics7 = Chi. Udaya Shankar
| extra7 = S. Janaki
| length7 = 2:50
| title8 = Bhoomiya Thanditta
| lyrics8 = Chi. Udaya Shankar
| extra8 = P. B. Sreenivas
| length8 = 3:30
}}

==References==
 

==External links==
*  

 
 
 
 


 