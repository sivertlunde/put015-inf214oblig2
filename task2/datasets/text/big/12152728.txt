Uuno Turhapuro (film)
{{Infobox film
| name = Uuno Turhapuro
| image =
| caption =
| writer =Spede Pasanen
| starring =Vesa-Matti Loiri Marjatta Raita Spede Pasanen Simo Salminen Juhani Kumpulainen Anna-Liisa Ruotsi
| director =Ere Kokkonen
| producer =Spede Pasanen
| cinematography =
| editing = Filmituotanto Spede Pasanen OY
| released = 
| runtime =87 min.
| country =Finland
| language =Finnish
| music =
}}

Uuno Turhapuro is a 1973 Finnish film directed by Ere Kokkonen, and the first, black and white Uuno Turhapuro film. It stars Vesa-Matti Loiri, Marjatta Raita, Pertti Pasanen, and Simo Salminen.   

Uuno Turhapuro is the main character of the film, which was created by Spede and Loiri for Spedevisio. The unexpected success led to the creation of a 19-movie series. In an effort to save money, Uuno became Spedes first feature to be filmed and edited on video before being printed to film for theatrical distribution. This method was employed by Spede all the way into his late 1980s features.

The film was the first produced by Spede after nearly a two-year-long hiatus, when Spedes three feature films released in 1971 (Hirttämättömät, Helvetin Radikaalit and Kahdeksas Veljes) all failed to make a notable profit.

==References==
 

== External links ==
*  

 

 
 
 
 
 
 


 
 