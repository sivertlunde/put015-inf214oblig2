Tintorettor Jishu (film)
{{Infobox film
| name           = Tintorettor Jishu
| image          = Tintorettor-jesus-2008.jpg
| image_size     = 200px
| caption        = Poster of Tintorettor Jishu
| director       = Sandip Ray
| producer       = T Sarkar Productions
| writer         = Satyajit Ray
| based on       = Tintorettor Jishu by Satyajit Ray
| narrator       =
| starring       = Sabyasachi Chakrabarty Parambrata Chatterjee Bibhu Bhattacharya
| music          = Sandip Ray
| cinematography = Barun Raha Sasanka Palit
| editing        = Subroto Ray
| distributor    =
| released       =  
| runtime        = 100 minutes
| country        = India
| language       = Bengali
| budget         = Rs.1.25 crore
| gross          = Rs.1.25 crore 
}}

Tintorettor Jishu ( ) (2008) is a thriller film directed by Sandip Ray based on the story of the same name by Satyajit Ray. 
{{cite web
| url        = http://www.gomolo.com/74/feature-i-didnt-get-jesus-by-tintoretto-sandip-ray
| title      = I Didnt Get Jesus by Tintoretto - Sandip Ray 
| publisher  = www.gomolo.in
| accessdate = 2008-11-11
| last       = 
| first      = 
}}
  
{{cite news
| url        = http://www.telegraphindia.com/1081212/jsp/entertainment/story_10239426.jsp
| title      = Role call for Ray thriller
| publisher  = www.telegraphindia.com
| accessdate = 2008-12-12
| last       = 
| first      = 
| location   = Calcutta, India
| date       = 2008-12-12
}} Kailashey Kelenkari

==Plot==
The Niyogi family have a famous painting by the Italian master Tintoretto. However, not everyone is aware of the value of the painting. One of the family members steals it, and international buyers are interested in it. Feluda chases the criminals all the way to Hong Kong. There was a surprise waiting for him there. Eventually, Feluda (with the help of a relative stranger) succeeds in solving the mystery.

==Cast==
* Sabyasachi Chakrabarty as Feluda
* Parambrata Chatterjee as Topshe
* Bibhu Bhattacharya as Jatayu
* Tota Roy Chowdhury as Robin/Rajsekhar Neogi
* Shilajeet Majumdar as Nandakumar/Fake Rudrasekhar Neogi
* Biswajit Chakraborty as Hiralal Somani
* Paran Bandyopadhyay as Purnendu
* Debesh Roy Chowdhury
* Rajaram Yagnik

Sagar Bhoumik, a young Bengali painter from Dum Dum Cantonment, North Kolkata, painted the depiction of Tintoretto’s Jesus used in the film.
 
{{cite web
| url        = http://www.gomolo.com/207/feature-kolkatas-tintoretto
| title      = Kolkata’s Tintoretto
| publisher  = www.gomolo.in
| accessdate = 2009-06-10
| last       = 
| first      = 
}}
 

== References ==
 

==External links==
* 
* 
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 