Break in the Circle
 
 
{{Infobox film
| name           = Break in the Circle
| image          = "Break_in_the_Circle"_(1955).jpg
| caption        = British quad poster
| director       = Val Guest
| producer       =  Michael Carreras
| writer         = Val Guest 
| based on       = novel by Robin Estridge  (as Philip Loraine)
| starring       = Forrest Tucker  Eva Bartok  Marius Goring
| music          = Doreen Carwithen
| cinematography = Walter J. Harvey
| editing        = Bill Lenny
| studio         = Hammer Film Productions
| distributor    = Twentieth Century Fox  (USA)  Exclusive Films   (UK)
| released       = 28 February 1955  (UK)
| runtime        = 87 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} directed by German millionaire Polish scientist escape to the Western World|West. 

Doreen Carwithen composed the score for the film.

==Cast==
* Forrest Tucker as Captain Skip Morgan 
* Eva Bartok as Lisa 
* Marius Goring as Baron Keller 
* Guy Middleton as Major Hobart 
* Eric Pohlmann as Emile 
* Arnold Marlé as Professor Pal Kudnic 
* Fred Johnson as Chief Agent Farquarson 
* David King-Wood as Colonel Patchway 
* Reginald Beckwith as Dusty 
* Guido Lorraine as Franz
* André Mikhelson as Russian thug  
* Stanley Zevic as Russian thug 
* Marne Maitland as The phony Kudnic  
* Arthur Lovegrove as Bert

==References==
 

 
 

 
 
 
 
 
 
 
 
 


 