Ivide Thudangunnu
{{Infobox film
| name           = Ivide Thudangunnu
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = Mohan Sharma
| writer         = S. L. Puram Sadanandan
| screenplay     = S. L. Puram Sadanandan Rohini Balan K Nair Johnson
| cinematography = Vipin Das
| editing        = G Venkittaraman
| studio         = Sadguna Combines
| distributor    = Sadguna Combines
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, Rohini and Meri Adalat.


==Plot==
Krishnakumar is orphan and a policeman who lived with his younger sister Sheela. Sheela falls in love with her classmate Babu and Krisnha accepts him as his brother in law. But tragedy strikes the couple on their honeymoon when Babu was beaten into a coma and Sheela was raped and killed. The police arrested four middle-age friends who follow them. But Krishnakumar found out that his sisters murderers  were actually Babus three classmates but now they are freed thanks to M. S. Menon, one of the killers father. Krishnakumar takes revenge against Menon.

==Cast==
 
*Mohanlal as Krishnakumar  Rahman as Babu  Rohini as Sheela 
*Rajalakshmi as Indu 
*Balan K. Nair as M. S. Menon 
*Thikkurisi Sukumaran Nair as Adv. Balachandra Menon 
*K.P. Ummer as Pappachan 
*Meenakumari as Sarada 
*Sankaradi as Old Man at the Hotel #1 
*Achankunju as Old Man at the Hotel #2  Bahadur as Kurup 
*Maniyan Pillai Raju as Peter 
*Santhosh as M.S Menons Son
*C. I. Paul as Smuggler Janardanan
 

==Soundtrack== Johnson and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ennomal sodarikku || Mohan Sharma || Poovachal Khader || 
|-
| 2 || Etho swapnam pole || Vani Jairam, Mohan Sharma || Poovachal Khader || 
|-
| 3 || Neeyente jeevanaanomale || P Susheela, Mohan Sharma || Poovachal Khader || 
|-
| 4 || Thaanaaro thannaaro || Chorus, Mohan Sharma || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 
 
 
 

 
 