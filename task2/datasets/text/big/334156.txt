Character (film)
{{Infobox film
| name           = Character
| image          = Karakter.jpg
| caption        = German film poster of Character
| director       = Mike van Diem
| producer       = Laurens Geels
| writer         = Ferdinand Bordewijk (novel) Mike van Diem
| narrator       = 
| starring       = Jan Decleir Fedja van Huêt Victor Löw
| music          = Paleis van Boem
| cinematography = Rogier Stoffers
| editing        = Jessica de Koning
| distributor    = 
| released       = 17 April 1997 (Netherlands) 27 March 1998 (U.S.) 10 April 1998 (Canada) 18 October 1998 (UK)
| runtime        = 122 minutes
| country        = Netherlands, Belgium Dutch
| budget         = $4,500,000
| gross         = $713,413
| preceded_by    = 
| followed_by    = 
}} Belgian film, based on the best-selling novel by Ferdinand Bordewijk and directed by Mike van Diem. The film won the Academy Award for Best Foreign Language Film at the 70th Academy Awards. The film stars Fedja van Huêt, Jan Decleir, and Betty Schuurman.

==Plot==
In the Netherlands of the 1920s, Dreverhaven (Decleir), a dreaded bailiff, is found dead, with a knife sticking out of his stomach. The obvious suspect is Jacob Willem Katadreuffe (van Huêt), an ambitious young lawyer who worked his way up from poverty, always managing to overcome Dreverhavens personal attacks against him. Katadreuffe was seen leaving Dreverhavens office on the afternoon of the murder. He is arrested and taken to police headquarters, where he reflects back on the story of his long relationship with Dreverhaven, who, police learn, is also Katadreuffes father. 

The story begins when Katadreuffes taciturn mother, Joba (played by Schuurman), worked as a housekeeper for Dreverhaven. During that time, they had sex only once (it is implied that the encounter was forced upon Joba). She becomes pregnant and leaves her employer to make a living for herself and her son. Time and again, she rejects Dreverhavens offers by mail of money and marriage.  

Even as a child, Katadreuffe finds that his path crosses with Dreverhaven, often with dire consequences. When he is arrested for becoming involved in a boyish theft and tells the police that Dreverhaven is his father, Dreverhaven refuses to recognize him as his son. When, as a young man, he unwittingly takes a loan from a bank that Dreverhaven owns to purchase a failed cigar store, Dreverhaven sues him to win the money back and force him into bankruptcy. Still, Katadreuffe manages to pay back the debt, finding a clerical position in the law firm retained to pursue him for his cigar-store debt.  He manages to secure this job, even though most of his education is derived from reading an incomplete English-language encyclopedia that he finds as a boy in his mothers apartment; studying this set, he manages to teach himself English, which turns out to be a valuable talent in the eyes of his employers. 

After paying back the cigar-store debt, Katadreuffe immediately seeks a second loan from Dreverhaven, so that he can finance his education and legal studies and, ultimately, take and pass the bar examination. Dreverhaven agrees, on the condition that he can call back the loan at any time. Despite the bailiffs efforts to hinder his son, Katadreuffe passes his bar examination and qualifies as a lawyer. On the afternoon when his firm holds a celebration of his becoming a lawyer (the day with which the film begins, the day of the murder), Katadreuffe storms into Dreverhavens office to confront his life-long tormentor, the bailiff.  Katadreuffe reacts with rage to Dreverhavens congratulations, and his offer of a handshake, and, though he at first turns to leave, he runs toward Dreverhaven and attempts to attack him. After a bloody and angry brawl, Katadreuffe is witnessed leaving the bailiffs office. 

However, the police discover that Katadreuffe left Dreverhaven at 5:00 p.m., though an examination of the bailiffs body reveals that Dreverhaven died at 11:00 p.m. The police finally reveal to Katadreuffe that Dreverhaven actually committed suicide.  After Katadreuffe is cleared, a police official hands him a document, left by Dreverhavens lawyer, that turns out to be the bailiffs will, which leaves all of his considerable wealth to Katadreuffe. The will is signed "Vader" (Father).

==Themes==
The complicated relationship between Katadreuffe and Dreverhaven lies at the center of this film. Unrequited love and blind ambition are among the other themes in this film, as is the difficulty some of the characters have in expressing emotions fully and the repercussions that ensue.  Another theme is the clash between ones desires and ones ambitions, particularly keenly felt by Katadreuffe.

==Production==
Most scenes of the movie were shot in Wrocław, Poland.

==References==
 
 

==External links==
* 
* 
*broken  
*broken`(13/10/2010)  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 