One-Hand Clapping
{{Infobox film
| name           = One-hand Clapping
| image          = At klappe med en hånd.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Gert Fredholm
| producer       = Mikael Olsen
| writer         = Gert Fredholm Gert Duve Skovlund Mikael Olsen
| starring       = Jens Okking Peter Gantzler  
| music          = Jon Bruland  Ole Arnfred
| cinematography = Lars Skree
| editing        = Molly Malene Stensgaard
| studio         = Zentropa
| distributor    = Sandrew Metronome
| released       =  
| runtime        = 94 minutes
| country        = Denmark
| language       = Danish
| budget         = 
| gross          = 
}} 2001 Cinema Danish comedy film written and directed by Gert Fredholm, and starring Jens Okking, Peter Gantzler, and  . The film was produced by Zentropa. 

==Plot==
Erik Svensson is a wealthy but aging businessman whose wife has finally died after years of serious illness.. He is now able to sell his company and plans a life in luxury in Spain with his beautiful girlfriend Helene. Just before departure, he receives a letter from a former mistress claiming that he is the father of her daughter. He hires the undertaker Anders as his chauffeur and together they set out to investigate the claim.

==Cast==
* Jens Okking as Svensson
* Peter Gantzler as Anders
*   as Heidi
*   as Lotte
* Ann Eleonora Jørgensen as Helene
* Jesper Christensen as H.C. Krøyer
*   as Elisabeth

==Accolades== Bodil and Robert award Best Actor in a Leading Role for his performance.
 
 
==References==
 

==External links==
*  
*  

 
 
 
 
 


 
 