The Princess of Montpensier
 
 
{{Infobox film
| name           = The Princess of Montpensier
| image          = Princesse-de-montpensier.jpg
| caption        = Theatrical release poster
| director       = Bertrand Tavernier
| producer       = Marc Silam Eric Heuman
| screenplay     = Jean Cosmos François-Olivier Rousseau Bertrand Tavernier
| story          = Madame de La Fayette  
| starring       = Mélanie Thierry Gaspard Ulliel Grégoire Leprince-Ringuet Lambert Wilson Raphaël Personnaz
| music          = Philippe Sarde
| cinematography = Bruno de Keyzer
| editing        = Sophie Brunet
| studio         = Paradis Films StudioCanal
| distributor    = StudioCanal
| released       =  
| runtime        = 139 minutes
| country        = France
| language       = French
| budget         = € 13.35 million
}} Wars of 63rd Cannes Film Festival and was released in French cinemas on 3 November 2010.

==Cast==
* Mélanie Thierry as Marie de Mézières Duke de Guise Prince de Montpensier
* Lambert Wilson as Count de Chabannes Duke dAnjou
* Michel Vuillermoz as Duke de Montpensier
* Anatole De Bodinat as Joyeuse
* Éric Rulliat as Quelus
* Samuel Theis as La Valette
* Judith Chemla as Catherine de Guise
* Philippe Magnan as Marquis de Mézières
* César Domboy as Mayenne Cardinal de Lorraine
* Florence Thomassin as Marquise de Mézières

==Production==
Unusual for a Bertrand Tavernier project, the director was not attached from the very start. When he became involved, there was already a first version of a screenplay ready, written by François-Olivier Rousseau. However, with his usual co-writer Jean Cosmos, Tavernier went back to the original source in order to adapt the script to his own vision.    The screenplay was not an entirely faithful adaptation of the original short story, published anonymously in 1662. "Mme de La Fayette, who was from the 17th century, wrote about the 16th. Knowing that the 17th century had become very puritanical, while the 16th was not, we removed some filters, but never bent the feelings portrayed", Tavernier explained in Le Figaro. 
 National Center of Cinematography and the Deutsch-Französische Förderkommission. The budget was 13.35 million euro.  
 La Reine Margot, which is set during the same period. What Tavernier liked about the film was how casual the costumes were, and not at all based on the ceremonial clothing seen in paintings from 16th century.  Horses were brought to the set from Paris.  Lambert Wilson and Raphaël Personnaz were the only actors with previous riding experience, and all main actors prepared for their roles by taking riding lessons. 
 Centre and Western films, where important conversations often take place on horseback.  The lighting was inspired by film noir, as the director primarily aimed to create an atmosphere of emotional tension, "not imitate paintings or pictorial reconstruction".  The film was shot in Panavision and contains no artificial special effects or computer-generated imagery. 

==Release==
The film premiered on 16 May as part of the main competition of the 2010 Cannes Film Festival.  StudioCanal released it in 384 French cinemas on 3 November the same year.  Distribution rights for the United States were bought in Cannes by IFC Films, which releases it on 1 April 2011.   The release in the United Kingdom is set to 8 July 2011,    The film was released in the United States on 14 April 2011. 

==Reception==
François-Guillaume Lorrain reviewed the film for   was less enthusiastic and described the film as "the wars of religion in a teen movie". He did think it had a certain sense of fresh air and lucidity, but that "the flamboyant feelings and the battles are freeze-dried", which only left an impression of emptiness.  It received the top rating of three stars in Le Parisien, where Marie Sauvion wrote: "The beauty of the images, of the costumes, the delight of a dusted off romance, of an inspiring troupe of actors, of amazing supporting roles ... , all of this contributes to make The Princess of Montpensier an ambitious and poignant film." 

In the U.S., the film has received largely favorable reviews, including one from Roger Ebert. 

==See also==
* 17th-century French literature
* Duchy of Montpensier
* Counts and Dukes of Guise
* House of Valois

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 