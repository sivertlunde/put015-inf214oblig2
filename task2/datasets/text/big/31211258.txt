Hokuspokus (film)
Hokuspokus German comedy film directed by Gustav Ucicky and starring Lilian Harvey, Willy Fritsch and Oskar Homolka.  It was an adaptation of the play Hokuspokus (play)|Hokuspokus by Curt Goetz. A woman is put on trial for the murder of her husband only for him to turn up alive. An English-language version The Temporary Widow was made at the same time also directed by Ucicky and starring Laurence Olivier.

==Main cast==
* Lilian Harvey - Kitty Kellermann
* Willy Fritsch - Peter Bille
* Oskar Homolka - Grandt
* Gustaf Gründgens - Dr. Wilke
* Otto Wallburg - Dr. Schüler
* Fritz Schmuck - Hartmann
* Ferdinand von Alten - Lindborg
* Harry Halm - Kolbe
* Rudolf Biebrach - Morchen
* René Hubert (actor)|René Hubert - Loiret
* Kurt Lilien - Kulicke
* Ruth Albu - Anny Sedal
* Max Ehrlich - Kuhnen

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 