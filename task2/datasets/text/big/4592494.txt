Castro Street (film)
{{Infobox film|
  name     = Castro Street |
  image          = |
  director       = Bruce Baillie |
  producer       = Bruce Baillie  | 1966 |
  runtime        = 10 min. | English |
}}

Castro Street (1966 in film|1966) is a visual nonstory documentary film which uses the sounds and sights of a city street—in this case, Castro Street near the Standard Oil Refinery in Richmond, California—to convey the streets own mood and feel. There is no dialogue in this non-narrative film|non-narrative experimental film. It was directed by Bruce Baillie.

In 1992, the film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

== External links ==
*  
*   at the Library of Congress

== References ==
 

 
 
 
 
 
 
 
 
 


 