Ressha Sentai ToQger vs. Kyoryuger: The Movie
 
{{Infobox film
| name           = Ressha Sentai ToQger vs. Kyoryuger: The Movie
| film name = {{Film name| kanji = 烈車戦隊トッキュウジャーVSキョウリュウジャー THE MOVIE
| romaji = Ressha Sentai Tokkyūjā Tai Kyōryūjā Za Mūbī}}
| image          = TQG vs Kyoryu.jpg
| alt            = 
| caption        = Theatrical poster
| director       = Katsuya Watanabe
| producer       =  
| writer         = Yasuko Kobayashi
| narrator       =
| starring       =  
| music          =  
| cinematography = Fumio Matsumura
| editing        = Kazuko Yanagisawa Toei
| Toei Co. Ltd
| released       =  
| runtime        = 64 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          =  }}
  is a 2015 Japanese film, featuring a crossover between the casts and characters of the Super Sentai television series Ressha Sentai ToQger and Zyuden Sentai Kyoryuger, including the debut appearance of the main cast of Shuriken Sentai Ninninger. Kyaeens Hiroyuki Amano guest stars.   It was released nationally in Japan on January 17, 2015.

==Story==
Lady of the Galaxy Line informs the ToQgers that a strange energy is heading its way to Earth as it sends a Galaxy Line terminal crashing to Earth. The ToQgers investigate, finding themselves fighting a group of Cambrima and Zorima before the main five members of the Kyoryugers save them from the Deboth Army grunts. Noticing how long it has been since they fought together, the Kyoryugers take over the fight and defeat the Deboth Army minions before learning the Galaxy Line terminal is surrounded by a barrier. The Ressha arrives and Daigo, able to see it despite being an adult due, is given a special pass. Once in the Ressha, Daigo reveals the figure who created around the terminal to be the one who created his teams enemy Deboth: Creator Devius. Furthermore, Daigo explains that Devius is siphoning the Galaxy Line terminals energy and that his team have a chance to stop him before he reaches full power to destroy Earth. Though Right offers his aid, Daigo turns down the ToQgers help as they are unable to fight the Deboth Army and advises them to leave this to him and his team. At Castle Terminal, Crimson High Priest Salamazu appears before Emperor Z to set up an alliance between the Shadow Line and Debious.

While waiting for a train, Utsusemimaru finds himself facing Clock Shadow and a group of Close before Akira arrives to aid the Kyoryuger. The two sixth member Sentai warriors transform to fight Clock Shadow as the rest of the ToQgers arrives. The only one who knows Clock Shadows power to de-age people with his singing voice, ToQ 6gou summons the Build Ressha to get his team and Kyoryu Gold away from the Shadow Creep before he is driven off by Shuriken Sentai Ninninger. But Clock Shadow succeeds in restoring the ToQgers to their child forms while turning Utsusemimaru into a baby. After being driven back by Salamazu and the Shadow line, the Kyoryugers meet with the ToQgers as the latter discuss a team-up despite Daigos refusal to her children involved. Before Right explains it is no different than before, the groups are ambushed by Clock Shadow the Close and Zorima. Despite the Kyoryugers intent to protect the children, the ToQgers transform and exchange their ToQ Ressha with the Kyoryugers Zyudenchi so the Sentai teams can combine their Imagination and Brave to defeat the grunts. 

But after dissolving his partnership with the Shadow Line to steal their darkness to complete Deviuss power up, Salamazu appears and absorbs Clock Shadow and the remaining grunts before he transforms into a giant box train worn by five giant Close. Kyoryuzin, ToQ-Oh and BuildDai-Oh face Salamazu before forming Gigant Kyoryuzin with Ressha and ToQ Rainbow with Zyudenchi to defeat their enemy while the ToQgers and Utsusemimaru return to their adult status. However, the victory is short-lived as Devius appears and proceeds to use the darkness Salamazu gave him to overwhelm the Sentai teams with the Kyoryugers injured. Later, at the Hyper Ressha Terminal, the ToQgers understand that they may need to fight Devius on their own as the Zyudenryu left their Zyudenchi in the terminal. Daigo, overhearing the ToQgers discussion about protecting their families, understands them a bit better. The next morning, the Conductor, Ticket, and Wagon have gathered in the office of the President of the Rainbow Line where they meet the spirit of Torin. With Torins cooperation, they were able to combine the giant Zyudenchi to create a Brave Zyudenchi. The attack plan is for the other ToQgers to hold off Devius defenses when the Red Ressha gets the Brave Zyudenchi to the barrier to break through. 

While the others contend with Cryners and revived Shadow Creeps, ToQ 1gou reaches the terminal and faces Devius before being overwhelms. Things seem bleak until Kyoryu Red arrives to ToQ 1gous aid while the other Kyoryugers aid the ToQgers. After Devius achieves full power, ToQ 1gou and Kyoryu Red assume their Hyper ToQ 1gou and Kyoryu Red Carnival forms to defeat him with a Gabutyra Ressha before escaping the cave-in. But Devius is revealed to have survived as he emerges as a giant to wipe out the two Sentai teams. However, the ToQgers find unexpected help from the Shadow Line, who want revenge on Deviuss treachery, along with the other Kyoryugers that Canderrilla and Luckyuro called last night. The three groups proceed to clip Deviuss wings before destroying him for good. Later, the ToQgers say their good byes to the Kyoryugers with Daigo wishing them the best of luck at getting home someday.

==Cast==
* :  ,  
* :  ,  
* :  ,  
* :  ,  
* :  ,  
* :  
* :  
* :  
* :  
* :  
* :  
* :  ,  
* :  
* :  
* :  

===Voice cast===
* :  
* :  
* :  
* :  
* , ToQger Equipment Voice:  
* :  
* :  
* :  
* :  
* , Cryner and Castle Terminal Announcements:  
* :  
* :  
* :  
* :  
*Kyoryuger Equipment Voice:  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
*Ninninger Equipment Voice:  

==Reception==
The film has earned   at the Japanese box office. 

==References==
 

==External links==
* 

 
 
 

 
 
 
 