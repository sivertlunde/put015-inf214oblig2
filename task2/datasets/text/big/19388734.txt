Spanky (film)
{{Infobox film
| name           = Spanky
| image          = Spanky.JPEG
| image_size     =
| caption        =
| director       = Robert F. McGowan
| producer       = Robert F. McGowan Hal Roach
| writer         = Hal Roach H. M. Walker
| narrator       =
| starring       =
| music          = Leroy Shield Marvin Hatley
| cinematography = Art Lloyd
| editing        = Richard C. Currier
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 19:37
| country        = United States
| language       = English
| budget         =
}}
 short comedy George "Spanky" McFarland.

==Plot==
Although this is a remake of a 1920s silent Our Gang episode called Uncle Toms Uncle, the main character is Spanky. Early scenes of this film were part of Spankys screen test taken back in April 1931 during his first visit to Hal Roach Studios. It included him bug hunting. Meanwhile, his brother  Breezy Brisbane and the rest of the gang are putting on a play of Uncle Toms Cabin. Brisbane is forced to supervise Spanky. Also, Spankys dad refuses to spend money in order to keep the house clean though he has tons of it hidden in a closet.

Spanky disrupts the play the whole time and the play itself is a flop and the kids that came to watch it ruin the play even more by throwing tomatoes and garbage at the gang as they are trying to put the show on. Spanky also finds his dads money and begins throwing it out the window. The kids all run out and try to steal the money and Spankys dad arrives and throws the kids out forcing them to give back the money. The gang helps clean up the money and the father promises to put the money in the bank and spend more on mom and the kids.

==Note==
Coincidentally, Bobby Mallon appeared in both Spanky and Uncle Toms Uncle and was the only actor to do so.

On the syndicated television print, 12 minutes of this episode, which are the scenes of "Uncle Toms Cabin" were edited out in 1971 making the film only nine minutes long. This was due to perceived racism toward African Americans. It has been available on home video on several collections and is becoming available on DVD in its entirety on October 28, 2008.

==Cast==

===The Gang===
* George McFarland as Spanky
* Sherwood Bailey as Sherwood Spud / Aunt Ophelia Matthew Beard as Stymie / Uncle Tom / Topsy
* Dorothy DeBorba as Dorothy / Little Eva
* Bobby Hutchins as Wheezer / Marks the Lawyer
* Kendall McComas as Breezy Brisbane / Simon Legree
* Pete the Pup as Petie

===Additional cast===
* Douglas Greer as Speck, tough kid in the audience
* Bobby Mallon as Other tough kid in the audience
* Billy Gilbert as Spankys father

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 