The Scarlet Letter (1926 film)
{{Infobox film
| name           = The Scarlet Letter
| image          = The Scarlet Letter (1926 film).jpg
| caption        = Film poster
| director       = Victor Sjöström
| producer       = Victor Sjöström
| writer         = Nathaniel Hawthorne Frances Marion
| starring       = Lillian Gish Lars Hanson
| music          = 
| cinematography = Hendrik Sartov
| editing        = Hugh Wynn
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 115 minutes 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 book by the same name, and directed by Victor Sjöström. Louis B. Mayer was reluctant on using Miss Lillian Gish|Gish, fearing opposition from church groups. The film was announced as "Its a real A picture", taking advantage of the A for Adultery.    Prints of the film survive in the MGM/United Artists film archives and the UCLA Film and Television Archive.   

==Cast==
* Lillian Gish - Hester Prynne
* Lars Hanson - The Reverend Arthur Dimmesdale
* Henry B. Walthall - Roger Chillingworth
* Karl Dane - Master Giles
* William H. Tooker - The Governor
* Marcelle Corday - Mistress Hibbins
* Fred Herzog - The Jailer
* Jules Cowles - The Beadle
* Mary Hawes - Patience
* Joyce Coad - Pearl
* James A. Marcus - A Sea Captain
* Nora Cecil - (uncredited)
* Iron Eyes Cody - Young Native American at Dunking (uncredited) 
* Dorothy Gray - Child (uncredited)
* Margaret Mann - (uncredited)
* Polly Moran - Jeering Townswoman (uncredited)
* Chief Yowlachie - Native American (uncredited)

==Production==
The film was the second one Gish made under her contract with MGM. She asked Louis B. Mayer specifically to make it and he reluctantly agreed. Shooting took under two months. 

==Reception==
The film made a profit of $296,000. Scott Eyman, Lion of Hollywood: The Life and Legend of Louis B. Mayer, Robson, 2005 p 125 

==See also==
* Lillian Gish filmography

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 