Ballad in Blue
{{Infobox film
| name           = Ballad in Blue
| image_size     =
| image  	 = "Ballad_in_Blue"_(1964).jpg
| caption        =
| director       = Paul Henreid
| producer       = Herman Blaser
| exec producers = Alexander Salkind Miguel Salkind
| writers        = Paul Henreid Burton Wohl Tom Bell Mary Peach Dawn Addams Piers Bishop
| music          = Bernie Fenton
| cinematography = Robert Huke
| editing        = Raymond Poulton John Trumper
| studio         = Ardmore Studios, Bray, Ireland
| distributor    =
| released       =  
| runtime        = 88 minutes
| country        = United Kingdom
| language       = English
}}
 Paul Henreids theatrical-film directorial efforts. 

==Plot==
Ray Charles plays himself in this film where he helps blind boy David (Piers Bishop) in his struggle to regain his sight. David’s over-protective mother Peggy (Mary Peach) is afraid of the risks connected with restoring his sight. Ray tries to help the whole family, offering the heavy-drinking Peggy’s heavy-drinking partner Steve (Tom Bell) an opportunity to work with his band. 

Sub-plots include fashion designer Gina (Dawn Addams) trying to lure Steve; and Margaret (Lucy Appleby) encouraging David to sneak out and wander around London late at night on a mischievous adventure.

==Cast==
* Ray Charles as Himself Tom Bell as Steve Collins
* Mary Peach as Peggy Harrison
* Dawn Addams as Gina Graham
* Piers Bishop as David
* Betty McDowall as Mrs. Babbidge
* Lucy Appleby as Margaret
* Joe Adams as Fred
* Robert Lee Ross as Duke Wade
* Anne Padwick Bus Conductress
* Monika Henreid as Antonia
* Brendan Agnew as Antonias protector
* Vernon Hayden as Headmaster
* Leo McCabe as Doctor Leger
* The Raelettes as Themselves

==References==
 
 
 
 
 

==External links==
*  

 
 
 
 
 