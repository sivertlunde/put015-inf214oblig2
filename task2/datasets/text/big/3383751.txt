Zwei Frauen
{{Infobox film
| name           = Zwei Frauen
| image          = Zwei frauen.jpg
| image size     =
| caption        =
| director       = Carl Schenkel
| producer       = Michael Röhrig Carl Schenkel
| writer         = Bea Hellmann Carl Schenkel
| narrator       =
| starring       = Jami Gertz Martha Plimpton George Peppard Bruce Payne Rip Torn
| music          = Anne Dudley
| cinematography =
| editing        =
| distributor    = Moviestore Entertainment 1989
| runtime        = 104 min.
| country        = Germany / United States|U.S. English
| budget         =
| preceded by    =
| followed by    =
}}

Zwei Frauen (American title: Silence Like Glass) is a 1989 German dramatic film. (Though made in Germany, it is set in America and features an American cast with all English dialogue.) The film stars Jami Gertz, Martha Plimpton, George Peppard, Bruce Payne, and Rip Torn.

==Plot==
The main character of the film, Eva, is a wealthy, snobbish ballerina who is diagnosed with cancer. She is admitted into the youth cancer ward at a hospital where she is forced into humility by her disease. She rooms with fellow cancer patient Claudia (Martha Plimpton), and the majority of the film revolves around their interactions together in their hospital room. She is treated with concern and compassion by Dr Burton. 

Ultimately Eva goes into remission and is able to continue her life, but not before witnessing another young womans death and also assisting Claudia to suicide.

The film was nominated for Outstanding Feature Film at the German Film Awards. Notably, actress Plimpton shaved her head for this role, a move that would steer the direction of her career for the next several years. (She would again appear bald in the film Parenthood (1989 film)|Parenthood and accompanied then-boyfriend River Phoenix to the Oscars with a bald head.)

==Cast==
*Jami Gertz as Eva Martin
*Martha Plimpton as Claudia Jacoby
*George Peppard as Mr. Martin
*Bruce Payne as Dr. Burton
*Rip Torn as Dr. Markowitz

==References==
 

==External links==
*  

 

 
 
 
 
 


 