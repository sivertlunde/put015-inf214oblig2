Superman II
 
{{Infobox film
| name           = Superman II
| image          = Superman II.jpg
| caption        = North American teaser poster
| screenplay     = {{Plainlist|
* Mario Puzo David Newman
* Leslie Newman
* Tom Mankiewicz   }}
| story          = Mario Puzo
| based on       =   Joe Shuster}}
| starring       = {{Plainlist|
* Gene Hackman
* Christopher Reeve
* Ned Beatty
* Jackie Cooper
* Sarah Douglas
* Margot Kidder
* Jack OHalloran
* Valerie Perrine
* Susannah York
* Terence Stamp }}
| music          = {{Plainlist|
* Ken Thorne
* John Williams   }}
| cinematography = {{Plainlist|
* Robert Paynter  
* Geoffrey Unsworth   }}
| editing        = {{Plainlist|
* John Victor-Smith  
* Stuart Baird   }}
| studio         = {{Plainlist|
* Dovemead Ltd.
* Film Export A.G.
* International Film Production }}
| distributor    = Warner Bros.
| director       = {{Plainlist|
* Richard Lester
* Richard Donner   }}
| producer       = Pierre Spengler
| released       =  
| runtime        = 127 minutes
| country        = United Kingdom 
| language       = English
| budget         = $54 million
| gross          = $108.1 million 
}}
Superman II is a 1980    British superhero film directed by Richard Lester. It is a sequel to the 1978 film Superman (1978 film)|Superman and stars Gene Hackman, Christopher Reeve, Terence Stamp, Ned Beatty, Sarah Douglas, Margot Kidder, and Jack OHalloran. The film was released in Australia and mainland Europe on December 4, 1980,    and in other countries throughout 1981. Selected premiere engagements of Superman II were presented in Megasound, a high-impact surround sound system similar to Sensurround.

Superman II is well known for its controversial production. The original director  , restoring as much of Donners original conception as possible including deleted footage of Marlon Brando as Jor-El.

The film received positive reviews from film critics who praised the visual effects and story and was a box office success. Three years after the films release, a second sequel, Superman III, was released with Lester returning as director.

==Plot==
 
Prior to the destruction of Krypton (comics)|Krypton, the criminals General Zod (Terence Stamp), Ursa (Sarah Douglas) and Non (Jack OHalloran) are sentenced to banishment into the Phantom Zone for insurrection and murder, amongst other crimes.
 hydrogen bomb, which had been launched into space by Superman (Christopher Reeve) after foiling a terrorist plot to blow up Paris. The three Kryptonian criminals are freed from the zone, finding themselves with super-powers granted by the yellow light of Earths sun. After attacking human astronauts on the Moon and the small town of East Houston, Idaho (which they mistake as being capital city of "Planet Houston" due to NASAs transmissions), the three travel to the White House and force the President of the United States (E. G. Marshall|E.G. Marshall) to kneel before him on behalf of the entire planet during an international television broadcast. When the President pleads for Superman to save the Earth, Zod demands that Superman come and "kneel before Zod!".
 Pepper Martin).  It is there that Clark and Lois learn of Zods conquest of the world. Realizing that humanity cannot fight Zod themselves, Clark decides to return to the fortress to try to reverse the transformation.

Lex Luthor (Gene Hackman) escapes from prison with Eve Teschmachers help, leaving Otis behind. Luthor and Teschmacher find and infiltrates the fortress before Superman and Lois arrive. Luthor learns of Supermans connection to Jor-El and General Zod. He tells Zod about Superman being son of, Jor-El, their jailer, and offers to lead him to the Man of Steel in exchange for control of Australia. The three Kryptonians form an alliance with Luthor and go to the offices of the Daily Planet. Superman arrives, after having found the green crystal and reversing the transformation process, and battles the three Kryptonians in Metropolis (comics)|Metropolis. Zod realizes Superman cares for the innocent humans, and takes advantage of this weakness by threatening bystanders. To protect the civilians and the city, Superman realizes the only way to stop Zod and crew is to lure them to the fortress. Superman flies off while Zod, Ursa, and Non pursue, carrying Lois and Luthor. Upon arrival, Zod declares Luthor has outlived his usefulness and plans to kill both him and Superman. Superman tries to get Luthor to lure the three into the crystal chamber to depower them, but Luthor, eager to get back in Zods favor, reveals the chambers secret to the villains. Zod forces Superman into the chamber and activates it. Afterwards, he forces Kal-El to kneel before him, Superman does so, and Zod extends his hand. However, when Superman squeezes it, Zod grimaces in pain. He realizes too late that Superman reconfigured the chamber to expose the trio to red sunlight, whilst he was protected against. Superman easily defeats Non and Zod, while Lois knocks Ursa into a pit. Superman flies back to civilization, returning Luthor to prison and Lois home.

At the Daily Planet the following day, Clark finds Lois upset about knowing his secret. He then kisses her, using his abilities to wipe her mind of her knowledge of the past few days.  Later, Clark has a rematch with the truck driver who beat him up earlier and defeats him easily. Superman then restores the damage done by Zod, replacing the flag on top of the White House and promising the president to never let him down again.
 

==Cast==
* Gene Hackman as Lex Luthor: Evil criminal genius and Supermans nemesis. Armed with vast resources and scientific brilliance, Luthors contempt for mankind is only surpassed by his hatred for Superman. Luthor strikes a bargain with the three Kryptonian criminals in an effort to destroy Superman.   
*   reporter Clark Kent. Supermans abilities include: X-ray and heat vision, vast strength, speed and invulnerability, super-intelligence, flight and the hitherto unknown ability to throw the S symbol from his costume as a plastic trapping device of some description. 
* Ned Beatty as Otis (Superman)|Otis: Luthors incompetent henchman.
* Jackie Cooper as Perry White: Mercurial editor-in-chief of the Daily Planet newspaper and Lois and Clarks boss.
*   of an astronaut she kills.
* Margot Kidder as Lois Lane: The ace reporter for the Daily Planet and Supermans love interest. Lois, is a driven career journalist, who lets nothing stand in the way of breaking the next big story and scooping rival reporters. While ignoring the potential consequences that sometimes put her in peril. She finds out that Clark is Superman, but her memory is erased when Clark kisses her.
*  , who easily matches Supermans strength but has the intelligence and sometimes curiosity of a child and communicates only with guttural grunts and growls. Though he lacks the mental ability to use his powers effectively, he does however possess the same taste for destruction as his Kryptonian companions.
* Valerie Perrine as Eve Teschmacher: Lex Luthors beautiful assistant and girlfriend who helps Lex Luthor escape from prison.
* Susannah York as Lara: Jor-Els wife and Supermans biological mother.
* Clifton James as Sheriff.
* E.G. Marshall as the President of the United States.
* Marc McClure as Jimmy Olsen: Teenaged photographer at the Daily Planet.
*  cal leader of three Kryptonian criminals banished to the Phantom Zone and unwittingly set free by Superman. Zod, upon landing on Earth and gaining the same super powers as Superman, immediately views humans as a weak and insignificant sub-species and imposes his evil will for world dominance. However, his arrogance causes him to quickly become bored with his powers and he is almost disappointed at how little of a challenge humans are. His insatiable lust for power is replaced however by revenge when he learns that the son of Jor-El stands in the way of his absolute rule of the planet.
* Richard Griffiths as Terrorist #3
* John Ratzenberger as NASA Controller #1
* Shane Rimmer as NASA Controller #2
* Angus MacInnes as Prison Warden. 
* Antony Sher as Bell Boy.
* Gordon Rollings as Fisherman.
* Marcus DAmico as Willie.
* Richard LeParmentier as Reporter. Pepper Martin as Rocky.
* Eugene Lipinski as News Vendor. 

Gene Hackman, Valerie Perrine, Ned Beatty, and E.G. Marshall are the only actors who did not participate in the films reshoots under the direction of Richard Lester. Where additional shots were needed for continuity, Lester used body doubles in place of the original actors. Marlon Brandos scenes were excised entirely, due to the high fee the actor had demanded for the use of his footage in the film.

According to the 2006 documentary You Will Believe: The Cinematic Saga of Superman, Sarah Douglas was the only cast member to do extensive around-the-world press tours in support of the movie and was one of the few actors who held a neutral point of view in the Donner-Lester controversy.

Richard Donner briefly appears in a "walking cameo" in the film. In the sequence where the de-powered Clark and Lois are seen approaching the truck-stop diner by car, Donner appears walking "camera left" past the drivers side. He is wearing a light tan jacket and appears to be smoking a pipe. In his commentary for Superman II, Ilya Salkind states that the inclusion of his cameo in that scene is proof that the Salkinds held no animosity towards Donner, because if there were, then surely they would have cut it out. Conversely, Donner has used his inclusion in the scene to debunk praise heaped on Lester around the release of the film where Lester took credit for the intense nature of the "bully" scene in the diner, pointing out that he (Donner) filmed the scene and not Lester.

==Production==
  Alexander and The Three The Four Musketeers (1974), as an uncredited line producer on Superman. 

On March 15, 1979, shortly after the release of Superman, the Salkinds decided to replace Donner with Lester as director for Superman II. The decision was controversial amongst the cast and crew. Creative consultant Tom Mankiewicz, editor Stuart Baird, and actor Gene Hackman declined on returning for the sequel in support of Donner. Hackman, who had already completed many of his scenes under Donner, had had his role cut down and re-filmed with a body double. Actor Marlon Brando, who finished all his scenes for both Superman films early into production, successfully sued the Salkinds for $50 million over grossed profits gained from the first film. In response, the Salkinds cut Brando from Superman II, replacing his scenes with actress Susannah York.
 John Barry David and Leslie Newman. The new script featured several newly conceived scenes including the Eiffel Tower opening sequence and Clark rescuing Lois at Niagara Falls. However, under strict guidelines from Directors Guild of America, Lester needed to re-shoot several scenes Donner had already completed in order to receive full directorial credit. Location shooting took place in Canada, Paris, Norway and St Lucia, while Metropolis (which was shot in New York for the first movie) was filmed entirely on the back lot at Pinewood.  Superman II finally finished filming in March 1980.

Despite all the difficulties of the production, and with only a few noticeable shifts in tone between the two directors scenes (Lesters approach is lighter and more slapstick, as opposed to the verisimilitude Donner fought to bring to the film), it was noted by critics to be a remarkable and coherent film, highlighted by the movies battle sequence between Superman and the three Phantom Zone prisoners on the streets of Metropolis (comics)|Metropolis. Scenes filmed by Donner include all the Gene Hackman footage, the Moon sequences, the White House shots, Clark and the bully, and a lot of the footage of Zod, Ursa and Non arriving at the Daily Planet. Since the Lester footage was shot two years later, both Margot Kidder and Christopher Reeve look different between the Lester and Donner footage. Reeve appears less bulked up in Donners sequences (filmed in 1977), as he was still gaining muscle for the part. Kidder also has dramatic changes throughout; in the montage of Lester-Donner material, shot inside the Daily Planet and the Fortress of Solitude near the movies conclusion, her hairstyle, hair color, and even make-up are all inconsistent. Indeed, Kidders physical appearance in the Lester footage is noticeably different; during the scenes shot for Donner she appears slender, whereas in the Lester footage she looks frail and gaunt.

==Later releases== ABC and CBC telecasts, though edited differently, were derived from the European-Australian TV edit). Australians will notice scenes they originally viewed at cinemas in the deleted scenes menu on DVDs and notice some of the one liners they originally heard placed back in the Richard Donner cut.

All four Superman films received special or deluxe edition releases in 2006, coinciding with the release of  . Following the original Superman II script, the Donner cut features less than 20% footage filmed by replacement director Richard Lester and restores several cut scenes, including all the Marlon Brando footage and Lois jumping out of the Daily Planet to try and get Superman to reveal his identity to her.  It also restructures the beginning of the movie so that the outer space detonation of the Hackensack, New Jersey|Hackensack-bound nuclear missile from Superman: The Movie is responsible for releasing Zod and his companions from the Phantom Zone (and not the blast from the Eiffel Tower H-bomb).  The originally intended ending for Superman II, which was used instead for the climax of Superman: The Movie (where Superman reverses time) was also restored for the Donner cut, and incorporates footage Donner had shot in 1977 for this ending of Superman II.

==Score==
  Pick Up the Pieces", which appears both in the bar in Idaho as well as during Clarks second encounter with Rocky. The music was performed by the London Symphony Orchestra at the CTS Studios, Wembley, London in the spring of 1980.  The soundtrack was released on Warner Bros. Records, with one edition featuring Laser engraving|laser-etched "S" designs repeated five times on each side. 

==Release and reception==
Unlike its predecessor, Superman II did not open simultaneously around the world and had staggered release dates in an attempt to  maximize its box office returns. Originally opening in Australia on December 4, 1980, followed by selected European countries, it would be a further six months before it premiered in America, on June 1, 1981 at the National Theater, Broadway.

The film received much praise from critics. It holds an 88% "fresh" rating on Rotten Tomatoes, with the summary saying, "The humor occasionally stumbles into slapstick territory, and the special effects are dated, but Superman II meets, if not exceeds, the standard set by its predecessor.",  on Metacritic, the film has a score of 87 (out of 100); indicating universal acclaim.  Roger Ebert, who gave the original film very high acclaim  also praised Superman II, giving it four out of four stars, claiming that "Superman II begins in midstream, and never looks back..."  Even Reeve claimed that Superman II is "the best of the series". 
 Academy of Best Science Best Actor Best Actress, Best Music.

British cinema magazine Total Film named Terence Stamps version of General Zod No.32 on their Top 50 Greatest Villains of All Time list (beating out the No.38 place of Lex Luthor) in 2007.  Pop culture website IGN placed General Zod at No.30 on their list of the Top 50 Comic Book Villains while commenting "Stamp is Zod" (emphasis in original). 

Anti-smoking campaigners opposed the film as the largest sponsor of Superman II was the cigarette brand Marlboro (cigarette)|Marlboro, who paid $43,000 (approx £20,000), for the brand to be shown 22 times in the film. Lois Lane was shown as a chain smoker in the film, although she never smoked in the comic book version.  A prop included a truck sign written with the Marlboro logo, although actual vehicles for tobacco distribution are unmarked, for security reasons.  This led to a congressional investigation.  

==Broadcast television versions==

===American Broadcasting Company===
In 1984, when Superman II premiered on television, 24 minutes were re-inserted into the film (17 minutes on   — that question was answered in the extended versions where Superman reverses the rotation of the Earth where one of the things he does involves preventing Lex Luthor from escaping from prison. The ending of the extended cuts also has Superman, with Lois standing beside him, destroying the Fortress of Solitude.

More specifically:
* In the ABC-TV version, Superman passes a   as a bridge between Superman saving Air Force One and his conversation with Jor-El after his first night. 
* At the end of the film, Clark Kent bumps into a large bald man, which reminds him to go to the diner to face the obnoxious trucker who beat him up earlier.
* Superman destroys the Fortress of Solitude.
* The Phantom Zone villains land outside the Fortress of Solitude with Lex Luthor and Lois Lane, trying to figure out how to get in. Zod using Non frightening a dog. heat vision, during dinner with Lois at the Fortress of Solitude. Ursa on the Moon.
* The three Kryptonian villains are arrested in the TV version. In The Richard Donner Cut, Superman reversed the rotation of the Earth to keep the three Kryptonian criminals from being freed from the Phantom Zone.

Much of the added footage was later restored for the 2006  .

Also, there were various edits due to content issues:
* Much violence in the opening White House scene was left out.
* Much of the bullys line in the bar ("I dont like your meat anyway!") sounds like ("I dont want your meat anyway").
* About 35 seconds of the "battle of Metropolis Road" (Superman flying over Metropolis River) was deleted.
* Some language and profanity were re-dubbed.

===Canadian version===
During the 1980s, CFCF12 cable 11 screened an edition of Superman II that was differently edited to that to the one shown on in the United States on ABC. This particular version has only been screened once in Canada. The first Canadian broadcast of Superman II had an additional few seconds of dialogue as Luthor and Miss Tessmacher were stopped on a snow bank admiring the Fortress of Solitude. In the first U.S. broadcast (the same evening), the scene begins abruptly as Luthor starts the snow mobile immediately after the dialogue sequence.

Scenes seen in the Canadian version but not in the ABC version include:
* A little girl watching the destruction of East Houston by the Kryptonians on TV.
* Longer conversation between Lois and Superman after he destroys the Fortress of Solitude.
* Lex Luthor taking Perry Whites coffee during the Times Square battle.
* Lex and Miss Tessmacher admiring the Fortress of Solitude.
* Lexs negotiating with Superman after they leave the fortress is longer.

All the footage mentioned that had been added for various network telecasts were incorporated into an even longer cut of the film that aired in some countries in Europe (the other U.S./Canadian cuts were derived from this version). Prepared by the Salkinds production company, it is this 146-minute version that some Superman fans remastered from the best-possible materials into a professionally made "Restored International Cut" DVD for availability on one of the many Superman fan sites. However, such plans backfired when Warner Bros. threatened legal action against the bootleg release. The RIC, like the longer version of Superman, may still be found on Internet forums, torrent sites, and in science fiction conventions.

==In other media==

===Comics===
Supermans publisher DC Comics published a commemorative magazine of Superman II in 1981. Published as DC Special Series #25, it was produced in "Treasury format" and included photos and background photos, actor profiles, panel-to-scene comparisons, and pin-ups.    
 reboot of the character following the limited series Crisis on Infinite Earths.

In the film, after attacking the White House, Lex Luthor enters the Oval Office to make a deal with the Kryptonians. By the end of the scene, he is sitting behind the Presidents desk. In the comics (in the year 2000), Lex Luthor ran for President of the United States and won. 
 Ursa and Non made Last Son" story arc, co-written by Richard Donner.) 

===Television=== Superman theme was included when Reeve made his first appearance, and was later used in the series finale.  Margot Kidder, Marc McClure (Jimmy Olsen), and Helen Slater (Supergirl) have also made appearances on the show. Annette OToole (Lana Lang in Superman III) played Martha Kent.
 Young Justice, in the episode "Satisfaction" of its second season, Lex Luthor appears briefly talking to one of his assistants on the phone, who is called Otis, as a reference to the character in the films.

==See also==
*  

==References==
 
;Cited works
*  

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 