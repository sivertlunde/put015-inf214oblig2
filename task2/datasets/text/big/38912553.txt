Reach Me
{{Infobox film
| name           = Reach Me
| image          = Reach-me-poster.jpeg
| alt            = 
| caption        = Promotional poster
| director       = John Herzfeld
| producer       =  Rebekah Chaney   Cassian Elwes   Buddy Patrick  John Herzfeld 
| writer         = John Herzfeld Kevin Connolly   Tom Berenger   Danny Aiello  Omari Hardwick   Nelly   David OHara   Danny Trejo
| studio         = Seraphim Films Productions 
| distributor    = Millennium Entertainment
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Kevin Connolly, Lauren Cohan, Kelsey Grammer, and Tom Berenger. 
The film was produced by Rebekah Chaney, Cassian Elwes, Buddy Patrick, John Herzfeld. 

==Plot==
With an ensemble cast, this film tells interwoven stories from a diverse group of people who are united by one thing: a powerful book published by an unknown mysterious author. When the books positive message goes viral, a journalist and his editor, a former inmate, a hip-hop mogul, an actor and an undercover cop are inspired to change their lives by facing their own fears.

==Cast==
* Sylvester Stallone as Gerald
* Kyra Sedgwick as Collette
* Thomas Jane as Wolfie
* Lauren Cohan as Kate
* Kelsey Grammer as Angelo AldoBrandini Kevin Connolly as Roger
* Tom Berenger as Teddy
* Nelly as E-Ruption
* Omari Hardwick as Dominic
* Terry Crews as Wilson
* Danny Trejo as Vic
* Danny Aiello as Father Paul
* Ryan Kwanten as Jack Burns
* David OHara as Thumper
* Elizabeth Henstridge as Eve
* Rebekah Chaney as Denise-Denise
* Chuck Zito

== Production==
While shooting in 2013, funding for the film dried up when one of the investors backed out of production during principal photography. Herzfeld, Stallone and producers Rebekah Chaney and Cassian Elwes started a Kickstarter campaign to raise their goal of $250,000 by September 19.   Despite reaching the $250,000 goal on Kickstarter, the production team decided to withdraw its Kickstarter campaign and start again with the competing crowdfunding platform Indiegogo, citing its broader and more flexible capabilities.  The Indiegogo campaign set a goal for $50,000 starting on September 17 and ended on September 22 with a total of $181,140. 

==Marketing==
On July 7, 2014, the first official trailer for John Herzfelds Reach Me was released. 

==Reception== a rare 0% rating on Rotten Tomatoes,  with the consensus "Featuring a bewildering array of talented actors pummeled by disjointed direction and a dull, hackneyed script, Reach Me is so fundamentally misbegotten that its title reads more like a threat."

==See also== Grand Canyon
* Magnolia (film)|Magnolia Third Person
* Crash (2004 film)|Crash

==References==
 

==External links==
*  
*  

 


 
 
 
 
 
 
 
 
 