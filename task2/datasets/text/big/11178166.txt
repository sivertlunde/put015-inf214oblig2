Go, Go, Second Time Virgin
{{Infobox film
| name           = Go, Go, Second Time Virgin
| image          = Go, Go Second Time Virgin (DVD cover art).jpg
| image_size     = 
| caption        = Cover of the December, 2000 Region 1 DVD release
| director       = Kōji Wakamatsu
| producer       = Kōji Wakamatsu Kazuo Gaira Komizu
| narrator       = 
| starring       = Michio Akiyama Mimi Kozakura
| music          = Meikyu Sekai
| cinematography = Hideo Itō
| editing        = 
| distributor    = Wakamatsu Productions
| released       = 1969
| runtime        = 65 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}} Japanese film by Kōji Wakamatsu.

==Plot==
Poppo, a teenage girl, is raped by four boys on the roof of a seven-story apartment building. She asks them to kill her, but they mock her and leave. Tsukio, a teenage boy, has been watching the rape passively. Over the next couple days, Poppo and Tsukio begin a relationship, telling each other of their troubled past and philosophizing about their fate. Poppo describes earlier rapes, which are shown in flashback. In a color flashback, Tsukio tells of his own recent sexual abuse at the hands his parents and another couple, all of whom he has stabbed to death. Poppo repeatedly asks Tsukio to kill her, but he refuses.

When the gang returns and again rapes Poppo, Tsukio kills each of them and their three girlfriends. While he is doing this, Poppo follows him complaining that he refuses her request, yet is killing the gang. The story ends with Poppo and Tsukio both jumping off the apartment roof to their deaths. 

==Cast==
* Mimi Kozakura as Poppo
* Michio Akiyama as Tsukio

==Background==
Kōji Wakamatsu had worked for Nikkatsu studios between 1963 and 1965, and directed 20 exploitation films during that time. When his pink film   (also known as Secret Acts within Four Walls) (1965) ran afoul of the government, and the studio did not support him, Wakamatsu quit Nikkatsu to form his own production company. His independent films of the late 1960s were very low-budget, but often artistically done works, usually concerned with sex and extreme violence mixed with political messages. Some critics have suggested that these films were an intentional provocation to the government, in order to generate free publicity resulting from censorship controversies.  According to Patrick Macias, "No one had up to that point, or since, filmed porn with as overtly politically radical and aesthetically avant-garde an agenda as Wakamatsu had."   

These films were usually produced for less than 1,000,000 yen (about $5,000), necessitating extreme cost-cutting measures including location shooting, single-takes, and natural lighting. Usually predominantly in black and white, Wakamatsu occasionally uses bursts of color in these films for theatrical effect.  Like many of Wakamatsus films of this period, Go, Go, Second Time Virgin is set mostly at one location—an apartment rooftop. It was shot in four days with a minimal budget.   

==Themes and style==
Reviewer Donato Totaro calls the film an "alternating teenage expression of the sex (Eros) and death (Thanatos) drives, with sex continually made ugly and death the ultimate conqueror."  He further points out that Wakamatsu often puts moments in his films which appear to criticize the standard misogynistic tone of the Pink film genre. At one point in Go, Go, Second Time Virgin, Wakamatsu has Poppo look directly into the camera and address no character in the film, but the theatrical audience, saying, "My mother was gang raped, and then she gave birth to me. Are the tears we two shed when raped, the tears women shed? What tears? What sadness? I am not a woman. I’m not sad, not sad at all. I don’t cry. I’m never sad. I…I’m not at all sad…..FUCK YOU..FUCK YOU." 

Patrick Macias says that writer Masao Adachi, along with Wakamatsu, is responsible for much of Go, Go, Second Time Virgin s thematic, political and stylistic concerns.  According to Macias, this film, like other Wakamatsu films of the time, "combined, in still unique manner, disjunctive New Wave style, existentialist dread, sex, sadism, and gore, all on a ridiculously shoestring budget." 
 spiritual "Sometimes I Feel Like a Motherless Child" as well as Patty Waters avant-garde jazz arrangement of "Black Is The Color Of My True Loves Hair" are heard on the soundtrack. Desser, p.104. 

==Critical reception==
Though long relatively unknown outside of Japan,    Kōji Wakamatsu has been called "the most important director to emerge in the pink film genre,"  and one of "Japans leading directors of the 1960s."  Go, Go, Second Time Virgin is one of Wakamatsus best-known films,  but discussion of it in English has been hampered by its long lack of availability to the English-speaking world.

Compared to Wakamatsus earlier films The Embryo Hunts in Secret and Violated Women in White, David Desser, writing in 1988, calls Go, Go, Second Time Virgin "by any standards a more interesting and less painful film to watch..."  In 1998, Thomas and Yuko Mihara Weisser, in their Japanese Cinema Encyclopedia: The Sex Films, gave the film a middling review which concludes, "Wakamatsu remains an interesting director due to his obstinate disregard for social standards. But unfortunately, he doesnt make the alternative, cultural anarchy, seem very appealing either." 

With its release on region 1 DVD in December 2000, English-language criticism of Go, Go Second Time Virgin increased substantially. In his review of the DVD, Daniel Wible of Film Threat says Go, Go, Second Time Virgin is "clearly a film that will enthrall some with its ultra-stylized perversions while horrifying nearly everyone else." He calls the movie "a serious and profound work of filmic art," adding " rom the experimental, jazzy score by Meikyu Sekai to the shocking use of color in a predominantly black and white film, "Go Go" is a director’s film all the way. Wakamatsu gets away with his graphic images of sex and violence because they are staged in ways that resonate emotionally for both the characters and the audience. 

Totaro points out that in spite of the limited budget and location, "Wakamatsu sacrifices little in terms of aesthetics, using both the location and the cinemascope frame to increase the character’s psychological expression."  Though warning that " he relentless, downbeat atmosphere will prove tough going for many viewers," Mondo-Digital website says the film "packs a tremendous amount of artistry into every scene."   

Sarudama.coms review of the film calls it "a rather dismal tale of primitive morality in the face of degradation, humiliation and abuse," and "a bizarre and despairing film which will undoubtedly cause viewers to (re)consider the role of social morality." 
 The Headless Chickens also recorded a song named after and loosely based on this film. 

==Notes==
 

== Sources ==
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 