Dark Water (2005 film)
{{Infobox film
| name = Dark Water
| image = Darkwaterposter.jpg
| caption = Theatrical release poster
| director = Walter Salles
| producer = Roy Lee Doug Davidson Bill Mechanic
| writer = Rafael Yglesias
| starring = Jennifer Connelly Tim Roth John C. Reilly Dougray Scott Ariel Gade Pete Postlethwaite Perla Haney-Jardine
| music = Angelo Badalamenti
| editing = Daniel Rezende
| cinematography = Affonso Beato
| studio = Touchstone Pictures Vertigo Entertainment Buena Vista Pictures
| released =  
| runtime = 105&nbsp;minutes
| language = English
| budget = $30 million 
| gross = $49,483,352   
}}
 thriller film 2002 Japanese film of the same name, and also stars John C. Reilly, Pete Postlethwaite, Perla Haney-Jardine, Dougray Scott and Ariel Gade.

The film was released on July 8, 2005, and grossed almost $50&nbsp;million worldwide. 

==Plot==

The film opens in 1974, as a young girl, Dahlia, stands outside after school in the rain, waiting for her mother.

Flash forward to 2005, the audience sees a grown-up Dahlia (Jennifer Connelly) in the midst of a bitter mediation with ex-husband, Kyle (Dougray Scott), over custody of their daughter, Cecilia (Ariel Gade). Kyle wants Cecilia to live closer to his apartment in Jersey City, but Dahlia wants to move to Roosevelt Island, where she has found a good school. Kyle threatens to sue for full custody because he feels the distance is too great. He also claims that Dahlia is "mentally unstable."

Dahlia and Cecilia see an apartment in a complex on Roosevelt Island, which is just a few blocks from Cecilias new school. The superintendent of the dilapidated building is Mr. Veeck (Pete Postlethwaite). The manager is Mr. Murray (John C. Reilly). During the tour, Cecilia sneaks to the roof where she finds a Hello Kitty backpack near a large water tank. They leave the bag with Veeck, and Murray promises Cecilia that she can have it if no one claims it. Cecilia, who had disliked the apartment, now wants desperately to live there. Dahlia agrees to move in.

Shortly after, the bedroom ceiling begins to leak dark water. The source is the apartment above, 10F, where the Rimsky family lived up until a month ago. Dahlia enters 10F and finds it flooded, with dark water flowing from every faucet, the walls and toilet. She finds a family portrait of the former tenants—a mother, father, and a girl Cecilias age. Dahlia complains to both Veeck and Murray about the water, but the former does little about it despite the insistence of the latter. Meanwhile, Cecilia develops a strong bond of friendship with an imaginary friend called Natasha.

The ceiling, shoddily patched by Veeck, leaks again. At school, Cecilia appears to get into a fight with Natasha, who appears to control her hand while painting. Shes taken to the girls bathroom where she passes out after dark water gushes from the toilets and sinks. Dahlia, who is meeting with her lawyer, Jeff Platzer, cant be reached so Kyle picks Cecilia up and takes her to his apartment. Later on that night, Dahlia is feeling better, now that Jeff will have her apartment fixed and that Cecilia is safe with Kyle. Dahlia hears footfalls from the hallway outside of her apartment going up to the roof. She sees that water is spilling out of the water tank. She climbs up the ladder, opens the hatch to the water tank and finds Natashas body floating in the water. Dahlia is stunned and Natashas eyes snap open. 

When police arrive, Veeck is arrested for his negligence. He was aware of her body, which was why he refused to fix the water problem plaguing the complex. While Murray is questioned, Dahlia and her lawyer discover with cold irony that Natashas parents had left her behind. With each parent assuming she was with the other, Natasha was left to fend for herself and it led to her eventual death.

Dahlia agrees to move closer to Kyle so shared custody will be easier. As Dahlia packs, Cecilia is taking a bath. A girl in a hooded bathrobe comes out of the bathroom, wanting Dahlia to read to her. When she hears voices in the bathroom, she realizes that the girl is Natasha. Natasha begs Dahlia not to leave her, but Dahlia rushes into the bathroom to save Cecilia. Natasha then locks Cecilia in the shower compartment and holds her underwater. Dahlia pleads with Natasha to let her daughter go, promising to be her mother forever. Natasha lets Cecilia go and floods the apartment, causing Dahlia to die from drowning. Her and Natashas spirits are shown walking down the hallway and the roof.

Kyle picks up Cecilia from the police station. Weeks later, the two go back to pick up the rest of her belongings. Cecilia has a flashback of her and her mother looking at pictures together, and in the elevator, her mothers ghost braids her hair and comforts her — telling her she will always be there. Kyle, momentarily horrified with a malfunction in the elevator, the weird behavior of his daughter, and perhaps noticing her hair had been braided, finally takes her to his apartment in Jersey City.

==Cast==
*Jennifer Connelly as Dahlia Williams
*Dougray Scott as Kyle Williams
*John C. Reilly as Mr. Murray
*Ariel Gade as Cecilia "Ceci" Williams
*Perla Haney-Jardine as Natasha Rimsky/Young Dahlia
*Tim Roth as Jeff Platzer
*Pete Postlethwaite as Mr. Veeck
*Camryn Manheim as Cecis Teacher

==Reception==
Reviews of the film are mixed. It currently holds a 46% "Rotten" rating at Rotten Tomatoes. 

==Home media==
  UMD video version of the film. A Blu-ray Disc was released on October 17, 2006 but it only contains the widescreen PG-13 theatrical version and fewer extras than the DVD releases.

==See also==
*List of ghost films

==References==
 

==External links==
 
 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 