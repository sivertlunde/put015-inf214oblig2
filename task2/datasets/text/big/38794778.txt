Dharma (1998 film)
{{Infobox film
| name           = Dharma 
| image          = 
| image_size     =
| caption        =  Keyaar
| producer       = A. Abbas Rowther
| screenplay     = Keyaar
| story          = Robin Henry
| starring       =  
| music          = Ilaiyaraaja
| cinematography = Rajarajan
| editing        = R. T. Annadurai
| distributor    =
| studio         = Rowther Films
| released       =  
| runtime        = 160 minutes
| country        = India
| language       = Tamil
}}
 1998 Tamil Tamil action film directed by Kothanda Ramaiah|Keyaar. The film features Vijayakanth and Preetha Vijayakumar in lead roles. The film, produced by A. Abbas Rowther, had musical score by Ilaiyaraaja and was released on 9 July 1998. The film is a remake of the Hindi film Ziddi (1997 film)|Ziddi.  

==Plot==

Dharma (Vijayakanth) lives with his lawyer father (Jaishankar), his mother (Vadivukkarasi), his journalist brother Vijay (Thalaivasal Vijay) and beloved sister Geetha (Chippy (actress)|Shilpa). Dharma and Sharmila (Preetha Vijayakumar) fall in love with each other. Dharma is an angry man who cannot tolerate injustice. One day, his sister is molested by a rowdy Raja and Dharma kills him in public. Thus, he is sent in jail. In the meantine, Ranjith (Ranjith (actor)|Ranjith), Dharmas friend, becomes an Assistant Commissioner of Police (India)|ACP.

On his return, Dharma becomes a powerful gangster who punishes the rowdies in his own way and helps the poor. The honest chief minister (S. S. Rajendran) gives free hand to arrest all the goons including Dharma. Dharmas sister, Geetha, gets married with Ranjith.
 Mansoor Ali Khan), a drug smuggler, Khan (Ponnambalam (actor)|Ponnambalam), a killer, and Amarnath (Kazan Khan), a land grabber, work under a corrupted politician Chakravarthy (Vinu Chakravarthy). They decide to kill the current Chief Minister but Vijay has listened their plan and immediately informs Ranjith. Surprisingly, Ranjith kills Vijay by behind. In fact, Ranjith is Rajas brother (the man who was killed by Dharma) and wants to take revenge on Dharma.

Later, the Chief Minister is severely injured by the rowdies but Dharma saves him and hides him in a secured place. In the meanwhile, Geetha finds out that her brother Vijay was killed by her husband Ranjith and Ranjith also kills her.

The police department seeks Dharma for kidnapping the Chief Minister. What transpires later forms the crux of the story.

==Cast==

*Vijayakanth as Dharma
*Preetha Vijayakumar as Sharmila Shilpa as Geetha
*Jaishankar as Dharmas father Ranjith as Ranjith
*Thalaivasal Vijay as Vijay
*Vinu Chakravarthy as Chakravarthy Mansoor Ali Khan as Daas Ponnambalam as Khan
*Kazan Khan as Amarnath
*S. S. Rajendran as the Chief Minister Manorama as Sharmilas grandmother
*Vadivukkarasi as Dharmas mother
*Aswini 
*LIC Narasimhan
*Rajasekhar
*Singamuthu
*Master Aravind
*Baby Aarthi

==Production==
Since climax of original Hindi film took 90 days to shoot, KR decided to add the bomb blast scenes from Hindi film Ziddi (1997 film)|Ziddi in the Tamil version replacing Sunny Deol with Vijayakanth thereby shooting the climax in 3 days. 

==Soundtrack==

{{Infobox album |  
| Name        = Dharma
| Type        = soundtrack
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1998
| Recorded    = 1998 Feature film soundtrack |
| Length      = 28:41
| Label       = 
| Producer    = Ilaiyaraaja
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1998, features 8 tracks with lyrics written by Pulamaipithan and Vasan. The soundtrack was well received.    

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Dharmangal || S. P. Balasubrahmanyam || 5:00
|- 2 || Iru Kanngal  (Happy)  || Ilaiyaraaja || 1:09
|- 3 || Iru Kanngal  (Sad)  || Ilaiyaraaja || 1:06
|- 4 || Iru Kanngal  (Happy)  || S. P. Balasubrahmanyam || 5:04
|- 5 || Iru Kanngal  (Sad)  || S. P. Balasubrahmanyam || 5:00
|- 6 || Manakkum || S. P. Balasubrahmanyam, Sujatha Mohan || 5:01
|- 7 || Sembaruthi || S. P. Balasubrahmanyam, Swarnalatha || 5:02
|- 8 || Thinam || S. P. Balasubrahmanyam || 1:19
|}

==References==

 

==External links==
* 
 

 
 
 
 
 
 