The Smile of the Lamb
 
{{Infobox film
| name           = The Smile of the Lamb
| image          = 
| image size     = 
| caption        = 
| director       = Shimon Dotan
| producer       = Jonathan Aroch
| writer         = Shimon Dotan David Grossman
| starring       = Rami Danon
| music          = 
| cinematography = Daniel Schneor
| editing        = Netaya Anbar
| distributor    = 
| released       = 1986
| runtime        = 95 minutes
| country        = Israel
| language       = Hebrew
| budget         = 
}}

The Smile of the Lamb ( , Transliteration|translit.&nbsp;Hiuch HaGdi) is a 1986 Israeli drama film directed by Shimon Dotan. It was entered into the 36th Berlin International Film Festival where Tuncel Kurtiz won the Silver Bear for Best Actor.   

==Cast==
* Rami Danon as Laniado
* Iris Hoffman as Shosh
* Makram Khoury as Katzman
* Tuncel Kurtiz as Hilmi
* Dan Muggia as Sheffer

==References==
 

==External links==
* 

 
 
 
 
 
 
 