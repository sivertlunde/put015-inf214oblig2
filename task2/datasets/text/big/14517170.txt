Spicy Love Soup
 
{{Infobox film
| name           = Spicy Love Soup
| image          = SpicyLoveSoup.jpg
| caption        =  Zhang Yang
| producer       = Zhang Peimin
| writer         = Zhang Yang Cai Shangjun Diao Yinan Liu Fendou
| starring       = 
| music          = Jia Minshu Zhang Jian
| editing        = Yang Hongyu
| distributor    = Xian Film Studio
| released       =  
| runtime        = 109 minutes
| language       = Mandarin
| budget         = US$200,000 
| film name = {{Film name| jianti         = 爱情麻辣烫
| fanti          = 愛情麻辣燙
| pinyin         = Aìqíng málà tāng}}
}} Chinese film Zhang Yang and written by Zhang, Liu Fendou, Cai Shangjun, and Diao Yinan based on a story by Zhang and Peter Loehr. Spicy Love Soup was produced by Loehrs Imar Film Company, Xian Film Studio, and Taiwanese financing. 
 Night Train) have since gone on to direct their own films, while Liu Fendou has expanded into both directing (Green Hat) and film production (Zhang Yibais Spring Subway). The cast is also notable for the debuts of two of Chinas most popular actresses, Gao Yuanyuan and Xu Jinglei.

The film is an anthology of sorts, and tells its story of love and life in modern Beijing through a series of five vignettes.

Spicy Love Soup was, in addition, the first film in mainland China to see a simultaneous release of a soundtrack, consisting largely of contemporary pop songs.   

== Cast ==

===First vignette===
*Wang Xuebing, a husband
*Liu Jie, his wife

===Second vignette===
*Tang Sifu, an elderly widow Li Mei, her daughter Liu Zhao, a suitor Li Tang, a suitor
*Wen Xingyu, a suitor

===Third vignette===
*Zhao Miao, a school boy,
*Gao Yuanyuan, his object of affection

===Fourth vignette=== Guo Tao, a husband
*Xu Fan, and his wife

===Fifth vignette===
*Sun Yisheng, a young boy
*Pu Cunxin, his father
*Lu Liping, his mother

===Sixth vignette===
*Shao Bing, a young man
*Xu Jinglei, a young woman
*Wakin Chau, a neighbor

== Reception == Variety cited the film as being a pioneering example of the "well directed" Chinese movie, that nevertheless doesnt fall into the camp of "artier" fare. 
The film was also well received in mainland China, where it had been released a year earlier, where it became one of the most successful independent films released domestically. 

== References ==
 

== External links ==
* 
* 
*  at the Chinese Movie Database

 

 
 
 
 
 
 
 
 
 
 