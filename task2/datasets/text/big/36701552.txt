Smashed (film)
{{Infobox film
| name           = Smashed
| image          = Smashed (film).jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = James Ponsoldt
| producer       = Jennifer Cochis Jonathan Schwartz Andrea Sperling Susan Burke
| starring       = {{Plain list | 
* Mary Elizabeth Winstead
* Aaron Paul
* Octavia Spencer
* Nick Offerman
* Megan Mullally
}}
| music          = Eric D. Johnson Andy Cabic
| cinematography = Tobias Datum
| editing        = Suzanne Spangler
| studio         = Super Crispy Entertainment
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 81 minutes  
| country        = United States
| language       = English
| budget         = $500,000 
| gross          = $499,725  
}}
Smashed is a 2012 American   on January 22, 2012 and won the U.S. Dramatic Special Jury Prize for Excellence in Independent Film Producing. " "  On February 5, the film was picked up by Sony Pictures Classics " "  and was released on October 12, 2012. " " 

==Plot==
Kate Hannah (Mary Elizabeth Winstead), an elementary school teacher, comes to work hungover and vomits in front of her class. Asked by a student if she is pregnant, she pretends she is, then continues the lie to the school principal Mrs. Barnes (Megan Mullally). Co-worker Dave (Nick Offerman) reveals that he knows she’s been drinking. Kate makes Dave swear not to tell anyone.
 crack and the two get high together. The next morning, Kate wakes alone on the street. She recovers her car and drives home, where Charlie acknowledges they both are alcoholics. The couple gets intoxicated and have sex before Charlie passes out. Kate heads out alone to buy wine but is turned down by the cashier. She urinates on the floor because the bathroom door is locked, then steals a bottle of wine.

Waking up, she realizes she has passed out again. At work, Dave, a recovering alcoholic, invites her to an Alcoholics Anonymous (AA) meeting. Kate befriends Jenny (Octavia Spencer), who has chosen a love of food and cooking over alcohol. Kate decides to stay sober and change her life. Dave drives Kate home, bluntly making an offensive comment which upsets her. Kate and Charlie visit her estranged alcoholic mom, Rochelle (Mary Kay Place). Kate mentions the AA meetings but Rochelle is skeptical; Kates father left them after getting sober and now lives in another state with his "shiny new family."

Kate is surprised the next day when she is thrown a baby shower by her colleagues. She reconciles with Dave, putting his comments behind them. At home, Kate is angry when it becomes clear Charlie told Owen and his friend about Kate smoking crack. That night, she rebuffs Charlies sexual advances. At school, Kate is questioned by a curious student as to why she’s not getting any fatter. Kate feigns a miscarriage and students accuse her of killing her baby, for which she reprimands them.

Kate tells Charlie she feels she must confess to Mrs. Barnes the truth about her faked pregnancy.  Charlie discourages her, saying shell lose her job.  They begin to fight over financial issues.  Kate lashes back that she would never depend on Charlie’s parents money and that she has struggled her whole life. Kate decides to tell Mrs. Barnes the truth and is fired. In a bar, she relapses. Kate is driven home by Jenny and Dave and starts an argument with Charlie.

Time passes. Kate speaks at an AA meeting, celebrating one year of sobriety. Charlie gets in trouble for riding a bicycle while drunk. Kate visits Charlie and they play croquet. Charlie asks if she would move back in with him if he begins going to meetings. Kate says he must get sober for himself, not for her. Charlie then asks Kate to play another round, to give him a chance to redeem himself. The film ends before Kate gives her answer.

==Cast==
* Mary Elizabeth Winstead as Kate Hannah
* Aaron Paul as Charlie Hannah
* Octavia Spencer as Jenny
* Nick Offerman as Dave Davies
* Megan Mullally as Principal Patricia Barnes
* Mary Kay Place as Rochelle
* Kyle Gallner as Owen Hannah
* Bree Turner as Freda
* Mackenzie Davis as Millie
* Richmond Arquette as Arlo
* Natalie Dreyfuss as Amber

==Production== Susan Burke. 

Shooting completed on October 22, and Smashed entered the 2012 Sundance Film Festival on November 30. 

==Reception==
===Box office=== Variety reported that Smashed was playing at the Deauville American Film Festival, which ran from August 31 - September 9.  Smashed was also screened at the 2012 Toronto International Film Festival. 
The film was picked up by Sony Pictures Classics on February 5, and released on October 12, 2012.  

Smashed opened in limited release in North America grossing $26,943 from 4 theaters averaging $6,736 per theater ranking number 53 at the box office. The films widest release was 50 theaters and it ended up earning $376,597 domestically and $123,128 internationally for a total of $499,725, off a $500,000 budget.  

===Critical response===
The film received generally positive reviews from critics. Rotten Tomatoes gave it a "Certified Fresh" score of 84% based on 102 reviews with an average score of 6.9 out of 10.  Metacritic gave the film a score of 71 out of 100, based on 32 reviews. 

Winstead received critical acclaim for her role. Chicago Sun-Times film critic Roger Ebert gave the film 3.5 stars out of four, saying that "This is a serious movie about drinking but not a depressing one." He praised Winsteads performance in the film, saying that "Mary Elizabeth Winstead is sort of wonderful in this movie, worn and warm. She doesnt play a victim. She has that courage an alcoholic requires to persist in the punishment of getting drunk day after day." {{cite web | author = Roger Ebert
| date = October 17, 2012 | title = Smashed | url = http://www.rogerebert.com/reviews/smashed-2012 }} 

==Soundtrack==
The soundtrack listing was released on Film Music Reporter on September 21, 2012. The soundtrack, released by Lakeshore Records, was released digitally on October 9, 2012.  The soundtrack features a mix of the films original score composed by Andy Cabic and Eric D. Johnson as well as music from other artists heard throughout various portions of the film.
  
# "Shower and a Beer" – Andy Cabic & Eric D. Johnson Richard and Linda Thompson
# "Wake Up and Run" – Andy Cabic & Eric D. Johnson
# "Someone2Dance" – Bart Davenport
# "Highland Park Bike Ride" – Andy Cabic & Eric D. Johnson
# "Por Ti" – Omero Guerrero
# "Dreams-Come-True-Girl" – Cass McCombs
# "Lake Arrowhead" – Andy Cabic & Eric D. Johnson
# "When the Shelter Came" – Dark Meat
# "Santa’s Village" – Andy Cabic & Eric D. Johnson
# "Planet of Women" – Sonny & The Sunsets
# "One More Game of Croquet" – Andy Cabic & Eric D. Johnson Smog

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 