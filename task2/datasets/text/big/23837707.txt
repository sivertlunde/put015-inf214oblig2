Treffen in Travers
 
{{Infobox film
| name           = Treffen in Travers
| image          = 
| caption        = 
| director       = Michael Gwisdek
| producer       =  Fritz Hofmann Thomas Knauf
| starring       = Hermann Beyer
| music          = 
| cinematography = Claus Neumann
| editing        = Evelyn Carow
| distributor    = 
| released       =  
| runtime        = 101 minutes
| country        = Germany
| language       = German
| budget         = 
}}

Treffen in Travers is a 1988 German drama film directed by Michael Gwisdek. It was screened in the Un Certain Regard section at the 1989 Cannes Film Festival.   

==Cast==
* Hermann Beyer - Georg Forster
* Corinna Harfouch - Therese Forster
* Uwe Kockisch - Ferdinand Huber
* Susanne Bormann - Röschen Forster
* Lucie Gebhardt - Klärchen Forster
* Astrid Krenz - Liese
* Peter Dommisch - Leonidas
* Heide Kipp - Marthe
* Wolf-Dietrich Köllner - Rougemont
* Andreas Schneider - Jean Claude
* Hark Bohm - Bürgermeister

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 