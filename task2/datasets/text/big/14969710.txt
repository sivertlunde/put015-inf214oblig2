The Spider's Stratagem
{{Infobox film
| name           = The Spiders Stratagem
| image          = 
| caption        = 
| director       = Bernardo Bertolucci
| producer       = Giovanni Bertolucci
| writer         = Bernardo Bertolucci  
| starring       = Giulio Brogi Alida Valli
| music          = Giuseppe Verdi from Aida and Rigoletto
| cinematography = Vittorio Storaro
| editing        = Roberto Perpignani	
| distributor    = 
| released       = (August 1970, Italy 5 January 1973 (United States|USA)
| runtime        = 100 minutes
| country        = Italy Italian
|}} political film directed by Bernardo Bertolucci.  

The screenplay was written by Bertolucci based on  "Theme of the Traitor and the Hero" story  written by Jorge Luis Borges.

==Plot==
Athos Magnani, a young researcher, returns to Tara, where his father was killed before his birth, at the request of his mistress, Draifa. The father, also named Athos Magnani (a wartime anti-fascist hero) and looking exactly like the son, was killed by a fascist in 1936—or so says Draifa, the town statue, and everyone in the city. As the son untangles the web of lies this story is constructed from, he finds himself ensnared in the same web.

==Cast==
*Giulio Brogi: Athos Magnani, father and son
*Alida Valli: Draifa
*Tino Scotti: Costa
*Pippo Campanini: Garibazzi
*Giuseppe Bertolucci: portatore di leone

==External links==
*  .

 

 
 
 
 
 
 
 
 
 
 
 

 