Le vieux fusil
 
{{Infobox film
| name           = Le vieux fusil
| image          = Le vieux fusil.jpg
| caption        = Theatrical release poster
| director       = Robert Enrico
| producer       =
| writer         = Robert Enrico Pascal Jardin Claude Veillot
| starring       = Philippe Noiret Romy Schneider Jean Bouise
| music          = François de Roubaix
| cinematography = Étienne Becker
| editing        = Ava Zora Eva Zora
| studio         = Les Productions Artistes Associés Mercure Productions TIT Filmproduktion GmbH
| distributor    = Mercure Productions
| released       =  
| runtime        = 103 minutes
| country        = France
| language       = French
| budget         =
}}
 Best Actor Best Music, Massacre of Oradour-sur-Glane in 1944.

==Plot==
In Montauban in 1944, during the German retreat from France, Julien Dandieu is an ageing, embittered surgeon in the local hospital. Frightened by the German army entering Montauban, Dandieu asks his friend Francois to drive his wife and his daughter to the remote village where he owns a chateau. One week later, Dandieu sets off to meet them for the weekend, but the Germans have now occupied the village. He finds that all the villagers have been herded into the church and shot. In the château, he finds his daughter shot and his wife immolated by a flame-thrower.

Dandieu decides to kill as many Germans as possible to avenge his family. He takes an old shotgun he used as a child while hunting with his father and starts to kill them one by one. They begin to think they are surrounded by many partisans and do not realise that he is, in fact, the only one, taking advantage of his knowledge of the secret passages within the chateau. He beats one of the SS men to death, shoots some of them and lets two of them drown in the well, where he closes the grid, preventing them from escaping.

With no more cartridges for the shotgun, he finds the flame-thrower which killed his beloved wife and uses it to kill the SS officer as he is about to commit suicide standing in front of the two-way mirror. The film ends with the liberation of the place by the French Resistance.

==Cast==
* Philippe Noiret as Julien Dandieu
* Romy Schneider as Clara Dandieu
* Jean Bouise as François Joachim Hansen as SS officer
* Robert Hoffmann as Lieutenant
* Karl Michael Vogler as Doctor Müller
* Madeleine Ozeray as Juliens Mother

==See also==
* Bruniquel, in the Tarn-et-Garonne department, the village where the film was shot.
* Château de Bruniquel, the landmark of the village, where most of the film was shot.

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 