Sunday Dinner for a Soldier
 
{{Infobox film
| name           = Sunday Dinner for a Soldier
| image          = SundayDinnerForASoldierPoster.JPG
| image size     =
| caption        =
| director       = Lloyd Bacon
| producer       = Walter Morosco
| writer         = Martha Cheavens (story) Melvin Levy Wanda Tuchock
| narrator       =
| starring       = Anne Baxter John Hodiak
| music          =
| cinematography = Joseph MacDonald
| editing        = J. Watson Webb Jr.
| distributor    = Twentieth Century Fox
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Sunday Dinner for a Soldier is a 1944 motion picture directed by Lloyd Bacon and
based on a novelette by Martha Cheavens.

==Primary cast==
*Anne Baxter - Tessa
*John Hodiak - Eric Moore
*Charles Winninger - Grandfather
*Anne Revere - Agatha
*Chill Wills - Mr. York Robert Bailey - Kenneth Normand
*Bobby Driscoll - Jeep
*Jane Darwell - Mrs. Dobson

==Plot==
A poor family in Florida saves all the money they can in order to plan a Sunday dinner for a soldier at a local Army airbase. They dont realize that their request to invite the soldier never got mailed. On the day of the scheduled dinner, another soldier is brought to their home and love soon blossoms between him (Hodiak) and Tessa (Baxter), the young woman who runs the home.

==Radio adaptation==
On February 19, 1945, Baxter, Hodiak, and Winninger appeared in a radio adaptation of the film on Lux Radio Theatre.

==References==
*"The Screen; Crisis and Romance", The New York Times (January 25, 1945)

==External links==
*  
* 

 

 
 
 
 
 
 
 
 
 


 