Aaina (1993 film)
 
{{Infobox film
| name = Aaina
| image =Aaina (movie poster).jpg
| image_size =
| caption =
| director = Deepak Sareen
| producer = Pamela Chopra
| writer = Honey Irani Shanker Iyer Dr. Rahi Masoom Reza
| narrator =
| starring = Juhi Chawla Jackie Shroff Amrita Singh Saeed Jaffrey Deepak Tijori
| music = Dilip Sen-Sameer Sen
| cinematography = Romesh Bhalla Nazir Khan
| editing = Keshav Naidu
| released = 31 May 1993
| runtime =
| country = India
| language = Hindi
| budget =
| preceded_by =
| followed_by =
| website =
}}

Aaina ( : آئینہ,   directed by Deepak Sareen and produced by Yash Chopra. The film starred Juhi Chawla, Jackie Shroff and Amrita Singh in lead roles. The film is about sibling rivalry. The film was a blockbuster hit in India, and solidified Juhi Chawlas career as a leading lady in the 1990s. The film was remade in Telugu as Aayanaki Iddaru, in Tamil as Kalyana Vaibhogam and in Kannada as Yare Nee Abhimani.

==Plot==
Roma (Amrita Singh) and Reema Mathur (Juhi Chawla) are the daughters of Mr. Mathur (Saeed Jaffery), a wealthy businessman. Roma, the older one, has always been spoiled, gets everything she desires, and is very competitive. Reema is quite reserved and usually lets her sister take the spotlight. Both grow up to be entirely different. The only similarity is that the sisters fall in love with the same man, Ravi Saxena (Jackie Shroff).

Always the center of attention, Roma catches his eye first. Reema is heartbroken but puts on a brave face. Ravi and Roma decide to get married. Unfortunately, Roma is ambitious to be a star in a film and gets an offer on the day of her wedding. She accepts and abandons Ravi minutes before their wedding. Ravi is furious and, in turn, marries Reema to save the honor of her family.

At first, Ravi and Reemas relationship is quite uncomfortable. But, as time passes, Ravi falls in love with Reema. Unfortunately, Roma comes back home in a rage and tells them that they will never be happy because she has been betrayed. Determined to get Ravi back Roma is willing to do anything including ruining her sisters life.

==Cast==
* Jackie Shroff ... Ravi Saxena
* Juhi Chawla ... Reema Mathur Saxena
* Amrita Singh ... Roma Mathur
* Saeed Jaffrey ... Mr. Mathur
* Dina Pathak ... Grandma
* Maya Alagh ... Mrs. Mathur
* Deepak Tijori ... Vinay Saxena

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Dil Ne Dil Se Kya Kahan"
| Nitin Mukesh, Lata Mangeshkar
|-
| 2
| "Meri Sanson Mein Tum"
| Kumar Sanu, Asha Bhosle
|-
| 3
| "Banno Ki Aayegi Baraat (Happy)"
| Pamela Chopra
|-
| 4
| "Banno Ki Aayegi Baraat (Sad)"
| Asha Bhosle, Suresh Wadkar, Pamela Chopra, Sameer
|-
| 5
| "Goriya Re Goriya"
| Jolly Mukherjee, Lata Mangeshkar
|-
| 6
| "Yeh Raat Khushnaseeb Hai"
| Lata Mangeshkar
|-
| 7
| "Aaina Hai Mera Chehra"
| Suresh Wadkar, Lata Mangeshkar, Asha Bhosle
|-
|}

==Awards==
* Amrita Singh won a Filmfare Best Supporting Actress Award for her performance.

==External links==
*  

 

 
 
 
 
 

 