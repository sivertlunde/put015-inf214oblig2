Vénus aveugle
{{Infobox film
| name           = Vénus aveugle
| image          = Venusaveugle.jpg
| image_size     = 
| caption        = Theatrical release poster
| writer         = Abel Gance Stève Passeur
| starring       = Viviane Romance Georges Flamant
| director       = Abel Gance
| producer       = France Nouvelle; Jean-Jacques Mecatti
| cinematography = Léonce-Henry Burel Henri Alekan
| music          = Raoul Moretti
| editing        = 
| distributor    = 
| released       = 14 September 1941
| runtime        = 140 minutes (première); 100 minutes in distribution
| language       = French
| country        = France
| budget         =
}}
 1941 cinema French film melodrama, directed by Abel Gance, and one of the first films to be undertaken in France during the German occupation.  (It is also sometimes cited as La Vénus aveugle.)

In the upheaval following the German invasion of France, in summer 1940 Abel Gance went to the Free Zone in the south and arranged a contract to make a film at the Victorine studios in Nice.  The original title was to be Messaline, drame des temps modernes ("Messalina, a drama of modern times"), but it was later changed to Vénus aveugle.  Although the film is not set in any specified period, Gance wanted it to be seen as relevant to the contemporary situation in France.  He wrote, "...La Vénus aveugle is at the crossroads of reality and legend...  The heroine ... gradually sinks deeper and deeper into despair. Only when she has reached the bottom of the abyss does she encounter the smile of Providence that life reserves for those who have faith in it, and she can then go serenely back up the slope towards happiness. If I have been able to show in this film that elevated feelings are the only force that can triumph over Fate, then my efforts will not have been in vain." 

==Plot==
The beautiful Clarisse learns that she is going blind, and in order to prevent her lover Madére, a boatman, from sacrificing himself for her, she decides to break with him, pretending she no longer loves him.  Madère angrily leaves, and Clarisse takes a job as a singer in a harbour bar to support herself and her crippled sister Mireille. When she discovers that she is pregnant, she wants to confess everything to Madère, but he has left on a year-long voyage with a new lover Giselle. Clarisse gives birth to a daughter Violette, and when Madère and Giselle return she learns that they are married and also have a baby daughter. Clarisses child dies, and she herself becomes completely blind; embittered against men, she withdraws into herself. Mireille tells the truth to Madère, who has separated from Giselle, and he undertakes an elaborate deception to take care of Clarisse, posing as the owner of a yacht on which he wants to take her on a cruise. The yacht is in fact the broken-down boat which used to be their shared home, but all their friends conspire to create the illusion that Clarisse is on a sea voyage.  Madère restores the boat in preparation for a real voyage, and just when it is ready, Clarisse tells him she has recognised him and the boat. She accepts his love for her, and simultaneously regains her sight.

==Cast==
* Viviane Romance, as Clarisse
* Georges Flamant, as Madère
* Mary-Lou (Sylvie Gance), as Mireille
* Lucienne Lemarchand, as Giselle

==Production==
Filming began on 11 November 1940 and continued through the winter into 1941. Gances assistant director was Edmond T. Gréville, and cinematography was by Léonce-Henry Burel with Henri Alekan as his assistant. In addition to the material difficulties that any film-making of that period underwent, the production was troubled by a disagreement between Viviane Romance and Sylvie Gance (the wife of Abel Gance), which became so severe that they refused to continue working together. As a result, many of Romances scenes were directed by Gréville, while Sylvie Gance was directed separately by her husband. Since the two actresses were playing sisters, the situation required extensive use of stand-ins filmed from behind. 

==Distribution and reception==
The original version of the completed film (shown at its première) ran for 2 hours 20 minutes. For distribution, it was shortened to 1 hour 40 minutes, resulting in problems of comprehensibility in a complicated plot. 

Although the censorship of the time prevented any explicit references to contemporary politics, Gance was anxious that the relevance of his vision of hope for a new France should be understood.  The film received a gala première in Vichy on 14 September 1941 in the presence of Philippe Pétain,  and it was preceded by a speech by Gance himself in which he paid tribute to the man whom he saw as the hope of Frances salvation: "For us French people, two great names rise up over our future: Joan of Arc and Philippe Pétain. Joan had saved France at Rheims, and it is from Vichy that our Marshal is saving France."   Some prints of the film were also prefaced by a handwritten dedication from Gance: "It is to the France of tomorrow that I wanted to dedicate this film, but since France is now personified in you, Marshal, allow me to dedicate it in all humility to you." 

The film seems to have found favour with the authorities at Vichy. On the other hand, even before its première the film became the object of an attack from the collaborationist and anti-Semitic newspaper Aujourdhui (French newspaper)|Aujourdhui, which insinuated that the freedoms of film-making in the unoccupied zone in the south of France were being exploited by Jews: the producer Jean-Jacques Mecatti, Viviane Romance and Gance himself were singled out for derisive references to their Jewish connections.   The film was first shown in Paris two years later in October 1943. 

Much subsequent criticism has concentrated on the absurdities of the films high-flown melodrama, but understanding has sometimes been hampered by the difficulty of seeing the film in its complete version. Some attention has been directed to the historical interpretation of its political allegory, and also to aspects of its depiction of women and of the power of collective action. Harsh critics of its preposterous melodrama have included Roger Régent, in Cinéma de France de "La Fille du puisatier" aux "Enfants du Paradis". (Paris: Bellehaye, 1948; re-issued Editions dAujourdhui, 1975); and Jacques Siclier, in La France de Pétain et son cinéma (Paris: Henri Veyrier, 1990.) pp.80-82. Gilbert Adair, writing in   reached a somewhat different conclusion: "because of the sheer force of Gances faith in his disreputable material, hokum not merely squared but cubed, and above all because one genuinely does find oneself moved, it is one of the mediums demented masterworks".

On its feminism and collectivism, see Norman King, Abel Gance: a politics of spectacle. (London: BFI, 1984.) p.174; and Sylvie Dallet,  , in 1895, no.31 (2000), Abel Gance, nouveaux regards", pp.53-79. 

==References==
 

==External links==
*  
*  :   .

 

 
 
 
 
 
 
 