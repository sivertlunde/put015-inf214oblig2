How to Play Baseball
{{Infobox film
| name           = How to Play Baseball
| image          = goofy_baseball.jpg
| caption        = Goofy at bat.
| director       = Jack Kinney
| distributor    = RKO Radio Pictures
| studio         = Walt Disney Animation Studios
| released       = 4 September 1942
| runtime        = 8 minutes
| country        = United States
| language       = English
}}

How to Play Baseball is a cartoon released by Walt Disney Animation Studios in September 1942, produced at the request of Samuel Goldwyn and first shown to accompany the 1942 feature film The Pride of the Yankees.     

==Synopsis==
 
Goofy takes the time to demonstrate Americas national pastime, then plays a game - one in which he plays all the bases. The short describes the basics of baseball in humorous terms; the equipment, uniforms, positions, and pitches, as well as the mannerisms of the players. It then switches to a game in progress, a deciding game in the World Series between the fictional Blue Sox and Gray Sox.  The Blue Sox are up three runs and working a no-hitter when the Grays rally in the bottom of the ninth.  In a series of events the Grays load the bases, leading to a base clearing hit.

The game is tied, but the play at the plate is too close to call for the umpire, and it then ends in an argument.  The narrator then concludes the short praising the values of what makes baseball Americas sport.

==Production== The Reluctant Dragon in 1941); How to Be a Detective (1952); and How to Sleep and How to Dance (both 1953).  

After Disney’s death, the studio produced How to Haunt a House (1999) and   and The Art of Self Defense in November and December 1941, respectively.

==Reception==
 
Bosley Crowther of The New York Times called it "deliciously confused ... goofy burlesque." 

==Releases==
*1942 &ndash; theatrical release
*1956 &ndash; Walt Disney anthology television series|Disneyland, episode #2.24: "The Goofy Sports Story" (TV)
*1972 &ndash; The Mouse Factory, episode #17: "Sports" (TV)
*1977 &ndash; The New Mickey Mouse Club, episode #1.20: "Showtime" (TV)
*1979 &ndash; Walt Disney anthology television series|Disneys Wonderful World, episode #26.4: "Baseball Fever" (TV)
*c. 1983 &ndash; Good Morning, Mickey!, episode #22 (TV)
*1983 &ndash; "Cartoon Classics: Sport Goofy" (VHS)
*c. 1992 &ndash; Mickeys Mouse Tracks, episode #55 (TV)
*c. 1992 &ndash; Donalds Quack Attack, episode #41 (TV)
*c. 1997 &ndash; The Ink and Paint Club, episode #3: "Sports Goofy" (TV)
*2002 &ndash; " " (DVD)
*2005 &ndash; " " (DVD)
*2010 &ndash; Have a Laugh!, episode #15 (TV)
*2011 &ndash; "Have a Laugh! Volume Three" (DVD)

==References==
 
 

==External links==
*  

 
 
 
 
 
 