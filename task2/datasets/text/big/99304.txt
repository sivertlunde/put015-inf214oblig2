Pearl Harbor (film)
 
{{Multiple issues|
 
 
}}

{{Infobox film
| name           = Pearl Harbor
| image          = pearl harbor movie poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Michael Bay
| producer       = Michael Bay Jerry Bruckheimer
| writer         = Randall Wallace
| starring       =  Ben Affleck Josh Hartnett Kate Beckinsale Cuba Gooding Jr. Tom Sizemore Jon Voight Colm Feore Alec Baldwin 
| music          = Hans Zimmer
| cinematography = John Schwartzman Roger Barton Chris Lebenzon Mark Goldblatt Steven Rosenblum Jerry Bruckheimer Films Buena Vista Pictures
| released       =  
| runtime        = 183 minutes    Box Office Mojo, 2009. Retrieved: March 25, 2009. 
| country        = United States
| language       = English Japanese
| budget         = $140 million  Cagle,Jess.   Time (magazine)|Time,  May 27, 2001. Retrieved: August 17, 2010. 
| gross          = $449.2 million 
}} romance and action elements Mako and Alec Baldwin.
 Japanese attack Technicolor dye Best Sound Worst Picture. This marked the first occurrence of a Worst-Picture-nominated film winning an Academy Award.
 

==Plot==
  Jesse James) trenches in France during World War I, and that he prays that no one will ever have to see the things he saw.
 RAF outfit for American pilots to fight during the Battle of Britain) which Rafe immediately accepts the position. Rafe lies to Danny, though, saying that Doolittle had assigned him.

While on a train ride to New York, a nurse named Evelyn (Kate Beckinsale) tells her fellow nurses Sandra (Jennifer Garner), Betty (Jaime King), Martha (Sara Rue) and Barbara (Catherine Kellner) the story of how she had met Rafe four weeks earlier after passing his medical exam, even though Rafe suffers from dyslexia. That night, Rafe and Evelyn enjoy an evening of dancing at a nightclub and later a spin in New York harbor in a borrowed police boat. Rafe shocks Evelyn by saying that he has joined the Eagle Squadron and is leaving the next day.  He asks her not to see him off, but when he leaves the following morning, he is pleased to see that she has come anyway.

In the meantime, Danny, Evelyn and the rest of their fellow pilots and nurses are transferred to Pearl Harbor, where there is little action going on; meanwhile, Rafe flies in numerous dogfights with the RAF against the Luftwaffe. During one battle, Rafe is shot down over the English Channel and presumed to be killed in action. Danny gives Evelyn the news and she is devastated. Danny learns from Evelyn that Rafe volunteered to go to England.

Three months later, Evelyn and Danny realize they are developing feelings for each other. Danny later takes Evelyn for a sunset flight over the harbor and begin a relationship of their own.

On the night of December 6, Evelyn is shocked to discover Rafe, alive and well, standing outside her door. He explains that he survived his aircraft crash and was rescued by a French fishing boat, and was stuck in occupied France ever since. Danny comes soon afterward holding a telegram from Western Union stating that Rafe is in fact alive. Rafe realizes that Danny and Evelyn are now together and leaves feeling betrayed. He goes to the Hula bar where he is welcomed back by his overjoyed fellow pilots. Danny finds Rafe in the bar with the intention of making things right but a drunken Rafe will have none of it and they get into a fight with each other. When the police arrive, the two drive away and, after talking, eventually fall asleep in their car.
 Day of Infamy Speech to the nation and asks the US Congress to declare a state of war with the Empire of Japan.

In the aftermath, the survivors attend a memorial service to honor the numerous dead, including Betty. Later, Danny and Rafe are assigned to travel stateside under Major Doolittle for a secret mission. Before they leave, Evelyn reveals to Rafe that she is pregnant with Dannys child and that she will remain with Danny but deep down she will always love Rafe just as much.
 bombing Tokyo and then landing in China. However, the Japanese discover them early, forcing the raiders to launch from a longer distance then planned. After a successful bombing against Tokyo, the raiders crash land on Japanese-occupied territory in China on a rice paddy. The Japanese Army have the members of Rafes plane pinned down, but Dannys plane flies over and shoots the Japanese patrol surrounding Rafe and his crew before crashing.

Rafe runs to Dannys side and attempts to pull a sharp piece of metal from Dannys neck, but they are once again attacked by Japanese patrols, and Danny gets shot in the crossfire. The other pilots Red (Ewen Bremner) and Gooz (Michael Shannon) kill off the remaining Japanese patrolmen. Holding a dying Danny in his arms, Rafe tells Danny that he cant die because he is going to be a father, to which Danny replies that Rafe will have to be the father and succumbs to his wounds. The remaining pilots are rescued by the Chinese. Upon his return home, a visibly pregnant Evelyn sees Rafe getting off the aircraft, carrying Dannys coffin.

Afterward, both Evelyn and Rafe are awarded medals. Rafe is awarded his medal by President Roosevelt, and he and Evelyn are discharged from the army. A few years later after the war ends, Rafe and Evelyn, now married, are visiting Dannys grave with their son, also named Danny. Rafe asks baby Danny if he would like to go flying, and the two fly off in the sunset in an old biplane.

==Cast==
 
* Ben Affleck as First Lieutenant (later Captain) Rafe McCawley
* Josh Hartnett as First Lieutenant (later Captain) Daniel "Danny" Walker
* Kate Beckinsale as Lieutenant Evelyn Johnson Dorie Miller
* Tom Sizemore as Earl President Franklin D. Roosevelt
* Colm Feore as Admiral Husband E. Kimmel Mako as Kaigun Taishō (Admiral) Isoroku Yamamoto Major (later Lieutenant Colonel) Jimmy Doolittle
 

===Supporting characters===
 
* William Lee Scott as First Lieutenant Billy Thompson
* Greg Zola as First Lieutenant Anthony Fusco
* Ewen Bremner as First Lieutenant Red Winkle
* Jaime King as Nurse Betty Bayer (credited as James King)
* Catherine Kellner as Nurse Barbara
* Jennifer Garner as Nurse Sandra
* Michael Shannon as First Lieutenant Gooz Wood Matt Davis as Joe Kaigun Chūsa (Commander) Minoru Genda
* Dan Aykroyd as Captain Thurman Scott Wilson General George Marshall
* Graham Beckel as Admiral Chester W. Nimitz Secretary of the Navy Frank Knox Frank J. Jack Fletcher Captain Mervyn S. Bennion William F. Bull Halsey Jr.
* Madison Mason as Admiral Raymond A. Spruance
* Sara Rue as Nurse Martha
* Reiley McClendon as Young Danny Jesse James as Young Rafe
* Kim Coates as Lieutenant Jack Richards Captain Marc Marc Andrew "Pete" Mitscher
* William Fichtner as Mr. Walker (Dannys father)
* Steve Rankin as Mr. McCawley (Rafes Father)
* Andrew Bryniarski as Joe the Boxer
* Leland Orser as Major Jackson
* Michael Milhoan as Army Commander
* Eric Christian Olsen as gunner to Captain McCawley David Kaufman as young nervous doctor
* Brandon Lozano as Baby Danny (Danny and Evelyns son)
 

==Production==
  Disney executives, since a great deal of the budget was to be expended on production aspects. Also controversial was the effort to change the films rating from R to PG-13. Bay wanted to graphically portray the horrors of war and was not interested in primarily marketing the final product to a teen and young adult audience. Budget fights continued throughout the planning of the film, with Bay "walking" on several occasions.
 Pearl Harbor Naval Base, the set at Rosarito Beach in the Mexican state of Baja California was utilized for scale model work. Formerly serving as the set for Titanic (1997 film)|Titanic (1997), Rosarito served as the ideal location to recreate the death throes of the battleships in the Pearl Harbor attack. A large-scale model of the bow section of the   mounted on a gimbal produced an authentic rolling and submerging of the doomed dreadnought. Production Engineer Nigel Phelps realized that the sequence of the ship, rolling out of the water and slapping down would involve one of the "biggest set elements" to be staged. Matched with computer generated imagery, the action had to reflect precision and accuracy throughout.  In addition, to emulate the ocean, a massive, stadium-like "bowl" was filled with water. The bowl was built in Honolulu, Hawaii and cost nearly $8 million. Today the bowl is used for training for scuba diving and deep water fishing tournaments.

The movie primarily utilized USS Lexington as both the USS Hornet and a Japanese carrier. All aircraft take-offs during the movie were filmed on board the Lexington. Other ships used in filler scenes included  , Heines, vienne.   Military.com. Retrieved: January 10, 2014.  and   during filming for the carrier sequences. Filming was also done on board the museum battleship   located near Houston, Texas.

The first trailer was released in 2000 and was shown alongside screenings of  .

==Reception==

===Box office===
Pearl Harbor grossed nearly $200 million at the domestic box office and $450 million worldwide. The film was ranked the sixth highest-earning picture of 2001 in film#Highest-grossing films|2001.  It is also the third highest-grossing romantic drama film of all time. 

===Critical reception===
Despite the box office success, the critical response to Pearl Harbor at the time of its release tended to be very negative, and the film earned only a 25% approval rating according to review aggregator website  ,   and behind Bad Boys II.  On Metacritic, the film has a score of 44 out of 100 based on 35 reviews, indicating "mixed or average reviews."  While it earned praise for its technical achievements, the screenplay and acting were popular targets for critics.

 , May 25, 2001. Retrieved: June 25, 2009. 
 , May 25, 2001. Retrieved: June 25, 2009.   , June 7, 2001. Retrieved: June 25, 2009. 

In his review for  , May 26, 2001. Retrieved: June 29, 2009.   , June 29, 2001. Retrieved: June 29, 2009.   . Besides, megahistory and personal history never integrate here". Schickel, Richard.   Time (magazine)|Time, May 25, 2001.  Retrieved: June 25, 2009. 

 , June 1, 2001. Retrieved: June 25, 2009. 

In his review for  , June 10, 2001. Retrieved: June 25, 2009. 

===Historical accuracy===
 
{{multiple image direction = horizontal width = 220 footer = image1 = A6M3 Zero N712Z.jpg alt1 = caption1 = A6M3 Zero Model 32 in green camouflage used in the film. image2 = Mitsubishi A6M2 Zero.jpg alt2 = caption2 = All Zeros involved in the attack on Pearl Harbor were light-colored (JN grey-green) early series A6M2 Model 21s.   warbirdsresourcegroup.org. Retrieved: November 20, 2010. 
}}
  were filmed on the  , a  . Although the early North American B-25 Mitchell|B-25B was used in the actual raid, late production models of the B-25J was used for the film. Furthermore, Kitty Hawk-class aircraft carriers were not used by the Navy until almost 20 years after the Doolittle Raid.]] historical dramas, Pearl Harbor provoked debate about the artistic license taken by its producers and director. National Geographic Channel produced a documentary called Beyond the Movie: Pearl Harbor  detailing some of the ways that "the films final cut didnt reflect all the attacks facts, or represent them all accurately." 
 Stearman biplane crop duster in 1923: the aircraft was not accurate for the period, the first commercial crop-dusting company did not begin operation until 1924,  and the U.S. Department of Agriculture did not purchase its first cotton-dusting aircraft until April 16, 1926. 
 

The inclusion of Afflecks character in the Eagle Squadron is another jarring aspect of the film, since active-duty U.S. airmen were prohibited from doing so, though some American civilians did join the RAF.  . Ben Afflecks Spitfire has insignia "RF" – this is an insignia of No. 303 Polish Fighter Squadron.  Countless other technical lapses rankled film critics, such as Bays decision to paint the Japanese Zero fighters green (Most of the aircraft in the attack being painted light gray/white), even though he knew that was historically inaccurate, because he liked the way the aircraft looked and because it would help audiences differentiate the "good guys from the bad guys". Cagle 2001, p. 51. 

For the aircraft take offs for both American and Japanese aircraft carriers, they shared the same design. The reason why is because those scenes were filmed on the Essex-class aircraft carrier|Essex-class carrier  , which is currently a museum ship in Corpus Christi, Texas. The aircraft on display were removed for filming and were replaced with film aircraft as well as WWII anti-aircraft turrets.

The harshest criticism was aimed at instances in the film where actual historical events were altered for dramatic purposes. For example, Admiral Kimmel was not on a golf course on the morning of the attack (he had been planning to meet General Short for a regular game, but cancelled when news of the attack came in), nor was he notified before the attack that the Japanese embassy staff was leaving Washington, D.C.. Also, Admiral Kimmel did not receive the report that an enemy midget submarine was being attacked until after the bombs began falling, and did not receive the first official notification of the attack until several hours after the attack ended. Sullivan 2001, p. 54.  
 George Welch and Kenneth M. Taylor, who took to the skies in P-40 aircraft during the Japanese attack and, together, claimed six Japanese aircraft and a few probables. Taylor, who died in November 2006, previously declared the film adaptation "a piece of trash... over-sensationalized and distorted."   Additionally, the combat scenes between the P-40s and the Zeros would not have been fought at wave-top height or with the aircraft darting around various obstacles as seen in the movie as such tactics would have been suicidal for both participants.

Attacks against Battleship Row and Pearl Harbor have been further dramatized. The movie depicts the four other battleships that survived the attack with severe damage,  ,  ,  , and   being sunk and rendered irreparable. These ships managed to escape further damage during the attack, although Tennessee herself was seen trapped in a listing manner during the attack and wedged against Ford Island by her sunken neighbor West Virginia, and Nevada being beached after the attack.   and other ships on Carrier Row (the ship-mooring row on the other side of Ford Island) were not depicted. During the attack scenes the positions of the ships under attack also change constantly in several continuity errors.

A sequence is included of Japanese aircraft targeting medical staff and the bases hospital. Although it was damaged in the attack, the Japanese did not deliberately target the U.S. naval hospital and only a single member of its medical staff was killed as he crossed the navy yard to report for duty. 
 Dorie Miller. In the film, Petty Officer Second Class Miller comforts Captain Mervyn S. Bennion who has been mortally wounded by a torpedo that strikes the  , and is with him when he dies. Miller is depicted as delivering the captains last orders to the ships executive officer, and then mans a twin .50 caliber Browning anti-aircraft machine gun. In actuality, Petty Officer Third Class Miller was first ordered to carry injured sailors to places of greater safety,   Naval History and Heritage Command. Retrieved: June 20, 2012.  and later ordered to go assist Captain Mervyn Bennion, mortally wounded by shrapnel from a bomb dropped on USS Tennessee.  The captain refused to leave his post on the bridge and continued to direct the battle until he died of his wounds just before the ship was abandoned. Ensign Victor Delano actually comforted the captain in his final moments. Miller was then ordered to help load a machine gun, but assumed control of the unmanned weapon instead. Delano showed Miller how to fire the weapon, saying later that Miller did not even "know how to shoot a gun." 

There are also some minor inaccuracies with the Doolittle Raid. Jimmy Doolittle and the rest of the Doolittle raiders had to launch from the USS Hornet 624 miles off the Japanese coast and after being spotted by a few Japanese patrol boats. In actuality, the Doolittle raiders had to launch 650 miles off the Japanese coast and after being spotted by only one Japanese patrol boat. In the film, all of the raiders are depicted as dropping their bombs on Tokyo. In actuality, the Doolittle raiders did bomb Tokyo, but also targeted three other industrial cities.  Furthermore, as fighter pilots, Rafe and Danny would have been ineligible to fly B-25 Mitchell bombers without extensive retraining.

A scene in New York involved the backdrop of the   in her commercial colors but by 1940, had actually been repainted grey, for refit completion to serve as a troopship already serving the Royal Navy, mainly in the Atlantic and Indian oceans. 
 Marlboro Light Willys M38 Jeep is seen; this vehicle was not introduced until 1950. The scene when Admiral Kimmel complains about transferring twelve destroyers to the Atlantic,  s are seen in the background, these vessels were not introduced until 1969, and are also seen in the attack scenes along with several modern landing ship, tank vessels. Other anachronistic ships also appear, such as 1970s Spruance/Kidd-class destroyers, notably when a row of them are bombed by the Japanese (another inaccuracy is present here; as the nests of destroyers moored in tight rows separate from land were not bombed in the attack). After the attack Danny and Rafe are seen boarding a C-47 transport which is to take them to their destination where they will train for the top secret mission. The C-47 used, clearly has a radar dome mounted in the nose; a variant not available in 1941. Doolittles trophies on a display case depicts a model of an F-86 Sabre which wasnt even on the drawing board.

Actor Kim Coates criticized the film  for choosing not to portray historically-accurate smoking habits and mens hairstyles.

Pearl Harbor was also criticized for the way it, "distinguished Americans from Japanese, including the wearing of black clothes, the lack of a social life, family or friends, and the devotion to warring, juxtaposing these with the portraits of Americans".  

===Honors and Awards===
 
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- Academy Award Academy Award Best Sound Editing George Watters II and Christopher Boyes
| 
|- Academy Award Best Sound Greg P. Kevin OConnell
| 
|- Academy Award Best Visual Effects Eric Brevig, John Frazier, Ben Snow, and Ed Hirsh
| 
|- Academy Award Best Original Song   oscars.org. Retrieved: November 20, 2011.  Diane Warren ("There Youll Be")
| 
|- Golden Globe Award Golden Globe Best Original Song Diane Warren ("There Youll Be")
| 
|- Golden Globe Best Original Score Hans Zimmer
| 
|- MTV Movie Award MTV Movie Best Action Sequence Attack on Pearl Harbor
| 
|- Golden Raspberry Award Golden Raspberry Worst Actor Ben Affleck
| 
|- Golden Raspberry Worst Screen Couple
| 
|- Josh Hartnett
| 
|- Kate Beckinsale
| 
|- Golden Raspberry Worst Screenplay Randall Wallace
| 
|- Golden Raspberry Worst Picture Jerry Bruckheimer
| 
|- Michael Bay
| 
|- Golden Raspberry Worst Director
| 
|- Golden Raspberry Worst Remake or Sequel
| 
|- World Stunt Taurus Award   imdb.com. Retrieved: November 28, 2010.  Best Aerial Work
| 
|-
|}

==Popular culture==
The soundtrack for the 2004 film   contains a song entitled "End of an Act" whose lyrics describe the emotion of longing for someone as well as panning the hapless Pearl Harbor. The songs chorus recounts, "Pearl Harbor sucked, and I miss you" equating the singers longing to how much "Michael Bay missed the mark when he made Pearl Harbor" which is "an awful lot, girl". The ballad contains other common criticisms of the film, concluding with the rhetorical question "Why does Michael Bay get to keep on making movies?" 

==Home media  ==
 
A Commemorative 60th Anniversary Edition was released on December 4, 2001. The feature was spread across two videotapes in letterbox format, and tape two also included Unsung Heroes of Pearl Harbor, a 50-minute documentary on little-known heroes of the attack, and a Faith Hill music video.

Around the same time a two-disc DVD of the Commemorative 60th Anniversary Edition was released. This release included the first two hours of the feature on disc one, and on disc two, the last hour of the feature, Journey to the Screen, a 47-minute documentary on the monumental production of the film, Unsung Heroes of Pearl Harbor, the Faith Hill music video and theatrical trailers.
 National Geographics "Beyond the Movie" feature and a dual-sided map was released concurrently on December 4, 2001.

A deluxe Vista Series edition of the film was released on July 2, 2002. It contained an R-rated directors cut of the film, with numerous commentaries from the cast and crew alongside a few "easter eggs". The directors cut of the film included the reinsertion of graphic carnage during the central attack (including shots of eviscerated bodies being torn apart by strafing, blood, flying limbs, etc.); small alterations and additions to existing scenes; Doolittle addressing the pilots before the raid; and the replacement of the campfire scene with a scene of Doolittle speaking personally to Rafe and Danny about the value of friendship. It runs at 184 minutes compared to the 183 minutes of the theatrical cut.

This elaborate package, which DVDtalk.com called "the most extensive set released   only one film" includes four discs of film and bonus features, a replication of Franklin D. Roosevelt|Roosevelts speech, collectible promotional postcards and a carrying case that resembles a historic photo album. The bonus features include all the features included in the commemorative edition, plus additional footage. There are three audio commentaries: 1) Director and film historian, 2) Cast and 3) Crew. Other features include: "The Surprise Attack", a multi-angle breakdown of the films most exciting sequence (30 minutes), which includes multiple video tracks (such as previsualization and final edit) and commentaries from veterans. Also included is the "Pearl Harbor Historic Timeline", a set-top interactive feature produced by documentarian Charles Kiselyak (68 minutes). The "Soldiers Boot Camp" follows the actors as they take preparation for their roles to an extreme (30 minutes)), "One Hour Over Tokyo" and "The Unsung Heroes of Pearl Harbor", two History Channel documentaries along with "Super-8 Montage", a collection of unseen Super-8 footage shot for potential use in the movie by Michael Bays assistant, Mark Palansky; "Deconstructing Destruction", an in-depth conversation with Michael Bay and Eric Brevig (of Industrial Light and Magic) about the special effects in the movie and "Nurse Ruth Erickson interview" complete the extra features component.

On December 19, 2006, a 65th Anniversary Commemorative Edition high-definition Blu-ray Disc was released.

==Soundtrack==
 
{{Infobox album |  
| Name        = Pearl Harbor: Music From The Motion Picture
| Type        = soundtrack
| Artist      = Hans Zimmer
| Cover       = Pearl Harbor OST Front Cover Amazon.jpg
| Released    = May 22, 2001
| Recorded    =
| Genre       = Film score
| Length      = 46:21 Hollywood
| Producer    = Bob Badami Trevor Horn & Byron Gallimore  ("There Youll Be") 
| Last album  =Riding in Cars with Boys (2001)
| This album  = Pearl Harbor (2001)
| Next album  =Hannibal (film)|Hannibal (2001)
}}
 Moulin Rouge!). score was Academy Award and Golden Globe Award for Best Original Song.

===Track listing===
# "There Youll Be" – song performed by Faith Hill
# Tennessee – 3:40
# Brothers – 4:04
# ...And Then I Kissed Him – 5:37
# I Will Come Back – 2:54
# Attack – 8:56
# December 7 – 5:08
# War – 5:15
# Heart of a Volunteer – 7:05
;Total Album Time: 46:21

==References==

Notes
 

Citations
 

Bibliography
 
* Arroyo, Ernest. Pearl Harbor. New York: MetroBooks, 2001. ISBN 1-58663-285-X.
* Barker, A.J. Pearl Harbor (Ballantines Illustrated History of World War II, Battle Book, No. 10). New York: Ballantine Books, 1969. No ISBN.
* Cohen, Stan. East Wind Rain: A Pictorial History of the Pearl Harbor Attack. Missoula, Montana: Pictorial Histories Publishing Company, 1981. ISBN 0-933126-15-8.
* Craig, John S.   New York: Algora Publishing, 2004. ISBN 978-0-87586-331-3.
* Golstein, Donald M., Katherine Dillon and J. Michael Wenger.  The Way it Was: Pearl Harbor (The Original Photographs). Dulles, Virginia: Brasseys Inc., 1995. ISBN 1-57488-359-3.
* Kimmel, Husband E. Kimmels Story. Washington, D.C.: Henry Regnery Co., 1955.
* Prange, Gordon W. At Dawn we Slept: The Untold Story of Pearl Harbor. Harmondsworth, Middlesex, UK: Penguin Books, 1981. ISBN 0-14-006455-9.
* Sheehan, Ed. Days of 41: Pearl Harbor Remembered. Honolulu: Kapa Associates, 1977. ISBN 0-915870-01-0.
* Sunshine, Linda and Antonia Felix, eds. Pearl Harbor: The Movie and the Moment. New York: Hyperion, 2001. ISBN 0-7868-6780-9.
* Sullivan, Robert. "What Really Happened." Time, June 4, 2001.
* Thorpe. Briagdier General Elliott R. East Wind Rain: The Intimate Account of an Intelligence Officer in the Pacific, 1939–49. Boston: Gambit Incorporated, 1969. No ISBN.
* Wilmott, H.P. with Tohmatsu Haruo and W. Spencer Johnson. Pearl Harbor. London: Cassell & Co., 2001. ISBN 978-0304358847.
* Winchester, Jim, ed. Aircraft of World War II (The Aviation Factfile). London: Grange Books, 2004. ISBN 1-84013-639-1.
* Wisiniewski, Richard A., ed. Pearl Harbor and the USS Arizona Memorial: A Pictorial History. Honolulu: Pacific Basin Enterprises, 1981, first edition 1977. No ISBN.
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 