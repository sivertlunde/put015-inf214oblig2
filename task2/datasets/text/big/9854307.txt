Song of the Flame
{{Infobox film name           = Song of the Flame image          = SongoftheFlame1922.jpg producer       =  director       = Alan Crosland  writer         = Gordon Rigby based on the 1925 operetta by Oscar Hammerstein II and Otto A. Harbach starring  Alexander Gray  Bernice Claire  Alice Gentle   Noah Beery music  David Mendoza Edward Ward cinematography = Lee Garmes  (Technicolor) editing        = Alexander Hall distributor    =   released       =   runtime        = 96 minutes language       = English country        = United States
|}}
 musical operetta Broadway musical Sound Recording George Groves).   

==Film==
 
Bernice Claire, known in the film as The Flame, is a young peasant girl who incites the people against the Czarist regime and the aristocracy through singing. Alexander Gray, who plays as a prince who is the leader of a group of Cossack troops, falls in love with Claire, even though she is part of a revolution that is dead set against his social class. Noah Beery is also part of the revolutionaries who are attempting to overthrow the Czarist regime. After meeting Claire, he also falls in love with her, much to the anger of his lover, who is played by Alice Gentle. The revolutionaries success in overthrowing the czarist regime leaving Gray and his aristocratic class in peril for their lives and fortunes. Beery becomes the new leader and his brutal treatment of the people make many regret having supported the revolution in the first place. After Beery attempts to seduce her, Claire flees to a village in her native Poland. Gray, fleeing from the new regime, happens to arrive at the village. When he meets Claire again he decides to stay. They put their political differences aside and become romantically involved. Hearing from his spies that the Prince is at a Polish village, Beery quickly goes there and arrests Gray and announces that he attends to execute him. Claire desperately attempts to free Gray by agreeing to have sex with Beery. Gray is released from prison through this ruse and when is it discovered that she had no intention of keeping her side of the bargain she is thrown into jail. Gray disguises himself and attempts to free Claire but he is discovered and imprisoned again. Before they can be executed, Alice Gentle, revealing the real reason behind Beerys execution order, tells the troops to release both Gray and Claire. Beery is arrested soon after as a traitor to the revolution by the troops and executed. Claire and Gray are left free to pursue their romance.

==Cast==
  Alexander Gray - Prince Volodya
* Bernice Claire - Aniuta, The Flame
* Noah Beery - Konstantin
* Alice Gentle - Natasha
* Bert Roach - Count Boris
* Inez Courtney - Grusha
* Shep Camp - Officer
* Ivan Linow - Konstanins Pal

==Music==
 
 Golden Dawn (1930). The public was so enthralled by his singing abilities that Brunswick Records hired Beery to record songs from both of these films which were issued in their popular series.

==Preservation== Russian folk folk tunes and one drawn from Tchaikovskys The Nutcracker.

==Stage musical== Russian Revolution in 1917 and for several years thereafter. 

==See also==
*List of lost films
*List of incomplete or partially lost films
*List of early color feature films

==References==
 

==External links==
* , David Coles, 2001
* 

 
 

 
 
 
 
 
 
 
 
 