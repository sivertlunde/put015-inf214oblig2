Mellisai
{{Infobox film
| name           = Mellisai
| image          = Mellisai.png
| alt            =  
| caption        = Poster
| director       = Ranjit Jeyakodi
| producer       = Deepan Boopathy Ratesh Velu
| financier      =  
| writer         = Ranjit Jeyakodi
| starring       = Vijay Sethupathi Gayathrie
| music          = Sam C. S.
| cinematography = Dinesh Krishnan
| editing        = Bavan Sreekumar
| studio         = Rebel Studio
| released       = Scheduled for  
| country        = India
| language       = Tamil
| budget         =  
| gross          =
}}
Mellisai is an upcoming Tamil film directed by Ranjit Jeyakodi and produced under Rebel Studio Productions. Vijay Sethupathi and Gayathrie feature in the leading roles.  The film features music by Sam C. S. and cinematography by Dinesh Krishnan. 

==Cast==
* Vijay Sethupathi
* Gayathrie
* Ramesh Thilak
* Sonia Deepti Arjunan

==Production==
Vijay Sethupathi announced that he signed the film in September 2013 in an interview to The Hindu, under the direction of first time film maker Ranjith.  Since Sethupathi had prior commitments, the film was supposed to take off only a year later. However, as a project of Sethupathi was getting delayed, the team chose to begin principal photography immediately, with the director telling that they had no time for pre-production but started filming since he had a cinematographer and a music director ready.  Gayathrie was signed on to play a violin teacher in the film, and to practice for her character she began to learn the instrument part-time.  In October 2014, Telugu actress Sonia Deepti stated that she had a short role in the film. 

The film was launched on 17 December 2013.  After the first schedule which lasted for 26 days, 50 percent of the film had reportedly been completed.  Filming was held at Fortis Healthcare, Vadapalani by mid-February.  In June, the director informed that "about 10 -12 days of shooting" was remaining. 

== References ==
 

==External links==
*  
*  

 
 
 
 
 


 
 