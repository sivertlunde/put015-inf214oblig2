Born Reckless (1958 film)
{{Infobox Film
| name           = Born Reckless
| image          = Bornreckless.jpg
| caption        =
| director       = Howard W. Koch
| producer       = Aubrey Schenck
| writer         = Richard H. Landau Aubrey Schenck Jeff Richards
| music          = Buddy Bregman
| cinematography = Joseph F. Biroc
| editing        = John F. Schreyer Warner Bros.
| released       = November, 1958
| runtime        = 80 minutes
| country        = United States English
}} western Drama drama film starring Mamie Van Doren and released through Warner Bros. studios.

==Plot==

Kelly Cobb travels and performs in various country rodeos in order to get enough money to buy a patch of land to call his own. One day he picks up Jackie Adams, a saloon singer and trick rider whom he saves from a clutching admirer. The two travel together and Jackie begins to fall in love with Kelly. Kelly however doesnt notice because of his drive to risk his life for the dream of the land he pursues. Jackie sums up that Kelly was just born reckless and she strives to change his free roaming lifestyle.

==Cast==

*Mamie Van Doren as Jackie Adams Jeff Richards as Kelly Cobb
*Arthur Hunnicutt as Cool Man
*Carol Ohmart as Liz
*Tom Duggan as Mark Wilson
*Nacho Galindo as Papa Gomez
*Allegra Varron as Mama Gomez
*Jim Canino as Jose
*Jeanne Carmen as Rodeo Girl

==External links==
*  

 
 
 
 
 
 
 


 