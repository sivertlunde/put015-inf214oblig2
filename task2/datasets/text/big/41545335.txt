Biyer Phul
{{Infobox film
| name           = Biyer Phul
| image          = Biyer Phul.jpeg
| image_size     = 200px
| caption        = VCD Cover
| director       = Matin Rahman
| producer       = Ananda Mela Cinema
| writer         = Joseph Satabdi
| starring       = Riaz Shakil Khan Shabnur Kabori Amol Bose Probir Mitra Ahmed Sharif Misha Swaodagor
| music          = Ahmed Imtiaz Bulbul
| cinematography = Mostafa Kamal
| editing        = Amjad Hossain
| distributor    = Ananda Mela Cinema
| released       = January 1999
| runtime        = 150 Minutes
| country        = Bangladesh Bengali
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
| imdb_id        = 1990180
}}

Biyer Phul ( ) is a Bangladeshi romantic movie. It was released 1999 and directed by Matin Rahman. The film stars Riaz, Shabnur, and Shakil Khan in lead roles along with Kabori, Amol Bosh, Probir Mitra, Ahmed Sharif, and Misha Swaodagor. 

== Cast ==
* Riaz – Sagor
* Shabnur – Nadi
* Shakil Khan – Akash
* Kabori – Dilruba
* Amol Bose – Kashem Mallik
* Probir Mitra – Ashrafi
* Ahmed Sharif – Raihan Chowdhury
* Misha Swaodagar – Didar 
* Bobby
* Lipi
* Beauty

== Music ==
Ahmed Imtiaz Bulbul was music director for the film.

=== Soundtrack ===
{| class="wikitable plainrowheaders sortable"
|-
! scope="col" | Track
! scope="col" | Song
! scope="col" | Singer
! scope="col" class="unsortable" | Notes
|-
! scope="row" | 1
|Tomay Dekhle Mone Hoy Andrew Kishore and Rumana Islam Kanak Chapa
|
|-
! scope="row" | 2
|Nesha Lagilo Re Baka Dunoyone Agun and Rumana Islam Kanak Chapa
|
|-
! scope="row" | 3
|Payel Amar Rumjhum Bajere Andrew Kishore and Rumana Islam Kanak Chapa
|
|-
! scope="row" | 4
|O Nodir Panire De Koiya De Alamgir Haque
|
|-
! scope="row" | 5
|Dil Dil Dil Tumi Amar Dil Andrew Kishore and Rumana Islam Kanak Chapa
|
|-
! scope="row" | 6
|Oi Chad Mukhe Jeno Lagena Groho Andrew Kishore 
|
|-
! scope="row" | 7
|Pother Majhe Khuje Pabi Apon Thikana James (Nagar James
|
|-
! scope="row" | 8
|Mon Na Dile Hoy Ki Prem Kumar Sanu and Alka Yagnik
|
|}

==See also==
 
* Soshur Bari Zindabad
* Rani Kuthir Baki Itihash

==References==
 

==External links==
*  

 
 
 
 
 
 


 
 