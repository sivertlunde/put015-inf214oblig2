The Fat Spy
{{Infobox film
| name           = The Fat Spy
| image          = Fatspyposter.jpg
| caption        = 1966 Theatrical Poster
| director       = Joseph Cates
| producer       = Everett Rosenthal Rick Pleven
| writer         = Matthew Andrews
| narrator       =
| starring       = Phyllis Diller Jack E. Leonard Brian Donlevy Jayne Mansfield
| music          = Joel Hirschhorn Al Kasha
| cinematography = Joseph C. Brun
| editing        = Barry Malkin
| distributor    = Troma Entertainment
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
}}
The Fat Spy is a 1966 Z movie that attempts to parody teenage beach party films. It was filmed at Cape Coral, Florida. It is featured in the 2004 documentary The 50 Worst Movies Ever Made. Briefly released to theaters in 1966, it was rarely seen until the 1990s, when it was released to the public domain. Since then it has been widely released on DVD and VHS in various editions sold mainly at dollar stores.

==Plot==
A mostly-deserted island, which is believed to be the home to the fountain of youth, is off the coast of Florida. The island gets some visitors in the form of a teenage boy band, "the Wild Ones", and their gang of swimsuit-clad young people, who head there in a crowded powerboat ostensibly for a scavenger hunt. However, they spend about half their screen time crooning to each other, or dancing on the beach.

The islands wealthy owner, Wellington (Brian Donlevy) recruits his blonde bombshell daughter, Junior (Jayne Mansfield), to remove the teenagers from the island. Junior is eager to see her love interest (and the islands only resident), rotund toupee-wearing botanist Irving (Jack E. Leonard). However, Irving is more interested in flowers and his bicycle than in the amorous Junior. Wellington asks Irving to spy on the teenagers, which he does by donning a sweatshirt that reads "Fink University", and "getting their trust" by joining them in dancing the Turtle. Meanwhile, Irvings twin brother Herman (also Jack E. Leonard, without a toupee), Wellingtons trusted employee, plots with his love interest, the scheming harridan Camille Salamander (Phyllis Diller) to find the fountain of youth first.

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 


 