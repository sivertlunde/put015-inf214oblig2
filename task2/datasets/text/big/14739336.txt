Don't Throw That Knife
{{Infobox Film |
  | name           = "Dont Throw That Knife"|
  | image          = DontThrowThatKnifeTITLE.jpg|
  | caption        = |
  | director       = Jules White Felix Adler|
  | starring       = Moe Howard Larry Fine Shemp Howard Dick Curtis Jean Willes|
  | cinematography = Fayte M. Browne| 
  | editing        = Edwin H. Bryant |
  | producer       = Jules White |
  | distributor    = Columbia Pictures |
  | released       = May 3, 1951 (United States|U.S.) |
  | runtime        = 15 50"
  | country        = United States
  | language       = English
}}
"Dont Throw That Knife" is the 131st short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges are census takers trolling through an apartment complex when they come upon a Mrs. Wyckoff (Jean Willes). The Stooges learn that Wyckoff is one-half of a husband-and-wife Magician Troupe who perform regularly. Unfortunately, Mr. Wyckoff (Dick Curtis) is also an evil man-hating jealous man and a precise knife thrower who kills any perverted men who talks to his wife. When Mr. Wyckoff comes home, the Stooges make a failed effort to take cover. After several frightening threats, the trio depart as fast as their feet will carry them.

==Production notes==
The film title is officially listed with quotation marks around it (as in "Dont Throw That Knife"). 
 director Jules producer Hugh McCollum. This left White as the sole director of the Stooges films from late 1952 to 1957 when trios contract with the studio expired. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 

 