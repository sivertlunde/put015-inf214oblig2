Me, Myself and I (film)
{{Infobox film
| name           = Me, Myself & I
| image          = MeMyselfandI1992.jpg
| image size     = 
| alt            = 
| caption        = VHS/DVD cover
| director       = Pablo Ferro
| producer       = 
| writer         = Julian Barry
| narrator       = 
| starring       = JoBeth Williams George Segal
| music          = Michael Elliott
| cinematography = Michael Sullivan
| editing        = Alan Ferro
| studio         = 
| distributor    = Sony Pictures
| released       =  
| runtime        = 97 minutes
| country        = United States
| city filmed    = Port Perry, Ontario
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

Me, Myself & I is a 1992 dark romantic comedy starring JoBeth Williams and George Segal.  The movie is the directorial debut of editor and producer Pablo Ferro.  Bill Macy, Shelley Hack and Ruth  Gilbert also appear in this independent film.  

==Cast==
*JoBeth Williams ...  Crazy Diane / Sane Diane 
*George Segal ...  Buddy Arnett 
*Don Calfa ...  Irving 
*Shelley Hack ...  Jennifer 
*Betsy Lynn George ...  Jailbait 
*Bill Macy ...  Sydney 
*Sharon McNight ...  Jailbaits Mom  Ruth Gilbert ...  Mrs. Landesman 
*Cheryl Paris ...  Aunt Felicia 
*Hartley Haverty ...  Kim Trombitas
*Nicholas Kadi ...  Saudi Prince 
*Jaid Barrymore ...  Lucy Lindell 
*Sheila Scott-Wilkenson ...  Katherine 
*Jennifer Ashley ...  T.V. Show Host 
*Paul Cavonis ...  Ronnie Pauson (Award Presenter)

==External links==
*  

==References==
 

 
 

 
 
 
 

 