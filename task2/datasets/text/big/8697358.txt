Crazy Stone (film)
{{Infobox film
| name = Crazy Stone
| image = Xin_CrazyStone_Poster.jpg
| caption = Crazy Stone film poster Zhang Cheng Yue Xiaojun
| director=Ning Hao
| producer=Bao Shihong
| starring = 
| cinematography=Du Jie
| editing = Du Yuan
| released = June 30, 2006
| runtime = 98 min.
| movie_language = Mandarin
| music = Funky Sueyoshi
| budget = $400,000
| worldgross = over $1,500,000
}} mainland Chinese HD cameras and produced as part of Andy Laus "FOCUS: First Cuts" series.

==Plot==

When a precious jade stone is discovered in an old outhouse, the owner of said outhouse and the surrounding buildings suddenly finds himself with the financial clout to withstand the buy-out pressure of an unethical developer who wishes to build a large building on his plot.  The owner, intending to display the stone to the public, puts his dedicated chief of security in charge of keeping it safe.  But with the stakes running high, this is easier said than done.  The developer hires a high-tech cat burglar from Hong Kong to steal the stone, the owners wayward son sees the jewel as the perfect symbol of wealth and hatches a plan to use it to increase his chances at getting laid, and a gang of three con-men who hear about the jewel see it as their ticket to the big time.  These three groups find themselves in direct competition and, finding their attempts foiled as often by the security guard as by each other, become more and more desperate as the film progresses.

==Style==
Crazy Stone is uncommon in that the entire movie is spoken in a mishmash of  , the angry owner shouts that his car is a "Bié Mō Wǒ" (别摸我, which  means "Dont touch me"), a deliberate play on the actual initialism.

The film also pays homage to or draws influence from a number of films, both foreign and domestic.  The scene in which a black suited thief tries to steal the coveted jade stone by descending from the ceiling attached to a rope is a reference to  ,  for example.  Similarly, one of the final scenes, which features a fight in an elevator, is a reference to the ending of the famous Hong Kong movie Infernal Affairs.

==Notes==
 

==External links==
* 
* 
*  article on Crazy Stone and Piracy

 

 
 
 
 
 
 
 
 
 