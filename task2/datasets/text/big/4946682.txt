Backdancers!
{{Infobox Film name           = Backdancers! image          = caption        = director       = Kozo Nagayama producer       = Shinya Kawai writer         = Kozo Nagayama Rin Eto starring       = Aya Hirayama Mako Ishino Takanori Jinnai Yoshino Kimura music          = Kozo Nagayama cinematography = Kazuhiko Ogura editing        = Ryuji Miyajima studio         = Studio Three distributor    = GAGA Corporation released  2006
|runtime        = 117 minutes country        = Japan language       = Japanese budget         = preceded_by    = followed_by    =
}} Japanese film released in Japan in September 2006.

The movie stars popular J-pop singer and dancer Hiroko Shimabukuro (hiro), of the group Speed (Japanese band)|Speed.

==Plot summary==
The lead singer of a famous pop group announces a shock walkout that leaves the four background dancers behind. The four set up their own group: "Backdancers!."

==Cast==
*Hiroko Shimabukuro (hiro) as Yoshika
*Aya Hirayama as Miu Sonim as Tomoe
*Saeko as Aiko
*Yu Hasebe as Juri Mai as Mayu
*Yoshino Kimura as Reiko Mihama
*Takanori Jinnai as George
*Mako Ishino as Naomi Saeki
*Haruna Ono as Mika

== External links ==
*  

 
 
 
 
 