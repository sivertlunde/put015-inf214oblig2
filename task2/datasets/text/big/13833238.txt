Spitfire 944
{{multiple issues|
 
 
}}

  documentary in Spitfire crash-landing for the first time, sixty-one years after the event.
__TOC__

== Behind the scenes == Spitfire Mk XI fighter plane at Mount Farm Airbase in Great Britain.  Being the flight surgeon at the base, Captain Savage was alerted to the impending accident and had the presence of mind to bring his movie camera to the landing strip.

Within 30 seconds of entering the Spitfires tail number into Google, the filmmaker was able to ascertain the date of the crash, the location of the crash and the name of the pilot: John S. Blyth.

The filmmaker sent a letter to the pilot requesting a general interview about World War II aviation and received a positive response.  He did not reveal the existence of the 16mm footage until the interview took place about two weeks later near Tacoma, WA.  At the end of a three-hour interview about the pilots World War II exploits, the filmmaker asked the pilot to review "about one minute" of footage.  John S. Blyth was quite surprised to suddenly be watching his death-defying landing of 61&nbsp;years earlier.

==Critical dimensions==
The filmmakers object was to capture what it looks like when a man is unexpectedly confronted with a motion picture of the most dangerous moment in his life, which took place during a major historical event, before the proliferation of consumer video cameras.  John S. Blyth and his family later commented that his "Spitfire Crash-Landing" story had been told so many times, some considered it a "tall tale."  No longer.

The film is complex in that it is a photographic record of a man reviewing a photographic record of his own photographic record-making process (a wartime photo-reconnaissance flight) across a timespan of 61 years.  The film includes several high-altitude World War II reconnaissance photos actually taken by the subject pilot.

== Historical context == Lockheed P-38 Supermarine Spitfire Mk XI, which was better suited to high-altitude, low-temperature flight conditions than the American planes.

== Articles in the media ==
* "Love for Spitfire Served Pilot Well," Tacoma News Tribune, 29 March 2007 by Soren Andersen
* "Scenes from Sundance,"  USA Today, 29 January 2007 by Anthony Breznican
* "Bay Area Films Keep It Real at Sundance Festival," Oakland Tribune, 16 January 2007 by Chris De Benedetti
* "Sundance Fest Unveils Shorts Program," Daily Variety, 6 December 2006 by Jeff Sneider

== Awards ==
* Spitfire 944 won an Honorable Mention for Short Filmmaking at the 2007 Sundance Film Festival.
* Spitfire 944 won the Best Documentary Award at the 2007  .
* Spitfire 944 won a Bronze Award for Short Documentary at the 2007 WorldFest-Houston International Film Festival.
* Spitfire 944 won the Best Documentary Short Award at the 2008 GI Film Festival.

== External links ==
* 
* 
* 
* 
 

 
 
 
 
 