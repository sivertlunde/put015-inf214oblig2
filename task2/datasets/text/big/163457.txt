The Last Detail
{{Infobox film
| name           = The Last Detail
| image          = Last_detail.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical poster
| director       = Hal Ashby 
| producer       = Gerald Ayres
| writer         = 
| screenplay     = Robert Towne 
| story          = 
| based on       =  
| starring       = Jack Nicholson Randy Quaid Otis Young Clifton James Carol Kane 
| music          = Johnny Mandel  Michael Chapman
| editing        = Robert C. Jones
| studio         = 
| distributor    = Columbia Pictures
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = $2.6 million
| gross          =$10,000,000 
}}

The Last Detail is a 1973 American comedy-drama film directed by Hal Ashby and starring Jack Nicholson, with a screenplay adapted by Robert Towne from a 1970 novel of the same name by Darryl Ponicsan.  The film became known for its frequent use of profanity. It was nominated for three Academy Awards.

==Plot== petty officers collection box of his C.O.s wifes favorite charity. Despite their initial resentment of the detail, the oddly likeable Meadows begins to grow on the two Navy "lifers" as they escort him on a train ride through the wintry north-eastern states; particularly as they know what the Marine guards are like at Portsmouth and the grim reality facing their young prisoner. As the pair begin to feel sorry for Meadows and the youthful experiences he will lose being incarcerated, they decide to show him a good time before delivering him to the authorities.

With several days to spare before they are due in Portsmouth, the trio stop off at the major cities along their route to provide bon-voyage adventures for Meadows. However, in Washington, their first endeavor ends in failure when they are denied drinks at a bar as Meadows is too young. Instead Buddusky gets a few six-packs allowing them all to get drunk in a hotel room. When Meadows passes out on the rooms only real bed, the other two let him stay there and take the uncomfortable roll-away beds for themselves. In Philadelphia they seek out Meadowss mother, only to find her away for the day and the house a pigsty, cluttered with empty whiskey bottles. They take him ice skating at Rockefeller Center in New York City. Buddusky tells Mulhall, "the kid is 18, he will be out of prison at 26," they take Meadows to a brothel in Boston, to lose his virginity. In between, they brawl with Marines in a public restroom, dine on "the worlds finest" Italian sausage sandwiches, chant with Nichiren Shōshū Buddhists, and open intimate windows for each other in swaying train coaches. Meadows pronounces his several days with Badass and Mule to be the best of his whole life.
 BRAVO YANKEE Annapolis ring), while executing the paperwork for the prisoner transfer, angrily berates Buddusky and Mulhall for beating Meadows (his facial wounds from Budduskys pistol-whipping being plainly visible), telling them that such conduct may be alright for the Navy but would not be tolerated by the Marines. The duty officer then asks if Meadows had tried to escape, to which they reply he had not to avoid getting Meadows into more trouble. The lieutenant finally notices that their orders were never officially signed by the master-at-arms in Norfolk, stating they had effectively "never left yet". At which point Mulhall and Buddusky ask to speak to the XO (Executive Officer) and the young marine officer relents.

With their duty completed, the pair stride away, angrily complaining about the duty officers incompetence – in the wake of his rebuke, hed momentarily forgotten to keep his copy of the paperwork – and hoping that their orders will come through when they get back to Norfolk.

==Cast==
* Jack Nicholson as Signalman 1st Class Billy L. "Badass" Buddusky
* Otis Young as Gunners Mate 1st Class Richard "Mule" Mulhall
* Randy Quaid as Seaman Laurence M. "Larry" Meadows
* Clifton James as Master-at-arms|M.A.A.
* Carol Kane as Young Whore
* Michael Moriarty as First Lieutenant Marine Duty Officer Nancy Allen as Nancy
* Gilda Radner as Nichiren Shoshu Member
* Jim Hohn as Nichiren Shoshu Member
* Luana Anders as Donna

==Production==
Producer Gerry Ayres had bought the rights to Darryl Ponicsans novel in 1969.  After returning from the set of Drive, He Said, Robert Towne began adapting the novel. Biskind 1998, p. 174.  The screenwriter tailored the script for close friends Jack Nicholson and Rupert Crosse.  In adapting the novel, Towne removed Budduskys "closet intellectualism and his beautiful wife". {{cite news
 | last = Berg
 | first = Charles Ramírez
 | coauthors =
 | title = Robert Towne
 | work = Film Reference
 | pages =
 | language =
 | publisher = 
 | date = 
 | url = http://www.filmreference.com/Writers-and-Production-Artists-Ta-Vi/Towne-Robert.html Bonnie & Clyde but had difficulty getting it made because of the studios concern about the bad language in Towness script.  Peter Guber recalls, "The first seven minutes, there were 342 fucks".  The head of Columbia asked Towne to reduce the number of curse words to which the writer responded, "This is the way people talk when theyre powerless to act; they bitch".  Towne refused to tone down the language and the project remained in limbo until Nicholson, by then a bankable star, got involved. 

Ayres sent the script to Robert Altman and then Hal Ashby. Ayres remembers, "I thought that this was a picture that required a skewed perspective, and thats what Hal had". Biskind 1998, p. 175.  Ashby was coming off the disappointing commercial and critical failure of Harold and Maude and was in pre-production on Three Cornered Circle at MGM when Jack Nicholson told him about The Last Detail, his upcoming film at Columbia. Dawson 2009, pp. 136–7.  The director had actually been sent the script in the fall of 1971 and the readers report called it "lengthy and unimaginative", but he now found it very appealing. Dawson 2009, p. 136.  He wanted to do it but it conflicted with his schedule for Three Cornered Circle. However Ashby pulled out of his deal with MGM and Nicholson suggested that they team up on Last Detail. Dawson 2009, p. 137.  Columbia did not like Ashby because he had a reputation of distrusting authority and made little effort to communicate with executives. The budget was low enough, at $2.6 million, for him to get approved.  

===Casting===
Nicholson was set to play Buddusky and so the casting of The Last Detail focused mainly on the roles of Mule and Meadows.  Bud Cort met with Ashby and begged to play Meadows but the director felt that he was not right for the role. Dawson 2009, p. 139.  Casting director Lynn Stalmaster gave Ashby a final selection of actors and the two that stood out were Randy Quaid and John Travolta. As originally written, the character of Meadows was a "helpless little guy", but Ashby wanted to cast Quaid, who was 64". {{cite news
 | last = Rabin
 | first = Nathan
 | coauthors =
 | title = Robert Towne
 | work = The A.V. Club
 | pages =
 | language =
 | publisher = 
 | date = March 14, 2006
 | url = http://www.avclub.com/content/node/46322
 | accessdate = 2007-12-03}}  He had offbeat and vulnerable qualities that Ashby wanted.  Towne remembers thinking, "Theres a real poignancy to this huge guys helplessness thats great. I thought it was a fantastic choice, and Id never thought of it."   Rupert Crosse was cast as Mule.

===Pre-production===
The project stalled for 18 months while Nicholson made The King of Marvin Gardens.  Guber told Ayres that he could get Burt Reynolds, Jim Brown, and David Cassidy and a new writer and he would approve production immediately. Ayres rejected this proposal and the studio agreed to wait because they were afraid that the producer would take the film to another studio.  Ashby and Ayres read navy publications and interviewed current and ex-servicemen who helped them correct minor errors in the script.  The director wanted to shoot on location at the naval base in Norfolk, Virginia and the brig at Portsmouth, New Hampshire but was unable to get permission from the United States Navy. However, the Canadian Navy was willing to cooperate and in mid-August 1972, Ashby and his casting director Stalmaster traveled to Toronto, Ontario to look at a naval base and meet with actors.  The base suited their needs and Ashby met Carol Kane whom he would cast in a small role. Dawson 2009, p. 138. 

Ashby was busted for possession of marijuana while scouting locations in Canada. This almost changed the studios mind about backing the project but the directors drug bust was not widely reported and Nicholson remained fiercely loyal to him, which was a deciding factor. Biskind 1998, p. 169.  Just as the film was about to go into production, Crosse was diagnosed with terminal cancer. Ashby postponed principal photography for a week to allow Crosse to deal with the news and decide if he still wanted to do the film. Biskind 1998, p. 178.  The actor decided not to do the film and Ashby and Stalmaster scrambled to find a replacement. They cast Otis Young. 

===Principal photography=== Michael Chapman, his camera operator on The Landlord,  to director of photography. They worked together to create a specific look for the film that involved using natural light to create a realistic, documentary-style.  Ashby let Nicholson look through the cameras viewfinder as a shot was being set up so he knew the parameters of a given scene and how much freedom he had within the frame. Dawson 2009, p. 142.  The actor said, "Hal is the first director to let me go, to let me find my own level". {{cite news
 | last = Starr
 | first = T
 | coauthors =
 | title = High on the Future
 | work = Ticketron Entertainment
 | pages = 9
 | language =
 | publisher = 
 | date = June–July 1973
 | url = 
 | accessdate = }} 

===Post-production===
The day after principal photography was completed, Ashby had his editor send what he had cut together so far. Dawson 2009, p. 144.  The director was shocked at the results and fired the editor. He was afraid that he would have to edit the film himself. Ayres recommended bringing in Robert C. Jones, one of the fastest editors in the business who had been nominated for an Academy Award for Guess Whos Coming to Dinner.  Jones put the film back into rushes and six weeks later had a first cut ready that ran four hours. Ashby was very impressed with his abilities and trusted him completely. Dawson 2009, p. 145.  Jones cut the film with Ashby at the filmmakers home and the process took an unusually long time as the director agonized over all the footage he had shot. Biskind 1998, p. 180.  Ashby would ignore phone calls from Columbia and eventually executives higher and higher up the corporate ladder tried to contact him.  Ashby was in London, England meeting with Peter Sellers about doing Being There when he received a phone call from Jones who told him that Columbia was fed up with the time it was taking for the film to be assembled. Dawson 2009, p. 147.  The head of the studios editing department called Jones to say that a representative was coming to take the film. Jones refused to give up the film and Ashby called the studio and managed to calm them down.  Towne occasionally visited Ashbys house to check in and did not like the pacing of the film. According to Towne, Ashby "left his dramatizing to the editing room, and the effect was a thinning out of the script".  During the editing process, Columbia hated the jump cuts Ashby employed. Biskind 1998, p. 183.  The studio was also concerned about the number of expletives. It needed a commercial hit as they were in major financial trouble.  By August 1973, the final cut of The Last Detail was completed and submitted to the MPAA which gave it an R rating. Columbia was still not happy with the film and asked for 26 lines with the word "fuck" in them to be cut. Dawson 2009, p. 148.  The theatrical release of The Last Detail was delayed for six months while Columbia fought over the profanity issue.  The film contained 65 uses of "fuck" overall and at the time of its release, broke the record for most uses in a motion picture. Ashby convinced Columbia to let him preview the film as it was to see how the public would react. It was shown in San Francisco and the screening was a huge success. Dawson 2009, p. 149. 

==Release==
Ayres persuaded Columbia to submit The Last Detail to the Cannes Film Festival. After Nicholson won Best Actor there, it shamed the studio into releasing the film.  The studio decided to give the film a limited release to qualify for Oscar consideration with a wide release planned for the spring of 1974.  By the time of its wide release, any pre-Oscar hype that was generated was now gone. Biskind 1998, p. 193. 

When the film was released for a week in Los Angeles, it received very positive reviews. In his review for The New York Times, Vincent Canby wrote, "Its by far the best thing hes ever done", referring to Nicholsons performance. {{cite news
 | last = Canby
 | first = Vincent
 | coauthors =
 | title = Last Detail a Comedy of Sailors on Shore
 | work = The New York Times
 | pages =
 | language =
 | publisher = 
 | date = February 11, 1974
 | url = http://movies.nytimes.com/movie/review?_r=2&res=9A0CE5DA133FEF34BC4952DFB466838F669EDE&oref=slogin&oref=login
 | accessdate = 2007-12-05}}  Variety (magazine)|Variety magazine also praised Nicholson, writing that he was "outstanding at the head of a superb cast". {{cite news
 | last = 
 | first = 
 | coauthors =
 | title = The Last Detail Variety
 | pages =
 | language =
 | publisher = 
 | date = January 1, 1973
 | url = http://www.variety.com/review/VE1117792458.html?categoryid=31&cs=1&p=0
 | accessdate = 2007-12-05}}  Andrew Sarris praised Ashbys "sensitive, precise direction". {{cite news
 | last = Sarris
 | first = Andrew
 | coauthors =
 | title = Salty Way to Naval Prison
 | work = Village Voice
 | pages =
 | language =
 | publisher = 
 | date = February 7, 1975
 | url = 
 | accessdate = }}  Time (magazine)|Time magazines Richard Schickel wrote, "there is an unpretentious realism in Townes script, and director Ashby handles his camera with a simplicity reminiscent of the way American directors treated lower-depths material in the 30s". {{cite news
 | last = Schickel
 | first = Richard
 | coauthors =
 | title = Not Fancy, Not Free Time
 | pages =
 | language =
 | publisher = 
 | date = February 18, 1974
 | url = http://www.time.com/time/magazine/article/0,9171,942781,00.html
 | accessdate = 2010-08-26}} 

It was shown as part of the Cannes Classics section of the 2013 Cannes Film Festival. 

===Awards and nominations=== Best Actor. Best Actor Best Actor Best Writing, Best Motion Best Supporting Actor – Motion Picture. Dawson 2009, p. 159.  Nicholson did win a BAFTA award for his role in the film.  Nicholson won the Best Actor awards from the National Society of Film Critics and the New York Film Critics Circle. However, he was disappointed that he failed to win an Oscar for his performance. "I like the idea of winning at Cannes with The Last Detail, but not getting our own Academy Award hurt real bad. I did it in that movie, that was my best role". Wiley 1996, p. 493. 

==Sequel==
In 2006, filmmaker Richard Linklater expressed an interest in adapting Last Flag Flying, a sequel to The Last Detail, into a film. {{cite news
 | last = Carroll
 | first = Larry
 | coauthors =
 | title = Movie File: Snoop Dogg, Oceans Thirteen, Jack Nicholson, Richard Linklater & More
 | work = MTV
 | pages =
 | language =
 | publisher = 
 | date = August 24, 2006
 | url = http://www.mtv.com/movies/news/articles/1539249/story.jhtml
 | accessdate = 2007-12-03 }}  He wrote a screenplay and sent a copy to Quaid but said that he would not do it unless Nicholson was involved.  In the novel, Buddusky runs a bar and is reunited with Larry Meadows after his son is killed in the Iraq War. It was rumored that Morgan Freeman was interested in taking over the role of Mule from Otis Young, who died in 2001. 

==Notes==
 

==References==
* Biskind, Peter (1998) Easy Riders, Raging Bulls. New York: Simon & Schuster.
* Dawson, Nick (2009) Being Hal Ashby. Lexington: University Press of Kentucky.
* Wiley, Mason and Damien Bona (1996) Inside Oscar. New York: Ballentine

==Further reading==
*   by Richard Armstrong, Senses of Cinema, April 2003.
*   by John and Judith Hess, Jump Cut, no. 2, 1974.

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 