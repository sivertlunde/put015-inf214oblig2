Traces of a Dragon
 
 
{{ infobox film
| name           = Traces of a Dragon
| image          = Traces_of_a_Dragon_poster.jpg
| caption        = 
| director       = Mabel Cheung
| producer       = Jackie Chan Willie Chan Solon So
| narrator       = Ti Lung
| starring       = Jackie Chan
| music          = Henry Lai
| editing        = Maurice Li
| distributor    = Fortissimo Films
| released       = 2003
| runtime        = 94 min Hong Kong Cantonese Standard Mandarin
| budget         = 
| gross          =  
}}

Traces of a Dragon ( ; aka Traces of a Dragon: Jackie Chan and his Lost Family) is a 2003 documentary film directed by Mabel Cheung.  The film explores the touching and history-filled background of Jackie Chan as we have never seen him before.

==Overview==
 
The world knows him as Jackie Chan. His Chinese fans know him by the stage name Sing Lung (成龍); which means "Becoming the Dragon"). The official record gives his birth name as Chan Kong Sang (陳港生). But two years ago Jackie Chan found out from his father that his ‘real’ name is Fong Si Lung/Fang Shilong (房仕龍). This revelation came with the uncovering of a previously hidden family history, a chronicle of lives scarred by war, poverty and separation. Chan Chi-Ping (陳志平) worked at the US Consulate. Mr Chan had met his wife-to-be Lily in China, years before, amid the turmoil of the Japanese invasion and the chaos of the civil war between Nationalists and Communists. They married in Hong Kong as newly arrived refugees from the Communist revolution, and Jackie was their only child. But both of them had been married before …
Chan Chi-Ping (at that time going by his real name Fang Daolong (房道龍) first became an economic migrant in the 1930s, moving from his native province Shandong to the more prosperous areas along the Yangtze River. Mr Fang met his first wife in Anhui and married her in Wuhu; they had two sons, Shide and Shishen. But his wife fell ill with cancer when their elder son was only seven. She died in 1947. Mr Fang had already worked in many jobs (apprentice draper, river trader, and strong-arm man for the Nationalists’ Intelligence Bureau); he made his way to Shanghai and headed the underworld ‘Shandong Gang’ until the city fell to the Communists in 1949.

Lily Chen was a native of Wuhu who married a local cobbler in the 1930s and had two daughters with him, Guilan and Yulan. The very first time that Japanese aircraft bombed the city, Lily’s husband died in an explosion at Wuhu Railway Station. Lily was 28 at the time. She refused to be ‘sold’ into a marriage arranged by her parents and stormed out of the family to find a way to support herself and her two young daughters. It was while she was trafficking opium that she first met Fang Daolong, then working as an inspector at the port; he took pity on her and let her go without punishment. She went on to become a ‘queen’ of the demi-monde in Shanghai, a legendary gambler in the city’s casinos.

Both Fang Daolong and Lily Chen had an urgent need to flee China after the Communist victory in October 1949. Both of them had underworld connections, which made them (in Communist eyes) "bad class elements"; Mr Fang also had the disadvantage of having served with the Nationalist army. He moved to Hong Kong at the first opportunity, and took the precaution of hiding his tracks by changing his name to "Chan Chi-Ping". Lily reached Hong Kong (via Macau) soon afterwards. Both of them were forced to abandon their children in China. They married soon after their reunion in Hong Kong, and Jackie was born (by Caesarian section) in 1954.

Jackie was an exuberant and well-liked boy who didn’t enjoy school but at the age of seven found his niche in a Peking Opera Academy run by Master Yu. His father pledged him to a ten-year apprenticeship with Master Yu, learning acrobatic stagecraft and other performing skills. His classmates included his lifetime friends and future collaborators Sammo Hung and Yuen Biao; all three of them were members of the academy’s performing troupe, the Seven Little Fortunes. Right after Jackie entered the academy his parents emigrated to Australia with their employer, US Consul Greene, who had been appointed US Ambassador in Canberra. For many years, Jackie’s only contact with his folks was through the taped letters he regularly received from them. But his father returned to Hong Kong for his graduation, and bought him an apartment in Sun Po Kung to set him up for a future in stage and film work.
 Yuen Woo-Ping, Snake in the Eagles Shadow and Drunken Master, both made in 1978.

Meanwhile his parents in Australia were taking advantage of China’s stabilising political situation to set about tracing the long-lost children they had been forced to leave behind when they fled to Hong Kong. Lily found her daughters with no great difficulty, and Guilan emigrated to Australia to look after her mother when she fell ill. Chan Chi-Long eventually traced his two sons with the help of the Chinese Ambassador to Australia (a fellow native of Shandong), and had a reunion with them in Guangzhou in 1985. Both had suffered victimisation in Mao’s Cultural Revolution of the mid-1960s, and both were having difficulty in adjusting to life in the era of Deng Xiaoping’s economic reforms. Eventually, in the late 1990s, Chan Chi-Long was able to convene a gathering in Anhui of his extended family. He revised and restored the Fang family register, proudly adding the name of his Hong Kong-born son Jackie: Fang Shilong.

Traces of the Dragon explores this emotionally charged history and shows Jackie Chan’s reaction to the discovery that he has two half-brothers he has never met. It’s the story of one Chinese family, but it mirrors the fates of countless other Chinese families in the 20th century. The film is dedicated to the memory of Jackie’s mother Lily Chan, who died in 2001.

==Production notes==
Director Mabel Cheung (Yuen-Ting Cheung) recalled in January 2003: One day, back in 1999, Jackie Chan heard from his father that he was at last ready to tell him some family secrets – to clarify the mysteries which had been preoccupying Jackie for some time. Jackie was well aware of all the rumours surrounding his origins and background: that he was not in fact the biological child of his parents, that he had elder siblings in China, even that his real surname was not ‘Chan’.

He asked Alex Law and me if we would be interested in filming his belated discovery of his real family background. We were of course intrigued, and agreed to fly to Australia with him to meet his father. I had never made a documentary before, and had no real idea of what to expect. We didn’t even have any clear idea of what we would do with the material we shot. Since Jackie’s mother Lily was in failing health, we decided that it would make (if nothing else) a nice souvenir for the Chan family!

After talking with his father over a few days, though, we found the story to be much more interesting than we’d expected it to be. We heard the story of one family and realised that it could stand for the stories of almost every Chinese family caught up in the turbulent history of China in the 20th century. We later flew to Anhui Province in China to interview other members of the family. We also interviewed some of the father’s friends and colleagues from the 1950s in Hong Kong. It was an enlightening process, almost like witnessing the history of modern China and colonial Hong Kong through the eyes of these people.
 The Soong Sisters, I watched many documentaries and a lot of archival footage on modern Chinese history. Many of the images were stunning, and they’d lodged in my mind. And so while we were editing the interviews for the documentary we began looking for appropriate historical footage to intercut with the story of Jackie Chan and his lost family.

Traces of the Dragon has ended up as a combination of the hard, brutal facts of war with intimate, touching glimpses of a family and its joys and sorrows. While we traced the story of Jackie the dragon, we also found ourselves tracing the story of modern China, the land of the dragon.. 

==External links==
*   
* 

 
 
 
 
 