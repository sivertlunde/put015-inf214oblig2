Pavee Lackeen
 
{{Infobox film
| name           = Pavee Lackeen: The Traveller Girl
| image          =
| director       = Perry Ogden
| producer       =
| writer         = Perry Ogden, Mark Venner Michael Collins
| original_music =
| distributor    = Pierre Grise Distribution
| released       =  
| runtime        =
| language       = English (subtitled due to accent)
| budget         =
| awards         = IFTA Awards 2005
}}

Pavee Lackeen: The Traveller Girl is a documentary-style film released in 2005. The film tells the story of an Irish Traveller girl (Winnie Maughan) and her family.  Most of the characters are played by the Maughan family themselves, led by youngest daughter Winnie.

Its director and co-writer, Perry Ogden, won an IFTA Award in the category of Breakthrough Talent in 2005. Winnie Maughn was nominated for Best Actress in a Feature Film in the same awards.

Filmed entirely on video, the film was described as startlingly real, showing the conditions in which the family—who are members of Irelands Travelling community— live.

== External links ==
*  
*  

 

 
 
 
 


 
 