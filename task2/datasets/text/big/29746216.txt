The Irishman (film)
 
{{Infobox film
| name           = The Irishman
| image          = 
| image size     =
| caption        = 
| director       = Donald Crombie
| producer       = Anthony Buckley
| writer         = Donald Crombie
| based on = 
| narrator       = Michael Craig Simon Burke Robyn Nevin
| music          = 
| cinematography = 
| editing        = 
| studio = Forest Home Films South Australian Film Corporation
| distributor    = 
| released       = 1978
| runtime        = 
| country        = Australia English
| budget         = AU $840,000 (est.) David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p147-149 
| gross = AU $622,000 
| preceded by    =
| followed by    =
}}

The Irishman is an Australian 1978 romantic drama film directed and written by Donald Crombie.

==Production==
In 1978, the book was adapted for the screen in a film of the same name.    The film version was written and directed by Donald Crombie for whom it was a labour of love.  

The movie was budgeted at $767,000 but went $800,000 over.  It was filmed in May through to July in 1977.

==Release== The Mango Tree, another coming of age period movie set in Queensland, when there was a backlash against period films. Peter Beilby & Scott Murray, "Donald Crombie", Cinema Papers, Oct-Nov 1978 p131-132 

==References==
 

==External links==
* 

 
 
 
 
 


 
 