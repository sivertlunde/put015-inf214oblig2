Rendezvous (1935 film)
{{Infobox film
| name           = Rendezvous
| image          = Rendezvous-1935.jpg
| image_size     = 225px
| caption        = theatrical release poster
| director       = William K. Howard Sam Wood (uncredited) 
| producer       = Lawrence Weingarten Herbert O. Yardley}}
| story          = 
| screenplay     = P. J. Wolfson George Oppenheimer Adaptation:   Howard Emmett Rogers
| starring       = William Powell Rosalind Russell Binnie Barnes
| music          = William Axt
| cinematography = William H. Daniels James Wong Howe (uncredited) 
| editing        = Hugh Wynn
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 94 minutes
| country        = United States English
| budget         = 
| gross          = 
}}
 cryptologist who tangles with German spies while falling in love.
 Bella and Samuel Spewack.

==Plot==
In 1917, former newspaperman William Gordon (William Powell) enlists in the United States Army|U.S. Army. The day before he is to leave Washington, D.C. for the fighting in Europe, he meets socialite Joel Carter (Rosalind Russell). The couple spend the day together. He tells her that, because he once wrote a book on cryptography under a pen name, the army is searching for him to put him to work behind a desk, but he is eager to fight the Germans.

The next day, just before he boards his train, Lieutenant Gordon is ordered to report to John Carter (Samuel S. Hinds), the Assistant Secretary of War. He reassigns the unhappy Gordon to Major William Brennan (Lionel Atwill) to spend the war decoding German transmissions. He learns that Joel had revealed his secret to her uncle, John Carter, in order to keep him around.

The U.S. is preparing to send its army across the Atlantic Ocean, but there are grave worries about the U-boat threat. Brennan comes up with the idea to have British escorts meet American transports before they enter the most dangerous zone. For maximum security, the rendezvous point is to be transmitted in code to the ships the day before. An ammunition ship is sent first to test the new system. Unbeknownst to the Americans, a German spy ring in the city has already cracked their code and deliberately allowed the ship through in order to lure more valuable troop transports into their trap. However, when Gordon manages to decipher one of their messages, he realizes the truth and alerts his superiors.

Brennan suspects his supposedly unbreakable code was stolen by his mistress, Olivia Karloff (Binnie Barnes), and sets a trap. He catches her red-handed, but foolishly he is alone, and she shoots him dead. To her dismay, she is ordered to sacrifice herself to divert suspicion from the spy ring. When she attends Brennans funeral, Gordon has her brought in for questioning. Carter sees her enter Gordons office and becomes very jealous. Gordon lets Karloff go free so that she might lead him to the rest of the spies.

The Germans decide to betray one of their own men, Captain Nicholas Nieterstein (Cesar Romero), in order to reassure the Americans and get them to resume operations. When he is arrested at the Russian Embassy, Nieterstein apologizes to Ambassador Gregory (Henry Stephenson) for giving his loyalty to his mothers country before flinging himself through a window to his death.

Afterward, Gordon escorts a shaken Karloff back to her hotel, where he discovers her guilt. However, since the hotel is staffed by German agents, he is the one who is captured. He refuses to divulge the new American code until they show him that Joel is also a prisoner; she had followed the pair to the hotel. He then gives in and provides the coordinates to the spies. When the Germans transmit the information, it is decoded by the Americans. The latitude and longitude that Gordon had given are actually the address of the hotel. Gordon and Joel are rescued.

The couple get married, and Gordon says his farewell to Joel as he begins to board the train, but he is again pulled off with orders to report to the War Department.

==Cast==
  
* William Powell as Bill Gordon
* Rosalind Russell as Joel Carter
* Binnie Barnes as Olivia
* Lionel Atwill as William Brennan
* Cesar Romero as Nieterstein
* Samuel S. Hinds as Carter
* Henry Stephenson as Ambassador
 
* Frank Reicher as Dr. Jackson
* Charley Grapewin as Martin
* Leonard Mudie as Roberts Howard Hickman as G-man
* Charles Trowbridge as Secretary of War
* Murray Kinnell as de Segroff
 

Cast notes:
*A 15-year-old   

==Production==
Rendezvous was originally intended as a vehicle for Powell and  , they were an unbeatable team, so my first day on Rendezvous, I tried to apologize. I know you dont want me, youd rather have Myrna. Powell denied it: I love Myrna, but I think this is good for you, and Im glad were doing it together. He was not only a dear, he was cool. If an actor thought he could get any place by having tantrums, watching Bill Powell would have altered his opinion. I remember a story conference during which he objected to a scene that he felt wasnt right for him. He was at once imperious and lucid. Its beyond my histrionic ability to do this, he said. I thought that was delicious.  

The picture had several working titles including Blonde Countess, White Bird, and Puzzle Man. It was shot in five weeks from June 24 to July 29, 1935, although production was suspended for a time due to an attack of appendicitis suffered by Binnie Barnes.   on TCM.com 

When it was reported in the press that the studio was going to scrap the picture and rewrite and reshoot it, director   and James Wong Howe, respectively, neither of whom received credit for their work.  The studio brought in Herman Mankiewicz and Howard Emmett Rogers to work on the script; it was also reported that George S. Kaufman was involved in script meetings as well. 

As a result of the rewrites, 17 new sets were built to shoot the new ending, which took place from September 6 to 26, with Wood directing and Howe behind the camera.   The film as released, at 94 minutes, was 12 minutes shorter than the preview version, which ran 106 minutes. 

==Critical response==
At the time it was released,   said: "The comedy is cleverly worked into the action and becomes a part of it instead of an interpolation, and herein lies its success. Interest is never diverted from the thread of the story. Powell is at ease as the nonchalant decoder who can face danger with a grin and teams perfectly with Russell. She has both looks and intelligence, playing the wilful girl with delightful spirit." 

==Adaptations==
In 1942, the story was updated to World War II and filmed as Pacific Rendezvous with Lee Bowman and Jean Rogers in the Powell and Russell roles.  Yardleys book was not credited as a source, nor were adapters Bella and Sam Spewack, but the two original screenwriters, P.J. Wolfson and George Oppenheimer, were given credit for the remakes screenplay. 

==References==
Notes
 

==External links==
*  
*  
*  
*  

 

 
 

 
 
 
 
 
 
 
 
 
 