127 Hours
 
{{Infobox film
| name           = 127 Hours
| image          = 127 Hours Poster.jpg
| caption        = North American release poster
| director       = Danny Boyle
| producer       = Danny Boyle Christian Colson John Smithson
| screenplay     = Danny Boyle Simon Beaufoy
| based on       =  
| starring       = James Franco Amber Tamblyn Kate Mara
| music          = A. R. Rahman
| cinematography = Anthony Dod Mantle Enrique Chediak Jon Harris
| studio         = Everest Entertainment Film4 Productions HandMade Films
| distributor    = Fox Searchlight Pictures Pathé|Pathé International Warner Bros. Pictures
| released       =   
| runtime        = 93 minutes
| country        = United Kingdom United States
| language       = English
| budget         = $18 million   
| gross          = $60.7 million 
}} biographical survival survival drama canyoneer Aron Ralston, who became trapped by a boulder in an isolated slot canyon in Blue John Canyon, southeastern Utah, in April 2003. The film was a co-production of the United Kingdom and the United States.
 Between a Best Picture Best Actor for Franco.

==Plot==
 
Engineer Aron Ralston (James Franco) drives to Utahs Canyonlands National Park for a day of Canyoning|canyoneering. On foot, he befriends hikers Kristi (Kate Mara) and Megan (Amber Tamblyn) and shows them an underground pool.

After parting ways with the hikers, Ralston enters Blue John Canyon through a slot canyon. He slips and falls; a boulder falls and traps his arm against the wall. Failing to move the boulder, he calls for help, but no one is around. He begins recording a video diary on his camera and using the larger blade on his pocket multi-tool to attempt to chip away at the boulder. He also rations his water and food.
 Kate Burton), and the hikers. He reflects that everything he has done has led him to this ordeal. After five days, Ralston sees a vision of a little boy.
 radius and the ulna bones, letting him amputate his arm. He severs his arm with the smaller, less dull knife on the multi-tool. He fashions a crude tourniquet out of the insulation for his CamelBak tube and uses a carabiner to tighten it, and cuts his arm off. He wraps the stump of his arm and takes a picture of the boulder that trapped him. He makes his way out of the canyon and rappels down a 65-foot rockface, drinks from a pool of rainwater, and meets a family on a day hike. Ralston is picked up by a Utah Highway Patrol helicopter. Ralston recovers, continues his climbing and mountaineering hobbies, and starts a family.

==Cast==
* James Franco as Aron Ralston
* Kate Mara as Kristi Moore
* Amber Tamblyn as Megan McBride
* Clémence Poésy as Rana, Aron Ralstons lover
* Lizzy Caplan as Sonja Ralston, Arons sister Kate Burton as Donna Ralston, Arons mother
* Treat Williams as Larry Ralston, Arons father

Aron Ralston himself and his wife and son make a cameo appearance at the end of the film.

==Authenticity==
The scenes early in the film of Ralstons encounter with the two hikers were altered to portray Ralston showing them a hidden pool, when in reality he just showed them some basic climbing moves. Despite these changes, with which he was initially uncomfortable, Ralston says the rest of the film is "so factually accurate it is as close to a documentary as you can get and still be a drama". 

Franco is never shown uttering even an "Ow"; Ralston wrote that this is accurate.  Other changes from the book include omissions of descriptions of Ralstons efforts after freeing himself: he had to decide where to seek the fastest medical attention; he took a photo of himself at the small brown pool from which he really did drink; had his first bowel movement of the week; abandoned a lot of the items which he had kept throughout his confinement; got lost in a side canyon; and met a family from the Netherlands (not an American family), Eric, Monique, and Andy Meijer, who already knew that he was probably lost in the area, thanks to the searches of his parents and the authorities. Ralston did send Monique and Andy to run ahead to get help. Ralston walked seven miles before the helicopter came.  This trek was indeed shown, however, in the alternate ending.

==Production== The Wrestler. So 127 Hours is my version of that." 

Boyle and Fox Searchlight announced plans to create 127 Hours in November 2009.  News of the World reported in November that Cillian Murphy was Boyles top choice to play Ralston.  In January 2010, James Franco was cast as Ralston.    Filming began in March 2010 in Utah.  Boyle intended to shoot the first part of the film with no dialogue.  By 17 June 2010, the film was in post-production. 

Boyle made the very unusual move of hiring two cinematographers to work first unit, Anthony Dod Mantle and Enrique Chediak, each shooting 50 percent of the film by trading off with each other. This allowed Boyle and Franco to work long days without wearing out the crew. 

Boyle enlisted Tony Gardner and his effects company Alterian, Inc. to re-create the characters self-amputation of his own arm. Boyle stressed that the realism of the arm as well as the process itself were key to the audience investing in the characters experience, and that the makeup effects success would impact the films success. The false arm rigs were created in layers, from fiberglass and steel bone, through silicone and fiberous muscle and tendon, to functional veins and arteries, and finally skinned with a translucent silicone layer of skin with a thin layer of subcutaneous silicone fat. Gardner states that the effects work was extremely stressful, as he wanted to do justice to the story, and he credits James Franco equally with the success of the effects work.

Franco admitted that shooting the film was physically hard on him: "There was a lot of physical pain, and Danny knew that it was going to cause a lot of pain. And I asked him after we did the movie, How did you know how far you could push it? ... I had plenty of scars... Not only am I feeling physical pain, but Im getting exhausted. It became less of a façade I put on and more of an experience that I went through." 

The film had two official  , which is designed to resemble the Rubin vase|vase-versus-faces optical illusion. On the poster, the viewer sees two inward-thrusting rocks or, more subtly, an hourglass. 

==Release==
127 Hours was screened at the Toronto International Film Festival on 12 September 2010, following its premiere at the 2010 Telluride Film Festival.  The film was selected to close the 2010 London Film Festival on 28 October 2010.  It was given a limited release in the United States on 5 November 2010.  It was released in the United Kingdom on 7 January 2011, and in India on 26 January 2011.  
 The Exorcist – and the movie has not even hit theaters yet."  
During the screenings at Telluride Film Festival, two people required medical attention. At the first screening, an audience member suffered from light-headedness and was taken out of the screening on a gurney. During a subsequent screening, another viewer suffered a panic attack.  Similar reactions were reported at the Toronto International Film Festival  and a special screening hosted by Pixar and Lee Unkrich, director of Toy Story 3 (2010).  The website Movieline published "Armed and Dangerous: A Comprehensive Timeline of Everyone Whos Fainted (Or Worse) at 127 Hours." 

===Reception===
127 Hours received universal acclaim from critics, with widespread praise directed towards Francos performance. Review aggregation website Rotten Tomatoes reports that 93% of 216 professional critics have given the film a positive review, with a rating average of 8.3 out of 10.    The sites consensus is that "As gut-wrenching as it is inspirational, 127 Hours unites one of Danny Boyles most beautifully exuberant directorial efforts with a terrific performance from James Franco." 
 Oscar nomination The Huffington Post, "Franco is mesmerizing as he steers his character from one who acts with reckless disregard to an introspective, remorseful soul, all the while maintaining his playful spark. To accomplish this range in a role that mostly consists of him speaking aloud to himself is incredible."  James Franco was awarded Best Actor by New York Film Critics Online.

===Accolades===
  Best Actor, Best Screenplay Best Original Score. 
 Outstanding British Best Direction, Best Actor Best Adapted Best Cinematography, Best Editing, Best Film Music. 
 Best Picture, Best Actor, Best Adapted Best Original Best Original Best Film Editing. 
 Best Film, Best Director, Best Actor, Best Adapted Best Cinematography, Best Editing, Best Song, Best Sound.  Its main theme song "If I Rise" won the Critics Choice award for Best Song. 

==Soundtrack==
 

==See also==
* Gerry (2002 film)|Gerry, a 2002 film directed by Gus Van Sant, inspired by the death of David Coughlin
* Survival skills

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 