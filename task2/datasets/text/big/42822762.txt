Geethanjali (2014 film)
{{Infobox film
| name           = Geethanjali

| image          = Geethanjali 2014 film poster.jpg
| alt            =  
| caption        = Movie poster
| director       = Raj Kiran
| producer       = M.V.V.Satyanarayana
| story          = Raj Kiran
| writer         = Kona Venkat Anjali Srinivasa Reddy
| music          = Praveen Lakkaraju
| cinematography = Sai Sriram
| editing        = Atkuri Teja
| studio         =
| distributor    = 
| runtime        =
| released       =  
| country        = India
| language       = Telugu
| budget         =₹ 4 crore 
| gross          =₹ 12 crore  (4 weeks collection) screenplay = Kona Venkat}} Telugu Horror horror comedy Anjali in the lead role.  The films soundtrack  and background score were composed by Praveen Lakkaraju. 

==Plot== Nandi award for a story he has. He faces many ridicules from different producers, including one (Raghu Babu) who tries to make him add dirty scenes into the film and another lady (Jhansi) who tells that everybody in her family and outside including cooks and cleaners have to like the story if she has to produce it.
He meets a businessman, Ramesh Rao (Rao Ramesh) with whom at first he has a fight for being late for an appointment, but eventually gets along with. He narrates to him the story. Rao also wants to do a film which will get a Nandi award as he wants to dedicate it to his father. In the opening scene in an apartment in Hyderabad, a girl commits suicide. The case appears to be serious and it is being
investigated by inspector Shravan (Shravan) who at first suspects the security, who was the first one to find about the suicide. The next day, the owner of the house (Ali (actor)|Ali) comes with a babaji who takes the spirit of the girl into a lemon but dies in a car accident on the way back. Then Sreenu tells Ramesh Rao about 3 months later. He also tells Ramesh Rao to imagine himself (Sreenu) in place of the hero. In a bus from Vijayawada to Hyderabad, The hero (Sreenu as imagined) meets a girl Anjali (Anjali) on the train. The both of them become friends during the journey but the next morning, Anjali leaves without Sreenu knowing. After getting down the bus, Sreenu goes to his friend Madhunandan (Madhunandan), and now it is revealed that he had actually come to Hyderabad to meet director Dil Raju and narrate to him a story he wrote. A house broker (Prudhviraj (Telugu actor)|Prudhviraj) gets them a house in the same flat where the suicide took place. After a lot of persuasion, Sreenu agrees to stay there. There he meets Athreya and Arudra (Satyam Rajesh and Shankar) who claim themselves to be assistants of Dil Raju although they are not and began to stay with Sreenu and Madhu. That night, the girl in the bus, Anjali comes there as she thought her friends who used to stay there were still there. Sreenu manages to persuade her to come in with a cup of coffee as coffee is her weakness. Soon she becomes a regular guest at the house. One day, two policemen come there and give Sreenu a bundle and tell him to put it in the prohibited room which belongs to the owners of the flat. When he keeps them inside, he sees a picture of Anjali in the room. Sreenu asks Venkatesh (Harshavardhan), the watchman about the photo and he tells Sreenu that it is a photo of the girl who committed suicide. Sreenu, Athreya and Arudra get frightened out of their wits when Venkatesh tells them that he hasnt seen any girl enter the apartment at night time since a week. That night, The doorbell rings again but Sreenu does not open the door. Then next morning, he packs everything that belonged to him and runs away from the apartment. It is now revealed that whatever Sreenu told Ramesh Rao till now is his real life story. Sreenu tells Rao that he just wanted to tell the story to somebody and he doesnt want to actually make it a movie. And saying so, he leaves from there. On the way back, Sreenu encounters a friend of Anjali whose photo he had seen in the room. When he enquires with the girl, he finds out that Anjali had a boyfriend, Madhunandan who was in jail as people believed him to be the reason Anjali committed suicide. At first, Sreenu thinks it is his friend, Madhu but later finds out it is not. Madhu takes Sreenu, Athreya and Arudra to his uncle Shaitan Raj (Brahmanandam), a psychiatrist. Raj helps the three of them and they find out it is just a man (Saptagiri (actor)|Saptagiri) who has been hiding there as he thought he will get a free house if they also run away. The 4 of them encourage the man to work for a living. That night, Anjali comes there to tell them the truth. She reveals to them that her name is Ushanjali and the one who died is her twin sister, Geethanjali. Geethanjali was in love with Madhu, but her boss, who is surprisingly Ramesh Rao, rapes her after she disagrees to marry him. He killed her and hanged her to the fan, and has Madhu blamed for the death. The gang decide to take revenge on Rao. Sreenu tells Rao that he went back to the flat and the girls spirit was coming that night to tell him who killed her. Ramesh Rao tells Sreenu he too will come along and hear the spirit. Sreenu and the gang send Ushanjali to Shaitan Raj to take training of the language and body language of devils so that she can act as Geethanjali. That night, at Sreenus house, Ramesh Rao reveals he knows everything and that the spirit who has come now is not Geethanjali but Ushanjali. In a fit of rage, Ushanjali kills Ramesh Rao by beating him to death and then goes away. Just after she leaves, She comes back in again and tells them sorry for being late and that she is ready. It is now revealed that Ushanjali got locked inside her house after her door got jammed and that the one who came was the real Geethanjali (who escaped from the lemon after it broke open). The ending scene shows all of them together celebrating Geethanjalis boyfriend, Madhunandans birthday and after that a song.

==Cast== Anjali as Geetanjali & Ushanjali (Dual role)
* Srinivasa Reddy as Shrinivas (Sreenu)
* Madhunandan as Madhunandan,Shrinivas friend
* Harshvardhan Rane as Madhunandan,Anjalis Lover
* Brahmanandam as Shaitan Raj Ali as Haunted House Owner
* Rao Ramesh as Ramesh Rao
* Satyam Rajesh as Rajesh/Athreya Shankar as Shankar/Arudra
* C.V.L. Narasimha Rao as Madhunandans father
* Raghu Babu as film producer
* Jhansi as film producer
* Rajeev Kanakala special appearance throughout the song before climax Prudhviraj as house broker
* Vennela Kishore as a patient of Shaitan Raj
* Shravan as inspector Shravan (special appearance)
* Saptagiri as a ghost in the house
* Dil Raju as himself
* Harshavardhan as Venkatesh,Watchman

==Production==
 
The first look launch event of the movie was held in Hyderabad and V. V. Vinayak, who has attended as the chief guest, launched the first look.  The movie is running successfully in theatres. 

==References==
 

==External links==
*  

__FORCETOC__

 
 
 
 
 
 