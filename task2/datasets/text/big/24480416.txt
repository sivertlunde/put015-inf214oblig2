Men for Sale
 
{{Infobox film
| name           = Men for Sale
| film name      =  
| image          = Hommes-a-louer.jpg
| caption        = Theatrical release poster
| director       = Rodrigue Jean
| writer         = Rodrigue Jean
| starring       =
| producer       = InformAction Co-producers:  National Film Board of Canada (NFB)
| music          = Tim Hecker
| cinematography = Mathieu Laverdière
| editing        = Mathieu Bouchard-Malo
| distributor    =
| released       =  
| runtime        = 140 minutes
| country        = Canada
| language       = French
| awards         =
| gross          =
}} Canadian director male prostitutes working in Montreal, Quebec, Canada. The film was shot over a one-year period in Montreals Gay Village.   

The interviews for Hommes à louer are in French language. The English version Men for Sale has subtitles in English language|English.

Canadian National Film Board calls the documentary "an unflinching portrait with neither voyeurism nor false sympathy acknowledging those society prefers to ignore". 

==Synopsis==
The documentary follows the life of 11 male prostitutes over the course of a year, recounting their struggles to survive alcohol and drug-related addictions, abuse and stigmatization – and, their troubled pasts. 

Trapped in a vicious circle of prostitution and drugs, they pursue their lives, realizing their prospects for the future are dim.

==Festivals==
In 2009, the documentary was an official selection for:
*Festival du Nouveau Cinéma
*Festival dAvignon (63rd season)
*Festival international du cinéma francophone en Acadie (FICFA) (23rd season)
*Atlantic Film Festival

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 