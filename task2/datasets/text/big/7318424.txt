Where's Jack?
 
 
{{Infobox Film
| name           = Wheres Jack?
| image_size     = 
| image	=	Wheres Jack? FilmPoster.jpeg
| caption        = 
| director       = James Clavell
| producer       = Stanley Baker James Clavell 
| writer         = Rafe Newhouse David Newhouse
| narrator       = 
| starring       = Stanley Baker  Tommy Steele
| music          = Elmer Bernstein
| cinematography = John Wilcox
| editing        = Peter Thonrton
| studio         = Oakhurst Productions
| distributor    = Paramount Pictures
| released       = 1969 
| runtime        = 120 m
| country        =  
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1969 film recounting the exploits of notorious 18th century criminal Jack Sheppard and London "thieftaker" Jonathan Wild.

The film was produced by Stanley Baker through his company Oakhurst Productions, and starred Baker himself as Jonathan Wild. Tommy Steele played Jack Sheppard. 

The film was directed by novelist James Clavell.  Mary Hopkin sings the title song.

The ending of the film is ambiguous. It suggests the possibility that Sheppard may have survived his execution and escaped to the Americas.

==Cast==
*Tommy Steele ....  Jack Sheppard 
*Stanley Baker ....  Jonathan Wild 
*Alan Badel ....  The Lord Chancellor 
*Dudley Foster ....  Blueskin 
*Fiona Lewis ....  Edgworth Bess Lyon 
*Sue Lloyd ....  Lady Darlington  Noel Purcell ....  Leatherchest  
*Eddie Byrne ....  Rev. Wagstaff   
*Michael Elphick ....  Hogarth 
*Howard Goorney  ....  Surgeon  
*John Hallam ....  The Captain   
*Harold Kasket ....  The King   
*Caroline Munro ....  Madame Vendonne 
*Cardew Robinson ....  Lord Mayor   George Woodbridge ....  Hangman

==Production== Point Blank (1967), produced by the maker of Zulu (1964 film)|Zulu (1963) and directed by the man who made To Sir, with Love (1967) which made a combined profit of $45 million. Divided by four that meant a profit of over $10 million after $3 million cost was deducted. The pitch was successful and Deeley says it remains one of his happiest memories in getting a film funded. Ironically, Wheres Jack? turned out to be a box office flop. Michael Deeley, Blade Runners, Deer Hunters and Blowing the Bloody Doors Off: My Life in Cult Movies, Pegasus Books, 2009 p 43-44 

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 