See Ya Later Gladiator
{{multiple issues|
 
 
}}

{{Infobox Hollywood cartoon
| series = Looney Tunes (Daffy Duck/Speedy Gonzales)
| image=
| caption =
| director = Alex Lovy
| story_artist = Cal Howard Ed Solomon
| musician = William Lava Jaime Diaz
| background_artist = Bob Abrams
| voice_actor = Mel Blanc
| producer = William L. Hendricks Warner Bros. Pictures
| release_date = June 29, 1968 (USA)
| color_process = Technicolor
| runtime = 7 minutes
| movie_language = English
}}
 Cool Cat, Bunny and Claude, Merlin the Magic Mouse, and a few one-shot cartoons made up all of WBs output.

==Plot==
 
The plot concerns Daffy and Speedy accidentally being sent back in time via a time machine to Rome, 65 A.D., where Emperor Nero plans to feed them to the lions as entertainment in a gladiator arena. Daffy and Speedy work together to thwart the lions and manage to make it back to the present. However, Nero has accidentally returned with them, and joins Speedys band by playing his fiddle.

==Music==
This cartoon features a slightly different arrangement of The Merry-Go-Round Broke Down by Bill Lava (The second being 3 Ring Wing Ding.)

==Reception==
See Ya Later, Gladiator is widely considered by Looney Tunes fans to be the worst cartoon Warner Brothers ever made (although some consider other post-1967 shorts to be worse).    Criticisms include the cartoons limited animation and uncharacteristic plot.

==References==
 

==External links==
*  
*  

 
 
 
 
 


 