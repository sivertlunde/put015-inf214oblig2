Vampires in Havana
 
{{Infobox Film
| name           = ¡Vampiros en La Habana!
| image          = Vampiros en La Habana.png
| caption        = DVD cover
| director       = Juan Padrón
| producer       = 
| writer         = Ernesto Padrón Juan Padrón
| narrator       = 
| starring       = 
| music          = Rembert Egues
| cinematography = Julio Simoneau
| editing        = Rembert Egues Rosa Maria Carreras
| distributor    = 
| released       =  
| runtime        = 69 min. / 80 min. Cuba
| Spanish
| budget         = 
}}
¡Vampiros en La Habana! (English title: Vampires in Havana) is a Cuban animated film directed by Juan Padrón. Released in 1985, the film features trumpet performances by Arturo Sandoval. A sequel to the film, called Más vampiros en La Habana, was released in 2003.

==Plot==
Joseph Amadeus von Dracula, known as Pepito to his friends, is a trumpet player in 1930s Havana who spends his time away from the bandstand dabbling in quasi-terrorist plots to overthrow the Cuban government of dictator, Gerardo Machado. He is unaware that he is really a vampire, and that his uncle, Werner Amadeus von Dracula, the son of Count Dracula, has been using him as a test subject for a formula that negates the usually fatal effects of sunlight.

A Chicago-based crime syndicate and a group of vampires with members from several countries in Europe have both learned of the formula and wish to possess it for different reasons—the Chicago group to suppress it and thus maintain their monopoly on indoor, artificial beach resorts, and the Europeans to market it as "Vampisol."  When Pepito learns of his true heritage (and his uncles wish to give the formula away to vampires everywhere) he becomes the target of a multi-pronged manhunt, leading all parties involved on a wild chase through some of the seediest neighborhoods of Havana.
 O positive blood as his last drink, the blood type which vampires consider to be the most delicious. However, when he spits it out in disgust, Terrori realizes that Pepitos dislike of drinking blood, the fact that he was harmed by a lead bullet earlier (vampires can only be harmed by silver bullets) and that he is completely impervious to sunlight (it instantly kills vampires) means that Pepito has stopped being a vampire. Terrori loses interest in the Vampisol formula, realizing that its effect is to turn vampires into humans. However, the leader of the European vampires suggests a deal with his counterparts from Chicago, whereby they can encourage vampires to take small amounts of Vampisol in the summer and visit the mobsters artificial beaches in the winter. Both groups believe that they are going to make a fortune from Vampisol but, as a final resolution, Pepito sings instructions on how to prepare the formula over the radio to vampires worldwide, instructing them to use it sparingly to avoid becoming human. The Vampisol formula becomes financially worthless and both vampire cartels find themselves defeated.

At the very end of the film a vampire addresses the audience and says, "Be careful, because that guy next to you on the beach... might just be a vampire!"

==External links==
* 
*  

 
 
 
 
 
 
 
 
 