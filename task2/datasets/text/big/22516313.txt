The Aryan Couple
{{Infobox Film
| name           = The Aryan Couple
| image          = 2009 Region 2 DVD cover for The Aryan Couple.jpg
| image_size     = 180px
| caption        = Region 2 DVD cover John Daly
| producers      = John Daly, Peter BealeArcadiy Golubovich, Ilya Golubovich, Irina Szufarska
| writers        = John Daly, Kendrew Lascelles, Lance Miccio Caroline Carver Kenny Doughty  Judy Parfitt  
| music          = Igor Khoroshev
| cinematography = Sergey Kozlov
| distributor    = Atlantic Film Productions
| released       = December 10, 2004
| runtime        = 120 min.
| country        = UK, U.S. 
| locations      = Warsaw, Poland
| language       = English
}} John Daly for Atlantic Film Productions. The films story line is set in 1944, during World War II, and is about a Jewish Hungarian industrialist who, in order to ensure his large familys safe passage out of the Third Reich, is forced to hand over his business and his enormously valuable possessions to the Nazis. The plot is loosely based on the life events of Hungarian Jewish industrialist Manfred Weiss and his Manfréd Weiss Steel and Metal Works.

==Plot==
Joseph Krauzenberg (Martin Landau) is a very wealthy Hungarian Jewish industrial tycoon whose fortune is mirrored in the great palaces he owns. However, by 1944 his important businesses are needed by the Nazis and Hitlers Final Solution is sweeping through Europe. 

The Nazis are greedy to accumulate wealth as easily as possible, and under the terms of the Third Reichs "Europa Plan", Krauzenberg arranges with Nazi leaders to exchange his fortune, his business holdings (textile plants, steel mills, ownership of several banks) and a collection of rare art for safe passage to Switzerland for himself, his wife Rachel (Judy Parfitt), and their family.  As the night of transaction approaches, Krauzenberg visits his large family being held by the Gestapo, and reassures them that all will be safe.
 Danny Webb).  
 Caroline Carver) are a married Aryan couple who, despite the law, are still working for Jews. 

 
As it happens, Eichmann and Himmlers suspicions are well-founded – despite appearing to be the perfect Aryan couple, Hans and Ingrid are actually Jews working deep undercover with the underground Resistance. Young, married, in love and expecting their first child, they work as valet and maid for the elderly Jewish couple. They have everything to look forward to or so they would have believed. 

In a sub-plot one of the officers, Edelhein (Christopher Fulford), the most repulsive of the group, pursues Ingrid Vassman – despite her pregnancy – as a perfect foil for implanting his precious seed in the Aryan beauty to populate the new Germany. 

As they serve the Nazis in their secret mission, Hans and Ingrid toy with the idea that they should kill Eichmann and Himmler for the greater good, even if it would mean certain death for the Krauzenbergs and themselves. Instead, the Vassmans, who express their admiration for the Krauzenbergs, ultimately confessing that they are Jewish and wish to escape with the Krauzenbergs, place their fate in the hands of the Krauzenbergs.  Nothing can be done immediately – "if we had known we could have placed you on the list of people to come with us". 

On his way to the plane that is to take him and his family to the safety of Switzerland, Josef Krauzenberg makes a deal with Dressler, a German Captain. Though we do not know what it is, he deceives Eichmann, and gives the Vassmans the means to escape.

In the final climactic scene, the Vassmans are confronted at the last checkpoint before Switzerland by Edelhein, who has discovered Dresslers treachery and has pursued them for the so-called murder of a German soldier, and of course for being Jews. However, the Reichsfuhrer himself, hearing of the events, and wanting to avoid any bloodshed that could reveal his plot concerning the Krauzenbergs to a wider audience, has sent word that they should be allowed to pass freely and safely. Angered that he has lost his chance to plant his seed in Ingrid, Edelhein, regardless of the Reichsfuhrers orders, attempts to stop them passing. This results in his demise at the hands of the commanding Major, who shoots him dead before he manages to harm them.

The film ends with the couple being reunited safely in Switzerland with the Krauzenbergs.

==Cast==
*Martin Landau – Joseph Krauzenberg, a very wealthy Hungarian Jewish industrial tycoon
*Judy Parfitt – Rachel Krauzenberg, Josephs wife
*Kenny Doughty – Hans Vassman, a Jew posing as an Aryan working in Krauzenbergs household Caroline Carver – Ingrid Vassman, posing as Aryan in Krauzenbergs household and like Hans working for the Resistance Danny Webb – Heinrich Himmler
*Christopher Fulford – Edelhein, a Nazi officer 
*Steven Mackintosh –  Adolf Eichmann

==Awards==
Kenny Doughty won Best Actor at the Palm Beach International Film Festival in 2005 for his performance as Hans Vassman.

==External links==
*  

 
 
 
 
 
 
 
 