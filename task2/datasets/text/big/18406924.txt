Dayo: Sa Mundo ng Elementalia
 
{{Infobox Film
| name           = Dayo: Sa Mundo ng Elementalia
| image          = Dayo Poster.jpg image_size      =
| caption        = Dayo Official Poster
| director       = Robert Quilao
| producer       = Cutting Edge Productions
| eproducer      = Jessie Lasaten
| aproducer      =
| writer         = Artemio Abad Eric Cabahug James Ronald and Rodfil Obeso
| music          = Jessie Lasaten
| cinematography =
| editing        =
| distributor    = Cutting Edge Productions
| released       = December 25, 2008
| run_time        =
| country        = Philippines
| awards         =
| language       = Filipino / Tagalog
| budget         = 
| gross          = PHP 5.6 million
| preceded_by    =
| followed_by    =
}}

Dayo: Sa Mundo ng Elementalia (Tourist: In the world of Elementalia) is the Philippines’  first all-digital full-length animated feature film by Cutting Edge Productions, advertised as "tradigital", a mix of traditional animation with 3D animation. It reinvents ghastly images of Philippine mythical creatures into heartwarming characters in a young boys adventure. This $1.3 million (roughly PHP 58 million) production composed of over 500 local animators features a “tra-digital animation” technique using paperless 2D and 3D technologies. It has 2D animation for its characters and 3D animation for the backdrops.

The production team of Dayo spent several months developing the story, mood, and the whole package of the animation. While writing the script, the writers consulted a book on local mythology by Maximo Ramos entitled Creatures of Philippine Lower Mythology. Compared to the usual three-year completion of a traditional full-length animation, this film aimed to be finished within a short span of two years.

Dayo is written by Artemio Abad, Jr. and Eric Cabahug and directed by Robert Quilao. It is among the eight movies (and the first of movie of its kind) that was screened during the 34th Metro Manila Film Festival in December 2008.
 34th Metro Manila Film Festival.

==Synopsis==
Dayo is a heartwarming story of overcoming ones fear and succeeding over adversity. The plot revolves around Bubuy (voiced by Nash Aguas) who is out to save his abducted grandparents in the land of Elementalia, a magical and mystical world that houses many of the Philippines mythical creatures and other enchanted elements. This locally produced animated film aims to reintroduce the other side of mythical creatures like the tikbalang, kapre, manananggal, and aswang by giving a new dimension to these typically reviled creatures.

==Production==
According to executive producer Jessie Lasaten, the film required the work of over five hundred artists and took two years to complete. The films entry into the Metro Manila Film Festival proved to be problematic when the screening committee rejected the script written by Temi Abad, Jr. and Eric Cabahug. The committee challenged Cutting Edge Productions to prove that Dayo was financially viable. Dayos soundtrack used full orchestration from Gerard Salonga and FILharmoniKA. In addition, Lea Salonga performed the movies themes song "Lipad". 

Dayos budget was revealed to be $1,300,000. 

==Voice casts==
*Nash Aguas as Bubuy
*Katrina Legaspi as Anna
*John Manalo as Carlo
*Michael V. as Narsi
*Peque Gallaga as Lolo Nano
*Noel Trinidad as Lolo Miong
*Nova Villa as Lola Nita
*Johnny Delgado as Carpio
*Pocholo Gonzales as Toti and Hal-Lan
*Pokwang as Vicky
*Gabe Mercado as Jo
*Laurice Guillen as Bruha, Diwata and Kapress
*Carl John Barrameda as Arvi
*Igi Boy Flores as Mark James "Moymoy" Obeso as Tiyanak Rodfill "Roadfill" Obeso as Tiyanak

==Reception==
===Critical===
Dayo was given a grade of "A" by the Cinema Evaluation Board, which allowed the producers a 100% amusement tax rebate. It was also endorsed by the Philippines Department of Education (DepEd) and the National Council for Childrens Television. The film was praised for presenting elements derived from folklore, myths, pop culture in the Philippine perspective. Its soundtrack and voice acting marked other strong points. On the technical side, however, flaws on animation rendering and shading were noted.  Most notably, Dayo received praise for its cel-animated portrayal of Manila as well as its catchy presentation of Filipino culture. 

===Box-Office results===

* P1,200,000 (December 25, 2008) 
* P2,200,000 (December 26, 2008) 
* P3,200,000 (December 28, 2008) 
* P3,700,000 (December 29, 2008) 
* P4,100,000 (December 31, 2008) 
* P5,600,000 (January 7, 2009) 

===Awards===
At the awards ceremony of the 2008 Metro Manila Film Festival, Dayo won four awards for sound, visual effects, musical score and song. 

====2008 Metro Manila Film Festival==== Best Sound: Albert Idioma and Whannie Dellosa Best Visual Effects: Robert Quilao Best Musical Score: Jessie Lasaten Best Theme Song: "Lipad" by Jessie Lasaten and Artemio Abad Jr., performed by Lea Salonga

==Official Soundtracks==
*Lipad - Lea Salonga
*Lipad (Rock version) - Roots of Nature
*Kapit - Moymoy Palaboy
*Daybreak - Juan Lunar
*Kasalo at Kasuyo - Noel Cabangon
*Pang-surprise - Jay Durias

== See also ==
* Urduja (film)|Urduja – a Filipino animated film
* 2008 Metro Manila Film Festival

== References ==
 

==External links==
* 
*  

 
 
 
 