The Sea Hawk (1924 film)
{{Infobox film
| name           = The Sea Hawk
| image          = The Sea Hawk - 1924 theatrical poster.jpg
| image size     = 
| alt            = 
| caption        = 1924 theatrical poster
| director       = Frank Lloyd
| producer       = Frank Lloyd
| writer         = 
| screenplay     =  
| story          = 
| based on       =  
| narrator       = 
| starring       = Milton Sills Enid Bennett Lloyd Hughes Wallace MacDonald Marc McDermott Wallace Beery
| music          = Modest Altschuler Cecil Copping John LeRoy Johnston 
| cinematography = Norbert F. Brodin
| editing        = Edward M. Roskam Frank Lloyd Productions Associated First National Pictures
| released       =  
| runtime        = 123 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         = 
| gross          = $2 million 
| preceded by    = 
| followed by    = 
}}
 the same name. 

==Plot==
At the instigation of his half brother Lionel (Lloyd Hughes), Oliver Tressilian (Milton Sills), a wealthy baronet, is shanghaied and blamed for the death of Peter Godolphin (Wallace MacDonald), brother of Olivers fiancée, whom Lionel actually has slain.  At sea Oliver is captured by Spaniards and made a galley slave, but when he escapes to the Moors he becomes Sakr-el-Bahr, the scourge of Christendom. Learning of Rosamunds (Enid Bennett) impending marriage to his half brother, he kidnaps both of them, but to avoid the risk of giving her to Asad-ed-Din (Frank Currier), the Basha of Algiers, he surrenders to a British ship.  Rosamund intercedes to save his life, and following the death of Lionel they are married.

==Background== movie with the same title (but an entirely different plot) was made in 1940 in film|1940, starring Errol Flynn.  In the remake, the studio used some key scenes from battles in the 1924 film.  They spliced the scenes into the 1940 film, thinking they could not have been done better.    The life-sized replicas were considered so well recreated, that Warner Bros. repeatedly used them in later nautical films. 
 Catalina Island, with 150 tents set up on the island for housing and support of the films 1,000 extras, 21 technicians, 14 actors and 64 sailors.   
 The Lost World, when the explorers return to London, there is a shot of the London Pavilion with a flashing sign advertising a showing of The Sea Hawk.

==Cast==
 
* Milton Sills as Sir Oliver Tressilian 
* Enid Bennett as Lady Rosamund Godolphin 
* Lloyd Hughes as Lionel Tressilian 
* Wallace Beery as Capt. Jasper Leigh 
* Marc McDermott as Sir John Killigrew
* Wallace MacDonald as Peter Godolphin 
* Bert Woodruff as Nick 
* Claire Du Brey as Siren 
* Lionel Belmore as Justice Anthony Baine 
* Christina Montt as The Infanta of Spain 
* Albert Prisco as Yusuf-Ben-Moktar 
* Frank Currier as Asad-ed-Din
* William Collier Jr. as Marsak 
* Medea Radzina as Fenzileh 
* Fred DeSilva as Ali 
* Kathleen Key as Andalusian Slave Girl 
* Hector Sarno as Tsmanni
* Robert Bolder as Ayoub 
* Fred Spencer as Boatswain 
 

==Reception==
 sea story thats yet been done up to that point".   It held that unofficial status for years. 

==References==
 

 {{cite news
|url=http://pqasb.pqarchiver.com/latimes/access/59343543.html?dids=59343543:59343543&FMT=ABS&FMTS=ABS:FT&type=current&date=Apr+07%2C+1994&author=KENNETH+TURAN&pub=Los+Angeles+Times+(pre-1997+Fulltext)&desc=A+Bounty+of+Rescued+Celluloid+Movies%3A+The+1924+%60Sea+Hawk+launches+UCLAs+monthlong+Festival+of+Preservation+tonight.+Gems+from+international+archives+are+featured.&pqatl=google
|title=A Bounty of Rescued Celluloid Movies: The 1924 `Sea Hawk launches UCLAs monthlong Festival of Preservation tonight.
|last=Turan
|first=Kenneth
|date=April 7, 1994
|work=Los Angeles Times
|pages=1, Calendar; PART–F;
|accessdate=25 November 2010}} 

 {{cite web
|url=http://www.tcm.com/tcmdb/title.jsp?stid=16041&atid=11932&category=Articles&titleName=The%20Sea%20Hawk&menuName=MAIN
|title=The Sea Hawk (1924)
|last=Wood
|first=Bret
|work=Turner Classic Movies
|accessdate=25 November 2010}} 

 {{cite book
|last=Sabatini
|first=Rafael
|title=The Sea-Hawk
|url=http://books.google.com/books?id=d6RXKfqB9GsC&pg=PR8&dq=%22The+Sea+Hawk%22,+1924&hl=en&ei=OSruTM33MYOqsAOTyczzCw&sa=X&oi=book_result&ct=result&resnum=1&ved=0CCcQ6AEwAA#v=onepage&q=%22The%20Sea%20Hawk%22%2C%201924&f=false
|edition=reprint
|year=2002
|publisher=W. W. Norton & Company
|isbn= 978-0-393-32331-3
|page=viii}} 

 {{cite book
|last= Druxman
|first=Michael B.
|title=Make it again, Sam: a survey of movie remakes
|edition=illustrated
|year=1975
|publisher=A. S. Barnes
|isbn= 978-0-498-01470-3}} 

 {{cite web
|url=http://www.moviediva.com/MD_root/reviewpages/MDSeaHawk.htm
|title=The Sea Hawk (1924)
|work=Movie Diva
|accessdate=25 November 2010}} 

 

==External links==
*  
*   at Silentera.com
*  

 

 
 
 
 
 
 
 
 
 