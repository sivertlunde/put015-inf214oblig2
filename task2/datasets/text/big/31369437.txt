8.5 Hours
{{Infobox film
| name           = 8.5 Hours
| image          = 8point5 film poster.jpg
| alt            = 
| caption        = Official film Poster
| director       = Brian Lally
| producer       = Brian Lally
| writer         = Brian Lally
| starring       = Lynette Callaghan Victor Burke
| music          = Karim Elmahmoudi
| cinematography = Arthur Mulhern
| editing        = 
| studio         = 
| distributor    = Breaking Glass Pictures
| released       =  
| runtime        = 112 minutes
| country        = Ireland 
| language       = English
| budget         = 
| gross          = 
}}
8.5 Hours is a 2008 Irish film written and directed by Brian Lally. The film tells the story of one frantic day in the lives of four Dublin software workers during the final months of Irelands Celtic Tiger boom years. The film stars Lynette Callaghan, Art Kearns, Victor Burke and Jonathan Byrne. The film won several awards on the Irish film festival circuit, including Best Irish Feature Film at the International Film Festival Ireland in Clonmel (2009), Best Actress award for Lynette Callaghan at the Yellow Fever Independent Film Festival in Belfast (2009) and was awarded 2nd place in the Best Feature category at the Waterford Film Festival.

8.5 Hours went on a limited Irish cinema release in January 2010 and was released on DVD throughout Ireland in November 2010. It was released on DVD and VOD in North America by the American distributor Breaking Glass Pictures in March 2011. The film was the first feature produced by the Irish production company Instigator Films.

== Plot == ensemble drama that mixes serious drama with black comedy and has some very dark and unexpected plot twists. The film is set in 2007 just before Irelands Celtic Tiger was about to end and the plot intertwines the four parallel narratives of the lead characters; Rachel, Eoin, Frank and Tony. Their lives are linked as they all work together in the same office in a small Irish software company.
 
Rachel, played by Lynette Callaghan, is an ambitious marketing executive who on turning 30 appears to panic about what she perceives as her lack of achievement at a time of great economic opportunity. She becomes obsessed with an overpriced apartment in Dublins prestigious Ballsbridge area and after a bruising opening scene where her boyfriend ends their relationship because she had a drunken one night stand, Rachel is determined to buy the apartment on her own at whatever cost. All closing bids for the apartment must be in by the end of the day so for most of the film, Rachel is preoccupied with a race-against-time to raise her salary in order to get a mortgage she cant afford. As she runs out of time, she becomes increasingly desperate and irrational and ends up blackmailing her boss to give her an unreasonable pay rise.

Eoin, played by Victor Burke is a computer programmer who is about to get married to his fiancee Lisa (played by Clodagh Reid). Initially he appears to be a very ordinary character who is becoming stressed about his impending marriage due to its spiraling costs as Lisa makes increasingly extravagant wedding arrangements. However, as this story unfolds, Eoins character is revealed to have a history of bisexual encounters with other men. Simon, a handsome French man (played by French actor Frederic Ledoux) who seduced Eoin at a party the previous year, unexpectedly resurfaces and wants to meet Eoin that day for a coffee. The meeting with Simon puts Eoin on the spot as Simon confronts him about getting married and whether he prefers boys or girls.

Frank, played by Art Kearns is a senior computer programmer in the company. His story is revealed quite late in the film. For most of the early section, he appears to be a grumpy, angry character who has some issue with his wife, and away from the eyes of his fellow workers, vents his anger by smashing a computer printer to pieces with his bare hands. He doesnt communicate much with the other characters and most of his story is internal and revealed through flashbacks as he mulls over the past. Eventually, it is revealed that he is troubled over his wifes uncomfortable fondness with his best friend David (played by Brendan McCormack).
 womaniser who casually shambles through his job in the software company. His story opens with a bawdy bedroom romp but as he cant even get the girls name right, that soon ends in a row. Arriving late at work, Tony appears to be a happy-go-lucky type who never takes things too seriously. However, he is troubled by some strange, puzzling letters he has been receiving and after one is left at work, he visits a church to meet a bizarre old woman called Maggie (Geraldine Plunkett). She reveals that she is the mother of a girl he had an affair with the previous year who killed herself shortly afterwards. She accuses him of treating her badly. Tony denies the claims and storms off. For the rest of the day, Tony is deeply troubled by memories of not returning the girls calls when she was clearly distressed and in the final scene of the film, Maggie shows up at his apartment to tell him a piece of very disturbing news.

== Cast ==
* Lynette Callaghan as Rachel.
* Art Kearns as Frank.
* Jonathan Byrne as Tony.
* Victor Burke as Eoin.
* Geraldine Plunkett as Maggie.
* Tom OSullivan as Martin.
* Gary Egan as Ian.

== Production == Meath in February and March 2007 with additional scenes and second unit material being shot in May and September of that year. The office of the software company featured in the film is an actual company office in Merrion Square in Dublin. After initial screenings of early edits at Irish film festivals (Galway Film Fleadh, Waterford Film Festival, Mid-Ulster Film Festival), a more advanced edit was shown at the Marche Du Film at the Cannes Film Festival in May 2009. This screening and subsequent meetings led to the film being distributed in the USA by Philadelphia based distributor, Breaking Glass Pictures. Brian Lally was constantly tweaking the edit throughout 2009 as it appeared in more film festivals and the final edit was not completed until December 2009, just a few weeks before its Irish cinema release. The film was independently financed and produced on a very small budget of under 100,000 euro. Unusually, for a film that was released in Irish cinemas, it did not receive any funding from the Irish Film Board. It was shot on digital video, initially on the HDV format but then converted to High Definition for final editing and grading. A digital cinema print (JPEG 2000) was created for its Irish cinema release and it was one of the first Irish films to be released entirely digitally in Irish cinemas. The JPEG2000 cinema print was created by XDC Cinema in Belgium.

The film was released on DVD in Ireland in November 2010 and became the first ever Irish film DVD to carry the Guaranteed Irish logo. This branding device is used to help Irish consumers identify products which are produced in Ireland.

== Reception ==
The critical response to 8.5 Hours was overwhelmingly negative. On its cinema release in Ireland, the film received a harsh review from The Irish Independent  which appeared appalled by the films depiction of the Dublin middle class behaving badly, and wrote that the film was "poisonous to the cause of Irish tourism".  The Irish Times listed the film among the 10 worst of 2010. 

==References==
 

== External links ==
*  
*  
*  
*  
*  
*  

 
 
 