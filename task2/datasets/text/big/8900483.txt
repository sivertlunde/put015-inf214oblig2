Sands of the Kalahari
 
 
{{Infobox film
| name           = Sands of the Kalahari
| image          = Original movie poster for the film Sands of the Kalahari.jpg
| image_size     = Frank McCarthy
| director       = Cy Endfield
| producer       = Stanley Baker Cy Endfield 
| writer         = William Mulvihill (novel) Cy Endfield
| narrator       =
| starring       = Stuart Whitman Stanley Baker Susannah York
| music          = John Dankworth
| cinematography = Erwin Hillier
| editing        =
| distributor    = Paramount Pictures
| released       = 1965
| runtime        = 119 min.
| country        = United Kingdom
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
 1965 British Spain and released by Paramount Pictures.

==Synopsis==
The plot involves a disparate and desperate group of plane crash survivors thrust into a savage mountainous desert region somewhere within present-day Namibia. Brian OBrien - played by Stuart Whitman - is a big game hunter and the best survivalist of the group. Shortly after his plane crashes, stranding its passengers, he risks his life by re-entering the burning wreck and recovering vital supplies, including a hunting rifle; however, OBriens motives are far from noble. Thinking his own chances will be improved by the absence of competition, he ruthlessly eliminates his fellow survivors, one by one, intending to leave only Grace Munkton (Susannah York) alive, an "Eve" for his "Adam."

In addition to OBriens treachery, the survivors are menaced by a troop of baboons inhabiting the area. Initially content to holler at the intruders from the distance, the animals gradually become more aggressive.

Before OBrien is able to bring his dastardly plan to fruition, a fellow survivor he had driven off into the desert at gun point (presumably to die) returns with a rescue party. The remaining survivors make their escape in a helicopter. OBrien, however, aware he will be prosecuted for murder if he returns to civilization, chooses to remain behind.

With OBrien the sole human in their domain, the baboons become more belligerent. At first he is able to keep them at bay with his rifle.  When he runs out of ammunition, OBrien brazenly challenges the alpha male to a fight and succeeds in killing him with his bare hands. In the films final shot the remaining baboons encircle the lone hunter and ominously amble towards him.

==Cast==
*Stuart Whitman as Brian OBrien
*Stanley Baker as Mike Bain
*Susannah York as Grace Munkton
*Harry Andrews as Grimmelman
*Theodore Bikel as Bondrechai
*Nigel Davenport as Sturdevan

==Production==
Joseph E. Levine was keen for Stanley Baker and Cy Endfield to make another film in Africa after the success of Zulu (1964 film)|Zulu (1963). They initially announced plans to adapt Wilbur Smiths debut novel, When the Lion Feeds but eventually decided on Sands of the Kalahari. Baker persuaded his childhood friend Richard Burton to star along with his wife Elizabeth Taylor, but Taylor was reluctant to film in Africa and demanded more money than Levine was interested in paying. Burton pulled out and George Peppard and Susannah York were cast instead. Mel Neuhaus, Apes of Wrath, Examiner.com 19 July 2011  However shortly after filming commenced, Peppard left the project   (probably for The Blue Max), and Stuart Whitman was flown in as a replacement.  Peppard Ankles Kalahari Set
Hopper, Hedda. Los Angeles Times (1923-Current File)   26 Mar 1965: d13. 

==See also==

* Survival film, about the film genre, with a list of related films

==Notes==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 