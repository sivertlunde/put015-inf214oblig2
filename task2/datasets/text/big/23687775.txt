Adorables créatures
{{Infobox film
| name           = Adorables créatures
| image          = Adorables créatures.jpg
| image_size     =
| caption        = 
| director       = Christian-Jaque
| producer       = 
| writer         = Charles Spaak
| narrator       = 
| starring       = Daniel Gélin, Antonella Lualdi, Danielle Darrieux, Martine Carol
| music          =  
| cinematography =    
| editor       =  
| distributor    =  1952
| runtime        = 110 minutes
| country        = France and Italy
| language       = French
| budget         = 
}}
 1952 cinema French and Italian romantic comedy film directed by Christian-Jaque.

==Cast==
*Daniel Gélin	... 	André Noblet
*Antonella Lualdi	... 	Catherine Michaud
*Danielle Darrieux	... 	Christine
*Martine Carol	... 	Minouche
*Edwige Feuillère	... 	Denise Aubusson
*Renée Faure	... 	Alice
*Georges Chamarat	... 	Edmond, Catherines Father
*Daniel Lecourtois	... 	Jacques
*Marilyn Buferd	... 	Evelyne (as Marilyn Bufferd)
*Jean-Marc Tennberg	 ... 	Pianist
*France Roche	... 	Françoise
*Giovanna Galletti	... 	Director
*Georges Tourreil	... 	Étienne
*Raphaël Patorni	... 	Man
*Robert Rollis	... 	Bob
*Judith Magre	... 	Jenny

== External links ==
*  

 

 
 
 
 
 
 


 
 