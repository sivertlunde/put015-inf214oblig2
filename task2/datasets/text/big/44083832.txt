Pushyaraagam
{{Infobox film 
| name           = Pushyaraagam
| image          =
| caption        =
| director       = C Radhakrishnan
| producer       =
| writer         = C Radhakrishnan
| screenplay     = C Radhakrishnan Madhu Jayan Sharada Srividya
| music          = A. T. Ummer
| cinematography = KB Dayalan
| editing        = G Venkittaraman
| studio         = Universal Movies
| distributor    = Universal Movies
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film, Sharada and Srividya in lead roles. The film had musical score by A. T. Ummer.   

==Cast== Madhu
*Jayan Sharada
*Srividya
*Manavalan Joseph
*K. P. Ummer Ravikumar
*Silk Smitha

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Cheramangalam and Sakunthala Rajendran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Madhuramadhuramoru || Vani Jairam || Cheramangalam || 
|-
| 2 || Munthirithenozhukum Saarangame || K. J. Yesudas || Cheramangalam || 
|-
| 3 || Oru manikingini || K. J. Yesudas, S Janaki || Sakunthala Rajendran || 
|-
| 4 || Pathupetta || S Janaki || Sakunthala Rajendran || 
|}

==References==
 

==External links==
*  

 
 
 

 