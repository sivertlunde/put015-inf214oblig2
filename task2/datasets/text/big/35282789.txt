Badman's Territory
{{Infobox film
| name           = Badmans Territory
| image          = 
| image size     = 
| caption        = 
| director       = Tim Whelan
| producer       = Nat Holt
| writer         = 
| based on       = 
| starring       = Randolph Scott
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| studio         = RKO
| prod. company  = 
| released       =  
| runtime        = 
| country        = United States
| language       = 
| budget         = 
}} Western film starring Randolph Scott. It was followed by the loose sequels Return of the Bad Men (1948) and Best of the Badmen (1951).

==Plot==
Just north of Texas and west of the Oklahoma border is "Badmans Territory," a region not yet governed by statehood. This is where Jesse James and brother Frank head after a train robbery, along with their partner, Coyote.

Mark Rowley, a lawman, and his deputy brother Johnny are after the James gang. So is a ruthless U.S. marshal named Hampton who shoots anybody who gets in his way. He even wings Johnny Rowley just to take the newly captured Coyote away from him.

In the town of Quinto, newspaper editor Henryetta Alcott is a crusader for law and order. Mark takes an immediate liking to her. He also helps Belle Starrs horse win a big race.

Johnnys injuries mend, but the Dalton gang persuades Johnny to go bad and join them. Mark tries to dissuade him. He shoots a man named McGee who stole his horse. Hampton puts up wanted posters on both Rowleys.

Henryetta spreads the word that Oklahoma has annexed this territory into the union. Mark is appointed a "regulator" and proposes marriage to Henryetta before he rides to Coffeyville, Kansas, where the Daltons are about to pull a job with Johnny as part of the gang.

Johnny is shot and killed, and Hampton also kills Coyote. A determined Mark Rowley must deal with Hampton once and for all if he and Henryetta are to have a future together.

==Cast==
 
*Randolph Scott as Mark Rowley
*George "Gabby" Hayes as Coyote Ann Richards as Henryetta Alcott Ray Collins as Colonel Farewell James Warren as John Rowley
*Morgan Conway as William Hampton
*Virginia Sale as Meg
*John Halloran as Hank McGee
*Andrew Tombes as Doc Grant
*Richard Hale as Ben Wade
*Harry Holman as Hodge
*Chief Thundercloud as Chief Tahlequah
*Lawrence Tierney as Jesse James
*Tom Tyler as Frank James Steve Brodie Bob Dalton Grat Dalton Bill Dalton Sam Bass
*Isabel Jewell as Belle Starr
*Chet Brandenburg as Townsman (uncredited)
*Budd Buster as Docs Friend (uncredited)
*George Chesebro as Johnny (uncredited)
*Neal Hart as Townsman (uncredited)
*Harry Harvey, Sr. as Stationmaster (uncredited)
*Robert Homans as Judge (uncredited) Ben Johnson as Deputy Marshall (uncredited)
*Ethan Laidlaw as Lt. Patton (uncredited)
*Elmo Lincoln as Dick Broadwell (uncredited)
*Theodore Lorch as Citizens Committee Member (uncredited)
*Wilbur Mack as Cattle Baron (uncredited)
*Kermit Maynard as Carson (uncredited)
*Bud Osborne as Deputy Dan Mercer (uncredited)
*Emory Parnell as Bitter Creek (uncredited)
*Snub Pollard|Snub Pollard as Town Barber (uncredited)
*Jason Robards, Sr. as Alert Coffeyville Citizen (uncredited)
*Buddy Roosevelt as Lt. Lake (uncredited)
*Harry Semels as Bettor (uncredited)
*Robert J. Wilke as Deputy Marshall (uncredited)
 

==Reception==
The film made a profit of $557,000. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p211 

==References==
 

==External links==
*  at TCMDB
* 

 
 
 
 
 