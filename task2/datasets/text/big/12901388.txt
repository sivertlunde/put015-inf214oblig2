The Mourning Forest
{{Infobox Film
| name           = The Mourning Forest
| image          = The_Mourning_Forest_poster.jpg
| caption        = 
| director       = Naomi Kawase
| producer       = 
| writer         = Naomi Kawase
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 26 May 2007 (premiere at 2007 Cannes Film Festival|Cannes)
| runtime        = 97 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 2007 Japanese Grand prix at the 2007 Cannes Film Festival.    It tells the story of a nurse (played by Machiko Ono) who is grieving for her dead child.  She works at a nursing home and grows close to an elderly man (Shigeki Uda), suffering from dementia, who is searching in the local forest for something connected to his dead wife that he cannot explain.

==Cast==
* Yôichirô Saitô - Machikos husband
* Kanako Masuda - Mako
* Machiko Ono - Machiko
* Shigeki Uda - Shigeki
* Makiko Watanabe - Wakako

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 


 