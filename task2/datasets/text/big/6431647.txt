Pauvre Pierrot
{{Infobox film
| name           = Pauvre Pierrot
| image          = Pauvre Pierrot.png
| caption        = Hand-painted scene
| director       = Charles-Émile Reynaud
| producer       = 
| writer         = 
| starring       = 
| music          = Gaston Paulin
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 15 minutes (approx)
| country        = France Silent
| budget         = 
}}
 short animation|animated film directed by Charles-Émile Reynaud. It consists of 500 individually painted images and lasts about 15 minutes.    
 
It is one of the first animated films ever made, and alongside Le Clown et ses chiens and Un bon bock was exhibited in October 1892 when Charles-Émile Reynaud opened his Théâtre Optique at the Musée Grévin.  It was the first film to demonstrate the Theatre Optique system developed by Reynaud in 1888, and is also believed to be the first usage of film perforations. The combined performance of all three films was known as Pantomimes Lumineuses.

These were the first animated pictures publicly exhibited by means of picture bands. Reynaud gave the whole presentation himself manipulating the images. 
 

==References==
 
 

== External links ==
* 
*  

 
 
 
 
 
 


 
 