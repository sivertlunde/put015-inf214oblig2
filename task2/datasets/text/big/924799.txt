Honey, I Blew Up the Kid
{{Infobox film
| name=Honey, I Blew Up the Kid
| image=honey_I_blew_up_the_kid_film_poster.jpg
| border = yes
| caption = Theatrical release poster
| director = Randal Kleiser
| producer = Dawn Steel Edward S. Feldman
| screenplay = Garry Goodrow Thom Eberhardt  Peter Elbling
| story  = Garry Goodrow
| based on = Characters by: Stuart Gordon Brian Yuzna Ed Naha
| starring = Rick Moranis Marcia Strassman Robert Oliveri Keri Russell John Shea Lloyd Bridges Amy ONeill Ron Canada
| music = Bruce Broughton
| cinematography = John Hora
| editing = Harry Hitner
| studio = Walt Disney Pictures Buena Vista Pictures
| released =  
| country = United States
| runtime = 89 minutes
| language = English
| budget = $40 million
| gross = $76 million 
}}
 love interest and babysitter of Adam, the Szalinskis new two-year-old son, whose accidental exposure to Waynes new industrial-sized growth machine causes him to gradually grow to enormous size. Made only three years after Honey, I Shrunk the Kids, this film is set five years after the events depicted in the previous film.

The antagonist to the Szalinskis is Dr. Charles Hendrickson (John Shea), who wants the giant Adam stopped at all costs and would like to take over Waynes invention that is now owned by the major corporation they work for, which is in turn owned by the kind Clifford Sterling (Lloyd Bridges).

This film would be followed by one last sequel in 1997, this time a  .

==Plot== previous film, enlarge objects. His research is hindered by Charles Hendrickson, jealous that Wayne had been put in charge of the research department and threatens to sabotage him to take his place.

It is Friday morning, Diane is cooking breakfast, Nick is practicing his guitar, Wayne is getting ready for work and Adam is in his playpen watching a small TV. Waynes wife Diane leaves to take their daughter Amy to college, leaving him in charge of watching their 2-year old son Adam along with their fifteen-year-old son Nick; now obsessed with guitars and an unrequited crush on Adams babysitter Mandy. Wayne works at the Sterling Laboratories where his boss, Charles Hendrickson and the other lab people are busy testing out their new machine. Wayne arrives late as the experiment has ended. At home, Wayne is cooking dinner, but the oven makes a fire and Nick extinguishes the flames using a fire extinguisher. Whilst Wayne is on the phone to Diane, he is told by Quark, that Adam is making a dash for the ice-cream truck, not realizing he is too young to be outside on his own. Wayne grabs Adam and takes him back into the home where Adam blows on the burnt roast chicken. At suppertime, Wayne, Nick and Adam are at the dinner table. Adam offers his big brother a sandwich, to which Nick storms off to his bedroom. At bedtime, Wayne is getting his baby son to sleep with Adams favourite stuffed toy, "Big Bunny". Adam soon falls asleep after singing "Twinkle, twinkle little star". Nick tries to make a phone call to Mandy Park, but his call is disconnected when Adam pulls a wire out of a socket. On Saturday morning, Wayne decides to take Adam and Nick into the lab in order to test his enlarging machine on Big Bunny. But while they are looking over the controls and the countdown begins, Adam slips out of his stroller, gets in the way of the machine and attempts to get Big Bunny back. Adam is hit by the beam which causes him to fly away from the machine and Big Bunny ending up on the floor with his legs in the air, giggling. But then he picks himself up and returns to his stroller before he is noticed. The beam causes a power surge that blacks the facility out, and Wayne and Nick leave none the wiser. At home, Wayne cooks Adam some lunch after calling Mandy to come and look after Adam. As soon as Wayne goes, Adam looks at the microwave that starts flashing blue lightning around it. Adam grabs Big Bunny and gets closer to the microwave. It is soon discovered that electric waves cause Adam to start growing in size after a microwave oven causes him to sprout to seven feet in height. They return to the lab to try and correct this but Waynes unauthorized use causes Hendrickson to revoke his access and ban him from the facility. Diane returns home and discovers the problem and she and Wayne leave to go to the warehouse where the original shrink ray is crated, hoping to use that to undo the mess. Meanwhile, Mandy arrives to babysit Adam and faints at the sight of him. Nick ties her up to keep her from fleeing as he explains the situation to her, meanwhile, a television Adam is watching causes him to grow to fourteen feet in height and he breaks out of the house. Mandy agrees to help Nick find his brother, while at the lab, Hendrickson discovers that Adam was hit by the ray and orders his men to find and detain Adam. They capture the giant toddler and stow him in a semi-truck en route to the labs, but power cables along the side of the highway cause him to grow even larger and he escapes. Adam then finds Nick, his brother and Mandy, the babysitter below him. So he picks them up and puts them in his front pocket of his overalls, mistaking them as new toys.

Now, over fifty feet in height, Adam sees the lights of Las Vegas in the distance and makes his way toward them, thinking it to be a playground. When Nick and Mandy try to stop him, Adam mistakes them and the car they are driving as toys, and picks them up, putting them in his pocket. The giant baby incites terror for everyone who sees him, and soon Adam is surrounded by journalists, authorities and Hendrickson and his men. Adam rips the guitar of a Hard Rock Cafe off of its foundations and begins to play with it, imitating Nick. Hendrickson has his men fire tranquilizers at Adam from a helicopter, but the first shot misses and the second one shoots at the guitar electrocuting Adam, making him cry. The public sees that he is an innocent baby not a menace and are now sympathetic toward him as Wayne and Diane as well as Waynes boss Mr. Sterling arrive with the shrink ray to return him to normal. Wayne says that it requires thirteen seconds for the shrink ray to establish a lock on Adam and he needs to hold still. Seeing that at their size, Adam would not see them as his parents, Diane orders Wayne to use the ray to enlarge her. Hendrickson orders his men to fire again at Adam, but a giant sized Diane intervenes, making the helicopter back off. Reunited with her son, Diane has him hold still as though they are going to take a picture, while Wayne targets, then successfully shrinks them both back to normal size. Hendrickson tries to have Wayne arrested for causing the chaos, having meant no harm to Adam by trying to shoot him with the tranquilizers, but Diane punches Charles knocking him unconscious and Sterling sees Wayne and Diane as heroes and concerned parents, and fires Hendrickson instead for his methods. But Wayne realizes he accidentally shrunk Nick and Mandy which were in Adams front pocket of his overalls.

Finding that Nick and Mandy had fallen out of Adams pocket after his shrinking, Wayne discovers Nick and Mandy alone, having been shrunk in size. Nick waves Wayne away as he puts his arm around Mandy, and the two share a kiss. Back at home, life is back to normal, though they discover, to Adams delight, that his Big Bun is still at its enormous size. As Wayne and Diane kiss, the credits roll.

==Cast==
* Rick Moranis as Wayne Szalinski
* Marcia Strassman as Diane Szalinski
* Amy ONeill as Amy Szalinski
* Robert Oliveri as Nick Szalinski
* Daniel & Joshua Shalikar as Adam Szalinski
* John Shea as Dr. Charles Hendrickson
* Lloyd Bridges as Clifford Sterling
* Keri Russell as Mandy Park
* Ron Canada as Marshall Brooks
* Gregory Sierra as Terence Wheeler
* Michael Milhoan as Captain Ed Myerson
* Leslie Neale as Constance Winters
*Julia Sweeney as a Nosey neighbor

==Production==
The film was not originally written as a sequel to Honey, I Shrunk the Kids. Originally titled Big Baby, it was about a toddler who grew to giant size by a freak accident involving a growth ray and eventually terrorized Las Vegas in a non-violent, yet Godzillaesque way. Disney saw the possibilities of making this into a sequel to Honey and rewrote the script. Whereas most of the characters from Big Baby were rewritten as characters from Honey, I Shrunk the Kids, there was no character in the original that "Amy Szalinski" could replace, so she is seen going away to college in the beginning of the film. 

===Casting===
Rick Moranis returns from the original film to portray "wacky" inventor Wayne Szalinski. Also returning is his wife, Diane, who is portrayed by Marcia Strassman. Amy ONeill and Robert Oliveri return to portray the Szalinski children, Amy and Nick. Nick has matured in his personality and interests since the last film. He is still considered "nerdy", but has taken more interest in girls and guitars.

Casting director Renee Rousselot searched over 1,000 small children for someone to portray Adam, the newest addition to the Szalinski clan. She searched for mostly three- to four-year-old boys because a younger child was thought to be problematic. She came across twins Daniel and Joshua Shalikar, from New Jersey and immediately cast them in December 1990. One twin would act in the morning, while the other was eating lunch or taking a nap. Baby consultant Elaine Hall Katz and director Randal Kleiser would plan the twins scenes a week in advance. Tom Smith reported that, "On his own, Dan was almost too adventuresome to repeat one move, and Josh seemed very cautious. Put them together and they could do anything." However, the film did have difficulties in working with such small children, and one crew member later remarked it was "like playing hopscotch on hot coals". {{cite news |url=http://www.ew.com/ew/article/0,,311330,00.html
|title=Honey, the Kids Coulda Blown the Movie
|author=Steve Daley
|date=August 7, 1992
|accessdate=2009-09-21
 |work=Entertainment Weekly}} 
At the time, the Shalikar twins were scheduled to appear in two more Honey films. They did appear once, but were recast in Honey, We Shrunk Ourselves.

In the film, Nick has a crush on a girl named Mandy Park, played by Keri Russell in her first feature film. John Shea portrays Dr. Charles Hendrickson, who is scheming to get Waynes control of the project, while Lloyd Bridges portrays Clifford Sterling, the owner of Sterling Labs.

Fred Rogers and Richard Simmons are also seen in videos in TV scenes in the film.

===Direction=== White Fang fame, was chosen to direct this film, replacing Joe Johnston. Kleiser would return to film with the cast in the 3D show, Honey, I Shrunk the Audience, which was at several Disney parks until 2010. Like its predecessor, and Grease (film)|Grease, Honey, I Blew Up the Kid had animated opening credits.
 Wet n Wild in Las Vegas. It closed in 2004, twelve years after the film.

Special effects were used heavily throughout the film, but some were not. When Adam knocks down his bedrooms door, production designer Leslie Dilley created a set with miniature furniture about four feet away from the camera, while the adult actors would be about fifteen feet away. Kleiser recalled, "Danny was generally better at improvising and fresh reactions. Josh was better at following directions, so we would alternate." {{cite news |url=http://www.ew.com/ew/article/0,,310541,00.html
|title=Blowing Up Baby
|author=Steve Daley
|date=May 22, 1992
|accessdate=2009-09-21
 |work=Entertainment Weekly}} 

===Lawsuit before release===
Disney would later find itself the subject of a lawsuit as a result of the film. The suit was filed in 1991 by Mark Goodson Productions director Paul Alter, who claimed to have come up with the idea of an oversized toddler after babysitting his granddaughter and watching her topple over building blocks. He wrote a screenplay titled "Now, Thats a Baby!", which had not been made into a film but had received some sort of treatment beforehand.  Alter claimed there were several similarities between the movie and his script, which consisted of the baby daughter of two scientists fall victim to a genetic experiment gone wrong instead of an enlarging ray. The case went to trial in 1993, with the jury finding in Alters favor. Disney was forced to pay $300,000 in damages. 

==Reception==

===Box office===
The film opened on July 17, 1992 to 2,492 theatres, almost twice as many as the first film. It was No. 1 on opening weekend with $11,083,318, and grossed $58,662,452 in the U.S. 

===Critical===
 
The film has received generally mixed reviews. It has a "rotten" rating of 41% at Rotten Tomatoes.    Desson Thompson and Hal Hinson, both writers from the Washington Post, agreed that the film was "a one-joke film." Roger Ebert, from the Chicago Sun-Times, said that Adam "didnt participate in the real world but simply toddled around." {{cite news |url=http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/19920717/REVIEWS/207170301/1023
|title=Roger Eberts Report on Honey, I Blew Up the Kid
|year=2000
|accessdate=2009-09-21
| work=Chicago Sun-Times}} 

==Soundtrack==
{{Infobox album
| Name   =Honey, I Blew Up the Kid
| Type   =Soundtrack
| Longtype =
| Artist  =Bruce Broughton
| Cover   =
| Released =1992
| Recorded =
| Genre   =Soundtrack
| Length  =39:57
| Label   =Intrada Records
| Producer =Bruce Broughton
}}

Intrada Records released the record in 1992, in time for the films release. The score was composed and conducted by Bruce Broughton, who would return to provide the score for Honey, I Shrunk the Audience. "Stayin Alive" by the Bee Gees appears in the film. So does "The Loco-Motion|Loco-Motion" by Carole King, Gerry Goffin, and "Ours If We Want It" written by Tom Snow and Mark Mueller. The soundtrack album consists of just the score.

===Track listing===
# "Main Title"&nbsp;– 3:03
# "To the Lab"&nbsp;– 1:53
# "Adam Gets Zapped"&nbsp;– 0:35
# "Putting on Weight?"&nbsp;– 1:19
# "Macrowaved"&nbsp;– 3:15
# "Howd She Take It?"&nbsp;– 3:11
# "Sneaking Out"&nbsp;– 1:12
# "Dont Touch That Switch!"&nbsp;– 0:26
# "The Bunny Trick"&nbsp;– 2:41
# "Get Big Bunny"&nbsp;– 4:11
# "Clear the Streets!"&nbsp;– 3:00
# "Car Flight"&nbsp;– 4:38
# "Ice Cream!"&nbsp;– 3:47
# "Look at That Mother!"&nbsp;– 2:26
# "Thats All, Folks!"&nbsp;– 4:20
==Home media==
Honey, I Blew Up the Kid was first released on VHS and Laserdisc on January 6, 1993. The film was released on a bare-bones DVD in 2002. While the VHS release contained no bonus material whatsoever, the laserdisc release contains the 1992 animated short film, Off His Rockers directed by Barry Cook, which accompanied the theatrical release. To date, Off His Rockers has only appeared on the laserdisc release of this film, making its availability rare, although the short can be viewed on YouTube. 

The film was released on VHS in 1997, alongside its predecessor to coincide with the release of the third film in the series, Honey, We Shrunk Ourselves.

==See also==
The Amazing Colossal Man a 1957 film featuring a giant that wrecks Las Vegas

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 