Marguerite de la nuit
{{Infobox film
| name           = Marguerite de la nuit
| image          = Marguerite de la nuit poster.jpg
| image size     =
| caption        =
| director       = Claude Autant-Lara
| producer       = Cino Del Duca
| writer         = Ghislaine Autant-Lara  Gabriel Arout
| narrator       =
| starring       = Michèle Morgan   Yves Montand
| music          = René Cloërec
| cinematography = Jacques Natteau
| editing        = Madeleine Gug
| studio         = Cino del Duca Del Duca Films Société Nouvelle des Établissements Gaumont (SNEG)
| distributor    = Société des Etablissements L. Gaumont
| released       = 18 January 1955
| runtime        = 125 min
| country        = Italy / France
| language       = French
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} Pierre Dumarchais. The film stars Michèle Morgan and Yves Montand.

It tells the story of an older pedant who buys adolescence from Satan.

==Cast==
*Michèle Morgan as  Marguerite
*Yves Montand as  M. Léon
*Jean Debucourt as  Lhomme austère
*Jacques Clancy as  Angelo
*Paul Demange as  Un client du Pigalls
*Jacques Erwin as  Le ténor
*Camille Guérini
*Suzet Maïs as  Une cliente du Pigalls
*Max Mégy
*Geneviève Morel as  La concierge
*Fernand Sardou as  Le patron du café
*Hélène Tossy as  La patronne du café
*Massimo Girotti as  Valentin
*Louis Seigner as  Lhomme de lhôtel
*Jean-François Calvé as  Georges Faust Palau as  Dr. Faust

==External links==
* 
*  at Alice Cinema
* 
* 

 
 

 
 
 
 
 
 
 


 
 