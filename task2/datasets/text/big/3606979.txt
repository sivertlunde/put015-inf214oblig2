The Lucky Texan
{{Infobox Film
| name           = The Lucky Texan
| image          = File:Lucky Texan lobby card.jpg
| caption        = Lobby card
| director       = Robert N. Bradbury
| producer       = Paul Malvern
| writer         = Robert N. Bradbury
| starring       = {{plain list|
*John Wayne
*Barbara Sheldon
}}
| cinematography = Archie Stout
| editing        = Carl Pierson
| distributor    = Monogram Pictures Corporation
| released       =  
| runtime        = 55 minutes
| country        = United States
| language       = English
}} Western film featuring John Wayne, five years before his breakthrough appearance in Stagecoach (1939 film)|Stagecoach, Barbara Sheldon, Gabby Hayes, and legendary stuntman–actor Yakima Canutt. It was directed by Robert N. Bradbury who also wrote it.

The plot concerns Wayne finding gold and making the mistake of trusting the local assayer.

It also contains a rare (perhaps unique) instance of "Gabby" Hayes sans beard and in drag.

==Cast==
* John Wayne as Jerry Mason
* Barbara Sheldon as Betty Benson
* Lloyd Whitlock as Harris
* George "Gabby" Hayes as Jake "Grandy" Benson
* Yakima Canutt as Joe Cole
* Eddie Parker as Al Miller (Sheriffs son)
* Gordon De Main as Banker Williams 
* Earl Dwire as Sheriff Miller

==See also==
* John Wayne filmography
* 1934 in film

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 


 