19 Red Roses
 
{{Infobox film
| name           = 19 Red Roses
| image          = 
| caption        = 
| director       = Esben Høilund Carlsen
| producer       = Per Årman Erik Crone Jørgen Lademann
| writer         = Esben Høilund Carlsen Torben Nielsen
| starring       = Henning Jensen
| music          = 
| cinematography = Henning Camre
| editing        = Lars Brydesen
| studio         = 
| distributor    = 
| released       =  
| runtime        = 99 minutes
| country        = Denmark
| language       = Danish
| budget         = 
| gross          = 
}}

19 Red Roses ( ) is a 1974 Danish crime film directed by Esben Høilund Carlsen and starring Henning Jensen.

==Plot==
The film follows Detective Ancher (Poul Reichhardt) and his team as they investigate a series of murders that happened over a period of days.  Seemingly unrelated at first, the investigators soon deduce that the killings are connected and stem from an incident that associates of the victims were all involved in.

==Cast==
* Henning Jensen - William Brehmer
* Poul Reichhardt - Kriminalassistent Ancher
* Jens Okking - Kriminalassistent Brask
* Ulf Pilgaard - Kriminalassistent Rieger
* Holger Juul Hansen - Kriminalkommisær Runge
* Troels Munk - Henri Durant
* Lisbet Lipschitz - Frk. Durant
* Helle Virkner - Marianne Durant
* Mogens Brix-Pedersen - Otto Lintz
* Birgit Sadolin - Fru Lintz
* Paul Hüttel - Poul Steffensen
* Bendt Rothe - Janus Bech
* Birgitte Federspiel - Louise Bech
* Lene Vasegaard - Charlotte Nørlund
* Rasmus Windfelt - Charlotte Nørlunds søn
* Preben Lerdorff Rye - Holger Hjort
* Pia Grønning - Bitten Hjort
* Holger Munk - Direktør Pelving
* Vibeke Juul Bengtsson - Direktør Pelvings sekretær
* Bente Puggaard-Müller - Politibetjent
* Per Årman - Spejderfører

==Sequel==
The film was followed with a sequel, Terror (1977 film)|Terror, in 1977.  All of the major characters returned for this follow-up.

==External links==
* 
* 

 
 
 
 
 
 

 
 