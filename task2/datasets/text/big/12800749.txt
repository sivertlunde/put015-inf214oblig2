Millennium Mambo
{{Infobox film
| name           = Millennium Mambo
| image          = millennium mambo.jpg
| image_size     = 
| caption        = DVD cover
| director       = Hou Hsiao-Hsien
| producer       = Chu Tien-wen Gilles Ciment Eric Heumann Wen-Ying Huang
| writer         = Chu Tien-wen
| narrator       = Shu Qi SHU Qi Jack Kao Chun-hao Tuan Yi-Hsuan Chen	 
| music          = Yoshihiro Hanno Giong Lim
| cinematography = Mark Lee Ping Bin
| editing        = Ching-Song Liao
| distributor    = Palm Pictures (USA)
| released       = October 31, 2001 (France) November 28, 2001 (Belgium) July 21, 2002 (Spain) November 21, 2002 (Netherlands) May 23, 2003 (South Korea)
| runtime        = 119 minutes
| country        = Taiwan Mandarin
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 2001 film.

==Synopsis==
The main character, Vicky, portrayed by actress Shu Qi narrates from 2011 about her life 10 years earlier. She describes her youth and story of her changing life at the beginning of the new millennium. She works as a hostess in a trendy bar.  Vicky is torn between two men, Hao-Hao and Jack, and her journeys display the parallel journey of the psyche and how one girl deals with her fleeting youth.

==Cast==
{| class="wikitable"
|-
! Actor
! Role
|- Qi Shu || Vicky
|-
| Jack Kao || Jack
|-
| Tuan Chun-hao || Hao-Hao
|-
| Chen Yi-Hsuan || Xuan
|-
| Jun Takeuchi || Jun
|-
| Doze Niu || Doze
|-
| Jenny Tseng Yan Lei || 
|- Pauline Chan || 
|-
| Huang Xiao Chu || 
|}

==Awards and nominations==
*2001 Cannes Film Festival   
**Won: Technical Grand Prize (Du-Che Tu for sound design)
**Nominated: Palme dOr

*Chicago International Film Festival
**Won: Silver Hugo (Hou Hsiao-Hsien)

*Ghent International Film Festival
**Won: Best Director (Hou Hsiao-Hsien)
**Nominated: Grand Prix

*Golden Horse Film Festival
**Won: Best Cinematography (Pin Bing Lee)
**Won: Best Original Score (Lim Giong)
**Won: Best Sound Effects (Du-Che Tu)

==References==
 

==External links==
* 
*  
*  

 

 
 
 
 
 


 
 