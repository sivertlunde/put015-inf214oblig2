Bophana: A Cambodian Tragedy
{{Infobox film
| name           = Bophana: A Cambodian Tragedy
| image          = 
| caption        = 
| director       = Rithy Panh
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Jacques Pamart
| editing        = Marie-Christine Rougerie	 	
| distributor    = Catherine Dussart Productions Institut national de laudiovisuel France 3
| released       = 1996
| runtime        = 60 minutes
| country        = Cambodia/France
| language       = 
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1996 Cinema Cambodian Television film|made-for-television docudrama directed by Rithy Panh.

==Synopsis==
Based upon documentation of forced confessions made during the   monk turned Khmer Rouge cadre.
 S21 prison, where they were tortured and forced to make confessions before they were both executed in 1976.

==Bophana, the Audiovisual Resource Center - Cambodia==
Bophana is the namesake of Bophana, the Audiovisual Resource Center - Cambodia, which was co-founded by director Rithy Panh to collect and preserve Cambodias audiovisual heritage.

The name Bophana means "flower" in the Khmer language.

==External links==
* 
*  
* 
* 
* 

 
 
 
 
 
 
 
 
 