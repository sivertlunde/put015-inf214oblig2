Love Without Pity
{{Infobox film
| name           = Love Without Pity
| image          = 
| caption        = 
| director       = Éric Rochant
| producer       = Alain Rocca  
| writer         = Éric Rochant
| starring       = Hippolyte Girardot   Mireille Perrier   Yvan Attal
| music          = Gérard Torikian 
| cinematography = Pierre Novion 
| editing        = Michèle Darmon 	 CNC Christian Bourgois Productions Fetoux France 3 Cinéma Gérard Mital Productions UGC
| released       =  
| runtime        = 84 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

Love Without Pity (French title: Un monde sans pitié) is a 1989 French romantic comedy film written and directed by Éric Rochant. 

== Cast ==
* Hippolyte Girardot as Hippo  
* Mireille Perrier as Nathalie 
* Yvan Attal as Halpern  
* Jean-Marie Rollin as Xavier 
* Cécile Mazan as Francine 
* Aline Still as La mère  
* Paul Pavel as Le père   
* Anne Kessler as Adeline     
* Patrick Blondel as J.F. 
* Paul Bisciglia as Lhomme de LHumanité

==Accolades==
{| class="wikitable plainrowheaders sortable"
|- style="background:#ccc; text-align:center;" Award / Film Festival Category
! Recipients and nominees Result
|- 15th César Awards Best Film
|Love Without Pity
|  
|- Best Actor Hippolyte Girardot
|  
|- Most Promising Actor Yvan Attal
|  
|- Most Promising Actress Mireille Perrier
|  
|- Best Writing
|Éric Rochant
|  
|- Best Music
|Gérard Torikian 
|  
|- Best First Feature Film
|Love Without Pity
|  
|- 3rd European Film Awards European Film European Discovery of the Year
|Love Without Pity
|  
|- Louis Delluc Prize Best Film
|Love Without Pity
|  
|- Venice Film Festival International Critics’ Week FIPRESCI Prize
|Love Without Pity
|  
|-
|Kodak-Cinecritica Award
|Éric Rochant
|  
|-
|}

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 

 
 