Porky's Pooch
{{multiple issues|
 
 
}}

{{Infobox Hollywood cartoon
|cartoon_name=Porkys Pooch
|series=Looney Tunes (Porky Pig) thumb
|caption=Intertitle of the film. Robert Clampett
|story_artist=Warren Foster
|animator=I. Ellis
|layout_artist=
|background_artist=
|voice_actor=Mel Blanc (uncredited)
|musician=Carl W. Stalling
|producer=Leon Schlesinger
|distributor=Warner Bros. Pictures
|release_date=December 27, 1941 (USA)
|color_process=Black-and-white
|runtime=7 minutes
|movie_language=English
}}
Porkys Pooch is a 1941 Warner Bros. cartoon directed by Bob Clampett.

==Plot==
A Scottie dog Sandy is starving. He asks his friend, Rover what hes doing in a car. Rover tells him how he got a master. In a flashback Rover goes up to Porkys apartment room while hes taking a bath. He puts on a towel and answers it. Rover walks in and says proposes to be Porkys pet. Porky does not want him and kicks him out. Rover bangs on the door and Porky gets it. Porky is not impressed with Rovers talents, carries him out of his apartment and drops him off the stair banister. He runs back to his room panting along with Rover near him. Then Rover flicks Porkys snout and dresses up like Carmen Miranda and sings Mi Caballero, by M.K. Jerome and Jack Scholl. Porky just throws him out. Rover walks back in yelling Porky doesnt want him. He becomes sorrowful and walks over to the window. He pretends to jump to end his life. Porky runs over, but he sees Rover lying on the edge. Porky shuts the window while Rover pretends that hes going to fall. But he actually does fall. Porky runs down the stairs and tries to catch him, but unfortunately misses. Porky weeps for Rover and tries to wake him up. Rover finally wakes up glad to see Porky and kisses him. Then he uses the famous catchphrase of Lou Costello, "Im a bad boy!".

==Trivia==
*The same premise was re-worked for Chuck Jones 1947 short Little Orphan Airedale which is ironic since Chuck disliked Bob Clampett.
*The backgrounds in this cartoon include still photos.

==Availability==
Porkys Pooch is available in the   and Looney Tunes Spotlight Collection Vol. 5 DVD sets.

==See also==
*Looney Tunes and Merrie Melodies filmography (1940–1949)
*List of Porky Pig cartoons

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 

 