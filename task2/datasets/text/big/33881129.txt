Bachelor Party (2012 film)
 
 

{{Infobox film
| name=Bachelor Party
| image = Bachelor Party - Amal Neerad.jpg
| caption = Teaser poster
| story = Johnnie To (uncredited)
| director = Amal Neerad
| screenplay = Santhosh Echikkanam Unni. R Indrajith Rahman Rahman Kalabhavan Prithviraj
| producer = Amal Neerad  V. Jayasurya
| cinematography = Amal Neerad
| editing = Vivek Harshan
| music = Rahul Raj
| studio = Amal Neerad Productions
| distributor = August Cinema
| released =  
| runtime = 120 minutes
| country = India
| budget =   Malayalam
}}
 Prithviraj makes Padmapriya and Remya Nambeesan appear in musical item numbers. The films screenplay is by noted authors Unni. R and Santhosh Echikkanam while the original music and background score are composed by Rahul Raj. 

The film released on 15 June 2012, to mixed reviews.      Its plot is adapted from Johnnie Tos Exiled, a 2006 Hong Kong action film.  Though released to mixed reviews, the film was profitable according to Neerad. 

==Plot==
Former mobster Tony (Asif Ali) leads a quiet life with his wife, Neethu (Nithya Menon), and his newborn child in a nondescript villa, having turned over a new leaf. But vengeful underworld don Prakash Kamath (John Vijay) - whom Tony and friends once tried to assassinate - has dispatched a pair of his henchmen to cut that peaceful existence short. Once arrived, gangsters Ayyappan (Kalabhavan Mani) and Fakeer (Vinayakan) find a second pair-Benny (Rahman (actor)|Rahman) and Geevarghese (Indrajith)- determined to protect Tony. After a brief showdown, the whole group comes to an uneasy truce, lay their weapons down and bond over dinner-after all, these men grew up together.

Reunited and searching for a way to save Tony, they visit a fixer called Chettiyar (Ashish Vidyarthi). Chettiyar gives the gang two options – the job of killing a businessman who is Kamaths rival or looting a large quantity of foreign currency being transported. The Gang chooses the first option, and Tony makes them promise that if anything happens to him, his wife and son will be looked after.

As fixed, the gang meets up at a cinema where the target businessman is about to strike a deal. However, Kamath crashes in as the second party of the deal, as Chettiyar had double-crossed the friends. Kamath recognising Ayyappan and others, openly chastises and humiliates Tony, culminating in Tony shooting Kamath. A gunfight erupts in the theatre, with Kamath and Tony being shot. The rival businessman, cornered by Kamaths men, comes to an agreement to share territory and profits, further agreeing to kill the gang of friends.
Having narrowly escaped the theatre shootout, the friends decide to take a severely shot Tony to an underground clinic for medical assistance. After negotiating a price, the doctor operates removing the bullets from Tony. However as he is sewing up Tonys wound, there is a loud banging at the door. Having heard this, the remainder of the waiting friends hide.

The door is answered and Kamath and his men burst in, seeking help for injuries sustained in the theatre shootout. The gang of friends manages to hide the still unconscious Tony, but he wakes up and slowly gets to his feet in a trance before collapsing. The rest of the friends try to get away, but Kamath holds Tony hostage and eventually shoots him. The gang desperately tries to retrieve their critically injured friend and escape. Tony, knowing that he is near death, asks to be taken back to his wife and daughter.

Neethu, broken over her husbands death, demands to know what has happened and in her grief threatens to open fire on others. Neethu buries Tony and leaves the villa with her daughter. The reduced gang, hell-bent on taking revenge on Chettiyar and securing a livelihood for Neethu, leave in search of the currency consignment. After coming across the heavily guarded convoy carrying the notes, however, they come across another gang attacking it. They witness all the security guards being killed, bar one crack-shot (Prithviraj Sukumaran|Prithviraj). The friends decide to help the guard by dispatching the rest of the gang.

The friends, appreciating the guards fighting skills, decide to split the currency with him and drive off to a hidden dock to transport the notes to a safe haven and a new life. Meanwhile, Neethu ends up in the hands of Chettiyar and Kamath, who in turn contact the gang of friends for the money.

Ayyappan is told to meet Kamath at Chettiyars den; otherwise Neethu and her child will be killed. Determined to protect Neethu after Tonys death, the friends agree and leave the guard at the dock with Neethus share of the money, telling him they or she will return by dawn. Once at the meeting place, Kamath agrees to leave Neethu, but tells them Ayyappan must stay to face the consequences of not following orders. Ayyappan agrees to this deal and the remainder of the friends leave with Neethu. However as they leave, Geevarghese informs Neethu of the boat and the guard and tells her to drive there. With Neethu safe the greatly outnumbered friends open fire. In the resulting gunfight, all are killed. As the friends lie dying they all smile knowing they have kept their promise to Tony.

As the credits roll, all dead gangsters meet up in Hell for a sing-and-dance.

==Cast== Indrajith as Geevarghese Rahman as Benny
* Kalabhavan Mani as Ayyappan
* Vinayakan as Fakeer
* Asif Ali as Tony Prithviraj as guard
* Nithya Menen as Neethu
* John Vijay as Prakash Kamath
* Ashish Vidyarthi as Chettiyar Lena as Sheela Mathews Kattuparambil (Cameo)
* Remya Nambeesan (Special Appearance) Padmapriya (special appearance in the song "Kappa Kappa") 
*  Jinu Joseph as Jerry Kalappurakal

==Production==
Amal Neerad says the film was inspired particularly from Sin City, a graphic novel that was turned into a film of the same name.   
The flick might have few boys-will-be-boys, raunchy scenes, but that doesnt mean it only targets the male audience. It has all elements of an entertainer and like its title suggests, it is about a bunch of bachelors having a blast," says the director. 

==Genre==
Amal Neerad combined elements from several different film genres into the film, notably spy, action, thriller, comedy, and musical. The film is also a road movie that takes place over three days. Amal Neerad says the film "could be called an action comedy, but it is tough to include it in a particular genre." He adds that "it is a travelogue that involves the journey of some friends but I wont call it a road movie either." 

==Soundtrack==
{{Infobox album|  
| Name = Bachelor Party
| Longtype =
| Type = Soundtrack
| Artist = Rahul Raj
| Released = 20 May 2012
| Recorded = 2012 Feature film soundtrack
| Length = 17:46 Malayalam
| Label = Mathrubhumi Music
| Producer = Rahul Raj
| Reviews =
| Last album = Oh My Friend (2011)
| This album = Bachelor Party (2012)
| Next album = Kili Poyi (2013)
}}
All music is composed, arranged and programmed by Rahul Raj. The lyrics are penned by Rafeeq Ahammed. The audio was released on 20 May 2012 in a grand function at Kochi. Much before the release of the film; the soundtrack became a sensational superhit across the state and met with high critical acclaim. 

"Britney Spears meets Kathakali!"-Thats what Rahul Raj calls the music of Bachelor Party. "Director Amal Neerad is known for the stylish treatment he brings onscreen. And here I was, wanting to do something new in Mwood. Amal wanted something fresh in the music department and Ive tried my best to bring it in," says Rahul. The singers for Bachelor Party include established names like Shreya Ghoshal, besides "fresh voices for fresh tunes" including Kuttappan master of the Thayillam folk song group, and singer Sunil Mathai, whom the composer calls the "Kailash Kher of Mollywood". Remya Nambeesan also sang one of the songs. 

{{Track listing
| extra_column = Singers
| total_length = 17:46
| title1 = Karmukilil
| extra1 = Shreya Ghoshal, Nikhil Mathew
| length1 = 4:07
| title2 = Vijana Surabhi
| extra2 = Remya Nambeesan, Kalamandalam Kolathapally, K. M. Udayan
| length2 = 4:01
| title3 = Bachelor Life
| extra3 = Sunil Mathai
| length3 = 3:28
| title4 = Kappa Kappa
| extra4 = C. J. Kuttappan, Sunil Mathai, Resmi Sateesh, Sricharan
| length4 = 3:06
| title5 = We Dont Give a Fcuk ("Kick" of Bachelor Party)
| extra5 = Rahul Raj ft. Majestic(Rap)
| length5 = 1:12
}}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 