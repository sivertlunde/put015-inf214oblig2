Coney Island (1917 film)
{{Infobox film
| name = Coney Island
| image = Coney Island (1917 film).jpg
| image_size =
| caption = French theatrical poster to Coney Island
| director = Roscoe Arbuckle
| producer = Joseph M. Schenck
| writer = Roscoe Arbuckle
| narrator = Al St. John
| music = George Peters
| editing = Herbert Warren
| distributor = Paramount Pictures
| released = {{Film date|1917|10|29|ref1= {{cite book
|last=Knopf
|first=Robert
|title=The theater and cinema of Buster Keaton
|url=http://books.google.com/books?id=fU5qDx7tawIC&pg=PA180
|accessdate=21 October 2010
|date=August 2, 1999
|publisher=Princeton University Press
|isbn=978-0-691-00442-6
|page=180}} }}
| runtime = 24 minutes
| country = United States English intertitles
}}
 Roscoe "Fatty" Arbuckle, and starring Arbuckle and Buster Keaton.
 

==Synopsis==
 
The twenty-four minute film follows Arbuckles antics at Coney Island, the New York City amusement park and beach resort, where he sneaks away from his wife to enjoy the attractions, gets a rival for another woman arrested, and disguises himself as a woman. The Keystone Kops are also featured.

Coney Island was filmed before Keaton had fully established his screen persona. Because of this, he employs a wide range of facial expressions, including mugging and laughing, differing drastically from his subsequent unsmiling, but still eloquent, expression.

Arbuckle breaks the fourth wall in one scene where, about to change his clothes, he directly looks at the camera and gestures for it to raise its view above his waist; the camera obligingly does so.

==Cast== Roscoe Fatty Arbuckle - Fatty
* Agnes Neilson - Fattys wife Al St. John - Old friend of Fattys wife
* Buster Keaton - Rival / cop with mustache
* Alice Mann - Pretty girl
* Joe Bordeaux - Sledgehammer Man / Cop (as Joe Bordeau)
* Jimmy Bryant
* Alice Lake

==Production notes== The Witching Waves and Shoot-the-Chutes.

==See also==
* List of American films of 1917
* Fatty Arbuckle filmography
* Buster Keaton filmography

==References==
 

==External links==
*  
*  
*  
*   on YouTube

 
 
 
 
 
 
 
 
 
 
 
 
 
 