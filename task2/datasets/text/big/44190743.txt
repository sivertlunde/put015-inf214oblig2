Sakhavu
{{Infobox film
| name           = Sakhavu
| image          =
| caption        =
| director       = KS Gopalakrishnan
| producer       = Edappazhinji Velappan Nair
| writer         = Kallayam Krishnadas
| screenplay     = Kallayam Krishnadas Anuradha Bheeman Ramu
| music          = V. D. Rajappan
| cinematography =
| editing        = A Sukumaran
| studio         = Chithradesam Productions
| distributor    = Chithradesam Productions
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, Ramu in lead roles. The film had musical score by V. D. Rajappan.   

==Cast==
*Unnimary Anuradha
*Bheeman Raghu Ramu

==Soundtrack==
The music was composed by V. D. Rajappan and lyrics was written by Kallayam Krishnadas and Panthalam Sudhakaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Khalbinte || Alice, Chorus, Madhubhaskar || Kallayam Krishnadas || 
|-
| 2 || Raktham chinthi || Chorus, Madhubhaskar || Panthalam Sudhakaran || 
|-
| 3 || Suralokam || Alice || Panthalam Sudhakaran || 
|}

==References==
 

==External links==
*  

 
 
 

 