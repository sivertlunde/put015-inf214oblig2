Dil Diwana
{{Infobox film
| name           = Dil Diwana
| image          = Dil Diwana.jpg
| image_size     = 
| caption        = 
| director       = Narendra Bedi
| producer       = Ramesh Behl
| writer         =
| narrator       = 
| starring       =
| music          = R.D.Burman
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1974
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1974 Bollywood drama film directed by Narendra Bedi. The film stars .

==Cast== Mumtaz Begum  ...  Nitas mom 
*Jaya Bhaduri ...  Nita 
*Aruna Irani ...  Gita 
*Kamal Kapoor ...  Kapoor 
*Pinchoo Kapoor ...  Manager 
*Randhir Kapoor ...  Vijay 
*Satyendra Kapoor ...  Jamal Khan 
*Kader Khan ...  Advocate 
*Durga Khote ...  Vijays Dadimaa  Komal (Poonam Sinha) ...  Sunita 
*Manmohan ...  Pyarelal 
*Paintal ...  Ratan 
*Rajrani ...  Vijay 
*Ratnamala   
*Sharat Saxena ...  Saxena Saxi (as Sarat Saxena) 
*Tun Tun

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Mujhko Mohabbat Mein"
| Kishore Kumar, Asha Bhosle
|-
| 2
| "Kisise Dosti Karlo"
| Kishore Kumar, Asha Bhosle
|-
| 3
| "Sun Neeta Main Tere"
| Kishore Kumar
|-
| 4
| "Jaa Re Jaa Bewafa"
| Kishore Kumar, Asha Bhosle
|-
| 5
| "Khan Chacha Khan Chacha"
| Kishore Kumar, Asha Bhosle, Manna Dey
|-
| 6
| "Main Ladki Tu Ladka"
| Kishore Kumar, Asha Bhosle
|}
==External links==
*  

 
 
 
 

 
 