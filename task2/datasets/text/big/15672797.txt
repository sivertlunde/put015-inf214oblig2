The Perils of Pauline (1933 serial)
{{Infobox film
 | name = The Perils of Pauline
 | image = The Perils of Pauline VideoCover.jpeg
 | caption = DVD cover Ray Taylor Harmon, Jim, and Glut, Donald F. Great Movie Serials. Florence, Ky.: Psychology Press, 1973, p. 8. 
 | producer = Henry MacRae Ella ONeill George H. Plympton Charles W. Goddard (original story)  Craig Reynolds John Davidson 
 | music =  Richard Fryer
 | editing = Saul A. Goodkind
 | distributor = Universal Pictures
 | released =  
 | runtime = 12 chapters (231 minutes)
 | country = United States
 | language = English
 | budget = 
}} film serial, silent Short short subjects, starred as the heroine, Pauline Hargraves. Historic newsreel footage of the 1930 flight of the Dornier Do X seaplane is featured in chapter eight. 

Each episode in the serial is approximately 20 minutes long. The first one to two minutes of each episode are taken up with a title sequence and an intertitle describing the action from the previous serial. This is followed by about three minutes of the previous serials closing moments.

==Cast==
* Evalyn Knapp as Pauline Hargraves Robert Allen as Robert Warde William Desmond as Professor Thompson (chapters 9 through 12)
* James Durkin as Professor Hargraves John Davidson as Doctor Bashan
* Sonny Ray as Willie Dodge
* Frank Lackteen as Fang Pat OMalley as Tim Sullivan (chapters 5 through 7)
* Adolph Muller as Captain Drake (chapters 2 through 3)

==Plot==
Pauline Hargraves (Evalyn Knapp) is the intrepid daughter of Professor Hargraves (James Durkin), a noted doctor of chemistry and archeology. She and Willie Dodge (Sonny Ray), her fathers cowardly secretary, have accompanied Prof. Hargraves to Asia in search of a legendary ivory disk which may contain the chemical recipe for a deadly gas created by an ancient Egyptian named Confu. Unfortunately, the villainous Asian warlord, Dr. Bashan (John Davidson), is also after the disk, along with his right-hand-man and assassin, Fang (Frank Lackteen).
 shells fall ancient Egyptian temple. The ships captain frees Dr. Bashan (having no reason to imprison him), and Bashan pursues the Hargaves in a faster ship. Bashan catches up to the Hargraves party at night, and Fang attempts to kill Pauline. But a prowling leopard attacks him and he is captured. Fang escapes into alligator-infested waters and is presumed dead—but again escapes to safety. Pauline and Warde discover the Egyptian temple and retrieve the other half of the disk in "Trapped By the Enemy." On their way back to the professor in "The Flaming Tomb", they run into Tim Sullivan (Pat OMalley), an old friend of Wardes who was building a railroad nearby some years ago but stayed behind to live with the natives. Dr. Bashan, accompanied by a band of evil natives, captures everyone but Warde (who is in the jungle with Sullivan, helping a wounded native) and seizes both halves of the disk. Pauline escapes, finds Warde, and rouses a tribe of good natives to help attack the village. Dr. Bashan flees. Pauline saves her father and Dodge from a burning native hut. The Hargraves group tries to fly out of the jungle in Sullivans plane in "Pursued by Savages". The plane has engine trouble, and lands. Pauline goes exploring, and discovers Dr. Bashan (who has been traveling on foot) in the nearby jungle. Pauline steals the disk and tries to take it back to her father. But Prof. Hargraves and Dodge are seized by natives, who also pursue Pauline through the jungle.

Warde and Sullivan save Pauline from a tiger, and Pauline, Dr. Hargraves, and Goode from savages in "Tracked by the Enemy". They fly out in the repaired plane, but learn that the second half of the disk doesnt reveal the formula. Instead, it shows the way to the temple of Imu-Anh, where the remaining half of the disk can be found. The Hargraves party takes a steampship to Singapore, but Bashan takes a plane and catches up to them. One of Bashans female accomplices searches Paulines room for the disk, but is discovered. A fight breaks out between Bashans men and Warde, during which Warde and Pauline tumble into a shark-filled lagoon. Pauline and Warde are rescued by courageous hotel staff in "Dangerous Depths", but Bashan has the second half of the disk. The Hargraves party (followed closely by Bashan) heads for Varanasi|Benares, and travels to the temple by horseback. Another plaque is found directing them to the sarcophagus of Menka-Ra in Egypt. But since that tomb was excavated years ago, the team must head to New York City where the sarcophagus is on display in a museum. Pauline hears a moaning sound, and investigates. She and Warde are seized by Bashan, but escape into a side room and lock the door. Bashan decides to take a plane to New York City to avoid being imprisoned by the local people. Unable to open the door again, Pauline and Warde try another way out—only to have the floor collapse beneath them. They fall into an underground river. In "The Mummy Walks", they are rescued by the temple guards. The two groups unwittingly take the same seaplane to New York City. Professor Thompson (William Desmond) meets the Hargrave group, and that night Hargraves, Thompson, Pauline and Warde go to the museum. Bashan has gotten there first, and one of his accomplices masquerades as a guard. Pauline is almost kidnapped. But when Willie Dodge appears (having fallen into a vat of wet plaster), everyone believes hes a mummy. Bashan flees. Prof. Hargraves discovers a vase with the next inscription on it (although he wonders why it was a vase and not the sarcophagus). He gives the vase to Pauline for safekeeping. While Warde struggles with the guard, she attempts to flee the museum with Fang close behind her. But she trips on the stairs, and a powerful ancient explosive in the vase goes off when she drops it.

The audience learns in "The Night Attacks" that Pauline was not injured in the blast, and that another half-disk was in the vase. Everyone flees the museum before the police arrive. Prof. Hargraves writes down the entire formula the next day, and Thompson and Hargraves discuss the formula as Fang listens from outside the house. That night, as Hargraves and Thompson work on recreating the formual in a laboratory, Bashan and Fang sneak into the Thompson home and kidnap Pauline (who has the formula on her person). Hargraves, Warde, and Thompson arrive just in time to save her in "Into the Flames". The next day, Hargraves compounds a large amount of the formula. While Hargraves, Thompson, and Goode are out, Bashan and Fang break into the lab and try to seize the formula. A fire breaks out in the lab, and the secret formula goes off—exploding and spreading more fire. Bashan and Fang flee, leaving Pauline and Warde trapped in the labor (which is on the top floor of a skyscraper).

The serial ends with "Confus Sacred Secret". Pauline and Warde leap into the river adjacent to the building to escape the flames. Hargraves tries to recreate the formula at the laboratory in Prof. Thompsons home. Bashan, learning Pauline is alive and the disks safe, breaks into the home with a band of thugs in broad daylight. While Warde, Hargraves, Thompson, and Thompsons guards hold off the men, Bashan and Fang enter the lab. When they are discovered, Bashan tells Fang to shoot anyone who comes through the door. He tosses his pistol to Fang, who drops it. It falls on the floor and goes off—breaking the container holding the secret gas. The gas disintegrates both Bashan and Fang.
 fades out.

===Chapter titles===
The Perils of Pauline contained 12 chapters. Their titles are: {{Cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 208–209
 | chapter = Filmography
 }} 

# The Guns of Doom
# The Typhoon of Terror
# The Leopard Leaps
# Trapped by the Enemy
# The Flaming Tomb
# Pursued by Savages
# Tracked by the Enemy
# Dangerous Depths
# The Mummy Walks
# The Night Attacks
# Into the Flames
# Confus Sacred Secret

==Production==
The storyline of the 1914 The Perils of Pauline involved a young woman who inherited a million dollars from a distant relative. Her relatives personal secretary was named guardian of the funds, and would inherit if she died before she remarried. Determined to enjoy some adventures before marrying, Pauline sets off around the world—only to have her guardian, Mr. Koerner, attempt to murder her several times.  The 1914 serial was so successful that Pathé followed it up with a very similar serial, The Exploits of Elaine.

In September 1930, Pathé said it would remake The Perils of Pauline.  But no action was taken. Universal announced it would remake the serial in July 1932.  It purchased the talking picture rights from Pathé, and the media speculated that Lucile Browne would take the title role.  In March 1933, the studio said production would begin in July.  The studio was actively searching for a female lead in May to develop into a star who could appear in a large number of serials. 

Filming began on the serial in the middle of August 1933. "Hollywood in Review." New York Times. August 20, 1933.  Universal announced that Ray Taylor was directing, and Evalyn Knapp was starring.  The original screenplay had each episode set in a different country,  although this clearly changed by the time the film was finished. The young actor Hugh Enfield was originally cast in the role of Robert Warde. 
 New York Times said in 1936 that Universal usually paid $3,000 to $17,000 for a good character, gave its stars $1,000 to $5,000 per week, spent six to eight weeks shooting each serial, and spent $165,000 to $250,000 per serial. 

The first episode of the serial was released on November 6, 1933.

==See also==
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
*  
*  

 
{{Succession box Universal Serial Serial
| before=Gordon of Ghost City (1933)
| years=The Perils of Pauline (1934)
| after=Pirate Treasure (1934)}}
 

 
 

 
 
 
 
 
 
 
 
 
 