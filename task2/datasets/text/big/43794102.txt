The Big Game (1936 film)
{{Infobox film
| name           = The Big Game
| image          = 
| alt            =
| caption        = 
| director       = George Nicholls, Jr. Edward Killy (football scenes) Doran Cox (assistant)
| producer       = Pandro S. Berman
| writer         = Irwin Shaw
| based on       =  
| starring       = Philip Huston James Gleason June Travis Bruce Cabot Andy Devine
| music          = Nathaniel Shilkret
| cinematography = Harry Wild
| editing        = Frederic Knudtson RKO Radio Pictures
| distributor    = 
| released       =   }}
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
The Big Game is a 1936 American sports drama film directed by George Nicholls, Jr. and produced by RKO Radio Pictures, which released the film on October 9, 1936.  The screenplay was written by Irwin Shaw, adapted from the 1936 novel of the same name by Francis Wallace.  The film stars Philip Huston, James Gleason, June Travis, Bruce Cabot and Andy Devine.

==Plot==
Clark Jenkins is the star quarterback of Atlantics college football team. He falls in love with classmate Margaret Anthony, whose father, Brad, is a newspaper sports columnist who disapproves of their romance.

A gambler and school booster, George Scott, has been discreetly giving money to Clark, as he has in the past for players like Pop, who couldnt have afforded to go to college otherwise. Clarks roommate and teammate, Cal Calhoun, snitches on him to Brad Anthony, who investigates and falsely concludes that Clark intends to deliberately lose a game for a payoff from gambling kingpin Blackie Dawson.

No such arrangement exists. But with the big game against Erie coming up, Blackie kidnaps Clark to make sure Atlantic cant win. Pop creates a distraction on the field to delay the proceedings while Margaret, George and an apologetic Cal rush to rescue Clark in time to play in the game.
 
==Cast==
* Philip Huston as Clark Jenkins
* James Gleason as George Scott
* June Travis as Margaret Anthony
* Bruce Cabot as Cal Calhoun
* Andy Devine as Pop
* C. Henry Gordon as Brad Anthony
* John Harrington as Blackie

==References==
 

 
 
 
 
 
 
 


 
 