Doctor Glas (film)
 
{{Infobox film
| name           = Doctor Glas
| image	         = Doctor Glas FilmPoster.jpeg
| caption        = Film poster
| director       = Mai Zetterling
| producer       = Joseph Hardy Benni Korzen Mogens Skot-Hansen David Hughes Hjalmar Söderberg Mai Zetterling
| starring       = Per Oscarsson
| music          = 
| cinematography = Rune Ericson
| editing        = Wic Kjellin
| distributor    = 
| released       =  
| runtime        = 83 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}
 same name. It was listed to compete at the 1968 Cannes Film Festival,    but the festival was cancelled due to the events of May 1968 in France.

==Cast==
* Per Oscarsson as Dr. Glas
* Lone Hertz as Helga Gregorius
* Ulf Palme as Rev. Gregorius, Helgas husband
* Bente Dessau as Eva Martens
* Nils Eklund as Markel, journalist
* Lars Lunøe as Klas Recke, Helgas lover
* Berndt Rothe as Birck
* Helle Hertz as Anita
* Ingolf David as Dr. Glas father Jonas Bergström as A university friend of Dr. Glas
* Per Goldschmidt
* Helge Scheuer
* Annemette Svendsen

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 