Peyton Place (film)
{{Infobox film
| name           = Peyton Place
| image          = Peyton_place.jpg Theatrical release poster
| image_size     = 220px
| director       = Mark Robson
| producer       = Jerry Wald
| screenplay     = John Michael Hayes
| based on       =   Lee Philips Arthur Kennedy Terry Moore
| music          = Edward B. Powell
| cinematography = William C. Mellor
| editing        = David Bretherton
| studio         = Jerry Wald Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 162 minutes
| country        = United States
| language       = English
| budget = $2.2 million  or $1.8 million HOLLYWOOD SCENE: Jerry Wald Presents His Treasurers Report -- Blausteins Horsemen
By THOMAS M PRYORHOLLYWOOD.. New York Times (1923-Current file)   27 July 1958: X5.  
| gross          = $25.6 million
}}
 1956 novel of the same name by Grace Metalious.
 moral hypocrisy Lee Philips, Lloyd Nolan, and Diane Varsi.

==Plot== Arthur Kennedy) stumbles out of his house as his son leaves town. Lucas downtrodden wife, Nellie (Betty Field) goes to work as the housekeeper for Constance "Connie" MacKenzie (Lana Turner), a local dress shop owner. The daughters of the two families, Allison MacKenzie (Diane Varsi) and Selena Cross (Hope Lange) are best friends and are about to graduate high school.
 Lee Philips), Leon Ames), Terry Moore) to the party, due to Bettys overt sexual conversations; ultimately, Constance reconsiders and allows Allison to invite anyone to her party. Betty arrives at the party with Rodney Harrington (Barry Coe), who turns off the lights and kisses Allison; the party is ended, however, when Connie walks in, embarrassing Allison by making a scene.

Later that week, Rossi arrives at the MacKenzie house to announce that Allison has been named valedictorian, and he asks Connie to chaperone Allisons graduation dance. Meanwhile, Harrington tells his son, Rodney, that he will not accept him going to the graduation dance with a girl with such a bad reputation, and forces Rodney to call Betty and uninvite her to the dance. Instead, Rodney goes with Allison, though Allison is in love with another classmate, Norman (Russ Tamblyn). When they get to the dance, Rodney splits off to make-out with Betty in his car, but she is angry at him for dumping her and refuses to have sex with him. Outside, after dancing with her, Rossi kisses Connie, but she again rejects his advances. Selena returns home, and is raped by Lucas.

Selena becomes pregnant, and when she goes to Dr. Swain (Lloyd Nolan) for an abortion, he refuses, and she confides in him that her step-father raped her. Swain confronts Lucas, and he is forced by Swain to sign a confession and leave town. Lucas chases Selena home out of revenge, and she trips and falls, causing her to miscarry. Swain operates on her and tells her family it was an appendectomy instead of the true reason. 

At a picnic for Labor Day 1941, Rodney and Betty reunite and go skinny dipping while Allison and Norman go swimming nearby. A town busybody sees and tells Connie, who explodes at Allison for causing rumors. They fight, and Connie tells her that Allisons father was married to another woman, and was not a great man like Connie had told her. Allison runs upstairs and finds that Nellie Cross has committed suicide by hanging herself in a closet.  This shocks Allison, and she is confined to a bed for a time, until she decides to leave Peyton Place for New York City.

World War II erupts and the men of Peyton Place go off to war. However, when Rodney is killed in action, his father offers to take care of his now-wife Betty, and she is welcomed into the family. Meanwhile, Connie visits Rossi to apologize for being so dismissive of him, and when she confesses that she was a married mans mistress, Rossi decides to stay in Peyton Place and promises to always care for her.

Years later, Lucas tries to rape Selena again, but this time she kills him in self-defense. She is then arrested and tried by the District Attorney (Lorne Greene) for murder: the truth about Selinas self-defense actions and other ongoings then come to light, and she is acquitted.

==Cast==
  
* Lana Turner - Constance MacKenzie
* Diane Varsi - Allison MacKenzie
* Hope Lange - Selena Cross Lee Philips - Michael Rossi Arthur Kennedy - Lucas Cross
* Lloyd Nolan - Dr. Matthew Swain
* Russ Tamblyn - Norman Page Terry Moore - Betty Anderson
  David Nelson - Ted Carter
* Barry Coe - Rodney Harrington
* Betty Field - Nellie Cross
* Mildred Dunnock - Miss Elsie Thornton Leon Ames - Leslie Harrington
* Lorne Greene - District Attorney
* Staats Cotsworth - Charles Partridge
* Peg Hillias - Marion Partridge
* Scotty Morrow - Joey Cross (uncredited)
 

Cast notes
*Both Diane Varsi and Lee Philips made their film debuts in Peyton Place.  Harriet and Ricky Nelson|Ricky. 

==Production== Hays Code, and his suggestion Pat Boone be cast as Norman Page, she returned to her home to Gilmanton, New Hampshire. She hated the film, but she eventually earned a total of $400,000 in exhibition profits from it. Kashner and McNair, pp.248-51   
      
The film was shot primarily in   in New York. 

==Reception==
The film premiered in Camden two days before going into general release in the US on December 13, 1957.
 Cheryl killed Imitation of Life. 

(According to Wald the film earned $10.1 million in 4,185 theatres. 
 popular prime time television series that aired from September 1964 until June 1969.

== Gallery ==
 
File: Peyton_Place_0.JPG
File:Peyton_Place_3-3.jpg|
Image:Peyton_Place_6.JPG|
Image:Peyton_Place_5-5.jpg|
Image:Peyton_Place_7.JPG|
Image:Peyton_Place_4-4.jpg|
File:Peyton_Place_1.JPG|
Image:Peyton_Place_2-2-2.jpg|
Image:Peyton_Place_000.JPG |
Image:Peyton_Place_11.JPG|
 

==Critical reception== Catholic Legion of Decency, meaning it was deemed acceptable to all.  
But in actually, it was given an "A-III" rating, meaning appropriate only for adults. )

On the films 40th anniversary in 1998, celebrations were held in some of the Maine towns in which the film was shot, attended by Hope Lange.
 Academy Awards== Tom Jones, The Little The Turning The Color Purple in 1985, both of which won zero of eleven nominations.
 Best Motion Picture: Jerry Wald, Producer Best Director: Mark Robson Best Actress: Lana Turner Best Supporting Actress: Diane Varsi
* Best Supporting Actress: Hope Lange Best Supporting Actor: Arthur Kennedy
* Best Supporting Actor: Russ Tamblyn
*  
*  

==See also== Illegitimacy in fiction

==References==
Notes
 

Bibliography
*Kashner, Sam and MacNair, Jennifer. The Bad and the Beautiful: Hollywood in the Fifties W.W. Norton & Company, Inc., 2002., ISBN 0-393-04321-5

==External links==
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 