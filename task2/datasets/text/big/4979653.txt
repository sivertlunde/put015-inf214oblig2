New York Doll
 
{{Infobox Film|
  name     = New York Doll|
  image          = Nyd-poster.jpg|
  director       = Greg Whiteley|
  producer       = Ed Cunningham,  Seth Gordon|
  writer         = |
  starring       = Arthur Kane,  David Johansen,  Sylvain Sylvain|
  distributor    = First Independent Pictures Vivendi Entertainment|
  released   = 2005|
  runtime        = 75 min. |
  language = English |
    music          = Brett Boyett|
  awards         = |
  budget         = | 
  tagline        = one man. two journeys.|
}}
 documentary based on the life of former New York Dolls member Arthur Kane. It was nominated for both a Satellite Award and a Grand Jury Prize at the Sundance Film Festival, where it premiered in 2005.

==Summary==

The film details the history of both the New York Dolls and one of its members, Arthur "Killer" Kane. The film narrates the history of the band from its formation in 1972, through its drug problems and the deaths of several of its members. The central focus of the film, however, is Arthurs life after conversion to The Church of Jesus Christ of Latter-day Saints after struggles with alcoholism, drug abuse, an attempt at suicide, and a beating with a baseball bat that, contrary to popular belief, did not happen during the Rodney King riots, but left him near death, and needing a long recovery.     

As Kane struggles with both loneliness and poverty, he discovers that he will once again have a chance to perform with the Dolls in London. After overcoming his differences with former band member David Johansen, Kane has a successful performance. Upon his return to Los Angeles, Kane contemplates touring with the reunited Dolls, but plans are cut short by his unexpected death from leukemia.

The film features interviews with Morrissey, who arranged the reunion show, as well as other artists such as Chrissie Hynde and Bob Geldof.

New York Doll was shot on digital camcorders with an aspect ratio of 1.78:1.     

It scored a 90% on Rotten Tomatoes out of 51 reviews. 

==References==
 

==External links==
*  
* 
* 

 
 
 
 
 
 


 
 