Bigger, Stronger, Faster*
{{Infobox film
| name           = Bigger, Stronger, Faster*
| image          = Bigger stronger faster ver5.jpg
| caption        = Theatrical release poster
| director       = Christopher Bell
| producer       = Alexander Buono Jim Czarnecki Kurt Engfehr Tamsin Rawady
| executive producer = Rob Weiser Richard Schiffrin
| writer         = Christopher Bell Alexander Buono Tamsin Rawady
| starring       =
| music          = Dave Porter
| cinematography = Alexander Buono
| editing        = Brian Singbiel Madman Films
| released       =
| runtime        = 105 min
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Bigger, Stronger, Faster* is a 2008 documentary film directed by Christopher Bell, about the use of anabolic steroids as performance-enhancing drugs in the United States and how this practice relates to the American Dream. The film had its world premiere    on January 19, 2008 at the 2008 Sundance Film Festival.  The film was shown at the Tribeca Film Festival in April 2008, and opened in limited release in the United States on May 30, 2008. 

==Title==
The * in the title refers to how athletes who are implicated in using  , "Better, Stronger, Faster."

==Synopsis== Smelly and Mad Dog,  who all grew up idolizing Arnold Schwarzenegger, Hulk Hogan, and Sylvester Stallone, and also features professional athletes, medical experts, fitness center members, and US Congressmen talking about the issue of anabolic steroids. 

Beyond the basic issue of anabolic steroid use, Bigger, Stronger, Faster* examines the lack of consistency in how the US views drugs, cheating, and the lengths people go to achieve success. The film looks beyond the steroid issue to such topics as Tiger Woods laser eye correction to 20/15 vision, professional musicians use of blood pressure reducing drugs, or athletes dependence on cortisone shots, which are a legal steroid. It takes a skeptical view of the health risks of steroids and is critical of the legal health supplement industry.

==Reception==
The film received highly positive reviews from critics. It received three out of four stars in People magazine and was marked as a Critics Choice pick (63/65 Fresh).  Metacritic reported the film had an average score of 80 out of 100, based on 17 reviews.   

Stephen Holden of The New York Times released a positive review shortly before the films release, noting that it takes a look at steroid use from numerous perspectives and that " lthough the movie doesnt defend steroid use, neither does it go on the attack." Holden said that the film "left   convinced that the steroid scandals will abate as the drugs are reluctantly accepted as inevitable products of a continuing revolution in biotechnology. Replaceable body parts, plastic surgery, anti-depressants, Viagra and steroids are just a few of the technological advancements in a never-ending drive to make the species superhuman."   

Roger Ebert gave the film 3.5/4 stars, saying that it is "remarkable in that it seems to be interested only in facts."   

==Aftermath== Mike Bell, brother of director Chris Bell, who was prominently featured in the film, died at 37. Christian Boeving whose appearance in the film included the admission of steroid use, was later fired by his sponsor, MuscleTech. 

==DVD release==
The DVD version of the film was released on September 30, 2008.

==References==
 

==External links==
* 
* 
* 
* 
* 
*  at sundance.org
*  in Variety

 
 
 
 
 
 
 
 