A Decade Under the Influence (film)
{{Infobox film
| name           = A Decade Under the Influence
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Ted Demme Richard LaGravenese
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 138 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
A Decade Under the Influence is a 2003 American documentary film, directed by Ted Demme and Richard LaGravenese. It was produced by Independent Film Channel.    Its about the "turning point" in American cinema in the 1970s.

==Cast==
* Robert Altman
* John G. Avildsen
* Warren Beatty (archive footage)
* Linda Blair (archive footage)
* Peter Bogdanovich
* Peter Boyle (archive footage)
* Marshall Brickman
* Ellen Burstyn
* John Calley
* Jimmy Carter (archive footage)
* John Cassavetes (archive footage)
* Julie Christie
* Clint Eastwood
* Peter Fonda (archive footage)
* Francis Ford Coppola
* Milos Forman
* Roger Corman
* Bruce Dern
* William Friedkin
* Pam Grier
* Monte Hellman
* Dennis Hopper
* Sidney Lumet
* Paul Mazursky
* Polly Plat
* Roy Scheider
* Sydney Pollack
* Jerry Schatzberg
* Paul Schrader
* Martin Scorsese
* Sissy Spacek
* Robert Towne
* Jon Voight


==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 

 