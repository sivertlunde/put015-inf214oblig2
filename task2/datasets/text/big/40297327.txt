Friends from France
 
{{Infobox film
| name           = Friends from France
| image          = 
| caption        = 
| director       = Anne Weil Philippe Kotlarski
| producer       = Laetitia Gonzalez
| writer         = Anne Weil Philippe Kotlarski
| starring       = Dmitry Brauer
| music          = 
| cinematography = Frédéric Serve
| editing        = 
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = France
| language       = French
| budget         = 
}}

Friends from France ( ) is a 2013 French drama film written and directed by Anne Weil and Philippe Kotlarski. It was screened in the Contemporary World Cinema section at the 2013 Toronto International Film Festival.       The film is set in Odessa in 1979. 

==Cast==
* Dmitry Brauer as KGB Agent
* Ania Bukstein as Vera
* Alexandre Chacon as David
* Jérémie Lippmann as Jérôme
* Alexander Senderovich as Nathan

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 