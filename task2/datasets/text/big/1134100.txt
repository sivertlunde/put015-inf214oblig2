Hell Divers
{{Infobox film
| name           = Hell Divers
| image          = Poster - Hell Divers 01.jpg
| image_size     =220px
| caption        = Theatrical release poster
| director       = George W. Hill (uncredited)
| producer       = George W. Hill
| writer         = Frank Wead Harvey Gates Malcolm Stuart Boylan
| narrator       =
| starring       = Wallace Beery Clark Gable
| music          =
| cinematography = Harold Wenstrom Charles A. Marshall
| editing        = Blanche Sewell
| studio         =  George Hill Productions
| distributor    = Metro-Goldwyn-Mayer (MGM)
| released       = January 16, 1932
| runtime        = 109 min
| country        = United States English
| budget         = $821,000 "The Eddie Mannix Ledger". Margaret Herrick Library, Center for Motion Picture Study, Los Angeles.  gross = $2,161,000 
}} Curtiss F8C-4 Helldiver dive bombers after which the movie was named. Beck, Sanderson.   Movie Mirrors, 2000. Retrieved: December 27, 2011.   
 Dorothy Jordan, Robert Young appears near the end of the film in a speaking role as Graham, a pilot. 

==Plot== Chief Petty Helldiver dive Fighting Squadron One, about to go to Panama aboard the   aircraft carrier. He loses his five-year title of "champion machine gunner" after young C.P.O. Steve Nelson (Gable) joins the squadron. Windy, notorious for using his fists to enforce discipline, is charged by local police with wrecking a Turkish bath. Windy is saved from arrest, however, when Lieutenant Commander Jack Griffin ( Miljan), skipper of the squadron, intervenes on his behalf. Griffin and his second-in-command, Lieutenant "Duke" Johnson (Nagle), agree that Nelson is the best candidate to replace Windy as he ponders retirement.

The chiefs engage in friendly rivalry until the squadron practices a new dive-bombing technique and Steve becomes a hero, saving the base from being accidentally bombed by climbing out on the wing of his dive bomber to hold in place a bomb that failed to release. Feelings turn bitter when Steve contradicts Windys explanation of the accident and Windy punches him in resentment. Windy is dressed down by Duke when the officer sees the punch. When Steves sweetheart, Ann Mitchell (Jordan), visits him, he proposes marriage to her, but Windy uses a practical joke to get even with Steve. Unaware that Ann is Steves fiancee and not simply a girl he is trying to impress, Windy bribes an old acquaintance, Lulu (Prevost), to pretend to be Steves outraged lover.  Ann leaves upset and will not listen to Steves denials.
 liberty and keeping him from seeing Mame Kelsey (Rambeau), the woman in Panama he wants to settle down with after retirement.
 bails Windy rate from chief. Windy is disciplined at "Captains Mast" and reduced to Aviation Machinists Mate 1st Class for leaving his post without authorization, absent without leave, and missing ship. Steve reluctantly becomes leading chief.

During a mock battle, Steves aircraft crashes near a rocky island, killing the pilot and leaving Steve with a broken leg. Duke and Windy land to rescue Steve, but Duke suffers a head injury and Windy has to save both. Steve and Windy become friends while waiting in the fog to be found. Windy writes Ann a note confessing that he used Lulu as a joke to frame Steve.  After four days, Dukes condition worsens and Steve develops blood poisoning. Windy tries to save them by flying them out in Dukes dive bomber, with Duke in the rear cockpit and Steve riding on the wing. Despite the fog, they find the aircraft carrier, but his aircraft crashes during landing, fatally trapping Windy in the burning wreckage. At his last request, Windy is buried at sea as a missing man formation flies overhead.

==Cast==
As appearing in Hell Divers, (main roles and screen credits identified):   Turner Classic Movies. Retrieved: December 21, 2011. 
 
*Wallace Beery as C.P.O. H.W. "Windy" Riker
*Clark Gable as C.P.O. Steve Nelson
*Conrad Nagel as Lieutenant D.W. "Duke" Johnson Dorothy Jordan as Ann Mitchell
*Marjorie Rambeau as Mame Kelsey
*Marie Prevost as Lulu Farnsworth
*Cliff Edwards as "Baldy"
*John Miljan as Lieutenant Commander Jack Griffin
*Landers Stevens as Admiral
*Reed Howes as Lieutenant Fisher
*Alan Roscoe as Captain, U.S.S. Saratoga Frank Conroy as Chaplain
 
 
 , 1931.]]

==Production== What Price Glory? (1926), "Lt. Comdr. Frank Wead, U.S.N. (Ret.)" is credited for the films story.   Wead was himself later portrayed by John Wayne in John Fords movie biography The Wings of Eagles, in which footage of Hell Divers appears.   Ford regular Jack Pennick has a small role in both, appearing uncredited in Hell Divers as a recruit sailor. 
 North Island Naval Air Station in San Diego, California.  The aircraft used in the film were the Curtiss F8C-4 variant, the first production variant to bear the nickname "Helldiver." Farmer 1989, p. 26.  A total of 25 F8C-4 aircraft were transferred to the U.S. Marine Corps shortly after the release of Hell Divers.  The production received full cooperation from the US Navy Department, not only featuring the song (uncredited but considered the unofficial song of the US Navy), "Anchors Aweigh" (1906) (written by Charles A. Zimmerman, lyrics by Alfred Hart Miles and R. Lovell) in the opening credits but also a dedication to the naval aviators who made the film possible. 
 USS Los Angeles aboard the carrier was also incorporated into the story.   In addition to sequences filmed aboard the Saratoga at sea and in the Panama Canal, Hell Divers was filmed at the NAS North Island, as one of the first of a series of naval epics filmed there. 

==Reception==
Hell Divers was received well both critically and with audiences. Reviews focused on the exciting aerial sequences. Mordaunt Hall, The New York Times reviewer effused over the "magnificently photographed production, one that includes naval air stunts and impressive landing feats," while dismissing the plot as "inconsequential." Hall, Mordaunt.   The New York Times, December 23, 1931.  More recent reviews note the "boisterous" interplay between notorious scene-stealer Beery and Gable,  while, for aircraft enthusiasts, the film is considered an aviation classic. 

===Box Office===
According to MGM records, Hell Divers earned $1,244,000 in the U.S. and Canada and $917,000 elsewhere, resulting in a profit of $458,000. 

==References==
Notes
 

Citations
 

Bibliography
 
* Dolan, Edward F. Jr. Hollywood Goes to War. London: Bison Books, 1985. ISBN 0-86124-229-7.
* Donald, David, ed. Encyclopedia of World Aircraft. Etobicoke, Ontario: Prospero Books, 1997. ISBN 1-85605-375-X.
* Farmer, Jim. "Hollywood goes to North Island NAS." The Making of the Great Aviation Films, General Aviation Series, Volume 2, 1989.
* Hardwick, Jack and Ed Schnepf. "A Viewers Guide to Aviation Movies". The Making of the Great Aviation Films, General Aviation Series, Volume 2, 1989.
* Maltin, Leonard. Leonard Maltins Movie Guide 2009. New York: New American Library, 2009 (originally published as TV Movies, then Leonard Maltin’s Movie & Video Guide), First edition 1969, published annually since 1988. ISBN 978-0-451-22468-2.
* Orriss, Bruce. When Hollywood Ruled the Skies: The Aviation Film Classics of World War II. Hawthorne, California: Aero Associates Inc., 1984. ISBN 0-9613088-0-X.
 

==External links==
*  
*  
*  
*  

 
  

 
 
 
 
 
 
 
 
 
 