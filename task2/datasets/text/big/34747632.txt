Bouka (film)
{{Infobox film
| name           = Bouka
| director       = Roger Gnoan MBala
| producer       = Abyssa Films
| writer         = Roger Gnoan MBala
| starring       = Akissi Delta Félix Gazekagnon Drissa Kone
| music          = Martial Anney
| sound editor   = Yao Johnson
| photography    = Paul Kodjo
| editing        = Ahoussy Djangoye
| distributor    = Marfilmes
| released       =  
| runtime        = 90 minutes
| country        = Ivory Coast
| language       = French
}}
 1988 drama film directed by Roger Gnoan MBala.

==Synopsis==
Bouka is a gifted young teenager. He lives with his parents in a village and forms with them a solid family. His father gives him a traditional education close to the  nature. Unfortunately this happiness will be troubled by the brutal death of the father… Remained widow Boukas mother will suffer the consequences of a relentless traditional principle. She becomes the new wife of the nephew of her late husband, Bouka doesn’t accepts this new condition of her mother. He suspects his stepfather to be involved in his fathers death. He stops to go to school and organizes a gang in the forest. In this tormented atmosphere, he develops a mortal hate towards its new "father"…
According to MBissine Diop, "Bouka" is the first film where Roger Gnoan MBala deals with the issue of the Western influence on the local culture. 

==Awards==
* 1st Prize at Vues d’Afrique, Canada (1989) 
* 1st Prize at Festival de Belfort, France (1989) 
* Sankofa Prize at FESPACO - Panafrican Film and Television Festival of Ouagadougo, Burkina Faso (1989)
* Prize ID des Arts et Lettres, Ivory Coast (1989)
* Public Prize at Festival d’Angers, France (1989)

==See also==
*  
* 

==External links==
*  - IMDb page about Bouka
*  at Africultures.
*  in Film lAfrik

==References==
 

 
 
 

 
 