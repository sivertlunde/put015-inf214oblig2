A Life Without Pain
{{Infobox Film name           = A Life Without Pain image          = caption        = director       = Melody Gilbert producer       = Melody Gilbert music          = cinematography = Melody Gilbert editing        = Charlie Gerszewski distributor    = released       = 2005 runtime        = language  English
|budget         = preceded_by    = followed_by    = mpaa_rating    = tv_rating      =
}}

A Life Without Pain is a documentary film by Melody Gilbert about children who cant feel pain. 

==Plot synopsis==
The film explores the daily lives of three children with Congenital insensitivity to pain, a rare genetic disorder shared by just a hundred people in the world.  Three-year-old Gabby from Minnesota, 7-year-old Miriam from Norway and 10-year-old Jamilah from Germany have to be carefully guarded by their parents so they dont suffer serious, life-altering injuries.

==Reception==
The release of the film garnered widespread interest in the topic, and the character Gabby was featured on The Oprah Winfrey Show in 2006. 

==References==
 

==External links==
* 
*  

 
 
 
 

 