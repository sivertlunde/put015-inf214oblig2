The Mark (2012 film)
 
{{Infobox film
| name           = The Mark
| image          =  
| image_size     =
| caption        =
| director       = James Chankin
| producer       = James Chankin
| writer         =
| narrator       =
| starring       = Craig Sheffer, Gary Daniels, Sonia Couling, Ivan Kamaras and Eric Roberts 
| music          = Edwin Wendler 
| cinematography =
| editing        = Vance Null
| distributor    = Pure Flix Entertainment
| released       =  
| runtime        = 1hr 34min
| country        = United States
| language       = English
| budget         = $1,500,000
}}

The Mark is a 2012 Christian film about the Rapture, starring Craig Sheffer. 

==Plot==
Chad Turner (Craig Sheffer) is implanted with a biometric computer chip (the Mark of the Beast). The Rapture occurs, and Joseph Pike (Gary Daniels) searches for Turner in hopes of gaining control of the Mark of the Beast. Cooper (Eric Roberts), the head of security of Avanti, the company that created the chip, is held hostage by Pike in order to locate Turner. Chad Turner must stay alive against all odds and keep the chip from falling into the wrong hands. As the story moves forward, Chad becomes more still within his faith and understands the nature of the situation. Just then the rapture happens and Chad decides to escape with his friend, off the plane. They make it out but discover that the tribulation has begun.

==Cast==
* Craig Sheffer as Chad Turner
* Gary Daniels as Joseph Pike
* Eric Roberts as Cooper
* Sonia Couling as Dao, flight attendant

==Sequel==
A Sequel titled   was released in 2013, taking place right after the ending of the first film. The plot follows the main cast from the first film right after the events of the last film, just as the Antichrist rises to power. The film features all of the original cast reprising their roles. The ending of the film, hints for a sequel, but none has been confirmed as of yet.

==References==
 

==External links==
*  

 
 
 

 