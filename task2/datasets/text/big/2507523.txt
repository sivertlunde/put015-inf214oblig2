Humanity and Paper Balloons
{{Infobox film
| name = Humanity and Paper Balloons
| image = Humanity and Paper Balloons poster.jpg
| caption = Theatrical release poster.
| director = Sadao Yamanaka
| producer = Toho
| writer = Shintarō Mimura
| starring = Chojuro Kawarazaki Kanemon Nakamura Shizue Yamagishi
| distributor = Toho
| music = Tadashi Ota
| cinematography = Akira Mimura
| released =  
| runtime = 86 minutes
| country = Japan
| language = Japanese
}}
 1937 black-and-white film directed by Sadao Yamanaka, his last film.

==Plot==
The story is set in the 18th century, and dramatically depicts the struggles and schemes of Unno, a ronin, or masterless samurai in feudal Japan, as well as those of his similarly impoverished neighbours.

==Cast==
* Chojuro Kawarazaki
* Kanemon Nakamura
* Shizue Yamagishi
* Noboru Kiritachi
* Tsurozo Nakamura
* Choemon Bando
* Suzeko Suketakaya
* Emitaro Ichikawa

==Reception==
Largely unknown outside of Japan for years, the film has been hailed by critics such as Tadao Sato and Donald Richie, and a number of other Japanese filmmakers, including Akira Kurosawa among them, as one of the most influential examples of jidaigeki, or Japanese period films. 

Jasper Sharp of Midnight Eye described the film as "a fascinating time capsule of a movie that not only reframes the feudal period in which it is set to present a harsh critique of the social and political conditions of the time it was made, but also demonstrates just how tight, coherent, and entertaining films from this period actually were."  In 2012, Spanish film programmer Fran Gayo listed the film as one of the greatest films of all time. 

==References==
 
* Donald Richie: 100 Years Of Japanese Cinema, Kodansha, 2003.
* Arne Svensson: Japan: Screen Series, Zwemmer/Barnes, 1970.

==External links==
*  
*  
*  

 
 
 
 
 
 