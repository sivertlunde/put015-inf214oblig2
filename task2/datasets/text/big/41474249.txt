Puratchikkaaran
{{Infobox film
| name           = Puratchikkaaran
| image          = 
| image_size     =
| caption        = 
| director       = Velu Prabhakaran
| producer       = 
| writer         = 
| story          = 
| screenplay     =  Arun Pandian Roja
| Vidyasagar
| cinematography = 
| editing        =
| distributor    =
| studio         = 
| released       = 2000
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 2000 Tamil Tamil drama Arun Pandian, Roja in other pivotal roles. The film, which had musical score by Vidyasagar (music director)|Vidyasagar, was released in 2000.   

==Cast==
*Velu Prabhakaran as Thamizhmani
*Sathyaraj as Anna Arun Pandian Mansoor Ali Khan
*Kushboo Roja as Kanimozhi
*Neena
*Manivannan
*Radharavi

==Production==
The film was launched at a ceremony in February 1998, with Kamal Haasan attending as the chief guest. 

==Release==
The film struggled to find distributors, owing to its atheist theme and received a limited release across cinemas. A critic compared it to the several devotional movies releasing in Tamil Nadu during the period calling it "more mature and better presented" but that "the extremity of the propaganda puts one off". The critic praised Velu Prabhakarans efforts noting that the maker "believes in his convictions and the dialogs sizzle with intelligence." 

==Soundtrack==

{{Infobox Album |  
| Name        = Purathikkaaran
| Type        = soundtrack Vidyasagar
| Cover       = 
| Released    = 2000
| Recorded    = 2000 Feature film soundtrack |
| Length      = 
| Label       =  Vidyasagar
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Vidyasagar (music director)|Vidyasagar. The soundtrack, released in 2000, features 5 tracks with lyrics written by Vairamuthu. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Hariharan || 5:00
|- 2 || Mano || 4:55
|- 3 || Mannuku Nammathan || Krishnaraj || 4:52
|- 4 || Thoongum Puli || Nithyashree || 5:08
|- 5 || Srinivas || 4:00
|}

==References==
 

 
 
 
 

 