Genesis (1999 film)
 
{{Infobox film
| name           = Genesis
| image          = 
| caption        = 
| director       = Cheick Oumar Sissoko
| producer       = Jacques Atlan Chantal Bagilishya
| writer         = Jean-Louis Sagot-Durvaroux
| narrator       = 
| starring       = Sotigui Kouyaté
| music          = 
| cinematography = Lionel Cousin
| editing        = Aïlo Auguste-Judith
| distributor    = 
| released       = 16 May 1999
| runtime        = 102 minutes
| country        = Mali France
| language       = Bambara
| budget         = 
}}

Genesis ( ) is a 1999 French-Malian drama film directed by Cheick Oumar Sissoko. It was screened in the Un Certain Regard section at the 1999 Cannes Film Festival.   

==Cast==
* Sotigui Kouyaté - Jacob
* Salif Keita - Esau
* Balla Moussa Keita - Hamor
* Fatoumata Diawara - Dina
* Maimouna Hélène Diarra - Lea
* Balla Habib Dembélé - (as Habibou Dembele)
* Magma Coulibaly
* Oumar Namory Keita

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 