The Flight Commander (film)
{{Infobox film
| name           = The Flight Commander 
| image          =
| caption        =
| director       = Maurice Elvey
| producer       = Maurice Elvey   Gareth Gundrey   Victor Saville
| writer         = Eugene Clifford   John Travers John Stuart   Humberston Wright
| music          =
| cinematography = Basil Emmott   Percy Strong
| editing        = 
| studio         = Gaumont British Picture Corporation
| distributor    = Gaumont British Distributors
| released       = September 1927
| runtime        = 8,000 feet  
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}} silent war John Stuart. It was made by British Gaumont at their Lime Grove Studios in Shepherds Bush. The celebrated First World War pilot Alan Cobham appeared as himself.
 Chinese town. It was built with great publicity in Hendon. 

==Cast==
* Alan Cobham as Himself
* Estelle Brody as Mary  John Stuart as John Massey 
* Humberston Wright as James Mortimer 
* Vesta Sylva as Babette 
* Alf Goddard as Tommy 
* John Longden as Ivan 
* Cyril McLaglen as Sammy 
* William Pardue as Pierre 
* A. Bromley Davenport as Philosopher  Edward ONeill as Missionary

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.
* Wood, Linda. British Films 1927-1939. British Film Institute, 1986.

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 

 