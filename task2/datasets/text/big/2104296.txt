Requiem for a Heavyweight
Requiem for a Heavyweight was a teleplay written by Rod Serling and produced for the live television show Playhouse 90 on 11 October 1956. Six years later, it was adapted as a 1962 feature film starring Anthony Quinn, Jackie Gleason and Mickey Rooney.

The teleplay won a Peabody Award, the first given to an individual script, and helped establish Serlings reputation. The broadcast was directed by Ralph Nelson and is generally considered one of the finest examples of live television drama in the United States, as well as being Rod Serling|Serlings personal favorite of his own work. Nelson and Serling won Emmy Awards for their work.

==American television version== boxer who Ed plays McClintocks cut man, Army.

McClintock is suffering from   and tried to raise funds by betting that McClintock would be knocked out early (instead, by gamely and bravely taking a beating and refusing to go down, McClintock cost Maish a fortune).

Kim Hunter portrayed Grace Carney, an employment agency worker who tries to help the boxer make a transition to a new career. Maish persuades the boxer to turn to professional wrestling, though McClintock is proud that he never had a fixed fight and is uncomfortable with the staged, predetermined wrestling match.

Army disapproves of Maishs plans and refuses to be a part of them. Just before hes scheduled to go into the wrestling ring in a humiliating mountain man costume, McClintock learns of Maishs betting against him, and parts ways with his manager and mentor. Though he feels that boxing can ruin mens lives, Maish finds another promising young boxer to train. McClintock takes a chance on working with children at summer camp.

Because Serling and Palance were both experienced boxers, they brought a level of authenticity to Requiem for a Heavyweight, although there was very little boxing depicted in the broadcast. Requiem for a Heavyweight was the beginning of what became one of the new mediums most successful creative teams, writer Rod Serling and director Ralph Nelson.

 

===Cast===
Jack Palance	 ... 	Harlan Mountain McClintock 
	Keenan Wynn	... 	Maish 
	Kim Hunter	... 	Grace Carney 
	Ed Wynn	... 	Army 
	Joe Abdullah	... 	Fight Announcer  Max Baer	... 	Mike 
	Eddie Cantor	... 	Host 
	Ted Christy	... 	Wrestler 
	Karl Killer Davis	... 	Wrestler 
	Ned Glass	... 	Bartender 
	Young Jack Johnson	... 	Champ 
		Lyn Osborn	... 	Photographer 
	Ivan Rasputin	... 	Wrestler 
	Frank Richards	... 	Fighter in Bar 
	Max Rosenbloom	... 	Steve

==British television version==
BBC Television in the United Kingdom screened a version of the play in their regular Sunday Night Theatre anthology strand on March 31, 1957. Sean Connery, five years before portraying James Bond, starred as McClintock,    while Alvin Rakoff produced and, with Serlings approval, also wrote some new material to cover costume changes that took place during commercial breaks on US television, but could not do so on the non-commercial BBC.    Co-starring with Connery were Warren Mitchell and Rakoffs future wife Jacqueline Hill, who had recommended Connery for the leading part.  Michael Caine was featured in a small role in a new scene written by Rakoff. 

This production was reviewed in The Times newspaper the following day, which gave it a generally positive assessment, with some reservations. "It is unfortunate that Mr. Serling has allowed a saccharine romance to intrude into this self-sufficient and wholly masculine situation. Otherwise his touch is sure. Although physically miscast as the fighter, Mr. Sean Connery played with a shambling and inarticulate charm that almost made the love affair credible."    This version has not survived,  although the discovery of a complete recording of the soundtrack was announced in 2014. It had been in possession of Rakoff, who had made a recording at the time of transmission for posterity. 

==Dutch television version==
In 1959 Dutch television adapted the story as Requiem voor een zwaargewicht.

===Cast===

	Ko Van Dijk	... 	Malloy 
	Ton Van Duinhoven	... 	Manager 
	Jan Blasser	... 	Verzorger

==Yugoslav television version== Radio Television Belgrade adapted the story as Rekvijem za teškaša.

===Cast===
Bata Živojinović - Harold Brdo Maklintok

Bora Todorović - Mes Lumis

Jovan Janićijević - Armi

Neda Spasojević - Keri

Slavko Simić - Lekar

Eugen Verber - Pareli

Miodrag Andrić - Foksi

Miroslav Bijelić - Gost u kafani

Mida Stevanović - Arnold

Božidar Savićević - Barmen Čarli

Bogdan Jakuš - Drugi gost u kafani

Đorđe Jovanović - La Plant

Ivan Jonaš - Hansonov poverenik

Radomir Popović - Službenik

Melita Bihalji

Ras Rastoder

Branislav Radović

Nebojsa Bakočević

==Film version==
{{Infobox film
| name           = Requiem for a Heavyweight
| image          = Requiem for a Heavyweight film.jpg
| image_size     =
| caption        =
| director       = Ralph Nelson
| producer       = David Susskind
| writer         = Rod Serling
| narrator       =
| starring       = Anthony Quinn Jackie Gleason Mickey Rooney
| music          =
| cinematography = Arthur J. Ornitz
| editing        = Carl Lerner
| distributor    = Columbia Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States English
| budget         =
}} movie version in 1962 with Anthony Quinn in the role originated by Jack Palance, Jackie Gleason and Mickey Rooney in the parts portrayed on television by Keenan Wynn and his father Ed Wynn, and social worker Grace Miller was portrayed by Julie Harris.
 Cassius Clay, later known as Muhammad Ali, appears as Quinns opponent in a boxing match at the beginning of the movie, a memorable sequence filmed with the camera providing Quinns point of view as the unstoppable Clay rapidly punches directly at the movie audience. Afterward, Maish is confronted by bookies who threaten his life if he fails to repay the bet he just lost on the fight.

The film version is somewhat darker in its plotline than the original teleplay. Mountain Rivera (Quinn) is to interview for a counselor position at a childrens camp, arranged by Grace Miller, but Maish takes him to a bar where they both get drunk, hoping that Mountain will forget about the job interview. Army (Rooney) arrives at the bar to remind Mountain about the appointment, but he embarrasses himself at the hotel where the interview is to take place, behaving drunkenly in plain sight of the camp owners. Grace follows Mountain home to try to understand what went wrong, and though they are attracted to each other, Mountains aggression scares Grace off. She confronts Maish in tears, condemning him for controlling Mountain and ruining his chance to make a new life for himself.
 Haystack Calhoun," a grappler from Arkansas billed at 601&nbsp;lbs.

===Cast===

Anthony Quinn	 ... 	Luis Mountain Rivera 
	Jackie Gleason	... 	Maish Rennick 
	Mickey Rooney	... 	Army 
	Julie Harris	... 	Grace Miller  Stanley Adams	... 	Perelli (as Stan Adams) 
	Madame Spivy	... 	Ma Greeny  Cassius Clay	... 	Himself  
	Val Avery	... 	Young fighters promoter 
	Herbie Faye	... 	Charlie, the Bartender 
	Jack Dempsey	... 	Himself  Haystack Calhoun	... 	Himself

==The Man in the Funny Suit== The Diary of Anne Frank. The Man in the Funny Suit was telecast as an installment of the Westinghouse Desilu Playhouse with Rod Serling and Red Skelton playing themselves, and it remains available for public viewing at the Paley Center for Media in New York City and Los Angeles.

===Cast===

Keenan Wynn ... Keenan Wynn 
Ed Wynn ... Ed Wynn 
Rod Serling ... Rod Serling 
Maxine Stuart ... Sharley Wynn 
Ralph Nelson ... Ralph Nelson 
Red Skelton ... Red Skelton 
Bob Mathias ... Bob Mathias 
William Roerick ... Martin Manulis 
Maxie Rosenbloom ... Slapsie Maxie Rosenbloom 
Seymour Berns ... Skeltons Director 
Robin Blake ... Script Girl 
Joey Faye ... Latecomer 
Ned Glass ... Ed Wynns Understudy 
Charlene Glazer ... Secretary 
Drew Handley ... Assistant Director 
Robert H. Harris ... Technical Director 
Richard Joy ... Announcer 
Bill Walker ... Porter

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 