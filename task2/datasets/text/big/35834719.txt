Anita: Dances of Vice
{{Infobox film
| name           = Anita: Dances of Vice
| image          = Anita – Dances of vice.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Rosa von Praunheim 
| producer       = Rosa von Praunheim
| writer         = 
| screenplay     = Rosa von Praunheim   Marianne Enzensberger  Lotti Huber   	Hannelene Limpach	 	
| story          = 
| based on       =  
| narrator       = 
| starring       = Lotti Huber   Ina Blum   Mikael Honesseau
| music          =Konrad Elfers   Rainer Rubbert   Alan Marks   Wilhelm Dieter Siebert   Ed Lieber 
| cinematography = Elfi Mikesch
| editing        = Mike Shephard  Rosa von Praunheim	
| studio         = Road Movies    ZDF
| distributor    =
| released       = 19 February 1988
| runtime        = 89 minutes
| country        = West Germany
| language       = German
| budget         = 
| gross          = 
}} German film directed by Rosa von Praunheim.  The film follows an elderly delusional woman who thinks she is Anita Berber (1899-1928), a German dancer who, with her partner, Sebastian Droste, came to represent the decadence of 1920s Berlin with their nude dancing, their cocaine habits and their uninhibited sex lives. Murray, Images in the Dark, p. 108 

Anita Berber’s story is recounted through the thoughts and remembrance of the elderly lady (played by Lotti Huber) who is confined in a lunatic asylum. There, in her dreams and exchanges with patients and staff, scenes from Anitas scandalous life are relived. The film is divided in two sections with all scenes taking place on the asylum shot in black and white and the Anita Berber part shot in color. 

==Notes==
 

== References ==
*Murray, Raymond. Images in the Dark: An Encyclopedia of Gay and Lesbian Film and Video Guide. TLA Publications, 1994, ISBN 1880707012

==External links==
* 

 
 
 