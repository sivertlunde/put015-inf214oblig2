The Angrez
 
 
{{Infobox film
| name           = The Angrez
| image          = Angrez.jpg
| image_size     =
| caption        = Film Poster 
| director       = Kuntaa Nikkil 
| producer       = M.Sridhar rao<sridhar Cinema Production
| writer         = Kuntaa Nikkil
| starring       = Kuntaa Nikkil Mast Ali Aziz Naser Soumya Bollapragada Ganesh Venkatraman Tara DSouza Dheer Charan Srivastav 
| music = Mallinath Maruth
| cinematography =
| editing        =
| studio         =
| DVD            = KAD/Shalimar
| released       = 2 October 2005
| runtime        = 110 minutes
| country        = India Telugu English Hindi
| 

}}

The Angrez is a 2005 Indian comedy movie directed by Kuntaa Nikkil. In this movie, the word "Angrez" refers to English people and Non-resident Indian and Person of Indian Origin|non-resident Indians.

The movie was a trend-setter in that, it was one of the first to effectively and humorously, often resorting to the slapstick kind, portray the dialect of the Old City, Hyderabad and the Hyderabadi tongue.   The film also portrays the fascination for American materialism, the hype around the IT industry and the lifestyle and culture it has spawned.

==Plot==
The movie revolves around three sets of people.
# The Angrez – Two guys who come from the US to serve in an IT company. 
# Ismail bhai & gang – A bunch of locals from the Old City Area
# Mama a.k.a Annaa & gang – Local gangsters
 Hyderabad to take up jobs in their friends company and settle in Hyderabad. Based in the old city, Ismail bhai and the gang meet up near Charminar and start their daily routine with gossips, babble and talks of bravery. Ismail bhai is apparently the gang leader and his gang comprises Saleem Pheku- a habitual liar, Jahangir – a self claimed hardcore gangster, Gafoor and Chaus who all follow Ismail bhai throughout the film.

While visiting the old city, Pranai and Rochak meet the Ismail bhai gang at a hotel in the old city. The Ismail bhai gang gets into a spat with Pranai and Rochak.  Rochak and Pranai are guided to safety by their guide as they are chased by the Ismail bhai gang. While Pranai and Rochak are engaged in love affairs in their office, the Ismail bhai gang keeps planning a strategy to seek their revenge. 

Meanwhile, Ramesh, Pranais cousin (who is also their butler) colludes with Mama, the typical gangster of the Hyderabadi underbelly, to kidnap Pranai for ransom. Mama and his goons take up the assignment and the plan is set in motion.

Ismail bhai is still enraged as he believes that the harmless spat is now a matter worth dying for and is a matter of his honour.    On the other side of the town, Mama and his gang enter the residence of the NRIs, at the same time the Ismail bhai gang after sniffing out their whereabouts go into the NRIs house at night to seek their revenge and restore Ismail bhais image. Mama and gang mistakenly kidnap Rochak. Ismail bhai and gang return from their mission failing as usual. Rochak finds out the mastermind behind the kidnapping and bribes the guard and warns Pranai of his cousins intentions. 

Ismail bhai and Mama gang have a brush against each other. Pranai and Rochak succeed in making the two gangs fight against each other and manage their escape. As Mama and gang is nabbed by the Police, Ismail bhai and gang returns to Charminar after being heavily assaulted by Mama and his goons. And life, in Hyderabadi style, goes on as usual for the Ismail bhai gang. 

==Pre-production== script for expressions of people from the old city.    When a half-hour promo was shot on DV camera nobody was willing to produce the movie. Then Sreedhar Rao stepped forward and decided to produce the movie since he felt that the movie had highlighted the core culture of the city.  

==Reception==

=== Release ===
The movie was released initially only in four theatres in Hyderabad, but later on it was released in many theatres in Hyderabad and CDs/DVDs were released only after it completed 25&nbsp;weeks. The producer petitioned the Department of Tourism to promote the film to raise interest in the Hyderabad area. {{cite news
|url=http://www.hindu.com/mp/2006/01/21/stories/2006012102620400.htm |title=Full throttle ahead |last=Chowdhary |first=Sunita
|date=21 January 2006 |work=The Hindu|accessdate=26 February 2009}}  Hyderabadi culture, record by running to packed houses in a single theatre for over 25 weeks. 

===Impact on the movie industry=== Kal ka Nawab, Hyderabad Nawaabs, Aadab Hyderabad, Hungama in Dubai among others.  But none of them quite succeeded in creating the effect and the charm of the original, though these movies were popular among the Old City residents. The success of The Angrez can be attributed to the spontaneity of the humour whereas in the movies that followed, the humour looks forced.  The film not only gained popularity in Andhra Pradesh and the other parts of India but also among the large diaspora in the Middle-East and the US, thanks to the Internet and availability of original as well as pirated DVDs. With pirated DVDs being released in huge numbers, several raids were conducted and lakhs of pirated copies were recovered.    The DVD rights for the movie were sold to KAD Entertainment.

===Spoofs from around the world=== spoofs and parodies on the popular video sharing site YouTube. The clips include people re-enacting some of the most popular segments from the movie. Others have adopted the Hyderabadi "style" and come up with new material. KAD, the manufacturers of the original DVD, lost around $5&nbsp;million due to pirated copies being released and the movie being uploaded on all the major video sites.  The film collected 150&nbsp;million.

==Notes==
 
M.Sridhar Rao is the producer under the banner-Sridhar Cinema.
SDC presents is an abbreviation for Sridhar Constructions.
Kunta Nikkel has directed and also acted in the movie.

==External links==
* 

 
 
 
 
 