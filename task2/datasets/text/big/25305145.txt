Love Affair (1932 film)
{{Infobox film
| name           = Love Affair
| image_size     = 
| image	=	Love Affair FilmPoster.jpeg
| caption        = 
| director       = Thornton Freeland
| producer       = 
| writer         = Ursula Parrott (story) Jo Swerling
| narrator       = 
| starring       = Dorothy Mackaill Humphrey Bogart Hale Hamilton
| music          = 
| cinematography = 
| editing        = 
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       = March 17, 1932
| runtime        = 68 minutes
| country        = United States
| language       = English}}
 1932 romantic drama film starring Dorothy Mackaill as an adventurous socialite and Humphrey Bogart as the airplane designer she falls for. It is based on the short story of the same name by Ursula Parrott.

==Plot==
Wealthy socialite Carol Owen (Dorothy Mackaill) decides to take up flying. Gilligan (Jack Kennedy) sets her up with a homely instructor, but she requests dashing Jim Leonard (Humphrey Bogart) instead. Jim has some fun, taking her through some aerobatic maneuvers that leave her queasy, but still game. For revenge, she gives him a lift into town in her sports car, driving at breakneck speeds. They begin seeing each other.

Carol learns that Jim is designing a revolutionary airplane engine, but cannot get any financial backing. She decides to give him a secret helping hand, persuading her skeptical financial manager, Bruce Hardy (Hale Hamilton), to invest in the project. Hardy is only too pleased to oblige, as he has asked Carol numerous times to marry him.
 Broadway producer. Things become serious between Carol and Jim. He begins neglecting his work and eventually spends the night with her. The next day, he asks her to marry him. She realizes that she is distracting him from making a success of his engine and turns him down.

When Hardy asks Carol once again to marry him, she jokingly tells him she would only consider his offer if she were broke. He then informs her that she is. He has been paying all her bills for the past year. Hoping to help Jim, she agrees to wed Hardy.

Hardy tries to break off his relationship with Linda. This is what Georgie has been waiting for. He has coached Linda to extort $50,000 from Hardy to finance a new play in which Linda will star, but the businessman will only write her a check for $10,000. To try to pressure Hardy, Georgie has Linda lie to Jim about the relationship.

Meanwhile, Carol has second thoughts and goes to break the news to Hardy. Before she can however, Jim shows up and insists that Hardy marry his sister. However, when Hardy shows him the canceled $10,000 check endorsed to Georgie, Jim realizes Linda has deceived him. He apologizes and leaves.

Carol decides to kill herself by crashing an airplane. As she starts to take off, Jim reads the suicide note she left with Gilligan. He manages to cling to the fuselage, work his way gingerly to the cockpit (while the plane is in flight), and reconcile with Carol.

==Cast==
*Dorothy Mackaill as Carol Owen
*Humphrey Bogart as Jim Leonard
*Hale Hamilton as Bruce Hardy
*Halliwell Hobbes as Kibbee, Carols long-time butler
*Astrid Allwyn as Linda Lee
*Bradley Page as Georgie Keeler

==External links==
* 
* 

 

 

 
 
 
 
 
 
 
 
 