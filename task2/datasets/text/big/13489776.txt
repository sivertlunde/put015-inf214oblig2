The Conspiracy (1916 film)
 
{{Infobox film
| name           = The Conspiracy
| image          =
| caption        =
| director       = Henry MacRae
| producer       =
| writer         = William Parker Leona Radnor
| narrator       = Harry Carey
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = Silent (English intertitles)
| budget         =
}}
 silent drama Harry Carey.

==Cast== Harry Carey
* Edith Johnson
* Lee Shumway - (as Leon C. Shumway)
* Edwin Wallock - (as E.N. Wallack)

==Reception== city and state film censorship boards. The Chicago Board of Censors required cuts of the intertitle "Pour that in her glass, do you understand?", flash scenes involving gambling, and the scene showing a suicide. 

==See also==
* List of American films of 1916
* Harry Carey filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 



 