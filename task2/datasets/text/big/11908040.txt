Who's Your Caddy?
{{Infobox film
| name           = Whos Your Caddy?
| image          = Whosyourcaddy.jpg 
| caption        = Theatrical release poster
| director       = Don Michael Paul
| producer       = Christopher Eberts Tracey Edmonds Kia Jam Arnold Rifkin
| writer         = Don Michael Paul Bradley Allenstein Robert Henny Antwan "Big Tony Cox 
| music          = Jon Lee Russ "Aaddict" Howard Zack "Bizness" Burke Nate & Brainz
| cinematography = Thomas Callaway
| editing        = Vanick Moradian Scott Mosier
| studio         = Dimension Films Metro-Goldwyn-Mayer Our Stories Films Cheyenne Enterprises
| distributor    = The Weinstein Company
| released       = July 27, 2007
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $15,000,000
| gross          = $5,713,425 (United States) 
}} Tony Cox, Jeffrey Jones, and Jesper Parnevik. It is the first film produced by Robert L. Johnsons Our Stories Films studio. It was released on July 27, 2007 in the United States and was released on DVD on November 27, 2007.

==Plot==
 
When hip-hop star Christopher “C-Note” Hawkins (Big Boi) is denied membership into an exclusive Carolina Pines Country Club, he comes up with a cunning plan that will oblige the country club to allow his acceptance. C-Note purchases property that contains land from the 17th hole, and bribes the country club for a membership in exchange for his land. The rest of the movie’s plot revolves around the club members and their efforts to get C-Note kicked out, while he disrupts the club’s atmosphere.

==Cast==
*Big Boi as Christopher "C-Note" Hawkins (credited as "Antwan André Patton")
*Chase A.A. Jackson as Christopher "C-Note" Hawkins flashback as little boy
*David Kelly  as Robert "Bobby" Hawkins   James Avery as Caddy Mack
*Bruce Bruce as Golf-Ball Eddie Tony Cox as Big Willie Johnson
*Terry Crews as Tank
*Faizon Love as Big Large
*Finesse Mitchell as Dread
*Jeffrey Jones as Cummings
*Jesper Parnevik as Himself
*Andy Milonakis as Wilson Cummings
*Sherri Shepherd as Lady G
*Tamala Jones as Shannon Williams
*Garrett Morris as Rev. JJ Jackson
*Cam Gigandet as Mick
*Chase Tatum as Kidd Clean
*Susan Ward as Mrs. Cummings
*Hugh Jass as Bobby Lee
*Lawrence Hilton-Jacobs as Joseph Williams
*Robert Curtis Brown as Frosty
*Todd Sherry as Realtor
*Matthew Reichel as Himself
*Lil Wayne as Himself

==Reception==
The film had received negative reviews from critics. In particular, many critics have deemed this film a "terrible rip-off" of Caddyshack.      Overall it is ranked a "Rotten" rating of 6% on Rotten Tomatoes, with the consensus calling the film "unoriginal, unfunny, and just plain forgettable."   However, former U.S. president Bill Clinton "loves" the film.   The film was nominated for a Golden Raspberry awards in the fields of Worst Remake or Rip-off, but lost to I Know Who Killed Me.

==Box office==
The movie grossed US$2.76 million in its first week at the box office, debuting in the number 10 spot. But the film was a dud at the box office, earning only $5,713,425.   

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 