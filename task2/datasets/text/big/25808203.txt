Blessings of the Land
 
{{Infobox film
| name           = Biyaya ng Lupa (Blessings of the Land)
| image          = Biyaya59.jpg
| caption        = 
| director       = Manuel Silos
| producer       = 
| writer         = Celso Al Carunungan Pablo Naval
| starring       = Rosa Rosal
| music          = 
| cinematography = 
| editing        = Ike Jarlego Sr.
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = Philippines Filipino (Tagalog language|Tagalog)
| budget         = 
}}

Blessings of the Land ( )  is a 1959 Filipino drama film directed by Manuel Silos. It was entered into the 10th Berlin International Film Festival.     The film won Best Picture and Best Story from the Filipino Academy of Movie Arts and Sciences.  In 1960, it was shown at the Asian Film Festival held in Tokyo.  The story was written by Celso Al. Carunungan, while the screenplay was written by Carunungan together with Pablo Naval.  The movie was produced by LVN Pictures.  , kabayancentral.com 
 Filipino couple residing in a village.  The couples deaf-mute Men in the Philippines|son, Miguel, is tested and "must rise above his Disability|handicap" after the tragedies experienced by the family. Pavlides, Dan.  , movies,nytimes.com 

==Plot== married life courts Choleng, a niece of Jose.  Choleng dies by falling from a cliff while trying to evade Bruno.  Bruno goes to the mountains to hide from the angry villagers.  Bruno returns and rapes Joses daughter, Angelita.  Together with the villagers, Jose pursues Bruno but is shot by the latter.  
 mortgage the rice fields before going back to Manila. 

A landowner from another town hires Bruno to destroy the lanzones harvest of Maria’s family.  Bruno and his group fail because of the villagers.  Miguel kills Bruno.   Arturo comes back from Manila and reconciles with his family. 

==Cast==
* Rosa Rosal as Maria
* Leroy Salvador as Miguel (Salvador won Best Supporting Actor during the 1960 Asian Film Festival for this film )
* Tony Santos, Sr. (as Tony Santos) as Jose
* Carmencita Abad as Gloria
* Carlos Padilla, Jr. as Arturo
* Marita Zobel as Angelita
* Joseph de Cordova as Bruno
* Danilo Jurado as Lito
* Carmen Del Ocampo
* Miguel Lopez
* Mario Roldan
* Tony Dantes
* Jerry Reyes
* Mila Ocampo as Choleng
* Pedro Faustino

==Accolades==
{| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Group
! width="25%"| Category
! width="25%"| Category
! width="10%"| Result
|- 1960
| Asian Film Festival
| align="center"| Best Supporting Actor
| align="center"| Leroy Salvador
|  
|- 10th Berlin Berlin International Film Festival
| align="center"| Golden Bear for Best Film
| align="center"| 
|  
|-
|}

{| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Group
! width="25%"| Category
! width="25%"| Category
! width="10%"| Result
|- 1960
| FAMAS Awards
| align="center"| FAMAS Award for Best Picture| Best Picture
| align="center"| 
|  
|-
| align="center"| Best Original Story
| align="center"| Celso Al. Carunungan 
|  
|- Best Actor
| align="center"| Leroy Salvador
|  
|-
| align="center"| Tony Santos
|  
|- Best Actress
| align="center"| Rosa Rosal
|  
|- Best Supporting Actress
| align="center"| Marita Zobel
|  
|- Best Director
| align="center"| Manuel Silos
|  
|-
| align="center"| Best Screenplay
| align="center"| Celso Al. Carunungan Pablo Naval 
|  
|-
| align="center"| Best Editing
| align="center"| Enrique Jarlego 
|  
|-
| align="center"| Best Musical Score
| align="center"| Juan Silos Jr. 
|  
|-
| align="center"| Best Sound Engineering
| align="center"| Manuel Silos
|  
|-
|}

==References==
 

==External links==
* 

 
 
 
 
 
 