O'Malley of the Mounted
{{Infobox film
| name           = OMalley of the Mounted
| image          = OMalley of the Mounted (1921) - Ad 1.jpg
| alt            = 
| caption        = Newspaper ad
| director       = Lambert Hillyer
| producer       = William S. Hart
| screenplay     = William S. Hart Lambert Hillyer Alfred Allen Bert Sprotte Antrim Short
| music          = 
| cinematography = Joseph H. August
| editing        = LeRoy Stone 
| studio         = William S. Hart Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent Western Western film Alfred Allen, Bert Sprotte, and Antrim Short. The film was released on February 6, 1921, by Paramount Pictures.   A copy of the film is in the Library of Congress and the Museum of Modern Art film archives.
 
==Plot==
 

== Cast ==
*William S. Hart as Sergeant OMalley
*Eva Novak as Rose Lanier
*Leo Willis as Red Jaeger Alfred Allen as Big Judson
*Bert Sprotte as Sheriff
*Antrim Short as Bud Lanier 

== References ==
 

== External links ==
 
*  

 

 
 
 
 
 
 
 
 