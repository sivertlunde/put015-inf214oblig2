A Life for the Taking
 
{{Infobox film
| name           = A Life for the Taking
| image          = 
| caption        = 
| director       = Göran du Rées
| producer       = Göran du Rées
| writer         = Per Gunnar Evander Göran du Rées
| starring       = Hans Mosesson
| music          = 
| cinematography = Göran du Rées
| editing        = 
| distributor    = 
| released       =  
| runtime        = 82 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}}

A Life for the Taking ( ) is a 1995 Swedish drama film directed by Göran du Rées. It was entered into the 19th Moscow International Film Festival.   

==Cast==
* Hans Mosesson as Stig F. Dahlman
* Björn Granath as Björn Granath
* Benny Granberg as Benny
* Niklas Hald as Clark
* Barbro Kollberg as Signe Dahlman
* Jussi Larnö as Ensio
* Anna Lindholm as Eva
* Anneli Martini as Måna
* Bibi Nordin as Vera Lundberg
* Terri-Lynne Sandberg as Birgitta

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 