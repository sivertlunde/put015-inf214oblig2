Just Cause (film)
{{Infobox film
| name           = Just Cause
| image          = Just_cause_ver1.jpg
| caption        = 
| director       = Arne Glimcher 
| producer       = Arne Glimcher  Steve Perry Lee Rich Jeb Stuart Peter Stone
| starring       = Sean Connery Laurence Fishburne Kate Capshaw Blair Underwood Ed Harris
| music          = James Newton Howard
| cinematography = Lajos Koltai
| editing        = William M. Anderson Armen Minasian
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = $27 million
| gross          = $36,853,222
}} novel of the same name.

==Plot==
 liberal Harvard Harvard professor raping and coerce Fergusons confession.

The plot thickens when Ferguson tells the professor that the murder was actually committed by Blair Sullivan (Ed Harris), a serial killer awaiting execution, who later reveals the location of the weapon used to kill the girl. When Armstrong discovers the weapon, Brown tries to threaten him into abandoning the investigation. (It is revealed that the murdered girl was Browns daughters best friend.) Ferguson gets a re-trial and is freed from prison. Subsequently, the governor signs Sullivans death warrant.

Armstrong then receives a call from Sullivan, who says he has a final clue for him, but first asks him to visit his parents and tell them he said goodbye. Armstrong is shocked to find the butchered bodies of Sullivans parents, and returns demanding an explanation before telling Sullivan what he saw. Sullivan gloats that he and Ferguson struck a deal: Ferguson would kill Sullivans parents in exchange for freedom, while Sullivan would claim responsibility for the girls murder, which Ferguson in fact committed. Armstrong has the last laugh by lying to Sullivan that his parents were alive and that they "forgive him". Sullivan becomes furious and resists the guards taking him to the electric chair where he is executed.

Armstrong and Brown go after Ferguson, who desires revenge on Armstrongs wife (Kate Capshaw); she was the prosecutor in a previous rape trial which, while thrown out of court on a technicality, resulted in him being brutalized and castrated in jail, as well as being kicked out of Cornell, robbing him of any chance of a future. Ferguson plans to murder Armstrongs wife and daughter (Scarlett Johansson) and then disappear, but Armstrong and Brown come to the rescue. They kill Ferguson and save Armstrongs family.

==Cast==
*Sean Connery – Paul Armstrong
*Laurence Fishburne – Sheriff Tanny Brown
*Kate Capshaw – Laurie Prentiss Armstrong
*Blair Underwood – Bobby Earl Ferguson
*Ed Harris – Blair Sullivan Christopher Murray – Detective T. J. Wilcox
*Ruby Dee – Evangeline
*Scarlett Johansson – Katie Armstrong
*Daniel J. Travanti – Warden
*Ned Beatty – McNair Kevin McCarthy – Phil Prentiss

==Reception==

===Critical===
Unlike Glimchers previous film, The Mambo Kings, Just Cause received mostly negative reviews,    with a "Rotten" 22% rating on Rotten Tomatoes   and an average score of 6.2 on The Internet Movie Database.

===Box office===

The movie debuted with moderate success. 

==References==
 

==External links==
* 
* 
*   at Rotten Tomatoes

 
 
 
 
 
 
 
 
 
 
 
 