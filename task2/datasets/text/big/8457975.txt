Goodbye Dear Moon
{{Infobox film
| name           = Adiós Querida Luna
| image          = Adiósqueridaluna.jpg
| caption        = Theatrical release poster
| director       = Fernando Spiner
| producer       = Executive Producer: Rolo Azpeitia Producers: Laura Barroetaveña Elena Carpman Ricardo Preve
| writer         = Sergio Bizzio Valentín Javier Diment Alejandra Flechner Fernando Spiner Alejandro Urdapilleta
| narrator       = Claudio Rissi
| starring       = Alejandro Urdapilleta Alejandra Flechner
| music          = Valentín Javier Diment Horacio Fontova Gustavo Pomeranec
| cinematography = Claudio Beiza
| editing        = Alejandro Parysow
| distributor    = Primer Plano Film Group S.A.
| released       =  
| runtime        = 100 minutes
| country        = Argentina
| language       = Spanish
| budget         = 
|}} Argentine sci-fi comedy film directed by Fernando Spiner and written by Spiner, Sergio Bizzio, Valentín Javier Diment, Alejandra Flechner, Alejandro Urdapilleta, and Sergio Bizzio. 

==Plot==
In the year 2068 the Earth is plagued by typhoons, tsunamis, droughts, and floods. Against this apocalyptic background an Argentine scientist, Carlos Nahuel Goldstein, outlines a theory: knowing that the axis of rotation of our planet is inclined due to the gravitational force exerted by the Moon, if men were able to destroy it, the Earth would be straightened and then climate would become stabilized.

While the international community rejects that reasoning, the Argentine government, without consulting the other nations, decides to send a secret mission to bomb the Moon. The project is called "Good bye, dear Moon." When the great powers find out, they threaten to apply severe  ), the subcommanders Esteban Ulloa (Alexander Urdapilleta) and Silvia Rodulfo (Alejandra Flechner).

Facing an uncertain destiny, the passions among the crew build up. An extraterrestrial presence watches them and ejects the commander into space. When they are alone, Rodulfo confesses his love to Ulloa. His love made him become astronaut, so he could be near her. Affected by the confession, Ulloa gives herself to him. Unexpectedly, the extraterrestrial bursts into the ship to declare its love to Rudolfo.

==Cast==
* Alejandro Urdapilleta as Subcomandante Esteban Ulloa
* Alejandra Flechner as Subcomandante Silvia C. Rodulfo
* Gabriel Goity as Comandante Humberto A. Delgado
* Horacio Fontova as Extraterrestre
* Luis Ziembrowsky

==Awards==
;Wins
* Mar del Plata Film Festival: Best Actor, Alejandro Urdapilleta; 2004.

;Nominations
* Argentine Film Critics Association Awards: Silver Condor; Best Costume Design, Ricky Casali and Paola Delgado; 2006.

==References==
 

==External links==
*  
*   at the cinenacional.com  

 
 
 
 
 
 


 