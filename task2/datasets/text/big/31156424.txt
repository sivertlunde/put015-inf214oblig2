Manushyaputhran
{{Infobox film 
| name           = Manushyaputhran
| image          =
| caption        = Baby Rishi
| producer       = Kadakkavoor Thankappan
| writer         = KG Sethunath
| screenplay     = KG Sethunath Madhu Jayabharathi Vincent
| music          = G. Devarajan Ashok Kumar
| editing        = 
| studio         = Srushti Films International
| distributor    = Srushti Films International
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, Baby and Vincent in lead roles. The film had musical score by G. Devarajan.   

==Cast==
  Madhu as Karthikeyan
*Jayabharathi as Madhavi
*Vidhubala as Ammu Vincent as Thommi
*Adoor Bhasi
*Muthukulam Raghavan Pillai as Vaidyar Prema
*T. R. Omana
*Adoor Bhavani as Madhavi
*Bahadoor as Kochu Govindan GK Pillai Khadeeja as Kaalikutty
*S. P. Pillai as Kunjandi
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma and Gowreesapattam Sankaran Nair. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Amme Kadalamme || P. Madhuri || Vayalar Ramavarma || 
|-
| 2 || Kadalinu Pathinezhu || P. Madhuri || Gowreesapattam Sankaran Nair || 
|-
| 3 || Swargasaagarathil || K. J. Yesudas || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 