The Glitterball
{{Infobox film 
| name           = The Glitterball
| image          = Title Card from British film "The Glitterball" (1977).jpg
| image_size     = 
| caption        = Title Card
| director       = Harley Cokeliss
| producer       = Mark Forstater
| writer         = Michael Abrams, Howard Thompson, Harley Cokeliss
| starring       =  
| music          =  
| cinematography = Alan Hall
| editing        =  
| studio         =  
| distributor    = 
| released       = 1977 (UK)
| runtime        = 55 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          = 
}}
 British Sci Mark Forstater Productions for the Childrens Film Foundation.    The film was screened at the 2010 Edinburgh Film Festival as part of a retrospective of 16 "rarely seen" British films made between 1967 and 1979, "rediscovered" after a year of detective work by event staff.     In 1979, Methuen Publishing released the childrens novel by the same name, written by screenwriters Howard Thompson and Harley Cokeliss. ISBN 9780416863406.

==Plot==
Two boys befriend a stranded alien in the shape of a little silver ball and help it to return home. 

==Cast==
*Ben Buckton as Max Fielding
*Keith Jayne as Pete
*Ronald Pember as George Filthy Potter
*Marjorie Yates as Mrs. Fielding Barry Jackson as Sergeant Fielding
*Andrew Bradford as Corporal
*Derek Deadman as Ice Cream Man
*Leslie Schofield as Radar Operator

==Reception==

 

==Awards and nominations==
* 1982, nominated for Fantasporto International Fantasy Film Award for Best film for Harley Cokeliss. 

==References==
 

==External links==
*  
*  . Full synopsis and film stills (and clips viewable from UK libraries).

 

 
 
 
 
 
 
 

 