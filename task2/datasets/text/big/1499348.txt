Happy Feet
 
 
{{Infobox film
| name           = Happy Feet
| image          = Happy Feet Poster.jpg
| caption        = Promotional poster George Miller Bill Miller George Miller Doug Mitchell
| writer         = Warren Coleman John Collee George Miller Judy Morris
| starring       = Elijah Wood Robin Williams Brittany Murphy Hugh Jackman Nicole Kidman Hugo Weaving John Powell Gia Farrell
| cinematography = David Peers
| editing        = Christian Gazal Margaret Sixel
| studio         = Village Roadshow Pictures Kennedy Miller Productions Animal Logic Warner Bros. Roadshow Films    
| released       =  
| runtime        = 108 minutes
| country        = United States Australia
| language       = English
| budget         = $100 million
| gross          = $384,335,608  . Box Office Mojo. Retrieved 2011-01-02. 
}} musical family George Miller. Warner Bros., Village Roadshow Pictures and Kingdom Feature Productions and was released in North American theaters on November 17, 2006. It is the first animated film produced by Kennedy Miller in association with visual effects/design company Animal Logic.

Though primarily an animated film, Happy Feet does incorporate motion capture of live action humans in certain scenes. The film was simultaneously released in both conventional theatres and in IMAX 2D format.  The studio had hinted that a future IMAX 3D release was a possibility. However, Warner Bros., the film’s production company, was on too tight a budget to release Happy Feet in IMAX digital 3D. 

Happy Feet won the Academy Award for Best Animated Feature and the BAFTA Award for Best Animated Film, and was nominated for the Annie Award for Best Animated Feature and the Saturn Award for Best Animated Film.

The film was dedicated in memory of Nick Enright, Michael Jonson, Robby McNeilly Green, and Steve Irwin.

A sequel, Happy Feet Two, was released into theatres November 18, 2011 and received mixed reviews.

==Plot== yellow band, which he says that it is from an alien abduction. Mumble narrowly escapes the hungry birds by falling into a crevice.
 rockhopper penguin, about its origin. Lovelace has the plastic rings of a six pack entangled around his neck, which he claims to have been bestowed upon him by mystic beings.
 My Way", with Mumble lip syncing, but the plan fails. In desperation, Mumble begins tap dancing in synch with her song. She falls for him and the youthful penguins join in for singing and dancing to "Boogie Wonderland". The elders are appalled by Mumbles conduct, which they see as the reason for their lean fishing season. Memphis begs Mumble to stop dancing, for his own sake, but when Mumble refuses, he is exiled.

Mumble and the Amigos return to Lovelace, only to find him being choked by the plastic rings. Lovelace confesses they were snagged on him while swimming off the forbidden shores, beyond the land of the elephant seals. Not long into their journey, they are met by Gloria, who wishes to join with Mumble as his mate. Fearing for her safety, he ridicules Gloria, driving her away.
 tracking device attached to his back. He returns to his colony and challenges the will of the elders. Memphis reconciles with him, just as a research team arrives, proving the aliens to be true. The whole of the colony, even Noah, engages in dance.

The research team returns their expedition footage, prompting a worldwide debate. The governments realize they are overfishing, leading to the banning of all Antarctic fishing. At this, the emperor penguins and the Amigos celebrate. In the final scene, a baby penguin is seen dancing next to Mumble and Gloria, revealed to be their son Erik in Happy Feet Two.

==Cast==
{{multiple image direction = vertical
|image1=Happy Feet Premiere (307976990).jpg
|width1=150
|image2=Happy Feet Premiere (307987303).jpg
|width2=150
|image3=Happy Feet Premiere (3).jpg
|width3=150
|image4=Happy Feet Premiere (307982333).jpg
|width4=150
|footer=Elijah Wood, Robin Williams, Brittany Murphy and Nicole Kidman at the films European premiere in London, UK.
|header_align=center
}}
* Elijah Wood as Mumble
* Robin Williams as Ramón, Cletus, Lovelace and the narrator
* Brittany Murphy as Gloria
* Hugh Jackman as Memphis
* Nicole Kidman as Norma Jean
* Hugo Weaving as Noah the Elder
* Carlos Alazraqui as Nestor
* Steve Irwin as Trev
* Lombardo Boyar as Raul
* Jeffrey Garcia as Rinaldo
* Johnny Sanchez as Lombardo
* Miriam Margoyles as Mrs. Astrakhan
* Fat Joe as Seymour
* Anthony LaPaglia as Skua Boss
* Roger Rose as Leopard Seal
* Elizabeth Daily as baby Mumble
* Alyssa Shafer as baby Gloria

==Production==
Miller cites as an initial inspiration for the film an encounter with a grizzled old camera-man, whose father was Frank Hurley of the Shackleton expeditions, during the shooting of Mad Max 2: "We were sitting in this bar, having a milkshake, and he looked across at me and said, ‘Antarctica.’ He’d shot a documentary there. He said, ‘You’ve got to make a film in Antarctica. It’s just like out here, in the wasteland. It’s spectacular.’ And that always stuck in my head.” 
Happy Feet was also partially inspired by earlier documentaries such as the BBCs Life in the Freezer.  In 2001, during an otherwise non-sequiter meeting, Doug Mitchell impulsively presented Warner Bros., studio president Alan Horn with an early rough draft of the films screenplay, and asked them to read it while he and Miller flew back to Australia. By the time theyd landed, Warner Bros. had decided to provide funding on the film. Production was slated to begin sometime after the completion of the fourth Mad Max film, Fury Road, but geo-political complications pushed Happy Feet to the forefront in early 2003.
 Siggraph 2007 demonstration,  and are available online, as well.

The animation in Happy Feet invested heavily in motion capture technology, with the dance scenes acted out by human dancers. The tap-dancing for Mumble in particular was provided by Savion Glover who was also co-choreographer for the dance sequences.  The dancers went through "Penguin School" to learn how to move like a penguin, and also wore head apparatus to mimic a penguins beak. 

Happy Feet needed an enormous group of computers, and Animal Logic worked with IBM to build a server farm with sufficient processing potential. The film took four years to make. Ben Gunsberger, Lighting Supervisor and VFX Department Supervisor, says this was partly because they needed to build new infrastructure and tools. The server farm used IBM BladeCenter framework and BladeCenter HS20 blade servers, which are extremely dense separate computer units each with two Intel Xeon processors. Rendering took up 17 million CPU hours over a nine-month period. 

===Environmental message===
As things progress, there is increasing emphasis on environmental problems in the Antarctic. The films denouement shows a group of researchers taking video of the colony of dancing emperor penguins, and the footage is broadcast globally. After many heated arguments this publicity generates considerable pressure to stop commercial overfishing of the Antarctic.
 canary in the coal mine for this stuff. So it sort of had to go in that direction." This influence led to a film with a more environmental tone. Miller said, "You cant tell a story about Antarctica and the penguins without giving that dimension." 

===Music===
  John Powells instrumental score. They were released on October 31, 2006 and December 19, 2006, respectively.

==Reception==

===Box office===
The film opened at #1 in the United States on its first weekend of release (November 17–19) grossing $41.6 million and beating  . As of June 8, 2008,  Happy Feet has grossed $198.0 million in the U.S. and $186.3 million overseas, making about $384.3 million worldwide. Happy Feet was the third highest grossing animated film in the U.S., behind  . The film has been released in about 35 international territories at the close of 2006.   

The production budget was $100 million. 

===Critical reception===
Happy Feet received generally positive reviews. Review aggregation site Rotten Tomatoes reported that the film has a 75% fresh rating. 

===Analysis===
The film has also garnered, since its release, quite a bit of analysis and dissection from various places. Film critic Yar Habnegnal has written an essay, published in Forum on Contemporary Art and Society, that examines the themes of encroachment presented throughout the film, as well as various other subtexts and themes, such as religious hierarchy and interracial tensions.  And, Vadim Rizov of the Independent Film Channel sees Mumble as just the latest in a long line of cinematic religious mavericks.

On a technical or formal level, the film has also been lauded in some corners for its innovative introduction of Millers roving style of subjective cinematography into contemporary animation, among other things.

===Home media===
Happy Feet was released on home media on March 27, 2007  in the United States in three formats; DVD (in separate widescreen and pan and scan editions), Blu-ray Disc, and an HD DVD/DVD combo disc. 

Among the DVDs special features is a scene that was cut from the film where Mumble meets a blue whale and an albatross. The albatross was Steve Irwins first voice role in the film before he voiced the elephant seal in the final cut. The scene was finished and included on the DVD in memory of Steve Irwin. This scene is done in Steves classic documentary style, with the albatross telling the viewer all about the other characters in the scene, and the impact people are having on their environment.

==Accolades==

{| class="wikitable"
|-
! Award !! Category !! Winner/Nominee !! Result
|-
| Academy Awards Best Animated Feature
|
|  
|- American Film Institute Awards
| Honored as one of the Top Ten Best Films of the Year
|
|  
|-
| AFIs 10 Top 10
|
|  
|- Annie Awards Best Animated Feature George Miller
|  
|- Best Writing in an Animated Feature Production George Miller, John Collee, Judy Morris and Warren Coleman
|  
|-
| British Academy Childrens Awards
| Best Feature Film
|
|  
|- 60th British British Academy Film Awards Best Animated Feature Film
|
| 
|- Golden Globe Awards Best Animated Feature Film
|
|  
|- Best Original Song Song of Prince
|  
|-
| Golden Trailer Awards 
| Best Music
|
|  
|- Grammy Awards Best Score Soundtrack Album for Motion Picture, Television or Other Visual Media John Powell
|  
|- Best Song Written for Motion Picture, Television or Other Visual Media Prince
|  
|-
| 2007 Kids Choice Awards|Kids Choice Awards
| Favorite Animated Movie
|
|  
|- Los Angeles Film Critics Association Awards
| Best Animation
|
|  
|- New York Film Critics Circle Awards
| Best Animated Film
|
|  
|- Satellite Awards Best Motion Picture, Animated or Mixed Media
|
|  
|- Saturn Awards Best Animated Film
|
|  
|}

===Top ten lists===
The film appeared on numerous critics top ten lists of the best films of 2006, including AFIs Annual list, which is listed above.   AFIs jury said:

"HAPPY FEET is a one-of-a-kind motion picture experience. George Miller continues to paint outside the lines of traditional filmmaking, and his genius expands upon the animated art form to illuminate a world where penguins embrace dance and differences to survive and thrive. But that is just the tip of the iceberg, as the environment, religion and the chasm between generations enrich this sweet and subtle tale&nbsp;– one that is fun and funny, brilliant and beautiful, groundbreaking and global in its message."

*1st – Jack Matthews, The New York Daily News Sun News
*2nd – William Arnold, Seattle Post-Intelligencer
*2nd – Mark Palermo, The Coast
*3rd – Kirk Honeycutt, Hollywood Reporter
*4th – Edward Douglas, ComingSoon.net
*5th – Scott Foundas, Village Voice
*5th – Peter Rainer, Christian Science Monitor
*5th – Matthieu Santelli,  
*5th – Kurt Loder, MTV
*5th – Missy Thompson, Tooele Transcript-Bulletin
*6th – Constance Garfinkle, The Patriot Ledger
*6th – Carole Wrona,  
*6th – Lou Lumenick, New York Post
*6th – Kyle Smith, New York Post
*6th – Michael Wilmington, Chicago Tribune
*6th – Keith Cohen, Sun Newspapers
*8th – Rene Rodriguez, Miami Herald Boxoffice Magazine

==Video games==
 
 A2M and published by Midway Games. It has the same main cast as the film. It was released for the  Microsoft Windows|PC, PlayStation 2, Nintendo GameCube|GameCube, Game Boy Advance|GBA, Nintendo DS|NDS, and Wii. 

Artificial Life, Inc. has also developed a Happy Feet mobile game for the Japan market. 

==Sequels==
 
Happy Feet Two was produced at Dr. D Studios  and released on November 18, 2011. Wood and Williams reprised their roles for the sequel. Brittany Murphy, was set to reprise her role and begin recording sometime in 2010,  but died from pneumonia on December 20, 2009. Matt Damon and Brad Pitt signed on as Bill the Krill and Will the Krill respectively.    

In an interview with Collider.com, director George Miller mentioned the small possibility of Happy Feet Three, stating that if he came up with an idea for a third film, that he and his studio would produce it if they both agreed the idea was better than the first two. As of now, however, he has yet to mention if he has any ideas for a third film since the interview. 

==Happy Feet 4-D Experience==
Happy Feet 4-D Experience is a 12-minute 4D film shown at various 4D theatres over the world. It retells the condensed story of Happy Feet with the help of 3D projection and sensory effects, including moving seats, wind, mist and scents. Produced by SimEx-Iwerks, the 4D experience premiered in March 2010 at the Drayton Manor Theme Park.    Other locations included Sea World (2010&ndash;2011),    Shedd Aquarium (2010&ndash;2012),    Moody Gardens (2010&ndash;2011),    Nickelodeon Suites Resort,  and Adventure Aquarium.   

==See also== 2005 Academy Best Documentary Feature.   ).

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 