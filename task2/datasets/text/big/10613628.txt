Live for Life
{{Infobox film
| name     = Live for Life   (Vivre pour vivre)
| image    = Vivre pour vivre poster.jpg
| caption  = original film poster
| producer = {{plainlist|
* Georges Dancigers
* Alexandre Mnouchkine
}}
| director = Claude Lelouch
| writer   ={{plainlist|
* Claude Lelouch
* Pierre Uytterhoeven
}}
| starring       ={{plainlist|
* Yves Montand
* Candice Bergen
* Annie Girardot
}}
| music          = Francis Lai
| cinematography = Patrice Pouget
| editing        = Claude Barrois Claude Lelouch 
| distributor    = United Artists
| released       = 1967
| runtime        = 130 minutes
| country        = France
| language       = French
| gross          = $400,000 (USA) Tino Balio, United Artists: The Company The Changed the Film Industry, Uni of Wisconsin Press, 1987 p 231 
}} Golden Globe for Best Foreign Language Film and was nominated for the Academy Award for Best Foreign Language Film.    The film had a total of 2,936,035 admissions in France and was the 7th highest grossing film of the year. http://www.jpbox-office.com/fichfilm.php?id=9123&affich=france 

==Plot==
Yves Montand is Robert Colomb, a famous TV newscaster, married to Catherine (Annie Girardot), but continually unfaithful to her. Then he meets, and becomes fascinated with Candice (Candice Bergen). He takes her along on an assignment in Kenya and later establishes an "arrangement" with her in Amsterdam.

He is then assigned to Vietnam, tells Candice their affair is over and discovers that is more than acceptable to her as she as tired of him. Returning from a Vietnamese prison, he decides to return to Catherine, but discovers she has made a new life for herself.

==Cast==
* Yves Montand as Robert Colomb
* Candice Bergen as Candice
* Annie Girardot as Catherine Colomb
* Irène Tunc as Mireille
* Anouk Ferjac as Jacqueline
* Uta Taeger as Lucie / Maid
* Jean Collomb as Le maître dhôtel / Waiter
* Michel Parbot as Michel
* Jacques Portet as Un ami de Candice / Photographer
* Louis Lyonnet as Le chef des mercenaires

==See also==
* List of submissions to the 40th Academy Awards for Best Foreign Language Film
* List of French submissions for the Academy Award for Best Foreign Language Film

==References==
 

== External links ==
*  
*  

 
 
 

 
 
 
 
 
 
 


 
 