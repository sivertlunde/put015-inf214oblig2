The Lady in Scarlet
{{Infobox film
| name           = The Lady in Scarlet
| image_size     =
| image	         =
| caption        =
| director       = Charles Lamont
| producer       = George R. Batcheller
| writer         = Arthur Hoerl
| screenplay     = Robert Ellis & Helen Logan
| narrator       =
| starring       = 
| music          =
| cinematography = M. A. Andersen
| editing        = Roland D. Reed
| distributor    =
| released       = 1935
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

The Lady in Scarlet is a 1935 American film directed by Charles Lamont. It was made by Chesterfield Motion Pictures Corporation.

== Plot ==
In New York City, Dr. Phillip Boyer admires an antique clock at the premises of antique dealer Albert J. Sayre, who has apparently missed his appointment with him. Sayre is however, present and covertly watching Boyer. Deciding to buy and with Mrs. Sayre present, Boyer arranges with salesman Arthur Pennyward for the clock to be delivered that afternoon "about 4.30 or a little after." After Boyer departs, Sayre accuses his wife of a romantic interest in him and obliquely threatens her.

Sayre telephones his attorney Jerome T. Shelby who dismisses his suspicions about Mrs. Sayre and the "4.30 or a little after" being a coded signal for an illicit rendezvous, and asks him to bring his latest will to his office for signing.

About 5.30pm, Mrs. Sayre meets Dr. Boyer at a restaurant. Also present at the restaurant are Private Investigator Oliver Keith and his Girl Friday (idiom)|"girl friday" Ella Carey. When Boyer departs, Mrs. Sayre approaches Keith, asking for his help in finding out why a man has been watching the Sayre house and the reason for her husbands strange behavior. To reassure her, Keith escorts her home and together they find an unlocked front door, salesman Pennyward absent, and the dead body of Sayre in his office.

Soon after, Pennyward returns. He states that he left the premises about 4pm, delivering the clock to Dr. Boyers home about 5.30 and setting the time correctly to 5.35.

Police are called and establish time of death at "about 5 oclock." Mrs. Sayre says she went to her hairdressing salon at around 4pm. Questioning is interrupted by the arrival of Dr. Boyer who is returning the clock because its not the one he ordered. Questioning is again interrupted by the arrival of Sayres daughter Alice who accuses Mrs. Sayre, her stepmother, of being in love with Dr. Boyer and murdering her father for inheritance money. Dr. Boyer strongly denies any romantic interest in Mrs. Sayre. Fingerprints provide a lead to rival antique dealer F.W. Dyker but he cannot be found.

Questioning reveals that Pennyward and Alice married earlier in the day and when they told her father he fired Pennyward and told them he would disinherit her, a strong motive for murdering him before any change in the will. Boyers alibi of being at his medical practice till 5.30 is found to be false as he left about 3.30.

Dyker, Pennyward and Alice, Boyer, and Mrs. Sayre are now all under suspicion. Dyker turns up at Keiths office in response to a fake art collection ad designed to lure him there, and is arrested by Inspector Trainey. In questioning, Pennyward reveals to Keith that Sayre ran a profitable enterprise selling fake antiques through Dyker, but they argued over payments.

Mrs. Sayre admits that neither she nor Boyer has told the entire truth as they were at the restaurant not long after the time of the murder, but their relationship was platonic. She genuinely loved her husband but he was a very difficult man.

The family assembles for the reading of the will by attorney Shelby. The estate is shared equally between Sayres wife and daughter, as Sayre was murdered before signing the new will. Strangely, $100,000 in bonds which Alice knows should be hers upon marriage are not mentioned in the will. Shelby suggests the bonds may be in Sayres personal effects in the safe but when its opened by Mrs. Sayre, who alone knows the combination, the cash box is found to be empty. Alice accuses her of theft.

Arriving at Boyers medical premises to interview him, Keith, Ella, and Inspector Trainey are told his whereabouts are not known. In a side room they discover the man whos been seen watching the Sayre home. Named Quigley, hes been doped with truth serum. Recovering, he reveals he was paid by Sayre to follow Mrs. Sayre and that she and Boyer were together much earlier on the afternoon of the murder than they had admitted.

Later that evening, Dr. Boyer is found murdered in his car.

Keith makes use of a planned meeting between Pennyward and his wife and attorney Shelby to hold a meeting of his own, with Mrs. Sayre, Dyker, Quigley, Inspector Trainey, and Ella also present. He disposes of each suspects motive and alibi. Quigley is found to have not only followed Mrs. Sayre on behalf of Sayre but falsely led him to believe that she was being unfaithful, and then blackmailed him over it.

Several of the missing bonds are found in Pennywards satchel and are identified by Shelby as exactly the same type as the missing bonds, despite his remark on the day of the reading of the will that he knew nothing of their type or denomination. Caught in a lie, Shelby is accused by Keith of murdering Sayre when he visited Sayres home for the signing of the will, taking the $100,000 of bonds from the open safe, and planting some on Pennyward to implicate him. Shelby pulls a gun and fires it but is overpowered, with the bullet expected to provide ballistics evidence that he killed Boyer too.

Quigley is arrested for blackmailing both Sayre and Boyer, which had led Boyer to dope him and find out that while watching the Sayre house, Quigley had spotted Shelby entering and probably killing Sayre. Boyer had then confronted Shelby, but was killed by him. Quigley had been too afraid to mention any of this as he was guilty of blackmail and feared Shelby would kill him too.

Alice and her stepmother reconcile, Pennyward and Alice go off on their honeymoon, Dyker goes free but with his reputation even more tarnished.

== Cast == Reginald Denny as Oliver Keith
*Patricia Farr as Ella Carey Jamison Thomas (sic) as Dr. Phillip J. Boyer
*Dorothy Revier as Mrs. Julia Sayre James Bush as Arthur Pennyward
*John St. Polis as Jerome T. Shelby
*Claudia Dell as Alice Sayre/Pennyward
*John T. Murray as Albert J. Sayre
*Lew Kelly as Inspector Lewis Trainey
*Jack Adair as F.W. Dyker

== External links ==

* 

 
 
 
 
 
 
 
 
 