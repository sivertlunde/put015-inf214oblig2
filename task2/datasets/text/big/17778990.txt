A Very Special Love
 
{{Multiple issues|
 
 
 
}}

{{Infobox film
| name           = A Very Special Love
| image          = A Very Special Love.jpg
| caption        = Theatrical movie poster
| director       = Cathy Garcia Molina
| producer       = Veronique Del Rosario-Corpus( ) Charo Santos-Concio(Star Cinema) Malou N. Santos(Star Cinema)
| aproducer      = Carmi Raymundo
| writer         = Raz de la Torre 
| starring       = John Lloyd Cruz Sarah Geronimo
| music          = Jessie Lasaten Michael Lloyd
| cinematography = Manuel Teehankee
| editing        = Marya Ignacio
| distributor    = Star Cinema|ABS-CBN Film Productions Star Cinema VIVA Films
| released       =  
| runtime        = 1 hour 6 minutes
| country        = Philippines
| awards         =
| language       = Filipino / Tagalog, English
| budget         =
| gross          =₱185,235,324  
}}

A Very Special Love is a 2008 Filipino comedy romance film produced by Star Cinema and Viva Films, starring John Lloyd Cruz and Sarah Geronimo. This movie was directed by Cathy Garcia Molina and had received an “A” rating from the Cinema Evaluation Board.

The film was released on the 30th day of July 2008 and ran for eight weeks on cinemas in the Philippines. It was also released in the United States on August 15 of the same year.
This movie is the very first movie of Sarah Geronimo under Star Cinema in a lead female character after Sarah: The Teen Princess 13 years ago.
 seventh highest grossing Filipino film of all time.

==Plot==
Laida Magtalas is a modern-day Belle. "Miggy" is the youngest member of the Montenegro clan – a well-established family in the business. She applies as an Editorial Assistant at Miggys newly launched men’s magazine, "Bachelor". Laida revels working in such close proximity with the man of her dreams.

The film opens with Laida starting out her day for a job interview with Flippage, owned and managed by her crush Miguel "Miggy" Montenegro. Unbeknownst to her, on the day of her interview, Miggy was having a heated meeting with his creative team on the issue of the Bachelor, Flippage’s mens magazine. Miggy’s mean demeanor and undermining of both his friend and editors causes a walkout of half of the team. In the middle of the chaos, Laida was hired on the spot after she presented Miggy with coffee left by an attendant who also walked out after being screamed at.

By some stroke of luck, Laida lands the job. Laida remains blinded to the fact that the Miggy of her dreams is very different from the real one. In reality, he is an unapologetic, hothead who always thinks hes in the right and obsesses about nothing making his magazine number one. Everyone is scared of him with the exception of the love-struck Laida, who adamantly defends him. Imperceptively, she caters to his every whim, even sending her in his absence on a date with his girlfriend, breaking her heart a little. Her colleagues question her unrelenting devotion to such a monster and when Laida continues to proves her loyalty to Miggy, gossip regarding her feelings for the boss starts to circulate.

However the moment was short lived, as the next day after Laida made a suggestion in regards to an article content that Miggy should put for the magazine, Miggy publicly humiliates her and her knowledge of sex and questioned her virtue (virginity) in front of the staff by making her say the word “Sex”. Miggy was later embarrassed himself after he realized that it was Laida who made the last minute call and effort to find a printing press to do a rush job on their magazines overhaul. After being chastised by the despotic Miggy, Laida’s finally opens her eyes and sees him for the tyrant he truly is.

The confrontation with Laida was a rude awakening for Miggy as well and he realizes why people are so put-off by him.

Miggy then tries with much difficulty on his part of apologizing in his own way to Laida. First by ordering pizza for the team and serving a slice himself to her, which Laida ignores. Laida, now disenchanted with Miggy was called in by her work. However upon arriving in Miggy’s apartment, she found him high with fever with very little food or necessities in his apartment, with no househelp or family to care for him. Laida took care of Miggy, missing her own mothers birthday. Miggy woke up during a break in fever and saw the exhausted Laida next to him patting his back as a mother would to a sick child. This prompted Miggy to realize his feelings to Laida igniting a change in him and wishing Laida to be more closer to him as well as being more friendly with the rest of his team. The productivity and atmosphere of the company thus also changed in a positive note. Miggy also became comfortable enough to tell Laida the truth behind his past, that he was an illegitimate child of his father later adopted to the main family after his mothers death. Thus, his strong desire is to prove himself to his father and older brother. For the first time in his life, Miggy garners the gumption to apologize and this new-found humility opens up a whole new world of “firsts” for Miggy. With Laidas help, Miggy slowly learns to be more of a team player and the true value of loyalty.

==Cast and characters==

===Main cast===
* John Lloyd Cruz as Miguel "Miggy" Montenegro
* Sarah Geronimo as Adelaida "Laida" Magtalas
* Dante Rivero as Luis Montenegro
* Rowell Santiago as Arturo "Art" Montenegro
* Johnny Revilla as Roger Montenegro
* Bing Pimentel as Alice Montenegro
* Daphne Oseña-Paez as Anya Montenegro
* Al Tantay as Tomas Magtalas
* Irma Adlawan as Baby Magtalas
* Arno Morales as Stephen Magtalas
* Miles Ocampo as Rose Magtalas
* Andre Garcia as Lion Magtalas
* Matet de Leon as Zoila
* Gio Alvarez as Vincent
* Joross Gamboa as John Rae
* Paw Diaz as Mitch
* Bernard Palanca as Mondy
* Will Devaughn as Cris
* Kalila Aguilos as Violy

==Reception==
The film was huge box office success, it became the highest grossing Filipino film of 2008. It earned ₱179,235,324 in its entire run.   

==Filming locations==
* Manila, Metro Manila, Luzon, Philippines
* Marikina City, Metro Manila, Luzon, Philippines
* Pasig City, Metro Manila, Luzon, Philippines
* Tagaytay City, Cavite, Luzon, Philippines
* La Mesa Dam, Eco Park, Quezon City, Philippines

==Soundtrack Listing==
* A Very Special Love
*: Performed by Sarah Geronimo
*: Composed by Michael Lloyd
*: Published by Warner/Chappell Music Philippines
*: Originally by Maureen McGovern
* Kailan
*: Composed by Ryan Cayabyab
*: Published by Filscap
* Ngiti
*: Performed by Ronnie Liang
*: Lyrics and Music by Vince Katinday
*: Arranged by Tito Rapadas
*: Published by Universal Records

==Awards==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Work
! width="10%"| Result
|- 2009
| rowspan="4" align="left"| GMMSF Box-Office Entertainment Awards  
| align="left"| Box Office King
| align="center"| John Lloyd Cruz
|  
|- GMMSF Box-Office Box Office Sarah Geronimo || 
|- Most Popular Film Director ||align=center| Cathy Garcia Molina|| 
|- Most Popular Raz Sobida dela Torre || 
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 