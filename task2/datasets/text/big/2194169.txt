Teenage Mutant Ninja Turtles III
 
 
{{Infobox film
| name           = Teenage Mutant Ninja Turtles III
| image          = TMNT III film.jpg
| caption        = Theatrical release poster
| director       = Stuart Gillard
| producer       = David Chan Kim Dawson Thomas K. Gray
| writer         = Stuart Gillard
| based on       =   Stuart Wilson
| music          = John Du Prez
| cinematography = David Gurfinkel
| editing        = William D. Gordean James R. Symons Golden Harvest Clearwater Holdings
| distributor    = New Line Cinema   20th Century Fox  
| released       =  
| runtime        = 96 minutes
| country        = United States Hong Kong Japan
| language       = English
| budget         = $21 million
| gross          = $42.2 million (domestic)
}} Teenage Mutant Golden Harvest. This was the last Teenage Mutant Ninja Turtles film released by New Line Cinema and released on VHS along with Columbia Tristar Home Video. It was internationally distributed by 20th Century Fox. With this film, the All Effects Company provide the advanced animatronics instead of Jim Hensons Creature Shop.

==Plot==
 
In feudal Japan 1603 (Late Sengoku period) a boy is being chased by four samurai on horseback. As they go into the woods, a mysterious woman emerges from the underbrush and watches closely. However, the samurai eventually capture and take the boy, revealed to be a prince named Kenshin, with them.
 Michaelangelo is Donatello is Leonardo is Raphael is to receive a fedora, but having stormed off earlier, he is never formally given it. For Splinter (TMNT)|Splinter, she brings an ancient Japanese scepter. Back in the past, Kenshin is being scolded at by his father, Lord Norinaga, for disgracing their family name, but Kenshin argues that his fathers desire for war is the true disgrace. Their argument is interrupted by Walker, an English trader who has come to supply Norinaga with added manpower and firearms, and Kenshin leaves his fathers presence to brood alone in a temple. There, he finds the same scepter and reads the inscription: "Open Wide the Gates of Time".
 Casey Jones to watch over the lair and use the scepter to warp through time. When doing so, the turtles are replaced by four of Norinagas Honor Guards and are confused at their new surroundings.

Back in time, the turtles awake on horseback and make a poor show of riding their steeds. During the confusion, Mikey (who is carrying the scepter) ends up riding off alone into the forest and gets ambushed by an unknown assailant. The others go to search for April at Norinagas palace, where their identity as Honor Guards allows them cover in their search. After following one of Walkers thugs into the prison, the turtles rescue April and also free another prisoner named Whit (locked up for trying to start a mutiny against Walker, and who bears a striking resemblance to Casey), but their sloppy escape ends up leaving them all alone in the wilderness and without a clue where to go. Meanwhile, in the present, Kenshin is getting impatient and anticipates a fight from Casey. Casey instead introduces him and the Honor Guards to television hockey, which manages to calm them down for the time being.

Out in the woods, the turtles, April, and Whit are again attacked, this time by villagers mistaking them for Norinagas forces. The attack stops when Mitsu, leader of the rebellion against Lord Norinaga, unmasks Raphael and sees that he looks just like one of her prisoners. The turtles realize that she is talking about Mikey and accompany Mitsu to her village. When they arrive, the village is being burned down by Walkers men. As the turtles help the villagers save it, Mikey is let out by a pair of clueless soldiers and joins in the fight. Walker is forced to retreat, but the fire continues to burn and has trapped a young boy named Yoshi inside a house. Michelangelo saves Yoshi from the fire, then Leonardo helps him recover by performing CPR.

As Walker continues bargaining with Lord Norinaga over buying guns in exchange for gold, the turtles spend some time in the village. Donatello decides to have a replica scepter made so they can get back home, while Michaelangelo teaches some of the people about pizza and later tries to console Mitsu about Kenshin, whom she is in love with. Raphael also gets in touch with his sensitive side through the child Yoshi, ironically being the one who teaches Yoshi on how to control his temper. Back in the present, the Honor Guards from the past are quickly adjusting to life in the 20th Century, and Casey decides to challenge them to a hockey game. To Caseys dismay, the Honor Guards think hockey is about beating up each other. Meanwhile, Kenshin and Splinter show fear that the ninja turtles will not return home in time before their sixty hours are up.

In the past, the replica scepter is completed, but an argument between Michelangelo and Raphael ends up breaking it. To make matters worse, Mitsu informs them that Lord Norinaga has agreed to purchase Walkers guns and will attack the village in the morning. When Raphael sneaks off to visit Yoshi, however, he is surprised to find the original scepter in the childs possession. The turtles are overjoyed to see it but are angry at Mitsu for hiding it and essentially forcing them to fight her war, however, Mitsus father clarifies that it was his idea to have the turtles fight in her place.

Suddenly, Whit betrays everybody and captures Mitsu, and the turtles return to Norinagas palace to save her. After rescuing her, they are cornered by Norinaga and are made to fight waves of his soldiers. The turtles respond by freeing the prisoners in the palace, starting an all-out war on the palace grounds. After a while of fighting, Leo defeats Lord Norinaga in a heated sword duel, comedically finishing him by cutting his hair and then trapping him inside of a bell. Deciding to cut his losses, Walker takes the scepter and tries to escape to his boat. When cornered by the turtles at the dock, Walker throws the scepter into the air as a distraction. The turtles catch the scepter, while Whit launches a catapult at Walker and knocks him off the dock to his death.

The turtles are now ready to return to their own time, but Mikey says hed rather stay (in particular because he wanted to be with Mitsu). Raphael decides he wants to stay as well because he feels like the Turtles are appreciated in Japan unlike back home. The other turtles and April try to convince them otherwise until Kenshin activates the scepter and makes the decision harder. After a long debate (which included Mitsu telling Mikey to keep his promise about Kenshin returning to the past), Michelangelo reluctantly agrees to go home with his brothers, but just barely misses grabbing the scepter in time. The Honor Guards switch back with the Turtles (all except for Michelangelo). Fortunately, the last remaining Honor Guard activates the scepter and swaps places with Mikey just before the scepter burns out.

In the past, Norinaga admits surrender to Mitsu and Kenshin, and the two lovers share a tender reunion. Michaelangelo, meanwhile, is depressed over the thought of growing up, but Splinter cheers him up by performing the "lampshade Elvis" impression, and the rest of the turtles join in with a final dance number.

==Cast==

===Live actors===
*Paige Turco as April ONeil Casey Jones / Whit James Murray Splinter
*Stuart Stuart Wilson as Walker
*John Aylward as Niles
*Sab Shimono as Lord Norinaga
*Vivian Wu as Mitsu
*Henry Hayashi as Kenshin
*Travis A. Moon as Yoshi Leonardo
*David David Fraser Michaelangelo
*Jim Donatello
*Matt Raphael

===Voice cast===
*Brian Tochi as Leonardo
*Robbie Rist as Michaelangelo
*Corey Feldman as Donatello Tim Kelleher as Raphael

Rist and Tochi (who did the voices of Michaelangelo and Leonardo, respectively) are the only two voice actors to voice the same character throughout all three live-action TMNT movies. However, Corey Feldman voiced Donatello in both this and the first movie.

==Reception==
Reviews for the film have been mostly negative by critics. Based on a sample of 28 reviews, the film holds a 21% "rotten" rating on Rotten Tomatoes with the consensus "Its a case of one sequel too many for the heroes in a half shell, with a tired time-travel plot gimmick failing to save the franchise from rapidly diminishing returns."  It was poorly received by the LA Times as well. 
 Shredder or Krang. James Berardinelli gave it one out of four stars, citing that "any adults accompanying their kids will have to invent new and interesting ways to stay awake. Not only is this movie aimed at young children, the script could have been written by them."  TV Guide gave it two out of four stars and said in their review, "If the time-travel gimmick has to be employed twice in a row then its probably best to banish these characters to a retirement sewer,"  when commenting about a possible future film invoking time travel.

Despite most of the reviews from critics, Teenage Mutant Ninja Turtles III debuted at No.1 at the box office.  

As with the both previous films, the British PG version was censored due to usage of forbidden weapons (Michelangelos nunchaku). For these scenes, alternate material was used. The cuts were waived for the DVD release. 
The German theatrical and video version was based on the censored UK cut; the DVD is uncut.

==Home media releases==
The film was released to VHS in 1993.  The film was released to DVD in Region 1 on September 3, 2002; it contained only minor special features and interactive menus.

On August 4, 2009, the film was included in a special 25th-anniversary boxset, released to both DVD and  , and  .

==References==
 
 
*   Rotten Tomatoes. Retrieved January 14, 2005.
*   Box Office Mojo. Retrieved January 14, 2005.

 

==External links==
 
 
*  
*  
*   at Rotten Tomatoes
*   at the Official Ninja

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 