Mankurt (1990 film)
:This article is for the 1990 film. For information on the term "mankurt", please see the article Mankurt 
{{Infobox film
| name           = Mankurt  (Russian: Манкурт) 
| image          = Mankurt 1990.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Khodzhakuli Narliyev
| producer       = 
| writer         = Mariya Urmatova
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Tarık Tarcan Maya-Gozel Aymedova Yılmaz Duru Khodzhadurdy Narliev Maysa Almazova
| music          = Redzhep Redzhepov
| cinematography = Nurtai Borbiyev
| editing        = 
| studio         = Turkmenia Studios Tugra Film 
| distributor    = 
| released       = 1990  (Soviet Union) 
| runtime        = 86 minutes
| country        = Turkmenistan USSR 
| language       = Russian
| budget         = 
| gross          =
}}
 1990 Soviet Soviet film written by Mariya Urmatova  and the last film directed by Khodzha Narliyev.  The main cast were Tarik Tardzhan, Maya-Gozel Aymedova, Jylmaz Duru, Khodzhadurdy Narliev, and Maysa Almazova.         

==Background== Turkic legend slave of Djungar masters.

==Synopsis==
The film is about a Turkmenian who defends his homeland from invasion. He is captured, tortured, and brainwashed into serving his homelands conquerors. He is so completely turned that he kills his mother when she attempts to rescue him from captivity.

==Cast==
 
 
* Maya-Gozel Aimedova
* Nurberdy Allaberdyyev
* Maysa Almazova
* Baba Annanov
* Kerim Annanov
* Yılmaz Duru
* Altyn Khodzhayeva
 
* Khommat Mullyk
* Khodzha Durdy Narliyev
* Takhyr Narliyev
* Mergen Niyazov
* Maya Nuryagdyyeva
* Sapar Odayev
* Tarık Tarcan
 

==See also==
* Mankurt
* The Day Lasts More Than a Hundred Years#Mankurt|The Day Lasts More Than a Hundred Years (Mankurt)
* Pitchcapping

==References==
{{reflist|2|refs=
   

   

   

   

   

   

 {{cite news
|url=http://www.mustafacetin.org/tr/kole-mangutlar-ve-kaprisli-develermankurt-filminini-kakadokya-cekimleritempo-dergisi18081998s6667
|archiveurl= http://www.webcitation.org/5wg55Fpo2
|archivedate= 21 February 2011
|title=DR. MUSTAFA ÇETİN,FUAT UZKINAY,CENGİZ DAĞCI,CENGİZ AYTMATOV KÖLE  
|last=staff
|date=August 18, 1988
|work=Kultur Sanat
| pages=66, 67
|language=Turkish
|accessdate=9 February 2011}} 

 {{cite news
|url=http://www.mustafacetin.org/tr/kultur-kokunuzu-ve-torenizi-untmayinmankurt-filmi-cekimleriolcay-yaziciturkiye-gaz1708198811
|archiveurl= http://www.webcitation.org/5wg5f66NZ
|archivedate= 21 February 2011
|title=DR. MUSTAFA ÇETİN,FUAT UZKINAY,CENGİZ DAĞCI,CENGİZ AYTMATOV KÜLTÜR 
|last=staff
|date=August 17, 1988
|work=mustafacetin.org
| pages=66, 67
|language=Turkish
|accessdate=9 February 2011}} 

 {{cite news
|url=http://www.mustafacetin.org/tr/gun-uzar-yuzyil-olur-film-oluyoryeni-dusunce02101987
|archiveurl= http://www.webcitation.org/5wg64K0Kk
|archivedate= 21 February 2011
|title=GÜN UZAR YÜZYIL OLUR FİLM OLUYOR,YENİ DÜŞÜNCE
|last=staff
|date=October 2, 1987
|work=mustafacetin.org
| pages=
|language=Turkish
|accessdate=9 February 2011}} 

 {{cite news
|url=http://www.mustafacetin.org/tr/mankurt-bird-memorymankurt-filminin-sinopsis-ve-tanitimisdtarihsiz
|archiveurl= http://www.webcitation.org/5wg6GvHht
|archivedate= 21 February 2011
|title=MANKURT -BIRD MEMORY,"MANKURT" FİLMİNİN SİNOPSİS VE TANITIMI,SD
|last=
|date=February 2, 1988
|work=mustafacetin.org
| pages=
|language=Turkish, English & French
|accessdate=9 February 2011}} 

 {{cite news
|url=http://www.mustafacetin.org/tr/sinemada-turksovyet-isbirligimilliyet07021988
|archiveurl= http://www.webcitation.org/5wg6dRrMz
|archivedate= 21 February 2011
|title=SİNEMADA TÜRK-SOVYET İŞBİRLİĞİ,MİLLİYET
|last=
|date=October 2, 1988
|work=mustafacetin.org
| pages=
|language=Turkish
|accessdate=9 February 2011}} 
}}

==External links==
*   at the Internet Movie Database
 

 
 
 
 