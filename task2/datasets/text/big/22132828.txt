So Big (1953 film)
{{Infobox film
| name           = So Big
| image          = So Big! (1932 film).jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Robert Wise
| producer       = Henry Blanke
| writer         = John Twist
| based on     =  
| narrator       = Steve Forrest Nancy Olson
| music          = Max Steiner
| cinematography = Ellsworth Fredericks
| editing        = Thomas Reilly
| studio         =
| distributor    = Warner Bros.
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $2 million (US) 
}}
 1953 Cinema American drama Steve Forrest, directed by Robert Wise. Variety Film Reviews|Variety film review; September 30, 1953, page 6.  Harrisons Reports and Film Reviews|Harrisons Reports film review; October 3, 1953, page 159. 
 1924 novel 1924 silent So Big! with Barbara Stanwyck, released in 1932.

==Plot==
 

In the late 1890s, boarding school student Selina Peake learns of the death of her father, who has left her penniless as the result of bad business transactions. August Hempel, the father of her best friend Julie, secures her a teaching position in New Holland, a small Dutch farming community outside Chicago. There she rents a room in the home of Klaas Pool, who lives with his unhappy wife Maartje and intelligent but troubled adolescent son Roelf.

After discovering the boy has an ear for music, Selina gives Roelf piano lessons and encourages his artistic talents, leading him away from juvenile delinquency. She meets Pervus DeJong and eventually accepts his proposal of marriage, settling into the daily routine of a farmers wife. The two have a son they christen Dirk, but as he grows older he is nicknamed So Big.

Dirk, like Roelf, exhibits signs of artistic abilities. His mother encourages him, despite her husbands disapproval. When the boy is eight years old, Pervus dies and Selina struggles to run the farm alone. She and Dirk travel to the Chicago Haymarket to sell their produce, but no one there will deal with a woman.

Facing financial difficulties, Selina reunites with Julie, now the divorced mother of two. Her father offers to invest in Selinas plan to grow exotic vegetables. Her asparagus is a huge success and she is able to send Dirk to college to study architecture. Following graduation, he finds employment as a draughtsman and continues his relationship with Julies daughter Paula, a social climber who convinces him to forgo his career in favor of becoming a more immediate financial success. Dirk accepts a job in sales, disappointing his mother so much, she no longer calls him So Big.

Dirk falls in love with artist Dallas OMara and proposes marriage. Although she likes him, she declines his offer, telling him she wants a man whose hands have been roughened by physical labor. Roelf, now a famous composer, arrives in Chicago and reunites with Dallas, whom he had met in Paris. He, Dallas, and Dirk visit Selina, and the two women become friends.

Roelf and Dallas leave together, and Dirk bemoans the path his life has taken. His mother tells him it is not too late to pursue his creative dreams. Taking him in her arms, she calls him So Big once again.

==Cast==
*Jane Wyman ..... Selina DeJong 
*Sterling Hayden .... Pervis DeJong  Steve Forrest ..... Dirk DeJong 
*Nancy Olson ..... Dallas OMara 
*Walter Coy ..... Roelf Pool 
*Elisabeth Fraser ..... Julie Hempel
*Jacques Aubuchon ..... August Hempel  
*Martha Hyer ..... Paula Hempel 
*Roland Winters ...  Klaas Pool 
*Ruth Swanson ..... Maartje Pool
*Lillian Kemble-Cooper .... Mrs. Fister 
*Richard Beymer ..... Roelf, ages 12–16 
*Tommy Rettig ..... Dirk, age 8

==Critical reception==
Bosley Crowther of The New York Times wrote, Despite the fact that the story is "dated," that it offers a very familiar theme and that the present production is loaded with some typical "Hollywood" flaws, there is still a great deal of solid substance in this latest retelling of the tale . . . For the better part of the picture, we are largely indebted to Jane Wyman . . . remarkably strong and effective in every forthright little bit she does. From her earliest appearance . . . to her ultimate poignant scene . . . she is humble, restrained and sure . . . Where the picture falls down is in those phases in which the grown-up son goes to Chicago and gets himself infected with false notions of a successful career. Here John Twists adaptation and Mr. Wises direction are unsure, and the acting by all and sundry has a specious and artificial quality . . . As a consequence of its downward tangent toward the end, the whole picture loses force. The poignance of the mothers disappointment in her dearly loved son goes awry. Even so, the performance of Miss Wyman holds up right through to the end, and the character she sets throughout the first part stands firm amid the flow of glycerin tears.  

Radio Times rated the film three out of five stars and commented, "The tales a mite old-fashioned for todays tastes, but under skilled editor-turned-director Robert Wise the Warner Bros gloss is not too intrusive, and there are nicely honed performances from big Sterling Hayden and lovely Nancy Olsen in support."  

TV Guide rated the film three out of four stars and added, "A good production, but a bit inflated, So Big commits the same error as the 1932 picture in that it stuffs a quarter of a century and many characters into not enough time."  

==Release==
There is no record that a VHS or DVD version of the film has been released; So Big was released on VHS then the copies mysteriously disappeared.

==Awards and nominations== Richard Egan.
==Adaptations to Other Media== Studio Ones December 29, 1947 broadcast with Joan Blondell and Lux Radio Theaters September 21, 1954 broadcast with Ida Lupino and Robert Stack.

==References==
 

==External links==
* 
*   Tvcinemateatro-i protagonisti

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 