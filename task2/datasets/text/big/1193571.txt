Turn of the Tide
{{Infobox film
| name           = Turn of the Tide
| image          =
| image_size     =
| caption        = Norman Walker
| producer       = John Corfield
| writer         = Leo Walmsley (book)   L. du Garde Peach   J.O.C. Orton 
| narrator       = Wilfrid Lawson   Moore Marriott
| music          = Arthur Benjamin
| cinematography = Franz Planer
| editing        = Ian Dalrymple   Stephen Harrison   David Lean
| studio         = British National Films
| distributor    = Gaumont British Distributors
| released       = 1935
| runtime        = 80 minutes
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} British drama Norman Walker Wilfrid Lawson, and was the first feature film made by J. Arthur Rank.
 Wilfrid Lawson speak in the local accent. The work is based on the novel Three Fevers by Leo Walmsley.

==Cast==
* John Garrick as Marney Lunn 
* J. Fisher White as Isaac Fosdyck 
* Geraldine Fitzgerald as Ruth Fosdyck  Wilfrid Lawson as Luke Fosdyck 
* Moore Marriott as Tindal Fosdyck 
* Sam Livesey as Henry Lunn 
* Niall MacGinnis as John Lunn 
* Joan Maude as Amy Lunn 
* Derek Blomfield as Steve Lunn 
* Hilda Davies as Mrs. Lunn

==Reception==
Although the film was originally considered a box office disappointment it was eventually voted the sixth best British movie of 1936 
==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 