Pyari Behna
{{Infobox film
 | name = Pyari Behna
 | image = PyariBehnaa.jpg
 | caption = Promotional Poster Bapu
 | producer = B. Loganathan
 | writer = Umachandran
 | dialogue = 
 | starring = Mithun Chakraborty Padmini Kolhapure Tanvi Azmi Vinod Mehra Shakti Kapoor Master Ajay Devgan
 | music = Bappi Lahiri
 | lyrics = Anjan Indivar
 | cinematographer = Baba Azmi
 | associate director = 
 | art director = 
 | choreographer = 
 | released =  July 12, 1985 (India)
 | runtime = 130 min.
 | language = Hindi Rs 2.8 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1985 Hindi Indian feature directed by Tamil film Mullum Malarum starring Rajinikanth. The film also had Ajay Devgan playing the young Mithun Chakraborty.

==Plot==

Pyari Behna is family drama, featuring Mithun Chakraborty, Padmini Kolhapure, Vinod Mehra, Tanvi Azmi and Shakti Kapoor

==Summary==

Kaali and his sister Seeta lives in a shanty house. He assists the Engineer Vinay. Mangla, Seetas friend falls in love with Kali and they gets married much to the delight of his sister. Now Kali meets with an accident and loses his left hand. He blames Vinay for the accident. Now Vinay falls in love with Kaalis sister Seeta but Kaali is against their relationship. Will the true love triumphs forms the climax.?

==Cast==

*Mithun Chakraborty ...  Kalicharan Kaali 
*Padmini Kolhapure ...  Mangla 
*Tanvi Azmi ...  Seeta 
*Vinod Mehra ...  Vinay Verma 
*Shakti Kapoor ...  Nekiram Chaturvedi 
*Deven Verma ...  Makhan Singh - Tanvis prospective groom 
*Ajay Devgan ...  Young Kalicharan (as Master Chotu) 
*Huma Khan ...  Banjaran 
*Meenakshi ...  Chameli 
*Nalini ...  Bijli 
*Nirmala ...  Champa 
*Asha Shah ...  Manglas mom 
*Mini Tabassum ...  Young Seeta

==Trivia==
Ajay Devgan played the young Mithun Chakraborty in this film.    

==References==
 
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Pyari+Behna

==External links==
*  

 
 
 
 
 
 