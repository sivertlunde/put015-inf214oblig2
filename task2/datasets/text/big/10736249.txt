The Last Days of Pompeii (1926 film)
{{Infobox film 
| name=Gli ultimi giorni di Pompei
| image= GliultimigiornidiPompei.jpg
| director=Carmine Gallone  Amleto Palermi
| writer=Edward George Bulwer-Lytton  Alfredo Panzini (titles) 
| starring= Victor Varconi
| cinematography = Alfredo Donelli
| producer=
| distributor=Società Italiana Grandi Films
| released= 9 March 1926 
| runtime=
| language=Silent film
| country=Italy
| budget=
}}
 Italian historical silent drama colorized by the Pathechrome stencil color process.

==Cast==
*Victor Varconi as Glauco 
*Rina De Liguoro as Ione 
*María Corda as Nydia, the blind flower seller
*Bernhard Goetzke as Arbace 
*Emilio Ghione as Caleno 
*Lia Maris as Julia 
*Gildo Bocci as Diomede 
*Enrica Fantis as Julias friend 
*Vittorio Evangelisti as Apecide 
*Ferruccio Biancini as Olinto 
*Carlo Gualandri as Clodio 
*Vasco Creti as Sallustius 
*Alfredo Martinelli as Lepidus 
*Giuseppe Pierozzi as Josio 
*Enrico Monti as Lidone 
*Enrico Palermi as Medone 
*Carlo Reiter as Pansa 
*Carlo Duse as Burbo 
*Osvaldo Genazzani   
*Italia Vitaliani   
*Paola Dria   
*Donatella Neri

==See also==
* List of early color feature films

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 

 
 