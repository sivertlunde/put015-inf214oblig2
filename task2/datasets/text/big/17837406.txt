Mother o' Mine
 
{{Infobox film
| name           = Mother o Mine
| image          = Mother o Mine (1921).jpg
| caption        = Film still with Betty Ross Clarke, Lloyd Hughes, Claire McDowell
| director       = Fred Niblo
| producer       = Thomas H. Ince
| writer         = C. Gardner Sullivan
| based on       =  
| starring       = Lloyd Hughes Betty Ross Clarke Henry Sharp
| editing        =
| distributor    = Associated Producers
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         =
}}

Mother o Mine is a 1921 American  .   

==Plot==
As described in a film publication,    several years earlier Mrs. Sheldon (McDowell) had been deserted by her husband. She brought up her son Robert (Hughes) in the belief that his father was dead. His desire to make good in the city leads his mother to send him to his father, Willard Thatcher (Kilgour). Unknown to him, Robert is now working for his own father, and all goes well until he learns of his fathers nefarious financial schemes. They end up fighting, and Willard tells Robert that while he is married to his mother, Robert is not his son. Willard is accidentally killed, and on the evidence of Fan Baxter (Blythe), Willards woman, Robert is condemned. A last minute forced confession from Fan by Roberts mother saves the day.

==Cast==
* Lloyd Hughes - Robert Sheldon
* Betty Ross Clarke - Dolly Wilson
* Betty Blythe - Fan Baxter
* Joseph Kilgour - Willard Thatcher
* Claire McDowell - Mrs. Sheldon
* Andrew Robson - District Attorney Andrew Arbuckle - Henry Godfrey

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 

 