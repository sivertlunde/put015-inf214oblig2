Kuxa Kanema: The Birth of Cinema
{{Infobox film
| name           = Kuxa Kanema: The Birth of Cinema
| image          = 
| caption        = 
| director       = Margarida Cardoso
| producer       = Filmes do Tejo, Laspus, Derives
| writer         = 
| starring       = 
| distributor    = 
| released       = 2004
| runtime        = 52 minutes
| country        = Mozambique Portugal
| language       = 
| budget         = 
| gross          = 
| screenplay     = Margarida Cardoso
| cinematography = Lisa Hagstrand
| editing        = Isabelle Rathery
| music          = 
}} 2003 Documentary documentary by Margarida Cardoso on the National Institute of Cinema (INC), created by President Samora Machel following the 1975 independence of Mozambique.  

At the time of independence, Mozambique lacked a national television network, so a newsreel program was the only way to reach the population through visual media.  The first cultural act of President Machel’s government was the establishment of the  Kuxa Kanema weekly newsreel program.  The newsreels were shown in Mozambique’s relatively few cinemas in the 35mm format.  In the rural areas, mobile units provided by the Soviet Union offered the newsreels in the 16mm format. Machels government, however, did not finance or encourage the creation of a film industry designed strictly for entertainment value.    

The film also details the struggle to get INC up and running: Mozambique had no film industry or film schools (Brazilian and Cuban instructors were flown in to offer on-the-spot educational training). The resulting newsreels were shot in black-in-white because color film was too expensive.

Machels death in an airplane crash in 1986, coupled with guerrilla attacks launched by the apartheid governments of South Africa and Rhodesia that destabilized Mozambique to the point of civil war, helped to kill this program.  Many of the cinemas in the cities were damaged in the civil war, while the rural presentations of Kuxa Kanema were cancelled due to the lack of safe passage along rural roads.  By the end of the conflict, national television had been established and there was no call for a theatrical newsreel presentation. 

Kuxa Kanema: The Birth of Cinema offers clips of rarely seen footage from the INC films. It also looks into the offer by French filmmaker Jean-Luc Godard to create a national independent television network. 

Kuxa Kanema: The Birth of Cinema has played in film festivals and has been released on DVD. 

==See also==
*Estas são as armas, a 1978 National Cinema Institute documentary

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 