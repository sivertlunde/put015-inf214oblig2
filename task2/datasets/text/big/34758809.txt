Alyam Alyam O les jours aka Oh the days
 

{{Infobox film
| name           = Alyam Alyam O les jours aka Oh the days
| image          = Alyam_Alyam.jpg
| image size     = 
| alt            = 
| caption        = 
| director       =  
| producer       =  , Rabii Films Productions
| writer         = 
| narrator       = 
| starring       = Abdelwahad y familia, Tobi, Afandi Redouane, Ben Brahim
| distributor    =  
| released       = 1978 
| runtime        = 90 minutes
| country        = Morocco
| language       = Arabic with English and French subtitles 
| source         = Mars / Lux Films 
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
| screenplay     = Ahmed El Maanouni
| cinematography = Ahmed El Maanouni
| sound          = Ricardo Castro
| editing        = Martine Chicot
| music          = Nass El Ghiwane
}} Moroccan drama film.

== Synopsis ==
Following his father’s death, Abdelwahad, a young man, has to take his place as head of the family. His presence is crucial to the family unit, especially as he has to provide for his seven brothers. Hlima, his mother and a woman of exemplary strength and nature, also fully plays her role. When Abdelwahad tells her that he wishes to leave to work in France, she tries to talk him out of it. He no longer can bear the life of young people in the countryside. He refuses to be a poor man without a future and applies for a work permit in France.

== Awards ==
* Grand prize 1978 Mannheim-Heidelberg International Film Festival
* Taormina 1978
* FESPACO 1979
* CICAE
* Cartago
* Damasco
* FIFEF

== References ==
 
*  

== External links ==
*   on Cinefiches.com
*   on  critikat.com

 
 

 