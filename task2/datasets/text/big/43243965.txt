Kshana Kshana
{{Infobox film
| name = Kshana Kshana
| image =
| caption = 
| director = Sunil Kumar Desai
| writer = Sunil Kumar Desai
| based on = 
| producer = B. R. Thirumalai Aditya  Prema  Vishnuvardhan
| music = R. P. Patnaik
| cinematography = R. Prabhakar
| editing = B. S. Kemparaju
| studio  = Trans India Films Division 
| released =  
| runtime = 154&nbsp;minutes
| language = Kannada
| country = India
}}
 2007 Indian Prema and Vishnuvardhan appearing in a special role. 

The film released on 1 June 2007 across Karnataka to positive response from the critics. However upon release, the film generally met with average reviews from the audience. 

==Plot==
Samarth returns from Switzerland to India and eventually gets appointed as a bodyguard to famous actress Maya. He wins over the confidence of Maya and Phalguni along with their family. His real purpose of visit to India is to trace the psychiatrist killer of his brother and thinks that Maya has some special relationship with the killer. He manages to almost convince her that she might be the killer of his brother when a don Kishore who is after Maya kidnaps her and keep her in a remote place. Meanwhile, her friend Ananth who is aware of this entire incident is also abducted. Then enters a specially appointed DCP Vishnu who interrogates everyone involved in this case.

== Cast == Aditya as Samarth
* Kiran Rathod as Maya Prema as Phalguni
* Dilip Raj as Ananth Kishore as Kishore
* Ashutosh Rana as Rana
* Sridevika
* Sowmya
* Nagesh
* Ramesh
* Uday Jadugar
* Vishnuvardhan (actor)|Dr. Vishnuvardhan in a guest appearance as DCP Vishnu

== Soundtrack ==
{{Infobox album  
| Name        = Kshana Kshana
| Type        = Soundtrack
| Artist      = R. P. Patnaik
| Cover       = 
| Released    = 2007
| Recorded    = 
| Genre       = Film soundtrack
| Length      =
| Label       = Anand Audio
| Producer    = R. P. Patnaik
| Reviews     =
| Last album  = 
| This album  = 
| Next album  = 
}}

{{tracklist
| headline = Track listing
| all_music = R. P. Patnaik
| lyrics = yes
| extra_column = Singer(s)
| title1 = I Love You
| lyrics1 = K. Kalyan
| extra1 = Rajesh Krishnan, Shreya Ghoshal
| length1 = 03:46
| title2 = Madira Madhura Kaviraj
| extra2 = KK (singer)|KK, Malgudi Subha
| length2 = 05:28
| title3 = Ondu Kotre Eradu Kodtheeni
| lyrics3 = K. Kalyan
| extra3 = Udit Narayan, K. S. Chithra
| length3 = 03:56
| title4 = Love Love
| lyrics4 = K. Kalyan
| extra4 = Chaitra H. G.
| length4 = 04:42
| title5 = Ee Kshana Gharshana
| lyrics5 = Kaviraj
| extra5 = Rajesh Krishnan, Sunitha Upadrashta
| length5 = 04:59
| title6 = Kshana Kshana
| lyrics6 = V. Nagendra Prasad
| extra6 = Usha Uthup
| length6 = 04:15
}}

== References ==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 