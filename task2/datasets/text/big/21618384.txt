Promise the Moon
 

{{Infobox Film |
  name           = Promise the Moon |
  image          = Promise the Moon DVD.jpg |
  caption        = DVD cover |
  writer         = Randall Beth Platt Kevin Sullivan|
  starring       = Henry Czerny Colette Stevenson Shawn Ashmore |
  director       = Ken Jubenville |
  producer       = Trudy Grant Kevin Sullivan |
  distributor    = Four Arrows Productions Inc. Sullivan Entertainment |
  released       = 1997|
  runtime        = 97 mins |
  language       = English |
  music          = |
  awards         = Gemini Award|
  budget         = |
}} 1997 film Kevin Sullivan, based on the book The Four Arrows Fe-As-Ko by Randall Beth Platt.  The script was adapted by Kevin Sullivan and Peter Behrens, and directed by Ken Jubenvill.

The film won a Gemini Award for Best Costume Design. 

==Synopsis== David Fox) a local, cut throat businessman, is plotting to take over the ranch. Roy thinks the case is hopeless until Butler’s former secretary Jane Makepeace (Colette Stevenson) joins forces with Roy to keep her greedy ex-employer at bay. The prim and proper Jane brings order to the household and discovers that Levi can’t speak because he is deaf. Jane and Roy join forces to teach Levi to communicate and work on the ranch and an unlikely romance blossoms between them. In the end, in spite of their differences, Roy, Jane and Levi defeat Butler and forge a family in the process. 

== Cast List ==
*Henry Czerny – Royal Leckner
*Colette Stevenson – Jane Makepeace
*Shawn Ashmore – Leviatus Bennett David Fox – Sir Robert Butler
*Ricard Donat – Wilbur Bennett
*Aidan Devine – James Bennett
*Gloria May Eshkibok – Sophie Twelvetrees 

==External links==
* 
*  - Official Promise the Moon Page

==References==
 

 

 
 
 
 
 
 
 


 