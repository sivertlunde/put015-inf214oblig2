Édith et Marcel
 
{{Infobox film
| name           = Édith et Marcel
| image          = Édith et Marcel.jpg
| caption        = 
| director       = Claude Lelouch
| producer       = 
| script         = 
| starring       = Évelyne Bouix Marcel Cerdan Jr
| music          = Francis Lai
| cinematography = 
| editing        = 
| studio         =
| distributor    = 
| released       =  
| runtime        = 140 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
 French film directed by Claude Lelouch.

==Plot==
In 1947, the singer Édith Piaf and the boxer Marcel Cerdan are both at the peak of their respective careers. Their encounter gives birth to a passionate love affair lasting some two years, cut short by Cerdans death in an air crash.

==Technical details==
*Title: Édith et Marcel
*Director: Claude Lelouch
*Music: Francis Lai
*Length: 140 minutes
*Release date: April 13, 1983

==Cast==
{|style="border:0px;width:800px;"
|-
| width="50%" |
*   / Margot de Villedieu
* Jacques Villeret : Jacques Barbier
* Francis Huster : Francis Roman
* Jean-Claude Brialy : Loulou Barbier
* Jean Bouise : Lucien Roupp
* 
*Charles Gérard : Charlot
*Charlotte de Turckheim : Ginou Richet
*Micky Sébastian : Marinette Cerdan
*Maurice Garrel : le père de Margot
*Ginette Garcin : Guite
*Philippe Khorsand : Jo Longman
*Jany Castaldi : Momône
*Candice Patou : la sœur de Margot
*Tanya Lopert : Le professeur danglais
*Charles Aznavour : himself
| width="50%" |
*Jean Rougerie
*Beata Tyszkiewicz
*Fouad Sakete
*Stéphane Ferrara
*Dominique Benkouai
*Michel Chapier
*Yveline Ailhaud
*Pierre Aknine
*Mireille Audibert
*Jean-Pierre Bacri
*Marc Berman
*François Bernheim
*Corinne Blue
*Jean-Claude Bourbault
*Michel Aumont
|}

==Trivia==
* In the film, the songs of Édith Piaf are sung by Mama Béa Tékielski.
* Marcel Cerdan is played in the film by his son, Marcel Cerdan Jr.

==External links==
*  

 

 
 
 
 
 
 

 