Asokamala
{{Infobox film| name = Asokamala
 | image =
 | imagesize =
 | caption =
 | director = Shanthi Kumar / T. R. Goppu
 | producer = Sir Chittampalam A. Gardiner
 | screenplay = Shanthi Kumar
 | starring = Shanthi Kumar, Emaline Dimbulana
 | music = D. T. Fernando (lyrics) Mohammed Ghouse (music)
 | cinematography =
 | editing =
 | distributor =
 | country    = Sri Lanka
 | released =   April 9, 1947
 | runtime = Sinhala
 | budget =
 | followed_by =
}}

Asokamala is a 1947 Sri Lankan historical film. It was the second film made in the Sinhalese language and the first by a Sinhalese director. It also was the first film that W. D. Albert Perera (later known as Amaradeva), Mohideen Baig and Mohammed Ghouse contributed to. The melodies of the songs of Asokamala were original ones devised by Mohamed Ghouse- a departure from what was to be the copying of Indian melodies in the time to come.

==Plot==
Prince-heir Saliya gives up the throne to marry the commoner Asokamala.

Cast
*Shanthi Kumar as Saliya
*Emilyn Dimbulana as Asokamala
*W. D. Albert Perera ( Pandith W.D.Amaradewa) as Thapasaya (hermit)

==Songs==
*"Bhave beetha hera" &ndash; W. D. Albert Perera ( Pandith W.D.Amaradewa)
*"Prithi Prithi" &ndash; Bhagyarathi and chorus
*"Ho Premey Babaley" &ndash; Mohideen Baig and Bhagyarathi
*"Shantha Mey Rathriye" &ndash; Bhagyarathi
*"Aaley Mage" &ndash; Mohideen Baig
*"Ai Kare Yamak Aley" &ndash; W. D. Albert Perera ( Pandith W.D.Amaradewa)
*"Jale Mey Dane" &ndash; Bhagratathi
*"Nayana Wani Sudo" &ndash; Mohideen Baig and Bhagyarathi
*"Aloke Dey" &ndash; Bhagyarathi
*"Suduwa Jaya Sri" &ndash; Mohideen Baig and chorus

==External links==
* 

 
 


 