Bhagyalakshmi Bumper Draw
 
 

{{Infobox Film
| name = Bhagyalakshmi Bumper Draw
| image =
| image_size =
| caption =
| director = Nidhi Prasad
| producer =
| writer =
| narrator = Rajendra Prasad Ali Richard Venu Madhav Nagababu Kadambari Mallikarjuna Rao Kovai Sarala Bhuvaneswari Abhinayasri Mumaith Khan Chakri
| cinematography = Sarath
| editing =
| distributor =
| released = 16 November 2006
| runtime = 180 minutes
| country = India
| language = Telugu
| budget =
| gross =
| preceded_by =
| followed_by =
}} Telugu Film, Released in 2007. The film is a remake of Bollywood blockbuster Malamaal Weekly directed by Priyadarshan, which in turn is inspired by the Hollywood film Waking Ned (1998).

==Plot== Rajendra Prasad) is a lottery ticket agent for Bhagyalakshmi Bumper Draw in a remote village. Being an agent, he is the only person who knows how to claim a lottery. After knowing that someone has won a lottery for ten&nbsp;million rupees, he becomes greedy. On investigating the identity of the winner he comes to know that it is the local drunk and bachelor Yesudas (Ali (actor)|Ali) On reaching Yesudas place Bullebai finds him dead. The story from here is about how Bullebbai manages to claim the prize money using Yesudas’corpse and shares it with a few villagers.

==Cast== Rajendra Prasad Ali
* Rishi
* Farjana Venu Madhav
* Kiran Rathod
* Brahmanandam
* M. S. Narayana
* Tanikella Bharani
* L.B. Sriram Nagababu
* Kadambari Kiran Mallikarjuna Rao
* Kovai Sarala
* Bhuvaneswari
* Abhinayasri
* Mumaith Khan

==Crew== Chakri
* Dialogues: L.B. Sriram
* Cinematography: Sarath
* Art: Peketi Ranga
* Fights: Ram Lakshman
* Story: Priyadarshan
* Screenplay : Nidhi Prasad
* Direction: Nidhi Prasad
* Producers: Raju & Praveen

==Music==
* "Maya Chesindhi" - Udit Narayan, Swetha Pandit, Aadarshini
* "Aa Tholisari" - Hemachandra, Kousalya
* "Manmadhularaa" - Sunanda
* "Chakkani Chukka" - Ravi Varma, Teena
* "Bhigi Kowgili" - Shaan (singer)|Shaan, Kousalya

==External links==
*  
*  

 
 
 
 


 