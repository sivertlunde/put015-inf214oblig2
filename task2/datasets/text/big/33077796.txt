Seven Keys to Baldpate (1917 film)
{{infobox film
| name            = Seven Keys to Baldpate
| image           = Seven Keys to Baldpate 1917 poster.jpg
| imagesize       =
| caption         = lobby poster with likenesses of George M. Cohan and Elda Furry. Hugh Ford
| producer        = George M. Cohan
| based on        =  
| writer          = Earl Derr Biggers (novel)
| starring        = George M. Cohan Anna Q. Nilsson
| music           =
| cinematography  = Ned Van Buren Lewis W. Physioc
| editing         =
| distributor     = Artcraft Pictures
| released        =  
| runtime         = 
| country         = United States
| language        = Silent English intertitles
}}
 1917 American silent mystery play of 1916 and Richard Dix. 

An extant film with much home video availability.  

==Cast==
* George M. Cohan - George Washington Magee
* Anna Q. Nilsson - Mary Norton Elda Furry - Myra Thornhill
* Corene Uzzell - Mrs. Rhodes
* Joseph W. Smiley - Mayor Cargan
* Armand Cortes - Lou Max
* Warren Cook - Thomas Hayden (* as C. Warren Cook)
* Purnell Pratt - John Bland
* Frank Losee - Hall Bentley
* Eric Hudson - Peter the Hermit
* Carleton Macy - Police Chief Kennedy
* Paul Everton - Langdon
* Russell Bassett - Quimby Robert Dudley - Clerk

==See also==
* The House That Shadows Built (1931 promotional film by Paramount); a possibility that the unnamed Cohan clip is from Seven Keys to Baldpate)

==References==
 

==External links==
*  
*  
*  (free download)

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 