The Future (film)
 
{{Infobox film
| name           = The Future
| image          = TheFuture2011Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Miranda July
| producer       = Gina Kwon Roman Paul Gerhard Meixner
| writer         = Miranda July
| starring       = Miranda July Hamish Linklater
| music          = Jon Brion
| cinematography = Nikolai von Graevenitz Andrew Bird
| studio         = The Match Factory Medienboard Berlin-Brandenburg
| distributor    = Roadside Attractions   Alamode Film  
| released       =  
| runtime        = 90 minutes  
| country        = Germany United States
| language       = English
| budget         = $1 million 
| gross          = $887,172 
}} world premiere at the 2011 Sundance Film Festival, where it was screened in the Premieres section. The film was nominated for the Golden Bear at the 61st Berlin International Film Festival.      

==Plot==
The story involves a couple in their mid-30s, Sophie (Miranda July) and Jason (Hamish Linklater)—whose relationship is on the rocks—and their plans to adopt an injured cat, Paw Paw. When the couple decides to adopt the stray cat, their perspective on life changes radically, testing their faith in each other and themselves.

==Cast==
* Miranda July as Sophie
* Hamish Linklater as Jason
* David Warshofsky as Marshall
* Isabella Acres as Gabriella
* Joe Putterlik as Joe
* Angela Trimbur as Dance studio receptionist
* Mary Passeri as Animal shelter receptionist
* Kathleen Gati as Dr. Straus
* Erinn K. Williams as Tammy
* Oona Mekas as Sasha

==Background==
The Future was born as a performance piece July had staged at The Kitchen, among other venues, in 2007.

==Reception==
The Future received generally positive reviews, holding a 71% "fresh" rating on Rotten Tomatoes; the consensus states "A dark and whimsical exploration of human existence that challenges viewers as much as it rewards them."  On Metacritic, which uses an average of critics reviews, the film has a 67/100, indicating "generally favorable reviews". 

The film didnt perform very well in the box office, grossing only $568,290 domestically based on a $1 million budget.   

The film was shown at the LotteryWest Film Festival at the Sommerville Auditiorium (December 12–18) as a part of the Perth International Arts Festival.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 