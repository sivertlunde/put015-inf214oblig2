Chinna (2005 film)
 
{{Infobox film
| name = Chinna
| image =
| director = Sundar C
| writer         = Suraj  (Dialogue) 
| based on       =  
| screenplay     = Sundar C
| story          = Sundar C Arjun Sneha Sneha Livingston Livingston Vikramaditya Vikramaditya Vijayakumar Vijayakumar Mansoor Mansoor Ali Khan Kiran Rathod
| producer = B. Srinivasa Raju
| editing = Mu. Kasi Vishwanathan
| music = D. Imman
| cinematography = Prasad Murella
| studio = Sree Sree Chitra
| distributor = Sree Sree Chitra
| released =  
| runtime = 175 minutes
| country = India
| language = Tamil
}} Tamil Director Sneha in the lead roles. It did reasonable business at the box-office. This film is a remake of the Hindi film Darr which has Shahrukh Khan and Juhi Chawla in lead roles.

==Plot==

The film begins with Gaythri (Sneha) getting married to an irrigation engineer (Vikramaditya) against her wishes. As she conducts her marriage proceedings with mechanical unconsciousness, her sunken heart reflects her past.

Gayathri, a marine student goes to Rameswaram to study coral reef and other marine eco-system while staying with her uncle (Vinu Chakravarthy) in a fisherman hamlet. She witnesses Chinna (Arjun) and his assistants Mansoor Ali Khan and Ponnabalam- local mobsters working for a gang- committing pitiless and violent crimes. Though she wanted to report these crimes to the local SP (Vijay Kumar), her uncle dissuades her.

Meanwhile, she asks her uncle for a guide to take her to locations rich in marine life. Vinu Chakravarthy directs Chinna to help Gayathri in her pursuit. After couple of close encounters with Chinna, Gayathri understands his soft nature and vows to reform him as a socially acceptable man. Later love blossoms between Chinna and Gayathri. 

Even as Chinna tries to make his life straight, a violent feud ensues between the local gang and Chinna. Trying to save Gayathri, Chinna kills the gang leader and is sentenced to life imprisonment. Taking the opportunity of Chinnas absence, Gayathris parents forcibly marry her to the irrigation engineer. 

As the story ploughs through in the second half, Chinna, characterized by obsession with "Gayathri", kills people working against his goal to attain his ladylove. 

At the end, after much drama, when Chinna almost killed Gayathris husband, she shot Chinna in order to save her husband. Chinna asked her if she had feelings for him before this, at least for a second. Just after she shook her head saying yes, he jumped off the building. The movie ends there indicating his death.

==Cast==

* Arjun Sarja as Chinna Sneha as Gayathri Livingston as Police Officer Vikramaditya as Gayathri`s Husband  Vijayakumaras Gayathri`s Uncle and Ex-Police Officer Mansoor Ali Khan as Chinna`s Friend
* Ponnabalam as Chinna`s Friend
* Vinu Chakravarthy as Gayathri`s Uncle
* Manivannan 
* Kiran Rathod as Item Number

==Music==
{{Infobox album Name = Chinna Type = Soundtrack
|Artist= D. Imman Cover  = Released =  Genre = Feature film soundtrack Length =  Language = Tamil
|Label = Think Music Producer = D. Imman Reviews = Last album = Anbe Vaa (2005) This album = Chinna (2005) Next album =ABCD (2005 film)|ABCD (2005)
}}

 

 
 
 
 
 
 


 
 