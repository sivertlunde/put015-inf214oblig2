Kohlhiesels Töchter (1920 film)
{{Infobox film
| name           = Kohlhiesels Töchter
| image          =
| caption        =
| director       = Ernst Lubitsch
| producer       =
| writer         = Hanns Kräly (play) Ernst Lubitsch
| starring       =
| music          = Aljoscha Zimmermann
| cinematography = Theodor Sparkuhl
| editing        =
| studio         = Messter Film 
| distributor    =
| released       =  
| runtime        = 40 minutes 63 minutes (German 1992 version) 58 minutes (20 frame/s) 64 minutes (18 frame/s) Weimar Republic Silent German German intertitles
| budget         =
| gross          =
}}
 silent comedy 1930 sound remake which also starred Porten. 

==Synopsis==
In Bavaria, a sweet-natured young woman Gretel wants to get married but her father refuses to allow the match until her elder sister Liesel has married first. As Liesel is notorious for her bad-tempered personality this is no easy challenge.

== Cast ==
*Jakob Tiedtke as Mathias Kohlhiesel, Wirt des "Dorfkruges"
*Henny Porten as Liesel, the older daughter & Gretel, the younger daughter
*Emil Jannings as Peter Xaver
*Gustav von Wangenheim as Paul Seppl
*Willy Prager as the merchant

==References==
 

== External links ==
* 
*  (German title cards and French subtitles)

 

 
 
 
 
 
 

 
 