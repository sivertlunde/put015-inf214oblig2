Muthuchippikal
{{Infobox film 
| name           = Muthuchippikal
| image          =
| caption        = Hariharan
| producer       = C Das Hariharan KT Muhammad (dialogues) Hariharan
| Madhu Srividya Hari Sankaradi
| music          = Traditional KJ Joy
| cinematography =
| editing        = VP Krishnan
| studio         = Vasanthachithra
| distributor    = Vasanthachithra
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film, Hariharan and Hari and Sankaradi in lead roles. The film had musical score by Traditional and KJ Joy.   

==Cast==
  Madhu
*Srividya Hari
*Sankaradi
*T. R. Omana
*Praveena
*C Das
*Sathaar
*MG Soman
*Mala Aravindan
*Nellikode Bhaskaran
*Oduvil Unnikrishnan
*P. K. Abraham
*Paravoor Bharathan Bhavani
 

==Soundtrack==
The music was composed by Traditional and KJ Joy and lyrics was written by AP Gopalan and Muringoor Sankara Potti. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Alakayilo || S Janaki || AP Gopalan || 
|-
| 2 || Muthukilungum Cheppaanada || K. J. Yesudas, Vani Jairam, Chorus || AP Gopalan || 
|-
| 3 || Nalla Mannennum || Chorus, Jency, Jolly Abraham || AP Gopalan || 
|-
| 4 || Ranjini Ranjini || P Susheela, P Jayachandran || AP Gopalan, Muringoor Sankara Potti || 
|-
| 5 || Thaalikkuruvi thenkuruvi || P Jayachandran || AP Gopalan || 
|}

==References==
 

==External links==
*  

 
 
 

 