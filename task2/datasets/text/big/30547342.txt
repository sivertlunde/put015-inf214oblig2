Socks and Cakes
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Socks and Cakes
| image          =
| image size     =
| alt            =
| caption        =
| director       = Antonio Padovan
| producer       = Merry Colomer Marta Jover. John Kontoyannis
| writer         = Antonio Padovan Alex Vincent
| music          =
| cinematography = Alessandro Penazzi
| editing        =		Antonio Padovan
| studio         = Kimistra Films
| released       =  
| runtime        = 13 minutes
| country        = United States English
}}

Socks and Cakes is a 2010 American comedy/drama short film directed and written by Antonio Padovan. The film includes performances by Timothy J. Cox, Kirsty Meares, Jeff Moffitt, Ben Prayz and Alex Vincent.

==Plot==
Harry (Timothy J. Cox) is a misanthropic French literature professor who is just taking his life one day at a time, getting by with just enough to survive. His ex-wife Amanda (Kirsty Meares) is now married to Harrys best friend, Richard (Jeff Moffitt), an architect. Both seem to put on a positive front that all is well with their marriage, but Amanda has her doubts and Richard has a wandering eye, especially when he meets the free-spirited, innocent, Sophie (Alex Vincent). Sophie has come to the party with her current boyfriend, the older and always grinning real estate broker, David (Ben Prayz).

==Cast==
* Timothy J. Cox as Harry Mogulevsky 
* Kirsty Meares as Amanda
* Jeff Moffitt as Richard
* Ben Prayz as David Alex Vincent as Sophie

==Reviews==
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

==External links==
*  
*  

 
 
 