Mutts to You
{{Infobox Film
  | name           = Mutts to You 
  | image          = Muttstoyou38lobby.jpg
  | caption        = 
  | director       = Charley Chase|
  | writer         = Al Giebler Elwood Ullman
  | starring       = Moe Howard Larry Fine Curly Howard Bess Flowers Bud Jamison Vernon Dent Lane Chandler Cy Schindell 
  | cinematography = Allen G. Siegler
  | editing        = Art Seid
  | producer       = Charley Chase Hugh McCollum 
  | distributor    = Columbia Pictures 
  | released       =  
  | runtime        = 18 02" 
  | country        = United States
  | language       = English
}}

Mutts to You is the 34th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges operate a successful dog grooming business featuring a conveyor belt contrivance and a water wheel that requires Curly to pedal a stationary bicycle in order to keep water flowing. Among the Stooges clients is an affluent couple named Manning (Bess Flowers and Lane Chandler), who have an elaborate misunderstanding that leads to their baby being left momentarily on the Mannings front doorstep just as the Stooges pass by on their way home from work. Thinking the infant has been abandoned, the trio take the child back to their apartment house, despite the firm rule of no babies or dogs being allowed on the premises. 

When the Stooges see the afternoon newspaper saying the baby was kidnapped, they attempt to return the infant to his parents. Moe and Larry disguise Curly as the babys mother, dubbing him "Mrs. OToole." Unfortunately, kindly Policeman OHalloran (Bud Jamison) strikes up a conversation with the "Irish mother", concludes that they are the kidnappers, and tries to apprehend them. The boys make a run for it and are chased by OHalloran, Moe and Larry pulling Curly and the baby in a laundry cart. After being caught, the Mannings reunite, explain the misunderstanding, and the Stooges are free to go. 
 

==Production notes==
Filming for Mutts to You took place from March 30 to April 2, 1938.    The film title is a pun on the insult "Nuts to you!"    The Stooges would also become babysitters of sorts in Sock-a-Bye Baby, Three Loan Wolves, and Baby Sitters Jitters. 
 hak mir Slobodka and I beg you dont bother me and I dont mean maybe." Moe follows this with, "He from China, East Side," a reference to the Lower East Side of Manhattan, which was predominantly Jewish at the time. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 