The Japanese Wife
{{Infobox film
| name           = The Japanese Wife ザ ・ジャッパニーズ ・ワイフ
| image          = The Japanese Wife.jpg
| caption        = Poster
| director       = Aparna Sen
| producer       = 
| eproducer      = 
| aproducer      = 
| story          = Kunal Basu
| screenplay     = Aparna Sen
| starring       =  
| music          = Sagar Desai
| cinematography = Anay Goswamy
| editing        = Raviranjan Maitra| Saregama Films
| released       =   
| runtime        = 105 minutes
| country        = India/Japan
| language       =  
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
The Japanese Wife (Japanese: ザ ・ジャッパニーズ ・ワイフ) Indian film Bengali filmmaker Bengali and Japanese language|Japanese. 
The film was originally scheduled for release in October 2008,  but the release was delayed until 9 April 2010.
 Bengali village Japanese pen friend (Chigusa Takaku) over letters and remaining true and loyal to her throughout his life, while actually never meeting her.

== Plot ==
Snehmoy Chatterjee (Rahul Bose) and Miyage (Chigusa Takaku) are pen friends who exchange wedding vows through letters. Seventeen years pass but they never meet. Yet the bond of marriage is strong between them. This unusual relationship comes under a cloud when a young widow, Sandhya (Raima Sen), comes to stay with Snehmoy along with her eight-year-old son Poltu. Snehmoy and the little boy bond and the arithmetic teacher discovers the joy of palpable bonds and fatherhood. There develops an inexplicable thread of understanding with Sandhya too.
But Snehmoy remained loyal to his unseen Japanese wife. When Miyage was ill from cancer, he took a long leave from his school and tried to find a cure for her illness. Snehamoy sets out one day during a storm to talk to the closest oncologist in Calcutta, but leaves upon realization that without Miyage physically being there, the doctor can do little.  On the way back,he calls Miyage and this is the last time he talks to her. The storm turns violent, with harsh wind and rain.  He catches pneumonia when he returns to his house.  Due to the continuing storm, no villagers are able to travel to Gosaba by boat to obtain the antibiotics required to cure his infection.  He dies some days later. After the sea calms down,a bald Miyage in a white Sari, visits the house of her late husband Snehamoy. Sandhya welcomes her.

== Cast ==
*Rahul Bose as Snehamoy Chatterjee
*Raima Sen as Sandhya
*Chigusa Takaku as Miyage
*Moushumi Chatterjee as Maashi
*Rudranil Ghosh as Fatik
*Kunal Basu: Special appearance

==Production==
The production of the film started in April 2007. This is the first time Aparna Sen has made a film based on someone elses story. This movie is based on the title story of The Japanese Wife and Other Stories by Bengali Indian author Kunal Basu, who writes from Oxford and is an engineer by training. This film was earlier titled as The Kite, but later changed to the name of the original story title.

The shooting locations are Kolkata and Sundarbans in Bengal and the Japanese cities of Yokohama and Tsukuba, Ibaraki. 

===Casting===
Aparna Sen had seen Rahul Boses work in English, August and Split Wide Open and felt that he was a good, controlled and intelligent actor. In an interview Aparna states that her choice of him for three of her films in a row is because she "can deconstruct him completely and mould him differently in any which way I can. Few actors have this kind of malleability". 

Aparna Sens daughter Konkona Sen Sharma was the first choice for the role now played by Raima Sen.  For her role, her name is not mentioned in the original short story, but Aparna Sen named her "Sandhya". Aparna told the Hindustan Times that "Raima as Sandhya shared perfect onscreen chemistry with Rahul. Both are shy and refined and suited the characters well".   

For casting Chigusa Takaku in the title role, Aparna said "We had hired an agency and chose her after auditioning 12 girls. She is a sensitive woman and an intuitive actress. Hence, she got a feel of the character quickly enough.  She didn’t know any English. We had to converse with her through her translator who was always on the sets." 

==Reception==

===Critical reception===

The film received positive to very positive reviews from critics in India.
 
Nikhat Kazmi of The Times Of India rated the film 4 out of 5 stars, and gave it an excellent review saying that, "There is such beauty, restraint and minimalism in this akin-to-a-haiku film, it transports you into another world altogether." She praised the acting of all the lead actors, saying "Rahul Bose is in stellar form with his village boy look and his Bangla Angrezi. Raima Sen is delectable as the reticent, shy widow and Moushumi Chatterjee is a revelation." Rajeev Masand of CNN-IBN rated the film 3 out of 5 stars and gave a good review and praised the acting and direction, saying that, "Despite its leisurely pace and its wildly implausible premise, populated with believable, flesh-and-blood characters who have real fears and anxieties that you can connect with."  Anupama Chopra of NDTV rated the film 3 out of 5 stars and praised the acting, but criticized the pace of the film, saying "How much you enjoy Aparna Sen’s The Japanese Wife is directly proportionate to how patient you are. The film is slow to the point of being painful. And yet, if you stay with it, The Japanese Wife is a rewarding experience. It’s rich with yearning and sadness and yet hopeful, in a quiet way."  Omar Qureshi of Zoom (TV channel)|Zoom rated the film 5 out of 5 stars and called it "zoombastic".

===Box office===
The Japanese Wife had a decent opening with a 44% theatre occupancy. Released on only 32 prints, the film had an 88% initial collection according to Saregama Films. 

===Home media===
In order to combat piracy, The Japanese Wife DVD was released in India only a month after the films theatrical premiere.  Despite this safeguard, the film was pirated soon after cinematic release and was sold online before the films producers were able to file an injunction against the company.  It was purchased by Databazaar Media Ventures for distribution in North America where it was released through Netflix, Amazon.com and iTunes on 13 July 2010.  In the United Kingdom, the film was shown on Channel 4, on 25 April 2011. 

==Awards==
*Star Entertainment Award for Best Film
*Star Entertainment Award for Best Director, Aparna Sen
*Star Entertainment Award for Best Cinematography, Anay Goswamy
*Best Film Award at the 2010 Hidden Gems Film Festival 
*Silver Crow Pheasant Award (Audience Choice) at 2010 International Film Festival of Kerala 

== References ==
 

==External links==
{{External media
|video1=  
}}
* 
*   at ReviewGang

 

 
 
 
 
 
 
 
 
 
 