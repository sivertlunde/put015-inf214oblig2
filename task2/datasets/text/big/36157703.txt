Tohfa
 
 
{{Infobox film
| name = Tohfa
| caption = Movie Poster
| writer = Sachin Bhowmik
| starring = Sridevi Jeetendra Jaya Pradha Kadar Khan Aruna Irani  Shakti Kapoor
| director = K. Raghavendra Rao
| producer = D. Rama Naidu
|music=Bappi Lahiri  Indeevar   (Lyrics)  
| studio  = Suresh Productions
| distributor =
| released =  3 February 1984
| runtime =
| language = Hindi
| awards =
| budget =
|}}
 1984 Hindi film directed by K. Raghavendra Rao, and starring Sridevi, Jeetendra and Jaya Prada.    The film featured a hit pair of the 1980s, Jeetendra and Sridevi, both known for their dancing skills, who appeared in numerous films together, giving hits like Himmatwala (1983 film)|Himmatwala (1983) and Justice Chaudhry (1986). 
 1959 Tamil Highest grossing Best Comic Best Music Best Lyrics nomination for Tohfa Tere Pyaar Ka. 

==Synopsis==
Janaki (Jayaprada) and Lalita (Sridevi) are sisters, who love each other dearly. Lalita and Ram (Jeetendra) fall passionately in love with each other. Unknown to them, Janaki is also in love with Ram. When Lalita finds out, she decides to sacrifice her relationship with Ram. In haste, she marries a good-for-nothing man (Shakti Kapoor), who is arrested by the police immediately afterwards. Unaware of Lalitas true motive, Ram and Janaki are furious with her. Lalita moves away and is not seen for several years. Ram and Janaki get married and live happily; their only sorrow is their inability to have children.
Many years later, Ram meets Lalita again. She is working in an office and singlehandedly bringing up her son. To his shock, Ram realises that he is the father of Lalitas son. The child had been conceived in a moment of passion between Ram and Lalita before they had separated. Ram is torn between his loyalty to his wife Janaki and the pull of affection towards his son. Janaki, now expecting a child, finds out about her sisters reappearance but misunderstands the whole situation. She begins to suspect Ram and Lalita of cheating on her. However, at last, all the misunderstandings are cleared up. The true extent of Lalitas sacrifice becomes known to Janaki, who feel very guilty. Janaki gives birth to a daughter and passes away, leaving the baby and Ram in Lalitas care.

==Cast==
* Sridevi as Lalita
* Jeetendra as Ramu
* Jayapradha as Janki
* Aruna Irani
* Kader Khan as Raghuveer singh
* Shakti Kapoor as Kaamesh singh
* Jagdeep
* Leela Mishra as Dadi
* Mohan Choti as Jamoore
* Asrani as Poojari

==Marketing and reception==
Producer D. Rama Naidu employed huge billboards and cut outs for films publicity prior to its release and managed to create considerable public interest. Upon its release, the film was immediately notice for its item number "Pyar Ka Tohfa Tera" picturised on Jaya Prada and the risque "Ek Ankh Marun To", sung by Asha Bhonsle which became a chartbuster. 

== Soundtrack == Anjaan and Indeevar penned the lyrics.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)!! Duration
|-
| "Pyar Ka Tohfa Tera"
| Asha Bhosle, Kishore Kumar
| 5.45
|-
| "Gori Tere Ang Ang Main"
| Asha Bhosle, Kishore Kumar
| 7.16
|-
| "Ek Aankh Marun Toh"
| Asha Bhosle, Kishore Kumar
| 5.25
|-
| "O Milan Maunjo Se Maunjo Ka"
| Asha Bhosle,  S.P.Balasubrahmanyam
| 5.39
|-
| "Albela Mausam Kehta Hai Swagtam"
| Lata Mangeshkar,  Kishore Kumar
| 5.29
|}

==References==
 

==External links==
*  

 
 
 
 
 
 