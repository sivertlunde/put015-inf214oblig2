Little Big League
{{multiple issues|
 
 
 
}}
{{Infobox film
| name           = Little Big League
| image          = MPW-7352.jpg
| caption        = Little Big League theatrical poster
| director       = Andrew Scheinman
| producer       = Steven Nicolaides Andrew Bergman Mike Lobell
| writer         = Gregory K. Pincus John Ashton Ashley Crow Kevin Dunn
| music          = Stanley Clarke
| cinematography = Donald E. Thorin
| editing        = Michael Jablow 
| studio         = Castle Rock Entertainment
| distributor    = Columbia Pictures
| released       = June 29, 1994
| runtime        = 119 minutes
| country        = United States English
| budget         = 
| gross      = $12,211,068 (USA)
}}
 family sports manager of the Minnesota Twins baseball team. It stars Luke Edwards, Timothy Busfield, and Dennis Farina.

==Plot==
Billy Heywood (Luke Edwards) is a preteen son to a widowed single mom, Jenny (Ashley Crow), and a Little League Baseball player. Billys grandfather is Thomas Heywood (Jason Robards), owner of the Minnesota Twins.

They are a last-place team (though in real life they were World Series champions just three years prior), but Billy and his grandfather love each other, the Twins, and the game of baseball. When the grandfather dies, it is revealed that he wants Billy to inherit the franchise. He has specified that if Billy is still a minor, Thomas Heywoods aides are to help him along until Billy is old enough to run the team by himself.

Billy quickly runs afoul of the teams manager, George OFarrell (Dennis Farina). Billy believes he is too hard on the players. OFarrell despises the idea of working for a kid. After he insults Billy and tells him to butt out of the teams business, Billy fires him.

There is considerable difficulty finding another manager to replace OFarrell, since no one particularly wants to work for a kid. Billy therefore decides to name himself the new manager after one of his friends points out, "Its the American League! They have the Designated hitter|DH!  How hard can it be?"  (In real life, for conflict of interest reasons, MLB does not allow team owners to make themselves their teams manager.)

The players are very skeptical, but Billy promises that if he does not improve the teams position in the standings within a few weeks, he will resign. The team quickly moves up to division race contention. Unfortunately, not all is going smoothly for Billy, as his friend and star first baseman Lou Collins (Timothy Busfield) takes a romantic interest in Billys mother.
 umpire because of a call he didnt like. He also must release his personal favorite Twins player, Jerry Johnson (Duane Davis), who is clearly in the twilight of his career. He ends up making Jerry feel even worse when Billy immaturely tries to illustrate his own distress by pointing out he owns Jerrys baseball card and wouldnt give it up for a Wade Boggs and a Sammy Sosa.

The pressures of managing the team while also fulfilling his other responsibilities, such as schoolwork, wear him down and consume his free time. Billys friends do not like how Billys managerial responsibilities are keeping him away from being with them. Even when hes physically present (as opposed to on the road with the team), he is typically distracted by team business.
 slump and the jealous Billy benches him, sending the Twins into a losing skid. Billy later tells his mom that hes tired of being a "grown-up" and decides to quit as manager after the end of the season, even reinstating Lou to starter on first base.
 1980 Houston 1995 Seattle California Angels Wild Card playoff spot on the line. With two outs in the bottom of the twelfth inning, losing by a run with a man on base and Randy Johnson pitching. Lou tells Billy he asked his mom to marry him. He says her reply was to ask Billy. Initially, Billy says if Lou hits a homer, he will give the marriage his OK, but quickly relents and gives Lou his consent whether or not he hits a homer.
 John Ashton) taking his place as well as bringing back Jerry to be the third base coach and new hitting instructor.

Billy reassures all the players that he will still be the owner, and says that he might come back as manager if junior high doesnt work out. He and the rest of the team then receive a standing ovation from everyone in the Hubert H. Humphrey Metrodome.

==Cast==
* Luke Edwards as Billy Heywood
* Timothy Busfield as Lou Collins John Ashton as Mac Macnally
* Ashley Crow as Jenny Heywood
* Kevin Dunn as Arthur Goslin
* Billy L. Sullivan as Chuck
* Miles Feulner as Joey
* Jonathan Silverman as Jim Bowers
* Dennis Farina as George OFarrell
* Jason Robards as Thomas Heywood
* Wolfgang Bodison as Spencer Hamilton
* Duane Davis as Jerry Johnson
* Leon Durham as Leon Alexander
* Kevin Elster as Pat Corning
* Eric Gendreau as Himself
* Joseph Latimore as Lonnie Ritter
* Brad Lesley as John Blackout Gatling
* John Minch as Mark Hodges
* Michael Papajohn as Tucker Kain Scott Patterson as Mike McGrevey
* Troy Startoni as Larry Hilbert
* Antonio Lewis Todd as Mickey Scales
* ONeal Compton as Major League Umpire John Gordon as announcer Wally Holland
* Chris Berman as Himself
* Ken Griffey, Jr as Himself
* Randy Johnson as Himself
* Lou Piniella as Himself
* Dave Magadan as Himself Paul ONeill as Himself
* Rafael Palmeiro as Himself
* Ivan Rodriguez as Himself
* Wally Joyner as Himself
* Mickey Tettleton as Himself
* Eric Anthony as Himself
* Carlos Baerga as Himself
* Sandy Alomar Jr. as Himself Alex Fernandez as Himself
* Lenny Webster as Himself
* Dean Palmer as Himself
* Tim Raines as Himself

==References==

 

==External links==
*  
*  
*  

 
 

==References==

 
 
 
 
 
 
 
 
 
 
 
 