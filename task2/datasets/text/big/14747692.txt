Play It to the Bone
 
{{Infobox film
| name           = Play It to the Bone
| image          = Play it to the Bone.jpg
| caption        = Theatrical release poster
| image_size     =
| director       = Ron Shelton
| producer       = Stephen Chin Kellie Davis David V. Lester
| writer         = Ron Shelton
| starring       = Antonio Banderas Woody Harrelson Lolita Davidovich Tom Sizemore Lucy Liu Robert Wagner
| music          = Alex Wurman
| cinematography = Mark Vargo
| editing        = Patrick Flannery Paul Seydor
| studio         = Touchstone Pictures Buena Vista Pictures
| released       =  
| runtime        = 124 min.
| budget         = $24 million
| gross          = $8.4 million
}}
 1999 sports film|sports/comedy-drama film, starring Antonio Banderas and Woody Harrelson, written and directed by Ron Shelton.
 boxers and Las Vegas in order to fight each other for the sake of a chance to compete for the middleweight title. The film also starred Lolita Davidovich, Tom Sizemore, Lucy Liu, and Robert Wagner.

 , Tony Curtis, Wesley Snipes, Mike Tyson, Kevin Costner, Rod Stewart, Jennifer Tilly, Natasha Gregson Wagner, Drew Carey and Chuck Bodak. The film was released to neither critical acclaim nor commercial success.

==Plot==
Aging prizefighters and longtime pals Cesar Dominguez (Banderas) and Vince Boudreau (Harrelson) always regretted not getting one last shot. Out of the blue, such an opportunity comes their way — except it is to fight each other.

Boxing promoter Joe Domino (Sizemore) has a problem on his hands. The fighters scheduled to be on his undercard in Vegas, a preliminary to a main event featuring heavyweight Mike Tyson, suddenly become unavailable at the last minute. He needs replacements fast, so a call is made to a gym in Los Angeles to see if Dominguez and Boudreau would be willing to step into the ring against one another.

The boxers negotiate one condition: that the winner will be given a chance to fight for the middleweight championship. Domino agrees, although the untrustworthy promoter is not necessarily a man of his word.

Cesar and Vince have only a day to get to the fight. They decide to drive rather than fly, so they call upon their friend Grace (Davidovich) to drive them in her lime green Oldsmobile 442. Grace is a former love interest of both. Graces own plan is to pitch her various money-making ideas to Vegas bigshots like hotel and casino boss Hank Goody (Wagner) and raise venture capital. Along the way, they pick up a hitchhiker (Liu) whose insults finally result in Graces flattening her with a solid right cross worthy of her traveling companions.

The fight between the two friends is sparsely attended, ringside fans and celebrities remaining uninterested until the nights main event. Cesar and Vince mix it up so savagely, however, beating each other to a bloody pulp, that fans in the arena begin paying more and more attention, as do commentators on TV.

When the action-packed and dramatic bout comes to an end, Cesar and Vince are paid off, but promptly spend most of their money in the casino. Grace, too, comes away bruised and empty-handed, except for her everlasting relationship between a couple of hard-headed but soft-hearted guys.

==Reception==
The film garnered a generally poor reception, with Rotten Tomatoes giving it a score of 12% based on 78 reviews and an average rating of 3.9 out of 10. {{cite web
| url = http://www.rottentomatoes.com/m/play_it_to_the_bone/
| title = Play It to the Bone
| accessdate = 2007-12-17
| author = 
| last = 
| first = 
| date =  2007-12-17
| work = Rotten Tomatoes
| publisher = 
| pages = 
| language = 
| archiveurl = 
| archivedate = 
| quote = 
}} 
Professional critical reception was similar, with Empire magazine giving it just 2 stars out of 5. {{cite web
| url = http://www.empireonline.com/reviews/review.asp?FID=6157
| title = Play It to the Bone
| accessdate = 2007-12-17
| author = 
| last = 
| first = 
| date =  2007-12-17
| work = Empire Online
| publisher = 
| pages = 
| language = 
| archiveurl = 
| archivedate = 
| quote = 
}} 

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 