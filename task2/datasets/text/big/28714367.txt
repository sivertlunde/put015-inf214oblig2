Beyond (2010 film)
{{Infobox film
| name           = Beyond
| image          = Svinalangorna poster.jpg
| caption        = Theatrical release poster
| director       = Pernilla August
| producer       = Helena Danielsson Ralf Karlsson
| writer         = 
| screenplay     = Pernilla August Lolita Ray
| story          = Susanna Alakoski
| based on       =   Ville Virtanen
| music          = Magnus Jarlbo Sebastian Öberg
| cinematography = Erik Molberg Hansen
| editing        = Åsa Mossberg
| studio         = Drakfilm Produktion
| distributor    = Nordisk Film
| released       =  
| runtime        = 92 minutes
| country        = Sweden
| language       = Swedish Finnish
| budget         = 
| gross          = 
}} novel with the same name by Susanna Alakoski. It was shown at the 67th Venice International Film Festival on 6&nbsp;September&nbsp;2010 and got the International Critics Week Award.    

The Swedish Film Institute submitted Beyond for a 2011 Academy Award for Best Foreign Language Film nomination,     but it did not make the final shortlist.   

==Cast==
* Noomi Rapace as Leena
* Ola Rapace as Johan
* Tehilla Blad as Leena as a child
* Outi Mäenpää as Aili Ville Virtanen as Kimmo
* Rasmus Troedsson as Sten
* Alpha Blad as Marja
* Junior Blad as Sakari
* Selma Cuba as Flisan

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Swedish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 

 
 