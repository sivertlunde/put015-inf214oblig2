The Hep Cat
{{Multiple issues|
 
 
}}

{{Infobox Hollywood cartoon
| cartoon_name   = The Hep Cat
| series         = Looney Tunes
| image = TheHepCatBRReissueTitle.gif
| caption = The November 12, 1949 Blue Ribbon reissue title card of The Hep Cat.
| director       = Robert Clampett
| story_artist   = Warren Foster
| animator       = Bob McKimson
| voice_actor    = Mel Blanc
| musician       = Carl W. Stalling
| producer       = Leon Schlesinger Warner Bros. Pictures
| release_date   =   (USA premiere)
| color_process  = Technicolor
| runtime        = 
}}

The Hep Cat is a 1942   features the "Blue Ribbon" titles.

==Synopsis==
The Hep Cat opens with a cat (who resembles the one from Notes to You) strolling through an abandoned lot.  Unfortunately, he stumbles across a dog named "Citizen Kane|Rosebud"&mdash;otherwise known by fans as Willoughby the Dog&mdash;who, upon noticing the cat, gives chase.  The cat, after a successful escape, begins singing "Java Jive."  Later, the cat encounters an attractive female cat, and attempts to woo her, failing utterly.  Suddenly, Rosebud the dog reappears and the chase resumes.  After a series of zany, Clampett-esque sight gags, the cat once again evades the dog.  As the cartoon closes, the cat can be seen kissing his dream girl&mdash;a puppet.

==Title alterations== Merrily We Roll Along" as title music. The Looney Tunes drum with Porky Pig saying Thats All Folks!, also closes the cartoon. This was done to identify the short as a Looney Tune, since the Blue Ribbon titles miscredited the short as a Merrie Melody.

The opening title cards are not correct, since the 1942-43 season was the first in which Looney Tunes cartoons opened with the "bulls-eye" titles, usually with thicker rings.

==Cultural references==
 Jerry Colonna. "Ah, something new has been added" was a slogan for Old Gold (cigarette).

 
 
 
 
 
 
 