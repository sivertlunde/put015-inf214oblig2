The Glimpses of the Moon
{{infobox film
| title          = The Glimpses of the Moon
| image          =
| imagesize      =
| caption        =
| director       = Allan Dwan
| producer       = Adolph Zukor Jesse Lasky
| based on       =   
| writer         = E. Lloyd Sheldon (scenario) Edfrid Bingham (scenario)
| cinematography = Harold Rosson (as Hal Rosson)
| editing        =
| distributor    = Paramount Pictures
| released       = March 25, 1923
| runtime        = 70 minutes; 7 reels (6,502 feet)
| country        = United States Silent (English intertitles)

}} lost  1923 American silent drama film directed by Allan Dwan and starring Bebe Daniels. It was produced by Famous Players-Lasky and distributed by Paramount Pictures.  The film is based upon the 1922 Edith Wharton novel The Glimpses of the Moon.

==Cast==
*Bebe Daniels - Susan Branch David Powell - Nick Lansing
*Nita Naldi - Ursula Gillow
*Maurice Costello - Fred Gillow
*Rubye De Remer - Mrs. Ellie Vanderlyn
*Billy Quirk - Bob Fulmer (credited as William Quirk)
*Charles K. Gerrard - Streffy, Lord Altringham

==References==
 

==External links==
* 
* 
* 

 
 
 

 