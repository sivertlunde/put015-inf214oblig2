Nagamandala
{{Infobox Film name           = Nagamandala image          = Nagamandala poster.jpg director       = T.S. Nagabharana producer       = Srihari L. Khoday writer         = Girish Karnad starring  Vijayalakshmi   Mandya Ramesh   B. Jayashree music          = C.Ashwath cinematography = G.S. Bhaskar released       = 1997 language       = Kannada country        =   India
}}
 Vijayalakshmi in the lead roles. The film, upon release, went on to win many prestigious awards for its content and screen adaptation. The soundtrack, consisting of 15 tracks, tuned by C. Ashwath became very popular for the folksy touch. 

==Introduction==
The story of the film was adapted from a play of the same name written by well-known writer Girish Karnad. The movie was directed by award winning director T.S. Nagabharana, who is deemed to be one of the ace directors in Kannada film industry. Music was scored by C. Aswath and Srihari L. Khoday produced the movie.

The film touches one of the most sensitive issues of marital life. In folk style and form, the film throws open a question as to who is the husband – the person who marries an innocent girl and indulges in self pleasures or the person who gives the real and complete experience of life.
 
G.S.Bhaskar, and his work is a visual thunder in this movie. Especially the night scenes & interior scenes are spellbounded. Bhaskar is a famous technician; he used contrast lighting in this movie, that clearly gave the touch of folk life in every frame.

==Cast==
The film stars Prakash Raj, Vijayalakshmi (Kannada actress)|Vijayalakshmi, Mandya Ramesh, and B. Jayashri in prominent roles. The film is centered on three people, Appanna (Prakash Raj), his wife Rani (Vijayalakshmi) and Naga, a Cobra, who can assume the form of a human being (Prakash Raj).

The strong points of the movie remain the amazing acting by the leading cast and an authentic portrayal and command on story by the director. The director has made some change to the original play in the climax.

==Plot Summary==

Rani is a young bride who is neglected by her indifferent and unfaithful husband, Appanna. Appanna spends most of his time with his concubine and comes home only for lunch. Rani is a typical wife who wants to win her husband’s affection by any means. In an attempt to do so, she decides to drug her husband with a love root, which she mixes in the milk. That milk is spilled on the nearby anthill and Naga, the Cobra drinks it.

Naga, who can take the form of a human, is enchanted with her and begins to visit her every night in the guise of her husband. This changes Ranis life completely as she starts to experience the good things in life though she never knows that the person with her is not her husband but the Naga.

Soon she becomes pregnant and breaks the news to Appanna. He immediately accuses her of adultery and says that he has not impregnnated her. The issue is referred to the village Panchayat. Rani is then asked to prove her fidelity by putting her hand in the snake burrow and taking a vow that she has not committed adultery. (It is a popular belief that if any person lies holding the snake in their hand, they will be instantly killed by the snake God.)

Rani places her hand in the snake burrow and vows that she has never touched any male other than her husband and the Naga in the burrow. She is declared chaste by the village Panchayat. However, her husband is not ready to accept that she is pregnant with his child and decides to find out the truth by spying on the house at night. Appanna is shocked to see the Naga visiting Rani in his form, spending time with her and then leaving the house.

Appanna gets furious with the Naga and indulges in a fight with him. Both of them fight vigorously. Eventually, the Naga dies in the fight. After this incident, Appanna realizes his mistake and accepts Rani along with the child she is carrying.

==Awards==

Karnataka State Awards

Second Best Film State Award 
Best Supporting Actor and Actress Awards 
Best Art Direction Award 
Best Photography Award

Udaya Cine Awards

Best Film Award 
Best Direction Award (T.S. Nagabharana) 
Best Supporting Actor and Actress Awards (Mandya Ramesh and B. Jayashri) 
Best Female Singer Awards (Sangeetha Katti)

Other Awards & Recognitions

Selected for Indian Panorama in the International Film Festival 1997 
Film Fare Best Direction Award 
Cine Express Best Director Award 
Film Fans Association award 
Exhibited in nineteen important centers in USA

 

==Cultural Influence==

Nagamandala is a movie based on folk tales spread in North Karnataka, India. The movie portrays lifestyle, food habits, and routine habits of medieval period in Karnataka. Snakes are considered sacred and are feared and worshipped. North Karnataka dialect is used for conversation. Supernatural elements play a central part in the film.

==Human Complexities / Social Issues==

The film uses a magical folktale to reveal the complexity of human life. In particular, the film focuses on the folktale in the Indian context to reveal the social and individual relations. Some of the most complex issues dealt in the movie reflect the social stigmas prevailing in the society of those times.

The intimate relationships between a man and a woman, an Indian womans desperation to win the affections of her husband in spite of the husband’s open infidelity, the need to prove fidelity being imposed on married women while their husbands are not even questioned about their extramarital affairs, and the village judicial system.

== Soundtrack ==
The music of the film was composed by C. Ashwath. All the 16 songs composed for this film have been written by Gopal Yagnik. The songs are rendered by popular Sugama Sangeetha singers.

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Ee Hasiru Siriyali
| extra1 = Sangeetha Katti
| lyrics1 = Gopal Yagnik
| length1 = 
| title2 = Kambada Myalina Bombeye
| extra2 = Sangeetha Katti
| lyrics2 = Gopal Yagnik
| length2 = 
| title3 = Hudugi Hoo Hudugi
| extra3 = Rathnamala Prakash
| lyrics3 = Gopal Yagnik
| length3 = 
| title4 = Intha Cheluvige
| extra4 = Shivanand Patil
| lyrics4 = Gopal Yagnik
| length4 = 
| title5 = Gediya Beku Magala
| extra5 = Rathnamala Prakash
| lyrics5 = Gopal Yagnik
| length5 = 
| title6 = Chikkiyanthaki
| extra6 = Shivanand Patil
| lyrics6 = Gopal Yagnik
| length6 = 
| title7 = Gavvana Devvada
| extra7 = C. Ashwath
| lyrics7 = Gopal Yagnik
| length7 = 
| title8 = Maguve Nanna Naguve
| extra8 = C. Ashwath
| lyrics8 = Gopal Yagnik
| length8 = 
| title9 = Jodu Haasige
| extra9 = C. Ashwath
| lyrics9 = Gopal Yagnik
| length9 = 
| title10 = Odedoda Manasu Koodi
| extra10 = C. Ashwath
| lyrics10 = Gopal Yagnik
| length10 = 
| title11 = Koncha Kodari Gamana
| extra11 = Chorus
| lyrics11 = Gopal Yagnik
| length11 = 
| title12 = Enidu Hosa Hurupu
| extra12 = Chorus
| lyrics12 = Gopal Yagnik
| length12 = 
| title13 = Mayada Manada Bhara
| extra13 = Chorus
| lyrics13 = Gopal Yagnik
| length13 = 
| title14 = Danidana Nanna Dori
| extra14 = Chorus
| lyrics14 = Gopal Yagnik
| length14 = 
| title15 = Ekanthadolu Koothu
| extra15 = Chorus
| lyrics15 = Gopal Yagnik
| length15 = 
| title16 = Sathyulla Sheelavathige
| extra16 = Chorus
| lyrics16 = Gopal Yagnik
| length16 = 
}}

==Trivia==

The Shah Rukh Khan-Rani Mukherjee starrer Bollywood film Paheli has resemblances to the screenplay of Nagamandala.  Paheli’s director, Amol Palekar, was accused of plagiarizing the screenplay and storyline from Nagamandala. However, Amol dismissed the accusation saying that Paheli was adapted from a short story written by Vijayadan Detha.

==References==
 

==External Sources==
 
* http://www.iicdelhi.nic.in/iic2007/performance_nagamandala.html 

 
 
 
 
 
 
 