The Old Dark House
 
{{Infobox film name           =The Old Dark House image          =Olddarkhouseposter.png image_size     = caption        =theatrical poster director       =James Whale producer       =Carl Laemmle, Jr. writer         =J. B. Priestley (novel) R. C. Sherriff Benn W. Levy starring       =Boris Karloff Melvyn Douglas Charles Laughton Gloria Stuart music          =Bernhard Kaun cinematography =Arthur Edeson editing        =Clarence Kolster distributor    =Universal Pictures released       =  runtime        =71 minutes country        =United States language       =English budget         =$250,000 (est) gross          =
}}
The Old Dark House (1932 in film|1932) is an American comedy and horror film directed by James Whale and starring Boris Karloff. The film is based on the 1927 novel Benighted by J. B. Priestley.     The supporting cast includes Melvyn Douglas, Gloria Stuart, Charles Laughton and Ernest Thesiger.

==Plot==
Seeking shelter from a pounding rainstorm in a remote region of Wales, several travellers are admitted to a gloomy, foreboding mansion belonging to the extremely strange Femm family. Trying to make the best of it, the guests must deal with their sepulchral host, Horace Femm, who claims to be on the run from the police, and his religious, obsessive, malevolent sister, Rebecca.

Things get worse as the brutish mute butler, Morgan, gets drunk, runs amok, threatens Margaret Waverton and releases the long pent-up brother, Saul, a psychotic fantasist and pyromaniac who gleefully tries to destroy the residence by setting it on fire.

==Cast== mute butler employed by the Femm family (billed as KARLOFF)
*   who arrives at the Femm household with Margaret and Philip
* Gloria Stuart as Margaret Waverton: Philips wife, who arrives at the house with Roger
* Charles Laughton as Sir William Porterhouse
*   who is the girlfriend of Sir William
* Ernest Thesiger as Horace Femm: the host of the house, brother to Rebecca and Saul, and son of Sir Roderick
* Raymond Massey as Philip Waverton: Margarets husband who arrives at the house with Roger
*   religious fanatic sister of Horace
* Elspeth Dudgeon as Sir Roderick Femm: the 102-year-old father of Horace, Rebecca and Saul
* Brember Wills as Saul Femm: the pyromaniac family member

==Background==
The film is based on the 1927 novel Benighted (novel)|Benighted by J. B. Priestley, published in the United States under the same title as the film, Booklet essay of the Region 2 Network DVD  and was adapted for the screen by R. C. Sherriff and Benn Levy.

The movie also stars Melvyn Douglas and features Charles Laughton (in his first Hollywood film), Ernest Thesiger, Raymond Massey, Gloria Stuart and Lilian Bond as the Ingenue (stock character)|ingenue. According to the Penguin Encyclopaedia of Horror and the Supernatural, the Femm familys ancient patriarch was played by a woman, Elspeth Dudgeon (billed as "John Dudgeon"), because Whale could not find a male actor who looked old enough for the role.

In spite of the presence of Karloff, The Old Dark House was largely ignored at the American box office, although it was a huge hit in Whales native England. For many years, it was considered a lost film and gained a tremendous reputation as one of the pre-eminent gothic horror films. In 1968, a print of the film was discovered by Curtis Harrington in the vaults of Universal Studios  and was restored with the help of George Eastman House.

==Production== Universal City Waterloo Bridge (1931) which was also directed by James Whale. Levy was loaned to Paramount Pictures, where he worked on the screenplay for Devil and the Deep. When Levy finished work on the film, he returned to Universal to start work on The Old Dark House. {{cite DVD notes
 | title = The Old Dark House
 | origyear = 1932
 | others = James Whale
 | type = Booklet
 | publisher = Kino Video
 | location = New York, New York
 | id = K113
 | year = 1999
}}  The film is based on 1927 novel Benighted by J. B. Priestley, a novel about post-World War I disillusionment.    The film follows the original plot of the book, while adding levels of comedy to the story. 
 Waterloo Bridge film of the same name in 1930.      

==Release==
The Old Dark House was previewed in early July 1932 and was re-issued into theaters in 1939.  In 1957, Universal Studios lost the rights to the original story.  Whales fellow director and friend Curtis Harrington helped to prevent The Old Dark House from becoming a lost film. Harrington repeatedly asked Universal to locate the film negative and then persuaded the George Eastman House film archive to finance a new duplicate negative of the poorly kept first reel. 

===Reception=== dailies gave the film positive reviews.  
 Capitol Theatre in London, England|London.  Stephen Jacobs, Boris Karloff: More Than a Monster, Tomohawk Press 2011 p 117 
 Time Out conducted a poll with several authors, directors, actors and critics who have worked within the horror genre to vote for their top horror films.  The Old Dark House placed at number 57 on their top 100 list. 

==References==
 

==Further reading==
*{{Cite book
 | last= Nollen
 | first= Scott Allen
 | title= Boris Karloff
 |publisher= McFarland
 |year= 1991
 |isbn= 0-89950-580-5
 |url=http://books.google.ca/books?id=c1iyAPhqmboC
 |ref=harv
}}
*{{Cite book
 | last= Mank
 | first= Gregory William
 | title= Hollywood Cauldron
 |publisher= McFarland
 |year= 2001
 |isbn= 0786433329
 |url=http://books.google.ca/books?id=Zyyf9E7OAlkC
 |ref=harv
}}
*{{Cite book
 | last= Hallenbeck
 | first= Bruce G. 
 | title= Comedy-Horror Films: A Chronological History, 1914–2008
 |publisher= McFarland
 |year= 2009
 |isbn= 0-7864-1112-0
 |url=http://books.google.ca/books?id=2AIgAef-bAcC
 |ref=harv
}}

==See also==
*List of rediscovered films

==External links==
*  
*  
*  
*  

 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 