Karate Girl
 

{{Infobox film
| name           = Karate Girl
| image          = Karate-girl-poster.jpg
| caption        = 
| director       = Kimura Yoshikatsu
| producer       = 
| writer         = Nishi Fuyuhiko
| screenplay     = 
| story          = 
| based on       =  
| starring       = Rina Takeda Tobimatsu Hina Tatsuya Naka Yokoyama Kazutoshi
| music          = Yasukawa Goro
| cinematography = Soma Daisuke
| editing        = Murakami Masaki
| studio         =  Toei
| released       =  
| runtime        = 92 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}

  is a 2011 Japanese martial arts film directed by Kimura Yoshikatsu starring Rina Takeda. 

==Plot==
Rina Takeda and Haruna   Tobimatsu co-star as sisters Ayaka and Natsuki Kurenai, the youngest descendants of a legendary Okinawan karate master named Shoujirou Kurenai. As children, they live a happy life with their father (Tatsuya Naka) who encourages them both to practice karate. However, one day a mysterious group breaks into their dojo – killing their father, kidnapping Natsuki, and stealing the black belt which has been passed down in their family for over 200 years. Several years later, Ayaka is living the humble life of an ordinary high school student in Yokohama. One day, when Ayaka was working a group of burglars were stealing a womans purse. Ayaka uses her karate skills to stop them causing her to be a hero to the public. Ayakas heroics was caught on camera causing the evil organization to notice Ayakas fighting skills. Natsuki, on the other hand, has been trained as a killing machine by the mysterious group that kidnapped her all those years ago. Soon, Natsuki and the group begin to take aim at Ayaka. Out of love for her sister and with the teachings of her father still in her heart, Ayaka decides to do whatever it takes to get Natsuki and their family’s black belt back from the clutches of the mysterious group.

==Cast==
* Rina Takeda as Kurenai Ayaka/Ikegami Ayaka
* Tobimatsu Hina as Kurenai Natsuki/Sakura, her younger sister
* Tatsuya Naka as Kurenai Tatsuya, their father
* Yokoyama Kazutoshi as Muto Ryuji
* Richard William Heselton as Keith
* Iriyama Noriko as Ikegami Miki, Ayakas adoptive mother
* Takizawa Saori as Ohashi Satoko
* Horibe Keisuke as Tagawa Shu. 
* Noriko Iriyama

==Reception==
The movie received a positive review. {{cite news|title= Karate Girl (2011) Movie Review
|work= Beyond Hollywood|date=|url= http://www.beyondhollywood.com/karate-girl-2011-movie-review/|accessdate=2011-06-04}} 

==References==
 

==External links==
*    
*  

 
 
 
 
 
 
 
 

 