Weighed But Found Wanting
{{Infobox Film
| name           = Tinimbang Ka Ngunit Kulang (Weighed But Found Wanting)
| caption        = 
| image          = File:Tinimbangkangunitkulang.jpg
| director       = Lino Brocka
| producer       = Mario OHara
| writer         = Lino Brocka Mario OHara
| starring       = Lolita Rodriguez Christopher De Leon Mario OHara Eddie Garcia Lilia Dizon Hilda Koronel
| music          = Lutgardo Labad Emmanuel Lacaba
| cinematography = Jose Batac
| editing        = Augusto Salvador
| distributor    = CineManila Corporation
| released       = May 30, 1974
| runtime        = 128 minutes Filipino English
| country        = Philippines
}}
Weighed But Found Wanting (Filipino language|Filipino: Tinimbang Ka Ngunit Kulang; also known as Human Imperfections Best Picture) at the 23rd FAMAS Awards in 1975.

== Plot ==
The story begins with a flashback to the past of a woman called Kuala (Lolita Rodriguez). An albularyo|herbolario (traditional/folk medicine practitioner) performs an abortion on Kuala, as Cesar (Eddie Garcia) watches her. The abortion was a success, but when Kuala sees the aborted fetus, she becomes disturbed. In the next scene, she walks in the middle of a grassy plain, and as the heat becomes more and more unbearable, she becomes insane.

In the present, Kuala, now the  village idiot, wanders about her Nueva Ecija town in dirty clothes and with mangy hair. The townsfolk mock and deride Kuala, and she is pushed into a watering hole where she almost drowns.

Bertong Ketong (Mario OHara), a leper yearning for female companionship, attracts Kuala with a rattle and takes her to his shack in the cemetery. Junior (Christopher de León) makes friends with them, defying the prohibitions of his father, Cesar Blanco, who is a lawyer and failed politician.
 has a crush on him, and with his girlfriend, Evangeline (Hilda Koronel), who flirted with her escort during that years Santacruzan. The jealous Junior left the procession and sought the company of Milagros (Laurice Guillen), who seduces him.

The local Asociación de las Damas Cristianas (Association of Christian Ladies) is later scandalised to discover that Kuala has fallen pregnant. She is forced to live in the custody of the pious Lola Jacoba (Rosa Aguirre). When Berto makes a clandestine visit to Kuala, she tells him of his unhappiness. Berto tells this to Junior, who resolves to help the pregnant Kuala make an escape from Lola Jacobas house and lead her back to Bertos shack. However, Berto knows she will be taken away and returns her to Lola Jacoba, and promises to retrieve her after she has given birth.

Some nights later, Kuala experiences labour pains. She finds her way to Bertos shack, at which point Berto rushes out to fetch a doctor. When the doctor refuses to help him, Berto takes him hostage but repeats he will not kill him. As Berto flees with the doctor, the doctors wife shouts for help, awakening the townspeople who rush to follow the fleeing pair. Before Berto and the doctor reach the shack, however, the doctor escapes and a chase ensues. A group of policemen come to the doctors rescue and shoot Berto. Junior sees this and is shocked; he holds Bertos dead body and weeps in the midst of the crowd.

Junior then enters the shack where Kuala has successfully birthed a boy, but lies weakened by the labour. She becomes lucid, and in her sanity she recognises Junior and realizes that Berto has been killed. She also recognises Cesar amongst the crowd, and asks him why he killed their child, revealing his secret. Kuala then gives her baby to Junior, and dies. As Junior leaves the shack, he stares hard at the townspeople, including his parents, Evangeline, and all who were unkind to him, Berto and Kuala. He walks near Bertos corpse and stops by, as the people look on in silence. Junior leaves the cemetery with Berto and Kualas son.

== Cast ==
*Christopher de Leon as Junior
*Hilda Koronel as Evangeline Ortega
*Mario OHara as Bertong Ketong
*Lolita Rodriguez as Kuala
*Lilia Dizon as Mrs. Carolina Blanco
*Eddie Garcia as Mr. Cesar Blanco
*Laurice Guillen as Milagros
*Orlando Nadres as Mr. Del Mundo

== Production ==

=== Villa Epifania ===
The movie was partly filmed in "The Grand Old House of Sta. Rita"  (also the film site of "Tanging Yaman" and many other films). 

 
File:Villaepipaniajf.JPG|Façade of Villa Epifania Villa Epifania: The Grand Old House of Sta. Rita The ancestral lot was owned by the Guanzon patriarch Don Agapito Guanzon (Captain Pitong) the then Captain Municipal (equivalent to today’s municipal mayor) of Sta. Rita.
 

== Background ==
Tinimbang, considered by Lino Brocka as his "first novel" and his first production for his own film outfit, is the story of a young boy growing up in a small town and the unusual friendship he develops with a leper and the village idiot. Their stories draw forth the true nature of hypocrisy in the small town and the boy bears witness and participates in the various emotions that throb under the seemingly quiet village life-prejudice, cruelty, forgiveness, and even love. In Tinimbang, Brocka clearly shows mans limitations as a mortal being, but sends a message of hope for the movie, and in the end, speaks ultimately of rebirth and maturity.

== Release ==

=== Box office ===
 

=== Critical reception ===
 

== Accolades ==
The film won six FAMAS Awards out of eight nominations: Best Picture Best Actor (Christopher de Leon) Best Actress (Lolita Rodriguez) Best Director (Lino Brocka)
*Best Musical Score (Lutgardo Labad)
*Best Theme Song (Emmanuel Lacaba for Awit ni Kuala) Best Supporting Best Supporting Actress (Laurice Guillen). 

== References ==
 

== External links ==
* 
* 
* 

 
 
 
 