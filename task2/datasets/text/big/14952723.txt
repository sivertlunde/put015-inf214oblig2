The Man from the East
 
{{Infobox film
| name           = The Man from the East
| director       = Tom Mix
| writer         = Tom Mix
| starring       = Tom Mix
| released       =  
| runtime        = 10 minutes
| country        = United States Silent with English intertitles
}}
 short Western Western film written, directed by and starring Tom Mix.

==Cast==
* Tom Mix as Tom Bates
* Goldie Colwell as May
* Leo D. Maloney as Ranch Foreman
* Pat Chrisman as Stage Driver
* Inez Walker as Mays Aunt
* Hoot Gibson as Butler
* C.W. Bachman as Valet (uncredited)
* Ed Jones as Hotel Keeper (uncredited)
* R.H. Kelly as Tough (uncredited)
* Susie Morella as Maid (uncredited)

==See also==
* Hoot Gibson filmography
* Tom Mix filmography

==External links==
* 

 
 
 
 
 
 
 
 
 