O Mistério de Robin Hood
{{Infobox film
| name           = Os Trapalhões e O Mistério de Robin Hood
| image          = 
| caption        = 
| director       = José Alvarenga Jr.	
| producer       = Marco Altberg Denise Aragão Diler Trindade
| writer         = Paulo Andrade Renato Aragão Mauro Wilson
| starring       = Renato Aragão Dedé Santana Mussum Xuxa Meneghel
| music          = Jota Morais	
| cinematography = Walter Carvalho
| editing        =
| studio         = 
| distributor    = Renato Aragão Produções Artísticas
| released       = 1990
| runtime        = 90 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = 
}}
Xuxa e os Trapalhões em O Mistério de Robin Hood is a 1990 Brazilian film, directed by José Alvarenga Júnior. The film is starring Xuxa Meneghel and Os Trapalhões.

== Plot ==
The tramp Didi is a modern Robin Hood who steals from the smugglers and moneylenders to give to the needy. He lives in hiding near a circus, and is in love with Tatiana, the daughter of an old magician. In this circus, Tonho and Fredo are very clumsy employees. 

== Cast ==
*Renato Aragão .... Didi
*Dedé Santana .... Fredo
*Mussum .... Tonho
*Xuxa Meneghel .... Tatiana
*Carlos Eduardo Dolabella .... Gavião
*Márcio Seixas .... Voice Gaviãos
*Duda Little .... Rosa
*Roberto Guilherme
*Átila Iório
*Nildo Parente .... Father Rosas
*Tião Macalé
*Beto Carrero
*Amadeu Celestino

== See also ==
* List of Brazilian films of the 1990s

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 


 
 