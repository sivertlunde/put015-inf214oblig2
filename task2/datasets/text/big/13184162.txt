Scandal in Sorrento
{{Infobox film
| name           = Scandal in Sorrento
| image          = Pane-amore-e.jpg 
| image size     = 
| caption        = Italian poster
| director       = Dino Risi
| producer       = Marcello Girosi
| writer         = Marcello Girosi Ettore Margadonna Dino Risi Vincenzo Talarico
| narrator       = 
| starring       = Sophia Loren Vittorio De Sica Lea Padovani Tina Pica
| music          = Alessandro Cicognini
| cinematography = Giuseppe Rotunno
| editing        = Mario Serandrei
| distributor    = Titanus Distributors Corporation of America (US)
| released       = 22 December 1955 (Italy) 19 June 1957 (US)|
| runtime        = 106 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
 Italian  comedy film directed by Dino Risi. This is the third film of the trilogy, formed by Bread, Love and Dreams in 1953, Bread, Love and Jealousy in 1954. Innovations include the use of color rather than black and white, as well the location of Sorrento instead of the small village of the previous films of the series. At the 6th Berlin International Film Festival it won the Honorable Mention (Best Humorous Film) award.   

==Plot==
In this Italian romantic comedy set in the beautiful Bay of Naples, Marshal Antonio Carotenuto arrives back in his home town of Sorrento to take care of the local traffic police.  Donna Sofia, an attractive fishmonger,has rented the home from a dashing marshal who now returns to become the town police chief and wants to reclaim his home. The woman refuses to leave and almost accepts marriage to Antonio almost as a joke to make Nicolino, a fisherman who she is genuinely in love with, jealous. She goes along with the marshals wooing and agrees to dump her fiancé and says she will marry him instead. When the marshal realizes what she is doing, he jilts her instead and decides to woo his own landlady instead.

==Cast==
* Vittorio De Sica - Maresciallo Carotenuto
* Sophia Loren - Donna Sofia, a Smargiassa
* Lea Padovani - Donna Violante Ruotolo
* Antonio Cifariello - Nicola Pascazio, Nicolino
* Tina Pica - Caramella
* Mario Carotenuto - Don Matteo Carotenuto
* Virgilio Riento - Don Emidio

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 

 
 