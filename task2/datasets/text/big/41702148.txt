Amazons of Rome
{{Infobox film
| name           = Amazons of Rome 
| image          =Amazons of Rome.jpg
| caption        =
| director       = Carlo Ludovico Bragaglia, Vittorio Cottafavi and Peter OCord
| producer       = 
| writer         =
| based on = 
| starring       = Louis Jourdan
| music          = Marcel Landowski 	
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  1961
| runtime        = 
| country        = Italy
| awards         =
| language       = Italian
| budget         = 
| preceded_by    =
| followed_by    =
}}
Le vergini di Roma is a 1961 Italian peplum film directed by Carlo Ludovico Bragaglia, Vittorio Cottafavi and Peter OCord. It was internationally released as Amazons of Rome and Warrior Women. 

==Cast==
* Louis Jourdan 	as	Drusco
* Sylvia Syms 	as	Clelia
* Jean Chevrier 	as	Porcenna, Etruscan leader
* Nicole Courcel 	as	Lucilla, Porcennas wife
* Ettore Manni 	as	Horatio / Cocles, Roman Consul
* Paola Falchi  	as	Aurelia
* Renaud Mary 	as	Stravos
* Michel Piccoli  	as	Console Publicola
* Corrado Pani 	as	Muzio Scevola
* Nicolas Vogel 	as	Rasmal
* María Luisa Rolando 	as	Donna Romana
* Carlo Giustini 	as	Bruto 
* Jacques Dufilho 	
	
==References==
 
==External links==
*  at IMDB

 

 
 
 
 
 

 