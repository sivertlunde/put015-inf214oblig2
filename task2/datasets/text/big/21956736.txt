Eminent Domain (film)
{{Infobox film
| name           = Eminent Domain
| image          = Eminent Domain (movie poster).jpg
| image_size     = 
| caption        = Original theatrical poster
| director       = John Irvin
| producer       = 
| writer         = Andrzej Krakowski (story) Andrzej Krakowski Richard Greggson Paul Freeman
| music          = Zbigniew Preisner
| cinematography = Witold Adamek
| editing        = Peter Tanner
| distributor    = Triumph Releasing Europe Image Distribution
| released       = 1990
| runtime        = 106 minutes
| country        = France Canada Poland
| language       = English
| budget         = 
}}
Eminent Domain is a film released in 1990. It stars Donald Sutherland and Anne Archer and is directed by John Irvin.
 Polish Politburo mental asylum and their 15-year-old daughter is kidnapped.

== Cast ==
*Donald Sutherland - Josef Borski
*Anne Archer - Mira Borski Paul Freeman - Ben
*Anthony Bate - Kowal
*Jodhi May - Ewa
*Pip Torrens - Anton
*Françoise Michaud - Nicole
*Yves Beneyton - Roger

==Production==
The movie was filmed on location in Gdańsk and Warsaw. According to film publicity, the movie was based on actual occurrences.  

The movie grossed $151,098 in a limited U.S. theatrical release.

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 