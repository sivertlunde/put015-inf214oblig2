The Bigamist (1921 film)
{{Infobox film
| name           = The Bigamist
| image          =
| caption        =
| director       = Guy Newall  George Clark
| writer         = F.W. Mills Young (novel)   Guy Newall
| starring       = Guy Newall   Ivy Duke   Julian Royce   Barbara Everest
| music          = 
| cinematography = 
| editing        = 
| studio         = George Clark Productions
| distributor    = Stoll Pictures
| released       = August 1921
| runtime        = 9,000 feet  
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = £51,000 
| preceded_by    =
| followed_by    =
}} silent romance film directed by Guy Newall and starring Newall, Ivy Duke and Julian Royce. A happily married couples life is turned upside down when they are approached by a woman also claiming to be the husbands wife.

==Cast==
*  Guy Newall as George Dane 
* Ivy Duke as Pamela Arnott 
* Julian Royce as Herbert Arnott 
* Barbara Everest as Blanche Maitland 
* A. Bromley Davenport as Richard Carruthers 
* Dorothy Scott as Mrs. Carruthers  Douglas Munro as Proprietor

==References==
 

==Bibliography==
* Bamford, Kentom. Distorted Images: British National Identity and Film in the 1920s. I.B. Tauris, 1999.
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

 

==External links==
* 
 
 
 
 
 
 
 
 
 
 
 
 