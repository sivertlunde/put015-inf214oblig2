Creature with the Atom Brain (1955 film)
{{Infobox film
| name           = Creature with the Atom Brain
| image          =  | image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Edward L. Cahn
| producer       = Sam Katzman
| writer         = Curt Siodmak
| screenplay     = Curt Siodmak
| story          = Curt Siodmak
| based on       =  
| narrator       = 
| starring       = {{Plainlist |
* Richard Denning
* Angela Stevens
* S. John Launer
}}
| music          = 
| cinematography = Fred Jackman Jr.
| editing        = Aaron Stell
| studio         = Clover Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 69 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Edward L. Cahn from a screenplay by Curt Siodmak and distributed by Columbia Pictures as the bottom half of a double bill with It Came from Beneath the Sea. The cast included Michael Granger and Gregory Gaye as well as Richard Denning, who starred in a number of similar 1950s B movies.

== Plot ==
Deported American gangster Frank Buchanan (Michael Granger) forces ex-Nazi scientist Wilhelm Steigg (Gregory Gaye) to create zombies by resurrecting corpses through radiation in order to help him exact revenge on his enemies. 

== Cast ==
*Richard Denning as Dr. Chet Walker 
*Angela Stevens as Joyce Walker
*Lane Chandler as Gen. Saunders
*Charles Horvath as Creature  Michael Granger as Frank Buchanan 
*Gregory Gaye as Dr. Wilhelm Steigg
*Pierre Watkin as Mayor Bremer
*Harry Lauter as Reporter #1
*Larry J. Blake as Reporter #2
*Richard H. Cutting as Dick Cutting, radio broadcaster

== Reception ==
In The Zombie Movie Encyclopedia, academic Peter Dendle wrote, "Good 50s fun abounds, with all the twisted gender ideology and antiseptic social ideals that that implies, packed in a tightly-wrought action film with strong (if entertainingly dated) conceptual support."   David Maine of PopMatters rated it 6 out of 10 stars and called it "a thoroughly enjoyable, noir-ish SF chiller, if you can get past the dingbat wife and cutie-pie kid." 

== DVD release == The Werewolf and The Giant Claw and Zombies of Mora Tau).

== Influence == Roky Erickson & The Aliens song of the same name.

Director Cahn would go on to make Invisible Invaders (1959) using the same basic concept (in the later film, invading aliens inhabit the reanimated corpses of humans).

==References==
 

==External links==
*   at Atomic Monsters.com
*  

 
 

 
 
 
 
 
 
 
 