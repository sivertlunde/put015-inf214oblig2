Lonesome Luke, Lawyer
 
{{Infobox film
| name           = Lonesome Luke, Lawyer
| image          =
| image size     =
| caption        =
| director       = Hal Roach
| producer       = Hal Roach
| writer         =
| narrator       =
| starring       = Harold Lloyd
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States Silent English English intertitles
| budget         =
}}
 short comedy film featuring Harold Lloyd.

==Cast==
* Harold Lloyd - Lonesome Luke
* Bebe Daniels
* Snub Pollard
* Bud Jamison Charles Stevenson - (as Charles E. Stevenson)
* W.L. Adams
* Estelle Harrison
* Sidney De Gray
* Gus Leonard
* Lottie Case
* Sammy Brooks
* Merta Sterling - (as Myrtle Sterling)
* Dorothea Wolbert - (as Dorothy Wolbert)
* Harry L. Rattenberry
* C.G. King
* Norman Napier

==See also==
* Harold Lloyd filmography

==External links==
* 

 
 
 
 
 
 
 
 
 


 