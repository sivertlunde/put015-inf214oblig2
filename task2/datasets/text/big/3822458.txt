Confessions of a Nazi Spy
{{Infobox film
| name           = Confessions of a Nazi Spy
| image          = Confessions of a Nazi Spy 1939 poster.jpg
| image_size     = 220
| caption        = 1939 Theatrical Poster
| producer       = Hal B. Wallis Jack L. Warner Robert Lord
| director       = Anatole Litvak
| writer         = Leon G. Turrou (articles) Milton Krims John Wexley (screenplay)
| starring       = Edward G. Robinson Francis Lederer George Sanders Paul Lukas
| music          = Max Steiner (uncredited)
| cinematography = Sol Polito Ernest Haller
| editing        = Owen Marks
| distributor    = Warner Bros.
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = United States dollar|USD$1.5 M  
| gross          =
}}
  spy thriller Nazi film Hollywood studio FBI agent Leon G. Turrou, who had been active in investigating Nazi spy rings in the United States prior to the war, and lost his position at the Bureau when he published the articles without permission. 

The film failed at the box office.  Confessions of a Nazi Spy was banned in Germany, Japan, and many Latin American and European countries. 

The film was re-released in 1940 with scenes describing events that had taken place since the initial release, such as the invasions of Norway and France. Scenes from Confessions of a Nazi Spy are shown in  .

==Plot== Nazi cause among German-Americans. He instructs his audience at a German restaurant that the Führer has declared war on the evils of democracy and that as Germany|Germans, they should carry out his wishes. Kurt Schneider (Francis Lederer), an unemployed malcontent, joins the cause and eventually becomes a spy for the group. A letter written by Schneider to a liaison in Scotland is intercepted by a British Military Intelligence officer (James Stephenson), leading to the rings downfall.

FBI agent Ed Renard (Edward G. Robinson) is assigned to the case, and is able to capture Schneider and extract a confession by flattering his ego. Through Schneider, Renard is led to Hilda Kleinhauer (Dorothy Tree), then Kassels mistress Erika Wolff (Lya Lys), and eventually the ringleader himself. While the FBI manages to capture many members of the ring and their accomplices, several, including Kassel, are secretly spirited back to Germany, but some ultimately face a worse fate there.
 Bund in Mayor La Guardia for his bravery.

There are many similarities between the events depicted in the movie and the real world round up of the Nazi Duquesne Spy Ring in 1941.
 The Stranger.

==Cast==
 
*Edward G. Robinson as Edward Renard
*Francis Lederer as Kurt Schneider
*George Sanders as Franz Schlager
*Paul Lukas as Dr. Karl Kassel
*Henry ONeill as U.S. Atty. Kellogg
*Dorothy Tree as Hilda Kleinhauer
*Lya Lys as Erika Wolff
*Grace Stafford as Helen Schneider
*James Stephenson as British Military Intelligence agent
*Hedwiga Reicher as Lisa Kassel
*Joe Sawyer as Werner Renz
*Sig Ruman as Dr. Julius Krogmann
*Henry Victor as Hans Wildebrandt Hans Heinrich von Twardowski as Max Helldorf
*Wolfgang Zilzer as Johann Westphal
*Rudolph Anders as Capt. Wilhelm Straubel
*Eily Malyon as Mrs. MacLaughlin
*Alec Craig as McGregor, the Postman
*Ward Bond as American Legionnaire (uncredited)
*Martin Kosleck as Dr. Paul Joseph Goebbels (uncredited)
 

==Notes==
 

==See also==
* Confusions of a Nutzy Spy
* The Stranger (1946 film)|The Stranger (1946 film)

==External links==
*  
*  
*  
*  
*   at the  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 