Magizhchi
{{Infobox film
| name = Magizhchi
| image = 
| director = V. Gowthaman
| producer = D. Manivannan
| screenplay = V. Gowthaman
| based on =  
| starring =  
| cinematography = Chezhiyan Vidyasagar
| editing =
| studio = Athirvu Thiraipattarai
| distributor = 
| released =  
| runtime = 
| language = Tamil
| country = India
}} Seeman and Neela Padmanabhans novel Thalaimuraigal,  released on 19 November 2010,  to  favorable reviews but was a big failure at the Box office

== Cast ==
* V. Gowthaman as Thiravi Anjali as Kuzhali Karthika
* Seeman
* Sampath Raj as Sevatha Perumal
* Prakash Raj
* Ganja Karuppu
* Sukumari
* V.S. Raghavan

==Soundtrack==
{{Infobox album |  
| Name       = Magizhchi
| Type       = Soundtrack Vidyasagar
| Cover      = 
| Released   = 2010
| Recorded   = 
| Genres     = World Music
| Length     =
| Label      =  Vidyasagar
| Last album = Siruthai (2010)
| This album = Magizhchi (2010)
| Next album = 
}} Vidyasagar has composed a total of 6 songs for the movie. 
{|class="wikitable" 
|-
! Song Title !! Singers !! Lyrics
|-
| "Theakku Maramaattam" || Manickka Vinayakam, Velmurugan, Karisal Karunanithi || Vaiyyampatti Muthuswamy
|-
| "Otthhu Thanni Aathoda" || Karthik (singer)|Karthik, Chandrayee || Vairamuthu
|-
| "Selai Kattiya Sevanthi" || Karthik (singer)|Karthik, Roshini, Sithara || Vairamuthu
|-
| "Kanne Kaniyurangu" || Madhu Balakrishnan, Binny Krishnakumar || Patchaiappan
|-
| "Utchukotta Itchu Vachhu" || Gopal Rao, Rajalakshmi || Arivumathi
|- Karthik || Patchaiappan
|}

== References ==
 

==External links==
*  

 
 
 
 
 


 