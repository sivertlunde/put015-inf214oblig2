The True Glory
{{Infobox film
| name = The True Glory
| image =
| caption =
| director = Carol Reed Garson Kanin (uncredited) Ministry of Information
| writer =
| starring = Dwight D. Eisenhower
| music =
| narration =
| cinematography =
| editing =
| distributor =
| released =  
| runtime = 85 minutes
| country = United Kingdom United States
| language = English
| amz_id = B000J3E3YW
}} Ministry of Information, documenting the victory on the Western Front, from Normandy to the collapse of the Third Reich. 

Although many individuals, including screenwriter and director Garson Kanin, contributed to the film, British director Carol Reed is normally credited as the director. The film was promoted with the tagline, "The story of your victory...told by the guys who won it!" The film won the Academy Award for Best Documentary Feature.

The documentary film is notable for using multiple first-person perspectives as narrative voices, somewhat in the manner of Tunisian Victory (1944). However, in The True Glory, instead of just an American G.I. (military)|G.I. and a British Tommy Atkins|Tommy, the voices include a Canadian, a French resister, a Parisian civilian family, an African-American tank gunner, and several female perspectives including a nurse, and clerical staff. The film is introduced by General Dwight D. Eisenhower, Supreme Commander of Allied Forces in Europe, and many other prominent individuals appear in it including General George S. Patton.

The title is taken from a letter of Sir Francis Drake, which is quoted in a final caption: "There must be a beginning of any great matter, but the continuing unto the end until it be thoroughly finished yields the True Glory."

==Further reading==
* 
* 
== See also ==
*List of Allied Propaganda Films of World War 2

== External links ==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 


 