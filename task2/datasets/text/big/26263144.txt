Aakhari Decision
{{Infobox film
| name           = Aakhari Decision
| image          = AakhariDecision2010Poster.jpg
| alt            =
| caption        = Poster
| director       = Deepak Bandhu
| producer       = A.Singh
| writer         =
| narrator       =
| starring       = Amar Sidhu   Anant Jog  Nagesh Bhonsle
| music          = Tutul, Bapi, Hanif Sheikh
| cinematography = Andrew Strahorn
| editing        = Raju Surve
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}
Aakhari Decision is a Bollywood action film directed by Deepak Bandhu and stars Anant Jog, Nagesh Bhonsle, Mushtaq Khan, Navni Parihar, Amar Sidhu, and Sumona Chakravarti. It was filmed in India and the United States. 

==Cast==
*Amar Sidhu
*Anant Jog
*Nagesh Bhonsle
*Mushtaq Khan
*Navni Parihar
* Sumona Chakravarti

==Box office==
The film was profitably released in phases all over India as well as in theaters across the United States. It was one of few films selected for screening out of thousands of entries for the Pravasi Film Festival in Delhi, India in Jan 2010. 

==References==
 

==External links==
*  

 
 
 


 