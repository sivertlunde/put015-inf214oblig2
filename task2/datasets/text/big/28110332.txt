Chłopaki nie płaczą
Chłopaki Warszawa and Jelenia Góra, the film premiered on 25 February 2000.

== Cast ==
* Maciej Stuhr – Kuba Brenner
* Wojciech Klata – Oskar
* Tomasz Bajer – Laska
* Cezary Pazura – Fred
* Mirosław Zbrojewicz – Grucha
* Michał Milowicz – Bolec
* Anna Mucha – Lili
* Bohdan Łazuka – chef
* Andrzej Zieliński – Silnoręki
* Mariusz Czajka – Alfons Czesiek
* Mirosław Baka – Cichy, Kubas friend
* Tadeusz Huk – older policeman
* Radosław Pazura – young policeman
* Paweł Nowisz – Dziekan Zajączek
* Edward Linde-Lubaszenko – rector Rudolf
* Asja Łamtiugina – Professor
* Magdalena Mazur – Weronika
* Monika Ambroziak – Cycofon
* Paweł Deląg – Jarek Psikuta
* Leon Niemczyk – "Król Sedesów", Laskas father
* Jakub Sienkiewicz – doctor
* Sławomir Pacek – dermatologist
* Krzysztof Krupiński – waiter
* Katarzyna Śmiechowicz – storewoman in rental video
* Jerzy Nasierowski – guest
* Grzegorz Kowalczyk – bandit that tried to steal Freds suitcase
* Robert Hennig – Gangster
* Julian Karewicz – Bąbel, Laskas friend
* Marcin Kołodyński – Serfer, Laskas Friend
* Ryszard Jabłoński – Hairdresser in "Galeria Fryzur"
* Andrzej Butruk – redactor Woźnica
* Grzegorz Skrzecz – Władzio, traffic policeman
* Jerzy Kolasa – Jerzyk, traffic policeman
* Krzysztof Kosedowski – Siwy
* Arkadiusz Walkowiak – young dodger
* Violetta Arlak – deans secretary
* Janusz Wójcik – guest in bar
* Władysław Antoni Żmuda|Władysław Żmuda – disputant in bar
* Krzysztof Dmoszyński – disputant in bar
* Michał Litwiniec – saxophonist
* Andrzej Mleczko – guest in bar
* Aleksandra Kisio – partner of guest in bar (Andrzej Mleczko)

== External links ==
*  
*  

 
 
 


 