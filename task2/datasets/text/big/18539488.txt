The Fifth Seal
{{Infobox film
| name           = The Fifth Seal
| image          = 
| caption        = 
| director       = Zoltán Fábri
| producer       = 
| writer         = Zoltán Fábri Ferenc Sánta
| starring       = Lajos Őze László Márkus Zoltán Latinovits
| music          = György Vukán
| cinematography = György Illés
| editing        = Ferencné Szécsényi
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}} Best Foreign Language Film at the 49th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Lajos Őze - Miklós Gyuricza (Gyuricza Miklós)
* László Márkus - László Király (Király László)
* Ferenc Bencze - Béla
* Sándor Horváth - János Kovács (Kovács János)
* István Dégi - Károly Keszei (Keszei Károly)
* Zoltán Latinovits - civvies
* Gábor Nagy (actor)|Gábor Nagy - the blonde one
* György Bánffy - the high one
* József Vándor - Macák
* Noémi Apor - Mrs Kovács (Kovácsné)
* Ildikó Pécsi - Irén
* Marianna Moór - Lucy (as Moór Mariann)
* Rita Békés - Erzsi
* György Cserhalmi - dying communist
* Gábor Kiss - Guard
* Gabriella Kiss - Gyuriczas daughter

==See also==
* List of submissions to the 49th Academy Awards for Best Foreign Language Film
* List of Hungarian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 

 
 