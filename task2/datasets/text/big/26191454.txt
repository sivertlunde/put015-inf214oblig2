In Fast Company
{{Infobox Film
| name           = In Fast Company
| image          = 
| caption        = 
| director       = Del Lord
| producer       = Jan Grippo Lindsley Parsons Tim Ryan Victor Hammond Raymond Schrock	
| starring       = Leo Gorcey Huntz Hall Bobby Jordan William Benedict
| music          = Edward J. Kay
| cinematography = William Sickner William Austin
| distributor    = Monogram Pictures
| released       =  
| runtime        = 63 minutes
| language       = English
| budget         = 
}}
In Fast Company is a 1946 film starring the comedy team of The Bowery Boys.  It is the second film in the series.

==Plot==
The boys are involved in an altercation with a vegetable vendor and are saved by Father Donovan who convinces the policeman to let them go.  He uses that to guilt Slip into becoming a driver at Cassidys Cab Company after the owner is knocked out of commission by a rival cab company, Red Circle Cab.  

Slip clashes with drivers of the rival company and enlists the aid of the rest of the gang to expose the company to the owner, Mr. McCormick.

==Cast==
===The Bowery Boys===
*Leo Gorcey as Terrance Slip Mahoney
*Huntz Hall as Sach
*Bobby Jordan as Bobby
*William Benedict as Whitey
*David Gorcey as Chuck

===Remaining cast===
*Bernard Gorcey as Louie Judy Clark as Mabel Dumbrowski
*Jane Randolph as Marian McCormick
*Douglas Fowley as Steve Trent Charles D. Brown as Father Donovan

==Production==
Gorceys father, Bernard Gorcey makes his first appearances as the owner of Louies Sweet Shop, and it is also the first appearance of an all-out fistfight which would become a common plot element in the series.

David Gorceys first Bowery Boys film. He would remain with the series up until the end in 1958, playing the role of Chuck.

The film, made under the working title In High Gear, is a remake, with Monogram Pictures filming an earlier version in 1938. 

==Home media== Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume One" on November 23, 2012.

==References==
 

== External links ==
*  

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958
| before=Live Wires 1946
| after=Bowery Bombshell 1946}}
 

 

 
 
 
 
 