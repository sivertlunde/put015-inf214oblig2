Sallah Shabati
{{Infobox film
| name           = Sallah Shabati
| image          = Sallah.jpg
| caption        = 
| director       = Ephraim Kishon
| producer       = Menahem Golan
| writer         = 
| starring       = Chaim Topol Arik Einstein Gila Almagor Shraga Friedman
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Israel
| language       = Hebrew
| budget         = 
}} Israeli immigration Fiddler on the Roof) to audiences worldwide.
 Yemenite Jewish name, it is also intended to evoke the phrase סליחה שבאתי, "sorry that I came". In earlier print versions of Kishons short stories which were revised for the film, the character was known as Saadia Shabtai.

==Plot==
The film begins with Sallah Shabati, a Mizrahi Jewish immigrant, arriving with his family in Israel. Upon arrival he is brought to live in a Maabarot|maabara, or transit camp. He is given a broken down, one room shack in which to live with his family and spends the rest of the movie attempting to make enough money to purchase adequate housing. His money-making schemes are often comical and frequently satirize the political and social stereotypes in Israel of the time.

==Cast== Topol as Sallah Shabati (as Haym Topol)
* Geula Nuni as Habbubah Shabati (as Geula Noni), Sallahs daughter
* Gila Almagor as Bathsheva Sosialit Albert Cohen
* Shraga Friedman as Neuman, a supervisor at the kibbutz
* Zaharira Harifai as Frieda, a supervisor at the kibbutz
* Shaike Levi as Shimon Shabati, Sallahs son
* Nathan Meisler as Mr. Goldstein, Sallahs neighbor and backgammon pal
* Esther Greenberg as Sallahs wife
* Mordecai Arnon as Mordecai

==Themes==
Sallah Shabatis irreverent and mocking depiction of core Zionist institutions like the kibbutz provoked strong reactions among many filmgoers and critics.  "The kibbutzniks in the film resemble bureaucrats and are clearly divided into veterans with managing roles and simple workers, a division which contradicts the myth of Socialist solidarity and collectivist idealism.  The kibbutzniks betray total indifference, furthermore, to the miserable conditions of the poor maabara next to them." 

==Critical Reception== Best Foreign Best Foreign Italian film, Yesterday, Today, and Tomorrow.   

==Trivia==
At first, Foreign Affairs Minister Golda Meir refused to let the film leave the country because of her unflattering portrayal in the film. 

==See also==
* List of submissions to the 37th Academy Awards for Best Foreign Language Film
* List of Israeli submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 