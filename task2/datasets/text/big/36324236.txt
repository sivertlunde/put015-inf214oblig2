Turning Point (2012 film)
{{Infobox film name = Turning Point border = yes director = Niyi Towolawi producer = Niyi Towolawi Catrine Mcgregor-Unger Pax Osaro Sanni Ibrahim writer = Niyi Towolawi starring = Igoni Archibong Jackie Appiah K.D. Aubert Todd Bridges Ernie Hudson Patience Ozokwor music = Lawrence Adeokun cinematography = Timur Civan editing = Niyi Towolawi Thomas Wong studio = HekCentrik Productions released =   country = United States language = English
}}
Turning Point is a 2012 drama film written and directed by Niyi Towolawi, starring Jackie Appiah, K.D. Aubert, Todd Bridges, Ernie Hudson, Patience Ozokwor, et al., with Igoni Archibong in leading role of "Ade". It received 2 nominations at the 9th Africa Movie Academy Awards.

==Plot==
Ade (Igoni Archibong) is a playboy Nigerian-American investment banker working at a successful firm in New York City. He is in a relationship with keen-to-marry African-American Stacey (KD Aubert). Her family (Ernie Hudson, Cynda Williams) had been cautious of this “African” but begins to warm to him due to his professional success and charm.

However, Ades manipulative mother (Patience Ozokwor) back in Nigeria would rather her son gets serious and dumps “that girl with no traceable roots” in favour of a wife from within her social circle. He is tricked into visiting Nigeria only to discover an arranged marriage had already been conducted on his behalf with a complete stranger. Reluctantly, Ade accepts the new wife, Grace (Jackie Appiah), since she is beautiful and seems submissive.

Back in the US, Ade avoids Stacey until she crudely discovers his secret marriage. Grace quickly settles into the American lifestyle, living as a kept woman while Ade continues to enjoy the freedom of a bachelor.

Grace eventually tires of Ade’s behaviour and confronts him, setting off a series of battles that makes Ade realise how easily his enviable lifestyle could be taken away. With the going tough and friends thin, Ade decides to mount a final showdown that will be a turning point for everyone.

==Cast==
* Igoni Archibong as Ade
* Jackie Appiah as Grace
* K.D. Aubert as Stacey
* Todd Bridges as Marvin
* Joe Estevez as The Boss
* Ernie Hudson as Mr Johnson
* Enyinna Nwigwe as Steve
* Oge Okoye as Ebony
* Patience Ozokwor as Ade’s Mum
* Cynda Williams as Mrs Johnson

==External links==
*  
*  

 
 
 
 
 
 
 
 
 


 