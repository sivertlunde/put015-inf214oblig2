Company of Heroes (film)
 
{{Infobox film
| name           = Company of Heroes
| image          =  
| caption        = Blu ray disc cover
| director       = Don Michael Paul
| producer       = Jeffery Beach Phillip J. Roth	
| writer         = Danny Bilson Paul De Meo David Reed
| starring       = Tom Sizemore Chad Michael Collins Vinnie Jones Dimitri Diatchenko Neal McDonough  Sam Spruell   Jürgen Prochnow
| music          = Frederik Wiedmann	 	
| cinematography = Martin Chichov	 	
| editing        =
| studio         =
| distributor    = Sony Pictures
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English German
| budget         =
| gross          =
}}
 the same name.

==Plot==
With the Germans apparently near defeat in the latter part of   dead, the youngest of the soldiers (Chad Michael Collins) and a cook (Tom Sizemore) who had been demoted from Lieutenant after the D-Day landings lead them deep into Nazi territory.  There, they are joined by an escaped British airman (Vinnie Jones) and Russian (Dimitri Diatchenko). Discovered and pursued, the Americans make a series of hair-breadth escapes from vastly superior numbers of well-armed Nazi soldiers and finally make contact with a woman (Melia Kreiling), their link to the bomb and to the scientist (Jürgen Prochnow).

==Cast==
*Tom Sizemore as Lt. Dean Ransom
*Chad Michael Collins as Nate Burrows
*Vinnie Jones as Brent Willoughby
*Dimitri Diatchenko as Ivan Pozarsky
*Neal McDonough as Lt. Joe Conti
*Sam Spruell as Sgt. Matherson
*Richard Sammel as Col. Beimler
*Melia Kreiling as Kestrel
*Philip Rham as Lt. Schott
*Alastair Mackenzie as Duncan Chambliss
*Jürgen Prochnow as Dr. Luca Gruenewald
* Peter Ladjev as Ricky Rizzo
* Ivo Arakov as Johnny Lewis
* Atanas Srebrev as Office of Strategic Services|O.S.S. Officer
* Zara Dimitrova as Coat-Check Girl
* Hristo Balabanov as German Guard
* Uti Buchvarov as German Cook
* Alexander Nosikoff as Opera Singer
* Vanya Rankova as Beimlers Wife

==External links==
* 

 

 
 
 
 
 
 
 


 