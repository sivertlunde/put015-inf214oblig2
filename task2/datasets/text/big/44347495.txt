Yoogan
 

{{Infobox film
| name           = Yoogan
| image          = 
| alt            = 
| caption        = Yoogan Movie Poster
| director       = Kamal G
| producer       = Kamal G
| writer         = Kamal G
| starring       = Yashmith Sakshi Agarwal Siddhu GRN Pradeep Balaji Shyam Kirthivasan Manoj
| music          = Rashaanth Arwin & Alex Premnath
| cinematography = Ravi Arumugam
| editing        = Kamal G
| studio         = Twins Productions
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
 Tamil horror, suspense thriller film directed by Kamal G. Starring Yashmith and Sakshi Agarwal in the lead roles along with Siddhu GRN, Pradeep Balaji, Shyam Kirthivasan, Tarun Chakarvarthy and Suresh Pillai.

Rashaanth Arwin composed the music For the film. Alex Premnath composed the background score , and cinematography was done by Ravi Arumugam.

It is a horror story which revolves around the problems that happen in an IT company. The movie got released on 24th April 2015.

==Cast==
* Yashmith as Vinay
* Sakshi Agarwal as Pooja
* Siddhu GRN as David
* Pradeep Balaji as Amir
* Shyam Kirthivasan as Rahul
* Manoj as Arun
* Tarun Chakravarthy as ACP
* Suresh Pillai as Raghu
* Venkatrenga Gupta as CEO

==Production==
The movie is produced by Twins Productions, owned by director Kamal G.

==Soundtrack==
Music for the film is composed by debutant Rashaanth Arwin and the background score is composed by Alex Premnath.

==References==
* http://timesofindia.indiatimes.com/entertainment/tamil/movies/news/Yoogan-is-a-horror-story-set-in-an-IT-company/articleshow/45004633.cms
* http://www.bollywoodlife.com/south-gossip/real-make-up-over-vfx-in-horror-film-yoogan/
* http://www.business-standard.com/article/news-ians/real-make-up-over-vfx-in-horror-film-yoogan-114062400355_1.html
* http://www.mediatimez.com/entertainment/tamil/movies-2/yoogan
* http://www.deccanchronicle.com/140625/entertainment-kollywood/gallery/working-stills-horror-tamil-flick-yoogan
* http://movies.sulekha.com/tamil/yoogan/default.htm
* http://www.cinemaglitz.com/news/tamil-movie-news/yoogan-thriller-horror-movie/
* http://www.cinemaglitz.com/news/tamil-movie-news/real-make-up-over-vfx-in-horror-film-yoogan/
* http://www.cinemaglitz.com/news/tamil-movie-news/yoogan-is-a-horror-movie/

==External links==
*  
*  
*  

 
 
 


 