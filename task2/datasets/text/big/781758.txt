Little Women (1994 film)
{{Infobox film
 | name = Little Women
 | image = Little women poster.jpg
 | caption = Original film poster
 | director = Gillian Armstrong
 | producer = Denise Di Novi
 | writer = Robin Swicord
 | based on =   Matthew Walker
 | music = Thomas Newman
 | cinematography = Geoffrey Simpson
 | editing = Nicholas Beauman
 | distributor = Columbia Pictures
 | released =  
 | runtime = 118 minutes
 | country = United States
 | language = English
 | budget = $18 million
 | gross = $50,083,616
}}
 novel of 1933 George 1949 adaptation 1978 television adaptation by Gordon Hessler. It was released exclusively on December 21, 1994, and was released nationwide four days later on December 25, 1994, by Columbia Pictures.

==Plot summary== the war, plays written by Jo in their attic theater.
 John Neville), whose grandson Theodore, nicknamed "Laurie" (Christian Bale), moves in with him and becomes a close friend of the March family, particularly Jo. Mr. Laurence becomes a mentor for Beth, whose exquisite piano-playing reminds him of his deceased daughter, and Meg falls in love with Lauries tutor John Brooke (Eric Stoltz).

Mr. March is wounded in the war and Marmee is called away to nurse him.  While Marmee is away, Beth contracts scarlet fever from a neighbors infant.  Awaiting Marmees return, Meg and Jo send Amy away to live in safety with their Aunt March. Prior to Beths illness, Jo had been Aunt Marchs companion for several years, and while she was unhappy with her position she tolerated it in the hope her aunt one day would take her to Europe. When Beths condition worsens, Marmee is summoned home and nurses her to recovery just in time for Christmas. Mr. Laurence gives his daughters piano to Beth, Meg accepts John Brookes proposal and Mr. March surprises his family by returning home from the war.
 German professor Victorian melodramas she has penned so far.

In Europe, Amy is reunited with Laurie. She is disappointed to find he has become dissolute and irresponsible and scolds him for pursuing her merely to become part of the March family. In return, he bitterly rebukes her for courting one of his wealthy college friends in order to marry into money. He leaves Amy a letter asking her to wait for him while he works in London for his grandfather and makes himself worthy of her.

Jo is summoned home to see Beth, who finally dies of the lingering effects of scarlet fever that have plagued her for the past four years. Grieving for her sister, Jo retreats to the comfort of the attic and begins to write her life narrative|story. Upon its completion, she sends it to Professor Bhaer. Meanwhile, Meg gives birth to twins Demi and Daisy.

A letter from Amy informs the family Aunt March is too ill to travel, so Amy must remain in Europe with her. In London, Laurie receives a letter from Jo in which she informs him of Beths death and mentions Amy is in Vevey, unable to come home. Laurie immediately travels to be at Amys side. They finally return to the March home as husband and wife, much to Jos surprise and eventual delight.

Aunt March dies and she leaves Jo her house, which she decides to convert into a school. Professor Bhaer arrives with the printed galley proofs of her manuscript but when he mistakenly believes Jo has married Laurie he departs to catch a train to the West, where he is to become a teacher. Jo runs after him and explains the misunderstanding. When she begs him not to leave, he proposes marriage and she happily accepts.

==Cast== Josephine "Jo" March, an ambitious young woman who longs to become a successful author. 
* Gabriel Byrne as Friedrich Bhaer, an older professor who falls in love with Jo while he works as a tutor in New York and eventually marries her. Margaret "Meg" March, the oldest March sister.  She marries Lauries tutor, John Brooke, and gives birth to twins, a boy (Demi) and girl (Daisy).
* Kirsten Dunst and Samantha Mathis as Amy March, the youngest March daughter.  Instead of the brown hair and brown or green eyes of her sisters, she has golden curls and blue eyes. She later marries Laurie and becomes a successful painter.  Amy was the only character played by two different actresses; Dunst portrayed her at 12 years old in the first half of the movie, Mathis as a teenager in the second half of the movie.  Elizabeth "Beth" March, the third March daughter and the pianist of the family. She is shy, good, sweet, kindly and loyal. She contracted scarlet fever which weakened her heart and resulted in her death some years later.
* Christian Bale as Theodore "Laurie" Laurence, the young pianist grandson of James Laurence, the March neighbor.
* Eric Stoltz  as John Brooke, Lauries tutor and Megs eventual husband. John Neville as Mr. Laurence, a kindly neighbor.
* Mary Wickes as Aunt March, the only March family member who still had very much money. Upon her death, her estate is left to Jo who transforms it into a school.
* Susan Sarandon as Margaret "Marmee" March, the mother of the March daughters. Matthew Walker as Mr. March, the father of the March daughters.
* Florence Paterson as Hannah, the housemaid of the family March.
* Janne Mortil as Sally Moffat, the rich girl friend of Meg.

==Critical reception==
Roger Ebert of the Chicago Sun-Times awarded the film 3½ stars, calling it "a surprisingly sharp and intelligent telling of Louisa May Alcotts famous story, and not the soft-edged childrens movie it might appear." He added, "  grew on me. At first, I was grumpy, thinking it was going to be too sweet and devout. Gradually, I saw that Gillian Armstrong . . . was taking it seriously. And then I began to appreciate the ensemble acting, with the five actresses creating the warmth and familiarity of a real family." 

Edward Guthmann of the San Francisco Chronicle called the film "meticulously crafted and warmly acted" and observed it "is one of the rare Hollywood studio films that invites your attention, slowly and elegantly, rather than propelling your interest with effects and easy manipulation." 

Little Women has a strong 90% on Rotten Tomatoes based on 30 reviews with the consensus: "Thanks to a powerhouse lineup of talented actresses, Gillian Armstrongs take on Louisa May Alcotts Little Women proves that a timeless story can succeed no matter how many times its told."

==Box office==
The film opened on 1,503 screens in the US and Canada on December 21, 1994. It grossed $5,303,288 and ranked #6 at the box office on its opening weekend and eventually earned $50,083,616.  Against its budget of $18 million, the film was a success.

==Awards and nominations== Best Actress Best Costume BAFTA Award Best Original BMI Film Music Award.

Winona Ryder was named Best Actress by the  .

Robin Swicord was nominated for the Writers Guild of America Award for Best Adapted Screenplay but lost to Eric Roth for Forrest Gump.

==Home video==
The film had its initial North America video release on VHS on June 20, 1995, followed by its initial digital release on DVD on April 25, 2000.

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 