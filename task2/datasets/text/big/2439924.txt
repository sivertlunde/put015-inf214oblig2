The Ascent
 
{{Infobox film
| name = The Ascent
| image = Ascent poster.jpg
| caption = German poster - (left to right) Rybak, the village headman, Sotnikov, the young girl, Demchikha
| director = Larisa Shepitko
| producer =
| writer = Vasil Bykaw (novel Sotnikov) Yuri Klepikov Larisa Shepitko Sergei Yakovlev Lyudmila Polyakova Anatoli Solonitsyn
| music = Alfred Schnittke
| cinematography = Vladimir Chukhnov Pavel Lebeshev
| editing =
| distributor =
| released =  
| runtime = 111 minutes
| country = Soviet Union
| language = Russian
| budget =
}} Golden Bear Best Foreign Language Film at the 50th Academy Awards, but was not accepted as a nominee. 

==Plot==
During the Great Patriotic War (World War II), two Soviet partisans go to a Belarusian village in search of food. After taking a farm animal from the collaborationist headman (Sergei Yakovlev), they head back to their unit, but are spotted by a German patrol. Though the two men get away, Sotnikov (Boris Plotnikov) is shot in the leg. Rybak (Vladimir Gostyukhin) has to take him to the nearest shelter, the home of Demchikha (Lyudmila Polyakova), the mother of three young children. However, they are discovered and captured.

The two men and a sobbing Demchikha are taken to the German camp. Sotnikov is questioned first by the traitor Portnov (Anatoli Solonitsyn). When he refuses to answer Portnovs questions, Sotnikov is brutally tortured by members of the Belarusian Auxiliary Police, loyal to the Germans, but gives up no information. However, Rybak is a different story. He tells as much as he thinks the police already know, hoping to live so he can escape later. The headman, now suspected of supporting the partisans, and a young girl are imprisoned in the same cellar for the night.

The next morning, all are led out to be hanged. Rybak persuades Portnov and the Germans to let him join the police. The others are executed.

As he heads back to the camp with his new comrades, Rybak is vilified by the villagers. Finally realizing what he has done, he tries to hang himself with his belt in the outhouse, but the belt becomes unfastened. He ties it more securely, but cannot summon the courage to go through with it a second time. Exiting the outhouse, he sees the door to the camp open, but breaks down in tears as he realizes he does not dare to try to escape.

==Cast==
* Boris Plotnikov as Sotnikov
* Vladimir Gostyukhin as Rybak
* Sergei Yakovlev as Village elder
* Lyudmila Polyakova as Demchikha
* Viktoriya Goldentul as Basya
* Anatoli Solonitsyn as Portnov, the Nazi interrogator
* Mariya Vinogradova as Village elders wife
* Nikolai Sektimenko as Stas

==See also==
*List of recent films in black and white
*List of submissions to the 50th Academy Awards for Best Foreign Language Film
*List of Soviet submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 
*  at the official Mosfilm site with English subtitles

 

 
 
 
 
 
 
 
 
 
 
 