Power (2013 film)
 
{{Infobox film
| name           = Power
| image          = 
| caption        = 
| director       = Rajkumar Santoshi
| producer       = Feroz Nadiadwala
| writer         = Rajkumar Santoshi R. D. Tailang
| starring       = Amitabh Bachchan Ajay Devgn Anil Kapoor Sanjay Dutt Ameesha Patel Kangna Ranaut
| music          = Pritam Thiru
| editing        = 
| studio         = 
| distributor    = Base Industries Group
| released       = 2016
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} action crime thriller film written and directed by Rajkumar Santoshi and produced by Feroz Nadiadwala under the banner of Base Industries Group. The film has a multiple cast including Amitabh Bachchan, Sanjay Dutt, Ajay Devgn, Anil Kapoor, Ameesha Patel and Kangna Ranaut.  This fims shooting is delayed due to Sanjay Dutts jail sentencing verdict in Arms Act.

==Cast==
* Amitabh Bachchan
* Ajay Devgn
* Sanjay Dutt
* Anil Kapoor as Balraj   
* Ameesha Patel
* Kangna Ranaut
* Anuradha Patel
* Lekha Washington as Sneha 
* Sohail Khan

==Production==
The film was announced on Anant Chaturdashi in September 2010.  Ajay Devgn was initially signed in to play the lead role but he later opted out of the film.  Later, Devgn rejoined the project after continued negotiations. 
It has been announced that Anil Kapoors character Balraj will be bald in the film. Due to Sanjay Dutt prison term due to the 1993 Bomb blasts, The film will release in 2016 after his release from jail.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 

 
 