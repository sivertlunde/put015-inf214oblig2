Wavemakers
 
Wavemakers (original French title Le chant des ondes) is a 2012 Quebec documentary film about the Ondes Martenot, directed by Caroline Martel.  

Martel discovered the Ondes Martenot while making her 2004 film, Le fantôme de l’opératrice, which featured a soundtrack by accomplished "Ondiste" Suzanne Binet-Audet. Martel has stated that in post-screening question periods, people often asked about the mysterious music on the soundtrack.   

The film documents the musical instruments invention by Maurice Martenot, who conceived his instrument after being inspired by the humming and static sounds made by the telegraph machines he operated during the First World War. He built fewer than 300 of the instruments that bear his name during his lifetime, of which there are only about 70 left.    

Wavemakers includes a performance by musician Jonny Greenwood, filmed during a Radiohead soundcheck. Greenwood, who was self-taught on the instrument, stated after his performance Binet-Audet pointed out a technical error in his playing style. The film also features interviews with Jean-Loup Dierstein, an electronic-instrument repairman-turned-Ondes Martenot maker, who was commissioned by Greenwood to build him a new instrument, Binet-Audet, as well as Montreal-based Ondiste Jean Laurendeau.  

==Release==
The film had its world premiere in November 2012 at the Rencontres internationales du documentaire de Montréal.   

==References==
 

==External links==
* 

 

 
 
 
 
 

 
 