The World's Fastest Indian
 
 
{{Infobox film
| name           = The Worlds Fastest Indian
| image          = Worlds fastest indian.jpg
| caption        = Theatrical release poster
| director       = Roger Donaldson
| producer       = Roger Donaldson Gary Hannam
| writer         = Roger Donaldson
| starring       = Anthony Hopkins Diane Ladd Jessica Cauffiel Christopher Lawford
| music          = J. Peter Robinson
| cinematography = David Gribble John Gilbert
| studio         = 2929 Productions Tanlay Productions Cameo FJ Entertainment
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 127 minutes
| country        = United States Japan Switzerland New Zealand
| language       = English
| budget         = $25 million
| gross          = $18,297,790
}} sports drama Indian Scout motorcycle. Munro set numerous land speed records for motorcycles with engines less than 1000 cc at the Bonneville Salt Flats in Utah in the late 1950s and into the 1960s.

The film stars Anthony Hopkins and was written and directed by Roger Donaldson. The film opened in December 2005 to positive reviews  and quickly became the highest grossing local film at the New Zealand box-office taking in $7,043,000;  and taking in over US$18,297,690 worldwide. 

==Plot==
Burt Munro is a sort of folk hero in his hometown of Invercargill, New Zealand, known for his friendly easygoing personality, for having the fastest motorcycle in New Zealand and Australia, and for being featured in Popular Mechanics magazine . However, that recognition is contrasted by his exasperated next-door neighbours, some of whom are fed up with his un-neighbourly habits such as revving his motorbike early in the morning, urinating on his lemon tree, and not mowing his grass. Burt however has a longtime dream, to travel to the USA and test his motorbikes capabilities at the Bonneville Speedway.

Burt is finally able to save enough to travel by cargo ship to Los Angeles, but when he arrives, he experiences bureaucracy#Criticism|bureaucracy, scepticism, and the indifference of big city people.  It is his blunt but gregarious nature which overcomes each hurdle.  He wins over the motel clerk, a transvestite woman named Tina, who assists him in clearing customs and helps him in buying a car. The car salesman allows Burt to use his shop to make a trailer and later offers him a job after Burt fine-tunes a number of the cars on the lot. Burt declines the offer, however, and shortly afterwards begins his long trip to Utah.
 Native American who aids him when his trailer fails, a woman named Ada who allows him to him repair his trailer and briefly becomes his lover, and an Air Force pilot who is on leave from military service in Vietnam.
 
He finally arrives at the Bonneville Salt Flats, only to be blocked by race officials for not registering his bike for competition in advance.  In a show of sportsmanship, however, various competitors and fans in the Bonneville series intervene on his behalf, and he is eventually allowed to make a timed run. Despite various problems, he succeeds in his quest and sets a new land speed record at the 8th mile of run when he reaches 201.851&nbsp;mph (324.847kph).  By the end, his leg is burned by the exhaust, and he then falls with the bike and skids to a stop, but he is able to return home to New Zealand a hero.

==Cast==
* Anthony Hopkins as Burt Munro
* Jessica Cauffiel as Wendy  Joe Howard as Otto Chris Williams as Tina, a transvestite motel clerk
* Paul Rodriguez as Fernando, a used car salesman
* Christopher Lawford as Jim Moffet
* Annie Whittle as Fran
* Aaron James Murphy as Tom
* Chris Bruno as Bob
* Tim Shadbolt as Frank
* Bruce Greenwood as Jerry
* Diane Ladd as Ada
* Saginaw Grant as Jake

==Production==
Director   stated that Munro was one of the easiest roles that he has ever played in his career, simply because Munros view on life was not all that different from his own. Chris Bruno stated this was "one of the most exciting jobs" hes ever done.
 Black Power gang during the 1990s until the house burnt down in 1998. A house has been built on the plot since the film was released. 

==Historical accuracy==
 
 stillborn twin sister. The historical Munro had set numerous speed records in New Zealand during the late 1930s through the early 1970s. However, these records are only implied in the film. Munro was never known to urinate on his lemon tree; film director Roger Donaldson added that detail as a tribute to his own father, who did.

The Bonneville run in the film is a composite of several runs Munro made, the first in 1956. The May 1957 edition of Popular Mechanics (p6) has a letter to the editor about H.A. "Dad" Munro and his 1920 Indian Scout.   In 1962 at Bonneville, he set the record of 178.971&nbsp;MPH. Munros fastest complete run at Bonneville was 190.07&nbsp;MPH. He never set a record of 201&nbsp;MPH at Bonneville as the film portrays, but did reach 205.67&nbsp;MPH on an uncompleted run, on which he unfortunately crashed.  Munro does crash after his 201&nbsp;MPH record-breaking run, which is officialised unlike the 205.67&nbsp;MPH run.

Near the end of the film, Speed Week participants throw money into "the hat" and Burt Munro is presented with a bag of cash before he sets the speed record.  In fact, Munro had to take up a collection before Speed Week as U.S. Customs required a cash bond before releasing his motorcycle.  A photograph of the gathered crowd presenting Munro with the bag of cash, in 1962, was reprinted in a 2006 issue of Hot Rod magazine.

==Hopkins portrayal of a New Zealander== Welsh valleys high veldt without ever alighting in Southland Region|Southland"  . Nevertheless the same reviewer said Hopkins gives a "generous, genial and utterly approachable performance … he nails the backyard eccentric genius dead centre … he has inhaled the nature of a mid-century Kiwi bloody good bloke and he inhabits the part to perfection". 

==Featured vehicles== Indian Scout portrayed in the film started life as a 600 cc motorcycle with a top speed of 55&nbsp;mph. Munro’s streamlined record setter was largely modified with capacity being increased to 950 cc, and a recorded speed of 205&nbsp;mph. These modifications were largely done in Munro’s shop using primitive methods and tools as depicted in the film. Four bikes are used in the film to depict the actual "Munro Special" two of which were replica Indian Scouts, the others being Ducatis.

Antarctic Angels Bikes: The bikes ridden by the New Zealand bikers comprise largely of early to mid-1960s Triumphs and BSAs. Most of these bikes are twin cylinder models and are 500 cc or larger. Burt’s Car (New Zealand): 1954 Vauxhall Velox. The Vauxhall Velox depicted as Burt’s car in New Zealand is typical of the many British built cars exported to New Zealand and Australia in the mid-1950s. Assembled In Bedfordshire England, the Velox used a number of inline six cylinder engines and was produced until 1965.

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 