Virodhi (1992 film)
{{Infobox film
| name           = Virodhi
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Rajkumar Kohli
| producer       = Rajkumar Kohli
| writer         = 
| screenplay     = 
| story          = Lalit Mahajan
| based on       =  
| narrator       = 
| starring       = Dharmendra Sunil Dutt Armaan Kohli Anita Raj Poonam Dhillon  Harsha Mehra
| music          = Anu Malik
| cinematography = Thomas Xavier
| editing        = Shiv Kumar Sharma
| studio         = 
| distributor    = 
| released       =    
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film produced and directed by Rajkumar Kohli. It stars Dharmendra, Sunil Dutt, Armaan Kohli, Anita Raj, Poonam Dhillon and Harsha Mehra in pivotal roles. 

==Plot==
Inspector Shekhar gets involved in a drug racket case which involves ministers like Pandey Sahib & his allianceses.  When Inspector Shekhar tries to bring justice against Pandey Sahib & his allianceses,  he gets stripped off from police force & gets killed by thugs & hooligans of Pandey Sahib. Its up to his brother Raj (whom is Shekhars small brother) to avange his brothers death by trusting the LAW & JUSTICE or take law in his own hands by finishing minister Pandey & allianceses by becoming "VIRODHI."  

==Cast==
* Dharmendra...Inspector Shekhar
* Armaan Kohli...Raj
* Anita Raj...Mrs. Shekhar
* Harsha Mehra... Pinky
* Sunil Dutt...Police Commissioner
* Roopa Ganguly...Reena
* Prem Chopra...Bhagwat Pandey
* Shakti Kapoor...Pratap
* Amjad Khan...Judge
* Paresh Rawal...Badrinath Pandey
* Gulshan Grover...Rocky (Pandeys Son)
* Sharat Saxena...Sub-Inspector
* Shiva Rindani...Sub-Inspector

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Tere Mere Pyar Ka Aisa Naata"
| Kumar Sanu, Mohammed Aziz, Sarika Kapoor
|-
| 2
| "Nain Kabootar Ud Gaye Dono"
| Kumar Sanu, Asha Bhosle
|-
| 3
| "Chullu Bhar Pani Mein Doob Ja"
| Udit Narayan
|-
| 4
| "Jaanam Jaanam Jaanam"
| Kumar Sanu, Asha Bhosle
|-
| 5
| "Ek Chumma De De"
| Amit Kumar
|}

==References==
 

==External links==
* 

 
 
 

 