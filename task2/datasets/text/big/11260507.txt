Zibahkhana
 
{{Infobox film
| name           = Zibahkhana: Hells Ground
| image          = Zibahkhana 2007 film poster.jpg
| caption        = Film poster
| director       = Omar Ali Khan
| producer       = Pete Tombs
| writer         = Omar Ali Khan
| narrator       =
| starring       = Ashfaq Bhatti Sultan Billa Osman Khalid Butt Rubya Chaudhry Rooshanie Ejaz Najma Malik Salim Meraj Haider Raza Rehan Kunwar Ali Roshan
| music          = Stephen Thrower
| cinematography = Najaf Bilgrami
| editing        = Andrew Starke
| distributor    = Mondo Macabro
| released       =  
| runtime        = 80 minutes
| country        = Pakistan
| language       = Urdu English
| budget         = 
| gross          = 
}}
Zibahkhana (English: Hells Ground) is a 2007 Urdu-English slasher film directed by Omar Khan. It premiered at the NatFilm Festival in Denmark and has been screened at festivals all over the world including Toronto, New York City, London, Neuchatel, Stockholm, Cape Town, Austin, Philadelphia, Cambridge, Puerto Rico, Sitges, Valencia, Oslo, and Helsinki.   The film passed censors in Pakistan (with 9 seconds cut) and became the first non 35mm, HDV feature film released in Pakistan cinema history. It also holds the distinction of being shot in 30 days. 

== Major characters ==
===Ayesha===
Ayesha is introduced as the Final girl played by Rooshanie Ejaz. She is the modest one of the group, she has the most trouble getting permission to go on the road trip from her mother and does not smoke Marijuana like her friends. She is very religious and wears a necklace with "Allah hu Akbar" engraved on it.

===Baley=== The Texas Chainsaw Massacre. Baleys mother refers to him as a Healer (alternative medicine)|healer, he is introduced in a small tent reciting something, surrounded by candles, he gets into Vickys van uninvited and gives them directions. He demands water and the teenagers tell him they dont have any, he retaliates by showing them a severed head of a previous victim and is immediately kicked out of the van. He is played by Salim Meraj.

===Bari Bua===
Played by Najma Malik, Bari Bua is the mother and dominant member of the killers family. She is initially shown as a sweet, hospitable woman living in a tent. She is widowed and she keeps the remains of her husband in a closet with his photograph attached to his skull. She dislikes urban development as the new highways are cutting the rest of Pakistan from her humble village, but she does mention she is grateful to Allah as many customers come from the cities to purchase her "goats".

===Burqaman===
Played by Sultan Billa, the Burqaman is a large, hefty butcher wielding a morning star. The character is shown as a cross dresser, his history shows that he was born a boy, but as he grew up he became more feminine, started wearing make up and dressing like a girl. He is mute throughout the film, the only sound he makes is loud, heavy breathing. He works in a workshop where he butchers stranded travelers, his profile is similar to Leatherface, he also has one hole in his mask to see like Jason Voorhees in Friday the 13th Part II.

===Deewana=== Queen Elizabeth, Lady Diana, Angelina Jolie and Brad Pitt have visited his shop.

===O.B.===
A teenage stoner from Islamabad played by Osman Khalid Butt, he is a fan of classic horror movies and is often seen smoking weed, he is the first of the group to be attacked though he survives it, he later becomes a zombie and tries to attack Ayesha.

===Roxy===
A promiscuous teenage girl played by Rubya Chaudhry. She is wealthy and spoiled by her family, she is Pakistani American and longs to go back to the U.S. saying "Nothing can keep me in this stinkin shithole".

===Simon===
Simon is a lower classed, but well educated boy from the minority Christian group of Pakistan. He is bullied by his father who demotivates him from his studies telling him that the son of a sweeper will always be a sweeper. Like Ayesha he is slightly more innocent than his friends. He is played by Haider Raza.

===Vicky===
The unofficial leader of the group played by Kunwar Ali Roshan, he tries to be the dominant one and shows a courageous attitude whenever there is trouble, he drives the van for the group and is shown to be "in between" innocent and rule breaker, he tells O.J. to light up the charas but at the same time he is not obsessed with it, he isnt shown doing any drugs.

==Critical reception==
Director Omar Khan was appreciated a lot for it.

== Awards ==
* Won "Jurys Award for Best Film of 2008" at the Riofan Film Festival, Rio de Janeiro, Brazil
* Won "Jurys Special Award for Best Gore 2007" at the Fantastic Film Festival, Austin, Texas 
* Won "Best Film" award at the Fantaspoa film festival 2009. Port Alegre, Brazil

 
== References ==
 
*   2007 awards coverage
*   coverage of the Fantastic Festival awards, 2007

== External links ==
* 31 Flavors of Death – Bidoun magazine profile by Achal Prabhala  
*  
*  
*  
*   at Fantastic Film Festival 2007, Austin, Texas
*   at the 2007 Philadelphia Film Festival
*   at Rotten Tomatoes

 
 
 
 
 
 
 
 