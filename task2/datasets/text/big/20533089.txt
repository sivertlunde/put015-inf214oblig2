Goa (film)
 
{{Infobox film
| name           = Goa
| image          = Goa-thefilm.jpg
| image_size     =
| caption        = Theaterical Poster
| director       = Venkat Prabhu
| producer       = Soundarya Rajinikanth
| writer         = Venkat Prabhu Jai Vaibhav Sneha Piaa Bajpai Melanie Marie Jobstreibitzer

| music          = Yuvan Shankar Raja
| cinematography = Sakthi Saravanan
| editing        = Praveen K. L. N. B. Srikanth
| studio         = Ocher Studios Ocher Picture Productions
| released       =  
| runtime        = 164 minutes
| country        = India
| language       = Tamil
| budget         =  80&nbsp;million 
| gross          =  125&nbsp;million
| preceded_by    =
| followed_by    =
}} Tamil adult Vaibhav and Prasanna make guest appearances in the film, which features music composed by Yuvan Shankar Raja, whilst cinematography is handled by Sakthi Saravanan and the film is edited by K. L. Praveen and N. B. Srikanth.
 Caucasian girl whilst on holiday there. The film explores their time in Goa, the people they meet ranging from gay hoteliers to suave casino owners, and dwells on the relationships they encounter in the region.
 using the same title and released in 2015.

== Plot == karagattam artist, Vinayakam/Vinay (Jai), son of a military man, and Ramarajan/Ram (Vaibhav Reddy), son of the local bigwig and the village Casanova, are three rebellious youths who try various attempts to flee their village and see the outside world but are usually caught and punished by the conservative elders. After being punished harshly, they attempt one last escape and succeed. The trio runs away to the town of Madurai, hoping to stay with Vinayakams friend Azhagar.

On arriving at Azhagars place, they find him getting married to an American woman named Angelina Jolie. Azhagar tells them that he had met her in Goa and that they will spend their honeymoon in London. The three friends are inspired to travel to Goa, meet a foreign woman there and marry her. Upon realizing that they have mistakenly brought along a sacred golden jewel from their village temple and thus can never return, they pack their bags and head to Goa.

At Goa, they meet a fellow Tamil named Jack (Aravind Akash) who provides food and shelter and introduces them to the party culture of Goa. Vinayakam falls in love with Roshini (Piaa Bajpai), a club singer, while Samikannu attempts to woo an American named Jessica Alba (Melanie Marie) whom he had seen earlier at the wedding in Madurai. The three friends are given a makeover by Jack and Roshini, and they begin a new lifestyle in Goa.

At this point, enters hotelier Danny (Sampath Raj), who is Jacks gay lover. Danny is attracted by Samikannus innocence and begins making affectionate advances toward him; Samikannu is oblivious. This invokes the jealousy of Jack, who hires ninja-themed henchmen to beat up Samikannu. In an awkwardly heroic scene, Sami nullifies their attack.

Ramarajans life takes a twist after he comes across Suhasini Fernando ( ). The marriage takes a bitter turn, and Ram turns to his friends for help. Together they plan a covert mission and successfully retrieve the jewel.

Vinay and Sami are successful in their romance. The three boys eventually return to their native village, along with Roshini, Jessica, Danny and Jack. There, a surprise awaits Ram, in the form of Nayantara. The epilogue takes place in Goa, showing Suhasini falling in love with Madhan Kumar (Silambarasan Rajendar). As they hug, Madhans nose begins to bleed, indicating that Suhasini has become a victim of Manmadhan (film)|Manmadhan.

The film parodies various themes prevalent in Tamil cinema, as well as many popular Tamil songs. Director Venkat Prabhu makes several cameos throughout the film.

== Cast == Jai as Vinayagam Manickam (Vinay)
* Vaibhav Reddy as Ramarajan Ambalavanar (Ram)
* Premji Amaran as Samikannu (Sam)
* Aravind Akash as Jack
* Sampath Raj as Daniel (Danny) Sneha as Suhasini Fernando
* Piaa Bajpai as Roshini
* Melanie Marie as Jessica Alba

;Guest appearances (in order of appearance) Manmadhan
* Prasanna as Shakthi Saravanan
* Nayanthara as the New Girl in Village

== Production ==
=== Development ===
Even before the release of his second directorial venture     starring Soundaryas father Rajinikanth, directed by Soundarya herself. Interestingly, the script was first narrated to Rajinikanth; following Rajinikanths recommendation, his daughter got to listen to the script and she immediately agreed to produce it. 

In April 2009, Warner Bros. was said to have backed out of the project, bringing forward the argument that they asked for complete details about the film, including script and expenses. Since the Goa team was not able to do so, because they were running out of time regarding the actors dates and couldnt provide the details, Warner Bros. opted out.  In May 2009, producer Soundarya Rajinikanth denied that Warner Bros. opted out of the project, disclosing that "any rumor that suggests otherwise is simply not true", that Warner Bros. and Ocher Studios have "established a very successful relationship" and that they are still a part of the Goa project.  
 American Pie Road Trip.  Moreover, it was rumored, that the film was based on a real-life incident, the murder case of the 15-year-old English teenager Scarlett Keeling       that happened in Goa in February 2008.  However, this plot was not part of the completed film.

=== Casting ===
Following the successes of Chennai 600028 and Saroja (film)|Saroja, director Venkat Prabhu stated he would continue his trend of introducing new actors and using relatively young actors for his next film. The four lead male characters were announced to be Jai (actor)|Jai, Aravind Akash, Vaibhav Reddy and Premji Amaren, with the former two collaborating with Venkat after their roles in Chennai 600028, whilst Vaibhav was a part of Venkats Saroja. Premji, Venkats brother, had been a part of both of Venkats previous ventures and was consequently signed on for a role. Supporting actor Sampath Raj was also signed, renewing his association with Venkat, appearing in his third straight film.
 Sneha and Mamma Mia! fame,  before the role was eventually handed to popular American television actress and model Julie Fine.     However Fine, too, was no longer part of the project, as she was surreptitiously replaced by a Swedish actress and model, Nouva Monika Wahlgren, who predominantly works in India, having earlier acted in several Indian films, including Fun Aur Masti and the Shriya Saran-starrer The Other End Of The Line. She also appeared in Indian advertisements and who was finally said to be confirmed to play the foreign character. Before Wahlgren was roped in, it was  reported that an Australian actress, Melanie Marie Jobstreibitzer, was approached and confirmed for the role.  Eventually, it turned out, that indeed Melanie Marie was roped in for the role. Early rumors suggested, that the foreign girl would play the murdered Scarlett Keeling, which were later revealed to be fictitious,  with the character actually portraying Premji Amarens pair. 
 Sneha resulted villainous role as described by the media.  The role initially offered to Genelia was subsequently taken over by Pooja Umashankar|Pooja.   According to reports, Pooja was signed after Preetika Rao, sister of Hindi film actress Amrita Rao, who was supposed to do the role, backed out at last-minute for reasons unknown.  In June 2009, however, Piaa Bajpai, who had acted in films like Poi Solla Porom and Aegan earlier, joined the crew,  and was said to have replaced Pooja again, who was apparently busy completing her Sinhalese projects.    Bajpais role plays the love interest of Jai. 

In November 2009, additionally Nayantara was roped in to do a cameo appearance in the last scene of the film.       Reports indicated that she shot for the scene, involving herself and the lead cast, for one day without taking any remuneration as she was a "big fan of Venkat Prabhus movies". 

The technical crew remained mostly the same as in Venkat Prabhus earlier ventures. His cousin Yuvan Shankar Raja composes the musical score, whereas his songs featured lyrics provided by veterans Vaali (poet)|Kavignar Vaali and Gangai Amaran. Sakthi Saravanan, Videsh and Vasuki Bhaskar were signed as the cinematographer, art director and costume designer, respectively, joining Venkat Prabhu for the third time in his third venture. Praveen K. L. and Srikanth N. B. were selected to edit the film, whilst dance moves were choreographed by Kalyan again, who is joined by Ajay Raj and Saravana Rajan. Whilst the former had worked with Venkat Prabhu in his debut venture, Chennai 600028, the latter was part of Prabhus second film, Saroja. Kalyan was a choreographer in all Venkat Prabhu films.

=== Filming === Sneha and Premji Amaran.  The shooting was slated to start on 11 March 2009 but had to be postponed, because of the complaint lodged against actor Jai at the Tamil Film Producers council, who had stated that some of his forthcoming films would flop at the box office.   Producer Soundarya Rajinikanth is said to have approached the council and requested to lift the ban, explaining the loss due to the ban, after which the ban on Jai was revoked and the filming started.  

The first schedule of film shooting was held in Theni, Pannaipuram and surrounding places in Theni district in Tamil Nadu. It included a folk song, which is said to be the introduction song and which was shot from 31 March 2009 onwards in Goa featuring Jai, Vaibhav, Aravind Akash, Premji and Sneha, along with Ajay Raj, who choreographs the song, Payal and around 500 Goans. 
 Langkawi Islands, monsoon season that had set in heavy rains, affecting the west coastal area of India.  He chose Langkawi as it has "very similar locations and looks" to Goa and has a very good climate.  It was reported that music director Yuvan Shankar Raja would perform one of the songs himself live on the beaches  there.  In late 2009, two more songs were filmed in Kerala and Goa itself, with which the films shooting was completed.

==Release== Sun TV. The film was given an "A" certificate by the Indian Censor Board., reportedly because of its content that "requires a mature audience",  while also describing it as a "path breaking entertainer which explores human sexuality very beautifully".    The film released with over 200 prints in Tamil Nadu.  The films release, initially expected to happen in late 2009, got postponed several times. It was released on 29 January 2010 worldwide with Thamizh Padam and the controversial Jaggubhai.

==Reception==
The film opened to mostly positive reviews especially from the youth, who were Venkat Prabhus target. It also received mixed reviews. On the opening weekend, it collected 11.2&nbsp;million at Chennai alone.      The movie showed highly positive return and initially topped at the box office collections before becoming an average grosser.   
   

===Accolades===
{| class="wikitable" style="font-size:95%;"
|-
! Ceremony
! Award
! Category
! Name
! Outcome
|- Edison Awards 
| Edison Award Best Romantic Movie
|
| 
|-
|rowspan="5"| 2011 Vijay Music Awards 
|rowspan="5"| Vijay Music Award Best Folk Song of the Year 2010 Adida Nayandiya
| 
|- Popular Melody of the Year 2010 Idhu Varai
| 
|- Popular Duet of the Year 2010 Andrea Jeremiah & Ajeesh for Idhu Varai
| 
|- Popular Female Singer of the Year 2010 Andrea Jeremiah for Idhu Varai
| 
|- Best Debut Male Playback Singer (Jury) Ajeesh
| 
|-
|rowspan="3"| 5th Vijay Awards
|rowspan="3"| Vijay Award Vijay Award for Best Supporting Actor Sampath Raj
| 
|- Vijay Award for Best Female Playback Singer Andrea Jeremiah
| 
|- Vijay Award for Best Lyricist Gangai Amaran
| 
|-
|rowspan="3"| Mirchi Music Awards 
|rowspan="3"| Mirchi Music Award Mirchi Listeners’ Choice – Best Song of the Year Idhu Varai
|  
|- Technical – Sound Mixer Ramji & Guru for Idhu Varai
| 
|- Best Upcoming Singer of the Year – Male Ajeesh for Idhu Varai
| 
|}

== Soundtrack ==
 

Film score and soundtrack for Goa are composed by Venkat Prabhus cousin and regular music composer, Yuvan Shankar Raja. Since approximately the first twenty minutes of the film are set in the 1980s, it is known that the music for the affected scenes and a couple of the songs were recorded live (as it was done during that time) to "do justice to the scenes".  Yuvan Shankar Raja had started the re-recording works for the film score on 21 December 2009, after having watched the entire film  and had finished it within seven days with the help of his cousin and assistant Premji Amaran.   

The soundtrack, after several postponements, was finally released on 4 January 2010.    The album features 9 songs overall.    18 singers have lent their voices for the songs, including composer Yuvan Shankar Raja, his father Ilaiyaraaja, his siblings Karthik Raja and Bhavatharini, his cousins Premji Amaran and director Venkat Prabhu,   S. P. Balasubrahmanyam,  K. S. Chitra and actress-singer Mamta Mohandas.   

Contrary to earlier reports that suggested that merely three or four numbers would feature in the film,  seven out of the nine songs were used in their entirety, besides two more additional tracks. 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 