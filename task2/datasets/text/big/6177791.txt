Invaders from Space
 
{{Infobox Film name            = Invaders from Space  image           = Invaders from Space.JPG caption         = Opening titles director        = Teruo Ishii producer        = Mitsugi Okura writer          = Ichiro Miyagawa starring        = Ken Utsui  music           =  cinematography  = editing         = distributor     =Walter Manley Enterprises Inc.  released        =   runtime         = 78  min. (USA) language        = English (dubbed) budget          =
|}}
Invaders from Space is a 1965 film edited together for American television from films #3 and #4 of the Japanese short film series Super Giant.

==Plot==
The story involves the superhero Starman who is sent by the Emerald Planet to protect Earth from the Salamander Men from the Planet Kulimon.

==American adaptation==
The 9 Super Giant films were purchased for distribution to U.S. television and edited into 4 films by Walter Manley Enterprises and Medallion Films. The 2 original Japanese films which went into Invaders from Space (The Mysterious Spacemens Demonic Castle and Earth on the Verge of Destruction) were 48 minutes and 39 minutes in duration. The two films were edited into one 78-minute film, resulting in a total of 9 minutes being cut from the two films during the re-editing. Also, most of the original music was replaced by library cues.

==DVD release==
Invaders from Space is currently available on DVD. Something Weird Video with Image Entertainment released the film and the other Starman film, Atomic Rulers of the World on a single disc on December 10, 2002. 
 
== See also ==
* Super Giant
* Atomic Rulers of the World
* Attack from Space
* Evil Brain from Outer Space

==References==
*Ragone, August. THE ORIGINAL "STARMAN"; the Forgotten Supergiant of Steel Who Fought for Peace, Justice and the Japanese Way Originally published in Planet X Magazine, included in Something Weird Videos DVD release.

== External links ==
*  
*  

 
 
 
 
 
 


 