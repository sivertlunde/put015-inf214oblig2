Valley of Angels
{{Infobox film
| name           = Valley of Angels
| image          = Valley of Angels North American Poster.jpg
| caption        = North American poster
| director       = Jon Rosten
| producer       = Edwin Johnston Jon Rosten Spencer Winslow
| writer         = Jon Rosten
| starring       = Danny Trejo George Katt
| music          = Mark Petrie
| cinematography = Armando Salas
| editing        = Jon Rosten
| distributor    = Peace Arch Entertainment ITN Distribution
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         =
}}
Valley of Angels is a 2008 urban action drama film written and directed by Jon Rosten.  The film is about a young West LA drug dealer who searches for a path out of his dangerous existence. 

==Plot==
The Grand Prize winner of the 2004 WriteMovies.com International Screenwriting Contest, Valley of Angels follows a twenty-something drug dealer who falls into the dark side of the underworld after realizing that the City of Angels has lost its soul. Zachary "Zeus" Andrews is a Chicago transplant who moved to Los Angeles as a young boy, and quickly realized that he would never fit in there. A stranger in a hostile landscape, Zeus quickly became the go-to guy for rich kids in search of drugs. One day, after meeting a girl, he receives a glimpse of his destiny. Now, just as Zeus becomes convinced that he is finally on the path to enlightenment and begins preparing to leave his old life behind, the harsh reality of his grim existence strikes back with vengeance.

==Cast==
* George Katt as Zachary Zeus Andrews
* Danny Trejo as Hector
* Caroline Macey as Lisa
* Renee George as Sandra
* Heather Trzyna as Natalie
* Miguel Angel Munguia as Miguel
* Jessica Martine as Maria

==Production==
Casting for Valley of Angels started in March 2005.  The film was shot between Thanksgiving and Christmas of that year with a budget of only $200,000.  It debuted in March 2007 at the NY International Independent Film & Video Festival where it took the Grand Jury Prize for Best Picture.  The film was released in North America on DVD in February 2008.   

==Awards==
*N.Y. International Independent Film & Video Festival:
**Won: Grand Jury Award for Best Feature
**Won: Best Actor (Danny Trejo)
**Won: Best Breakthrough Actor (George Katt) 
*Writemovies.com International Screenwriting Competition:
**Won: Grand Prize - top script out of 1,000 entries 
  

==Reception==
Independent Film Quarterly says that the film is "a debut feature that is both as confident and skillful as Reservoir Dogs, while containing the spiritual contemplation often inherent in Scorsese’s films." 
 

==References==
 

==External links==
*  
*  
*  
* Writer/Director Jon Rostens interview with IFQ:  

 
 