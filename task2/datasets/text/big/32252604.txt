Destiny Express Redux
{{Multiple issues|
 
 
}}

{{Infobox film
| name           = Destiny Express Redux
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Tobe Hooper
| producer       = Eric Laughlin
| writer         = 
| based on       =  
| starring       = Theo Morrison Helen Leary Claire Craft 
| music          = 
| cinematography = Darren Allen 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
}}
Destiny Express Redux is a non-existent 2009 film by American director Tobe Hooper that exists only in the confines of his novel, Midnight Movie. Conceived and filmed originally as a student project in 1959 under the title Destiny Express, this version was screened only once, as part of the South by Southwest festival in 2009. 

==Cast==
{| class="wikitable"
|-
! Name !! Role
|-
| Theo Morrison || 
|-
| Helen Leary || 
|-
| Claire Craft || 
|-
|}

==Release==
The film was met with enthusiasm at the screening, but was not shown again due to complicated ongoing reception from the screening audience. Despite this, Hooper revisited the project in 2009 and 2010, working with executive producer Eric Laughin to re-boot the project. Hooper and a small crew re-created the project shot for shot, and screened first at Regal Arbor Cinema in Austin, Texas. 

Hooper currently has no plans to pursue further distribution of the film. As documented in his 2011 book Midnight Movie, Hooper "has not left the house since" filming and screening Destiny Express.

==References==
 

==External links==
*  

 

 
 
 


 