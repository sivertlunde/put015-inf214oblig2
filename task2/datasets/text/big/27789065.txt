A Death in Canaan
{{Infobox television film
| name           = A Death in Canaan
| image          =
| caption        =
| genre          = Drama
| director       = Tony Richardson
| producer       = Robert W. Christiansen Anna Cottle
| writer         = Joan Barthel Spencer Eastman Thomas Thompson
| screenplay     =
| story          =
| based on       =  
| starring       = Stefanie Powers Paul Clemens Brian Dennehy
| music          =
| cinematography = James Crabe
| editing        = Bud S. Smith
| studio         = Chris/Rose Productions Warner Bros. Television
| distributor    = CBS
| released       =  
| network        = CBS
| runtime        = 125 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
A Death in Canaan is a 1978 American drama television film directed by Tony Richardson and starring Stefanie Powers, Paul Clemens, and Brian Dennehy. 
 
  Its plot concerns the true-life story of a teenager who is put on trial for the murder of his mother in a small Connecticut town. 

The film first aired on the CBS Wednesday Night Movies on March 1, 1978 and was never officially released on any analog or digital medium for rental or sale.

==Cast==
* Stefanie Powers - Joan Barthel
* Paul Clemens - Peter Reilly Tom Atkins - Lt. Bragdon
* Jacqueline Brookes - Mildred Carston
* Brian Dennehy - Barney Parsons
* Conchata Ferrell - Rita Parsons
* Charles Haid - Sgt. Case
* Floyd Levine - Thomas Lanza Kenneth McMillan - Sgt. Tim Scully
* Gavan OHerlihy - Father Mark
* Yuki Shimoda - Dr. Samura
* James Sutorius - Jim Barthel
* Bonnie Bartlett - Teresa Noble
* William Bronder - Judge Revere
* Pat Corley - Judge Vincet
* Art Mehr - Drugstore Manager Art
* Charles Hallahan - Cpl. Sebastian Mary Jackson - Sarah Biggens
* Sally Kemp - Barbara Gibbons
* Doreen Lang - Nurse Pynne
* Lane Smith - Bob Hartman
* Michael Talbott - Trooper Miles

==Location== Ferndale and Eureka, California. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 


 