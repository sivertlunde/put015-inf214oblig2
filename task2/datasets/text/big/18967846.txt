Hoodoo Ann
 
 
{{Infobox film
| name        = Hoodoo Ann
| image       = Screen Acting 1921 page 95 Mae Marsh Hoodoo Ann.png
| caption     = Mae Marsh in scene from film
| producer    = D. W. Griffith
| writer      = D. W. Griffith (screenplay)
| starring    = Mae Marsh Robert Harron William H. Brown Wilbur Higby Loyola OConnor Mildred Harris
| director    = Lloyd Ingraham
| distributor = Triangle Film Corporation
| released    =  
| runtime     = 64 minutes
| language    = Silent film English intertitles
| country     = United States
| budget      =
}}
Hoodoo Ann is a 1916  American comedy-drama silent film, written by D.W. Griffith, directed by Lloyd Ingraham and released by Triangle Film Corporation.

==Plot==
Ann (Mae Marsh) is a young girl who has been living in an orphanage since infancy. She is disliked and spurned by the other children, and treated coldly by the orphanage administrators—the reason for this, however, is unclear.  She is told by the orphanage cook Black Cindy (Madame Sul-Te-Wan) during a palm-reading that she will be cursed until she is married. Anns stay at the orphanage is an endless series of unhappy circumstances; she steals a doll belonging to a popular girl named Goldie (Mildred Harris), then accidentally breaks the doll, thereby adding to her loneliness and misery. One day, while the children are napping, a fire breaks out in the orphanage and Ann heroically saves Goldie from the flames.
 Charles Lee) lying on the floor. Believing she killed Mr. Higgins, Ann is despondent, sure that the curse is still upon her and fearful that Jimmie will never marry her now that she has committed murder.

After tearfully confessing to her "crime" and a subsequent investigation into the peculiar disappearance of the body of Mr. Higgins, the town is shocked when Mr. Higgins returns home several days later and reveals that he had simply left town to avoid his wifes incessant nagging. Overjoyed, Jimmie and Ann marry and the "hoodoo" is lifted.  But the wedding ceremony is not entirely a happy affair; Ann appears distracted and pensive throughout, leaving the viewer to wonder if she perhaps believes that the curse is still upon her. 

== Cast ==

{| border="1" cellpadding="4"  cellspacing="0"
|- bgcolor="#CCCCCC"
! Role || Actor
|-
| Hoodoo Ann  || Mae Marsh
|-
| Jimmie Vance || Robert Harron
|-
| Wilson Vance  || William H. Brown
|-
| Samuel Knapp || Wilbur Higby
|-
| Elinor Knapp || Loyola OConnor
|-
| Goldie || Mildred Harris
|-
| Miss Prudence Scraggs  || Pearl Elmore
|-
| Sarah Higgins || Anna Dodge
|- Charles Lee
|-
| Officer Lambert || Elmo Lincoln
|-
| Constable Drake || Carl Stockdale
|-
| Black Cindy || Madame Sul-Te-Wan
|}

==References==
 

== External links ==
* 
* 
* 

 
 
 
 
 
 