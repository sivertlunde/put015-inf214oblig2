Moondance Alexander
{{Infobox film
| name           = Moondance Alexander
| image          = Moondance_Alexander_poster.jpg
| caption        = Theatrical poster
| director       = Michael Damian
| writer         = Janeen Damian
| producer       = Brad Krevoy
| starring       = Kay Panabaker Don Johnson Lori Loughlin
| released       =  
| studio         = Motion Picture Corporation of America
| distributor    = Fox Faith
| country        = United States
| language       = English
}}
Moondance Alexander is a comedy-drama film directed by Michael Damian and written by Janeen Damian. The film was released in North America in October, 2007. The story is shot on location in Okotoks, High River and Calgary, Alberta, Canada  and is based on actual events from the life of Janeen Damian. It stars Kay Panabaker as the title character and Lori Loughlin as her eccentric mother. Don Johnson and Olympic-skating silver medalist Sasha Cohen also star.

==Plot==
Moondance (Kay Panabaker) is faced with the difficulties of her fathers passing and her mother moving on. When she finds a lost pinto horse and discovers his jumping abilities, she convinces his owner Dante Longpre (Don Johnson) to train them to compete in a jumping show. With a lot more to think about, Moondance has to worry about her enemy Fiona Hughes (Sasha Cohen) putting her down constantly with her fancy horse Monte Carlo. When they do a surprisingly good job at the show, Dante isnt questioned anymore about his ability to train riders and horses. Everybody is shocked when Moondance ties Fiona, the reigning Bow River Classic champion.

==Cast==
*Kay Panabaker as Moondance Alexander
*Don Johnson as Dante Longpre
*Lori Loughlin as Gelsey Alexander
*James Best as Mr.McClancy
*Sasha Cohen as Fiona Hughes
*Whitney Sloan as Megan Montgomery
*Joe Norman Shaw as Ben Wilson
*Aedan Tomney as Josh Wilson
*Mimi Gianopulos as Bella
*Landon Liboiron as Freddie

== Behind the scenes ==
Moondance Alexander was not well received by HorseChannel.com for storyline believability reasons.    However, the story is closely based upon Janeen Damians childhood experience of befriending a horse that she had found, receiving help from a trainer, and entering the horse in the Memphis Classic.   
 Micheal Damian Daytime Emmy Tom Weir was the mixing engineer at Studio City Sound.

Checkers the horse was played by three horses, a horse for tricks, a horse for jumping and horse for Kay Panabaker to ride. Michael Damian recalled two of the horses names as Picasso and Trigger. 
 General Audiences rating and was filmed in Alberta, Canada. 

 
|+ 
<!-- Begin comment, I had problems finding references and determinations for the following awards Won code =   or were simply Nominated code =  
|-
| 2007
| Jury Prize
| Vienna International Kider Film Festival
|  
|-
| 2007
| Best Family Film 
| Newport Beach Film Festival
|  
|-
| 2007
| Best Screenplay
| L.A. Femme Film Festival
|  
|-
| 2007
| Best Family Film
| Calgary International Film Festival
|  
End comment -->
|-
| 2007
| Best Picture
| Dixie Film Festival    
| 
|- 2007
| Best Actress (Kay Panabaker)
| Dixie Film Festival  
|   
|}

== Soundtrack ==
The Moondance Alexander Original Soundtrack was the second soundtrack released under the Caption Records label. Featured artists who also performed on Caption Records Teen Witch the Musical soundtrack are denoted with "*".

=== Featured Vocalists ===

 
* Mimi Gianopulos
* Lea Herman   
* Tessa Ludwick *
* Buck McCoy
* Monét Monico
* Sara Niemietz *
* Ian Walsh
* Laura Wight
* Heather Youmans *
 

{{Discography list
| Type          =   soundtrack
| Name          =   Moondance Alexander Original Soundtrack
| Other info    =   by various artists
| from Album    =    
| Released      =   09 October 2007
| Format        =   LP/CD
| Label         =   Caption Records / Studio City Sound / INgrooves
| Writer        =    Larry Weir, Bernie Barlow, Dave Darling, Lea Herman, Ian Walsh Tom Weir, Bernie Barlow
| Director      =    
| Chart position=    
| Sales         =    
| Certification =    
| Tracks        =    
| Bonus tracks  =    
| Singles       =   "I Choose You" }}
{{Track listing
| collapsed       =
| headline        = Track listing: Moondance Alexander (2007)
| extra_column    = Performer(s)
| all_lyrics      =
| writing_credits = yes
| title1          = I Choose You
| writer1          = Bernie Barlow
| extra1          = Laura Wight
| length1         = 3:36
| title2          = Call It A Day
| writer2          = Larry Weir
| extra2          = Monét Monico
| length2         = 4:08
| title3          = If I Could
| writer3          = Michael Damian
| extra3          = Heather Youmans
| length3         = 4:26
| title4          = It Only Gets Better
| writer4          = Larry Weir
| extra4          = Mimi Gianopulos
| length4         = 3:47
| title5          = Goodbye
| writer5          = Ian Walsh
| extra5          = Ian Walsh
| length5         = 3:55
| title6          = Coldest Day Of Summer
| writer6          = Dave Darling
| extra6          = Laura Wight
| length6         = 3:35
| title7          =  Think For Each Other
| writer7          = Milo Decruz, Lea Herman

| extra7          = Lea Herman
| length7         = 3:50
| title8          = You Dont Know
| writer8          = Larry Weir
| extra8          = Heather Youmans
| length8         = 4:12
| title9          = It Only Takes One   
| writer9          = Larry Weir
| extra9          = Sara Niemietz
| length9         = 3:13
| title10         = What Might Have Been
| writer10         = Larry Weir
| extra10         = Buck McCoy
| length10        = 3:58
| title11         = Better Kind Of Life
| writer11         = Larry Weir
| extra11         = Tessa Ludwick / Laura Wight / Heather Youmans
| length11        = 3:34
| title12         = If I Could
| writer12         = Larry Weir
| extra12         =  (Instrumental)
| length12        =  4:27
}}

=== Musicians ===

 
* Bernie Barlow, Vocals (
* Paul Bushnel, Bass
* Michael Chaves, Guitar
* Jorge Costa, Vocals (Background)
* Michael Damian, Percussion,  Vocals (Background)
* Milo Decruz Bass, Guitar, Keyboards, Percussion, Producer
* Burleigh Drummond, Percussion
* Blake Ewing, Vocals (Background)
* Josh Freese, Drums
* Joshua Grange, Guitar
* Lea Herman,  Keyboards, Vocals (Background)
* Matt Laug, Drums
* Lance Morrison, Bass
* Ginger Murphy, Cello
* Leah Nelson, Viola
* Michael Parnell, Keyboards
* Tim Pierce, Guitar
* Cam Tyler, Drums
* Ian Walsh, Bass,  Guitar, Piano, Vocals (Background)
* Kevin Walsh, Guitar
* Larry Weir,  Percussion,  Vocals (Background)

 

=== Personnel ===

 
* Jorge Costa, Audio Engineer, Engineer
* James Gaynor, Audio Engineer
* Josh Mosser, Audio Engineer
* Luke Tozour, Audio Engineer, Engineer
* Tom Weir, Audio Engineer, Engineer
 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 