Sökarna: Återkomsten
{{Infobox film 
| name           = Sökarna: Återkomsten
| image          = 
| caption        = 
| director       = Thorsten Flinck Liam Norberg Lena Koppel
| producer       = Kaj Ellertsson
| writer         = Liam Norberg Mårten Skogman	
| narrator       = 
| starring       = Liam Norberg Ray Jones IV Thorsten Flinck
| music          = Stefan Ekström Life Form Area
| cinematography = Per Källberg Kjell S. Koppel
| editing        = Fredrik Lundberg
| distributor    = 
| released       = 
| runtime        = 91 min.
| country        = Sweden Swedish
| budget         = 
| gross          = 
}}
 Swedish crime thriller from 2006. It was directed by Liam Norberg, Lena Koppel and Swedish rock star Thorsten Flinck. Noomi Rapace appears in the film in small role. The budget was 120.000 Swedish crowns. Much of the cast and crew and the plot of the film where recycled from Blodsbröder.

==Plot==
Jocke is broken out of prison to help his brother pay a debt to the insane kingpin Zoran. Soon he is dragged into an evil spiral of violence and crime. 

==Cast==
*Liam Norberg - Jocke
*Thorsten Flinck - Zoran
*Ray Jones IV - Ray
*Johanna Sällström - Johanna
*Mats Helin - Matte
*Johan Wahlström - Roman
*Reine Brynolfsson - Father Roger
*Lakke Magnusson - Falk
*Thomas Hedengran - Officer Stig
*Matti Berenett - Sven
*Malou Hansson - Hooker Ken Ring - Gangster
*Matti Sarén - Zorans Hitman
*Leif Andrée - Greasy Cop
*Noomi Rapace - Enforcer
*Tommy Sporrong - Cop
*Stella Stark - Zorans Girl
*Bojan Westin - Enforcer
*Fabrizio Pollo - Fiji
*Peter Söderlund - Bodyguard 

==References==
 

==External links==
* 

 
 
 
 
 


 
 