I'll Be Yours
{{Infobox film
| name           = Ill Be Yours
| image          = Ill_Be_Yours_1947_Poster.jpg
| image_size     = 225px
| border         = yes
| caption        = theatrical release poster
| director       = William A. Seiter
| producer       = Felix Jackson
| writer         = Feliz Johnson (adaptation)
| screenplay     = Preston Sturges
| based on       =  
| starring       = {{Plainlist|
* Deanna Durbin
* Tom Drake
* William Bendix
* Adolphe Menjou
}} Frank Skinner
| cinematography = Hal Mohr Otto Ludwig
| studio         = Universal-International
| distributor    = Universal-International
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 musical comedy A jó The Good Fairy by Preston Sturges.

==Plot==
Louise Ginglebusher (Deanna Durbin) is a young woman from the small town of Cobbleskill who comes to New York City to make it in show business.  In a café, shes befriended by a kindhearted but ornery waiter, Wechsberg (William Bendix), and meets a bearded struggling attorney, George Prescott (Tom Drake).  She gets a job as an usherette from Mr. Buckingham (Walter Catlett), the owner of the prestigious Buckingham Music Hall, whos an old friend of her father.  
 Broadway musical theatre|musical.  To discourage Nelsons obvious physical interest in her, Louise tell him that shes married, whereupon Nelson offers buy her out of her marriage by paying her husband for his loss.  Impetuously deciding to do a good deed, she gives Nelson the business card that George Prescott, the struggling lawyer, had given her, and tells him that George is her husband.

When Nelson visits George the next day in his shabby storefront law office, and offers to make him the legal representative for his company, George is suspicious and refuses the offer, but Nelson allays his concerns by telling the ethical young attorney that he needs an honest lawyer as a role model for his staff &ndash; the truth is he wants George on his staff so he can keep him occupied while he pursues Louise.  Many complications ensue after Louise gets George to shave off his old-mans beard, revealing the handsome young man underneath, and a stroll in the moonlight provokes George to propose marriage to Louise.  Deming, Mark   

==Cast==
* Deanna Durbin as Louise Ginglebusher
* Tom Drake as George Prescott
* William Bendix as Wechsberg
* Adolphe Menjou as J. Conrad Nelson
* Walter Catlett as Mr. Buckingham
* Franklin Pangborn as Barber
* William Trenk as Captain
* Joan Shawlee as Blonde
* John Phillips as Thug

Cast notes:
*Two years after making Ill Be Yours, Deanna Durbin retired from film acting.  In an interview in 1981, she described her last four films &ndash; this movie, Something in the Wind (1947), Up in Central Park (1948), and For the Love of Mary (1948) &ndash; as "terrible". 

==Songs==
Ill Be Yours was designed to be a vehicle for Deanna Durbin, and all the songs in it are sung by her.  

*"Granada (song)|Granada" - words and music by Agustín Lara
*"Its Dream Time" - by Walter Schumann (music) and Jack Brook (lyrics)
*"Cobbleskill School Song" - by Walter Schumann (music) and Jack Brook (lyrics)
*"Loves Own Sweet Song" - by Emmerich Kalman (music) and Catherine Chisholm Cushing and E. P. Heath (lyrics)
*"Sari Waltz"
*"Brahms Lullaby" - by Johannes Brahms

==Production==
Ill Be Yours was in production from the middle of August to the middle of October 1946.  Background scenes were shot on location in New York City. TCM    It was released on 2 February 1947.   Among other taglines, it was marketed with "Heaven Protects the Working Girl...but who protects the guy shes WORKING to get?" 

==Other versions and adaptations== The Good Broadway with musical Make Make a Wish, which had music and lyrics by Hugh Martin. 
 Maurice Evans, George Schaefer, and starring Julie Harris, Walter Slezak and Cyril Ritchard. 

==References==
Notes
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 