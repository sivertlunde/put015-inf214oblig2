Asuravithu (1968 film)
{{Infobox film
| name           = Asuravithu
| image          = Asuravithu 1968.jpg
| caption        = Film poster
| director       = A. Vincent
| producer       = Madhavankutty
| studio         = Manoj Pictures
| writer         = M. T. Vasudevan Nair
| based on       =  
| cinematography = A. Venkat Sharada
| music          = K. Raghavan
| editing        = G. Venkitaraman
| distributor    = 
| released       =  
| country        = India
| language       = Malayalam
}} Sharada in the lead roles. The film was scripted by M. T. Vasudevan Nair based on his own critically acclaimed novel Asuravithu (novel)|Asuravithu.   

==Plot==
The film is set in an Indian village in the 1960s. The film portrays the plight of the protagonist, the youngest son of a proud Nair tharavadu, as he is trapped between the social scenario, social injustice and his own inner consciousness.

==Cast==
* Prem Nazir as Govindankutty Sharada as Meenakshi
* Adoor Bhasi
* P. J. Antony
* Kaviyoor Ponnamma
* Sankaradi
* N. Govindankutty
* Santha Devi
* Kalamandalam Kalyanikutty Amma Khadeeja

==Soundtrack==
The music was composed by K. Raghavan and lyrics was written by  and P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kattakattakkayarittu || S Janaki, Chorus ||  ||
|-
| 2 || Kunkuma Maram Vetti || P. Leela, CO Anto ||  ||
|-
| 3 || Kunnathoru Kaavundu || P. Leela, CO Anto ||  ||
|-
| 4 || Njanitha || P Jayachandran, Renuka || P. Bhaskaran ||
|-
| 5 || Pakalavaninnu || K. Raghavan || P. Bhaskaran ||
|-
| 6 || Theyyam Thaare   || Chorus, Renuka ||  ||
|-
| 7 || Theyyam Theyyam || CO Anto ||  ||
|}

==References==
 

==External links==
*  
*   at the Malayalam Movie Database

 
 
 
 

 