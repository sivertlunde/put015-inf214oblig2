The F Word (2005 film)
{{Infobox Film |
  name           = THE F WORD |
  image          = FwordDVDcover06finalwiki.jpg |
  caption        = Promotional poster for the film. |
  writer         = Andrew Osborne   Jed Weintrob | Josh Hamilton Edoardo Ballerini Sam Rockwell Zak Orth Callie Thorne Yul Vazquez|
  director       = Jed Weintrob|
  producer       = Christian Ditlev Bruun Nick Goldfarb Lawrence Mattis|
  distributor    = Independent Film Channel|IFC|
  released       = 2005|
  country        = United States |
  runtime        = 77 minutes |
  language       = English |
  }}
The F Word is a feature fiction/documentary hybrid film from 2005, directed by Jed Weintrob and produced by Christian Ditlev Bruun and Nick Goldfarb. The film was selected to world premiere at the 2005 Tribeca Film Festival in New York.

==Plot==
Joe Pace, a radio talk show host whose program, The F Word, is being shut down by the U.S. Federal Communications Commission (FCC) after racking up over $1 million in unpaid indecency fines. On his last day on the air, which coincides with the last day of the 2004 Republican National Convention, Joe sets off to broadcast his own one-man march through Manhattan.

==Style and structure== color corrected composited using several different types of software as well as in-camera adjustments creating a unique, photographic expression.

==Cast==
 , Peter Camejo, John Perry Barlow.

==Release== IFC TV IFC TV May 4, 2008. 

==See also==
*Fuck (film)

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 