Cry Uncle!
 Cry Uncle (disambiguation)}}
{{Infobox film
| name           = Cry Uncle!
| image          = Cry uncle.JPG
| caption        = Theatrical release poster
| director       = John G. Avildsen
| producer       = David Jay Disick	
| writer         = Novel Michael Brett Screenplay David Odell Additional dialogue Allen Garfield John G. Avildsen
| starring       = Allen Garfield
| cinematography = John G. Avildsen
| editing        = John G. Avildsen
| music          = Harper MacKay 
| distributor    = Troma Entertainment
| released       = August 17, 1971 
| runtime        = 87 min.
| country        = United States
| language       = English
| awards         =
| budget         = $250,000 
}} Troma library. It is directed by John G. Avildsen and stars Allen Garfield. The story, based on the Michael Brett novel Lie a Little, Die a Little, follows the misadventures of a slobbish private detective who is hired by a millionaire to investigate a murder. The movie features one of Paul Sorvinos first screen performances, and an early appearance from TV star Debbie Morgan.

The film features a great deal of nudity, sex, drug use, and an explicit act of necrophilia. The film was banned in Finland for the year following its release, and in Norway until 2003. In addition to becoming a cult classic, the film launched a string of Troma films that appeared in the 1970s, 80s, and 90s, many of them becoming cult films that would run on cable TV.

==Plot==
The story follows a detective who takes on a murder case, complicated by a diverse group of suspects and a lot of sexual situations.

==Cast==
Lloyd Kaufman, co-founder of Troma, has a cameo appearance as a hippie.

==Reception==
During an interview featured in the Special Edition of the films DVD, Allen Garfield claims that Cry Uncle! is Oliver Stones favorite comedy.

==See also==
*List of Troma films

==References==
 

==External links==
* 
*  – at the Troma Entertainment movie database

 

 
 
 
 
 
 
 
 

 