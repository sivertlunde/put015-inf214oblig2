A Man for Emmanuelle
 
{{Infobox film
| name           = A Man for Emmanuelle
| image          = A Man for Emmanuelle.jpg
| caption        = Film poster
| director       = Cesare Canevari
| producer       =
| writer         = Emmanuelle Arsan Cesare Canevari Graziella Di Prospero Giuseppe Mangione
| starring       = Erika Blanc
| music          =Gianni Ferrio
| cinematography = Claudio Catozzo
| editing        =
| distributor    =
| released       = 11 September 1969
| runtime        = 96 minutes
| country        = Italy
| language       = Italian
| budget         =
}}
A Man for Emmanuelle ( ) is a 1969 Italian drama film directed by Cesare Canevari and starring Erika Blanc.

==Cast==
* Erika Blanc as Emmanuelle
* Adolfo Celi as Sandri Paolo Ferrari as Raffaello
* Ugo Adinolfi
* Lucia Folli
* Sandro Korso as Il capellone
* Renato Nardi
* Mirella Pamphili
* Lia Rho-Barbieri as Anita
* Ben Salvador as Phil
* Milla Sannoner as Ginette
* Walter Valdi

==External links==
* 

 
 
 
 
 
 