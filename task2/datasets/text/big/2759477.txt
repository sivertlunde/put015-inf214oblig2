Big Momma's House 2
{{Infobox film
| name           = Big Mommas House 2
| image          = Big mommas house 2.jpg
| alt            =
| caption        = Promotional poster
| director       = John Whitesell
| producer       = David T. Friendly Michael Green
| writer         = Don Rhymer
| based on       =  
| starring       = Martin Lawrence
| music          = George S. Clinton
| cinematography = Mark Irwin
| editing        = Priscilla Nedd-Friendly Runteldat Entertainment Deep River Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = $40 million   
| gross          = $138,259,062 
}} FBI agent Malcolm Turner. The film was released theatrically on January 27, 2006 and was critically panned as critics felt that a sequel was unnecessary. Unlike the original, the film is more family friendly compared to the original films more mature target demographic. The film was not well received by film critics, scoring 6% at Rotten Tomatoes.

==Plot== FBI agent, since he wants to live with his new wife, Sherry Pierce (Nia Long), during her delivery for the couples new baby boy. Meanwhile, an incident occurs in Orange County, California, where Malcolms old friend, Doug Hudson (Kirk B.R. Woller), has been killed while he was going undercover. FBI agent Kevin Keneally (Zachary Levi) is doing surveillance on a former United States Army|U.S. Army military intelligence specialist named Tom Fuller (Mark Moses), who has since retired and is working for a private corporation called National Agenda Software. The FBI has soon discovered that Tom is developing a computer worm which will create backdoors into the databases of all the branches of the U.S. government. Affected by his friends death, Malcolm asks FBI chief, Crawford (Dan Lauria), to put him on the case, but Crawford refuses and tells him to stay away for safety analysis. By eavesdropping via the webcam, Malcolm finds out that the FBI is sending one of the agents to infiltrate Fullers house as a nanny. Giving Sherry the pretext of attending a safety conference in Phoenix, Arizona, Malcolm leaves for Orange County and takes the "Big Momma" costume with him.

Malcolm reprises his disguise as Big Momma from the original film and showing up at Fullers house as Mrs. Fuller ( ), Carrie (Chloë Grace Moretz), and Andrew. After failing to perform the housekeeping tasks assigned to her, Big Momma is fired, and works all night cleaning up and makes a large breakfast. Upon seeing it the next day, Mrs. Fuller changes her mind when the family awakes to find this. Big Momma is soon accepted within the household and becomes a daily part of their lives. His tasks include accompanying Mrs. Fuller to the spa, taking the family to the beach, watching out for trouble, and simply playing a game of bingo as part of her routine. After Big Momma finds out the password from Tom, Molly calls him who tells her that she needs her at a nightclub. Big Momma goes at once, only to find that Molly was lured by Fullers bosses, who kidnaps her and Big Momma. Big Momma and Molly are tied up and placed in the back of the van. Big Momma has a switchblade, which Molly reaches for and uses to free them. He sees that they are at the waterfront and witnesses Tom giving a disc to a man who puts it in his laptop and is granted full access to FBI data. Big Momma gets on a jet ski and jumps it onto the dock, sending it into two men, and landing on one himself. Big Momma helps Tom and they attempt to escape, but one of the men shot him. The FBI shows up, and Keneally gave Malcolm the handcuffs to put on Tom, but Malcolm tells the agent in charge that Toms family was threatened, and that no charges should be filed. Malcolm and Keneally agree, and the case is closed.

Sometimes later, Big Momma goes to the girls state cheerleading championships. Their stuntwoman broke her leg, and Big Momma helps them out by doing the routine and winning the competition. He later leaves and gives the family a farewell letter saying he must go on, but to look out, because one day he might be back.

==Cast== FBI Agent Malcolm Turner/Big Momma
* Nia Long as Sherry Pierce Turner
* Zachary Levi as Kevin Keneally
* Mark Moses as Tom Fuller
* Emily Procter as Leah Fuller
* Kat Dennings as Molly Fuller
* Chloë Grace Moretz as Carrie Fuller
* Marisol Nichols as Liliana Morales
* Jascha Washington as Trent Pierce Turner

==Reception==

=== Critical response ===
 
  in 2006  in the category "Worst Prequel or Sequel", but lost the trophy to Basic Instinct 2.

Metacritic gives the film a score of 34% based on reviews from 20 critics, indicating "Generally Unfavorable Reviews". 

Its poor reception has been lampooned in The Onion. {{Cite news 
| last = 
| first = 
| coauthors = 
| title = Passengers Bravely Take Down Plane Showing Big Mommas House 2
| newspaper = The Onion
| location =
| pages = 
| language = 
| publisher = 
| date = 24 May 2006
| url = http://www.theonion.com/articles/passengers-bravely-take-down-plane-showing-big-mom,1968
| accessdate = 4 March 2011
}} 

=== Box office ===
Big Mommas House 2 grossed $27,736,056 in its opening weekend ranking number one.  As of March 3, 2011, the film has grossed a total of $70,165,972 at the United States box office with a worldwide gross of $138,259,062.

== Sequel ==
  was released on February 18, 2011. Brandon T. Jackson was cast in the role of Trent, who was originally played by Jascha Washington. Nia Long also did not reprise her role, which resulted in her character, Sherry, being written out. The film received overwhelmingly negative reviews from film critics as well.

==References==
 

==External links==
*  
*  
*   in Slant Magazine by Keith Uhlich

 

 
 
 
 
 
 
 
 
 
 
 
 