Big Stan
 
{{Infobox film
| name = Big Stan
| image = Big Stan Poster.png
| caption = Promotional poster for Big Stan
| director = Rob Schneider John Schneider Mark A.Z. Dippé Rob Schneider David Hillary Timothy Wayne Peternel
| writer = Josh Lieb Scott Wilson Henry Gibson Richard Kind Jackson Rathbone M. Emmet Walsh David Carradine
| music = John Hunter
| cinematography = Victor Hammer
| editing = Richard Halsey
| studio = Crystal Sky Pictures Silver Nitrate From Out of Nowhere Productions Chicago Entertainment Partners
| distributor = Sony Pictures Home Entertainment
| released =  
| runtime = 105 minutes
| country = United States
| language = English
| budget = $7.5 million
| gross = $8,742,330 
}} Scott Wilson and David Carradine. Although released in some overseas markets during the fall of 2008, it was released straight to DVD in the U.S. on March 24, 2009. It debuted at number 17 on the DVD rental charts of March 23–30, 2009.  On the radio show Loveline, Schneider stated that this film will be an "anti-man-raping" film — referring to prison rape. 

==Plot==
A real estate con artist named Stan Minton (Rob Schneider) panics when he learns that he is going to prison for fraud. Stans fear of jail-house rape leads him to hire the mysterious guru known as "The Master" (David Carradine) who helps transform him into a creative martial-arts expert. During his incarceration, Stan uses his new-found skills to intimidate his fellow prisoners and prevents the prisoners from hitting or raping each other.

He gains the prisoners respect, and eventually becomes their leader, bringing peace and harmony to the prison yard. But the corrupt warden has a plan to profit by turning the prison into a war zone, forcing its closure, and selling off the property as valuable real estate. Stan helps him with the real estate aspects in exchange for early parole, however his peacemaking efforts threaten the wardens plan for a riot and he is persuaded to bring back violence.

In a last minute attack of conscience he deliberately blows the parole hearing to rush back and prevent the deaths of his fellow inmates, only to discover that his message of peace has sunk in and the prisoners are dancing instead of fighting. The warden orders the guards to open fire on the dancing men and, when they refuse, grabs a gun and shoots wildly. He attempts to shoot Minton but he is stopped by Mintons wife and the Master, who had snuck in. Three years later Minton leaves the prison, which is now run by one of the more sympathetic guards as the original one is now an inmate, to be met outside by his wife, his young daughter, and the Master.

==Cast==
*Rob Schneider as "Big" Stan Minton
*David Carradine as The Master
*Jennifer Morrison as Mindy Minton
*M. Emmet Walsh as Lew Popper Scott Wilson as Warden Gasque
*Richard Kind as Mal
*Sally Kirkland as Madame Foreman
*Henry Gibson as Aamir Khan Budda
*Marcia Wallace as Alma
*Diego Corrales as Julio
*Randy Couture as Carnahan
*Don Frye as Nation Member
*Brandon Molale as Piken
*Lil Rob as Inmate
*Brandon T. Jackson as Deshawn (as Brandon Jackson)
*Olivia Munn as Maria
*Richard Riehle as Judge Kevin Gage as Bullard
*Jackson Rathbone as Robbie the Hippie
*Dan Inosanto as Prison Chef
*Bob Sapp as Big Raymond
*Marcelo Ortega as Big Haa the Inmate
*Tsuyoshi Abe as Dang
*Salvator Xuereb as Patterson Buddy Lewis as Cleon
*Adam Sandler (voice) (uncredited)
*Dan Haggerty as the ex-con

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 