The Star of Bethlehem (film)
{{Infobox film
| name = The Star of Bethlehem
| image = Starofbethlehem-1912-movieposter_674x900.jpg
| image_size =
| caption = Movie poster
| director = Lawrence Marston
| producer = Edwin Thanhouser
| writer = Lloyd Lonergan
| starring = {{Plainlist |
* Florence La Badie William Russell
* James Cruze
}}
| music =
| cinematography =
| studio = Thanhouser Company
| distributor = Film Supply Company Mutual Film
| released =  
| runtime = 106 minutes
| country = United States
| language = Silent English intertitles
| budget =
}}
 silent Historical historical drama William Russell, the film is a retelling of Biblical events preceding the Nativity of Jesus. Directed by Lawrence Marston, the entire film is staged as brief Tableau vivant|tableaux. With much of the original lost (only 15 minutes survive), the existing footage can be difficult to interpret as a coherent whole.   

==Overview== King Herod.    

==Cast==
* Florence La Badie as Mary
* James Cruze as Micah/Joseph
* William Russell as King Herod
* Justus D. Barnes as Gaspar, a Magi
* Riley Chamberlin as Balthasar, a Magi
* Charles Horan as Melchior, a Magi
* Harry Marks as Scribe
* Lawrence Merton as Scribe
* N.S. Woods as Scribe
* David H. Thompson as Pharisee, rabbi
* Lew Woods as Pharisee, scribe
* Joseph Graybill as Roman messenger
* Carl LeViness as Shepard
* Frank Grimmer as Shepard
* Ethyle Cooke

==Production notes==
This film was produced by the Thanhouser Company in 1912.  In later life, La Badie described her role of Mary as the favorite of her career.   

==Status and availability==
The film was originally shot on three full Reel#Motion picture terminology|reels.  The original negatives to the film were destroyed in a fire at the Thanhouser Studio in 1913.    Today, only a one-reel edited version survives and is preserved at the BFI National Archive and the John E. Allen, Incorporated archive.   The surviving footage is now in the public domain. Because no copyright number exists in the film, it is possible that it was never officially copyrighted at the time of its release. 

The Star of Bethlehem was included on The Thanhouser Collection: Volumes 1, 2 and 3 DVD collection released in 2006.  

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 