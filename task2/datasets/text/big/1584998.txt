Black Christmas (1974 film)
 
{{Infobox film
| name = Black Christmas
| image = black christmas movie poster.jpg
| caption = The theatrical poster, released under the alternate title
| director = Bob Clark
| producer = Bob Clark
| writer = A. Roy Moore John Saxon Marian Waldman Andrea Martin James Edmond  
| music = Carl Zittrer
| cinematography = Reginald H. Morris
| editing = Stan Cole
| studio = Film Funding Limited of Canada
| distributor = Ambassador Film Distributors (Canada)  Warner Bros. (U. S.)
| released =            
| runtime = 98 minutes
| country = Canada
| language = English
| budget = $620,000
| gross = $4,053,000
}}
 horror film John Saxon. The story follows a group of sorority sisters who are stalked and murdered during the holiday season, by a killer hiding in their sorority house. It was inspired by a series of murders that took place in the Westmount section of Montreal, in the province Quebec, Canada.
 slasher films. remake of the same name, produced by Clark, was released in December 2006.

== Plot ==

A disoriented man climbs up into the attic of a sorority house while the occupants hold a Christmas party. Jess Bradford (Olivia Hussey) receives an obscene phone call from "the moaner," a man who has recently been calling the house. After she calls sorority friends Barb Coard (Margot Kidder), Phyllis Phyl Carlson (Andrea Martin), Clare Harrison (Lynne Griffin) and the several other girls into the room to listen, he starts saying very perverted things to them, until Barb provokes the caller, to which he replies, "IM GOING TO KILL YOU," then hangs up. Barb and Clare then begin to argue about the things she said to him, and Barb ends up rudely commenting on Clares innocent nature. Offended, Clare goes upstairs to finish packing for her trip home. She hears Claude the cats cries and goes to investigate. Clare is attacked and asphyxiated with plastic dry cleaning covering over her head. Her dead body is carried up into the attic, where the killer places her in a rocking chair next to the window and puts a doll in her lap.

The next day, Clares father arrives to take her home for the holidays. When she is not at their appointed meeting place, he goes to the sorority house. The housemother, Mrs. Mac (Marian Waldman), can not help him, and neither can Phyl or Barb. Meanwhile, Jess meets her boyfriend, Peter Smythe (Keir Dullea), a neurotic aspiring pianist, at the piano recital hall to inform him that she is pregnant and wants to have an abortion. Peter is upset and urges her to discuss the situation with him more later but she refuses. Elsewhere, Mr. Harrison, Barb, and Phyl go to the police to report Clares disappearance. Sgt. Nash dismisses the report and says that Clare is probably with a lover. 
 John Saxon). A mother reports that her daughter, Janice, is missing as well. That evening, Mr. Harrison, Chris, and the sorority sisters (except for Barb, who got drunk during dinner and was sent to bed, and Mrs. Mac, who has a taxi scheduled to pick her up for the holidays) join a search party aiming to find Janice or Clare. Back at the house, Mrs. Mac is packing her bags to leave for Christmas break and hears Claudes meows in the attic and goes to investigate. She stares in horror as she discovers Clares body, but the killer launches a crane hook, stabbing Mrs. Mac in the neck and forcefully dragging her into the attic. The long awaited taxi driver, not realizing that Mrs. Mac has been killed, becomes impatient and goes knocking on the door and then leaves.

After one search party finds Janices dead body, Jess returns home and receives another obscene call. Jess phones the police to report it and suddenly becomes impatient. Peter arrives and argues with Jess about her planned abortion. He leaves after Lt. Fuller arrives to discuss the phone calls. A technician places a tap "bug tracer" onto the sorority house phone. Lt. Fuller also reminds Jess and Phyl that there will be an officer in his car stationed outside the house. Minutes later, the killer appears in Barbs room and stabs her to death with a glass unicorn ornament but door-to-door Christmas carolers drown out the noise of the attack. Jess receives another obscene call that quotes a part of the argument she had with Peter. She now suspects Peter of being the caller, but then she and Phyl conclude that it cannot be him, since Peter was present during one of the earlier calls. 

Phyl goes upstairs to bed, deciding to check on Barb first. As she enters the room, the killer suddenly shuts the door behind her and murders her off-screen. Another call comes in, and this time, Jess manages to keep the caller on the phone for a minute, allowing the police to trace it inside the house (from Mrs. Macs separate phone line). Jess is ordered to leave the house immediately but she puts down the phone and yells up to Barb and Phyl. At the police station, Lt. Fuller is informed of the situation and leaves for the house. Jess arms herself with a fireplace poker and ventures upstairs, finding both Barb and Phyls dead bodies. Then, in possibly the most infamous moment of the film, Jess sees the killer spying on her through the door crack, telling her not to Tell what we did, Agnes.., before she slams the door on him. The killer then attacks Jess and chases her through the house before Jess locks herself in the cellar. Peter appears outside a basement window, telling Jess he heard screaming. He breaks the glass and enters. Jess, believing him to be the attacker, backs into a corner as he approaches. 

Lt. Fuller and the police arrive and find the officer stationed outside dead in his car with his throat slashed. When they hear Jess scream, they rush inside and find Jess in the basement with Peter, whom she has bludgeoned to death in self-defense. Jess is sedated as Fuller and the officers discuss how Peter must have been the killer all along. They also discuss the fact that Clare still has not been found, revealing that the attic has not been searched. The officers leave Jess to sleep in her bed, stating that a man will be right outside the front door. Once the house is quiet, the camera pans from Jesss room to the attic ladder and up, with Clare and Mrs. Macs bodies still unfound, untouched. the killer says "Agnes, its me Billy". The camera pulls back to show the outside of the house. The film ends as the telephone begins to ring as the credits roll.

== Cast ==
* Olivia Hussey as Jessica Bradford
* Keir Dullea as Peter Smythe
* Margot Kidder as Barbara Corad John Saxon as Lt. Kenneth Fuller
* Marian Waldman as Mrs. MacHenry
* Andrea Martin as Phyllis Carlson James Edmond as Mr. Harrison Douglas "Doug" McGrath as Sergeant Nash Arthur "Art" Hindle as Chris Hayden
* Lynne Griffin as Clare Harrison
* Michael Rapport as Patrick Cornell Leslie "Les" Carlson as Bill Graham
* Martha Gibson as Mrs. Quaife John Rutter as Laughing Detective Robert Warner as Doctor Syd Brown as Farmer
* Jack Van Evera as Search Party
* Les Rubie as Search Party
* Marcia Diamond as Woman
* Pam Barney as Jean Robert Hawkins as Wes
* David Clement as Cogan (as Dave Clement)
* Julian Reed as Officer Jennings Dave Mann as Cop
* John Stoneham Sr. as Cop (as John Stoneham)
* Danny Gain as Cop Tom Foreman as Cop

=== Uncredited ===
* John Frenchie Berger as Man on snowmobile
* Bob Clark as Prowler Shadow / Phone Voice
* Nick Mancuso as The Prowler / Phone Voice
* Debi Weldon as Sorority Girl
* Michael J Eaton as boy on Santas lap

== Development ==

Canadian Roy Moore wrote the screenplay; he based it on a series of murders in Montreal, Quebec. However, there has been speculation over the years as to whether the screenplay was actually inspired by The Babysitter and the Man Upstairs urban legend as opposed to real events. Moore died in the late 1980s and was never interviewed about the film. The script was originally titled "Stop Me" and was partially typed and partially handwritten.    Moore submitted the screenplay to director Bob Clark. Clark made several alterations in dialogue, camera placement and added some notes. On the final page of the screenplay was a hand written note by Clark calling it "a damn good script". The original screenplay in its entirety was released as a DVD-ROM feature on one of the films DVD releases.

== Production ==
  Annesley Hall National Historic Site. The films budget of $620,000 was shot in 35mm format with Panavision cameras. Cameraman Albert J. Dunk created the POV camera shot by mounting a camera onto his back and creeping around the house. He crawled up the housing trellis in the beginning of the film as well. According to Bob Clark, due to the surprisingly light snowfall, most of the snow scenes outside of the sorority house were made of foam material provided by a local fire department. The house used for the sorority residence was filmed on location in Toronto; it is now a private home. 

The role of Mrs. Mac was offered to Bette Davis. The role of Peter was originally offered to Malcolm McDowell, but he turned it down. The role of Lieutenant Fuller was originally supposed to be played by Edmond OBrien, but due to failing health he had to be replaced. John Saxon was brought in at the last minute. Gilda Radner was offered the role of Phyllis Carlson. She was attached, but dropped out one month before filming began owing to Saturday Night Live commitments. The composer of the films score, Carl Zittrer, stated in an interview that he created the films mysterious music by tying forks, combs and knives onto the strings of the piano in order to warp the sound of the keys. Zittrer also stated that he would distort the sound further by recording its sound onto an audio tape and make the sound slower. The audio for the disturbing phone calls was performed by actor Nick Mancuso, director Bob Clark and an unknown actress. Mancuso stated in an interview that he would stand on his head during the recording sessions to make his voice sound more demented.

During preparation in 1975 for the films American release, Warner Bros. studio executives asked Clark to change the concluding scene to show Claires boyfriend, Chris, appear in front of Jess and say, "Agnes, dont tell them what we did" before killing her, however, Clark insisted on keeping the ending ambiguous. The original title of the film was initially planned to be Stop Me. Clark has stated in an interview that he came up with the films official title, saying that he enjoyed the irony of a dark event occurring during a festive holiday. According to Clark as well, Warner Bros. changed the title to Silent Night, Evil Night, for the U. S. theatrical release. During later television broadcasting, the films title was changed to Stranger in the House. However, it was cancelled due to broadcasters deeming it "too scary" for television broadcast. 

== Release ==

Black Christmas was officially released on October 11, 1974, in Canada through Ambassador Film Distributors, and in the United States on December 20, 1974, through Warner Bros.,  where it grossed $4,053,000. It was released in October 1975 in New York City and Chicago,    and previously played under the title Silent Night, Evil Night in Virginia in July 1975.    and grossed over $4,053,000 internationally, managing to earn more than the films budget of $620,000.  When released in the United Kingdom|UK, the BBFC had the word "cunt" removed, as well as several other crude and sexual references during the first obscene phone call. 

=== Home media release ===
 remake of the film on Christmas day, containing extra and similar bonus content to the previous collectors edition, including interviews with stars Olivia Hussey and Margot Kidder. A Blu-ray edition of the film was released on 11 November 2008.

== Critical reception ==
 
The film has since received generally positive reviews from modern critics. According to the review aggregator website Rotten Tomatoes, it has a 63% "fresh" score based on 24 reviews, with an average rating of 6.2 out of 10.  Heidi Martinuzzi of Film Threat called the film "innovative" and praised the leading actresses, Olivia Hussey and Margot Kidder. 

However upon its initial release, the film had garnered mixed reviews. A writer for The New York Times scored the film a 1 out of 5, calling it "a whodunit that raises the question as to why was it made." 

== Awards and nominations ==
 Academy of Science Fiction, Fantasy & Horror Films
* 1976: Nominated, "Best Horror Film"

; Canadian Film Awards
* 1975: Won, "Best Sound Editing in a Feature" – Kenneth Heeley-Ray
* 1975: Won, "Best Performance by a Lead Actress" – Margot Kidder

; Edgar Allan Poe Awards
* 1976: Nominated, "Best Motion Picture" – A. Roy Moore

== Cult status ==
  Friday the Romeo and Juliet, but was surprised when Martin said it was Black Christmas, and that he had seen the film 27 times. 

== Remake ==
 remake of executive producer. It was poorly received by critics.

== See also ==

* List of films featuring home invasions

== References ==
 

== External links ==
 

*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 