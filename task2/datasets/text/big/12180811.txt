Fire with Fire (1986 film)
{{Infobox film
| name           = Fire with Fire
| image          = Firewithfireposter.jpg
| caption        = Theatrical release poster
| director       = Duncan Gibbins
| producer       = Gary Nardino Ben Pillips Warren Skaaren Paul Boorstin Sharon Boorstin
| starring       = {{plainlist|
* Virginia Madsen
* Craig Sheffer
* Kate Reid
}} 
| music          = Howard Shore
| cinematography = Hiro Narita
| editing        = Peter E. Berger
| distributor    = Paramount Pictures
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| gross          = $4,636,000
}} romantic drama Catholic boarding prison camp. The film stars Virginia Madsen, Craig Sheffer, Kate Reid, Kari Wührer, Tim Russ and D. B. Sweeney. It was directed by Duncan Gibbins, and features a soundtrack by noted film composer Howard Shore. It was released on Blu-ray Disc and DVD on July 31, 2012, by Olive Films.

==Plot==
In this fact-based adolescent melodrama, Joe Fisk is a juvenile delinquent who falls in love with Lisa Taylor, a beautiful Catholic girls school student, in an Oregon forest. The two meet by accident when the troubled young man stumbles upon her while being chased by his peers in a training exercise, and sees the lovely girl floating in a small lake as she works on a photography assignment, recreating the Pre-Raphaelite painting Ophelia (painting)|Ophelia by John Everett Millais. The two are immediately drawn to each other, but neither of their custodians encourage contact with the opposite sex, and when their relationship is discovered there is trouble all around, forcing the young lovers to flee. The question then remains: Will they be able to escape the law and other authorities long enough to find happiness?

==Cast==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| Craig Sheffer || Joe Fisk
|-
| Virginia Madsen || Lisa Taylor
|-
| Jon Polito || Mr. Duchard, the Boss
|-
| J. J. Cohen || Myron, the "Mapmaker"
|-
| Kate Reid || Sister Victoria
|-
| Jean Smart || Sister Marie
|-
| Tim Russ || Jerry Washington
|-
| Kari Wührer || Gloria
|-
| D. B. Sweeney || Thomas Baxter
|- Ann Savage || Sister Harriet
|- David Harris || Ben Halsey
|}

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 


 