El analfabeto
{{Infobox film
| name           = El analfabeto
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Miguel M. Delgado
| producer       = Jacques Gelman
| writer         = 
| screenplay     = Jaime Salvador  (adaptation)  Carlos León  (additional dialogue)  Miguel M. Delgado  (technical screenplay) 
| story          = Marcelo Salazar Juan López  (original story) 
| based on       =  
| narrator       = 
| starring       = Cantinflas Lilia Prado Ángel Garasa Sara García
| music          = Manuel Esperón
| cinematography = Víctor Herrera
| editing        = Jorge Bustos
| studio         = Posa Films International Estudios Churubusco
| distributor    = Columbia Pictures
| released       =  
| runtime        = 128 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}}

El analfabeto ( ) is a 1961  Mexican comedy film, directed by Miguel M. Delgado, starring Cantinflas, Lilia Prado, and Sara García. It is the second Cantinflas film presented by Columbia Pictures.

==Plot== baptismal certificate as proof of identity. However, as an illiteracy|illiterate, Inocencio has no idea of the contents of the letter. While waiting for the local druggist to wait on him so he can have the letter read to him, Inocencio is embarrassed to see that a customers young daughter is already able to read while he, a grown man, cannot. He leaves without telling the druggist his problem, resolved to go to school and to wait to learn the letters contents until he can read them for himself, so that never again will he have to share private matters with others because of his own ignorance. After registering at school, he stops by the local bank to ask for a job, having quit his previous employment that morning. Leaving the bank, he meets Blanca, an attractive young woman newly arrived in town, and shows her the way to her new place of employment, partly to avoid admitting he cannot read the written address. The daughter of Blancas employer is entertaining her fiancé, Aníbal, who finds Blanca appealing and begins to make advances on her almost immediately. These advances are spurned each time; the final time, Aníbal warns her she will regret her refusals.

Over the course of the film, Inocencio gradually learns to read, courts Blanca, and makes both friends and enemies at the bank. He foils a robbery and then a plot to make him look guilty; the bank manager is so pleased with his honesty that he gives Inocencio a 1000-peso reward, which the grateful man proceeds to spend on a new dress for his godparent|godmother, a traditional regional dress for Blanca to wear in a beauty contest, and new shoes for himself. While going about his cleaning work in the bank, Inocencio unwittingly drops the lawyers letter &mdash; which he still has yet to read &mdash; and a fellow employee with a grudge against him finds the item on the floor. On the day of the contest, Aníbal and the bank employee, who are revealed to be cousins, conspire to make it appear that Blanca has stolen her employers jewels and passed them to Inocencio. Though both are arrested, the trial is cut short when the bank employee discovers Aníbal has betrayed him and gone alone to claim the inheritance, leaving the employee to reveal the whole plot. Inocencio and his friends rush to Mexico City to thwart the attempt and denounce Aníbal, who is arrested at the lawyers office after he arrives to claim the funds. The film concludes with Inocencios and Blancas wedding.

==Cast==
*Cantinflas as Inocencio Prieto y Calvo
*Lilia Prado as Blanca Morales: Inocencios girlfriend
*Ángel Garasa as Don Rómulo González: Wealthy banker of Guanajuato
*Sara García as Doña Epifanita: Inocencios godmother Miguel Manzano as Don Fermín: Bank employee
*Carlos Agostí as Licenciado Aníbal Guzmán: Ofelias fiancé Daniel Herrera as "El Pocaluz": Inocencios friend
*Fernando Soto as "El Sapo": Inocencios friend
*Guillermo Orea as El Comandante: Head of the Guanajuato police department
*Óscar Ortiz de Pinedo as Jesús López: Public notary
*Carlos Martínez Baena as El Profesor: Inocencios teacher
*Judy Ponte as Ofelia González: Rómulos daughter
*María Teresa Rivas as Señora González: Rómulos wife

==DVD details == Spanish audio English Subtitle (captioning)|subtitles. This edition of the film is letterboxed and in widescreen format.

==External links==
* 


 
 
 
 
 