Terrorism and Kebab
{{Infobox film
| name           = Terrorism and Kebab
| image          = Terrorism and kebab poster (1992).jpg
| caption        = 
| director       = Sherif Arafa
| producer       =
| writer         = Wahid Hamed
| starring       = Adel Emam
| music          = Moody Elemam ( Elimam )
| cinematography = 
| distributor    = 
| released       = 1992
| runtime        = 105 minutes
| country        = Egypt
| language       = Arabic
| gross          = 
}}

Terrorism and Kebab ( , transliterated: Al-irhab wal kabab) is a popular 1992  , Retrieved December 13, 2010 

==Plot==
  The action primarily takes place in  , Retrieved December 13, 2010  (2 September 1992).  ,  , Retrieved December 14, 2010   Adel Imams character, Ahmed, queues up at the Mogamma one day to try to get a school transfer for his children, but gets bogged down.  Endless lines of citizens march through the building and up and down its iconic spiral staircases seeking help.  One government worker frustrates Ahmed particularly, because he is constantly praying in a show of alleged piousness to avoid getting any work done.  This leads to a scuffle between the two, and eventually Ahmed ends up with a rifle from a guard and shots are fired in the resulting confusion.  A mass spontaneous evacuation of the building ensues, Ahmed inadvertently takes the building hostage, and subsequently is assumed to be a terrorist.  He is joined in his "takeover" by a few other "misfits," including Hind, a prostitute played by Yousra.

Ahmed and his new compatriots negotiate with the  , Retrieved December 13, 2010 

As the film draws to a close, Ahmed orders the "hostages" to leave the building, and he will wait behind to meet up with the military police now ready to swarm the building, assuming he will be killed.   The crowd, however, insists that he leave with them.  Ahmed walks out unnoticed among his former "hostages", and the commandos find the building empty.  (9 October 2005).  ,  , Retrieved December 16, 2010  

==Reception==
The film has been described as a "classic Egyptian comedy about government corruption, bumbling and the good hearted nature of the shaab (people) of Egypt."  ,  , Retrieved December 16, 2010 (" perhaps the past years most popular movie")  and has even has been cited as the most popular Egyptian film of all time. Sardar, Ziauddin (2 April 2007).  ,  , Retrieved December 16, 2010 

==Primary cast includes==
*Adel Emam - Ahmed
*Yousra - Hind
*Kamal el-Shennawi - Minister of Interior

==References==
 

==External links==
*  
*  

 

 
 
 
 
 