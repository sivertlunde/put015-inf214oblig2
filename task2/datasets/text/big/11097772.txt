The 601st Phone Call
{{Infobox film
| name           = The 601st Phone Call
| image          = 601PhoneCall.jpg
| caption        = Theatrical release poster
| director       = Zhang Guoli
| producer       = Feng Xiaogang
| writer         = Zou Jingzhi
| narrator       = 
| starring       = Cecilia Cheung Zhou Bichang Hu Ge
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = August 18, 2006
| runtime        = 90 mins China
| Mandarin
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Chinese drama film directed by Zhang Guoli and written by Zou Jingzhi. It was released in mainland China on August 18, 2006.

==Plot==
A young woman named Yishu believes she has no luck at all and she blames her name for it. One day she starts receiving phone calls from people thinking that she is the popular singer Tianyou. It turns out that someone has leaked 600 phone numbers of famous people to the internet and Yishus phone number got mixed up in the bunch.

While Yishu finds it annoying at first, she soon receives a text message from Xiaowen, who is the singer in an amateur rock band. He wishes to write a song for Tianyou and wants Yishu - who he thinks is Tianyous assistant - to deliver the song to Tianyou. Tianyou is a popular singer, but hasnt had a hit number lately.  While she wants to sing for the sake of art, her cruel agent cares only for money and pressures her into recording an album she does not want to record because it doesnt contain any good songs.  This decision gets her into a lot of trouble and she is attacked, furthering her personal depression.

While Yishu hears more of Xiaowens song through their phone calls and text messages, Yishu is distracted from the problems she attributes to her bad luck.  However, Xiaowen conceals the fact he is dying from a terminal illness, thus his desperation to finish his song and have it delivered to Tianyou.  Meanwhile, Tianyou has lost interest in singing, since none of the songs she has been forced to record mean anything to her, and she contemplates suicide, a path that her agent secretly delights in because it will mean her unreleased record will sell-out after her death.

On the night that Tianyou is about to commit suicide, Xiaowen calls her cellphone from his hospital bed, having received the correct phone number from Yishus efforts to find it, and sings her his song.  Tianyou is inspired to continue and her career is revitalized; at her next concert, she dedicates the song Xiaowen has written to him and thanks him for saving her before singing it.  Tianyou sends Yishu tickets to her concert, for one each for Yishu and Xiaowen, and Yishu invites Xiaowen, hoping to finally meet him.  However, at the end of the concert, she finds Xiaowens bandmates and learns from them that Xiaowen has already died.

==Cast==
* Zhou Bichang as Yi Shu
* Cecilia Cheung as Tianyou
* Hu Ge as Xiaowen
* Niu Ben as Old Man Li
* Zhang Guoli as Chairman of the Board Zhang Meng as He Ling
* Wei Zongwan
* Liu Yiwei
* Guo Degang

==External links==
*  
*   at Cinemasie

 
 
 
 
 


 
 