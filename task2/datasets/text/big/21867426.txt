Retrato de Familia
{{Infobox Film
| name           = Retrato de Familia
| image          = Retrato de familia, film poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Antonio Giménez-Rico
| producer       = José Sámano
| writer         = Antonio Giménez-Rico   José Sámano
| starring       = Antonio Ferrandis   Amparo Soler Leal   Miguel Bosé
| music          = Carmelo A. Bernaola
| cinematography = José Luis Alcaine
| editing        = Rosa Salgado
| studio         = Sabre Films , C. B. Films S. A
| distributor    = 
| released       = 17 September 1976
| runtime        = 97 minutes
| country        = Spain
| language       = Spanish
| budget         = 
| gross          = 
}}
 Spanish film directed by Antonio Giménez-Rico. It stars Antonio Ferrandis, Amparo Soler Leal and Miguel Bosé, making his film debut. This is Giménez-Rico ’s best known film. Schwartz, The Great Spanish Films, p. 34  The script was written by him, based on the novel Mi idolatrado hijo Sisi (My Beloved Son Sisi), by Miguel Delibes. The film was shot in Burgos, the director’s birthplace, although the novel takes place in Cáceres, Spain|Cáceres. Schwartz, The Great Spanish Films, p. 35 

==Plot==
Spain in the 1930s. The Rubes clan is headed by the aristocratic father Cecilio, a roué in love with another woman, Paulina, although he lives with his wife and his teenage son, Sisi. His wife hates the thought and practice of normal sexuality in their marriage. Their son notes his parent’s sexual frustrations but must experience his own initiation into the mysteries of love and sex. We watch Sisi grows, mature, and compete with his father for the attentions and sexual favors of the local actresses in Burgos.

One day, quite by chance, Sisi meets Paulina, his father’s more or less discarded lover, and they begin a tumultuous affair. Sisi is then inducted  into the army and goes off to war. News of his death arrives quickly. Apparently, Sisi was killed by a land mine while driving his army truck. His father wants his wife  to bear him another son, but she refuses. The old man, broken hearted by Sisi’s death, returns to his former mistress Paulina, who tells him she is pregnant by his own son. With no other legal heirs, the elder Cecilio Rubes, desperate, jumps off his mistress’s balcony and commits suicide. Paulina had always wanted the elder Rubes’s child but is now content with having one sired by his son.

== Cast ==

*Antonio Ferrandis as Cecilio Rubes
*Amparo Soler Leal  as Adela Rubes
*Mónica Randall  as Paulina
* Miguel Bosé as  Sisi Rubes
*Gabriel Llopart as Luis Sendin
*Encarna Paso as Gloria Sendin

==Notes ==
 

== References ==
 

== External links ==

*Schwartz, Ronald, The Great Spanish Films: 1950 - 1990, Scarecrow Press, London, 1991, ISBN 0-8108-2488-4

 
 
 
 
 
 
 