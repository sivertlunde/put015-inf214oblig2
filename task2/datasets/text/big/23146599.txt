Killer Fish
{{Infobox film
| name           = Killer Fish
| image          = Killer-fish.jpg
| caption        =
| director       = Antonio Margheriti
| producer       = Alex Ponti
| writer         = Michael Rogers
| starring       = Lee Majors Karen Black Margaux Hemingway Marisa Berenson James Franciscus
| music          = Guido De Angelis Maurizio De Angelis
| cinematography = Alberto Spagnoli
| editing        = Cesare DAmico
| studio         = Fawcett-Majors Productions Victoria Productions Filmar do Brasil
| distributor    = Paris Filmes
| released       =  
| runtime        = 101 minutes
| country        = Italy France Brazil
| language       = English
| budget         =
}}
 Brazilian horror movie directed by Antonio Margheriti.   The film, along with many monster movies of the 1970s and 1980s, is very similar to Jaws (film)|Jaws (1975).

==Plot==
The mastermind behind a precision theft of priceless emeralds decides to hide the jewels at the bottom of a reservoir hes secretly stocked with savage deadly piranha.  Retrieving the gems turns to be a caper in itself since the group is now torn by suspicion and jealousy.  Several gang members try to recover the loot on their own, only to become screaming victims of the insatiable horde of killer fish.  The treasure is down there just waiting to be brought up.  To get them, everyone must face the inescapable terror of thousands of man-eating creatures.

==Cast==
* Lee Majors as Robert Lasky
* Karen Black as Kate Neville 
* Margaux Hemingway as Gabrielle
* Marisa Berenson as Ann
* James Franciscus as Paul Diller
* Roy Brocksmith as Ollie
* Dan Pastorini as Hans
* Frank Pesce as Warren
* Charles Guardino as Lloyd
* Anthony Steffen as Max
* Fábio Sabag as Quintin Gary Collins as Tom

==Filming==
The film was made on location in the city of Angra dos Reis, Rio de Janeiro, Brazil.   

==Release==
Key Video released the film on VHS in the US in 1980. The film saw its first DVD release in Italy in 2002 by Pulp Video. There were other DVD releases in Germany and Spain in the following years, all in full screen. In 2014, Scorpion Releasing, in conjunction with ITV studios, released the first Blu-ray and DVD of the film in the US on September 30th. This release almost marked the first HD widescreen release in the world, sourced from the original interpositive.

==Reception==
Despite the low budget, Killer Fish has received generally positive reviews by critics, and currently has an 60% positive score at Rotten Tomatoes from the general audience.

==References==
 

==External links==
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 