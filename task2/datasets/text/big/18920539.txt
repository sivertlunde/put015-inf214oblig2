Dance, Little Lady
 
 
{{Infobox film
| name           = Dance, Little Lady
| image	         = "Dance,_Little_Lady"_(1954).jpg	
| caption        = Belgian theatrical poster
| director       = Val Guest
| producer       =  George Minterl
| writer         = Val Guest Doreen Montgomery
| based on       = a story by R. Howard Alexander and Alfred Dunning
| starring       = Terence Morgan  Mai Zetterling  Guy Rolfe  Mandy Miller
| music          = Ronald Binge
| cinematography =  Wilkie Cooper
| editing        =   John Pomeroy
| studio         = George Minter Productions (as Alderdale)
| distributor    =  Renown Pictures (UK)
| released       = 13 July 1954	(London) (UK)
| runtime        = 87 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}
Dance, Little Lady is a 1954 British film directed by Val Guest, and starring Terence Morgan, Mai Zetterling, Guy Rolfe and Mandy Miller. 

==Plot==
Prima ballerina Nina Gordon is being financially exploited by her husband Mark (Terene Morgan). On the night of her triumphant Royal Opera House debut, she discovers he is also being unfaithful. Distraught, she drives off into the night in a fury, breaking her leg in a motor accident. Learning that shell never dance again, Nina is abandoned by Mark. But with the help of a sympathetic doctor (Guy Rolfe), Nina recovers the use of her legs, and begins to live her life vicariously through her talented daughter (Mandy Miller). When Mark reenters Ninas life, intending to take control of the daughters dancing career, he finds the tables are turned on him.

==Cast==
* Terence Morgan as Mark Gordon  
* Mai Zetterling as Nina Gordon  
* Guy Rolfe as Dr. John Ransome  
* Mandy Miller as Jill Gordon  
* Eunice Gayson as Adele  
* Reginald Beckwith as Poldi  
* Ina De La Haye as Mme. Bayanova  
* Harold Lang as Mr. Bridson  
* Jane Aird as Mary   David Poole as Dancer  
* Maryon Lane as Dancer   William Kendall as Mr. Matthews  
* Joan Hickson as Mrs. Matthews  
* Alexander Gauge as Joseph Miller  
* Marianne Stone as Nurse
* Vera Day uncredited

==Critical reception==
The Radio Times wrote, "the dance sequences are fine, but the poor production values ruin the look of the film" ;  while TV Guide called it "a trite film" ;  but Sky Movies wrote, "Terence Morgan makes the best impression, as a sponger as smooth as he is nasty, in this ballet-orientated story, tailored to the talents of Britains then screen wonder child, Mandy Miller. It bases its appeal on a blend of small-girl sentiment, highly coloured melodramatics and ballet (the dance ensembles are very well done). Mai Zetterling and Guy Rolfe provide rather limp support to Mandys undeniable charm, but the storys fiery climax is most effective."  

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 