Mahakaal (2008 film)
{{Infobox film
| name = Mahakaal
| image = Mahakaal.jpg
| caption        =  The poster for the film Mahakaal
| director = Swapan Ghosal
| writer =
| starring = Prasenjit Indira Dhar
| producer =
| distributor =
| cinematography =
| editing =
| released =  
| country        = India
| budget         = 
| gross          = 
| runtime = 2:20:00 Bengali
| music = Devjit
}}
Mahakaal ( ) is a 2008 Bengali film directed by Swapan Ghosal. Mahakaal is basically a revenge story based on Anil Kapoor- Madhuri Dixit Starred Parinda. Indira Dhar,debut this film,21 year of age, is heroine in the film. She find her romancing with Prosenjit. She plays the role of a college goer Ratri) who falls in love with Prosenjit (Joy). Sangeeta Dhar has hosted few TV shows like Wishing Subhodin and Tollywood Reported at Sangeet Bangla but Mahakaal is her first appearance in big flat screen! 
{{Cite news
|url=http://www.telegraphindia.com/1081103/jsp/entertainment/story_10052873.jsp
|title=Maha-bore
|publisher=www.telegraphindia.com
|accessdate=2008-11-03
|last=Das
|first=Mohua
|location=Calcutta, India
|date=3 November 2008
}}
  
{{Cite web
|url=http://calcuttatube.com/2008/11/01/mahakaal-2008/
|title=Mahakaal (2008)-Calcutta Tube
|publisher=calcuttatube.com
|accessdate=2008-11-03
|last=
|first=
}}  
 
 
{{Cite web
|url=http://www.gomolo.in/Movie/Movie.aspx?mid=21527
|title=Mahakaal (2008) Movie
|publisher=www.gomolo.in
|accessdate=2008-11-03
|last=
|first=
}}
 

==Plot==
“Prof. Ajoy Mukherjee” and his spouse “Aditi” witness a murder, committed by the vociferous criminal “Digbijay”. In spite of repeated warnings from “Digbijay” and his right hand “Loha” the Professors give witness against them and they go to jail for 7 years. After coming out Djgbibay turns out to be even stronger. He attacks Ajoy’s family. He sends a man called “Binod Sharma” who pretends to be the friend of Joy (Ajoy’s brother). Digbijay and Binod conspire against Joy and Ajoy. After sending his own man to rob Joy of two lack rupees, Binod compels Joy to do a murder. In the meantime Digbijay stabs Ajoy and Joy gets entangled for the murder of his brother. A local inspector Dilip Lahiri also turns out to be a peer of Digbijay. While Joy remains in police custody, Digbijay tactically rapes and murders Joy’s younger sister Dia. After all these incidents Joy’s sister-in-law Aditi commits suicide. Joy teams up with his friend Kanchan and Kumar to seek revenge killing Binod, Dilip, Loha and Digbijay one after the other. Joy and his friends are jailed for 5 years.

==Cast==
* Prasenjit   as   Joy Mukherjee
* Indira Dhar   as   Ratri
* Tapas Paul   as  Ajay Mukherjee
* Laboni Sarkar  as  Aditi Mukherjee
* Rajatava Dutta   as   Binod Sharma
* Kaushik Banerjee as Digbijoy
* Pushpita Mukherjee  as   Dia Mukherjee
*  Kanchan Mullick as   Kanchan Mallick
* Sumit Gangopadhyay   as   Loha Rajesh Sharma as   Inspector Dilip Lahiri

==References==
 

==External links==
* 

 

 
 
 
 