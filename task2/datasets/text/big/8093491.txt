Restless Knights
{{Infobox Film
| name           = Restless Knights
| image          = RestlessKnightsTITLE.jpg
| caption        =
| director       = Charles Lamont Felix Adler
| starring       = Moe Howard Larry Fine Curly Howard Geneva Mitchell Walter Brennan Stanley Blystone
| cinematography = Benjamin H. Kline
| producer       = Jules White | William Lyon
| distributor    = Columbia Pictures
| released       = February 20, 1935
| runtime        = 16 11"
| country        = United States
| language       = English
}}
Restless Knights is the sixth short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
Set in the 17th century, the father of the Stooges calls them out to tell them something shocking: they learn they are of royal blood.

Now dubbed as the Duke of Durham (Larry), the Count of Fife (Moe), and Baron of Grey Manor (Curly), their father entrusts them to take up arms and protect the young queen of their old kingdom of Anesthesia, as word had spread that the present prime minister, Prince Boris, may attempt to seize the throne. The Stooges accept and make their way to Anesthesia, where, as the "Duke of Mixture," the "Fife of Drum," and "Baron (or Barren) of Brains," they become the queens royal guards. The prince begins to enact his plan to abduct the queen as a royal wrestling match starts. Disgusted by the result of the match, Moe and Curly take it upon themselves to wrestle in their place with Larry acting as the referee. After an unforgettable match, the queen ends up missing with the Stooges blamed for being lax. A sword fight ensues, and the Stooges are taken away to be executed.

The Stooges are sentenced to be shot by crossbows but before the arrows are shot the archers spot a woman undressing at a window. Enchanted, they watch her, giving the Stooges a chance to escape. As they hide out from the guards someone drops a jug on Curlys head, containing a note stating that the queen is hidden in the wine cellar. The Stooges head to the cellar, spotting a few of the men who had taken the queen and enact a plan; Curly lures them out one by one, and Moe and Larry knock them out. However, one guard trips while chasing Curly and Moe and Larry inadvertently knock themselves out. Curly takes off with the guard hot on his trail, until Curly sneaks up behind him and knocks him out. Curly then runs back to the alcove, where Moe and Larry come to and hear his footsteps. Thinking them to be of the guard they missed they swing their clubs and knock Curly out instead. The queen, tied up and hidden away, is able to loosen the gag on her mouth and call for help. The Stooges rush over to her rescue, but a few more guards show up to search the wine cellar for them. Moe instructs Curly to do the same tactic he did before, but as he and Larry take position and Curly dashes past the doorway, the queen follows him, unaware of their plan, and is accidentally knocked out. Once they realize what they did, all three Stooges hit themselves on the head, knocking each other out.

==Production notes==
The title Restless Knights is a pun on "restless nights," or chronic insomnia. 
 grey matter

==Quotes==
**Queen Anne: "And what were you doing in Paris?"
**Moe: "Oh, looking over the paris-ites."

**Archer: (as the boys stand in front of the crossbow firing squad) "Ready! Aim!"
**Larry: "Maybe theyll miss us!"
**Curly: "That would an-arrow escape! Nyuk nyuk nyuk-" *BONK!*

**Moe: (stopping the archers) "Hold on a minute, now! Aint there another way we can die?"
**Archer: "You may either have your head cut off, or be burned at the stake!" (chuckles)
**Larry: "Cut my head off!"
**Curly: "Not me, Id rather be burned at the stake!"
**Larry: "Why?"
**Curly: "A hot steak is better than a cold chop!" *BONK!*

**Knight: (after the Queen has been kidnapped) "The guards have been lax" 
**Moe: "We have not — weve been wrestlin!"

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 