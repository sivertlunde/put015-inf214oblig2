Topiwala
 
 

{{Infobox film
| name = Topiwala
| image = Topiwala movie poster.jpg
| caption = Theatrical Poster
| director = MG Srinivas
| producer = {{plainlist |
* Kanakapura Sreenivas
* K. P. Srikanth
}}
| writer = Upendra
| starring = {{plainlist |
* Upendra Bhavana
* Raju Talikote
}}
| music = V. Harikrishna
| cinematography = Shreesha Kuduvalli
| editing = Sri Crazymindz
| studio =  Crazymindz
| released =  
| runtime = 
| country = India
| language = Kannada
| budget =  
| gross =    +   satellite Rights
}}
 Kannada political Bhavana in the lead roles. The film is written by Upendra and directed by MG Srinivas. Kanakapura Sreenivas and K.P. Srikanth jointly produce this venture under R.S. Productions banner. V. Harikrishna composed the music. 

The film was released on 15 March 2013 in 150 Karnataka theaters  and grossed   in its opening weekend  and more than   in its first week. 

==Cast==
* Upendra as Basak Bhavana as Suman Bedi
* Rangayana Raghu as Ramanayana Raghu
* Maithreyi as Malashri
* P. Ravi Shankar as Sarkar
* Vijanath Biradar as Mr India
* Raju Talikote
* Achyut Kumar as Lokayuktha Loki
* Mukti Mohan (Special appearance)

==Production==

===Casting=== Bhavana of Jackie (2010 film)|Jackie fame was cast in the female lead. The film was produced by Kanakapura Sreenivas, who produced Upendras Omkara (2004 film)|Omkara in 2004. The film also stars debutante actress Maithreyi, Rangayana Raghu, and Raju Talikote in supporting roles while Ravishankar plays the antagonist. Vijanath Biradar has a surprise cameo.   Mukti Mohan, participant of Star Ones dance reality show Zara Nachke Dikha, was brought in for an item song. 

===Promotions===
At the first press meet of the film, held in December 2012, producer Kanakapura Srinivas said that a younger team was brought in for the film to try to generate fresh thoughts in presentation. "Now, we have Topiwala with a young team which is sure to create new bench marks", said Sreenivas. Upendra described the film as a political satire based on current affairs and corruption. "The film is based on incidents that happen in contemporary times. Political incidents are part of the script. It is a humorous commentary on every distortion that we see in our society", said Upendra. 

==Reception==

===Box office===
Topiwala was released on 15 March 2013 in over 150 theaters in Karnataka. Topiwala opened to strong box office collections on its release day with 90 to 100% occupancy at single screens and around 90% occupancy at multiplexes. The film grossed   during its opening weekend  and grossed more than   in its first week.  Upendra and the entire team of Topiwala celebrated a success meet of the movie at the Udayavani Head Office in Bangalore, by launching a campaign against corruption.  Topiwala did average business at the box office  and grossed around   in five weeks earning a net share of   at the box office. The film also did an additional business of   through satellite and audio rights, therefore recovering its cost of production.

==Soundtrack==
The films music was composed by V. Harikrishna. Audio CDs were released into the market on 13 February 2013 and an official audio launch was held at the Windsor Manor Hotel in Bangalore on 23 February 2013. All three brothers of the Rajkumar family, Shivrajkumar, Raghavendra Rajkumar, and Puneeth Rajkumar attended the audio launch along with cast and crew of the film and other film personalities. 

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Shangrila
| lyrics1 	= Yogaraj Bhat
| extra1        = Manu, Priyadarshini
| length1       = 04:31
| title2        = Topiwala
| lyrics2 	= MG Srinivas
| extra2        = V. Harikrishna
| length2       = 04:51
| title3        = Girgitle Grigitle
| lyrics3       = Upendra
| extra3 	= Vijay Prakash, Kannika Urs
| length3       = 04:28
| title4        = Gala Gala
| extra4        = Supriya Lohit, Tippu
| lyrics4 	= Topiwala Team
| length4       = 03:43
| title5        = Theme
| extra5        = 
| lyrics5 	= 
| length5       = 01:43
}}

==References==
 

==External links==
*  
*   (An article in Kannada)
*   at  

 

 
 
 
 
 
 
 