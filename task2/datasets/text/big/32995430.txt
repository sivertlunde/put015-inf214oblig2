The Strangler's Grip
 
 
{{Infobox film
| name           = The Stranglers Grip
| image          = 
| image_size     =
| caption        = 
| director       = 
| producer       = 
| writer         = 
| narrator       =
| starring       = 
| music          =
| cinematography = Franklyn Barrett
| editing        = 
| studio   = Wests Pictures
| distributor    = 
| released       = 5 February 1912 
| runtime        =
| country        = Australia
| language       = Silent film  English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}}

The Stranglers Grip is a 1912 Australian silent film shot by Franklyn Barrett. It is considered a lost film. 

==Plot==
The movie featured a "furious motor ride in the night" 

==Cast==
*Sidney Stirling as Squatter John Dalton 
*Cyril Mackay as his friend Frank Wood
*Leonard Willey as Mike Logan, the tramp
*Chas Laurence as Old Simon, Johns Butler
*Master Willey as Bill Dalton, Johns Son
*Irby Marshall as Maurice Dalton, Johns wife

==Production==
It is likely that the film was directed jointly by the three lead actors, Sydney Stirling, Cyril Mackay and Leonard Willey. It was the first of four movies they made for Wests Pictures in 1912.  Cyril Mackay (d. 1923) was a London stage actor brought out to Australian by J.C. Williamson in 1906. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 31 

==Reception==
The film was described as being a "splendid draw" with the public.  It was likely the first thriller film made in Australia. 

==Trivia==
Willey and Irby Marshall were real life husband and wife. They later moved to the USA and had successful stage careers there. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 