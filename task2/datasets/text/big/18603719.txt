Mille chilometri al minuto!
 
{{Infobox film
| name           = Mille chilometri al minuto!
| image          = 
| caption        = 
| director       = Mario Mattoli
| producer       = Alfredo Villetti
| writer         = Aldo De Benedetti Mario Mattoli
| starring       = Nino Besozzi
| music          = 
| cinematography = Domenico Scala
| editing        = Mario Mattoli
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

Mille chilometri al minuto! (or 1000 km al minuto!) is a 1939 Italian comedy film directed by Mario Mattoli and starring Nino Besozzi.  

==Cast==
* Nino Besozzi - Guido Renzi
* Antonio Gandusio - Lavvocato
* Mario Ersanilli - Lo scienziato
* Vivi Gioi - Figlia del scienziato
* Romolo Costa - Un altro scienziato
* Franca Volpini - Una cameriera (as Flora Volpini)
* Amelia Chellini - La moglie dell avvocato

==External links==
* 

 

 
 
 
 
 
 
 
 