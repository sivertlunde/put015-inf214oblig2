List of lesbian, gay, bisexual or transgender-related films of 1993
 
This is a list of lesbian, gay, bisexual or transgender-related films released in 1993. It contains theatrically released cinema films that deal with or feature important gay, lesbian or bisexual or transgender characters or issues and may have same-sex romance or relationships as an important plot device.

==1993==
{| class="wikitable sortable"
! width="16%" | Title
! width="4%" | Year
! width="15%" | Director
! width="15%" | Country
! width="13%" | Genre
! width="47%" | Notes
|- valign="top"
|- And the Band Played On|| 1993 ||   ||  || Drama||
|-
|Anthem (film)|Anthem|| 1993 ||   ||  || Short||
|-
|Belle (1993 film)|Belle|| 1993 ||   ||  || Romance ||
|-
| || 1993 ||  , Stephen Cummins, Laurie Lynd, Michael Mayson, Chris Newby, Marlon Riggs ||        || Drama||
|-
|Blue (1993 film)|Blue|| 1993 ||   ||   || Drama ||
|- Desperate Remedies|| Peter Wells ||   || Drama||
|-
|Dönersen Islık Çal|| 1993 ||   ||   || Drama ||
|- Even Cowgirls Get the Blues|| 1993 || Gus Van Sant ||  || Romantic, comedy, drama||
|-
|Feed Them to the Cannibals!|| 1993 || Fiona Cunningham-Reid ||  || Documentary ||
|-
|Gece, Melek ve Bizim Çocuklar|| 1993 ||   ||   || Drama ||
|-
|Grief (film)|Grief|| 1993 ||   ||  || Comedy||
|- Farewell My Concubine|| 1993 ||   ||     || Romantic, drama||
|- 
|Kika (1993 film)|Kika|| 1993 ||   ||     || Drama, comedy ||
|-
|Love and Human Remains|| 1993 ||   ||  || Drama ||
|- One Nation Under God|| 1993 ||  , Francine Rzeznik ||  || Documentary||
|-
|Philadelphia (film)|Philadelphia|| 1993 ||   ||  || Drama||
|- Poison Ivy|| 1993 ||   ||  || Drama, thriller||
|-
|Prinz in Hölleland|| 1993 ||   ||  || Drama ||
|-
|Relax (film)|Relax|| 1993 ||   ||  || Short ||
|-
|Sex Is...|| 1993 ||   ||  || Documentary||
|-
| || 1993 ||   ||   || Drama||
|-
| || 1993 ||  , Tom Joslin ||   || Documentary ||
|- Six Degrees of Separation|| 1993 ||   ||  || Drama, mystery||
|- Three of Hearts|| 1993 ||   ||  || Romantic, comedy||
|-
| || 1993 ||   ||   || Romantic, drama||
|-
|Totally Fucked Up|| 1993 ||   ||  || Drama||
|-
|Todos a la cárcel|| 1993 ||   ||  || Comedy||
|-
|Verzaubert|| 1993 ||  , Jens Golombek, Dirk Hauska, Sylke Jehna, Claudia Kaltenbach, Ulrich Prehn, Johanna Reutter, Katrin Schmersahl, Dorothee Van Diepenbroick ||  || Documentary ||
|-
| || 1993 ||   ||    || Romantic, comedy, drama||
|-
|Wittgenstein (film)|Wittgenstein|| 1993 ||   ||    || Drama||
|-
|Zero Patience|| 1993 ||   ||     || Musical||
|}

 

 
 
 