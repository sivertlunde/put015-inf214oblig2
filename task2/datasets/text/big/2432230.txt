The World According to Bush
The French documentary, Nazi period. The film  examines and interviews Bush supporters, including the Christian right, Neoconservatism in the United States|neo-conservatives, and businessperson|businesspeople, as well as Bush critics and shows televised statements made by Bush and his supporters, including Jerry Falwell. It was nominated for the Best Documentary Award at the European Film Awards and was to have been an Official Selection for the 2004 Cannes Film Festival but was rejected following the selection of Michael Moores Fahrenheit 9/11.

The film features interviews with (in order of appearance):

* Michael Ledeen
* David Frum
* Jim Hoagland, Washington Post
* Stanley Hoffmann, Harvard University
* Jerry Falwell
* James Robison
* Ed McAteer
* Arnaud de Borchgrave, Washington Post
* Robert David Steele, Central Intelligence Agency
* James Woolsey
* Richard Perle
* Robert Baer, Central Intelligence Agency
* Anthony Blinken
* David Corn, The Nation
* Hans Blix
* Sam Gwynne, Texas Monthly
* Colin Powell Joseph Wilson
* Charles Lewis (journalist), Center for Public Integrity
* Viet Dinh
* Norman Mailer
* Frank Carlucci, Carlyle Group
* Laurent Murawiec, former analyst at Rand Corporation
* David Kay

== External links == CBC
* 

 

 
 
 
 
 
 


 
 