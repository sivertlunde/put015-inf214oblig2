Monogamy (film)
 
{{Infobox film
| name           = Monogamy
| alt            =
| image          = Monogamy FilmPoster.jpeg
| caption        =
| director       = Dana Adam Shapiro
| producer       =
| writer         = Dana Adam Shapiro Evan Wiener
| narrator       = Chris Messina Rashida Jones Meital Dohan
| music          = Jamie Saft
| cinematography = Doug Emmett
| editing        = Mollie Goldstein Jeremy Kotin
| studio         = Renart Films
| distributor    = Oscilloscope Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = $1 million
| gross          =
}} Chris Messina) and Nat (Rashida Jones).

==Synopsis==
Theo is bored with his job as a wedding photographer—the generic backgrounds, the artificial posing, the stilted newlyweds—so he develops an unconventional side business, called "Gumshoot," a service where clients hire him to stalk them with his camera. Becoming infatuated with one of his clients, a mystery woman who goes by the name Subgirl (Meital Dohan), Theo develops a voyeuristic obsession that forces him to confront uncomfortable truths about himself and his impending marriage.

==Production notes==
Monogamy premiered April 24, 2010 at the Tribeca Film Festival.

==External links==
*  
*  
*  
*  
*  
*  

 
 
 