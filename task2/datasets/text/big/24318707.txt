Sura (film)
 
 
{{Infobox film
| name = Sura
| image = Sura Vijay.jpg
| alt =  
| caption =
| director = S. P. Rajkumar
| producer =Sangili Murugan
| writer = S. P. Rajkumar
| starring =  
| music = Mani Sharma
| cinematography = M. S. Prabhu N. K. Ekambaram
| editing =DonMax Murugan Cine Arts
| released =  
| runtime =   170 minutes
| Box ofiice= 500 crores
| country = India
| language = Tamil
}}
 Vijay in the lead role, starring in his 50th film, along with Tamannaah.Sura was produced by Sangili Murugan and distributed by Sun Pictures.The films soundtrack, composed by Mani Sharma,was released in Match 2009.The Film Released on 30 April 2010 worldwide. The film received mixed to negative reviews and was declared as a "Flop"  at the box office.

==Plot==
Sura is a story that is set in Yaazh Nagar, a fishermen hamlet in coastal Tamil Nadu.Sura (Vijay) is born and brought up here in the company of his friend, Ambar La, alias Umbrella (Vadivelu). Meanwhile, he comes across young bubbly Poornima (Tamannaah) who chooses to end her life unable to cope up with the death of her pet dog.

Slowly, she becomes attracted towards Sura thanks to his good deeds. They start romancing. When things seem to go well, trouble enters in the form of a greedy and corrupt Minister Samuthira Raja(Dev Gill).He wants to usurp the land where these fishermen live. His attempts to take away the wealth is resisted by Sura. They cross fight each other. The Minister hatches a conspiracy to bump off Sura with the help of his Ministerial post and authority and sues him. Sura takes on them single-handedly and finally destroy his bad intention and help his town fishermen to get their own house as he wished.

But the Minister is plotting revenge against. Sura for taking his money and because he had lost that land in which he was about to build a fair.

He kidnapped Poornima and asks Sura to meet him on a boat.A fight occurs and Sura kills the Minister and rescues Poornima.

==Cast== Vijay as Sura
* Tamannaah as Poornima
* Vadivelu as Umbrella
* Dev Gill as Samuthira Raja
* Riyaz Khan
* Yuvarani as Samuthira Rajas wife
* Sriman as Dhandapani
* Radharavi as Church Father
* "Paruthiveeram" Sujatha as Suras mother
* Ilavarasu
* Madhan Bob as Poornimas Father
* Sachin Tyler Sadachcharan as Dinesh
* R. Thyagarajan as Judge
* Mahanadhi Shankar as Kandhu Batti Govinda
* Shobha
* Vaiyapuri
* Balaji
* King Kong
* Venniradai Moorthy

==Production== Raja Mohan, Once More. 

In May 2009, Tamannaah was confirmed to play a role in the film.          Originally Pasupathi was to be playing a villain but instead Dev gill of Magadheera was selected as a villain  after Vijay being impressed by his performance in Magadheera. 

The shooting of the film commenced in Kerala in November 2009  and was continued in different places in Tamil Nadu like Marakkanam, Villupuram, Mamallapuram, Chennai, Alleppey and Pondicherry.  The shoot continued on 21 October and canned a few stunt sequences on the Uppaneri Bridge near Marakkanam Salt Lake. Vijay fought with villain Devs gang for almost three days. After a three days gap, the shoot resumed in Prasad Studios and the song thanjavur jillakari was shot at AVM Studio on 27 October.   Unit created a slum set at ECR.  A song was shot at Ooty.  During the shooting, cameraman M. S. Prabhu and Rajakumar had misunderstanding and he was replaced by N. K. Ekhambharam.  Ekambaram reveals that one of the fight sequences, which were shot in the high seas in Tuticorin at 2 am, This fight was choreographed by Kanal Kannan and has stuntmen from Bangkok. Ekambaram says that he has used high speed camera considering the risks involved in shooting in the seas. The opening song has been picturised in a scenic location in Dhanushkodi. 

==Soundtrack==
{{Infobox album
| Name = Sura
| Type = Soundtrack
| Cover = 
| Released =  
| Recorded =
| Artist = Manisharma
| Genres =
| Label = Sony Music
| Producer = Mani Sharma
| Reviews =
| Last album = Maanja Velu (2010)
| This album = Sura (2010)
| Next album = Varudu (2010)
}}
 Kabilan for penning lyrics that suited his preferences in Vettaikaran. A jubilant Kabilan reveals that Sura’s lyrics were written even before Vettaikaran was released and it was during this discussion he was given the ring.  The album features 6 songs.The Songs were all CHartBusters.

{{tracklist
| headline = Track listing
| extra_column = Singer(s)
| total_length =
| title1 = Vetri Kodi Yethu
| extra1 = Ranjith (singer)|Ranjith, Mukesh
| length1 = 5:21
| title2 = Thanjavoor Jillakkari
| extra2 = Hemachandra(singer)|Hemachandra, Saindhavi
| length2 = 5:21
| title3 = Naan Nadanthal Athiradi
| extra3 = Naveen, Shoba Chandrasekar, Janani Madhan
| length3 = 4:35
| title4 = Vanga Kadal Ellai
| extra4 = Naveen, Maladhi Lakshmanan
| length4 = 4:45
| title5 = Siragadikkum Nilavu
| extra5 = Karthik (singer)|Karthik, Reeta
| length5 = 4:29
| title6 = Thamizhan Veera Thamizhan
| extra6 = Rahul Nambiar
| length6 = 3:47
}}

==Release==
 Sun TV. The film was given a "U" certificate by the Indian Censor Board.  Release was originally slated to 23 April 2010 but posponded to 30 April 2010   

===Critical Reception===
The film received predominantly negative reviews and was considered a flop.  Behindwoods gave it one and a half stars, declaring it "Sura is a usual Vijay fare ."  The entertainment site Sify agreed that "the story is wafer thin and as old as the Kodai hills", but stated that Sura proved that "conviction can bring any formula to life".  rating  . Rediff gave 2 out of 5 stars and stated that "Give your brains a rest, and enjoy this mass masala entertainer". 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 