The Camp on Blood Island
 
 
{{Infobox film
| name           = The Camp on Blood Island
| image          = Camponbloodisland.jpg
| image_size     = 
| caption        = UK release poster
| director       = Val Guest
| producer       = Anthony Hinds
| writer         = Jon Manchip White
| starring       = Carl Möhner André Morrel Edward Underdown Walter Fitzgerald
| music          = Gerard Schurmann
| cinematography = Jack Asher
| editing        = Bill Lenny
| studio         = Hammer Film Productions
| distributor = Columbia Pictures
| released       = 15 April 1958 (UK) 11 June 1958 (U.S.)
| runtime        = 82 minutes
| country        = United Kingdom English
}}
The Camp on Blood Island is a 1958 British World War II film, directed by Val Guest for Hammer Film Productions and starring Carl Möhner, André Morell, Edward Underdown and Walter Fitzgerald.
 Lord Russell of Liverpool, "We may forgive, but we must never forget", and an image of a Japanese soldier wielding a samurai sword.

From its powerful opening sequence of a man being forced to dig his own grave before being shot to death, with an intertitle stating "this is not just a story - it is based on brutal truth", The Camp on Blood Island is noted for a depiction of human cruelty and brutality which was unusually graphic for a film of its time, and received some contemporary allegations of going beyond the bounds of the acceptable and necessary into gratuitous sensationalism.

It was followed by a sequel The Secret of Blood Island in 1964.

==Plot==
As the Pacific War draws to an end, the commandant of the Blood Island prisoner-of-war camp has let it be known that should Japan surrender, he will order the massacre of the entire captive population.  When the prisoners hear through underground sources that Japan has indeed surrendered, they mobilise themselves to try to prevent the news reaching the commandant.  Colonel Lambert (Morell), the authoritarian self-appointed leader of the prisoners, deems that they must sabotage communications between the camp and the outside world, and arm themselves in however makeshift a way in readiness for a final showdown.

Lamberts unilateral assumption of military authority is not universally welcomed, as other prisoners including Piet van Elst (Möhner), diplomat Cyril Beattie (Fitzgerald) and priest Paul Anjou (Michael Goodliffe) chafe against his quasi-dictatorial personality, obstinacy and refusal to listen to any views other than his own.  Lambert is forced continually to justify his at times apparently illogical and counter-productive decisions.  Matters are not helped by the growing suspicion that the camp harbours a collaborator in its midst.

Van Elst is given the task of chief saboteur, while Anjou passes messages and instructions to the captives via coded sermons.  When the endgame becomes inevitable, the prisoners rise up against their captors in a bloody insurrection, feeling that they have nothing left to lose and the survival of a few is better than the alternative.  When Allied relief planes finally arrive they find a mere handful of survivors on either side.

==Cast==
* Carl Möhner as Piet van Elst
* André Morell as Col. Lambert
* Edward Underdown as Major Dawes
* Walter Fitzgerald as Cyril Beattie Phil Brown as Lt. Peter Bellamy
* Barbara Shelley as Kate Keiller
* Michael Goodliffe as Father Paul Anjou
* Michael Gwynn as Tom Shields
* Ronald Radd as Commander Yamamitsu
* Marne Maitland as Captain Sakamura
* Richard Wordsworth as Dr. Robert Keiller
* Mary Merrall as Helen Beattie
* Wolfe Morris as Interpreter
* Michael Ripper as Japanese Driver
* Edwin Richfield as Sergeant-Major
* Geoffrey Bayldon as Foster
* Lee Montague as Japanese Officer
* Jan Holden as Nurse

==Production==
The film was allegedly based on a true story which Hammer executive Anthony Nelson Keys heard from a friend who had been a prisoner of the Japanese. Keys in turn told the story to colleague Michael Carreras who commissioned John Manchip White to write a script. Finance was provided as part of a co-production deal with Columbia Pictures and shooting began in Bray Studios on 14 July 1957. Marcus Hearn, "The Camp of Blood Island" Viewing Notes, Camp of Blood Island DVD, 2009 

==Reception==
The film was very successful at the box office, being one of the most popular British movies of the year, despite sometimes hostile reviews. 

The novelisation of the script sold over two million copies and has been described as "arguably the most successful piece of merchandise ever licensed by Hammer." Marcus Hearn, The Hammer Vault, Titan Books, 2011 p19 

==References==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 