King of the Mountain (film)
 
{{Infobox Film
| name =       King of the Mountain caption =  producer =    Jack Frost Sanders  writer =      Leigh Chapman H.R. Christian starring = Richard Cox Seymour Cassel Dennis Hopper director =    Noel Nosseck editing =     William Steinkamp distributor = Universal Studios released = May 1, 1981 runtime =     90 min. language =    English music =  budget = 
| gross = $2.1 million PRODUCERS GUBER AND PETERS: AN ENDLESS HONEYMOON?
Pollock, Dale. Los Angeles Times (1923-Current File)   26 July 1981: m1.  
}} film starring Richard Cox, cars up and down Mulholland Drive for both money and prestige.

The films primary focus is Steve (Harry Hamlin), who has found himself generally content with his uncomplicated life of working and racing. This creates some amount of tension between him and his friends, who have been losing their interest in racing and have been attempting to make serious inroads in the music industry. Steves blossoming relationship with singer Tina (Deborah Van Valkenburgh) causes him to re-think his mantra, as he realizes that a truly fulfilling life involves more than just work and play.

The film was poorly regarded critically and did not perform well in the box office, although it was significant in being among the first films about street racing and communities of street racers, as well as because it was inspired by the activities of real people who raced in the Los Angeles area. It also marked somewhat of a return for Dennis Hopper, who had spent several months secluded away from Los Angeles prior to making his appearance.

== Story == 356 Speedster, Steve races against both newcomers and veterans alike, never really considering the risks associated with the lifestyle or if there might be more for him elsewhere.
 Corvette and drove him over the edge mentally.

When Steve meets Tina (Deborah Van Valkenburgh), a young singer working with Buddy and Roger, he quickly falls for her and tries to introduce her to the world of street racing, but finds that she isnt as enthralled with it as he is and isnt interested in returning to the races or in being involved with a man is constantly risking his life for a thrill. Torn between Tina and his life as a racer, Steve must choose to either remain King, or abdicate and leave so that he can start over in "real life". All the while, Cal is secretly rebuilding his Corvette, and he wants a shot at winning his title back.

The films climax depicts a dangerous, high speed race down the hill between Steve and Cal.

== Background == Nichols Canyon Road and other adjacent streets. This community of racers became notorious to the nearby canyon residents and later, to people all across the city. Light Police patrols were eventually increased into a full-force crackdown on racing in the area after complaints were fielded from the residents and accidents, sometimes resulting in death and/or serious injury occurred. On a few occasions, Police were forced to chase suspected racers through the narrow roads, and were not always able to keep up, leading to a few "escapes" by the racers.

During the late 1970s an article titled "Thunder Road" appeared in an issue of New West Magazine, the article detailed some of what was going on in the canyon and made specific reference to two particular drivers; Chris Banning, who owned and raced a heavily modified Porsche 911 and Charles "Crazy Charlie" Woit, who owned and raced a vintage, 427 cubic-inch Chevrolet Big-Block engine|Big-Block powered Chevrolet Corvette. The article provided the inspiration for the film, and its two main characters, who were based on Banning and Woit.

In 2006, Chris Banning wrote a book entitled "The Mulholland Experience" detailing his experiences, what it was like being a part of that community, and being the inspiration for the film.

== Production ==

Harry Hamlins Speedster was actually a modified Volkswagen Beetle based kit car wearing a replica body with flared fenders; actual Speedsters did not have the flares. This was done primarily because real Speedsters are very rare; it would have been difficult and expensive to locate a real one for rent and the possibility of damaging the vehicle was too great.
 Corvette on film installed and each connected to an in-cabin switch allowing Hopper to turn them on when he started and off when the film ran out. On the day the footage was to be shot, Hopper was reportedly very drunk; he had spent the day in his trailer drinking beer. Despite crew protests, director Noel Nosseck allowed Hopper to drive the car, and according to crew members he entered the car with a fresh six-pack. After taking off, he spent almost an hour in the vehicle, despite there being only enough film for a few minutes of footage, and returned with the six-pack empty.

== Home media ==
The films only official home video release was a VHS format version available for a brief period during the 1980s. DVD copies of the film occasionally appear for sale on sites such as EBay, but these are illegal "backup" copies made from VHS cassettes.

==References==
 

== External links ==
*  

 
 
 