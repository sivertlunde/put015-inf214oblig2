Titoo MBA
{{Infobox film
| name           = Titoo MBA
| image          = Titoo MBA.jpg
| alt            = 
| caption        = First Look Poster
| director       = Amit Vats
| producer       = Rajan Batra Mayank Patel
| writer         = Puja Ballutia Amit Vats
| starring       = Nishant Dahiya Pragya Jaiswal Abhishek Kumar
| music          = Arjuna Harjai
| cinematography =  Karan B Rawat
| editing        = Abhijit Kokate Sunil Wadhwani
| studio         = Beatrix Entertainment
| distributor    = 
| released       =   }}
| runtime        = 107 minutes
| country        = India
| language       = Hindi
| budget         = 2 crore

| gross          = 
}}
 Hindi film directed by Amit Vats and produced by Rajan Batra and Mayank Patel in association with Beatrix Entertainment.  The film stars Nishant Dahiya and Pragya Jaiswal in principal roles.  The film was released on November 21, 2014. 

==Synopsis==
Titoo (Takhat Singh Gill) is your average Chandigarh boy with big dreams of becoming a businessman. But as usual, destiny plays spoilsport and nothing works out for Titoo. Added to that his heavy debts force him to take up a completely weird and unexpected career. In the midst of this financial mess, Titoo is married off to Gulshan Kaur Grover from Jalandhar following an elaborate arranged marriage. He leads a double life for a while and tries his best to hide his embarrassing secret from his newlywed wife. But as fate would have it, she finds out all about Titoo’s alternate career and the poor fellow’s life flips upside down once again.

==Cast==
* Nishant Dahiya as Titoo 
* Pragya Jaiswal as Gulshan 
* Abhishek Kumar
* Puja Ballutia
* Nandini Singh
* Anchal Singh

==Soundtrack==
The soundtrack is composed by Arjuna Harjai except for "Saiyaan Bedardi" which is composed by Leonard Victor.  The first single titled "O Soniye" sung by Arijit Singh and Vibha Saraf released on October 13, 2014. The lyrics was penned by Surabhi Dashputra.
   The second single titled "Plan Bana Le" sung by Aishwarya Nigam and Surabhi Dashputra released on October 30, 2014. Lyrics for the song was penned by Kumaar.  The third single titled "Kya Hua" sung by Arijit Singh was released on November 5, 2014.  Full album was released on November 11, 2014.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits  = yes
| total_length = 
| collapsed = no

| title1          = O Soniye
| extra1          = Arijit Singh, Vibha Saraf 
| lyrics1         = Surabhi Dashputra
| length1         = 04:16
| title2          = Plan Bana Le
| extra2          = Aishwarya Nigam and Surabhi Dashputra
| lyrics2         = Kumaar
| length2         = 04:16
| title3          = Kya Hua
| extra3          = Arijit Singh
| lyrics3         = Kumaar
| length3         = 04:39
| title4          = Atyachaari
| extra4          = Surabhi Dashputra, Arjuna Harjai
| lyrics4         = Surabhi Dashputra
| length4         = 03:23
| title5          = O Ranjhna
| extra5          = Surabhi Dashputra
| lyrics5         = Surabhi Dashputra
| length5         = 03:13
| title6          = Kyu Hua Khafa
| extra6          = Neeti Mohan
| lyrics6         = Kumaar
| length6         = 02:48
| title7          = Saiyaan Bedardi  (composed by Leonard Victor) 
| extra7          = Priyanka Agarwal, Abhishek Kumar
| lyrics7         = Nitin Raikwar 
| length7         = 03:48
}}

==External links==
*  
*  

==References==
 

 
 
 