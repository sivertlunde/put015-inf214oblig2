Schlock (film)
{{Infobox film
| name           = Schlock
| image          = Schlock.jpg
| caption        =
| director       = John Landis
| producer       = Jack H. Harris James C. ORourke
| writer         = John Landis
| starring       = John Landis Charles Villiers
| music          = David Gibson
| cinematography = Robert E. Collins
| editing        = George Folsey Jr.
| distributor    = Jack H. Harris (Enterprises) Anchor Bay Entertainment (DVD)
| released       =  
| runtime        = 80 min
| country        = United States English
| budget         = $60,000 (estimated)
}}
 1973 low-budget comedy horror film, written, directed by and starring filmmaker John Landis.

==Plot== prehistoric apeman blind beauty and terrorizes her Southern California suburb. Schlock is no ordinary simian; he possesses some very unusual skills. Among other things, he plays the piano and gives TV interviews. In this spoof of early monster movies and missing-link science fiction films, John Landis pays homage to the monster movies of the past with irreverent humor and wacky hijinks.

==Production== Rick Baker.

==Reception==
This film has received positive reviews, it currently holds a 60% score on Rotten Tomatoes.

==Release==
The film was released theatrically in the United States by Jack H. Harris Enterprises.  It opened in Hollywood in March 1973 and in West Germany on 17 September 1982.   

The film was released on DVD by Anchor Bay Entertainment in 2001.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 