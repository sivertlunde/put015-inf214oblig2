The Man from Home (1914 film)
{{Infobox film
| name           = The Man From Home
| image          = The Man from Home 1914 newspaperphoto.jpg
| caption        = Scene from the film.
| director       = Cecil B. DeMille
| producer       = Jesse L. Lasky Cecil B. DeMille
| writer         = Cecil B. DeMille Booth Tarkington Harry Leon Wilson Charles Richman
| music          =
| cinematography = Alvin Wyckoff
| editing        = Cecil B. DeMille
| studio         = Jesse Lasky Feature Plays
| distributor    = Paramount Pictures
| released       =  
| runtime        = 5 reels
| country        = United States Silent English intertitles
| budget         =
| gross          = 
}}
 The Man From Home, and released by Famous Players-Lasky.

==Cast== Charles Richman - Daniel Voorhees Pike
* Theodore Roberts - The Grand Duke Vaseill
* Fred Montague - Earl Of Hawcastle
* Monroe Salisbury - Hon. Almerce St. Aubyn
* Horace B. Carpenter - Ivanoff
* Jode Mullally - Horace Granger Simpson
* Dick La Reno - Old Man Simpson
* Mabel Van Buren - Ethel Granger Simpson
* Anita King - Helene, Countess De Champigney
* Bob Fleming - Ribiere (as Robert Fleming)
* Jack W. Johnston - Prefect Of Italian Police
* Florence Dagmar - Ivanoff Maid
* Tex Driscoll - Undetermined Role (uncredited)
* James Neill - Officer of Gendarmes (uncredited)

==Preservation status==
Like many of DeMilles early films, this film is extant, preserved at the Library of Congress.     

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 