Sheltered Daughters
{{Infobox film
| name           = Sheltered Daughters
| image          = Sheltered Daughters (1921) - 1.jpg
| caption        = Newspaper ad Edward Dillon
| producer       =
| writer         = Clara Beranger, George Bronson Howard
| narrator       =
| starring       =
| cinematography = George J. Folsey
| editing        =
| studio         = Realart Pictures
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = Silent (English intertitles)
| budget         =
}}
 Edward Dillon, starring Justine Johnstone, Riley Hatch, and Warner Baxter.  Its survival status is classified as unknown,    which suggests that it is a lost film.

==Plot==
As described in a film publication,    the story involves an underworld plot to defraud givers to a charity for French orphans. Jenny Dark (Johnstone), who greatly admires Joan dArc, supposedly the wife of a French officer, at a banquet collects $200,000 for the French orphans. Jennys father Jim (Hatch) is a plain clothes man, so the crooks do not get away. However, when Jim goes to arrest the impostor, he finds his daughter Jenny in the room with him. However, soon all is explained.

==Cast==
*Justine Johnstone as Jenny Dark
*Riley Hatch as Jim Dark, Her Father
*Warner Baxter as Pep Mullins
*Charles K. Gerrard as French Pete 
*Helen Ray as Adele
*Edna Holland as Sonia
*James Laffey as Cleghorn
*Jimmie Lapsley as Pinky Porter
*Dan E. Charles as The Ferret

==References==
 

==External links==
 
* 

 
 
 
 
 

 