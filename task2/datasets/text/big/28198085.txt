The Little Death (film)
{{Infobox film
| name           = the little Death
| image          =
| caption        = Theatrical release poster Morgan Nichols
| producer       = Eric Pulier Morgan Nichols Lonnie Zion David S. Danesh
| writer         = Laura Lee Bahr Chris Butler G. Maximilian Zarou
| music          =
| cinematography = David S. Danesh
| editing        =
| studio         = Movie Farm
| distributor    =
| released       = 2006
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| followed_by    =
}}
 thriller directed Morgan Nichols. Chris Butler, Laura Lee Bahr, Oded Gross, G. Maximilian Zarou, and tells the story of a man who attempts to find a mysterious box hidden by his father in an L.A. apartment. Atlanta Film Festival director Jake Jacobson called the little Death  "a beautiful mystery-thriller done on a shoestring." 

==Synopsis==
Seventeen years ago, Sam (Chris Butler)s father hid a mysterious box in the walls of apartment 1412, and now Sam has come to Los Angeles to claim it. But the rooms have been renumbered and Sams hunt soon leads him into the world of a mysterious young woman in what is ultimately "a sinister tale of a very unmerry Christmas."

==Cast==
*Laura Lee Bahr as Audrey / Angel
*Chris Butler as Sam
*G. Maximilian Zarou as Jr. Detective Hal Gerard
*Oded Gross as Buddy / Stacey

==Awards, honors, and festivals==
* The Little Death premiered at the closing ceremony of the Atlanta Film Festival; festival director Jake Jacobson praised the film with a comparison to Stephen Soderberghs Sex, Lies, and Videotape and Chris Butler won the festivals best actor honors.  VISIONFEST in New York City, writer/star Laura Lee Bahr won the Best Writing and Best Actress awards for the little Death.   
* Special Jury Prize for Cinematography at the Lake Forest Film Festival. 
* The film also showed at the Australian International Film Festival and the Temecula Valley International Film Festival 

==Reception==
Critic Curt Holman wrote "Director Morgan Nichols displays an eerie, deadpan style reminiscent of David Lynch or Barton Fink-era Coen Brothers." 

==References==
 

==External links==
* 
*The original   for the little Death
*official site for production company  
* 
* 
* 
* 

 
 