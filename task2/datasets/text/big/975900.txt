Ghosts of Mars
{{Infobox film
| name = John Carpenters Ghosts of Mars
| image = John Carpenters Ghosts of Mars.jpg
| image_size = 215px
| alt =
| caption = US Theatrical release poster
| director = John Carpenter
| producer = Sandy King
| writer = John Carpenter Larry Sulkis
| starring = Ice Cube Natasha Henstridge Jason Statham Pam Grier Clea DuVall Joanna Cassidy Anthrax
| cinematography = Gary B. Kibbe
| editing = Paul C. Warschilka
| studio = Storm King Productions
| distributor = Screen Gems
| released =  
| runtime = 98 minutes
| country = United States
| language = English
| budget = $28 million   
| gross = $14,010,832 
}} science fiction action horror film composed, written, and directed by John Carpenter. The film stars Ice Cube, Natasha Henstridge, Jason Statham, Pam Grier, Clea DuVall, and Joanna Cassidy. The film had received negative reviews and was a box office bomb, scoring just a 21% rating on Rotten Tomatoes  and earning $14 million at the box office, against a $28 million production budget. 

==Plot==
Set in the second half of the 22nd century, Mars has been 84% Terraforming|terraformed, allowing humans to walk on the surface without pressure suits. Martian society has become matriarchal, with women in most positions of authority. The story concerns police officer Melanie Ballard (Natasha Henstridge), second in command of a team alongside Sergeant Jericho (Jason Statham) sent to a remote mining outpost to transport prisoner Desolation Williams (Ice Cube). Arriving at the remote mining town, Ballard finds all of the people missing. She learns that they had discovered an underground doorway created by an ancient Martian civilization. When the door was opened it released disembodied spirits or "ghosts", which took possession of the miners.

The possessed miners commit horrific acts of death and destruction, along with self-mutilation. When team leader Helena Bradock (Pam Grier) is murdered, Ballard must assume command, fight off the possessed miners, escape the town and hopefully destroy the ghosts. Unfortunately, killing a possessed human merely releases the Martian spirit to possess another human. The team eventually decides to blow up a nuclear reactor to vaporize all of the ghosts. 

Ballards crew, along with survivors who gathered in the jail, are eventually wiped out by the miners. At one point, Ballard is nearly possessed, but resists when she is given a drug and discovers that the spirits are attacking them as they believe that the humans are invaders and plan to exterminate the humans on Mars (as it is presumed that the spirits are unaware of the fact that the humans believed that life on Mars died out). Only Ballard and Williams are left after Sergeant Jericho and the other officers, along with the two train  operators, are killed when they try to finish the fight. Not wanting to be blamed for the massacre, Williams handcuffs Ballard to her cot and escapes from the train. Returning home, Ballard delivers her report, which her superiors refuse to believe. While Ballard recuperates in the hospital, the released spirits, unharmed from the nuclear explosion, attack the city.

==Cast==
* Ice Cube as James Desolation Williams
* Natasha Henstridge as Lieutenant Melanie Ballard
* Jason Statham as Jericho Butler
* Clea DuVall as Bashira Kincaid
* Pam Grier as Commander Helena Braddock
* Joanna Cassidy as Dr. Arlene Whitlock
* Richard Cetrone as Big Daddy Mars
* Liam Waite as Michael Descanso
* Duane Davis as Uno
* Lobo Sebastian as Dos
* Rodney A. Grant as Tres
* Peter Jason as McSimms
* Wanda De Jesus as Akooshay
* Robert Carradine as Rodal

==Production==
The script originally started off as a potential Snake Plissken sequel. Entitled Escape from Mars, the story would have been largely much the same. However, after Escape from L.A. failed to make much money at the box office the studio did not wish to make another Plissken move. Snake Plissken was then changed to Desolation Williams and the studio also insisted that Ice Cube be given the part. 

Michelle Yeoh, Franka Potente and Famke Janssen were the first choices for the role of Melanie Ballard, but they turned it down. Courtney Love was originally cast, but she left the project after her then-boyfriends ex-wife ran over her foot in her car while she was in training for the picture. Natasha Henstridge replaced her by the suggestion of her then-boyfriend Liam Waite. Jason Statham was originally going to play Desolation Williams, but he was replaced by Ice Cube because the producers needed some star power for the part and Statham instead played the character of Jericho Butler.

Although Mars has a day/night cycle almost identical in length to Earths, most of the film is set at night. Mars is shown only once in the daytime, in a flashback when a scientist describes how she found and opened a "Pandoras Box," unleashing the alien spirits.

Production had to be shut down for a week when Henstridge fell ill due to extreme exhaustion, as she had just done two other films back-to-back before joining production at the last moment. 
 New Mexican food dye to recreate the Martian landscape.
 The Ward.

==Release==
===Critical reception===
Rotten Tomatoes gives the film a score of 21% based on 103 reviews, with the consensus stating "John Carpenters Ghosts of Mars is not one of Carpenters better movies, filled as it is with bad dialogue, bad acting, confusing flashbacks, and scenes that are more campy than scary."    
Rob Gonsalves of eFilmCritic.com suggested that the film was symbolic of Carpenter at rock bottom. According to press reviews factors contributing to the box office failure of the film included "poor set designs, hammy acting and a poorly developed script". 

===Box office===
The film opened at #9 in the North American box office in its opening weekend (8/24-26) with $3,804,452  and grossed $8,709,640 at the North American domestic box office, and $5,301,192 internationally, totaling $14,010,832 worldwide. 

==Soundtrack==
{{Infobox album  
| Name        = John Carpenters Ghosts of Mars (film soundtrack)
| Type        = soundtrack
| Longtype    = 
| Artist      = John Carpenter
| Cover       = 
| Released    =  
| Recorded    = Cherokee Studios, Hollywood Heavy metal
| Length      = 40:59
| Label       = Varèse Sarabande
| Producer    = Bruce Robb
}}
{{Album ratings
| rev1 = Filmtracks
| rev1Score =    
| rev2 = SoundtrackNet
| rev2Score =    
| noprose = yes
}}

For the films soundtrack, John Carpenter recorded a number of synthesizer pieces and assembled an all-star cast of guitarists (including thrash metal band Anthrax (American band)|Anthrax, virtuoso Steve Vai, master guitarist Buckethead, and former Guns N Roses/current Nine Inch Nails guitarist Robin Finck) to record an energetic and technically proficient heavy metal score. Reaction to the soundtrack was mixed; many critics praised the high standard of musicianship and the strong pairing of heavy metal riffs with the films action sequences, but complained about the overlong guitar solos, the drastic differences between the cues used in the film and the full tracks and the absence of any of the films ambient synth score from the soundtrack CD. 

;Track listing   
# "Ghosts of Mars" (3:42) – Steve Vai, Bucket Baker & John Carpenter Anthrax (Scott Ian, Paul Crook, Frank Bello & Charlie Benante)
# "Fight Train" (3:16) – Robin Finck, John Carpenter & Anthrax
# "Visions of Earth" (4:08) – Elliot Easton & John Carpenter
# "Slashing Void" (2:46) – Elliot Easton & John Carpenter
# "Kick Ass" (6:06) – Buckethead, John Carpenter & Anthrax
# "Power Station" (4:37) – Robin Finck, John Carpenter & Anthrax
# "Cant Let You Go" (2:18) – Stone (J.J. Garcia, Brian James & Brad Wilson), John Carpenter, Bruce Robb & Joe Robb
# "Dismemberment Blues" (2:53) – Elliot Easton, John Carpenter & Stone
# "Fightin Mad" (2:41) – Buckethead & John Carpenter
# "Pam Griers Head" (2:35) – Elliot Easton, John Carpenter & Anthrax
# "Ghost Poppin" (3:20) – Steve Vai, Robin Finck, John Carpenter & Anthrax

==See also==
 
*List of ghost films
*Mars in fiction

==References==
 

==External links==
*  
*  
*  
*  
*  
*   at John Carpenters official site
*   on YouTube

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 