Eaten Alive!
{{Infobox film name            = Mangiati vivi! image           = Eaten-Alive-1980-poster.jpg caption         =  director        = Umberto Lenzi producers       = {{plainlist|
*Mina Loy
*Luciano Martino }} writer          =  starring        = Robert Kerman Janet Agren music           =  cinematography  = Federico Zanni    editing         = Eugenio Alabiso  distributor     =  released        = 1980 runtime         =  country         = Italy   language  Italian
|budget          = 
|}}
Eaten Alive! ( ) is an 1980 Italian horror film directed by Umberto Lenzi. The film is about a young woman (Janet Agren) who is searching for her sister after her abduction by a cult in the jungles of Sri Lanka.

==Synopsis==
The film begins with a woman named Sheila who is searching for her sister, who has disappeared in the southeastern jungles of Asia (India). Sheila (Janet Agren) joins up with Mark (Robert Kerman), and they both encounter many perils while searching for Sheilas sister, Diana (Paola Senatore). Diana has joined a cult run by a man called Jonas, played by Ivan Rassimov. Jonas physically and sexually abuses his followers and local people alike. In one graphic scene, he rapes Sheila with a dildo covered in snake blood, and decapitates a native. In another, a native widow named Mowara (Me Me Lai) is ritualistically raped after her late husbands body is burned on a pyre. A group, consisting of Mowara, Mark, Sheila and Diana escapes into the jungle, where Diana and Mowara are caught by a group of cannibals, Diana raped, and then both hacked to death, while Mark and Sheila helplessly watch from the cover of the bushes. They quickly escape back to New York when helicopters sent by the authorities come looking for them. Back in the village, the rest of the cult commits ritual suicide, leaving one young female survivor for the authorities to find.

==Production== cannibal films, Last Cannibal World.   

==Release==
The film was released in 1980.  It has has been released under the alternative title Doomed to Die in the United States. 

==Reception==
The assistant professor Danny Shipka of Louisiana State University described the film as Lenzi capitalizing on public interest in cult leader Jim Jones and referring to it as a "ridiculous film". Shipka, 2011. p.136  Online film database AllMovie gave the film a one and a half star out of five rating, noting that the film includes "Stone Age cannibals, a Jim Jones-type cult, hired assassins, and gratuitous animal slaughters thrown onscreen every five minutes just to keep the viewer awake."  The review also commented on the themes of the film, stating that "Both of the leading characters are given a backstory of having exploited blacks in their Alabama cotton mill, only to lose all their ill-gotten money and -- in one case -- pay the ultimate price, as one underclass avenges another half a world away. In more talented hands, there could have been a real statement made with this film. As it turns out, the only statement most viewers are likely to come away with is "yuck."   

==Notes==
 

===References===
* Shipka, Danny. Perverse Titillation: The Exploitation Cinema of Italy, Spain and France, 1960-1980. McFarland & Company|McFarland, 2011. ISBN 0786448881.
* Kawin, Bruce F. Horror and the Horror Film. Anthem Press, 2012. ISBN 0857284495.

==See also==
* List of Italian films of 1980
* List of horror films of 1980

==External links==
*  
*   is available for free download at the Internet Archive

 

 
 
 
 
 
 
 
 


 
 