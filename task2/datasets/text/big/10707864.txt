Son of the Bride
 
{{Infobox film
| name           = Son of the Bride
| image          = Son of the the bride-1-.jpg
| caption        = Theatrical release poster
| director       = Juan José Campanella
| producer       = Adrián Suar
| writer         = Juan José Campanella Fernando Castets Eduardo Blanco Natalia Verbeke
| music          = Ángel Illaramendi
| editing        = Camilo Antolini
| cinematography = Daniel Shulman
| studio         = Patagonik Film Group Pol-ka
| distributor    = Distribution Company Sony Pictures Classics  
| released       =  
| runtime        = 123 minutes
| country        = Argentina Spanish
| gross          = $8,597,323
}}
 comedy drama Eduardo Blanco and Natalia Verbeke. 
 Best Foreign Language Film and won the Silver Condor for Best Film.

==Plot==
The film tells of Rafael Belvedere (Ricardo Darín), a 42-year-old divorced restauranteur, with a young daughter named Vicky (Gimena Nóbile) of whom he has joint custody. Rafael lives a very hectic lifestyle.

His mother Norma (Norma Aleandro) suffers from Alzheimers disease and he has not seen her in a year. Rafael sees his father Nino (Héctor Alterio) frequently but his friends rarely. Hes fielding offers to sell the restaurant he runs in Buenos Aires, but changes his mind because it was started by his mother and father and has been in the family for years.
 Eduardo Blanco), who he has not seen for twenty years, drops by the restaurant and renews their old friendship, demonstrating the fact that he has become an actor.

One day, Rafael suddenly suffers a heart attack and, as he recovers in the intensive care unit, he is forced to reevaluate his life and decide his priorities.  He now wants to sell the restaurant and move to southern Mexico and raise horses. At the same time his father wants to remarry his wife because they were never married in the Catholic Church.  Rafael is opposed to the renewal of vows because his ailing mother will not be much of a participant.

As part of his new life, he tells his much-younger girlfriend Nati that he wants some space and some freedom.  At first she is hurt and tells him that he is no Albert Einstein, Bill Gates, or even Dick Watson. (Rafael is puzzled about the identity of Dick Watson and we find out at the middle of the end credits that it refers to a character in a porno film.) Although Rafael tries to get her to understand his perspective, she ultimately breaks off their relationship. Rafael tries to deal with his struggles with this relationships and the fact that the Church is unsupportive, and ends up selling the restaurant and having Juan Carlos act the role of a priest at his parents second wedding.

The film ends after the wedding, with Rafael opening a new restaurant and apologizing to Nati; she joyously forgives him and there are strong hints that their relationship will revive.

==Cast==
* Ricardo Darín as Rafael Belvedere
* Héctor Alterio as Nino Belvedere
* Norma Aleandro as Norma Belvedere Eduardo Blanco as Juan Carlos
* Natalia Verbeke as Nati
* Gimena Nóbile as Vicki
* David Masajnik as Nacho
* Claudia Fontán as Sandra
* Atilio Pozzobon as Francesco
* Salo Pasik as Daniel
* Humberto Serrano as Padre Mario
* Fabián Arenillas as Sciacalli
* Mónica Cabrera as Carmen
* Giorgio Bellora as Marchiolli
* Mónica Virgilito as Enfermera hospital
* Adrián Suar as Dodi
* Juan José Campanella as Médico
* Alfredo Alcón as Himself

== Critics Reviews ==
Son of the Bride garnered mostly positive reviews from film critics. On review aggregate website  , which assigns a weighted mean rating out of 0–100 reviews from film critics, the film has a rating score of 68 based on 29 reviews, classified as a generally favorably reviewed film. 

Variety (magazine)|Variety magazine film critic Eddie Cockrell lauded the film in his review and wrote, "A 42-year-old Buenos Aires workaholic discovers family means more than his restaurant business in superb contempo comedy The Son of the Bride. Unflaggingly genial and universally funny pic is already generating favorable word of mouth on twin strengths of Montreal competish jurys Special Grand Prix and kudos as best Latin American film of the fest. Wedding bells will ring in cinema cathedrals worldwide, with whirlwind honeymoons and long and happy lives on the tube and in homevid." 

Critic Jeff Stark liked the comedy and wrote, "Argentinean director Juan José Campanellas Son of the Bride is about a lot of things, but at its core its about a mans midlife crisis. A witty script, a fleet camera and a pitch-perfect cast keep the movie from being dragged under by the selfishness of its central character...By the end, this crisply agreeable picture has made several points. One, I suppose, is that there are different levels of reconciliation, in troubled families and beleaguered businesses as well as in nations beset by permanent crisis. A more important one, perhaps, is that theres a difference between dropping out and running away." 
 New York Times film critic Stephen Holden gave the comedy drama a mixed review, writing, "Although there is much to like in this psychologically canny film and in Mr. Darins warm-hearted central performance, the movie is also rambling and digressive. It is so determined to delve into every cranny of Rafaels world, from his minor business woes to his rekindled friendship with the zany childhood friend with whom he used to play games of Zorro, that it has the feel of an unedited personal journal." 

==Distribution==
The film opened wide in Argentina on August 16, 2001.  Later it was presented at the Montreal World Film Festival|Montréal Film Festival on August 29, 2001.

The picture was screened at various film festivals, including: the Valladolid International Film Festival, Spain; the Havana Film Festival, Cuba; the Muestra de Cine Mexicano en Guadalajara, Mexico; the Latin America Film Festival, Poland; the Copenhagen International Film Festival, Denmark; and others.

It opened in the United States on a limited basis on March 22, 2002, and it was later optioned for a remake with an American setting and characters.

==Awards==
Wins
* Havana Film Festival:  Audience Award; OCIC Award; both for Juan José Campanella; 2001.
* Montreal World Film Festival|Montréal World Film Festival: Best Latin-American Feature Film; Special Grand Prize of the Jury; both for Juan José Campanella; 2001.
* Valladolid International Film Festival, Valladolid, Spain: Silver Spike, Juan José Campanella; 2001.
* Viña del Mar Film Festival, Viña del Mar, Chile: Paoa, Best Supporting Actress, Norma Aleandro; 2001.
* Argentine Film Critics Association Awards: Silver Condor; Best Actor, Ricardo Darín; Best Director, Juan José Campanella; Best Editing, Camilo Antolini; Best Film; Best New Actress, Claudia Fontán; Best Original Screenplay, Juan José Campanella and Fernando Castets; Best Supporting Actor, Eduardo Blanco; Best Supporting Actress, Norma Aleandro; 2002.
* Cartagena Film Festival: OCIC Award, Juan José Campanella; 2002.
* Gramado Film Festival: Audience Award Latin Film Competition, Juan José Campanella; Golden Kikito Latin Film Competition - Best Actress, Norma Aleandro; Kikito Critics Prize Latin Film Competition, Juan José Campanella; 2002.
* Lima Latin American Film Festival: Elcine First Prize, Juan José Campanella; 2002.
* Cinema Writers Circle Awards, Spain: CEC Award, Best Supporting Actor, Héctor Alterio; 2002.
* Ondas Awards, Barcelona, Spain: Film Award,  	 Best Actress, Natalia Verbeke, (also for El Otro lado de la cama); 2002.
* Oslo Films from the South Festival: Audience Award, Juan José Campanella; 2002.
* Sant Jordi Awards, Barcelona, Spain: Sant Jordi, Best Foreign Actor, Ricardo Darín (also for La Fuga and Nueve reinas; Best Foreign Film, Juan José Campanella; 2002.
* São Paulo International Film Festival, São Paulo, Brazil: Audience Award, Best Feature - Foreign Language, Juan José Campanella; 2002.

Nominations
* Montréal World Film Festival: Grand Prix des Amériques, Juan José Campanella; 2001.
* Valladolid International Film Festival: Golden Spike, Juan José Campanella; 2001.
*  , Argentina; 2002.
* Argentine Film Critics Association Awards: Silver Condor, Best Art Direction, Mercedes Alfonsín; Best Cinematography, Daniel Shulman; Best Music, Ángel Illarramendi; Best Supporting Actor, Héctor Alterio; 2002.
* Cartagena Film Festival: Golden India Catalina, Best Film, Juan José Campanella; 2002.
* Cinema Writers Circle Awards, Spain: CEC Award, Best Actor, Ricardo Darín; Best Supporting Actress, Norma Aleandro; 2002.
* Gramado Film Festival: Golden Kikito, Latin Film Competition - Best Film, Juan José Campanella; 2002.

==References==
 

==External links==
*  
*   at the cinenacional.com  
*   review at Cineismo by Guillermo Ravaschino  
*   review at Cinestros by Juan Antonio Bermúdez  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 