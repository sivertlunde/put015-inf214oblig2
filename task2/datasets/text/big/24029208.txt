The W Plan
 
{{Infobox film
| name           = The W Plan
| image          = TheWPlanPoster.jpg
| alt            = 
| caption        = Theatrical Poster
| film name      = 
| director       = Victor Saville  Marjorie Gaffney   
| producer       = Victor Saville
| writer         = 
| screenplay     = Victor Saville Miles Malleson Frank Launder
| story          = 
| based on       =     
| starring       = Brian Aherne Madeleine Carroll Gibb McLaughlin Gordon Harker
| narrator       = 
| music          = John Reynders 
| cinematography = René Guissart (director)|René Guissart  Freddie Young 
| editing        = Maclean Rogers 
| studio         = British International Pictures  Elstree Studios  RKO Radio Pictures (USA)   
| released       =   }}
| runtime        = 87-105 minutes 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 British spy film produced and directed by Victor Saville, from a screenplay which he also co-wrote with Miles Malleson and Frank Launder, based on the novel of the same name by Graham Seton.  It starred Brian Aherne, Madeleine Carroll, Gibb McLaughlin, and Gordon Harker.  When this film was released in the United States, Aherne was appearing in The Barretts of Wimpole Street on Broadway, and receiving praise for his performance as Robert Browning.  The film was a critical success, but did not do well at the box office.

==Plot==
Colonel Duncan Grant (Brian Aherne) is a British officer during World War I.  When the British high command get wind of a German plan, titled The W Plan, from the lips of a dying German officer, Major Ulrich Muller (George Merritt), they send Grant behind enemy lines to learn the details.  After successfully being dropped by airplane near the German town of Essen, where he makes his way to home of the dead German who was responsible for the plan.  Grant is chosen because he speaks fluent German, having spent a significant amount of time in Germany prior to outbreak of hostilities.  While in Essen, he runs into an old girlfriend, Rose Hartmann (Madeleine Carroll).  When he and Rose go to a nearby café, he is approached by German officers and asked for his papers.  While he has the documents taken from Muller, the Germans become suspicious, and Grant has to make a quick getaway.  Unfortunately, the plane he is supposed to meet with to make his escape is shot down, after which Grant is arrested for desertion.

When he is about to be shot, he is instead sent to the very project he had been sent to Germany to learn about, The W Plan.  It consists of a very elaborate series of underground works which are being dug beneath the British controlled territory, in order to collapse their lines. Grant succeeds in destroying a vital portion of the German underpinnings, and makes his escape back to British territory.  The film ends with the allusion that he will meet up with Rose in Switzerland in the coming days.

==Cast==
* Brian Aherne as Colonel Duncan Grant George Merritt as Major Ulrich Muller
* C.M. Hallard as Commander in Chief
* Frederick Lloyd as Colonel Jervois
* B. Gregory as Flight Commander Mayne
* Mary Jerrold as Frau Muller
* Madeleine Carroll as Rosa Hartmann
* Clifford Heatherley as Café proprietor
* Austin Trevor as Captain of military police
* Norah Howard as Lady of the town Cameron Carr as Private Otto Goddern
* Milton Rosmer as President of the court martial
* Alfred Drayton as Prosecuting counsel
* Charles Paton as Defending counsel
* Wilhelm Koenig as Prison commandant
* Gordon Harker as Private Waller
* Gibb McLaughlin as Private Ginger McTavish
 BFI database) 

==Reception==
While it did not do well at the US box office,  it did receive critical acclaim.  Mordaunt Hall of The New York Times gave it a very positive review, applauding the acting of Aherne and Madeleine Carroll, and calling the picture, "... an exciting and splendidly staged English espionage melodrama," and the "... most satisfactory production sent over here from the Elstree Studios." 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 