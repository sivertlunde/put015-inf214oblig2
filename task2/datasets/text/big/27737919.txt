Love Song (2000 film)
{{Infobox Film
| name           =  Love Song
| image          = 
| image_size     = 
| caption        = 
| director       =   Julie Dash
| producer       =   Kimberly Olgetree Claudio Castravelli
| writer         =   Josslyn Luckett
| narrator       = Monica Christian Kane Essence Atkins
| music          =   Frank Fitzpatrick   
| cinematography =   David Claessen    
| editing        =   Pamela Malouf   
| studio         =   
| distributor    =   MTV Films
| released       =   December 1, 2000
| runtime        =   90 min.
| country        =   United States English
| budget         = 
| gross          = 
}}

Love Song is a 2000 film produced for television by MTV and directed by Julie Dash. The film stars Monica (singer)|Monica, Christian Kane, and Essence Atkins. It was released on December 1, 2000.

==Plot==
Camille Livingston is the reserved, sheltered only child and daughter of prosperous African American parents in New Orleans. She attends school, works with under privileged inner city children and lives with her two roommates.  Camille has been dating Calvin, who looks up to Camilles father and appears to be following him into medicine.  It is also understood that Camille will follow Calvin and work in some type of medical field.  Her life is considered by her and people around her to be "normal" and what is expected.  

Then on her 21st birthday she is attended to at a gas station by Billy Ryan Gallo, a mechanic during the day who turns out to be a blues musician at night.  When Camille shows up at Thelmas Bar later that evening, Billy formally himself and then serenades her from the stage, much to her surprise and the amusement of her roommates.  

Camille runs into Billy again when she is out with Calvin and Billy is playing at the restaurant/casino.  Impressed with Billys style, Calvin asks if he can buy Billy a drink and Billy joins them at their table and does not let on that he already has met Camille.  After a short conversation, Billy invites both to Thelmas Bar the following evening but Calvin declines saying he will be out of town.

The next day, Camille goes alone to Thelmas and watches again as Billy takes the stage and appears to be singing only to her.  When his set is complete, Billy asks if Camille will go out and about New Orleans, a town where Camille has lived for over ten years but has never experienced.  The night sees Camille and Billy visiting clubs, riding around, laughing and dancing and being very free and comfortable with each other.  They then go to a club where she is handed a mic and impromptuly sings with a powerful voice that has Billy awed.

As the chemistry between the two heats up, Camille retreats back to her safe world and Billy, respecting her decision, says if they cannot be more than friends, then would she consider singing in his new band.  Camille agrees and two begin a musical friendship.  They decide to try their act out at Camilles colleges open mic night, Billy playing guitar and Camille singing.  All during the song neither Billy nor the audience take their eyes off of Camille and when the song is complete receive a standing ovation.  Afterwards, energised by their performance, Camille stares at Billy differently and unexpectedly kisses him.  Billy then kisses her back and asks her if she knows who she wants to be with.  Camille confidently tells Billy she wants to be with him and the two kiss again.

With this new change, Camille realises that she wants to continue to work with the inner city children and not head off to some college and a career in medicine.  Newly inspired, Camille goes to tell her parents and is surprised to find Calvin and his parents at their house and Calvin asking her to marry him.  Overwhelmed by the pressure and her parents happiness, Camille agrees to marry Calvin and options out of her desired career choice.  She then meets with Billy and displays the engagement ring.  Furious with Camille for not standing up for what she wants and trying to make everyone happy but herself, Billy ends their friendship.

Two weeks later at the engagement party, Camilles roommates observe Camille and find that their friend isnt herself and the whole event just "doesnt feel right."  They take Camille aside and tell her that at any point she wants to leave, they will help drive her away.  

The party is an upscale social event attended by what Calvin calls "very important people."  Camille decides to serenade Calvin with a love song and everyone in the audience is amazed by her talent except for Calvin and her father, who appear to be engrossed in their own conversation.  And while she sings, Camilles thoughts turn to Billy as she watches Calvin ignore her performance.  When the song ends she announces to everyone that she is not in love with Calvin but in love with a man who "really sees her."  She calls off the engagement and drives away with her roommates.

Camille goes back to her place, changes and drives to find Billy at the gas station.  He is standoffish towards her and asks her to leave but she stays to show him her bare finger finger.  Billy softens and understands the enormity of what Camille did and he again asks her if this is for real.  She smiles and says yes and the two kiss and hug and finally appear as a couple.

The movie ends with Camille and Billy Ryan at Thelmas Bar, this time on stage together for the first time, in front of their friends and both of their mothers, singing a Love Song.

==Cast== Monica
*Billy Ryan Gallo: Christian Kane
*Toni: Essence Atkins
*Evie Livingston: Vanessa Bell Calloway
*Malik/Joey Anderson: Teck Holmes
*Renee: Rachel True Tyrese
*House Chilli
*Alfred Livingston: Peter Francis James
*Calvin Dumas: Rainbow Sun Francks

==Production==
The movie was filmed in Montréal, Québec, Canada. 

==References==
 

==External links==
*   at Internet Movie Database
*  at All Movie

 
 
 
 


 