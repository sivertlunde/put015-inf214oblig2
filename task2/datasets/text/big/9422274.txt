Unlawful Entry (film)
{{Infobox film
| name = Unlawful Entry
| image = Unlawful_Entry.jpg
| caption = Theatrical release poster
| director = Jonathan Kaplan Charles Gordon Sulla Hamer Gene Levy
| writer = George Putnam John Katchmer Lewis Colick (Screenplay)
| starring = Kurt Russell Ray Liotta Madeleine Stowe
| music = James Horner
| cinematography = Jamie Anderson
| editing = Curtiss Clayton
| studio = Largo Entertainment
| distributor = 20th Century Fox
| released =  
| runtime = 112 minutes
| country = United States
| language = English
| budget = $23 million
| gross = $57,138,719 (USA)   
}}
Unlawful Entry is a 1992 American thriller film directed by Jonathan Kaplan starring Kurt Russell, Ray Liotta and Madeleine Stowe.   
 fixation on the wife, leading to chilling consequences. Ray Liotta was nominated for an MTV Movie Award in 1993 for his portrayal of the psychopathic cop. The film was remade in Bollywood as Fareb (1996 film) | Fareb, starring Faraaz Khan, Suman Ranganathan, Ashok Lath, Vishwajeet Pradhan and Milind Gunaji.

== Plot ==
Michael and Karen Carr (Kurt Russell and Madeleine Stowe) are a couple living in an upscale part of Los Angeles, and their peace of mind is upset by an intruder coming in through their skylight one night. The intruder doesnt take anything except Karen, briefly, as a hostage, before dumping her in the swimming pool and making his escape. 

The Carrs call in the police, one of whom, Pete Davis (Ray Liotta), takes an interest in the couples case. He cuts through department red tape and expedites speedy installation of a security system in the Carrs house.

When Michael expresses an interest in getting revenge on the intruder, Pete invites him on a "ride-along" with his partner, Roy Cole (Roger E. Mosley). After dropping Cole off, Pete takes Michael out to arrest the man who broke into the Carrs house, offering Michael a chance to take some revenge using Petes nightstick. Michael declines, but Pete administers a vicious beating to the intruder, leaving Michael deeply suspicious of Petes mental stability. He suggests that Pete get some professional help and, especially, stay far away from him and Karen in the future.

Pete takes neither suggestion. Instead, he begins to stalk the couple, particularly Karen, with whom hes obsessed. Pete even appears in the couples bedroom one night while they are having sex, just to "check that everythings okay."

When Michael files a complaint against Petes unwanted attentions, Pete uses his police connections to destroy Michaels business reputation. Encountering bemused apathy from Petes superiors in the LAPD, Michael turns to Cole (who orders his partner to cease his obsessing, see a shrink or face suspension). Pete then murders Cole, blaming it on a known criminal.

Pete then frames Michael on drug charges by planting a supply of cocaine in the Carrs house, leaving the way clear for him to move in on Karen. Putting his attorneys finances on the line, Michael gets out on bail and takes matters into his own hands. 
 rejects a tease for leading him on and kissing him) goes berserk and tries to rape her. Michael returns home which leads to a confrontation between the two men which ends in Michael shooting Pete dead (with his own side arm) in self-defense.

==Cast==
* Kurt Russell as Michael Carr
* Ray Liotta as Officer Pete Davis
* Madeleine Stowe as Karen Carr
* Roger E. Mosley as Officer Roy Cole
* Ken Lerner as Roger Graham
* Deborah Offner as Penny, Karens Friend
* Carmen Argenziano as Jerome Lurie
* Andy Romano as Capt. Russell Hayes
* Johnny Ray McGhee as Ernie Pike
* Dino Anello as Leon, the Dealer
* Sonny Carl Davis as Neighbor Jack
* Harry Northup as McMurtry, Desk Sergeant
* Sherrie Rose as Girl in car
* Alicia Ramirez as Taco Stand Worker
* Ruby Salazar as Rosa, the Hooker
* Spider Madison as Goatee
* Myim Rose as Layla
* T.J. McInturff as Laylas Kid
* Tiny The Cat as Merv.
* Tony Longo as Big Anglo
* Djimon Hounsou as Prisoner on Bench
* Dick Miller as Impound Clerk

== Reception ==
Unlawful Entry received positive reviews from critics, as it holds a 75% rating on Rotten Tomatoes based on 32 reviews.

The film was released in the U.S. on June 26, 1992, opening at #2 in 1,511 theaters, an average of $6,662 per theater. Grossing $10,067,609 in the opening weekend, it went on to gross $57,138,719 in the domestic market. 

==See also==
* List of films featuring home invasions
* List of American films of 1992

== References ==
 

== External links ==
*  
*  
*  
*   

 

 
   
  
    
 
 
 
 
  
 
 