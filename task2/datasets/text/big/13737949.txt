The Counterfeiters (2007 film)
 
{{Infobox film
| name           = The Counterfeiters
| image          = Counterfeiters ver3.jpg
| alt            = 
| caption        = Danish-language poster
| director       = Stefan Ruzowitzky
| producer       = Josef Aichholzer Nina Bohlmann Babette Schröder
| writer         = Stefan Ruzowitzky
| based on       =  
| starring       = Karl Markovics August Diehl Devid Striesow
| music          = Marius Ruhland
| cinematography = Benedict Neuenfels
| editing        = Britta Nahler
| studio         = Aichholzer Film Magnolia Filmproduktion Studio Babelsberg
| distributor    = Filmladen   Universum Film  
| released       =  
| runtime        = 98 minutes
| country        = Austria Germany
| language       = German Russian English Hebrew
| budget         = $6,250,000 {{cite web|title=Die Fälscher
|url=http://www.the-numbers.com/movies/2008/CNTRF.php}}  
| gross          = $20,199,663  
}} forged Bank pound notes. The film centres on a Jewish counterfeiter, Salomon Sally Sorowitsch, who is coerced into assisting the operation at the Sachsenhausen concentration camp.
 Best Foreign Language Film Oscar at the 80th Academy Awards.

==Plot== tattooed numerals on his arm, revealing him as a survivor of the Nazi concentration camps.
 Salomon Sorowitsch, forger of labour camp, Mauthausen concentration camp near Linz. In an effort to secure himself protection and meagre comforts at the camp, he turns his forging skills to portraiture, attracting the attention of the guards, who commission him to paint them and their families in exchange for extra food rations.

Sorowitschs talents bring him to wider attention, and he is transferred out of the concentration camp. Brought in front of the police officer who arrested him in Berlin, he finds himself put together with other prisoners with artistic or printing talents, and begins working in a special section of the Sachsenhausen concentration camp devoted to forgery. The counterfeiters are kept in relatively humane conditions, with comfortable bunks, a washroom and adequate food, although they are subjected to brutality and insults at the hands of the prison guards. His fellow prisoners have a range of backgrounds from Jewish bank managers to political agitators, and while some are content to work for the Nazis to avoid the extermination camps, others see their efforts as supporting the German war effort.

At first, self-preservation appears to guide Sorowitsch, but his motives for forging for the Nazis are complicated by his growing concern for his fellow prisoners, his awareness of their role in the wider war against the Nazis, and his professional pride in counterfeiting the US dollar, a currency he was previously unable to forge.

Sorowitsch juggles the demands for progress of the Nazi authorities, his co-counterfeiters determination to sabotage the operation, and his loyalties to his fellow prisoners. The prisoners successfully counterfeit the British pound but intentionally delay the forgery of the US dollar. Gradually, slivers of evidence that the war has turned decidedly against the Nazis arrive. One day the camp guards suddenly announce that the printing machines are to be dismantled and shipped away, which leads the counterfeiters to fear that they will finally be killed. Before anything happens to them, the German guards flee the camp in advance of the Red Army. Starving prisoners from other parts of the camp, armed with confiscated weapons, take over and break into the compound where the counterfeiters had been held in relative luxury. Until the insurrectionists see the well-fed printers prison tattoos, they believe them to be SS officers and threaten to shoot them. The counterfeiters then must account for their forging actions to the half-dead prisoners.

The film then returns to Monte Carlo where Sorowitsch, apparently disgusted by the life he is now leading on the currency that he forged for the Nazis, intentionally gambles it all away. Sitting alone afterward on the beach, he is joined by the French woman, concerned after his seemingly disastrous losses at the table. Dancing slowly together on the beach, she consoles him regarding all the money he has lost, to which he replies, laughing, that we can always make more.

==Cast==
 ,   and   at the Premier of   at the  ]]
* Karl Markovics as Sorowitsch (Salomon Smolianoff)
* August Diehl as Burger (Adolf Burger)
* Devid Striesow as Sturmbannführer Herzog (Bernhard Krüger)
* Veit Stübner as Atze
* Sebastian Urzendowsky as Karloff/Kolya
* August Zirner as Dr Klinger
* Martin Brambach as Hauptscharführer Holst
* Andreas Schmidt as Zilinski
* Tilo Prückner as Hahn
* Lenn Kudrjawizki as Loszek

==Production== tangos recorded Argentine harmonica Hugo Díaz, and opera recordings from the 1930s and 1940s.

==Reception==

===Critical response===
The film appeared on some critics top ten lists of the best films of 2008. Josh Rosenblatt of The Austin Chronicle named it the 4th best film of 2008,  and Ella Taylor of LA Weekly named it the 8th best film of 2008.     

===Accolades=== Academy Awards 2007 (24 February 2008) Best Foreign Language Film 

* Berlin International Film Festival, 2007 Golden Bear award: Stefan Ruzowitzky
 German Film Awards, 2007
** Won Best Performance by an Actor in a Supporting Role: Devid Striesow
** Nominated for Best Cinematography: Benedict Neuenfels
** Nominated for Best Costume Design: Nicole Fischnaller
** Nominated for Best Performance by an Actor in a Leading Role: Karl Markovics
** Nominated for Best Production Design: Isidor Wimmer
** Nominated for Best Screenplay: Stefan Ruzowitzky
** Nominated for Outstanding Feature Film: Nina Bohlmann, Babette Schröder, Josef Aichholzer

==Home media==
The Counterfeiters was released on DVD and Blu-ray Disc in the United Kingdom by Metrodome Distribution on 17 March 2008, and in the USA by Sony Pictures Home Entertainment on 5 August 2008.

==See also==
* Private Schulz, a BBC production covering the same events.

==References==
 

==External links==
 
*   (US)
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 