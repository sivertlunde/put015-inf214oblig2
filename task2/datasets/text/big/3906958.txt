Good Luck (2000 film)
{{Infobox film
| name = Good Luck
| image = "good_luck,_2000".jpg caption = Movie poster
| writer = Sukanya Vivek Vivek Vaiyapuri Ilavarasi Manoj Bhatnaghar Manoj Bhatnaghar
| Editing = B. S. Vasu   Saleem
| choreographer = K. S. Shiva
| banner = Sameera Films
| art director = S. A. C Ramki
| lyrics = Vairamuthu
| Choreography = Raja Sundaram   L. L. Cool Jayanth   Ravidev
| released = February 4, 2000
| runtime = Tamil
| music = Manoj Bhatnaghar 
}}
 Tamil film. Manoj Bhatnaghar.

==Plot==
Surya (Prashanth) is a carefree youth living in Malaysia with his brother Chandramohan (Raghuvaran) and sister-in-law Devi (Sukanya). Chandramohan and Devi are childless because of a problem with Chandramohan.  Surya falls in love with dancer Priya (Riya Sen) and manages to steal her heart too. Just when things seem happy, a bombshell is dropped by a Sister Mary who tells Surya that he is the father of a small girl Pooja. Pooja must undergo an operation to save her life but insists on seeing her dad before agreeing to the operation. Surya agrees to pose as her dad and is forced to do so on a more permanent basis even after the operation is done. He is driven out of the house and loses Priya, who agrees to wed an earlier suitor, Shyam (Sanjay Asrani). Person who happens to be the brother of Poojas mother wants to kill Surya for cheating his sister but ends up being arrested by police. Second half shifts to Cruise ship, where Chandramohan finds out that Pooja is actually his daughter. The film ends with Surya and Priya reuniting.

==Cast==
*Prashanth as Suryakumar
*Riya Sen as Priya
*Raghuvaran as Chandramohan Sukanya as Devi Vivek
*Vaiyapuri
*Ilavarasi as Sangeetha Rajan
*Sanjay Asrani as Shyam

==Production==
The film was initially set to feature Mayuri Kango as the lead heroine, but she was later replaced by Riya Sen.  The entire film was shot in Thailand and Malaysia, while significant portions were shot on a cruise ship sponsored by Star Cruises.

==Soundtrack==

{{Infobox Album |  
| Name        = Good Luck
| Type        = soundtrack Manoj Bhatnaghar
| Cover       = 
| Released    = 2000
| Recorded    = 2000 Feature film soundtrack |
| Length      = 
| Label       = Sa Re Ga Ma  Manoj Bhatnaghar
| Reviews     = 
}}

The film score and the soundtrack were composed by director Manoj Bhatnaagar himself. The soundtrack, released in 2000, features 6 tracks . All the songs were Massive Hits.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Sujatha || 
|- 2 || Chitra || 
|- 3 || Chitra || 
|- 4 || Chorus || 
|- 5 || "Idhayam Thudikkirathe" || K.Prabhakaran, K. S. Chithra|Chitra, Baby Deepika || 
|- 6 || Sujatha || 
|}

==References==
 

 
 
 
 
 