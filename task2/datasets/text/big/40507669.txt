The Price of a Good Time
{{infobox film
| title          = The Price of a Good Time
| image          = File:The Price of a Good Time.jpg
| imagesize      =190 px
| caption        =Ad for film
| director       = Lois Weber Phillips Smalley Arthur Forde (asst. director)
| producer       = Lois Weber
| writer         = Marion Orth (story) Ethel Weber (continuity) Lois Weber
| starring       = Mildred Harris
| music          = 
| cinematography = Allen Siegler and/or Duke Hayward
| editing        =
| distributor    = Universal Film Manufacturing Company
| released       = November 4, 1917
| runtime        = 5-6 reels
| country        = USA
| language       = Silent film(English intertitles)
}}
The Price of a Good Time is a 1917 silent film drama directed by Lois Weber and Phillips Smalley and starring teen Mildred Harris.  
 
File:The Price of a Good Time 2.jpg
File:The Price of a Good Time 3.jpg
 

==Cast==
*Mildred Harris - Linnie
*Anne Schaefer - Her Mother
*Helene Rosson - Molly
*Kenneth Harlan - Preston Winfield Alfred Allen - His Father
*Adele Farrington - His Mother
*Gertrude Astor - Miss Schyler

==References==
 

==External links==
 
* 
* 
* (Univ. of Washington, Sayre collection)

 
 
 
 
 


 