In the Kingdom of the Goldhorn
  Slovene feature Skala and was 107 minutes long. Only about two thirds of the original   film, i.e. the shortened 76-minute version, have been preserved until today.

The screenplay was written by Juš Kozak. The story tells about a trip by a student, a railway worker and a peasant to the Julian Alps, the people they meet on their way, and their ascent to Triglav|Mt. Triglav. The film features the mountaineers   (Roban) and   (Klemen).

In the Kingdom of the Goldhorn was released on 29 August 1931 in Grand Hotel Union in Ljubljana. It was well received by the public, but criticised in reviews as amateurish. The last time the film was shown to the wider public was on its 80th anniversary in Grand Hotel Union in August 2011. A commemorative stamp with a motif from the film was published by the Post of Slovenia on this occasion. 

==See also==
*The Slopes of Triglav, the second Slovene feature film (1932)
*On Our Own Land, the first Slovene sound feature film (1948)

==References==
 

 
 
 
 
 