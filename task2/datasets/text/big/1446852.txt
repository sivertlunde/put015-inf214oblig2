More (1998 film)
 
{{Infobox film
| name           = More
| image          = More film title.jpg
| image_size     =
| caption        = Mark Osborne
| producer       = Steve Kalafer
| writer         = Mark Osborne
| narrator       =
| starring       =
| music          = New Order
| cinematography = Mark Osborne
| editing        = Mark Osborne
| studio         = Bad Clams Productions Swell Productions
| distributor    = Flemington Pictures Film Movement
| released       =  
| runtime        = 6 min
| country        = United States
| language       =
| budget         =
| gross          =
}} Mark Osborne Best Animated Short Film in 1998.

==Plot==

More tells the story of an inventor who lives in a drab, colorless world. Day by day, he toils away in a harsh, dull, and dehumanizing job, his only savior being the memories of the bliss of childhood. But at night, he works secretly on an invention that could help him relive those memories and spread their joy to everyone in his despair-filled life.

When he finishes the invention, it changes the way people look at the world. His success changes him, however, because he loses an important part of himself.

==Production and publication== Mark Osborne, and created by a team that included, among others, Keith and Shannon Lowry, Rick Orner, Nick Peterson, and David Candelaria. Although it was only a 6-minute short film|short, it was, as Osborne put it, an "absolutely massive undertaking"—as it was the first short to be shot using the IMAX format. In addition, it was filmed using stop motion, a much more time-consuming method than live-action filming techniques.

  Academy Award nomination—commercial options proved limited once the initial hype died down.
 Wholphin Magazine.

More was featured on the Sci Fi Channel (United States)|Sci-Fi Channel series Exposure (U.S. TV series)|Exposure.

More has gained reputation for being one of the greatest short movies ever created , and was for a long time ranked as the best short movie at  , where it was ranked 11th (as of May 2012).  

==Soundtrack==
The song featured as background music is titled "Elegia". It was recorded by the band New Order on the 1985 album Low-Life.
 Kenna which was aired on MTV2 and the Toonami block on Cartoon Network in 2001.

Composer and remixer Justin Lassen created two very rare and intensely orchestral remixes and reversions of "Elegia" for the DVD.

==Awards==
More was awarded the following honors:
*Sundance Film Festival - Special Jury Prize for Short Films
*South by Southwest - Best Animated Short
*Nominated - Academy Award for Best Animated Short Film
*ResFest – Audience Award for Best Film, Grand Audience Prize for Best Film
*Aspen Shorts Fest - Special Jury prize
*World Fest Houston - Gold / Special Jury Prize for Shorts
*USA Film Festival - Dallas - Grand Jury Prize for Shorts
*Toronto International Short Film Fest - Best Animated Short, Best Short Overall
*Stony Brook Film Fest - Best Short Film
*Message to Man International Film Festival - Russia – Best International Debut Film
*PhilaFilm - Philadelphia – Best Animated Short
*Nominated - Annie Award for Best Animated Short Subject
*St. Louis International Film Festival - Best Short Film
*Uppsala International Short Film Festival/Sweden - Audience Award for Best Film
*San Francisco Indie Fest – Audience Award

==See also==
*List of stop-motion films

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 