The Life (2002 film)
{{Infobox Film
| name           = The Life
| image          = 
| image_size     = 
| caption        = 
| director       = Rolando Hudson
| producer       = 
| writer         = Julie Atwell Rolando Hudson
| narrator       = 
| starring       = Mary Alice   Nicole Leach   Keith Tisdell   Alton White   Kimberly Jajuan   Kolbe Powell   Chantale Hosein   Tarantino Smith
| music          = 
| cinematography = Michael Garafalo
| editing        = 
| distributor    =  2002
| runtime        = 12 min.
| country        = U.S.A. English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
The Life is a 2002 American film directed by Rolando Hudson and written by Julie Atwell and Rolando Hudson.

==Plot==

Emiline Crane is a widow in New York City. Shes cheerful with her neighbors, but she misses her husband and prizes a locket she wears that contains his picture. A sudden noise startles her, she falls and dies. At Heavens door, a Mr. Peters tells her theres been a mistake. She can sit in the atrium for 22 years or she can return to earth for 22 years to inhabit the body of a just-deceased young woman. Emiline thinks a moment and opts for earth, and there she is, inside the muscular body of Jasmine, another NYC resident whose apartment is full of high-tech gadgets. How Emiline sets about reconciling her past and her possibilities makes up the rest of the film.

==External links==
* 

 
 
 
 
 


 