Thaliritta Kinakkal
{{Infobox film 
| name           = Thaliritta Kinakkal
| image          =
| caption        =
| director       = P Gopikumar
| producer       =
| writer         =
| screenplay     =
| starring       = Prathap Pothen Sukumaran Kuthiravattam Pappu Madhumalini
| music          = Jithin Shyam
| cinematography =
| editing        = G Murali
| studio         = Chithra Geethi
| distributor    = Chithra Geethi
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film,  directed by P Gopikumar. The film stars Prathap Pothen, Sukumaran, Kuthiravattam Pappu and Madhumalini in lead roles. The film had musical score by Jithin Shyam.   

==Cast==
*Prathap Pothen
*Sukumaran
*Kuthiravattam Pappu
*Madhumalini
*Thanuja

==Soundtrack==
The music was composed by Jithin Shyam and lyrics was written by Jamal Kochangadi, Aayish Kamal and P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aa churam ee churam || K. J. Yesudas, Vani Jairam, Chorus || Jamal Kochangadi || 
|-
| 2 || En Mooka vishadham || S Janaki || Jamal Kochangadi || 
|-
| 3 || Saaz-E-Dil Tod do || K. J. Yesudas || Aayish Kamal || 
|-
| 4 || Shaarike varoo nee || K. J. Yesudas || P. Bhaskaran || 
|-
| 5 || Shabaab Leke || Mohammed Rafi || Aayish Kamal || 
|-
| 6 || Vaiki vanna vasanthame || K. J. Yesudas || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 