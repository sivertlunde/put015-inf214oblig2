Lahore (film)
 
{{Infobox film
| name           = Lahora
| image          = Lahore Cheering Poster.jpg
| alt            =  
| caption        = 
| director       = Sanjay Puran Singh Chauhan
| producer       = Vivek Khatkar
| writer         = Sanjay Puran Singh Chauhan & Piyush mishra
| starring       =  
| music          =  
| cinematography =  
| editing        =  
| studio         = Sai Om Films Private Limited
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 126 minutes
| country        = India
| language       = Hindi
| budget         =   250,000,000
| gross          = 
}}
Lahore ( ) is a 2010 Bollywood sports film that was released on March 19, 2010. It was directed by Sanjay Puran Singh Chauhan and produced by Vivek Khatkar, with Sai Om Films Pvt. Ltd.’s, and distributed by  Warner Bros. Pictures. The movie is loosely based on American martial arts movie Best of the Best.

The film stars debutants Aanaahad and Shraddha Das in lead roles and has veteran actors like Farooq Shaikh, Nafisa Ali, Nirmal Pandey, Sushant Singh, Sabyasachi Chakrabarty, Saurabh Shukla, Ashish Vidyarthi, Kelly Dorji, Mukesh Rishi, Jeeva (actor)|Jeeva, Shraddha Nigam in key roles.

==Plot==
An Indian kickboxer defeats a Pakistani kickboxer in an international tournament. But the event ends in a tragedy due to the Pakistani players intemperance. Will the peace process, initiated by the two countries lose its momentum, especially when the Indian heros brother is determined to avenge his family and country during a second international bout that is being held in Lahore? 

==Cast==
* Aanaahad as Veerender Singh
* Shraddha Das as Ida
* Farooq Shaikh as S K Rao
* Nafisa Ali as Amma
* Sushant Singh as Dheeru
* Shraddha Nigam as Neela Chaudhary
* Mukesh Rishi as Noor Mohammmad
* Sabyasachi Chakrabarty as Sikandar Hyaat Khan
* Kelly Dorji as Gajanan Jeeva as Kunjal Bhaskar Reddy
* Pramod Mouthu as Shahnawaz Qureshi
* Nirmal Pandey as Anwar Shaikh
* Saurabh Shukla as Madhav Suri
* Ashish Vidyarthi as Mohd. Akhtar

==Production==
The film was shot in Delhi, Mumbai, Lahore, Hyderabad, India|Hyderabad, Ladakh, Lonavala, Malaysia, Bhiwandi, Chembur, Ghatkopar, and Chinchpokli.
  Chronicles of Narnia fame has sung vocals for Lahore &mdash; her first Asian film. Rob Miller, who earlier won a Filmfare award for Chak De! India (2007), is Lahores sports consultant.    MM Kreem has done the music.

==Home release==
Lahores DVDs and VCDs are available in Hindi with English subtitles on Moserbaer Home Video.  The DVD also features the making of the film.

==Soundtrack==
* Ab Ye Kaafila - KK (singer)|KK, Karthik (singer)|Karthik, M.M. Keeravani
* Musafir - Daler Mehndi
* Rang De - Shankar Mahadevan, Shilpa Rao
* Saaware - M.M. Keeravani
* Musafir - M.M. Keeravani
* O Re Bande - Rahat Fateh Ali Khan, Shilpa Rao
* Lahore Theme - Lisbeth Scott
* Fitrat - Surendra Chaturvedi (lyrics)

==International awards and recognition==
Lahore has won international awards including Special Jury Award at the 42nd WorldFest-Houston International Film Festival,  Best Actor - Aanaahad at Tenerife International Film Festival (UK), Most Aspiring Film Maker (Sanjay Chauhan) at Filmmakers International Film Festival (UK), Best actor - Aanaahad - at Salento International Film Festival, Italy and had all six nominations at Asian Festival Of First Films, Singapore.

==National film awards== National Film Awards, Indira Gandhi Award for Best First Film of a Director went to producer Vivek Khatkar and director Sanjay Puran Singh Chauhan while Farooque Shaikh won National Film Award for Best Supporting Actor. 

==Criticism==
The film was not released in Pakistan as it was regarded there as portraying Pakistan in a bad light. 

==Other awards and nominations==
;6th Apsara Film & Television Producers Guild Awards
Nominated
* Apsara Award for Best Performance in a Supporting Role (Male) - Farouque Sheikh
 2011 Zee Cine Awards
Nominated 

* Star Screen Awards for Best Background Score 2010 - Wayne Sharpe
* Stardust Awards for Hottest New Director 2010 - Sanjay Puran Singh Chauhan

==Subtitles==
The film subtitles major places in either Devanagari for India or Nastaleeq for Pakistan.

==References==
 

==External links==
* 
* 
* 
* 
*  
* 
* 
*  
*  
* 
* 
*  

 
 
 
 
 
 
 
 
 
 