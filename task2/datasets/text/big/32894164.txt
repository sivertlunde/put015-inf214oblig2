Bora Bora (1968 film)
{{Infobox film
| name           = Bora Bora
| director       = Ugo Liberatore
| producer       = Alfredo Bini  Eliseo Boschi	
| writer         = Ugo Liberatore
| starring       = Haydée Politoff Corrado Pani Doris Kunstmann Rosine Copie
| music          = Piero Piccioni (Italian version) Les Baxter (American version)
| cinematography = Leonida Barboni
| distributor    = American International Pictures
| released       =  
| runtime        = 97 minutes
| country        = Italy
| language       = Italian}}

Bora Bora is a 1968 Italian sexploitation film, directed by Ugo Liberatore. The story is about an estrange married couple, who reconcile their differences in the sensual surroundings of the Tahitian island. The movie was picked up for American distribution by American International Pictures, who removed some scenes (cutting down the running length from 97 to 90 minutes) and replaced the original music of Italian composer Piero Piccioni with a new score by exotica legend Les Baxter, the regular composer of choice for the studio.

 
 
 
 