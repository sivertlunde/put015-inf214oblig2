Lycanthropus
{{Infobox Film
| name           = Lycanthropus
| image          = Werewolfgirlsdormitory.jpg
| image_size     = 
| caption        = Film poster under alternative title
| director       = Paolo Heusch
| producer       = Guido Giambartolomei
| writer         = Ernesto Gastaldi
| narrator       = 
| starring       = Barbara Lass Curt Lowens Carl Schell
| music          = Armando Trovajoli
| cinematography = George Patrick
| editing        = Julian Attenborough MGM
| released       =  
| runtime        = 82 min
| country        = Italy Austria
| language       = Italian
| budget         = 
}}
Lycanthropus (also known as Werewolf in a Girls Dormitory, The Ghoul in School, I Married a Werewolf and Monster Among the Girls) is a 1962 horror film directed by Paolo Heusch. MGM released it as a double feature with Corridors of Blood in the United States. 

==Synopsis==
Wolves have been seen roaming around a girls reformatory, and when the girls begin to get murdered, suspicion focuses on both the wolves and on a newly hired science teacher who might be a werewolf.

==Cast==

*Barbara Lass as Priscilla
*Carl Schell as Julian Olcott
*Curt Lowens as Director Swift
*Maureen OConnor as Leonore MacDonald
*Maurice Marsac as Sir Alfred Whiteman
*Luciano Pigozzi as Walter the Caretaker
*Joseph Mercier as Tommy the Porter
*Mary McNeeran as Mary Smith
*Annie Steinert as Sheena Whiteman
*Grace Neame as Sandy

==Notes==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 