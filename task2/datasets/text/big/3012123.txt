Howling V: The Rebirth
{{Infobox film
| name           = Howling V: The Rebirth
| image          = Howling V.jpg
| caption        =
| director       = Neal Sundstrom
| producer       = Gary Barber Harvey Goldsmith Steven A. Lane Robert Pringle Edward Simons Clive Turner
| writer         = Source novels: Gary Brandner Screenplay: Freddie Rowe Clive Turner Phil Davis Victoria Catlin Elizabeth Shé Ben Cole Lee Pembleton
| music          = The Factory
| cinematography = Arledge Armenaki
| editing        = Claudia Finkle Bill Swenson
| distributor    = Lionsgate
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $2,000,000
| website        =
}}
 horror sequel The Howling. It was directed by Neal Sundstrom from the screenplay by Freddie Rowe and Clive Turner, and filmed in Budapest, Hungary.
 The Howling The Beast Must Die, the setting of a large castle (rather than that films mansion) and the castles backstory are taken from the narrative of the original The Howling novel, where it served as the backstory of a town named Dradja.

It stars   in 2003 by Artisan Home Entertainment and in 2007 by Timeless Media Group.

==Plot==
After being shuttered for over 500 years following a horrific, intentionally staged family massacre, a mysterious Hungarian castle opens its doors with the apparent intention of attracting tourist business. A diverse group of people from different parts of the globe is assembled at the eerie dwelling after having been chosen when they applied for a visa. But once they arrive some begin to wonder if there is more going on than meets the eye. First they hear terrible stories about savage packs of wolves that used to roam the area and then people begin to disappear, only some of whom are found later with their throats torn out. It soon becomes clear that a murderer is among them, and the culprit may only partially be human.

However, as the story progresses and the ultimate truth is revealed, ties between predator, prey and the very castle itself will be fatally exposed.

==Cast== Philip Davis as The Count
* Victoria Catlin as Dr. Catherine Peake
* Elizabeth Shé as Marylou Summers
* Ben Cole as David Gillespie William Shockley as Richard Hamilton
* Mark Sivertsen as Jonathan Lane
* Stephanie Faulkner as Gail Cameron
* Mary Stavin as Anna
* Clive Turner as Ray Price
* Nigel Triffitt as The Professor
* Jill Pearson as Eleanor
* József Madaras as Peter
* Renáta Szatler as Susan

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 