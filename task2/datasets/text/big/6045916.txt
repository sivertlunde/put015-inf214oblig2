I Love Lucy (film)
{{Infobox film
| name = I Love Lucy
| image = 
| caption = 
| director = Marc Daniels ( ) Edward Sedgwick ( )
| producer = Jess Oppenheimer
| writer = Jess Oppenheimer Madelyn Pugh Bob Carroll, Jr.
| starring = Lucille Ball Desi Arnaz Vivian Vance William Frawley
| music = Eliot Daniel
| editing = Dann Cahn
| studio = Desilu
| distributor = Metro-Goldwyn-Mayer CBS Paramount Home Entertainment (2007)
| released = October 23, 2007 April 27, 2010 ( )
| country = United States
| runtime = 81 minutes
| language = English
| budget = 
}}
I Love Lucy, aka I Love Lucy: The Movie is a 1953 American feature film Spin-off (media)|spin-off of the sitcom I Love Lucy. Except for one test screening in Bakersfield, California,  the film was never theatrically released and was shelved.

==Plot==
The film plays out with three first-season episodes edited together into a single story: "The Benefit", "Breaking the Lease", and "The Ballet", with new footage included between episodes to help transition the episodes into one coherent storyline. As the series routinely took the format of filming scenes in chronological order, this adds to the "show within a show within a show" format of the film, as viewers watch the cast perform the episodes live. The film itself ends with a "curtain call", as the cast comes out and Arnaz thanks the audience for their support.   

==Cast== Lucille Esmeralda "Lucy" McGillicuddy Ricardo / Herself Enrique Alberto Fernando "Ricky" Ricardo y de Acha III / Himself Ethel May Mertz (née Potter)  / Herself Frederick Hobart "Fred" Mertz / Himself
* Ann Doran as Audience member
* Mary Wickes (archive footage) as Madame Lemand
* Barbara Pepper (archive footage) as Party guest

==Development==
Shortly after the end of the first season of I Love Lucy, Desi Arnaz and Lucille Ball decided to cash in on their shows popularity by compiling several episodes of the first season of the series into a movie.

A test screening in Bakersfield, California went very well and Desilu (through distributor United Artists) prepared to release the film. But Metro-Goldwyn-Mayer demanded the film be shelved because they felt it would diminish interest in the upcoming MGM film, The Long, Long Trailer, which Lucy and Desi co-starred in, and were contractually bound to promote. The I Love Lucy movie was ultimately forgotten.

==Format and framing sequence==
Unlike most movies of existing media properties, I Love Lucy: The Movie is framed within the meta-context of a "show within a show within a show". The film is framed around a plot involving a young married couple (played by actress Ann Doran and actor Benny Baker) attending the filming of an episode of I Love Lucy. After an opening sequence of Ann Doran and Benny Bakers characters arriving at the studio, there is a brief introduction by the shows announcer, Roy Rowan, as he introduces Desi Arnaz, who speaks to the studio audience and introduces the cast (something Arnaz would do throughout the run of the series).

==Lost and found==
After plans for a theatrical release were scuttled by MGM, the film was largely forgotten and the twelve minutes of new footage shot for the film were considered to be lost forever. However, in 2001, the film was found and clips of it were featured in I Love Lucys 50th Anniversary Special. A screening was held in August 2001 at the fifth Loving Lucy fan convention in Burbank, California. 

==DVD releases==
The film was released on DVD on October 23, 2007 as one of the features on a bonus disc in The "I Love Lucy" Complete Series boxset.

The disc received a separate release on April 27, 2010. 

==References==
 
 

==External links==
*  

 

 
 
 
 
 
 
 
 