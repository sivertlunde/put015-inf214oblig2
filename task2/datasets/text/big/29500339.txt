Emergency Wedding
{{Infobox film
| name           = Emergency Wedding
| image          = "Emergency_Wedding"_(1950).jpg
| image_size     = 
| caption        = 
| director       = Edward Buzzell
| producer       = Nat Perrin
| writer         = Nat Perrin Claude Binyon
| narrator       = 
| starring       = Larry Parks Barbara Hale
| music          = Werner R. Heymann
| cinematography = Burnett Guffey Al Clark
| studio         = Columbia Pictures Corporation
| distributor    = Columbia Pictures
| released       =  
| runtime        = 78 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} You Belong to Me, which featured Larry Parks in a bit part.    

==Plot==
Dr. Helen Hunt is a physician married to millionaire Peter Judson Kirk Jr., who is jealous his wife is spending too much time with her male patients. He makes a fool of himself trying to prove her guilt, which causes his wife to leave. But then he puts up the money for a new hospital and she comes back to him.  

==Cast==
*Larry Parks as Peter Judson Kirk
*Barbara Hale as Dr. Helen Hunt  
*Willard Parker as Vandemer
*Una Merkel as Emma
*Alan Reed as Tony
*Eduard Franz as Dr. Heimer
*Irving Bacon as Filbert - Mechanic
*Don Beddoe	as Forbish - Floorwalker
*Jim Backus	as Ed Hamley

==Critical reception== You Belong to Me." The verdict on this entry was amiable. It was a lightweight lot of fun done by a covey of actors who were happy in their work. Yesterday, the Palace received "Emergency Wedding,"...This remake of a yarn about a millionaire—an insecure gent ludicrously jealous of his doctor-wife—is lightweight without being especially gay or serious. "Emergency Wedding," except for a titter or two and an attempt to diagnose what ails organized medicine, is an unimpressive reproduction."      

==References==
 

==External links==
* 

 

 
 
 