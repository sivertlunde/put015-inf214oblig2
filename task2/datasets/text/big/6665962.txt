Gypsy (1962 film)
{{Infobox film   
| name           = Gypsy
| image          = GypsyFilmPoster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Mervyn LeRoy
| producer       = Mervyn LeRoy
| screenplay     = Leonard Spigelgass
| story          =  
| based on       =  
| starring       = Rosalind Russell Natalie Wood Karl Malden
| music          = Jule Styne
| cinematography = Harry Stradling Philip W. Anderson
| distributor    = Warner Bros.
| released       =  
| runtime        = 143 minutes 
| country        = United States
| language       = English
| budget         = $4 million
| gross          = $11,076,923  . The Numbers. Retrieved June 13, 2013. 
}} remade for television in 1993.

==Plot== Rose Hovick Orpheum Circuit.

 Years pass, and the girls no longer are young enough to pull off the childlike personae their mother insists they continue to project. June rebels and elopes with Jerry, one of the dancers who backs the act. Devastated by what she considers an act of betrayal, Rose pours all her energies into making a success of Louise, despite the young womans obvious lack of singing and dancing skills. Not helping matters is the increasing popularity of sound films, which leads to a decline in the demand for stage entertainment. With bookings scarce, mother and daughter find themselves in Wichita, Kansas, where the owner of a third-rate burlesque house offers Louise a job. 

When one of the strippers is arrested for shoplifting, Louise unwillingly becomes her replacement. At first her voice is shaky and her moves tentative at best, but as audiences respond to her she begins to gain confidence in herself. She blossoms as an entertainer billed as Gypsy Rose Lee, and eventually reaches a point where she tires of her mothers constant interference in both her life and wildly successful career. Louise confronts Rose and demands she leave her alone. Finally aware she has spent her life enslaved by a desperate need to be noticed, an angry, bitter, and bewildered Rose stumbles onto the empty stage of the deserted theatre and experiences a moment of truth that leads to an emotional breakdown followed by a reconciliation with Louise.

==Cast==
* Rosalind Russell as Rose Hovick 
* Natalie Wood as Louise Hovick 
* Karl Malden as Herbie Sommers 
* Paul Wallace as Tulsa Suzanne Cupito as Baby June 
* Ann Jillian as Dainty June 
* Diane Pace as Baby Louise 
* Betty Bruce as Tessie Tura  
* Faith Dane as Mazeppa 
* Roxanne Arlen as Electra

==Musical numbers==
# Overture – Orchestra, conducted by Jule Styne
# "Small World" – Rose
# "Some People" – Rose
# "Baby June and Her Newsboys" – Baby June, Chorus
# "Mr. Goldstone, I Love You" – Rose and chorus
# "Little Lamb" – Louise
# "Youll Never Get Away From Me" – Rose, Herbie
# "Dainty June and Her Farmboys" – Dainty June, Chorus
# "If Mama Was Married" – June, Louise
# "All I Need Is the Girl" – Tulsa
# "Everythings Coming Up Roses" – Rose
# "Together Wherever We Go" – Rose, Herbie, Louise
# "You Gotta Have a Gimmick" – Tessie Tura, Mazeppa, Electra
# "Small World" (Reprise) – Rose
# "Let Me Entertain You" – Louise
# "Roses Turn" – Rose

"Together Wherever We Go" was deleted prior to the films release, although it was included on the soundtrack album, and "Youll Never Get Away From Me" was abbreviated to a solo for Rose following the initial run. In the DVD release of the film, both numbers – taken from a 16-millimeter print of inferior quality – are included as bonus features. 

==Production==
Rosalind Russell and her husband, theatre producer Frederick Brisson, were hoping to do a straight dramatic version of the story based directly on the memoir by Gypsy Rose Lee, but the book was irrevocably tied up in the rights to the play. Coincidentally, Russell had just starred in the film version of the Leonard Spigelgass play A Majority of One at Warner Bros., which Brisson had produced, and all parties came together to make Gypsy, with Russell starring, LeRoy directing, and Spigelgass writing the highly faithful adaptation of the Arthur Laurents stage book.

Although Russell had starred and sung in the 1953 stage musical Wonderful Town and the 1955 film The Girl Rush, the Gypsy score was beyond her. Her own gravelly singing voice was artfully blended with that of contralto Lisa Kirk. Kirks ability to mimic Russells voice is showcased in the final number "Roses Turn", which is a clever blend of both of their voices. Kirks full vocal version was released on the original soundtrack, although it is not the version used in the finished film. In later years, Russells original tryout vocals were rediscovered on scratchy acetate discs and included as bonus tracks on the CD reissue of the films soundtrack.  
 West Side Story the previous year, but Wood did her own singing in Gypsy. While Wood recorded a separate version of "Little Lamb" for the soundtrack album, in the film she sang the song "live" on the set.  Other songs performed live were "Mr. Goldstone, I Love You" and the reprise of "Small World," both sung by Russell (not Kirk).

==Reaction==
===Critical reception===
Film historian Douglas McVay observed in his book The Musical Film, "Fine as West Side Story is, though, it is equaled and, arguably, surpassed – in a rather different idiom – by another filmed Broadway hit: Mervyn LeRoy’s Gypsy. Arthur Laurents book (for) West Side Story (adapted for the screen by Ernest Lehman), though largely craftsmanlike, falls short of his libretto for Gypsy (scripted on celluloid by Leonard Spigelgass), based on the memoirs of the transatlantic stripper Gypsy Rose Lee. The dialogue and situations in Gypsy have more wit, bite and emotional range, and the characterizations are more complex.   

Variety (magazine)|Variety noted, "There is a wonderfully funny sequence involving three nails-hard strippers which comes when Gypsy has been unreeling about an hour. The sequence is thoroughly welcome and almost desperately needed to counteract a certain Jane One-Note implicit in the tale of a stage mother whose egotisms become something of a bore despite the canny skills of director-producer Mervyn LeRoy to contrive it otherwise. Rosalind Russells performance as the smalltime brood-hen deserves commendation ... It is interesting to watch   ... go through the motions in a burlesque world that is prettied up in soft-focus and a kind of phony innocence. Any resemblance of the art of strip, and its setting, to reality is, in this film, purely fleeting." 

===Box office performance=== theatrical rentals. 9th highest grossing film of 1962.

===Awards and nominations===
The film was nominated for 3 Academy Awards: Best Cinematography Lawrence of Arabia) Best Costume Design – Color – Orry-Kelly (lost to Mary Wills for The Wonderful World of the Brothers Grimm) Best Music Frank Perkins The Music Man)

Rosalind Russell won the Golden Globe Award for Best Actress – Motion Picture Musical or Comedy, her second consecutive win in this category; she won the previous year for A Majority of One. Additional nominations included: Best Motion Picture – Musical or Comedy (lost to The Music Man) Best Director – Mervyn LeRoy (lost to David Lean for Lawrence of Arabia) Best Actress – Motion Picture Musical or Comedy – Natalie Wood (lost to Rosalind Russell in this film) Best Actor – Motion Picture Musical or Comedy – Karl Malden (lost to Marcello Mastroianni in Divorce, Italian Style) New Star West Side Story)
 Writers Guild of America Award for Best Written American Musical.

==Home media==
Warner Home Video released the Region 1 DVD on May 2, 2000. The film is in anamorphic widescreen format with an audio track in English and subtitles in English and French.
 fullscreen format with audio tracks in French and English and subtitles in French.

Gypsy is one of six films included in the box set The Natalie Wood Collection released on February 3, 2009.

Gypsy was released on Blu-Ray Disc through the Warner Archive Collection on November 20, 2012.

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 