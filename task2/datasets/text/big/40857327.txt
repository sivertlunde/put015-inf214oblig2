The Heir to the Hoorah
 
{{infobox film
| title          = The Heir to the Hoorah
| image          =
| imagesize      =
| caption        =
| director       = William C. deMille
| producer       = Jesse Lasky
| writer         = Beatrice DeMille Leighton Osmun
| based on       =  
| starring       = Thomas Meighan Anita King
| music          =
| cinematography = Charles Rosher
| editing        =
| distributor    = Paramount Pictures
| released       = October 26, 1916 reels
| country        = US
| language       = Silent film English intertitles

}}
The Heir to the Hoorah is a surviving 1916 silent film produced by Jesse Lasky and released through Paramount Pictures. It was directed by William C. deMille. 

A print survives in the Library of Congress.  

==Cast==
*Thomas Meighan - Joe Lacy
*Anita King - Geraldine Kent
*Edythe Chapman - Mrs. Kent
*Horace B. Carpenter - Bud Young Charles Ogle - Bill Ferguson
*Ernest Joy - Mr. Marshall
*Joane Woodbury - Mrs. Marshall

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 

 