Invasion (miniseries)
{{Infobox television film
| name           = Invasion
| image          =
| caption        =
| director       = Armand Mastroianni
| producer       = Jeffrey Morton Randy Sutter Richard Arredondo
| writer         = Novella:   Christopher Orr Don Davis
| cinematography = Bryan England
| editing        = Scott Vickrey
| distributor    = Artisan Entertainment
| first_aired    =  
| last_aired     =  
| runtime        = 175 min.
| num_episodes   = 2
| country        = United States
| awards         =
| language       = English
| budget         =
}}
 novel of Robin Cook. It starred Luke Perry, Kim Cattrall and Rebecca Gayheart.

==Plot==
Small rocks fall from the sky which, when touched, trigger a latent virus that has always existed in humans and begins mutating them into an alien species. Taking advantage of its hive mentality, the aliens are absolutely dedicated to transforming every human on Earth and do so with alarming swiftness. Only a small group of remaining humans have the medical knowledge to devise antibodies to reverse the effects of the virus.

== Cast ==
* Luke Perry : Beau Stark
* Kim Cattrall : Dr. Sheila Moran
* Rebecca Gayheart : Cassy Winslow Christopher Orr : Pitt Henderson
* Jon Polito : Detective Kemper
* Neal McDonough : Randy North
* Cástulo Guerra : Eugene Ochoa Michael Warren : Dr. Harlan McCoy
* Chuck McCann : chairman of health committee
* Tim DeKay : Mike Landry
* John Finn : Colonel Brown  (non credited) 

== Awards ==
* Saturn Award 
** Saturn Award for best TV series 1998
* ALMA Awards 
* Motion Picture Sound Editors 
** Best Sound Editing in a TV movie 1998

== External links ==
* 
*  

 

 
 
 
 
 
 


 