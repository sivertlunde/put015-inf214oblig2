Caught in the Web
 
{{Infobox film
| name           = Caught in the Web
| image          = Caught in the Web poster.jpg
| caption        = Film poster
| director       = Chen Kaige
| producer       = 
| writer         = 
| starring       = Gao Yuanyuan
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 121 minutes
| country        = China
| language       = Mandarin
| budget         = 
}}
 Best Foreign Language Oscar at the 85th Academy Awards, but it did not make the final shortlist.   

==Plot==
Set in Hangzhou(杭州方向), in modern-day China, a young woman becomes embroiled in controversy after a cell phone video of her being disrespectful on a public bus to an elderly person goes viral. The aftermath effects her personal and professional life and brings her face to face with the videos poster, an ambitious journalist.

==Cast==
* Gao Yuanyuan as Ye Lanqiu
* Yao Chen as Chen Ruoxi
* Mark Chao as Yang Shoucheng
* Wang Xueqi as Shen Liushu Chen Hong as Mo Xiaoyu
* Wang Luodan as Yang Jiaqi
* Chen Ran as Tang Xiaohua
* Zhang Yi as Zhang Mu

==Reception==
Derek Elley of Film Business Asia gave the film a score of 9/10, and states "Chen has come late in the game to dealing with the social and ethical contradictions of life in modern-day China, but Web more than makes up for lost time. Densely plotted across its two hours running-time, the film starts as a black comedy on the destructive power of modern media (especially Chinas internet discussion boards) but from its midway point gradually morphs into a complex web of love and ambition, both gained and lost." 

Maggie Lee of Variety (magazine)|Variety describes it as "An engrossing, if lengthy social drama in which a womans minor public misconduct plunges her into a dragnet of cyber witch-hunting that unseats relationships, careers and a business empire, the punningly titled "Caught in the Web" reps a thought-provoking, character-driven morality tale by Chinese helmer Chen Kaige." 

Deborah Young of The Hollywood Reporter states "Chen Kaiges fast-paced contemporary dramedy is a masterful take on Internet character assassination." 

Dan Fainaru of Screen Daily describes the film as "A cinematic tour de force in every respect, Che Kaige’s complex, multi-layered story looks for a while like a satirical take on the misuse of new fancy gadgetry to deprive individuals of their last shred of privacy and ends on an introspective tone, suggesting there is no use blaming the gadgets, but the people behind them."     Fainaru further describes the film as "A surprisingly refreshing departure for one of China’s leading Fifth Generation filmmakers, better identified with period pieces." 

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Chinese submissions for the Academy Award for Best Foreign Language Film
* Human flesh search engine

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 