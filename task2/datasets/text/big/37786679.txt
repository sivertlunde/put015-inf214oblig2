House No. 44
{{Infobox film
| name           = House No. 44
| image          = House_No._44_Movie.jpg producer       = Dev Anand
| director       = M. K. Burman Kumkum Parshuram
| music          = Sachin Dev Burman Sahir Ludhianvi (lyrics) cinematography = V. Ratra
|editing=Dharamvir M.D. Jadhav Rao
| studio    = Navketan Films
| language          = Hindi released = 1 Jan 1955
| country         = India
| story          = Vishwamitter Adil
}} 1955 Hindi film directed by M. K. Burman and produced by Dev Anand for his banner Navketan Films. The movie stars Dev Anand and Kalpana Kartik in a lead role.  The film is also noted for its popular songs with music by Sachin Dev Burman with lyrics by Sahir Ludhianvi, including Teri Duniya Mein Jeene Se and Chup Hai Dharti Chup Hain Chand Sitaare sung by Hemant Kumar. 

==Plot==
In the movie Ashok works for a notorious gangster Sunder and his men. One day he comes across a dead body and he reports it to the police. Ashok than meets Nimmo and falls in love with her. Nimmo asks Ashok to leave the life of a gangster and settle down with her. Ashok agrees but after a few days of not having any food, Ashok realizes that he was better of being a gangster. The rest of the movie portrays the struggle of Ashok trying to stay with Nimmo or with Sunder.

==Cast==

* Dev Anand as Ashok
* Kalpana Kartik as Nimmo
* K. N. Singh as Captain
* Bhagwan Sinha as Sunder Rashid Khan as Jibbo Kumkum

==Songs==
The films music score was given by Sachin Dev Burman with lyrics by Sahir Ludhianvi  
#  Aag ladi bangle mein
#  Chup Hai Dharti Chup Hain Chand Sitaare -- Hemant Kumar
#  Dekh Idhar O Jaadugar
#  Dum Hai Baaki To Gum Nahin
#  Oonche Sur Mein Gaaye Jaa
#  Peeche Peeche Aakar   -- Lata Mangeshkar; Hemant Kumar; 
#  Phaili Hui Hain Sapnon Ki Baahen
#  Teri Duniya Mein Jeene Se

== References ==
 
* Cinema Modern: Navketan Story, by Sidharth Bhatia. Harpercollins, 2011. ISBN 978-93-5029-096-5.

==External links==
* 

 
 
 
 