Fascist Legacy
{{Infobox film
| name           = Fascist Legacy
| image          =
| caption        =
| director       = Ken Kirby
| producer       = BBC
| writer         = Ken Kirby Michael Palumbo
| starring       =
| music          =
| cinematography = Nigel Walters
| editing        = George Farley
| distributor    = BBC
| released       =  
| runtime        = 2x50 minutes
| country        = United Kingdom English
| budget         =
| gross          =
}}

Fascist Legacy is a 1989 BBC documentary film about Italian war crimes during World War II. It consists of two parts.

The first part itself consists of two sections and was aired on November 1, 1989, on BBC, under the title A Promise Fulfilled.

==Part one== Ethiopia are shown.
 Slovene and Croatian civilians Podhum near Rijeka are shown.

==Part two== Italian war criminals (the most wanted were Pietro Badoglio, Mario Roatta and Rodolfo Graziani), for whom Yugoslavia, Greece and Ethiopia provided full documentation of their crimes.
 Churchills quote about "the better tomorrow with a new world order."

==Historical truth==
If Italian officers were prosecuted by the (British controlled) court at all, they were accused only of the death of the British prisoners of war, but not of the death of the civil population in occupied territories. It was on September 9, 1943, the day of Allies invasion of the Italian mainland, when   was wounded. As an anti-fascist, general Bellomo may have been considered a threat to the Badoglio government. Nicola Bellomo, as a gesture of military honour, preferred not to escape from the prison when the door was intentionally left open, after he was sentenced to death.

==Non-prosecution of Italian war criminals== Italian war criminals who were however never prosecuted because the British and American governments with the beginning of cold war saw in Pietro Badoglio a guarantee of an anti-communist post-war Italy. 

==Italian public media== war crimes resistance mythology, are aware of Vichy, too.   (Archived by WebCite®) 

After in the 1950s two Italian film-makers were jailed for depicting the Italian invasion of Greece, the Italian public and media were forced into the repression of collective memory, which led to historical amnesia and eventually to historical revisionism.  

In 2004 only the Italian private channel La7 has shown large excerpts of "Fascist Legacy". Showings of the documentary were also organized in Italy by groups with an anti-fascist orientation and members of the Slovene minority in Italy. 

==See also==
* Italian invasion of Ethiopia Italian invasion of Yugoslavia
* Italian invasion of Greece
* Italian concentration camps
* Italian war crimes
* Province of Ljubljana

==References==
 

==External links==
* , The Guardian, London, UK, June 25, 2003
* , Il Pane e le Rose, November 16, 2005 (many Italian newspapers articles referenced at the end)

 
 
 
 
 
 
 
 
 
 