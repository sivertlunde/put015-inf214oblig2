Simon Peter Ninakku Vendi
{{Infobox film
| name           = Simon Peter Ninakku Vendi
| image          =
| caption        =
| director       = PG Vishwambharan
| producer       = John Paul
| screenplay     = Madhu Urvashi Urvashi Devan Devan Jagathy Sreekumar
| music          = A. T. Ummer
| cinematography =
| editing        =
| studio         = Santhosh Creations
| distributor    = Santhosh Creations
| released       =  
| country        = India Malayalam
}}
 1988 Cinema Indian Malayalam Malayalam film, Devan and Jagathy Sreekumar in lead roles. The film had musical score by A. T. Ummer.   

==Cast== Madhu as Kesavadas Urvashi as Alice Devan as Simon Peter
*Jagathy Sreekumar as Lazar Innocent as Vaniyambadi Chandran
*Captain Raju as Ramji
*K. R. Vijaya as Savithri
*Lalu Alex as Arjun

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Manithooval Chirakulla || P Jayachandran || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 