Rude Awakening (film)
 
{{Infobox film
| name           = Rude Awakening
| image          = Rude Awakening 1989.jpg
| image_size     =
| alt            =
| caption        = UK DVD release cover
| director       = David Greenwalt Aaron Russo
| producer       = Aaron Russo
| writer         = Neil Levy Richard LaGravenese
| narrator       =
| starring       = Eric Roberts Cheech Marin
| music          = Jonathan Elias
| cinematography = Newton Thomas Sigel
| editing        = Paul Fried
| studio         =
| distributor    = Orion Pictures
| released       =  
| runtime        = 100 minutes
| country        = United States English
| budget         =
| gross          = $3,169,719 (USA)
}}
 1989 comedy film directed by David Greenwalt and Aaron Russo.

==Plot==
 FBI and underground newspaper stoner whose pacifist hippies into committed soldiers; Jesus proves their failure by wishing them peace as he leaves the lab.) The two flee the inner-city commune they are living in, leaving behind Sammy who feels it is important that he keep writing and publishing their message, and Freds girlfriend, artist Petra (Julie Hagerty).

Twenty years later, Fred and Jesus are still living in the jungle, when they find a dying man who has been shot by soldiers. He gives them some documents and tells them it is vital they get the papers back to the US government. The documents imply that the US is planning to invade that very country, and outraged, Fred and Jesus decide to return to the US to get the action stopped. Having been living in isolation (and by implication, stoned the entire time) for the last 20 years, they find the 1980s, entrenched in the yuppie ethos, to be something of a shock. Sammy and Petra have both embraced the materialistic culture, and it takes considerable persuasion from Fred and Jesus (including a memorable speech where Jesus makes numerous profound points, ending each one with Thats all I got to say, before launching into another ramble) before they will agree to help.

Fred, Jesus, Sammy, and Petra join forces to lead a sit-in at the University of New York to protest the planned invasion, which leaves the group despondent; the student body is indifferent and the documents turn out to be a theoretical exercise and not any genuine invasion plans. However, the controversy brought up by their publication implies that Americans would welcome a war we can win and so the invasion actually happens. Fred is broken by the idea that he started a war, and gives up all hope, until some students track him down to ask for his help in mobilising action; they are concerned by the numerous ecological and social problems they see around them. Fred realizes that despite having failed in his personal mission to prevent war, as long as there are young and idealistic people out there that share his views, there will always be hope for the world.

The film concluded with an onscreen sing-along to the song "Revolution (Beatles song)|Revolution" during the credits.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Cheech Marin || Jesus Monteya
|-
| Eric Roberts || Fred Wook
|-
| Julie Hagerty || Petra
|-
| Robert Carradine || Sammy
|-
| Cindy Williams || June
|-
| Tom Sizemore || Ian
|-
| Andrea Martin || April
|-
| Rae Dawn Chong || Marlene
|-
| Louise Lasser || Bonnie
|-
| Timothy Leary || Diner at Ronnies
|}

==Critical reception==
Rude Awakening received negative reviews from critics. It currently holds a 19% rating on Rotten Tomatoes based on 21 reviews.

 ." 

Roger Ebert of The Chicago Sun-Times gave the film zero out of 4 stars and expressed his dislike of it: "Rude Awakening  is such a hapless movie that one is tempted to be charitable toward it, to describe it as a sincere idea gone horribly wrong, rather than as an exercise in idiocy. But kindness is the wrong policy here, I think; the perpetrators of this film should instead be encouraged to seek out entirely new directions for their next work... No one in this movie has an adequate intelligence level. The dialogue of the characters is half-witted, their actions are inexplicable, and to the degree that they possess personalities, they are boring, self-important clods." 

==References==
 

== External links ==
* 
* 
* 
* 

 
 
 
 
 
 
 
 