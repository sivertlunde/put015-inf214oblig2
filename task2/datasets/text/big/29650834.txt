Safe House (2012 film)
 
{{Infobox film
| name           = Safe House
| image          = Safe House Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Daniel Espinosa
| producer       = Scott Stuber
| writer         = David Guggenheim
| starring       = {{plainlist|
* Denzel Washington
* Ryan Reynolds
}}
| music          = Ramin Djawadi Oliver Wood Rick Pearson
| studio         = {{plainlist|
* Relativity Media
* Stuber Productions
}} Universal Pictures
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         = $85 million 
| gross          = $208.1 million 
}} action thriller thriller film Universal Pictures.  Filming took place in Cape Town, South Africa. The film premiered in New York City on February 7, 2012, and was released in U.S. theatres on February 10, 2012.

==Plot== CIA Non-official NOC operative MI6 agent Liam Cunningham). Attacked by a team of mercenaries led by Vargas (Fares Fares), Wade is killed and Frost is forced to surrender himself to the American consulate in Cape Town, South Africa.
 CIA headquarters in Langley, Virginia, Catherine Linklater (Vera Farmiga), another CIA operative, orders Weston to lie low and await further instructions.

Weston hides out with Frost and calls his girlfriend Ana Moreau (Nora Arnezeder), a French medical resident who does not know that he works for the CIA, and tells her to leave the house. Barlow later tells Weston to go to Cape Town Stadium to retrieve a GPS device with the location of a nearby safe house. He retrieves the GPS at the stadium, but Frost creates a diversion and gets away by disguising himself as a policeman. Weston, detained by the police, escapes but cant catch Frost. Weston goes to recapture Frost but Frost ambushes and aims a gun at him, then says bluntly "I only kill professionals" and bursts Westons eardrum by shooting into the wall right next to him before leaving the area.

Weston is ordered to visit the nearest American embassy for debriefing. Instead he meets with Ana and reveals that he is a CIA agent. He tells her to return to Paris for her safety. Weston tracks Frost to a shantytown in Langa, Cape Town|Langa, where Frost is meeting Carlos Villar (Rubén Blades), an old contact and document forger. Vargas attacks again, killing Villar along with his wife, but Frost eludes him and his men with Westons help. Weston subsequently learns that Vargas is actually working for the CIA, which is seeking to retrieve the storage device Frost received from Wade. The device is Israeli intelligence, revealed to contain details of corrupt officials and secret money transfers involving American CIA, British MI6, and other intelligence agencies; Weston recognizes the reference to the Mossad because the CIA accused Frost of selling secrets to them before he became a rogue agent. Westons own superiors may be implicated. Frost is taken to the new safe house by Weston, where Weston is attacked by the housekeeper, Keller (Joel Kinnaman).

After a great deal of struggling, Weston kills Keller but is badly wounded in the fight. Frost leaves Weston who then passes out from the wounds sustained fighting Keller. Meanwhile, Linklater has arrived in South Africa with Barlow to collect Frost and Weston from the safe house, but on the way is shot and killed by Barlow, who goes to the safe house and reveals that he is Vargass employer. He confirms that the file contains incriminating evidence against him, and encourages Weston to lie about what has happened. Frost returns to rescue Weston by killing Vargas and his men but is shot by Barlow. Weston then shoots Barlow in the chest, killing him. Frost gives Weston the file and tells Weston he is better than him before he dies from his injuries.

Back in the United States, Weston meets with CIA Deputy Director Harlan Whitford (Sam Shepard), who informs Weston that unflattering facts about the CIA must be removed from his report, but that he will be promoted. He asks Weston about the files location but Weston denies having been told about it by Frost. Whitford states that whoever has those files will have many enemies. Weston leaves, then leaks the files to the media, incriminating personnel from many intelligence agencies, including Whitford. Later on, Weston sees Ana across a street in Paris, France. She reads a note passed to her from him, looks up at Weston, and they both make eye contact and she smiles before Weston walks away.

==Cast==
* Denzel Washington as Tobin Frost
* Ryan Reynolds as Matt Weston
* Vera Farmiga as Catherine Linklater
* Brendan Gleeson as David Barlow
* Sam Shepard as Harlan Whitford
* Rubén Blades as Carlos Villar
* Nora Arnezeder as Ana Moreau
* Robert Patrick as Daniel Kiefer Liam Cunningham as Alec Wade
* Joel Kinnaman as Keller
* Fares Fares as Vargas
* Sebastian Roché as Robert Heissler
* Jake McLaughlin as Miller
* Nicole Sherwann as Whitfords Assistant
* Robert Hobbs as Morgan

==Soundtrack==
{{Infobox album  
| Name       = Safe House: Original Motion Picture Soundtrack
| Type       = Soundtrack
| Artist     = Ramin Djawadi
| Cover      = Safe House.jpg
| Alt        = 
| Released   =  
| Recorded   = 
| Genre      = Film score
| Length     =  
| Label      = Varèse Sarabande  067137  
| Producer   = Ramin Djawadi Fright Night (2011)
| This album = Safe House (2012) Red Dawn (2012)
}}

Ramin Djawadi composed the score to the film. Songs used in the film but not included in the soundtrack album were:
* "Rebel Blues" performed by Don Omar|Lëk Sèn
* "No Church in the Wild" performed by Kanye West & Jay-Z featuring Frank Ocean

===Track listing===
{{Track listing
| all_music       = Ramin Djawadi
| title1          = Safe House
| length1         = 3:15
| title2          = A Hundred Lies a Day
| length2         = 3:15
| title3          = Get in the Trunk
| length3         = 4:24
| title4          = Do I Make You Nervous?
| length4         = 3:07
| title5          = I Used to Be Innocent Like You
| length5         = 2:15
| title6          = Tobin Frost
| length6         = 2:19
| title7          = Off the Grid
| length7         = 3:27
| title8          = Do What You Have to Do
| length8         = 4:48
| title9          = Dont Kill Innocent People
| length9         = 3:45
| title10         = Who Do You Work For?
| length10        = 3:44
| title11         = Walk Away
| length11        = 6:03
| title12         = People Change
| length12        = 2:16
| title13         = Be Better Than Me
| length13        = 4:11
| title14         = Langa
| length14        = 6:14
| title15         = More Past Than Future
| length15        = 3:19
| title16         = 12 Months
| length16        = 3:05
| title17         = Truth
| length17        = 3:42
| title18         = Ill Take It from Here
| length18        = 5:48
}}

==Release==

===Critical reception===
Safe House earned mixed reviews from critics, with praise for solid performances by Reynolds and Washington. Review aggregator Metacritic gives the film a metascore of 52 out of 100 based on 36 reviews, indicating "mixed or average" reviews,  while it holds a "rotten" score of 53% on Rotten Tomatoes based on 177 reviews, with an average rating of 5.7/10, and the sites consensus reading: "Safe House stars Washington and Reynolds are let down by a thin script and choppily edited action sequences." 

===Box office===
The film grossed $126,373,434 in North America and $81,702,771 in other territories, for a worldwide total of $208,076,205.

Safe House earned $13.6 million on opening day, and $40.1 million over the weekend including $10.5 million overseas, ranking in second place to   ($85,058,003) and  ,   and The Vow.

===Home media===
Safe House was released to Blu-ray Disc|Blu-ray and DVD on June 5, 2012,  in the United States. 

==Sequel==
In September 2012, it was announced that Universal hired writer David Guggenheim to write a script for the sequel. 

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 