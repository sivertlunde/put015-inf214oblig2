The Seashell and the Clergyman
 
The Seashell and the Clergyman ( ) is an experimental French film directed by Germaine Dulac, from an original scenario by Antonin Artaud. It premiered in Paris on 9 February 1928 in film|1928.

==Synopsis==
The film follows the erotic hallucinations of a priest lusting after the wife of a general.

==Production background==
Although accounts differ, it seems that Artaud disapproved of Dulacs treatment of his scenario. The film was overshadowed by Un chien andalou (An Andalusian Dog, 1929), written and directed by Luis Buñuel and Salvador Dalí. Un chien andalou is considered the first surrealist film, but its foundations in The Seashell and the Clergyman have been all but overlooked. However, the iconic techniques associated with surrealist cinema are all borrowed from this early film. In Lee Jamiesons own analysis of the film, the surrealist treatment of the image is clear. He writes: 
"The Seashell and the Clergyman penetrates the skin of material reality and plunges the viewer into an unstable landscape where the image cannot be trusted. Remarkably, Artaud not only subverts the physical, surface image, but also its interconnection with other images. The result is a complex, multi-layered film, so semiotically unstable that images dissolve into one another both visually and semantically, truly investing in films ability to act upon the subconscious."  

The British Board of Film Censors famously reported that the film was "so cryptic as to be almost meaningless. If there is a meaning, it is doubtless objectionable".  

Alan Williams has suggested the film is better thought of as a work of or influenced by German expressionism. 

==Musical scores==
The silent film is popular with musicians and has been scored by many groups. It was one of the first films scored by Silent Orchestra and performed by them at the National Museum of Women in the Arts in Washington, DC in 2000.  This was the first film to be scored by live accompaniment band Minima.  Their debut performance was at the UKs Shunt Vaults at London Bridge in 2006. It has also been rescored by Steven Severin of Siouxsie and the Banshees and The Black Cat Orchestra.  

Sons of Noel and Adrian performed a live score at The Roundhouse in June 2009.  In March 2011, Imogen Heap performed an a cappella score of her own composition with the Holst Singers as part of the Birds Eye View festival. 
 Creative Commons license.

==See also==
*  (DVD collection which includes The Seashell and the Clergyman)
*Experimental film

==References==
 

==Bibliography==
*Wendy Dozoretz, Germaine Dulac : Filmmaker, Polemicist, Theoretician,   (New York University Dissertation, 1982), 362 pp.
*Charles Henri Ford, Germaine Dulac : 1882 - 1942, Paris : Avant-Scène du Cinéma, 1968, 48 p. (Serie: 	Anthologie du cinéma ; 31)

==External links==
 
* 

 
 
 
 
 
 
 
 
 
 