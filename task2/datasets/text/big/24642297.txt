26 Summer Street
{{Infobox Film
| name           = 26 Summer Street
| director       = Steve Erik Larson
| producer       = Scott Ferril
| writer         = Peter Syvertsen
| starring       = Peter Syvertsen Rachael Leigh Cook
| music          = Michael Wandmacher
| cinematography = Gregory R. Winter
| editing        = Tony Fischer
| released       =  
| runtime        = 17 minutes
| country        = United States
| language       = English
}}
26 Summer Street is a 1996 short film directed by Steve Larson, conceived and written by Peter Syvertsen, starring Syvertsen and Rachael Leigh Cook. It is based upon the short story The Girl with a Pimply Face by William Carlos Williams.

==Plot==
A doctor is called to treat an ailing infant in an apartment in 26 Summer Street, which is in a poorer area of the town. He is welcomed by the infants elder sister who tells him right at the door that it looks to her like the infant will die soon. Her mother is not home. While he examines the baby, he notices that the girl scratches her legs. He gives her advice on what to do to help with her legs and cure her acne. The doctor diapers the baby and gives her back to her sister. She asks if the baby is going to die. He says no and leaves with the excuse that he has to visit other patients in the neighborhood and instructs the girl to tell her mother that he will be back at 6 pm. She wants to come with him, but he passes over the offer with a joke. Before he goes she asks him why he is lying to her—he doesnt have another place around the neighborhood to go to. He just smiles and leaves.

He sits down on the stairs in front of the house and writes down his thoughts about the girl (under the title "The Girl with the Pimpy Face"): "Boy, she was tough and no kidding but I fell for her immediately. There was that hard, straight thing about her that in itself gives an impression of excellence." The girl comes down the stairs with the infant in her arms and asks if he plans to sit there till six. She gives him her sister. After holding her for a while, he is surprised and uses his stethoscope on her. He looks worried. He gives the baby back and leaves to buy soap for the girl. His thoughts reveal that he knows that the baby has maybe 6 months to live because she has a bad heart.

==Cast==
* Peter Syvertsen as Dr. Williams
* Rachael Leigh Cook as The Girl
* Marybeth Fisher as Flossie
* Bill Caulfield as Man in Hallway
* Julie Tehven as Woman in Dream
* Liam Caulfield and Heather Wilson as Baby

==Showings==
* 1997 Minneapolis-St. Paul International Film Festival
* 1997 Midwest Film and Video Showcase

==External links==
*  

 
 
 

 