Chal Pichchur Banate Hain
 
 
{{Infobox film
| name           = Chal pichchur Banate Hain
| image          = Chal Pichchur Banate Hain.jpg
| caption        = 
| director       = Pritish Chakraborty
| editor         = Amardeep Singh Khichi
| producer       = Madanlal Jain  Twilight Entertainment Pvt Ltd
| screenplay     = Pritish Chakraborty
| story          = Twilight Entertainment Pvt Ltd Rahil Tandon Bhavna Ruparel
| music          = Gaurav Dagaonkar
| cinematography = Hari Nair
| released       =  
| country        = India
| language       = Hindi website         = http://www.imdb.com/title/tt2367056/
}}
Chal Pichchur Banate Hain is a Hindi family comic drama film, the debut film of writer and director Pritish Chakraborty. The film was released all over on 7 September 2012.  The film is a delightful satirical take on the Hindi film industry  and was appreciated for its fresh and cinematic content. Filming of the movie took place primarily in Goa and Mumbai. Made on a shoestring budget the film explores themes like rampant copy paste in the film industry, the lack of originality in ideas, the prevailing star system alongside the unending passion of a true cinema lover in a humorous but realistic way through the brilliantly portrayed journey of a young MBA who quits everything and goes against everyone in order to pursue his dream of becoming a filmmaker.  Screenplay, Dialogues and Direction of this 2012 film is by Pritish Chakraborty, Music is by Gaurav Dagaonkar, Cinematography is by Hari Nair while the film is edited by debutante editor Amardeep Singh Khichi. 

==Plot==
Suraj has everything any young man would kill for – an MBA, a well-paid job and an offer from UK with a salary of 5,000 pounds.
But Suraj is no ordinary boy! He has only one passion – Cinema. His only dream is to be a film Producer-Director. He chooses passion over money and becomes a rebel. He produces a shocker to his family and friends, that he is quitting his job to follow his dream of film making. Being disowned by everyone and without any industry contacts, Suraj is left all alone to find his way in the big bad world of Bollywood.

Suraj starts his career by putting posters alongside roads. He struggles as an assistant on the sets and has to face rejection from the industry, his friends, family and loved ones.

Chal Pichchur Banate Hain is Suraj’s passionate journey from an MBA to a filmmaker.

==Cast==
* Rahil Tandon as Suraj Kumar has one dream; he wants to become a film director-producer. He has been obsessed with films since childhood and he is willing to give up his MBA career in order to fulfill his dream. His parents, like in most cases are against it and even his friends regard him as a fool to think of giving up a stable job and salary for the film industry where he has no knowledge or experience. He only has one thing – PASSION
* Bhavna Ruparel as Melrena is the girl next door. She is beautiful, lively and becomes Suraj’s greatest strength.
* Sandeep Sachdev as Sadiq Khan is classy and cocky. He is the actor who everybody wants to make films with and everybody wants to be around. Sadiq however, is not all talk – and all he really wants to do is be someone more than just a star in a baseless film. He wants to be seen doing meaningful films.
* Denesh Pandey as Sameer Kumar is Suraj’s father and since childhood has groomed Suraj to take up a serious career. In order for Suraj to have a settled life, he has encouraged him to do his MBA. He is fed-up of his son’s non-chalant attitude towards his job and wants him to give up all ambitions of being a filmmaker.
* Smita Hai as Amrita Kumar is Suraj’s mother. Like any other typical Indian mother, Amrita wants her son to be free of evils and bad thoughts. She is the mother who strongly believes in astrology, also consulting a Pandit to remove all thoughts of pursuing a ‘film career’ from her son’s mind. Amrita is beautiful and loud. She speaks her mind out and is what every mother is – protective.
* Errol Peter Marks as Nikhil is Suraj’s childhood friend. His sister was to wed Suraj, before Suraj shocks his friends and family and plans to work on films.
*Sheikh Arif as Shakti is an audience pleaser and a money launderer. He is ambitious and liked by all. He is also someone who at first glance comes across as a trouble maker.
*Vipul Bhatt as Ramani is a strong powerful man, who initially is a family friend, but later turns around to be the bad guy.
*Shahnawaz Pradhan as Irani is a very straightforward biggie in the film industry. He is extremely upfront and lets newcomers know what he wants. Irani is like every top director-producer who does not give newcomers a break in the filmy parivaar.
* Mukesh Bhatt as Mansoor has a callous attitude of his own. He works for film producers with young boys, hiring them for a job that includes putting up posters alongside roads.
*Punkaj Kalraa as Khanna is a ‘know it all-have it all’ Bollywood Biggie. Khanna is a smart film Director and Producer who knows exactly what he wants.

* Reecha Sinha as Sadhna.

==Critical Reception==
Martin DSouza, of Yahoo gave the film a rating of 2.5/5 and wrote, "Chal Pichchur Banate Hain is a refreshing story idea. Although we have seen films like Luck By Chance, Khoya Khoya Chand and more recently, Mere Dost Picture Abhi Baaki Hai, this film is not repetitive in the sense that there is no sense of having seen it. The reason being, it is totally and outsiders perspective and his journey of following his passion of film-making.

Although director Pritish Chakraborty has not dwelt on nepotism, it is one underlying fact that comes out loud and clear; if Suraj was the son of a well-known actor, producer or director... his life would not have been a struggle. Breaks would have come by the dozen! Even after flops... but that would be a script for another film. Maybe Part II.
Rahil Tandon plays the role of a fresh MBA with his eyes on 70mm with ease. You can easily identify with his character. Although his performance is not powerful, he makes you believe in his character and takes you through his frustration. Bhavana Ruparel as Merlena, on the other hand is a powerful performance. She is in sync with the role she plays. A talent to watch out for. Absolutely comfortable with any scene she is asked to perform with a terrific body language.

Director Pritish Chakraborty has most of the nuances of this plot in tune. The bit struggle, the frustration, the angst of his parents, the indifference of the industry and the blatant plagiarism are dealt with well. A song in particular sensitizes the way the industry works today with lyrics that go thus... Original ka gaya zamana, sab copy paste hai. Ctrl C, Crtl V." 

== References ==
 

==External links==
*  
 
*  
 

 
 
 
 
 
 
 
 
 
 