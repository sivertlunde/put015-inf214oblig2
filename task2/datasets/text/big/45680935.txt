Tommy Atkins (1915 film)
{{Infobox film
| name           = Tommy Atkins 
| image          =
| caption        =
| director       = Bert Haldane 
| producer       = 
| writer         = Ben Landeck (play)   Arthur Shirley (play)   Rowland Talbot 
| starring       = Blanche Forsythe   Jack Tessier   Roy Travers 
| cinematography = 
| editing        = 
| studio         = Barker Films
| distributor    = I.C.C. 
| released       = January 1915
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent war play of the same title by Ben Landeck and Arthur Shirley. 

==Cast==
*    Blanche Forsythe as Ruth Raymond  
* Jack Tessier as The Curate 
* Roy Travers as Captain Richard Maitland 
* Maud Yates as Rose Selwyn 
* Barbara Rutland  

==References==
 

==Bibliography==
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999.

==External links==
* 

 

 
 
 
 
 
 
 

 