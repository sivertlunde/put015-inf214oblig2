Pale Rider
 
 
{{Infobox film
| name           = Pale Rider
| image          = Pale_Rider.jpg
| caption        = Theatrical release poster
| director       = Clint Eastwood
| writer         = Michael Butler Dennis Shryack
| starring = {{Plainlist|
* Clint Eastwood
* Michael Moriarty
* Carrie Snodgress Christopher Penn
* Richard Dysart
* Sydney Penny
* Richard Kiel
* Doug McGrath John Russell
}}
| producer       = Clint Eastwood
| music          = Lennie Niehaus
| cinematography = Bruce Surtees
| editing        = Joel Cox
| studio         = The Malpaso Company
| distributor    = Warner Bros.
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = $6.9 million  
| gross          = $41,410,568  
}} western film produced and directed by Clint Eastwood, who also stars in the lead role. This movie bears a striking similarity to the classic Western Shane (film)|Shane, as well as similarities to Eastwoods earlier film High Plains Drifter. The title is a reference to the Four Horsemen of the Apocalypse, as the rider of a pale horse is Death.

==Plot==
The film opens in the countryside outside the fictional town of Lahood, California, and takes place sometime in the 1870s or early 1880s (based on remarks in the film about outlawing hydraulic mining). Thugs working for big-time miner Coy LaHood ride in and destroy the camp of a group of struggling miners and their families who have settled in nearby Carbon Canyon and are panning for gold there. In leaving, they also shoot the little dog of fourteen-year-old Megan Wheeler. As Megan buries it in the woods and prays for a miracle, we see a stranger heading to the town on horseback.

Megans mother, Sarah, is keeping company with Hull Barret, the leader of the miners. Hull heads off into town to pick up supplies, where the same thugs start to beat him up and he is rescued by the stranger, who takes the men on single-handedly. Hull invites him to his house, and while Eastwoods character is washing, Hull notices what looks like six bullet wounds to his back. Shortly after, he appears wearing a clerical collar and thereafter is called Preacher.
 Marshal named Stockburn to clear them out.

The miners initially want to take the offer but, when Hull reminds them why they came and what they have sacrificed, they decide to stay and fight. The next morning, however, the Preacher disappears. Megan, who has grown fond of the Preacher, heads out looking for him, but Josh captures and attempts to rape her, while his cohorts look on and encourage him. Club sees what is happening and moves forward to help her before Josh can do anything serious. At this moment the Preacher arrives on horseback armed with a Remington Model 1858 revolver he has recovered from a Wells Fargo office and, after shooting Josh in the hand when he goes for his gun, takes Megan back to her mother in the mining camp.

Stockburn arrives at the town and he and his men gun down Spider, one of the miners, who was drunkenly insulting LaHood from the street. LaHood describes the Preacher to Stockburn, who says that he sounds like someone that he once knew, but that he couldnt be, since that man is dead.

Preacher teams up with Hull and they go to LaHoods strip mining site and blow it up with dynamite. To stop Hull from following him, Preacher then scares off Hulls horse and rides into town alone. In the gun fight that follows he kills all but two of Coys thugs and then, one by one, Stockburn’s men as they hunt for him through the town. Finally he shoots it out with Stockburn, too, who recognizes him in disbelief before Preacher guns him down. Coy LaHood, watching from his office, aims a rifle at Preacher, only to be killed by Hull coming in through a back door.

Preacher rides his horse out of a barn and by way of thanks remarks to Hull "Long walk" before riding off into the snow-covered mountains. Megan then drives into town and shouts her love and thanks after him. The words echo along the ravine that he is traversing.

==Cast==
 
*Clint Eastwood as "Preacher"
*Michael Moriarty as Hull Barret
*Carrie Snodgress as Sarah Wheeler
*Richard Dysart as Coy LaHood
*Chris Penn as Josh LaHood
*Sydney Penny as Megan Wheeler
*Richard Kiel as Club
*Doug McGrath as Spider Conway
*Jeffrey Weissman as Teddy Conway John Russell as Marshal Stockburn
*S. A. Griffin as Deputy Folke
*Billy Drago as Deputy Mather
*Charles Hallahan as McGill
*Marvin J. McIntyre as Jagou
*Fran Ryan as Ma Blankenship Richard Hamilton as Pa Blankenship
*Terrence Evans as Jake Henderson

==Production== Boulder Mountains Sun Valley Sawtooth Mountains south of Stanley, Idaho|Stanley. Train-station scenes were filmed in Tuolumne County, California, near Jamestown. Scenes of a more established Gold Rush town (in which Eastwoods character picks up his pistol at a Wells Fargo office) were filmed in the real Gold Rush town of Columbia, also in Tuolumne County, California. 

==Religious themes==
In an audio interview, Clint Eastwood said that his character Preacher "is an out-and-out ghost". {{cite web | last =  | first =  | authorlink =  | title = Clint Eastwood.net Filmography / Pale Rider
  | work =  | publisher =  | date =
  | url = http://www.clinteastwood.net/filmography/palerider
  | format =  | doi =  | accessdate = 2008-02-12 }}  But whereas High Plains Drifter resolves its story-line by means of a series of unfolding flash back narratives (although ambiguity still remains), Pale Rider does not include any such obvious clues to the nature and past of the Preacher. One is left to draw ones own conclusions regarding the overall story line and its meaning.

The movies title is taken from The Book of Revelation, chapter 6, verse 8: "And I looked, and behold a pale horse: and his name that sat on him was Death, and Hell followed with him." The reading of the biblical passage describing this character is neatly choreographed to correspond with the sudden appearance of the Preacher, who arrived as a result of a prayer from Megan, in which she quoted   teaching on divine retribution (Romans 12:19-21). 

==Similarities to Shane==
The plot contains many similarities to the 1953 film Shane (film)|Shane.  Both films center on a community of laborers united against a gang of crooked entrepreneurs trying to control and take advantage of them.  In both, a stranger suddenly appears who no one knows, but who is an experienced gunfighter and sympathizes with the townspeople against the gang.  In both, the protagonists child idolizes the stranger, and his wife is attracted to him.  In both, the gang kills a member of the community who confronts them; and, the stranger ultimately avenges the town.  In both, the stranger rides off with the child calling after him.

== Reception ==
Pale Rider was released in the United States in June, 1985, and became the highest grossing western of the 1980s.  It was the first mainstream Hollywood western to be produced after the massive financial failure of Heavens Gate (film)|Heavens Gate. The film received positive reviews, and currently holds a 92% score on Rotten Tomatoes. Roger Ebert praised the film, giving it four out of four stars.  , Roger Ebert, June 28, 1985. 

The movie was a success at the North American box office, grossing $41,410,568  against a $6,900,000 budget.  

The film was entered into the 1985 Cannes Film Festival. Hughes, p.38    

 

==References==
Notes
 

Bibliography
* 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 