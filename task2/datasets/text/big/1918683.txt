Return of the Killer Tomatoes
 
{{Infobox film
| name           = Return of the Killer Tomatoes!
| image          = Returnofthekillertomatoes.jpeg
| caption        = Theatrical release poster.
| director       = John De Bello
| producer       = Stephen Peace
| writer         = John De Bello Costa Dillon Stephen Peace Karen Mistal Anthony Starke George Clooney
| music          = Neal Fox Rick Patterson
| cinematography = Victor Lou Stephen Welch
| editing        = John De Bello
| distributor    = New World Pictures
| released       =  
| runtime        = 98 min
| country        = United States
| language       = English
| budget         =
}}

Return of the Killer Tomatoes! (1988), a horror comedy, was the first sequel to Attack of the Killer Tomatoes.

==Synopsis==
Set 10 years after the events of Attack of the Killer Tomatoes (referred to as the Great Tomato War), the United States is once again safe, and tomatoes have been outlawed (although authorities still deal with "tomato smugglers" who sell to people who cannot live without ordinary tomatoes). Wilbur Finletter (Steve Peace) has been praised as a hero of the Great Tomato War and parlayed his fame into opening Finletters Pizzeria, which serves tomato-less pizzas. Working for Wilbur is his nephew Chad Finletter (Anthony Starke) who is a delivery boy. Also with Chad is his roommate Matt Stevens (George Clooney), a suave ladies man.
 Karen Mistal), who serves as Gangreens assistant until she realizes his abusive attitude towards a wrongly mutated tomato whom she dubs FT, or Fuzzy Tomato. Tara defects to Finletters Pizza where she starts dating Chad.

==Cast==
*Anthony Starke as Chad Finletter
*George Clooney as Matt Stevens Karen Mistal as Tara Boumdeay
*Steve Lundquist as Igor
*John Astin as Professor Gangreen
*J. Stephen Peace as Wilbur Finletter
*Michael Villani as Bob Downs
*Frank Davis as Sam Smith
*Harvey Weber as Sid Charlie Jones as the Sportscaster
*John De Bello as Charles White
*Ian Hutton as Greg Colburn
*Rick Rockwell as Jim Richardson/Tomato Dealer

==Production== Fifth Symphony; Gone with the Wind restores her to human form.

The characters and timeline of this film were recycled for the   (with some changes, such as Professor Gangreens first name being changed to Putrid and his title being changed from Professor to Doctor and Tara having an allergy to salt which changes her back into a tomato with pepper changing her back to human form).

Return of the Killer Tomatoes! also has the distinction of being one of George Clooneys earliest movies.
 National City. Chula Vista in what is now known as Don Pedros Taco Shop.

A trailer to the film shows up on some VHS copies of Twister (1996 film)|Twister.
==Art== branding concerns, and a "patch" was created to alter the work. High-end digital compositing was too expensive at the time, so Darrow was further commissioned to create the new replacement label which was physically glued over the original illustration, preserving the original background and tomato characters. Darrow still retains the original, 2-layer artwork.

==External links==
* 
*  

 

 
 
 
 
 
 