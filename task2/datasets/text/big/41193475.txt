Trancers: City of Lost Angels
 
Trancers: City of Lost Angels (also known as Trancers 1.5) is a short science fiction film that was released on DVD in 2013 and later a bonus addition for other releases of the original Trancers. It consists of approximately 30 minutes of stock footage lifted from the unreleased 1988 anthology movie, Pulse Pounders, which also featured two other segments. It stars Tim Thomerson who reprises his role as the time journeying cop, Jack Deth, from 1985s Trancers.  The film is marketed as a digitally restored lost sequel. The film takes place between Trancers and Trancers II. 

==Plot==
Jack Deth, the super cop from the future, has put away three centuries worth of time traveling criminals. But Deth’s most dangerous collar, the ultraviolent assassin Edlin Shock (Velvet Rhodes), has escaped from her maximum security holding cell and won’t rest until she’s exacted revenge. Meanwhile, Deth is trying to make a life for himself as a private eye in 1988 Los Angeles with his hot-blooded girlfriend Lena (played by Academy Award winning actress Helen Hunt). Relationship troubles are just the beginning of Deth’s problems when he learns that Edlin Shock has followed him back in time. Aided by his former police chief McNulty, whose consciousness is inhabiting the body of a 13-year-old girl, Jack Deth will have to use his wits, as well as his fists, to save the past, present, and future!

==References==
 

 