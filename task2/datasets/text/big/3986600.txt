Rock the Night: Collectors Edition
 
{{Infobox film
| name        = Rock the Night: Collectors Edition
| image       = Rock the Night DVD.jpg
| director    =
| producer    =
| writer      = Europe
| Europe
| distributor = Sony Music
| released    =  
| runtime     = 90 min
| language    = English, Swedish
| imdb_id     =
| music       =
| awards      =
| budget      =
}}
 Swedish hard rock band Europe (band)|Europe. It features the music videos the band made from 1986 to 1992. The bonus material is live footage and interviews from Swedish TV. DVD format is DVD9.

The DVD was released as Rock the World in North America.

==Track listing== The Final Countdown"  Rock the Night" 
# "Carrie (song)|Carrie" 
# "Cherokee (Europe song)|Cherokee" 
# "Superstitious (song)|Superstitious"  Open Your Heart" 
# "Let the Good Times Rock"  Prisoners in Paradise" 
# "Ill Cry for You" Halfway to Heaven"

==Bonus Features==
*Rock-SM - Final 1982: "In the Future to Come" and "The King Will Return" (Live) + Award ceremony at Tyrol, Stockholm.
*Casablanca - 1983: Interview with the band in the studio.
*Bagen / Rocksugen - 1984: "Scream of Anger", "Ninja", "Dreamer" and "Memories" (Live) at the club Studion in Stockholm.
*Rapport - 1986: Tour report from Örebro, Sweden. 
*Ritz - 1988: Report from the US tour with Def Leppard.

 

 
 