A Very Moral Night
{{Infobox film
| name           = A Very Moral Night
| image          =
| caption        =
| director       = Károly Makk
| producer       =
| writer         = Péter Bacsó Sándor Hunyady István Örkény
| starring       = Margit Makay
| music          =
| cinematography = János Tóth
| editing        = György Sívó
| distributor    =
| released       =  
| runtime        = 110 minutes
| country        = Hungary
| language       = Hungarian
| budget         =
}}

A Very Moral Night ( ) is a 1977 Hungarian comedy film directed by Károly Makk. It was entered into the 1978 Cannes Film Festival.   

==Cast==
* Margit Makay - Aunt Kelepei
* Irén Psota - Mutter
* Carla Romanelli - Bella
* Georgiana Tarjan - Darinka (as Györgyi Tarján)
* György Cserhalmi - Jenő Kelepei
* Edith Leyrer - Nusika
* Mari Kiss - Rózsi
* Ildikó Kishonti - Karolina
* Zsuzsa Mányai - Girl
* Edit Soós - Girl
* Katalin Szécsi - Girl
* Ági Szirtes - Servant-maid
* Judit Törő - Girl
* András Ambrus - Major
* Lajos Balázsovits - Szabó
* Zoltán Basilides - Tivadar
* Zoltán Benkóczy - Chief Hunter
* Gyula Benkő - Deputy Homoródy
* László Csákányi - Mr. Kautsky

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 