Geet Gaya Patharon Ne
{{Infobox film
| name           = Geet Gaya Patharon Ne
| image          = Geet Gaya Patharon Ne.jpg
| image_size     = 
| caption        = 
| director       = V. Shantaram
| producer       = V. Shantaram
| writer         = 
| narrator       = 
| starring       = Jeetendra Rajshree
| music          = Ramlal
| singer         = Kishori Amonkar 
| cinematography = Krishnarao Vashirda
| editing        = 
| distributor    = 
| released       = 1963
| runtime        = 
| country        = India
| language       = Hindi 
| budget         = 
}}

Geet Gaya Patharon Ne (The Rocks Sang a Melody) is a 1963 film produced and directed by V. Shantaram. The film marked the debut of V. Shantarams daughter Rajshree and Jeetendra. The films music was composed by Ramlal who gave a number of melodious tunes to the songs penned by Hasrat Jaipuri.

==Plot==

Vijay is the son of a line of temple sculptors who has not touched his craft since he became a young man. Vijay lives an impoverished lifestyle along with his widower father who works as a guide for tourists. One day while guiding a group of young women, his father asks Vijay to assist them. He does so quite nonchalantly, and is corrected by one, who later identifies herself as Vidya. He is inspired by her beauty and makes several sculptures. Both continue to meet, fall in love, but face opposition from her mother, who is all set to sell her to wealthy Dhanraj. After meeting a girl, Vidya, while giving a tour of the monuments, his heart is enraptured by her image and this inspires a return to his sculpting. Unfortunately, her mother has been preparing her with dancing lessons in order to sell Vidyas life and virginity to a rich man who will take both of them into his palace to keep this girl as a part of his collection of pleasures. When the day comes for this decision to be revealed, Vidya escapes from home into the arms of Vijay. Although Vijays father is hostile toward meddling with these matters and has no confidence that Vidya could tolerate their lifestyle of poverty. With the help of Chacha and his father, Vijay and Vidya do get married, and he makes several sculptures.  One day, Vijay and his wife are approached by a patron from the big city who is looking to build a house for the sake of an eccentric gentleman who has lost his beloved daughter, a girl whose whereabouts remain unknown.

He is then asked by an armless sculptor to accompany him to Delhi, which he does. After several months he returns home to find Chacha has died, and Vidya, unable to live in poverty, has moved back with her mother and is now not only living with Dhanraj but also is the mother of a young male child.

==Awards==
* Filmfare Best Cinematographer Award: Krishnarao Vashirda

==Music==

The movie has music by Ramlal. Hasrat Jaipuri & Vishwamitra Adil have penned the lyrics.

{|border="2" cellpadding="3" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px # aaa solid; border-collapse: collapse; font-size: 95%"
|-bgcolor="#CCCCCC" align="center"
!Sr.No. !! Song!! Singer(s)!! Lyricists!! Music Director
|-
|1|| Saanson Ke Taar Par, Geet Gaya Pathro Ne (Solo) || Kishori Amonkar || Hasrat Jaipuri || Ramlal
|-
|2|| Saanson Ke Taar Par, Geet Gaya Pathro Ne (Duet) || Mahendra Kapoor & Asha Bhosle || Hasrat Jaipuri || Ramlal
|-
|3|| Ho Saath Agar, Mandave Tale Garib Ke || C. H. Atma || Hasrat Jaipuri || Ramlal
|-
|4|| Aa Ja, Janeja Mere Meharaban || Asha Bhosle || Hasrat Jaipuri || Ramlal
|-
|5|| Janewale O Mere Pyar || Asha Bhosle || Hasrat Jaipuri || Ramlal
|-
|6|| Aayiye Padhariye  || Mahendra Kapoor, Asha Bhosle & Corus || Vishwamitra Adil || Ramlal
|-
|7|| Raat Naujawan || Asha Bhosle || Hasrat Jaipuri || Ramlal
|-
|8|| Ek Pal Jo Mila Hai || C. H. Atma || Hasrat Jaipuri || Ramlal
|- Tere Khayalo me Hum  || Asha Bhosle Vishwamitra Adil || Ramlal
|}

== External links ==
*  

 
 

 
 
 
 


 