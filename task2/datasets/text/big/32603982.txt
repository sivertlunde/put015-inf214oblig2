Brother Officers
{{Infobox film
| name           = Brother Officers
| image          = 
| alt            =  
| caption        = 
| director       = Harold M. Shaw
| producer       = 
| writer         = Bannister Merwin Leo Trevor
| starring       = Henry Ainley Lettice Fairfax Gerald Ames Charles Rock
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = United Kingdom
| language       = 
| budget         = 
| gross          = 
}} British silent silent war film directed by Harold M. Shaw and starring Henry Ainley, Lettice Fairfax and Gerald Ames.  It was based on a play by Leo Trevor. A soldier wins the Victoria Cross during the First World War.

==Cast==
* Henry Ainley – John Hinds 
* Lettice Fairfax – Baroness Honour Royden 
* Gerald Ames – Lieutenant Lancelot Pleydell 
* Charles Rock – Jim Stanton  George Bellamy – Colonel Stapleton  Frank Stanmore – Dean 
* Wyndham Guise – Bookmaker 
* Gwynne Herbert – Lady Pleydell

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 