Buster's Bedroom
{{Infobox film
| name           = Busters Bedroom
| image          = 
| caption        = 
| director       = Rebecca Horn
| producer       = Aimée Danis Luciano Gloor Udo Heiland Bruce Marchfelder George Reinhart
| writer         = Rebecca Horn Martin Mosebach
| starring       = Donald Sutherland Geraldine Chaplin Amanda Ooms
| music          =Sergey Kuryokhin	 	
| cinematography = Sven Nykvist	
| editing        = Barbara von Weitershausen	 			
| distributor    = 
| released       = 1990
| runtime        = 104 min
| country        = Germany Canada Portugal
| language       = Filmed in several languages
| budget         =
| gross          =
}} German comedy film directed by Rebecca Horn. The film follows a young woman with an infatuation for Buster Keaton. The film was shown at the Marché du Film of the Cannes Film Festival in May 1990. Later that year it was shown at the Museum of Contemporary Art, Los Angeles together with Horns exhibition. The objects of the exhibition were connected to the film, as themes, character references and props.  The film was released in Germany on 9 May 1991.The film stars Amanda Ooms, Donald Sutherland and Geraldine Chaplin.

==Plot== Santa Barbara at Nirvana House, a large, mostly abandoned villa. The residents are a strange group of characters that appear to inhabit lives outside of real time. Nirvana House is also the location where Keaton himself frequented over his alcohol problems and was even detained in a straitjacket in the villa. Micha shares this history in common with her idol when she is also placed in a straitjacket but manages to escape.
 asexual beekeeper entranced by pollination. Other residents of the facility include a crazed pianist and a sailor-obsessed middle-aged gardener.

Another resident is the former prima donna, Serafina (Cortese), who keeps butterflies in her icebox, who frequent an area of the sanatorium known as the Pavilion of Love to play dramatic scenes of welcoming back the Legionnaire, Joe (Wuttke) from the war. The pair sleep together only for Serafina to reject Joe in the morning, doubting his valiance alongside those who died at war, and pulling a fake trigger to his head. However things become dangerous when reality overcomes the fantasy of Nirvana House. This is especially true when Diana develops very jealous feelings over Michas relationship with Dr. OConnor. 

==Cast==
*Amanda Ooms as Micha
*Donald Sutherland as Dr. OConnor
*Geraldine Chaplin as Diana Daniels
*Valentina Cortese as Serafina Tannenbaum
*David Warrilow as Mr. Warlock
*Taylor Mead as James
*Ari Snyder as Lenny Silver
*Martin Wuttke as Joe
*Mary Woronov as Jane
*Nina Franoszek as Sue

==Reception==
Michael Brenson of The New York Times described the film as "largely convincing" and that "In mood and style, it has a touch of Fellinis magic, a touch of Viscontis decadence and a touch of Bunuels liberating illogic. The films narrative weaves together the objects in the exhibition. Its flow defines the artists sensibility." Brenson, Michael. Buster Keaton Inspires a Spooky German Film. The New York Times. 4 November 1990 

The film was nominated for  .

==References==
 

==External links==
*  

 
 
 
 
 
 
 