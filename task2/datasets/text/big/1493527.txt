Timecop
{{Infobox film
| name           = Timecop
| image          = Timecopposter.jpg
| caption        = Theatrical release poster
| director       = Peter Hyams
| producer       = Moshe Diamant Sam Raimi Robert Tapert
| screenplay     = Mark Verheiden Mike Richardson Mark Verheiden
| based on       =   Mark Verheiden
| starring       = Jean-Claude Van Damme Mia Sara Ron Silver Gloria Reuben
| music          = Mark Isham Robert Lamm
| cinematography = Peter Hyams
| editing        = Steven Kemper Largo Entertainment JVC Entertainment Dark Horse Entertainment Renaissance Pictures Universal Pictures   Warner Home Video  
| released       =  
| runtime        = 98 minutes
| language       = English
| country        = United States
| budget         = United States dollar|US$27 million  |
| gross          = $101,646,581
}}
 Mike Richardson Time Cop, Phil Hester and Chris Warner which appeared in the anthology comic Dark Horse Comics, published by Dark Horse Comics.
 Federal agent in 2004, when time travel has been made possible. It also stars Ron Silver as a rogue politician and Mia Sara as the agents wife. The story follows an interconnected web of episodes in the agents life (or perhaps Parallel universes|lives) as he fights time-travel crime and investigates the politicians unusually successful career.

Timecop remains Van Dammes highest grossing film (his second to break the $100 million barrier for a worldwide gross) as a lead actor. It is generally regarded as one of Van Dammes better films by critics.

==Plot== anachronistic machine pistols and futuristic tech equipment, leaving them stranded and dead before suddenly vaporizing into the mist.

131 years later, the U.S. government creates the Time Enforcement Commission (TEC) to combat the misuse of newly developed time travel technology. They have discovered that the same gold bullion was used in recent arms purchases. Senator Aaron McComb (Silver) volunteers to oversee the commission, and a short time later, police officer Max Walker (Van Damme) is offered a job as a TEC agent. Later that evening, Max is attacked in his home by intruders and his wife Melissa (Sara) is killed in an explosion.

Ten years later, Walker is now a veteran TEC Agent. He is sent back to 1929, in the midst of the Wall Street crash to arrest his former partner Atwood (Jason Schombing) for taking advantage of the U.S. stock-market crash. Atwood reveals that he is working for McComb, who needs money for his presidential campaign. Terrified by McCombs threat to murder his ancestors, thereby wiping out his existence, Atwood tries to kill himself by jumping out a window. Walker catches him as he falls and takes him back to 2004, but Atwood refuses to testify against McComb and the TEC agency sends him back to 1929, right where he left off, this time falling to his death.

Walker is partnered with agent Fielding (Gloria Reuben), and sent back to 1994, where they find a young Senator McComb arguing with Jack Parker about their companys new computer chip. Parker offers to buy McCombs share of the company, but suddenly, the older McComb arrives from 2004 to warn his younger self that the chip will make huge profits. A fight starts when Walker is double-crossed by Fielding, who reveals she works for McComb. McComb kills Parker, wounds Fielding, attempts to kill Walker, and manages to escape back to 2004.
 hijacks the original prototype time machine (which McComb had been using for their illegal trips) with the help of Matuzak, who sacrifices himself when McCombs men try to stop Walker from escaping.

Finding himself once more in 1994, Walker finds Fielding in the hospital, where she agrees to testify against McComb. Whilst trying to find Fieldings DNA from a blood sample in the lab, Walker finds a sample of Melissas blood and it indicates she is pregnant. Walker realizes her death occurred later that night, and he decides to stop it. After going back to Fieldings room, he discovers that she has been murdered and he is framed as the prime suspect. He goes to the mall where he and Melissa met that night. Eventually Max finds her and manages to convince her he is from the future, but does not tell her of her impending death.

That evening, McCombs thugs break into Walkers home, like before, only this time the older Walker is waiting for them. Without his younger self realizing it, the older Walker helps to fight off the thugs, though the younger Walker is wounded. McComb appears and takes Melissa hostage. He goes on to explain to Walker that he was behind Melissas murder, though the younger Walker was the actual target because the older Walker had been interfering with his plans. As before, McComb sets a time bomb; though he will also die in the explosion, his younger self will survive and go on to claim the Presidency when the time comes. The older Walker reminds McComb that he created a time flux by killing his wife before in the past, and the older Walker plans to set his familys history back on course. The young McComb then appears, having been tricked into coming by a false message sent by Walker. In the ensuing scuffle, McComb shoots Melissa, but Walker pushes the young McComb into the older one, causing them to become a writhing, screaming mass that melts into nothingness, destroying McComb once and for all. Walker barely manages to escape the house with Melissa before the explosion, and lays her body beside the young Walker.

Walker returns to 2004, where he finds the timeline has been changed once again, this time for the better. The TEC still exists, Fielding and Matuzak are alive, and McComb does not exist, having "vanished" ten years earlier. As Walker returns home, he is happily shocked to find Melissa alive and their 9-year-old son waiting to greet him. Melissa has something to announce to Walker, implying that she is pregnant again.

==Cast==
* Jean-Claude Van Damme as Max Walker
* Mia Sara as Melissa Walker
* Ron Silver as Sen. Aaron McComb
* Bruce McGill as Com. Eugene Matuzak
* Gloria Reuben as Sarah Fielding
* Scott Bellis as Ricky
* Jason Schombing as Lyle Atwood
* Scott Lawrence as George Spota
* Kenneth Welsh as Sen. Utley
* Brad Loree as Reyes Kevin McNulty as Jack Parker Gabrielle Rose as Jdg. Marshall
* Steven Lambert as Lansing

==Production==
Hyams later recalled:
 It wasn’t at all planned from the beginning that I would make two films with Jean-Claude Van Damme back-to-back. I was approached to do Timecop, and I loved the auspices. (Producer) Larry Gordon was involved with it; Moshe Diamant was a terrific producer; Sam Raimi was involved... It was a really clever story, and I thought it was a chance to make the best movie Van Damme ever made. I said yes and we made it, and it was clear that it was going to be a hit because it previewed through the roof every time. It’s still his biggest hit. So Universal and Moshe Diamant wanted to team us again as soon as possible, so they put Sudden Death together. There was never any question that we would just do Timecop 2. I would never have agreed to that. The last thing you want to do is repeat yourself. That would be awful.  
==Release==

===Box office===
Timecop was released on September 16, 1994, where it opened at the number 1 spot with $12,064,625 from 2,228 theaters and a $5,415 average per theater.   In its second week, it took the top spot again with $8,176,615.  It finished its run with $45 million in total U.S. overseas, it grossed about $57 million, with a total gross of $101 million. This makes it Van Dammes highest-grossing film in which he starred, and his second to make over $100 million (after Universal Soldier (1992 film)|Universal Soldier).

===Reception===
Critics were mixed on Timecop, citing its various plot holes and inconsistencies.  Roger Ebert called Timecop a low-rent Terminator.  Richard Harrington of The Washington Post said, "For once, Van Dammes accent is easier to understand than the plot." David Richards of The New York Times disparaged Van Dammes acting and previous films but called Timecop "his classiest effort to date".  Timecop currently holds a 43% rating and average rating 5.2/10 on Rotten Tomatoes based on 40 reviews with 17 fresh reviews and 23 rotten. The sites consensus is: "Its no Terminator, but for those willing to suspend disbelief and rational thought, Timecop provides limited sci-fi action rewards."

The film made Entertainment Weeklys "Underrated Films" list in November 2010, mostly because of Van Dammes acting. 

==Music==
The musical score of Timecop was composed by Mark Isham and conducted by Ken Kugler.

===Soundtrack===
Track listing
# "Time Cop" – 2:20
# "Melissa" – 2:41
# "Blow Up" – 2:12
# "Lasers and Tasers" – 4:23
# "Polaroid" – 6:10
# "Rooftop" – 6:16
# "C4" – 2:37
# "Rescue and Return" – 3:22

==Home media release==
Timecop was released on DVD in 1998. Two separate versions were released, a 2.35:1 anamorphic widescreen edition and a fullscreen edition.  The widescreen release is identified with the title on the front cover having green lettering, whereas the fullscreen is red and white.  

The DVD extras include production notes, a theatrical trailer and notes on the cast and crew.

By 2010, the rights to the film had reverted to Largo successor InterMedia, and distribution shifted to Warner Home Video. A Blu-ray of the film was released as a double feature for both this and Bloodsport (film)|Bloodsport from Warner Home Video on September 14, 2010, which has the full uncut 98-minute version in 2.35:1 widescreen, but no extra features.

==Spinoffs== a two-issue comic book series. game based SNES in 1995 in video gaming|1995. 
* A series of tie-in novels by author Dan Parkinson published in 1997–1999 featured the Jack Logan character from the television series. of the spun off, 1997 on American Broadcasting Company|ABC.  It starred Ted King (actor)|T.W. King as Jack Logan and Cristi Conaway as Claire Hemmings.
*  , a direct-to-DVD sequel was released in 2003 in film|2003, starring Jason Scott Lee and Thomas Ian Griffith, and directed by Steve Boyum. 
* In 2010, Universal announced a reboot of the film, with Marc Shmuger producing alongside Tom McNulty and Mark & Brian Gunn writing.   

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 