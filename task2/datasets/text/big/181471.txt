Lady Jane (film)
{{Infobox film
| name = Lady Jane
| image = Lady jane poster.jpg
| image_size = 
| caption = Cinema poster
| director = Trevor Nunn David Edgar Chris Bryant  (story) 
| starring = {{Plainlist|
* Helena Bonham Carter
* Cary Elwes
}} Stephen Oliver Peter Snell
| cinematography = Douglas Slocombe
| distributor = Paramount Pictures
| editing = Anne V. Coates
| budget = $8.5 million
| released = 7 February 1986
| runtime = 141 minutes
| country = United Kingdom
| language = English 
| gross =  $277,646 
|}} David Edgar, and starring Helena Bonham Carter as the titular character. It tells the story of Lady Jane Grey, the Nine Days Queen, on her reign and romance with husband Lord Guildford Dudley. The film features several members of The Royal Shakespeare Company.
 Tudor Rose.

==Plot summary== Edward VI, consumption and Reformation by Mary from John Dudley, Lord President of the Council and second only to the king in power, hatches a plan to marry his son, Lord Guildford, to Lady Jane Grey, and have the royal physician keep the young king Edward VI alive&mdash;albeit in excruciating pain&mdash;long enough to get him to name Jane his heir.

Jane is not happy with the proposed marriage, and must be forced into it through corporal punishment by her parents. At first Jane and Guildford decide to treat their union purely as a marriage of convenience, but then they fall deeply in love.

After Edward VI dies, Jane is placed on the throne. She is troubled by the questionable legality of her accession, but after consulting with Guildford, turns the tables on John Dudley and the others who thought to use her as a puppet.

After only nine days, however, Queen Jane is abandoned by her council precisely because of her reformist designs for the country. The council then supports Mary I of England|Mary, who at first imprisons Jane and Guildford.
 Thomas Wyatts rebellion. When the rebellion fails, Queen Mary I offers to spare Janes life if she renounces her Protestant faith. When she refuses, Jane, her father and Guildford are all executed.

==Cast==
*Helena Bonham Carter as Lady Jane Grey
*Cary Elwes as Lord Guildford Dudley, Janes husband Queen Mary
*Patrick Stewart as Henry Grey, 1st Duke of Suffolk, Janes father
*Sara Kestelman as Lady Frances Brandon, Janes mother
*Michael Hordern as Doctor Feckenham John Wood as John Dudley, 1st Duke of Northumberland Jill Bennett as Mrs. Ellen, Lady-in-Waiting
*Adele Anderson as Lady Warwick King Edward VI Morgan Sheppard as Executioner

==Locations==
Dover Castle was used to represent the Tower of London in the film. Interior scenes of Hever Castle were used. The long gallery was used in the scene where Jane visits Queen Mary. The moat around Leeds Castle was used in the scene where Dudley first visits Lady Jane. 

==References==
 

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 