The Well (2010 film)
{{Infobox film
| name           = The Well
| image          = Vihir.jpg
| caption        = Film poster
| director       = Umesh Vinayak Kulkarni
| producer       = Amitabh Bachchan Corporation
| screenplay     = Girish Kulkarni, Satee Bhave
| starring       = Madan Deodhar Alok Rajwade Mohan Agashe Sulabha Deshpande Jyoti Subhash
| cinematography = Sudheer Palsane
| editing        = Neeraj Voralia
| released       =  
| runtime        = 135 minutes
| language       = Marathi
| country        = India
}} Marathi film directed by Umesh Vinayak Kulkarni which was released in March 2010  and was featured in Berlin film festival and Rotterdam International Film Festival 2010, the Netherlands.

This is AB Corp Limiteds first Marathi film.

==Storyline==
Vihir is a story of two adolescent boys Sameer and Nachiket (cousins, who are best friends) who struggle with their unconventional outlooks on life and family. They write to each other telling each other about their doings and happenings in life. They get to meet after a long time as Sameer with his mother and sister visit Nachikets village for a family wedding. 

Sameer and Nachiket both observe people in the house and try to understand their behaviour as they are not used to complexity of family relationships. Meanwhile, Sameer finds Nachikets lonely and cut-off attitude towards the family quite strange and when he confronts Nachiket about it, Nachiket tells him about his outlook towards life which confuses Sameer even more. Both of them share interest in swimming in the well and playing hide and seek. While swimming in the well, Nachiket always uses a wooden support as he doesnt know how to swim while Sameer is going to represent his school in swimming competition.

With swimming trials scheduled, Sameer has to leave for his home. When he returns after two days, he comes to know that Nachiket drowned in the well and is no more. Sameer, appalled at the sudden death of his best friend, is unable to accept Nachikets death and then starts Sameers journey in search of truth. Deep inside, he believes that Nachiket has hidden himself somewhere, like he used to do when they played hide and seek; and wants Sameer to seek him.

Sameer looks everywhere for Nachiket but fails. At last he remembers what Nachiket had told him about the hiding and seeking approach. He runs to the well and finds Nachiket there. They laugh and Sameer says that he has finally found him. It is inferred that Nachiket is dead but Sameer understands this fact after he completes his journey and understands that it is through the eyes of heart, one could find and remember the one who is not with us physically. The story ends with Sameer going back to his home in a bus, satisfied.

==Cast==
* Sameer: Madan Deodhar
* Nachiket: Alok Rajwade
* Ajoba (Grandfather) : Mohan Agashe
* Aaji (Grandmother): Jyoti Subhash
* Bau-Aaji : Sulabha Deshpande
* Sulabha : Renuka Daftardar
* Shobha : Ashwini Giri
* Bhavasha Mama (uncle) : Girish Kulkarni
* Prabha: Amruta Subhash
* Seema  : Veena Jamkar
* Tayadi : Parna Pethe
* Soni: Sharavi Kulkarni
* Ashu : Aditya Ganu
* Anshu : Ajinkya Ganu
* Sameers Father : Kiran Yatnopavit
* Nachikets Father: Abhay Godse
* Pickpocket: Shrikant Yadav

==Selection in festivals==
* Pusan International Film Festival 2009. 
* 8th Pune International Film Festival, Pune, India
* Kerala International Film Festival 2009
* Berlin International Film Festival 2010  
* Rotterdam International Film Festival 2010
* 26th Warsaw International Film Festival (2010)

==Awards and achievements==
* Best Director: Umesh Kulkarni, Best Cinematographer: Sudhir Palsane (PIFF 2010)
* Best Sound Design Anthony B J Ruban, Best Cinematographer: Sudhir Palsane (Zee gaurav Awards 2010)

==Sources==
*  
*  
*  

==References==
 

==External links==
*  
*  
*  

 
 
 
 