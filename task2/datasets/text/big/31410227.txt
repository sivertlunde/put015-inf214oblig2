Makkal
{{Infobox film
| name = Makkal
| image =
| caption =
| director = KS Sethumadhavan
| producer = MO Joseph
| writer = EP Kurian Parappurathu (dialogues)
| screenplay = KS Sethumadhavan
| starring = Jayabharathi Kaviyoor Ponnamma Adoor Bhasi Jose Prakash
| music = G. Devarajan
| cinematography = Balu Mahendra
| editing = MS Mani
| studio = Manjilas
| distributor = Manjilas
| released =  
| country = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film, directed by KS Sethumadhavan and produced by MO Joseph. The film stars Jayabharathi, Kaviyoor Ponnamma, Adoor Bhasi and Jose Prakash in lead roles. The film had musical score by G. Devarajan.   

==Cast==
  
*Jayabharathi 
*Kaviyoor Ponnamma 
*Adoor Bhasi 
*Jose Prakash 
*Sankaradi 
*Bahadoor 
*MG Soman 
*Mallika Sukumaran 
*Satheesh Sathyan
 

==Soundtrack==
The music was composed by G. Devarajan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aadathe Srishtichu || Jayachandran, CO Anto, Sreekanth || Vayalar || 
|- 
| 2 || Chellam Chellam || P Madhuri || Vayalar || 
|- 
| 3 || Raam banaaye || Vani Jayaram, Chorus || Rajbal Devaraj || 
|- 
| 4 || Sreerangapattanathil || K. J. Yesudas || Vayalar || 
|}

==References==
 

==External links==
*  

 
 
 


 