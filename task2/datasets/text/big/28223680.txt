Fracchia la belva umana
 
{{Infobox film
| name           = Fracchia la belva umana
| image          = Fracchia la belva umana.jpg
| caption        = DVD cover
| director       = Neri Parenti
| producer       = Bruno Altissimi, Claudio Saraceni
| writer         = Leo Benvenuti, Piero De Bernardi, Paolo Villaggio, Neri Parenti
| starring       = Paolo Villaggio, Lino Banfi, Gigi Reder, Anna Mazzamauro, Massimo Boldi
| music          = Fred Bongusto
| cinematography = Alberto Spagnoli
| editing        = Sergio Montanari
| distributor    = Cecchi Gori Group
| released       =  
| runtime        = 99 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Fracchia la belva umana (Fracchia the fanatic or Fracchia The Human Beast) is a 1981 Italian comedy film directed by Neri Parenti. It was shown as part of a retrospective on Italian comedy at the 67th Venice International Film Festival.    In the cast includes Paolo Villaggio, Lino Banfi and the then young Massimo Boldi and Francesco Salvi.

==Plot==
The misfortune of Giandomenico Fracchia (an employee in a chocolate factory) is to be the double of the terrible criminal known as "the human beast": this will give rise to a series of problems and misunderstandings.

==Cast==
* Paolo Villaggio - Giandomenico Fracchia / La Belva Umana
* Lino Banfi - Commissario Auricchio
* Anna Mazzamauro - Miss Corvino
* Francesco Salvi - Neuro
* Sandro Ghiani - De Simone
* Antonio Allocca - Carabinieris Marshal
* Jole Silvani - Palmira (as Iole Silvani)
* Fiammetta Baralla - chubby girl doing jogging
* Ugo Bologna - bank director
* Giulio Farnese - afterworlds gatekeeper
* Renzo Rinaldi - Carabinieris Colonel
* Renato Cecchetto - DIGOS Chief
* Roberto Della Casa - Tino
* Massimo Boldi - Pera
* Gigi Reder - The Human Beasts mother

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 