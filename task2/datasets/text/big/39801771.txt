Radio Magic
{{Infobox film
| name           = Radio Magic
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Oswald 
| producer       = Richard Oswald Paul Morgan   Nino Ottavi
| narrator       = 
| starring       = Werner Krauss   Xenia Desni  Fern Andra   Alfred Braun
| music          = 
| editing        =
| cinematography = Axel Graatkjær
| studio         = Richard-Oswald-Produktion
| distributor    = Deutsch-Nordische Film
| released       = 30 September 1927
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
|  German silent silent comedy film directed by Richard Oswald and starring Werner Krauss, Xenia Desni and Fern Andra. The films art direction was by Gustav A. Knauer and Willy Schiller. It premiered on 30 September 1927. 

==Cast==
* Werner Krauss as Theophil Schimmelpfenning, der Funkaugust 
* Xenia Desni as Gerda Reiner, Stenotypistin 
* Fern Andra as Melitta Mirabella, Seiltänzerin 
* Alfred Braun as Sprecher des Berliner Rundfunks 
* Margarete Kupfer as Frau Reiner 
* Gerd Briese as George Lenz, Gerdas Bräutigam 
* Anton Pointner as Herr Haßdorf, Erfinder 
* Leo Peukert as Max Bügeleisen 
* Ellen Plessow as Victoria Fadenschein, Bügeleisens Hausdame 
* Fritz Kampers as Schupo

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.
* Weniger, Kay. Es wird im Leben dir mehr genommen als gegeben ... Lexikon der aus Deutschland und Österreich emigrierten Filmschaffenden 1933 bis 1945. ACABUS Verlag, 2011.

==External links==
* 

 

 
 
 
 
 
 
 


 
 