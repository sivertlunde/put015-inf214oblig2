Bongoland
{{Infobox film
| name           = Bongoland
| image          = Bongoland poster.jpg
| image_size     = 
| caption        = Movie poster
| director       = Josiah Kibira
| producer       = Cully Gallagher Onesmo Kibira
| writer         = Josiah Kibira
| narrator       = 
| starring       = Mukama Morandi Laura Wangsness
| music          = Justine Kalikawe Innocent Mfalingundi Jeff Green
| editing        = Takaaki Sato
| distributor    = Kibira Films
| released       = September 9, 2003
| runtime        = 105 mins
| country        = United States Tanzania Swahili
| budget         = $20,000 (estimated)
| preceded_by    = 
| followed_by    = 
}} American / Tanzanian film directed by Josiah Kibira. It tells the story of an illegal Tanzanian immigrant living in the United States.

==See also==
* List of American films of 2003
* African cinema

==External links==
*  
*  
*  

 
 
 
 
 

 
 