Tunes of Glory
 
 
{{Infobox film
|  name           = Tunes of Glory
|  image          = Tunes of glory76.jpg 
|  image_size     = 225px
|  caption        = theatrical poster
|  director       = Ronald Neame
|  producer       = Colin Lesslie
|  writer         = James Kennaway (novel & screenplay)
|  starring       = Alec Guinness John Mills
|  music          = Malcolm Arnold
|  cinematography = Arthur Ibbetson
|  editing        = Anne V. Coates
|  distributor    = United Artists (UK) Lopert Pictures (US)
|  released       = 20 December 1960 (US)
|  runtime        = 106 minutes
|  country        = United Kingdom
|  language       = English
|  budget         =
|  gross          =
}} Scottish Highland John Fraser, Duncan MacRae Gordon Jackson.
 bagpiping that accompanies every important action of the regiment. The original pipe music was composed by Malcolm Arnold, who also wrote the music for The Bridge on the River Kwai.  The film was generally well received by critics, the acting in particular garnering praise. Kennaways screenplay was nominated for an Academy Award.

==Plot== mess of Major Jock Commanding Officer. Sinclair, who had been in command since the battalions colonel was killed in action during the North African campaign in Second World War, is to be replaced by Lieutenant Colonel Basil Barrow (John Mills). Although Major Sinclair led the battalion through the rest of the war, Brigade HQ considered Barrow to be a more appropriate peacetime commanding officer.
 Barlinnie Prison Japanese POW Japanese but does not tell this to Sinclair who privately resents the fact that he is being replaced by a "stupid wee man".

Meanwhile Morag (Susannah York), Sinclairs daughter, is observed illicitly meeting an enlisted bagpipes|piper.

Barrow immediately passes several orders designed to instil discipline in the battalion that Sinclair had allowed to slip. Particularly controversial is an order that all officers take lessons in Highland dancing in an effort to make their customary rowdy style more formal and suitable for mixed company. However the unchanged energetic dancing of the officers, led by a drunken Sinclair at Barrows first cocktail party with the townspeople, incites his anger. An outburst by Barrow, one that seems to reflect having been psychologically weakened by his experience of torture, only further damages his own authority.
 NCO and deserved to be court-martialled. The decision further undermines his authority, Sinclair along with other officers, notably Captain Alec Rattray (Richard Leech), treat him with a renewed lack of respect. In a scene remarkable for its depiction of glacial cruelty, Barrow is then told by Major Scott that other senior officers believe it is Sinclair who is really running the battalion, because he forced Barrow to dismiss the charges against him. Realising that his authority will never be accepted, Barrow shoots himself in the head.

With the colonels death, Sinclair realises he is to blame. He calls the officers to a meeting and announces plans for a grandiose funeral fit for a field marshal, complete with a march through the town in which all the "tunes of glory" will be played by the pipers. When it is pointed out how out disproportionate the plans are to the circumstances, especially given the manner of the colonels death, Sinclair insists that it was not suicide but murder! He tells everyone he himself was the murderer and the other senior officers were his accomplices with the exception of the colonels adjutant.

Minutes later, Sinclair himself suffers a nervous breakdown and is escorted from the barracks while the officers and men salute as he passes.

==Cast==
 
  MM
*John Mills as Lieutenant Colonel Basil Barrow MC & Bar
*Kay Walsh as Mary Titterington John Fraser as Corporal Piper Ian Fraser
*Susannah York as Morag Sinclair Gordon Jackson as Captain Jimmy Cairns, MC Duncan MacRae as Pipe Major Maclean Percy Herbert as Regimental Sergeant Major Riddick
*Allan Cuthbertson as Captain Eric Simpson
 
*Paul Whitsun-Jones as Major Dusty Miller
*Gerald Harper as Major Hugo MacMillan
*Richard Leech as Captain Alec Rattray
*Peter McEnery as 2nd Lieutenant David MacKinnon
*Keith Faulkner as Corporal Piper Adam
*Angus Lennie as Orderly Room Clerk John Harvey as Sergeant Finney
*Andrew Keir as Lance Corporal Campbell
*Jameson Clark as Sir Alan
*Lockwood West as Provost
 
;Cast notes
*According to an article in the New York Times  Alec Guinness wanted to play the role of Barrow, and John Mills wanted to play Sinclair &ndash; both initially turned down the film for those reasons. It took a meeting between Guinness, Mills and director Ronald Neame to straighten out why each was best suited for the role they had been offered.  However, in his autobiography, John Mills claimed that he brought the script to Guinness, and between them they decided who should play which role.  Guinness believed this performance to be among his best.
*Tunes of Glory was Susannah Yorks film debut. Her opening screen credit reads "and Introducing". TCM   

==Production==
Tunes of Glory was shot at Shepperton Studios in London. Establishing location shots were done at Stirling Castle in Stirling, Scotland. Stirling Castle is the Regimental Headquarters of the Argyll and Sutherland Highlanders  which was the actual location but in fact James Kennaway served with the Gordon Highlanders. Although the production was initially offered broad co-operation to film within the castle from the commanding officer there, as long as it didnt disrupt the regiments   routine, after seeing a lurid paperback cover for Kennaways book, that co-operation evaporated, and the production was only allowed to shoot distant exterior shots of the castle. 
 The Horses Mouth (1958), and a number of other participants were also involved in both films, including actress Kay Walsh, cinematographer Arthur Ibbetson and editor Anne V. Coates. 

==Awards and honours== Academy Award Best Adapted Elmer Gantry. It also received numerous BAFTA nominations, including Best Film, Best British Film, Best British Screenplay and Best Actor nominations for both Guinness and Mills. 

The film was the official British entry at the 1960 Venice Film Festival, and John Mills won the Best Actor award there.  That same year the film was named "Best Foreign Film" by the Hollywood Foreign Press Association. 

==Adaptations==
Tunes of Glory was adapted for the stage by Michael Lunney, who directed a production of it which toured England in 2006.  

==Home video==
Tunes of Glory is available on DVD from Criterion and Metrodome.

==References==
;Notes
 

==External links==
*  
*  
*  
* Murphy, Robert  , Criterion Collection essay

 

 
 
 
 
 
 
 
 