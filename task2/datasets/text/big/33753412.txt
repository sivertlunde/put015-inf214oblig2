The Eternal Jew (1934 film)
{{Infobox film
| name           = The Eternal Jew
| caption        =
| director       = Maurice Elvey
| producer       = Julius Hagen
| writer         = H. Fowler Mear
| starring       = Conrad Veidt
| music          = Hugo Riesenfeld
| cinematography = Sydney Blythe
| editing        = Jack Harris
| studio         = Julius Hagen Productions
| distributor    = Gaumont British Distributors (1933) (UK)
| released       =  
| runtime        = 111 minutes
| country        = United Kingdom
| language       = English
}}

The Eternal Jew (Also known as The Wandering Jew) is a 1934 British fantasy drama film produced by the Gaumont Film Company|Gaumont-Twickenham Film Studios. It is the story of a Jew (played by Conrad Veidt) who is forced to wander the Earth for centuries because he rebuffed Jesus while he was carrying his cross. 

Other cast members included a very young Peggy Ashcroft, Francis L. Sullivan, and Felix Aylmer.

==Plot== titular characters epic journey. He is finally burnt at the stake by the Spanish Inquisition. As he burns, he is forgiven by God and finally allowed to die. The story bears a resemblance to the legend of the Flying Dutchman.

==Portrayal of Jews== antisemitic propaganda film produced by the Nazis in 1940, this film portrayed Jews in a favorable light as the victims of unjustified persecution throughout history, in the Spanish Inquisition, for example. The Nazi propaganda film, by contrast, was  intended as violently anti-Semitic. 

==References==
 

==External Links==
*   at The Internet Movie Database

 
 
 
 