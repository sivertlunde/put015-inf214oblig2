Hercules and the Conquest of Atlantis
{{Infobox film
| name           = Hercules at the Conquest of Atlantis
| image          = Hercatcwpos.jpg
| caption        = Original US film poster
| director       = Vittorio Cottafavi
| producer       = Achille Piazzi Pierre Benoît Vittorio Cottafavi Sandro Continenza Duccio Tessari Archibald Zounds Jr. (Nicolò Ferrari)
| narrator       = Leon Selznick (U.S. Version)
| starring       = Reg Park Fay Spain Ettore Manni Luciano Marin Laura Efrikian
| music          = Gino Marinuzzi Jr. Armando Trovajoli
| cinematography = Carlo Carlini
| editing        = Maurizio Lucidi
| distributor    = Woolner Brothers Pictures Inc.
| released       =  
| runtime        = 101 minutes
| country        = Italy / France
| awards         =
| language       = Italian
| budget         =
}}
Ercole alla conquista di Atlantide (English Translation: Hercules at the Conquest of Atlantis) is a 1961 Italian / French international co-production film directed by Vittorio Cottafavi and starring Reg Park in his film debut as Ercole/Hercules. It was originally released in Super Technirama 70.
 title design by Filmation.  As Hercules and the Captive Women, it was featured on a fourth season episode of Mystery Science Theater 3000.

==Plot==
 Thebes seeks the assistance of his friend, the legendary Hercules. Hercules, now married to Deianira with a son named Hylas does not wish to leave the comfort of his family, though Hylas is keen for adventure.
 dwarf and Hylas who has to hide below deck from Hercules lest he face his wrath for leaving home.  Rather than being angry Hercules merely lazes away on deck without offering any assistance.
 mutinies by attacking Androcles but Hercules turns the tide.  The crew is left stranded on the island, however Hercules discovers Hylas on board.

Continuing their voyage, a storm wrecks the ship with all of them separated.  Hercules sees a vision of Androcles begging for his help.  Coming ashore on a mist shrouded island he sees a woman held captive not only chained to a cliff, but gradually becoming a part of the rock formation.  When he rescues her he has to fight Proteus a god able to change form into various creatures.  Defeating Proteus, Hercules discovers the woman he rescued is Princess Ismene, daughter of Antinea, the Queen of Atlantis that is where Hercules has landed.  Hercules soon discovers his son Hylas and Timoteo and bring the rescued Princess to Antinea.

Though welcomed at first, Ismene discovers that she has been selected for sacrifice as a prophesy foretells that if she is not killed Atlantis and its population will be destroyed. The death of Proteus has already stripped Atlantis of its protective fog that keeps it unseen by the outside world. Ismene is recaptured and taken for execution. Antinea denies all knowledge of Androcles, however Androcles memory has been taken away and he is hidden from his friends.  During a celebration Hercules discovers that children are selected and taken away from their parents for an unknown reason.
 supermen or disfigures the weak ones who are placed in the pit. A priest explains that the stone is from the blood of Uranus (mythology)|Uranus. The power of Uranus’s stone has created Antinea an invicible army of black uniformed blonde supermen that she plans to conquer the world with.

== Cast ==
*Reg Park as Ercole (Hercules)
*Fay Spain as Queen Antinea of Atlantis
*Ettore Manni as Androclo, Re di Tebe
*Luciano Marin as Illo
*Laura Efrikian as Ismene
*Enrico Maria Salerno as Re di Megara
*Ivo Garrani as Re di Megalia
*Gian Maria Volonté as Re di Sparta
*Mimmo Palmara as Astor
*Mario Petri as Zenith, prete di Urano
*Mino Doro as Oraclo
*Salvatore Furnari as Timoteo
*Alessandro Sperli
*Mario Valdemarin as Gabor
*Luciana Angiolillo as Deianira
*Maurizio Coffarelli as Proteus the Monster
*Nicola Sperli as Dyanaris
*Leon Selznick as Narrator of the U.S. Version

==Reception==
An imaginative entry in the sword and sandal genre, Cottafavi called it "Neo-Mythology". p.14 M. Winkler, Martin  Troy: from Homers Iliad to Hollywood Epic
Wiley-Blackwell, 2007   The film was made at the same time of George Pals Atlantis, the Lost Continent with the American release of the film dropping all reference to Atlantis in the films title.

Christopher Frayling noted the films slapstick violence at the beginning as a link to the Spaghetti Western;  Frank Burke in an essay in the book Popular Italian Cinema noted the films depiction of Nazi Germany and the atomic age  and Parks depiction of the Italian quality of "sprezzatura" in his portrayal of Hercules.

==Biography==
* 

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 