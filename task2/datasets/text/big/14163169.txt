Bcuz of U
{{Infobox Film
| name           = Bcuz of U
| image          = Bcuz-of-U-siap-tandatangan-Kristine-lagi.jpg
| image_size     =
| caption        = DVD cover with one of the casts signature.
| director       = John D Lazatin   Mae Cruz   Cathy Garcia-Molina
| producer       = Maricel Samson-Martinez
| writer         = 
| narrator       = 
| starring       = Heart Evangelista Sandara Park Kristine Hermosa Geoff Eigenmann Hero Angeles Diether Ocampo 
| music          = 
| cinematography = 
| editing        = Vito Cajili
| distributor    = Star Cinema
| released       = November 17, 2004 (Philippines)
| runtime        = 116 mins.
| country        = Philippines
| language       = English language|English/Tagalog
| budget         = 
| gross          =  
| preceded_by    = 
| followed_by    = 
}}

Bcuz of U is a Filipino romantic film, starring Kristine Hermosa, Diether Ocampo, Heart Evangelista, Geoff Eigenmann, Hero Angeles and Sandara Park. Bcuz of U is a 3 love story in 1 movie.

==Synopsis==
A runaway bride returns, a woman falls for her worst nightmare and two opposites come together in this trio of romantic tales. In the first story, Ria (Kristine Hermosa) tries to win back the man (Diether Ocampo) she left at the altar. In tale two, solitary Cara (Heart Evangelista) meets Mr. Wrong (Geoff Eigenmann) -- or is he? And in the final yarn, a tour guide (Hero Angeles) meets a girl (Sandara Park) who pretends to be a famous actress.

==Story==

===First story===
Ria (Kristine Hermosa) and RJ (Diether Ocampo) share the perfect relationship until she didnt show up on their wedding. Years after,she decided to win him back. But the indecisive and the new RJ is now the most sought-after model in town. Will their love prevail over the pain theyve caused each other?

===Second story===
Cara (Heart Evangelista) never took a chance on falling in love because she believes that it will only break her heart. That was until she meets Roni (Geoff Eigenmann) but only to find out that he is the kind of man shes been trying to avoid. Will she take the risk this time?

===Third story===
Louie (Hero Angeles), a tourist guide, meets April (Sandara Park), a Korean girl who got lost in the city and is left with no choice but to pretend that she is a famous actress in her hometown only to survive. Will it be possible for two very different individuals find home in one another?

==Cast==
*Kristine Hermosa as Ria 
*Diether Ocampo as RJ 
*Heart Evangelista as Cara 
*Geoff Eigenmann as Roni 
*Hero Angeles as Louie 
*Sandara Park as April 
*Nikki Valdez as Lee  
*Desiree Del Valle as Stella  
*Ilonah Jean   
*Luis Alandy
*Joshua Dionisio   
*Gerald Madrid   
*Aiza Marquez   
*Pen Medina   
*Susan Africa   
*Yayo Aguila

==Soundtrack== Because of Keith Martin, was performed by Gary Valenciano for the film.

== External links ==
*  
*  

 
 
 
 
 
 
 
 