To Live and Die in Tsimshatsui
 
 
 
{{Infobox film
| name           = To Live and Die in Tsimshatsui
| image          = ToLiveandDieinTsimshatsui.jpg
| alt            = 
| caption        = Film poster
| film name = {{Film name| traditional    = 新邊緣人
| simplified     = 新边缘人
| pinyin         = Xīn Biān Yuán Rén
| jyutping       = San1 Bin1 Jyun4 Jan4}}
| director       = Andrew Lau
| producer       = Wong Jing
| writer         = Roy Szeto Tony Leung Jacklyn Wu Power Chan Gigi Lai Roy Cheung Shing Fui-On
| music          = Jonathan Wong
| cinematography = Cheung Man Po
| editing        = Angie Lam
| studio         = Upland Films Corporate
| distributor    = Wong Jings Workshop
| released       =  
| runtime        = 100 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$9,192,000 
}} Tony Leung, Jacklyn Wu and Power Chan.

==Plot==
Crazy Lik and his friend Pong are two undercover cops who are sent to infiltrate triad leader Coffin sing. Later as sing is killed, the gang is split into two. Lik follows Hung Tais side and Pong goes to the other. Lik becomes down as he is separated from his best friend and feeling responsible for Sings death. This leads to a time where Lik gets drunk at his girlfriend Moons mothers birthday party and making a fool of himself and Moon feels disgusted and leaves him. One time in a bar, Lik meets triad member Milky Fai, who helps Lik out to get good with Hung Tai and his sister Po. Milky Fai is actually a former undercover cop who has his own problems. Later on, Lik must decide whether or not to betray his triad brothers that he have grown very close to complete the case for his superior whom he really hates and is hitting on his girlfriend.

==Cast==
*Jacky Cheung as Crazy Lik
*Tony Leung Ka-fai as Milky Fai
*Jacklyn Wu as Siu Po
*Power Chan as Pong
*Gigi Lai as Moon
*Roy Cheung as Hung Tai
*Shing Fui-On as Uncle On
*Kwong Wah as Officer Suen
*Hung Yan-yan as Bald rascal
*Frankie Ng as Father Man
*Bobby Yip as Man in brothel (uncredited)
*Yuen Bun as SDU officer
*Joan Tong as Fais ex-wife
*Parkman Wong as Coffin Sing
*Sandra Ng as woman in movie (uncredited)
*Mimi Chu as Moons mother
*Koo Ming Wah as Tais thug
*Chan Chi Fai as Brother Bau
*William Chu as Milky Fais son
*Hung Siu Wan as triad girl
*Yee Tin Hung as one of Mans goons
*Sung Poon Chung as triad
*Bowie Lau as Tais thug
*Tam Kon Chung as Tais thug
*Hau Woon Ling
*Jacky Cheung Chun Hang as Tais thug
*John Cheung
*Wong Chi Keung as cop
*Gary Mak as cop
*Lam Foo Wai as Baus thug
*Chun Kwai Bo as Baus thug
*So Wai Nam as Tais thug
*Lam Kwok Kit as Mans thug
*Mei Yee
*Wong Hoi Yiu as gambler
*Simon Cheung as cop
*Tenny Tsang as policeman

==Box office==
The film grossed HK$9,192,000 at the Hong Kong box office in its theatrical run from 13 August to 7 September 1994 in Hong Kong.

==Award nomination==
*14th Hong Kong Film Awards Best Actor (Jacky Cheung)

==See also==
*Jacky Cheung filmography
*Wong Jing filmography

==External links==
* 
*  at Hong Kong Cinemagic
* 

 

 
 
 
 
 
 
 
 
 