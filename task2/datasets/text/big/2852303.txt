Baghban (film)
{{multiple issues|
 
 
 
}}
 
 
{{Infobox film
| name           = Baghban बाग़बान  
| image          = Baghban.jpg
| caption        = Theatrical release poster
| director       = Ravi Chopra
| producer       = B. R. Chopra
| story          = B. R. Chopra Achala Nagar Satish Bhatnagar Ram Govind Shafiq Ansari
| screenplay     = Achala Nagar Satish Bhatnagar Ram Govind Shafiq Ansari
| narrator       =
| starring       = Amitabh Bachchan Hema Malini Salman Khan Aman Verma Samir Soni Mahima Chaudhry
| music          = Aadesh Shrivastav Uttam Singh
| cinematography = Barun Mukherjee
| editing        = Shailendra Doke Godfrey Gonsalves Shashi Mane
| studio         =
| distributor    = B. R. Films
| released       =  
| runtime        = 183 minutes
| country        = India Hindi
| gross          =  
}}
 Kannada film School Master Vishnuvardhan and Jaya Pradha in the lead. Baghban was originally a remake of Oon Paoos, a Marathi film.  Raja Paranjape was the lead actor and director of Oon Paoos. It explores themes that are prevalent in the play King Lear by Shakespeare, because too suffers much pain and duress at the hands of his children.

== Plot ==
The movie tells the story of Raj Malhotra (Amitabh Bachchan), his wife Pooja (Hema Malini) and their four sons Ajay (Aman Verma), Sanjay (Samir Soni), Rohit (Saahil Chadda), and Karan (Nasir Kazi). Raj and Pooja have provided their children with everything they wanted, often at the cost of Rajs and Poojas own requirements. The Malhotras also have an adopted son Alok(Salman Khan), who is in love with Arpita (Mahima Chaudhry) (both in special appearances). Alok was an orphan and Raj had provided him with money and education, bringing him up as his own son. Now a successful man, Alok worships Raj because of all his help and thinks him as his own father which he never had.

Raj has a flourishing job but, once he retires, he is no longer able to support himself and has to vacate the house he and Pooja have shared for years. They decide to live with their kids to get their love and affection. However, their kids do not want them, as they think looking after their parents will be a hassle. The children decide to split the parents up, each parent going to live with one of the sons for the next 6 months. The children think the parents will refuse the offer and therefore, they will remain in their home. However, the parents reluctantly accept the offer. The story shows how the aged parents endure this separation as well as horrible treatment from their children after all they have sacrificed for them during their lifetime. Their grandson, Rahul, is the only one who gives Raj love and affection. Saddened by the treatment he has received from his children and their families, Raj writes down all his feelings regarding how he fulfilled all his childrens dreams and in return how they treat him, as well as his love for his wife, and how much pain their separation has caused, his writing eventually becoming a novel. To pass time in the lonesome and angry house he temporarily lives in, he makes friends that admire him and regard him more than Rajs own sons. Pooja, meanwhile, endures abuse from her son and daughter-in-law, as well as her granddaughter, Payal. Payal, though, realizes her mistake when Pooja saves her from her boyfriends sexual advances, and nurtures Pooja with love.

While changing their trains after 6 months, Raj and Pooja spend some time together in Mumbai, when they suddenly meet Alok, who takes them to his home and gives them the importance and care which their real sons did not give them. Raj then finds out that his writing has become a published book named Baghban. This book becomes a big hit, selling the first copies immediately. Raj, as a result, gains the money he needs to support himself and Pooja in their old age. Their four sons find out, and are irked by the fact that they are not receiving any money. They decide to ask their parents for forgiveness to achieve this end. Payal and Rahul, though, realize their true motives and refuse to come with their parents. Raj is honored for his work, and the four sons attend with their wives. When they ask Raj to forgive them, he refuses and instead disowns them. Not even Pooja will forgive her children for the pain they have put them through.

Raj stays with Pooja in their home, near Alok and Arpita, and enjoy the company of Rahul and Payal, while disowning their sons and daughters-in-law for all eternity.

==Cast==

* Amitabh Bachchan  as  Raj Malhotra
* Hema Malini  as  Pooja Malhotra
* Salman Khan  as  Alok Raj Malhotra, Raj and Poojas adopted son
* Aman Verma  as  Ajay Malhotra, Rajs first son
* Samir Soni  as  Sanjay Malhotra, Rajs second son
* Saahil Chadha  as  Rohit Malhotra, Rajs third son
* Nasir Kazi  as  Karan Malhotra, Rajs fourth son
* Suman Ranganathan  as  Kiran Malhotra,  Ajays wife
* Rimi Sen  as  Payal Malhotra, Ajays daughter
* Divya Dutta  as  Reena Malhotra, Sanjays wife
* Master Yash Pathak  as  Rahul Malhotra, Sanjays son
* Arzoo Gowitrikar  as  Priya Malhotra, Rohits wife
* Mahima Chaudhry  as  Arpita Raj, Aloks wife
* Paresh Rawal  as  Hemant Patel
* Lilette Dubey  as  Shanti Patel
* Sharat Saxena  as  Ram Avtaar
* Mohan Joshi  as  Khuber Desai, Hotel Owner
* Sanjeeda Sheikh  as  Nilli
* Nakul Vaid  as  Rahul 
* Avtar Gill  as  Rawat, ICICI Bank Manager
* Asrani  as  Bedi
* Shashi Kiran  as  Pritam
* Gajendra Chouhan  as  a Car salesman

== Production ==
According to film expert Rajesh Subramanian B. R. Chopra had planned to make this movie in the mid eighties with Dilip Kumar and Raakhee in the lead roles.

Mohnish Behl was offered Aman Vermas role; he did not accept it because he did not want to be portrayed as the father of a 15-year-old girl. Juhi Chawla was selected for the role of Salman Khans wife but could not accept because of her pregnancy. Shahrukh Khan was offered the role played by Salman Khan. Amitabh Bachchan and Hema Malini were paired opposite each other 20 years after Nastik (1983 film)|Nastik in 1983.

Amitabh Bachchan sang three songs himself.

== Reception ==
Baghban was premiered at the Leeds International Film Festival, England. It was critically acclaimed and went on to become one of Chopras biggest hits. The film received an award at the EMVIE 2004 for best media innovation, cinema for the in-film placement of the ICICI group done by Group M. 

As per Filmfare, the film was made at a budget of   and made a domestic collections of   while it fetched   from overseas market.

==Soundtrack==
Lyrics are penned by Sameer and Hasan Kamaal.

===Track listing===
{{Track listing
| all_music = Aadesh Shrivastav, Uttam Singh
| extra_column = Singer(s)
| lyrics_credits  = yes
| title1 = Chali Chali Phir Chali
| note1 = Part I
| extra1 = Alka Yagnik, Hema Sardesai, Amitabh Bachchan, Aadesh Shrivastav
| lyrics1 = Sameer
| length1 = 4:57
| title2 = Chali Chali Phir Chali
| note2 = Part II
| extra2 = Hema Sardesai, Aadesh Shrivastav
| lyrics2 = Sameer
| length2 = 1:40
| title3 = Holi Khele
| extra3 = Alka Yagnik, Sukhwinder Singh, Udit Narayan, Amitabh Bachchan
| lyrics3 = Sameer
| length3 = 5:39
| title4 = Main Yahan Tu Wahan
| extra4 = Amitabh Bachchan, Alka Yagnik
| lyrics4 = Sameer
| length4 = 7:03
| title5 = Meri Makhna
| extra5 = Alka Yagnik, Sudesh Bhosle
| lyrics5 = Sameer
| length5 = 7:00
| title6 = O Dharti Tarse Richa Sharma
| lyrics6 = Sameer
| length6 = 10:10
| title7 = Om Jai Jagdish
| note7 = Aarti
| extra7 = Sneha Pant, Udit Narayan
| lyrics7 = Sameer
| length7 = 3:39
| title8 = Pehle Kabhi Na Mera Haal
| extra8 = Alka Yagnik, Udit Narayan
| lyrics8 = Sameer (lyricist)|Sameer, Hasan Kamaal
| length8 = 4:39
| title9 = Baghban (Title Song)
| extra9 = Richa Sharma
| lyrics9 = Sameer
| length9 = 9:59

}}

==References==
 

== External links ==
* 

 

 
 
 
 
 
 

 