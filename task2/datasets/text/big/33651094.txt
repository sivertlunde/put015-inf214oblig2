Red 2 (film)
 
{{Infobox film
| name           = Red 2
| image          = RED 2 poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Dean Parisot
| producer       = Lorenzo di Bonaventura
| screenplay     = Jon Hoeber Erich Hoeber
| based on       =  
| starring       = Bruce Willis John Malkovich Mary-Louise Parker Catherine Zeta-Jones Lee Byung-hun|Byung-hun Lee Anthony Hopkins Helen Mirren
| music          = Alan Silvestri
| cinematography = Enrique Chediak Don Zimmerman
| studio         = DC Entertainment di Bonaventura Pictures Lionsgate
| released       =  
| runtime        = 116 minutes  
| country        = United States
| language       = English
| budget         = $84 million 
| gross          = $148.1 million 
}}
 American action limited comic of the same name, created by Warren Ellis and Cully Hamner, and published by the DC Comics imprint Homage Comics|Homage. The film stars Bruce Willis, John Malkovich, Mary-Louise Parker, Catherine Zeta-Jones, Lee Byung-hun, Anthony Hopkins, and Helen Mirren, with Dean Parisot directing a screenplay by Jon and Erich Hoeber. Red 2 was released on July 19, 2013.

==Plot==
  previous film, SWAT team, agencies that contracted by MI6 to kill the three of them. Meanwhile, top contract killer Han Cho-Bai (Lee Byung-hun), whom Horton knows is seeking revenge on Frank, is also hired.

Frank, Marvin, and Sarah steal Hans plane and fly to Paris to find a man nicknamed "The Frog" (David Thewlis), with the Americans and Han in pursuit. As they arrive in Paris, they are stopped by Katya (Catherine Zeta-Jones), a Russian secret agent with whom Frank had a relationship earlier in his career. Katya is also in search of Nightshade, and joins them to find the Frog. When he sees them the Frog flees, but Frank and Katya catch him and bring him back to his house, where Sarah seduces him, both to help them and to prove she is a better girlfriend than Katya. The Frog gives them the key to his security box, which Katya apparently takes from Frank after drugging him; but Marvin, anticipating this, had handed a similar-looking key to Frank before his meeting with her. Marvin, Frank, and Sarah later find documents in the Frogs security box which point to Dr. Edward Bailey (Anthony Hopkins), a brilliant physicist, as the creator of the Operation Nightshade bomb.
 maximum security asylum for the criminally insane in London. Victoria (alerted by Marvin) unexpectedly confronts the trio, but helps to fake their deaths and then gain access to the asylum. Frank and Victoria meet Bailey, who is hyperactive and cannot rationally respond to their questions thanks to mind-fogging drugs the asylum had been giving him, so they take him to one of Marvins safehouses. After the drugs begin to wear off, Bailey remembers the bomb is still in Moscow. They go to Moscow, and Bailey concludes he hid the bomb in the Kremlin. They break into the Kremlin, and Bailey locates the suitcase-sized bomb, which is powered by red mercury, which has no radioactive signature and causes no fallout. As they are about to leave, Katya stops them. Frank persuades her to switch to their side. After they escape and are celebrating, Victoria, who has escaped MI6 imprisonment for failing to kill him, calls Frank from London and tells him that Bailey was locked up because he had wanted to detonate the bomb, not sell it. Bailey quickly holds Frank at gunpoint and confirms Victorias message, revealing that he made a deal with Horton and the Americans to give them the red mercury. He shoots Katya, staging her death at Franks hands, and leaves with the bomb case. Horton reneges on his deal with Bailey, intending to interrogate him until all his secrets have been tortured out of him, but Bailey during air transit escapes using a nerve gas he created, administering the antidote to both himself and Horton. Bailey then moves to the Iranian embassy in London; before Frank can follow him, Han attacks. Reaching a standoff, Frank urges Han to join sides with him and stop the bomb. Han finally relents, and the five enact a plan to recapture Bailey and the bomb.

Sarah first seduces the Iranian ambassador, then takes him hostage. Marvin poses as a person seeking to defect to Iran, causes a diversion with the embassy plumbing, and the disguised team comes to "fix" it. They discover in the ambassadors safe plans disclosing the location of the bomb, but find that Bailey has already triggered the bombs countdown timer and killed Horton. When they are discovered by embassy guards, Bailey seizes Sarah and flees to the airport to escape the imminent explosion. Frank, Marvin, Victoria, and Han, taking the active bomb case with them, give chase, but Marvin cannot stop the countdown. Frank, holding the bomb case, boards the plane and confronts Bailey who releases Sarah and forcefully insists he take the bomb off the plane with her. They rejoin Marvin, Victoria, and Han and wait for death as Hans plane takes off. As it disappears high in the sky it explodes in an immense mushroom cloud|fireball. Frank reveals that he had covertly placed the bomb from the case into a compartment near the planes exit and confronted Bailey with only a closed empty case. The closing scene shows Sarah enjoying herself on a mission in Caracas with Frank and Marvin.

==Cast==
* Bruce Willis as Francis "Frank" Moses
* John Malkovich as Marvin Boggs
* Helen Mirren as Victoria Winslow
* Mary-Louise Parker as Sarah Ross
* Anthony Hopkins as Dr. Edward Bailey 
* Catherine Zeta-Jones as Katya Petrokovich
* Lee Byung-hun|Byung-hun Lee as Han Cho Bai Brian Cox as Ivan Simanov
* David Thewlis as The Frog
* Neal McDonough as Jack Horton
* Garrick Hagon as Davis
* Tim Pigott-Smith as Director Philips
* Philip Arditti as Arman
* Jong Kun Lee as Hans father
* Mitchell Mullen as Wade
* Martin Sims as Blackwell
* Tristan D. Lalla as Vance
* Natalie Buscombe as Serena Emma Heming Willis as Kelly
* Titus Welliver as Senior Director of Military Intelligence (uncredited)

==Production== Jon and Fun With Dick and Jane, entered final negotiations to direct the sequel. 

In May 2012,  .  In July 2012, Neal McDonough entered negotiations to join the cast of Red 2. 
 Kremlin using a poisonous Amazonian frog.  Principal photography began in late September in Montreal.  Production moved to Paris in mid-October then to London by the end of the month.   In March 2013, Summit moved the films release date from August 2, 2013 to July 19, 2013. 

The childhood photo of Han Cho-bai (Lee Byung-hun) and his father that appears in the film are actually photos of Lee with his late father, who died in 2000.   Lees father was a fan of Hollywood films and dreamed of being an actor himself. When Lee shared this story with Dean Parisot, the director, he was so touched that he decided to include Lees father at the end credits as one of the main cast, even though the photos appear only briefly in the film. 

==Reception==

===Box office===
Red 2 opened on July 19, 2013 in North America. In its opening weekend, the film grossed $18.5 million and finished in fifth place, which was lower than the $21.8 million its predecessor earned in October 2010. According to exit polling, 67% of the audience was over 35 and 52% was male.  Red 2 grossed $53.3 million in North America and $88.9 million overseas for a total of $142.1 million worldwide. 

===Critical response===
Red 2 has received mixed reviews from film critics. On Rotten Tomatoes, the film holds a rating of 42%, based on 137 reviews, with an average rating of 5.4/10. The sites consensus reads, "While its still hard to argue with its impeccable cast or the fun they often seem to be having, Red 2 replaces much of the goofy fun of its predecessor with empty, over-the-top bombast."  Metacritic, which uses a weighted mean, assigned a score of 47 out of 100, based on 38 critics, indicating "mixed or average reviews". 

Justin Chang of Variety (magazine)|Variety called Red 2, "An obligatory sequel that cant quite recapture the sly, laid-back pleasures of its cheerfully ridiculous predecessor."  Todd Gilchrist of The Wrap said, "...in a lackadaisical sequel no one asked for except perhaps his creditors,   seems unmotivated to smile at all, much less offer a series of emotions that constitute a believable or compelling performance."  Justin Lowe of The Hollywood Reporter said, "Not that it isn’t entertaining, but the films premise is certainly well past its use by date, resulting in another passably palatable sequel distinguished by a lack of narrative and stylistic coherence that could potentially underpin a really viable franchise."  Betsy Sharkey of Los Angeles Times said, "No doubt the hope was that   Parisot could do to the action genre what he did to the Star Trek universe in the spot-on satire of 1999s Galaxy Quest. He has, and he hasnt. Red 2 is much more of a mixed bag than it should have been."  Nicolas Rapold of The New York Times said, "Cars careen, lazily written infiltration plans are executed, and the violence is plentiful and toothless." 

==Sequel==
In May 2013, Lionsgate re-signed Jon and Erich Hoeber to write a third installment. 

==References==
{{Reflist|30em|refs=
   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   
}}

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 