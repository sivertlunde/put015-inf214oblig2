The Go-Getter (1923 film)
{{Infobox film
| name           = The Go-Getter
| image          = 
| alt            = 
| caption        =
| director       = Edward H. Griffith
| producer       = 
| based on       =   
| screenplay     = John Lynch 
| starring       = T. Roy Barnes Seena Owen William Norris Tom Lewis Louis Wolheim Fred Huntley
| music          = 
| cinematography = Harold Wenstrom
| editing        = 
| studio         = Cosmopolitan Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent comedy film directed by Edward H. Griffith and written by Peter B. Kyne and John Lynch. The film stars T. Roy Barnes, Seena Owen, William Norris, Tom Lewis, Louis Wolheim, and Fred Huntley. The film, which is based upon the short story "The Go-Getter" by Peter B. Kyne, was released April 8, 1923, by Paramount Pictures.   A fragmentary print of the film is held in a private collection. 

==Plot==
 

== Cast ==
*T. Roy Barnes as Bill Peck
*Seena Owen	as Mary Skinner
*William Norris as Cappy Ricks
*Tom Lewis as Charles Skinner
*Louis Wolheim as Daniel Silver
*Fred Huntley as Jack Morgan
*John Carr as Joe Ryan
*Frank Currier as Hugh McNair
*William Sorelle as Mayor Healey 
*William J. MacMillan as Pilot
*Jane Jennings

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 