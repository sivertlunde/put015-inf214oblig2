Bowery Bombshell
{{Infobox film
| name           = Bowery Bombshell
| image          = Bowery Bombshell.jpg
| caption        = Theatrical poster to Bowery Bombshell
| director       = Phil Karlson
| producer       = Jan Grippo Lindsley Parsons Tim Ryan
| starring       = Leo Gorcey Huntz Hall Bobby Jordan William Benedict Sheldon Leonard Wee Willie Davis
| music          = Edward J. Kay
| cinematography = William Sickner William Austin
| distributor    = Monogram Pictures
| released       =  
| runtime        = 65 minutes
| language       = English
| budget         =
}}
Bowery Bombshell is a 1946 film starring the comedy team of The Bowery Boys.  It is the third film in the series.

==Plot==
Sweet Shop owner Louie needs to raise $300.  The Boys try to sell their car to raise the money, but are unable to because the car falls apart when they try to show it to a prospective buyer.  They decide to go to the bank and take a loan out on it, but just as they arrive the bank is robbed.  The robbers bump into them and drop the bag full of the stolen money.  As Sach picks up the bag to return it to the robber, Cathy, a photographer, takes his photo.

After trying unsuccessfully to get the photo back, it winds up on the front page of the newspaper and Sach becomes a wanted criminal.  Slip pretends to be a notorious gangster, Midge Casalotti, in order to get the stolen money back and to clear Sachs name.  In the end, Sach is cleared and the gangsters, led by Ace Deuce, are apprehended. 

The film ends in an explosion, where a spare tire with the words, "Dead End" on it falls around the necks of Sach and Slip.

==Notes==
First Bowery Boys film with former East Side Kid Buddy Gorman. He only has a small cameo as Bud (the newsboy), but would become a regular member of the gang beginning with the film Blonde Dynamite.

==Home media== Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume One" on November 23, 2012.

==Cast==
===The Bowery Boys===
*Leo Gorcey as Terrance Slip Mahoney
*Huntz Hall as Sach
*Bobby Jordan as Bobby
*William Benedict as Whitey
*David Gorcey as Chuck

===Remaining cast===
*Bernard Gorcey as Louie Dumbrowski
*Teala Loring as Cathy Smith
*Sheldon Leonard as Ace Deuce
*Wee Willie Davis as Moose James Burke as Officer OMalley
*William Ruhl as Biff

==References==
 

== External links ==
*  

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958
| before=In Fast Company 1946
| after=Spook Busters 1946}}
 

 
 

 
 
 
 
 