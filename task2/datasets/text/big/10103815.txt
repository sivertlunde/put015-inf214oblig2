Unknown Pleasures (film)
{{Infobox film
| name           = Unknown Pleasures
| image          = Unknown Pleasures Cannes.jpg
| image_size     =
| caption        =
| director       = Jia Zhangke Masayuki Mori
| writer         = Jia Zhangke
| narrator       =
| starring       = Zhao Weiwei Wu Qiong Zhao Tao
| music          =
| cinematography = Nelson Yu Lik-wai
| editing        = Chow Keung
| distributor    = United States:  
| released       =  
| runtime        = 113 minutes
| country        = China Mandarin Chinese
| budget         =
| gross          =
}} 2002 Cinema Chinese film directed by Jia Zhangke, starring Wu Qiong, Zhao Weiwei and Zhao Tao as three disaffected youths living in Datong in 2001, part of the new "Birth Control" generation. Fed on a steady diet of popular culture, both Western and Chinese, the characters of Unknown Pleasures represent a new breed in the Peoples Republic of China, one detached from reality through the screen of media and the internet.
 The Pianist. 
 The World, state film bureaucrats (SARFT). {{cite web | url =
http://www.asiaarts.ucla.edu/article.asp?parentid=20814 | title =  Asia Pacific Arts: Presenting the World  | accessdate = 2008-09-07 | author = Hu, Brian |date= 2005-02-17 | publisher = UCLA Asia Institute}} 

== Plot ==
Unknown Pleasures follows three disaffected, aimless young people in the industrial city of Datong in Chinas Shanxi province throughout 2001. Nineteen-year-old Bin Bin (Zhao Weiwei) lives with his mother, an adherent of the Falun Gong, in a small apartment near Datongs textile mill. Bin Bins best friend, the reckless Xiao Ji (Wu Qiong), lives in an even smaller apartment with his father, and spends his time riding his motorbike around the city. The two friends eventually meet Qiao Qiao, a young singer and dancer working for the Mongolian King Liquor company as a spokesmodel. Xiao Ji immediately becomes enamored with Qiao Qiao, which gets him in trouble with Qiao Qiaos boyfriend, the loan shark and local thug, Qiao San (Li Zhubin).
 Pulp Fiction, Zhuangzi who, in his poem Ren Xiao Yao, "philosophized that we should do what feels good." Soon afterward, it is learned that Qiao San has died in a car accident. The film implies that Qiao Qiao nevertheless leaves Xiao Ji and is last seen wearing a blue wig as a prostitute in a run-down club.

Bin Bin, meanwhile, following the advice of his mother, tries to join the Peoples Liberation Army|PLA, but is rejected when it is discovered that he suffers from hepatitis. Shattered, he borrows ¥1500 from a small-time crook Xiao Wu (Wang Hongwei). Bin Bin uses the money to purchase a cell phone for his girlfriend, but when she tries to get close to him, he refuses, and notes only that there is no future for him anymore.

Bin Bin and Xiao Ji decide to rob a bank, as they have seen so often in American films. Attaching a fake bomb to Bin Bins chest, Xiao Ji drives Bin Bin to a China Construction Bank, where the latter is immediately arrested. Fleeing, Xiao Ji drives his motorbike down the highway until it breaks down and he hitches a ride to locations unknown. Bin Bin is left at the police station, where an officer informs him that robbery is a capital crime. The film ends as the police officer forces Bin Bin to stand and sing. Bin Bin chooses to sing Ren Xiao Yao, a pop song about being spiritually free through love.

== Production ==
{{quote box|width=40%|align=right|bgcolor=#c6dbf7|quote= "At first it was the bleak and lonely buildings that attracted me. When I saw the streets filled with lonely, directionless people, I became interested in them."
|source=Jia Zhangke, 2002
}} In Public, his entry in a documentary competition sponsored by the 2001 Jeonju International Film Festival held in South Korea.     The competition (which also drew entries from Tsai Ming-liang and John Akomfrah) required that the shorts be filmed entirely in digital video.  While Jia had originally intended only to film the derelict factories in Datong, the filming with digital video would soon inspire the director to begin production of Unknown Pleasures.  As Jia stated at a news conference: "At first it was the bleak and lonely buildings that attracted me. When I saw the streets filled with lonely, directionless people, I became interested in them." 

Unknown Pleasures was filmed using digital video in only nineteen days, as a result of time and budgetary constraints.  In his production notes, Jia claims that the use of digital video produced a slight color discrepancy that lent itself to the tone he wanted the film to take.  Additionally, the use of digital cameras meant a more streamlined production and greater ease of movement.  As a result, Jia was able to begin shooting a mere three weeks after developing the idea for the film. 

According to Jia, the final scene of Xiao Ji riding down the highway as a thunderstorm approaches would not have been possible had traditional film cameras been used. But because of the flexibility of digital video, Jia Zhangke was able to capture the scene with the storm and in the directors words, creates a moment where the "environment is complementing   internal feelings."  At the same time, use of digital video restricted Jia. He noted in an interview shortly after the release of the film that he and cinematographer Yu Lik-wai were forced to cut back on exterior scenes due to the drawbacks of filming on digital video in sunlight. 

=== Creative team === The World, Still Life. Along with producer Li Kit Ming, Chow and Yu have been described by Jia as the "core of his creative team."  Among the cast, Zhao Tao (Qiao Qiao) and Wang Hongwei (Xiao Wu) are also Jia regulars.

== Cast ==
* Zhao Weiwei as Bin Bin. A young man most often seen wearing an oversized dress shirt, Bin Bin is frustrated by his life in Datong. His relationship with his girlfriend is distant but tender, while his relationship with his mother is strained. Despite his seeming timidity, it is Bin Bin who eventually carries out the poorly thought through plan to rob a bank.
* Wu Qiong as Xiao Ji. The long-haired Xiao Ji is Bin Bins best friend. Considerably more reckless than Bin Bin, Xiao Jis infatuation with Qiao Qiao drives much of the films narrative. The World, Still Life, and 24 City) plays the female lead of Qiao Qiao. Slightly older than both Bin Bin and Xiao Ji (the film states that she is born in 1980 making her 21 years old), Qiao Qiao serves as the singing and dancing enticement for the Mongolian King Liquor company. It is Qiao Qiao that explains the philosophy of "ren xiao yao," a form of hedonism. Jia wrote the character of Qiao Qiao to reflect the modern Chinese woman, who struggle between conservative tradition and modernity. According to Jia, Qiao Qiao is unable to continue her relationship with Qiao San because she cannot reconcile her hidden conservatism with the idea of becoming a mistress. 
* Li Zhubin as Qiao San, Qiao Qiaos older boyfriend, former gym teacher, and current "agent." Unknown Pleasures closest thing to a true villain, Qiao San is essentially a local thug in Datong. Though he is rarely physically violent himself, he carries a gun with him and has several of his cronies restrain and humiliate Xiao Ji at a dance club. Love Will Tear Us Apart (directed by cinematographer Yu Lik-wai) DVDs. According to the director, this self-reference was possible in part because Xiao Wu (and Wang Hongwei) had become a cultural icon in Chinas independent film scene. 
* Zhou Qingfeng as Yuan Yuan, Bin Bins studious girlfriend. Throughout the course of the film, Yuan Yuan has dreams of getting into a Beijing university in order to study international trade. Yuan Yuans character was consciously set apart from the main three characters, in that she is the only character with set goals for life and the possibility to escape provincial life. 
* Bai Ru as Bin Bins mother, a proponent of the Falun Gong.
* Liu Xian as Xiao Jis father, an uneducated man who mistakes a single US dollar to be a fortune.
* Jia Zhangke plays a small role in his own film as the opera-singing man seen throughout Unknown Pleasures.

== Themes ==

===The "Birth Control" generation ===
{{quote box|width=40%|align=right|bgcolor=#c6dbf7|quote= "In Unknown Pleasures, young people lack discipline. They dont have any goals for the future. They refuse all constraints. They run their own lives and act independently. But their spirit is not as free."
|source=Jia Zhangke
}}
In his production notes, Jia has stated that the portrayals of youth by Wu Qiong, Zhao Weiwei, Zhao Tao was meant to illustrate the "birth control" generation, or the generation to emerge from Chinas One-child policy.  With no brothers or sisters, Jia wanted to show these individuals as isolate, alone, "confronted with an existential crisis of individuality."  In a separate interview, Jia noted that unlike his own generation, this generation is often detached from reality, filtering their experiences through the internet, television, and other media.  In one oft-referenced scene, Xiao Ji discusses the film Pulp Fiction to Qiao Qiao, after which Jia quickly cuts to the two dancing in a club with music sampled out of that film. Critics and scholars also picked up on this existentialist strain in the characters of Unknown Pleasure. Elvis Mitchell, for example, wrote, " he saddest thing about it is that the social ineptitude of the Pleasure youth doesnt even belong to them -- theyve sampled it from Western culture, just like the clangorous funk of the dance club music. They want to soak up someone elses dream."  Similarly, Kevin Lee of Senses of Cinema writes how "the attitudes of these kids are almost completely derived by the electronic mass media that they consume and that consumes them."  As J. Hoberman notes in his review, for Xiao Ji, Bin Bin, and Qiao Qiao, Unknown Pleasures are those that "are everywhere in evidence, yet satisfaction itself is beyond reach." 

Each of the three main characters therefore try to achieve a state of "Ren Xiao Yao" - freedom from all constraints. This phrase and concept arises multiple times in the film. As described by Qiao Qiao, it is part of the philosophy of the Taoist Zhuang Zhou|Zhuangzi. She refers to the belief that life is the pursuit of absolute freedom and pleasure. Jia writes, however that   In another reference, "Ren Xiao Yao" is the name of a pop song from 2001,  and is the song that Bin Bin sings in the films ironic final scene in a jail cell. Freedom, it seems, is harder than it looks. In a running theme, Bin Bin and Xiao Ji consistently refer to the Sun Wukong, the Monkey King. Bin Bin explicitly draws the point that unlike himself, the Monkey King has no parents and no burdens. For Jia, the story of the Monkey King "reflects the fatalism of  " in that unlike the Monkey King, these characters "struggle desperately. They pull themselves out of difficult situations, but they always fall back into new problems because no one can escape the rules of the game. True freedom doesn’t exist in this world."

=== 2001 ===
Jias production notes also reveal the importance of the films time period: 2001. At numerous instances in the film, newscasts and other media link the characters to external current events. These include the Hainan Island incident with the United States, Chinas entry into the World Trade Organization, a sabotage in a factory in Datong itself, and Chinas successful bid to become the host city of the 2008 Summer Olympics. For Jia, the year 2001 was particularly significant:
 

=== Datong and Beijing === Three Sisters-- dreamland as a receding horizon." 

== Reception == New York Times critic Elvis Mitchell commented on the film, noting that even if "the world doesnt need another picture on disaffected youth...Unknown Pleasures is about more than alienation."    Stylistically, however, Mitchell felt that Jias long-takes and slow pans started to feel repetitive, a sort of "reductive neo-realism."  The Village Voices J. Hoberman gave the film a much stronger review than many of his contemporaries, arguing that Unknown Pleasures was "Jias most concentrated evocation of contemporary Chinas spiritual malaise."   

The film was not universally praised, however, and many critics found significant flaws in the films style and pacing. One common complaint was that like the films aimless protagonists, Unknown Pleasures seemed lost in its own narrative. One critic argues that the films story "goes nowhere" and as a result the audience never "understand  the motivation of the characters."  The industry magazine Variety (magazine)|Variety also gave the film only a middling review, with a similar complaint that the film "is far more diluted thematically, touching on a number of interesting points but failing to bring them together in any cohesive way."  Two internet review aggregates reflect the films somewhat average impression among western critics; coincidentally, both Metacritic (ten reviews) and Rotten Tomatoes (twenty-eight reviews) give Unknown Pleasures scores of "61" (out of 100), or "Generally favorable reviews" and "fresh," respectively. 

===Awards, nominations, and honors===
* 2002 Toronto International Film Festival
**Official Selection     
* 2002 Cannes Film Festival
**Official Selection  2002 New York Film Festival
**Official Selection  2003 Singapore International Film Festival NETPAC Award — special mention 

=== Top ten lists ===
Several American critics placed Unknown Pleasures within their top-10 lists for 2003.

*2nd - Dennis Lim, The Village Voice   
*10th - Jonathan Rosenbaum, Chicago Reader 
*  - Robert Koehler, Variety (magazine)|Variety (tied with Platform (2000 film)|Platform) 
*  - Manohla Dargis, Los Angeles Times 

== References ==
 

== External links ==
* 
* 
* 
* 
* 
*  An essay on ideology and aesthetics in Platform and Unknown Pleasures at Offscreen Journal

 

 
 
 
 
 
 