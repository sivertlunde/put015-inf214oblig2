Sleeping Beauty (1995 film)
{{multiple issues|
 
 
}}
{{Infobox Film |
  name           = Sleeping Beauty |
  image          = Sleeping Beauty 1995.JPG |
  director       = Toshiyuki Hiruma  Takashi |
  producer       = Mark Taylor |
  writer         = Charles Perrault and The Brothers Grimm (original story)  Larry Hartstein | La Belle Little Briar Rose by Brothers Grimm|
  starring       = |
  distributor    = GoodTimes Entertainment |
  released= December 19, 1995 (U.S.) |
  runtime        =48 minutes | English |
  country        =   Japan     United States |
  budget         = |
  music          = Andrew Dimitroff |
}}
 directly to video, the 48-minute film was produced by Jetlag Productions and was distributed to DVD in 2002 by GoodTimes Entertainment as part of their "Collectible Classics" line.

== Story ==
 spindle of a spinning wheel and fall dead.  Saddened and frightened by Odelias curse, the king and queen beg of Primrose to save their child with her gift.  Primrose, who doubts her magic could defeat Odelias, decides that if she cannot remove the curse, she will be able to change it, and therefore declares that Felicity will indeed prick her finger on a spindle, but she will not die, instead shell fall into a sleep of one hundred years and a day, when shell be awakened by a kiss of true love and Odelia will be the one to die instead.
 thread with spell over red rose. noble men thorns surrounding happily ever after.

== Songs ==
*"Follow Your Heart" Sung by Wendy K Hamilton
*"Princess, Did You Know?" Sung by Wendy K Hamilton
*"Just Keep On Going"

== See also ==
*List of animated feature-length films
*Sleeping Beauty, the original fairy tale
*Charles Perrault
*Jetlag Productions

== External links ==

*  
*  
*   at the Big Cartoon DataBase

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 