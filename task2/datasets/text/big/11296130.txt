Wittgenstein (film)
{{Infobox film
| name           = Wittgenstein
| image          = Wittgenstein (film).jpg
| image size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Derek Jarman
| producer       = Tariq Ali Takashi Asai Ben Gibson Eliza Mellor
| writer         = Ken Butler Terry Eagleton Derek Jarman
| narrator       = 
| starring       = 
| music          = 
| cinematography = James Welland
| editing        = Budge Tremlett
| studio         = 
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = Japan, UK
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
Wittgenstein is a 1993 film by the English director Derek Jarman. It is loosely based on the life story as well as the philosophical thinking of the philosopher Ludwig Wittgenstein. The adult Wittgenstein is played by Karl Johnson.

The original screenplay was by the literary critic Terry Eagleton. Jarman heavily rewrote the script during pre-production and shooting, radically altering the style and structure, although retaining much of Eagletons dialogue. The story is not played out in a traditional setting, but rather against a black backdrop within which the actors and key props are placed, as if in a theatre setting.

==Principal cast==
*Clancy Chassay (young Wittgenstein) 
*Karl Johnson (adult Wittgenstein) 
*Nabil Shaban (Martian) 
*Michael Gough (Bertrand Russell)
*Tilda Swinton (Lady Ottoline Morrell) Maynard Keynes) Kevin Collins (Johnny)
*Lynn Seymour (Lydia Lopokova)

==Script==
* Terry Eagleton|Eagleton, Terry (1993).  . London, England: British Film Institute, pp.&nbsp;151. ISBN 978-0-85170-397-8

==Award==
* Teddy Award for best feature film, 1993

==See also==
 
* List of avant-garde films of the 1990s

==External links==
*  
*  
*  
*   on YouTube

 
 

 
 
 
 
 
 
 
 
 
 
 


 