Vellayani Paramu
{{Infobox film 
| name           = Vellayani Paramu
| image          = Vellayani Paramu.jpg
| caption        =
| director       = J. Sasikumar
| producer       = EK Thyagarajan
| writer         = Pappanamkodu Lakshmanan
| screenplay     =
| starring       = Prem Nazir Jayan Jayabharathi Adoor Bhasi
| music          = G. Devarajan
| cinematography =
| editing        =
| studio         = Sree Murugalaya Films
| distributor    = Sree Murugalaya Films
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by J. Sasikumar and produced by EK Thyagarajan. The film stars Prem Nazir, Jayan, Jayabharathi and Adoor Bhasi in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Prem Nazir
*Jayan
*Jayabharathi
*Adoor Bhasi
*Sreelatha Namboothiri
*Janardanan
*MG Soman
*Vazhoor Rajan

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aalam udayone || P Susheela, P Jayachandran, Chorus, Jolly Abraham || Sreekumaran Thampi || 
|-
| 2 || Aalolalochanakal || K. J. Yesudas, P. Madhuri || Sreekumaran Thampi || 
|-
| 3 || Shariyethennaararinju || P Jayachandran || Sreekumaran Thampi || 
|-
| 4 || Villadichaan paattupaadi || P Jayachandran, CO Anto || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 