When Ladies Meet (1941 film)
{{Infobox film
| name           = When Ladies Meet
| image          = File:When Ladies Meet 1941 poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Robert Z. Leonard
| producer       = Robert Z. Leonard Orville O. Dull
| based on       =  
| writer         = S.K. Lauren Anita Loos Leon Gordon (playwright) John Meehan (screenwriter) Robert Taylor Greer Garson Herbert Marshall
| music          = Bronislau Kaper
| cinematography = Robert H. Planck
| editing        = Robert Kern
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $640,000  . 
| gross          = $1,846,000 
}}
 Robert Taylor, same name, Robert Montgomery, and Frank Morgan in the roles played by Garson, Crawford, Taylor and Marshall.

==Synopsis== Robert Taylor), however, is convinced hes the right man for her and pursues her. He sees through her rationalizations and wrong-thinking and decides to throw Mary and Woodruffs wife Claire (Greer Garson) together at the house of a friend (Spring Byington). The two women do not know each other, but during their chats Mary appreciates and respects Claires maturity and wisdom. When Mary learns Woodruff is a philandering womanizer of long standing, she realizes she cannot love him and welcomes Jimmys attentions.

==Cast==
* Joan Crawford as Mary Minnie Howard Robert Taylor as Jimmy Lee
* Greer Garson as Mrs. Claire Woodruff
* Herbert Marshall as Rogers Woodruff
* Spring Byington as Bridget Bridgie Drake
* Rafael Storm as Walter Del Canto
* Mona Barrie as Mabel Guiness
* Max Willenz as Pierre, Bridgets Summer House Handyman
* Florence Shirley as Janet Hopper
* Leslie Francis as Homer Hopper

==Reception==
Howard Barnes in the New York Herald Tribune wrote, "Even when   is wearing spectacles, she is not particularly convincing in the part." 

===Box Office===
According to MGM records the film earned $1,162,000 in the US and Canada and $684,000 elsewhere resulting in a profit of $607,000. 

==Awards and nominations==
The film earned an Academy Award nomination for art directors Cedric Gibbons, Randall Duell and Edwin B. Willis.   

==DVD release==
When Ladies Meet was released on Region 1 DVD on March 23, 2009 from the online Warner Bros. Archive Collection.

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 

 