Navy Blues (1941 film)
{{Infobox film
| name           = Navy Blues
| image          = Navy Blues (1941 film) poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Lloyd Bacon
| producer       = Jack Saper Jerry Wald
| screenplay     = Jerry Wald Richard Macaulay Arthur T. Horman Sam Perrin
| story          = Arthur T. Horman
| starring       = Ann Sheridan Jack Oakie Jack Haley Herbert Anderson Jack Carson Jackie Gleason William T. Orr
| music          = Heinz Roemheld
| cinematography = Tony Gaudio
| editing        = Rudi Fehr
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Navy Blues is a 1941 American comedy film directed by Lloyd Bacon and written by Jerry Wald, Richard Macaulay, Arthur T. Horman and Sam Perrin. The film stars Ann Sheridan, Jack Oakie, Jack Haley, Herbert Anderson, Jack Carson, Jackie Gleason and William T. Orr. The film was released by Warner Bros. on September 13, 1941.  

==Plot==
 

== Cast == 
*Ann Sheridan as Marge Jordan
*Jack Oakie as Cake OHara
*Jack Haley as Powerhouse Bolton
*Herbert Anderson as Homer Matthews
*Jack Carson as Buttons Johnson
*Jackie Gleason as Tubby 
*William T. Orr as Mac
*Richard Lane as Rocky Anderson
*John Ridgely as Jersey
*Kay Aldridge as Navy Blues Sextette Member 
*Georgia Carroll as Navy Blues Sextette Member
*Marguerite Chapman as Navy Blues Sextette Member
*Peggy Diggins as Navy Blues Sextette Member
*Leslie Brooks as Navy Blues Sextette Member 
*Claire James as Navy Blues Sextette Member 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 