Dharma Yuddham
{{Infobox film|
| name = Dharma Yuddam
| image = DharmaYuddamfilm.png
| caption = LP Vinyl Records Cover
| director =  R. C. Sakthi
| writer =
| starring = Rajinikanth Sridevi Thengai Srinivasan
| producer =
| music = Ilayaraja
| editor =
| released = 29 June 1979
| runtime = Tamil
| budget =
}}

Dharma Yuddam is a 1979 Tamil film directed by R. C. Sakthi, a close associate of Kamal Hassan. It stars Rajinikanth and Sridevi in the lead.

==Plot==
Rajinis parents are killed by Thengai Srinivasan. He is adopted and brought up by a rich man. After many years Rajini finds out that Thengai Srinivasan has been stealing body parts, especially eyes. How Rajini takes revenge on him forms the crux of the story.

==Trivia==
The film became a major hit for Rajinikanth.
Ilaiyaraajas music was a major highlight of this film, especially the songs "Aagaya Gangai"(rendered by Malaysia Vasudevan and S. Janaki) and "Oru Thanga Rathathil", rendered by Malaysia Vasudevan. Music had given some impeccable Background scores in the form of Western and Indian fusion.

 
 
 
 
 
 
 


 