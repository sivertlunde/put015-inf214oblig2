Rabbit's Feat
 

{{Infobox Hollywood cartoon
| cartoon_name = Rabbits Feat
| series = Looney Tunes (Bugs Bunny)
| image = Rabbits Feat title card.png
| caption = Title card
| director = Chuck Jones
| producer = John Q. Burton (uncredited)
| story_artist = Michael Maltese (uncredited) Richard Thompson
| voice_actor = Mel Blanc
| musician = Milt Franklyn
| studio = Warner Bros. Cartoons
| distributor = Warner Bros. Pictures The Vitaphone Corporation
| release_date = June 4, 1960 (USA)
| color_process = Technicolor
| runtime = 6:07
| movie_language = English
}}
 Wile E. Maltese had left for Hanna-Barbera, his name was removed from the credits (although his name remained on the credits for The Mouse on 57th Street a year later). 

==Synopsis==
The scene opens with Wile E Coyote saying, "Allow me to introduce myself. My name is Wile E Coyote. Uh, I am a genius by trade." Wile E Coyote then explains that he is hunting for the common western rabbit. Wile E Coyote sets a picnic blanket and tries to catch Bugs Bunny. Bugs Bunny comes out of his hole and takes some carrot juice and pours it into a cup. Wile E Coyote this time succeeds in catching Bugs Bunny but not for long. In the next scene, Wile E, convinced that he has the rabbit, puts the sack where Bugs Bunny had been into and puts it into a cauldron. Bugs Bunny, hiding behind a rock, pretends to cry in agony. When Wile E Coyote takes a look, Bugs Bunny kisses him and says, "Papa! You came back from Peru! Oh Papa, we thought you were run over by an elevator!" Wile E Coyote spits it out and while sad violin music is playing, Bugs Bunny laments, "Boo-hoo! Oh boo-hoo-the-hoo! Ive been rejected by my onliest father!" The next scene shows Wile E Coyote talking to himself about how hes going to kill Bugs Bunny. Bugs Bunny sneaks over and rejects the first two ideas. When Wile E Coyote mentions the dynamite carrot idea, Bugs Bunny yells and Wile E Coyote falls on his face. (twice) 

==External links==
*  
*  

 
{{succession box |
before= Person To Bunny | Bugs Bunny Cartoons |
years= 1960 |
after= From Hare to Heir|}}
 

 

 
 
 
 
 


 