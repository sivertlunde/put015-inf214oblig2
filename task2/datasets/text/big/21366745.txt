Monster (2008 film)
{{Infobox film
| name           = Monster
| image          = monster2008.jpg
| caption        = DVD cover
| director       = Eric Forsberg
| producer       = David Michael Latt David Rimawi Paul Bales
| writer         = Eric Forsberg David Michael Latt
| starring       = Yoshi Ando Sarah Lieving Shinichiro Shimizu Erin Sullivan
| music          = 
| editing        = 
| distributor    = The Asylum
| released       =  
| runtime        = 90 minutes
| country        = United States Japan
| language       = English Japanese
| budget         = 
| gross          = 
}}
Monster is a 2008 direct-to-DVD Japanese daikaiju film.

The film is a mockbuster created to capitalize on the release of Cloverfield. It was released direct-to-DVD on January 15, 2008.  Cloverfield was released theatrically three days later on January 18, 2008.

== Plot ==
 Fukuoka earthquake Miyagi earthquake), although careful analysis of the evidence suggests otherwise.

As time goes on, all of Tokyo begins to suffer from abnormal earth tremors similar to those registered in 2005. The tremors are found not to be caused by an earthquake, but by a gigantic octopus that has been dormant for centuries. It has since been awakened by mankind and now sees Tokyo as a new feeding-ground; the filmmakers document the catastrophe as it unfolds.

It starts as Hana and Hyuga are talking about filming Tokyo. The scene then switches to the interior of Hanas car as the two drive to LAX, to catch their flight to Tokyo. In Tokyo they rent a hotel room, and, the next day, Hyuga films Hana talking with the global warming minister. During the interview, an earthquake strikes and the scene again switches to the reporters in the basement of the environmental building. They find a survivor named Aoyagi, and as they flee, they hear the sounds of panic and plane engines from a tunnel.

Later, they find a mall, and another earthquake occurs. In the chaos, Aoyagi is impaled by a pole as the reporters flee in panic. Some small text appears on the screen saying that tape #3 was damaged. As the reporters run toward a mall they find a woman and her grandfather, and they eat and sleep, Hana not realizing she had left the camera turned on. Then, another earthquake begins and kills the grandfather as the woman tells the reporters to flee. They hear the mall explode as they run away and soon find a building which they enter. They have gone upstairs when the building suddenly collapses. The reporters survive, but the collapse damages the camera lens.

Night arrives and they see helicopters ready to save refugees, but a tentacle destroys them and proceeds to throw cars at the people, killing many of them. Panic ensues, and Hyuga abandons Hana. A tentacle slams into the ground where Hyuga is and he is wounded. Additionally, the cameras batteries run low. Hana cries for Hyuga as the crash of a tentacle hitting the ground is heard, ending the film, which indicates that the reporters were crushed by the monster and were among the thousands, if not millions, killed.

== Reception ==
The few reviews of the film posted online have been extremely negative.   Scott Foy of Dread Central did an audio file parody of the "found footage" concept used by the movie: in this case, the file consists of Foy giving his thoughts on the movie while at the same time pretending to be slowly going insane due to the movies abysmal quality; it culminates in him apparently jumping out of a window.  Foy would later state that some people actually thought his "insanity" was real  and would also name Monster the "Worst Direct To DVD Horror Movie of 2008" in a later podcast. 

== See also ==
* Cloverfield, another monster film released in the same year giant octopus 1977

== References ==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 