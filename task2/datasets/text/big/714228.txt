Ikiru
{{Infobox film 
  | name = Ikiru
  | image = Ikiru poster.jpg
  | caption = Original Japanese poster
  | director = Akira Kurosawa
  | producer = Sōjirō Motoki
  | writer = Shinobu Hashimoto Akira Kurosawa Hideo Oguni
  | starring = Takashi Shimura
  | music = Fumio Hayasaka
  | cinematography = Asakazu Nakai Toho Studios
  | editing = Kōichi Iwashita
  | distributor = Toho
  | released =  
  | runtime = 143 minutes
  | country = Japan
  | language = Japanese
  | budget = 
  }}

  is a 1952 Japanese film directed and co-written by Akira Kurosawa. The film examines the struggles of a minor Tokyo bureaucrat and his final quest for meaning. The script was partly inspired by Leo Tolstoys 1886 novella The Death of Ivan Ilyich, although the plots are not similar beyond the common theme of a bureaucrat struggling with a terminal illness.  It stars Takashi Shimura as Kanji Watanabe.

==Plot summary==
Kanji Watanabe (Takashi Shimura) is a middle-aged man who has worked in the same monotonous bureaucratic position for thirty years. His wife is dead and his son and daughter-in-law, who live with him, seem to care mainly about Watanabes pension and their future inheritance.

After learning he has stomach cancer and less than a year to live, Watanabe attempts to come to terms with his impending death. He plans to tell his son about the cancer, but decides against it when his son does not pay attention to him. He then tries to find escape in the pleasures of Tokyos nightlife, guided by an eccentric novelist whom he just met. In a nightclub, Watanabe requests a song from the piano player, and sings "Gondola no Uta" with great sadness. His singing greatly affects those watching him. After one night submerged in the nightlife, he realizes this is not the solution.

The following day, Watanabe encounters a young female subordinate, Toyo, who needs his signature on her resignation. He is attracted to her joyous love of life and enthusiasm and tries to spend as much time as possible with her. She eventually becomes suspicious of his intentions and grows weary of him. After convincing her to join him for the last time, he opens up and asks for the secret to her love of life. She says that she doesnt know, but that she found happiness in her new job making toys, which makes her feel like she is playing with all the children of Japan and that he should find a purpose in his own life.

Inspired by her, Watanabe realizes that it is not too late for him and that he still can do something. He then dedicates his remaining time and energy to accomplish one worthwhile achievement before his life ends. Through his tireless and persistent efforts, he is able to overcome the stagnation of bureaucracy and turn a mosquito-infested cesspool into a childrens playground.

The last third of the film takes place during Watanabes wake, as his former co-workers try to figure out what caused such a dramatic change in his behavior. His transformation from listless bureaucrat to passionate advocate puzzles them. As the co-workers drink, they slowly realize that Watanabe must have known he was dying, even when his son denies this, as he was unaware of his fathers condition. They drunkenly vow to live their lives with the same dedication and passion as he did. But back at work, they lack the courage of their newfound conviction.

An iconic scene from the film is from the last few moments in Watanabes life, as he sits on the swing at the park he built. As the snow falls, we see Watanabe gazing lovingly over the playground, at peace with himself and the world. He again starts singing "Gondola no Uta".

==Cast==
 
* Takashi Shimura as Kanji Watanabe
* Shinichi Himori as Kimura
* Haruo Tanaka as Sakai
* Minoru Chiaki as Noguchi
* Bokuzen Hidari as Ohara
* Miki Odagiri as Toyo Odagiri, employee 
* Kamatari Fujiwara as Sub-Section Chief Ōno
* Nobuo Nakamura as Deputy Mayor
* Yūnosuke Itō as Novelist
* Minosuke Yamada as Subordinate Clerk Saito
* Kamatari Fujiwara as Sub-Section Chief Ono
* Makoto Kobori as Kiichi Watanabe, Kanjis Brother
* Nobuo Kaneko as Mitsuo Watanabe, Kanjis son Atsushi Watanabe as Patient
* Noriko Honma as Housewife

==Reception==
The film has a 100% positive rating based on 30&nbsp;reviews from critics at the review aggregator website Rotten Tomatoes.   

Ikiru ranks 459th on Empire magazines 2008 list of the 500 greatest movies of all time.   Ranked #44 in Empire (magazine)|Empire magazines "The 100 Best Films Of World Cinema" in 2010. {{cite web 
| title = The 100 Best Films Of World Cinema – 44. Ikiru 
| url = http://www.empireonline.com/features/100-greatest-world-cinema-films/default.asp?film=44 
| work = Empire 
}} 
 

  Ebert called it Kurosawas greatest film.    

==Awards==
;Won
* 1954 4th Berlin International Film Festival:  "Special Prize of the Senate of Berlin" 
{{cite web | title = Berlin International Film Festival (1954) |publisher=IMDb| url = http://www.imdb.com/event/ev0000091/1954 
| accessdate = 2013-12-24 }} 

;Nominated
*     

==Remake==
*Ikiru was remade as a television drama that debuted on TV Asahi on September 9, 2007, and it starred the kabuki actor Matsumoto Kōshirō IX. Set in 2007, some characters and plot lines were changed.

==References==
 

==External links==
 
 
 
*  
*  
*  
* 
*  
*  
*     at the Japanese Movie Database

 
{{Navboxes title = Awards list =
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 