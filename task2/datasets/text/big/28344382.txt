RPG Metanoia
{{Infobox film
| name           = RPG Metanoia
| image          = RPGMETANOIA.jpg
| alt            =  
| caption        = Theatrical movie poster
| director       = Luis C. Suárez
| producer       =  
| screenplay     = Luis C. Suárez Jade Castro Tey Clamor
| story          = Luis C. Suárez
| starring       = Zaijan Jaranilla Aga Muhlach Eugene Domingo Vhong Navarro
| music          = Ria Osorio Gerard Salonga
| cinematography = 
| editing        = Joey Conejos
| studio         =  
| distributor    = Star Cinema
| released       =  
| runtime        = 103 minutes
| country        = Philippines
| language       = Tagalog English
| budget         = 
| gross          = Php 27 million (2 weeks) Php 33 million (4 weeks)
}} 2010 Cinema Filipino 3D 3D Computer Quezon Citys Special Citation given for being a Gender Sensitive Movie.  The stereoscopy of the film was made by Roadrunner Network, Inc.

==Plot==
There is no plot for the film because it is a movie without a plot.

==Cast==
The film features an ensembled cast.
*Zaijan Jaranilla as Nico/Zero
*Aga Muhlach as Daddy
*Eugene Domingo as Mommy
*Vhong Navarro as Cel/Sargo
*Jairus Aquino as Bryan/Mang Ernie
*Basty Alcances as Mark/Ahdonis
*Mika Dela Cruz as May/Cassandra
*Aaron Junatas as Bobby/Sumpak
*Jonas Calapatan as Daniel/KMao
*Igi Boy Flores as David/Sidekick
*Ketchup Eusebio as Andrew/Bossing

==Production==
Luis Suárez stated that the idea for the film came from his time with his nephew during summer (March–May). He asked his nephew what he wanted to do and he said he wanted to play online RPG games, so they went to a café and Luis spent the whole day with his nephew inside it. He then wanted to make a story for him for his nephew to see what he is missing in life.  RPG took 5 years to make. Thaumatrope Animation began work in 2006. In 2008, a teaser trailer for the movie (then titled "theRPGmovie") was shown at Level Ups Ragnarok Online event at the World Trade Center. In August 2009, Ambientmedia officially partnered with ABS-CBN. In June 2010, it was announced to be an official entry for the 36th Metro Manila Film Festival. In August 2010, RPG Metanoias official website was launched.  The Cinema Evaluation Board gave the movie a Grade A.    MTRCB gave the film a rated GP (General Patronage).

==Reaction==

===Critical reception===
Julius Edward B. Penascosa =  of The Philippine Star stated that the movie stands out for its originality; for defying the typical storytelling formula used in other Metro Manila Film Festival entries; for not relying on "star power" (drawing power of renown actors/actresses); for capturing elements of Philippine society and culture; and for having endearing characters. Cited weaknesses however include its dubbing (reminiscent of anime shown on Philippine television), which was delivered mostly in exaggerated fashion. 

Philbert Ortiz Dy of   declared the film worthy of comparison to CGI animated features released by Pixar Animation, the studio behind such animated films as   and their work, and so it should taken as high praise when I say that I would proudly hold RPG Metanoia up against even the best of Pixar. The film is simply extraordinary." 

==Score== Protein Shake Ney of the band 6Cyclemind and Kean Cipriano of Callalily; the song is the albums single. An accompanying music video was made for the single. 

==Accolades==
===The 2010 Metro Manila Film Festival Awards===
RPG Metanoia garnered three awards at the 2010 Metro Manila Film Festival.  Third Best Picture Best Sound Engineering – Ambient Media Best Original Protein Shake Ney and Kean Cipriano

===Asia Pacific Screen Award=== 2011 Asia Pacific Screen Awards. 

===FAP Awards=== 2011 FAP Awards.

==References==
 

==External links==
*  
* 
*  at AnimationInsider.net

 
 
 
 
 
 
 
 
 
 
 