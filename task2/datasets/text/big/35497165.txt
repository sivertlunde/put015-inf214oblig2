Britain at Bay
{{Infobox film
| name           = Britain at Bay
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        =  Harry Watt
| producer       = 
| writer         = J. B. Priestley
| screenplay     = 
| story          = 
| based on       =  
| narrator       = J. B. Priestley
| starring       = 
| music          = Richard Addinsell
| cinematography = James E. Rogers
| editing        = 
| studio         = GPO Film Unit
| distributor    = 
| released       =  
| runtime        = 8 mins
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 Ministry of Information to raise morale following the Fall of France in June 1940.    It was released in other countries under the title Britain on Guard.

==Synopsis==
A compilation of footage with accompanying narration by J. B. Priestley. The film opens with images of rural and urban Britain, and then depicts the rise of Nazi Germany through newsreel footage, including the recent Fall of France. Priestley says that Britain is responsible for "the future of the civilised world", as iconic images of Britain are shown, including shots of Big Ben and the White Cliffs of Dover. Civilians are seen contributing towards the war effort and scenes of allied troops are juxtaposed with an extract from Winston Churchills "We Shall Fight on the Beaches" speech.

==Production==
The film was produced with the purpose of raising morale. 

Britain at Bay is an example of compilation documentary, in which various film extracts are assembled into a coherent whole, accompanied by a commentary to explain the intended message.  This form of documentary was used for other British wartime propaganda films, such as Words for Battle.

==References==
 

==External links==
*   at BFI Screenonline
*  

 
 
 
 


 