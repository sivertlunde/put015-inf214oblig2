Nanjundi
{{Infobox film
| name = Nanjundi
| image = 
| caption =
| director = S. R. Brothers
| writer = S. R. Brothers
| starring = Shivrajkumar  Debina Bonnerjee   Umashri   Lokesh
| producer = Ramu
| music = Hamsalekha (250th film work)
| cinematography = B. Suresh Babu
| editing = Jo Ni Harsha
| studio = Ramu Enterprises
| released =  
| runtime = 155 minutes
| language = Kannada
| country = India
| budget =
}}
 drama film Kambala race that happens along the coastal region of Karnataka.

The film featured original score and soundtrack composed and written by Hamsalekha which marked his 250th film work. The film scored the Karnataka State Film Award for Best Sound Recording award for the year 2003-04. 

== Cast ==
* Shivarajkumar as Nanjundi
* Debina Bonnerjee
* Umashri 
* Lokesh
* Suresh Heblikar Ramakrishna
* Doddanna
* Karibasavaiah
* Sadhu Kokila
* M.N Lakshmi Devi
* Chitra Shenoy
* Shobha Raghavendra
* M. P. Shankar
* M. S. Karanth
* Biradar

== Soundtrack ==
The music was composed by Hamsalekha scoring for his 250th film. Veteran singers like S. Janaki, K. J. Yesudas, S. P. Balasubrahmanyam among others have sung for the songs as a mark to their long association with the composer.  The soundtrack is said to be purely associated with traditional musical instruments without any usage of the modern electronic instruments.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Kayuthalanno Namma
| extra1 = S. P. Balasubrahmanyam
| lyrics1 = Hamsalekha
| length1 = 
| title2 = Nanjundi Haadu
| extra2 = S. Janaki
| lyrics2 = Hamsalekha
| length2 = 
| title3 = Andada Maneya
| extra3 = Madhu Balakrishnan
| lyrics3 = Hamsalekha
| length3 = 
| title4 = Thayi Endalli Januma Ide
| extra4 = K. J. Yesudas
| lyrics4 = Hamsalekha
| length4 = 
| title5 = Koli Ko Koli
| extra5 = Rajesh Krishnan
| lyrics5 = Hamsalekha
| length5 = 
| title6 = Deepadinda Deepava
| extra6 = Madhu Balakrishnan, Nanditha
| lyrics6 = Hamsalekha
| length6 = 
| title7 = Baaro Maleraya
| extra7 = Madhu Balakrishnan
| lyrics7 = Hamsalekha
| length7 =
| title8 = Chellidaro Malligeya
| extra8 = Chetan Sosca, Nanditha
| lyrics8 = Hamsalekha
| length8 = 
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 

 