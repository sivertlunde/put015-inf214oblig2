The Devil's Daffodil
 
{{Infobox film
| name           = The Devils Daffodil
| image          = 
| caption        = 
| director       = Ákos Ráthonyi Donald Taylor
| writer         = Basil Dawson Egon Eis Edgar Wallace
| narrator       =  William Lucas, Joachim Fuchsberger
| music          = Keith Papworth
| cinematography = Desmond Dickinson Peter Taylor
| studio         = Omnia Pictures Ltd., Rialto Film
| distributor    = Prisma Filmverleih
| released       =  
| runtime        = 97 minutes
| country        = United Kingdom West Germany
| language       = English German
| budget         = 
}}
 William Lucas in the English version and Joachim Fuchsberger in the German one.

==Cast==

{| class="wikitable"
|-
! Actor British version !! Role !! Actor German version
|- William Lucas || Jack Tarling || Joachim Fuchsberger
|-
|   || Anne Rider ||   
|-
| Ingrid van Bergen || Gloria Lyne || Ingrid van Bergen
|-
| Albert Lieven || Raymond Lyne || Albert Lieven
|-
| Jan Hendriks || Charles || Jan Hendriks
|-
| Marius Goring || Oliver Milburgh || Marius Goring
|-
| Peter Illing || Mr. (Jan) Putek || Peter Illing
|-
| Walter Gotell || Supt. Whiteside || Walter Gotell
|-
| Christopher Lee || Ling Chu || Christopher Lee
|-
| Colin Jeavons || Peter Keene || Klaus Kinski
|}

==Production== Donald Taylor. The German dialogue was written by Horst Wendlandt and  . Wendlandt was also co-producer along with Preben Philipsen (both of Rialto Film). 

Cinematography took place in April and May 1961 in London and environments. The studio was Shepperton Studios/Middlesex.   

==Reception== FSK gave the film a rating of "16 and up" and found it not appropriate for screenings on public holidays. The German version premiered on 21 July 1961. 

==See also==
* Christopher Lee filmography

==References==
 

==External links==
* 
*http://ftvdb.bfi.org.uk/sift/title/65203

 

 
 
 
 
 
 
 
 
 
 
 
 