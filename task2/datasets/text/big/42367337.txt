Bajirao Mastani (film)
 
 
{{Infobox film
| name           = Bajirao Mastani
| image          = 
| alt            = 
| caption        = 
| director       = Sanjay Leela Bhansali
| producer       = Sanjay Leela Bhansali
| writer         = 
| starring       = Ranveer Singh Deepika Padukone Priyanka Chopra
| music          = Sanjay Leela Bhansali
| cinematography =  
| editing        =
| studio         = SLB Films
| distributor    = Eros International  
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} Indian historical Maratha Peshwa Baji Rao I and his second wife Mastani. Ranveer Singh and Deepika Padukone portray the titular protagonists, Priyanka Chopra plays Raos first wife, and Tanvi Azmi plays Raos mother. Bajirao Mastani is scheduled for release on 25 December 2015.  

==Cast==
* Ranveer Singh as Baji Rao I, a warrior and the second Peshwa of the Maratha Empire, from 1720 until his death. 
* Deepika Padukone as Mastani, the second wife of Baji Rao.
* Priyanka Chopra as Kashibai, the first wife of Baji Rao. 
* Tanvi Azmi as Radhabai, Bajiraos mother.   
* Sukhada Khandkekar as Anubai, Baji Raos sister.   
* Anuja Gokhale as Bhiubai, Baji Raos sister. 
* Vaibbhav Tatwawdi as Chimaji Appa, Baji Raos younger brother.   
* Mahesh Manjrekar   
* Milind Soman 
* Ayush Tandon

==Production==

=== Casting ===
The film was first announced in 2003. Sanjay Leela Bhansali initially wanted to cast Salman Khan and Aishwarya Rai (his lead pair from Hum Dil De Chuke Sanam) for the titular roles, but could not cast them together after their highly publicised break-up.    In July 2003, there were reports that Bhansali had cast Khan and Kareena Kapoor as the lead pair with Rani Mukerji playing Bajiraos first wife.  During his appearance on season 1 of the popular talk show Koffee with Karan, Bhansali stated that he scrapped the casting of Khan and Kapoor after the paired signed other films together as he wanted to be the first director to showcase their pairing.  Bhansali shelved Bajirao Mastani and went on to direct Black (2005 film)|Black (2005), Saawariya (2007), Guzaarish (2010), and Goliyon Ki Raasleela Ram-Leela (2013). Throughout these ten years, media speculation continued about the production and casting of Bajirao Mastani. Several actors were rumoured to be linked to the project including Shahrukh Khan,    Ajay Devgn,  Hrithik Roshan,  Ranveer Singh,    Deepika Padukone,  and Katrina Kaif. 

In July 2014, it was confirmed that Bhansali had revived the project with Ranveer Singh and Deepika Padukone (the lead pair of Bhansalis Goliyon Ki Raasleela Ram-Leela) playing the titular roles and Priyanka Chopra playing Bajiraos first wife, Kashibai.  Both Singh and Padukone will undergo extensive preparation for their roles. Bhansali has reportedly approached Daksha Sheth to train the pair in Kalaripayattu, an ancient Indian martial art. Sheth will also train Padukone in the dance style of Kathak.  Singh will be required to learn Marathi, learn horseback riding, and shave his head for the role.  In addition, the role of Baji Rao will require Singh to gain weight to lose his physique.  In preparation for her role of Kashibai, Chopra took a 15-day coaching course and she will also undergoing language training in a dialect of Marathi spoken during the time of the ascension of the Peshwas. 

In September 2014, it was confirmed that Tanvi Azmi was cast for the role of Bajiraos mother. Other actresses reportedly considered for this role were Shabana Azmi, Dimple Kapadia, and Supriya Pathak.  Tanvi has gone completely bald for her role.  In November 2014, the actresses for the roles of Bajiraos sisters were finalised with Sukhada Khandkekar cast as Anubai
and Anuja Gokhale as Bhiubai.  Mahesh Manjrekar has been cast to play a Maratha king in the film.  The films costumes will be designed by Anju Modi.  It was announced in January 2015 that actor Vaibhav Tatwawadi was chosen to play Bajiraos younger brother, Chimaji Appa. 

===Filming===
Principal photography of the film started on 23 September 2014 with Singh and Chopra.     Deepika Padukone has joined the crew of the movie on 8 March 2015 in Rajasthan.  She will be seen doing physically tough scenes such as sword fighting and others along with sporting the 20 kg armour in the movie.  Previously, Padukone shot for a crucial scene for two days during the end of December 2014 and will resume shooting in March 2015, after completing work for her other projects.  A major portion of the film will be shot in Mumbais Film City Studios.    For the outdoor sequences, Bhansali has finalised Madhya Pradesh, Rajasthan, Gujarat, and Wai, Maharashtra|Wai.    

== Soundtrack==
The soundtrack for the film will be composed by Bhansali. 

==References==
 

==External links==

*  
*  


 
 
 
 
 
 
 
 
 
 
 