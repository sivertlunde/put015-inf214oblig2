Becky Sharp
 
{{Infobox film
| name           = Becky Sharp
| image          = Beckysharp1935.jpg
| image_size     =
| caption        =
| director       = Rouben Mamoulian
| producer       = Kenneth Macgowan Rouben Mamoulian Robert Edmond Jones
| writer         = Story:  
| narrator       =
| starring       = Miriam Hopkins Alan Mowbray Frances Dee Cedric Hardwicke
| music          = Roy Webb
| cinematography = Ray Rennahan
| editing        = Archie Marshek
| studio         = Pioneer Pictures
| distributor    = RKO Radio Pictures
| released       =     In New York, the film premiered at Radio City Music Hall. 
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} historical drama Vanity Fair. The screenplay was written by Francis Edward Faragoh. The film was considered a landmark in cinema as the first film to use the newly developed three-strip Technicolor production, opening the way for a growing number of color films to be made in Britain and the United States in the years leading up to World War II.

The film recounts the tale of a lower-class girl who insinuates herself into an upper-class family, only to see her life and the lives of those around her destroyed. The ruthless, self-willed and beautiful Becky is one of the most famous characters in English literature.

==Color development== The Cat La Cucaracha (31 August 1934).

Becky Sharp is now in the public domain.

Miriam Hopkins was nominated for the Academy Award for Best Actress for her performance.

==Plot==
Becky Sharp ( ), Joseph Sedley (Nigel Bruce), Rawdon Crawley (Alan Mowbray), and George Osborne (G. P. Huntley Jr).
 British society and becomes the scourge of the social circle, offending the other ladies such as Lady Bareacres (Billie Burke).

Finally, Sharp falls into the humiliation of singing for her meals in a beer hall. But Becky never stays down for long.

==Cast==
 
* Miriam Hopkins as Becky Sharp
* Frances Dee as Amelia Sedley
* Cedric Hardwicke as Marquis of Steyne
* Billie Burke as Lady Bareacres
* Alison Skipworth as Miss Crawley
* Nigel Bruce as Joseph Sedley
* Alan Mowbray as Rawdon Crawley
* G.P. Huntley as George Osborne
* William Stack as Pitt Crawley George Hassell as Sir Pitt Crawley
* William Faversham as Duke of Wellington Charles Richman as Gen. Tufto
* Doris Lloyd as Duchess of Richmond
* Colin Tapley as Captain William Dobbin
* Leonard Mudie as Tarquin Charles Coleman as Bowles

==Production== John Hay A Star Nothing Sacred (both 1937), were produced by Selznick, copyrighted by Pioneer Pictures, and released through United Artists rather than RKO.

Lowell Sherman, the original director, developed pneumonia and died early in the filming of Becky Sharp. His replacement, Rouben Mamoulian, scrapped all of the original footage and started over. Sherman mostly filmed several pieces of test footage, which have survived.

==Preservation==
For many years, the original three-color Technicolor version of the film was not available for viewing, though a 16 millimeter version was available. This version had been printed (poorly) on two-color Cinecolor stock which did not accurately reproduce the colors of the original film. The smaller film stock also resulted in a grainier, inferior image.

In the 1980s the UCLA Film and Television Archive restored the film, under the supervision of archivist Robert Gitt. Rouben Mamoulian appeared at the premiere of the restored print at the Academy of Motion Picture Arts and Sciences theatre in Beverly Hills.

==Awards==
Wins
* Venice Film Festival: Best Color Film, Rouben Mamoulian; 1935.

Nominations
* Academy Awards: Best Actress in a Leading Role, Miriam Hopkins; 1935.
* Venice Film Festival: Mussolini Cup, Rouben Mamoulian; 1935.

==Trivia== Pat Nixon (then Pat Ryan), later the wife of Richard Nixon and First Lady of the United States from 1969 to 1974, worked as a movie extra at this time, and can be seen as a walk on during the ball scene.

==References==
 

==External links==
*  
*  
*   at Film Reference web site.
*  
*  
*  on Theatre Royal: July 21, 1954

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 