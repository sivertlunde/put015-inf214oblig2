Tara (2013 film)
 

{{Infobox film
| name= Tara 2013
| image=
| alt=
| caption=
| director=Kumar Raj
| producer=Kumar Raj
| writer=Kishan Pawar
| starring=Rekha Rana Rohan Shroff Sapna P Chuobisha Brijesh K. Kori Ujjwala Shikhare S. C. Makhija Rohit Raj
 Archana Tendulkar Ashish Salim
| music=Prakash Prabhakar
| cinematography=Suresh Beesaveni
| editing=Ajay Verma
| studio=
| distributor=Kumar Raj Productions
| released=  
| runtime=
| country=India
| language=Hindi
}}

Tara  is a 2013 Hindi drama film directed and produced by Kumar Raj. It was released on July 12, 2013. The film features Rekha Rana, Rohan Shroff and Sapna P Chuobisha as main characters.
 

==Cast==

 

==Crew==
Story, Screenplay & Dialogues: Dr. Prof. Kishan Pawar  
Costume: Pooja Valeja  & Meena Valeja  
Chartered Accountant:  Neha Valeja  
Legal Consultant: Manu Malkani  
Financial Adviser: Sonam Rajendra Raney  
Production Head: Girish Chandra Arya  
Associate Directors: Brijesh K Kori, Raji Palakadan, Amit Parwatkar  
Creative Director:  Utpal Chakraborty  
Assistant Directors: Rohit Pasi, Sonu Yadav, Vishal Sharma, Krishna Aryal, Umair Siddiqui, Neelkamal, Rahul Pandit, Pankaj Pathak, Baljeet K Singh,                Ranjeet Jaiswal, Ajay Mishra  
Chief Camera Assistant:   Vishal D Jaiswal 
Action: Rajendra Singh 
Choreographer: Lollipop Pradhan & Jyotika  
Visual Effects & Titles: Sanket Mangrule & Vaibhav Adurkar 
Song & Music Recording: Panchamda Sur Studio & S.K. Music Works 
Production Designers: Mala Vazirani 
Casting Director: Deepak Ambawadekar  
Publicity Design: Brijesh Pandit & Shahnawaz Shaikh 
Line Producer: Pawan Rana, Pooja & Vinod Harisingani 
Line Producer Gujarat: Manmath Nimavat 
Executive Producer: Vinod Yadav 
Art Director: Vinita Sharma 
Sound Design: Anirbhan Borthakur 
Re- Recording Mixer: Anup Dev 
Creative Consultant: Rohit Raney 
Locations: Prakash Sadhwani 
Promo Designer & Editor: Ajay Varma 
Lyrics: Tanveer Ghazi  & Devmani Pandey 
Music Director: Prakash Prabhakar     
Music Director: Siddharth Kasyap  
( Pardesi Raja Song Composed By Siddharth Kasyap ) 
Director Of Photography: Suresh Beesaveni 
Co. Producer: Dharm 
Produced   by: Kumar Raj Productions 
A Film by  Kumar Raj 
Presented By Kumar Raj Productions & H.S. Cine Art Pvt. Ltd.

==References==
 

==External links==
* .

 
 
 
 
 


 