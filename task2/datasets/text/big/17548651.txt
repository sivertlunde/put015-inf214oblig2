City Without Baseball
 
 
 
{{infobox film
| name = City Without Baseball
| image = City Without Baseball poster.jpg
| caption = Theatrical release poster
| director = Lawrence Ah Mon Scud (stage name of Danny Cheng Wan-Cheung)
| producer = Heman Peng 
| Associate Producer = Scud   Ron Heung Tze-Chun Leung Yu-Chung Yuan Lin John Tai Ji-Ching Monie Tung Man-Lee
| music = Eugene Pao 
| cinematography = Ying Zhang    
| editing = Kwok-Wing Leung (as Jacky Leung)
| studio = ArtWalker Productions
| distributor = Golden Scene (2007) (worldwide) (all media)
| released =  
| runtime = 100 minutes
| country = Hong Kong
| language = Cantonese
}}
City Without Baseball ( ; stylized:  無野の城 ;   and other members of the   in 2009, Amphetamine (film)|Amphetamine in 2010, Love Actually... Sucks! in 2011, and his most recent, Voyage (2013 film)|Voyage, in 2013. Two more films, one named Utopians, and the other, Naked Nation, are currently in production. 

==Plot==
The actual members of the Hong Kong National Baseball Team appear in the film as themselves, in a story set in 2004. Their isolated existence leads them to take unconventional choices in both love and friendship, and to summon great courage in the face of their lonely and disconnected existence.
The story focuses on the easy-going, yet often detached, main character, Ronnie, as played by Ron Heung, and his friendships and relationships with others, both on and off the sports field.

==Awards==
* 2008 Hong Kong Film Critics Society Awards Winners for Top 7 Suggested Films
* 2008 Taiwan Film Critics Society Awards Winners for Top 10 Best Chinese Films

==Films by the same director/producer== Permanent Residence (2009)
* Amphetamine (film)|Amphetamine (2010)
* Love Actually... Sucks! (2011)
* Voyage (2013 film)|Voyage (2013)

==Home media==
A Panorama Distributions Co.:
* VCD first became available on 2008-10-03 (3 October 2008)
* DVD (DVD Region 3) first became available on 2008-10-10 (10 October 2008)
* Blu-ray Disc first became available on 2011-06-03 (3 June 2011)

==See also==
* Hong Kong films of 2008
* List of lesbian, gay, bisexual or transgender-related films
* List of lesbian, gay, bisexual, or transgender-related films by storyline Nudity in film (East Asian cinema since 1929)

==References==
 

==External links==
*    
*  
*   in the Chinese Movie Database
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 