Brief Interviews with Hideous Men (film)
{{Infobox film
| name           = Brief Interviews with Hideous Men
| image          = Brief interviews poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = John Krasinski
| producer       = John Krasinski Kevin Patrick Connors Thomas Fatone Chris Hayes
| writer         = John Krasinski
| starring       = Julianne Nicholson
| music          = Billy Mohler Nate Wood John Bailey
| editing        = Zene Baker Rich Fox
| studio         = 
| distributor    = IFC Films
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $27,935
}} short story collection of the same name by David Foster Wallace.

==Plot==
Sara Quinn (Julianne Nicholson) is interviewing men as part of her graduate studies. Her intellectual endeavor has emotional consequences as the men’s twisted and revealing stories are juxtaposed against the backdrop of her own experience. As she begins to listen closely to the men around her, Sara must ultimately reconcile herself to the darkness that lies below the surface of human interactions.

==Cast==
* Julianne Nicholson as Sara Quinn
* Josh Charles as Subject #2
* Christopher Meloni as R / Subject #3
* Will Arnett as Subject #11
* Ben Shenkman as Subject #14
* Michael Cerveris as Subject #15
* Chris Messina as Subject #19
* John Krasinski as Ryan / Subject #20
* Ben Gibbard as Harry / Subject #20
* Lou Taylor Pucci as Evan / Subject #28
* Max Minghella as Kevin / Subject #28
* Timothy Hutton as Prof. Adams / Subject #30
* Clarke Peters as Subject #31
* Bobby Cannavale as Subject #40
* Frankie Faison as Subject #42
* Dominic Cooper as Daniel / Subject #46
* Corey Stoll as Subject #51
* Joey Slotnick as Tad / Subject #59
* Will Forte as Subject #72
* Rashida Jones as Hannah

==Production==
The film was shot in studios in Brooklyn, New York and exterior filming took place in Staten Island, New York, and on the campuses of Columbia University and Brooklyn College. 

==Reception==
The film was selected for the US Dramatic Competition in the 2009 Sundance Film Festival.  It scored 40% at Rotten Tomatoes  and 44% at Metacritic. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 