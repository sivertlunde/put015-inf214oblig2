List of Malayalam films of 1991
 

The following is a list of Malayalam films released in the year 1991.

{| class="wikitable sortable"
! width="25%" | Title
! width="18%" | Director
! width="18%" | Story
! width="18%" | Screenplay
! width="18%" | Cast
|- valign="top"
| Souhrudam
| Shaji Kailas
| &nbsp;
| &nbsp;
| Mukesh (actor)|Mukesh, Urvashi (actress)|Urvashi, Saikumar (Malayalam actor)|Saikumar, Parvathi
|- valign="top"
| Abhayam (1991 film)|Abhayam
| Sivan
| &nbsp;
| &nbsp;
|- valign="top"
| Apoorvam Chilar
| Kaladharan
| &nbsp;
| &nbsp;
| Innocent (actor)|Innocent, Jagathy Sreekumar, Parvathi, K.P.A.C. Lalitha
|- valign="top"
| Njan Gandharvan
| P. Padmarajan
| &nbsp;
| &nbsp;
| Nitish Bharadwaj, Suvarna
|- valign="top"
| Chavettu Pada
| Sekhar
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Ennum Nanmakal
| Sathyan Anthikkad
| &nbsp;
| &nbsp;
| Jayaram, Shanti Krishna, Sreenivasan
|- valign="top"
| Eagle (1991 film)|Eagle
| Ambili
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Perumthachan (film)|Perumthachan Ajayan
| &nbsp;
| &nbsp;
| Thilakan, Prashanth Thiagarajan|Prashanth, Monisha Unni, Vinaya Prasad
|- valign="top"
| Ganamela
| Ambili
| &nbsp;
| &nbsp;
| Mukesh (Malayalam actor)|Mukesh, Innocent (actor)|Innocent, Geetha Vijayan
|- valign="top"
| Aavanikunnile Kinnaripookkal
| Paul Babu
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Amaram
| Bharathan
| &nbsp;
| A. K. Lohithadas
| Mammootty, Mathu, Murali (Malayalam actor)|Murali, Ashokan
|- valign="top"
| Raid (1991 film)|Raid
| K. S. Gopalakrishnan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Dhanam
| Sibi Malayil
| &nbsp;
| A. K. Lohithadas
| Mohanlal
|- valign="top"
| Kakka Thollayiram
| V. R. Gopalakrishnan
| &nbsp;
| &nbsp; Saikumar
|- valign="top"
| Pookkalam Varavayi Kamal
| &nbsp;
| &nbsp; Geetha
|- valign="top" Parallel College
| Thulasidas
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Oru Tharam Randu Tharam Moonu Tharam
| P.K. Radhakrishnan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Kuttapathram
| R. Chandru
| &nbsp;
| &nbsp;
| Suresh Gopi
|- valign="top"
| Aakasha Kottayile Sultan
| Jayaraj
| &nbsp;
| &nbsp;
| Sreenivasan (actor)|Sreenivasan, Saranya
|- valign="top"
| Poonthenaruvi Chuvannu Balu
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Agni Nilavu
| N. Sankaran Nair
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Raagam Anuragam
| Nilkhill
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Mizhikal Suresh Krishna
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Ezhunnallathu Harikumar
| &nbsp;
| &nbsp; Siddique

|- valign="top"
| Arangu (film)|Arangu
| Chandrasekharan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Mookilla Rajyathu
| Ashokan-Thaha
| &nbsp;
| &nbsp;
| Mukesh (Malayalam actor)|Mukesh, Siddique (actor)|Siddique, Thilakan, Jagathy, Vinaya Prasad

|- valign="top"
| Nayam Vyakthamakkunnu
| Balachandra Menon
| &nbsp;
| &nbsp;
| Mammootty, Shanthi Krishna
|- valign="top"
| Bharatham
| Sibi Malayil
| &nbsp;
| A. K. Lohithadas Lakshmi
|- valign="top"
| Omana Swapnangal
| P. K. Balakrishnan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Vishnu Lokam Kamal
| &nbsp;
| &nbsp; Urvashi
|- valign="top"
| Ente Sooryaputhrikku Fazil
| &nbsp;
| &nbsp;
| Srividya, Amala (actress)|Amala, Suresh Gopi
|- valign="top"
| Nattu Vishesham
| Paul Njarackel
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Thudar Katha
| Dennis Joseph
| &nbsp;
| &nbsp;
| Saikumar (Malayalam actor)|Saikumar, Mathu
|- valign="top"
| Georgekutty C/O Georgekutty
| K.K. Haridas
| &nbsp;
| &nbsp;
| Jayaram, Sunitha (actress)|Sunitha, Thilakan
|- valign="top"
| Amina Tailors Sajan
| &nbsp;
| &nbsp;
| Risa bava, Parvathy Jayaram|Parvathy, Rajan P. Dev
|- valign="top" Teenage Love
| J. Krishnachandra
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Inspector Balram
| I. V. Sasi
| &nbsp;
| T. Damodaran Geetha
|- valign="top"
| Kankettu
| Rajan Balakrishnan
| &nbsp;
| &nbsp;
| Jayaram, Sreenivasan (actor)|Sreenivasan, Shobana, Geetha Vijayan
|- valign="top"
| Sundhari Kakka
| Mahesh Soman
| &nbsp;
| &nbsp;
| Rekha, Jagadish, Siddique (actor)|Siddique, Risabava
|- valign="top"
| Mahassar
| C. P. Vijayakumar
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Vasthuhara
| G. Aravindan
| &nbsp;
| &nbsp;
| Mohanlal, Neena Gupta, Shobana
|- valign="top"
| Adayallam
| K. Madhu
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Kalamorukkam
| B. S. Indran
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Ennathe Programme
| P. G. Viswambaran
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Cheppukilukkana Changathi
| Kaladharan
| &nbsp;
| &nbsp;
| Mukesh (Malayalam actor)|Mukesh, Jagadish
|- valign="top"
| Miss Stella Sasi
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Keli
| Bharathan
| &nbsp;
| &nbsp;
| Jayaram
|- valign="top"
| Daivasahayam Lucky Centre
| Rajan Chevayoor
| &nbsp;
| &nbsp;
| Risa Bava
|- valign="top"
| Mimics Parade
| Thulasidas
| &nbsp;
| Kaloor Dennis
|  Siddique (actor)|Siddique, Sunitha (actress)|Sunitha, Jagadish, Asokan, Zainuddin (actor)|Zainuddin, Suchitra
|- valign="top"
| Koodikazhca
| T. S. Suresh Babu
| &nbsp;
| &nbsp;
| Jayaram, Jagadish, Urvashi (actress)|Urvashi, Usha
|- valign="top"
| Manmadha Sarangal
| Baby
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Kanalkattu
| Sathyan Anthikkad
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Mukha Chithram
| Suresh Unnithan
| &nbsp;
| &nbsp;
| Jayaram, Urvashi (actress)|Urvashi, Siddique (actor)|Siddique, Sunitha (actress)|Sunitha, Jagadish

|- valign="top"
| Bhoomika (film)|Bhoomika
| I. V. Sasi
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Vashyam Suresh
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Uncle Bun
| Bhadran
| &nbsp;
| &nbsp; Khushboo
|- valign="top"
| Anashwaram
| Joemon
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Kilukkam
| Priyadarsan
| &nbsp;
| Venu Nagavally
| Mohanlal, Revathi, Jagathy, Innocent (actor)|Innocent, Thilakan
|- valign="top"
| Khanta Kavyam
| Vasan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Ottayal Pattalam
| Rajiv Kumar
| &nbsp;
| &nbsp;
| Mukesh (Malayalam actor)|Mukesh, Madhoo
|- valign="top"
| Aadhyamayi
| J. Vattoli
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Nagarathil Samsara Vishayam
| Prasanth
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Ulladakkam Kamal
| &nbsp;
| &nbsp;
| Mohanlal, Amala (actress)|Amala, Shobhana
|- valign="top"
| Kadinjool Kalyanam
| Prathap Kumar
| &nbsp;
| &nbsp;
| Jayaram, Urvashi (actress)|Urvashi, Suchitra, Jagadish

|- valign="top"
| Aanaval Mothiram
| G. S. Vijayan
| &nbsp;
| &nbsp;
| Sreenivasan (actor)|Sreenivasan, Suresh Gopi, Saranya
|- valign="top"
| Venal Kinavukal
| K. S. Sethumadhavan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Post Box No. 27
| P. Anil
| &nbsp;
| &nbsp; Kalpana
|- valign="top"
| Kizhakkunarum Pakshi
| Venu Nagavalli Leon
| &nbsp;
| Mohanlal, Rekha, Shankar
|- valign="top"
| Sandhesam
| Sathyan Anthikkad
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Kalari (film)|Kalari
| Prassi Mallur
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Onnaam Muhurtham
| Rahim Chelavoor
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Naagam (1991 film)|Naagam
| K. S. Gopalakrishnan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Godfather (1991 film)|Godfather
| Siddique Lal
| &nbsp;
| &nbsp;
| Mukesh (Malayalam actor)|Mukesh, N. N. Pillai, Thilakan, Innocent (actor)|Innocent, Kanaka (actress)|Kanaka, Bheeman Raghu, Jagadish, Philomina
|- valign="top"
| Nettippattam
| Kaladharan
| &nbsp;
| &nbsp;
| Sreenivasan (actor)|Sreenivasan, Jagadish, Rekha
|- valign="top"
| Chakravarthy (1991 film)|Chakravarthy
| A. Sreekumar
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Neelagiri (film)|Neelagiri
| I. V. Sasi
| &nbsp;
| &nbsp;
| Mammootty, Sunitha (actress)|Sunitha, Madhoo, Anju
|- valign="top"
| Kadalora Kattu
| C. P. Joemon
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Koumara Swapnangal
| K. S. Gopalakrishnan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Premolsavam
| M. S. Unni
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Sandhwanam
| Sibi Malayil
| &nbsp;
| &nbsp;
| Nedumudi Venu, Meena Durairaj|Meena, Suresh Gopi, Rekha
|- valign="top"
| Kilukkampetti
| Shaji Kailas
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Abhimanyu (1991 film)|Abhimanyu
| Priyadarsan
| &nbsp;
| &nbsp; Geetha
|- valign="top"
| Irikku M.D. Akathundu
| P. G. Viswambaran
| &nbsp;
| &nbsp;
|  Mukesh (Malayalam actor)|Mukesh, Sunitha (actress)|Sunitha, Jagadish, Geetha Vijayan
|- valign="top"
| Chanchattam (1991 film)|Chanchattam
| Thulasidas
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Oru Prathyeka Ariyippu
| R. S. Nair
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Vaisakha Rathiri
| Jayadevan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Kadavu (film)|Kadavu
| M. T. Vasudevan Nair
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Yamanam
| Bharath Gopi
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Achan Pattalam
| Noranadu Ramachandran
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Bali
| V. K. Pavithran
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Aprahnam
| M. P. Sukumaran Nair
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Vembanad (film)|Vembanad
| Sivaprasad
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Neduveerppukal
| Suresh Heblikar
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Vasavadatta (film)|Vasavadatta
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Athirathan (film)|Athirathan
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Karppooradeepam
|&nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Aviraamam
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Kadhanayika
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Kadamkadha
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Veendum Oru Aadyaraathri
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Puthiya Adhyaayam
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Nagarathil Kallanmar
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Sandhyaragam (1991 film)|Sandhyaragam
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Aswathy (1991 film)|Aswathy
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Oru Yaathrayude Anthyam
| &nbsp;
| &nbsp;
| &nbsp;
| &nbsp;
|}

==Dubbed films==
{| class="wikitable sortable"
! width="25%" | Title
! width="18%" | Director
! width="18%" | Story
! width="18%" | Screenplay
! width="18%" | Cast
|- valign="top"
|- valign="top"
| Kattu Veeran
| Jabir Mubin
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Moordhanyam
| Sunilkumar Desai
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Goodbye To Madras
| K. S. Gopalakrishnan
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top" Master Plan
| Kumar Mahadeva
| &nbsp;
| &nbsp;
| &nbsp;
|- valign="top"
| Ina Pravukal
| Suraj R. Barjathya
| &nbsp;
| &nbsp;
| &nbsp;
|}

==References==
 
* 
* 
* 

 
 
 

 
 
 
 
 

 