Teddy Bear (1980 film)
{{Infobox film name            = Miś (Teddy Bear) image           = caption         = DVD cover for Miś writer          = Stanisław Tym Stanisław Bareja starring        = Stanisław Tym Barbara Burska Christine Paul-Podlasky director        = Stanisław Bareja music           = Jerzy Derfel cinematography  = Zdzisław Kaczmarek released        = 1980 runtime        = 111 minutes language  Polish
}}

  in Gdynia 2009]]
 cult Polish film directed by Stanisław Bareja.
 The Cruise censorship at the time. 

==Plot==

Rysiek (Stanislaw Tym|Stanisław Tym, who also wrote the screenplay), the shrewd manager of a state-sponsored sports club, has to get to London before his ex-wife Irena (Barbara Burska) does to collect an enormous sum of money from a savings account the two used to share in happier days.

  Byzantine scheme which involves the production of a movie with his friend. He uses this as an opportunity to track down his look-alike "borrowing" his passport to stop his wife. 

Hilarity ensues as Bareja gives the audience a guided tour of the corruption, absurd bureaucracy, pervasive bribery and flourishing black market that pervaded socialism in the Peoples Republic of Poland.

==Cast==

* Stanislaw Tym|Stanisław Tym as Ryszard Ochódzki and Stanisław Paluch
* Barbara Burska as Irena Ochódzka
* Krystyna Podleska (Christine Paul-Podlasky) as Aleksandra Kozel
* Krzysztof Kowalewski as Jan Hochwander
* Bronislaw Pawlik|Bronisław Pawlik as Stuwała
* Ewa Bem as herself Zofia Czerwińska as Irena Siwna
* Stanislaw Mikulski|Stanisław Mikulski as "Captain Ryś" a.k.a. "Wujek Dobra Rada" ("Uncle Good-Advice")
* Wojciech Pokora as Włodarczyk
* Eugeniusz Priwieziencew as a militiaman Hanna Skarżanka

== See also ==
*Cinema of Poland
*List of Polish language films

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 