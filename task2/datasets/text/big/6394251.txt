Flirt (1995 film)
{{Infobox film
| name           = Flirt 
| caption        = 
| image	=	Flirt FilmPoster.jpeg
| director       = Hal Hartley
| producer       = Ted Hope
| writer         = Hal Hartley
| starring       = Bill Sage Parker Posey Miho Nikaido Martin Donovan Dwight Ewell Ned Rifle Jeffrey Taylor
| cinematography = Michael Spiller
| editing        = Steve Hamilton
| distributor    = Artificial Eye
| released       =  
| runtime        = 85 minutes
| country        = United States Germany Japan
| language       = English
| budget         = 
| gross          = 
}}

Flirt is a 1995 drama film written and directed by Hal Hartley. 

==Introduction==
The story takes place in New York, Berlin and Tokyo, with each segment using the same dialogue.

==Plot==
In New York, Bill (Bill Sage) struggles to decide whether he has a future with Emily (Parker Posey), while attempting to restrain Walter (Martin Donovan), the angry husband of a woman he thinks he might be in love with.

In Berlin, Dwight (Dwight Ewell) has a similar experience with his lover, while the events that befall Miho in Tokyo take a more dramatic turn.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 