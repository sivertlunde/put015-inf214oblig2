Vaadaa
 
{{Infobox film
| name = Vaadaa 
| image = 
| image_size =
| caption = 
| director = A. Venkatesh (director)|A. Venkatesh 
| producer = M. R. Mohan Ratha
| writer = A. Venkatesh (director)|A. Venkatesh Pattukottai Prabhakar Vivek
| music = D. Imaan
| cinematography = K. S. Selvaraj
| editing =  V. T. Vijayan
| distributor = Screen Play Entertainment Ptd. Ltd. 
| released = 15 October 2010 
| runtime = 
| country = India
| language = Tamil
| budget =
| gross =
| preceded_by =
| followed_by =
}} Tamil action film directed by A. Venkatesh (director)|A. Venkatesh. The film featured Sundar C. in the lead role, and finally released after several postponements on October 15, 2010.
 

==Plot==
Vetrivel is a person who lives as a temporary driver in Rishikeh, Uttranchal. Annamalai is the owner of Annamalai Transports but not the building-cum-residence of the transports and the staff. He is very innocent and funny. Vivek always arranges a marriage bride for himself but in one way or another Sundhar C. will stop the marriage. Anjali is an ardent fan of MGR and she feels her husband should also be like him. She is the very beautiful and attractive daughter of the Annamalai Transport building owner and she comes to Rishikesh to check the buildings condition and to collect the yearly rent. She meets Sundhar C. and falls in love. Sundhar C. also falls in love with Anjali and reveals his past to her. Sundar C. was a highly accomplished district collector  and was transferred to Tanjore. He gets himself in a cat-and-mouse chase with local goon Naachiyaar. Naachiyaars son kidnaps Sundhar C.s friends sister who was engaged to another man & tries to rape her. But Sundhar C. comes in and saves the girl and destroys Naachiyaars sons reproductive capacity. Naachiyaars son dies in hospital. The marriage of the girl happens happily. Naachiyaar kills Sundhar C.s friend brutally for revenge. Then the governor comes to Tanjore and CBCID informs that an attempt will be made on governors life at a Tanjore meeting. Sundhar C. is told to protect the governor as he trained for IPS first and was an excellent shooter. But the governor is killed and Sundhar C. is framed by the DGP himself. The rest of the film shows how Sundhar C. proves himself (he does) and again stops another marriage of Vivek after returning to being a collector.

==Cast==

* Sundar C. as Vetrivel
* Sheryll Brindo as Anjali Vivek as Annamalai Raj Kapoor
* Kiran Rathod as Manjaari
* Deepa Venkat Prem as Prem
* Kushboo Sundar (Special appearance)

==References==
 

==External links==
*  

 

 
 
 
 
 


 