Familia (film)
{{Infobox film|
  name           = Familia |
  image          = Familia (film).jpg |
  director       = Louise Archambault |
  writer         = Louise Archambault |
  starring       = Sylvie Moreau   Mylène St. Sauveur   Macha Grenon |
  producer       = Luc Déry Kim McCraw |
  released       =   |
  runtime        = 102 minutes |
  magic          = Ramachandra Bocar|
  editing        = Sophie Leblond|
  country        = Canada |
  language       = French   English|
  budget         = |
  }}
Familia is a 2005 French-language Canadian drama film. It was directed and written by Louise Archambault.

== Plot ==
The story revolves around two main characters: Michèle (Moreau, also known as "Charlotte Rocks her Socks"), a free-spirited aerobics instructor with a penchant for gambling, and Janine (Grenon), a suburban housewife and home decorator with a cheating husband.  The lives of these two longtime friends intersect when Michele goes to live with Janine to escape an abusive boyfriend.  Tensions abound as Micheles daughter Marguerite (St. Sauveur) introduces Janines daughter Gabrielle (Gosselin) to a world of boys, drugs, and alcohol.  Meanwhile, Michele cant quite kick her gambling addiction - no matter how many people she seems to hurt and deceive.  Things come to a head when Janine confronts her adulterous husband and Marguerite discovers shes pregnant.

== Cast ==
* Sylvie Moreau as Michèle
* Mylène St. Sauveur as Marguerite
* Macha Grenon as Janine

==Awards==
Archambault won the 2005 Claude Jutra Award for the best feature film by a first-time film director in Canada.

== External links ==
*  

 
 
 
 
 
 
 


 
 