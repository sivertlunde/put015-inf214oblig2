Those Were the Days (1934 film)
{{Infobox film
| name           = Those Were the Days
| image          = Those Were the Days (1934 film) DVD boxart.jpg
| image_size     = 150px
| caption        = 
| director       = Thomas Bentley
| producer       = Walter C. Mycroft
| writer         = Jack Jordan Frank Launder Frederick A. Thompson
| narrator       =  George Graves John Mills
| music          = 
| cinematography = Otto Kanturek
| editing        = Edward B. Jarvis
| distributor    = 
| released       = April 1934 
| runtime        = 80 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 The Magistrate and was the first of two Hay movies based on Pineros plays, the other being Dandy Dick. The movie also featured music hall acts of the time  - acts of a type rarely committed to film.

==Synopsis==
Strait-laced Magistrate Brutus Poskett (Will Hay) is concerned that his wife (Iris Hoey) may be older than he believes her to be, especially as his young stepson (John Mills) seems very precocious for an apparently fifteen-year-old boy.

Mrs Poskett tries to stop an upcoming visit from her first husbands friend (Claud Allister), who knows her true age, by confronting him at a local music hall.  However, unbeknownst to her, Poskett has also been convinced to go to the music hall with his "adolescent" stepson and, in an ensuing melee, Posketts wife and her sister are arrested.

The following day, Poskett sentences both to seven days imprisonment, failing to recognise them as they are heavily veiled.

==Cast==
* Will Hay as Magistrate Brutus Poskett
* Iris Hoey as Agatha Poskett
* Angela Baddeley as Charlotte
* Claud Allister as Capt. Horace Vale George Graves as Col. Alexander Lukyn
* John Mills as Bobby Jane Carr as Minnie Taylor
* Marguerite Allan as Eve Douglas
* H. F. Maltby as Mr. Bullamy
* Laurence Hanray as Wormington
* Syd Crossley as Wyke
* Wally Patch as Insp. Briggs
* Jimmy Godden as Pat Maloney

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 