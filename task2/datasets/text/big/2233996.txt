Apocalypse Now Redux
 
{{Infobox film
| name = Apocalypse Now Redux
| image = Apocalypse Now Redux.jpg
| image_size = 215px
| alt = 
| caption = UK DVD cover
| director = Francis Ford Coppola
| producer = Francis Ford Coppola Kim Aubry  
| writer   = John Milius Francis Ford Coppola Michael Herr  
| based on = Heart of Darkness by Joseph Conrad  
| narrator = Martin Sheen Albert Hall Sam Bottoms Laurence Fishburne Christian Marquand Aurore Clément Harrison Ford Dennis Hopper
| music = Carmine Coppola Francis Ford Coppola
| cinematography = Vittorio Storaro
| editing        = Richard Marks Walter Murch Gerald B. Greenberg Lisa Fruchtman Zoetrope Studios United Artists
| distributor = Miramax Films  
| released =  
| runtime = 196 minutes  
| country = United States Central Khmer
| gross = $4,626,290 (US)  $7,916,979 (non US)   Retrieved 2012-11-06  $12,543,269 (total)
}} extended version of Francis Ford Coppolas epic war film Apocalypse Now, which was originally released in 1979. Coppola, along with editor/long-time collaborator Walter Murch, added 49 minutes of scenes that had been cut out of the original film. It represents a significant re-edit of the original version.

==Production==
Francis Ford Coppola began production on the new cut with working-partner Kim Aubry. Coppola then tried to get Murch, who was reluctant at first. He thought it would be extremely difficult recutting a film which had taken two years to edit originally. He later changed his mind (after working on the reconstruction of Orson Welless Touch of Evil). Coppola and Murch then examined several of the rough prints and dailies for the film. It was decided early on that the editing of the film would be like editing a new film altogether. One such example was the new French Plantation sequence. The scenes were greatly edited to fit into the movie originally, only to be cut out in the end. When working again on the film, instead of using the (heavily edited) version, Murch decided to work the scene all over again, editing it as if for the first time.
 ADR or Albert Hall, Frederic Forrest, and Aurore Clément were brought back to record ADR for the new scenes.

===Music===
New music was composed and recorded for the remade film. For example, it was thought no music had been composed for Willard and Roxannes romantic interlude in the French Plantation scene. To make matters worse, composer Carmine Coppola had died in 1991. However, the old recording and musical scores were checked and a track titled "Love Theme" was found. During scoring, Francis Coppola had told his father (Carmine) to write a theme for the scene before it was ultimately deleted. For the remake, the track was recorded by a group of synthesists. 

===Cinematography===
Vittorio Storaro also returned from Italy to head the development of a new color balance of the film and new scenes. When Redux was being released, Storaro learned that a Technicolor dye-transfer process was being brought back. The dye-transfer is a three-strip process that makes the color highly saturated and has consistent black tone. Storaro wished to use this on Redux, but in order to do it, he needed to cut the original negative of Apocalypse Now, leaving Apocalypse Now Redux the only version available. Storaro decided to do it, when convinced by Coppola that this version would be the one that would be remembered.

==New scenes and alterations==
The film contains several newly added sequences and alterations to the original film:
* In the original film, the PBR Street Gang crew members relax and play around, listening to The Rolling Stoness "(I Cant Get No) Satisfaction" while Willard first looks at the dossier. The scene plays right before the crew members meet Kilgore. In the Redux version, the scene is moved to later in the film, and Willard is shown reading the dossier without the surrounding activity and music.
* In the original version, Willard first meets Kilgore when asking a fellow officer who simply replies, "Hes over there, you cant miss him". In the Redux, the officer now says "Theres the Colonel coming down". We later learn that Kilgore is arriving (via helicopter) to the scene. When he arrives, he tells an officer riding with him, "Lieutenant, bomb back that tree line bout a hundred yards, give me some room to breathe". He later asks another for his "Death Cards" (which he uses in the original version).
* During the raid, Kilgore looks over some of the wounded and dead. He then walks away, simply replying "Damn".
* After Kilgore has ordered an air strike, a Vietnamese mother, with her wounded child in hand, runs to Kilgore. Kilgore immediately takes the child and tells his men to rush the child to a hospital (mother as well) on his chopper.
* After the helicopter carrying the wounded child leaves, Kilgore hands Lance a new pair of shorts to go surfing in (Note: Throughout the original cut, Lance is wearing them, but it is never explained how he got them).
* After giving the famous "Napalm" speech, Kilgore soon learns that the napalm has changed the wind current, ruining the perfect waves. Willard immediately uses this as an excuse to leave. Before he and Lance run back to the boat, Willard steals Kilgores surf board.
* Before Willard and Chef go to search for mangoes, theres a scene where the crew is lying around in a river. Chef asks Chief if he can go get some mangoes and Willard goes with him. The Redux version contains a new scene before this, in which it is clear that the crew are hiding from Kilgore, who is trying to get back his surf board. A helicopter soon flies by, carrying a recording by Kilgore, asking Lance for the board back. Chief then changes the subject by asking how far they are going up the river. Willard says its classified. Chief later asks Willard if he likes it like that, "hot and hairy" (to which Willard replies: "Fuck. You dont get a chance to know what the fuck you are in some factory in Ohio"). Chef later asks Chief if he can get some mangoes. USO show, ARVN Lieutenant who ruined his foldouts.
* The "Satisfaction" scene comes immediately after the above scene. In the following scene, Willard reads a letter by Kurtz, criticizing the incompetent young soldiers sent to Vietnam, blaming them for their losing.
* At one point during their travels, the crew stop at a destroyed Medical evacuation|Medevac. The area is completely wrecked, with no real commanding officer (much like the Do Lung Bridge sequence). Willard tries to find someone in charge, but later learns that the Playboy bunnies helicopter has landed there. Willard is called over by the Bunnies manager, who negotiates two barrels of fuel for 2 hours with the bunnies (along with the rest of the crew). Chef spends his time with his idol, Miss December (now Miss May), in the Playboy helicopter, and Lance spends his time with the Playmate of the Year in a Medevac tent.  During their escapades, a large cooler is upended, revealing the corpse of a soldier who had died at the Medevac camp. Lance is so deeply engrossed in their encounter that he barely notices the dead man a few feet away.  During these scenes, Clean constantly interrupts, trying to get his turn.
* After the above scene, Chef learns that Clean is still a Virginity|virgin. Chef makes fun of him for it, only to be stopped by Chief. The exchange is only partially heard in the original cut.
* In the Do Lung Bridge sequence, after being asked where hes going, Willard now mentions wanting to get some fuel for the ship. In the original, it is only said when Willard returns to the PBR vessel. French rubber plantation near the Cambodian border. Willard tells the head of the plantation (Christian Marquand) that they lost one of their men. He tells Willard that they will bury him (to pay respects to the fallen of their allies). What later follows is a solemn funeral for Clean. Following the recital of a poem by one of the French children (played by Roman Coppola and watched by older brother Gian-Carlo Coppola|Gian-Carlo), the crew then has dinner with the new arrivals. Willard, sitting with the family, asks when they are going back to France. The family soon get into a long argument over the First Indochina War and the Vietnam War. There is a dispute over "traitors at home" (e.g., the famous Henri Martin Affair) and most of the family leaves in anger. After they all leave, one, Roxanne (the only one not in the conversation, played by Aurore Clément), apologizes for her familys behavior. She and Willard talk, smoke opium, and she later explains the conflicts her deceased husband had faced with himself during the Indochina War. After she undresses and approaches Willard, she tells him, "There are two of you, cant you see? One that kills, and one that loves." We later see the crew back on the river continuing the mission. Time Magazine articles detailing Americas success in the war.

==Cast==
;This list only includes the cast members not present in the films original cut.
* Christian Marquand as Hubert de Marais
* Aurore Clément as Roxanne Sarrault
* Roman Coppola as Francis de Marais
* Gian-Carlo Coppola as Gilles de Marais
* Michel Pitton as Philippe de Marais
* Franck Villard as Gaston de Marais
* David Olivier as Christian de Marais
* Chrystel Le Pelletier as Claudine
* Robert Julian as The Tutor
* Yvon Le Saux as Sgt. Le Fevre
* Henri Sadardiel as French soldier #1
* Gilbert Renkens as French soldier #2
* Pierre Segui (uncredited) as French soldier #3

==Release==
Apocalypse Now Redux originally premiered at the 2001 Cannes Film Festival in May.    The screening marked the anniversary of the famous Apocalypse Now screening as a work in progress, where it ended up winning the Palme dOr. Coppola went to the festival, also with Murch, Storaro, production designer Dean Tavoularis, producer Kim Aubry and actors Sam Bottoms and Aurore Clément.

===Critical reception===
When it was released, the response from the critics was largely positive, holding a 93% rating on Rotten Tomatoes; the consensus states "The additional footage slows down the movie somewhat (some say the new cut is inferior to the original), but Apocalypse Now Redux is still a great piece of cinema."  Some critics thought highly of the additions, such as A. O. Scott of The New York Times, who wrote that it "grows richer and stranger with each viewing, and the restoration of scenes left in the cutting room two decades ago has only added to its sublimity." 

Some critics, however, thought the new scenes slowed the pacing, were too lengthy (notably the French plantation sequence), and added nothing overall to the films impact. Owen Gleiberman wrote "Apocalypse Now Redux is the meandering, indulgent art project that   was still enough of a craftsman, in 1979, to avoid."  Despite this, other critics still gave it high ratings. Roger Ebert wrote: "Longer or shorter, redux or not, Apocalypse Now is one of the central events of my life as a filmgoer." 

===Box office===
The film was given a limited release in the US on August 3, 2001, and was also released theatrically around the world in some 30 countries, generating a worldwide total of $12,543,269 ($4,626,290 in the US    plus $7,916,979 outside the US ) in box office revenue.

==Soundtrack==
{{Infobox album  
| Name    = Apocalypse Now
| Type    = Soundtrack
| Artist  = Carmine Coppola and Francis Ford Coppola
| Cover   = Apocalypse Now Redux Soundtrack.jpg
}} Carmine and Francis Ford Coppola (with some tracks co-composed by Mickey Hart and Richard Hansen). The first track is an abridged version of The Doorss 11 minute long, epic "The End".
 The End" – The Doors
# "The Delta" – Carmine Coppola, Francis Ford Coppola
# "Dossier" – Carmine Coppola, Francis Ford Coppola
# "Orange Light" – Carmine Coppola, Francis Ford Coppola
# "Ride of the Valkyries" – Richard Wagner Suzie Q" (Dale Hawkins) – Flash Cadillac
# "Nung River" – Carmine Coppola, Francis Ford Coppola, Mickey Hart
# "Do Lung" – Carmine Coppola, Francis Ford Coppola, Richard Hansen
# "Cleans Death" – Carmine Coppola, Francis Ford Coppola, Mickey Hart
# "Cleans Funeral" – Carmine Coppola, Francis Ford Coppola
# "Love Theme" – Carmine Coppola, Francis Ford Coppola
# "Chiefs Death" – Carmine Coppola, Francis Ford Coppola
# "Voyage" – Carmine Coppola, Francis Ford Coppola
# "Chefs Head" – Carmine Coppola, Francis Ford Coppola
# "Kurtz Chorale" – Carmine Coppola, Francis Ford Coppola
# "Finale" – Carmine Coppola, Francis Ford Coppola
# "The Horror... The Horror" – Finale quote of Marlon Brandos character

==References==
 

==External links==
*  
*  
*  
*  
*  
*   – An extensive look at the making of the new cut.
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 