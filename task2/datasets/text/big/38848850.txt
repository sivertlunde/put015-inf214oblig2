A Promise (2013 film)
{{Infobox film
| name           = A Promise
| image          = A Promise (2013 film).jpg
| alt            = 
| caption        = 
| director       = Patrice Leconte
| producer       = 
| writer         = Patrice Leconte   Jérôme Tonnerre
| starring       = Rebecca Hall   Alan Rickman   Richard Madden   Maggie Steed   Shannon Tarbet   Jean-Louis Sbille
| music          = Gabriel Yared
| cinematography = Eduardo Serra
| editing        = Joelle Hache
| studio         = 
| distributor    = 
| released       =  
| runtime        = 95 minutes 
| country        = France
| language       = English
| budget         = 
| gross          = 
}}
 drama romance Stefan Zweigs novel "Journey into the Past" and stars Rebecca Hall, Alan Rickman, Richard Madden and Maggie Steed. It was screened in the Special Presentation section at the 2013 Toronto International Film Festival.      

==Plot==
Germany 1912, a freshly graduated engineer with modest origins, Friedrich Zeitz, becomes the right hand of ageing tycoon Karl Hoffmeister. When Hoffmeisters degrading health condition starts to confine him permanently to his house, Friedrich has to visit him at home to get briefed. Thus Friedrich makes the acquaintance of Hoffmeisters younger wife Charlotte, a beautiful and reserved woman in her early 30s. He immediately becomes enamored with her and struggles with his growing unrequited feelings for her, not realizing they are reciprocated. Just as they disclose their mutual attraction towards one another, Friedrich has to leave the country to represent Hoffmeister overseas. The outbreak of World War I keeps him away from Germany for a long time. Only after the end of the war and many years of separation are Friedrich and Charlotte able to reunite.

==Cast==
* Rebecca Hall as Charlotte Hoffmeister
* Alan Rickman as Karl Hoffmeister
* Richard Madden as Friedrich Zeitz
* Maggie Steed as Frau Hermann
* Shannon Tarbet as Anna
* Jean-Louis Sbille as Hans
* Toby Murray as Otto Hoffmeister
* Christelle Cornil
* Jonathan Sawdon as engineer

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 
 