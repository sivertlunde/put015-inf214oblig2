Samurai Rebellion
{{Infobox film
  | name = Samurai Rebellion
  | image = Samurai Rebellion 1967.jpg
  | caption = Theatrical poster for Samurai Rebellion (1967)
  | director =  Masaki Kobayashi
  | producer = Toshiro Mifune   Tomoyuki Tanaka
  | writer = Shinobu Hashimoto   Yasuhiko Takiguchi (novel)
  | starring = Toshiro Mifune   Yoko Tsukasa   Tatsuyoshi Ehara   Etsuko Ichihara   Isao Yamagata   Tatsuya Nakadai   Shigeru Kôyama   Michiko Otsuka
  | music = Tōru Takemitsu
  | cinematography = Kazuo Yamada
  | editing = Hisashi Sagara Toho Company Toho International Company Inc. (1967, USA)
  | released = June 3, 1967 (Japan)   December 1967 (USA)
  | runtime = 128 min. (Japan)   120 min (USA) Japanese
  | budget = 
  }} directed by Masaki Kobayashi. Its original Japanese title is Jōi-uchi: Hairyō tsuma shimatsu (上意討ち 拝領妻始末), which translates approximately as "Rebellion: Result of the Wife Bestowed" or "Rebellion: Receive the Wife". 

==Plot==
In the Edo period of Japan, Isaburo Sasahara (Toshiro Mifune) is a vassal of the daimyo of the Aisu clan.  Sasahara is the most skilled swordsman in the land, whose only rival in ability is his good friend Tatewaki Asano (Tatsuya Nakadai).  Isaburo is in a loveless marriage with a shrew of a woman.  One day one of the daimyos advisors orders Isaburos elder son Yogoro (Go Kato) to marry the daimyos ex-concubine, Ichi (Yoko Tsukasa), even though she is the mother to one of the daimyos sons. With much trepidation, the family agrees.  In time, Ichi and Yogoro find love and happiness in the marriage and a daughter Tomi is born.

In the meantime, the daimyos primary heir dies, and he orders his ex-concubine to rejoin his household to care for their son and heir.  The family refuses, but Ichi is tricked into the castle by Isaburos younger son, and her husband and father-in-law are ordered to commit seppuku for their insolence and insubordination.  Isaburo counters that he will comply only if the heads of the daimyo and his two primary advisors are brought to him first.

Isaburo sends his younger son and wife away and dismisses his household servants.  With his elder son he prepares for battle, removing the tatami from his house to prevent slipping in the blood that will be spilled and removing the houses walls to allow for more space for combat.

The daimyos steward, accompanied by a platoon of 20 samurai, brings Ichi to the Sasahara house and tries to force her at spear point to renounce her marriage to Yogoro and join the daimyos household.  The daimyo also "graciously" offers to commute Isaburo and Yogoros sentences to permanent confinement in a shrine outside his castle.  Not only does Ichi refuse to join his household, she throws herself onto a spear instead of abandoning her husband.  Her husband goes to her side and is killed with her in his arms.  His father, enraged, kills the stewards entire party, killing the steward last as he attempts to flee.
 assassins hidden nearby cut Isaburo down with musket fire. In his dying breath, he laments that no one will ever know the love story of Yogoro and Ichi, which had inspired him, an otherwise obedient vassal, to rise against his clan and lord. He beseeches Tomi to be a good and kind woman like her mother, and to seek out a fine and kind husband like her father. As Isaburo dies, we see Tomis wet-nurse comforting the baby: she has been secretly following him.

== Cast ==
*Toshiro Mifune as Isaburo Sasahara 
*Yoko Tsukasa as Ichi Sasahara 
*Go Kato as Yogoro Sasahara 
*Tatsuya Nakadai as Tatewaki Asano 
*Shigeru Koyama as Geki Takahashi 
*Masao Mishima as Sanzaemon Yanase 
*Isao Yamagata as Shobei Tsuchiya 
*Tatsuyoshi Ehara as Bunzo Sasahara
*Etsuko Ichihara as Kiku
*Tatsuo Matsumura as Masakata Matsudaira 
*Takamaru Sasaki as Kenmotsu Sasahara
*Jun Hamamura as Hyoemon Shiomi

==Music==
The music, by Tōru Takemitsu, is performed almost exclusively on traditional Japanese instruments, including shakuhachi, biwa, and taiko.

==Awards==
Rebellion won the Fipresci Prize (Masaki Kobayashi) at the Venice Film Festival in 1967. At the 1968 Kinema Junpo Award it won Best Director (Masaki Kobayashi), Best Film (Masaki Kobayashi), Best Screenplay (Shinobu Hashimoto). At the 1968 Mainichi Film Concours it won Best Film (Masaki Kobayashi).

==References==
 

==External links==
*  
*  
*  
* 
* 
*  

 
{{Navboxes title = Awards list =
 
 
}}

 
 
 
 
 
 
 
 
 
 
 