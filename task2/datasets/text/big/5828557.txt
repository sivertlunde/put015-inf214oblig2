Deepwater (film)
{{Infobox film
| name           = Deepwater
| image          = Deepwater film cover.jpg
| caption        = 
| director       = David S. Marfield
| producer       = Peter Wetherell Chris Coen
| based on       =  
| writer         = David S. Marfield
| starring       = Lucas Black Peter Coyote Mía Maestro
| music          = Charlie Clouser
| cinematography = Scott Kevan
| editing        = Eric Strand
| distributor    = Monarch Home Video Lightning Entertainment
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
}} Clearwater and Little Fort in B.C., Canada. 

==Synopsis==
Nat Banyon (Lucas Black) is a hitch-hiker whose dream is to open an ostrich farm in Wyoming. Early in the trip, he gets in a fight at a truck-stop bar and in the ruckus, takes his opponents car keys, stealing his sports car. Continuing the journey, he finds a stranger (Peter Coyote) trapped in an overturned vehicle. He rescues him just as the car is completely destroyed by an incoming truck. Grateful for saving his life, the stranger, Herman Finch, hires Nat as the handyman for his motel. Nat soon learns that Finch runs a criminal network, acting in collusion with the police. Most of the locals are also in league with Finch. And everyone who irritates or opposes him soon turns up missing. They are then found dead.

Finchs young, attractive wife Iris (Mía Maestro) soon hatches a plan to run off with Nat, planning to steal Finchs ill-gotten gains. Soon, Finch begins manipulating Nat when the latter begins questioning him about all the mysterious activities that had been taking place. Finch challenges Nat to a boxing match. If Nat won, he would be free to leave and take his car as well as the salary that he had made as Finchs handyman. 

Nat shows up to the fight after training himself to tip-top shape. When he steps into the ring, Finch suddenly reveals that the fight was a joke. Finch actually never intended Nat to leave; Nat had been too good of a worker and had learned about too many of his dark secrets. Enraged, Nat attacks Finch, and then rushes to the motel. There, he grabs Iris and tells her that they had to leave. Despite her resistance, Nat is too deeply in love with Iris to escape alone. He kidnaps her and speeds away. Unsure of where to go in the night, Nat follows Iris directions. However, Iris deceives Nat to drive to the barn where he had seen Finch for the fight. In a final fit of frustration, Nat drives off into the water. He swims out for a final showdown with Finch. Nat loses the fight, and the police arrive to arrest him. 

It is only later discovered that Nat was a schizophrenic who had multiple personalities. The film had generally focused on his nobler personality. Not only did he fix Finchs motel, Nat also helped Finch murder his enemies. His personalities were likely entirely unaware of each other. The film also slowly alludes to the fact that Iris love for Nat had been Nats own hallucination.  

Nat is sent to a mental hospital where he falls deeper into mental illness, whimpering about the baby ostriches that he would now never have the chance to raise.

==Cast==
*Lucas Black...Nat Banyon
*Peter Coyote...Herman Finch
*Mía Maestro...Iris
*Lesley Ann Warren...Pam
*Xander Berkeley...Gus
*Jason Cerbone...Sal
*Michael Ironside...Walnut
*Kristen Bell...Nurse Laurie
*Ben Cardinal...Petersen
*Dee Snider...Bartender
*John Boncore...Joe Littlefeet
*Valerie Murphy...Stripper

==DVD== region 1 DVD was released July 25, 2006.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 