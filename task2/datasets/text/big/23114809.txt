Main Street (2010 film)
{{Infobox film
| name           = Main Street
| image          = Main Street Poster.jpg
| caption        = Theatrical release poster John Doyle
| producer       = Adi Shankar Megan Ellison Jonah Hirsch Spencer Silna
| writer         = Horton Foote   
| starring       = Orlando Bloom Colin Firth Andrew McCarthy Ellen Burstyn Amber Tamblyn Patricia Clarkson
| music          = Patrick Doyle
| cinematography = Donald McAlpine
| studio         = 1984 Films
| distributor    = Magnolia Pictures Myriad Pictures (International) 
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          = $2,560  
}}
Main Street is a 2010 drama film about several residents of Durham, North Carolina, a city in the Southern U.S., whose lives are changed by the arrival of a stranger with a controversial plan to save their decaying hometown.

==Plot==
Each of the colorful citizens of a close-knit North Carolina community—from a once-wealthy tobacco heiress to the citys mayor to a local police officer—will search for ways to reinvent themselves, their relationships and the very heart of their neighborhood.  

==Cast==
*Colin Firth as Gus Leroy
*Ellen Burstyn as Georgiana Carr
*Patricia Clarkson as Willa Jenkins
*Orlando Bloom as Harris Parker
*Amber Tamblyn as Mary Saunders
*Margo Martindale as Myrtle Parker
*Andrew McCarthy as Howard Mercer
*Victoria Clark as Miriam
*Isiah Whitlock Jr. as Mayor
*Tom Wopat as Frank
*Viktor Hernandez as Estaquio
*Juan Piedrahita as Jose (as Juan Carlos Piedrahita)
*Thomas Upchurch as Trooper Williams
*Reid Dalton as Crosby Gage
*Amy da Luz as Rita

==Production==
  near Durham, NC]]
The film was shot nearly entirely in Durham, North Carolina in April and May 2009.     The screenplay was written by Pulitzer Prize–winning writer Horton Foote after he found downtown Durham empty on a weekend visit several years earlier.   
 Myriad Pictures bought the international distribution rights in May 2009.  The film was promoted at the 2009 Cannes Film Festival by its producers and stars.    
 

==Reception==
Reception for the movie has been predominantly negative, with the film currently holding a 13% rating on the tomatometer. Blog Critics reviewed the film, saying "Everything that occurs in the film feels shallow somehow, and it’s a shame because Main Street had all of the basic elements that would have made it truly, a great film."  

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 