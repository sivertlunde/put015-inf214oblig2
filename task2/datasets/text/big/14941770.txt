Unholy Women
 
{{Infobox Film
| name           = Unholy Women
| director       = Rattle Rattle:   The Inheritance: Keisuke Toyoshima
| producer       = Machiko Komatsu Miyuki Sato Yukihiko Yamaguchi Takashi Shimizu
| writer         = Rattle Rattle:     Takashi Shimizu Keisuke Toyoshima
| music          = Gary Ashiya
| cinematography = 
| editing        = 
| distributor    = Art Port
| released       = November 25, 2006
| runtime        = 107 min.
| budget         = 
| gross          = 
| preceded       = 
| followed       = 
| jmdb_url       = 
| trailer_url    = http://www.nipponcinema.com/trailers/unholy_women/
}}

  is a 2006 Japanese film. It is a compilation of three separate short horror films written and directed by Keita Amemiya, Takuji Suzuki, and Keisuke Toyoshima respectively.

==Segments==

=== ===
Kanako returns home after an evening with her fiance, Akira, a divorcee whom she plans to marry. Their car accidentally hits a vase in a pavement, though they do not notice. Nearing Kanakos apartment, Akira repeatedly gets phone calls from his ex-wife, which he shuts off. While Kanako walks home by foot, an earring suddenly falls on her, which is quickly followed by something heavy that knocks her out. Waking up, Kanako sees a little girl who quickly disappears afterwards. In her apartment, Kanako is phoning her friend, Miyuki, while she treats her head wound when suddenly a strange voice startles her. Akira phones Kanako, telling her he met his ex-wife who held a knife, but she quickly ran away when he approached her. As Kanako investigates steam coming from her bathroom, she confronts a woman in red dress holding a knife, who then approaches her. Kanako knocks the woman with a vase, but the woman gets up and chases Kanako throughout the complex. Kanako escapes to the street and sees several statue-like people circling a dead body when she meets the little girl again, who points to the broken vase in the pavement. Kanako remembers an insane woman who was always looking at the memorial vase of her dead daughter (the little girl), who fell from the apartment. As the woman in red continues the chase, Kanako climbs up the apartment and meets several statue-like people again. She arrives at the home of the insane woman, which seems to be the lair of the woman in red. The woman catches up and starts to freakishly contort, and as Kanako tries to escape, she falls out of a window.
 a monster before throwing Kanako to her death.

=== ===
Mikio Sekiguchi is a teenager working at a workshop owned by Tetsu Takahashi. He is offered by Tetsu a date with his sister, Hagane. When he arrives at the Takahashi residence, Mikio sees Hagane operating a sewing machine with a burlap sack covering her upper body. She goes to change her clothes, but when she comes out, she wears a skirt and heels with the sack still covering her body. Throughout the date, Hagane does not speak, only letting out strange noises and growls. She also acts strangely, running around the street wildly. At one point, Mikio notices a grasshopper coming out of Haganes sack. After Hagane throws herself to the river and dragging Mikio when he tries to catch her, Mikio returns home with Hagane to change his clothes. When he attempts to lend Hagane his clothes, she kicks him to the window until Mikio pushes her to his bed. Hagane draws the word "more" using Mikios blood and moves her feet around Mikio, who then asks her permission to open the sack so he could have sex with her, which she seems to agree with. However, halfway through opening, Mikio sees what appears to be open flesh under the sack, and Hagane trashes Mikios bedroom before going away.

Tetsu leaves a voice mail informing Mikio that Hagane is pleased with the date and hopes that he will someday date with her again, but finds that Mikio already resigned from his job. As Mikio walks through the street, he is targeted by wooden nails shot by Hagane, but he manages to retaliate and beats her up, although he backs down when his arm is stabbed by spikes protruding from Haganes sack. Escaping from the scene, Mikio avoids a following Tetsu and takes a walk outside his house that night. He sees Hagane walking limply until she collapses; he uses this opportunity to throw her off a cliff. However, she appears unharmed and chases him to a warehouse, which Mikio locks up. Using her newly opened hands, Hagane opens the door and cries as Mikio hugs her. She then opens up her sack and allows Mikio to see her inside until a chomping sound is heard. Mikio manages to break free, terrified with his face covered in bite marks as Hagane approaches him. She proceeds to slowly eat him up in a way reminiscent of an intercouse. Tetsu eventually discovers her and burns Mikios belongings. The next day, Hagane continues her sewing activity and walks through a forest happily.

=== ===
Saeko Hishikawa returns to her childhood home with her son, Michio, to take care of her elderly mother. However, Saekos mother still resents Saeko for divorcing her husband and breaking up the family, and she tells her that she should return to Tokyo. At night, Michio glimpses a young boy whom he eventually learns to be his uncle, Masahiko, who had disappeared when the latter was still a child. When he tells his mother about it, Saeko dismisses it. The next day, Saeko goes inside the shed behind her house and finds a wooden chest with a scroll inside, which she proceeds to look into. She begins to act strangely, now smoking and acting offensively towards Michio, at one point attempting to strangle him. During one night, Michio sneaks to the shed to find Saeko combing her hair while singing a lullaby, but he escapes when she notices him. Meanwhile, Saekos mother attempts to call someone to take Michio away, but Saeko closes the call and says that Michio will not go anywhere. 

As the situation worsens, Saeko eventually locks Michio inside the shed while she burns planks of wood, proceeding to then throw the scroll from earlier into the fire while screaming in agony. Michio discovers Masahikos skull inside a box and sees visions of his young grandmother being the one who kidnapped and killed Masahiko because of the curse of the scroll, and that Saeko is now cursed with the scroll and will kill him. Michio is freed by his grandmother who urges him to leave, but he decides to look for his mother in the house. He is pursued by the possessed Saeko, until she apparently apologizes and the two embrace. The next day, Saekos childhood friend, Kajibo arrives to find Saekos mother babbling, while Saeko is in the shed, singing a lullaby while holding Michios dead body in the same way her mother once did to Masahiko.

==Cast==

* Rattle Rattle
** Noriko Nakagoshi ... Kanako
** Yuko Kobayashi ... Woman in red
** Kosuke Toyohara ... Akira Tasaki, Kanakos fiance
* Hagane
** Tasuku Emoto ... Sekiguchi
** Nahana ... Hagane
** Teruyuki Kagawa ... Takahashi, Sekiguchis boss
* The Inheritance
** Maki Meguro ... Saeko
** Kenta Suga ... Michio
** Shunsuke Matsuoka ... Kajibo
** Tokie Hidari ... Saekos mother

==External links==
*  
*  
*   at Cinematical
*   at MovieWalker (includes "Hagane-chan" shorts)

 
 
 