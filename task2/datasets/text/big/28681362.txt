Uninvited (1999 film)
 
{{Infobox film
| name           = Uninvited
| image          =Uninvited1999.jpg
| caption        = 
| director       = Carlo Gabriel Nero
| producer       = 
| writer         = James Gabriel Berman (novel) Carlo Gabriel Nero
| starring       = Vanessa Redgrave Franco Nero Eli Wallach Adam Hann-Byrd
| music          = Carlo Siliotto
| cinematography = Giancarlo Ferrando
| editing        = Paolo Benassi
| distributor    = Vine International Pictures
| released       = 19 May 2000 (Italy)
| runtime        = 90 min Italy United States
| language       = English
| budget         = 
| gross          = 
}} 1999 Cinema Italian thriller The Fever.

==Plot==
Tony has lusted after Patricia his whole life, ever since high school his yearning never ceased. But he was only ever to appreciate her from a distance. But Tony is a victim of his own heart when he is named chief suspect to the murder of Patricia, her husand and her children are murdered. As Tony is incarcerated his lawyer, Barolo struggles to make a case of defence. Meanwhile, Tony struggles to come to terms with Patricias death. 

==Cast==
*Vanessa Redgrave as Mrs. Ruttenburn
*Franco Nero as Avvocato Ralph Barolo
*Eli Wallach as Strasser
*Adam Hann-Byrd as Young Tony Grasso
*Kevin Isolad as Tony Grasso
*Bethel Leslie as Mrs. Wentworth
*Stephen Mendillo as Vincent Grasso
*Patricia Dunnock as Rose Grasso
*Olivia Birkelund as Patricia Macchiato Carver
*Barton Tinapp as Kirk Carver
*Jessica Munch as Young Patricia
*Jennifer Wiltsie as Charlotte Celeste Hinney
*Nick Sandow as Ed
*Tommy J. Michaels as Koosh

==References==
 

==External links==
*  

 
 
 
 
 
 

 