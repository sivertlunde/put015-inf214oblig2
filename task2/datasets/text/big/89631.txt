The Goonies
 
{{Infobox film
| name           = The Goonies
| image          = The Goonies.jpg
| caption        = Theatrical release poster by Drew Struzan
| director       = Richard Donner
| producer       = {{Plainlist|
* Richard Donner
* Harvey Bernhard
}}
| story          = Steven Spielberg Chris Columbus
| starring       = {{Plainlist |
*Sean Astin
*Josh Brolin Jeff Cohen
*Corey Feldman
*Kerri Green
*Martha Plimpton Ke Huy Quan
 
}}
| music          = Dave Grusin Michael Kahn
| cinematography = Nick McLean
| studio         = Amblin Entertainment
| distributor    = Warner Bros.
| released       =  
| runtime        = 114 minutes
| country        = United States
| language       = English
| budget         = $19 million
| gross          = $61.4 million
}}
 adventure comedy Chris Columbus from a story by executive producer Steven Spielberg. The films premise features a band of pre-teens who live in the "Goon Docks" neighborhood of Astoria, Oregon attempting to save their homes from demolition, and in doing so, discover an old Spanish map that leads them on an adventure to unearth the long-lost fortune of One-Eyed Willie, a legendary 17th-century Piracy|pirate.

==Plot== pirate named "One-Eyed" Willy rumored to be hidden nearby. Evading Brand for one last adventure together, the kids find themselves at a derelict restaurant near the coast, which coincides with the doubloon and the map. While there, they encounter the Fratellis, a family of criminals hiding out after Ma and Francis Fratelli had broken her middle son Jake out of prison earlier. They manage to deter the kids from exploring. Outside, they run into Brand and two girls: the popular cheerleader Andy, the girlfriend of Troy, whose family owns the expanding country club, and Steph, a nerdy, tough-talking girl and Andys best friend.

Mikey convinces Brand to return to the restaurant to explore after the Fratellis leave, discovering the criminals are running a counterfeiting operation. As the Fratellis return, the group finds a tunnel beneath the restaurant and hide in there, sending Chunk, who is too big to enter, to notify the authorities. They explore the tunnel and find the remains of a previous treasure explorer, and Mikey is sure they are following the path to the treasure. Evading various booby traps, they find themselves under an old wishing well. The kids have a chance to be pulled out of the tunnel by Troy and his friends, but Mikey convinces the group to continue on their journey. Meanwhile, Chunk is captured by the Fratellis and reveals the treasure that the others are seeking. The Fratellis chain Chunk next to Sloth, their deformed younger brother, while they pursue the other group. Chunk manages to become friends with Sloth over their common love of food, and Sloth is able to break their chains.

Mikey and the others discover the Fratellis on their tail, and hasten through the remaining traps. They ultimately find an enclosed grotto where "One-Eyed" Willys pirate ship The Inferno rests. They explore the ship, finding a hoard of treasure in front of the skeletal remains of Willy and his crew. Mikey gives a sober speech to Willy, naming him as the first "Goonie", then they fill their pockets with the riches; Mikey insists that the coins on a scale in front of Willy remain untouched as Willys tribute. The Fratellis are waiting for them as they leave. The criminals make them drop the treasure before threatening to kill them, when suddenly Sloth and Chunk arrive. Sloth, angered by how the other Fratellis have treated him in the past, easily subdues them and helps the rest of the Goonies to escape the boat. Though Mikey insists they go back for the treasure, Brand worries more for their lives, and the group escapes through a hole in the grotto, eventually arriving on a nearby beach shore. Police quickly come to their help and reunite them with their families.

Meanwhile, the Fratellis free themselves and begin to loot the boat. When they take the coins that Mikey had left earlier, they trip another trap that causes the grotto to start to cave in. The Fratellis are forced to abandon the loot and flee to the beach, where police quickly take them into custody. As the Goonies are taken care of by their families, including Chunk offering to bring Sloth into his family, the owners of the country club show up and demand that Mr. Walsh sign away their home. As he is about to do so, their housekeeper finds Mikeys marble bag in his wet clothes filled with gems that the Fratellis had not found, and Mouth stops him from signing the paperwork. Mr. Walsh recognizes the value of the gems and tears up the paperwork, having enough money to save all of their homes. As the Goonies celebrate, they watch as The Inferno, clear of the grotto, drifts off towards the ocean.

==Cast==
 .]]
 , the setting of the film]]

; The Goonies
*Sean Astin as Michael "Mikey" Walsh
*Corey Feldman as Clark "Mouth" Devereaux Ke Huy Quan as Richard "Data" Wang
*Josh Brolin as Brandon "Brand" Walsh Jeff Cohen as Lawrence "Chunk" Cohen
*Kerri Green as Andrea "Andy" Carmichael
*Martha Plimpton as Stephanie "Stef" Steinbrenner
*John Matuszak as Lotney "Sloth" Fratelli

; The Fratellis
*Anne Ramsey as Agatha "Mama" Fratelli
*Joe Pantoliano as Francis Fratelli
*Robert Davi as Jake Fratelli
; Others
*Mary Ellen Trainor as Irene Walsh
*Keith Walker as Irving Walsh
*Lupe Ontiveros as Rosalita
*Steve Antin as Troy Perkins
*Curtis F. Hanson as Mr. Perkins
* Paul Tuerpe as the Sheriff

Notes
*Director Richard Donner makes a cameo appearance as a deputy

==Production== audio dubbing recording.    The shooting script was lengthy, over 120 pages, and several sequences were eventually cut from the final theatrical version. During the films Dramatic structure#Dénouement, resolution, revelation or catastrophe|dénouement, mention is made of an octopus which refers to a scene that was excised from the final cut.

In The Making of The Goonies, director Richard Donner notes the difficulties and pleasures of working with so many child actors. Donner praises them for their energy and excitement, but says that they were a handful when brought together. The documentary frequently shows him coaching the young actors and reveals some techniques he used to get realistic performances. One of these tricks involved One-Eyed Willies ship, which was actually an impressive full-sized mock-up of a pirate ship created under the direction of production designer J. Michael Riva. Donner restricted the child actors from seeing the ship until they filmed the scene wherein it is revealed to their characters. The characters first glimpse of the ship was thus basically also the actors first view of it, bringing about a more realistic performance. However, that particular scene in the movie is actually the second shot, as the cast was so overwhelmed at first sight that the scene had to be re-shot. It was later noted that the entire set was scrapped after shooting because they could not find anyone who wanted it. 

In his book There and Back Again, Sean Astin claims that Richard Donner and Steven Spielberg were "like codirectors" on the film as he compares and contrasts their styles when directing scenes.   
 Ecola State Warner Bros. Studios in Burbank, California, including the cavernous set where the Goonies find One-Eyed Willies ship, which was in Stage 16, one of the largest sound stages in America. 

==Reception==
Film critics were generally favorable toward The Goonies. Review aggregator   it has a rating score of 60, indicating "mixed or average reviews".  Several reviewers noted that the film appeared to be enjoyable for children and teens, but not so much for adults.   

The Goonies grossed US$9 million in its opening weekend in the US, second on the charts behind  .    It grossed over US$61 million that year, placing it among the top ten highest-grossing films of 1985 (in the US). 

Ramsey won a Saturn Award for Best Supporting Actress for her role.  At the 7th Youth in Film Awards (now known as Young Artist Awards), Astins portrayal of Mikey won the award for Best Starring Performance By a Young Actor in a Motion Picture. Cohen, Feldman, and Plimpton were also nominated for awards for their performances in The Goonies. The film itself was nominated for best adventure motion picture. 

==Home video==
 

===VHS and Laserdisc=== CED versions also debuted that year. Warner Home Video released a theatrical widescreen laserdisc on January 29, 1992.

===DVD=== Region 1 DVD on August 21, 2001. The DVD featured several notable extras, including:

# Commentary by actors Ke Quan, Feldman, Astin, Green, Plimpton, Cohen, Brolin, and director Donner. This option frequently switches back and shows the actors and Donner gathered together as they are watching the film while recording the commentary. Midway through the film, Sean Astin had to leave, much to the puzzlement of his costars, and without ever getting to say a personal message to Cyndi Lauper that he intended to. He left a Samwise Gamgee action figure in his place, however. In an interview with IGN.com, Astin explained that the recording session started late, which resulted in him having to leave early to honor a prior commitment to his friend (and fellow Goonies castmate), Joe Pantoliano. 
# A 7-minute behind-the-scenes documentary called "The Making of The Goonies".
# The 12-minute, 2-part "The Goonies R Good Enough" music video by Cyndi Lauper. The video is notable in its casting of some 1980s wrestlers, like André the Giant and Lou Albano|"Captain Lou" Albano. The video was directed by Richard Donner.
# Deleted scenes:  MAD magazine back cover. Eight Arms to Hold You" song can be heard) that Data describes to a reporter at the end of the film.
#* The convenience store scene (which explains why the map, which was intact in the Walshes attic, is singed later in the film). The convenience store scene, however, cuts about 15 seconds short, not showing Mouth retrieving the map from Brands pocket and the boys fleeing the store with Brand chasing.
#* A scene with Sloth and Chunk, where Chunk talks about his friend Joey who only goes out to play at night.
# The theatrical trailer.

===Blu-ray===
Warner Home Video released The Goonies on Blu-ray Disc in October 2008 in Europe and November 2010 in North America. The video is in 1080p high definition VC-1 and accompanied by a Dolby TrueHD soundtrack. Extras mirrored that of the DVD release:

# Commentary (with hidden video treasures) by director Richard Donner
# "The Making of The Goonies" featurette
# Deleted scenes
# Cyndi Laupers two "The Goonies R Good Enough" music videos
# The theatrical trailer
# Soundtrack remastered in Dolby TrueHD 5.1

The special features are presented in 480i/576i standard definition.

==Video games== Apple II US Gold.   This game featured eight screens in which a player had to use two members of the Goonies group to solve puzzles and reach an exit to advance to the next stage. The screens were largely inspired by actual sets and puzzles seen in the film. A reference to the aforementioned "octopus scene" is included, as the seventh level.
 The Goonies) Family Computer The Goonies) Famicom (and its international counterpart, the Nintendo Entertainment System). The Goonies II was released in North America, Europe and Australia, although the original was one of the NES games released as part of the Nintendo VS. System arcade machine in the 1980s. The Goonies II had little to do with the film, but achieved a following for its inventive gameplay. In it the Fratellis have managed to kidnap all the Goonies (except Mikey, whom the player guides) and hide them in hidden cages across a terrain of caverns, mazes and abandoned buildings. As Mikey, the player must rescue them all and ultimately free a mermaid named Annie.

In February 2007,  -based game, developed by Fuel Industries. The players goal is to collect map pieces and doubloons, and then race the Fratellis to One-Eyed Willies treasure.  

==Soundtrack and score album==
 

  featured music by Cyndi Lauper, REO Speedwagon, The Bangles, and others. The cast members (except Kerri Green) appeared alongside professional wrestlers "Rowdy" Roddy Piper and "Captain" Lou Albano in the 12 minute "The Goonies R Good Enough" music video. Steven Spielberg makes a cameo appearance. Lauper also has a cameo in the film, performing the song on TV, although the song was not completed until after filming.

Dave Grusins score was unavailable for 25 years. The main theme, "Fratelli Chase", has been used in numerous trailers, such as Innerspace and Guarding Tess, and was re-recorded by Grusin and the London Symphony Orchestra for the album Cinemagic. The score makes liberal use of the Max Steiner-composed theme from Adventures of Don Juan. 

Soundtrack label Varèse Sarabande released the score in March 2010 in a limited edition of 5000 copies. 

==Proposed sequels and adaptations==
  }}

The possibility of a film sequel has been confirmed and denied many times in recent years by the original cast and crew. Donner said that he had a story he liked and Spielberg behind him, but in 2004 several of the actors from the original revealed that Warner Bros., the films owner, had shown no interest in a sequel.    Sean Astin told MTV in October 2007 that Goonies 2 "is an absolute certainty&nbsp;... The writings on the wall when theyre releasing the DVD in such numbers,"  Donner has expressed doubt that the sequel will ever happen, as many of the actors had not shown interest in returning for a sequel.    Corey Feldman stated in his November 25, 2008 blog post, "NO! There is no Goonies 2! Im sorry but its just not gonna happen&nbsp;.... Course now that Ive said that, theyll do it."  However, on the July 2010 release of "The Making of a Cult Classic: The Unauthorized Story of The Goonies" DVD,  Richard Donner states a sequel to The Goonies is a "definite thing" and will involve as much of the old cast as possible. "It will happen," says Donner. "Weve been trying for a number of years."  On April 5, 2014, Richard Donner revealed a sequel is in the works, and he hopes to bring back the entire cast.   

Rumors of adaptations and sequels in other media took off in 2007, including a comic book miniseries,  an animated television series, and a musical adaptation of the film. Corey Feldman said he was asked to reprise the role of Mouth in a cartoon series that would feature the original Goonies characters as adults and focus on the adventures of a new set of kids.     Apparently this project was briefly in the works for Cartoon Network before being shelved.       Entertainment Weekly reported in March 2007 on a potential musical adaptation of the film. "Steven and I have discussed it, and its something that Im fairly passionate about right now," Donner says.  Variety reported in October 2008 that Donner had met with Broadway entertainment attorney John Breglio, and is "confident things are moving in the right direction."   As of May 2011, the musical was still in the beginning stages, but Donner was hopeful that an "irreverent" script would be completed by October.   

==See also==
* 
* 

==References==
Notes
 

==External links==
 
*  
*  
*  
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 