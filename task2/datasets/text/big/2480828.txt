Valentine (film)
{{Infobox film
| name           = Valentine
| image          = Valentine film.jpg
| caption        = Theatrical release poster
| director       = Jamie Blanks
| producer       = Dylan Sellers
| based on       =  
| writer         = Donna Powers Gretchen J. Berg Aaron Harberts
| starring       = David Boreanaz Denise Richards Marley Shelton Hedy Burress Jessica Cauffiel Katherine Heigl Don Davis
| cinematography = Rick Bota
| editing        = Steve Mirkovich
| studio         = Village Roadshow Pictures NPV Entertainment
| distributor    = Warner Bros. Pictures Roadshow Entertainment   
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $29 million 
| gross          = $36,684,136 
}} novel of the same name by Tom Savage.

==Plot==
At a junior high school dance in 1988, outcast student Jeremy Melton asks four popular girls to dance. Three girls, Shelley, Lily and Paige reject him cruelly; while the fourth girl, Kate, kindly turns down his offer. Their overweight friend Dorothy accepts Jeremys invitation and they proceed to secretly make out underneath the bleachers. When a group of school bullies discover the pair, Dorothy claims that Jeremy sexually assaulted her, causing the boys to publicly strip and severely beat him up, and his nose drips blood. Later in the film, Paige reveals Jeremy was sent to a reform school as punishment for his alleged "assault".

Thirteen years later, Shelley (Katherine Heigl), a medical student, is at the morgue practicing for her medical exams. After receiving a vulgar Valentines card and being pursued by a killer wearing a Cupids mask, Shelleys throat is slit as she hides in a body bag; the killers nose is seen to bleed as he performs the act. Her friends are questioned at her funeral but nothing is concluded. 

All the girls except Kate (Marley Shelton) and Paige (Denise Richards) receive cards in the same fashion as Shelley. Dorothy (Jessica Capshaw) who is now much thinner receives a card, which reads "Roses are red, Violets are blue, Theyll need dental records to identify you". Her boyfriend, Campbell, loses his apartment and stays with her. Lily (Jessica Cauffiel) receives a box of chocolates and a card which says "You are what you eat". She then takes a bite of one of the chocolates, and vomits upon realizing that there are maggots inside the chocolates.
 LA on a work trip. Upon contacting the police, they agree that the culprit could be Jeremy Melton. Meanwhile Kates neighbor breaks into Kates apartment to steal her underwear and is killed by the cupid killer with a hot iron pressed to his face and then bludgeoned with it.

As Valentines Day approaches, Dorothy is planning a theme party at her house. Campbell is killed with an axe to the back the day of the party as he relights the hot furnace, after being revealed as a con-man who is using Dorothy to gain access to her vast inheritance. The others assume he has simply left Dorothy after duping her, angering Dorothy, who believes that they are jealous. After coming to the party to confront Dorothy with the truth about Campbell, Ruthie is thrown through a shower window by the killer who then impales her neck on the glass. At the party, Paige is attacked and trapped in a hot tub by the killer, who proceeds to try and kill her with a drill. After cutting her, he opens the lid of the hot tub and throws the electric drill into the water, electrocuting her.

The party disintegrates when the power cuts out, and Dorothy and Kate argue over who the killer is. Kate claims that Campbell could be a suspect because they do not know anything about him, while Dorothy counters by accusing Adam (David Boreanaz), Kates recovering alcoholic on-off boyfriend. After being told by Lilys boyfriend that she did not arrive in Los Angeles as planned, Kate realizes she is also probably dead, and calls the detective assigned to the case. After dialing the number, she follows the sound of a ring tone outside the house and discovers the detectives severed head in the pond.

Kate then becomes convinced that Adam is actually Jeremy, disguised by reconstructive surgery, and goes back into the house, only to find Adam waiting for her. To her surprise, he asks her to dance, and they dance together for a while until she becomes frightened, kneeing him in the groin and escaping. She runs through the house, discovering Paige  and Ruthies corpses. She locates a gun, but someone in the Cupids mask jumps out and runs into Kate resulting in knocking the gun from her hand and sending them both tumbling down a staircase. The supposed killer arises and is shot by Adam who appears at the top of the stairs using Kates gun, shocking and confusing Kate. As she apologizes profusely, Adam pulls off the Cupids mask to reveal the killer as Dorothy. Adam forgives Kate, explaining that childhood trauma can lead to lifelong anger and some people are eventually forced to act on that anger. As Kate and Adam wait for the police to arrive, they hug as Adam says he has always loved her. Moments later, as Kate closes her eyes as they wait for the police to arrive, his nose begins to bleed, indicating that he is Jeremy Melton after all.

==Cast==
* Marley Shelton as Kate Davies
* Denise Richards as Paige Prescott
* David Boreanaz as Adam Carr/Jeremy Melton
* Jessica Capshaw as Dorothy Wheeler
* Jessica Cauffiel as Lily Voight
* Katherine Heigl as Shelley Fisher
* Hedy Burress as Ruthie Walker
* Fulvio Cecere as Detective Leon Vaughn
* Daniel Cosgrove as Campbell Morris
* Johnny Whitworth as Max Raimi
* Woody Jeffreys as Brian
* Adam J. Harrington as Jason Marquette
* Claude Duhamel as Gary Taylor

==Irony==
When Jeremy Melton/Adam Carr asks each girl for a dance they each state a mean comment right after he asks them. This foreshadows each of the girls fate.
*Shelley
**"In your dreams, loser."
**Dies lying down in a sleeping position.
*Lily
**"Eww!"
**Receives maggots in a chocolate box and her body lands in a dumpster.
*Paige
**"Id rather be boiled alive."
**Gets thrown into a hot tub and is later electrocuted.
*Dorothy
**"He attacked me!" Makes everyone believe she was "attacked" by Jeremy after being caught kissing him behind the bleachers.
**Jeremy/Adam makes everyone believe that all the killings were Dorothys doing.
*Kate
**"Maybe later, Jeremy."
**Jeremy/Adam lets Kate live since she is the only girl that did not insult him and wasnt mean to him. She does eventually dance with Adam and have a relationship with him. Another assumption is Jeremy/Adams future intentions of murdering Kate later on.

==Production==
Boreanaz shot all his scenes in less than two weeks. Katherine Heigl only had three days to shoot her scenes as she was already committed to the television series Roswell (TV series)|Roswell.

Blanks later said in an interview, "Forgive me for  . A lot of people give me grief for that, but we did our best." 

===Original cast=== Richard Kelly was originally offered the chance to direct, but turned the offer down. Hedy Burress auditioned for the role of Dorothy Wheeler, and Anna Yuhas was considered for the role, but it was given to Jessica Capshaw instead. However, Blanks wanted Burress to star in the film, and cast her as Ruthie Walker. Jessica Cauffiel originally auditioned for Denise Richards role of Paige. In the original cast, Jennifer Love Hewitt was to play Paige Prescott.

==Soundtrack== Don Davis. The soundtrack also includes the songs "Pushing Me Away" by Linkin Park, "God of the Mind" by Disturbed (band)|Disturbed, "Love Dump (Mephisto Odysseys Voodoo Mix)" by Static-X, "Superbeast (Porno Holocaust Mix)" by Rob Zombie, "Valentines Day" by Marilyn Manson, and "Opticon" by Orgy (band)|Orgy. This soundtrack compilation was lampooned in a sketch by Saturday Night Live, which humorously pointed out that many of the bands featured on it were not only unknown to a mass audience, but have oddly nonsensical names. 

===Track listing===
* "Superbeast" (Porno Holocaust Mix) — Rob Zombie Disturbed
* "Love Dump" (Mephisto Odysseys Voodoo Mix) — Static-X
* "Pushing Me Away" — Linkin Park
* "Rx Queen" — Deftones Orgy
* "Valentines Day" — Marilyn Manson
* "Filthy Mind" — Amanda Ghost
* "Fall Again" — Professional Murder Music BT
* "Son Song" — Soulfly featuring Sean Lennon Hybrid Mix) Filter
* "Breed" — Snake River Conspiracy Beautiful Creatures

==Reception==
Valentine had its Hollywood premiere at Hollywood Post No. 43, American Legion, on February 1, 2001. It earned $20,384,136 in the United States and Canada and a total gross of $36,684,136, allowing the film to surpass its $29 million budget.    At Rotten Tomatoes, a review aggregator, the film received a poor rating of 9% from critics, with the general consensus being that "Valentine is basically a formulaic throwback to conventional pre-Scream (film series)|Scream slasher flicks. Critics say it doesnt offer enough suspense or scares to justify its addition to the genre." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 