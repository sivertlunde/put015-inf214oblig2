Wedding Bell Blues (film)
{{Infobox film
| name           = Wedding Bell Blues
| image          = Wedding Bell Blues (film).jpg
| caption        = 
| director       = Dana Lustig
| producer       = Ram Bergman Mike Curb Dana Lustig Carole Curb Nemoy
| writer         = Dana Lustig Annette Goliti Gutierrez
| story          = Annette Goliti Gutierrez John Corbett Jonathan Penner Charles Martin Smith Stephanie Beacham
| music          = Tal Bergman Paul Christian Gordon
| cinematography = Kent L. Wakeford
| editing        = Caroline Ross Curb Entertainment
| distributor    = Legacy Releasing
| released       = October 18, 1996  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $44,052
}} 5th Dimension song "Wedding Bell Blues".  

==Cast==
* Illeana Douglas as Jasmine
* Paulina Porizkova as Tanya Touchev
* Julie Warner as Micki Rachel Levine John Corbett as Cary Maynard Philco
* Jonathan Penner as Matt Smith
* Charles Martin Smith as Oliver Napier
* Richard Edson as Tom
* Carla Gugino as Violet
* Debbie Reynolds as Herself

==Release== R Rating, grossed only $44,052 in the domestic box office, on a maximum/initial release of 11 theaters. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 

 