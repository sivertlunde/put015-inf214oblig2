Tera Naam Mera Naam
{{Infobox film
| name           = Tera Naam Mera Naam
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       =  Ramesh Talwar
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       ={{Plain list| 
*Suparna Anand
*Tanvi Azmi
*Shafi Inamdar
*Deepak Tijori 
*Ajit Pal
*Karan Shah
}}
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = 
| language       = Hindi
| budget         = 
| gross          = 
}}

Tera Naam Mera Naam is a 1988 Hindi language romance comedy film directed by Ramesh Talwar. Karan Shah plays the role of a scheduled caste boy who loves a girl belonging to a higher class. He exchanges his identity with another boy belonging to a higher caste.

In his book Eena Meena Deeka: The Story of Hindi Film Comedy, film critic Sanjit Narwekar described it as a "new wave twist to an age old mainstream situation."   

==References==
 

== External links ==

*  

 
 
 
 


 
 