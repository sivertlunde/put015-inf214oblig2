Adharmam
 
 
 
{{Infobox film
| name           = Adharmam
| image          = 
| caption        = 
| director       = Ramesh Krishnan
| producer       = G. Ramesh G. Suresh G. Satheesh
| writer         = Ramesh Krishnan N. Chandra Mohan (dialogues)  Murali Ranjitha Vijayakumar Vadivelu Ajay Rathnam
| music          = Ilaiyaraaja
| cinematography = V. Manikandan
| editing        = B. Ramesh
| studio         = G. K. Films International
| distributor    = G. K. Films International
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
}}
 1994 Tamil language action film directed by Ramesh Krishnan. The film features Murali (Tamil actor) |Murali, Ranjitha and Nassar in lead roles. The film had musical score by Ilaiyaraaja and was released on Tamil New Years day, 14 April 1994. V. Manikandan debuted as cinematographer from this film.  

==Cast== Murali
* Ranjitha
* Nassar
* Thalaivasal Vijay
* Vadivelu Vijayakumar
* Thalapathi Dinesh
* Ajay Rathnam
* S. N. Lakshmi
* R. P. Viswam
* Janaki

==Music==
 Vaali and Ilaiyaraaja.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers
|-
| 1 || Dharmam Endru || Ilaiyaraaja
|-
| 2 || Muthu Mani || S. P. Balasubrahmanyam, S. Janaki
|-
| 3 || Nooru Vayasu || Ilaiyaraaja
|-
| 4 || Oru Pakkam Oru Nyaayam || Ilaiyaraaja
|-
| 5 || Thakathana || S. P. Balasubrahmanyam, S. Janaki
|- Mano
|-
| 7 || Vambookara || K. S. Chithra
|}

==References==
 

 
 
 
 
 


 