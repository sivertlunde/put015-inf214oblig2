It's a Great Life (1929 film)
{{Infobox film
| name           = Its a Great Life
| image          = Its a Great Life poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Sam Wood
| producer       = 
| screenplay     = Al Boasberg Willard Mack
| story          = Alfred Block Byron Morgan Leonard Praskins
| starring       = Rosetta Duncan Vivian Duncan Lawrence Gray Jed Prouty Benny Rubin
| music          = William Axt
| cinematography = J. Peverell Marley Frank Sullivan
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Its a Great Life is a 1929 American comedy film directed by Sam Wood and written by Al Boasberg and Willard Mack. The film stars Rosetta Duncan, Vivian Duncan, Lawrence Gray, Jed Prouty and Benny Rubin. The film was released on December 6, 1929, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==	 
*Rosetta Duncan as Casey Hogan
*Vivian Duncan as Babe Hogan
*Lawrence Gray as Jimmy Dean
*Jed Prouty as Mr. David Parker
*Benny Rubin as Benny Friedman 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 