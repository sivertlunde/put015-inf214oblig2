A Good Year
 
 
 
{{Infobox film
| name           = A Good Year
| image          = A Good Year.jpg
| caption        = Original poster
| director       = Ridley Scott
| producer       = Ridley Scott
| screenplay     = Marc Klein
| based on       =  
| starring       = Russell Crowe Albert Finney Marion Cotillard Didier Bourdon Abbie Cornish Tom Hollander Freddie Highmore
| music          = Marc Streitenfeld
| cinematography = Philippe Le Sourd
| editing        = Dody Dorn
| distributor    = 20th Century Fox
| country        = United Kingdom
| released       =  
| runtime        = 118 minutes
| language       = English French
| budget         = $35 million
| gross          = $42,064,105 
}}  romantic comedy-drama 2004 novel of the same name by British author Peter Mayle.

==Plot==
In the prologue, a young Max Skinner spends his childhood summer holidays learning to appreciate the finer things in life at his Uncle Henrys vineyard estate in Provence in south-eastern France. Some 25 years later, Max is an unethical, aggressive, hard-working London-based bond Bond trader|trader. 

Following his uncles death, Max is the sole beneficiary of the French property. He travels to Provence to prepare a quick sale. Shortly after arriving he knocks a local café owner, named Fanny Chenal, off her bicycle as a result of his careless driving. Subsequently, he discovers that his latest City financial stunt has landed him in hot water with the UK government and with his firms directors, necessitating his return to London. 

To assist in his planned sale of the property, Max hurriedly snaps some photos and in the process falls into an empty swimming pool. He is unable to escape until Fanny Chenal, driving by and spotting his rental car, appears and turns on the water supply in retaliation. This delay causes Max to miss his flight and having failed to report to the directors in person, he is suspended from work and trading activities for one week.

On Henrys estate, Max must deal with a gruff, dedicated winemaker, Francis Duflot, who fears being separated from his precious vines. Duflot pays a vineyard inspector to tell Max that the soil is bad and the vines worthless. 

In the meantime, they are surprised by the unexpected arrival of young Napa Valley oenophile Christie Roberts, who is backpacking through Europe and claims to be Henrys previously unknown illegitimate daughter. Max is concerned that she might lay claim to the estate. Worried about being usurped by Kenny, his second-in-command (Maxs secretary uses a cell phone to send proof to Max) in London (through whom Max continues to direct trades), Max intentionally gives the ambitious young trader bad advice, which gets him fired.

Max becomes enamoured with the beautiful, feisty café owner Fanny, who is rumoured to have sworn off men. He successfully woos Fanny into his bed, where she leaves Max the next morning expecting him to return to his life in London. 

A disillusioned Christie also decides to move on and the estate is sold. Max returns to his life in London where Sir Nigel, the company chairman, offers Max a choice: "Money or your life." He must either choose a discharge settlement, which includes "a lot of zeros," or accept the offer of a partnership in the trading firm, where he would then be "made for life".

Max chooses the money. He then connives to invalidate the estates contract of sale by forging a letter from Henry (as a child he signed cheques for his uncle, therefore being able to replicate his handwriting and signature) confirming that Christie is his daughter and also has a valid claim on the property. 

He puts his London residence up for sale and returns to Provence, entering into a relationship with Fanny. Christie also returns and she and Francis now jointly have to run the vineyard while trying to reconcile their vastly different philosophies of wine production. Meanwhile Max is now able to focus his entire attention on Fanny.

==Cast==
*Russell Crowe as Max Skinner
*Albert Finney as Uncle Henry
*Marion Cotillard as Fanny Chenal
*Abbie Cornish as Christie Roberts
*Didier Bourdon as Francis Duflot
*Isabelle Candelier as Ludivine Duflot
*Freddie Highmore as Young Max Skinner
*Tom Hollander as Charlie Willis
*Rafe Spall as Kenny
*Richard Coyle as Amis
*Archie Panjabi as Gemma
*Kenneth Cranham as Sir Nigel
*Daniel Mays as Bert, the doorman.
*Giannina Facio as Maitre D

==Production==

===Development and writing===
 
Ridley Scott had owned a house in Provence for fifteen years,    and wanted to film a production there. Scott Free president Lisa Ellzey recommended the works of author Peter Mayle, who had written best-selling books set in the south of France. Scott and Mayle were acquaintances and neighbours, having worked together in advertising and commercials during the 1970s, but as the author did not want to write a screenplay, he instead wrote a new book after discussing a film plot with Scott. Screenwriter Marc Klein was brought in after Scott read an adaptation he did of The Girls Guide to Hunting and Fishing – eventually released in 2007 as Suburban Girl. 
 flashbacks which "occur just as another scene" where it would depict "the grooming of Max as child which will be used as payoffs for the three acts that follow". Klein described Henry as "sounding like Albert Finney" so Scott hired the actor, with whom he had worked in The Duellists.    Scott brought Russell Crowe as the protagonist Max. The actor stated that it was a good opportunity for them to reunite after 2000s Gladiator (2000 film)|Gladiator as "it just seemed more fun to go into this smaller place, where the problems werent as vast." The character was considered a change from Crowes usual roles, with some noting it may reflect "maturity" or "contentment", with Australias Courier-Mail dubbing him "A Mellow Fellow". Crowe said of his life at the time: "  relaxed ... Work isnt the most important thing in my life now. Its not even in the top ten." The actor also stressed the importance of his family.  Scott also stated one of the reasons for the project was that he had "not done much in the way of comedy" and it seemed to be a good opportunity to "keep challenging yourself". 

===Filming===
The film was shot throughout nine weeks in 2005,  mostly in locations Scott described as "eight minutes from my house". French locations were filmed at Bonnieux, Cucuron and Gordes in Vaucluse, Marseille Provence Airport, and the rail station in Avignon. London locations included Albion Riverside in Battersea, Broadgate, the Bluebird Cafe on Kings Road in Chelsea, London|Chelsea, and Criterion Restaurant in Piccadilly Circus. 
 Wimbledon courts. Fannys cafe was shot in a Gordes restaurant, with designer Sonja Klaus decorating it with items bought from second-hand shops considering the character would have done the same. Klaus employed a kitsch decoration on Duflots estate to show it was "a character keeping up with the Joneses – if it was in America, he would drive a golden Cadillac with leopard skin print seats", and decorated the large water basin of Cucuron with floating candles to "make it look like a fabulous event" for Maxs dinner with Fanny. 

===Music===
Marc Streitenfeld worked as a music editor on Hans Zimmers Remote Control Productions and was invited by Scott to make his debut as a film score composer.  The soundtrack includes "Moi Lolita" by Alizée, "Breezin Along with the Breeze" by Josephine Baker, "Gotta Get Up", "Jump into the Fire", and "How Can I Be Sure of You" by Harry Nilsson, "Hey Joe" by Johnny Hallyday, "Vous, qui passez sans me voir" and "Jattendrai" by Jean Sablon, "Le chant du gardien" by Tino Rossi, "Je chante" by Charles Trenet, "Old Cape Cod" by Patti Page, "Walk Right Back" by the Everly Brothers, "Boum" by Adrien Chevalier, and "Itsy Bitsy Petit Bikini" by Richard Anthony. The CD includes only 15 songs from the film; several are left out.

==Release==

===Box office===
The film was budgeted at $35&nbsp;million. The total worldwide gross is $42,061,749.  Although exceeding its production budget, the film was a Box office bomb.  As of 2010, it has earned over $7&nbsp;million in US DVD sales. 

===Critical response===
The film received negative reviews. In Variety (magazine)|Variety, Todd McCarthy called the film "a divertissement, an excuse for the filmmakers and cast to enjoy a couple of months in Provence and for the audience, by proxy, to spend a couple of hours there. A simple repast consisting of sometimes strained slapsticky comedy, a sweet romance and a life lesson learned, this little picnic doesnt amount to much but goes down easily enough." "Crowe executes a lightweight change of pace with his charisma entirely intact ... Cotillard is enchanting. Abbie Cornish...has a nice note of observant reserve. The setting could hardly be made to look less than glorious, and production standards are up to what one expects from a Scott picture." 

In his review in The New York Times, Stephen Holden called it "an innocuous, feel-good movie", "a sun-dappled romantic diversion", and "a three-P movie: pleasant, pretty and predictable. One might add piddling." 
 Under the Tuscan Sun for a robustly romantic diversion." 

Kenneth Turan of the Los Angeles Times observed, "  scenery may be attractive and the cast likewise, but something vital is missing in this all-too-leisurely film ... The fact that we know exactly what will happen to Max from the moment he appears on screen is not whats wrong with A Good Year. After all, we go to films like this precisely because the satisfaction of emotional certainty is what were looking for."

Jessica Reaves of the Chicago Tribune rated the film two stars out of a possible four and described it as "Despite the occasional seductive moment, A Good Year ... deep as a wading pool, as substantive as cotton candy." 

In the UK, Peter Bradshaw of The Guardian called it "a humourless cinematic slice of tourist gastro-porn". 

A Good Year holds a 25% rating on Rotten Tomatoes based on 120 reviews.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 