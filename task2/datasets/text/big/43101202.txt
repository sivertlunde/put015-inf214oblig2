Thayamma
{{Infobox film
| name           = Thayamma
| image          =
| image_size     =
| caption        = 
| director       = Gopi Bhimsingh
| producer       = M. Jagadeeswaran
| writer         = Gangai Amaran  (dialogues) 
| screenplay     = Gopi Bhimsingh
| story          = Kaloor Dennis
| starring       =  
| music          = Ilaiyaraaja
| cinematography = Balu Thevan
| editing        = B. Lenin V. T. Vijayan
| distributor    =
| studio         = P.M.S. Cine Arts
| released       =  
| runtime        = 125 minutes
| country        = India
| language       = Tamil
}}
 1991 Cinema Indian feature directed by Geetha and Oru Thalai Ragam Shankar. The film also starred Goundamani as well as Senthil (actor)|Senthil. The film was a remake of Malayalam film Thoovalsparsham which was inspired by the 1985 French film Three Men and a Cradle.   The film was dubbed in Telugu as Bujjipapa Brahmacharulu.

==Plot==

Three young bachelors Pandiyan (Pandiyan (actor)|Pandiyan), Anand (Anand Babu) and Babu (Babu) are friends and room-mates in an apartment. One day, Pandiyan warns his friends that a parcel will arrive during his absence. The next day, Anand and Babu find a baby (Baby Radha) before their door. The three bachelors swear that they are not her father. Since then, their lives are completely changed. First, they try to abandon the baby but then they take care of the baby and name her Thayamma. What transpires later forms the crux of the story.

==Cast==
 Pandiyan as Pandiyan
*Anand Babu as Anand
*En Uyir Thozhan Babu as Babu
*Baby Radha as Thayamma
*Shankar Panikkar as Rangarajan
*Goundamani as Sisubalan Senthil as Azhagappan Geetha as Kalyani
*Gowri
*Venniradai Moorthy as Bhoothalingam
*Ra. Sankaran
*Oru Viral Krishna Rao as Anands uncle
*Typist Gopu
*Vikraman
*M. N. Rajam
*Brinda
*Mani Meenu
*Shruthi
*Premi

==Soundtrack==

{{Infobox Album |  
| Name        = Thayamma
| Type        = soundtrack
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1991
| Recorded    = 1991 Feature film soundtrack
| Length      = 23:45
| Label       = 
| Producer    = Ilaiyaraaja
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1991, features 4 tracks with lyrics written by Gangai Amaran.  
 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Mano || 4:14
|- 2 || Enga Paattukku || Malaysia Vasudevan, Mano, S. N. Surendar || 4:53
|- 3 || Oru Muthukili Kathum || Arunmozhi, Mano, S. N. Surendar || 4:49
|- 4 || Pazhaiya Kanavai || K. S. Chithra || 4:51
|} 	

==References==
 

==External links==

 
 
 
 
 
 
 


 