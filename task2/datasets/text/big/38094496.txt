Mastermind (film)
{{Infobox film
| name           = Mastermind
| image          = Mastermind (film).jpg
| image_size     =
| alt            =
| caption        = 
| director       = Alex March Malcolm Stuart
| writer         = William Peter Blatty and Ian McLellan Hunter
| based on = 
| narrator       =
| starring       = Zero Mostel
| music          = Fred Karlin
| cinematography = Gerald Hirschfeld
| editing        = John C. Howard
| studio         = 
| distributor    = 
| released       = 1976
| runtime        = 86 minutes
| country        = 
| language       = English
| budget         = $2.5 million "ABCs 5 Years of Film Production Profits & Losses", Variety, 31 May 1973 p 3 
| gross          = 
| preceded_by    =
| followed_by    =
}} Malcolm Stuarts A Shot in the Dark which Blatty had co-written with producer/director Blake Edwards. Blattys script was drastically revised by Ian McLellan Hunter prior to production and the disgruntled screenwriter chose the pseudonym of Terence Clyne for his screen credit. By 1973 it had recorded a loss of $2.9 million.  Blattys original screenplay was published as part of a limited edition collection by Lonely Road Books in 2013 under the title, Five Lost Screenplays by William Peter Blatty.

==Cast==
* Zero Mostel	as 	Inspector Hoku Ichihara
* Keiko Kishi	as 	Nikki Kono
* Gawn Grainger	as 	Nigel Crouchback
* Bradford Dillman	as	Jabez Link
* Frankie Sakai	as	Captain Yamada
* Sorrell Booke	as 	Max Engstrom
* Felix Silla	as 	Schatzi
* Jules Munshin	as 	Israeli Agent 
* Phil Leeds	as 	Israeli Agent #2

==References==
 

==External links==
* 

 