Almenrausch und Edelweiß
{{Infobox film
| name           = Almenrausch und Edelweiß
| image          = 
| image_size     = 
| caption        = 
| director       = Harald Reinl
| producer       = Kurt Hammer (executive producer)
| writer         = Franz Marischka (screenplay) & Ilse Lotz-Dupont (screenplay) & Tibor Yost (screenplay) Willy Stock (story) & Albert Janschek (story)
| narrator       = 
| starring       = See below
| music          = Nils Nobach
| cinematography = Walter Riml
| editing        = Eva Kroll
| studio         = 
| distributor    = 
| released       = 1957
| runtime        = 85 minutes
| country        = West Germany Austria
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Almenrausch und Edelweiß is a 1957 West German / Austrian film directed by Harald Reinl.

== Plot summary ==
 

=== Differences from source ===
 

== Cast ==
*Elma Karlowa as Ilonka Ferency
*Karin Dor as Maresi Meier
*Bert Fortell as Robert Teichmann
*Harald Juhnke as Max Lachner
*Paul Westermeier as Generaldirektor Ferdinand Meyer
*Maria Andergast as Friedl Meier
*Joseph Egger as Förster Fenninger
*Theodor Danegger as Hotelportier Xandl
*Theo Lingen as Kammerdiener Leo Amadeus Schulze

== Soundtrack ==
*Das Hansen-Quartett - "Der starke Max aus Halifax"
*Elma Karlowa and Harald Juhnke dubbed by Das Hansen-Quartett - "Ich tanz heut ohne Schuh"
*Das Hansen-Quartett - "Liebling, denk an Mich" (Music by Gert Wilden)

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 


 
 
 