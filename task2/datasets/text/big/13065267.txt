The Midnight Cabaret
 
{{Infobox film
| name           = The Midnight Cabaret
| image          =
| caption        =
| director       = Larry Semon
| producer       = Larry Semon
| writer         = Larry Semon
| starring       = Oliver Hardy
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}

The Midnight Cabaret is a 1923 American film directed by Larry Semon and featuring Oliver Hardy.   

==Cast==
* Larry Semon - Larry, a Waiter
* Kathleen Myers - Kathleen, a Cabaret Performer
* Oliver Hardy - Oliver, an Impetuous Suitor (as Babe Hardy)
* Fred DeSilva
* William Hauber
* Al Thompson
* Joe Rock

==See also==
* List of American films of 1923
* Oliver Hardy filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 