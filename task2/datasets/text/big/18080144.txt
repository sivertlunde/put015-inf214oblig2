Father of Four
{{Infobox film
| name           = Father of Four
| image          = Far til fire DVD.jpg
| image_size     =
| caption        = Front cover of the Danish DVD
| director       = Alice OFredericks Robert Saaskin
| producer       = Lau Lauritzen Jr. Henning Karmark
| writer         = Lis Byrdal  Kaj Engholm(cartoon)
| narrator       =  Birgitte Bruun
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen
| editing        = Wera Iwanouw
| distributor    = ASA Filmudlejning
| released       = 2 November 1953
| runtime        = 97 min.
| country        = Denmark
| language       = Danish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Danish family Birgitte Bruun. The film is based on the comic strip by Kaj Engholm and Olaf Hast. It was the inaugural film in a series of eight Father of Four films made by ASA Films, one each year from 1953 to 1961.

==Cast==
*Ib Schønberg as Father; Schønberg also played the role in the first sequel, but unfortunately died before they could finish any further films and was then replaced by Karl Stegger in the next 6 films. Niels Olsen plays the father in the reimagined series of the 2000s.
 Birgitte Bruun as Søs; Bruun, later known as Birgitte Price, played the part in the first six films.
*Rudi Hansen as Mie
*Otto Møller Jensen as Ole; Jensen played the role in all of the first films and then left acting for fashion design.
*Ole Neumann as Little Per; Neumann was 5-years-old when he played Per, and played the same role in each of the first seven sequels. He later became a documentary cinematographer
*Peter Malberg as Uncle Anders; Buster Larsen played the role pf Uncle Anders in subsequent sequels
*Jørgen Reenberg as Teacher Jørgen Stæhr
*Ove Sprogøe as Baker Høst
*Sigurd Langberg as Director Andersen
*Ib Mossin as Peter
*Paul Hagen as Burglar
*Ilselil Larsen as Grete
*Agnes Rehni as Mrs. Sejersen
*Svend Aage Madsen as Kristian
*Else Jarlbak as Kristians Mother
*Poul Reichhardt as Himself
*Einar Juhl as Pastor
*Svend Bille as Train Conductor
*Hugo Herrestrup
*Poul Thomsen

==Story origin==
In 1947, Hakon Steffensen, editor of the Politiken newspaper wanted to start running a comic strip that depicted typical Danish life. It was his response to the glut of American comic strips flooding the European marketplace in the post-war years. Steffensen asked cartoonist Kaj Engholm for ideas. Engholm then asked his friend, advertising executive Olav Hast, and Hast proposed the father of four idea. Together, Engholm and Hast roughed out the story of a single father of four children with the oldest daughter running the household. The cartoonists agreed that the father would be a single parent without ever creating a back story for the mother absence. She might have died or left the father for someone else. In interviews, whenever the authors were asked, "Where is the mother?", they replied, "I dont know but we promise to look into it." 

The comic strip first appeared in 1948 and ran daily for 40 years—on the back page of Politiken until 1955, then in the Berlingske Tidende newspaper until 1988. The text was written by Hast until retired from the strip in 1973, after which it was written by a variety of writers. Engholm drew the strip until his death in 1988.

==Sequels==
Father of Four was such an enormous success that a sequel Father of Four in the Snow (Far til fire i sneen) was quickly made and released in 1954. Thereafter, seven more sequels were made, one every year until 1961, all directed by Alice OFredericks. A ninth film was planned for release in 1962 but was never produced.

In 2005, the series was renewed with the release of   (Far til fire - i stor stil).

{|class="wikitable" width="80%" Original title||Notes
|-
|1954|| Father of Four in the Snow ||Far til fire i sneen|| directed by Alice OFredericks
|-
|1955||Father of Four in the Country||Far til fire på landet||
|-
|1956||Father of Four in the City|| Far til fire i byen||
|- 1957 ||Father of Four and Uncle Sofus||Far til fire og onkel Sofus||
|- 1958 ||Father of Four and the Wolf Cubs||Far til fire og ulveungerne||
|- 1959 ||Father of Four on Bornholm||Far til fire på Bornholm||
|- 1960 ||Father of Four with Full Music||Far til fire med fuld musik||
|- 1971 ||Father of Four in Good Humor|| || directed by Ib Mossin
|-
|2005|| Father of Four Never Gives Up|| || directed by Claus Bjerre
|-
|2006|| || ||
|-
|2008||  || ||
|-
|2010||Father of Four on Japanese || ||
|-
|2011||Father of Four Back in the Nature || || 
|-
|2012|| || || 
|-
|2014|| || || directed by Giacomo Campeotto
|-
|2015|| ||Far til Fires vilde ferie|| 
|}

==References==
 

==External links==
* 
* 
* 
*  
 

 
 
 
 
 
 
 
 