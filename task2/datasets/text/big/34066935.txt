Nurmoo: Shout from the Plain
{{Infobox film
| name           = Nurmoo: Shout from the Plain
| image          =
| caption        =
| director       = Harri J. Rantala
| producer       = Harri J. Rantala
| writer         = Harri J. Rantala
| starring       = Eerik Kantokoski Anna Alkiomaa Oskari Katajisto Kalevi Haapoja
| music          = Tapio Ylinen
| cinematography = Anton Verho
| editing        = Harri J. Rantala
| studio         = Nurmo-Filmi
| distributor    = Nurmo-Filmi
| released       =  
| runtime        = 30 minutes
| country        = Finland
| language       = Finnish
| budget         =
| gross          =
}}
Nurmoo: Shout from the Plain ( ) is a 2009 Finnish film directed by Harri J. Rantala and starring Eerik Kantokoski, Anna Alkiomaa, Oskari Katajisto and Kalevi Haapoja.

==Plot==
Summer 1939 Little village Nurmo at the Finnish countryside is planning an international wrestling match that versus the whole world. But as autumn 1939 comes everything changes and the story forms into a legend. The time to tell it has now come.

==Cast==
* Eerik Kantokoski as Erkki 
* Anna Alkiomaa as Anna 
* Oskari Katajisto as Coach 
* Kalevi Haapoja as Tradesman 
* Juha Koistinen as Paavo 
* Karri Liikkanen as Heinis 
* Hannu Rantala as Kloppi 
* Arto Honkala as Jaakko 
* Saara Saastamoinen as Miina 
* Reeta Annala as Enni
* Laura Rämä as Maria
* Janna Haavisto as Laina
* Kauko Salo as Father
* Panu Mikkola as Goljat

== Festivals ==
Nurmoo: Shout from the Plain have been screened round the world in 33 festivals in 18 countries.

*62nd Cannes Film Festival, France 2009 (Short Film Corner)
*17th St. Petersburg International Film Festival, Russia 2009 (Official Competition) 
*8th Dokufest International Film Festival, Prizren Kosovo 2009 (Official Competition)
*5th Budapest Short Film Festival, Budapest Hungary 2009 (Official Competition) 
*22nd Helsinki International Film Festival, Helsinki Finland 2009 (Official Selection) 
*12th Auburn International Film Festival, Sydney Australia 2009 (Official Competition)
*17th Santiago International Film Festival, Santiago Chile 2009 (Official Competition)
*3rd L´Aquila International Film Festival, L´Aquila Italy 2009 (Official Competition)  
*11th Madurai International Film Festival, Madurai India 2009 (Official Competition)
*11th Dhaka International Film Festival, Dhaka Bangladesh 2010 (Official Competition)
*33rd Grenzland-Filmtage Film Festival, Selb Germany 2010 (Official Competition)
*62nd Montecatini International Film Festival, Montecatini Italy 2011 (Official Competition)

==See also==
* 2009 in film
* Cinema of Finland
*  

==References==
{{reflist |refs=

}}

==External links==
*  

 
 
 
 
 


 