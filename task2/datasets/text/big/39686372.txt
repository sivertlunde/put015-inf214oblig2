Perjury (film)
{{Infobox film
| name           = Perjury
| image          = 
| image_size     = 
| caption        = 
| director       = Georg Jacoby 
| producer       = Seymour Nebenzal
| writer         = Georg C. Klaren   Herbert Juttke 
| narrator       =  Alice Roberts   Francis Lederer   Miles Mander   Inge Landgut
| music          = 
| editing        =
| cinematography = Willy Goldberger
| studio         = Nero Film 
| distributor    = Vereinigte Star-Film 
| released       = 26 April 1929
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German drama Alice Roberts, Francis Lederer and Miles Mander.  The films art direction by Andrej Andrejew.

==Main cast==
*  Alice Roberts (actress)| Alice Roberts as Inge Sperber 
* Francis Lederer as Fenn 
* Miles Mander as Adolf Sperber 
* Inge Landgut as Elschen Sperber 
* Paul Henckels as Staatsanwalt  La Jana as Daisy Storm 
* Carl Auen as Verteidiger 
* Gerd Brieseas Polizeioffizier

==References==
 

==Bibliography==
* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910-1933. Berghahn Books, 2005.

==External links==
* 

 

 

 
 
 
 
 
 
 