Breaking In
 
{{Infobox film
| name = Breaking In
| image = Breaking in poster.jpg
| image_size = 
| caption =
| director  = Bill Forsyth
| producer = Harry Gittes
| writer = John Sayles
| starring = Burt Reynolds Casey Siemaszko Lorraine Toussaint
| music = Michael Gibbs
| cinematography = Michael Coulter
| editing = Michael Ellis Act III Communications
| distributor = The Samuel Goldwyn Company
| released = October 13, 1989
| runtime = 94 minutes
| country = United States
| language = English
| budget = $6,000,000 
| gross = $1,877,618 (US)  Gerry Molyneaux, "John Sayles, Renaissance Books, 2000 p 182 
}} crime comedy directed by written by John Sayles. It stars Burt Reynolds, Casey Siemaszko and Lorraine Toussaint. It is a movie about how professional small-time criminals live and practice their trades.

==Plot== Bonnie and Clyde."   
 Sheila Kelley). Harry Carey), adversarial lawyers (Maury Chaykin and Stephen Tobolowsky).

==Cast==
* Burt Reynolds as Ernie Mullins
* Casey Siemaszko as Mike Lafeve
* Lorraine Toussaint as Delphine the Hooker Sheila Kelley as Carrie aka Fontaine
* Albert Salmi as Johnny Scot, Poker Player Harry Carey as Shoes, Poker Player
* Maury Chaykin as Vincent Tucci, Attorney
* Stephen Tobolowsky as District Attorney
* Eddie Driscoll as Paul the Apostle

==Release and reception==
===Box office===
*The film opened in 400 theaters at #12 in its opening weekend (10/13–15) with $679,200. 
*The films final gross was $1,877,618.

==References==
 

==External links== 
*  
*  
*  

 
 
 
 
 
 
 