Marudhamalai
 
 
{{Infobox film
| name           = Marudhamalai
| image          =
| caption        = Suraj
| producer       = Viswanathan Ravichandran
| writer         = Suraj Arjun Vadivelu Nila Lal Lal
| music          = D. Imman
| cinematography = U. K. Senthil Kumar Vaidy S
| editing        = Manoj Aascar Film Pvt. Ltd
| released       =  
| runtime        =
| country        = India
| language       = Tamil
| budget         =
| gross          =
}}
Marudhamalai is a Tamil language film starring Arjun Sarja|Arjun, Meera Chopra|Nila, and Vadivelu. The film was released in 7 September 2007. During its release, this film had a grand opening and it became a commercial success at the Box Office depending on the size of movies budget.

==Plot==
Marudhamalai (Arjun Sarja|Arjun), a police constable, goes to work in the village of Nachiapuram after passing his police exams and getting a merit. Here, he meets Encounter Ekambaram (Vadivelu) who is his senior officer. He is asked to do all sorts of work for him and one day, Marudhamalai loses a convict on the way to the court due to Ekambarans soft heart. He then is to clean the floors of the inspectors house and there, he falls in love with Nila. Then, later in the market, after Ekambaram is humiliated by a beggar, Marudhamalai sees Maasi, the boss of the city, killing a person who stood for a candidate in the elections. Then the election commissioner comes to the place and he discovers that elections have not been held for 16 years in the city due to Maasis opposition.

Also, he challenges Maasi to stop him from holding elections in that area. Then, during the election day, despite lot of security, Maasis men with the help of a man who acts as a guard come and drive away the people who came for voting and are about to kill the commissioner when Marudhamalais father slaps Maasi for creating violence. At that time, Marudhamalai comes and beats up Maasis men and humiliates him in front of all the people by handcuffing him with his dhoti. He is then arrested and all attempts to release him fail. For his bravery, he is promoted to the inspector of the station. Later, he kills Maasis two brothers using various methods and Maasi vows to kill him the day he is released. Later, as Maasi is released, Marudhamalai gets an order to kill him. He fights the men single handedly and kills them proving his bravery. He also starts loving Nila and she starts loving him too.

==Cast== Arjun as Marudhamalai Nila
*Vadivelu as Encounter Ekambaram
*Raghuvaran as Suryanarayanan Lal as Maasi
*V. M. Subburaj as Saarapaambu
*Krishnamoorthy as Maatturavi
*Sharan Preeth
*Nassar Rajesh
*Santhana Bharathi Thyagu
*Kadhal Thandapani as Puli
*Shanmugarajan
*Bose Venkat
*M. N. Rajam
*Kalairani
*Mumaith Khan (Special appearance)

==Soundtrack==
The soundtrack features six songs composed by D. Imman and lyrics penned by Pa.Vijay and Thabu Shankar

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Artist(s)
|-
| "Hey Yenmama" Megha
|-
| "Marudhamalai" Anitha Venkatnarasimhan Karthikeyan
|-
| "Marudhamalai II" Anitha Venkatnarasimhan Karthikeyan
|-
| "Oonjaliley Oru Angeley"
| Karthik (singer)|Karthik, Dr.G.K.Lavanya
|-
| "The Khakhee Story"
| Vasu, Naveen, Karthikeyan
|-
| "Yenna Velay"
| D. Imman, Jyotsna Radhakrishnan
|}

 
 

 
 
 
 
 
 