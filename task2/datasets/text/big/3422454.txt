Boss'n Up
{{Infobox film  |
  name     = Bossn Up |
  image          = Bossnup.jpg |
  producer         = Snoop Dogg and Dylan C. Brown |
  director       = Dylan C. Brown |
  writer         = Dylan C. Brown | Larry McCoy (actor) Lil Jon  Shillae Anderson Scruncho|
  music         = Snoop Dogg |
  cinematography = Max Da-Yung Wang |
  editing         = Jason Resmer |
  distributor    = Snoopadelic Films(USA)  Big Pook Films (USA)   Geffen Records   (music provider) (USA)   CodeBlack Entertainment (outside US)|
  released   =   |
  budget = es. $550,000 |
  runtime        = 88 minutes |
  language = English |
}}
 

Snoop Doggs "musical" film inspired by his preceding album R&G (Rhythm & Gangsta) The Masterpiece, Boss n up debuted on 6 December 2005. It came out in DVD format. It was the first release of the newly founded Snoopadelic Films, run by Snoop Dogg.

Snoop stars as a grocery clerk named Cordé Christopher, a young and magnetic man, subject to keen female interest. He becomes a Mentoring|protégé of Orange Juice, an experienced pimp, after being informed of the better and richer life he can lead if he becomes a pimp. OJ cultivates Cordés talent and shares street knowledge with him, including the Rules of the "Pimping" Game. To some degree OJ treats Corde like a son. After achieving success Cordé has to choose between the love of his life, Chardonnay Allen, and his successful career as a pimp.

The casting includes other musicians like Atlanta rapper Lil Jon plays the role of Sheriff, a strip club owner who becomes a partner in Cordes business, Dominique, Cordes attractive attorney who frees him from prison, and comedian Scruncho is Hucky G, Cordes own pimping protégé.

== Box office and business  ==
{| class="wikitable"
|align="center"| Sales
|align="center"| Revenue
|align="center"| Screens
|align="center"| Country Codes
|-
| 150.000 copies
| $1 million - $2 million
| 1,000,000 theaters
| FR, UK, JP,   Television: AUS, RU, BE, LUX, NL
|}

==Awards and nominations==
{| class="wikitable"
|align="center" colspan="4"| Source Awards
|-
|align="center"| Year
|align="center"| Result
|align="center"| Category
|align="center"| Lost to 
|-
| 2006
| Nomination
| Best Hip Hop Movie ATL
|}

==List of songs performed or played in the musical interludes==
*"Powerful" performed by Blizzard (of the Block Boiz)
*"Perfect" performed by Snoop Dogg, Pharrell and Charlie Wilson
*"I Love to Give You light" performed by Snoop Dogg, Ricky Harris and Tanya Devine
*"Panties Lead in" performed by Adam Gillohm and Phillip Rodriguez
*"Fresh Pair of Panties on" performed by Snoop Dogg and J. Black
*"The Battle" performed by Allokol Peete
*"Girl Like You" performed by Snoop Dogg
*"The Finer Things" performed by J. Davey
*"Stay" performed by Jelly Roll
*"Get 2 Know Ya" performed by Snoop Dogg and Jelly Roll
*"Closer" performed by Snoop Dogg
*"Promise I" performed by Snoop Dogg
*"Supernatural Things part I." performed by Ben E. King
*"Bossn Up (theme song)" performed by Snoop Dogg
*"Remember me" performed by Larrance Dopson and Lamar Edwards of T.I.s new reality show "Life On Mars"
*"Love dont go through no changes" performed by Sister Sledge
*"The Bidness" performed by Snoop Dogg
*"Nights over Egypt" performed by The Jones Girls
*"Can You Control Yo Hoe" performed by Snoop Dogg and Soopafly
*"Drop it like its hot" performed by Snoop Dogg
*"Im Threw Witchu" performed by Snoop Dogg
*"South" performed by 4chaluv
*"Pass it, Pass it" performed by Snoop Dogg
*"Hustle & Struggle" performed by J. Black
*"Full of Fire" performed by Al Green
*"Helicopter" performed by Cliff Martinez
*"Lets Get Blown" performed by Snoop Dogg
*"O-H-I-O" performed by The Ohio Players
*"I Like Everything About You" performed by Willie Hutch
*"Bongo Instrumental" performed by Niggerachie
*"Butterfly Blues" performed by Adam Gillohm and Phillip Rodriguez
*"I Do I Do" performed by Leroy Hutson
*"Oh No" performed by Snoop Dogg
*"Now At Last" performed by Feist
*"Cold Cold World" performed by Teddy Pendergrass
*"No Thang On Me" performed by Snoop Dogg and Bootsy Collins
*"Step Yo Game Up" performed by Snoop Dogg, Lil Jon and Trina
*"Ups & Downs" performed by Snoop Dogg (and The Bee Gees)

==Additional Material==
*Bonus CD Jelly Roll) - 03:38
#Drop It Like Its Hot (Remix) (Feat Pharrell And Jay-Z) (Produced by The Neptunes) - 04:16 
#No Sticks, No Seeds - 04:19 (Produced by Terrace Martin)
#Shake That Shit (Feat Tiffany Foxxx & Young Watt) (Produced by Terrace Martin)- 03:49 
 

*Singles
*:Get to Know Ya - digital download on MSN.
 

==References==
 
* 
* 

== External links ==
*  
*  
*  

 
 
 
 
 
 