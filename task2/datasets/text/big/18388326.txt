You Can't Take It With You (film)
{{Infobox film
| name           = You Cant Take It With You
| image          = You_Cant_Take_It_with_You_1938_Poster.jpg
| border         = 
| caption        = Theatrical release poster
| director       = Frank Capra
| producer       = Frank Capra
| screenplay     = Robert Riskin
| based on       =  
| starring       = {{Plainlist|
* Jean Arthur
* Lionel Barrymore
* James Stewart Edward Arnold
}}
| music          = Dimitri Tiomkin Joseph Walker
| editing        = Gene Havlick
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 126 minutes
| country        = United States
| language       = English
| budget         = US$1,644,736  (est.) 
| gross          = {{Plainlist|
* US$2,137,575  (US rentals) 
* US$5,295,526  (Intl rentals) 
}}
}}
 Edward Arnold. Pulitzer Prize-winning play of the same name by George S. Kaufman and Moss Hart,  the film is about a man from a family of rich snobs who becomes engaged to a woman from a good-natured but decidedly eccentric family.
 Best Director for Frank Capra. This was Capras third Oscar for Best Director in just five years, following It Happened One Night (1934) and Mr. Deeds Goes to Town (1936). It was also the highest-grossing picture of the year.

==Plot== Edward Arnold) Clarence Wilson), to offer a huge sum for the house, and if that is not accepted, to cause trouble for the family.  

Kirbys son, Tony (James Stewart), a vice president in the family company, has fallen in love with a company stenographer, Alice Sycamore (Jean Arthur). When Tony proposes marriage, Alice is worried that her family would be looked upon poorly by Tonys rich and famous family. In fact, Alice is the only relatively normal member of the eccentric Sycamore family, led by Grandpa Vanderhof (Lionel Barrymore). Unbeknownst to the players, Alices family lives in the house that will not sell out.

Kirby and the snobbish Mrs. Kirby (Mary Forbes) strongly disapprove of Tonys choice for marriage. Before she accepts, Alice forces Tony to bring his family to become better acquainted with their future in-laws. But when Tony purposely brings his family on the wrong day, the Sycamore family is caught off-guard and the house is in disarray.  As the Kirbys are preparing to leave after a rather disastrous meeting, the police arrest everyone in the house for making unlicensed fireworks and disturbing the peace.  
 Harry Davenport) repeatedly asks why the Kirbys were at the Vanderhof house. When Grandpa says it was to talk over selling the house, Alice has an outburst and says it was because she was engaged to Tony but is spurning him because of how poorly she has been treated by his family. This causes a sensation in the papers, and Alice flees the city.

With Alice gone, Grandpa decides to sell the house, thus meaning the whole section of the town must vacate in preparation for building a new factory. Now, the Kirby companies merge, creating a huge fluctuation in the stock market.  When Kirbys competitor, Ramsey (H. B. Warner), dies after confronting him for being ruthless and a failure of a man, Kirby has a realization that he does not have any friends&nbsp;– just as Grandpa Vanderhof told him back in the drunk tank. 

Kirby visits the Vanderhofs as they are moving out of the house, and Kirby lets loose and plays the harmonica and realizes these lower-class people he previously belittled are good people. Alice takes Tony back and the film ends with the Vanderhofs and Kirbys enjoying a meal together.

==Cast==
 
* Jean Arthur as Alice Sycamore
* Lionel Barrymore as Grandpa Martin Vanderhof
* James Stewart as Tony Kirby Edward Arnold as Anthony P. Kirby
* Mischa Auer as Boris Kolenkhov
* Ann Miller as Essie Carmichael
* Spring Byington as Penny Sycamore
* Samuel S. Hinds as Paul Sycamore
* Donald Meek as Poppins
* H. B. Warner as Ramsey
* Halliwell Hobbes as DePinna
* Dub Taylor as Ed Carmichael
* Mary Forbes as Mrs. Anthony P. Kirby
* Lillian Yarbo as Rheba Eddie Anderson as Donald Clarence Wilson as Mr. John Blakeley Charles Lane as Wilbur G. Henderson, IRS agent Harry Davenport as the Night Court Judge
* Ian Wolfe as A.P. Kirbys secretary (unbilled)
* Ward Bond as detective (unbilled)
 
 

==Production notes== A Christmas Carol.
* Ann Miller, who plays Essie Carmichael (Ed Carmichaels wife), was only 15 years old when this movie was filmed.
 Academy Awards==
;Wins   
*   Best Director: Frank Capra

;Nominations Best Supporting Actress: Spring Byington
*  
*  
*  
*  , Sound Director

==Adaptations to other media==
You Cant Take it With You was adapted as a radio play on the October 2, 1939 broadcast of Lux Radio Theater with Edward Arnold, Robert Cummings and Fay Wray.

==Digital restoration==  digital restoration of the film was done by Sony Colorworks. The digital pictures were frame by frame digitally restored at Prasad Corporation to remove dirt, tears, scratches and other artifacts, restoring the film to its original look.  

==See also==
* Lionel Barrymore filmography
* Frank Capra filmography
* James Stewart filmography

==References==
;Citations
 

;Bibliography
 
*  
 

==External links==
 
*  
*  
*  
*  
*  
*  
*   on Lux Radio Theater: October 2, 1939 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 