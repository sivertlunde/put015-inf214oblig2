The Cube (film)
 
{{Infobox television film
| name           = The Cube
| image          =  
| image_size     = 180px
| director       = Jim Henson
| producer       =
| writer         = Jim Henson Jerry Juhl
| starring       = Richard Schaal
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = February 23, 1969
| runtime        = 54 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Cube is a hour-long teleplay that aired on NBCs weekly anthology television show NBC Experiment in Television in 1969. The production was produced and directed by puppeteer and filmmaker Jim Henson, and was one of several experiments with the live-action film medium which he conducted in the 1960s, before focusing entirely on The Muppets and other puppet works. The screenplay was co-written by long-time Muppet writer Jerry Juhl.

The teleplay only aired twice: first on February 23 of 1969, with a rerun on February 21 of 1971.

==Synopsis==
  maintenance man Hugh Webster) who discovers it covered in strawberry jam as he wipes it down. After Arnie leaves, the Man tries to find the door. He encounters a variety of people come through various hidden doors in which each one claim that he can get out through his own door.

His first visitor is a woman named Margaret (Lolo Farell) who claims that the man is her husband Ted as she appears with parents (Alice Hill and Moe Margolese) are also present. Her door closes on the Man before she can get him out.

The Man then meets Mr. Thomas (Rex Sevenoaks) the manager of this entire establishment. When asked about the Cube, Mr. Thomas states that he has asked himself that question many times where it is too good to be true. Mr. Thomas shows the Man the Call Button that he should push if he ever needs anything. Mr. Thomas then works to deny the fact that there are other Cubes like this while stating that while some people are quite content and stay in the Cube, others would wish to leave. As Mr. Thomas leaves, he tells the Man that he must find his own door.

Arnie returns again where he gives the Man a telephone and states that he can get him anything he wants. Arnie exclaims that the only trouble with this establishment is that there is no organization and The Man wouldnt believe the weirdos that run it. He also talks about the four tons of chocolate rabbits while mentioning that hes been working here for 14 years where there hasnt been a request for chocolate rabbits. As Arnie goes outside to call the Man to see if the telephone is working, the Man sees a brief opening until the telephone rings and Mr. Thomas briefly appears to give the receiver to him before leaving. The Man answers it and Arnie tells him that the telephone is in business. The Man discovers that his telephone only calls up Arnie. When the Man throws it towards the wall, Arnie opens it as it flies through as Arnie rewards his accuracy by giving him a chocolate rabbit.

Two police officers enter with a search warrant. They find different things in the panels of the Cube including a bound and gagged Dr. Kingsley. The Man is then handcuffed in an uncomfortable way as the police officers leave with everything.

A House painter and decorator named Miss Bix (Sandra Scott) comes in while the Man tries to get out of her handcuffs as Arnie comes in with the mustard paint in order to decorate the Cube. She then changes her mind and wants tangerine paint as the Man struggles to get his leg out from the position the handcuffs have him in. Upon trying to hold back the info on how long the Man will be in the Cube, Miss Bix changes her mind about the paint and has shellack sprayed onto the already white Cube as she leaves. Arnie states to the Man that he is actually spraying deodorant and that Miss Bix cant tell the difference. Before leaving, Arnie stops the Man from following him since this is Arnies door.

A guitarist (Ralph Endersby) comes in and gives the Man the keys to the handcuffs which frees the Man. Upon finding a bed that manifested, the Man rests on it while the guitarist practices his music. The guitarist is then joined by the rest of his band as they sing a song to the Man about never getting out of the Cube. After the band leaves, Mr. Thomas briefly enters telling the Man that the "Rest Period" is over.

A prisoner named Watson (Jack Van Evera) enters the Cube having escaped from his Cube. He notices that the Man hasnt been here for long while mentioning how his Cube had squares while the Mans Cube has rectangles. Watson tells the Man that he had to tell how many times has passed by making marks in his thumbnail which didnt work when he lost his thumbnails. Watson decides to return to his Cube to see if he left anything behind. When the Man states that if he re-enters his Cube where he wont get out again, Watson ranted about having to depend on the squares. Watson decides to go back to his Cube. When Mr. Thomas enters asking if someone else was in his Cube, the Man states that there was a visitor. Mr. Thomas states that other visitor was acting and introduces him to Jack Van Evera who asks Mr. Thomas if they can take out the part about the thumbnails. As they leave, Jack asks Mr. Thomas what the rest of these Cubes are for anyway. Before leaving, Mr. Thomas states "He was only kidding. You know that."
 nurse (Jean Christopher) run some medical tests on the Man while Cora leaves. The doctors tell the Man his results before leaving to tend to a platypus.

A professor (Don McGill) enters stating that him being here is either part of a teleplay or hes hallucinating. He even shows him the ending with him in the Cube with a girl before leaving.

A straight man (John Granic) enters where he finds the Mans Cube is all white thinking that its a mausoleum for whiteness while claiming that the Man will die in the Cube. The Man claims that this isnt his place and had nothing to do with the Cubes construction. When the Straight Man tells the Man to go out the door, the Man states that he cant since its the Straight Mans door. The Straight Man then leaves quoting "You make me sick."

A classy party manifests in the Mans cube. When Mr. Thomas arrives offering tonic to the guests, the Man tries to get some only to be blocked by a barrier. A female partygoer exclaims that Mr. Thomas is projected and states that nobody having a party in the Mans Cube is real. The image of the party then disappears as it shows the Man on his stool.

After a panel briefly opens where an old man (Eric Clavering) asks if he has considered that he is dead and this is what his afterlife is like, the Man is visited by a scientist who asks the Man to define reality for him. When the Man claims that the hammer doesnt exist, the scientist proves him wrong by throwing the hammer at one of the cubes walls causing a hole in it. Arnie reprimands the scientist for breaking the wall and gives him his hammer back while he gets the wall fixed. Upon being unable to convince the Man, the scientist leaves hoping that the Man rots in the Cube. Arnie fixes the hole and leaves stating that the scientist is a pain in the neck.

A woman enters where she gives the Man a hint on how to get out which is "its going to get worse before it gets better." Her other hint is "dont trust anybody." After she leaves, Mr. Thomas enters stating that the Mans time is up and that he must leave. As the Man tries to leave through the door that Mr. Thomas claims to be the Mans door, the Man has his suspicions in which if he steps out the door, two gorillas in ballerina dresses would grab him, throw him to the ground, and dance around him singing "Home Sweet Home." The Man attempts to step out and is assaulted by the ballerina-dressed gorillas (depicted as two men in gorilla suits) in the manner that he described. Mr. Thomas leaves stating that the Man is getting pretty good at predicting these things.

Two comics enter and tell jokes to the audience. They then turn their attention to the Man when they find that he wasnt joining in on the laughter. The Man stated that he just didnt feel like laughing. The comics then laugh at the Man before leaving.

A kid on a tricycle rides around the Cube mocking the Man stating that hes never going to get out of the Cube.

A monk (Jerry Nelson) enters and disperses his wisdom onto the Man while stating that he is part of the "All." Before leaving, the monk gives the Man an orb called the Ramadar which is supposed to hold the meaning of life. After the monk leaves, the Ramadar only makes a grinding noise. The Man smashes it with the stool to find that inexplicably it is made of strawberry jam inside. Arnie comes in and cleans up the broken Ramadar stating that most people break their Ramadar. Arnie then leaves with the broken Ramadar and the broken stool. After six people bring in a coffin, the Man finds a gun is left in the room. Grabbing the gun, the man attempts to shoot himself and ink squirts onto his face.

All the people he had encountered enter and laugh at him as Arnie states that this is all a joke. Enraged, he tells them hes had enough with their tricks and that no matter what happens he knows he is real. The Man then leaves the Cube as the people applaud him. The Man is then escorted into an office so that the head of the organization can sign his release. Once there, the Man reflects on the revelation of his own realness. He accidentally cuts himself with a knife while demonstrating and is asked to taste his blood. He does so and discovers his blood is strawberry jam. The head of the organization and the office fades away to reveal the Man is still trapped in the cube.

As the credits roll, the Man wanders around the room one last time and then sits down on the floor apparently resigned to his fate of never getting out while the bands song is reprised in the background.

==Cast==
* Richard Schaal - The Man in the Cube Hugh Webster - Arnie
* Rex Sevenoaks - Mr. Thomas
* Jack Van Evera - Himself/Watson
* John Granic - Straight Man, Sergeant
* Guy Sanvido - Comic, Officer Fritz
* Eliza Creighton - Cora
* Don Crawford - Black Militant
* Jerry Nelson - Monk
* Sandra Scott - Miss Bix
* Claude Rae - Dr. Connors
* Don McGill - Professor
* Ralph Endersby - Guitarist
* Trudy Young - Liza 1
* Ruth Springford - Liza 2
* Moe Margolese - Dr. Bingham, Father-In-Law
* Alice Hill - Mother-In-Law, Mrs. Stratton
* Lolo Farell - Margaret
* Eric Clavering - Dr. Bradowski, Old Man Nurse
* Jerry Juhl - Pipe-Smoking Party Guest (uncredited)
* Jim Henson - Gorillas (voice, uncredited)

==Influences== The Squirrel Fun With Your New Head (Doubleday, 1968).

At one point Henson comments on his own teleplay through a Professor who wanders in from yet another door:
:PROFESSOR: Well, as I interpret what you’re doing here, this is all a very complex discussion of reality versus Illusion. The perfect subject for the television medium!
:MAN: What do you mean, television?
:PROFESSOR: Well, this is a television play.
:MAN: What?
:PROFESSOR: Oh, you don’t believe that?
:MAN: Of course not!
:PROFESSOR: I should have thought you’d want to. After all, theres only one other possible explanation.
:MAN: Which is?
:PROFESSOR: Hallucination. That you are altogether insane.

==Adaptations== German Fringe Theater troupe Glassbooth presented a live stage adaptation of The Cube titled KUBUS, directed by Roger Hoffmann and starring Jens Dornheim. 

In 2012, Tale of Sand, a graphic novel co-written by Henson and Juhl, was released utilizing similar set pieces and sight gags. Although released long after The Cube, the script for Tale of Sand actually predates it by several years.

==See also==
* Cube (film series)

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 