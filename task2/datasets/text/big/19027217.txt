Forever Activists
 
{{Infobox Film
| name = Forever Activists
| image = Foreveractivists-1-.jpg
| image_size =
| caption =
| director = Connie Field Judith Montell
| producer =
| writer = Yasha Aginsky Phil Cousineau Judith Montell
| narrator = Ronnie Gilbert
| starring =
| music =
| cinematography =
| editing = Yasha Aginsky
| distributor =
| released = 1990
| runtime = 60 min.
| country = United States
| language = English
| budget =
}} American veterans Best Documentary Feature.   

==Summary==
The film makes the point that for many of the men and women who fought in the Spanish Civil War it was just one of a series of continuing social struggles that they believed in.

The scene-setting material (old newsreel clips) is familiar, but veterans faces and their stories seem ever new and raw, even when it is apparent that the same tales have been told many times before. All of those who testify are creatures of the Great Depression, idealistic and dedicated to improving the lot of the people President Franklin D. Roosevelt addressed as his fellow United States|Americans.

The films highlight is a reunion of the veterans in Spain in 1986, on the 50th anniversary of the wars outbreak. It looks to have been both a picnic and a time for summing up. Yet the films most moving moments come when the veterans recall their treatment when they went home after the war.

Passports were revoked, and for many their membership in the brigade was to cost them jobs and reputations for decades to come. Most, it seems, absorbed the shocks and went on to fight on behalf of Labor movement|labor, civil rights and the peace movement during the Vietnam War.

==See also==
*Abraham Lincoln Brigade
*Jewish volunteers in the Spanish Civil War
*Spanish Civil War
*List of American films of 1990
;Other documentaries about Jewish activism:
*Professional Revolutionary

==References==
 
*{{cite news
 | last =Canby
 | first =Vincent
 | title =Forever Activists (1990)
 | publisher = New York Times
 | date =June 14, 1991
 | url =http://movies.nytimes.com/movie/18220/Forever-Activists/overview
 | accessdate = August 25
}}

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 


 