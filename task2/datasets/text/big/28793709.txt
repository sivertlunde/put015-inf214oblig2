Shantha Sakkubai
{{Infobox film
| name           = Shantha Sakkubai
| image          = Cp19shantha sakubhai 68988e.jpg
| caption        = Poster of film Shantha Sakkubai
| director       = Sundar Rao Nadkarni
| writer         =
| starring       = K. Aswathamma, Bannibai, K. Sarangapani, Kothamangalam Seenu, Kothamangalam Subbu, P. G. Venkatesan, S. S. Rajamani, K. Aranganayaki, T. S. Krishnaveni, M. A. Ganapathi Bhat
| producer       = Salem Shankar Films
| distributor    =
| music          = Songs : Papanasam Sivan Background Score : Thuraiyur Rajagopala Sarma
| cinematography =
| editing        = Sundar Rao Nadkarni
| released       = 1939
| runtime        = Tamil
}}

Shantha Sakkubai was a 1939 Tamil film starring K. Aswathamma, Bannibai, K. Sarangapani, Kothamangalam Seenu, Kothamangalam Subbu, P. G. Venkatesan, S. S. Rajamani, K. Aranganayaki, T. S. Krishnaveni, M. A. Ganapathi Bhat and Sundar Rao Nadkarni. The film was directed by acclaimed director Sundar Rao Nadkarni.

== Production ==

The film was a devotional movie based on a Marathi folk tale. Most of the actors in the film were Kannada-speakers. The leading role was played by the Kannada actress Aswathamma. The music for the film was composed by Thuraiyur Rajagopala Sarma and the lyrics were written by Papanasam Sivan. Rajagopala Sarma appears in a Pandharpur festival sequence singing a bhajan. Sundar Rao Nadkarni too appears in the movie, singing and dancing an abhang.

==References==
*  

 
 
 
 


 