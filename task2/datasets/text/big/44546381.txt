The Franciscan Friars of Killarney
{{infobox film name = The Franciscan Friars of Killarney director = Sidney Olcott producer = Kalem Company cinematography = George K. Hollister
| released =  
| runtime = 
| country = United States language = Silent film (English intertitles) 
}}
The Franciscan Friars of Killarney is a 1911 American silent documentary produced by Kalem Company. It was directed by Sidney Olcott.

==Production notes==
The film was shot in Beaufort, co Kerry, Ireland, during summer of 1911.

==References==
* Michel Derrien, Aux origines du cinéma irlandais: Sidney Olcott, le premier oeil, TIR 2013. ISBN 978-2-917681-20-6  

==External links==
* 
*    website dedicated to Sidney Olcott

 
 
 
 
 
 
 

 