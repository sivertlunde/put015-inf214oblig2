Mission Incredible: Adventures on the Dragon's Trail
{{Infobox film
| name           = Pleasant Goat and Big Big Wolf: Mission Incredible: Adventures on the Dragons Trail
| image          = Adventures on the Dragons Trail poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Jian Yaozong
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Creative Power Entertaining
| released       =  
| runtime        = 
| country        = China
| language       = Mandarin
| budget         = 
| gross          = US$25.9 million  
}}
Pleasant Goat and Big Big Wolf: Mission Incredible: Adventures on the Dragons Trail (喜羊羊与灰太狼之开心闯龙年 Xǐ Yáng Yáng yǔ Huī Tài Láng zhī kāixīn chuǎng lóngnián "Pleasant/Happy Lamb/Goat and Grey Wolfs Happy Year of the Dragon (zodiac)|Dragon") is a 2012 Chinese animated film based on the animated television series Pleasant Goat and Big Big Wolf. 

In the film, Weslie and his crew discover an evil mechanical dragon who defeats Wolffy as he was attempting to capture the goats, but a series of good dragons rescue Weslie and the goats. The good dragons say that evil dragons have taken over their world, and they need the help of the goats.

==Characters==

Lambs
* Weslie (喜羊羊 Xǐ Yáng Yáng) - Zu Liqing (祖丽晴) in Mandarin
* Slowy (慢羊羊 Màn Yáng Yáng) - Gao Quansheng (高全勝) in Mandarin
* Tibbie (美羊羊 Měi Yáng Yáng) - Deng Yuting (邓玉婷) in Mandarin
* Paddi (懒羊羊 Lǎn Yáng Yáng) - Liang Ying (梁 颖) in Mandarin
* Sparky (沸羊羊 Fèi Yáng Yáng) - Liu Hongyun (刘红韵) in Mandarin
* Jonie (暖羊羊 Nuǎn Yáng Yáng) - Deng Yuting in Mandarin
Wolves Zhang Lin (張　琳) in Mandarin
* Wolnie (红太狼 Hóng Tài Láng) - Zhao Na (趙　娜) in Mandarin
* Willie (小灰灰 Xiǎo Huī Huī) - Liang Ying in Mandarin
Dragons
* Chamelon  (变色龙 Biànsèlóng "Chameleon") - Liu Hongyun in Mandarin
* Molle (轰龙龙 Hōng Lónglóng "Red Dragon")
* Orito (钻地龙 Zuāndi Lóng "Drill Earth Dragon")
* Quinto (朦朦龙 Méngméng Lóng "Deceive Dragon")
* Raho (七窍玲龙 Qīqiàolíng Lóng "No Human Head Feature (Eye, ear, nostril, mouth) Dragon")
* Drago (小黑龙 Xiǎo Hēilóng "Little Black Dragon")
** Drago appears as an innocent victim of the Tyranno-Rex, but he turns out to be the operator of it.
* Xiao Shen Long (小神龙 Xiǎo Shénlóng "Little Divine/Mysterious Dragon") - Luo Yanqian (骆妍倩) in Mandarin
** Xiao Shen Long teaches the goats and the wolves  . Retrieved on 27 August 2012.  The Walt Disney Company and Toon Express made an agreement to share licensing income generated from sales of merchandise with Xiao Shen Long for a three-year period. 
* Archaeo (蝶龙 Diélóng "Butterfly Dragon")

==References==
 

==External links==
 
*    ( )
**  ( )
* 
 

 
 
 
 