We Don't Live Here Anymore
{{Infobox film
| name           = We Dont Live Here Anymore
| image          = Here anymore ver2.jpg
| image_size     =
| caption        = Theatrical poster John Curran
| producer       = Naomi Watts Harvey Kahn Jonas Goodman
| writer         = Short stories:  
| narrator       =
| starring       = Mark Ruffalo Laura Dern Naomi Watts Peter Krause
| music          = Michael Convertino
| cinematography = Maryse Alberti
| editing        = Alexandre De Franceschi
| distributor    = Warner Independent Pictures
| released       =  
| runtime        = 101 minutes
| country        = United States Canada English
| budget         = $3,000,000
| gross          = $2,037,545
}} 2004 drama John Curran. It is based on the short stories We Dont Live Here Anymore and Adultery by Andre Dubus.

Set in Washington state, the film was shot around Vancouver. 

==Cast==
*Mark Ruffalo as Jack Linden
*Laura Dern as Terry Linden
*Peter Krause as Hank Evans
*Naomi Watts as Edith Evans

==References==
 

==External links==
*  
*   official web site.

 

 
 
 
 
 
 
 
 


 