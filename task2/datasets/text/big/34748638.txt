Rust and Bone
 
{{Infobox film
| name = Rust and Bone
| image = Rust and Bone poster.jpg
| caption = Theatrical release poster
| director = Jacques Audiard
| producer = Jacques Audiard Martine Cassinelli Pascal Caucheteux
| screenplay = Jacques Audiard Thomas Bidegain
| based on =  
| starring = Marion Cotillard Matthias Schoenaerts
| music = Alexandre Desplat
| cinematography = Stéphane Fontaine
| editing = Juliette Welfling
| studio = Why Not Productions Canal+ Ciné+ France Télévisions UGC Distribution Lumière StudioCanal
| released =  
| runtime = 123 minutes  
| country = France Belgium
| language = French
| budget = €15.4 million (United States dollar|$20 million)
| gross = $25,807,712 
}} romantic drama short story collection of the same name. It tells the story of an unemployed 25-year-old man who falls in love with a killer whale trainer. 

The film competed for the Palme dOr at the 2012 Cannes Film Festival       and received positive early reviews and a ten-minute standing ovation at the end of its screening.  It was also nominated for a Screen Actors Guild Award, two Golden Globes, two BAFTA Awards and nine César Awards, winning four.

==Plot==
Alain van Versch, an unemployed father in his mid 20s known as Ali, arrives in Antibes, southern France, to look for work to support his young son, Sam. Having no money, he crashes with his sister Anna, who already has her own share of problems with money and temporary employment.
 bouncer in a nightclub but still keeps his passion burning for fighting. On a usual evening in the night club, Ali meets Stéphanie Ménochet and escorts her safely to her home after she is injured in a brawl at the club. She works at a local marine tourist park where she suffers a tragic accident during a show and wakes up in the hospital to realize that her legs have been amputated. Ali meets a guy at work who informs him about a kick boxing fixture he can make money from. Stéphanie, now in a wheelchair, is terminally depressed and gives Ali a call. Ali visits her and takes her to a beach where they swim.

Over a period of time, Ali and Stéphanie spend a lot of time together and get involved in casual sex every now and then, while Ali hooks up with other women as well, being honest about it with Stéphanie. Stéphanie starts to accompany Ali to the mixed martial arts fights and a symbiosis forms between them. Stéphanie starts to feel better about herself in Alis company and gets artificial limbs. She starts to walk again and even manages Alis bets for his fighting. Ali, Stéphanie, and some friends visit the same night club where Ali used to work. Ali goes to the dance floor and flirts with a girl as Stéphanie watches curiously. Ali goes away with the girl leaving a dejected Stéphanie with other friends.

Anna is fired from her job when the managers realize she has been taking home expired food products. Anna blames Ali for this, as Ali was involved in an odd job where he installed spy cameras in work areas at the direction of the management to spy on the activities of their employees. This results in a standoff between Ali and Annas partner and Ali leaves. Sam stays with Anna while Ali goes to a combat sports training facility near Strasbourg (as evidenced by earlier references in the film and the fact that Annas partner drops Sam off on the way to a delivery in Colmar),losing touch with Stéphanie. Sam visits Ali at his facility for a day in deep winter and both are shown playing in the snow and on a frozen lake. A weak spot on the frozen lake cracks and Sam falls through the ice, and swiftly loses consciousness, submerged in the icy waters. Ali momentarily distracted by a call of nature, takes a while to realize that Sam has had a dangerous accident. Once he spots the hole in the ice and finds Sam in the frozen lake, Ali releases a desperate volley of punches to break the surface and gets an unconscious Sam out. In the process, Ali fractures his hands.

Sam survives and in the hospital Ali breaks down while talking to Stéphanie on the phone and confesses his love for her. As the film closes, an unknown character, possibly Ali, is shown to be celebrating a post-match victory at a professional sports combat event in Warsaw.  As Ali narrates, he explains how severe injuries to the hands are a lifelong disability which at first seems contradictory to the victorious nature of the scene until he takes Sam by the hand on the way out of the door, at last without distraction from his parental duties. They are joined by Stéphanie.

==Cast==
* Marion Cotillard as Stéphanie
* Matthias Schoenaerts as Ali
* Armand Verdure as Sam
* Corinne Masiero as Anna
* Céline Sallette as Louise
* Bouli Lanners as Martial
* Mourad Frarema as Foued
* Jean-Michel Correia as Richard
* Yannick Choirat as Simon

==Production==
The film was produced by Why Not Productions for €15.4 million.  It was co-produced with   guys." 

==Release==
  65th Cannes UGC Distribution the same day.  StudioCanal UK acquired the British distribution rights,  and the film was released in the UK on 2 November 2012. It opened in the United States on 23 November 2012. 

===Critical reception===
The film was screened at the 2012 Cannes Film Festival and received early positive critical reactions. Rotten Tomatoes gives the film a score of 82% based on 156 reviews  Metacritic gave the film a rating of 73/100, based on 39 reviews.  HitFix praised Audiard "for the way he takes melodramatic convention and bends it to his own particular sensibility, delivering a powerful tale about the reminders we all carry of the pains that have formed us" and found Cotillards work "incredible, nuanced and real".  Peter Bradshaw of The Guardian gave the film a four-star rating out of five, writing Rust and Bone is "a passionate and moving love story which surges out of the screen like a flood tide" and "its candour and force are matched by the commitment and intelligence of its two leading players".  Time (magazine)|Times Mary Corliss found that the romance is "sometimes engrossing, sometimes exasperating" and that the cinematography recalls Kings Row and An Affair to Remember. Corliss also wrote, "Schoenaerts exudes masculinity that is both effortless and troubled" while "Cotillard demonstrates again her eerie ability to write complex feelings on her face, as if from the inside, without grandstanding her emotions" and added, "her strong, subtle performance is gloriously winning on its own".  Michael Phillips of the Chicago Tribune thought Schoenaerts sensitive-brute instincts recall Marlon Brando and Tom Hardy.  Critic A. O. Scott of The New York Times called the film "a strong, emotionally replete experience, and also a tour de force of directorial button pushing."  Roger Ebert, who did not review the film upon its original release, later added it to his "Great Films" series and gave four stars.  Cate Blanchett wrote a review for Variety (magazine)|Variety praising Marion Cotillards performance in the film, describing it as "simply astonishing" and said that "Marion has created a character of nobility and candour, seamlessly melding herself into a world we could not have known without her. Her performance is as unexpected and as unsentimental and raw as the film itself". 

===Awards and nominations===
{| class="wikitable" style="font-size:small;" ;
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | List of accolades
|- style="text-align:center;"
! style="background:#ccc; width:35%;"| Award / Film Festival
! style="background:#ccc; width:25%;"| Category
! style="background:#ccc; width:32%;"| Recipient(s)
! style="background:#ccc; width:8%;"| Result
|- style="border-top:2px solid gray;" Cannes Film Festival Palme dOr Jacques Audiard
| 
|-
|rowspan="9"|César Awards  Best Film Jacques Audiard
| 
|- Best Director Jacques Audiard
| 
|- Best Actress Marion Cotillard
| 
|- Most Promising Actor Matthias Schoenaerts
| 
|- Best Adapted Screenplay Jacques Audiard and Thomas Bidegain
| 
|- Best Original Score Alexandre Desplat
| 
|- Best Cinematography
|Stéphane Fontaine
| 
|- Best Editing Juliette Welfling
| 
|- Best Sound Brigitte Taillandier, Pascal Villard and Jean-Paul Hurier
| 
|-
|rowspan="4"|Étoiles dOr Best Film  Jacques Audiard 
| 
|- Best Actress Marion Cotillard
| 
|-  Best Male Newcomer Matthias Schoenaerts 
| 
|-  Best Screenplay Jacques Audiard and Thomas Bidegain
| 
|- Globes de Cristal Award Best Film  Jacques Audiard 
| 
|- Best Actress Marion Cotillard
| 
|-  2nd AACTA AACTA Awards  AACTA International Best International Actress Marion Cotillard
| 
|- BFI London Film Festival 2012 BFI Best Film Jacques Audiard
| 
|- Lumiere Awards
| Best Director Jacques Audiard
| 
|- Best Screenplay Jacques Audiard and Thomas Bidegain
| 
|- Best Actress Marion Cotillard 
| 
|- Best Actor Matthias Schoenaerts 
| 
|- Best Film Jacques Audiard
| 
|- British Independent Film Awards Best International Independent Film
| 
|  
|- British Academy of Film and Television Arts Best Actress in a Leading Role
| Marion Cotillard
|  
|-
| Best Film Not in the English Language
!
|  
|-
| rowspan="2"| Broadcast Film Critics Association   Best Actress
| Marion Cotillard
|  
|- Best Foreign Language Film
!
|  
|- Cabourg Romantic Film Festival
| Best Film (Meilleur film) Jacques Audiard
| 
|- David di Donatello Awards
| Best European Film Jacques Audiard
| 
|- Golden Trailer Awards
| Best Foreign TV Spot – For "Sexy Action"
|
| 
|- Best Foreign TV Spot – For "Reviews TV:30"
|
| 
|- Hawaii International Film Festival Best Actress Marion Cotillard
| 
|-
| Best Film
| Jacques Audiard
| 
|- Irish Film and Television Awards Best International Actress Marion Cotillard
| 
|-
| Chicago Film Critics Association  
| Best Foreign-Language Film
!
|  
|- Golden Globe Awards  Golden Globe Best Foreign Language Film
|Rust and Bone
| 
|- Golden Globe Best Actress – Motion Picture Drama Marion Cotillard
| 
|- Golden Reel Awards 
| Best Sound Editing: Sound Effects, Foley, Dialogue and ADR in an Animation Feature Film
|
|  
|- Goya Awards   Best European Film
!
|  
|- Hollywood Film Festival  Best Actress of the Year Marion Cotillard
|  
|-
| Sant Jordi Awards
| Best Foreign Actress
| Marion Cotillard
|  
|-
| Online Film Critics Society Awards
| Best Film Not in the English Language
!
|  
|-
|rowspan="3"| Dublin Film Critics Circle Best Film
|
| 
|- Best Actress Marion Cotillard
| 
|- Best Actor Matthias Schoenaerts
| 
|-
| Rembrandt Awards Best International Actress Marion Cotillard
|  
|-
| North Carolina Film Critics Association Best Foreign Language Film
|
|  
|- Georgia Film Critics Association
| Best Film Jacques Audiard
| 
|- Best Foreign Film Jacques Audiard
| 
|- Best Actress Marion Cotillard 
| 
|- Best Actor Matthias Schoenaerts 
| 
|- Best Adapted Screenplay Jacques Audiard and Thomas Bidegain
| 
|-
| Houston Film Critics Society  
| Best Foreign Language Film 
!
|  
|- Independent Spirit Independent Spirit Awards   Best International Film
|Rust and Bone
| 
|- London Film Critics Circle   Foreign Language Film of the Year
|Rust and Bone
| 
|-
| Technical Achievement Award
| Alexandre Desplat (music)
|  
|- 3rd Magritte Magritte Awards 
| Best Actor
| Matthias Schoenaerts
|  
|- Best Foreign Film in Coproduction
!
|  
|-
| Best Supporting Actor
| Bouli Lanners
|  
|- 19th Screen Screen Actors Guild Awards   Screen Actors Outstanding Performance by a Female Actor in a Leading Role Marion Cotillard
| 
|- Telluride Film Festival Silver Medallion Marion Cotillard
| 
|-
|rowspan="3"| Valladolid International Film Festival Best Actor Matthias Schoenaerts
| 
|- Best Director Jacques Audiard
| 
|- Best Screenplay Craig Davidson, Thomas Bidegain, Jacques Audiard 
| 
|-
|rowspan="3"| International Cinephile Society Best Film Not on the English Language
|
| 
|- Best Actress (Runner-up) Marion Cotillard
| 
|- Best Adapted Screenplay Jacques Audiard and Thomas Bidegain
| 
|-
| Vancouver Film Critics Circle
| Best Actress
| Marion Cotillard
|  
|-
| rowspan="2"| Washington D.C. Area Film Critics Association Awards   Best Actress
| Marion Cotillard
|  
|-
| Best Foreign Language Film
!
|  
|- World Soundtrack World Soundtrack Awards World Soundtrack Soundtrack Composer of the Year Alexandre Desplat
| 
|}

==See also==
* 2012 in film
* Cinema of France
* Cinema of Belgium

==References==
 

== External links ==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 