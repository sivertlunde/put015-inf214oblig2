The Reader (2008 film)
{{Infobox film
| name           = The Reader
| image          = Reader_ver2.jpg
| image_size     = 
| alt            = 
| border         = yes
| caption        = Theatrical release poster
| director       = Stephen Daldry
| producer       = Anthony Minghella Sydney Pollack Donna Gigliotti Redmond Morris David Hare
| based on       =  
| starring       = Kate Winslet Ralph Fiennes David Kross Lena Olin Bruno Ganz
| music          = Nico Muhly
| cinematography = Chris Menges Roger Deakins
| editing        = Claire Simpson Mirage Enterprises Neunte Babelsberg Film GmbH
| distributor    = The Weinstein Company   Senator Film  
| released       =  
| runtime        = 124 minutes  
| country        = United States Germany 
| language       = English German Greek Latin
| budget         = $32 million 
| gross          = $108,901,967 
}} romantic drama novel of David Hare and directed by Stephen Daldry. Ralph Fiennes and Kate Winslet star along with the young actor David Kross. It was the last film for producers Anthony Minghella and Sydney Pollack, both of whom had died before it was released.  Production began in Germany in September 2007, and the film opened in limited release on December 10, 2008.

It tells the story of Michael Berg, a German lawyer who as a mid-teenager in 1958 had an affair with an older woman, Hanna Schmitz, who then disappeared only to resurface years later as one of the defendants in a war crimes trial stemming from her actions as a guard at a Nazi concentration camp. Michael realizes that Hanna is keeping a personal secret she believes is worse than her Nazi past&nbsp;– a secret which, if revealed, could help her at the trial.

Winslet and Kross, who plays the young Michael, received much praise for their performances; Winslet won a number of awards for her role, including the Academy Award for Best Actress. The film itself was nominated for several other major awards, including the Academy Award for Best Picture.

==Plot== flashing back to a tram in 1958 Neustadt (urban district)|Neustadt. A 15-year-old Michael (David Kross) gets off because he feels sick and wanders the streets, pausing in the entryway of a nearby apartment building where he vomits. Hanna Schmitz (Kate Winslet), a tram Conductor (transportation)|conductor, comes in and helps him return home.

Michael, diagnosed with scarlet fever, rests at home for the next three months. After he recovers, he visits Hanna with flowers to thank her. The 36-year-old Hanna seduces him and they begin an affair. They spend much of their time together having sex in her apartment after she has had Michael read to her from literary works he is studying. After a bicycling trip, Hanna learns she is being promoted to a clerical job at the tram company. She abruptly moves without telling Michael.
 law school. SS guards death march following the 1944 evacuation of a concentration camp near Krakow. Michael is stunned that Hanna is one of the defendants.

In the trial the key evidence is the testimony of Ilana Mather (Alexandra Maria Lara), author of a memoir of how she and her mother (Lena Olin), who also testifies, survived. She describes how Hanna had women from the camp read to her in the evenings.

Hanna, unlike her co-defendants, admits that Auschwitz was an extermination camp and that the ten women she chose during each months Glossary of Nazi Germany#S|Selektion were gassed. She denies authorship of a report on the church fire, despite pressure from the other defendants, but then admits it rather than complying with a demand to provide a handwriting sample.

Michael realizes Hannas secret: she is illiterate and has concealed it her whole life. The other guards who claim she wrote the report are lying to place responsibility on Hanna. Michael informs the professor that he has information favourable to one of the defendants but is not sure what to do since the defendant herself chose not to disclose the information. Michael arranges a visit with Hanna in prison, but once there he leaves without seeing her.

Hanna receives a life sentence for her admitted leadership role in the church deaths while the other defendants are sentenced to four years and three months each. Michael (Ralph Fiennes) meanwhile marries, has a daughter, and divorces. Retrieving his books from the time of the affair with Hanna, he begins reading them into a tape recorder. He sends the cassette tapes and a recorder to Hanna. Eventually, she begins to check the books out from the prison library and teaches herself to read and write by following along with Michaels tapes. She starts writing back to Michael in brief, childlike notes, asking him to write to her. As time goes on, the letters reflect her gradually improving literacy.

Michael does not write back or visit but continues simply sending tapes, and in 1988 a prison official (Linda Bassett) telephones him to seek his help with Hannas transition into society after her upcoming early release due to good behavior. He finds a place for her to live and a job and finally visits Hanna a week before her release. In their meeting, Michael remains somewhat distant and confronts her about what she has learned from her past. Michael arrives at the prison on the date of Hannas release with flowers only to discover that Hanna hanged herself. She has left a tea tin with cash in it with a note asking Michael to give the cash and money in a bank account to Ilana. He discovers that she killed herself after reading Ilanas memoir of her horrifying experience in the concentration camp.

Michael travels to New York City where he meets Ilana (now Lena Olin) and confesses his relationship with Hanna. He tells her about the suicide note and Hannas illiteracy. Ilana tells Michael there is nothing to be learned from the camps and refuses the money. Michael suggests that she donate the money to an organization that combats adult illiteracy, preferably a Jewish one. She wants him to take care of this instead. Ilana keeps the tea tin since it is similar to one stolen from her in Auschwitz.

Michael drives Julia, his daughter, to Hannas grave and tells her their story.

==Cast==
* Kate Winslet as Hanna Schmitz
* Ralph Fiennes as the older Michael Berg
* David Kross as the younger Michael Berg
* Bruno Ganz as Professor Rohl, a Holocaust survivor
* Alexandra Maria Lara as Ilana Mather, a former victim of a concentration camp
* Lena Olin as Rose Mather (Ilanas mother) the older Ilana Mather
* Vijessna Ferkic as Sophie, Michaels friend at school
* Karoline Herfurth as Marthe, Michaels friend at university
* Burghart Klaußner as the judge at Hannas trial
* Linda Bassett as Mrs. Brenner, prison official
* Hannah Herzsprung as Julia, Michael Bergs daughter
* Susanne Lothar as Carla Berg
* Florian Bartholomäi as Thomas Berg
* Volker Bruch as Dieter Spenz, a student in the seminar group

==Production== Revolutionary Road led her to leave the film and Nicole Kidman was cast as her replacement.  In January 2008, Kidman left the project, citing her recent pregnancy as the primary reason. She had not filmed any scenes yet, so the studio was able to recast Winslet without affecting the production schedule.   

Filming took place in Berlin, Görlitz, on the Kirnitzschtal tramway near Bad Schandau, and was finished in Cologne on July 14.  Filmmakers received $718,752 from Germanys Federal Film Board.     Overall, the studio received $4.1 million from Germanys regional and federal subsidiaries.    
 that period in German history, and read books and articles about women who had served as SS guards in the camps. Hare, who rejected using a voiceover narration to render the long internal monologues in the novel, also changed the ending so that Michael starts to tell the story of Hanna and him to his daughter. "Its about literature as a powerful means of communication, and at other times as a substitute for communication", he explained.   
 Best Actress Golden Globe Award for Best Actress for Revolutionary Road.

Entertainment Weekly reported that to "age Hanna from cool seductress to imprisoned war criminal, Winslet endured seven and a half hours of makeup and prosthetic prep each day." 

Lisa Schwarzbaum of Entertainment Weekly writes that "Ralph Fiennes has perhaps the toughest job, playing the morose adult Michael&nbsp;– a version, we can assume, of the author. Fiennes masters the default demeanor of someone perpetually pained." 

The sex scenes were shot last after Kross had turned 18. 

==Release==
On December 10, 2008 The Reader had a limited release at 8 theaters and grossed $168,051 at the domestic box office in its opening weekend. The film had its wide release on January 30, 2009 and grossed $2,380,376 at the domestic box office. The films widest release was at 1,203 theaters on February 27, 2009, the weekend after the Oscar win for Kate Winslet.

In total, the film has grossed $34,194,407 at the domestic box office and $108,901,967 worldwide.   
The movie was released in the US on April 14 (DVD)  and April 28 (Blu-ray), 2009  and in the UK on May 25, 2009 (both versions).  In Germany two DVD versions (single disc and 2-disc special edition) and Blu-ray were released on September 4, 2009. 

==Critical reception==
The Reader received mixed to positive reviews and has a rating of 61% on Rotten tomatoes based on 193 reviews with an average score of 6.4 out of 10. The consensus states, "Despite Kate Winslets superb portrayal, The Reader suggests an emotionally distant, Oscar-baiting historical drama."  The film also has a score of 58 out of 100 on Metacritic based on 38 reviews. 

Ann Hornaday of The Washington Post wrote: This engrossing, graceful adaptation of Bernhard Schlinks semi-autobiographical novel has been adapted by screenwriter David Hare and director Stephen Daldry with equal parts simplicity and nuance, restraint and emotion. At the center of a skein of vexing ethical questions, Winslet delivers a tough, bravura performance as a woman whose past coincides with Germanys most cataclysmic and hauntingly unresolved era.  
 Germans who grappled with its legacy: its about making the audience feel good about a historical catastrophe that grows fainter with each new tasteful interpolation.  
 The Los Angeles Times, said "The pictures biggest problem is that it simply doesnt capture the chilling intensity of its source material," and noted that there was a "largely lackluster early reaction" to the film by most film critics. Most felt that while the novel portrayed Hannas illiteracy as a metaphor for generational illiteracy about the Holocaust, the film failed to convey those thematic overtones. 

Ron Rosenbaum was critical of the films fixation on Hannas illiteracy.  
so much is made of the deep, deep exculpatory shame of illiteracy&nbsp;– despite the fact that burning 300 people to death doesnt require reading skills&nbsp;– that some worshipful accounts of the novel (by those who buy into its ludicrous premise, perhaps because its been declared "classic" and "profound") actually seem to affirm that illiteracy is something more to be ashamed of than participating in mass murder... Lack of reading skills is more disgraceful than listening in bovine silence to the screams of 300 people as they are burned to death behind the locked doors of a church youre guarding to prevent them from escaping the flames. Which is what Hanna did, although, of course, its not shown in the film. 
 

Kirk Honeycutt in   Chris Menges and Roger Deakins lent the film a "fine professional polish".  Colm Andrew of the Manx Independent also rated it highly and said the film had "countless opportunities to become overly sentimental or dramatic and resists every one of them, resulting in a film which by its conclusion, has you not knowing which quality to praise the most". 

At the Huffington Post, Thelma Adams found the relationship between Hanna and Michael, which she termed child sexual abuse|abusive, more disturbing than any of the historical questions in the movie:
 
Michael is a victim of abuse, and his abuser just happened to have been a luscious retired Auschwitz guard. You can call their tryst and its consequences a metaphor of two generations of Germans passing guilt from one to the next, but that doesnt explain why filmmakers Daldry and Hare luxuriated in the sex scenes&nbsp;– and why its so tastefully done audiences wont see it for the child pornography it is. 
 
When asked to respond, Hare called it "the most ridiculous thing ... We went to great lengths to make sure that thats exactly what it didnt turn into. The book is much more erotic." Daldry added, "Hes a young man who falls in love with an older woman who is complicated, difficult and controlling. Thats the story."   

The film appeared on several critics top ten lists of the best films of 2008. Rex Reed of The New York Observer named it the 2nd best film of 2008. Stephen Farber of The Hollywood Reporter named it the 4th best film of 2008,  Tasha Robinson of The A.V. Club named it the 8th best film of 2008,  and Roger Ebert of the Chicago Sun-Times put it on his un-ranked top 20 list.     

Special praise went to Winslets acting; she then swept the main prizes in the 2008/2009 award season, including the Golden Globe, the Critics Choice Award, the Screen Actors Guild Award, the BAFTA, and the Academy Award for Best Actress. 
 played a often awarded Oscars.  However, in the fictional film, Winslet played a nun sheltering children from the Holocaust, rather than one of its perpetrators. Regarding the similarity, Winslet commented that the similarity "would be funny" but the connection didnt occur to her until "midway through shooting the film...this was never a Holocaust movie to me. Thats part of the story and provides something of a backdrop, and sets the scene. But to me it was always an extraordinarily unconventional love story." 

==Awards and nominations==
{|class="wikitable" style="font-size: 95%;" border="2" cellpadding="4" background: #f9f9f9;
|- bgcolor="#CCCCCC" align="center"
! colspan="4" style="background: LightSteelBlue;" | Awards
|- bgcolor="#CCCCCC" align="center"
! Award
! Category
! Name
! Outcome
|- Academy Awards Academy Award Best Actress Kate Winslet
| 
|- Academy Award Best Cinematography Roger Deakins and Chris Menges
|rowspan=4  
|- Academy Award Best Director Stephen Daldry
|- Academy Award Best Picture Sydney Pollack, Anthony Minghella, Redmond Morris, Donna Gigliotti
|- Academy Award Best Adapted Screenplay David Hare David Hare
|- BAFTA Awards BAFTA Award Best Actress Kate Winslet
| 
|- BAFTA Award Best Cinematography Roger Deakins and Chris Menges
|rowspan=4  
|- BAFTA Award Best Director Stephen Daldry
|- BAFTA Award Best Film
|- BAFTA Award Best Screenplay – Adapted David Hare
|- Broadcast Film Critics Association Top 10 Films of the Year
|
| 
|- Best Film
|
| 
|- Best Supporting Actress Kate Winslet
| 
|- Best Young Performer David Kross
| 
|-
|rowspan=4|Golden Globe Awards Golden Globe Best Director – Motion Picture Stephen Daldry
|rowspan=3  
|- Golden Globe Best Picture – Drama
|- Golden Globe Best Screenplay David Hare
|- Golden Globe Best Supporting Actress – Motion Picture Kate Winslet
| 
|- San Diego Film Critics Society San Diego Best Actress Kate Winslet
| 
|-
|rowspan=5|Satellite Awards Top 10 Films of 2008
|
| 
|- Satellite Award Best Actress – Motion Picture Drama Kate Winslet
|rowspan=4  
|- Satellite Award Best Director Stephen Daldry
|- Satellite Award Best Film – Drama
|- Satellite Award Best Adapted Screenplay David Hare
|-
|Screen Actors Guild Awards Screen Actors Outstanding Performance by a Female Actor in a Supporting Role Kate Winslet
| 
|}

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 