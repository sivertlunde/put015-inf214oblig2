The Eagle Has Landed (film)
{{Infobox film
| name           = The Eagle Has Landed
| image          = The Eagle Has Landed poster.JPG
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = John Sturges
| producer       = {{Plainlist|
* David Niven, Jr.
* Jack Wiener
}}
| screenplay     = Tom Mankiewicz
| based on       =  
| narrator       = Patrick Allen
| starring       = {{Plainlist|
* Michael Caine
* Donald Sutherland
* Robert Duvall
}}
| music          = Lalo Schifrin
| cinematography = Anthony B. Richmond
| editing        = Anne V. Coates
| studio         = ITC Entertainment
| distributor    = {{Plainlist| CIC  
* Columbia Pictures  
}}
| released       =  
| runtime        = {{Plainlist|
* 134 minutes  
* 123 minutes  
}}
| country        = United Kingdom
| language       = English
| budget         = $6,000,000 Lovell, Glenn. Escape Artist: The Life and Films of John Sturges. University of Wisconsin Press, 2008, pp. 284–288. 
| gross          = 
}}

The Eagle Has Landed is a 1976 British film directed by John Sturges and starring Michael Caine, Donald Sutherland and Robert Duvall.

Based on the novel The Eagle Has Landed by Jack Higgins, the film is about a German plot to kidnap Winston Churchill during the height of the Second World War. The Eagle Has Landed was Sturges final film, and received positive reviews and was successful upon its release.   

==Plot==
The film begins with captured Second World War film footage of the rescue from Italy of Mussolini by German paratroopers. rescue of Admiral Canaris (Anthony Quayle), head of the Abwehr (German military intelligence), is ordered to make a feasibility study of the seemingly impossible task of capturing the British Prime Minister Winston Churchill and bringing him to Germany.

Canaris considers the idea a joke, but realises that although Hitler will soon forget the matter, Himmler will not. Fearing Himmler may try to discredit him, Canaris orders one of his officers, Oberst Radl (Robert Duvall) to undertake the study, despite feeling that it is a waste of time.
 IRA lecturing at a Berlin university, to the mission.
 Eastern Front, Channel Island of Alderney, where they made high risk attacks with Neger Manned Torpedoes against English Channel convoys.

Radl travels to Alderney and, with the help of Devlin, recruits Steiner and his surviving men. The team will parachute into England from a captured C-47 Skytrain|C-47 with Allied markings. The commandos outfit themselves as Polish paratroopers, as few of them speak English. The plan is to infiltrate Studley Constable, complete their mission, rendezvous with an E-boat on the nearby coast and escape.
The paratroopers land and the operation proceeds.

Radl visits Himmler to announce, "The eagle has landed.", but then Himmler destroys the authorising document hed given Radl, and Radl then knows the plan must succeed or he is in serious trouble.

As the Germans take up positions in the town, the plan is foiled when a paratrooper rescues a local girl from certain death by a water wheel. He instead is killed in the process and his German uniform (worn under the Polish uniforms at the prior insistence of Steiner) is revealed to the onlooking villagers. The locals are rounded up into the village church, but Pamela Vereker (Judy Geeson), the sister of the village priest Father Vereker (John Standing), escapes to alert a unit of the United States Army Rangers stationed nearby. Inexperienced, glory-seeking Colonel Pitts (Larry Hagman) tries to foil the German plan almost single-handed but is killed by treasonous Nazi sympathizer Joanna Grey in her house, while his poorly-planned assault on the church fails with heavy casualties. Pitts young deputy, Captain Clark (Treat Williams) then organises a second, successful attack.

Steiners men sacrifice themselves to delay the Americans while Devlin, Steiner and his wounded second-in-command Neustadt (Sven-Bertil Taube) escape, with the aid of local girl Molly Prior (Jenny Agutter) who was romantically involved with Devlin. Instead of boarding the escape boat, Steiner vows to make one last attempt at Churchill.
 summarily executed by firing squad, under the pretext that he "exceeded his orders to the point of treason". In this way Himmler distances himself from the failed mission.

Steiner does manage to infiltrate the country house and apparently succeeds in killing Churchill before being shot and killed himself. Captain Clark, on scene, is stunned by Churchills death, but is then informed by security personnel that "Churchill" was actually George Fowler, an impersonator – the real Prime Minister is at the Tehran Conference. Clark and a superior officer are also advised that, "No one will ever know about this. This never happened. It did not occur."

Devlin, evading capture, leaves a love letter for Molly, and disappears to live in obscurity.

==Cast==
 
* Michael Caine as Oberst Kurt Steiner
* Donald Sutherland as Liam Devlin
* Robert Duvall as Oberst Max Radl
* Jenny Agutter as Molly Prior
* Donald Pleasence as Heinrich Himmler
* Anthony Quayle as Adm. Wilhelm Canaris
* Jean Marsh as Joanna Grey
* Sven-Bertil Taube as Hauptmann (Hans) Ritter von Neustadt
* Siegfried Rauch as Sgt. Brandt
* John Standing as Father Philip Verecker
* Judy Geeson as Pamela Verecker
* Treat Williams as Capt. Harry Clark
* Larry Hagman as Col. Clarence E. Pitts Michael Byrne as Karl Joachim Hansen as SS Gruppenführer
* Terence Plummer as Arthur Seymour
* Tim Barlow as George Wilde (publican)
* Kate Binchy as Mrs. Wilde John Barrett as Laker Armsby
* Maurice Roëves as Maj. Corcoran
* Jeff Conaway as Frazier
* Richard Wren as Sgt. Altmann
* Alexei Jawdokimov as Cpl. Kuniski
* Leonie Thelen as Branna
* David Gilliam as Moss
* Leigh Dilley as Winston Churchill / George Fowler   Kent Williams as Mallory  
* Roy Marsden as Sturmführer Toberg  
* Malcolm Tierney as Hauptsturmfuhrer Fleischer
* Wolf Kahler as SS Officer  
* Ferdy Mayne as Radls doctor  
* Harry Fielder as motorcycle outrider  
* Denis Lill as Churchills aide Keith Buckley as pilot George Leech as SS Officer at railway station  
 

==Production==
===Casting and production===
Caine was originally offered the part of Devlin but did not want to play an IRA man so he asked if he could have the role of Steiner. Richard Harris stepped into the part of Devlin but then it was felt he was too associated with financing the real IRA so Donald Sutherland was given the role.  Tom Mankiewicz thought the script was the best he had ever written "but John Sturges, for some reason, had given up" and did a poor job. He said editor Anne V. Coates was the one who saved the movie and made it watchable. Tom Mankiewicz and Robert Crane, My Life as a Mankiewicz, University Press of Kentucky 2012 p 179 

Michael Caine had initially been excited at the prospect of working with Sturges. During shooting, Sturges confessed to Caine that he was only doing the film to earn enough money to go fishing. Caine wrote later in his autobiography: "The moment the picture finished he took the money and went.   Jack Wiener later told me   never came back for the editing nor for any of the other good post-production sessions that are where a director does some of his most important work." {{cite web
 | last =Nixon
 | first =Rob
 | authorlink =
 | title =The Eagle Has Landed
 | work =
 | publisher =Turner Classic Movies
 | date =
 | url =http://www.tcm.com/this-month/article/194391%7c0/The-Eagle-Has-Landed.html
 | format =
 | doi =
 | accessdate = 5 August 2014
 | archiveurl = 
 | archivedate = }} 

===Filming locations===
Cornwall was used to represent the Channel Islands, and Berkshire for East Anglia.    The majority of the film, set in the fictional village of Studley Constable, was filmed at Mapledurham on the A329 in Oxfordshire and features the village church, Mapledurham Watermill and Mapledurham House, which represented the Manor House where Winston Churchill was taken.  A fake waterwheel was added to the 15th century structure for the film.  Mock buildings such as shops and a pub were constructed on site in Mapledurham while interiors were filmed at Twickenham Studios. The "Landsvoort Airfield" scenes were filmed at RAF St Mawgan, five miles from Newquay. 
 Rock in Dunkeswell Airfield Holkham Beach in Norfolk.

In the movie there is a scene where Radl and Karl look at an Ordnance Survey map with a magnifying glass, at an area just south east of present day Wells Next The Sea. This seems to suggest that the filmmakers wanted Studley Constable to be located between Warham, Wighton and Binham. This is only a few miles from Holkham where the parachute drop was actually filmed. Reading of the book suggests the fictional village may have actually been based on nearby Cley Next The Sea.

==Release==
The original running time for the film was 134 minutes in Europe, while 11 minutes were cut by the producers before the film was released in the United States. The US 131 minutes NTSC DVD version is close to the original European cinema version, and most European PAL DVD versions also have that length (130 minutes).
 Carlton Visual Entertainment released a two-disc PAL version which contains two versions of the film: the regular DVD version as well as an extended 145 minute version.    This 15 minutes longer version contains a number of scenes that had been deleted even before the European cinema release:  Schloss Hohenschwangau for a conference with Hitler, Canaris, Bormann and Goebbels. This deleted opening would have preceded the scenes under the opening credits which are a long aerial shot of a staff car leaving the castle in question. The deleted scene explains why Schloss Hohenschwangau appears in the credits but does not appear in the film.
* Extended scene when Radl arrives at Abwehr headquarters, he discusses his health with a German Army doctor (played by Ferdy Mayne).
* Scene at a Berlin University where Liam Devlin is a lecturer.
* Scene in Landsvoort where Steiner and von Neustadt discuss the mission and its merits and consequences.
* Devlins arrival at Studley Constable is now extended where he and Joanna Grey discuss their part in the mission.
* Devlin drives his motorbike through the centre of the village and on to the cottage, where he inspects the barn before returning to the village.
* Scene where Devlin reads poetry to Molly Prior.
* Extended scene in which Molly interrupts Devlin shortly after he receives the army vehicles.
* Scene on the boat at the end that shows the fate of von Neustadt. (This scene is visible in one of the special edition DVDs extras, in the action photo gallery)

==Reception==
The film was a success and ITC made two more films with the same production team, Escape to Athena and Green Ice. Lew Grade, Still Dancing: My Story, William Collins & Sons 1987 p 250 
===Critical response===
In his review for The New York Times, Vincent Canby called the film "a good old-fashioned adventure movie that is so stuffed with robust incidents and characters that you can relax and enjoy it without worrying whether it actually happened or even whether its plausible."  Canby singled out the writing and directing for praise:
 

==See also==
* Cultural depictions of Winston Churchill
* Went the Day Well? (1942) A film with a similar premise.

==References==
 

==External links==
 
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 