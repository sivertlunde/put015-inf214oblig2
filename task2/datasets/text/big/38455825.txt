International Squadron (film)
{{Infobox film
| name           = International Squadron
| image          = Intsqpos.jpg
| caption        = Original film poster
| director       = Lewis Seiler Lothar Mendes
| producer       = Hal Wallis Edmund Grainger
| writer         = Kenneth Gamet   Barry Trivers
| based on       = Ceiling Zero by Frank Wead
| starring       = Ronald Reagan Olympe Bradna James Stephenson 
| music          = William Lava Ted McCord James Van Trees
| editing        = Frank Magee
| special effects = Robert Burks
| studio         = Warner Bros.
| distributor    = Warner Bros
| released       = 13 August 1941 
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
}}
International Squadron is a 1941 American war film directed by Lewis Seiler and Lothar Mendes that starred Ronald Reagan, Olympe Bradna and in his final film, James Stephenson. The film is based on the Eagle Squadrons, American pilots who volunteered to fly for the Royal Air Force during World War II.

==Cast==
Ronald Reagan  ...  Jimmy Grant   
Olympe Bradna  ...  Jeanette   
James Stephenson  ...  Squadron Leader Charles Wyatt  
William Lundigan  ...  Lt. Rog Wilkins   
Joan Perry  ...  Connie Wilkins    Reginald Denny  ...  Wing Commander Severn   
Cliff Edwards  ...  Omaha McGrath    Julie Bishop  ...  Mary Wyatt   
Tod Andrews  ...  Michele Edmé  
John Ridgely  ...  Bill Torrence    Charles Irwin  ...  Biddle   
Addison Richards  ...  Chief Engineer   
Selmer Jackson  ...  Saunders   
Holmes Herbert  ...  Sir Basil Wryxton  
Joan Perry	... Connie Wilkins

==Production== film of the same name that appeared in early 1942. Producer Darryl F. Zanuck of 20th Century Fox wrote angry letters to Hal Wallis of Warner Bros. accusing them of not only stealing his idea of his A Yank in the R.A.F. but making a low budget B picture to beat Foxs prestigious production to the screen. Zanuck threatened legal action unless Warners stopped the film from being made or not to release their film until 60 days after Zanucks film was released. Warners ignored Zanuck as the Eagle Squadrons were a major news item of the day and Warners based their screenplay on a film made by the studio Ceiling Zero based on a play by Frank Wead.  They did change the films original title from Eagle Squadron to Flight Patrol then finally International Squadron and released it two months before A Yank in the R.A.F. 

Warners acquired another boost by hiring Byron Kennerly (09/30/1908-02/01/1962), a former member of an Eagle Squadron as the films technical advisor. Kennerly had written a magazine story Squadron 71, Scramble! A Day in the Eagle Squadron, R.A.F. that was published in the July 1941 issue of Harpers Magazine and was in the process of writing a book The Eagles Roar! that Warners bought the rights to. However according to some sources Kennerly left the Eagle Squadron before their first engagement;  with some sources saying he was on furlough for an ear ailment. Kennerly later served with the United States Army Air Forces and was jailed for bank robbery in February 1951. 

Actual film of dogfighting between Spitfires and Messerschmitts and Heinkels and a London air raid were shot by Warners Teddington studios technicians and shipped to the United States for inclusion in the film.  Paul Mantz acted as the films stunt pilot.

==References==
 

==Bibliography==
* Glancy, H. Mark. When Hollywood Loved Britain: The Hollywood British Film 1939-1945. Manchester University Press, 1999.

==See also==
* A Yank in the R.A.F. Eagle Squadron
* Higher Than a Kite

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 