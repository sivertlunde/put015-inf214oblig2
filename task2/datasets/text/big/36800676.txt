Billy Frankenstein
{{Infobox film
| name           = Billy Frankenstein
| image          = DVD_release_cover_for_Billy_Frankenstein.jpg
| image_size     = 
| alt            = DVD release cover
| caption        = DVD release cover
| director       = Fred Olen Ray
| producer       = Ashok Amritraj Andrew Stevens
| writer         = Kim Ray
| starring       = Jordan Lamoureux Mary Elizabeth McGlynn Daran Norris Peter Spellos Brian Carrillo
| music          = 
| cinematography = 
| editing        = Randy Carter
| studio         = Royal Oaks Entertainment
| released       = 1998
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         =
}}

Billy Frankenstein is a 1998 horror-comedy movie directed by Fred Olen Ray and written by his wife Kim Ray.

==Plot== Melissa Williamson) the Frankenstein Vernon Wells), and his lady Fraulein (Griffin Drew), to scare the Franks into selling him the castle and make way for a Shopping mall|mall.

Billy, his mother Sandy and Billys aunt Thelma (Kristin Jadrnicek) arrive at the castle and meet Bloodstone, who introduces himself as the caretaker of the house. Billy discovers one of Dr. Frankensteins books about life and death and finds a map of the castle inside the book which leads him into the basement where he discovers the inanimate Frankenstein monster. When Billy falls asleep in the basement, the monster comes to life. Rather than being scary and threatening, the monster turns out to be friendly and confused. Billy wakes up later to discover the monster had gotten loose and goes out to find him, as does Bloodstone, who hires Sloanes lawyer (who announced the house repossession) to find Billy and the monster. Frogg disguises himself as a cable guy and sets up microphones around the house. Billy eventually comes across the monster, befriends it and brings the monster, poorly disguised as his grandmother, through a nearby village. Soon though, the monster begins to run out of power and so Billy and Bloodstone take him back to the lab basement to recharge him.

Billys father, George, finally arrives home much to Billys excitement. George announces that he got fired from his job, but isnt worried because of his family importance and inheritance to the castle, but Billy and Sandy tell him that the lawyer had told them earlier in the film that they need to raise $25,000 to buy the castle, so George comes up with the idea of inviting Sloane to dinner to discuss the payment. Billy and Bloodstone try bring the monster back to life, but leave when they think theyve failed, but the monster shows signs of life when they leave. Billy and Bloodstone try to hide the monster from prying eyes, but Thelma faints after seeing him. Sloane and Fraulein visit Frogg to find he has disguised himself as the monster, which Sloane takes an interest in. While out shopping, George and Sandy buy a Frankenstein monster doll to give to Billy as a present and arrive home to give Billy the doll and have dinner with Sloane and Fraulein. Finally, Sandy sees the monster for herself, who Sloane mistakes for Frogg. When Sloane is convinced of the monsters existence, he and Fraulein both flee.

The film ends with the Franks, Thelma, Bloodstone and Frogg, raising the $25,000 by having tourist attractions in their castle, complete with photos with the Frankenstein monster and tours down to the lab, where a little girl pulls the lever and electrocutes herself, making her resemble the Bride of Frankenstein. When the Franks end up wondering what happened to Sloanes lawyer, we see the lawyer, still looking for Billy and the monster, asking Dracula where they are, and Dracula points into the direction of Transylvania, which the lawyer follows.

==Cast==
* Jordan Lamoureux as Billy Frankenstein
* Mary Elizabeth McGlynn as Sandy Frankenstein (as Melissa Williamson)
* Daran Norris as George Frankenstein (as Daran W. Norris) and Victor Frankenstein|Dr. Frankenstein (photographic cameo)
* Peter Spellos as Bloodstone
* Brian Carrillo as the Frankenstein monster
* Kristin Jadrnicek as Thelma Vernon Wells as Otto von Sloane
* Griffin Drew as Fraulein
* John Maynard as Constable Frogg

==Reception==
The film has been received poorly by most critics and holds a 2.8 rating on IMBD.  Theres no approval rating on Rotten Tomatoes, but the Want-To-See score is currently 0%.

==External links==
 
*  
*  

== References ==
 

 
 
 
 
 
 
 

 