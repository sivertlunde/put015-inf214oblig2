Pyaar Ka Punchnama
 
 
{{Infobox film
| name           = Pyaar Ka Punchnama
| image          = Pyar Ka Punchnama.jpg
| caption        =
| director       = Luv Ranjan
| producer       = Abhishek Pathak
| screenplay     = Deepak, Vaibhav Suman, Sumit Saxena
| starring       = Kartik Tiwari Raayo S Bakhirta Divyendu Sharma Sonalli Sehgall Nushrat Bharucha Ishita Sharma
| music          = Clinton Cerejo Hitesh Sonik Luv Ranjan AD Boyz
| cinematography = Sudhir K Choudhary
| editing        = Akiv Ali
| released       =  
| runtime        = 149 minutes
| country        = India
| language       = Hindi
| budget         =  
| gross          =   (domestic nett. gross)
}}
 Hindi film starring Kartik Tiwari, Raayo S Bakhirta and Divyendu Sharma. Directed by Luv Ranjan, the film is the story of three bachelors who find girls whom they fall in love with and the twists and turns of the newly developing love stories. The film completed 50 days at the box office. It grossed Rs 17 crores in box office, satellite rights and DVDs against a budget of Rs 7 crores.    

==Plot==
Rajat (Kartik Tiwari), Nishant "Liquid" Agarwal (Divyendu Sharma) and Vikrant Chaudhary (Raayo S Bakhirta) are bachelors who live together in a flat in Noida. Rajat falls in love with Neha (Nushrat Bharucha). Nishant falls for Charu (Ishita Sharma) who is his colleague. She makes him do a good part of her own work in office and also makes him foot her beauty parlour bills but he is dumb not to understand that she is only using him for money. Charu has a boyfriend, Abhi, but they aren’t on the best of terms. Vikrant loves Riya (Sonalli Sehgall) who also can’t get over her boyfriend of 5 years, Varun. Rajat leaves the bachelor pad. Vikrant, however is aware of Varun, and also knows that Riya has not yet called off that relationship but he doesn’t mind waiting because he is besotted by Riya. On her part, Riya keeps assuring him that she will end the relationship but actually ends up sleeping with Varun while she is dating Vikrant. Missing their meetings and bar-hopping, the trio decide to take a time-out by themselves. All three women, however, find out and decide to accompany them to the beach where they eventually mingle. Charu here kisses Nishant, while Rajat has a fight with Neha back at home. Charu starts to ignore Nishant and ends up eventually insulting him openly at work. Rajat and Neha eventually work out their differences, but soon after, further problems arise between them (such as Nehas constant tantrums). Rajat becomes so frustrated that he walks out on Neha, telling her that she is not worthy of him. Nishant goes into depression but is brought back to reality by his two friends Vikrant and Rajat who drive him to Charus house and Nishant slaps Charus boyfriend. Vikrant finds out that Riya slept with her ex and leaves her.

In the end the trio are seen sitting together, laughing and feeling happy with the moment they have with each other. Later on, the three girls whom the trio fell for are shown having new boyfriends.

==Cast==
* Kartik Tiwari as Rajat aka Rajjo 
* Divyendu Sharma as Nishant Agarwal aka Liquid
* Raayo S Bakhirta as Vikrant Choudhary aka Choudhary
* Nushrat Bharucha as Neha
* Sonalli Sehgall as Rhea
* Ishita Sharma as Charu
* Padam Bhola as Varun
* Ravjeet Singh as Abhi (Charus boyfriend)

==Critical reception==
Taran Adarsh of Bollywood Hungama gave the movie 3.5 stars out of 5, saying that "On the whole, Pyaar Ka Punchnama has a single-point plan of engaging and amusing the spectators by telling a story which is unusual, yet relevant. Its radiantly good cinema that needs to be lauded and encouraged. Strongly recommended, go for it!"  Subhash K. Jha of Now Running gave the movie 3.5 stars in a scale of 5, commenting that "Pyaar Ka Punchnama is a delightful rugged romantic-comedy."  Nikhat Kazmi of Times of India gave the movie 3 stars out of 5, noting that "The film had promise and does have a few funny sequences. But by and large, its a case of promises unfulfilled."  Blessy Chettiar of DNA India gave the movie 3 stars in a scale of 5, concluding that "The only problem with PKP is its running time. The second half seems never-ending, sappy and pleading for sympathy. The MCPs probably don’t realise that women don’t think much of them and PKP won’t do much to change that."  Swati Bhattacharyya of Daily Bhaskar gave the movie 2.5 stars out of 5, saying that "It’s a youth centric rom-com, watch it to see the contemporary take on love with twists and turns. Whether your relationship is a danger warning, if your brain can’t tell you at this moment, Pyaar Ka Punchnama will definitely give you an answer. It’s entertaining and fun filled. Don’t go if you are expecting a conventional romantic movie as it may qualify to be blasphemous and a sex comedy."  Anupama Chopra of NDTV Movies gave the movie 2 stars in a scale of 5, commenting that "Pyaar Ka Punchnama, starts out with some promise, echoing Dil Chahta Hai with a laid-back, buddy vibe but that evaporates as soon as the women appear."  Mansha Rastogi of Now Running gave the movie 2 stars out of 5, noting that "On the whole Pyaar Ka Punchnama works only in parts. Would manage to entice only those people who have vendetta against women."  Shaikh Ayaz of Rediff gave the movie 2 stars in a scale of 5, concluding that "Pyaar Ka Punchnama is half-way funny but soon descends into a running commentary on why relationships with women are impossible and eventually wends its way to a very implausible end. It could have definitely done with some editing. If it were crisper, with more thought put into its script, situations and dialogue, Pyaar Ka Punchnama would have made the cut."  Mayank Shekhar of Hindustan Times gave the movie 2 stars out of 5, saying that "The movie acquires rhythms of a TV show, like so many do. Were already in the fifth season by the end of it. Though if this were a TV show, itd be closest to an authentic Indian version of Friends, or How I Met Your Mother."  Rajeev Masand of CNN-IBN gave the movie 1.5 stars in a scale of 5, commenting that "Disguised as a light-hearted comedy, Pyaar Ka Punchnama is such a misogynistic film that you cant help but wonder if it was made by someone whos had his heart brutally stamped on by a woman. Its about as much fun as walking on broken glass."  Sonia Chopra of Sify gave the movie 1.5 stars in a scale of 5, noting that "The film claims that men are victims; but actually there’s only victim here – the viewer."  Komal Nahta of Koimoi gave the movie 1 stars out of 5, concluding that "On the whole, Pyaar Ka Punchnama may be a moderate-budgeted film but its box-office prospects would be limited to a few cinemas in the cities because it is radically different from what the average cinegoer is used to watching." 



==Box office==
According to Box Office India, Pyaar Ka Punchnama started nimbly, then became strong at the box office by word of mouth for the excellent story and acting.  The film grossed around 52.5&nbsp;million nett in week one and 8th day was steady.  Pyaar Ka Punchnama held up well in week two with collections of around 35.0&nbsp;million nett. The two-week total is around 90&nbsp;million nett.  According to Box Office India, Pyaar Ka Punchnama fell just 60% in its third week which was when Ready was released. The film grossed around 15.0&nbsp;million nett in its third week to take its total to  . 
Initially Box Office India termed it Below Average by second week, but later changed its verdict to hit as the film completed 50 days at Indian boxoffice.  Pyaar Ka Punchnama had remained rock steady in CI for 2 weeks.  Pyaar Ka Punchnama was steady in several centres in second week. 

== Awards ==
* Pyaar Ka Punchnama was screened at the Indian Film Festival in Berlin and was widely appreciated by the audience. 

* The film also won the Most successful small budget film] award at the ETC Bollywood Business awards 2010.  IIT Kanpur, Antaragini 2012. 

==Soundtrack==
* "Ban Gaya Kutta" – 3:50
* "Life Sahi Hai" – 5:05
* "Koi Aa Raha Paas Hai" – 4:42
* "Ishq Na Kariyo Kakke" – 5:27
* "Chak Glassi" – 4:11
* "Chak Glassi (Ad Boyz)" – 4:09
* "Baanware" – 6:31
* "Koi Aa Raha Paas Hai (Revisited)" – 4:47
* " Life Sahi Hai" – 5:60

There will be a sequel.  

==References==
 

==External links==
*  
*   on Facebook

 
 
 
 
 
 