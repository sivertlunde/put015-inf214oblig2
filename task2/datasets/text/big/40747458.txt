The Boys of Ghost Town
{{Infobox film
| name           = The Boys of Ghost Town
| image          = The boys of ghost town.jpg
| image_size     = 
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Pablo Véliz 
| producer       = Pablo Véliz 
| writer         = 
| screenplay     = Rosalio Martinez
| story          = Manuel Garcia
| based on       =  
| narrator       = 
| starring       = Danny Trejo Manuel Garcia
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Boys of Ghost Town is a 2009 American drama film directed and produced by Pablo Véliz and written by Rosalio Martinez and Manuel Garcia. It stars Danny Trejo, Marian Zapico, and Garcia. The film follows young criminal Danny Ortego rejoining the forces of crime after being released from prison.

==Plot==
Delinquent Danny Ortego (Manuel Garcia) is released from prison after a seventeen-year-long term. He returns to Houston, Texas, and is shocked to learn that drug barons and other bosses of crime have taken over his beloved hometown. Although he initially wanted to turn over a new leaf, Ortego finally decides that only by ascending the underworld ladder can he change the drastic situation in Houston. With the help of his big-sized acquaintance Corando (Corando Martinez, Jr.), he gets to know drug lord "Big Joe" (Ricardo G. Lerma) and starts peddling drugs. Ortego also starts to indulge in clubbing and it is at a club that he meets and falls in love with Carmen (Nova Aragon), whom he later weds.

Ortego tries to quit his life of crime after some self-reflection. Big Joe accommodates his request to resign and Ortego agrees to pull off one last job, this time with major drug kingpin Xavier (Danny Trejo). Big Joe stops to retrieve the cash first during the journey and gets shot at by an unknown assailant. He dies, and Ortego meets Xavier alone with the $1.5 million. Xavier tells him to hand the money to one of his henchmen. The henchman attempts to double-cross Xavier and kill Ortego. Ortego takes out the crooks sent to kill him and proceeds to find the henchman, armed with only a knife.

Sapped of energy and caked with blood, Ortego finds him takes one last stab at the thug, before the latter man collapses to the ground, dead. Xavier is impressed with Ortego and offers him a place in his cartel, to which Ortego replies, "Were just getting started."

==Production==
Danny Trejo was cast as alongside Manuel Garcia in the film.  Garcia played the films protagonist, Danny Ortego.    Pedro Castaneda made a cameo appearance as Abuelo Ignacio. 

The Boys of Ghost Town was directed and produced by filmmaker Pablo Véliz.    Rosalio Martinez and Manuel Garcia wrote the screenplay and the story respectively.  Filming locations included San Antonio. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 