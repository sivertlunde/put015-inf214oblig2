Low Down
{{Infobox film
| name           = Low Down
| image          = Low down poster.jpg
| caption        = Movie poster
| director       = Jeff Preiss Albert Berger
| writer         =  Topper Lilien Amy-Jo Albany John Hawkes Flea Taryn Manning Peter Dinklage Tim Daly
| music          = Ohad Talmor
| cinematography = Christopher Blauvelt
| editing        = Michael Saia
| studio         = Bona Fide Productions
| distributor    = Oscilloscope Laboratories
| released       =  
| runtime        = 120 mins
| country        = United States
| language       = English
| budget         = 
| gross          = $54,051 
}} John Hawkes), and his struggles with drug addiction. 
 Flea (who also co-stars in the film) of the Red Hot Chili Peppers.

The film premiered in competition at 2014 Sundance Film Festival on January 19, 2014.  It won the Cinematography Award: U.S. Dramatic at the festival. 

==Plot summary==
Joe Albany was a well accomplished jazz pianist during the 1960s and 1970s performing with the likes of Charlie Parker, Miles Davis, and Charles Mingus before his descent into heroin addiction. The film tells the story of Albanys life from the perspective of his then 11 year old daughter Amy Albany through her own personal memoirs, as she watches him contend with his drug addiction during the 1960s and 70s jazz scene. 

==Cast== John Hawkes as Joe Albany
* Elle Fanning as Amy-Jo Albany
* Lena Headey as Sheila Albany
* Glenn Close as Gram Flea as Hobbs
* Taryn Manning as Colleen
* Peter Dinklage as Alain
* Burn Gorman as Wiggenhern
* Tim Daly as Dalton Linda Wang as Chinese woman
* Myles Cranford as Jimmy

==Production==
Actor Mark Ruffalo was originally cast in 2011 to star as Joe Albany however was forced to back out due to scheduling issues and production being delayed. 

==Release==
The film made its debut at the Sundance Film Festival on January 19, 2014. The first official trailer was released on September 18, 2014. The film opens in limited release on October 24, 2014 in New York and October 31, 2014 in Los Angeles. 

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 