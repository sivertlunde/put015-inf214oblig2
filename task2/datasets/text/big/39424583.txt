Davy Crockett, Indian Scout
{{Infobox film
| name           = Davy Crockett, Indian Scout
| image size     = 
| image	=	
| caption        = 
| director       = Lew Landers
| producer       = Edward Small
| writer         = Richard Schayer
| based on       = story by Ford Beebe George Montgomery
| music          = 
| cinematography = 
| editing        = 
| studio = Edward Small Productions
| distributor    = United Artists
| studio         = 
| released       = 1950
| runtime        = 
| country        = USA
| language       = English
| budget         = 
}}
Davy Crockett, Indian Scout is a 1950 Western film starring George Montgomery as  Davy Crocketts nephew. 

It was shot at the Motion Picture Centre, with filming commencing June 1948. SMALL PLANS FILM ON MONTE CRISTO: Seeks Louis Hayward for Lead in Movie on Dumas Hero -- Beloin Doing Hope Script 
By THOMAS F. BRADY Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   15 June 1948: 33. 
 Kit Carson, Jon Hall, Dana Andrews, and Clayton Moore. 

Wartime hero Johnny McKee had a small role Bankhead Movie Deals Reported on Fire Here
Schallert, Edwin. Los Angeles Times (1923-Current File)   24 June 1948: 23.   as did Jim Thorpe. Noted Pair Costarred
Los Angeles Times (1923-Current File)   16 Mar 1949: A8. 

==Reception==
The New York Times film critic wrote the movie "should warm the cockles of the hearts of 10 year olds". THE SCREEN IN REVIEW: Woman of Distinction, Starring Rosalind Russell, Is New Bill of Radio City Music Hall At the Palace
By BOSLEY CROWTHER. New York Times (1923-Current file)   17 Mar 1950: 34.  

==References==
 

==External links==
*  at IMDB
*  at TCMDB
 
 
 
 
 
 

 