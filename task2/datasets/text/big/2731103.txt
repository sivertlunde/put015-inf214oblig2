Pat and Mike
{{Infobox film
| name           = Pat and Mike 
| image          = Patandmike.jpg
| caption        = Theatrical-release poster
| alt            =
| director       = George Cukor
| producer       = Lawrence Weingarten
| screenplay     = Ruth Gordon Garson Kanin Sammy White Charles Bronson
| music          = David Raksin
| cinematography = William H. Daniels (as William Daniels)
| editing        = George Boemler	
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         =$1,618,000  . 
| gross          = $2,696,000 
}}
 The Philadelphia Story (1940) with Hepburn, and Adams Rib (1949) with Hepburn and Tracy.

== Plot ==
Pat Pemberton (Katharine Hepburn) is a brilliant athlete who loses her confidence whenever her charming but undermining fiancé Collier (William Ching) is around. Ladies golf and tennis championships are within her reach, however she gets flustered by his presence at the contests. He wants her to give up her goal and marry him, but Pat does not give up on herself that easily. She enlists the help of Mike Conovan (Spencer Tracy), a slightly shady sports promoter. Together they face mobsters, a jealous boxer (Aldo Ray), and a growing mutual attraction.

==Cast==
* Spencer Tracy as Mike Conovan
* Katharine Hepburn as Patricia Pat Pemberton
* Aldo Ray as Davie Hucko
* William Ching as Collier Weld Sammy White as Barney Grau George Mathews as Sylvester Spec Cauley
* Gussie Moran as Herself (Sports Star)
* Babe Didrikson Zaharias as Herself (Sports Star)
* Don Budge as Himself (Sports Star)
* Alice Marble as Herself (Sports Star)
* Frank Andrew Parker as Himself (Sports Star) (as Frank Parker)
* Betty Hicks as Herself (Sports Star)
* Beverly Hanson as Herself (Sports Star)
* Helen Dettweiler as Herself (Sports Star)
* Loring Smith as Mr. E.H. Beminger

==Production and filming==
Garson Kanin and Ruth Gordon were friends with Hepburn and Tracy, and had the idea of writing a film to showcase Hepburns athletic abilities. She was an avid golfer and tennis player, and indeed performed all the sports footage in the film herself.
 cameo roles or play themselves in the film, including golfers Babe Didrikson Zaharias, Betty Hicks, and Helen Dettweiler, and tennis champions Don Budge, Gussie Moran and Alice Marble. Other notables in the cast include Charles Bronson (credited as Charles Buchinsky) in his first credited movie role, Carl "Alfalfa" Switzer, Jim Backus, and, in his film debut, Chuck Connors of The Rifleman television series.

==Music==
The score for the film was composed and conducted by David Raksin, with orchestrations by Robert Franklyn and Ruby Raksin. {{cite journal
| title      = David Raksin at MGM (1950-1957)
| others    = David Raksin
| date      = 2009
| url       = http://www.filmscoremonthly.com/notes/pat_and_mike.html
| last      = Bettencourt
| first     = Scott
| pages     =| type    = CD online notes
| journal = Film Score Monthly
| volume=12 |issue=2
| location  = Los Angeles, California, U.S.A.
| language  = 
}}   Of his music, Raksin said "My music was sly and a mite jazzy, and despite the fact that everyone seemed to like it, so did I." {{cite journal
| title      = David Raksin at MGM (1950-1957)
| others    = David Raksin
| date      = 2009
| url       = 
| last      = Bradford
| first     = Marilee
| pages     =18
| type    = CD liner notes
| journal = Film Score Monthly
| volume=12 |issue=2
| location  = Los Angeles, California
| language  = 
}} 

The complete score was issued on CD in 2009, on Film Score Monthly records.

==Reception==
According to MGM records the film earned $2,050,000 in the US and Canada and $646,000 elsewhere, resulting in a profit of $74,000. 

==See also==
 
* 1952 in film
* List of American films of 1952
 

== References ==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 