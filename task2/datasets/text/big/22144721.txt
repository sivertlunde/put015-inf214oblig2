Iron Man 2
 
 
{{Infobox film
| name = Iron Man 2
| image = Iron Man 2 poster.jpg
| alt = Tony Stark is pictured center wearing a smart suit, against a black background, behind him are is the Iron Man red and gold armor, and the Iron Man silver armor. His friends, Rhodes, Pepper, are beside him and below against a fireball appears Ivan Vanko armed with his energy whip weapons.
| caption = Theatrical release poster
| director = Jon Favreau
| producer = Kevin Feige
| screenplay = Justin Theroux
| based on =  
| starring = {{Plain list|
* Robert Downey Jr.
* Gwyneth Paltrow
* Don Cheadle
* Scarlett Johansson
* Sam Rockwell
* Mickey Rourke
* Samuel L. Jackson
 
}}
| music = John Debney  
| cinematography = Matthew Libatique
| editing = {{Plainlist|
* Dan Lebental Richard Pearson
}}
| studio = {{Plainlist|
* Marvel Studios
* Fairview Entertainment
}}
| distributor = Paramount Pictures 
| released =  
| runtime = 125 minutes 
| country = United States
| language = English
| budget =  $200 million   
| gross = $623.9 million 
}} Iron Man, Ivan Vanko has developed the same technology and built weapons of his own in order to pursue a vendetta against the Stark family, in the process joining forces with Starks business rival, Justin Hammer.
 James Rhodes. In the early months of 2009, Rourke, Rockwell and Johansson filled out the supporting cast, and the film went into production that summer. Like its predecessor the film was shot mostly in California, except for a key sequence in Monaco.

Iron Man 2 premiered at the El Capitan Theatre on April 26, 2010, and was released internationally between April 28 and May 7 before releasing in the U.S. on May 7, 2010. The film was a critical and commercial success, grossing over $623.9 million at the worldwide box office. The DVD and Blu-ray were released on September 28, 2010. The third installment of the Iron Man series, Iron Man 3, was released on May 3, 2013.
 

==Plot== Tony Starks Ivan Vanko, Anton Vanko has just died, sees this and begins building a miniature arc reactor similar to Starks. Six months later, Stark is a superstar and uses his Iron Man suit for peaceful means, resisting government pressure to sell his designs. He re-institutes the Stark Expo in Flushing Meadows to continue his father Howard Stark|Howards legacy.
 James Rhodes dons Starks Mark II prototype armor and tries to restrain him. The fight ends in a stalemate, so Rhodes confiscates the Mark II for the US Air Force.
 Natasha Romanoff and that Howard Stark was a S.H.I.E.L.D. founder whom Fury knew personally. Fury explains that Vankos father jointly invented the arc reactor with Stark, but when Anton tried to sell it for profit, Stark had him deported. The Soviets sent Anton to the gulag. Fury gives Stark some of his fathers old material; a hidden message in the diorama of the 1974 Stark Expo proves to be a diagram of the structure of a new element. With the aid of his computer Edwin Jarvis#J.A.R.V.I.S.|J.A.R.V.I.S., Stark synthesizes it. When he learns Vanko is still alive, he places the new element in his arc reactor and ends his palladium dependency.
 heavily weaponized version of the Mark II armor. Stark arrives in the Mark VI armor to warn Rhodes, but Vanko remotely takes control of both the drones and Rhodes armor and attacks Iron Man. Hammer is arrested while Romanoff and Starks bodyguard Happy Hogan go after Vanko at Hammers factory. Vanko escapes, but Romanoff returns control of the Mark II armor to Rhodes. Stark and Rhodes together defeat Vanko and his drones. Vanko seemingly commits suicide by blowing up his suit.
 Hulk plays, Agent Coulson a large hammer at the bottom of a crater in a desert in New Mexico.

==Cast==
{{multiple image
   | direction = vertical
   | width     = 150
   | footer    = Downey, Johansson and Rockwell promoting the film at the 2009 San Diego Comic-Con International.
   | image1    = Robert Downey Jr SDCC 2009 3.jpg
   | alt1      =
   | caption1  =
   | image2    = Scarlett Johansson SDCC 2009 1.jpg
   | alt2      =
   | caption2  =
   | image3    = Sam Rockwell SDCC 2009 1.jpg
   | alt3      =
   | caption3  =
}} Tony Stark / Iron Man:
:A billionaire who escaped captivity in Afghanistan with a suit of armor he created, he now struggles to keep his technology out of the governments hands. Downey and Favreau, who had been handed a script and worked from it on the first movie, conceived of the films story themselves.  On Stark being a hero, Downey said "Its kind of heroic, but really kind of on his own behalf. So I think theres probably a bit of an imposter complex and no sooner has he said, I am Iron Man &ndash; that hes now really wondering what that means. If you have all this cushion like he does and the public is on your side and you have immense wealth and power, I think hes way too insulated to be okay."  Downey put on 20 pounds of muscle to reprise the role.  Six-year-old Davin Ransom portrays Tony Stark as a child.  Virginia "Pepper" Potts:
:Starks closest friend, budding love interest, and business partner; Pepper is promoted to CEO of Stark Industries.  On her characters promotion, Paltrow opined "When we start Iron Man 2 Pepper and Tony are very much in the same vibe... as the movie progresses, Pepper is given more responsibility and shes promoted and its nice to see her sort of grow up in that way. I think it really suits her, the job fits her really well."  Paltrow expressed excitement about working with Johansson. 
*Don Cheadle as War Machine|Lt. Col. James "Rhodey" Rhodes:
:An officer in the U.S. Air Force and Tony Starks close personal friend. Cheadle replaces Terrence Howard from the first film.  Cheadle only had a few hours to accept the role and did not even know what storyline Rhodes would undergo.  He commented that he is a comic book fan, but had not previously participated in comics-themed films due to the scarcity of black superheroes.  Cheadle said he thought Iron Man was a robot before the first film came out.  On how he approached his character, Cheadle stated "I go, whats the common denominator here? And the common denominator was really his friendship with Tony, and thats what we really tried to track in this one. How is their friendship impacted once Tony comes out and owns  I am Iron Man? ".  Cheadle said his suit was 50 pounds of metal, and that he couldnt touch his face while wearing it.  Natalie Rushman / Natasha Romanoff:
:An undercover spy for S.H.I.E.L.D. posing as Starks new assistant. Johansson dyed her hair red before she landed the part, hoping that it would help convince Favreau that she was right for the role.  On why she chose the role, Johansson said, "the Black Widow character resonated with me...   is a superhero, but she’s also human. She’s small, but she’s strong... She is dark and has faced death so many times that she has a deep perspective on the value of life... It’s hard not to admire her."  She stated that she had "a bit of a freak-out moment" when she first saw the cat-suit.  When asked about fighting in the costume, Johansson responded "a big part of me is like can I move in this? Can I run in it? Can I like throw myself over things with this? And I think just the prep, you just have to put in the hours. Thats what I realized is that just putting in the hours and doing the training and repetition and basically just befriending the stunt team and spending all day, every day, just over and over and over and over until you sell it." 
*Sam Rockwell as Justin Hammer:
:A rival weapons manufacturer. Sam Rockwell was considered for the role of Tony Stark in the first film, and he accepted the role of Hammer without reading the script.  He had never heard of the character before he was contacted about the part, and was unaware Hammer is an old Englishman in the comics.  Rockwell said, "I worked with Jon Favreau on this film called Made (2001 film)|Made. And Justin Theroux, who wrote the script, is an old friend of mine, they sort of cooked up this idea and pitched it to Kevin Feige. What they did, they were maybe going to do one villain like they did with Jeff Bridges, but then they decided to split the villains. And really Mickey   is the main  , but I come to his aid."  Rockwell described his character as "plucky comic relief, but hes got a little bit of an edge".  Ivan Vanko: physicist and ex-convict who builds his own arc reactor-based weapon to exact vengeance on the Stark family.  The character is an amalgam of Whiplash and Crimson Dynamo. Rourke visited Butyrka prison to research the role,  and he suggested half of the characters dialogue be in Russian.  He also suggested the addition of tattoos, gold teeth and a fondness for a pet cockatoo, paying for the teeth and bird with his own money.  Rourke explained that he did not want to play a "one-dimensional bad guy", and wanted to challenge the audience to see something redeemable in him.  Not knowing anything about computers, Rourke described pretending to be tech-savvy as the hardest part of the role. 
*Samuel L. Jackson as Nick Fury:
:Director of S.H.I.E.L.D.; Jackson signed a nine-film contract to play the character.  On the subject of his character not seeing any action in the film, Jackson said "We still havent moved Nick Fury into the bad-ass zone. Hes still just kind of a talker." 

Jon Favreau reprises his role as Happy Hogan,  Tony Starks bodyguard and chauffeur, while Clark Gregg and Leslie Bibb reprise their roles as S.H.I.E.L.D. Agent Phil Coulson  and reporter Christine Everhart,  respectively. John Slattery appears as Tonys father Howard Stark  and Garry Shandling appears as United States Senator Stern, who wants Stark to give Iron Mans armor to the government.  Paul Bettany again voices Starks computer, Edwin Jarvis#J.A.R.V.I.S.|J.A.R.V.I.S.  Olivia Munn has a small role as Chess Roberts, a reporter covering the Stark expo,   and Stan Lee appears as himself (but is mistaken for Larry King). 
 Bill OReilly  play themselves in newscasts. Adam Goldstein appears as himself and the film is dedicated to his memory.  Further cameos include Tesla Motors CEO Elon Musk and Oracle Corporation CEO Larry Ellison.  Tanoai Reed appears as a security guard.

==Production==
===Development===
  A New Hope. You just cant do it."  Favreau also discussed in interviews how the films version of Mandarin "allows us to incorporate the whole pantheon of villains". He mentioned that S.H.I.E.L.D. will continue to have a major role. 

During development, Favreau said the film would explore Starks alcoholism, but it would not be "the Demon in a Bottle version".  While promoting the first film, Downey stated that Stark would probably develop a drinking problem as he is unable to cope with his age, the effects of revealing he is Iron Man, and Pepper getting a boyfriend.  Downey later clarified that the film was not a strict adaptation of the "Demon in a Bottle" storyline from the comic book series, but was instead about the "interim space" between the origin and the "Demon" story arc.  Shane Black gave some advice on the script, and suggested to Favreau and Downey that they model Stark on J. Robert Oppenheimer, who became depressed with being "the destroyer of worlds" after working on the Manhattan Project. 

===Pre-production===
Immediately following Iron Mans release, Marvel Studios announced that they were developing a sequel, with an intended release date of April 30, 2010.  In July 2008, after several months of negotiating, Favreau officially signed on to direct.  That same month Justin Theroux signed to write the script, which would be based on a story written by Favreau and Downey.  Theroux co-wrote Tropic Thunder, which Downey had starred in, and Downey recommended him to Marvel.  Genndy Tartakovsky storyboarded the film,  and Adi Granov returned to supervise the designs for Iron Mans armor. 

In October 2008, Marvel Studios came to an agreement to film Iron Man 2, as well as their next three films, at Raleigh Studios in Manhattan Beach, California.  A few days later, Don Cheadle was hired to replace Terrence Howard.  On being replaced, Howard stated, "There was no explanation, apparently the contracts that we write and sign arent worth the paper that theyre printed on sometimes. Promises arent kept, and good faith negotiations arent always held up."  Entertainment Weekly stated Favreau did not enjoy working with Howard, often re-shooting and cutting his scenes; Howards publicist said he had a good experience playing the part, while Marvel chose not to comment. As Favreau and Theroux chose to reduce the role, Marvel came to Howard to discuss lowering his salary – Howard was the first actor hired in Iron Man and was paid the largest salary. The publication stated they were unsure whether Howards representatives left the project first or if Marvel chose to stop negotiating.  Theroux denied the part of the report which claimed the size of the role had fluctuated.  In November 2013, Howard stated that, going into the film, the studio offered him far less than was in his three-picture contract, claiming they told him the second will be successful, "with or without you," and, without mentioning him by name, said Downey "took the money that was supposed to go to me and pushed me out." 

In January 2009, Rourke and Rockwell entered negotiations to play a pair of villains.  A few days later, Rockwell confirmed he would take the role, and that his character would be Justin Hammer.  Paul Bettany confirmed that he would be returning to voice J.A.R.V.I.S.  Marvel entered into early talks with Emily Blunt to play the Black Widow,  though she was unable to take the role due to a previous commitment to star in Gullivers Travels (2010 film)|Gullivers Travels.  Samuel L. Jackson confirmed that he had been in discussions to reprise the role of Nick Fury from the first films post-credits scene, but that contract disputes were making a deal difficult. Jackson claimed that "There was a huge kind of negotiation that broke down. I dont know. Maybe I wont be Nick Fury." 
 2009 Golden The Avengers.  In April, Garry Shandling,  Clark Gregg,  and Kate Mara  joined the cast.

===Filming=== fake working title was Rasputin.  The bulk of the production took place at Raleigh Studios,  though other locations were also used. Scenes were filmed at Edwards Air Force Base from May 11 through May 13. The location had also been used for Iron Man, and Favreau stated that he felt the "real military assets make the movie more authentic and the topography and the beauty of the desert and flightline open the movie up".  The Historic Grand Prix of Monaco action sequence was shot in the parking lot of Downey Studios, with sets constructed in May  and filming lasting through June.  Permission to film in Monaco prior to the 2009 Monaco Grand Prix had initially been awarded, but was later retracted by Bernie Ecclestone. The filmmakers shipped one Rolls-Royce Phantom (2003)|Rolls-Royce Phantom there, and filmed a track sequence in which race cars were later digitally added. Tanner Foust took on the role of driving Starks racing car.  Also in June, it was reported that John Slattery had joined the films cast as Howard Stark.  Olivia Munn was also cast, in an unspecified role. 

 A massive green screen was constructed at the Sepulveda Dam to film a portion of the Stark Expo exterior, with the rest either shot at an area high school or added digitally. To construct the green screen, hundreds of shipping containers were stacked, covered in plywood and plaster, and then painted green.  For the conclusion of that climactic scene, which the crew dubbed the "Japanese Garden" scene, a set was built inside Sony Studios in Los Angeles. 
 anamorphic lenses to match Thor. 

===Post-production===
 
Janek Sirrs was the films visual effects supervisor,  and Industrial Light & Magic again did the bulk of the effects, as it did on the first film.  ILMs visual effects supervisor on the film, Ben Snow, said their work on the film was "harder" than their work on the first, stating that Favreau asked more of them this time around. Snow described the process of digitally creating the suits:
 
 Perception worked LG smartphone,  and created the backdrops for the Stark Expo as well as the computer screen interfaces on the touch-screen coffee table and the holographic lab environment.  In total, 11 visual effect studios worked on the film. 

In January 2010, IMAX Corporation, Marvel, and Paramount announced that the film would receive a limited release on digital IMAX screens.  It was not shot with IMAX cameras, so it was converted into the format using the IMAX DMR technology.   The film underwent reshoots in February.  Olivia Munns original role was cut, but she was given a new role during the reshoots. 

The post-credits scene where Coulson finds Mjölnir in the desert was directed by Kenneth Branagh, director of Thor (film)|Thor. 

==Music==
 
A soundtrack album featuring  , The Clash, Queen (band)|Queen, Daft Punk, 2Pac and Beastie Boys. 

The film score was released commercially as Iron Man 2: Original Motion Picture Score on July 20, 2010, featuring 25 songs. John Debney composed the score with Tom Morello. 

==Release==
Iron Man 2 premiered at the El Capitan Theatre in Los Angeles, California on April 26, 2010,  and was released in 54 countries between April 28 and May 7 before going into general release in the U.S. on May 7, 2010.  The international release date of the film was moved forward to increase interest ahead of the 2010 FIFA World Cup association football tournament.  Since the film was included in a predetermined legacy distribution deal that was signed before the Walt Disney Company purchased Marvel, Paramount Pictures distributed the film and collected 8% of the box office, while the remaining portion went to Disney.  

===Marketing===
  San Diego Sherlock Holmes (another Robert Downey, Jr. film). This trailer was released online on December 16, 2009. A new trailer was shown by Robert Downey, Jr. on Jimmy Kimmel Live! on March 7 after the Academy Awards.  Promotional partners included Symantec, Dr Pepper, Burger King, 7 Eleven, Audi, LG Electronics 
and Hershey. 

Author Alexander C. Irvine adapted the script into a novel, also titled Iron Man 2, that was released in April 2010.  Prior to the film release, Marvel Comics released a four issue miniseries comic book titled Iron Man vs Whiplash, which introduced the films version of Whiplash into the Marvel Universe.  A three issue prequel miniseries titled Iron Man 2: Public Identity was released in April.   
 Ultimo (depicted as a man named Kearson DeWitt in a large armor) are enemies in the game as well as reveal that the wearer of the Crimson Dynamo armor is General Valentin Shatalov.  The game received generally unfavorable reviews, with a Metacritic score of 41% for both the PS3  and Xbox 360  versions.

===Home media===
On September 28, 2010, the film was released on DVD and Blu-ray Disc. 
 The Avengers, was delayed until April 2, 2013, due to a pending lawsuit over the suitcase used to package the collection.  

==Reception==
===Box office===
Iron Man 2 earned $312.4 million in North America, as well as $311.5 million in other territories, for a worldwide total of $623.9 million. 

====North America==== Star Treks Alice in Wonderland. 

====Outside North America====
Iron Man 2 launched in six European markets with number-one openings on Wednesday, April 28, 2010, for a total $2.2&nbsp;million from 960 venues.  It earned $100.2&nbsp;million its first five days from 6,764 theaters in 53 foreign markets for a strong average of $14,814 per site.  IMAX Corporation reported grosses of $2.25&nbsp;million at 48 IMAX theaters overseas, for an average of $46,875. This surpassed the previous record-holder for an IMAX 2D release, 2009s   ($2.1&nbsp;million).   It was the seventh-highest grossing film of 2010 internationally, behind Toy Story 3, Alice in Wonderland,  . 

===Critical response===
 
  normalized rating of 40 reviews. 

Brian Lowry of Variety (magazine)|Variety stated, "Iron Man 2 isnt as much fun as its predecessor, but by the time the smoke clears, itll do". 
Anthony Lane of   and Spider-Man 2. But Iron Man 2 hums along quite nicely".  Roger Ebert gave it 3 stars out of 4, stating that "Iron Man 2 is a polished, high-octane sequel, not as good as the original but building once again on a quirky performance by Robert Downey Jr".  Frank Lovece of Film Journal International, a one-time Marvel Comics writer, said that, "In a refreshing and unexpected turn, the sequel to Iron Man doesnt find a changed man. Inside the metal, imperfect humanity grows even more so, as thought-provoking questions of identity meet techno-fantasy made flesh." 

Conversely, Kirk Honeycutt of The Hollywood Reporter stated, "Everything fun and terrific about Iron Man, a mere two years ago, has vanished with its sequel. In its place, Iron Man 2 has substituted noise, confusion, multiple villains, irrelevant stunts and misguided story lines." 

===Accolades===
{| class="wikitable" style="width:99%;"
|-
! Year !! Award !! Category !! Winner/Nominee !! Result !! Ref.
|- 2010
|rowspan="1"|Hollywood Hollywood Film Award Visual Effects of the Year
| Iron Man 2
| 
|style="text-align:center;"| 
|- Satellite Awards Satellite Awards Best Sound (Mixing & Editing)
| Iron Man 2
| 
|rowspan="2" style="text-align:center;"| 
|- Best Visual Effects
| Iron Man 2
| 
|- 2010 Teen Teen Choice Awards
| 
| Iron Man 2
| 
|rowspan="7" style="text-align:center;"| 
|-
|  Robert Downey Jr.
| 
|-
|  Gwyneth Paltrow
| 
|- Choice Movie Actress: Sci-Fi Scarlett Johansson
| 
|- Teen Choice Choice Movie Villain Mickey Rourke
| 
|- Choice Movie Dance Robert Downey Jr.
| 
|- Choice Movie Fight/Action Sequence Don Cheadle and Robert Downey Jr. (Iron Man & War Machine vs The Hammer Drones)
| 
|-	
|rowspan="10"| 2011
|rowspan="5"| 37th Peoples Choice Awards|Peoples Choice Awards
| Favorite Action Movie
| Iron Man 2
|  
|rowspan="5" style="text-align:center;"| 
|- Favorite Movie	
| Iron Man 2
| 
|- Favorite Movie Actor Robert Downey Jr.
| 
|- Favorite Action Star Robert Downey Jr.
| 
|- Favorite On-Screen Team Robert Downey, Jr. and Don Cheadle			
| 
|- Academy Awards Best Visual Effects
| Iron Man 2
| 
|style="text-align:center;"| 
|- Saturn Awards Best Science Fiction Film
| Iron Man 2
| 
|rowspan="4" style="text-align:center;"| 
|- Best Actor Robert Downey Jr.
| 
|- Best Supporting Actress Scarlett Johansson
| 
|- Best Special Effects
| Iron Man 2
| 
|}

==Sequel==
  The Walt The Avengers.  Disney, Marvel and Paramount announced a May 3, 2013 release date for Iron Man 3.  Shane Black directed Iron Man 3,  from a screenplay by Drew Pearce.  Downey, Paltrow, and Cheadle reprised their roles, while Ben Kingsley  played Trevor Slattery,  Guy Pearce played Aldrich Killian,  and Rebecca Hall  played Maya Hansen. 

==See also==
* List of films featuring powered exoskeletons

==Notes==
  Walt Disney Studios.   
 

==References==
{{Reflist|30em|refs=
     

     

 Iron Man 2 DVD commentary with Jon Favreau 

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   ACDC song-shoot to thrill 

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

    

   

   

   

   

   (Video) 

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

}}

==External links==
 
 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 