Vanity (1935 film)
{{Infobox film
  | name           = Vanity
  | image          = 
  | image_size     = 
  | caption        = 
  | director       = Adrian Brunel
  | producer       = George Smith
  | writer         = Ernest Denny (play)   Adrian Brunel
  | starring       = Jane Cain Percy Marmont
  | music          = 
  | cinematography = Geoffrey Faithfull
  | editing        = 
  | studio         = George Smith Productions
  | distributor    = Columbia Pictures  
  | released       = December 1935
  | runtime        = 76 minutes
  | country        = United Kingdom English
  }} 1935 British John Counsell. The plot concerns a conceited actress, convinced of the general adoration in which she is held, faking her own death in order to gratify herself by observing the depth of grief caused by her demise. However the actual reactions to the "news" prove to be far from what she had expected.

==Production==
The film was a quota quickie production, made at Nettlefold Studios in Walton-on-Thames for distribution by Columbia Pictures. It was based on a play by Ernest Denny. It is now considered to be a lost film.
 speaking clock in the United Kingdom between 1936 and 1963.

==Partial cast==
* Jane Cain as Vanity Faire
* Percy Marmont as Jefferson Brown
* H.F. Maltby as Lord Cazalet John Counsell as Dick Broderick

==Bibliography==
* Low, Rachael. History of the British Film: Filmmaking in 1930s Britain. George Allen & Unwin, 1985 .

==External links==
*  
*   at BFI Film & TV Database

 

 
 
 
 
 
 
 
 
 
 
 
 


 