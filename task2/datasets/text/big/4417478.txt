Rally Round the Flag, Boys!
{{Infobox film
| name           = Rally Round the Flag, Boys!
| image          = Poster of the movie Rally Round the Flag, Boys!.jpg
| image_size     =
| caption        =
| director       = Leo McCarey
| producer       = Leo McCarey
| writer         = Max Shulman (novel) Claude Binyon (screenplay)
| narrator       = David Hedison
| starring       = Paul Newman Joanne Woodward Joan Collins
| music          = Cyril J. Mockridge
| cinematography = Leon Shamroy
| editing        = Louis R. Loeffler
| distributor    = 20th Century Fox
| released       =  
| runtime        = 106 min.
| country        = United States English
| budget         = $1,870,000 
| gross          = $3.4 million (est. US/ Canada rentals) 
}}

Rally Round the Flag, Boys! is a 1958 film adaptation of the novel of the same name by Max Shulman, directed by Leo McCarey, starring Paul Newman and Joanne Woodward, and released by 20th Century Fox. The title comes from a line in the song "Battle Cry of Freedom".

==Plot summary==
In the fictional town of Putnams Landing, Harry Bannerman (Paul Newman) is slowly going insane because of his wife Grace (Joanne Woodward), who insists on attending every civic committee meeting. When the government selects their town for their new missile base, Grace joins a committee to stop it. 

Harry is made the liaison for the military, and Graces activities cause him no end of trouble. Added to the dilemma is Angela Hoffa (Joan Collins), whose efforts to get Harry for herself lead to dizzying recriminations and misunderstandings.

==Cast==
* Paul Newman as Harry Bannerman
* Joanne Woodward as Grace Bannerman
* Joan Collins as Angela Hoffa
* Murvyn Vye as Oscar Hoffa
* Jack Carson as Captain Hoxie
* Tuesday Weld as Comfort Goodpasture
* Dwayne Hickman as Grady Metcalf, Comforts suitor
* Gale Gordon as General Thorwald
* Tom Gilson as Corporal Opie
* O.Z. Whitehead as Isaac Goodpasture, Comforts Father
* Stanley Livingston as Peter Bannerman
* Percy Helton as Waldo Pike, the Plumber
* David Hedison as Narrator (voice only)

==Production== Paul Douglas, but was taken over by Jack Carson after Douglas ended up being ill, according to a July 1958 The Hollywood Reporter news item. 
 bombshell Jayne Mansfield, who was replaced with Joan Collins. 
 Daily Variety news item reported that in March 1958, Buddy Adler was set to produce the movie, and was considering the film to star Frank Sinatra, Deborah Kerr and William Holden.

==Release and reception==
Rally Round the Flag, Boys! premiered in New York City theatres on December 23, 1958. It was released nationwide in February of 1959.

The film currently holds a 25% "Rotten" rating at RottenTomatoes.com.

===Awards===
Rally Round the Flag, Boys! was nominated for the Golden Laurel Awards held on September 23, 1959 and received 4th place for both Top Comedy Female Performance (Joanne Woodward) and Top Comedy. Director Leo McCarey was later nominated for the Directors Guild of America Award in 1960 for Outstanding Directorial Achievement in Motion Pictures for Rally Round the Flag, Boys!.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 