The Suspended Step of the Stork
{{Infobox film
| name           = The Suspended Step of the Stork
| image          =
| alt            =
| caption        =
| director       = Theo Angelopoulos
| producer       = Theo Angelopoulos
| writer         = Theo Angelopoulos Tonino Guerra, et al
| narrator       =
| starring       = {{Plainlist| *Jeanne Moreau
*Gregory Patrikareas
*Gerasimos Skiadaresis Christoforos Nezer
*Dimitris Poulikakos }}
| music          = Eleni Karaindrou
| cinematography = {{Plainlist|
*Giorgos Arvanitis
*Andreas Sinanos
}}
| editing        =
| distributor    =
| released       =  
| runtime        = 143 minutes; 126 minutes (Greece)
| country        = Greece
| language       = Greek
| budget         =
}}

The Suspended Step of the Stork ( ,  Transliteration|translit.&nbsp;To meteoro vima tou pelargou) is a 1991 Greek film directed by Theodoros Angelopoulos.    It was entered into the 1991 Cannes Film Festival.   

==Cast==
* Marcello Mastroianni as Missing Politician
* Jeanne Moreau as The Woman
* Gregory Patrikareas as Alexandre the Reporter (as Gregory Karr)
* Ilias Logothetis as Colonel
* Dora Hrisikou as The Girl
* Vassilis Bouyiouklakis as Production Manager
* Dimitris Poulikakos as Chief Photographer
* Gerasimos Skiadaressis as Waiter
* Tasos Apostolou as Perchman
* Akis Sakellariou as Sound Operator
* Athinodoros Prousalis as Hotel-keeper
* Mihalis Giannatos as Shopkeeper Christoforos Nezer as Parliaments President
* Yilmaz Hassan as Hanged Man
* Benjamin Ritter as Sound Operator

== References ==
 

==External links==
*  

 

 
 
 
 
 
 
 


 
 