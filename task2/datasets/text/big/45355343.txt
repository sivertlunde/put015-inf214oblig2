Unexpected (2015 film)
 
 
Unexpected is a 2015 American drama film directed by Kris Swanberg. The film was acquired by The Film Arcade after it premiered at the 2015 Sundance Film Festival. Swanberg co-wrote the film with Megan Mercier. The film is Swanbergs third feature, although she said in an interview that it felt like her first.  The film stars Cobie Smulders as a teacher at an inner city Chicago high school who unintentionally falls pregnant. One of her students, Jasmine (Gail Bean), is also unexpectedly pregnant, and the two bond through planning their futures.

==Production==
Swanberg took about two years to write the films script. The film was shot in Englewood, Chicago.  Cobie Smulders was pregnant during filming. The coinciding of Smulders pregnancy with that of her character was not intentional.  The film was completed in October 2014. 

==Cast==
*Cobie Smulders as Samantha Abbott
*Gail Bean as Jasmine
*Anders Holm as John
*Elizabeth McGovern as Samanthas mother

==References==
 

==External links==
*Unexpected on the Internet Movie Database

 
 
 
 
 


 