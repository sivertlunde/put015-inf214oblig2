Night Watch (2004 film)
 
{{Infobox film
| name = Night Watch Nochnoi Dozor
| image = Night Watch (2004 film) theatrical poster.jpg
| alt = 
| caption = International poster
| director = Timur Bekmambetov
| producer = Konstantin Ernst Anatoli Maksimov
| screenplay = Timur Bekmambetov Sergei Lukyanenko
| based on =  
| starring = Konstantin Khabensky Vladimir Menshov Valeri Zolotukhin Mariya Poroshina Galina Tyunina
| music = Yuri Poteyenko
| cinematography = Sergey Tromifov
| editing = Dimitry Kiselev Channel One Russia Bazelevs Company
| distributor = Gemini Film
| released =  
| runtime = 114 minutes  
| country = Russia
| language = Russian
| budget = $4.2 million
| gross = $33,899,078
}} The Night Day Watch.

==Plot== Others (Иные, иной; Inyye, Inoy). The Others are divided into two camps, with each Other either allied with the forces of Light or the forces of Darkness, and the prologue recounts a great battle between the two sides. Geser, the Lord of the Light, realized that the two forces would eventually annihilate each other. He stopped the battle and offered a truce to the forces of Darkness, led by Zavulon.   To make sure each side honored its side of the agreement, the Light would have a "Night Watch" to monitor the forces of Darkness at night, and the Darkness would have a "Day Watch" to police the forces of Light during the day. This agreement maintained balance for centuries to come.

In modern day Moscow, Anton Gorodetsky ( ) visits a witch named Daria and asks her to cast a spell to return his wife to him, agreeing that she should miscarry her child as part of it. Just as the spell is about to be completed, two figures burst in and restrain Daria, preventing her from completing the spell. When they notice that Anton is able to see them, they realize that he is an Other. 

Twelve years later, Anton has become a member of the Night Watch and is working with the same team. On Antons request, Kostya, his neighbor, takes him to see his father, a butcher, to get blood for Anton to drink. The father does this reluctantly, and then tells Kostya that members of the Night Watch only drink blood when they hunt vampires like themselves.

A twelve-year-old boy,   Yegor, hears "The Call", a psychic call by a vampire who intends to feed on him. Anton tracks Yegor, being able to hear the call as he gets closer to Yegor, due to the blood he drank. On the way he sees a blond woman that terrifies him. Realizing she is under a deadly curse, Anton uses an ultraviolet flashlight to see if she is a vampire, but she is not. Two vampires are about to feed on Yegor when Anton arrives, and Anton kills one of them while being badly injured. A member of the Day Watch arrives and reveals that the Day Watch is aware of the murder of one of their Dark members.

Anton is healed by Geser, who notes that he could have solved things more easily by entering into the Gloom - a shadow world only accessible by the Others. After Anton tells him about the woman in the subway, he reveals a legend about a virgin who is cursed so that people and animals around her die or sicken, and that she is accompanied by a "vortex of damnation". It is now clear that the virgin, now reborn, will soon die, unless the Night Watch finds the one who cursed her. Geser gives Anton an assistant named Olga, who looks like a stuffed owl. Anton refuses and laughs, until he sees Geser throw Olga out the window, whereupon she turns into a living owl and flies away. At Antons apartment, the owl arrives and shape shifts into a woman.

Kostya arrives and says he knows that Anton killed the vampire Dark Other. Anton and Olga track Yegor to his home, where they must enter the Gloom, as Yegor is there hiding from the female vampire. The Gloom almost consumes Yegor, but a blood sacrifice from Anton satisfies it enough for them to escape. Emerging from the Gloom, Anton sees a photo of Yegor and his mother, who is apparently Antons ex-wife. The Night Watch members, Tiger and Bear, stay behind to protect Yegor, but as soon as they are distracted, the boy escapes and follows the call of the female vampire. During this time, Zavulon is seen working on a computer prediction in which he is fighting with someone, constantly losing.

Anton and Olga arrive at a command and control center set up near the apartment of Svetlana, the cursed woman from the subway train. A vortex capable of immense destruction has appeared over her apartment and bad things have been happening to those near her. Just before Anton goes into speak with her, Svetlana tells her neighbor Gregori that his mother has passed away, and this sets off a series of events that will shortly cause a nuclear meltdown unless the curse is abated. Anton enters Svetlanas apartment under the guise of needing medical care and talks to her, revealing that she cursed herself on her own, which also means she is an Other. As soon as this is revealed, the curse ends and the vortex disappears. Yegor escapes the grip of the female vampire, but is caught by  Zavulon, who appears on the roof. During a duel, Anton attempts to stab   Zavulon, but   Zavulon sidesteps the swipe just as Yegor runs up.   Zavulon stops Antons momentum, both saving Yegors life and making it appear as if Anton were attempting to kill his son.   Zavulons assistant reads Antons personal file aloud. When Yegor finds out that Anton tried to kill him before he was born, and that he has special powers that the prophecy says are held by the Great One, he willingly turns to the Darkness, to Antons great dismay. An afterword describes that as Yegor has chosen the Dark, he will lead the world into Darkness, but as long as there are those who choose to fight the Darkness all around them, there is hope.

==Cast==
* Konstantin Khabensky as Anton Gorodetsky
* Vladimir Menshov as Geser
* Valeri Zolotukhin as Gennady Saushkin, Kostyas father
* Mariya Poroshina as Svetlana
* Galina Tyunina as Olga Yuriy "Gosha" Kutsenko as Ignat
* Aleksei Chadov as Kostya Saushkin
* Zhanna Friske as Alicia Donnikova
* Ilya Lagutenko as Andrei
* Viktor Verzhbitsky as Zavulon

==Production==
The film was the first big-budget  .
 shots to which a modern audience is accustomed. 16 Russian VFX studios and several freelancers were used, each chosen for their individual strengths.  Many shots were created by different artists across different time zones, using the Internet to share data and images. 

===Music===
The film contains several songs from rock bands, e.g. "Jack" by the Belarusian group TT-34 and "Spanish" by Drum Ecstasy.
The song played in the credits of the international version of the movie is called "Shatter" and performed by the Welsh rock band Feeder. The track was a top 20 hit single in the United Kingdom charting at #11 in 2005, to coincide with the international release of the film. The song playing during the end credits of the American release of Night Watch is "Fearless" by The Bravery.  In the original Russian version it is a rap.  

==Release== Day Watch, was released across the CIS on 1 January 2006. There is also a TV series in production.   A third film was announced by 20th Century Fox but, Timur decided to film Wanted for Universal Pictures. He went on to say that Twilight/Dawn watch would just be to much like Wanted. 

The film attracted the attention of Fox Searchlight Pictures, which paid $4 million to acquire the worldwide distribution rights (excluding Russia and the Baltic states) of Night Watch and its sequel Day Watch.  

===Critical reception===
Night Watch holds a 58% rating on review aggregator website Rotten Tomatoes based on 127 reviews; the consensus states: "This Russian horror/fantasy film pits darkness and light against each other using snazzy CGI visuals to create an extraordinary atmosphere of a dank, gloomy city wrestling with dread." 

===International release===
One year after the Russian release, the international distribution began. Other than a London premiere at the Odeon West End as part of the Frightfest horror film festival, that screened amid heavy security on August 28, 2005,  the first European country outside CIS was Spain where it was released on September 2, 2005. By mid October it had been released in most European countries, and on February 17, 2006 it had a limited release in the United States, followed by a full release on March 3. By February 13, 2006 (i.e. before the U.S. release) it had grossed US$32 million.

  dubbed in subtitles appearing UK on zone 4 Day Watch, are now available in HD on Vudu. The HDX encodes are based on the International release and retain the original Russian dialog track with the stylized subtitles.

The original Russian "Directors Cut" of the film was released, apart from Russia, in some European countries on DVD by 20th Century FOX. The only difference of this version from the original Russian version is the absence of the opening credits.

=="Nochnoi Bazar" fun re-dub==
In 2005, a "fun re-dub" was released under the title "Nochnoi Bazar" ("Night Chat"). The project was initiated by the writer Sergei Lukyanenko as a nod to popular (illegal) fun re-dubs by "Goblin" (Dmitry Puchkov). However, this fun redub was made with full consent of the filmmakers and copyright holders and released on DVD by Channel One Russia. The script was written by the Russian comedian Alexander Bachilo, the song parodies were written and composed by Alexander Pushnoy. The narration was done by Leonid Volodarskiy, a popular voiceover translator of pirated videoreleases in the Soviet Union.

==Novel vs. film==
The film primarily follows the events of first part ("Story One: Destiny") of the novel Night Watch, with two opening scenes added from later in the series.  Although the movie had one of the biggest budgets in the history of Russian filmmaking, there were still restraints on its content, especially given the length of the original three-hundred page, three-part book. Some of the changes made were small and insignificant; others significantly altered the nature of the plot. So, the film doesnt precisely follow the contents of the book - rather, the blockbuster is composed of different episodes, found in both "Night Watch" and "Day Watch" books by Sergei Lukyanenko. In the film. certain scenes were reassessed, the plot line (as a chain of episodes and the logical links between them) has been significantly modified.

The subtitles of the English-language version reflect some difference in translation: the "gloom" in the film is translated as "twilight" in the book; the name transliterated as "Yegor" in the film is transliterated as "Egor" in the book, and "Zavulon" in the movie is transliterated as "Zabulon" in the book.

==See also==
*Vampire film

==References==
 

==External links==
*   (US)
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 