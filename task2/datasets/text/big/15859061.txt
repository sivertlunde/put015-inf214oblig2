Periya Idathu Penn
{{Infobox film|
| name = Periya Idathu Penn பெரிய இடத்துப் பெண்
| image = Periya Idathu Penn.jpg
| caption = 1963 film poster
| director       = T. R. Ramanna
| producer       = T. R. Ramanna
| writer         = Sakthi T. K. Krishnasamy
| screenplay     = Sakthi T. K. Krishnasamy
| story          = Sakthi T. K. Krishnasamy
| starring       = M. G. Ramachandran B. Saroja Devi M. R. Radha T. R. Rajakumari S. A. Ashokan Nagesh
| music          = Viswanathan–Ramamoorthy
| cinematography = M. A. Rehman
| editing        = M. S. Mani
| studio         = R. R. Pictures
| distributor    = R. R. Pictures
| released       =   
| runtime        = 156 mins
| country        =   India Tamil
}}
 1963 Tamil language drama film directed by T. R. Ramanna. The film features M. G. Ramachandran, B. Saroja Devi, M. R. Radha and T. R. Rajakumari in lead roles. 
The film, produced by , had musical score by Viswanathan–Ramamoorthy and was released on 10 May 1963.  The film was blockbuster, highest grossing film of the year and ran more than 100 days. 

==Plot==

Murugappa (MG Ramachandran) is a small time farm labourer who lives with his widowed sister Gangamma in a village. Pillaival (M. R. Radha) is the zamindar of the village and Sabapathy (SA Asokan) and Punitha (Saroja Devi) are his children. Punitha is studying in college in a nearby town while Sabapathy is not educated. Both the father and the children are both arrogant about their wealth and try to rule the villagers. Murugappa tries to question their authority and this leads to frequent clashes with the zamindars family. Pichandi (Nagesh) is a wealthy college mate of Punitha who is crazy about her.

Sabapathy falls in love with Thillaiammal (Manimala) who has been informally enagaged to Murugappa for a long time. Both Pillaival and Gangamma propose for her on the same day. To avoid a direct clash with the zamindar, her father says that he took a vow that his daughter would marry the winner of a silambam competition.

Punitha promises to marry Pichandi if he dopes a drink which Murugappa drinks during the fight. Sabapathy wins the fight and marries Thilakam. Punitha goes back on her word and an angry Pichandi confesses his duplicity to Murugappa, who confronts Punitha, The  two get into an argument during which Murugappa wows to mary Punitha.

The two families get into another clash regarding the villagers right to worship at the temple at the same time as Pillaival. In retaliation he sends his henchmen to beat up Murugappa and burn down their house. When Gangamma confronts him, he rapes her and she disappears after writing a suicide  note to her brother urging him to leave the village and make a life elsewhere. Pillaival is haunted by the fear that Gangamma would return from the grave to take revenge.
 Pygmalion on him and MGR emerges from the tutelage as Azhagappa competent in English, and even knows to play the piano. He meets Punitha at a club and the two begin dating. She fails to connect the suave Azhagappa with the village bumpkin Murugappa and falls for him.

Pillaival gets an anonymous letter informing him that his daughter is in love with someone in the city and he sends for her immediately and confronts her. She admits to being in love with Azgagappa and her family decides to get them married. Azhagappa and Pichandi as his secretary meet Pillaival and the marriage takes place.

He meets Thillaiammals father and reveals his identity to him. He discovers that Pillaival is in responsible for his sisters death and the whole family comes to know. Sabapathy tries to support his father and Punitha opposes him. Azhagappa reveals his identity and walks out of the marriage. He finds it impossible to live with Punitha after knowing what her father did to his sister.

Punitha discovers that she is pregnant and with the help of her sister in law Thillaiammal, meets Murugappan and tells him the truth. Murugappan is caught between his desires to live with his wife and avenge his sisters death. Sabapathy discovers her there and gets into a fight with Murugappan. Confronted by a deadlock situation, Punitha initially contemplates suicide but decides to live and have the baby. Pichandi meanwhile falls in love with Thillaiammals sister Valli and learns silambam from her father in order to wed her. Murugappan and Punitha have a baby boy and are still unable to be together.

Murugappan longs to see his baby and goes to her house secretly in the nights. He finds that Pillai val has been stabbed. Punitha and Sabapathy think that their father was killed by Murugappan, who tries to follow the killer and finds out that it is his sister. She says that she was in hiding waiting for a chance to avenge herself and advices him to return to his wife and son.

Meanwhile, he is confronted by Sabapathy and the police pursue Gangamma who jumps off a bridge and kills herself. All they find is a note from her confessing to Pillaivals murder and urging Murugappan to return to his wife.  Pichandi marries Valli and the family unites.

==Cast==
* M. G. Ramachandran
* B. Saroja Devi
* M. R. Radha
* S. A. Ashokan
* Nagesh
* T. R. Rajakumari
* N. S. K. Kolappan
* Manimala
* Jothilakshmi

==Crew==
*Director: T. R. Ramanna
*Producer: T. R. Ramanna
*Production Company: R. R. Pictures
*Music: Viswanathan–Ramamoorthy
*Lyrics: Kannadasan
*Story: Sakthi T. K. Krishnasamy
*Screenplay: Sakthi T. K. Krishnasamy
*Dialogues: Sakthi T. K. Krishnasamy
*Cinematography: M. A. Rehman
*Editing: M. S. Mani
*Art Direction: Selvaraj
*Choreography: Joseph Krishna
*Stunt: Shyam Sundar
*Audiography: T. S. Rangasamy

==Soundtrack==

{{Infobox album |  
 Name        = Periya Idathu Penn|
 Type        = soundtrack |
 Artist      = Viswanathan–Ramamoorthy|
 Cover       = | 
 Released    = 1963| 
 Recorded    = |  Feature film soundtrack |
 Label       = Westrex| 
 Producer    = Viswanathan–Ramamoorthy| 
}}

The music composed by Viswanathan–Ramamoorthy. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Kannadasan || 03:09
|-
| 2 || Andru Vandhadhum (Pathos) || T. M. Soundararajan, P. Susheela || 03:00
|-
| 3 || Avanukkena Thoongi || T. M. Soundararajan, P. Susheela || 02:43
|-
| 4 || Kannenna Kannenna || T. M. Soundararajan || 03:14
|-
| 5 || Kattodu Kuzhalaada || P. Susheela, L. R. Eswari || 04:58
|-
| 6 || Paarappa Pazhaniappa || T. M. Soundararajan || 03:01
|-
| 7 || Thulli Odum Kaalgal || T. M. Soundararajan, P. Susheela || 03:29
|-
|}

==References==
 

==External links==
* 

 
 
 
 
 