South of the Border (2009 film)
{{Infobox film
| name           = South of the Border
| image          = Poster of the movie South of the Border.jpg
| caption        = Theatrical release poster
| director       = Oliver Stone
| producer       = Fernando Sulichin Jose Ibanez Rob Wilson
| writer         = Tariq Ali Mark Weisbrot
| narrator       = Oliver Stone Lula da Silva Tariq Ali
| music          = Adam Peters
| cinematography = Carlos Marcovich Albert Maysles Lucas Fuica
| editing        = Alexis Chavez Elisa Bonora
| studio         = Good Apple Productions Ixtlan Muse Productions New Element Productions Pentagrama Films
| distributor    = Cinema Libre
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         =
| gross          = 
}}
South of the Border is a 2009 American  , September 1, 2009. (Archived by WebCite at http://www.webcitation.org/5n4FtQ9zB)  Stone stated that he hopes the film will help people better understand a leader who is wrongly ridiculed "as a strongman, as a buffoon, as a clown." 
 Lula da Silva of Brazil. 

==Content== Argentine peso collapse of 2001, combined with Latin suspicions of Plan Colombia|U.S. drug-eradication efforts and resentment over the selling off of natural resources through multinational companies, have contributed to the rise of socialist and social-democratic leaders across the region.

According to the Associated Press, "Stone said he didnt see it necessary to present the oppositions case in his film." 

==Production==
Oliver Stone spoke of his move to balance the media reactions to other pink tide leaders in the region: "...&nbsp;the project started as something about the American media demonizing Latin leaders. It became more than that as we got more involved. The press in America, I think youre aware, has divided the Latin continent into the bad Left and the good Left." 

US economist Mark Weisbrot advised Stone on the documentary and was credited as one of the writers alongside Tariq Ali.   MSNBC. 7 Sept 2009. (Archived by WebCite at http://www.webcitation.org/5n4Fgb838)    

In May 2010, Stone began a Latin American tour to promote the film, with screenings planned in Ecuador, Brazil, Bolivia, Paraguay and Argentina. The documentary was also being released in some cities in the United States and Europe later in 2010.  , by Ian James, Associated Press, 29-05-2010 

The Box Office Mojo site reports that as of August 2010 the film had grossed $198,600 domestically. 

The documentary was released in the U.K. at the end of July 2010 by Dogwoof film distributors. 

==Reception==
South of the Border received mixed reviews, and it holds a 50% approval rating on Rotten Tomatoes, as well as a score of 45 out of 100 on Metacritic, based on 19 film reviews collected. 

===United States===
 , 8 September 2009,    
Variety (magazine)|Variety said, "The docu (sic) offers little genuine information and no investigative research, adopting a style even more polemical than Stone’s earlier focus on Fidel Castro and Yasser Arafat."   Reuters said Stone "deliver  a strong endorsement of Chavezs socialist agenda, and question  the tenets of what he calls U.S. predatory capitalism," {{cite news|url=http://www.reuters.com/article/entertainmentNews/idUSL72457020090908|title=Stone says Chavez film may struggle to get U.S. play
|date=8 September 2009|publisher=Reuters}} 

Bloomberg L.P.|Bloombergs journalist, Fabiola Moura described the film as "rosy in its picture", while being "a tonic dose of a perspective rarely seen in U.S. media coverage of the region", Fabiola Moura, 29 September 2009, Bloomberg L.P.|Bloomberg,    and commented:  The movie doesn’t mention Chavez’s blacklisting of millions of people who signed a petition seeking a recall vote against him in 2004; the persecution of political rivals; the creation of a new "Capital District" to usurp power from the opposition-led Caracas city government; and the refusal to renew the broadcasting license of Radio Caracas Television, the country’s oldest station.  
 NPR says the film tells only one side of the story and gives "kid glove treatment" to Chavez and his allies. 

The  , 9 July 2010,   

Foreign Policy Magazine said Stone asked softball questions of the South American leaders and  

  added that "he   failed to find any factual errors in the film – despite some rather desperate attempts." Mark Weisbrot, guardian.co.uk, 16 July 2010,    Rohter replied and reiterated that a number of economical and historical facts were either omitted or changed by the filmmakers in Chavezs favor. He called the accusations levied against him by the films creators a "smokescreen" and a "smear campaign".  The films creators claimed similar treatment from Rohter.

Ronald Radosh criticized the film in both the Wall Street Journal and Pajamas Media   

Mark Weisbrot, who worked on the movie, wrote: "Its nice when you make a documentary about how the major media outlets misrepresent reality, and the media response to the film proves your point. In fact, the medias response to Oliver Stones South of the Border, which I wrote with Tariq Ali, really completes a number of the films arguments." 

Tariq Ali, who also worked on the movie added: “It’s hardly a secret that we support the other side. It’s an opinionated documentary”...“the aim of our film is very clear and basic.” In “South of the Border,” he added: “We were not writing a book, or having an academic debate. It was to have a sympathetic view of these governments.”  

===United Kingdom===
The documentary received mixed reviews from the mainstream press. The   but hes more Alan Partridge."  The film received a rating of one star out of five from The Independent reviewer who said "it becomes clear that Stone is ill-equipped to conduct a serious political analysis of the continent. He gets amazing access to national leaders and yet, face to face with them, he doesnt even look interested in what they have to say." Anthony Quinn, The Independent, 30 July 2010,   

===Latin America===
In Venezuela, the film grossed US$18,601 on 20 screens after 12 days.   The film was criticised by Leopoldo López, an opposition leader, for ignoring and not mentioning a number of very serious problems in Venezuela such as escalating crime, inflation, food scarcity, housing, and access to water and electricity which has (according to López) worsened under Chavezs rule. He disagrees with Stones argument that "most peoples lives in this country have improved under Chavez".  Stone responded that presenting the second party oppositions case in the documentary was not his main goal. He also said that: "People forget that he cut the poverty rate by one half...People in Venezuela are getting an education, they are getting health care and welfare.   actually delivered on what he said he would." 

=== Other ===
  or Che Guevara. All these changes have come about through democratic elections." 

==See also==
* The Revolution Will Not Be Televised (film)
* The War on Democracy, documentary by John Pilger
* X-Ray of a Lie

==References==
 

==Further reading==
* 
*  - slideshow by The Huffington Post
*  by Oliver Stone, March 28, 2010
*  by NPR
*  - video report & interview by Democracy Now!

==External links==
* 
* 
* 
* 
* 
 

 
 
 
 
 
 
 
 
 
 
 