Chal Chalein
{{Infobox film
| name           = Chal Chalein
| image          = Chal_Chalein.jpg
| alt            =  
| caption        = Original DVD Cover
| director       = Ujjwal Singh
| producer       = Mahesh Padalkar
| writer         = Vijaya Ramchandrula
| starring       = Mithun Chakraborty Chirag P Ruia Mukesh Khanna Rati Agnihotri Shilpa Shukla Kanwaljeet Singh
| music          = Ilaiyaraaja
| cinematography = Arvind K
| editing        = Aseem Sinha
| studio         = 
| distributor    = 
| released       =  
| runtime        = 148 minutes
| country        = India
| language       = Hindi Rs 5 Crores
| gross          =  
}}
 2009 Bollywood|Hindi-language Indian family directed by Ujjwal Singh and produced by Mahesh Padalkar, starring Mithun Chakraborty, Rati Agnihotri, Mukesh Khanna and Kanwaljeet Singh in a story about the academic pressures on children.

== Plot ==

A student commits suicide because of extreme anxiety over pressure from his parents to get good grades. The students empathetic friends enlist the help of a lawyer named Sanjay to speak up against the parents, the government and the school system. The case draws national attention and generates massive public opinion.

==Production==

Chakraborty (prosecuting lawyer) worked again with his 1980s romantic films co-star Rati Agnihotri (students mother). Their previous films included Pasand Apni Apni, Shaukeen, and Boxer (1984 film)|Boxer.

== Cast ==

*Mithun Chakraborty
*Mukesh Khanna
*Rati Agnihotri
*Nishikant Dixit as Father
*Tanvi Hegde
*Syed Hussain Haider Abidi
*Shilpa Shukla
*Kanwaljeet Singh
*Vishwajeet Pradhan
*Anup Soni
* Anand Abhyankar

==Soundtrack==

Maestro Illayaraja provided the film score for this film and lyrics penned by Piyush Mishra.

===Track listing===

{{track listing
| extra_column = Singer(s)
| total_length = 28:48

| title1 = Tum Bhi Dhoondna Hariharan
| length1 = 
| title2 = Shehar Hai Khoob Kya Hai
| extra2 = Shaan (singer)|Shaan, Shreya Ghoshal & Krishna
| length2 = 
| title3 = Jhoom Jhoom So Ja
| extra3 = Sadhna Sargam
| length3 = 
| title4 = Gup Chup Shaam Thi Hariharan
| length4 = 
| title5 = Chal Chal Chal Ke
| extra5 = Aditya Narayan, Kavita Krishnamurthy & Krishna
| length5 = 
| title6 = Batladein Koi
| extra6 = Aditya Narayan, Shaan (singer)|Shaan, Sunidhi Chauhan & Krishna
| length6 = 
| title7 = Uff Are Tu Mirch Hai
| extra7 = Shaan (singer)|Shaan, Shreya Ghoshal & Krishna
| length7 = 
}}

== External links ==
*   at Bollywood Hungama

 
 
 
 
 


 