La Conjura de El Escorial
{{Infobox Film
| name           = La Conjura de El Escorial
| director       = Antonio del Real
| writer         = Antonio del Real   Manuel Mir
| starring       = Jason Isaacs   Julia Ormond   Jordi Mollà
| music          = 
| cinematography = 
| editing        = 
| distributor    = Mascara Films
| released       = 5 September 2008
| runtime        = 128 mins 156 mins (extended cut)
| country        = Spain Spanish English English
}} Spanish historical historical drama Philip II of Spain. 

On the night of Easter Monday March 31, 1578, assassins murdered the treasury secretary Juan de Escobedo in cold blood in the royal palace of El Escorial). There ensues a series of investigations that are aimed at people close to the Court of Philip II.
 Crown Prince Ferdinand which occurred in 1807 during the reign of Ferdinands father Charles IV of Spain.

==Cast==
*Jason Isaacs  ...  Antonio Pérez, royal secretary 
*Julia Ormond  ...  Princess of Éboli 
*Jürgen Prochnow  ...  Captain Espinosa 
* Blanca Jara ... Damiana, the servant girl, the love of Captain Espinosa
*Jordi Mollà  ...  Mateo Vázquez, the investigating Jesuit father
*Joaquim de Almeida  ...  Juan de Escobedo, secretary of Don Juan
*Juanjo Puigcorbé  ...  King Philip II of Spain

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 

 