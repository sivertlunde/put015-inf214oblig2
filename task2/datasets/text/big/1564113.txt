The Big Heat
 
{{Infobox film
| name           = The Big Heat
| image          = thebigheatmp.JPG
| image_size     = 
| alt            = 
| caption        = Theatrical release poster Robert Arthur
| director       = Fritz Lang
| screenplay     = Sydney Boehm
| based on       =  
| starring       = Glenn Ford Gloria Grahame Lee Marvin
| music          = Henry Vars
| cinematography = Charles Lang 
| editing        = Charles Nelson
| studio         = Columbia Pictures 
| distributor    = Columbia Pictures 
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1.25 million (US) 
}}

The Big Heat is a 1953 film noir directed by Fritz Lang, starring Glenn Ford, Gloria Grahame and Lee Marvin. It is about a cop who takes on the crime syndicate that controls his city, after the murder of his wife. The film was written by former crime reporter Sydney Boehm, based on a serial by William P. McGivern, which appeared in the Saturday Evening Post and was published as a novel in 1953. The film was selected for inclusion in the National Film Registry of the Library of Congress in 2011.

==Plot== Lieutenant Ted Wilks (Willis Bouchey), who is under pressure from "upstairs" to close the case.

Chapman is found dead after being tortured and covered with cigarette burns. Bannion investigates, although it is not his case or his jurisdiction. After receiving threatening calls to his home, he confronts Mike Lagana (Alexander Scourby), the local mob boss. Its an open secret that Lagana runs the city, even to the point that cops guard his house while his daughter hosts a party. Lagana is astounded by Bannions accusations in his own home: "Ive seen some dummies in my time, but youre in a class by yourself."

Bannion finds that people are too scared to stand up to the crime syndicate. When warnings to Bannion go unheeded, his car is blown up and his wife Katie (Jocelyn Brando), is killed in the explosion. Feeling that the department will do little to bring the murderers to justice, Bannion resigns and sets off on a one-man crusade to get Lagana and his second-in-command Vince Stone (Lee Marvin).

When Stone viciously "punishes" a girl in a nightclub—by burning her hand with a cigar butt—Bannion stands up to him by ordering Stone and a bodyguard out of the joint, which impresses Stones girlfriend, Debby Marsh (Gloria Grahame). She tries to get friendly with Bannion, who keeps pointing out that she gets her money from a thief. Marsh states: "Ive been rich and Ive been poor. Believe me, rich is better." As soon as Debby unwittingly reminds Bannion of his late wife, he sends her packing, to which she retorts: "Well, youre about as romantic as a pair of handcuffs."

Debby was seen with Bannion. When she returns to Stones Penthouse apartment|penthouse, he accuses her of talking to Bannion about his activities and throws boiling coffee in her face. Debby is taken to hospital by none other than Police Commissioner Higgins, who was playing poker with Stone and his cronies at the flat. Higgins warns that he will have to file a report but Stone reminds the commissioner that he is well-paid to deal with that sort of thing.
  Adam Williams), DA but Mrs. Duncan kept them for herself and is collecting blackmail payments from Lagana.

Told by Debby that killing for revenge would make him no better than Vince Stone, Bannion refrains from killing Gordon, instead spreading the word that Gordon had talked and Gordon is murdered by Stones men. Bannion next confronts Mrs. Duncan, accusing her of betraying Chapman, causing her death and protecting Lagana and Stone "for the sake of a soft plush life." Cops sent by Lagana arrive just in time and Bannion departs when they do.

Stone decides to kidnap Bannions little daughter Joyce (Linda Bennett), who is staying with an aunt and uncle with a police guard nearby. When the police guard is called away at the behest of Lagana, to further the kidnap plot, the uncle calls in a few army buddies for their protection. Satisfied that she is in good hands, Bannion sets off to deal with Stone. On the way he meets Lieutenant Wilks (Willis Bouchey), who is now prepared to make a stand against the mob, admitting that, in spite of concern over what might happen to his pension, "Its the first time in years Ive breathed good clean air."

Debby goes to see Mrs. Duncan. Noting they are both wearing the same expensive coats, Debby remarks that they are "sisters under the mink" and have benefited from an association with gangsters. She kills Mrs. Duncan, starting the process that will see Tom Duncans evidence surface and bring about Stones and Laganas downfall.

Stone returns to his penthouse. Debby throws boiling coffee at him, just as he had done to her. Stone shoots her but after a short gun battle is captured by Bannion, who had followed him to the flat. As Debby lies dying, Bannion describes his late wife to her in terms of their relationship, rather than the physical "police description" he gave earlier: "You and Katie would have gotten along fine," he tells her. Stone is arrested for murder, Duncans evidence is made public and Lagana and Commissioner Higgins are indicted. Bannion returns to his job at Homicide.

==Cast==
* Glenn Ford as Det. Sgt. Dave Bannion
* Gloria Grahame as Debby Marsh
* Lee Marvin as Vince Stone
* Jeanette Nolan as Bertha Duncan
* Alexander Scourby as Mike Lagana
* Jocelyn Brando as Katie Bannion Adam Williams as Larry Gordon, the car bomber
* Kathryn Eames as Marge, Bannions sister-in-law
* Linda Bennett as Joyce Bannion, the Bannions young daughter
* Chris Alcaide as George Rose
* Peter Whitney as Tierney
* Willis Bouchey as Lt. Ted Wilks
* Robert Burton as Det. Gus Burke
* Howard Wendell as Police Commissioner Higgins Michael Granger as Hugo (police clerk)
* Dorothy Green as Lucy Chapman
* Carolyn Jones as Doris
* Dan Seymour as Mr. Atkins
* Edith Evanson as Selma Parker

==Reception==

===Critical response===
The New York Times and Variety (magazine)|Variety both gave The Big Heat very positive reviews. Bosley Crowther of the Times described Glenn Ford "as its taut, relentless star" and praises Lang for bringing "forth a hot one with a sting."  Variety characterized Langs direction as "tense" and "forceful."  Critic Roger Ebert listed the film among his category of "Great Movies"; he praised the films supporting actors.   

Writer David M. Meyer states that the film never overcomes the basic repulsiveness of its hero, but notes that some parts of the film, though violent, are better than the film as a whole. "Best known is Gloria Grahames disfigurement at the hands of über-thug Lee Marvin, who flings hot coffee into her face." 

According to film critic Grant Tracey, the film turns the role of the femme fatale on its head: "Whereas many noirs contain the tradition of the femme-fatale, the deadly spiderwoman who destroys her man and his family and career, The Big Heat inverts this narrative paradigm, making Ford   the indirect agent of fatal destruction. All four women he meets&mdash;from clip joint singer, Lucy Chapman, to gun moll Debby&mdash;are destroyed." 

===Accolades===
In December 2011, The Big Heat was selected for inclusion in the Library of Congress National Film Registry.    Proclaiming it "one of the great post-war noir films", the Registry stated that The Big Heat "manages to be both stylized and brutally realistic, a signature of its director Fritz Lang." 

===American Film Institute Lists===
* AFIs 100 Years...100 Thrills - Nominated 
* AFIs 100 Years...100 Movie Quotes: "Were sisters under the same mink." - Nominated 
* AFIs 10 Top 10 - Nominated Gangster Film 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*   at the Greatest Film web site
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 