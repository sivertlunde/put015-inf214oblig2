His Father's Rifle
{{Infobox film
| name           = His Fathers Rifle
| image          = File:Scene_from_1915_silent_film_His_Fathers_Rifle.jpg
| alt            = 
| caption        = Scene from film
| film name      =  Edward J. Le Saint 
| producer       = 
| writer         =  Joseph F. Poland 
| screenplay     = 
| story          = 
| based on       = 
| starring       = Earle Foxe Bertram Grassby William Howard
| narrator       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Selig Polyscope Company
| distributor    = Selig Polyscope Company
| released       = 1915
| runtime        = 3 reels
| country        = United States Silent
| budget         = 
| gross          = 
}} American silent silent short short drama directed by Edward LeSaint and written by Joseph F. Poland. The film starred Earle Foxe and Bertram Grassby in the main roles. The survival status of the film is presumed to be lost. 

==Plot==
James Birch, an English hunter, is accidentally shot by the servant of Kirke Warren, a wild animal painter who is camping in the jungle. The terrified servant leaves the rifle, which is marked with his masters initials beside the body of the man. Later Warren meets Mrs. Birch, the widow of the unfortunate hunter and is invited to a house party given by her. Here he finds the rifle, which she has kept in hopes of some time discovering the identity of her husbands supposed murderer. Thinking that Warren is the man, she plans vengeance by sending him hunting with the rifle equipped with cartridges a size too large. As a result of these cartridges jamming when Warren is attacked by a lion, he is nearly killed by this ferocious beast. In the meantime, Mrs. Birch becoming conscience stricken, sets out to find the hunting party in order to prevent the catastrophe which she had planned. After losing her way and falling in with a band of hostile Zulus, she is rescued through the efforts of Warren, who though wounded, leads the searching party. While Warren is being nursed back to life, the servant confesses the truth about the shooting. Mr. Warren and Mrs. Birch discover that she and Warren have grown to love one another.
:Motion Picture News (1915)

==Cast==
*Earle Foxe
*Bertram Grassby
*William Howard
*Stella LeSaint
*Guy Oliver

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 