The Beloved Rogue

(For a similar sounding 1929 early talking pirate movie, see The Delightful Rogue)
{{Infobox film
| name           = The Beloved Rogue
| image          = Poster of the movie The Beloved Rogue.jpg
| caption        = Original theatrical poster
| director       = Alan Crosland
| producer       = Joseph M. Schenck
| writer         = Justin Huntley McCarthy (novel & play If I Were King) Paul Bern (adaptation & scenario) Walter Anthony (intertitles)
| starring       = John Barrymore
| cinematography = Joseph H. August
| editing        = Hal C. Kern
| studio         = Art Cinema Corporation/ Feature Productions
| distributor    = United Artists
| released       =  
| runtime        = 10 reels (9,264 ft)
| country        = United States English intertitles
| budget         =
}}
 American silent film, loosely based on the life of the 15th century French poet, François Villon.  The film was directed by Alan Crosland for United Artists.

François Villon is played by John Barrymore, and other cast members include Conrad Veidt as King Louis XI and Marceline Day as Charlotte de Vauxcelles.
 If I Were King with William Farnum. The film was later re-made in the sound era again reverting to its original title If I Were King with Ronald Colman.

==Plot==
 

==Cast==
* John Barrymore—François Villon
* Conrad Veidt—King Louis XI
* Marceline Day—Charlotte de Vauxcelles
* Lawson Butt|W. Lawson Butt—Duke of Burgundy
* Henry Victor—Thibault dAussigny
* Slim Summerville—Jehan
* Mack Swain—Nicholas
* Angelo Rossitto—Beppo the Dwarf
* Nigel De Brulier—Astrologer
* Lucy Beaumont—Villons mother
* Otto Matieson—Olivier (as Otto Mattiesen)
* Jane Winton—The Abbess
* Rose Dione—Margot
* Bertram Grassby—Duke of Orleans
* Dick Sutherland—Tristan lHermite
* Martha Franklin—Maid (uncredited)
* Stubby Kruger -- (uncredited) Dickie Moore—Baby Francois (uncredited)

==Production== Eternal Love (1929) is another UA film Pickford preserved. This surviving Pickford print of The Beloved Rogue represents what a true tinted & toned silent film looks like, made directly on tinted film stock prevalent in the silent era.

==Reception==
John Barrymore viewed the premiere of the film with a large picture palace audience. Unbeknownst to the audience he was standing at the back of the movie house. Barrymore apparently was discontented or bemused with his own performance stating, "...what a ham".

==External links==
 
*  
* 
*  
*   at Kino Video
*   at Silents Are Golden
* 

 

 
 
 
 
 
 
 
 
 