Broadcast News (film)
{{Infobox film
| name = Broadcast News
| image = Broadcast News.jpg
| caption = Theatrical release poster
| director = James L. Brooks
| producer = James L. Brooks
| writer = James L. Brooks
| starring = William Hurt Albert Brooks Holly Hunter
| music = Bill Conti
| cinematography = Michael Ballhaus
| editing = Richard Marks
| studio = Gracie Films
| distributor = 20th Century Fox 
| released =  
| runtime = 133 minutes
| country = United States
| language = English
| budget = $15 million  
| gross = $67,331,309
}}

Broadcast News is a 1987 romantic comedy-drama film written, produced and directed by James L. Brooks. The film concerns a virtuoso television news producer (Holly Hunter), who has daily emotional breakdowns, a brilliant yet prickly reporter (Albert Brooks) and his charismatic but far less seasoned rival (William Hurt). It also stars Robert Prosky, Lois Chiles, Joan Cusack, and Jack Nicholson (billed only in the end credits) as the evening news anchor.

==Plot==
The film revolves around three characters who work in television news. Jane Craig (Hunter) is a talented, neurotic producer whose life revolves around her work. Janes best friend and frequent collaborator, Aaron Altman (Brooks), is a gifted writer and reporter ambitious for on-camera exposure who is secretly in love with Jane. Tom Grunick (Hurt), a local news anchorman who until recently was a sports anchorman, is likeable and telegenic, but lacks news experience and knows that he was only hired for his good looks and charm. He is attracted to Jane, although he is also intimidated by her skills and intensity.

All three work out of the Washington, D.C., office of a national television network. Craig is drawn to Grunick, but resents his lack of qualifications for his new position as news anchor. Altman also is appalled by Grunicks lack of experience and knowledge, but accepts his advice when finally getting an opportunity to anchor a newscast himself. Unfortunately, he lacks Grunicks poise and composure in that seat, his debut as anchor a resounding failure.

Altman acknowledges to Craig that he is in love with her while trying to dissuade her from pursuing a romantic relationship with Grunick. As a massive layoff hits the network, resulting in many colleagues losing their jobs, Altman tenders his resignation while tipping off Craig to a breach of ethics on Grunicks part. She decides she cannot in good conscience get personally involved with Grunick and she no longer has either man in her personal or professional life, at least until the three of them reunite several years later.

==Cast==
*William Hurt as Tom Grunick
*Albert Brooks as Aaron Altman
*Holly Hunter as Jane Craig
*Robert Prosky as Ernie Merriman
*Lois Chiles as Jennifer Mack
*Joan Cusack as Blair Litton
*Peter Hackes as Paul Moore
*Christian Clemenson as Bobby
*Jack Nicholson as Bill Rorish
*Leo Burmester as Janes Dad
*Marita Geraghty as Date-Rape Woman
*Glen Roven as News Theme Writer
*Marc Shaiman as News Theme Writer
*John Cusack as Angry Messenger

==Production==
The score was by Bill Conti. Emmy Award-winning composers Glen Roven and Marc Shaiman make cameo appearances as a dorky musician team who have composed a theme for the news program in the film.

The female lead was originally written for Debra Winger, who worked with James L. Brooks in Terms of Endearment. However, Winger was replaced by Holly Hunter at the last minute because of her pregnancy.   

==Reception==

===Box office===
Broadcast News was given a limited release on December 16, 1987 in seven theaters, and managed to gross USD $197,542 on its opening weekend.  It went into wide release on December 25, 1987 in 677 theaters, grossing $5.5&nbsp;million on its opening weekend. The film went on to make $51.3&nbsp;million in North America and $16.1&nbsp;million in the rest of the world for a worldwide total of $67.3&nbsp;million.   

===Critical response===
Film critic  , wrote, "  Brooks is excellent at taking us inside the world of television, but not terribly good at analyzing it. He has a facile, too-pat approach to dealing with issues; theres still too much of the sitcom mentality at work".    In his review for    and an 84 metascore at Metacritic. 

===Recognition=== Best Picture, Best Actor Best Actress Best Supporting Best Film Best Cinematography (Michael Ballhaus).

American Film Institute
* AFIs 100 Years... 100 Movies - Nominated
* AFIs 100 Years... 100 Laughs - #64
* AFIs 100 Years... 100 Movie Quotes:
** "Ill meet you at the place near the thing where we went that time." - Nominated
* AFIs 100 Years... 100 Movies (10th Anniversary Edition) - Nominated

38th Berlin International Film Festival
* Golden Bear - Nominated
* Silver Bear for Best Actress - Holly Hunter (won)   

==Home media==
A digitally restored version of the film was released on DVD and Blu-ray by The Criterion Collection. The release includes new audio commentary featuring Brooks and Marks, James L. Brooks—A Singular Voice, a new documentary on Brooks’s career in television and film, featuring actresses Marilu Henner and Julie Kavner, among other collaborators, an alternative ending and deleted scenes, with commentary by Brooks, new video interviews with veteran CBS news producer Susan Zirinsky, one of the models for Holly Hunter’s character and an associate producer on the film, and a featurette containing on-set footage and interviews with Brooks, Hunter, and actor Albert Brooks. There is also a booklet featuring an essay by film critic Carrie Rickey. 

==References==
 

==External links==
 
*  
*  
*  
*  
*   at The Numbers
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 