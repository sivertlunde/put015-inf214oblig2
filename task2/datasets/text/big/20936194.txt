The Great Glinka
{{Infobox film
| name           = The Great Glinka (Глинка)
| image          = 
| caption        = 
| director       = Lev Arnshtam
| producer       = 
| writer         = Lev Arnshtam
| starring       = Boris Chirkov
| music          = Vissarion Shebalin
| cinematography = Yu-Lan Chen Aleksandr Shelenkov
| editing        = Tatyana Likhachyova
| distributor    = 
| released       =  
| runtime        = 116 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}}

The Great Glinka ( ) is a 1946 Soviet drama film directed by Lev Arnshtam. It was entered into the 1946 Cannes Film Festival.     

==Cast==
* Boris Chirkov - Mikhail Ivanovich Glinka
* Vasili Merkuryev - Jacob Ulanov Ulyanich
* Mikhail Derzhavin - Vassili Andreyevich Zhukovsky
* Vladimir Druzhnikov - Rileyev
* Katya Ivanova - Anna Petrovna Kern
* Valentina Serova - Maria Petrovna Ivanova Glinka
* Klavdiya Polovikova - Luisa Karlovna Ivanova, her mother
* Pyotr Alejnikov - Pushkin (as Petr Alennikov)
* Nikolai Svobodin - Baron Igor Feodorovich Rozen Aleksandr Sobolev - Glinka as a Child (as Sasha Sobolev)
* Lev Snezhnitsky - Ivan Nikolayevich Glinka (as L. Shnezhnitsky)
* Ye. Kondratyeva - Mother Glinka
* Viktor Koltsov - Vladimir Fedorovich Odoyevsky
* Vladimir Vladislavsky - Count Mikhail Yurelivich Vielgorsky
* Mikhail Yanshin - Petr Andreyevich Vyazensky

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 