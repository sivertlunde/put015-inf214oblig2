Choice of Arms
 

Choice of Arms is a 1981 French film starring Yves Montand, Catherine Deneuve and Gerard Depardieu. It was directed by Alain Corneau.
==Plot==
After escaping from prison, Mickey (Gerard Depardieu) and his elderly mentor Serge face a gangster hit squad and a shootout with the police. 

They retreat to a ranch owned by Noel (Yves Montand), a former underworld figure who knows Serge is now a successful businessman. 

Police arrive looking for the escaped convicts - the laidback chief inspector (Michel Galabru) and his more intense younger colleague (Gerard Lanvin). 
==External links==
* 

 
 

 