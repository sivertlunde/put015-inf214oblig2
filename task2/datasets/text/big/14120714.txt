Escape (1940 film)
{{Infobox film
| name           = Escape
| image          = Poster - Escape (1940) 01.jpg
| image_size     =
| caption        = Lobby card
| director       = Mervyn LeRoy
| producer       = Mervyn LeRoy Lawrence Weingarten (uncredited)
| writer         = Arch Oboler Marguerite Roberts Grace Zaring Stone (novel) (as Ethel Vance)
| narrator       = Robert Taylor Conrad Veidt Alla Nazimova
| music          =
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       = November 1, 1940
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $1,205,000 Glancy, H. Mark "When Hollywood Loved Britain: The Hollywood British Film 1939-1945" (Manchester University Press, 1999)   . 
| gross          = $1,357,000 (Domestic earnings)  $1,007,000 (Foreign earnings) 
| preceded_by    =
| followed_by    =
}} Robert Taylor, Conrad Veidt and Alla Nazimova. It was adapted from the novel of the same name by Grace Zaring Stone.

==Plot==
Famous German stage actress Emmy Ritter (Alla Nazimova) is held in a Nazi concentration camp. She is scheduled to be executed soon, but the sympathetic camp doctor, Ditten (Philip Dorn), has been a fan since childhood and offers to deliver a letter from her to her children...afterwards.

Emmys son Mark Preysing (Robert Taylor), an American citizen, travels to Germany in search of his mother, but nobody, not even frightened old family friends, want anything to do with him. A German official tells Mark that she has been arrested and advises him to return to the United States. 

The postmark of a returned letter guides Mark to the region where she is being held. There, he meets by chance Countess Ruby von Treck (Norma Shearer), an American-born widow, but she also does not want to become involved, at least at first. Then, she asks her lover, General Kurt von Kolb (Conrad Veidt), about Emmy and learns that she has been judged a traitor in a secret trial and sentenced to death.

At a concert, Mark encounters Doctor Ditten, who takes the opportunity to deliver Emmys letter. Then, Ditten drugs Emmy into a coma, making it appear as if she has died. He tells Mark what he has done. Mark sends longtime family servant Fritz Keller (Felix Bressart) to collect the coffin, but the Americans nervousness raises the suspicion of the political police and he is brought to the camp for questioning. Fortunately, he is allowed to take his mothers body away.  

When the road is blocked by snow, Mark is forced to find heat and shelter for his mother at the house of the countess. The next day, he meets von Kolb, who is jealous of the younger man. Later, when Mark and a disguised Emmy leave for the airport, von Kolb guesses what is happening (from Marks earlier lack of reaction to the news of his mothers "death") and confronts the countess. She begs him not to interfere, but he is implacable. Knowing about his health problems, she taunts him with her love of Mark, which provokes him into having a heart attack, giving her new friends time to escape.

==Cast==
*Norma Shearer as Countess Ruby von Treck Robert Taylor as Mark Preysing
*Conrad Veidt as General Kurt von Kolb
*Alla Nazimova as Emmy Ritter
*Felix Bressart as Fritz Keller
*Philip Dorn as Dr. Berthold Ditten
*Albert Bassermann as Dr. Arthur Henning, an unhelpful family friend
*Edgar Barrier as the Commissioner of Police
*Bonita Granville as Ursula, a suspicious student of the Countess
*Elsa Bassermann as Mrs. Henning
*Blanche Yurka as Emmys concentration camp nurse-matron
*Lisa Golm as Anna, Emmys ill cellmate
==Box Office==
According to MGM records the film earned $2,364,000 at the box office resulting in a profit of $345,000. 
==External links==
* 
*  
* 

==Notes==
 

 
 
 
 
 
 
 
 
 
 
 

 