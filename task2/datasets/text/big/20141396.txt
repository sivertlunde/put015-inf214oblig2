Live at Sturgis 2006
{{Infobox film
| name           = Live at Sturgis 2006
| image          = Live at Sturgis 2006.jpg
| caption        =
| director       = Daniel Catullo
| producer       = Jack Gulick, Daniel Catullo & Nickelback
| writer         =
| starring       = Nickelback
| music          =
| cinematography =
| editing        =
| distributor    = Coming Home Studios
| released       =  
| runtime        = 155 minutes
| country        = Canada
| language       = English
| budget         =
| gross          =
}} Live at Dark Horse which had come out on November 18, 2008, a year later. The Blu-ray Disc release of Live At Sturgis was released in late 2009. It was certificated Gold by the RIAA . 

==Track listing==
#"Animals (Nickelback song)|Animals"
#"Woke Up This Morning"
#"Photograph (Nickelback song)|Photograph" Because of You" Far Away" Never Again"
#"Savin Me"
#"Someday (Nickelback song)|Someday"
#"Side of a Bullet"
#"How You Remind Me"
#"Too Bad"
#"Figured You Out"

===Additional content===
*A behind-the-scenes documentary including backstage interviews with the band.
*The music video for "Rockstar (Nickelback song)|Rockstar".
*A Sturgis 101 primer.
*A photo gallery of scenes from the concert.

==References==
 

 

 
 
 
 
 
 

 