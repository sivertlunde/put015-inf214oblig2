Sweet Toronto
{{Infobox film name = Sweet Toronto
  caption = VHS cover of Sweet Toronto director = D.A. Pennebaker producer = Matt Friedman Peter Hansen Mark Woodcock writer =  starring = Alan White Yoko Ono Little Richard Jerry Lee Lewis Bo Diddley Chuck Berry music = cinematography = D.A. Pennebaker Roger Murphy Richard Leacock editing = distributor = BMG released = 1971  runtime = 70 min. language = English
|budget =
|}}
Sweet Toronto (sometimes referred as Sweet Toronto Peace Festival) is a documentary by  , Little Richard, and Bo Diddley. The actual concert lasted twelve hours, but Pennebakers documentary focuses mainly on the final hours of the concert.     At the time of the performance Yoko Onos popularity was sufficiently low that the audience booed and left the Plastic Ono Band performance.  There was a similar response from film reviewers at the time.  The performances "and this film have grown in interest and watchability since that time, particularly given the rarity of such thorough documentation of these key performers work in concert." 

The film is available on DVD from Shout! Factory, under the name John Lennon and the Plastic Ono Band: Live in Toronto.

== Track listing ==
 Bo Diddley"
#* Performed by Bo Diddley Hound Dog"
#* Performed by Jerry Lee Lewis
# "Johnny B. Goode"
#* Performed by Chuck Berry
# "Lucille (Little Richard song)|Lucille"
#* Performed by Little Richard
# "Blue Suede Shoes"
# "Money (Thats What I Want)"
# "Dizzy, Miss Lizzy"
# "Yer Blues"
# "Cold Turkey" 
# "Give Peace a Chance"
# "Dont Worry Kyoko (Mummys Only Looking for Her Hand in the Snow)|Dont Worry Kyoko (Mummys Only Looking for a Hand in the Snow)"
# "John, John (Lets Hope for Peace)"
#* Tracks 5-12 performed by The Plastic Ono Band

The complete sets by Berry, Little Richard and Lewis were subsequently released as their own films.

== References ==

 

 
 

 
 
 
 
 
 
 


 
 