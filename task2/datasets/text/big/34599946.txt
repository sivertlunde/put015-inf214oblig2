The Astronomer's Dream
 
{{Infobox film
| name           = The Astronomers Dream
| image          = The moon from The Astronomers Dream.jpg
| image_size     =
| caption        = The moon
| director       = Georges Méliès
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = France Silent
| budget         =
| gross          =
}}
  1898 France|French short black-and-white silent film, directed by Georges Méliès.

==Plot==
In the observatory, the astronomer is studying at his desk. Satan appears, then a woman appears and makes Satan vanish. Then she disappears. The astronomer draws a globe on a blackboard. The globe develops a sun-like head and limbs and starts to move on the blackboard. The astronomer looks through a small telescope.

The moon appears in a building as a large face. It has eaten the astronomers telescope. Men tumble from its mouth. Then the moon is in the sky. The astronomer, in different dress, stands on a table, which disappears. He falls.

The moon becomes a crescent. A spirit, in the form of a lady, appears from it. The astronomer chases her, but she eludes him.

Now another figure stands in the crescent of the moon, before reclining into its C shape.

The moon appears as a large face again, and the astronomer jumps into its mouth. a woman and Satan appears. The astronomer appears again. Then, in the observatory, the astronomer is sitting asleep in his chair.

==Production Notes==
The film was hand colored like many of Méliès other films.

When this film was imported into the United States by producer Sigmund Lubin in 1899 he re-titled it A Trip to the Moon. However this had no relation to the 1902 film A Trip to the Moon. 

==Current status==
Given its age, this short film is available to freely download from the Internet.

==References==
 

== External links ==
*  
*   on YouTube
*   on YouTube

 
 
 
 
 
 
 
 
 
 

 