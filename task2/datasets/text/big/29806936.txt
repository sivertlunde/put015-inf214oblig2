The Legend of Zelda: The Hero of Time
{{multiple issues|
 
 
 
}}

The Hero of Time is an independent The Legend of Zelda series|Legend of Zelda fan film produced by BMB Finishes, directed by Joel Musch and starring David Blane as Link (The Legend of Zelda)|Link. The film is around two hours long and was first shown at the independent Plaza Theatre (Atlanta) on June 6, 2009. The film was uploaded onto Dailymotion on December 14, 2009, and was subsequently made available as a free downloadable DVD on December 21, 2009. On January 1, 2010, the contract between BMB Finishes and Nintendo that allowed them to showcase the film until the end of 2009 expired, and the films distribution stopped.

The film follows much of the same plot as  , albeit with a heavily altered/truncated storyline.

== Plot == forest named Link is beckoned by his dreams and the sage of the forest, the Great Deku Tree, to venture to Hyrule Castle and seek Princess Zeldas council. The evil Gerudo sorcerer Ganondorf has plotted to overthrow the kingdom by murdering the king and stealing the Triforce, which propels Princess Zelda into hiding. Link endeavors to save them by retrieving the Master Sword in the Temple of Time, consequently sending him 5 years into the future where Ganondorf now rules and threatens the future of the kingdom. The Sage of Time gives Link his power and sends him on his way to defeat Ganondorf by collecting power from the other Four Sages, to restore the Sword of Time to its original state as the Master Sword.

Link journeys to Kakariko Village and encounters Sheik (Legend of Zelda)#Sheik|Sheik, who accosts him for disappearing for five years before revealing that the Sage of Fire resides on Death Mountain. While scaling the Mountain, he meets the sole Goron still living, Dariu, the prince of his people. After saving his life, the Goron helps Link scale the rest of the mountain, and gain access to the area when the Sage of Fire was being held captive. After defeating a dragon, Link travels to Lake Hylia, and defeats a monster resembling a giant Octorok, then to Kakariko Graveyard where he destroys a giant Recurring enemies in The Legend of Zelda series#Bubble|Bubble, then to the Lost Woods, where he destroys a Moblin, freeing the final Sage.

Having re-awoken the Master Sword, Link returns to Hyrule Castle with Sheik, for a final confrontation with Ganondorf. Ganondorfs minions capture Sheik while Link fights Ganondorf in a one-on-one battle, and finally defeats him after becoming seriously injured. Despite protests from Sheik, Link returns to rescue her, and is mortally injured by the guards before dispatching them. While holding Links near-lifeless body in her hands, Sheik reveals her true identity as Zelda, and takes him into the Temple of Time, helping him replace the Master Sword, sealing Ganondorf and the Demons of Element, but also sending Link back five years, effectively saving his life. Link then returns to Hyrule Castle, charging through the guards, to the courtyard where he first met Zelda.

== Characters == Link - The protagonist of the movie. Link is on a quest to save Hyrule and its people from Ganondorf. After he draws the Master Sword from the Pedestal of Time, however, he is sealed away for five years, giving Ganondorf enough time to take over the land. He possesses the Triforce of Courage. When Link awakens, he sets out to undo the wrongdoings of Ganondorf and free Hyrule from Ganondorfs evil reign.
* Ganondorf - The antagonist of the movie. Ganondorf usurps the royal throne and morphs the land of Hyrule into a nightmare-like wasteland. He wishes to possess the Triforce, and will stop at nothing to obtain all three pieces. He possesses the Triforce of Power.
* Princess Zelda - The princess of Hyrule. Zelda possesses the Triforce of Wisdom that Ganondorf desires. Sheik - Princess Zeldas disguise. Sheik is the persona that Zelda takes on when Link awakens from his five year slumber. Impa - Princess Zeldas nursemaid. Impa knows about Zeldas dreams and believes in the prophecy.
*   - Links adoptive mother. She lives in Kokiri Forest with the rest of the Kokiri.
* Dariu - The prince of the Gorons. He is based on Darunia from Ocarina of Time and is one of the last survivors of his people.
* Great Deku Tree - The Sage of Forest. The Great Deku Tree sends Link off on his quest. Talon - The owner of the Epona Inn in Hyrule Castle Town. Talon moves his family to an isolated ranch after Ganondorf usurps the throne of Hyrule. Malon - Talons daughter. She works at the Epona Inn and is saved by Link during Ganondorfs raid on Hyrule Castle.
* King of Hyrule - The ruler of the land of Hyrule. He is betrayed by Ganondorf and killed in the process.
* Links Mother - The mother of Link. Links Mother leaves Link with Saria when he is an infant.
* Sage of Time - The leader of the sages. He gives Link his power and sends him on his way to defeat Ganondorf by collecting power from the other Four Sages. It seems likely that the Sage of Time is based on Rauru.
* Sage of Fire - The guardian spirit of the fire element. She is imprisoned atop Death Mountain. After Link rescues her, she explains that he must free the other three sages to defeat Ganondorf.
* Tomaz - Ganondorfs attendant.
* Honest Ivan - A comic relief character. He owns a shop in Hyrule Castle Town.

== Production ==
Production of the film began in 2005, with the first teaser trailer released in 2006, one year later. Not too soon after, cast and crew for the film began to break away from the project, and it was reportedly canceled. The film was later picked back up in early 2008 and a full trailer was released late that year. Since then, production has been going smoothly, and the film is expected to see a release in 2009 by the end of the year. Since then a new trailer has been released and BMB Finishes has set up a Hotspot program on their website so you can set up a viewing of the movie in your hometown. A promotional video titled "Hero of Time Dance Party" released on YouTube, followed shortly by a musical montage featuring select track excerpts from the film. Another video was posted a few days later revealing the release date for the film: June 6, 2009. The video was released to   as a full length feature film on December 14, 2009, and released to DVD download on December 21, 2009.

As of December 31, 2009, BMB Finishes came to terms with Nintendo to stop distributing the film online for both live viewing and download.

== Reception ==
Critical reception was relatively positive for a fan film. The plot was widely considered to be well-translated from the games, and the music was hailed as "beautiful" and "epic" by many, often cited as the films greatest attribute. The actors for Link and Princess Zelda were well-received, and the action sequences also gained praise. Criticisms for the film included uneven, and sometimes atrocious, acting from the supporting cast, occasional "shoddy" camera work, and "fake-looking" costumes. The film retains a consensus rating of 9.7/10 on FanFilms.net.

== New Projects ==
A year or so later after The Hero of Time was shut down, both producers Joel Musch and David Blane announced their plans for new projects, Ruins and Reckoning and The Story of Michael.

== External links ==
*  
*  
*   

 
 
 
 
 
 