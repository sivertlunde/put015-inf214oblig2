Ivide Kattinu Sugandam
{{Infobox film
| name           = Ivide Kattinu Sugandam
| image          =
| caption        =
| director       = PG Vishwambharan
| producer       =
| writer         = Kaloor Dennis A. Sheriff (dialogues)
| screenplay     = A. Sheriff
| starring       = Jayan Jayabharathi Srividya Sankaradi
| music          = KJ Joy
| cinematography = C Ramachandra Menon
| editing        = VP Krishnan
| studio         = Yuvachetana films
| distributor    = Yuvachetana films
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by PG Vishwambharan. The film stars Jayan, Jayabharathi, Srividya and Sankaradi in lead roles. The film had musical score by KJ Joy.   

==Cast==
*Jayan as Jayadevan
*Jayabharathi as Indu
*Sankaradi as Sreedharan Menon
*Sathaar as Ravi
*Aranmula Ponnamma as Pushkarans grandmother
*Bahadoor as Raman Pilla
*Kuthiravattam Pappu as Pushkaran
*MG Soman as Gopi
*Thodupuzha Vasanthy as Sarojam
*Urmila as Sunitha

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ee malayil || Vani Jairam, Chorus || Bichu Thirumala || 
|-
| 2 || Muthum muthum koruthum || P Susheela, Vani Jairam || Bichu Thirumala || 
|-
| 3 || Neelaaranyam poonthukil || K. J. Yesudas, Vani Jairam || Bichu Thirumala || 
|-
| 4 || Niradeepanaalangal || K. J. Yesudas || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 