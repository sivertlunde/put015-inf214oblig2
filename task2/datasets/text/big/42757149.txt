Imperial Dreams
{{Infobox film
| name           = Imperial Dreams
| image          =  
| alt            = 
| caption        = 
| director       = Malik Vitthal
| producer       = Jonathan Schwartz Andrea Sperling Katherine Fairfax Wright
| writer        = Malik Vitthal Ismet Prcic
| screenplay     = 	 Rotimi Akinosho Keke Palmer Glenn Plummer DeAundre Bonds
| music          = Flying Lotus
| cinematography = Monika Lenczewska	
| editing        = Suzanne Spangler
| studio         = Super Crispy Entertainment Sundance Channel
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English 
}}
Imperial Dreams is an American drama film written and directed by Malik Vitthal.   The film had its world premiere at 2014 Sundance Film Festival on January 20, 2014.   It won the Audience Award at the festival.  

==Plot==
A 21-year-old reformed gangsters devotion to his family and his future is put to the test when he is released from prison and returns to his old stomping grounds in Watts, Los Angeles.

==Cast==
*John Boyega as Bambi Rotimi Akinosho as Wayne
*Keke Palmer as Samaara
*Glenn Plummer as Uncle Shrimp
*DeAundre Bonds as Gideon
*Sufe Bradshaw as Detective Gill
*Jernard Burks as Cornell
*Anika Noni Rose as Miss Price
*Maximiliano Hernández as Detective Hernandez

==Reception==
 .]]
Imperial Dreams received positive reviews from critics. Review aggregator Rotten Tomatoes reports that 83% of 6 film critics have given the film a positive review, with a rating average of 7.5 out of 10. 

Geoffrey Berkshire of Variety (magazine)|Variety, in his review called the film "Bighearted yet surprisingly nuanced."  Justin Lowe in his review for The Hollywood Reporter praised the film by saying that "An assured debut that stands to connect with a diverse audience."  Chase Whale of Indiewire grade the film B+ by saying that "John Boyega first wowed audiences with his dynamite performance in Attack the Block. Once again playing the anti-hero, Imperial Dreams is another victory lap for this young actor, whos going to go on to do big, big things." 

==References==
 

==External links==
*  
*  

 
 
 
 
 