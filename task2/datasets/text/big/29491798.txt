Mickey's Disguises
{{Infobox Film
| name           = Mickeys Disguises
| image          = 
| image_size     = 
| caption        = 
| director       = Jesse Duffy
| producer       = Larry Darmour
| writer         = Fontaine Fox
| narrator       = 
| starring       = Mickey Rooney Billy Barty Jimmy Robinson Delia Bogard Marvin Stephens Douglas Fox
| music          = 
| cinematography = 
| editing        = 
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 18 minutes
| country        = United States 
| language       = English
| budget         = 
}}
 Mickey McGuire series starring a young Mickey Rooney. Directed by Jesse Duffy, the two-reel short was released to theaters on July 17, 1933 by RKO Radio Pictures.

==Synopsis==
After being accused of stealing chickens, Hambones Uncle Nemo gets arrested. Mickey, Hambone, and the Gang believe that Nemo is innocent, and form a detective agency in order to catch the real thief. The kids suspect Mayor Davis chauffeur; its up to Billy (in a chicken costume) to catch the real crook.

==Notes==
The short marked the final appearances of Delia Bogard and Douglas Fox in the series. Following this film, Shirley Jean Rickert would replace Bogard in the role of Tomboy Taylor, and Douglas Scott would replace Fox in the role of Stinkie Davis.

==Cast==
*Mickey Rooney - Mickey McGuire
*Billy Barty - Billy McGuire Jimmy Robinson - Hambone Johnson
*Marvin Stephens - Katrink
*Delia Bogard - Tomboy Taylor Douglas Fox - Stinkie Davis Spencer Bell - The chauffeur

== External links ==
*  

 
 
 
 
 
 
 


 