Tears of the Black Tiger
{{Infobox film
| name = Tears of the Black Tiger
| image = Tearsposter.jpg
| caption = The Thai movie poster.
| director = Wisit Sasanatieng
| producer = Pracha Maleenont Brian L. Marcar Adirek Wattaleela Nonzee Nimibutr
| writer = Wisit Sasanatieng
| narrator =
| starring = Chartchai Ngamsan Stella Malucchi Supakorn Kitsuwon Sombat Metanee
| music = Amornbhong Methakunavudh
| cinematography = Nattawut Kittikhun
| editing = Dusanee Puinongpho
| distributor = Five Star Production Magnolia Pictures
| released = Thailand: September 28, 2000 United States (limited): January 12, 2007
| runtime = 110 minutes
| country = Thailand
| language = Thai
| budget =
| preceded_by =
| followed_by =
}} the heavens Thai Western western film provincial governor, romantic melodramas of the 1950s and 1960s.

The film was the first from Thailand to be selected for competition at the 2001 Cannes Film Festival,           where it was critically hailed.       It was screened at several other film festivals in 2001 and 2002, including the Vancouver International Film Festival, where it won the Dragons and Tigers Award for Best New Director.    It also won many awards in Thailand for production and costume design, special effects and soundtrack.
 revisionist westerns of Sergio Leone and Sam Peckinpah.  It has also been compared to the works of such directors as Douglas Sirk, John Woo, Jean-Luc Godard, Sam Raimi and Quentin Tarantino.       

Miramax Films purchased the film for distribution in the United States, but changed the ending and then shelved it indefinitely. In 2006, the distribution rights were obtained by Magnolia Pictures, which screened the original version of the film in a limited release from January to April 2007 in several US cities.     

== Plot ==
 lotus swamp. She gazes at a photo of the man for whom she waits.

The young man, whose name is Dum, is with another gunman named Mahesuan, who defies Yoi, an enemy of their boss Fai, to come out of a house. Dressed all in   fashion.
 engaged to arranged by her father, the governor of Suphanburi Province.

Mahesuan is bitter about his status as second fiddle to Dum. Until Dum came along, Mahesuan was the top gunman in the outlaw gang headed by the ruthless Fai. Mahesuan goes looking for Dum and finds him playing a harmonica. Mahesuan mocks the Black Tigers sentimentality and challenges him to a duelling|gunfight. The quick-drawing Dum fires first, yet Mahesuan is uninjured. A decapitated snake drops from an overhanging tree branch onto Mahesuans cowboy hat. Dum targeted the venomous snake, saving Mahesuans life.

Retrieving his harmonica, Dum thinks back to his childhood 10 years earlier during the Second World War, when Rumpoey and her father left the city to stay on Dums fathers farm in rural Thailand.
 hanged herself. Rumpoey weeps over this tragedy.

On the way home, they bump a boat carrying Koh and two other boys, who taunt Rumpoey. Dum defends Rumpoey, and Koh cuts Dums forehead with a paddle, and his toadies overturn Dums boat. Dum rescues Rumpoey but is late in bringing her home. So his father, commanding, "Never again!" severely beats the boys back with a rattan cane. Rumpoey, feeling sorry for getting Dum into trouble, gives him a harmonica to replace the flute she smashed.
 wild west of Suphanburi Province. Taking leave of his rather cold fiancée, Captain Kumjorn takes a small, framed portrait of Rumpoey, warmly promising to guard her photo with his life.
 Buddhist temple, blood oath Buddha statue. Mahesuan swears loyalty to Dum, "If I break this oath, may his gun take my life."

A betrayer guides Kumjorns police forces to Fais hide-out, and the outlaws run out of ammunition. The police are charging in to mop up, when Dum and Mahesuan arrive on a cliff overlooking the battle. The pair break out rocket-propelled grenades and wipe out the police.

Fai imprisons Kumjorn in a cabin and orders Dum to execute him. Kumjorn pleads with Dum to tell his engagement|fiancée of his fate and hands Dum the framed photo of his beloved. Dum is stunned to see the portrait of Rumpoey. Mahesuan enters to find Kumjorn gone and Dum with a knife in his chest.
 high born for a serious relationship with him. Later, Rumpoey is attacked by Koh and two toadies. Dum appears on the path and routs Rumpoeys attackers, for which he is expulsion (academia)|expelled. Rumpoey finds Dum walking and insists on giving him a ride in her car. She orders her driver to take them to Bang Pu beach. Dum and Rumpoey confide their love for each other and are engaged, agreeing to meet a year later at Sala Awaiting the Maiden.

Arriving home in Suphanburi, however, Dum finds his family murdered. With his dying breath, Dums father blames Kong, and Dum swears vengeance. Dum shoots Kong and several of his clan with his fathers lever-action carbine, but is chased into the forest. With one cartridge left, Dum turns the gun on himself, but is stopped by Fai, who has ridden up with his horsemen. Fai recognizes the rifle, saying he had given it to Dums father years before. Fai hands Dum a loaded revolver and tells him to execute the men who murdered his father. Dum is now an outlaw.
 hang herself but is stopped by her nanny.

Fai, meanwhile, plans to attack the governors mansion on the wedding night. Dum warns of the multitude of police who will be attending the Captains nuptials, but Fai declares a love of danger.

On their way to attack the governors mansion, Mahesuan tricks and disarms Dum, whom he accuses of intentionally freeing Kumjorn. A co-conspirator congratulates Mahesuan on robbing the Tiger of his teeth.
 white suit, appears at the wedding, wishes groom and bride a happy life, and warns Kumjorn of Fais plan to attack. Kumjorn, however, tries to shoot the man he knows as the "Black Tiger", who is his rival for Rumpoeys affection.

Just ahead of the outlaw gangs assault, Mahesuan sneaks into the mansion, where he finds Rumpoey and knocks her unconscious.

Fais attack kills many police officers. Fai enters the mansion, but the governor drives a bayonet through his chest and then shoots him dead.

Mahesuan is carrying Rumpoey across the lawn, when he meets Dum, who demands a rematch gunfight. The men fire simultaneously. Mahesuans bullet is deflected harmlessly, but Dums fated bullet rips through Mahesuans teeth. Dum drops his weapon and tends to Rumpoey.
 life is suffering and a chasing after brief moments of hope.

== Cast ==

* Chartchai Ngamsan as Dum Dua, or Seua Dum (Black Tiger)
* Suwinit Panjamawat as Dum Dua (youth)
* Stella Malucchi as Rumpoey Prasit
* Supakorn Kitsuwon as Mahesuan
* Arawat Ruangvuth as Police Captain Kumjorn
* Sombat Metanee as Fai
* Pairoj Jaisingha as Phya Prasit (Rumpoeys father)
* Naiyana Sheewanun as Rumpoeys maid
* Kanchit Kwanpracha as Kamnan Dua (Dums father)
* Chamloen Sridang as Sergeant Yam

== Production ==

=== Origins ===
 revisionist westerns Thai cultural Thai films of the 1950s made by pioneering director Rattana Pestonji, whose films Wisit had viewed in screenings at the Thailand National Film Archive.
 Thai audiences Thai movies, especially the old ones, which they consider nam nao", he said, using the Thai language euphemism for the old films, which are viewed as stagnant and clichéd. Literally, nam nao means "stinky water."     

"What I saw in them was a way to stay true to the spirit of those old styles of Thai filmmaking, as well as a way to make them new again. And none of the older generation of filmmakers impressed me more than Rattana Pestonji." 
 Tom Yum Goong cowboys because at one time cowboys were very popular in Thai films as you can see in Mitr Chaibanchas films."   

Still more influences include the novels of Thai humorist Por Intharapalit and an old Thai pop ballad, "Fon Sang Fah" ("When the Rain Bids the Sky Farewell"). "I do love those rain songs. I kept picturing a beautiful frame of two guys shooting each other in the rain. And that sparked it all", Wisit said in an interview for the films production notes.  Initially Fon Sang Fah was to be the title of the film, but eventually Fah talai jone (literally "the heavens strike the thief") was chosen because the name has different meanings depending on the context. In addition to being the Thai name for an herb, Andrographis paniculata, Fah talai jone "can convey either a sense of obsoleteness or the feel of great chic (style)|chic", the director said.  "In terms of the film it refers to predestination, in which most Thais believe. To put it frankly, the main reason is simply because I liked the name." 

=== Production design, lighting, processing ===
 Jan Dara, also by Nonzee.   

  Thai culture. For example, the first gun battle between Mahesuan and Dum is set on what is obviously a sound stage with a painted theatrical scenery|backdrop, a setting that is similar to Dance of Thailand|likay, a Thai form of folk opera.

"I wanted the audience to feel like theyre reading a novel with moving illustrations", Wisit said. "Its pure imagination and completely unrealistic. I wanted to try and go back to our roots. I wanted to make a link between the traditional and the contemporary in our own style."   
 saturated colors Oxide Pang, working as a telecine colorist, won a special effects award in Thailand for his work.   

Wisit was able to experiment with the set design and lighting effects in a commercial he directed for Wrangler Jeans, which featured the films leading man, Chartchai Ngamsan, as a boxer.     

=== Casting and promotion ===

Most of the cast were relative newcomers, whom the director said he chose because he felt established stars would not be able to handle the old-style dialog. Chartchai Ngamsan and Supakorn Kitsuwon had previously had supporting roles in Dang Bireleys and Young Gangsters. Italian-born, Thailand-raised model and actress Stella Malucchi was acting in a music video in Bangkok, which was noticed on television by Wisit. He then sought out Malucchi, saying he thought she had the right look for the part of Rumpoey.     He told her later she reminded him of Elizabeth Taylor.  Through costuming and makeup, Malucchi, a "plain farang", in the words of Wisit,  was transformed into the daughter of a Thai noble family. There are experienced actors in the cast as well, including Sombat Metanee and Naiyana Sheewanun, who worked in the era of Thai filmmaking that Wisit was trying to recreate.
 serial novels cinemas in Thailand. Wisit designed movie posters and print ads that emulated the style of Thai film posters from the 1950s and 1960s.    

== Reception ==

 

=== Festivals and awards ===
 Thai cinemas. Bangkok Critics Entertainment News Oxide Pang for best special effects.  Tears of the Black Tiger was referenced in another Thai film, Monrak Transistor, in 2001. The comedy by director Pen-ek Ratanaruang starred Supakorn Kitsuwon, who co-starred in Tears as Mahesuan. At the 2006 Bangkok International Film Festival, Tears of the Black Tiger was screened as part of a tribute to Sombat Metanee, who portrays the outlaw leader, Fai.     
 Cannes Film Bucheon International Fantastic Film Festival it won a jury prize.    Other festival appearances included the Seattle International Film Festival and Edinburgh International Film Festival in 2001 and the Sundance Film Festival, International Film Festival Rotterdam, Deauville Asian Film Festival and Moscow International Film Festival in 2002.   

=== Critical reception ===

On Rotten Tomatoes, the film has a 76% fresh rating, based on 58 reviews.    On Metacritic, it has a score of 69/100, based on 19 reviews, for a "generally favorable" rating.    Critics were "wowed" by the film at the 2001 Cannes Film Festival, according to Peter Bradshaw of The Guardian, who termed the film a "Stir frying|stir-fry horse opera   uproarious high-camp cowboy drama." 
 The Quick and the Dead. He said the "outlandishly painted backdrops and garish acid colors" reminded him of old Asian movie posters. "The overall effect is hallucinatory, as if were experiencing someone elses druggy dream."   
 blood oath to one another under the shadow of an impassive Buddha." 
 Tarantino jealous." 

However,  , Crouching Tiger, Hidden Dragon. 
 New York Magazine compared the film to 1940s low-budget westerns of Lash LaRue, Douglas Sirk melodramas, the heroic bloodshed films of John Woo and George A. Romeros gore-filled horror films. "Its no buried postmodern masterpiece, but it certainly is a jaw-dropper: a delirium-inducing crash course in international trash", Edelstein wrote. 
 macho posturing of Chartchai Ngamsans Dum, and the underlying homoeroticism of the characters relationship with Mahesuan, portrayed by Supakorn Kitsuwon. "  may be the love of Dums life, but there is far more heat and intimacy in his relationship with Mahesuan."   
 camp film and as a cult film, saying "the best B-movies are both."   

== Purchase by Miramax, alternate versions ==

International sales rights to Tears of the Black Tiger were purchased by Fortissimo Films, which marketed a 101-minute "international cut", edited by director Wisit Sasanatieng from the original 110-minute length. The shorter version omits some transitional scenes in order to streamline the pacing of the film.      This version was released theatrically in several countries, including France, the Netherlands and the United Kingdom. 

Among the deleted scenes are those involving the comic relief character, Sergeant Yam, Rumpoeys engagement to Captain Kumjorn and other transitional scenes.   

Fortissimo sold the US distribution rights to Miramax Films during the 2001 Cannes Film Festival.    Miramax then sent word that it wanted to alter the film. Wisit offered the company an even shorter version than the international cut, but the company refused. "They didnt allow me to re-cut it at all", Wisit said in an interview with the Los Angeles Times. "They did it by themselves and then sent me the tape. And they changed the ending from tragic to happy. They said that in the time after September 11, 2001 attacks|9/11, nobody would like to see something sad."    
 Harvey and Bob Weinstein, who defended their actions by saying the films needed editing to make them marketable to American audiences. Other examples were the Miramax releases of Shaolin Soccer and Hero (2002 film)|Hero. "Im not cutting for fun", Harvey Weinstein said in an interview. "Im cutting for the shit to work. All my life I served one master: the film. I love movies."   

The Miramax version was screened at the Sundance Film Festival in 2002. The company then shelved the film, fearing it would not do well in a wider release. 

This was another routine by the Weinsteins, who delayed releases so they could shift potential money-losing films to future fiscal years and ensure they would receive annual bonuses from Miramaxs corporate parent, The Walt Disney Company.   
 Holy Grail" for film fans.         For viewers in the US, the only way to watch it was to purchase the DVD from overseas importers, however some of those versions of the film had also been heavily edited.  

In late 2006, Magnolia Pictures acquired the films distribution rights from Miramax. Magnolia screened the original version of the film in a limited release from January to April 2007 in several US cities.       

== DVD releases ==
 Region 3 release, also with English subtitles, had scenes involving graphic violence cut. The Region 2 release, marketed in Europe by Pathé and in Region 4 by Madman Entertainment is the 101-minute "international cut". 

The first DVD release for Region 1 was on April 24, 2007 by Magnolia Pictures, which acquired the original, uncut version of the film.    

== Soundtrack ==

{{Infobox album
| Name        = Tears of the Black Tiger
| Type        = soundtrack
| Longtype    =
| Artist      = various artists
| Cover       = Fahtalaijonesoundtrackcover.jpg
| Released    = 2000
| Recorded    = pop
| Length      = 38:20
| Label       = BEC-TERO|BEC-TERO Entertainment
| Producer    =
| Last album  =
| This album  =
| Next album  =
}}
{{Album ratings|title=Soundtrack
| rev1      =  
| rev1Score =   }}
Just as Tears of the Black Tiger has been compared to the Spaghetti Westerns of director Sergio Leone, the music in the film has been likened to the scores Ennio Morricone composed for Leones films. 
 heard in Thailand in the 1940s and 1950s. Among the songs is the 1940s Thai pop ballad, "Fon Sang Fah" ("When the Rain Bids the Sky Farewell").  There is also "Mercy", composed by 1940s Thai bandleader and jazz violinist Eua Sunthornsanan, which features whimsical fiddle playing and whistling. However, the lyrics to the song, written by Leud Prasomsap, offer a contrast to the mood evoked by the jaunty tune:

: What a miserable life, so alone
: No one cares for me
: Im so alone, so lonesome I could die.   

A traditional song, "Kamsuanjan" ("The Moon Lament"), was arranged with new lyrics by Wisits wife, Siripan Techajindawong. She and arranger Sunthorn Yodseethong won the Phra Suraswadee ("Golden Doll") prize for best song from the Entertainment News Reporters Association of Thailand.  The songs tune was taken from Thomas Moores The Last Rose of Summer. Its lyrics was partly inspired by the Moores as well.

=== Track listing ===

A soundtrack CD was issued around the time the film was released. The first half of the CD is songs with vocals. The songs are then repeated as instrumentals.

# "Mercy" – composed by Leud Prasomsap and Eua Sunthornsanan; performed by Veera Bamrungsri (3:01)
# "Kamsuanjan" ("The Moon Lament") – traditional, lyrics by Wisit Sasanatieng and Siripan Techajindawong; performed by Yaowaret Methakhunnawut (3:23)
# "Fon Sang Fah" ("When the Rain Bid the Sky Farewell") – composed by Salai Krailoed and Suthin Thesarak; performed by Kamonwan Thasanon (2:49)
# "Prom Likit" ("Pre-Destiny")– composed by Kaew Achariyakun and Wet Sunthonjamon; performed by Niwat Charoenmit (2:55)
# "Beautiful Beach" – composed by Sakon Mitranon and Sanae Komarachun; performed by Kamonwan Thasanon (3:10)
# "Splendid Night Sky" – composed by Kaew Achariyakun and Eua Sunthornsanan; performed by Yaowaret Mathakhunnawut (3:04)
# "Mercy" – composed by Eua Sunthornsanan (3:01)
# "Kamsuanjan" ("The Moon Lament") – traditional (3:24)
# "Fon Sang Fah" ("When the Rain Bid the Sky Farewell") – composed by Suthin Thesarak (2:51)
# "Destiny" – composed by Wet Sunthonjamon (2:55)
# "Beautiful Beach" – composed by Sanae Komarachun (3:11)
# "Splendid Night Sky" – composed by Eua Sunthornsanan (3:04)
# "Horse Riding" (1:04)   

== References and notes ==
 

== External links ==
 
*   at Magnolia Pictures
*   at Film Bangkok  
*  
*  
*  
*   at the Movie Review Query Engine
*  
*  
*  
*   at CityOnFire.com
*  

 

 
 
 
 
 
 
 
 
 
 