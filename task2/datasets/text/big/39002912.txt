Kattabomman (film)
 
{{Infobox film
| name           = Kattabomman
| image          = 
| image_size     =
| caption        = 
| director       = Manivasagam
| producer       = Rajeswari Manivasagam
| writer         = Manivasagam
| starring       =   Deva
| cinematography = K. B. Dhayalan
| editing        = L. Kesavan
| distributor    = Raja Pushpa Pictures
| studio         = Raja Pushpa Pictures
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 1993 Tamil Tamil comedy-drama Sarath Kumar Deva and was released on 13 November 1993 as a Deepavali release .  

==Plot==
 Uday Prakash) Sarath Kumar) is an angry man who cannot tolerate injustice. He was brought up by his grandfather (Nagesh) and his widowed mother. His family and Kalingarayans family are in feud since several years. Kattabomman falls in love with Kalingarayans daughter Priya (Vineetha) and he gets married with her without their familys wishes. In angry, his grandfather tells their past. What transpires later forms the crux of the story.

==Cast==
 Sarath Kumar as Kattabomman
*Vineetha as Priya
*Nagesh as Kattabommans grandfather
*Srividya as Devada, Kattabommans sister-in-law
*Goundamani as Subramani Senthil as Pazhani Vijayakumar as Ponnurangam, Kattabommans father
*Sakthivel as Kalingarayan
*Kavitha as Saroja, Kalingarayans wife Uday Prakash as Rajappa
*Poonam Dasgupta as Rani
*Prasanna
*S. N. Lakshmi
*Tirupur Ramasamy as Ramasamy 
*Jayamani

==Soundtrack==

{{Infobox Album |  
| Name        = Kattabomman
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1993
| Recorded    = 1993 Feature film soundtrack |
| Length      = 22:19
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1993, features 5 tracks with lyrics written by Kalidasan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Enga Thanga Pandi || Malaysia Vasudevan, Swarnalatha  || 4:27
|- 2 || Koondaivittu || K. J. Yesudas, P. Susheela || 4:42
|- 3 || Palaivanathil || S. P. Balasubrahmanyam, K. S. Chithra || 5:05
|- 4 || Priya Priya || S. P. Balasubrahmanyam, K. S. Chithra || 5:13
|- 5 || Thulasi Chediyoram || S. Janaki || 4:31
|}

==References==
 

 
 
 
 
 