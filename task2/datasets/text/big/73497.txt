From the Manger to the Cross
{{Infobox film
| name           = From the Manger to the Cross
| image          = From the Manger to the Cross.jpg
| caption        = Theatrical poster 
| image size     =
| producer       = Frank J. Marion
| director       = Sidney Olcott
| writer         = Gene Gauntier
| starring       = Robert Henderson-Bland Gene Gauntier Alice Hollister Robert G. Vignola
| music          =
| cinematography = George K. Hollister
| editing        =
| country        = United States Kalem
| released       =  
| runtime        = 71 minutes Silent (English intertitles)
}}
  Virgin Mary.

The film received excellent reviews at the time of its original release. After Vitagraph Studios acquired Kalem, the film was re-released in February 1919. 

==Production background==
According to Turner Classic Movies, the film cost $35,000 to produce (roughly between $1,600,000 and $3,300,000 adjusted to 2007 dollars)  and its profits eventually amounted to almost $1 million (roughly $46,000,000 to $95,000,000). TCM host Robert Osborne and the National Film Preservation Foundation consider this film to be the most important silent film to deal with the life of Christ. In 1998, the United States Library of Congress deemed the film "culturally, historically, or aesthetically significant" and selected it for preservation in the National Film Registry.

In later years, Louis B. Mayer, head of Metro-Goldwyn-Mayer, would say this was the premiere film for his movie theater in Haverhill, Massachusetts and a major boost for him in the movie business.  However, most sources place the release date of this film as 1912, long after the opening of Mayers theater. 

== Cast ==
* , the man
*Percy Dyer  :  the Boy Jesus
* 
* 
*Sidney Olcott  :  Blind Man
* 
* 
* 
* 

==References==
 

==External links==
 
* 
* 
*  
*  full movie on the Internet Archive
*    website dedicated to Sidney Olcott
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 