Docteur Jekyll et les femmes
{{Infobox film
| name           = Docteur Jekyll et les femmes
| image          = Docteur-Jekyll-et-les-femmes.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Walerian Borowczyk
| producer       = {{plainlist|
*Jean-Pierre Labrande
*Ralph Baum
*Robert Kuperberg }}
| writer         = Walerian Borowczyk
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Patrick Magee Howard Vernon Clement Harari
| music          = Bernard Parmegiani
| cinematography = Noël Véry
| editing        = Kadisha Bariha
| studio         = {{plainlist|
*Whodunit Productions
*Allegro Productions
*Multimedia Gesellschaft für audiovisuelle Information mbH }} 
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = {{plainlist|
*France
*West Germany   }}
| language       = 
| budget         = 
| gross          =
}}
 Patrick Magee and Howard Vernon.

The film was released in France in 1981 and won the award for "Best Feature Film Director" at the 1981 Sitges Film Festival for Borowczyk.

==Plot==
 
Dr. Henry Jekyll (Udo Kier), where the doctor is being feted prior to his engagement to the austere Miss Fanny Osborne (Marina Pierro). The guests arrive and are various dignitaries and officials.  After a meal, the doctor is summoned to his laboratory, to get his will. He returns to the living room when a scream is heard where one of his guests has been discovered raped and murdered.

Henry Jekyll transforms to his alter ego by taking a bath filled with a chemical cocktail. He emerges physically transformed. His alter ego has none of the restrictions of morality and he proceeds to rape and torture various guests.

Eventually Fanny witnesses one such transformation. She leaps into the bath to be transformed as well. The two transformed leave the house and in an carriage they depart, whilst undertaking blood letting of each other, and love making.

==Release==
Borowczyk wanted to call his film Le cas étrange de Dr.Jekyll et Miss Osbourne but his producers insisted it be released under the title Docteur Jekyll et les femmes.  The film was released in France on June 17, 1981.  Docteur Jekyll et les femmes never opened commercially in the United States and in Britain it played at one cinema for one week. {{Cite AV media notes
| title       = Contemporary Reviews
| year        = 2015
| page        = 20
| type        = booklet
| publisher   = Arrow Films
| id          = AV005
}} 

The film was released theatrically in the US and UK under the title Blood of Dr. Jekyll and then later on video as Bloodlust.  On the films presentation at the Sitges Film Festival, it was shown under the title Docteur Jekyll et Miss Osborne.  Arrow films released the film on April 21, 2015 first time on Blu Ray  in the United States. 

==Reception==
The film won Walerian Borowczyk the award for "Best Feature Film Director" at the 1981 Sitges Film Festival.   

==Notes==
 

===References===
*{{cite book
 | last= Atkinson
 | first= Michael
 | title= Exile Cinema: Filmmakers at Work Beyond Hollywood
 |publisher= SUNY Press
 |year= 2008
 |isbn= 0-7914-7378-3
}}

==See also==
* List of French films of 1981
* List of German films of the 1980s
* List of horror films of 1981

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 