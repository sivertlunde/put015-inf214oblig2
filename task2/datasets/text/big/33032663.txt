Dan Morgan (film)
 
 
{{Infobox film
  | name     = Dan Morgan
  | image    = 
  | caption  = 
  | director = Alfred Rolfe   		
  | producer = Charles Cozens Spencer
  | writer   = 
  | based on = 
  | starring = Alfred Rolfe "and company"    Stanley Walpole
  | music    = 
  | cinematography = 
  | editing  =  Spencers Theatrescope Company
  | distributor = 
  | released = 22 May 1911   
  | runtime  = 3,500 feet  or 2,700 feet 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}
 Dan Morgan.

It was said to be starring "Alfred Rolfe and company".  Rolfe directed three movies for Spencer, all starring himself and his wife Lily Dampier so there is a chance he may have directed this one and that is starred his wife. A prospectus for the Australian Photo Play Company said he directed it. 

It is considered a lost film. 

==Synopsis==
The film consists of a series of episodes from the life and criminal career of bushranger Dan Morgan, leading up to his violent death at the hands of the police.

It started with Morgans dismissal on a station, after which he steals his former employers horses and begins his criminal career. He murders several people, including policemen and mailmen. 

In the words of the Sydney Morning Herald, "it is a candid tale of a cruel, evil life. Dan Morgan is not made into a hero, but something quite the opposite. His robberies and cold blooded murders inspire horror and leave no room for regret when he dies riddled with bullets." 

According to the Bathurst National Advocate, Morgan "shot people with as much compunction as a small boy would shoot flies. Consequently, it was with great relief that those present witnessed him get his just desserts – shot by police as he was in the act of crossing a stream by means of a fallen tree." 

The chapter headings were:
*dismissed for laziness;
*his first crime;
*horse stealing
*Kirby joins Morgan;
*murder of gold buyer and a constable;
*discovery of murder by mail man;
*Morgan shoots Kirby to effect his own escape;
*burning of the homestead;
*sticking up the mail coach;
*the beginning of the end;
*the wages of sin. 

==Cast==
*Alfred Rolfe "and company" 
*Stanley Walpole as old stockman

==Production== Captain Starlight.  Unlike those, it appears Dan Morgan was written originally for the screen and not adapted from a play or novel. Production took place at a time when there were rising fears about the negative influence of bushranger films on the general public. Advertising tried to counterbalance this, claiming:
 In the past some films descriptive of bushranging may have been inclined to create a fale impression in the minds of young Australia. Mr Spencer has produced at great expense the true story of Dan Morgan, the notorious Australian outlaw, making no attempt to glorify his doings or palliate the heiniousness of his crime, but presenting the subject in such a way as will point a strong moral lesson, and show the ultimate fate of all evil-doers, for the wages of sin is death.  
Theatre actor Stanley Walpole made his movie debut in the cast. 

==Reception==
The film was premiered at Spencers Lyceum Theatre in Sydney on 17 May 1911 and ran until June.  It then played other cinemas and country areas, although does not appear to have received the wide release enjoyed by Spencers earlier bushranger movies. As was standard practice at the time, the film was usually screened with an actor commenting on the action. 

===Critical===
Critical reaction was positive  with the reviewer from the Sydney Morning Herald calling it:
 A thrilling series relating to the life of the notorious outlaw Dan Morgan, which was well received by a large audience. The picture-play shows realistically the exploits of that criminal, amidst surroundings of some beautiful Australian bush scenery, while the photographic quality is quite equal to anything yet accomplished from Mr Spencers factory.  
The Sunday Times said "in the technical sense the pictures are admirable, and the reproductions of bush scenery are wonderfully well done." 

The Sydney Sportsman wrote that "the story abounds In thrilling episodes... with beautiful typical Australian scenery throughout. The quality generally is up to the high standard of all Mr. Spencers productions."  The same paper later approved of the depiction of the police saying they were finally "receiving something in the nature of fair play. As in true life, they triumph over the bushrangers without being placed in the ignoble and ridiculous position of incapacity in which modern dramatists have been so prone to depict them." 

The critic from the Sydney Referee said the movie had:
 Realism without so much as a suggestion of romance. To phrase the matter bluntly, the new film reproduces some of the sensational incidents in the infamous career of a fierce outlaw, who stained his hands with blood. The glorification of Australian bushrangers in moving pictures having been condemned on all sides, Mr. Spencer claims that no harm can be done if, as in this instance, the true character of the outlaw is presented. Still, it is a point for argument whether the doings of bushrangers are proper subjects for exhibition when there is a danger of the young receiving bad impressions at a picture show. No good purpose is served by illustrating the criminal side of life in Australia. Mr. Spencers artist has apparently taken the raw official records of the heartless Dan Morgan and constructed a picture story in all its natural horrors. One is shown a series of sensational encounters, in which Morgan murders his colleagues in cold blood, shoots wholesale his pursuers from behind safe shelter, and robs indiscriminately throughout his brief career, with never more than a natural suggestion of wavering courage on the part of other people. The last stand of the bushranger, when he is shot by a dozen bullets, closes the story. In the technical sense, the Dan Morgan pictures are admirable.    

===Box Office===
It appears the movie was a success at the box office.  
 
 bushrangers were banned in New South Wales soon after and Charles Cozens Spencer made no more movies in that genre. 

==References==
 

==External links==
*  
*  at AustLit

 
 

 
 
 
 
 