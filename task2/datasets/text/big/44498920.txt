America Ladina
{{Infobox film
 | name           = America Ladina
  
 | caption        = 
 | director       = Yaron Avitov
 | producer       =  Yaron Avitov
 | writer         =  Yaron Avitov
 | starring       = Yaron Avitov
 | music          = 
 | cinematography = 
 | editing        = 
 | studio         = 
 | distributor    =
 | released       =
 | runtime        = 
 | country        = 
 | language       = Spanish
 | budget         = 
 | gross          = 
}}
America Ladina is a 2011  documentary film directed by and starring  Yaron Avitov, an Israeli independent filmmaker. The film is the telling the arrival of Sephardic conversos in the sixteenth century to America, and the lives of their descendants today.
   

==Content==
The Israeli filmmaker Yaron Avitov addresses on his film, with grace, good taste and deep history of converts anusim in America; and how was his emigration from Spain and Portugal to America was due largely in the sixteenth century, persecution of religious inquisition. The way the Jewish people emigrated to America from the sixteenth century, during the time of the Inquisition, should be studied thoroughly again. Avitov is immersed in a multi-year investigation by South and Central America in search of motives, reasons and consequences of this fact. So that the viewer is discovering, as the film progresses, not only the origins, but also exactly the places where you can find the presence of the first anusim inserts migrants in Andean culture, as in the Central. Living descendants of these early immigrants from 10 countries, including Colombia, Ecuador, Brazil, and Peru in South America testimonials. Mexico, Cuba, El Salvador, Costa Rica and Panama in Central America. Not to mention others that can be seen, in turn, in this film. The documentary takes us through time with the testimonies of the descendants of these first anusim who came to America in the sixteenth century, with a life expectancy that would give their children the chance to be born and grow into a new free world, without fear. Five centuries after its mission was fulfilled and their descendants are more alive than ever. The proof of that the filmmaker Avitov found embedded in the culture of the indigenous peoples of America, in the same way that some traditional customs of converts who came from Spain anusim fleeing the Inquisition and today, for not knowing this, practice other religions. Today there are words, idioms, words, sayings in (Judaeo-Spanish|Ladino) and even many customs that these groups of America was unknown where they came from and who were part of the traditions of their ancestors and now know of departure.   

==References==
 

 
 
 
 
 
 