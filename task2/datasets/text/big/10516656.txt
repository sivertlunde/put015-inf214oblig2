The Crab with the Golden Claws (film)
{{Infobox Film
| name = The Crab with the Golden Claws
| image = Crabwithgoldenclaws film poster.jpg  
| caption = 
| imdb_rating =
| director = Claude Misonne
| producer = Wilfried Bouchery
| writer = João B. Michiels
| starring =
| music = G. Bethune, A. Ducat
| cinematography = B. Michel, A. Dunil, E. Bernstein
| editing = A. Leduc
| distributor = 
| released =  
| runtime = 58 minutes
| country = Belgium French
| budget = 
| preceded_by = 
| followed_by = 
| mpaa_rating = 
| tv_rating = 
}}
 Belgian stop comic book of the same name from The Adventures of Tintin by Hergé. This was the first Tintin story to be adapted into a movie and follows the story of the comic almost exactly.
 ABC Cinema on 11 January 1947 for a group of special invited guests, while the other one was shown in public on December 21 of that year, before Bouchery declared bankruptcy and fled to Argentina. All of the equipment was seized and a copy of the film is currently stored at Belgiums Cinematek|Cinémathèque Royale. The copy is available to watch for paying members of the Tintin club. 

== Plot ==
Tintin finds himself involved in a mystery of a drowned man, a regular tin of crab meat, and the name of a ship called the Karaboudjan. Upon investigating the ship, Tintin discovers that the shipment of tin cans contains not crab meat, but drugs. After learning about the ships shady business, Tintin ends up becoming prisoner on the ship which already casted off from the port. The only way for Tintin to escape is by heading for dry land by life boat, and the only person to aid him is the ships beer guzzling Captain named Haddock who is the only one on board not aware that his crew is trafficking drugs right under his nose. 
 

== Release in DVD ==
On 14 May 2008, the film was released on PAL DVD in France by Fox Pathe Europa.

== See also ==
*List of animated feature-length films
*List of stop-motion films

== References ==
* Oliver Battrick|Battrick, Oliver. (21 March 2004).  . Tintinologist.org.

== External links ==
*  

 
 
 

 
 
 
 
 
 
 

 
 