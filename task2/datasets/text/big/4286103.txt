Blood Thirst
{{Infobox film
| name           = Blood Thirst
| image size     =
| image	=	Blood Thirst FilmPoster.jpeg
| caption        = DVD poster
| director       = Newt Arnold
| producer       = Newt Arnold
| writer         = N.I.P. Dennis
| narrator       =
| starring       = Yvonne Nielson Robert Winston Judy Dennis
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1971
| runtime        =
| country        = U.S.A. / Philippines English
| budget         =
| preceded by    =
| followed by    =
}} vampiric murders linked to a Manila nightclub.  Blood Thirst was filmed in the 1965 but not released until 1971 in the USA, where it mostly played the grindhouse circuit.

==Plot==
The films plot, scripted by N.I.P. Dennis in his only listed film credit, is frequently nonsensical, involving a South American belly dancer, played by Yvonne Nielson, who has uncovered an ancient Aztec secret for eternal life, involving regular blood transfusions. To this end, she has enlisted the aid of a Filipino club owner whose visage turns monstrous when he is stalking his prey.

==Cast==
The film starred Robert Winston as detective Adam Rourke.

==External links==
*  
*  

 
 
 
 
 
 
 
 