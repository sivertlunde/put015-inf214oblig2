American Promise
{{multiple issues|
 
 
}}

{{Infobox film
| name = American Promise
| image = American Promise poster.jpg
| caption = Theatrical release poster
| director = Joe Brewster Michèle Stephenson
| released =  
| country = United States
| language = English
| gross =  $94,000 
}}
American Promise is a documentary film spanning 13 years from directors Joe Brewster and Michèle Stephenson. The film captures the stories of Brewster and Stephensons 5-year-old son Idris and his best friend and classmate Seun as these families navigate their way through the rigorous prep-school process. The film is set against the backdrop of a persistent educational achievement gap that dramatically affects African-American boys at all socioeconomic levels across the country.

American Promise premiered at the 2013 Sundance Film Festival. The filmmakers also launched a national campaign at Sundance to help raise $100,000 and 100,000 volunteer hours for Big Brothers Big Sisters of America Mentoring Brothers in Action program.

==Release==
The PBS documentary series POV (TV series)|POV is set to broadcast American Promise in 2013. Random House is set to publish a companion book about the film and the issues it raises in conjunction with the American Promise Campaign.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 


 