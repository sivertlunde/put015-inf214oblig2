Abdullah (film)
{{Infobox film
| name           = Abdullah
| image          = Abdullah 1980 film poster.jpg
| image_size     = 
| caption        = Film poster
| director       = Sanjay Khan
| producer       = Asghar Ali Sanjay Khan Zarine Khan
| writer         = George Marzbetuny
| narrator       = 
| starring       = Raj Kapoor Sanjay Khan Zeenat Aman Danny Denzongpa
| music          = R.D. Burman
| cinematography = V. Gopi Krishna
| editing        = M. S. Shinde
| distributor    = 
| released       = 1980
| runtime        = 
| country        = India
| language       = Hindi/Urdu
| budget         = 
| gross          =   3,40,00,000 
}}
 1980 Cinema Indian Bollywood film that was directed by Sanjay Khan. The film starred Raj Kapoor, Sanjay Khan, Zeenat Aman and Danny Denzongpa, with Sanjeev Kumar and Farida Jalal in minor roles. The story was written by George Marzbetuny, and Kader Khan wrote the dialogues.

The films performance at the box office was deemed below average.   

==Plot==
In an unspecified Arab country, Khaleel (Danny Denzongpa) is a dangerous outlaw bringing terror to the land. Sheikh Mohammed Al-Kamal (Sanjay Khan) is a man of honour, who helps to protect people from harm and is asked by the government to help search for Khaleel. The quest to bring Khaleel to justice becomes a personal one for the Sheikh when his wife Zainab (Zeenat Aman) is injured during a bungled kidnap attempt by Khaleel.

Abdullah (Raj Kapoor) is a devout Muslim who lives in a small hut in the middle of the desert, and looks after a well which provides water to thirsty travelers. One day a friend, Ameer(Sanjeev Kumar), informs him that Khaleel had raided a settlement nearby, killing everyone except for Yashoda (Farida Jalal), a pregnant woman. Shortly thereafter, Ameer himself is killed, a mortally wounded Yashoda gives birth to a boy, names him Krishna, asks Abdullah to care for him, and passes away. Abdullah overcomes his fears of bringing up a Hindu boy, and looks after Krishna as his own son.
 the Hindu deity Krishna once slew Kansa, his maternal uncle, so also will Khaleels life end at this Krishnas hands. Angered by this, Khaleel sets out to kill Krishna. He attacks Abdullah, abducts Krishna and readies to kill the boy to get rid of any threat against him. In response, Abdullah and the Sheikh set out to stop this and to deal with Khaleel once and for all.

==Cast==
*Raj Kapoor as Abdullah
*Danny Denzongpa as Khaleel
*Sanjay Khan as Sheikh Mohammed Al Qamal
*Zeenat Aman as Zainab (Sheikh Mohammeds wife)
*Sanjeev Kumar as Ameer
*Farida Jalal as Yashoda
*Mehmood as Ahmed
*Rajeev Bhatia as Krishna
*Bob Christo as a magician in the employ of Khaleel
*Nazir Hussain as Blind man
*Kader Khan as Military Officer

==Soundtrack==

{{Track listing
| extra_column = Singer(s)
| title1          = Ae Khuda Har Faisla
| extra1	  = Kishore Kumar
| length1         = 2:23
| title2          = Bheega Badan Jalne Laga
| extra2	  = Asha Bhonsle
| length2         = 3:37
| title3          = Dance Music (Instrumental)
| extra3	  = Rahul Dev Burman
| length3         = 2:29
| title4          = Jashne Baharaan Mehfile Yaaraan
| extra4	  = Asha Bhonsle
| length4         = 4:41
| title5          = Allah Tera Nighebaan
| extra5	  = Manna Dey
| length5         = 5:19
| title6          = Maine Poochha Chand Se
| extra6	  = Mohammed Rafi
| length6         = 5:09
| title7          = Om Jai Jagdish Hare
| extra7	  = Lata Mangeshkar
| length7         = 4:52
}}

== References ==
 

== External links ==
*  
*  

 

 
 
 