Edwin Boyd: Citizen Gangster
{{Infobox film
| name           = Citizen Gangster
| image          = EdwinBoydCitizenGangster2011Poster.jpg
| alt            =  
| caption        = Film poster
| director       = Nathan Morlando
| producer       = Daniel Bekerman
| writer         = Nathan Morlando
| starring       = {{Plainlist|
*Scott Speedman
*Kelly Reilly
*Kevin Durand Joseph Cross}}
| music          = Max Richter
| cinematography = Steve Cosens
| editing        = Richard Comeau
| studio         = 
| distributor    = Entertainment One
| released       =  
| runtime        = 105 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
Citizen Gangster is a 2011 Canadian drama film directed and written by Nathan Morlando.   Scott Speedman stars as Canadian gangster and folk hero Edwin Alonzo Boyd.

The film originally premiered at the 2011 Toronto International Film Festival under the title Edwin Boyd but was retitled Citizen Gangster in general release.

== Plot == Brian Cox). Cox’s portrayal shows a man who disapproves of his son’s life and terrible decisions, but is made more poignant by his sad attempts to maintain a father-son relationship. 

In a moment of desperation, Boyd grabs an old pistol, disguises his face with theatrical greasepaint, and goes out to rob a bank; this sets off a series of events leading to one of Canada’s most infamous crime sprees. 
 Joseph Cross).  Jackson is also a veteran of WWII, and with his muscular build is the picture of the hardened criminal. There is an uneasy relationship between him and Boyd, given the attention lavished on the latter by the media; yet the two of them, both war vets, have a deep, unspoken brotherly bond. Durand’s portrayal is of a man whose vulnerability is cloaked by a gruff, practical-minded exterior, but who reveals his poetic side to the woman he loves, Ann Roberts. One of only two women in the group, Ann reveals the human side of Lenny, behind the ruthless criminal in the headlines and on the radio. Val Kozak (Joseph Cross) is in a similar bind as Boyd, with a young wife and a desperate need to support his family. But he is also carrying on an affair with Mary Mitchell (Charlotte Sullivan), a fur-coat-wearing, flashy blonde party girl, whose flamboyant facade reveals a woman with past hurts and a deep-seated need for love and attention. 

Boyd’s theatrical air and showmanship captivated attention; he would fling his arms wide open like a maestro upon entering the bank, a “welcome to the show” gesture. In a few telling scenes – including one where he dances along the top of the bank counter, while behind him the others wave guns and shove money into sacks – the patrons of the bank and even the tellers can barely hide their admiring glances as Boyd charms the money right out of them. One smiling teller whispers to Boyd as she’s emptying her till into a bag, “You know, some of the girls, they talk about you . . . about what you’d be like!” 

The foil to the gang’s mayhem is Detective Rhys, (William Mapother). A sombre family man who takes his work seriously, Rhys is the face of law enforcement in the movie, constantly on the gang’s trail.  

Boyds marriage grows increasingly strained. Despite the fact that Boyd’s initial motivation was a bid to provide for his young family and prove himself as a man, the nature of his lifestyle proves too much for her to handle. Whenever a siren screams past their window, all activities are dropped while they wait tensely to see if it will stop outside their apartment.   

Notably, the Boyd gang’s breakout of the Don Jail forms the very first news story of then-fledgling news broadcast network, CBC. 

== Cast ==
* Scott Speedman as Edwin Boyd
* Kelly Reilly as Doreen Boyd
* Kevin Durand as Lenny Jackson Joseph Cross as Val Kozak
* Brendan Fletcher as Willie The Clown Jackson
* Charlotte Sullivan as Mary Mitchell
* Melanie Scrofano as Ann Roberts Brian Cox as Glover
* William Mapother as Detective David Rhys
* Christian Martyn as Billy Boyd

== Production ==
Filming began February 17, 2011 in Sault Ste. Marie, Ontario. 

== Release ==
Edwin Boyd: Citizen Gangster premiered at the 2011 Toronto Film Festival.   IFC Films distributed it in the United States. 

== Reception ==
Rotten Tomatoes, a review aggregator, reports that 50% of 14 surveyed critics gave the film a positive review; the average rating is 5.5/10.   Metacritic rated it 56/100 based on 9 reviews.   Linda Barnard of the Toronto Star rated it 3/4 stars called it a "carefully crafted, sympathetic examination of Canadas most notorious bank robber".   Joe Leydon of Variety (magazine)|Variety called it "generally low-key but sporadically exciting".  Jeannette Catsoulis of The New York Times wrote, "Citizen Gangster is a good-looking but passionless affair that remains stubbornly aloof from its audience."   Gary Goldstein of the Los Angeles Times called it a stylish but overly familiar film whose main draw is Speedmans performance. 
 2012 Genie Awards: Best Actor (Speedman), Best Supporting Actor (Durand) and Best Supporting Actress (Sullivan). 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 