Cinderella (1947 film)
{{Infobox_Film name           = Cinderella Золушка image          = 1947 zolushka.jpg director       = Nadezhda Kosheverova   Mikhail Shapiro writer         = Evgeny Shvarts   Nikolai Erdman starring       = Erast Garin   Faina Ranevskaya producer       =  music          = cinematography = distributor    =
 | based on =   studio         = Lenfilm released       = 1947 runtime        = 84 min.  country        = Soviet Union language  Russian
}} 1947 Soviet Soviet musical film by Lenfilm studios.

It is a classical story about Cinderella, her evil Stepmother, and a Prince.

==Cast==
* Cinderella - Yanina Zheymo
* Prince - Aleksei Konsovsky
* King - Erast Garin
* Stepmother - Faina Ranevskaya
* Anna, 1st daughter - Yelena Yunger
* Marijana, 2nd daughter - Tatyana Sezenyovskaya
* Forester - Vasili Merkuriev
* Pas de Trois - Aleksandr Rumnyov
* Fairy - Varvara Myasnikova
* Page - Igor Klemenkov Sergei Filippov

==Crew==
*Evgeny Shvarts, screenwriter
*Mikhail Shapiro
*Nadezhda Kosheverova
*Nikolay Akimov, artistic designer

==External links==
* 

 

 
 
 
 
 
 
 

 
 