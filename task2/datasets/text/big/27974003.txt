Aravaan
{{Infobox film
| name           = Aravaan
| image          = Aravaan1.jpg
| caption        = Production poster
| director       = Vasanthabalan
| producer       = T. Siva S. Madhan
| based on       =  
| writer         = Su. Venkatesan   (Dialogue) 
| screenplay     = Vasanthabalan
| story          = Su. Venkatesan   Vasanthabalan  (Additational Story)  Aadhi Pasupathy Archana Kavi Karthik
| cinematography = Siddharth
| editing        = Praveen K. L. N. B. Srikanth Amma Creations
| distributor    = Vendhar Movies
| released       =  
| runtime        =
| country        = India
| language       = Tamil
| budget         =   
}} period film directed by Vasanthabalan, based on Sahitya Academy winning Su. Venkatesans novel Kaaval Kottam.  It stars Aadhi (actor)|Aadhi, Dhansika, Malayalam actress Archana Kavi and Pasupathy,  with Kabir Bedi playing a prominent role,  and Bharath appearing in a cameo role.  The film marks noted playback singer Karthik (singer)|Karthiks debut as a music director.  Aravaan released on March 2, 2012 to mixed reviews but was successful. The film was dubbed in Hindi as "Jungle The Battleground".

==Plot==
The film set in the 18th century is all about a group of people who steals from the wealthy to provide food and shelter for their tribe. Kombodhi (Pasupathy) is the gang leader and during one of his missions he meets the brave Varipuli (Aadhi (actor)|Aadhi). He brings Varipuli to his village and a bond develops between the two. But Varipuli has a past and his real name is Chinna. For the death of a youth Thogaimayan (Bharath) from neighbouring village, the King had in fact ordered one youth to be given as offering to the Almighty and Chinna was the choice. Sequence of events lead to Chinna escaping from the offering only to find out the mystery behind Thogaimayans death. But that was too late as too many things happened in his village by then. Now he goes on the hiding.  Cut to present, Chinna is taken to be offered to God.  Where the flashback begins. 
He himself being a border guarding person loves a girl in the same village called Vannapechi(Dhansika).
later the village find Thogaimayan dead.  After Chinna is chosen to be the offering to the god Chinnas lover asks her father to marry Chinna to at least spend his last days together.  They marry on the 2nd day. Chinna finds a fellow villageman running with a silver jewellery.
When Chinna asks where he got it from he replies he got it from Thogaimayans hand.
Chinna later finds out that the killer was the villages King himself. he confronts the king and the king tells the story.  His second wife was found out to be pregnant but he didnt like it as he never touched her. The queen tell she has the baby to take revenge on him as she was the queen of a neighbouring village and this King had killed everyone from her family and married her to prevent anyone else from her family. The king finds out it was Thogaimayan who created this chaos and kills him and his second wife.  Chinna tries taking him back to the villagebut the king escapes and falls down a waterfall to prevent the villagers from knowing.  Chinna jumps too but his leg is broken and comes out only to find the King is dead.  Meanwhile in the village as Chinna is not to be found anywhere they sacrifice someone else.  But the village head tells everyone that everyone has a right to kill Chinna as he has bought a disgrace to the village.  Chinna gets well and meets his family to know that his wife is pregnant.  He says he will go to the killing but his wife tells him to wait for the baby see its face then go.But her father tells Chinna to go out of the village and not come back for 10 years as the villagers will forgive anyone after 10 years as the villagers will not believe the truth. Chinna leaves.But the neibouring villagers find him in 9 years
At present Kombodhi and his friends save Chinna and take him in the way they come back again and Chinna surrenders as he doesnt want to live a life like this. He sees his sons face for the first and the last time. only to find out a child he knew from the childs young age is going to kill him he gets the knife and kills himself.

== Cast == Aadhi as Varipuli/Chinna
* Pasupathy as Kombodhi
* Dhansika as Vannapechi
* Archana Jose Kavi as Chimitti
* Karikaalan as Mathara
* Singampuli
* Thirumurugan as Veeranna
* Vijayachander as Paalaiya Karar

Special appearances by :

* Bharath as Thogaimaan
* Shweta Menon as Rajaambaal Anjali as Vanchi
* Sruthi Prakash as Kanaganuga                                          
* Kavignar Vikramathithan Abhinaya

==Production==

===Development===
In April 2010, following the success of Angaadi Theru, Vasanthabalan started working on his new project based on Su. Venkatesans novel Kaaval Kottam, a novel depicting the history of Madurai during 1310-1910, which was released in 2009. Aravaans story would be based on one chapter from the novel, running to about 10 pages, with Vasanthabalan claiming that he expanded it and took one year for penning the scriptment,    further telling that he had tried to "mix action and emotion".    The director told that he included more characters and incidents into the script, disclosing that the films lead character Varipuli was not there in the short story and that he created him and gave a name. 

===Casting=== Aadhi to six pack Archana Kavi was roped in for her first Tamil film to play the role of Chimitti, a bold and aggressive woman, who "loves to take control of everything around her", with Archana reporting that the character was "unlike her real self" and that she had difficulties to "get the body language and the Madurai Tamil right".  In January 2011, Kabir Bedi, noted Hindi film actor, who had also appeared in several international productions, notably in the 1983 James Bond film Octopussy, was signed up for a significant role, making his Tamil film debut,  while the following month, actor Bharath, who had collaborated with Vasanthabalan in Veyil as well, accepted to perform a cameo role.  In August 2011 reports confirmed that Malayali model-actress Shweta Menon would appear in a cameo role as a dasi.  

===Filming===
Since the film was set in the past, the crew needed to find locations that had no trace of "modern life" yet and searched for remote unsettled areas devoid of cell phone towers, overhead power lines and tarred roads. Cinematographer Siddharth commented that it took him four months to find a suitable location.   
 Hogenakkal in Tamil Nadu, and forest areas in Kerala and Andhra Pradesh.  Arithapatti was a place that boasted off many caves, streams and huge valleys. The place further had hundreds of huts made of bamboo straws, palm leaves and stones and a Karuppan Temple.  Siddharth stated these locations were chosen as the film had two different tones; the first of half of the film took place in a hot, dry, barren land and the entire portion was shot in Arithapatti, while the flashback scenes would happen in a humid area with greenery, water and huge trees, which was canned in Courtallam and the Kerala forests.  The first shots were filmed in the Achankovil forests in Kerala.  In Ovvamalai, near Melur, a settlement of 100 houses was created, while another set was erected at Kongaimalai, near Tenkasi.  Art director Vijay Murugan needed fifty days to erect the sets.  The scene where buffaloes are sacrificed was shot in dense forests in Tamil Nadu-Kerala border.  Producer Siva said that he ordered 5,000 panchas and 3,000 cheeras specially made from Erode and Karur to give it a historical feel.  Aadhi injured himself while performing a risky stunt sequence, in which the protagonist had to jump over a five feet palm tree. 

==Soundtrack==
{{Infobox album|  
  Name        = Aravaan
| Type        = Soundtrack Karthik
| Cover       = 
| Border      = 
| Alt         = 
| Caption     = 
| Release     = October 5, 2011
| Recorded    = 2011 Feature film soundtrack
| Length      =  Tamil
| Label       = Junglee Music
| Producer    = Karthik
| Reviews     =
}}
The soundtrack was composed by playback singer Karthik (singer)|Karthik, debuting as a composer through this film. The soundtrack release function was held on 5 October 2011 at the Anna Centenary Library at Kotturpuram in Chennai, with A. R. Rahman and Mani Ratnam launching the album.

{{tracklist
| headline        = Track listing 
| extra_column    = Singer(s)
| total_length    = 
| all_lyrics      = Na. Muthukumar (except where noted)
| title1          = Oore Oore Ennapetha
| note1           = Viveka
| extra1          = Krishnaraj, Mukesh, Periya Karuputhevar, Rita & Priya
| length1         = 
| title2          = Nila Nila Poguthae
| extra2          = Vijay Prakash & Harini
| length2         = 
| title3          = Kalavuda
| extra3          = Mano (singer)|Mano, Pasupathi, Kottaichami, Karunanidhi, Rahul Nambiar, M. L. R. Karthikeyan, Vijay Narain, Malathi, Harish
| length3         = 
| title4          = Unna Kolla Poren
| extra4          = M. L. R. Karthikeyan, Bhavatharini
| length4         = 
| title5          = Naagamalai Sanchuidchu
| extra5          = Gopal, Sirkazhi G. Sivachidambaram & Narayanan 
| length5         = 
| title6          = Nanadakumara
| extra6          = Subhiksha
| length6         = 
| title7          = Oruvaan Irruvaan Karthik
| length7         =
}}

===Critical reception===
Rediff rated 3 out of 5 stars and wrote:"Aravaan is worth a listen".  Behindwoods rated 2 1/2 out of 5 and wrote:"Without doubt, the music album of Aravan will satisfy the taste buds of the patrons of true music. Favorites are Oruvaan Irruvaan, Nila Nila Poguthae and Oore Oore Ennapetha".  Milliblog wrote:"Disappointingly safe and innocuous composing debut by Karthik".  Musicaloud wrote:"A debut replete with feel-good songs, but devoid of much innovativeness".  Musicperk wrote:"In his very first attempt, singer Karthik has proved himself as a promising music director   his compositions are equally charming, he does justice to this period film" 

==Release==
Following several postponements, the film was released on 2 March 2012. A dubbed Telugu version titled Eka Veera was released simultaneously in Andhra Pradesh. Director N. Linguswamys Thirupathi Brothers had acquired the distribution rights of the film, before they were sold again to Vendhar Movies, couple of days prior to the release.  The film opened at No.1 in Chennai box office accounting for 54% of the takings  and 46% in its second weekend. 

The satellite rights of the film were secured by Kalaignar TV instead it went to Sun TV. 

==Critical reception==
Aravaan fetched mixed reviews from critics. Behindwoods.com gave 2.5 out of 5 stars, claiming that it was "high on detailing and effort but low on engaging".  Pavithra Srinivasan from Rediff.com gave it four out of five stars, labelling it as "brilliant" and calling it a "must-watch film". The reviewer further commented: "Aravaan is a slice of life from 18th century Tamil Nadu, with its lifestyle, humour, sorrows, loves and losses documented in painstaking fashion. This is an ode to history that deserves every bit of attention it receives. As such, its one of those movies that deserve to become a legend".  

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 