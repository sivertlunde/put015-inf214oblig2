Equinox Flower
{{Infobox film
| name = Equinox Flower Higanbana
| image = Higanbana.jpg
| caption = Studio still showing from left: Ineko Arima, Fujiko Yamamoto & Yoshiko Kuga
| director = Yasujiro Ozu
| producer = Shizuo Yamanouchi
| based on = novel by Ton Satomi
| writer = Kōgo Noda, Yasujirō Ozu
| starring = Shin Saburi, Kinuyo Tanaka, Ineko Arima, Yoshiko Kuga
| music = Takanobu Saitō
| cinematography = Yuharu Atsuta
| editing = Yoshiyasu Hamamura
| distributor = Shochiku
| released = 1958
| runtime = 118 minutes
| country = Japan
| language = Japanese
| budget =
| preceded_by =
| followed_by =
}} Japanese film Blue Ribbon Award for Fujiko Yamamotos performance as Best Actress.

==Plot==
Wataru Hirayama (Shin Saburi) is a wealthy Tokyo businessman who acts as a go-between for couples seeking marriages.  When an old schoolmate Mikami (Chishū Ryū) approaches him for help concerning his daughter Fumiko (Yoshiko Kuga), who has run off owing to a conflict with her father, he agrees.  Finding her in a bar where she now works, he listens to her side of the story.  Fumiko complains that her father is stubborn, insisting on arranging her marriage, whereas she has now fallen in love with a musician and is adamant to lead life her own way.

One day during work, a young man named Masahiko Taniguchi (Keiji Sada) approaches Hirayama to ask for the hand of his elder daughter, Setsuko (Ineko Arima).  Hirayama is extremely unhappy that his daughter has made wedding plans on her own.  He confronts her at home, during which he blows his top, and rules that she must remain locked at home until she sees the folly of her ways.  Hirayama tries to find out more about Taniguchi from his subordinate.

Owing to the standoff, his daughters friend Yukiko (Fujiko Yamamoto) tries a ruse in which she asks Hirayama opinion concerning a similar situation – her mother forcing her to marry someone she didnt like.  When Hirayama advises her to ignore her mother, Yukiko reveals it is all a setup and states that Hirayama has just given his consent to Setsukos marriage.

Hirayamas wife Kiyoko (Kinuyo Tanaka) tries unsuccessfully to make him change his mind.  She accuses her husband of being "inconsistent". Even his younger daughter Hisako (Miyuki Kuwano) is on the side of her sister, finding her father too old-fashioned.  Finally, after the couples insistence on getting married, Hirayama decides to give in by attending his daughters wedding.

After the wedding, Mikami reveals that he, like Hirayama, has agreed to let his daughter select her own marriage partner.  After going for a short business trip outside Tokyo, Hirayama decides to visit the newly-weds at Hiroshima by train, where Taniguchi is stationed by his company.

==Production== Agfa film from Germany over Kodak or Fujifilm, as he felt that it conveyed red colors better. The meaning of "equinox flower" or "higanbana" of the title is the red Lycoris (genus)|Lycoris.

== Cast ==
* Shin Saburi - Wataru Hirayama
* Kinuyo Tanaka - Kiyoko Hirayama
* Ineko Arima - Setsuko Hirayama
* Miyuki Kuwano - Hisako Hirayama
* Yoshiko Kuga - Fumiko Mikami
* Chishū Ryū ... Shukichi Mikami
* Fujiko Yamamoto - Yukiko Sasaki
* Keiji Sada - Masahiko Taniguchi
* Teiji Takahashi - Shotaru Kondo
* Fumio Watanabe - Ichiro Nagamura
* Nobuo Nakamura - Toshihiko Kawai
* Ryūji Kita - Heinosuke Horie

==Release==

===Home media===
In 2011, the BFI released a Region 2 Dual Format Edition (Blu-ray + DVD).  Included with this release is a standard definition presentation of There Was a Father.

== References ==
 

==External links==
*  
*  
*  
*  
* 

 

 
 
 
 
 
 