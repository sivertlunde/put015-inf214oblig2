Born to Fight (1989 film)
{{Infobox film
| name = Born to Fight
| image =
| image_size =
| caption =
| director = Bruno Mattei
| producer = Franco Gaudenzi
| writer = Claudio Fragasso
| narrator =
| starring = Brent Huff John van Dreelen Werner Pochath
| music = Al Festa
| cinematography = Richard Grassetti
| editing = Bruno Mattei
| distributor = Flora Film
| released = July 17, 1989 (Portugal)
| runtime = 85 minutes
| country = Italy
| language = English Italian
| budget =
| preceded_by =
| followed_by =
| website =
}} low budget in 1988 and is the third time − after Strike Commando 2 (1988) and Cop Game (1988) − when Brent Huff worked with director Bruno Mattei.

==Plot==
Super tough Vietnam Veteran Sam Woods (Huff) is a survivor of a vicious prison camp where he was brutally and painfully tortured before finally managing to escape. When Wood returned to rescue his friends, he found that they were already dead. Some time later a woman named Maryline Kane (Stavin) offers him a tremendous amount of money if he will accompany her back to the area where the prison camp was to do interviews for a documentary story. It all turns out to be a lie − her father is now a prisoner of that same camp where Wood was tortured, and she knows that only a man like Wood can help set him free. Sam adopt a proposal, but then situation came out to be much more complicated, because the camp is now run by an old nemesis Duan Loc (Pochath).

==Cast==
* Brent Huff as Sam Wood
* Werner Pochath as Duan Loc
* Romano Puppo as Alex Bross
* John van Dreelen as Gen. Weber
* Mary Stavin as Maryline Kane
* Don Wilson (IV) as Gen. Webers Aide
* Claudio Fragasso (as Clyde Anderson) as one of the prisoners


==External links==
*  

==References==
 


 
 

 

<!--  
 -->

 
 
 
 
 
 
 
 
 