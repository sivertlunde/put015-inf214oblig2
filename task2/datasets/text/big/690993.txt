Alone in the Dark (2005 film)
 
{{Infobox film
| name           = Alone in the Dark
| image          = Alone in the Dark 2005.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Uwe Boll
| producer       = Uwe Boll Wolfgang Herold Shawn Williamson
| writer         = Elan Mastai Michael Roesch Peter Scheerer
| based on       =  
| starring       = Christian Slater Tara Reid Stephen Dorff
| music          = Reinhard Besser Oliver Lieb Bernd Wendlandt Peter Zweier
| cinematography = Mathias Neumann
| editing        = Richard Schwadel Boll KG Herold Productions Infogrames Entertainment Lions Gate Films  
| released       =  
| runtime        = 96 minutes 99 minutes  
| country        = Germany Canada United States
| language       = English
| budget         = $20 million 
| gross          = $10,442,808 
}} science fiction action horror same name. worst films ever made.
 Alone in the Dark II, directed by Michael Roesch and Peter Scheerer. 

==Plot==
Edward Carnby (Slater) is a detective who specializes in the occult & other paranormal subjects. He was apparently the subject of strange experiments when he was a child, leaving him with heightened abilities as well as a "sixth sense" that allows him to sense the paranormal. Throughout the film, we also learn that Carnby used to work for Bureau 713, a secret government organization that seeks to protect the world from paranormal dangers.

In his spare time, Carnby investigates the disappearance of the Abkani, an ancient Maya civilization|Mayan-like civilization that worshipped demonic creatures from another dimension. Central to the plot are several artifacts un-earthed in 1967 and now on display at the citys Museum of Natural History, at which Carnbys ex-girlfriend Aline (Reid) is the assistant curator. While working on a missing-child case, Carnby soon finds himself investigating the very scientist who conducted experiments on him as a child. He also finds himself working with Aline and former protégé (now rival) Commander Burke, his replacement at Bureau 713, to stop an invasion of the Alien (creature in Alien franchise)|Alien-like demonic creatures who are pouring through a portal opened by the Abkani artifacts.

==Cast==
* Christian Slater as Edward Carnby: Raised at an orphanage under Sister Clara, Carnby lost his memory when he was ten years old. At twenty, he was recruited by Bureau 713, gaining knowledge on the paranormal soon after. His current assignment is investigating his past along with researching the disappearance of the Abkani. Due to the experiments conducted on him as a child, he has the ability to sense paranormal activity and has increased strength and speed, which allow him to perform acrobatic moves that a normal human could not do.
** Dustyn Arthurs as Young Edward
* Tara Reid as Aline Cedrac, an archaeologist and museum curator; Edwards ex-girlfriend who knows about the Abkani and their culture.
* Stephen Dorff as Commander Richard Burke, the Commander of Bureau 713, formerly worked under Carnbys direction.
* Frank C. Turner as Agent Fischer, the head of the medical unit of Bureau 713; he is one of Carnbys few trusted allies and friends. Matthew Walker as Professor Lionel Hudgens
* Will Sanderson as Agent Miles
* Francoise Yip as Agent Cheung
* Mark Acheson as Captain Chernick
* Darren Shahlavi as John Dillon
* Karin Konoval as Sister Clara, owner of the orphanage which cared for Edward. In the 80s, she was persuaded by Hudgens to allow experiments on the orphans. She keeps this secret from everyone but is inwardly guilty for her immoral actions.
* Ed Anders as James Pinkerton, a former Agent of Bureau 713 who went missing in action in the 1980s. He and Hudgens were in charge of the investigation of the disappearance of goldminers at Brutan Goldmine. Pinkerton became an experiment for Hudgens, who attached a Xenos creature to his spine. His abilities included increased awareness, strength, speed and willpower.
* Brendan Fletcher as Cab driver

==Connections to the Game==
 
* In the film and the games, the lead protagonist is named Edward Carnby.
* The game version of Alone in the Dark features an ending that takes place on the morning after and is open-ended, showing a mysterious cab driver pick up Edward Carnby or Emily Hartwood and whose destination is unknown. The film version also offers an ending which takes place in the morning when something mysteriously startles Edward Carnby and Aline Cedrac.
*   mentions Edward Carnby as part of a paranormal agency known as Bureau 713. The film version goes into greater detail as this is part of the focus of the film.
* Alone in the Dark: The New Nightmare and the film both feature creatures that thrive in the darkness, have invisibility and can be harmed by light and electricity. In the film, these creatures are known as Xenos.
* The film version features a storyline with elements found in the games. The kidnapping of the orphaned children in the film is similar to the kidnapping of Grace Saunders from Alone in the Dark 2. The disappearance of the orphans is similar to the disappearance of Detective Ted Striker from Alone in the Dark II. The closing of the gateway of darkness storyline from the film is similar to Alone in the Dark: The New Nightmare which requires the protagonists to close a gateway of darkness. The film version also features a scientist doing experiments on people which is similar to Alone in the Dark: The New Nightmare.

==Alternative versions==

===Alternate Script===
Blair Erickson came up with the first drafts of the script for Alone in the Dark. According to Erickson, Uwe Boll changed the script to be more action packed than a thriller. Erickson stated his disgust and his working relationship towards Boll on Somethingawful.com.
 The original script took the Alone In the Dark premise and depicted it as if it were actually based on a true story of a private investigator in the northeastern U.S. whose missing persons cases begin to uncover a disturbing paranormal secret. It was told through the eyes of a writer following Edward Carnby and his co-worker for a novel, and depicted them as real-life blue-collar folks who never expected to find hideous beings waiting for them in the dark. We tried to stick close to the H. P. Lovecraft style and the low-tech nature of the original game, always keeping the horror in the shadows so you never saw what was coming for them.

Thankfully Dr. Boll was able to hire his loyal team of hacks to crank out something much better than our crappy story and add in all sorts of terrifying horror movie essentials like opening gateways to alternate dimensions, bimbo blonde archaeologists, sex scenes, mad scientists, slimy dog monsters, special army forces designed to battle slimy CG dog monsters, Tara Reid, "The Matrix|Matrix" slow-motion gun battles, and car chases. Oh yeah, and a ten-minute opening back story scroll read aloud to the illiterate audience, the only people able to successfully miss all the negative reviews. I mean hell, Boll knows thats where the real scares lie. }}

==Home Media==
The film was released on DVD on May 10, 2005.

===Unrated Directors Cut===
An Unrated Directors Cut was released in Germany, France, and Australia and was #1 on the German DVD market for three weeks.  It was released on DVD in North America on 25 September 2007.  In the newest version of the film, virtually all of the scenes with Tara Reid in them have been removed by Boll himself. 

==Original film and game tie-in concept== Alone in the Dark 5, which was released on June 26, 2008.

==Reception==

===Box office===
Alone in the Dark grossed $2,834,421 in its opening weekend, ranking at #12; by the end of its run, the film had grossed $10,442,808 and was a box office bomb, considering its $20 million budget.   

===Critical response=== worst films video game series.  Scott Brown of Entertainment Weekly gave the film a very rare F grade, commenting that the film was "so bad its postmodern." 

Game Trailers ranked the film as the third worst video game movie of all time; among other things, it was emphasized that "the inadvertently hilarious action-horror flick had little to do with the series and even less to do with common decency!" 

In one of the films only positive reviews, Michelle Alexandria of Eclipse Magazine wrote "Alone in the Dark isnt going to set the world on fire, but it largely succeeds with what it has to work with. Just dont take it seriously and youll have a fun time." 

===Awards and nominations===
{| class="wikitable"
|-  style="background:#b0c4de; text-align:center;"
! Award
! Subject
! Nominee
! Result
|- Stinkers Bad Movie Awards Worst Picture
|Alone in the Dark
| 
|- Worst Actress Tara Reid
| 
|- Worst Special Effects
| 
|- Worst Song
|"Wish I Had an Angel" (Nightwish)
| 
|- Worst Director Uwe Boll
| 
|- Golden Raspberry Awards
| 
|- Golden Raspberry Worst Actress Tara Reid
| 
|}

==Soundtrack==
{{Infobox album  
| Name       = Alone in the Dark: Music from and Inspired by Alone in the Dark
| Type       = Soundtrack
| Artist     = Various artists
| Cover      = 
| Alt        = 
| Released   =   
| Recorded   = 
| Genre      = 
| Length     =     
| Label      = Nuclear Blast Misery Index, Fredrik Nordström, Zack Ohren, Eric Rachel, Nick Raskulinecz, Samael, Ben Schigel, Andy Sneap, Waldemar Sorychta, Patrik J. Sten, Peter Tägtgren, Devin Townsend, Paul Trust, Zeuss   
| Last album = 
| This album = 
| Next album = 
}}

The 2-disc soundtrack was released by Nuclear Blast, with Wolfgang Herold as executive producer. The German band Solution Comas contribution was the title song. Finnish symphonic metal band Nightwish had a music video of "Wish I Had an Angel" directed by Uwe Boll, with clips from the film.

;Disc 1
#Dimmu Borgir - Vredesbyrd
#Shadows Fall - What Drives the Weak
#Fear Factory - Cyberwaste
#In Flames - Touch of Red
#Strapping Young Lad - Devour
#Agnostic Front - Peace
#God Forbid - Gone Forever
#Chimaira - Down Again
#Dark Tranquillity - Lost to Apathy Exodus - Blacklist Machine Head Imperium
#Soilwork - Stabbing the Drama
#Lacuna Coil - Daylight Dancer
#The Dillinger Escape Plan - Panasonic Youth
#Meshuggah - Rational Gaze
#Nightwish - Wish I Had an Angel
#Cradle of Filth - Mother of Abominations

;Disc 2
#Arch Enemy - Dead Eyes See No Future
#Death Angel - The Devil Incarnate Diecast - Medieval
#Fireball Ministry - Daughter of the Damned
#Heaven Shall Burn - The Weapon They Fear Hypocrisy - Eraser
#Mastodon - Blood and Thunder Misery Index - The Great Depression
#Mnemic - Ghost
#Dew-Scented - Slaughtervain
#Suffocation - Souls to Deny Raunchy - Watch Out
#Kataklysm - As I Slither
#Bloodbath - Outnumbering the Day
#All Shall Perish - Deconstruction Bleed the Sky - Minion
#Samael - On Earth
#Dying Fetus - One Shot, One Kill
#The Haunted - 99

==See also==
* List of American films of 2005
* List of films based on video games
* List of films considered the worst

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 