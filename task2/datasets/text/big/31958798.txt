Paglu
{{multiple issues|
 
 
}}
{{Infobox Film
| name           = Paglu
| image          = Paglu Film Poster.jpg
| caption        = Movie Commercial Poster
| director       = Rajib Biswas
| producer       = Surinder Films
| writer         = N.K. Salil Dev Koel Mallick Rajatava Dutta
| music          = Jeet Ganguly
| designer       = Poulami Dasgupta
| music release  = From May
| cinematography = Kumud Verma
| released       =  
| runtime        = 143 minutes
| country        = India Bengali
| budget         = 3.75 cr
| gross          = 7.25 cr
}} Tollywood romantic comedy film. Directed by Rajib Biswas. The film is a remake of Telugu film Devadasu (2006 film)|Devadasu.

==Plot==
The movie begins with Paglu (Dev (actor)|Dev),a college student, meeting Rimi (Koel Mallick)– a girl who comes down from the U.S. to take admission in Princeton College. She becomes a matter of contention between Dev and his rival in college, Ronnie whom Dev defeats in a challenge and wins over Rimi.
Rimi’s father, no less than a Senator in the U.S (Rajatava Dutta) lands up in town and takes her back to the U.S on the pretext of celebrating her birthday. He separates the couple, but they vow to get back to each other.
Dev’s visa is rejected so he organizes a public rally and pleads his love for Rimi which helps him get his visa!
Once he lands up there, he starts a game with Rimi’s father which sees Dev laying down a challenge to him and also sees him getting bashed up by his goons. He returns and takes Rimi away with him, but later Rimi goes back to her house before the deadline ends though they could have escaped as Dev wanted to prove a point.
Dev goes back to India and Rimi joins him later.

==Cast== Dev as Paglu
* Koel Mallick as Rimi
* Rajatava Dutta as Rimis Father

==Crew==
*Producer: Surinder Films
*Director: Rajib Biswas
*Screenplay: N.K. Salil
*Cinematographer: Kumud Verma
*Editor: Rabiranjan Moitra
*Designer: Poulami Dasgupta

==Music==
Paglu films music composed by Jeet Ganguly.

===Soundtrack===
* "Paglu" – Mika Singh & Akriti Kakkar
* "Prem Ki Bujhini" – Zubeen Garg & Akriti Kakkar
* "Monbebagi" – Kunal Ganjawala, Akriti Kakkar & Rana Mazumder
* "Esechi Toke Niye" – Mohit Chauhan
* "Jaane Mon" – Jeet Ganguly
* "Paglu" (Remix) – Mika Singh & Akriti Kakkar

===Designer===
Paglu films designer is Poulami Dasgupta.

==See also== Sathi
* Prem Aamar

==External links==
*  


 
 
 
 