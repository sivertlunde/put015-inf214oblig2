Song of the Open Road
 
{{Infobox film
| name           = Song of the Open Road
| image          =Song of the Open Road.jpg
| image_size     =
| caption        =
| director       = S. Sylvan Simon
| producer       = Charles R. Rogers
| writer         = Irving Phillips (Story) Edward Verdier (Story) Albert Mannheimer
| narrator       = Peggy ONeill
| music          = Charles Previn
| cinematography = John W. Boyle
| editing        = Truman K. Wood
| distributor    = United Artists
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
Song of the Open Road is a 1944  , 1995. Included on the DVD Classic Musicals Double Feature: Nancy Goes to Rio/Two Weeks with Love (Warner Home Video, 2008) 

==Overview==
Child film star Jane Powell, tired of her life being run by her stage mother, runs away from home and tries to lead a "normal" life at a Civilian Conservation Corps camp.  When a crop needs picking, Powell enlists the help of some celebrity friends.  It was W. C. Fieldss next-to-last film; he is one of several performers playing themselves in the production.  In the film, Fields—who began his career as an accomplished juggler—juggles some oranges for a few moments.  He remarks "This used to be my racket".  Then, missing a catch, he drops the oranges and walks away muttering "used to be my racket, but it isnt anymore!".

==Cast==
* Jane Powell as Jane Powell
* Bonita Granville as Bonnie Peggy ONeill as Peggy
* Jackie Moran as Jack Moran
* Bill Christy as Bill Reginald Denny as Director Curtis
* Regis Toomey as Connors
* Rose Hobart as Mrs. Powell
* Sig Arno as Spolo
* Edgar Bergen as Himself
* W.C. Fields as Himself Sammy Kaye and His Orchestra
* Frank, Harry and Steve Condos as Condos Brothers (Dance Specialty)
* The Lipham Four as The Lipham Four
* Irene Tedrow as Miss Casper

==Production==
Director S. Sylvan Simon had terrible difficulty filming scenes with W. C. Fields due to Fields alcoholism. After lunch hour he was often nowhere to be found. This problem was solved by luring Fields into his truck early in the day and removing the ladder. Fields would often rant and complain before eventually falling asleep. 

Although Fields often made fun of singers and singing in general, he had a fondness for the promising young singer Jane Powell and even referred to her (as "little Janie Powell") on one of his CBS radio broadcasts (preserved on transcription discs).  Powell sang several songs in the film and made such an impression that MGM signed her to a contract to make a number of musical comedies for them, through the mid-1950s. Powells real name was Suzanne Burce, but prior to the release of this film, MGM assigned her the stage name "Jane Powell", the name of the character she portrays in this film. 

Location shooting was done in Palm Springs, California and at the Pan-Pacific Auditorium in Los Angeles. 

==Award nominations==
{| class="wikitable"
|-
! style="background-color: #BCBCBC" | Year
! style="background-color: #BCBCBC" | Result
! style="background-color: #BCBCBC" | Award
! style="background-color: #BCBCBC" | Category
! style="background-color: #BCBCBC" | Recipient
|- style="background-color: #EAEAEA;" |
| 1945 || Nominated || Academy Award || Best Music, Original Song ("Too Much in Love")  || Walter Kent (Music) & Kim Gannon (Lyrics)
|-
| 1945 || Nominated || Academy Award || Best Music, Scoring of a Musical Picture || Charles Previn
|- style="background-color: #EAEAEA;" |
| 1945 || Nominated || Academy Award || Best Art Direction, Black and White || N/A (nomination withdrawn)
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 