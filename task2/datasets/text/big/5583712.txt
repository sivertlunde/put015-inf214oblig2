The Arch
 
 
{{Infobox film
| name           = The Arch
| image          = 
| caption        = 
| director       = Tang Shu Shuen
| producer       = Li Chiu-chung
| writer         = Tang Shu Shuen
| starring       = Lisa Lu
| music          = Lui Tsun-yuen
| cinematography = Subrata Mitra
| editing        = Les Blank C.C. See
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Hong Kong
| language       = Mandarin
| budget         = 
}}
 Best Foreign Language Film at the 42nd Academy Awards, but was not accepted as a nominee. 

==Cast==
* Lisa Lu as Madame Tung
* Roy Chiao as Captain Yang
* Szu-yun Chen
* Yu-Kuan Chen
* Hilda Chow Hsuan as Wei-Ling
* Po Hu
* Ying Lee as Chang
* Yui Liang as Monk
* Tang Shu Shuen

==See also==
* List of submissions to the 42nd Academy Awards for Best Foreign Language Film
* List of Hong Kong submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 