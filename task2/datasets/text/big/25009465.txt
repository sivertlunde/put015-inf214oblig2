Stormy Weather (2003 film)
{{Infobox film
| name           = Stormy Weather
| image          =
| caption        =
| director       = Sólveig Anspach
| producer       = Þorfinnur Ómarsson
| writer         = Sólveig Anspach Roger Bohbot Pierre-Erwan Guillaume Cécile Vargaftig
| starring       = Élodie Bouchez
| music          =
| cinematography = Benoît Dervaux
| editing        = Anne Riegel
| distributor    =
| released       = 19 September 2003
| runtime        = 91 minutes
| country        = France Iceland
| language       = French English
| budget         =
}}

Stormy Weather ( ) is a 2003 French-Icelandic drama film directed by Sólveig Anspach. It was screened in the Un Certain Regard section at the 2003 Cannes Film Festival.   

==Cast==
* Élodie Bouchez - Cora
* Didda Jónsdóttir - Loa
* Baltasar Kormákur - Einar
* Ingvar Eggert Sigurðsson - Gunnar (as Ingvar E. Sigurðsson)
* Christophe Sermet - Romain
* Natan Cogan - Grandfather
* Christian Crahay - Le médecin-chef
* Tinna Gudmundsdóttir - Tinna
* Davíð Örn Halldórsson - David
* Ólafía Hrönn Jónsdóttir - Gudrun
* Marina Tomé - Mademoiselle Ramirez

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 