Garden of Evil
 
{{Infobox film
| name           = Garden of Evil
| image          = Garden of evil.jpg Frank Fenton Cameron Mitchell Víctor Mendoza Rita Moreno 
| director       = Henry Hathaway
| producer       = Charles Brackett
| cinematography = Milton R. Krasner Jorge Stahl, Jr.
| music          = Bernard Herrmann James B. Clark
| distributor    = 20th Century Fox
| released       =  
| runtime        = 100 minutes
| language       = English
| country        = United States
| budget = $2,070,000 
| gross = $3.1 million (US rentals) 
}}
 soldiers of Cameron Mitchell, who are hired by a woman, portrayed by Susan Hayward, to rescue her husband.  The movie was directed by Henry Hathaway.

==Plot==
En route to California to prospect for gold, Hooker (Gary Cooper), Fiske (Richard Widmark), and Luke Daly (Cameron Mitchell) stop over in a tiny Mexican village. The three men and Vicente Madariaga (Victor Manuel Mendoza) are hired by a desperate Leah Fuller (Susan Hayward) to rescue her husband John (Hugh Marlowe), who is trapped in a gold mine in hostile Indian territory.

During the harrowing journey, the partys already frayed nerves are aggravated when the men become attracted to the woman. The group then arrives at the mine site — called the "Garden of Evil" because the Indians regard it as the domain of evil spirits. They find an injured, but living John Fuller.

As they leave, they are pursued by Apaches. Eventually, only Hooker, Fiske and Leah are left alive. At a narrow point in the road, the two men draw cards to see who will stay behind to hold off the Indians while the other two ride to safety. Fiske "wins" and succeeds in killing or driving off the enemy. After seeing that Leah is safe, Hooker returns to talk with a dying Fiske, who urges him to settle down with Leah.

==Cast==
* Gary Cooper as Hooker 
* Susan Hayward as Leah Fuller 
* Richard Widmark as Fiske 
* Hugh Marlowe as John Fuller  Cameron Mitchell as Luke Daly 
* Rita Moreno as Cantina singer

==Production==
The working title for the film was Volcano, it was changed because "there is an Italian pic of same title now playing U.S. art houses," a 1953 film directed by William Dieterle and starring Rossano Brazzi and Anna Magnani.

Robert L. Jacks was originally set to produce, but he left Twentieth Century-Fox to join Panoramic Productions and was replaced by Charles Brackett.

Outdoor sequences were shot on location in  , in the jungle areas near Acapulco, Parícutin Volcano, and the village of Guanajuato, Guanajuato|Guanajuato. Interior scenes were also shot at the Churubusco Studios in Mexico City.

==See also==
* List of American films of 1954

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 