Who's Been Sleeping in My Bed?
{{Infobox film
| name           = Whos Been Sleeping in My Bed?
| image          = WhosBeenSleepinginMyBed1963.jpg
| image size     =
| alt            =
| caption        = Theatrical release lobby card
| director       = Daniel Mann Jack Rose
| writer         = Jack Rose
| narrator       =
| starring       = Dean Martin Elizabeth Montgomery Carol Burnett Martin Balsam Jill St. John Richard Conte Macha Méril  Johnny Silver  Louis Nye Yoko Tani Jack Soo
| music          = George Duning
| cinematography = Joseph Ruttenberg
| editing        = George Tomasini
| distributor    = Paramount Pictures
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         =
}}

Whos Been Sleeping in My Bed? is a 1963 movie comedy starring Dean Martin, Elizabeth Montgomery, and Carol Burnett, and directed by Daniel Mann.

==Plot==
Jason Steele is an actor who plays a doctor on TV. He is so convincing at it, women of all kinds wont leave him alone.

His poker buddies are envious, but his fiancee, art teacher Melissa, isnt happy in the least. She is unaware that the women paying so much attention to Jason are the wives and girlfriends of his fellow poker players, who confuse his TV role with the real Jason.

Melissa takes her frustration to her friend Stella, who hatches a scheme. They will pretend Melissa has fallen in love with another man and decided to marry him, which will force Jasons hand. Complications ensue and a fake Mexico divorce must be arranged as well.

==Cast==
* Dean Martin as Jason Steele
* Elizabeth Montgomery as Melissa Morris
* Carol Burnett as Stella Irving
* Martin Balsam as Sanford Kaufman
* Jill St. John as Toby Tobler
* Richard Conte as Leonard Ashley
* Macha Méril as Jacqueline Edwards
* Johnny Silver as Charlie
* Louis Nye as Harry Tobler
* Yoko Tani as Isami Hiroti
* Jack Soo as Yoshimi Hiroti

==External links==
*   in the Internet Movie Database
*  

 

 
 
 
 
 
 
 
 
 

 