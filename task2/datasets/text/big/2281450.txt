Simon Birch
 
{{Infobox film
| name           = Simon Birch
| image          = Simon Birch.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Mark Steven Johnson
| producer       = Roger Birnbaum Laurence Mark
| screenplay     = Mark Steven Johnson
| based on       =  
| narrator       = Jim Carrey
| starring       = Ian Michael Smith Joseph Mazzello Jim Carrey Ashley Judd Oliver Platt
| music          = Marc Shaiman Aaron E. Schneider
| editing        = David Finfer
| studio         = Hollywood Pictures Caravan Pictures Buena Vista Pictures
| released       =  
| runtime        = 114 minutes  
| country        = United States
| language       = English
| budget         = $30 million
| gross          = $18,253,415 
}}
Simon Birch is a 1998 American comedy-drama film loosely based on A Prayer for Owen Meany by John Irving and was directed and written for the screen by Mark Steven Johnson.  The film stars Ian Michael Smith, Joseph Mazzello, Jim Carrey, Ashley Judd, and Oliver Platt. It omitted much of the latter half of the novel and altered the ending.

The movie does not share the books title at Irvings request; he did not believe that this novel could successfully be made into a film.  The name "Simon Birch" was suggested by him to replace that of Owen Meany.  The opening credits of the film state that it was "suggested by" Irvings novel.  The main plot centers on 12-year old Joe Wenteworth and his best friend Simon Birch.

== Cast ==
* Ian Michael Smith as Simon Birch
* Joseph Mazzello as Joe Wenteworth
* Jim Carrey as Adult Joe / Narrator
* Ashley Judd as Rebecca Wenteworth
* Oliver Platt as Ben Goodrich
* David Strathairn as Reverend Russell
* Dana Ivey as Grandmother Wenteworth
* Beatrice Winde as Hilde Grove
* Jan Hooks as Miss Leavey
* Cecilley Carroll as Marjorie
* Sumela-Rose Keramidopulos as Ann
* Sam Morton as Stuart
* Holly Dennison as Mrs. Birch
* Peter MacNeill as Mr. Birch
* Thomas J. Burns as Simon Birch Stunt Double

This was Smiths first role in film, and he has not done film acting since. He was chosen because of his small height, due to Morquio syndrome. His role in this film was suggested by a hospital worker in Chicago. After his parents read through the novel (A Prayer for Owen Meany) they agreed to let him work on the film. Sandra Bullock was originally cast for the role Ashley Judd was given.

==Production== French River. The films quarry scenes were shot at Elora, Ontario. The church featured in many parts of the film is in Lunenburg, Nova Scotia (town)|Lunenburg, Nova Scotia. Its black borders were painted white for the film. At the end of the film when it switches to the future, the borders are black. The baseball scene as well as many indoor scenes were filmed in Glen Williams, Ontario.

==Soundtrack== Epic Soundtrax:
 Babyface
# Bread and Butter" – The Newbeats
# "A Walkin Miracle" – The Essex
# "Mickeys Monkey" – Smokey Robinson / The Miracles
# "Can I Get a Witness" – Marvin Gaye
# "Fever (1956 song)|Fever" – Peggy Lee Up on the Roof" – The Drifters
# "Papas Got a Brand New Bag|Papas Got a Brand New Bag (Part 1)" – James Brown
# "The Nitty Gritty" – Shirley Ellis Nowhere to Run" – Martha and the Vandellas The Impressions
# "(Your Love Keeps Lifting Me) Higher and Higher" – Jackie Wilson
# "Simons Theme" – Marc Shaiman
# "Friends Forever" – Marc Shaiman
# "Simons Birth" – Marc Shaiman
# "Life Goes On" – Marc Shaiman

==Reception==
Simon Birch holds a 44% rating on Rotten Tomatoes, with the sites consensus calling the film "Overly mushy; tries too hard to pull at the heart-strings."  On Metacritic, the film has a 39/100 rating, indicating "generally unfavorable reviews."   Movie critic Gene Siskel rated it the 8th best movie of 1998.  

The film opened at #5 at the North American box office making $3,321,370 in its opening weekend. The film would go on to gross $18,253,415 domestically.   

==Home media== region 1 DVD contains a theatrical trailer for this film.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 