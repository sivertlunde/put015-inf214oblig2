Zephyr (film)
{{Infobox film
| name           = Zephyr
| image          = ZephyrFilmPoster.jpg
| alt            = 
| caption        = Theatrical Poster
| director       = Belma Baş
| producer       = Seyhan Kaya   Birol Akbaba 
| writer         = Belma Baş
| starring       = Şeyma Uzunlar Vahide Gördüm Sevinç Baş
| music          = 
| cinematography = Mehmet Zengin
| editing        = 
| studio         =
| distributor    =
| released       =  
| runtime        = 93
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          =
}}
Zephyr ( ) is a 2010 Turkish drama film directed by Belma Baş, which tells the story of a young girl Zefir’s longing for her mother while staying with her grandparents for summer holidays in the beautiful mountains of the Black Sea region. The film was selected for the 47th Antalya "Golden Orange" International Film Festival and the 35th Toronto International Film Festival, where it premiered. It is a thematic sequel to the directors previous short Boreas ( ).      

== Plot ==
Zephyr is a strong-willed little girl, spending her summer holidays in her grandparents’ house up in the Eastern Black Sea mountains. With her mother often away travelling, she has had to get used to being alone. Zephyr takes refuge in daydreams, creating whole worlds in her mind to cope with her mother’s absence.

Zephyr looks forward to her mother’s return. She spends her days helping her grandparents out with daily tasks, and roaming the outdoors. One day her mother finally and unexpectedly arrives. However, she comes back not to pick her daughter up, but to say goodbye to her before embarking on an even longer journey. Yet Zephyr has made up her mind not to part with her mother ever again.

== Release ==

=== Festival screenings ===
* 35th Toronto International Film Festival (September 9–19, 2010)
* 47th Antalya "Golden Orange" International Film Festival (October 9–14, 2010)
* 16th Festival on Wheels (December 3–19, 2010)
* Pondicherry University International Womens Film Festival (March 9–11, 2012)
   

==References==
 

==External links==
*  

 
 
 
 
 
 