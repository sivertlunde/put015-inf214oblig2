Unstoppable (2010 film)
 
{{Infobox film
| name           = Unstoppable
| image          = Unstoppable Poster.jpg
| caption        = Theatrical release poster
| alt            =
| director       = Tony Scott
| producer       = {{plainlist|
* Julie Yorn
* Tony Scott
* Mimi Rogers
* Eric McLeod Alex Young }}
| writer         = Mark Bomback
| starring       = {{plainlist|
* Denzel Washington
* Chris Pine
* Rosario Dawson
* Lew Temple
* Ethan Suplee 
* Kevin Dunn }}
| music          = Harry Gregson-Williams
| editing        = {{plainlist|
* Chris Lebenzon
* Robert Duffy }}
| cinematography = Ben Seresin
| studio         = {{plainlist|
* 20th Century Fox
* Dune Entertainment
* Scott Free Productions Prospect Park
* Millbrook Farm Productions  }}
| distributor    = 20th Century Fox
| released       =  
| runtime        = 98 minutes  
| country        = United States 
| language       = English
| budget         = $85–$95 million  
| gross          = $167,805,466 
}} action Thriller thriller film directed by Tony Scott, written by Mark Bomback, and starring Denzel Washington and Chris Pine. The film, loosely based on the real-life CSX 8888 incident, tells the story of a runaway freight train, and the two men (Washington and Pine) who attempt to stop it. It was Scotts final feature film before his death in 2012.

The film was released in the United States and Canada on November 12, 2010, and in the United Kingdom on November 24, 2010. It received mostly favorable reviews from film critics; it garnered a "Certified Fresh" rating on Rotten Tomatoes based upon aggregated reviews,  and a rating of "Generally favorable reviews" at Metacritic.  The film was nominated for an Academy Award for Best Sound Editing at the 83rd Academy Awards, but was beaten by Inception.

==Plot ==
 
 engineer Frank conductor Will Colson (Chris Pine) as they use AWVR locomotive #1206 to run a train outside the fictional city of Stanton, Pennsylvania. Their job is to pick up twenty cars at a zinc processing facility outside of Stanton but Will, who is dealing with personal issues, hooked five additional cars inadvertently.
 hostlers Dewey air brakes welder Ned Oldham (Lew Temple), who has not yet shown up at the yard, to go further down track to intercept the train at the nearest siding to 777s last known whereabouts.

Ned arrives just as Dewey and Gilleece head to the siding, with the train believed to be due into the area as it is still supposed to be coasting. However, Dewey reveals to Connie that he left the cab with the throttle at full power. Because of this, Connie realizes that 777 is not a coaster but a runaway train. She tells Dewey and Gilleece to catch up to the train before it reaches a split in the track and try to climb on board while Ned is dispatched to resume his pursuit. Dewey and Gilleece are able to reach the train, which by the time they catch it is running at 70 miles per hour. They manage to align their high-railing truck with the train, but just before Gilleece could board, the door hits a railroad signal in between the two tracks and almost breaks it off, making them unable to board the train from their truck.

Connie reports the runaway to Oscar Galvin (Kevin Dunn), vice-president of operations for AWVR, and coordinates with local police, sheriffs, and Pennsylvania State Police to ensure all grade crossings along the line are secured. Visiting Federal Railroad Administration safety inspector Scott Werner (Kevin Corrigan), who was scheduled to speak to a group of school children, alerts them to that the molten phenol being carried by eight tanker cars, plus the diesel fuel in the locomotives, on the train poses an immediate danger. Meanwhile, the runaway 777 has become a massive media event as television stations across Pennsylvania have picked up on the story.

Connie, aware that the train is heading toward a stretch of the main line that runs through a fairly highly populated area, suggests that the train be purposely derailed in a stretch of unpopulated farmland before it reaches the towns. Galvin, however, rejects the idea out of concern for the massive financial loss AWVR will take rather than the lives of innocent people that could be lost if the train should happen to derail. Instead, AWVR comes up with is own solution: a lashup of two engines, #7375 and #7346, driven by 26 year veteran engineer Judd Stewart (David Warshofsky) is sent ahead of the runaway at a slower rate of speed, with the belief that the lashup will force 777 to decelerate once it makes contact. Once that happens, AWVR employee and United States Marine Corps|U.S. Marine Ryan Scott (Ryan Ahern) is to rappel from a helicopter onto the train and climb into the cab to take control. Things seem to go well as Stewart was able to slow down the runaway train and Scott is lowered onto the locomotive, but just as he puts his feet down on it, 777 suddenly begins to shove the lashup again, causing Scott, still connected to the cable leading from the helicopter, to be pulled back from the locomotive and crash into the windshield of the second locomotive, #767, which nearly kills him. After this happens, Scott is lifted back into the helicopter, unconscious, as Stewart tries his best to slow the train down again, but 777 keeps on pushing the lashup faster. Then they try to abort the attempt by having the lashup go into a siding, but 777 bumps into the lashup again, causing the two diesels to jump the rails and crash just as it goes into the siding, exploding into a fireball that kills Stewart. 777 continues on, and the major concern becomes what could happen once the train reaches Stanton; the main line includes an elevated curve that cannot be navigated at that high a speed (dubbed the Devils Curve, due to its sharp curvature and limit of fifteen miles per hour): should 777 hit the curve, it will derail there and cause a massive disaster that could result in the destruction of the whole town and beyond.

As 1206 heads towards Fuller, Frank radios into the yard. Earlier, 1206 found out about 777 being in its path and was still operating under the belief that the train was a coaster. Connie informs Frank that the situation is now much worse than he had known. Frank had been commanded to pull off the track and into a siding several miles back but due to Wills earlier error, 1206 could not fit onto the siding and was forced onto a RIP track|Repair-In-Place track further along the line. They make it into the RIP siding track as the runaway speeds past them, smashing through the rear car. As it passes, Frank notices that 777s rear car has an open railway coupling|knuckle. He gets out of the cab and unhooks 1206, telling Will that if they go long hood forward toward 777, they could connect to the rear car and properly slow the train enough to where it can pass through the curve in Stanton without major damage.
 derailer near Arklow, a town along the route where 777 is headed next. Frank informs Galvin that the plan will not work, as 777 is too heavy and too fast to derail now. Unmoved, Galvin threatens to fire the engineer and conductor of 1206 if they proceed. Frank then reveals that he has already been fired; being forced into an early retirement by AWVR with half benefits, he admits to have nothing to lose.
 M16s as 777 passes their grade crossing. They fire several rounds off before ceasing fire at the risk of hitting the diesel fuel tank next to the button. As Frank foresaw, Galvins plan to derail the train outside Arklow fails as the train blows right through the derailers and sends shrapnel shooting into police cars parked by the tracks. Galvin is left dumbfounded by the failure of the derailers and has no choice but to rely on Frank and Will. Meanwhile, the area around the curve in Stanton is evacuated as 777 approaches as Ned continues his pursuit of the runaway with his police backup.
 dynamic brakes begin to reduce the speed of 777, the train is still moving too fast for the curve. Initially they manage to slow down the train but accelerate again since the train is too heavy and they are being dragged behind it.  Frank goes out and begins to engage each cars manual brakes in a last ditch attempt to slow 777 down as it nears the curve, eventually planning to get into the cab and stop the train there.  Things turn for the worse again when Will sees 1206s load ammeter going from motoring to zero in just a few seconds, and then suddenly to full braking in just one second, causing the locomotives dynamic brakes to blow out and the runaway 777 to pick up speed again dragging 1206 with it.  Meanwhile, Will uses 1206s independent brake to keep the train on the rails as it speeds through the curve. As it does, it leans dangerously to the side, causing a load of pipes to fall off one of its two bulk-head flatcars and land dangerously close to the large gas tanks. The locomotive even takes out a few electrical power poles, but it eventually makes it through the curve. But although the train makes it through the curve without falling from the track, 777 is still out of control and Frank encounters a gap in between one of the tankers hes standing on top of and the bulk-head flatcar that still has its load of pipes that is too wide to cross and thus cannot get to the cab. To make matters worse, the whole train is headed toward Stantons yard near the end of the line; any one of its switch tracks will derail the locomotives and contaminate the entire yard.

Ned arrives in his truck, and pulls onto a parallel road next to the line. Will jumps onto the truck, and is driven to the front of the train, where he jumps into 777 and is finally able to stop the train. Frank, Will and Ned are celebrated as heroes, and the two reunite with their families. A pre-credit  . It is unknown what happened to Galvin after the events, but it is possible that he was fired from the railroad for how he mishandled the events.

== Cast ==
* Denzel Washington as Frank Barnes, a veteran railroad engineer
* Chris Pine as Will Colson, a young train conductor
* Rosario Dawson as Connie Hooper, a train yardmaster
* Lew Temple as Ned Oldham, a railroad lead welder
* Ethan Suplee as Dewey, a hostler who accidentally instigates the disaster
* Kevin Dunn as Oscar Galvin, vice-president of AWVR train operations FRA inspector who helps Frank, Will, and Connie
* Kevin Chapman as Bunny, a railroad operations dispatcher
* T. J. Miller as Gilleece, Deweys friend, also a hostler
* Jessy Schram as Darcy Colson, Wills estranged wife
* David Warshofsky as Judd Stewart, a veteran engineer who dies in an attempt to slow the runaway
* Victor Gojcaj as Groundman, a railroad ground specialist
* Meagan Tandy and Elizabeth Mathis as Maya and Nicole Barnes, Franks daughters who work as waitresses at Hooters US Marine veteran of the war in Afghanistan who attempts unsuccessfully to board the runaway from a helicopter
* Aisha Hinds as Railroad Safety Campaign Coordinator
* Jeff Wincott as Jesse Colson, Wills brother who helps him on his family situation

== Production ==
Unstoppable suffered various production challenges before filming could commence, including casting, schedule, location and budgetary concerns.      

In June 2007, 20th Century Fox was in negotiations with Martin Campbell to direct the film, {{cite news
| url=http://www.variety.com/article/VR1118005527.html
| title=Fox dealing with Unstoppable budget
| last=Fleming |first= Michael
| date=June 7, 2007
|work=Variety
| accessdate=August 17, 2009
}}  and he was attached as director, until March 2009 when Tony Scott came on board as director.   
In April, both Denzel Washington and Chris Pine were attached to the project. {{cite news
| url=http://www.variety.com/article/VR1117966502.html
| title=Fox train thriller just Unstoppable
| last=Fleming |first= Michael
| date=June 29, 2009
|work=Variety
| accessdate=August 17, 2009
}} 

The original budget had been trimmed from $107 million to $100 million, but Fox wanted to reduce it to the low $90 million range, asking Scott to cut his salary from $9 million to $6 million and wanting Washington to shave $4 million off his $20 million fee.   
Washington declined and, although attached since April,    formally withdrew from the project in July, citing lost patience with the films lack of a start date.  Fox made a modified offer as enticement, and he returned to the project two weeks later.     
 Martins Ferry, Mingo Junction, Steubenville and Port Matilda, Port Allegany Portville and Western New York and Pennsylvania Railroads Buffalo Line was used for two months during daylight, while the railroad ran its regular freight service at night.    The real-life bridge and elevated curve in the climactic scene is the B & O Railroad Viaduct in Bellaire, Ohio. {{cite news
| date=November 12, 2010
| first=Barbara 
| last=Vancheri
| title=Unstoppable director Tony Scott loved filming in Pennsylvania
| url=http://www.post-gazette.com/pg/10316/1102606-60.stm
| work=Pittsburgh Post-Gazette
| publisher=Block Communications
| accessdate=December 9, 2010
}}  Wilkins Township, a Pittsburgh suburb, featuring 10 Hooters Girls from across the United States. Other interior scenes were shot at 31st Street Studios (then the Mogul Media Studios) on 31st Street in Pittsburgh. Filming began on August 31, 2009, {{cite news
| date=August 17, 2009
|author=Gayle Fee & Laura Raposa
| title=We Hear: Kevin Chapman, Denzel Washington, Tom Werner & more...
| url=http://www.bostonherald.com/track/inside_track/view/20090817we_hear_kevin_chapman_denzel_washington_tom_werner__more
| publisher=Boston Herald
}}  for a release on November 12, 2010.

Filming was delayed for one day when part of the train accidentally derailed on November 21, 2009. 

The locomotives used on the runaway train, 777 and trailing unit 767, were played by GE AC4400CWs leased from the Canadian Pacific Railway. CP #9777 and #9758 played 777 and 767 in early scenes, and CP #9782 and #9751 were given a damaged look for later scenes.  These four locomotives were repainted by Canadian Pacific in standard colors following the filming, but the painted pilot warning stripes from the AWVR livery were left untouched and remained visible on the locomotives. 
{{cite web|url=http://cs.trains.com/TRCCS/forums/t/182617.aspx 
|title=Unstoppable AWVR decals? – Forums – Model Railroader Magazine – Online Community: Forums and Galleries |publisher=Cs.trains.com 
|accessdate=2012-06-12}}  
The plow on 9777 appears to have been repainted black as of 2013. 
{{cite web|url=http://www.rrpicturearchives.net/showPicture.aspx?id=3486954|accessdate=2014-02-22
|title=CP 9777
|publisher=RRPictureArchives.net}}. 

Most of the other locomotives seen in the film, including chase locomotive #1206, and the lashup locomotives used in an attempt to stop the train, #7375 and #7346, were played by    rebuilt from an EMD GP9. Passenger coaches carrying schoolchildren were provided by the Orrville Railroad Heritage Society. 

== Inspiration ==
 
Unstoppable was inspired by the 2001 CSX 8888 incident, in which a runaway train ultimately traveled   through northwest Ohio. Led by CSX Transportation EMD SD40-2|SD40-2 #8888, the train left the Walbridge, Ohio, rail yard with no one at the controls, after the hostler got out of the slow-moving train to correct a misaligned switch, mistakenly believing he had properly set the trains dynamic braking system, much as his counterpart (Dewey) in the film mistakenly believed he had properly set the locomotives throttle.
 coupled onto the runaway and slowly applied its brakes. Once the runaway was slowed down to   miles per hour, CSX trainmaster Jon Hosfeld ran alongside the train and climbed aboard, shutting down the locomotive. The train was stopped just southeast of Kenton, Ohio. No one was seriously injured in the incident. 

When the film was released, the Toledo The Blade (newspaper)|Blade compared the events of the film to the real-life incident. "Its predictably exaggerated and dramatized to make it more entertaining," wrote David Patch, "but close enough to the real thing to support the Inspired by True Events announcement that flashes across the screen at its start." He notes that the dead man switch would probably have worked in real life despite the unconnected brake hoses, unless the locomotive, or independent brakes, were already applied.  As explained in the movie, the dead mans switch failed because the only available brakes were the independent brakes, which were quickly worn through, similar to CSX 8888.  The film exaggerates the possible damage the phenol could have caused in a fire, and he found it incredible that the fictional AWVR freely disseminated information such as employees names and images and the cause of the runaway to the media. In the real instance, he writes, the cause of the runaway was not disclosed until months later when the National Transportation Safety Board released its report, and CSX never made public the name of the engineer whose error let the train slip, nor what disciplinary action it took. 

== Soundtrack ==
 
The film score was composed by Harry Gregson-Williams and the soundtrack album was released on December 7, 2010.

== Release ==

=== Marketing ===
 
A trailer was released online on August 6, 2010.  The film went on general release November 12, 2010.

=== Home media ===
 
Unstoppable was released on DVD and Blu-ray on February 15, 2011. 

== Reception ==

=== Critical response ===
 
Unstoppable received positive reviews from film critics. Rotten Tomatoes gives it a score of 86% based on 177 reviews, with an average score of 6.9/10. The film is "Certified Fresh", and the critical consensus is: "As fast, loud, and relentless as the train at the center of the story, Unstoppable is perfect popcorn entertainment—and director Tony Scotts best movie in years." {{cite web
| title=Unstoppable Movie Reviews, Pictures
| url=http://www.rottentomatoes.com/m/unstoppable-2010
| publisher=Flixster
| work=Rotten Tomatoes
| accessdate=2010-12-24
}}  Metacritic gives the film a score of 69% based on reviews from 32 critics indicating "generally favorable reviews". {{cite web
| title=Unstoppable Reviews, Ratings, Credits
| url=http://www.metacritic.com/movie/unstoppable
| work=Metacritic
| publisher=CBS
}} 

Film critic Roger Ebert rated the film three and a half stars out of four, remarking in his review, "In terms of sheer craftsmanship, this is a superb film." {{cite news
| date=November 10, 2010
| last=Ebert
| first=Roger
| authorlink=Roger Ebert
| title=Unstoppable
| url=http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20101110/REVIEWS/101119995
| newspaper=Chicago Sun-Times
| accessdate=November 11, 2010
}} 
In The New York Times, Manohla Dargis praised the films visual style, saying that Scott "creates an unexpectedly rich world of chugging, rushing trains slicing across equally beautiful industrial and natural landscapes." {{cite news
| date=November 11, 2010
| authorlink=Manohla Dargis
| last=Dargis
| first=Manohla
| title=I Think I Can: Trying to Stop a Crazy Train Hurtling to Disaster
| url=http://movies.nytimes.com/2010/11/12/movies/12unstop.html
| page=2
| newspaper=The New York Times
| accessdate=November 14, 2010
}} 

The Globe and Mail in Toronto was more measured. While the movies action scenes "ha  the greasy punch of a three-minute heavy-metal guitar solo", its critic felt the characters were weak. It called the film "an opportunistic political allegory about an economy thats out of control and industries that are weakened by layoffs, under-staffing and corporate callousness." {{cite news
| date=November 12, 2010
| title=Unstoppable: Like derivatives trading, this train is out of control
| url=http://www.theglobeandmail.com/news/arts/movies/unstoppable-like-derivatives-trading-this-train-is-out-of-control/article1795593
| newspaper=The Globe and Mail
| location=Toronto, Canada
| publisher=CTVGlobeMedia
| accessdate=November 14, 2010
}} 

=== Box office === The Taking of Pelham 123, another Tony Scott film involving an out-of-control train starring Denzel Washington. Pelham took in $23.4&nbsp;million during its opening weekend in the United States and Canada. {{cite news
| date=November 11, 2010
| title=Movie Projector: Unstoppable seeks to derail Megamind as Morning Glory looks dim
| url=http://latimesblogs.latimes.com/entertainmentnewsbuzz/2010/11/movie-projector-unstoppable-seeks-to-derail-megamind-as-morning-glory-looks-dim.html
|quote=One person close to the production said "Unstoppable" cost about $100 million after the benefit of tax credits, though another person close to Fox said the final budget was closer to $85 million.
|work=Los Angeles Times
}} 
Unstoppable had a strong opening night on Friday November 12, 2010, coming in ahead of Megamind with a gross of $8.1 million. However, Megamind won the weekend, earning $30 million to Unstoppable s $23.9 million. {{cite web
| url=http://latimesblogs.latimes.com/entertainmentnewsbuzz/2010/11/box-office-megamind-outraces-unstoppable-to-stay-on-top-of-box-office.html
| title=Box office: No. 1 Megamind stops Unstoppable
|work=Los Angeles Times
| accessdate=December 9, 2010
}} 
Unstoppable performed slightly better than The Taking of Pelham 123 did in its opening weekend. As of April 2011, the film had  earned $167,805,466 worldwide.
 {{cite web
| title=Unstoppable (2010)
| url=http://boxofficemojo.com/movies/?id=unstoppable.htm
| work=Box Office Mojo
|publisher=Amazon.com
| accessdate=December 9, 2010
}}  {{cite web
| date=November 13, 2010
| first=Brandon
| last=Gray
| title=Friday Report: Unstoppable Squeaks by Megamind
| url=http://boxofficemojo.com/news/?id=2980&p=.htm
| work=Box Office Mojo
|publisher=Amazon.com
}} 

=== Awards ===
  Best Sound Editing (Mark Stoeckinger) category at the 83rd Academy Awards and nominated for Teen Choice Award for Choice Movie - Action.    

==See also== Silver Streak"
*"Narrow Margin" Runaway Train"

== References ==
 

== External links ==
 
*  
*  
*  
*  
*   at Metacritic
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 