Orizuru Osen
{{Infobox film
| name           = Orizuru Osen
| image          = Orizuru Osen DVD cover.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD cover of 2009 release
| director       = Kenji Mizoguchi
| producer       = Masaichi Nagata
| writer         = Tatsunosuke Takashima
| screenplay     = 
| story          = 
| based on       =  
| narrator       = Suisei Matsui Midori Sawato
| starring       = Isuzu Yamada Daijiro Natsukawa Ichiro Yoshizawa
| music          = 
| cinematography = Minoru Miki
| editing        = 
| studio         = Daiichi Eiga
| distributor    = Shochiku
| released       =  
| runtime        = 90 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}

  ( ) is a 1935 black and white Japanese silent film directed by Kenji Mizoguchi, starring Isuzu Yamada.  It is based on Kyōka Izumis novel Baishoku Kamo Nanban. 

Like most Japanese silent films, it played with benshi accompaniment. The film centers on the theme of the strength of a woman who gives everything to the man she loves; a theme which Mizoguchi explored his whole life. The moving camera technique and bold retrospective scenes greatly reflect Mizoguchis experimental approach. 

==Cast==
* Isuzu Yamada as Osen
* Daijiro Natsukawa as Sokichi Hata
* Ichiro Yoshizawa as Ukiki
* Shin Shibata as Kumazawa
* Genichi Fujii as Matsuda
* Eiji Nakano as the Professor

==Reception==
Japanese film scholar Chika Kinoshita noted that the film occupies a special place within the critical reception of Kenji Mizoguchis oeuvre and it has been signed out as one of the earliest embodiments of his style in the late 1930s. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 

 