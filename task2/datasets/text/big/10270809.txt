Ziegfeld Girl (film)
{{Infobox film
| name           = Ziegfeld Girl
| image          = Ziegfeld Girl Movie Poster.jpg
| image_size     = 
| caption        = 1941 US Theatrical Poster
| director       = Robert Z. Leonard
| producer       = Pandro S. Berman
| writer         = William Anthony McGuire (story)  Marguerite Roberts (screenplay)  Sonya Levien (screenplay)
| narrator       =
| starring       = James Stewart  Judy Garland  Hedy Lamarr  Lana Turner 
| music          = Herbert Stothart
| cinematography = Ray June Joseph Ruttenberg
| editing        = Blanche Sewell
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 132 minutes
| country        = United States
| language       = English budget = $1,468,000  .  gross = $3,101,000 
}}
 Tony Martin, Jackie Cooper, Eve Arden, and Philip Dorn. Released by MGM, it was directed by Robert Z. Leonard and featured musical numbers by Busby Berkeley.

Set in the 1920s, the film tells the parallel stories of three women who become performers in the renowned Broadway show the Ziegfeld Follies. It was intended to be a 1938 sequel to the 1936 hit The Great Ziegfeld, and recycled some footage from the earlier film.

==Synopsis==
The story deals with three showbiz hopefuls - Susan Gallagher (Judy Garland), Sandra Kolter (Hedy Lamarr) and Sheila Regan (Lana Turner) - and their efforts to attain the lofty status of "Ziegfeld Girl." 

==Cast==
*James Stewart as Gilbert Young
*Judy Garland as Susan Gallagher
*Hedy Lamarr as Sandra Kolter
*Lana Turner as Sheila Regan Tony Martin as Frank Merton
*Jackie Cooper as Jerry Regan Ian Hunter as Geoffrey Collis
*Charles Winninger as Pop Gallagher
*Eve Arden as Patsy Dixon
*Edward Everett Horton as Noble Sage
*Philip Dorn as Franz Kolter Paul Kelly as John Slayton
*Dan Dailey as Jimmy Walters
*Fay Holden as Mrs Regan
*Al Shean as Al

== Musical Numbers ==

# "Overture" — played by Orchestra and sung by Chorus
# "Laugh? I Thought Id Split My Sides" — sung and danced by Judy Garland and Charles Winninger
# "You Stepped Out of a Dream" — sung by Tony Martin and Chorus
# "Im Always Chasing Rainbows" — sung by Judy Garland
# "Caribbean Love Song" — sung by Tony Martin and Chorus Antonio and Rosario, then sung and danced by Judy Garland and Chorus
# "Mr. Gallagher and Mr. Shean" — performed by Charles Winninger and Al Shean
# "Ziegfeld Girls/You Gotta Pull Strings" — sung by Judy Garland and Chorus
# "You Stepped Out of a Dream" (reprise) — sung by Tony Martin
# "You Never Looked So Beautiful" — sung by Judy Garland and Chorus

==Box Office==
According to MGM records the film earned $1,891,000 in the US and Canada and $1,210,000 elsewhere resulting in a profit of $532,000. 
==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 