Shikshanachya Aaicha Gho
{{Infobox film name = Shikshanacha Aaicha Gho image =Shikshanachya Aaicha Gho.jpg caption = Theatrical release poster director = Mahesh Manjrekar producer =   screenplay =  story = starring =   editing =  cinematography =  music = Ajit-Atul-Sameer studio =   distributor = Everest Entertainment released =   country = India runtime =   language = Marathi budget         =  gross          =   (lifetime) 
}} Marathi film directed by Mahesh Manjrekar starring Sachin Khedekar, Bharat Jadhav, Saksham Kulkarni, Gauri Vaidya, Siddharth Jadhav and Kranti Redkar. The film was released on 15 January 2010.  Films music composed by trio Ajit-Atul-Sameer. After Astitva with 9 years long gap Mahesh Manjrekar directed Marathi film.This film was later remade in Tamil & Telugu as Dhoni (film)|Dhoni and in Bengali as "Chalo Paltai" starring Prosenjit Chatterjee.

== Synopsis ==
Shrinivas Rane, is an average student, born with average academic intelligence, but when it comes to cricket he is a born genius. His extraordinary talent was lost on his father who like millions of other parents believed that a childs intellect is only reflected in their mark sheet, which eventually will give them a “secure future”. So he begins his quest to make his son the brightest and best student in the world. But Shree can’t handle this pressure and it reflects on his psyche deteriorating the relation between father and son, upon which the father in a fit of anger does something which makes him repent later.

== Cast ==
* Sachin Khedekar ..... CM
* Bharat Jadhav.....Madhukar Rane
* Saksham Kulkarni .....Shrinivas Rane-Son
* Gauri Vaidya.....Durga Rane-Daughter
* Siddharth Jadhav......Ebrahim Bhai
* Kranti Redkar.......Nalini
* Mahesh Manjrekar......Doctor

== Production ==
Shikshanacha Aaicha Gho was released in India on 15 January 2010.

== Music ==
Marathi music trio Ajit-Atul-Sameer scored the music. The soundtrack was released on 5 December 2009.

==Controversy ==
Maratha Mahasangh took an abjection over the name "aaiycha". According to them, it was abusive and would have an adverse effect on children. Later Manjrekar agreed to make particular announcement before starting of film  and dispute ends.
 

==Remake==
Prakash Raj remade this movie in Tamil & Telugu As "Dhoni". It was speculated Mahesh Manjarekar wants remake this movie in Hindi with Bollywood Bhai Salman Khan in lead.

==References==
 

==External links==
*  

 
 
 
 