The Sorcerer's Apprentice (2002 film)
 

{{Infobox film
| name           = The Sorcerers Apprentice
| image_size     =
| image	=	The Sorcerers Apprentice FilmPoster.jpeg
| caption        =
| director       = David Lister
| writer         = Brett Morris
| producer       = Paul Matthews
| starring       = Byron Taylor Robert Davi Kelly LeBrock Mark Thomas
| cinematography = Vincent G. Cox
| country        = South Africa English
| budget         =
}} 2002 fantasy film. Although set in England, the film was shot on location in South Africa.

==Synopsis== Morgana (Kelly talisman from the wizard Merlin, with which she intends to destroy the world. For the last fourteen hundred years she has failed... now she intends to conquer all. 

Young Ben Clark (Byron Taylor) moves with his parents to a new town, where he befriends his elderly magician neighbor, Milner (Robert Davi). Ben has a natural talent for magic and wants to learn all that he can from this old man. Ben carries the same scar as the original staff-bearer 1,400 years before. Both Morgana and Milner, who is revealed to be Merlin, see this as a sign that this time, the battle between good and evil will be stronger and harder than ever. 

Ben must make his own choice between good and evil as he is drawn into a battle and must draw on his own spirit and magic to decide which path to follow and hence, the fate of the world as we know it.

==See also==
*Sorcerers Apprentice (disambiguation)
*The Sorcerers Apprentice (2010 film)|The Sorcerers Apprentice (2010 film)

==External links==
* 

 

 
 
 
 
 
 

 
 