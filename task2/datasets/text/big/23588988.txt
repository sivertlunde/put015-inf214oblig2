We Go Way Back
 
 
{{Infobox film
| name           = We Go Way Back
| image          = 
| caption        = 
| director       = Lynn Shelton
| producer       = Peggy Case AJ Epstein
| writer         = Lynn Shelton
| narrator       = 
| starring       = Aaron Blakely Maggie Brown Basil Harris Amber Hubert Robert Hamilton Wright
| music          = Laura Viers Jason Staczek
| cinematography = Benjamin Kasulke
| editing        = Lynn Shelton Michelle Witten
| studio         = Geisha Years, LLC
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
We Go Way Back is a 2006 American film starring Aaron Blakely, Maggie Brown, Basil Harris, Amber Hubert and Robert Wright. It was written and directed by Lynn Shelton.

==Plot== specter of her thirteen-year-old self.  The dialog between the 23 year old actress and her younger self begins in memory, and then climaxes in an apparitional experience with the specter of her own, repressed, precocious youth.   

==Production==
The film began shooting in 2004 in Seattle.

In 2009, the company Geisha Years LLC was formed by Shelton specifically to purchase the film from TFC Productions 1, LLC, which was the original producer for the film. Geisha plans to secure a distribution deal for the film.

==Cast==
* Aaron Blakely – Jeff
* Maggie Brown – Kate-at-13
* Basil Harris – Pete
* Amber Hubert – Kate-at-23
* Robert Hamilton Wright – The Director
* Sullivan Brown – Jeremy
* Russell Hodgkinson – Frank

==Reception and awards==
* Kodak Vision Award for Best Cinematography – 2006 Slamdance International Film Festival
* Grand Jury Award for Best Narrative Feature – 2006 Slamdance International Film Festival 

We Go Way Back has been described by critics as "polished" and "impressionistic". 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 