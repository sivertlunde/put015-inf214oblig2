Janam
{{Infobox film
| name           = Janam
| image          = 
| image_size     = 
| caption        = 
| director       = Mahesh Bhatt
| producer       = Mudra Videotech
| writer         = Mahesh Bhatt (Story and screenplay) Suraj Sanim (Dialogues)
| story  =       
| dialogue       =  Suraj Sanim 
| starring       = Kumar Gaurav Anupam Kher Anita Kanwar Shernaz Patel
| music          = Ajit Verman
| lyrics         = 
| cinematography =  Pravin Bhatt
| editing        = Renu Saluja
| distributor    = 
| released       = 04 Jan 1985
| runtime        = 
| country        = India
| language       = Hindi
}} Hindi film directed and written by Mahesh Bhatt. It is one of the autobiographical films Bhatt created in the early part of his career. 

==Overview==
Janam was created after Bhatts acclaimed early films Arth (film)|Arth (1982) and Saaransh (1984), and established him as a promising new director of the Indian film industry, long before he ventured into more commercial endeavours.

This film was followed by other films with similar themes from Bhatts personal life, including Naam (1986 film)|Naam (1986), Zakhm (1998) and Woh Lamhe (2006), films for which he provided the story.

==Cast==
* Kumar Gaurav
* Anupam Kher
* Shernaz Patel
* Kitu Gidwani
* Anita Kanwar
* Deepak Qazir
* Askash Khurana

==External links==
*  
  
 
 

 