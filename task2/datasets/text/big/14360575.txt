Hoa-Binh (film)
{{Infobox film
| name           = Hoa-Binh
| image          = 
| caption        = 
| director       = Raoul Coutard
| producer       = Gilbert de Goldschmidt
| writer         = Raoul Coutard Françoise Lorrain
| starring       = Phi Lan
| music          = Michel Portal
| cinematography = Georges Liron
| editing        = Victoria Mercanton
| distributor    = Warner Bros.
| released       =  
| runtime        = 93 minutes
| country        = France
| language       = French
| budget         = 
}}
Hoa-Binh is a 1970 French film directed by Raoul Coutard and based on a novel by Françoise Lorrain. It was nominated for the Academy Award for Best Foreign Language Film.    It was also entered into the 1970 Cannes Film Festival, where Coutard won the prize for Best First Work.   

==Cast==
* Phi Lan as Hung
* Huynh Cazenas as Xuan
* Le Quynh as Father
* Marcel Lan Phuong as Nam
* Bui Thi Thanh as Tran Thi Ha
* Tran Van Lich as Political Commissioner
* Anh Tuan as Viet Cong Officer
* Danièle Delorme as French Nurse
* Kieu Anh as Vietnamese Nurse
* Xuan Ha as Mother

==See also==
* List of submissions to the 43rd Academy Awards for Best Foreign Language Film
* List of French submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 