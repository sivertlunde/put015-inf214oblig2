Mazha Peyyunnu Maddalam Kottunnu
{{Infobox film
| name           = Mazha Peyyunnu Maddalam Kottunnu
| image          = Mazha Peyyunnu Maddalam Kottunnu.jpg
| caption        = VCD Cover
| director       = Priyadarshan
| producer       = Edappazhanji Velappan Sreenivasan
| story          = Jagadish Mukesh Lizy Lizy Sreenivasan Jagathi Sreekumar Kuthiravattam Pappu Maniyanpilla Raju
| music          =
| released       =  
| runtime        = 129 minutes
| cinematography = S. Kumar
| editing        = N. Gopalakrishnan
| studio         = Seven Arts
| country        = India
| language       = Malayalam
}}
 1986 Malayalam Malayalam comedy film directed by Priyadarshan. Starring Mohanlal, Mukesh (actor)|Mukesh, Lizy (actress)|Lizy, Sreenivasan (actor)|Sreenivasan, Jagathi Sreekumar, Maniyanpilla Raju, and Kuthiravattam Pappu in the main roles. This film was a huge hit and considered as one of the best comedy films in Malayalam cinema.

==Plot==
Madhavan, who calls himself M.A.Dhavan (Sreenivasan (actor)|Sreenivasan) is back in Kerala after spending a few years getting his MBA in USA.  His schoolmate Shambhu (Mohanlal) is now working as a driver at his house, whom the now vain and snobbish Madhavan treats with disdain. Madhavans parents want him to get married to Shobha (Lizy (actress)|Lizy), daughter of Sardar Krishna Kurup (Jagathy Sreekumar), a rich and aristocratic businessman. Madhavan, who wants to observe his fiancée from a distance, decides to go to her house disguised as a driver and makes his driver Shambhu act as himself. Just minutes after they set out from he house, his parents telephone Krishna Kurupp about their sons scheme. Various mishaps on way end up with Madhavan discarding his plan and deciding to visit as himself.  Unaware of the change in plan, Kurup and wife have the driver Shambhu mistaken for the prospective groom and Madhavan as the driver.  The prospective in-laws dote over Shambu, ignoring Madhavan, who is unaware of the impression that Kurup and wife are under.  In the meantime Shobha, who also mistakes Shambhu as the fiance, falls in love with him.  At the same time, Sardar Koma Kurup (Kuthiravattam Pappu), is in a big fight with Krishna Kurup for past long years. Both are trying to capture the power at the Nethaji Club, a reputed social club for personal reasons. Damu (Maniyanpilla Raju) enters inside the house of Koma Kurup and wins his heart. But the real intention of Damu is to kill Koma Kuruppu and marry his daughter Aruna (Priya (actress)|Priya), so that he could grab the whole property. But one after another, the attempts of Damu to kill Koma Kurup fails, with each time, he hurting himself. It was then, Shivan (Mukesh (actor)|Mukesh), a friend of Damu arrives at the house of Koma Kurup as the brother of Damu, who also makes everyone believe that he is a heart patient. He succeeds in winning the heart of Aruna, destroying the plans of Damu. Thus the two love stories, one between Shambhu and Shobha and the other between Shivan and Aruna, blooms side by side. This unites Shambhu and Shivan against Madhavan and Damu. Madhavan  hires Kadathanatt Pappan Gurukkal (Cochin Haneefa), a known goon to beat up Shambhu, but instead, he  himself got beaten up by Shambhu. This incident makes Shambhu to force Krishna Kurup to conduct the marriage within three days. But, everything turns more messy as the parents of Madhavan makes a surprise visit at the house of Sardar Krishna Kurup. They finds the foul play of Shambhu, and Krishna Kurup, who realizes the mistake decides to get his daughter married to Madhavan. Shobha, who by this time is in love with Shambhu is adamant that she wont marry Madhavan.  The marriage venue and date is fixed. At the same time Sardar Koma Kurup also finds out that his daughter is in love with Shivan, a jobless chap, which he objects to. At the marriage venue both Shambhu and Shivan are denied entry. They calls up police by informing that there is gold hidden by Krishna Kurup at the marriage hall. Police enters the venue and then happens a long fight packed with several comic incidents. In the end, with the help of police inspector (Jagadish), Shambhu marries Shobha and Shivan marries Aruna. This tragic incident unites both Krishna Kurup and Koma Kurup. Everything ends well and both couples starts their married life with the blessing of all others including Madhavan and Damu.

==Cast==
*Shambu - Mohanlal Mukesh
*M.A. Sreenivasan
*Damu - Maniyanpilla Raju
*Sardar Krishna Kurup  - Jagathy Sreekumar
*Koma Kuruppu - Kuthiravattam Pappu Lizy
*Aruna Priya
*M.A.Dhavans father - C. I. Paul
*M.A.Dhavans mother - Sukumari
*Thankam - Lalithasree Bahadur
*Inspector - Jagadish
*Kadathanattu Pappan Gurukal - Cochin Haneefa
*Guest Role - Mammootty

==Trivia==
*Upon release, this film was a huge hit at box office. Without a strong plot or any emotional scenes as it is a senseless comedy flick. It is usually categorized as one of the best comedy films to have ever happened in Malayalam cinema. The repeated telecasts on TV channels has made it a cult film. 
*The performance of Sreenivasan as M. A. Dhavan is considered as his best in his career.
*The story of this film was by Jagadeesh. The screenplay and dialogues were written by Sreenivasan.
*Mammooty appears in a guest appearance for just one scene in the climax. Miami Beach is 1,704&nbsp;km (1,059 miles) through the Interstate 95 highway     (In reference to a popular dialogue from this movie, that starts with the question How many kilometers from Washington D.C. to Miami Beach?). Ironically, the metric system is not of common use in day to day life in the United States.

==References==
 

==External links==
*  

 

 
 
 
 
 
 