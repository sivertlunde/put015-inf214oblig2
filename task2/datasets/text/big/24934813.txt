Slackistan
{{Infobox film
| name           = Slackistan
| alt            =  
| image	=	Slackistan FilmPoster.jpeg
| caption        = 
| director       = Hammad Khan
| producer       = Hammad Khan
| writer         = Hammad Khan Shandana Ayub
| starring       = Shahbaz Hamid Shigri Aisha Linnea Akthar Ali Rehman Khan Shahana Khan Khalil Osman Khalid Butt Khalid Saeed Rafey Alam
| music          = 
| cinematography = 
| editing        = 
| studio         = Big Upstairs Films
| distributor    = Big Upstairs Films
| released       =   
| runtime        = 
| country        = Pakistan
| language       = English
| budget         = Rs. 20 Million
| gross          = 
}}
Slackistan is an independent film directed by London-based filmmaker, Hammad Khan.  The film stars Shahbaz Hamid Shigri, Aisha Linnea Akhtar, Ali Rehman Khan, Shahana Khan Khalil, Osman Khalid Butt, Khalid Saeed and Rafey Alam.  The film is distributed by Big Upstairs Films.  Slackistan was released in the UK in 2010,   and has also played at a number of festivals, in such locations as London, Abu Dhabi, New York, San Francisco and Goa. It has been banned in Pakistan.   

==Plot==
A young man in his early twenties juggles his dreams to be a filmmaker with his family life, his best friends troubles, the girl hes interested in and living in Pakistan during political turmoil.

==Cast==
*Shahbaz Hamid Shigri as Hasan
*Aisha Linnea Akhtar as Aisha
*Ali Rehman Khan as Sherry
*Shahana Khan Khalil as Zara
*Osman Khalid Butt as Saad
*Khalid Saeed as Mani
*Rafey Alam as Zeeshan
*Adil Omar as himself
*Uzair Jaswal as himself

==Controversy==
Slackistan has not been released in Pakistan because the director refused to make cuts to the film as requested by the countrys Central Board of Film Censors (CBFC) on January 25, 2011. According to The Guardian, the CBFC objects to the movie because it has swear words in English and Urdu, and "contains the words Taliban and lesbian. Scenes showing characters drinking (fake alcohol for the filming, incidentally) and a joke about beards (as in, my beard is longer than your beard) made between characters talking hypothetically about a fancy dress party. These are not the CBFCs only objections, but the main ones its highlighted."  The CBFC have also stated that, even if all cuts are made as demanded, the film would still receive a restrictive adults-only ‘18+’ rating." 

In a press release, director Hammad Khan stated “The censor board’s verdict is oppressive, arbitrary and steeped in denial about life outside their government offices. Maybe the establishment’s view is that young Pakistanis saying words like Taliban and Lesbian represent a more potent threat than the bullets and bombs that are, day by day, finding increasing legitimacy in the country.” 

Members of the Slackistan cast have publicly expressed their disagreement with the CBFCs decision.  “This objection honestly reinforces the feeling of being voiceless that seems to be lingering in the country these days. We really are stripped of our basic right to express ourselves,” actress Shahana Khan Khalil said. “I also find it highly hypocritical for our cinemas to be allowed to show both Hollywood and Bollywood films that depict everything and a lot more are never banned by the censor board.” Actor Shahbaz Shigri said, “We   haven’t developed the ability to scrutinize ourselves. We point fingers at others rather than correcting ourselves. We don’t laugh at ourselves. This limits our film industry and young film makers that will never get through to the right channels.” 

The CBFCs own website states that it prevents the public exhibition of films that break certain vague rules, which include "giving offense to any section of the public or injured the feelings of any class of persons" or "ridiculing, disparaging or attacking any religious sect caste and creed."

==Music==
The original soundtrack consists of music  by The Kominas, Mole, Zerobridge and others. An official tracklist is yet to be announced.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 