White Corridors
{{Infobox film
| name = White Corridors
| image = "White_Corridors".jpg
| director = Pat Jackson
| producer = Joseph Janni John Croydon
| based on = novel Yeomans Hospital by Helen Ashton starring       = Googie Withers
|  cinematography = C.M. Pennington-Richards
|  editing        = Sidney Hayers
|  studio         = Vic Films Productions
|  distributor    = General Film Distributors (UK)
|  released       = 12 June 1951 (London)(UK)
|  runtime        = 102 minutes
|  language       = English 
| country        = United Kingdom 
|  budget         =  
}}
 1951 British drama film directed by Pat Jackson and based on a novel by Helen Ashton. It starred Googie Withers, Godfrey Tearle, James Donald and Petula Clark. The film is set in a hospital shortly after the establishment of the National Health Service. 

==Plot==
The day-to-day life of the staff and patients at a city hospital. 

==Cast==
* Googie Withers as Dr. Sophie Dean
* James Donald as Neil Marriner
* Godfrey Tearle as Mr. Groom, Sr.
* Petula Clark as Joan Shepherd
* Jean Anderson as Sister Gater
* Timothy Bateson as Dr. Cook
* Fabia Drake as Miss Farmer Henry Edwards as Phillip Brewster
* Gerard Heinz as Dr. Macuzek
* Megs Jenkins as Mrs. Briggs Barry Jones as Dr. Shoesmith
* Avice Landone as Sister Jenkins
* Bernard Lee as Burgess
* Moira Lister as Dolly Clark
* Dandy Nichols as Char
* Basil Radford as Retired Civil Servant
* Bruce Seton as Policeman
* Patrick Troughton as Sailor
* Jack Watling as Dick Groom
* Philip Stainton as Sawyer
* Dana Wynter – first film role

==Production==
The film marked Googie Withers return to acting after 13 months off following the birth of her child.  John Mills at once stage was announced to play the male lead. 

Bombardier Billy Wells, the man who bangs the gong on the Rank trademark, had a small role. 
  

==Reception==
At the 1951 BAFTAS it was nominated for Best Film and Best British Film.  Petula Clark was nominated for Best Supporting Actress for her role.

It was the 8th most popular movie at the British box office in 1951. 

==References==
 

 

 
 
 
 
 
 
 


 