A Praga
{{Infobox film
| name = A Praga
| image = A_Praga1.jpg
| image_size = 230px
| caption = Brazilian film poster
| director = José Mojica Marins
| producer = Heco Produções & Eugenio Puppo
| writer = José Mojica Marins Rubens Francisco Luchetti
| starring = Felipe Von Rhine Sílvia Gless Wanda Kosmo
| music =
| cinematography = Giuseppe Romero
| editing = Eugenio Puppo
| distributor =
| released = 1980
| runtime = 52 minutes
| country = Brazil
| language = Portuguese
| budget =
}}
 1980 Cinema Brazilian horror film directed by José Mojica Marins. 

==Plot==
Young couple Juvenal and Mariana go on a trip and Juvenal unknowingly takes some pictures of a strange elderly lady (played by Wanda Kosmo) who is eventually revealed to be a sinister witch. The witch then places a curse on the man for photographing her. A wound begins to open in his side which has an agonizing hunger for raw meat which he must constantly feed in order to stop the burning pain. Perplexing his doctors, Juvenals wound becomes hungrier as the film progresses, the man becomes delirious and murders his wife, thinking she will leave him because of his condition. The witch then reappears and coaxes him into feeding the wound with the body of his dead wife. The separate skeletons of the couple are discovered lying next to each other by police months later.   

==Cast==
* Felipe Von Rhine as Juvenal
* Sílvia Gless as Mariana
* Wanda Kosmo as The Witch 

==Production==
 
The film was mostly shot in Super 8 mm film|Super-8 in 1980, but the footage was shelved due to lack of resources to finish it. During the preparations of a major retrospective of his work that took place in São Paulo and Brazil in 2007, for which brand new 35 mm film prints of no less than 25 films were made, Mojica and producer Eugenio Puppo decided to finish the film. Puppo put together the raw material, shot additional scenes, recovered the audio using lip reading, edited and supervised the post-production process, and the result was shown to those who attended the retrospective. Puppo and Mojica are now looking for a way to transfer the film to 35mm. A Praga was expected to travel around international festivals.    As of 2013, however, Marins still trying to release it. 

The story is also featured in Marins graphic novel series O Estranho Mundo de Zé do Caixão in the issue A Praga (V.1, #2).

==References==
 

==External links==
*    
*  
*   on  
*  

 

 
 
 
 
 
 
 
 