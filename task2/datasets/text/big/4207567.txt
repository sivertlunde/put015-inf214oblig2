Nancy Drew (2007 film)
 
{{Infobox film
| name           = Nancy Drew
| image          = Nancy drew.jpg
| caption        = Theatrical release poster
| director       = Andrew Fleming
| producer       = Jerry Weintraub
| writer         = Andrew Fleming Tiffany Paulsen
| starring       = Emma Roberts Josh Flitter Max Thieriot Rachael Leigh Cook Tate Donovan Daniella Monet Kelly Vitz Marshall Bell Laura Harring
| music          = Ralph Sall
| cinematography = Alexander Gruszynski
| editing        = Jeff Freeman Jerry Weintraub Productions
| distributor    = Warner Bros.
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = $20 million
| gross          = $30,666,930
}} titular teen detective. It stars Emma Roberts as Nancy Drew, Max Thieriot as Ned, Kay Panabaker as George Fayne|George, and Amy Bruckner as Bess Marvin. Set in Los Angeles, it was directed by Andrew Fleming.

Critics reactions were mixed with the general thought of it being refreshing.   The film grossed $30,666,930 worldwide on a $20 million budget. Emma Roberts signed on for two sequels, however they were canceled to due negative fan reception and the low gross from the opening weekend.

==Plot==
Nancy Drew (Emma Roberts) and her  widowed father, Carson Drew (Tate Donovan), move from River Heights for only a few months and rent a house in California, where Carson has a temporary job. Nancy chose their California house because it was the home of Dehlia Draycott, a murdered movie star based on Natalie Wood whose case has never been solved. Despite the mystery, Nancys father has forbidden her from further sleuthing and encourages her to focus on high school and being normal. Nancy struggles to fit in at her new school, only befriending a younger boy, Corky (Josh Flitter). She realizes that the sleuthing world is the only place she fits in and decides to solve the Draycott mystery. In the Draycott mansion, she discovers a letter that Draycott wrote to an unknown "Z", who was supposedly Draycotts love interest. From photographs of Draycott before her death, she figures out that just before her death, Draycott had a child and gave it up for adoption in privacy. She learns that the child, Jane Brighton (Rachael Leigh Cook), is the sole beneficiary of Draycotts will, which has disappeared. Nancy receives a threatening phone call telling her to get off the case, and contacts her fathers business associate, Dashiel Biedermeyer (Barry Bostwick), the lawyer of the Draycott estate, to assist her with the case. 
 underground passageway to a neighbors basement, which is rented by Leshing (Marshall Bell), the groundskeeper to the Draycott estate.

One afternoon, a tearful Jane arrives on Nancys doorstep and announces that her daughter has been taken away from her on false charges of child endangerment. She reveals that after Nancys initial visit, a man showed up on her doorstep to threaten her. Nancy demands that her father take up Janes case. He agrees as Jane stays with them. While watching a Dehlia Draycott film, Nancy realizes that Draycott must have hidden her revised will in a prop from one of her movies. She tracks the will to a Chinese antique shop, but just after retrieving it, Nancy is chloroformed into unconsciousness from behind and kidnapped by the villains henchmen, and left in a locked room. Naturally, Nancy escapes, but gets into a car crash and must go to the emergency room. Her father, along with Biedermeyer, arrives and demands to know what is going on. She admits to her secret sleuthing and explains about Draycotts hidden will. Biedermeyer offers them a ride home so he can sign a business deal with Mr. Drew. Nancy discovers that Biedermeyer is the one who was disinherited by Dehlias will (signing his papers with a large "Z" as his middle name is Zachary), concludes that he is Dehlia Draycotts supposed love. However, when he questions Nancy about the will, she manages to jump out of the moving car, leaving her father with Biedermeyer and his men. Nancy manages to make it all the way home and is caught by Biedermeyer who threatens to "squeeze the will out of her." Nancy asks him why he killed Dehlia and he replies that Dehlia went a bit crazy after her reappearance, that Jane is not his daughter, but Leshings, and demands the will. Nancy kicks Biedermeyer in the shin and escapes, but is once again cornered by Biedermeyer and his henchmen. Leshing arrives via the secret passageway and knocks the henchmen unconscious and Nancy reveals that she secretly recorded what Biedermeyer told her. While police arrive at the house and arrest Biedermeyer, Nancy tells Leshing hes Janes father and he tells Jane and she hugs him for the first time in her life. The will is restored to its rightful owner. Jane is able to get back her daughter and converts the Draycott mansion into a home for single mothers as the Drews return to River Heights. 

As Nancy watches a video sent by Jane and her new Draycott Home for Single Mothers, she is a bit sad that the mystery is over. She goes outside to see Ned repairing her car. They talk and they both lean in for a kiss. Right after Nancy and Ned kiss, her father tells her that she has a long-distance phone call for a new mystery in Scotland. She is just as cheerful as ever as she runs back into their River Heights home.

==Cast==
* Emma Roberts as Nancy Drew
* Josh Flitter as Corky Veinshtein
* Max Thieriot as Ned Nickerson
* Rachael Leigh Cook as Jane Brighton
* Tate Donovan as Carson Drew
* Marshall Bell as John Leshing
* Laura Harring as Dehlia Draycott
* Barry Bostwick as Dashiel Zachary Biedermeyer
* Daniella Monet as Inga Veinshtein
* Kelly Vitz as Trish
* Caroline Aaron as Barbara Barbara
* Adam Clark as Sgt. Billings Pat Carroll as the Elderly Neighbour
* Amy Bruckner as Bess Marvin
* Kay Panabaker as George Fayne
* Cliff Bemis as Chief McGinnis
* Kaitlyn Van Item as Allie

Several well-known actors make uncredited guest appearances throughout the movie. Bruce Willis appears as himself, shooting a crime film in Los Angeles; Adam Goldberg plays Willis director Andy; Chris Kattan plays one of the burglars Nancy catches in the opening sequence of the film; Lindsay Sloane plays a saleslady in a clothing boutique; Eddie Jemison appears as an adoption clerk; and Geraint Wyn Davies makes a brief appearance as a drama teacher.

==Background and production== roadster for South Pasadena, Santa Clarita, Long Beach, La Canada Flintridge and Burbank, California|Burbank. 

Nancys car in the film is a blue Nash Metropolitan convertible.

==United States TV rights==
U.S. cable networks, such as ABC Family and Disney Channel, acquired the rights to the 2007 film version of Nancy Drew. ABC Family, not Disney Channel, aired the 2002 made-for-TV version of Nancy Drew starring Maggie Lawson, though it was shown on Disney Channel Asia.

==Home media==
Nancy Drew was released on DVD and Blu-ray on February 11, 2008 in a single-disc edition and combo pack.

==Reception==
Nancy Drew received mixed reviews from critics. It received a "Rotten" 50% rating at Rotten Tomatoes with the sites consensuses being "Emma Roberts is bubbly and charming as Nancy Drew, the junior detective. But despite her best efforts, Nancy Drew still lacks excitement, surprise, and compelling secondary characters." The film received a rating of 54% at Metacritic based on 31 critics, indicating "mixed or average" reviews.  Plugged In said that "the film has all of the oversimplifications of a teen mystery novel with a little—but not enough—humorous self-awareness tossed in to make the story satisfying for adults". Opening at #7 in the U.S. box office, the film grossed $6,832,318 on its opening weekend and has since grossed $25,612,520 in the US and $5,054,410 overseas for a total $30,666,930 worldwide. 

Fans of the book series have given negative reception on the movie, but praise Roberts performance as Drew.

==Book adaptions==
A novelization of the movie was written by Daniella Burr the year of the films release and published by Simon Spotlight.

==Soundtrack==
# "Come to California" (Matthew Sweet)
# "Perfect Misfit" (Liz Phair) Kids in America" (The Donnas)
# "Pretty Much Amazing" (Joanna Pacitti|Joanna)
# "Looking for Clues" (Katie Melua)
# "Hey Nancy Drew" (Price)
# "Like a Star" (Corinne Bailey Rae)
# "Nice Day" (Persephones Bees)
# "Blue Monday" (Flunk)
# "We Came to Party" (J-Kwon)
# "All I Need" (Cupid (singer)|Cupid)
# "Party Tonight" (Bizarre)
# "When Did Your Heart Go Missing?" (Rooney (band)|Rooney)
# "Dare (song)|DARE" (Gorillaz featuring Shaun Ryder)
 
;Nominated
*Nickelodeon Australian Kids Choice Awards 2008 – Favorite Movie Star for Emma Roberts 2007 – Favorite Movie
*Teen Choice Awards
*2007 – Choice Movie Actress: Comedy for Emma Roberts
*2007 – Choice Movie: Breakout Female for Emma Roberts Young Artist Awards
*2007 – Best Family Feature Film (Comedy or Drama)
*2007 – Best Performance in a Feature Film – Leading Young Actress for Emma Roberts
*2007 – Best Performance in a Feature Film – Young Ensemble Cast for Emma Roberts, Josh Flitter, Amy Bruckner and Kay Panabaker

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 