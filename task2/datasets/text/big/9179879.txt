A Date with Judy (film)
{{Infobox film
| name           = A Date with Judy
| image          = A Date With Judy film poster.jpg
| image_size     =
| caption        = A Date with Judy film poster
| director       = Richard Thorpe
| producer       = Joe Pasternak
| writer         = Dorothy Cooper Dorothy Kingsley Aleen Leslie (characters)
| narrator       =
| starring       = Wallace Beery Jane Powell Elizabeth Taylor Carmen Miranda Xavier Cugat Robert Stack
| music          = Ernesto Lecuona Robert Surtees
| editing        = Harold F. Kress
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 113 minutes
| country        = United States English
| budget = $1,353,000  . 
| gross = $4,586,000 
}}
 radio series of the same name.

The film was photographed in Technicolor and largely served to showcase the former child star Elizabeth Taylor, age 16 at the time. Taylor was given the full MGM glamor treatment, including specially designed gowns.
 Leon Ames as a dignified father figure, the same role he played in the Judy Garland film Meet Me in St. Louis (1944) and Wallace Beery in his penultimate role as a contrasting "rough and ready" father figure.

The film features the soprano singing voice of young Jane Powell, and is also a showcase for the musical performances of the Latin American singer Carmen Miranda and bandleader Xavier Cugat. In this film, she is given to humorous malapropisms such as "His bite is worse than his bark" and "Now Im cooking with grass". The songs "Judaline" and "Its a Most Unusual Day" also debuted in this film.

==Plot== Santa Barbara is coming up. Judy Foster (Jane Powell) expects boyfriend "Oogie" Pringle to be her escort, but he declines. Meanwhile, Oogies sister, sophisticated senior Carol Pringle (Elizabeth Taylor), has booked famous bandleader Xavier Cugat and his orchestra for the dance.

Cugats lady friend, Rosita Cochellas (Carmen Miranda), is a dance instructor. She is secretly giving rumba lessons to Judys dad, Melvin Foster (Wallace Beery), who wants to surprise his wife with a dance for their upcoming wedding anniversary.

Soda shop owner Pop Scully (Lloyd Corrigan) introduces a disappointed Judy to his handsome nephew Stephen I. Andrews (Robert Stack), who volunteers to take Judy to the dance, even though hes considerably older. Judy finds him dreamy, and having Stephen as her date definitely makes Oogie jealous.

Stephen, however, falls for the beautiful Carol instead. This is annoying to Judy, as is her discovery that her dad is seeing Rosita behind her mothers back, presumably carrying on a romantic affair. Misunderstandings abound, including Rosita trying to explain the situation to her boyfriend, Cugat.

During the dance scene in which Mrs. Foster dances with Xavier Cugat, the song is the instrumental version of "The Walter Winchell Rumba", which Cugat and his band played during their normal gigs.  The words, can be heard during a PBS Lawrence Welk show called, "The New York Show" by Gail, Sandy, and Marylou.

==Cast==
*Wallace Beery as Melvin R. Foster
*Jane Powell as Judy Foster
*Elizabeth Taylor as Carol Pringle
*Carmen Miranda as Rosita Cochellas
*Xavier Cugat as Himself
*Robert Stack as Stephen Andrews
*Scotty Beckett as Ogden "Oogie" Pringle
*Selena Royle as Mrs. Foster Leon Ames as Lucien T. Pringle
*Clinton Sundberg as Jameson 
*George Cleveland as Gramps 
*Lloyd Corrigan as "Pop" Sam Scully
*Stuart Whitman as Young Man in the ballroom (uncredited)  Jerry Hunter as Randolph Foster 
*Jean McLaren as Mitzi Hoffman

 
 
Image:Jane Powell - A Date with Judy (1948).jpg|
Image:Elizabeth Taylor - A Date With Judy (1948).jpg|
Image:Carmen Miranda - A Date with Judy (1948).jpg|
Image:Robert Stack - A Date with Judy (1948).jpg|
Image:Xavier Cugat - A Date with Judy (1948).jpg|
 
 

==Production==
  ABC network. Louise Erickson and Ann Gillis. Pre-production news items in HR indicate that actor Thomas E. Breen was originally set to co-star in the film with Jane Powell, and that Leslie Kardos was set to direct. A Dec 1947 HR news item notes that Selena Royle replaced Mary Astor, who withdrew from the film due to illness. A contemporary HR news item lists Marcia Van Dyke in the cast, but her appearance in the released film has not been confirmed. A biography of director Vincente Minnelli notes that a musical number entitled Mulligatawny, which was created by Stanley Donen, was cut from the film before its release. Actress Patricia Crowley portrayed "Judy Foster" in the ABC television series A Date with Judy , which ran from 1951 to 1953.   
 
==Reception==
The film was a hit, earning $3,431,000 in the US and Canada and $1,155,000 elsewhere resulting in a profit of $1,495,000. 

== Critical reception ==
"Jane Powell was used to playing good-girl types, but for Elizabeth Taylor A Date With Judy represented a chance to build a whole new image. The role of sultry bad-girl Carol in the film gave her the opportunity to show the world that she was no longer a child, but a beautiful young woman. A direct contrast to Powells girlish wholesomeness, Taylors character was more mature, and costumes and makeup strategically played up her sensuality (...) Musical number highlights in A Date With Judy include the popular "Its a Most Unusual Day" and "Judaline." However, its the "Brazilian Bombshell" Carmen Miranda who nearly steals the show in a supporting role as rhumba teacher Rosita. Her energetic rendition of "Cuanto Le Gusta" alone makes watching A Date With Judy worthwhile." said Andrea Passafiume of the  Turner Classic Movies  

== Soundtracks ==
* Judaline ... Performed by Jane Powell, Scotty Beckett & Quartet
* Its a Most Unusual Day ... Performed by Jane Powell
* Im Strictly on the Corny Side ... Performed by Jane Powell & Scotty Beckett
* Love Is Where You Find It ... Performed by Jane Powell
* Its a Most Unusual Day ... Performed by Elizabeth Taylor (Dubbed by Jean McLaren)
* Swing Low, Sweet Chariot ... Performed by Lillian Yarbo
* Smiling Through The Years ... Performed by Jane Powell & George Cleveland
* Love Is Where You Find It  (Reprise)  ... Performed by Jane Powell
* Home Sweet Home ... Performed by Jane Powell, Jerry Hunter & Selena Royle Xavier Cugat and His Orchestra 
* Marguerita ... Performed by Xavier Cugat and His Orchestra
* Cuanto La Gusta ... Performed by Carmen Miranda with Xavier Cugat and His Orchestra 
* Its a Most Unusual Day  (Reprise #2 - Finale)  ... Performed by Elizabeth Taylor, Jane Powell, Carmen Miranda, Wallace Beery, Xavier Cugat, Robert Stack, Scotty Beckett, Selena Royle, Leon Ames, George Cleveland, Jerry Hunter & Ensemble

==Notes==
 

==External links==
*  
*  
* 
*   at NNDB
* 

 

 
 
 
 
 
 
 
 
 
 
 
 