Burying the Ex
 
{{Infobox film
| name           = Burying the Ex
| image          = Burying the Ex.jpeg
| caption        = Film poster
| director       = Joe Dante
| producer       = Carl Effenson Kyle Tekiela Frankie Lindquist Mary Cybriwsky Alan Trezza David Johnson
| writer         = Alan Trezza
| starring       = Anton Yelchin  Ashley Greene  Alexandra Daddario Oliver Cooper
| music          = Joseph LoDuca
| cinematography = Jonathan Hall
| editing        = Marshall Harvey
| studio         = Voltage Pictures
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
}}

Burying the Ex is a 2014 American comedy horror film directed by Joe Dante. It screened out of competition at the 71st Venice International Film Festival.      

==Plot==
Max is dating the overbearing Evelyn who hes afraid to break up with. When shes killed in a freak accident, he falls in love with Olivia, with whom he is much better suited. However Evelyn rises from the grave.
==Cast==
* Anton Yelchin as Max
*  Ashley Greene  as Evelyn
* Alexandra Daddario as Olivia
* Oliver Cooper as Travis Archie Hahn as Chuck
* Mark Alan as Goth Bartender 
* Gabrielle Christian as Coco 
* Mindy Robinson as  Mindy
* Dick Miller as Grumpy Cop

==Filming==
The film is based on a short film by Alan Trezza, who expanded it into a feature film script. According to an interview with director Joe Dante in 2014:
 I met Alan several years ago, got the script for the feature, which was not substantially different from the one we shot. I thought it was funny and different. Cut forward five years. Unexpectedly, after another project fell through, money appeared so we could make the movie, but we had to shoot cheaply and quickly. We shot in 20 days and it was a lot of fun, a hectic shoot, but there was a lot to do. We were clever about our locations and shooting all within the same area of town... The four leads are really fun and creative and brought a lot to the table.  
Principal photography began November 17, 2013 and ended December 19, 2013. 

Mary Woronov played the owner of a store called Bloody Marys. However she was cut out of the film.   accessed 14 Sept 2014 

Dante said the film was "very EC Comics, but it’s not a comic book movie, I think the characters are a little bit more real...  I was left alone in terms of creativity. I think this is film for my fan’s for sure." 

== Release ==
In January 2015 Image Entertainment acquired the release rights of the film  and set the release of the Film in the United States, to summer via Video on demand. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 