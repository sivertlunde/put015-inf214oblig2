Planet Raptor
{{Infobox television film
| name           = Planet Raptor
| image          =  
| caption        = DVD cover
| director       = Gary Jones
| writer         = Steve Latshaw
| starring       = Steven Bauer Florian Ghimpu Serban Celea Vanessa Angel Ted Raimi Musetta Vander
| studio         = Apollo Media
| network        = Syfy
| released       =  
| runtime        = 87 minutes
| country        = United States United Kingdom Romania
| language       = English
}} 
Planet Raptor is a 2007 science-fiction made-for-television film directed by Gary Jones.  It is a sequel to the 2004 made-for-TV film, Raptor Island. Steven Bauer returned in this sequel playing a different role. It was entirely filmed in Romania. The movie premiered in the United States on 25 January 2009 but was previously released on DVD in Brazil and Japan.

==Plot==
In 2066, the planet is entirely occupied by velociraptor|raptors. The only hope for the human race is a group of marine (military)|marines.

==Alternate titles==
The film is also known as "Raptor Planet 2", "Raptor Island 2: Raptor Planet", "Jurassic Planet".

==Cast==
*Steven Bauer as Captain Mace Carter
*Florian Ghimpu as Sgt. Krieger
*Serban Celea as Commander Bakewell
*Vanessa Angel as Dr. Anna Rogers
*Ted Raimi as Tygoon
*Musetta Vander as Sergeant Jacqueline Moore
*Peter Jason as Pappy

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 

 