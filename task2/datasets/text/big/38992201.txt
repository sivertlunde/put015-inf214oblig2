Hammer of the Gods (2013 film)
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Hammer of the Gods
| image          = Hammer of the Gods.jpg
| caption        = Theatrical poster
| director       = Farren Blackburn
| producer       = Rupert Preston Huberta Von Liel Matthew Read
| starring       = Charlie Bewley Clive Standen James Cosmo
| music          = Benjamin Wallfisch
| cinematography = Stephan Pehrsson
| editing        = Sam Williams	 
| studio         = Vertigo Films
| distributor    = Magnet Releasing
| released       =   
| runtime        = 103 min
| country        = United Kingdom
| language       = English
| budget         = 
| gross          =  $641 
}}
Hammer of the Gods is a 2013 British action film directed by Farren Blackburn and released on 5 July 2013. In it, a dying Viking king sends his son on a quest to seek out his older brother, the clans only hope for defeating an approaching enemy horde.

==Plot==

Britain 871 AD. Young Viking prince Steinar arrives in England with a complement of 500 reserve warriors to combat a Saxon uprising that is crushing the occupying forces led by his father, King Bagsecg. Arriving at his fathers camp, Steiner attends a family meeting with his father, his older brother Harold, and their younger half brother Vali, who is disliked by everyone but Steinar for being half Saxon. Absent is Steinar and Harolds older brother Hakan, who has not been seen for over a decade due to a bitter falling out between him and their father, the cause unknown to Steinar.

Bagsecg, who is bed ridden and dying, dreads of leaving the throne to Harold (now next in line due to Hakans absence) whose insistence that diplomacy being their best option would instead put their clan under English tyranny. He orders Steinar to kill Vali for cowardliness, to test his strength as a leader.

He refuses and warns Harold off who attempts it himself to gain favour with their father. Furious, Bagsecg dismisses Harold and Vali and charges Steinar with a near impossible task. Despite still harbouring contempt for his eldest son, Bagsecg  orders Steinar to venture deep in to the hostile English lands to find Hakan and bring him back so he may assume the throne.

Steinar departs with his closest comrades: his close friend Hagen, a Berserker named Grim, and Jokul; a superstitious believer in omens. Later they are joined by Vali who warns he has witnessed Harold secretly meeting with the Saxon King. Despite the urgency to return, Steinar pushes forward.

They approach Ivar the Boneless, a Viking recluse and paedophile who lives with a slave girl named Agnes and a mute Catamite|catamite. He agrees to lead them to where he believes Hakan to be and departs with Agnes while abandoning his latter.

The group however are pursued by hooded men who slay Grim and later capture them all. Revealed to be soldiers of the Christian faith, their captain confirms that Harold has been secretly negotiating a surrender, on condition he remains in power over his people. But the captain proposes to Steinar that he would be best suited to rule his clan, if he agrees to submit to Christianity. Steinar refuses; knowing the stranglehold it would have on his people. Vali however switches sides to save his own neck and is taken to a nearby church, while Ivar is castrated for his perversions and foul talk.

After Agnes (who had evaded capture) frees Steinar, he then frees Hagen and Jokul. Ivar dies from blood loss but tells Steinar where he may find Hakan. They rescue Vali, but Hagen and Jokul demand he be killed for his cowardliness and treachery, forcing Steinar to kill Hagen instead to protect his brother.

Now down to four, they head in to an eerie forest and are captured again; this time by a tribe who dwell deep within a nearby cave. Taken there, Jokul is served-up as the tribes banquet, Vali again switches sides, while Agnes is claimed by the tribal chief, Steinars older brother Hakan, who the tribe worship as a god.

During the tribal festivities, Steinar encounters his mother Astrid; whom he believed dead, and who is also deluded by Hakans megalomania. Further shocking revelations are made as Steinar learns finally the truth behind Hakans exile. To his disgust his mother and brother openly share a passionate kiss, revealing their incestuous relationship.

After Hakan kills Vali to further his dominance over the tribe, he and Steinar are lowered in to a dark pit to fight to the death, but despite Astrid secretly handing Hakan a knife to win, Steinar emerges the victor. The tribe bow in submission while Astrid, in an attempt to kill Steinar, is thrown in to the pit by him.

Steinar later returns to Bagsecgs camp with Agnes, and presents Hakans head to his father. Harold argues he was supposed to bring Hakan back alive. But Bagsecg responds “He was sent to find a king” seeing that Steinar is now ready to lead their people.

Steinar then kills Harold for his treachery; much to Bagsecgs applause. Later with Agnes by his side he musters his army to confront the approaching Saxon forces.

==Cast==
* Charlie Bewley as Prince Steinar
* Clive Standen as Hagen
* Michael Jibson as Grim
* James Cosmo as King Bagsecg
* Elliot Cowan as Hakan
* Guy Flanagan as Jokul
* Glynis Barber as Astrid Ivar
* Alexandra Dowling as Agnes
* Finlay Robertson as Prince Harold 
* Frances Magee as Ulric The Chronicler 
* Salomon Thomson as Wilfred 
* Michael Lindall as Leader Saxon 
* Laura Sibbick as Woman Saxon
* Theo Barklem-Biggs as Prince Vali
* Daniel Stephen Price as the ginger king from Hertford

==Reception==
{{multiple issues|section=yes|
 
 
}}
The films reception was negative, with a 30% rating on Rotten Tomatoes. "Its hardly high art, but for a cheapjack homegrown action flick this is surprisingly solid." Another review stated "A nasty, brutal and relatively short entertainment, aimed at middle-of-the-woad Conanists." All in all, it was a box-office flop.

==References==
 
* 

==External links==
*  
* 

 
 
 