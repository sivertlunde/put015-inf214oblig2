What Happened on Twenty-third Street, New York City
{{Infobox film
| name = What Happened on Twenty-third Street, New York City
| image = What Happened on Twenty-third Street, New York City.png
| image_size =
| caption = A screenshot from the film
| director = George S. Fleming Edwin S. Porter
| producer =
| writer =
| starring = A. C.  Abadie Florence Georgie
| cinematography = Edwin S. Porter
| editing =
| distributor =
| released =  
| runtime = 77 seconds
| country = United States
| language =
| budget =
| gross =
| website = italic title = force
}}
 . Circa 1901]]
What Happened on Twenty-third Street, New York City is a 1901 American short film depicting a woman walking over a grate, the hot air lifting her skirt.
 The Library The Seven Year Itch, writing "With The Seven Year Itch (1955, FGB 0012-0023), the image of Marilyn Monroes thighs exposed under her billowing skirt entered American popular culture. The Librarys motion picture and broadcasting collections provide the opportunity to document not only how womens roles and their depictions have changed throughout the past hundred years, but also how much has remained the same." 

Tom Gunning makes another observation, contrasting the two events as narrative devices writing "The act of display   is both climax and resolution here and does not lead to a series of incidents or the creation of characters with discernible traits. While the similar lifting of Marilyn Monroes skirt in The Seven Year Itch also provides a moment of spectacle, it simultaneously creates character traits that explain later narrative actions."  

==See also== More Treasures from American Film Archives

== References ==
 

== External links ==
 
* 
*  
* 

 
 
 
 
 
 
 
 
 
 
 
 


 