A Music Fairy
 
{{Infobox Film name        = A Music Fairy; A Tribute to Nazia Hassan image       = A-Music-Fairy.jpg caption     = Promotional poster for A Music Fairy director    = Ahmad Haseeb producer    = Ahmad Haseeb writer      = Ahmad Haseeb & Khalid Ahmad Siddique starring    =  distributor =  released    = January 3, 2007 runtime     = 44 minutes country  Pakistan
|language    = Urdu budget      = 
}}
A Music Fairy is a 44-minute documentary about Nazia Hassan directed and produced by Ahmad Haseeb. {{cite web
|url= http://splus.nation.com.pk/pakistan-news-newspaper-daily-english-online/In-Focus/14-Mar-2009/Nazia-lives-on|title= Nazia Lives on |accessdate= 2009-03-14 |author= Rao Dillshad Hussain |date= 2009-03-14 |work= http://splus.nation.com.pk|publisher= The Nation| archiveurl= http://web.archive.org/web/20090414053150/http://splus.nation.com.pk/pakistan-news-newspaper-daily-english-online/In-Focus/14-Mar-2009/Nazia-lives-on| archivedate= 14 April 2009  | deadurl= no}}  The dcoumentary was screened at the Kara Film Festival in 2009. 

The documentary starts with the Anwar Maqsoods tribute poem on Nazia Hassan and then talks about Nazias childhood. Ahmad Haseeb uses rare music videos of Nazia and Zoheb like Disco Deewane, Boom Boom, Mujhay Chahie, Telephone Pyar & Hulla Gulla Karien Hum. The Documentary explains the death of Nazia Hassan, and ends with her funeral. http://blog.pakspice.com/?p=2309= The life and Times of Nazia Hassan 

A Music Fairy is a non-commercial documentary. In an interview Ahmad Haseeb reveals that he has no plan to release it commercially, because it was his school thesis and too personal. 

== World screening ==
On 29 March 2012 A Music Fairy has been screened at University of Ankara under celebration of Pakistan Culutral Week "Jeevay Jeevay Pakistan" arranged by Embassy of Pakistan in Turkey. The documentary is appreciated by the audience. {{cite web title = Screening of Pakistani film ‘Khuda Kay Liye’ and Documentary ‘A Music Fairy’  |accessdate= 2012-03-29 |author= Pakistan Turkey Blog |date= 2009-03-29 |work= Pakistan Turkey Blog | archiveurl= http://pakturkey.blogspot.com/2012/03/pakistan-cultural-week-screening-of.html| archivedate= 30 March 2012  | deadurl= no}} 

== See also ==
* Nazia Hassan
* Ahmad Haseeb

== References ==
 


 

 
 
 
 
 
 

 