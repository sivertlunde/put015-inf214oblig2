Lope (film)
{{Infobox film
| name           = Lope
| image          = Lope.jpg
| caption        = 
| director       = Andrucha Waddington
| producer       = 
| screenplay     = 
| story          =  Antonio de la Torre Juan Diego Luis Tosar Selton Mello Sonia Braga
| music          = 
| cinematography = 
| editing        = 
| studio         = Antena 3 Films Conspiração Filmes Ikiru Films El Toro Pictures
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = Spain Brazil
| language       = Spanish
| budget         = £13.000.000 
| gross          = 
}}
 drama film Spain and Cinema of Brazil|Brazil, inspired in the youth of Lope de Vega.

==Plot==
Biopic of the poet and playwright Lope de Vega. Set in 1588, Lope is a young soldier who has just arrived in Madrid from the war. The city is still under construction and he, like many others, still does not know which path to follow. While struggling with his restlessness and ambition, two women come into his life. One, a liberal and successful businesswoman who can help Lope with his career, and the other, an aristocrat who ends up being his true lover.

== Cast ==
* Alberto Ammann as Lope de Vega
* Leonor Watling as Isabel de Urbina
* Pilar López de Ayala as Elena Osorio Antonio de la Torre as Juan de Vega
* Juan Diego as Jerónimo Velázquez
* Luis Tosar as Frai Bernardo
* Ramón Pujol as Claudio
* Selton Mello as Marqués de Navas
* Sonia Braga as Paquita
* Jordi Dauder as Porres
* Miguel Angel Muñoz as Tomás de Perrenot
* Antonio Dechent as Salcedo
* Carla Nieto as María de Vega

==References==
 

==External links==
*    
*  

 

 
 
 
 
 
 