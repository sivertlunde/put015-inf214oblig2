Eşrefpaşalılar
{{Infobox film
| name           = Eşrefpaşalılar
| image          = EsrefpasalilarFilmPoster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical poster
| director       = Hüdaverdi Yavuz
| producer       = M. Yusuf Kulaksız
| writer         = Burak Tarık
| narrator       = 
| starring       =  
| music          = Yücel Arzen Rico
| editing        = 
| studio         = 
| distributor    = Medyavizyon
| released       =  
| runtime        =  
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = $1,847,671
}}
Eşrefpaşalılar is a 2010 Turkish comedy-drama film directed by Hüdaverdi Yavuz.

The film, which is the debut feature of director Hüdaverdi Yavuz, treads into familiar territory, the urban lower-middle class, seen in his previous TV work like Şehnaz Tango and Hayat Apartmanı.   

The film is adapted from a play, written by lead actor Burak Tarık, based on real life experiences, which has been successfully staged at theaters across Turkey and in European countries including Germany by Tiyatro ANSE.   

Burak Tarık takes one of the leading roles but the rest of the cast is different from the play, with Hüseyin Soysalan, Turgay Tanülkü, and Sermin Hürmeriç heading the cast. 

The film takes its name from the residents of İzmir’s Eşrefpaşa neighborhood who were infamous for their fierce lifestyle and bad habits until a new Muslim religious teacher was appointed to the neighborhood mosque. 

== Production ==
After three years touring with the play, which has been seen by more than 400,000 people, writer-star Burak Tarık and the Anse team were encouraged by fans to turn the play into a film and were unable to resist this nice pressure. 

The script by Burak Tarık is not an identical adaptation of the original play and many features make the film different. “Instead of just explaining the Eşrefpaşalılar story as is, we aimed to present a portrait of Turkey and explain the kind of neighborhood we miss,” the Anse team said. 

The four-week shoot commenced on   and included location filming around the Balat, Ayvansaray and Eyüp neighbourhoods of Istanbul. 

== Plot ==
The film is about two friends, Tayyar (Hüseyin Soysalan), a mafia leader, and Davut (Turgay Tanülkü), a neighborhood coffee shop owner, who move from their hometown, Eşrefpaşa, to Istanbul, where they both fall for Eleni (Sermin Hürmeriç). Tayyar marries the girl, but she is actually in love with Davut. Aware of the situation, Tayyar attempts to take revenge on Davut by encouraging his foster child, Nusret (Burak Tarık), to become involved in the mafia. Nusret is forced to choose between the girl he loves and the appealing world of big money, but when a hodja is appointed to the derelict neighborhood mosque, the course of events starts to change on its own.

==Release==
The film opened in 153 screens across Turkey on   at number two in the Turkish box office chart with an opening weekend gross of $915,082.   

==Reception==

===Box office===
The film has made a total gross of $1,847,671. 

===Reviews===
Emrah Güler, writing in Hürriyet Daily News and Economic Review|Hürriyet Daily News, states that. In his debut feature, Hüdaverdi Yavuz treads into familiar territory, the urban lower-middle class, seen in his previous TV work like “Şehnaz Tango” and “Hayat Apartmanı,” and recommends the film to, Those who like family dramas set in the urban-lower class, as that’s where director Yavuz’s expertise lies. 

== See also ==
* 2010 in film
* Turkish films of 2010

==References==
 

==External links==
*   (Turkish)
*  

 
 
 
 
 
 
 