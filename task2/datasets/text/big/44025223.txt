Violent (film)
{{Infobox film
| name           = Violent
| image          = 
| alt            = 
| caption        = 
| director       = Andrew Huculiak
| producer       = Josh Huculiak Amy Darling Brent Hodge
| writer         = Andrew Huculiak Josh Huculiak Joseph Schweers Cayne McKenzie
| starring       = Dagny Backer Johnsen Mari Sofie Andreassen Karl Bird Tomine Mikkeline Eide Tor Halvor Halvorsen Yngve Seterås
| music          = We Are the City
| cinematography = Joseph Schweers
| editing        = 
| studio         = Amazing Factory
| distributor    = Media Darling
| released       =  
| runtime        = 102 minutes
| country        = Canada Norway
| language       = Norwegian English
| budget         =$300,000
| gross          = 
}}

Violent is a 2014 Canadian-Norwegian Drama film directed by Andrew Huculiak and created by the Vancouver, BC|Vancouver-based production company Amazing Factory Productions. The film, which was showcased at the 2014 Cannes Film Festival, acts as a companion to the Vancouver band We Are the Citys album of the same name. Though the film features an original score by the band, none of the bands songs are featured in the film. 

The film was nominated for the "Independent Camera" award by the Karlovy Vary International Film Festival.

==Plot Synopsis==
The film follows Dagny (Dagny Backer Johnsen), a young woman longing to escape small town life and move to the big city. Dagnys mother arranges for her to work for a family friend who lives in the city. Dagny recalls her most recent memories of the five people who loved her the most, all while experiencing a catastrophic event.  

==Production==
The movie, produced by Amy Darling of Media Darling and Brent Hodge of Hodgee Films, was made for $300,000, and was shot sometimes with less than five crew members on set at a time. The film is the feature film debut for Amazing Factory Productions, who are well known for creating music videos for bands such as Hey Ocean!, We Are the City and Said the Whale. 

"The film was truly a collaboration between two Vancouver groups, one a band called We Are The City and the other a video production company called Amazing Factory," said Andrew Huculiak in an interview the Vancouver International Film Festival, "The film is mostly in Norwegian. None of us speak Norwegian. It’s funny, because we always knew that conducting this sort of experiment would be a challenge in many obvious ways, but there were so many things that came up as we were already deep into the process. For example, when we got to the editing room with all the footage taken in Norway we assumed that we could subtitle each individual takes by referencing the script, but it quickly became clear that doing it without knowing Norwegian would be impossible. There were natural breaks mid-sentence and we’d be like “where in the English translation are they stopping?”.

The script was written in English and translated into Norwegian with nobody on the production team speaking Norwegian. At times, there were less than five people on set at one time, and with no production vehicle, the crew would walk over 10 kilometers a day.  

==Reception==
Out of the films Canadian premiere at the Vancouver International Film Festival, the film has received critical acclaim. Marina Antunes of Quiet Earth called the film "a fantastic achievement" saying, "Huculiak marks Dagnys day to day normalcy with gorgeous, if sometimes baffling images. Some are abstract and look like static while others are simply strange, people and furniture floating above the city. Violent is unlike any other coming of age story Ive seen and its unlikely Ill ever see another one quite this profound, a movie that works as both a story of self discovery and of self reflection at the same time. A fantastic achievement that is not to be missed."  

Matthew Ritchie of Exclaim! applauded the film, saying that the film draws "on the synth-based soundscapes and minimalist distorted rumblings hidden underneath the bands recent LP of the same name, the film and album act like strange companion pieces that enhance one another while not being entirely essential to enjoying the experience of either. That being said, if youre a fan of We Are the City, Huculiaks first-time feature feels like a testament to not only his strength as an up-and-coming musician, but a cinematic savant as well."  

The Provinces Stuart Derdeyn called the film "a poetic and beautiful tale of tragedy" and "an impressive debut, capturing both a distinctly Scandinavian and, perhaps, Canadian way of looking at traumatic events".  

The film won "Best Canadian Film" and "Best British Columbia Film" at the Vancouver International Film Festival.  

The film was included in the list of "Canadas Top Ten" feature films of 2014, selected by a panel of filmmakers and industry professionals organized by TIFF.  

==References==
 

==External links==
*  
*  
*  
*  

 