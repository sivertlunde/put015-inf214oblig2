Dangerous Intruder
{{Infobox film
| name           = Dangerous Intruder
| image          = Dangerous Intruder film poster.jpeg
| caption        = Theatrical release poster
| director       = Vernon Keays
| producer       = Leon Fromkess Martin Mooney
| writer         = Martin Goldsmith
| story          = F. Ruth Howard Philip MacDonald Richard Powers 
| music          = Karl Hajos	 	
| cinematography = James S. Brown Jr.
| editing        = Carl Pierson
| studio         = 
| distributor    = Producers Releasing Corporation
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Richard Powers . 

==Plot==
New-York-bound hitchhiker Jenny (Borg) is accidentally struck by a car. The driver, art dealer Max Ducane (Arnt), offers to take her into his home until she can resume traveling. Later, Ducanes wife is murdered and Jenny determines to find the killer. With the aid of detective Curtis (Powers), she discovers that Ducane is the murderer, having killed his wife in order to have the funds to finance his antique collection. When she tries to get away the murderer is killed in a car crash when trying to run her down.

==Cast==
* Charles Arnt as Max Ducane 
* Veda Ann Borg as Jenny  Richard Powers as Curtis 
* Fay Helm as Millicent Ducane  John Rogers as Foster 
* Jo Ann Marlowe as Jackie 
* Helena Phillips Evans as Mrs. Swenson

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 

 