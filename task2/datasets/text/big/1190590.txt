Thirty Seconds Over Tokyo
{{Infobox film
| name           = Thirty Seconds Over Tokyo
| image          = ThirtySecondsOverTokyo.jpg
| image size     =220px
| caption        = Theatrical release poster
| director       = Mervyn LeRoy
| producer       = Sam Zimbalist
| based on       = Thirty Seconds Over Tokyo&nbsp;by    
| screenplay     = Dalton Trumbo  Robert Walker Spencer Tracy
| music          = Herbert Stothart Robert Surtees, ASC Harold Rosson, ASC   Frank Sullivan
| studio         = Loews Inc. 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 138 minutes
| country        = United States
| language       = English
| budget         = $2,924,000 "The Eddie Mannix Ledger." Margaret Herrick Library, Center for Motion Picture Study (Los Angeles). 
| gross          = $6,247,000  
}}

 
 

Thirty Seconds Over Tokyo is a 1944 American war film released by Metro-Goldwyn-Mayer. It is based on the true story of the Doolittle Raid, Americas first retaliatory air strike against Japan four months after the December 1941 Japanese attack on Pearl Harbor.
 USS Hornet aka, "Shangri-La."
 Robert Walker as Corporal David Thatcher, Robert Mitchum as Lieutenant Bob Gray and Spencer Tracy as Lieutenant Colonel Jimmy Doolittle, the man who planned and led the raid. The film is noted for its accurate depiction of the historical details of the raid, as well as its use of actual wartime footage of the bombers in some flying scenes.
 

==Plot== Pearl Harbor attack, the United States Army Air Forces plan to retaliate by bombing Tokyo and four other Japanese cities—taking advantage of the fact that US aircraft carriers can approach near enough to the Japanese mainland to make such an attack feasible.

Jimmy Doolittle|Lt.Col. James Doolittle (Spencer Tracy), the leader of the mission, assembles a volunteer force of aircrews, who begin their top-secret training by learning a new technique to make their North American B-25 Mitchell medium bombers airborne in the short distance of 500 feet or less, to simulate taking off from the deck of an aircraft carrier.
 Eglin Field, Florida and Naval Air Station Alameda, the story goes on to describe the raid and its aftermath.

While en route to Japan, the Hornet s task force is discovered by a Japanese picket boat, which has radioed their position. It is sunk, and the bombers are forced to take off 12 hours early at the extreme limit of their range. However, the bombers do make it to Japan and drop their bombs.

After the attack, all but one of the B-25s run out of fuel before reaching their recovery airfields in China. As a result, their crews are forced to either bail out over China or crash-land along the coast. 

Lawsons B-25 unfortunately crashes in the surf just off the Chinese coast while trying to land on a beach in darkness and heavy rain. He and his crew survive, badly injured, but then face more hardships and danger while being escorted to American lines by friendly Chinese. While he is en route, Lawsons injuries require the missions flight surgeon to amputate one of his legs.

The story ends with Lawson being reunited with his wife Ellen in a Washington, D.C., hospital.

==Cast==
As appearing in screen credits (main roles identified):
  
* Van Johnson as Lt. Ted W. Lawson Robert Walker David Thatcher
* Tim Murdock as Lt. Dean Davenport David M. "Davey" Jones
* Gordon McDonald as Lt. Bob Clever
* Don DeFore as Lt. Charles McClure
* Robert Mitchum as Lt. Bob Gray
* John R. Reilly as Lt. Jacob "Shorty" Manch
* Horace McNally as Lt. Thomas "Doc" White James Doolittle
* Phyllis Thaxter as Ellen Lawson
* Donald Curtis as Lt. Randall
* Louis Jean Heydt as Navy Lt. Henry Miller
* William "Bill" Phillips as Lt. Don Smith
* Douglas Cowan as Lt. Everett "Brick" Holstrom
 

==Production== Eglin Field USAAF B-25C and -D bombers (which closely resembled the B-25B Mitchells used in 1942) made for a very authentic, near-documentary feel. 
Auxiliary Field 4, Peel Field, was used for the short-distance take off practice scenes. 

Although an aircraft carrier was not available due to wartime needs (the USS Hornet itself had been sunk in the Battle of the Santa Cruz Islands 27 October 1942 only six months after launching the raid), a mix of realistic studio sets and original newsreel footage faithfully recreated the USS Hornet scenes. Principal photography took place between February and June 1944.   

==Reception==
Recognized as an inspirational patriotic film with propagandistic values, The New York Times in 1944 summed up the universal verdict on the production, "our first sensational raid on Japan in April 1942 is told with magnificent integrity and dramatic eloquence ..."  Variety (magazine)|Variety focused on the human elements, "inspired casting ... the war becomes a highly personalized thing through the actions of these crew members." 

Critical acclaim followed Thirty Seconds Over Tokyo with many reviewers considering it the finest aviation film of the period.  The film is now considered a "classic aviation and war film."  The actual Raiders considered it a worthy tribute. 

===Box Office===
According to MGM records the film made $4,297,000 in the US and Canada and $1,950,000 elsewhere, resulting in a profit of $1,382,000. 

===Awards and honors=== Best Special Effects. Robert Surtees, A.S.C. and Harold Rosson, A.S.C. were also nominated in the category of Black and White Cinematography.   oscars.org. Retrieved: June 23, 2013.  

American Film Institute lists:
* AFIs 100 Years...100 Movies - Nominated
* AFIs 100 Years...100 Heroes and Villains:
** Lt. Colonel James H. Doolittle - Nominated Hero
* AFIs 100 Years...100 Cheers - Nominated

==In popular culture==
 
    Post Fortified Oat Flakes breakfast cereal on a set reminiscent of B-25s on an aircraft carrier flight deck, concluding with the line that the cereal would "take me to Tokyo – and back!"  
 Pere Ubus 1975 debut single are named after the film.  
 Meyer, George. "Commentary for Thirty Minutes Over Tokyo." The Simpsons: The Complete Tenth Season  , 20th Century Fox, 2007. ---its a single episode on a TV series--->

==See also==
 
*The Purple Heart (1944), a fictionalized account of the fate of a group of American airmen from the Doolittle raid placed on trial in a Japanese court. Pearl Harbor (2001), which includes a fictionalized version of the raid.  

==References==

Notes
 

Citations
 

Bibliography
 
* Dolan, Edward F. Jr. Hollywood Goes to War. London: Bison Books, 1985. ISBN 0-86124-229-7.
* Glines, Carroll V. The Doolittle Raid: Americas Daring First Strike Against Japan. New York: Orion Books, 1988. ISBN 0-88740-347-6
* Harwick, Jack and Ed Schnepf. "A Viewers Guide to Aviation Movies". The Making of the Great Aviation Films, General Aviation Series, Volume 2, 1989.
* Orriss, Bruce. When Hollywood Ruled the Skies: The Aviation Film Classics of World War II. Hawthorne, California: Aero Associates Inc., 1984. ISBN 0-9613088-0-X.
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 