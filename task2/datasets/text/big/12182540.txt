Trucker's Woman
{{Infobox Film
  | name = Truckers Woman
  | image = TruckersWoman.jpg
  | caption = VHS cover for Truckers Woman
  | director = Will Zens 
  | producer = Robert W. McClure W. Henry Smith
  | writer = Joseph A. Alvarez W. Henry Smith  Michael Hawkins Mary Cannon Peggy Linville Phil Smoot Doodles Weaver
  | music = 
  | cinematography = Darrell Cathcart
  | editing = 
  | distributor = Troma Entertainment
  | released = 1975 
  | runtime = 81 minutes
  | language = English
  | budget =  
  | preceded_by = 
  | followed_by = 
  }}

Truckers Woman is a 1975 action film directed by Will Zens, the director of Hot Summer in Barefoot County and The Starfighters. It was distributed by Troma Entertainment.

Truckers Woman follows a young man who goes undercover as a truck driver in order to solve the mysterious murder of his trucker father.

==External links==
* 

 
 
 
 
 
 

 