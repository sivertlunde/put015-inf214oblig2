Keelu Gurram
{{Infobox film
| name           = Keelu Gurram
| image          = Keelu Gurram.jpg
| image_size     =
| caption        =
| director       = Raja of Mirzapur
| producer       =
| writer         = Tapi Dharma Rao
| narrator       =
| starring       = Anjali Devi Akkineni Nageswara Rao Laxmirajyam T. Kanakam Surabhi Kamalabai Relangi Venkata Ramaiah A. V. Subba Rao
| music          = Ghantasala Venkateswara Rao
| cinematography = D. L. Narayana
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        = 220 minutes
| country        = India
| language       = Telugu
| budget         =
}}
 Tamil entitled Maya Kudhirai. 

==Plot==
King of Vidarbha becomes attracted to Mohini (Anjali Devi). He brings her to his kingdom as a second wife. She is a demon. She eats elephants and horses in the fort at night leaving bones. According to her plan, she manages to shift the blame onto the Queen. The king punishes the pregnant queen and sends her to the forest. She gives birth to a son (Akkineni). He grows up in Koya Gudem and learns all fighting skills. With the help of Keelu Gurram (Magic horse) which can fly in the sky, he wins the kings attention. He was given the Sainyadhikari (Chief of Army) position due to his bravey. He saves a damsel in distress (Lakshmirajyam). Knowing his intentions, Mohini sends him on an impossible mission to bring her a medicinal herb for her headache. He faces many adventures in pursuit of his goal. He learns that Mohini is in an insect. He reaches the kingdom, while his mother is sentenced to death by hanging. He kills Mohini and saves his mother and restores her dignity.

==Cast==
* Anjali Devi as Mohini
* Akkineni Nageshwara Rao
* T. Kanakam
* Suryashree
* G. Varalakshmi
* Lakshmirajyam Jr.
* Surabhi Kamalabai
* Gangaratnam
* M. Subbulu
* A. V. Subba Rao
* Relangi Venkataramaiah
* Valisetty Koteswara Rao
* D. Satyanarayana

==Songs==
* Bhaagyamu Naadenoyi (Lyrics:  )
* Choochi Teeravakada Naa Chadarangamu Vechichoodavalera (Lyrics: Tapi Dharma Rao; Singer: Krishna Veni)
* Enta Anandabayenaha Madikenta (Lyrics: Tapi Dharma Rao; Singer: V. Sarala Rao)
* Enta Krupamative Bhavani Enta Dayanidhive (Singers: Ghantasala (singer)|Ghantasala)
* Evaru Chesina Karma Varanubhavimpaka Edikainanu Tappadanna (Singer: Ghantasala)
* Gaali Kanna Gola Kanna Pada Biraana (Lyrics: Tapi Dharma Rao; Singer: Ghantasala)
* Kaadusuma Kala Kaadusuma (Lyrics: Tapi Dharma Rao; Singers: Ghantasala and V. Sarala Rao)
* Mana Kali Chettuki Mana Kanna Talliki  Mokkulanu Chellincha Ravoyi (Singer: Ghantasala group)
* Payanamaye Priyathama Nanumarachipokuma (Lyrics: Tapi Dharma Rao;Singer: Ghantasala)
* Sobhana Giri Nilaya Dayamaya (Lyrics: Tapi Dharma Rao; Singer: Krishna Veni)
* Teliyavasama Palukatarama (Lyrics: Tapi Dharma Rao;Singers: Ghantasala and Krishna Veni)
* Yentha Krupamathive Bhavani (Lyrics: Tapi Dharma Rao; Singers: Ghantasala and P. Sridevi)

==Box office==
* The film ran for more than 100 days in 11 centers in Andhra Pradesh. 

==References==
 
* Naati 101 Chitralu, S. V. Rama Rao, Kinnera Publications, Hyderabad, 2006 pp: 42-43.

==External links==
*  
*  

 
 
 
 