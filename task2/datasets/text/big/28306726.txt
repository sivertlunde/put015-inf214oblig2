Timbuktu (1959 film)
 
{{Infobox film
| name           = Timbuktu
| image          = Timbuktupost.jpg
| image_size     = 
| caption        = Original film poster
| director       = Jacques Tourneur
| producer       = Edward Small
| writer         = Paul Dudley Anthony Veiller   
| narrator       = 
| starring       = Victor Mature Yvonne de Carlo
| music          = Gerald Fried
| cinematography = Maury Gertsman
| editing        = 
| studio         = Imperial Pictures
| distributor    = United Artists
| released       =  
| runtime        = 91 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Timbuktu is a 1959 U.S. black-and-white adventure film set in Timbuktu (Africa) but filmed in the Coral Pink Sand Dunes State Park in Kanab, Utah.   It was directed by Jacques Tourneur in 1958.  

==Plot== soldier of fortune Mike Conway (Victor Mature) sees a chance to pay his way back to the United States by gunrunning to hostile Tuareg people|Tuaregs.

Wearing a slouch hat and bush jacket, Conway is armed with a Thompson sub machine gun and a wristwatch with an alarm engraved "From Conway to Conway". He finds himself walking a razors edge between an anti-French Tuareg leader (John Dehner) keen for Conways supply of weapons but keener to use his tarantulas on his prisoners, a moderate Imam (Leonard Mudie) wanting peace, the local French Foreign Legion commander (George Dolenz), and the commanders attractive wife (Yvonne de Carlo) who Conway cannot keep away from.

==Production==
The film was originally meant to be shot on location in colour and widescreen based on an idea of Small and Frank Cavett with Stuart Heisler to direct. Drama: Walter Hampden Takes Over Stone Role; Lyle Bettger to Fret Murphy
Schallert, Edwin. Los Angeles Times (1923-Current File)   28 Sep 1953: B7.   Later, there was a script done by Horace McCoy. Drama: Timbuktu Scheduled as African Epic; Large Jubilee Will Proceed
Schallert, Edwin. Los Angeles Times (1923-Current File)   22 Apr 1954: B9.   At one stage, the film was going to be made by the team of Clarence Greene and Russell Rouse, who were making films for Small. GREENE AND ROUSE TO MAKE 12 FILMS: They Form Team to Produce, Write and Direct With the Backing of Edward Small
By THOMAS M. PRYOR. New York Times (1923-Current file)   31 Aug 1954: 26.  

In 1956 producer   described as "boxing the compass"), The Road to Timbuktu, and Timbuktu Theme. HOLLYWOOD SCENE: Producer Jerry Lewis Descants on His Delicate Delinquent--Other Items Purpose Inducement Double Duty Title Tangle
By THOMAS M. PRYOR. New York Times (1923-Current file)   30 Sep 1956: X7.   However, he eventually settled on the plain title Timbuktu.   In 1957, Anthony Veillier signed to write a script. NEWMAN TO STAR IN MICHENER TALE: Actor and Jean Simmons Will Be in Film Based on Story From Return to Paradise Neame Quits Metro Film Of Local Origin
By THOMAS M. PRYOR. New York Times (1923-Current file)   10 Jan 1957: 26.  Filming started May 1958. FILM EVENTS: Brynner in Vienna Picture
Los Angeles Times (1923-Current File)   30 Apr 1958: B7.  

Director Jacques Tourneur claimed that producer Small thought the film was not long enough so he inserted reaction shots of close-ups of various actors all throughout the film. 

Edward Small felt so embarrassed by the film that he removed his name from the final credits. Fujiwara, Chris Jacques Tourneur: The Cinema of Nightfall McFarland p.263 

==Cast==
* Victor Mature as Mike Conway
* Yvonne De Carlo as Natalie Dufort
* George Dolenz as Colonel Charles Dufort
* John Dehner as Emir Bhaki aka The Lion of the Desert
* Marcia Henderson as  Jeanne Marat
* Robert Clarke as Captain Girard
* James Foxx as Lt. Victor Marat

Turkish actor Feridun Çölgeçen was credited as technical adviser.
Fred Carson acted as both stuntman and Victor Matures stand-in. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 