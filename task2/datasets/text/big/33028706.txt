Sham (film)
{{infobox film
| name           = Sham
| image          = Ethel Clayton in Sham (1921) poster.jpg
| imagesize      =
| caption        = Poster
| director       = Thomas N. Heffron
| producer       = Jesse Lasky Adolph Zukor Elmer Harris (play) Geraldine Bonner (play) Douglas Z. Doty (adaptation)
| starring       = Ethel Clayton Theodore Roberts Sylvia Ashton
| music          =
| cinematography = Charles Schoenbaum (* as C. Edgar Schoenbaum)
| editing        =
| distributor    = Paramount Pictures
| released       = May 5, 1921
| runtime        = 5 reels
| country        = United States Silent (English intertitles)
}}
 Elmer Harris and Geraldine Bonner which starred actress Henrietta Crosman. This silent film version was directed by Thomas Heffron and starred Ethel Clayton in the Crosman role with Theodore Roberts, Clyde Fillmore and Sylvia Ashton. This silent film appears to be lost film|lost.  

==Plot==
Based upon a description in a film publication,    Katherine Van Riper (Clayton) is an extravagant young society girl who is very much in debt, and her wealthy aunts and uncle refuse to give her any money. Katherine is desperate enough that she is considering marrying the wealthy Montee Buck (Hiers), although she is in love with the westerner Tom Jaffrey (Fillmore), who says he is poor. Finally, Katherine decides to sell the famous Van Riper pearls, pay off her debts, and marry Tom. However, upon examination the jewelry turns out to be paste, with her father having sold the genuine pearls several years earlier before his death. Montee is assured by the aunts that Katherine will marry him and tells this to Tom. Tom is about to leave town when Uncle James (Ricketts) steps in and pays off Katherines debts, leaving the niece free to marry Tom.

==Cast==
*Ethel Clayton - Katherine Van Riper
*Clyde Fillmore - Tom Jaffrey
*Theodore Roberts - Jeriamiah Buck
*Sylvia Ashton- Aunt Bella
*Walter Hiers - Montee Buck
*Helen Dunbar - Aunt Louisa
*Arthur Edmund Carewe - Bolton
*Tom Ricketts - Uncle James
*Blanche Gray - Clementine Vickers
*Eunice Burnham - Maud Buck
*Carrie Clark Ward - Rosie

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 


 
 