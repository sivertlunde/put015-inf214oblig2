Atlantis: The Lost Empire
 
 
{{Infobox film
| name        = Atlantis: The Lost Empire
| image       = Atlantis The Lost Empire poster.jpg
| alt         = The expedition crew stand together as a mysterious woman is floating in the background, surrounded by stone effigies and emitting brilliant white beams of light from a crystal necklace.
| caption     = Theatrical release poster
| director    = Gary Trousdale Kirk Wise
| producer    = Don Hahn Tab Murphy David Reynolds (additional screenplay material)
| story       = Tab Murphy Gary Trousdale Kirk Wise Bryce Zabel Jackie Zabel Joss Whedon See Cast
| music       = James Newton Howard
| editing     = Ellen Keneshea Walt Disney Feature Animation Buena Vista Pictures
| released    =  
| runtime     = 95 minutes
| country     = United States
| language    = English
| budget      = $90–120 million          
| gross       = $186.1 million 
}}
 traditionally animated Walt Disney Feature Animation—the first science fiction film in List of Disney theatrical animated features|Disneys animated features canon and the 41st overall. Written by Tab Murphy, directed by Gary Trousdale and Kirk Wise, and produced by Don Hahn, the film features an ensemble cast with the voices of Michael J. Fox, Cree Summer, James Garner, Leonard Nimoy, Don Novello, and Jim Varney in his final role before his death. Set in 1914, the film tells the story of a young man who gains possession of a sacred book, which he believes will guide him and a crew of adventurers to the lost city of Atlantis.
 The Hunchback Linguist Marc language specifically for use in Atlantis, while James Newton Howard provided the score. The film was released at a time when audience interest in animated films was shifting away from hand-drawn animation toward films with full CGI.

Atlantis: The Lost Empire premiered at the  , was released in 2003.

== Plot == large tidal wave, triggered by a distant explosion, threatens to drown the island of Atlantis. In the midst of an evacuation from the capital city, the Queen of Atlantis is caught by a strange, hypnotic blue light and lifted up into the "Heart of Atlantis", a powerful crystal protecting the city. The crystal consumes her and creates a dome barrier that protects the citys innermost district. She leaves behind a young daughter, Princess Kida (Cree Summer), and husband, King Kashekim Nedakh (Leonard Nimoy), as the island sinks beneath the ocean.

Nearly nine thousand years later in 1914, Milo Thatch (Michael J. Fox)—a cartographer and linguist at the Smithsonian Institution who is marginalized for his research on Atlantis—believes that he has found the location of The Shepherds Journal, an ancient manuscript allegedly containing directions to the lost island. After his proposal to search for the Journal is rejected by the museum board, a mysterious woman, Helga Sinclair (Claudia Christian), introduces Milo to Preston B. Whitmore (John Mahoney), an eccentric millionaire. Whitmore has already funded a successful effort to retrieve the Journal as repayment of a debt to Milos grandfather, and recruits Milo to join an expedition to Atlantis as soon as he deciphers it.

The expedition departs with a team of specialists led by Commander Rourke (James Garner), who also led the Journal recovery expedition. The crew includes Vinny (Don Novello), a demolitions expert; Mole (Corey Burton), a geologist; Dr. Sweet (Phil Morris), a medical officer; Mrs. Packard (Florence Stanley), a radio operator; Audrey (Jacqueline Obradors), a mechanic; and Cookie (Jim Varney), a mess cook. They set out in the  Ulysses, a massive submarine, but are soon attacked by the monstrous Leviathan, a robotic lobster-like creature that guards Atlantis entrance. The Ulysses is destroyed, but Milo, Rourke, and part of the crew escape and make their way to an underground cavern described in the Journal as the entrance to Atlantis.

After traveling through a network of caves and a dormant volcano, the team reaches Atlantis. They are greeted by Kida — who, despite her age, resembles a woman in her early 20s — and discover that the Atlantean language is the basis of many existing languages which allows the Atlanteans to understand English. Kida enlists Milos aid in deciphering the Atlantean written language, long forgotten by the natives. By swimming deep within the citys submerged ruins and translating underwater murals, Milo helps Kida uncover the nature of the Heart of Atlantis: it supplies the Atlanteans with power and longevity through the crystals worn around their necks. He is surprised this is not mentioned in the Journal, but upon examination realizes a page is missing.

Returning to the surface with Kida, Milo discovers Rourke has the missing page. Rourke and the crew intend to bring the Crystal to the surface and sell it. They offer Milo a chance to join them, which he rejects. Rourke mortally wounds the King of Atlantis while trying to extract information about the crystals location, but finds its location for himself hidden beneath the Kings throne room. The crystal detects a threat and merges with Kida. Rourke and the mercenaries lock Kida in a crate and prepare to leave the city. Knowing that when the crystal is gone the Atlanteans will die, Milo berates his friends for betraying their consciences and ultimately convinces them to leave Rourke and remain in Atlantis. The King explains to Milo that the crystal has developed a consciousness; it will find a royal host when Atlantis is in danger. He admits that he tried to use it as a weapon, but the crystals powers were too great to control, thus leading to the tidal wave that destroyed the city. This lead to his decision to hide it as a precaution to ensure history would not repeat itself, and prevent Kida from meeting the same fate as her mother. He warns Milo that if Kida remains bonded to the Heart of Atlantis, she will be lost to it forever. As he dies, he gives his crystal to Milo, telling him to save Kida and Atlantis. Encouraged by Sweet, Milo rallies the crew and the Atlanteans to stop Rourke.

In a battle inside the volcano, Helga and the other mercenaries are defeated including Rourke, who is killed when Milo slashes his arm with a crystal shard (which gradually turns him to crystal), and then collides with his air ships propellers, breaking him into pieces of crystal. As Milo and the others fly the crystal back to the city, the volcano erupts. With lava flowing towards the city, Kida (in her crystal form) rises into the air and creates a protective shield. The lava breaks away harmlessly, showing a restored Atlantis, and the crystal returns Kida to Milo. The surviving crew members return to the surface and promise to keep the discovery of Atlantis a secret. Milo, having fallen in love with Kida, stays behind to help her rebuild the lost empire.

== Cast ==
 
 .]] linguist and cartographer at the Smithsonian who was recruited to decipher The Shepherds Journal while directing an expedition to Atlantis. Kirk Wise, one of the directors, said that they chose Fox for the role because they felt he gave his characters his own personality and made them more believable on screen. Fox said that voice acting was much easier than his past experience with live action because he did not have to worry about what he looked like in front of a camera while delivering his lines.  The directors mentioned that Fox was also offered a role for Titan A.E.; he allowed his son to choose which film he would work on, and he chose Atlantis.  Viewers have noted similarities between Milo and the films language consultant, Marc Okrand, who developed the Atlantean language used in the film. Okrand stated that Milos supervising animator, John Pomeroy, sketched him, claiming not to know how a linguist looked or acted.    war and Western films, and said the role "fits him like a glove". When asked if he would be interested in the role, Garner replied "Id do it in a heartbeat." 
* Cree Summer as Kidagakash "Kida" Nedakh, the Princess of Atlantis. Kidas supervising animator, Randy Haycock, stated that Summer was very "intimidating" when he first met her; this influenced how he wanted Kida to look and act on screen when she meets Milo.  Natalie Strom provided dialogue for Kida as a young child. Italian Demolition|demolitions improvise dialogue. Edmonds recalled, "  would look at the sheet, and he would read the line that was written once, and he would never read it again! And we never used a written line, it was improvs, the whole movie."  Phil Morris Native American descent. Sweets supervising animator, Ron Husband, indicated that one of the challenges was animating Sweet in sync with Morris rapid line delivery while keeping him believable. Morris stated that this character was extreme, with "no middle ground"; he mentioned, "When he was happy, he was really happy, and when hes solemn, hes real solemn." 
{{ external media
| topic  = Podcast interview about the film with cast members Phil Morris, Cree Summer, Don Novello, Claudia Christian, and Corey Burton.
| audio1 =  , from   retrieved July 3, 2012
}}
* Claudia Christian as Lieutenant Helga Katrina Sinclair, Rourkes German second-in-command. Christian described her character as "sensual" and "striking". She was relieved when she finally saw what her character looked like, joking, "Id hate to, you know, go through all this and find out my character is a toad."  Puerto Rican mechanic and the youngest member of the expedition. Obradors said her character made her "feel like a little kid again" and she always hoped her sessions would last longer. 
*  . Stanley felt that Packard was very "cynical" and "secure": "She does her job and when she is not busy she does anything she wants." 
* David Ogden Stiers as Fenton Q. Harcourt, a board member of the Smithsonian Institution who dismisses Milos belief in the existence of Atlantis. Stiers previously worked with Michael J. Fox in Doc Hollywood He earlier voice-acted for Disney in Beauty and the Beast (1991 film)|Beauty and the Beast and would do so again in Lilo & Stitch.
*   
* Jim Varney as Jebidiah Allardyce "Cookie" Farnsworth, a Western-style chuckwagon chef. Varney died of lung cancer in February 2000, before the production ended, and the film was dedicated to his memory. Producer Don Hahn was saddened that Varney never saw the finished film, but mentioned that he was shown clips of his characters performance during his site sessions and said, "He loved it." Shawn Keller, supervising animator for Cookie, stated, "It was kind of a sad fact that   knew that he was not going to be able to see this film before he passed away. He did a bang-up job doing the voice work, knowing the fact that he was never gonna see his last performance."  Supplemental dialogue for the character was provided by Steve Barr.
* Corey Burton as Gaëtan "Mole" Molière, a French geologist who acts like a mole (animal)|mole. Burton mentioned that finding his performance as Mole was by allowing the character to "leap out" of him while making funny voices. To get into character during his recording sessions, he stated that he would "throw myself into the scene and feel like Im in this make-believe world". 
* Leonard Nimoy as Kashekim Nedakh, the King of Atlantis and Kidas father. Michael Cedeno, supervising animator for King Nedakh, was astounded at Nimoys voice talent, stating that he had "so much rich character" in his performance. As he spoke his lines, Cadeno said the crew would sit there and watch Nimoy in astonishment. 

== Production ==

=== Development ===
  to get a sense of the underground spaces depicted in the film.]] The Hunchback Kurtti 2001, Adventureland setting. clairvoyant readings of Edgar Cayce and decided to incorporate some of his ideas—notably that of a mother-crystal which provides power, healing, and longevity to the Atlanteans—into the story.  They also visited museums and old army installations to study the technology of the early 20th century (the films time period), and traveled 800&nbsp;feet underground in New Mexicos Carlsbad Caverns to view the subterranean trails which would serve as a model for the approach to Atlantis in the film. 
 Tibetan works. Kurtti 2001, circular layout of Atlantis were also based on the writings of Plato,  and his quote "in a single day and night of misfortune, the island of Atlantis disappeared into the depths of the sea"  was influential from the beginning of production.  The crew wore T-shirts which read "ATLANTIS—Fewer songs, more explosions" due the films plan as an Adventure film|action-adventure (unlike previous Disney animated features, which were Musical film|musicals). 

==== Language ====
 
  grammatical structure. He would change the words if they began to sound too much like an actual, spoken language.  John Emerson designed the written component, making hundreds of random sketches of individual letters from among which the directors chose the best to represent the Atlantean alphabet.   The written language was boustrophedon: designed to be read left-to-right on the first line, then right-to-left on the second, continuing in a zigzag pattern to simulate the flow of water. 
 

=== Writing ===
Joss Whedon was the first writer to be involved with the film, but soon left to work on other Disney projects.  Tab Murphy completed the screenplay, stating that the time from initially discussing the story to producing a script that satisfied the film crew was "about three to four months".    The initial draft was 155 pages, much longer than a typical Disney film script (which usually runs 90 pages). When the first two acts were timed at 120 minutes, the directors cut characters and sequences and focused more on Milo. Murphy said that he created the centuries-old Shepherds Journal because he needed a map for the characters to follow throughout their journey.  A revised version of the script eliminated the trials encountered by the explorers as they navigated the underground caves to Atlantis. This gave the film a faster pace, because Atlantis is discovered earlier in the story. 

 
The character of Milo Thatch was originally supposed to be a descendant of Edward Teach, otherwise known as Blackbeard the pirate. The directors later related him to an explorer so he would discover his inner talent for exploration.  The character of Molière was originally intended to be "professorial" but Chris Ure, a story artist, changed the concept to that of a "horrible little burrowing creature with a wacky coat and strange headgear with extending eyeballs", said Wise.   Don Hahn pointed out that the absence of songs presented a challenge for a team accustomed to animating musicals, as solely action scenes would have to carry the film. Kirk Wise said it gave the team an opportunity for more on-screen character development: "We had more screen time available to do a scene like where Milo and the explorers are camping out and learning about one anothers histories. An entire sequence is devoted to having dinner and going to bed. That is not typically something we would have the luxury of doing." 

Hahn stated that the first animated sequence completed during production was the films  .   

=== Animation ===
 

At the peak of its production, 350 animators, artists and technicians were working on Atlantis  ,   as an inspiration.  Because switching to the format would require animation desks and equipment designed for widescreen to be purchased, Disney executives were at first reluctant about the idea.   The production team found a simple solution by drawing within a smaller frame on the same paper and equipment used for standard    Layout supervisor Ed Ghertner wrote a guide to the widescreen format for use by the layout artists and mentioned that one advantage of widescreen was that he could keep characters in scenes longer because of additional space to walk within the frame.  Wise drew further inspiration for the format from filmmakers David Lean and Akira Kurosawa. 
 Ricardo Delgado) hired by the Disney studio for the film. Accordingly, he provided style guides, preliminary character and background designs, and story ideas.  "Mignolas graphic, angular style was a key influence on the look of the characters," stated Wise.  Mignola was surprised when first contacted by the studio to work on Atlantis.  His artistic influence on the film would later contribute to a cult following. 
  }}

The final pull-out scene of the movie, immediately before the end-title card, was described by the directors as the most difficult scene in the history of Disney animation. They said that the pullout attempt on their prior film, The Hunchback of Notre Dame, "struggled" and "lacked depth"; however, after making advances in the process of Multiplane camera|multiplaning, they tried the technique again in Atlantis. The scene begins with one 16-inch piece of paper showing a close-up of Milo and Kida. As the camera pulls away from them to reveal the newly restored Atlantis, it reaches the equivalent of an 18,000-inch piece of paper composed of many individual pieces of paper (24&nbsp;inches or smaller). Each piece was carefully drawn and combined with animated vehicles simultaneously flying across the scene to make the viewer see a complete, integrated image. 

 
At the time of its release, Atlantis: The Lost Empire was notable for using more  " for complicated shots within the film. With the ability to operate in the z-plane, this camera moved through a digital Wire-frame model|wire-frame set; the background and details were later hand-drawn over the wire frames. This was used in the opening flight scene through Atlantis and the submarine chase through the undersea cavern with the Leviathan in pursuit. 

=== Music and sound === Indonesian orchestral sound incorporating chimes, bells, and gongs. The directors told Howard that the film would have a number of key scenes without dialogue; the score would need to convey emotionally what the viewer was seeing on screen. 
 sound production. water pick. 

== Release ==

=== Promotion ===
Atlantis was among Disneys first major attempts to utilize internet marketing. The film was promoted through Kellogg Company|Kelloggs, which created a website with mini-games and a movie-based video game give-away for UPC labels from specially marked packages of Atlantis breakfast cereal.  The film was one of Disneys first marketing attempts through mobile network operators, and allowed users to download games based on the film.  McDonalds (which has an exclusive licensing agreement on all Disney releases) promoted the film with Happy Meal toys, food packaging and in-store decor. The McDonalds advertising campaign involved television, radio, and print advertisements beginning on the films release date.  Frito-Lay offered free admission tickets for the film on specially marked snack packages. 

=== Box office ===
Before the films release, reporters speculated that it would have a difficult run due to competition from   (an action-adventure film from Paramount Pictures). Regarding the markets shift from traditional animation and competition with CGI films, Kirk Wise said, "Any traditional animator, including myself, cant help but feel a twinge. I think it always comes down to story and character, and one form wont replace the other. Just like photography didnt replace painting. But maybe Im blind to it."  Jeff Jensen of Entertainment Weekly noted that CGI films (such as Shrek) were more likely to attract the teenage demographic typically not interested in animation, and called Atlantis a "marketing and creative gamble". 

Atlantis: The Lost Empire had its world premiere at Disneys   also loaned a variety of fish for display within the attraction.  With a budget of $100 million,  the film opened at #2 on its debut weekend, earning $20.3 million in 3,011 theaters.  The films international release began September 20 in Australia and other markets followed suit.  During its 25-week theatrical run, Atlantis: The Lost Empire grossed over $186 million worldwide ($84 million from the United States and Canada).  Responding to its disappointing box-office performance, Thomas Schumacher, then-president of Walt Disney Feature Animation, said, "It seemed like a good idea at the time to not do a sweet fairy tale, but we missed." 

=== Home media   ===
Atlantis: The Lost Empire was released on VHS and DVD January 29, 2002.  During the first month of its home release, the film led in VHS sales and was third in VHS and DVD sales combined.  Sales and rentals of the VHS and DVD combined would eventually accumulate $157 million in revenue by mid-2003.  Both a single-disc DVD edition and a two-disc collectors edition (with bonus features) were released. The single-disc DVD gave the viewer the option of viewing the film either in its original theatrical 2.35:1 aspect ratio or a modified 1.33:1 ratio (utilizing  .   

== Reception ==

=== Critical response ===
Atlantis: The Lost Empire received mixed reviews from film critics. Review aggregator   assigned the film a weighted average score of 52 out of 100 based on 29 reviews from mainstream critics; this was considered "mixed or average reviews".  CinemaScore polls conducted during the opening weekend revealed the average grade cinema-goers gave Atlantis: The Lost Empire was an A on an A+-to-F scale. 

While critics had mixed reactions to the film in general, some praised it for its visuals, action-adventure elements, and its attempt to appeal to an older audience. Roger Ebert gave Atlantis three-and-half stars out of four. He praised the animations "clean bright visual look" and the "classic energy of the comic book style", crediting this to the work of Mike Mignola. Ebert gave particular praise to the story and the final battle scene and wrote, "The story of Atlantis is rousing in an old pulp science fiction sort of way, but the climactic scene transcends the rest, and stands by itself as one of the great animated action sequences."      In The New York Times, Elvis Mitchell gave high praise to the film, calling it "a monumental treat", and stated, "Atlantis is also one of the most eye-catching Disney cartoons since Uncle Walt institutionalized the four-fingered glove."  James Berardinelli, film critic for ReelViews, wrote a positive review of the film, giving it three out of four stars. He wrote, "On the whole, Atlantis offers 90 minutes of solid entertainment, once again proving that while Disney may be clueless when it comes to producing good live-action movies, they are exactly the opposite when it comes to their animated division."  Wesley Morris of the San Francisco Chronicle wrote positively of the films approach for an older audience: "But just beneath the surface, Atlantis brims with adult possibility." 

Other critics felt that the film was mediocre in regards to its story and characters, and that it failed to deliver as a non-musical to Disneys traditional audience.   panned the film, calling it a "new-fashioned but old-fangled hash" and wrote, "Ironically Disney had hoped to update its image with this mildly diverting adventure, yet the picture hasnt really broken away from the tried-and-true format spoofed in the far superior Shrek." 

=== Themes and interpretations ===
Several critics and scholars have noted that Atlantis plays strongly on themes of anti-capitalism and anti-imperialism. M. Keith Booker, academic and author of studies about the implicit messages conveyed by media, views the character of Rourke as being motivated by "capitalist greed" when he pursues "his own financial gain" in spite of the knowledge that "his theft   will lead to the destruction of  ".  Religion journalist Mark Pinsky, in his exploration of moral and spiritual themes in popular Disney films, asserts that "it is impossible to read the movie ... any other way" than as "a devastating, unrelenting attack on capitalism and American imperialism".  Max Messier of AMC Networks|FilmCritic.com observes, "Disney even manages to lambast the capitalist lifestyle of the adventurers intent on uncovering the lost city. Damn the imperialists!"  According to Booker, the film also "delivers a rather segregationist moral" by concluding with the discovery of the Atlanteans kept secret from other surface-dwellers in order to maintain a separation between the two highly divergent cultures.  Others saw Atlantis as an interesting look at utopian philosophy of the sort found in classic works of science fiction by H. G. Wells and Jules Verne. 
 news group Daniel Jackson, the protagonist of Stargate and its spinoff television series Stargate SG-1—which coincidentally launched its own spinoff, titled Stargate Atlantis. 

=== Accolades   ===
{| class="plainrowheaders wikitable" style="align:center; font-size: 90%;"
|-
! scope="col"| Award
! scope="col"| Category
! scope="col"| Name
! scope="col"| Outcome
|- 29th Annie Awards  || Individual Achievement in Directing || Gary Trousdale and Kirk Wise ||  
|-
| Individual Achievement in Storyboarding || Chris Ure ||  
|-
| Individual Achievement in Production Design || David Goetz ||  
|-
| Individual Achievement in Effects Animation || Marlon West ||  
|-
| Individual Achievement in Voice Acting – Female || Florence Stanley ||  
|-
| Individual Achievement in Voice Acting – Male || Leonard Nimoy ||  
|-
| Individual Achievement for Music Score || James Newton Howard ||  
|- 2002 DVD Exclusive Awards  || Original Retrospective Documentary || Michael Pellerin ||  
|- 2002 Golden Reel Award  || Best Sound Editing – Animated Feature Film || Gary Rydstrom, Michael Silvers, Mary Helen Leasman, John K. Carr, Shannon Mills, Ken Fischer, David C. Hughes, and Susan Sanford ||  
|- Best Animated Feature || ||  
|- 2002 Political Democracy || ||  
|- Human Rights || ||  
|- Peace || ||  
|- World Soundtrack Best Original Song for Film || Diane Warren and James Newton Howard ||  
|-
| Young Artist Awards  || Best Feature Family Film – Drama || Walt Disney Feature Animation ||  
|}
 

== Related works ==
Atlantis: the Lost Empire was meant to provide a springboard for an animated television series entitled Team Atlantis, which would have presented the further adventures of its characters. However, because of the films under-performance at the box office the series was not produced. On May 20, 2003, Disney released a  , consisting of three episodes planned for the aborted series.  In addition, Disneyland planned to revive its Submarine Voyage ride with an Atlantis theme with elements from the movie and the ride was promoted with a meet-and-greet by the movies characters. These plans were canceled and the attraction was re-opened in 2007 as the Finding Nemo Submarine Voyage, its theme based on the 2003 Walt Disney Pictures|Disney·Pixar animated film Finding Nemo. 

=== Soundtrack ===
{{Infobox album
| Name     = Atlantis: The Lost Empire
| Type     = soundtrack
| Artist   = James Newton Howard
| Cover    =
| Released =  
| Length   =   Walt Disney
| Producer = James Newton Howard Jim Weidman
}}
  3D album bootlegged and Filmtracks said, "Outside of about five minutes of superior additional material (including the massive opening, "Atlantis Destroyed"), the complete presentation is mostly redundant. Still, Atlantis is an accomplished work for its genre." 

=== Video games ===
{{Video game reviews width = 26m GR = (PS) 73.83%    (GBC) 64.50%    (GBA) 55.86%    MC = (PS) 73/100    (GBA) 51/100    Allgame = (PS)    (GBA)    CVG = (GBA) 4/10  EGM =(PS) 7/10  GI = (GBA) 7.25/10  GameRev = (PS) B−  GameZone = (GBA) 7.8/10  IGN =(PS) 7.5/10  NP =(GBC)     OPM =   
}}
There are several video games based on the film.   (commonly known as Atlantis: Search for the Journal) was developed by   (commonly known as Atlantis: Trial by Fire) was the second game developed by Zombie Studios and published by Disney Interactive, and was released May 18, 2001 for the Microsoft Windows platform. 

Atlantis: The Lost Empire is an   and Game Boy Color. It is a platform game in which the player controls Milo and three other characters from the film across 14 levels on a quest to discover Atlantis.   The game was met with average to mixed reviews upon release. GameRankings and Metacritic gave it a score of 73.83% and 73 out of 100 for the PlayStation version;   64.50% for the Game Boy Color version;  and 55.86% and 51 out of 100 for the Game Boy Advance version.  
 

== See also ==
* Atlantis in popular culture

== Notes ==
 

== References ==
 

=== Bibliography ===
; Books
 
*  
*  
*  
*  
*  
*  
*  
*  
 

; DVD media
 
*   | ref = DVD1}}
*   | ref = DVD2}}
 

; Periodicals
 
*  
 

== External links ==
 
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 