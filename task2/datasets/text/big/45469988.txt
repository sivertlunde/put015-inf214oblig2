The Strange Case of Clara Deane
{{Infobox film
| name           = The Strange Case of Clara Deane
| image          = 
| alt            = 
| caption        = 
| director       = Louis J. Gasnier Max Marcin
| producer       = 
| screenplay     = Max Marcin Pat OBrien Dudley Digges George Barbier Russell Gleason Lee Kohlmar
| music          = John Leipold
| cinematography = Henry Sharp  
| editing        = 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 Pat OBrien, Dudley Digges, George Barbier, Russell Gleason and Lee Kohlmar. The film was released on May 6, 1932, by Paramount Pictures. {{cite web|url=http://www.nytimes.com/movie/review?res=9803E1DE153EE633A25755C0A9639C946394D6CF|title=Movie Review -
  The Strange Case of Clara Deane - An Unfortunate Mother. - NYTimes.com|work=nytimes.com|accessdate=22 February 2015}}  
 
==Plot==
 

== Cast ==
*Wynne Gibson as Clara Deane Pat OBrien as Frank Deane Dudley Digges as Detective Garrison
*Frances Dee as Nancy Deane George Barbier as Richard Ware
*Russell Gleason as Norman Ware
*Lee Kohlmar as Moses Herzman
*Cora Sue Collins as Nancy Deane 
*Florence Britton as Miriam Ware 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 