Hum Intezaar Karenge
{{Infobox film
 | name = Hum Intezaar Karenge
 | image = HumIntezaarKarenge89.jpg
 | caption = VCD Cover
 | director = Prabhat Roy
 | producer = Neelima Paul
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Padmini Kolhapure Vinod Mehra Jagdeep Shakti Kapoor Shafi Inamdar Dina Pathak
 | music = Bappi Lahiri
 | lyrics = 
 | cinematographer = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released =  1989 (India)
 | runtime = 140 min.
 | language = Hindi Rs 6 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1989 Hindi Indian feature directed by Prabhat Roy, starring Mithun Chakraborty, Padmini Kolhapure, Vinod Mehra, Jagdeep, Shakti Kapoor, Vinod Mehra, Shafi Inamdar and Dina Pathak.

==Plot==

Hum Intezaar Karenge is an action drama, featuring Mithun Chakraborty and Padmini Kolhapure in lead roles, well supported by Vinod Mehra, Jagdeep, Shakti Kapoor, Vinod Mehra and Shafi Inamdar.

==Cast==

*Mithun Chakraborty...... Ajay 
*Padmini Kolhapure...... Manisha 
*Vinod Mehra...... Ravi 
*Jagdeep
*Shakti Kapoor...... Kundan 
*Shafi Inamdaar...... Bar. Vikas Anand 
*Shiva Rindan...... Shankar 
*Dina Pathak...... Ravis mother 
*Urmila Bhatt...... Jyotis mother 
*Beena...... Jyoti 
*Birbal...... Pinto 
*Mohan Choti...... Taxi driver 
*Tiku Talsania...... Wild Life Professor 
*Brijesh Tiwari......
*Shakila
*Johar
*Manmouji
*Moolchand
*Sunil
*Jagdev Bhambri
* baby amita choksi.... Junior Ajay

==References==
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Hum+Intezar+Karenge -
* http://www.bollywoodhungama.com/movies/cast/5290/index.html

==External links==
*  

 
 
 
 

 