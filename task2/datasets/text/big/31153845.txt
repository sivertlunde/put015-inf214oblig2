Cheenavala
{{Infobox film
| name = Cheenavala
| image =
| caption =
| director = Kunchacko
| producer = Kunchacko
| writer = Sarangapani
| screenplay = Sarangapani
| starring = Prem Nazir Jayabharathi KPAC Lalitha Adoor Bhasi
| music = M. K. Arjunan
| cinematography = Balu Mahendra
| editing = TR Sekhar
| studio = Udaya
| distributor = Udaya
| released =  
| country = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film, directed by Kunchacko and produced by Kunchacko. The film stars Prem Nazir, Jayabharathi, KPAC Lalitha and Adoor Bhasi in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
  
*Prem Nazir as Pushkaran 
*Jayabharathi as Pennaal 
*KPAC Lalitha as Maanikki 
*Adoor Bhasi as Pappu 
*Thikkurissi Sukumaran Nair as Richman Rana 
*Pattom Sadan as Mohan 
*Sankaradi as Konthi 
*Adoor Pankajam as Karthyayani 
*Janardanan as Rowdi Paachan 
*K. P. Ummer as Prathapachandran  Kunchan as Madhu 
*Kuthiravattam Pappu as Fernandes  Meena as Paaru 
*Nellikode Bhaskaran as Ayyappan 
*Sathi
*TS Radhamani
 
 
==Soundtrack==
The music was composed by M. K. Arjunan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Azhimukhathu || K. J. Yesudas || Vayalar || 
|- 
| 2 || Kanyaadaanam || K. J. Yesudas, B Vasantha || Vayalar || 
|- 
| 3 || Poonthurayil   || Ambili || Vayalar || 
|-  Susheela || Vayalar || 
|- 
| 5 || Thalirvalayo || K. J. Yesudas || Vayalar || 
|}

==References==
 

==External links==
*   
*  

 
 
 


 