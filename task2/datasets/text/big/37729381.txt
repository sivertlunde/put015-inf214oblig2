Santo contra el cerebro diabolico
 
{{Infobox film
| name           = Santo contra el cerebro diabolico
| image          = Santo contra el cerebro diabolico film poster 1963.jpg
| caption        = Theatrical release poster
| director       = Federico Curiel
| producer       =
| writer         = Federico Curiel
| starring       = Fernando Casanova Ana Bertha Lepe Rodolfo Guzmán Huerta
| music          = Enrico C. Cabiati (credited as Enrico Cabiati) Federico Curiel	 	
| cinematography = Fernando Colín
| editing        = José Juan Munguía
| distributor    = Something Weird Video
| studio         = Azteca Films
| released       =  
| runtime        = 90 minutes
| country        = Mexico Spanish
| budget         =
}}

Santo contra el cerebro diabolico (marketed as Santo and the Diabolical Brain or Santo vs. the Diabolical Brain outside of Mexico) is a 1963 Mexican action film directed by Federico Curiel, written by Curiel and Antonio Orellana and starring Rodolfo Guzmán Huerta (reprising the role of El Santo)       

==Plot==
A crazed scientist named Doctor Zuko creates a creature similar to that of Frankensteins Monster and uses it to battle the masked hero, El Santo (Rodolfo Guzmán Huerta).

==Cast==
* Fernando Casanova as Fernando Lavalle
* Ana Bertha Lepe as Virginia 
* Roberto Ramírez Garza as Conrado
* Luis Aceves Castañeda as Refugio Canales
* Celia Viveros as La Jarocha
* José Chávez as Roque
* Santo as himself (credited as "Santo" El Enmascarado de Plata)
* Augusto Benedico as Santos assistant
* Manuel Dondé as Carlos

==References==
 

==External links==
 
*  
*  

 
 
 
 
 


 
 