Verde por fora, vermelho por dentro
Verde Portuguese  Ricardo Costa.

==History== Ricardo Costa, neorealistic tendency of the Portuguese films of its time, it caricaturizes politics and social agents in a surrealistic mood. It was highly controversial.

==Synopsis==
"A middle-aged businessman, a neoliberal, returns to Portugal after the Carnation Revolution to realise the ambitious dream of a banana plantation and help reconstruct the national economy, weakened  by the "communists". He fails. He even fails his death, from which he mysteriously escapes. To be continued ?" 

==Cast==
* Rogério Paulo
* Adelaide João
* Armando Venâncio
* Maria Teresa Melro
* Ana Luísa Nascimento
* Luís Alberto
* António Machado
* Pedro Valentim
* Mário Vasco
* António Germano Anjos
* António Anjos
* Helena Isabel
* and others

==Credits== Ricardo Costa, Ilídio Ribeiro e Maurício Cunha
* Production – Diafilme (1978 - 1979)
* Producers – Ilídio Ribeiro and Ricardo Costa
* Director – Ricardo Costa
* Music and songs –   
* Cinematography – Vítor Estêvão
* Sound engineer – Vítor Duarte
* Format – 35 mm colour
* Language – Portuguese and English
* Locations - Vale de Lobos (interiors: house of Alexandre Herculano)
* Genre – comedy (comic drama)
* Production year – 1978
* Studios – Tobis Portuguesa and Nacional Filmes
* Distribution – Doperfilme
* Premiere – Estúdio 144, Lisbon, 16 October 1980

==Festivals==
* 9th International Film Festival of Figueira da Foz (1980) – Portugal
* International Film Festival of Santarém, Portugal|Santarém (1980) – Portugal
* Thessaloniki International Film Festival (1981) – Greece
* Mediterranean Film Festival of Lecce (1982) – Italy

==Bibliographic references==
*O Cais do Olhar by José de Matos-Cruz, Portuguese Cinematheque, 1999
* , article by José de Matos-Cruz

==References==
 

==External links==
*   – producer’s web page
*   at IMDb
*   by   – English, French and Portuguese
*   at  
*   – IMDb

 
 
 
 