Richie Rich's Christmas Wish
 
{{Infobox film
| name           = Richie Rich $  Christmas Wish
| image          = RichieRichsChristmasWish.jpg
| caption        = DVD cover
| director       = John Murlowski Mike Elliott Amy Goldberg
| writer         = Jason Feffer Mark Furey Rob Kerchner
| starring       = David Gallagher Martin Mull Michelle Trachtenberg Eugene Levy Keene Curtis
| music          = Deddy Tzur, Saban
| cinematography = Christian Sebaldt
| editing        = John Gilbert
| studio         = Saban Entertainment Harvey Entertainment
| distributor    = Warner Home Video
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
}} Richie Rich George Bailey and Reggie Van Dough in the role of Mr. Potter. Currently, it appears on television annually during ABC Familys 25 Days of Christmas.

==Plot==
Its Christmas Eve and Richie Rich, the worlds richest kid, is all excited to spend the day with his friends. While racing wildly with his friends in the snow, Richies butler, Cadbury, controls the cars using a remote and guides the children back to Richies house. He then reminds him about his responsibilities that are due to be executed on Christmas Eve, and instructs him to change his clothes and get ready for tea. Before going to do so, he visits his home scientist, Professor Keanbean, who shows him his recent invention, a wishing machine which works only on Christmas Eve. The day being that, Richie wishes for a "big pie" from it, and is given a "pig sty" instead. Cadbury is disgusted seeing this, and sends him off to change his clothes. He meets his parents, Richard and Regina Rich, to ask what they would like to have for Christmas. While with them, he also tries out his fathers new fishing rod invented by Keanbean, which hooks on to a tuna sandwich in the vicinity. He then goes on to change his clothes.

During tea, Richie meets his spoiled cousin, Reggie Van Dough, who wishes that he was as rich as Richie. Later, he dresses up like an elf and Cadbury does so like Santa Claus to distribute Christmas presents to the orphanage run by the Rich family. While getting ready, Cadbury tells Richie about how he was a rock star in his youth days, in a band called Root Canal. When they take off in the sleigh with Richie driving it, Reggie takes control of it using a remote invented by Keanbean. He guides it through streets by shops, houses, and people, thus nearly destroying everything in the whole process. Richie and Cadbury end up in an accident in which the sleigh falls and literally explodes along with the presents, while Cadbury hurts his knee badly. Richie runs off to fetch help, but once he enters the city, he sees that the situation has changed dramatically. Reggie is cooking up rumors about him, and all the people have turned against him. 

Devastated, Richie goes into Keanbeans laboratory and squats in front of the wishing machine. While fretting over his ill luck, he accidentally wishes that he was never born. The machine at once grants his wish, following which he is transported to another world in which he was never born and hence nobody recognizes him. Reggie has taken over as Richard and Reginas son, and is now the richest kid in the world. He bosses around everyone. Roads and buildings are named after him. This world is very sad, and hunger and misery are seen all over. This makes Richie realize that things would not be better if he was not born, and hence decides to go back to the world in which he is Richie Rich. 

Though his parents do not recognize him anymore, he is happy that his dog, Dollar, does. He takes Dollar with him, only to enrage Reggie, who is his current owner. Reggie orders policemen to search for Richie, who is falsely called the "dognapper", and also announces a reward for catching him. After outsmarting various policemen who try but fail to catch him, Richie finds Cadbury, who is still part of Root Canal, and Keanbean, who runs his own laboratory called "Keanbeans World of Wonders". Richie questions Keanbean about the wishing machine, which he says, requires a Pegliasaurus wishing bone in order to be complete. Along with his friends who decide to help him, Richie goes to the city museum to retrieve the bone from the dinosaur skeleton. After passing through laser detection systems successfully, they get it, using the fishing rod invented by Keanbean. Before they get out of the museum, Richie and his friends spot Reggies parents, who are now working as night guards there. 

Once they reach the lab, they get the machine to work properly. However, before Richie could wish himself back, Reggie arrives there with the policemen. Richie, Cadbury, Keanbean, and Richies friends are put in jail, while Reggie takes the machine home. At home, Reggie wishes that he could fly, and then when Irona, his robotic servant maid, accidentally turns off the machine, he tries to make a wish again. When it does not work the second time, he leaves the room in a huff, and retires. In jail, Richie and his friends are set free by Dollar. They all rush to Reggies house, and while he is still sleeping, Richie tries to wish himself back. However, they find that the machine is no longer working, as Reggie had kicked it in anger earlier. While Keanbean is fixing it, Reggie wakes up and comes flying in, only to be attacked by Richie and his friends. They defeat him and everyone quits working for him, with the police chief refusing to work for someone who would cancel Christmas. After that, the machine starts working again, and Richie wishes himself back as Richie Rich.  

Richie sets right all the things that had gone wrong since his vanishing act, and is now much more grateful to be alive. As everyone is glad to have him back, they gather around the Christmas tree and sing.

===Extended TV Ending===
Reggie admitted to Richie that he was the one who took control of the sleigh and ruined the event. Richie realizes that it wasnt his fault and he made that wish for nothing. They forgive each other.

==Cast==
* David Gallagher - Richard "Richie" Rich Jr., the main protagonist.
* Martin Mull - Richard Rich Sr.
* Lesley Ann Warren - Regina Rich
* Jake Richardson - Reggie Van Dough, the main antagonist
* Eugene Levy - Professor Keanbean
* Keene Curtis - Herbert Cadbury
* Richard Riehle - Sgt. Mooney
* Don McLeod - Irona
* Michelle Trachtenberg - Gloria (sharing the given name of Gloria Glad)
* Richard Fancy - Mr. Van Dough (Not to be confused with Lawrence Van Dough from the first film)
* Marla Maples - Mrs. Van Dough
* Blake Jeremy Collins - Freckles
* Austin Stout - Pee Wee

==Location shots==
The Langham Huntington Hotel and Spa, in Pasadena, CA, was used for exterior scenes of the sprawling Rich Family Mansion. The interior lobby and vault of the disused (currency exchange) Valuta Bank building, in downtown Los Angeles, was used for the scene in which Richie traps the pursuing Rich security men (this same bank was also used for the bank-robbery scene in   and Casper Meets Wendy, obviously because the music was done by Saban. Colonial Street at Universal City, previously used in The Munsters, Leave it to Beaver, and The Burbs, is also featured.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 