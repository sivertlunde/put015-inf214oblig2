Le Wazzou polygame
{{Infobox film
| name           = Le Wazzou polygame
| image          = Wazzou.jpg
| image size     =
| caption        = screenshot
| director       = Oumarou Ganda
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1971
| runtime        = 50 minutes
| country        = Niger France
| language       = Djerma
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1971 Nigerien/France|French film about polygamy directed by  and starring Oumarou Ganda. It was produced by Argos Films in France.  It won the Grand Prize at the 1972 Panafrican Film and Television Festival of Ouagadougou, and was the first official winner of that festival. 

==Cast==
*Oumarou Ganda
*Joseph Salamatou
*Zalika Souley
*Garba Mamane
*Amadou Seyni
*Ousmane Diop

==See also==
*Polygamy in Niger

==References==
;Footnotes
{{Reflist|refs=
 {{Citation  | title = Le Wazzou Polygame  | work = British Film Institute  | url = http://ftvdb.bfi.org.uk/sift/title/392433  | accessdate =22 April 2011
}} 
 #Pfaff|Pfaff, p. 265 
 #Thackway|Thackway, p. 220 
 {{Citation  | title = About Fepasco  | work = BBC World Service  | publisher = BBC  | year = 2003  | url = http://www.bbc.co.uk/worldservice/specials/1551_whatisfespaco/page2.shtml  | accessdate =22 April 2011
}} 
}}
;Bibliography
 
*{{Citation
  | last = Pfaff
  | first = Françoise
  | title = Focus on African Films
  | publisher = Indiana University Press
  | year = 2004
  | isbn = 0-253-21668-0
  | ref = Pfaff}}
*{{Citation
  | last = Thackway
  | first = Melissa
  | title = Africa Shoots Back: Alternative Perspectives in Sub-Saharan Francophone African Film
  | publisher = James Currey Publishers
  | year = 2003
  | isbn = 0-85255-576-8
  | ref = Thackway}}
 

==External links==
* 

 
 
 
 
 
 
 


 