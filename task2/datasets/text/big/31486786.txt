Talk of the Devil
 
  British crime film directed by Carol Reed and starring Ricardo Cortez, Sally Eilers and Basil Sydney.  A dishonest shipbuilder plans to frame his half-brother for his own criminal activities. It was the first film to be completely made at Pinewood Studios. 

==Cast==
* Ricardo Cortez - Ray Allen 
* Sally Eilers - Ann Marlow 
* Basil Sydney - Stephen Rindlay 
* Randle Ayrton - John Findlay  Charles Carson - Lord Dymchurch 
* Frederick Culley - Mr Alderson  Anthony Holles - Colquhoun  Gordon McLeod - Inspector 
* Denis Cowles - Philip Couls 
* Quentin McPhearson - Angus 
* Langley Howard - Clerk 
* Margaret Rutherford - Housekeeper 
* Moore Marriott - Dart-thrower

==References==
 

==Bibliography==
* Pykett, Derek. British Horror Film Locations. McFarland & Company, 2008.

==External links==
* 

 

 
 
 
 
 
 


 