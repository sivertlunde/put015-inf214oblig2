Once Upon a Time Was I, Verônica
{{Infobox film
| name           = Once Upon a Time Was I, Verônica
| image          = Once Upon a Time Was I, Verônica.jpg
| alt            = Theatrical release poster
| caption        = Theatrical poster Marcelo Gomes	
| producer       = João Vieira Jr. Sara Silveira Maria Ionescu Chico Ribeiro Ofir Figueiredo
| screenplay     = Marcelo Gomes	
| starring       = Hermila Guedes João Miguel (actor)|João Miguel W. J Solha
| music          = Tomaz Alves Souza Karina Buhr
| cinematography = Mauro Pinheiro Jr
| editing        = Karen Harley	 	
| studio         = Dezenove Som e Imagem  REC Produtores Associados
| distributor    = Imovision
| released       =  
| runtime        = 91 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = R$209,975  
}}
 Brazilian drama Marcelo Gomes. At the 45th Festival de Brasília it shared the Best Film Award with Theyll Come Back, and won Best Supporting Actor (W. J Solha), Best Screenplay, Best Cinematography and Best Score. 

==Plot==
Veronica, a newly graduated medical student, goes through a moment of reflection and doubt. She questions not only her professional choices, but also her intimate relationships and even her ability to cope with life. 

==Cast==
*Hermila Guedes as Verônica
*João Miguel (actor)|João Miguel as Gustavo
*W. J Solha as Zé Maria
*Renata Roberta as Maria
*Inaê Veríssimo as Ciça
*Maeve Jinkings

==References==
 

==External links==
*      
*  

 

 
 
 
 
 
 

 
 