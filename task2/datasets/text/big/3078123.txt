Microcosmos (film)
{{Infobox film name = Microcosmos image = Microcosmos.jpg director = Claude Nuridsany Marie Pérennou producer = Christophe Barratier Yvette Mallet Jacques Perrin writer = Claude Nuridsany Marie Pérennou starring = movie_music = Bruno Coulais studio = France 2 Cinema Canal+ Télévision Suisse Romande distributor = BAC Films (France) Miramax Films (US) Pathé (UK) released =   runtime = 80 minutes country = France Switzerland Italy language = French music = awards = budget = €3.81 million
}}
__NOTOC__
Microcosmos (original title Microcosmos: Le peuple de lherbe &mdash; Microcosmos: The grass people) is a 1996  documentary film by Claude Nuridsany and Marie Pérennou and produced by Jacques Perrin. Set to the music of Bruno Coulais, this film is primarily a record of detailed interactions between insects and other small invertebrates. 

The film was screened out of competition at the 1996 Cannes Film Festival.   

Scenes from the film were used in the music video for the single "You Dont Love Me (Like You Used to Do)" from The Philosopher Kings album Famous, Rich and Beautiful.

==Reception==
The film holds a 97% rating on critical aggregation website Rotten Tomatoes.

==Awards and nominations==
*César Awards (France)
**Won: Best Cinematography (Thierry Machado, Claude Nuridsany, Marie Pérennou and Hugues Ryffel)
**Won: Best Editing (Florence Ricard and Marie-Josèphe Yoyotte)
**Won: Best Music (Bruno Coulais)
**Won: Best Producer (Jacques Perrin)
**Won: Best Sound (Philippe Barbeau and Bernard Leroux)
**Nominated: Best Film
**Nominated: Best First Work (Claude Nuridsany and Marie Pérennou)
**Nominated: Best Sound (Laurent Quaglio)

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 

 