Chellata (film)
 
{{Infobox film
| name           =  Chellata
| image          =  
| caption        =  
| director       = M. D. Sridhar
| producer       = Jagadish Kotyan
| story          = M. D. Sridhar 
| screenplay     = M. D. Sridhar
| starring       = Ganesh (actor)|Ganesh,  Rekha Vedavyas,  Devaraj
| music          = Gurukiran
| cinematography = Sundarnath Suvarna
| editing        = P. R. Soundar Raj
| studio         = Bhoomi Productions
| distributor    = 
| released       =  
| runtime        = 157 minutes
| country        = India Kannada
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
Chellata ( ) is a 2006 Indian Kannada language film directed by M. D. Sridhar and produced by Jagadish Kotyan under the Bhoomi Productions banner. It stars Ganesh (actor)|Ganesh, Rekha Vedavyas and Devaraj in pivotal roles. The soundtrack was composed by Gurukiran.   

The film is a remake of the Malayalam Pulival Kalyanam (2003) that starred Jayasurya and Kavya Madhavan.  

==Plot==
Ganesh (Ganesh (actor)|Ganesh) is the adopted younger brother of Devaraj. When Devaraj loses his right hand in an accident, Ganesh at a young age takes responsibility of his family. Devarajs sister is in love with the son of Rangayana Raghu, who is unscrupulous and demands a huge dowry for the marriage to be made possible. To make this marriage possible Devaraj and Ganesh take a loan from a financier (Mohan Guneja) and invest it in an explosives business, only to lose it completely.

Simultaneously, Ganesh gets his phone muddled with Rekha (Rekha Vedavyas) on account of both the models being the same. Rekhas father   
Avinash is the boss of Rangayana Raghu. Ganesh and Rekha have loads of comical misunderstandings before falling in love and deciding to get married. Of course this is not acceptable to Avinash, and he puts obstacles in their way. The story is whether or not Ganesh and Rekha can overcome these obstacles.
==Cast== Ganesh as Ganesh
*  Rekha Vedavyas as Rekha
*  Devaraj
*  Rangayana Raghu
*  Tennis Krishna
*  Umashri
*  Avinash
*  Komal Kumar
*  Mohan Guneja

==Box Office==

The film opened to positive reviews from critics and performed well at the box-office by completing 100 days.
 
==Soundtrack==
{{Infobox album  
| Name        = Chellata
| Type        = Soundtrack
| Artist      = Gurukiran
| Cover       =
| Released    =  2006
| Recorded    = Feature film soundtrack
| Length      =
| Label       = Anand Audio
}}

{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
| Track # || Song || Singer(s) || Duration
|-
| 1 || Alele Tuntu || Shamita Malnad, Murali Mohan || 04:39
|- Nanditha || 04:17 
|-
| 3 || Pataaki Pataaki || Udit Narayan || 04:42
|-
| 4 || Indu Yenage Govinda || Gurukiran, Chetan Sosca || 04:38
|-
| 5 || Jama Jama Jamaisu || Haneef, Chorus||
|}
 

==Home media==
The movie was released on DVD with 5.1 channel surround sound and English subtitles and VCD.

==References==
 

==External links==
*  

 
 
 
 
 
 