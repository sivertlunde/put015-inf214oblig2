Gumrah (1963 film)
{{Infobox film
| name           = Gumraah
| image          = Gumrah.jpg
| image_size     =
| caption        =
| director       = B. R. Chopra
| producer       = B. R. Chopra
| writer         =
| narrator       =
| starring       = Sunil Dutt Ashok Kumar Mala Sinha Ravi
| cinematography =
| editing        = Pran Mehra
| distributor    =
| released       = 1963
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| preceded_by    =
| followed_by    =
}} Ravi while the lyrics were by Sahir Ludhianvi. It was remade in Malayalam as Vivahita (1970), starring Padmini (actress)|Padmini, Prem Nazir and Satyan. It was also a hit. It is really surprising that while Shashikala got Filmfares award in Best Supporting Actress Category, Mala Sinha was totally ignored by Film Fare although her role was equally tough and difficult and she gave her one hundred percent share.

==Plot==
Meena (Mala Sinha) and Kamla (Nirupa Roy) are two daughters of a wealthy Nainital resident. While Kamla lives with her established attorney husband, Ashok (Ashok Kumar), in Mumbai, Meena is in love with artist-singer Rajendra (Sunil Dutt). When Kamla comes to Nainital for her delivery, she comes to know of Meenas affair and plans to get her married to Rajendra. Ashok however, is totally unaware of this fact.

Before Kamla can do this, she dies after falling off a cliff near her fathers home. Afraid that her sisters children will be ill-treated by a stepmother, Meena is compelled to marry Ashok. Ashok does not know about her love affair with Rajendra. For a while things go well, until she meets Rajendra again. He follows her to Mumbai, and they begin meeting secretly.

One day, Meena is caught by Leela (Shashikala), a woman who claims to be Rajendras wife and who begins blackmailing her. Caught between Rajendras deception and Leelas hounding, Meenas life comes to a crisis, and she is forced to make a choice between Rajendra and Ashok.

The movie examines the conflict of a married woman who is caught between her feelings for her lover and her duty to her husband and family. A bold theme for the times (1963), the same conflict is examined again in the 2005 Kareena Kapoor-starrer Bewafaa (2005 film)|Bewafaa.

==Cast==
* Ashok Kumar as Ashok
* Sunil Dutt  as Rajendra
* Mala Sinha as Meena
* Nirupa Roy as Kamla
* Shashikala as Leela

==Soundtrack==
{{Tracklist
| headline     = Songs
| extra_column = Playback Ravi
| all_lyrics   =
| title1  = Aa Bhi Ja
| extra1  = Mahendra Kapoor

| title2  = Aa Ja Aa Ja Re
| extra2  = Mahendra Kapoor, Asha Bhosle

| title3  = Aap Aaye To
| extra3  = Mahendra Kapoor

| title4  = Chalo Ek Baar Phir Se
| extra4  = Mahendra Kapoor

| title5  = Ek Pardesi Door Se Aaya
| extra5  = Asha Bhosle

| title6  = Ek Thi Ladki Meri Saheli
| extra6  = Asha Bhosle

| title7  = Tujhko Mera Pyar Pukare
| extra7  = Mahendra Kapoor, Asha Bhosle
}}

==Awards==
 
|- 1963
| B. R. Chopra Certificate of Merit for Third Best Hindi Feature Film   
|  
|-
| Shashikala
| Filmfare Award for Best Supporting Actress
|  
|-
| Mahendra Kapoor
| Filmfare Award for Best Male Playback Singer
|  
|-
| Pran Mehra
| Filmfare Award for Best Editing
|  
|}

==References==
 

== External links ==
*  
 
 

 
 
 
 