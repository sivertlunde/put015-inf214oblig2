Murder in a Blue World
{{Infobox film
| name           = Murder in a Blue World
| image          = Una gota de sangre para morir amando.jpg
| image size     =
| border         =
| alt            =
| caption        = Theatrical release poster
| director       = Eloy de la Iglesia
| producer       = José Frade.
| writer         =
| screenplay     = Eloy de la Iglesia    José Luis Garci   Antonio Fons    Antonio Artero   George Lebourg
| story          =
| based on       =  
| narrator       =
| starring       = Sue Lyon    Christopher Mitchum    Jean Sorel
| music          = Georges Garvarentz
| cinematography = Francisco Fraile
| editing        = José Luis Matesanz
| studio         = José Frade P.C.
| distributor    =
| released       = 22 August 1973
| runtime        = 100 min
| country        = Spain
| language       = English
| budget         =
| gross          = ESP 25,198,396 (Spain)
}}
 Spanish science fiction/crime film directed by Eloy de la Iglesia and starring Sue Lyon, Christopher Mitchum and Jean Sorel. Set in the near future, the plot follows a respectable nurse who seduces young men, take them home to bed, listen to the post-coital beating of their hearts, and then stab them to death with a surgical scalpel. The film takes some cues from Stanley Kubricks A Clockwork Orange. It was released in the United States as Clockwork Terror. Bentley,  A Companion to Spanish Cinema, p. 165 

==Plot==
Anna Vernia, a beautiful young nurse, receives a medal of recognition for her outstanding dedication to her patients at the medical center where she works. She is going out with Victor Sender, a doctor working in the same hospital. Victor is deeply involved in a project that employs electro-shock therapy in violent criminals in an effort to turn them into model citizens.

Crime is rampant in the city. There has been a number of unresolved killings of  young men which have been attributed to a serial killer believed to be a sadistic homosexual. A family is getting ready to watch Stanley Kubricks  A Clockwork Orange on television  when they are assaulted by a gang of delinquents who knock at their door. The assailants, wearing red helmets, leather biker’s outfits, and handling bull whips smash the modern looking apartment. They rape both husband and wife, but leave the couple’s young son unharmed. After their crime, there is a dispute among the four members of the gang. One gang member, David, is beaten up and expelled from the group.

Anna is a pop art collector. She is the highest bidder in an auction of Alex Raymond’s artwork for Flash Gordon. At the auction she gives her phone number to her bidder rival, Toni, a young man with a handicapped leg. Anna lives alone in a large mansion in the outskirts of the city where Toni comes to see her. After they have sex, Anna listens to Toni’s heart beats while he sleeps and stabs him to death with a surgical scalpel. She disposes of Tony body in a river, but she has been secretly observed by David. He finds out Annas information through her car’s number plate and begins to follow her.
Wearing a wig and dressed matronly, Anna seduces Bruno, a narcissistic underwear model, who she has seen on T.V commercials. She takes him home and kills him. His body falls next to the book Anna was reading: Vladimir Nabokovs novel "Lolita".

Although Anna is still going out with Victor, she rebuffs his romantic advances. Dressed in drag, Anna enters a gay bar where she picks up Roman Mendoza, a gay man she takes home. After dancing a waltz, Anna proposes to have sex. He is initially hesitant. He thought she was a lesbian. He had never had sex with a woman before, but he is game. While Anna was out, David entered her house after befriending the German Shepherd that guarded her property. Hiding behind curtains, David witnesses Anna seducing the young gay man and sees her stabs him in the heart.

Broken glass inside her house alert Anna that there is an intruder. When she looks outside she sees David playing with her dogs behind the mansion fence. Anna, pretending to be a maid, befriends David and invites him in. When she is going to start her pre-murder routine, David shows her the surgical scalpel used by Anna to kill her victim. She has been hiding it inside a music box. David does not want to denounce her to the police, so he begins to black mail her. Through her monetary transaction David begins to arouse Annas personal interest. David buys a motorcycle with the money, but the members of his former gang, who believed he has stolen a bounty from them, pursue him, and leave him badly beaten. David is taken to the hospital where Anna works. Since David was a violent criminal in the past Victor wants to experiment on him the electro-shock therapy to turn killers like David into “useful citizens”. Anna is moved when she finds out that David is now her patient. She is not going to allow Victor to use David for his experiments. At night on New Years Eve, Anna reads a poem by Edgar Allan Poe to David. When Victor arrives he finds Anna covered in Davids blood after she killed him. In another room, the criminal patients in Victors experiment go berserk, savagely killing each other.

==Cast==
*Sue Lyon  as Ana Vernia
*Christopher Mitchum as David
*Jean Sorel as Dr. Victor Sender
*Ramón Pons as Toni
*Charly Bravo as Bruno
*Alfredo Alba as Román Mendoza
*Antonio del Real as Mick
*David Carpenter as Phil
*Ramón Tejela as Nicola

==Alternative titles==
The original title of the film Una gota de sangre para morir amando translates as A Drop of Blood to die loving . The English-language title used for the film was "Murder in a Blue World". It was released in the United States as Clockwork terror.

==DVD==
The film has been released on DVD in the United Kingdom by Pagan Films. It is presented in its slightly shorter, 98 minutes-long British cut. That is still 10 more minutes than the U.S. release, dubbed Clockwork Terror. The original Spanish version is 100 minutes long. There is no region 1 DVD available.

==Notes==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 