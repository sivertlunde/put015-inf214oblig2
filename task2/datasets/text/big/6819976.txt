The Adventures of Huck Finn (1993 film)
{{Infobox film
| name           = The Adventures of Huck Finn
| image          = Adventures of huck finn.jpg
| caption        = Theatrical release poster
| director       = Stephen Sommers Steve White
| based on       =  
| screenplay     = Stephen Sommers
| starring       = Elijah Wood Courtney B. Vance Jason Robards Robbie Coltrane
| music          = Bill Conti
| cinematography = Janusz Kamiński
| editing        = Bob Ducsay
| studio         = Walt Disney Pictures Buena Vista Pictures
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = $6.5 million
| gross          = $24,103,594
}} Huckleberry Finn and an escaped slave named Jim, who travel the Mississippi River together and overcome various obstacles along the way.
 MPAA for some mild violence and language.

==Plot==
Huckleberry Finn (Elijah Wood), the half-literate son of a drunk (Ron Perlman), runs away from home and follows the Mississippi River with an escaped slave named Jim (Courtney B. Vance). Along the way, the duo encounter adventures with colorful characters like The Duke (Robbie Coltrane) and the King (Jason Robards), two con men who impersonate British visitors in order to swindle three sisters (Anne Heche, Renee OConnor, Laura Bell Bundy) out of their fortune. Jim also re-educates Huck away from the racist views that he has grown up with.

==Cast== Huckleberry "Huck" Finn
* Courtney B. Vance as Jim
* Robbie Coltrane as The Duke
* Jason Robards as The King
* Ron Perlman as Pap Finn
* Dana Ivey as Widow Douglas
* Mary Louise Wilson as Miss Watson
* Anne Heche as Mary Jane Wilks
* James Gammon as Deputy Hines
* Paxton Whitehead as Harvey Wilks
* Tom Aldredge as Dr. Robinson
* Renée OConnor as Julia Wilks Laura Bundy as Susan Wilks
* Curtis Armstrong as Country Jake
* Frances Conroy as Scrawny Shanty Woman Daniel Tamberelli as Ben Rodgers
* Garette Ratliff Henson as Billy Grangerford
* Stephen Sommers (cameo appearance|cameo) as Silhouetted man

Archie Moore, who played Jim in the 1960 version of the novel, appears in a cameo as a slave who warns Huck about the two feuding families, saying "lots of people are going to die today."

==Production==
The movie was filmed entirely in Natchez, Mississippi.

==Reception==
The Adventures of Huck Finn was a financial success, debuting at No.2 at the box office,  and grossing over $24 million against a $6.5 million budget.

The film received generally positive reviews from critics, and currently holds a 69% "fresh" rating at review aggregate Rotten Tomatoes. Noted critic Roger Ebert gave the film 3 out of 4 stars, writing "The story of Huck and Jim has been told in six or seven earlier movies, and now comes The Adventures of Huck Finn, a graceful and entertaining version by a young director named Stephen Sommers, who doesnt dwell on the films humane message, but doesnt avoid it, either." 

==See also==
 
* Tom and Huck - A 1995 Disney film, co-written and produced by Sommers, adapted from The Adventures of Tom Sawyer.
*List of films featuring slavery

==References==
 

== External links ==
 
*   
*  
*   at Rotten Tomatoes
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 