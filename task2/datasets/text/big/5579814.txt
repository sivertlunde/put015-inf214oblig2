Bad Company (1995 film)
{{Infobox film
| name           = Bad Company
| image          = Bad companyposter.jpg
| caption        = Promotional poster for Bad Company
| director       = Damian Harris
| producer       = Jeffrey Chernov Amedeo Ursini Ross Thomas Michael Murphy
| music          = Carter Burwell Frank Fitzpatrick (Music Supervisor)
| cinematography = Jack N. Green
| editing        = Stuart H. Pappé
| studio         = Touchstone Pictures Buena Vista Pictures
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $3,674,841
}} thriller film Ross Thomas.

The film stars Ellen Barkin and Laurence Fishburne as former CIA operatives engaging in a dubious romance while plotting to murder their boss, played by Frank Langella, and take over his firm, which specializes in blackmail and corporate espionage.

==Plot==

Former CIA covert operative Nelson Crowe (Laurence Fishburne) is hired by Vic Grimes (Frank Langella) for a position with his company nicknamed "The Toolshed," a firm which employs people with intelligence service backgrounds in order to sell their talents, with regard to extortion and corporate espionage, to domestic and foreign corporations. Grimes second in command, Margaret Wells (Ellen Barkin), seduces Crowe and entices him with a plot to kill Grimes so they can take over the firm.

The Toolsheds top client, Curl Industries, is being sued in a class action lawsuit in a case currently on appeal at the Washington state Supreme Court. Curl Industries is accused of poisoning the water supply to a small town, resulting in the birth of disabled children. Grimes gives Crowe $1 million to bribe one of the justices, Justin Beach (David Ogden Stiers), into swinging the verdict in favor of Curl Industries.
 mole for the CIA, albeit against his will. Crowe was dismissed from the agency on suspicion of stealing a $50,000 bribe meant for an Iraqi colonel.
 Michael Murphy), is threatening prison time for the disappearance of the bribe as leverage to get Crowe to infiltrate the Toolshed. The CIA intends to acquire the firm and use it as a black operations hub with Smitty in charge. During the meeting, as he turns over the $1 million bribe money for inspection, Crowe secretly records his conversation with Smitty, who also forces him to sign a receipt. Stapp later discovers Crowes secret objective and extorts a payoff from Smitty to remain silent about it.

Beach accepts the $1 million bribe delivered by Crowe. He and his mistress Julie Ames (Gia Carides) sign a receipt to ensure Beachs cooperation. Beach buys tickets for a vacation to the Caribbean and sends Julie ahead with the money, telling her he intends to leave his wife and join her. However, after reneging on his agreement and voting against Curl Industries, Beach commits suicide.

Despite the setback caused by Beachs death and his vote, Wells and Crowe continue with their plan to murder Grimes. Wells spends a romantic weekend with Grimes at his fishing cabin, where Crowe sneaks in and shoots Grimes, then beats Wells to make it appear like the murder was a robbery gone wrong.

Wells and Crowe then take over the Toolshed, though Wells now rebuffs Crowes affections towards her, having used him to get what she wanted. Upon hearing of her lovers death, Julie travels to Europe, sending Goodwin postcards telling him how shes enjoying spending the $1 million. Goodwin sells this information to Crowe, who in turn takes it to Wells.

Wells orders Crowe to find and kill Julie because of her knowledge of the bribe attempt. Smitty confronts Wells in her office at the Toolshed and informs her of the CIAs plan to take over and also of Crowes involvement in the agencys infiltration.

Julie buys a gun from Goodwin and goes to Crowes apartment to kill him in revenge for Beachs death, arriving shortly after Wells, who also came to kill Crowe. In a chaotic shootout, Julie blindly fires at both as Crowe and Wells shoot each other dead.

Julie somehow remains unharmed. As she meticulously picks up her shell casings, she finds Crowes briefcase containing incriminating evidence, including the tape of his conversations with Smitty and the receipt she and Beach signed. After burning the receipt, Julie mails the tape to the U.S. Attorney|U.S. Attorneys office to expose the corrupt dealings of both the CIA and the Toolshed. She then leaves town for good, alone.

==Cast==
* Ellen Barkin as Margaret Wells
* Laurence Fishburne as Nelson Crowe
* Frank Langella as Vic Grimes
* Michael Beach as Tod Stapp
* Gia Carides as Julie Ames
* David Ogden Stiers as Judge Justin Beach
* Daniel Hugh Kelly as Les Goodwin
* Spalding Gray as Walter Curl
* James Hong as Bobby Bridsong
* Tegan West as Al Michael Murphy as William "Smitty" Smithfield (uncredited)
* Michelle Beaudoin as Wanda

==Critical reception==
The film received mostly negative reviews from critics. As of December 30, 2010, the review aggregator Rotten Tomatoes reported that 27% of critics gave the film positive reviews, based on 11 reviews. 

==References==
 
 

==External links==
* 
*  
* 
* 

 
 
 
 
 
 
 
 