Free Land (film)
{{Infobox film
| name = Free Land 
| image =
| image_size =
| caption =
| director = Milo Harbich
| producer = 
| writer =  Kurt Hahne  (play)   Milo Harbich
| narrator = Fritz Wagner   Herbert Wilk   Hans Sternberg
| music = Werner Eisbrenner  
| cinematography = Otto Baecker 
| editing = Margarete Steinborn
| studio = DEFA
| distributor = Sovexport-Film 
| released = 18 October 1946 
| runtime = 94 minutes
| country = Germany   (Soviet sector) German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Fritz Wagner neorealist style Soviet authorities. 

==Cast==
* Ursula Voß as Frau Jeruscheit Fritz Wagner as Neubauer Jeruscheit
* Herbert Wilk as Bürgermeister Siebold
* Hans Sternberg as Altbauer Strunk
* Aribert Grimmer as Altbauer Melzig
* Peter Marx as Altbauer Schulzke Oskar Höcker as Neubauer Kubinski
* Elfie Dugal as Küchenmädchen
* Kurt Mikulski as Siedler
* Karl Platen
* Hans Ulrich 
* Albert Arid

== References ==
 

== Bibliography ==
* Feinstein, Joshua. The Triumph of the Ordinary: Depictions of Daily Life in the East German Cinema, 1949-1989. University of North Carolina Press, 2002.

== External links ==
*  

 

 
 
 
 
 
 
 
 