La Muerte de Mikel
{{Infobox film
| name           = La muerte de Mikel
| image          = La muerte de Mikel.jpg
| caption        = Theatrical release poster
| director       = Imanol Uribe
| producer       = José Cuxant for Aiete
| writer         = José Ángel Rebolledo Imanol Uribe
| starring       = Imanol Arias Monserrat Salvador Fama Amaia Lasa
| music          = Alberto Iglesias
| cinematography = Javier Aguirresarobe
| editing        = José Luis Peláez
| distributor    =
| released       =  
| runtime        = 90 minutes
| country        = Spain
| language       = Spanish
}}
 La Muerte de Mikel ( ) is a 1984 Spanish film directed by Imanol Uribe, starring Imanol Arias. The film tells in flash back, the story of a gay member of ETA who died under mysterious circumstances.

==Plot==
The film opens at Mikels funeral mass. A flashback spins out the circumstances of his life and death.

Mikel, a young pharmacist involved in Basque Nationalist politics is living in Lekeitio, a Basque coastal town. When his wife, Begoña, returns from a long trip abroad, he picks her up at the airport. Their marriage is unhappy; the relationship between the couple is tense. Begoña wants to resolve their problems, but Mikel is indifferent. In the encounter after the return, the couple visits Mikel’s domineering mother, Doña Maria Luisa, a widow who lives in the same small town. The relationship between mother and son is also fractured. Mikel and Begoña have an argument when she tells him that she has discussed their sexual problems with his mother. They are going to have supper to house of a pair of friends, Martín a doctor who has arrived at the town fleeing the Chilean dictatorship and Martin’s wife, Arantza. The calm of town is disrupted with the senseless death of two young people, who did not stop in a nocturnal control and were killed by the Civil Guard. Mikel attends a political meeting, and is offered a seat in the next local election for the Basque independent party to which he belongs. Mikel takes parts in local festivities. At dawn, he arrives drunk at home. Having sex with his wife, Mikel bites Begoñas clitoris during an alcohol-fuelled attempt at oral sex. This incident effectively marks the end of their marriage.

The next day Martín, takes cares of Begoñas wounds inflected in the attack and as a friend and mentor to Mikel, he recommends him a therapist in Bilbao. After his first session, Mikel joins an old friend in a bar and gets drunk.  Mikel wakes up the following morning, knowing that he has had sex with Fama, a female impersonator, whose show he has seen at the bar. Realizing what he has done, humiliated and confused, Mikel embarks on a suicidal drive down the wrong side of the motorway, but swerves aside in time to avoid a crash.

Begoña moves out and a new chapter begins in Mikel’s life. Uninvited, Fama comes to the town and gives Mikel a surprise visit in the pharmacy where he works. She offers him emotional support and recounting the story of her life, she encourages him to come to term with his sexuality. Mikel has a meeting with Begoña and is forgiven by her. He tells her that never before he has seen the future with promise like now. However, when his homosexual relationship becomes public, his political comrades reject him. Mikel’s proud mother is horrified with her son homosexual relationship. Mikel is arrested and questioned about ETA activities. His friend, Martin, has confessed that years ago both helped a wounded E.TA member to escape to France. Resisting with dignity the violent attempts of the police to get information out of him, Mikel comes out of prison enthusiastic about developing his relationship with Fama. The next morning his brother finds him dead in bed in his mothers home. His death is not explained, but cinematography points clearly to the mother, who has already stated that she will not accept the public humiliation of Mikels homosexual relationship. Mikel’s political comrades who rejected him in life appropriate his death as a forum for political protest.

==Cast==
* Imanol Arias as Mikel
* Monserrat Salvador as Doña Maria Luisa, Mikel’s mother
* Amaia Lasa as Begoña
* Fama (Fernando Telletxea) as Fama  
* Martín Adjermián as Martín
* Alicia Sánchez as Arantxa
* Xabier Elorriaga as Iñaki, Mikels brother

==Reception==
La muerte de Mikel was filmed in Lekeitio, a small town in the Bay of Biscay. Schwartz, The Great Spanish Films, p. 63  Some scenes, like those that take place in the gay-club with Fama, were  shot in Bilbao. Schwartz, The Great Spanish Films, p. 64 

La muerte de Mikel did very well at the box office. It was the most successful film in a relative boom resulting from increased Basque government subsidies between 1982 and 1987. Evans Schwartz, The Great Spanish Films, p. 101 
It was also among the top-grossing Spanish films of 1984 and precipitated interest in regional Spanish Cinema. Schwartz, The Great Spanish Films, p. 65  Uribe became the best known Basque filmmakers of the 1980s with this, his strongest film up to that point. 
 Kiss of the Spider Woman (1985), La muerte de Mikel juxtaposes revolutionary politics with homosexuality to examine ideological suppression of difference. Evans, The Great Spanish Films, p. 101 

==Notes==
 

==References==
*Schwartz, Ronald, The Great Spanish Films: 1950 - 1990, Scarecrow Press, London, 1991, ISBN 0-8108-2488-4
* Stone, Robe, Spanish Cinema, Pearson Education, 2002, ISBN 0-582-43715-6
* Evans, Jo,  Imanol Uribes La muerte de Mikel: Policing the Gaze/Mind the Gap, 109 Bulletin of Hispanic Studies, Volume 76, Number 1, 1 January 1999.

==External links==
*  

 
 
 
 
 
 