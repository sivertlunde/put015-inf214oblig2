The Show-Off (1934 film)
{{Infobox film
| name           = The Show-Off
| image          = File:The Show Off 1934.JPG
| image_size     = 
| caption        = Lobby card
| director       =  George Kelly (play) Herman J. Mankiewicz (screenplay)
| narrator       = 
| starring       = Spencer Tracy
| music          = 
| cinematography =  William S. Gray
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       = 1934
| runtime        = 
| country        = United States
| language       = English
| budget         = $162,000   . 
| gross          = $397,000  
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
The Show-Off is a 1934 film. It was the first movie Spencer Tracy made for Metro-Goldwyn-Mayer.
 George Kelly, it made a profit of $78,000. James Curtis, Spencer Tracy: A Biography, Alfred Knopf, 2011 p231 

==Plot==
Out sailing one day, J. Aubrey Piper saves a man from drowning. He overhears an impressed Amy Fishers remark and looks her up in New Jersey, irritating her family with his constant bragging but winning Amy, who marries him.

A humble railroad clerk, Aubrey keeps pretending to be a more important man. He spends lavishly, piling up so much debt that he and Amy must move in with her parents. He gets fired by his boss Preston for making a wild offer on a piece of land, overstepping his authority by far.

Amy is fed up and intends to leave him. Aubrey runs into her brother Joe, an inventor whose rust-prevention idea has received a firm offer of $5,000. Aubrey goes to the firm and demands Joe get $100,000 plus a 50% ownership interest. The company rescinds its offer entirely.

Everybodys fed up with Aubrey, but suddenly Joe rushes home to say the companys changed its mind, offering him $50,000 plus 20%. And the railroad property paid off, too, so Aubreys offered his old job back, with a raise. He knows how lucky hes been and that he should just shut up, but he just cant.

==Cast==
* Spencer Tracy as Aubrey
* Madge Evans as Amy
* Claude Gillingwater as Preston
* Henry Wadsworth as Joe

==References==
 

==External links==
*  at IMDB
*  at TCMDB

 
 
 
 
 
 
 


 