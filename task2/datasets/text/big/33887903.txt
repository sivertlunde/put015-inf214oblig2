Food and Magic
{{Infobox film
| name           = Food and Magic
| image          =
| caption        =
| director       =
| producer       = US War Department
| writer         =
| narrator       =
| starring       = Jack Carson
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 10 minutes
| country        = United States
| language       = English
| budget         =
}}
 short documentary film commissioned by the United States Government during World War II. Food and Magic, was produced by the War Activities Committee of The Motion Picture Industry and it deals with food conservation and healthy eating. It stars Jack Carson as a sideshow barker who informs the crowd about proper wartime food consumption, including conservation and rationing. {{cite book
  | last = William H.
  | first = Young
  |author2=Nancy K. Young
  | title = World War II and the Postwar Years in America: A Historical and Cultural Encyclopedia, Tom 1
  | publisher = ABC-CLIO
  | year = 2010
  | doi =
  | page = 338
  | isbn = 0-313-35652-1 |id=ISBN 9780313356520
}} 

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 


 