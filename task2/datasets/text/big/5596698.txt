Oh, What a Lovely Tea Party
{{Infobox film 
| name=Oh, What a Lovely Tea Party 
| image= 
| caption = 
| writer=  Jason Lee
| director=Jennifer Schwalbach Smith 
| cinematography=Malcolm Ingram
| music=  
| distributor= View Askew Productions
| released=2004 
| runtime= 
| language=English 
| movie_series=
| awards=
| producer= 
| budget= 
}}

Oh, What a Lovely Tea Party is a 2004 documentary about the making of Jay and Silent Bob Strike Back, released and produced by Kevin Smiths View Askew Productions.  The 3-hour work marks the directorial debut of Jennifer Schwalbach Smith, Kevin Smiths wife. 

It was originally intended to be a bonus feature on the Jay and Silent Bob Strike Back DVD, but due to its length it became a standalone feature.  Plans to include it on the Clerks| Clerks X DVD were scrapped for similar reasons.  It has since been screened at several of Kevin Smiths "Vulgarthon" film festivals. According to a Q&A session with Smith in Vancouver in early 2009, it would be included in full on a future Blu-ray Disc release of Clerks,  and it was on the November 2009 release.

==References==
 

==External links==
* 
*  

 
 
 
 


 