The Soul of a Man
 
 
{{Infobox film
| name =Soul of a Man
| image = The Soul of a Man - DVD cover.jpg
| caption =
| writer = Wim Wenders
| starring =
| director =Wim Wenders
| producer = Martin Scorsese
| music = Skip James Blind Willie Johnson J. B. Lenoir
| cinematography = Liza Rinzler
| editing = Mathilde Bonnefoy PBS
| released = 2003
| runtime = 103 minutes.
| country = United States Germany
| language = English
| movie_series_label=
| movie_series =
| awards =
| budget =
}}
 The Blues, blues musicians Skip James, Blind Willie Johnson and J. B. Lenoir. 

The film is narrated by Laurence Fishburne in character as Blind Willie Johnson, and features performances by Nick Cave and the Bad Seeds, Beck, Jon Spencer Blues Explosion, James Blood Ulmer, T-Bone Burnett, Eagle Eye Cherry, Shemekia Copeland, Garland Jeffreys, Alvin Youngblood Hart, Los Lobos, Bonnie Raitt, Lou Reed, Marc Ribot, Lucinda Williams, and Cassandra Wilson. The film won an Emmy Award for Outstanding Cinematography for Nonfiction Programming, and the Audience Award at the São Paulo International Film Festival. It was also screened out of competition at the 2003 Cannes Film Festival.   

==References==
 

==External links==
* 
* 
* 
*  (QuickTime, 15 megabyte|MB) Blind Willie Johnson (played by Chris Thomas King) performs "Trouble (Will) Soon Be Over"
* Bransford, Steve.   Southern Spaces, November 3, 2008, http://southernspaces.org/2008/gold-records-deep-space.

 

 
 
 
 
 


 