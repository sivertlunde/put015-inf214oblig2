Nandanam (film)
{{Infobox Film
| name           = Nandanam
| image          = Nandanam.jpg Ranjith
| Siddique Ranjith Ranjith
| Ranjith
| Siddique  Innocent   Aravindar  Jagathy Sreekumar N. F. Varghese
| music          = Raveendran Rajamani ( background score )
| cinematography = Azhagappan
| editing        = Ranjan Abraham 
| studio         = Bhavana Cinema Kokers Release
| released       =   
| runtime        = 145 minutes
| country        = India
| language       = Malayalam
}}
Nandanam ( ) is a 2002 Malayalam film co-produced, written, and directed by Ranjith (director)|Ranjith, starring Navya Nair and Prithviraj Sukumaran along with Kaviyoor Ponnamma, Revathi, and Aravindar. Singer K. J. Yesudas made a cameo appearance. The music was composed by Raveendran.

==Synopsis==
An orphaned girl and housemaid, Balamani (Navya Nair) is an ardent devotee of Lord Krishna. Balamani takes care of Unni Amma (Kaviyoor Ponnamma) who lives in the ancestral house. Unni Amma treats Balamani like her daughter. Unni Ammas grandson Manu (Prithviraj Sukumaran|Prithviraj) comes from Bangalore to stay with his grandmother for a few days before he leaves for the US. Balamani was shocked to see Manu for first time, because she had seen him in her dreams before as her groom. They fall in love. Manus mother (Revathy), who was unaware of his sons affair, asks him to marry her friends daughter.

Manu discloses his wish to his mother, but in vain, as his mother had taken the marriage preparations to a point of no-return. His decision to marry the housemaid was not welcomed and Manu was forced to enter the marriage that was set for him. Disheartened Balamani prays to Guruvayurappan. Unnikrishnan (Aravindar), who was her neighbor boy, consoles her and tells  about a way to enable her marriage with Manu. In the events that follow, Balamani unites with Manu and they get happily married in Guruvayur Temple in front of Lord Guruvayurappan. Later she realizes that it was The Lord Himself (Aravindar) who came to console her as the neighbor lad, Unnikrishnan(Sudheesh).

==Cast==
*Navya Nair as Balamani
*Prithviraj Sukumaran as Manu
*Kaviyoor Ponnamma as Unni Amma
*Revathi as Thankam Siddique as Balan Innocent as Kesavan Nair
*Aravindar as Guruvayurappan
*Jagathy Sreekumar as Kumbidi alias Palarivattom Sasi
*N. F. Varghese as Sreedharan
*Sudheesh as Unnikrishnan
*Kalaranjini as Unnikrishnans Mother (Janu Edathi)
*V. K. Sreeraman Augustine
*Kalabhavan Mani
*Mala Aravindan as Sankaran Mushari
*Jagadeesh
*Vijayakumari as Parootty Amma
* Subbalakshmi
*Zeenath as Unni Ammas relative
*Jyothirmayi
*Ambika Mohan as Unni Ammas relative
*Gayathri
*K. J. Yesudas (cameo appearance)

==Soundtrack==
{{Infobox album 
| Name        = Nandanam
| Type        = Soundtrack
| Artist      = Raveendran
| Cover       =
| Background  = 
| Recorded    = 
| Released    = 2002
| Genre       = Film 
| Length      =
| Label       = Satyam Audios Ranjith
| Reviews     = 
|}}

The most clitically acclaimed and popular songs of this film were written by Gireesh Puthenchery and composed by maestro Raveendran. Kerala State Film Award for Best Female Playback Singer.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Raga
|-
| 1
| Sreelavasantham
| K. J. Yesudas|Dr. K. J. Yesudas
| Yamuna Kalyani
|-
| 2
| Manassil
| M. G. Sreekumar, Radhika Thilak Naatta
|-
| 3
| Aarum Sujatha
| Shuddha Dhanyasi
|-
| 4
| Mouliyil
| K. S. Chithra
| Mohanam
|-
| 5
| Gopike
| Dr. K. J. Yesudas Megh
|-
| 6
| Karmukil
| K. S. Chithra
| Harikambhoji
|-
| 7
| Aarum
| Sujatha
| Shuddha Dhanyasi, Abhogi
|-
| 8
| Manassil
| M. G. Sreekumar Naatta
|}

==Remakes==
Ranjith also worked on a Tamil remake of the film, which was shelved after some pre-production activities,  and the movie was remade as Seedan by director Subramaniya Shiva. It was also remade in Telugu as Maa Baapu Bommaki Pellanta by Raviraja Pinisetty and in Kannada as Nanda Gokula.

==References==
 

== External links ==
*  

 
 
 
 
 
 