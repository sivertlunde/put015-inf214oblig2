Shining Victory
 
{{Infobox film
| name           = Shining Victory
| director       = Irving Rapper
| image          = Shing-victory-1941.jpg
| image size     =
| caption        = Movie poster
| producer       = Hal Willis Howard Koch
| starring       = James Stephenson Geraldine Fitzgerald Donald Crisp Barbara ONeil
| music          = Max Steiner
| cinematography = James Wong Howe
| editing        = Warren Low
| distributor    = Warner Brothers
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
}} 1941 film based on the play, Jupiter Laughs, by A. J. Cronin.  It stars James Stephenson, Geraldine Fitzgerald, Donald Crisp, and Barbara ONeil, and it was the first film directed by Irving Rapper. Bette Davis makes a brief cameo appearance as a nurse in the film.

==Plot summary== sanitarium where he can continue his research on dementia precox, a disease from which his father had suffered. 

Mary Murray, a young, pretty doctor, becomes his laboratory assistant. They fall in love, though she has plans to go to China to engage in medical missionary work in a years time.  Paul convinces her to remain with him, and the two become betrothed. A fire breaks out in the lab and Mary tragically dies in an effort to salvage Pauls valuable records. The deeply distressed doctor turns down several posts at prestigious universities in order to realize Marys dream of helping the sick in China.
 Winged Victory was filmed in 1944.

==Cast==
*James Stephenson as Dr. Paul Venner
*Geraldine Fitzgerald as Dr. Mary Murray
*Donald Crisp as Dr. Drewett
*Barbara ONeil as Miss Leeming
*Montagu Love as Dr. Blake
*Sig Ruman as Professor Herman Von Reiter
*George Huntley, Jr. as Dr. Thornton
*Richard Ainley as Dr. Hale
*Bruce Lester as Dr. Bentley
*Leonard Mudie as Mr. Foster
*Doris Lloyd as Mrs. Foster
*Frank Reicher as Dr. Esterhazy
*Hermine Sterler as Miss Hoffman 
*Billy Bevan as Chivers
*Clare Verdera as Miss Dennis 
*Crauford Kent as Dr. Corliss
*Alec Craig as the jeweler
*Louise Brien as a nurse 
*Bette Davis as a nurse

==See also==
* List of American films of 1941

== External links ==
 
*  (trailer)
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 