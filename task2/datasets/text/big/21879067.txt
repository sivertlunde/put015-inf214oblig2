Cho Kamen Rider Den-O & Decade Neo Generations: The Onigashima Warship
{{Infobox film 
| italic title = force
| name           = Cho Kamen Rider Den-O &amp; Decade Neo Generations: The Onigashima Warship
| image          = Cho Den-O & Decade.jpg
| film name = {{Film name| kanji = 劇場版 超・仮面ライダー電王＆ディケイド ＮＥＯ ジェネレーションズ 鬼ヶ島の戦艦
| romaji = Gekijōban Chō-Kamen Raidā Denō ando Dikeido Neo Jenerēshonzu Onigashima no Senkan}}
| alt            = 
| caption        = 
| director       = Ryuta Tasaki
| writer         = Yasuko Kobayashi Toei 
| starring       = Dori Sakurada Takuya Mizoguchi Ruka Sawaki Akina Minami Masahiro Inoue Kimito Totani
| music          = Toshihiko Sahashi Shuhei Naruse Toei Co. Ltd 
| released       =  
| runtime        = 80 minutes
| language       = Japanese
| country        = Japan
}}
  is the fourth film adaptation of the popular  ,  , and  . The Onigashima Warship is the first of the films to be part of the   at #1.  Like its previous film  , its plot focuses more on Cho Den-O, although the characters of Decade give support to them.

==Plot==
  Tsukasa Kadoya World of Kotaro Nogami out with the true threat to their world. 

Meanwhile, in the year 1980, a boy named Yu encounters Mimihiko and then Yu is possessed by Deneb who gets him away from the Gelnewts before New Den-O arrives to cover his escape. Deneb then brings Yu to Ryotaro, revealing Mimihiko to be one of the Oni of legend and is searching for the   which Yu possesses. Seeing Yu reluctant to give them the item, Ryotaro leaves him with a DenLiner admission ticket should he change his mind. The next day, Yu runs off after learns that his father wants him to leave with him. Learning he is hunted by the Gelnewts, Yu tries to outrun the monsters on his bike before managing to reach the sands of time where the DenLiner is waiting for him. Giving them the Trump Stone, Yu wishes to be remain on the train. Though Owner warns him their intent to go back in time to fight the Oni, Yu refuses to turn back. After Yu is informed on the effects the distortions had on Ryotaro and Yuto, the former regressed to a child while the latter has mysteriously disappeared, the DenLiner arrives at the Junction Point where they meet the Station Master who reveals a 1936 newspaper revealing that Urataros, Kintaros, and Ryutaros are making a living as rice thieves in the bodies of another trio named Jiro, Ramon, and Riki (the Arms Monsters of Kamen Rider Kiva). During that time, the kids find Tsukasa who offers his aid. Kotaro is reluctant to accept his help until Tsukasa reveals the scroll Sieg brought with him that has Momotaros on it. 

Arriving in the Muromachi period, DenLiner gang arrive and defeat the Gelnewts, meeting Toki and joining her at the villages pleas. Once at Onigashima, New Den-O and the villagers catch the Gelnewts off guard as the kids find Momotaros. Deneb asks Yu to form a contract with him, at least until Yuto returns, to be able to assist the others in. Den-O and Deneb join the fray as Kuchihiko arrives and assumes his Rider form Goludora. During the fight, Toki takes a hit meant for Yu before New Den-O is defeated. Using Kotaro as a bargaining chip, Goludora gives them a day to accept his demands for both the Trump Stone and the DenLiner. The next day, after planning it out, the DenLiner gang gives up both the Trump Stone and the DenLiner. Once the Owner and Naomi are evicted, Kuchihiko ditches Kotaro as he enters the DenLiner to reach his brother in the present time period. However, his journey is staged as the evicted "Owner" is the Station Master in disguise as the real Owner pulled the emergency break as everyone else has erected a massive set designed to look like modern Tokyo. Once the DenLiner staff and the Station Master enter the train, with Kohana guarding the door, Den-O, the Tarōs, and New Den-O battle Goludora and the Gelnewts with Toki providing backup, until she is wounded in battle.

However, Diend appears long enough to summon Kamen Riders Ouja, G3 and Caucasus as wild cards in the fight and Kuchihiko reassembles the Onis Trump Stone with Mimihiko activating the Demons Warship in the present and using it to return to the past. The Oni Brothers are able to defeat them until Tsukasa and Sieg arrive, allowing Den-O to assume Wing form as Momotaros possess Tsukasa to fight as Decade. The other Tarōs do the same with Diends summoned Riders (Urataros with G3, Kintaros with Caucasus and Ryutaros with Ouja) while Deneb enters Kotaro, enabling Kamen Rider New Den-O to assume Vega Form. Helping Toki fire her arrow, Yu manages to break the Demons Warship’s anchor to give the Kamen Riders time to set up a way to end the fight. At Decades suggestion, Ryotaro forms Kamen Rider Den-O Super Climax Form with the Tarōs and Sieg. Cho-Den-O and Decade manage to overwhelm the Oni Brothers with New Den-O Vega Form and Teddys aid. After Shilubara sacrifices himself to protect his brother, an enraged Goludora enters the Warship and engages the DenLiner in a battle with Decade taking his leave as the DenLiner gang manages to sink the Warship from the inside out as Kamen Rider Den-O Sword Form finishes Goludora off with a new Rider Kick attack. Soon after, Yu bids farewell to Toki as she is revealed to be his ancestor. Though offered a slight detour before they return to present, Yu turns it down as he wants to be back in his time. After they part ways, Deneb is dropped back in 2009 where he finds Yuto waiting for him, where he reveals that he went by the name Yu as a child and they return to the ZeroLiner.

==Pre-production and casting==
 Yuichi Nakamura, and Kenjirō Ishimaru).  Also to be featured in the film are several cast members of the current Kamen Rider Series Kamen Rider Decade and some of the cast members of Kamen Rider Kiva. 

==Characters==
  Yuto Sakurai as a child.
; : Toki is the protector of the Trump Stone during the Muromachi period, protecting from it the Oni Brothers with her master archery skills. She is the ancestor of Yus mother.
; : Chiyoko is Yus maternal grandmother and his legal guardian.
; : Yus classmates who make fun of him because he is from Tokyo, and doesnt like mud and bugs.

===Oni Clan===
The movies antagonists are the two brothers of the   based on Onigashima, who both use the   Mirror Monsters as their foot soldiers when they came into their world from the World of Ryuki. They travel through time using the colossal legendary   It is several hundred times larger than the Kamen Rider Den-O (character)#DenLiner|DenLiner, and is armed with torpedoes and harpoons. It is protected by a special barrier so no unwanted visitors can reach the deck. Because of their machinations in the past, they are behind the radical changes occurring to the World of Den-O. Furthermore, the distortions enable them to assume Kamen Rider-like forms.

====Goludora====
The older Oni Brother  , armed with  , which also enables his transformation into the golden grasshopper-like  . He commands Mimihiko to travel back in time to steal items to further his agenda to control all of time and space by rewriting the legend of Onigashima. But in the end, his plans are ruined by the group effort of the Kamen Riders as he is destroyed by Kamen Rider Den-O as the battleship sinks.

====Shilubara====
The younger Oni Brother   is armed with the  , which also enables his transformation into the silvery dragonfly-like  . He briefly appears in Kamen Rider Decade in the World of Den-O to acquire the vase for his brothers agenda, getting it while Decade and Den-O are distracted with their fight as New Den-O arrives to stop Shilubara before he took the item back in time. He later travels to 1980 to steal the Trump Stone, but is forced to wait for his brother to acquire it in their time. Once the Trump is complete, Mimihiko pilots the Demons Warship back into the past where he sacrfices himself to save his brother from Cho-Den-Os attack.

==Cast==
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  ,  ,  

===Voice actors===
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  ,  
* :  

==Songs==
;Theme song:
*  
**Lyrics: Shoko Fujibayashi
**Composition & Arrangement: Shuhei Naruse
**Artist:  : Momotaros (Toshihiko Seki),    Urataros (Kōji Yusa),  Kintaros (Masaki Terasoma),    Ryutaros (Kenichi Suzumura),  Teddy (Daisuke Ono),    the Owner (Kenjirō Ishimaru),  Kotaro Nogami (Dori Sakurada),  Naomi (Rina Akiyama),  and Kohana (Tamaki Matsumoto) 

==References==
 

==External links==
*  - The official website for The Onigashima Warship  

 
 

 
 
 
 