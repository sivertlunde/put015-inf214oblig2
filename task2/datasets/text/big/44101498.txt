Aazhi
{{Infobox film 
| name           = Aazhi
| image          =
| caption        =
| director       = Boban Kunchacko
| producer       = Boban Kunchacko
| writer         = E Mosus
| screenplay     = E Mosus Baby Shalini Chithra
| music          = Raj Kamal
| cinematography = U Rajagopal
| editing        = SPS Veerappan
| studio         = Udaya
| distributor    = Udaya
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, Baby Shalini Chithra in lead roles. The film had musical score by Raj Kamal.   

==Cast==
 
*Thilakan
*Captain Raju Baby Shalini Chithra
*G. GK Pillai
*K. R. Vijaya
*Kaduvakulam Antony
*Kalpana Ayyer
*MG Soman
*Ramu
*Shanavas
*Raveendran
 

==Soundtrack==
The music was composed by Raj Kamal and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Akale Polum || K. J. Yesudas || Bichu Thirumala || 
|-
| 2 || Alliyilam Poo || K. J. Yesudas, S Janaki || Bichu Thirumala || 
|-
| 3 || Ezhu Paalam Kadannu || S Janaki, KP Brahmanandan || Bichu Thirumala || 
|-
| 4 || Hayya Manassoru Shayya || Vani Jairam || Bichu Thirumala || 
|-
| 5 || Kalyaanimulle || P Susheela || Bichu Thirumala || 
|-
| 6 || Manuja Janmam || K. J. Yesudas || Bichu Thirumala || 
|-
| 7 || Ulakudayon || Chorus, CO Anto, Thomas Williams || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 