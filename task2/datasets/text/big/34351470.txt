Chasing Through Europe
Chasing David Butler silent and part-talkie versions. Most of the film was shot on location in Europe.

==Synopsis==
Dick Stallings, a newsreel reporter in London, falls in love with Linda Terry a wealthy American woman. Together they travel round Europe interviewing leading politicians and celebrities, while being pursued by a gangster who plans to kidnap Linda. 

==Cast==
* Sue Carol - Linda Terry 
* Nick Stuart - Dick Stallings 
* Gustav von Seyffertitz - Phineas Merrill  Gavin Gordon - Don Merrill 
* E. Alyn Warren - Louise Herriot

==References==
 

==Bibliography==
* Solomon, Aubrey. The Fox Film Corporation, 1915-1935. A History and Filmography. McFarland & Co, 2011.

==External links==
* 

 
 

 
 
 
 
 
 


 