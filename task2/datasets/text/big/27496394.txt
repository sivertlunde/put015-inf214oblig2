Man from Tangier
 
 
{{Infobox film
| name           = Man from Tangier
| image          = "Man_from_Tangier"_(1957).jpg
| image_size     = 
| caption        = U.S. 1-sheet poster
| director       = Lance Comfort
| producer       = W.G. Chalmers
| writer         = Paddy Manning OBrine  Robert Hutton Martin Benson   Derek Sydney
| music          = Wilfred Burns
| cinematography = Geoffrey Faithfull Peter Mayhew
| studio         = Butchers Film Service
| distributor    = Butchers Film Service   
| released       =  
| runtime        = 67 mins   Linked 2014-04-30 
| country        = United Kingdom
| language       = English
}}
 1957 Cinema British crime Robert Hutton, Martin Benson.

A criminal flees from Tangier to London with forged money plates, leading to the gang he works for sending a dangerous woman to pursue him.

==Main cast== Robert Hutton as Chuck Collins
* Lisa Gastoni as Michele Martin Benson as Voss
* Derek Sydney as Darracq
* Leonard Sachs as Heinrich
* Emerton Court as Armstrong Robert Shaw as Johnny
* Robert Raglan as Inspector Meredith
* Harold Berens as Sammy Jack Allen as Rex Michael Balfour as Spade Murphy
* Frank Forsyth as Sergeant Irons
* Reginald Hearne as Walters
* Fred Lake as Hotel Porter
* Alex Gallier as Max
* Marianne Stone as Woman in Hotel
* Ronnie Clark as Coster

==Release==
Man in Tangier was cut by the British Board of Film Classification to 67 minutes running time, in order to achieve a "U" classification.  The film premiered at Odeon Marble Arch in London on 27 January 1957, where it ran as a double bill together with Monkey on My Back (film)|Monkey on My Back. 
 The Breaking Point. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 


 
 