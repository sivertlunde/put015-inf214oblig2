Nymph (film)
 
{{Infobox film
| name           = Nymph
| image          = 
| caption        = 
| director       = Pen-Ek Ratanaruang
| producer       = 
| writer         = 
| starring       = Jayanama Nopachai
| music          = 
| cinematography = Charnkit Chamnivikaipong
| editing        = Patamanadda Yukol
| distributor    = 
| released       =  
| runtime        = 109 minutes
| country        = Thailand
| language       = Thai
| budget         = 
}}

Nymph ( , Transliteration|translit.&nbsp;Nang mai) is a 2009 Thai drama film directed by Pen-Ek Ratanaruang. It competed in the Un Certain Regard section at the 2009 Cannes Film Festival.   

== Plot ==
May is a city woman who has everything she could ask for. Her long-time husband, Nop, showers love and attention on her. But fate or desire play tricks on the couple who watches their lives drift by without much thought or reflection, and May starts an affair with Korn, himself a married man. One day Nop, a professional photographer, is assigned to take a trip into the forest to film its wildlife. He decides to bring his wife along. But the journey slowly reveals how the invisible weight of their urban lifestyle haunts them like a spectre. When her husband fails to return to the tent, May sets out to look for him and then Nop returns. But the forest has changed him into someone else. 
  
==Cast==
* Jayanama Nopachai as Nop
* Porntip Papanai as Nymph
* Wanida Termthanaporn as May
* Chamanun Wanwinwatsara as Korn

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 