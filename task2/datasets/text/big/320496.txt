Bad Santa
 
{{Infobox film
| name           = Bad Santa
| image          = Bad Santa film.jpg
| alt            = A scruffy dishevelled Santa Claus, standing beside a scowling man in a yellow suit, and a fat child and an Santa helper elf standing in front of them. 
| caption        = Theatrical release poster
| director       = Terry Zwigoff
| producer       = {{Plain list |
* John Cameron
* Sarah Aubrey
* Bob Weinstein
}}
| writer         = {{Plain list |
* Glenn Ficarra
* John Requa
}}
| starring       = {{Plain list |
* Billy Bob Thornton Tony Cox
* Lauren Graham Brett Kelly
* Lauren Tom
* John Ritter
* Bernie Mac 
}}
| music          = David Kitay
| cinematography = Jamie Anderson
| editing        = Robert Hoffman 
| studio         = Dimension Films
| distributor    = Buena Vista Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $23 million http://www.boxofficemojo.com/movies/?id=badsanta.htm 
| gross          = $76.5 million 
}} Christmas black crime film Tony Cox, Brett Kelly, Lauren Tom, and John Ritter in supporting roles. It was Ritters last film appearance before his death in 2003. The Coen brothers are credited as executive producers.

The film was screened out of competition at the 2004 Cannes Film Festival.   

An unrated version was released on DVD on March 5, 2004 and on Blu-ray Disc on November 20, 2007 as Bad(der) Santa. A directors cut DVD was released in November 2006; it features Zwigoffs cut of the film (including an audio commentary with him and the films editor), which is three minutes shorter than the theatrical cut and ten minutes shorter than the unrated version.

==Plot== Tony Cox) sex addict, and is gradually getting unable to perform his Santa duties with children, much to Marcus dismay. When they are hired at a mall in Phoenix, Arizona|Phoenix, the vulgar remarks made by Willie shock the prudish mall manager Bob Chipeska (John Ritter), who brings it to the attention of security chief Gin Slagel (Bernie Mac).
 Brett Kelly), Santa Claus fetish, and they begin a sexual relationship. Willie is harassed by a man in the bar, but Thurman intervenes. Willie gives Thurman a ride home, then enters the boys house where he lives with his senile grandmother (Cloris Leachman). Thurman reveals that his mother passed away, and his father is away "exploring mountains" (when he is actually in jail for embezzlement). Willie breaks into the house safe and steals a BMW owned by Thurmans father.

Bob informs Gin that he overheard Willie having sex with a woman in a mall dressing room; Gin starts to investigate. Willie goes to his motel room and sees it being raided, causing him to take advantage of Thurmans naivete and live in his house. Marcus berates Willie for taking advantage of Thurman, stating his disapproval of Willies sex addiction.

Gins investigation of Willie includes visiting Thurmans imprisoned father, revealing that Willie is staying there illegally. He confronts Willie at the mall, and takes him and Marcus to a bar. There, he reveals that he has figured out their plan, blackmailing them for half of the cut to keep silent.
  suicide by inhaling vehicle exhaust fumes. He gives Thurman a letter to give to the police, confessing all his misdeeds. Willie notices Thurmans black eye, which persuades him to make an example of the skateboarding bully. A renewed sense of purpose for Willie has him attempt to train Thurman in boxing.
 jump start for their vehicle from Gins. Lois hits Gin with the car, then Marcus kills him.

On Christmas Eve, when the heist is almost complete, Willie goes to get Thurman a pink stuffed elephant that he had wanted for Christmas. Just as he gets the elephant, Marcus reveals to Willie that he intends to kill him, fed up with his increasing carelessness. Lois tells him to hurry up and kill Willie so they can get away with the merchandise. The police swarm them, tipped off by the letter Willie gave to Thurman. When Marcus opens fire, the police shoot at him and Willie flees. Determined to give Thurman his present, he leads the police on a chase to Thurmans house, ignoring orders to freeze. He is repeatedly shot on Thurmans porch, but survives.
 flipping off the bully.

==Cast==
* Billy Bob Thornton as Willie T. Stokes Tony Cox as Marcus Brett Kelly as Thurman Merman
* Lauren Graham as Sue
* Lauren Tom as Lois
* Bernie Mac as Gin Slagel
* John Ritter as Bob Chipeska
* Octavia Spencer as Opal
* Cloris Leachman as Granny (uncredited)
* Alex Borstein as Milwaukee mom
* Billy Gardell as Milwaukee Security Guard
* Bryan Callen as Miami bartender
* Tom McGowan as Harrison
* Ajay Naidu as Hindustani Troublemaker
* Ethan Phillips as Roger Merman Matt Walsh as Herb (uncredited)
* Max Van Ville as Skateboard Bully
* Ryan Pinkston as shoplifter
* Curtis Taylor as Phoenix Security Guard
* Sheriff John Bunnell as Phoenix Police Chief
* Dylan Cash as Kid on Bike (uncredited)

==Production== Ghost World. WGA rules, they were uncredited.
 Lost in Translation, respectively. 
 Tony Cox as Marcus, and the Weinsteins filmed additional sequences with another director without Zwigoffs approval, in order to make the film more mainstream. 

===Music===
The film has been praised for its innovative use of classical music in scenes. The following pieces of music were used in the film: 
  Nocturne No. 2 in E-flat major, Op. 9 No.2 by Frédéric Chopin
* "Up on the House Top" by Benjamin Hanby, performed by Eddy Arnold; and by the Cherry Sisters
* "Jingle Bell Rock", performed by Bobby Sherman Charles Brown, performed by Sawyer Brown
* "Papa Loves Mambo" by Al Hoffman, Dick Manning, and Bickley Reichner, performed by Xavier Cugat and his orchestra Holly Jolly Christmas" by Johnny Marks, performed by Burl Ives
* "Let It Snow! Let It Snow! Let It Snow!" by  Sammy Cahn and Jule Styne, performed by Dean Martin
* "Deck the Halls" performed by Boots Randolph James Pierpont, performed by Ricky Nelson
* "It Came Upon the Midnight Clear" by Edmund Sears and Richard Storrs Willis, performed by The Symphonette Society
* "Its the Most Wonderful Time of the Year" by Edward Pola and George Wyle, performed by Andy Williams The Sleeping Berlin Radio Symphony Orchestra Jazz Suite No. 2 by Dmitri Shostakovich, performed by the Royal Concertgebouw Orchestra
* "Have Yourself a Merry Little Christmas" by Hugh Martin and Ralph Blane, performed by Bing Crosby
* "Winter Wonderland" by Felix Bernard and Richard B. Smith, performed by The Symphonette Society
* Overture to The Barber of Seville by Gioachino Rossini, performed by Zagreb Festival Orchestra, conducted by Michael Halász
* Anvil Chorus from Il trovatore by Giuseppe Verdi, performed by the Mormon Tabernacle Choir
* "Seasons Freaklings" by Bunnygrunt
* "Christmas (Baby Please Come Home)" by Ellie Greenwich, Jeff Barry, Phil Spector, performed by Swag Music Group featuring Tom Chappell Habanera from Carmen by Georges Bizet
* "Silent Night" by Franz Xaver Gruber and Joseph Mohr

==Reception== Miramax subsidiary. 

 
The film has an aggregate "Certified Fresh" rating of 77% at Rotten Tomatoes.  On Metacritic, the film has a score of 70 out of 100, based on reviews from 38 critics, indicating "generally favorable reviews". 

Roger Ebert of the Chicago Sun-Times gave the film 3½ stars out of four. 

  Lost in Translation.

===Box office===
 
The film grossed over $60 million domestically and more than $76 million in total worldwide. 

==Home media  ==
In the U.S, a theatrical version, an unrated version, a directors cut and a Blu-ray version (which includes unrated and directors cut) have all been released. According to dvdtown.com, the special features for the theatrical cut of the film included: a behind-the-scenes special, outtakes, and deleted scenes. The unrated edition was released June 22, 2004 and had all of the above plus a Badder Santa gag reel and over seven minutes of unseen footage. The directors cut was released October 10, 2006 and contained the new version of the film (as Zwigoff originally intended it). It also had a new commentary (in addition to the rest of the features: outtakes, deleted/alternate scenes, and the behind-the-scenes feature). The Blu-ray version released November 20, 2007 contained the unrated version and the directors cut of the movie. Among its special features were directors commentary, an interview with Zwigoff and editor Robert Hoffmann, along with other features ported over from the previous unrated versions release in addition to a showcase feature.

==Sequel==
On September 18, 2009, Billy Bob Thornton appeared on the NFL Network show NFL Total Access. He confirmed, after host Rich Eisen hinted, that there would be a sequel to Bad Santa, aimed for release by Christmas 2011.  In March 2011, Thornton and The Weinstein Company confirmed that negotiations had begun for a sequel.  A sequel had been scheduled for December 2013.  On May 30, 2013, it was revealed that Miramax has hired Entourage creator Doug Ellin to rewrite the script. 
On 25 November 2013, Billy Bob Thornton confirmed that the sequel Bad Santa 2 is expected to start production in early 2014 after script problems were resolved. It is due for release by 2016. 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 