Lamhe
 
 
{{Infobox film
| name           = Lamhe
| caption        = DVD cover
| image          = Lamhe-cover-small.jpg
| writer         = Honey Irani Rahi Masoom Raza
| cinematographer = Manmohan Singh
| starring       = Sridevi Anil Kapoor Waheeda Rehman Anupam Kher
| director       = Yash Chopra
| producer       = Yash Chopra T. Subbarami Reddy
| distributor    = Yash Raj Films
| released       =  
| runtime        = 187 minutes
| language       = Hindi, English
| music          = Shiv-Hari
| awards         =
| budget         =
}}

Lamhe (  and starring Sridevi and Anil Kapoor. The story was written by Honey Irani. It was inspired from the 1986 film Anokha Rishta starring Rajesh Khanna.

Lamhe was shot in two schedules in Rajasthan and London. Over the years it has been hailed as a classic and is regarded as a modern masterpiece and possibly Yash Chopras finest film. Sridevi received acclaim for her double role, winning the Filmfare Award for Best Actress. Although a commercial failure in India, it became a success in the UK Despite its commercial failure, Lamhe went on to win several awards and features in Outlook magazines list of Bollywoods Best Films. It has been cited as Yash Chopras personal favourite of the movies he has done. This was one of the last films that the famous Dr. Rahi Masoom Reza scripted as a writer. He died a couple of months after its release.

On the occasion of the Centenary of Indian Cinema in 2013, Lamhe featured among the Top Ten Romantic Movies of 100 years.  

==Plot==
Virendra "Viren" Pratap Singh (Anil Kapoor) travels to Rajasthan with his governess, affectionately called Dai Jaa (Waheeda Rehman). He meets the beautiful Pallavi (Sridevi) and falls in love with her. However, she happens to be older than he is, though this does not bother Viren. During a property dispute and a court case, Pallavis father dies of a heart attack. She is shattered and goes into isolation. At the wake, Viren goes to console her, when she runs past Viren—towards Siddhharth, the man she loves. Viren is heartbroken but, to fulfil Pallavis fathers dying wish, he arranges the wedding between Siddhharth and Pallavi. Allowing Pallavi to live her life happily, Viren leaves for London.

One year into the marriage, Siddhharth and Pallavi are killed in an accident. It is revealed that she was pregnant at the time and gave birth to a daughter. The girl is named Pooja and is kept in the care of Dai Jaa. Viren has a childhood friend, Prem (Anupam Kher), who knows that Virens heart still pines for Pallavi. Over the course of time, and with much persuasion, slowly Viren hops back to reality by engaging himself in work in London. There, a girl named Anita (Dippy Sagoo), who works along with Viren, falls for him. She knows about Virens love for Pallavi and constantly tries to gain his attention.

Viren visits Rajasthan from time to time, for Pallavis death anniversary, and buys gifts for the young Pooja. He never meets her remembering that she was born on the day Pallavi died and had never spent any time with her, which makes her sad. A few years later Viren returns from London and meets the grown up Pooja for the first time. When he meets her he is shocked to see that she looks exactly like her mother.

Pooja and Dai Jaa come to London for vacation, and there Anita discovers that Pooja is the image of Pallavi. This upsets her as she is afraid that Viren will eventually fall for Pooja. Pooja develops feelings for Viren whom she lovingly calls "Kuwarji". Prem also likes Pooja and understands that Pooja loves Viren. Though he likes the alliance, he is sceptical since Viren is still living in the past.

Later in the movie, Anita realises that Viren is over Pallavi and has fallen for Pooja. There are scenes when Pooja and her coming face to face over the topic of Viren. She finally confronts Pooja with the utmost respectful way of saying "Hes mine." There, eventually she insults Pooja as a kid infatuated by Viren and asks what relation Pooja is to Viren that she acts authoritatively over him. Pooja retorts back saying that if she is not related to Viren, Anita is not related to him either. This frustrates Anita leading her to lambaste Viren about having feelings for a younger woman.

Pooja finds a sketch (which Viren has made of her mother) and misunderstands that it is a sketch of her. She dresses herself in old- fashioned clothes and when he comes home and asks him to put "sundoor" on him since he loves her. Furious, he reveals to her that he had loved her mother and not her. Pooja is heartbroken and returns to India with Dai Jaa. Viren decides to marry Anita since he feels she and has been waiting for him for a long time. Pooja tells that she will marry on the condition that Viren must marry before her. Pooja lies and tells Viren that she is already married however Pooja never marries for her love for Viren was too great. Viren understands that she is lying and lies to her and sends her a marriage invitation to make her believe that he is now married.

The resolution she took shows a heartbroken Pooja narrating a folk tale to an audience in a village show. After the show, Viren approaches a confused and wary Pooja. Viren reveals that he realised, after she left London, she had lied and tells her that he is not married either. He says that he loves Pooja and not her mother. She runs into his arms and they are together at last.

==Cast==
* Sridevi as Pallavi/Pooja
* Anil Kapoor as Virendra Pratap Singh (Viren)
* Waheeda Rehman as Dai Jaa
* Anupam Kher as Prem
* Dippy Sagoo as Anita Malhotra
* Manohar Singh as Kothiwale Thakur (Pallavis father)
* Lalit Tiwari as Sudheshwar Narayan Tiwari
* Deepak Malhotra as Siddharth Bhatnagar
* Ila Arun as Gypsy
* Richa Pallod as Young Pooja
* Vikas Anand as Doctor Vikas

==Reception==
Lamhe was hugely popular with metropolitan elite and the overseas market, but it had a poor box-office response with the Indian mass market because of its supposed incest theme.Lamhe faced tough Competition from Ajay Devgn Debut Phool Aur Kaante.   

==Music==
The music of Lamhe was composed by Shiv Kumar Sharma and Hariprasad Chaurasia, known together as Shiv-Hari and lyrics were provided by Anand Bakshi. The song "Kabhi Main Kahoon" was made from melody used as background music in Yash Chopras previous movie Chandni, also scored by Shiv-Hari. In the famous Parody Sequence, Waheeda Rehman danced to "Aaj Phir Jeene ki Tamanna Hai" – the superhit number from her own classic Guide (film)|Guide.

The songs included on the official soundtrack are listed here:
{| class="wikitable"
|-
!Song
!Singer(s)
|-
|"Yeh Lamhe Yeh Pal" Hariharan (singer)|Hariharan
|-
|"Yeh Lamhe Yeh Pal (Sad Version)" Hariharan
|-
|"Mhaare Rajasthan Ma" Moinuddin
|-
|"Mohe Chhedo Naa" Lata Mangeshkar
|- 
|"Chudiyan Khanak Gayee (contains excerpt of Mhaare Rajasthan Ma in the introduction)" Lata Mangeshkar, Moinuddin and Ila Arun
|-
|"Chudiyan Khanak Gayee (Sad Version)" Lata Mangeshkar
|-
|"Kabhi Main Kahoon" Lata Mangeshkar and Hariharan
|- 
|"Megha Re Megha" Lata Mangeshkar and Ila Arun
|- 
|"Yaad Nahin Bhool Gaya" Lata Mangeshkar and Suresh Wadkar
|-
|"Gudiya Rani" Lata Mangeshkar
|-
|"Meri Bindiya" Lata Mangeshkar
|- 
|"Freak Out (Parody Song)" Pamela Chopra and Sudesh Bhosle 
|-
|"Moments of Rage (Instrumental)" Instrumental
|-
|"Moments of Passion (Instrumental)" Instrumental
|}

==Critical acclaim and achievements==
The film was critically acclaimed, had exceptional music, and did brilliant business in UK. Unfortunately, it was not a box-office success in India. Lamhe is one of the few films that picked up the Filmfare Award for Best Movie despite its lacklustre business. Lamhe is also one of the biggest Bollywood hits in the overseas market and video circuit. Sridevi won much acclaim for her double-role as mother and daughter, winning the Filmfare Best Actress Award among others. The film finds a place in the Times Movie Guides Top 100 Indian films.  It is in the Outlook magazines list of All-Time Great Indian films.    
It has been cited as Yash Chopras personal favourite his films.    Times of India included it in its list of Top 10 Films of Yash Chopra describing it as a tale of "love transcending the boundaries of time and space"  while Rediff called it "Quite easily one of his most definitive films, Chopra surpassed his own findings of romance with the insightful, lovely Lamhe".   

Sridevi played both mother and daughter cast in what iDiva described as "another double role but it was unlike any played before".    Hailed by Rediff as "one of the most remarkable films of her career...often considered a film way ahead of its time",    Her performance brought her much acclaim with BizAsia stating that "Her rendition of both Pallavi and Pooja serves well in highlighting how versatile she is as an actress, playing contrasting characters in the same movie".  Speaking to Karan Johar about the Making of Lamhe, Yash Chopra revealed "When 90% of the London schedule was over, tragedy happened. Sridevis father died...She came back after 16 days and had to shoot a comedy scene...At that moment she forgot everything and gave a wonderful scene. I understood that is the secret of her success...Why she is what she is".  Sridevis folk dance number Morni Baga ma also became a rage and was placed among the Top 5 Songs of Yash Chopra by Hindustan Times.

Sridevi received universal critical acclaim. Many called it one of the best performances by a female in Indian cinema. Over the years Lamhe has become a cult classic.     Talking about the film, critic Rachel Dwyer wrote in her biography of the film-maker "Yash Chopras own favourite film, Lamhe (Moments, 1991), divided the audience on a class basis: it was hugely popular with metropolitan elites and the overseas market, which allowed it to break even, but it had a poor box-office response (largely lower class, especially the repeat audience) because of its supposed incest theme".  The Hindu reported that "With shades of incest, Lamhe caused more than a flutter and remained the talk of the town"  while Sridevi herself admitted in an interview with Rajeev Masand that she found the subject "too bold".  
Rediff described its failure as "one of those bizarre, unexplained moments of cinema", 

==Awards==
;1992 Filmfare Awards Best Film – Yash Chopra Best Actress – Sridevi Best Comedian – Anupam Kher Best Story – Honey Irani Best Dialogue – Dr. Rahi Masoom Reza Best Director – Yash Chopra Best Actor – Anil Kapoor Best Actress in a Supporting Role – Waheeda Rehman National Film Awards Best Costume Design – Neeta Lulla

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 