A Business Affair
 
{{Infobox film
| name           = A Business Affair
| image          = A Business Affair.jpg
| image size     =
| caption        =
| director       = Charlotte Brandstrom
| producer       = Willi Bär Martha Wansborough
| writer         = Charlotte Brandstrom, Lucy Flannery (dialogue) Barbara Skelton (novel), William Stadiem (screenplay)
| narrator       =
| starring       = Carole Bouquet, Christopher Walken and Jonathan Pryce
| music          = Didier Vasseur
| cinematography = Willy Kurant
| editing        = Laurence Méry-Clark
| distributor    =
| released       = France: 16 March 1994
| runtime        = 102 minutes
| country        = United Kingdom France English
| budget         =
| gross          =  $7,556 
| preceded by    =
| followed by    =
}}
 1994 romantic Big Ben with the two men beside her.

== Plot ==
The film is centred on the life of Kate Swallow and her susceptibility for falling in love with different men. At the beginning of the film she is in love with a famous writer named Alec Bolton, who dismisses any intentions she has of writing a novel herself as nonsense, strongly discouraging her. Later falls in love with a man named Vanni Corso who is the publisher of the firm for which Alec writes books, and she leaves Alec for Vanni. Kate later finds out that Vanni also doesnt think highly of her writing abilities yet he had strung her along. Gradually both men change their attitudes as they vainly struggle to win her affections.
 tango routine.

== Cast ==
* Carole Bouquet as  Kate Swallow
* Jonathan Pryce as Alec Bolton
* Christopher Walken as  Vanni Corso
* Sheila Hancock as  Judith
* Anna Manahan as  Bianca
* Fernando Guillén Cuervo as Ángel
* Tom Wilkinson as  Bob
* Marisa Benlloch as  Carmen
* Roger Brierley as Barrister
* Bhasker Patel as  Jaboul
* Jessica Hamilton as Vanni Corsos secretary
* Paul Bentall as  Drunken Man
* Allan Corduner as  Dinner Guest
* Marian McLoughlin as Dinner Guest
* Miguel De Ángel as  Spanish Taxi Driver
* Christopher Driscoll as  Policeman
* Beth Goddard as Student
* Fergus ODonnell as Student
* Richard Hampton as  Doctor
* Togo Igawa as a Japanese Golfer
* Rodolfo Recrio as himself

== Release ==
The film premiered in France on 16 March 1994, later screening in Germany on 5 May 1994 and Spain on 22 July 1994. It didnt premiere in the States until a year and a half later finally showing in New York City on 3 November 1995.

The UK version ran for 102 minutes but the US version has several scenes censored, particularly some of the scenes with nudity and ran for 98 minutes.

== References ==
 

== External links ==
*  

 
 
 
 
 


 