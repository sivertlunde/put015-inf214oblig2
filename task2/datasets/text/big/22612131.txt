92 in the Shade
 
{{Infobox film
| name           = 92 in the Shade
| image          = 92InTheShade1975Poster.jpg
| alt            = 
| caption        = Film Poster
| director       = Thomas McGuane
| producer       = Georges Pappas
| writer         = Thomas McGuane
| starring       = Peter Fonda Warren Oates Margot Kidder Burgess Meredith
| music          = Michael J. Lewis
| cinematography = Michael C. Butler
| editing        = Ed Rothkowitz
| studio         = 
| distributor    = United Artists
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
92 in the Shade is a 1975 film written and directed by Thomas McGuane, based on his 1973 novel of the same name. It stars Peter Fonda, Warren Oates, Elizabeth Ashley and Margot Kidder.

==Plot==
The story centers on Tom Skelton, a young man who opens a charter fishing business in Key West, Florida. He enters into a rivalry with a local sea captain named Dance and his partner Carter, who steal one of the new fishing guides clients. Skelton retaliates by burning Dances boat.

==Cast==
*Peter Fonda as Tom Skelton
*Warren Oates as Nicholas Dance
*Margot Kidder as Miranda
*Burgess Meredith as Goldsboro
*Harry Dean Stanton as Carter
*Elizabeth Ashley as Jeannie Carter

==Production==
McGuane directed the film and wrote the script. He was married to one of the films female stars and had a scandalous affair with the other, as detailed in the autobiography Actress authored by Elizabeth Ashley.

==Release==
Although the film was a box-office failure, a January 22, 1976 New York Times review described it as "a more satisfying picture" than Rancho Deluxe, another 1975 film written by McGuane. He would later write The Missouri Breaks, which co-starred Jack Nicholson and Marlon Brando.

==External links==
 
 
*  

 
 
 
 
 
 
 
 
 


 