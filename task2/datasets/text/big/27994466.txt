Fight for Us
{{Infobox Film | name = Fight for Us
  | image = 
  | caption = 
  | director = Lino Brocka
  | producer = Salvatore Picciotto Leonardo de la Fuente
  | writer = Jose F. Lacaba
  | starring = Phillip Salvador Dina Bonnevie Gina Alajar Bembol Roco
  | music = 
  | cinematography = Rody Lacap
  | editing =  Cannon Films
  | distributor = Pathé
  | released = 1989
  | runtime = 125 minutes
  | country = Philippines United States Italy
  | language = Tagalog
  | budget = 
  | followed_by = 
}}
 1989 Filipino Filipino political thriller film directed by Lino Brocka. The film stars Phillip Salvador and Dina Bonnevie.

==Plot==
After being released from a Philippine prison following the fall of Ferdinand Marcos, a former priest (Salvador) gives up his violent activities in favor of peaceful social activism. But he quickly discovers that vicious death squads, and notorious counter-insurgency operations, still plague his country, and soon he is considering joining the resistance again.

==Trivia==
* The production was notorious when those involved were caught up in criminal activity by Giancarlo Parretti (then the CEO of Pathe Communications, which owned Cannon Films at that time), leading people from the production to be arrested, while others continue to flee from the US authorities. Due to the incident, the film is now in the public domain.
* During its initial release, film was banned in the Philippines by Corazon Aquino due to its subversive message (Lino Brocka was a member of a progressive organization representing artists). It was shown to the Filipino public after its premiere at the 1989 Cannes Film Festival.   
* The film became popular for activists concerning Human Rights and against "Counter-Insurgency" operations initiated by the Government. Names and places were changed yet these were based from real incidents concerning the status of the Philippines during its production.
* Aside from the film being in the public domain, ancillary rights rest with the Philippine government.

==References==
 

 
 
 
 


 
 