Viva Zapata!
 
{{Infobox film
| name           = Viva Zapata!
| image          = Viva Zapata!.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Elia Kazan
| producer       = Darryl F. Zanuck
| writer         = John Steinbeck
| starring       = Marlon Brando Jean Peters Anthony Quinn
| music          = Alex North
| cinematography = Joseph MacDonald
| editing        = Barbara McLean
| distributor    = Twentieth Century Fox Film Corporation
| released       =  
| runtime        = 113 minutes
| country        = United States Spanish
| budget         = $1.8 million 
| gross          = $1,900,000   
}}
Viva Zapata! is a 1952 biographical film starring Marlon Brando and directed by Elia Kazan. The screenplay was written by John Steinbeck, using as a guide Edgcomb Pinchons book, Zapata the Unconquerable, a fact that is not credited in the titles of the film.

The cast includes Jean Peters and, in an Academy Award-winning performance, Anthony Quinn.

The movie is a fictionalized account of the life of Mexican Revolutionary Emiliano Zapata from his peasant upbringing, through his rise to power in the early 1900s, to his death.

To give the film as authentic a feel as possible, Kazan and producer Darryl F. Zanuck studied the numerous photographs that were taken during the revolutionary years, the period between 1909 and 1919 when Zapata led the fight to restore land taken from the people during the dictatorship of Porfirio Díaz.

Kazan was especially impressed with the Agustin Casasola collection of photographs and he attempted to duplicate their visual style in the film. Kazan also acknowledged the influence of Roberto Rossellinis Paisan (film)|Paisan. 

==Plot== Eufemio (Anthony Harold Gordon).
 then has him murdered. Zapata himself is lured into an ambush and killed.

Zapata  is depicted in the film as an incorruptible rebel leader. He is guided by his desire to return the land to the peasants, who have been robbed, while forsaking his personal interest. Steinbeck meditates in the film on power, military and political, which corrupts men.

==Cast==
* Marlon Brando as Emiliano Zapata
* Jean Peters as Josefa Zapata, his wife
* Anthony Quinn as Eufemio Zapata
* Joseph Wiseman as Fernando Aguirre
* Arnold Moss as Don Nacio
* Alan Reed as Pancho Villa Margo as Soldadera
* Harold Gordon as Francisco Indalecio Madero
* Lou Gilbert as Pablo
* Frank Silvera as Victoriano Huerta
*   as Señor Espejo
* Richard Garrick as Old General
* Fay Roope as Porfirio Díaz
* Mildred Dunnock as Señora Espejo

==Awards==

===Academy Awards===
Anthony Quinn won the 1952 Academy Award for Best Supporting Actor.   

The film was also nominated for:  Best Actor in a Leading Role - Marlon Brando Best Writing, Story and Screenplay - John Steinbeck Best Art Direction-Set Decoration, Black-and-White - Lyle R. Wheeler, Leland Fuller, Thomas Little, Claude E. Carpenter Best Music, Scoring of a Dramatic or Comedy Picture - Alex North

===BAFTA Awards===
Marlon Brando won the 1953 BAFTA Award for Best Foreign Actor. The film was also nominated for Best Film from any Source.

===Cannes Film Festival=== Best Actor, Grand Prix du Festival International du Film.   

===Directors Guild of America===
Elia Kazan was nominated for a DGA Award for Outstanding Directorial Achievement in Motion Pictures in 1953.

===Golden Globe Award===
Mildred Dunnock was nominated for Best Supporting Actress in 1953.

==Production==
 

===Filming and casting===
Filming took place in various locations, including Durango, Colorado; Roma, Texas; and New Mexico.

The film tends to romanticize Zapata and in doing so distorts the true nature of the Mexican Revolution. Zapata fought to free the land for the peasants of Morelos and the other southern Mexican states.  Additionally, the movie inaccurately portrays Zapata as illiterate.  In reality, he grew up in a family with some land and money and received an education. John Steinbeck wrote a book titled Zapata. The original screenplay was written by the author and the book contains a newly found introduction by Steinbeck, the original proposed screenplay, and the official movie script.

Barbara Leaming writes in her biography of Marilyn Monroe that the actress tried and failed to obtain a part in this picture, presumably due to Darryl F. Zanucks lack of faith in her ability, both as an actress and as a box office draw.

 
 

==Release==

===Critical reception===
Viva Zapata! received mixed reviews from critics. Review aggregator Rotten Tomatoes reports that 67% critics have given the film a positive review, with a rating average of 6.3/10. Bosley Crowther of The New York Times gave a highly favorable review and noted that the film "throbs with a rare vitality, and a masterful picture of a nation in revolutionary torment has been got by Director Elia Kazan."  Variety (magazine)|Variety, on the other hand, criticized the direction and script: "Elia Kazans direction strives for a personal intimacy but neither he nor the John Steinbeck scripting achieves in enough measure."

==References==
 

==External links==
 
*  
*  
*  
*   detailed description of the plot
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 