The Beach Boys: An American Family
 
{{Infobox film
| name        = The Beach Boys: An American Family
| image       =
| director    = Jeff Bleckner
| producer    = John Whitman
| writer      = Kirk Ellis
| starring    = Kevin Dunn Fred Weller Alley Mills Nick Stabile Matt Letscher Emmanuelle Vaugier
| distributor =
| released    =  
| runtime     = English
| budget      = Gary Griffin, Clair Marlo
| awards      =
| imdb_id     =
}}

The Beach Boys: An American Family is a 2000 television film written by Kirk Ellis and directed by Jeff Bleckner. It is a dramatization of the early years of The Beach Boys, from their formation in the early 1960s to their peak of popularity as musical innovators, through their late-60s decline (and Brian Wilsons beginning battle with mental illness), to their re-emergence in 1974 as a nostalgia and "goodtime" act.
 ABC Television, licensed for the production, but was important to the story (such as the Smile (The Beach Boys album)|Smile album sessions, and music by criminal Charles Manson, who had collaborated with Dennis Wilson), was filled in with sound-alikes, reminiscent of the original recordings.

In 2000, Brian Wilson stated of the film: "I didnt like it, I thought it was in poor taste. ... And it stunk. I thought it stunk!"  He elaborated further: "I didnt like the second part. It wasnt really true to the way things were. Id like to see another movie if it was done right. But I just sort of turned my back to this one, or my other cheek, or whatever you wanna call it. It was best just to ignore it because it really wasnt true to life." 

==Cast==
{{columns-list|2|
*Kevin Dunn as Murry Wilson
*Fred Weller as Brian Wilson
*Alley Mills as Audree Wilson
*Nick Stabile as Dennis Wilson
*Emmanuelle Vaugier as Suzanne Love
*Ryan Northcott as Carl Wilson
*Matt Letscher as Mike Love
*Ned Vaughn as Al Jardine David Marks
*Jesse Caron as Bruce Johnston
*Amy Van Horne as Marilyn Rovell
*Jad Mager as Nick Venet
*Eric Matheny as Chuck Britz
*Harris Laskaway as Voyle Gilmore
*Clay Wilcox as Tommy Schaeffer
*Anthony Rapp as Van Dyke Parks
*Erik Passoja as Charles Manson
*David Polcyn as Phil Spector
*Tera Hendrickson as Carol Kaye
*James Intveld as Hal Blaine
*Jessica Shannon as a concert girl
*Elliot Kendall as Glen Campbell
*Steve Stanley as Barney Kessel
*Trevor St. John as Jan Berry
*Jacob Young as Dean Torrence
}}

== Awards ==
The Beach Boys: An American Family was nominated in nine individual categories at different award ceremonies. 

The film won three out of those 8 categories that it was nominated in. Listed below is the categories that the movie was nominated in as well as for what award:

*2000
**Emmy Awards Outstanding Mini-series
***Single-Camera Picture Editing For A Mini-series, Movie Or A Special
***Single-Camera Sound Mixing For A Mini-series Or A Movie
**Artios Awards
***Best Casting For TV Mini-Series
*2001 Eddie Awards
***Best Edited Miniseries or Motion Picture for Commercial Television (won)
**Excellence In Production Design Awards
***Best Television Movie or Mini-Series
**C.A.S. Awards
***Outstanding Achievement In Sound Mixing For A Television Movie-of-the-Week, Mini-Series (won) DGA Awards
***Outstanding Directorial Achievement In Movies For Television (won) Golden Satellite Awards
***Best Mini-series

== References ==
 

== External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 