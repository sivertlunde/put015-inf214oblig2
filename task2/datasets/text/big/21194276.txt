King of the Lost World
 

{{Infobox Film name = King of the Lost World image = King of the Lost World.jpg caption = DVD cover art director = Leigh Scott producer =   writer = Arthur Conan Doyle (Novel, uncredited) Carlos De Los Rios (Screenplay) starring =Bruce Boxleitner Jeff Denton Rhett Giles Steve Railsback Thomas Downey music = cinematography = editing = distributor = released =   runtime = 90 mins. language =English country = United States budget = $ 1,000,000
}}
 The Lost the remake of King Kong released in the same year, particularly as both stories center on a giant ape. Hence, King of the Lost World is a mockbuster of said film, a tradition that The Asylum usually undergoes.

==Plot==
A plane crashes in a remote jungle. Many survive, but the front end of the plane and the cockpit are nowhere to be found. The only way to seek help is to find the cockpit and radio a message.

Ed Malone (Jeff Denton) climbs a small hill and sees the cockpit about a mile distant. A group decide to leave the plane to search for the radio. John Roxton (Rhett Giles) leads the group through the jungle. The remaining survivors stay at the crash site in case a plane passes by.
 Lieutenant Colonel Challenger (Bruce Boxleitner), who managed to travel alone to the downed plane,  carrying a briefcase he doesnt seem to want to part with.

The group continues to look for the cockpit with some individuals being killed or lost along the way. They encounter a military plane. Challenger tries to rig a missile as a signal gun, but fails. The group finds a cavern for shelter that night, and are attacked by giant scorpions. John and Tianka are killed, while a desperate Ed, Challenger, Rita, Dana, and Natalie flee for safety. As they escape from the stream, they are captured by natives living on a plateau. It is learned that the natives have been stripping the planes to avoid outsiders. The natives are discovered to be survivors from a crash long ago, and have developed a  sacrificial rite to appease the creatures of the jungle. Dane and Natalie are brainwashed into joining them, while Ed is chosen as the sacrifice.

However, the sacrificial ceremony fails when a giant ape attacks, and Dana (who faked being brainwashed) saves Ed. They meet up with Challenger and Rita, then spot military jets overhead, moving in to attack the giant ape, but are destroyed. Challenger is killed by a native, leaving Ed alone to fight the ape.

Ed detonates a nuclear bomb from a crashed plane and successfully kills the ape, but also destroys their cockpit, trapping  Ed, Rita, and Dana in the jungle. They contemplate dying, to which Ed tersely replies: "Not today" before kissing Rita.

==Differences from The Lost World==
* The setting is the 21st century rather than the early 20th century.
* In the  novel by Arthur Conan Doyle, it was made clear that Ed Malone was Ireland|Irish. In this film he is United States|American.
* In the novel, the character of Professor Challenger was a misanthropic scientist. In this film, he is a lieutenant colonel in the United States Air Force.
* The character of Professor Summerlee is now a woman named Rita Summerlee.
* The expedition in the novel originally journeyed to the Lost World to prove Prof. Challengers claims that dinosaurs lived in modern times. In the film, the expedition accidentally crash-land into the Lost World.
* There was no giant ape in the novel, the original antagonists were a race of ape-men living in the Lost World.
*In the film, the characters are stranded on an island, however, in the novel and previous film adaptions, the story takes place on a plateau.
*There are giant scorpions, giant spiders, man-eating vines and giant winged lizards in the film.  In the novel there are only dinosaurs and other prehistoric creatures.
* In the movie many characters, including Challanger and John Roxton die. However they survive in the original novel.

==Cast==
* Bruce Boxleitner as Professor Challenger|Lt. Challenger
* Jeff Denton as Ed Malone
* Rhett Giles as John Roxton
* Sarah Lieving as Rita Summerlee
* Christina Rosenberg as Dana
* Steve Railsback as Larry
* Chriss Anglin as Olo
* Amanda Ward as Natalie
* Boni Yanagisawa as Tianka
* Andrew Lauer as Steven
* Thomas Downey as Reggie
* Amanda Barton as Taylor
* James Ferris as Yuri
* Jennifer Lee Wiggins as Etienne
* Angela Horvath as Chrissy

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 