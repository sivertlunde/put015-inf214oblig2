Verginità
{{Infobox film
| name           =Verginità
| image          =Verginit_poster.jpg
| image_size     =
| caption        =
| director       =Leonardo De Mitri
| producer       =
| writer         =
| narrator       =
| starring       =Irene Genna 
| music          =Gino Filippini 
| cinematography =Giuseppe La Torre 	
| editor       =
| distributor    =
| released       = 1952
| runtime        =
| country        = Italy Italian
| budget         =
}}
 1952 Italy|Italian melodrama film written and directed by Leonardo De Mitri. 

==Plot==
A country girl who is a diligent devourer of picture stories, decides to participate in a beauty contest without the consent of her father. She dreams of a glamorous career in the big city. When she loses the contest, she is approached by two men who offer her a modeling job. However, they are in fact white slavers who intend to lure her to Brazil. 

==Cast==

*Irene Genna as Gina
*Leonardo Cortese as  Franco Rossi
*Franca Marzi as  Aurora
*Otello Toso as  Giancarlo
*Eleonora Rossi Drago as  Mara Sibilia
*Arnoldo Foà as  René
*Tamara Lees as  Lidia
*Checco Durante as Mr. Corelli 
*Guglielmo Barnabò as Impresario 
*Mario Siletti   
*Mario Ferrari   

==References==
 

==External links==
*  

 
 
 
 

 