The Unprecedented Defence of the Fortress Deutschkreuz
{{Infobox film
| name           = The Unprecedented Defence of the Fortress Deutschkreuz
| image          = unprecedented.jpg
| image_size     =
| caption        =
| director       = Werner Herzog
| producer       = Werner Herzog
| writer         = Werner Herzog Jaime Pacheco
| editing        = Werner Herzog
| distributor    =
| released       = 1966
| runtime        = 15 min.
| country        = West Germany German
| budget         =
| preceded_by    =
| followed_by    =
}}

The Unprecedented Defence of the Fortress Deutschkreuz ( ) is a 1966 short film by Werner Herzog filmed in Deutschkreuz, Austria. {{cite book
  | last = Herzog
  | first = Werner
  | title = Herzog on Herzog
  | publisher = Faber and Faber
  | year = 2001
  | isbn = 0-571-20708-1 }}  Herzogs official website describes the film as "A satire on the state of war and peace and the absurdities it inspires." 

In the film, four men break into an abandoned castle that was the site of a battle between the Russians and Germans during World War II. The men find old military uniforms and equipment, and equip themselves for a defense of the castle. They see farmers approaching the castle, but are disappointed when they fail to attack. The film ends with the four men, armed, storming out of the castles front gates. The films actors have no dialogue- the only spoken text is delivered by a narrator, who discusses his thoughts on war and various other subjects.

==Directors Thoughts== Signs of Signs of Life a few years later. People think they are being besieged, yet there is actually no enemy and they are left in the lurch." 

==References==
 

== External links ==
*  

 

 
 
 
 
 


 