Dancer, Texas Pop. 81
{{Infobox film
| name = Dancer, Texas Pop. 81
| image = Dancer texas pop eighty one.jpg
| image_size = 215px
| alt = 
| caption = Promotional poster
| director = Tim McCanlies
| producer = Dana Shaffer Peter White Chasse Foster
| writer = Tim McCanlies
| starring = Breckin Meyer Peter Facinelli Eddie Mills Ethan Embry
| music = Steve Dorff
| cinematography = Andrew Dintenfass
| editing = Rob Kobrin
| studio = Caribou Pictures
| distributor = TriStar Pictures
| released =  
| runtime = 97 minutes
| country = United States
| language = English
| budget = 
| gross = $676,631 
}}
Dancer, Texas Pop. 81 is a 1998 Comedy-drama|comedy-drama film starring Breckin Meyer, Peter Facinelli, Eddie Mills, and Ethan Embry.

==Plot==
Set in the small, fictional American town of Dancer (Brewster County, Texas), only 81 people live in this town. Following their high school graduation, four young men wrestle with their decisions to leave town for Los Angeles.

==Cast==
* Breckin Meyer as Keller Coleman
* Peter Facinelli as Terrell Lee Lusk
* Eddie Mills as John Hemphill
* Ethan Embry as Squirrel
* Ashley Johnson as Josie Hemphill
* Patricia Wettig as Mrs. Lusk Michael ONeill as Mr. Lusk Eddie Jones as Earl
* Wayne Tippit as Kellers grandfather
* Alexandra Holden as Vivian
* Keith Szarabajka as Squirrels father
* Shawn Weatherly as Sue Ann
* Michael Crabtree as Mr. Hemphill
* Lashawn McIvor as Mrs. Hemphill

==Reception==
Dancer, Texas Pop. 81 received positive reviews; it currently holds an 80% rating on Rotten Tomatoes. 

Film reviewer Joe M. OConnell, writing for the San Antonio Express-News, called the film "The finest representation of small-town Texas since The Last Picture Show."  The movie was filmed in Fort Davis, Texas as dancer and the tri county area.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 


 