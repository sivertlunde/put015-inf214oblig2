Pontianak Harum Sundal Malam
 
 
{{Infobox film
| name = Pontianak Harum Sundal Malam
| image =
| caption =
| director =  Shuhaimi Baba
| producer = Persona Pictures
| writer = Shuhaimi Baba
| starring = Maya Karin  Azri Iskandar
| distributor = Golden Satellite
| released =  
| runtime = 
| language = Malaysian
| country = Malaysia
| budget =
}}

Pontianak Harum Sundal Malam, also known as Pontianak Scent of the Tuber Rose or Fragrant Night Vampire, is a 2004 Malaysian horror film directed and written by Shuhaimi Baba. Starring Maya Karin, the film is about a restless spirit (Pontianak (folklore)|pontianak) Meriam who seeks revenge upon those who killed her. The film was released on 20 May 2004 and was a major box office success in Malaysia.

A sequel, Pontianak Harum Sundal Malam 2, was released in 2005. It is about Meriam continue to revenge the family of Marsani until Zali, a son of Marsani dies for saving her daughter Maria.

==Casts==
* Maya Karin - Meriam and Pontianak (Main Role)
* Azri Iskandar - Marsani (Main Role)
* Rosyam Nor - Asmadi (Main Role)
* Ida Nerina - Si Tam (Supporting Role)
* Kavita Sidhu - Anna (Supporting Role)
* Eizlan Yusof - Norman (Supporting Role)
* Nanu - Laila (Supporting Role)
* Shahronizam Noor - Daniel (Supporting Role)
* Leya - Rafiah (Pelakon Pembantu) Datuk Aziz Sattar - Tok Selampit
* Isma Hasnor - (Extra)

==External links==
*  
*  

 
 

 
 
 
 
 
 
 