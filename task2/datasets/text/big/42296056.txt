Sajani (2004 film)
{{Infobox film
| name           = Sajani
| image          = Sajani.jpeg
| image_size     = 
| border         = 
| alt            = 
| caption        = Sajani Dvd cover
| film name      = 
| director       = Swapan Saha
| producer       = Debendra Kuchar
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  See below
| music          = Ashok Bhadra
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Eskay Movies
| released       = 2004
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali film directed by Swapan Saha and produced by Debendra Kuchar.The film features actors Prosenjit Chatterjee and Rimi Sen in the lead roles. Music of the film has been composed by Ashok Bhadra.    The film was a remake of Tamil film Arputham.

== Cast ==
* Prosenjit Chatterjee
* Rimi Sen
* Jishu Sengupta
* Subhasish Mukhopadhyay
* Locket Chatterjee
* Bodhisattwa Majumdar
* Lokesh Ghosh
* Anuradha Ray
* Kalyani Mondal
* Abhik Bhattacharya

==Audio==
{{Infobox album
| Name        = Sajani 
| Tagline     =
| Type        = film
| Artist      = Ashok Bhadra
| Cover       =
| Released    = 2004
| Recorded    =
| Genre       =
| Length      =
| Label       =
| Producer    = Debendra Kuchar
| Reviews     =
| Last album  =
| This album  =
| Next album  =
}}
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 90%;"
|- align="center"
! style="background:#B0C4DE;" | No
! style="background:#B0C4DE;" | Song title
! style="background:#B0C4DE;" | Singers
|-
|1|| "Bhalo Laage Shudhu Tomake" || Udit Narayan
|-
|2|| "Chokher Taray Ki Jadu" ||
|-
|3|| "Jani Khunje Pabo" || 
|-
|4|| "Kal Je Ki Hobe Keu Jane Na" || 
|-
|5|| "Ki Hoto Moner Kotha Janale" ||  
|-
|6|| "Ki Je Byatha Shudhu Mon-e Jane" ||
|-
|7|| "Ki Je Byatha Shudhu Mon-e Jane (2)" ||
|-
|8|| "Tomar Oi Duti Chokh" ||
|}

== References ==
 
 

 
 
 
 


 