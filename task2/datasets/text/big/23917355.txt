Paris Trout
{{Infobox film
| name           = Paris Trout
| image          =
| caption        =
| director       = Stephen Gyllenhaal
| producer       = Frank Konigsberg Larry Sanitsky
| writer         = Peter Dexter (novel and screenplay)
| starring       = Dennis Hopper Barbara Hershey Ed Harris
| music          = David Shire
| cinematography = Robert Elswire
| editing        = Harvey Rosenstock
| distributor    = Palace Pictures
| released       = April 20, 1991
| runtime        = 100 minutes
| country        = United States
| language       = English
}}
 1991 drama film directed by Stephen Gyllenhaal, starring Dennis Hopper, Barbara Hershey, and Ed Harris.   
 Paris Trout by the author Peter Dexter. 

==Plot==
Paris Trout is an unrepentant racist in 1949 Georgia.  The greedy and paranoid shopkeeper murders the sister of a black man who refuses to repay Trout’s IOU. When Trout is arrested for the crime he is stunned and enraged, showing himself to be a man of the old south. Lawyer Harry Seagraves arrives to calm the waters in court but is soon caught in crimes of his own, including a dangerous and doomed affair with Trout’s wife.

==Cast==
* Dennis Hopper as Paris Trout
* Barbara Hershey as Hanna Trout
* Ed Harris as Harry Seagraves Ray McKinnon as Carl Bonner
* Tina Lifford as Mary Sayers
* Darnita Henry as Rosie Sayers
* Eric Ware as Henry Ray Sayers
* RonReaco Lee as Chester Sayers
* Gary Bullock as Buster Devonne
* Sharlene Ross as Mother Trouts Nurse
* Jim Peck as Estes Singletray
* Dan Biggers as Mayor Harn
* Ernest Dixon as Truck Driver
* Wallace Wilkinson as Dr. Brewer
* Ron Leggett as Glass Man

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 