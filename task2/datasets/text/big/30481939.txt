Santrash
{{Infobox film 
 | name = Santrash
 | image = Santarshfilmnew.jpg
 | caption = DVD Cover
 | director = Narayan Ray
 | producer = Sunil Sharma
 | camera =
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Madhabi Mukherjee Ranjit Mullick Raja Chattopadhyay
 | music = Bidyut Goswami
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = 2003
 | runtime = 125 min.
 | country  = India Bengali
 | budget =   1.5 Crores
 | preceded_by = 
 | followed_by = 
 }}
 2003 Bengali Indian feature directed by Narayan Ray, starring Mithun Chakraborty, Madhabi Mukherjee, Ranjit Mullick and Raja Chattopadhyay

==Plot==

A fast paced thriller, with Mithun in the lead.

==Cast==

*Mithun Chakraborty
*Ranjit Mullick
*Madhabi Mukherjee
*Locket Chatterjee
*Narayan Ray
*Piyali Banerjee
*Rahul Barman
*Raja Chattopadhyay
*Rimita Ray

==References==
* http://www.bengalitollywood.com/movies/Bengali-Tollywood/Santrash-2003
* http://www.gomolo.in/Movie/Movie.aspx?mid=15250

==External links==

 
 
 
 
 
 