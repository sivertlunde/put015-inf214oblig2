Bhoomiyile Rajakkanmar
{{Infobox film
| name = Bhoomiyile Raajakkanmar
| image = Bhoomiyilefilm.jpg
| caption = Promotional Poster
| director = Thampi Kannanthanam
| producer = Joy Thomas
| writer = Dennis Joseph
| screenplay = Dennis Joseph
| starring = Mohanlal Suresh Gopi Adoor Bhasi Nalini
| music = S. P. Venkatesh
| cinematography = Jayanan Vincent
| editing = K Sankunni
| studio = Jubilee Productions
| distributor = Jubilee Productions
| released =  
| country = India Malayalam
}} 
 
Bhoomiyile Rajakkanmar is a Malayalam movie released meaning Kings of Earth in 1987 directed by Thambi Kannanthanam and featuring  Mohanlal, Suresh Gopi and Jagathy. The scion of Thekkumkoor royal family wanted to introduce his heir to democracy. For this sole purpose he bribes the Chief Minister. Once in power, Mahendra Varma tries to be independent and help his friend Jayan.   

==Cast ==
*Mahendra Varma - Mohanlal
*Valiya Thampuran - Adoor Bhasi
*Aromalunni - Jagathy
*Jayan - Suresh Gopi
*Lakshmi - Nalini
*Chief Minister - Balan K. Nair
*Babu - Jagadish
*Geurilla Chandran - Mohan Jose
*Gopala Pillai - Kollam Thulasi
*Antony - Azeez
*Vishwambaran - K.P.A.C. Sunny
*Rajashekharan - Prathapachandran
*Raju - K.B. Ganesh Kumar Siddique
*Kunjunni Asan - Kunjandi
*Lawyer to H.M. - Oduvil Unnikrishnan

==Soundtrack==
The music was composed by S. P. Venkatesh and lyrics was written by Shibu Chakravarthy.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Nin Kannukal Kavitha || K. J. Yesudas || Shibu Chakravarthy ||
|-
| 2 || Shuklaambaradharam   || || Shibu Chakravarthy ||
|-
| 3 || Sindooravaanil Mullappoonkaavil || Unni Menon || Shibu Chakravarthy ||
|}

==References==
 

== External links ==
*  

 
 
 
 


 