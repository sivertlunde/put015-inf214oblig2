A Dream or Two Ago
{{Infobox film
| name           = A Dream or Two Ago
| image          =
| image size     =
| caption        = James Kirkwood
| producer       =
| writer         = Arthur Henry Gooden Henry Albert Phillips (story)
| narrator       =
| starring       = Mary Miles Minter
| music          =
| cinematography = Carl Widen
| editing        =
| distributor    = Mutual Film
| released       =  
| runtime        = 51 mins. 
| country        = United States Silent English intertitles
| budget         =
}}
 silent drama James Kirkwood. The film was one of the few films of Mary Miles Minter which survived. The film was restored in 2004 and was shown along with Innocence of Lizette (1916) at a Dutch film festival. 

==Plot==
Millicent Hawthorne is the six-year-old daughter of a wealthy family. When a jewel thief robs the family, he kidnaps and raises Millicent to be a thief. Her parents try to forget about her loss by supporting people who need help, while Millicent gets amnesia after a fall on her head. When a friend of her parents notices her, he immediately informs them. A chase to reunite with the daughter follows.

==Cast==
* Mary Miles Minter - Millicent Hawthorne
* Dodo Newton - Millicent (age 6)
* Lizette Thorne - Her Mother
* Clarence Burton - Her Father
* John Gough - Humpy
* Orral Humphrey - Kraft
* Gertrude Le Brandt

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 