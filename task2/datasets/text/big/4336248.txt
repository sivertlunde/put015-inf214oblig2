Hell Is for Heroes (film)
{{Infobox film
| name           = Hell Is for Heroes
| image          = Hell Is for Heroes cover.jpg Richard Carr & Robert Pirosh Nick Adams
| director       = Don Siegel
| producer       = Henry Blanke
| movie_music    = Leonard Rosenman
| cinematography = Harold Lipstein
| editing        = Howard A. Smith
| distributor    = Paramount Pictures
| released       =  
| runtime        = 90 min
| country        = United States English
| music          = Leonard Rosenman
| awards         = 
| budget         = 
}}
 1962 United American war film directed by Don Siegel and starring Steve McQueen. It tells the story of a squad of U.S. soldiers from the 95th Infantry Division who, in the fall of 1944, must hold off an entire German company for approximately 48 hours along the Siegfried Line until reinforcements reach them.

==Plot==
 ) and his men are taking a well-deserved rest behind the lines after conducting front-line combat operations for several weeks.  Rumor has it the unit will be rotated state-side and the men are almost giddy in anticipation. During an interlude at a church and later at a tavern, the senior non-commissioned officer, Technical Sergeant Pike (Fess Parker), happens upon acquaintance John Reese (Steve McQueen), a former master sergeant, demoted to private, who walks about armed with a distinctive M3 submachine gun. Reese is the quintessential troubled loner, managing to alienate almost everyone in the squad right from the beginning. Unlike his jubilant comrades, the prospect of a long break from combat, perhaps the end of the war itself, renders Reese morose.  The company commander, Captain Loomis (Joseph Hoover), is worried because Reese, although already having won a field citation, acts irresponsibly when there is no fighting, but Pike comments that he is a good soldier in combat.
 Nick Adams), who is not a soldier, but stays with the squad in hopes of accompanying the men upon their return to the United States. The morning after they arrive at their appointed post and dig in, the men realize that an unannounced overnight withdrawal of the main American force has left them spread dangerously thin.  Finally, Pike arrives to explain the situation, which only heightens everyones awareness that any reconnaissance by the Germans across the valley will quickly reveal how weak the American defenses are there. 
 pillbox (Newhart was noted for his telephone conversation skits in his stand-up comedy routines). Additionally, Larkin has his men run wire to three empty ammo cans, partially filled with rocks and hung from trees, distributed along gaps in their front lines, which they can pull to create noise to make the Germans believe that a much larger American force is present.  

A German raid results in Cumberlys death, but Reese manages to kill three Germans in close combat, the last with a butcher knife. Worried that the German survivors will report on the understrength American lines, Reese recommends attacking a large, opposing German Bunker#Pillbox|pillbox, flanked by a minefield and barbed wire, to make the enemy pause and convince them the Americans are at normal strength.  Larkin, fearing an overwhelming enemy assault on their positions, decides to go find Pike and obtain his permission for the pillbox attack, but is killed in an artillery barrage. Reese decides to proceed without orders and two others, Henshaw and Kolinsky, go along. The attack fails, when Henshaw accidentally sets off an undetected S-mine, fatally burning with the exploding flamethrower tanks he carries, as well as illuminating the battlefield. Reese and Kolinsky retreat. As they run back to their lines, Kolinsky is struck by shrapnel through the back and abdomen, and finally dies, screaming about his guts, as a medic and others attend to his wounds.

Reinforcements arrive soon after, along with Sgt. Pike and a furious Captain Loomis, who berates Reese and promises him a court-martial for defying orders to hold the line, but only after the American assault at dawn. The dominant German pillbox fires on the advancing Americans, who press on despite heavy casualties. Determined to eliminate the pillbox, Reese gets within striking range, aided by Corby, manning a flamethrower. Reese throws a satchel charge into the pillbox, but, in the process, is wounded in the back and stomach. When the unexploded satchel charge is tossed out by the alert defenders, the wounded Reese retrieves it and carries it back through the pillbox opening, blowing up the fortifications occupants and himself. Corby, at Pikes command, directs his flamethrower at the blown-out pillbox window, until it is engulfed with fire, as the Americans continue to advance, and fall, to other unseen German weapons.

== Cast ==
* Steve McQueen as Pvt. John Reese
* Bobby Darin as Pvt. Dave Corby
* Fess Parker as T/Sgt. Bill Pike
* Harry Guardino as Sgt. Jim Larkin
* James Coburn as Cpl. Frank Henshaw
* Bob Newhart as Pfc. James E. Driscoll
* Mike Kellin as Pvt. Stan Kolinsky
* Joseph Hoover as Cpt. Roger Loomis
* Bill Mullikin as Pvt. Joe Cumberly
* L.Q. Jones as Supply Sergeant Frazer
* Michele Montau as Monique Ouidel
* Don Haggerty as Cpt. Mace Nick Adams as Pvt. Homer Janeczek

==Production== 35th Infantry Go for Broke! a 1951 war film about the famed 442nd Regimental Combat Team.  Soon after Hell Is for Heroes, he created the World War II TV series Combat!. Pirosh based some of the events in his film on his unit being withdrawn from the Vosges area to move towards the Battle of the Bulge, with their former positions in the line held by a small force in a then-classified deception operation. Pirosh based Nick Adams Polish character after an actual Displaced Person who followed his unit around. 

Originally, Pirosh was also to have directed and produced the film, but he walked away from the project after trouble with McQueen.  Piroshs screenplay was originally entitled Separation Hill, but the title was changed by Paramounts publicity office as being too close to the 1959 Korean War film Pork Chop Hill. 
 malfunctioning firearms M3 Grease blanks used.
 James Bacon visited the set and said that "Steve McQueen is his own worst enemy".  Bobby Darin overheard the remark and replied, "Not while Im still alive."  Bob Newhart said that he had been offered the film a year previous and found that the script had changed when Steve McQueen came on board; Newhart believed the original script had been set to feature Darin as the main star of the film. 
 The Great Escape.)
 Cottonwood and Redding, California, many of the scenes were shot at night for the comfort of the actors. 

During the production, Newharts comedy albums were selling unexpectedly well, resulting in higher fee offers for stand-up comedy nightclub appearances.  As a result, he sought ways to have his character killed off so that he could leave the production.  The director consistently told him that he would be in the film until the end. 

Both Newhart and Parker  recalled that the film ended abruptly due to Paramount shortening the production of the film for financial reasons.

A novelisation of the screenplay was written by Curt Anders.

==Star Trek: Deep Space Nine== The Siege of AR-558" are named after characters and actors from this film. These include Patrick Kilpatricks character Reese, Annette Heldes character Larkin and Bill Mumys character Kellin (named after the actor Mike Kellin). Unseen characters named after characters from the film include Captain Loomis and Commander Parker. The episode has a similar plot, where Starfleet troops have been holding off multiple attacks from enemy forces for five months.

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 