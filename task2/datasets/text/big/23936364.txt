Long Shot (1939 film)
 
{{Infobox film
| name           = Long Shot
| image          =C. Henry Gordon in Long Shot.jpg
| image_size     =180px
| caption        =C. Henry Gordon in the film
| director       = Charles Lamont
| producer       = Charles Lamont (associate producer) Franklyn Warner (producer)
| writer         = Ewart Adamson (screenplay) Harry Beresford (original story) and George Callaghan (original story)
| narrator       =
| starring       = See below
| music          =
| cinematography = Arthur Martinelli
| editing        = Bernard Loftus
| distributor    =
| released       = 6 January 1939
| runtime        = 69 minutes
| country        = United States
| language       = English
}}

Long Shot is a 1939 American horse racing film directed by Charles Lamont.

The film is also known as The Long Shot.

==Plot==

Henry Sharon is about to be ruined financially by rival stable owner Lew Ralston when he gets an idea to fake his own death. His prize horse Certified Check is bequeathed to niece Martha, a young woman Ralston had hoped to marry.

Martha and friend Jeff Clayton begin to enter Certified Check in races, but he always loses. Then they get a tip that the horse hates running near the rail.
 Santa Anita, but first he must be kept out of sight to keep Ralston from sabotaging his chances.

== Cast ==
 Gordon Jones as Jeff Clayton Marsha Hunt as Martha Sharon
*C. Henry Gordon as Lew Ralston
*George Meeker as Dell Baker Harry Davenport as Henry Sharon
*George E. Stone as Danny Welch
*Frank Darien as Zeb Jenkins Tom Kennedy as Mike Claurens
*Emerson Treacy as Henry Knox
*Gay Seabrook as Helen Knox
*Benny Burt as Joe Popopopolis James Robinson as Tucky
*Denmore Chief as Certified Check Joe Hernandez as Racing Announcer
*James Keefe as Racing Announcer
 Carl Meyer, Lee Phelps, Norman Phillips, Jason Robards Sr. and Claire Rochelle appear uncredited.

== External links ==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 


 