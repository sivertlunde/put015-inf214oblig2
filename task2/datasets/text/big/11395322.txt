The Last Sin Eater (film)
{{Infobox film
| name           = The Last Sin Eater
| image          = Last sin eater.jpg
| caption        = US Poster for "The Last Sin Eater"
| director       = Michael Landon Jr.
| producer       = Brian Bird  Michael Landon Jr.
| based on       =  
| screenplay     = Brian Bird  Michael Landon Jr.
| narrator       = 
| starring       = Liana Liberato Louise Fletcher Henry Thomas
| music          = Mark McKenzie Robert Seaman
| editing        = Michael Landon Jr.
| studio         = Believe Pictures
| distributor    = Fox Faith
| released       =  
| runtime        = 117 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} 1998 The novel of the same name by Francine Rivers. It was produced by Believe Pictures.

==Plot==
The Last Sin Eater is a story that takes place in 1850s Appalachia in a settlement community of Welsh Americans. Ten-year-old Cadis grief over the death of her beloved grandmother, the only person who seemed to love her unconditionally, is compounded by a previous family tragedy for which she believes her family blames her. During her grandmothers funeral rites, Cadi sees the face of the village sin-eater, a person who absolves the deceased, at death, of their sins in this tiny Smoky Mountain community. 

About the same time, a preacher comes to the isolated valley and camps outside the village. Through dialogue with the holy man, the young girl slowly realizes that the sin eater is false and learns of Jesus and Christianity. Cadis baptism leads to a reconciliation with her family.

But Cadis new found faith leads to tragedy and confrontation as the close-knit community must finally face the horrific secret on which their village was founded.

==Cast==
*Henry Thomas ... Man of God
*Louise Fletcher ... Miz Elda
*Liana Liberato ... Cadi Forbes
*Soren Fulton ... Fagan Kai
*Stewart Finlay-McLennan ... Brogan Kai
*Peter Wingfield ... The Sin Eater

==Reception==
===Box office===
The Last Sin Eater was unsuccessful commercially. It was released in 429 theaters and remained in theaters for just two weeks. It fell far short of recovering production costs: according to Box Office Mojo the film grossed only $388,390 in these 14 days. However, the film earned $246,483 in its opening weekend (63.5% of total income).   

===Reviews===
The Last Sin Eater received mixed reviews. Matt Zoller Seitz of The New York Times said, "Handsomely produced, earnestly performed and 100 percent irony-free, The Last Sin Eater is religious art for mainstream consumption." The review gave credit to the direction and photography, saying, "The movie is a big-screen Sunday school story with sumptuous scenery, graceful crane shots and Rembrandt lighting — designed mainly to impart and then repeat wisdom about guilt, sin and redemption — this cant really be considered a flaw."    Laura Kelly of South Florida Sun-Sentinel said, "With its dulling straightforward pitch for Jesus, The Last Sin Eater seems like the worst of films from a non-born-again perspective." 

Marjorie Baumgarten of The Austin Chronicle said the direction of The Last Sin Eater, "The camera always seems to be too far off when you wish to get a better look and too close when you wish that it would point at anything other than what’s in the frame."    However, Betty Joe Tucker of Reel Talk was impressed by the cinematography, saying, "This extremely well-photographed film establishes a compelling you are there atmosphere. It also boasts impressive performances by children and adult actors alike. Liana Liberato, who plays Cadi, is a real find. Because of the intensity and vulnerability she projects."    Additionally, Variety (magazine)|Variety complimented the look of the film: "Pic was attractively lensed by Robert Seaman in Utah locales that adequately double for Appalachian mountain country."   

==Differences from Novel==
* In the book, Cadi has an older brother named Iwan, but in the movie, he is completely omitted, along with much of the epilogue.
* A few subplots are also left out, such as Miz Elda turning out to be the mother of Iona Kai, making Fagan her grandson, along with the reason Sim Gilivray never doubted that his being chosen as the sin eater was a punishment from God. (In the novel, it is revealed that he killed Bletsungs father when he found the man raping Bletsung.)
* In the book, Fagan has two older brothers, Cleet and Douglas, while only Cleet appears in the movie.
* In the epilogue of the novel, Cadi says that for a short time Fagan leaves the valley and receives an education. He returns with a Bible, which is the first ever brought to the valley. In the film, the "man of God" is in possession of a Bible and gives it to Cadi as he is dying.
* The scene in which Cadi and Fagan announce that the practice of sin-eating is useless and what the man of God has taught them is much altered from the novel version. In the movie, it takes place in the cemetery, while the book presents it with Fagan and Cadi (mostly Fagan, while the film shows Cadi as the main speaker) speaking from Miz Eldas front porch, as the townspeople have gone to her house upon hearing the bell, believing her to be dead. In the book, Sim Gillivray is not saved at the gathering as presented in the movie, but on Dead Mans mountain, and is converted by Fagan, not Cadi.
* In the written version of when Cadi finds the sin eater and persuades him to attempt to takes hers away, the sin eater asks her to promise to do him a favor in turn when the ceremony is complete, no matter the outcome. He asks her to go and see what the man of God has to say, and she is terrified, but knows she must keep her word. The movie presents her as going on her own after the sin-eating fails.

== References ==
 

== External links ==
*  

 
 
 
 
 
 