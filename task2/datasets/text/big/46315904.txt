Expiation (film)
{{Infobox film
| name           = Expiation 
| image          =
| caption        =
| director       = Sinclair Hill
| producer       = 
| writer         = E. Phillips Oppenheim (novel)   Sinclair Hill
| starring       = Ivy Close   Fred Raynham   Lionelle Howard 
| cinematography = 
| editing        = 
| studio         = Stoll Pictures 
| distributor    = Stoll Pictures
| released       = October 1922
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent crime film directed by Sinclair Hill and starring  Ivy Close, Fred Raynham and Lionelle Howard.  It was based on a novel by E. Phillips Oppenheim. The film was made by Stoll Pictures at the Cricklewood Studios.
 
==Cast==
*    Ivy Close as Eva Mornington  
* Fred Raynham as Cecil Braithwaite  
* Lionelle Howard as Godfrey Mornington  
* Malcolm Tod as Lord Dereham 
* Daisy Campbell as Mrs. Langton  
* Fred Rains as Mr. Woodruffe  

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
  
 
 

 