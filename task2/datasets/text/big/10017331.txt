My Suicidal Sweetheart
 
{{Infobox Film
| name           = My Suicidal Sweetheart
| image          = Mss_poster.jpg
| image_size     = 
| caption        = 
| director       = Michael Parness
| producer       = Michael Parness Elana Pianko Gene Raphael Miller Ron Brown G. Mac Brown
| writer         = Michael Parness
| narrator       = 
| starring       = Natasha Lyonne David Krumholtz Tim Blake Nelson Lorraine Bracco Rosanna Arquette David Paymer Michael Andrews
| cinematography = Horacio Marquínez
| editing        = Mark Livolsi
| distributor    = Full Glass Films
| released       = 
| runtime        = 115 minutes
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} American independent independent dark comedy film written and produced by Michael Parness.  The initial working title of the film was Saving Grace; this title was later changed to Max & Grace during production, and remained in place prior to its release on the film festival circuit in March 2005.  During an appearance at the 2006 Sacramento Film Festival, Parness remarked that he was dissatisfied with the title of Max & Grace and announced plans to change it to "My Suicidal Sweetheart".  The film has been released on DVD under the title "Crazy for Love"

==Background==
According to Filmmaker Magazine, "Michael Parness was on his fourth career when he stepped behind the camera to direct the comedy Max and Grace in New York ... Originally the proprietor of a thriving sports memorabilia business that supported his efforts as a playwright, theater director and screenwriter, he lost most of his savings in the October 98 stock market crash. Down to $33,000, he opened an online brokerage account and in 15 months turned it into $7 million; Parness launched financial guru site trendfund.com along the way and recounts his experiences in the 2002 bestseller, Rule the Freakin Markets. All that made him famous in financial circles and a regular in their media. Dustin Hoffmans Punch Productions even optioned his life story. But he still wanted to make a movie.  Its what Ive wanted to do my whole life, says Parness. Film allows you to take to the next level what you can do on the stage, like showing a characters inner thoughts or using special effects to highlight emotion."

In an interview with efilmcritic.com, Parness explained how the movie came to be:
 "I was busy teaching people how to Rule the Freakin Markets trading stocks and I got an email from someone who had seen one of my plays years earlier and wanted to produce something in L.A. Being the control freak I am, I reluctantly let him do so and in the process he asked if I had any screenplays. Max and Grace (then Saving Grace) was lying around and he loved it and his company loved it and they said they would put up money to make it. They ended up, like many, being full of BS, and the money fell through, but I decided I might as well go bankrupt to make it anyway, which is virtually what happened. Like many of my other writings, this was based on a broken relationship where I was always trying to save the woman I was with. Needless to say, I couldnt save her, but I did get a movie out of it!"
 

==Plot==

From the moment he was six, Max wanted to die. He tried shooting himself, hanging himself and even throwing himself out of a window. But somehow, he always managed to survive. Exasperated, his parents have had him committed to a mental institution, where miraculously hes found something worth living for. Her name is Grace; shes a wild mental patient whose death wish is even stronger than Maxs. But Max has a plan to save her. Theyre taking their marriage vows, til death do us part, and escaping the loony bin for the adventure of a new start. Max thinks Grace will be cured by visiting her mother, but he has to keep her alive until they get there. She tries overdosing on pills, running in front of a truck and jumping from a bridge, but luckily each time, Max comes to her rescue. Finally, he brings Grace to her mother and in a bizarre turn of events, the terminally ill oddball orders them to dig her grave. Mom says her goodbyes, but not before she tells Max and Grace that they both have something to live for—she sees a vision of their baby girl in Graces tummy. Its more than the hope Grace and Max have been searching for; its the wonderful beginning of a whole new life.

==Cast==
*Natasha Lyonne ... Grace 
*David Krumholtz ... Max 
*Tim Blake Nelson ... Doctor, Chief Nakahoma, Minister, Roger Bob 
*Lorraine Bracco ... Sheila 
*David Paymer ... Max, Sr. 
*Rosanna Arquette ... Vera 
*Karen Black ... Graces Mom 
*Ralf Möller ... Bruno  Guillermo Díaz ... Hector 
*Dave Attell ... Efram the driver 
*Emma Adele Galvin ... Sis 
*James Apaumut Fall ... Interpreter
*Dov Davidoff ... Doorman
*Michael Parness ... Peter Brown
*Dena Ferreira ... Wife
*Tom Shillue ... Orderly
*Ron Brown  ... O Sole Mio Singer
*Kimberly Anne Thompson ... Nurse
*Ross Babbitt ... Soldier Man (non-speaking)
*Charlotte Rose Bayer ... Baby Grace
*Zora ... The Cat

The main cast for "My Suicidal Sweetheart" was set in July 2002.  After reading the script, Tim Blake Nelson convinced the producers to let him assume several different roles—as a psychiatric doctor, Chief Nakahoma, a wrestling minister, and as a motivational speaker named Roger Bob.  Director Michael Parness popped into a small role as a character named Peter Brown, a participant at Roger Bobs outdoor support group.

Natasha Lyonne was the directors first choice for the role of Grace, and as soon as she signed on, agents began calling and roughly 25 male leads auditioned for the role of Max.  The producers at the time wanted a well-known actor for the part and so they chose Edward Furlong; unfortunately, due to personal issues and alleged drug and/or alcohol abuse, Furlong was cut from "Max & Grace" and another film called "Firecracker" and had to be replaced on both projects.  At Lyonnes prompting, Parness then cast actor David Krumholtz as Max in March 2003.  Lyonne and Krumholtz first worked together in the 1998 20th-Century Fox film "The Slums of Beverly Hills", in which they played Vivian and Ben Abromowitz (brother and sister).

==Post-Production==
According to a   dated March 12, 2005,   was responsible for the website (www.maxandgrace.com),   and promotional pieces for "Max & Grace." This was the companys first time handling a motion picture project.

==Film festival showings==
The film was first shown at the South by Southwest (SXSW) Film Festival in Austin, Texas.  It was screened on Saturday, March 12 and Tuesday, March 25, 2005 at 1:30 p.m. at the Paramount Theatre, and also on Thursday, March 17, 2005 at 7 p.m. at the Alamo Downtown.  Other festival appearances for the film included:
*  (Newport Beach, CA, USA; April 23, 2005)
*Ashdod International Film Festival (Ashdod, Israel; April 26, 2005)
*Seattle International Film Festival (Seattle, WA, USA; June 9, 2005) Rhode Island International Film Festival (Providence, RI, USA; August 13, 2005)
*  (Toronto, Canada; September 7, 2005)
*  (San Diego, CA, USA; September 23, 2005)
*Sidewalk Moving Picture Festival (Birmingham, AL, USA; September 24, 2005)
*  (Livermore, CA, USA; October 30, 2005)
*Nolita Film Festival (New York, NY, USA; December 4, 2005)
*  (Washington, D.C., USA; March 4, 2006)
*  (Tiburon, CA; USA; March 11 & 16, 2006)
*  (West Point, MS, USA; March 16, 2006)
*  (Sacramento, CA, USA; April 1, 2006)
*  (Sarasota, FL, USA; April 14, 2006)
*Oxford International Festival of Films (Oxford, UK; May 12, 2006)
*  (Trenton, NJ, USA; May 15, 2006)
*  (Waikoloa, HI, USA; May 18, 2006)

At many of the festival appearances, the cast and crew of the film also participated in conferences and question-and-answer sessions with the audience members.  Both David Krumholtz and Michael Parness were present for the SXSW Film Festival.

==Awards and achievements==
*  - Outstanding Achievement in Filmmaking: Acting (David Krumholtz)
*Seattle International Film Festival - Official Selection Rhode Island International Film Festival - Best Feature Film (tie)
*  - Official Selection
*  - two "Slate" Awards: Best Actor (David Krumholtz, Best Score
*  - Best Film
*  - Official Selection
*  - Best Film, Best Actress (Natasha Lyonne), Best Supporting Actor

==Title Change==
Director  -based film acquisition, finance, production and distribution company) acquired worldwide theater, television and home video/DVD distribution rights to the film.  Changes to the title subsequently resulted in changes to the films   in 2006, and the   carried by the Full Glass Films website.

Initially, the film was set for limited release during Fall 2006 for three major cities: Toronto, New York and Los Angeles.  This was to be followed by a home video/DVD release, but the limited-release runs never took place.   The film has been released as "Crazy for Love" on DVD as of 06/10/08.

==External links==
*  (formerly "maxandgrace.com," relating to the films old title)
*  at Full Glass Films website
*  at Full Glass Films website
*  - includes dates of screening
*  - early version of "My Suicidal Sweetheart" trailer
*  
*  

 
 
 
 