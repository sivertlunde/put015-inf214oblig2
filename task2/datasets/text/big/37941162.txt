List of Malayalam films of 1969
 

The following is a list of Malayalam films released in 1969.
{| class="wikitable"
|- style="background:#000;"
! colspan=2 | Opening !! Sl. No. !!Film !! Cast !! Director !! Music Director !! Notes
|-
| rowspan="4" style="text-align:center; background:#ffa07a; textcolor:#000;"| J A N 
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 3
|valign="center" |  1 
| Anaachadanam || Prem Nazir, Sheela || M. Krishnan Nair (director)|M. Krishnan Nair || G. Devarajan ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 10
|valign="center" |  2 
| Padichakallan || Prem Nazir, Adoor Bhasi || M. Krishnan Nair (director)|M. Krishnan Nair || G. Devarajan ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 24
|valign="center" |  3  Madhu || P. Venu || G. Devarajan ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 26
|valign="center" |  4  Madhu || A. Vincent || A. T. Ummer ||
|-
| rowspan="4" style="text-align:center; background:#dcc7df; textcolor:#000;"| F E B 
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 6
|valign="center" |  5  Sharada || P. Bhaskaran || G. Devarajan ||
|-
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 20
|valign="center" |  6  Janmabhoomi || Madhu || John Sankaramangalam || B. A. Chidambaranath ||
|-
| rowspan="2"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 27
|valign="center" |  7 
| Ballatha Pahayan || Prem Nazir, Jayabharathi || T. S. Muthaiah || KV Job ||
|-
|valign="center" |  8  Khadeeja || R Velappan Nair || BA Chidambaranath ||
|-
| rowspan="2" style="text-align:center; background:#d0f0c0; textcolor:#000;"| M A R 
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 14
|valign="center" |  9 
| Mr. Kerala || Internet Movie Database  || G Viswanath || Vijayakrishnamoorthy ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 20
|valign="center" |  10  Rahasyam || Prem Nazir, Sheela || J. Sasikumar || BA Chidambaranath ||
|-
| rowspan="2" style="text-align:center; background:#ffa07a; textcolor:#000;"| A P R 
| rowspan="2"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 5
|valign="center" |  11  Sharada || Kunchacko || G. Devarajan ||
|-
|valign="center" |  12  Sathyan || KS Sethumadhavan || G. Devarajan ||
|-
| rowspan="1" style="text-align:center; background:#dcc7df; textcolor:#000;"| M A Y 
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 16
|valign="center" |  13 
| Kannoor Deluxe || Prem Nazir, Adoor Bhasi || AB Raj || V. Dakshinamoorthy ||
|-
| rowspan="1" style="text-align:center; background:#d0f0c0; textcolor:#000;"| J U N 
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 20
|valign="center" |  14  Nurse || Thikkurissi || Thikkurissi || MB Sreenivasan ||
|-
| rowspan="3" style="text-align:center; background:#ffa07a; textcolor:#000;"| J U L 
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 10
|valign="center" |  15 
| Ardharathiri || Internet Movie Database  || Sambasiva Rao ||  ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 20
|valign="center" |  16  Sandhya || Sharada || Dr. Vasan || MS Baburaj ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 25
|valign="center" |  17  Sathyan || K. S. Sethumadhavan || Devarajan ||
|-
| rowspan="5" style="text-align:center; background:#dcc7df; textcolor:#000;"| A U G 
| rowspan="2"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 15
|valign="center" |  18  Madhu || MA Rajendran || Pukazhenthi ||
|-
|valign="center" |  19  Sathyan || P. Bhaskaran || G. Devarajan ||
|-
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 22
|valign="center" |  20  Madhu || P. Bhaskaran || K. Raghavan ||
|-
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 23
|valign="center" |  21  Madhu || AK Sahadevan || Jaya Vijaya ||
|-
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 26
|valign="center" |  22  Jwala || Prem Nazir, Sheela || M. Krishnan Nair (director)|M. Krishnan Nair || G. Devarajan ||
|-
| rowspan="2" style="text-align:center; background:#d0f0c0; textcolor:#000;"| S E P 
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 17
|valign="center" |  23 
| Poojapushpam || Prem Nazir, Sheela || Thikkurissi Sukumaran Nair || V. Dakshinamoorthy ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 19
|valign="center" |  24  Sathyan || M. S. Mani (editor)|M. S. Mani|| A. T. Ummer ||
|-
| rowspan="4" style="text-align:center; background:#ffa07a; textcolor:#000;"| O C T 
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 8
|valign="center" |  25 
| Chattambikkavala || Sathyan (actor)|Sathyan, Srividya || N Sankaran Nair || BA Chidambaranath ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 14
|valign="center" |  26 
| Urangatha Sundary || Sathyan (actor)|Sathyan, Rajasree || P. Subramaniam || G. Devarajan ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 24
|valign="center" |  27  Sharada || A. Vincent || G. Devarajan ||
|-
| rowspan="1"  style="text-align:center; background:#ffdacc; textcolor:#000;"| 31
|valign="center" |  28  Madhu || MM Nesan || MS Baburaj ||
|-
| rowspan="2" style="text-align:center; background:#dcc7df; textcolor:#000;"| N O V 
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 21
|valign="center" |  29  Sadhana || AB Raj || V. Dakshinamoorthy ||
|-
| rowspan="1"  style="text-align:center; background:#ede3ef; textcolor:#000;"| 28
|valign="center" |  30  Sathyan || KS Sethumadhavan || G. Devarajan ||
|-
| rowspan="3" style="text-align:center; background:#d0f0c0; textcolor:#000;"| D E C 
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 10
|valign="center" |  31  Madhu || P. Venu || MS Baburaj ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 18
|valign="center" |  32  Rest House || Prem Nazir, Sheela || J. Sasikumar || M. K. Arjunan ||
|-
| rowspan="1"  style="text-align:center; background:#edefff; textcolor:#000;"| 26
|valign="center" |  33  Padmini || P. Subramaniam || G. Devarajan ||
|}

 
 
 

 
 
 
 
 