The Nebraskan
{{Infobox film
| name           = The Nebraskan
| image          =File:The Nebraskan 1953 3-D film poster.jpg
| image_size     =
| alt            =
| caption        =
| director       = Fred F. Sears
| producer       = Wallace MacDonald
| writer         = David Lang, Martin Berkeley
| narrator       = Phil Carey Roberta Haynes
| music          = Ross DiMaggio
| cinematography = Henry Freulich Al Clark, James Sweeney
| studio         =  Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 68 min
| country        =   English
| budget         =
| gross          =

}} 1953 3-D Western film Phil Carey and Roberta Haynes.  The Nebraskan was one of six feature films from prolific director Fred Sears that were released that year. 

==Plot==
 Phil Carey) attempts to make peace with the Sioux Indians, who demand the handover of Wingfoot(Maurice Jara), an Indian scout who is believed to be responsible for the murder of their chief Thundercloud.  While being held in the guardhouse at Fort Kearny, Wingfoot escapes with Reno(Lee Van Cleef),an army private awaiting trial for murder.       


==Cast==

*Philip Carey as     Wade Harper
*Roberta Haynes as   Paris
*Wallace Ford as McBride Richard Webb as Ace Eliot
*Lee Van Cleef as Reno
*Maurice Jara as Wingfoot
*Regis Toomey as Col. Markham
*Jay Silverheels as Spotted Bear
*Pat Hogan as Yellow Knife
*Dennis Weaver as Capt. DeWitt
*Boyd "Red" Morgan as Sgt. Phillips

==References==
 

==External links==
*  
*  
*    at American Film Institute
*  

 
 
 
 
 
 
 
 
 
 