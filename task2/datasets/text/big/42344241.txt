Storm Over Bengal
{{Infobox film 
 | name =Storm Over Bengal
 | image = File:Stormbengal.jpg
 | caption = Film poster
 | director = Sidney Salkow
 | producer = Armand Schaefer
 | writer = Garrett Fort Richard Cromwell Rochelle Hudson
 | music = Cy Feuer (uncredited) William Lava (uncredited) Ernest Miller William Morgan
 | distributor = Republic Pictures
 | released =  
 | runtime = 65 minutes
 | language = English
 | country = United States
 | budget = 
}}
 Best Score, Lives of a Bengal Lancer.

==Plot== Flt Lt Hallett and Flying Officer|F/O Neil Allison visit a British outpost in India. Neil is the younger brother of Army Intelligence Officer Captain Jeffrey Allison who is away on an undercover mission in the unaligned and independent Princely state of Lhanapur. The British are worried about radio broadcasts from Ramin Khan inciting the Indian people to revolt.  Upon the death of the elderly Maharajah of Lhanapur, Ramin Khan schemes to usurp the throne as a base for his revolt.  

Disguised as an Indian holy man, Jeffrey gathers intelligence on Ramin Khans insurgents whilst the British send a diplomatic mission to Lhanapur who are ambushed and killed.  The young Neil is himself in love with Jeffreys fiancee Joan Lattimore and is jealous of his brother.  Upon his return from Lhanapur Jeffrey postpones his wedding so Joan can flee to safety.  With the death of the diplomatic party Jeffrey is flown to Lhanapur by Hallett in his aircraft to meet with the Maharajah. Ramin Khan captures Jeffrey and mortally wounds Hallett who flies back to inform the British of Ramin Khans activity.  Ramin Khan is delighted as he plans to ambush the British field force in a ravine near the caves of Kali.

==Cast==

*Patric Knowles as Capt. Jeffrey Allison Richard Cromwell as Flying Officer Neil Allison 
*Rochelle Hudson as Joan Lattimore
*Douglass Dumbrille as Ramin Khan 
*Colin Tapley as Flight Lieutenant Hallett  
*Gilbert Emery as Colonel Torrance  Douglas Walton as Terry
*Halliwell Hobbes as Sir John Galt  John Burton as Captain Carter Clyde Cook as Alf
*Claud Allister as Redding 
*Pedro de Cordoba as Abdul Mir 
*Edward Van Sloan as Maharajah of Lhanapur

==References==
 

== External links ==
*  
* 
*  

 
 
 
 
 
 
 
 
 
 
 
 

 