Fire and Sword
 
{{Infobox film
| name           = Fire and Sword
| image          = 
| alt            =  
| caption        = 
| director       =  
| producer       = 
| based on       = 
| screenplay     =  
| starring       = Christoph Waltz Antonia Preser Peter Firth Leigh Lawson
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 
| runtime        = 
| country        = West Germany
| language       = German
| budget         = 
| gross          = }}

Fire and Sword (Feuer und Schwert – Die Legende von Tristan und Isolde) is a 1982 German romance/adventure film, directed by  . It is based on the legend of Tristan and Iseult.

The film won the Caixa de Catalunya in Best Cinematography and was nominated for the International Fantasy Film Award for Best Film.

==Cast==
*Christoph Waltz – Tristan
*Antonia Preser – Isolde
*Peter Firth – Dinas
*Leigh Lawson – King Mark
*Brendan Cauldwell – 1st Baron
*Walo Lüönd – Gorvenal
*Kurt Raab – Ganelon
*Vladek Sheybal – Andret

==External links==
* 

 

 
 
 

 