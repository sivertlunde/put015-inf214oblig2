Koi Sugata Kitsune Goten
{{Infobox Film
| name           = Koi Sugata Kitsune Goten
| image          = Koi sugata kitsune goten poster.jpg
| image_size     = 
| caption        = Original Japanese movie poster
| director       = Nobuo Nakagawa
| producer       = Takarazuka Eiga Company Ltd. Sadao Sugihara (producer)
| writer         = Hideji Hōjō (writer) Ryozo Kasahara (writer)
| narrator       = 
| starring       = 
| music          = Masao Yoneyama
| cinematography = Kozo Okazaki
| editing        = 
| distributor    = Toho
| released       = May 17, 1956 (Japan) 
| runtime        = 90 min.
| country        = Japan
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Japanese film directed by Nobuo Nakagawa.

== Cast ==
* Hibari Misora as Maruya, Tomone
* Haruhisa Kawada as Akinobu
* Senjaku Nakamura
* Chieko Naniwa as Okon
* Chikage Oogi as Akemi
* Shunji Sakai as Heihachi
* Kyu Sazanka as Fujimaru
* Eijirō Yanagi

==References==
 

==External links==
* http://www.imdb.com/title/tt0330531/

 
 
 
 


 