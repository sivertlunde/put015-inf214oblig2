Like a Star Shining in the Night
{{Infobox film
| name           = Like a Star Shining in the Night
| image          = 
| caption        = 
| director       = René Féret
| producer       = René Féret Fabienne Camara  
| writer         = René Féret 
| starring       = Salomé Stévenin   Nicolas Giraud
| music          = Juan Guillermo Dumay 
| cinematography = Benjamín Echazarreta 
| editing        = Fabienne Camara 
| studio         = Les Films Alyne
| distributor    = JML Distribution 
| released       =  
| runtime        = 89 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

Like a Star Shining in the Night ( ) is a 2008 French drama film written, produced and directed by René Féret. 

== Cast ==
* Salomé Stévenin as Anne  
* Nicolas Giraud as Marc   
* Jean-François Stévenin as Le père dAnne 
* Marilyne Canto as Dr. Camille Bamberger 
* Aurélia Petit as Aurélie 
* Guillaume Verdier as Eric 
* Sabrina Seyvecou as Sabine   
* Yves Reynaud as Le père de Marc    
* Claire Stévenin as La mère dAnne 
* Caroline Loeb as La mère de Marc  
* Julien Féret as Antoine 
* René Féret as Le chef de Marc et Eric

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 


 