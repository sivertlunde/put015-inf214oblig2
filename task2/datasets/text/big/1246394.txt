Mary Shelley's Frankenstein (film)
 
{{Infobox film
| name           = Frankenstein
| image          = Mary_shelleys_frankenstein_ver2.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Kenneth Branagh   
| producer       = Francis Ford Coppola
| screenplay     = Steph Lady Frank Darabont
| based on       =  
| starring       = Robert De Niro Kenneth Branagh Tom Hulce Helena Bonham Carter Ian Holm John Cleese Aidan Quinn
| music          = Patrick Doyle  Roger Pratt
| editing        = Andrew Marcus Japan Satellite Broadcasting
| distributor    = TriStar Pictures
| released       =  
| runtime        = 123 minutes  
| country        = United States Japan
| language       = English
| budget         = $45 million
| gross          = $112,006,296  	 
}}
Mary Shelleys Frankenstein is a 1994 American horror film directed by Kenneth Branagh and starring Robert De Niro and Branagh. The picture was produced on a budget of $45 million and is considered the most faithful film adaptation of Mary Shelleys novel Frankenstein|Frankenstein; or, The Modern Prometheus. 

==Plot==
The film opens with a few words by Mary Shelley:
 

The story begins in the year 1794. Captain Walton is leading a daring, but troubled, expedition to reach the North Pole, even as his crew is threatening to mutiny. While their ship is trapped in the ice of the Arctic Sea, Walton and his crew discover a man traveling across the Arctic on his own. In the distance, a loud moaning can be heard. When the man sees how obsessed Walton is with reaching the North Pole, he asks, "Do you share my madness?" The man then reveals that his name is Victor Frankenstein and begins his tale.
 flashback to Victors childhood in Geneva as the son of the wealthy Baron and Caroline Frankenstein. He grows up with his adopted sister, Elizabeth Lavenza, who becomes the love of his life. Years later, Caroline dies giving birth to his brother William. Sometime before going off to the university at Ingolstadt, a grief-stricken Victor vows on his mothers grave that he will find a way to conquer death. On the night of his graduation, Victor and Elizabeth promise to marry when Victor returns from his studies. At university, Victors previous studies with the works of alchemists such as Paracelsus, Albertus Magnus, and Cornelius Agrippa make him unpopular with certain professors. However, he finds a friend in Henry Clerval and a mentor in Professor Waldman. Victor comes to believe that the only way to cheat death is to create life. Professor Waldman warns Victor not to follow through with his theory; he tested it once, but ended his experiments because they resulted in an "abomination".

While performing vaccinations, Waldman is killed by a patient who thinks the doctors are trying to poison him. After Waldman is buried, Victor breaks into the professors laboratory, takes his notes, then begins to work on his own creation. Victor spends months working on creating a living, breathing creature, giving it dead body parts from various sources, including the body of the murderer, Waldmans own brain, a new leg in place of the missing one and a new eye to replace the damaged one. He is so obsessed with his work that not even a cholera outbreak tears him from it. Late one night, Victor finally gives his creation life, but he recoils from it in horror and renounces his experiments.

That night, the creature escapes, running off to the wilderness. He spends months hiding in the woods, living in an unwitting familys barn. As time progresses, the creature learns to read and speak, and even tries to win the familys love, but his efforts are in vain. Through a journal that the creature finds in the coat that he fled with, he learns of the circumstances of his creation and that Victor Frankenstein is responsible. He then burns down the familys abandoned cottage and heads to Geneva, vowing revenge on his creator.

Victor, who believes his creation is destroyed, returns to Geneva with the intent of marrying Elizabeth. He finds there that his little brother William (Ryan Smith) has been murdered. Justine Moritz, a servant of the Frankenstein household, is framed for the crime and hanged by a lynch mob. That night Victor is approached by his creation, who tells him to meet him on the mountain. Realizing that the creature murdered his brother, Victor goes with the intent of destroying his creation, but is no match for his enhanced speed and strength. The creature asks who the people were that Victor used to build him. Victor replies that they were merely raw materials, but the creature replies that he knew how to read, speak and play the recorder; he didnt learn how to, he remembered how.

Rather than killing his creator, the creature insists that Victor make a bride for him. If he does this, then he promises to disappear with his mate forever. To ensure his familys safety, Victor begins gathering the tools he used to create life, but when the creature insists he use Justines body to make the bride, Victor breaks his promise. Enraged, the creature once more vows revenge, saying, "If you deny me my wedding night, I will be with you on yours!"

Victor and Elizabeth are married. En route to their honeymoon, Victor and Elizabeth are flanked by body guards. Meanwhile, Victors father dies while the creature watches over him. That night Victor takes every precaution to defend his new family, but the creature finds them anyway and gains access to Elizabeths room. He uses his hand to prevent her crying out, then despite her pleas he kills her by ripping out her heart as Victor searches the house. Victor comes back to find the monster holding Elizabeths heart saying, "I keep my promises." He tosses Elizabeths body off the bed; her head slams into a nearby table, and her hair is set aflame by candles there. The creature flees out of the window amidst gunfire. Victor frantically extinguishes the fire.
 setting herself on fire, in the process burning the mansion to the ground.

The story returns to the Arctic Circle. Victor tells Walton that he has been pursuing his creation for months with the intent of killing him. Soon after relating his story, Victor succumbs to pneumonia and dies. After a word with his crew, Walton hears a noise coming from the room he left Frankensteins body in. There they find the creature weeping over his creators dead body. They take Frankensteins body and prepare a funeral pyre for him. The ceremony is interrupted when the ice around the ship begins to crack. The creature takes the torch and finishes the ceremony, burning himself alive with his creators body. Walton, having seen the result of Frankensteins obsession, puts his own aside and orders the ship to return home.

==Cast==
  The Creation, the product of an experiment with corpses and electricity. De Niro also portrays Professor Waldmans killer.
* Kenneth Branagh as Victor Frankenstein, a scientist obsessed with conquering death.
** Rory Jennings as young Victor
* Tom Hulce as Henry Clerval, Dr. Frankensteins best friend from medical school.
* Helena Bonham Carter as Elizabeth, Frankensteins fiancée and adoptive sister.  Hannah Taylor Gordon as young Elizabeth
* Ian Holm as Baron Frankenstein, Victor Frankensteins father.
* John Cleese as Professor Waldman, Frankensteins tutor and colleague who shares his interest in creating life. His brain is later used for the creature.
* Aidan Quinn as Captain Robert Walton, the commander of the ship which picks up Frankenstein in the Arctic Circle.
* Richard Briers as Grandfather, an elderly blind man who is kind to the Creation.
* Robert Hardy as Professor Krempe, a university tutor of medical sciences who condemns Frankensteins theories of life beyond death.
* Trevyn McDowell as Justine Moritz, a worker in the Frankenstein household who is close friends with Elizabeth.
** Christina Cuttall as young Justine
* Celia Imrie as Mrs. Moritz, the head servant in the household who often fights with Justine.
* Cherie Lunghi as Caroline Frankenstein, Victors mother who dies during the birth of his younger brother, William.
* Ryan Smith as William Frankenstein, Victors younger brother.
** Charles Wyn-Davies as young William Richard Bonneville as Schiller
* Jenny Galloway as Vendors wife
* Patrick Doyle (uncredited) as Ballroom orchestra conductor Alex Lowe as Crewman
* Stuart Hazeldine (uncredited) as Man in crowd scene
* Fay Ripley (deleted scenes) as Whore
 

==Reception==

===Critical response===
Critical reviews were mixed; the film currently holds a 40% "Rotten" rating on Rotten Tomatoes based on 40 reviews with the consensus: "Mary Shelleys Frankenstein is ambitious and visually striking, but the overwrought tone and lack of scares make for a tonally inconsistent experience". 

  wrote, "Branagh is in over his head. He displays neither the technical finesse to handle a big, visually ambitious film nor the insight to develop a stirring new version of this story. Instead, this is a bland, no-fault Frankenstein for the 90s, short on villainy but loaded with the tragically misunderstood. Even the Creature (Robert De Niro), an aesthetically challenged loner with a father who rejected him, would make a dandy guest on any daytime television talk show." 

Conversely,  , and the director likely attempted more than is practical for a two-hour film, but overambition is preferable to the alternative, especially if it results — as in this case — in something more substantial than Hollywoods typical, fitfully entertaining fluff." 

===Box office===
The film did disappointing business upon its U.S. theatrical release, grossing only $22 million, but did well in global markets where it grossed $90 million.  

===Accolades===
{| class="wikitable plainrowheaders" style="font-size: 95%;"
|- Award
! Category
! Recipients and nominees Result
|-
| Academy Awards Best Makeup
| Daniel Parker, Paul Engelen, Carol Hemming
|  
|-
| British Academy Film Awards Best Production Design Tim Harvey
|  
|-
| rowspan=7 | Saturn Awards Saturn Award Best Horror/Thriller Film
|  
|- Best Actor
| Kenneth Branagh
|  
|- Best Actress
| Helena Bonham Carter
|  
|- Best Make-up
| Daniel Parker, Paul Engelen
|  
|- Best Music
| Patrick Doyle
|  
|- Best Supporting Actor
| Robert De Niro
|  
|- Best Writing
| Steph Lady, Frank Darabont
|  
|-
|}

==Other media==
  Super Nintendo and Sega Genesis game (the latter of which was by Sony Imagesoft), following a Platform game|platform-style format. A Sega CD game was also produced by the same company that had a more Adventure game|adventure-based format that would sometimes switch to a fighting game.

==See also==
* Frankenstein in popular culture|Frankenstein in popular culture
* List of films featuring Frankensteins monster

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 