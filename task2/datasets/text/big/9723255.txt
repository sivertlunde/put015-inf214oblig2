Out of the Dark (1989 film)
{{Infobox film
| name           = Out of the Dark
| image          =
| image_size     =
| caption        = Michael Schroeder
| producer       = Zane W. Levitt David C. Thomas Paul Bartel
| writer         = J. Greg De Felice Zane W. Levitt
| narrator       = Geoffrey Lewis Divine Cameron Dye
| music          = Paul Antonelli David Wheatley
| cinematography = Julio Macat
| editing        = Mark S. Manos
| distributor    = Cinetel Films
| released       =  
| runtime        = 89 min
| country        = United States
| language       = English
| budget         = $1,600,000 (estimated)
| gross          = $419,428 (USA)
}}
 1989 starring Karen Witter. The film is notable for being Divine (actor)|Divines last movie.

==Plot==
"Suite Nothings" is a sleazy L.A. phone-sex hot-line voiced by failed models.  Preying on them is Bobo, a psychotic killer who dresses in a clown costume.  While the police try to uncover the clowns identity, the agencys models fall to Bobos murderous rage.

==Home Video Release==

"Out of the Dark" was released on VHS and Laserdisc in 1989 by RCA/Columbia Pictures home video.  Sony Pictures released the film on a manufactured-on-demand DVD-R of the film on March 1, 2011. The film ran on FearNet in 2009.

==Cast==
*Karen Black as Ruth Wilson
*Bud Cort as Doug Stringer Geoffrey Lewis as Dennis
*Tracey Walter as Lt. Frank Meyers Divine as Det. Langella
*Cameron Dye as Kevin Silvers
*Lainie Kazan as Hooker Nancy
*Karen Witter as Jo Ann
*Karen Mayo-Chandler as Barbara
*Tab Hunter as Driver
*Paul Bartel as Hotel Clerk

== External links ==
* 
* 

 
 
 
 
 


 