Edison, the Man
{{Infobox film
| name           = Edison, the Man
| image	     = Edison, the Man FilmPoster.jpeg
| image_size     =
| alt            =
| caption        =
| director       = Clarence Brown
| producer       = John W. Considine Jr.
| writer         = Talbot Jennings Bradbury Foote Dore Schary Hugo Butler
| narrator       =
| starring       = Spencer Tracy Rita Johnson
| music          = Herbert Stothart
| cinematography = Harold Rosson
| editing        = Fredrick Y. Smith
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = $893,000  . 
| gross          = $1,787,000 
}}
 Academy Award for Best Writing, Original Story for their work on this film.  However, much of the films script fictionalizes or exaggerates the real events of Edisons life. 

==Plot==
In 1869, anxious to be more than a tramp telegraph operator, Edison (Spencer Tracy) travels to New York at the prompting of an old friend, Bunt Cavatt (Lynne Overman). He goes to work for Mr. Els (Henry Travers).  He tries to persuade financier Mr. Taggart (Gene Lockhart) to fund the development of his inventions, but Taggart has no interest in financing “green electrical workers”. However, General Powell (Charles Coburn), the president of Western Union, does.
 Menlo Park.  In the next few years, he perfects the phonograph with his devoted staff.

Trouble arises when Bunt brags to reporters that Edison has invented the electric light.  Since he hasnt yet, he is condemned by the scientific community (encouraged by Taggart, whose gas stocks are threatened by the announcement). Edison “leaves science behind”, and with a Herculean trial-and-error effort, finally succeeds in inventing a practical electric light.  His subsequent plans to light New York are again hindered by Taggart, who arranges it so that Edison is only given six months to complete the entire task. Nevertheless, Edison finishes the job just in time.

==Cast==
 
* Spencer Tracy as Thomas Alva Edison
* Rita Johnson as Mary Stillwell
* Charles Coburn as General Powell
* Gene Lockhart as Mr. Taggart
* Henry Travers as Ben Els
* Lynne Overman as James J. Bunt Cavatt
* Felix Bressart as Michael Simon, Edisons assistant
* Gene Reynolds as Jimmy Price
* Byron Foulger as Edwin Hall Peter Godfrey as Bob Ashton
* Guy DEnnery as Lundstrom
* Milton Parsons as Acid Graham

==Quotes==
“I’m an inventor. I can’t be told what to do. I’ve got to do the things I want to do. I work with ideas, visionary things. Nobody—not even I—knows how useful they’re going to be or how profitable until I had a chance to work them out in my own way.”

“You think you’re nothing but wood and metal and glass. But you’re not: you’re dreams and hard work and heart. You’d better not disappoint us.”

“It’s not the money wrapped up in the laboratory, it’s the lives wrapped up in the laboratory.  It’s come to mean everything that I ever set out to do. It means a weekly paycheck for all my men. It means home, shelter, clothing, and food for lots of families.”

“He hasn’t got a darn thing but I like to hear him talk that way.”

==Box office==
According to MGM records the film earned $1,152,000 in the US and Canada and $635,000 elsewhere resulting in a profit of $143,000. 

==References==
 

==External links==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 