The Gift Supreme
 
{{Infobox film
| name           = The Gift Supreme
| image          = 
| image size     = 
| caption        = 
| director       = Ollie L. Sellers 
| writer         = Ollie L. Sellers
| based on       =  
| narrator       =  Bernard Durning Seena Owen Tully Marshall
| music          = 
| cinematography = Jack MacKenzie
| editing        = 
| studio         = 
| distributor    = Republic Distributing Corporation
| released       =  
| runtime        = 90 mins.
| country        = United States
| language       = Silent English intertitles
| budget         = 
| gross          =
}}
 silent drama Bernard Durning Jack Curtis, Anna Dodge, Claire McDowell and Lon Chaney in a villainous bit role.  

==Synopsis==
The plot involves a son (Durning) being disinherited by his father (MacDowell) for refusing to end his friendship with a beautiful young woman (Owen) from the slums of London.

==Cast== Bernard Durning as Bradford Chandler Vinton 
* Seena Owen as Sylvia Alden
* Melbourne MacDowell as Eliot Vinton
* Tully Marshall as Irving Stagg
* Eugenie Besserer as Martha Vinton
* Lon Chaney as Merney Stagg Jack Curtis as Muggs Rafferty
* Anna Dodge as Mrs. Wesson
* Claire McDowell as Lalia Graun

==Status== Kino produced 1995 documentary Lon Chaney: Behind the Mask. 

==Notes==
 

==References==
*  
*  
*  

==External links==
*  

 
 
 
 
 
 
 
 
 
 

 