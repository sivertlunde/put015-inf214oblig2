Extermineitors IV: Como hermanos gemelos
{{Infobox film name              = Extermineitors IV: Como hermanos gemelos image             = Extermineitors IVdvdcover.jpg caption           = DVD cover image_size        = 200px director          = Carlos Galettini producer          = Carlos Galettini production design = Aldo Guglielmone writer            = Salvador Valverde Calvo Salvador Valverde Freire starring          = Guillermo Francella Aldo Barbero Rand McClain Javier Belgeri Mike Kirton music             = Lalo Fransen cinematography    = Héctor Collodoro Carlos Torlaschi editing           = Serafín Molina studio            = Argentina Sono Film distributor       = Argentina Sono Film released          = 23 January 1992 runtime           = 90 min. country           = Argentina language          = Spanish budget            = preceded_ by      =  
}} comedy action DeVito comedy Twins (1988 film)|Twins. It premiered on 23 January 1992 in Buenos Aires.

== Synopsis ==
The leader of the Extermineitors task force, Colonel William, has discovered that the old enemy of the organization, Mc Clain, has left crime and lives quietly in Iguazu Falls. William wants Mc Clain to work for him and on the right side of the law. He develops a plan for his top man, Guillermo, to perform the mission of recruiting Mc Clain, by making him believe that Mc Clain is his long lost twin brother.  From then on, Guillermo, and Mc Clain become friends and fight together against the old criminal organization of ninja robbers, now leadered by the vicious criminal known as The Albino.

== Cast ==
* Guillermo Francella ... Guillermo
* Aldo Barbero ... Colonel William
* Rand McClain ... Randolf Mc Clain (as Randolf Mc Clain)
* Javier Belgeri ... Nico
* Mike Kirton ... The Albino
* Verónica Varano ... Ana
* Marcela Labarca
* Charlie Nieto
* Jorge Montejo ... Paolo el Rockero
* Riki Maravilla ... Himself
* Oscar Roy
* Alejandra Pradon
* Poupee Pradon
* Jorge Rial
* Valeria Britos ... Sol

== References ==
 

== External links ==
*  

 
 
 
 


 
 