Outlaw Trail: The Treasure of Butch Cassidy
{{Infobox film
| name           = Outlaw Trail: The Treasure of Butch Cassidy 
| image          = Outlaw Trail DVD (2008).jpg
| caption        = 2008 DVD cover 
| director       = Ryan Little
| producer       = Ryan Little Adam Abel
| writer         = David Pliler
| starring = Ryan Kelley Arielle Kebbel Dan Byrd Brent Weber Bruce McGill James Gammon Shauna Thompson James Karen Rick Macy Brian Peck Ron Melendez
| music          = Jay Bateman
| cinematography = T. C. Christensen Geno Salvatori   
| editing        = John Lyde
| studio         = 
| distributor    = 
| released       = 2006
| runtime        = 87 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by  =  
| followed_by  =  
}}

Outlaw Trail: The Treasure of Butch Cassidy is a 2006 American adventure film, produced and directed by Ryan Little.    It is loosely based on legends arising from the fate of real-life Western outlaw Butch Cassidy, the alias of Robert LeRoy Parker, whose gang robbed trains and banks in the 1890s. Cassidy fled to South America in 1901, where he is believed to have died in 1908.

The film continues the Butch Cassidy saga beyond the ending of the 1969 Robert Redford–Paul Newman hit film, Butch Cassidy and the Sundance Kid, in 1908. The main protagonist, Cassidys teenage grand-nephew, Roy Parker, sets out in 1951 to find the treasure he believes Cassidy left behind in Utah. Overcoming his unforgiving grandfathers opposition, Roy and friends are involved in several chase scenes, pursued by criminals.  

The film was produced on location in Utah by GO Films from a screenplay by David Pliler. The music score was by Jay Bateman. The film stars Ryan Kelley, Arielle Kebbel, Dan Byrd, and Brent Weber, and features Bruce McGill and James Gammon. The film was released in North America in 2006 and is available on DVD.

==Plot== engraved with a treasure map. Cassidy and his accomplice, Harry Longabaugh (the "Sundance Kid"), attempt to elude capture when their hideout is surrounded by Bolivian police.

The film then shifts to Circleville, Utah, in 1951, where Butch Cassidys 16-year old great-nephew, Roy Parker, defends his infamous ancestors reputation despite the opposition of Sam, his stern grandfather, Cassidys younger brother. Sam resents Roys interest in Cassidy, even acquiescing in the boys brief jailing on a trumped-up charge, where he scolds him, "I spent the better part of my life trying to live down the reputation that your hero has laid out for the Parker name and you grow up worshipping him."  Young Roy believes that Cassidy was trying to make amends by returning to the U.S. from Bolivia. He discovers the belt buckle left by Cassidy and learns that it is a map to the treasure buried by Cassidy somewhere in the Utah wilderness.

After Roy is rescued from jail by Jess, his best friend and fellow Boy Scout in Circleville Troop&nbsp;14, they embark on a quest for the treasure. Joining the two in the hunt are Ellie, with whom Roy is becoming infatuated, and Martin, who reluctantly goes along after Roy takes his truck during the jailbreak. The four are pursued by Garrison, a corrupt museum official who will stop at nothing to get the treasure for himself. There are automobile chases, a desperate escape down a river in a raft without paddles, jumping onto the roof of a moving train, Roy and Ellie tied up and gagged, and even a harrowing biplane ride, interspersed by gun fights, as the undaunted youths vie to find Butch Cassidys treasure first.  Roy also embarks on a personal mission to learn the truth about his infamous ancestors death. 

==Cast== backlighting by cinematographer T. C. Christensen imparts a halo effect]]

*Ryan Kelley as "Roy Parker": The 16-year old great-nephew of Butch Cassidy

*Arielle Kebbel as "Ellie": Roy Parkers romantic interest.

*Dan Byrd as "Jess", Roys best friend and fellow Boy Scout in Circleville Troop&nbsp;14, whose outdoor survival skills help the quartet of treasure hunters overcome various wilderness challenges in their search for gold.

*Brent Weber as "Martin", the arrogant son of the mayor and Roys nemesis

*Bruce McGill as the unscrupulous villain, "Garrison", who uses his position as a museum curator to steal archeological artifacts and then gives chase to Roy Parker on the trail of the Cassidy treasure.

*James Karen as "Butch Cassidy", whose real name is LeRoy Parker

*James Gammon as "Sam Parker", LeRoys youngest brother and Roys grandfather

*Shauna Thompson as "Lorraine", Roys mother

*Rick Macy as "Mayor Blanding" of Circleville, Utah

*James D. Hardy as "Stewart"

*Brian Peck as "Clay"
 
*Ron Melendez as "Vince"

==Production==
  steam locomotive #618, used in the train sequence]]  Bridal Veil Circleville and Kane and Tooele Counties, Bryce Canyon. Stearman biplane Boy Scout Life Scout badge.  

The train sequence was filmed on Utahs Heber Valley Historic Railroad, using 2-8-0|2-8-0 "Consolidation"-type steam locomotive #618.  The engines original owner was the Union Pacific Railroad &ndash; whose trains were sometimes the target of Cassidys larcenous ways in the 1890s.   

==Release==
The film was released in 2006 by GO Films and then as a direct to video movie on DVD in 2007. Film critic David Cornelius rated it one of the ten best direct-to-video movies of 2007, saying, "Those seeking old fashioned family fun should look no further than this rip-roarin’ tale of Butch Cassidy’s teenage nephew, caught up in a wild race to find hidden gold".
 

==Awards==
Ryan Little and Adam Abel won the Heartland Film Festivals Crystal Heart Award for independent filmmakers in 2006, for Outlaw Trail: The Treasure of Butch Cassidy.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 