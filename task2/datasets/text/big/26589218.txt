The Taqwacores (film)
{{Infobox film
| name          = The Taqwacores
| image         = The Taqwacores.jpg
| caption       = Theatrical release poster
| alt           = 
| director      = Eyad Zahra
| producer      = David Perse Allison Carter Nahal Ameri
| screenplay    = Eyad Zahra Michael Muhammad Knight
| story         = Michael Muhammad Knight  
| starring      = Dominic Rains Bobby Naderi Noureen DeWulf
| music         = Omar Fadel
| cinematography = JP Perry
| editing       = Josh Rosenfield
| released      =  
| country       = United States
| language      = English
}}
The Taqwacores is a 2010 film adaptation of the 2003 novel The Taqwacores by Michael Muhammad Knight.  The film was directed by Eyad Zahra and stars Bobby Naderi, Dominic Rains, and Noureen Dewulf.  The film imagines a fictitious Islamic punk rock scene through characters living in a punk house in Buffalo, New York.  It was filmed in Cleveland, Ohio.

==Plot==
A sophomore majoring in engineering, Yusef (Bobby Naderi) seeks living quarters with fellow Muslims after a year in the godless dorms. He moves—rather improbably, given his conservative nature—into a building inhabited by various punky misfits (it is unclear whether they are also students) wrestling with their cultural and religious identity. Or, as red-mohawked guitarist Jehangir (Dominic Rains) puts it, their "mismatching of disenfranchised subcultures."
 burka yet is full of ideas that might be considered blasphemous.

==Cast==
(in alphabetical order)    at the Internet Movie Database, Retrieved 
September 6, 2010 
* Kashish Tanwar as Rabeya Jim Dickson as Gas Station Bully
* Volkan Eryaman as Amazing Ayyub
* Denise George as Dee Dee Ali
* Marwan Kamel as Himself (Cameo)
* Anne Leighton as Lynn
* Nav Mann as Umar
* Rasika Mathur as Fatima John Charles Meyer as Hamza
* Bobby Naderi as Yusef
* Dominic Rains as Jehangir Tabari
* Nicholas Riley as Harun
* Ara Thorose as Lead Singer of The Gilmans
* Ian Tran as Fasiq
* Tony Yalda as Muzzamil

===music===
The Taqwacores features an original score by Los Angeles composer, Omar Fadel, as well as licensed music from The Kominas and Al-Thawra.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 