In the Shadow of the Moon (film)
{{Infobox film
| name           = In the Shadow of the Moon
| image          = In the shadow of the moon poster.jpg
| caption        = Promotional film poster
| director       = David Sington Christopher Riley
| producer       = Duncan Copp Christopher Riley Sarah Kinsella John Battsek Julie Goldman
| writer         = 
| starring       =  Philip Sheppard
| cinematography = Clive North
| editing        = David Fairhead
| studio         = DOX Productions/Passion Pictures/Discovery Films
| post production= Molinare
| distributor    = Vertigo Films
| released       =  
| runtime        = 100 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} manned missions to the Moon.     It premiered at the 2007 Sundance Film Festival, where it won the World Cinema Audience Award.  In March 2008, it was the first film to win the Sir Arthur Clarke Award for Best Film Presentation. It was given a limited release in the United States on September 7, 2007, and in Canada on October 19, 2007.     It was released on DVD in the United States on February 22, 2008, and March 31, 2008, in the United Kingdom.    It is also notable for giving Gareth Edwards (director)| Gareth Edwards (who would go on to direct Godzilla (2014 film)|Godzilla) an early credit in visual effects.

==Synopsis== HD by Apollo era Al Bean, Michael Collins, John Young, David Scott, Charlie Duke, Eugene Cernan and Harrison Schmitt.  The former astronauts have the only speaking roles in the movie, although occasional supplementary information is presented on screen with text and archival television footage presents the words of journalists such as Jules Bergman and Walter Cronkite. Neil Armstrong, the first person to set foot on the Moon, declined to participate, the only surviving moon walker at the time to do so.
 book by Colin Burgess and Francis French, and both include many original interviews with Apollo lunar astronauts. The documentary offers a view of the Apollo program that is complementary to the book and is neither a source nor a tie-in.

==Participants== Michael Collins). All manned Apollo flights with the exception of Apollo 7 , which was an earth-orbit mission, were represented in the film. The film is presented by Ron Howard, but there is no narration in the film.

*Apollo 11:
**Buzz Aldrin, Lunar Module Pilot Michael Collins, Command Module Pilot

*Apollo 12:
**Alan Bean, Lunar Module Pilot

*Apollo 13:
**Jim Lovell, Commander (also Apollo 8 CMP)

*Apollo 14:
**Edgar Mitchell, Lunar Module Pilot

*Apollo 15:
**David Scott, Commander (also Apollo 9 CMP)

*Apollo 16: John Young, Commander (also Apollo 10 CMP)
**Charles Duke, Lunar Module Pilot (also Apollo 11 CapCom)

*Apollo 17:
**Eugene Cernan, Commander (also Apollo 10 LMP)
**Harrison Schmitt, Lunar Module Pilot

==Reception== IMDb gave the film 8.1/10 points.

Entertainment Weekly rated the movie an A.  The Los Angeles Times called the film a "fresh and compelling film, made with intelligence and emotion"  Meanwhile, The Hollywood Reporter concluded that "The value of this film, not just to moviegoers today but to future generations, is simply enormous." 

On June 23, 2008 the Independent Investigations Group (IIG) honoured In the Shadow of the Moon for promoting scientific skepticism in media; the award was received by producer Duncan Copp. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 