Rama Rama Kya Hai Dramaa?
{{Infobox film
| name           = Rama Rama Kya Hai Dramaa?
| image          =Rama_Rama_Kya_Hai_Dramaa?_poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Chandrakant Singh
| producer       = Surendra Bhatia Rajan Prakash
| story          = 
| starring       = Neha Dhupia Rajpal Yadav Aashish Chaudhary Amrita Arora
| music          = Siddharth-Suhas
| cinematography = Mahendra Rayan
| editing        = 
| studio         = 
| distributor    = Oracle Entertainment Pvt. Ltd.
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Rama Rama, Kya Hai Dramaaa is a Bollywood comedy film directed by Chandrakant Singh, and produced by Surendra Bhatia and Rajan Prakash. The film stars Rajpal Yadav, Neha Dhupia, Aashish Chaudhary and Amrita Arora in lead roles. It released on 2 January 2008, and received generally negative response upon release.

==Plot==
The story revolves around Santosh (Rajpal Yadav) who is married to Shanti (Neha Dhupia). Santosh works at a bank in Mumbai and is a very simple man. However, his simplicity and lack of tactfulness often sees him engage in domestic altercations with Shanti. The regular fights start taking their toll on Santoshs work life, also where he is often looked down by his boss Prem (Aashish Chaudhary) who is always bragging about how smooth his own married life is and how understanding his wife Khushi (Amrita Arora) is. But the reality is that his wife also nags him a lot and he is as disturbed at times as Santosh.

The film takes many twists and turns including many scenes in which Santosh imagines that he is the boyfriend or husband of other random women. In the end this lands him in trouble with the police but finally when Shanti bails him out, the two come to terms with each other and realize that such things are a normal part of every marriage and they must learn to live with it.

==Cast==
* Rajpal Yadav as Santosh Singh
* Aashish Chaudhary as Prem Bhatia
* Neha Dhupia as Shanti Singh
* Amrita Arora as Khushi Bhatia
* Anupam Kher as Vishwas Khurana
* Rati Agnihotri as Shraddha Sanjay Mishra as Mishraji
* Mona Thiba as Shivani
* Razak Khan as Abdul (Rickshaw driver)
* Deepak Shirke as Police Inspector

==References==
 

==External links==
* 

 
 

 