Naariya Seere Kadda
{{Infobox film
| name = Naariya Seere Kadda
| image =
| caption = 
| director = BMP Annaiah
| writer = BMP Annaiah
| based on = 
| producer = D. B. Kumaraswamy
| starring = V. Ravichandran   Naveen Krishna    Nikita Thukral    Harshika Poonacha
| music = V. Manohar   V. Ravichandran (1 song)   Rajesh Ramanath (Background score)
| cinematography = K M Vishnuvardhan
| editing = Sanjeev Reddy
| studio  = Sri Gouthami Enterprises
| released =  
| runtime = 136&nbsp;minutes
| language = Kannada
| country = India
}}
 2010 Indian Kannada language action film written and directed by BMP Annaiah. The film stars V. Ravichandran, Naveen Krishna, Nikita Thukral and Harshika Poonacha in the lead roles.  While the main music composer is V. Manohar, Ravichandran has composed, written and directed one song in the film.

The film released on 26 November 2010 across Karnataka. Upon release, the film generally met wide negative reviews from the critics and audience. 

== Cast ==
* V. Ravichandran as Gopal
* Naveen Krishna as Vijay
* Nikita Thukral as Rekha
* Harshika Poonacha
* Shobharaj
* Bullet Prakash
* M. S. Umesh
* Shivaram
* Rekha Das
* Dingri Nagaraj

== Soundtrack ==
{{Infobox album  
| Name        = Naariya Seere Kadda
| Type        = Soundtrack
| Artist      = V. Manohar, V. Ravichandran
| Cover       = 
| Released    = September, 2010
| Recorded    = 
| Genre       = Film soundtrack
| Length      =
| Label       = Manoranjan Audio
| Producer    = 
| Reviews     =
| Last album  = 
| This album  = 
| Next album  = 
}}

The audio comprises total 4 songs out of which 3 songs are composed and written by V. Manohar and one song is composed and written by V. Ravichandran. The audio launch took place at a Bangalore hotel in September 2010. Actor turned producer Raghavendra Rajkumar released the audio in the presence of entire film team 

{{tracklist
| headline = Track listing
| all_music = V. Manohar
| all_lyrics =V. Manohar
| extra_column = Singer(s)
| title1 = Naviloora Devi
| extra1 = Santhosh, Nanditha
| length1 = 
| title2 = Naajooku Naajooku
| extra2 = Rajesh Krishnan, Vijaya Shankar
| length2 = 
| title3 = Kole Kole
| extra3 = Rajesh Krishnan, Anuradha Bhat
| length3 = 
| title4 = Neere Neere Panneere( )
| extra4 = Srinivas (singer)|Srinivas, Anuradha Sriram
| length4 = 
}}

== References ==
 

== Further reading ==
*  
*  
*  

 
 
 
 
 
 
 

 