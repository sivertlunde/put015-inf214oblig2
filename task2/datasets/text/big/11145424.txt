Satanás

{{Infobox film
| name           =  Satanas
| image          = Satanás.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Andrés Baiz 
| producer       = Rodrigo Guerrero
| screenplay     = Andrés Baiz
| based on       =  Mario Mendozas book and inspired by true events  
| starring       =Damián Alcázar   Blas Jaramillo   Marcela Mar
| music          = Angelo Milli
| cinematography = Mauricio Vidal
| editing        = Alberto de Toro
| distributor    = 
| released       = March 4, 2007 (U.S.) June 1, 2007 (Colombia) October 10, 2007 (Mexico)
| runtime        = 120 minutes
| country        = Colombia
| language       = Spanish
| audience        = 
| box office      = 
}} Colombian film directed by Andi Baiz. It is adapted from the novel of the same title by Mario Mendoza Zambrano. It was Colombias submission to the 80th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.        

The story is framed in a context of urban solitude in the modern world and sheds some light on the events that surrounded a spree killing that took place in Bogotá, Colombia, in 1986. Satanás, the film, explores the final stages in the life of the killer Campo Elías Delgado.

==Plot==
The film follows the lives of three separate characters, Eliseo, Paola, and Father Ernesto, living in the city of Bogota, Columbia, in the mid 1980s. 

Eliseo, a middle-aged English teacher and veteran of the Vietnam War, has  difficulties creating and maintaining relationships (especially with women). He lives with his aging mother, Blanca, but they detest each other and argue constantly. Eliseo is also irritated by his neighbor and landlord, Beatriz, who keeps asking him for money for her charitable works. He always coldly refuses, and is treated rudely at the local store as a result.

Eliseo works as a teacher of English, and one of his students is 15 year old Natalia. He develops a crush on Natalia, who is gentle and polite during lessons. Invited to Natalias birthday party, Eliseo is angry when he realizes that Natalia has a boyfriend, Esteban.

After a heated argument with his mother, Eliseo loses his temper, shoots her, and then sets the place on fire. On his way out he encounters Beatriz and he kills her too. Arriving at Natalias house, he viciously attacks Natalia and her mother, killing them both. In the evening, he visits an old friend, a librarian who has always been kind to him. He tells her that he is leaving town and thanks her for her friendship. From there, he goes on the last leg of his killing spree, the high class restaurant, "Pozzetto". He chooses an expensive meal and a few drinks, and consumes them both. He pays the bill, leaving a big tip. He then goes into the bathroom and prepares himself for the upcoming massacre. When he comes out, he shoots indiscriminately.
 
Paola is an attractive young woman who hates her job in the marketplace. She accepts an offer from an acquaintance to become a member of a gang of robbers. Her job is to act as bait to attract wealthy men in a bar, incapacitate them by putting Scopolamine in their drinks, and then lure them into a cab. Her accomplices then drive the victim to an ATM machine and steal his money. She is very good in this role, but is concerned about the welfare of her victims. She gets in a cab to go home one night after a successful operation, but the driver and an accomplice kidnap her and take her to an old taxi shop where they assault and rape her. She is deeply traumatized and eventually asks members of the gang to kill the two people that raped her. The rapists are successfully located and are murdered. Regretting their deaths and all the robberies committed before, she quits the gang and gets a job as a waitress at Pozzetto, an Italian restaurant. There she becomes one of Eliseos victims.

Father Ernesto is a priest involved in a passionate love affair with his housekeeper. He is conscientious in his duties, but tormented by his sexual urges. Father Ernesto is trying to help a disturbed woman with three children who has come to his church seeking spiritual guidance. He goes to get food for them but when he returns they are gone. She returns some time later, alone and covered in blood, having murdered her children "to release them" from this life of evil. The scene anticipates the slaughter at the end of the movie. Father Ernesto visits the woman in prison where her demented diatribes predict future events. Traumatized by his encounters with the woman and unable to restrain his carnal desires, Ernesto loses his faith and refuses to go on as a priest. He resigns his position and takes his housekeeper for a date at a restaurant. There they are both killed by Eliseo.

== Cast ==

* Damián Alcázar as Eliseo
* Marcela Mar as Paola
* Blas Jaramillo as Padre Ernesto Teresa Gutierrez as Blanca, Eliseos mother
* Vicky Hernandez as Beatriz, Eliseo and Blancas annoying neighbor
* Martina Garcia as Natalia, Eliseos English Student
* Patricia Castañeda as Valeria
* Carolina Gaitan as Natalias friend
* Marcela Gallego as Natalias mother
* Isabel Gaona as Irene

== Prizes and Festivals ==
 

Festival of Monte Carlo (Monaco)

* Award for Best Film Festival
* Award for Best Actor (Damián Alcázar)

San Sebastián International Film Festival – Horizons (Spain)

* Honorable Mention to Marcela Mar (Actress)
 
Festival de Cine de Bogotá – (Colombia)

* Best Film Award Colombiana
* Critics Award for Best Film Colombiana
* Bronze Award for Best Opera Prima
 
Ibero-American Film Festival of Huelva (Spain)

* Pablo Neruda Prize for Best Feature – Awarded by the Student Film.
 
Latitude Zero Film Festival – (Ecuador)

* Honorable Mention of Criticism and the Press

==See also==
 
*Satan
*Cinema of Colombia
*List of submissions to the 80th Academy Awards for Best Foreign Language Film

==References==
 

== External links ==
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 