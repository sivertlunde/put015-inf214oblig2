DC Showcase: Jonah Hex
{{Infobox film
| name        = DC Showcase: Jonah Hex
| image       =Jonah Hex (DC Showcase banner).jpg
| caption     = 
| director    = Joaquim Dos Santos
| producer    = Bobbie Page Alan Burnett Joaquim Dos Santos Sam Register Bruce Timm
| writer      = Joe R. Lansdale
| starring    = Thomas Jane Linda Hamilton
| music       = Benjamin Wynn Jeremy Zuckerman
| studio      = Warner Bros. Animation Warner Premiere DC Comics
| distributor = Warner Home Video
| released    =  
| runtime     = 12 minutes
| country     =  
| language    = English
| budget      =
}}
DC Showcase: Jonah Hex is a 2010   DVD, was the second of the DC Showcase series and was included on the compilation DVD DC Showcase Original Shorts Collection in an extended version. 

==Plot==
Red Doc, a man that claims he can take on anyone and anything, arrives in town looking for more booze and more action. Madame Lorraine spots him, and after inviting him upstairs, kills him, takes his money and has two of her henchmen dispose of the body.

The next day, Jonah Hex arrives in town, looking to pick up a $5000 bounty on Red. After taking on an arrogant young man who taunts him, Jonah learns from a scared bar girl about Madame Lorraine, who goes after the men with money. Paying the girl off, Hex buys drinks for the entire saloon, getting Lorraines attention and joining her upstairs. However, Hex manages to avoid her shot and knocks her out, then proceeds to kill her henchmen. Stepping out of the room, he is shot at by the bartender, whom he kills. He then demands that Lorraine tell him where Red is.

Lorraine takes him to an old mine, and shows him a dark hole that leads to a caved in lower shaft. She and Hex journey down, and it is revealed that Lorraine has murdered at least fifteen men. Grabbing a knife, Lorraine tries once more to kill Hex, who punches her and secures Reds body. Lorraine comes to just as Hex reaches the top and kicks down the rope trapping her there. Lorraine first pleads that they can be partners, then tells Hex he cant leave her. Hex says she has plenty of companions whom she knows, then departs. Lorraine trembles as she stares at the dead bodies, the only lamp in the shaft slowly going out, leaving her trapped in the dark.

==Cast==
* Thomas Jane as Jonah Hex   
* Linda Hamilton as Madame Lorraine 
* Jason Marsden as Young Man,  Bartender 
* Michael Rooker as Red Doc 
* Michelle Trachtenberg as Bar Girl 

==References==
 

==External links==
*  
*  

 
 
 

 

 
 
 
 
 
 
 
 
 