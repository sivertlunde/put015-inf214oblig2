Jeepers Creepers (1939 animated film)
{{multiple issues|
 
 
 
 
}}
{{Infobox Hollywood cartoon
|cartoon_name=Jeepers Creepers
|series=Looney Tunes (Porky Pig)
|image=
|caption=
|director=Robert Clampett
|story_artist=Ernest Gee
|animator=Vive Risto
|voice_actor=Mel Blanc Pinto Colvig
|musician=Carl W. Stalling
|producer=Leon Schlesinger
|distributor=Warner Bros. Pictures The Vitaphone Corporation
|release_date=September 23, 1939 (USA)
|color_process=Black and White
|runtime=8 minutes
|movie_language=English
}}
Jeepers Creepers is a 1939 Looney Tunes animated short starring Porky Pig. It was directed by Robert Clampett.

== Plot ==
Porky is a police officer, who is in a police car that is named 6 7/8. He gets a call from his chief to go investigate goings-on at a haunted house. The house is haunted to the core, and the fun loving ghost plays a series of pranks on the unsuspecting pig. As Porky knocks on the door to enter the haunted house, the ghost does a lady voice "Come in." Porky enters, already frightened.

He enters again, the ghost places Frogs into a pair of shoes to look like a person walking, as Porky doesnt notice, the laces of the shoes get stuck to a coat hanger pole then rips off a curtain to make it look like a person with a cloak on. It immediately scares him and then the ghost scares him. Porky runs upstairs and lands in the ghosts arms with realizing, until that famous line comes as the ghost says it very goopy. "What the matter baby?".
 Rochester imitation).

== Cast ==
* Mel Blanc – Porky Pig / Singing Ghost / Police Dispatcher (uncredited)
* Pinto Colvig – Ghost (uncredited)

==Edited versions==
*As with many Black and white produced Looney Tunes shorts, Jeepers Creepers has been colorized twice for television, redrawn in the 1960s and by computer in the early 1990s. When the short aired on TV, the actual editing of the ending (where the ghost, after getting exhaust smoke blown on him, is left in blackface commenting "My, oh my! Tattletale Gray!") has been done in different ways:
**When it aired in syndication, the ending was altered to have the ghost in purple face so the blackface joke would be less offensive.
**When shown on Nickelodeon, the cartoon ended via fake iris-out after the exhaust on Porkys car blew in the ghosts face. Fox as part of The Merrie Melodies Show, the cartoon ended via fake fade-out after Porky drove his car past the ghost.
**When it aired on Cartoon Network (with the exception of the versions shown on Late Night Black & White and The Bob Clampett Show), the cartoon ended with a black-out as Porkys car blows exhaust in the ghosts face.

== Sources ==
 

== External links ==
* 

 
 
 
 
 
 
 
 
 
 

 