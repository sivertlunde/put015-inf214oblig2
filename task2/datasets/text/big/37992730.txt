Robert and Bertram (1961 film)
{{Infobox film
| name = Robert and Bertram
| image =
| image_size =
| caption =
| director = Hans Deppe
| producer = Artur Brauner
| writer = Gustav Raeder (play)   Janne Furch   Gustav Kampendonk
| narrator =
| starring = Willy Millowitsch   Vico Torriani   Trude Herr   Marlies Behrens
| music = Gert Wilden
| cinematography = Siegfried Hold
| editing = Ilse Voigt
| studio = Central Cinema Company
| distributor = Europa-Filmverleih
| released = 29 September 1961
| runtime =
| country = West Germany German
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} West German Robert and Bertram, update to the modern era. Two vagabonds, Robert and Bertram, are hired by a shoe company to walk 500 kilometres to test their new product. 

==Cast==
* Willy Millowitsch as Robert Ziegel
* Vico Torriani as Bertram Weiler
* Trude Herr as Klara Ziegel
* Marlies Behrens as Yvonne Berger
* Helen Vita as Mieze Frühling
* Erwin Strahl as Franco
* Hubert von Meyerinck as Kriminalkommissar Wolf
* Ralf Wolter as Toni Knauer
* Margarete Haagen as Alte Dame im Auto
* Arno Paulsen as Direktor Malina
* Kurt Pratsch-Kaufmann as Polizist
* Erich Fiedler as Dr.Abendroth
* Kurt Waitzmann as Dr.Sommerfeld
* Johanna König as Fräulein Sellner Franz Schneider as Kegelbruder Franz
* Josef Tilgen as Kegelbruder Hans
* Egon Vogel as Kegelbruder Willi
* Lotti Krekel as Dagmar, Anhalterin
* Anja Brüning as Gisela, Anhalterin

==References==
 

==Bibliography==
* OBrien, Mary-Elizabeth. Nazi Cinema as Enchantment: The Politics of Entertainment in the Third Reich. Camden House, 2006.

==External links==
* 

 
 
 
 
 
 
 
 


 
 