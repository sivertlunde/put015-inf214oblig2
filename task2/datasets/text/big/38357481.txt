V/H/S/2
 
{{Infobox film
| name           = V/H/S/2
| image          = V-H-S-2_Poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = {{plainlist|
* Simon Barrett (Tape 49)
* Adam Wingard (Phase I Clinical Trials) Eduardo Sánchez Gregg Hale (A Ride in the Park) Gareth Huw Evans (Safe Haven)
* Jason Eisener (Slumber Party Alien Abduction)
}}
| producer       = {{plainlist|
* Roxanne Benjamin
* Gary Binkow
* Kyle David Crosby
* Brad Miska
* Jamie Nash
}}
| writer         = {{plainlist|
* Simon Barrett (Tape 49 and Phase I Clinical Trials)
* Jamie Nash (A Ride in the Park)
* Timo Tjahjanto and Gareth Huw Evans (Safe Haven)
* John Davies (Slumber Party Alien Abduction)
}} see below
| music          = {{plainlist|
* James Guymon
* Steve Moore
* Aria Prayogi
* Fajar Yuskemal
}}
| cinematography = {{plainlistt|
* Tarin Anderson
* Abdul Dermawan Habir
* Stephen Scott
* Seamus Tierney
* Jeff Wheaton
}}
| editing        = {{plainlist|
* Jason Eisener
* Gareth Huw Evans
* David Geis
* Bob Rose
* Eduardo Sánchez
* Adam Wingard
}}
| studio         = {{plainlist|
* Bloody Disgusting The Collective
* Haxan Films
}}
| distributor    = Magnet Releasing
| released       =  
| runtime        = 92 minutes  
| country        = {{plainlist|
* United States
* Canada
* Indonesia
}}
| language       = English Indonesian
| budget         =
| gross          = $795,661 
}} Gareth Evans, Eduardo Sánchez, Gregg Hale, and franchise returnees Simon Barrett and Adam Wingard. 

==Plot==
The film is presented as an anthology of short horror films, built into a frame narrative which acts as its own short horror film. Each short film is linked together with the concept of found footage (each segment is from the VHS tapes found in the first film).

===Tape 49/frame narrative===
: Directed by Simon Barrett
After Larry botches an investigation, a college students mother requests that Larry and his girlfriend Ayesha, also a private investigator, investigate the disappearance of her son. After breaking into the students dorm, they discover a large stack of VHS tapes and a laptop that is still recording video. On the laptop, the student discusses the VHS tapes, saying where he got one of the most recent ones, and Larry tells Ayesha to watch the tapes while he investigates the house. As Ayesha watches the first tape, a figure peers out and watches her.

Ayesha calls Larry into the room, and they discuss the tapes legitimacy. Larry tells Ayesha to continue viewing the tapes, which the students video explains must be watched in the proper order "to affect you".

Larry re-enters the room and finds Ayesha in a hypnotized state with her nose bleeding. After being woken, she says she has a migraine. Larry leaves to find medicine, and a seemingly entranced Ayesha watches another tape. From the shadows, the figure crawls out and watches her.

Upon Larrys return, he discovers Ayesha lying on the floor with her gun in hand and a bullet wound in her head, implying she committed suicide. A VHS tape with the word "WATCH" written on it in lipstick, lies beside her. Larry picks up the tape and anxiously watches it.

Confused, Larry watches the webcam footage and sees the student explain that he and his mother want to make their own tape; the student then attempts suicide on-camera by shooting himself on the jaw, but seemingly survives, even though his jaw was completely ripped off, and runs off moments before Larry and Ayesha enter the dorm. An undead Ayesha suddenly attacks Larry. When he breaks her neck, she chases after him on fours. Larry hides in a closet and shoots Ayesha in the face when she finds him. Larry hears a gurgling sound and explores the closet only to find the student hiding in the back. The student strangles Larry and afterwards gives the camera a "thumbs up", his plan being a success, and the recording ends.

===Phase I Clinical Trials===
: Directed by Adam Wingard
After a car accident, Herman receives an ocular implant with a camera to replace his damaged left eye. The doctor warns that he may experience "glitches" because the implant is still in at an experimental stage, and Herman notices a young red-haired woman staring at him on the way out of the hospital. That night at his home, he is haunted by an old man and young girl, both of whom appear dead. Herman calls the doctor to demand that he remove the implant, and spends the night locked in his bathroom.

The next day, the red-haired woman, Clarissa, appears at his door, asking him if he is starting to see dead people. She reveals that she was born deaf and had an implant installed in her ear, which allowed her to hear but also gave her visions of dead people. She says the that the ghosts become stronger when one pays attention to them, so they have sex in an effort to ignore them. Later that night, several ghosts appear and drown Clarissa in the outdoor pool. Running back through the house, Herman again locks himself in the bathroom and uses a scalpel to cut out his eye. Undeterred, the ghosts break down the door, and stuff the eye, still attached to the razor, down his throat, presumably killing him.

===A Ride in the Park=== Eduardo Sánchez Gregg Hale
A cyclist, Mike, with a Go-Pro camera affixed on his helmet, is riding through a national park, when he runs into a hysterical and bloody woman, asking for help with her boyfriend. Mike then sees several zombies approaching them, before he is suddenly attacked and bitten on the throat by the woman, whom he kills. Mike staggers through the park, heavily bleeding, before finally collapsing and apparently dying. A pair of bikers come across him and attempt to help, but he reanimates, attacks, and partially devours them. Hearing noise in the distance, the three zombies head off towards it.

The trio invades a young girls birthday party, killing several people, some of whom reanimate to attack others. While trying to attack a man in his car, he notices his bloodied reflection, which seems to subdue his aggressive behavior. When he accidentally pocket dials his girlfriend, he is shocked back to a semi-conscious state upon hearing her voice, and commits suicide with a discarded shotgun.

===Safe Haven=== Gareth Huw Evans
A news crew composed of 4 members (Malik the interviewer,Lena his girlfriend and Adam and Joni the cameramens) infiltrates an Indonesian cult, in the hope of shooting a documentary about their mysterious activities. Inside the building, they find the walls adorned in bizarre symbols, schoolchildren in classrooms, and women dressed in white garments. Malik overhears that his fiancée, Lena, is pregnant with Adams child.

In the basement, Adam finds a bloody woman strapped to a chair, who begins convulsing causing him to run away. While the cult leader is being interviewed, a bell chimes, and he suddenly announces the "time of reckoning" over the intercom. The cultists begin a mass suicide via poison and gunshots, while Joni has his throat cut by the cult leader. Lena is abducted by several women in surgical apparel, and Malik is shot dead by one of the cult members. As Adam attempts to rescue Lena, an explosion knocks him down, and the cult leader then proceeds to explode in front of him. Adam finds Lena placed on an alter but is unable to save her as a horned demon tears its way out of her body. As Adam attempts to flee, he is attacked by the previously deceased cultists and schoolchildren who have turned into zombie-like ghouls. He eventually makes it to his car and attempts to drive away, but the demon chases him down and overturns the vehicle. As he attempts to crawl out, the demon calls him "papa", causing him to begin laughing hysterically, realizing that the demon is actually his child. Shortly thereafter, the camera malfunctions.

===Slumber Party Alien Abduction===
: Directed by Jason Eisener
Young brothers, Gary and Randy, attach a camera to their Yorkshire Terrier dog, Tank, to create videos at their lake-side house. After their parents leave, Gary and Randy invite over their friends, Shawn and Danny, and the group harass the older sister, Jen, and her boyfriend, Zack. While the group is swimming at a nearby lake, they dont notice a Grey alien hiding beneath the water.

Later that night, the aliens begin frightening the kids with bright lights and deafening noises that resemble a large ship. Realizing the aliens are in the house and have cut the power, Zack attempts to scare off the intruders with a shotgun, only to be quickly grabbed by one of the aliens. The aliens attack the rest of the teenagers in the house, dragging them in their sleeping bags, and attempting to drown them in the lake. Only Gary, Randy, Jen, and Tank escape, running into the woods to hide. They run toward what they believe to be police lights and sirens, but it turns out to be a trap set by the aliens who abduct Randy. Jen and Gary flee to a nearby barn, where the aliens drag Jen away as Gary and Tank escape up a ladder. As the aliens close in on Gary, he is suddenly pulled into the air by the alien ship, dropping the dog in the process. Tank lies dying on the ground, as the camera slowly dies.

==Cast==
 

===Tape 49===
* Lawrence Michael Levine as Larry
* Kelsy Abbott as Ayesha
* L.C. Holt as Kyle
* Simon Barrett as Steve
* Mindy Robinson as Tabitha

===Phase I Clinical Trials===
* Adam Wingard as Herman
* Hannah Hughes as Clarissa
* John T. Woods as Dr. Fleischer
* Corrie Lynn Fitzpatrick as Young Girl
* Brian Udovich as Bloody Man
* John Karyus as Uncle
* Casey Adams as Justin

===A Ride in the Park===
* Jay Saunders as Biker
* Bette Cassatt as Screaming Girl
* Dave Coyne as Good Samaritan Guy
* Wendy Donigian as Good Samaritan Girl
* Devon Brookshire as Bikers Girlfriend

===Safe Haven===
* Fachry Albar as Adam
* Hannah Al Rashid as Lena
* Oka Antara as Malik
* Andrew Suleiman as Joni
* Epy Kusnandar as Father
* R R Pinurti as Ibu Sri

===Slumber Party Alien Abduction===
* Rylan Logan as Gary
* Samantha Gracie as Jen
* Cohen King as Randy
* Zach Ford as Shawn
* Josh Ingraham as Danny
* Jeremie Saunders as Zack
 

==Production and release==
The film was rushed into production in late 2012,  and premiered January 19, 2013 at Park Citys Library Center Theatre as part of the 2013 Sundance Film Festival, much like its predecessor. 
 VOD on June 6, and theatrically on July 12. 

==Reception==
According to   rated it 49/100.   Borys Kit of The Hollywood Reporter wrote, "The scares are as hit-or-miss as the filmmaking".   Dennis Harvey of Variety (magazine)|Variety called it "rip-roaring good time for genre fans". 

On July 10, 2013, Rex Reed was the subject of controversy due to a scathing review of the film in which he admitted having walked out at the end of the first segment.  His review does complain about parts of the film that happened after he left the film, but his references are rather imprecise, e.g. describing segment Slumber Party Alien Abduction as "a sleepover invaded by psycho kidnappers   told from the perspective of a GoProcamera attached to the back of a dog" or summarizing segment A Ride in the Park as the tale of "a mountain biker pursued by flesh-eating zombies  ". 

==Sequel==
A third film in the series, titled  , has been announced. According to Screen Daily, V/H/S Viral will center around “fame-obsessed teens who unwittingly become stars of the next internet sensation.” Like the two previous V/H/S films, V/H/S Viral will consist of several short segments, which will be directed by Todd Lincoln (The Apparition), Nacho Vigalondo (Timecrimes), Marcel Sarmiento (Deadgirl), Gregg Bishop (Dance of the Dead), Justin Benson and Aaron Moorhead (co-directors of Resolution). The film was released on November 25th, 2014. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 