The Enchanted Square
 
{{Infobox Hollywood cartoon|
| cartoon_name = The Enchanted Square
| image = 
| caption =
| director = Seymour Kneitel
| story_artist = Shane Miller   Orestes Calpini
| animator = Orestes Calpini   Al Eugster
| voice_actor =
| musician = Winston Sharples
| producer = Seymour Kneitel   Isadore Sparber
| studio = Famous Studios
| distributor = Paramount Pictures
| release_date = 9 May 1947
| country = United States
| color_process = Technicolor
| runtime = 9:38
| movie_language = English
}}

The Enchanted Square was a Noveltoon cartoon, produced by Famous Studios and released on May 9, 1947. It marks the third appearance of Raggedy Ann in a cartoon. The cartoon was directed by Seymour Kneitel, written by Orestes Calpini and Shane Miller, who was also the scenic artist, and animated by Calpini and Al Eugster, with music composed by Winston Sharples.

==Summary==
It is Halloween and Officer Patrick Flanagan finds a discarded Raggedy Ann and decides that it will make a good present for Billie, the blind daughter of the pretty Mrs. Davis whom Flanagan fancies. When Billie receives Raggedy Ann she is very thankful but she wishes she could see Ann. Raggedy Ann tells her that she can see her through her imagination. Billie imagines a fantastic voyage where she and Ann travel through a carnevalesque land based on the real world but run by Mr. Giuseppe the calliope player. He was crying about song book scared away.

==External links==
*  
*  
 

 
 
 
 