The Tender Trap (film)
{{Infobox film
| name           = The Tender Trap
| caption        =
| image	     = The Tender Trap FilmPoster.jpeg
| director       = Charles Walters
| producer       = Lawrence Weingarten
| writer         = Julius J. Epstein Max Shulman (play) Robert Paul Smith (play)
| starring       = Frank Sinatra Debbie Reynolds David Wayne Celeste Holm
| released       =  
| cinematography = Paul Vogel
| editing        = John D. Dunning
| music          = Jeff Alexander
| costumes       = Helen Rose
| distributor    = Metro-Goldwyn-Mayer
| runtime        = 111 minutes
| country        = United States
| language       = English budget = $1,274,000  . 
| gross          = $4,495,000  
}}

The Tender Trap (1955 in film|1955) is a CinemaScope comedy starring Frank Sinatra, Debbie Reynolds, David Wayne, and Celeste Holm.
 the 1954 On the Guys and Dolls, was actually released ahead of The Tender Trap by one day on November 3, 1955.
  Best Original Song for "(Love Is) the Tender Trap" (music by Jimmy Van Heusen and lyrics by Sammy Cahn). The song proved a hit for Sinatra, one he would continue to sing throughout his career. It is performed in a pre-credits sequence by Sinatra, sung in the film by Reynolds in a lackluster version that Sinatra corrects and yet again at the end of the film by Sinatra, Reynolds, Holm and Wayne.

==Plot summary== theatrical agent in New York, living a seemingly idyllic life as a bachelor. Numerous women (among them Poppy (Lola Albright), Helen (Carolyn Jones), and Jessica (Jarma Lewis)) come and go, cleaning and cooking for him.

Charlies best friend since kindergarten, Joe McCall (David Wayne), who has a wife named Ethel and children in Indianapolis, comes to New York for a stay at Charlies apartment, claiming that the excitement is gone from his 11-year marriage and that he wants to leave his wife. Joe envies and is amazed by Charlies parade of girlfriends, while Charlie professes admiration for Joes married life and family.

At an audition, Charlie meets singer-actress Julie Gillis (Debbie Reynolds). She has her life planned to a tight schedule, determined to marry and retire from performing to a life of child-rearing by 22. Although at first she wards off Charlies advances, she comes to see him as the ideal man for her plans. Julie demands that Charlie stop seeing other women. Charlie balks, but he begins to fall in love with her.

Joe starts keeping company with Sylvia Crewes (Celeste Holm), a sophisticated classical musician and a typically neglected lover of Charlies. Sylvia is approaching 33 and desires marriage as much as the younger Julie does.

One day, annoyed by Julie and possibly jealous of Joes attentions, Charlie blurts out a proposal of marriage to Sylvia. She is thrilled, only to discover the morning after their engagement party that he has proposed to Julie as well.

Joe confesses his love to Sylvia and asks her to marry him. She turns him down, knowing that he loves his wife and children. Sylvia reminds Joe that girls turn into wives when they marry and she wants the same things Ethel does. On her way out, Sylvia runs into a charming stranger near the elevator who clearly wants to get to know her much better.

Joe packs up and returns to Indiana to his wife. Charlie, his other girlfriends also having moved on with their lives, leaves for Europe for a year.

Charlie returns just in time to see Sylvia marrying the new man in her life. She flips him the bridal bouquet. Julie is also at the wedding. Charlie tosses the flowers to her, then asks her to marry him. She agrees and they kiss.

==Cast==
* Frank Sinatra as Charlie Y. Reader
* Debbie Reynolds as Julie Gillis
* Celeste Holm as Sylvia Crewes
* David Wayne as Joe McCall
* Lola Albright as Poppy
* Carolyn Jones as Helen
* Jarma Lewis as Jessica
* Howard St. John as Sayers
* Tom Helmore as Sylvias Suitor
* Joey Faye as Sol Steiner

==Trivia==
Poppy Matsons surname is the same as that of Max Shulmans agent Harold Matson, while that of Joe McCall echoes that of Robert Paul Smiths representative, Monica McCall.

==Reception==
According to MGM records the film earned $3,054,000 in the US and Canada and $1,441,000 elsewhere resulting in a profit of $1,410,000. 

==See also==
* List of American films of 1955

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 