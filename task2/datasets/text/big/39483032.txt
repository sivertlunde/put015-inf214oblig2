We Are What We Are (2013 film)
 
{{Infobox film
| name           = We Are What We Are
| image          = We Are What We Are 2013.jpg
| border         = yes
| alt            = A bearded man, standing behind two seated girls, both dressed in white, his hands on their shoulders. 
| caption        = Film poster
| director       = Jim Mickle
| producer       = {{Plain list | 
* Rodrigo Bellott
* Andrew Corkin
* Nicholas Shumaker
* Jack Turner
* Nicholas Kaiser
}}
| writer         = Nick Damici Jim Mickle
| starring       = {{Plain list | 
* Bill Sage
* Julia Garner
* Ambyr Childers
* Kelly McGillis
* Odeya Rush
}}
| music          = {{Plain list | 
* Jeff Grace
* Darren Morris
* Phil Mossman
}}
| cinematography = Ryan Samul
| editing        = Jim Mickle
| studio         = Belladonna Productions Memento Films International Uncorked Productions Venture Forth
| distributor    = Entertainment One
| released       =   |2013|09|27|limited|ref2=   }}
| runtime        = 105 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = $81,381  
}} Mexican film of the same name.  Both a sequel and prequel have been announced. 

==Plot==
During a torrential downpour, a woman confusedly staggers into a store as the butcher receives a delivery. After several attempts to address her, she finally responds and explains that the foul weather has strongly affected her. The butcher says that it will get worse before it gets better, and she purchases groceries. As she leaves the store, she sees a poster that advertises missing teenage girls. Before she can reach her car, she begins bleeding from her mouth and loses consciousness as she falls into a rain-filled ditch, where she drowns.

Later, the sheriff tells Frank Parker that his wife has died. Consumed by grief, Frank does not show up to identify the body but instead sends his two daughters, Rose and Iris. Doctor Barrow, who delivered Franks young son Rory, explains that an autopsy is mandated by the state. During the examination, he finds evidence of Kuru disease. Meanwhile, Frank is comforted by his kindhearted neighbor Marge, and, while driving through the storm later, finds a motorist in need of assistance; the film implies that he attacks her with a tire iron.

Rose and Iris debate whether they are prepared to take over their mothers religious duties, but Iris is adamant that they perform this years ritual. Rory, too, has trouble keeping the familys fast. Eventually, Rory wanders into his fathers shed and finds a young woman held hostage. Frank angrily demands that Rory leave, then forces his daughters to kill and butcher the captive. They reluctantly obey, and the entire family eats her remains after a bit of urging from Frank. Marge attempts to deliver a vegetarian meal to the Parkers, noting she thinks she heard a woman crying in the shed, but she receives an icy welcome from Iris.

Barrow, whose daughter previously went missing, becomes suspicious when he finds a bone fragment in a creek. Though Sheriff Meeks brushes off his concerns, Barrow is able to convince Deputy Anders to investigate. Anders finds more evidence in the creek, only to be confronted by Iris, on whom he has a crush. Iris leads him to a secluded spot, and Anders confesses his feelings for her. Confused and overwhelmed with guilt, Iris breaks into tears. As Anders comforts her, they begin to have sex, but Frank finds them and kills Anders. Disgusted, he tells Iris to return home.

Frank orders the girls to stay in their bedroom, and they form a plan to escape. Frank prays alone in his room, muttering that they have kept their tradition and will be joining their mother soon. While Frank recites prayers, Rose takes the car keys. When Frank prepares dinner, he takes a jar full of powder and adds it to the soup. As the children set the table, Rose notices white powder residue and realizes Frank is planning to poison them with arsenic. She unsuccessfully attempts to alert Iris, then knocks Rorys bowl on the floor to prevent him from eating. Before Frank can react, Barrow, whose research has turned up evidence that the Parkers may have engaged in cannibalism, arrives at the home and confronts Frank, demanding to know what happened to Anders and his daughter, whose hair ornament he sees Iris is wearing. Frank stalls for time as he reaches for his pistol, and Iris jumps in front of Barrow to protect him. Frank accidentally wounds his daughter, and Barrow shoots Frank.

Frank survives and knocks out Barrow. Rose and Rory flee the house in a panic, and they take refuge with Marge. Frank breaks into Marges house and kills her, then convinces Rose and Rory to rejoin him. Back at Franks house, he again urges his children to eat. When he tells Rose that she looks like her deceased mother, Rose and Iris bite into him and rip away his flesh. Rose notices Barrow, barely conscious, has witnessed the whole thing, and she places his daughters stolen hair ornament on his chest. The next morning, the children leave town, and Rose brings a diary that details their ancestors memories of cannibalism, implying their tradition will live on.

==Cast==
* Bill Sage as Frank Parker
* Julia Garner as Rose Parker
* Ambyr Childers as Iris Parker
* Kelly McGillis as Marge
* Odeya Rush as Alyce Parker
* Michael Parks as Doc Barrow
* Wyatt Russell as Deputy Anders
* Nick Damici as Sheriff Meeks Jack Gore as Rory Parker
*Kassie DePaiva as Ma Parker

==Production== original film, Catskills without Fessenden, who has a house there.  The dark subject matter caused issues with the films younger cast members.  Mickle consulted Jack Gores parents, and they decided that Gore should only know his own scenes. 

==Release==
We Are What We Are premiered at the 2013 Sundance.  After a limited release in New York City and Los Angeles on September 27, 2013, it opened nationally on October 4, 2013.   It was released on home video January 7, 2014. 

==Reception==
We Are What We Are received positive reviews, with a Metacritic rating of 69 out of 100, indicating "generally favorable reviews".   Review aggregator Rotten Tomatoes reports that 85% of 62 surveyed critics gave the film a positive review, and the average rating was 7.1/10; the sites consensus states: "A compelling story cleverly told, We Are What We Are quenches horror buffs thirst for gore while serving up serious-minded filmmaking and solid acting."   

Jeannette Catsoulis of The New York Times called it "a dreamy commentary on the ravages of extreme religious observance."   Guy Lodge of Variety (magazine)|Variety called it an "exuberantly grisly" film that genre fans will enjoy.   David Rooney of The Hollywood Reporter called it "a refreshingly mature genre entry that plants queasy dread and unleashes a good dose of scares".   Michael OSullivan of The Washington Post called it predictable and gross.   Scott Weinberg of Fearnet called it "a trenchant and fascinating indictment of the ways in which religion can brainwash and poison even the most innocent of souls."   Tim Gierson of Screen Daily called it "a tense, unsettling experience that offers very little gore but nonetheless knows how to turn the stomach."   Ryan Daley of Bloody Disgusting rated it 3.5/5 stars and wrote that the film "lacks any real surprises" but "has a lot to say and it says it well."   Drew Tinnin of Dread Central rated it 2.5/5 stars and wrote that the payoff is much better than the slow-paced buildup. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 