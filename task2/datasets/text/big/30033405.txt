Pardesi (1993 film)
{{Infobox film
 | name = Pardesi
 | image = Pardesi(1993 film).jpg
 | caption = DVD Cover
 | director = Raj N Sippy
 | producer = Monika Paddhal
 | writer = Iqbal Durrani
 | dialogue = Iqbal Durrani
 | starring = Mithun Chakraborty Varsha Usgaonkar Shakti Kapoor Sumalatha Suresh Oberoi
 | music = Anand-Milind
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released =  August 20, 1993
 | runtime = 140 min.
 | language = Hindi Rs 2 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1993 Hindi Indian feature directed by Raj N Sippy, starring Mithun Chakraborty, Varsha Usgaonkar, Shakti Kapoor, Sumalatha and Suresh Oberoi. Dialogues of this movie was highly appreciated written by Iqbal Durrani

==Plot==

Pardesi is the story of an honest man who is sentenced to death for a crime he did not commit. The Judge who sentenced him meets his look alike who is a criminal. The look alike of the honest man had to face uphill task of taking the honest mans place, as he is a born criminal.

==Cast==

*Mithun Chakraborty
*Varsha Usgaonkar
*Sumalatha
*Suresh Oberoi
*Shakti Kapoor
*Pankaj Dheer

==Soundtrack==

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Him Hain Bhangedi Hamein Bhang Pila De"
| Abhijeet Bhattacharya|Abhijeet, Usha Timothi  
|-
| 2
| "Hun Mainu Yaad Teri Aayee"
| Anuradha Paudwal
|-
| 3
| "Mere Dil Mein Tu Hi Hai"
| Udit Narayan
|-
| 4
| "Meri To Har Shaam Hai Tere Naam"
| Amit Kumar, Anuradha Paudwal
|-
| 5
| "Pardesi Lout Ke Aana (I)"
| Kumar Sanu, Anuradha Paudwal, Anoop Jalota 
|-
| 6
| "Pardesi Lout Ke Aana (II)"
| Anoop Jalota
|-
| 7
| "Sathiya Mujhe Neend Na Aaye Aajkal"
| Suresh Wadkar, Anuradha Paudwal
|-
| 8
| "Sathiya Sun Le Pukar"
| Debashish Dasgupta, Anuradha Paudwal
|-
| 9
| "Wallah Wallah Sare Gaon Mein Mach Gaya Halla"
| Anuradha Paudwal
|}

==References==
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Pardesi+%281993%29

==External links==
*  

 
 
 
 
 
 


 