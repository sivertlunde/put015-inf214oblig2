The Good Fairy (film)
{{Infobox film
| name           = The Good Fairy
| image          = Good Fairy cover.jpg
| image_size     =
| caption        = video cover
| director       = William Wyler
| producer       = Carl Laemmle Jr.
| writer         = Ferenc Molnár (play) Jane Hinton (translation) Preston Sturges (screenplay)
| narrator       =
| starring       = Margaret Sullavan Herbert Marshall Frank Morgan Reginald Owen
| music          = David Klatzkin Heinz Roemheld
| cinematography = Norbert Brodine
| editing        = Daniel Mandell
| distributor    = Universal Pictures
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} A jó Broadway in 1931.  The film was directed by William Wyler and stars Margaret Sullavan, Herbert Marshall, Frank Morgan and Reginald Owen.
 musical Make Make a Wish. TCM     In particular, Sturges added a movie-within-the-movie in which the actors communicate in one-syllable sentences. 

==Plot==
Luisa Ginglebusher (Margaret Sullavan) is a young, naive orphan who is given a job as an usherette in a Budapest movie palace.  Detlaff (Reginald Owen), a waiter she meets in the theatre, takes her to an exclusive party where, to hold off the advances of Konrad (Frank Morgan), a meat-packing millionaire a little too fond of drink, she picks a name from the phone book to be her "husband", hoping to do a good deed and divert some of Konrads wealth to someone else.

When the lucky man, stuffy but poor Dr. Max Sporum (Herbert Marshall), gets a 5-year employment contract and a big bonus from Konrad, he thinks the millionaire is interested in him because of his ethical behavior, diligent hard work and integrity, but actually Konrad plans to send the "husband" to South America so that he will be free to seduce the girl. Many complications ensue when Lu gets curious about Sporum, and pays him a visit. Erickson, Hal     

==Cast==
*Margaret Sullavan as Luisa "Lu" Ginglebusher
*Herbert Marshall as Dr. Max Sporum
*Frank Morgan as Konrad
*Reginald Owen as Detlaff, the Waiter
*Eric Blore as Dr. Metz
*Beulah Bondi as Dr. Schultz Alan Hale as Maurice Schlapkohl
*Cesar Romero as Joe
*Luis Alberni as The Barber
*June Clayworth as Mitzi, on-screen actress

Cast notes:
*Future film musical star Ann Miller, who it was once claimed had the worlds fastest feet when tap dancing, has an uncredited bit part, her second film appearance. 
*Future child star Jane Withers appears as a child in the orphanage sequence.  At only nine years old, it was already her seventh film appearance.  McHugh acting family has a small uncredited part as a moving man. 

==Production==
 eloped to Yuma, Arizona and got married.   Their marriage would last two years.

There were problems between the studio, Universal Pictures|Universal, and the films principals.  Despite complaints that Wyler was taking too much time because of multiple retakes of scenes involving Sullavan, especially close-ups, Sturges was keeping only a day or so ahead of the shooting, writing new scenes and feeding them to Wyler to shoot "off the cuff".  Eventually both Wyler and Sturges were dropped from the studio payroll. 

Filming had begun before the script had received formal approval from the Hays Office, which objected to some scenes and many lines in the submitted original, but allowed filming to start on assurance from the studio that changes would be made. The censors particularly objected to a scene in which the head of the orphanage explains the "facts of life" to Luisa before she leaves, to the attempted seduction of the girl by Konrad, the millionaire, and to there being a sofa in the room when Lu goes to Konrads apartment. 

The Good Fairy premiered in New York City on January 31, 1935,  in Hollywood on February 12, and went into general release on February 18.   It was the first film to be booked into Radio City Music Hall without first being previewed. 

==Other versions and adaptations== Broadway with musical Make Make a Wish, which had music and lyrics by Hugh Martin. 
 Maurice Evans, George Schaefer, and starring Julie Harris, Walter Slezak and Cyril Ritchard. 

==Notes==
 

==External links==
* 
* 
* 
* 
* 
*  on Screen Guild Theater: July 31, 1944

 
 

 
 
 
 
 
 
 
 
 
 