Chinna Mapillai
{{Infobox film
| name = Chinna Mapillai
| image = Chinna Mapillai.jpg
| caption =
| director = Santhana Bharathi
| writer = Crazy Mohan(dialogues)
| screenplay = Santhana Bharathi
| story = P. Kalaimani Sukanya  Anand  Sivaranjani
| producer = T. Siva
| music = Ilaiyaraaja
| cinematography = Ravishankar
| editing = G. Jayachandran
| studio = Amma creations
| distributor = Amma creations
| released = 14 January 1993
| runtime =
| country = India
| language = Tamil
}} Sukanya in lead roles. The film was a commercial success upon release in January 1993.  It was remade in Telugu as Chinna Alludu (1993), in Hindi as Coolie No.1 (1995)  and in Kannada as Coolie Raja (1999).

==Plot==
Prabhu is a kind-hearted poor man who works as a coolie in bus stand. Visu, a marriage broker wants him to pose as a rich man from Singapore and marry Sukanya to break the arrogance and her money-minded father (Radha Ravi). Her father finds out that he is a coolie. But Prabhu brilliantly lies that it is his younger brother. How he reforms both of them forms the story.

==Cast==
* Prabhu  as Thangadurai
* Radha Ravi as Periya Pannai
* Visu as Broker Ambalavanan Sukanya as Janaki
* Vinu Chakravarthy as Anands father Anand as Mythilis husband Sivaranjani as Mythili
* Kavithalaya Krishnan

==Soundtrack== Vaali and Gangai Amaran.
* "Vaanam Vazhthida" - S. P. Balasubramaniam, S. Janaki
* "Vennilavu Kothipathana" - Mano (singer)|Mano, Swarnalatha
* "Kaatu Kuyil Paatu" - Mano, Swarnalatha
* "Kadhoram Loolakku" - Mano, S. Janaki
* "Kanmanikkul Chinna" - S. P. Balasubramaniam, Uma Ramanan, Minmini
* "Ada Mama Nee" - Mano

==References==
 

==External links==
*  

 
 
 
 
 
 


 