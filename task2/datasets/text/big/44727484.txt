Poli Huduga
{{Infobox film|
| name = Poli Huduga
| image = 
| caption = 
| director = S. S. Ravichandra
| story  = Paruchuri Brothers
| writer =  Tara
| producer = K. Janakiram
| music = Hamsalekha
| cinematography = B. Purushottham
| editing = Suresh Urs
| studio = Sri Productions
| released =  
| runtime = 139 minutes Kannada
| country = India
| budget =
}}
 Tara and Devaraj in the lead roles while many other prominent actors featured in supporting roles.  The soundtrack and score composition was by Hamsalekha.

== Cast ==
* V. Ravichandran 
* Karishma Tara 
* Devaraj
* Thoogudeepa Srinivas
* Jaggesh
* Mukhyamantri Chandru
* Doddanna
* Sundar Krishna Urs
* Umashri
* Avinash
* Dinesh
* Mysore Lokesh
* Jyothi

== Soundtrack ==
The music was composed and lyrics written by Hamsalekha and audio was bought by Lahari Music. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Yaaru Poli
| extra1 = S. P. Balasubramanyam, Manjula Gururaj
| lyrics1 = Hamsalekha
| length1 = 
| title2 = Aa Suryana Sutthodu
| extra2 = S. P. Balasubramanyam, Vani Jairam
| lyrics2 = Hamsalekha 
| length2 = 
| title3 = Kuhu Kuhu Kogile
| extra3 = S. P. Balasubrahmanyam, Lata Hamsalekha
| lyrics3 = Hamsalekha 
| length3 = 
| title4 = Janana Marana
| extra4 = S. P. Balasubramanyam
| lyrics4 = Hamsalekha 
| length4 = 
| title5 = Jokumarane
| extra5 = S. P. Balasubramanyam, Vani Jairam
| lyrics5 = Hamsalekha 
| length5 = 
| title6 = Mugiyitu Aa Kaalavu
| extra6 = S. P. Balasubramanyam
| lyrics6 = Hamsalekha 
| length6 = 
}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 