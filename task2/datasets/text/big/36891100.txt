Hold 'Em Jail
{{Infobox film
| name           = Hold Em Jail
| image          =
| image_size     =
| caption        =
| director       = Norman Taurog
| producer       = Harry Joe Brown (associate producer) David O. Selznick (executive producer)
| writer         = Walter DeLeon (screenplay) S.J. Perelman (screenplay) Eddie Welch (screenplay) Mark Sandrich (screenplay) Tim Whelan (story) Lew Lupton (story) John P. Medbury (radio dialogue) Albert Ray (continuity)
| starring       = Wheeler and Woolsey Edna May Oliver Edgar Kennedy Betty Grable
| music          = Max Steiner
| cinematography = Leonard Smith Arthur Roberts RKO Radio Pictures
| released       =  
| runtime        = September 16, 1932
| country        = United States
| language       = English
| budget         = $408,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p57 
| gross          = $511,000 
}}
Hold Em Jail is a 1932 comedy film starring Wheeler and Woolsey.

==Cast==
* Bert Wheeler as Curly Harris
* Robert Woolsey as Spider Robbins
* Edna May Oliver as Violet Jones Robert Armstrong as Radio Announcer
* Roscoe Ates as Sam
* Edgar Kennedy as Warden Elmer Jones
* Betty Grable as Barbara Jones
* Warren Hymer as Steele
* Paul Hurst as Butch
* G. Pat Collins as Whitey
* Stanley Blystone as Kravette
* Jed Prouty as Warden Charles Clark
* Spencer Charters as the Governor
* John Sheehan as Mike Maloney

==Box Office==
According to RKO records the film recorded a loss of $55,000. 

==References==
 

==External links==
*  at IMDB

 