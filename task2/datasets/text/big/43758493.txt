Man Hunt (1933 film)
{{Infobox film
| name           = Man Hunt
| image          = 
| alt            = 
| caption        = 
| director       = Irving Cummings
| producer       = J.G. Bachmann
| writer         = Sam Mintz Leonard Praskins
| starring       = Junior Durkin Charlotte Henry Dorothy Davenport Arthur Vinton Edward LeSaint Howard Jackson
| cinematography = Joseph A. Valentine William Morgan
| studio         = J.G. Bachmann Productions
| distributor    = RKO Pictures
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Man Hunt is a 1933 American Mystery film directed by Irving Cummings and written by Sam Mintz and Leonard Praskins. The film stars Junior Durkin, Charlotte Henry, Dorothy Davenport, Arthur Vinton and Edward LeSaint. The film was released on May 24, 1933, by RKO Pictures.         

==Plot==
 

== Cast ==
*Junior Durkin as William Junior Scott, Jr.
*Charlotte Henry as Josie Woodward
*Dorothy Davenport as Mrs. Scott
*Arthur Vinton as James J. Wilkie
*Edward LeSaint as Henry Woodward
*Richard Carle as Sheriff Bascom
*Carl Gross as Abraham Jones

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 


 