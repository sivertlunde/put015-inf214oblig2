Bibi Fricotin
{{Infobox film
| name           = Bibi Fricotin
| image          = 
| caption        = 
| director       = Marcel Blistène
| producer       = 
| writer         = Arthur Harfaux
| starring       = Maurice Baquet Louis de Funès
| music          = Jacques Besse
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1951
| runtime        = 89 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy film from 1950, directed by Marcel Blistène, written by Arthur Harfaux, and starring Maurice Baquet. 

== Cast ==
* Maurice Baquet: Bibi Fricotin
* Colette Darfeuil: Mrs Suzy Fatma
* Nicole Francis: Catherine
* Alexandre Rignault: Mr Tartazan Paul Demange: the museums curator
* Yves Robert: Antoine Gardon
* Milly Mathis: Mrs Tartazan (Catherines aunt)
* Jacques Famery: a reporter
* Louis de Funès (uncredited)

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 


 