Sadhu (film)
{{Infobox film
| name           = Sadhu
| image          = 
| image_size     =
| caption        =
| director       = P. Vasu
| producer       = Meera Vidhyasagar
| writer         = P. Vasu
| narrator       =
| starring       =  
| music          = Ilaiyaraaja
| cinematography = M. C. Sekar
| editing        = P. Mohan Raj
| studio         = Eashwara Chandra Combines
| distributor    = Eashwara Chandra Combines
| released       = 22 July 1994
| runtime        = 123 minutes
| country        = India
| language       = Tamil
| budget         =
| gross          =
}}
 Arjun and Raveena Tandon in the lead roles with Vijayakumar (actor)|Vijayakumar, Goundamani and Radha Ravi in pivotal roles.     The film marked the debut of Raveena Tandon in Tamil cinema. The film was written and directed by P. Vasu and Ilaiyaraaja provided the musical score. Upon release, Sadhu turned out to be a successful venture.  

==Cast== Arjun
* Raveena Tandon Vijayakumar
* Goundamani as Anjukolai Aarumugam
* Radha Ravi
* Rajeev
* Sethu Vinayagam
* Kavitha

==Soundtrack==
The soundtrack was composed by Ilaiyaraaja and had 4 songs.

{{Infobox album  		
| Name =	Sadhu	
| Type =	soundtrack	
| Longtype =		
| Artist =	Ilaiyaraaja	
| Cover =		
| Cover size =		
| Caption =		
| Released =    	
| Recorded =		
| Genre =		
| Length =     Tamil	
| Label =		
| Producer =		
| Reviews =		
| Compiler =		
| Misc =		
}}		
		
{{tracklist	
| collapsed =	
| headline =	
| extra_column =  Singers
| total_length =	
	
| all_writing =	 Vaali
| all_music =
	
| writing_credits =	
| lyrics_credits =	
| music_credits =	
	
| title1 = Ammamma Unnai
| note1 =	
| writer1 =	
| lyrics1 =	
| music1 =	
| extra1 = K. J. Yesudas
| length1 = 4:44
	
| title2 = Ithayame Oh
| note2 =	
| writer2 =	
| lyrics2 =	
| music2 =	
| extra2 = K. S. Chithra
| length2 = 6:35
	
| title3 = Oorariya Pereduththa	
| note3 =	
| writer3 =	
| lyrics3 =	
| music3 =	
| extra3 = Mano (singer)|Mano, K. S. Chithra
| length3 = 5:00
	
| title4 = Paddikirom
| note4 =	
| writer4 =	
| lyrics4 =	
| music4 =	
| extra4 = Mano (singer)|Mano, K. S. Chithra, Arun Mozhi
| length4 = 5:40
	
| title5 = Vitta Mochile
| note5 =	
| writer5 =	
| lyrics5 =	
| music5 =	
| extra5 = Mano (singer)|Mano, Swarnalatha
| length5 = 4:54
}}

==References==
 

==External links==
*  

 

 
 
 
 
 


 