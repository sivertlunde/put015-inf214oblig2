Murder Ahoy!
 
 
{{Infobox film
| name           = Murder Ahoy!
| image          = Murder Ahoy.jpg
| caption        = Theatrical release poster by Tom Jung George Pollock
| based on       =
| screenplay     = David Pursall Jack Seddon
| writer         = Agatha Christie (motifs)
| starring       = Margaret Rutherford Stringer Davis Lionel Jeffries Bud Tingwell
| music          = Ron Goodwin
| cinematography = Desmond Dickinson
| editing        = Ernest Walter
| studio         =
| distributor    = Metro-Goldwyn-Mayer
| runtime        = 93 min.
| released       =  
| country        = United Kingdom
| language       = English
}}

Murder Ahoy! is the last of four Miss Marple films made by Metro-Goldwyn-Mayer that starred Margaret Rutherford. As in the previous three, the actress plays Agatha Christies amateur sleuth Miss Jane Marple, with Bud Tingwell as (Chief) Inspector Craddock and Stringer Davis (Rutherfords real-life husband) playing Mr Stringer.

The film was made in 1964 and directed by George Pollock, with David Pursall and Jack Seddon credited with the script. The music was by Ron Goodwin.

Unlike the previous three films that were adapted from Christie novels – The 4.50 from Paddington (Murder, She Said – the only Miss Marple novel used), After the Funeral (a Poirot mystery, adapted for Miss Marple with the title Murder at the Gallop) and Mrs. McGintys Dead (another Poirot novel, adapted as Murder Most Foul) – this film used an original screenplay that was not based on any of Christies stories.

It does, however, employ elements of the Miss Marple story They Do It With Mirrors. Specifically, the Battledore is a training ship for teenage boys with criminal tendencies, who are supposedly being set on the straight and narrow path – when, in fact, one of the members of the crew is training them for careers in housebreaking. Likewise, in They Do It WIth Mirrors, Lewis Serrocold is running his wifes mansion, Stonygates, as a boarding school for delinquent youths, to straighten out their lives – but, in fact, he is training selected students to hone their criminal skills, not to give them up. That is the only element borrowed into the film from a Christie story.

There is also an entirely tongue-in-cheek reference to The Mousetrap, the Christie play that has been running continuously on the West End since 1952. Audiences who see The Mousetrap are asked to keep the ending a secret, so it is amusing when Rutherfords Miss Marple says that shes reading a "rattling-good detective yarn" and "I hope I wont be giving too much away if I say the answer is a mousetrap!"  She then notes that shell "say no more – otherwise, Ill spoil it for you!"

==Plot==
The action takes place mainly on board an old wooden-walled battleship, HMS Battledore, which has been purchased by a Trust for the rehabilitation of young criminals, and intended by the founder to put backbone into young jellyfish.

Shortly after joining the board of management of the Trust, Miss Marple (Margaret Rutherford) witnesses the sudden death of a fellow trustee, who has just returned from a surprise visit to the ship. She manages to obtain a small sample of his Snuff (tobacco)|snuff, which is found to have been poisoned. She visits the ship, much to the distress of the Captain and crew, who do not like visitors.

Her first night on board, one of the officers is murdered – run through with a sword and then hanged. As the police investigation proceeds, the assistant matron is killed, apparently by a poisoned mousetrap.

Miss Marple sets a trap by pretending to be left alone at night on the deserted ship, but secretly smuggling in Chief Inspector Craddock (Bud Tingwell) and another policeman. She finds a large sum of money hidden in a cannon which turns out to have been embezzled by Commander Breeze-Connington (William Mervyn), money he feels is owed to him after he was passed over for promotion. It was protected by a poisoned mousetrap. After the commander appears and admits all, Miss Marple calls out to the police inspector to make the arrest, but he and his colleague have accidentally been locked in and cannot help. Miss Marple and Breeze-Connington engage in a ferocious fencing match, before she is disarmed. Just as he is about to administer the coup de grace, however, he is hit over the head from behind by Mr. Stringer who, alarmed at what might be going on, had secretly rowed out in the dark.

The finale is a court martial for Captain Rhumstone who has been accused of mismanagement. Seeing the hilt of the sword toward him, he mistakenly thinks he has been found guilty but is corrected by Miss Marple. Nevertheless, he announces that he must resign just the same because Matron wants to get married, and so does he. He fully realises that it is a golden rule of the trust that there should be no hanky-panky between the sexes on board ship, so he makes his farewell and turns to go. Miss Marple stops him, saying, "I think I speak for my fellow trustees when I say that golden rule is hereby rescinded. Youre a fine sea dog captain, but it seems to me the Battledore could do with a womans hand at the helm." As Matron and the Captain wave good-bye to Miss Marple, he says, "As soon as I saw her, I said, What an old darling!"

==Cast==
* Margaret Rutherford – Miss Marple
* Lionel Jeffries – Captain Rhumstone
* Bud Tingwell – Chief Inspector Craddock
* William Mervyn – Commander Breeze-Connington
* Joan Benham – Matron Alice Fanbraid
* Stringer Davis – Mr. Stringer
* Nicholas Parsons – Dr. Crump
* Miles Malleson – Bishop Faulkner
* Henry Oscar – Lord Rudkin
* Derek Nimmo – Sub-Lieutenant Humbert
* Gerald Cross – Lieutenant Commander Dimchurch
* Norma Foster – Assistant Matron Shirley Boston
* Terence Edmond – Sergeant Bacon Francis Matthews – Lieutenant Compton Lucy Griffiths – Millie
* Bernard Adams – Dusty Miller
* Tony Quinn – Kelly
* Edna Petrie – Miss Pringle
* Roy Holder – Petty Officer Lamb
* Ivor Salter – Policeman Henry B. Longhurst – Cecil Ffolly-Hardwicke
* Desmond Roberts – Sir Geoffrey Bucknose

==References==
 	

==External links==
* 
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 