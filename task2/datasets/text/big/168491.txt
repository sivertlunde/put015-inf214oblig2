Reuben, Reuben
 
 
{{Infobox film
| name = Reuben, Reuben
| image = Reuben, Reuben - movieposter.jpg
| caption = Reuben, Reuben film poster
| director = Robert Ellis Miller
| producer = Julius J. Epstein Walter Shenson
| writer = Peter De Vries (novel) Julius J. Epstein Herman Shumlin (play)
| starring = Tom Conti Kelly McGillis
| music =
| cinematography =
| editing = TAFT Entertainment Pictures
| distributor = 20th Century Fox
| released =  
| runtime = 101 min.
| country = United States English
| budget =
}}
Reuben, Reuben is a 1983 comedy-drama film starring Tom Conti, Kelly McGillis (in her film debut), Roberts Blossom, Cynthia Harris, and Joel Fabiani.

The film was adapted by Julius J. Epstein from the play Spofford by Herman Shumlin, which in turn was adapted from the novel Reuben, Reuben by Peter De Vries. It was directed by Robert Ellis Miller.

The main character in DeVriess novel was based largely on the Welsh poet Dylan Thomas, who was a compulsive womanizer and lifelong alcoholic, finally succumbing to the effects of alcohol poisoning in November 1953, while on a speaking tour in America.

==Plot==
Gowan McGland (Tom Conti) is a creatively blocked Scottish poet who ekes out a day-to-day existence by exploiting the generosity of strangers in an affluent Connecticut suburb, where he recites his verse to various arts groups and womens clubs.  Gowan is something of a leech, cadging expensive dinners from well-off patrons (usually stealing the tips afterward) while seducing their bored wives and affecting a superior attitude toward the smug bourgeois types he exploits.

Although a talented poet, he is also a chronic drunk, indifferent to the wounds he can casually inflict with his wit. (When one of Gowans middle-aged conquests undresses for him, he mutters, "Deprived of their support, her breasts dropped like hanged men," reducing her to tears.)  

Gowan falls in love with a young college student, Geneva Spofford (Kelly McGillis), who has everything to lose from a relationship with a drunken deadbeat poet unable to hold a job.  Gowan instigates two ugly incidents that eventually cause their breakup: first, a bar fight from which Geneva rescues him, and later, when he causes a scene in a fancy restaurant where the waiters know hes stolen their tips.

He also suffers an ironic comeuppance from Dr. Jack Haxby (Joel Fabiani). The dentist, after finding out about the poets affair with his wife, uses the ruse of free dental care for ruining Gowans smile and forcing him to wear dentures. When Gowan finds out, it is already too late, and the damage is irrevocable.  (Gowan fears losing his teeth, equating it with death.)

Gowan prepares to hang himself, but while dictating his last thoughts into a tape recorder, he comes up with some good lines and regains his will to write. Unfortunately, his hosts pet dog, an old English sheepdog named Reuben, comes bounding into the room, causing Gowan to lose his balance before he can undo the noose, turning the aborted suicide into accidental asphyxiation. The films title comprises Gowans final words, an unsuccessful attempt to halt the dog.

==Awards== Best Actor Best Writing, Screenplay Based on Material from Another Medium.

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 