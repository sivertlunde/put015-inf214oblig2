03:34: Earthquake in Chile
{{Infobox film
| name           = 03:34: Earthquake in Chile
| image          = 
| alt            = 
| caption        = 
| director       = Juan Pablo Ternicier
| producer       = 
| writer         = Mateo Iribarren
| starring       = Andrea Freund Marcelo Alonso Loreto Aravena Fernando Gómez-Revira Eduardo Paxeco Andrés Reyes Gabriela Medina
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Chile
| language       = Spanish
| budget         = US dollar|US$ 1.4 million
| gross          = 
}} Chilean film directed by Juan Pablo Ternicier, and written by Mateo Iribarren, which will narrate three different stories, based on the 2010 Chile earthquake.  

The movie premiered in the town of Dichato, in the Biobío Region, on February 27, 2011,  exactly a year after the catastrophe, and will be released to the rest of Chile on March 3. The first teaser of 03:34 appeared in the Internet on January 17, 2011. 

Filming began in November 2010, in the same places that the catastrophe occurred, with the support of the National Council of Culture and the Arts, and Carabineros de Chile,       who helped with the recording in the affected zones. The movie had a budget of 1.4 million US dollars, and all money grossed by the movie will be given for the construction of schools in the most affected coastal zones.  

In January 2012, the film was selected to be preserved in the Library of the Congress of the United States catalog. 

== Plot ==
 s in Pichilemu, where part of the film is set.]]
The movie shows three stories of people that was affected by the disaster.   

The first of them, shows a woman (Andrea Freund) that goes from Pichilemu, OHiggins Region, to the devastated town of Dichato, Biobío Region, as her sons were there spending their vacations there with Manuel (Marcelo Alonso), their father.   

The second story shows the experience of a convict (Fernando Gómez-Revira), who escapes from the Chillán Prison to Concepción, Chile|Concepción, as his daughter was in the Alto Río building, which collapsed during the earthquake. 

The third story is developed in Dichato, where a group of young people (Loreto Aravena, Eduardo Paxeco, and Andrés Reyes (actor)|Andrés Reyes), while enjoying their last day of vacations in a party, experience the strong movement and tsunami that annihilates the place. 

==Cast==
 
* Andrea Freund
* Marcelo Alonso
* Loreto Aravena
* Fernando Gómez-Revira
* Eduardo Paxeco
* Andrés Reyes
* Gabriela Medina
* Berta Lasala
* Remigio Remedy
* María Paz Jorquera
* Hugo Medina
* Jorge Ali
* Jose Luis Bouchon
* Nathalia Aragonese
* Claudio Castellón
* Ernesto Anacona
* Julio Fuentes
 

==References==
 

==External links==
*    

 
 
 
 
 
 
 
 
 
 
 