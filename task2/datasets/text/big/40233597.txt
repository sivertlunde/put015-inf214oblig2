De tal palo tal astilla
{{Infobox film
| name           = De tal palo tal astilla
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Miguel M. Delgado
| producer       = Jesús Galindo
| writer         = José María Fernández Unsáin Eulalio González Alfredo Varela, Jr. Miguel M. Delgado
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Luis Aguilar Eulalio González Flor Silvestre Marina Camacho
| music          = Manuel Esperón
| cinematography = Agustín Jiménez
| editing        = Jorge Bustos
| studio         = Estudios Churubusco
| distributor    = Filmadora Chapultepec
| released       =  
| runtime        = 88 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}} western comedy Luis Aguilar, Eulalio González, Flor Silvestre, and Marina Camacho in the main roles. 

The films story was written by José María Fernández Unsáin, based on an original idea from Eulalio González, one of the films actors. Alfredo Varela, Jr. and Miguel M. Delgado wrote the adapted and technical screenplays, respectively. 

==Cast== Luis Aguilar as Miguel Marmolejo
*Eulalio González as Gumaro Malacara (as Lalo Gonzalez Piporro)
*Flor Silvestre as Elena
*Marina Camacho as Rosa
*León Barroso as Sóstenes Morales
*José Jasso as Cantinero
*Roberto Meyer as Juez de Registro Civil
*Armando Gutiérrez as Notario
*José Chávez as Chalío (credited as Jose T. Chavez)
*Francisco Meneses		
*Carlos Guarneros as Mensajero de Chalío
*Aurelio Salinas		
*Lupe Carriles as Doña Eulalia

==Production==
Principal photography began on June 1, 1959, in Estudios Churubusco.   

==Release==
De tal palo tal astilla premiered at the Orfeón theater in Mexico City on October 20, 1960 during one week. 

==References==
 

==External links==
* 