Woman of Tokyo
{{Infobox film
| name           = Woman of Tokyo
| image          = 
| caption        = 
| film name = {{Film name| kanji          = 東京の女
| romaji         = Tokyo no Onna}}
| director       = Yasujirō Ozu
| producer       = 
| writer         = Kōgo Noda Tadao Ikeda
| starring       = Yoshiko Okada Ureo Egawa
| music          = 
| cinematography = Hideo Mohara
| art director    = Takashi Kanesu
| editing        = Kazuo Ishikawa
| studio         = Shochiku/Kamata
| distributor    = 
| released       =  
| runtime        = 45 mins
| country        = Japan
| language       = Japanese
| budget         = 
| gross          =
}}
  is a 1933 Japanese film directed by Yasujirō Ozu. The films working title was Her Case, For Example (例えば彼女の場合 Tatoeba kanojo no baai)   

The film tells of a student whose sister supports his studies by moonlighting as a translator. When he hears that, in reality, she is working as a hostess in a seedy dance-hall, he is devastated. 

==Plot==

Ryoichi (Ureo Egawa), a student, and Chikako (Yoshiko Okada), his older sister, go through their morning routine. Chikako gives Ryoichi his pocket money and he leaves for university.

At Chikakos office, a police officer asks Chikakos boss for her employment records. The boss tells him that Chikako also works evenings for a professor. Chikako remains unaware that she is under investigation.

Ryoichi and his girlfriend, Harue (Kinuyo Tanaka), are at the cinema watching If I Had a Million. Afterwards, Harue returns home and talks with her brother, Kinoshita (Shinyo Nara). Kinoshita reveals that he has heard a rumour that Chikako is not assisting a professor but rather working nights at a cabaret bar. He also implies that she may be a prostitute.

Harue visits Ryoichi and tells him what she has heard about Chikako. At first Ryoichi laughs, before becoming angry and demanding that Harue leave.

Chikako touches up her makeup at the cabaret bar, and then phones Ryoichi to tell him that she will be late home. Ryoichi hangs up and Chikako gets into the back of a car with a customer.

Chikako returns home later that night to find Ryoichi still awake. He confronts her, demanding that she give up her job at the bar. When Chikako fails to justify her behaviour, Ryoichi repeatedly strikes her. Chikako implores Ryoichi to work hard at university, thereby making her hardships worthwhile. Ryoichi storms out.

The next morning, Kinoshita dons his police officers uniform. He tells Harue that she should have let him talk to Ryoichi, and then leaves for work. Chikako comes to the door looking for Ryoichi, since he did not return home the previous night. Harue invites Chikako inside and tearfully reveals that she told Ryoichi what she had heard about Chikakos job. Harue takes a phone call from Kinoshita, who has arrived at the police station. He tells her that Ryoichi has committed suicide. Harue informs Chikako that her brother is dead.

Chikako talks with three newspaper reporters. One of them enters her house uninvited and questions Harue, who is kneeling before Ryoichis body. Eventually the reporters leave, muttering "theres no scoop here". As Harue weeps, Chikako insults Ryoichi, calling him a "weakling" for his unwillingness to comprehend his sisters motives.

The reporters walk down the street, laughing and joking. They see a poster announcing that a gang of criminals has been apprehended, and one tells the other that his paper was first to the story.

==Production==

Woman of Tokyo was shot in nine days or fewer, during a gap in Shochikus production schedule. Ozu had a week or so free before he was due to begin production on his next feature, Dragnet Girl, and was drafted in to direct. The credits indicate that the screenplay, by regular Ozu collaborators Kogo Noda and Tadao Ikeda, was based on the novel "Twenty-Six Hours" by Ernest Schwartz; neither Schwartz nor the novel ever existed. 

The script originally suggested that Chikako was contributing her illicit earnings to the Communist Party, but these political elements were excised before shooting began. The reasons for this are unknown, although Ozu scholar Tony Rayns has suggested either Ozu or the studio were responsible, possibly to avoid the attentions of the censor. 

When Ryoichi and Harue visit the cinema, they watch the film If I Had a Million. Ozu includes material from the Ernst Lubitsch section of this anthology picture, in which Charles Laughton, an office worker, barges into his bosss office. Ozu cuts away before the climax of the scene, when Laughton stands before his boss and blows a raspberry.   

==Cast==

* Yoshiko Okada as Chikako
* Ureo Egawa as Ryoichi, Chikakos brother
* Kinuyo Tanaka as Harue, Ryoichis girlfriend
* Shinyo Nara as Kinoshita, Harues brother
* Chishū Ryū as a reporter

==Home media==
 Early Spring, as Three Melodramas. 

==References==
 
 

 

 

 
 
 
 
 
 
 
 