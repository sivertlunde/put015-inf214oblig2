The Hero of Color City
{{Infobox film
| name           = The Hero of Color City
| image          = The Hero of Color City poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Frank Gladstone 
| producer       = John D. Eraklis Max Howard
| screenplay     = Jess Kedward J.P. McCormick Kirsty Peart Rich Raczelowski Evan Spiliotopoulos
| starring       = Christina Ricci Sean Astin Owen Wilson Elizabeth Daily Jessica Capshaw Rosie Perez Tara Strong Craig Ferguson Wayne Brady Jess Harnell David Kaye
| music          = Zoë Poledouris Angel Roché Jr. 
| cinematography = 
| editing        = 
| studio         = Exodus Film Group
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $32,048 
}}

The Hero of Color City is a 2014 American computer-animated comedy film directed by Frank Gladstone and written by Jess Kedward, J.P. McCormick, Kirsty Peart, Rich Raczelowski and Evan Spiliotopoulos. The film stars Christina Ricci, Sean Astin, Owen Wilson, Elizabeth Daily, Jessica Capshaw, Rosie Perez, Tara Strong, Craig Ferguson, Wayne Brady, Jess Harnell and David Kaye. The film was released theatrically on October 3, 2014, by Magnolia Pictures.

==Plot==
Every night when Ben goes to bed, his crayons turn alive and set out to Color city. Yellow, a timid crayon afraid of everything, accidentally summons two unfinished drawings known as King Scrawl and his sidekick Gnat. Now the crayons must act fast and save their town before their colors fade.

== Cast ==
*Christina Ricci as Yellow
**Tara Strong as Yellows singing voice
*Sean Astin as Horatio
*Owen Wilson as Ricky The Dragon
*Elizabeth Daily as Ben
*Jessica Capshaw as Duck
*Rosie Perez as Red
*Craig Ferguson as Nat
*Wayne Brady as Blue
*Jess Harnell as Green
*David Kaye as Black/King Scrawl
*Jeremy Guskin as Professor Heliotrope
*Frank Gladstone as Astronaut
*Sophia Eraklis as Purple
*Zoë Bright as Madame Pink
*Tom Lowell as Brown
*Laura Lane as Horse
*Zoe Bright as Opera Singer
*Josh Gladstone as Cow

==Reception==
The Hero of Color City was critically panned. On Rotten Tomatoes, the film has a rating of 21%, based on 17 reviews, with an average rating of 3.5/10.  On Metacritic, the film has a rating of 33 out of 100, based on 12 critics, indicating "generally unfavorable reviews". 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 