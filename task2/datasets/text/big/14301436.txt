Follow the Crowd (film)
 
{{Infobox film
| name           = Follow the Crowd
| image          = Follow the Crowd 1918 movieposter.jpg
| caption        = Poster for the film.
| director       = Alfred J. Goulding
| producer       = Hal Roach
| writer         = 
| starring       = Harold Lloyd
| music          = 
| cinematography = 
| editing        = 
| studio         = Rolin Film Company
| distributor    = Pathé Exchange
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 short comedy film with Harold Lloyd.    Previously thought to be a lost film, the SilentEra website says now that a "print exists". 

==Cast==
* Harold Lloyd 
* Snub Pollard 
* Bebe Daniels 
* William Blaisdell
* Billy Fay - (as B. Fay)
* Helen Gilmore
* Dee Lampton
* Gus Leonard
* Marvin Loback
* James Parrott Charles Stevenson
* Dorothea Wolbert

==See also==
* List of American films of 1918
* Harold Lloyd filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 