I'll See You in My Dreams (1951 film)
{{Infobox film
| name           = Ill See You in My Dreams
| image          = Ill See You in My Dreams poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Michael Curtiz
| producer       = Louis F. Edelman
| based on       = The Gus Kahn Story Jack Rose Melville Shavelson
| writer         = Louis F. Edelman Grace Kahn
| narrator       =
| starring       = Doris Day Danny Thomas Frank Lovejoy Patrice Wymore James Gleason
| music          = Gus Kahn
| cinematography = Ted D. McCord
| editing        = Owen Marks
| distributor    = Warner Bros.
| released       =  
| runtime        = 110 minutes
| country        = United States English
| budget         =
| gross          = $2.9 million (US rentals) 
}}

Ill See You in My Dreams is a 1951 musical film starring Doris Day and Danny Thomas, directed by Michael Curtiz.
 The Jazz Singer. 

==Plot summary==
  Pretty Baby, My Buddy, Toot Toot Tootsie and "Makin Whoopee, only to go into eclipse when he loses his savings in the 1929 stock-market crash. 

==Cast==
* Doris Day as Grace LeBoy Kahn  
* Danny Thomas as Gus Kahn 
* Frank Lovejoy as Walter Donaldson
* Patrice Wymore as Gloria Knight (singing voice was dubbed by Bonnie Lou Williams)  
* James Gleason as Fred Thompson  
* Mary Wickes as Anna  
* Julie Oshins as Johnny Martin  
* Jim Backus as Sam Harris  
* Minna Gombell as Mrs. LeBoy  
* Harry Antrim as Mr. LeBoy   William Forrest as Florenz Ziegfeld, Jr.  
* Bunny Lewbel as Irene, at age 6  
* Robert Lyden as Donald, at age 8  
* Mimi Gibson as Irene, at age 3   Christopher Olsen as Donald, at age 4 (as Christy Olson)

==Notes== album of the same name was released by Columbia Records, containing songs sung by Day (some of them duets with Thomas) in the film.

==References==
 

==External links==
*  
*  
*  
*   
*  

 

 
 
 
 
 
 
 
 
 