Yakeen (2005 film)
{{Infobox film
| name           = Yakeen
| image          = Yakeen2005.jpg
| caption        = Theatrical release poster
| director       = Girish Dhamija
| producer       = Sujit Kumar Singh
| story          = Vikram Bhatt
| screenplay     = Vikram Bhatt
| starring       = Arjun Rampal Priyanka Chopra
| music          = Himesh Reshammiya
| cinematography = Anshuman Mahaley
| editing        = Kuldeep Mehan
| distributor    = Shreya Creations P R Films
| released       =  
| runtime        = 116 minutes
| country        = India
| language       = Hindi
| budget         =4 crore
| preceded by    =
| followed by    =
}} 2005 Bollywood romantic  mystery thriller film directed by Girish Dhamija, starring Arjun Rampal, Priyanka Chopra in the lead roles while Kim Sharma, Sudhanshu Pandey and Saurabh Shukla are the supporting and special appearances respectively.   It also has a guest appearance by Ankur Nayyar. The films screenplay was written by Vikram Bhatt.  The film is much inspired from the 1991 Hollywood film Shattered (1991 film)|Shattered. The films premise centres on a man who recovers from an accident with amnesia, and as he starts to piece together his former life he feels that something is wrong.Yakeen was an above average grosser at the box office. 

==Plot==
 
The film begins with the recovery of Simar (Priyanka Chopra) and her husband Nikhil (Arjun Rampal) from a car accident. Simar is almost unscratched but Nikhil has amnesia, along with major physical damage. Simar is determined that Nikhil will get better and resume his life, and brings in the best surgeons to reconstruct his damaged leg and face, and then later helping him with his physical therapy.

Although Nikhil is at first cautious, Simars love and dedication wins him over. After six months of therapy he is released from the hospital and the couple return home. However, when Nikhil meets up with his old friends and goes through his personal files, he finds evidence that all was not well in his previous life. Now uncertain of his wifes true motives, Nikhil starts retracing his steps from the night of the accident, meeting with his old friend Tanya (Kim Sharma) who tells him that on the night of the accident, he had planned to confront and divorce Simar for infidelity.

Nikhil hires a private investigator named Chamanlal (Saurabh Shukla) who discovers that the accusation is true, and Simar had been having an affair with a man named "Kabir". One day Nikhil secretly overhears Simar talking on the phone with Kabir, in which she insists that their relationship is over because she has fallen back in love with Nikhil. However, Simar agrees to meet Kabir one last time. Nikhil follows Simar to a secluded house, where he witnesses an apparent argument between Simar and Kabir. After a chase through the woods, it is revealed that Simar staged the entire thing: Kabir was never on the phone nor in the house, because he has been dead the whole time. Simar confesses that on the night of the  accident, Nikhil had found Simar and Kabir together, and in his rage he killed Kabir. Simar wanted to protect Nikhil from the guilt, since shed believed his amnesia had been a chance to start their relationship anew. Nikhil believes this story, and is temporarily reconciled with Simar.

Nikhil is then contacted by Tanya, who claims that she has some new important information. When he goes to her house, he finds her shot dead. Nikhil informs Chamanlal that he has been having more vivid flashbacks to the night of the accident, and has an idea where Kabir was buried. They drive out and dig up the body, which has been preserved in the snow. However, the corpses face is exactly the same as Nikhils.

"Nikhil" realises that he is in fact Kabir, and had been the one having an affair with Simar. Through flashback, it is revealed that the real Nikhil was emotionally abusive towards Simar. Kabir (Sudhanshu Pandey, but with Arjun Rampals voice) witnessed Simar being abandoned by Nikhil at a party one night, and picked her up. They began their affair, but after a while Simar became too possessive and obsessed with him. On the night of the accident, Kabir had been trying to leave Simar, who threatened to commit suicide if he did. Just then Nikhil arrived and started a fight with Kabir, which ended when Simar killed Nikhil with her gun. They drove out buried the body in the snow, but on the way back Kabir blamed Simar for the murder and declared that they should go to the police. In the ensuing argument, Kabir lost control of the car, which resulted in the accident at the beginning of the film.
 plastic surgeons to reconstruct his face in Nikhils likeness. He and Chamanlal decide to bring Nikhils body to the police, but are stopped when Simar arrives and shoots Chamanlal. Simar still wants Kabir to accept Nikhils identity so they can be together, but Kabir refuses. Simar tries to kill them both in another accident so they can be together in death; Kabir manages to escape, but Simar does not.
 end credits begin over footage of Kabir explaining his story to the officers.

==Cast==
* Arjun Rampal as Nikhil Oberoi
* Priyanka Chopra as Simar Oberoi
*Ankur Nayyar  as Siddhartha Thakur
* Kim Sharma as Tanya Thakur
* Sudhanshu Pandey as Kabir
* Saurabh Shukla as Chamanlal

==Soundtrack==
{{Infobox album  
| Name        = Yakeen
| Type        = Soundtrack
| Artist      = Himesh Reshammiya
| Cover       = Yakeen_albumcover.jpg
| Released    =   
| Recorded    = Feature film soundtrack
| Length      =
| Label       = T-Series
| Producer    =
| Reviews     =
| Last album  = Main Aisa Hi Hoon (2005)
| This album  = Yakeen (2005)
| Next album  = Silsiilay  (2005)
}}

The music is composed by Himesh Reshammiya and the lyrics are penned by Sameer. The album has eight tracks, including one instrumental and two reprise tracks. 

===Track listing===

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Title || Artist(s) || Length
|-
| 1 || "Tune Mujhko" || Udit Narayan, Alka Yagnik || 6:19
|-
| 2 || "Meri Aankhon Mein" || Udit Narayan, Alka Yagnik || 6:29
|-
| 3 || "Tu Hi" ||  Sonu Nigam, Shreya Ghoshal || 6:02
|- Shaan || 6:19
|-
| 5 || "Chehra Tera" || Sonu Nigam, Alka Yagnik ||  5:38
|-
| 6 || "Meri Aankhon Mein" || Instrumental || 6:29
|-
| 7 || "Bhoolna Nahin - II" || Sunidhi Chauhan || 5:04
|- Shaan || 6:15
|}

==References==
 

==External links==
*  

 
 
 
 
 