Last Woman on Earth
{{Infobox film
| name           = Last Woman on Earth
| image          = Lastwomanonearth.jpg
| alt            =
| caption        = Film poster by Albert Kallis
| director       = Roger Corman
| producer       = Roger Corman Charles Hannawalt
| screenplay     = Robert Towne
| starring       = Betsy Jones-Moreland Antony Carbone Robert Towne
| music          = Ronald Stein
| cinematography = Jacques R. Marquette
| editing        = Anthony Carras
| distributor    = Filmgroup
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} American science-fiction apocalypse which The Little Shop of Horrors.

==Plot==
  Edward Wain), Harolds lawyer, who has come to discuss the latest indictment.

Harold invites him along on a boat trip during which all three try out some newly bought scuba diving equipment. When they resurface, they are unable to breathe without using their scuba tanks. They climb back into their boat and find Manuel, the crewman, dead, apparently of asphyxiation. Upon rowing ashore, they enter the jungle. With their air running out, they discover that the foliage gives off oxygen they can breathe.

When they go into town, they find nobody left alive, and they cannot contact anyone by radio. It dawns upon the three that they might be the only survivors in the area, maybe in the world. The domineering Harold takes charge. They becoming self-sufficient; the two men fish&mdash;marine creatures have survived. Later, they also find living insects and baby chicks, presumably newly hatched. Harold feels that in the long run they will have to move north to a colder climate&mdash;to avoid an insect problem and also problems with food preservation and to increase their chances of meeting other survivors.

Soon, a love triangle develops. Martin points out to Harold that neither the latters marriage certificate nor his money mean anything any more. One day, while Harold is out fishing by himself, Evelyn gives in to her attraction to Martin. When Harold finds out, he beats Martin and orders him to leave. Evelyn hops in the car, and the two lovers drive off. Harold chases after them. At the harbour, another fight breaks out when Martin refuses to tell Harold where his wife is. Martin eventually runs into the church where Evelyn has been waiting. There, he dies of his injuries. The two survivors are left wondering where they will go or what they will do now.

==Cast==
* Betsy Jones-Moreland as Evelyn Gern
* Antony Carbone as Harold Gern
* Robert Towne as Martin Joyce

==Home media==
The film is in the public domain, and several DVD editions exist, including one by Alpha Video. Most are copies of black and white 16mm prints struck for television but a faded colour print is carried by the Internet Archive. The Retromedia release is transferred from a color-corrected 35&nbsp;mm print. This was released on DVD through Image Entertainment, featuring introductions by Corman, with commentary tracks by Jones-Moreland and Carbone. The release also features the other two entries in Cormans "Puerto Rico Trilogy" – Creature from the Haunted Sea and Battle of Blood Island – films shot back to back with Last Woman.

==See also==
*Five (1951 film)|Five, 1951 film directed by Arch Oboler
* List of films in the public domain in the United States
* Survival Island, 2005 film directed by Stewart Raffill The World, the Flesh and the Devil, 1959 film directed by Ranald MacDougall

==References==
 

==External links==
 
*  
*  
*   (from faded colour print)
*   (higher resolution, from higher-quality black-and-white print)
*   DVD review

 

 
 
 
 
 
 
 
 
 
 
 
 