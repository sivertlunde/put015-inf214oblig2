Dekh Kabira Roya (1957 film)
Dekh Amiya Chakravarty.

==Plot summary==
The Bollywood classic is a farcical comedy about the vicissitudes of modern love. Mohan (Anoop Kumar), Pradeep (Daljit), and Ranjeet (Jawahar Kaul) are three struggling artists—singer, painter, and writer, respectively—who live together in the same rooming house. Romantic hijinks ensue when the trio meet three lovely girls, Geeta (Ameeta), Rekha (Anita Guha), and Kalpana (Shubha Khote), with very specific ideas about art and love.   The three male friends decide to help each other out by using their talents, and utter and hilarious chaos reigns. 

==Cast==
Anita Guha as Rekha

Anoop Kumar as Mohan

Jawahar Kaul as Pradeep

Ameeta as Geeta

Daljeet as Ranjeet

Shubha Khote as Kalpana

Shivraj as Rekhas Father

Praveen Paul as Geetas Mother
 Sunder as the coffee house waiter who plays a central role in disentangling the situation

==Crew==

Director: Amiya Chakrabarty

Editor: C. Ramarao

Writer: Chandrakant, Amiya Chakrabarty, Manoranjan Ghose

Producer: Amiya Chakrabarty

Music: Madan Mohan

Cinematographer: Ajit Kumar  

== Music ==
 Madan Mohan. Songs are penned by Rajendra Krishan. Following is the list of songs featured in this film.

{|border="2" cellpadding="3" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px # aaa solid; border-collapse: collapse; font-size: 95%"
|-bgcolor="#CCCCCC" align="center"
!Song!! Singer(s)!! Lyricists!! Music Director
|- Lagan Tose Madan Mohan
|- Meri Veena Tum Bin Roye || Lata Mangeshkar || Rajendra Krishan || Madan Mohan
|- Hum Panchhi Mastaane || Lata Mangeshkar Geeta Dutt & Seeta || Rajendra Krishan || Madan Mohan
|- Humse Aaya Na Gaya || Talat Mahmood || Rajendra Krishan || Madan Mohan
|- Tum Meri Raakho Laaj Hari || Sudha Malhotra || Rajendra Krishan || Madan Mohan
|- Tu Pyar Kare Ya Thukraye || Lata Mangeshkar || Rajendra Krishan || Madan Mohan
|- Ashqon Se Teri Humne || Asha Bhosle || Rajendra Krishan || Madan Mohan
|- Hum Bulate Hi Rahe || Asha Bhosle & Mohammad Rafi || Rajendra Krishan || Madan Mohan
|- Bairan Ho Gayi Rain || Manna Dey || Rajendra Krishan || Madan Mohan
|- Kaun Aaya, Mere Man Ke Dwaare || Manna Dey || Rajendra Krishan || Madan Mohan
|}

==References==
 

==External links==
*  

 
 
 
 
 
 


 
 