Hold That Co-ed
{{Infobox film
| name           = Hold That Co-ed
| image          = 
| alt            = 
| caption        =  George Marshall
| producer       = David Hempstead
| story          = Karl Tunberg Don Ettlinger
| screenplay     = Jack Yellen
| starring       = John Barrymore George Murphy Marjorie Weaver
| music          = Arthur Lange
| cinematography = Robert H. Planck
| editing        = Louis R. Loeffler
| studio         = 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} 1938 comedy George Marshall, starring John Barrymore, George Murphy and Marjorie Weaver.

==Plot summary==
 

==Cast==
 
* John Barrymore as Governor Gabby Harrigan
* George Murphy as Rusty Stevens
* Marjorie Weaver as Marjorie Blake
* Joan Davis as Lizzie Olsen
* Jack Haley as Wilbur Peters
* George Barbier as Major Hubert Breckenridge
* Ruth Terry as Edie
* Donald Meek as Dean Fletcher
* Johnny Downs as Dink Paul Hurst as Slapsy
* Guinn Big Boy Williams as Mike Wurgeski (as Guinn Williams)
* William Billy Benedict as Sylvester (as Bill Benedict)
* Frank Sully as Steve Wurgeski
* Charles C. Wilson as Coach Burke
* Glenn Morris as Spencer
 

==References==
 	

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 


 
 
 