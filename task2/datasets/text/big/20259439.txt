Feast 3: The Happy Finish
 
{{Infobox film
| name          = Feast 3: The Happy Finish
| image         = Feast 3 poster.jpg
| director      = John Gulager
| producer      = Michael Leahy Ron Cosmo Vecchiarelli
| writer        = Marcus Dunstan Patrick Melton Jenny Wade Diane Ayala Carl Anthony Payne Tom Gulager Hanna Putnam Juan Longoria Garcia William Prael John Allen Nelson Josh Blue Craig Henningsen
| cinematography = Kevin Atkinson
| country       = United States
| language      = English 
| released      =  
}}
Feast 3: Happy Finish is the third installment of the Feast (2005 film)|Feast trilogy. The film was released, direct to DVD, on February 17, 2009. 

==Plot==

The film picks up where the previous film left off. Honey Pie is mauled and beheaded by a monster who quickly digests and excretes her head. Lightning survived the explosion near him and stumbles off. The remaining survivors (Biker Queen, Tat Girl, Tit Girl, Secrets, Bartender, Slasher and Greg now with a pipe lodged in his head) are then attacked by monsters on the roof of the building they are hiding on. Thunder is seen still alive and crawling away after being disemboweled, but is run over by a man named Shitkicker.

Shitkicker smashes open the front door of the police station and enters. The rooftop survivors make their way to the jail, where Hobo attempts to kill them, but literally gets the shit beaten out of him by the biker girls, spreading all over his legs. Greg reveals that Slasher has a lot of used cars that the survivors can use to escape. Shitkicker is accidentally shot in the head by Secrets. The gunshot alerts the monsters, who enter the police station. The group then decides to run to the used car lot.
 impregnating him. A monster then immediately bursts through Slashers stomach, giving birth to a Slasher hybrid, killing the dozen survivors inside the unit. Meanwhile, Secrets, Greg and Bartender find the wounded Lightning and take him with them. Biker Queen frees the biker girls and they run from the Slasher/Monster Hybrid. They follow Hobo down a hole, in an attempt to hide inside his buried school bus/meth lab.

A monster follows them and kills Tit Girl. Biker Queen is finally able to get the bus started and as they leave, Tat girl sets the Hobo and a monster on fire who then fall out the back of the bus. The bus emerges from underground with Biker Queen and Tat Girl intending to abandon the remaining survivors, however the bus dies just as the other survivors catch up. The monsters immediately swarm the bus, but the group is saved by a man named Short Bus Gus, who seemingly has the ability to repel the monsters. He then leads the survivors into the sewers in an attempt to reach the big city. While working their way through the sewers, Tat Girl is killed by infected townies. The survivors are about to be killed by the infected until another survivor known as Jean Claude Segal saves them. Jean Claude tries to lead the survivors to the surface when he is attacked by a monster and has one of his arms bitten off. The survivors are then separated into two groups, Jean Claude and Bartender, and Biker Queen, Secrets, Greg, Lightning and Short Bus Gus. While trying to cauterize one of Jean Claudes wounds, Bartender accidentally blows off his remaining arm.

The other group of survivors finds the Hive, which is a gigantic rave with infected townies and monsters who spew their vomit on the people, causing horrible mutation and insanity. The survivors are reunited but are spotted by an infected townie and Biker Queen is infected. Jean Claude volunteers to stay behind to fight off infected townies to give the survivors a chance to escape. Jean Claude manages to fend off the infected townies for a short while before being ripped in half.

Short Bus Gus finds out that it has been his malfunctioning hearing aid that has been repelling the monsters and is impaled by a monster. The Slasher monster finally catches up with the group and kills the attacking monster and then forcibly removes the pipe lodged in Gregs head, killing him. Secrets is insane with grief and savagely attacks the Slasher Hybrid, killing him with the same pipe that was lodged in Gregs head.

Biker Queen takes off with one of the monsters, setting off its internal alarm system in an attempt to draw the monsters away from the remaining survivors. Bartender tells Secrets and Lightning that they are the only survivors left and that they have to repopulate now. Secrets looks up just as the foot of a giant robot crushes her, as well as Lightning, and walks away. As the credits roll, a mariachi sings a song recapping the entire Feast series. After the credits, Bartender slowly walks towards the camera and murmurs, "Goddamn it". (Super-8 footage from the beginning make the assumption that Bartender shortly dies of his neck wound afterwards.)

==Cast== Jenny Wade as Honey Pie
*Clu Gulager as Bartender
*Diane Goldner as Biker Queen
*Josh Blue as Short Bus Gus
*Martin Klebba as Thunder Carl Anthony Payne as Slasher
*Tom Gulager as Greg Swank
*Hanna Putnam as Secrets
*Juan Longoria Garcia as Lightning
*William Prael as Hobo
*John Allen Nelson as Shitkicker
*Craig Henningsen as Jean Claude Segal
*Chelsea Richards as Tat Girl
*Melissa Reed as Tit Girl

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 