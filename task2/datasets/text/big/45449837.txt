Thirumoorthy
{{Infobox film
| name           = Thirumoorthy
| image          = 
| image_size     =
| caption        =  Pavithran
| producer       = M. G. Sekar S. Santhanam
| writer         = Pavithran
| starring       =   Deva
| cinematography = B. Balamurugan
| editing        = B. Lenin V. T. Vijayan
| distributor    =
| studio         = M. G. Pictures
| released       =  
| runtime        = 155 minutes
| country        = India
| language       = Tamil
}}
 1995 Tamil Tamil action Senthil and Janagaraj playing Deva and was released on 11 May 1995. The film turned out to be a failure at box office.   

==Plot==

Moorthy (Vijayakanth) is a lorry driver, who lives with his mother Ram Aatha (Manorama (Tamil actress)|Manorama) and often confronts the criminal Govindan (Anandaraj). Moorthy and Uma (Ravali) fall in love with each other. One day, her father has a heart attack, when Moorthy and Uma rush him to the hospital the roads were blocked by a local political party. Her father unfortunately dies, the angry Moorthy clashes with the political party leader Sigamani (Rajan P. Dev) in public. Later, Govindan joins his party and stands in election as well as Moorthy. In the meantime, Moorthy and Uma get married. The corrupt politician Sigamani orders to kill Govindan and blames the innocent Moorthy. What transpires later forms the crux of the story.

==Cast==

 
*Vijayakanth as Moorthy
*Ravali as Uma
*Anandaraj as Govindan
*Rajan P. Dev as Sigamani Manorama as Ram Aatha
*Shenbagam as Geetha Senthil as Azhagesan Janagaraj as Umas father
*R. Sundarrajan (director)|R. Sundarrajan as Geethas brother
*Rajkumar
*Jojo Sangeetha as Lakshmi
*Srija
*Pasi Sathya
*Jyothi Meena
*Lakshmi Rathna
*Ennatha Kannaiya
*Pasi Narayanan
*Omakuchi Narasimhan
*T. N. B. Rajendran
*A. N. Babu
*Tirupur Ramasamy as Ramasamy
*Kullamani as Peruchazhi
*Karuppu Subbaiah
*Easwaran
*Samraj
*Ram-Lakshman
*Chelladurai
*Ghajini
*Kalyan
 

==Soundtrack==

{{Infobox Album |  
| Name        = Thirumoorthy
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1995
| Recorded    = 1995 Feature film soundtrack |
| Length      = 27:56
| Label       = Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1995, features 6 tracks with lyrics written by Vaali (poet)|Vaali.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Manjal Nila || S. P. Balasubrahmanyam, Rahini Santhanam || 4:42
|- 2 || Masthu Masthu || S. P. Balasubrahmanyam, S. Janaki, Chorus || 4:23
|- 3 || Namma Ooru || Atha Ali Azad, Chorus || 4:43
|- 4 || Namma Thalaivar || S. P. Balasubrahmanyam, Chorus || 4:12
|- 5 || Senguruvi || S. P. Balasubrahmanyam, S. Janaki || 5:58
|- 6 || Thaka Thimi Tha || Suresh Peters, Annupamaa, Chorus || 3:58
|}

==References==
 

 
 
 
 
 