Frankenstein (2004 film)
{{Multiple issues|
 
 
}}

{{Infobox Film
| name           = Frankenstein
| image          = Frankenstein (2004 film).jpg
| image_size     = 
| caption        = 
| director       = Marcus Nispel
| producer       = Martin Scorsese Marcus Nispel
| screenplay     = John Shiban
| story          = Dean Koontz
| based on       = Dean Koontzs Frankenstein by Dean Koontz| Frankenstein by Mary Shelley
| narrator       = 
| starring       = Parker Posey Vincent Pérez Thomas Kretschmann Adam Goldberg Ivana Miličević Michael Madsen
| music          = Normand Corbeil Angelo Badalamenti Daniel Pearl
| editing        = Jay Friedkin
| distributor    = USA Network
| released       = October 10, 2004
| runtime        = 88 minutes
| country        = 
| language       = 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 version of pilot for an ongoing series, but this was not successful.  Koontz later developed the concept into The Frankenstein 5-Book series: Frankenstein: Prodigal Son, City of Night, Dead and Alive, Lost Souls, The Dead Town.

==Plot==
  gothic reinvention set in present-day New Orleans. It recasts the doctor as the villain and the creature as a tragic hero determined to stop him; the primary action involves two police detectives (Parker Posey and Adam Goldberg) who enlist the aid of the creature ("Deucalion" in this version) to stop a serial killer who is one of Victors later creations.

==Cast==
{| class="wikitable"
|-
! Actor
! Role
|-
| Parker Posey
| Detective Carson OConner
|-
| Vincent Pérez
| Deucalion
|-
| Thomas Kretschmann
| Victor Helios
|-
| Adam Goldberg
| Detective Michael Sloane
|-
| Ivana Miličević
| Erika Helios
|-
| Michael Madsen
| Detective Harker
|-
| Deborah Duke
| Angelique
|-
| Ann Mahoney
| Jenna
|-
| Deneen Tyler
| Kathleen Burke
|-
| Brett Rice
| Detective Frye
|-
| Stocker Fontelieu
| Patrick
|}

==References==
 

==External links==
* 

 
 
 
 

 
 
 
 
 
 
 
 
 


 
 