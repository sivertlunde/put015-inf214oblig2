Motorcycle Gang (film)
{{Infobox film
| name           = Motorcycle Gang
| image          =
| caption        =
| director       = Edward L. Cahn
| producer       =
| writer         = Lou Rusoff
| based on       = John Ashley
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    = American International Pictures
| released       =  
| runtime        = 78 mins
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Motorcycle Gang is a 1957 film which is a semi remake of Dragstrip Girl. It was released by American International Pictures as a double feature with Sorority Girl.

==Cast==
*Anne Neyland as Terry Lindsay
*Steve Terrell as Randy John Ashley as Nick Rogers
*Carl Switzer as Speed
*Raymond Hatton as Uncle Ed
*Russ Bender as Lt. Joe Watson
*Jean Moorhead as Marilyn
*Scott Peters as Hank
*Eddie Kafafian as Jack
*Shirley Falls as Darlene
*Aki Aleong as Cyrus Q. Wong
*Wayne Taylor as Phil

==Production==
Filming was held up when star John Ashley was drafted into the army. It was shot during two weeks when he was on leave after basic training. Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p133 

==References==
 

==External links==
*  at IMDB
*  at TCMDB

 

 
 
 
 
 
 


 