The Wonder of It All (2007 film)
 

{{Infobox film
| name           = The Wonder of it All
| image          =
| image size     =
| alt            =
| caption        =
| director       = Jeffrey Roth
| producer       = Paul Basta, Gregory Schwartz
| writer         = Jeffrey Roth, Paul M. Basta, Gregory Schwartz
| narrator       = Steve Evans
| starring       = Buzz Aldrin, Alan Bean, Gene Cernan, John Young, Charlie Duke, Harrison Schmitt 
| music          = Scott Starrett
| cinematography = Paul M. Basta
| editing        = Andy Zall, David DeMore
| studio         = Jeffrey Roth Productions
| distributor    = Indican Pictures
| released       =  
| runtime        = 01:22:37
| country        = United States
| language       = English, French subtitles
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 documentary directed Apollo astronauts who walked on the moon.  The astronauts explain their backgrounds, their moon missions, and how walking on the moon changed their lives.

==External links==
*  
*   at the Internet Movie Database

 
 
 
 

 