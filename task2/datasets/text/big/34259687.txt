Kissing Cup's Race (1920 film)
 
 
{{Infobox film
| name           = Kissing Cups Race
| image          =
| caption        = Walter West
| producer       = 
| writer         = Campbell Rae Brown (poem)   J. Bertram Brown   Benedict James
| starring       = Violet Hopson Gregory Scott Clive Brook
| music          =
| cinematography = 
| editing        = 
| studio         = Hopson Productions
| distributor    = Butchers Film Service
| released       = 1920 
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    = British silent silent sports Walter West and starring Violet Hopson, Gregory Scott and Clive Brook.  It is based on the play Kissing Cups Race by Campbell Rae Brown.

==Cast==
* Violet Hopson - Constance Medley 
* Gregory Scott - Lord Hilhoxton 
* Clive Brook - Lord Rattlington 
* Arthur Walcott - John Wood 
* Philip Hewland - Vereker 
* Adeline Hayden Coffin - Lady Corrington 
* Joe Plant - Bob Doon

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 

 
 