Mumbai Cutting
 
 
{{Infobox film
| name           = Mumbai Cutting
| image          =Mumbai_Cutting_poster.jpg
| image size     =
| alt            =
| caption        = Movie Poster
| director       =  
| producer       =  
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =130 minutes
| country        = India
| language       =Hindi
| budget         =
| gross          =
}}
Mumbai Cutting is a 2010 anthology Indian film comprising eleven short films, telling eleven different stories based on life in  , Sudhir Mishra, Rahul Dholakia, Kundan Shah, Revathy, Jahnu Barua, Rituparno Ghosh, Shashanka Ghosh, Ruchi Narain, Ayush Raina and Manish Jha.   

The film is produced by Sahara One and is a Whitecloud production.

==Films==
* "Anjane Dost", directed by Jahnu Barua
* "Bombay Mumbai Same Shit", directed by Rahul Dholakia
* "Urge", directed by Rituparno Ghosh
* "10 minutes", directed by Shashanka Ghosh
* "And It Rained", directed by Manish Jha
* "Pramod Bhai 23", directed by Anurag Kashyap
* "The Ball", directed by Sudhir Mishra
* "Jo Palti Nahin Woh Rickshaw Kya", directed by Ruchi Narain
* "Bombay High", directed by Ayush Raina
* "Parcel", directed by Revathi
* "Hero", directed by Kundan Shah

==Release== 2008 Indian Film Festival of Los Angeles, thereafter it was also the closing film of the 10th Osian Film Festival  in July 2008,    though it was commercially unreleased. 

==References==
 

==External links==
*  

 
 

 
 
 
 
 