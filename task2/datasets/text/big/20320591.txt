Rich in Love
{{Infobox film title          = Rich in Love distributor    = Metro-Goldwyn-Mayer released       = March 5, 1993 U.S.A. May 7, 1993 U.K. May 27, 1993 Australia runtime        = 105 minutes image          = Rich in love (1993).jpg starring       = Albert Finney Kathryn Erbe Jill Clayburgh Kyle MacLachlan Suzy Amis Ethan Hawke director       = Bruce Beresford screenplay     = Alfred Uhry Mark Warner cinematography = Peter James music          = Georges Delerue producer       = Richard D. Zanuck Lili Fini Zanuck rating         = PG-13
}}
Rich in Love is a 1993 drama film based on the 1987 novel with the same name by Josephine Humphreys.  The film stars Albert Finney, Kathryn Erbe, Kyle MacLachlan, Jill Clayburgh, Suzy Amis, and Ethan Hawke.

==Plot==
The Odom family lives in a large, white, Southern house in Mount Pleasant, South Carolina, just off Pitt Street, looking out onto Charleston Harbor.  Lucille Odom (Erbe) is nearing the end of her last year of high school when her mother, Helen Odom (Clayburgh), leaves the family, breaking all ties.  Lucille is left to care for her recently retired father, Warren Odom (Finney). 

The family spends the summer trying to get their lives together, which is complicated when the older daughter of the family, Rae (Amis), moves back home with a new husband (MacLachlan) and a new baby on the way, about which Rae has mixed feelings.  While reminiscing about their mother, Rae tells Lucille that their mother tried to abort Lucille when she was pregnant. Warren notices his daughters in a new and different way now that his wife is gone and he is no longer working.  Everything changes for Lucille as she comes-of-age and learns about her family in new ways.

==Cast==
*Albert Finney as Warren Odom
*Jill Clayburgh as Helen Odom
*Kathryn Erbe as Lucille Odom
*Kyle MacLachlan as Billy McQueen
*Piper Laurie as Vera Delmage
*Ethan Hawke as Wayne Frobiness
*Suzy Amis as Rae Odom
*Ramona Ward as Sharon
*Alfre Woodard as Rhody Poole

==Production==
The film was set and produced in Mount Pleasant, South Carolina.    With some additional footage shot across the Cooper River in Charleston.  

==Reception==
The film received mostly positive reviews.  One drawback, however, was that many did not find the story believable, as the familys problems and happy ending were something of a fantasy. For example, Roger Ebert of the Chicago Sun-Times, giving the film three stars out of four, wrote "I must confess I didnt much believe the story". 

==Home media==
The film was released on DVD via Amazon.com|Amazon’s On-Demand service on December 15, 2009.  

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 