Vayuputra
{{Infobox film name           = Vayuputra image          = image_size     = caption        = director       = Kishore Sarja producer       = Arjun Sarja writer         = N. Linguswamy narrator       = starring       = Chiranjeevi Sarja Aindrita Ray Ambareesh music          = V. Harikrishna cinematography = Sundarnath Suvarna editing        = Kay Kay studio         = Sri Ram Films International released       =   runtime        = 142 minutes country        = India language       = Kannada budget         =
}}
 Kannada romantic action film directed by Kishore Sarja and produced by his brother, actor Arjun Sarja.  The Sarja brothers launched their nephew Chiranjeevi Sarja through this film in the lead role. The rest of the cast includes Ambareesh, Aindrita Ray, Ajay, Sadhu Kokila and several others. 
 Tamil film Vishal and Meera Jasmine. The original score and soundtrack for the film is composed by V. Harikrishna. The film released on 3 September 2009 to mixed and average response at the box-office and average critical response 

==Cast==
* Chiranjeevi Sarja as Balu
* Aindrita Ray as Divya
* Ambareesh Ajay
* Sadhu Kokila
* Mukhyamantri Chandru
* Ramesh Bhat
* Padmaja Rao
* Shobha Raghavendra
* Arjun Sarja...Cameo appearance

==Soundtrack==
The music of the film was composed by V. Harikrishna and lyrics written by V. Nagendra Prasad and Kaviraj (lyricist)|Kaviraj. 

{{Infobox album  
| Name        = Vayuputra
| Type        = Soundtrack
| Artist      = V. Harikrishna
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Akash Audio
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Bhagavantha Banda
| lyrics1 	= V. Nagendra Prasad Karthik
| length1       = 
| title2        = Baare Baare Gopamma
| lyrics2 	= V. Nagendra Prasad
| extra2        = Rahul Nambiar, Jyotsna Radhakrishnan
| length2       = 
| title3        = Rock A Body Kaviraj
| extra3 	= Tippu (singer)|Tippu, Jyotsna Radhakrishnan
| length3       = 
| title4        = Banda Gandara Ganda
| extra4        = Vandemataram Srinivas
| lyrics4 	= V. Nagendra Prasad
| length4       = 
| title5        = Bhoomi Namma Jeeva
| extra5        = Amul Raj, Priya Himesh
| lyrics5       = V. Nagendra Prasad
| length5       = 
| title6        = Yaare Yaare Yaramma
| extra6        = Rajesh Krishnan
| lyrics6       = V. Nagendra Prasad
| length6       =
}}

==References==
 

==External source==
*  

 
 
 
 
 
 
 


 

 