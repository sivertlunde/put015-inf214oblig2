When Tomorrow Comes (film)
{{Infobox film
| name           = When Tomorrow Comes
| image          = When Tomorrow Comes poster.jpg
| caption        = 
| director       = John M. Stahl
| producer       = John M. Stahl
| writer         = James M. Cain (story) Dwight Taylor
| starring       = Irene Dunne Charles Boyer
| music          = 
| cinematography = 
| editing        = 
| studio         = Universal Pictures John M. Stahl
| distributor    = Universal Pictures
| released       = August 11, 1939
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
When Tomorrow Comes is a 1939 romantic drama film starring Irene Dunne and Charles Boyer. A waitress falls in love with a man who later turns out to be a married concert pianist. Bernard B. Brown won the Academy Award for Best Sound.   

A scene in the movie where the two protagonists take refuge from a storm in a church was the subject of Cain v. Universal Pictures, a case in which the writer James M. Cain sued Universal Pictures, the scriptwriter and the director for copyright infringement.  Judge Leon Rene Yankwich ruled that there was no resemblance between the scenes in the book and the film other than incidental "scènes à faire", or natural similarities due to the situation, establishing an important legal precedent. {{cite web |ref=harv |url=http://scholar.google.ca/scholar_case?case=5293541731877026047&hl=en&as_sdt=2&as_vis=1&oi=scholarr&sa=X&ei=qhviT-LHJcPu2gXmuojvCw&ved=0CAgQgAMoADAA
 |title=CAIN v. UNIVERSAL PICTURES CO., Inc., et al.
 |publisher=District Court, S. D. California, Central Division.
 |date=December 14, 1942
 |first=Leon Rene |last=Yankwich
 |accessdate=2012-06-20}} 

==Cast==
*Irene Dunne as Helen Lawrence
*Charles Boyer as Philip Chagal
*Barbara ONeil as Madeleine Chagal
*Onslow Stevens as Jim Holden
*Nydia Westman as Lulu
*Nella Walker as Betty Dumont
*Fritz Feld as Nicholas

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 