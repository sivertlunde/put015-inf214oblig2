It's a Disaster
{{Infobox film
| name = Its a Disaster
| image = Its a Disaster film festival poster.jpg
| caption = Film festival circuit poster
| director = Todd Berger
| producer = Kevin M. Brennan Jeff Grace Gordon Buelonic Datari Turner
| writer = Todd Berger
| starring = Rachel Boston Kevin M. Brennan David Cross America Ferrera Jeff Grace Erinn Hayes Blaise Miller Julia Stiles
| music = Chris Martins
| cinematography = Nancy Schreiber, ASC
| production design = Peter K. Benson
| editing = Franklin Peterson
| distributor = Oscilloscope Laboratories
| released =  
| runtime = 88 minutes
| country = United States
| language = English 
| gross    = $60,818 
}}
Its a Disaster is a 2012 art film|art-house black comedy film written and directed by Todd Berger. The film was made by Los Angeles-based comedy group The Vacationeers and stars Rachel Boston, David Cross, America Ferrera, Jeff Grace, Erinn Hayes, Kevin M. Brennan, Blaise Miller, Julia Stiles, and Todd Berger.       The film premiered on June 20, 2012, at the Los Angeles Film Festival.   Its a Disaster was commercially released in US theaters by Oscilloscope Laboratories, which acquired the US distribution rights to the film, on April 12, 2013.   

==Plot==
Four couples gather for a regular brunch which, over the years, has devolved into a gathering fraught with tension and awkwardness. One guest is meeting the others for the first time, on his third date with the hosts sister. As they settle into the afternoon (awaiting an "always late" fifth couple), they get to know the new member of the group and catch up on old times. When the men excuse themselves to watch American football|football, they discover the TV, Internet and landline phones are down. When the host accuses his wife of not paying the bills, their upcoming divorce is revealed to the guests.
 swinger couple toward the nervous newcomer, who is implied to be a 42-year-old virgin.
 VX nerve epiphany and calls off her six-year engagement. Her spirits lifted, she champions a musical session/"dance party" in the living room.

Her now-former fiance is a comic book trader and bases his survival strategy on what hes learned from zombie films like Night of the Living Dead. After hearing of the bombs, he becomes highly suspicious of outsiders (including the new guy) and suggests others watch for bite marks or odd behaviour, and they find weapons (like crowbars and chainsaws). He becomes concerned by the lack of life immediately outside the front window, so he questions the doctor guest about how quickly "mutations" should appear and spread. She explains how genetic mutation really works, with some disdain, but later agrees to not let the late fifth couple into the house when they finally arrive, with the pair visibly sick with something. While he seems more concerned by the womans vague and rushed description of the situation outside, her decision to leave them is also partly punishment for always being late and not caring.

The recently dumped trader decides the best option is to leave the house and drive blindly till he either reaches fellow survivors or runs out of gas, having at least died trying. He asks if anyones coming with him, and they all load into his SUV. The battery has been drained by a guest listening to satellite radio, so it doesnt start.

The group eventually decides to stay home and have their meal as planned, enjoying what time they have. The newcomer goes to the cellar to fetch wine, and is discovered by his date adding rat poison to it. He explains that hes a firm believer in The Last Judgment, this is likely that and he wants to save his new "non-believer" friends from experiencing the worst of the Great Tribulation. Convinced hes crazy, she goes upstairs and tells the others, after the virgin pours the wine, but before they drink. He calmly admits it, and gives his reasons. Theyre unconvinced, until the chemist agrees their deaths would be easier this way, without the VX symptoms she details.

After some debate, they all agree to drink the poison together on the count of three. After one count, everyone only pretends to drink, even the "true believer", who says he figured that might happen. They again ready themselves and finally tip the glasses back in unison. Again, they all fake it. Their fate is left unknown.

==Cast==
* Rachel Boston as Lexi Kivel
* David Cross as Glenn Randolph
* America Ferrera as Hedy Galili
* Jeff Grace as Shane Owens
* Erinn Hayes as Emma Mandrake
* Kevin M. Brennan as Buck Kivel
* Blaise Miller as Pete Mandrake
* Julia Stiles as Tracy Scott
* Todd Berger as Hal Lousteau

==Poster==
The films festival-touring poster, which parodies James Montgomery Flaggs famous Uncle Sam recruitment poster,  has independently received critical acclaim and was named as one of the 12 best movie posters of 2012 by Film School Rejects.  Oscilloscope Pictures is using an updated version of the same poster design, with the movies film festival awards and a critics quote added, for its commercial theatrical release. 

==Festivals==
Its a Disaster has been selected to screen at the following film festivals:
*2012 Los Angeles Film Festival (June 20, 2012) (world premiere)
*2012 Edmonton International Film Festival (October 6, 2012)
*2012 BendFilm Festival (October 12, 2012)
*2012 New Orleans Film Festival (October 13, 2012) Friars Club Comedy Film Festival (October 24, 2012)
*2012 Virginia Film Festival (November 3, 2012)
*2012 Lone Star International Film Festival (November 10, 2012)
*2012 Napa Valley Film Festival (November 10, 2012)
*2012   (November 12, 2012)
*2012 Whistler Film Festival (December 2, 2012)
*2013 RiverRun International Film Festival (April 13, 2013)

Its a Disaster was chosen by the 2012 Friars Club Comedy Film Festival to be the festivals opening film. 

==Distribution==

===Oscilloscope and Theatrical Release===

On August 23, 2012, Oscilloscope Laboratories announced that it had acquired North American distribution rights to Its a Disaster and that it planned both a theatrical and digital release of the film.    Oscilloscope has since announced its planned release schedule for the film, with the film opening in New York and Los Angeles on April 12, 2013, with theatrical distribution expanding into additional markets beginning April 19, 2013.   The movie earned $15,305, or $5,102 per theater, over its first weekend in limited release.   

===Vine App Release===

On February 19, 2013, Oscilloscope announced that it would initially distribute Its a Disaster via the new mobile app Vine (app)|Vine.     Vine, a social networking app released by Twitter|Twitter, Inc. on January 24, 2013, allows users to post video clips up to six seconds long, which then play in loop mode for viewers. A few hours after announcing the Vine distribution, described as a "stunt" and a "tongue in cheek experiment", Oscilloscope began tweeting links to 6-second-long chunks of the film each individually uploaded to Vine, thus making Its a Disaster the first film ever released on Vine.       

The following day, Oscilloscope issued another tongue-in-cheek press release admitting they were wrong in expecting audiences to embrace watching films in six-second clips on their smartphones, and that they were abandoning plans to release future films on Vine.    

===iTunes and Digital Release===
 video on demand (VOD) services on March 5, 2013.   On March 5, 2013, actor Blaise Miller and actress Julia Stiles used Twitter to announce the films release on iTunes, Google Play, Amazon Instant Video, and VOD.   It is currently available for streaming on Netflix.

==Awards==
;BendFilm Festival
:2012: won Best Screenplay

;Edmonton International Film Festival
:2012: won Grand Jury Award for Best Feature - Audience Choice

;New Orleans Film Festival
:2012: won Audience Award for Best Picture

==References==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 