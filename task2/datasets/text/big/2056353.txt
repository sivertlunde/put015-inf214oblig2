Ordet
 
{{Infobox film
| name           = Ordet
| image          = Ordetposter.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Carl Theodor Dreyer
| producer       = Carl Theodor Dreyer
| screenwriter   = Carl Theodor Dreyer
| based on       =  
| starring       = Henrik Malberg Emil Hass Christensen Cay Kristiansen Preben Lerdorff Rye
| music          = Poul Schierbeck
| cinematography = Henning Bendtsen
| editing        = Edith Schlüssel
| distributor    =
| released       =  
| runtime        = 126 min.
| country        = Denmark
| language       = Danish
| budget         =
| preceded_by    =
| followed_by    =
}}
Ordet ( ,  meaning "  database   

The film is now regarded by many critics as a masterpiece, admired particularly for its cinematography.

==Plot==
The film centers around the Borgen family in rural Denmark.  The devout widower Morten, patriarch of the family, prominent member of the community, and patron of the local parish church, has three sons.  Mikkel, the eldest, has no faith, but is happily married to the pious Inger, who is pregnant with their third child.  Johannes, who went insane studying Søren Kierkegaard, believes himself to be Jesus Christ and wanders the farm condemning the ages lack of faith, including that of his family and the modern-minded new pastor of the village.  The youngest son, Anders, is lovesick for the daughter of the leader of a local Christian religious sect.

Anders confesses to Mikkel and Inger that he loves Anne Petersen, the daughter of Peter the Tailor.  They agree to convince Morten to assent to the match.  Later, Inger attempts to convince Morten to allow Anders to marry Anne.  Morten angrily refuses, but changes his mind when he finds out Peter has refused Anders proposal.  Morten and Anders go to meet Peter, in order to negotiate the betrothal.

 ) believes that he is Jesus Christ after a mental breakdown.]]

Morten tries to convince Peter to permit the marriage, but he continues to refuse unless Morten and Anders join his sect.  As the discussion collapses into sectarian bickering, Morten receives a call announcing that Inger has gone into a difficult labor.  Peter says that Ingers difficulties are punishment from God for Morten not joining his sect. Furious at Peters comments, Morten attacks Peter and storms out with Anders, the two of them rushing home. While the doctor is forced to abort the baby, he is able to save Ingers life.  After the doctor and pastor leave, Johannes angers his father by telling him that death is nearby and will take Inger, unless Morten has faith in him.  Morten refuses to listen and, as prophesied, Inger dies suddenly.

While preparing to go to Ingers funeral, Peter realizes that he has wronged Morten terribly, and reconciles with him over Ingers open coffin, agreeing to permit Anne and Anders to marry.  Johannes suddenly interrupts the wake, approaches Ingers coffin, and proclaims that she can be raised from the dead if the family will only have faith and ask God to do so.  Ingers daughter takes Johannes hand and impatiently asks him to raise her mother from the dead.  Johannes praises her childlike faith and asks God to raise Inger, who begins to breathe and twitch in her coffin.  Seeing what seems to be the miracle of resurrection, both Morten and Peter rejoice, forgetting their religious differences.  As Inger sits up, Mikkel embraces her and proclaims that he has finally found faith. 

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
!   Actor !! Role
|- Gerda Nielsen || Anne Petersen
|- Sylvia Eckhausen|| Kirstin Petersen
|- Ejner Federspiel || Peter Petersen
|- Cay Kristiansen || Anders Borgen
|- Birgitte Federspiel || Inger Borgen
|- Emil Hass Christensen || Mikkel Borgen, her husband
|- Susanne Rud || Lilleinger Borgen, Mikkels Daughter
|- Ann Elisabeth Rud || Maren Borgen, Mikkels Daughter
|- Preben Lerdorff Rye || Johannes Borgen
|- Henrik Malberg || Morten Borgen
|- Ove Rud || Pastor
|-
|  || The Doctor
|- Edith Trane || Mette Maren
|- Hanne Agesen || Karen, A servant
|}

==Production==

===Pre-production===
After several years of financial problems having stalled his film career, in 1952 Dreyer was awarded a lifelong lease to the Dagmar Bio, an art-house movie theater in Copenhagen.  The Danish government had often given such awards to older artists and the profits from the theater allowed Dreyer to begin production on a new film through Palladium Studios.  Around that time Palladium president Tage Nielsen received a suggestion for Dreyers next film from the government run Dansk Kulturfilm, whom Dreyer had worked with before. Kaj Munks play I Begyndelsen var Ordet (In the Beginning was the Word) was written in 1925 and premiered in Copenhagen in 1932.  Dreyer had attended the plays premiere in 1932 and had been writing notes for a film adaptation since 1933. 
 The Word was released, directed by Gustaf Molander and starring Victor Sjöström,  which couldnt premiere in Denmark until after World War II.  For legal reasons the 1943 film forced Palladium to wait until 1954 to begin production on Dreyers film.  Dreyer made changes to the original play such as adding the films opening scene (which was inspired by a passage from Munks published memoir),  omitting Johanness previous love affair as having contributed to his mental illness  and cutting two-thirds of Munks original dialogue.  Although he was careful to stay true to Munks intention of the plays message, he cut dialogue down to the bare essentials resulting in a minimalist film. 

===Filming===
 

Filming lasted for four months: two months on a studio set and two months in Vedersø,  a village in West Jutland where Munk had served as a Lutheran priest. Wakeman. p. 271.  Ordet is best remembered for its cinematography. The film has a total of 114 individual shots, half of which occur in the first and last scenes of the film. Many shots lasted for up to seven minutes.  Dreyer used mostly long takes and a slow moving camera, as his style had been gradually progressing towards for several years. He also paid close attention to every minute detail during filming and later said that he "made the film crew equip the kitchen with everything he considered right for a country kitchen. Then...he set about removing the objects. Finally, only ten to fifteen remained, but they were just what were wanted to create the right psychological illusion." 

Dreyers shooting method was to shoot one individual shot per day. In the mornings Dreyer would rehearse with the actors while simultaneously blocking their movement, set up lights and block the camera movement. Then at the very end of the day Dreyer would film the single shot, usually shooting between 2 to 3 takes. Dreyer chose not to pre-plan any shots and every decision was made the day of the shoot. 

Cinematographer Henning Bendtsen said that "each image is composed like a painting in which the background and the lighting are carefully prepared." Whereas most scene at that time would use one or two lights for a scene, Dreyer and Bendtsen often used up to 20. Electricians often had to turn lights on and off during the shooting without it being noticeable on film. Dreyer disliked light meters and preferred to deduce the correct exposure at each location by eye. Actress Birgitte Federspiel said that “Lighting was his great gift and he did it with expertise. He shaped light artistically like a sculptor or a painter would.” 

Dreyer staged the actors around the lighting design on the set and not to accommodate their performances.  Federspiel said that Dreyer never verbally told the actors when he was pleased with their performances and would simply smile when they were performing the way that he wanted them to. Ordet DVD Special Features. Birgitte Federspiel interview. The Criterion Collection. 2011.  The blocking of the actors movement was so precise and carefully planned out that many them had to count out their steps while speaking their dialogue.  Federspiel said that Dreyer personally picked out costumes for every actor and even went shopping for stocking with her. He also hired a linguistic specialist to make sure that all the actors spoke the same dialect as the characters that they played.  Dreyer had some difficulty with the elderly actor Henrik Malberg being able to remember his dialogue for such long takes.  Munk had written the original role of Morten Borgen specially for Malberg in 1925. 

Like her character, Federspiel was really pregnant during the films production and went into labor towards the end of the shoot. She agreed to allow Dreyer to tape record her labor pains while she was giving birth and then later use the recording as a sound effect in the finished film. While waiting for Federspiel to return to the production after having a child Dreyer and Bendtsen planned out the final resurrection scene. They shot two different versions of the scene, a version from Munks play which suggests the possibility of Inger just appearing to be dead and a version where the resurrection is a genuine miracle. Dreyer chose to use the second version in the final film. 

==Release and reception==
The film premiered on 10 January 1955 at Dagmar Teatret in Copenhagen.  It was Dreyers first film since The Passion of Joan of Arc to immediately receive critical praise and was especially popular in his native Denmark by both critics and audiences, as well as by critics in other countries. It also won the Golden Lion at the 16th Venice International Film Festival. Wakeman. p. 272. 
 Master of Robin Wood Time Out said that "‘Powerful’ doesn’t do justice" to the film and that it "reminds us how in the end we know little about the mysteries of life. Dreyer manages to say all this within the framework of a strange, wondrous and shocking work. Once seen, it’s unlikely to leave you."  Peter Bradshaw of The Guardian called it "a gripping and eerie tragedy of the supernatural", but also thought it was not as good as Day of Wrath or The Passion of Joan Of Arc. 

It has been released on DVD by The Criterion Collection with spine number 126, as part of a box set with the other Dreyer films Day of Wrath and Gertrud (film)|Gertrud. 

==Awards== Best Actor Best Actress Best Danish film with Sven Methlings Der kom en dag.  It is currently ranked as the third most spiritually significant film of all time by Arts and Faith online community. 
 
==References==
 

==External links==
* 
* 
* 
*  (in English) Retrieved 2013-03-12
*    Retrieved 2013-03-12
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 