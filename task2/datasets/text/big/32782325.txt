Fury (1923 film)
{{infobox film
| name           = Fury
| image          = Fury - 1923.jpg
| imagesize      =
| caption        = 1923 lobby poster Henry King
| producer       = Richard Barthelmess (Inspiration Pictures)
| writer         = Edmund Goulding (original story and scenario) Tyrone Power
| music          =
| cinematography = Roy Overbaugh
| editing        = W. Duncan Mansfield
| distributor    = Associated First National (*later First National)
| released       =  
| runtime        = 9 reel#Motion picture terminology|reels; 8,709 feet
| country        = United States Silent (English intertitles)
}}
 silent drama Henry King and released through First National Pictures which was then called Associated First National. 
 Tyrone Power whose character Captain Leyton dies of heart attack, leaving his son, called Boy played by Barthelmess, to carry on. In real life actor Power would die of a heart attack in 1931 in the arms of his own son Tyrone Power, the later film star. 

The survival status of this film is unknown. 

==Cast==
*Richard Barthelmess - Boy Leyton
*Tyrone Power, Sr. - Captain Leyton
*Pat Hartigan - Morgan
*Barry Macollum - Looney Luke
*Dorothy Gish - Minnie
*Jessie Arnold - Boys Mother
*Harry Blakemore - Mr. Hop
*Adolph Milar - Yuka
*Ivan Linow - Zece
*Emily Fitzroy - Matilda Brent
*Lucia Backus Seger - Mrs. Ross
*Patterson Dial - Looney Lukes Girl

==References==
 

==External links==
 
* 
* 
* 

 
 
 
 
 
 


 