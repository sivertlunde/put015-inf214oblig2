Blood and Lace
 
{{Infobox Film
| name           = Blood and Lace
| image          = 
| image_size     = 
| caption        = 
| director       = Philip Gilbert
| producer       = Ed Carlin Gil Lasky Chase Mishkin (associate producer)
| writer         = Gil Lasky
| narrator       = 
| starring       = Gloria Grahame Melody Patterson Milton Selzer Len Lesser Vic Tayback Dennis Christopher
| music          = 
| cinematography = Paul Hipp
| editing        = Marcus Tobias
| distributor    = American International Pictures
| released       = 17 March 1971
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 200,000 $
| preceded_by    = 
| followed_by    = 
}}

Blood and Lace is a 1971 horror film directed by Philip Gilbert, starring Gloria Grahame, Melody Patterson, Len Lesser, and Milton Selzer.

==Plot==
After her prostitute mother and her john are beaten to death while 
they are asleep in bed, teen-aged Ellie Masters is sent to an 
isolated orphanage run by Mrs. Deere and her handyman. Taking an 
avid interest in her welfare is detective Calvin Carruthers. 
Taking almost no interest at all, is social worker Harold Mullins 
who is completely under Mrs. Deeres thumb. Lots of unpleasant 
surprises are in store for Ellie, not the least of which is the 
fact that Mrs. Deere and her handyman are both brutal sadists, 
who run the orphanage like a concentration camp and the strong 
possibility that her mothers hammer-wielding killer is now 
stalking her.

==Cast==
*Gloria Grahame as Mrs.Deere
*Melody Patterson as Ellie Masters
*Milton Selzer as Mr. Mullins
*Len Lesser as Tom Kredge
*Vic Tayback as Calvin Carruthers
*Terri Messina as Bunch
*Ronald Taft as Walter
*Dennis Christopher as Pete Peter Armstrong as Ernest
*Maggie Corey as Jennifer
*Mary Strawberry as Nurse
*Louise Sherrill as Edna Masters
*Joe Durkin as Unidentified Man

==References==
 
*http://www.imdb.com/title/tt0066848

 
 
 
 
 


 