Roll Up Your Sleeves
{{Infobox Film
| name           = Roll Up Your Sleeves
| image          =
| image_size     =
| caption        =
| director       = Dylan Haskins
| producer       = Project Arts Centre and DCTV
| writer         =
| narrator       =
| starring       = Ian Mackaye Ellen Lupton The Ex (band)
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 2008
| runtime        = 27 min.
| country        = Ireland
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} Irish documentary about do-it-yourself counterculture directed by Dylan Haskins. It also examines the relationship between DIY culture and the need for autonomous social spaces, looking at various projects across Europe and how these compare with the situation in Ireland.

==Outline==

Roll Up Your Sleeves was shot over a two-year period by Haskins and his friends. The film begins by focusing on the non-profit all ages gigs in his Haskins own home The Hideaway House in Ireland leads him to drive US folk punk band Ghost Mice on their European tour and to the conclusion that this is all about much more than music. The Ex

==Funding==

The film was received BCI Sound & Vision funding. It was produced by Project Arts Centre for DCTV.

==Reception==

‘Roll Up Your Sleeves’ premiered at the 2009 Stranger Than Fiction Festival in the IFI in Dublin.  

In January 2011, the film was made available free online garnering international media attention.    

==External links==
*  on Vimeo

==References==
 

 
 
 
 
 
 
 