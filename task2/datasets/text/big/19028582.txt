Hearts Adrift
{{Infobox film 
 | name = Hearts Adrift
 | image = Hearts Adrift 1914.jpg
 | caption = Film poster
 | director = Edwin S. Porter 
 | writer = Cyrus Townsend Brady (story) Mary Pickford
 | starring = Mary Pickford
 | producer = Famous Players Film Company
 | distributor = States Rights Distribution Arrangement
 | budget = 
 | released =  
 | country = United States English intertitles
 | runtime = 
}}
 silent Short short romance film directed by Edwin S. Porter. The film is now considered Lost film|lost. 

==Production==
  The Blue 1923 and several decades later with Jean Simmons in 1948 and Brooke Shields in 1980.

The film proved to be a huge success.  Actress Mary Pickford eventually demanded a higher salary as her popularity rose because of this film. 

==Plot==
Nina (Mary Pickford) and Jack Graham (Harold Lockwood) are both marooned on a deserted island. They fall in love and eventually Nina gives birth to a child. Despite being stranded, they are very happy together. One day, Jacks wife comes to rescue him. Nina is crushed and throws herself in a volcano.

==Cast==
* Mary Pickford - Nina
* Harold Lockwood - Jack Graham

==See also==
* Mary Pickford filmography
* List of lost films

==References==
 

==External links==
*  
*  

 
 
 
 
 
 

 
 