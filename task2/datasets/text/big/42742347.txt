Armistice (film)
{{Infobox film
| name = Armistice
| image = Movie poster of Armistice.jpg
| caption = Theatrical poster
| director = Luke Massey
| producer = Billy Budd Leon Davies
| writer = Luke Massey Benjamin Read Joseph Morgan Matt Ryan Al Weaver William Troughton 
| music = Jonathan Fletcher 
| distributor = Double Dutch Media
| released =  
}}
Armistice is a 2013 supernatural psychological thriller film about one mans fight to preserve his humanity and sanity over years of terrible imprisonment. It was originally called Warhouse.   

In 2012, it was sold at Cannes Film Market.   It was then premiered in Bruges, Belgium on 29 October 2013 at Razor Reel Fantastic Film Festival. 

XLrator Media released the film on DVD on 11 March 2014. 

==Plot==
Royal Marine A.J Budd (Joseph Morgan) awakes in a mysterious house and is forced to fight for his life every day against grotesque inhuman opponents. Trapped alone in an unchanging prison of unbreakable routines, he must kill every day or die himself. As days stretch into years, the isolation and unceasing violence threaten his very soul. The only note of hope lies in the journals of a former prisoner of the Warhouse, World War I officer Lieutenant Edward Sterling (Matt Ryan). Discovered behind a secret wall, Sterlings diaries serve as a mentor to the young marine and help to keep him alive, but what dark fate befell their author? The stories of these two men from different times interweave as their desperation to escape the Warhouse and the endless killing leads them both into taking terrible measures.

==Cast== Joseph Morgan as A.J. Budd   Matt Ryan as  Edward Sterling  
* William Troughton as The Fallen  
* Al Weaver as a Gulf war soldier (uncredited)

==Production==
The original concept was a graphic novel Benjamin Read (screen writer) was writing.  The film was filmed in Stratford-Upon-Avon. 

==Reviews==
Matt Molgaard of Best-Horror-Movies.com: "But above and beyond the technical effectiveness of the work turned in by the behind-the-scenes crew, is the sheer brilliance of focal performer, Joseph Morgan". 

==References==
 

==External links==
*  

 
 