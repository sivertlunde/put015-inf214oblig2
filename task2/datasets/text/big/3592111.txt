Moving (1988 film)
{{Infobox film
| name=Moving
| image= Moving movie poster.jpg
| caption = Theatrical release poster
| writer=Andy Breckman Dave Thomas Dana Carvey Randy Quaid
| director=Alan Metter
| music=Howard Shore
| cinematography=Donald McAlpine
| editing=Alan Balsam
| distributor=Warner Bros.
| released= 
| runtime=89 minutes
| language=English
| movie_series=
| awards=
| producer=Stuart Cornfeld
| budget=
| gross=$10,815,378
}}

Moving is a 1988 American comedy film starring Richard Pryor as Arlo Pear, a father moving his family cross-country.
 WWF wrestler King Kong Bundy as a monstrous mover.  The movie is also the film debut of Stacey Dash, as Arlos daughter Casey.

==Plot==

Arlo Pear (Pryor) is a transportation engineer living in the New Jersey suburbs.  One day he goes to work and meets a new female co-worker, and when both of them attempt to enter their keys in the same office doorknob, Arlo guesses what has happened and confronts his boss, Roy Hendersen.  He learns that his company has merged with another, and now Arlo is out of a job.  He ends the meeting by telling off his boss and in his state of anger, he flips Roy off using his index, rather than his middle finger.

Arlos wife Monica tries to defuse the situation by telling her husband that she can get him a job at her fathers mustard plant, at least until something better comes along.  Knowing a job in his own field would better suit him, Arlo refuses.  His attempts to find work are futile until he receives a phone call from another engineering firm, due to Roys influence.  The firm all but hires him over the phone, and Arlo is excited – until he finds out that his new job will be in Boise, Idaho.  With some hesitation, Arlo takes the job.

Arlo breaks the news to his family first over dinner by telling them he has a new job, but holds off until telling them of the move until towards the end of the conversation, prompting angry responses from both his wife and his daughter Casey.  The family uses a "swear jar" to collect cash penalties for the use of obscenities in the house, and Casey puts cash in the jar as she makes her feelings known.  Shortly afterwards, Monica retrieves her purse and withdraws a large sum of cash, ordering their twin sons Randy and Marshall to leave the room, implying that her own use of profanity will cost her dearly.

However, Monica calms down later that evening and agrees to the move, but Casey is much less willing to concede, even going so far as to sabotage their attempts to sell their home.  They finally tie Casey up in the basement as an attempt to control her, which finally makes Casey listen to reason. 

And when Cornell Crawford gets ready to mow his grass, he is interrupted by Pear, who tells him to put his contraption back in the garage and invest in a "human-sized mower".  As Cornell says "whos going to make me", hes answered with ferocious barking from Flipper, the Pears normally hopelessly lazy dog, apparently at the end of his own rope from all the moving mess.  Cornell immediately backs off, obviously alarmed, and expresses his admiration for his new neighbors.
The film ends with Arlo replying to his new neighbor by flipping him his index finger.

==Cast==
*Richard Pryor as Arlo Pear
*Beverly Todd as Monica Pear
*Stacey Dash as Casey Pear
*Randy Quaid as Frank and Cornell Crawford
*Dana Carvey as Brad Williams
*King Kong Bundy as Gorgo
*Ji-Tu Cumbuka as Edwards
*Robert LaSardo as Perry
*Morris Day as Rudy

==Reception==
The movie received mixed reviews. A negative one came from the Los Angeles Times, which stated that the film "is pretty flat as a comedy but is of interest as a case study in sociology, as the Pears could just as easily be white as black. Theres a certain irony that a comedy of errors, even a disappointing one, is set against the perfection of an idealized backdrop of a fully and harmoniously racially integrated society."    The newspaper also noted that, due to its heavy language, "  R rating is appropriate."  Janet Maslin, The New York Times film critic, provided a positive notice and stated that "Pryor presides over Moving with Cosbyesque geniality" and that he "does a lot to make   funny."  As of May 17, 2014, the film has no official score on Rotten Tomatoes based on 3 reviews (one of three reviews on the site was positive, while the other two reviews were negative).

===Box office===
The movie debuted at No.4.  The movie was a failure at the box office, grossing United States dollar|US$10,815,378.

==DVD==
The film was released on DVD as of August 22, 2006, as part of a double feature, packaged with Greased Lightning. Both movies are on the same side of a single disc.

==References==
 

== External links ==
 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 