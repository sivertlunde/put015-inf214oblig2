La Virgen Negra
{{Infobox film
| name           = La Virgen Negra
| image          = La Virgen Negra.png
| caption        = La Virgen Negra theatrical poster
| director       = Ignacio Castillo Cottin
| producer       = Ignacio Castillo Cottin & Nathalie Sar-Shalom
| writer         = Ignacio Castillo Cottin
| starring       = Carmen Maura Angélica Aragón Matheus Nachtergaele Jessika Grau Caridad Canelón
| music          = Erik Alvarez
| editing        = Danielle Firos
| distributor    = Pa Los Panas Producciones
| released       =  
| runtime        = 86 minutes
| country        = Venezuela Chile
| language       = Spanish
| budget         = 
| gross          = 
}} Venezuelan comedy romance and magic realism. It is the first movie of the 25-year-old filmmaker Ignacio Castillo. http://www.venelogia.com/archivos/2559/ 
 black virgin. 

The film is set in the town of Higuerote in the northern coastal state of Miranda (state)|Miranda, Venezuela  

==Primary cast==
*Angélica Aragón as Lurdita
*Carmen Maura as Sra. Isabel
*Matheus Nachtergaele as Dr. Joao Pinto Carolina Torres as Manita Francisco Diaz as Cura Isidro
*Caridad Canelón as Leonor Julio Rodríguez as Franklin
*Geily Rosales as La negrita
*Cynthia Cabrera as La blanquita
*Jesika Grau as Marlene
*César Suárez (actor)|César Suárez as Virgilio
*Martin Peyrou as Velero

==Plot==
The film begins with Franklin (Rodriguez) trying to give his first kiss to "la negrita" (Rosales) when he is suddenly interrupted by a desperate Manita (Torres), who is concerned because her husband hasnt had sex with her in months. 

Manita then visits a mysterious woman, who is believed to be a witch, Lurdita (Aragon) who asks her to change the virgin of the church by the black virgin. Behind everybodys back and with the approval of Sra Isabel (Maura), the founder of the town, the black virgin becomes their new patron.

After that, strange things begin to happen in this village changing the routine of its inhabitants:  the black virgin stopped it from getting dark, stopped the invasions of the vandals, make people fell in love and at the same time killed and sickened some. On the other hand, Lurdita (who is believed to be the builder of the black virgin) leaves the town saying others might need her help. 

One morning, Franklin is playing baseball and as the kids were making fun of him he hits the ball so hard that breaks the image of the black virgin. After this, the vandals took over the town and killed almost everyone on it. The only survivors are shipped in a boat to the middle of the sea. Suddenly it begins to dawn in the middle of the night and Lurdita is seen smiling at the shores of the beach marking the end of the movie.

==Production==

*The film was made by Ignacio Castillo son of Leopoldo Castillo (host of the controversial TV show, Alo Ciudadano
*The vocals of the music was done by Lisbeth Scott who also participated in the score of The Chronicles of Narnia and The Passion of the Christ. 

==Awards==
===Won===
*Mérida National Film Festival:
**Peoples Choice Award

===Nominated===
*32nd Annual São Paulo Film Festival
**Best New Filmmaker (Ignacio Castillo Cottin)

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 