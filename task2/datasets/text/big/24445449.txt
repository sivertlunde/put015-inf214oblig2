Yuddha Kaanda
 {{Infobox film
| name           = Yuddha Kaanda
| image          =
| image_size     =
| caption        =
| director       = K.V. Raju
| producer       = Raj Rohit Combines
| writer         =
| narrator       = Ravichandran Poonam Dhillon Bharathi Vishnuvardhan Shashi Kumar
| music          = Hamsalekha
| cinematography =
| editing        =
| distributor    =
| released       = 1989
| runtime        = 142 minutes
| country        = India Kannada
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Yuddha Kaanda ( ) is a 1989 Indian Kannada movie directed by K. V. Raju and starring V. Ravichandran|Ravichandran, Poonam Dhillon. It is a remake of the 1985 Hindi film Meri Jung.

==Cast== Ravichandran
* Poonam Dhillon
* Bharathi Vishnuvardhan
* Devaraj
* Shashikumar
* Jaggesh Ramakrishna
* Avinash
* Doddanna

==Soundtrack==

{{Infobox album  
| Name        = Yuddha Kanda
| Type        = Soundtrack
| Artist      = Hamsalekha
| Cover       =
| Released    =  
| Recorded    = Feature film soundtrack
| Length      =
| Label       = Lahari Audio
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) || Duration
|-
| 1 || Kudiyodhe Nanna || S. P. Balasubramanyam||
|-
| 2 || Sole Illa || S. P. Balasubramanyam, S. Janaki || 
|-
| 3 || Kempu Thotadalli || S. P. Balasubramanyam, Vani Jayaram, B. R. Chaya ||
|-
| 4 || Bolo Re Shanthi || S. P. Balasubramanyam, S. Janaki ||  
|-
| 5 || Nooraroorugalalli || S. P. Balasubramanyam, Latha Hamsalekha ||
|-
|}

==References==
*http://www.linuxbazar.com/yuddha-kanda-kannada-dvd-p-13380.html

 
 
 
 
 


 