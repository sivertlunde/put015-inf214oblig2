Andula Won
 
{{Infobox film
| name = Andula Won
| image =Andula Won.jpg
| image size =
| alt =
| caption =
| director = Miroslav Cikán
| producer =
| writer = Olga Scheinpflugová Josef Neuberg Jaroslav Mottl
| narrator =
| starring = Věra Ferbasová
| music =
| cinematography = Karl Degl
| editing = Antonín Zelenka
| studio =
| distributor =
| released = 2 December 1937
| runtime = 88 minutes
| country = Czechoslovakia
| language = Czech
| budget =
| gross =
| preceded by =
| followed by =
}} Czech comedy film directed by Miroslav Cikán. It was released in 1937.

==Cast==
* Věra Ferbasová - Andula Mrácková
* Hugo Haas - Pavel Haken
* Růžena Šlemrová - Hakenová
* Václav Trégl - Václav - Servant
* Saša Rašilov - Mrácek
* Stanislav Neumann - Tonda Mrácek
* Milada Smolíková - Aunt Kristýnka
* Anna Steimarová - T.O.Z.K. Club Directress
* Fanda Mrázek - Tramp
* Gustav Hilmar - Richard Kalous
* Marie Grossová - Kalous Wife
* Eva Prchlíková - Gisela Kalousová
* Vlasta Hrubá - Sylvie Símová
* Milada Gampeová - Sylvies Mother
* Dagmar Vondrová - Sylvies Friend

==External links==
*  

 
 
 
 
 
 
 

 
 