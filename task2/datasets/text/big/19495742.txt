Fodboldpræsten
 
{{Infobox film
| name           = Fodboldpræsten
| image          = Fodboldpræsten.jpg
| caption        = 
| director       = Alice OFredericks
| producer       = Henning Karmark
| writer         = Grete Frische Jens Locher
| starring       = Jørgen Reenberg
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen
| editing        = 
| distributor    = 
| released       =  
| runtime        = 99 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Fodboldpræsten is a 1951 Danish family film directed by Alice OFredericks.

==Cast==
* Jørgen Reenberg - Præst Søren Holm
* Grethe Thordahl - Inger Dahl
* Ib Schønberg - Direktør Jacob Dahl
* Peter Malberg - Degnen Ottesen
* Helga Frier - Fru Dahl
* Inger Stender - Aase Dahl
* Johannes Meyer - Prokurist Nielsen
* Erika Voigt - Menighedsrådsformand Fru Andersen
* Preben Neergaard - Ole Dahl William Rosenberg - Egon Petersen
* Buster Larsen - Mads
* Louis Miehe-Renard - Peter Hansen
* Tove Maës - Lilly
* Hannah Bjarnhof - Jytte Dahl
* Elise Berg Madsen - Lise Lotte Dahl
* Betty Helsengreen - Tjenestepigen Maria Henry Nielsen - Kustode
* Poul Müller - Læge
* Ole Monty - Tilskuer
* Gunnar Hansen - Himself
* Agnes Rehni - Peters Mor

==External links==
* 

 

 
 
 
 
 
 


 