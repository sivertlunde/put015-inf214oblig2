The Man in Half Moon Street
{{Infobox Film
| name           = The Man in Half Moon Street
| image          = The Man in Half Moon Street.jpg
| caption        = Theatrical poster 
| director       = Ralph Murphy
| producer       = Walter MacEwen
| writer         = Barré Lyndon (play) 
| narrator       = 
| starring       = Nils Asther Helen Walker
| music          = Miklós Rózsa Henry Sharp
| editing        = Tom Neff
| distributor    = Paramount Pictures
| released       = January 19, 1945
| runtime        = 92 min
| country        =   English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Picture of Dorian Gray, except that there are more logical explanations for the eternal youth of the main character. The film is based on a play by Barré Lyndon, and stars Nils Asther and Helen Walker with direction by Ralph Murphy.

==Cast==
* Nils Asther as Dr. Julian Karell
* Helen Walker as Eve Brandon
* Reinhold Schünzel as Dr. Kurt van Bruecken
* Paul Cavanagh as Dr. Henry Latimer
* Edmund Breon as Sir Humphrey Brandon
* Morton Lowry as Alan Guthrie
* Matthew Boulton as Det. Insp. Ned Garth
* Brandon Hurst as Simpson-Butler

==Plot==
A scientist, Dr. Karell (Asther), has found a way to prolong life (he is 120 years old) with the help of Dr. Van Bruecken (Schünzel). However, Dr. Karell has now fallen in love, and has discovered that if he doesnt get new glands, he will die.

==Home video==
This film is not available on DVD or VHS, although the 1959 Hammer Films remake, The Man Who Could Cheat Death, is available on DVD from Legend Films.

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 
 