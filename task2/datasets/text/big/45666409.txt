The Invitation (2015 film)
{{Infobox film
| name           = The Invitation
| image          = The Invitation (2015 film) POSTER.jpg
| director       = Karyn Kusama
| producer       = {{plainlist| Phil Hay
* Matt Manfredi
* Martha Griffin
* Nick Spicer
}}
| writer         = {{plainlist|
* Phil Hay
* Matt Manfredi
}}
| starring       = {{plainlist|
* Logan Marshall-Green
* Tammy Blanchard
* Michiel Huisman
* Emayatzy Corinealdi
}} Theodore Shapiro
| cinematography = Bobby Shore
| editing        = Plummy Tucker
| studio         = {{plainlist|
* Gamechanger Films
* Lege Artis
* XYZ Films
}}
| distributor    = Falcon Films
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = 
}} thriller film Phil Hay Drafthouse Films for distribution. 

== Plot == 
While attending a dinner party at his former home, a man thinks his ex-wife and her new husband have sinister intentions for their guests.

== Cast ==
* Logan Marshall-Green as Will
* Tammy Blanchard as Eden
* Michiel Huisman as David
* Emayatzy Corinealdi as Kira
* Karl Yune as Choi
* John Carroll Lynch as Pruitt
* Toby Huss as Dr. Joseph Mike Doyle as Tommy
* Michelle Krusiec as Gina
* Marieh Delfino as Claire
* Lindsay Burdge as Sadie
* Jay Larson as Ben

== Production == The Celebration. Let the Right One In was also an influence.   Shooting ended in July 2014. 

== Reception ==
Justin Chang of Variety (magazine)|Variety wrote, "This teasingly effective thriller represents director Karyn Kusamas strongest work in years."   Josh Kupecki of The Austin Chronicle wrote, "There are some very interesting ideas about grief, depression, and how we cope with life-changing events in this modern world, but ultimately, the film doesnt offer anything new to the dinner party from hell subgenre."   Dominick Suzanne-Mayer of Consequence of Sound rated it A− and wrote, "In so many words, The Invitation is supremely well-crafted."   Samuel Zimmerman of Shock Till You Drop wrote, "The Invitation is a startlingly adult thriller that, unlike Eden and her guests, is willing stare down the weight our lives can bear."   Peter Martin of Twitch Film wrote, "The sincerity and craft on display notwithstanding, the movie achieves a limited, unsettling level, and then stops right there."   Drew Tinnin of Dread Central rated it 4/5 stars and wrote, "The Invitation works so well because it taps into our general distrust of the world around us and how our survival instinct has been muted and ignored in order to maintain the appearance of being polite."   Matt Donato of We Got This Covered rated it 2.5/5 stars and wrote, "The Invitation is a slow, SLOW burn that fails to find the winning ingredient that makes for a perfect dinner party thriller."   Heather Wixson of Daily Dead rated it 4.5/5 stars and called it "some of the most assuredly confident and nuanced work from her to date and one of the most devastating horror films I’ve seen in years." 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 