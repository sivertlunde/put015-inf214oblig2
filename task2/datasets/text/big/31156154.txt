Jeevitha Samaram
{{Infobox film 
| name           = Jeevitha Samaram
| image          =
| caption        =
| director       = Sathyan Bose
| producer       = Tharachand Bharjathya
| writer         = Vishwanath Rai Abhayadev (dialogues)
| screenplay     =
| starring       = BG Jagirdar Dharmendra Kanyalal Leela Chitnis
| music          = Laxmikant Pyarelal
| cinematography = Madan Sinha
| editing        = K.Narayanan
| studio         = Rajasree Productions (P) Ltd.
| distributor    = Rajasree Productions (P) Ltd.
| released       =  
| country        = India Malayalam
}}
 1971 Cinema Indian Malayalam Malayalam film, directed by Sathyan Bose and produced by Tharachand Bharjathya. The film stars BG Jagirdar, Dharmendra, Kanyalal and Leela Chitnis in lead roles. The film had musical score by Laxmikant Pyarelal.   

==Cast==
 
*BG Jagirdar
*Dharmendra
*Kanyalal
*Leela Chitnis
*Master Bunty
*Rajendranth
*Ramesh Dev
*Sabnam
*Seb Rehman
*Vipin Gupta
*Rakhi 
 

==Soundtrack==
The music was composed by Laxmikant Pyarelal and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chinnum venthaarathin || S Janaki || P. Bhaskaran || 
|-
| 2 || Chinnum venthaarathin || K. J. Yesudas, S Janaki || P. Bhaskaran || 
|-
| 3 || Hey Maane || S Janaki || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 