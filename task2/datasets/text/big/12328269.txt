Fire Sale (film)
{{Infobox Film
| name           = Fire Sale
| image          = 
| image_size     = 
| caption        = 
| director       = Alan Arkin
| producer       = Marvin Worth
| writer         = Robert Klane Based on his novel
| narrator       = 
| starring       = Alan Arkin Rob Reiner Vincent Gardenia Anjanette Comer Kay Medford Sid Caesar Byron Stewart
| music          = Dave Grusin
| cinematography = Ralph Woolsey
| editing        = Richard Halsey
| distributor    = 20th Century Fox
| released       = June 9, 1977
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $1.5 million 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Fire Sale is a 1977 comedy film starring Alan Arkin (who also directed) as Ezra Fikus; Rob Reiner as his brother Russel; Vincent Gardenia as their father Benny; Sid Caesar as Bennys brother Sherman; Anjanette Comer as Marion (Ezras wife); and Kay Medford as Ruth (Bennys wife).

==Synopsis==
Benny Fikus decides to cash in on his business fire insurance by committing arson.  Benny plans to have Sherman, who is in a mental hospital believing that World War II is still being fought, escape and burn down Bennys failing clothing store which he has made Sherman believe is a Nazi military headquarters.

During a vacation trip with Marion, Benny has a heart attack, and his sons Ezra and Russel take over the store.  The low self-esteemed Russell wants to expand the store and marry his girlfriend, while Ezra needs money to adopt an orphaned 68" African-American teenage boy named Booker T (Byron Stewart).  Ezra needs Booker T. to play on the high-school basketball team he coaches because he has won a total of two games in seven years as a coach and is in danger of losing his job.   

Russell discovers that his father is bankrupt, with his only asset being the surrender value on the stores fire insurance policy.  Russell cashes in the policy and splits it with Ezra, using the money to buy more stock.  Back at home, Benny is practically comatose after his heart attack. An "in denial" Marion is told by house painters helping with a home redecoration project that her husband is "very sick" which she interprets as Benny being already dead.  She then decides to change redecoration plans to prepare for funeral services.

Meanwhile, Sherman has escaped and is on his way to burn down the "Nazi Headquarters" (as he believes the store is).  Benny recovers from his heart attack, and informs Russell that Sherman is on his way to burn down the store so they can collect the fire insurance that they no longer have.

Hilarity ensues as Ezra has his wife run his basketball team while he and Russell attempt to stop Sherman from his quest to fight the Huns by any means necessary.

==Notes==

*This movie received a "BOMB" rating in a review by critic Leonard Maltin.
*This was Byron Stewarts first ever acting role and, per Ken Howards commentary on The White Shadow Season 1 DVDs, got him the role of Warren Coolidge.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 