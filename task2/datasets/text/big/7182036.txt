Mrugaraju
{{Infobox film
| name           = Mrugaraju
| image          =
| caption        =
| director       = Gunasekhar
| producer       = Devi Vara Prasad
| writer         = Gunasekhar Simran Sanghavi Nagendra Babu
| music          = Mani Sharma
| cinematography =
| editing        =
| distributor    =
| released       = January 11, 2001
| runtime        =
| language       = Telugu
| budget         =
| preceded_by    =
}} Tollywood Action Tamil with the title Vettaikkaaran.The Hindi version of this is available named "Rakshak, The Protector". The  film was released along with deviputhrudu and narasimha Naidu on the same day. 
== Plot ==
In a jungle, there lives a man-eating lion.  The latest victim of lions havoc is the death of the chief engineer who is constructing the rail bridge.  The Railway department asks Aishwarya (Simran (actress)|Simran) to go on the mission of constructing the bridge. Even after Aishwarya resumes the duty, Lion starts terrorizing the crew of the construction team.  The ace hunter of Adavipalli, Raju (Chiranjeevi), is entrusted with the job of hunting the lion.  The rest of the first half is all about how Raju traps the lion and kill it at the cost of Appanna Dora (Nagendra babu). But, the other lion in the jungle gets enraged at Raju for killing its companion.  There is another trouble for the bridge project in the form of forest ranger (Surya) and a local smuggler (Ramireddy).  Raju gets rid of these two goons by the interval.  Then the viewers are revealed that Aishwarya is the wife of Raju. In the second half, the flashback is revealed. Raju comes to city to participate in the crafts mela to sell his herbs from the jungle. There he meets Aishwarya, who has come to visit a fun stall along with her fiancée Vicky (Raja Ravindra).  When Aishwarya tries to take a shot at shooting the balloon, Vicky disappoints her saying that she cannot do it. Raju, who is an onlooker comes forwards and gives her confidence and tips to shoot. Then Aishwaryas bullet hits the bulls eye.  Impressed with Rajus help, she invites him to come to her engagement.  At the engagement, Raju spots Vicky flirting with another girl and tells him about the importance of being a good human being.  Aishwarya watches the entire episode from the behind and she decides that Raju is a loyal man compared to the flirtatious Vicky.  And she seeks for Rajus hand and then they get married.  The father of Aishwarya cunningly separates the couple by creating misunderstandings.  The rest of the film is about how the hunter Raju and engineer Aishwarya sort out the matters and complete the construction of the bridge.

== Cast ==
* Chiranjeevi...... ....  Raju(supreme hero)(mega star)(dance king) Simran ....  Aishwarya as engineer
* Sanghavi ....  Sivangi 
* Nagendra Babu ....  Appanna Dora 
* Brahmanandam
* Prakash Raj 
* Tanikella Bharani   
* Chalapathi Rao
* Gundu Hanumantha Rao
* L.B. Sriram Rambha ....  Guest role in the song Ramaiah Padaletti 
* Raja Ravindra
* Kovai Sarala Vijayakumar

== Crew ==
* Director: Gunasekhar
* Writing Credits: Gunasekhar(story), Yandamoori Veerendranath (story) & Satyanand
* Producer: Devi Vara Prasad
* Music: Mani Sharma
* Cinematography: Sekhar V. Joseph
* Editor: Kotagiri Venkateswara Rao
* Art: Thotta Tharani
* Assistant Director: Yadagiri Vijayan
* Chandrabose & Bhuvana Chandra

== Soundtrack ==
{{Infobox album 
| Name       = Mrugaraju
| Artist     = Mani Sharma
| Type       = Soundtrack
| Released   = 
| Recorded   = 
| Genre      = Film soundtrack
| Length     =  
| Label      = 
| Producer   = Mani Sharma
| Last album = 
| This album = 
| Next album = 
}}

The soundtrack of the film was composed by Mani Sharma. The album consists of six songs. Lyrics for the three songs were penned by Veturi Sundararama Murthy and the remaining three songs were written by Chandrabose (lyricist)|Chandrabose, Kulasekhara and Bhuvanachandra.

{{tracklist
| headline       = Tracklist
| extra_column   = Singer(s)
| total_length   = 34:11
| lyrics_credits = yes
| title1         = Ramayya Padaletti Veturi
| extra1         = Shankar Mahadevan
| length1        = 6:00
| title2         = Aley ley Aley ley
| lyrics2        = Veturi
| extra2         = Udit Narayan, S. P. Sailaja
| length2        = 5:36
| title3         = Sathamana Mannadile
| lyrics3        = Veturi
| extra3         = Hariharan (singer)|Hariharan, Sadhana Sargam
| length3        = 5:38
| title4         = Chai Chai Chandrabose
| extra4         = Chiranjeevi
| length4        = 6:00
| title5         = Hangama Hangama
| lyrics5        = Kulasekhar
| extra5         = Raghu Kunche, K. S. Chithra
| length5        = 5:29
| title6         = Dammentho
| lyrics6        = Bhuvanachandra
| extra6         = Sukhwinder Singh, Swarnalatha
| length6        = 5:28
}}

== External links ==
*  

 

 
 