Anna Proletářka
{{Infobox film
| name           = Anna Proletárka
| image          = 
| caption        = 
| director       = Karel Steklý
| producer       = 
| writer         = Ivan Olbracht Karel Steklý
| screenplay     = 
| story          = 
| based on       =   -->
| starring       = Marie Tomásová Josef Bek Jana Dítetová Borivoj Kristek
| music          = Jan Seidel
| cinematography = Rudolf Stahl
| editing        = Jirina Lukesová
| studio         = 
| distributor    = 
| released       =  
| runtime        = 140 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
| gross          = 
}}

Anna Proletářka is a 1953 Czechoslovak film based on the novel by Ivan Olbracht.  The movie was filmed on location in Osek (Teplice District)|Osek, and Prague. It was released on 20 February 1953. It describes proletarian life in the Czech Lands after World War I. {{cite web 
| title = Plot Synopsis (for Book)
| url  = http://www.ctenarsky-denik.cz/215-Ivan-Olbracht-Anna-proletarka
| publisher = Ctenarsky-denik.cz
| accessdate = 2008-01-22 }} 

==Cast==
* Marie Tomásová as Anna
* Josef Bek as Tonik
* Jana Dítetová as Mana
* Borivoj Kristek as Bohous
* Bedrich Karen as Rubes
* Jarmila Májová as Rubesova
* Libuse Pospísilová as Dadla
* Oldrich Velen as Dr. Houra
* Vítezslav Vejrazka as Jandak
* Sasa Lichy as Jarous
* Martin Ruzek as Podhradsky
* Frantisek Vnoucek as Tusar
* Karel Máj as Habrman Theodor Pištěk as Nemec

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 

 
 