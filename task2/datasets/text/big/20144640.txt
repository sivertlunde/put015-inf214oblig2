The Story of Jang-hwa and Hong-ryeon
{{Infobox film
| name           = Dae Jang-hwa Hong-ryeon jeon
| image          =
| caption        =
| director       = Chang-hwa Jeong
| producer       = Hwa-ryong Lee
| writer         = Keum-dong Choi Jo Mi-ryeong Choi Nam-hyeon Um Aing-ran
| music          = Chun-seok Park
| cinematography = Mun-baek Lee
| editing        = Hui-su Kim
| distributor    =
| released       =  
| runtime        = 115 minutes
| country        = South Korea
| language       = Korean
| budget         =
| gross          =
| film name      = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Dae Jang-hwa Hong-ryeon jeon
 | mr             = Tae Chang-hwa hong-ryŏn jŏn}}
}}
The Story of Jang-hwa and Hong-ryeon (Dae Jang-hwa Hong-ryeon jeon) is a 1962 South Korean film directed by Chung Chang-wha. The film is based on a Korean folklore story called Janghwa Hongryeon jeon  which had been adapted into film versions in Janghwa Hongryeon jeon (1924 film)|1924, Janghwa Hongryeon jeon (1936 film)|1936, Janghwa Hongryeon jeon (1956 film)|1956, Dae Jang-hwa Hong-ryeon jeon|1962, Janghwa Hongryeon jeon (1972 film)|1972, A Tale of Two Sisters|2003, and The Uninvited (2009 film)|2009.

==Cast== Jo Mi-ryeong
*Choi Nam-hyeon (최남현)
*Um Aing-ran

==References==
 

==External links==
* 

 

 
 
 

 