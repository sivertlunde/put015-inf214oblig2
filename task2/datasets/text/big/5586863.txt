Hamlet (1921 film)
Hamlet of the Danish silent film actor Asta Nielsen. It was directed by Svend Gade and Heinz Schall.

In this interpretation, inspired by Dr. Edward P. Vinings book The Mystery of Hamlet, Hamlet is born female and disguised as a male to preserve the lineage.  Though a radical interpretation, the New York Times said this film, "holds a secure place in class with the best."

== Further reading ==
* Buchanan, Judith (2009). Shakespeare on Silent Film: An Excellent Dumb Discourse. Cambridge: Cambridge University Press. Ch. 7. ISBN 0-521-87199-9
* Howard, Tony (2007). Hamlet as a Woman. Cambridge: Cambridge University Press. ISBN 0-521-86466-6

==External links==
*   - an examination of the idea of Hamlet as a woman, with a focus on this film
* 

 

 
 
 
 
 
 
 