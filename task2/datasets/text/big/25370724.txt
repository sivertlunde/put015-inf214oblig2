Zweiohrküken
{{Infobox film
| name           = Zweiohrküken
| image          = Zweiohrkueken.jpg
| caption        = Film poster
| director       = Til Schweiger
| producer       = Til Schweiger Thomas Zickler
| writer         = Til Schweiger Anika Decker
| narrator       = 
| starring       =Til Schweiger Nora Tschirner
| music          = Daniel Nitt Dirk Reichardt Mirko Schaffer
| cinematography = 
| editing        = Constantin von Seld 
| distributor    = Warner Bros.
| released       =  
| runtime        = 124 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = $45,302,911
}} produced and directed by Til Schweiger. A sequel to 2007s Keinohrhasen (Rabbit Without Ears), it was co-written by Anika Decker and Schweiger, starring Schweiger and Nora Tschirner as former yellow press reporter Ludo and his girlfriend Anna.

Produced by Barefoot Films and Warner Bros., the film premiered in theaters across Germany and Austria on December 3, 2009.

== Plot ==

Ludo Decker and Anna Gotzlowski have been living together for two years. Everyday routine has set in, with Ludo neglecting his household responsibilities much to Annas dislike, while he is in turn annoyed by her constant complaining. When Ludo runs into his former lover Marie in the disco, and Annas old boyfriend Ralf comes to stay in their flat for a few days, the young couple faces serious jealousy and doubt in their relationship. Anna secretly reads Ludos phone messages, while he in turn stumbles upon "The List", an account of Annas former lovers—including Ralf, who scores better than Ludo does.

After a number of provocations, Ludo beats Ralf in a restaurant, and angrily leaves Anna. Even though the two still strongly care for each other, they end up sleeping with their respective ex-lovers. Ralf confesses that he never stopped loving Anna, but she rejects him and tells Ludo what happened. Even though Ludo has cheated on her as well, he angrily argues that she herself told him that, unlike men, women do not actually sleep with someone without feelings being involved. Anna begs him to come home, but he only says that he has no home any more, and leaves.

Meanwhile, Ludos best friend Moritz desperately tries to get more successful with women, and ends up in a number of absurd situations. In the end, he meets a young and beautiful Hypersexuality|sex-addict named Lana.

After a few weeks, Anna receives a letter from Ludo, who has traveled back to the place where they spent their only vacation together. He writes that he misses her terribly, and that he wants nothing more than to come back, have children, and spend the rest of his life with her. Anna follows him and finds him on a lonely beach, where she tells him that she wants a "little Ludo" before they can talk about a "little Anna". In the end, they return home together.

A further sequel is planned.

== Cast ==
* Til Schweiger as Ludo Decker
* Nora Tschirner as Anna Gotzlowski
* Matthias Schweighöfer as Moritz
* Ken Duken as Ralf Edita Malovčić as Marie
* Emma Tiger Schweiger as Cheyenne-Blue
* Annika Ernst as Judith
* Pegah Ferydoni as Lana
* Jasmin Gerat as Caro
* Heiner Lauterbach as Herb
* Uwe Ochsenknecht as Dr. Eisenberger
* Karoline Schuch as Lena Luna Marie Schweiger as young Anna  Valentin Florian Schweiger as young Ludo  Lilli Camille Schweiger as young Ludo 
*   as the beauty surgeon
* Thomas Kretschmann as himself
* Wladimir Klitschko as himself
* Yvonne Catterfeld as herself
* Johannes B. Kerner as himself

== References ==
 

== External links ==
*  
*    

 

 
 
 
 
 
 
 
 