The Ferocious Saladin
{{Infobox film
| name = The Ferocious Saladin
| image =
| image_size =
| caption =
| director = Mario Bonnard
| producer = 
| writer = Ettore Maria Margadonna   Gino Rocca   Mario Bonnard 
| narrator = Angelo Musco   Alida Valli   Lino Carenzio   Mario Mazza
| music = Giulio Bonnard 
| cinematography = Carlo Montuori
| editing = Eraldo Da Roma 
| studio = Capitani Film 
| distributor = Generalcine 
| released = 1937
| runtime = 90 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Ferocious Saladin (Italian:Il feroce Saladino) is a 1937 Italian  , this was one of the films he saw being made. 

The films sets were designed by art director Alfredo Montori.

==Cast== Angelo Musco as il professor Pompeo Darly / Il Feroce Saladino
*Alida Valli as Dora Florida / Sulamita
*Lino Carenzio as Gastone, il fine dicitore Mario Mazza as lacrobata Johnson / Tarzan
*Rosina Anselmi as Amalia, la moglie di Pompeo
*Maria Donati as Yvonne, la tenutaria della pensione per artisti
*Nicola Maldacea as ciambellano
*Nino Marchesini as il commendator Fani
*Luigi Zerbinati as il segretario di Fani
*Carlo Duse as il regista del teatro di rivista
*Giuseppe Pierozzi as un signore a teatro
*Claudio Ermelli as lusciere del teatro
*Eduardo Passarelli as un signore ipnotizzato
*Paolo Ferrara as Girolamo Mipaghi Eugenio Colombo as lamministratore del teatro
*Checco Durante as il direttore del teatro Vittoria
*Alfredo Martinelli as il direttore dorchestra del Teatro Apollo
*Pina Gallini as una signora sul treno
*Alberto Sordi as luomo nascosto sotto il costume del leone
*Elli Parvo as lattrice truccata allorientale
*Giuliana Gianni as una ballerina
*Margot Pellegrinetti as il soprano Carlo Cecchi as il signore calvo che compra le caramelle a teatro
*Pina Renzi as lattrice nervosa dal parlare incomprensibile
*Armando Fineschi as il marito dellattrice nervosa
*Rocco DAssunta as il conte di Montholon
*Carla Candiani as la contessa Albina di Montholon

== References ==
 

== Bibliography ==
* Gundle, Stephen. Mussolinis Dream Factory: Film Stardom in Fascist Italy. Berghahn Books, 2013.

== External links ==
*  

 
 
 
 
 
 
 
 


 