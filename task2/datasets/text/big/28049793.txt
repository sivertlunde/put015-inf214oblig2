Date Bait
{{Infobox film
| name           = Date Bait
| image_size     =
| image	=	Date Bait FilmPoster.jpeg
| caption        =
| director       = ODale Ireland
| producer       = George Caras (associate producer) Nicholas Carras (associate producer) ODale Ireland George S. Reppas (executive producer)
| writer         = Ethelmae Wilson Page
| based on = story by ODale Ireland Ethelmae Wilson Page Robert Slaven
| narrator       =
| starring       = Gary Clarke
| music          = Nicholas Carras
| cinematography = Lawrence Raimond
| editing        = Anthony DiMarco
| distributor    = Filmgroup
| released       = 1960
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Date Bait is a 1960 American film directed by ODale Ireland.

==Plot==
U.S. film. Delinquent teenage drama! Two confused teenagers are in love and want to get hitched, but their uptight parents say, "no way! The couple finds themselves on the sleazy side of town where they are victimized by the new brides hoodlum ex-boyfriend and his psychotic drug-addicted brother! Big trouble! The teens must also contend with other assorted criminals, including drug dealers and mobsters!

== Cast ==
*Gary Clarke as Danny Logan
*Marlo Ryan as Sue Randall
*Dick Gering as Brad Martinelli
*Carol Dawne
*Jon Lindon
*Gabe Delutri
*Michael Bachus
*Mildred Miller
*Steve Ihnat
*Chad Williams
*Lemar Crast
*Rita Guinan
*Anton von Stralen
*Trep Howard
*Reggie Perkins as Title Song Singer (voice)
*Johnny Faire as Singer (voice)

== Soundtrack ==
*Reggie Perkins - "Date Bait Baby"
*   complete soundtrack album released by Monstrous Movie Music

== External links ==
* 
* 
*List of American films of 1960

 
 
 
 
 
 
 
 