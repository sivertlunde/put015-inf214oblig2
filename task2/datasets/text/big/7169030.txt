Children of Divorce (1927 film)
{{Infobox film
| name           = Children of Divorce
| image          = Childrenofdivorce.JPG
| alt            = 
| caption        = Theatrical release poster
| director       = {{Plainlist|
* Frank Lloyd
* Josef von Sternberg  
}}
| producer       = {{Plainlist|
* Jesse L. Lasky
* E. Lloyd Sheldon
* Adolph Zukor
}}
| writer         = {{Plainlist|
* Louis D. Lighton
* Hope Loring
* Alfred Hustwick  
}}
| story          = Adela Rogers St. Johns
| based on       =  
| starring       = {{Plainlist|
* Clara Bow
* Esther Ralston
* Gary Cooper
* Einar Hanson
* Norman Trevor
}}
| music          = 
| cinematography = {{Plainlist|
* Norbert Brodine
* Victor Milner
}}
| editing        = E. Lloyd Sheldon
| studio         = Famous Players-Lasky
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = Silent English intertitles
| budget         = 
| gross          = 
}}
 silent romantic Owen Johnson, and written by Louis D. Lighton, Hope Loring, Alfred Hustwick, and Adela Rogers St. Johns, the film is about a young flapper who tricks her wealthy friend into marrying her during a night of drunken revelry. Even though she knows that he is in love with another woman, she refuses to grant him a divorce and repeat the mistake of her divorced parents. Produced by Jesse L. Lasky, E. Lloyd Sheldon, and Adolph Zukor for the Famous Players-Lasky, the film was released on April 25, 1927 by Paramount Pictures.   

==Plot==
Jean Waddington (Esther Ralston) and Ted Larrabee (Gary Cooper) grew up together in an affluent society, the children of divorced parents. Most of their friends have cynical attitudes towards love and marriage, but Jean and Ted are more serious. In fact, Jean has fallen in love with Ted, who one day proposes marriage. Knowing, however, that Teds father was unfaithful to his wife and irresponsible, Jean demands that he prove himself before she accepts his proposal. Soon Ted starts a business and opens up an office in the building where their mutual friend Kitty Flanders (Clara Bow) works. Kitty is also a child of divorce.

One evening, Kitty throws a wild party at work, and Ted takes part in the revelry. At the party, Kitty meets Prince Ludovico de Saxe (Einar Hanson) and is immediately attracted to him. The prince return her affection, but the princes guardian Duke Henri de Goncourt (Norman Trevor) prevents them from seeing each other because she is not of their social class. Raised by a mother who insisted that she marry a wealthy man, Kitty soon sets her sights on Ted—even though she knows that Ted and her close friend Jean love each other. One evening, after going on a drunken spree, Kitty tricks Ted into marrying her, even though she does not love him.

Desperately unhappy, Ted assures Jean that he will seek a divorce as soon as possible. Not wanting him to repeat the mistakes of their parents, Jean refuses to marry him if he divorces, and sails off for Europe. The arrival of their baby does little for their marriage, and Ted avoids spending any time with his unwanted wife. Sometime later, Kitty and Ted and their child visit the prince, whom Kitty once loved. Kitty remembers her feelings for the prince and dreams of marrying him someday. When she learns that he can never marry a divorced woman for religious reasons, she poisons herself.

==Cast==
* Clara Bow as Kitty Flanders
* Esther Ralston as Jean Waddington
* Gary Cooper as Edward D. Ted Larrabee
* Einar Hanson as Prince Ludovico de Saxe
* Norman Trevor as Duke Henri de Goncourt
* Hedda Hopper as Katherine Flanders
* Edward Martindel as Tom Larrabee
* Julia Swayne Gordon as Princess de Saxe
* Tom Ricketts as The Secretary
* Albert Gran as Mr. Seymour
* Iris Stuart as Mousie
* Margaret Campbell as Mother Superior
* Percy Williams as Manning
* Joyce Coad as Little Kitty
* Yvonne Pelletier as Little Jean

==Production==
Clara Bow was largely responsible for getting Cooper the lead male role in the film. Meyers, p. 40.  She assured the young actor, "Well go places and do things together. Well become an item."  The inexperienced and miscast Cooper, who had mainly appeared in minor roles in Westerns up to that point, was not prepared to play a sophisticated New Yorker in a love story.  Coopers initial party scenes and love scenes were particularly difficult for the young actor from Montana, and after numerous frustrating retakes, Cooper disappeared from the set. Meyers, pp. 40–41.  The studio replaced him with an actor whose performances were just a bad as Coopers. Dickens, p. 34.  A disheveled Cooper was found a few days later and was asked to complete the film. Meyers, p. 41. 

==References==
;Notes
 
;Bibliography
 
*  
*  
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 