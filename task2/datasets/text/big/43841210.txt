All Apologies (film)
{{Infobox film
| name           = All Apologies
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Emily Tang
| producer       = Yang Jian Chow Keung 
| writer         = 
| screenplay     = Han Jie Emily Tang Dong Fang 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Roger Lin
| cinematography = Lai Yiu-fai
| editing        = Chow Keung Baek Seung-han 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 89 minutes
| country        = China
| language       = Mandarin
| budget         = 
| gross          = 
}}

All Apologies (愛的替身 Ai De Ti Shen) is a 2012 Chinese drama film directed by Emily Tang. It had its premiere on September 26, 2012 at the San Sebastián International Film Festival.   

==Plot==
The film is set in Guilin, Guangxi. 

==Cast==
*Cheng Taishen as Cheng Yonggui 
*Yang Shuting  as Li Qiaoyu 
*Liang Jing  as Yunzhen 
*Gao Jin  as He Man 
*Ge Ge 
*Qu Yi as Dazhuang 
*Tang Xi 
*Chen Bing as Huang 
*Yang Shu 
*Zou Xi as Tie Niu 
*Xiao Jinan 
*Gao Yinan 

==Reception==
At the 2012 San Sebastián International Film Festival the film was in the Official Selection,  in competition for the Golden Shell. It was shown in the A Window on Asian Cinema section at the 17th Busan International Film Festival.

On Film Business Asia, Derek Elley gave the film a grade of 6 out of 10, calling it an "affecting story of surrogate motherhood". 

==References==
 

==External links==
* 

 
 
 
 
 

 
 