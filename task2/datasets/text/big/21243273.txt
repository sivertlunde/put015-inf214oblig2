There's Always a Thursday
{{Infobox film
| name           = Theres Always a Thursday
| image          = "Theres_Always_a_Thursday"_(1957).jpg
| caption        = British quad poster Charles Saunders
| producer       = Guido Coen
| writer         = Brandon Fleming
| starring       = Charles Victor   Jill Ireland
| music          = Reg Owen Anthony Spurgin
| cinematography = Brendan J. Stafford
| editing        = Tom Simpson
| distributor    = Associated Sound Film Industries
| released       = 1957
| runtime        = 60 min
| country        = United Kingdom
| language       = English
}} British crime Charles Saunders and starring Charles Victor, Jill Ireland, Lloyd Lamble and Robert Raglan.  Much of the film was shot at Southall Studios.   

==Plot==
Comedy about a down-trodden clerks new found fame as the director of a racy lingerie firm, after an innocent encounter with a fast woman is misreported and earns him the reputation of a suburban Romeo.

==Cast==
* Charles Victor as George Potter
* Frances Day as Vera Clandon
* Marjorie Rhodes as Marjorie Potter
* Bruce Seton as James Pelly
* Robert Raglan as Crosby
* Jill Ireland as Jennifer Potter
* Richard Thorp as Dennis Potter
* Lloyd Lamble as Detective Sergeant Bolton
* Patrick Holt as Middleton
* Ewen Solon as Inspector Bradley
* Alex Macintosh as TV Interviewer
* Reginald Hearne as Bannister
* Deidre Mayne as Miss Morton
* Glen Alyn as Mrs. Middleton Alexander Field as Tramp
* Martin Boddey as Sergeant

==Critical reception==
TV Guide wrote that a "good performance by Victor and an intelligent script lift this one above the ranks."  

==References==
 

== External links ==
*  

 
 
 
 
 
 
 


 
 