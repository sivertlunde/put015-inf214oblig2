Project Almanac
 
{{Infobox film
| name           = Project Almanac
| image          = Project Almanac poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Dean Israelite
| producer       = Andrew Form Bradley Fuller Michael Bay
| writer         = Jason Harry Pagan Andrew Deutschman
| starring       = Jonny Weston Sofia Black DElia Sam Lerner Allen Evangelista Virginia Gardner Amy Landecker
| cinematography = Matthew J. Lloyd
| editing        = Julian Clarke Martin Bernfeld
| studio         = Insurge Pictures Platinum Dunes MTV Films 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 106 minutes  
| country        = United States
| language       = English
| budget         = $12 million 
| gross          = $32.2 million    
}}
 found footage science fiction thriller film directed by Dean Israelite and written by Jason Harry Pagan and Andrew Deutschman. Filmed in 2013 and originally planned for an early 2014 release, the release date was later moved to January 30, 2015. 

==Plot==
In 2014, 17-year-old high school senior and aspiring inventor David Raskin (Jonny Weston) is admitted into Massachusetts Institute of Technology|MIT, but is unable to afford its tuition fees. Upon learning his mother, Kathy Raskin (Amy Landecker), is planning to sell the house, David enlists his sister Christina (Virginia Gardner) and his friends Adam Le (Allen Evangelista) and Quinn Goldberg (Sam Lerner) to go through the belongings of his father, Ben Raskin (Gary Weeks), an inventor who died in a car crash on Davids 7th birthday, in the hope of finding something that David can use to get a scholarship. David ends up finding an old camera with a video recording of his birthday, in which he briefly spots his 17-year-old self in a reflection. The four later find the blueprints of a temporal relocation device that Ben was developing for the United States military, and use the available resources to build a functional time machine, including hydrogen canisters stolen from their school. David, Christina, Adam, and Quinn later use the battery from the car of Davids longtime crush, Jessie Pierce (Sofia Black DElia), who is attending a party in the neighborhood, to charge up the machine, and successfully send a toy car back in time, blowing out the power for the entire neighborhood. They end up being caught by Jessie and recruit her to their experiment, which they nickname "Project Almanac."

David, Jessie, Christina, Adam, and Quinn eventually travel back in time to the day before and break into Quinns house, where they draw a smiley face on the back of the neck of a sleeping past Quinn (which simultaneously appears on the back of the visiting Quinns neck). However, he awakens and seeing his future self nearly erases them both from the timeline. The five agree to use the machine for personal gain on the condition that they always use it together. Adam uses it to win the lottery, Christina gets back at her bullies, Quinn aces a chemistry test to secure his academic future, and the group eventually decides to travel back to Lollapalooza three months before. David hesitates to declare his feelings for Jessie, and decides to travel back in time alone to change that, leading to a future in which they are a couple. When he returns, he finds that the blackout caused by them sending the toy car back resulted in their schools star basketball player getting hit by a car and breaking his leg; the team doesnt make it to the championships, and those that would have attended go elsewhere, including a pilot who ends up crashing a commercial airliner. David goes back alone once again and prevents the accident that would lead to the players injury, and averts the plane crash. He returns to the future to learn that instead Adam is in critical condition in the hospital after being run over.

Jessie confronts David, who is forced to admit to using the time machine to win her affection. They travel back in time together to rectify Davids mistakes and save Adam, but Jessie ends up running into her past self and is erased from the timeline, causing an even more devastating ripple effect, leading to worldwide catastrophes. David decides to go back to prevent the machine from being created, but is confronted by the police, who suspect him of being connected to Jessies disappearance. David escapes to his school to use a hydrogen canister to return to 2004. He manages to activate the machine just as the police break into the supply room. They fire shots at him, which ignite the Hydrogen and send David back to his 7th birthday

At the basement, David confronts his father Ben, who recognizes him and deduces that hell eventually complete the machine. David convinces Ben of the machines danger and tells him that he should say goodbye to his son. Meanwhile, David destroys the blueprints and a vital circuit, before he is erased from the timeline. However, his camera is left behind and records the whole thing. Back in the future, David and Christina are once again going through their fathers belongings, when they find their fathers camera as well as the alternate Davids camera with footage of them from minutes before. Later, at school, David approaches Jessie for the first time once again, displaying knowledge of the future, and confides in her that theyre about to "change the world."

==Cast==
* Jonny Weston as David Raskin
* Sofia Black DElia as Jessie Pierce
* Sam Lerner as Quinn Goldberg
* Allen Evangelista as Adam Le
* Virginia Gardner as Christina Raskin
* Amy Landecker as Kathy Raskin
*Gary Weeks as Ben Raskin
* Michelle DeFraites as Sarah Nathan Patrick Johnson as Todd
* Gary Grubbs as Dr. Lu
* Katie Garfield as Liv

The members of rock bands Imagine Dragons and Atlas Genius appear briefly as themselves.

==Production and release==
Principal photography began in June 2013 in Atlanta, Georgia (U.S. state)|Georgia.  

On February 5, 2014 Paramount Pictures postponed the release date from February 28, 2014, to an unknown date. Paramount partnered with MTV Films on the marketing of the film. On March 24, 2014, The Hollywood Reporter|THRs Borys Kit tweeted that the film had been re-titled to "Project Almanac."  The film was released on January 30, 2015.   

 

==Reception==
=== Box office ===
As of February 7, 2015, Project Almanac earned a gross of $21,671,849 in North America and $5,400,000 in other territories for a worldwide total of $27,071,849 against a budget of $12 million.  

The film earned $3.1 million in its opening day (including previews) which was far below industry’s mid-day estimate.    The film made a total of estimated $3,936,000 in its second day and $1,374,000 for its third day, bringing in a weekend gross of $8,500,000 playing in  2,893 theaters with a per-theatre average of $2,938 and ranking #3.  

The films opening weekend gross ranks #18 among the found footage genre. 

=== Critical response ===
Project Almanac has received mixed reviews. On  , the film has a score of 47 out of 100, based on 25 critics, indicating "mixed or average reviews."  In CinemaScore polls conducted during the opening day, cinema audiences gave the film an average grade of "B" on an A+ to F scale.  

John DeFore of The Hollywood Reporter in his review said that the film "begins as a marginally fun diversion before proving to have nearly no interest in the possibilities of its premise." 

A.A. Dowd of The A.V. Club gave the film a C+ and said, "As found-footage thrillers go, Project Almanac is perfectly watchable, but it never taps into the adolescent joy of its premise, the way Chronicle (film)|Chronicle did." 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 