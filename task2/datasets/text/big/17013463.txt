American Mormon
{{Infobox film
|  name     = American Mormon |
  image          = |
  director       = Jed Knudsen |
  producer       = Daryn Tufts   Jed Knudsen |
  writer         = Daryn Tufts Jed Knudsen |
  starring       = Daryn Tufts Jed Knudsen |
  producer       = Daryn Tufts   Jed Knudsen |
  distributor    = Excel Entertainment Group   (DVD) |
  released   = September 6, 2005 (USA) |
  runtime        = 35 min |
  language = English |
  }}

American Mormon is a 2005 comedic documentary (starring Daryn Tufts and Jed Knudsen) that takes look at the real life misconceptions (and understandings) concerning the faith and culture of The Church of Jesus Christ of Latter-day Saints.

==Background and plot==
 producers of LDS faith. Believing this to be a fascinating and worthwhile subject matter for a documentary, the two men began traveling across the United States, armed with nothing more than a digital video camera and microphone.

American Mormon was shot in  .
The goal was to approach subjects in a random and non-biased fashion. Tufts did not reveal to his subjects that he was LDS, nor did he tell them that they would be discussing the LDS faith, or Mormons, on camera. The idea was to get interviews that were not only honest, but spontaneously honest. The interviewees were not preached to. However, Tufts and Knudsen found themselves engaged with many people, off camera, who wanted to learn factual information about the LDS faith. After several weeks of traveling and shooting, Tufts and Knudsen began to compile the footage which would become a comedic cross-section of people from all over the country.   by Cody Clark, The Daily Herald, September 16, 2005 

==Reception==

American Mormon was released by Excel Entertainment Group in 2005. The film was unique in that it was both the first documentary and straight-to-DVD release of its kind to the LDS cinema market. American Mormon became an instant success with audiences, eventually grossing over forty times its production budget.   

==American Mormon in Europe==

Upon the success of the documentary, Excel partnered with Tufts and Knudsen to produce a sequel. For it, the two men traveled all over Western Europe, including Italy, France, Germany, and the United Kingdom. Their goal this time was to find out what people from different parts of the world knew about the Mormons. However, Tufts and Knudsen also decided to add a new element to this documentary. All over Europe, the two men also talked, on camera, with members of the LDS faith to document the experiences of people who are members of the LDS church outside of the United States.  This included an in-depth discussion with LDS members who lived in West and East Berlin during the fall of the Berlin Wall and a visit to the Gadfield Elm Chapel, the oldest LDS meetinghouse on earth, in Herefordshire, England.

==See also==
* Culture of The Church of Jesus Christ of Latter-day Saints
* Mormon folklore
* Mormon studies

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
*  

 
 
 
 