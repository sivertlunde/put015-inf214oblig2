Lagaan
 
 
 
{{Infobox film
| name           = Lagaan: Once Upon a Time in India
| image          = Lagaan.jpg
| caption        = Theatrical release poster
| director       = Ashutosh Gowariker
| producer       = Aamir Khan Mansoor Khan
| writer         = K.P. Saxena  (Hindi Dialogue)  Ashutosh Gowariker  (English Dialogue) 
| story          = Ashutosh Gowariker
| screenplay     = Ashutosh Gowarikar Abbas Tyrewala Sanjay Daima
| narrator       = Amitabh Bachchan
| starring       = Aamir Khan Gracy Singh Rachel Shelley Paul Blackthorne
| music          = A. R. Rahman
| cinematography = Anil Mehta
| editing        = Ballu Saluja
| studio         = Aamir Khan Productions
| distributor    = Sony Entertainment Television (India)
| released       =  
| runtime        = 224 minutes   
| country        = India Hindi
| budget         =   
| gross          =   (worldwide gross)   
}} epic sports sports Drama drama film written and directed by Ashutosh Gowariker. Aamir Khan, who was also the producer, stars with Gracy Singh in the lead roles; British actors Rachel Shelley and Paul Blackthorne play the supporting roles. Made on a then-unprecedented budget of  , the film was shot in an ancient village near Bhuj, India.
 Victorian period of Indias colonial British Raj. The story revolves around a small village whose inhabitants, oppressed by high taxes, find themselves in an extraordinary situation as an arrogant officer challenges them to a game of cricket as a wager to avoid the taxes. The narrative spins around this situation as the villagers face the arduous task of learning the alien game and playing for a result that will change their villages destiny.
 international film Indian film awards. It became the third Indian film to be nominated for the Academy Award for Best Foreign Language Film after Mother India (1957) and Salaam Bombay! (1988). It was one of the biggest box office hits of 2001. In 2010, the film was ranked No. 55 in Empire (film magazine)|Empire magazines "The 100 Best Films of World Cinema".  In 2011, it was listed in Time magazines special "The All-TIME 25 Best Sports Movies".

== Plot == western India) British Empire in India in 1893, Captain Andrew Russell (Paul Blackthorne), the commanding officer of the Champaner cantonment, has imposed high taxes ("lagaan") on people from the local villages which they are unable to pay due to a prolonged drought. Led by Bhuvan (Aamir Khan), the villagers beg Raja Puran Singh (Kulbhushan Kharbanda) to help them. He tells them that much to his regret, he is also bound by British law.
 cricket match. Bhuvan mocks the game and gets into a fight with one of the British officers. Taking an instant dislike to Bhuvan, Russell offers to cancel the taxes of the whole province for three years if the villagers can beat his men in a game of cricket. If the villagers lose, however, they will have to pay three times the amount of their normal taxes. Bhuvan accepts this wager on behalf of all the villages in the province, without their consent. When the other villagers find out about the bet, they are furious with Bhuvan. He argues that it is important for everyone to fight against British rule.

Bhuvan thus begins to prepare the villagers for the match. He initially finds only five people willing to join the team. He is aided in his efforts by Russells sister Elizabeth (Rachel Shelley) who feels that her brother has mistreated the people in the villages. As she teaches them the rules of the game, she falls in love with Bhuvan, much to the anguish of Gauri (Gracy Singh) who is also in love with him. After Bhuvan reassures Gauri of his feelings for her, the woodcutter Lakha (Yashpal Sharma) becomes enraged as he is also in love with Gauri. In an attempt to discredit Bhuvan, Lakha offers himself as a spy for Russell and joins the villagers team to destroy it. Eventually, as the villagers realise that winning equals freedom, and as a few of them are insulted by the British, they soon join the team. Still short one player, Bhuvan invites an Dalit|untouchable, Kachra (Aditya Lakhia), who can bowl spinners. The villagers, conditioned by long term prejudice against untouchables, refuse to play if Kachra joins the team. Bhuvan chastises the villagers, shaming them into accepting Kachra.

The second half of the film focuses on the match itself. On the first day, Russell wins the toss and elects to bat, giving the British officers a strong start. Bhuvan brings Kachra into the match only to find that Kachra has somehow lost his ability to spin the ball, because new cricket balls do not spin as well as worn-down ones (such as those the team have been practising with). In addition, as part of his agreement with Russell, Lakha deliberately drops many catches. During the evening, however, Elizabeth sees Lakha meeting with her brother. She races to the village and informs Bhuvan of Lakhas deception. Rather than allow the villagers to kill him, Bhuvan offers Lakha the chance to redeem himself.
 wickets by the lunch break. Kachra is brought back to bowl and, bowling with a now-worn ball, takes a hat-trick, which sparks the collapse of the British batting side. The villagers soon start their innings. Bhuvan and Deva (a Sikh, who has played cricket earlier when he was a British sepoy) give their team a solid start. Deva misses out on his half-century when a straight-drive from Bhuvan ricochets off the bowlers hand onto the stumps at the non-strikers end, where Deva is backing up too far. When Lakha comes on to bat, he is hit on the head by a bouncer, and falls onto his stumps. Other batsmen get out trying to score a boundary off each delivery. Ismail (Raj Zutshi), a good batsman, retires hurt as he is hit on the leg. The villagers team ends the day with four batsmen out of action with barely a third of the required runs on board.
 over with Kachra on strike. With one ball remaining and the team down 5 runs, Kachra knocks the ball a short distance, managing only a single. However, the umpire signals no ball and Bhuvan returns to bat, and swings extremely hard at the next ball. Captain Russell backpedals and catches the ball, gleefully believing that the British team has won, until he realises that he had actually caught the ball beyond the boundary which gives 6 runs, and the win, to Bhuvans team. Even as they celebrate the victory, the drought ends as a rainstorm erupts.

Bhuvans defeat of the British team leads to the disbanding of the humiliated cantonment. In addition, Russell is forced to pay the taxes for the whole province and is transferred to Central Africa. After realising that Bhuvan loves Gauri, Elizabeth returns to London. Heartbroken, she remains unmarried for the rest of her life. The narrator (Amitabh Bachchan) says that Bhuvan went on to marry Gauri with great pomp and show, but concludes by saying that despite the historic triumph, Bhuvans name became lost in the pages of history.

== Cast ==
* Aamir Khan as Bhuvan. Ashutosh first thought of having Shahrukh Khan and Abhishek Bachchan for the role of Bhuvan. After Bachchan chose to enter cinema with J. P. Duttas Refugee (2000 film)|Refugee (2000), Aamir was approached with the idea. 
* Gracy Singh as Gauri. Several actresses had offered to act in the film, but Aamir needed someone who matched the description of the character given in the script. After considering Sonali Bendre, Nandita Das, Rani Mukerji and Ameesha Patel for the role,    Ashutosh selected Gracy Singh for the female lead because he was convinced that she was a good actress and dancer and resembled actress Vyjayanthimala. Singh, a newcomer, devoted all her time to the film.   
* Rachel Shelley as Elizabeth Russell casting directors.  After Danielle and Ashutosh screen-tested many, Rachel Shelley and Paul Blackthorne were chosen for the prime roles. Overall, the film cast 15 foreign actors. 
* Suhasini Mulay as Yashodamai, Bhuvans mother
* Kulbhushan Kharbanda as Raja Puran Singh
* Rajendra Gupta as Mukhiya Ji Haji Nasruddin in the teleplay Mullah Nasiruddin and has given many memorable performances such as Mungerilal Ke Haseen Sapne. Yadav was selected based on his performance in Earth (1998 film)|Earth (1998). He had undergone an appendectomy operation in-between the filming schedule and returned to complete some of his scenes. 
* Rajesh Vivek as Guran (seamer), the fortune teller. Vivek was spotted by Ashutosh in the film Junoon (1978 film)|Junoon (1978). His liking for cricket helped him in his role.    Raj Zutshi as Ismail (batsman), the potter. Zutshis friendship with Aamir and association in several films brought him the role after the auditions.    Pradeep Rawat as Deva (all-rounder), a Sikh sepoy. Rawats association with Aamir in Sarfarosh (1999) brought him the role of Deva which was initially intended for Mukesh Rishi. Rawat claimed that it was the highest ever compensation he received in his career.    Chandrakanta and also in the film The Legend of Bhagat Singh (2002) as Chandra Shekhar Azad alongside Ajay Devgn who performed the role of Bhagat Singh.
* Daya Shankar Pandey as Goli (seamer), the man with the largest piece of land. Pandey, who preferred the role of Kachra, was known to Aamir and Ashutosh through previous films (Pehla Nasha (1993), Baazi (1995 film)|Baazi (1995) and Ghulam (film)|Ghulam (1998)). Pandey credited Ashutosh for his acting in the film, saying that Ashutosh and he would discuss the required emotions and expressions before shooting. 
* Shrivallabh Vyas as Ishwar (wicket-keeper), the vaidya (doctor) in the village and Gauris father. Yashpal Sharma as Lakha (batsman), the woodcutter. Sharma was chosen by Ashutosh after his portrayal in Samar (1999 film)|Samar (1999). He said it was a good experience working with Aamir and Ashutosh during the film. 
* Amin Hajee as Bagha (batsman), the mute drummer. Hajee earlier worked in a film with Ashutosh. The friendly association brought Ashutosh to him with the script, which he liked, and thereafter he successfully auditioned for his role. His knowledge of mute people and some assistance from a music band helped him better prepare for his role. Ashutosh, who believed that Amin was like Sylvester Stallone, would refer to him as Stallone during filming.   
* Aditya Lakhia as Kachra (spinner), the untouchable. Lakhias association with Ashutosh in Kabhi Haan Kabhi Naa (1993) and Pehla Nasha (1993) brought him this role. He read the book Everybody Loves a Good Drought by Palagummi Sainath|P. Sainath to better understand and portray his character.    Javed Khan as Ram Singh, Indian who works with British and helps Elizabeth in translating villagers language.
* A. K. Hangal as Shambu Kaka
* Amitabh Bachchan as Narrator

== Production ==

=== Origins ===
Director Ashutosh Gowariker has stated that it was almost impossible to make Lagaan. He first put forth the idea to Shahrukh Khan who was not able to do the film and recommended Aamir Khan. Gowariker then went to Aamir, who agreed to participate after hearing the detailed script. Even after securing Khan, Ashutosh had trouble finding a producer. Producers who showed interest in the script wanted budget cuts as well as script modifications. Eventually, Aamir agreed to Ashutoshs suggestion that hed produce the film.  Aamir corroborated this by saying that the faith he had in Ashutosh, the story and script of the film,    and the opportunity of starting his own production company inspired him to produce Lagaan.    He also said that by being a producer himself, he was able to give greater creative freedom to Ashutosh. He cited an example:

 "If the director tells the producer that he wants 50 camels, the latter will probably say, Why not 25? Cant you manage with 25 camels? Whereas, if he is telling me the same thing... I will not waste time asking him questions because I am also creatively aware why he needs them."  

Jhamu Sughand co-produced the film because he liked the emotional and patriotic story. 

=== Location, language and costumes === Kutch district, by May 1999, where the film was primarily shot.   

The script demanded a dry location: an agricultural village where it had not rained in several years. To depict the 1890s era, the crew also required a village which lacked electricity, communication and automobiles.  Kutch faced the same problems at that time and hence the village of Kanuria, located a few miles away from Bhuj, was chosen. During the filming of Lagaan, it did not rain at all in the region. However, a week after the shoot finished, it rained heavily bringing relief to Bhuj, which had had a lean monsoon the previous year.  The typical old Kutch hamlet was built by the local people four months before the arrival of the crew.  The 2001 Gujarat earthquake devastated this region and displaced many locals. The crew, including the English, contributed to their cause by donating  , with further contributions during the year. 
 Bhojpuri and Braj Bhasha) were penned by Hindi writer K. P. Saxena. 
 Oscar winner for Gandhi (film)|Gandhi, was the costume designer for the film. With a large number of extras, it was difficult for her to make enough costumes. She spent a lot of time researching to lend authenticity to the characters. 

=== Filming ===
Pre-planning for a year, including ten months for production issues and two months for his character, was tiring for Aamir. As a first-time producer, he obtained a crew of about 300 people for six months. Due to the lack of comfortable hotels in Bhuj, he hired a newly constructed apartment and furnished it completely for the crew. Security was set up and a special housekeeping team was brought to take care of the crews needs.  Most of the 19th century tools and equipment depicted in the film were lent to the crew by the local villagers. Initially, they did not want to part with their equipment, but after much coaxing, they gave in. They then travelled to different parts of the country to collect the musical instruments used in that day and era. 
 slipped disc and had to rest for 30 days. During this period, he had his bed next to the monitor and continued with his work. 

The filming schedule spanned the winter and summer, commencing in early January and finishing in mid-June. This was physically challenging for many, with the temperatures ranging from  .   The actors had to drink frequently and sit in the shade.   The schedule was strict. The day began at 6&nbsp;am, changing into costumes and getting onto the actors bus, which took them to the sets in Kanuria. The actors, including Aamir, all travelled on the same bus. If anyone missed it, it was up to them to reach the sets. One day, Aamir was late and missed the actors bus. That day, his wife Reena, the executive producer, reprimanded him for being late. She told him he had to set an example for the rest of the crew. "If he started coming late, how could she tell the others to come on time?"  While on the sets, the actors were given call sheets with the days timetable such as breakfast, hair styling, make-up, costumes, etc. 

== Release ==
Before its worldwide release, Aamir Khan kept a promise to screen the film to the locals of Bhuj.   Lagaan clashed with   at the box office. The film made it to the UK Top 10 after its commercial release.  It was the first Indian film to have a nationwide release in China  and had its dubbed version released in Italy.    With favourable reviews from the French press, Lagaan premiered in Paris on 26 June 2002 and continued to have an unprecedented nine weeks of screening with over 45,000 people watching.   It was released in the United States, France, Germany, Japan, Malaysia, Hong Kong, South Africa and the Middle East with respective vernacular subtitles.   The film took a cumulative of $2.5 million at the international box-office   and   at the domestic box-office. 
 Sun City, South Africa.  The Locarno International Film Festival authorities published the rules of cricket before the film was screened to a crowd which reportedly danced to its soundtrack in the aisles.  Lagaan was shown four times due to public demand as against the usual norm of showcasing films once at the festival.  It subsequently won the Prix du Public award at the festival.  After the films publicity in Locarno, the director, Ashutosh Gowarikar said that distributors from Switzerland, Italy, France, Netherlands, North Africa, Finland and Germany were wanting to purchase the distribution rights.  Special screenings were held in Russia, where people were keen to watch the film after its Oscar nomination. 

Apart from these screenings, it was shown at the Sundance Film Festival,  Cairo International Film Festival,  Stockholm International Film Festival,  Helsinki Film Festival  and the Toronto International Film Festival. 

== Reception ==
Lagaan was met with high critical acclaim. The film currently scores a 95% "Certified Fresh" approval rating on review aggregate site Rotten Tomatoes, based on 59 reviews, with an average rating of 7.9/10. The sites critical consensus is, "Lagaan is lavish, rousing entertainment in the old-fashioned tradition of Hollywood musicals."  Derek Elley of Variety (magazine)|Variety suggested that it "could be the trigger for Bollywoods long-awaited crossover to non-ethnic markets".  Somni Sengupta of The New York Times, described it as "a carnivalesque genre packed with romance, swordplay and improbable song-and-dance routines"  Roger Ebert gave three and half out of four stars and said, "Lagaan is an enormously entertaining movie, like nothing weve ever seen before, and yet completely familiar... At the same time, its a memory of the films we all grew up on, with clearly defined villains and heroes, a romantic triangle, and even a comic character who saves the day. Lagaan is a well-crafted, hugely entertaining epic that has the spice of a foreign culture."  Peter Bradshaw of The Guardian described the film as "a lavish epic, a gorgeous love story, and a rollicking adventure yarn. Larger than life and outrageously enjoyable, its got a dash of spaghetti western, a hint of Kurosawa, with a bracing shot of Kipling."  Kuljinder Singh of the BBC stated that "Lagaan is anything but standard Bollywood fodder, and is the first must-see of the Indian summer. A movie that will have you laughing and crying, but leaving with a smile."  Kevin Thomas of the Los Angeles Times argued that the film is "an affectionate homage to a popular genre that raises it to the level of an art film with fully drawn characters, a serious underlying theme, and a sophisticated style and point of view."    Sudish Kamath of The Hindu suggested that "the movie is not just a story. It is an experience. An experience of watching something that puts life into you, that puts a cheer on your face, however depressed you might be."  The Times of India wrote, "Lagaan has all the attractions of big-sounding A. R. Rahman songs, excellent performances by Aamir Khan... and a successful debut for pretty Gracy Singh. In addition, there is the celebrated David vs Goliath cricket match, which has audiences screaming and clapping."  Perhaps one of the most emphatic recommendations for the movie, coming 10 years later, is by John Nugent of the Trenton Independent, who wrote "a masterpiece ... and what better way to learn a bit about Indias colonial experience! History and great entertainment, all rolled in to one (albeit long) classic film." 
 

Lagaan was listed as number 14 on Channel 4s "50 Films to See Before you Die" and was the only Indian film to be listed. 

=== Awards ===
 
Aamir Khan and Gowariker went to Los Angeles to generate publicity for the Academy Awards. Khan said, "We just started showing it to whoever we could, even the hotel staff."    About Indias official entry to the 2002 Oscars, The Daily Telegraph wrote, "A Bollywood film that portrays the British in India as ruthless sadists and Mafia-style crooks has been chosen as Delhis official entry to the Academy Awards."    It added that the film was expected to win the nomination. 
 best foreign national film for Lagaan". 
 No Mans Land,  there was disappointment in India. Khan said, "Certainly we were disappointed. But the thing that really kept us in our spirits was that the entire country was behind us."    Filmmaker Mahesh Bhatt criticised the "American film industry" as "insular and the foreign category awards were given just for the sake of it."  Gowariker added that "Americans must learn to like our films". 
 National Film IIFA Awards.  Apart from these major awards, it also won awards at other national and international ceremonies.

== Soundtrack ==
{{Infobox album
| Name        = Lagaan: The Official Motion Picture Soundtrack
| Type        = soundtrack
| Artist      = A. R. Rahman
| Cover       = Lagaan_Soundtrack.JPG
| Released    =    Panchathan Record Inn, Chennai Feature film western classical music
| Length      = 41:58 Sony Music
| Producer    = A. R. Rahman
| Last album  = Love You Hamesha (2001)
| This album  = Lagaan (2001)
| Next album  = Star (2001 film)|Star (2001)
}}
The acclaimed soundtrack for the film was composed by A. R. Rahman with lyrics by Javed Akhtar. There are six songs and two instrumental pieces in this film. Rahman incorporated several music styles and genres to create the soundtrack.

Upon release, the soundtrack became highly popular, with all songs becoming huge hits. It also received high critical acclaim, with many reviewers stating it as a "Magnum Opus" by A. R. Rahman. It is described as one of Rahmans finest and most successful works to date. The rich background score of the film also got great critical acclaim and is said to have contributed immensely to the films success.

===Album reception===
 Screen India National Film best music, Best Male Best Lyrics Award for "Ghanan Ghanan" and "Radha Kaise Na Jale".  Lagaan also became the biggest audio hit of the year by topping the music charts and selling 3.5 million records within a year. 

{{Track listing
| extra_column = Singer(s)
| title1 = Ghanan Ghanan | extra1 = Udit Narayan, Sukhwinder Singh, Alka Yagnik, Shankar Mahadevan, Shaan (singer)|Shaan, Chorus | length1 = 6:11 Srinivas | length2 = 6:47
| title3 = Radha Kaise Na Jale | extra3 = Asha Bhosle, Udit Narayan, Vaishali Samant, Chorus | length3 = 5:34
| title4 = O Rey Chhori | extra4 = Udit Narayan, Alka Yagnik, Vasundhara Das | length4 = 5:59
| title5 = Chale Chalo | extra5 = A. R. Rahman, Srinivas | length5 = 6:40
| title6 = Waltz for a Romance (in A Major) | note6 = Instrumental | length6 = 2:29
| title7 = O Paalanhaare | extra7 = Lata Mangeshkar, Sadhana Sargam, Udit Narayan | length7 = 5:19
| title8 = Lagaan..... Once Upon a Time in India | extra8 = Anuradha Sriram | length8 = 4:12
}}

== Home media ==
There were two releases for the DVD. The first, as a 2-DVD set, was released on 27 May 2002 in limited regions. It contained subtitles in Arabic language|Arabic, English, Hebrew, Hindi, Turkish and several European languages. It is available in 16:9 Anamorphic widescreen, Dolby Digital 5.1 Surround, progressive 24 frame/s, widescreen and NTSC format. It carried an additional fifteen minutes of deleted scenes, filmographies and trailers. 

The second was released as anniversary edition three-disc DVD box after six years of the theatrical release. This also included Chale Chalo which was a documentary on the making of Lagaan, a curtain raiser on the making of the soundtrack, deleted scenes, trailers, along with other collectibles. |    After its release, it became the highest selling DVD in India beating Sholay (1975). 

== Merchandise == National Film Award-winning documentary, Chale Chalo – the lunacy of film making, 11 collector cards, a collectible Lagaan coin embossed with the character of Bhuvan, a 35&nbsp;mm CinemaScope filmstrip hand-cut from the films filmstrip were bundled with the film. 

A comic book, Lagaan: The Story, along with two colouring books, a mask book and a cricket board game were subsequently released to the commercial market. The comic book, available in English and Hindi, was targeted at children between the ages of six and 14. At the books launch, Aamir Khan said that they were keen to turn the film into a comic strip during the pre-production phase itself.  

In March 2002, a book titled The Spirit of Lagaan – The Extraordinary Story of the Creators of a Classic was published. It covers the making of the film, describing in detail the setbacks and obstacles that the crew faced while developing the film from concept to its release. 

== References ==
 

== Further reading ==
 
* Bhatkal, Satyajit (March 2002). The spirit of Lagaan. Mumbai: Popular Prakshan. pp.&nbsp;243. ISBN 81-7991-003-2.
*  
 

== External links ==
*  
*  
*   at Bollywood Hungama

 

 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 