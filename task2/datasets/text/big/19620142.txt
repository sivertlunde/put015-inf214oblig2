A Woman of Mystery
{{Infobox film
| name = A Woman of Mystery
| image = "A_Woman_of_Mystery"_(1958).jpg
| image size =
| alt =
| caption =
| director =Ernest Morris
| producer =Edward J. Danziger Harry Lee Danziger
| writer =Brian Clemens Eldon Howard
| narrator =
| starring = Dermot Walsh Hazel Court
| music =Edwin Astley Albert Elms James Wilson
| editing =Maurice Rootes
| studio = Danziger Productions
| distributor = United Artists Corporation(UK)
| released = April 1958 (UK)
| runtime =71 minutes
| country = United Kingdom
| language =English
| budget =
| gross =
| preceded by =
| followed by =
}}
A Woman of Mystery (1958 in film|1958) is a British crime film directed by Ernest Morris and starring Dermot Walsh, Hazel Court, and Ferdy Mayne.  The film features an early performance from Michael Caine in an uncredited role.

==Cast==
* Dermot Walsh as Ray Savage
* Hazel Court as Joy Grant
* Jennifer Jayne as Ruby Ames
* Ferdy Mayne as Andre
* Ernest Clark as Harvey
* Diana Chesney as Mrs. Bassett
* Paul Dickson as Winter
* Michael Caine Minor Role (uncredited)

==Plot==
Ray Savage, reporter for a sensational magazine in London is assigned over his objections to investigate a pretty blonde hat-check attendants suicide. Inquiring at Jane Hales place of work he learns that someone from her past had recognized her, and she was afraid. Finding a luggage label on her suitcase, he follows the lead to a dubious hotel where the manager tells him someone had taken a shot at her while she was staying there. From there he goes to her former place of work, then to the home of her friend at work who tells him that Miss Hale regularly called her mother, long distance. Following the number to a sanatorium where questioning Mrs. Hale yields nothing, but the cooperative administrator tells him the upscale neighborhood Jane was living before she went on the run.

Finally determining that she had been employed at an escort agency he is so excited as to ignore the indications that he is being followed until he returns home to be attacked by three men who try to duplicate Miss Hales supposed suicide by knocking him on the head and turning the gas on. Rescued by his girlfriend he grabs a gun and starts taking up housebreaking, until he determines the escort agency is cover for a counterfeiting operation. Then he shoots it out with the villains but feels frustrated because the mastermind behind the operation, the man who actually killed Jane has apparently gotten away unseen. Fortunately that doesnt last, because Janes new boyfriend comes after Ray to try to kill him and Janes friend who came to talk to Ray.

==External links==
*  

==References==
 

 
 
 
 
 
 
 

 
 