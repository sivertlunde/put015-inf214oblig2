The Patriot (1928 film)
 
{{Infobox film 
| name = The Patriot
| image = Image_patriot.jpg
| image_size = 
| caption = 
| director = Ernst Lubitsch
| producer = 
| writer = Hanns Kräly Ashley Dukes   Neil Hamilton  
| music = Max Bergunker
| cinematography = Bert Glennon
| editing = Ernst Lubitsch
| distributor = Paramount Pictures
| released =  
| runtime = 113 minutes
| country = United States
| language = Silent film English sequences
| budget = $1 million
}}
 Alfred Neumann, and The Patriot by Ashley Dukes. The Dukes play was performed on Broadway in January 1928.  A young John Gielgud made his Broadway debut in that play. The movie is a biographical story of emperor Paul I of Russia, starring Emil Jannings, Florence Vidor and Lewis Stone. 
 with the same title.

==Plot==
In 18th-Century Russia, the Czar, Paul, is surrounded by murderous plots and trusts only Count Pahlen. Pahlen wishes to protect his friend, the mad king, but because of the horror of the kings acts, he feels that he must remove him from the throne. Stefan, whipped by the czar for not having the correct number of buttons on his gaiters, joins with the count in the plot. The crown prince is horrified by their plans and warns his father, who, having no love for his son, places him under arrest for his foolish accusations. Pahlen uses his mistress, the Countess Ostermann, to lure the czar into the bedroom, where she tells the czar of the plot. The czar summons Pahlen, who reassures him of his loyalty. Later that night the count and Stefan enter his bedroom, and presently the czar is dead. But moments later Stefan turns a pistol on Pahlen. As the count lies dying on the floor, the countess appears and embraces Pahlen as he says, "I have been a bad friend and lover--but I have been a Patriot."

==Cast==
*  
* Florence Vidor - Countess Ostermann
* Lewis Stone - Count Pahlen
* Vera Voronina - Mademoiselle Lapoukhine Neil Hamilton - Crown Prince Alexander
* Harry Cording - Stefan

==Awards== Academy Award Best Actor Best Art Best Director Best Picture.    It was the only silent film nominated for Best Picture that year and the last to ever receive a Best Picture nomination until The Artist (film)|The Artist won for Best Picture in 2012.

==Status as a lost film== Best Picture Academy Award nominee for which no complete or near-complete copy has been found.

==See also==
*List of lost films

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 