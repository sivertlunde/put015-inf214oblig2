1981 (film)
 
{{Infobox film
| name           = 1981
| image          = 1981-film-ricardo-trogi.jpg
| image_size     = 215px
| alt            = 
| caption        = French release poster
| director       = Ricardo Trogi
| producer       = Nicole Robert
| writer         = Ricardo Trogi
| narrator       = Ricardo Trogi
| starring       = Jean-Carl Boucher
| music          = Frédéric Bégin
| cinematography = Steve Asselin
| editing        = Yvann Thibaudeau
| studio         = Go Films Alliance VivaFilm
| released       =  
| runtime        = 102 minutes
| country        = Canada
| language       = French
| budget         =  4.625 million ( 4.5 million)
}} Canadian French autobiographical about the youth years of the director as told by him during the film.

== Synopsis ==
Ricardo (played by Jean-Carl Boucher), 11 years, arrives to a school where he feels completely foreign. With the aim of integrating, he befriends a group of youth named "K-Way rouges" (the K-Way Reds) composed of Jérôme (Gabriel Maillé), Marchand (Dany Bouchard) and Plante (Léo Caron) from the school and tries to woo and impress the beautiful Anne Tremblay (played by Élizabeth Adam). In the process he has to lie his way all through.

==Cast==
* Jean-Carl Boucher as Ricardo Trogi
* Sandrine Bisson as Claudette Trogi, Ricardos mother
* Claudio Colangelo as Benito Trogi, Ricardos father
* Gabriel Maillé as Jérôme, the leader of the "K-Way rouges"
* Dany Bouchard as Marchand, member of the "K-Way rouges"
* Léo Caron as Plante, member of the "K-Way rouges"
* Marjolaine Lemieux as teacher Aline
* Élizabeth Adam as Anne Tremblay, the beautiful student Ricardo falls ion love with
* Lauriane Fortier as Anne Tremblays friend
* Pierre-Xavier Martel as the Nazi
* Rose Adam as Nadia Trogi, Ricardos sister
* Simone Chevalot as secretary at the school
* Pierre Mailloux as Mr. Dagenais
* Jean-Robert Bourdage as Maître Schneider 
* Mikahel Turcot-Beauchemin
* Ricardo Trogi as Narrator

==Sequel==
In August of 2014, a sequel was released called 1987 reuniting Jean Carl Boucher, Sandrine bisson, Claudio Colangelo, and Rose Adam to play their roles as the Trogis. The sequel tells the story of Ricardo at 17 years old graduating, trying to loose his virginity, open a club, and loads of other things. The film was released in December 2014 on DVD, Blu-ray, and VHS (for a limited time) as a kind of retro thing.

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 
 