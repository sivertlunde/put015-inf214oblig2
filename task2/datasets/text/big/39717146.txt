Life Partners (film)
{{Infobox film
| name           = Life Partners
| image          = Life Partners poster (2014).png
| alt            = 
| caption        = 
| director       = Susanna Fogel
| producer       =  
| writer         =  
| starring       =  
| music          = Eric D. Johnson
| cinematography = Brian Burgoyne
| editing        = Kiran Pallegadda
| studio         =  
| distributor    = Magnolia Pictures
| released       =   
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 directorial debut. The film stars Leighton Meester, Gillian Jacobs, Adam Brody, Greer Grammer, Gabourey Sidibe, and Julie White. The film premiered on April 18, 2014 at the Tribeca Film Festival in the Spotlight section.   The film was released on November 6, 2014 on demand platforms, then it will be released in select theaters on December 5, 2014. 

==Plot==
The comedy is about a co-dependent friendship between two very different girls—straight type A personality|type-A Paige (Jacobs) and lesbian slacker Sasha (Meester)—All is fine until Paige meets Tim (Brody), a young and charming doctor. As the relationship heats up, Paige and Sasha have to learn how to work Tim into their friendship. 

==Cast==
* Gillian Jacobs as Paige
* Leighton Meester as Sasha
* Adam Brody as Tim
* Greer Grammer as Mia
* Gabourey Sidibe as Jen
* Julie White as Deborah
* Abby Elliott as Vanessa
* Kate McKinnon as Trace
* Mark Feuerstein as Casey
* Monte Markham as Ken
* Rosanna DeSoto as woman in car
* Elizabeth Ho as Valerie
* Simone Bailly as Angelica
* Beth Dover as Jenn
* Zee James as Claire
* John Forest

==Production==
Joni Lefkowitz and Susanna Fogel both starred in the 2008 web series Joni & Susanna. The film is an adaptation of the play Life Partners, written by Lefkowitz and Fogel based on their friendship, which starred Amanda Walsh and Shannon Woodward. The play was created and premiered as part of Unscreened, which develops and produces world-premiere short plays by some of Hollywood’s fastest-rising writers and featuring multi-star casts, in 2011. Lefkowitz and Fogel first thought about Kristen Bell and Evan Rachel Wood to portray the role of Sasha, but when they both got pregnant, the role was given to Leighton Meester.  Magnolia Pictures acquired the US rights to the film on May 16, 2014. 

===Filming===
Principal photography began in April 2013 in Los Angeles.  Some scenes were shot at Long Beach, California during the Long Beach Lesbian & Gay Pride.    Some scenes were also filmed in Minneapolis, Minnesotas Uptown area. The Minneapolis skyline and a few Minneapolis landmarks are also shown in the film. Filming wrapped by June 2013. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 