The Man Who Changed His Mind
 
 
{{Infobox film
  | name = The Man Who Changed His Mind
  | image = Themanwholivedagain.jpg
  | caption = Film poster using alternative title Robert Stevenson
  | producer = Michael Balcon
  | writer = John L. Balderston Sidney Gilliat L. Du Garde Peach
  | starring = Boris Karloff Anna Lee
  | cinematography = Jack E. Cox
  | editing = R.E. Dearing Ben H. Hipkins Alfred Roome
  | studio = Gainsborough Pictures
  | distributor = Gaumont British Distributors
  | released = 11 September 1936 (London) 1 November 1936 (North America)
  | runtime = 66 min.
  | country   = United Kingdom English
  }} Robert Stevenson and was produced by Gainsborough Pictures. The film was also known as The Brainsnatcher or The Man Who Lived Again. Stephen Jacobs, Boris Karloff: More Than a Monster, Tomohawk Press 2011 p 198 

==Plot== Frank Cellier) with the personality of the crippled, caustic Clayton. With Lord Haslewoods wealth and prestige at his command, Laurience becomes an almost unstoppable mad scientist.
 John Loder) in an effort to seduce Clare, but finds it impossible to disguise his own strange physicality even in the body of another man.  Nor can he go without a cigarette in front of Clare although he is aware that young Dick Haslewood never smoked.  Unfortunately, before transferring his mind with that of Dick, Laurience strangled Clayton, who was inhabiting the body of Lord Haslewood, so that Dick, afterwards a prisoner in Lauriences own body, would be hanged for the murder of the man presumed to be his father.

Realizing the truth, Clare and her friend Dr. Gratton (Cecil Parker) return Lauriences mind to its proper body, but that body has been badly broken in a panicked fall out of a high window, taken while Dick Haslewood was in unwilling possession.  Admitting he has wasted an incredible invention on a selfish and murderous scheme, the shattered Laurience tells Clare he should never have meddled with the human soul. He takes his knowledge to the grave, having changed his mind for the last time.

==Cast==
* Boris Karloff as Dr. Laurience
* Anna Lee as Dr. Clare Wyatt John Loder as Dick Haslewood Frank Cellier as Lord Haslewood
* Donald Calthrop as Clayton
* Cecil Parker as Dr. Gratton
* Lyn Harding as Prof. Holloway
* Clive Morton as Journalist  
* D.J. Williams (actor)|D.J. Williams as Landlord

==See also==
* Boris Karloff filmography
==References==
 
==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 