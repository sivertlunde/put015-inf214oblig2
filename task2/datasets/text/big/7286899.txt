Dor (film)
 
{{Infobox film
| name        = Dor
| image       = dorfilm.jpg
| caption     = Theatrical release poster
| director    = Nagesh Kukunoor
| producer    = Elahe Hiptoola
| writer      = T.A. Razak
| starring    = Ayesha Takia Gul Panag Shreyas Talpade Girish Karnad Uttara Bhavkar Prateeksha Lonkar
| music       = Salim Merchant Sulaiman Merchant
| cinematography = Sudeep Chatterjee
| editing     = Sanjib Datta
| distributor = Sahara One Motion Pictures Percept Picture Company
| released    =  
| runtime     = 147 minutes
| country     = India
| language    = Hindi Urdu
}}
Dor ( ,  ,  ) is a 2006 Indian drama film written and directed by Nagesh Kukunoor that features Ayesha Takia, Gul Panag and Shreyas Talpade as the lead actors. It is a remake of the Malayalam film, Perumazhakkalam (2004) and was well received by the critics after its release on 22 September 2006. Dor, which also released on a DVD, garnered rave reviews from critics and film buffs.
 Hindi as the predominant language with a sporadic use of Urdu, Salim-Sulaiman composed the background score.

The story is about two women who come from different backgrounds and how fate brings them together. Meera (Ayesha Takia), a young woman who becomes a widow shortly after marriage, is trapped by tradition. Zeenat (Gul Panag), on the other hand, faces the daunting task of saving the life of her husband, who is on trial for murder. A Behrupiya|bahuroopiya (Shreyas Talpade) helps her reach Meera, who holds the "string" to Zeenats hope. The companionship that develops between Meera and Zeenat results in redemption for both.

== Plot ==
Zeenat (Gul Panag) is an independent Muslim woman living in Himachal Pradesh. She agrees to marry Amir Khan, her boyfriend, despite his parents reservations. After their marriage, her husband leaves for Saudi Arabia to begin employment.

Meera ( . Coincidentally, her husband, Shankar, is in Saudi Arabia, his new workplace. Meera finds it tough without her husband but they manage to stay in touch. Shankar regularly sends his wages home to support his family that includes his father, Randhir Singh (Girish Karnad), mother, Gowri Singh (Prateeksha Lonkar), his paternal grandmother (Uttara Baokar) and Meera. One day, Meera does not find the remittance from Shankar. Time passes and when there are neither any further remittances nor any correspondence from her husband, Meera gets worried. When frantic inquiries are made, she is devastated to know that Shankar was killed in a freak accident that was allegedly caused by his Muslim roommate.

When the news of Shankars death reaches the Singh haveli, everyone turns somber and the ceremonies towards rendering her into a widow emotionally drains Meera. Her vivaciousness and exuberance are thrust behind her black veil. The rest of the family vents their frustration of losing their only bread-winner on Meera, by blaming her for bringing bad-luck to their family. Meera, being her respectful self, bears the insults silently.

On the other side, Zeenat hears that her husband has been arrested for murdering his roommate in Saudi Arabia. She is convinced that it must have been an accident, but the Saudi law is unforgiving and Amir is scheduled to be executed. An Indian officer explains to her that Saudi law permits release of a criminal if the wife of the deceased forgives the guilt. Armed only with a photograph of Shankar and Amir, Zeenat sets out to find Meera. En route, she meets a Behrupiya (Shreyas Talpade). The bahuroopiya introduces himself and his profession as being multi-faceted and multi-talented in arts and mimicry. This profession requires him to keep visiting different places to perform "tricks" for his income. He turns out to be a petty con-man when he hoodwinks Zeenat and steals her belongings. However, when Zeenat is in trouble, he returns to rescue her with his artistic talent. He reveals his sympathetic view when Zeenat details her plight; he offers to help her with whatever knowledge he has. After making a lot of educated guesses, they reach Jodhpur. With local help, they identify the Singh haveli. When Zeenat directly requests the Singh family to pardon Amirs mistake, their anger drives her away. She feels that maybe talking through and befriending Meera might help her cause. The bahuroopiya takes leave of Zeenat and wishes her the best for her efforts.

 ), who leads Zeenat (Gul Panag) to Meera, is seen in one of his multi-faceted roles in the film.]]
At a temple, which Meera visits as her daily ritual, Zeenat makes her first contact. Zeenat is too afraid to tell Meera the truth about the situation, and she does not reveal who she is or why she has come. Over a few weeks, they become good friends and spend most of the time together. Their friendship brings out the missing part in each of their personalities. In the process, Zeenat realizes helplessness; this is totally new to her forthright way of thinking. Meera, on the other hand, gets a glimpse of freedom; this brings her out of the shell of the traditions in her haveli and gives her a new perspective on her life.

In the meanwhile, the Singh haveli is under a debt to Chopra (Nagesh Kukunoor), a local factory owner. When Randhir requests him for more time to repay it, he is given an offer — pardoning the debt in exchange for Meera. Though initially set back by Chopras offer, Randhir prefers the haveli to Meera.

When the news of the imminent death sentence arrives, Zeenat is compelled to tell the truth to Meera. Meera is shocked beyond belief to hear Zeenats words. The fact that her friendship was based on false pretenses is what hurts her more. She immediately refuses to sign the maafinama (statement of forgiveness). She makes it clear that she wants to hurt her husbands murderer, even if it was an accident, because of how much she is hurting in her new, veiled life. She leaves for the haveli. Zeenat, initially, is deeply hurt from Meeras decision but eventually accepts it as fate and decides to leave for her hometown. Meera later has a change of heart, perhaps because of her disillusionment at Randhirs willingness to "sell" her to Chopra. She gets encouragement from her paternal grandmother and hurries towards the railway station, where she meets Zeenat and gives her the signed statement of forgiveness. Zeenat extends her hand from the train and Meera grabs it and climbs aboard, presumably running away from the only life she has ever known, as the train speeds up into the distance.

== Cast ==
* Ayesha Takia as Meera, one of the two protagonists. Coming from an orthodox Rajput family, she has to descend into a life devoid of joy.
* Gul Panag as Zeenat, the other protagonist. A gutsy and determined wife who hopes to save her husbands life and this leads her to Meera.
* Shreyas Talpade as Bahuroopiya, the multi-faceted personality. He plays a supportive role to Zeenat by helping her to find Meera.
* Girish Karnad as Randhir Singh, father-in-law of Meera. Quite orthodox in their customs and traditions, he turns tough on Meera after the death of the only bread-winner of their house, his son.
* Nagesh Kukunoor as Chopra, a businessman. Comes up with a proposal to forgive the debt of Randhir Singhs ancestral house at the expense of Meera.
* Prateeksha Lonkar as Gowri Singh, Randhir Singhs wife. On the pretext of tradition and customs, she exercises restraint on Meeras freedom and choices.
* Uttara Baokar as Dadima, Meeras grandmother-in-law. Her grumpy nature towards Meera transforms into empathy after Shankar, Meeras husband, is murdered. She plays a significant role in Meeras redemption from oppression.

== Production ==
=== Pre-production ===
The story of Dor began when Nagesh Kukunoor was attending the International Film Festival of India in November 2005. During the festival, he had hinted to a journalist that he began writing his next script.  He confirmed this in another interview that after watching Perumazhakkalam (2005),  whose story is based on a newspaper article, at the Film Festival, he decided to make his own version of the film. After purchasing the story rights from Kamal, director of Perumazhakkalam, he wanted to remake it in a different way. Through his story, he wanted to emphasize the protagonists ordeal while in isolation in the form of a visual drama.    However, acknowledgments to Perumazhakkalam or its makers were not provided in the credits. 

Kukunoor announced the news about film making in early March 2006 and suggested of its release in August 2006.  He conceptualized the film in thirds: the first and third for the lead characters and the second for the supporting one.    However, Kukunoor said that all his films, at the core, have "the human element — the simplicity of the basic emotions that bind us all."   

Since the backdrop of the film was to be Rajasthan, he had been there and did the necessary research so as to better portray the place and characters. 

The CEO of Sahara One Motion Pictures, who produced Kukunoors Iqbal (film)|Iqbal (2005), while speaking about their motivation to continue the association with Kukunoor said, "we all look forward to Kukunoors films mainly because the subject and content is so out of the ordinary and captivating." 

=== Locations and casting ===
 
Kukunoor had not visited Rajasthan or Himachal Pradesh before, but he felt that he would find great locations there. Salooni in Chamba district is considered for shooting in Himachal Pradesh. In spite of this geographical inexperience, all went as per the plans while shooting in Rajasthan. However, minor modifications in the script were allowed for the surroundings.  Though Panag had been in Rajasthan, she felt the scorching summer heat unbearable at times. She liked the only continuous shooting schedule and said that it provided consistency in look and performance.    In Himachal, it was slightly different when the crew had hard time framing the mountains correctly. On seeing the captured frames, they shifted all the interior shots to outside. 

Filming was completed in 37 days in several locations of Rajasthan.    Since most of the old palaces in Rajasthan have been converted into hotels, the crew stayed at a palace resort called Manwar.    They resided at a palace resort in Pokhran, while filming was done at Mehrangarh Fort in Jodhpur.  Kukunoor, after completion of the shooting, said that it was his most challenging film as it involved a real-life story. 
 voice for Panag in the film. 
 rakhi symbolically. 

When Kukunoor was thinking of a character full of disguises, he first came up with an old man. When the thought of a scene with the three major characters dancing in the dunes came up, he realized that an older character could not realistically dance in the desert. He immediately felt that Shreyas Talpade, whom he knew of knowing mimicry, could be used for bahuroopiya.  Kukunoor, who worked previously with Girish Karnad, said that he was the strength for the film and added that he found him a good actor and a good human being. 

Talpade, who previously featured as the title character of Kukunoors Iqbal (film)|Iqbal (2005), observed the bahuroopiyas in Rajasthan and incorporated their dialect, accent, and their body language for his character. About Kukunoor, he said that, "though one tends to go overboard as an actor most of the times, the way he handles the character as a director, Nagesh makes you feel that it is very simple for you to delineate the character."  Through his character of a bahuroopiya, Talpade was required to perform mimicry. While he was good at mimicry in college, it was during the filming of Iqbal, Kukunoor noted this talent of his and thus Dor came into Talpades hands. About his co-actors, he said that it was refreshing to work with Panag and Takia despite the scorching heat of  .   

According to the DVD extras, Nagesh Kukunoor first wrote the screenplay in English, then had it translated to Hindi. 

=== Filming and music ===


About the background score, Kukunoor said that "to put soaring music to give it a larger-than-life film image was pretty difficult".  Despite this worry, with Salim-Sulaiman composing the music, he was happy with the way it was composed according to the sequence. Karsan Saquria, Sunidhi Chauhan and a Pakistani singer, Shafqat Amanat Ali Khan were among those who sang the songs.  While releasing the soundtrack, Kukunoor said that Salim-Sulaiman and he tried make songs that "stood out from the clutter and something that was different from the item numbers or the boring love ballads."   

== Release and reception ==
Dor accumulated about Indian rupee|Rs. 38 million in India at the end of 2006. 

=== Reviews === The Telegraph said in its review, "When you first watch Nagesh Kukunoors impressively shot Dor, you want to simply applaud it as good cinema. Whether its Ayeshas award-worthy performance or Shreyas amusing moments, the credit marks pile up in favor of the director who has made engaging cinema."  The Times of India said:

 Dor makes a strong feminist statement without being strident or shouting slogans. And all along, the riveting friendship between the two polarized women and the events that bring them together, keep tugging at your heart. Shreyas Talpade proves that (his previous film), Iqbal was no accident and Gul Panag gives one of the most convincing portrayals of her career. But, it is Ayesha Takia who simply blows your breath away. Her journey from joyous subjugation — where she is content to dance before her husband and be at the beck and call of her in-laws — to silent emancipation is absolutely endearing.  

About the technical department, The Hindu said:
: "There is this one scene when the director cuts from Zeenat (Gul Panag) trying to fix her house literally from the outside, perched on a ladder, with a hammer in hand, to Meera (Ayesha Takia) inside the house and behind a veil. The play of such visual metaphors throughout gives the film a world-class feel, the kind of stuff you usually see in Iranian cinema." 
Another review from The Telegraph says:
: "Sudeep Chatterjees cinematography is excellent. He composes and constructs stylised but simple images which dont scream for attention, but unassumingly add up to create the films striking overall visual design." 

While writing about the plot and the picturization, Rediff.com concluded:
: "The script is engaging; the dialogues entertaining, witty and yet quite profound. Every scene seems to have been well etched out. The character sketches are strong and the characters are inspiring but not patronizing. The script, the story and the characters take the spotlight and the sets, though picturesque add to the plot instead of overshadowing it." 
Taran Adarsh, while writing about the chances of its commercial success, said, "Dor is a well-made film that caters to those with an appetite for qualitative cinema. Awards and glowing critical acclaim, yes, it has the power to win it. But, box-office rewards and a mandate from the aam junta (common man) will elude it. The lethargic pacing will also go against it." 

=== Reception and awards ===
As per the reviews, the film could not capture much appeal at the theaters. In Kolkata, theater officials withdrew Dor  from screening one week after its theatrical release. The reason cited for this withdrawal was due to many simultaneous releases such as the films Woh Lamhe and Snakes on a Plane. However, commercial success notwithstanding for these films, Dor was brought back to the screens.  Following this, Dor was screened at the annual Indo-American Arts Council Film Festival  and the Atlanta Indo-American Film Festival. 

Along with nominations for cinematography, Dialogue (fiction)|dialogues, lyrics and supporting actor (for Shreyas Talpade), the film won the critics award for Ayesha Takia and Gul Panag at the 2007 Zee Cine Awards.  At the annual Star Screen Awards, Talpade and Takia won the best actor in a comic role and critics choice for best actress awards respectively along with other nominations.   At another such awards ceremony, Takia and Panag won awards for their performances.  Takia further won the best actress award at the Bengal Film Journalists Association Awards. 

== Home media ==
=== DVD ===
The DVD version of the film was released on 20 October 2006.      Dolby Digital progressive 24 Frame rate|FPS, widescreen and NTSC format. With a runtime of 147 minutes, the DVD has a provision for English subtitles.  However, some versions of the film jacket list the runtime as 63 minutes.

=== Soundtrack ===
The soundtrack, which was composed by Salim-Sulaiman and the lyrics by Mir Ali Husain, was released on 26 August 2006 with a typical and traditional Rajasthani flavor. 

One review about the soundtrack said that, "this is no ordinary album and will be preferred by musical elites. Infused with classical music and Rajasthani folk music, it is a good quality album coming out of Salim-Sulaiman. But the shortcoming comes in the form that this is not the kind of music thatll please every ear."  Another review, in a similar tone, said that the album "works only for those who are either followers of classical music or enjoy hearing songs with a Rajasthani folk music base. There is no doubt that composers good quality throughout, but overall the album caters only to a niche audience." 

{{Track listing
| extra_column = Singer(s)
| title1 = Allah Hoo Allah Ho | extra1 = Salim Merchant | length1 = 04:50
| title2 = Expression of Love | extra2 = Trilok Gurtu | length2 = 05:44
| title3 = Imaan Ka Asar | extra3 = Sunidhi Chauhan, Shreya Ghoshal | length3 = 04:12
| title4 = Kesariya Balam | extra4 = Karsan Sagathiya | length4 = 06:05
| title5 = Piya Ghar Aaya | extra5 = Pratichee | length5 = 05:39
| title6 = Theme Music | extra6 = Salim Merchant | length6 = 01:48
| title7 = Yeh Honsla | extra7 = Shafqat Amanat Ali Khan | length7 = 04:39
| title8 = Yeh Honsla | note8 = Sad | extra8 = Shafqat Amanat Ali Khan | length8 = 01:33
}}

== See also ==
 
* Perumazhakkalam

== References ==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 