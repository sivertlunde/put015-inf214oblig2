Touched By Evil
{{Infobox film
| name           = Touched By Evil
| image          = 
| image size     = 
| caption        = 
| director       = James A. Contner
| producer       = Harry R. Sherman
| writer         = Phil Penningroth
| narrator       =
| starring       = Paula Abdul Adrian Pasdar Susan Ruttan
| music          = Dan Slider
| cinematography = Robert Primes
| editing        = Thomas Fries	
| studio         = ABC
| released       = January 12, 1997
| runtime        = 96 minutes US
| English
}} 1997 television American singer/choreographer, Paula Abdul. 
  Allmovie.com 

==Plot==
Ellen Collier (Paula Abdul), a well-put-together businesswoman finds comfort and security in her relationship with her loving and supporting new boyfriend, car salesman Jerry Braskin (Adrian Pasdar), after being savagely attacked by a serial rapist. But other rapes occur just when Ellen thought it was safe for her slowly break free from her self-imposed shell, which eventually causes her to believe that Jerry is hiding a very dark secret. Eventually Ellen is confronted with rock-solid evidence that her boyfriend Jerry is the very same man who raped her, and she has to save herself before its too late.

==Cast==
*Paula Abdul as Ellen Collier
*Adrian Pasdar as Jerry Braskin
*Susan Ruttan as Madge Jaynes
*Charlayne Woodard as Det. Duvall Tracy Nelson as Clara Devlin
*Dale Wilson as Ronald Myers

==References==
 

==External links==
* 

 
 
 
 
 