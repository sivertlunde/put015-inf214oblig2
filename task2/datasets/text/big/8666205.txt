Cardcaptor Sakura Movie 2: The Sealed Card
 
{{Infobox film
| name           = Cardcaptor Sakura Movie 2:  The Sealed Card
| film name = {{Film name| kanji          = 劇場版カードキャプターさくら 封印されたカード
| romaji         = Gekijōban Kādokyaputā Sakura Fūin Sareta Kādo}}
| image          = CCSMovie2.jpg
| caption        = DVD cover
| director       = Morio Asaka
| producer       = Kazuhiko Ikeguchi Kouichi Tsurunari Shinji Komori Tatsuya Ono Tsuyoshi Yoshida
| writer         = Nanase Ōkawa
| starring       = Sakura Tange Aya Hisakawa Masaya Onosaka Motoko Kumai Junko Iwao Tomokazu Seki Megumi Ogata Yukana Nogami Nozomu Sasaki Maaya Sakamoto
| music          = Takayuki Negishi
| cinematography =
| editing        = Harutoshi Ogata Madhouse
| distributor    = Bandai Visual Shochiku
| released       =  
| runtime        = 80 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          =
}}
  is the second anime film, sequel, and the finale to the anime television series adaptation of Clamp (manga artists)|Clamps Cardcaptor Sakura. The animation was produced by Madhouse (company)|Madhouse, while it was directed by Morio Asaka and written by Nanase Ōkawa (Clamps head writer). It won the Feature Film Award at the 2000 Animation Kobe.  The film was released on DVD in 2003.

==Plot==
Eriol Hiiragizawas house is demolished to make way for a new amusement park in Tomoeda, activating a Clow Card, The Nothing, hidden underneath the house. After the park is built, she hides in its clock tower and begins secretly stealing the other cards from their mistress Sakura Kinomoto. Sakura faces her own challenges, having the leading role in a play her school is putting on as part of Tomoedas annual festival, and her own feelings towards her friend Syaoran Li, who confessed to her before returning to Hong Kong. Sakura and her best friend Tomoyo Daidoji visit the amusement park, where Sakura senses a magical aura. Running into the park, she bumps into Syaoran and Meiling Li who returned for a visit, though Tomoyo and Meiling planned it to get Sakura to confess to Syaoran.
 Kero and Toya and his friend Yukito Tsukishiro. The next couple of days involve Sakura and her friends rehearsing the play and Sakura repeatedly attempts to confess to Syaoran but is interrupted. They also notice that things in the city are disappearing including a bridge in the local park. Sakura, Syaoran, Tomoyo and Meiling spend a day at the amusement park, with Sakura and Syaoran ending up on a ferris wheel together where Sakura tries to confess again. However, both witness one of the Clow Cards vanishing and chase its aura to a hall of mirrors where they encounter the Nothing who steals several more cards.

Sakura and Kero are contacted by Eriol from England, who explains the Nothing was created to counter the positive magic of the Clow Cards with her own negative magic. The Nothing was released due to Sakura changing the cards power from Clow Reeds to her own, and every time the Nothing steals a card, her power erases part of Tomoeda. Eriol warns Sakura that when she seals the card, her greatest feeling at the time, namely her love for Syaoran, will be erased as a payment. Sakura informs Syaoran, but he concludes the sacrifice is their only option. Sakura runs off in tears but is consoled by List of Cardcaptor Sakura characters#Yue|Yue, the second guardian of the cards and Yukitos true form. During another rehearsal, the Nothing attacks the school, injuring Takashi Yamazaki who was to play the lead role opposite Sakura, so Syaoran steps in.

During the play, the Nothings power spreads and erases many of Sakuras loved ones including Tomoyo, Meiling, and her family. Sakura, Syaoran, Kero, and Yue go to the amusement park and battle the Nothing who erases Kero and Yue. Syaoran attacks her on the ferris wheel but is caught in her destructive spheres and vanishes. Sakura pursues the Nothing to the clock tower, where she is stripped of her last cards apart from an unnamed card she created with her own magic after Syaoran left for Hong Kong. Sakura learns the Nothing collected the cards so she would be alone anymore, but Sakura promises that she will never be isolated again and seals the Nothing. However, the required toll instead comes from Syaoran who tells Sakura he will fall in love with her all over again.

The Nothing and the nameless card fuse into one, becoming the Hope Card and avert the toll, though Sakura is not aware of this and she finally confesses to Syaoran, who replies that he feels the same. The Nothings powers are reversed, reviving Tomoeda and its inhabitants. Sakura springs across the reforming clock tower to Syaorans arms, together at last.

==Soundtrack==
Containing 32 tracks of background instrumental songs and vocal tracks used within the movie, Cardcaptor Sakura Movie 2: The Sealed Card Original Soundtrack was released in Japan on August 2, 2000 by Victor Entertainment.

==Bonus art== Madhouse also brought out several pieces of high quality artwork, postcards and illustrated poster art (including the final scene bonus poster). The Special Edition DVD featured a separate art gallery section along with a booklet and pencil boards. CLAMP also brought out an artbook titled "The complete book of the animated movie Cardcaptor Sakura - The Sealed Card" in October 2000 which also featured interviews with CLAMP and the voice actors for the Cardcaptor Sakura series.

==Reception==
  special was the best extra.  Allen Divers of Anime News Network noted that the English dub was closer to the Japanese than previous English dubs, even with Sakuras trademark expression "Hoe!", and that the voice actors did a great job of matching the emotions of the original Japanese ones. He felt the movie was a satisfying conclusion to the series.  THEM Anime felt that the plot of the film was more substantial than the plot for the first film, and enjoyed the two storylines of Sakuras emotions and the final card. 

==References==
 

==External links== Madhouse  
* 
*  

 
 
 

 
 
 
 