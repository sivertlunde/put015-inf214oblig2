Eyes of Laura Mars
{{Infobox film
| name = Eyes of Laura Mars
| image = Eyesoflauramars.jpg
| alt = 
| caption = Theatrical release poster
| director = Irvin Kershner
| producer = Jack H. Harris Jon Peters Laura Ziskin
| writer = John Carpenter David Zelag Goodman H.B. Gilmour (novelization)
| starring = Faye Dunaway Tommy Lee Jones Brad Dourif René Auberjonois Raúl Juliá
| music = 
| cinematography = Victor J. Kemper Michael Kahn
| distributor = Columbia Pictures
| released =  
| runtime = 104 minutes
| country = 
| language = English
| budget = $7 million
| gross = $20 million Richard Nowell, Blood Money: A History of the First Teen Slasher Film Cycle Continuum, 2011 p 257 
}} thriller film spec script titled Eyes, written by John Carpenter, and would become Carpenters first major studio film of his career. The late H. B. Gilmour would later write the novelization, one of at least six that marked her literary career.

Producer Jon Peters, who was dating Barbra Streisand at the time, bought the screenplay as a starring vehicle for the actress, but Streisand eventually decided not to take the role because of "the kinky nature of the story," as Peters later explained. As a result, the role went to Dunaway, who had just won an Oscar for her performance in Network (film)|Network.

Streisand nevertheless felt that "Prisoner", the torch song from the film, would be a good power ballad vehicle for her. She sang it on the soundtrack and garnered a moderate hit as a result (the record peaked at number 21 on the Billboard Hot 100|Billboard Hot 100).

Eyes of Laura Mars is said to be an example of an American version of the giallo genre. The film is also noted for its use of red herrings and its twist ending.

==Plot==
Laura Mars (Faye Dunaway) is a glamorous fashion photographer who specializes in stylized violence. Amid controversy over whether her photographs glorify violence and are demeaning to women, Laura begins seeing, in first person through the eyes of the killer, real-time visions of the murders of her friends and colleagues.

John Neville (Tommy Lee Jones), the lieutenant in charge of the case, shows Laura unpublished police photographs of unsolved murders that very closely mirror Lauras fashion shoots. Lauras visions continue, including visions of the killer stalking her and continuing to murder those around her. Meanwhile, Laura and Neville fall in love. The murders continue as Lauras various colleagues, acquaintances and past romantic interests come in and out of focus as potential suspects or victims, until a final confrontation between Laura and the killer occurs.

At her apartment, Laura is affected by one last vision of the killer, who has now come for her. The killer attempts to break in through her front door, but Laura deadbolts it before he/she can enter. Upon hearing her distress, Neville (who had been on his way to meet her) breaks through her balcony window. He proceeds to tell Laura they have caught the killer, a troubled colleague of hers named Tommy, and begins an elaborate explanation of Tommys motivations and back story. Knowing Tommy well, Laura recognizes this as a lie and that Neville himself is the killer. As Neville details more of his own story, it is implied that he may have multiple personalities. Because of this, and his love for her, he cannot bring himself to murder her and instead asks that she end his life. She shoots him dead, calling the police as we close in on her eyes—the eyes of Laura Mars.

==Cast==
* Faye Dunaway as Laura Mars
* Tommy Lee Jones as Lieutenant John Neville
* Brad Dourif as Tommy Ludlow
* Rene Auberjonois as Donald Phelps
* Raúl Juliá as Michael Reisler
* Frank Adonis as Sal Volpe
* Lisa Taylor as Michelle
* Darlanne Fluegel as Lulu
* Rose Gregorio as Elaine Cassel
* Bill Boggs as Himself
* Steve Marachuk as Robert
* Meg Mundy as Doris Spenser
* Marilyn Meyers as Sheila Weissman

==Production==
Production began on October 17, 1977. The film was shot entirely in New York and New Jersey. A memorable sequence where the Laura Mars character photographs a group of models against a backdrop of two burning cars was filmed over four days at New Yorks Columbus Circle. The $7 million production wrapped on January 9, 1978, after 56 days of filming. It was reported that Peters and Dunaway had a tense relationship while making the film, and that Streisand visited the set on a few occasions.

==Reception==
The movie received a broadly positive review in The New York Times, in which Janet Maslin called the ending of the film "dumb," but otherwise liked it. She wrote of it: "Its the cleverness of Eyes of Laura Mars that counts, cleverness that manifests itself in superlative casting, drily controlled direction from Irvin Kershner, and spectacular settings that turn New York into the kind of eerie, lavish dreamland that could exist only in the idle noodlings of the very, very hip." 

Roger Ebert was less enthusiastic and pointed out its clichéd "woman in trouble" plot. 

As of May 2010, the film had only a 47% fresh rating on the Rotten Tomatoes movie review website. 

On its release, the film received mixed critical reviews, but it was a box office hit, earning $20 million from a $7 million budget.

  because he was very impressed after seeing an early rough cut of the film. 

==Soundtrack==
Eyes of Laura Mars (Music from the Original Motion Picture Soundtrack) was released by Columbia Records (PS 35487) in July 1978. It was produced by Gary Klein with executive producers Jon Peters and Charles Koppelman.

Mark Iskowitz of The Barbra Streisand Music Guide wrote: "The side one Prisoner track is actually identical to the single and Greatest Hits Volume 2 version. The side two reprise version does contain instrumentation from the film score at the beginning and during the first sections of the song, which is featured in its entirety. Track 3 opens with Barbra singing the first four lines from Prisoner with a sparse, spooky film score backing."

The Eyes of Laura Mars LP is out of print; it was never released on CD.

===Track listing===

{{Track listing
| headline        = Side One
| writing_credits = yes
| extra_column    = Artist
| title1          = Prisoner (Love Theme from Eyes of Laura Mars)
| writer1         = Karen Lawrence & John Desautels 
| extra1          = Barbra Streisand
| length1         = 3:53
| title2          = Lauras Nightmare
| writer2         = Artie Kane
| extra2          = Artie Kane
| length2         = 2:06
| title3          = Burn
| writer3         = George Michalski & Nikki Oosterveen
| extra3          = Michalski & Oosterveen
| length3         = 4:16
| title4          = Elaine
| writer4         = Artie Kane
| extra4          = Artie Kane
| length4         = 1:25
| title5          = Laura & Neville (Instrumental)
| writer5         = Artie Kane
| extra5          = Artie Kane
| length5         = 2:33
| title6          = Medley: Native New Yorker (Shake, Shake, Shake) Shake Your Booty Prisoner (Disco Instrumental) Richard Finch Artie Kane Odyssey KC & The Sunshine Band Artie Kane
| length6         = 4:33
}}
{{Track listing
| collapsed       =
| headline        = Side Two
| writing_credits = yes
| extra_column    = Artist
| title1          = Laura – Warehouse
| writer1         = Artie Kane
| extra1          = Artie Kane
| length1         = 1:11
| title2          = Lets All Chant
| writer2         = Michael Zager & Alvin Fields Michael Zager Band
| length2         = 4:05
| title3          = Laura & Neville (Dialogue & Vocal)
| writer3         = Artie Kane
| extra3          = Artie Kane
| length3         = 2:33
| title4          = Lulu & Michelle
| writer4         = Artie Kane
| extra4          = Artie Kane
| length4         = 3:06
| title5          = Love & Pity
| writer5         = Artie Kane
| extra5          = Artie Kane
| length5         = 4:10
| title6          = Love Theme from Eyes of Laura Mars (Prisoner) – Reprise
| writer6         = Karen Lawrence, John Desautels 
| extra6          = Barbra Streisand
| length6         = 3:56
}}

==In popular culture==
* In the Malcolm in the Middle episode "Lois Battles Jamie", a flashback scene shows that Francis broke Hals Laserdisc copy of the film.
* Tori Amos refers to Laura Mars in her song "Gold Dust", off the 2002-album "Scarlets Walk".
* A parody of the film titled Eyes of Lurid Mess was published in Mad Magazine. It was illustrated by Angelo Torres and written by Larry Siegel in regular issue #206, April 1979.  , MAD #206 April 1979.  Ladyhawke makes several visual references to the film.

==References==
 

==External links==
*  
*  
*  
*  

 
 

 

 
 
 
 
   
   
 
 
 
 
 
 
 
 
 
 
 
 