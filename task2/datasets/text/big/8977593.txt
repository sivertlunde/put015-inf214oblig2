American Flyers
 
{{Infobox film
| name = American Flyers
| image = American flyers.jpg
| caption = Theatrical release poster
| director = John Badham
| writer = Steve Tesich
| starring = {{Plainlist|
* Kevin Costner David Grant
* Rae Dawn Chong
* Alexandra Paul
* Janice Rule
}}
| producer = Paula Weinstein   Gareth Wigan
| distributor = Warner Bros.
| released = August 16, 1985
| runtime = 113 minutes
| country = United States
| language = English
| music = Greg Mathieson   Lee Ritenour
| cinematography = Donald Peterman
| editing = Jeff Jones   Frank Morriss   Dallas Puett
| budget = $8.5 million
| gross = $1,420,355
}}

American Flyers is a 1985 film starring Kevin Costner, David Marshall Grant, Rae Dawn Chong, Alexandra Paul and Janice Rule about bicycle racing. 

It was directed by John Badham and written by Steve Tesich (who had previously won an Oscar for Best Original Screenplay for another film featuring a bicycle race, 1979s Breaking Away).

==Plot== Sports physician Marcus Sommers (Costner) persuades his younger brother David (Grant) to train with him for a three-day bicycle race across the Rocky Mountains known as "The Hell of the West." 

There is a history of cerebral aneurysm in the Sommers family, which killed their father. Their mother (Rule) is concerned that the condition may now be affecting David as well. Marcus convinces David to undergo testing at his sports medicine center. Following the test, David overhears a conversation in which Marcus says that he does not want to worry David about something. David assumes that he does have an aneurysm.

Marcus convinces David to embark on a cross-country journey to the bicycle race in Colorado, along with Marcus girlfriend Sarah (Chong). Along the way, they pick up a beautiful, hippie hitchhiker named Becky (Paul).

Marcus realises that David needs to train to be prepared for the race. The brothers engage in fun training, such as practising sprinting against an angry vicious dog. They even go so far as have an impromptu bikes vs. horses race. The group also has run-ins with some of Marcuss old cycling rivals, including Sarahs ex-husband.

In the three-stage race in the Rockies, with mountain and prairie backdrops, the brothers compete against the worlds top cyclists on dangerous roads at breakneck speeds. David crashes his bike at the end of the first stage and barely qualifies for the next round. Marcus works out how much time David needs to regain and develops a strategy for the second leg of the race.  During the race, it turns out that it is Marcus, intending to hide his condition from his brother, who eventually suffers an aneurysm during the second stage and drops out. 

David faces a dilemma: to quit and look after his brother, or continue to defy the odds and win the race. David decides to quit the race but is encouraged by Marcus to keep in the race. David sprints to an early lead, which the competitors put down to youthful exuberance. But after a few miles they realize that David is able to maintain the savage pace. As they enter the mountain stages one of the riders approaches David to throw the race and settle for second. He pushes on and races for the finish. As he hits the line the clock ticks down and there has to be a gap of 11 seconds between him and the second placed rider to be assured of the win. As the clock ticks down David scrapes the win and runs to celebrate with Marcus.

==Cast==
* Kevin Costner as Marcus Sommers
* Rae Dawn Chong as Sarah
* Alexandra Paul as Becky
* David Marshall Grant as David Sommers
* Janice Rule as Mrs. Sommers
* Jennifer Grey as Leslie Robert Townsend as Jerome
* John Amos as Dr. Conrad

==Featured elements== Boulder and the "Tour of the Moon" at Colorado National Monument, were legendary Coors Classic stages.
 Robert Townsend has a minor role as a rival teammate.

ShaverSport, the company that sponsors Marcus and David in The Hell of the West, is an actual company. It was formed in 1980 by competitive cyclist Bob Shaver with its mission being to produce quality cycling wear. ShaverSport was asked by Warner Brothers to design the clothing for the film. To this day, the company exists and continues to produce not only cycling gear, but ShaverSport and Hell of the West replica jerseys that are featured in the film. 

Marcus and David are seen riding 1985 Specialized Allez SE road bikes with red frames in the race scenes and most of the movie.

==Reception==
The film was released prior to Costner becoming a Hollywood superstar. It had a limited release on 16 August 1985 and grossed $1.4 million in the US. 

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 