Rio Sex Comedy
{{Infobox film
| name           = Rio Sex Comedy
| image          =Riosexcomedyposter.jpg
| caption        = Promotional poster
| director       = Jonathan Nossiter
| producer       = Matias Mariani Flávio R. Tambellini Philippe Carcassonne Santiago Amigorena Jonathan Nossiter
| writer         = Jonathan Nossiter
| starring       = Charlotte Rampling Bill Pullman Irène Jacob
| music          = 
| cinematography = Lubomir Bakchev
| editing        = Sophie Brunet Jonathan Nossiter
| distributor    = 
| released       =  
| runtime        = 124 minutes
| country        = France
| language       = English French
| budget         = €5 million
| gross          = 
}}
Rio Sex Comedy is a 2010 comedy film, written and directed by Jonathan Nossiter. It premiered at the 2010 Toronto International Film Festival on 16 September. 

==Plot==
Rio de Janeiro is the destination of choice for the misadventures of several expatriates, seeking both personal pleasure and social justice. The eccentric grouping of expatriates include; A plastic surgeon, an unconventional new US ambassador to Brazil as well as a filmmaking French couple. 

==Cast==
*Charlotte Rampling as Charlotte
*Bill Pullman as William
*Irène Jacob as Irène
*Fisher Stevens as Fish
*Daniela Dams as Iracema
*Jérôme Kircher  as Robert
*Jean-Marc Roulot  as Antoine
*Bob Nadkarni as Bob

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 