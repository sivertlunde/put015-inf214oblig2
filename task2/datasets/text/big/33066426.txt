Big Fat Gypsy Gangster
{{Infobox film
| name           = Big Fat Gypsy Gangster
| image          = Big Fat Gypsy Gangster Uk Theatrical.jpg
| alt            =  
| caption        = Promotional Poster
| director       = Ricky Grover
| producer       = Ricky Grover Jonathan Sothcott Paul Silver Maria Grover
| writer         = Ricky Grover Maria Grover
| starring       = Ricky Grover Steven Berkoff Peter Capaldi Omid Djalili Rochelle Wiseman Tulisa Contostavlos Rufus Hound
| music          = Venti Venti Media
| cinematography = Gary Shaw
| editing        = Jason de Vyea
| studio         = 
| distributor    = 4Digital Media
| released       =  
| runtime        = 94 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Big Fat Gypsy Gangster is a straight-to-DVD British comedy film directed, produced, written by and starring Ricky Grover. The film centres on Grovers character Bulla from The 11 OClock Show, after his release from prison. The film was written by Grover and his wife Maria, and has an all-star line up including Steven Berkoff, Peter Capaldi, Omid Djalili, Rufus Hound and Tulisa Contostavlos.

==Plot==
The film begins with Bulla (Ricky Grover), a well known dangerous criminal, being released from prison after serving 16 years for burglary. However, as soon as he is released back into society, he finds himself being followed by a film crew. With the world at his fingertips, Bulla returns home to find that everything he was once part of has been taken over by corrupt police officer Conrad (Eddie Webber), the man who put Bulla behind bars. Bulla vows to regain everything that was once his—and begins his offensive by being interviewed on national television by Michael Parkinson.

==Cast==
* Ricky Grover as Bulla
* Steven Berkoff as Guru Shah
* Peter Capaldi as Peter Van Gellis
* Omid Djalili as Jik Jickels
* Laila Morse as Aunt Queenie
* Tulisa Contostavlos as Shanikwa
* Rochelle Wiseman as Jodie
* Rufus Hound as Kai
* Eddie Webber as Conrad
* Dave Legeno as Dave
* Leo Gregory as Danny Geoff Bell as Geoff
* Roland Manookian as Roland Andy Linden as Lenny
* Leanne Lakey as Bev
* Derek Acorah as Himself
* Michael Parkinson as Himself

==External links==
*  
*  
* http://www.4digitalmedia.com/index.php/details/160

 
 
 