Tora-san, the Expert
{{Infobox film
| name = Tora-san, the Expert
| image = Tora-san, the Expert.jpg
| caption = Theatrical poster
| director = Yoji Yamada
| producer =
| writer = Yoji Yamada Yoshitaka Asama
| starring = Kiyoshi Atsumi Yuko Tanaka
| music = Naozumi Yamamoto
| cinematography = Tetsuo Takaba
| editing = Iwao Ishii
| distributor = Shochiku
| released =  
| runtime = 106 minutes
| country = Japan
| language = Japanese
| budget =
| gross =
}}

  is a 1982 Japanese comedy film directed by   when the Hong Kong label Panorama did so in 2006. 

==Synopsis==
Tora-san gets into an argument with his uncle and sets out on the road again. In Kyushu he meets a young woman named Keiko and the shy zoologist Saburō, and attempts to play matchmaker between the two when they all return to Tokyo.       

==Cast==
* Kiyoshi Atsumi as Torajirō 
* Chieko Baisho as Sakura
* Yuko Tanaka as Keiko
* Kenji Sawada as Saburō
* Masami Shimojō as Kuruma Tatsuzō
* Chieko Misaki as Tsune Kuruma (Torajiros aunt)
* Gin Maeda as Hiroshi Suwa
* Hisao Dazai as Boss (Umetarō Katsura)
* Gajirō Satō as Genkō
* Hidetaka Yoshioka as Mitsuo Suwa
* Asao Utada as Katsuzō Muta
* Miyuki Kojima as Yukari Nomura
* Haruko Mabuchi as Kinuko

==Critical appraisal== Japan Academy Prize.   The casting of Kenji Sawada, then one of Japans most flamboyant rock stars, as the painfully shy Saburo, also drew attention.

Stuart Galbraith IV judges this film to be one of the best in the series, helped by Tanakas outstanding performance.  The German-language site molodezhnaja gives Tora-san, the Expert three and a half out of five stars.   

==Availability==
Tora-san, the Expert was released theatrically on December 28, 1982.  In Japan, the film has been released on videotape in 1986 and 1996, and in DVD format in 2005 and 2008. 

==References==
 

==Bibliography==
===English===
*  
*  
*  
*  

===German===
*  

===Japanese===
*  
*  
*  
*  

==External links==
*   at www.tora-san.jp (Official site)

 
 

 
 
 
 
 
 
 
 
 

 