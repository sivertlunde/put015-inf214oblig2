Hula-La-La
{{Infobox Film |
  | name           = Hula-La-La
  | image          = Hulalal51.jpg
  | image size     = 190px
  | director       = Hugh McCollum
  | writer         = Edward Bernds Kenneth MacDonald Emil Sitka Joy Windsor Maxine Doviat
  | cinematography =  Henry Freulich
  | editing        =  Edwin H. Bryant
  | producer       = Hugh McCollum 
  | distributor    = Columbia Pictures 
  | released       =  
  | runtime        = 16 06"
  | country        = United States
  | language       = English
}}

Hula-La-La is the 135th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges are choreographers at B. O. Pictures who are assigned to teach island natives how to dance. The studios president, Mr. Baines (Emil Sitka) has purchased the fictional Pacific island of Rarabonga (parody of Rarotonga one of the Cook Islands) for his next musical extravaganza, but learns that the local natives have never heard of dancing.
 Kenneth MacDonald). Shemp makes it clear he does not want the "hair cuts down to my neck!" and the Stooges try to flee with the help of the Tribe Kings daughter Luana (Jean Willes). She wants them to rescue her boyfriend from the witch doctor, who plans to behead him in the morning—along with the Stooges. In one of the huts, the Three Stooges try to get their hands on a box of surplus World War II hand grenades guarded by a living Kali type four-armed totem idol (Lei Aloha). After getting the daylights beat out of them by the fierce idol, the boys grab the box of grenades, and fool the Witch Doctor into proving his expertise with his sword by  slicing the box of grenads with his huge sword, and the grenades promptly explode, blowing him out of the atmosphere.

With Witch Doctor Varanu gone, the Stooges commence with their choreography lessons and teach the natives to dance.

==Production==
Hula-La-La was filmed on May 25-29, 1951.    It was the only Three Stooges film directed by producer Hugh McCollum, who gave the medium a shot while Edward Bernds was busy directing feature films. Bernds described McCollums directing style as "gentle and tasteful", like McCollum himself. However, film author Ted Okuda believed this hurt his films, not allowing them to reach their full potential. Hula-La-La was cited as an example of suffering from moments of restraint, resulting in several scenes lacking their comedic punch.   

The standard "Three Blind Mice" theme is replaced during the end titles with a hula composition entitled "Lu-Lu." The tune was written by Columbia Pictures composers Ross DiMaggio and Nico Grigor.   

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 