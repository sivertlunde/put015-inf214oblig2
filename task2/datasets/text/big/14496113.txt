Remix (film)
{{Infobox film name = Remix director   = Martin Hagbjer producer   = Søren Juul Petersen writer     = Martin Hagbjer Anders Valbro starring   = Micky Skeel Hansen Camilla Bendix Jakob Cedergren Sofie Lassen-Kahlke music          = Martin Hagbjer cinematography = Anders Löfstedt editing        = Marie-Louise Bordinggaard
|distributor= Scanbox
|released=   country        = Denmark runtime    =  language = Danish
|}}

Remix is a Danish 2008 feature film directed by Martin Hagbjer starring Micky Skeel Hansen as a 16-year-old pop singer Ruben. Remix is inspired by the true story of Danish pop idol Jon Gade Nørgaard known by the mononym Jon. Jon was also the subject of the documentary feature film Solo released in 2007. The film was released on January 25, 2008.

==Synopsis==
Ruben (played by Micky Skeel Hansen), an aspiring young man is offered a record contract by the music executive Tanya (portrayed by Camilla Bendix). The film, which co-stars Jakob Cedergren, Sofie Lassen-Kahlke, Henrik Prip and Anette Støvelbæk, follows Rubens fall from grace in the hands of the music industry. 

==Cast==
* Micky Skeel Hansen - (Ruben)
* Camilla Bendix - (Clara)
* Jakob Cedergren - (Jes)
* Anette Støvelbæk - (Lone, Rubens mother)
* Kristian Halken - (Flemming, Rubens father)
* Thomas Waern Gabrielsson - (Michael)
* Henrik Prip - (Mark)
* Sofie Lassen-Kahlke - (Robinson Tanja)
* Kim Sønderholm - (Stig)
* Kasper Warrer Krogager - (Rubens friend)
* Svend Laurits Læssø Larsen - (Victor, Rubens friend)
* Jarl Friis-Mikkelsen - (Studievært)
* Johan H. Kjellgren - (Björn)
* Carina Due - (Disco pige)
* Søren Juul Petersen - (Stengade conferencer)
* Marcelino Ballarin - (music video instructor)
* Christian Geisnæs - (photographer)
* Sune Bjørnvig - (young man)
* Caspar Møller - (Rubens friend)
* Georgios Alexander Fylakouris - (Rubens friend) Peter Lehmann - (recording engineer)
* Cajsa Hansen - (Marks daughter)

==Music==
Prior to the films theatrical release, its original songs were made available online for free, legal download, which attracted wide, positive attention.

==Critical reception==
Remix was a major film by Martin Hagbjer who had won the 2005 Prize of the Childrens Jury at Lübeck Nordic Film Days. Reviews were mixed. Most critics noted a lack of surprise, but praised the fine acting, especially by the films two leads Camilla Bendix and Micky Skeel Hansen.

Berlingske Tidendes film critic Ebbe Iversen gave the film four stars out of six and called Micky Skeel Hansen "a true star with musicality, attractive looks and a both defiant and vulnerable radiance which gives him the appearance of a new James Dean". 

== External links ==
* 
* 

 
 


 