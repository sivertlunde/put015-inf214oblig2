Beginning of the End (film)
{{Infobox film
| name = Beginning of the End
| image = Beginningoftheend.jpg
| image_size =
| caption = Poster art for the film
| director = Bert I. Gordon
| producer = Bert I. Gordon
| writer = Fred Freiberger Lester Gorn
| narrator = Peter Graves Peggie Castle Morris Ankrum Than Wyenn Richard Benedict James Seay
| music = Albert Glasser
| cinematography = Jack A. Marta
| editing = Aaron Stell
| distributor = Republic Pictures
| released =  
| runtime = 76 minutes
| country = United States English
| budget =
| gross =
}}

For the 1947 docudrama, see The Beginning or the End

 American science Peter Graves and Peggie Castle. The film is about an agricultural scientist (Graves) who has successfully grown gigantic vegetables using radiation. Unfortunately, the vegetables are then eaten by locusts (the swarming phase of short-horned grasshoppers), which grow to gigantic size and attack the nearby city of Chicago. The film is generally recognized for its "atrocious" special effects and considered to be one of the most poorly written and acted science fiction motion pictures of the 1950s. Warren, Keep Watching the Skies! American Science Fiction Movies of the Fifties, 1997, p. 325-326. 

==Plot== grain silo.
 city bus. The monsters have eaten all the crops in the area, and now are seeking human beings as a means of sustenance. It is also clear that they are headed for the city of Chicago.  Wainwright and Aimes meet with General Hanson (Ankrum), Colonel Sturgeon (Henry), and Captain Barton (Seay) to strategize a solution. Machine gun and artillery fire seem ineffective against the creatures, and there are far too many to effectively deal with all at once.  The United States Army and Illinois National Guard are called upon to help protect the city.  But the monsters quickly invade Chicago, and began to feast on human flesh as well as several buildings.

General Hanson concludes that the only way to destroy the beasts en masse is to use a nuclear weapon and destroy Chicago. However, Dr. Wainwright realizes that the locusts are warm-weather creatures. He concludes that he might be able to lure the locusts into Lake Michigan. There, the cold water will incapacitate them, and they will drown.  The lure itself will be a decoy locust mating call, generated electronically with test-tone oscillators.  The plan is put into effect, and it works at the last possible moment.  The monstrous locusts drown, but Dr. Wainwright and Ms. Aimes wonder if other insects or animals might have eaten other radioactive crops. They ponder whether the whole world might be facing an attack of monstrous creatures.

==Cast==
*Peter Graves as Dr. Ed Wainwright
*Peggie Castle as Audrey Aimes
*Morris Ankrum as Gen. John Hanson
*Than Wyenn as Frank Johnson
*Thomas Browne Henry (appearing as "Thomas B. Henry") as Colonel Sturgeon
*Richard Benedict as Corporal Mathias
*James Seay as Captain James Barton

==Production== Invaders from The War subgenre of science fiction films in the 1950s.  Everman, Cult Horror Films: From Attack of the 50 Foot Woman to Zombies of Mora Tau, 1993, p. 205. 

Beginning of the End was financed by   to get them into theatres. Ryfle, Godzilla: The Unauthorized Biography, 1998, p. 67. 

Beginning of the End went into production in 1956, the first of the "boom years" for science fiction films in the United States.  Its production was a direct outcome of the success of Them! 
 supervising producer televised commercials network TV shows, had produced his first feature film (Serpent Island) in 1954, and directed his first feature film (King Dinosaur) in 1955.  Johnson, Cheap Tricks and Class Acts: Special Effects, Makeup and Stunts From the Films of the Fantastic Fifties, 1996, p. 89.  
 The Food of the Gods.) 

Casting was complete within two weeks of the start of production. In late November, AB-PT said actress Mala Powers was being considered for the female lead.  But on December 2, the studio revealed that Peter Graves and Peggie Castle had been cast as the leads.   Three days later, AB-PT announced that Don C. Harvey, Morris Ankrum, Pierre Watkin, Ralph Sanford, and Richard Benedict had also been cast. Scheuer, "John Beal Back in Film," Los Angeles Times, December 6, 1956.  The studio also said that Pat Dean, its "sexboat" discovery (and a former dancer at the El Rancho Vegas hotel and casino in Las Vegas, Nevada) would also appear in the picture.  Larry Blake, Duane Cress, James Douglas, Eileen Jannsen, John Kranston, Ann Loos, and Jeanne Wood were added to the cast a few days later.  Ankrum, Henry, and Seay were cast because they usually played military men in B movies, roles they portrayed in Beginning of the End as well. 
 art director was Walter Keller. 

It is not clear what the budget for the picture was, although descriptions often use the term "low budget" or "ultra-low-budget." According to a statement by AB-PT President Leonard Goldenson in 1957, the average cost of the AB-PT pictures greenlit to date was $300,000. "ABPT Plans Shift to Quality Prods; $1,000,000 Budgets," Variety, September 24, 1957.  In comparison, Invaders From Mars was budgeted at $150,000,  The Beast From 20,000 Fathoms cost $400,000,  It Came From Outer Space cost $532,000,  Them! came in at under $1 million,  and 1953s The War of the Worlds cost $2 million. 

Filming began December 3, 1956. "Film Production Chart," Variety, December 14, 1956.  Shooting took place on the Republic Pictures backlot at 4024 Radford Avenue in Los Angeles, California (built by Mack Sennett and now home to CBS Studio Center).  Weaver, Earth vs. the Sci-Fi Filmmakers: 20 Interviews, 2005, p. 145.   Actor Peter Graves said Gordon "was okay, he was a good director."  He also had praise for actress Peggie Castle, and said he felt privileged to be working with an actor of Morris Ankrums stature. 

Gordon himself provided the special effects for the film.    According to composer Glasser, Gordon literally worked out of his home garage.   ,   for filming, state agricultural officials required that every single one of the animals be inspected and sexed.    He later described his efforts: 
:I had to get my grasshoppers from Waco, Texas. They had the only species large enough to carry focus. I could only import males because they didnt want the things to start breeding. They even had someone from the agricultural department or some place like that come out to take a head count, or wing count. The grasshoppers turned cannibalistic.
Gordon kept the grasshoppers in a box for only a few days, but during that time the grasshoppers devoured one another and only 12 were left when Gordon finally got around to shooting them.     Parla, and Mitchell, "Talking Eye To Eye With Duncan Dean Parkin (1993 Interview)," Filmfax, October 1996 – January 1997, p. 104.  Gordon also considered building miniatures for the grasshoppers to climb on, but this, too, was deemed too expensive.   Instead, Gordon used still photographs of the Wrigley Building and other noted Chicago landmarks and simply filmed the grasshoppers moving about on top of the photograph.   Pratt, Doug Pratts DVD: Movies, Television, Music, Art, Adult, and More!, 2004, p. 104.  When the monsters are supposed to be wounded or killed by gunfire, Gordon merely tipped the photograph and the grasshoppers slid down it.  According to one film historian, "the effect looks (almost) real" until one of the grasshoppers steps off the "building" into what is supposed to be thin air. 

==Release== Midwest on Island of Lost Souls and 1956s The Black Sleep), another AB-PT production.     "Oversized Insects Go on Rampage," Los Angeles Times, September 19, 1957.  As planned, it was distributed by Republic Pictures.  Two taglines were used to promote the film.  The first was "New Thrills! New Shocks! New Terror!" Halliwell and Walker, Halliwells Film Guide, 1994, p. 92.  This tagline is depicted on the film poster (see the infobox, above). The other tagline used was "The screens first full-length science-fiction feature with real-live creatures!"  There is some discrepancy as to how long the film ran.  Some sources cite 73 minutes,  Maltin, Leonard Maltins 2009 Movie Guide, 2008, p. 105.  some 74 minutes,   Broderick, Nuclear Movies..., 1991, p. 82.  and some 76 minutes. 

The film was a modest hit, and profitable for AB-PT.  For example, in its first week playing in San Francisco, California, it made $16,000—just behind the top-ranked movie of the week for the area, the reissue of Bambi (which grossed $18,500).  The films debut in Los Angeles saw its gross reach $16,500, although this was a soft movie-going market. 

Beginning of the End proved an apt title for its parent studio.  AB-PT shuttered its operations immediately after releasing the film, for reasons which are still unclear.  Gordons work for AB-PT landed him a new contract with American International Pictures (AIP).  The week Beginning of the End opened, Gordon began shooting his next feature film, The Amazing Colossal Man, for AIP. 

==Critical reception==
Beginning of the End made Bert I. Gordon famous for his giant monster films.  Critics and film historians point out that the film is only one of many which drew heavily on most Americans fear about atomic weapons, open-air nuclear tests, and the possibility of nuclear war.    Derry, Dark Dreams: A Psychological History of the Modern Horror Film, 1977, p. 54.  Tsutsui, "Looking Straight at Them!: Understanding the Big Bug Movies of the 1950s," Environmental History, April 2007.  However, Beginning of the End had very little of the metaphorical creativity of films such as Godzilla (1954 film)|Godzilla or Them!   A more recent assessment, however, concludes that the film taps more deeply into 1950s Americans worries over invasive species and growing unease over pesticides (like DDT). 

The film received extensive negative reviews at the time of its release and by modern film historians.   (which might be expected to go easy on the film due to the Chicago locale), had several negative things to say about it. "The film obviously was made on a shoestring budget, and the people in it are no more than props for the magnified insects. I doubt if it will fool anyone. But youngsters will probably think its great stuff."  The   summed up his review in one word: "awful".  Another recent film guide called it "Bottom-of-the-sci-fi-barrel rubbish, very boring to watch."  One review pointed out that Gordon didnt even bother to hide the mountains in the background of the shots (Central Illinois is mostly flat prairie land).  Some have also been upset with films lack of horrifying images. Schoell, Creature Features: Nature Turned Nasty in the Movies, 2008, p. 54.  The derivative nature of the picture has also upset some critics. Critics say the film covered almost the same ground as the far superior monster movie Them!,  and it is clear that Gordon merely wanted to "cash in" on giant bug craze rather than come up with a story that was fresh and creative. 
 Deadly Mantis, the complete disappearance of the victims is especially chilling, as is the notion of 150 men, women and children being devoured overnight while in their beds. This aspect is like something out of H. P. Lovecraft|Lovecraft, although it is not exploited as well as it could have been."  Producer-director Bert I. Gordon said he did not care whether reviews were bad; what mattered was whether people went to see the film: "The movie audience these days consists almost entirely of teenagers. Either theyre naïve and go to get scared, or theyre sophisticated and enjoy scoffing at the pictures. There isnt much a teenager can scoff at these days, you know."  Lead actor Peter Graves also felt the film worked on a certain level. "I think they played OK. ... All of that was ludicrous, but there were a certain amount of people who bought it and loved it." 

Beginning of the End has become somewhat infamous because of its notoriously poor production values and unrealistic plot. The movie was parodied in the   (another blond, all-American actor similar to Peter Graves). 

==Home video releases==
Beginning of the End was released on DVD in March 2003 by Image Entertainment. However, this print was considered "smeary" and not a very high quality issue. 

The movie was the basis of an episode of the cable television show Mystery Science Theater 3000 (MST3K) during its fifth season.  This episode of MST3K (during which the movie is shown and heckled, or "riffed") was released on DVD in 2001. 

==Footnotes==
 

==Bibliography==
*{{cite journal
 | author = Variety staff
 | date = November 30, 1956
 | title = AB-PT Starting Prodn Sans Govt Greenlight Variety
 | volume = 
 | issue = 
 | pages = 
 | location = Los Angeles, California, USA
 | publisher = Syd Silverman
 | issn = 0042-2738
 | oclc = 810134503
}}
*{{cite journal
 | author = Variety staff
 | date = September 24, 1956
 | title = AB-PT to Enter Film Prodn At Same Time Goldenson Predicts 8,000 Houses to Fold Variety
 | volume = 
 | issue = 
 | pages = 
 | location = Los Angeles, California, USA
 | publisher = Syd Silverman
 | issn = 0042-2738
 | oclc = 810134503
}}
*{{cite journal
 | author = Variety staff
 | date = September 24, 1957
 | title = ABPT Plans Shift to Quality Prods; $1,000,000 Budgets Variety
 | volume = 
 | issue = 
 | pages = 
 | location = Los Angeles, California, USA
 | publisher = Syd Silverman
 | issn = 0042-2738
 | oclc = 810134503
}}
*{{cite book
 | last = Alberti
 | first = John
 | title = Leaving Springfield: The Simpsons and the Possibilities of Oppositional Culture
 | url = 
 | year = 2004
 | publisher = Wayne State University Press
 | location = Detroit, Michigan, USA
 | isbn = 9780814328491
 | oclc = 51323494
 | page = 247
}}
*{{cite journal
 | author = Variety staff
 | date = December 12, 1956
 | title = Beginning for Seven Variety
 | volume = 
 | issue = 
 | pages = 
 | location = Los Angeles, California, USA
 | publisher = Syd Silverman
 | issn = 0042-2738
 | oclc = 810134503
}}
*{{cite book
 | last = Beaulieu
 | first = Trace
 | title = The Mystery Science Theater 3000 Amazing Colossal Episode Guide
 | url = 
 | edition = illustrated
 | date = April 1, 1996
 | publisher = Bantam Books
 | location = New York City, New York, USA
 | isbn = 9780553377835
 | oclc = 33361547
 | page = 103
}}
*{{cite book
 | last = Berry
 | first = Mark F.
 | title = The Dinosaur Filmography
 | url = 
 | edition = illustrated
 | year = 2002
 | publisher = McFarland & Company
 | location = Jefferson, North Carolina, USA
 | isbn = 9780786424535
 | oclc = 62596035
 | page = 178
}}
*{{cite book
 | last = Broderick
 | first = Mick
 | title = Nuclear Movies: A Critical Analysis and Filmography of International Feature Length Films Dealing With Experimentation, Aliens, Terrorism, Holocaust, and Other Disaster Scenarios, 1914-1989
 | url = 
 | edition = illustrated
 | year = 1991
 | publisher = McFarland & Company
 | location = Jefferson, North Carolina, USA
 | isbn = 9780899505435
 | oclc = 24246995
 | page = 82
}}
*{{cite news
 | title = Which First on DVDs?
 | first = Cain
 | last = Tim
 | url =  Decatur Herald & Review
 | publisher = Todd Nelson
 | location = Decatur, Illinois, USA
 | issn = 
 | oclc = 22714514
 | date = January 12, 2001
 | page = 
}}
*{{cite news
 | title = City Locale for Science Fiction Film
 | author = Tribune staff
 | url = 
 | newspaper = Chicago Daily Tribune
 | publisher = Harold Grumhaus
 | location = Chicago, Illinois, USA
 | issn = 1085-6706
 | oclc = 60639020
 | date = June 16, 1957
 | page = 
}}
*Cornea, Christine. Science Fiction Cinema: Between Fantasy and Reality. New Brunswick, N.J.: Rutgers University Press, 2007.
*Dangcil, Tommy. Hollywood Studios. Charleston, S.C.: Arcadia, 2007.
*Derry, Charles. Dark Dreams: A Psychological History of the Modern Horror Film. South Brunswick, N.J.: A.S. Barnes, 1977.
*Dixon, Wheeler W. Lost in the Fifties: Recovering Phantom Hollywood. Carbondale, Ill.: Southern Illinois University Press, 2005.
*Erb, Cynthia Marie. Tracking King Kong: A Hollywood Icon in World Culture. Detroit: Wayne State University Press, 2009.
*Evans, Joyce A. Celluloid Mushroom Clouds: Hollywood and the Atomic Bomb. Boulder, Colo.: Westview Press, 1998.
*Everman, Welch D. Cult Horror Films: From Attack of the 50 Foot Woman to Zombies of Mora Tau. Secaucus, N.J.: Carol Publishing Group, 1993.
*"Film Production Chart." Variety. December 14, 1956.
*"Film Reviews: Beginning of the End." Variety. July 3, 1957.
*"Filming Planned by Theatre Chain." New York Times. September 22, 1956.
*Fischer, Dennis. Science Fiction Film Directors: 1895–1998. Jefferson, N.C.: McFarland, 2000. New York Times. May 24, 1951.
*"Graves, Peg Castle in Am-Pars Beginning." Variety. December 3, 1956.
*Halliwell, Leslie and Walker, John. Halliwells Film Guide. New York: HarperPerennial, 1994.
*Hardy, Phil and Gifford, Denis. The Encyclopedia of Science Fiction Movies. Minneapolis, Minn.: Woodbury Press, 1986.
*Heffernan, Kevin. Ghouls, Gimmicks, and Gold: Horror Films and the American Movie Business: 1953–1968. Durham, N.C.: Duke University Press, 2004.
*Johnson, John. Cheap Tricks and Class Acts: Special Effects, Makeup and Stunts From the Films of the Fantastic Fifties. Jefferson, N.C.: McFarland 1996.
*Kalat, David and Kaisha, Tōhō Kabushiki. A Critical History and Filmography of Tohos Godzilla Series. Jefferson, N.C.: McFarland & Co., 2007.
*"L.A. 1st Run Biz Droops; 189G Week." Variety. September 24, 1957.
*Larson, Randall D. Musique Fantastique: A Survey of Film Music in the Fantastic Cinema. Metuchen, N.J.: Scarecrow Press, 1985.
*Lisanti, Tom. Fantasy Femmes of Sixties Cinema: Interviews With 20 Actresses From Biker, Beach, and Elvis Movies. Jefferson, N.C.: McFarland, 2001.
*Loftus, Joseph A. "Paramount, A.BC. Cleared to Merge." New York Times. February 10, 1953.
*Lucian, Patrick and Coville, Gary. Smokin Rockets: The Romance of Technology in American Film, Radio, and Television, 1945–1962.  Jefferson, N.C.: McFarland & Co., 2002.
*Maltin, Leonard. Leonard Maltins 2009 Movie Guide. New York: Penguin, 2008.
*McGee, Mark Thomas. Fast and Furious: The Story of American International Pictures. Jefferson, N.C.: McFarland, 1984.
*"Movieland Events." Los Angeles Times. June 17, 1957.
*Nagro, Anne. "Bugshow." Pest Control Technology. July 27, 2010.
*"New DVDs." New York Times. July 1, 2008.
*"Oversized Insects Go on Rampage." Los Angeles Times. September 19, 1957.
*Parla, Paul and Mitchell, Charles P. "Talking Eye To Eye With Duncan Dean Parkin (1993 Interview)." Filmfax. October 1996 – January 1997.
*Payment, Simon. Introducing "It Came From Outer Space". New York: Rosen, 2007.
*Pomerance, Murray. American Cinema of the 1950s: Themes and Variations. New Brunswick, N.J: Berg, 2005.
*Pratt, Douglas. Doug Pratts DVD: Movies, Television, Music, Art, Adult, and More! New York: Harbor Electronic Publishing, 2004.
*Pryor, Thomas M. "6 Films Planned by Am-Par Corp." New York Times. January 28, 1957.
*Rajewski, Genevieve. Introducing the Deadly Mantis. New York: Rosen Publishing, 2007.
*"Reissued Bambi Out-Pulls New Product On Frisco 1st-Run Front." Variety. August 16, 1957.
*Rickman, Gregg. The Science Fiction Film Reader. New York: Limelight Editions, 2004.
*Ryfle, Steve. Godzilla: The Unauthorized Biography. Toronto: ECW Press, 1998.
*Scheuer, Philip K. "ABC-Paramount Starts Production Experiment." Los Angeles Times. December 3, 1956.
*Scheuer, Philip K. "John Beal Back in Film." Los Angeles Times. December 6, 1956.
*Schneider, Jerry L. Edgar Rice Burroughs and the Silver Screen: Vol. IV, The Locations. Raleigh, N.C.: Lulu, 2008.
*Schoell, William. Creature Features: Nature Turned Nasty in the Movies. Jefferson, N.C.: McFarland & Co., 2008.
*Tinee, Mae. "Beginning of the End Is Both Wild and Weird." Chicago Tribune. June 20, 1957.
*Tinee, Mae. "Full Summer of Film Fare on Its Way." Chicago Daily Tribune. June 23, 1957.
*Tinee, Mae. "2 Local Boys Now in Films Revist City." Chicago Tribune. May 19, 1957.
*Tsutsui, William M. "Looking Straight at Them!: Understanding the Big Bug Movies of the 1950s." Environmental History. April 2007.
*  Bright Lights Film Journal. August 2004.
*Warren, Bill. Keep Watching the Skies! American Science Fiction Movies of the Fifties. Jefferson, N.C.: McFarland & Company, 1997.
*Weaver, Tom. Earth vs. the Sci-Fi Filmmakers: 20 Interviews. Jefferson, N.C.: McFarland & Co., 2005.
*Weaver, Tom; Brunas, Michael; and Brunas, John. Science Fiction Stars and Horror Heroes: Interviews With Actors, Directors, Producers and Writers of the 1940s Through 1960s. Jefferson, N.C.: McFarland, 2006.
*Wright, Gene. Horrorshows: The A-to-Z of Horror in Film, TV, Radio and Theater. New York: Facts on File Publications, 1986.
*Young, R.G. The Encyclopedia of Fantastic Film: Ali Baba to Zombies. New York: Applause, 1997.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 