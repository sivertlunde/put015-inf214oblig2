Leonie (film)
{{Infobox film
| name           = Leonie
| image          = Leonie Film Poster.jpg
| caption        =
| director       = Hisako Matsui
| producer       = Patrick Aiello (executive) Ashok Amritraj Manu Gargi Yuuki Itoh
| writer         = Hisako Matsui
| starring       = Emily Mortimer Shido Nakamura
| music          = Jan A.P. Kaczmarek
| cinematography = Tetsuo Nagata
| editing        = Barbara Tulliver, Craig Hayes, Sabine Hoffman
| studio         = Hyde Park Entertainment
| distributor    = Vertigo Films
| released       =  
| runtime        =
| country        = Japan Japanese
| budget         =
| gross          =
}}
  is a 2010 Japanese film directed by Hisako Matsui and starring Emily Mortimer and Shido Nakamura.  The film is based on the life of Léonie Gilmour, the American lover and editorial assistant of Japanese writer Yone Noguchi and mother of sculptor Isamu Noguchi and dancer Ailes Gilmour.  

Production started in April 2009 and the film was released in Japan on November 20, 2010. An extensively reedited version of the film began a limited theatrical run in the United States on March 22, 2013 and was released on DVD on May 14, 2013.

==Plot (Japan release version)==
The film opens on a beach. A window overlooks the beach.  In a dark room, Isamu Noguchi, grown old, is chipping away at a large stone with a hammer and chisel.  "Mother, I want you to tell the story." The film periodically returns to this scene of Isamu at work.
 Bryn Mawr 1892.  After a class in which she argues with a professor about the importance of artist Artemisia Gentileschi, Leonie (Emily Mortimer) befriends Catherine Burnell (Christina Hendricks). Later, they meet Umeko Tsuda (Mieko Harada), a graduate student. In Tsuda’s room, Leonie gazes at a print of Hokusai’s The Great Wave off Kanagawa. 
 Pasadena 1904—where Leonie, living in a primitive tent with her mother Albiana (Mary Kay Place), bears a child temporarily named "Yo,"—and New York, where Leonie met Japanese poet Yone Noguchi (Shido Nakamura). She and Yone succumb to passion while collaborating on his anonymous novel, The American Diary of a Japanese Girl, published by Frederick A. Stokes (David Jensen). They quarrel when Yone returns unannounced from London with an apparently drunk Charles Warren Stoddard (Patrick Weathers). The Russo-Japanese War begins and Yone, declaring he will return to Japan, greets Leonies announcement of pregnancy with angry disbelief. Leonie tells her sad story to the now unhappily married Catherine, who reminds her of her advice not to be boring. In California, Leonie fends off a racist attack against her son and decides, against Albianas advice, to accept Yones invitation to come to Japan.

In Yokohama when the steam ship arrives, Yone finds Leonie and the child, on whom he now bestows the name Isamu. Welcomed somewhat coldly by Yone, Leonie accustoms herself to unfamiliar customs and meets three Tokyo University students Yone has arranged for her to tutor. Angered by Yones belated confession that he now has another wife, she moves out, against Yones protests. She begins tutoring the children of Setsu Koizumi, whose stories of her idyllic marriage with the late Lafcadio Hearn starkly contrast with her own. She also visits Umeko Tsuda to ask for a job at her now famous school, but Umeko, fearing scandal, refuses her. Leonie then gives birth to a daughter, Ailes Gilmour|Ailes. When they visit Yone, he calls her a slut. She decides to build a house in Chigasaki, allowing Isamu, who is unhappy in a Yokohama school, to stay home and supervise the construction.

  (Jay Karnes) for alleged treason. Rumely belatedly appears and makes arrangements for Isamu, now Americanized as Sam Gilmour. 

Leonie and Ailes (Kelly Vitz) arrive in New York and surprise Isamu (Jan Milligan) who, on Rumelys advice, is studying medicine. Leonie objects, telling Rumely Isamu is destined to be an artist, and he is soon seen neglecting his medical studies for drawing and sculpture. As Isamu gains artistic success and Ailes enters the world of dance, Leonie grows old, eking out a meager existence selling Japanese knickknacks. After an argument with Ailes, she becomes ill and is hospitalized. By the time Isamu makes it to her bedside she has died. At the small funeral Isamu meets Catherine.

In a closing scene shot in Sapporos Moerenuma Park, Leonie watches children play in the playground designed by Isamu.

==Cast==

* Emily Mortimer as Leonie Gilmour
* Shido Nakamura as Yone Noguchi
* Christina Hendricks as Catharine Burnell
* Mieko Harada as Umeko Tsuda
* Keiko Takeshita as Setsu Koizumi
* Masatoshi Nakamura as Toshu Senda
* Mary Kay Place as Albiana Gilmour
* Rob Brownstein as Frederick A. Stokes
* Yasuo Daichi as a carpenter
* Jay Karnes as Dr. Edward Rumely
* Takashi Kashiwabara as Michihiko Kawada
* Melissa Caudle as Maternity Ward Doctor
* Jan Milligan as Isamu Noguchi
* Marco St. John as Onorio Ruotolo
* Kelly Vitz as Ailes Gilmour
* Patrick Weathers as Charles Warren Stoddard
* So Yamanaka as Tomoharu Iwakura

==Production==
Hisako Matsui, an independent filmmaker who previously directed the films Yukie (1998) and Ori ume (2002)., http://home.att.net/~creativeentpr/JLSF/Aurora2004Screening.html  became interested in making a film of Leonie Gilmours life after reading Masayo Duuss biography of Isamu Noguchi. She began a grassroots fund-raising project for the film in 2005, giving personal presentations throughout Japan and encouraging her supporters to take an active role in fund-raising. http://www.myleonie.com/  One private donor contributed 1.2 billion yen (about ten million dollars) to the project. 

On Feb. 12, 2009, Production Weekly listed the film as a production of Hyde Park Entertainment to be produced by Ashok Amritraj and Patrick Aiello, with Emily Mortimer and Shidou Nakamura identified as cast members. Takamatsu and Moerenuma Park, Sapporo. In both cases, Matsui invited supporters to participate as extras.

On July 30, 2009, Matsui returned to the United States and began postproduction work in Los Angeles.  On September 1, 2009, she reported on her selection of  Barbara Tulliver as editor, citing Tullivers work on numerous films, and noting her extensive work for   started writing the score for Leonie.

On April 3, 2010 at Sogetsu Hall in Tokyo a special preview screening of the film was given for the films supporters.  The screening was followed by a party attended by Matsui, Nakamura Shido, and many of the films organizers.  It was announced that   in August 2011.

Following the films theatrical run in Japan, Matsui assembled a new production team to reedit and market the film for international release. The new edit, credited to Craig Hayes and Sabine Hoffman, simplified the narrative structure reducing the length from 132 minutes to 102 minutes. On August 30, 2012, The Wrap announced that Monterey Media had acquired U.S. rights to the film from the director. "The deal was brokered by ICM Partners," (Emily Mortimers agency) "on behalf of the filmmakers." The article noted that "Monterey plans a winter theatrical release for the film."  The reedited version opened its American theatrical run at New Yorks Clearview 1st & 62nd Cinemas on March 22, 2013. A DVD was released by Monterey Home Video on May 14, 2013.

==Critical Reception==

Response to the films limited theatrical release in the United States was mixed, with the film review aggregator Rotten Tomatoes reporting a 45% favorable score based on eleven reviews and a 71% "liked it" score out of 247 user ratings on the eve of the films DVD release in May 2013.  John Andersons review in Variety praised Matsui for "bring  to light a curious and intriguing story of a great-woman-behind-a-great-man" and also praised the production: "Tech credit are generally good, especially d.p. Tetsuo Nagata’s bronzed and burnished widescreen lensing; the interiors in particular pay much homage to Ozu."   The Hollywood Reporter commented that "the screenplay co-written by the director and David Weiner is frustratingly sketchy and at times overly florid, especially in its heavy doses of pseudo-poetic narration delivered by the older Leonie recounting her story. But it is also moving and inspirational in its portrait of a devoted mother’s passionate commitment to nurturing her son’s artistic talents.  The emotional impact of the film was also mentioned by several reviewers, including Rex Reed, who praised the film as "inspirational... a remarkable portrait of a brave, uncompromising woman who maintained her identity and spirit against all odds," though giving it an overall score of C+. 

Varietys reference to the film as a "feminist/revisionist celebration of the life of a major artist" was echoed in a number of reviews, but some were left unimpressed by its supposed feminism, including Rachel Saltz in the New York Times: "Too bad that the film that bears her name ultimately reduces her to the mother of her child."  Emily Mortimers performance in the title role was almost universally praised, The Hollywood Reporter and The Village Voice both referring to it as "stellar."  The Hollywood Reporter also praised the "stellar support" of Shido Nakamura and Mary Kay Place.

==Fictional Elements and Historical Inaccuracies==
 Pasadena campground to which Albiana welcomes Léonie in Matsui’s film,... in fact, Léonie lived during her Los Angeles years in Boyle Heights," and only worked in an office building in Pasadena.  
 John Sterling); Léonie did not meet her in an art history class, which Bryn Mawr College did not have at the time, but rather in their dormitory, where they shared a dining table.   "Noguchi’s relations with various editors and publishers were more complex than Matsui’s single figure of Frederick A. Stokes, publisher of The American Diary of a Japanese Girl, nor did Gilmour have anything to do with the decision to publish that book anonymously."  One of Gilmours students (played by actor So Yamanaka in Matsuis film) is misidentified by Duus and Matsui as Tomoharu Iwakura (grandson of Iwakura Tomomi); the student, named Iwamura according to Gilmour, was presumably "one of the five sons of Baron Iwamura Michitoshi, a former samurai from Tosa Domain|Tosa."  Duuss assumption that Gilmours scandalous life was the cause of her failure to land an anticipated job at Umeko Tsuda’s school is regarded by Marx as unlikely; Tsuda already had a qualified American teacher who worked without pay, and Gilmour worked at other morally-strict Christian schools.  Lafcadio Hearns wife Koizumi Setsukos English-speaking role in the film is at odds with Gilmours comment that "she has a very sympathetic face, but we could not speak each other’s language."  

Marx also notes the absence of "Yone’s other American love interest, Ethel Armes" from the film "Originally, blonde bombshell Nichole Hiltz was cast to play the role of Ethel, which would have made Yone’s motivations more comprehensible," but the Ethel scenes were eliminated because of time constraints. 

==Notes==
 

==References==
 
*  
*   
*  

 

==External links==
*    
*  
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 