The Devil Commands
 
{{Infobox film
| name           = The Devil Commands
| image          = Devil Commands poster.jpg
| caption        = Theatrical poster
| director       = Edward Dmytryk
| producer       = Wallace MacDonald William Sloane
| starring       = Boris Karloff Richard Fiske
| music          = Morris W. Stoloff
| cinematography = Allen G. Siegler
| editing        = Al Clark
| distributor    = Columbia Pictures Corporation
| released       =  
| runtime        = 65 minutes
| country        = United States 
| language       = English
| budget         = 
}}
 The Wolf William Sloane. Stephen Jacobs, Boris Karloff: More Than a Monster, Tomohawk Press 2011 p 265 

==Plot==
Dr. Julian Blair is engaged in unconventional research on human brain waves when his wife Helen (Shirley Warde) is tragically killed in a freak auto accident. The grief-stricken scientist becomes obsessed with redirecting his work into making contact with the dead and is not deterred by dire warnings from his daughter Anne (Amanda Duff), his research assistant Richard (Richard Fiske), or his colleagues that he is delving into forbidden areas of knowledge. He moves his laboratory to an isolated New England mansion where he continues to try to reach out to his dead wife. He is aided by his mentally-challenged servant Karl (Ralph Penney) and abetted by the obsessive Mrs. Walters (Anne Revere), a phony medium, who seems to exert a sinister influence over him. When their overly curious housekeeper discovers the truth about their experiments, her death brings the local sheriff in to investigate.

==Cast==
* Boris Karloff as Dr. Julian Blair
* Richard Fiske as Dr. Richard Sayles
* Amanda Duff as Anne Blair
* Anne Revere as Mrs. Walters
* Ralph Penney as Karl
* Dorothy Adams as Mrs. Marcy
* Walter Baldwin as Seth Marcy Kenneth MacDonald as Sheriff Ed Willis
* Shirley Warde as Helen Blair

==See also==
* List of American films of 1941
* Boris Karloff filmography

==Notes==
 

==References==
*  

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 