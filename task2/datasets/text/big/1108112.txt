Luxo Jr.
{{Infobox film
| name = Luxo Jr.
| image = Luxo Jr. poster.jpg
| border = yes
| caption = Poster for Luxo Jr.
| alt = Poster for Luxo Jr.
| director = John Lasseter William Reeves
| writer = John Lasseter
| starring =
| music =
| cinematography =
| editing =
| studio = Pixar
| distributor = Pixar
| released =  
| runtime = 2 minutes
| country = United States
| awards = Nominated for the Academy Award for Animated Short Film
| language = 
| budget =
| gross =
| preceded by =
| followed by =
}} desk lamp. Luxo Jr. Ed Catmull and John Lasseter left Industrial Light and Magics computer division. It is the source of the hopping desk lamp included in Pixars corporate logo.

Lasseters aim was to finish the short film for SIGGRAPH, an annual computer technology exhibition attended by thousands of industry professionals. The film would come from his experiments with modeling his Luxo lamp. Lasseter worked to improve the story within the allotted two minutes. In animation, the film demonstrates the use of shadow maps within the rendering software. Lasseter applied classic animation principles defined by Disneys Nine Old Men to convey the lamps emotions. Catmull and Lasseter worked around the clock, and Lasseter even took a sleeping bag into work and slept under his desk, ready to work early the next morning. The Pixar Story (2007) (TV documentary) 
 Eben Ostby. Best Animated CGI film nominated for an Academy Award.

In 2014, Luxo Jr. was deemed "culturally, historically, or aesthetically significant" by the Library of Congress and selected for preservation in the National Film Registry. 

==Plot==
Two Balanced-arm lamp|balanced-arm desk lamps, named Luxo Jr. (small) and Luxo Sr. (large), are playing with a small inflatable rubber ball. When Luxo Jr. tries balancing on it, the ball eventually deflates due to excessive jumping. As a result, Luxo Sr. admonishes Luxo Jr., but later finds Luxo Jr. playing with a beach ball, causing him to shake his head as a reaction to Luxo Jr.s antics. 

==Background==
The Graphics Group, which was one third of the Computer Division of  . The newly independent company was headed by Dr. Edwin Catmull as President and Alvy Ray Smith|Dr. Alvy Ray Smith as Executive Vice President. They were joined on the Board of Directors by Steve Jobs who was Chairman. 

Pixars small animation department—consisting of Lasseter, plus the part-time supporting efforts of several graphics scientists—was never meant to generate any revenue as far as Jobs was concerned. Price, p. 89  Catmull and Smith justified its existence on the basis that more films at SIGGRAPH like André and Wally B. would promote the companys computers. The group had no film at SIGGRAPH the preceding year, its last year under Lucass wing, apart from a stained-glass knight sequence they produced for Young Sherlock Holmes. Catmull was determined that Pixar would have a film to show at its first SIGGRAPH as an independent company in August 1986.  Luxo Jr. was produced by Pixar employee John Lasseter as a demonstration of the Pixar Image Computers capabilities.
 Brown architecture major turned graphics programmer, made Beach Chair, starring a chair that walked across the sand and nervously approached the water, dipped its front legs in just far enough to test the temperature, then scurried along. Reeves and Ostby also assisted Lasseter with model making and rendering on Luxo Jr. 

==Production==
Lasseters student film at CalArts, The Lady and the Lamp (1979), applied Walt Disneys observation that giving lifelike qualities to inanimate objects held comic potential. Luxo Jr. displayed a further insight, however: that inanimate objects as characters held the potential for dramatic value as well.  The film would come from Lasseters experiments with modeling his Luxo lamp.  He had felt inspiration strike when fellow employee Tom Porter brought his infant son Spencer to work one day and Lasseter, playing with the child, became fascinated with his proportions. A babys head was huge compared with the rest of its body, Lasseter realized. It struck Lasseter as humorous and he began to wonder what a young lamp would look like.  He fiddled with the dimensions of all the parts of his Luxo model—all but the bulb, since lightbulbs come from a store and dont grow, he reasoned—and he emerged with a second character, Luxo Jr. Price, p. 90 

Lasseter initially intended the film as a plotless character study. When he showed some early tests at animation festival in Brussels, respected Belgian animator Raoul Servais exhorted him, "No matter how short it is, it should have a beginning, a middle, and an end. Dont forget the story." Lasseter protested that the film would be too short for a story. "You can tell a story in ten seconds," Servais responded.  Lasseter was convinced. He devised a simple plot line in which the two lamps would play a game of catch with an inflated ball; Luxo Jr. would then approach the ball, hop onto it, bounce until he ball popped under him, and show dejection as the parent lamp looked on. Finally, Luxo Jr. would reappear feeling excited with a new, larger ball. Among the films shown at SIGGRAPH in 1985, Lasseter particularly admired a piece of character animation called Tony de Peltrie, from a group at the University of Montreal; it featured a strikingly expressive human character, an aging piano player who entertained while inwardly reflecting on better days, which inspired Luxo Jr. 
 RenderMan surface surface textures. articulation of "limbs" is carefully coordinated, and power cords trail believably behind the moving lamps. 
 cinematic level, it demonstrates a simple and entertaining story, including effectively expressive individual characters.  Catmull and Lasseter worked around the clock, and Lasseter even took a sleeping bag into work and slept under his desk, ready to work early the next morning. 

The music that plays during the short comes from a "needle drop" source, in particular, the album "Counterpoint In Rhythm" by British composer Brian Bennett on Bruton Production Music Library album BRD 17.  The final edit is a mix between at three different tunes: "Chateau Latour", "Quicksilver" and "Finesse".  

==Release== Dallas Convention Center Arena, where the audience of six thousand immediately recognized Luxo Jr. as a breakthrough. Price, p. 92  Before Luxo Jr. finished playing at SIGGRAPH, the crowd had already risen in applause.  "Pixars marketing department did not go out of its way to point out that none of the film, not a single frame, had been rendered on a Pixar Image Computer," wrote David Price in his book The Pixar Touch.  The audience was captivated by the far more realistic look than André and Wally B.. More significant than its photorealism, however, was its emotional realism. "It was perhaps the first computer-animated film that enabled viewers to forget they were watching computer animation," wrote Price. 

Afterward, Lasseter saw Jim Blinn, longtime professional colleague, approaching him, obviously readying a question. Lasseter braced for a question about the shadowing algorithm or some other recondite technical issue that he knew equally little about.  Blinn instead asked whether the big lamp was the mother or the father. Although the memories of those involved are now hazy, Lasseter elsewhere referred to the parent lamp as the father.  Lasseter then realized that he had succeeded in applying the Disney touch of thought and emotion to his characters. 

 "Luxo Jr. sent shock waves through the entire industry – to all corners of computer and traditional animation.  At that time, most traditional artists were afraid of the computer.  They did not realize that the computer was merely a different tool in the artists kit but instead perceived it as a type of automation that might endanger their jobs.  Luckily, this attitude changed dramatically in the early 80s with the use of personal computers in the home.  The release of our Luxo Jr. ... reinforced this opinion turnaround within the professional community.” &ndash;Edwin Catmull, Computer Animation: A Whole New World, 1998. 

The film was released for home video as part of Tiny Toy Stories in 1996 and Pixar Short Films Collection, Volume 1 in 2007.

==Legacy== Academy Award Best Animated CGI film Up and Down", and "Front and Back" have appeared on Sesame Street, which are now available on the Pixar Short Films Collection – Volume 1 though the original narrative track is not included.

Luxo Jr., without the cord, now serves as the mascot for Pixar Animation Studios, appearing in its production logo before and after every feature film (except for Toy Story, where he only appears after the film), in addition with the latest Walt Disney CGI variant used from 1995 to 2007. He hops in from the right, stops next to the "I" in "PIXAR", and jumps on it until he has completely squashed it down, as he did to the rubber ball in the short. He then looks around to check if the letter had been squashed down, then angles his "head" toward the camera; at this point, all the light typically fades to black except for his head, which goes out with a click after a moment. Occasionally, the head fades in time with the light—this is usually the case when the logo appears right before a Pixar feature film—but this is exceptional.

Some variations of this sequence were created for specific Pixar films. For Cars, the message "Celebrating 20 Years" appeared as the background faded out, with Luxo Jr.s head used as the zero. Pixar was founded in 1986; Cars, released in 2006, marked their 20th anniversary. 

In a teaser trailer for WALL-E, his bulb burns out when he turns to look at the camera. WALL-E rolls in from the right, replaces the bulb with an energy-efficient fluorescent bulb(to keep with the environmental themes of the film), and pats Luxo Jr. on the head before going back the way he came. However, he trips over the "R" in PIXAR as he goes, so he stops and positions his body to take its place, after which Luxo Jr. looks at the camera. WALL-E slightly peeks his eyes out, and the lights go out normally. This is also shown after the end credits of that film, also concluding with a "BnL" jingle and logo afterwards.

A new variant of the sequence, optimized for 3-D projection, was first played with the 3D version of Up (2009 film)|Up. The same animation as the regular logo plays as normal. However, the camera pans to the right while zooming out from the "P" in the PIXAR letters; therefore Luxo Jr is seen approaching from the centre when hes first seen.

Another anniversary variation was created for the 2011 film Cars 2, with the message "Celebrating 25 Years" appearing as the background faded out. The C appeared in the same position as Luxo Jr.s head, which subsequently faded away to leave only the text, again celebrating Pixars anniversary.

In Toy Story Toons, Luxo Jr. looks up to the falling curtain, which reveals the title, Woody and Buzz. Both pose as soon as the curtain fell completely.

==Other appearances==
*In 2012 three design students, Adam Ben-Dror, Shanshan Zhou & Joss Doggett created a real life, animatronic version of Luxo Jr. They called the project Pinokio. 

 
*Since the shorts release, the Luxo Ball has appeared in almost every Pixar production to date.

*In  , a book of Pixars history up through January 2007, film critic Leonard Maltin said that he "like  the fact that Luxo   still has significance to the people at Pixar", and remarked that it was something like Disneys Mickey Mouse. 

*The short was re-issued in 1999 and shown before screenings of Toy Story 2.  This version is preceded with the modern Pixar logo and the message "In 1986 Pixar Animation Studios produced its first film. This is why we have a hopping lamp in our logo". There is also a scene in Toy Story 2 where Hamm frantically flicks through TV channels to find a certain commercial. One of the channels is showing Luxo Jr. 

*In Tin Toy, a picture of Luxo Jr. appears.
 Toy Story Toon Hawaiian Vacation. In Toy Story 3, it is decorated with stickers.

*An audio-animatronics|audio-animatronic version of Luxo Jr. used to appear in Pixar Place at the Disneys Hollywood Studios theme park. 

*The Pixar logo was parodied by the comedy website CollegeHumor in a short titled "Pixar Intro Parody." The logo plays out as normal but Luxo Jr. kills the I letter; as the other letters mourn the loss of capital I Luxo Jr. is tried for murder and subsequently sentenced to the electric chair, leaving the other letters of the logo to celebrate its "death." 

==Notes==
 

==References==
*    

== External links ==
*  
*  
*  
* , Jim Hill Media
* , Designer of Luxo.
* , An Interview with John Lasseter

 
{{succession box
 | before = The Adventures of André and Wally B. short films
 | years  = 1986
 | after  = Reds Dream}}
 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 