Parineeta (2005 film)
 
 
{{Infobox film
| name           = Parineeta
| image          = Parineetaposter.jpg
| caption        = Theatrical release poster
| director       = Pradeep Sarkar
| producer       = Vidhu Vinod Chopra
| based on          = Parineeta by Sarat Chandra Chatterjee
| screenplay     = Vidhu Vinod Chopra Pradeep Sarkar
| starring       = Vidya Balan Sanjay Dutt Saif Ali Khan Raima Sen Diya Mirza
| music          = Shantanu Moitra
| cinematography = Natarajan Subramaniam
| editing        = Hemanti Sarkar Nitish Sharma
| studio         =
| distributor    = Vinod Chopra Productions
| released       =  
| runtime        = 131 minutes
| country        = India Hindi
| budget         =     
| gross          =  
}} musical romantic Bengali novella, Parineeta by Sarat Chandra Chattopadhyay. Directed by debutant Pradeep Sarkar, it was based upon a screenplay by the films producer, Vidhu Vinod Chopra. The film featured Vidya Balan, Saif Ali Khan and Sanjay Dutt in the lead roles. Raima Sen plays supporting role of Lalitas chirpy cousin. Sabyasachi Chakrabarty plays the pivotal role of Shekhars father. Diya Mirza, with a cameo appearance as Shekhars fiancé and Rekha, with a cameo performance of a night club (Moulin Rouge) singer, are other notable performances.

Parineeta primarily revolves around the lead characters, Lalita and Shekhar. Since childhood, Shekhar and Lalita have been friends and slowly this friendship blossoms into love. A series of misunderstandings surface and they are separated with the conniving schemes of Shekhars father. The plot deepens with the arrival of Girish who supports Lalitas family. Eventually, Shekhars love defies his fathers greed and he seeks Lalita.
 National Award Best First Film. Parineeta was showcased at prominent international film festivals.

==Plot==
The film places the story in Kolkata. As the credits roll, scenes from erstwhile Calcutta are displayed along with the narrators (Amitabh Bachchan) introduction of the era. The narration focuses on the night of the marriage of Shekhar and Gayatri Tantiya, a rich industrialists daughter. The audience is introduced to Navin Rai as Shekhars father while we see Shekhar readying himself for the occasion. While he is doing so, images of Lalita calling him by his name flash through his mind. Downstairs, musical celebrations begin as Shekhar meets Vasundhara, a widow from his neighbourhood, who is thankful to her son-in-law, Girish (Sanjay Dutt), for supporting their family after the death of her husband, Gurcharan. Lalita, who is present there, playfully confronts Shekhar as to why he is being indifferent to her. Shekhar admonishes her for speaking so in spite of being married.

An angry Shekhar comes back home to play a favourite tune from the past on his piano. The flashback shows a young Shekhar playing Rabindranath Tagore’s tune on his piano while young Lalita and Koel are around. Lalita, with her parents having died in a car accident, lives with Gurcharan’s family with Koel as her cousin whereas Charu is her neighbour. As this scene flashes across Shekhar’s mind, he sings a song full of sadness and loss. As time flies, they grow up to become close friends. The rebellious and musically inclined Shekhar spends his days playing the music of Rabindranath Tagore or Elvis Presley and composing his own songs with Lalita rather than becoming part of his shrewd fathers world of profit and business. Part of this rebellion involves resistance to meeting Gayatri Tantiya, the beautiful but devious daughter of a wealthy industrialist, whom his father would like Shekhar to marry into. Meanwhile, Girish, a steel tycoon from London, makes a dramatic entry into Charu’s house. Girish seems smitten by Lalita while Koel is by Girish. Shekhar is visibly jealous of Lalita’s close friendship with Girish.

One day, a shocked Lalita, who is employed at the Ray’s office, remembers a hotel project from Gurcharan’s ancestral haveli (palatial house). On an earlier occasion, Gurcharan had borrowed money from Navin Ray after putting his haveli on mortgage. She understands that if the money is not repaid in a few months, Navin Ray would take over the property. She immediately thinks of asking Shekhar for monetary help. Unforeseen circumstances prevent this, and Girish, upon realising this, alleviates their problem by making Gurcharan his business partner. Gurcharan repays the debt and the turn of events prompts Shekhar to think why Lalita chose to ask Girish for money instead of him. On one auspicious night, Shekhar and Lalita exchange garlands and consummate their "marriage" unbeknownst to anyone else.

While Shekhar is off to Darjeeling on a business trip, Navin Ray violently thunders at Lalita about the loss of his hotel project, embarrassing and humiliating her. Ray gets a wall built between his and Gurcharan’s house symbolising the end of their association. Gurcharan, unable to digest this, suffers a heart attack. Upon Shekhar’s return, Ray informs him of the ill-health of his mother and Gurcharan and viciously adds a note of Lalita and Girish’s marriage. Shekhar is disgusted to hear of the marriage and in his anger he scowls at Lalita, humiliating her like his father. In the meanwhile, Girish assists the Gurcharan family and takes them to London for the heart treatment. Misunderstandings follow and upon the family’s return from London, Shekhar assumes that Girish and Lalita are married and agrees to marry Gayatri. The film returns to the night of Shekhar’s marriage when Girish hands him the ownership papers of Gurcharan’s haveli. He shocks Shekhar by telling him that he got married to Koel because Lalita denied his marriage proposal. As a conclusion, Shekhar confronts his father and symbolically breaks down the wall separating the two families. He then brings Lalita to his home as his bride much to the delight of his mother.

==Main cast==
* Vidya Balan as Lalita. A woman of dignity with unflinching love for Shekhar. A lovely singer, whos resigned to the circumstances, upholding her respect when she faces insult.
* Saif Ali Khan as Shekhar Rai. Shows a balance of love for Lalita and jealousy towards Girish. He is a passionate musician. Towards the end has utter hatred of himself for transforming into such a cold and bitter person. 
* Sanjay Dutt as Girish. Simple and straightforward character who plays Lalitas moral support with warmth and compassion.
* Raima Sen as Koel. Lalitas playful, mischievous and cheerful cousin whos lively, peppy and lives every moment.
* Diya Mirza as Gayatri. Short but a devious character who longs for Shekhar with her charm and beauty.
* Sabyasachi Chakrabarty as Navin Ray. Cold at heart, a shrewd and a conniving businessman who even places human values and relationships below his greed for money.
* Surinder Kaur as Rajeshwari. The mother of Shekhar has to watch how bad her husband treats the neighbours and she gets ill.
* Tina Dutta as Teenage Lalita
* Supriya Shukla as Sunita
* Ninad Kamat as Ajit
* Rekha in a special appearance in song "Kaisi Paheli Zindagani" George Baker as Sir William Eckhardt

== Production ==

=== The key elements === commercials and 15 music videos.  Vidhu Vinod Chopra, the producer, took notice of his music videos and contacted Sarkar to direct some of the music videos of the film Mission Kashmir (2000).    After carving a niche in filming music videos, Sarkar gave the thought of filming Parineeta to Chopra.  There began the making of the film.
 1953 namesake film by Bimal Roy, the story was based in the year 1962. He said that it took them one and a half years to script the film, with them adding new characters and emphasising under-represented characters from the novel.         In a separate interview, Chopra admitted that he was actively involved with the screenplay because the film was an adaptation of the novel. When speaking about cinematic adaptation, Chopra gave due credit to Sarkar, and Natarajan Subramaniam (the films cinematographer), for providing the vintage visuals.  Saif Ali Khan once said that the film was initially attempted to be made in a contemporary way. When the films crew did not find the depiction appealing, the filming began with the 60s look.   

Chopra once cited an interesting anecdote about his belief in Sarkars film-making abilities. He said that he never personally signed the cheques for the films expenditure. He transferred money into Sarkars bank account and the cheques were eventually signed by Sarkar and his spouse. 

=== Casting ===
Chopra said in an interview that Saif Ali Khan and Vidya Balan were not the first choices for the lead roles in Parineeta.  In fact Abhishek Bachchan was being cast as Shekhar and Saif Ali Khan as Girish. However Abhishek walked out of the project and Saif readily was accepted as a replacement. This fact was corroborated by Saif in an interview  when he said that even though Sarkar had faith in his abilities, Chopra was not too keen on having him.

The choice of Vidya Balan came with Sarkars prior work experience with her in three music videos.    Chopra said that Vidya was screen-tested for six months and only then they were sure of her acting abilities.  Saif once said that he was not convinced of Vidyas abilities, since Parineeta was to be her first film.  He instead hoped to work with established actresses like Aishwarya Rai or Rani Mukherjee. However, after seeing Vidya portray the character of Lalita, he was thoroughly appreciative of her performance.  Getting an opportunity to debut with big names from the industry along with a lovely character to portray made Vidya accept the role of Lalita. 

The choice of Sanjay Dutt was because of his versatility, said Chopra in an interview. He also said that the affable nature of Sanjay was a plus during film making. 

Diya Mirza liked the script so well that she chose to play the role of the devious Gayatri in spite of it being a short role.    Raima Sen had a similar story with her want to work with the duo of Chopra and Sarkar.  The cameo of the veteran actress, Rekha was purely out of her interest in the song, "Kaisi Paheli Zindgani". 

=== Filming and music ===
Since Sarkar knew the city of Kolkata very well, the film was primarily shot there.  While most of the film was made in Kolkata, a small portion was shot in Darjeeling. 

To re-create the 60s era, lot of efforts were made by the producers. For instance, Chopra said that it cost them money and difficulty to procure the green coloured car which was driven by Shekhar in the film. Chopra confirmed the authenticity of the piano used in the song "Piyu Bole",  and of the toy train used in the song "Kasto Mazza".   About the look of the characters, Mirza said that the 60s look was well captured by the make-up artist, Vidyadar.  The song "Kaisi Paheli Zindagani" was based on the tune to Louis Armstrongs "A Kiss to Build a Dream on". 

Critics had high expectations from Parineetas music  because of the award-winning music that was provided in some of Chopras previous films, such as (  (1993) and Mission Kashmir (2000)). Shantanu Moitra, Parineetas music director composed hundreds of tunes before six of them got finalised after a years effort. After finalising the soundtrack, Chopra was appreciative of the musicians abilities when he said that Moitra has the potential to become another R. D. Burman, a yesteryear Bollywood music director. 

== Cultural and cinematic allusions == homage to this relationship by referencing Satyajit Rays film Charulata, which itself is based upon Tagores noted novella, Nashtanir. Lalita (Parineeta) is dressed to resemble Nashtanir / Charulata s Charu (Madhabi Mukherjee), particularly during the song "Soona Man Ka Aangan", which incorporates Tagores song "Phoole Phoole Dhole Dhole." In both Parineeta and Charulata, "Phoole Phoole Dhole Dhole" is sung while Lalita and Charu are each on a swing.   The film continues the above connection by placing Saif Ali Khan on the same train used in the film Aradhana (1969 Hindi Film)|Aradhana which featured his mother, Sharmila Tagore. 
 1953 namesake film and the 1976 film, Sankoch. 

== Reception ==

=== Box-office and ratings ===
Parineeta notched up Indian rupee|Rs. 206 million in India at the end of 2005.  Its overseas success was notable as well with gross collections of Rs. 36 million in the United Kingdom, Rs. 35 million in North America, and Rs 15 million in the rest of the overseas. 

=== Reviews, critiques and controversies === 1953 version, the preview suggested that debutant director, Sarkars inexperience in film-making, the probable inability of debutant actress, Balan to portray Lalitas character appropriately, suitable 1960s depiction by contemporary actors and musician, Shantanu Moitras until then unimpressive soundtracks, could be impediments to a successful adaptation. 

Post-release, the same critic said "Vidhu Vinod Chopras "Parineeta" – a remake of an old classic of the same name...(had) the love story (which) was received well by the younger generation and it went on to become the biggest hit of the year."  It was generally well-received by the critics,  with one of them terming the film as "...a beautiful story, beautifully told. It approximates what most of us expect, and increasingly yearn for, in vain, our cinematic experience."  A reviewer from About.com said that it is "contemporary retelling of...(an) engaging and timeless tale rich with human emotion and universal drama". The reviewer appreciates the film in most of the nuances of film making saying that this is "enhanced by a brilliant musical score, and accentuated by superb performances by Sanjay Dutt, Saif Ali Khan, Raima Sen, Diya Mirza and debutante Vidya Balan in the lead role of Lalita".  3000 copies of the novella were sold within weeks of the films release, owing to the films good reception. 
 Variety said "A character-driven meller thats a treat for the eyes, with performances to match, "Parineeta" is high-end Bollywood near its best". He also says that, "though the climax is still emotionally powerful, it comes over as overcooked."  A mixed review from Mid-Day  says "Pradeep Sarkar weaves the story like magic, especially in the first half&nbsp;... excelling in the detailing&nbsp;... the vintage look of the film (that stood out) with authentic costumes, props and the roads of Kolkata". However, the review criticised the films climax terming it as amateur, thereby diluting the whole films impact.  Another such review came from The Hindu which begins by saying "A simple enough story, but Sarkar tells it well, with some great shots of Shekhar and Lalita together." The review eventually says "Except for the end&nbsp;... where it is too much to bear." 

The issue of piracy cropped up when a news article published in The Indian Express exposed the dark side of the film market. Hardly into weeks of the films release, CDs were being sold at as low as Rs. 40.  Another blemish was that Soumitra Dasgupta, a writer and close associate of Sarkar alleged that the films story had a striking resemblance to his parallel work on the novella. 

== Screenings == International Indian Film Awards (IIFA) weekend in Amsterdam.    Owing to this European premiere, Sarkar said that the film had a great opening in the American and English box-offices.  The film was on the UK Top Ten films for four consecutive weeks. 

The film was chosen among 15 debut works for the 2006 Berlin Film Festival.   It received enthusiastic reception from the audience as it ran to packed houses to the Chopras surprise. Expecting an audience of about 100 Westerners, a critic visiting the festival was surprised to see the cinema hall full and people jostling for seats even on the steps. 

At the 24th Annual Minneapolis-St. Paul International Film Festival held in April 2006, Parineeta was the only Indian mainstream cinema to feature among 135 films from 40 countries. 

In 2006, the film featured in the Palm Springs International Film Festival,  24th San Francisco International Asian American Film Festival,  Indian Film Festival of Los Angeles,  Helsinki Film Festival  and the International Film Festival of Marrakech. 

== Awards == Best Art Best Choreography Best Debut RD Burman Best Sound Recording to Bishwadeep Chatterjee. It was also nominated for an additional nine awards, for the categories Best Film and Best Director, three for actors portraying Lalita, Shekhar and Girish and four nominations for the soundtrack.

The film also won a prominent awards in the form of two Star Screen Awards, three Zee Cine Awards among a notable awards. Apart from these, the film was nominated for an eclectic mix of awards.

2006 Filmfare Awards Best Debut (Female) – Vidya Balan 
*Best Choreography – Howard Rosemeyer 
*Best Sound – Bishwadeep Chatterjee 
*Best Art Director – Pradeep Sarkar, Tanushree Sarkar 
*R D Burman Award – Shantanu Moitra 

2006 Zee Cine Awards
*Best Dialogue – Rekha Nigam, Vidhu Vinod Chopra 
*Best Costume Design – Subarna Ray Chadhuri 

2006 Screen Awards
*Best Playback Singer (Female) – Shreya Ghosal 
*Best Debut (Female) – Vidya Balan 

2006 IIFA Awards
*Best Screenplay – Vidhu Vinod Chopra & Pradeep Sarkar 
*Best Art Direction – Pradeep Sarkar, Tanushree Sarkar 
*Best Sound Recording – Bishwadeep Chatterjee 
*Best Costume Designing – Subarna Ray Chadhuri 
*Best Makeup – Vidyadhar Bhatte 

== DVD == Dolby Digital progressive 24 Frame rate|FPS, widescreen and NTSC format.  

== Soundtrack ==
The soundtrack to Parineeta was released by Tips Music in April 2005 to positive reviews. 

{{Track listing
| extra_column = Singer(s)
| title1 = Piyu Bole | extra1 = Sonu Nigam, Shreya Ghoshal | length1 = 4:21
| title2 = Kasto Mazza | extra2 = Sonu Nigam, Shreya Ghoshal | length2 = 4:43
| title3 = Soona Man Ka Aangan | extra3 = Sonu Nigam, Shreya Ghoshal | length3 = 4:20
| title4 = Kaisi Paheli Zindagani | extra4 = Sunidhi Chauhan | length4 = 4:03
| title5 = Raat Hamari Toh | extra5 = K. S. Chithra|Chitra, Swanand Kirkire | length5 = 5:19
| title6 = Dhinak Dhinak Dha | extra6 = Rita Ganguly | length6 = 3:53
| title7 = Hui Main Parineeta | extra7 = Sonu Nigam, Shreya Ghoshal | length7 = 2:23
}}

==See also==
 
*Indira Gandhi Award for Best First Film of a Director

==References==
 

==External links==
*  - Vinod Chopra Films
*  
*  at Rediff.com
*  at Bollywood Hungama


 
 
 
 

 
 
 
 
 
 
 
 
 
 
 