Dharti Ke Lal
{{Infobox film
| name           = Dharti Ke Lal
| image          = 
| image_size     = 
| caption        = 
| director       = Khwaja Ahmad Abbas IPTA Pictures
| writer         = Khwaja Ahmad Abbas(Screenplay), Bijon Bhattacharya (Screenplay),   Krishan Chander (Story)
| story  =       Krishan Chander
| dialogue  =  Khwaja Ahmad Abbas
| starring       = Balraj Sahni Tripti Mitra Sombhu Mitra
| music          = Ravi Shankar
| lyrics       = Ali Sardar Jafri, Prem Dhawan
| cinematography =  Jamnadas Kapadia
| editing        = 
| distributor    = 
| released       = 1946
| runtime        = 125 mins
| country        = India
| language       = Hindi
}}
 1946 Hindi film and the first directorial venture of the noted film director Khwaja Ahmad Abbas (K. A. Abbas). It was jointly written by Khwaja Ahmad Abbas and Bijon Bhattacharya, based on plays by Bijon Bhattacharya and the story Annadata by Krishan Chander.

The film had lyrics by Ali Sardar Jafri, and Prem Dhawan.

In 1949, Dharti Ke Lal became the first Indian film to receive widespread distribution in the USSR. 

==Overview==

Dharti Ke Lal was critically acclaimed for its scathing view of notorious Bengal famine of 1943 in which over 1.5 million people died. It is considered an important political film as it gives a realistic portrayal of the changing social and economic climate during the World War II.

The film uses the plight of a single family caught in this famine, and tells the story of human devastation, and the loss of humanity during the struggle to survive.

During the Bengal famine of 1943, members of IPTA travelled all over India, performing plays and collecting funds for the survivors of the famine, which has destroyed a whole generation of farmer families in Bengal.  IPTA plays, Nabanna (Harvest) and Jabanbandi by Bijon Bhattacharya, and the story Annadata by Krishan Chander. Even the cast of the film was mainly actors from IPTA.
 Chetan Anand, also scripted by Abbas, and which continued with Bimal Roys Do Bigha Zamin (1953).
 IPTA (Indian Peoples Theater Association) and remains one of the important Hindi films of that decade. The film marked the screen debut of Zohra Sehgal and also gave actor Balraj Sahni his first important on screen role. 

The New York Times called it "...a gritty realistic drama."    

It proved to be tremendously influential not only to future filmmakers who admired its neorealist-like qualities—but also to intellectuals of Indias left-wing. 

==Cast==

* Tripti Mitra
* Sombhu Mitra
* Balraj Sahni
* Rashid Ahmed
* Damayanti Sahani Rashid Khan
* K. N. Singh
* David
* Zohra Sehgal
* Snehaprabha

==References==
 
* Dictionary of Films (Berkeley: U. of CA Press, 1977), p.&nbsp;84.
* Vasudev and Lenglet, eds., Indian Cinema Super-bazaar (New Delhi: Vikas, 1978).
* Shyamala A. Narayan, The Journal of Commonwealth Literature, 1 1976; vol. 11: pp.&nbsp;82 – 94.
* Amir Ullah Khan and Bibek Debroy, Indian Economic Transition through Bollywood Eyes.

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 