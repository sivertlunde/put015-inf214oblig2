Giallo (film)
 
{{Infobox film
| name           = Giallo
| image          = Giallo (movie poster).jpg
| alt            =
| caption        = Theatrical release poster
| director       = Dario Argento
| producer       = Claudio Argento Donald A. Barton Adrien Brody Tommaso Calevi Aitana de Val Luis de Val Billy Dietrich Patricia Eberle Oscar Generale Nesim Hason John S. Hicks Lisa Lambert Martin McCourt David Milner Rafael Primorac Richard Rionda Del Castro
| writer         = Jim Agnew Sean Keller Dario Argento
| starring       = Adrien Brody Emmanuelle Seigner Elsa Pataky
| music          = Marco Werba
| editing        = Roberto Silvi
| studio         = Giallo Production Footprint Investment Fund Media Films Opera Film Produzione
| distributor    = Hannibal Pictures
| released       =  
| runtime        = 92 minutes
| country        = United States United Kingdom Spain Italy
| language       = English
| budget         = 
| gross          = $32,655 
}}
Giallo is a 2009 giallo film directed by Dario Argento and starring Adrien Brody. The film was poorly received and is perhaps most-known for Brodys lawsuit against the film for not having been paid rather than the movie itself.

== Plot ==

Turin, Italy: French flight attendant Linda and Italian-American detective Enzo Avolfi team up to find Lindas younger sister Celine. Celine, a model, has been abducted by a serial killer. Known only as "Yellow" ("Giallo" in Italian) he kidnaps beautiful foreign women in his unlicensed taxi cab. After drugging them, the killer proceeds to mutilate and finally murder them. He photographs his deeds so that the photos may grant him personal sexual gratification.

Enzo receives a phone call from a fellow officer, who finds the body of an Asian woman outside a church near a fountain. They find out shes still alive, and starts to speak in Japanese. Enzo and Linda start to look for a translator, who tells them that the girl was praying Buddha and then repeated "yellow". They visit the morgue, where Linda realizes that the killers face could be yellow, and the coroner tells Enzo that yellow skin is symptom of liver disease, so the killer might be in the waiting list for a new liver.

They go to hospital and find Giallo but he escapes. Enzo finds out his name is Flavio Volpe and gets his address. Giallo starts to torture Celine by cutting one of her fingers when starts to bad-mouth him. Enzo breaks into Giallos apartment, but finds it empty and full of torture devices.

Enzo tells Linda why he became a policeman: long ago, Enzo, who was 10, witnessed his mother being murdered by a local butcher, whom she owed money to. 5 years later, Enzo kills the butcher in his own shop, while Inspector Mori knew why he did it. Mori took him in as if he were his own son. Enzo had kept the knife he used to kill the butcher as a reminder of how he became a cop. Enzo also tells Linda that Giallo might be a pattern killer who hates beautiful women, due to his deformity.

Over the course of the film, Enzo and Linda find more victims and clues as to who the killer is, and why he tortures them. An origin story shows that Giallos mother was a prostitute that gave her son up for adoption at a church, where orphaned kids started to bully him. Celine tries to escape from Giallo, but gets captured. Just when he was about to strangle her, Celine tells him shes rich and will reward him with money.

Giallo breaks into Lindas apartment and demands a ransom for the safe return of Celine. The film ends with Enzo and Giallo having a shootout in a hotel. Giallo tries to escape, but falls from a skylight to the floor of the lobby, killing him instantly. The police search his apartment for Celine, but cannot find her. They also try finding her at an abandoned gas factory where Giallo brings his victims.

Linda starts to blame Enzo for killing her sister, as well as calling him selfish and inhumane, because Giallo was going to tell her the location of where her sister is. Enzo tries to convince Linda that he was a killer and couldnt be trusted. After telling her this, he walks away, while Linda continues to berate him. By chance, while patrolling a car park, a policeman hears Celine as she tries to gain his attention, tied up and gagged in the trunk of Giallos car.

== Cast ==

* Adrien Brody as Inspector Enzo Avolfi / Flavio Volpe (Giallo)
* Emmanuelle Seigner as Linda
* Elsa Pataky as Celine
* Robert Miano as Inspector Mori
* Silvia Spross as Russian Victim
* Giuseppe Lo Console as Butcher
* Luis Molteni as Sal
* Lorenzo Pedrotti as Delivery Boy
* Daniela Fazzolari as Sophia
* Valentina Izumi as Keiko
* Taiyo Yamanouchi as Toshi
* Sato Oi as Midori
* Maryann McIver as Girl In Bookstore
* Barbara Mautino as Nurse
* Massimo Franceschi as Coroner
* Liam Riccardo as Baby Yellow
* Anna Varello as Butchers Wife
* Giancarlo Judica Cordiglia as Desk Sergeant
* Nicolò Morselli as Young Enzo
* Farhad Re as Designer
* Patrick Oldani as Officer Gian Luca
* Andrea Redavid as Officer #1
* Linda Messerlinker as Girl Victim

== Production notes== term used for a specific genre of crime fiction.

Argento said in an interview that he dislikes the producers cut of the film and that he no longer is attached to the project. 

== Release ==

Giallo premiered at the 2009 Edinburgh International Film Festival in Edinburgh, Scotland.  It premiered in America at the Omaha International Film Festival in Omaha, Nebraska on 14 March 2010.  Claiming he was not paid the correct amount for his role in the film, Adrien Brody filed a lawsuit against the filmmakers, and attempted to prevent the films release on DVD until he received his full salary.  In late November 2010 Brody was granted a ruling in United States District Court that blocked the sale or use of Brodys likeness in Giallo until the actors remaining salary was paid.  On 20 January 2011, Deadline Hollywood Daily reported that Brody had reached a settlement with the producers. Brody stated, "I very much enjoyed the process of making Giallo and am happy that things have been resolved and that people can now enjoy seeing the film." 

=== Critical reception ===
 torture porn slashers that made Argentos name." 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 