Swindle (2002 film)
 
{{Infobox Film
| name           = Swindle
| image          = Swindle (film).png
| caption        = 
| director       = K.C. Bascombe
| producer       = Nick Seferian
| writer         = K.C. Bascombe
| starring       = Tom Sizemore  Sherilyn Fenn  Dave Foley
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 2002
| runtime        = 
| country        = United States
| language       = 
| budget         = 
}}
Swindle is a 2002 crime thriller film written and directed by K.C. Bascombe and starring Tom Sizemore, Sherilyn Fenn and Dave Foley.

==Plot==
Four thieves attempt to make the richest score in history.

==Cast==
*Tom Sizemore  as Seth George
*Sherilyn Fenn  as Sophie Zenn
*Dave Foley  as Michael Barnes
*Conrad Pla  as Cisco
*Katie Griffin  as Judy

==See also==
*Girls with guns

==External links==
* 

 
 
 
 
 


 