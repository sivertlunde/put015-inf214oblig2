Nightwatch (1997 film)
{{Infobox film
| name           = Nightwatch
| image          = Nightwatch ver1.jpg
| caption        = Theatrical poster
| director       = Ole Bornedal
| producer       = Michael Obel
| writer         = Ole Bornedal Steven Soderbergh
| narrator       = 
| starring       = Ewan McGregor Patricia Arquette Josh Brolin Lauren Graham Nick Nolte
| music          = Joachim Holbek
| cinematography = Dan Laustsen
| editing        = Sally Menke
| distributor    = Dimension Films
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1,278,516 (United States|US) 
}} Danish film Nattevagten (1994), which was also directed by Bornedal.

==Plot==
A law student (McGregor) is hired as a night watchman at a morgue. He slowly discovers clues that point to him as the lead suspect in a string of serial murders.

==Cast==
*Ewan McGregor as Martin Bells
*Patricia Arquette as Catherine
*Josh Brolin as James Gallman
*Lauren Graham as Marie
*Nick Nolte as Inspector Thomas Cray
*Brad Dourif as Duty Doctor
*Alix Koromzay as Joyce
*Lonny Chapman as Old Watchman
*Larry Cedar as Waiter (uncredited)
*Sandra Hess as Student (uncredited)
*John C. Reilly as Deputy Inspector Bill Davis (uncredited)

==Reception==
Nightwatch was poorly received by critics, as the film holds a 23% rating on Rotten Tomatoes based on 26 reviews.

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 