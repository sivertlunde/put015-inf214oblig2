Daybreak (1948 film)
{{Infobox film
| name           = Daybreak
| image          = daybreakposter.jpg
| caption        = UK release poster
| director       = Compton Bennett
| producer       = Sydney Box
| writer         = Sydney Box Muriel Box
| starring       = Eric Portman Ann Todd Maxwell Reed
| music          = Benjamin Frankel
| cinematography = Reginald Wyer
| editing        = Peter Price Helga Cranston   Gordon Hales
| distributor    = General Film Distributors
| released       =  
| runtime        = 75 minutes
| country        = United Kingdom
| language       = English
}}
 lacunae which are considered to detract somewhat from an otherwise well-regarded film. 

==Plot== Bill Owen) public hangmen, called on periodically to travel to prisons around the country to perform judicial executions.

One evening during a rainstorm, Eddie goes into a public house for a drink and meets the bedraggled Frankie (Todd), an aimless, world-beaten drifter.  (There are suggestions that Frankie may be, or have been in the past, a prostitute; it is surmised that some of the deleted footage may have made this more explicit.)  The pair fall in love and are soon married, although Frankies vagueness about her past hangs unspoken over the relationship.  Eddies father dies, leaving him the family barge business on the River Thames.  He hands over the barbers shop to Ron and takes over the business, setting up home with Frankie on one of the barges.

Eddie hires a Scandinavian seaman Olaf (Reed) to work for him, and soon the arrogant Olaf has Frankie in his sights.  Despite herself she is attracted to him, but tries to deny her feelings.  Meanwhile Eddie is called away for reasons he cannot explain, and Frankie increasingly finds herself struggling to rebuff Olafs advances during Eddies absences.  When he is next called away, she begs him either not to go or to take her with him, but as neither is an option for Eddie, she pleads with elderly bargeman Bill Shackle (Edward Rigby) to stay with her while Eddie is gone.  Shackle is unable to grant her request due to other commitments.

Eddie returns unexpectedly early, and discovers Frankie and Olaf in an apparently compromising situation.  A fight ensues between the two men, during which Eddie is knocked overboard and fails to resurface.  The police are called by a witness to events, and Olaf is arrested for murder as it is presumed that Eddies body has been carried away on the tide.  In despair, Frankie commits suicide by shooting herself.  However, Eddie has managed to swim to shore and takes refuge with Ron.

Olaf is convicted of murder and sentenced to hang.  With nobody but Ron aware that Eddie and the alleged murder victim are one and the same, Eddie is summoned to carry out Olafs execution, and relishes the prospect of being able to avenge Frankies death.  When the time comes however, he is unable to go through with it and confesses his identity at the last moment.  He returns to the barbers shop and prepares to end his own life.

==Cast==
* Eric Portman as Eddie
* Ann Todd as Frankie
* Maxwell Reed as Olaf Bill Owen as Ron
* Edward Rigby as Bill Shackle
* Jane Hylton as Doris
* Eliot Makeham as Mr. Bigley
* Margaret Withers as Mrs. Bigley John Turnbull as Superintendent
* Maurice Denham as Inspector
* Milton Rosmer as Governor

==References==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 