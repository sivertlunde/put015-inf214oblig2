Black Gunn
{{Infobox film
| name           = Black Gunn
| caption        = 
| image	=	Black Gunn FilmPoster.jpeg
| alt            = 
| director       = Robert Hartford-Davis
| producer       = Jon Heyman, Franklin Coen, Rick Senat, Norman Priggen
| screenplay     = Robert Shearer, Franklin Coen, Robert Hartford-Davis (story)
| based on       = 
| starring       = Jim Brown Martin Landau Bernie Casey Herbert Jefferson Jr.
| music          = Tony Osborne
| cinematography = Robert H. Kline
| editing        = Pat Somerset
| distributor    = Columbia Pictures
| released       =  
| runtime        = 96 minutes
| country        = United States English
| budget         = 
| gross          =  $1,015,000 (US/ Canada rentals) 
}}
 blaxploitation film starring Jim Brown and Martin Landau and directed by Robert Hartford-Davis. 

== Plot ==
In Los Angeles, a nighttime robbery of an illegal mafia bookmaking operation is carried out by the militant African-American organization BAG (Black Action Group). Though successful, several of the bookmakers and one of the burglars are killed. The mastermind behind the robbery, a Vietnam veteran named Scott (Herbert Jefferson Jr.), is the brother of a prominent nightclub owner, Gunn (Jim Brown). Seeking safe haven, Scott hides out at his brothers mansion after a brief reunion.

Meanwhile, mafia caporegime and used-car dealer Russ Capelli (Martin Landau) meets with a female West Coast crime boss, Toni Lombardo (Luciana Paluzzi), to report the theft of daily payoff records and monies. Though Capelli receives an unrelated promotion for years of loyal service, he nonetheless fears the consequences of a loss of face and status as well as incriminating mob financial information. He therefore orders his men, led by psychotic assassin Ray Kriley (Bruce Glover), to shake down anyone who might have a connection to the robbery and to recover the lost goods using any means necessary.

== Cast ==
* Jim Brown as Gunn
* Martin Landau as Capelli
* Brenda Sykes as Judith
* Herbert Jefferson Jr. as Scott Gunn
* Luciana Paluzzi as Toni
* Vida Blue as Sam Green
* Stephen McNally as Laurento
* Keefe Brasselle as Winman William Campbell as Rico
* Bernie Casey as Seth
* Gary Conway as Adams
* Chuck Daniel as Mel
* Tommy Davis as Webb
* Rick Ferrell as Jimpy
* Bruce Glover as Ray Kriley

== Release ==
The film was released theatrically in the United States by Columbia Pictures in December 1972. 

The film was given a VHS release by Goodtimes Home Video in the United States.  It was later released on DVD in 2004 via Sony Pictures Home Entertainment.  This release is anamorphic in 1.85:1 aspect ratio. 

== References ==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 