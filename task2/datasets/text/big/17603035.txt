Odoru daisosasen bangaihen – Wangansho fukei monogatari shoka no kôtsûanzen special
 
{{Infobox film
| name           = Odoru daisosasen bangaihen – Wangansho fukei monogatari shoka no kôtsûanzen special
| image          = Odoru daisosasen bangaihen - Wangansho fukei monogatari shoka no kôtsûanzen special.jpg
| image size     = 270
| border         = 
| alt            = 
| caption        = DVD cover
| director       = Katsuyuki Motohiro
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       = Bayside Shakedown
| narrator       = 
| starring       = Yuki Uchida
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
 drama "Odoru daisosasen" (Bayside Shakedown) that aired in 1997 in Japan, but the original cast of the series only made small cameos in this sequel. The movie follows the struggles of young rookie Natsumi beginning her career as a female police officer at the same Wangan police station in which the action from the TV series took place.

==Cast==
{| class="wikitable"
!Actor
!Character
|- Yuki Uchida Natsumi Shinohara
|- Eriko Watanabe Saeko Kuwano
|- Eri Fukatsu Sumire Onda
|- Miki Mizuno Yukino Kashiwagi
|- Chosuke Ikariya Heihachiro Waku
|- Takehiko Ono  Kengo Hakamada
|- Yusuke Santamaria Masayoshi Mashita
|- Kenta Satoi Jiro Uozumi
|- Toshio Kakei  Kentaro Shinjo
|- Yuji Oda  Shunsaku Aoshima
|- Soichiro Kitamura
|Sōichirō Kanda
|}

==External links==
*  

 
 

 
 
 
 
 
 
 


 
 