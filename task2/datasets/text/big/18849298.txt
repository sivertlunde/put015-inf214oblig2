Harichandra (1998 film)
 
{{Infobox film
| name = Harichandra
| image = 
| director = Cheyyar Ravi
| producer = G. Thyagarajan G. Saravanan
| screenplay = Cheyyar Ravi G. Thyagarajan K. Jeevakumaaran
| story = K. Jeevakumaaran
| starring =  
| music = Anand, Gopal, Shaleen 
| editor = G. R. Anilmalnad
| cinematography = Ravi Yadav
| distributor = 
| released =  
| runtime = 150 minutes
| language = Tamil
| country = India
| budget = 
}}
 1998 Tamil Raasi in lead roles.

== Plot ==
A movie about a guy whose name his Harichandra (Karthik Muthuraman) who always lies and cheats everyone. But no matter what, nobody knows that he is a big liar and  a cheat and still continues to believe him. Then he meets a school principal Nandhini (Meena Durairaj|Meena) for a shooting purpose. Later he falls in love with her, thus he tries to cheat and lie to impress Nandhini (Meena Durairaj|Meena). But this time Nandhini (Meena Durairaj|Meena) was clever enough to identify all his lies and cheats but no matter what they still reunite again after every quarrel and fights. Quarrel and fights lasted till their wedding moment but eventually at the end only everyone know that no matter Harichandra (Karthik Muthuraman) lies and cheats people he is still a good human being. Thus Harichandra (Karthik Muthuraman) and Nandhini (Meena Durairaj|Meena) got married and lived happily.

== Cast ==
*Karthik Muthuraman as Harichandra Meena as Nandhini
*Priya Raman as Chitra
*Chinni Jayanth as Sundaram
*Vaiyapuri as Muthu Vivek as Gopal
*Delhi Ganesh as Harichandras father
*Satyapriya as Harichandras mother Janagaraj as a doctor
*Thalaivasal Vijay as Raghu, Chitras brother Anand as Joseph
*Vinu Chakravarthy as Anthony
*Mohan V. Ram
*Kavithalaya Krishnan

==Soundtrack==
Music is composed by Agosh, consisting of three people R. Anand, Gopal Rao and Shaleen. The Songs were mesmerizing.

* Harichandran Varraan - Gopal Rao
* Mundhaanai Saelai - Mano, Chitra
* Kaadhal Enbadhu - Swarnalatha
* Naadodi Paattu Paada - SPB
* Enna Idhu Kanavaa - Mano, Sujatha

 
 
 
 
 


 