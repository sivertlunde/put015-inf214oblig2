Diwan (film)
 
 
{{Infobox film
| name = Diwan
| image =
| caption =
| director = Suryaprakash
| writer = Suryaprakash Sriman Jai Sharmili Anandaraj Manorama
| producer = P. L. Thenappan
| music = S. A. Rajkumar
| distributor = Sri Raja Lakshmi Film Pvt. Ltd.
| cinematography = Vijay C Chakravarthy
| editing        = K. Thanigachalam
| released = 22 August 2003
| runtime =
| country = India Tamil
}}

Diwan is a Tamil film from South India, released in 2003. The film stars Sarathkumar, Kiran Rathod|Kiran, Vadivelu. It was directed by Suryaprakash, who previously directed Maayi with Sarathkumar. S. A. Rajkumar composed the music. Its Hindi dubbed version was released under the title "Mahabali".

==Plot==

Raghavan (Sarathkumar) and Velu (Vadivelu) work as cooks in a non-veg hotel. Raghavan loves Geetha (Kiran) who is the daughter of a rich man (Vijayan), but her father is against this love and insults him as a poor man. Raghavan challenges that he will be rich in a year. There is a subplot in the story, for Meenakshi (Sharmili), a daughter from a rich family; she is the apple of the eye of the family. Meenakshi ran home with her lover Raju, who worked as a driver to her house. He tells that his friend Dinesh will help them. However, Raju escaped because he saw a bad dream where he is killed by her family as if it will happen in reality. Due to that he vanishes. Raghavan understood her plight and helps her.

Suddenly he comes to a place where all call him as Duraisingam. Raghavan does not know who Duraisingam is. Manorama explains to him that Raghavan is the grandson of Duraisingam. The flashback is Duraisingam (Sarathkumar again) is a do-gooder who helps poor people, and he has a clash with Kaathavaraayan (Jaiprakash Reddy), a big rowdy. Duraisingam could not kill him because he promised to his wife that she will never become a widow. In the clash, Duraisingams three sons are killed, and the baby is lost. The baby happens to be Raghavan. Duraisingam surrenders himself to the police. After the flashback, Raghavan visits the jail only to know that Duraisingam had died a few years back. Kaathavarayan kidnaps Raghavan and Meenakshi. Meenakshis father and relatives bash Raghavan but leaves him after knowing the truth. Raghavan leaves Kathavarayan and says that he should live as a reformed man. Meenakshi marries the groom selected by her parents, and Raghavan unites with Geetha.

==Cast==
* Sarathkumar as Raghavan/Duraisingam Kiran as Geetha
* Vadivelu as Velu Sharmilee as Meenakshi
* Anandaraj as Meenakshis father Manorama as Duraisingams sister
* Jaya Prakash Reddy as Kaathavaraayan Sriman as Duraisingams son
* Ravi Raghavendra Uday Prakash as Chandran
* Ajay Rathnam Vijayan
* Ponnambalam
* Besant Ravi
* Thalapathy Dinesh
* Thennavan
* Manobala
* Balu Anand
* Alwa Vasu
* Madhan Bob
* Suryakanth
* Muthukaalai as Marudha Muthukaalai

==Production==
After directing Telugu film Bharatasimha Reddy with Rajasekhar (actor)|Rajasekhar, Suryaprakash announced his fourth directorial titled "Diwaan" with Sarathkumar collaborating with him for second time after Maayi.

Shooting for the film commenced at Chennai and neighbouring areas. In this film, the actor reportedly has  done yet another risky scene without the help of a double. He had to climb a high-rise building using a rope hanging from the roof. A stunt scene was shot on grandpa Sharat Kumar and about 20 stuntmen. A romantic scene and a song was picturised at the Besant Nagar Beach in Chennai, where Sharat and Kiran took part. A few songs were also shot at exotic locations in New Zealand. 

==Soundtrack==
* Ayyayyo - Tippu, Sujatha
* Gundu Pakkara - Shankar Mahadevan
* Konjam Konjama - Karthik, Anuradha Sriram
* Oru Thaalaattu - Harish Raghavendra
* Paarthathile - Sathya, SA Rajkumar

==Reviews==
The film received mixed reviews.  Lollu express criticized the film comparing it to Lion King.  Thiraipadam.com wrote:"Diwaan boasts of one of the most shoddy screenplays and atrocious editing in recent times. The movie has a lot of things going on, which is usually a good thing since it contributes to the pace of the movie. But the director fails to keep a rein on things and the movie soon spirals out of his control. There are just too many loose ends and unanswered questions". 

==References==
 

 
 
 
 
 
 