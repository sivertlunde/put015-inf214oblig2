Jeans (film)
 
 
{{Infobox film
| name           = Jeans
| image          = Jeans 1998.jpg
| imagesize      =
| director       = S. Shankar
| producer       = Ashok Amritraj Sunanda Murali Manohar
| writer          = Balakumaran  (Dialogue) 
| screenplay      = Shankar
| story           = Shankar Prashanth Aishwarya Rai Nassar
| music          = A. R. Rahman Ashok Kumar
| editing        = B. Lenin V. T. Vijayan
| studio         = Amritraj Solomon Communications Oscar Films Sri Surya Movies(Telugu) Eros Labs (U.S.) Cee I TV (UK)
| released       =  
| runtime        = 175&nbsp;min.
| country        = India
| awards         = Tamil
| budget         =  190 million   200 million 
| gross          =
}} Tamil romantic Murali Manohar, Lakshmi play Ashok Kumar and the duo B. Lenin and V. T. Vijayan handled the cinematography and editing respectively.
 Hindi and nominated by Best Foreign Language Film for the Academy Awards in 1998.   

==Plot==
Nachiappan (Nassar) is a restaurateur in Los Angeles, USA. His twin sons Vishwanathan (Visu) and Ramamoorthy (Ramu) (both played by Prashanth Thyagarajan|Prashanth), are medical students, who help him out in the evenings and alongside with the restaurants chief cook, Juno (Senthil (actor)|Senthil) enjoys on weekends.One evening, Vishwanathan goes to the airport to check on the familys supply concession and sees that some fellow Indians – Madhumitha (Aishwarya Rai), her brother Madhesh (Raju Sundaram) and their grandmother Krishnaveny (Lakshmi (actress)|Lakshmi) – have difficulty with an address. He pitches in to help and learns that they just flew in from India so that Krishnaveny can undergo a crucial surgery to remove her brain tumour.

The story shifts to the hospital where Vishwanathan, an intern, visits Krishnavenys room after the operation, and notices she has been operated on the wrong side of her brain. Vishwanathan appeals aggressively to the doctors and has the error corrected by another surgery and then spearheads an angry fight for Damages|compensation. The hospital compensates $2 million to avoid a messy court case. Viswanathans drastic measures win the respect and love of Madhumitha and her family.

When the grandmother realises that Vishwanathan and Madhumitha had fallen in love, she extends the familys stay in the United States and takes a liking to Vishwanathans good nature. However, Nachiappan objects to the budding romance and wants his sons to marry identical twins because he himself has an identical twin brother, Pechiappan (Nassar). They both had married for love in their youth, but are now estranged because Pechiappans wife Sundaramba (Raadhika Sarathkumar) showed such tyrannical behaviour towards Nachiappans wife Meiyaththa (Geetha (actress)|Geetha), that the latter died delivering the twin boys.

Krishnaveny tries to solve the problem by telling Nachiappan that Madhumitha also has an identical twin, Vaishnavi. The story spun by Krishnaveny is that Vaishnavi has been brought up in an orthodox Brahmin household. At this point they ring in Madhumithas alter ego, contrasting Madhumitha with a very demure, typically traditional Indian version. Ramamoorthy falls for the act and visits India along with his sons to meet Vaishnavi, unaware that Vaishnavi and Madhumitha are the same person. Meanwhile, Pechiappan arrives to a warm welcome by his brother, but later attempts suicide. Nachiappan then rescues him and comes to hear of his sad story and comes up with a plan. Accordingly, Nachiappan and Pechiappan switch places. Nachiappan goes to his brothers wife and impersonate as her husband to convince and reunite with her through his sons(Visu and Ramu) marriage plan talks. He succeeds. Pechiappan impersonated as his brother towards his nephews. Vishwanathan then meanwhile, figures out that Vaishnavi is Madhumitha and she was impersonating Vaishnavi. In anger, he immediately leaves Madhumithas household along with his family, but Ramamoorthy persuades his father to have Madhumitha and Vishwanathan married. Though not knowing, it was not his father he talked critically about his uncle who was right there impersonating. That made him guilty. He decided to have Vishwanathan married to Madhumitha. When Nachiappan figure out that Madhumitha doesnt have a twin, he goes and stops the wedding. There they figure out that the brothers impersonated each other as well to get along. Nachiappans brother Pechiappans wife(Sundaramba) persuades Nachiappan to have them married because Madhumitha did the same thing the brothers did. In the end, Visu and Madhu get married followed by a grand dinosaur reception specially given by Madhesh.

==Cast== Prashanth as Vishwanathan and Ramamoorthy. The twin brothers are both medical students, who assist their father run a catering business. While Vishwanathan is more fun-loving and has a relationship with Madhumitha, Ramamoorthy is more reserved and emotional, especially after being deceived by the double act of Madhumitha. The name of the roles is clearly inspired by the Music Director duo, Viswanathan-Ramamoorthy.
* Aishwarya Rai as Madhumitha and Vaishnavi. Madhumitha is a loving and optimistic young woman who leads her family to the United States seeking medical attention for her grandmother. Vaishnavi is the double act improvised by Madhumitha to deceive Nachiappan. Nassar as Nachiappan and Pechiappan Rajamani. A naive father of twin sons, who remains upbeat on re-uniting with his own twin, Pechiappan. Nachiappan Rajamani insists on his twin sons getting married to twin daughters since his brother and him were split due to the non-chemistry between the women they both married. Because of this Nachiappans wife and Visu and Ramus mother died after giving birth. Lakshmi as Krishnaveny nts. She first begins her role by playing a brain patient who has come to America with her grandkids for a surgery.
* Raadhika as Sundaramba,  Pechiappans wife, who starts the friction between the brothers.
* Raju Sundaram as Madhesh, Madhumithas energetic brother with a habit of ruining the situation due to his innocence and cracking jokes. Senthil as Juno. A cook who works at Nachiappans catering company. The character provides comic relief throughout the film, and instigates complicated situations for the brides family.
* S. V. Shekhar as Madhumithas Father
* Janaki Sabesh as Madhumithas Mother Geetha as Meiyaththa, Nachiappans Wife
* S. N. Lakshmi as Meiyaththas mother

==Production==

===Development===
The entire production of the film took about a year and a half to be completed and released. Jeans, unlike the others, was his first film which was shot outside of India for major portions of the film. The producers of the film were Ashok Amritraj, Michael Soloman and Murali Manohar, whom all made their Tamil film debuts with Jeans respectively.  The film reunited Shankar with his award-winning technical crew from his previous film Indian, whilst the cast was finalised by him after he had finalized the story. Genes was the title selected but Shankar felt it would not appeal to the masses so he used the homonym, Jeans.   
 AVM Studios Tamil filmdom.    The cast and crew of the film, wore their favourite pair of blue jeans as per request from the producers to the launch. 

===Casting=== Geetha were signed up for the film, whilst Radhika agreed to appear in a guest appearance. Another supporting role was taken by S. Ve. Sekhar after playback singer S. P. Balasubrahmanyam opted out of the role. 
 Ashok Kumar was publicised as the official cinematographer for the film. In mid-1997, film organisation FEFSI striked and in the midst of this, the films art director Thotta Tharani, a FEFSI supporter, refused to sacrifice his position in FEFSI and stopped working in Jeans.  Without much choice, Shankar signed a newcomer Bala to take over the set design and art work for the film. The films art direction is credited with both Thotta Tharani and Bala. Venki was signed up to deal with the special-effects in the film, with Jeans. The film also was assisted in graphics effects created by Pentafour Company. 

=== Filming ===
 , one of the Seven Wonders of the Ancient World seen in the film]] Las Vegas, Manhattan Beach, Malibu Lake and many scenic spots throughout California. The song Columbus was picturised at the shores of Venice Beach, California with some foreign male and female dancers. Shankar also filmed in New Jersey and New York at the World Trade Center when co-producer Michael Salomon and his wife, Luciana Balusia visited the sets of Jeans. 
 eighth wonder April Fools joke. 

==Release==

===Reception=== Telugu and Hindi. The film completed 100 days of screening in the theatres in the state of Tamil Nadu.         
 nominated by Best Foreign Filmfare Award for Best Music for A. R. Rahmans musical score. 

===Reviews===
Jeans opened to positive reviews from most critics. A reviewer from Rediff praised the lead characters Prashanth, Aishwarya Rai and Nassar as "ever dependable", whilst singling out praise for Radhika whom she describes that " with her startling cameo, sweeps the acting honours".    The reviewer praised the technical crew describing Venkys FX as a "virtual reality", Ashok Kumars cinematography as "throughout and outstanding", Raju Sundarams choreography as "memorable" and A. R. Rahmans score as "entirely hummable".  Shankars directorial attributes were described to be to a "perfect flow of narrative and a penchant for demanding and getting perfection out of every element of his cast and crew" and that the film was an "easy fit". 

The reviewer from The Indian Express called the film a "hilarious comedy" and drew significant praise to the performance of Aishwarya Rai and the music of Rahman.  Another critic, labelled Jeans as "glossy and extravagant" and comments that the "film clicks".    The lead actors were mixed with Prashanth labelled as "impressive", while Aishwarya Rai is described as "pretty" and that she "exhibits grace in dancing" but that "her performance leaves lots of room for improvement" and that she "overacts" in the character of Vaishnavi.  Nassar, Lakshmi and Raju Sundaram were all appreciated for their roles. Rahmans musical composition of the film was described as "one of his best soundtracks", whilst Vairamuthus "terrific lyrics" also stood in the song Poovukkul. 

In contrast, the film was described by the Deccan Herald as a "colossal waste" criticising Shankars story and direction and the performances of Prashanth and Lakshmi. 

==Soundtrack==
{{Infobox album |  
| Name        = Jeans
| Type        = soundtrack
| Artist      = A. R. Rahman
| Cover       =
| Released    = 1998
| Recorded    = 1998 Feature film soundtrack |
| Length      =
| Label       = New Music West Top
| Producer    = A. R. Rahman
| Reviews     =
}}
 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || "Anbea Anbea" || Hariharan (singer)|Hariharan, Anuradha Sriram || 05:13
|- 2 || Sujatha || 06:50
|- 3 || "Enake Enaka" || Unnikrishnan, Pallavi || 06:36
|- 4 || "Kannodu Kanbathellam" || Nithyasree Mahadevan || 05:09
|- 5 || "Varaya Thozhi " || Sonu Nigam, Shahul Hameed, Harini, Sangeetha || 05:52
|- 6 || "Columbus Columbus" || A. R. Rahman || 04:46
|- 7 || "Punnagayil Theemoti" || Hariharan || 02:57 
|- 8 || "Theme Music - Nisarisa" || || 02.01
|}

==Possible sequel==
Prashanth announced that he had registered the title Jeans 2 and was completing the pre-production works of a sequel to the 1998 film in November 2013. The film was set to be directed and produced by Prashanths father Thiagarajan, who revealed that production would begin in May 2014 and that they were trying to bring members of the original team back for the venture.   In January 2014, Ashok Amritraj distanced himself from the sequel and questioned the viability of the project, citing that he did not believe they had the rights to make a sequel. 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 