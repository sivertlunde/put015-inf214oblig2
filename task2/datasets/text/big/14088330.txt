The Death Kiss
{{Infobox film
| name           = The Death Kiss
| image          = Thedeathkissposter.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Edwin L. Marin
| producer       = E. W. Hammons
| writer         = Madelon St. Dennis (story) Gordon Kahn Barry Barrigan
| narrator       =
| starring       = David Manners Adrienne Ames Bela Lugosi
| music          =
| cinematography = Norbert Brodine
| editing        = Rose Loewinger
| studio         = Tiffany Pictures 
| distributor    = Sono Art-World Wide Pictures
| released       =  
| runtime        = 75 minutes
| country        = United States English
| budget         =
}}
The Death Kiss (1932) is a mystery film starring David Manners as a crusading studio writer, Adrienne Ames as an actress, and Bela Lugosi as a studio manager.  The comedy thriller features three leading players from the previous years Dracula (1931 English-language film)|Dracula (Lugosi, Manners, and Edward Van Sloan), and was the first film directed by Edwin L. Marin. 

The movie was produced by KBS Productions at Tiffany Pictures and released by Sono Art-World Wide Pictures. The film is currently in the public domain.

==Plot==
During the filming of a death scene of The Death Kiss, leading man Myles Brent is really shot and killed. Tonart Studios manager Joseph Steiner (Lugosi) is assigned to handle the situation. The studio wants to pass it off as a simple accident, but screenwriter Franklyn Drew (Manners) digs a bullet out of a wall and tells Homicide Detective Lieutenant Sheehan that it is a .38 caliber, while the guns used in the film are all .45s. 

Sheehan finds a letter in the dead mans pocket, in which Brent wrote to his lawyer that Marcia Lane (Ames), his co-star and ex-wife, would not sign a release as beneficiary of his $200,000 life insurance policy. Chalmers, an alcoholic extra with a self-admitted grudge against Brent for getting him fired as head gaffer (electrician), is spotted trying to dispose of a loaded .38, but Drew points out that the gun has not been fired. 
 rouge on it. It is a special rouge normally used by only two women. One was away on location, making Lane the prime suspect. Before another print can be made, the negative is destroyed with acid.

While snooping around on the set, Drew finds a derringer mounted inside a lamp and electrically wired to be fired remotely, but he is knocked out and the gun taken. He goes to question Chalmers, but finds him dead beside a glass of poison and a written confession. However, Drew finds several clues that make him suspicious. Through more detective work, he discovers that the new battery of Lanes car is dry, and battery fluid is poisonous. Meanwhile, Goldsmith comes to see Lane; she rejects his advances once again.

In Brents dressing room, Drew finds a letter from a love-stricken married woman named "Agnes" and a hotel room key. Later, in Steiners office, Sheehan takes Lane into custody; Drew spots a photo of a woman on the desk; the inscription reveals that Steiners wife is named Agnes. When Drew goes to the hotel, he finds out from a bellhop that Brent had been there with a woman; her husband was waiting, and the two men got into a fight.

The studio decides to finish the film (only the last, fatal scene needs to be shot), using a double for Brent and arranging for Lanes temporary release. Drew finds out from the prop man that the guns were originally supposed to be .38s, but he made an unauthorized substitution. Drew takes him to Sheehan. Just as he is about to reveal who ordered the guns, the lights go out. (The murderer had overheard the conversation through a studio microphone.) After a gunfight and chase, the killer falls to his death. It is Avery, the director.

==Cast (in credits order)==
*David Manners as Franklyn Drew
*Adrienne Ames as Marcia Lane
*Bela Lugosi as Joseph Steiner John Wray as Detective Lieut. Sheehan
*Vince Barnett as Officer Gulliver (chief of the studio police)
*Alexander Carr as Leon A. Grossmith ("president of this concern  ") 
*Edward Van Sloan as Tom Avery (the director)
*Harold Minjir as Howell Barbara Bedford as Script Girl Al Hill as Assistant Director
*Harold Waldridge as Charlie (bellhop)
*Wade Boteler as Sergeant Hilliker
*Lee Moran as Todd (publicist)

==Production==
The location of the studio in the film, "Tonart Studios", was actually the Tiffany Pictures studio.
 tinted or hand colored by artist Gustav Brock.

==References==
 

==See also==
*List of early color feature films

==External links==
*   at the Internet Movie Database 
*  

 

 
 
 
 
 
 
 
 
 
 