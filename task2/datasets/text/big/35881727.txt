Gimme the Loot (film)
 
 
{{Infobox film
| name           = Gimme the Loot
| image          = Gimme The Loot poster.jpg
| caption        = Theatrical release poster
| director       = Adam Leon
| producer       = Natalie Difford Dominic Buchanan Jamund Washington
| writer         = Adam Leon
| starring       = Ty Hickson   Tashiana Washington
| music          = Nicholas Britell
| cinematography = Jonathan Miller
| editing        = Morgan Faust
| distributor    = IFC Films
| released       =  
| runtime        = 79 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = $104,235   
}} SXSW Film Festival in 2012.  It was released on March 22, 2013 in the U.S.

==Cast==
* Ty Hickson as Malcolm
* Tashiana Washington as Sophia
* Zoë Lescaze as Ginnie
* Meeko as Champion
* Sam Soghor as Lenny
* Joshua Rivera as Rico

==Release==
After premiering at SXSW, Gimme the Loot was picked up for distribution by IFC Films/Sundance Selects in the U.S.  It was officially “presented” in its release by Oscar winning filmmaker Jonathan Demme. 

The movie made its international premiere at the Cannes Film Festival and went on to play at numerous festivals around the world, including the London Film Festival, Deauville American Film Festival, and the Los Angeles Film Festival.    It was released theatrically in France by Diaphana in January 2013, IFC Films/Sundance Selects in the U.S. on March 22, 2013 and by Soda Pictures in the U.K. in May, 2013.   

==Reception==

===Critical Response===
The film received positive reviews. Review aggregation website Rotten Tomatoes gives a score of 92% based on 59 reviews.   
Metacritic, another review aggregation website, assigned the film a score of 81%, based on 26 reviews from mainstream critics. 

Roger Ebert called it, “A remarkably natural and unaffected film about friendship between two high-spirited graffiti artists” and Peter Travers of Rolling Stone in his 3.5 star review said the movie is, “A fresh, funky jolt of filmmaking joy.”  

A.O. Scott of The New York Times wrote that, “Adam Leon’s loose and rambunctious debut… has a lot to say about the contradictions of a place that is defined by both abundant opportunity and ferocious inequality. But the film makes its points in a lighthearted, street-smart vernacular, treating its protagonists not as embodiments of a social condition but rather as self-aware individuals who are, like teenagers everywhere, both smart and dumb. Their friendship — which is based above all on shared artistic ambitions — is a perfect comic pairing.”
 

Other notable positive reviews included the Los Angeles Times, Christian Science Monitor, NPR, Entertainment Weekly, Chicago Tribune, Wall Street Journal, and Chicago Sun Times, whose Sheila O’Malley wrote, “Adam Leon has created something unique and current, with affectionate nods to New York films of the past.” 

===Box office===
Released on March 22, 2013 in the United States, Gimme the Loot opened to the highest per screen average in the country. 

===Accolades=== 2013 Independent Spirit Awards. 

Adam Leon received a Gotham Award nomination for "Bingham Ray Breakthrough Director" in 2013. 

The film also won Best Director at the Sofia Film Festival and Best Feature at the Molodist Film Festival, with lead actor Ty Hickson receiving a Special Jury Mention.  

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 