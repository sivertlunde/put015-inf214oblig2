The Last Mile (1959 film)
{{Infobox film
| name           = The Last Mile
| image          = The Last Mile 1959 film poster.jpg
| caption        = Theatrical release poster
| director       = Howard W. Koch
| producer       = Max Rosenberg Milton Subotsky
| writer         = Milton Subotsky Seton I. Miller
| based on        =  
| starring       =  Mickey Rooney
| music          = Van Alexander
| cinematography = Joseph Brun
| editing        = Robert Brockman Patricia Jaffe
| studio         =
| distributor    = United Artists
| released       = January 21, 1959
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 1932 film of the same name starring Preston Foster.

==Plot==
In a death row cell block nine inmates are scheduled for execution. Then "Killer" John Mears (Rooney) comes along. His viciousness infects the environment and his plans to break out of prison are the catalyst for tragedy.

==Cast==
* Mickey Rooney as "Killer" John Mears, Cell 3	 
* Frank Overton as Father OConnors	 
* Michael Constantine as Ed Werner, Cell 1	 
* John Vari as Jimmy Martin, Cell 2	 
* Clifford David as Richard Walters, Cell 4	 
* Harry Millard as Fred Mayor, Cell 5	  John McCurry as Vince Jackson, Cell 6	 
* Ford Rainey as Red Kirby, Cell 7	  John Seven as Tom DAmoro, Cell 8	 
* George Marcy as Pete Rodriguez, Cell 2 (later) 
* Donald Barry as Drake	 
* Leon Janney as Callahan	 
* Clifton James as Harris	 
* Milton Selzer as Peddie	  Frank Conroy as OFlaherty

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 