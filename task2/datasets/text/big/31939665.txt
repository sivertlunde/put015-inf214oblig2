Johny Mera Naam Preethi Mera Kaam
{{Infobox film
| name = Johny Mera Naam Preethi Mera Kaam
| image = Johny mera naam.jpg
| caption = 
| director = Preetam Gubbi
| producer = Jayanna Bhogendra
| writer = Preetam Gubbi
| starring = Duniya Vijay Ramya
| music = V. Harikrishna Krishna
| editing = Deepu S. Kumar
| distributor =  
| released =  
| runtime = 
| country =   Kannada
| budget =   3.5 crore
| gross =   16 crore
}} Kannada in romance genre starring Duniya Vijay and Ramya in the lead roles . The film is directed by Preetam Gubbi of Mungaru Male fame. Jayanna and Bhogendra have jointly produced this venture under Jayanna combines banner. V. Harikrishna has composed the music and a long associate, S.Krishna works as the cameraman. 

==Plot==
This is a wholesome entertainer. The Jaan of Johny Mere Naam….Preethi Mere Kaam is the wonderful performance of Ramya, Vijay, Rangayana Raghu, Dattanna plus lovely songs and cinematography add to the value of the film.

There is lot of fun, memorable songs, color of the film is quite impressive and over all Preetham Gubbi has given a winner. The set erected for this film is glorious and the computer technique for a song is glorious.

In the Gandhi Colony Johny is into deals with Rangayana Raghu and Black Bonda (Rakesh). The trio is so funny that the colony is happy with smile always. By mistake the trio accepts one of the deals and kidnap Priya (Ramya). This kidnap does not lead to various complications but Johny is bowled over by the beauty of Priya. His heart starts demanding Priya. He knows that he is very local Priya is international. The series of attempts to please Priya brings Johny very near to her. Johny takes up social service in his colony only because of Priya.

To the shock of Johny one day Priya takes him to her boy friend house. Very soon the net chatting boy friend of Priya Rakesh is disclosed as fake. Irritated with it Priya decide to leave to USA. On her way travelling in the car Priya changes her mind and accepts the true love of Johny

==Cast==
* Duniya Vijay as Johnny
* Ramya as Priya
* Rangayana Raghu as Maamu
* Sadhu Kokila as Dr.Halappa
* Dattanna as Taata
* Achyuth Kumar

==Soundtrack==
{|class="wikitable" width="70%"
! # !! Song Title !! Singers !! Lyricist
|- Priyadarshini || Kaviraj
|-
| 2 || "Yaava Seemeya" || Sonu Nigam || Kaviraj
|-
| 3 || "Shirtu Pantinali" || V. Harikrishna, Chethan Sosca || Yograj Bhat
|-
| 4 || "Yellavanu Heluvaase" || Sonu Nigam, Rangayana Raghu || Jayanth Kaikini
|-
| 5 || "Bhavalokada Rayabharige" || Shamita Malnad || Kaviraj
|-
|}

==References==
 

 
 
 
 