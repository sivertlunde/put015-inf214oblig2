Dogs is Dogs
{{Infobox film
| name           = Dogs Is Dogs
| image          = Dogs is dogs.JPEG
| image_size     =
| caption        =
| director       = Robert F. McGowan
| producer       = Robert F. McGowan Hal Roach
| writer         = H. M. Walker
| narrator       =
| starring       =
| music          = Leroy Shield Marvin Hatley
| cinematography = Art Lloyd
| editing        = Richard C. Currier MGM
| released       =  
| runtime        = 20:39
| country        = United States
| language       = English
| budget         =
}} short comedy film directed by Robert F. McGowan.    It was the 110th (22nd talking episode) Our Gang short to be released.

==Plot==
Youngsters Wheezer and Dorothy now live with their wicked stepmother (Blanche Payson) and her bratty son Sherwood — whom they derisively call "Spud." Their father seems to be long gone, though Wheezer tearfully observes that since he said hed come back for them, "I know he will." The two-tier class system among the humans in the house is reflected by its canine residents: Spuds posh police dog Nero is described by mom as "a pedigreed animal" and has the run of the house, while Wheezers dog Pete "is nothing but an alley dog" and is banned from entry.

A typical day begins with Pete coming into Wheezers bedroom through an open window, and Sherwood wastes no time telling on Wheezer, who promptly gets a spanking from six-foot-two-inch Payson. She threatens to send Pete to the pound next time he is found in the house. Wheezer then pops Spud in the face and Spud screams and cries for his "mama-mama-mama." This brings a second barrage of spanking and the threat to throw him and Dorothy into an orphanage if their "good for nothing" father does not show up soon. It also brings tender comfort for Wheezer from Dorothy and Pete, whose close-up reveals big lush tears rolling down his concerned snout. Payson then leaves to go downtown and tells Wheezer to not let Sherwood get dirty.

Outside, we pick up Stymie stopping at Petes doghouse for a chat about how hungry they both are. Stymie wistfully rhapsodizes about the spread hed put together for both of them, and we cut back repeatedly to Pete, whose mouth is watering at the mention of all the fine food.

Stymie arrives at the kitchen door, where  Wheezer and Dorothy have only mush to eat while Spud and Nero enjoy ham and eggs. Spurred by the aromas of the kitchen, Symie runs a con job on Spud, telling him that ham and eggs can talk: "I heard em talkin this mornin." To disprove it, skeptical Spud cooks up a heapin frying pan of ham and eggs, then loses interest and goes outside when the egg-to-ham dialogue fails to materialize. Stymie, Wheezer and Dorothy dig in and enjoy the feast.

Spud, squatting by the edge of a well, is pushed in — by his own dog. He sends Dickie to get Wheezer. Wheezer and Stymie, stretching and in no great hurry, stroll out "to see what the trouble is." They get Spud a rope after teasing him a little while. As they pull him up from the well, he states that hell be "telling mother about this." Wheezer drops the rope and Spud plunges back in. Then as he pulls him out again, Spud swears he will keep this a secret — until Spud gets his feet on the ground and says "I am too gonna tell Mama!" Wheezer states that the dunking Spud got will be worth the whipping hell get.

Later, Spud goes to a neighbors barn and finds that Nero has killed another chicken. He tells the owner, Mr. Brown (Billy Gilbert), that Pete killed the bird. Mr. Brown then tries to shoot Pete, but Wheezer, Dorothy, and finally a policeman (Harry Bernard), stop him. Nevertheless, Pete is sent to the pound because he is unlicensed.

At the pound, Wheezer gazes at Pete through the fence and cries until a kind lady (Lyle Tayo) asks whats the matter. Turns out she is his auntie ("Yes, I am your fathers sister") and she gives him the two dollars to spring Petey from the slammer. She then tells Wheezer and Dorothy that their "daddy has been very, very sick" and she would be taking them to live with her in a nice place. As she takes the gussied-up Wheezer, Dorothy and Pete to the chauffeured car with all their belongings, the mean stepmother gripes that their father was no good anyway and she was fed up with taking care of the children. Auntie coldly tells the stepmother, "Well, Im sure you wont be bothered anymore!" The stepmother bends over to straighten the carpet and the aunt comes back to give her a swift kick in the backside. This sends stepmother into a fit of bawling, and when Sherwood tries to comfort her, she yells at him to "oh, get into the house!"

The film closes when Wheezer says to Dorothy that "I sure hate to leave my old pal Stymie," but the final shot reveals Stymie — in a brand new suit of his own — riding comfortably in the spare tire.

==Cast==

===The Gang===
* Bobby Hutchins as Wheezer
* Sherwood Bailey as Sherwood Spud Matthew Beard as Stymie
* Dorothy DeBorba as Dorothy
* Dickie Jackson as Dickie
* Pete the Pup as Himself

===Additional cast===
* Harry Bernard as Policeman
* Baldwin Cooke as The driver
* Billy Gilbert as Mr. Brown
* Blanche Payson as Spuds mother
* Lyle Tayo as Wheezer and Dorothys aunt

==Reception==
Film critic Leonard Maltin has rated Dogs is Dogs as one of the best Our Gang films in the series. 

==Notes== AMC from 2001-2003. The complete, unedited version is currently available in VHS and DVD formats

==See also==
* Our Gang filmography

==References==
 

==External links==
* 
*  comprehensive data page at Our Gang website TheLuckyCorner.com

 
 
 
 
 
 
 
 