Arizona Raiders
{{Infobox film
| name           = Arizona Raiders
| image          =
| image_size     =
| caption        =
| director       = William Witney
| writer         =
| narrator       =
| starring       = Audie Murphy Buster Crabbe
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = $400,000 Don Graham, No Name on the Bullet: The Biography of Audie Murphy, Penguin, 1989 p 296 
| gross          =
| website        =
}} Western film starring Audie Murphy as a member of Quantrills Raiders. 

==Plot== Confederate war hero who has gone out West to join a renegade band, Quantrills Raiders, and ends up captured by the law.

Sent to prison with his friend Willie Martin, he goes along with a jailbreak plan masterminded by U.S. Army Captain Andrews to grant the men amnesty if they help the soldiers round up Quantrill and his men. Clint has an old score to settle with Montana, one of the Raiders, but plans to take off on his own until both his younger brother and his pal Willie are killed.

==Cast==
*Audie Murphy as Clint
*Michael Dante as Brady
*Ben Cooper as Willie Martin
*Buster Crabbe as Captain Andrews of the Arizona Rangers
*Gloria Talbott as Martina
*Ray Stricklyn as Danny Bonner
*George Keymas as Montana
*Fred Krone as Matt Edwards
*Willard Willingham as Eddie
*Red Morgan as Tex Fred Graham as Quantrell/Quantrill

==Production==
Murphys salary for the film was $45,000. 

==References==
 

==External links==
* 
*   at IMDB
*  at TCMDB

 
 
 
 


 