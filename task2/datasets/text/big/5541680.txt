Race to Space
{{Infobox film name        = Race to Space image       = Race to Space.jpg caption     = DVD cover for Race to Space writer      = Eric Gardner Steven H. Wilson starring    = James Woods Annabeth Gish Alex D. Linz Wesley Mann William Atherton director  Sean McNamara producer    =Sean McNamara distributor = Brookwell McNamara Entertainment released    =   runtime     = 104 minutes language    = English budget      =
|}}

Race to Space is a 2001 American family drama film. The film was shot on location at Cape Canaveral and Cocoa Beach and Edwards AFB CA in cooperation with NASA and the United States Air Force|U.S. Air Force.

==Plot==

The film takes place during the 1960s space race between the United States and the Soviet Union.

Dr. Wilhelm von Huber (James Woods), a top NASA scientist, relocates to Cape Canaveral with his 12-year-old son, Billy (Alex D. Linz). Their relationship has become strained in the wake of the recent death of Billys mother, and the ever-widening gap between father and son has become obvious.

Billy finds his father old-fashioned and boring. He wants to lead an exciting life: to be a hero like the astronaut Alan Shepard.

However, Billys life takes an exciting turn when he is hired by Dr. Donni McGuinness (Annabeth Gish), the Director of Veterinary Sciences, to help train the chimpanzees for NASA space missions. Billy begins to develop a close bond with one particular chimp named Mac. With Billys help and companionship, Mac is chosen to become the first American astronaut launched into space.

All seems like a wonderful game until Billy realizes that his new friend is being prepared to be hurled hundreds of miles into orbit on a historical mission - and that someone (William Atherton) at NASA is about to sabotage the mission. Macs big chance to explore the farthest frontier and hurtle America ahead in the race to space might easily cost him his life.

==Cast==
* James Woods
* Annabeth Gish
* Alex D. Linz
* Wesley Mann
* William Atherton

==References==

 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 

 