Break Up Club
 
 
{{Infobox film
| name           = Break Up Club
| image          = Break Up Club poster.jpg
| caption        = 
| film name = {{Film name| traditional    = 分手說愛你
| simplified     = 分手说爱你
| pinyin         = Fēn Shǒu Shuō Ài Nǐ
| jyutping       = Fan1 Sau2 Syut3 Oi3 Nei2}}
| director       = Barbara Wong
| producer       = Lawrence Cheng Barbara Wong Gus Liem
| writer         = Lawrence Cheng Barbara Wong Hayama Go Bonnie Sin
| music          = 
| cinematography = 
| editing        = Azrael Chung
| studio         = Diva Productions Limited
| distributor    = Golden Scene Co. Ltd.
| released       =  
| runtime        = 104 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$10,333,691
}} 2010 Cinema Hong Kong romance film starring Jaycee Chan and Fiona Sit. This film revolves around a website that allows users to win back lost loves, so long as they agree to break up another happy couple.

==Reception==
The film received generally positive reviews from the Hong Kong media.

Perry Lam of Muse (Hong Kong Magazine)|Muse Magazine writes, Wong juggles a mixed bag of styles, including mockumentary and DIY video, and conjures up a teen romance that tries to tug at your heartstrings one moment, and make you laugh your head off the next. 

==Cast==
{| class="wikitable"
|-
! width=50% | Cast
! width=50% | Role
|-
| Jaycee Chan || Joe Chan
|-
| Fiona Sit || Flora
|-
| Patrick Tang || Sunny
|- Hayama Go || Lies Hayama
|-
| Bonnie Xian || Fanny
|-
| Lawrence Cheng || Himself
|-
| Barbara Wong || Herself
|-
| Gus Liem || Himself
|-
|}

==References==
 

==External links==
*  
*  
*  
*  
*   at Hong Kong Cinemagic

 

 
 
 
 
 
 


 