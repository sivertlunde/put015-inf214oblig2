Atlantis (1913 film)
{{Infobox film
| name           = Atlantis
| image          = Atlantis1913poster.jpg
| caption        = 1913 film poster by Aage Lund
| director       = August Blom
| producer       =
| writer         = Karl Ludvig Schroder Axel Garde Gerhart Hauptmann (book)
| starring       = Olaf Fønss Ida Orloff
| music          =
| cinematography = Johan Ankerstjerne
| editing        = Nordisk Film Kompagni
| released       =  
| runtime        = 113 minutes
| country        = Denmark Danish intertitles
| budget         =
}}

Atlantis is a 1913 Danish silent film directed by August Blom, the head of production at the Nordisk Film company, and was based upon the 1912 novel by Gerhart Hauptmann. It starred an international cast headlined by Danish matinée actor Olaf Fønss and Austrian opera diva Ida Orloff. The film was the first Danish multi-reeled feature film. The story, which tells the tale of a doctor who travels to the United States in search of a cure for his ailing wife, includes the tragic sinking of an ocean liner after it strikes an object at sea. Released only one year after the sinking of the RMS Titanic, the movie drew considerable attention as well as criticism due to similarities to the actual tragedy.

The high production costs for Atlantis were not equaled by box office returns at that time. However, the film went on to become the most watched film for Nordisk Film and has been hailed by film historian Erik Ulrichsen as a Danish masterpiece and "one of the first modern films." {{cite book
| last = Ulrichsen
| first = Erik
| authorlink =
| title = Introduction to Atlantis
| publisher = Det Danske Filmmuseet
| year = 1958
| location = Copenhagen
| pages =
| url =
| doi =
| id =
| isbn = }}
 

==Plot==
Dr. Friedrich von Kammacher (Olaf Fønss), a surgeon, is devastated after his wife develops a brain disorder and is institutionalized. On the advice of his parents, von Kammacher leaves Denmark to gain some respite from his wifes illness. Von Kammacher travels to Berlin, where he meets a young dancer named Ingigerd (Ida Orloff) and the doctor becomes fond of her and very interested in her. However she has a large amount of admirers and thus Von Kammacher gives up on her. However, while in Paris he sees an ad in the paper that she is going to New York with her father and decides to follow her. Von Kammacher buys a first ticket on the same liner as Ingigered, the SS Roland.

Aboard the ship, von Kammacher learns Ingigerd has a boyfriend with her and thus he backs down. Shortly after, he is called to treat a young Russian girl with seasickness and they nearly get romantically involved but class stops this from happening.

Halfway across the sea the Roland strikes an unseen object which causes massive flooding and dooms the ship. The passengers panic as the ship sinks into the Atlantic. Von Kammacher finds Ingigered passed out in her cabin from shock and carries her to a lifeboat. He goes back and searches in vain her father but when he cant find him, von Kammacher returns to the lifeboat and holds Ingigereds hand as the lifeboat pulls away. They watch in horror as the Roland sinks into the ocean. The liner sinks so rapidly that many of the lifeboats are never launched and several passengers are swept into the sea and drowned. By morning, only von Kammachers lifeboat is still floating (the rest having been swamped by swimmers) and 8 still alive. They are spotted by a cargo liner and saved. Ingigerd is devastated when she is told that there are no more survivors and both her father and boyfriend have drowned.

Von Kammacher and Ingigerd arrive in New York and she is unable to continue with her career since she is still shocked over the Roland disaster. Von Kammacher tries to tell her that he loves her and wants a life with her in New York but she refuses to be tied down by one man. He gives up on her and they go their separate ways after she turns down his offer to live with him in New York. Von Kammacher is impressed by an art gallery and takes an interest in fine art. Through the artistic community, he is introduced to a kind and pleasant sculptor named Eva Burns, and they develop a friendship. A New York doctor, who is a friend of von Kammacher, offers him the use of a mountain cabin, where it is hoped that Friedrich will find some peace and solace. While he is in the mountains, a telegram from Denmark arrives in New York with information that von Kammachers wife has died. Upon hearing the news, Friedrich falls critically ill. Eva takes it upon herself to tend to him in the mountain cabin. As she nurses him back to health, their relationship blossoms. Happiness returns to Friedrichs life as he realizes Eva will be a good mother for his children.

==Production== Hungarian director White Christmas. Curtiz also appeared in Atlantis in a small supporting role.

For the filming of the shipboard scenes, Nordisk Film chartered the Norwegian steamship C.F. Tietgen which had been taken out of service that year. However, the Atlantis sinking scene used a large-scale model and about 500 extras as swimmers, and was filmed in the bay off Køge, Denmark. Pedersen, Sune Christian,  , Post & Tele Museum of Denmark, 3rd Quarterly, (2001) 

Blom filmed two endings for the movie—one happy and one tragic. The alternate tragic ending, in which the Doctor dies at the end, was made in particular for the Russian market. It was believed that the Russians had a preference for sad endings. 

===Titanic myth===
Some sources claimed that the sinking ship scenes were inspired by the RMS Titanic|Titanic sinking which had occurred the previous year. However, Blom based his film entirely on Gerhardt Hauptmanns 1912 novel, Atlantis. Hauptmanns novel was published in serialized form in the Berliner Tageblatt a month before the Titanic disaster.   Nevertheless, due to the film’s release only one year after the Titanic sinking, Atlantis became associated with the Titanic. In Norway, the film was banned because authorities felt it was in poor taste to turn a tragedy into entertainment. 

== Cast ==

Because his original story was partly autobiographical, Hauptmanns contract with Nordisk Film required two roles be acted by the actual people who were their inspiration. A letter from Nordisk Film to Fred  A. Talbot of England explained, "...the part of "Ingigerd" and the armless virtuoso "Arthur Stoss" were played by the very same persons, who  , so to speak, used as models by Gerhart Hauptmann, when he wrote his famous novel. These two artists, Miss Ida Orloff and Mr. C. Unthan (these are their real names)  were his traveling companions on the trip across the Atlantic." Orloff had had a romantic relationship with Hauptmann beginning several years earlier when she was a 16-year-old cabaret dancer. Reviewers of Atlantis criticized the choice of Orloff because, by the time of filming, she was no longer a svelte athletic dancer who could embody the eroticism of the part. Nevertheless, Nordisk Films had been forced to cast her. 

The other required actor was Carl Herman Unthan (credited as Charles Unthan), a Prussian violinist who had been born without arms and learned to use his feet as hands. He played the role of Arthur Stoss, an armless virtuoso. Although his abilities were impressive, critics of Atlantis felt his appearance in the film was simply extraneous and non-integral to the story. 

Another notable actor in Atlantis was Torben Meyer who went on to a long Hollywood career as a comedic character actor, usually playing a thick-accented waiter or maitrde. Some film historians have also spotted Danish comedian Carl Schenstrøm, later the tall half of the Pat and Patachon comedy team, playing a waiter in the film. Although Schenstrøm was employed at Nordisk Film at the time, his participation has not been fully established. 

{| class="wikitable" |- bgcolor="#CCCCCC"
!   Actor !! Role
|-
| Olaf Fønss || Dr. Friedrich von Kammacher
|-
| Ida Orloff || Ingigerd Hahlstroem, artistic danser
|-
| Ebba Thomsen || Eva Burns, sculptor
|-
| Carl Lauritzen || Dr. Schmidt
|-
| Frederik Jacobsen || Dr. Georg Rasmussen
|- Charles Unthan || Arthur Stoss, armless virtuoso
|-
| Torben Meyer || Willy Snyders, artist
|-
| Cajus Bruun || Friedrichs Father
|-
| Michael Curtiz || Hans Fuellenberg, Friedrichs Colleague (credited as Michael Curtiz|Mihály Kertész)
|- Marie Dinesen || Friedrichs Mother
|-
| Lily Frederiksen || Angle, Friedrichs Wife
|-
| Thomas P. Hejle || Office Worker
|-
| Alma Hinding || Russian Immigrant
|-
| Musse Kornbech || Young Canadian Woman
|-
| Svend Kornbeck || Ships Captain
|-
| Bertel Krause || Artists Agent
|-
| Emilie Otterdahl || Lady at Fancy Dress Ball
|- Albrecht Schmidt || Evas Father
|-
| Christian Schrøder || Ingigerds Father
|-
| Franz Skondrup || Stosss Waiter/Helper
|-
| Alfred Stigaard || Crewman Wilhelm
|}

==Restored version==
Atlantis was restored and released on laserdisc in 1993 and in DVD format in 2005. The restoration was created through a high definition scan of a restored negative and the tinting was recreated using an abbreviated version from The National Film Center in Japan. {{cite web
|title=Atlantis DVD
|publisher=Det Danske Filminstitut
|url=http://dfishop.filerelay.com/Shop/ItemList.php?CategoriSelect=6
|accessdate=2008-05-21
| archiveurl= http://web.archive.org/web/20080409004844/http://dfishop.filerelay.com/Shop/ItemList.php?CategoriSelect=6| archivedate= 9 April 2008  | deadurl= no}}  Danish and Russian audiences and the 15-minute surviving fragment of August Blom’s and Holger Madsen|Holger-Madsen’s 1914 film Liebelei.

== Notes ==
 

== External links ==
 
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 