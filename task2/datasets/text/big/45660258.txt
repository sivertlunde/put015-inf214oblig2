Manasella Neene
{{Infobox film name           = Manasella Neene image          =  director       = Mugur Sundar producer       = Mahadevamma Sundar writer         = M. S. Raju (based on Manasantha Nuvve) starring       = Nagendra Prasad   Gayathri Raguram music          = Ravi Raj cinematography = V. K. Kannan   editing        = Basavaraj Urs distributor    =  studio         = Malai Madeshwara Cine Theaters released       =     runtime        = 156 min. country        = India language       = Kannada
}}
 Kannada romance film directed by Mugur Sundar, a popular choreographer marking his debut in film direction. The film stars Nagendra Prasad and Gayathri Raguram in the lead roles with Prabhudeva and Ananth Nag in other pivotal roles. 
 Telugu film Manasantha Nuvve  starring Uday Kiran and Reemma Sen. It was also remade in Hindi as Jeena Sirf Merre Liye and in Tamil as Thithikudhe. 

==Cast==
*Nagendra Prasad
*Gayathri Raguram
*Prabhudeva
*Ananth Nag
*Srinath Ambika
*Chithra Shenoy
*Mugur Sundar
*M.N Lakshmi Devi
*Karibasavaiah
*Suja Raghuram

==Soundtrack==
The films score and soundtrack was composed by Ravi Raj. Four songs were retained as the same tune from the original version by R. P. Patnaik. 

{{tracklist
| headline        = Track-list
| extra_column    = Singer(s)
| lyrics          = Yes
| total_length    = 
| title1          = Phala Phala Holeyuva
| extra1          = K. S. Chithra
| lyrics1         = Nagathihalli Chandrashekar
| length1         =
| title2          = Jheer Jimbe Jheer Jimbe
| extra2          = Nanditha, Archana Udupa
| lyrics2         = Belur Ramamurthy
| length2         = 
| title3          = Karaguthiro Ondu Kanasanthe
| extra3          = Lata Hamsalekha
| lyrics3         = M. L. Prasanna
| length3         = 
| title4          = Preethiye Ninna 
| extra4          = Swarnalatha, Rajesh Krishnan
| lyrics4         = V. Manohar
| length4         = 
| title5          = Jheer Jimbe Jheer Jimbe
| extra5          = Rajesh Krishnan 
| lyrics5         = Belur Ramamurthy
| length5         = 
| title6          = Arathi Arathi
| extra6          = Rajesh Krishnan
| lyrics6         = Belur Ramamurthy
| length6         = 
| title7          = Chakumaki Chitte
| extra7          = Gurukiran, Anuradha Sriram
| lyrics7         = Hamsalekha
| length7         = 
| title8          = Elliruve Neenu
| extra8          = Hemanth, Archana Udupa
| lyrics8         = V. Manohar
| length8         = 
| title9          = Prema Prema
| extra9          = Ramesh Chandra
| lyrics9         = M. L. Prasanna
| length9         = 
| title10          = Karaguthiro Ondu Kanasanthe
| extra10          = Ravi Raj
| lyrics10         = M. L. Prasanna
| length10         =
}}

==References==
 

==External links==
* 
* 


 
 
 
 
 
 
 


 