Ithente Neethi
{{Infobox film
| name           = Ithente Neethi
| image          = Neethinew.png
| caption        = Shankar in a scene from Ithente Neethi
| director       = J. Sasikumar
| producer       = Sreelakshmi Creations
| writer         = Salim Cherthala Shankar Rajya Rajya Lakshmi Shari Jose Jose Balan K. Nair Bahadoor V N Raj Kuthiravattam Pappu Johnson
| cinematography = Thara
| editing        = G Venkittaraman
| studio         = Sreelakshmi Creations
| distributor    = Sreelakshmi Creations
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         = 20 Lakhs
| gross          =
}}
 1987 Cinema Indian feature directed by Shankar in Rajya Lakshmi Shari playing other important roles.    The film is the remake of Bollywood hit, Teri Meherbaniyan.

==Plot==

Ithente Neethi is the story of a faithful dog and his love for his master, who was brutally killed by the villain. The film is the tale of revenge done by the dog on his masters killers.

== Cast ==
 Shankar
*Rajya Rajya Lakshmi Shari
*Jose Jose
*Balan K. Nair
*Bahadoor
*V N Raj
*Kuthiravattam Pappu

==Soundtrack== Johnson and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Changaathi arinjuvo  || Vani Jairam || Poovachal Khader || 
|-
| 2 || Ekaantha Theera Bhoomiyil || K. J. Yesudas || Poovachal Khader || 
|-
| 3 || Swaram Manassile Swaram || P Jayachandran, Lathika || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 
 


 