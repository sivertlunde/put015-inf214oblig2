Ishqedarriyaan
{{Infobox film
 | name = Ishqedarriyaan
 | image = 
 | caption = 
 | director = V. K. Prakash
 | producer = Rajesh Banga
 | writer = 
 | dialogue = 
 | starring = Mahaakshay Chakraborty Evelyn Sharma Mohit Dutta
 | music = Jeet Gannguli Jaidev Kumar Bilal Saeed
 |cinematography=
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released =15 May 2015
 | runtime = 135 minutes
 | country = India
 | language = Hindi
 | budget = 
 | preceded_by = 
 | followed_by = 
 }}

Ishqedarriyaan is an upcoming 2015 Bollywood Romantic film directed by V. K. Prakash and produced by Rajesh Banga, starring Mahaakshay Chakraborty, Evelyn Sharma and Mohit Dutta.   It has been slated to release on May 8, 2015.

==Plot==
Ishqedarriyaan is a story about love, sacrifice, family values and relationships. Rishteydarriyaan means relationships and Ishqedarriyaan signifies the relationship when you fall in love! The movie stars Mahaakshay Chakraborty, Evelyn Sharma and Mohit Dutta. Evelyn Sharma is the lead actress, who will play the character of Luvleen, a teacher by profession who wants to collect donations for her grandfather’s school. Mahaakshay Chakraborty plays Aagam Diwan, a millionaire who loves his profession more than anything else. Mohit Dutta will play Arjun, a passionate singer.

The movie is releasing on 15th May, 2015.

==Cast==
* Mahaakshay Chakraborty as Agam Diwan
* Evelyn Sharma as Luvleen
* Mohit Dutta as Arjun
* Kavin Dave as Rahul
* Suhasini Mulay as Dadi
* Ravi Khemu as Arjuns father

==Crew==
*DIirector : V. K. Prakash
*Music : Jeet Ganguly, Jaidev Kumar and Bilal Saeed
*Singers : Arijit Singh, Ankit Tiwari, Mohit Chauhan, Bilal Saeed, Master Saleem and Asses Kaur 
*Lyrics : Kumaar, Kausar Munir, Manoj Muntashir and Bilal Saeed
*Background Score : Jaidev Kumar

==Promotions==
The cast of ‘Ishqedarriyaan’ has been doing promotions across various cities in India.Lead actors Mahaakshay Chakraborty,Evelyn Sharma along with singer Asees were present.  
Mahaakshay Mimoh Chakraborty & Evelyn Sharma for promotions in:
* Indore
* Bhopal
* Pune ( Central Mall) 
* Nagpur
* Raipur  
Mahaakshay Chakraborty and Evelyn Sharma were present at an event held to launch the Ishqedarriyaan trailer,in Mumbai on 7 April 2015.Many celebrities & big personalities namely Ajaz Khan,Smita Thackeray, Mohit Dutta & Kevin Dave were also present at the trailer launch. 

==Soundtrack==
{{Infobox album
| Name = Ishqedarriyaan
| Type = Soundtrack
| Artist =  
| Released =  
| Length =   Feature Film Soundtrack Zee Music Company
| Last Album = }}
The songs of Ishqedarriyaan are composed by Jeet Ganguly, Jaidev Kumar and Bilal Saeed, while the lyrics are written by Kumaar, Kausar Munir, Manoj Muntashir and Bilal Saeed.

===Track Listing===

{{track listing
| extra_column=Singer(s)
| lyrics_credits=yes
| music_credits=yes
| total_length =  
| title1= Judaa
| extra1= Arijit Singh
| lyrics1= Kumaar
| music1= Jaidev Kumar
| length1=5:25
| title2= Mohabbat Yeh
| extra2= Bilal Saeed
| lyrics2= Bilal Saeed
| music2= Bilal Saeed
| length2= 04:14
| title3= Ishqedarriyaan (Title Song)
| extra3= Ankit Tiwari
| lyrics3= Kausar Munir 
| music3= Jeet Ganguly
| length3= 04:40
| title4= Mohabbat Yeh (Reprise version)
| extra4= Asees kaur
| lyrics4= Bilal Saeed
| music4= Bilal Saeed, Bloodline
| length4= 03:31
| title5= Das Dae
| extra5= Mohit Chauhan
| lyrics5= Kumaar
| music5= Jaidev Kumar
| length5= 03:54
| title6= Georgia Se Jalandhar
| extra6= Master Saleem
| lyrics6= Manoj Muntashir
| music6= Jeet Ganguly
| length6- 02:38
}}

==References==
 

==External links==
*  
*  
*  
*  
 
 
 
 
 
 


 