Beongeoli Sam-ryong
{{Infobox film name            = Beongeoli Sam-ryong image           = 
| film name      = {{Film name hangul          =     hanja           = 벙어리   rr              = Beongeori Samryong mr              = Pŏngŏri Samryong}} caption         = Na Woon-gyu in Beongeoli Sam-ryong (1929) director        = Na Woon-gyu producer        = Baek Chan-gi or Na Woon-gyu writer          = Na Woon-gyu starring        = Na Woon-gyu (Beongeori Samryong); Yu Shin-bang; Ju Sam-son (Oh Saeng-weons son); Lee Geum-yong (Oh Saeng-weon) ;  and 1,000 extras  music           = cinematography  = Son Yong-jin editing         =  distributor     = Na Woon Kyu Productions released        =   runtime         = (1,572 ft) language  Korean intertitles budget          = 3,000 won
}}
Beongeoli Sam-ryong (Deaf Sam-ryong) is a 1929 Korean film written, directed, produced by and starring Na Woon-gyu (1902-1937). It premiered at the Choseon Theater in January 1929. It was the fifth film produced by Na Woon-gyu Productions, and its failure with the public was blamed for the bankruptcy of that company.

== Plot summary ==
The plot concerns Sam-ryong, a deaf servant who is in love with his landlords daughter-in-law. Critics praised the final scene of the film, in which the house burns, as a work of pioneering and experimental film making.

== 1964 remake == 1964 Shin Deaf Sam-yong, Grand Bell (Daejong) Award in 1965 for Best Picture. 

== References ==
*   The Korean Film Archive (KOFA)
*    

== External links == The Korean Film Archive (KOFA)

==See also==
* List of Korean language films
* Cinema of Korea
* List of Korea-related topics

 
 
 
 
 
 


 
 