The Scientific Cardplayer
{{Infobox film
| name = The Scientific Cardplayer Lo Scopone Scientifico
| image = TheScientificCardplayer.jpg
| caption =
| director =Luigi Comencini
| writer = Rodolfo Sonego
| starring = Alberto Sordi, Silvana Mangano, Bette Davis
| music = Piero Piccioni
| cinematography = Giuseppe Ruzzolini
| editing =  Nino Baragli
| producer = Dino De Laurentiis
| country = Italy
| released =  
| runtime = 115 minutes
| language = Italian 
}}
 Italian drama film directed by Luigi Comencini. The screenplay was written by Rodolfo Sonego.

==Synopsis==
An aging   on her parents behalf.

==Production notes==
The films Italian title is Lo scopone scientifico.

Bette Davis was in the midst of a three-week vacation at the health spa La Costa in Carlsbad, California when she received the script. On twenty-four-hour notice she flew to Rome for filming. It wasnt until the first day of shooting she learned the dialogue was to be recorded in Italian language|Italian. Of her co-star she said, "My name for Albert Sordi was Albert Sordid. It was unforgivable of him to refuse to speak English with me, especially as he spoke very good English."    

This was the third on-screen pairing for Davis and Joseph Cotten. They previously co-starred in Beyond the Forest (1949) and Hush… Hush, Sweet Charlotte (1964).

==Principal cast==
*Bette Davis ..... The Millionairess
*Joseph Cotten ..... George
*Alberto Sordi ..... Peppino
*Silvana Mangano ..... Antonia
*Antonella Demaggi .....  Cleopatra
*Mario Carotenuto .....  The Professor
*Domenico Modugno ..... Righetto

==Principal production credits== Producer ..... Dino De Laurentiis Original Music ..... Piero Piccioni
*Cinematography ..... Giuseppe Ruzzolini Art Direction ..... Luigi Scaccianoce
*Costume Design ..... Bruna Parmesan

==Critical reception==
TV Guide says, "Ones enjoyment of this picture is correlated with ones understanding of scopa."  

==Awards== David di Donatello Award for Best Actor (Alberto Sordi, winner)
*1973 David di Donatello Award for Best Actress (Silvana Mangano, winner)
*1973 Italian National Syndicate of Film Journalists Nastro dArgento for Best Supporting Actor (Mario Carotenuto, winner)

==See also==
 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 