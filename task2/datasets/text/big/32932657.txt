The Breaking of the Drought
 
{{Infobox film
| name           = The Breaking of the Drought
| image          = 
| image_size     =
| caption        = 
| director       = Franklyn Barrett
| producer       = Franklyn Barrett Percy Rea
| writer         = Percy Rea (as "Jack North")
| based on  =  play by Bland Holt and Arthur Shirley
| narrator       =
| starring       = 
| music          =
| cinematography = Franklyn Barrett
| editing        = 
| studio = Golden Wattle Film Syndicate
| distributor    = 
| released       = 19 June 1920
| runtime        = 6 reels
| country        = Australia
| language       = Silent film  English intertitles
| budget         =
| preceded_by    = 
| followed_by    = 
}}

The Breaking of the Drought is a 1920 Australian silent film from director Franklyn Barrett based on the popular play by Bland Holt and Arthur Shirley.

==Plot==
Drought causes Jo Galloway to lose possession of Wallaby Stationn to the bank. He moves to the city with his wife and daughter Marjorie to stay with his son Gilbert only to discover that Gilbert has been embezzling family funds, and fallen in with conman Varsy Lyddleton and femme fatal Olive Lorette.

Lyddleton murders Olive then kills himself. Marjories sboyfriend Tom Wattleby saves Gilbert from a bushfire, just as the drought breaks, restoring the familys fortunes.

==Cast==
*Trilby Clark as Marjorie Galloway
*Dunstan Webb as Tom Wattleby
*Charles Beetham as Jo Galloway
*Marie La Varre as Olive Lorette John Faulkner as Varsy Lyddleton
*Rawdon Blandford as Gilbert Galloway
*Nan Taylor as Mrs Galloway
*Arthur Albert as Walter Flour
*Ethel Henry as Molly Henderson

==Original Play==
{{Infobox play
| name       = The Breaking of the Drought
| image      = 
| image_size = 
| caption    = 
| writer     = Bland Holt Arthur Shirley
| characters =
| setting    = 
| premiere   = 26 December 1902
| place      = Lyceum Theatre, Sydney
| orig_lang  = English
| subject    = 
| genre      =
}}
The film was based on a 1902 Australian play written for Bland Holt by English playwright Arthur Shirley.

===Synopsis===
In 1902, at drought-stricken Wallaby Station in New South Wales, a squatter, Jo Galloway, lives with his wife and daughter Marjorie while his son Gilbert trains to be a doctor in Sydney. Gilbert falls in with bad company, in the shape of financier Varsey Lyddleton, who encourages him to forge his fathers name on some cheques and ruins his family. A neighbouring squatter, Tom Wattleby, who loves Marjorie Galloway, returns from a trip to India to find the father working as a lamp cleaner and the daughter was a maid. The neighbour rescues the family and the father swears vengeance on his son. However during a bush fire that ends in a heavy rain that breaks the drought, the hero rescues Gilbert.

===Reception===
The play made its debut at the end of 1902 and was very popular. Audiences and critics were particularly impressed by the stage design, which included things like real horses, recreations of Paddys Market, swimming pools and real trees. 

Annette Kellerman appeared in a 1903 production. 

Holt later adapted another play of Shirleys, The Path of Thorns, to an Australian setting, calling it Besieged in Port Arthur. 

==Production==
Bland Holt had refused offers to film his play for a number of years until approached by Barrett and Percy Rea. 

Shooting began in December 1919 in Narrabri and Moree, New South Wales|Moree, with interiors filmed at a temporary studio at the Theatre Royal in Sydney.

An additional sequence was shot consisting of a water ballet and a diving display by "water nymphs", shot in the National Park near Sydney. This sequence is missing from most versions of the film. 

==Reception==
Female lead Trilby Clarke went to the US after filming and worked in theatre and movies.  
 

==Controversy==
A New South Wales MP, Mr Wearne, asked questions in  parliament complaining that the films depiction of drought could create a bad impression overseas. An investigation was launched by the Chief Secretarys office, who later assured Wearne that new legislation meant that the export of the film could be banned by the Minister of Customs if he deemed it to be "harmful to the Commonwealth". Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 97. 

==Lost Film==
The film was thought lost until 1976, when a several rusty film cans containing it were found under a house in Hornsby. 

==References==
 

==External links==
* 
*  at Australian Screen Online
 

 
 
 
 
 
 