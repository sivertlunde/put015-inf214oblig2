Open Your Eyes (1997 film)
{{Infobox film
| name = Open Your Eyes
| image = Abre los ojos movie.jpg
| caption = Theatrical poster
| director = Alejandro Amenábar
| producer = Fernando Bovaira José Luis Cuerda
| writer = Alejandro Amenábar Mateo Gil Eduardo Noriega Penélope Cruz Chete Lera Najwa Nimri
| music =Alejandro Amenábar Mariano Marín
| cinematography =  
| editing =
| studio = Redbus Film Distribution
| distributor = Live Entertainment
| released =  
| runtime = 117 minutes
| country = Spain France Italy   
| language = Spanish
| budget = ESP 370 million
}} Eduardo Noriega, Penélope Cruz, Fele Martínez and Najwa Nimri. In 2002, Open Your Eyes was ranked No. 84 in the Top 100 Sci-Fi List by the Online Film Critics Society.  The movies intersecting planes of dream and reality have prompted some critics to suggest comparisons to Pedro Calderón de la Barca|Calderóns masterwork Life Is a Dream ( , 1635).    

An American remake entitled Vanilla Sky, directed by Cameron Crowe, was released in 2001, with Penélope Cruz reprising her role.

==Plot==
A young, handsome man wakes up to a female voice telling him to open his eyes. He drives to an empty city. He wakes up again, this time to a woman in his bed. He tells her not to leave messages on his alarm.

From a prison cell in  ), the girlfriend of his best friend Pelayo (Fele Martínez). Later on, he takes her home and stays the night, although they do not sleep together. The next morning, Césars obsessive ex-lover Nuria (Najwa Nimri) pulls up outside Sofías flat, offering him a ride and sex. On the way to her home, however, she crashes the car with the intent to kill them both. César is horribly disfigured, beyond the help of cosmetic surgery. Sofía cannot bear to see him like this and goes back to Pelayo.

After Césars disfigurement, he begins to have a series of disorienting experiences. Drunk, César falls asleep in the street. On awakening, everything has changed: Sofía now claims to love him and the surgeons restore his lost looks. But as he makes love to Sofía one night, she changes into Nuria. Horrified, César smothers her with a pillow, yet finds everyone else believes Nuria was indeed the woman everyone else calls Sofía.

While he is confined to the prison, fragments of his past return to him as if in a dream. It is revealed that, shortly after his disfigurement, César contracted with Life Extension, a company specializing in cryonics, to be cryogenically preserved and to experience extremely lucid and lifelike virtual reality dreams. Returning to their headquarters, under supervision by prison officers, he discovers they specialise in cryonics with a twist: "artificial perception" or the provision of a fantasy based on the past to clients who are reborn in the future. He had committed suicide at home after sleeping drunk on the street, and was placed in cryonic suspension. His experiences from about the midpoint of the movie onward have been a dream, spliced retroactively into his actual life and replacing his true memories. At the end of the film he elects to wake up and be resurrected. Convinced his life since the drunken night in the street is simply a nightmarish vision created by Life Extension, César leaps from the roof of the companys high-rise headquarters, and wakes to a female voice telling him to relax and open his eyes.

==Cast== Eduardo Noriega as César; a young confident man, who has money and success with women. The day after his birthday party, the main character gets into a car accident which destroys his face. After such a trauma, he will never be the same again, and it is precisely this new complex that will rule his life from then on.
*Fele Martínez as Pelayo; Césars best friend, whose personality is marked by his inferiority complex when it comes to meeting women.
*Penélope Cruz as Sofía; the woman César falls in love with right before his car accident.
*Najwa Nimri as Nuria; Césars ex-girlfriend, who still has feelings for him. She will be the one to cause the car accident that ends her own life and Césars beauty.
* Chete Lera as Antonio; a psychologist who tries to help César recover from his mental traumas.

==Reception==
Critical reaction to Open Your Eyes has been positive.  James Berardinelli of ReelViews gave the film three and a half stars (out of four), saying that movies "of this intelligence, audacity, and complexity come along so rarely that its mandatory to cry out their arrival" and that "those who see it will not quickly forget the experience."  Rob Blackwelder of SplicedWire gave the film four stars (out of four), calling it "a jaw-dropping psychological thriller" thats "beautifully orchestrated." 

Richard Scheib of The Science Fiction, Horror and Fantasy Film Review also gave Open Your Eyes four stars, calling it "quite a remarkable film."  Holly E. Ordway of DVD Active said, "I don’t give out “perfect 10” ratings lightly, but Open Your Eyes earns one by all accounts."  However, Aaron Beierle of DVD Talk gave a more lukewarm review, saying that he "found most of Open Your Eyes interesting" but remarked that "theres something about the picture that kept me from being completely involved." 

==American remake== Jason Lee New York. The film follows the original plot very closely but makes several changes to the ending.

==Soundtrack==
;Vol. #1
#"Glamour" - Amphetamine Discharge (6:03)
#"Risingson" - Massive Attack (5:29) Chucho (5:17)
#"How do" - Sneaker Pimps (5:04)
#"Sick of you" - Onion (4:28)
#"T-sebo" - Side Effects (5:44)
#"Flying away" - Smoke City (3:50)
#"Arrecife" - Los Coronas (3:11) If (3:37)
#"Tremble (goes the night)" - The Walkabouts (5:02) Chucho / Side Effects (5:01)

;Vol. #2 (Instrumental)
#"Abre los ojos" (2:28)
#"Sofía" (1:12)
#"Soñar es una mierda" (1:04)
#"La operación" (1:33)
#"¿Dónde está Sofía?" (0:54)
#"El parque" (3:08)
#"Hipnosis" (2:20)
#"Quítate la careta" (2:56)
#"Eres mi mejor amigo" (2:41)
#"La única explicación" (2:05)
#"Quiero verte" (6:38)
#"Esa sonrisa" (0:53)
#"Deja vu" (1:51)
#"Excarcelación" (4:28)
#"La vida" (1:46)
#"La azotea" (5:21)
#"Créditos finales" (3:31)

== See also ==
 
* Life Is a Dream (La vida es sueño)
* Lucid dream
* Simulated reality in fiction
* Twist ending
* Ubik
* Vanilla Sky

== References ==
 

== External links ==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 