The Image (short film)
{{Infobox film
| name           = The Image
| image          = The Image Ticket Stub.png
| caption        = Ticket stub Michael Armstrong
| producer       = Olive Negus-Fancey
| writer         = Michael Armstrong Michael Byrne
| music          = Noel Janus
| cinematography = Ousama Rawi
| editing        = Julian Hindson
| distributor    = Border Film Productions
| released       = 1969
| runtime        = 14 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 Michael Armstrong Michael Byrne and David Bowie in his first film role. The film is one of the few short films ever to receive a certified History of British film certificates#1951–1970|X Rating and it gained this rating due to its violent content.

==Plot==
A troubled artist (Michael Byrne) is haunted by a ghostly young man (David Bowie) who appears to step right out of one of the artists paintings. From a brief summary located on the cover of the films script, it reads: a study of the illusionary reality world within the schizophrenic mind of the artist at his point of creativity.   

==Cast==
* David Bowie as The Boy Michael Byrne as The Artist

==References==
 

==External links==
*  
*  
*  

 
 
 
 