The Rats (1921 film)
{{Infobox film
| name           = The Rats
| image          = 
| image_size     = 
| caption        = 
| director       = Hanns Kobe 
| producer       =  Grete Ly
| writer         = Gerhart Hauptmann (play)   Julius Sternheim
| narrator       = 
| starring       = Emil Jannings   Lucie Höflich   Eugen Klöpfer   Marija Leiko
| music          = 
| editing        =
| cinematography = Karl Freund
| studio         = Grete Ly-Film 
| distributor    = Terra Film 
| released       = 29 July 1921
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent drama The Rats a 1955 film.

The films art direction was by Robert Neppach.

==Cast==
* Emil Jannings as Bruno 
* Lucie Höflich as Frau John 
* Eugen Klöpfer as Herr John 
* Marija Leiko as Pauline Piperkarcka 
* Blandine Ebinger as Sidonie Knobbe 
* Gertrude W. Hoffman   
* Hermann Vallentin   
* Hans Heinrich von Twardowski   
* Max Kronert   
* Kaethe Richter   
* Preben J. Rist   
* Claire Selo   
* Emmy Wyda

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 