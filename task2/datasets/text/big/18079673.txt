Alien Agent
{{Infobox Film
| name           = Alien Agent
| image          = Alien_agent.jpg
| image_size     = 
| caption        = 
| director       = Jesse V. Johnson
| producer       = Martin J. Barab
| writer         = Vlady Pildysh
| narrator       = 
| starring       = Mark Dacascos Billy Zane Amelia Cooke
| music          = Michael Richard Plowman
| cinematography = C. Kim Miles
| editing        = Asim Nuraney Gordon Williams
| distributor    = Alien Films
| released       = 
| runtime        = 95 min.
| country        = Canada
| language       = English
| budget         = CAD 4,000,000 (estimated)
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 Canadian Science science fiction/action Jesse Johnson and starred Mark Dacascos, Emma Lahana with Billy Zane and Amelia Cooke.

== Plot ==
Rykker is an intergalactic warrior trapped on Earth, constantly fighting a gang of ruthless aliens known as The Syndicate, an alien fifth column plotting to take over the planet. The film opens with a high speed chase, with Rykker killing several syndicate agents.

Saylon is a top syndicate leader who crash-lands on Earth. His mission is to build a wormhole portal between Earth and his home planet - allowing a full-scale invasion of the Earth. Isis is the Syndicates sexy and ruthless leader. During a series of robberies for parts to build the portal, Isis becomes determined to destroy Rykker.

15 year old Julies family was killed when a truck carrying materials for the portal was hijacked. Left alone in the world, she plots to avenge her family. Julie and Rykker hook up, though he tries to leave her behind for her own safety. But she keeps showing up, even saving Rykkers life one  time. They go on a cross country journey, with Isis and her army in pursuit. The final showdown inside a nuclear reactor, has Rykker and Julie battling Isis, Saylon and their army of killers in an attempt to destroy the portal and stop the invasion.

== Main characters ==
*Rykker (  agent. Before Mark Dacascos got the part, Dolph Lundrgen was considered for the role.
*Tom/Saylon (Billy Zane): Saylon is a top Syndicate leader. His mission is to build a wormhole portal between Earth and their home planet, making a full-scale invasion possible.
*Isis (Amelia Cooke): The ruthless leader of the Syndicate on Earth. She leads a series of robberies to steal parts to build the portal. Isis is determined to destroy Rykker and clear the way for Saylons master plan.
*Julie (Emma Lahana): Julie is a 15-year old girl whose family is murdered during the hijacking of a truck carrying material for the portal. Left alone in the world, she wants to avenge her family and destroy the Syndicate.

== Cast ==
*Mark Dacascos as Rykker
*Amelia Cooke as Isis
*Emma Lahana as Julie William MacDonald as Sheriff Devlin
*Kim Coates as Carl Roderick
*Billy Zane as Saylon/Tom Hanson
*Dominiquie Vandenberg as Sartek
*Annabel Kershaw as Aunt Lorry
*Jim Shield as Jack Braden
*John Tench as C.C.
*Keith Gordey as Uncle Jim
*Meghan Flather as Amber
*Sean O. Roberts as Jerry
*Darren Shahlavi as Kaylor
*Derek Hamilton as Joe
*Lindsay Maxwell as Monica

== Reception ==
The film was not well received by critics. Adam-Troy Castro in Sci Fi Weekly described the work as "a not-very-interesting alien invasion fought by a not-very-interesting hero and not-very-interesting heroine."  Leah Holmes from SFX magazine gave it 1.5/5, saying "Its stupid and nonsensical, but at least its funny"  and George Tiller of PopMatters gave it just 1/10. 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 