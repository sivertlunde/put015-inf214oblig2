Thalappavu
{{Infobox Film
| name = Thalappavu
| image = Thalappavufilm.jpg
| story = Babu Janardhanan
| screenplay = Babu Janardhanan
| dialogues = Babu Janardhanan
| caption = Prithviraj as Naxal Joseph
| director = Madhupal
| writer = Babu Janardhanan Prithviraj Lal Lal Atul Kulkarni Maniyanpilla Raju Jagathy Sreekumar Dhanya Mary Varghese Mohan Rao
| distributor = Lal Release
| cinematography = Azhagappan
| editing = Don Max
| released =  
| runtime = 
| budget = 
| country = India
| language = Malayalam
| lyrics = O.N.V Kurup 
| music = Alex Paul and Shyam Dharman
}}
 Mohan Rao under the Civic Cinema banner. And distributed by Lals Lal Release. Thalappavu marks actor-cum-writer Madhupal’s directorial debut.

The film released on September 12, 2008 (Onam) to mainly good reviews/critical acclaim. However, in the box-office it failed to recover the cost of production. 
 Best Actor Best Debut Kerala Film Critics Award for Best Film, Best Director, Best Script writer,  Amateur Little Theatre Award for Best Debutant Director, Sohan Antony Memorial Film Award for Best Director.
 International Film Festival of Kerala (IFFK) 2008 and Indian Habitat Film Festival 2009.

== Plot ==
How betraying a friend or an acquaintance can ruin ones life is poignantly depicted in the Malayalam film Thalappavu made by actor-turned-director Madhupal. The movie is based on a real-life confession of a police constable (P. Ramachandran Nair) about gunning down a Naxalite (Arikkad Varghese) in a fake encounter as per the order of his superiors in 1970. The screenplay by Babu Janardhanan uses the stream of consciousness flow to show the mental state of the constable who feels deranged after the incident. Lal plays constable Raveendran Pillai whose family life is ruined after the killing of Joseph (Prithviraj Sukumaran), a revolutionary leader fighting for the rights of the hapless farmers of Wayanad. Raveendran Pillai is a misfit in the police force as he is not courageous or cruel as the others of his ilk. He is a family man to the hilt and dotes on his children. He befriends Joseph on the way while on duty. Joseph introduces him to the revolutionary ideas. Raveendran Pillai takes instant liking for Joseph and roams in the forests with Joseph during his free time.

* P. Ramachandran Nairs confession was in Nov. 1998 to a magazine, whereas in the film the it takes place in late 2000s to a television channel.

== Production ==
His dream project Thalappavu was always there at the back of his mind, says Madhupal. "I was moved by the story of Varghese, a man who lived for others. More than his politics or his background, what attracted me was his willingness to help the most oppressed sections in society."    Babu Janardhanan who wrote the script said he had carried out extensive research on the life of Naxal Arikkad Varghese before capturing it on paper. "The spark for the script came from the revelation made by Ramachandran Nair (the police constable) through television channels that he was forced to kill Varghese,” said Janardhanan. "The revelation struck me and I started to think about the mental agony he had to endure by suppressing the fact for nearly 30 years," he said.    Janardhanan, the script writer, and Madhupal sketched the whole film, shot by shot. Madhupal says his stint with Rajiv Anchal in Hollywood, Los Angeles helped him polish the quality of the sound track of Thalappavu. "For them, natural sound is of utmost importance. They also use it as transitions. Sound will appear before the cut to lead you to the next scene. We have also used sound to enhance cuts and dissolves. Travelling sound also contributed in smoothening the transitions." 

Parts of the film were shot in Alappuzha, Changanassery and in the rubber plantations near Kothamangalam.  Paucity of funds did bog the production of the film and it was released on September 12, 2008.  

The film was praised for its script (Babu Janardhanan), flawless treatment, acting (Lal) and cinematography (Azhagappan).  That Thalappavu was Madhupal’s crowning glory became clear when veterans like M. Mukundan and R. Sukumaran (Paadha Mudra and Rajashilpi) gave the film their stamp of approval. Litterateur M. T. Vasudevan Nair also expressed a desire to watch his film.  However, Madhupal denied allegations that the film set against the backdrop of the Naxalite movement reflected his sympathies for the political ideology. "The film is not an attempt to show my sympathy or political views. It is a reminder to a society that has forgotten how to respond to the problems faced by human beings," he said.  His new role of a director in Malayalam film industry has made Madhupal a much sought-after star, something that eluded him in his roles as actor, assistant director and scenarist. "While walking into a restaurant the other day, I overheard a few taxi drivers saying: "See, Madhupal, director of Thalappavu." Till then I had always been referred to as the serial actor Madhupal, villain Madhupal and so on…I am still getting used to this new avatar," says the actor-turned-director. 

== Cast == Lal as constable S. Raveendran Pillai (P. Ramachandran Nair)
* Prithviraj Sukumaran as Joseph (Arikkad Varghese)
* Dhanya Mary Varghese as Saramma
* Atul Kulkarni as Krishnadeva Saivar (Vasudeva Adiga) Rohini as Karthyayani, Pillais wife
* Sudheer Karamana as Nalinakshan
* Maniyanpilla Raju as Shivan Pillai
* Sreejith Ravi as Karunan
* Jagathy Sreekumar as Govinda Pillai
* Geetha Vijayan as Rosamma
* Saranya Sasi as S. Raveendran Pillas daughter
* Renjusha Menon
* Santha Devi as trader women

== References ==
 

== External links ==
*  
 
 
 
 
 
 