Jealousy (1942 film)
{{Infobox film
| name =   Jealousy 
| image =
| image_size =
| caption =
| director = Ferdinando Maria Poggioli 
| producer =  Sandro Ghenzi 
| writer =  Luigi Capuana  (novel)   Sergio Amidei    Angelo Besozzi   Vitaliano Brancati   Giacomo De Benedetti   Gino Sensani   Sandro Ghenzi 
| narrator =
| starring = Luisa Ferida   Roldano Lupi   Ruggero Ruggeri   Elena Zareschi
| music = Enzo Masetti  
| cinematography = Arturo Gallea 
| editing = Mario Serandrei 
| studio =    Società Italiana Cines  ENIC
| released =  25 December 1942 
| runtime = 86 minutes
| country = Italy Italian
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Jealousy (Italian:Gelosia) is a 1942 Italian drama film directed by Ferdinando Maria Poggioli and starring  Luisa Ferida, Roldano Lupi and Ruggero Ruggeri.  The film was shot at  Cinecittà in Rome.

==Cast==
*  Luisa Ferida as Agrippina Solmo  
* Roldano Lupi as Il marchese Antonio di Roccaverdina  
* Ruggero Ruggeri as Il parocco don Silvio 
* Elena Zareschi as Zosima Munoz 
* Elvira Betrone as Carmelina Munoz, sua madre  
* Wanda Capodaglio as La baronessa Santina di Lagomorto  
* Franco Coop as Lavvocato don Aquilante Guzardi  
* Bella Starace Sainati as Mamma Grazia 
* Angelo Dessy as Neli Casaccio  
* Anna Arena as Agata Casaccio, sua moglie 
* Pio Campa as Il giudice  
* Andrea DAlmaniera as Il dottore Meccio 
* Renato Navarrini as Lavvocato dell accusa  
* Amalia Pellegrini as Un amica del marchese di Roccaverdina  
* Peppino Spadaro as Un amico del marchese di Roccaverdina 
* Umberto Spadaro as Il testimone Sante di Mauro 
* Massimo Turci as Michele, loro piccolo figlio  

== References ==
  

== External links ==
* 

 
 
 
 
 
 

 