The Flower of Hawaii (1953 film)
The West German The Flower of Hawaii.  It is based on the life of the last Queen of Hawaii Liliuokalani.

==Cast==
* Maria Litto - Princess Lia 
* William Stelling - William Stelling 
* Ursula Justin - Pepsy 
* Rudolf Platte - Bebe 
* Paul Westermeier - Director Winterwind 
* Marina Ried - Marlene Elling 
* Ilja Glusgal - Bob 
* Lonny Kellner - Gloria 
* Madelon Truß - Madame Biller
* Alice Treff - Frau Studienrat Rathje 
* Inge Drexel - Mizzi Schikaneder
* Joseph Offenbach - Salvatore 
* K.A. Jung - Buhnenarchitekt 
* Edith Schollwer - Eine Blode Sängerin 
* Erna Sellmer - Frau Müller 
* Werner Dahms - Niki 
* Joachim Teege - Otto-Heinz 
* Horst von Otto - Inspizient 
* Hans Schwarz Jr. - Heini 
* Max Walter Sieg - Bühnenportier 
* Benno Sterzenbacher - Eine dicker Journalist 
* Carl Voscherau - Onkel Jensen 
* Gerhard Heinrich - Tobias Müller 
* Xenia Grey - Singer
* Bruce Low - Singer 
* Lentini Brothers - Acrobat 
* Guenter Schnittjer - Singer 
* Kilima Hawaiians - Singer 
* Margarete Slezak - Singer 
* Lani Moe - Lead Dancer 
* Dorina Naniola Moe - Singer, Dancer 
* Tau Moe - Singer 
* Rose Moe - Singer, Dancer 
* Margret Neuhaus - Editha Knefke

==References==
 

==Bibliography==
* Traubner, Richard. Operetta: A Theatrical History. Routledge, 2003.

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 