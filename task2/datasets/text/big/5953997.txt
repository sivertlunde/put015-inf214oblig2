Mind the Gap (2004 film)
{{Infobox film
| name = Mind the Gap
| image = Mind The Gap 2004.jpg
| caption = Official poster
| director = Eric Schaeffer
| producer = Terence Michael Chip Hourihan Bob Kravitz Noel Ashman
| writer = Eric Schaeffer  Alan King Charles Parnell Jill Sobule
| music = Veigar Margeirsson
| cinematography = Marc Blandori
| editing = Mitchel Stanley
| distributor = Sky Island Films Showtime Networks 111 Pictures
| released =  
| runtime = 134 minutes
| country = United States
| language = English
| budget = 
| gross = $10,637   
}} 2004 multi-story Charles Parnell, John Heard, Alan King in his last film role.

==Plot summary== Alan King) Charles Parnell) reels from his failed marriage.

==Cast== Alan King as Herb Schweitzer
* Elizabeth Reaser as Malissa Zubach Charles Parnell as John McCabe
* Eric Schaeffer as Sam Blue
* Jill Sobule as Jody Buller John Heard as Henry Richards
* Todd Weeks as Dr. John Albertson
* Kim Raver as Vicki Walters
* Vera Farmiga as Allison Lee
* Deirdre Kingsbury as Mother Zubach
* Christopher Kovaleski as Rocky Blue
* Stan Berger as Morris
* Yolonda Ross as Deniese
* Malcolm-Ali Davis as William
* Mina Badie as Dana

==Production==

===Development===
The film was written and directed by Eric Schaeffer, who also stars in the film. It was produced by Terence Michael, Chip Hourihan, Bob Kravitz and Noel Ashman. The film was distributed by Sky Island Films, Showtime Networks and 111 Pictures, and was premiered at the South by Southwest Film Festival (SXSW) on March 13, 2004. It was later released in New York on September 24, 2004. 

===Filming===
Most of the film was shot in Vermont, including the North Carolina scenes and most of the interiors. Exterior shots were also filmed in New York City and Tucson, Arizona.

==Reception==

===Box office===
The film made $5,503 in its opening weekend, after being released to one theater screen in New York City.  Mind the Gap went on to gross a total of $10,637 domestically, from the limited release in the United States. 

===Critical response===
The film received generally mixed reviews from film critics. It currently holds a 59% rating on aggregator website Rotten Tomatoes.  On Metacritic the film scored a 46 out of 100 rating based on 10 reviews.  Joe Leydon of Variety (magazine)|Variety wrote: "Warm-hearted but clear-eyed indie effort richly repays audience patience during deliberately paced and provocatively allusive early scenes with a cumulative emotional impact that is immensely satisfying." 

  of   wrote: "Mr. Schaeffer takes his time cryptically setting up his characters situations in the film. When they finally start moving toward one another and revealing their secrets, the revelations flow like diet soda. The improbable ending is oddly satisfying. But dont ask moviegoers to believe that Sam and Malissa would both find parking spaces right in front of Lenox Hill Hospital." 

==References==
 

==External links==
*  
*  

 
 
 
 