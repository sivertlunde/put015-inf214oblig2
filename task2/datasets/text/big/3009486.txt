Tank (film)
 
{{Infobox film
| name           = Tank
| image          = Tank - Film Poster.jpg
| image_size     = 225px
| caption        = Film Poster
| producer       = Irwin Yablans
| director       = Marvin J. Chomsky  Dan Gordon
| starring = {{Plainlist|
* James Garner
* G. D. Spradlin
* Shirley Jones
* C. Thomas Howell
}}
| music          = Lalo Schifrin
| cinematography = Donald H. Birnkrant
| editing        = Donald R. Rode
| distributor    = Lorimar Productions Universal Pictures
| released       =  
| runtime        = 113 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $11,302,884
}}
 Dan Gordon and directed by Marvin J. Chomsky. It was produced by Lorimar Productions and was commercially released in the United States by Universal Studios on March 16, 1984.

==Plot==
  Georgia (loosely based on Fort Benning and filmed there, as well as the small town of Zebulon, Georgia|Zebulon, GA). Despite being offered possibility of becoming Sergeant Major of the Army, he insists he just wishes to finish his tour and retire in peace to spend time with his family. Several years earlier, his older son, an active duty soldier, had been killed in an Army training accident, and his relationship with his only surviving son, Billy (played by Howell), is strained.  He is shown to be a tough but fair Non-commissioned officer|NCO, who quickly earns the respect and admiration of his troops.
 Sherman tank from World War II that he has restored with his younger sons help, and he uses it for parades and public relations. While visiting an off-base bar, he meets a young woman named Sarah (Jenilee Harrison) and the two of them strike up a humorous conversation over a few drinks.  During their conversation, the local deputy sheriff, Euclid Baker (James Cromwell) sees them together and orders Sarah to get back to work, insulting her and slapping her in the process. Carey quickly steps in and defends her. Unfortunately, Sarah had been forced into prostitution by Cyrus Buelton, the corrupt sheriff (G. D. Spradlin). Sheriff Buelton tries to arrest CSM Carey, but finds his jurisdiction wont let him touch him while hes on the base, which is Federal property. To get revenge on Carey, Sheriff Buelton frames Billy for drug possession by planting marijuana in his gym locker at school.

Sheriff Buelton offers to drop the charges, if CSM Carey would give the Sheriff a hefty bribe, approximately equal to his retirement savings. However, Zacks wife, LaDonna (Shirley Jones) refuses to take part in "good old boy" justice and calls a lawyer. The lawyer is thrown into jail himself on trumped-up contempt of court charges, Billy is put on trial immediately without benefit of counsel, is promptly found guilty and sentenced to several years of hard labor at the county work farm. LaDonna, finally realizing the depths of Sheriff Bueltons corruption and cruelty, goes to Carey and tells him what happened. When CSM Carey tries to offer the bribe, Buelton accepts the money, but refuses to release his son, simply stating that it will prevent him from being shot "accidentally" or while "attempting to escape", or from being raped by other inmates&nbsp;– temporarily.

Carey decides to take matters into his own hands and climbs into his vintage tank. To prevent the police from following him immediately, he destroys the local jail & police station, shoots the local telephone exchange, destroys the parked police cars with his tanks cannon and forces Deputy Baker, with the help of Sarah, who had seen the tanks arrival in town, to strip naked. Sarah then handcuffs Baker to a telephone pole and asks Carey if she can accompany him out of town. Carey accepts and the two of them depart for the county work farm where they then liberate Billy (and all the other prisoners) and flee from the town. Once away from town and the jail, he reveals his plan: to escape to neighboring Tennessee, where they can get a fair hearing in a court of law regarding extradition, which will at least be a fair hearing instead of the kangaroo court that Billy received in Georgia.
 commanding general, Major General V.E. Hubik, but the general points out that Carey had already retired from the Army, so hes broken no military law (other than breaking a small section of fence to leave the base); he hasnt stolen the tank, which is legally his; and all his violations are of civilian law. Buelton then demands that the MG Hubik order the posts personnel and vehicles pursue Carey, and that if he refuses, he is quite friendly with, and more than willing to call, the Governor of Georgia.  The general then happily points out to Buelton that he doesnt take orders from state governors due to the Posse Comitatus Act, prohibiting him or anyone else in the active duty U.S. Army from providing any military aid to civilian law enforcement.  However, the general is more than willing to provide Buelton with the address for his actual boss, the President of the United States. In a running joke of the movie, Sheriff Buelton does not understand the name of the act, and thinks hes being called a "pussy communist".

Through a long series of chases and evasion through rural Georgia, including being aided by relatives of people he broke out of jail earlier (with the tank sometimes assisting them in return), the tank and its crew quickly become folk heroes throughout the country. Despite Sheriff Buelton insisting they are criminals, the nation rallies behind them as a sort of modern-day Robin Hood, meaning the Sheriff has little public support for his hunt for the tank. On the Tennessee side of the line, thousands of people gather to welcome the tank. Meanwhile, LaDonna has met with the Governor of Tennessee, and using the threat of public electoral opinion manages to get a formal guarantee that they will be given a proper extradition hearing (and informal implication that they will be granted asylum should they reach the state line).

However, a showdown brews at the Tennessee state line, where a huge crowd of supporters for the tank and its crew have gathered, including LaDonna and the Governor of Tennessee, who arrive via helicopter. Buelton has managed to block the road with tractor-and-semi-trailer rigs and set up an ambush, including a large mud trap to inhibit the tanks freedom of movement. Using a vintage military bazooka, (actually a German "Panzerschreck") Buelton manages to disable the tank within sight of the state line. However, the tanks firepower is unaffected and the Careys and Sarah are able to hold the Sheriff and his forces at bay. Carey, who had been injured earlier while attempting to repair the tank, instructs Billy and Sarah to sneak over the state line at night. They refuse. With his father going from bad to worse, Billy is even willing to surrender to Buelton in exchange for his father getting hospitalized.
 posse to Little Big Horn".

Thus the Sheriff is forced to stop the crowd another way; he and his men all run for the mud field and begin pulling on their end, resulting in a tug of war between the crowd and the posse. The crowd find themselves unable to help pull the tank free this way and decide to try something else. The posse cheers their assumed victory, and Sheriff Buelton climbs on top of the tank and demands its occupants surrender. Zach motions to Billy to turn the tanks turret, which he does, knocking the gloating Buelton facedown into the mud. And on the other side of the state line, the tow cable is affixed to a bulldozer and the crowd renews their pulling efforts. This time, the posses attempts to stop them are completely futile, and the tank is hauled out of the mud and over the state line to safety. LaDonna happily greets her husband and son, as well as Sarah, as they climb out of the tank to a heros welcome by the people and the Governor of Tennessee.

==Cast==
* James Garner as CSM Zack Carey
* Shirley Jones as LaDonna Carey
* C. Thomas Howell as William "Billy" Carey
* Mark Herrier as SSG Jerry Elliott, Soldier Magazine Reporter
* Sandy Ward as MG V.E. Hubik
* Jenilee Harrison as Sarah
* James Cromwell as Deputy Euclid Baker
* Dorian Harewood as SFC Ed Tippet, Provost Marshals Office
* G. D. Spradlin as Sheriff Cyrus Buelton John Hancock as Mess MSG Johnson Guy Boyd as SGT Wimofsky Randy Bass as Governors Aide

==See also==
* List of American films of 1984

==References==
 

==External links==
 
* 
* 
*  at Archive of American Television
*  

 

 
 
 
 
 
 
 
 
 
 