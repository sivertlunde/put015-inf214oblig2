Tubby the Tuba (1975 film)
{{Infobox Film |
  name         = Tubby the Tuba |
  director     = Alexander Schure |
  producer     = Barry Yellin Steven Carlin Alexander Schure | original song) | See below |
  music        = George Kleisinger |
  editing      = Phillip Schopper |
  distributor  = Avco Embassy Pictures |
  released     = April 30, 1975 |
  runtime      = 88 min. | United States |
  language     = English |
    }} 1975 animated 1945 childrens story for concert orchestra and narrator of the same name by Paul Tripp and George Kleinsinger. It was released on April 30, 1975 by Avco Embassy Pictures.

The film was produced by the New York Institute of Technology, under the supervision of its founder, Alexander Schure, who was the projects director. Beck, Jerry (2005), pp. 295–6. 
 1947 Puppetoons|Puppetoon which was Paramount short nominated for a Best Animated Short Oscar.

==Story==

A young tuba named Tubby sets off on a quest to find a song of his own. He visits a circus and ventures into the forest while on the way to Singing City. 

==Production==
Tubby the Tuba had his start as the main character in a 1945 childrens story for orchestra and narrator, by Paul Tripp and George Kleinsinger, originally performed by Tripp himself, and recorded most famously by Danny Kaye, though many other actor-narrators have performed the piece on record as well, including David Wayne (who also provided a voice for the 71 feature film). The success of the Decca Records track encouraged George Pal, the Puppetoon artist, to make a 1947 short based on it. It would later receive an Oscar nomination for Best Animated Short.

A full-length version of Tubby the Tuba was announced in 1974 by Alexander Schure, the millionaire founder of the New York Institute of Technology. He set up the production at its Westbury, New York facilities, in the Animation Department, Visual Arts Center and Tech Sound Lab of that campus. In order for it to compete with the works of childrens film leader The Walt Disney Company|Disney, he rounded up a celebrity cast (led by Dick Van Dyke), as well as Tripp, the librettist, and a man known as "the dean of Broadway musical directors," Lehman Engel.

Schure, however, did not know anything about the animation process at the time he started working on it. Because of this, he hired the industrys best artists from the Eastern Seaboard, among whom were Sam Singer from Courageous Cat and Minute Mouse, and John Gentilella from the classic Popeye series. The majority of the final crew were previously members of Fleischer Studios.

Schure found the progress on the new Tubby was very slow, hindered by the tedious frame-by-frame process occasionally encountered in the hand-drawn art. In response, he turned to an interest in the then-young field of computer graphics, and recruited several consultants and scientists from NYIT so that the project could go on. Two of the later crew members were Edwin Catmull and Alvy Ray Smith, the future founders of Pixar Studios.

Thus, it should have marked the first time that computers were ever used in the making of an animated feature, but it ended up being done the conventional way after all. When the film wrapped up production, the first test screenings did not do as well as the crew had hoped it would. As a result, Catmull removed Sam Singers name from the final prints, taking a credit in Singers place. He later went on to say about the initial reaction to Tubby:

 

Of director Schure, Catmulls partner Smith observed: "We realized   that he really didnt have what it takes to make a movie." Neither of the duo were satisfied with what the finished film had to offer. 

==Release==
In 1974, sometime after the end of its production, independent distributor Avco Embassy acquired the rights to release Tubby worldwide. The film came out in select U.S. markets during the following Easter holiday. 

The feature-length Tubby has been generally forgotten in the annals of animation history since its original run,  but on September 11, 2006, a small label called Pegasus brought out a Region 2 DVD in the United Kingdom. To date, it has only received minor VHS releases in North America, including:

*Vestron Video|Childrens Video Library (1983)
*Vestron Video (1985)
*Family Home Entertainment (Early 1990s)

==Voice cast==
*Paul Tripp - Narrator
*Dick Van Dyke - Tubby the Tuba
*David Wayne - Pee-Wee the Piccolo
*Pearl Bailey - Mrs. Elephant
*Jack Gilford - The Herald
*Ray Middleton - The Great Pepperino
*Jane Powell - Celeste
*Cyril Ritchard - The Frog
*Ruth Enders - The Haughty Violin
*Hermione Gingold - Ms. Squeek

==See also==
*Timeline of CGI in film and television
*List of animated feature-length films

==References==
 
:  IMDb incorrectly lists the release year as 1976; Allmovie, 1977.

==Sources==
*Beck, Jerry (2005). The Animated Movie Guide. ISBN 1-55652-591-5. Chicago Reader Press. Accessed April 9, 2007.

==External links==
*  
* 
*  

 
 
 
 
 
 
 
 