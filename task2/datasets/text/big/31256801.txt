The Adventurers (1951 film)
{{Infobox film
| name           = The Adventurers
| image          = "The_Adventurers"_(1951_film).jpg
| image_size     = 
| caption        = Original British 1-sheet poster David MacDonald
| producer       =  Aubrey Baring Maxwell Setton
| writer         = Robert Westerby
| narrator       =  Siobhan McKenna music           =  Cedric Thorpe Davie
| cinematography = Oswald Morris
| editing        = Vladimir Sagovsky   
| studio         = Mayflower Pictures Corporation
| distributor    = General Film Distributors  (UK)
| released       = 7 March 1951	(London)  (UK)
| runtime        = 86 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} David MacDonald Peter Hammond Boer War several men journey into the South African veldt in search of diamonds. The film was cut by 12 minutes for its U.S. release, and was twice retitled, as Fortune in Diamonds and The Great Adventure.   

==Plot==

As the Boer War finalizes a South African soldier hides a cache of diamonds he finds on a body. He returns to the town he left three years earlier where his girl has married a disgraced English officer. Needing funds to get back to pick up the diamonds the Boer enlists the help of a fellow soldier as well as the Englishman and a local hotel keeper. 

==Cast==
* Jack Hawkins - Pieter Brandt Peter Hammond - Hendrik van Thaal
* Dennis Price - Clive Hunter
* Grégoire Aslan - Dominic
* Charles Paton - Barman Siobhan McKenna - Anne Hunter
* Bernard Lee - OConnell Ronald Adam - Van Thaal
* Martin Boddey - Chief Engineer
* Philip Ray - Man in Restaurant
* Walter Horsbrugh - Man in Restaurant
* Cyril Chamberlain - Waiter

==Release==
The film was originally known as The South Africa Story. It had its world premiere aboard the Queen Mary liner. 

==Critical reception== Treasure of the Sierra Madre, The Adventurers is buoyed by an unusually vicious performance by Jack Hawkins" ;  while the Radio Times wrote, "this could have been quite stirring if it hadnt been morbidly under-directed at a snails pace by David MacDonald" ;  and TV Guide found that, despite its borrowings  from Sierra Madre and from von Stroheims Greed (film)|Greed, "it is nevertheless an often-gripping film." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 