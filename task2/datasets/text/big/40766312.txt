Zombie Hunter (film)
{{Infobox film
| name           = Zombie Hunter
| image          = Zombie Hunter film poster.jpg
| image_size     = 
| border         = yes
| alt            = 
| caption        = 
| film name      = Theatrical release poster
| director       = Kevin King
| producer       = Kevin King Chris Le Jennifer Griffin
| writer         = Kevin King
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Danny Trejo Martin Copping Clare Niederpruem
| music          = Christian Davis
| cinematography = 
| editing        = Chris Le
| studio         = The Kilmax Arrowstorm Entertainment
| distributor    = Highland Film Group
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Zombie Hunter is a 2013 American direct-to-video action-thriller film directed and written by Kevin King for The Klimax and Arrowstorm Entertainment. The film stars Danny Trejo, Martin Copping and Clare Niederpruem. It follows a group of humans, led by Jesús (Trejo), defending themselves from flesh-devouring zombies. A fundraising campaign was hosted on the website Kickstarter to help fulfill the production teams desired budget. The film had its official premiere at the 2013 Fantasia International Film Festival in Montreal, and was released on home media on October 8, 2013. It was a critical failure, garnering mostly negative reviews.

==Plot==
The film opens with a news report on "Natas", a strange drug which has been reported to induce zombie-like effects in those who consume it. The film then fast forwards in time, to an Earth mostly inhabited by zombies. The Hunter (Martin Copping) drives along a post-apocalyptic wasteland and meets a hostile "Death Angel". The zombie is reduced to purplish goo after Hunter rams his car at it. He proceeds to stop at a deserted petrol station to fill his cars empty gas tank. Hunter enters the stations convenience store to check out any available supplies, but is ambushed by invading zombies. He eliminates them all after a gory battle and downs his sorrows by gulping a bottle of tequila. It is revealed that Hunters daughter and wife have both died. Resuming his journey, Hunter unknowingly ventures into the turf of the survivors, who have not been infected by Natas. Thinking Hunter is a zombie, an unknown assailant takes a shot at him. Hunter survives nevertheless and is greeted by survivor Fast Lane Debbie (Jade Regier) upon regaining consciousness.

A flirty Debbie attempts to make out with Hunter but fellow survivor Alison (Clare Niederpruem) awkwardly walks into the room to hand him a can of cola, before coercing an annoyed Debbie to leave Hunter alone. Outside, Debbie accuses Alison of being a slut. The latter woman throws back the same insult and they start to quarrel. The audience is then introduced to Father Jesús (Danny Trejo), a pastor-like zombie slayer and leader of the survivors colony. As the survivors assemble in a small room to have a meal together, a groggy Hunter joins the crowd and gets to formally know the other survivors. Alison and Hunter start to get romantically passionate with each other.

All is well until a wave of normal zombies and a monstrously large zombie somehow make their way into the survivors camp. The survivors are able to fend off the normal zombies but the gigantic zombie is seemingly unstoppable to the point that even bullets do not faze it. Jesús rushes in to save them and is able to mutilate the hulking zombie with his axe by slicing off its arm. However, Jesús is unable to defeat him wholly and as a result, the zombie slices off his head. The remaining survivors scramble into a car and drive off to an airport in hopes of being able to find a working aeroplane to fly off with.

The zombies, unshakable, still manage to track them down. Consequently, all of the survivors, except for Alison, Hunter, and Alisons brother Ricky (Jason K. Wixom), are killed. Mustering their courage, they confront the zombies and begin shooting them. At last, only the giant zombie is left. Having run out of bullets, Hunter is impaled by the sharp-clawed zombie. Nonetheless, he miraculously finds the energy to detonate a grenade, wiping out the creature and destroying the rundown airport. Just as the bomb explodes, Alison and Ricky climb into a truck and drive off to safety.

Alison vows to always remember Hunter and his courageous deed, echoing that he had finally found what he yearned for — peace. In the aftermath of the explosion, Hunter is shown to be still alive. He explains that he feels cursed as the zombies never seem to be able to kill him and decides to take his own life before the credits roll.

==Production==
  pictured in 2007]]
The film stars Danny Trejo, Martin Copping, Clare Niederpruem, Terry Guthrie, Jake Suazo, Jason K. Wixom and Jade Reiger.  It was directed and written by Kevin King. King was also in charge of production for The Klimax and Arrowstorm Entertainment, alongside Chris Le and Jennifer Griffin. Kynan Griffin, Jason Faller, Brian King, Rocky Mudaliar, Sultan Saeed Al Darmaki, Brett Bozeman, Doug Miller and Spencer Bowen served as executive producers. Ephraim Smith headed the cinematography. Chris Le edited the film. The films music was composed by Christian Davis.   

As the budget then was not sufficient enough to allow for better visual effects and others, additional funds for the film, totalling to $46,254 were raised through the website Kickstarter.  The film was shot on location in Utah.   

==Release and reception==
The film was premiered at the 2013 Fantasia International Film Festival, which was held from July 18 to August 16 in Montreal.  International sales were handled by the Highland Film Group.  The film was released on Blu-ray Disc by Well Go USA, with both single and double disc sets available, on October 8, 2013. 

The film received predominantly negative reviews from contemporary film critics. Bill Gibron of DVD Verdict found the film to be "  goofy, god-awful mess", criticising all the actors acting skills with the exception of Trejos. He also opined that the film "  lacking in legitimate entertainment value that camp may be all its capable of." In concluding his review, Gibron wrote, "When you hire Danny Trejo and give your movie a title like Zombie Hunter, you better be prepared to deliver the horror geek goods. Kevin King cant and wont. Instead, he hopes borrowing from his betters would work. It doesnt."  Mark Adams of Screen Daily stated, "Falling well and truly in the B-movie midnight screening slot, the gleefully and violently silly Zombie Hunter is littered with modest moments to enjoy but eventually lacks the humour and style to grab the cult credentials it is looking for."    John DeFore of The Hollywood Reporter described the film as "Grade Z genre fare", panning Coppings acting and the zombie makeup. In summarising his review, he wrote, "The undead craze reaches the bottom of the barrel in Kevin Kings Zombie Hunter, which may or may not have been intended as parody but produces none of the laughs that might support that categorization."  Paul Mount of the magazine Starburst rated the film with 4 out of 10&nbsp;stars, critiquing that "  is largely charmless and yet manages to provide mild diversion by virtue of some decent action sequences and impressive, if entirely unexplained, Ray Harryhausen-style monsters which provide a welcome respite from the relentless tide of drooling, slobbering undead." He concluded, "Zombie Hunter sure isn’t a good movie but it’s competent enough in its own relentlessly lowbrow way." 

Éric S. Boisvert, writing for the Fantasia International Film Festival, gave the film a rare positive review, describing it as a "bounty of pure entertainment" and "a simple yet highly effective work that has all the hallmarks of a great midnight movie." He also felt that " he ’80s soundtrack, the allure of the actresses and occasionally maladroit delivery of some actors make the whole thing all the more rewarding." 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 