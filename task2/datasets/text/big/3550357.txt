Abbott and Costello Meet the Mummy
{{Infobox film
| name           = Abbott and Costello Meet the Mummy
| image          = a&cmeetmummy.jpg
| caption        = theatrical poster
| director       = Charles Lamont
| producer       = Howard Christie
| writer         = John Grant
| starring       = Bud Abbott Lou Costello Marie Windsor Michael Ansara Peggy King
| music          = Joseph Gershenson (supervisor) Uncredited: Irving Gertz Henry Mancini Lou Maury Hans J. Salter
| cinematography = George Robinson
| editing        = Russell Schoengarth
| distributor    = Universal-International
| released       =  
| runtime        = 79 minutes
| language       = English
| budget         = $726,250 Furmanek, Bob; Palumbo, Ron (1991). Abbott and Costello in Hollywood. New York: Perigee Books. ISBN 0-399-51605-0 p 251 
}}

Abbott and Costello Meet the Mummy is a 1955 film directed by Charles Lamont and starring the comedy team of Abbott and Costello.  It is also the 28th and final Abbott and Costello film produced by Universal-International.

==Plot== Richard Deacon), overhear the conversation along with Madame Rontru (Marie Windsor), a business woman interested in stealing the treasure of Princess Ara.

Abbott and Costello go to the doctors house to apply for the position to accompany the mummy back to America.  However, two of Semus men, Iben (Mel Welles) and Hetsut (Richard Karlan),  murder the doctor and steal the mummy just before Abbott and Costello arrive.  However, the medallion has been left behind and is found by Abbott and Costello, who attempt to sell it. Rontru offers them $100, but Abbott suspects it is worth much more and asks for $5,000, which Rontru agrees to pay.  She tells them to meet her at the Cairo Café, where Abbott and Costello learn from a waiter that the medallion is cursed.  They frantically try to give it to one another (the Slipping the Mickey routine from The Naughty Nineties), until it winds up in Costellos hamburger and he swallows it.  Rontru arrives and drags them to a doctors office to get a look at the medallion under a fluoroscope.  However, she cannot read the medallions inscribed instructions, which are in hieroglyphics.  Semu arrives, claiming to be an archaeologist, and offers to guide them all to the tomb.  Meanwhile, Semus followers have returned life to Klaris.

They arrive at the tomb, where Costello learns of Semus plans to murder them all.  Rontru captures Semu, and one of her men, Charlie (Michael Ansara), disguises himself as a mummy and enters the temple.  Abbott follows suit by disguising himself as a mummy, and he and Costello rescue Semu.  Eventually all three mummies are in the same place at the same time, and the dynamite that Rontru intends to use to dig up the treasure detonates, killing Klaris and revealing the treasure.  Abbott and Costello convince Semu to turn the temple into a nightclub to preserve the legend of Klaris and the three criminals who wanted to steal the treasure are presumably arrested.

==Cast==
  
*Bud Abbott as himself
*Lou Costello as himself
*Marie Windsor as Madame Rontru  
*Michael Ansara as Charlie
*Dan Seymour as Josef Richard Deacon as Semu
*Kurt Katch as Dr. Gustav Zoomer
 
*Richard Karlan as Hetsut
*Mel Welles as Iben
*George Khoury as Habid
*Eddie Parker as Klaris, the Mummy
*Mazzone-Abbott Dancers as dance troupe
*Chandra Kaly and His Dancers as dance troupe
*Peggy King as vocalist
 

 

;Cast notes
*Lou Costellos daughter, Carol Costello, has a small part as a flower girl. She was sixteen years old at the time. 

==Production==
Abbott and Costello Meet the Mummy was filmed from October 28 through November 24, 1954 and is the last film that Abbott and Costello made for Universal Pictures, although Universal released a compilation film of clips from their films, titled The World of Abbott and Costello in 1965. The day after filming completed, Abbott and Costello arrived in New York City to ride on the first float of the annual Macys Thanksgiving Day Parade.

Although Abbott and Costello were called "Pete Patterson and Freddie Franklin" in the script and in the closing credits, they used their real names onscreen during filming.

In Universals previous Mummy films, the Mummy was called "Kharis", but in this film it is called "Klaris." Stuntman Eddie Parker (billed as "Edwin") played the Mummy.  He had previously doubled Lon Chaney, Jr. in Universals earlier Mummy films.

==DVD releases==
This film has been released several times on DVD.  Originally released as a single DVD on August 28, 2001, it was released twice as part of two different Abbott and Costello collections, The Best of Abbott and Costello Volume Four, on October 4, 2005, and again on October 28, 2008 as part of Abbott and Costello: The Complete Universal Pictures Collection.  The film was released as part of the 3-disc The Mummy: The Complete Legacy Collection  and the 21-disc Universal Classic Monsters: Complete 30-Film Collection on September 2, 2014.

==Merchandise== Imperial Toy was based on the monster from this film. 

==References==
;Notes
 
 

==External links==
* 
* 
*  
*  at Trailers From Hell

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 