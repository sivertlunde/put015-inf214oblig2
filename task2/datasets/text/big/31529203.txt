Mallikarjuna (film)
{{Infobox film
| name = Mallikarjuna
| image = 
| caption = 
| director = Murali Mohan
| producer = S. Dinesh Gandhi
| writer =  Seetha
| music = S. A. Rajkumar
| cinematography = 
| editing = 
| distributor = 
| released =  
| runtime = 
| country = India
| language = Kannada
| budget = 
| gross = 
}} action genre starring V. Ravichandran and Sadha in the lead roles . The film has been directed and written by director Murali Mohan and produced by S. Dinesh Gandhi . S. A. Rajkumar has composed the soundtrack and  the background score. The film is slated for release on May 2011.  The film is remake of the 2001 Tamil film Thavasi starring Vijaykanth.

==Plot==
Ravichandran plays a dual role of Father - son. While Seetha pairs up for the father role, Sadha pairs for the son role.

==Cast==
* V. Ravichandran
* Sadha Seetha
* Ramya Barna
* Ashish Vidyarthi
* Hema Chaudhari
* Ramesh Bhat
* M. N. Lakshmi Devi
* Adhi Lokesh
* Bullet Prakash
* Raju Talikote

==Soundtrack==
S. A. Rajkumar has composed the music for the film.

{|class="wikitable" width="70%"
! Song Title !! Singers 
|-
| "Chanda Oh Chanda" || Karthik (singer)|Karthik, Priya Himesh 
|-
| "Hey Miya Miya" || Tippu (singer)|Tippu, Ramya
|-
| "Rudram Thrinetram" || S. A. Rajkumar 
|-
| "Shuruvayithe" || Karthik (singer)|Karthik, Rita, Priya Prakash
|-
| "Olle Janakke Kaala" || S. A. Rajkumar
|-
| "Pallakki Haaduva" || Rajesh Krishnan, Anuradha Sriram
|-
|}
 

==References==
 

 
 
 
 


 