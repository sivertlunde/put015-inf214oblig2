Johnny Stool Pigeon
{{Infobox film
| name           = Johnny Stool Pigeon
| image          = johnnystoolpigeon.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = William Castle
| producer       = Aaron Rosenberg
| screenplay     = Robert L. Richards
| story          = Henry Jordan
| narrator       =
| starring       = Howard Duff Shelley Winters Dan Duryea
| music          = Milton Schwarzwald
| cinematography = Maury Gertsman
| editing        = Ted J. Kent
| distributor    = Universal Pictures
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Johnny Stool Pigeon is a 1949 black-and-white film noir directed by William Castle, and starring, Howard Duff, Shelley Winters and Dan Duryea. 
 Criss Cross, mob gang member. "Stool pigeon", in gangster terminology, means a police informant.

==Plot==
A narcotics agent convinces a convict he helped send to Alcatraz go undercover with him to help expose a heroin drug smuggling ring.  The unlikely pair travels from San Francisco to Vancouver and finally to a dude ranch in Tucson which is run by mob bosses. They end up getting help breaking the case from the gang leaders dingy blonde girlfriend (Winters), who falls for the narcotics agent during the sting.

==Cast==
* Howard Duff as George Morton aka Mike Doyle 
* Shelley Winters as Terry Stewart 
* Dan Duryea as Johnny Evans 
* Tony Curtis as Joey Hyatt
* John McIntire as Nick Avery
* Gar Moore as Sa m Harrison Leif Erickson as Pringle
* Barry Kelley as William McCandles
* Hugh Reilly as Charlie
* Wally Maher as T.H. Benson

==Reception==

===Critical response===
When the film was released, the film critic for The New York Times, gave the film a tepid review, writing, "Despite a serious attempt at authenticity it is merely a brisk cops-and-smugglers melodrama, which follows an obvious pattern and is fairly strong on suspense and short on originality and impressive histrionics ... Howard Duff, who has had plenty of experience as a gumshoe both on the radio and in films, is appropriately self-effacing, hard and handsome as the intrepid agent. Dan Duryea adds a surprising twist to his usual characterizations of tough hombres as the convict who turns on his own kind, and Shelley Winters gives a credible performance as the blonde moll who also gives the law a much-needed assist. But aside from a few variations their crime and punishment adventures are cast in a familiar mold." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 