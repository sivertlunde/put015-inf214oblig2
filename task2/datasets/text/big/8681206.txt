It's Trad, Dad!
 
 
{{Infobox film
| name           = Its Trad, Dad!
| image          = Its Trad Dad.jpg
| caption        = Theatrical release poster
| director       = Richard Lester
| producer       = Max Rosenberg Milton Subotsky
| writer         = Milton Subotsky
| starring       = Helen Shapiro Craig Douglas Felix Felton Deryck Guyler
| music          = Ken Thorne (incidental music)
| cinematography = Gilbert Taylor
| editing        = Bill Lenny
| studio         = Amicus Productions Ed. Allan Bryce, Amicus: The Studio That Dripped Blood, Stray Cat Publishing, 2000 p 15 
| distributor    = Columbia Pictures
| released       = 30 March 1962 
| runtime        = 78 min.
| country        = United Kingdom
| language       = English
| budget         = £50,000 Alexander Walker, Hollywood, England, Stein and Day, 1974 p227 
| gross          = £300,000 (UK) 
}}

Its Trad, Dad! (1962), known in the U.S. as Ring-A-Ding Rhythm, is a musical comedy featuring a variety of jazz and some rock and roll acts. The film was one of the first films put out by predominantly horror company Amicus Productions, and one of director Richard Lesters first films. 

==Plot== traditional jazz. However, the mayor as well as a group of adults dislike the trend and move to have the jukebox in the local coffee shop taken away.
 David Jacobs, who agrees to help, along with several artists who are willing to perform. But upon hearing the news of the upcoming performance, the mayor decides to stop the bands van by any means necessary.

When the show is scheduled to start, Craig and Helen find that their disc-jockey and musicians have not yet arrived, so they decide to find local talent within the crowd of guests. The interim acts manage to stall the crowd long enough for the true performers bus to traverse a series of traps the city council had set up for them. Just in time, the performers reach their stage and put on their act for the citizens. The film ends with the towns kids and teens enjoying the music and the adults grudgingly accepting it.

==Cast==
* Helen Shapiro as Helen
* Craig Douglas as Craig
* John Leyton as Himself
* The Brook Brothers as Themselves
* Chubby Checker as Himself
* Del Shannon as Himself
* Gary U.S. Bonds as Himself (as Gary   Bonds)
* Gene Vincent as Himself
* Gene McDaniels as Himself
* The Paris Sisters as Themselves (as Paris Sisters)
* The Dukes of Dixieland as Themselves (as Dukes of Dixieland)
* Chris Barbers Jazz Band as Themselves
* Ottilie Patterson as Herself
* Acker Bilk and His Paramount Jazz Band as themselves 
* Kenny Ball and his Jazzmen as themselves (as Kenny Balls Jazzmen)

==Soundtrack==
The film predominantly comprises musical numbers, including performances by the principal actors Helen Shapiro and Craig Douglas themselves. However, unlike traditional "musicals" the songs have little to do with the movie but rather serve more to show off the artists talents and give a taste for the style, as the plot stops whenever a number starts. U.S. acts were added to the film including Chubby Checker, Del Shannon,  Gary US Bonds, Gene Vincent, The Paris Sisters and Gene McDaniels. Other performers who appear include John Leyton, The Brook Brothers, The Temperance Seven and Acker Bilk.

== References ==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 