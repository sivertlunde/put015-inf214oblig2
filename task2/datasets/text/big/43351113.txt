Onnu Chirikku
{{Infobox film
| name           = Onnu Chirikku
| image          =
| image_size     =
| caption        =
| director       = P. G. Viswambharan
| producer       =
| writer         =
| screenplay     = Swapna Adoor Bhasi Jalaja Johnson
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}} 1983 Cinema Indian Malayalam Malayalam film, directed by  P. G. Viswambharan. The film stars Mammootty, Swapna (actress)|Swapna, Adoor Bhasi and Jalaja in lead roles. The film had musical score by Johnson (composer)|Johnson.   

==Cast==
*Mammootty as Unnikrishnan Swapna as Rohini Menon
*Adoor Bhasi as Krishnan Nair
*Jalaja as Urmila Menon
*K. P. Ummer as Govindankutty
*Sukumari as Radha
*Sankaradi as Ananthapadmanabhan Iyer
*Nedumudi Venu as Dr. George Santhakumari as Rohinis mother
*Kundara Johny

==Soundtrack== Johnson and lyrics was written by Poovachal Khader.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Nee manassin thaalam || Vani Jairam, Unni Menon || Poovachal Khader ||
|-
| 2 || Nee Manassin Thaalam   || K. J. Yesudas || Poovachal Khader ||
|-
| 3 || Sankalppangal poochoodum || K. J. Yesudas || Poovachal Khader ||
|}

==References==
 

 
 
 


 