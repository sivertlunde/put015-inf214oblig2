Swarm and Destroy
 
{{Infobox Film  | 
  name     = Swarm and Destroy |
  image          = Swarm_and_destroy_documentary_title_screen.jpg |
  caption          = Title screen from Swarm and Destroy |
  producer       = Jaakan Page-Wood Ken Dirkin John Wagner |
  music          = Trocar Berwer Monoglot |
  distributor    = Hardcore Productions    |
  released   = 2003 |
  runtime        = 46 min. |
  language       = English |
}}
 documentary chronicling the Moped Army and moped lifestyle.  The documentary was filmed at the Decepticons 2003 Moped BBQ 6 event in Kalamazoo, Michigan|Kalamazoo, Michigan. 
{{Cite web
| url = http://www.mopedarmy.com/main/swarmanddestroy/
| accessdate = 2009-03-09
| title = Moped Army - Swarm and Destroy the Movie
}}
 
Swarm and Destroy contains numerous interviews with Moped Army members, interleaved with animations and footage from the ongoing Moped BBQ rally.  The documentary is named after the motto of the Moped Army. 
{{Cite web
| url = http://www.mopedarmy.com/main/mission/
| accessdate = 2009-03-09
| title = Moped Army - Mission Statement
}}
  
{{Cite web
| url = http://everything2.com/title/Swarm%2520and%2520Destroy
| accessdate = 2009-03-09
| title = Swarm and Destroy@Everything2.com
}}
 

Tagline:  The Greatest Documentary about a Moped Gang Ever Made

==Synopsis==
Swarm and Destroy opens with an interview of several moped riders at the Moped BBQ 6 rally, asking how they began riding.  These riders, from various branches of the Moped Army, describe their draw to the organization and the development of moped culture, recounting their favorite anecdotes of themselves and fellow moped riders.  The stories they tell are widely varied&mdash;from the gruesome account of a rider named Ree who was injured in a debilitating accident, 
{{Cite web
| url = http://www.mopedarmy.com/resources/articles/storyofree/
| accessdate = 2009-03-09 
| title = Moped Army - Articles - The Story of Ree
}}
  to stories of vigilante justice, and many accounts of bizarre occurrences which happened while riding.

The documentary, from there, continues with an overview of the Moped BBQ event itself and the race at the BBQ, with footage shot around Kalamazoo at various race checkpoints.

The film ends with a recap of the event and interviews about growth of moped riding as a movement and the future of the Moped Army.

==DVD features==
* Sketchbook - containing artwork from the documentary
* Soundtrack
* Production Gallery
* American Style Violence - a short film by Daniel Webber Kastner and Simon King
* Race Route - information on the checkpoints in the race

==References==
 

==External links==
* 
*  available from 1977 Mopeds.
*  for the East Lansing Film Festival.

 
 
 


 