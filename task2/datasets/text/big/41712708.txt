Dahavi Fa
 
{{Infobox film
| name           = Dahavi Fa
| image          = Dahavi Fa.jpg
| image_size     =
| caption        = DVD cover
| director       = Sumitra Bhave and Sunil Sukthankar
| producer       = Sunil Sukthankar
| story          = Sumitra Bhave
| narrator       = 
| starring       = Atul Kulkarni Jyoti Subhash Milind Gunaji, Nimish Kathale and Vrishasen Dabholkar
| music          = Shrirang Umrani
| distributor    = Everest Entertainment Pvt. Ltd.
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
}}

Dahavi Fa ( : Tenth F) is a Marathi drama film released on February 17, 2002.  Produced by Sunil Sukthankar and directed by Sumitra Bhave and Sunil Sukthankar, the film stars Atul Kulkarni, Jyoti Subhash, Milind Gunaji, Nimish Kathale and Vrushasen Dabholkar. The films music is composed by Shrirang Umrani.

The film is based on a story of how teachers discriminate against the students failing the exams.  Atul Kulkarni, who plays the teacher in the film, however teaches students to channelize their anger in a positive way. The film did well at the box office and was screened for 35 weeks in Pune. 

==Plot==
Kids from class 10 F realized that they are the victims of brutal discrimination. They were not willing to accept tags like goons or thugs. Frustrated with the situation, these kids decide to take matters in their own hands and ended up vandalizing school properties. As a result, they get suspended from school.

Finally a courageous teacher steps up and confronts the biased approach of the school based on the education system and rallies these kids to channel anger in a positive way.

==Cast==
* Atul Kulkarni
* Jyoti Subhash
* Milind Gunaji
* Nimish Kathale
* Vrishasen Dabholkar

==Soundtrack==
The music has been directed by Shrirang Umrani.
{{Track listing
| title1 = Khushi Cheharyaavar Majhya Kaa Naahi Yenaar  | length1 = 5:58
| title2 = O Sir O Sir Amhi Kelacha Kaay  | length2 = 4:32
| title3 = Shudhi De Buddhi De He Dayaghana  | length3 = 5:11
| title4 = Srushti Che Mitra Aami  | length4 = 4:48
| title5 = Dahavi Fa  | length5 = 5:39
| title6 = Tumipan Sahi Aamhipan Sahi  | length6 = 5:17
}}

==Awards==
; Maharashtra State Film Awards
* 2003 - Best Feature Film   
* 2003 - Best Story Award to Sumitra Bhave 
* 2003 - Best Lyrics to Sumitra Bhave for the song "Khushi Cheheryavar Mazhya" 
* 2003 - Best Music Director to Shriram Umrani 

== References ==
 

== External links ==
*  

 
 
 