The Poker House
{{Infobox film
| name           = The Poker House 
| image          = The Poker House Poster.jpg
| caption        = Theatrical release poster
| director       = Lori Petty
| producer       = Stephen J. Cannell Michael Dubelko
| screenplay     = Lori Petty David Alan Grier
| story          = Lori Petty
| starring       = Jennifer Lawrence Bokeem Woodbine Sophi Bairley Chloë Grace Moretz David Alan Grier Selma Blair
| music          = Mike Post
| cinematography = Ken Seng
| editing        = Tirsa Hackshaw
| studio         =
| distributor    = Phase 43 Films
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} directorial debut. The film depicts a painful day in the life of a teenage girl who is raising her two younger sisters in their mothers brothel|whorehouse. It is noted for being Academy Award winning actress Jennifer Lawrences first film in a leading role.

==Plot==
The film focuses on one single day in the life of three abused and neglected sisters, Agnes (age 14) (Jennifer Lawrence), Bee (age 12) (Sophi Bairley), and Cammie (age 8) (Chloë Grace Moretz). Their mother, Sarah (Selma Blair), a woman who has turned to prostitution to support the girls, but is forced into alcohol and drug abuse by her pimp, Duval (Bokeem Woodbine). Because of this, Sarah is unable to care for the girls, forcing Agnes into a position of being a mother to her two younger sisters. The three girls live in the mothers whorehouse, the Poker House, where neighborhood pimps and criminals gather to play poker as well. Agnes believes Duval loves her, as a boyfriend would, despite his abuse towards her mother.

The movie starts when Agnes arrives home, very early in the morning. She begins tidying the house and wakes Bee, after preparing her paper route for her. The conversation between the two reveals there is another sister, Cammie, and that Cammie often stays the night at her friend Sheilas house. The movie reveals that the girls and their mother once had a real family. Their father, a preacher, used to beat Sarah and the girls. The four fled, and Sarah, struggling to make ends meet, became a prostitute.

The day shifts from girl to girl. There is little interaction among the three. Bee speaks of moving into a foster home, hoping to be adopted. Cammie spends the day at a bar, making friends with Dolly (Natalie West), the bar owner, and Stymie (David Alan Grier), an alcoholic. Agnes rides through town, talking with a few friends, playing a game of basketball and picking up a couple of paychecks from her part-time jobs.

Towards the end of the day, Agnes climbs through Bees window, avoiding the living room, which is full of gamblers, pimps and drunks. Bee has locked herself in her room and, like Agnes, avoids the downstairs chaos. Agnes makes Bee leave the house, telling her not to come back for a while. She then makes her way into the living room, and a stranger begins to talk to her. He asks her why she is there, and she responds by telling him that this is where she lives and that Sarah was her mother. When the man finds out that Agnes is a star basketball player for her high school team with an important game that night, the man gives her a sympathetic look and tells her to get out of the house and go to the game, but she ignores him.

Later that evening, Duval and Agnes begin kissing again, Agnes Narrates over the entire scene, after a few minutes Duval then rapes Agnes. As Duval releases her, she runs to the bathroom to clean herself, horrified by the thoughts of the violence and possibility of pregnancy. She is completely traumatized. Her mother enters the bathroom, and as Agnes reaches for her in utter distress, Sarah refuses to touch her and instead tells Agnes to go to the store to pick up alcohol after reminiscing on Agnes being a handful as a young child, showing intelligence even when she was a one-year-old.

Soon after, Agnes overhears Duval telling Sarah that he will begin pimping and selling Agnes as well. She threatens to shoot Duval, firing a couple of shots to prevent Duval from leaving, screaming to her mother that he raped her and deserves to be shot for what he does to Sarah as well. Sarah only tells Agnes that shell defend him. Agnes leaves for her basketball game.

After scoring 27 points in the second half alone, a record that lasts for years to come, Agnes limps to the car and has a meltdown. She then wipes her tears and puts the horrific events of the night in the back of her mind. She drives off and finds Bee and Cammie at a nearby bridge. The two get in the car with Agnes not telling her young sisters of events that took place that evening, and instead takes them to get dinner. Bee reveals that she went to the bar after she went to a friends house and that she found Cammie. Cammie then plays "Aint No Mountain High Enough", and the movie closes as the three girls sing together.

At the start of the film credits, it reveals that Agnes left Iowa to go to New York and became an actress and artist. It also reveals that 20 years later she directed the movie, and that the movie is the true story of director and actress Lori Pettys childhood.

==Cast==
*Jennifer Lawrence as Agnes
*Bokeem Woodbine as Duval
*Sophi Bairley as Bee
*Chloë Grace Moretz as Cammie
*David Alan Grier as Stymie
*Selma Blair as Sarah
*Danielle Campbell as Darla
*Casey Tutton as Sheila

Jennifer Lawrences father, Gary Lawrence, appears uncredited in the film as the basketball coach of the other team. 

==Reception==
===Critical response===
The Poker House has received mixed reviews from film critics. Review aggregator Rotten Tomatoes reports that 57% of critics have given the film a positive review based on 7 reviews, with an average score of 6.2/10. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 