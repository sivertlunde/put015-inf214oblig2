It's All Happening (film)
{{Infobox film
| name           = Its All Happening
| image          = "Its_All_Happening"_(film).jpg
| image_size     = 
| caption        = Original British quad poster
| director       = Don Sharp
| producer       =  Norman Williams
| writer         = Leigh Vance
| narrator       = 
| starring       = Tommy Steele Michael Medwin Angela Douglas Philip Green
| cinematography =  Ken Hodges
| editing        = John Jympson
| studio         =  K.N.P. Productions
| distributor    = British Lion Film Corporation
| released       = 1963
| runtime        = 86 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Its All Happening is a 1963 British  , Norman Newell).     A talent scout for a record company is frustrated by his lack of progress with his career. It is sometimes known under the alternative title The Dream Maker.

==Selected cast==
* Tommy Steele - Billy Bowles 
* Michael Medwin - Max Catlin 
* Angela Douglas - Julie Singleton 
* Jean Harvey - Delia 
* Bernard Bresslaw - Parsons 
* Walter Hudd - J.B. Magdeburg 
* John Tate - Julian Singleton 
* Janet Henfrey - April 
* Richard Goolden - Lord Sweatstone
* Keith Faulkner - Mick  
* Edward Cast - Hugh  
* Anthony Dawes - Cyril Bong

==Critical reception==
*TV Guide observed, "a number of Englands pop singers and groups of the 1960s are on display in this variety show held together by a slim story line." 
*The New York Times noted, "young Mr. Steele, all teeth and yellow hair, gives his all to the role." 
*The Radio Times called the film "sentimental hokum...The songs sound as though they were knocked out on a slow afternoon on Denmark Street, Londons very own Tin Pan Alley." 
*Allmovie called it an "engaging childrens musical." 

==References==
 

==External links==
* 
 

 
 
 
 
 


 
 