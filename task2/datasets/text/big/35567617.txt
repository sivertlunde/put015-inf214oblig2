3 Magic Words
{{multiple issues|
  
 
}}
{{Infobox film
| name           = 3 Magic Words
| image          = 3 Magic Words.jpg
| alt            = 
| director       = Michael Perlin
| producer       = Michael Perlin
| screenplay     = Michael Perlin
| distributor    = Viva Pictures/Warner Bros.
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
}}
3 Magic Words is a 2010 documentary film about spirituality. The film was written, directed and produced by Michael Perlin and co-produced by Maura Hoffman. The film was produced over four years.   The lead is played by Gabriella Ethereal and the film is narrated by Cameron Smith.  The film premiered in the U.S. at the Harmony Gold Theater in Hollywood and had its European premiere in London on December 21, 2012 at the Odeon West End in Leicester Square. 

==Content== New Age Sai Baba, Einstein, Osho, and others.  

==Development==
 
Perlin’s spiritual interests started when he was involved with metaphysical studies in 1992. Perlin’s inspiration for the film came from the 1954 book "Three Magic Words" by Uell Stanley Andersen.  He first had the idea to create the film after an existential crisis while working at the Macaroni Grill. 

==References==
 

==External links==
*  
*  

 
 
 
 
 