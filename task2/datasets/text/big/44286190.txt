Proper Patola (2014 film)
 
{{Infobox film
| name           = Proper Patola
| image          = Proper Patola (2014 film).jpg
| alt            = 
| caption        =
| director       = Harish Vyas
| producer       = Deepak Lalwani
| writer         =  Amit Saxena 
| screenplay     = 
| starring       = Neeru Bajwa   Harish Verma   Yuvraj Hans 
| music          = Jassi Katyal & Saurabh Kalsi
| cinematography = 
| editing        =
| studio         = Country Media Pvt Ltd.
| distributor    = OmJee Cine World
| released       =  
| runtime        = 
| country        = India
| language       = Punjabi
| budget         = 
| gross          = 
}}
 Punjabi Romantic comedy film directed by Harish Vyas. The film has an ensemble cast which includes Neeru Bajwa, Harish Verma, Yuvraj Hans & others.Written & co directed by amit saxena.The film will be distributed by OmJee Cineworld. 

==Cast==

* Neeru Bajwa
* Harish Verma
* Yuvraj Hans

==Marketing==
The trailer of the film was released on YouTube 1 November 2014  


==Awards==

PTC Punjabi Film Awards 2015 


Pending
*PTC Punjabi Film Award for Best Editing - Gurjant Singh / Vicky Singh
*PTC Punjabi Film Award for Best Screenplay - Harish Vyas / Amit Saxena
*PTC Punjabi Film Award for Best Performance in a Comic Role - Teji Sandhu
*PTC Punjabi Film Award for Best Debut Director - Harish Vyas
*PTC Punjabi Film Award for Best Actress - Neeru Bajwa

==References==
 

== External links ==
*  
*  

 
 
 

 