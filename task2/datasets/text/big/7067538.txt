Dust to Glory
{{Infobox film
| name = Dust to Glory
| director = Dana Brown
| writers = Dana Brown
| producers = Mike McCoy and Scott Waugh
| executive producer = C. Rich Wilson
| music by = Nathan Furst
| starring =
| cinematography =
| editing = Dana Brown and Scott Waugh
| released =  
| runtime = 97 minutes
| language = English
}}
Dust to Glory (2005) is a documentary about the famous Baja 1000 off-road race. Filming occurred throughout the 2003 event.  The film is directed by Dana Brown of Step Into Liquid fame. The film was edited in Adobe Premiere Pro. The film score was by Nathan Furst.

==Cast== Mike McCoy, Robby Gordon, J.N. Roberts, Larry Roeseler 

==Reviews==
* "All in all, this is an entertaining and informative film that exposes an event unlike any other. Some scenes are strictly for race lovers, but there is enough here overall to entertain curious outsiders." -Jeff Otto, IGN Filmforce

==See also==
* Off-road racing
* SCORE International

== References ==

 

==External links==
*  
*  

 
 
 
 
 
 