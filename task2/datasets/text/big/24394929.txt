Almost Human (1974 film)
 
 
{{Infobox film
| name = Almost Human
| image = Almost human poster.jpg
| caption = Film poster
| director = Umberto Lenzi
| producer = Luciano Martino
| writer = Ernesto Gastaldi 
| narrator =
| starring = Tomas Milian  Henry Silva
| music = Ennio Morricone 
| cinematography = Federico Zanni 
| editing = Eugenio Alabiso 
| distributor = Interfilm 
| released = 8 August 1974
| runtime = 100 minutes 
| country = Italy 
| language = Italian
| budget = lire
}}

Almost Human ( ) is a 1974 Italian crime film directed by Umberto Lenzi. This film stars Tomas Milian and Henry Silva. NoShame Films DVD Case, 2005. Last accessed: September 2008. 

==Plot==
A bunch of thieves kidnap a boy in a red hoodie from his mothers arms. This is followed by an action-packed car chase full of breaking boxes, defacing cars, and slim getaways.
The chase ends when the kidnappers are saved by an oncoming train, which cuts off the police from their trail. They find this a perfect opportunity to dump the kid and make their getaway. The boy is returned to his family but the kidnappers gang beat them up for their failure.
Following a castration threat, the kidnapper/thief goes home to rape his girlfriend. Following this, he robs a cigarette dispenser and stabs the beat cop who catches him. This leads to a detective to start asking questions.
The following day, while the thief is picking up his aforementioned girlfriend from her office, he notices the young brown-haired daughter of his girlfriends boss, and decides to kidnap her.
After a love-making session with his girlfriend in her apartment, the kidnapper leaves to find his friend and convinces him to join his plan. We are shown how tough a cop the detective is through a cut scene. The protagonist (kidnapper) and his gang start stalking the girl while shes playing tennis with her father and his friends.
Using his girlfriends stolen red car, the thieves go buy guns from an old confidant ("Papa") of theirs. The guns are worth a 100 thousand per piece, so if they kill someone, their confidant keeps their deposit.
Not wanting to pay their confidant the deposit, they murder Papa and his staff, and make their escape.

They then catch up with their target while she is discussing her future with the boy she wants to marry in his car in the middle of the forest. Her boyfriend shows his reluctance to marry her unless she refuses her inheritance on the basis of principal. The girl refuses and starts making out with her boyfriend against his will. The kidnappers then attack and kidnap her during her lovemaking & murder her boyfriend.
But the girls escapes into the forest until she reaches a bungalow, where she is taken in, and the family agree to protect her, but the bad guys break in, murder an old man, humiliate the rest, and run off with the girl (who they stash in an abandoned ship yard).

After returning his girlfriends car to her, he murders her by drowning and disposes of her car.

During all this, the inspector gets a lead when three bad guys make a ransom call and ask for "No Police interference".
While posting a ransom letter that they had forced the girl to write, one of the thieves realizes the cops have discovered the murder of the daughter of his girlfriends boss. The cops conclude all the murders and thefts were done by the same gang. Deducing the kidnappers knew the girlfriend, the cops decide to visit her apartment. The protagonist notices this and follows them back to his own apartment and sees them discover his connection to the whole mess.
Scared, he calls his acquaintance (the guy who had beat him up) and asks for a deal. He then goes to the police and acts innocent to throw off suspicion from himself.
When he and the detective visit the acquaintance, he covers for his "friend", and then threatens the kidnapper with castration again.

After the cops start closing in on him, the protagonist goes insane and murders the hostage(the girl), a shoot-out ensues, and only the protagonist survives. He runs off to hide, but the detective finds him the next day and shoots him in the head.

==Cast==	
*Tomas Milian: Giulio Sacchi
*Henry Silva: Inspector Walter Grandi Ray Lovelock: Carmine
*Gino Santercole: Vittorio
*Laura Belli: Marilù Porrino
*Luciano Catenacci Ugo Maione
*Anita Strindberg: Ione Tucci
*Guido Alberti: Commendator Porrino
*Lorenzo Piani: Gianni  
*Pippo Starnazza: "Papà" 

==Production==
When production began, director Umberto Lenzi offered the role of Giulio Sacchi to actor Marc Porel. After an unpleasant meeting with the actor, Lenzi found him to be "unreliable from both a human and professional point of view." Lenzi would not do the film if Porel was cast and he told the producer "Its him or me." The producer suggested Tomas Milian, who had appeared in numerous spaghetti westerns. Lenzi admits that his first meeting with Milian was difficult; Milian had heard that he was an "impulsive, hot-headed director." Despite the stories Milian felt Lenzi was the right person for the job. This would begin what Lenzi has called a love–hate relationship between the two that would continue for six more films.

American actor Richard Conte was originally cast as Commissario Walter Grandi, but he died right before filming began; Henry Silva was cast in his place. Lenzi states that he worked hard to have Silva fit the role of a tough cop because Silva was more tailored to play villainous roles. 

==Releases==
Almost Human was released on 8 August 1974. Curti, 2003. p. 105  Its domestic gross was 1,168,745,000 Italian lira|lire. 

===Home Video===
Almost Human was released on home video in the United States in the early 1980s.  The home video was promoted as a horror film due to Lenzis fame as a director of films such as Cannibal Ferox and Eaten Alive!. 

The film was released on Region 0 NTSC DVD by NoShame films in 2005. The DVD is currently out-of-print. 

It has also been released on DVD in Italy by Alan Young Pictures and in the United Kingdom by Shameless Screen Entertainment 

==Reception==
On its initial release, Almost Human was unanimously condemned by Italian film critics. Curti, 2003. p. 106 

==Notes==
 

===References===
*  

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 