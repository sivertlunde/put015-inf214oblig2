Haiku Tunnel
{{Infobox film name		= Haiku Tunnel image		= writer		= Jacob Kornbluth Josh Kornbluth starring       = Josh Kornbluth Warren Keith director       = Jacob Kornbluth Josh Kornbluth producer       = distributor    = Sony Pictures Classics released	=   runtime        = 88 minutes language	= English budget         =
}} temporary and permanent employment).

== Plot ==
Josh is the consummate temp employee, avoiding all long-term connections and responsibilities, both at work and in his personal life. However, by the time his agency places him at the Schuyler & Mitchell   seventeen important letters.

== Cast ==
* Josh Kornbluth as himself
* Warren Keith as Bob Bob Shelby
* Amy Resnick as Mindy
* Harry Shearer as Orientation Instructor
* Leah Alperin as Temp #1
* Jacob Kornbluth as Temp #2
* Stephen Muller as Temp #3
* Linda Norton as Temp #4
* Helen Shumaker as Marlina DAmore
* Sarah Overman as Julie Faustino
* Brian Thorstenson as Clifford
* June Lomena as DaVonne
* Joanne Evangelista as Caryl
* Jennifer Laske as Helen the Ex-Girlfriend
* Patricia Scanlon as Helen the Ex-Secretary
* Joe Bellan as Jimmy the Mail Clerk
* Michael X. Sommers as Crack Attorney

== Meaning of the title ==
The Haiku Tunnel Project (part of Interstate H-3) is a huge document containing lists of all the products needed for a tunnel in Hawaii. Josh was working in a word processing job when he was given the project. At the time he was "totally-temp" and as he was going through copying this project, he found himself inside the Haiku Tunnel. And now that he finally had what he always wanted (no strings attached) he found himself unhappy.

== External links ==
 
*  
*  

 
 
 
 
 
 
 
 
 