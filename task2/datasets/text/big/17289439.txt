The Pirates of Penzance (1983 film)
{{Infobox film
| name = The Pirates of Penzance
| image = The-pirates-of-penzance-1982.jpg
| image_size =
| caption =
| director = Wilford Leach
| producer = Joseph Papp
| writer = William S. Gilbert (operetta)  Wilford Leach
| starring = {{plainlist|
* Kevin Kline
* Angela Lansbury
* Linda Ronstadt George Rose
* Rex Smith
}}
| music = Arthur Sullivan (non-original music)
| cinematography = Douglas Slocombe
| editing = Anne V. Coates
| distributor = Universal Pictures
| released =  
| runtime = 112 minutes
| country = United Kingdom United States
| gross = $694,497 (US)
}} comic opera George Rose, Linda Ronstadt, and Tony Azito. The cast also includes Timothy Bentinck, Louise Gold and Tilly Vosburgh.
 Broadway cast reprised their roles in the film, except that Lansbury replaced Estelle Parsons as Ruth. The minor roles used British actors miming to their Broadway counterparts. It was filmed at Shepperton Studios in London.

==Plot summary== George Rose), and realizes that Ruth is "plain and old".  Frederic quickly falls in love with one of them, Mabel (Linda Ronstadt).  He has a strong "sense of duty" and has vowed to lead a blameless life and to exterminate the pirates.  Soon, however, the pirates return and seize the girls.  Their father then arrives and lies to the pirates, telling them that he is an orphan.  He knows that they are orphans themselves and never attack an orphan.

After the pirates leave, General Stanley wrestles with his conscience, having told a lie.  Mabel and Frederic try to cheer him up, and Frederic has engaged the constabulary to help him defeat the pirates.  The police arrive, but they turn out to be timid.  Then the pirate king and Ruth find Frederic alone.  They have reviewed the fine print on his apprenticeship indenture and have discovered that he is still a pirate because he was born in leap year on February 29, and he will not be out of his indentures to the pirates until his 21st birthday.  Mabel agrees to wait for Frederic until then!  The Police return and, hearing the pirates approach, they hide.  The pirates arrive and seize the still guilt-ridden Major-General.  The police are coaxed to battle the pirates, but they are defeated.  However, the Sergeant of Police (Tony Azito) calls on the pirates to "yield in Queen Victorias name."  The pirates tearfully do so and release the Major-General, surrendering to the police.  However, Ruth reveals that the pirates are all "noblemen who have gone wrong", and the Major-General pardons them and invites them to marry his daughters, as all ends happily.

==Musical numbers==
#Overture
#Pour, oh Pour the Pirate Sherry – Pirates and Samuel
#When Frederic Was A Little Lad+ – Ruth
#Oh Better Far to Live and Die++ – Pirate King
#Oh False One, You Have Deceived Me – Frederic and Ruth
#Climbing Over Rocky Mountain+ – Major Generals Daughters
#Stop, Ladies, Pray – Frederic and Daughters
#Oh Is There Not One Maiden Breast+ – Frederic and Daughters
#Oh Sisters, Deaf to Pitys Name – Mabel and Daughters
#Poor Wandering One++ – Mabel and Daughters
#Stay, We Must Not Lose Our Senses – Frederic, Daughters and Pirates
#Hold Monsters and I Am the Very Model of a Modern Major-General+ – Mabel, Major General and Chorus
#Act I Finale – Company
#Oh Dry the Glistening Tear* – Mabel and Daughters
#When the Foeman Bares His Steel++ – Sergeant of Police
#Now For The Pirates Lair – Frederic, King and Ruth
#When You Had Left Our Pirate Fold+ – Ruth, King and Frederic
#My Eyes are Fully Open (from Ruddigore) – Frederic, Ruth and King
#Away, Away, My Hearts On Fire – King, with Frederic and Ruth
#Stay, Frederic, Stay – Mabel and Frederic
#Ah, Leave Me Not To Pine – Mabel and Frederic
#Oh Here Is Love And Here Is Truth – Mabel and Frederic
#No, I Am Brave+ and Sergeant, Approach+++ – Mabel, Police and Sergeant
#When A Felons Not Engaged In His Employment+ – Sergeant and Police
#A Rollicking Band of Pirates, We – Sergeant, Pirates and Police
#With Cat Like Tread++ – Pirates and Police
#Sighing Softly To The River – Major-General and Men
#Act II Finale++ – Company

;Differences from the stage version
:+Shortened
:++Extended
:+++Originally dialogue.
:Omitted: How Beautifully Blue the Sky

==Reception== SelecTV and ON-TV. Shepherd, Marc.  , the Gilbert and Sullivan Discography, July 7, 2010, accessed January 31, 2015  Only 91 theaters showed it. 

The film was released on VHS in 1984 and on DVD in 2010. 

==See also==
*The Pirate Movie – 1982 adaptation of the opera starring Christopher Atkins and Kristy McNichol.

==References==
 

==External links==
* 
*  at Rotten Tomatoes
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 