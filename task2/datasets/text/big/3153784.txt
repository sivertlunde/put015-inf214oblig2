Piranha (1978 film)
{{Infobox film
| name           = Piranha
| image          = PiranhaPosterA.jpg
| caption        = US theatrical release poster by John Solie
| director       = Joe Dante Jon Davison Chako van Leeuwen
| screenplay     = John Sayles
| story          = John Sayles Richard Robinson Kevin McCarthy Keenan Wynn Dick Miller
| music          = Pino Donaggio
| cinematography = Jamie Anderson
| editing        = Joe Dante Mark Goldblatt
| distributor   = New World Pictures (US) United Artists (international)
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $770,000 Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 146-147 
| gross          = $6 million (US) $10 million (international) 
}} Kevin McCarthy, Great White.
 in 1995, another in spawned its own sequel in 2012.

==Plot==
 
Two teenagers come upon an apparently abandoned military installation. They take advantage of what appears to be a swimming pool to skinny dip. The teenagers are attacked by an unseen force and disappear under the water. A light activates in the main building and a silhouetted figure investigates the screams, but is too late to help.

A determined but somewhat absent-minded insurance investigator named Maggie McKeown is dispatched to find the missing teenagers near Lost River Lake. She hires surly backwoods drunkard Paul Grogan to serve as her guide. They come upon the abandoned compound, which functioned as a fish hatchery before being militarized. They discover bizarre specimens in jars and indications of an occupant. Maggie locates the drainage switch for the outside pool and decides to empty it to search the bottom, but the moment she activates it a haggard and frantic man begins to panic and attacks her, attempting to stop the draining until he is subdued by Grogan. The two found a skeleton in the filtration trap of the empty pool, and learn it was filled with salt water. The man awakens and steals their jeep, but crashes it due to his disorientation, and is taken to Grogans home where they spend the night. They take Grogans raft down the river, where the man wakes up and tells them that the pool in the facility was filled with a school of piranhas, and that Maggie released them. They are skeptical until they hear a dog barking and they come across the corpse of Grogans friend Jack, who has bled to death from an attack on a fishing dock.

The man reveals himself to be Doctor Robert Hoak, lead scientist of a defunct Vietnam War project, Operation: Razorteeth, tasked with engineering a ravenous and prodigious strain of piranha that could endure the cold water of the North Vietnamese rivers and inhibit Viet Cong movement. The project was shut down when the war ended, but some of the mutant specimens survived, and Hoak tended to them to salvage his work. Grogan realizes that if the local dam is opened, the school will have access to the Lost River water park and summer camp, where his daughter is in attendance. They encounter a capsized canoe with a boy whose father has been killed by the fish. Hoak rescues the boy but suffers mortal injuries when the school attacks him; he dies before he can reveal how to kill them. Blood from Hoaks corpse causes the piranha to tear away the rafts lashings, and they barely reach shore. Grogan stops the dam attendant from opening the spillway and calls the military.

A military team led by Colonel Waxman and former Razorteeth scientist Dr. Mengers feed poison into the upstream section, ignoring the protests that the fish survived the first attempt. When Grogan discovers that a tributary bypasses the dam, Waxman and Mengers quarantine them to prevent the agitated pair from alerting the media. After they escape, Waxman alerts law enforcement to capture them. The school attacks the summer camp during a swimming marathon, injuring and killing many children and a supervisor. Grogans daughter escapes due to her fear of water, and aids her camp mates in escaping. 
 catatonic state.

Mengers gives an on-site television interview, providing a sanitized version of events and downplaying the existence of piranha. Her voice is heard carrying out over a radio on the shore of a West Coast beach. As she says "theres nothing left to fear", the piranhas characteristic trilling sound drowns out the waves on a beach.
 

==Cast==
* Bradford Dillman as Paul Grogan
* Heather Menzies as Maggie McKeown Kevin McCarthy as Dr. Robert Hoak
* Keenan Wynn as Jack
* Dick Miller as Buck Gardner
* Barbara Steele as Dr. Mengers
* Belinda Balaski as Betsy
* Melody Thomas Scott as Laura Dickinson Bruce Gordon as Colonel Waxman
* Paul Bartel as Mr. Dumont Barry Brown as Trooper
* Shannon Collins as Suzie Grogan
* Shawn Nelson as Whitney Richard Deacon as Earl Lyon

==Reception==
 
Based on reviews from 25 critics collected by the film review aggregator Rotten Tomatoes, 72% gave Piranha a positive review.  Steven Spielberg called it "the best of the Jaws (film)|Jaws ripoffs".   

==Release==
The film was released theatrically in the United States by New World Pictures in August 1978.  Given the proximity to Jaws 2, Universal Pictures had considered an injunction, but Spielberg convinced them otherwise. 
  New Concorde Home Entertainment released the film on special edition DVD.   This version is currently out of print.

In 2010, Shout! Factory re-released Piranha on DVD and Blu-ray Disc|Blu-ray. 
 R rating Ultimate Edition presented by the Fangoria Magazine and the speciual guests Rebekah McKendry, curator Mike Williamson and origin actress Belinda Balaski. 

==Remakes==
 
 
Piranha was first remade in 1995, and this version was also produced by Roger Corman and originally debuted on Showtime (TV network)|Showtime. It used footage from the original for certain sequences.
 The Hills Have Eyes. Distributor Dimension Films Bob Weinstein told Variety (magazine)|Variety, "We will maintain the fun and thrilling aspects of the original film, but look forward to upping the ante with a modern-day twist."    Piranha 3D was theatrically released in the United States on August 20, 2010 and is in 3-D film|3D.
 The Blob, Adam Scott, and Jerry OConnell.

==See also==
*Killer Fish

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 