A Desert Wooing
{{Infobox film
| name           = A Desert Wooing right
| alt            = 
| caption        = DVD cover (2012)
| director       = Jerome Storm
| producer       = Thomas H. Ince
| screenplay     = J.G. Hawks  Jack Holt Donald MacDonald J. P. Lockney Charles Spere
| music          = 
| cinematography = Edwin W. Willat 
| editing        = 
| studio         = Thomas H. Ince Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent Jack Holt, Donald MacDonald, J. P. Lockney and Charles Spere. The film was released on June 23, 1918, by Paramount Pictures.   The film has been preserved and was released on DVD format in 2012. 

Screen debut of actress Irene Rich in uncredited role.

==Synopsis==
Avice, daughter of a decadent rich family, realizes that her mother has put her on the marriage market and resigns herself to her fate. So she marries Masters, a rich westerner, still believing herself in love with Dr. Van Fleet, one of societys lounge lizards. He is a member of the party which Masters invites to his ranch house after the wedding. One night he breaks into Avices room but much to his surprise she repulses him. He is roundly beaten up by Masters, is driven off the ranch and takes refuge with a Mexican. After the departure of the rest of the guests, Masters sets himself to the task of crushing the proud spirit of Avice but with little success. His best friend, Keno Clark and Avices brother, thinking to assist in the happy ending, try to arouse Avices jealousy by hinting of another woman in the life of Masters. This however, fails to have the desired effect, for Avice announces her intention of returning East and never setting eyes on her husband again. Van Fleet, the villian, in revenge, shoots Masters, wounding him severely and then Avice awakens to her real state of mind. At the point of a gun she forces the doctor to dress her husbands wound and when he recovers there is complete happiness.
::Synopsis from Motion Picture News (1918)

==Cast==
*Enid Bennett as Avice Bereton Jack Holt as Barton Masters Donald MacDonald as Dr. Van Fleet 
*J. P. Lockney as Keno Clark
*Charles Spere as Billy Bereton

==Reviews== Photoplay Magazine said that Bennett "plays the wife with much speed and prettiness, though her method of handling a gun would hardly do in France", and noted that Holt "is a handsome hero for a change, and takes kindly to the work. It is a lively production, slightly tinged with suggestiveness at the outset".  Edward Weitzel praised Bennetts performance in The Moving Picture World, saying her "part was much stronger than any she has yet played" and also singled out camerman Edwin Willats work, saying "scenically the production has many moments of beauty". 
===Censorship===
The Exhibitors Herald (1918) reported that the Chicago Board of Censors had cut out several scenes from the film, including: subtitles that said "You may be mad - but you must pay", two choking scenes, a close-up of a man looking at a woman through a window, and all scenes in the bed up to the time she appears with her robe on, after the man enters the room. 

== References ==
 

== External links ==
*  
* 
* 
 
 
 
 
 
 
 
 