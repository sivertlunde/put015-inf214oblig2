Bolero (1984 film)
{{Infobox film
| name           = Bolero
| image          = Boleroposter.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = John Derek
| producer       = Bo Derek
| writer         = John Derek
| starring = {{Plainlist|
* Bo Derek
* George Kennedy
* Andrea Occhipinti
* Ana Obregón
}}
| music          = Elmer Bernstein   Peter Bernstein
| cinematography = John Derek
| editing        = Sophie Bhaud Hughes Damois
| studio         = City Films Cannon Film Distributors
| released       =  
| runtime        = 104 minutes   
| country        = United States
| language       = English
| budget         = $7 million 
| gross          = $8,914,881 
}} romantic drama film starring Bo Derek, and written and directed by her husband John Derek. The film centers on the protagonists sexual awakening and her journey around the world to pursue an ideal first lover who will take her virginity.

Bolero was the film that dissolved the distribution deal between Metro-Goldwyn-Mayer, then known as MGM/UA Communications, and Cannon Films, over the potentially X-rated material in the film.  MGM then had a rule of not releasing X-rated material theatrically.  Cannon parted ways with MGM shortly before the release of Bolero, and Cannon again became an in-house film production and distribution company.

==Plot==
Ayre "Mac" MacGillvary (Bo Derek), a virginal young woman, on graduating from an exclusive British boarding school, is determined to find the right man for her first sexual encounter wherever he might be in the world. Rich enough not to venture forth alone, she brings along her friend Catalina (Ana Obregon) and the family chauffeur Cotton (George Kennedy).

Ayre first travels to an Arab country where she meets an ideal lover, a sheik who offers to deflower her but falls asleep almost immediately. Giving up on the sheik, Ayre goes on to Spain, where she meets the toreador Angel (Andrea Occhipinti) who manages to stay awake. Unfortunately, after she has succeeded in her quest, Angel is gored while bull-fighting.

The injury leaves Angel unable to perform in the bedroom, and so Ayre makes it her mission in life to see to his recovery. Along the way, she learns to fight a bull herself as a way of getting her despondent lover motivated to stop moping. Eventually, she is successful in aiding Angel to full recovery, and the film ends with their wedding.

==Cast==
* Bo Derek as Ayre “Mac” McGillvary
* George Kennedy as Cotton
* Andrea Occhipinti as Rejoneador Angel Contreras
* Ana Obregon as Catalina
* Olivia dAbo as Paloma
* Greg Bensen as Sheik
* Ian Cochrane as Robert Stewart
* Mirta Miller as Evita Mickey Knox as Sleazy Moroccan guide
* Paul Stacey as Young Valentino #1
* James Stacy as Young Valentino #2

==Release==
Bolero was released with no MPAA rating; its nudity and sexual content disqualified it from an "R" rating. At the time of release, the NC-17 rating had not yet been established and the only higher rating being X-rated|X, John Derek decided to release the film unrated. The films tagline is "An adventure in ecstasy." The film is officially on DVD with an "R" rating with no cuts.

==Reception==
The film holds a 6% rating on Rotten Tomatoes based on 16 reviews.  It was nominated for nine Golden Raspberry Awards and won six, including "Worst Picture," "Worst Actress," "Worst Director," and "Worst Screenplay."    In 1990, the film was nominated for, but lost the Razzie Award for "Worst Picture of the Decade."    The movie was nominated for a Stinkers Bad Movie Awards for Worst Picture.  

The film earned about $8.9 million in American ticket sales    based on a $7 million budget.   

==Video Releases==

In 1985, Artisan Entertainment|U.S.A. Home Video released Bolero in both Unrated and R-Rated versions to the video rental marketplace.  In 2005, MGM Home Entertainment released Bolero on DVD, after the rights to the majority of Cannon Film productions reverted to MGM, an ironic move, considering the events that transpired between MGM and Cannon over the original theatrical release of the film.
The MGM release, although rated R, is the full, uncut version of the film.

==References==
 

==External links==
*  
*  
*  
*  
*  

   
{{Succession box Razzie Award for Worst Picture
| years=5th Golden Raspberry Awards
| before=The Lonely Lady
| after= 
}}
 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 