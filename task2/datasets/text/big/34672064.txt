Dr. Vidya
{{Infobox film
| name           = Dr. Vidya
| image          =
| image_size     =
| caption        =
| director       = Rajendra Bhatia
| producer       = Mohan Segal 
| writer         = Screenplay:Sachin Bhowmik Dialogue:Sarishar Sailani
| narrator       =
| starring       = Vyjayanthimala Manoj Kumar Leela Chitnis Nazir Hussain
| music          = Sachin Dev Burman|S. D. Burman
| cinematography = C. S. Puttu
| editing        = Pratap Dave
| studio         = Mohan Studio
| distributor    = Deluxe Films
| released       = 1962
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          =  80,00,000
}} 1962 Hindi Romantic family Mumtaz Begum forms an ensemble cast. The film was produced by Mohan Segal with his own banner, Deluxe Films. The films score was composed by Sachin Dev Burman|S. D. Burman with lyrics provided by Majrooh Sultanpuri and Raja Mehdi Ali Khan. While the editing was done by Pratap Dave. This story was adopted from the Marathi film Shikleli Bayko (1959)(Meaning " Educated Wife"). 
==Plot==
The movie was shot in an era when educated people in cities were getting influenced by the western culture hence the people from rural areas who placed a lot of importance to values and culture, strongly abstained from marrying into city peoples homes. Geeta is a girl from a well to do family and is well educated, her parents had brought up her with traditional values and respect for those values. After her graduation, her parents decide to marry her off with Ratan Chowdhury, the son of a zaminder, who had the mind frame that a highly educated wife would be incapable of handling and managing a home, specially in a joint family. Later he marries Geeta but his mentality and the talk of others stop him from accepting her. He disowns her and poor Geeta has to go back to her parents house.

Even after lot of insistence from her parents and friends she refuses to remarry. Instead she continues her studies to become a doctor. After garnering a degree in medical science she goes back to Ratans village as Vidya. She wants to clear the wrong perceptions that Ratan has about her. Ratan couldnt recognise her, as he had not even seen her face, even during marriage. He falls in love with Vidya and intends to marry her, but one day during the annual cart race, Ratan meets with an accident and suffers a very bad injury post which needs surgery. Due to unavailability of any other doctor, Geeta has to do the job. She performs it successfully. Will Ratan accept Geeta in her new avataar?

==Cast==
* Vyjayanthimala as Geetha / Dr. Vidya
* Manoj Kumar as Ratan Chowdhury
* Leela Chitnis
* Nazir Hussain Helen
* Sunder
* Madan Puri
* Prem Chopra Mumtaz Begum

==Soundtrack==
{{Infobox album  
| Name        = Baat Ek Raat Ki
| Type        = Soundtrack
| Artist      = Sachin Dev Burman
| Cover       = 
| Released    = 1962 (India)
| Recorded    =  
| Genre       = Film soundtrack|
| Length      = 
| Label       = Sa Re Ga Ma|
| Producer    = Sachin Dev Burman
| Reviews     = 
| Last album  = Baat Ek Raat Ki  (1962)
| This album  = Dr. Vidya  (1962)
| Next album  = Tere Ghar Ke Samne   (1963) 
|}}
The films soundtrack was composed by Sachin Dev Burman|S. D. Burman with lyrics provided by Majrooh Sultanpuri and Raja Mehdi Ali Khan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Picturization || Length (m:ss) ||Lyrics ||Notes
|-
| 1|| "Aayi Hai Dilruba Are Tujh Ko Kya " ||   ||
|-
| 2|| "Aye Dil-E-Awaara Chal Phir Wahin Dobaara" ||   || 
|-
| 3|| "Bheegi Bheegi Aankhon Pe Parda" ||   ||
|-
| 4|| "Jaani Tum To Dole Daga Deke" ||   ||
|-
| 5|| "Khanke Kangna Bindiya Hanse" ||   || 
|-
| 6|| "Main Kal Phir Miloongi" ||   ||
|-
| 7|| "Pawan Diwani Na Maane" ||   || 
|-
| 8|| "Yoon Hans Hans Ke Na Dekho" ||   || 
|}

==Box office== eighteenth highest grossing film of 1962 with verdict average success at box office.  While Ibosnetwork.com had reported that the film had grossed around  50,00,000 and declared the film as the fifteenth highest grossing film.  

==References==
 

==External links==
*  
*   at Upperstall.com
*   at Gomolo.com

 
 
 
 
 