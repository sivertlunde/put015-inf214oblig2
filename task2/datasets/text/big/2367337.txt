Loophole (1954 film)
{{Infobox film
| name            = Loophole
| image           = Loophole.jpg
| alt            = 
| caption         = Theatrical release poster
| director        = Harold D. Schuster
| producer        = Lindsley Parsons
| screenplay      = Warren Douglas
| story           = Dwight V. Babcock George Bricker Barry Sullivan Dorothy Malone Charles McGraw
| music           = Paul Dunlap
| cinematography  = William A. Sickner
| editing         = Ace Herman
| distributor     = Monogram Pictures
| released        =  
| runtime         = 80 minutes
| country         = United States
| language        = English
| budget         = 
| gross          = 
}} Barry Sullivan and Dorothy Malone. The film was directed by former editor Harold D. Schuster. Mary Beth Hughes plays the movies femme fatale. 

==Plot==
The film tells the story of bank teller Mike Donovan (Sullivan) who failed to report a $49,000 shortage from his drawer. Hes accused of theft and quickly fired from his job.  He is then prevented from finding other employment by insurance investigator Gus Slavin (McGraw), who is convinced Donovan took the money.

Despite many setbacks, Donovan tries to clear his name but even his wife (Malone) doesnt think that hell be able to do it.  Turns out the money was heisted by a phony bank examiner and his mole working at the bank (Hughes).

==Cast== Barry Sullivan as Mike Donovan
* Dorothy Malone as Ruthie Donovan
* Charles McGraw as Gus Slavin
* Don Haggerty as Neil Sanford
* Mary Beth Hughes as Vera
* Don Beddoe as Herman Tate
* Dayton Lummis as Jim Starling
* Joanne Jordan as Georgia Hoard John Eldredge as Frank Temple Richard Reeves as Pete Mazurki / Tanner

==Reception==

===Noir analysis===
Film critic Dennis Schwartz explains why the film is considered a film noir: "The poignancy of the story is in how an innocent, hard-working person like Mike, could have his whole life turned upside-down over an incident where he makes an error in judgment. When he tells his boss (Lummis) about it, he has no explanation why he didnt report it immediately except he couldnt understand how so much money was missing. This slip-up is why Mike becomes a noir protagonist, though he doesnt have the dark side to his character this genre usually calls for...  his life turns into hell when, even though he is not charged with anything, the bonding company that must insure him cancels his certification and the bank is forced to fire him. Not only cant he get bonded so he can get another tellers job, but the bond company puts a mean-spirited insurance investigator on his tail, Gus Slavin (Charles McGraw). Slavin is convinced Mike is guilty and tails him everywhere, and when Mike gets a job he informs the boss on him and Mike is always promptly fired." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 