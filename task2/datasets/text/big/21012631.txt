Santa Buddies
{{multiple issues|
 
 
}}

{{Infobox Film
| name = Santa Buddies
| image = Santa Buddies.jpg
| director = Robert Vince
| producer = Robert Vince Anna McRoberts
| writer = Robert Vince Anna McRoberts
| based on =  
| starring = Zachary Gordon Josh Flitter Tom Bosley George Wendt Quinn Lord Christopher Lloyd Danny Woodburn Tim Conway Liliana Mumy Ty Panitz Field Cate Skyler Gisondo Richard Kind Kaitlyn Maher
| music = Brahm Wenger
| caption = DVD cover art
| distributor = Walt Disney Pictures
| released =  
| runtime = 88 minutes
| country = United States United Kingdom
| language = English
}}
Santa Buddies  is a 2009 direct to video film. It is the fourth installment of the Air Buddies spin-off series as well as the ninth film in the Air Bud franchise.  It was released on November 2, 2009.

==Plot==
The movie begins at the North Pole with Santa Claus (George Wendt) and his dog Santa Paws (Tom Bosley) taking a look at the magical Christmas Icicle, which is melting because of nobody believing in Christmas anymore; if it continues to melt, Christmas will be gone forever.

Puppy Paws (Zachary Gordon) is the fun-loving son of Santa Paws,who has not learned the true meaning of Christmas, finds Budderball (Josh Flitter) on Santas naughty list for eating the Thanksgiving turkey and figures hes just the dog to show him how to be an ordinary puppy.

The film then moves forward to Budderball and the other Buddies (Field Cate, Liliana Mumy, Skyler Gisondo, Ty Panitz) in Fernfield, who also dont have any Christmas spirit, believe that their father is Santa Paws and that the naughty list is a way to scare them to behave. However, Sniffer (Tim Conway) replies that Christmas is about giving and a holiday they must learn to respect.

The Christmas Icicle makes a big crack and shuts off the power at the North Pole and makes the reindeer weak and powerless (earlier before he left the North Pole, Puppy Paws stares at the icicle stating "I wish Christmas would just disappear" causing it to crack).

Puppy Paws makes it to Fernfield to find Budderball in his home and Budderball himself introduces Puppy Paws to his brothers and sister and the North Pole puppy causes trouble to each of the Buddies: Budderball gets framed for eating the gingerbread cookies as he tries to get back on the nice list, B-Dawg gets beaten by Puppy Paws at dancing who even breaks a vase, Mudbud teaches Puppy Paws to roll in the dirt, and unaware not to shake dirt out of his fur in the living room, he makes holiday shapes of the dirt spots causing Mudbud to wear a cover-up coat for the rest of the day, Rosebud gets an extreme Christmas makeover, and Buddhas meditation statue gets turned into a snowman as Puppy Paws explains that the snowman is "what citizens of the North Pole meditate in front of".
 dognapped by a dog catcher named Mr. Stan Cruge (Christopher Lloyd) and takes him to the Dog pound|pound. There, Puppy Paws meets a puppy named Tiny (Kaitlyn Maher) who is wishing for a Christmas miracle and sings a song about miracles, which teaches Puppy Paws   about the true meaning of Christmas.

The Buddies, along with an elf dog named Eddy (Richard Kind), have come to the rescue. With Tiny and Eddy still in the pound, the elf dog speaks to Cruge that he knows the dog catcher always wanted a puppy for Christmas, but his mother was allergic to dogs and never got one, which made Cruge hate Christmas and become a dog catcher. Eddy becomes free from the pound and changes Cruge.
 the Buddies remembered what Shasta taught them). A changed Mr. Cruge brings Tiny over to the child who has been asking for a puppy as a Christmas present and Tiny herself says her goodbye to the Buddies and Puppy Paws. After their last delivery, Santa Claus and Santa Paws arrive in Fernfield and Santa makes the Buddies (including Budderball) on top of the nice list, Puppy Paws becomes part of Santas family, and the Buddies say farewell.

Mr. Cruge is invited to dinner with Tinys new family and the movie ends with the entire town, led by Cruge, singing "Silent Night" in front of Fernfields Christmas Tree.

==Cast==
*George Wendt as Santa Claus
*Christopher Lloyd as Stan Cruge
*Danny Woodburn as Eli
*Quinn Lord as Pete
*Ryan Grantham as Sam
*Andrew Astor as Mikey
*Sophia Ludwig as Alice
*Gig Morton as Billy
*Craig Anton as Bob
*Michael Teigen as Sheriff Dan

==Voice cast==
*Zachary Gordon as Puppy Paws, Santa Paws son
*Tom Bosley as Santa Paws, a Great Pyrenees
*Tim Conway as Sniffer
*Josh Flitter as Budderball 
*Liliana Mumy as Rosebud 
*Ty Panitz as Mudbud (Jonathan Morgan Heit when singing)
*Field Cate as Buddha 
*Skyler Gisondo as B-Dawg 
*Richard Kind as Eddy, a Jack Russell Terrier
*Kaitlyn Maher as Tiny, a Yorkshire Terrier
*Chris Coppola as Comet, a Reindeer

==Prequel==
A prequel called The Search for Santa Paws was released on November 23, 2010. 

==Sequels== Indiana Jones-like movie, featuring the Buddies in another new adventure trying to stop Cleocatra in Egypt. 

==References==
 

==External links==
* 
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 