One More Time (1970 film)
 
 
{{Infobox film
| name           = One More Time
| image          = One More Time (1970 movie poster).jpg Jack Davis
| director       = Jerry Lewis
| producer       = Milton Ebbins
| writer         = Michael Pertwee Ester Anderson Les Reed
| cinematography = Ernest Steward
| editing        =
| distributor    = United Artists
| released       =  
| runtime        = 92 minutes
| country        = United States
| awards         =
| language       = English
| budget         =
}}
 Salt and Pepper.

==Synopsis==
 aristocratic twin brother for help. He refuses to help them, and is then found murdered. Pepper assumes his identity, and soon discovers that he was a diamond smuggler, and was murdered by his accomplices. Salt and Pepper band together to put the criminals behind bars.

==Production notes==
*Lawford plays both Pepper and Peppers twin brother, Lord Sydney Pepper.

* This is the only film which Jerry Lewis directed but did not star in (though he does have a role as the offscreen voice of the bandleader).

*Peter Cushing and Christopher Lee have cameo roles as Baron Frankenstein and Count Dracula, respectively.

==DVD release==
The film was released on DVD on January 25, 2005.

==External links==
* 
*  

 

 
 
 
 
 
 
 


 