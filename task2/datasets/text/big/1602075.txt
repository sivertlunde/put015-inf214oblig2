Revolution (1985 film)
 
 
{{Infobox film
| name = Revolution
| image = Revolution_imp.jpg
| alt = 
| caption = Original release poster
| director = Hugh Hudson
| producer = Irwin Winkler
| writer = Robert Dillon
| starring = {{Plainlist|
* Al Pacino
* Donald Sutherland
* Nastassja Kinski
}}
| music = John Corigliano
| cinematography = Bernard Lutic
| editing = Stuart Baird
| studio =   Goldcrest Films 
| distributor = Warner Bros.
| released =  
| runtime = 124 minutes  
| country = United Kingdom
| language = English
| budget = $28 million    (Pound sterling|£19 million)   24 September 1995: 9 . The Sunday Times Digital Archive.] Web. 29 March 2014. 
| gross = $358,574 
}} historical drama film directed by Hugh Hudson, written by Robert Dillon, and starring Al Pacino, Donald Sutherland, and Nastassja Kinski. The film stars Pacino as a New York fur trapper who involuntarily gets enrolled in the Revolutionary forces during the American Revolutionary War.
 Sea of Love.

==Plot==
 
Fur trapper Tom Dobb unwillingly participates in the American Revolutionary War after his son Ned is drafted into the Army. Later, his son is captured by the British, and taken by the strict Sergeant Major Peasy. Dobb attempts to find him, and along the way, becomes convinced that he must help fight for the freedom of the Colonies, alongside the disgraced & idealistic aristocrat Daisy McConnahay.

==Cast==
* Al Pacino as Tom Dobb
* Donald Sutherland as Sgt. Maj. Peasy
* Nastassja Kinski as Daisy McConnahay
* Dexter Fletcher as Ned Dobb
** Sid Owen as Young Ned Dobb
* Joan Plowright as Mrs. McConnahay Dave King as Mr. McConnahay
* Steven Berkoff as Sgt. Jones John Wells as Corty
* Annie Lennox as Liberty woman
* Chris Halling as British trooper

==Production==
The film was produced by the British company Goldcrest Films|Goldcrest, and was filmed largely in the old dock area of the English port town of Kings Lynn. The main battles scenes were filmed at Burrator Reservoir on Dartmoor in Devon and on the coastal cliff top near Challabought Bay, South Devon where a wooden fort was built. Military extras were recruited from ex-servicemen mainly from the Plymouth, South Devon, area.

==Reception==
Revolution cost $28 million to make, and proved to be a box-office disaster, only grossing $346,761 in the United States. The film was also a critical letdown, with many criticizing the performances (especially the accents), writing, and choice to shoot a story of American history in England.    It currently holds a critical approval rating of a mere 10% at Rotten Tomatoes based on 20 reviews. 

Amateur historian and BeatlesandBeyond Radio show presenter Pete Dicks hails Revolution as a more accurate film about the American Revolution than any other about the genre. "The portrayal of the Colonists rioting and disorganisation at the films beginning is more likely than any other film would indicate. However, there are still errors in that "England is used instead of Britain" in the prologue. British brutality, as usual is over emphasised, and as usual, Colonial brutality is ignored. The role of the French in winning the war for the rebels is also largely ignored".     

===Accolades===
Revolution was nominated for four Golden Raspberry Awards:
*  ) Worst Director - Hugh Hudson (lost to Sylvester Stallone) Worst Actor - Al Pacino (lost to Sylvester Stallone) Worst Musical Score - John Corigliano (lost to Vince DiCola)

The film won a Stinkers Bad Movie Awards for Worst Picture. 

==Directors Cut==
Dissatisfied, Hudson would revisit the film years later, and released a new cut on DVD; Revolution: Revisited, in 2009. This has an added narration by Pacino (recorded for this release, though it was meant to be in the original version), as well as ten minutes of footage being cut. Also of note is an included special feature: a conversation with Pacino and Hudson discussing the film being rushed for a US Christmas release, being trashed by the critics, and other issues relating to making and releasing Revolution.  

The film was also released in the UK in 2012 by the British Film Institute in a Blu-ray Disc/DVD combo pack. This edition came with both cuts of the film, as well as a booklet with essays written by Nick Redman, Michael Brooke and critic Philip French, who argues that the film was a victim of bad publicity and cultural misunderstandings, and regards the Revisited cut as a masterpiece. 

==See also==
* Heavens Gate (film)|Heavens Gate (1980) – Another historical epic made in the same period which was a critical and commercial failure upon release in the United States, but has since gained more acclaim.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 