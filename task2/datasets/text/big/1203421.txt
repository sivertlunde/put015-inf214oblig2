King David (film)
{{Infobox film
| name =King David
| image =King david.jpeg
| caption =Original film poster
| director = Bruce Beresford
| producer = Martin Elfand
| writer = Andrew Birkin James Costigan
| starring = {{Plainlist|
* Richard Gere
}}
| music = Carl Davis
| cinematography = Donald McAlpine
| editing = William M. Anderson
| distributor = Paramount Pictures
| released =  
| runtime = 115 minutes
| country = United Kingdom United States
| language = English 
| budget = $21 million
| gross = $5,111,099
}}
King David is an American 1985 drama film about the second king of Israel, David. It was directed by Bruce Beresford and starred Richard Gere in the title role.

==Synopsis==
The film follows the life of David, drawing mainly from biblical accounts, like I and II Samuel, I Chronicles, and the Psalms of David. 

==Cast==
*Richard Gere as "David"
*Edward Woodward as "Saul"
*Alice Krige as "Bathsheba"
*Denis Quilley as "Samuel (Biblical figure)|Samuel"
*Niall Buggy as "Nathan (prophet)|Nathan"
*Cherie Lunghi as "Michal"
*Hurd Hatfield as "Ahimelech"
*Jack Klaff as "Jonathan (1 Samuel)|Jonathan"
*John Castle as "Abner"
*Tim Woodward as "Joab"
*David de Keyser as "Ahitophel" Young David"
*Simon Dutton as "Eliab"
*Jean-Marc Barr as "Absalom" George Eastman as "Goliath"
*Arthur Whybrow as "Jesse"
*Christopher Malcolm as "Doeg the Edomite"
*Valentine Pelka as "Shammah"
*Ned Vukovic as "Malchishua"
*Gina Bellman as "Tamar (daughter of David)|Tamar"
*James Coombes as "Amnon"
*James Lister as "Uriah the Hittite" Jason Carter as "King Solomon|Solomon" 
*Genevieve Allenbury as "Ahinoam"
*Massimo Sarchielli as "Palastu"
*Aïché Nana as "Ahinoab"
*Ishia Bennison as "Maacah"
*Jenny Lipman as "Abigail (biblical figure)|Abigail"
*Roberto Renna as "Zabad (Bible)|Zabad" 
*Marino Masé as "King Agag" George Eastman as "Goliath of Gath|Goliath" Anton Alexander  as "Runner"
*Tomás Milián as "Akiss" (uncredited)
*John Barrard as "Benjamite Elder"
*Michael Müller as "Abinadab"
*Mark Drewry as "Ishbosheth" John Gabriel King Jehosaphat"
*Lorenzo Piani as "Guardian" Young Solomon" Young Absalom"
*Peter Frye as "Judean Elder" David Graham as "Ephraimite Elder" 
*David George as "Messenger"
*Nicola Di Gioia as "Hebrew"
*John Hallam as "Philistine Armour Bearer"

==Production==
It was filmed in 1984 in Matera and Craco both in Basilicata, and Campo Imperatore in Abruzzo, the Lanaitto valley (Oliena) in Sardinia, Italy, and at Pinewood Studios in England. 

==Reception==
The film was not well received by the critics, with the   gave the film a rotten 14% rating.  Richard Geres performance in the film earned him a   and Rocky IV.

===Aftermath===
Years later, Bruce Beresford said of the film:
 I think there are a few things in it that are interesting. But, I think there are so many things that are wrong. We never liked the script... we never really caught the friendship between David and Jonathan. There werent enough scenes between them. And David, himself - I think Richard Gere was miscast. He is a wonderful actor but he is much better in contemporary pieces.  

==See also==
* List of historical drama films
* Kings (U.S. TV series)
* List of films based on military books (pre-1775)

==References==
 

==External links==
*  
*  at Rotten Tomatoes

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 