Juke Girl
{{Infobox film
| name           = Juke Girl
| image size     = 
| image	=	
| caption        = 
| director       = Curtis Bernhardt
| producer       = 
| writer         = 
| based on       = 
| starring       = Ann Sheridan Ronald Reagan Richard Whorf
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| studio         = Warner Bros.
| released       = 1942
| runtime        = 
| country        = United States
| language       = English
| budget         = 
}}
Juke Girl is a 1942 film  starring Ann Sheridan and future President of the United States Ronald Reagan.  The supporting cast includes Richard Whorf, George Tobias, Gene Lockhart, Alan Hale, Sr., Howard Da Silva, Donald MacBride, Faye Emerson, Willie Best, and Fuzzy Knight. The plot focuses on the plight of exploited farmers and farmworkers in the South.

==Plot summary==

 

==Cast==
* Ann Sheridan as Lola Mears
* Ronald Reagan as Steve Talbot
* Richard Whorf as Danny Frazier
* George Tobias as Nick Garcos
* Gene Lockhart as Henry Madden Alan Hale as Yippee
* Betty Brewer as Skeeter
* Howard D Silva as Cully
* Donald MacBride as “Muckeye” John
* Willard Robertson as Mister Just
* Faye Emerson as Violet “Murph” Murphy
* Will Best as Jo-Mo

==References==
 

==External links==
*  at IMDB
*  at TCMDB

 

 