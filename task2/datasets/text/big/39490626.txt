Beauty's Worth
{{Infobox film
| name           = Beautys Worth
| image          =File:Beautys Worth 1922.jpg
| image_size     =
| caption        =Film poster
| director       = Robert G. Vignola
| producer       =
| writer         = Luther Reed (scenario)
| story          =
| based on       =  
| narrator       =
| starring       = Marion Davies Frederick Stanley
| music          =
| cinematography =
| editing        =
| studio         = Cosmopolitan Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 112 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
| gross          =
}} Quaker who falls in love.

==Cast==
*Marion Davies as Prudence Cole
*Forrest Stanley as Cheyne Rovein
*June Elvidge as Amy Tillson
*Truly Shattuck as Mrs. Garrison
*Lydia Yeamans Titus as Jane
*Hallam Cooley as Henry Garrison
*Antrim Short as Tommy Thomas Jefferson as Peter
*Martha Mattox as Aunt Elizabeth Whitney
*Aileen Manning as Aunt Cynthia Whitney
*Gordon Dooley as Doll (in charade scene)
*Johnny Dooley as Soldier (in charade scene)

==External links==
* 
* 
*  at Silentera.com
* 

 
 
 
 
 
 
 
 


 
 