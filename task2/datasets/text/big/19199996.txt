Rose of the Alley
{{Infobox film
| name           = Rose of the Alley
| image          =
| caption        =
| director       = Charles Horan
| producer       =
| writer         = Harry O. Hoyt Jackie Saunders
| starring       = Mary Miles Minter
| cinematography =
| editing        =
| distributor    = Metro Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States Silent English English intertitles
| budget         =
}}
 1916 silent silent crime drama film directed by Charles Horan.

This film is extant at George Eastman House, Rochester New York. 

==Cast==
* Mary Miles Minter - Nell Drogan
* Danny Hogan - Kid Hogan
* Frederick Heck - Dan Hogan
* Geraldine Berg - Mamie
* Alan Edwards - Frank Roberts
* Thomas Carrigan - Tom Drogan

==Reception== city and state film censorship boards. The Chicago Board of Censors ordered cut scenes from Reel 1 of a shooting, from Reel 2 the signaling from man on roof to man on street, shooting from the roof, and a vision of a gun fight in a saloon, and from Reel 5 fourteen gun fight and shooting scenes and two scenes of struggle between man and girl. 
==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 