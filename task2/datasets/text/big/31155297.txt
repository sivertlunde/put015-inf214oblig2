School Master (1958 film)
{{Infobox film
| name           = School Master ಸ್ಕೂಲ್ ಮಾಸ್ಟರ್ எங்கள் குடும்பம் பெரிசு
| image          = 
| caption        =
| director       = B. R. Panthulu
| producer       = B. R. Panthulu
| writer         = Kusumagraj
| based on       =  
| starring       =  
| music          = T. G. Lingappa
| cinematography = W. R. Subba Rao   Karnan
| editing        = R. Devarajan
| distributor    = Padmini Pictures
| released       =  
| runtime        = 164 minutes
| country        = India
| language       = Kannada
| budget         =
| gross          =
}}
School Master ( ) is a 1958 Indian Kannada language film, which was simultaneously dubbed as Tamil language film as Engal Kudumbam Perisu ( ). It was released in 1958. It was later remade into various other languages including Telugu language|Telugu, Malayalam and Hindi. Movie is a classic film starring Dikki Madhav Rao, Udaykumar, Sivaji Ganesan, Gemini Ganesan and B. Saroja Devi. It was the first Kannada film to complete Silver Jubilee Celebration.
 Badi Pantulu School Master, again directed by Panthulu.

The Malayalam version  , K. Balaji, Thikkurissi Sukumaran Nair, Sivaji Ganeshan, Ragini (actress)|Ragini, Ambika Sukumaran|Ambika, Sowcar Janaki, Aranmula Ponnamma, Bahadur etc. The lyrics were by Vayalar Ramavarma, music by  G. Devarajan and the dialogues by - Ponkunnam Varki. The film was a super hit.

In 1973 once again the same story was filmed in Tamil.

==Plot==
The story revolves mostly around the producer director B. R. Panthulu who plays the role of School Master, and his wife. Uncommon about Balkrishna is that he plays a positive role in the film. Bustling actor Narasimharaju is also there.

Humble School Teacher with his 2 sons and a daughter arrives at a new place. He finds the school building is in bad shape and his staff are not motivated to teach.
He is a strict disciplinarian, which is shown in humorous way.
His elder son gets into fight with a ruffian student Vaasu. Vaasu is punished, but his innate goodness wins over his teacher. Vaasu is given special care, to become a good student.
He has to fight against village politics, when he wishes to build a new building to the school. Undeterred, he motivates his students and villages to amass enough funds to build it.

However, henchmen in the village burn down his house.
His pet student Vaasu gathers all his friends and builds a small house for his teacher.
Years pass and Vaasu tops matriculation exams.

Teachers sons get married much against the wishes of their parents, but teacher takes it in his stride.
Their beautiful daughter is married to simpleton business man.

When Teacher retires, his sons are averse to take their parents. 
With no savings of his own, Teacher has to rely on his sons to lead retired life.
Elder son takes his father, while younger one takes his mother.
Teacher is thus separated from his wife.

Things take such a bad turn, that teacher cannot even read his wifes letter, because his sons will not provide him with spectacles.

In the climax, teachers erstwhile house, which was painfully constructed by his students, has to be auctioned off.
Vaasu Sivaji Ganeshan, now the Superintendent of Police happens to pass by in the car and notices the auction.
Distraught with pain, he confides to his wife, this is the very house, he built with his fellow-students for the tecaher, who led him to the garden path. He is overcome by emotions, on seeing the auction. Vaasus wife immediately removes all her jewellery and urges Vaasu to use them to stop the auction and pay the bank.
Vaasu is filled with joy and does so.

Vaasu makes his school teachers life with joy and Teacher is filled with happiness on seeing his once ruffian student becoming a role model. His journey of teacher has met a satisfactory end.

Film has some sentimental and emotional moments and laughable moments too. It has some classroom incidents. Film also has some village politics at the start, which continues to influence the school master character throughout the film. The Film teaches life lessons. 

==Soundtrack==
The films score was composed by T. G. Lingappa

===Tamil film===
Engal Kudumbam Perisu
Lyrics: K.D. Santhanam, Ku. Sa. Krishnamoorthy & Ku. Ma. Balasubramaniam.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Adhi Madhuraa Anu Raaga || A. M. Rajah & K. Jamuna Rani || || 03:27 
|- 
| 2 || Ellaarum Nammavare  || A. P. Komala || || 03:06
|- 
| 3 || Raadhaa Maadhava Vinodha Raadha  || T. M. Soundararajan & P. Suseela || || 03:12
|-  Soolamangalam Rajalakshmi || || 03:02
|- 
| 5 || Somasekara Loga Paalane  || T. G. Lingappa, A. P. Komala & K. Rani || || 02:39
|- 
| 6 || Varugave Varugave Guru Sevaiye  || A. P. Komala || || 02:58
|-  Soolamangalam Rajalakshmi || || 03:23
|-  Soolamangalam Rajalakshmi || || 03:12 
|- 
| 9 || Sugamaana Andhi Velai || T. M. Soundararajan & P. Suseela || || 02:42
|- 
| 10 || Ramayana Childrens Drama  ||  || || 06:48
|}

===Kannada film===
School Master
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Adhi Madhuraa Anu Raaga || A. M. Rajah & K. Jamuna Rani || || 03:27
|- 
| 2 || Ellaru Nammavare || A. P. Komala || || 03:06
|- 
| 3 || Raadhaa Maadhava Vinodha Raadha  || T. G. Lingappa & P. Suseela || || 03:12
|-  Soolamangalam Rajalakshmi || || 03:02
|- 
| 5 || Swami Devane Loka Paalane || T. G. Lingappa, A. P. Komala & K. Rani  || || 02:39
|- 
| 6 || Bannirai Bannirai  || A. P. Komala & K. Rani || || 02:58
|-  Soolamangalam Rajalakshmi || || 03:23
|-  Soolamangalam Rajalakshmi || || 03:12
|- 
| 9 || Sompaada Sanjevele  || T. G. Lingappa & P. Suseela || || 03:27
|}

===Telugu film=== Badi Pantulu
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 ||  ||  || || 
|- 
| 2 ||  ||  || ||
|- 
| 3 ||  ||  || ||
|- 
| 4 ||  ||  || ||
|- 
| 5 ||  ||  || ||
|- 
| 6 ||  ||  || ||
|- 
| 7 ||  ||  || || 
|- 
| 8 ||  ||  || || 
|- 
| 9 ||  ||  || || 
|}

==Awards== National Film Awards    1959 - All India Certificate of Merit for Third Best Feature film
* 1959 - National Film Award for Best Feature Film in Kannada

==Trivia==
Few scenes in Amitabh Bachchan starrer Baghban (film) are taken from this movie, like parents being separated by their sons, Father unable to read wifes letter, due to his spectacles being broken.

==References==
 

==External links==
*   on Chiloka
*  
*  

 
 
 

 
 
 
 
 