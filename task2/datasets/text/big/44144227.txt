Khamoshiyan
 
{{Infobox film
| name           = Khamoshiyan
| image          = Khamoshiyan.Jpg
| alt            = 
| caption        = Movie Poster
| director       = Karan Darra
| producer       = Mahesh Bhatt  (Presenter)  Mukesh Bhatt
| writer         = Vikram Bhatt
| starring       = Gurmeet Choudhary Ali Fazal Sapna Pabbi
| music          = Songs:  
| cinematography = Nigam Bomzan
| editing         = Kuldip K. Mehan
| studio         = Vishesh Films
| distributor    = Fox Star Studios
| released       =   }}
| runtime        = 
| country        =India Hindi
| budget         =  
| gross          =    
}}

Khamoshiyan  is a 2015 Indian supernatural horror film. The film is directed by Karan Darra and produced by Mahesh Bhatt and Mukesh Bhatt in association with Vishesh Films. The film is a supernatural love triangle revolving around a writer adrift in the icy slopes of Kashmir where he discovers a married woman with a strange, silent past along with her bed-ridden husband. The film is said to be a sequel to the 2011 film, Haunted 3D as per Vikram Bhatt.   

The film stars Gurmeet Choudhary, Ali Fazal and Sapna Pabbi in principal roles. Initially, the film was scheduled for release on 23 January 2015, but pushed to a 30 January 2015 release.    The music for the movie has been composed by Jeet Ganguly and Ankit Tiwari. Its box office collections were reportedly Rs 1.92 crore on the opening day  Released amidst high publicity, most critics panned the movie and it was declared as a "Flop". 

==Plot==
The plot deals with a novelist Kabir (Ali Fazal) whose failed career and relationship with Simran (Debina Bonnerjee) force him to move to Kashmir in search for an inspirational story. In Kashmir, Kabir meets a mysterious beautiful lady Meera (Sapna Pabbi) who is married to an industrialist Jaidev (Gurmeet Choudhary). Meera runs a mysterious guest house, where Kabir arrives as a tourist and also take cares of her bed-ridden husband. Kabir inadvertently starts getting attracted towards the married Meera.Meera also was falling for Kabir but she refused to go with him.Kabir finds out that Meera has not been out of that house for two years.He even heard Meera talking in the phone in a mans voice.

Kabir now in love with Meera, tries to take her away from the house.While driving themselves away from the mysterious lake house an evil force comes before their car and they have an accident . Curious to know the reality Kabir ends up calling a  tantrik after drugging Meera. The tantrik discovers an evil spirit in the house which even attacks them. The tantrik claims the spirit is of Jaidev as he is dead .Realization dawns upon Kabir,he remembers Meera talking in a mans voice trying to hide the fact the only man in the house is dead. Kabir sneaks into Jaidev s room only to find Jaidev sleeping on bed. Furious with all this, Meera asks Kabir to leave but he returns to help her.

On confronting, Meera reveals that she was involved in a hit and run case two years back and to evade imprisonment she ran away to a small town in Kashmir, where she meets Jaidev, a rich industrialist who lives in a huge mansion. She falls in love with him and soon they both get married. One night Meera sees Jaidev performing a ritual slaughter and praying to evil. Scared and shocked Meera tries to run away but she is caught by Jaidev. In struggle to stop her Jaidev falls from stairs and gets paralysed below waist. Later Jaidev commits suicide and leaves a letter for her mentioning, that she will have to live in the house for eternity with his soul and if she ever tries to run away from his house, and if his lawyer comes to know about it his lawyer will file an FIR against her for killing him and for her involvement in the hit and run case, for which he has already instructed his lawyer through a letter. To avoid been caught by police for double murder, Meera had to stay in the house with Jaidevs soul and delude the world that her husband is alive and bed-ridden.

Kabir decides to take care of the lawyer and goes to his house, only to discover that there is no such lawyer. On knowing Jaidevs lie Meera and Kabir now had to just cremate Jaidevs body and set his spirit free. They cremate his body and think that Jaidevs spirit has been set free.While they cremate his body, inside the Lake house a laugh echos in the huge rooms and Jaidev in the picture smiles sadisticly. To their surprise Jaidev returns to haunt Meera and captures her in a painting. Kabir takes help of the same tantrik who himself is a spirit.He confesses that he belongs to the world beyond the picture which he revels is a porthole.Only people like him can enter that world.He gives him Kabeer holy water to spray on Jaidev after informing kabir that he had very less time to save Meera After a lot of deadly struggle Kabir drinks a drop of Jaidevs blood to get his spirit enter his own body. He later drinks holy water, which makes Jaidevs evil spirit perish, setting Meera free. The movie ends with Kabir and Meera reuniting, whereas Kabir also converts his experience into a story and publishing it titled as "Khamoshiyan".

==Cast==
* Sapna Pabbi as Meera
* Ali Fazal as Kabir
* Gurmeet Choudhary as Jaidev
* Vikram Bhatt as Kabirs Friend and Book Editor
* Debina Bonnerjee as Simran (Cameo)

==Production==

===Development=== horror genre in Bollywood".  Bhatt further added that the film has the potential of becoming a franchise. Ankit Tiwari was roped in to compose music for the film. 

=== Filming ===
Shooting of the films first schedule began in 5 June 2014,    and ended on August 2014.  Shooting for the films second schedule took off in Kashmir in September 2014 scheduled for 15 days.    Due to the high flood in Kashmir, producers had to shift the shoot to South Africa, as a replacement for Kashmir.    In 21 October 2014 filming took place in Cape Town in South Africa.  Pre-climax was filmed at the forests of Legacy Estates in Cape Town. 

=== Marketing ===
A video was created to announce the launch date of trailer of Khamoshiyan; it was uploaded on the video sharing platform, YouTube.  The video had background music only and was used to introduce the characters, unveil the logo and the punchline.    Khamoshiyan music launch was held in a venue that is below the ground level and this turned to be the first underground music launch event for any Hindi film.  The filmmakers announced launch of a novel and a game. The novel will have not only the story of Khamoshiyan but also two other films –  . The writer of all the three films is Vikram Bhatt.   The game will have different characters and every character will unveil a dark secret and tell a different story as the player advances. 

Publicity team has planned a unique promotional event for the film with debutant Sapna Pabbi where in she would be chatting with men over Social Media sites. This movie is aimed by filmmakers to reach larger audience which helps their movie benefiting at the box office.

==Soundtrack==
{{Infobox album 
| Name        = Khamoshiyan
| Type        = Soundtrack
| Longtype    = to Khamoshiyan
| Artist      =  
| Cover       = 
| Border      = 
| Alt         = 
| Caption     = Soundtrack cover
| Released    =   Feature Film Soundtrack
| Recorded    = 
| Length      =   Sony Music
| Producer    =  Mahesh Bhatt  (Presenter)  Mukesh Bhatt
}}
They have purchased the rights of the song Aayega Aane Wala  from the film Mahal (1949 film).  
The soundtrack of Khamoshiyan has been composed by Ankit Tiwari, Jeet Ganguly, Navad Zafar & Bobby Imraan. The lyrics for the soundtrack are penned by Rashmi Singh and Sayeed Quadri. The first single of the album titled "Khamoshiyan" composed by Jeet Ganguly and crooned by Arijit Singh was released on 10 December 2014. The second single of the album titled "Tu Har Lamha" composed by Bobby Imraan and sung by Arijit Singh was released on 19 December 2014.

===Track listing===
{{track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length =  
| music_credits = yes Khamoshiyan (Title Track)
| extra1 = Arijit Singh
| music1 = Jeet Ganguly
| lyrics1 = Rashmi Singh
| length1 = 5:30
| title2 = Tu Har Lamha
| extra2 =  Arijit Singh
| music2 =  Bobby-Imran
| lyrics2 = Sayeed Quadri
| length2 = 4:32 Baatein Ye Kabhi Na (Male)
| extra3 = Arijit Singh
| lyrics3 =Sayeed Quadri
| music3 = Jeet Ganguly
| length3 = 4:49
| title4 = Kya Khoya
| extra4 = Naved Jafar
| lyrics4 = Rashmi Singh
| music4 = Naved Jafar
| length4 = 5:28 Baatein Ye Kabhi Na (Female)
| extra5 = Palak Muchhal
| lyrics5 = Sayeed Quadri
| music5 = Jeet Ganguly
| length5 = 4:49
| title6 = Subhan Allah
| extra6 = Anupam Amod, Imran Ali
| music6 =Bobby-Imran
| lyrics6 = Sayeed Quadri
| length6 = 4:31
| title7 = Bheegh Loon (Male)
| extra7 = Ankit Tiwari
| music7 = Ankit Tiwari
| lyrics7 = Abhendra Kumar Upadhyay
| length7 = 4:07
| title8 = Bheegh Loon (Female)
| extra8 = Prakriti Kakar
| lyrics8 = Abhendra Kumar Upadhyay
| music8 = Ankit Tiwari
| length8 = 4:07 Khamoshiyan (Unplugged)
| extra9 = Arijit Singh
| music9 = Jeet Ganguly
| lyrics9 = Rashmi Singh
| length9 = 2:30 Tu Har Lamha (Remix)
| extra10 =  Arijit Singh, DJ Angel
| music10 =  Bobby-Imran
| lyrics10 = Sayeed Quadri
| length10 = 4:18
| title11 = Bheegh Loon (Female - Remix)
| extra11 = Prakriti Kakar, DJ Angel
| lyrics11 = Abhendra Kumar Upadhyay
| music11 = Ankit Tiwari
| length11 = 3:15
| title12 = Khamoshiyan (Mashup)
| extra12 = DJ Angel
| length12 = 2:18
}}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 