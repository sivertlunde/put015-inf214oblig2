The Poacher's Pardon
{{infobox film name = The Poachers Pardon image =  caption =  director = Sidney Olcott producer = Kalem Company writer =  starring = Jack J. Clark Alice Hollister J.P. McGowan distributor = General Film Company cinematography = George K. Hollister
| released =  
| runtime = 1000 ft
| country = United States language = Silent film (English intertitles) 
}}

The Poachers Pardon is a 1912 American silent film produced by Kalem Company and distributed by General Film Company. It was directed by Sidney Olcott with Alice Hollister, Jack J. Clark and J.P. McGowan in the leading roles.

==Cast==
* Jack J. Clark - Jim Warren
* Alice Hollister - Dora Wallace
* J.P. McGowan - Wallace
* Helen Lindroth

==Production notes==
The film was shot in England.

==External links==
* 
*    website dedicated to Sidney Olcott

 
 
 
 
 
 
 
 
 
 


 