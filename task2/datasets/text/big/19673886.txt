So Red the Rose (film)
{{Infobox film
| name           = So Red the Rose
| image          =
| image_size     =
| caption        =
| director       = King Vidor
| producer       =
| writer         = Maxwell Anderson Edwin Justus Mayer Laurence Stallings Stark Young (novel)
| narrator       =
| starring       = Margaret Sullavan
| music          =
| cinematography = Victor Milner
| editing        = Eda Warren
| distributor    = Paramount Pictures
| released       =
| runtime        =
| country        = United States English
| budget         =
| preceded_by    =
| followed_by    =
}} Civil War-era romance is based on the 1934 novel of the same name by Stark Young.
 Gone with the Wind in 1939, an adaptation of Margaret Mitchells bestseller of the same name.

==Primary cast==
*Margaret Sullavan - Valette Bedford, a plantation mistress
*Walter Connolly - Malcolm Bedford, a planter and Valettes father
*Randolph Scott - Duncan Bedford, a planter, Valettes distant cousin who lives with her family
*Harry Ellerbe - Edward Bedford, Valettes brother
*Robert Cummings - Archie Pendleton,  Edwards friend from Texas
*Clarence Muse - Cato, a slave field hand
*Daniel L. Haynes - William Veal, a slave house servant Elizabeth Patterson - Mary, Valettes cousin

==References==
* Andre Sennwald, "King Vidors Screen Version of the Stark Young Novel So Red the Rose at the Paramount," The New York Times, November 28, 1935.
* Rodriguez, Junius P. Encyclopedia of Slave Resistance and Rebellion. Greenwood milestones in African American history. Westport, Conn: Greenwood Press, 2007. ISBN 0-313-33271-1

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 