Tlayucan
{{Infobox film
| name           = Tlayucan
| image          = 
| caption        = 
| director       = Luis Alcoriza
| producer       = Antonio Matouk
| writer         = Jesús Murciélago Velázquez Luis Alcoriza
| starring       = Julio Aldama
| music          = 
| cinematography = Rosalío Solano
| editing        = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}
Tlayucan is a 1962 Mexican comedy film directed by Luis Alcoriza and based on a novel by Jesús Murciélago Velázquez. It was nominated for the Academy Award for Best Foreign Language Film.   

==Cast==
* Julio Aldama as Eufemio Zárate
* Norma Angélica as Chabela
* Andrés Soler as Don Carlos
* Anita Blanch as Prisca
* Noé Murayama as Matías
* Juan Carlos Ortiz as Nico (as niño Juan Carlos Ortíz)
* Pancho Córdova as Sacristán
* Eric del Castillo as Doroteo
* Dolores Camarillo as Dolores (as Dolores Camarillo Fraustita)
* Antonio Bravo as Doctor
* Amado Zumaya as Máximo
* Joaquín Martínez

==See also==
* List of submissions to the 35th Academy Awards for Best Foreign Language Film
* List of Mexican submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 