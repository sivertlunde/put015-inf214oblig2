You'd Be Surprised (film)
{{Infobox film
| name           = Youd Be Surprised
| image          =
| image size     =
| caption        =
| director       = Arthur Rosson
| producer       = Adolph Zukor Jesse Lasky B. P. Schulberg
| writer         = Jules Furthman Robert Benchley
| narrator       =
| starring       = Raymond Griffith Dorothy Sebastian
| music          = William Marshall
| editing        = E. Lloyd Sheldon
| distributor    = Paramount Pictures
| released       = September 25, 1926
| runtime        = 60 minutes
| country        = USA English intertitles
| budget         =
| gross          =
}}
Youd Be Surprised is a silent film released in 1926 starring Raymond Griffith.  The film, a murder mystery-comedy film|comedy, was well received, and included title cards written by humorist Robert Benchley.  

The film is preserved in the Library of Congress collection.  

==Cast==
*Raymond Griffith - Mr. Green, The Coroner
*Edward Martindel - Mr. White, The District Attorney
*Earle Williams - Mr. Black, The Deputy District Attorney Thomas McGuire - Inspector Brown
*Dorothy Sebastian - Dorothy
*Granville Redmond - Grey, A Valet
*Roscoe Karns - A Party Guest
*Carl M. LeViness - A Party Guest
*Isabelle Keith - A Party Guest
*Dick La Reno - The Jury Foreman
*Monte Collins - The Milkman Juror
*Jerry Mandy - The Hot Dog Salesman Juror

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 