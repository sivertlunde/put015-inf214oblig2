The Honeymoon Killers
 

{{Infobox film
| name           = The Honeymoon Killers
| image          = the honeymoon killers poster.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Leonard Kastle
| producer       = Warren Steibel
| writer         = Leonard Kastle
| starring       = Shirley Stoler Tony Lo Bianco Marilyn Chris Doris Roberts
| music          = Gustav Mahler Oliver Wood
| editing        = Richard Brophy Stanley Warnow
| distributor    = Cinerama Releasing Corporation
| released       =  
| runtime        = 108 minutes 115 minutes  (original cut) 
| country        = United States
| language       = English
| budget         = $150,000 }}

The Honeymoon Killers is a 1969 American crime film written and directed by Leonard Kastle, and starring Shirley Stoler and Tony Lo Bianco. It is inspired by the true story of Raymond Fernandez and Martha Beck, the notorious "lonely hearts killers" of the 1940s.  The soundtrack is from the first movement of the 6th Symphony and a section of the 5th Symphony of Gustav Mahler. The Honeymoon Killers, went on to receive cult status as well as critical recognition. It was released on DVD for the first time by The Criterion Collection in 2003. François Truffaut called it his "favorite American film."   

==Plot==
Martha Beck is a sullen, overweight nursing administrator living in  ) surreptitiously submits Marthas name to a "lonely hearts" club, which results in a letter from Raymond Fernandez of New York City. The audience sees Ray surrounded by the photographs of his previous conquests as he composes his first letter to Martha. Overcoming her initial reluctance, Martha corresponds with Ray and becomes attached to him. He visits Martha and seduces her. Thereafter, having secured a loan from her, Ray sends Martha a Dear Jane letter, and Martha enlists Bunnys aid to call him with the (false) news that she has attempted suicide.
 con man who makes his living by seducing and then swindling lonely women. Martha is unswayed by this revelation. At Rays directive and so she can live with him, Martha installs her mother in a nursing home. Marthas embittered mother disowns her for abandoning her. Martha insists on accompanying Ray at his "work". Woman after woman accepts the attentions of this suitor who goes courting while always within sight of his "sister". Ray promises Martha he will never sleep with any of the other women, but complicates his promise by marrying pregnant Myrtle Young (played by Marilyn Chris). After Young aggressively attempts to bed the bridegroom, Martha gives her a dose of pills, and the two put the drugged woman on a bus. Her death thereafter escapes immediate suspicion.
 Albany (Mary Jane Higby) and takes her to the house he shares with Martha. Janet gives Ray a check for $10,000, but then becomes suspicious of the two. When Janet tries to contact her family, Martha and Ray hit her in the head with a hammer and strangle her to death. They bury her body beneath their cellar floor in her trunk, tossing into the graves dirt the two framed depictions of Jesus that, Martha notes sarcastically, shed told them she took everywhere she went.

Next, they spend several weeks living in Michigan with the widowed Delphine Downing and her young daughter. Delphine, younger and prettier than most of Rays conquests, confides in Martha, hoping that she will help her persuade Ray to marry her as soon as possible because she is pregnant with Rays child. Martha is in the midst of drugging Delphine when the womans daughter enters the room with Ray. He shoots Delphine in the head, and Martha, off camera, drowns the daughter in the cellar. Ray tells Martha that he must proceed with his plan to move on to one more woman, this one in New Orleans, and then he will marry Martha; he reaffirms his promise never to betray her with one of his marks. Realizing that Ray will never stop lying to her, Martha calls the police and waits calmly for them to arrive.

The epilogue takes place four months later, with Martha and Ray in jail. As she leaves the cellblock for the first day of their trial, Martha receives a letter from Ray in which he tells her that, despite everything, she is the only woman he ever loved. Titles on the screen then conclude the story, saying that Martha Beck and Raymond Fernandez were executed at Sing Sing on March 8, 1951. 

==Cast==
 
  Martha Beck Raymond Fernandez
*Doris Roberts as Bunny
*Marilyn Chris as Myrtle Young
*Barbara Cason as Evelyn Long
*Dortha Duckworth as Mrs. Beck 
*Mary Jane Higby as Janet Fay
 
*Kip McArdle as Delphine (actually, Deliphene) Price Downing
*Mary Breen as Rainelle Downing
*Ann Harris as Doris
*Elsa Raven as Matron
*Mary Engel as Lucy
*Guy Sorel as Mr. Dranoff
*Michael Haley as Jackson
 

==Production== Oliver Wood,
and Shirley Stoler and Tony Lo Bianco (both stage actors). A wealthy friend of Steibel, Leon Levy, suggested to Steibel that he make a film, and gave Steibel $150,000, the amount that Steibel suggested it would cost.  After deciding the film would be about Raymond Fernandez|"The Lonely Hearts Killers", Steibel asked Kastle, his roommate, to do some research on the subject; financial limitations led Steibel to ask his friend to write the screenplay. 

Steibel hired Martin Scorsese to direct, but Scorsese was fired for working too slowly; a few scenes he did were included in the final film. Industrial film-maker Donald Volkman took over, but lasted only two weeks. Kastle then stepped in as director for the last four weeks of principal photography. 

Budgetary constraints meant that actors did their own hair and makeup, and special effects were not fancy.  In the scene in which Martha bludgeons Janet Fay with a hammer, "condoms containing glycerine and red dye were affixed to the head of the victim with plaster of Paris. The hammer, a Ochroma pyramidale|balsa-wood prop, had a pin at the end. When the pin pricked the condoms, the blood began to flow." 

==Reception==
The film was initially marketed as an exploitation film; it "performed weakly" at the U.S. box office in spite of critical praise.  For example, Variety (magazine)|Variety magazine said it was "made with care, authenticity and attention to detail."   Its "modest financial success" in Britain and France probably meant that its financial backer recouped his investment. 

When  . 

The film currently holds a 94% rating on Rotten Tomatoes, based on 18 reviews. 

==Historical accuracy==
Although the film is inspired by true events and uses the real names of Raymond Fernandez|"The Lonely Hearts Killers" and of those they murdered, as well as the true locations of the crimes, the film takes substantial liberties with the facts, contrary to the opening titles. Although the actual events unfolded in the late 1940s, the film is set at the time of its making, the late 1960s. The film presents the killings as commencing with Becks entrance on the scene and as a consequence of her jealousy. In contrast, it is believed that Fernandez had murdered at least one of the victims of his swindling, Jane Wilson Thompson, before meeting Beck, in order to pose as Thompsons widower and claim title to her property, including the apartment in New York where Beck joined him.  There is no acknowledgment that Beck was divorced with two children.  whom she abandoned on Fernandezs orders (her abandonment of her mother is substituted); nor is there mention of Fernandezs legal wife and four children in Spain. 

The film depicts Becks surrender of herself and Fernandez, but that implied demonstration of remorse is contrary to the historical record: neighbors noticed the Downings disappearance, and Beck and Fernandez were apprehended at the Downing house after returning from an evening at the movies. 

==See also==
*Alleluia (film)|Alleluia, a 2014 film about the same events Lonely Hearts, a 2006 film about the same events.
*Deep Crimson, a 1996 film about the same events.

==Notes==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 