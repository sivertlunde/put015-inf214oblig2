Time of My Life (film)
{{Infobox film
| name           = Time of My Live
| image          =
| alt            =
| caption        =
| director       = Nic Balthazar
| producer       = Peter Bouckaert
| writer         = Nic Balthazar
| starring       =
| music          = Henny Vrienten
| cinematography = Danny Elsen
| editing        = Philippe Ravoet
| studio         =
| distributor    = Kinepolis Film Distribution
| released       =  
| runtime        = 119 minutes
| country        = Belgium
| language       = Dutch
| budget         =
| gross          =
}}
Time of My Life ( ) is a 2012 Belgian film written and directed by Nic Balthazar. The story is based upon real life events and contains original news footage.

== Plot ==

The story is set between 1980 and 2002. In 1980Mario Verstraete was a healthy Belgian with big ambitions. Some years later he is diagnosed with multiple sclerosis. Mario decides to advocate for legalizing euthanasia. He starts up an action group to convince the government to allow the practice under restricted conditions.

His plan works and the Belgian government approves an enactment on 28 May 2002:  euthanasia is allowed when an adult victim cant be cured, has to live in unworthy conditions, and at least three doctors confirm this situation. Verstraete was the first Belgian to commit euthanasia on 30 September 2002.   

== Recognition ==

=== Awards ===
The movies awards include:

* Flemish Movie Award for best movie at the Ostend Film Festival 2012
* Flemish Movie Award for best actor at the Ostend Film Festival 2012
* Flemish Movie Award for best editing at the Ostend Film Festival 2012
* Public favorite at the Seminci film festival in Spain
* Public favorite at The End film festival in Amsterdam
* Grand Prix Hydro-Québec at the International Film Festival of Abitibi   

=== Nominations ===

* Festroia International Film Festival 2012   

== References ==
 

 
 
 
 