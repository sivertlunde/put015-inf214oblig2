God Tussi Great Ho
 
{{Infobox film
| name            = God Tussi Great Ho
| image           = godtussigreatho.jpeg|
| image_size      = 
| caption         = Theatrical release poster
| director        = Rumi Jaffrey Afzal Khan Sohail Khan
| story           = Rumi Jaffery Yunus Sajawal
| starring        = Salman Khan Priyanka Chopra Amitabh Bachchan Sohail Khan

| music           = Sajid-Wajid
| cinematography  = Ashok Mehta
| editing         = Pranay Patel
| distributor     = Sohail Khan Productions T-Series Eros International
| released        = 15 August 2008
| based on = Bruce Almighty
| runtime         = 152 mins.
| country         = India
| language        = Hindi
| budget          = 
| gross           =    
}}
 fantasy comedy comedy film, 2003 Hollywood comedy Bruce Almighty, starring Jim Carrey and Jennifer Aniston.  

==Plot== TV anchor, but success has always eluded him. He blames God (Amitabh Bachchan) for this lack of success. Arun is head over heels in love with Alia Kapoor(Priyanka Chopra), who is a TV anchor and a well-known star working in the same TV channel|channel, but he has never been able to express his love for her.

When Rocky (Sohail Khan) is appointed as an anchor for the channel, Arun starts believing that Rocky will win Alia. Later, Arun is sacked from the channel. He has nobody to blame but God Himself, whom he later meets in person. An argument ensues between the two, at the end of which God then decides to give Arun the power over all things for ten days, wherein Arun may prove that he is a better operator of the universe.

Arun uses this power to put Rocky into an uneasy spot and win Alias heart. After God scolds him for only making things better for himself, Arun starts listening to peoples prayers. He later realizes that considering each persons wishes individually would be too time-consuming, so to save time and effort he grants everybodys wishes, including the wish of criminals to be free and Rockys wish that Alia marries him.

He later asks God why is everything happening towards him. God explains it is his own fault and that everyone cannot blame Him for this. Arun feels bad, but God forgives him and starts his life over again. During the game show, he tricks Rocky into lying about loving Alia Kapoor (who was present on the show, as it was the channel "Zoop"s first game show), and Alia turns on him. Arun wins the heart of Alia.

==Cast==
* Salman Khan as Arun Prajapati
* Priyanka Chopra as Alia
* Amitabh Bachchan as God
* Sohail Khan as Rakesh Sharma a.k.a. Rocky
* Satish Kaushik as Netaji (Special Appearance)
* Anupam Kher as Aruns father
* Dilip Tahil as Aruns Boss
* Beena Kak as Aruns mother
* Upasana Singh as Aruns maid
* Rukhsar Rehman as Madhu Prajapati

==Box office==
God Tussi Great Ho did not have a good run at the box office and was declared flop by Box Office India, apart from a few single screens.  However the movie has emerged as a moderate success on the home video circuit. 

==Soundtrack==
{{Infobox album |  
 Name = God Tussi Great Ho
| Cover       = GTGH albumcover.jpg
| Caption     =
| Type = Soundtrack
| Artist = Sajid-Wajid
| Released =  15 March 2008 (India)
| Recorded = Feature film soundtrack
| Length =
| Label =  T-Series
| Producer = Sajid-Wajid
| Reviews =
| Last album = Welcome (2007 film)|Welcome (2007)
| This album = God Tussi Great Ho (2008)
| Next album = Hello (2008 film)|Hello (2008)
}}
The music of the film is composed by Sajid-Wajid, and the lyrics are penned by Jalees Sherwani, Shabbir Ahmed and Deven Shukla. 

=== Track listing ===
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
! # !! Song !! Singer(s)!! Length
|-
| 1
| "Lets Party"
| Shaan (singer)|Shaan, Sunidhi Chauhan
| 4:56
|-
| 2
|"Tujhe Aksa Beach Ghuma Doon"
| Sajid-Wajid|Wajid, Amrita Kak
| 4:10
|-
| 3
| "God Tussi Great Ho"
| Sonu Nigam, Shankar Mahadevan
| 4:31
|-
| 4
|"Tumko Dekha"
| Neeraj Shridhar, Shreya Ghoshal
| 5:34
|-
| 5
|"Lal Chunariya"
| Alka Yagnik, Udit Narayan
| 4:18
|-
| 6
|"Tujhe Aksa Beach Ghuma Doon (Remix)"
| Sajid-Wajid|Wajid, Amrita Kak
| 3:58
|-
| 7
|"Lets Party (Remix)"
| Shaan (singer)|Shaan, Sunidhi Chauhan
| 4:34
|-
| 8
|"God Tussi Great Ho (Remix)"
| Sonu Nigam, Shankar Mahadevan
| 3:56
|}

==See also==
*List of Indian film selections and winners at the Cannes Film Festival

==References==
 

==External links==
*  
*  
*  

 
 
 
 