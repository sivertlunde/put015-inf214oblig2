The Ugly American
 
{{Infobox film
| name           = The Ugly American
| image          = The Ugly American poster.jpg
| caption        = original movie poster
| director       = George Englund
| producer       = George Englund
| screenplay        = Stewart Stern
| based on       = The Ugly American by Eugene Burdick and William Lederer
| starring       = {{Plain list |
* Marlon Brando
* Sandra Church
* Eiji Okada
* Pat Hingle
* Judson Pratt Arthur Hill
}}
| music          =
| cinematography = Clifford Stine
| editing        = Ted J. Kent
| distributor    = Universal Pictures
| released       = 1963
| runtime        = 115 minutes
| language       =
| budget         =
| gross          = est. $3,500,000 (US/ Canada) 
}}
The Ugly American is a 1963 film starring Marlon Brando and directed by George Englund. It was based on a 1958 political novel by Eugene Burdick and William Lederer. The novel became a bestseller, was influential at the time, and is still in print. The film and book are a quasi-roman à clef; that is, they present, in a fictionalized guise, the experience of Americans in Southeast Asia (Vietnam) and allegedly portrays several real people who are represented by pseudonyms.

==Plot==
 

==1958 novel== hearts and minds in Southeast Asia—because of innate arrogance and the failure to understand the local culture.  The title is actually a double entendre, referring both to the physically unattractive hero, Homer Atkins, and to the ugly behavior of the American government employees. 

In the novel, a Burmese journalist says "For some reason, the   people I meet in my country are not the same as the ones I knew in the United States. A mysterious change seems to come over Americans when they go to a foreign land. They isolate themselves socially. They live pretentiously. They are loud and ostentatious." 

The "Ugly American" of the book title fundamentally refers to the plain-looking engineer Atkins, who lives with the local people, who comes to understand their needs, and who offers genuinely useful assistance with small-scale projects, such as the development of a simple bicycle-powered water pump. It is argued in the book that the Communists were successful because they practiced tactics similar to those of Atkins.  
 International Cooperation Agency technician named Otto Hunerwadel, who, with his wife Helen, served in Burma from 1949 until his death in 1952.  They lived in the villages, where they taught farming techniques, and helped to start home canning industries. 

Another of the books heroes, Colonel Hillandale, appears to have been modeled on the real-life United States Air Force|U.S. Air Force Lieutenant General Edward Lansdale, who was an expert in Counter-insurgency|counter-guerrilla operations.

After the book had gained wide readership, the term Ugly American (epithet)|"Ugly American" came to be used to refer to the "loud and ostentatious" type of visitor in another country, rather than the "plain looking folks, who are not afraid to get their hands dirty like Homer Atkins" to whom the book itself referred.
 John Kennedy was gripped by The Ugly American. In 1960, he and five other opinion leaders bought a large advertisement in the New York Times, saying that they had sent copies of the novel to every U.S. Senator, because its message was so important." 

== Production ==
The novel was made into a film in 1963 starring Marlon Brando as Ambassador Harrison Carter MacWhite. Its screenplay was written by Stewart Stern, and the film was produced and directed by George Englund. The film was shot mainly in Hollywood, with Thailand serving as the inspiration for the background sceneries. Parts of the film were also shot on locations in Bangkok, Thailand, including at Chulalongkorn University, one of the leading institutes of higher learning of the country. Upon release, the film garnered generally positive reviews from critics. Review aggregator Rotten Tomatoes reports that 80% of critics have given the film a positive review, with a rating average of 6.1/10. 

Kukrit Pramoj, a Thai politician and scholar, was hired as a cultural expert/advisor to the film and later played the role of Sarkhans Prime Minister "Kwen Sai". Later on, in 1975, he, in fact, did become the 13th Prime Minister of Thailand. Probably because of this, the word "Sarkhan" has entered the Thai language as a nickname of Thailand itself, often with a slight self-deprecating or mocking tone. 

==Cast==
* Marlon Brando as Ambassador Harrison Carter MacWhite
* Eiji Okada as Deong
* Sandra Church as Marion MacWhite
* Pat Hingle as Homer Atkins Arthur Hill as Grainger
* Jocelyn Brando as Emma Atkins Mom Rajawongse Kukrit Pramoj as Prime Minister Kwen Sai
* Judson Pratt as Joe Bing
* Reiko Sato as Rachani, Deongs Wife
* George Shibata as Munsang
* Judson Laire as Senator Brenner
* Philip Ober as Ambassador Sears
* Yee Tak Yip as Sawad, Deongs Assistant
* Carl Benton Reid as Senator at Confirmation Hearing

== References ==
  

== External links ==
*  

 
 
 