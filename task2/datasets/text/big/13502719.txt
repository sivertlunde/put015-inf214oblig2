Bare Fists
 
{{Infobox film
| name           = Bare Fists
| image          =
| caption        =
| director       = John Ford Pat Powers
| writer         = Eugene B. Lewis Bernard McConville Harry Carey
| cinematography = John W. Brown
| editing        =
| distributor    =
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 Western film Harry Carey. The film is considered to be lost film|lost.   

John Ford and actor Harry Carry collaborated on at least 25 films in the years of 1917-1921.  
During these collaborations, Carry made more per film then Ford.  By 1919 Ford was making 300 dollars a week, Carry was making 1,250.  This differential in pay led to tension between the two.
 

==Plot==
The film takes place in a lawless town in southwestern Kanasas during the era of outlaws and cowboys.  After the marshal is killed while breaking up a saloon fight, his son, Cheyenne Harry avenges his father’s death by killing two of the men involved.  His mother pleads with him to never carry a gun again and Cheyenne Harry agrees.

Harry is wooing the beautiful Conchita.  Conchita is also being wooed by the devious Boone Travis.  Travis, in order to eliminate his rival, murders a man and frames Harry.  Harry is sentenced to die, but is allowed one last visit to see his mother.
During this trip he is told that his brother Bud was attacked and branded by cattle thieves.  Harry escapes custody and punishes the men who attacked his brother.  He is also cleared of the charge of murder.

==Cast== Harry Carey as Cheyenne Harry
* Betty Schade as Conchita Joe Harris as Boone Travis
* Vester Pegg as Lopez
* Mollie McConnell as Conchitas Mother (as Molly McConnell)
* Anna Mae Walthall as Ruby (as Anna May Walthall)
* Howard Enstedt as Bud
* Joseph W. Girard as Harrys Father (as Joseph Girard)

==See also==
* List of American films of 1919
* Harry Carey filmography
* John Ford filmography
* List of lost films

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 