Intimate Stories
{{Infobox film
| name           = Historias mínimas
| image          = Historiasminimas.jpg
| caption        = Theatrical release poster
| director       = Carlos Sorín
| producer       = Martin Bardi
| writer         = Pablo Solarz
| starring       = Javier Lombardo Antonio Benedicti Javiera Bravo
| music          = Nicolás Sorín Hugo Colace
| editing        = Mohamed Rajid
| distributor    = Guacamole Films
| released       =  
| runtime        = 92 minutes
| country        = Argentina Spain Spanish
| budget         =
|}} Argentine and Spanish drama film, directed by Carlos Sorín and written by Pablo Solarz. The film was produced by Martin Bardi, Leticia Cristi, and José María Morales. It features, among others, Javier Lombardo, Antonio Benedicti, and Javiera Bravo. 
 Santa Cruz Province, Patagonia.

==Plot==
Don Justo (Antonio Benedicti) hands over the running of his grocery store to his overbearing son and daughter-in-law and escapes to search for his lost dog Badface. 

Roberto (Javier Lombardo) is a love-struck obsessive-compulsive traveling salesman who drives to San Julián to surprise one of his clients by bringing a cake for her childs birthday.

Finally, María Flores (Javiera Bravo) travels to San Julián with her daughter because she has won a spot on "Multicoloured Casino," a fusty TV game show. 

The film captures a lot of small details that make a realistic and moving depiction of life in southern Argentina.

==Cast==
* Javier Lombardo as Roberto
* Antonio Benedicti as Don Justo Benedictis
* Javiera Bravo as María Flores
* Julia Solomonoff as Julia
* Laura Vagnoni as Estela
* Enrique Otranto as Carlos
* Mariela Díaz as Marías friend
* María Rosa Cianferoni as Ana
* María del Carmen Jiménez as Female Baker
* César García as García
* Armando Grimaldi as El mesero
* Mario Splanguño as Panadero
* Rosa Valsecchi as Panadera #2
* Aníbal Maldonado as Fermín

==Background==

===Casting===
In a Neorealism (art)|neo-realist fashion, the film director used mostly non-professional actors; the only professional actor was Javier Lombardo (Roberto).

==Distribution==
The film was first presented at the Donostia-San Sebastián International Film Festival, Spain on September 26, 2002, and was released in Argentina on October 24, 2002.

It was featured at various film festivals, including the International Film Festival, Rotterdam; the Latin America Film Festival, Poland; the Karlovy Vary Film Festival, Czech Republic; the Copenhagen International Film Festival, Denmark; the Bergen International Film Festival, Norway; the Spanish Film Festival, Philippines; Havana Film Festival, Cuba; the Cartagena Film Festival, Colombia; the Festróia - Tróia International Film Festival, Portugal; the Fribourg International Film Festival, Switzerland; the Tromsø International Film Festival, Norway, and the Uruguay International Film Festival, Uruguay.

In the United States it was shown at the Sundance Film Festival in January, 2003, then released in New York on March 4, 2005.

==Critical reception==
Tom Dawson, film critic for the BBC wrote, "Patagonian landscapes with the modesty of his characters aspirations, Sorín has crafted an appealing portrait of this remote region, where television provides the inhabitants with their main link to the wider world. Convincingly acted by the mainly non-professional cast, Historias mínimas is further proof of the diversity and strength of contemporary Argentine cinema." 

Ed Gonzales, a critic for   Short Cuts, but the pursuit of enlightenment and the poetic texture of Soríns images similarly evokes David Lynch|Lynchs The Straight Story. Quiet and unpretentious, the films humanism isnt confrontational exactly but its intense nonetheless." 

==Awards==
Wins
*   Prize – Special Mention (Carlos Sorín); SIGNIS Award – Special Mention (Carlos Sorín); and Special Prize of the Jury (Carlos Sorín); 2002.
* Havana Film Festival: Grand Coral – Second Prize (Carlos Sorín); Martin Luther King Memorial Center Award (Carlos Sorín); 2002.
* Argentine Film Critics Association Awards: Silver Condor for best director (Carlos Sorín); Best Film; Best Music (Nicolás Sorín); Best Male (Antonio Benedicti); Best Original Script (Pablo Solarz); Best Artistic Direction (Margarita Jusid); Best Cinematography (Hugo Colace); and Best Sound (Carlos Abbate and José Luis Díaz); 2003.
* Cartagena Film Festival: Special Jury Prize (Carlos Sorín); 2003.
* Festróia - Tróia International Film Festival: Golden Dolphin (Carlos Sorín); 2003.
* Fribourg International Film Festival: Grand Prix (Carlos Sorín); 2003.
* Los Angeles Latino International Film Festival: Best Film; 2003.
* Tromsø International Film Festival: Aurora Award – Special Mention (Carlos Sorín); 2003.
* Uruguay International Film Festival: Best Film; 2003.
* Uruguayan Film Critics Association Awards 2003: Best Latinamerican film; 2003.
* Goya Awards: Best foreign Spanish language film; 2004.

Nominations
* San Sebastián International Film Festival: Golden Seashell, Carlos Sorín; 2002.
* Argentine Film Critics Association Awards: Silver Condor, Best Editing, Mohamed Rajid; Best New Actress, Javiera Bravo; Best New Actress, Julia Solomonoff; Best Supporting Actor, Javier Lombardo; 2003.
* Cartagena Film Festival: Golden India Catalina, Best Film, Carlos Sorín; 2003.
* Ariel Awards, Mexico: Silver Ariel, Best Latin-American Film, Carlos Sorín, Argentina; 2004.

==References==
 

==External links==
*  
*  
*   at the cinenacional.com  
*  

 
 

 
 
 
 
 
 
 
 
 
 