Matinee (2012 film)
{{Infobox film
| name           = Matinee
| image          = Matinee (2012 film).jpeg
| alt            = 
| caption        = 
| director       = Aneesh Upasana
| producer       = AOPL Entertainment
| writer         = Anil Narayanan
| starring       = Maqbool Salmaan Mythili
| music          = Songs:  
| cinematography = Pappinu
| editing        = Nikhil Venu
| studio         = AOPL Entertainment
| distributor    = AOPL Entertainment Release
| runtime        = 114 minutes
| online media partner = PrimeGlitz Media
| released       =  
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Matinee is a 2012   show.

The films lead role is played by Maqbool Salmaan who had earlier appeared in a short role in A. K. Saajans Asuravithu (2012 film)|Asuravithu. Maqbool is the nephew of noted actor Mammootty. He is the son of Ibrahimkutty, the younger brother of Mammootty who had been a popular star in the miniscreen. The film is produced by AOPL Entertainment which also launched Mammoottys son Dulquer Salmaan earlier the year through Second Show. 

Matinee released on 13 December to positive reviews from critics and audiences. The film was appreciated for its realistic approach and the performances by lead actors.

==Plot==
The plot of this film revolves around two people who come from different backgrounds but are united by fate. Najeeb (Maqbool Salmaan) is a youngster from an orthodox background who wishes to be a film star while Savithri (Mythili) is a motherless girl from an economically backward family who lands up in the film industry to escape her abusive father (Sasi Kalinga). The duo get a chance to be the lead pair in a film that appears to be an ordinary love story but turns out to be a sleaze-fest when it releases. The pair, subsequently, is ostracised from society.

==Cast==
* Maqbool Salmaan as Najeeb
* Mythili as Savithri Lena
* Thalaivasal Vijay
*Valsala Menon
* Sasi Kalinga
* Kiran Raj
* Soja Jolly
* Valsala Menon
* Dinesh nair 
* Fathima Babu
* Anjali Upasana

==Release==
The films release was delayed by several weeks with a decision from the Kerala Film Exhibitors Association, who resisted the release of the film in their theatres. It was widely reported that an unofficial ban from the exhibitors came in the wake of AOPL-owned Carnival Cinemas, Angamaly not participating in a theatre strike organised by the Association. But AOPL maintained that the ban was based on a technical error as AOPL Entertainment is a separate private limited company and Carnival belongs to another company though both have some common investors. The ban was lifted after a discussion between AOPL and the Association and the film released on 13 December 2012. 

===Reviews===
The film opened to mostly positive reviews from critics. Paresh C. Palicha of Rediff.com rated the film   and said that the film "tackles a dark subject with much conviction." He concluded the review saying, "Matinee shows that there are many interesting avenues to explore if you want to tell the story of the film industry other than lampooning the superstars, Sreenivasan style."  Dalton L. of Deccan Chronicle rated the film   while appreciating the performances by the lead, editing, cinematography and music. The critic stated: "One occasionally stumbles upon a rotten canvas with strokes of a genius or a great canvas tarnished by splashes of awkwardness. Matinee is neither of the extremes; it simply appears to have been created either by two different painters or one with opposing moods / sensibilities." 

Rating the film  , Veeyen of Nowrunning.com said, "Matinee is a dark parable that splendidly traces the ever changing contours of human lives. It reaffirms that the human power of endurance is immense and that as individuals we wobble along alleys of hope, fear, distress, anger and regret before turning around and walking down the hope street all over again."  IBN Live rated the film   and said that the film "stands out for its realistic approach." 

===Controversies===
The film courted considerable controversy with the item dance by Mythili.  Another row originated when anti-tobacco activists turned against the film, thanks to the movie posters portraying Mythili with a lit cigarette and a bottle of booze. Activists of the Ernakulam district Anti-tobacco Council protested against the film and blackened several posters of the movie in the city. They also filed a petition before the police and the district collector against the movie posters. "When youngsters have slowly started quitting and keeping away from cigarettes, this movie has come up with posters of its actor with a cigarette. The movie-makers have failed to put up a statutory warning too. This certainly affects the awareness created by Kerala Voluntary Health Services and other anti-tobacco organisations," said K. S. Dilip Kumar, secretary of the Ernakulam district Anti-tobacco Council.    A case was registered by the Health Department against Mythili and the producers of the film on 19 December. 

==Music==
The film features songs composed by Ratheesh Vegha and Bollywood composer Anand Raj Anand. Anand composed the song "Ayalathe Veetile" written by Vinukrishnan, an item number featuring Mythili.  The song was a profound success and hit 4.5 lakh views on YouTube in just two weeks of its release.  Gireesh Puthencherys son Dinnath Puthenchery has penned the lyrics for two of the songs in the movie. Actress Kavya Madhavan has lent her voice to the song "Maunamayi Manassil" which was also well received among music critics.

{{tracklist
| headline     =
| extra_column = Artist(s)
| total_length =
| title1       = Ayalathe veettile
| extra1       = Resmi Sateesh
| length1      = 3:58
| title2       = Malabarin thalamai
| extra2       = Thulasi
| length2      = 3:50
| title3       = Maunamayi manassil
| extra3       = Kavya Madhavan
| length3      = 3:54
}}

==References==
 

 
 
 
 
 
 
 
 