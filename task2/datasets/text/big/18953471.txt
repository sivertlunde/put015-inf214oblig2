Yes Man (film)
 
 
{{Infobox film
| name           = Yes Man
| image          = YesMan2008poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Peyton Reed
| producer       = {{Plain list |
*David Heyman
*Richard D. Zanuck
}}
| screenplay     = {{Plain list |
*Nicholas Stoller
*Jarrad Paul
*Andrew Mogel
}}
| based on       =  
| starring       = {{Plain list |
*Jim Carrey
*Zooey Deschanel
*Bradley Cooper
*John Michael Higgins
*Terence Stamp
}}
| music          = {{Plain list |
*Lyle Workman
*Mark Oliver Everett
}}
| cinematography = Robert D. Yeoman
| editing        = Craig Alpert
| studio         = {{Plain list |
*Village Roadshow Pictures The Zanuck Company
*Heyday Films
}} Warner Bros. Pictures
| released       = January 2009
| runtime        = 104 minutes
| country        = {{Plain list |
*United States
*United Kingdom
}}
| language       = English
| budget         = $70 million
| gross          = $223.2 million 
}}

 Yes Man Danny Wallace, who also makes a cameo appearance in the film.

The film was a box office success, despite receiving mixed reviews from critics. It was released on December 19, 2008, opening at No. 1 at the box office in its first weekend with $18.3 million and was then released on December 26, 2008 in the United Kingdom going straight to the top of the box office in its first weekend after release. Production for the film began in October 2007 in Los Angeles.

==Plot==
 
Bank loan officer Carl Allen (Jim Carrey) has become withdrawn since his divorce from ex-wife Stephanie. Routinely ignoring his friends Pete (Bradley Cooper) and Rooney (Danny Masterson), he has an increasingly negative outlook on his life. He subsequently misses Petes engagement party. An irate Pete turns up at his house and berates him, telling him that he will end up completely lonely if he does not change his life. Coincidentally, an old colleague suggests that he goes to a motivational "Yes!" seminar with him, which encourages its attendants to seize the opportunity to say "Yes!". Carl decides to attend the seminar and meets inspirational guru Terrence Bundley (Terence Stamp), who publicly browbeats him into making a covenant with himself. Carl reluctantly promises to stop being a "No Man" and vows to answer "Yes!" to every opportunity, request, or invitation that presents itself thereafter.
 Elysian Park. Disillusioned, he hikes to a gas station where he meets Allison (Zooey Deschanel), an unorthodox young woman. She gives him a ride back to his car on her scooter and kisses him before leaving. After this positive experience, Carl feels more optimistic about saying yes. However, he refuses oral sex from his elderly neighbor Tillie (Fionnula Flanagan), which results in falling down the stairs and almost getting attacked by a dog. Seeing the repercussions of saying no, he goes back to Tillie. 
 Munchausen by Proxy whose lead singer turns out to be Allison. He is charmed by her quirkiness; she is charmed by his spontaneity and the two begin dating.

As their relationship develops, Carl and Allison meet at the airport for a spontaneous weekend excursion. Having decided to take the first plane out of town, regardless of its destination, they end up in Lincoln, Nebraska, where they bond more. As they take shelter from rain, Allison asks Carl to move in with her and he hesitantly agrees. While checking in for the return flight, Carl and Allison are detained by FBI agents who have profiled him as a potential terrorist because he has taken flying lessons, studied Korean, approved a loan to a fertilizer company, met an Iranian, and bought plane tickets at the last minute. Pete, his attorney, travels to Nebraska to explain Carls odd habits, lessons, and decisions. As she finds out about Carls motivational covenant, Allison begins to doubt whether his commitment to her was ever sincere. Deciding that she can no longer trust him, Allison leaves Carl and refuses to return his phone calls.

Carls life takes a turn for the worse and he almost forgets about Lucys shower. He manages to arrange a major surprise shower, set his friend Norm up with Soo-Mi (Vivian Bang), a Korean girl, and Rooney with Tillie. After the party, Carl receives a tearful phone call from Stephanie, whose new boyfriend has walked out on her. When Carl goes to Stephanies apartment to comfort her, she kisses him and asks him to spend the night with her. After Carl emphatically says no, his luck takes a turn for the worse and he decides to end his commitment to the covenant.

Carl goes to the convention center and hides in the backseat of Terrences convertible so that he can beg to be released from the covenant. Carl emerges as Terrence drives off, and an oncoming vehicle collides with the startled and distracted Terrence. The two are taken to a hospital. After Carl recovers consciousness, Terrence tells Carl that there was no covenant. The starting point was merely to open Carls mind to other possibilities, not to permanently take away his ability to say no if he needed to. Freed from this restraint, Carl finds Allison teaching a sports-photography lesson and admits that he is not ready to move in with her just yet, but that he genuinely loves her, and they reconcile with a French kiss (with people in Allisons photography lesson subsequently taking pictures of them making out).

==Cast==
 
* Jim Carrey as Carl Allen
* Zooey Deschanel as Allison
* Bradley Cooper as Peter, Carls best friend
* John Michael Higgins as Nicholas "Nick" Lane
* Rhys Darby as Norman, Carls boss
* Maile Flanagan as Janet
* Danny Masterson as Rooney
* Terence Stamp as Terrence "Terry" Bundley
* Sasha Alexander as Lucy Burns, Peters fiancée
* Molly Sims as Stephanie, Carls ex-wife
* Aaron Takahashi as Lee
* Fionnula Flanagan as Tillie, Carls eldery neighbor
* Sean OBryan as Ted
* John Cothran as Tweed
* Spencer Garrett as Multack
* Rocky Carroll as Wesley T. Parker
* Vivian Bang as Soo-Mi
* Luis Guzmán as Jumper
* Arne Starr as Conventioner (uncredited) Danny Wallace as Conventioner (uncredited)
* Brent Briscoe as Homeless Guy
 

==Production== Danny Wallace. The book tells of the 6-month period in which he committed himself to saying Yes to everything based on a brief conversation with a stranger he met on the bus. Danny Wallace also has a cameo in the film, in the final bar-scene of the movie, in which he is speaking to someone behind Danny Masterson. Reynolds, Simon (December 10, 2008). Video: DS at the Yes Man premiere. Digital Spy, December 10, 2008. Retrieved from http://www.digitalspy.co.uk/movies/a138060/video-ds-at-the-yes-man-premiere.html. 

Jim Carrey declined an upfront salary for his role in the film. He was instead paid 36.2% of the films gross after its production and marketing costs were recovered. 
 bungee jumps off a bridge, Carrey interrupted and asked to do the stunt himself.  Carrey stated to the stunt double that he intended to do it in one take. When he jumps off, he is seen taking out a cell phone for the scene.

While shooting the scene in the bar where Carreys character turns around into a waitress and falls flat on his back, Carrey executed the stunt incorrectly and fell to the floor harder than he expected, breaking three ribs in the process. 

Carrey also mastered basic  " by Third Eye Blind, which had a high number of digital downloads after the films theatrical release.  After the final date of filming, Carrey "retired" his set guitar, and Deschanel kept it. When asked about this, Carrey said: "Ill never need that, or any guitar ever again; guitar is not for me! Never has been, never will be!" 
 Amoeba in Hollywood. {{cite web url = http://suicidegirls.com/interviews/Von+Iva%3A+Yes+Women/ title = publisher = SuicideGirls.com date = accessdate =December 9, 2008}}  For the DVD/Blu-ray release of the film, Deschanel and Von Iva filmed a spoof MTV music show-style documentary on the band for which they filmed mock music videos for several of the songs; the home video release also includes full-length performances by the group that were not included in the film.

The soundtrack also features nine songs by Eels (band)|Eels, including a brand-new song entitled "Man Up". 
 Separate Ways" by Journey (band)|Journey. It is also featured when Carreys character bails out from the hospital to catch the joggography at 6&nbsp;am.

==Release==

===Critical reception===
Yes Man generated mixed reviews. Review aggregator Rotten Tomatoes gave the film a rating of 45%, based on 152 reviews, with an average rating of 5.3. The sites consensus reads, "Jim Carreys comic convulsions are the only bright spots in this otherwise dim and predictable comedy."  On Metacritic, the film has a score of 46 out of 100, based on 30 critics, indicating "mixed or average reviews". 

Many critics thought that its plot was too similar to Carreys 1997 work, Liar Liar. 

In his review for The Miami Herald, Rene Rodriguez wrote, "Yes Man is fine as far as Jim Carrey comedies go, but its even better as a love story that just happens to make you laugh,"  while Kyle Smith of The New York Post countered in his review that, "The first time I saw Yes Man, I thought the concept was getting kind of stale toward the end. As it turns out, that was only the trailer."  Roger Ebert of the Chicago Sun Times gave the film 2 stars out of 4. 

===Box office===
Despite the mixed critical reception, the film opened No. 1 in its first weekend at the US box office with $18.3 million,  and went straight to the top of the UK box office in its first weekend after release. 
 Fun with Dick and Jane but falling short of his 2003 film Bruce Almighty.

===Accolades===

;2009 BMI Film Music Award

* Best Music – Lyle Workman (Won)

;2009 Taurus World Stunt Awards

* Best Overall Stunt by a Woman – Monica Braunger (Nominated)

;2009 Artios Awards

* Best Casting – David Rubin & Richard Hicks (Nominated)

;2009 MTV Movie Awards

* Best Comedic Performance – Jim Carrey (Won)

;2009 Teen Choice Awards

* Choice Movie Actor – Comedy – Jim Carrey (Nominated)
* Choice Movie Rockstar Moment – Jim Carrey (Nominated)
* Choice Movie Hissy Fit – Jim Carrey (Nominated)
* Choice Movie: Comedy (Nominated)

;2009 Kids Choice Awards

* Favorite Movie Actor – Jim Carrey (Nominated)

===Home media===
The DVD and Blu-ray were released on April 7, 2009. Customers have the option of the single-disc edition and the 2 disc edition titled the "Ultimately Yes!" edition.

==References==
 

==Further reading==
* 

==External links==
 
*  
*  
*  
*  
*   Production Details

 

 
 
 
 
 
 
 
 
 
 
 