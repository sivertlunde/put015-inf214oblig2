A Rage to Live
{{Infobox Film
| name           = A Rage to Live
| image          = RageToLive.jpg
| image_size     = 
| caption        = Original poster
| director       = Walter Grauman
| producer       = Lewis J. Rachmil
| writer         = John OHara (novel) John T. Kelley
| narrator       = 
| starring       = Suzanne Pleshette Bradford Dillman Ben Gazzara
| music          = Nelson Riddle 
| cinematography = Charles Lawton, Jr.
| editing        = 
| studio         = 
| distributor    = United Artists
| released       = 
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1965 United American drama film directed by Walter Grauman and starring Suzanne Pleshette as a woman whose passions wreak havoc on her life. The screenplay by John T. Kelley is based on the 1949 novel of the same name by John OHara.

==Plot==
Newspaper heiress Grace Caldwells nymphomania threatens to destroy the reputation of her staid, wealthy Pennsylvania family. As a precocious teenager, she succumbs to the advances of her older brother Brocks friend Charlie Jay, a decision that apparently ignites her passion for all men.
 monogamous relationship, a pledge she keeps for the first few years of their union, which produces a son and a seemingly idyllic life on a farm.

Problems ensue when lusty contractor Roger Bannon, the son of one of her mothers former servants, arrives to repair their barn and seduces Grace. When she eventually tries to end the affair, he becomes enraged, gets drunk, and accidentally crashes his truck, killing himself. Reports of his death include details about his tryst with Grace, prompting her husband to wonder if she also is involved with newspaper editor Jack Hollister, a suspicion shared by Jacks wife Amy. Brandishing a gun, she publicly confronts Grace at a charity event and attempts suicide but is rescued by Sidney who, realizing that Grace never will change, decides the time has come to begin a new life with his son and leaves his wife.

==Cast==
*Suzanne Pleshette as Grace Caldwell 
*Bradford Dillman as Sidney Tate 
*Ben Gazzara as Roger Bannon  Peter Graves as Jack Hollister 
*Bethel Leslie as Amy Hollister 
*Carmen Mathews as Emily Caldwell 
*Linden Chiles as Brock Caldwell 
*Mark Goddard as Charlie Jay 
*Brett Somers as Jessie Jay

==Critical reception==
Variety (magazine)|Variety said, "In this banal transfer from tome to film, the characters in John OHaras A Rage to Live have retained their two-dimensional unreality . . . Nympho heroine goes from man to man amidst corny dialog and inept direction which combine to smother all actor|thesps."  

TV Guide rates it 1½ out of a possible four stars and adds, "In the transfer from novel to screen, OHaras characters have been transformed from vital, living personalities into stiff, unmotivated soap opera fodder."  

==Awards and nominations== Academy Award Julie Harris for Darling (1965 film)|Darling.

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 