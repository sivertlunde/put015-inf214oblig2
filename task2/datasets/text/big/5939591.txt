Salvation! (1987 film)
{{Infobox Film
| name           = Salvation!
| image          = Salvationposter.jpg
| image_size     = 
| caption        = Theatrical release poster Beth B
| producer       = Beth B
| writer         = Beth B Tom Robinson
| narrator       = 
| starring       = Stephen McHattie Dominique Davalos Exene Cervenka Viggo Mortensen Rockets Redglare
| music          = New Order
| cinematography = Francis Kenny
| editing        = Elizabeth Kling	
| distributor    = Circle Films
| released       =  
| runtime        = 80 min
| country        =   English
| budget         = 
| gross          = 
}}
 Beth B, starring Viggo Mortensen, Exene Cervenka and Stephen McHattie.  The film is a parody of televangelism released right after the real-life Jim Bakker and Jimmy Swaggart scandals. 

The film was released on VHS, but not as yet on DVD.

The film tagline was "SEX. POWER. MONEY. All in the name of God."

==Plot== born again Christian.  After Rhondas non-believer husband, Jerome (Viggo Mortensen), loses his factory job, he and two cousins enlist Rhonda in a plot to entrap Rev. Randall in a sex scandal and blackmail him.

==Soundtrack==
The soundtrack album was released on Factory Benelux/Les Disques du Crépuscule (catalogue TWI-774)  and Factory Australasia (catalogue FACT-182)    in February 1988.

===Soundtrack===
{{Infobox album  
| Name        = Salvation!: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Various artists
| Cover       = 
| Released    = 1988
| Recorded    =  electronic 
| Length      =  Factory Australasia  FACT-182 
| Producer    = Various
| Chronology  = 
| Last album  = 
| This album  = 
| Next album  = 
| Misc        = 
}}

Side A:
#New Order: "Salvation Theme"
#Jumpin Jesus (Arthur Baker and Stuart Kimball, vocals by Stephen McHattie): "You Cant Blackmail Jesus" Cabaret Voltaire: "Twanky Party"
#New Order: "Touched by the Hand of God"
#Dominique* (Dominique Davalos): "Play The Beat" The Hood: "Nightmare"
Side B:
#New Order: "Lets Go"
#The Hood: "Salvation! Have You Said Your Prayers Today" Arthur Baker: "Come On"
#New Order: "Sputnik"
#Cabaret Voltaire: "Jesus Saves"
#New Order: "Skullcrusher"
#Dominique*: "Destroy All Evil"

CD editions include the 12" remix of "Touched By The Hand Of God".  

Singles from the soundtrack were The Hood "Salvation"/Jumpin Jesus "You Cant Blackmail Jesus" (FAC 182)  and New Order "Touched By The Hand Of God" (FAC 193). 

==Trivia==
Exene Cervenka and Viggo Mortensen met on the films set in 1986, and they married on July 8, 1987. On January 28, 1988, Cervenka gave birth to her only child, Henry Blake Mortensen. Mortensen and Cervenka separated in 1992, and were divorced in 1997.  

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 