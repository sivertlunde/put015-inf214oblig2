The Broken Coin
 
{{Infobox film
| name           = The Broken Coin
| image          = Thebrokencoin-1915-newspaper.jpg
| caption        = From an article in a newspaper. Francis Ford
| producer       = 
| writer         = Grace Cunard Emerson Hough
| starring       = Grace Cunard Francis Ford
| cinematography = R. E. Irish Harry McGuire Stanley
| editing        =  Universal Film Manufacturing Co.
| released       =  
| runtime        = 440 minutes (22 episodes)
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 mystery film Francis Ford.  This serial is presumed to be Lost film|lost.      

==Cast==
* Grace Cunard - Kitty Gray Francis Ford - Count Frederick
* Eddie Polo - Roleau
* Harry Schumm - King Michael II
* Ernest Shields - Count Sacchio
* John Ford - Sacchios Accomplice (as Jack Ford)
* W.C. Canfield - Gorgas the Outlaw
* Reese Gardiner - The Apache
* Doc Crane - Pawnbroker
* Harry Mann - Servant
* Victor Goss - Servant (as Vic Goss)
* Lew Short - Prime Minister (as Lewis Short)
* George Utell - Henchman (as G.J. Uttal)
* Bert Wilson - Confidante
* Mina Cunard - Kings Sweetheart
* Carl Laemmle - Editor-in-Chief Jack Holt - Captain Williams
* Mark Fenton - King of Grahaffen John George - uncredited

==Chapter titles==
#The Broken Coin
#The Satan of the Sands
#When the Throne Rocked
#The Face at the Window
#The Underground Foe
#A Startling Discovery
#Between Two Fires
#The Prison in the Palace
#Room 22
#Cornered
#The Clash of Arms
#A Cry in the Dark
#War
#On the Battlefield
# Either
#The Deluge
#Kitty in Danger
#The Castaways
#The Underground City
#The Sacred Fire
#Between Two Fires
#A Timely Rescue
#An American Queen 

==See also==
* List of American films of 1915
* List of film serials
* List of film serials by studio
*List of lost films

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 
 


 