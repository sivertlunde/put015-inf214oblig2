Aval Viswasthayayirunnu
{{Infobox film
| name = Aval Viswasthayayirunnu
| image = avalvish.png
| caption = Promotional Poster designed by Kitho
| director = Jeassy
| writer =  Vincent Kamal Haasan
| producer = J.J.Productions
| music = M. K. Arjunan 
| lyrics = Kanam.E.J
| editor = K.Sankunni
| released = 1978
| runtime = Malayalam
| budget =
}}
 1978 Cinema Indian feature directed by Jeassy, starring M. G. Soman, Vincent (actor)|Vincent, Jayabharathi and Kamal Haasan in lead roles.  

==Cast ==

*M. G. Soman Vincent
*Jayabharathi
*Jose Prakash
*Kamal Haasan
*Bahadoor
*Unnimary
*Sankaradi
*Adoor Bhasi
*Mallika Sukumaran
*Sreelatha Namboothiri
*T. R. Omana
*Manavalan Joseph

==Soundtrack==
{{Infobox album Name     = Aval Viswasthayayirunnu Type     = film Cover    = Viswasthayayirunnu.jpg Released =  Artist   = M. K. Arjunan Genre  Feature film soundtrack Length   = 8:48 Label    = 
}}

The music composed by M. K. Arjunan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics 
|-
| 1 || Thirayum Theeravum... || K. J. Yesudas|| Kanam EJ
|-
| 2 || Chakravalam Chamaram   || K. J. Yesudas|| Kanam EJ
|-
|}

==References==
 

 
 
 
 
 


 