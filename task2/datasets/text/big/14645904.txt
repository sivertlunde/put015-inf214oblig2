Love Walked In (1997 film)
{{Infobox film
| name           = Love Walked In
| image          = Love walked in.jpg
| caption        = Theatrical release poster
| image_size     = 250px
| director       = Juan José Campanella
| producer       = Ricardo Freixa Denis Leary
| screenplay     = Juan José Campanella Lynn Geller  Larry Golin
| based on       =  
| starring       = Denis Leary Terence Stamp Aitana Sánchez-Gijón Michael Badalucco Danny Nucci J.K. Simmons Moira Kelly
| music          = Wendy Blackstone
| cinematography = Daniel Shulman
| editing        = Darren Kloomok
| studio         = Apostle JEMPSA Triumph Films
| distributor    = Tri-Star Pictures Líder Films (1999, Argentina)
| released       = January 18, 1997  (Sundance Film Festival) 
| runtime        = 95 minutes
| country        = Argentina United States
| language       = English
| budget         =
| gross          =
}}
Love Walked In is a 1997 Cinema of Argentina|Argentine-American film noir drama/thriller film co-written and directed by Juan José Campanella and starring Denis Leary, Terence Stamp and Aitana Sánchez-Gijón. It was based on the novel Ni el tiro del final ("Not Even The Final Shot") by Argentine writer José Pablo Feinmann. The film takes its title from the George Gershwins song "Love Walked In".

== Plot synopsis ==
Jack (Denis Leary) is a world-weary pianist and writer in a lounge named the Blue Cat. His wife, Vicki (Aitana Sánchez-Gijón), is a songstress who has a way with the "pseudo-Gershwin" tunes her husband writes. The couple is desperately poor after 10 years of touring crummy clubs.

Meanwhile, Fred Moore (Terence Stamp), the club owner, is captivated by the beauty of Vicki. Moore is married to a wealthy woman, known only as Mrs. Moore (Marj Dusay), whom he admits to having married for her money. Although Fred is a faithful husband, the jealous Mrs. Moore has hired Eddie (Michael Badalucco), a private detective who happens to be an old friend of Jacks, to gather evidence of Freds infidelity. Having come up with nothing, the sleazy detective begs Jack to help by arranging for Vicki to seduce Fred in front of a hidden camera. Together Jack, Vicky and Eddie plan to blackmail Moore.

At the same time Jack is also writing a crime short story set in the 1930s, a noiresque crime thriller, which the viewer sees inter cut in imaginary scenes as Jack narrates. This secondary narration is also a telling of what happens with Jack and Vicki in a different and subtle way.

== Cast ==
* Denis Leary ... Jack Hanaway
* Terence Stamp ... Fred Moore
* Aitana Sánchez-Gijón ... Vicky Rivas
* Michael Badalucco ... Eddie Bianco
* Danny Nucci ... Cousin Matt
* Marj Dusay ... Mrs. Moore
* Neal Huff ... Howard
* J.K. Simmons ... Mr. Shulman
* Moira Kelly ... Vera
* Justin Lazard ... Lenny
* Rocco Sisto ... Ilm Zamsky
* Gene Canfield ... Joey
* Jimmy McQuaid ... Young Howard
* Murphy Guyer ... Howards Boss
* Paul Eagle ... Landlord
* Fiddle Viracola ... Aunt Ethel
* Jeremy Webb ... Hamptons Waiter
* Gregory Scanlon ... Valet
* Gary DeWitt Marshall ... Broken Ivory Bartender
* D.C. Benny ... Comedian
* Patrick Boll ... Porter

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 

 
 