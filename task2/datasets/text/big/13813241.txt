Running Out of Time 2
 
 
{{Infobox film name           = Running Out of Time 2 image          = Running Out of Time 2 DVD cover.jpg image_size     = caption        = director       = Johnnie To Law Wing-Cheong producer       = Johnnie To writer  Milkyway Creative Team narrator       = starring       = Lau Ching Wan Kelly Lin Ekin Cheng music  Raymond Wong cinematography = Cheng Siu-Keung editing        = Law Wing-Cheong Yau Chi Wai One Hundred Years of Film Milkyway Image distributor    = China Star Entertainment Group released       =   runtime        =  country        = Hong Kong language  English
|budget         =
}}
 crime heist caper film Running Out of Time, with Lau Ching-Wan returning as Inspector Ho Sheung-Sang, who this time has to go after an elusive thief played by Ekin Cheng.

==Synopsis==
The film brings back Inspector Ho Sheung-Sang (Lau Ching-Wan), this time to contend with another clever thief (Ekin Cheng, whose character is nameless). This time around, the Thief is determined to extort money from a high-strung businesswoman (Kelly Lin) and play a few rounds of clever games with Ho in the process.

==Cast==
* Lau Ching-Wan - Inspector Ho Sheung Sang
* Kelly Lin - Teresa 
* Ekin Cheng - The Thief
* Hui Shiu Hung - Superintendent/Assistant Commander Wong Kai-fat 
* Lam Suet - Ken
* Ruby Wong - Madam

==Awards==
Golden Horse Film Festival
* Won: Best Action Choreography (Bruce Mang)
* Won: Best Film Editing (Law Wing-Cheong and Yau Chi Wai)
* Nominated: Best Visual Effects (Stephen Ma)
* Nominated: Best Sound Effects
* Nominated: Best Cinematography (Cheng Siu-Keung)

Hong Kong Film Critics Society Awards
* Won: Film of Merit Award

== External links ==
*  

 
 

 
 
 
 
 
 

 