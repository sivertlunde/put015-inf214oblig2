Are You with It?
 
{{Infobox film
| name           = Are You With It?
| image size     = 
| image	=	Are You With It? FilmPoster.jpeg
| alt            = 
| caption        = 
| director       = Jack Hively Robert Arthur
| writer         = George Malcolm-Smith George Balzer Oscar Brodney
| narrator       = 
| starring       = Donald OConnor Sidney Miller, Inez James
| cinematography = Maury Gertsman
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
Are You With It? is a 1948 American film about a young mathematician who quits his job to join a traveling carnival. The film is based on the novel Slightly Perfect by George Malcolm-Smith and uses a screenplay by Oscar Brodney. The film stars Donald OConnor as Barker, Lew Parker as Goldie McGoldrick, Olga San Juan as Vivian, Walter Catlett as Jason (Pop) Carter, Patricia Dane as Sally, and Julie Gibson as Ann. Louis Da Pron, longtime choreographer at Universal, appears and dances as the bartender.

== External links ==
*  
*  


 
 
 
 
 
 
 


 