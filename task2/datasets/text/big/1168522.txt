Mimic (film)
{{Infobox film
| name           = Mimic
| image          = Mimic.jpg
| image_size     =
| caption        = Film poster
| director       = Guillermo del Toro 
| producer       = Ole Bornedal Bob Weinstein Harvey Weinstein B. J. Rack Matthew Robbins Guillermo del Toro
| story          = Matthew Robbins Guillermo del Toro
| based on       =  
| narrator       =
| starring       = Mira Sorvino Jeremy Northam Josh Brolin Charles S. Dutton Giancarlo Giannini F. Murray Abraham
| music          = Marco Beltrami
| cinematography = Dan Laustsen
| editing        = Patrick Lussier
| studio         = Dimension Films
| distributor    = Miramax Films
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = United States dollar|$30 million 
| gross          = $25,480,490
}}
 science fiction horror film co-written and directed by Guillermo del Toro, based on a short story of the same name by Donald A. Wollheim.

Del Toro was unhappy with the film as released, especially because he   (2003), neither of them with del Toro involved.

It includes several examples of del Toros most characteristic hallmarks. "I have a sort of a fetish for insects, clockwork, monsters, dark places, and unborn things," said del Toro,  and this is evident in Mimic, where at times all are combined in long, brooding shots of dark, cluttered, muddy chaotic spaces. According to Alfonso Cuarón, del Toros friend and colleague, "with Guillermo the shots are almost mathematical — everything is planned.” 

==Plot== Entomologist Susan praying mantis) roaches by speeding up their metabolism. The Judas Breed works spectacularly and the crisis is abated. The released population was all-female and designed with a lifespan of only a few months, so that it would only last one generation.
 subway shoe-shiner CDC officer named Josh (Josh Brolin) investigates the church, which is connected to the subway, and comes across some excrement containing wooden buttons.

Two kids later sell a "weird bug" from the subway to Susan. Only a baby, the specimen is already the size of a mans hand. After performing some tests, it turns out that it is a member of the Judas Breed. A creature then breaks into Susans office to steal back the specimen. Susan and Peter, the kids in tow, check out the maintenance room where the specimen was found. However, a subway cop called Leonard (Charles S. Dutton) turns them back until they can get permits (Susan meets Chuy in this scene). Looking for more valuable specimens, the kids go down the tracks and find an egg sack the size of a mans chest (on the way, a homeless man mentions "Long John" and "Overcoat Slim", referring to the tall creatures which they think are a killer on the loose). However, a creature notices the children and kills them. Meanwhile, Chuy goes looking for "Funny Shoes" in the church, and gets captured by creatures. Susan, Peter, and their staff go examine another, larger, mutant Judas found at a water treatment plant. Susans boss Dr. Gates (F. Murray Abraham) surmises that it is a highly evolved soldier, part of a colony.

At this point, the main characters all enter the subway, each for a different reason. Leonard, Peter, and Josh enter the abandoned tunnels through the maintenance room to investigate (later, they get separated and Josh is killed by a creature while trying to get to the surface). When Chuy turns up missing in the morning, Manny goes underground after him. While on the subway, Susan figures out from its pictures that the carapace of the treatment plant specimen fold up to look like a human face. Just then, the station goes partly dark, and Susan gets caught and taken away by one of the creatures, while nobody notices.

Before long, the four meet up and hide inside an abandoned train car. They capture and kill one of the creatures, but not before it slashes Leonards leg, making him bleed profusely. Susan explains that the Judas accelerated metabolism allowed it to reproduce and mutate very fast, despite not being able to biologically reproduce at all, and that they have evolved to mimic their human predator. The smell of Leonards blood attracts more mimics, so Susan retrieves the dead ones smell gland and uses it on the windows and Leonards wound to cover the smell. The group formulates a plan to get the car moving: Peter is sent to get the power back up, and Manny is sent to switch the tracks. Both cover their scent with the gland to blend in with the creatures. Manny finds Chuy imprisoned in a cage, and is killed by a male Judas, preventing the car from moving. Meanwhile, in the car, Susan projects that the mimics could spread out through the tunnels and take over the world. She also theorizes that the Judas somehow breed a single fertile male for every colony. Since Manny hasnt returned, Susan decides to go after him. Leonard then starts bleeding uncontrollably. Meanwhile, Peter finds a dumbwaiter right next to the car which leads to the regular tracks above. Susan frees Chuy and comes across Peter, who leads the two to the dumbwaiter. Leonard sacrifices himself by leaving the car to distract the mimics from the other three.

As Chuy and Susan get into the dumbwaiter, the male Judas drops in. Peter stays behind, hoping to stop the mimics for good. He gets chased into a room which turns out to be the colony nest, where he discovers some gas pipes and an axe, and decides to blow up the nest. He smashes open the pipes with the axe, flooding the nest with gas. After his lighter fails, he strikes the ax against a metal grate, which sparks, ignites the gas and destroys the nest. He escapes into a flooded tunnel under the grate just after sparking the gas.

Meanwhile, the male Judas climbs up the dumbwaiter and goes after Chuy. Susan lures it with her blood, and it is killed by an oncoming train. The two make it to the surface, and it turns out that Peter is also alive as they reunite.

==Cast==
*Mira Sorvino as Dr. Susan Tyler
*Jeremy Northam as Dr. Peter Mann
*Josh Brolin as Josh
*Charles S. Dutton as Officer Leonard Norton
*Giancarlo Giannini as Manny
*F. Murray Abraham as Dr. Gates
*Norman Reedus as Jeremy
*Julian Richings as Workman Doug Jones as Long John #2
*Alexander Goodwin as Chuy
*Alix Koromzay as Remy Panos

==Reception==
Mimic received mixed to positive reviews from critics. It currently holds 61% fresh rating on Rotten Tomatoes. 

==Box office==
According to Box Office Mojo, its domestic gross is $25,480,490; it did not beat its budget of $30 million. 

==Related works==
Mimic was planned as one of three 30 minute short films intended to be shown together.  It was expanded into a full-length movie, as was Impostor (film)|Impostor.  The short film Alien Love Triangle remains a 30 minute short film, and has never been released.  In 2010, Del Toro revealed that he had been working on a directors cut of Mimic and that he is "happy" with it.  The directors cut runs for 111 minutes, 6 minutes longer than the theatrical release. It had a store-specific release on September 6, 2011 with a wider release on September 27, 2011.

==Sequels==
*Mimic 2
* 

==See also==
*Aggressive mimicry

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 