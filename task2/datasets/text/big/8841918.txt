Bulat-Batır
{{Infobox film name        = Bulat-Batır image       = director    = Yuri Tarich  Vladimir Korsh-Sablin writer      = Natan Zarhi  Abdraxman Şakirov starring    = Galina Kravchenko distributor = Sovkino Tatkino released    = 1927 country     = Soviet Union language  Russian intertitles Tatar intertitles
}}
 1927 silent film, believed to be the first Tatar film and probably the only Tatar full-length feature silent film. The film was shot mostly in Kazan, and the Kazan Kremlin was one of its stills. The film is devoted to the Pugachev rebellion and its alternative name is Pugachyovshchina ( ).

The story was written by Abdraxman Şakirov, a young Communist from Agryz and the script was written by Natan Zarhi, a Soviet scenario writer.

==Plot== Tatar village celebrates the Sabantuy festival. Orthodox monks accompanied by soldiers appear to forcibly baptize the population of the village. Locals resist and soldiers commit a punitive action. The wife of peasant Bulat is killed by soldiers, his son Asfan is carried off. Bulat stays alone with another son, Asma. 15 years after Bulat and Asma joined the Pugachev rebellion and Bulat became famous as a defender of paupers. But his son Asfan, who was reared among nobles, received a commission and led a punitive force directed to suppress a rebellion in his motherland.

==Critical reception==
 
It is known that after the premiere in Germany one White émigré Antonov-Ivanov managed to climb to the film projector and to destroy that copy of the film.

==External links==
*    
*    

 
 
 
 
 


 
 