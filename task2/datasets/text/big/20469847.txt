Jo Jo in the Stars
{{Infobox Film 
 | name             = Jo Jo in the Stars 
 | image            = Jo Jo in the Stars poster low resolution.jpg
 | image_size       = 
 | caption          = 
 | director         = Marc Craste 
 | producer         = Sue Goffe 
 | writer           = Marc Craste 
 | narrator         = 
 | starring         = Oliver Miceli  Andrew Stirk 
 | music            = Die Knodel  Samuel Barber  Dumb Type 
 | cinematography   = 
 | editing          = William Eagar 
 | distributor      = 
 | released         =  
 | runtime          = 12 minutes
 | country          = United Kingdom
 | language         = English
 | budget           = 
 | gross            = 
}}
Jo Jo in the Stars is a twelve-minute film that won the 2004  .

==Plot==
Madame Pica is the cold-hearted mistress of a circus of "monsters and misfits", attended each night by thousands of curious spectators. Hero is among them every night, but he is there only to see Jo Jo, the winged trapeze artist.  One night after the show, he steals the keys of the cell where Jo Jo is imprisoned, freeing her.  The two escape and start to dance a romantic waltz in the stars.  But soon the two lovers are discovered and as a last desperate act, hand-in-hand, they jump from the highest window in the tower.  Jo Jo attempts to fly the two of them to safety, but the hero loses his grip and falls to the ground. Jo Jo is blown onto a window sill and re-captured by Madame Pica. A single feather from JoJos wing floats to the ground, landing on Heros apparently lifeless body.

Ten years pass, and Madame Pica is in search of some new attraction to draw in the crowds. She discovers that Hero is still alive but horribly disfigured, and takes him on as the new star of the show. He is taken to the cell next to JoJos, and the reunited lovers embrace through the bars.

==Production==
Marc Crastes original aim was to make a short film based on "The Carny", a song by Nick Cave and the Bad Seeds. He began work on a storyboard, intending it to be a "straight visual interpretation of the text", featuring live action sequences combined with 3D animation. He received encouragement from Nick Cave, but was ultimately unable to secure funding for the project.   

In the following years, Craste made three one-minute films for Studio AKA, starring Madame Pica in a circus setting. Studio AKA then asked Craste to make "a longer film using the same characters, but without any murders". JoJo in the Stars was the result. Carter, Jonathan (2004-06-04).  . features, film interview. BBC. Retrieved on 14 December 2009. 

Crastes influences include David Lynchs Eraserhead (1977) and Wim Wenderss Wings of Desire (1987). 

===Personnel===
* Marc Craste - Director, Writer

* Sue Goffe - Producer, Executive producer

* Oliver Miceli – voice
* Andrew Stirk – voice

* Mike Cachuela - Storyboard Artist

* Dominic Griffiths - Animator
* Boris Kossmehl - Animator
* Fabienne Rivory - Animator

* William Eagar - Editor

* Melissa Lake – foley artist
* Ben Meechan – sound editor
* Barnaby Smith – foley editor
* Michele Woods – sound mixer
* Hilary Wyatt – supervising sound editor

* Ren Pesci - Production Assistant 
* Lindsay Fraine - Production Assistant

==Awards==
Jo Jo in the Stars has won the following awards: 

{| class="wikitable" border="1"
|-
!Year
!Award
!Category&nbsp;–&nbsp;Recipient(s)
|- 2004
|Clermont-Ferrand Short Film Festival   International Short Film Festival - Clermont-Ferrand. Retrieved on 30 November 2008.  Prix du Meilleur Film dAnimation
|- Bradford Animation Festival Grand Prix
|- British Academy British Academy of Film and Television Arts (BAFTA) Film Awards Best Animation Short 
|- Aspen Shortsfest  Special Jury Recognition 
|- Copenhagen 3D Awards Digital Hero Award for the Best Short Film  
|- Seoul International Seoul International Cartoon and Animation Festival (SICAF) Animasia Grand Prize for Short Film  
|- Bristol International Short Film Festival (BriefEncounters) Best of British
|- 2005
|Paris Les Lutins du Court-Métrage Press Lutin
|- Cartoon dor Cartoon dor
|- 2007
|IFCT Awards Most Innovative Animation
|}
 Short Film Anima Mundi, Annecy International Holland Animation Soho Shorts Film Festival, SAND,  Independent Film Festival of Boston, Los Angeles Film Festival,  Tallgrass Film Festival, and The World According to Shorts  in 2004. 
 Norwegian Film Institute, Future Shorts (South Africa), Fantoche Film Festival (Switzerland), Draken Film Festival (Sweden), Stockholm International Film Festival, Golden Horse Film Festival, Turkey British Council (tour of Turkey), Animated Encounters (United Kingdom|UK), Animex (University of Teesside), Cambridge Film Festival, CineMagic (film festival)|Cinemagic, Commonwealth Film Fest (Manchester), Hertfordshire International Film Festival (HIFF), London Institutes Arts Festival, Northern Film Network (UK), Norwich Film Festival, Antelope Valley Independent Film Festival, Brooklyn In Film Fest, Milwaukee International Film Festival, Portland International Film Festival,  REDCAT, Red Stick International Animation Festival (Louisiana) in 2005. 
 Kyiv IFF Brussels Short Film Festival, Rooftop Films, Fresh Film Festival in 2008. 

<!--
==Releases==
-->

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 