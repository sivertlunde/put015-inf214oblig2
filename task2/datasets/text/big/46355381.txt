Show Flat
{{Infobox film
| name = Show Flat
| image =
| image_size =
| caption =
| director = Bernard Mainwaring    
| producer = Anthony Havelock-Allan 
| writer =  Martha Robinson  (play)   Cecil Maiden  (play)   George Barraud   Sherard Powell 
| narrator =
| starring = Eileen Munro   Anthony Hankey   Clifford Heatherley   Polly Ward
| music = 
| cinematography = 
| editing = 
| studio = British & Dominions Film Corporation 
| distributor = Paramount British Pictures 
| released = October 1936 
| runtime = 76 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} flat in order to impress somebody by holding a dinner there.

==Cast==
*   Eileen Munroas Aunt Louisa 
* Anthony Hankey as Paul Collett 
* Clifford Heatherley as Ginnsberg 
* Max Faber as Ronnie Chubb 
* Polly Ward as Mary Blake 
* Vernon Harris as Tom Vernon  
* Miki Decima as Miss Jube  Billy Bray as Fox 
* Victor Rietti

==References==
 

==Bibliography==
*Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 