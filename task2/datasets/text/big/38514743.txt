Hide Away (film)
{{Infobox film
| name           = Hide Away
| image          = 
| caption        = 
| director       = Chris Eyre
| producer       =  
| writer         = Peter Vanderwall
| starring       =   Edward Rogers
| cinematography = Elliot Davis
| editing        = Jonathan Lucas Devin Maurer
| studio         = MMC Joule Films A Year in Mooring
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $15,208  
}}

Hide Away (aka A Year in Mooring) is a 2011 drama film directed by Chris Eyre. 
{{cite news|url= http://movies.nytimes.com/2012/06/01/movies/hide-away-directed-by-chris-eyre.html?_r=0|accessdate= 2013-02-14
|title= Seeking Balm for Soul in Boat Going Nowhere|date= May 31, 2012|last= Holden|first= Stephen|journal= New York Times}}  The film stars actors Josh Lucas, Ayelet Zurer, and James Cromwell.  It was released on 30 October in 2011.

It screened at the 2011 SxSW under the title A Year in Mooring. 
{{cite journal|url= http://www.variety.com/review/VE1117944931/|accessdate= 2013-02-14|title= A Year in Mooring|date= March 30, 2011
|last= Leydon|first= Joe|journal= Variety (magazine)|Variety}}  

==Cast==
*Josh Lucas as Young Mariner
*Ayelet Zurer as The Waitress
*James Cromwell as The Ancient Mariner
*Jon Tenney as The Divorced Man
*Taylor Nichols as The Boss
*Casey LaBow as Lauren
*Anne Faba as Helen

==References==
 

==External links==
 

 
 

 