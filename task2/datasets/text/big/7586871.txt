The Cockettes (film)
{{Infobox Film
| name           =  
| image          = DVD cover of The Cockettes (film).jpg
| caption        = 
| director       =   and  
| producer       =   
| writer         = 
| starring       = 
| music          = Richard "Scrumbly" Koldewyn
| cinematography = Marsha Kahm
| editing        = Bill Weber
| distributor    = Strand Releasing
| released       = January 16, 2002 ( )
| runtime        = 100 min.
| country        = USA
| language       = English
| budget         = $450,000 (estimated)
| gross          = 
}}
  is a 2002 American    performance group The Cockettes. The film debuted at the 2002 Sundance Film Festival, where it was nominated for the Grand Jury Prize. It went on to a limited theatrical release and to play the film festival circuit.   received the LA Film Critics Award for Best Documentary of 2002.

==Cast==
* Sylvia Miles John Waters
* Dusty Dawn
* Larry Brinkin
* John Flowers
* Goldie Glitters
* Ann Harris
* Fayette Hauser
* Jilala
* Michael Kalmen

;Archive footage Divine
* Jackie Curtis Hibiscus
* Angela Lansbury
* Anthony Perkins
* Ronald Reagan
* Gore Vidal Sylvester
* The Grateful Dead
* Taylor Mead Richard and Tricia Nixon

==DVD release==
The Cockettes was released on Region 1 DVD on January 21, 2003.

==See also==
*Dzi Croquettes, a documentary film about a Brazilian ensemble inspired by the Cockettes.   

==References==
 

==External links==
*  
*  

 
 
 
 
 


 
 