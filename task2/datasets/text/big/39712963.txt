Doublecross (1956 film)
{{Infobox film
| name           = Doublecross
| image          = 
| image size     =
| caption        = 
| director       = Anthony Squire
| producer       =
| writer         =
| starring       = Donald Houston
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =1956
| runtime        = 
| country        = UK
| language       = English
| budget         =
| preceded by    =
| followed by    =
}} British crime film directed by Anthony Squire and starring Donald Houston, Fay Compton and William Hartnell.  It was also known as Queer Fish.   

==Synopsis==

A Cornish fisherman becomes involved with Iron Curtain spies.

==Cast==
* Donald Houston - Albert Pascoe
* Fay Compton - Alice Pascoe
* Anton Diffring - Dmitri Krassin
* Allan Cuthbertson - Clifford
* Delphi Lawrence - Anna Krassin
* William Hartnell - Herbert Whiteway
* Kenneth Cope - Jeffrey of the Coast Guard

==References==
 

==External links==
* 

 
 
 
 
 
 


 