Kalikaalam
 
{{Infobox film
| name           = Kalikaalam
| image          = Kalikalam.jpg
| caption        = 
| director       = Reji Nair
| writer         = Reji Nair
| narrator       = 
| producer       = Pilakandi Mohammed Ali Sharada Lalu Ashokan Suresh Suresh Krishna Shari Lakshmi Sharma Mamukkoya Kozhikode Narayanan Nair Sreehari
| music          = Ousepachan O. N. V. Kurup (lyrics)
| editing        = Ranjan Abraham
| cinematography = Madhu Ambat
| studio         = Pilakandi Films International
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Kalikaalam (  written and directed by Reji Nair. Focussing mainly on relationships, it tells the story of a mother and her three children. The film tries to explore the complexities of a small family. Scriptwriter Reji Nair makes his directorial debut with this film. Kalikalam was filmed by award winning cinematographer Madhu Ambat. {{cite news
|url=http://www.thehindu.com/arts/cinema/article3500286.ece?homepage=true
|title=Wedded to cinema
|newspaper=The Hindu
|publisher=thehindu.com
|first=Prema
|last=Manmadhan
|date=June 7, 2012
|accessdate=June 17, 2012
|location=Kochi
}}  The films music is by O. N. V. Kurup-Ousepachan team, who reunites once again after Akashadoothu (1992). The film was shot in Thalassery and nearby places. {{cite news
|url=http://timesofindia.indiatimes.com/entertainment/regional/others/news-interviews/Kalikalam-progressing/articleshow/10184286.cms
|title=Kalikalam progressing
|newspaper=The Times of India
|publisher=indiatimes.com
|first=Ammu
|last=Zachariah
|date=September 30, 2011
|accessdate=June 17, 2012
|location=Kochi
}} 

==Cast== Sharada as Teacher
* Lalu Alex as Satheesh Ashokan as Raghu Suresh Krishna Shari as Meenakshi
* Leona Lishoy as Ananya {{cite news
|url=http://articles.timesofindia.indiatimes.com/2012-06-06/news-and-interviews/32057099_1_mollywood-malayalam-film-industry-love-interest
|title=Leona Lishoy finds Mollywood challenging
|newspaper=The Times of India
|publisher=indiatimes.com
|first=Shiba
|last=Kurian
|date=June 6, 2012
|accessdate=June 17, 2012
|location=Kochi
}} 
* Lakshmi Sharma
* Mamukkoya
* Kozhikode Narayanan Nair as Master
* Sreehari
* Munshi Venu
* Anoop Chandran
* Navami {{cite news
|url=http://articles.timesofindia.indiatimes.com/2012-06-05/news-and-interviews/32032952_1_navami-big-screen-small-screen
|title=No fears for Navami!
|newspaper=The Times of India
|publisher=indiatimes.com
|first=Shiba
|last=Kurian
|date=June 4, 2012
|accessdate=June 17, 2012
|location=Kochi
}} 
* Sattya Pilla {{cite news
|url=http://www.deccanchronicle.com/channels/showbiz/mollywood/mollywood%E2%80%99town%E2%80%99s-new-star-394
|title=Mollywood’town’s new star
|newspaper=Deccan Chronicle
|publisher=deccanchronicle.com
|first=Shreejaya
|last=Nair
|date=May 31, 2012
|accessdate=June 17, 2012
|location=Kochi
}} 

==Soundtrack==

 

==References==
 

 
 
 
 
 

 
 