Balupu
 
 
{{Infobox film
| name = Balupu
| image = Balupu poster.jpg
| caption = Movie Poster
| director = Gopichand Malineni
| producer = Prasad Vara Potluri
| writer = Kona Venkat K. S. Ravindra Anjali Adivi Sesh
| music = S. Thaman
| cinematography = Jayanan Vincent
| editing = Gautham Raju
| studio = PVP Cinema
| released =  
| runtime = 159 mins
| country = India
| language = Telugu
| budget =  
| gross =   
}}
 action comedy Anjali in lead roles.  The film soundtrack is being composed by S. Thaman. The film released on 28 June 2013 in Telugu and Tamil. Movie was entirely shot in Bangalore and Vishakapatnam.   The film opened to positive reception from critics as well as audience and was declared a super hit at both the Indian and overseas box office.   It was dubbed into Hindi as Jani Dushmann.

== Plot ==
Ravi (Ravi Teja) works as a collection agent of ICICI in Bangalore and leads a happy life along with his friends and his father Mohan Rao (Prakash Raj) who wishes to see Ravi married to a girl soon. Once Ravi rushes to a Hospital where his friend (Satyam Rajesh) is admitted as he tried to commit suicide by drinking nail polish solution and asks the reason for it. Then he describes about the traits of Shruti (Shruti Haasan) and her uncle Crazy Mohan (Brahmanandam). The duo have a habit of cheating gullible young men including his friend and Ravi decides to teach them a lesson.

Ravi enters their life as a gullible youngster and starts torturing them. Crazy Mohans plans and attempts always go in vain before him. As a part of their cunning plans, Crazy Mohan advises Shruti to ask Ravi to marry her. As fate would have it, Ravi also comes with a marriage proposal to Shruti. Then the pair approach Mohan Rao and tell him that Ravi is in love with Shruti and that he proposed to her. They assume that this would enrage him but the result is negative as Mohan Rao accepts the proposal heartily. However, Shruti was already engaged to Rohit (Adivi Sesh) by her father (Nassar) sometime in the past. Ravi one days saves Shruti from some goons and after doing so advises her not to play with peoples feelings in future. Shruti finds herself falling in love with Ravi and informs Ravis father Mohan Rao.

Mohan Rao leaves the final decision to Shrutis father who in turn guarantees that they would not face any problem in the future from Ravi. On the other hand Rohits mother (Sana) warns Shruti that she would torture her after her marriage. Listening to both, Shrutis father fixes Shrutis marriage with Ravi. Rohits mother gets humiliated by this and calls her brother Poorna (Ashutosh Rana). Poorna who arrives at the wedding venue with his hench men recognise that Ravi-Mohan Rao are his biggest enemies Shankar-Nanaji. While Ravi is fighting with the goons, Poorna stabs Mohan Rao and flees away to Vizag with Shruti. After admitting Mohan Rao into the hospital, Ravi starts narrating his past.

In the past in Vizag when Poorna tries to destroy the empire of Nanaji, a dreaded don those days, he takes the help of Shankar and makes him his partner in crime. Shankar, with his aggressiveness and cleverness, starts destroying the empire of Nanaji. Meanwhile, without knowing the fact that she was Nanajis daughter, he falls in love with Dr. Anjali (Anjali (actress born 1986)|Anjali) who reciprocates his love. Knowing this, Poornas elder son Babji (Ajay (actor)|Ajay) tries to injure Anjali by throwing Acid on her, only to be killed by Nanaji. Then she comes to know that Shankar is a criminal and her fathers enemy. She pleads both Shankar and Nanaji to leave rowdyism, in which Shankar turns successful. After his younger son Kaasi (Shafi) advice, Poorna tells to Nanaji that Shankar and Anjali are eloping. While Shankar is involved in a feud with Nanaji, Kaasi shoots Anjali fatally And Thus Shankar Kills Kaasi That Moment Only . Later they fulfill her last wish by leaving rowdyism and living a normal life And Later They Leave For Bangalore

Meanwhile Shrutis father rushes to the hospital and tells to Ravi that Shruti is being married to Rohit forcibly. Ravi reaches the spot, acts as if though Kaasis soul is in his body and it wants to kill Shankar and Nanaji. With the help of Shruti, Anjalis father Nanaji, Dr. Savitri (Ali (actor)|Ali) and his friends, Ravi manages to kidnap Rohit. Poorna along with Shruti and Nanaji along with Rohit reach the agreed spot. Shankar reveals his real identity, defeats Poorna and marries Shruti in the end.

== Cast ==
 
* Ravi Teja as Ravi Shankar
* Shruti Haasan as Shruti Anjali as Dr. Anjali
* Prakash Raj as Mohan Rao/Nana/Nanaji
* Nassar as Shrutis father
* Ashutosh Rana    as Poorna
* Adivi Sesh as Rohit
* Sana as Rohits mother
* Brahmanandam as Crazy Mohan Ali as Dr. Savitri
* Brahmaji as Suri Adithya Menon as Nanajis Brother
* Raghu Babu as Nanajis henchman
* Rao Ramesh as A. R. Naidu
* Jaya Prakash Reddy as CI Basava Ajay as Babji
* Lakshmi Rai (Special appearance) as Lakshmi Rai
* Shafi as Kaasi
 

== Crew ==
 
* Director: Gopichand Malineni
* Screenplay: Gopichand Malineni
* Producer: Prasad Vara Potluri
* Music: S. Thaman
* Cinematography: Jayanan Vincent
* Editing: Gautham Raju
* Story: Kona Venkat, K. S. Ravindra
* Dialogues: Kona Venkat
* Lyrics: Sirivennela Sitaramasastri, Bhaskarabhatla
* Fights: Stun Siva, Allan Amin, Ram Lakshman
* Choreography: Raju Sundaram, Brinda (choreographer)|Brinda,shekar
* Line Producer: Rengarajan Jaiprakash
* Art: A. S. Prakash
 

== Production ==

=== Development ===
Gopichand Malineni and Ravi Teja together announced that their collaboration is set for a new movie whose name was revealed later. In August 2012, it was announced that the filming would begin from October 2012.   The films muhurtam was held on 26 October 2012 in a quiet ceremony held at producer Potluris office.   
Few of the movies songs were canned in Lisbon of Portugal thus making the first Telugu film being filmed in Portugal.     In an interview to the Portugal media, Ravi Teja, Shruti Haasan and Gopichand Malineni thanked the Portugal tourism board and the local crew for their support.    In May, Gopichand Malineni responded about the delay in the movies shooting, stating "Balupu has been delayed due to fight masters and fighters union problems. Now Ram-Laxman masters have got Hyderabad union card and we are on the way."  For the first time in his career, Ravi Teja sung a song thus making this film his Debut as a Singer, adding yet another feather in his cap.  As on 4 June, The post-production works were reported to be running in full-swing at a neck break speed. 

=== Casting ===
  was selected as lead heroine marking her first collaboration with Ravi Teja.]] Anjali was Ali would essay the role of Dr. Savitri, a Male/Female Doctor which was said to be one of the highlights of the film. 

=== Filming ===
The movie shooting started on 13 November 2012 and had a silent shoot and in December, the film has competed its first schedule in which the scenes between Ravi Teja, Bramhanandam and Sruthi Hassan were shot.  In End of December, it was reported that shooting is currently progressing near Simhachalam and it was reported that the film is reportedly set in the backdrop of Vizag and some important scenes are being shot right now on Ravi Teja and Anjali.    Later, it was told that the film has completed about 50% of the shoot and the work is going on a good pace.  The next schedule started in Hyderabad in January 2013 where scenes featuring Ravi Teja and Shruti Haasan were shot, which was confirmed by the actress in her Twitter.    The film’s shooting was put on hold temporarily due to Gopichand Malineni’s wedding and after a brief gap, the film’s next schedule began in End of February at Hyderabad where some important scenes were canned between Ravi Teja and Shruti Haasan.  In March, the song "Patikella Chinnadi" was shot on Ravi Teja and Shruti Haasan in Ramoji Film City and sources reported that Shruti Haasan will be seen in a Glamorous attire in that song.   After the songs completion, the unit had its shoot at Appa Junction in Hyderabad for some crucial scenes at a very fast pace.  After the completion of the major schedules, The movie unit proceeded for the shooting of the remaining songs.  Later it was known that few songs would be canned in Lisbon, the capital city of Portugal.  After a shoot of 10 Days in Lisbon, the unit moved to Algarve region in Southern part of Portugal to shoot couple of songs on Ravi Teja and Shruti Haasan.  After completing 2 songs under the choreography of Raju Sundaram, The unit returned from Portugal nearly on 20 April 2013.  The final schedule began at Hyderabad in the month of May.  On 18 May, Some scenes were canned at Lingampally Railway Station in Hyderabad.  After completion of the schedule nearly on 18 May, the unit proceeded to Bangkok for canning some crucial scenes.  After completing a long schedule in Bangkok, the unit resumed its shooting in Nanakramguda where some crucial scenes on Ravi Teja and Shruti Haasan were shot.  The film unit then canned the song "Lucky Lucky Rai" on Ravi Teja and Lakshmi Rai at Bangkok. It was informed that Sekhar master is choreographing that song and shooting will go on till 15 June, with which the shooting of the film will be complete.  Finally Shooting Came To An End On 15 June.

== Soundtrack ==
{{Infobox album
| Name = Balupu
| Longtype = to Balupu
| Type = Soundtrack
| Artist = S. Thaman
| Cover =
| Released = 28 June 2013
| Recorded = 2012-2013 Feature film soundtrack
| Length = 19:53 Telugu
| Label = Junglee Music
| Producer = S. Thaman
| Reviews =
| Last album = Tadakha (2013)
| This album = Balupu (2013)
| Next album = Pattathu Yaanai (2013)
}}
 Bodyguard and Hyderabad on 1 June 2013. 

{{Tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 19:53
| writing_credits =
| lyrics_credits  = yes
| music_credits   =
| title1          = Kajal Chellivaa
| note1           =
| lyrics1         = Bhaskarabhatla
| extra1          = Ravi Teja, S. Thaman
| length1         = 3:54
| title2          = Evaindho
| note2           = 
| lyrics2         = Sirivennela Sitaramasastri
| extra2          = S. P. Balasubrahmanyam, Geetha Madhuri
| length2         = 4:03
| title3          = Lucky Lucky Rai
| note3           =
| lyrics3         = Bhaskarabhatla
| extra3          = Naveen Madhav, M. M. Manasi
| length3         = 3:52
| title4          = Padipoyanila
| note4           =
| lyrics4         = Anantha Sreeram Megha
| length4         = 3:07
| title5          = Pathikella Chinnadi
| note5           =
| lyrics5         = Bhaskarabhatla
| extra5          = Mika Singh, Ranina Reddy
| length5         = 4:00
| title6          = Hello Boys and Girls
| note6           =
| lyrics6         = Bhaskarabhatla
| extra6          = Ravi Teja
| length6         = 0:57
}}

=== Reception ===
The audio got positive response. 123telugu.com gave a review stating "Balupu is one of the most commercially promising audio albums that Ravi Teja has had in recent times. The songs are peppy and are easy to like."  APHerald.com gave a review stating "Over all Balupu audio sounded as mix of new and old track from Thaman’s pervious movies."  Telugucinema.com gave a review stating "All in all, Balupu is different in Thamans repertoire and has songs that are easy to like. Thaman has come up with songs that would help the film. Has mass numbers as well as a melody number. Right mix of songs. Best album in Ravi Tejas career in the recent times."  Cineoutlook.com gave a review stating "Overall Balupu Audio is a combination of Class and Mass Masala songs. It’s a S.S. Thaman Brand Music." 

== Release ==
Initially the movie was slated for a worldwide released in 302 screens on 21 June 2013.  But due to delay in Post-Production of the film, the release date was shifted to 28 June 2013.  The film would have a premiere on 27 June 2013 in all the overseas countries.  Though it was rumoured that the film would be delayed further, the unit confirmed its release on 28 June 2013.  The film was submitted to the Central Board of Film Certification on 24 June 2013.  The film was awarded an A certificate by the Board after the objectionable dialogue was removed.  The film released in 70 screens across USA, making it the biggest release of Ravi Teja there.  With a massive release in AP, the film was also released in and around Chennai in 16 Screens. 

=== Marketing ===
It was reported in the first week of January that the first look posters and stills would be released on 26 January 2013 thus coinciding with Ravi Tejas Birthday  but the logo and teaser of Balupu was launched at a function organized in Marigold Hotel at Greenlands, Hyderabad on 25 January 2013. Ravi Teja launched the teaser while PVPs executive director Rajeev Kamineni launched the logo.    The audio launch was held on 1 June 2013 at HICC Novotel in Hyderabad. It was organised by Shreyas Media, a popular event organizing company. The cost of organizing was   3.5&nbsp;million, which was well received, both in terms of TRPs and Public Response, making it one of the successful events organized by them. For the first time ever, Ravi Teja and Shruti Hassan performed live on stage along with S. Thaman, which elevated the event to Bollywood and Hollywood standards.  The Theatrical trailer, which was released on the same day generated Unusually positive response from the public.  From 18 June 2013 the character sketches of Ali,  Srinivasa Reddy, Satyam Rajesh, Pradeep, Thagubothu Ramesh,  Ashutosh Rana,  Lakshmi Rai,  Adivi Sesh,  Anjali,  Prakash Raj,  Shruti Haasan,  Brahmanandam,  Ravi Teja  were displayed at idlebrain.com, the official web partners of the film in images with a countdown till 28 June 2013 which was told as a unique promotion. Moreover, the posters of the film were decorated to 3 Trains covering all the parts of Andhra Pradesh nearly on 26 June 2013 which was also a unique promotion activity. 

=== Pre-release revenues ===
Noted Producer and Distributor Dil Raju bought the Nizam Area Rights of the film for a Reported Fancy Prize.  The Overseas rights were secured by BlueSky for a fancy price.    Zee Telugu bought the satellite rights of the film.  The Australian Distribution rights of this film, along with Sahasam, were secured by Sai Manali and Lorgan Entertainment.  The movie had a great pre-release business in USA, with 33 centers in USA were closed within 48 to 72 hours of press note and remaining in final stages of closing, with all Non USA centers closed as on 21 June 2013. 

=== Controversy ===
On 20 June 2013 Mr. Dronamraju Ravikumar, representative of the A.P. Brahmana Seva Sangha Samakhya, alleged that the trailer of the film contained derogatory remarks against Brahmins. The representatives of Brahmin organizations demanded the removal of trailers from all TV channels, thus submitting a formal complaint to the Censor Board and the Film Chamber with an intention of necessary action by the Censor Board since the film was not censored by that time.  On the other hand, Gopichand Malineni stated that his intention was not to hurt anyone’s feelings but just to conclude the controversy in a positive manner in this movie and concluded saying that the Brahmin organizations are invited to watch the film before it gets censored.  After showing the film to the irked community, the producer of the film sent a letter to Regional Censor Board Officer stating that he is going to delete the entire scene from the film to avoid controversies. 

=== Reception ===
Jeevi of idlebrain.com gave a review of rating 3.25/5 stating "Ravi Teja whose previous hit was released in January 2011 didnt find success at box office despite having seven films in a span of 2 and half years. Balupu will provide oxygen to his career. Balupu has all ingredients with a dominating portion of entertainment to become a decent hit. You can watch it."  The Times of India gave a review stating "The first half of the film looks pretty entertaining with good comedy, emotions and interval bang. Kona Venkats comic drama style coupled with Gopichand Malinenis well-shot action sequences hogged the limelight in the second half, where the story gets into the flash back. Powerful dialogues, emotions and nice comedy might engage the audience."  Postnoon.com gave a review of rating 3.5/5 stating "Balupu is a good concoction of action and entertainment which hits all the right notes. Ravi Teja has finally managed to hit the ball out of the park, after almost two years."  Mahesh S Koneru of 123telugu.com gave a rating of 3.25/5 stating "With Balupu, we can perhaps say that Ravi Teja is back! His mass look and powerful punch dialogues work well. It ends up being a mass masala entertainer that is definitely worth a watch."  Oneindia Entertainment gave a review stating "Ravi Teja is back with a bang this time. With repeated failures till now, it is refreshing to watch Ravi Teja at his best."  IndiaGlitz gave a review of rating 3.25/5 stating "Balupu is a mix of action plus entertainment. Ravi Teja will win a battle for the first time since Mirapakay."  APHerald gave a review of rating 2.25/5 stating "Balupu is Not a complete Action Entertainer, but watchable once for Ravi Teja-Brahmi comedy."  SuperGoodMovies gave a review of rating 3/5 stating "Raviteja’s Balupu is a movie with entertainment and action sequences. But it is just one time watch." 

=== Home Media ===
The film was aired for the first time on 12 January 2014 as Sankranti special on Zee Telugu Channel which generated Huge TRPs for the channel. 

== Box office ==

=== India ===
The film had a good start at the domestic box office collecting   on its first day.  The film collected a share of   in 3 days, with a collection of   on the second day and   on the third day in Andhra Pradesh.    The film collected   4025,000 in 3 days in Tamil Nadu, which is the highest collection for a Ravi Tejas film in Tamil Nadu till date, hence being declared a Super Hit at both Andhra Pradesh and Tamil Nadu Box offices respectively, breaking Ravi Tejas previous records held by Kick (2009 film)|Kick, Don Seenu and Mirapakay.    The film continued its pace by collecting   at the end of the first week and with staedy collections in the second week even after finding a stiff competition from Singam, the successful dubbed version of Suriyas Singam II.   The film completed a successful 50 Day run in 85 Direct Centers, collecting a gross of nearly   allover the state.   

=== Overseas === Baadshah which collected   at that point of time, however subjected to the number of screens it was released.  The film collected a total of   at the end of its Third week. 

=== Remake === Sayantika and Mithun Chakraborty.

== References ==
 

== External links ==
*  
*   for Release dates and USA Schedule  

 

 
 
 
 
 
 