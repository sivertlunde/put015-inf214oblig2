The Order (2001 film)
 
 
{{Infobox film
| name           = The Order
| image          = Theorder2001dvd.jpg
| image_size     =
| caption        = DVD cover
| director       = Sheldon Lettich
| producer       = Avi Lerner
| writer         = Jean-Claude Van Damme Les Weldon
| starring       = Jean-Claude Van Damme Charlton Heston Sofia Milos Brian Thompson Vernon Dobtcheff
| music          = Pino Donaggio
| cinematography = David Gurfinkel
| editing        = Donn Aron Alain Jakubowicz
| distributor    = TriStar Pictures
| released       =  
| runtime        = 89 min
| country        = Aruba US Syriac Aramaic Aramaic English English Arabic Arabic French French Hebrew Hebrew Ukrainian Ukrainian
| budget         = $12 million
| gross          = 
}}
The Order is a 2001 American action film directed by Sheldon Lettich, and written by Jean-Claude Van Damme, who also starred in the film. The film was released on direct-to-video|direct-to-DVD in the United States on March 12, 2002.

==Plot==
The film opens in 1099 at the end of the First Crusade, depicting Christian Crusaders sacking Jerusalem and slaughtering the local population. One of the Christian knights, Charles Le Vaillant (Jean-Claude Van Damme), becomes demoralized by the horrors of war, and decides to create a new religious order. This new order brings together members from the three main religions of the region: Christians, Jews, and Muslims. As a self-imposed leader and messiah, Charles writes the sacred texts of the Order. He is subsequently accused of heresy by the Christian church, who attack his stronghold, killing Le Vaillant. During the attack, the last chapter from their religious text becomes lost in the desert after he buried it in a secret place.

In modern-day Israel, a devout contingent of Le Vaillants followers continues to practice his peaceful teachings; however, one of the disciples, Cyrus (Brian Thompson) holds a distorted view of the Orders ways. Cyrus ordered a priest killed by using a car bomb, to his delight. Meanwhile, Rudy Cafmeyer (Jean-Claude Van Damme), a thief and smuggler of valuable historical artifacts, breaks into a high-security building and steals a precious Fabergé egg. He triggers an alarm in the process and is forced to fight his way out of the building.

It is revealed that Rudy works for his father, archaeologist and museum curator Oscar "Ozzie" Cafmeyer (Vernon Dobtcheff), and that the Fabergé egg was stolen from the personal collection of a powerful Russian mafia boss. Ozzie discovers the lost manuscripts of the Order, lost ever since the Crusades, and reads their accounts of interfaith harmony. The manuscripts also contain an ancient map of Jerusalem, which depicts the location of a mythical Jewish treasure. Ozzie travels to Israel, where Cyrus has him kidnapped.

Rudy, having heard the abduction take place while talking to Ozzie on the phone, travels to Jerusalem to rescue him. Ozzies associate, Professor Walt Finley (Charlton Heston), gives Rudy the key to a safe-deposit box in East Jerusalem before being gunned down by unknown assailants. Israeli Police Chief Ben Ner (Ben Cross) views Rudys arrival with hostility and takes steps to have Rudy deported, but police lieutenant Dalia Barr (Sofia Milos) risks her career to help Rudy, subsequently revealing that  she was once a disciple of the Order, but she left when she was 18.

Meanwhile, Cyrus assassinates the Orders leader, Pierre Gaudet, and steps up his inflammatory rhetoric, hoping to inflame his followers. Rudy opens the safe-deposit box and finds a map that shows a series of tunnels and a treasure room beneath Jerusalem. Ben Ner, who has partnered with Cyrus, just covets the gold that the underground tunnels lead to, but Cyrus hopes to plant a bomb during Ramadan that will turn the Israelis against the Palestinians. At the films climax, Dalia and Rudy must infiltrate the Orders monastery, free Ozzie,kill Ben Ner by pushing him on a hole with the nuke and prevent an explosion that will push tensions in the Holy Land past the point of no return. In the ensuing explosion, Jews worships on the Wailing Wall, heard the explosion,but ignored it and continued worshiping.

==Cast==
*Jean-Claude Van Damme as Rudy Cafmeyer / Charles Le Vaillant
*Charlton Heston as Prof. Walter Finley
*Sofia Milos as Lt. Dalia Barr
*Brian Thompson as Second/then First Disciple Cyrus Jacob
*Ben Cross as Ben Ner
*Vernon Dobtcheff as Oscar Cafmeyer
*Sasson Gabai as Yuri (as Sasson Gabay)
*Alon Aboutboul as Avram
*Joey Tomaska as Joey
*Peter Malota as Amnon
*Sharon Reginiano as Bassam
*Sami Huri as Lieutenant Itsik

==Box office==
In   and Germany.

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 