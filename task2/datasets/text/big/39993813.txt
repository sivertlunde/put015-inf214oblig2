Banker Margayya
{{Infobox film name           = Banker Margayya image          =  image_size     = caption        =  director       = T. S. Nagabharana producer       = T. S. Narasimhan   B. S. Somasundar writer         = R. K. Narayan screenplay     = T. S. Nagabharana based on       =   narrator       = starring  Jayanthi Sundar Raj Sundar Krishna Urs Musuri Krishnamurthy Master Manjunath music          = Vijaya Bhaskar cinematography = S. Ramachandra editing        = Suresh Urs distributor    = released       =   runtime        = 139 minutes country        = India language       = Kannada budget         = preceded_by    = followed_by    = website        =
}}

Banker Margayya ( ) is 1983 Indian Kannada-language film directed by T. S. Nagabharana, based on the novel The Financial Expert by R. K. Narayan, and starring Lokesh in the lead role.
 Best actor.

==Plot==
Bad culture drives out good culture. Bad money drives out good money. But here, quick money itself, acquired by a miser, though initially boosts his image in the society gradually makes him understand that human values are more important than money.

The film is derived from R. K. Narayans novel The Financial Expert, another of his works set in Malgudi. Margayya is a smart money-lender who, by advising villagers about the rules of borrowing money, sends corrupt bank officials running scared. By a quirk of fate he loses this upper hand. He tries odd jobs to beat poverty but it takes another quirk of fate for his life to change. As with a lot of Narayan’s works, the sum total of joy and despair might end up at zero, but the experience is always up in the positives.

An ironic morality tale about an entrepreneur whose endeavours are constantly ruined by his son. Margayya (Lokesh) starts out as a moneylender sitting under a banyan tree opposite a co-operative bank, filling in forms, and offering advice to the villagers of Narayan’s fictional village of Malgudi, usually on how to circumvent the bank’s bureaucratic process of offering loans. His career as a banker is ruined when his son Balu (Sundarraj) throws away all the account books. Then Margayya publishes a sex manual with its author, a Dr Pal (Urs). The venture is very profitable and Margayya becomes wealthier than all the banks in the area. But Balu is the victim of the salacious book and starts visiting prostitutes. Dr Pal manoeuvres to keep all the profits for himself and Margayya has to start all over again under his banyan tree, with the threatening but beloved presence of his son by his side.

==Cast==
*Lokesh
* Jayanthi
* Sundar Raaj
* Surekha
* Sundar Krishna Urs
* Musuri Krishnamurthy
* Master Manjunath
* Pramila Joshai

==Soundtrack==
{{Infobox album	
| Name = Anveshane
| Longtype = to Anveshane
| Type = Soundtrack	
| Artist = Vijaya Bhaskar
| Cover = 
| Border = Yes
| Alt = Yes	
| Caption =	
| Released = 
| Recorded =	
| Length =  Kannada
| Label = Sangeetha
| Producer = 
| Last album = 
| This album = 
| Next album = 
}}

The music of the film was composed by Vijaya Bhaskar
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Lyricist
|- 1 || Naatya Gaana Manaranjane  || Vani Jayaram || Vijaya Narasimha
|- 2 
| Balu Mojina Ee Youvana  
| Vani Jayaram 
| Vijaya Narasimha
|-
|-
|}

==Awards and honors==
* This movie was selected for Indian Panorama.
* National Film Awards 1983 - Best Kannada Film
* Karnataka State Film Awards 1983-84 - Best Actor - Lokesh

==References==
 

 

 
 
 
 
 
 


 