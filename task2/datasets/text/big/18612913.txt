Doraemon: Nobita and the Galaxy Super-express
{{Infobox film
| name           = Nobita and the Galaxy Super-express
| image          = Nobita and Galactic Express.jpg
| caption        =
| director       = Tsutomu Shibayama
| producer       =
| writer         =
| narrator       =
| starring       = Nobuyo Ōyama Noriko Ohara
| music          = Shunsuke Kikuchi
| cinematography = Hideko Takahashi
| editing        = Hajime Okayasu
| studio         = Asatsu
| distributor    = Toho Company
| released       =  
| runtime        = 97 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          = ¥1.60 billion  ($18 million)
}}
  is a feature-length Doraemon film which premiered on March 2, 1996. The film is a parody of Leiji Matsumotos Galaxy Express 999.

==Plot==
Doraemon went missing for 3 days. But it turns out later that he went to the 22nd century to buy a mystery galactic express train ticket whose destination is a secret until the passengers arrive there and see for themselves.

== Cast of Characters ==
* Nobuyo Ōyama - Doraemon
* Noriko Ohara - Nobita Nobi
* Michiko Nomura - Shizuka Minamoto
* Kaneta Kimotsuki - Suneo Honekawa
* Kazuya Tatekabe - Takesi Goda

==References==
 

== External links ==
*    
*  

 
 

 
 
 
 
 
 


 