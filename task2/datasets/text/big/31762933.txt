Do Paise Ki Dhoop, Chaar Aane Ki Baarish
{{Infobox film
| name = Do Paise Ki Dhoop,Chaar Aane Ki Baarish
| image = Do Paise Ki Dhoop, Chaar Aane Ki Baarish.jpeg
| caption = Film Poster
| writer = Deepti Naval
| starring = Manisha Koirala Rajit Kapur Sanaj Naval
| director = Deepti Naval
| producer =  Kite Films
| distributor = 
| cinematography = Kiran Deohans
| editing = 
| released =  
| runtime =
| country = India
| language = Hindi
| music = Sandesh Shandilya, Gulzar
| budget = 
}}
Do Paise Ki Dhoop, Chaar Aane Ki Baarish is a yet to be publicly released 2009, Bollywood film. Deepti Naval dons the directors role for the first time in this movie. The film has Manisha Koirala, Rajit Kapur and Naval’s nephew, newcomer,  Sanaj Naval in the lead roles. {{cite news
|url=http://www.deccanherald.com/content/24779/intricacies-bonding.html
|title=`The intricacies of bonding 
|date= 
|work=Deccan Herald
|last=Utpal Borpujari
}} 

==Cast==
*Manisha Koirala as Juhi
*Rajit Kapur as Debu
*Sanaj Naval as Kaku
*Milind Soman

==Plot== Gapers Block
|last=Amy Dittmeier }} 

==Production==
There are many sequences in this movie which are shot in the rain, during the Bombay monsoons, with  Kiran Deohans of Jodha Akbar fame taking charge as the cinematographer. {{cite web |url=http://www.cgtantra.com/index.php?option=com_content&task=view&id=336&Itemid=33 |title=FutureWorks delivers brilliant DI for ‘Do Paise Ki Dhoop Chaar Aane Ki Barish’ |date= October 4, 2010
|work= |last=Future Works }}  The music score is by Sandesh Shandilya, except the title song, which was done by Gulzar. The screenplay was written by Deepti Naval in three months.

==Critical Reception==
The film premiered at the market section of 2009 Cannes Film Festival, and got a positive response. 

This movie is Manisha Koiralas comeback movie, in a very sensitive role, said to be inspired by a real story. {{cite news
|url=http://www.mid-day.com/entertainment/2011/mar/240311-Manisha-Koirala-Deepti-Naval-Do-Paise-Ki-Dhoop-Char-Aane-Ki-Baarish.htm
|title=Manisha Koiralas next inspired by a real story |last=Upala KBR |date=March 24, 2011 |work=Mid Day}} 
 Gapers Block gave a moderate rating, calling the plot unoriginal, comparing it to an "elongated episode of Will & Grace". The representation of Debu, who is semi-closeted, was also criticised, for its stereotyped gay characteristics.  while   was of the opinion that,"...characters do not appear stereotypical, but a modern, sensitive and realistic portrait of people struggling to find happiness, with the ebbs and tides of human relationships"    The film was screened in the competition section of   film festival (2010).

The movie was slated to release commercially on 8 March 2012 in India but was postponed. 

==Accolades==
*2009-Best Feature Film -The Indian Film Festival of Houston    The New York Indian Film Festival (MIAAC)   
*2010-Best Actor - Rajit Kapoor at   film festival (2010) 

; Official Selection
* The Indian Film Festival of Houston, 2009
* Chicago South Asian Film Festival, 2010 (Opening film) The New York Indian Film Festival (MIAAC), 2010
* ImagineIndia Film Festival, Spain 2010
* Seattle South Asian Film Festival, 2011 (Opening film) 
* India International Film Festival, Tampa Bay, USA, 2012 
* First Annual Washington DC South Asian Film Festival, 2012 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 