Traces of the Trade: A Story from the Deep North
{{Infobox Film
| name           = Traces of the Trade: A Story from the Deep North
| image          = 
| caption        = 
| director       = Katrina Browne Alla Kovgan Jude Ray
| producer       = 
| writer         = Katrina Browne Alla Kovgan
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Traces of the Trade: A Story from the Deep North is a 2008 documentary film directed by Katrina Browne, Alla Kovgan, and Jude Ray.

==Plot==
The film focuses on the descendants of the DeWolf family, a prominent slave trading family from Rhode Island from 1769 to 1820, and the legacy of the slave trade in the North of the United States. The film follows ten family members as they retrace the triangle trade starting at Linden Place in Bristol, Rhode Island, the hometown of the DeWolfs. The family has been prominent in local businesses and banking, as academics, in the local Episcopal Church and other institutions, and organizing the Bristol Fourth of July Parade.  The film goes with the family to Ghana, where the slaves were purchased and where they meet with current residents, and to Cuba, where James DeWolf owned three sugar and coffee plantations in the 19th century. 

The film competed in the Documentary Competition at the 2008 Sundance Film Festival.    It was shown on PBS TV and nominated for an Emmy Award.

==See also==
*James DeWolf
*Linden Place

==References==
 

==Further reading==
*Thomas Norman DeWolf, Inheriting the Trade, Boston: Beacon Press, 2007

==External links==
*  

 
 
 
 

 