Kochu Kochu Santhoshangal
 
{{Infobox film
| name           = Kochu Kochu Santhoshangal
| image          = Kochu Kochu Santhoshangal.jpg
| alt            = 
| caption        = 
| director       = Sathyan Anthikkad
| producer       = Shantha Nair   Suku Nair
| writer         = C. V. Balakrishnan   Sathyan Anthikkad
| narrator       =  Innocent
| music          = Ilaiyaraaja
| cinematography = 
| editing        =    
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}

Kochu Kochu Santhoshangal ( ) is a 2000 Malayalam film by Sathyan Anthikkad starring Jayaram and his son Kalidas Jayaram. It won the National Film Award for Best Feature Film in Malayalam.

==Plot==
Gopan (Jayaram) is living with his 6 year old son Ashok (Kalidas Jayaram). His neighbor Celin (Kavya Madhavan) falls in love with him. But he reveals that Ashoks mom is alive and he narrates his story.

Asha Lakshmi (Lakshmi Gopalaswamy) is Ashoks mother. Gopan and Asha were in love before they got married. Asha is from a rich family but Gopan is just a videographer at a local studio. Asha, an admired classical dance artist prior to her marriage, left her career to lead a life with Gopan, becoming a simple housewife. Maya Varma (Bhanupriya), a famous classical dance artist, learns of Ashas talent and asks her to join her dance troupe. Ashas aspiring career starts to conflict with the peaceful life she had with Gopan and their child; this, fueled by tensions between Gopan and Ashas father, results in Gopan leaving with his son, Gopan thinks she has a change of heart after dancing with Maya Varma. After that he keeps his child away from Asha. He thinks she is in peace without a family. After he explains the story Celin understands his feelings and one day she brings Asha. She meets her Gopan and Ashok after many years. She also said that she had not even danced after her husband had left her. At last they both reunite and live together happily.

==Awards== National Film Awards
* Best Feature Film in Malayalam
* Best Choreography

==Cast==
*Jayaram as Gopan
*Lakshmi Gopalaswamy as Asha Lakshmi
*Kalidas Jayaram as Ashok
*Lalu Alex as Asha Lakshmis father
*Bhanupriya as Maya Varma Innocent as Jose Siddique as Ramesh Lena as jyodhi  
*Urmila Unni as Vilasini
*K. P. A. C. Lalitha as Jagadamma
*Kavya Madhavan as Celin
*Oduvil Unnikrishnan as Shekharan
*Mala Aravindan as Sankaran
*Thesni Khan as herself
*Yamuna  as Elizabeth

==Soundtrack==
{{Infobox album
| Name        = Kochu Kochu Santhoshangal
| Type        = Soundtrack
| Artist      = Ilayaraja
| Language = Malayalam
| Genre       = Film
|}}

The film features songs composed by Ilayaraja and written by Kaithapram.

{| class="wikitable"
|-
! Song Title !! Singer
|-
| Chellakkaatte ||  KJ Yesudas
|-
| Ghanashyaama ||  Gayathri Asokan
|-
| Kodamanjin Thaazhvarayil   ||  KJ Yesudas, KS Chithra
|-
| Kodamanjin Thaazhvarayil   ||  KS Chithra
|-
| Kodamanjin Thaazhvarayil   ||  KJ Yesudas
|-
| Palappoomazha ||  Bharathvarani
|-
| Shivakaradhamarukalayamaay Naadam ||  KS Chithra, Gayathri Asokan
|-
| Sumasaayaka ||   Kallara Gopan, Geetha Devi
|-
| Title song || 
|}

== External links ==
*  

 

 
 
 
 
 
 


 
 
 