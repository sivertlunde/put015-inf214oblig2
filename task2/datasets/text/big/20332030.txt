It's a Date
{{Infobox film
| name           = Its a Date
| image          = Its a Date poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = William A. Seiter
| producer       = Joe Pasternak
| screenplay     = Norman Krasna
| story          = {{Plainlist|
* Jane Hall
* Frederick Kohner
* Ralph Block
}}
| starring       = {{Plainlist|
* Deanna Durbin
* Kay Francis
* Walter Pidgeon
}}
| music          = Charles Previn (director)
| cinematography = Joseph A. Valentine
| editing        = Bernard W. Burton
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 103 minutes
| country        = United States English
| budget         = 
| gross          = 
}}
 

Its a Date is a 1940 American musical film directed by William A. Seiter and starring Deanna Durbin, Kay Francis, and Walter Pidgeon. Based on a story by Jane Hall, Frederick Kohner, and Ralph Block, the film is about an aspiring actress who is offered the lead in a major new play, but discovers that her mother, a more experienced actress, was hoping to get the same part. Their lives are complicated further when they both get involved with the same man. Distributed by Universal Pictures, Its a Date was remade in 1950 as Nancy Goes to Rio.   

==Plot==
The movie begins with Georgia Drake (Kay Francis) performing on the stage, singing "Gypsy Lullaby" while her daughter, Pamela (Deanna Durbin), watches with her boyfriend Freddie Miller (Lewis Howard). Georgia is an older Broadway actress who was once very famous. Her daughter, Pam, also has great acting skills and hopes to be a successful actress as well. At an after-party celebrating the closing of a play that Georgia was in, Pamela convinces director Sidney Simpson (Samuel S. Hinds) and writer Carl Ober (S.Z. Sakall) to attend one of the plays she is in. After the party ends, Pamela sings "Love is All" with the maid, Sara (Cecilia Loftus), playing piano and her mother listening.

Sidney and Carl go to Connecticut and decide to try out the second act of their new play, St. Anne. They put Pamela in the role of St. Anne. In the performance of the second act, Pamela sings, "Loch Lomond." (This is a particularly acclaimed version of that folk song. ) The directors are impressed with Pamelas performance and offer her the role of St. Anne in their upcoming play. Pamela is very excited, but does not know she is taking over a role which they had originally offered to her mother. Georgia is unaware of what has happened, as she is in Honolulu to prepare herself. 

Pamela travels to Hawaii to ask her mother to coach her. On her journey, she is noticed by pineapple king John Arlen (Walter Pidgeon). Noticing her practicing the play, he thinks that she is a troubled girl who just got jilted by her love, and sets out to distract her by posing as a stowaway.  Eventually they figure out the truth about each other, and become friends. Soon after Pamela arrives in Honolulu, she learns she is replacing her mother in the role of St. Anne. Not wanting to hurt her, she decides to give the role up. In order to keep Georgia from the truth she asks John to take the two of them to dinner in order to avoid a call from Sidney that would reveal the truth to Georgia. The three of them go out to many dinners and spend time together. Pamela becomes determined that she will quit acting and she wants to marry John.

Meanwhile, John has fallen in love with Georgia. At a ball, he tries to avoid Pamela, who wants to propose to him. He knows her intentions and wants to propose to Georgia before Pamela gets the chance. He gets her to sing for everyone at the ball.  Pamela sings, "Quando me nvo - When I go along," from "La bohème". After she finishes the song while the crowd is around her, John takes Georgia outside and proposes to her. Georgia decides that she will marry him, and then she tells the producers that she will not be performing St. Anne, for she wants a real honeymoon. Pamela is initially crushed, but she was not really in love with John. Pamela devotes herself to her role, with the help of her mother, and becomes the star of St. Anne. The movie ends with Pam portraying St. Anne, singing "Ave Maria (Schubert), as John and Georgia watch from the audience.

==Cast==
* Deanna Durbin as Pamela Drake
* Kay Francis as Georgia Drake
* Walter Pidgeon as John Arlen
* Eugene Pallette as Gov. Allen
* Henry Stephenson as Capt. Andrew
* Cecilia Loftus as Sara Frankenstein
* Samuel S. Hinds as Sidney Simpson
* Lewis Howard as Fred Freddie Miller
* S.Z. Sakall as Karl Ober
* Fritz Feld as Oscar
* Virginia Brissac as Miss Holden, Summer Stock Teacher
* Romaine Callender as Mr. Evans, Summer Stock Teacher
* Joe King as First Mate Dan Kelly
* Mary Kelley as Lil Alden, Governors Wife
* Eddie Polo as Quartermaster
* Harry Owens and His Royal Hawaiians as Group Performers
* Harry Owens as Himself, Leader of Harry Owens and His Royal Hawaiians   

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 