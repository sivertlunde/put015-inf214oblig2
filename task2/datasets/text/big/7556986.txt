Cool Air (film)
{{Infobox Film |
  name           = Cool Air | Bryan Moore |
  producer       = Ted Newsom Andrew Migliore |
  writer         = H. P. Lovecraft |
  starring       = Bryan Moore Jack Donner Vera Lockwood |
  distributor    = Lurker Films Beyond Books |
  released       = October 1999 |
  runtime        = 50 min |
  language       = English language|English|
  }} Bryan Moore, and co-starring Jack Donner, with cinematography by Michael Bratkowski. It is based on the short story "Cool Air" by H. P. Lovecraft.

==Plot==
In the 1920s, impoverished horror writer Randolph Carter rents a room from Mrs. Caprezzi, an elderly land lady. Not long after settling into the shabby and almost bare room, he discovers a pool of ammonia on the floor that has leaked down from the room above. Mrs. Caprezzi, while cleaning up the ammonia, regales Randolph with strange stories of Dr. Muñoz (Jack Donner), the eccentric old gentleman who lives in the room upstairs. Later, Randolph suffers a heart attack and painfully makes his way to the doctors room where he is treated with an unconventional medicine and makes a remarkable recovery. Befriending the doctor, Carter soon discovers the awful truth about the doctors condition, why his room is kept intensely cold, and the fragile line that separates life and death.

==Cast==
*Bryan Moore - Randolph Carter 
*Jack Donner - Doctor Muñoz 
*Vera Lockwood - Mrs. Caprezzi 
*Dukey Flyswatter - Street Bum 
*Ron Ford - Repairman

==Crew==
*Michael Bratkowski, Director of Photography

==Production notes==
Cool Air was filmed on location in Glendale, California, USA over several weekends, using a CP-16R regular 16mm camera package owned by DP Michael Bratkowski.  The film was shot on Ilford Black and White regular 16mm film stock, a great filmstock, though prone to film dust and shavings when transferred using a Telecine Flying Spot Scanner, otherwise known as a Rank.

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 


 