Vaiki Odunna Vandi
{{Infobox film 
| name           = Vaiki Odunna Vandi
| image          =
| caption        =
| director       = PK Radhakrishnan
| producer       = PK Radhakrishnan
| writer         =
| screenplay     =
| starring       = Nalini
| music          = Raveendran
| cinematography =
| editing        =
| studio         = Suradha Films
| distributor    = Suradha Films
| released       =  
| country        = India Malayalam
}}
 1987 Cinema Indian Malayalam Malayalam film, directed and produced by PK Radhakrishnan. The film stars  and Nalini in lead roles. The film had musical score by Raveendran.   

==Cast==
*Nalini

==Soundtrack==
The music was composed by Raveendran and lyrics was written by Ezhacheri Ramachandran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Maayanagaram || K. J. Yesudas || Ezhacheri Ramachandran || 
|-
| 2 || Swapnangal Seemantha || P Jayachandran, Chorus, SP Shailaja || Ezhacheri Ramachandran || 
|}

==References==
 

==External links==
*  

 
 
 

 