You'll Like My Mother
{{Infobox film
| name           = Youll Like My Mother
| image          = Youll-Like-My-Mother.jpg
| image_size     = 
| caption        = 
| director       = Lamont Johnson
| writer         = Jo Heims
| narrator       =  Richard Thomas
| music          = 
| cinematography = 
| editing        = 
| studio         = Bing Crosby Productions Universal
| released       = October 13, 1972
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Richard Thomas, and was filmed in Duluth, Minnesota at the Glensheen Historic Estate. In 1977 the Glensheen Mansion became the site of the infamous murders of mansion owner and prominent heiress Elisabeth Congdon and her nurse. 

==Synopsis==
In this psychological thriller, a very pregnant Francesca (Patty Duke) travels from Los Angeles to Minnesota to meet her late husbands mother, Mrs. Kinsolving, whom she has never met before. Mrs. Kinsolving (Rosemary Murphy) is cold to Francesca, questions whether she is actually pregnant with her sons baby, and tells Francesca she wants nothing to do with her or her baby in the future. It soon becomes clear that Francesca cannot leave that night as a blizzard has made the roads impassable. Francesca is forced to stay in the Kinsolving mansion for a few days. She soon begins to suspect that something is amiss due to inconsistencies in information between what her late husband (Matthew) told her and Mrs. Kinsolvings statements to her. 

While Matthew never mentioned he had a sister, Mrs. Kinsolving claims that the mentally challenged and non-verbal Kathleen (Sian Barbara Allen) is Matthews sister. After Mrs. Kinsolving retires for the night, Francesca sneaks around and discovers in the family Bible that Matthews mother (Maria) died eleven days after Matthew was killed in the Vietnam War, that Mrs. Kinsolving is actually Marias sister in law, Katherine, who is Kathleens mother and the mother of Kenny (Richard Thomas), a serial rapist and murderer who is hiding somewhere in the Kinsolving mansion. Francesca goes into labor, but Mrs. Kinsolving refuses to call for an ambulance. She sedates Francesca heavily. When the baby is born, Mrs. Kinsolving announces it is dead and hands the baby over to Kathleen to bury. 

That night, Kathleen rouses Francesca and takes her to the attic where she finds Kathleen has hidden her baby (who is actually very much alive) in a picnic basket. Mrs. Kinsolving, suspecting Francesca is sneaking around the mansion, locks her in her room. Kathleen is able to locate the key to the room and unlocks it, allowing Francesca to care for her baby. One night, Francesca secretly spies the unsuspecting Kenny who is hiding in the basement laundry. She overhears his conversation with Mrs. Kinsolving, and it is menacing. Meanwhile, Mrs. Kinsolving discovers that the family Bible has been opened to the page detailing the date of Maria Kinsolvings death. Mrs. Kinsolving informs Kenny that Francesca knows Maria is dead, but does not think she is aware that Kenny is hiding in the mansion. The next morning, Mrs. Kinsolving announces that the blizzard has cleared enough for a driver to take Francesca into town to take the bus back to Los Angeles. 

At breakfast, the driver arrives—and it is Kenny. Francesca quickly tells Mrs. Kinsolving that she left her gloves in her third floor room and she needs to retrieve them. Instead, she gets her baby from the attic, hides the baby under her coat and flees the mansion. However, Mrs. Kinsolving spots Francesca running away and yells for Kenny to get her. He takes chase, and Kathleen notices. Francesca sees Kenny is quickly gaining ground, and she darts into the carriage house in an attempt to elude him. He locates her, they struggle, and he knocks Francesca unconscious. The baby slips from under her coat. Kenny smiles sadistically and covers the crying babys face with his hand. Suddenly, Kathleen sneaks up behind him and stabs him in the back with a pair of scissors. The film ends with Mrs. Kinsolving cradling her dead son as Kathleen and Francesca, holding her baby, look on, and help arrives.

==Cast==
*Patty Duke as Francesca Kinsolving
*Rosemary Murphy as Mrs. Kinsolving Richard Thomas as Kenny
* Sian Barbara Allen as Kathleen
* Dennis Rucker as Red Cooper

==Reception==
Youll Like My Mother opened quietly in October 1972, and was well received by critics who singled out the performers and the films claustrophobic atmosphere. The film was a modest box office success.

==References==
 

==External links==
* 

 

 
 