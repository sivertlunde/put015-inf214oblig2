Diamonds Are Forever (film)
 
 
{{Infobox film
| name = Diamonds Are Forever
| image = Diamonds Are Forever - UK cinema poster.jpg
| border = yes
| image size = 250px
| caption = British cinema poster for Diamonds Are Forever, designed by Robert McGinnis Charles Gray Lana Wood Jimmy Dean Bruce Cabot
| producer = Harry Saltzman Albert R. Broccoli
| based on =  
| screenplay = Richard Maibaum Tom Mankiewicz
| cinematography = Ted Moore
| director = Guy Hamilton John Barry John Holmes
| studio = Eon Productions
| distributor = United Artists
| released =  
| runtime = 120 minutes
| country = United Kingdom
| language = English
| budget = $7.2 million
| gross = $116 million
}}
 MI6 agent James Bond.
 novel of the same name, and is the second of four James Bond films directed by Guy Hamilton. The story has Bond impersonating a diamond smuggler to infiltrate a smuggling ring, and soon uncovering a plot by his old nemesis Ernst Stavro Blofeld to use the diamonds to build a giant laser. Bond has to battle his nemesis for one last time, in order to stop the smuggling and stall Blofelds plan of destroying Washington, D.C., and extorting the world with nuclear supremacy.
 Las Vegas, camp tone.

==Plot==
James Bond—agent 007—pursues Ernst Stavro Blofeld and eventually finds him at a facility where Blofeld look-alikes are being created through surgery. Bond kills a test subject, and later the "real" Blofeld, by drowning him in a pool of superheated mud.
 M suspects that South African diamonds are being stockpiled to depress prices by Dumping (pricing policy)|dumping, and orders Bond to uncover the smuggling ring. Disguised as professional smuggler and assassin Peter Franks, Bond travels to Amsterdam to meet contact Tiffany Case. The real Franks shows up on the way, but Bond intercepts and kills him, then switches IDs to make it seem like Franks is Bond. Case and Bond then go to Los Angeles, smuggling the diamonds inside Franks corpse.

At the airport Bond meets his CIA ally Felix Leiter, then travels to Las Vegas. At a funeral home, Franks body is cremated and the diamonds are passed on to another smuggler, Shady Tree. Bond is nearly killed by Wint and Kidd when they put him in a cremation oven, but Tree stops the process when he discovers that the diamonds in Franks body were fakes, planted by Bond and the CIA.

Bond tells Leiter to ship the real diamonds. Bond then goes to the Whyte House, a casino-hotel owned by the reclusive billionaire Willard Whyte in Las Vegas, where Tree works as a stand-up comedian. Bond discovers there that Tree has been killed by Wint and Kidd, who did not know that the diamonds were fake.
 Circus Circus casino.

Tiffany reneges on her deal and flees, passing off the diamonds to the next smuggler. However, seeing that OToole was killed after being mistaken for her, Tiffany changes her mind. She drives Bond to the airport, where the diamonds are given to Bert Saxby, who is followed to a remote facility. Bond enters the apparent destination of the diamonds&nbsp;— a research laboratory owned by Whyte, where a satellite is being built by Professor Metz, a laser refraction specialist. When Bonds cover is blown, he escapes by stealing a moon buggy and reunites with Tiffany.

Bond scales the walls to the Whyte Houses top floor to confront Whyte. He is instead met by two identical Blofelds, who use an electronic device to sound like Whyte. Bond kills one of the Blofelds, which turns out to be a look-alike. He is then knocked out by gas, picked up by Wint and Kidd and taken out to Las Vegas Valley, where he is placed in a pipeline and left to die.
 laser satellite using the diamonds, which by now has already been sent into orbit. With the satellite, Blofeld destroys nuclear weapons in China, the Soviet Union and the United States, then proposes an international auction for global nuclear supremacy.

Whyte identifies an oil platform off the coast of Baja California as Blofelds base of operations. After Bonds attempt to change the cassette containing the satellite control codes fails due to a mistake by Tiffany, Leiter and the CIA begin a helicopter attack on the oil rig.
 cruise ship, where Wint and Kidd pose as room-service stewards and attempt to kill them with a hidden bomb. Bond kills them instead.

==Cast== James Bond: MI6 agent 007.
* Jill St. John as Tiffany Case: A diamond smuggler. You Only Live Twice).
*  .
* Bruce Glover as Mr. Wint and Mr. Kidd|Mr. Wint and Putter Smith as Mr. Wint and Mr. Kidd|Mr. Kidd: Blofelds henchmen.
* Norman Burton as Felix Leiter: CIA agent and Bonds ally in tracking Blofeld.
* Joseph Furst as Professor Doctor Metz: A brilliant scientist and worlds leading expert on laser refraction.
* Lana Wood as Plenty OToole: A beauty Bond meets who comes to an unhappy ending.
* Bruce Cabot as Bert Saxby: Whytes casino manager in cahoots with Blofeld.
* Bernard Lee as M (James Bond)|M: The head of MI6.
* Lois Maxwell as Miss Moneypenny: Ms secretary.
* Desmond Llewelyn as Q (James Bond)|Q: Head of MI6s technical department. Joe Robinson as Peter Franks: Diamond smuggler whose identity is taken by Bond.
* Marc Lawrence as Rodney
* Sid Haig as Slumber Inc. attendant
* Leonard Barr as Shady Tree: A stand-up comedian and smuggler.
* Laurence Naismith as Sir Donald Munger: Diamond expert who brings the case to MI6. David Bauer as Morton Slumber: President of Slumber Incorporated, a funeral home.
* Ed Bishop as Klaus Hergerscheimer: Health physicist for WW Techtronics.
* David de Keyser as Doctor
* Lola Larson and Trina Parks as Bambi and Thumper: Henchwomen in charge of guarding Willard Whyte.

==Production== On Her Majestys Secret Service and worked in all previous Bond films as editor, was invited before Hamilton, but due to involvement with another project could only work on the film if the production date was postponed, which the producers declined to do. 

===Writing=== For Your Eyes Only (1981), as Eons arrangements with the Fleming estate did not permit them to use McClorys works.

The original plot had as a villain Auric Goldfingers twin, seeking revenge for the death of his brother. The plot was later changed after Albert R. Broccoli had a dream, where his close friend Howard Hughes was replaced by an imposter. So the character of Willard Whyte was created, and Tom Mankiewicz was chosen to rework the script.    Mankiewicz says he was hired because Cubby Broccoli wanted an American writer to work on the script, since so much of it was set in Las Vegas "and the Brits write really lousy American gangsters"&nbsp;— but it had to be someone who also understood the British idiom, since it had British characters.  David Picker from United Artists had seen the stage musical Georgy! written by Mankiewicz, and recommended him; he who was hired on a two week trial and kept on for the rest of the movie. Mankiewicz later estimated the novel provided around 45 minutes of the films final running time. 
 Jack and Seraffimo Spang, but used the henchmen Shady Tree, Mr. Wint and Mr. Kidd. 
 Lord Nelsons famous cry, "Las Vegas expects every man to do his duty." Maibaum was misinformed; there were no Roman galleys or Chinese junks in Las Vegas, and the idea was too expensive to replicate, so it was dropped.   

Maibaum may have thought the eventual oil rig finale a poor substitute, but it was originally intended to be much more spectacular. Armed frogmen would jump from the helicopters into the sea and attach limpet mines to the rigs legs (this explains why frogmen appear on the movies poster). Blofeld would have escaped in his BathoSub and Bond would have pursued him hanging from a weather balloon.  The chase would have then continued across a salt mine with the two mortal enemies scrambling over the pure white hills of salt before Blofeld would fall to his death in a salt granulator. Permission was not granted by the owners of the salt mine. It also made the sequence too long. Further problems followed when the explosives set up for the finale were set off too early; fortunately, a handful of cameras were ready and able to capture the footage. 

===Casting=== On Her title role. This project was abandoned because another production of Macbeth (the Roman Polanski version) was already in production.
 You Only David Bauer, You Only Live Twice.
 Paul Williams was originally cast as Mr. Wint.  When he couldnt agree with the producers on compensation, Bruce Glover replaced him. Glover said he was surprised at being chosen, because at first producers said he was too normal and that they wanted a deformed, Peter Lorre-like actor. 

Film star Bruce Cabot, who played the part of Bert Saxby, died the following year and this turned out to be his final film role. Jimmy Dean was cast as Willard Whyte after Saltzman saw a presentation of him. Dean was very worried about playing a Howard Hughes pastiche, because he was an employee of Hughes at the Desert Inn. 

Actresses considered for the role of Tiffany Case included: Raquel Welch, Jane Fonda and Faye Dunaway. Jill St. John had originally been offered the part of Plenty OToole but landed the female lead after Sidney Korshak who assisted the producers in filming in Las Vegas locations recommended his client St. John,  who became the first American Bond girl.    Lana Wood was cast as Plenty OToole following a suggestion of screenwriter Tom Mankiewicz.  The woman in the bikini named "Marie", who in the beginning of the film is convinced by Bond to give up the location of Blofeld, was Denise Perrier, Miss World 1953. 

A cameo appearance by Sammy Davis, Jr. playing on the roulette table was filmed, but his scene was eventually deleted. 

===Filming===
  Cap DAntibes in France for the opening scenes, Amsterdam and Lufthansas hangar at Frankfurt Airport, Germany.   
 Circus Circus was a Bond fan, he allowed the Circus to be used on film and even made a cameo.   The cinematographers said filming in Las Vegas at night had an advantage: no additional illumination was required due to the high number of neon lights.  Sean Connery made the most of his time on location in Las Vegas. "I didnt get any sleep at all. We shot every night, I caught all the shows and played golf all day. On the weekend I collapsed – boy, did I collapse. Like a skull with legs." He also played the slot machines, and once delayed a scene because he was collecting his winnings. 
 Palm Springs, designed by John Lautner, became Willard Whytes house.  The exterior shots of the Slumber mortuary were of the Palm Mortuary in Henderson, NV. The interiors were a set constructed at Pinewood Studios, where Ken Adam imitated the real buildings lozenge-shaped stained glass window in its nave. During location filming, Adam visited several funeral homes in the Las Vegas area, the inspiration behind the gaudy design of the Slumber mortuary (the use of tasteless Art Deco furniture and Tiffany lamps) came from these experiences.  Production wrapped with the crematorium sequence, on 13 August 1971. 
 Ford to 1971 Mustang actual NASA fibreglass  tires had to be replaced during the chase sequence because the heat and irregular desert soil ruined them. 
 Joe Robinson.  The car chase where the red Mustang comes outside of the narrow street on the opposite side in which it was rolled, was filmed over three nights on Fremont Street in Las Vegas. The alleyway car roll sequence is actually filmed in two locations. The entrance was at the car park at Universal Studios and the exit was at Fremont Street, Las Vegas. It eventually inspired a continuity mistake, as the car enters the alley on the right side tires and exits the street driving on the left side.     While filming the scene of finding Plenty OToole drowned in Tiffanys swimming pool, Lana Wood actually had her feet loosely tied to a cement block on the bottom. Film crew members held a rope across the pool for her, with which she could lift her face out of the water to breathe between takes. The pools sloping bottom made the block slip into deeper water with each take. Eventually, Wood was submerged but was noticed by on-lookers and rescued before actually drowning. Wood, being a certified diver, took some water but remained calm during the ordeal, although she later admitted to a few "very uncomfortable moments and quite some struggling until they pulled me out." 

===Music===
 
 John Barry revealed that he told Bassey to imagine she was singing about a penis. Bassey would later return for a third performance for 1979s Moonraker (film)|Moonraker.
 John Barry, his sixth time composing for a Bond film.

With Connery back in the lead role, the "James Bond Theme" was played by an electric guitar in the somewhat unusual, blued gunbarrel sequence accompanied with prismatic ripples of light, and pre-credits sequence, and in a full orchestral version during a hovercraft sequence in Amsterdam.

==Release and reception==
Diamonds are Forever was released on 14 December 1971. It grossed $116 million worldwide,  of which $43 million was from the United States. 
 The Man with the Golden Gun and Die Another Day.  Total Film listed Mr. Wint and Mr. Kidd, and Bambi and Thumper, as the first and second worst villains in the Bond series (respectively). 
 Best Sound Fiddler on the Roof. 

==See also==
 
* Outline of James Bond

==References==
 

==Sources==
 
* 
 

==External links==
 
 
*  
*  
*  
*  
*  

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 