Les Frères Pétard
  
{{Infobox film
| name           = Les Frères Pétard
| image          = 
| caption        = 
| director       = Hervé Palud
| producer       = Christian Fechner Bernard Artigues
| writer         = Hervé Palud Igor Aptekman
| starring       = Gérard Lanvin Jacques Villeret Josiane Balasko Valérie Mairesse Daniel Russo
| music          = Jacques Delaporte
| cinematography = Jean-Jacques Tarbès
| editing        = Roland Baubeau
| distributor    = Gaumont Film Company
| studio         = Les Films Christian Fechner Les Films Optimistes Films A2
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         =
| gross          = $16,345,275 
}}

Les Frères Pétard ( )  is a French film directed by Hervé Palud released in 1986 in film|1986.

== Cast ==
* Gérard Lanvin : Manu
* Jacques Villeret : Momo
* Josiane Balasko : Aline
* Valérie Mairesse : Brigitte
* Daniel Russo : Harky
* Thomas M. Pollard : Sammy Le Black
* Patrice Valota : Teuch
* Alain Pacadis : la balance
* Cheik Doukouré : Razzo
* Philippe Khorsand : un flic 
* Dominique Lavanant : la policière
* Michel Galabru : monsieur Jabert, le père de Momo
* Raymond Aquilon : un flic
* Jean-Paul Bonnaire : un flic
* Albert Dray  : un flic 
* Michel Bonnet : le vigile
* René Duclos : Nanard
* Norbert Letheule : Aldo
* Smaïn : un petit trafiquant dans le train
* Tina Aumont : la fêtarde déguisée
* Mark Bellay
* Philippe Bellay
* Jean-Louis Calvet
* Wally Chetout
* Guy Cuevas : légyptien

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 


 