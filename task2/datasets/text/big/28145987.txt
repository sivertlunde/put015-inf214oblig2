Uninhabited (film)
 
 
{{Infobox film
| name = Uninhabited
| image = Uninhabited.jpg
| caption = Theatrical film poster Bill Bennett
| producer = Bill Bennett Silvana Milat Paul Quin
| writer = Bill Bennett
| starring = Geraldine Hakewill Henry James Tasia Zalar
| cinematography = Lachlan Milne
| music = Peter Miller
| editing = Jason Ballantine
| released =  
| runtime = 93 minutes
| country = Australia
| language = English
}} Bill Bennett. The film premiered at the 2010 Cannes Film Festival and in Australia during the 2010 Melbourne International Film Festival. It tells the story of a young couple vacationing at a deserted island, later discovering it has a disturbing history.

==Plot==

Marine biology student, Beth (Geraldine Hakewill), and Harry (Henry James), are a young couple seeking a distinctive holiday trip, and they spend ten days on a deserted and idyllic coral island on the Great Barrier Reef in Australia, recording many of their adventures with a camcorder. The first two days manifest that they are not alone and the island has a disturbing history. Haunted by a presence, they find an abandoned grave next to an old shack, the latter which has an old drawing and seven tally marks inside on the wall. When Beth and Harry sift through their camera, they discover they were filmed sleeping. Alarmed, Beth wants to leave, but Harry intends to stay, believing kids are playing pranks. They sight fishermen on a boat with guns and later realize that the grave has been remodeled, now decorated with red coral, with a wooden cross embedded at the head of the grave that reads "Coral" on it.

The following day, Beth and Harry find severed sea cucumbers (trepang) hanging from a nearby line. By this episode, they realize they are in trouble and their phone, which they plan to use to call for rescue, goes missing. Searching for the phone, they come across Spiro (Billy Milionis) and Elias (Terry Siourounis), the two lunatic Greek fishermen they had seen the day before. This leads to Beth and Harry being tied up after snooping around their space and boat, accusing them of stealing their phone. Beth is sexually molested, but a female ghost appears and injures one man, as the fishermen flee from the island on their boat, and Harry and Beth manage to free themselves.
 grave danger, the next day they plan to create a bonfire in the hopes a rescue team finds them. However, a ringing phone brings Harry alone to the shack and he encounters Coral, who kills him with a knife and hangs his naked body in a tree above her grave. As night falls, Beth finds his hanging corpse dripping blood onto the grave and faints.
 Bob Baines) comes back to pick up the couple he dropped off, but no one is seen. He goes to enter the shack finding the journal resting on a chair. A menacing Beth confronts and presumably kills him. One of the final scenes show Beths grave next to Corals, appearing as if they joined forces.

==Cast==
*Geraldine Hakewill as Beth
*Henry James as Harry
*Tasia Zalar as Coral Bob Baines as Jackson
*Billy Milionis as Spiro
*Terry Siourounis as Elias

==Location==
Filmed on location on Masthead Island, Great Barrier Reef,  Queensland, Australia.

Most of the music performed by the band The Truth is. (Ref: film credits end of film.)
https://maps.google.com/maps?ll=-23.538889,151.726667&q=loc:-23.538889,151.726667&hl=en&t=m&z=12

==Reception==
Uninhabited received mixed to positive reviews from audiences, with a Rotten Tomatoes Want-To-See score of 73% and an IMDB score of 4.3.

==External links==
*  

 

 
 
 
 