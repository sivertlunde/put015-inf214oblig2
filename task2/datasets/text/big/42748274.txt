The Crowded Hour
{{infobox film
| title          = The Crowded Hour
| image          =
| imagesize      =
| caption        = 
| director       = E. Mason Hopper
| producer       = Adolph Zukor Jesse Lasky Channing Pollock(play) Edgar Selwyn(play) John Russell(scenario)
| starring       = Bebe Daniels Kenneth Harlan
| cinematography = J. Roy Hunt
| editing        =
| distributor    = Paramount Pictures
| released       = April 20, 1925
| runtime        = 70 minutes; 7 reels
| country        = USA
| language       = Silent film..(English intertitles)
}} Channing Pollock and Edgar Selwyn.  

==Cast==
*Bebe Daniels - Peggy Laurence
*Kenneth Harlan - Billy Laidlaw
*T. Roy Barnes - Matt Wilde
*Frank Morgan - Bert Caswell
*Helen Lee Worthing - Grace Laidlaw
*Armand Cortes - Captain Soulier (*as Armand Cortez)
*Alice Chapin - GrandMere Buvasse
*Warner Richmond - Operator

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 


 