Sree Ayyappanum Vavarum
{{Infobox film 
| name           = Sree Ayyappanum Vavarum
| image          =
| caption        =
| director       = NP Suresh
| producer       = Purushan Alappuzha
| writer         = Purushan Alappuzha Alappuzha Karthikeyan (dialogues)
| screenplay     = Purushan Alappuzha Alappuzha Karthikeyan
| starring       = Prem Nazir Srividya MG Soman Nalini
| music          = A. T. Ummer
| cinematography = PN Sundaram
| editing        = NP Suresh
| studio         = Sreedevi Movies
| distributor    = Sreedevi Movies
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, directed by NP Suresh and produced by Purushan Alappuzha. The film stars Prem Nazir, Srividya, MG Soman and Nalini in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
 
*Prem Nazir
*Srividya
*MG Soman
*Nalini Swapna
*Mohanlal
*Cochin Haneefa
*Prathapachandran
*Unnimary Rajkumar
*Balan K Nair
*Kaduvakulam Antony
*Mala Aravindan Meena
 

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by , Poovachal Khader and Koorkkancheri Sugathan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Amme Naarayana || K. J. Yesudas ||  || 
|-
| 2 || Dharma saasthaave || K. J. Yesudas || Poovachal Khader || 
|-
| 3 || Dhyaaye chaaru || K. J. Yesudas ||  || 
|-
| 4 || Dhyaaye Kodi || K. J. Yesudas ||  || 
|-
| 5 || Dhyaayedananda || K. J. Yesudas ||  || 
|-
| 6 || Eeshwara jagadeeshwara || K. J. Yesudas || Poovachal Khader || 
|-
| 7 || Ezhazhake noorazhake || Ambili, Chorus || Poovachal Khader || 
|-
| 8 || Naagendra haaraaya || K. J. Yesudas ||  || 
|-
| 9 || Naagendra Haaraaya   || K. J. Yesudas ||  || 
|-
| 10 || Nilaavenna pole || S Janaki || Poovachal Khader || 
|-
| 11 || Om bhoothanatha || K. J. Yesudas ||  || 
|-
| 12 || Om Namasthe || K. J. Yesudas ||  || 
|-
| 13 || Shabarigireeshaa || K. J. Yesudas || Poovachal Khader || 
|-
| 14 || Sharanam Viliyude || K. J. Yesudas || Koorkkancheri Sugathan || 
|-
| 15 || Thathraagathaashwa || K. J. Yesudas ||  || 
|}

==References==
 

==External links==
*  

 
 
 

 