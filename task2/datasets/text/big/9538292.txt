Hindustan Ki Kasam
 
{{Infobox film
| name           = Hindustan Ki Kasam 
| image          =
| caption        =  Chetan Anand
| producer       = Ravi Anand
| writer         = 
| narrator       = 
| starring       = Raaj Kumar Amjad Khan Amrish Puri Parikshat Sahni
| music          = Madan Mohan
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  1973
| runtime        = 
| country        = India
| language       = Hindi
}}
 Chetan Anand, who has previously made popular war film, Haqeeqat (1964 film)|Haqeeqat (1964) on Indo-China war.,    the film however didnt perform well commercially. 

==Cast== Chetan Anand Vijay Anand
*Raaj Kumar
*Priya Rajvansh
*Balraj Sahni
*Padma Khanna
*Amjad Khan
*Amrish Puri
*Parikshat Sahni
*Bharat Kapoor
*Nitin Sethi
*Satyendra Kapoor

== Synopsis ==

The movie is different from other war movies in Indian cinema as it vividly describes the Indian Air Force|IAFs role in 1971 Indo-Pakistani war in the western sector. 

The film starts with an air raid by Pakistan Air Force (PAF) on an Indian Air Force (IAF) airbase in the western sector in India. After the raid a pilot (Raaj Kumar) takes an oath while standing near the body of a dead ground crewman - "Jawaab dene aaunga, is jawan ki kasam, Hindustan Ki Kasam" (we will avenge, we swear by this soldier, we swear by Hindustan). The credits start while the title song of the movie is played in the background (sung by the Mohammed Rafi and Manna Dey).
 intelligence plant Mohini,who is Tahiras (Priya Rajvansh) look alike, fiancee of a PAF pilot (Amjad Khan). Mohini goes to Pakistan and starts working as a singer on Pakistan TV (PTV) studio (where the radar which jams the radio frequency of IAF jets is also kept). Mohini informs the IAF about the jammer. IAF asks her to leave the building in the night after her programme is done so that they can raid the building. 

PAFs counter intelligence learns about her and they zero in on her on that very night. As soon her programme is finished, the IAF air raids the studio while she is still inside. In the dogfight with Pak sabers Raaj Kumars jet is destroyed and he crashes. He radios for help and the IAF fighters destroy the pursuing Pakistani soldiers and their vehicles. The pilot and Tahira are evacuated. The movie ends with IAFs flypast on the Republic day parade in New Delhi with the title song in the background.

== References ==
 

== External links==
* 

 
 
 
 
 
 
 
 
 
 