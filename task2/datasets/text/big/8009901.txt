Kids From Shaolin
 
 
 
{{Infobox film name = Kids From Shaolin image = ShaolinTemple2KidsFromShaolinJapanesePoster.jpg caption = Japanese poster traditional = 少林小子 simplified = 少林小子 pinyin = Shàolín Xiǎozǐ jyutping = Siu3 Lam4 Siu2 Zi2}} director = Chang Hsin-yen producer =  writer =  starring = Huang Qiuyan Yu Chenghui Yu Hai Hu Jianqiang music =  cinematography = Hu Jianqiang editing =  distributor =  released =   runtime = 99 minutes country = Hong Kong China language = Mandarin budget =  gross = HK $22,287,595.00
}} 1984 Cinema Hong Kong/Cinema Chinese Hong Kong action cinema|action-comedy kung fu film directed by Chang Hsin-yen.  
 Yu Hai Shaolin Temple film, which is also directed by Chang. However, the plot has no bearing with the first movie and hence Kids from Shaolin is a sequel to the original in name only.
 Huang Qiuyan (黃秋燕), as his rival and later love interest. Li first met Huang, also a martial artist, in martial school and they have two daughters. (The couple have since divorced and Li is married to Nina Li Chi, a former Hong Kong actress.)

==Plot==
  Shaolin monk, Lijiang where they stay in a hut.

The Long boys are playful and often bicker and fight with the daughters of the Feng (凤/鳳, "Phoenix") family who live just across the river and practice the Wudangquan style of martial arts. The mischievous San Long ("Third Dragon", Jet Li), the oldest of the Long children, likes to tease the third sister of the Feng family, San Feng ("Third Phoenix", Huang Qiuyan) who is a tomboy in her late teens and who has a nasty temper.

The Feng patriarch Bao San Feng (Yu Chenghui) is trying for a boy heir, yet he has only nine daughters.  Meanwhile, the Long family are saving up in order to pay the bride price - ten oxen - so that Tian Long can marry the eldest Feng girl, Tai Feng. The marriage plans are met with some resistance: the Feng matriarch likes Tian Long, but Bao San Feng believes he is out to steal his Wudang martial arts. Nonetheless, he agrees to marry off his eldest daughter if his wife gives birth to a son.
 Taoist soothsayer to infiltrate the Fengs to learn their martial arts and abduct their daughters.

The Feng matriarch manages to conceive and bear a male son. The bogus priest now dupes Bao San Feng into believing the Long family is his  . The Long family, he claims, has been throwing off the yin and yang balance for the Fengs, making it impossible for the wife to bear a male heir.
 feudal rules, San Feng and San Long are sentenced to be drowned, but the two manage to escape underwater.

San Long hides San Feng in a cave. San Long is struck by San Fengs beauty in female clothes and San Feng is grateful to San Long for rescuing her. The two develop romantic feelings for each other.

The bogus priest informs Bao San Feng where his daughter is hiding and Bao San Feng pursues her, then fights San Feng in the cave, accusing the latter of abducting his daughter and stealing his swordplay style. Tian Long finds the two fighting, breaks them up and allows Bao San Feng to bring San Feng home. 

Meanwhile, the Long boys are maligned by the bogus priest for abducting the Feng newborn. The Long family vow never to step into the Feng residence again.

Once their evil plot has become successful, the bandits burn down the Longs hut and show their true colors to Bao San Feng. They attempt to kidnap his daughters. The Feng family tries desperately to fight them until the Long family arrives. By combining their martial arts expertise, the two families roundly defeat and kill all remaining bandits.

The film ends with a dual marriage: Tian Long marries Tai Feng (the eldest daughter) while Yi Long marries  Yi Feng. The families are reconciled. Bao San Feng readily admits his folly and San Long and San Feng have also become a couple.

==Cast==
{| class="wikitable"
|-
! Character !! Actor !! Role
|-
| Bao San Feng || Yu Chenghui || father/head of Wudang clan
|-
| Yi Feng || Ding Lan || second Wudang daughter
|- Huang Qiuyan || third Wudang daughter
|-
| Tian Long || Hu Jianqiang || Shaolin "dad"
|- Yu Hai || Shaolin "uncle"
|-
| San Long || Jet Li || oldest Shaolin son
|-
||| Ji Chunhua || one-eyed bandit
|-
||| Jian-kui Sun || cross-eyed bandit
|-
||| Pan Qingfu || kabuki bandit
|}

==External links==
*  at Hong Kong Cinemagic
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 