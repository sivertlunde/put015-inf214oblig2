Shiner (2004 film)
{{Infobox film
| name           = Shiner
| image          = ShinerDVDCover.jpg
| image_size     =
| caption        = DVD Cover to Shiner
| director       = Christian Calson
| producer       =
| writer         = Christian Calson
| narrator       =
| starring       = Scott Stepp Derris Nile Nicholas T. King David Zelina Carolyn Crotty Seth Harrington Conny Van Dyke
| music          =
| cinematography =
| editing        =
| distributor    = TLA Releasing
| released       = 2004
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}Shiner is a 2004 released film written and directed by Christian Calson.

==Plot== stalked by Bob, a shy loner who still lives at home with his mother. Third is Reg and Linda, who discover their sex life improves when they hit each other during the act.

==Cast==
* Tony - Scott Stepp
* Danny - Derris Nile
* Charles - Ryan Soteres
* Linda - Carolyn Crotty
* Reg - Seth Harrington
* Tim - David Zelina
* Bob - Nicholas King
* Bobs Mom - Conny Van Dyke

==Reviews==
* "The homoerotic subtext many found in "Fight Club" is spelled out in "Shiner," about some ostensibly straight, even homophobic, guys whose sparring edges across the line between macho horseplay and sexual masochism. It takes a while to realize the laughs in writer/helmer Christian Colsons first feature are intentional—no-budget production values, deliberately awkward scene rhythms and minimalist character development make pic look at first very much like a solemnly silly fetish video. By the end, a sort of deliberate absurdity is clear, though this vid-shot exercise in behavioral extremity may be too rough-hewn and unratable for arthouse play." - Dennis Harvey, Variety (magazine)|Variety  Castro Theater during this festival so controversial that all 66 of our sponsors passed on it." - Michael Lumpkin, Festival Director-Frameline Film Festival 
* "Rife with ugly behavior, Shiner rejects the trend of queer filmmakers seeking straight understanding." - Carla Meyer, San Francisco Chronicle "Wedded Bliss at Frameline28," San Francisco Chronicle  June 13, 2004  , retrieved via ProQuest Newsstand 
* "Most people will probably be put off by the relentless violence. But if you can stick it out, you will find yourself asking some Big Picture questions." - Steve Weinstein, New York Blade 

==References==
 

==External links==
*  
*  
*   - Interview with director Christian Calson

 
 
 
 