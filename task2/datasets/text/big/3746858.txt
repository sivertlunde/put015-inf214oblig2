Requiem (2006 film)
 
{{Infobox film
| name           = Requiem
| image          = Requiemposter.jpg
| caption        = Theatrical release poster
| director       = Hans-Christian Schmid
| producer       = Hans-Christian Schmid Bernd Lange
| starring       = Sandra Hüller Burghart Klaußner Imogen Kogge Anna Blomeier Nicholas Reinke Jens Harzer Walter Schmidinger
| music          =
| cinematography = Bogumil Godfrejów
| editing        = Bernd Schlegel Hansjörg Weißbrich
| distributor    =
| released       = 2 March 2006
| runtime        = 93 minutes
| country        = Germany
| language       = German
| budget         =
| gross          =
}}
Requiem is a 2006 German drama film directed by Hans-Christian Schmid. It stars Sandra Hüller as a woman with epilepsy, Michaela Klingler, believed by members of her church and herself to be possessed. The film steers clear of special effects or dramatic music and instead presents documentary-style filmmaking, which focuses on Michaelas struggle to lead a normal life, trapped in a limbo which could either represent demonic possession or mental illness, focusing on the latter.

The film offers a medical condition (epilepsy) as the center of the affliction as opposed to demonic possession for the real-life events of Anneliese Michel, a German woman who was believed to have been possessed by six or more demons. These events also served as the basis of Scott Derricksons 2005 film The Exorcism of Emily Rose.

==External links==
*  

 
 
 
 
 
 
 
 
 


 
 