Anth
{{Infobox film
| name           = Anth
| image          = 
| caption        = Movie Poster
| director       = Sanjay Khanna
| producer       = Ashok Honda
| writer         = Dilip Shukla
| screenplay     = 
| story          = 
| based on       =  
| starring       = Sunil Shetty Somy Ali Paresh Rawal
| music          = Anand-Milind
| cinematography = Najeeb Khan
| editing        = Ashok Honda
| studio         = 
| distributor    = 
| released       = 10 June 1994
| runtime        = 138 mins
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}

Anth is a 1994 Indian action film directed by Sanjay Khanna and produced by Ashok Honda and starred Sunil Shetty and Somy Ali in pivotal roles. Paresh Rawal, Deepak Shirke, Mohan Joshi, Makrand Deshpande, Alok Nath and Vijayendra Ghatge also featured in the film. 

==Plot==

Kali (Makrand Deshpande) is a local ruffian who harasses students in his college. The principal Satyaprakash (Alok Nath) is helpless to stop him because Kali is the son of Dabala (played by Paresh Rawal) who is one of the biggest gangsters in town. The principal has two sons and a daughter - Vikas (Vijayendra Ghatge), Vijay (Sunil Shetty) and Pooja. Vijay is in love with Priya (Somy Ali) a colleague of his in his engineering company. 

Kali rapes Pooja, and then the corrupt inspector Shirke (Deepak Shirke) puts the blame on Poojas fiancee, and has him killed while in custody. This enrages Vijay and he embarks upon a bloody and violent fight to cleanse the city of ruffians like Kali and Dabala and Shirke. (Mohan Joshi) shows up in a cameo as an honest ACP.

==Cast==
* Sunil Shetty...Vijay Saxena 
* Somy Ali...Priya 
* Paresh Rawal...Dhabla 
* Alok Nath...Principal Satyaprakash Saxena
* Deepak Shirke...Inspector Shirke 
* Neena Gupta...Mrs. Vikas Saxena 
* Rita Bhaduri...Priyas mom 
* Makrand Deshpande...Kali 
* Vijayendra Ghatge...Advocate Vikas Saxena 
* Mohan Joshi...ACP Kulkarni

==Soundtrack==

The music was composed by Anand-Milind while Sameer wrote the lyrics.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Na Vaada Karte Hain"
| Suresh Wadkar, Sadhana Sargam
|-
| 2
| "Tu Deewani Main Deewana"
| Kumar Sanu, Sadhana Sargam
|-
| 3
| "Aa Jaa Jaane Jaa"
| Abhijeet Bhattacharya|Abhijeet, Kavita Krishnamurthy
|-
| 4
| "Dil Mera Yahan Wahan"
| Sapna Mukherjee
|-
| 5
| "Jaan E Jaan Dil Na Jala"
| Kumar Sanu, Kavita Krishnamurthy
|-
| 6
| "Aaja Sikhadoon Tujhe" Poornima
|}

==Reception==
The film was a success at the box office; that was mostly due to the action sequences featuring Sunil Shetty. One particular long action sequence features Shetty and Ali on a motorcycle, with Shetty clenching a stun-gun in his hand. Also the raunchy song "Aa Ja Jaane Jaa", gained popularity among the masses, thanks to some steamy shots between the leads (Sunil Shetty and a wet Somy Ali).

The film won the Filmfare Best Action Award.

==External links==
*  

 
 
 

 