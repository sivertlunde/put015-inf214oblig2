The Medicine Man (1930 film)
:For the 1933 UK comedy film, see The Medicine Man (1933 film).
{{Infobox film
| name           = The Medicine Man
| image          =
| image_size     =
| caption        =
| director       = Scott Pembroke
| producer       = Julius Hagen (producer) Phil Goldstone
| writer         = Ladye Horton (writer) Elliott Lester (play) Eve Unsell (adaptation)
| narrator       =
| starring       = See below
| music          =
| cinematography = Max Dupont Arthur Reeves
| editing        = Byron Robinson Tiffany Studios
| released       = June 15, 1930
| runtime        = 66 minutes (USA)
| country        = USA
| language       = English
| budget         =| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 American comedy film directed by Scott Pembroke, released by Tiffany Pictures, and starring Jack Benny, Betty Bronson and Eva Novak. 

The son and daughter of a shopkeeper fall in with a travelling medicine man. This was an early role for Jack Benny. After talking pictures took over the silent film, vaudeville died and Jack Benny and many other comedians went to motion pictures. Even though Benny was a comedian in the movie he did not do any jokes at all.  The film was adapted from a play by Elliot Lester. 

A print is preserved in the Library of Congress. 

== Cast ==
*Jack Benny as Dr John Harvey
*Betty Bronson as Mamie Goltz
*E. Alyn Warren as Goltz
*Eva Novak as Hulda
*Billy Butts as Buddy
*Adolph Millar as Peter
*George E. Stone as Steve
*Tom Dugan as Charley
*Vadim Uraneff as Gus
*Caroline Rankin as Hattie
*Dorothea Wolbert as Sister Wilson

==References==
 

== External links ==
* 
* 
* 
* 

 
 
 
 
 
 
 
 


 