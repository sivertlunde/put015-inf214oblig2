Il malato immaginario
{{Infobox film
| name = Hypochondriac (Il malato immaginario)
| image = Il malato immaginario.jpg
| director = Tonino Cervi
| writer = Alberto Sordi, Tonino Cervi, Cesare Frugoni (from Molière)
| starring = Alberto Sordi
| music = Piero Piccioni
| cinematography = Armando Nannuzzi
| editing = Nino Baragli
| producer =
| distributor =
| released =  
| runtime = 110 min
| awards =
| country = Italy
| language = Italian
| budget =
}} 1979 Cinema Italian comedy film directed by Tonino Cervi.   It is a loose adaptation of Molières Le Malade imaginaire set in 1600 papal Rome. 

== Plot ==
In Rome the rich and stingy landowner Argante believed to be sick in any possible harm, although born as a fish. Another thing that the cruel man holds great attention is his money and his contract with a young doctor for the wedding of his daughter Lucrezia. In fact, the good catch is considered the girl a total moron who knows nothing about medicine, but Don Argante not pay much attention and just think to combine the deal as soon as possible. Meanwhile his wife, without his or her knowledge, betrays him with another. Comes the doctor betrothed his daughter to the house of Argante, as he finds himself in yet another false relapse, and thus begins to visit him. Argante now realizes the nonsense that says the young but is only about money and does not care. Later, between the master and his servant turns a strong argument that Argante is not loved by anyone in the family except by his servants. To test the family Argante is persuaded by the servants to pretend to be dead in order to discover the hatred that his wife and family have of him and so it happens. As if that were not enough rich to the poor has been stolen also deposit money.

== Cast ==
*Alberto Sordi: Don Argante
*Laura Antonelli: Tonietta
*Bernard Blier: Il dottor Purgone
*Giuliana De Sio: Angelica
*Marina Vlady: Lucrezia, seconda moglie
*Christian De Sica: Claudio Anzalone
*Ettore Manni: Lamministratore dei poderi
*Vittorio Caprioli: Vincenzo, il vecchio servo
*Stefano Satta Flores: Il notaio
*Carlo Bagno: Il dottor Anzalone
*Eros Pagni: Il dottore

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 