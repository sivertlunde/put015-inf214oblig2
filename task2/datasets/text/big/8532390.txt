The Wounds
{{Infobox film
| name           = The Wounds
| image          = The Wounds.jpg
| caption        =
| writer         = Srđan Dragojević
| starring       = Dušan Pekić Milan Marić Dragan Bjelogrlić Branka Katić Vesna Trivalić
| director       = Srđan Dragojević
| producer       = Dragan Bjelogrlić Milko Josifov Igor Živković
| cinematography = Dušan Joksimović
| editing        = Petar Marković RTS Cobra Films
| distributor    =
| released       = 15 May 1998 (FR Yugoslavia)
| runtime        = 103 min. FR Yugoslavia Serbian
| music          = Aleksandar Habić
| awards         =
| budget         = $800,000 
}}
The Wounds ( ) is a 1998 Serbian drama film written and directed by Srđan Dragojević.

It depicts the violent lives of two boys in Belgrade as they aspire to make names for themselves in the citys underworld. The story takes place throughout the 1990s, against the backdrop of Yugoslav Wars and growing ethnic hatred.

The film won a Bronze Horse at the Stockholm International Film Festival and a FIPRESCI Prize at the Thessaloniki Film Festival, "For its powerful, dramatic depiction of the brutal reality and complexity of life in the Balkans today." 

==Plot==
Ranes opening sequence announces it as being "dedicated to the generations born after Josip Broz Tito|Tito".

The film follows the fate of two boys, Pinki and Švaba, growing up in Novi Beograd during the 1991-1996 period.
 Partisan Boško Croatian Serb persecution from the Croatian fascist movement Ustaše.

Living in the block of apartment buildings in Novi Beograds neighbourhood of Paviljoni, both kids are extremely juvenile, though Pinki is a bit more thoughtful and articulate while Švaba is moody, impulsive, and prone to anger outbursts.

The duo also has another friend in the neighbourhood — Dijabola, an eager, geeky, and bespectacled outsider whose sexy and aloof mother Lidija is a well-known television host. Though they hang out with him, Pinki and Švaba mostly treat Dijabola poorly. He is constantly the butt of their insults and occasionally even gets beaten up by them.

The story begins in the late summer of 1991 as the kids watch Serbian troops (regular JNA troops and various volunteer militias) going off to war in neighbouring Croatia where the Battle of Vukovar is raging. Pinkis father Stojan is extremely frustrated about being forced into early retirement by the JNA army and thus missing the chance to go to war. He spends his days glued to the television set, watching news reports from Vukovar and cheering on the JNA. By now he has transformed into a nationalist and has become extremely irritable, getting into petty quarrels with neighbours and venting his anger along the ethnic and political lines. He has also found a new idol - instead of Tito hes now a huge supporter of Slobodan Milošević. Pinki, for his part, is mostly oblivious to the events around him as he spends most of his time compulsively masturbating.
 UN trade embargo, and the war has spread from Croatia to Bosnia as well. Entering their early teens, Pinki, Švaba and Dijabola begin their fascination with a neighbour across the street Kure who drives a nice car, makes regular robbing excursions to Germany while dating a trashy kafana singer. Theyre deeply impressed with his swagger and lifestyle and are ecstatic one day when he invites them to unload his car thats full of stuff he brought over from Germany. In fact, he sends Dijabola away and picks only Švaba, but then upon Švabas suggestion tells Pinki to come along as well.
 same places Jesus was wounded two thousand years ago. Pinki manages to survive and after some time he escapes from the hospital, and calls his friend to make peace. The truce is more than terrible, as the wounded boy has, after an unwritten rule, to inflict five identical wounds to his friend, so the friendship can be rebuilt.
After shooting Švaba three times, he considers wounding him one more time instead of the required two. They are suddenly interrupted by a furious Dijabola who shoots at them, especially Švaba, for killing his mother. A shootout occurs and Švaba and Dijabola are killed. In the end, Pinki, who is wounded and is lying on the ground, laughs at the audience by claiming that he 
"made out better than you."

==Cast==
* Dušan Pekić &ndash; Pinki
* Milan Marić &ndash; Švaba
* Dragan Bjelogrlić &ndash; Čika Kure
* Vesna Trivalić &ndash; Lidija
* Andreja Jovanović &ndash; Dijabola
* Branka Katić &ndash; Suzana
* Miki Manojlović &ndash; Stojan (Pinkis father)
* Gorica Popović &ndash; Nevenka (Pinkis mother)
* Nikola Kojo &ndash; Biber
* Zora Doknić &ndash; Švabas grandmother
* Danica Maksimović &ndash; Ninana, the prostitute
.
. Bata Stojković &ndash; Neighbour
* Seka Sablić &ndash; Neighbour
* Radoslav Milenković &ndash; Police inspector
* Nikola Pejaković &ndash; Kafana owner
* Dragan Maksimović &ndash; Patient in the hospital
* Milorad Mandić &ndash; Body builder

==Production== Goran and Dragan Bjelogrlić|Dragan), wanted him to do before potentially going to America. In the end, after going through several Hollywood meetings during which he reportedly got offered scripts that he found "execrable",  Dragojević decided not to move to America, choosing instead to do the smaller of the two films in Serbia.

The story is built around an occurrence depicted in a mid-1990s television news item by Predrag Jeremić that aired on RTV Studio B about two criminally-involved adolescents who started out as friends before turning on one another. In a fit of anger one youth shot the other five times, but the wounded youth survived and recovered. Later, attempting to repair their friendship, the shooter offered his recovered friend to shoot him five times in the exact body parts in order to get even. The recovered victim accepted the offer and shot his assailant back five times before they resumed their friendship.    Dragojević was reportedly told of this by friends and eventually decided to write a screenplay around it. He claims to have purposely avoided actually watching the TV report because he didnt want to have his writing, casting, and directorial decisions subconsciously influenced by images or language in it. After finally seeing the report upon films completion, Dragojević said he was amazed with the visual and behavioural similarities between the two sets of teens. 

Dragojević cast Dušan Pekić for the lead role of Pinki, selecting him from 5,000 youths who auditioned for the role, noting that Pekić shared a similar background with character Pinki.    The film proved to be Pekićs first and only role, as he died in 2000. 

The filming began in fall 1997 and had 78 shooting days. 

The films was funded in large part by the state institutions such as the state-run broadcaster Radio Television of Serbia|RTS. Among its corporate sponsors, the movies closing credits also list the McDonalds|McDonalds Corporation and Fruit of the Loom.

==Release and reaction==
As Rane was going into theatrical release, the films director, Srđan Dragojević, put out an accompanying statement explaining his personal motivation to revisit the subject of Yugoslav Wars, this time from the perspective of those living behind the frontlines. In it he directly accused the Milošević administration of forcing the young people of Serbia to grow up in the country flooded by the wave of primitivism and nationalism where those who went through puberty while Vukovar and Sarajevo were being destroyed have been made the terrible victims whose wounds will never heal: "This is a story about young criminals whom I believe have a deep moral right to be violent, even to murder, despite the political unacceptability of this idea. An insensitive society and a totalitarian Serbian regime have made thousands of Serbian teenagers dangerous, senseless killing machines, ironically whose main victims are themselves". 

The film was released in FR Yugoslavia (Serbia and Montenegro) in May 1998 where it became a cinema hit with 450,000 admission tickets sold  despite its promotional cycle in the country being severely impacted by the regimes refusal to run the films ads on state television RTS (then under general manager Dragoljub Milanović) due to being dissatisfied with the countrys bleak portrayal in the film.  Talking to the Serbian edition of Playboy in 2004, the movies producer Dragan Bjelogrlić said the following:  
In January 2014, as guest on Veče sa Ivanom Ivanovićem, Bjelogrlić expanded on the problem Rane had with Serbian authorities back in 1998.  

Starting in April 1999, the film began a theatrical distribution in Croatia thus becoming the first Serbian film in the post-Yugoslav Wars era to have distribution in that country. Another curiosity of its release in Croatia was the fact that it was subtitled. Even its title was translated from Rane to Ozljede, all of which became subject of much outrage  and ridicule.   It became a hit in the cinemas regardless, selling more than 40,000 admission tickets (~42,000) in Croatia.

Looking back on his acting career, in February 2012, Bjelogrlić brought up the role of Čika Kure as being one of the dearest to him, but also revealed a later personal realization that he "couldve done a much better job portraying it". 

==Critical reception==

===United States===
Amy Taubin of The Village Voice finds Ranes pace to be erratic and frequently frantic, seeing its final scene as having "a relentless, demented logic of its own" while noting "theres nothing gratuitous about the violence of Dragojevićs cinematic language". She further remarks that the movies imagery is "a bit too invested in martyrdom" representing "a politicized and catholicized version of live fast, die young, leave a beautiful corpse" before she detects "a touch of Holden Caulfield in Pinkis voiceover narration", while noting "the film plays like a cross between Los Olvidados and Dead Presidents". 

The New York Times Janet Maslin brings up Emir Kusturicas Underground (1995 film)|Underground as having "demonstrated the anguish in the Balkans may be better conveyed through raucous, stinging satire than by more conventionally compassionate means" and notes that Dragojević employed the same tonal approach, which "helped him define a new generation of thugs who arouse both horror and pity". She concludes that the movie is "filmed with enough stylistic bravado and sardonic narration to recall Trainspotting (film)|Trainspotting, seeing it best appreciated as an answer to the question Švaba poses late in the movie (in response to Pinkis statement "Youve got to care about something") — "What if you cant find anything?" 
 A Clockwork Burgess and Kubrick could only have imagined; where Clockwork carried a heavy allegorical weight, Rane has the disturbing feel of reportage". 

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 