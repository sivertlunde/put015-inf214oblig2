Nikté
 
{{Infobox film
| name        = Nikté
| image       = NikteMoviePoster.jpg
| alt         =
| caption     = 
| director    = Ricardo Arnaiz
| producer    = Socorro Aguilar Ricardo Arnaiz
| writer      = Antonio Garci Omar Mustre
| based on    =  Pedro Armendáriz Alex Lora Maya Zapata Regina Orozco Jorge Arvizu Silverio Palacios Regina Torné Enrique Garay
| music       = Gabriel Villar
| editing     = Ricardo Arnaiz Gabriel Villar
| studio      = Animex Producciones
| distributor = Universal Pictures (Mexico only)
| released    =  
| runtime     = 86 minutes
| country     = Mexico
| language    = Spanish
| budget      = $2,400,000
| gross       = 
}} animated adventure adventure comedy Pedro Armendáriz Alex Lora, Jorge Arvizu, and Regina Torné. It premiered in theaters on December 18, 2009.

==Plot==
The story starts with a girl and her family at La Venta park. The girl only thinks about herself, and her family goes with the tour guide while she stays at the entrance listening to her music. She leans on the Olmec head and discovers something in the Olmec head which she is transferred back to the times of Olmec.

==Cast==
*Sherlyn as Nikté
*Pierre Angelo as Chin Pedro Armendáriz Jr as Kaas Alex Lora as Chamán Chanek
*Maya Zapata as Xtabay
*Regina Orozco as Ij Aesu
*Jorge Arvizu as Guardián de Roca
*Silverio Palacios as Jéfe Chaneke
*Regina Torné as Diosa Luna / Meztli
*Enrique Garay as Kike Garaytl

==Production==
Animex Producciones was in charge of coordinating the production which involved several animation studios in Mexico, including Grupo EsComic! and Estudio Haini.

==Box office==
This film was a box-office failure, due to an unsuccessful competition with James Camerons Avatar (2009 film)|Avatar.   

==Character name dispute and lawsuit==
On June 27, 2008, Rolando Tamayo sued director, Ricardo Arnaiz over the films "plagiarized" characters.  

==References==
 

==External links==
*  
*  

 
 
 
 
 

 