The Ballad of the Valiant Knight Ivanhoe
{{Infobox film name           = Ballad of the Valiant Knight Ivanhoe image          = image_size     = caption        = director  Sergei Tarasov producer       = writer         = Leonid Nekhoroshev Walter Scott (novel) narrator       = starring       = Tamara Akulova Peteris Gaudins Boris Khimichev Leonid Kulagin Romualds Ancans Boris Khmelnitsky music          = Igor Kantyukov cinematography = Roman Veseler editing        = distributor    = studio         = Mosfilm released       = 1983 runtime        = 92 minutes country        = Soviet Union language       = Russian budget         = gross          =
}} Soviet film, based on the novel Ivanhoe by Walter Scott. It reached the 9th place in Soviet box office distribution of 1983 with 28.4 million viewers.

The filming took place in Khotyn Fortress and Kamianets-Podilskyi Castle.

The movie features songs originally written and performed by Vladimir Vysotsky for another movie, The Arrows of Robin Hood, but removed from the latter for political reasons.

==Cast==
* Tamara Akulova as Lady Rowena
* Peteris Gaudins as Ivanhoe
* Boris Khimichev as Brian De Bois-Guilbert
* Leonid Kulagin as Cedric the Saxon Richard the Lionheart Robin Hood
* Yuri Smirnov as Friar Tuck
* Aleksandr Filippenko as Wamba
* Vitautas Tomkus as Reginald Front-de-Boeuf Prince John
* Nikolai Dupak as Prior Aymer
* Maya Bulgakova as Ulrica
* Oleg Fedulov as Knight
* Vladimir Talashko as Messenger
* Grigoriy Lyampe as Isaac

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 
 