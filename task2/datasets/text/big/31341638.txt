The Villiers Diamond
{{Infobox film
| name           = The Villiers Diamond
| image          = 
| image size     = 
| caption        = 
| director       = Bernard Mainwaring John Findlay
| writer         = David Evans
| screenplay     = 
| story          = F. Wyndham-Mallock
| narrator       = 
| starring       = Edward Ashley Evelyn Ankers Frank Birch
| music          = 
| cinematography = Stanley Grant
| editing        = 
| studio         = 
| distributor    = Twentieth Century Fox
| released       =  
| runtime        = 50 minutes
| country        = UK
| language       = English
| budget         = 
| gross          = 
}} British crime film directed by Bernard Mainwaring and starring Edward Ashley, Evelyn Ankers and Frank Birch.  A man is threatened with scandal when he accidentally acquires a stolen diamond.

==Cast==
* Edward Ashley - Captain Dawson
* Evelyn Ankers - Joan Raymond
* Frank Birch - Silas Wade
* Liam Gaffney - Alan OConnel
* Leslie Harcourt - Barker
* Julie Suedo - Mrs Forbes
* Sybil Brooke - Miss Waring
* Bill Shine - Joe
* Margaret Davidge - Mrs. Benson
* Anita Sharp-Bolster - Mlle. Dulac

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 