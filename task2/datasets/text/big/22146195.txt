For a Moment, Freedom
{{multiple issues|
 
 
}}

{{Infobox Film
| name           = For a Moment Freedom
| image          = 
| caption        = 
| director       = Arash T. Riahi 
| producer       = Veit Heiduschka, Wega Film
| writer         = Arash T. Riahi
| narrator       = 
| starring       = Navid Akhavan Pourya Mahyari Elika Bozorgi Sina Saba Payam Madjlessi Behi Djanati-Ataï Kamran Rad Said Oveissi Fares Fares Ezgi Asaroglu Toufan Manoutcheri Michael Niavarani Soussan Azarin Johannes Silberschneider
| music          = Karuan
| cinematography = Michael Riebl
| editing        = Karina Ressler
| distributor    = Les Films du Losange
| released       =  
| runtime        = 110 minutes
| country        = Austria France Persian Turkish Turkish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
For a Moment Freedom ( ;  ) is a 2008 film, written and directed by Austrian-Iranian filmmaker Arash T. Riahi.

==Synopsis==
For a Moment Freedom tells of the odyssey of three Iranians groups of refugees: a married couple with a child, two young men with two children, and two men who are friends despite the differences between them.

They have all managed to escape from Iran and Iraq, but now they are stuck in the Turkish capital; although freedom is at last almost within their grasp, first they have to wait in a dubious hotel, hoping each day that their applications for asylum will be approved. This enforced break in their journey towards independence is characterised by both hope and utter uncertainty.

The young Austrian-Iranian filmmaker Arash T. Riahi depicts the plight of people trying to flee their homeland and the curious, transitory state of asylum-seekers with tragic comedy and great suspense.

==Cast==
{| class="wikitable"
|-
! style="background-color:tan;" | Actor
! style="background-color:silver;" | Role
|-
| Navíd Akhavan || Ali
|-
| Pourya Mahyari || Mehrdad
|-
| Elika Bozorgi || Azy
|-
| Sina Saba || Arman
|-
| Payam Madjlessi || Hassan
|-
| Behi Djanati Ataï || Lale
|-
| Kamran Rad || Kian
|-
| Said Oveissi || Abbas
|-
| Fares Fares || Manu
|-
| Ezgi Asaroglu || Jasmin
|-
| Toufan Manoutcheri || Mother of Asy and Arman
|-
| Michael Niavarani || Father of Asy and Arman
|-
| Soussan Azarin || Grandmother
|-
| Muhammed Cangören || Grandfather
|-
| Kourosh Asadollahi || Ahmad
|-
| Halilibrahim Sahin || Son of Ahmad
|-
| Kave Foladi || Prosecutor
|-
| Johannes Silberschneider || Pifko
|}

Filming Locations

* Turkey
* Austria
* Germany

== Prizes ==

===Awards Nr. 13 & 14 ===

 

*Special Jury Award
*Special Audience Award

Jury

* Eva Zaoralová
* Carlos Hugo Aztarain
* Phillip Bergson
* Carlo di Carlo
* Dunja Clemens

===Awards Nr. 11 & 12 ===

 

* Grande Premio Ficcao - Grand Prize for Best Movie
* Premio do Público Ficcao - Publics Prize

Jury
 Midnight Express, The Commitments, Birdy (film)|Birdy)
*  , Wes Cravens Scream (film series), Wild Things) Princesse Mononoké, Sonatine (1993 film)|Sonatine, Le voyage de Chihiro) Dazed and Confused, Superman Returns)
*  )
*  , 1492, James Bond Goldeney)
* Malti Sahai (Festivaldirector & Curator/Indien)
*  , Oswaldianas)
*  )

===Award Nr. 10===

 

* Mejor Guion - Best Screenplay

Jury

* Ben Gazzara actor, Killing of a Chinese Bookie)
* Ariana Aizemberg actress
* Analía Gadé actress
* Juan José Jusid stage director Apasionados, Made in Argentina)
*  , Love in the Time of Cholera)
* Roberto Stabile manager of ANICA

===Awards Nr. 7, 8 & 9===

 

* Prix du Jury Officiel - Main Award of the Jury
* Prix du Jury étudiant - Student Award
* Prix du Public - Publics Prize
 
Jury

* Mahamat-Saleh Haroun cinéaste, président du fond SUD du CNC
* Patrick Berthomeau éditorialiste au journal Sud Ouest, ancien critique de cinéma
* Caroline Huppert réalisatrice, présidente du FIPA Biarritz
* Stéphane Krémis historien, ancien directeur de la revue L’Histoire
* Hugues Pagan ancien inspecteur de police à Paris, auteur de romans policiers et scénariste de films et de séries de télévision

===Awards Nr. 5 & 6===

For a Moment Freedom has won two more awards at the Festival du Cinéma Européen en Essonne in France. The film was awarded both the Public Award and the Student Award. This festival, which has been running for the last ten years, specializes in political films.

 

Jury

* Maurice Benichou actor, appeared in Caché (film)|Caché by Michael Haneke
* Jocelyne Desverchère actress
* Jean-Pascal Hattu director
* Pierre-Yves Borgeaud director and screenwriter
* Jérôme Mallien actor 
and a number of students.

===Award Nr. 4===

* Viennale 08-International Filmfestival - Vienna Filmaward for the best Austrian film 2008
 
For a Moment Freedom wins the Vienna Film Prize for the Best Austrian Film of the year at the 2008 Vienna Film Festival.

"On the basis of a majority decision the 2008 Vienna Film Prize is awarded to the feature film For a Moment Freedom by Arash T. Riahi. The film deals with one of the oldest human longings - the desire for freedom, the desire to live in a better, more just place. The film produces unexpected images of freedom: a piece of paper with a visa, a smile before an execution, an embrace, a cooked swan. In a gripping juxtaposition of tragedy and humor the film succeeds in depicting the reality of people who are refugees. The film shows the happiness and unhappiness of its protagonists without changing its tone. We were impressed by the commanding narrative style as well as the performances of the actors and actresses. We would like to express our warmest congratulations to Arash T. Riahi and his team.”

Jury

The jury consisted of,
* Andrea Braidt
* Dimitré Dinev
* Michael Kerbler
* Ernst Molden
* Sylvie Rohrer

===Award Nr.3===

 

* Alain Poire Screenvision Award for Best Director

The prize is named after Alain Poire, the French producer who produced a large number of films, including works by Robert Bresson, Claude Autant-Lara and Yves Robert.

Jury

In their statement the jury praised the outstanding direction and acting as well as the universal message of the film.

* The President of the Jury was the French/Tunisian producer Ariel Zeitoun (La Banquiére, The Wounded Man (film)|LHomme blessé by Patrice Chérau, Le dernier gang)
* Anna Galiena actress (Jamon, Jamon by Bigas Luna, A life in Suitcases by Peter Greenaway, Trois vies et une seule mort by Raoul Ruiz)
* Carmelo Romero Spanish theatre director, curator, festival director (Institut du Cinéma et des Arts Audiovisuelles ICAA, Festival de Vallodolid, Member of the Expert Committee of the FIAPF)
* Delphine Gleize director (Carnages Un certain Regard, Cannes 2002)
* Alfred Lot screenwriter, director (Roselyne et les lions, I AmantArtémisia)

===Award Nr. 2===

For a Moment Freedom was awarded the Golden Eye, the Award for the Best Debut Feature Film, at the 4th Zurich Film Festival by a jury headed by Peter Fonda. 
The Zurich Film Festival (ZFF) is one of the most important international festivals for young talent and is held annually in Zurich. The film festival is committed to promoting the works of young directors and screenwriters from all over the world.

 

Jury

"Weve chosen a film that is both important and moving. Powerfully unveiling the consequences of todays politics on human lives. A talented and strong cast, compelling intertwined stories and the focus on the human experience convinced us."

* Peter Fonda actor and director/president of the jury (Easy Rider)
* Claudia Puig film critic USA Today Pulitzer prize-winner
* Hervé Schneid Editor for Sally Potter, Mike Figgis, Jean-Pierre Jeunet (Delicatessen, * Orlando (film)|Orlando, Amélie) Stephen Nemeth founder and general manager of Rhimo Films (Fear and Loathing in Las Vegas, Fields of Fuel, winner of the Sundance Audience Award)
* Andrea Staka director and producer (Das Fräulein won the 2006 Golden Leopard at the Locarno Film Festival)
*   (2003))
* Dror Shaul screenwriter and director (His Sweet Mud won numerous prizes including the Sundance IFF Grand Jury Award.)

===Award Nr. 1===

At the renowned "A" festival, the World Film Festival in Montreal, For a Moment Freedom  has won an award for the best debut feature film, the Golden Zenith for the Best First Fiction Feature Film 2008. 

Jury

* Pierre-Henri Deleau France
* Denis Héroux Canada
* Armand Lafond Canada

== References ==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 