The Chosen (film)
{{Infobox film
| name           = The Chosen
| image          = http://www.moviefanfare.com/wp-content/uploads/2010/04/Chosen-DVD_sm.jpg
| image_size     = 
| caption        = 
| director       = Jeremy Kagan
| writer         = Edwin Gordon
| based on     =  
| narrator       = 
| starring       = Maximilian Schell Rod Steiger Robby Benson
| music          = Elmer Bernstein
| cinematography = Arthur J. Ornitz
| editing        = 
| distributor    = 20th Century Fox
| released       = April 30, 1982
| runtime        = 108 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} bestselling book of the same name by Chaim Potok published in 1967. It stars Maximilian Schell and Rod Steiger. It won three awards at the 1981 Montréal World Film Festival. 

==Plot== Hasidic Rebbe but Danny is somewhat distant from him. Danny has also been going to the library and reading books on psychology and amazes Reuven on how well he can remember portions of the psychology books. It turns out that Reuvens father has been showing him these books. Reuven and Danny also go to Sabbath service in Dannys Hasidic community as Danny is eager for Reuven to meet his father. Dannys father approves of their friendship. However, Rebbe Saunders disapproves of Prof. Malters writings which doesnt surprise Malter. Rebbe Saunders also wishes for Danny to become a rabbi, as this has been tradition for several generations but Danny doesnt seem too pleased to pursue it.

Some time later, the boys begin attending Hirsch College, a Jewish university. While Reuven efinds college life exciting and challenging, Danny finds it harder to adjust, especially in one of his psychology courses where his professor denounces Sigmund Freud whom Danny was fascinated by. During this time, World War II ends and Reuven takes Danny to his first movie. After the movie, a newsreel begins and broadcasts the horrors of the concentration camps and the genocide of over six million Jews in Europe; Rebbe Saunders is horror-stricken by this too. Shortly after this, the question arises of whether a Jewish state should be formed in Palestine, as many European Jews have emigrated there. This results in Prof. Malter heading to Chicago for a conference to debate the issue and Reuven goes to stay with Dannys family. He meets the rest of the family, including Shaindel whom he begins to have feelings for. Reuven is eventually accepted by the family and attends many services and ceremonies, including a Jewish wedding. However, during the reception Danny tells Reuven he cant pursue a relationship with Shaindel, because she already has an arranged marriage. 

After Prof. Malter returns, he becomes engrossed in the creation of Israel and writes several articles and speeches about it. This controversial issue, however, causes friction between Hasidic and Modern Orthodox Jews. While the Modern Orthodox Jews believe that creating a Jewish state in Palestine is the right thing to do, the Hasidic Jews believe that only the Messiah will grant them Palestine. This results in Rebbe Saunders excommunicating Reuven from the family and thus causes growing friction between Danny and Reuven. Eventually, the United Nations passes the partition to create Israel. Rebbe Saunders allows Reuven to come back and the two friends reconcile. It is also revealed that Danny plans to transfer to Columbia University to pursue a psychology degree and Reuven plans to be a rabbi. Rebbe Saunders approves of Dannys plans and finally reveals why he was so distant from Danny: when Danny was younger, his father was impressed by how much Danny remembered material from reading something. However, this ended up making Danny a know-it-all and feeling indifferent toward other people. As a result, Rebbe Saunders had to teach him empathy and the wisdom and pain of being alone by distancing himself from Danny and thus "teaching through silence", just like how Rebbe Saunders father taught him. Rebbe Saunders also tells Danny to keep his Jewish faith. This results in a tearful Danny reconciling with his father. In the end, Danny and his father now have a good relationship again and Danny loses his appearance that was part of his Hasidic tradition, such as shaving his beard and cutting his hair. Danny and Reuven part ways as Danny prepares for his new life.

==Cast==
*Maximilian Schell as Professor David Malter
*Rod Steiger as Reb Saunders
*Robby Benson as Danny Saunders Barry Miller as Reuven Malter
*Hildy Brooks as Mrs. Saunders
*Kaethe Fine as Shaindel Saunders
*Ron Rifkin as Baseball Coach Robert Burke as Levi Saunders
*Lonny Price as Davey
*Evan Handler as Goldberg
*Douglas Warhit as Sam
*Jeff Marcus as Schwartzi
*Stuart Charno as First Baseman
*Richard Lifschutz as Hassidic Coach
*Clement Fowler as Doctor
*John Pankow as Bully
*Richard Ziman as Bully
*Bruce MacVittie as Bully
*Val Avery as Teacher

==Reception==
The Chosen was well received by most critics and holds an 82% rating for the Rotten Tomatoes website. 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 