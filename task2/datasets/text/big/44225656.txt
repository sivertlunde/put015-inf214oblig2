Aksharam
{{Infobox film
| name           = Aksharam
| image          =
| caption        =
| director       = Sibi Malayil
| producer       = John Paul John Paul
| starring       = Suresh Gopi Annie Jagathy Sreekumar Kalabhavan Mani
| music          = Perumbavoor G Ravindranath
| cinematography = Venu
| editing        = L Bhoominathan
| studio         = Pegas Pictures
| distributor    = Pegas Pictures
| released       =  
| country        = India Malayalam
}}
 1995 Cinema Indian Malayalam Malayalam film, directed by Sibi Malayil. The film stars Suresh Gopi, Annie, Jagathy Sreekumar and Kalabhavan Mani in lead roles. The film had musical score by Perumbavoor G Ravindranath.   

==Cast==
 
*Suresh Gopi Annie
*Jagathy Sreekumar
*Kalabhavan Mani
*Anju Aravind
*Jose Pellissery
*Tej Sapru
*M. S. Thripunithura Madhavi
*N. F. Varghese
*Kozhikode Narayanan Nair
*Narendra Prasad
*Santhakumari Baby Sonia
*T. P. Madhavan
 

==Soundtrack==
The music was composed by Perumbavoor G Ravindranath and lyrics was written by Gireesh Puthenchery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Navarasabhaavam || K. J. Yesudas || Gireesh Puthenchery || 
|-
| 2 || Thankakkalabha   || K. J. Yesudas || Gireesh Puthenchery || 
|-
| 3 || Thankakkalabhakumkumam || K. J. Yesudas || Gireesh Puthenchery || 
|}

==References==
 

==External links==
*  

 
 
 

 