Heart of Stone (2009 film)
 
Heart of Stone is a 2009 documentary film about Weequahic High School in Newark, New Jersey, the United States, directed by Beth Toni Kruvant, with Zach Braff serving as executive producer. The film relates the struggles of Principal Ron Stone and the rest of the schools administration, plus students and alumni to return the school, working with African American and Jewish alumni, to its previous glory in the years before the 1967 Newark riots.

==The film==
The film documents Weequahic High School which graduated some of the top students in the country after opening in 1932 and was "known as one of the top schools in America before 1960", with graduates such as novelist  , July 17, 2009. Accessed October 14, 2009.  Heart of Stone focuses on the crisis in education in Newark as an example for the entire nation, showing how an alumni group raised $400,000 with one of its co-founders being Hal Braff, the attorney father of actor Zach Braff. 

The films title comes from the school principals last name, but also the directors belief that a heart of stone was needed to face the difficult challenge of earning the respect of the schools students, many of whom are gang members. Stones strategy was not to confront the gang bangers but to make use of their "natural leadership abilities" and to use their skills to help improve themselves and their school and to help end the pattern of violence in the school building. Goldrich, Lois.  , Jewish Standard, May 15, 2009. Accessed October 14, 2009. 

==Critical responses== Kaiser Permanente Thrive Award for telling a story of thriving in the face of adversity.  The film won the Best Feature Film at the Philadelphia Film Festival and the Best Documentary Film at the New Jersey Film Festival and Urban Suburban Film Festival.  Heart of Stone had its theatrical premiere at The Roxie in San Francisco on October 30, 2009. 

==Director== political and fiscal crisis of 2001. The Right to Be Wrong chronicles an Israeli and Palestinian friendship. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 