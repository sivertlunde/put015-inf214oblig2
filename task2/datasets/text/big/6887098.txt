Fanfan la Tulipe
 
{{Infobox film
| name           = Fanfan la Tulipe
| image          = Fanfanlatulipe.jpg
| caption        = Original 1952 Film Poster
| director       = Christian-Jaque
| producer       = Francis Cosne Georges Dancigers Alexandre Mnouchkine
| writer         = René Wheeler René Fallet Christian-Jaque Henri Jeanson René Wheeler Henri Jeanson
| narrator       =
| starring       = Gérard Philipe Gina Lollobrigida
| music          = Maurice Thiriet Georges Van Parys Christian Matras
| editing        = Jacques Desagneaux
| distributor    = Filmsonor
| released       = 21 March 1952
| runtime        = 102 minutes
| country        = France
| language       = French
| budget         =
| preceded_by    =
| followed_by    =
}}
 French comedy comedy adventure remade in 2003 with Penélope Cruz in Lollobrigidas role.

==Plot summary==
The film is set in France during the Seven Years War.  As the film begins, Fanfan (Gérard Philipe) is a charming, attractive young man who is trying to escape a shotgun marriage. At this vulnerable point in his life, he is approached by the daughter of a recruiting officer, Adeline (played by Gina Lollobrigida), who tells him that if he joins the army, he will find fame, fortune, and will marry the kings daughter.  Accordingly he joins the army, only to discover that she made the whole thing up in order for her father to get a recruiting bonus.  Nevertheless, encouraged by a series of improbable circumstances, he accepts her prediction as his destiny.  A series of events ensues which shows off to great advantage his athleticism and leadership ability.  As the film progresses, we become aware of a developing attraction between himself and Adeline which however conflicts with his perceived “destiny” of marrying a kings daughter.

==Cast==
* Gérard Philipe as Fanfan La Tulipe
* Gina Lollobrigida as Adeline La Franchise
* Marcel Herrand as Louis XV
* Olivier Hussenot as Tranche-Montagne
* Henri Rollan as Le maréchal dEstrée
* Nerio Bernardi as La Franchise
* Jean-Marc Tennberg as Lebel
* Geneviève Page as La marquise de Pompadour
* Sylvie Pelayo as Henriette de France
* Lolita De Silva as La dame dhonneur
* Irène Young as Marion
* Georgette Anys as Madame Tranche-Montagne
* Hennery as Guillot
* Lucien Callamand as Le maréchal de Brandebourg

==Production== Christian Matras. colorized version was created (supervised by Sophie Juin for Les Films Ariane) and issued in 2000 on DVD in Europe alongside the original version. 

==Awards==
;Won Berlin International Film Festival – Silver Berlin Bear    Cannes Film Festival – Best Director   

;Nominated
*1952 Cannes Film Festival – Grand Prize of the Festival

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 