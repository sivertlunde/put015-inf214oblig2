The Hunchback of Notre Dame II
{{multiple issues|
 
 
 
}}
{{Infobox film
| name           = The Hunchback of Notre Dame II 230px
| caption        =DVD cover
| director       = Bradley Raymond
| producer       = Chris Henderson Hiroshi Saotome Stephen Swofford
| writer         = Flip Kobler Cindy Marcus Jule Selbo
| starring       = Tom Hulce Jennifer Love Hewitt Haley Joel Osment Demi Moore Kevin Kline Michael McKean Jason Alexander Charles Kimbrough Jane Withers Carl Johnson Randy Petersen  Kevin Quinn  Walter Edgar Kennon (songs)  Chris Canute 
| editing        = Colleen Halsey Peter Lonsdale
| studio         = Walt Disney Pictures Walt Disney Animation Japan Buena Vista Home Entertainment
| released       =    
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         =
}} Disney animated The Hunchback of Notre Dame. It was produced by Walt Disney Animation Japan. Unlike many Disney film sequels, almost the entire key cast of the first film returns, apart from Tony Jay (since his character Judge Claude Frollo has died at the end of the first film), the late Mary Wickes (who had voiced Laverne the gargoyle, died during the release of the first film, and instead, is voiced by Jane Withers) and David Ogden Stiers (who had voiced the Archdeacon, who instead is voiced by Jim Cummings).

==Plot== Judge Claude Esmeralda have married and have a six-year-old son named Zephyr. Quasimodo is now an accepted part of Parisian society; though he still lives in Notre Dame with his gargoyle friends Victor, Hugo, and Laverne, and still serves as the cathedrals bell-ringer.
 the Emmanuel), the inside of which is decorated with gold lining and enormous jewels of various colors. He sends Madellaine, his aspiring assistant, to discover the whereabouts of La Fidèle. She encounters Quasimodo without seeing his face, and the two of them seem to get along quite well despite having just met, but she runs away after seeing his face, shocked at his grotesque hideous appearance. The gargoyles convince Quasimodo to go to the circus to see her again. Sarousch captures the audiences attention when he makes an elephant disappear, while his associates steal from the audience. Sarousch forces Madellaine to follow Quasimodo and obtain the information he wants. At first she tries to persuade Sarousch not to do so, but he reminds her of her background: years ago, when Madellaine was only six, Sarousch caught her trying to steal coins from him, but instead of turning her over to the cruel Frollo, Sarousch took her under his wing out of sympathy. She follows Quasimodo and Zephyr, and sees them spend the afternoon playing together. Eventually, the exhausted youngster falls asleep in Quasis arms. Realizing that Quasimodo possesses a kind and gentle nature, Madellaine ceases to be frightened by his  hideousness and ugliness. Quasimodo takes her around Paris, and shows her numerous sights.

Later, while Quasimodo is out with Madellaine, Sarousch and two of his subordinates sneak into the cathedral. Zephyr and Djali the goat follow them and watch as Sarousch causes La Fidèle to vanish. The gargoyles, who had tried to drop a bell on the thieves, end up trapped under it; Laverne rams one of its sides, causing the bell to clang loudly. Hearing the sound, Quasimodo and Madellaine rush back. When the now-skinny Archdeacon (skinnier than in the first film) informs everyone that La Fidèle has been stolen, Clopin claims that if they do not find the bell, the festival will be ruined. Phoebus suddenly realizes that Sarousch was behind the whole thing and played him for a fool. He sends the soldiers all over Paris to find Sarousch. Realizing now that Madellaine has deceived him (despite her pleas that she didnt intend to), Quasimodo angrily breaks off with her and tells Phoebus that he was right before running off into the cathedral, feeling heartbroken and betrayed. Feeling sorry for Quasimodo, Phoebus has the guards arrest Madellaine for her involvement in the theft.

At the festival, Hugo finally wins the heart of his longtime crush: Esmeraldas pet goat Djali, after years of pestering him. A number of romantic couples proclaim their equally strong and pure romantic feelings and love for each other while Quasi rings the bell, but then the bell falls silent when Madellaine (who has now been dropped from all charges for her involvement of the theft) shows up in the bell tower. Having forgiven her, Quasimodo proclaims his deep and true romantic love feelings for Madellaine and they share their first romantic kiss while Zephyr rings La Fidèle.

==Voice cast==
* Tom Hulce as Quasimodo 
* Jennifer Love Hewitt as Madellaine
* Michael McKean as Sarousch Esmeralda
* Kevin Kline as Captain Phoebus
* Haley Joel Osment as Zephyr Clopin 
* Charles Kimbrough as Victor
* Jason Alexander as Hugo
* Jane Withers as Laverne
* Jim Cummings as the Archdeacon
* April Winchell as Lady DeBurne
* Joe Lala as Guard 1

==Reception==
The Hunchback of Notre Dame II received mixed to negative reviews from film critics. The film currently bears a 30% on review aggregator Rotten Tomatoes, with an average rating of 3.6/10.  Critics panned the film for the poor quality of the animation in comparison to that of the original, the lighter tone, the original songs, as well as the weaker villain in Sarousch as opposed to Frollo.

DVD active said it was an "unusually chintzy production", noting "the characters are slightly off-model, their movements are stilted, optical zooms are used in place of animated camera moves, animation cycles are over-used, and painted highlights float around between frames". It compared it to the companys television shows, adding it looks "cheap", "old", and "awful". It concluded by saying "it is mercifully short – under an hour without credits."  Hi-Def Digest said "Theres really no point in wasting your time watching this subpar sequel of an already ho-hum movie", rating it 1.5 stars.  PopMatters notes "The Hunchback of Notre Dame II both addresses and cheapens the previous movie’s notes of melancholy, as it sets about finding Quasimodo a romantic partner".  DVD Talk says "the story...somehow stretches what might have once been a 12-minute segment of the Smurfs to over an hour", and concludes that "the whole thing has the awful feel of a cash grab". 

==Songs==
#"Le Jour DAmour" - written by Randy Petersen and Kevin Quinn; performed by Jason Alexander, Tom Hulce, Paul Kandel, Charles Kimbrough & Jane Withers 
#"An Ordinary Miracle" - written by Walter Edgar Kennon; performed by Tom Hulce 
#"Id Stick With You" - written by Walter Edgar Kennon; performed by Tom Hulce & Haley Joel Osment 
#"Fa-la-la-la Fallen In Love" - written by Walter Edgar Kennon; performed by Jason Alexander, Charles Kimbrough & Mary Jay Clough 
#"Im Gonna Love You (Madellaines Love Song)" - written by Jennifer Love Hewitt and Chris Canute; performed by Jennifer Love Hewitt

==References==
 

==External links==
 
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 