Plot for Peace
{{Infobox film
| name           = Plot For Peace
| image          =  
| image_size     =
| border         =
| alt            =
| caption        = 
| director       = Carlos Agulló Mandy Jacobson
| producer       = Mandy Jacobson Executive: Ivor Ichikowitz
| music          = Antony Partos
| cinematography = Rita Noriega Diego Ollivier
| editing        = Carlos Agulló
| studio         = 
| distributor    = Rezo Films (France) Caramel Films (Spain) Trinity Film (UK) Annie Planet (Japan) Indelible Media (South Africa)
| released       =  
| runtime        = 84 minutes 
| country        = South Africa
| language       = English, French, Portuguese, Afrikaans & Spanish
| budget         = 
| gross          = 
}}
Plot For Peace is a 2013 South African documentary directed by Carlos Agulló and Mandy Jacobson. 

The film tells the story of Algerian-born French businessman Jean-Yves Olliviers involvement in Cold War-era African parallel diplomacy, the signing of the 1988 Brazzaville Protocol and discussions surrounding the eventual release of Nelson Mandela. Using archive footage  from apartheid-era South Africa alongside interviews from Winnie Mandela, Thabo Mbeki, Denis Sassou-Nguesso and Mathews Phosa, Ollivier (previously unknown and referred to as Monsieur Jacques)  is revealed as a key architect of the withdrawal of Cuban troops from Angola and a 1987 prisoner-exchange programme involving six African nations. 

==Plot==
In the middle of the 80, Jean-Yves Ollivier, a French businessman working in Southern Africa, decides to use his network and the trust he inspires to the leaders of the countries, in order to help bringing peace and destroy the apartheid regime. 

The movie is showing the extraordinary life of the one who was called "Monsieur Jacques" (Mister Jacques) by secret services. It describes his action in parallel diplomacy based on prisoners exchanges, especially during the Brazaville accord, considered as the beginning of Southern Africas pacification.

== Awards ==
{| class="wikitable"
|-
! Festival !! Country !! Award
|-
|   || Argentina || First Mention in the International Feature Official Competition 
|-
|   || France || Jury Award for Best Documentary 
|-
|   || USA || Special Jury Award 
|-
|   || Brazil || Jury Award for Best Documentary 
|-
|   || Brazil || Audience Award for Best Documentary 
|-
|   || USA || Conflict and Resolution Award 
|-
|   || Ireland || Best International Feature Documentary 
|}

== External links ==
*  
*  

== References ==
 

 
 
 
 
 