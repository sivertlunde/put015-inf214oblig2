Dear Octopus (film)
 
{{Infobox film
| name           = Dear Octopus
| caption        = 
| image          = "Dear_Octopus"_(1943_film).jpg
| director       = Harold French Edward Black
| writer         = Patrick Kirwan R.J. Minney   Esther McCracken (adaptation)
| based on       = the play by Dodie Smith
| narrator       =  Michael Wilding  Celia Johnson
| music          = Hubert Bath
| cinematography = Arthur Crabtree
| editing        = Michael C. Chorlton
| studio         = Gainsborough Pictures
| distributor    = General Film Distributors (UK)
| released       = 20 September 1943 (UK)
| runtime        = 86 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}
 Michael Wilding Dear Octopus written by Dodie Smith. It was also released as The Randolph Family.

==Plot==
Well-to-do couple Dora and Charles Randolph are celebrating their golden wedding, and three generations meet at the Randolph country home. As the relatives gather, each reveals his or her personal quirks and shortcomings. Caught in the middle is family secretary Penny Fenton (Margaret Lockwood), who has the unenviable task of sorting and smoothing out the familys deep-set hostilities and jealousies so that a good time can be had by all.  

==Cast==
* Margaret Lockwood – Penny Randolph Michael Wilding – Nicholas Randolph
* Celia Johnson – Cynthia
* Roland Culver – Felix Martin
* Helen Haye – Dora Randolph
* Athene Seyler – Aunt Belle
* Jean Cadell – Vicars wife
* Basil Radford – Kenneth
* Frederick Leister – Charles Randolph
* Nora Swinburne – Edna
* Antoinette Cellier – Hilda
* Madge Compton – Marjorie
* Kathleen Harrison – Mrs Glossop
* Ann Stephens – Scrap
* Derek Lansiaux – Bill
* Alistair Stewart – Joe
* Evelyn Hall – Gertrude
* Muriel George – Cook
* Annie Esmond – Nannie
* Irene Handl – Flora
* Arthur Denton – Mr Glossop
* Pamela Western – Deirdre
* Arty Ash – Burton
* Graham Moffatt – Fred the Chauffeur
* Henry Morrell – Vicar

==Critical reception==
TV Guide described the film as a "routine English comedy of manners", but added, "it has its moments";  while Allmovie wrote "the film is variations on a single theme, albeit consistently amusing ones."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 