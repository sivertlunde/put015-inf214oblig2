The Glamorous Life of Sachiko Hanai
{{Infobox film
| name           = The Glamorous Life of Sachiko Hanai
| image          = The Glamorous Life Of Sachiko Hanai Poster.jpg
| caption        = Movie Poster
| director       = Mitsuru Meike
| producer       = Nakato Kinukawa Kyoichi Masuko Kazuto Morita
| writer         = Takao Nakano
| narrator       =  
| starring       = Emi Kuroda Takeshi Ito Yukijiro Hotaru Tetsuaki Matsue Kyoko Hayami
| music          = Taro Kataoka Taro Kishioka
| cinematography = Hiroshi Ito
| editing        = Naoki Kaneko
| distributor    = Shintōhō Palm Pictures (USA)
| released       = October 14, 2003
| runtime        = 90 minutes
| country        =  English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
  (2003 in film|2003) began life as a pink film, the durable Japanese soft-core genre, with the title   but it developed into a cult hit and the producers allowed director Mitsuru Meike to expand it into its present form. In 2007 the film was featured at the Santa Barbara International Film Festival. 

==Plot==
Sachiko Hanai (  and the other from the Middle East, who turn out to be spies in the middle of a transaction. When the argument escalates to gun-play, Sachiko foolishly starts to take a cell-phone picture of the incident and is shot in the forehead. Rather than killing her, the bullet lodges in her brain and gives her extraordinary mental powers, including the ability to understand languages of which she previously had no knowledge, arcane philosophical insight, advanced mathematical knowledge, and Extrasensory perception|ESP. After fleeing from the scene, she finds a metal cylinder in her pocket which contains a cloned copy of the finger of United States President George W. Bush.

While waiting in Sachikos house to reclaim the finger, the North Korean falls in love with her. Sachiko befriends and makes love to a philosophy professor and the professor hires her as his sons tutor. The professors wife becomes suspicious and goes to a detective, who happens to be the man who shot Sachiko. Because Bushs fingerprint is capable of unleashing a nuclear holocaust, the North Korean wants the cylinder back. In the end he takes Sachiko and she uses her powers to direct them to a cave where they find a machine that can decide the fate of the world. 

  has the American distribution rights and has promoted the film with their own trailers while   retains the original rights for Japan

==Further sources==
*  
*  
*  
*  
*  
* 
*  
*  
*  
*  

==References==
 

==External links==
* 
* 
* 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 