Chelsea Walls
{{Infobox film
| name = Chelsea Walls
| image = Chelsea Walls FilmPoster.jpeg
| alt = 
| caption = Theatrical release poster
| director = Ethan Hawke
| producer = Alexis Alexanian Pamela Koffler Christine Vachon Gary Winick
| writer = Nicole Burdette
| starring = Kris Kristofferson Uma Thurman Robert Sean Leonard Vincent DOnofrio Natasha Richardson Rosario Dawson
| music = Jeff Tweedy Tom Richmond
| editing = Adriana Pacheco IFC Films Lionsgate
| released =  
| runtime = 109 minutes    
| country = United States
| language = English
| budget = $100,000 
| gross = $60,902 
}} Chelsea Hotel in New York City.

==Plot== Chelsea Hotel, struggling with their arts and personal lives.

==Cast==
* Kris Kristofferson as Bud
* Uma Thurman as Grace
* Robert Sean Leonard as Terry Olsen
* Vincent DOnofrio as Frank
* Natasha Richardson as Mary
* Rosario Dawson as Audrey Mark Webber as Val
* Frank Whaley as Lynny Barnum
* Kevin Corrigan as Crutches Guillermo Díaz as Kid
* Bianca Hunter as Lorna Doone
* Matthew Del Negro as Rookie cop
* Paz de la Huerta as Girl
* Paul Failla as Cop
* Duane McLaughlin as Wall
* Jimmy Scott as Skinny Bones
* John Seitz as Dean
* Mark Strand as Journalist
* Heather Watts as Ballerina
* Tuesday Weld as Greta
* Harris Yulin as Buds editor
* Steve Zahn as Ross
* Sam Connelly, Richard Linklater, and Peter Salett as Cronies

==Reception==
Chelsea Walls received negative reviews, currently holding a 26% rating on Rotten Tomatoes. 
Roger Ebert gave the film three stars out of four, claiming that "Movies like this do not grab you by the throat. You have to be receptive. The first time I saw "Chelsea Walls," in a stuffy room late at night at Cannes 2001, I found it slow and pointless. This time, I saw it earlier in the day, fueled by coffee, and I understood that the movie is not about what the characters do, but about what they are. It may be a waste of time to spend your life drinking, fornicating, posing as a genius and living off your friends, but if youve got the money, honey, take off the time."  

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 