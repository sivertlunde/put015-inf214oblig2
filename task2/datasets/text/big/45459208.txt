Kalyana Vaibhogam
{{Infobox film
| name           = Kalyana Vaibhogam
| image          = Kalyana Vaibhogam DVD cover.jpg
| image_size     =
| caption        = DVD cover
| director       = N. Rathnam
| producer       = K.S.K. Sankara Subramaniam K.S.K. Arumugam K.S.K. Karthikeyan K.S.K. Kumaran
| writer         = N. Rathnam  (dialogues) 
| story          =
| screenplay     = N. Rathnam
| starring       =   Deva
| cinematography = B. S. Nandhalal
| editing        = Rajarajan&nbsp;— Kiton
| distributor    =
| studio         = Gomathi Shankar Films
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Tamil
}}
 1997 Tamil Tamil drama Deva and was released on 5 September 1997.    The film was based on Telugu film Aayanaki Iddaru and Hindi film Aaina (1993 film)|Aaina.

==Plot==

Ramya (Kushboo) and Shanthi (Sangita) are step-sisters. Ramya is arrogant while Shanthi is soft-spoken and sensitive. Sakthi (Ramki) is a writer and becomes popular by writing short stories in Tamil weekly magazines. Shanthi is Sakthis avid fan and sends him letters every week anonymously, she is in love with him. Sakthi really likes her letters, Sakthi decides to meet her and he is eager to confess his love.

Sakthi thinks that the anonymous fan is Ramya. Later, Sakthi and Ramya fall in love with each other. By luck, Ramya becomes a model, and thereafter her popularity grows rapidly. Sakthi and Ramya decide to get married whereas the heartbroken Shanthi remained silent. The day of the wedding, Ramya runs away with dreams of being a cinema actress and Ramya asks him to wait for him. Feeling betrayed, Sakthi cannot accept it and he finally gets married with Shanthi. They live happily until Ramya comes back. What transpires later forms the crux of the story.

==Cast==

*Ramki as Sakthi
*Kushboo as Ramya
*Sangita as Shanthi
*Vadivelu as Jacky
*R. Sundarrajan (director)|R. Sundarrajan as Ganesan
*V. K. Ramasamy (actor)|V. K. Ramasamy
*Venniradai Moorthy as Ramu
*Vijay Krishnaraj as Rajasekhar
*Haja Shareef
*Thiagarajan
*Kavitha as Kavitha
*Baby Srividya
*Baby Sowmya
*Baby Sridevi

==Soundtrack==

{{Infobox Album |  
| Name        = Kalyana Vaibhogam
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1997
| Recorded    = 1997 Feature film soundtrack
| Length      = 11:54
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1997, features 4 tracks with lyrics written by Pulamaipithan, Palani Bharathi, Nellai Arulmani, Ravi Bharathi and Navendan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Vizhioda Aadiye Azhaga || S. P. Balasubrahmanyam, K. S. Chithra || 2:16
|- 2 || Roja Poovile || Mano (singer)|Mano, Swarnalatha || 3:30
|- 3 || Hollywood Chance || Malgudi Subha || 1:57
|- 4 || Daada Budala || S. Janaki, Chorus || 4:11
|}

==References==
 

 
 
 
 
 