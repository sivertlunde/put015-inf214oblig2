My Name Is Bruce
{{Infobox film
| name           = My Name Is Bruce
| image          = My name is bruce.jpg
| caption        = Promotional film poster 
| director       = Bruce Campbell Mike Richardson
| writer         = Mark Verheiden
| starring       = Bruce Campbell Grace Thorsen Taylor Sharpe Ted Raimi James Peck Ellen Sandweiss Ben McCain
| music          = Joseph LoDuca Ben McCain Butch McCain
| cinematography = Kurt Rauf
| editing        = M. Scott Smith
| distributor    = Image Entertainment Dark Horse Entertainment
| runtime        = 86 minutes
| released       =  
| country        = United States
| language       = English
| budget         = $1.5 million
}}
My Name Is Bruce is a 2007 American comedy horror film, directed, co-produced by and starring B movie cult actor Bruce Campbell. The film was written by Mark Verheiden. It had a theatrical release in October 2008,  followed by DVD and Blu-ray releases on February 10, 2009.   
 Evil Dead series.  Ted Raimi (Sams brother), also a frequent collaborator, appears in this film.

Campbell has shown several minutes of the movie during some of his campus lectures, as well as a few public screenings including showings at the sixth annual Ashland Independent Film Festival, CineVegas and the eleventh annual East Lansing Film Festival. A trailer was released for the film as well and is available on various websites. A screening was held at the Alamo Drafthouse Cinema. Tickets for the show sold out in less than two minutes, breaking the previous Alamo ticket sellout record, which was also set by a Bruce Campbell appearance at the theater in 1998.

== Plot ==
In the mining town of Goldlick, Jeff (Taylor Sharpe), a young fan of B movie actor Bruce Campbell, and his friend Clayton (Logan Martin) go out to a cemetery to meet two girls, Big Debbie (Ariel Badenhop) and Little Debbie (Ali Akay). Jeff removes a medallion off the mausoleum, unleashing the Chinese god of the dead, Guan-Di (James Peck), who kills Clayton and the Debbies while Jeff flees.

Meanwhile, Bruce Campbell is finishing filming for the fictional Cave Alien II, and is promised a birthday surprise from his agent, Mills Toddner (Ted Raimi). Bruce meets Jeff, who kidnaps Campbell and takes him to Goldlick in hopes that his hero can save the town from Guan-Di. Upon arrival, Bruce assumes its his birthday surprise from Mills, and thinks its all a movie, despite a lack of cameras and a script, and agrees to "help". He learns about Guan-Di in the towns hall and during a dinner party, Bruce gets on the good side of Jeffs mother, Kelly (Grace Thorsen), who had initially been irritated by Bruces behavior.

After gearing up at Goldlicks gun shop, Bruce and many citizens of Goldlick go out into the woods to take on Guan-Di, which Bruce still thinks is part of a movie. Bruce then finds out that its all real and flees Goldlick, angering the townspeople, disappointing Kelly and upsetting Jeff. As part of a running gag, an Italian painter (also played by Ted Raimi) constantly repaints the population sign every time someone dies, including himself. Bruce returns to his caravan to find that everyone, including his own dog, hates him a lot. He has a restraining order placed upon him by his ex-wife, Cheryl (Ellen Sandweiss), and finds that his "surprise birthday present" from Mills was just a singing prostitute named Kasey (Janelle Farber). Bruce is then called by Jeff, who informs him that hes going to take on Guan-Di alone in spite of Bruces retreat.

Kasey takes Bruce back to Goldlick, where he is treated with contempt and tries to reconcile with Kelly. To rescue Jeff, they both drive to the old cemetery, in which they set up dynamite at the mausoleum and try to lure Guan-Di inside with one of Jeffs cardboard cut-outs of Bruce, which Guan-Di doesnt fall for. After kissing Kelly, Bruce decides to sacrifice himself (with bean curd playing a significant role in luring Guan-Di) and the dynamite is blown up. He emerges from the debris alive, and hangs the medallion back onto the mausoleum wall to ease the spirit. Guan-Di then also comes back to life, and at the very last minute, it turns out the whole scenario was a movie. Bruce argues with Ted Raimi about the cliched ending and turns it into a happy ending, which involves Bruce and Kelly married, living in a nice house with their son, Jeff, who is accepted into Harvard University. After the movie ends, Bruce asks "What could be a better ending than that?", after which Guan-Di appears and attacks Bruce.

== Cast ==
* Bruce Campbell as Himself
* Ted Raimi as Mills Toddner / Wing / Ted The Sign Painter
* Ben McCain as the Mayor
* Ellen Sandweiss as Cheryl Dan Hicks as Dirt Farmer
* Butch McCain as the Sheriff/Farmer
* Grace Thorsen as Kelly Graham
* Taylor Sharpe as Jeff
* Timothy Patrick Quill as Frank
* Logan Martin as Clayton
* Ali Akay as Little Debbie
* Ariel Badenhop as Big Debbie
* Janelle Farber as Kasey
* James Peck as Guan-Di / Cavealien Monster
* Jen Brown as Petra
* Mike Estes as Fan #2
* Adam Boyd as "Tiny"

==Release==
There was over a years gap between the films earliest screenings and its wider release in October 2008. Dark Horse Comics Mike Richardson commented on this:   
 "Some people maybe thought the film fell out or that there was something wrong with it, Richardson says, touching on Bruces slow journey getting before wide audiences. It was roughly a year ago that it screened to CineVegas film fest attendees. We did our shoot, put it in the can and the studio that financed it liked it so much they gave us more money to do a second shoot. We beefed it up so it could go into the theaters."  

For the week of November 12, 2008, My Name is Bruce took in $18,777 from its showing at the Sunshine Theater in New York. 

===Reception===
Reviews on My Name is Bruce were mixed, earning a Rotten Tomatoes approval rating of 39%, the consensus being "My Name Is Bruce succeeds or fails based entirely upon the viewers opinion of Bruce Campbell, an unreasonable burden for even the most accomplished actor.".

One positive review came from Nick Rogers of Suite101.com, saying "My Name is Bruce wont give you sugar, baby. Not on its budget. But Splenda works fine as a substitute for this Kool-Aid, which Campbell knows fans will happily drink. A little bit of purposefully lousy filmmaking winds up going a long way." One negative review came from Felix Vasquez Jr. of "Cinema Crazed", calling it "Smug, silly, and forgettable, this vanity project wants to be the next cult hit but really is just another vehicle for Bruce Campbell."

===Home media===
The film was released on DVD and Blu-ray Disc on February 10, 2009.  

==Sequel==
 

Richardson said that a sequel, titled My Name is Still Bruce, is in the works. Dark Horse Entertainment and Image Entertainment will distribute both films.  The title for the second film has since been changed to Bruce vs. Frankenstein. 
 
In a message sent in January, 2010 to Aint It Cool News, Campbell officially announced the sequel, stating that "principal photography begins this fall in Oregon."  In April, Ted Raimi confirmed that he would be involved with the project.  Campbell declared himself the director, saying, "no one will volunteer, so its me." 

==References==
 

==External links==
*  
*  
*  
*  
*  
*   UK Film and Bruce Information.

 
 
 

 
 
 
 
 
 
 