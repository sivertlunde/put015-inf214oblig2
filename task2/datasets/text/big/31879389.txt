The Shriek
{{Infobox Hollywood cartoon|
| cartoon_name = The Shriek
| series = Oswald the Lucky Rabbit
| image = Scene In The Shriek.jpg
| caption = Screenshot Bill Nolan Bill Nolan
| animator = Ray Abrams Fred Avery Cecil Surry Jack Carr Don Williams
| voice_actor =
| musician = James Dietrich
| producer = Walter Lantz
| distributor = Universal Pictures
| release_date = February 27, 1933
| color_process = Black and white
| runtime = 7:05 English
| The Plumber
| followed_by = Going to Blazes
}}
 The Sheik. {{cite web
|url=http://lantz.goldenagecartoons.com/1933.html
|title=The Walter Lantz Cartune Encyclopedia: 1933
|accessdate=2011-06-03
|publisher=The Walter Lantz Cartune Encyclopedia
}} 

==Plot summary==
Oswald and the girl beagle are on a camel, riding through the Egyptian desert. One day, they are encountered by a wolf wearing a bandana and also riding a camel. The girl beagle is taken by the wolf and is brought into the latters hideout, an ancient Egyptian temple. Though the temples entrance is closed upon his arrival, Oswald is able to crawl under it.

In a chamber of the temple, the wolf is smooching the girl beagle. As Oswald knocks on the door, the wolf stops momentarilly to confront the rabbit. After opening the door, the wolf hurls a log at Oswald, only to miss. Oswald quickly dashes into the chamber, locking the wolf outside.

As the wolf struggles to reenter the room, Oswald pulls the bandages off a wooden mannequin, causing it to spin. He then uses it to drill a hole in one of the chambers walls to make their escape.

While they are going through the temples many corridors and are not seen by their tormentor, Oswald tells the girl beagle to hide in another chamber while he tries to looks for exits. While he searches, Oswald is spotted by the wolf but still manages to stay on the run.

At the chamber where she is hiding, the girl beagle notices three kinds of materials: blankets, wooden staffs, and metal urns with eerie faces. She then uses them to create a scary disguise. Oswald returns inside and is slightly trembled upon seeing what she came up with. When the girl beagle reveals herself, Oswald is relieved and at the same time inspired to use a similar strategy for their getaway.

The wolf is standing around, still trying to figure the whereabouts of his two would-be-victims. Just then, Oswald and the girl beagle, in their ghostly disguises, pass by. Not realizing it is them, the wolf asks the two for directions. They reply, saying they know nothing, and walk away. The disguises of Oswald and the girl beagle, however, fail to keep them covered for long as their backs are partially exposed. The wolf notices this and goes after them.

Oswald and the girl beagle begin running when the wolf learns their identities. On the way, they find a toppled stone pillar at the end of a steep path. They then roll it toward the wolf. The wolf tries to outrun the rolling column but in vain. After getting flatten, the wolf is inflated back to shape by a couple of mice using a bellows.

Still on the run, Oswald and the girl beagle climb up a post to get on a higher ledge. They drop a stone plank on a bulging rock which they will use as a teeter totter. As the wolf walks by and stands on the lowered end of the teeter-totter, Oswald and the girl beagle drop a boulder on the other end. The wolf is thrown in the air and is last seen falling into the mouth of a sphinx. The film ends with Oswald and the girl beagle kissing each other.

==See also==
* Oswald the Lucky Rabbit filmography

==References==
 

==External links==
*   at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 
 