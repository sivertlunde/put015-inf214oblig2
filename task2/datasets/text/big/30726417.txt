Sally Marshall Is Not an Alien
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Sally Marshall is Not an Alien
| image          = 
| image size     =
| caption        = 
| director       = 
| producer       =
| writer         = 
| based on = 
| narrator       =
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Cinar Film Tonic
| distributor    = United International Pictures
| released       = 1999
| runtime        = 
| country        = Australia English
| budget         = 
| gross = A$1,291,802 (Australia) 
| preceded by    =
| followed by    =
}}
Sally Marshall Is Not an Alien is a 1999 family drama film starring Helen Neville, Natalie Vansier, and Thea Gumbert. It was released on July 1, 1999 in Australia.

== Plot ==
Twelve-year-old Pip Lawson is obsessed with watching outer space from her telescope. One day when shes watching from a nearby dock, local bully Rhonnie Bronston stops by with her dog. They get into a heated conversation in which Pip leaves with her telescope but forgets her walkman, which Rhonnie leaves with. When Pip returns home she discovers that a strange new family has moved into the house next door. Strange noises come from the shed, causing Pip to not be able to get to sleep. The next day Pip and her friend Ben Handlemen walk to the park were Rhonnie and her pose are. When Pip sees Rhonnie with her walkman she tries to take it back, but she threatens to let her dog on her. The children then notice the new girl "Sally Marshall" hanging upside down on the playground reading a book and eating a tomato the size of a grapefruit. Rhonnie starts talking about how she must be an alien, and all the kids except for Pip and Ben agree. Pip tells Rhonnie how stupid that is, and that aliens dont exist. Rhonnie then makes a bet with Pip that she has one week to prove that the Marshalls are not aliens or she will get her telescope, which Pip agrees to.

Pip begins spying on her new neighbors with her telescope, and taking pictures of the Marshalls around town. She tries with her telescope to look inside the shed to discover the cause of the strange noises but fails. She also sees Sallys teenage brother "Wayne Marshall" dance stripping in his room by himself with a biker helmet on his head that he never takes off while playing rock music from his stereo. The next day Pip walks by the Marshalls house and begins throwing tennis balls over the fence. After throwing several balls over the fence "Granny Marshall", who seems to be floating on air, gives Pip the ball back. Pip walks around to the front of the house then walks through the gate into the Marshalls back yard. Pip sees the huge tomatoes growing in bushes alongside the fence and takes one just as Sally comes outside. Before Sally talks to Pip she gives her dad a full glass of lemonade that he drinks in a matter of seconds. When Sally takes the glass back she says "Hi" to Pip, and they have a short conversation. Sally asks Pip if she would like to hang out but Pip says she cant, but maybe another time. Pip then leaves with the tomato in hand. Meanwhile Ben is at the playground because Pip is mad that he kissed her. He then gives Rhonnie the pictures she took in exchange for Pips walkman in hopes that she will forgive him.

The next day when Sally is at the playground the kids are all staring at her once again. When she gets down from the playground the all start chasing her. Pip who is there also follows Sally. When Sally gets away from the kids Pip is still following her. Sally sees Pip and assumes that the kids had been chasing her too. Sally says she knows a way to get away from the kids and tells Pip to follow her. The two then arrive in town were there is a tour boat about to leave. Pip suggests that they get on and they run towards it. Pip easily jumps from the dock on to the boat, but Sally gets scared. Pip talks her threw it then they take their seats. A man in front of them asks Pip to take a picture of him in his girlfriend with his camera. When shes done he says that he will take a picture of Pip and Sally with her camera which she lets him do. When they get off the boat the girls have a trip around town in which doing so they become fast friends. When Pip is buying her and Sally some ice cream Rhonnie drops the pictures Pip took of the Marshalls are on a mission to stop a fat person invading earth on to her table. When Pip returns Sally angrily confronts her about the pictures. Sally then leaves crying. Rhonnie comes out and makes fun of Pip. Pip asks Rhonnie how she got the pictures, and she tells Pip about Ben. Pip yells at Ben then realizes that he has her walkman. She takes it from him believing that he was going to keep it for himself. She then storms off home.

That night Pip who is keen on spying again sees Ben inside the Marshalls shed motioning for her to come down. She does and asks him why hes in there. He tells her that he climbed through the window, so he could help her win the bet. He also tells her that the strange sounds she heard were from "Mr. Marshalls" welding tools. Pip who is so happy forgives Ben and tries to pick the shed lock so Ben can get out. When shes trying to pick the lock all the Marshalls come outside and catch them. Mr.Marshall comes right down to the shed and unlocks the door and Ben comes out. The two kids, speechless, leave intermediately. The next day when Pip sees Sally she apologizes for everything and tells her about the bet. Sally who is touched that Pip would bet her beautiful telescope for her forgives her. Then Pip, Sally and, Ben begin to hang out together. Rhonnie who is made with Pips discovery rallies together all the kids saying that they need to get rid of the Marshalls, and that they will start with Sally. Pip leaves Sally for a second so she can go to the bathroom. Sally, who is sitting by herself sees all the children coming after her and runs to the end of the dock. The kids corner her so she climbs over the rail and jumps into the water. Pip who realized what was happening ran after the kids and jump into the water. Pip brings Sally back to shore and they are greeted by Ben. Pip and Ben then help a freezing Sally back home. Granny Marshall begins tending to Sally and asks the two to leave her alone for a bit so she can rest.

The next day is the end of the bet and Pip and Rhonnie met on the beach with all the children. Pip says that because of the stupid bet Sally almost died, and that she regrets making the bet. She says the Sally Marshall is not an alien but she will quit the bet anyways because she feels so bad about what happened to her. Rhonnie delighted with winning keeps telling Pip that she was right. Pip says that shes not and that she only won because she dropped out. Sally then comes on to the beach and apologizes to Pip about her telescope. Pip tells her its alright, and I know your not an alien. Then out of nowhere a UFO comes flying and hovers right above the beach. Sally begins crying and tells Pip that she is an alien, and if she doesnt leave now her whole family could die. Pip who is also crying tells her that shes her best friend, and so does Sally. Sally then leaves in the UFO forever. Pip leaves the beach and returns home. Pips dad comes up to her room and comforts her about Sally. He also gives her, her telescope which he says some kids dropped it off.

Pip goes outside and finds Rhonnie and Ben. She thanks Rhonnie for returning her telescope. Rhonnie,Pip, and Ben start talking and slowly begin to become friends. Rhonnie then begins playing fetch with her dog and he finally runs to the stick and brings it back to her. The movie ends with Pip, Rhonnie, and Ben celebrating about the accomplishment.

== Cast and crew ==

=== Cast ===
* Helen Neville as Pip Lawson
* Natalie Vansier as Sally Marshall
* Thea Gumbert as Rhonnie Bronston
* Glenn McMillian as Ben Handleman
* Lachlan Hodgson as Nick Jessop

=== Supporting cast ===
* Vince Poletto as Wayne Marshall
* Melissa Jafer as Granny Marshall
* Mac Teedie as Mr.Marshall Peter OBrien as David Lawson Paul Gordon 

=== Crew ===
Directed by Mario Andreacchio

Produced by:
*Micheline Charest   Producer
*Terry J. Charatsis  Producer
*Antony I. Ginnane   Executive Producer
*Ronald A. Weinberg  Executive Producer
*Jane Ballantyne     Co Producer
*Patricia Lavoie     Co Producer

Written by:
*Amanda McKay        Novel
*Robert Geoffrion    Writer

==References==
 

== External links ==
* Sally Marshall Is Not an Alien at the Internet Movie Database
* http://movies.nytimes.com/movie/180645/Sally-Marshall-Is-Not-an-Alien/overview
* http://www.film.com/movies/sally-marshall-not-an-alien/14705023
 

 
 