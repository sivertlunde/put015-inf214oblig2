In the Bag
{{Infobox film
|name=In the Bag
|image=
|image_size=
|caption=
|director=Jack Hannah
|producer=Walt Disney	
|writer=David Detiege
         Al Bertino 
|Animation=George Krisel
           John Sibley
           Al Coe
           Bob Carlson
|starring=Humphrey the Bear
|music=George Bruns
|cinematography=
|editing=
|distributor=
|released=27 July 1956
|runtime=7 min
|country=United States 
|language=English
|budget=
}}
In the Bag is a 1956 Walt Disneys animated theatrical short directed by Jack Hannah and featuring park ranger J. Audubon Woodlore and his comedic foil Humphrey the Bear.

The short can be found on disc 2 of  . This was the last Disney theatrical cartoon short subject distributed by RKO Radio Pictures. 

==Plot==
Tourists have departed the national park where Humphrey lives, leaving trash everywhere. The lazy park ranger gets the resident bears to "put it in the bag", by singing and dancing to a lively song. The bears happily collect the trash, bouncing and jouncing until they discover the rangers trick. Angrily they dump their bags of refuse on the ground. The ranger prepares some chicken cacciatore, but will not give it to the bears unless they clean up their sections of the park. All of the bears move their garbage into Humphreys section. In order to get his dinner, Humphrey has to make it fast by stuffing the garbage into a bag, but it gets caught by a bush and rips apart, letting the garbage out. Then he receives a new bag and follows the garbage down the line all the way to the cliff, but falls off. Next, he attempts to conceal the garbage under a bush, but a rabbit pushes it out in disgust. Next, he tries to burn it with a match, only to be stopped by Smokey Bear! (The scene with Smokey was edited at one time, but it has been reinstated in later showings.) Finally, Humphrey puts all of the trash into a hollow stump, which is actually a geyser named "Ol Fateful". The ranger prepares to reward Humphrey with a whole dish full of cacciatore but before Humphrey can take it, the geyser suddenly erupts, spouting garbage everywhere, resulting in Humphrey having to start all over at cleaning up the park.

==In the Bag song==
The catchy tune featured in In The Bag was so popular that Disney released a version of it (with similar instrumentation and different vocals) as a single, "The Humphrey Hop." 
The song was sung again by Woodlore to his Brownstone Bears in a 2002 episode of Disneys House of Mouse, where they serve as the Houses "clean-up crew."

==See also== Grin and Bear It
*Hooked Bear
*Bearly Asleep
*Rugged Bear

==References==
 
 

==External links==
* 

 
 
 
 
 
 
 