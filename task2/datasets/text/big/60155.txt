She Done Him Wrong
{{Infobox film
| name           = She Done Him Wrong
| image          = She-done-him-wrong.jpg
| image_size     = 
| caption        =  James Dugan (assistant)
| producer       = William LeBaron
| based on       =   John Bright
| narrator       = 
| starring       =  
| music          = John Leipold (uncredited)
| cinematography = Charles Lang
| editing        = Alexander Hall
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         = $200,000   
| gross          = $2.2 million 
}}
 comedy romance romance starring musical elements. The supporting cast features Owen Moore, Gilbert Roland, Noah Beery, Sr., Rochelle Hudson and Louise Beavers.
 directed by produced by script was John Bright Broadway Play play Diamond Diamond Lil by Mae West. Original music was composed by Ralph Rainger, John Leipold and Stephan Pasternacki. Charles Lang was responsible for the cinematography, while the costumes were designed by Edith Head.

The movie is famous for Wests many double entendres and quips, including her seductive, "I always did like a man in a uniform. That one fits you grand. Why dont you come up sometime and see me? Im home every evening." 

Blonde Venus, starring Marlene Dietrich and Cary Grant, predates She Done Him Wrong by a year even though Mae West always claimed to have discovered Cary Grant for her film, elaborating that up until then Grant had only made "some tests with starlets."

==Plot== New York in the Gay Nineties|1890s. A bawdy singer, Lady Lou (Mae West), works in the Bowery barroom saloon of her boss and benefactor, Gus Jordan (Noah Beery), who has given her many diamonds. But Lou is a lady with more men friends than anyone might imagine.
 San Francisco David Landau), spends most of the movie dropping hints to Lou that Gus is up to no good, promising to look after her once Gus is in jail. Lou leads him on, hinting at times that she will return to him, but eventually he loses patience and implies hell see her jailed if she doesnt submit to him.
 Federal agent working to infiltrate and expose the illegal activities in the bar. Gus suspects nothing; he worries only that Cummings will reform his bar and scare away his customers.

Lous former boyfriend, Chick Clark (Owen Moore), is a vicious criminal who was convicted of robbery and sent to prison for trying to steal diamonds for her. In his absence, she becomes attracted to the handsome young psalm-singing reformer.

Warned that Chick thinks shes betrayed him, she goes to the prison to try to reassure him. All the inmates greet her warmly and familiarly  as she walks down the cellblock. Chick becomes angry and threatens to kill her if she double-crosses or two-times him before he gets out. She lies and claims she has been true to him.

Gus gives counterfeit money to Rita and Sergei to spend. Chick escapes from jail, and police search for him in the bar. He comes into Lous room and starts to strangle her, breaking off only because he still loves her and cannot harm her. Lou calms him down by promising that she will go with him when she finishes her next number.
 Frankie and Johnny", she silently signals to Dan Flynn that he should go to her room to wait for her, even though she knows Chick is in there with a gun. Chick shoots Dan dead and the gunfire draws a police raid. Cummings shows his badge and reveals himself as "The Hawk," a well-known Federal agent, as he arrests Gus and Sergei. Chick, still lurking in Lous room, is about to kill Lou for double-crossing him, when Cummings also apprehends him. 

Cummings then takes Lou away in an open horse-drawn carriage instead of the paddywagon into which all the other criminals have been loaded. He tells her she doesnt belong in jail and removes all her other rings and slips a diamond engagement ring onto her marriage finger.

==Reception==
 
The film was a box-office success, grossing $2 000 000 domestically with a budget of $200 000.

Variety (magazine)|Varietys "Bige" gave She Done Him Wrong a negative review stating that Paramount was attempting to rush Mae West to stardom by giving her her own film and top billing, and that the film was not very good without known actors and an entertaining story. 
 Academy Award Outstanding Production, now known as Best Picture. At 66 minutes, it is the shortest film ever to be so honored.

In 1996, She Done Him Wrong was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

American Film Institute recognition
* 2000: AFIs 100 Years... 100 Laughs #75
* 2005: AFIs 100 Years... 100 Movie Quotes #26
**"Why dont you come up sometime and see me?"

==Notes==
Though Mae Wests famous line to Cary Grant is "Why dont you come up some time and see me?" in She Done Him Wrong, she changed it to "Come up and see me sometime" in her next movie, Im No Angel, which was released the same year and also co-starred Grant.
 The Bowery, a Raoul Walsh film released later the same year by United Artists starring Noah Beerys brother Wallace Beery in a similar role.  Also, the part Wallace Beery played in The Bowery, a saloon owner named Chuck Connors, appears in She Done Him Wrong as a small role and is played by a different actor.

Mae West loved to take credit for "discovering" Cary Grant for She Done Him Wrong but Grant had already made seven movies, including playing Marlene Dietrichs leading man in Blonde Venus the previous year. Louise Beavers was the only African American actress to be brought aboard the film by Mae West personally. She wanted a black woman to appear opposite her; when she did stage and screen work, West made it a point to act with Black American actors and actresses, helping to break racial discrimination in entertainment. Wests stage shows resulted in her arrest for saucy material and her having black actors on stage was extremely controversial. With this film, she and her Paramount bosses called the shots: black stars appeared in a few of her films after this one.

==Quotes==
"I always did like a man in a uniform. That one fits you grand. Why dont you come up sometime and see me? Im home every evening."

==Cast==
 
* Mae West as Lady Lou
* Cary Grant as Capt. Cummings
* Owen Moore as Chick Clark
* Gilbert Roland as Sergei Stanieff
* Noah Beery, Sr. as Gus Jordan David Landau as Dan Flynn
* Rafaela Ottiano as Russian Rita
* Dewey Robinson as Spider Kane
* Rochelle Hudson as Sally
* Tammany Young as Chuck Connors
* Fuzzy Knight as Ragtime Kelly
* Grace La Rue as Frances Kelly
* Robert Homans as Doheney
* Louise Beavers as Pearl (Lous maid)
 
All cast members are deceased.

==Notes==
 

==External links==
 
 
*  
*  
*  
*  
*  
*   at Virtual History

 

 
 
 
 
 
 
 
 
 
 