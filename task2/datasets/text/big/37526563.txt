Children of the Sun (2007 film)
{{Infobox film
| name           = Children of the Sun
| image          = Children of the Sun Poster.jpg
| caption        = Film poster
| director       = Ran Tal
| producer       = Amir Harel Ayelet Kait Ran Tal
| writer         = Ron Goldman, Ran Tal
| starring       = 
| music          = Avi Belleli
| cinematography = 
| editing        = Ron Goldman
| distributor    = Fortissimo Films, Maximum Film Distribution (Canada)
| released       =  
| runtime        =  70 minutes
| rating         =
| country        = Israel
| language       = Hebrew
| budget         = 
}}
Children of the Sun ( ) is a 2007 documentary feature film about the Israeli kibbutz directed by Ran Tal.  It won the Best Documentary and Best Editing Awards at the 2007 Jerusalem Film Festival, and Best Documentary at the 2008 Ophir Awards.
 communal child rearing and education.  The film combines archival footage culled from over eighty amateur films shot between 1930 and 1970, rare recordings, and interviews with more than a dozen people who reflect on their unconventional childhoods and being the unwitting subjects in an ambitious social and ideological experiment. 

==References==
 

==External links==
*  

 
 
 
 

 