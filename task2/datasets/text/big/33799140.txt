Beautiful (2011 film)
{{Infobox film
| name = Beautiful
| image = Beautiful malayalam film poster.jpg
| caption = Theatrical Poster
| director = V. K. Prakash
| writer = Anoop Menon
| producer = Anand Kumar
| starring = Jayasurya Anoop Menon Meghana Raj
| music = Ratheesh Vegha
| editing = Mahesh Narayanan
| cinematography = Jomon T. John
| studio = Yes Cinemas
| distributor = Yes Cinema Company  SRT Films   Dhanush Film Release
| released =  
| runtime = 108 minutes
| country = India
| language = Malayalam
| budget =  1 crore
| gross =  4.5crores
}}
 Malayalam Musical musical Drama drama thriller film written by Anoop Menon and directed by V. K. Prakash. The film stars Jayasurya, Anoop Menon and Meghana Raj in the lead roles. The cinematography was by Jomon T. John and the music was composed by Ratheesh Vegha. It tells the story about the intense bonding of two friends, one quadriplegic and other a musician. The film released on 2 December 2011 to predominantly positive reviews and was a commercial success. 

==Plot==
Stephen Louis (Jayasurya) is a quadriplegic by birth who is immensely rich, but he believes in celebrating each moment of life. He always loves to see beautiful things and speaks about celebrating life even though he cant move any part of his body other than his neck. He is always assisted by his Manager Kamalu (Nandu) and his caretaker/driver Karunan (Jayan) in a palatial house in Fort Kochi. As most of his relatives look to take advantage of his position, Louis has kept all of them at bay, barring his cousin brother Alex (Tini Tom) with whom he shares a good relationship.

John (Anoop Menon), a musician at a hotel, enters his life. John is completely opposite from Stephens nature. He is always worried about the future. Though poles apart in their attitudes, both enjoy a good friendship and try to make each other happy. But with the arrival of a beautiful home nurse named Anjali (Meghana Raj), the attitudes of both friends towards their lives turn around. The movie takes unexpected twists and turns and Anjali finally turns out to be Alexs secret wife. Her real name is Annie. She had come to Louiss residence undercover to poison him and to steal him off his wealth, with Alex, who had taken advantage of Lousiss openness with him, in on the vicious ploy. Unfortunately, John, who last visited Louis before his poisoning, was blamed. But after the police take John away,  Alexs maid recognizes him and Anjali at the hospital, in which Louis was dying of poisoning. The police let go off John and arrest Alex and Anjali. Surprisingly, on a good note, Louis survives and the friends live together happily.

==Cast==
* Jayasurya as Stephen Louis, a quadriplegic.
* Anoop Menon as John, a musician.
* Meghana Raj as Anjali/Annie, Alexs secret wife, living incognito in Stephens residence as a home nurse.
*Nandhu as Kamalu
* Jayan as Karunan, the driver.
* Tini Tom as Alex
* Unni Menon as Peter
* Aparna Nair as Meera
* Praveena as Doctor
* Thesni Khan as Kanyaka
* Kishore Kumar as Biju, Alexs friend
* Kochu Preman as Alexs uncle
* Ponnamma Babu as Alexs aunt
* Joju George
* Ammu Venugopal as Johns sister
* Lishoy as Stephens father
* Master Dhananjay as Stephen (as childhood)

==Production==
Anoop Menon says the film was inspired by a real life incident, the persisting memory of his quadriplegic childhood friend.    The film started its shooting in Kochi on September 2011. Other main Location was in Munnar.The film was first titled as "Ladies and Gentlemen" and later changed to "Beautiful". It has been produced by Anand Kumar under the banner of Yes Cinema.   

Moral anarchy is the religion of the films characters as they come out with volatile statements regarding marriage and loyalty. The casual reference to marital infidelity mouthed by Praveenas character in the film has irked many, but Anoop insists that he was just being realistic while penning the lines. "Even the films that come under the realistic tag hardly handle such subjects in honest and transparent manner. We camouflage the reality with flashy elements so that you don’t have to confront it openly. All my women characters are bold and I believe today’s women are daring enough to discuss such stuff," he says. 

Beautiful was a turning point in Meghana Rajs career. Although she herself was Prakashs first choice for the role, she was approached only four days before the shooting started. The actress says: "I was approached four days before the shoot. I absolutely fell in love with the bad girl role. I never dreamt that I could carry off the role and the entire credit goes to my director V.K Prakash. I remember coming for the shoot with makeup and groomed hair and being very politely told to remove my makeup and oil my hair by VKP. I was asked to bring all my bindis, out of which one was selected to be my only makeup in the film." Meghana admits that it was challenging to achieve the right amount of underplayed sensuality, which was part of the character of Anjali, who conveys a lot through her eyes and body language. 

Major portion of the movie was filmed from Bastion Bungalow in Fort Kochi, Kerala. 

==Release and reception==
The film was originally scheduled to release on November 18 but was pushed back to December 2 due to a strike in the Malayalam film industry.    It opened in 46 centres inside Kerala on 2 December. The film had its international premiere at the first Ladakh International Film Festival (LIFF) where it was one of the two Malayalam features to be screened. 

Paresh C. Palicha of Rediff.com rated the film 3.5/5 and stated: "V K Prakash-directed Beautiful can be considered a significant milestone in the revival of Malayalam cinema."   

Veeyen of Nowrunning.com gave the film 3/5 and said: "V K Prakashs Beautiful is a compelling evocation of the human desire dynamics at work." the site added that "Beautiful wouldnt call it a masterpiece, but it is without doubt, one of the most absorbing Malayalam films that I have seen this year." {{cite web url = http://www.nowrunning.com/movie/9547/malayalam/beautiful/3456/review.htm title = Beautiful Review publisher = Nowrunning.com date = 2011-12-03 accessdate = 2011-12-03}}
 

A critic from IndiaGlitz.com rated the film 3.25/5 and wrote: "On the whole, Beautiful has a fresh, honest and unpretentious air to it, which makes it appealing. Played with subtlety and intelligence by its stars, the movie is sure to end up in the lists of the bests of the year. Rightly advised for a must and decent watch."   

Sify.com gave a rating of 3.5/5 stating, "Beautiful has its own flaws and is far from perfect but needs to be appreciated for its courage to experiment. It could have been better and the jokes and the stance on morality may not find acceptance from all, but this one has sincerity written all over it. Now, the decision is all yours!"    Kerala9.com gave a (3/5) and commented, "Beautiful is a cool clean film. Malayalam has got a director who understands what subtlety is."   

==Accusation of plagiarism==
The movie was accused to be plagiarised from the 2011 French film  . Retrieved 5 January 2012.  The plot of Intouchables itself was inspired by a true story discovered by the directors in a 2004 documentary film. However the chances of plagiarism are feeble since Beautiful was already under production when the original film released. The original film released on 23 September 2011.  . (in French). Allocine.com. Retrieved 5 January 2012. 

==Soundtrack==
{{Infobox album
| Name = Beautiful
| Artist = Ratheesh Vegha
| Type = soundtrack
| Image =
| Cover =
| Released =  
| Recorded = 2011 Feature film soundtrack
| Length =
| Label = Manorama Music
| Music = Ratheesh Vegha
| Producer = Anand Kumar
}}

The films songs and background score were composed by  . Retrieved 9 December 2011. 

Beautiful was Anoop Menons debut work as a lyricist. Menon says his debut as a lyricist was quite accidental. "On the second day of the shooting of Beautiful, music director Ratheesh Vega, actor Jayasurya, and I were chilling out in Ratheeshs car, when he asked me to pen some lines to try out a tune hed composed for the film. The result is Mazhaneer Thullikal...," says the actor.

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Artist(s)
|-
| 1 || "Mazhaneer Thullikal" || Unni Menon
|-
| 2 || "Moovanthiyaay" || Vijay Yesudas
|-
| 3 || "Ninviral Thumbil" || Gayatri Asokan
|-
| 4 || "Raapoovinum" || Naveen Iyer, Balu Thankachan, Ajith, Thulasi Yatheendran
|-
| 5 || "Mazhaneer Thullikal" || Thulasi Yatheendran
|-
| 6 || "Raapoovinum (Movie Edit)" || Balu Thankachan, Pradeep Chandrakumar, Ajith, Thulasi Yatheendran
|}

==Awards==
;1st South Indian International Movie Awards  Best Lyricist - Anoop Menon for "Mazhaneerthullikal"

;Film Guidance Society of Kerala Film Awards
* Best Film
* Best Actor - Jayasurya
* Best Scriptwriter - Anoop Menon
* Best Music Director - Ratheesh Vegha
* Best Singer - Unni Menon

; Ramu Kariat Memorial Cultural Forum Awards
* Special Jury Award - Jayasurya

; Thikkurissi Foundation Awards (2012) 
* Second Best Actor - Jayasurya
* Best Script - Anoop Menon
* Best Cinematography - Jomon T. John

; Nana Film Awards
* Best Actor - Jayasurya

; Amrita TV Film Awards
* Entertainer of the Year - Jayasurya 

==References==
 
* http://cinemalablog.blogspot.com/2012/01/malayalam-movie-beautiful-inspired-from.html

==External links==
*  
*  
* Beautiful at  

 

 
 
 
 
 
 
 
 
 
 
 
 