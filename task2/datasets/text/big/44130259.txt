Uvvu
{{Infobox film 
| name           = Uvvu
| image          =
| caption        =
| director       = Ben Marcose
| producer       =
| writer         =
| screenplay     =
| starring       = Jalaja Sankar Mohan
| music          = MB Sreenivasan
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India
| language       = Malayalam Language|Malayalam}}
 1982 Cinema Indian Malayalam Malayalam film, directed by Ben Marcose. The film stars Jalaja and Sankar Mohan in lead roles. The film had musical score by MB Sreenivasan.   

==Cast==
*Jalaja
*Sankar Mohan

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by ONV Kurup. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ennu Ninne Kandu || K. J. Yesudas || ONV Kurup || 
|-
| 2 || Manjuthulliyude kunju kavililum || K. J. Yesudas, S Janaki || ONV Kurup || 
|-
| 3 || Nilaavu veenu mayangi || S Janaki || ONV Kurup || 
|-
| 4 || Thithithara poikakkarayilu || Chorus, CO Anto || ONV Kurup || 
|}

==References==
 

==External links==
*  

 
 
 

 