Periya Thambi
{{Infobox film
| name           = Periya Thambi
| image          = 
| image_size     =
| caption        = 
| director       = Chitra Lakshmanan
| producer       = Chitra Lakshmanan Chithra Ramu
| writer         = S. A. Abirama Moorthy  (dialogues) 
| screenplay     = Chitra Lakshmanan
| story          = Kesavan
| starring       =   Deva
| cinematography = Ilavarasu
| editing        = N. Chandran
| distributor    = Gayathri Films
| studio         = Gayathri Films
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Tamil
}}
 1997 Tamil Tamil drama Prabhu and Deva and was released on 14 January 1997.  

==Plot==

Siva (Prabhu (actor)|Prabhu) lives with his father Soundarapandian (Thalaivasal Vijay), a police officer, and his little sister Geetha. Geetha gets engaged with a bride and Siva is trouble to note that his father will not invite his family. Siva finds out his familys village and goes there. At his fathers village, Siva is insulted by his uncle Sankarapandian (Vijayakumar (actor)|Vijayakumar). While his aunt Kamachi (Vadivukkarasi) welcomes him. Soon, Kamachis daughter Selvi (Nagma) and Siva fall in love with each other. How did Siva reconcile his uncle and got married with Selvi is the rest of the story.

==Cast==
 Prabhu as Siva
*Nagma as Selvi
*Goundamani as Azhagu Sundaram Vijayakumar as Sankarapandian Chandrasekhar as Muthu
*Thalaivasal Vijay as Sundarapandian, Sivas father Uday Prakash as Thangarasu
*Ponvannan as Rathnam
*Vadivukkarasi as Kamachi, Malars mother Sangeeta as Sivagami
*R. V. Aswini as Meena, Sankarapandians daughter
*Suvarna Mathew as Kannamma
*Chitra Lakshmanan
*K. K. Soundar as Muthus father
*Kullamani
*Periya Karuppu Thevar
*Jaya Mani
*Mahanadi Dinesh

==Soundtrack==

{{Infobox Album |  
| Name        = Periya Thambi
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1997
| Recorded    = 1996 Feature film soundtrack |
| Length      = 25:13
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1997, features 5 tracks with lyrics written by Vairamuthu. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Aavaram Poovu || Krishnaraj || 5:22
|- 2 || Left Right || Swarnalatha || 4:13
|- 3 || Poovukku Oru Kalyanam || Malaysia Vasudevan, Mano (singer)|Mano, Swarnalatha || 5:17 
|- 4 || Taj Mahale || Hariharan (singer)|Hariharan, Anuradha Sriram || 5:12
|- 5 || Vellikizhamai || Swarnalatha || 5:09
|}

==References==
 

 
 
 
 
 