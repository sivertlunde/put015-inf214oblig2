Isabella (2006 film)
 
 
{{Infobox film
| name           = Isabella
| image          = Isabella2006poster.jpg
| caption        = Promotional poster
| film name = {{Film name| traditional    = 伊莎貝拉
| simplified     = 伊莎贝拉}}
| producer       = Pang Ho-Cheung Chapman To Jin Zhongqiang
| director       = Pang Ho-Cheung
| screenplay     = Pang Ho-Cheung Kearen Pang Derek Tsang Jimmy Wan
| story          = Pang Ho-Cheung Anthony Wong
| cinematography = Charlie Lam
| music          = Peter Kam
| editing        = Wenders Li Media Asia China Film Group Media Asia Distribution Ltd. (Hong Kong)
| released       =  
| runtime        = 91 minutes
| country        = Hong Kong Mandarin
| budget         = 
| gross          = $269,556 
}}
 2006 Cinema Hong Kong Anthony Wong. It played in competition at the 56th annual Berlin International Film Festival, where it won the Silver Bear for best film music (it was nominated for the Golden Bear as well). The film is set in Macau.

== Plot ==
  Portuguese handover of Macau, a crooked cop, Shin (To), meets the daughter he didnt know he had, Yan (Leong).

== Cast ==
  
*Chapman To
*Isabella Leong Anthony Wong
*Josie Ho
*Jim Chim Steven Cheung
*Shawn Yue
*Wan Yeung-ming

==Awards==
*Silver Bear, Film Music – 2006 Berlin International Film Festival 
*Golden Bear, Best Film – 2006 Berlin International Film Festival (Nominated) Best original film score of 26th Hong Kong Film Awards

==See also==
* List of Hong Kong films
* List of films set in Macau

== References ==
 
 

== External links ==
 
*  
 

 

 
 
 
 
 
 
 


 