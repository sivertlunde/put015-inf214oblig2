Bhoot Bhooturey Samuddurey
{{Infobox film
| name           = Bhoot Bhooturey Samuddurey
| image          = 
| alt            = 
| caption        = 
| director       = Swapan Ghoshal
| producer       = Swapan Ghoshal
| writer         = 
| starring       =
| music          = Priyo Chatterjee
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 
| runtime        = 
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}}
 Bengali childrens film directed by Swapan Ghoshal. The film is based on a short story by Prafulla Roy.

==Plot==
The story of the film is narrated by Paran Bandyopadhyay. The plot involves two siblings, Titli and Tabul, who goes to Digha in a family vacation. They find that Lalkamol babu, who occupied the next room, has disappeared without any trace. The two children, getting involved with this mystery, unexpectedly finds a group of ghosts.

==Cast==
*Megha Deb
*Sayan Sarkar 
*Bhaswar Chatterjee 
*Jayjit Banerjee 
*Santilal Mukherjee 
*Paran Bandopadhyay

==Reviews==
Sutapa Singha of The Times of India gave the film a 3 out of 5 stars. She asserts that the film "makes an impact because of the convincing performances". 

According to Shoma A. Chatterji of The Indian Express, the film has good actors, but its intention is wasted due to "badly-scripted characters". 

==References==
 

 
 
 

 