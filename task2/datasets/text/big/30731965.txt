An Angel Named Billy
{{Infobox film
| name           = An Angel Named Billy
| image          = An_Angle_Named_Billy.jpg
| caption        = Movie Cover
| director       = Greg Osborne
| producer       =  Kevin M. Glover,Greg Osborne,Eliezer J. Gregorio
| writer         = Eliezer J. Gregorio, Greg Osborne,
| starring       =  Brent Battles, Dustin Belt, Robin Dionne
| music          =
| cinematography = Dale Obert
| editing        =
| studio         = Ariztical Entertainment
| distributor    = 
| released       =  
| runtime        =120 minutes
| country        = United States
| language       = English
| budget         = $200,000 est
| gross          = 
}}

An Angel Named Billy  is a 2007 LGBT drama film starring Brent Battles, Dustin Belt and Robin Dionne. It was written  by Greg Osborne and Eliezer J. Gregorio, and produced by Kevin M. Glover and Eliezer J. Gregorio. The film was directed by Greg Osborne.   

==Plot==
Mark, a recent stroke victim, and his best friend, a drag queen named Thomas, embark on a quest to find a new partner for Mark’s son, James. Meanwhile Billy, a young man who has just turned 18, gets his very first kiss from his friend Rick. Unfortunately, he is caught and thrown out of his house by his abusive father. Lost, scared, and alone, Billy takes a job as the caretaker at a local café. Billy must learn to deal with the responsibility of being on his own and living as a homosexual male.

==Cast==
* Brent Battles as Todd
* Dustin Belt as Billy
* Robin Dionne as Rondell (as Robin Dionne Smith)
* Hank Fields as James
* Allison Fleming as Donna
* Buddy Daniels Friedman as Thomas
* Kadyr Gutierrez as Guy
* Molly Howe as Addy
* Amy Lyndon  as Nancy
* Toni Malone as Tina
* Franklyn Passmore III as Homeless Dave
* Edgar Allan Poe IV as Steve Houston
* Matt Prokop as Zack
* Shawn Richardson as Rick
* Richard Lewis Warren as Mark
* Kevin Perez as Matt

==Release==
An Angel Named Billy was released on July 6, 2007 at the LMBT Fesztivá although the official release of this film was on November 1, 2007 in Los Angeles, California, the DVD premier for this film was on November 6, 2007.  

==See also==
*List of lesbian, gay, bisexual, or transgender-related films by storyline

== References ==
 

== External links ==
*  at Internet Movie Database
*  at Rotten Tomatoes

 
 