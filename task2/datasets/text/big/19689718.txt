Third Time Lucky (1931 film)
Third 1931 British comedy film directed by Walter Forde and starring Bobby Howes, Dorothy Boyd and Gordon Harker. It was based on a play by Arnold Ridley. The films sets were designed by art director Walter Murton.

==Synopsis==
A young country vicar is spurred into action to protect his young ward, when she is blackmailed.

==Main cast==
* Bobby Howes ...  Rev. Arthur Fear
* Dorothy Boyd ...  Jennifer Elling
* Gordon Harker ...  Bill Meggitt
* Henry Mollison ...  Stanley Crofts
* Garry Marsh ...  Capt. Adrian Crowther
* Margaret Yarde ...  Mrs. Clutterbuck
* Harry Terry ...  Gregg
* Marie Ault ...  Mrs. Midge
* Clare Greet ...  Mrs. Scratton
* Matthew Boulton ...  Inspector

==References==
* 

 

 
 
 
 
 
 


 