Enoshima Prism
 
{{Infobox film
| name           = Enoshima Prism
| image          =
| image size     = 
| border         = 
| alt            = 
| caption        =
| director       = Yasuhiro Yoshida
| producer       = Toshinori Yamaguchi Kazushi Miki Junko Takemura Hisaaki Tai Norikazu Kumagai
| writer         = Yasuhiro Yoshida Hirotoshi Kobayashi
| screenplay     = 
| story          = Yasuhiro Yoshida Hirotoshi Kobayashi
| based on       =  
| narrator       = 
| starring       = Sota Fukushi Shūhei Nomura Tsubasa Honda
| music          = Shunsuke Kida
| cinematography = Yōichi Chiashi
| editing        = Taka Wada
| studio         = Geneon Universal Entertainment Video Planning Video 
 TVK
| distributor    = Video Planning
| released       =   
| runtime        = 90 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          =
}}

  is a 2013 Japanese film directed by Yasuhiro Yoshida. 

==Cast==
* Sota Fukushi as Shūta Jōgasaki
* Shūhei Nomura as Saku Kijima, Shūtas childhood friend
* Tsubasa Honda as Michiru Andō, Shūtas childhood friend
* Miki Honoka as Kyōko, a high school girl ghost
* You Yoshida as Matsudo, a science teacher
* Mariko Akama as Yoshie Kijima, Sakus mother
* Naomi Nishida as Hitomi Jōgasaki, Shūtas mother

==References==
 

==External links==
*    
*  
*   at   (in Japanese)
*   at   (in Japanese)

 
 

 