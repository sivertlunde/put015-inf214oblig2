Above the Rim
{{Infobox film
| name        = Above the Rim
| image       = Above the rim poster.jpg
| caption     = Theatrical release poster
| director    = Jeff Pollack
| story       = Jeff Pollack Benny Medina
| screenplay  = Barry Michael Cooper Jeff Pollack David Bailey Marlon Wayans
| producer    = James D. Brubaker Benny Medina Jeff Pollack
| distributor = New Line Cinema
| released    =  
| runtime     = 97 minutes
| language    = English
| country     = United States
| budget      = $6.5 million
| gross       = $16,192,320 
}}

Above the Rim is a 1994 American drama directed by Jeff Pollack.    The screenplay was written by Pollack and screenwriter Barry Michael Cooper,  from a story by Pollack and Benny Medina.

Starring Duane Martin, Tupac Shakur, Leon Robinson and Marlon Wayans, the film tells the story of a promising New York City high school basketball star and his relationships with two people; one a drug dealer and the other a basketball star now employed as a security guard at his former high school. The movie was shot in Harlem with various scenes in the movie filmed at Manhattan Center high school in East Harlem.  Some of the basketball scenes were filmed at Samuel J. Tilden High School in Brooklyn, NY.

==Plot== professional basketball player must make some tough decisions in this sports melodrama.
 coach or Birdie (Tupac Shakur), a local thug in the neighborhood. Kyle is also feeling resentment towards a security guard named Thomas "Shep" Shepherd (Leon Robinson), because Kyles own mother is falling in love with Shep.

Coincidentally, Kyles coach also wants Shep to coach his team when he feels it is time for him to retire, since Shep was previously a star basketball player himself.  It is later revealed to Kyle that Shep is Birdies older brother.  Due to the death of a friend, Nutso, Shep cannot bear the thought of playing again.

==Cast==
* Duane Martin as Kyle
* Leon Robinson as Shep
* Tupac Shakur as Birdie
* Marlon Wayans as Bugaloo
* Bernie Mac as Flip Johnson David Bailey as Rollins
* Tonya Pinkins as Mailika
* Wood Harris as Motaw
* Shawn Michael Howard as Bobby
* Henry Simmons as Starnes
* Matthew Guletz as Nutso
* Michael Rispoli as  Richie
* Byron Minns as Monrow
* Bill Raftery as Himself James Williams as Speedy John Thompson as Himself

== Soundtrack ==
{|class="wikitable"  Year
!rowspan="2"|Album Peak chart positions Certifications
|-
!width=40| Billboard 200|U.S. 
!width=40| Top R&B/Hip-Hop Albums|U.S. R&B 
|- 1994
|Above Above The Rim 
* Released: March 22, 1994 Interscope
|align="center"|2 1
|
* Recording Industry Association of America|US: Platinum
|-
|}

==Reception==

===Box office===
The film was released on March 23, 1994, grossing $3,738,800 on opening weeked. At the end of its theatrical run, it had grossed a total of $16,192,320.

===Awards=== 1995 MTV Movie Awards
* Best Movie Song: "Regulate (song)|Regulate" by Warren G. (nominated)

===Critical===
It holds a 53% rating on Rotten Tomatoes, based on 19 reviews. Peter Travers stated "Its Shakur who steals the show. The rappers offscreen legal problems are well known, but theres no denying his power as an actor."
Variety (magazine)|Variety said "A fine cast and the movies general energy cant overcome that mix of cliches and technical flaws, which should conspire to prevent any high flying at the box office." 

==See also==
* Above the Rim (Original Soundtrack)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 