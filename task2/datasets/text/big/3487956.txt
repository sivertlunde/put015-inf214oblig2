The Hills Have Eyes (2006 film)
 
{{Infobox film
| name           The Hills Have Eyes
| image          = The Hills Have Eyes film.jpg
| image_size     = 200px
| alt            = 
| caption        = Theatrical Release Poster
| director       = Alexandre Aja Peter Locke Marianne Maddalena Cody Zwieg
| screenplay     = Alexandre Aja Grégory Levasseur
| based on       =  
| starring       = Aaron Stanford Kathleen Quinlan Vinessa Shaw Emilie de Ravin Dan Byrd Robert Joy Ted Levine 
| music          = Tomandandy François-Eudes Chanfrault
| cinematography = Maxime Alexandre
| editing        = Baxter
| studio         = Dune Entertainment Major Studio Partners
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 106 Minutes
| country        = United States
                   Neston
| language       = English
| budget         = $15 million
| gross          = $69,623,713
}} The Hills Have Eyes. Written by filmmaking partners Alexandre Aja and Grégory Levasseur of the French horror film Haute Tension, and directed by Aja, the film follows a family that is targeted by a group of murderous mutants after their car breaks down in the desert.

The film was released theatrically in the United States and United Kingdom on March 10, 2006. It earned $15.5 million in its opening weekend in the U.S.,  where it was originally rated NC-17 for strong gruesome violence, but was later edited down to an R-rating. An unrated DVD version was released on June 20, 2006. A sequel, The Hills Have Eyes 2, was released in theaters March 23, 2007.

==Plot== Pluto (Michael Bailey Smith). Some time later, Bob Carter (Ted Levine) and his wife, Ethel Carter (Kathleen Quinlan) are traveling from Cleveland, Ohio to San Diego, California for their silver anniversary. With them are their teenage children Bobby (Dan Byrd), and Brenda (Emilie de Ravin), eldest daughter Lynn (Vinessa Shaw), Lynns husband Doug Bukowski (Aaron Stanford), their baby Catherine (Maisie Camilleri Preziosi), and their German Shepherds, Beauty and Beast.
 Tom Bower), Ruby (Laura Goggle (Ezra Papa Jupiter Lizard (Robert Joy), and Pluto.

Bobby returns to the trailer, but doesnt mention Beautys death not wanting to frighten the rest of the family. Later that night, the family is awakened by Bobs screams, and they all rush from the trailer, except for Brenda and the baby, who are left behind. Outside they find Bob being burned alive on a stake and they frantically try to save him, but to no avail. Meanwhile, Pluto and Lizard use the distraction to ransack the trailer, where Lizard beats and rapes Brenda. When Lynn returns to the trailer she is greeted by Lizard and Pluto, and forced to let Lizard drink from her breasts while her baby is held at gunpoint. When Ethel returns she is shot by Lizard, and Lynn stabs Lizard in the leg, prompting him to shoot Lynn in the head. Lizard and Pluto flee with the baby after Lizard attempts to shoot Brenda. 
 Big Mama Big Brain (Desmond Askew). After Big Brain tells him the mutants origins, Doug is attacked by Pluto, who severs two of his fingers with an axe. Pluto almost kills Doug, but Doug manages to gain the upper hand while Pluto is distracted, and kills him with his own axe.
 Cyst (Gregory Nicotero) and continues to search for Catherine. After ordering Lizard to kill Catherine, Big Brain is mauled to death by Beast. Lizard takes a cleaver and prepares to kill Catherine, but finds that Ruby has taken her. Doug sees Ruby running through the hills with Catherine and follows her. At the trailer, Brenda and Bobby prepare an explosive trap, which destroys the trailer, apparently killing Papa Jupiter.

Doug catches up with Ruby, but as she is about to return Catherine, Lizard suddenly attacks him. Lizard and Doug fight, and Lizard is presumed dead. When Doug turns his back Lizard aims a shotgun at him, but Ruby tackles Lizard off a cliff, killing them both. Bobby and Brenda find that Papa Jupiter managed to survive their trap, and Brenda finishes him off. They are then reunited with Doug, Catherine and Beast. As they celebrate their apparent victory, an unknown mutant watches through binoculars from the hills.

==Cast==
* Aaron Stanford as Doug Bukowski
* Emilie de Ravin as Brenda Carter
* Dan Byrd as Bobby Carter
* Vinessa Shaw as Lynn Carter-Bukowski
* Kathleen Quinlan as Ethel Carter
* Ted Levine as "Big" Bob Carter
* Billy Drago as Papa Jupiter
* Robert Joy as Lizard
* Michael Bailey Smith as Pluto
* Ezra Buzzington as Goggle
* Desmond Askew as Big Brain
* Gregory Nicotero as Cyst Tom Bower as Jeb, the Gas Station Attendant
* Laura Ortiz as Ruby
* Maisie Camilleri Preziosi as Catherine Bukowski
* Ivana Turchetto as Big Mama
* Maxime Giffard as First Victim
* Judith Jane Vallette as Venus
* Adam Perrell as Mercury

==Production== The Texas The Amityville Horror. The search then began for filmmakers to helm the project. Marianne Maddalena, Cravens long time producing partner, came across Alexandre Aja and his art director/collaborator Grégory Levasseur who had previously made the French slasher film Haute Tension. After showing the film to Craven and the rest of the production crew, they were impressed with the pair. Craven comments that they "demonstrated a multi-faceted understanding of what is
profoundly terrifying" and "After viewing the film and then meeting the film makers, I knew
I wanted to work with them."    Aja and Levasseur then began to re-write the story in what is the pairs first American production.

Director Aja and art director Grégory Levasseur chose not to film in the originals filming location of Victorville, California, and instead scouted many locations for filming including Namibia, South Africa, New Mexico, and Mexico. The two settled on Ouarzazate in Morocco, which was also known as "the gateway to the Sahara Desert". 
 Bikini and Eniwetok, between 1946 and 1962. The theatrical poster shows Vinessa Shaws character lying down with a mutant hand on her face.

===Effects=== Sin City.

K.N.B. spent over six months designing the mutants, first using 3D designer tools, such as ZBrush, allowing them to use a computer to generate their sculptures. After prosthetics were made, they could be fitted to the actors before filming. Robert Joy, who plays the mutant Lizard, explained, "Every day, these amazing artists took more than three hours to transform me into something that could only be found in a nightmare." 

K.N.B. artist Gregory Nicotero was also made a cameo as Cyst, the mutant with the halo head-gear.

Jamison Goei and his team, who had done previous work on   and  , had done over 130 visual effects for the film. A large part of that was digitally constructing the testing village, which in actuality was only one built street with others digitally added. The team also warped the mutants faces slightly, which is shown mostly in the character of Ruby.

Papa Jupiter displays no deformities. However, as shown in "The Making Of", Papa Jupiter appears to have a large parasitic twin attached to his upper left torso. The young children of the film had their deformities added by CGI, with the exception of Ruby, who had a combination of CGI and makeup.

===Casting===
The casting process began with the selection of   and   who was beginning her rise in the television series Lost (TV series)|Lost. After de Ravin, Dan Byrd was cast as Bobby. Byrd had previous genre experience starring in Salems Lot (2004 TV miniseries)|Salems Lot.

Aja then had the six actors arrive in the filming location of Morocco early to bond together.
 The Untouchables and other horror features. The most difficult mutant to cast was Ruby, who was a "touch of sweetness to the madness of the mutants."  Laura Ortiz was ultimately cast, making her film debut.

==Release==

===Box office===
The Hills Have Eyes was a commercial success, playing in total 2,521 theaters and taking in its opening weekend $15,708,512.    The film grossed $41,778,863 in the United States Box Office    and $69,770,032 worldwide,  surpassing its budget costs by over fourfold.

===Critical reception===
 
The movie got mixed reviews, with an average critic "Rotten" rating of 49% on  .    Bloody Disgusting, however, was scathing of those who referred to it as such, saying "some may call it “Torture porn” - these people are idiots".    Roger Ebert also gave a negative review, mentioning that the characters in the film are not familiar with horror movie, and went on to cite that the film should have focused more on the characters rather than the violence, saying "The Hills Have Eyes finds an intriguing setting in typical fake towns built by the government   But its mutants are simply engines of destruction. There is a misshapen creature who coordinates attacks with a walkie-talkie; I would have liked to know more about him, but no luck."   

==Soundtrack==
{{Infobox album |
|  Name        = The Hills Have Eyes
|  Type        = Film score
|  Artist      = Various
|  Cover       = The_Hills_Have_Eyes_Soundtrack.jpg
|  Released    = March 7, 2006
|  Recorded    = 
|  Genre       = Soundtracks Film scores
|  Length      = 70:14 
|  Label       = Lakeshore Records  33852  
|  Producer    = 
|  Reviews     =  The Hills Have Eyes The Hills Have Eyes 2
}}
The soundtrack score was composed by "Tomandandy". The record was released on March 7, 2006 via Lakeshore Records label.  

;Track listing
US Edition 
:Tracks 10-29 are all original music composed by Tomandandy
# Leave the Broken Hearts - The Finalist
# Blue Eyes Woman - The Go
# Highway Kind - Moot Davis
# Summers Gonna Be My Girl - The Go
# More and More - Webb Pierce
# The Walls - Vault
# In the Valley of the Sun - Buddy Stuart
# Daisy - Wires on Fire California Dreamin - The Mamas and the Papas
# Forbidden Zone
# Gas Haven
# Out House
# Praying
# Beauty
# Ravens
# Daddy Daddy
# Beast Finds Beauty
# Trailer
# Aftermath
# Ethels Death
# Next Morning
# Mine
# Village Test
# Breakfast Time
# Play with Us
# The Quest I
# The Quest II
# Sacrifice
# Its Over?

==References==
 

==External links==
* 
* 
* 
* 
*  at  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 