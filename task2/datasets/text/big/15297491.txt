A Dangerous Profession
{{Infobox film
| name           = A Dangerous Profession
| image          = A Dangerous Profession poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Ted Tetzlaff
| producer       = Robert Sparks
| writer         = Warren Duff Martin Rackin
| narrator       = Jim Backus Pat OBrien
| music          = Frederick Hollander Roy Webb
| cinematography = Robert De Grasse
| editing        = Frederic Knudtson
| distributor    = RKO Pictures
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Pat OBrien.  Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 143-144  The supporting cast features Jim Backus.

==Plot== Bill Williams), is murdered, Kane decides to investigate. He has two reasons for investigating: the curiosity of a former cop and it seems that he has fallen in love with Bracketts widow Lucy, an old flame.

==Cast==
* George Raft as Vince Kane
* Ella Raines as Lucy Brackett Pat OBrien as Joe Farley Bill Williams as Claude Brackett
* Jim Backus as Police Lt. Nick Ferrone
* Roland Winters as Jerry McKay
* Betty Underwood as Elaine Storm
* Robert Gist as Roy Collins, aka Max Gibney David Wolfe as Matthew Dawson

==Critical reception==
The New York Times gave the film a mixed review, and wrote, "Laconic and familiarly tough are the words for Rafts performance as the torch-bearing bail bonds-man. Ella Raines is decorative if little else as the object of his affections; Pat OBrien contributes a standard portrayal as his hard business partner; James Backus is professional as a tenacious detective lieutenant and Bill Williams is adequate in the brief role of the embezzler. A Dangerous Profession, in short, proves that the bail-bond business can be dangerous and that it also can be the basis for an exceedingly ordinary adventure." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 