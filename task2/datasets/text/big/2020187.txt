Common Law Cabin
{{Infobox film
| name           = Common Law Cabin
| image          = Commonlawcabin.jpg
| caption        =
| director       = Russ Meyer
| producer       = Jack Moran
| starring       = Alaina Capri Babette Bardot Jack Moran
| music          =
| cinematography = Russ Meyer
| editing        =
| distributor    =
| released       =  
| runtime        = 70 min.
| country        = United States
| awards         = English
| budget         =
}}
Common Law Cabin (original title How Much Lovin Does a Normal Couple Need?) is a 1967  ) 

==Plot==
Dewey Hoople (Jack Moran) runs a broken down tourist trap along the Colorado River along with his French wife Babette (Babette Bardot) and his daughter Coral (Adele Rein). Business is so bad that Hoople must pay a local alcoholic (Frank Bolger as "Cracker") to entice tourists, called "suckers", to spend some time and money there.

==Cast==
* Babette Bardot as Babette
* Frank Bolger as Cracker
* Alaina Capri as Sheila Ross John Furlong as Dr. Martin Ross
* Andrew Hagara as Laurence Talbot III
* Jack Moran as Dewey Hoople
* Adele Rein as Coral Hoople
* Ken Swofford as Barney Rickert

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 

 