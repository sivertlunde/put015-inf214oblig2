Staircase (film)
{{Infobox film
| name           = Staircase
| image          = Poster of the movie Staircase.jpg
| image_size     =
| caption        =
| director       = Stanley Donen
| producer       = Stanley Donen
| writer         = Charles Dyer
| narrator       =
| starring       = Rex Harrison Richard Burton
| music          = Dudley Moore
| cinematography =
| editing        =
| distributor    = Twentieth Century-Fox Film Corporation
| released       =  
| runtime        = 96 min
| country        = France
| language       =
| budget         = $6,370,000 
| gross          = $1,850,000  (US/ Canada rentals) 
}}
Staircase is a 1969 film adaptation of a two-character play, also called Staircase (play)|Staircase, by Charles Dyer. The film, like the play, is about an aging gay couple who own a barber shop in the East End of London. One of them is a part-time actor about to go on trial for propositioning a police officer. The action takes place over the course of one night as they discuss their loving but often volatile past together and possible future without each other. 

The two main characters are named Charles Dyer (the name of the playwright/screenwriter) and Harry C. Leeds, which is an anagram of his name.
==Production==
The screenplay was written by Dyer, and the film was directed by Stanley Donen. Dyer "opened up" the script to show the couples neighborhood, expanded the action to cover a period of ten days, and added characters. Rex Harrison and Richard Burton portrayed the couple and Cathleen Nesbitt and Beatrix Lehmann were featured as their mothers.

The film was produced by 20th Century Fox.
 The Only Game in Town at the same time as this film was in production. While that film is set in Las Vegas, Taylor demanded that director George Stevens shoot in France so she could be close to her husband. This caused the budget of The Only Game in Town to grow higher than most large-scale, high-profile films that Fox was producing at the time.

The films score was composed by musician/comedian Dudley Moore.
==Release== camp comedy. 
===Critical===
It was panned by most critics, including Roger Ebert, who gave it one star in his review and called it "an unpleasant exercise in bad taste . . .   gives us no warmth, humor or even the dregs of understanding. He exploits the improbable team of Rex Harrison and Richard Burton as a sideshow attraction."  The film was a commercial failure that lost most of its investment.

Rarely seen on television, the film was broadcast by Turner Classic Movies during its June 2007 tribute to gay cinema.

==See also==
*Staircase (play)

==References==
 

==External links==
* 
* , history of the film

 

 
 
 
 
 
 
 
 
 