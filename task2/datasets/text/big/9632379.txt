The Fighting Marines
 
 

{{Infobox film
| name           = The Fighting Marines
| image          = Fightingmarinesposter.JPG
| image_size     =
| caption        =
| director       = B. Reeves Eason Joseph Kane
| producer       = Nat Levine Barney A. Sarecky
| writer         = Wallace MacDonald Maurice Geraghty Ray Trampe Sherman L. Lowe Barney A. Sarecky
| narrator       = Adrian Morris Ann Rutherford Robert Warwick George J. Lewis Patrick H. OMalley, Jr.
| music          = Lee Zahler J.S. Zamecnik William Nobles
| editing        = Richard Fantl
| distributor    = Mascot Pictures
| released       =   23 November 1935
| runtime        = 12 chapters (216 min)
| country        =   English
| budget         =
| preceded_by    =
| followed_by    =
}} Mascot Serial movie serial.  It was the last serial ever produced by Mascot.  The studio was bought out and merged with others to become Republic Pictures.  This new company went on to become the most famous of the serial producing studios, starting with Darkest Africa in 1936.
 Republic Film Producer Franklin Adreon first became involved with serials with this production.  The former regular Marine then a Marine Corps Reserve officer was a technical consultant and played the small role of Captain Holmes in the later chapters.

==Plot== landing strip on Halfway Island in the Pacific Ocean, they interfere with the secret hideout of the masked mystery villain, The Tiger Shark, who begins to sabotage their efforts.  Sergeant Schiller is abducted by the villain after developing a gyrocompass that could pinpoint his location.  Corporal Lawrence and Sergeant McGowan attempt to rescue him and stop the Tiger Shark for good.
 

==Cast== US Marine Adrian Morris US Marine
*Ann Rutherford as Frances Schiller Colonel W. US Marine US Marine abducted by the Tiger Shark Captain Grayson
*Victor Potel as Fake Native Chief, one of the Tiger Sharks henchmen
*Jason Robards Sr. as Kota
*Warner Richmond as Metcalf, one of the Tiger Sharks henchmen
*Robert Frazer as H. R. Douglas
*J. Frank Glendon as M. J. Buchanan
*Donald Reed as Pedro, one of the Tiger Sharks henchmen
*Max Wagner as Gibson, one of the Tiger Sharks henchmen Richard Alexander as Ivan, one of the Tiger Sharks henchmen
*Tom London as Miller, one of the Tiger Sharks henchmen

==Production==

===Stunts===
*Yakima Canutt
*George DeNormand doubling Grant Withers
*Eddie Parker doubling Adrian Morris

===Special effects===
*Photographic effects by Bud Thackery
*Model effects by the Lydecker brothers

==Chapter titles==
# Human Targets
# Isle of Missing Men
# The Savage Hoard
# The Mark of the Tiger Shark
# The Gauntlet of Grief
# Robbers Roost
# Jungle Terrors
# Siege of Halfway Island
# Death from the Sky
# Wheels of Destruction
# Behind the Mask
# Two Against the Horde
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 213
 | chapter = Filmography
 }} 

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 

 
{{Succession box Mascot Serial Serial
| before=The Adventures of Rex and Rinty (1935 in film|1935)
| years=The Fighting Marines (1935 in film|1935)
| after=none}}
 

 

 

 
 
 
 
 
 
 
 
 
 