Kaadhal
{{Infobox film
| name           = Kadhal/Kadal
| image          =
| director       = Balaji Sakthivel
| writer         = Balaji Sakthivel Bharath Sandhya Sandhya Sukumar
| producer       = S. Shankar
| distributor    = S Pictures
| music          = Joshua Sridhar
| cinematography = S. D. Vijay Milton
| editing        = G. Sasikumar
| released       =  
| runtime        =
| country        = India
| language       = Tamil
}}
 Bharath and Sandhya in Telugu as Kannada as Bengali as Chirodini Tumi Je Amar in 2008. 

==Plot==
Murugan (Bharath (actor)|Bharath) is a diligent scooter mechanic in Madurai and life goes on smoothly for the young man until a rich student Aishwarya (Sandhya (actress)|Sandhya) sets eyes on him. The infatuation reaches a dangerous level when she coaxes Murugan to take her from the constraint of her family, who have other plans about her future. The naïve Murugan hesitantly yields to her charm and the two run away to Chennai. Murugans friend Stephen (Sukumar) helps them in their hour of crisis and the lovers unite in marriage. But Sandhyas family dotes on her and is not going to give up so easily. The family tracks down the couple and eventually separates them. Aishwarya agrees to marry another man to save Murugans life after he is being beaten by her father. While she believes this decision is best and moves on with her life, she one day (while on the road with her husband and daughter) runs into Murugan who has suffered permanent brain damage. Knowing that she made a mistake and not knowing how to undo the sins of her father, the movie ends with Aishwarya and her husband deciding to take care of Murugan. 

==Cast== Bharath as Murugan Sandhya as Aishwarya Rajendran
* Sukumar as Stephen
* Dhandapani as Rajendran
* S. Krishna Murthy as Aishwaryas Uncle
* Arun Kumar as Murugans assistant Saranya as Sathya

==Box office==
Produced on a budget of $100,000, the film was a surprise blockbuster grossing almost $1 million at the box office. Super HIT Movie 

== Soundtrack ==
The film has eight songs composed by Joshua Sridhar. Lyrics were penned by Na. Muthukumar.
{{Track listing
| extra_column = Singer(s) Tippu | length1 = 3:29
| title2 = Ivanthan | extra2 = Sunitha Sarathy | length2 = 2:09
| title3 = Thandattikarupaiyee | extra3 = Pop Shalini, Vidhya, Malar, Maalaiamma  | length3 = 5:45
| title4 = Thottu Thottu | extra4 = Haricharan, Harini Sudhakar | length4 = 5:41
| title5 = Pura Koondu | extra5 = Suresh Peters, Harish Raghavendra, Tippu, Premji Amaren|Premji, Karunas | length5 = 5:31
| title6 = Kiru Kiru | extra6 = Karthik (singer)|Karthik, Pop Shalini | length6 = 4:32
| title7 = Unakkena Iruppaen | extra7 = Haricharan | length7 = 6:16
| title8 = Kaadhal | extra8 = Haricharan | length8 = 4:03
}}

==Notes==
 

== References ==

*  

==External links==
*  
* 

 
 

 
 
 
 
 
 
 
 


 