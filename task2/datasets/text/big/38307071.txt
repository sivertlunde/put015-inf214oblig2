Ada Apa Dengan Rina
{{Infobox film
| name = Ada Apa Dengan Rina
| image = Ada Apa Dengan Rina.jpg
| caption = Theatrical release poster
| director = Harlif Haji Mohamad Farid Azlan Ghani
| producer = Nurain Abdullah
| writer = Farid Azlan Ghani
| starring = Syukri Mahari Tauffek Ilyas Dayangku Moniri Pengiran Mohiddin
| music = Pengiran Mohammad Yusri Pengiran Haji Yahya Harlif Haji Mohamad
| cinematography = Yusrin Yahya
| studio = Regalblue Production
| released =  
| runtime = 90&nbsp;minutes Brunei Darussalam
| language = Brunei Malay
}} comedy feature film produced by Regalblue Production.

The film is the second Bruneian feature film, after 1968 film called Gema Dari Menara.

Ada Apa Dengan Rina is in Brunei Malay, a dialect used by Bruneians everyday.     This is the first time such dialect being played on big cinema screens.        The film is also the first to involve fully local creative and technical expertise including cast members. 

==Plot==
Hakim, a handsome man with a high paying job is still looking for his one truelove. His housemate, Faisal introduces to him a theory in searching for a perfect significant other. According to it, the perfect person goes by the name Rina.

Ironically, Hakim is then introduced to a new marketing manager named Rina. Rina is a beautiful, smart woman whose exceptional beauty has win the hearts of her colleagues including Hakim.

Spending a lot of time working on a big project for the company together, Hakim and Rina get closer to each other each day. Seeing the blossoming relationship, Hakim’s mother and friend start pushing him to marry Rina.

Meanwhile, Faisal fell in love with a young widow named Tini. He was challenged by Elvis who is also trying to get Tini’s attention. So they compete in a reality TV singing competition.

As days go by, Hakim’s family and best friend keep on pushing him. Looking at Rina’s respond to him, he finally decides to propose to Rina.

Will she say yes?

==Cast==
*Syukri Mahari as Hakim
*Tauffek Ilyas as Faisal
*Dayangku Moniri Pengiran Mohiddin as Rina
*Haji Mohd Firdaus Haji Salleh as Elvis
*Norharizah Haji Sisi as Tini

==Special Appearance==
*Hans Anuar
*Zul Faden
*Lo Ryder

==Production==
The idea of producing a feature film has started when Regalblue Production was first established in 2002. However, the production team felt that they were not fully ready to produce a full-length feature film especially in terms of production equipments.

Nonetheless, with the ever growing passion for film, they came with an idea of producing a comedy feature film and hence, the script as well as the screenplay of Ada Apa Dengan Rina was first written in 2006.

As they approach their ten years of involvement in audio visual production, with the absence of Brunei film submission in a number of international film festivals that they have attended as well as seeing the importance of film as a medium to introduce Brunei to the world, they finally decided to produce the film.

In April 2012, the script was then revised and in the same month, pre-production of the film started. Followed by the production which took them two months to complete in October and November 2012. The post-production was then done in December 2012 until February 2013.

The film was fully funded by the company itself, with an estimated budget of BND$150,000 (US$120,000), a sum that they have accumulated throughout the ten years. Their attempts to seek for fund from many agencies were to no avail. The only sponsorship that they get was from the Brunei Tourism Development Department for the production team trip expenses to the Ulu Temburong National Park to film one of the scenes in the film.

Even though they have been using the Broadcasting Code of Practice of Brunei Darussalam as their guide in all of their audio visual productions throughout the ten years, the most anticipated moment was when the film, just like any other films to be screened in the Brunei cinemas, was going through the censorship board of Brunei. 

On February 16, 2013, just one a week from the premiere, the censorship result was out; the film was approved for public viewing in Brunei Darussalam and rated G under the Censorship of Films and Public Entertainment Act (Cap 69). 

==Release==
 
Ada Apa Dengan Rina premiered on February 17, 2013 and was released on February 23, 2013 in Brunei Darussalam.  The film was screened and will be screened at a number of film festivals including:
* 2013 ASEAN International Film Festival and Awards (AIFFA) in Kuching, Malaysia,
* ASEAN Film Festival 2013 in Singapore,   
* Luang Prabang Film Festival 2013 in Laos,   
* 7th Asian Film Festival 2014 in Jeddah, Saudi Arabia,   
* ASEAN Film Festival 2014 in Yangon, Myanmar,
* World Premieres Film Festival 2014, Philippines,   
* ASEAN Film Festival 2014 in Seoul, Korea,    

==Home media==
 
Ada Apa Dengan Rina was released on DVD on June 24, 2014.

==Reception==
Ada Apa Dengan Rina received such an overwhelming support from the public that the shows were fully booked for days after its release and 80 percent occupancy throughout its showing.   

The film was well-received by film festivals in ASEAN member countries as Ada Apa Dengan Rina, a feature film from Brunei, finally completes the entries for all 10 ASEAN countries.   
 The Nation stated that "Whats So Special About Rina? is flat-out hilarious". 

==Awards==
{| class="wikitable" style="font-size:90%"
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | Awards
|- style="text-align:center;"
! style="background:#ccc;"| Award
! style="background:#ccc;"| Category
! style="background:#ccc;"| Recipient(s)
! style="background:#ccc;"| Outcome
|- ASEAN International Film Festival & Awards
| Best Picture (Comedy)
| Ada Apa Dengan Rina
|     
|-
| Special Jury Award
| Ada Apa Dengan Rina
|     
|}

==References==
 

== External links ==
* 
* 
*  

 
 
 
 
 