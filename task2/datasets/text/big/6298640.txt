The North Star (1943 film)
{{Infobox film
| name           = The North Star
| image          = Northstar poster.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Lewis Milestone
| producer       = Samuel Goldwyn William Cameron Menzies 
| writer         = Lillian Hellman (story and screenplay)
| starring       = Anne Baxter Dana Andrews Walter Huston Walter Brennan Erich von Stroheim
| music          = Aaron Copland
| cinematography = James Wong Howe
| editing        = Daniel Mandell
| studio         = Samuel Goldwyn Productions
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
}}
The North Star (also known as Armored Attack in the US) is a 1943 war film produced by Samuel Goldwyn Productions and distributed by RKO Radio Pictures. It was directed by Lewis Milestone, written by Lillian Hellman and featured production design by William Cameron Menzies. The film starred Anne Baxter, Dana Andrews, Walter Huston, Walter Brennan and Erich von Stroheim. The music was written by Aaron Copland, the lyrics by Ira Gershwin, and the cinematography was by James Wong Howe. The film also marked the debut of Farley Granger.
 Ukrainian SSR. Soviet propaganda film at the height of the war. 

In the 1950s it was criticised for this reason and it was recut to remove the idealized portrayal of Soviet collective farms at the beginning and to include references to the Hungarian Uprising of 1956.  

==Plot==
In June 1941 Ukrainian villagers are living in peace.  As the schools break up for vacation, a group of friends decide to travel to Kiev for a holiday.  To their horror they find themselves attacked by German aircraft, part of the Nazi invasion of the Soviet Union. Eventually their village itself is occupied by the Nazis. Meanwhile men and women take to the hills to form partisan militias. 

The full brutality of the Nazis is revealed when a German doctor (Erich von Stroheim) uses the village children as a source of blood for transfusions into wounded German soldiers. Some children lose so much blood that they die. A famous Russian doctor (Walter Huston) discovers this and informs the partisans, who prepare to strike back. They launch a cavalry assault on the village to rescue the children. The Russian doctor accuses the German doctor of being worse than the convinced Nazis, because he has used his skills to support them. He then shoots him. The peasants join together, and one girl envisions a future in which they will "make a free world for all men".

==Cast==
 
* Anne Baxter as Marina Pavlova
* Dana Andrews as Kolya Simonov
* Walter Huston as Dr. Pavel Grigorich Kurin
* Walter Brennan as Karp
* Ann Harding as Sophia Pavlova
* Jane Withers as Clavdia Kurina
* Farley Granger as Damian Simonov
* Erich von Stroheim as Dr. von Harden
* Dean Jagger as Rodion Pavlov
* Carl Benton Reid as Boris Stepanich Simonov
* Ann Carter as Olga Pavlova
* Esther Dale as Anna Ruth Nelson as Nadya Simonova
 

==Criticism== Days of Dragon Seed on Chinese efforts against the Japanese occupation.    

The extent to which the film incorporated official Soviet propaganda about collective farms prompted British historian Robert Conquest, a member of the British Foreign Offices Information Research Department (a unit created for the purpose of combating communist influence and promoting anti-communist ideas)    in the 1950s to later write "a travesty greater than could have been shown on Soviet screens to audiences used to lies, but experienced in   to a degree requiring at least a minimum of restraint". The Harvest of Sorrow: Soviet Collectivization and the Terror Famine, Conquest, page 321, Oxford Press, 1986; see Chapter 17 for detailed information on the efforts of pro-Soviet Westerns to help the regime cover up the true conditions on the collective farms. 

==Recut==
The film was rereleased in 1957 under the title of Armored Attack. This version starts with the entry of the German column into the town and ends with narration of Hungarians fighting the Red Army during the Hungarian Uprising of 1956.

In later years, the actual print was made available on video with none of the cut segments  of the 1957 print.

==Awards==
The film was nominated for six Academy Awards:   
* Art Direction (Black-and-White) (Perry Ferguson, Howard Bristol)
* Cinematography (Black-and-White) (James Wong Howe)
* Music (Music Score of a Dramatic or Comedy Picture) (Aaron Copland) Sound Recording (Thomas T. Moulton) Special Effects (Clarence Slifer, Ray Binger, Thomas T. Moulton)
* Writing (Original Screenplay) (Lillian Hellman)

==References==
 

== External links ==
 
*  
*  
*  
*   can be viewed for free on  .

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 