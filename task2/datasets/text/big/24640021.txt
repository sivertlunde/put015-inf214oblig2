La bonne année
La bonne année (also known in the U.S. as Happy New Year) is a 1973 film directed by Claude Lelouch.

==Synopsis==
A gangster (Lino Ventura) with his accomplice (Charles Gérard), prepares to take part in the "first psychological hold-up in the history of crime". Next door to the jewellers of Van Cleef & Arpels, on the Promenade de la Croisette|Croisette, in Cannes, they find the shop of a beautiful antiques dealer (Françoise Fabian) who befriends the group.

The driver and the antiques dealer fall in love. He is friendly but unrefined, she is cultivated and independent, but discovers that talking with Simon shows up her vanity, and she desires a simple and clean love. The hold-up which had been planned for a long time by the driver is shown to be more sophisticated than his methods of seduction.

==About the film==
 

==Details==
*Director : Claude Lelouch
*Script : Claude Lelouch and Pierre Uytterhoeven
*Director of photography: Jean Collomb
*Music: Francis Lai
*Sound : Bernard Bats
*Editor: Georges Klotz
*Assistant director: Elie Chouraqui 
*Length: 115 minutes
*Release date: 13 April 1973
Production : Films 13 (Paris), Rizzoli Film (Rome)  
Distributor- Films 13 (Paris)

==Starring==
*Lino Ventura : Simon
*Françoise Fabian : Françoise
*Charles Gérard : Charlot
*André Falcon : bijoutier
*Lilo De La Passardière : Mme Félix
*Claude Mann : Claude
*Frédéric de Pasquale : Henri
*Gérard Sire : directeur de la prison/voix commentaire TV
*Silvano Tranquilli : amant italien
*  Bettina Rheims  : Nicole
*  Georges Staquet  : commissaire
*  Harry Walter  : 1st inspecteur
*  Elie Chouraqui  : Michel Barbier (non crédité)
*Michel Bertay : chauffeur de taxi
*Michou : Lui-même
*Mireille Mathieu : Elle-même
*André Barello
*Norman de la Chesnaye
*Pierre Edeline
*  Pierre Cottier
*  Joseph Rythmann
*  Jacques Villedieu.
*Pierre Pontiche

== About the film ==
Much of the film was financed by advertising, particularly for Van Cleef & Arpels.

==Awards==
*Best actor at the San Remo Film Festival for Lino Ventura and Françoise Fabian
*Prix Triomphe du Cinéma 1973

==American remake== Happy New Year directed by John G. Avildsen and starring Peter Falk, Charles Durning and Tom Courtenay.

==External links==
*  

 
 

 
 
 
 
 

 