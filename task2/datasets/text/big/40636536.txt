They Call It Pro Football
 
They Call It Pro Football is a 1967 sports documentary film about American football. The first full-length film from NFL Films, its visual style helped to define future presentations of the sport on film and TV.   In 2012, the film was deemed "culturally, historically, or aesthetically significant" by the United States Library of Congress and selected for preservation in the National Film Registry.   

==Production==
It was the first full-length production of NFL Films, founded by Ed Sabol and serving as the film division of the National Football League. 

It was written and produced by Ed Sabols son Steve Sabol, and voiced by John Facenda, whose narration begins: "It starts with a whistle and ends with a gun".   

It makes extensive use of slow motion and close-up photography, techniques which are now standard in sports coverage. 

Initially the makers struggled to find a distributor: none of the major TV networks were interested, and Steve Sabol struggled to show it one screening at a time.    It was first shown on American TV in 1969. 

==Critical reaction==
Critics have praised the film for its historical importance as the first film by NFL Films, and particularly for its great influence on subsequent depictions of the sport. CBS News said it "helped shape the look and sound of sports television ever since".  The Library of Congress in its citation said "Before They Call It Pro Football premiered, football films were little more than highlight reels set to the oom-pah of a marching band" but this film "presented football on an epic scale and in a way rarely seen by the spectator".  Rich Cohen in The Atlantic said: It came to define the aesthetic of modern, hyper-vivid sports coverage, taking viewers inside the huddle, letting them hear the collisions and understand the coaches’ tactics. It turned every game into Waterloo and every player into an epic hero. It taught America how to watch football.  

Josh Alper for NBC called it "one of the greatest productions on the long list of masterpieces produced by NFL Films". 

==References==
 

 
 
 