Wolf Children
{{Infobox film
| name           = Wolf Children
| image          = Ōkami Kodomo no Ame to Yuki poster.jpg
| alt            = The poster shows a young woman holding two children, both with tails and wolf ears standing in a grassy field on a cloudy day with the sun coming out. At the top is the films title, written in Japanese white letters and the tagline, written in blue letters. At the posters bottom is the films release date and production credits.
| caption        = Japanese release poster
| director       = Mamoru Hosoda
| producer       = Yuichiro Saito Takuya Ito Takashi Watanabe
| screenplay     = Satoko Okudera Mamoru Hosoda 
| story          = Mamoru Hosoda
| starring       = Aoi Miyazaki Takao Osawa Haru Kuroki Yukito Nishii
| music          = Takagi Masakatsu
| cinematography = 
| editing        = Shigeru Nishiyama Madhouse
| distributor    = Toho
| released       =     
| runtime        = 117 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = $53,923,613 
}}
 animated film directed and co-written by Mamoru Hosoda.   The film stars the voices of Aoi Miyazaki, Takao Osawa, Haru Kuroki and Yukito Nishii. The story follows a young mother who is left to raise two werewolf children after their werewolf father dies.

To create the film, director Hosoda established Studio Chizu, which co-produced the film with   (1990) and Neon Genesis Evangelion (1995), designed characters for the film. Wolf Children had its world premiere in Paris on June 25, 2012, and was released theatrically on July 21, 2012 in Japan.  It is licensed by Funimation Entertainment in North America and was released on DVD and Blu-ray Disc|Blu-ray on November 23, 2013.  It was screened in the UK at the end of October 2013 with a DVD and Deluxe Blu-ray/DVD edition from Manga Entertainment following on December 23, 2013.

==Plot==
 
College student Hana falls in love with a werewolf  and has two half-werewolf children with him: a daughter, Yuki, and a year later a son, Ame. Soon after Ames birth, their father is killed while hunting food for the children.

Raising Yuki and Ame alone is difficult; they constantly switch between their human and wolf forms, and Hana has to hide them from the world. When she is visited by social workers concerned that the children have not had vaccinations, Hana moves the family to the countryside away from prying neighbors. She works hard to repair the dilapidated house and sustain the family on their own crops. Ame almost drowns in a river after trying to hunt a kingfisher; Yuki saves him and Ame becomes more confident.

Yuki begs her mother to let her go to school like other children. Hana accepts on the condition that Yuki keeps her werewolf nature secret. Though Yukis classmates find her strange at first, she soon makes friends. However, Ame is more interested in the forest, and takes lessons from an old fox about survival in the wild.

Yukis class receives a new transfer student, Souhei, who realizes something is strange about her. When he pursues her, Yuki transforms into a wolf and accidentally injures him, leading to a meeting with his parents and teachers. Souhei tells them a wolf attacked him, absolving Yuki of blame, and the two become friends.

Yuki and Ame fight over whether they are human or wolf. The next day, while Yuki is at school, a fierce storm gathers and Ame disappears into the forest to help his fox teacher, who is dying; Hana goes after him. The other children are picked up from school by their parents, leaving Yuki and Souhei alone. Yuki reveals her secret to him by transforming into a wolf. Souhei tells her he knows already, and promises to keep her secret.

As Hana searches the forest for Ame, she slips and falls unconscious. Ame finds her and carries her to safety. She awakens to see Ame transform into an adult wolf and run into the mountains. She realizes he has found his own path and accepts his goodbye. 

The next year, Yuki leaves home to move into a dorm in junior high school. Ames wolf howls can be heard far and wide in the forest. Hana, living alone in the house, reflects that raising her wolf children was like a fairy tale, and feels proud to have raised them well.

==Voice cast==
===Japanese cast===
* Aoi Miyazaki as Hana
* Takao Osawa as the Wolfman
* Haru Kuroki as Yuki
* Yukito Nishii as Ame
* Momoka Ono as Yuki (Child)
* Amon Kabe as Ame (Child)
* Takuma Hiraoka as Sōhei Fujii
* Megumi Hayashibara as Mrs. Fujii Tadashi Nakamura as Hosokawa
* Tamio Ōki as Yamaoka
* Shota Sometani as Tanabe-sensei
* Mitsuki Tanimura as Dois wife
* Kumiko Aso as Horitas wife
* Bunta Sugawara as Nirasaki
* Tomie Kataoka as Nirasakis daughter

===English cast===
* Colleen Clinkenbeard as Hana
* David Matranga as the Wolfman
* Jad Saxton as Yuki
* Micah Solusod as Ame
* Lara Woodhull as Yuki (Child)
* Alison Viktorin as Ame (Child)
* Jason Liebrecht as Sōhei
* Lydia Mackay as Sōheis Mother
* Jerry Russell as Grandpa Nirasaki
* Kenny Green as Mr. Nirasaki
* Wendy Powell as Mrs. Nirasaki
* Sonny Strait as Mr. Tanabe
* Bob Magruder as Uncle Doi
* Linda Leonard as Aunt Doi
* Kate Oxley as Mrs. Doi
* Mark Stoddard as Uncle Hotta
* Melinda Wood Allen as Aunt Hotta
* Jamie Marchi as Mrs. Hotta
* Robert Bruce Elliott|R. Bruce Elliott as Hosokawa
* Bill Flynn as Yamaoka Kent Williams as Tendo
* Mike McFarland as Kuroda
* Jason C. Miller|J.C. Miller as Radio Announcer
* Leah Clark as Shino
* Kristi Kang as Keno
* Alexis Tipton as Sōko
* Felecia Angelle as Bunko

==Release==
At a press conference held on 18 June 2012, the director Mamoru Hosoda announced that Wolf Children would be released in 34 different countries and territories.    This film was first released in France on June 25, 2012, marking its international debut.  It was subsequently released in Japan on July 21, 2012.    The film made its US premiere at the 2012 Hawaii International Film Festival and had a limited release in the US on September 27, 2013.  The films Blu-ray and DVD release date for Japan has been confirmed for February 20, 2013.

The Newport Beach Film Festival in Newport Beach, CA, screened Wolf Children on April 27, 2013. 

Wolf Children was screened at Animefest 2013 in May in the Czech Republic  and at Animafest Zagreb 2013 in June in Croatia. 

===Other media===
In addition to the film, two novelizations and a manga written by Hosoda (with art by  ) were released by Kadokawa Shoten.    As tie-ins to the film, a film picture book, an art book, and a storyboard book were released from Kadokawa, Media Pal, and Pia.
*Wolf Children Ame and Yuki by Mamoru Hosoda, Kadokawa Shoten, 22 June 2012, ISBN 9784041003237
*Mamoru Hosoda Pia, Pia, 10 July 2012, ISBN 9784835621203
*Wolf Children Ame and Yuki by Yū (illustrations) and Mamoru Hosoda, Kadokawa Comic Ace, 14 July 2012, ISBN 9784041203217
*Wolf Children Ame and Yuki by Mamoru Hosoda, Kadokawa Tsubasa Bunko, 15 July 2012, ISBN 9784046312488
*Kadokawa Picture Book Wolf Children Ame and Yuki by Mamoru Hosoda, Kadokawa Shoten, 15 July 2012, ISBN 9784041102473
*Wolf Children Ame and Yuki Storyboards Animestyle Archive by Mamoru Hosoda, Media Pal, 21 July 2012, ISBN 9784896102468
*Wolf Children Ame and Yuki Official Book: Hana no Yō ni edited by the Wolf Children Ame and Yuki Production Committee, Kadokawa Shoten, 23 July 2012, ISBN 9784041102480
*Wolf Children Ame and Yuki Artbook edited by the Wolf Children Ame and Yuki Production Committee, Kadokawa Shoten, 25 August 2012, ISBN 9784041102862

==Reception==

===Box office===
Wolf Children was the second-highest grossing film in Japan on its debut weekend of 21–22 July 2012, beating Disney Pixars animation film Brave (2012 film)|Brave, which debuted in Japan on the same weekend.    It attracted an audience of 276,326 throughout the weekend, thus grossing a total of 365.14 million yen.  The film subsequently surpassed Mamoru Hosoda previous work Summer Warss Japanese gross of around 1.6 billion yen during the weekend of 12–13 August 2012.   

In total, Wolf Children grossed 4.2 billion yen, making the 5th highest grossing movie in Japan in 2012.   

===Critical reception===
Wolf Children was well received by critics. The film received a 72 out of 100 from review aggregate website Metacritic based on 5 critics, indicating "generally favorable reviews."  and a 93% on Rotten Tomatoes. 
 Call of the Wild fans in the next", with him taking issue with "the well-worn, stereotypical rails on which the stories ran".  Overall, Schilling gave the film a rating of 3 out of 5 stars. 

Par Thomas of Le Monde rated this film as a "Excellent", and gave it a rating of five out of five stars.    Dave Chua of  Mypaper also praised the film, saying that "There is a magnificent understated eye for detail, from the grain of wood on doors to the lovingly captured forest scenes, that help lift the movie above regular animation fare."    Chris Michael of The Guardian gave the film a rating of four stars out of five stating that "telling the story through the eyes of the harried, bereaved but indomitable mother gives this calm, funny, only occasionally schmaltzy family film a maturity Twilight (film series)|Twilight never reached."  Kenneth Turan of the Los Angeles Times reviewed the film, referring to it as "an odd story, told in a one-of-a-kind style that feels equal parts sentimental, somber and strange." Turan also said while the visuals are "colorful and at times lyrical, the dubbed English-language soundtrack can sound overly sweet and simplistic while the narrative takes some rather harsh turns."  Steven D. Greydanus, writing in the National Catholic Register, said, "Despite brief early problematic content and an ambiguous climactic letdown, the main story is magic." 

===Awards===
Wolf Children won the 2013  .  Wolf Children won the 2014 Best Anime Disc award from Home Media Magazine. 

==References==
 

==External links==
*   
*   (English)
* 
**  
**  
* 
*  
*  For Manga Entertainment UK release on 23 December 2013

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 