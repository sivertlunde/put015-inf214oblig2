Captains of the Clouds
{{Infobox film 
| name           = Captains of the Clouds 
| image          = Captainsoftheclouds3iz.jpeg
| image_size     =
| caption        = VHS cover
| producer       = Hal B. Wallis William Cagney 
| director       = Michael Curtiz
| writer         = Arthur T. Horman Roland Gillett Richard Macaulay Norman Reilly Raine 
| starring       = James Cagney Dennis Morgan Brenda Marshall Alan Hale, Sr.  Billy Bishop
| music          = Max Steiner (score) Harold Arlen (title song)|
| cinematography = Wilfred M. Cline, Sol Polito Winton C. Hoch Charles A. Marshall
| editing        = George Amy
| distributor    = Warner Bros.
| released       =    
| runtime        = 114 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Captains of the Clouds is a 1942 Warner Bros. war film in Technicolor,  directed by Michael Curtiz and starring James Cagney.  It was produced by William Cagney (James Cagneys brother), with Hal B. Wallis as executive producer. The screenplay was written by Arthur T. Horman, Richard Macaulay, and Norman Reilly Raine, based on a story by Horman and Roland Gillett. The cinematography was by Wilfred M. Cline, Sol Polito, and Winton C. Hoch and was notable in that it was the first feature length Hollywood production filmed entirely in Canada. 

The film stars   documentary (1939).

In 1942, Canada had been at war with the Axis Powers for over two years, while the United States had only just entered in December 1941. A film on the ongoing Canadian involvement made sense for the American war effort. The films ends with an epilogue chronicling the contributions of the Royal Canadian Air Force (RCAF) to the making of the film.
 

==Plot==
Brian MacLean (James Cagney), Johnny Dutton (Dennis Morgan), "Tiny" Murphy (Alan Hale, Sr.), "Blimp" Lebec (George Tobias), and "Scrounger" Harris (Reginald Gardiner) are Waco Standard Cabin series|Waco-flying bush pilots competing for business in rugged Northern Ontario, Canada in 1939, as the Second World War is beginning. Dutton flies by the book but MacLean is a seat-of-the-pants kind of pilot, mirroring the differences in their personalities.

When Dutton saves MacLeans life by transporting a doctor under dangerous flying conditions, MacLean is grateful. He steals and marries Duttons badly-behaved girlfriend Emily Foster (Brenda Marshall) in order to save him from a life of misery. Dutton, however, does not understand that MacLeans actions are an act of kindness, and so he abruptly ends their friendship. Depressed, Dutton gives his savings to charity and enlists in the Royal Canadian Air Force. Canadian Bushplane Heritage Centre, Sault Ste Marie Ontario Canada , and the Fairchild 71C above it, now displayed in the Alberta Aviation Museum. ]]

Later, after hearing Winston Churchills "We shall fight on the beaches" speech on the radio, MacLean and the other bush pilots attempt to enlist in the air force, only to discover that they are too old for combat. They reluctantly agree to train as flight instructors for the British Commonwealth Air Training Plan. Their superior officer is none other than Dutton. MacLeans brash and fiercely independent nature clashes with the military way of doing things, and he inevitably washes out. For revenge, he and "Tiny" buzz the airfield in their bush planes when renowned Canadian First World War ace Air Marshal William "Billy" Bishop (playing himself) is speaking during the groups graduation ceremony. During the buzzing,"Tiny" suffers a blackout (loss of vision due to g-forces), crashes the plane and dies.
 Newfoundland to Britain. MacLean pretends to be Murphy and volunteers to fly one. He finds himself commanded by Dutton, who recognizes him, but nonetheless permits him to fly. Near the coast of the British Isles, the bombers are attacked ruthlessly by a single German fighter plane, a Messerschmitt Bf 109. "Blimp" Lebecs plane is shot down, and it becomes clear that the Messerschmitt intends to pick off the bombers one by one. After MacLeans British navigator "Scrounger" is killed by the German pilots machine gun fire, MacLean uses his superb flying skills to crash his unwieldy bomber into the nimble fighter plane, sacrificing himself to save the remainder of the group.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| James Cagney || Brian MacLean
|-
| Dennis Morgan || Johnny Dutton
|-
| Brenda Marshall || Emily Foster 
|- Alan Hale || "Tiny" Murphy 
|-
|  George Tobias || "Blimp" Lebec 
|-
|  Reginald Gardiner || "Scrounger" Harris 
|- Air Marshal W.A. Bishop || Himself 
|- Reginald Denny || Commanding officer 
|-
| Russell Arms || Prentiss 
|-
| Paul Cavanagh || Group Captain 
|-
| Clem Bevans || "Store-teeth" Morrison 
|-
| J. M. Kerrigan || Foster 
|-
| J. Farrell MacDonald || Dr. Neville 
|-
| Patrick OMoore || Fyffe 
|-
| Morton Lowry || Carmichael 
|-
| O. Cathcart-Jones || Chief flying instructor  
|-
| Frederic Worlock || President of court-martial 
|-
| Roland Drew || Officer 
|-
| Lucia Carroll || Blonde 
|-
| George Meeker || Playboy 
|-
| Benny Baker || Popcorn Kearns 
|-
| Hardie Albright || Kingsley 
|-
| Ray Walker || Mason 
|-
| Charles Halton || Nolan 
|-
| Louis Jean Heydt || Provost marshal 
|- Byron Barr (Gig Young) || Student pilot 
|-
| Michael Ames (Tod Andrews)|| Student pilot 
|-
| Willie Fung || Willie 
|-
| Carl Harbord || Blake 
|-
| Miles Mander || Churchills voice (offscreen)
|}

==Music==
The music score is by Max Steiner, and Harold Arlen wrote the title song (lyrics by Johnny Mercer) which is used as a march in the film. Later, "Captains of the Clouds" was adopted as an official song of the Royal Canadian Air Force, although its use today is largely ceremonial.  The tune is also known for its use in Carl Stallings music scores for various Warner Bros. Cartoons shorts.

Canadas unofficial national anthem, "The Maple Leaf Forever", by Alexander Muir, is also heard, as well as "O Canada", the de facto Canadian anthem since 1939, and official anthem since 1980.

===Recording=== Decca 4174) by Dick Powell, with Over There as the A side. 

==Production==
During pre-production, Joseph W.G. Clark, the public relations director of the British Commonwealth Air Training Plan was heavily involved in promoting a film project that was initially identified as "Bush Pilots" based on a script submitted by Canadian screenwriters. With RCAF backing, Hal Wallis and Jack Warner were approached in Hollywood to undertake a "patriotic film." Warner was enthusiastic, and began the task of casting a major star to front the project. After considering Raymond Massey, Errol Flynn, and Clark Gable, the decision was made to cast George Brent, but Warner Brothers was unsure whether he could carry the film.  Numerous attempts to rewrite the script into a more acceptable form resulted in a final screenplay now titled "Captains of the Clouds". The name was borrowed from a Victory Loans speech given by Billy Bishop. The only holdup was casting, which was resolved when Warner persuaded its resident "cocky guy" (as producer Jerry Wald had dubbed him), 42-year old James Cagney, to take on the lead male role. Barris 2005, p. 145. 

This film was Cagneys first in Technicolor. His participation in the production has been characterized as reluctant, and he only accepted after his brother was taken on as an associate producer. He quipped, "I didnt like this story the last four times I did it and I dont like it now!", fearing that he was immersed in one of his trademark Warner Brothers "potboilers", playing a role he had reprised numerous times. Yet in certain scenes, Cagney improvised, reverting to his typecast style. His loose interpretation is evident in a cabin scene when he is playing against his cronies. Cagney veers off the script pages, reverting to the cocky persona he had cultivated in countless earlier features. 
 Cenotaph area. 
 Trout Lake Fairchild 71C Waco Waco Custom Cabin Series|EGC-7 and Waco Custom Cabin Series|AGC-8 cabin aircraft provided the other float planes. Metcalfe-Chenail 2009, p. 32. 

Principal photography did not go well; a number of incidents slowed production.  One of the huskies that was key to a scene bit Morgan, opening up a gash on his hand. Cagney, in an uncharacteristic move, decided to forgo a stunt double and play the scene himself in which his character is struck by a whirling propeller. At first things proceeded smoothly, but when it came time for him to fall into the lake, he overdid it and suffered a real concussion, putting the 10-day shoot at North Bay, Ontario farther behind schedule. Weather was a constant challenge, and with the need to ensure continuity, small scenes became unnecessarily complex; as a result, a typical shooting day lasted almost into the night.  One 30-second scene with all the principals running along the dock took an entire day to complete, with Cagney, Hale, and Tobias barely able to stand at the end of filming.  With a Hollywood production in their midst, North Bay residents became such a persistent nuisance that the crew reverted to sending messages out of the location site by homing pigeons.   
 Mountain View. Harvard training aircraft flew overhead in a salute to the graduates. 

The climactic ferry mission was staged out over the Atlantic from RCAF Station Dartmouth using the bases operational Lockheed Hudson bombers, along with a repainted Hawker Hurricane that posed as a German Bf 109 fighter.  Due to the prominent Luftwaffe markings on the RCAF fighter, special alerts had to be posted in order to prevent the "trigger-happy" home defence gunners from shooting down their own aircraft. 

==Reception==
Released in an era of patriotic films with overt propaganda themes, Captains of the Clouds received an enthusiastic public acceptance. Although it was a "Hollywood" production, the film premiered simultaneously on February 21, 1942 in New York, London, Ottawa, Cairo, Melbourne, Toronto, Winnipeg, and Vancouver, with RCAF pilots transporting film copies to all these cities.   The public reaction can be partly attributed to the plot line that revolved around the unique Canadian wilderness and the bush pilot mystique. "So Full of Spectacle and Glory it Had to be Made in Technicolor!" was the ad copy that was used. The vivid aerial scenes filmed in Technicolor were another aspect of the expensive production that garnered critical attention. Reviews were mixed; while some critics felt the film suffered from a stagey plot and a forced romantic story line, the aerial scenes were considered the films redeeming feature. 

==Awards== Ted Smith, Casey Roberts), and Best Color Cinematography at the 1943 Academy Awards. 

==References==

===Notes===
 

===Citations===
 

===Bibliography===
 
* Barris, Ted. Behind The Glory: The Plan that Won the Allied Air War. Markham, Ontario: Thomas Allen & Son Publishers, 2005. ISBN 0-88762-212-7.
* Dunmore, Spencer. Wings For Victory. Toronto: McClelland and Stewart, 1994. ISBN 0-7710-2927-6.
* Captains of the Clouds (DVD). Burbank, California: Warner Home Video, 2007.
* Dolan, Edward F. Jr. Hollywood Goes to War. London: Bison Books, 1985. ISBN 0-86124-229-7.
* Hardwick, Jack and Ed Schnepf. "A Viewers Guide to Aviation Movies". The Making of the Great Aviation Films, General Aviation Series, Volume 2, 1989.
* Mauro, Rudy. "Captains of the Clouds: A Half-Centurys Perspective on the First Canadian Air Epic."  Journal of the Canadian Aviation Historical Society, Vol. 29, No. 2, Summer 1991.
* Mauro, Rudy. "Captains of the Clouds: Filming the Bush Flying Sequences of Canadas First Air Epic." Journal of the Canadian Aviation Historical Society, Vol. 29, No. 3, Fall 1991.
* Mauro, Rudy. "Captains of the Clouds: A Postscript." Journal of the Canadian Aviation Historical Society, Vol. 33, No. 1, Spring 1995.
*  Mauro, Rudy. "Recovering the ‘Cagney Norseman’: Salvaging the Remains of CF-AYO after Four Decades." Journal of the Canadian Aviation Historical Society, Vol. 39, No. 1, Spring 2001.
* Metcalfe-Chenail, Danielle. For the Love of Flying: The Story of Laurentian Air Services. Montreal: Robin Brass Studio, 2009. ISBN 978-1-896941-57-8.
* Orriss, Bruce. When Hollywood Ruled the Skies: The Aviation Film Classics of World War II. Hawthorne, California: Aero Associates Inc., 1984. ISBN 0-9613088-0-X.
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 