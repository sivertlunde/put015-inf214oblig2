Maximum Shame
{{Infobox film
| name           = Maximum Shame
| image          = Maximum Shame - dvd.jpg
| alt            =  
| caption        = DVD cover
| director       = Carlos Atanes
| producer       = Carlos Atanes
| writer         = Carlos AtanesCarlos Atanes
| starring       = Ana Mayo Marina Gatell Ignasi Vidal Paco Moreno Ariadna Ferrer David Castro Eleanor James
| music          = Marc Álvarez
| cinematography = 
| editing        = 
| studio         = 
| distributor    = FortKnox Audiovisual
| released       =  
| runtime        = 81 minutes
| country        = United Kingdom Spain
| language       = English
| budget         = 
| gross          = 
}}

Maximum Shame is a feature-length dystopian movie, written and directed by Carlos Atanes and released in 2010.

==Plot==
The end of the world is imminent. A man goes into a parallel dimension, a limbo between reality and fantasy where the normal rules of time and space have ceased to apply.  His wife goes to rescue him.  Both will be trapped in a strange and cruel world where a ruthless Queen organizes reality as a mad game of chess, a post-apocalyptic dystopia of domination and subjugation where  characters can’t eat, speak or move about freely and are periodically viciously attacked. 

==Notes== transgressive and weird style that characterized Atanes early work.  Shooting, in English, was developed in Spain over just six days, but one scene was made in the UK with British scream queen Eleanor James.  The film was a failure upon release.

==Cast and roles include==
* Ana Mayo
* Marina Gatell
* Ignasi Vidal
* Paco Moreno
* Ariadna Ferrer
* David Castro
* Eleanor James

==Awards==
* BUT Film Festival 2010: nominated for BEST FEATURE MOVIE, Breda, Netherlands. 

* Nominated for WEIRDEST PICTURE of 2011 (Weirdcademy Awards) 

==Notes==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 