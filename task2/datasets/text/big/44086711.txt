Oormakale Vida Tharu
{{Infobox film 
| name           = Oormakale Vida Tharu
| image          =
| caption        =
| director       = Ravi Gupthan
| producer       =
| writer         =
| screenplay     =
| starring       = Srividya Prathap Pothen
| music          = KJ Joy
| cinematography =
| editing        = Ravi
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film,  directed by Ravi Gupthan and produced by . The film stars Srividya and Prathap Pothen in lead roles. The film had musical score by KJ Joy.   

==Cast==
*Srividya
*Prathap Pothen

==Soundtrack==
The music was composed by KJ Joy and lyrics was written by Dr Pavithran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Doore Neelavaanam || Vani Jairam, Jolly Abraham || Dr Pavithran || 
|-
| 2 || Jeevitha Nritham || S Janaki || Dr Pavithran || 
|-
| 3 || Kaarmukil || K. J. Yesudas || Dr Pavithran || 
|-
| 4 || Swapnangalkkarthangal undaayirunnenkil || K. J. Yesudas, Chorus || Dr Pavithran || 
|}

==References==
 

==External links==
*  

 
 
 

 