Knockabout (film)
 
 
 
 
{{Infobox film
| name           = Knockabout
| image          = Knockabout-Poster.jpg
| image_size     =
| caption        = Hong Kong film poster
| director       = Sammo Hung
| producer       = Raymond Chow
| writer         = {{plainlist|
* Louis Lau
* Huang Chik Chin
}}
| starring       = {{plainlist|
* Yuen Biao
* Sammo Hung
* Bryan Leung
}}
| music          = Chen Hsun Chi
| cinematography = Ricky Lau
| editing        = Peter Cheung Golden Harvest
| released       =  
| runtime        =
| country        = Hong Kong
| language       = Cantonese
| budget         =
| gross          = HK $2,830,519.80
}} martial arts comedy film directed by Sammo Hung and starring Hung and Yuen Biao.

==Plot==
The film follows two con artist brothers, Yipao (Yuen Biao) and Taipao (Bryan Leung). One day they are cheated out of their ill-gotten gains in an encounter with Jia Wu Dao (Lau Kar Wing). They try to fight him, to retrieve their money, but are defeated, so they ask him to train them, hoping to become the best fighters in the city. After surpassing the fighting skills of "ordinary people", Yipao soon discovers that Jia Wu Dao is a murderer. When he realises his secret has been revealed, Jia Wu Dao attempts to kill Yipao, but Taipao blocks the fatal blow and is killed in his place. monkey style snake style. As their fight moves outside of the wine shop, the fat beggar and Yipao defeat Jia Wu Dao, killing him with spiked vines, finally avenging Taipaos death. The fat beggar reveals that he is an undercover detective trying to arrest criminals, particularly Jia Wu Dao.

==Cast==
* Yuen Biao as Yipao / Little John
* Sammo Hung as Fat beggar / Blinking beggar
* Bryan Leung as Dai Pao / Big John (as Liang Chia-jen)
* Lau Kar Wing as Jia Wu Dao / Old Fox
* Karl Maka as Captain
* Lee Hoi San as Painter Mars as Tiger (as Fo Sing)
* Chan Lung (Peter Chan) as Banker Wei
* Louis Lau as Banker Weis dad
* Chung Fat as Vegetable hawker / Big Eyes

==Box office==
The domestic Hong Kong theatrical release of Knockabout ran from 12 - 25 April 1979, taking HK $2,830,519. 

==References==
 

==External links==
*  
*  
*   at the  

 

 
 
 
 
 
 
 
 