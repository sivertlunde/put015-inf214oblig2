Nam Yajamanru
{{Infobox film name           = Nam Yajamanru image          = image_size     = caption        = director       = T. S. Nagabharana producer       = Rajakumari Rajshekar writer         = T. S. Nagabharana narrator       = starring  Vishnuvardhan Navya Nair Lakshmi Gopalaswamy Vijay Raghavendra music          = Hamsalekha cinematography = Ramesh Babu editing        = G. Basavaraj Urs studio         = R. R. Creations released       =   runtime        = 140 minutes country        = India language       = Kannada budget         =
}}
 Kannada drama Vishnuvardhan  along with Lakshmi Gopalaswamy, Navya Nair and Vijay Raghavendra in the lead roles.  The film was produced by Rajakumari Rajshekar.

The film released on 27 February 2009 to generally positive reviews from critics.   However, the film failed commercially at the box-office.

==Cast== Vishnuvardhan as Shashank
* Lakshmi Gopalaswamy as Urmila
* Navya Nair as Charulatha
* Vijay Raghavendra as Alok
* Ananth Nag
* Sharath Babu
* Ramesh Bhat
* Mandya Lokesh
* Chitra Shenoy
* Umesh

==Soundtrack==
The music of the film was composed and lyrics written by Hamsalekha. 

{{Infobox album  
| Name        = Nam Yajamanru
| Type        = Soundtrack
| Artist      = Hamsalekha
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Akshaya Audio
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Dundu Bhoomige
| lyrics1 	= Hamsalekha
| extra1        = S. P. Balasubrahmanyam, K. S. Chitra
| length1       = 
| title2        = Yenayithe
| lyrics2 	= Hamsalekha
| extra2        = Hemanth Kumar
| length2       = 
| title3        = Ee Hrudaya
| lyrics3       = Hamsalekha Nanditha
| length3       = 
| title4        = Gombe Mari
| extra4        = S. P. Balasubrahmanyam
| lyrics4 	= Hamsalekha
| length4       = 
| title5        = Ninne Rathri
| extra5        = S. P. Balasubrahmanyam, Badri Prasad
| lyrics5       = Hamsalekha
| length5       = 
| title6        = Vadhu Varare
| extra6        = S. P. Balasubrahmanyam, Anuradha Bhat, Hemanth Kumar, Badri Prasad
| lyrics6       = Hamsalekha
| length6       =
| title7        = Ee Mouna
| extra7        = Anuradha Bhat, K. S. Chitra, Badri Prasad
| lyrics7       = Hamsalekha
| length7       =
}}

==References==
 

==External source==
*  
*  
*  

 
 
 
 
 
 


 

 