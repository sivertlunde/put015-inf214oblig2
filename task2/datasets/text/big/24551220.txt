Brazil (1944 film)
{{Infobox film
| name     = Brazil
| director = Joseph Santley
| image	=	Brazil FilmPoster.jpeg
| caption= A poster bearing the films alternate title: Stars and Guitars
| producer = Robert North
| writer   = Richard English Frank Gill Jr. Laura Kerr Robert Livingston Henry Da Silva Edward Everett Horton Veloz & Yolanda
| music    = Walter Scharf
| cinematography = Jack A. Marta
| editing  = Murray Seldeen
| distributor = Republic Pictures
| released =  
| runtime  = 91 minutes
| country  = United States
| language = English
| budget   =
| gross    =
}} American musical musical comedy film directed by Joseph Santley and starring Tito Guízar, Virginia Bruce and Edward Everett Horton. It is set in Brazil, and involves a composer masquerading as twins, trying to win the hand of an anti-Latin novelist.

==Cast==
* Tito Guízar as Miguel Soares
* Virginia Bruce as Nicky Henderson
* Edward Everett Horton as Everett St. John Everett Robert Livingston as Rod Walker
* Veloz and Yolanda as themselves
* Fortunio Bonanova as Senor Renaldo Da Silva Richard Lane as Edward Graham
* Frank Puglia as Senor Machado
* Aurora Miranda as Bailarina, Specialty Dancer
* Alfredo DeSa as Master of Ceremonies (as Alfred de Sa)
* Henry De Silva as Comerciante
* Rico De Montez as Airport Official
* Leonardo Scavino as Reporter (as Leon Lenoir)
* Roy Rogers as Himself, Roy Rogers Trigger as Trigger, Roys Horse
* Billy Daniel as Dancer (as Billy Daniels)

==Awards==
The film was nominated for three Academy Awards:   

* Music (Scoring of a Musical Picture)
* Music (Song) Sound Recording (Daniel J. Bloomberg)

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 
 