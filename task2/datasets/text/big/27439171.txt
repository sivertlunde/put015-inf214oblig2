Monkeys, Go Home!
{{Infobox film
| name           = Monkeys, Go Home!
| image          = Monkeys, Go Home! poster.jpg
| caption        = Theatrical release poster
| director       = Andrew V. McLaglen
| producer       = Walt Disney
| screenplay     = Maurice Tombragel 	 Dean Jones
| music          = Robert F. Brunner 
| cinematography = William E. Snyder
| editing        = Marsh Hendry 	 Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $3,000,000 (US/ Canada) 
}} chimp labor, upsetting the other workers. Hank eventually gains the towns confidence with the kind aid of Father Sylvain and his neighbor Maria Riserau.

==Cast==
*Maurice Chevalier as Father Sylvain Dean Jones as Hank Dussard
*Yvette Mimieux as Maria Riserau
*Bernard Woringer as Marcel Cartucci
*Clément Harari as Emile Paurilis

==Trivia==
The pig head ornament is referenced back in the film 12 Monkeys (1995), at the environmentalists headquarters. 

This is the first film produced by Walt Disney Productions after its founder death. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 