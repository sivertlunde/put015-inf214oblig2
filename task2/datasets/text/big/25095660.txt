Amaidhi Padai
{{Infobox film
| name           = Amaidhi Padai
| image          = AmaidhiPadaifilm.jpg
| image_size     = 
| alt            = 
| caption        = DVD Cover
| director       = Manivannan
| producer       = K. Balachandran U. K. Senthikumar K. N. Ilamurugan
| writer         = Manivannan
| narrator       =  Sujatha Kasthuri Kasturi
| music          = Ilaiyaraaja
| cinematography = D. Shankar
| editing        = P. Venkateshwara Rao
| studio         = M. R. Films International
| distributor    = M. R. Films International
| released       =  
| runtime        = 160 minutes
| country        = India
| language       = Tamil
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Amaidhi Padai ( . Sathyaraj played a dual role as father and son in the film which had Ranjitha playing the female lead. The story revolves around an ordinary man who builds his political career through short routes. This film is touted as one of Sathyarajs best performances till date. The film was followed by the sequel Nagaraja Cholan MA, MLA in 2013 also starring Naam Tamilar Katchis Seeman. It went on to run more than 25 weeks and declared a super hit movie.

==Cast==
 
*Sathyaraj as Amavasai (Nagaraja Cholan)/Thangavel
*Manivannan as Mani
*Ranjitha as Kuyili Kasthuri as Thayamma Sujatha as Sivakami
*R. Sundarrajan (director)|R. Sundarrajan
*S. S. Chandran
*Malaysia Vasudevan as Kuyilis father Thyagu
*Ganthimathi
*C. R. Saraswathi as Kuyilis mother
*Baby Aarthi as Kuyilis sister
*Vichithra
*Brinda
*Vahini Indraja
*Meesai Murugesan as Sivakamis father
*Halwa Vasu
*S. K. Muthukumar
*Viswanath
*Tirupur Ramasamy
*Vellai Subbaiah
*Rambo Rajkumar
 

==Plot==
Amavasai (Sathyaraj),homeless but egoistic, gets acquainted to politician Mani (Manivannan). Amavasai helps Mani with some petty tasks during election campaign and meetings. Amavasai grows into a well acclaimed helper for Mani. Meanwhile Amavasai falls in love with thayamma(kasthuri (actress)|Kasthuri). Mani instructs Amavasai to stand for MLA election on his party ticket. Amavasai wins and becomes the MLA. He forgets his relationship with Kasthuri and rejects her pregnancy and child. Also he rejects Manis help and offers him a role under him. He changes his name to Nagaraja Cholan for gaining status. He offers to marry the daughter of local landlord, Sivakami (Sujatha (actress)|Sujatha) and even cheats him of his Palace. Over years Amavasai grows into highly influential and corrupt politician with high criminal affluence. He builds a strong political empire through shortcuts. His ego and pride to remain in power make him kill many. Amavasais son Thangavel (also Sathyaraj) through Kasthuri comes out as a police constable who is deputed in Amavasais constituency. Thangavel comes to know of his real father and about his betrayal of his Mother and also his corrupt practices. Thangavel seeks revenge. Meanwhile Amavasai as an MLA is accused of many illegal practices. The story further revolves around how Amavasai and Thangavel proceed in their battle and finally who wins the battle.

==Production==
Sathyaraj was enjoying a good success as lead actor, when his friend Manivannan narrated the script of "Amaidhipadai" to Sathyaraj he rejected the script as he was not interested to play negative role but after being impressed by narration he accepted to play the role. 

==Soundtrack==
The music composed by Ilaiyaraaja while lyrics written by Vaali (poet)|Vaali, Pulamaipithan and Ponnadiyan.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track# !! Song !! Artist(s)
|-
| 1
| Sollividu Velli Nilave
| Mano (singer)|Mano, Swarnalatha
|-
| 2
| Enakku Unnai Ninaicha
| Swarnalatha
|-
| 3
| Ada Naan Aatchu Mano
|-
| 4
| Muthumani Ther Irukku 
| Mano (singer)|Mano, S. Janaki
|-
| 5
| Vetri Varuthu
|  Mano (singer)|Mano, SN. Surendar, Deepan Chakravarthy
|-
| 6
| Amma Thaaye 
| Illayaraja
|-
| 7
| Ippadai Thorkin Eppadai Vellum 
| 
|}

==Reception==
Amaidhi Padai emerged victorious among the Pongal releases of 1994, winning the race against other successful films like Kamal Haasans Mahanadi (film)|Mahanadi and Vijayakanths Sethupathi IPS. It was praised by film critics who declared the film an Asathal Padai.

==Remakes==

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" class=sortable
|- bgcolor="#CCCCCC" align="center"|-

! Year
! Film
! Language
! Cast
! Director
! Verdict
|-
| 1994
|M. Dharmaraju M.A. Telugu cinema|Telugu Mohan Babu, Sujatha (actress)|Sujatha, Surabhi Raviraja Pinisetty Hit
|-
| 1995
| Jallaad Hindi cinema|Hindi Mithun Chakraborty, Moushmi Chatterjee, Rambha (actress)|Rambha, Madhoo	 T L V Prasad
| Hit
|-
|}

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 