Attack in the Pacific
{{Infobox film
| name           = Attack in the Pacific
| image          = 
| image_size     = 
| caption        = 
| director       = Frank Capra
| producer       = 
| writer         = 
| narrator       = 
| starring       = See below
| music          = David Raksin
| cinematography = 
| editing        = 
| distributor    = 
| released       = 
| runtime        = 52 minutes 
| country        = United States
| language       = 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Attack in the Pacific (also known by the American series title: Armed Forces Information Film: A.F.I.F. Number 3) is a 1944 American war documentary film.

== Plot summary ==
 

== Cast ==
*Henry H. Arnold as Himself (with Marshall) (archive footage)
*Alan Brooke as Himself (at Cairo Conference) (archive footage)
*Chiang Kai-shek as Himself (at Cairo Conference) (archive footage)
*Winston Churchill as Himself (at Cairo Conference) (archive footage) Andrew Cunningham as Himself (at Cairo Conference) (archive footage)
*John Dill as Himself (at Anglo-American conference) (archive footage) James Doolittle as Himself (walks deck of Hornet with Mitscher) (archive footage)
*William F. Halsey as Himself (on deck) (archive footage)
*Hastings Ismay as Himself (at Cairo Conference) (archive footage)
*Ernest J. King as Himself (at Cairo Conference, in USMC uniform) (archive footage)
*William D. Leahy as Himself (at Cairo Conference) (archive footage) Wei Liu as Himself (at Cairo Conference) (archive footage)
*George C. Marshall as Himself (at Anglo-American conference) (archive footage)
*Marc A. Mitscher as Himself (commander, USS Hornet) (archive footage)
*Louis Mountbatten as Himself (at Cairo Conference) (archive footage)
*Chester W. Nimitz as Himself (decorates soldier) (archive footage)
*Charles Portal as Himself (at Cairo Conference) (archive footage)
*Lewis B. Puller as Himself (on Peleliu, holds helmet) (archive footage)
*Sam Rayburn as Himself (in Congress behind FDR) (archive footage)
*Franklin Delano Roosevelt as Himself (Day of Infamy speech) (archive footage)
*James Roosevelt as Himself (in Congress beside FDR) (archive footage)
*William H. Rupertus as Himself (on Peleliu, beside Puller) (archive footage)
*Chen Shang as Himself (at Cairo Conference) (archive footage)
*Raymond A. Spruance as Himself (studies map with Nimitz) (archive footage)
*Joseph W. Stilwell as Himself (at Cairo Conference) (archive footage) Henry Wallace as Himself (in Congress behind FDR) (archive footage)
*David Raksin

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 


 