Exposed (1983 film)
{{Infobox film
| name           = Exposed
| image          = Exposed (movie poser).jpg
| alt            = 
| caption        = 
| director       = James Toback
| producer       = Serge Silberman
| writer         = James Toback
| narrator       =  Iman
| music          = Georges Delerue
| cinematography = Henri Decaë Robert Lawrence
| studio         = United Artists
| distributor    = Metro-Goldwyn-Mayer|MGM/UA Entertainment Company
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1,352,083
| preceded by    = 
| followed by    = 
}}
Exposed is an English language|English-language 1983 film directed and written by James Toback. Nastassja Kinski, Rudolf Nureyev and Harvey Keitel star. 

==Plot==

A college girl from Wisconsin whose professor has romantic designs on her, Elizabeth Carlson packs up and moves to New York City, finding a job as a waitress while she attempts to launch a career as a fashion model.

As her career takes off, she meets Daniel Jelline, a violinist, who aggressively stalks Elizabeth until they begin an affair. When work takes her to Paris, however, Elizabeth encounters a terrorist named Rivas and her life is placed in considerable danger.

==Cast==
* Nastassja Kinski as Elizabeth Carlson
* Rudolf Nureyev as Daniel Jelline
* Harvey Keitel as Rivas
* Ian McShane as Greg Miller
* James Russo as Nick
* Bibi Andersson as Margaret
* Ron Randell as Curt
* Tony Sirico as Record Store Thief Iman appear as Models

==References==
{{Reflist|refs=
 {{Citation
 | last1     = Weldon
 | first1    = Michael J.
 | title     = The Psychotronic Video Guide To Film
 | publisher = Macmillan
 | page      = 193
 | year      = 1996
 | isbn      = 0312131496
 | url       = http://books.google.com/books?id=nhjsnWfFoiAC&pg=PA193
 | postscript= .
}} 
}}

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 