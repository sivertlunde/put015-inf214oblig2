Aada Paduchu
{{Infobox film
| name           = Aada Paduchu
| image          = Aada Paduchu.jpg
| image_size     =
| caption        =
| director       = K. Hemambharadhara Rao
| producer       = K. Hemambharadhara Rao
| writer         = L. V. Prasad   (story)  
K. Pratyagatma (dialogues ) 
| narrator       =
| starring       = N.T. Rama Rao Chandrakala Shobhan Babu Relangi Venkata Ramaiah Vanisree Geetanjali 
| music          = T. Chalapathi Rao
| cinematography = 
| editing        = B. Gopala Rao
| studio         = Prasad Studios
| distributor    =
| released       = 1967
| runtime        = 162 mins.
| country        = India Telugu
| budget         =
}}
Aada Paduchu or Aadapaduchu ( ) is a 1967 Telugu Drama film produced and directed by K. Hemambaradhara Rao. It is remake of 1959 Hindi film Chhoti Bahen directed by L. V. Prasad.

==The Plot==
The story is same as Chhoti Bahen. It is based on strong family bondage between two brothers Satyam (NTR) and Shekhar (Sobhan Babu) and their sister Sharada (Chandrakala).

==Cast==
* Nandamuri Taraka Rama Rao	... 	Satyam
* Shobhan Babu	... 	Shekhar
* Chandrakala	... 	Sharada
* B. Padmanabham		
* Vanisree		
* Relangi Venkataramaiah
* Geetanjali(Actress)
* Chadalavada Kutumba Rao
* Haranath (actor) Krishna Kumari
* Nagabhushanam
* Radha Kumari

==Crew==
* Director and Producer : K. Hemambharadhara Rao
* Production company : Subhashini Art Pictures
* Story : L. V. Prasad
* Dialogues : K. Pratyagatma
* Director of Photography : M. G. Singh - M. C. Sekhar
* Film Editor : B. Gopala Rao
* Choreography : K. S. Reddy
* Art Director : B. N. Krishna
* Lyrics: Dasaradhi, Arudra, C. Narayana Reddy, Srirangam Srinivasa Rao, Kosaraju Raghavaiah
* Music Director : T. Chalapathi Rao
* Playback singers : Ghantasala Venkateswara Rao, P. Susheela, Madhavapeddi Satyam, T. R. Jayadev, B. Vasantha
* Studio : Prasad Studios
* Distributors : Purna Pictures Private Limited

==Soundtrack==
* Anna Nee Anuraagam Yenno Janmala Punyaphalam (Singer: P. Susheela)
* Gaaradi Chese Kannulatho (Singers: T. R. Jayadev and P. Susheela)
* Madhi Thulli Thulli Yegirindi (Lyrics: Arudra; Singers: P. Susheela and group)
* Prema Pakshulam Manam Yevaremanna Ika Vinam (Singers: Madhavapeddi Satyam and B. Vasantha)
* Idhena Dayaleni Lokana Nyayam Idhena (Singer: P. Susheela)
* Rikshavaalanu Nenu Pakshilaga Pothanu (Lyrics:  )

==Boxoffice==
* The film ran for more than 100 days in five centers in Guntur, Vijayawada, Kakinada, Rajahmundry and Vizag.

==External links==
*  

 
 
 
 
 
 


 