Mothers Cry
 

{{Infobox film
| name           = Mothers Cry
| image          = Mothers_Cry_1930_Poster.jpg
| caption        = Theatrical poster
| director       = Hobart Henley
| producer       = Robert North
| writer         = Novel: Helen Grace Carlisle Screenplay: Lenore J. Coffee
| starring       = Dorothy Peterson Helen Chandler David Manners Evalyn Knapp Sidney Blackmer David Mendoza Erno Rapee
| cinematography = Gilbert Warrenton
| editing        = Frank Ware
| distributor    =  
| released       =  
| runtime        = 76 minutes
| country        = United States English
| budget         =
}}

Mothers Cry  (1930 in film|1930) is an talkie|all-talking Pre-Code Hollywood|pre-code drama film released by First National Pictures, a subsidiary of Warner Bros. and directed by Hobart Henley. The movie stars Dorothy Peterson, Helen Chandler, David Manners, Evalyn Knapp and Sidney Blackmer. The film is based on the popular novel of the same name written by Helen Grace Carlisle.   

==Synopsis==
The film is focused on the life of Mary Williams (Dorothy Peterson), a widowed mother, and her struggles to raise her four children. Her oldest son, Daniel (Edward Woods), torments his siblings and mother throughout childhood and eventually grows up to be a criminal. Arthur (David Manners), the younger son, grows up to be a successful architect. Jennie (Evalyn Knapp), one of the daughters, loves domestic work and home-life and is courted by a wealthy older gentleman Karl Muller (Reinhold Pasch). Beattie (Helen Chandler), the other daughter, grows up to be an idealistic dreamer. 

Daniel gets into trouble one day with some gangsters for double crossing them and is beaten up. He leaves home and disappears for three years. Soon after Jennie is married to Muller. Daniel returns home with a moll, whom he introduces as his wife. Detectives trail him to his mothers house as a suspect to a hold-up. Daniel later reappears at the house with a blackmail scheme and ends up shooting and murdering Beattie, his own sister. He is convicted of her cold blooded murder and is sent to the electric chair. As the film ends, Mary finds consolation in her two remaining children.

==Preservation==
A censored version of this film survives, prepared for re-release after June 1934 to remove Pre-Code Hollywood|pre-code material, which is missing at least two minutes of footage. This version has been broadcast on television and cable. The film has been released on DVD by the Warner Archive Collection.

==Cast==
*Dorothy Peterson as Mary Williams
*Helen Chandler as Beattie Williams
*David Manners as Arthur Artie Williams
*Evalyn Knapp as Jenny Williams
*Sidney Blackmer as Mr. Gerald Hart
*Edward Woods as Daniel Danny Williams
*Jean Laverty as Sadye Noonan Williams Pat OMalley as Frank Williams

==External links==
* 

 
 
 
 
 
 