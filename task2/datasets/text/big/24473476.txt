The Traveling Executioner
{{Infobox film
| name           = The Traveling Executioner
| image          = The Traveling Executioner.jpg
| caption        =
| director       = Jack Smight
| producer       = Jack Smight
| writer         = Garrie Bateson
| starring       = Stacy Keach Bud Cort Stefan Gierasch Marianna Hill
| music          = Jerry Goldsmith
| cinematography = Philip H. Lathrop
| editing        = Neil Travis
| distributor    = MGM
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
}}

The Traveling Executioner is a 1970 American film starring Stacy Keach, Bud Cort, Stefan Gierasch and Marianna Hill and directed by Jack Smight. 

==Plot==
Jonas Candide, a former carnival showman, travels around the South in 1918 with his own portable electric chair, going from prison to prison with his young assistant, Jimmy, charging one hundred dollars per execution. Two of Jonas potential victims are siblings Willy and Gundred Herzallerliebst. While Jonas successfully executes Willy, he falls for Gundred, hoping to fake her execution.

The musical The Fields of Ambrosia is based on the film. 

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 


 