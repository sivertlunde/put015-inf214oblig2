Tottie: The Story of a Doll's House
{{Infobox television
| show_name            = Tottie&nbsp;– The Story of a Dolls House
| image =
| caption =
| genre                = Animation Childrens Drama
| runtime              = 15 minutes
| creator              = Oliver Postgate
| voices               =  
| producer             = Oliver Postgate
| theme_music_composer =
| company              = Smallfilms
| opentheme            =
| composer             =
| country              = United Kingdom
| language             = English
| picture_format       =
| audio_format         =
| network              = BBC
| first_aired          = 1984
| last_aired           = 1986
| num_seasons          =
| num_episodes         = 10
| list_episodes        =
| preceded_by          = What-A-Mess
| followed_by          = Pinnys House
| related              =
| website              =
}}
 Victorian Dolls House belonging to sisters Emily and Charlotte Dane.
 dark edge as the dolls had to wish very hard that good things would happen and they would not fall on misfortune.  The series started with the phrase "Dolls are not like people. People choose, but dolls can only be chosen".

==Plot and characters==
The dolls were a family consisting of:
*Mr Plantaganet, who was clumsy, timid and spent his day reading his newspaper
*Birdie, his happy wife, an inexpensive celluloid doll from a Christmas cracker
*Apple, their son, a plush velvet baby boy doll, no bigger than your thumb, who often spoke in rhyme
*Darner the dog, made of clipped wool with a darning needle for a backbone. farthing doll called Tottie, who was thoughtful, sensible and kept them all together.   Mr Plantagenet, Birdie, Tottie and Apple all consider themselves a family and the series started with them living in a "Drafty Shoe Box".  Only Tottie remembered living in a real dolls house a long, long time ago and the rest of the dolls urge her to tell them all about it, a sitting room with real wallpaper, a fire of shining red paper, a lamp with a white china shade "that would really light up if you used a birthday candle".
 inherit a dolls house from their great-aunt, the group are moved in.  The house had a green front door with a knocker and six steps.  When the Plantagenets and Tottie first move in they cannot believe their luck at finding such a splendid and well-equipped home.  Later, the sisters are later given an antique china doll called Marchpane, a very grand doll, clothed in lace, with eyes of the finest blue glass, which they place in the house with the Plantagenets.  Marchpane had originally been with the dolls house but was sent away for cleaning prior to being in an exhibition.  She is selfish, nasty and schemes to become foremost in Emily and Charlottes affection.  She loathes Birdies pleasant and trusting nature, detests Mr Plantagenets weakness, and does her best to turn Apple against his mother.

Having gained the affection of the Dane sisters, Marchpane uses the dolls house lights, which contain real paraffin, to start a fire, and trap Apple.  She guessed that Birdie would try to save her little boy, and knew that as Birdie is made of celluloid she would quickly burn. Birdie saved Apple, but as schemed by Marchpane is incinerated.  Birdie loved it when she was burning as she was too simple and optimistic to know what was going on and loved how shiny she was.  While discussing her demise, Tottie remarked that "Birdie did look Beautiful in the Flame" and Mr Plantaganet agreed.

In the final sequence Marchpanes owner decides she is too nice to play with, and puts her away in a box.

==Second series==
There were two series of Tottie: The Story of a Dolls House (1984), of which the BBC retained copies, and The Dolls Wish (1986) which, as it was transmitted from VT, the BBC did not keep. Both series were classed as purchased programmes, which the BBC was not required to archive.  Smallfilms did not retain their own copies and therefore, although there may be a few low-grade VHS copies in private hands, there are no copies that may now be broadcast.

==Background==
Rumer Godden wrote the original childrens book, shortly after the Second World War.  It is unusual in the sensitive way it exercises childrens emotional muscles. Marchpane still sends shivers down the spine and the shocking fate of Birdie startles the reader with its reality.  Initially Godden disliked the idea of Oliver Postgate reproducing the book. 

Goddens publisher, Kaye Webb, founder of Puffin Books, did not see the point of the exercise either, asking "Can you really make a film in which the real life adults not only say nothing but dont even move, whilst the dolls speak and come to life?"  If these hurdles were not enough, the BBC shied away from the directness of a film that showed on screen the destruction of a favourite character. Initial doubts were replaced with enthusiasm and Godden took part and persuaded her publisher, her literary agent and his family, as well as Peter Firmin and Oliver Postgate, to take on the roles of the real life characters. The film was bought by Goldcrest and sold to the BBC.

The first series was originally transmitted in February 1984, and repeated in October/November 1985 and September 1988.  The second series was originally transmitted in September/October 1986 and repeated in September 1987 and December 1991/January 1992, making it a very rare example of a series transmitted as late as the 1990s on BBC TV which no longer exists in broadcast quality.

==Production staff==
 
*Peter Firmin: puppets and settings
*Josie Firmin: doll dressing
*David Heneker: music
*Kaye Webb: producer
 

==References==
 

==External links==
* 
*  at The Dragons Friendly Society
* 
*  – YouTube
*  – YouTube

 

 
 
 
 
 
 
 
 