Makers of Melody
Makers of Melody is a short film subject from 1929 that showcased the hit song Manhattan (song)|Manhattan.  Manhattan was Rodgers and Harts first hit song.  The song was sung by Ruth Tester and Allan Gould.

==External links==
*  
*Ruth Tester singing the Rodgers and Hart song, "Manhattan" in the short "Makers of Melody" with Allan Gould http://www.youtube.com/watch?v=NPIgQdOoEV0
*Manhattan "Ill Take Manhattan" 78 rpm and cylinder recording https://archive.org/details/RodgersHartInezCourteney-ManhattanMakersOfMelody1929

 
 