RPM (film)
 
RPM (also known as R.P.M.) Is a 1998 action film starring David Arquette, Emmanuelle Seigner, and Famke Janssen. It was shot in 1997 and first released on video in Germany on June 23, 1998. An earlier unrelated film with the same title, R. P. M., was released in 1970.

==Plot==
 without any kind of power source. Charkos, afraid that the mass production of the car would destroy his oil empire, offers him 1 billion dollars to steal it. But with the police detective on his tail, Biggerman, the man who built RPM, and also his sister Claudia (Famke Janssen), who wants the money, on his tail, he will find it very difficult to steal the car. Along the way, he meets Charkos girl (Emmanuelle Seigner), who wants her car back (which Luke has stole, but Claudia took it), and she helps him steal the RPM in exchange of bringing her car back, and the two eventually start a romance.

==Cast==
* David Arquette ... Luke Delson 
* Emmanuelle Seigner ... Michelle Claire
* Famke Janssen ... Claudia Haggs
* Steve John Shepherd ... Rudy 
* Stephen Yardley ... Chiarkos
* Kenneth Cranham ... Biggerman
* John Bluthal ... Grinkstein

==Production==

The film was mostly shot in the forests of France and in the Nice. The production ended in the summer of 1997. The film was recognized for many famous cars in the movie.

==External links==
*  ]

 