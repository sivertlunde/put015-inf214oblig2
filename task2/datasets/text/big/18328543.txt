Ten Dead Men
 
 
{{Infobox film
| name           = Ten Dead Men
| image          = Ten-dead-men.jpg
| caption        = Theatrical release poster
| director       = Ross Boyask
| producer       = Phil Hobden
| screenplay     = Chris Regan
| story          = Ross Boyask & Phil Hobden
| starring       = Brendan Carr, Terry Stone, Doug Bradley, Pooja Shah, Ben Shockley, Lee Latchford-Evans, JC Mac, Tom Gerald
| music          = Scott Benzie
| cinematography = Darren Berry
| editing        = Ross Boyask
| distributor    = IFM Worldwide Releasing LLC
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         = US$250,000
}} Left for Modern Life?) Ten Dead Men features many of the same actors as its predecessor. Ten Dead Men was directed by Ross Boyask and produced by Phil Hobden. The film released in UK, France, Indonesia and Japan.

==Synopsis==
As the film begins, Ryan has spent years putting his brutal past behind him—a different man now to the stone cold killer he was a lifetime ago. But when an old face from the past arrives on his doorstep, Ryan is called upon to repay a blood debt from years ago. But the price is too high. Betrayed, and with his life falling apart around him, Ryan goes on a murderous, bloody revenge spree against the Ten Men who took his life away from him.

==Production==
It took 18 months from production starting to finishing filming.  The films action was handled by Hong Kong fight director/stunt man Jude Poyer who has worked alongside actors such as Jet Li and Jean-Claude Van Damme on a host of US and Asian action films.

The film stars actor/martial artist Brendan Carr, Terry Stone, Pooja Shah, Ben Shockley, Lee Latchford-Evans, JC Mac, Tom Gerald, and P.L Hobden. Doug Bradley provides the films voiceover narration.
 MMA fighter Kimo Leopoldo|Kimo, Dave Legeno, Cage Rage promoter Dave ODonnell, Silvio Simac, Cecily Fay and Glenn Salvage.

==Cast==
{| class="wikitable"
|- bgcolor="#efefef"
! Actor !! Role
|-
| Brendan Carr (actor) || Ryan
|-
| Pooja Shah || Amy
|-
| Doug Bradley || Narrator
|-
| Ben Shockley || Keller
|- John Rackham || Axel
|-
| Keith Eyles || Projects Manager
|-
| JC Mac || Parker
|-
| Jason Lee Hyde || Garrett
|-
| Tom Gerald || Bruiser
|-
| Lee Latchford-Evans || Harris
|-
| Jason Maza || Boulder
|-
| Silvio Simac || Loomis
|}

==Critical reaction==
The production of Ten Dead Men was tracked in regular articles in Impact Magazine, written by the films producer Phil Hobden. Impact Magazine was given the first review of the film.

"This is passionate genre filmmaking inspired by… Rodriguez and the hard-hitting but playful style of Quentin Tarantino"
Richard Hawes, Facebook Movies

"The film easily clubs aside any British alternative… this is revenge actually, a dish served cold and bloody, littered with plenty of limbs and shrapnel"
Rich Badley, Close-Up Film

"Fusing action, drama and violence into a blistering whole, Ten Dead Men shows us Brits can hold our own in the action arena."
Andrew Skeats

"…well directed, great writing, incredible fight scenes and a star making performance from Carr all adding up to one of the best British action movies of the decade. "
Will Strong, Combat Magazine

==Locations==

Ten Dead Men is set in an unnamed fictional town, but in reality was filmed mostly in London and Brighton. Notable locations in the film included Bovingdon, Buckinghamshire and in and around the streets and the seafront of Brighton   IMDb 

== References ==
 

==External links==
* 
* 

 
 
   
 
 