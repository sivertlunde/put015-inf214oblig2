The Dragon Chronicles – The Maidens
 
 
{{Infobox film name = The Dragon Chronicles – The Maidens image = The Dragon Chronicles - The Maidens.jpg caption = DVD cover art traditional = 新天龍八部之天山童姥 simplified = 新天龙八部之天山童姥 pinyin = Xīn Tiān Lóng Bā Bù Zhī Tiān Shān Tóng Lǎo}} director = Andy Chin producer = Jimmy Heung story = Louis Cha screenplay = Charcoal Tan starring = Brigitte Lin Gong Li Sharla Cheung Frankie Lam Norman Chu music = Violet Lam cinematography = Poon Hang-sang Ray Wong Yim Wai-gwaan editing = Kam Ma studio = Wins Film Co. distributor = released =   runtime = 97 minutes country = Hong Kong language = Cantonese budget = gross = HK$6,529,579.00
}} Louis Chas novel Demi-Gods and Semi-Devils. The films plot centers on the story of Hui-juk, one of the three protagonists of the novel. The film was directed by Andy Chin and starred Brigitte Lin, Gong Li, Sharla Cheung, Frankie Lam and Norman Chu.

==Cast==
: Note: Some of the characters names are in Cantonese romanisation.

* Brigitte Lin as Lei Chau-shui / Lei Chong-hoi
* Gong Li as Mou Hang-wan (Tin-san Tung-lo)
* Sharla Cheung as Ah-tsi
* Frankie Lam as Hui-juk
* Norman Chu as Ting Chun-chau
* Ku Tin-yi
* Liu Kai-chi as So Sing-ho
* James Pak as Tai-hung

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 


 