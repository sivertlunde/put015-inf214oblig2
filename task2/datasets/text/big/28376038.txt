Sexton Blake and the Hooded Terror
{{Infobox film
| name           = Sexton Blake and the Hooded Terror
| image          =
| image_size     =
| caption        = George King George King (producer)
| writer         = Pierre Quiroule (story "The Mystery of Caversham Square") A.R. Rawlinson (writer)
| narrator       =
| starring       = See below
| music          = Jack Beaver Bretton Byrd
| cinematography = Hone Glendinning
| editing        = John Seabourne Sr.
| distributor    =
| released       =
| runtime        = 70 minutes
| country        = UK
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 British crime George King George Curzon, Tod Slaughter and Greta Gynt. 

== Plot summary ==
The film - described as the best in the Blake series of 30s movies   - features the character of Sexton Blake and his efforts to defeat a major crime organisation headed by Michael Larron, a sort of Moriarty figure. 

== Cast == George Curzon as Sexton Blake
*Tod Slaughter as Michael Larron
*Greta Gynt as Madamoiselle Julie
*Tony Sympson as Tinker Charles Oliver as Max Fleming
*Marie Wright as Mrs. Bardell David Farrar as Granite Grant
*Norman Pierce as Inspector Bramley
*H.B. Hallam as Monsieur Bertrand
*Bradley Watts as Paul Duvall

== References ==
 
== External links ==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 
 