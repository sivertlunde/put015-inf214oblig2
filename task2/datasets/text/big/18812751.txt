Desert of Blood
{{Infobox film
| name           = Desert of Blood
| image_size     = 
| image	=	Desert of Blood FilmPoster.jpeg
| alt            = 
| caption        = 
| director       = Don Henry
| producer       = Drew Brody
| writer         = Don Henry
| starring       = Justin Quinn Brenda Romero Naím Thomas Mike Dusi Annika Svedman Flint Esquerra Tori White Natalie J. Horton Yvonne Rawn
| music          = Dean Harada Jason Moss
| cinematography = Pablo Santiago-Brandwein
| editing        = Matthew McArdle
| studio         = Encantado Films Thats Hollywood
| distributor    = Gravitas Ventures
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Desert of Blood is a 2008 independent film about a vampire, Luis Diego, who has been left imprisoned underground by the village priest. When accidentally released, he begins to exact revenge on the villagers. However, when he falls in love with a young woman who is visiting the village to care for her aging aunt, he must face the possibility of forgiveness.
The story is set in modern-day Mexico. 

This film features a largely Latino cast, and was filmed in the Mexican town of Tecate. 

==Plot==
For thirty-five years, something evil lay buried beneath the sands of Mexico until a hapless treasure hunter digs up more than he expects, releasing the mysterious, handsome, and deadly Luis Diego (Justin Quinn) from his desert tomb. Finally free, Diego seeks to exact vengeance on those who nearly destroyed him all those years ago, including the woman he once loved, Sarita (Yvonne 
Rawn).

Diego’s revenge, however, is unexpectedly interrupted when he falls for Sarita’s niece, Maricela 
(Brenda Romero). He sees in her a second chance – a chance to defeat the darkness inside him and to lead a normal life. But torn between the love in his heart and the evil in his soul, Diego soon discovers that his thirst for blood cannot be denied. With a growing rage, Diego succumbs to his wicked hunger and resumes his quest for vengeance, determined to leave nothing in Mexico but a 
Desert of Blood. 

==Cast==
*Agustin Buñuel ... Tomas
*Justin Quinn ...  Luis Diego 
*Brenda Romero ...  Maricela 
*Naím Thomas ...  Cris 
*Mike Dusi ...  Carlos 
*Annika Svedman ...  Amy 
*Flint Esquerra ...  Father Hernandez 
*Tori White ...  Samantha 
*Natalie J. Horton ...  Heather 
*Yvonne Rawn ...  Sarita

== References ==
 

== External links ==
*  
*  

 
 
 
 