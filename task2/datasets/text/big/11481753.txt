Sri (2005 film)
{{Infobox film
| name           = Sree
| image          = Sri Telugu DVD.jpg Dasarath Satyanand Gopimohan Kona Venkat Tamanna Mohan Babu Sukanya Dasarath
| Lakshmi Prasanna
| editing        = Gowtam Raju
| studio         = Sree Lakshmi Prasanna Pictures
| cinematography = Arun Kumar
| music          = Sandeep Chowta
| released       = 3 December 2005
| language       = Telugu
| budget         = 3 crores
}}
 2005 Telugu Telugu  Tamanna and Mohan Babu. This film was directed by Kondapalli Dasarath Kumar|Dasarath.

==Plot==
Bhikshapati (Devaraj) is the cruellest landlord in Rayalaseema. He is on his way to hunt down the family of Sandhya (Tamanna) who stay in Bhuvaneswar. Sree (Manoj) grows up in Bhuvaneswar with his widowed mother (Sukanya). Sree falls in love at first sight with Sandhya. When things are getting right between Sandhya and Sree, Bhikshapatis men enter Bhuvaneswar. Sree rescues the family of Sandhya from Bhikshapatis men. Then he also finds out that there is some connection between his father and Bhikshapatis men. The rest of the story is about how he returns to Rayalaseema and rescues the people of village from the clutches of the feudal landlord Bikshapati.

== Cast ==
* Manoj Manchu ... Sriram Tamanna ... Sandhya
* Mohan Babu ... Basawadu (special appearance)
* Devaraj ... Bhikshapati
* Sukanya ... Srirams mother Jeeva
* Raghu Babu
* Giri Babu
* Jaya Prakash Reddy
* Paruchuri Venkateswara Rao
* Brahmanandam
* Dharmavarapu Subrahmanyam Sunil
* Subhashini
* Banerjee
* Ahuti Prasad

== Crew == Dasarath ... Story
* Satyanand ... Rachana
* Gopimohan ... Screenplay
* Kona Venkat ... Dialogues

== External links ==
*  

 

 
 
 


 