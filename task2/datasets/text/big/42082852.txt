Success (2003 film)
{{Infobox film
| name           = Success
| image          = 
| caption        =
| director       = Suresh Prasanna
| producer       = Isakki Sundar
| story          = 
| screenplay     =  Dushyanth Sonia Agarwal Nandhana Deva
| cinematography = 
| editing        =
| studio         = 
| distributor    = 
| released       =  
| country        = India
| budget         =
| runtime        = 
| language       = Tamil
}} Tamil drama film written and directed by Suresh Prasanna. The film featured newcomer Dushyanth in the lead role, a grandson of actor Sivaji Ganesan, while Sonia Agarwal and Nandhana played supporting roles. The film released in September 2003 to average reviews from critics.    

==Cast== Dushyanth as Ganesh
*Sonia Agarwal as Swetha Nandana as Maha Roja as Radhika
*Karunas Urvashi
*Y. Gee. Mahendra
*Riyaz Khan
*Abhinay
*Jennifer in an item number

==Production== Prabhu and cousin Vikram Prabhu.    Dushyanth was also credited as Junior Sivaji during the production of the film. The film was named Success, after the first on screen dialogue of Sivaji from the film Parasakthi (1952 film)|Parasakthi (1952).      

==Release==
The film opened to mixed reviews with a critic noting "the movie surprises us with a nice turn of events. It definitely energizes the movie and gets us interested again. Looking back on the first half after this, we realize that the movie has actually been quite clever in setting up stock characters but in a slightly different way that makes the twist surprising."    Another reviewer noted "To sum up Success is not the right film for Jr Ganesan to be successful."   

==Soundtrack==
Music is composed by Deva (music director)|Deva.   
*Hey Un Vayasena - Karthik, Mathangi
*Kanna Un - Unnikrishnan, Anuradha Sriram
*Kodi Muppathu Kodi - Tippu, Kovai Kamala
*Marathi Kutti - KK, Mahalakshmi Iyer
*Pothe Konjam Pothe - Udit Narayan

==References==
 

 
 
 
 