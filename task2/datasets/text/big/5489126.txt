Hitman Hart: Wrestling with Shadows
{{Infobox film
| name         = Hitman Hart: Wrestling with Shadows
| image        =  
| alt          =
| caption      =   Steve Austin
| director     =  Paul Jay
| writer       =  Paul Jay
| editing      =  Manfred Becker
| producer     =  Paul Jay  Sally Blake  David M. Ostriker  Silva Basmajian (NFB)
| distributor  =  Vidmark / Trimark
| released     =   
| runtime      =  93 minutes English
| rating       =   
}} World Wrestling WWF Championship SummerSlam to Survivor Series on November 9, 1997.

== Plot ==

=== Between two companies ===
 professional wrestler WWF Attitude" marketing brand which relies on an emphasis on sex, extreme violence, and the replacing of heroic wrestling characters with disaffected anti-heroes in effort to compete against the already-popular WCW, has left Hart disillusioned.
 Raw Is War is losing the Monday Night Wars to its rival WCW Monday Nitro, he signs a twenty-year deal with the WWF. McMahon soon retracts the deal, and encourages Hart to move on to WCW, citing financial reasons. This leaves him with no choice but to take WCW President Eric Bischoffs offer and prepare to leave the WWF. As the still-reigning WWF Champion, conflicts arise regarding his final match with the company, including Harts unwillingness to drop the WWF Championship. To be more precise, Hart is not willing to lose the title to Shawn Michaels, with whom he holds a bitter, real-life rivalry, in his home country of Canada. This leads to one of the most infamous events in professional wrestling as the Montreal Screwjob.
 Pat Patterson Vader discussing his recently acquired real estate license and future career plans.
 Hart House—childhood submission holds on a student of his basement—a notorious training room known as the Dungeon. Harts  ex- wife, Julie, is also interviewed about the toll wrestling has taken on their relationship and raising four children. Hart is shown discussing with both his father and his wife the choice to stay with the WWF or move to WCW. In one instance, his frustration at his kitchen table leads him to ask the director to stop filming.
 Meadowlands parking intoxicated and heel wrestlers Steve Austin. Another wrestling fan even shares beliefs that a strong connection lies between corporate America and the Undertaker/Bret Hart storyline. His words detail how Americans have to operate through a "by any means necessary" philosophy.

=== Montreal Screwjob ===

  Survivor Series no contest or "schmozz". This result would then allow Hart to make a farewell speech the following night on Raw and forfeit his WWF Championship.
 sharp shooter (Harts submission move) which Hart was in the process of escaping. However, McMahon comes to ringside and orders referee—and close friend of Hart—Earl Hebner to call for the ringside bell to be rung, signaling the end of the match. Hebner does so and declares Michaels the winner, stripping Hart of the title.

An enraged Hart spits on McMahon while Hebner, fearing for his safety, flees the building. Hart spells out "WCW" with his fingers to the shocked and confused crowd and smashes numerous ringside television monitors before storming backstage to confront Michaels and McMahon. Michaels feigns ignorance. During a closed-door confrontation with McMahon, Hart punches him in the face; Harts wife, believing that the incident was a conspiracy by the entire WWF roster, corners and confronts numerous wrestlers, most of whom appear bemused. A stunned, bruised McMahon staggers out of the building and Hart and his family leave. The film concludes with footage of Hart at home while on screen text details the immediate fallout of the screwjob.

== Production ==
Wrestling with Shadows is co-produced by High Road Productions Inc.
and the National Film Board of Canada (NFB).    It was released on VHS format to both the United States and United Kingdom in 1999. It has been available on DVD in the UK since 2004. On February 3, 2009 Wrestling with Shadows : The 10th Anniversary Collectors Edition was released on DVD for the first time in the United States. This two-disc edition includes the movie, interviews with Bret Hart and director Paul Jay ten years later and "The Life and Death of Owen Hart" documentary.

In an interview featured on the two-disc special edition, Jay states that the filmmakers had a contract with McMahon to provide not only stock footage, but the waivers for the use of the names and likeness of the other wrestlers featured in the film. After the fallout from Montreal, however, McMahon feared how he would be portrayed in the film and refused both. The director goes on to state that WCW contacted the filmmakers and not only offered to pay for the lawsuit at a cost of $750,000, they also offered a pay-per-view deal for the film and long term distribution on the Turner network. Once McMahon became aware of this, the director states that they received a fax from Titan Sports Inc., saying that they would honor their original contract on the condition that the lawsuit be dropped and they could never sell the film to Turner. Jay said they were told they had a "slam-dunk" case but when asked about the film, they were told they would most likely be in court three to four years and "there would be no film" even if they won; they chose to make the film and dropped the lawsuit. The director goes on to state that McMahon also used his reputation to kill some of the distribution deals in the U.S. and overseas.

==Reception==
It won numerous film festival awards and has aired several times on both A&E Network|A&E and the Documentary Channel as well as on BBC Two in the United Kingdom; after a 1999 airing on BBC Two, journalist Greg Wood described it "a story beautifully told". 

== References ==
 
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 