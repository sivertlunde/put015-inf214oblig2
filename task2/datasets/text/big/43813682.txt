Kranti Veera
{{Infobox film
| name           = Kranti Veera
| image          = 
| caption        = 
| director       = R. Ramamurthy
| producer       = R. Ramamurthy
| writer         = 
| screenplay     = K. V. Srinivasan
| story          = R. Rangarajan
| based on       =  
| narrator       =  Rajkumar  Jayanthi   Dwarakish Satyam
| cinematography = B. Dorairaj
| editing        = R. Ramamurthy
| studio         = Sri Rama Enterprise
| distributor    = 
| released       = 1972 
| runtime        = 163 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
 Rajesh and Jayanthi in Satyam was the soundtrack and score composer. The dialogues and lyrics were written by Chi. Udaya Shankar.

== Cast == Rajkumar  Jayanthi  Rajesh
* B. V. Radha
* Dwarakish
* Vajramuni
* Ranga
* Dinesh
* H. R. Shastry

== Soundtrack == Satyam and lyrics for the soundtrack written by Chi. Udaya Shankar. 

===Tracklist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Yaaru Enu Maduvaru"
| P. B. Sreenivas
|-
| 2
| "Kaniveya Kelagina"
| P. B. Sreenivas, P. Susheela
|-
| 3
| "Molagali Molagali"
| S. P. Balasubrahmanyam, Mahadevan, B. Vasantha
|-
| 4
| "Karinaaga Marinaaga"
| L. R. Eswari
|-
| 5
| "Harikathe" Rajkumar
|-
|}

==See also==
* Kannada films of 1972

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 

 