The Mysterious Dr. Fu Manchu
{{Infobox film
| name           = The Mysterious Dr. Fu Manchu
| image          = mysteriousfumanchu.jpg
| image_size     =
| caption        = The original film poster
| director       = Rowland V. Lee
| producer       =
| writer         = Sax Rohmer (novel) Lloyd Corrigan Florence Ryerson George Marion, Jr. (uncredited)
| narrator       = Neil Hamilton Jean Arthur O. P. Heggie
| music          = Oscar Potoker
| cinematography = Harry Fischbeck
| editing        = Paramount
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
}}

The Mysterious Dr. Fu Manchu is a 1929 film starring Warner Oland as Dr. Fu Manchu. It was the first Fu Manchu film of the talkie era. It was very loosely based on the novel, The Mystery of Dr. Fu-Manchu by Sax Rohmer. 

==Synopsis==
A young white girl, Lia Eltham, is left under Fu Manchu care. A British regiment, chasing Boxer rebels, fires on Dr. Fu Manchus home, killing his wife and child, and Fu Manchu, a brilliant scientist and good man, vows that all the man responsible, General Petrie, and his heirs. When Lia Eltham grows up, he uses her as an instrument for revenge. Opposing Manchu are Police Inspector Nayland Smith and Dr. Jack Petrie.

==Cast==
*Warner Oland as Dr. Fu Manchu Neil Hamilton as Dr. Jack Petrie
*Jean Arthur as Lia Eltham
*O. P. Heggie as Inspector Nayland Smith William Austin as Sylvester Wadsworth Claude King as Sir John Petrie
*Charles A. Stevenson as General Petrie
*Evelyn Selbie as Fai Lu
*Noble Johnson as Li Po

==References==
 

==External links==
* 
*  
* 

 

 
 
 
 
 
 
 
 


 