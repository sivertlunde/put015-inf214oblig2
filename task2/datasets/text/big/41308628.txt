Street Fighter (1992 film)
{{Infobox film
| name           = Street Fighter
| image          = streetfighterdaiwon.jpg
| alt            = 
| caption        = VHS cover art
| film name      = {{Film name
 | hangul         =  
 | rr             = Geori-eui Mubeopja}}
| director       = Sang Il Sim
| producer       = 
| writer         = Jong Jun Park
| based on       =
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Daiwon Animation Co. Ltd
| distributor    = Daiwon C&A Holdings
| released       =  
| runtime        = 51 minutes
| country        = South Korea
| language       = Korean
}}
Street Fighter ( ) is a 1992 Korean animation directed by Sang Il Sim. The film is notable for being based on the popular Capcom fighting game franchise Street Fighter, though made without any involvement or permission from the Japanese video game developer. It has never been released outside of South Korea.

==Plot==
 Bison from taking over the world. 

==Production== Teenage Mutant Ninja Turtles and Armored Fleet Dairugger XV. The film features cameos from other unlicensed franchises, including April ONeil, Arnold Schwarzenegger, Dracula and the Frankenstein monster.

==Release==
The film was released on September 4 in 1992 straight to home video in South Korea. It received a DVD re-release in 2007 by New Media under the Daiwon Classic Animation label alongside Sang Il Sims Red Hawk.

==Differences from the video game== Ryu and Bruce Lees name) and Saeng Yegal. M. Bison is portrayed with sunglasses, a black cape, long hair, and all his Shadoloo skulls replaced with Swastikas. Blanka is in this film shown to be Dhalsims personal pet.

==Reception==
Although the film is largely unheard of due to never having seen release outside of South Korea, the film was featured on Hardcore Gaming 101 in 2010, who felt that "the animation is nothing to brag with, and at least 15 minutes of the film are boring BS, but the ridiculousness of it all makes it a good time watching nonetheless". 

The film was largely criticized for low production values, nonsensical plot and uncharacteristic use of the Street Fighter cast, as well as the lack of Ryu and Ken. The film has since also been uploaded to various video sharing websites such as YouTube.

== References ==
 

==External links==
*  

 

 
 
 
 
 
 