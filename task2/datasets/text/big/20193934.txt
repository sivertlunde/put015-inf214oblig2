Hudugaata
{{Infobox film
| name           =  Hudugaata
| image          =  Hudugaata.jpg
| caption        =  A Romantic Journey
| director       =  Sanjay. K
| producer       =  B.G. Babu Reddy
| eproducer      =
| aproducer      =  Bhaskar
| writer         =  Sanjay. K
| screenplay     =  Sanjay. K
| starring       = Golden Star Ganesh,  Rekha Vedavyas,  Avinash,  Komal Kumar
| music          = Jassie Gift
| cinematography =  Satya Hegde
| editing        =  K M Prakash
| distributor    =  Nitin productions
| released       = 8 June 2007
| runtime        =
| country        = India
| awards         =
| language       = Kannada
| budget         =  
| boxoffice      =  
| preceded_by    =
| followed_by    =
| website        =
}} 2007 Kannada film directed by Sanjay K and produced by B.G. Babu Reddy under the Nitin productions banner. The film stars Golden Star Ganesh, Rekha Vedavyas and Avinash in pivotal roles. The soundtrack, composed by Jassie Gift, was launched on 27 May 2007. It was originally planned to be released on 1 June 2007, but the film released on 8 June 2007. It completed its 100 days run successfully. The film is a remake of the Hindi Dil Hai Ki Manta Nahin which itself was a remake of the Hollywood movie It Happened One Night.

==Plot==
Hudugaata is a fun-filled story with light-hearted comedy throughout. Balu Mahendar (Ganesh) is a crime reporter at a popular newspaper. His division gets shifted to a sports reporter because of his hudugaata. He is sent to interview a sports personality in Kanyakumari. From here, the romantic journey starts. He meets the heroine who needs help in the bus. He promises to help her in return for an exclusive news item about her in his newspaper. Will the heroine be able to get what she needs? Will Balu Mahender complete his assigned job? Look out for a comedy edition of this story on the screen.

==Review==
Ganesh, the golden star of comedy, has done an excellent job. He retains his Mungaru Male style of dialogues in many scenes which makes people jump with whistles.  Rekha has done a good job and looks pretty on screen. Avinash and Komal Kumar have done justice to their comedy roles. Evergreen Shobraj comes in to the frames for some action scenes with Ganesh and for some comedy. 

Sanjay, even though this is his first movie as a director, makes no mistakes; he has performed extremely well and succeeds in making people laugh.  Jassie Gift has given fantastic songs and enjoyable background music all through the movie. Songs are the major plus point. 

==Cast== Ganesh ... Balu Mahendar
*  Rekha Vedavyas ... Priya
*  Komal Kumar
*  Avinash
*  Shobaraj
*  Satyajit

==Crew==
* Director: Sanjay K
* Screenplay: Sanjay K
* Story: Sanjay K
* Dialogue: Malavalli Saikrishna
* Producer:  B.G. Babu Reddy
* Music: Jassie Gift
* Art Director: Mohan B Kere
* Cinematographer: Satya Hegde
* Editing: K.M. Prakash
* Fights: Ravivarma
* Executive Producer:  B.S. Murulidhar
* Associate Director: Bhaskar
* Assistant Directors: Vishwas Madisetty, Raghavendra Hedathale, Santosh Betageri, Prashanth
* Lyrics: Kaviraj, Jayanth Kaikini
* Editor: K.M. Prakash
* Choreography: Suresh, Raamu, Imran, Murali
* Stills: Harish
* Costume: Nagraj
* Make-Up: Prabhakar
* Production Manager: Shashidhar
* Team Co-ordinator: Anand Paalankar
* P.R.O: Vijay Kumar
* Web Publicity: Abhishek S N
* Poster Designs: Kumar
* Editing Studio: Skyline Creations Prasad Labs
* DTS Mixing: Balaji Studios
* Singers: Jassie Gift, Shaan, Vasundhara Das, Shreya Goshal, Naresh Iyer, Chitra, Alisha Cinai, Zubeen, Saindavi, Vijay Jesudas, Chethan
* Audio: Anand Audio
* Dubbing and Re-Recording: Akash Studios
* Media partner: BIG 92.7 FM Radio Channel

==Box office==
The movie was a much anticipated movie of 2007 because of previous success of lead star Ganesh (actor)|Ganesh. The movie received mixed to positive reviews from critics and received positive reviews from audience and was declared "super hit" at box office.

==Soundtrack==
{{Infobox album  
| Name        = Hudugaata
| Type        = Soundtrack
| Artist      = Jessie Gift
| Cover       =
| Released    =  2007
| Recorded    = Feature film soundtrack
| Length      =
| Label       =
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) || Duration
|-
| 1 || Are Are Sagutide || Naresh Iyer, K. S. Chitra || 05:28
|-
| 2 || Ishtu Jana || Karthik (singer)|Karthik, Saindhavi, Jessie Gift, Chetan Sosca || 05:00
|-
| 3 || Mandakiniye || Jessie Gift, Vasundhara Das || 04:35
|-
| 4 || Nenapagade  || Vijay Yesudas, Rajalakshmi || 05:36
|-
| 5 || Stylo Stylo || Jessie Gift, George Peter, Prasanna || 04:39
|-
| 6 || Ommomme Heego || Zubeen Garg, Alisha Chinoy || 04:17
|-
| 7 || Eno Onthara || Shaan (singer)|Shaan, Shreya Ghoshal || 05:01
|}
 

==Home media==
The movie was released on DVD with 5.1 channel surround sound and English subtitles and VCD.

==References==
 

==External links==
*  

 
 
 
 