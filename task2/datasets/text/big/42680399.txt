Discopath
{{Infobox film
| name           = Discopath
| image          = Discopath-poster.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Renaud Gauthier
| producer       = Marie-Claire Lalonde
| writer         = Renaud Gauthier
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{plainlist|
*Jeremie Earp
*Sandrine Bisson
*Ivan Freud
*Francois Aubin
*Ingrid Falaise}}
| music          = Bruce Cameron
| cinematography = John Londono
| editing        = Arthur Villers
| studio         = Durango Pictures
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = Canada
| language       = {{plainlist|
*French
*English}}
| budget         = 
| gross          = 
}}

Discopath ( ) is a 2013 Canadian film directed by Renaud Gauthier. The film is about Duane Lewis, a New York cook who becomes insane when hearing disco music. After murdering a woman in a club, Duane travels to Montreal where he continues his killing spree. Discopath premiered in Canada at the Fantasia Film Festival.

==Production==
Discopath is the debut feature film from Renaud Gauthier. 

==Release==
The film had its world premiere at the Fantasia Film Festival in Montreal on August 3, 2013.    Prom Night. 

==Reception==
Screen Daily referred to the Discopath as "an enjoyable oddball genre blending, fun while it lasts but ultimately too niche to make much of an impact, unless with horror fans."  The Hollywood Reporter stated, "Semi-campy slasher pic is for genre fetishists only," and, "Its hard not to feel that Gauthier cares more about mimicking the style of giallos greatest hits than about telling a story."  Indiewire gave the film a D- rating, citing poor quality dubbing and production values as well as stating, "This is the type of movie that used to be made all the time, by quick-change artists capitalizing on a couple of random trends. But at least there was a genuine sleaze element at work, and the idea that there was a genuine approach and vision that emerged from a generic incompetence with cameras, framing and effects. Now, it’s just an intentional throwback, wrapping everything in intentional artifice to preserve something that was appealing for being primitive and not particularly thought-out. You cant go home again." 

==Notes==
 

==External links==
*  

 
 
 
 
 
 