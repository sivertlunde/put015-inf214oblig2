The Darkest Hour (film)
{{Infobox film
| name           = The Darkest Hour
| image          = The_Darkest_Hour_Theatrical_Poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Chris Gorak
| producer       = Tom Jacobson Timur Bekmambetov
| screenplay     = Jon Spaihts
| story          = Leslie Bohem M.T. Ahern Jon Spaihts
| starring       = Emile Hirsch Olivia Thirlby Max Minghella Rachael Taylor
| music          = Tyler Bates
| cinematography = Scott Kevan
| editing        = Priscilla Nedd-Friendly Fernando Villena New Regency Jacobson Company Twentieth Century Fox  
| released       =  
| runtime        = 89 minutes  
| country        = United States Russia 
| language       = English Russian
| budget         = $30 million 
| gross          = $64,626,786 
}} science fiction thriller  film directed by Chris Gorak and produced by Timur Bekmambetov. The American-based production depicts an alien invasion and stars Emile Hirsch, Max Minghella, Olivia Thirlby, Joel Kinnaman, and Rachael Taylor as a group of people caught in the invasion. The film was released on December 25, 2011 in the United States.

==Plot==
Americans Ben and Sean (Max Minghella and Emile Hirsch) travel to Moscow to sell their social networking/party locating software. As they approach the airport the plane short circuits due to an electrical storm but regains power. They find their Swedish business partner, Skyler (Joel Kinnaman), has betrayed them and already made a deal with the Russians, using a knockoff application. They go to a nightclub and meet Natalie (Olivia Thirlby), an American, and her Australian friend Anne (Rachel Taylor). The lights go out in the club and everyone heads outside. There, they witness what appears to be an Aurora (astronomy)|aurora. Balls of light fall from the sky and then fade away. When a policeman investigates he is disintegrated. The invisible entities begin hunting and disintegrating people, sending them into a panic.
 GUM department store shopping mall. While they are searching the car, a barking dog discovers and fatally confronts an alien. Ben and Sean successfully hide under the car as the alien moves closer, causing the cars lights and siren to turn on. The alien moves on and the men run to the mall. Sean realizes that lights give the aliens away. The group takes shelter in one of the mall stores. Sean and Natalie go to look for clothes and almost run into an alien who cant see them through a glass wall. Sean theorizes that the aliens can only see their electrical charge, but not through glass.

The group finds the American Embassy gutted and lifeless. All except Skyler go to the roof to get an aerial view. They find a logbook telling them that the invasion is worldwide. They also find a radio broadcasting a message in Russian. They hear gunfire. Ben and Sean go to help Skylar, but he is killed. When the others go outside they see a light in a nearby apartment tower and go to investigate, bringing the radio they found. They find a young woman named Vika and a man named Sergei, an electrical engineer. Sergei has made his apartment into a giant Faraday cage that hides everyone from the aliens. He has also developed a microwave gun that weakens an aliens force field, so that it can actually be seen and killed. Vika and Sergei translate the message, which says that a nuclear submarine K-152 Nerpa is waiting in the Moscow River to take survivors to safety.

As Sergei shows the men the microwave device, Vika, Natalie, and Anne go to other apartments to gather supplies for the submarine journey. An alien senses them outside the Faraday cage and gives chase; Anne hesitates following Vika and goes another way, causing Natalie to follow her back inside. When they get to the apartment, Natalie is unable to close the door and the alien gets inside. Sergei shoots the alien with his gun and finds that it is only stunned. The alien disintegrates Sergei while the others get out by the fire escape. Anne hesitates again and is disintegrated while trying to escape. Natalie sets the apartment on fire as they climb down and meet up with Vika.

They meet up with a Russian police team with improvised armor led by Boris who manage to wound an alien with conventional weapons. Sean collects a piece of the aliens body. The small police band were also able to build another Faraday cage at the local library. Boris and his men eventually agree to help the remaining four to get to the submarine. The policemen believe that the aliens are strip mining for conductive metals since giant light columns can be seen drilling all over Moscow.

As they move through the subway, an alien discovers them and they escape on the tracks. Vika hides behind a pillar, unable to join the others without being seen. Ben helps her climb down to the tracks, but gets disintegrated.

The survivors make it to a powerless patrol boat on the river and drift downstream to the waiting submarine. The boat nears the submarine but runs aground. As they attempt to push free, a new light beam destroys a large apartment building right next to the river, causing the boat to capsize. Sean and the policemen swims towards the submarine but upon reaching it discover Natalie is missing. They see a flare fired from Natalies flare gun in a railway and bus yard near the river. Sean is determined to get her, possibly missing his chance to escape. The policemen agree to help him rescue her. The Russian submarine crew, after expressing doubt about the rescue, assist by building another microwave gun with stronger batteries.

After the team manages to destroy an alien, Sean finds Natalie on a bus while the policemen and Vika destroy three more aliens using the microwave guns and water. As Sean is about to get off the bus with Natalie, an alien climbs on board, locks the door, and sends the bus speeding around the bus yard. It grabs onto Natalies leg, but Sean blasts the aliens shield with the microwave gun. He then discovers the aliens weakness when he throws a piece of the wounded alien he had collected earlier, and it destroys the unshielded alien. The two stop the bus and narrowly avoid a collision.

After returning to the submarine, the police team decides to stay and fight for the city, and Boris tells them that now the war has begun. Sean, Natalie, and Vika plan to spread what they learned about the aliens - the microwave guns and their vulnerability to pieces of other dead aliens - to the rest of the world. Sean and Natalie nearly share a kiss on the submarine. They soon learn that survivors in Paris managed to destroy an alien mining tower, and the film ends on a hopeful note.

==Cast==
 
* Emile Hirsch as Sean
* Olivia Thirlby as Natalie
* Max Minghella as Ben
* Rachael Taylor as Anne
* Joel Kinnaman as Skyler
* Gosha Kutsenko as Matvei
* Veronika Vernadskaya as Vika
* Dato Bakhtadze as Sergei
* Nikolay Efremov as Sasha
* Pyotr Fyodorov as Anton Batkin
* Georgiy Gromov as Boris
* Artur Smolyaninov as Yuri
* Anna Rudakova as Tess
 

==Production==
The Darkest Hour was directed by Chris Gorak and produced by Timur Bekmambetov. While most films about alien invasions are centered in the United States or have an international scale, Bekmambetovs involvement ensured the premise to be an alien invasion from Russias perspective. 

With a production budget of US$30 million,    filming with 3D cameras began in Moscow on July&nbsp;18, 2010. Production used resources from the Russian-based company Bazelevs, owned by Bekmambetov.

Filming was temporarily suspended three weeks later due to the 2010 Russian wildfires affecting the city and its vicinity with their smog. By September 2010, filming had resumed. In April 2011 the release date was changed to December 25 due to filming conflicts in Russia.   

==Release==
The Darkest Hour was released on December 25, 2011 in the US in 2D, 3D and RealD 3D.  The DVD release date was April 10, 2012 by Summit Entertainment. It was released January 13 in the UK with the DVD release on May 21.

==Reception==
The Darkest Hour was not screened for critics, and received mostly negative reviews. Rotten Tomatoes gives a score of 12% based on reviews from 57 critics. 

The film was criticized for having a "flatlining screenplay and the absence of even a single compelling character"  as well as for being "a depressing failure of imagination". 

Budd Wilkins writing for Slant magazine called it "a dimwitted 3D sci-fi travesty" and complained, "Indifferently structured, Jon Spaihtss lame-brained script knows no narrative contrivance it doesnt love and, whats worse, blows its expositional load in the first 10 minutes, bringing together a quintet of cardboard cutout leads." 

At the box office the film was a moderate success, making just over twice its budget.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 