Hardbodies 2
{{Infobox Film
| name           = Hardbodies 2
| image          = 
| image_size     = 
| caption        =  Mark Griffiths
| producer       = Dimitri Logothetis, Jeff Begun
| writer         = Mark Griffiths Curtis Wilmot
| starring       = James Karen Alba Francesca Alexandros Mylonas Robert Rhine Sorrells Pickard Brad Zutaut Fabiana Udenio Brenda Bakke Roberta Collins Curt Wilmot
| music          = Eddie Arkin Jay Levy
| cinematography = Tom Richmond
| editing        = Andy Blumenthal
| distributor    = Cinetel Films
| released       =  
| runtime        = 88 min.
| country        = United States
| budget         =
| gross          = $78,068
| language       = English
}}

Hardbodies 2 is a 1986 adult comedy movie sequel to the 1984 film Hardbodies. It was directed by Mark Griffiths and featured Brad Zutaut, Fabiana Udenio, James Karen and Alba Francesca. The plot
involves a pair of film crews in Greece, and derives humor from the use of profanity and nudity.  The movie was released by CineTel Films and it has a run time of 88 minutes.  Leonard Maltin gave the film a "bomb" rating. 

==References==
{{reflist|refs=

   

   

   

}}

==External links==
*  

 
 
 
 


 
 