Dunston Checks In
{{Infobox film
|name=Dunston Checks In
|image=Dunston Checks In.jpg
|caption=Promotional poster
|director=Ken Kwapis
|producer=Todd Black Joe Wizan
|screenplay=John Hopkins   Bruce Graham
|story=John Hopkins
|starring=Eric Lloyd Graham Sack Jason Alexander Faye Dunaway Rupert Everett Glenn Shadix Paul Reubens 
|music=Miles Goodman
|cinematography=Peter Lyons Collister
|editing=Jon Poll
|distributor=20th Century Fox
|released= 
|runtime=88 minutes
|country=United States
|language=English
|budget=$16 million
|gross=$9,871,065
|}}
Dunston Checks In is a 1996 American comedy film starring Eric Lloyd, Graham Sack, Jason Alexander, Faye Dunaway, Rupert Everett, Paul Reubens, Glenn Shadix, and introducing Sam the Orangutan as Dunston. It was written by John Hopkins and Bruce Graham and directed by Ken Kwapis.

==Plot== sixth star.

Just then, "Lord" Rutledge (Rupert Everett) a jewel thief (who Mrs. Dubrow thinks is the critic), arrives with an orangutan named Dunston, intending to steal the guests jewelry. Dunston and his deceased brother Samson were trained in thievery their whole lives. Now Dunston has been wanting to escape from Rutledges poor treatment and life of crime ever since. 

Meanwhile, Dunston escapes Rutledge, but is cornered by both Mrs. Dubrow and LaFarge. Dunston is saved by Kyle and Dunston saves him in turn afterwards. Robert eventually manages to stand up to Mrs. Dubrow, but is fired in the process. However, it turns out that Mr. Spalding, humiliated and injured by Dunstons antics, was actually the critic all along. As a result, he immediately reduces the Majestic to a one-star hotel. Rutledge is arrested and LaFarge apologizes to Dunston, who then slaps him.

In the end, Robert, Kyle and Brian move to Bali, to manage the Majestic hotel there, where theyve managed to keep Dunston as a pet. They invite Mr. Spalding over with a complementary room and meals to make up for all the trouble he endured and assure him that nothing will go wrong this time. However, Dunston, in the last scene, causes more trouble by dropping a large coconut on his head.

==Cast==
* Eric Lloyd as Kyle Grant
* Graham Sack as Brian Grant
* Jason Alexander as Robert Grant 
* Faye Dunaway as Elena Dubrow
* Rupert Everett as Lord Rutledge 
* Paul Reubens as Buck LaFarge 
* Glenn Shadix as Lionel Spalding  Nathan Davis as Victor Dubrow 
* Jennifer Bassey as Angela Dellacroce 
* Bob Bergen as Special Vocal Effects
* Frank Welker as Special Vocal Effects
* Sam the orangutan as Dunston

==Reception== souffle and Razzie Award Worst Supporting Actress, but did not win the award.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 