Criminal (2015 film)
{{Infobox film
| name           = Criminal
| image          = http://www.millenniumfilms.com/Details.aspx?id=a23e2ff0-5913-e411-ba69-d4ae527c3b65
| alt            =
| caption        = 
| director       = Ariel Vromen Chris Bender   Christa Campbell   Boaz Davidson   Mark Gill   Lati Grobman   Matthew OToole   Trevor Short   J. C. Spink   John Thompson
| writer         = Douglas Cook   David Weisberg
| starring       = Ryan Reynolds   Kevin Costner   Tommy Lee Jones   Gary Oldman
| music          = Haim Mazar
| cinematography = Dana Gonzales
| editing        = Danny Rafic
| studio         = Campbell-Grobman Films   Millennium Films
| distributor    = Summit Entertainment (USA)   Lionsgate (International) 
| released       =  
| runtime        = 
| country        = United Kingdom   United States
| language       = English
| budget         = 
| gross          =  
}}

Criminal is an upcoming 2015 American-British action thriller film directed by Ariel Vromen and written by Douglas Cook and David Weisberg. Ryan Reynolds, Kevin Costner, Tommy Lee Jones and Gary Oldman star in the film, about a criminal who is implanted with a dead CIA agents memories to finish a mission.

Principal photography began on September 4, 2014, in London. The film is produced by Campbell-Grobman Films and Millennium Films. The film is scheduled to be released on August 21, 2015.

== Plot ==
The memories and skills of a deceased CIA agent are implanted into an unpredictable and dangerous criminal to find some criminals and finish the job.

== Cast ==
 
* Ryan Reynolds 
* Kevin Costner 
* Tommy Lee Jones 
* Gary Oldman 
* Alice Eve 
* Gal Gadot 

* Jordi Mollà 
* Antje Traue 
* Michael Pitt
* Scott Adkins
* Amaury Nolasco Richard Reid
 

== Production == Chris Bender, Matthew OToole and Mark Gill would produce the film.   On September 13, Millennium set Ariel Vromen to direct the film.   

On June 17, 2014, Kevin Costner was added to the cast to play a dangerous criminal with a dead CIA operatives skills, secrets and memories implanted into him to finish a job.    On July 10, Gary Oldman was in talks to join the film to play CIA chief.    On July 23, Tommy Lee Jones joined the film to play a neuroscientist who transplants the memories in criminal, while Oldmans role also confirmed.    On August 4, Ryan Reynolds was added to the cast.    On August 7, Alice Eve joined the cast of the film.    On August 11, Jordi Mollà joined the film for a villain role of Hagbardaka Heimbahl, an assassin who wants the dead CIA agents secrets now implanted into the criminals brain.    On August 12, Gal Gadot signed on to star in the film as Reynolds characters wife.    On September 26, Antje Traue joined the film to play a terrorists henchwoman.   

=== Filming ===
The principal photography on the film began on September 4, 2014, in London.  On September 8, actor Reynolds was spotted on a bike during the filming of some scenes in the streets of London.  On September 16, Costner and Gadot were spotted at the Camber Sands during filming in East Sussex.  Some actors and crews were also spotted filming scenes for the film on Kings Road in Kingston upon Thames|Kingston.  From September 22-25, the filming was taken place in Yateley, Hampshire, where actors were spotted filming car crashed and helicopter chase scenes at the Blackbushe Airport.   Filming would also be done in Croydon College in Croydon town, colleges building would be used as medical research labs and a CIAs operations centre.  On October 23 aerial drone filming was undertaken featuring Kevin Costner in a car chase scene on Whites Row in East London.  Some filming also took place in SOAS University of London library. 

=== Music ===
On December 9, 2014, it was announced that Haim Mazar had signed on to score the music of the film. 

== Release == Lionsgate set the film for an August 2015 release. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 