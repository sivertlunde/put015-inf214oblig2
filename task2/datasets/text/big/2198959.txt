National Security (2003 film)
{{Infobox film
| name           = National Security
| image          = National Security movie poster.jpg
| caption        = Poster for National Security.
| director       = Dennis Dugan
| producer       = Moritz Borman Martin Lawrence Peaches Davis Nigel Sinclair Jeff Kwatinetz Robert Newmyer
| writer         = Jay Scherick David Ronn
| starring       = Martin Lawrence  Steve Zahn
| music          = Randy Edelman
| cinematography = Oliver Wood
| editing        = Debra Neil-Fisher
| distributor    = Columbia Pictures
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| gross          = $50 million
}} action comedy Matt McCoy, and others.
 Greater Los Long Beach Santa Clarita. 

==Plot== LAPD policemen, Hank Rafferty (Zahn) and his partner Charlie Reed (Timothy Busfield), investigate a high-security warehouse break-in and discover a gang of thieves; one of them, who has a tattoo of a crab, shoots Charlie to death before they escape.

Meanwhile, Earl Montgomerys (Lawrence) lifelong dream to become a police officer is thwarted when he is expelled from the police academy.

Hank crosses paths with Earl when Hank notices Earl trying to get into his car when he finds his keys locked inside. Hank starts to question Earl, who race card|race-baits Hank to the point of getting himself arrested. A bumblebee comes along, to which Earl is virulently allergic. Earl panics and Hank tries to swat the bee away with his nightstick. From afar, it appears as if Hank, a white policeman, is brutalizing a black suspect while at the same time a Latino man films the whole scene. Disgraced, Hank is dismissed from the police force and convicted of aggravated assault against Earl. He is sentenced to six months in prison.

After being released from prison, Hank takes a job as a security guard and continues to investigate Charlies death. Noticing an alarm being tripped at a soda warehouse, Hank goes to investigate.

Meanwhile, Earl, who happens to be working for the same security company, is on duty at the soda warehouse, but is slacking off. When Hank arrives, he interrupts the heist, and a gunfight erupts with the thugs, during which Hank and Earl cross paths again. Though the thugs get away, Hank recognizes the tattoo of the man who shot Charlie, whose name he learns is Nash (Eric Roberts).

One of the thieves dropped a cellular phone, which leads them to a semi truck rented by the killers. Inside, Hank and Earl find a van. They drive the van out of the truck but the van falls off the bridge onto a garbage barge. Inside the van are what look like ordinary beer kegs, but Hank has them examined by a friend who works at a foundry, who informs them that the kegs are actually made of an aerospace alloy which is worth millions.

Hank takes the van and the kegs to the house of his ex-girlfriend, Denise (Robinne Lee). They broke up after Hank was arrested, and Hank orders Earl to tell Denise the truth about the "assault." Earl had previously said "I know baby.  But I promise you.  My life is pretty much figured out." However, when Earl sees that Denise is an attractive black woman, he forgets his promise and starts hitting on her, playing the victim again. She throws both of them out of the house, and when Hank asks for an explanation, Earl reveals that he disapproves of interracial dating.  Hank is infuriated and points out that for all his talk about racism, it is actually Earl himself who is the Reverse discrimination|racist.  During the argument, Hank punches Earl and storms off. Earl runs back to Hank, just as they are both cornered by police, learning that they are wanted as suspects in the bridge shootout. After they manage to escape, Hank realizes that the thieves must have an inside man in the police department.
 stake out the place, but Earl foolishly rushes inside on his own, where he is confronted by Nash. Hank manages to get Earl to safety, but Earl takes a bullet in the leg. A Denises house, they discover that Earls wound is a graze. Fortunately for Hank, a bee flies into the house, and Earl runs for cover, making Denise realize that Hanks outlandish story about the "assault" on Earl was actually true. She slaps Earl for lying and putting Hank in Jail. She throws Earl out of the house and reconciles with Hank.

Based on something overheard from Nash, they follow him to a meeting at a Yacht Club and witness him talking to McDuff, who is revealed to be the mole in the police force.  Hank and Earl share everything they know with Washington (Bill Duke), and then pretend to approach McDuff, offering to sell him back the "beer kegs" for a large sum of money. However, Nash gets wind of their plans and takes Washington hostage first.

During the confrontation, Earl and Hank meet with McDuff, Nash and his men near the coast, rescuing Washington and killing or apprehending most of the thugs, including McDuff. During the shootout, Hank saves Earls life by warning him about a gunman taking aim at him, getting shot himself in the process. Earl engages in a fight with Nash on an unstable slab while Hank is wounded and slow to get up.  Though wounded, Hank takes off after Nash alone and kills him by dropping a crane load onto an unstable slab Nash is standing on, catapulting him over a cliff and into the ocean.

In honor of their heroic actions, Hank is reinstated in the LAPD and Earl is admitted to the force, and they are made partners a short time later. However, Earl and Hank are left dumbstruck after they encounter a carjacker, whom Earl believes was locked out of his own car, and when they find out the cars real owner, Earl shoots at the tires; the car crashes and explodes.

==Cast==
*Martin Lawrence as Earl Montgomery
*Steve Zahn as Hank Rafferty
*Colm Feore as Detective Frank McDuff
*Bill Duke as Lieutenant Washington
*Eric Roberts as Nash
*Timothy Busfield as Charlie Reed
*Robinne Lee as Denise Matt McCoy as District Attorney Robert Barton
*Brett Cullen as Heston
*Mari Morrow as Lola

==Music==
The main songs are:
* "Silly" - The Warden
* "One of These Days" - Wu-Tang Clan
* "95 South" - Cool Ade
* "All Good? - De La Soul
* "N.S.E.W." - Disturbing tha Peace



* Fruko y Sus Tesos ("El Preso") 
* Graveyard Soldjas ("Dont Start None") 
* The Warden ("Silly") 
* Petey Pablo ("Blow Your Whistle") 
* De La Soul featuring Chaka Khan ("All Good") 
* Tracy ("One More Try") 
* Barry White ("Cant Get Enough Of Your Love Babe") 
* Fingaz ("Baby") 
* 95 South ("Cool Ade (Extended Mix)") 
* Lil O ("Ay Yo") 
* Bathgate ("Bump That") 
* Damian Valentine ("Revolution") 
* Disturbing Tha Peace ("N.S.E.W.")

==Reception==
===Box office===
The film grossed $50,097,949 worldwide.

===Critical response===
The film was poorly received by critics, receiving a rating of 11% on Rotten Tomatoes based on 88 reviews. 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 