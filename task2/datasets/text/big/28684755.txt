Cellofan – med døden til følge
 

{{Infobox film
| name           = Cellofan – med døden til følge
| image          = 
| image size     =
| caption        = 
| director       = Eva Isaksen
| producer       = 
| writer         = Leidulv Risan
| narrator       =
| starring       = Andrine Sæther   Sverre Anker Ousdal
| music          =  
| cinematography = 
| editing        = 
| distributor    = 
| released       = 28 August 1998
| runtime        = 90 minutes 
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
| preceded by    =
| followed by    =
}}
 Norwegian crime film directed by Eva Isaksen, starring Andrine Sæther and Sverre Anker Ousdal. The young journalist Marianne Hovden (Sæther) is instructed by her father on his deathbed to burn a box of letters hidden in his attic. Defying his wish, she reads the letters, where he is accused of the murder of a young woman 25 years ago. Convinced of her fathers innocence, she travels to the small village where the murder took place, to solve the mystery.

==External links==
*  
*  
*   at the Norwegian Film Institute

 
 
 
 


 
 