Men Suddenly in Love
 
 
{{Infobox film
| name           = Men Suddenly in Love
| image          = Men Suddenly in Love poster.jpg
| alt            = 
| caption        = 
| film name = {{Film name| traditional    = 猛男滾死隊
| simplified     = 猛男滚死队
| pinyin         = Měng Nán Gǔn Sǐ Duì
| jyutping       = Maang2 Naam4 Gwan2 Sei2 Deoi2}}
| director       = Wong Jing
| producer       = Wong Jing
| writer         = Wong Jing
| starring       = Eric Tsang Chapman To Jim Chim Wong Jing Tat Dik
| music          = 
| cinematography = Chow Lin Yau
| editing        = Lee Kar Wing
| studio         = Vigor Creative Production Limited
| distributor    = Gala Film Distribution Limited
| released       =  
| runtime        = 89 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
}}
Men Suddenly in Love is a 2011 Hong Kong romantic comedy film produced by, written by and directed by Wong Jing. Film stars Eric Tsang, Chapman To, Jim Chim, Wong Jing and Tat Dik.

==Cast==
* Eric Tsang as York Ng (吳郁仁)
* Chapman To as Keith Szeto (司徒奇峰)
* Jim Chim as Claude Cheung (張秋雲)
* Wong Jing as Sam Fu (傅武琛)
* Tat Dik as Charlie Lam (林查理)
* Richard Ng as Master Jude (朱老師)
* Chrissie Chau as Tina
* Carol Yeung as Jeanne (小倩)
* Jessica Xu as Audrey
* Caroline Zhu as Peril Ngai (倪險)
* Betrys Kong as Eleven Maggie Cheung as Hillary Lau (劉玉卿)
* Monica Chan as Nana Lam (林娜娜)
* Mak Ling Ling as Du Gu Ling Ling (獨孤靈靈)
* Jacqueline Chong as Lam Cha Leis wife (林查理妻)
* Yeung Sze Man as Cheung Chau Wans wife (張秋雲妻)
* Lee Kin-yan

==External links==
*  
*   at Hong Kong Cinemagic
*  

 
 
 
 


 
 