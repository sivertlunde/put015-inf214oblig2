Total Balalaika Show
 
{{Infobox film
| name           = Total Balalaika Show
| image          = Total Balalaika show poster.jpg
| image_size     = 
| caption        = Polish theatrical poster
| director       = Aki Kaurismäki
| producer       = Aki Kaurismäki
| writer         = Aki Kaurismäki
| narrator       = 
| starring       = 
| music          = B. Alexandrov Tipe Johnson
| cinematography = Heikki Ortamo
| editing        = Timo Linnasalo	 	
| distributor    = 
| released       =   (France, Rouen Nordic Film Festival)  (Finland)
| runtime        = 57 min
| country        = Finland Sweden
| language       = English, Finnish
| budget         = 
| gross          = 
}} 1994 film by director Aki Kaurismäki featuring a concert with Leningrad Cowboys and Alexandrov Ensemble.
 Senate Square in Helsinki, Finland. The event drew a crowd of approximately 70,000 people from two nations - Finland and Russia - that had been engaged in a state of "peaceful coexistence" during the Cold War.
 eclectic mix of Western rock and Russian folk music, and folk dancers performing to rock songs.

== Songs ==
The "rockumentary" features 13 songs from the concert:
 Finlandia
* Lets Work Together (song)|Lets Work Together
* The Volga Boatmens Song Happy Together Delilah
* Knockin on Heavens Door
* Polyushka Polye|Oh, Field Kalinka
* Gimme All Your Lovin
* Jewelry Box Sweet Home Alabama Dark Eyes Those Were The Days

== DVD release == DVD Region All Regions. Eclipse box set with the two original films.

==Soundtrack Album==
 

== External links ==
* 
* 
* 

 
 

 
 
 
 
 

 