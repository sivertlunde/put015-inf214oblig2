The Florentine Dagger
{{Infobox film
| name           = The Florentine Dagger
| image          =
| caption        =
| director       = Robert Florey
| producer       =
| writer         = Ben Hecht (novel) Tom Reed (screenplay)
| starring       = 
| music          =
| cinematography = Arthur L. Todd Thomas Pratt
| distributor    = Warner Brothers
| released       =  
| runtime        = 69 min.
| country        = United States
| language       = English 
| budget         =
}}

The Florentine Dagger  is a 1935 American mystery film directed by Robert Florey.  Donald Woods plays a descendant of the Borgia line, convinced that hes inherited their murderous tendencies.  Suspicions deepen when the father of the girl he loves turns up stabbed to death with a Florentine dagger.

==Cast==
 Donald Woods as Juan Cesare
* Margaret Lindsay as Florence Ballau
* C. Aubrey Smith as Dr. Lytton
* Henry ONeill as Victor Ballau
* Robert Barrat as Inspector Von Brinkner
* Florence Fair as Ballaus Housekeeper
* Frank Reicher as Stage Manager
* Charles Judels as Hotel Proprietor
* Rafaela Ottiano as Lili Salvatore
* Paul Porcasi as Italian Policeman
* Eily Malyon as Fredericka - Mask Maker
* Egon Brecher as Lyttons Butler
* Herman Bing as The Baker
* Henry Kolker as The Auctioneer

==External links==
*  

 
 

 
 
 
 
 
 
 


 