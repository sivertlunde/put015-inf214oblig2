Mean Girls
 
{{Infobox film
| name           = Mean Girls
| image          = Mean Girls movie.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster Mark Waters
| producer       = Lorne Michaels
| screenplay     = Tina Fey
| based on       =  
| starring       =   
| narrator       = Lindsay Lohan
| music          = Rolfe Kent
| cinematography = Daryn Okada
| editing        = Wendy Greene Bricmont
| studio         = SNL Studios
| distributor    = Paramount Pictures
| released       =  
| runtime        = 97 minutes  
| country        = United States
| language       = English
| budget         = $17 million 
| gross          = $129 million   
}} teen comedy Mark Waters directed, and Tina Fey wrote the screenplay, which is based in part on Rosalind Wisemans non-fiction book Queen Bees and Wannabes, which describes female high school social cliques and the damaging effects they can have on girls.
 Freaky Friday (2003), released a year earlier. Although set in Illinois, the film was mostly shot in Toronto, Canada.

The film grossed $129 million worldwide and has developed a cult following.      

==Plot== homeschooled daughter zoologist parents. queen bee Regina George. The Plastics take an interest in Cady, however, and invite her to sit with them at lunch. Seeing that Cady is slowly becoming one of The Plastics, Janis hatches a plan of revenge against Regina, using Cady as the infiltrator.

Cady soon learns about Reginas "Burn Book", a notebook filled with rumors, secrets, and gossip about the other girls and some teachers. Cady also falls in love with Reginas ex-boyfriend, Aaron Samuels, whom a jealous Regina steals back at a Halloween party. Cady continues with Janiss plan to cut off Reginas "resources", which involve separating her from Aaron, tricking her into eating nutrition bars that make her gain weight, and turning Reginas fellow Plastics – insecure rich girl Gretchen Wieners and sweet but ditzy Karen Smith – against her. In the process, Cady unwittingly remakes herself in Reginas image, becoming spiteful and superficial and abandoning Janis and Damian.

Cady hosts a party at her own house one weekend while her parents are away. The party is supposed to be a small get-together, but a large number of people show up. While waiting for Aaron, Cady drinks too much punch. When she finally finds him, she explains that she was failing math on purpose, just so she could have an excuse to talk to him. This angers Aaron, who tells Cady shes no better than Regina. The drunk Cady vomits on Aaron. While Cady chases after an infuriated Aaron, Janis and Damian, who are upset that Cady lied to them about not being able to attend Janiss art show that night, show up. Cady tries to explain her motives, but Janis says that Cady has become worse than the Plastics by hiding a spiteful personality behind her cute and innocent facade.

When Regina learns of Cadys treachery, she responds by spreading around the contents of her Burn Book, inciting a riot. To avoid suspicion, Regina inserts a fake libel of herself into the book in order to blame the only female students not mentioned in the book, The Plastics. Principal Ron Duvall soon quells the riot, and ends up sending all the girls in the school to gather in the gymnasium. Math teacher Sharon Norbury, whom the Burn Book slandered as a drug dealer, makes the girls mentioned in the book confess to spreading the rumors in the Burn Book and apologize to the other students and teachers. When Janiss turn comes, she confesses her plan to destroy Regina with Cadys help and openly mocks Regina with the support of the entire school. Pursued by an apologetic Cady, Regina storms out and gets hit by a school bus, breaking her Human spine|spine.

Without any friends, shunned by Aaron, and distrusted by everyone, Cady takes full blame for the Burn Book. Her guilt soon dissolves and she returns to her old personality. As part of her punishment for lying and failing Norburys class, she joins the Mathletes in their competition. There, while competing against an unattractive girl, Cady realizes that mocking the girls appearance would not stop the girl from beating her. She then realizes that the best thing to do is to just solve the problem in front of you and ends up winning the competition after her opponent answers incorrectly. At the Spring Fling dance, Cady is elected Queen, but declares that all her classmates are wonderful in their own way, whereupon she breaks her plastic tiara and distributes the pieces. Cady makes amends with Janis and Damian, reconciles with Aaron, and reaches a truce with the Plastics.

By the start of the new school year, the Plastics have disbanded. Regina joins the lacrosse team, Karen becomes the school weather reporter, and Gretchen joins the "Cool Asians." Aaron graduates from high school and attends Northwestern University, Janis and Kevin Gnapoor start dating, and Cady declares that she is now herself. Regina walks past Cady and smiles, showing that they made peace with each other. Damian witnesses the new "Junior Plastics" walking by, but they are immediately hit by a bus. It turns out, however, that this was only a humorous figment of Cadys imagination.

==Cast==
 
* Lindsay Lohan as Cady Heron 
* Rachel McAdams as Regina George
* Lizzy Caplan as Janis Ian
* Lacey Chabert as Gretchen Wieners
* Amanda Seyfried as Karen Smith Jonathan Bennett as Aaron Samuels
* Daniel Franzese as Damian
* Tina Fey as Ms. Sharon Norbury
* Tim Meadows as Principal Ron Duvall
* Amy Poehler as Mrs. George
* Rajiv Surendra as Kevin Gnapoor
* Ana Gasteyer as Cadys mom
* Neil Flynn as Cadys dad
* Daniel DeSanto as Jason
* Diego Klattenhoff as Shane Oman
* Nicole Crimi as Kylie George
* Dwayne Hill as Coach Carr
* Julia Chantrey as Amber DAlessio
* David Reale as Glen Coco
* Alisha Morrison as Lea Edwards
* Alexandra Stapley as Taylor Wedell
* Stefanie Drummond as Bethany Byrd
* Erin Thompson as Dawn Schweitzer
* Molly Shanahan as Kristen Hadley
* Ky Pham as Trang Pak
* Danielle Nguyen as Sun Jin Dinh
* Clare Preuss as Caroline Krafft
* Olympia Lukis as Jessica Lopez
* Jan Caruana as Emma Gerber
 

==Production==

===Development=== the first Saturday Night Live episode, in which she sang the song "At Seventeen", which can be heard playing in the background when the girls are fighting at Reginas house. Other characters bullying Caplans character persistently call her a lesbian throughout the film; the real Janis Ian is an out lesbian. 

===Casting===
Lindsay Lohan first read for Regina George, but the casting team felt she was closer to what they were looking for in the actress who played Cady, and since Lohan feared the "mean girl" role would harm her reputation, she agreed to play the lead. Rachel McAdams was cast as Regina because Fey felt McAdams being "kind and polite" made her perfect for such an evil-spirited character. Amanda Seyfried also read for Regina, and the producers instead suggested her for Karen due to Seyfrieds "spacey and daffy sense of humor". Both Lacey Chabert and Daniel Franzese were the last actors tested for their roles. Lizzy Caplan was at first considered too pretty for the part of Janis, for which Fey felt a "Kelly Osbourne-like actress" was necessary, but Caplan was picked for being able to portray raw emotion. Fey wrote two roles based on fellow SNL alumni, Amy Poehler (whom Fey thought the producers would not accept for being too young) and Tim Meadows, and the cast ended up with a fourth veteran of the show, Ana Gasteyer.   

===Filming=== Montclair High Convocation Hall and Sherway Gardens.

==Reception==

===Box office===
In its opening weekend, the film grossed $24,432,195 in 2,839 theaters in the United States, ranking #1 at the box office and averaging $8,606 per venue.  By the end of its run, Mean Girls grossed $86,058,055 and $42,984,816 internationally, totaling $129,042,871 worldwide. 

===Critical response===
 
Critics praised Mean Girls. Review aggregation website Rotten Tomatoes gives it a rating of 83% based on 175 reviews,  and a rating of 66% on Metacritic, indicating "generally favorable reviews", based on 39 reviews. 

==Accolades==
 
The film won and was nominated for a number of awards throughout 2004-05. 

{| class="wikitable sortable"
|- Year
!align="left"|Ceremony Category
!align="left"|Recipients Result
|- 2004
|rowspan="13"|Teen Choice Awards Choice Movie Comedy Actress Lindsay Lohan
| 
|- Choice Movie Breakout Actress Lindsay Lohan
| 
|- Choice Movie Blush Lindsay Lohan
| 
|- Choice Breakout Movie Star – Female Rachel McAdams
| 
|- Choice Breakout Movie Star – Male Jonathan Bennett Jonathan Bennett
| 
|- Choice Movie – Comedy
!
| 
|- Choice Movie Actress – Comedy Rachel McAdams
| 
|- Choice Movie Blush Rachel McAdams
| 
|- Choice Movie Chemistry Lindsay Lohan and Jonathan Bennett
| 
|- Choice Movie Fight/Action Sequence
!
| 
|- Choice Movie Hissy Fit Rachel McAdams
| 
|- Choice Movie Liar Lindsay Lohan
| 
|- Choice Movie Sleazebag Rachel McAdams
| 
|- 2005
|rowspan="4"|MTV Movie Awards MTV Movie Best Female Performance Lindsay Lohan
| 
|- MTV Movie Breakthrough Female Performance Rachel McAdams
| 
|- MTV Movie Best On-Screen Team Lindsay Lohan, Rachel McAdams, Lacey Chabert, and Amanda Seyfried
| 
|- MTV Movie Best Villain Rachel McAdams
| 
|- Kids Choice Awards Favorite Movie Actress Lindsay Lohan
| 
|-
|Peoples Choice Awards Favorite Movie: Comedy
!
| 
|- Writers Guild of America Award Best Adapted Screenplay Tina Fey
| 
|}

==Soundtrack==
{{Infobox album  
| Name     = Mean Girls: Music from the Motion Picture
| Type     = Soundtrack
| Artist   = Various Artists
| Cover    = MeanGirlsSoundtrack.jpg
| Released =  
| Recorded =1976 to 2004
| Genre    = {{flatlist| Pop
*Rock rock
*teen pop
*Contemporary R&B|R&B punk rock Christmas
*dance-pop
*Rap music|rap}}
| Length   = 49:17
| Label    = {{flatlist|
*Rykodisc
*Virgin Records|Virgin}}
| Producer =}}
{{Album ratings|title=Soundtrack
| rev1      = Allmusic
| rev1Score =   
}}

Mean Girls: Music from the Motion Picture was released on September 21, 2004, the same day as the DVD release.

  Generation X cover version|cover) God Is Pink
# "Milkshake (song)|Milkshake" by Kelis
# "Sorry (Dont Ask Me)" by All Too Much
# "Built This Way" by Samantha Ronson Blondie cover)
# "Overdrive (Katy Rose song)|Overdrive" by Katy Rose Blondie
# Peaches
# "Misty Canyon" by Anjali Bhatia
# "Mean Gurl" by Gina Rene and Gabriel Rene
# "Hated" by Nikki Cleary
# "Psyché Rock", by Pierre Henry (Fatboy Slim Malpaso mix)
# "The Mathlete Rap" by Rajiv Surendra
# "Jingle Bell Rock"
 Naughty Girl" Christina Aguilera, Orbital and "Loves Theme" by The Love Unlimited Orchestra.

Rolfe Kent wrote the films orchestral score, which was orchestrated by Tony Blondal. The score features taiko drums and a full orchestra.

==Home media==
 
Mean Girls was released on VHS and DVD in North America on September 21, 2004, five months after it opened in theaters. It was released in a widescreen special collectors edition and a fullscreen collectors edition, both including several deleted scenes, a blooper reel, three interstitials, the theatrical trailer, previews, and three featurettes.    A Blu-ray version of the film was released on April 14, 2009.

==Legacy and cultural impact==
The film has become a pop-culture phenomenon.   Fans have made GIFs and memes of the film and posted them on social media, including Facebook, Twitter, and Tumblr.   

Mariah Carey expressed several times that she is a fan of the film, using some quotes from the film in several interviews. Carey released a single, "Obsessed (Mariah Carey song)|Obsessed", which begins with an interlude quote where she says, "And I was like, Why are you so obsessed with me?", a line said by Regina in the film. Careys husband, Nick Cannon revealed the song was inspired by the film itself.  She then referenced the film again in 2013 during an episode of American Idol.

In August 2013, the White House tweeted a quote from the film and made a reference to President Obamas dog, Bo (dog)|Bo.   Taco Bell tweeted a reply to the tweet, also using one of the quotes from the film. 

In an interview about the film, Fey noted, "Adults find it funny. They are the ones who are laughing. Young people watch it like a reality show. Its much too close to their real experiences so they are not exactly guffawing."  Entertainment Weekly put it on its end-of-the-decade, "best-of" list, saying, "Fetch may never happen, but 2004s eminently quotable movie is still one of the sharpest high school satires ever. Which is pretty grool, if you ask me!"  In 2006, Entertainment Weekly also named it the twelfth best high school film of all time, explaining: "There was a time when Lindsay Lohan was best known for her acting rather than her party-hopping. Showcasing Lindsay Lohan in arguably her best role to date, this Tina Fey-scripted film also boasts a breakout turn by Rachel McAdams as evil queen bee Regina George (Gretchen, stop trying to make fetch happen! Its not going to happen!). While Mean Girls is technically a comedy, its depiction of girl-on-girl cattiness stings incredibly true."

At the 2013 Peoples Choice Awards, Jennifer Lawrence mentioned the film in her speech when she won "Favorite Movie Actress"  

October 3 has been dubbed on social media as "Mean Girls Day" in reference to a quote from the movie. 

===Video game===
 
A game for PC and Nintendo DS was released in 2009.  The video game features characters specifically created for the game.

===Stand-alone sequel===
 
A Direct-to-video|direct-to-DVD sequel, Mean Girls 2, was aired on ABC Family on January 23, 2011, and released on DVD on February 1. The film is a stand-alone sequel, and the plot does not continue the story of the first film nor does it have the same cast, with the exception of Tim Meadows. The film is directed by Melanie Mayron and stars Meaghan Martin and Jennifer Stone.

===Mean Moms===
  New Line announced a planned release date of May 8, 2015 for a proposed spin-off of Mean Girls  with Jennifer Aniston in talks to lead. 

Adapted from another book penned by Rosalind Wiseman, Mean Moms would be written by Sean Anders and John Morris, and would star Jennifer Aniston as a mom facing the cut-throat life of modern suburbia. However, in May 2014, New Line Cinema pulled the film from its proposed release date of May 2015; even though the film is still slated for development, there is not currently a release date for the spin-off.  On October 7, it was announced that the film was added to the California Film Tax Credit program for the 2014-15 fiscal year, in which the production must start in California within 180 days of notification from the state to receive the $6.7 million production tax credit.  in May 2015, it was confirmed the project was still happening and Sean Anders would now direct the film. 

===Stage musical=== Paramount will also be involved. 

===Potential sequel===

In late September 2014 it was revealed  that Lindsay Lohan pitched an idea to Mean Girls writer Tina Fey for a sequel. In October 2014, it was announced  that Lohan convinced other cast members of the original film to persuade Fey to write a screenplay for a sequel. The idea was brought up during a 10th anniversary for the film in People magazine. 

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 