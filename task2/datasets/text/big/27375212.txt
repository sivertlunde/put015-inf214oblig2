Big Bad John (film)
{{Infobox film
| name           = Big Bad John
| image_size     = 
| image	=	Big Bad John FilmPoster.jpeg
| caption        = 
| director       = Burt Kennedy
| writer         = Joseph Berry Burt Kennedy C.B. Wismar (story)
| producer       = Michael Emerson (line producer (as Michael S. Emerson) Patrick Peach (associate producer) Ron Rothstein (associate producer) Red Steagall (producer) Karen Theman (associate producer)
| narrator       = 
| starring       = Ned Beatty
| music          = Ken Sutherland
| cinematography = Ken Lamkin
| editing        = John W. Wheeler
| studio         = Thunderhead Productions
| distributor    = Magnum Video Double Helix Pictures
| released       = 1990
| runtime        = 92 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} song the film is based upon.  

==Cast==
*Ned Beatty as Charlie
*Jimmy Dean as Cletus Morgan
*Jack Elam as Jake
*Doug English as Big Bad John Tyler
*Bo Hopkins as Lester
*Romy Windsor as Marie Mitchelle
*Jeff Osterhage as Alvin Mahoney
*Ned Vaughn as Billy Mahoney
*Buck Taylor as Bob Simmons
*Amzie Strickland as Nellie
*Jerry Potter as Blany (as Jerry Potter)
*Red Steagall as Monahan
*Danny Kamin as Jacque (as Dan Kamin) Anne Lockhart as Lady Police Officer

==References==
 

==External links==
* 

 

 
 