Veera Madakari
{{Infobox film
| name       = Veera Madakari
| image      = 
| director   = Sudeep
| producer   = Dinesh Gandhi
| based on   =  
| writer     = Sudeep
| starring   =  
| music      = M. M. Keeravani
| cinematography = Sri Venkat
| editing    = B S Kemparaj
| studio     = Shankar Productions
| distributor = 
| released   =  
| country    = India
| language   = Kannada
| runtime    = 175 minutes
| budget     = 
| gross      = 
}} Kannada action-comedy Telugu blockbuster Vikramarkudu. 

==Plot==

The story of Veera Madakari revolves around Sathyaraju (Sudeep), a lowly fraudster, who is in love with Neeraja (Ragini Dwivedi). Madakari (Sudeep), an honest police officer who is murdered by the villain, Babjee (Gopinath) and his son Munna (Arun Sagar). After Madakaris death, Sathyaraju (his look-alike) takes over and does away with the villains in the small village of Devagarh terrorised by Babjee.

==Cast==
* Sudeep ... Madakari / Muttati Satyaraju
* Ragini Dwivedi ... Neeraja Goswamy
* Tennis Krishna ...
* Devaraj ... Guest Appearance
* Doddanna ... Minister
* Dharma
* Arun Sagar
* Vanishri
* Simran Khan (Special appearance)

==Character map of Veera Madakari and its remakes==
{| class="wikitable" style="width:50%; text-align:center;"
|- style="background:#ccc;"
|   (2012) (Cinema of West Bengal|Bengali) || Rowdy Rathore (2012) (Bollywood|Hindi)
|- Manna ||Sudeep || Karthi || Prosenjit Chatterjee || Akshay Kumar
|- Ragini Dwivedi || Tamannaah || Richa Gangopadhyay || Sonakshi Sinha
|}

==Production==
Actor-director Sudeep is well known for making successful remakes, especially from Telugu language box-office hits. Sudeep in the film casts himself in two different roles and has also given his voice to some songs. The film, as a remake of Vikramarkudu, successfully maintains the original script of its Telugu counterpart.  |url=http://www.nowrunning.com/movie/6247/kannada/veera-madakari/2073/review.htm|publisher=nowrunning.com|accessdate=13 June 2013}} 

==Soundtrack==
{{Infobox album
| Name = Veera Madakari
| Type = soundtrack
| Artist = M. M. Keeravani Kannada
| Label = Anand Audio
| Producer = 
| Cover = 
| Released    = January 01, 2009 Feature film soundtrack
| Last album = 
| This album = 
| Next album = 
}}

The film has 6 songs.  

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Jinta Ta Kaviraj
| extra1        = Sudeep, Tennis Krishna, Anuradha Bhat, Drum Prakash
| length1       = 
| title2        = Dummare Dumma
| lyrics2 	= Shridhar
| extra2        = Shankar Mahadevan, Akanksha Badami, Sunita
| length2       = 
| title3        = Jum Jum Maaya
| extra3        = Ajay Warrior, Anuradha Bhat
| lyrics3 	= Kaviraj
| length3       = 
| title4        = Manjari Manjari
| extra4        = Sinchana Dixit
| lyrics4       = V. Nagendra Prasad
| length4       =
| title5        = Jo Laali
| extra5        = Akanksha Kumari
| lyrics5       = K C Kiran
| length5       = 
| title6        = Veera Madakari
| extra6        = Satish Aryan and chorus
| lyrics6       = Satish Aryan
| length6       =
}}

==Review==
The reviews of the film are favorable regarding the rendition of songs, choreography and the dual role of the protagonist but the film suffers because of its long duration and its sluggish pace in the first half of the movie. Sudeep excels more as an actor rather than a film director. Ragini as a debutante is imposing.    Sudeep does justice to his dual role, so does Tennis Krishna. Dharma is impressive in the sentimental role. Camera work and the action scenes of the movie are worth commenting. 
chitraloka  {{cite web|url=http://www.chitraloka.com/movie-reviews/759-veera-madakari-review.html|title=
Veera Madakari Movie Review|publisher=chitraloka.com|date=20 March 2009   }} 

==Response==
Veera Madakari completed 100 days and it was blockbuster movie of 2009. 

==Awards==
{| class="infobox" style="width: 22.7em; text-align: left; font-size: 85%; vertical-align: middle; background-color: #eef;"
|-
| colspan="3" |
{| class="collapsible collapsed" width="100%"
! colspan="3" style="background-color: #d9e8ff; text-align: center;" | Awards and nominations
|- style="background-color:#d9e8ff; text-align:center;"
!style="vertical-align: middle;" | Award
| style="background:#cceecc; font-size:8pt;" width="60px" | Wins
| style="background:#eecccc; font-size:8pt;" width="60px" | Nominations
|-
|align="center"|
;Suvarna Film Awards
| 
| 
|-
|align="center"|
;Zee Kannada Innovative Film Awards
| 
| 
|}
|- style="background-color:#d9e8ff"
| colspan="3" style="text-align:center;" | Totals
|-
|  
| colspan="2" width=50  
|-
|  
| colspan="2" width=50  
|} 2nd Suvarna Film Awards :- Favorite Hero - Winner - Sudeep    Best Debut Actress - Winner - Ragini Dwivedi        Best Choreographer - Nominated - Pradeep Antony     Star Pair of the Year - Nominated - Sudeep & Ragini Dwivedi  

Zee Kannada Innovative Film Awards :-
* Special Jury Award - Winner - Sudeep   
* Best Child Actor - Winner - Baby Jorusha       

==References==
 

==External links==
*  

 

 
 
 
 
 
 