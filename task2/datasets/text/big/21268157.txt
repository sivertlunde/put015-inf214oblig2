Stars of the Russian Ballet
 
{{Infobox film
| name           = Stars of the Russian Ballet
| image          = 
| caption        = 
| director       = Gerbert Rappaport
| producer       = 
| writer         = 
| starring       = Galina Ulanova
| music          = 
| cinematography = Semyon Ivanov
| editing        = M. Bernatsky D. Lander
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}}

Stars of the Russian Ballet ( ) is a 1953 Soviet musical film directed by Gerbert Rappaport. It was entered into the 1954 Cannes Film Festival.    The film features excerpts from popular Russian ballets performed by the most famous stars of the Russian ballet. The passages include scenes from the French Revolution themed ballet Flames of Paris and Swan Lake.

==Cast==
* Galina Ulanova - Maris (segment "Fountains of Bakhchisarai")
* Konstantin Sergeyev - The Prince (segment "Swan Lake")
* Natalya Dudinskaya - Odille, the black swan (segment "Swan Lake")
* V. Bakanov - The Evil Wizard (segment "Swan Lake")
* Pyotr Gussyev - Girzi (segment "Fountain of Bakhchisarai")
* Maya Plisetskaya - Zarema (segment "Fountains of Bakhchisarai")
* Yuri Zhdanov - Vaclav (segment "Fountain of Bakhchisarai")
* Vakhtang Chabukiani - Philippe (segment "The Flames of Paris")
* Muza Gotlib - Jeanne (segment "Flames of Paris")

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 