Gaanam
{{Infobox film 
| name           = Gaanam
| image          =
| caption        =
| director       = Sreekumaran Thampi
| producer       = Sreekumaran Thampi
| writer         = Sreekumaran Thampi
| screenplay     = Sreekumaran Thampi Lakshmi Jagathy Hari
| music          = V. Dakshinamoorthy
| cinematography = C Ramachandra Menon
| editing        = K Narayanan
| studio         = Ragamalika
| distributor    = Ragamalika
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, Lakshmi in the lead roles, along with Jagathy Sreekumar, Adoor Bhasi, Nedumudi Venu and Hari (actor)|Hari. The film had musical score by V. Dakshinamoorthy.   

==Cast==
 
*Ambareesh Lakshmi
*Jagathy Sreekumar
*Adoor Bhasi
*Nedumudi Venu Hari
*Vaikkam Mani
*Kalaranjini
*Babu Namboothiri
*Baby Ponnambili
*Bhagyalakshmi
*Kailasnath
*Kuttyedathi Vilasini
*Master Rajakumaran Thampi
*PK Venukkuttan Nair
*Poornima Jayaram
*Shanavas
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Sreekumaran Thampi, Swathi Thirunal, Thyagaraja, Irayimman Thampi, Unnai Warrier, Muthuswamy Dikshithar and Jayadevar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aalaapanam || K. J. Yesudas, S Janaki || Sreekumaran Thampi || 
|-
| 2 || Aalaapanam   || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 3 || Aarodu Cholvene || K. J. Yesudas, Vani Jairam || Sreekumaran Thampi || 
|-
| 4 || Adri Suthaavara || K. J. Yesudas, P Susheela, M Balamuralikrishna || Swathi Thirunal || 
|-
| 5 || Aliveni enthu cheyvu || P Susheela || Swathi Thirunal || 
|-
| 6 || Guruleka   || M Balamuralikrishna || Thyagaraja || 
|-
| 7 || Karuna Cheyvaanenthu Thaamasam || Vani Jairam || Irayimman Thampi || 
|-
| 8 || Maanasa || S Janaki ||  || 
|-
| 9 || Sarvardhu Ramaneeya || Kalamandalam Sukumaran, Kalanilayam Unnikrishnan || Unnai Warrier || 
|-
| 10 || Sindooraaruna Vigrahaam || S Janaki ||  || 
|-
| 11 || Sree Mahaaganapathim || M Balamuralikrishna || Muthuswamy Dikshithar || 
|-
| 12 || Yaa Ramitha || M Balamuralikrishna || Jayadevar || 
|}

==References==
 

==External links==
*  

 
 
 

 