Dishyum
 

{{Infobox film
| name = Dishyum
| image = Sasi
| Sasi
| Sandhya Nassar(actor)|Nassar Guinness Pakru Venkat Prabhu Annee Malavika
| producer = Viswanathan Ravichandran
| music = Vijay Antony Aascar Film Pvt. Ltd
| cinematography = Sentonio Dersio
| released = February 2, 2006
| runtime = 137 minutes Tamil
| budget =
}}
 Tamil film Nassar and Anee Malavika. The films score and soundtrack are composed by Vijay Antony of Sukran fame. The story tells a love relationship between a stuntman and an art student providing plenty of exciting scenes along the way.

==Plot==
The movie is all about an affair between a stuntman and a student who pursues courses on arts and sculpture. A case of persons of two different thoughts and two extremes coming together.

Risk Bhaskar (Jiiva), a stuntman in films, comes across Cinthya (Sandhya (actress)|Sandhya), a college student. A couple of meetings help them to get acquainted with each other. Through Baskar she comes to know of the difficulties stuntmen face in their everyday work. Life is a daily risk for them, she understands.

Cinthya’s care and affection makes Baskar develop love towards her. However coming to know that she is not interested in reciprocating his love, he wants to leave from her life. Missing his company, Cinthya calls on him and urges him to be himself and continue their friendship.

Not wanting to miss his company, Cinthya promises to marry Baskar in case if she develops a love towards him in future or if not they would part as friends after sometime. Baskar, a happy-go-lucky youngster, tries all means to impress Cinthya.
 Anee Malavika), Cinthyas mother, coming to know of their relationship opposes it. Meanwhile, she loses her husband Jayachandran (Nassar), a fire and rescue personnel, while he is dousing a major blaze. She immediately decides to get her daughter married and even arranges her wedding. The rest is about interesting turn of events and an answer to the question whether Baskar and Cinthya reunite.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- Jiiva || Risk  Bhaskar
|- Sandhya || Cinthya
|-
| Guinness Pakru || Amitabh
|- Nassar || Jayachandran
|- Anee Malavika || Malar
|-
| SJ Suryah || Cameo    
|- Vishal || Cameo     
|}

==Production== Pooja to play the lead role in January 2004, before Sandhya replaced her. 

==Soundtrack==
{{Infobox album|  
| Name        = Dishyum: The Official Motion Picture Soundtrack |
| Type        = Soundtrack |
| Artist      = Vijay Antony |
| Cover       =  |
| Released    =  |
| Recorded    =  | Feature film soundtrack |
| Length      = 22:34 |
| Label       = The Best Audio |
| Producer    = Vijay Antony |
| Reviews     = 
| Last album  = Iruvar Maatum  (2006)
| This album  = Dhisyum
| Next album  = Ninaithale
}}
The lyrics were penned by Vairamuthu, Vijay Anthony and Pugazhendhi.   
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse:collapse; font-size:95%;"
|- bgcolor="#CCCCCF" align="center"

| No. || Song || Singers || Length (m:ss) || Lyricist || Notes
|-
| 1|| Bhoomiku || Gayathri,   ||
|-
| 2|| Dailamo Dailamo ||   ||
|-
| 3|| Kitta Neringivaadi || Gayathri,   ||
|-
| 4|| Nenjangootil || Jayadev, Rajalakshmi || 4:36 || Vairamuthu ||
|-
| 5|| Poo Meedhu || Malgudi Subha  || 4:36 || Pugazhendhi ||
|}

==Reception==
Nowrunning.com review said that "it is an absorbing story telling with minimum fuss for the youth" and gave it 3/5. 
Indiaglitz.com review said that "is a feel-good movie for youngsters". 
Sify.com gave a verdict that the film is "enjoyable". 
* The film grossed $1 million at the box office.
* This film was made after Jiivas success of Raam (2005 film)|Raam.
* It also starred Guinness Pakru, a promising 3&nbsp;ft dwarfism|dwarf.
* It was Sasi (director)|Sasis third film after Sollamalae and Roja Kootam.

==References==
 

==External links==
 
*  

 

 
 
 
 