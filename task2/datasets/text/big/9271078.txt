Adventures of Don Juan
{{Infobox film
| name           = Adventures of Don Juan
| image          = Adventures of Don Juan.jpeg
| caption        = Original film poster
| director       = Vincent Sherman
| producer       = Jerry Wald
| writer         = Herbert Dalmas George Oppenheimer
| starring       = Errol Flynn Viveca Lindfors
| music          = Max Steiner
| cinematography = Elwood Bredell
| editing        = Alan Crosland
| distributor    = Warner Bros.
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English Spanish
| budget         = $3.5 million Glancy, H. Mark. "Warner Bros film grosses, 1921-51." Historical Journal of Film, Radio and Television. March 1995. 
| gross          = $5 million 
}} adventure Technicolor romance film made by Warner Bros. It was directed by Vincent Sherman and produced by Jerry Wald from a screenplay by George Oppenheimer and Harry Kurnitz based on a story by Herbert Dalmas, with uncredited contributions by William Faulkner and Robert Florey.
 Robert Douglas, Alan Hale, Mary Stuart.

The film was originally to be scored by Erich Wolfgang Korngold, however, production of the film was postponed until 1947, by which time Korngold had retired from scoring motion pictures.  He was therefore replaced by Max Steiner.

==Plot==
  Queen Margaret (Viveca Lindfors), asking her to provide an opportunity at the court to rehabilitate Don Juan after the gossiping and rumors about his multiple illicit love affairs.  He is thus hired as a fencing instructor at the Spanish Academy.
 King Phillip Robert Douglas), who is plotting to depose the monarch, usurp power in Spain and declare war on England. With the support of his friends, Don Juan defends the Queen, the King and the loyal Count de Polan against Duke de Lorca and his henchmen. 

==Cast==
* Errol Flynn&nbsp;– Don Juan de Maraña
* Viveca Lindfors&nbsp;– Queen Margaret of Spain Robert Douglas&nbsp;– Duke de Lorca Alan Hale&nbsp;– Leporello
* Romney Brent&nbsp;– King Phillip III of Spain
* Ann Rutherford&nbsp;– Dona Elena
* Robert Warwick&nbsp;– Don Jose, Count de Polan
* Jerry Austin&nbsp;– Don Sebastian Douglas Kennedy&nbsp;– Don Rodrigo Jean Shepherd&nbsp;– Donna Carlotta (as Jeanne Shepherd)  Mary Stuart&nbsp;– Catherine
* Helen Westcott&nbsp;– Lady Diana
* Fortunio Bonanova&nbsp;– Don Serafino Lopez
* Aubrey Mather&nbsp;– Lord Chalmers Una OConnor&nbsp;– Duenna
* Raymond Burr&nbsp;– Captain Alvarez
* Nora Eddington&nbsp;– young woman asking for direction
* Tim Huntley&nbsp;– Cecil, Catherines husband
* Leon Belasco&nbsp;– Don de Cordoba David Leonard&nbsp;– Innkeeper 
* Barbara Bates&nbsp;– Innkeepers daughter, Micaela DVD scene 7, at the inn called La Casa Rosada over the gate 
* Monte Blue&nbsp;– Turnkey David Bruce&nbsp;– Count de Orsini

==Notes==
The film was originally meant to be directed in May 1945 under the direction of Raoul Walsh but was postponed for two and a half years due to script problems and industrial unrest in Hollywood after the war. Tony Thomas, Rudy Behlmer & Clifford McCarty, The Films of Errol Flynn, Citadel Press, 1969 p 156-157  Warner Bros were encouraged in 1947 by a successful reissue of The Sea Hawk and The Adventures of Robin Hood.
 Tony Thomas, Don Juan, Flynns idol John Barrymore performed a similar leap without a stunt double.

At the end of the film, the young woman in the coach asking Don Juan for directions is Flynns wife, Nora Eddington.

The film is the last of 13 in which Alan Hale appeared with his close friend Errol Flynn. Hale died on January 22, 1950, just over a year following this films release.
 Ian Fraser George Hamilton 1981 comedy Zorro, The Gay Blade.   A portion was also used in two scenes in the film The Goonies, although in the first scene, it accompanied a TV broadcast of the earlier film The Sea Hawk.
 The Adventures of Robin Hood, and is then followed by a grand procession with recycled outtakes from the 1939  "The Private Lives of Elizabeth and Essex", both starring Errol Flynn and Alan Hale. As mentioned above, this would be their final film together.

==Awards==
The film won the Academy Award for Best Costume Design, Color (Leah Rhodes, Travilla and Marjorie Best) and was nominated for the Academy Award for Best Art Direction-Set Direction, Color (Edward Carrere, Lyle Reifsnider).   

==Reception==
The film was very successful in Europe but less so in the US and struggled to recoup its large budget. From this point on, Warner Bros would reduce the budget of Flynns films. 

It recorded admissions of 3,763,314 in France, making it the 7th most popular movie in the country that year. 

==References==
 

==External links==
*  
*  
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 