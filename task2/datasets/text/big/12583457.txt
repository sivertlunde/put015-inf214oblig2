The Accompanist
{{Infobox film
| name           = The Accompanist
| image          = Accompanistposter.jpg
| image size     =
| caption        = North American release poster
| director       = Claude Miller
| producer       = Jean-Louis Livi
| writer         = Luc Béraud Nina Berberova Claude Miller Claude Rich
| narrator       =
| starring       = Richard Bohringer Yelena Safonova Romane Bohringer
| music          = Alain Jomy
| cinematography = Yves Angelo
| editing        = Albert Jurgenson	
| distributor    = AMLF
| released       = November 11, 1992
| runtime        = 102 minutes
| country        = France
| language       = French
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1992 French film directed by Claude Miller from a novel by Nina Berberova, and starring Romane Bohringer, Yelena Safonova and Richard Bohringer.

== Plot ==
In 1942 Nazi-occupied Paris, a young and impoverished accompanist named Sophie Vasseur gets a job with famed singer Irene Brice. As Irenes possessive husband and manager, Charles, a businessman collaborating with the Nazis, wrestles with his conscience, the highly impressionable Sophie becomes obsessed with Irene, taking on the role of maid as well as accompanist, living life vicariously through Irenes triumphs and affairs, especially romantic. When Irene relocates to London, Sophie goes along, much to the discomfort of Charles.

== External links ==
* 

 

 
 
 
 
 
 
 
 

 