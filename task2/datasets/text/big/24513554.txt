Safar (film)
 

{{Infobox film
| name = Safar
| image =Safarfilm.jpg
| caption = DVD cover Asit Sen
| producer = Mushir-Riaz
| writer = Asit Sen  (screenplay)  Inder Raj Anand  (dialogue)  Anil Ahuja  (dialogue) 
| narrator =
|story= Ashutosh Mukherjee (writer)| Ashutosh Mukherjee screenplay = 
| starring =Rajesh Khanna Sharmila Tagore Feroz Khan
| music = Kalyanji Anandji Indeevar (lyrics)
| cinematography = Kamal Bose
| editing = Tarun Dutta
| distributor = M.R. Productions
| released =1970
| runtime = 140 min
| country = India
| language = Hindi
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
 Indian Bollywood|Hindi Asit Sen, based on a novel by Bengali writer Ashutosh Mukherjee (writer)| Ashutosh Mukherjee. The film stars Ashok Kumar, Rajesh Khanna, Sharmila Tagore and Feroz Khan in lead roles. The film became the tenth top-grossing production of the year. It won one Filmfare Awards and four BFJA Awards.

==Plot==
Neela (Sharmila Tagore) and Avinash (Rajesh Khanna) are medical students, and they quite sympathise with each other. However, Avinash lives a poor lifestyle and does not want Neela to live this kind of life. Actually, the main reason behind his unwillingness to live with Neela is that he comes to know that he is cancer patient and that he will not be able to go with Neela very far which he hides from everybody including Neela. When Neela meets one of her students elder brother, businessman Shekhar (Feroz Khan), she instantly catches his eye. He goes to propose marriage to her but her brother, Kalidas (I. S. Johar) advises him to first meet with Avinash. Shekhar meets Avinash, tells him of his feelings and intentions, and Avinash immediately approves of him. Shekhar and Neela get married, and cannot understand why Avinash did not attend the ceremony. After a successful honeymoon, both return to their regular lives, and Shekhar wants Neela to continue with her medical studies. It is then that he suspects that Neela has been meeting with Avinash, and asks his brother to spy on her. The information that Shekhar finds lead him to the conclusion that Neela is in love with Avinash, and confronts her. An intense argument is followed by Shekhar drinking his medicine, and passing away. The next day Neela is arrested on the charge of murder. Surprisingly, Avinash is nowhere to be found. Neela has now to face the trial all alone.

Mrs Kapoor (Nadira) testifies in favor of Neela, the court acquits her and Dr Chandra (Ashok Kumar) takes her in his guidance for making her a top surgeon.

==Cast==
* Ashok Kumar ...  Dr. Chandra
* Sharmila Tagore ...  Mrs. Neela Shekhar Kapoor
* Rajesh Khanna ...  Avinash
* Feroz Khan ...  Shekhar Kapoor
* I. S. Johar ... Kalidas
* Aruna Irani ... Laxmi Nadira ... Mrs. Kapoor
Supported by Mahesh Kumar, Brahmchari, Iftikar, Birbal, Master Sachin, Jagdish Raj, Ratan Gaurang, Parsuram, Narbada Shanker, Avtar Singh & Oscar.

==Crew==
* Producer: Mohammad Riaz Asit Sen
* Story: Ashutosh Mukherjee
* Screenplay: Asit Sen
* Dialogues: Inder Raj Anand
* Lyrics: Indeevar
* Music: Kalyanji Anandji
* Choreography: Kamal Bose
* Editing: Tarun Dutta
* Art direction: Sudhendu Roy
* Costume Design: Shalini Shah

==Reception and awards==
Safar became the tenth highest grossing film of the year in India.  In a retrospective review, Rediff.com wrote, "Safar is a story of ordinary people grappling with staggering challenges and compromises. But in this refreshingly non-melodramatic fare, a murmur of protest, an escaped sob and a half-concealed smirk are the only emotional luxuries its characters afford themselves in the inexplicable journey of life, the eponymous safar of the title." 

Safar received two nominations at the annual Filmfare Awards and won one award. Asit Sen received his first Best Director award (he was previously nominated in this category for Mamta (1966 film)|Mamta in 1967), though the film was not nominated for the Best Film award.  Sharmila Tagore received her second Best Actress nomination (she won the previous year for Aradhana (1969 film)|Aradhana). Awards and nominations are listed below:
*Won, Filmfare Best Director Award - Asit Sen
*Nominated, Filmfare Best Actress Award - Sharmila Tagore Bengal Film Journalists Association acknowledged Safar as the eighth best Indian film of 1970, and gave it three more awards: 
*BFJA Awards, Best Screenplay (Hindi) - Asit Sen
*BFJA Awards, Best Dialogue (Hindi) - Inder Raj Anand
*BFJA Awards, Best Editing (Hindi) - Tarun Dutta

==Music==
{{Infobox album|
| Name = Safar
| Type = Album
| Artist = Kalyanji Anandji
| Cover =
| Released =   1970 (India)
| Recorded = Feature film soundtrack
| Length =
| Label =  Sa Re Ga Ma
| Producer = Kalyanji Anandji
| Reviews =
| Last album = Geet (1970 film)|Geet (1970)
| This album = Safar (1970)
| Next album = Holi Ayee Re (1970)
}}

The soundtrack of the film contains 5 songs. The music is composed by Kalyanji Anandji, with lyrics authored by Indeevar.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Song !! Singer(s)
|-
|1.
| "Hum The Jinke Sahare" Lata Mangeshkar
|-
|2.
| "Jeevan Se Bhari" Kishore Kumar
|-
|3.
| "Jo Tumko Ho Pasand" Mukesh (singer)|Mukesh
|-
|4.
| "Nadiya Chale" Manna Dey, Rajesh Khanna
|-
|5.
| "Zindagi Ka Safar" Kishore Kumar
|}

==References==
 

==External links==
* 

 
 
 
 
 