When Women Had Tails
{{Infobox film
| name           = When Women Had Tails
| image          = When Women Had Tails.jpg
| alt            = 
| caption        = Original film poster
| director       = Pasquale Festa Campanile
| producer       = Silvio Clementelli
| writer         = Marcello Coscia Lina Wertmüller Pasquale Festa Campanile
| starring       = Senta Berger Giuliano Gemma Lando Buzzanca
| music          = Ennio Morricone
| cinematography = Franco Di Giacomo
| editing        = 
| studio         = 
| distributor    = Film Ventures International
| released       =  
| runtime        = 90 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}
When Women Had Tails ( ) is a 1970 Italian comedy film set in pre-historic times when “women had tails” and were hunted by cavemen. It stars Giuliano Gemma, Senta Berger, and Lando Buzzanca. It was followed by When Women Lost Their Tails. 

==Plot==
Seven orphan cavemen grow up on a little island all by themselves. After a fire burns all the vegetation, they set out to find a new place to live. One day they trap a strange animal, looking very similar to them, only softer and with longer hair.

She, Filli (Senta Berger), is attracted to one of the brothers, Ulli (Giuliano Gemma), and convinces him that a certain kind of playing with each other is far more satisfying than just eating her.

==Cast==
* Senta Berger: Filli
* Giuliano Gemma: Ulli
* Lando Buzzanca: Kao Frank Wolff: Grr
* Renzo Montagnani: Maluc
* Lino Toffolo: Put
* Francesco Mulé: Uto
* Aldo Giuffrè: Zog
* Paola Borboni: leader of the tribe of cave women

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 


 
 