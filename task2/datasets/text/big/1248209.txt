Volcano High
 
{{Infobox film name           = Volcano High image          = Volcanohigh.jpg caption        = Volcano High promotional movie poster director  Kim Tae-kyun producer       = Cha Seung-jae writer         = Kim Tae-kyun Seo Dong-heon Jeong An-cheol starring       = Jang Hyuk Shin Min-ah Kim Su-ro Kwon Sang-woo Heo Joon-ho music          = Park Young cinematography = Choi Young-taek editing        = Ko Im-pyo distributor    = Cinema Service released       =   runtime        = 121 minutes country        = South Korea language       = Korean budget         = film name      = {{Film name hangul         =   hanja          =   rr             = Hwasango mr             = Hwasango}}
}}
 2001 Cinema South Korean martial arts action comedy film in the same vein as Tenjho Tenge. 

It revolves around a troublemaking high school student named Kim Kyung-soo (portrayed by Jang Hyuk) who finds himself transferred to the last school that will take him, the prestigious Volcano High, an institution whose students display an incredible talent in martial arts, with a few demonstrating even more mysterious psychic powers. Kyung-soo is drawn into fights between different clubs, a manuscript that is told to hold great power, and a group of teachers that will do whatever possible to keep the students in line. 
 Korean film of 2001 with 1,687,800 admissions nationwide. 

== Prologue ==
17 years of feuding, sparked by the Great Teachers Battle, has stripped authorities of their power, as self-indulgence, disguised as self-control, grips the student body. The schools have fallen into disarray. However, there is a legend. The one who acquires the Secret Manuscript will end the chaos. It is a legend that disrupts the Martial Court of Volcano High. Now, in the 108th year of Volcano...

== Cast and characters ==
*  . Volcano High is the last school that will accept him and hes determined to graduate. Kyung-soo has a history with Mr. Ma and was reluctant to use his powers to help the students out with their struggle to find the manuscript, fearing expulsion and shaming his family.

*   team and love interest of Kyung-soo.

*   team. One of the most powerful and feared fighters at Volcano High. He helps the vice principal Hak-sa frame and imprison Hak-rim for stealing the manuscript so he can take control of the school. Ryang is in love with Chae-yi and wants her to be his queen. He despises Kyung-soo, seeing that hes much stronger in a fight than Ryang himself.
 framed by Jang Ryang for trying to steal the manuscript. Realizing that Kyung-soo is Volcano Highs only hope, Hak-rim trains him.

* Gong Hyo-jin as So Yo-seon: Described as "Single Hearted." Co-captain of the kendo team and admirer of Song Hak-rim. She was the first to believe that he was framed and that Jang Ryang was involved.
 rugby team. Befriends Kyung-soo.

* Heo Joon-ho as Mr. Ma: Leader of the 5 Teachers who are sent to discipline the students. He has a history with Kyung-soo and is the secondary antagonist.

* Byun Hee-bong as Vice Principal Jang Hak-sa: The main antagonist in Volcano High. Angry due to the fact he isnt the principal and in spite of his own fears of Jang Ryang, Hak-sa enlisted his help to frame Hak-rim for stealing the manuscript and he can be imprisoned. When Hak-sa realizes the Dark Oxen has taken control of Volcano High, he enlists the help of the 5 Teachers to discipline the students.

* Jung Sang-hoon as Golbangi/Woo-ping: a member of the Dark Oxen who befriends Kyung-soo. He warns him of Chae-yis reputation as Icy Jade when Kyung-soo accidentally spilled a bucket of water on her. Golbangi often plays masseuse to Ryang.

== Soundtrack ==
The movie released different soundtracks in South Korea, in Japan and in the US. The original Korean soundtrack was scored by Park Young and features mostly rock music, including two songs from Korean nu metal band R.F. Children. The Japanese soundtrack was written by Daita (former guitarist of Siam Shade), and is similar in style, with rock music. The US version is a completely altered version from the original featuring hip hop music.

== American version == hip hop artists Snoop Dogg, Method Man, Lil Jon, Big Boi, and Mýa.
* Andre 3000 as Kim Kyung-soo
* Lil Jon as Jang Ryang
* Mya as Yoo Chae-yi (renamed Jade)
* Snoop Dogg as Song Hak-rim
* Method Man as Mr. Ma
* Pat Morita as Vice Principal Jang Hak-sa (renamed Ko)
* Big Boi as Shimma
* Kelis as So Yo-seon (renamed Song)
* Tracy Morgan as Woo-ping

This Kung Faux parody version of the film aired only on MTV.

=== Differences Between the Korean and American versions ===
{| class="wikitable" width="100%"
|- bgcolor="#CCCCCC"
! American!! Korean
|- 
| Nearly 30 minutes edited out with scenes rearranged to a running time of nearly 80 minutes. || The movie is much longer with 122 minutes.
|-
| Song Hak-rim is imprisoned because Jang Ryang framed him for trying to poison the principal. || Song Hak-rim is imprisoned because Jang Ryang framed him for trying to steal the manuscript.
|-
| The main plot involves 5 teachers who are sent to discipline the students. || While this plot is still in the Korean version, the main plot is actually about a manuscript that is told to hold great power. Many want to know its location while the 5 teacher plot is actually a subplot.
|- 
| Kyung-soo is portrayed as a daydreamer and his recurring dream is about a girl whom he meets in the forest. || This girl is actually Shimmas fraternal twin sister and she sends him into the forest to seduce him to play rugby.
|- 
| So Yo-seon is Song Hak-rims girlfriend. ||  So Yo-seon merely admires Hak-rim from a distance while writing letters he would never get to read. 
|- 
| Jang Ryang and Yoo Chae-yi used to date, but Chae-yi dumped him and Ryang hopes to get back together. || The two were never together and Jang Ryang only confessed his feelings for her.
|- The music hip hop released around the time this version came out. || The music is mostly rock music|rock.
|- name of the director.
|-
| Kyung-soo willingly gives up his endurance contest with Jang Ryang and accidentally loses his fight with Jang Ryang || Kyung-soo remembers his mother, father, and sister telling him to restrain himself during both of these incidents.
|}

== Comic ==
There is also a manhwa set before the movie called Volcano High Prelude. It was distributed in the United States by Media Blasters.

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 