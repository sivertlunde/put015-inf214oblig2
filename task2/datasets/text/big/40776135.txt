Pawn (film)
{{Infobox film
| name           = Pawn
| image          = 
| caption        = 
| director       = David A. Armstrong Jonathan Bennett Jeff Bozz Pamela M. Burrus Cameron Denny Roman Kopelevich Adam Moryto Jeff Rice Rick St. George Eliza Swenson
| writer         = Jerome Anthony White Common Marton Csokas Sean Faris Stephen Lang Ray Liotta Nikki Reed Forest Whitaker
| music          = Jacob Yoffee
| cinematography = 
| editing        = Jordan Goldman Danny Saphire
| studio         = 
| distributor    =
| released       = September 19, 2013
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Pawn is a 2013 film directed by David A. Armstrong.  

==Plot==
An old gangster, with a hard drive containing records of who he paid off, is targeted by a competition between dirty cops, internal affairs, etc.   The dirty cops hire a thug to get into the safe (in the back of a diner) at midnight. But he brings his friends and goes too early for the time-release lock. Another crooked cop shows up (for uncertain reasons). The shooting ensues and during hostage negotiations the thug tries to put the blame onto an ex-con who just got out of jail, so that no one notices the real target is the hard drive.   

==Cast==
 
*Forest Whitaker as Will
*Michael Chiklis as Derrick
*Stephen Lang as Charlie
*Ray Liotta as Man in the Suit
*Nikki Reed as Amanda Common as Jeff Porter
*Marton Csokas as Lt Barnes
*Max Beesley as Billy Jonathan Bennett as Aaron
*Cameron Denny as Nigel
*Jessica Szohr as Bonnie
*Sean Faris as Nick Davenport
*Ronald Guttman as Yuri Mikelov
*Jordan Belfi as Patrick
 

==References==
 

==External links==
*  

 
 