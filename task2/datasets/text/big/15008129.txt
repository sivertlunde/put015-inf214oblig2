The Brothers Rico
{{Infobox film
| name           = The Brothers Rico
| image          = BrothersRicoPoster.jpg
| image_size     = 
| alt            =
| caption        = Theatrical release poster
| director       = Phil Karlson
| producer       = William Goetz Lewis J. Rachmil
| screenplay     = Lewis Meltzer Ben Perry Dalton Trumbo
| story          = Georges Simenon
| narrator       =
| starring       = Richard Conte Dianne Foster Kathryn Crosby James Darren
| music          = George Duning
| cinematography = Burnett Guffey
| editing        = Charles Nelson
| studio         = William Goetz Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Kathryn Grant and James Darren. 

==Plot== syndicate boss, "Uncle" Sid Kubik (Larry Gates), has ordered him to Miami.  Eddie leaves despite his wifes objections that he will miss an adoption interview.

Eddie meets Kubik in Miami, where Kubik apologizes for orders to hire Wesson and congratulates Eddie on his impending adoption.  Kubik says the syndicate does not know where Johnny is, but they are concerned that because Johnnys new wifes brother is suspected of being a prosecution witness, Johnny will be persuaded to turn on the syndicate and testify against them in return for clemency. After insisting that he does not believe Johnny has not turned on them, Kubik tells Eddie to find Johnny and make him leave the country in order to protect his life. As Eddie leaves, Kubik goes into a different room where Gino is being beaten.
 El Camino, California.

In California, Eddie finds Johnny and his pregnant wife, Norah (Kathryn Crosby) hiding out on a farm. Johnny says he left the syndicate because he wants his son to grow up to be clean and not know a life of crime.  Norah becomes excited at the prospect of Johnny being dragged back into the syndicate, and needs a doctor. Eddie is asked to leave, and returns to his hotel room where Mike Lamotta (Harry Bellaver), a local crime boss, is waiting for him. There, Eddie realizes that Kubik used him to locate Johnny, and intended for Johnny to be killed the whole time. Lamotta instructs Eddie to tell Johnny to meet mobsters waiting outside his home, but Eddie tells Johnny to go to the cops instead, and is knocked out by Lamottas assistant, Gonzales (Rudy Bond). To save his wife and newborn son, Johnny goes to the mobsters and is killed.

As Eddie and Gonzales fly back to Florida, Eddie learns that Gino attempted to flee the country against orders and was killed. Eddie knocks out Gonzales while on a stopover in Phoenix, Arizona|Phoenix, and returns to Florida to give Alice money.  He goes to Malaks and offers to testify against the syndicate. When Eddie goes to say goodbye to his mother, Kubik is there and holds him at gunpoint.  Eddie pulls out his own gun and kills both Kubik and his accomplice, but is wounded himself. Eddie eventually testifies against the syndicate and it is broken up, while he and Alice go to an orphanage hoping to adopt a child.

==Cast==
* Richard Conte as Eddie Rico
* Dianne Foster as Alice Rico
* Kathryn Crosby as Norah Malaks Rico
* Larry Gates as Sid Kubik
* James Darren as Johnny Rico
* Argentina Brunetti as Mrs. Rico
* Lamont Johnson as Peter Malaks
* Harry Bellaver as Mike Lamotta
* Paul Picerni as Gino Rico
* Paul Dubov as Phil
* Rudy Bond as Charlie Gonzales
* Richard Bakalyan as Vic Tucci
* William Phipps as Joe Wesson

==Reception==

===Critical response===
The staff at Variety (magazine)|Variety magazine gave the film a positive review and lauded the acting in the drama, writing, "Phil Karlson forges hard action into unfoldment of film. Performances are first-class right down the line, Conte a standout as a man finally disillusioned after thinking of the syndicate leader who orders his brothers execution as a close family friend. Both femmes have comparatively little to do, Dianne Crosby as Contes wife and Kathryn Grant as the brothers, but make their work count. Larry Gates as gang chief scores smoothly and James Darren as younger brother handles character satisfactorily." 

==Adaptations==
The film was later re-made for television as The Family Rico (1972).

==References==
 

==External links==
*  .
*  .
*  .
*  

 

 
 
 
 
 
 
 
 
 
 
 
 