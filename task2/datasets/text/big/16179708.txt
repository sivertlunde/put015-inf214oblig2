Vetrivel Sakthivel
 
{{Infobox film|
| name = Vetrivel Sakthivel
| image =Vetrivel Sakthivel.gif
| caption =
| director = Lakshmi Priyan 
| writer =
| starring = Sathyaraj Sibiraj Kushboo Nikita Thukral
| producer = R. S. Venkatraman
| cinematographer = Kasi Viswa
| music =Srikanth Deva
| editor =
| released =  
| runtime = 155 minutes
| country = India
| language = Tamil
| budget =
}}
Vetrivel Sakthivel is a 2005 Indian Tamil film directed by Lakshmi Priyan. It stars Sathyaraj and his son Sibiraj alongside Kushboo and Nikita Thukral.

==Plot==
Vetrivel (Satyaraj) is a businessman who lives with his wife (Kushboo Sundar), his son Sakthivel (Sibiraj), his daughter Selvi (Bharathi) and his wifes brother Thandapani (Vadivelu). Sakthi wanted to become as an IPS officer but his father denied it and made him work in his shop. Selvi gets married and lives happily, while Sakthi falls in love with Manju (Nikita Thukral). It turns out that Manju is the daughter of Vetrivels sister who eloped with someone. Since, Vetrivel has lost touch with his sister. Manju came into their lives to reunite the siblings.

Meanwhile, Sakthis sister finds out that her father-in-law (Thambi Ramaiah), his wife and her husband are culprits and smugglers. Before she can tell someone about it she is killed and her child is kept. Sakthi goes to visit his sister and finds her dead. Sakthi takes vengeance on his sisters murderers and goes to jail, while Manjus mother reunites with her brother Vetrivel. After having served his sentence in jail, Sakthi marries Manju and lives happily ever after.

==Cast==
*Sathyaraj as Vetrivel
*Sibiraj as Sakthivel
*Kushboo as Kamatchi
*Nikita Thukral as Manju
*Vadivelu as Thandapani
*Bharathi as Selvi
*Thambi Ramaiah as Selvis father-in-law
*T. P. Gajendran

==Critical reception==
Sify called the film "nepotism of the worst kind", going to add that "director Lakshmi Priyan has dished out one of the most inept movies of the year". 

==References==
 

 
 
 
 


 