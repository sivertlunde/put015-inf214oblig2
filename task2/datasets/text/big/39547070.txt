The Law West of Tombstone
{{Infobox film
| name           = The Law West of Tombstone
| image          =
| caption        =
| director       = Glenn Tryon
| producer       = Cliff Reid
| writer         = Harry Carey Tim Holt
| music          =
| cinematography =
| editing        = RKO Radio Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Law West of Tombstone is a 1938 Western film. 

Tim Holt was borrowed by RKO from Walter Wanger to play the Tonto Kid. He would soon star in a series of Westerns for the studio. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p123  Anne Shirley and Harry Carey were also borrowed from Wanger. Gable to Play Doctor in Towne, Baker Yarn: Sheridan With Cagney Oakie May Do Series Tombstone Cast Set Balalaika to Start
Schallert, Edwin. Los Angeles Times (1923-Current File)   13 Sep 1938: 12.  However Shirley refused to play the role and was suspended. SCREEN NEWS HERE AND IN HOLLYWOOD: RKO and Cantor Negotiating for Comedian to Star on Percentage Basis MGM GETS WIMAN SHOW Acquires Rights to I Married an Angel--New Mr. Moto Film Opens Today Picture for Bing Crosby Of Local Origin
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   17 Sep 1938: 20. 

==Plot==
A Judge Roy Bean figure dispenses justice in Arizona. He teams up with the Tonto Kid to fight the McQuinn gang.

==References==
 

==External list==
*  at IMDB
*  at TCMDB

 
 
 
 
 
 

 