Melody of Murder
{{Infobox film
| name           = The Melody of Murder
| image          = Mordets Melody.jpg
| image_size     =
| caption        = Front Cover of the Danish DVD
| director       = Bodil Ipsen
| producer       = 
| writer         = Fleming Lynge Tavs Neiiendam (radio play)
| narrator       = 
| starring       = Gull-Maj Norin Poul Reichhardt Angelo Bruun Ib Schønberg
| music          = Erik Fiehn
| cinematography = Valdemar Christensen
| editing        = Valdemar Christensen
| distributor    = Nordisk Film
| released       = 1944
| runtime        = 100 mins
| country        = Denmark Danish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Danish film noir directed by Bodil Ipsen and starring Gull-Maj Norin and Poul Reichhardt. The dark tale revolves around a sexually ambiguous serial killer whose crimes are committed to a French cabaret song, and the police suspect a chanteuse who sings the same tune during her performances. Produced during the German occupation of Denmark in World War II, the film was praised by critics for its "stylish cinematography, thematic intensity, and dark vision of Copenhagen;"  and consider it one of the most influential Danish films of that period.

== Cast ==
*Gull-Maj Norin  ... Cabaret Singer Odette Margot / Sonja
*Poul Reichhardt ... Emcee Max Stenberg
*Angelo Bruun ... 	Hypnotist Louis Valdini
*Peter Nielsen ... 	Detective Baunsø
*Else Petersen ... 	Mrs. Baunsø
*Karen Poulsen ... 	Wardrobe Lady Flora Kristiansen
*Ib Schønberg ... 	Theater Director Perm
*Petrine Sonne ... 	Landlady Sonja Bohman
*Charles Wilken ... Pensionist
*Anna Henriques-Nielsen ... Salesgirl Sonja Neie
*Lili Heglund ... 	Baker Lise Rasmussen
*Valsø Holm ... 	
*Per Buckhøj ... 	
*Helga Frier ... 	Cleaning Lady Nielsen
*Lis Løwert ... 	Maid Poula

== References ==
 

== External links ==
* 
* 

 
 
 
 
 
 
 


 
 