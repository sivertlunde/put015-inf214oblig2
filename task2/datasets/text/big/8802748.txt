This Divided State
{{Infobox Film name = This Divided State image = This_Divided_State=DVD_Cover.jpg caption = This Divided State on DVD producer = Phil Gordon Steven Greenstreet Kristi Haycock director = Steven Greenstreet writer =  starring = Michael Moore Sean Hannity Kay Anderson music =  cinematography = Matt Eastin Wes Eldredge Steven Greenstreet Joshua Ligairi editing = Steven Greenstreet distributor = Disinformation released = July 22, 2005 runtime = 88 min language = English
 budget = United States dollar|US$10,000
}}
 documentary by Utah Valley 2004 presidential election. 
{{cite web
|last=Pate
|first=Michelle
|title=This Divided State (2005)
|url=http://www.imdb.com/title/tt0444698/plotsummary
|work=The International Movie Database
|accessdate=11 November 2011}}
 

== Synopsis ==
In September 2004, UVSC student council leaders, Jim Bassi and Joseph Vogel, invited liberal filmmaker Michael Moore to come speak on campus.  The predominantly conservative community of Orem surrounding the school erupted in an uproar.  A pandemonium of protests, petitions, and demonstrations escalated into hate mail, threatening phone calls, threatened lawsuits, and countless incendiary editorials. 
{{cite web
|last=Catsoulis
|first=Jeanette
|title=Trouble in Mormon Country When a Liberal Pays a Visit
|url=http://query.nytimes.com/gst/fullpage.html?res=9402E7D7133EF93AA2575BC0A9639C8B63
|work=The New York Times
|accessdate=19 November 2011
|date=19 August 2005}}
   

Attempting to offset the controversy, UVSC invited conservative commentator Sean Hannity, scheduling his appearance a few days prior to Moore’s visit.  Hannity waived his normal $100,000 speakers fee, but still demanded that UVSC cover his travel costs, which totaled $49,850, a figure which "generally surprised" UVSC officials when they received the bill. 
{{cite web
|last=Leiby
|first=Richard
|title=Documenting the Cost of Free Speech
|url=http://www.washingtonpost.com/ac2/wp-dyn/A4247-2005Mar27
|work=The Washington Post
|accessdate=7 November 2011
|date=27 March 2005}}
    Also featured in the film is prominent voice of the opposition, Kay Anderson, a local real estate mogul who offered UVSC $25,000 to cancel Moore’s appearance, and, when that failed, tried to sue the school for misuse of funds. 
{{cite web
|last=Eddington
|first=Mark
|title=Film on Moore visit to go on tour
|url=http://archive.sltrib.com/article.php?id=2610355&itype=NGPSID&keyword=&qtype=
|work=The Salt Lake Tribune
|accessdate=15 November 2011
|date=17 March 2005}}
  
{{cite web
|last=Hancock
|first=Laura
|title=Film dissects pros, cons of Moore visit
|url=http://www.deseretnews.com/article/600110152/Film-dissects-pros-cons-of-Moore-visit.html
|work=Deseret News
|accessdate=14 November 2011
|date=6 February 2005}}
 

== Production == Bryan Young and Elias and Michelle Pate, 
{{cite web
|title=Any which way you can: : Local film This Divided State finishes rocky road to DVD distribution
|url=http://www.heraldextra.com/lifestyles/article_9e0ff0a4-cecb-55aa-bc09-1d653382cb28.html
|work=Daily Herald
|accessdate=4 November 2011}}
  describing the ensuing commotion as “a huge crush of political debate and an overwhelming sense of activity and electricity.” 
{{cite web
|title=Movie made of controversial Moore campus visit
|url=http://www.ctv.ca/CTVNews/Entertainment/20050131/moore_film_050131/
|work=CTV News|accessdate=4 November 2011}}
  

The filmmakers captured at least 70 hours of material in the course of three months. Greenstreet eventually maxed out three credit cards, emptied his bank account, and dropped out of Brigham Young University, devoting himself exclusively to this project. In one instance, when the filmmakers could no longer afford the cost of home internet access, they resorted to piggybacking on an unsuspecting neighbors wireless signal, only accessible at the foot of a family member’s bed.   

Often spending twenty hours a day editing, Greenstreet carefully trimmed the footage down to eighty-eight minutes. 
{{cite web
|title=Moore documentary
|url=http://www.heraldextra.com/news/local/article_1d31f6c1-a5d1-5a64-ac0d-d47e8eda22d5.html
|work=Daily Herald
|accessdate=4 November 2011
|date=4 February 2005}}
   Juxtaposing candidly emotional interviews against unruly public spectacles, he consistently strove for neutrality, advancing the story without narration and allowing equal time to all opposing opinions.  The filmmakers could not forget the vital importance of finishing the film soon, while the events involved were still relevant and fresh in the public’s minds. 
{{cite web
|last=Montesano
|first=Anthony P.
|title=Interview With: Steven Greenstreet, Director/Producer
|url=http://moviesonmymind.blogspot.com/2006/03/interview-with-steven-greenstreet.html
|work=Movies on My Mind|accessdate=4 November 2011|date=8 March 2006}}
 

Upon learning UVSC professor and self-proclaimed liberal Phil Gordon had become involved in the project, Kay Anderson filed a claim against Greenstreet, attempting to revoke his previously signed consent to be included in the film. 
{{cite web
|last=Eddington
|first=Mark
|title=Documentary on Moores UVSC visit has the usual critic
|url=http://archive.sltrib.com/article.php?id=2550283&itype=NGPSID&keyword=&qtype=  
|work=The Salt Lake Tribune
|accessdate=13 November 2011
|date=February 3, 2005}} Patrick Shea, Anderson’s attempts to remove himself from the film proved unsuccessful. 
{{cite web
|last=Decker
|first=Marin
|title=Film footage is disputed
|url=http://www.deseretnews.com/article/600107906/Film-footage-is-disputed.html
|work=Deseret News
|accessdate=4 November 2011
|date=28 January 2005}}
  
{{cite web
|last=Eddington
|first=Mark
|title=Film on Moore fiasco a big hit with 400-plus UVSC audience
|url=http://archive.sltrib.com/article.php?id=2551465&itype=NGPSID&keyword=&qtype=
|work=The Salt Lake Tribune
|accessdate=16 November 2011
|date=February 4, 2005}}
   Greenstreet defended using Anderson in the film, “without the personal interview that I did with him, he doesn’t get a chance to explain his motivation.” 

== Release ==
On February 5, 2005, the eighty-eight minute final product premiered at UVSCs Ragan Theater, receiving a standing ovation from the 700 people overflowing the Theaters 400-seat capacity. 
{{cite web
|last=Hawkins
|first=Emily
|title=Reel Progress: This Divided State
|url=http://www.campusprogress.org/articles/reel_progress_this_divided_state
|work=Campus Progress
|accessdate=4 November 2011
|date=17 February 2005}}
   On the heels of the sold-out premiere, the filmmakers commenced a publicity blitz to any magazine, newspaper, or film studio that would take their call. Without previous experience, the filmmakers had to quickly learn how to produce their own press kits and market their movie with a team of only four people and minimal resources.   Their first big break occurred when Campus Progress, a division of the Center for American Progress, sponsored a tour of the film as part of its "Reel Progress" series.  The film went on to screen at twenty-three college campuses, including Yale University|Yale, Cornell University|Cornell, Vanderbilt University|Vanderbilt, and the University of Southern California. 
{{cite web
|last=Peterson
|first=Chris
|title=Filmmaker enjoys success of Moore documentary
|url=http://www.heraldextra.com/news/article_9c7d2c10-4f9e-5b95-9817-a7f9decd01a8.html
|work=Daily Herald
|accessdate=4 November 2011
|date=9 May 2005}}
  
 The Disinformation Company. 

== Reception ==
This Divided State has an approval rating of 83% on Rotten Tomatoes from 24 reviews counted. {{cite web
|url=http://www.rottentomatoes.com/m/this_divided_state/ 
|title=This Divided State (2005)
|work=Rotten Tomatoes
|accessdate=2010-09-10}}
   The New York Times called it "filmmaking gold" and "extremely moving."   Variety (magazine)|Variety found the film “as boisterous as it is sobering,” 
{{cite web
|last=Harvey
|first=Dennis
|title=This Divided State
|url=http://www.variety.com/review/VE1117927976?refcatid=31
|work=Variety
|accessdate=4 November 2011
|date=23 August 2005}}
  while Deseret News claimed it is “surprisingly cohesive and coherent and it does its best to tell all sides of the story.” {{cite web
|last=Vice|first=Jeff|title=Divided surprisingly coherent
|url=http://www.deseretnews.com/article/600150145/Divided-surprisingly-coherent.html
|work=Deseret News
|accessdate=4 November 2011
|date=22 July 2005}}
    Orlando Sentinel further posits, “This Divided State shows the power of the newer, cheaper video documentary in all its glory.” {{cite web
|last=Moore
|first=Roger
|title=This Divided State
|url=http://www.orlandosentinel.com/entertainment/movies/orl-db-moviereviews-searchresults,0,3279701,results.formprofile?Lib=turbine_cdb_lib%3Aresult_doc_id+result_doc_rank+document_id+cdb_num+cdb_01_txt+cdb_02_txt+cdb_03_txt+cdb_04_txt+cdb_05_txt+cdb_06_txt+cdb_07_txt+cdb_08_txt+cdb_09_txt+cdb_10_txt+cdb_11_txt+cdb_12_txt+cdb_13_txt+cdb_15_txt+cdb_14_txt+cdb_16_txt+cdb_17_txt+cdb_18_txt+cdb_19_txt+cdb_20_txt+cdb_21_txt+cdb_22_txt+cdb_23_txt+cdb_24_txt+cdb_25_txt+cdb_26_txt&PageSize=1&Page=1&MinCoarseRank=500&QueryType=CONCEPT&Query=&turbine_cdb_lib__cdb_01_txt=This%20Divided%20State&Find+it!=Submit+Query
|work=Orlando Sentinel
|accessdate=4 November 2011
|date=8 September 2005}}
 

== External links ==
* 
* 

=== Reviews ===
* 
* 
* 
* 
* 
* 
* 
* 
* 

=== References ===
 

 
 
 
 
 