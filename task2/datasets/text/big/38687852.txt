The Unbelievers
{{Infobox film name = The Unbelievers image = The Unbelievers Poster.jpg caption = Theatrical poster director = Gus Holwerda producer = Lawrence Krauss Gus Holwerda Luke Holwerda Jason Spisak screenplay =  story =  based on = starring = Richard Dawkins Lawrence Krauss music = Thomas Amason Chris Henderson cinematography = Luke Holwerda editing = Gus Holwerda Holwerda studio = Black Chalk distributor =   released =   runtime = 77 minutes country = United States language = English budget =  gross = $14,400  
}}
 Lawrence Krauss Sam Harris, David Silverman.

==Release==
The world premiere for The Unbelievers was on April 29, 2013 at Hot Docs Film Festival in Toronto, Ontario, and all four screenings of the film were sold out.   

==Reception==
The Unbelievers received mixed reviews. The review aggregation website Rotten Tomatoes reports that 44% of critics gave the film a positive review based on 9 reviews. There are too few reviews for the site to report a consensus.    Metacritic, which assigns a rating out of 100 top reviews from mainstream critics, calculated a score of 32 based on 7 reviews.   The film has fared better among viewers, garnering an average 4.0 out of 5.0 stars on iTunes and Amazon.  

==Interviewees== Sam Harris, James Morrison, Michael Shermer, and David Silverman. 

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 


 