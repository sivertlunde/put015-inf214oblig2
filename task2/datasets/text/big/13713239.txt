Robin Hood (2010 film)
 
 
 
{{Infobox film
| name           = Robin Hood
| image          = Robin Hood 2010 poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Ridley Scott
| producer       =  
| screenplay     = Brian Helgeland
| story          =  
| starring       =  
| music          = Marc Streitenfeld John Mathieson
| editing        = Pietro Scalia
| studio         =   Universal Pictures
| released       =  
| runtime        = 140 minutes
| country        = United Kingdom United States
| language       = English French
| budget         = $155 million   
| gross          = $321,669,730     
}} epic adventure film based on the Robin Hood legend, directed by Ridley Scott and starring Russell Crowe and Cate Blanchett. It was released in 12 countries on 12 May 2010, including the United Kingdom and Republic of Ireland, and was also the opening film at the 2010 Cannes Film Festival the same day. It was released in a further 23 countries the following day, among them Australia, and an additional 17 countries on 14 May 2010, among them the United States and Canada.  . Retrieved 3 February 2013 

==Plot==
  Robin Longstride archer in King Richard Chalus Castle. Allan ADayle (Alan Doyle) and Will Scarlett (Scott Grimes) and soldier Little John (Kevin Durand) – find themselves in the stocks.
 King Philip of France to assassinate Richard. After chasing off Godfrey, Robin decides to take advantage of the situation by having his men impersonate the dead English knights to return to England. As they depart, Robin promises one of the dying knights, Sir Robert Loxley (Douglas Hodge), to return his sword to his father in Nottingham.
 King John North to do so – unaware that Godfrey will instead use French troops to stir up unrest and create an opening for Philip to invade England.
 Lady Marion (Cate Blanchett), is initially cold toward Robin, but warms to him, when he and his men merrily recover tithed grain for the townsfolk to plant.
 charter of rights to ensure the rights of every Englishman and unite his country. Having realized Godfreys deception, and knowing he must meet the French invasion with an army, the King agrees. Meanwhile, the French marauders plunder Nottingham. Robin and the northern barons arrive and stop Godfreys men, but not before Godfrey has slain the blind Sir Walter.

As the French begin their invasion on the beach below the Cliffs of Dover, Robin leads the united English army against them. In the midst of the battle, Robin duels with Godfrey, who attempts to kill Marion and flees before Robin finally pierces him with an arrow from afar. Philip realizes his plan to divide England has failed and calls off his invasion. When King John sees the French surrender to Robin instead of himself, he senses a threat to his power. In London John reneges on his promise to sign the charter, instead declaring Robin an outlaw to be hunted throughout the kingdom. The Sheriff of Nottingham (Matthew Macfadyen) announces the decree as Robin and his men flee to Sherwood Forest with the orphans of Nottingham. Marion narrates their new life in the greenwood, noting that they live in equality as they right the many wrongs in the kingdom of King John. And "the legend begins".

==Cast== Robin Longstride, American Gangster, Body of Lies.  Marion Loxley, the strong-willed, intelligent widow of Sir Robert Loxley. She becomes Robin Hoods love interest. Marion takes on the responsibility of managing her aging father-in-laws debt-ridden estate and lean harvests, a situation made difficult by an unsympathetic Church, the greedy and lecherous Sheriff of Nottingham, and the recurring runaway children in Sherwood forest who frequently raid the grain storehouses in Peperharrow. Sienna Miller was originally cast in the part, but was dropped from the production. 
* Mark Strong as Sir Godfrey, Prince Johns henchman. Godfrey is portrayed as cruel, ruthless, and diabolically clever. Although he serves John, he intends to ally himself with the French and seize power for himself.  When interviewed in November 2008, Strong stated the character was originally called Conrad and was based on Guy of Gisbourne. He described the original character as having blond hair and a disfigurement from being struck by a crossbow bolt.  Prince John, the younger brother of King Richard who becomes king after his brothers death. Vain, selfish, and fiery-tempered, he is nevertheless brave, self-assured, and darkly charismatic. 
* Mark Lewis Jones as Thomas Longstride, Robins father who was a stonemason and philosopher.
* Mark Addy as Friar Tuck William Marshal  King Richard the Lionheart
* Eileen Atkins as Eleanor of Aquitaine, King Richard and Prince Johns mother. She makes no secret of loving Richard more than John and she and John have an antagonistic relationship as a result. Vanessa Redgrave was originally cast for the part, but pulled out after the death of her daughter, actress Natasha Richardson. 
* Max von Sydow as Sir Walter Loxley King Philip of France
* Matthew Macfadyen as the Sheriff of Nottingham 
* Kevin Durand as Little John     
* Léa Seydoux as Isabella of Angoulême, the French kings niece whom John marries after annulling his first marriage to sire an heir and gain a claim to the Castilian throne. She and John have a loving and affectionate relationship, despite his self-absorption. 
* Scott Grimes as Will Scarlet.  Allan ADayle; Crowe enlisted Doyle to play the merry mens minstrel, having collaborated with him on the album My Hand, My Heart. 
* Douglas Hodge as Sir Robert Loxley
* Denis Menochet as Adhemar
* Velibor Topic as Belvedere Isabel of Gloucester, Johns first wife whom he divorces.

==Production==
In January 2007,  , because Cary Elwes was quite a comic".    

Scotts dissatisfaction with the script led him to delay filming, and during 2008 it was rewritten into a story about Robin Hood becoming an outlaw, with the position of sheriff as part of the story. Scott dropped the latter notion and Nottingham was retitled to reflect the more traditional angle.
 Richard the Prince John WGA strike, production was put on hold.  Scott sought to begin production in 2008 for a release in 2009. 

Filming was scheduled to begin in August in Sherwood Forest if the 2008 Screen Actors Guild strike did not take place,  for release on 26 November 2009. By July, filming was delayed,  and playwright Paul Webb was hired to rewrite the script.  The film was moved to 2010.  The Sheriff of Nottinghams character was then merged with Robin.  Scott explained Robin "has to retire to the forest to resume his name Robin. So he was momentarily the Sheriff of Nottingham." {{cite news|first=Simon|last=Reynolds|title=
Scott explains Crowes Nottingham role|work= .  Scott was also pleased that the   Nottinghamshire set that was built during 2008 had aged into the landscape.  By February 2009, Scott revealed Nottingham had become his version of Robin Hood, as he had become dissatisfied with the idea of Robin starting as the Sheriff.   

{{Gallery
| title = Filming locations
| lines = 5
| width = 250 Mock castle Castle Chalus in the film) at the Bourne Wood at the end of filming, showing the burnt-out castle gate, Bourne Wood, Farnham, Surrey, 8 August 2009.   Burnt out gate of mock castle and battering ram (nicknamed Rosie), Bourne Wood, Farnham, Surrey, 8 August 2009. Filming the fight scenes on the beach, Freshwater West, Pembrokeshire, Wales, 23. June 2009.   The beach filming, Freshwater West, Pembrokeshire, Wales, 23. June 2009.
}} Castle Chalus took place at the Bourne Wood at Farnham, Surrey during July and August.    Filming also took place at Dovedale near Ashbourne, Derbyshire. 
 Scottish charity, Clanranald Trust to be used for battle re-enactments at a fort named Duncarron, built in a forest near the Carron Reservoir in North Lanarkshire. 

==Release==
The film was released on 12 May 2010 in 12 countries, including the United Kingdom and Ireland, and was also the opening film at the 2010 Cannes Film Festival the same day. It was released in a further 23 countries the following day, among them Australia, and an additional 17 countries on 14 May 2010, among them the United States and Canada. It was thus released in 52 countries within three days. However, it was not released in Japan until 10 December 2010.    

===Home media===
Robin Hood was released on DVD and Blu-ray Disc on 20 September 2010 in the UK,  and the following day in the US.  While the UK home media releases only consisted of the extended Directors Cut version (15 additional minutes), the US DVD and Blu-ray discs consisted of both the  Directors Cut version and the shorter theatrical version. 

==Reception==

===Box office===
On its opening week the film took £5,750,332 in the UK, ahead of Iron Man 2 and $36,063,385 in the US,  and grossed a total of £15,381,416 in the UK, $104,516,000 in the US and $321,669,741 worldwide. 
The box-office figures were seen as somewhat of a disappointment, even though films set in
medieval times tend to fare poorly and Robin Hood actually ranks as the second highest-grossing medieval film. 

===Critical reception===
  average rating normalized rating of 40 reviews.   
 New York Relevant Magazine accused Scott of replacing depth with detail and manipulative themes, like vengeance and unjust war, and stated that Scott had sucked the life out of a cherished fable, writing that "Scott has turned a myth, a concept essentially, into a history which emerges as dry, insensible clutter."   

Among the films more positive reviews,  ."  Lou Lumenick of the New York Post called Robin Hood "head and shoulders above the sort of lightheaded epics Hollywood typically offers during the summer season." 

Russell Crowe received criticism from the British media for his variable accent during the film. Empire (film magazine)|Empire said his accent was occasionally Scottish, {{cite web
 | url=http://www.empireonline.com/reviews/reviewcomplete.asp?FID=134778
 | first = Dan | last = Jolin
 | work = Empire
 | title=Reviews: Robin Hood
 | accessdate=14 May 2010
}}  while Total Film thought there were also times when it sounded Irish. {{cite web
 | url=http://www.totalfilm.com/reviews/cinema/robin-hood-1
 | author = Andy Lowe
 | publisher=Total Film Magazine
 | title=Reviews: Robin Hood
 | accessdate=14 May 2010}} 
Mark Lawson, while interviewing Crowe on BBC Radio 4, suggested there were hints of Irish in his accent, which angered Crowe who described this as "bollocks" and stormed out. {{cite news
 | url=http://www.guardian.co.uk/media/organgrinder/2010/may/14/russell-crowe-accent-acrimony
 | author=John Plunkett
 | publisher=The Guardian (blog)
 | title=Russell Crowe puts accent on acrimony
 | date=14 May 2010
 | accessdate=14 May 2010
| location=London
}} (Includes audio file)
  

A number of reviewers have criticised historical inaccuracies in the film. In The New York Times, A. O. Scott complained that the film made "a hash of the historical record".  In The Guardian, Alex von Tunzelmann complained that the film was filled with historical impossibilities and anachronisms. She notes that Richard the Lionheart was indeed fighting in France in 1199, but that he had actually come back from the Holy Land seven years earlier, so it is inaccurate to depict him fighting in France on his way back from the Holy Land in 1199, as is the case in the film. 

===Accolades===
{| class="wikitable"
|-
! Year !! Award !! Category !! Result
|-
|rowspan="3"| 2011
| Peoples Choice Award Favourite Action Film
|  
|-
| Screen Actors Guild Awards Stunt Ensemble
|  
|- Teen Choice Awards
| 
| 
|-
|}

==Soundtrack==
The soundtrack to Robin Hood, with music written and performed by Marc Streitenfeld, was released on 11 May 2010.

{{Track listing
| collapsed       = yes
| total_length    = 51:39 
| title1          = Destiny
| length1         = 3:36
| title2          = Creatures
| length2         = 2:09
| title3          = Fate Has Smiled Upon Us
| length3         = 2:02
| title4          = Godfrey
| length4         = 3:32
| title5          = Ambush
| length5         = 1:16
| title6          = Pact Sworn in blood
| length6         = 2:52
| title7          = Returning the Crown
| length7         = 1:13
| title8          = Planting the Fields
| length8         = 1:18
| title9          = Sherwood Forest
| length9         = 2:19
| title10         = John Is King
| length10        = 4:02
| title11         = Robin Speaks
| length11        = 2:33
| title12         = Killing Walter
| length12        = 2:02
| title13         = Nottingham Burns
| length13        = 2:12
| title14         = Siege
| length14        = 2:11
| title15         = Landing of the French
| length15        = 2:49
| title16         = Walters Burial
| length16        = 3:05
| title17         = Preparing for Battle
| length17        = 2:41
| title18         = Charge
| length18        = 1:20
| title19         = Clash
| length19        = 2:41
| title20         = The Final Arrow
| length20        = 2:30
| title21         = The Legend Begins
| length21        = 1:28
| title22         = Merry Men
| length22        = 1:48
}}

==Sequels==
Ridley Scott indicated he had been considering further Robin Hood films, in an interview with The Times on 4 April 2010, stating, "Honestly, I thought why not have the potential for a sequel?"  and, "Lets say we might presume theres a sequel." At the world premiere in Cannes, Russell Crowe declared he was willing, "if I had the opportunity to address what happens next with Ridley and Cate, then great, lets do it." 

==Notes==
 

==References==
 

==External links==
 
 
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 