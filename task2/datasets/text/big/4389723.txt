Muscle Beach Party
 
{{Infobox film
| name           = Muscle Beach Party
| image          = Muscle-Beach-Party-Poster.jpg
| image_size     = 190px
| border         = yes
| caption        =
| director       = William Asher
| producer       = Robert Dillon James H. Nicholson
| writer         = Robert Dillon
| narrator       = John Ashley Don Rickles
| music          = Les Baxter
| cinematography = Harold E. Wellman
| editing        = Fred R. Feitshans, Jr. Eve Newman
| distributor    = American International Pictures
| released       =  
| runtime        = 94 min.
| country        = United States
| language       = English
| budget         = $1 million Gary A Smith, American International Pictures: The Golden Years, Bear Manor 2013 p 208 
}}

Muscle Beach Party (1964) is the second of seven beach party films produced by American International Pictures. It stars Frankie Avalon and Annette Funicello and was directed by William Asher, who also directed four other films in this series.
 Dick Dale and the Del-Tones and Stevie Wonder appear in musical numbers, the latter aged thirteen and making his film debut, billed as "Little Stevie Wonder."

==Synopsis==
Frankie, Dee Dee, and the beach party gang hit Malibu Beach for another summer of surfing and no jobs only to find their secret surfing spot threatened by a gang of bodybuilders, led by the dim-witted coach Jack Fanny (Don Rickles). 

All the while, a bored Italian Countess (Luciana Paluzzi) is trying to steal Frankie from Dee Dee and, much to everyones surprise, he seems more than happy to go along with it.  Shes going to turn him into a teen idol, not unlike Frankie Avalons real-life persona.
 beach bunny, Dee Dee.

==Cast==
*Frankie Avalon     .... 	Frankie
*Annette Funicello  .... 	Dee Dee
*Luciana Paluzzi	   .... 	Contessa Juliana ("Julie") Giotto-Borgini  John Ashley	   .... 	Johnny
*Don Rickles	   .... 	Jack Fanny
*Peter Turgeon	   .... 	Theodore
*Jody McCrea	   .... 	Deadhead
*Dick Dale	   .... 	Himself
*Candy Johnson	   .... 	Candy Rock Stevens (Peter Lupus)	   .... 	Flex Martian
*Valora Noland	   .... 	Animal
*Delores Wells	   .... 	Sniffles
*Donna Loren	   .... 	Donna
*Morey Amsterdam	   .... 	Cappy Little Stevie Wonder      .... 	Himself
*Buddy Hackett	   .... 	S.Z. Matts (rich business manager)
*Dan Haggerty  .... 	Biff Larry Scott	   .... 	Rock
*Gordon Case	   .... 	Tug
*Gene Shuey	   .... 	Riff Chester Yorton	   .... 	Hulk
*Bob Seven	   .... 	Sulk
*Steve Merjanian	   .... 	Clod
*Alberta Nelson	   .... 	Lisa, Jack Fannys assistant
*Amadee Chabot	   .... 	Flo, Jack Fannys assistant
*Peter Lorre	   .... 	Mr. Strangdour

===Cast notes=== John Ashleys Pajama Party, Beach Blanket Bingo, How to Stuff a Wild Bikini, and The Ghost in the Invisible Bikini. Lembeck as von Zipper (but sans Rats gang) also appears in a cameo in Dr. Goldfoot and the Bikini Machine. Lembeck also appeared in Fireball 500, another Avalon-Funicello vehicle, as an entirely different character. Peter Lorre appears briefly near the end of the film and there is a notice explaining that he will appear in the next installment of the series. Lorre died in March 1964; thus, this was his only appearance in the series. 

==Production notes== Ira Wallachs satirical novel. This was eventually made as Dont Make Waves (1967). 
===Novelization===

A 141-page paperback adaptation of the screenplay, written by Elsie Lee, was published prior to the release of the film by Lancer Books.

===Jack Fannys bodybuilders===

In the above-cited paperback adaptation, the Jack Fanny character introduces his bodybuilders as Biff, Rock, Tug, Riff, Sulk, Mash and Clod, whereas in the film he calls them Biff, Rock, Tug, Riff, Hulk, Sulk, and Clod.  In two separate sequences, the latter version of these names is seen printed on their shirts.
 Larry Scott, Rock Stevens") was also a champion bodybuilder himself, holding the titles of Mr. Indianapolis, Mr. Indiana, Mr. Hercules, and Mr. International Health Physique.

===Costumes and props===

The swimsuits were designed by Rose Marie Reid; Buddy Hacketts clothes were from Mr. Guy of Los Angeles; and the hat that Deadhead wears was designed by Ed "Big Daddy" Roth.
 Phil of Downey, California - aka Phil Sauers, the maker of "Surfboards of the Stars."  Sauers was also the stunt coordinator for another beach party film that used his surfboards, Columbia Pictures Ride the Wild Surf, which was released later the same year. Sauers was even portrayed in that film as a character by Mark LaBuse.

The "globe" telephone cover on Mr. Strangdours desk is the same one in Norma Desmonds home in the film Sunset Blvd.

===Music===
The original score for this film, like Beach Party before it, was composed by Les Baxter.

  by Dick Dale with the cast.

Guy Hemric and Jerry Styner wrote two songs for the film: "Happy Street" performed by Little Stevie Wonder; and "A Girl Needs a Boy" first performed by Annette Funicello, then reprised by Frankie Avalon as "A Boy Needs a Girl".

===Opening title art===
The colorful, hand-painted mural that is shown in full and in detail as background during the opening credits is by California artist Michael Dormer, whose surfer cartoon character, "Hot Curl" can also be glimpsed throughout the film. 

===Closing credits===
The closing credits feature Stevie Wonder reprising ""Happy Street" while Candy Johnson dances to the music. They both appear against a black background.

===Deleted scene===
 Fred Astaire Studios", no such sequence is found in the films release prints.

==Reception== The Golden Laurel, which had no ceremony but published its award results in the trade magazine Motion Picture Exhibitor from 1958 to 1971, nominated Annette Funicello for “Best Female Musical Performance” for this film in 1965.

The film was banned in Burma, along with Ski Party, Bikini Beach and Beach Blanket Bingo. 

==Cultural references==
* Don Rickles character name "Jack Fanny" is based on then-popular bodybuilder and gym entrepreneur (and usually sharp-dressed) Vic Tanny. The forename "Jack" might also be a reference to another popular fitness instructor, bodybuilder, and gym-entrepreneur, Jack LaLanne.

* Julies remark to an angry Dee Dee, "Have you tried Miltown?" is in reference to the drug Miltown by Wallace Laboratories, a carbamate derivative used as an anxiolytic drug - it was the best-selling minor tranquilizer at the time.

* Cappys Place in this film (and Big Daddys club in the preceding Beach Party) is a reference to Southern California beach coffeehouses in general and Cafe Frankenstein in particular.

* This is the second and last time Avalon or any other "teenager" in the cast smokes cigarettes onscreen in the series - the   was released on January 11, 1964, while Muscle Beach Party was being filmed.

==References==
 

==External links==
*  
*  
*  
*   at Brians Drive In Theatre

 
 

 
 
 
 
 
 
 
 
 