The Queen of Spades (1949 film)
{{Infobox film
| name           = The Queen of Spades
| image_size     =
| image	=	The Queen of Spades FilmPoster.jpeg
| caption        =
| director       = Thorold Dickinson
| producer       = Anatole de Grunwald
| writer         = Alexander Pushkin (story) Rodney Ackland Arthur Boys
| narrator       =
| starring       = Anton Walbrook Edith Evans Yvonne Mitchell
| music          =
| cinematography = Otto Heller
| editing        = Associated British-Pathe (UK) Republic Pictures (US)
| released       = June 30, 1949 (USA)
| runtime        = 95 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =£107,250 (UK) 
| preceded_by    =
| followed_by    =
}} short story of the same name by Alexander Pushkin. It stars Anton Walbrook, Edith Evans and Yvonne Mitchell. Although Evans and Mitchell were both experienced stage actors, this was their cinematic debut.  

==Plot== sold her soul to the devil in exchange for success in playing Faro. Suvorin seduces her ward (Yvonne Mitchell) as part of a plan to learn the countesss secret of success. 

==Production== short story of the same name by Alexander Pushkin, with a script written by Arthur Boys and Rodney Ackland. The original director of the film left the project after suffering from poor health, and was replaced by Thorold Dickinson, who also rewrote sections of the script. 

The exterior shots of St Petersburg were filmed in Welwyn Studio, Hertfordshire, using sets created by William Kellner. 

==Release and reception== BAFTA Award for Best British Film. It was also entered into the 1949 Cannes Film Festival.   

The film was later considered lost, but was rediscovered and re-released in British cinemas on 26 December 2009. It was released on DVD in January 2010, with an introduction by Martin Scorsese. 

Wes Anderson ranked it as the sixth best British film.  Martin Scorsese has described Thorold Dickinson as an underrated director, saying of The Queen of Spades that "this stunning film is one of the few true classics of supernatural cinema." 

==Cast==
*Anton Walbrook as Captain Herman Suvorin
*Edith Evans as Countess Ranevskaya
*Yvonne Mitchell as Lizavetta Ivanova Ronald Howard as Andrei
*Mary Jerrold as Old Varvarushka
*Anthony Dawson as Fyodor
*Miles Malleson as Tchybukin
*Michael Medwin as Hovaisky
*Athene Seyler as Princess Ivashin
*Ivor Barnard as Bookseller
*Aubrey Mallalieu as Fedya
*Maroussia Dimitrevitch as Gypsy singer
*Violette Elvin as Gypsy dancer
*Pauline Tennant as young Countess Ranevskaya

==References==
{{Reflist|refs=
   
}}

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 