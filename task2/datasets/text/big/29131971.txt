Tu as crié: Let me go
 
{{Infobox Film
| name           = Tu as crié: Let me go
| image          = Tu as crié (Let Me Go) film poster.jpg
| image_size     = 
| caption        = 
| director       = Anne Claire Poirier
| producer       =   
| writer         =   
| narrator       =   
| starring       =   
| music          =   
| cinematography =   
| editing        =   
| studio         =   
| distributor    =   
| released       =   
| runtime        =   
| country        =  
| language       =   
| budget         =   
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Tu as crié LET ME GO  is a 1997 feature length documentary by Anne Claire Poirier exploring the events that led to the murder of her daughter, Yanne, who had turned to drugs and prostitution before being murdered at the age of twenty-six.    The film was shot in Montreal and produced by the National Film Board of Canada.   

==Story==
Poiriers daughter had turned to drugs and prostitution before being murdered at age 26. In this film, Poirier explores the life of her daughter to better understand her and other young people who put their lives at risk with drugs. She enters the world of Montreals street people, looking at addicts, prostitutes and people living with AIDS, to attempt to follow the events leading to her daughters death. 

The films title is a reference to something her daughter had screamed out, "Let me go," just before being killed. In the film, director Poirier also interprets these words to mean that she must now let go of her departed daughter.   

==Reception==
Tu as crié LET ME GO received numerous awards including the Genie Award for Best Feature Length Documentary, best feature film at the Rendez-vous du cinéma québécois and best documentary feature at the Vancouver International Film Festival. 

==Stage adaptation==
In 2007, the films story was adapted for the stage by Nadia Capone as Let Me Go.   

==References==

 

==External links==
*  (in French)
* 

 
 
 
 
 
 
 
 
 
 