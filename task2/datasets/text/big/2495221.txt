Swamp Women
 

{{Infobox film
| name = Swamp Women
| image = Swampwomen.jpg
| caption = A promotional film poster for "Swamp Women."
| director = Roger Corman
| producer = Roger Corman
| writer = David Stern Susan Cummings
| music = Willis Holman
| cinematography = Frederick E. West
| editing = Ronald Sinclair
| distributor = Woolner Brothers Pictures Inc.
| released =  
| runtime = 84 min
| country = United States
| awards = English
| budget =
}}

Swamp Women (1955 in film|1955) was one of the first films directed by Roger Corman. This adventure film|adventure/crime film|crime/horror film follows undercover police officer Lee Hampton who joins three female convicts and escapes from prison. The escape is part of a larger plot to uncover a cache of diamonds hidden deep within the swamps of Louisiana. This film is sometimes also known as Cruel Swamp or Swamp Diamonds. The film is now in the public domain. Years later, Swamp Women was included as one of the choices in the book The Fifty Worst Films of All Time.

In July 1993 the film was featured in the movie-mocking television show Mystery Science Theater 3000 under the title Swamp Diamonds.

==Production==
The film was shot on location in Louisiana. Ed. J. Philip di Franco, The Movie World of Roger Corman, Chelsea House Publishers, 1979 p 8  Shot in 22 days.

== DVD releases ==
*The film has been released by multiple companies as a bargain box disc. Rhino Home Video as part of the Collection, Volume 10 (out-of-print on both Rhinos and Mst3ks official websites) and Collection, Volume 10.2 DVD sets. As of January 2010, Volume 10.2 is out-of-print on Rhinos official website, but still available on Mst3ks official website.
*This was also released as part of a large DVD box set of vintage exploitation films categorized; Girls Gone Bad.

==See also==
* List of films in the public domain

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 