Naked Gun 33⅓: The Final Insult
 
{{Infobox film
| name = Naked Gun 33⅓:  The Final Insult
| image = Naked Gun 3 poster.jpg
| caption = Theatrical release poster
| director = Peter Segal    David Zucker
| writer = Pat Proft David Zucker Robert LoCash
| based on =  
| starring = Leslie Nielsen Priscilla Presley George Kennedy O.J. Simpson Fred Ward Kathleen Freeman Anna Nicole Smith
| music = Ira Newborn 
| cinematography = Robert M. Stevens
| editing = James R. Symons
| distributor = Paramount Pictures
| released =  
| runtime = 83 minutes
| country = United States
| language = English
| budget = $30 million
| gross = $51.1 million 
}} Naked Gun series, which was based on the Police Squad! television series.

It was marketed with the tagline: "Mostly All New Jokes." The "33⅓" of the title is a reference to the speed at which long playing (LP) phonograph records (gramophone records) play. The film was originally going to be titled Naked Gun 33⅓: Just for the Record, but was changed after the studio felt not many understood the joke.  It was also going to be called "The Naked Gun III: The Final Insult", according to some Christmas 1993 video previews.

 .

==Plot== extremely taxing; Jane becomes frustrated both at Franks sudden unwillingness to engage in his marital duties and the suspicion that he is doing police work again, and storms out of the house.
 Best Picture category and triggered when the card is pulled out.

At the awarding night, Frank and Jane separate from Roccos team and frantically begin searching for the bomb, with Frank inflicting his usual chaos on stage during the prelude show. However, Frank and Jane are unable to find the bomb before the nomination for Best Picture has begun. When Frank bursts onto the stage and awkwardly tries to prevent the detonation of the bomb, Rocco and his mother realize what is going on and kidnap Jane, but in the process, Frank loosens an electronic sign which takes out Muriel. Desperate, Rocco decides to detonate the bomb to follow his mother, but Frank manages to catapult Rocco and the bomb out of the awarding hall right into Papshmirs private helicopter (which was circling overhead), with the bomb eliminating all hostile parties involved. Frank and Jane reaffirm their love under the applause of the awarding audience and viewers worldwide. Nine months later, Frank and Nordberg rush into the pediatric ward to witness the birth of Franks child. In the end, Frank chases after Nordberg when he sees the baby look similar to him, rather than Frank. Just after they leave, Ed comes out of another hospital room with Jane, who is holding the real baby, unbeknownst to Frank.

==Production==
This is the only film in the series to be directed by  .

Several scenes had been planned for the earlier films but cut out. The opening sequence had been planned for the first film. The scene where Frank and Jane get married, then drive off with Nordberg on the back of the car, was shot for the second film. In the latter, the car being driven is the electric car featured in the second film.

In the opening scene at the train station, the woman with the baby carriage who is assisted by Frank Drebin is played by Susan Breslau, the sister of Jerry and David Zucker.
 The Untouchables, homage to the "Odessa Steps" montage in Sergei Eisensteins famous 1925 silent movie Battleship Potemkin.

Director Peter Segal, in addition to playing the producer of Sawdust and Mildew, also has several minor roles in the film (mostly in voiceover):
* The voice of the suicide bomber in The Untouchables (1987) parody at the start of the film.
* The voice of the KSAD deejay.
* The ADRed scream of the inmate escaping prison by pole-vaulting.
* The real Phil Donahue, before Frank knocks him out and takes his place.
* The voice of the man shouting "Stop the stairs, Joey!" at the Academy Awards.

==Reception== The Untouchables, and the climax at the Academy Awards,  but felt the middle was uninspired, and that the film overall had too little plotting and relied too much on comedy without the romantic or action elements of the previous films.  Others felt that the humor was weak and too similar to that of the previous films. The film holds a 53% rating on Rotten Tomatoes.

The director claims on the DVD that it would have pleased some critics to have stopped the film after the opening credits. Chris Hicks is an example of an unsympathetic critic of the film, who gave the film a rating of two stars.  Roger Ebert was more sympathetic and gave the film three stars, the same rating he had given to  . 

The movie was nominated for and won two Golden Raspberry Awards, Worst Supporting Actor for O.J. Simpson and Worst New Star for Anna Nicole Smith.

===Box office===
The film made over US$51 million domestically according to Box Office Mojo. That more than exceeded its estimate US$30 million production budget. However, this would be the lowest grossing film of the Naked Gun series. Still, 33⅓ managed to grab the #1 weekend box office title in the U.S. during its opening weekend (the other Naked Guns did as well). 

==Cameo appearances==
Numerous celebrities have cameo appearances in the film, both in credited and uncredited roles.

As themselves:
 
* Shannen Doherty – uncredited 
* Olympia Dukakis   
* Morgan Fairchild – uncredited 
* Elliott Gould 
* Mariel Hemingway – uncredited 
* Florence Henderson
* James Earl Jones 
* Mary Lou Retton 
* Raquel Welch 
* Vanna White 
*   as himself and in   as the police station thug)
* Pia Zadora 

As minor characters: director of the Academy Awards (previously appeared as the Pier 32 Dockman in The Naked Gun)
* Ann B. Davis as "Characters of The Brady Bunch#Alice Nelson|Alice" from The Brady Bunch (credited as playing herself) trucker
* R. Lee Ermey as the mess hall guard – uncredited 
* Bill Erwin as the Orchestra Conductor at the Academy Awards - uncredited 
* Julie Strain as the dominatrix
* Rosalind Allen as Bobbi the Soap Opera Actress
* Earl Boen as Dr. Eisendrath
* Robert K. Weiss as Tuba Player
* Peter Segal, Robert LoCash and William Kerr as producers of Sawdust and Mildew Waldo
* Timothy Watters as Bill Clinton
* Randall "Tex" Cobb as Big Hairy Con
* Doris Belack as Dr. Roberts
* Lois de Banzie as Dr. Kohlzac

==Related litigation==
  sued Paramount Second Circuit deemed the use to be protected fair use because of its transformative parodic purpose.

==References==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 