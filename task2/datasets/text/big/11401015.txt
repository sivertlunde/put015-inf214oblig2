Private Parts (1972 film)
{{Infobox film
| name           = Private Parts
| image          = Private Parts (1972).jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Paul Bartel
| producer       = Gene Corman
| writer         = Philip Kearney Les Rendelstein
| starring       = Ayn Ruymen Lucille Benson John Ventantonio
| music          = Hugo Friedhofer Andrew Davis
| editing        = Martin Tubor
| studio         = Penelope Productions
| distributor    = Metro-Goldwyn-Mayer Premier Pictures
| released       =  
| runtime        = 86 minutes  
| country        = United States
| language       = English
}}
Private Parts is a 1972 psychological thriller film with some elements of horror and comedy, directed by Paul Bartel as his feature film debut. The film stars Ayn Ruymen, Lucille Benson, and John Ventantonio.

==Plot==
When Cheryl and her roommate quarrel, Cheryl moves into her aunts skid-row hotel in downtown L.A. rather than return home to Ohio. The lodgers are strange, Aunt Martha is a moralizer obsessed with funerals, murder is afoot, and the inexperienced and trusting Cheryl may be the next victim. She wants to be treated like a woman, and shes drawn to George, a handsome photographer who longs for human contact but sleeps with a water-inflated doll and spies on Cheryl as she bathes. Jeff, a neighborhood clerk, may be Cheryls only ally in what she doesnt realize is a perilous residence haunted by family secrets. And, what happened to Alice, a model who used to have Cheryls room?

==Cast==
* Ayn Ruymen as Cheryl Stratton
* Lucille Benson as Aunt Martha
* John Ventantonio as George
* Laurie Main as Reverend Moon
* Stanley Livingston as Jeff
* Charles Woolf as Jeffs father
* Ann Gibbs as Judy
* Len Travis as Mike
* Dorothy Neumann as Mrs. Quigley
* Gene Simms as First Policeman
* John Lupton as Second Policeman
* Patrick Strong as Artie

==References==
 

==External links==
*  
*  
*   at Trailers From Hell

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 