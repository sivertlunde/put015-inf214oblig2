The House of the Seven Hawks
{{Infobox film
| name           = The House of the Seven Hawks
| image	         = The House of the Seven Hawks - Film Poster.jpg
| image_size     = 225px
| caption        = Theatrical Film Poster
| director       = Richard Thorpe
| producer       = 
| writer         = 
| based on = 
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       = 1959
| runtime        = 
| country        = United Kingdom English
| budget         = $535,000  . 
| gross          = $1,065,000 
| preceded_by    =
| followed_by    = Robert Taylor, Nicole Maurey and Linda Christian.  The film follows an American captain searching for sunken treasure who becomes entangled with criminals and is arrested by the Dutch police. It is based on the Victor Canning novel, The House of the Seven Flies, published in 1952.

==Cast== Robert Taylor ...  John Nordley 
* Nicole Maurey ...  Constanta Sluiter 
* Linda Christian ...  Elsa 
* Donald Wolfit ...  Inspector Van Der Stoor 
* David Kossoff ...  Wilhelm Dekker 
* Eric Pohlmann ...  Captain Rohner 
* Philo Hauser ...  Charlie Ponz 
* Gerard Heinz ...  Inspector Sluiter 
* Paul Hardtmuth ...  Beukleman 
* Lily Kann ...  Gerta 
* Richard Shaw ...  Police Sgt. Straatman 
* André Van Gyseghem ...  Hotel Clerk 
* Leslie Weston ...  Tulper 
* Guy Deghy ...  Desk Lieutenant  Peter Welch ...  Gannett
==Box Office==
According to MGM records the film earned $415,000 in the US and Canada and $650,000 elsewhere, resulting in a loss of $20,000. 
==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 