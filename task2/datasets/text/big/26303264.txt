The Unseen (1945 film)
{{Infobox film
| name           = The Unseen
| image          = Unseen 1945.jpg
| alt            = 
| caption        = Theatrical release poster Lewis Allen
| producer       = John Houseman
| screenplay     = Hagar Wilde Raymond Chandler
| story          = 
| based on       =  
| writer         = Adaptation: Hagar Wilde Ken Englund
| starring       = Joel McCrea Gail Russell Herbert Marshall
| music          = Ernst Toch
| cinematography = John F. Seitz
| editing        = Doane Harrison
| studio         =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Unseen is a 1945 film directed by  .
 The Uninvited (1944).

Raymond Chandler was one of the writers of the script.

==Plot==
At 11 Crescent Drive in a New England village, an unseen killer strangles a woman. Young Barnaby Fielding witnesses this from his window at 10 Crescent and later retrieves the victims watch.

The boys new governess, Elizabeth Howard, is unaware at first how attached Barnaby had become to Maxine, his former one. And the boys father, David, does not take the watch to police because in the past he had been suspected of murdering his wife.

Marian Tygarth, a widow who owns the shuttered-up 11 Crescent, returns to put the house up for sale. Elizabeth, meantime, is accosted by an unseen man and confides in Dr. Evans, a neighbor.

Maxine is found dead and David is missing, causing police to consider him the prime suspect. Marian catches the killer in the act and becomes his next victim. Elizabeth tries to protect Barnaby from his father, whom she has come to love, but it turns out that Dr. Evans is the one responsible for the crimes.

==Cast==
* Joel McCrea as  David Fielding
* Gail Russell as Elizabeth Howard
* Herbert Marshall as Dr. Charles Evans
* Phyllis Brooks as Maxine
* Isobel Elsom as Marian Tygarth Norman Lloyd as Jasper Goodwin
* Mikhail Rasumny as Chester
* Elisabeth Risdon as Mrs. Norris

==References==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 