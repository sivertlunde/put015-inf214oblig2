Disgrace (film)
 
{{Infobox film
| name           = Disgrace
| image          = Disgrace title.jpg
| caption        =
| based on       =  
| screenplay     = Anna Maria Monticelli
| starring       = John Malkovich Jessica Haines Eriq Ebouaney
| director       = Steve Jacobs
| producer       = Steve Jacobs Anna Maria Monticelli Emile Sherman
| cinematography = Steve Arnold
| country        = Australia
| editing        = Alexandre de Franceschi
| released       =  
| runtime        = 120 minutes
| language       = English Afrikaans Zulu Xhosa
| music          = Antony Partos
| gross          = $2,122,574
}} 
Disgrace is an Australian film  set in South Africa, adapted for the screen by Anna Maria Monticelli from the 1999 J. M. Coetzee novel, Disgrace (novel)|Disgrace. The film premiered at the 2008 Toronto International Film Festival,    where it was awarded the Prize of the International Critics.   

The film was directed by Monticellis husband, Steve Jacobs,    and stars John Malkovich alongside South African actress Jessica Haines.   

==Plot summary==
David Lurie (Malkovich) is a South African professor of English at an unnamed university in Cape Town. After an affair with one of his students, he loses his job and his reputation. He takes refuge with his daughter, Lucy (Haines), at her farm in the Eastern Cape. At first the two experience harmony and Lurie finds peace with himself. However, one day Lurie and his daughter are attacked by three men, and Lucy is raped. Subsequently, Lurie goes through a crisis, not knowing how to cope with his personal and family tragedies. He is also confused by the newfound guilt he suddenly feels about his last affairs.

==Reception==
The film received generally positive reviews, with an 82% favorable rating on Rotten Tomatoes, based on 57 votes. The critical consensus states that "Featuring outstanding performances from John Malkovich and newcomer Jessica Haines, Disgrace is a disturbing, powerful drama."
The film holds an average score of 71/100 at Metacritic.   It grossed $1,166,294 at the box office in Australia,  and $2,122,574 worldwide. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 


 