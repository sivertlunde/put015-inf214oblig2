Jhutha Sach (film)
 
{{Infobox film
| name           =Jhootha Sach
| image          = 
| image_size     = 
| caption        = 
| director       =Vikas Desai	 
| producer       =
| writer         =
| narrator       = 
| starring       =Dharmendra Rekha
| music          = Rahul Dev Burman
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1984
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Jhutha Sach  is a 1984 Bollywood film directed by Vikas Desai and starring Dharmendra and Rekha. The film was remade in Telugu as Kodetrachu.

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Kaisi Lag Rahi Hoon Main"
| Kishore Kumar, Lata Mangeshkar
|-
| 2
| "Kaisi Lag Rahi Hoon Main"
| Kishore Kumar, Lata Mangeshkar
|-
| 3
| "Jahan Bin Hawa Ke"
| Asha Bhosle
|-
| 4
| "Kisi Ko Khona Kisi Ko Pana"
| Kishore Kumar, Asha Bhosle, Preeti Sagar, Priya 
|-
| 5
| "Kya Karoon Main To"
| Asha Bhosle
|-
| 6
| "Loot Gai Main To"
| Asha Bhosle
|}

==External links==
*  

 
 
 
 

 