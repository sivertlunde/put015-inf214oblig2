Falling Hare
{{multiple issues|
 
 
}}
{{Infobox Hollywood cartoon
| cartoon_name = Falling Hare
| series = Merrie Melodies/Bugs Bunny
| image = Falling hare title card.jpg
| caption = Title card
| director = Robert Clampett
| story_artist = Warren Foster
| animators = Rod Scribner Virgil Ross (uncredited) Thomas McKimson (uncredited) Robert McKimson (uncredited) Phil Monroe (uncredited)
| voice_actor = Mel Blanc Carl W. Stalling
| producer = Leon Schlesinger Leon Schlesinger Productions
| distributor = Warner Bros. Pictures   The Vitaphone Corporation
| release_date = October 30, 1943 (USA)
| color_process = Technicolor
| runtime = 8 min. (one reel)
| movie_language = English
}}
Falling Hare is a 1943 Merrie Melodies cartoon directed by Robert Clampett, starring Bugs Bunny. As with many Bugs Bunny cartoons, the title is a play on words; "falling hair" refers to impending baldness, while in this cartoons climax the title turns out to be descriptive of Bugs situation (a hare falling / crashing to earth). Recorded on September 27, 1943.

==Plot synopsis== Army Air Force base, to the brassy strains of "We’re In To Win" (a World War II song also sung by Daffy Duck in Scrap Happy Daffy two months before).  The sign at the base reads "U.S. Army Air Field", and below that is shown the location, the number of planes and number of men, all marked "Censored" as a reference to military secrecy.  Beneath those categories, the sign reads "What men think of top sergeant", which is shown with a large white-on-black "CENSORED!!", as the language implied would not pass scrutiny by the Hays Office.
 ordnance next Disney film bomb Bugs asks the Vendell Villkie!"
 4F labeled on it  (the term refers to a military draftee rejected for being physically unfit). The gremlin then pries Bugs off the door with a bar, slamming the rabbit into a wall where he is flattened into a coin shape, then dropped through the bomb bay doors and caught by his feet on a wire between the doors. He sees the Gremlin in the cockpit at the controls, flying toward a pair of skyscrapers. Bugs rushes into the cockpit, takes control of the airplane and flies between the towers vertically, emerging in a "victory roll".

In the finale, the plane goes into a tailspin (ripping apart during its descent, with only the fuselage remaining), making Bugs visibly airsickness|airsick; the airspeed indicators spinning numbers escalate wildly into the tens of thousands of miles an hour (this was mainly for comic effect, as supersonic flight had not yet been achieved and no sonic boom was heard), briefly pausing to state, "Incredible Aint It???"—but comes to a sputtering halt (with sound effects by voice actor Mel Blanc, borrowing from his portrayal of the Maxwell automobile on the radio show The Jack Benny Program) about six feet before hitting the ground, hanging in mid-air, defying gravity.

Bugs and the Gremlin break kayfabe (as well as, again, the fourth wall) and address the audience. The gremlin apologizes for the plane having "run out of gas". Bugs chimes in and just as he speaks, the camera pans to the right, revealing a wartime gas rationing sticker: "You know how it is with these A cards!" 

==Production== Russian Rhapsody to remove the references to gremlins, so Leon Schlesinger merely re-titled the cartoons as a compromise. 

==Release and reception==
{{double image|right||150||150|
*Print comparison of the "Eh, whats all the hubbub, bub?" scene (PD print on left, LTGC print on right)}}

*Because of the cartoons   of the Looney Tunes Golden Collection.

*When the Southern Television broadcast interruption occurred in the United Kingdom, the interruption ended shortly before the start of this episode.
 Twilight Zone episode "Nightmare at 20,000 Feet". It also made a brief cameo in the Animaniacs episode "Plane Pals" as a passenger.

*When Bugs says "Im only 3 1/2 years old" (not in a childs voice) and rolling on the floor flat as a pancake, it was used in the Tiny Toon Adventures episode: "Who Bopped Bugs Bunny?"

==In popular culture==
The climactic scene in "Falling Hare" is described in detail in the novel The Long Dark Tea-Time of the Soul.

==Sources==
*  
*  

==See also==
* List of films in the public domain
* The Gremlins
* Gremlin
* Gremlins
* List of Bugs Bunny cartoons

== References ==
 

==External links==
 
*  
*  

  
{{succession box |
before= A Corny Concerto (not explicitly billed a Bugs Bunny cartoon) | Bugs Bunny Cartoons |
years=1943 |
after= Little Red Riding Rabbit|}}
 

 
 
 
 
 
 
 