Stranger on the Third Floor
{{Infobox film
| name            = Stranger on the Third Floor
| image           = Strangeronthe.jpg
| image_size      = 
| caption         = Theatrical release poster
| alt             =
| director        = Boris Ingster 
| producer        = Lee S. Marcus
| screenplay      = Frank Partos Nathanael West
| story           = Frank Partos Nathanael West John McGuire Margaret Tallichet Elisha Cook Jr.
| music           = Roy Webb
| cinematography  = Nicholas Musuraca
| editing         = Harry Marker
| distributor     = RKO Pictures
| released        =  
| runtime         = 64 minutes
| country         = United States
| language        = English
| budget          = $171,200 (estimated)}}

Stranger on the Third Floor is a 1940 film noir, starring Peter Lorre and released by RKO Radio Pictures. The film was directed by Boris Ingster and co-written by Nathaniel West. 

Stranger on the Third Floor is often cited as the first "true" film noir of the classic period (1940–1959).    But it was released August 16, 1940, which was after both Rebecca (1940 film)|Rebecca and They Drive by Night.  Nonetheless, it has many of the hallmarks of noir: an urban setting, heavy shadows, diagonal lines, voice-over narration, a dream sequence, low camera angles shooting up multi-story staircases, and an innocent protagonist falsely accused of a crime who is desperate to clear himself.

==Plot==
Reporter Michael Ward is the key witness in a murder trial. His evidence &ndash; that he saw the accused Briggs standing over the body of a man in a diner &ndash; is instrumental in having Briggs found guilty.

Afterwards Ward’s fiancee Jane is worried whether Ward was correct in what he saw and Ward becomes haunted by this question. Next, Ward’s neighbour is killed the same way as the man in the diner, but Ward is arrested for trying to point this out to the police. As a result, Jane goes out to try to clear Ward by finding the sinister stranger that Ward saw on the stairwell.

==Cast==
* Peter Lorre as The Stranger John McGuire as Mike Ward
* Margaret Tallichet as Jane
* Charles Waldron as District Attorney
* Elisha Cook Jr. as Joe Briggs
* Charles Halton as Albert Meng
* Ethel Griffies as Mrs. Kane, Michaels landlady
* Cliff Clark as Martin
* Oscar OShea as The Judge
* Alec Craig as Briggs Defense Attorney

==Reception==

===Critical response===
Upon its release in 1940, The New York Times film critic, Bosley Crowther, called the film pretentious and derivative of French and Russian films, and wrote, "John McGuire and Margaret Tallichet, as the reporter and his girl, are permitted to act half-way normal, it is true. But in every other respect, including Peter Lorres brief role as the whack, it is utterly wild. The notion seems to have been that the way to put a psychological melodrama across is to pile on the sound effects and trick up the photography." 

The staff at Variety (magazine)|Variety also believed the film was derivative, and wrote, "The familiar artifice of placing the scribe in parallel plight, with the newspaperman arrested for two slayings and only clearing himself because of his sweethearts persistent search for the real slayer, is used...Boris Ingsters direction is too studied and when original, lacks the flare to hold attention. Its a film too arty for average audiences, and too humdrum for others." 

Dave Kehr, writing for the Chicago Reader, calls the film "An RKO B-film from 1940, done up in high Hollywood expressionism. Its absurdly overwrought (which was often the problem with the German variety), but interesting for it. The director, Boris Ingster, is better with shadows than with actors&mdash;venetian blinds carve up the characters with more fateful force than Paul Schraders similar gambit in American Gigolo, and theres a dream sequence that has to be seen to be disbelieved." 

Currently, the film has an 80% "Fresh" rating at Rotten Tomatoes, based on five professional reviews. 

==References==

===Notes===
 

===Additional references===
* Lyons, Arthur (2000). Death on the Cheap: The Lost B Movies of Film Noir. New York: Da Capo. ISBN 0-306-80996-6
* Server, Lee (1998). "The Black List: Essential Film Noir" in The Big Book of Noir, ed. Ed Gorman, Lee Server, and Martin H. Greenberg. New York: Carroll & Graf. ISBN 0-7867-0574-4

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 