Thiruppu Munai
{{Infobox film
| name           = Thiruppu Munai
| image          = 
| caption        = 
| director       = Kalaivanan Kannadasan
| producer       = B. Kandaswami
| writer         = K. N. Lakshmanan
| starring       =  
| music          = Ilaiyaraaja
| cinematography = G. Dhanapal
| editing        = B. K. Mohan
| studio         = B. K. Enterprizes
| distributor    = 
| released       =  
| country        = India
| runtime        = 130 min
| language       = Tamil
}}
 Tamil crime Karthik and Chitra in lead roles. The film, produced by B. Kandaswami, had musical score by Ilaiyaraaja and was released on 3 November 1989.  

==Plot==

Sathyamoorthy (J. V. Somayajulu), an honest politician, becomes the new Minister of Justice. Chakravarthy (Vinu Chakravarthy), a corrupted politician, who wants to be a minister feels ridiculous and tries to kill Sathyamoorthy but Chakravarthy fails each time.

Rajaram (Karthik (actor)|Karthik), a jobless graduate, comes to the city alone to find a job. He has a mother and he has many debts. There, he becomes friend with Pichandi (Janagaraj (actor)|Janagaraj), who is also a jobless graduate, and Chidambaram (Vagai Chandrasekhar), a disabled family man. Sangeetha, Chakravarthys niece, (Silk Smitha) interviewed Rajaram for a job and he passes it well. Rajaram is hired by Chakravarthy but Chakravarthy asks him to first charm Chitra (Chitra (actress)|Chitra), Sathyamoorthys daughter. Rajaram tries to charm her and Chitra falls in love with him. Sathyamoorthy wants to see, his daughters lover, Rajaram, so Chakravarthy gives him a flower bouquet with a bomb inside. Sathyamoorthy appreciates Rajaram and he gives the flower bouquet to Sathyamoorthy. Sathyamoorthy dies in the bomb blast. Rajaram is sentenced to life emprisonnement and Pichandi to five years in prison.

Chakravarthy has succeeded to the minister post and he decides to kill the witnesses. Anand (Thyagu), the jailer, because of his corrupted services, becomes the sub-jailer. Vanchinathan, who looks like Rajaram, is the new jailer. Manimudi (Ajay Ratnam), Chakravarthys henchman, kills Pichandi under the command of Chakravarthy and Vanchinathan is sentenced to life emprisonnement. Vanchinathan escapes from the jail and with Chitra, they try to flee the innocent Rajaram. Chidambaram and his wife (Sabitha Anand) were murdered by Chakravarthys henchmen.

Finally, Rajaram and Vanchinathan kill Chakravarthys henchmen. Chakravarthy is sentenced to life emprisonnement, Rajaram and Chitra get married and Vanchinathan decides to grow up Chidambarams son.

==Cast==
 Karthik as Rajaram / Vanchinathan Chitra as Chitra Janagaraj as Pichandi
*Vagai Chandrasekhar as Chidambaram
*Vinu Chakravarthy as Chakravarthy
*J. V. Somayajulu as Sathyamoorthy
*Silk Smitha as Sangeetha
*Sabitha Anand as Chidambarams wife Thyagu as Anand
*Ajay Rathnam as Manimudi
*Jaiganesh in a guest appearance

==Soundtrack==

{{Infobox album  
| Name        = Thiruppu Munai
| Type        = soundtrack
| Artist      = Ilaiyaraaja
| Cover       =
| Released    = 1989
| Recorded    = 1989 Feature film soundtrack
| Length      = 17:35
| Label       = 
| Producer    = Ilaiyaraaja
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1989, 4 features tracks with lyrics written by Pulamaipithan, Ilaiyaraaja, Gangai Amaran and Kalaivanan Kannadasan.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Lyrics!! Duration 
|-  1 || Adi Ye Pulla || Mano (singer)|Mano, K. S. Chithra, Sunandha || Pulamaipithan || 4:33
|- 2 || Ammana Summa Illada || Ilaiyaraaja || Ilaiyaraaja || 4:40
|- 3 || Ilamai Ithu || Mano, K. S. Chithra || Gangai Amaran || 4:02
|- 4 || Oru Naal || Mano, K. S. Chithra || Kalaivanan Kannadasan || 4:20
|}

==External links==
 

==References==
 

 
 
 
 