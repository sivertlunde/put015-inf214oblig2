Does the Jazz Lead to Destruction
 
{{Infobox film
| name           = Does the Jazz Lead to Destruction
| image          = 
| caption        = 
| director       = 
| producer       = 
| writer         = 
| based on       = 
| starring       = Ethel Bennetto George Irving
| music          =
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        =
| country        = Australia
| language       = Silent film  English intertitles
| budget         = 
| gross          = 
}}

Does the Jazz Lead to Destruction? is a 1919 Australian silent film about the jazz craze. It is considered a lost film.

==Plot==
A family of wowsers, the McWowses, oppose jazz dancing but are converted to its joys.  Several dances are featured, including the Walking Waltz, the Jazz, the Tickle-Toe and the Whirly Whirly. These were performed by the leads.  

==Production==
Ethel Bennetto and George Irving were both jazz experts from Sydney who performed the dances in the film. 

==Release==
During the lead up to the films release, letters from the fictitious characters, the McWowses, would appear in press advertising complaining about jazz. 

The film is often confused with another jazz comedy, Why Jessie Learned to Jazz, for Australasian Films and director Frederick Ward, which was announced for production but was likely abandoned. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 88. 

==References==
 

==External links==
* 
*  at National Film and Sound Archive

 
 
 
 
 
 


 