Non-chan Kumo ni Noru
{{Infobox Film
| name           = Non-chan Kumo ni Noru
| image          = Non-chan kumo ni noru poster.jpg
| caption        = Japanese movie poster
| director       = Fumito Kurata
| producer       = Shintoho
| writer         = Momoko Ishii (novel)
| starring       = 
| music          = 
| cinematography = Joji Ohara
| editing        = 
| distributor    = Shintoho
| released       = June 7, 1955 
| runtime        = 84 min.
| country        = Japan
| awards         = 
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Japanese film directed by Fumito Kurata. It was based on a best-seller by Momoko Ishii.

== Cast ==
* Setsuko Hara as Nobukos mother
* Haruko Wanibuchi as Nobuko Tashiro
* Susumu Fujita as Nobukos father
* Musei Tokugawa as old man
* Akira Ōizumi 
* Michiko Ozawa

== References ==
 

== External links ==
*  

 

 
 
 
 