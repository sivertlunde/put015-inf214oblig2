Leela (2002 film)
{{Infobox film
| name           = Leela
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Somnath Sen
| producer       = Kavita Munjal   Anjalika Mathur
| screenplay     = 
| based on       =  
| narrator       = 
| starring       = Dimple Kapadia Deepti Naval Vinod Khanna
| music          = Shantanu Moitra
| lyrics         = Gulzar, Nida Fazli 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Lemon Tree Films
| released       =  
| runtime        = 97 min 
| country        = USA
| language       = English
| budget         = 
| gross          =
}}
 2002 drama film directed by Somnath Sen. The movie stars Dimple Kapadia and Deepti Naval. The films story is loosely based on Summer of 42,. {{cite news
|url=http://www.hindu.com/thehindu/fr/2002/11/15/stories/2002111501040203.htm
|title=`Leela 
|date=November 15, 2002 
|work=The Hindu
|last=Ziya US Salam
}}  The film premiered at Reel World Film Festivalm, Toronto, in 2005. {{cite news
|url=http://www.deeptinaval.com/writeups/poet/grn_pastures_statesman_feb26_05.htm
|title=`Deepti Naval in conversation with Shoma A. Chatterji 
|date=February 26, 2005 
|work=The Statesman
|last=Shoma A. Chatterji
}}  It also was featured in the 2002 edition of the IAAC Film Festival, {{cite news
|url=http://www.allbusiness.com/government/government-bodies-offices/5997789-1.html
|title=`Indian Diaspora Film Festival Runs Nov 6-10 at Clearview Cinema, New York; Event Highlights... 
|date=October 15, 2002 
|work=Business Wire
|last=Aroon Shivdasani
}}  conducted by the Indian diaspora, which works to showcase the Indian films to the West.

==Production==
The film was shot mostly in Los Angeles with funding from Kavita Munjal and Somnath Sen. This is the directorial debut of Somnath Sen.
. {{cite news
|url=http://aivf.org/node/295
|title=`The new, new, Indian Cinema 
|date=July 1, 2005 
|work=The Independent
|last=David Alm
}}  "Its a universal story that many women will be able to relate to," remarked Dimple Kapadia, on the sets. {{cite news
|url=http://www.rediff.com/news/2001/may/25usspec.htm
|title=`"I got more than my share in my life" 
|date= May 25, 2001
|work=Rediff
}} 

==Plot==
Leela is a professor from Bombay. Married to a popular poet, her life has always been prefixed with the title Nashaads wife. She wins a chance to be sent as a visiting professor to California, where she rediscovers herself, the woman she is, beyond the duties of a wife that she had always devoted her life to. Kris comes as a bright sunshine in her life, who makes her realise her own desires, the desires of the body. Is it going to be an happily ever after fairy tale? Or another Bollywood melodrama?

==Cast==
* Dimple Kapadia as Leela
* Deepti Naval as Chaitali
* Amol Mhatre as Kris/ Krishna
* Vinod Khanna as Nashaad
* Gulshan Grover as Jai Brendan Hughes as Summer
* Kelly Gunning as JC
* Garrett Devereux as Chip
* Kyle Erby as Jamaal
* Michelle Van Wagner as Jennifer
* Sarayu Rao as Mira
* Partha Dey as Shantanu
* Sandeep Walia as Harsh
* Delna Rastomjee as Joya Chatterjee
* Gargi Sen as Maheed

==Critical Reception==
Anil. S. Arora, of   wrote that film could be considered "the first art film made on the Indian community in north America". He added that the film was about the taboo that sex still is to the Indian Community and their "inability to accept them as the basis for family relationships". He though was dissatisfied with the way Leela is portrayed as a faithful wife to a straying husband, which he wrote is quite outdated for a Mumbai woman who dwells on sexual equality. {{cite news
|url=http://www.chowk.com/Arts/Sex-in-the-Indian-Family
|title=`IA review of Somnath Sen’s film ‘Leela’ 
|date=November 6, 2002  last = Anil. S. Arora
|work= 
}} 

The review in The Hindu was moderate, which mainly lauded Dimple Kapadias acting, declared that the plot has many loose ends andsigned off with a note that it is not a bad choice to watch "at the end of a long, lonely day" 

The Daily Telegraph declared it as a flop in its positive review on Shantanu Moitra. {{cite news
|url=http://www.telegraphindia.com/1051210/asp/weekend/story_5563284.asp
|title=`The new melody moguls  
|date=December 10, 2005  The Telegraph last = Sushmita Biswas
}} 

==Recognition & Awards==
* Special Jury Award - Reel World Film Festival, Toronto (2002)
* Screened at the Commonwealth Film Festival, Manchester (2002) {{cite web
|url=http://www.upperstall.com/content/indian-diaspora-film-festival-2002-new-york
|title=`Indian diaspora film festival 2002, New York  
|year=2002
|work=Upperstall.com
}} 

==References==
 

==External links==
*  

 
 
 