Pichathy Kuttappan
{{Infobox film
| name           = Pichathy Kuttappan
| image          =
| caption        =
| director       = P Venu
| producer       =
| writer         = Sasikala Venu N. Govindankutty (dialogues)
| screenplay     = N. Govindankutty Sharada
| music          = K. Raghavan
| cinematography = CJ Mohan
| editing        = K Sankunni
| studio         = SDM Combines
| distributor    = SDM Combines
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film, Sharada in lead roles. The film had musical score by K. Raghavan.   

==Cast==
*Prem Nazir
*Jayan
*Sheela Sharada
*KPAC Lalitha
*Jose Prakash
*Alummoodan
*Bahadoor
*Kaduvakulam Antony
*Mallika Sukumaran

==Soundtrack==
The music was composed by K. Raghavan and lyrics was written by Yusufali Kechery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Annanada ponnala || S Janaki, Chorus || Yusufali Kechery || 
|-
| 2 || Daaham njaanoru daaham || K. J. Yesudas || Yusufali Kechery || 
|-
| 3 || Moovanthi Nerathu || P Jayachandran, Chorus || Yusufali Kechery || 
|-
| 4 || Odivarum Kaattil || P Susheela || Yusufali Kechery || 
|-
| 5 || Punchiriyo || P Jayachandran || Yusufali Kechery || 
|}

==References==
 

==External links==
*  

 
 
 

 