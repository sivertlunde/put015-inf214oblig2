First Offenders
 

{{Infobox film
| name           = First Offenders
| image          =
| caption        = Frank McDonald
| producer       =
| writer         = J. Edward Slavin (story) Walter Wise (screenplay)
| narrator       =
| starring       = Walter Abel Beverly Roberts Iris Meredith
| music          =
| cinematography =
| editing        =
| studio         = Columbia Pictures
| distributor    =
| released       =  
| runtime        = 63 minutes
| country        = United States English
| budget         =
}}

First Offenders is a 1939 film starring Walter Abel, Beverly Roberts and Iris Meredith.

==Plot==
A crusading and reform-minded District Attorney resigns from his position in order to open establish a farm that give juvenile delinquents and first-offenders a place to straighten out their lives before they reach the point of no return. He meets much resistance from various segments of the law and the citizens.

==References==
 

==External links==
* 

 
 
 
 


 