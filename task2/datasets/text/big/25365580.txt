Gradiva (novel)
{{Infobox book 
| name          = Gradiva
| title_orig    = 
| translator    = 
| image         =  
| caption = 
| author        = Wilhelm Jensen
| illustrator   = 
| cover_artist  = 
| country       = Germany
| language      = German
| genre         = Romance novel Fischer Taschenbuch Verlag
| release_date  = 1903
| english_release_date =
| media_type    = Print (hardback & paperback)
| oclc = 7305023  
| preceded_by   = 
| followed_by   = 
}}
 a Roman bas-relief of the same name and became the basis for Sigmund Freuds famous 1907 study Delusion and Dream in Jensens Gradiva ( ). Freud owned a copy of this bas-relief, which he had joyfully beheld in the Vatican Museums in 1907; it can be found on the wall of his study (the room where he died) in 20 Maresfield Gardens, London &ndash; now the Freud Museum.

==Plot synopsis==
The story is about an archaeologist named Norbert Hanold who is obsessed with a woman depicted in a bas-relief that he sees in a museum in Rome. After his return to Germany, he manages to get a plaster-cast of the relief, which he hangs on a wall in his work-room and contemplates daily. He comes to feel that her calm, quiet manner does not belong in bustling, cosmopolitan Rome, but rather in some smaller city, and one day an image comes to him of the girl in the relief walking on the peculiar stepping-stones that cross the streets in Pompeii. Soon afterwards, Hanold dreams that he has been transported back in time to meet the girl whose unusual gait so captivates him. He sees her walking in the streets of Pompeii while the hot ashes of Vesuvius subsume the city in 79 AD.

This fantastical dream leads Hanold on a real journey to Rome, Naples, and ultimately Pompeii, where, amazingly, he sees the Gradiva of his bas-relief stepping calmly and buoyantly across the lava stepping-stones. He follows her, loses her, then finds her sitting on the low steps between two pillars. He greets her in Latin, only to be answered, "If you wish to speak to me, you must do so in German." When he addresses her as if she were the girl of his dream, however, she looks at him without comprehension, gets up and leaves. Hanold calls out after her, "Are you coming here again tomorrow in the noon hour?" But she does not turn round, gives no answer, and a few moments later disappears round the corner. Hanold hurries after her, but she is nowhere to be seen. What follows is his quest to determine whether the woman he has seen is real or a delusion.

==Films==
In 1970, the Italian actor and filmmaker Giorgio Albertazzi released a film titled Gradiva, based on Jensens novel and featuring Laura Antonelli as Gradiva. Albertazzi is best known for his portrayal of the male protagonist in Last Year at Marienbad, written by Alain Robbe-Grillet, who would himself go on to direct a film based on Jensens novel.
 art historian named John Locke who is doing research in Morocco on the paintings and drawings that French artist Eugène Delacroix (1798&nbsp;&ndash; 1863) produced over a century before, when he travelled to the country, then a French colony, as part of a diplomatic mission. Locke spots a beautiful, mysterious blonde girl (Gradiva) in flowing robes dashing through the back alleys of Marrakech, and becomes consumed with the need to track her down. Like most of Robbe-Grillets cinematic output, the film is highly surreal and also has explicit scenes of sex slavery, nudity and sadomasochism.

==External links==
*  
* Freud Museum, Vienna:  
* Freud Museum, London:  
* The relief at the Freud Museum, London:  

 
 
 
 
 
 
 
 
 