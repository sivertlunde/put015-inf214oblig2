Agent Cody Banks 2: Destination London
{{Infobox film
| name           = Agent Cody Banks 2: Destination London
| image          = Agent Cody Banks 2 film.jpg
| caption        = Theatrical release poster Kevin Allen
| producer       = David Glasser Andreas Klein Guy Oseary David Nicksay Dylan Sellers
| screenplay     = Don Rhymer
| story          = Harald Zwart Dylan Sellers  Don Rhymer
| based on       =  
| starring       = Frankie Muniz Anthony Anderson Cynthia Stevenson Daniel Roebuck Keith David
| music          = Mark Thomas
| cinematography = Denis Crossan
| editing        = Andrew MacRitchie
| studio         = Splendid Pictures Maverick Films Dylan Sellers Productions
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 100 minutes
| country        = United States United Kingdom
| language       = English  
| budget         = $26 million 
| gross          = $28.8 million
}}
Agent Cody Banks 2: Destination London is an action comedy film and the sequel to the 2003 film Agent Cody Banks, and was released in the United States on March 12, 2004. Frankie Muniz was the only major returning star, with Hannah Spearritt playing the love interest and Anthony Anderson as the sidekick. The film takes place in London with Cody trying to recover a stolen software activating the governments mind control project.

The film grossed United States dollar|US$28,818,995 worldwide, a significant drop from the original film, which grossed over $50 million internationally. 

==Plot== Keith Allen), Cody helps him escape, mistaking the CIA operation for a training exercise. The director informs Cody that Diaz stole disks containing plans for a secret mind-control device, and sends Cody to recapture him.

In the United Kingdom, Cody poses as a summer orchestra student at the Kenworth estate to spy on owner Lord Duncan Kenworth, suspected of working with Diaz, supported by his handler, Derek (Anthony Anderson) and Kumar, Dereks right-hand man, who are disguised as a chef hired by Lady Kenworth and a taxi driver respectively. Whilst keeping his mission a secret from his fellow students, Cody sneaks around the estate and confirms that Diaz and Duncan are working together and that they have a working prototype of the mind control device, evidenced when Duncan makes a dog serve drinks and play the piano.

The next day, Cody breaks into a lab owned by Duncan, where he sees the finished device: a microchip inserted as a filling into a tooth cavity by dentist Santiago. Shortly afterwards Cody and Derek chase Diaz, armed with a rocket gun, through London streets, but Cody is captured by the Metropolitan Police Service and taken to Scotland Yard. He is later freed by Emily (Hannah Spearritt), a fellow student who, similar to Cody, is actually a British MI6 undercover operative. While Emily buys coffee and soda, henchmen sneak up on Cody, knocking him unconscious by drugging him with spray. They kidnap him and implant him with the microchip.

Under Duncan, Santiago and Diazs influence, Cody meets the CIA director, who is then also converted. This is witnessed by Emily, who explains things to Derek. To get the microchip out of Cody, Derek cuts one of Codys gadgets, exploding Mentos mints, into a precisely minuscule amount to safely remove it. The group later realise Diazs plan: to implant all of the world leaders, who are all in London for a G7 summit at Buckingham Palace, effectively giving him control of the world.

Deducing that with the CIA director under Diazs control, they may be put on a most wanted list, Cody, Derek and Emily infiltrate the party before the summit. They explain the truth to the other students, who are performing for the guests, and urge them to keep the world leaders from attending the G7 summit. They later proceed to do so with an impromptu performance of War (Edwin Starr song)|War, whilst Cody, Emily and Derek search for the villains. Derek is implanted with the microchip, and is set on Cody by Santiago. Before Santiago can kill him through Derek, Emily finds and subdues him, disabling the mind control software and rescuing the U.S. President, who was to be implanted.

Shortly after Cody kicks out Dereks microchip, the two of them remove the CIA directors microchip. Diaz, realising that his plan has failed attempts to flee, but ends up fighting, and being defeated by Cody in the Elizabeth II of the United Kingdom|Queens gift room. Duncan also attempts to escape, but is tripped by his apparently senile and blind butler, who turns out to be Emilys handler.

After the villains are arrested, Cody returns to the camp, where Derek is now in charge as reward.  Codys parents pick him up, none the wiser about his dangerous exploits. Alex, Codys younger brother tries to eat a few of his explosive Mentos, but Cody tosses them into the pond where they explode harmlessly.

==Cast==
in credits order.

* Frankie Muniz as Cody Banks
* Anthony Anderson as Derek Bowman
* Hannah Spearritt as Emily Sommers
* Cynthia Stevenson as Mrs. Banks
* Daniel Roebuck as Mr. Banks
* Anna Chancellor as Lady Josephine Kensworth Keith Allen as Victor Diaz James Faulkner as Lord Duncan Kensworth David Kelly as Trival
* Santiago Segura as Dr. Santiago
* Connor Widdows as Alex Banks
* Keith David as the CIA Director
* Rod Silvers as Kumar
* Jack Stanley as Ryan
* Olivia Adams as Gabrielle
* Don Jones as George
* Joshua Brody as Bender
* Sarah McNicholas as Marisa
* Leilah Isaac as Sabeen - Bassoon Player
* Alfie Allen as Johan Berchamp
* Keiron Nelson as Habu - French Horn Player
* Ray Donn as Soldier (uncredited)

==Music==
* William Tell Overture|William Tell Overture

== Reception ==
Like the first film, Agent Cody Banks 2: Destination London received generally negative reviews. Review aggregate Rotten Tomatoes gives the film a 13% "Rotten" rating from 94 critics, commenting that "Young kids may find this London adventure fun, but older kids may find it too simplistic." 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 