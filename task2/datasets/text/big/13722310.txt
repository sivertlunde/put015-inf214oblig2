Ganito Kami Noon, Paano Kayo Ngayon
{{Infobox film
| name           = Ganito kami noon, paano kayo ngayon
| image          = 
| caption        = 
| director       = Eddie Romero
| producer       = Dennis Juban
| writer         = Roy C. Iglesias, Eddie Romero 
| narrator       = 
| starring       = Christopher De Leon , Gloria Diaz
| music          = Lutgardo Labad
| cinematography = Justo Paulino       
| editing        = Ben Barcelon 
| distributor    = Hemisphere
| released       =  
| runtime        = 125 minutes
| country        = Philippines
| language       = Tagalog
| budget         = 
}} Filipino musical romantic musical Spanish colonization Best Foreign Language Film at the 49th Academy Awards, but was not accepted as a nominee. 
 digitally restored version of the film by the ABS-CBN Archives and Central Digital Lab will be shown as the opening entry for the Cinema One Originals Festival.   

==Plot== revolution against American colonizers, it follows a naive peasant through his leap of faith to become a member of an imagined community.

==Cast==
*Christopher De Leon as  Nicolas "Kulas" Ocampo
*Gloria Diaz as  Diding
*Eddie Garcia
*Leopoldo Salcedo

==Awards==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award
! width="25%"| Category
! width="25%"| Nominee
! width="10%"| Result
|- 1976
| rowspan="6" align="left"| Metro Manila Film Festival   Best Film
| align="center"| Ganito Kami Noon, Paano Kayo Ngayon
|  
|- Best Director
| align="center"| Eddie Romero
|  
|- Best Actor
| align="center"| Christopher de Leon
|  
|- Best Screenplay
| align="center"| Eddie Romero & Roy C. Iglesias
|  
|- Best Music Lutgardo Labad 
|  
|- Best Art Direction
| align="center"| Laida Lim-Perez and Peque Gallaga
|  
|}

==See also==
* List of submissions to the 49th Academy Awards for Best Foreign Language Film
* List of Philippine submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 
 
 


 
 