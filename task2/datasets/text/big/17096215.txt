One Night in Eden
 
 
{{Infobox Film
| name           = One Night in Eden
| image          = OneNightinEden.JPG
| image_size     =
| caption        = David Mallet Frank Peterson
| producer       = Sarah Brightman Frank Peterson
| writer         =
| narrator       =
| starring       = Sarah Brightman
| music          =
| cinematography =
| editing        = Angel
| released       =  
| runtime        = 92 min.
| country        =
| language       =
| budget         =
| gross          =
}}

One Night in Eden is a live concert recording by Sarah Brightman, inspired by her Eden (Sarah Brightman album)|Eden album. The premiere concert held in Johannesburg, South Africa was recorded in 1999 and has been released on DVD and VHS.

== Track listing ==
# "Introduction"
# "Chants dAuvergne|Baïlèro"
# "In Paradisum"
# "Eden (Hooverphonic song)|Eden"
# "So Many Things"
# "Who Wants to Live Forever"
# "Anytime, Anywhere"
# "Lascia Chio Pianga"
# "Nella Fantasia"
# "Nessun Dorma" Captain Nemo"
# "La Mer"
# "Il Mio Cuore Va"
# "Only An Ocean Away"
# "First of May"
# "Phantom Overture & Little Lotte"
# "Wishing You Were Somehow Here Again"
# "Music of the Night"
# "Deliver Me"
# "Time to Say Goodbye"

==Certifications==
 
 
 
 

==References==
 

== External links ==
*  

 

 
 
 
 
 
 