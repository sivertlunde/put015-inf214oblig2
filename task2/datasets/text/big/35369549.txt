Dil Tujhko Diya
{{Infobox film
| name           = Dil Tujhko Diya
 
| caption        = Audio Cassette Cover Rakesh Kumar Rakesh Kumar
| writer         = Sayed Sultan
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Kumar Gaurav Rati Agnihotri Mala Sinha Parikshat Sahni Amrish Puri
| music          = Rajesh Roshan
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}} Indian Bollywood film directed and produced by Rakesh Kumar. It stars Kumar Gaurav and Rati Agnihotri in pivotal roles.

==Cast==
* Kumar Gaurav...Vijay Chhotu Munna Sahni
* Rati Agnihotri...Rati
* Mala Sinha...Savitri
* Parikshat Sahni...Ajay Sahni
* Aruna Irani...Mary / Meera V. Sahni
* Suresh Oberoi...Ashok A. Sahni
* Amrish Puri...Mohla
* Sharat Saxena...Advocate Heera Asit Sen...Teachers Day Speaker
* Leela Chitnis...Mrs. Sahni
* Gurbachchan Singh...Mahesh
* Piloo J. Wadia...Angry Teacher Asit Sen...Teachers Day Speaker
* Pinchoo Kapoor...Girdharilal

==Soundtrack== Eternal Sunshine of the Spotless Mind.

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Wada Na Tod"
| Lata Mangeshkar
|-
| 2
| "Zindagi Chand Dino Ki"
| Kishore Kumar, Asha Bhosle
|-
| 3
| "Mast Sama, O Meri Jaan"
| Asha Bhosle
|-
| 4
| "Jane Man Main Koi"
| Kishore Kumar
|-
| 5
| "Dil Tukhko Diya"
| Lata Mangeshkar
|}

==References==
 

==External links==
* 

 
 
 

 