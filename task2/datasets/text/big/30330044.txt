Return of the Sentimental Swordsman
 
 
{{Infobox film name = Return of the Sentimental Swordsman image = ReturnoftheSentimentalSwordsman.jpg alt =  caption = Film poster traditional = 魔劍俠情 simplified = 魔剑侠情 pinyin = Mó Jiàn Xiá Qíng jyutping = Mo1 Gim3 Hap6 Cing4}} director = Chor Yuen producer = Run Run Shaw writer = Chor Yuen based on =   narrator = Cheng Miu starring = Yueh Hua music = Eddie H. Wang cinematography = Wong Chit editing = Chiang Hsing-lung Yu Siu-fung studio = Shaw Brothers Studio distributor = Shaw Brothers Studio released =   runtime = 101 minutes country = Hong Kong language = Mandarin budget =  gross = HK$4,302,930
}} Xiaoli Feidao series of novels. It stars Ti Lung, Alexander Fu and Derek Yee.

It is the sequel to The Sentimental Swordsman (1977). It was one of Shaw Brothers highest grossing films in the studios history, surpassing the original at the box office. An in-name-only "sequel", Perils of the Sentimental Swordsman, was released in 1982, with no relation to the main character of Little Flying Dagger Li.

== Synopsis ==
Li Xunhuan comes back to his home after three years of wandering. He is decided to have a normal life, but a group of skilled martial arts fighters and leaders are bent on killing him, so they can be ranked top by Bai Xiaosheng in his renowned list of the best warriors in the martial arts world. Li Xunhuan battles them as he searches for his estranged friend Afei, who is now married and living in seclusion. Li asks Afei to join forces and fight against a new threat that wants to rule the world: the Money Clan.

== Cast ==
  
*Ti Lung as Li Xunhuan
*Alexander Fu as Jing Wuming
*Derek Yee as Afei
* Ching Li as Lin Shiyin
*Lo Lieh as Hu Bugui
*Ku Feng as Shangguan Jinhong
*Choh Seung-wan as Lin Xianer
*Ku Kuan-chung as Shangguan Fei
*Kara Hui as Sun Xiaohong
*Tony Liu as Lu Fengxian (White Gown)
*Helen Poon as Xianers maid Yueh Hua as Guo Songyang
*Cheng Miu as Bai Xiaosheng
*Yuen Wah as Ximen Rou (Green Faced Spear)
*Yuen Bun as Zhu Ge (One Legged Iron Stick)
*Shum Lo as boss at eatery
*Lau Wai-ling as prostitute
*Wong Ching-ho as restaurant boss
*Wong Pau-gei as Money Clan member
*Wong Chi-ming as martial artist
 
*Tam Bo as Money Clan member / martial artist
*Cheung Chok-chow as restaurant patron
*To Wing-leung as restaurant patron
*Kong Chuen as Money Clan member
*Lee Hang as Money Clan member
*Fong Yue as restaurant patron
*Siu Tak-foo as Money Clan member
*Cheung Bing-chan as Money Clan member
*Ma Hon-yuen as martial artist
*Lam Wai as Money Clan member
*Lee Wan-miu as Jinhongs guest
*Siao Yuk as Xianers hired assassin
*Jue Gong as Money Clan member
*Ling Chi-hung as Money Clan member
*Gam Tin-chue as Jinhongs guest
*Cheung Chi-ping as Money Clan member / martial artist
*Chan Siu-gai as martial artist
*Fung Ming as Jinhongs guest / eatery waiter Lo Wai
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 