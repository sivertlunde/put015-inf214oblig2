Chatterbox (1936 film)
{{Infobox film
| name           = Chatterbox
| image          = 
| image_size     = 
| caption        = 
| director       = George Nichols, Jr.
| producer       = Robert Sisk (assoc. producer)
| writer         = Sam Mintz
| story          = 
| based on       =  
| narrator       =  Anne Shirley Phillips Holmes
| music          = 
| cinematography = 
| editing        = 
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Anne Shirley as a young woman who dreams of becoming an actress.

==Plot==
Jenny Yates (Anne Shirley) dreams of following in the profession of her deceased actor parents, but her grandfather, Uriah Lowell, with whom she lives, strongly disapproves. One day, the chatty young woman strikes up a conversation with struggling painter/actor Phil Greene, Jr. He is associated with a summer stock company trying out a revival of an old melodrama, Virtues Reward, in Jennys rural community before opening in New York City. Jenny is thrilled when he offers to give her a free ticket to that nights show, as her mother had acted in that very play.

When Uriah finds a playbill, he forbids her to go out and threatens to lock her out if she disobeys his orders. She mistakenly believes that live-in young hired hand Michael Arbuckle betrayed her, and promises him that she will tell her grandfather about his own misdeeds. When Uriah finds her gone, he carries through with his threat, but then changes his mind and unlocks the door. However, Michael locks it again after he leaves.

Meanwhile, producer Archer Fisher is dissatisfied with the performance of actress Lillian Temple in the role of a young innocent girl. She complains that she has not been paid for six weeks. As a result, Jenny is disappointed when she finds another play being performed that night. When she returns home, she finds the door locked, and assumes her grandfather has thrown her out. 

The company is leaving that night, so she hides in the rumble seat of Phils car. When he finds the stowaway, he does not have enough gas to take her home, so he reluctantly takes her to New York. They both rent rooms from "Tippie" Tipton.

Phil tries to discourage Jenny from pursuing her unrealistic dreams by sending her to see Archie, but his plan backfires when she is hired to replace Lillian. Worse, Jenny assumes the play is a serious production, whereas Phil knows that Archie intends to draw laughs from the audience with the hopelessly old-fashioned play. When Phil is unable to disillusion Jenny, Tippie offers to do so, but cannot either. In fact, Jenny takes her hints to mean that Phil is in love with her, but is unwilling to start a relationship until he is established as a painter.

Uriah comes to New York in search of his granddaughter. Through Phils father, he finally manages to contact Phil. They all attend the premiere. Part way through her performance, Jenny realizes the truth, but gamely carries on. After the performance, she decides to return home. She tells Phil she never wants to see him again, but when she finds him hiding in the trunk of Uriahs car, she changes her mind. 

==Cast== Anne Shirley as Jenny Yates
* Phillips Holmes as Philip "Phil" Greene, Jr. Edward Ellis as Uriah Lowell Erik Rhodes as Archie Fisher Margaret Hamilton as Emily "Tippie" Tipton
* Granville Bates as Philip Greene, Sr.
* Allen Vincent as Mr. Harrison
* Lucille Ball as Lillian Temple
* George Offerman, Jr. as Michael Arbuckle
* Maxine Jennings as Herself 

==External links==
* 
* 
* 

 
 
 
 
 
 
 