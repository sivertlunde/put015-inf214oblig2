The Entertainer (film)
 
 
{{Infobox film
| name           = The Entertainer
| image          = Entertainer75858.jpg
| image_size     =
| caption        =
| director       = Tony Richardson
| producer       = Harry Saltzman
| screenplay     = John Osborne Nigel Kneale
| based on       =  
| narrator       = Daniel Massey
| music          = John Addison
| cinematography = Oswald Morris
| editing        = Alan Osbiston
| studio         = Woodfall Film Productions
| distributor    = British Lion Films
| released       = 25 July 1960
| runtime        = 107 min., 37 sec. 
| country        = United Kingdom
| language       = English
| budget         =
}} stage play of the same name by John Osborne. It starred Laurence Olivier as a failing third-rate Music hall|music-hall stage performer who tries to keep his career going even as the music-hall tradition fades into history and his personal life falls apart.
 Academy Award Best Actor location in the Lancashire seaside town of Morecambe.

==Plot==
Jean Rice, a young London art teacher, travels to a seaside resort (not specified but partly filmed in Morecambe) to visit her family. She is emotionally confused, having had a row with her fiancé, who wants her to emigrate with him to Africa. She is also deeply concerned about the Suez Crisis, having seen her soldier brother off to the war. She has attended a peace rally in Trafalgar Square, directed against prime minister Anthony Eden.
 World War II heyday and is now drawing waning crowds, despite being in mid-season. The music-hall act of her father Archie Rice (Laurence Olivier|Olivier) plays out to a small number of increasingly uninterested spectators. Her family is deeply dysfunctional. Her beloved grandfather, once one of the leading stars of the music hall, lives in quiet retirement with his daughter-in-law and grandson.

Jean goes to the theatre where her father is playing. As well as being an undischarged bankrupt and a semi-alcoholic, he is desperately short of money and is hounded by creditors – the income-tax people as well as his unpaid cast. He is adored by his cynical son and watched with mild amusement by his father; but his relationship with Phoebe, his second wife, is strained. He is a womaniser and she is well aware of his tendencies, openly commenting on them to the rest of the family. She is often found drinking heavily.

With his latest show drawing to a close, Archie is desperate to secure a new show for the winter season. While acting as master of ceremonies at a Miss Great Britain beauty contest he charms the young woman who finished in second place. Soon he is conducting an affair with her. Her wealthy and ambitious parents want her to have an entertainment career and are willing to put up the money for Archies new show, if it includes her. They shake hands on the deal.

While this is going on, the radio reports that Archies son Mick has been captured by the Egyptians at Suez after a major firefight. Archie seems oblivious of the news and the distress of his family. He is fixated with his dream of restarting his stalled career and his affair. His daughter discovers the affair and tells her grandfather. He, acting out of what he believes are his sons best interests, goes to the girls parents and tells them that Archie is already married and a bankrupt. They swiftly break off all connections with him, ending their financing for his next show.

While he is still digesting this turn of events, news arrives that Archies son has been killed in Egypt. His body is returned and a civic commemoration is attended by the whole town. It is reported that he will be awarded a Victoria Cross for his actions. Archie is still too busy fixating on his career to notice how his family is falling apart at the news. His brother-in-law wants to help the family to relocate to Canada and help him run a hotel but Archie rebuffs him. Instead he persuades an impresario to promote a new show, with his father as the headline attraction. His father, despite his age is still extremely popular and there is a public demand for his return.

On the opening night his father collapses and dies, completing the estrangement of the family. His wife and son are determined to go to Canada, while Archie is set on staying in Britain, even if it means going to jail. The film ends with Archie making an apparently final performance to an apathetic audience.

==Cast==
* Laurence Olivier – Archie Rice
* Brenda De Banzie – Phoebe Rice
* Roger Livesey – Billy Rice
* Joan Plowright – Jean Rice
* Alan Bates – Frank Rice Daniel Massey – Graham
* Shirley Anne Field – Tina Lapford
* Thora Hird – Mrs Lapford
* Albert Finney – Mick Rice

==See also==
*Angry young men
*Kitchen sink realism
*The Entertainer (play)

==External links==
* 
* 

==References==
 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 