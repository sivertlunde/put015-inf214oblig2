Harry and the Butler
{{Infobox film
| name           = Harry og kammertjeneren
| image          = Harry and the Butler (1961).jpg
| caption        =  Bent Christensen
| producer       = Bent Christensen Preben Philipsen
| writer         = Bent Christensen Leif Panduro
| starring       = Osvald Helmuth Ebbe Rode Gunnar Lauring
| music          = Niels Rothenborg
| cinematography = Kjeld Arnholtz
| editing        = Kirsten Christensen Lene Møller
| distributor    = 
| released       = 8 September 1961
| runtime        = 105 minutes
| country        = Denmark
| language       = Danish
| budget         = 
| gross          = 
}}
 Bent Christensen. It was nominated for the Academy Award for Best Foreign Language Film.    It was also entered into the 1962 Cannes Film Festival.   

== Cast ==
*Osvald Helmuth as Gamle Harry
*Ebbe Rode as Fabricius
*Gunnar Lauring as Biskoppen
*Henning Moritzen as Fyrst Igor
*Lise Ringheim as Magdalene
*Lily Broberg as Trine, Café Dråbens værtinde
*Olaf Ussing as Krause, bilforhandler
*Palle Kirk as Heisenberg, 8 år
*Aage Fønss as Herskabstjener I
*Ejner Federspiel as Herskabstjener II
*Einar Reim (as Einer Reim) as Mink, overbetjent
*Ernst Schou as Advokat Lund
*Emil Halberg as Viggo, udsmider
*Johannes Krogsgaard as Teaterdirektøren
*Valsø Holm as Meyer, grønthandler

==See also==
 
* List of submissions to the 34th Academy Awards for Best Foreign Language Film
* List of Danish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 