Crazy Racer
{{Infobox film
| name           = Crazy Racer
| image          = Crazy Racer poster.jpg
| film name      = 
| caption        = 
| director       = Ning Hao
| producer       = 
| writer         = Ning Hao
| starring       = Huang Bo
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = China Film Group
| released       =  
| runtime        = 110 minutes
| country        = China
| language       = Mandarin
| budget         = 
| gross          = over Renminbi|¥100 million (US$15.9 million) 
}}
Crazy Racer, also known in some countries as Silver Medalist, is a 2009 Chinese black comedy movie directed and written by Ning Hao, filmed mostly in the southern coastal city of Xiamen, PROC.  

== Plot ==
The plot follows four seemingly separate stories that intersect and converge at points throughout the movie. It begins with the protagonist, Geng Hao losing first place in a cycling race and subsequently being tricked into sponsoring an energy drink containing illegal performance enhancing substances by corrupt businessman Li Fala which causes him to forfeit the winnings from his silver medal. Disgraced and outlawed from ever participating again in the sport, Geng Haos coach suffers from a heart attack, prompting Geng Hao to seek retribution from Li Fala, who he believes is the cause. In the process of obtaining the money for his coachs funeral, Geng Hao crosses the paths of local criminals, perpetually confused policemen and Taiwanese gangsters.  

==Cast==
*Huang Bo
*Jiu Kong
*Rong Xiang

==Reception==
The film garnered mostly positive reviews from the Chinese press although it has remained relatively unknown outside of mainland china.  

Perry Lam of Muse (Hong Kong magazine)|Muse praises Ning Haos direction: the movie leaps from scene to scene with such an athletic deftness and comic inevitability that the many unlikely curves and switches in the plot and the same setups feel almost like the machinery of fate. 

==References==
 


==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 
 