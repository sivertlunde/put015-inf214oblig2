The Rocker (film)
{{Infobox film
| name = The Rocker
| image = Rockerposter.jpg
| caption = Promotional poster
| director = Peter Cattaneo
| producer = Shawn Levy Tom McNulty
| screenplay = Maya Forbes Wallace Wolodarsky
| story = Ryan Jaffe
| writer =
| starring = Rainn Wilson Christina Applegate Teddy Geiger Josh Gad Emma Stone Jane Lynch
| music = Chad Fischer
| cinematography = Anthony B. Richmond
| editing = Charles Kaplan Brad E. Wilhite
| studio = 21 Laps Entertainment Fox Atomic
| distributor = 20th Century Fox
| released =  
| runtime = 102 minutes
| country = United States
| language = English
| budget = $15 million   
| gross = $8.8 million 
}}
 2008 American comedy film directed by Peter Cattaneo, starring Rainn Wilson, Christina Applegate, Teddy Geiger, Josh Gad, Emma Stone,  and Jane Lynch. The film was written by Maya Forbes & Wallace Wolodarsky, from a story by Ryan Jaffe.

==Plot== heavy metal/glam metal band from Cleveland, Ohio on the verge of making it big. The band is offered a recording contract with one condition - Fish must be replaced as drummer with the president of the record companys nephew. After initially refusing, the band caves when their manager tells them they would get to tour as the opening act for Whitesnake.

20 years later, Vesuvius remains an immensely successful band, while Fish is working a mundane office job, which he is fired from for attacking a co-worker who tried to force him to listen to Vesuvius new album. After being kicked out by his girlfriend, he is taken in by his sister Lisa (Jane Lynch) and her family. Matt (Josh Gad), Fishs high school-aged nephew, plays keyboards in an alternative rock band called A.D.D., along with his friends Curtis (Teddy Geiger) and Amelia (Emma Stone). The band is scheduled to play their schools prom but the gig is in jeopardy when their drummer is suspended from school. Matt convinces the others to allow Fish to fill in, but he ruins the gig when he launches into an impromptu drum solo. However, Fish is so excited by Curtis songs and the chance to play again, he convinces them to let him join the band if he can deliver another gig. After repeated failed attempts, he finally succeeds in securing a gig at a club in Terre Haute. Because the other members are all minors, they have to sneak out to the gig, taking Lisas car without permission, resulting in the band being arrested.
 stereotypical acts, despite the physical costs on his body, and he vandalizes a hotel room, which again results in the bands arrest.

After bailing them out, Kim, Curtiss mother (Christina Applegate), promises the other parents she will stay for the remainder of the tour, so their children will not be influenced by Fishs antics. 

The label asks A.D.D. to open a show for Vesuvius at their induction into the Rock and Roll Hall of Fame. Fish refuses to play the gig and quits. Curtis eventually convinces him to put aside his grudge against his former bandmates and play the show. Vesuvius actually does not remember Fish until one bandmember says he faintly remembers. Discovering that he is able to move on, Fish wishes Vesuvius a great show. Fish and the band perform to a standing ovation. After their gig, Amelia and Curtis, and Fish and Kim both begin a relationship. Vesuvius takes the stage, and during their set the lead singers microphone falls off the stand while the voice track of their song continues, then one line of the song starts incessantly sounding like a broken record, revealing that they have been lip-syncing. The audience condemns Vesuvius and chants an encore for A.D.D. Over Davids objections, the band dismisses him and performs for the crowd once again.

==Cast==
* Rainn Wilson as Robert “Fish” Fishman
* Christina Applegate as Kim Powell
* Josh Gad as Matt Gadman
* Teddy Geiger as Curtis Powell
* Emma Stone as Amelia Stone
* Will Arnett as Lex Drennan
* Fred Armisen as Wayne Kerr
* Howard Hesseman as Nev Gator
* Lonny Ross as Timmy Sticks
* Jason Sudeikis as David Marshall
* Bradley Cooper as Trash Grice
* Jon Glaser as Billy Ault
* Jane Lynch as Lisa Gadman
* Jeff Garlin as Stan Gadman
* Demetri Martin as Kip
* Aziz Ansari as Aziz
* Pete Best as The Guy at the Bus Stop
* Bubba Fontaine as himself
* Jane Krakowski as Carol (Fishs Ex Girlfriend)
* Samantha Weinstein as Violet

==Production==
 
The Rocker was mostly filmed in Toronto, Ontario, Canada, with some exterior shots filmed in Cleveland, Ohio. 

The songs attributed to the band A.D.D. were mostly written and produced by musician/producer Chad Fischer, of the band Lazlo Bane. Fischer also performed most of the music, with Teddy Geiger singing the lead vocals. The songs attributed to the band Vesuvius were written and performed by the members of Lazlo Bane, with lead vocals sung by Keith England.  

==Promotion==
To promote the movie, the song "Promised Land", as performed by the fictional band Vesuvius, was released as free downloadable content for the video game Rock Band, which is shown being played in the movie. 
 The Office.  According to his message,  he had "kidnapped the lovely Jenna, put her, bound, in the trunk of my Firebird and logged onto her MySpace to send out this bulletin....To free Americas sweetheart...you must attend my new movie, The Rocker, which opens August 20th....As soon as the film grosses 18.7 Mil, she will be released and given a peach smoothie." In the end the film was not even close to grossing that much money. A subsequent blog entry pointed readers to freejennanow.com, where videos were posted and features such as a "Free Jenna Game" and countdown tickers could be found.
 Gibson musical Paul Brannigan, as the winner.   Alburn dressed and posed as characters from the film in the Kerrang! photo shoot. Rainn Wilson recorded a video message for the winning band, which was played at the MySpace Black Curtain Screening.

A Flash game titled The Rocker: TV Toss was also released. In the game, the player controls Fish (in 1st person view). The objective is to do damage to the hotel room by tossing TVs at various objects. The high score is calculated by means of the cost of objects damaged.

==Reception==

===Critical response===
The film received mixed reviews from critics. On  , the film has a score of 53 out of 100, based on 28 critics, indicating "mixed or average reviews". 

===Box office===
The Rocker was released on August 20, 2008 in the USA. It was a bomb at the worldwide box office, opening outside the Top 10 at #12 with $2.64 million for its first weekend, grossing less than $7 million overall during its entire theatrical run.  One of the stars of the film, Emma Stone, had another film (The House Bunny) open the same weekend. It fared much better than The Rocker, opening at #2.

==Cultural references== Jon "Fish" Fishman is the real life drummer and namesake of the Vermont-based jam band Phish.
*Robert shouting "I am a golden fish" before falling off a roof in the film is a reference to Cameron Crowes Almost Famous in which a character says "I am a golden god" before jumping off a roof into a pool. Which in turn is based on Robert Plant of Led Zeppelin, declaring the same. (Incidentally, Rainn Wilson had a minor role in Almost Famous.)

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 