The Girl on the Train (2013 film)
{{Infobox film
| name           = The Girl on the Train
| director       = Larry Brand
| producer       = James Carpenter Rebecca Reynolds Gary Sales
| writer         = Larry Brand
| starring       = Henry Ian Cusick Nicki Aycox Stephen Lang
| cinematography = David Sperling
| editing        = Larry Brand Stephen Mark
| studio         = 8180 Films
| distributor    = Monterey Media (US)
| released       =   }}
| country        = United States
| language       = English
}}
The Girl on the Train is a 2013 American independent thriller film directed and written by Larry Brand, and produced by James Carpenter, Rebecca Reynolds, Gary Sales. The film stars Henry Ian Cusick, Nicki Aycox, Stephen Lang. 

== Plot ==
A documentary filmmaker boards a train at Grand Central Terminal, heading to upstate New York to interview the subjects of his latest project. A chance encounter with a mysterious young woman leads him on a journey of a very different sort, and within the blink of an eye, Hart is forced to leave his complacent life behind for a world in which the line between fantasy and reality is blurred. As Hart tells his strange story to a police detective he finds himself being questioned as Martin tries to discover whether Hart is the victim or the suspect in the strange affair.   

== Cast ==
* Henry Ian Cusick as Danny Hart
* Nicki Aycox as Lexi
* Stephen Lang as Det. Lloyd Martin
* Charles Aitken as Spider
* James Biberi as Cabbie

== Production ==
The Girl on The Train is directed and written by Larry Brand, and produced by James Carpenter, Rebecca Reynolds, and Gary Sales.  Reynolds and Carpenter, who founded 8180 Films, are married.  Ross Satterwhite was the executive producer. 

The Girl on The Train was shot in New York City from March to April 2012, over a total of 17 days.  Filming locations include offices in the Wall Street area, an abandoned Catholic school, and Grand Central Terminal.  

== Release ==
The Girl on the Train premiered at the Newport Film Festival.   In November 2013, Monterey Media bought the United States distribution rights to the film, and they will release the film in the United States in 2014. 

=== Festivals ===
The Girl on the Train was selected to screen at the following film festivals:
* 2013 Napa Valley Film Festival – Winner Best Screenplay – Larry Brand 
* 2013 Big Bear Lake International Film Festival 
* 2013 Newport Beach Film Festival 
* 2013 Oaxaca Film Festival 
* 2013 San Diego Jewish Film Festival 
* 2013 Traverse City Film Festival 
*2014 Sedona International Film Festival    

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 