Murder 3
 
 
{{Infobox film
| name               = Murder 3
| image              = Murder 3.jpg
| caption            = First look poster
| director           = Vishesh Bhatt
| producer       = Mahesh Bhatt  (Presenter)  Mukesh Bhatt
| writer             = Mahesh Bhatt, Amit Masurkar (additional screenplay)
| based on           =  
| starring           = Randeep Hooda Aditi Rao Hydari Sara Loren Roxen
| cinematography     = Sunil Patel
| Marketng Designer  = Pankaj Jaiswal
| editing            = Devendra Murdeshwar
| distributor        = Fox Star Studios
| released           =  
| runtime            = 135 min
| country            = India
| language           = Hindi
| budget             =  
| gross              =  
}} Indian psychological Murder film The Hidden Face. Murder 3 was released on 15 February 2013 to mixed reviews. The film performed moderately well at the box office. 

==Story==
The film opens with Vikram (Randeep Hooda), a hot-shot fashion and wildlife photographer, viewing a video of his girlfriend, Roshni (Aditi Rao Hydari) telling him she is leaving him. Vikram becomes distraught. While drinking away his sorrows at a bar, he meets Nisha (Sara Loren) and they have a relationship where Nisha moves into the house that Vikram was sharing with Roshni. Vikram becomes a suspect in the disappearance of Roshni, however, the investigators can find no evidence of Vikrams involvement in Roshnis disappearance.

It is revealed that the house is owned by a German lady who shows Roshni a secret room built to hide her husband just in case someone came to look for him because he was in British Army at the time of 1947. The room is self-contained 

In a flashback, it is also shown that Roshni, jealous of Vikrams relationship with one of his colleague, decided to pretend she is leaving him. She creates the video saying she is leaving as she hides in the secret room. The room has some one way mirrors where she can observe Vikrams reaction. When she decides he has had enough she looks for the key and realizes she lost the key and is now trapped in the room with no way to contact him.

Nisha finds the key to the secret room, but she doesnt know what it is used for. Nisha eventually figures out that Roshni is trapped in the house because Roshni is able to communicate through tapping on the pipes in the secret room. As Nisha is ready to open the door, she pauses and decides not to rescue Roshni because she might lose Vikram after that. Nisha struggles with her decision, but decides to open the door and check on Roshni because the investigators gave Nisha pictures of Vikram and his Colleague and she herself feels the pangs of jealousy. As Nisha is checking on Roshni laying in a bed in the secret room, Roshni surprises Nisha and knocks her out with a glass bottle and leaves Nisha locked in the room. After Roshni comes out of the secret room, she sees Vikram and his colleagues photographs and shatters. She decides to leave the house. She leaves that picture of Vikram with his colleague on mirror and also sticks her necklace to show Vikram that she is out and then she sends the keys of the room to the investigator who actually loves Nisha. The investigator comes and arrests Vikram. The final scene shows Roshni tearing her and Vikrams photograph and is going on a highway road in a car alone, happy on leaving the devastated and arrested Vikram.

==Cast==
*Randeep Hooda as Vikram   
*Aditi Rao Hydari as Roshni   
*Sara Loren  as Nisha 
*Rajesh Shringarpure as Kabir (Police Officer). 

==Casting==
The makers of the film, who also worked on Murder (film)|Murder and Murder 2 decided that Emraan Hashmi would not be returning for the third movie and that Randeep Hooda would play the leading role.   Names like Asin Thottumkal and Esha Gupta were announced for the female lead but, Asin was not happy with script then the role went to Esha Gupta. After initial talks with Esha Gupta fell through, Aditi Rao Hydari was finally cast as the female lead. Pakistani actress Sara Loren was then chosen as the second female lead, as the film is a one hero, two heroine project.   

==Production and development==
Shooting of the film has happened in Cape Town, South Africa and Goa, India.
 Arth in the thriller genre which released in 1983. Then it was the institution of marriage that was deconstructed and 30 years later Murder 3 deconstructs love and questions its very existence in todays world of relationship crimes. Now that we have the official rights of the film (The Hidden Face), we look to doing it justice and taking the Murder franchise forward." 

==Critical reception==
Murder 3 received mixed reviews from critics.
Taran Adarsh of Bollywood Hungama gave it 4 out of 5 and stated that Murder 3 "is one of the finest thrillers to come out of Vishesh Films. An outstanding story narrated with ferocious enthusiasm. Vishesh Bhatt hits a boundary in his very first attempt!"  Madhureeta Mukherjee of The Times Of India gave it 3 out of 5, while commenting This ones no bloody Valentine, but watch it if you like it twisted. Komal Nahta wrote, On the whole, Murder 3 is a fair entertainer.Nabanita of OneIndia also gave it 3 out of 5 while commenting that it has thrills but it fails to captivate. Roshni Devi of Koimoi gave it 2.5 out of 5, saying that it is worth a watch. Saibal Chatterjee of NDTV gave it 2.5/5 stars and stated Murder 3, a thriller that vacillates between the taut to the toxic, packs enough punch and panache to keep the audience glued, if not sweep them off their feet. It tweaks the formula just a tad – it goes somewhat easy on the erotic component of the Murder franchise, opts for a markedly stronger emotional spine, and gives the female characters more than usual space.

Anupama Chopra gave it 2/5 while writing that With a little more imagination, Murder 3 could have been deliciously dark. Sukanya Verma for Rediff.com has given 2/5 stars and says Murder 3 has some badly-acted thrills.    

==Soundtrack==
{{Infobox album |  
 Name = Murder 3
| Type = Soundtrack Roxen
| Lyricists = Sayeed Qadri
| Cover = 
| Released = 2 Feb 2013
| Genre = Film soundtrack
| Length = 32:01
| Label = Sony Music India
| Producer = Vishesh Films
| Last album = Race 2 (2012)
| This album = Murder 3 (2013)
| Next album = I Love New Year (2013)
}}
The soundtrack of the film is composed by Pritam, Anupam Amod & Roxen (band). The lyrics are penned by Sayeed Quadri. The soundtrack was released on 2 Feb 2013. The songs were immensely popular specially the songs "Teri Jhuki Nazar" and "Hum Jee Lenge" was immensely popular among the masses while the other tracks like "Mat Azma Re" and "Jaata Hai Tujh Tak" were also very popular gaining good critical review and mass appeal. The music focused on the Rock Genre and was unique in the Murder brand. 

===Tracklist===
The soundtrack consists of 7 tracks which are composed by Pritam, Anupam Amod & Roxen Band.

{| class="wikitable"
|-
! Track No !! Song !! Singer(s) !! Composer
|-
| 1 || Teri Jhuki Nazar || Shafqat Amanat Ali Khan || Pritam
|- KK || Pritam
|-
| 3 || Jaata Hai Tujh Tak || Nikhil DSouza || Anupam Amod & Pritam 
|-
| 4 || Hum Jee Lenge || Mustafa Zahid || Roxen (Band)
|-
| 5 || Teri Jhuki Nazar (Film Version) || Shafqat Amanat Ali Khan || Pritam
|-
| 6 || Jaata Hai Tujh Tak (Film Version) || Nikhil DSouza || Pritam
|-
| 7 || Hum Jee Lenge (Rock Version) || Mustafa Zahid || Roxen (Band)
|}
The soundtrack received positive critical response. Planet Bollywood gave it 7.5/10 calling it a great album. Joginder Tuteja of Bollywood Hungama gave it 3.5/5 while Koimoi gave it 3/5. Music was very successful upon release.

==Box Office==
The film  had a dull opening of around   and grossed   in the first weekend.  The film had a first week of    and it made   in India in 10 days and was termed mediocre.

It eventually progressed to   in India.
The film was poor overseas grossing  .

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 