Ah Boys to Men 2
 
{{Infobox film
| name           = Ah Boys to Men 2
| image          = Ahboystomenposter2.jpg
| border         = Yes
| alt            =
| caption        = Theatrical release poster
| director       = Jack Neo
| producer       = Jack Neo Lim Teck Leonard Lai
| writer         = Jack Neo Link Sng
| starring       = Joshua Tan Wang Wei Liang Noah Yap Maxi Lim Richard Low Irene Ang
| music          = Tosh Zhang
| cinematography =
| editing        = Yim Mun Chong
| studio         = J Team mm2 Entertainment Clover Films Grand Brilliance K Kopter K&L Entertainment Neo-Film Sky Films StarHub Vividthree Production
| distributor    = Golden Village Pictures Clover Films
| released       =    (Singapore)  
   (Malaysia) 
| runtime        = 113 minutes
| country        = Singapore
| language       = English Mandarin Hokkien Malay
| budget         = Singapore dollar|S$3 million ($2.45 million; shared with part two)   
| gross          = S$7,900,000 (US$6,230,000)   
}}

Ah Boys to Men 2 ( ) is a 2013 Singaporean comedy film  produced and directed by   was announced by   is scheduled to open in cinemas on February 19, 2015.

==Plot==
Ken is quick to adopt a change in personality by becoming an "on-the-ball" recruit, even more so than "Wayang King" Aloysius. Differing viewpoints sour the friendship between Ken and Lobang. Meanwhile, Kens father has become partially paralyzed because of his stroke but is determined to make a recovery. After booking out, Aloysius seeks advice from his parents as he feels excluded from the group; his father (Chen Tianwen) tells him the best solution is not to do anything. Back at Tekong, Recruit IP Man learns about "Real Bullet" Zhen Zidan (Benjamin Mok), an "Ah Beng" who stole his girlfriend Mayoki (Sherraine Low). IP Man hits back by criticizing Mayoki for her inferior qualities. "Real Bullet" in turn uploads a video of him and Mayoki engaging in an initimate act. Crestfallen, IP Man seeks help from his bunk mates; Lobang plots a scheme to ambush "Real Bullet" and Mayoki at a car-park by throwing heaps of human excretion mixed with chilli and wasabi at them while they are in the car. However, a gang soon chases after them after "Real Bullet" calls for back-up, but at last the recruits manage to shake the gang off; they celebrate at a restaurant at White Sands later that evening. However, at the restaurant, they are ambushed by more gang members, who have managed to track them down. Ken runs back to save his mates, per the principle "Leave no man behind", unlike Aloyisus, who is quick to flee. Because Ken saved his life, Lobang vows to stop smoking in return.
 magazine but after a comical turn of events, it is Lobang who loses his magazine. However, Aloysius quickly gives his magazine to Lobang during an equipment check and takes the blame. While searching for the missing magazine, the relationship between the two grow better.

The eccentric Lieutenant S.T. Choong (mrbrown) is chased by a large herd of wild boars halfway into an assessment for leadership potential; the soldiers are quick to come to his rescue. Following their passing out parade, the recruits reunite with their kins. Kens father is present and comes to greet Ken; he is finally able to walk steadily without any aid. As the film ends, various recruits postings are shown — with Lobang and Ken being posted to Officer Cadet School (OCS).

==Cast==
 , who played a minor role as an army lieutenant in Ah Boys to Men 2|alt=Refer to caption]]
* Joshua Tan as Ken Chow
* Maxi Lim as Aloysius Jin Pseudonym|a.k.a. "Wayang King"
* Wang Wei Liang as Bang "Lobang" Lee Onn
* Noah Yap as Man In Ping a.k.a. IP Man (a parody of Yip Man)
* Ridhwan Azman as Ismail Mohammed
* Aizuddin Nasser as Muthu Shanmugaratnam
* Richard Low as Kens father
* Irene Ang as Mary Chow, Kens mother
* Wang Lei as Kens uncle
* Tony Koh Beng Hoe as Kens second uncle
* Yoo Ah Min as Kens grandmother
* Tosh Zhang as Sergeant Alex Ong
* Lee Kin Mun, better known as mrbrown,     as Lieutenant S T Choong:   
:An army officer. It is his first film role.    For the role, he was made to go through "physical torture".  At a press conference for part two, Lee said: "It was three days of physical torture. I ran about more during the filming than in my entire army life!"    Neo has said that there may be more future collaborations between Lee and himself. 
* Luke Lee  as Sergeant Jed Heng,    
*Fish Chaar as Officer Commanding,  
*Alvin Richard as Sergeant Kenneth Pang   
*Ryan Chioh, Douglas Foo, Lawrence Kim, Jaz Lai and Bennett Neo as army reservists    
*Chen Tianwen as Aloysius father,  
*Benjamin Mok as gangster Zhen Zi Dan (literally "Real Bullet", loosely a parody of Donnie Yen)
*Bernie Utchenik as a medical doctor,  Chan Ka Wai as 1970s Army "Deception|Geng King" (appears only in flashback)
*Ye Li Mei as Aloysius mother,  
*Justin Dominic Mission as a 1970s army sergeant,  
*Qiu Qiu as Ken Chows teenage girlfriend Amy,   
*Sherraine Low as IP Mans girlfriend Mayoki 
*Wilson Ng as Sergeant Major  
*Jack Neo as himself (footage on set).

==Themes==
Just like the first part, the main theme of the film is Conscription in Singapore, a popular topic amongst Singaporeans. 

Ah Boys to Men 2 focuses more on the unity of the protagonists,    as well as tapping more on hot social topics like foreign talent in Singapore.    It gave "a stronger story than its predecessor",    and had a "more meaty" drama aspect,    according to Jack Neo. Other themes for part two include "  sacrifice, love, family and patriotism". 

==Production==

===Music===

====Theme song====
The official theme song of Ah Boys to Men 2, titled "Brothers", was written, composed and performed by Tosh Rock Zhang, a YouTube personality who is part of the cast in the film. An official music video was uploaded on YouTube on January 24, 2013.

==Release== trailer for part two was showcased at the Asia TV Forum & Market and ScreenSingapore 2012 from December 4, 2012 to December 7, 2012.    Within a week of its upload online, the trailer had already amassed approximately 20 million views. 

Earlier reports gave the release date for the full film as January 31, 2013  and February 7, 2013.     However, the official commercial release date for part two in Singaporean cinemas was later confirmed as February 1, 2013.  

Ah Boys to Men 2 premiered on January 30, 2013, two days before its commercial release, at the Festive Grand Theatre at Resorts World Sentosa – the first local film to do so.  It was released in Malaysian cinemas on March 14, 2013.  

==Reception==

===Critical response=== The Business Time Out Singapore reviewer commented that the film was "wrapped up in a heart-warming message".  Channel News Asia called it an "enjoyable film with problems", giving a score of 3 out of 5 stars and citing the heavy product placement and over-emphasis on details as some of the situations that needed improvement. 

===Box office===

====Domestic====
Neo has anticipated that the second part would "do much better than the first one" and has also announced his consideration to produce more army-themed films.    At a press conference for part two of Ah Boys to Men, Neo said: "I anticipate that Part 2 will do much better than the first one and I hope that we will hit S$7 million this time." 

Part two out-grossed part one in terms of opening weekend earnings; it earned S$1.51 million in its opening weekend, thus breaking the record for the highest box office earnings for local productions, which was previously set by part one in 2012.  It took in S$2.7 million over the Chinese New Year weekend period;        on February 20, 2013, part two of Ah Boys to Men overtook part one in box office takings by grossing S$6.297 million and becoming the highest-grossing Singaporean film.    As of February 26, 2013, part two has grossed S$7.08 million.  The films overwhelming success has been seen as a "gift from Heaven"  by Neo. 

==Third installment==
The project had originally been envisaged as just two parts until after the release of the second part. Buoyed by "non-stop" requests for a threequel,  Jack Neo confirmed on February 20, 2013 that he had begun working on one under the working title Ah Boys to Men 3,  though actual filming would only take place after 2013.     Neo posted on his Twitter account (in Chinese): 

 
 Mark Lee and that " e are currently preparing for the shoot".    During which he also "officially announced":   

 

A spin-off to the Ah Boys to Men movie franchise titled   was announced by Jack Neo to be scheduled for production in August 2014 based on the story of a group of navy boys. 

==See also==
* List of Singaporean films of 2012
* List of Singaporean films of 2013
* Army Daze

 

==References==
 

==External links==
 
{{ external media
| align  = right
| width  = 210px
| image1 =   }}
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 