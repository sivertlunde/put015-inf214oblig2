Munna (film)

{{Infobox film
| name        = Munna
| image       = Munna_film_poster.jpg
| writer      = Vamsi Paidipally Koratala Siva BVS Ravi
| starring    = Prabhas Raju Uppalapati Ileana DCruz Prakash Raj Kota Srinivasa Rao
| director    = Vamsi Paidipally
| producer    = Dil Raju
| studio      = Sri Venkateswara Creations
| distributor = Sri Venkateswara Creations
| cinematography = C. Ram Prasad
| released    =  
| runtime     =
| editing     = Marthand K. Venkatesh
| language    = Telugu
| music       = Harris Jayaraj
| awards      =
| country     = India
| budget      = 
}} Tollywood action Tamil as Vetri Thirumagan. Later this film was remade in Bangladesh named Amar Challenge starring Shakib Khan and Shahara.

==Plot==
Munna (Prabhas) is a college student. His aim is to finish off mafia don Kakha (Prakash Raj) who keeps the entire city under his grip. Munna has a personal vengeance to take on Kakha. An honest politician Srinivasa Rao (Kota Srinivasa Rao) relentlessly works to expose Kakha but is killed. Meanwhile, Munnas classmate Nidhi (Ileana DCruz) falls in love with him. The twist in the tale is that Kakha is none other than his estranged father who tries to sell off his mother and was responsible for the death of his mother and his young sister. Another don in the city named Aatma (Rahul Dev), who is the opposition of Kakha asks Munna to join him but Munna refuses his proposal. Munna starts to beat Kakha and the mind game begins. Kakhas second wife, son and daughter leave him and join Munna. After some incidents including the death of his best friend, Munna finds out that Aatma was working for Kakha all along. The two of them plan on killing him in a factory when all of a sudden Aatma turns against Kakha and Munna. Kakha kills Aatma for the betrayal. Having lost everything Kakha worked for in his life, he commits suicide. Thereby, Munna succeeds in taking revenge against Kakha.

==Cast==
  was selected as lead heroine marking her first collaboration with Prabhas.]]
* Prabhas as Munna
* Ileana DCruz as Nidhi
* Prakash Raj as Kamalakar/Kaka
* Kota Srinivasa Rao as Shrinivasa Rao
* Rahul Dev as  Aatma
* Sukanya as Kakas wife Kalyani as Munnas mother
* Posani Krishna Murali as Kishan
* Brahmaji as Purushottam
* Tanikella Bharani as Qasim Surya as Dr. Niranjan Chowdary
* Sudha as Nidhis mother
* Sridhar as Prakash
* Medha Bahri as Shruti
* Sandra as Nidhis friend Venu Madhav as Mohan
* Nalla Venu as Tillu
* Raghu Babu as Satti Pandu
* Chalapathi Rao
* Sameer as police inspector
* Uttej as gun seller
* Gundu Sudharshan as priest
* Shriya Saran in item number

==Release==
The film released in 750 screens worldwide on 27 April 2007.

===Critical reception===
Idle brain rated 3/5 stated The movie starts off on a dull note and meanders aimlessly till the dark secret is revealed before interval. The flashback episode is unappealing. There are a couple of surprises in the second half. The plus points of the film are Prabhas, stylish taking of the director and lavish budget. On the flip side, screenplay and narration should have been better. There is no holistic perspective in the film. Let us wait and see if Dil Rajus Midas Touch works for this film or not!  Fullhyd states Munna is perhaps worth a watch for those seeking a testosterone high. Its fate depends less on its own merit and more on the kind of competing attractions at the box office in future weeks.  CineGoer.com rated 2.75/5 stated The movie has a lot of style, but lacks the necessary soul and substance. Swanky visuals, crisp narration, a demigod like hero and two major twists fail to elevate the movies average viewing status.   Telugu Cinema rates 2.5/5 stated There is not much entertainment in the film. The romance between Prabhas and Illeana was not properly developed. Too much of action is the undoing factor for this film. 

==Music==
{{Infobox album |  
  Name        = Munna
|  Type        = soundtrack
|  Artist      = Harris Jayaraj
|  Cover       = Munna_CD_Cover.jpg
|  Released    = 28 March 2007 (India)
|  Recorded    =  Feature film soundtrack
|  Length      = 
|  Label       = Supreme Music
|  Producer    = Harris Jayaraj
|  Reviews     = 
|  Last album  = Unnale Unnale (2007)
|  This album  = Munna (2007)
|  Next album  = Bheemaa (2008)
}}
 Filmfare Best Music Director in 2007. {{cite web| title= cinegoer.com| work= Catchy Numbers, Good Vocals|url= http://www.cinegoer.com/areviews/munna.htm
|accessdate=29 March 2007}}  {{cite web| title= idlebrain.com| work= Munna – Catchy but commonplace |url= http://www.idlebrain.com/audio/areviews/munna.html
|accessdate=4 April 2007}} 

==Track listing==
{| class="wikitable" style="width:50%;"
|-
! Song title !! Singers  
|-
| "Baga Baga" || Shankar Mahadevan
|-
| "Chammakkuro" || Karthik (singer)|Karthik, Anushka Manchanda, Blaaze
|-
| "Kadhulu Kadhulu" || KK (singer)|KK, Vishwa
|-
| "Koncham Koncha" || Kailash Kher, Sujatha Mohan
|- Krish
|-
| "Vastaava Vastaava" || KK, Benny Dayal, Pop Shalini, Ananth, Sriram Parthasarathy
|}

==Awards==
Sadhana Sargam won the Filmfare Award for Best Female Playback Singer – Telugu for the song "Manasa".

==References==
 

==External links==
*  
*   on  
*   at BharatMovies.com
 
 
 
 
 
 
 
 