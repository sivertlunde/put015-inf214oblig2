Tkaronto
{{Infobox Film
| name           = Tkaronto
| image          =
| caption        =
| director       = Shane Belcourt
| producer       = Shane Belcourt Duane Murray Jordan OConnor Michael Corbiere
| writer         = Shane Belcourt
| starring       = Duane Murray Melanie McLaren Lorne Cardinal
| music          = Jordan OConnor
| cinematography = Shane Belcourt
| editing        = Jordan OConnor
| distributor    = Kinosmith
| released       = 2007
| runtime        = 81 mins
| country        = Canada
| language       = English
}}

Tkaronto is a  , August 14, 2008.  
 Mohawk word from which the city of Toronto derives its name, stars Duane Murray and Melanie McLaren as Ray Morrin and Jolene Peltier, who meet while in Toronto on business. Ray, a Métis people (Canada)|Métis cartoonist from Vancouver, is in town to pitch an animated series called Indian Jones to a television network, and Jolene, an Anishinaabe artist from Los Angeles, is in town to paint a portrait of Max (Lorne Cardinal), a local aboriginal elder.

The films cast also includes Cheri Maracle, Jeff Geddis, Mike McPhaden, Rae Ellen Bodie, Jonah Allison, Abby Zotz and Tricia Williams.

==Themes==
Ray and Jolene experience a common struggle: as creative professionals living in big cities, they share a sense of disconnection from their aboriginal heritage. Both struggle with the question of how to live as an aboriginal person in an urban setting devoid of many of the stereotypical signifiers of aboriginal identity. Neither was raised with the language, religion and customs of their ancestors; Jolene doesnt even know how to pray. Both are also in relationships with non-aboriginal partners: Ray, whose girlfriend is pregnant, is ambivalent about becoming a father, while Jolenes husband is dismissive of her search for a deeper sense of her heritage. 

As their attraction to each other grows, Ray and Jolene are forced to confront the choice of whether to throw away their current lives in order to be together.
 Christi and a friend of hers, Inuit rock singer Lucie Idlout, about how they balanced their own aboriginal identities with their urban lifestyles. 

==Production==
Produced by The Breath Films in conjunction with Braincloud Films, the film was shot in just 19 days and completed on a budget of just $20,000. 

==Reviews==
The film was reviewed by Jason Anderson at Eye Weekly, who wrote "The quality of writer-director Shane Belcourts feature debut – named after our citys original Mohawk name – is all the more remarkable when you consider that it was made in six months on a measly budget of $20,000. Based on Belcourts experience as the son of a Métis father, the movie portrays the crises of Jolene and Ray (Duane Murray), two thirty-somethings who cant figure out a way to square up their urban lifestyles and material ambitions with what an elder (played by Lorne Cardinal) calls “blood memory.” But for all of Tkarontos heavy themes, the film has a sense of lightness that makes it one of the years most appealing local indie features." 

==References==
 

==External links==
*  
*  

 
 
 
 
 