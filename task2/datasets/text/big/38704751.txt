Gagak Item
 
 
 
{{Infobox film
| name           = Gagak Item
| image          = Gagak Item ad 2.jpg 300px
| image_size     = 275
| border         = 
| alt            = A black-and-white advertisement; at the left side is a small picture
| caption        = Newspaper advertisement
| director       = {{plain list| Joshua Wong Othniel Wong
}}
| producer       =Tan Khoen Yauw
| screenplay     = Saeroen
| narrator       = 
| starring       ={{plain list|
*Rd Mochtar
*Roekiah
*Eddy T. Effendi
}}
| music          = H. Dumas
| cinematography = Wong brothers
| editing        = 
| studio         = Tans Film
| distributor    = 
| released       =  
| runtime        = 
| country        = Dutch East Indies Vernacular Malay
| budget         = 
| gross          = 
}} Vernacular Malay Joshua and Othniel Wong for Tans Film. Starring Rd Mochtar, Roekiah, and Eddy T. Effendi, it follows a masked man known only as "Gagak Item" ("Black Raven"). The black-and-white film, which featured the cast and crew from the 1937 hit Terang Boelan (Full Moon), was a commercial success and received positive reviews upon release. It is likely lost film|lost.

==Production== Joshua and sound editing. screen couple. 
 Portuguese influences.  Gagak Item featured vocals by kroncong singer Annie Landouw. 

==Release and reception==
  and Roekiah in a promotional still]] eastern Java. domestic productions released in 1939; the film industry had undergone a significant downturn following the onset of the Great Depression, during which time cinemas mainly showed Hollywood productions, and had only begun recovering following the release of Terang Boelan. 

Gagak Item was a commercial and critical success, although not as much as Tans earlier production.  An anonymous review in Bataviaasch Nieuwsblad praised the film, especially its music. The reviewer opined that the film would be a great success and that the film industry in the Indies was showing promising developments.  Another review in the same paper found that, although "one may shake ones head over the cultural value of indigenous films",  the film was a step forward for the industry. The review praised Roekiahs "demure"  acting. 

Following the success of Gagak Item the Wongs, Saeroen, Roekiah, and Mochtar continued to work with Tans Film. Their next production,   in early 1942. 

Gagak Item was screened as late as January 1951.  The film is likely lost film|lost. Movies in the Indies were recorded on highly flammable nitrate film, and after a fire destroyed much of Produksi Film Negaras warehouse in 1952, old films shot on nitrate were deliberately destroyed.  The American visual anthropologist Karl G. Heider writes that all Indonesian films from before 1950 are lost.  However, JB Kristantos Katalog Film Indonesia (Indonesian Film Catalogue) records several as having survived at Sinematek Indonesias archives, and film historian Misbach Yusa Biran writes that several Japanese propaganda films have survived at the Netherlands Government Information Service. 

==Explanatory notes==
 

==References==
 

==Works cited==
 
*{{cite news
 |title=Agenda: Vrijdag 26 Januari, Bogor
 |trans_title=Agenda: Friday 26 January, Bogor
 |language=Dutch
 |url=http://kranten.delpher.nl/nl/view/index/query/%22Gagak%20Item%22/coll/ddd/image/ddd:010474895:mpeg21:a0073/page/1/maxperpage/10/facets//facets%5Btype%5D/artikel/sortfield/datedesc#info
 |work=De Nieuwsgier
 |location=Jakarta
 |page=6
 |date=26 January 1950
 |accessdate=5 December 2013
 |ref= 
}}
* {{cite book
  | title =  
  | trans_title = History of Film 1900–1950: Making Films in Java
  | language = Indonesian
  | last = Biran
  | first = Misbach Yusa
  | authorlink = Misbach Yusa Biran
  | location = Jakarta
  | publisher = Komunitas Bamboo working with the Jakarta Art Council
  | year = 2009
  | isbn = 978-979-3731-58-2
  | ref = harv
  }}
*{{cite book
 |title=Indonesia dalam Arus Sejarah: Masa Pergerakan Kebangsaan
 |trans_title=Indonesia in the Flow of Time: The Nationalist Movement
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |chapter=Film di Masa Kolonial
 |trans_chapter=Film in the Colonial Period
 |author-link=Misbach Yusa Biran
 |publisher=Ministry of Education and Culture
 |year=2012
 |volume=V
 |pages=268–93
 |isbn=978-979-9226-97-6
 |ref=harv
}}
*{{cite news
 |title=Filmaankondiging Cinema Palace: Gagak Item
 |trans_title=Film Review, Cinema Palace: Gagak Item
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011123019%3Ampeg21%3Ap012%3Aa0235
 |work=Bataviaasch Nieuwsblad
 |location=Batavia
 |page=12
 |date=21 December 1939
 |publisher=Kolff & Co.
 |accessdate=4 March 2013
 |ref= 
}}
*{{cite news
 |title=Filmniews. Sampoerna: De zwarte raaf
 |trans_title=Film News. Sampoerna: De Zwarte Raaf
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A010285888%3Ampeg21%3Ap002%3Aa0063
 |work=De Indische Courant
 |location=Surabaya
 |page=12
 |date=27 November 1939
 |accessdate=14 May 2013
 |ref= 
}}
*{{cite news
 |title=Gagak Item
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011123023%3Ampeg21%3Ap003%3Aa0061
 |work=Bataviaasch Nieuwsblad
 |location=Batavia
 |page=3
 |date=19 December 1939
 |publisher=Kolff & Co.
 |accessdate=4 March 2013
 |ref= 
}}
* {{cite web
  | title = Gagak Item
  | url = http://filmindonesia.or.id/movie/title/lf-g009-39-831646_gagak-item/credit
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 4 March 2013
  | archiveurl =http://www.webcitation.org/6ErSEzrWF
  | archivedate = 4 March 2013
  | ref =  
  }}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
*{{cite news
 |title=(untitled)
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011122978%3Ampeg21%3Ap006%3Aa0119
 |work=Bataviaasch Nieuwsblad
 |location=Batavia
 |page=6
 |date=16 November 1939
 |publisher=Kolff & Co.
 |accessdate=14 May 2013
 |ref= 
}}
*{{cite news
 |title=(untitled)
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A010383047%3Ampeg21%3Ap008%3Aa0065
 |work=De Sumatra Post
 |location=Medan
 |page=8
 |date=11 November 1939
 |publisher=J. Hallermann
 |accessdate=14 May 2013
 |ref= 
}}
*{{cite news
 |title=(untitled)
 |language=Dutch
 |url=http://kranten.kb.nl/view/article/id/ddd%3A010227091%3Ampeg21%3Ap010%3Aa0065
 |work=Het Nieuws van den dag voor Nederlandsch-Indië
 |location=Batavia
 |page=10
 |date=8 May 1939
 |publisher=Kolff & Co.
 |accessdate=14 May 2013
 |ref= 
}}
 

==External links==
* 
 

 
 
 
 
 