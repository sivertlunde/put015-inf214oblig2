Dances with Wolves
 
{{Infobox film
| name           = Dances with Wolves
| image          = Dances with Wolves poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Kevin Costner Jim Wilson Michael Blake
| based on       =  
| narrator       =   Graham Greene Rodney A. Grant John Barry
| cinematography = Dean Semler
| editing        = Neil Travis
| studio         = Tig Productions
| distributor    = Orion Pictures
| released       =   
| runtime        = 180 minutes 236 minutes  
| country        = United States Lakota Pawnee Pawnee
| budget         = $22 million
| gross          = $424,208,848
}} epic Western Western war Michael Blake Lakota Indians. "Dances with Wolves: Overview" (plot/stars/gross, related films), allmovie, 2007, webpage:  .  -->
 Best Picture Lakota with English subtitles. It was shot in South Dakota and Wyoming, and translated by Albert White Hat, the chair of the Lakota Studies Department at Sinte Gleska University.
 Western genre of filmmaking in Hollywood. In 2007, Dances with Wolves was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".   of films inducted into the National Film Registry 

==Plot== First Lieutenant Confederate front western frontier Indian tribes, he elects to stay and man the post himself. He begins rebuilding and restocking the fort and prefers the solitude afforded him, recording many of his observations in his diary.
  Pawnee Indians on his way back to Fort Hays. Timmons death and the suicide of Major Fambrough, who had sent them there, prevents other soldiers from knowing of Dunbars assignment to the post, effectively isolating him. Dunbar notes in his diary how strange it is that no other soldiers join him at the post.
 Pawnee tribe when she was young. Dunbar returns her to the Sioux to be treated, which changes their attitude toward him. Eventually, Dunbar establishes a rapport with Kicking Bird and warrior Wind In His Hair who equally wish to communicate. Initially the language barrier frustrates them, so Stands With A Fist, though with difficulty remembering her English language|English, acts as translator.
 buffalo and participates in the hunt. When at Fort Sedgwick, Dunbar also befriends a wolf he dubs "Two Socks" for its white forepaws. When the Sioux observe Dunbar and Two Socks chasing each other, they give him the name "Dances with Wolves". During this time, Dunbar also forges a romantic relationship with Stands with a Fist and helps defend the village from an attack by the rival Pawnee tribe. Dunbar eventually wins Kicking Birds approval to marry Stands with a Fist, and abandons Fort Sedgwick.

Because of the growing Pawnee and white threat, Chief Ten Bears decides to move the tribe to its winter camp. Dunbar decides to accompany them but must first retrieve his diary from Fort Sedgwick as he realises that it would provide the army with the means of finding the tribe. However, when he arrives he finds the fort re-occupied by the Union army|U.S. Army. Because of his Sioux clothing, the soldiers open fire, killing Cisco and capturing Dunbar, arresting him as a traitor. Senior officers interrogate him, but Dunbar cannot prove his story, as a corporal has found and discarded his diary. Having refused to serve as an interpreter to the tribes, Dunbar is charged with desertion and transported back east as a prisoner. Soldiers of the escort shoot Two Socks when the wolf attempts to follow Dunbar, despite Dunbars attempts to intervene.

Eventually, the Sioux track the convoy, killing the soldiers and freeing Dunbar. At the winter camp, Dunbar decides to leave with Stands With A Fist, since his continuing presence will put the tribe in danger. As they leave, Wind In His Hair shouts to Dunbar, reminding him of their friendship. U.S. troops are seen searching the mountains but are unable to locate them, while a lone wolf howls in the distance. An epilogue Thirteen years later, their homes destroyed, their buffalo gone, the last band of free Sioux submitted to white authority at Fort Robinson, Nebraska.  The great horse culture of the plains was gone and the American frontier was soon to pass into history.  states that thirteen years later the last remnants of the free Sioux were subjugated to the American government, ending the conquest of the Western frontier states and the livelihoods of the tribes on the Great Plains|plains.

==Cast==
 
* Kevin Costner as Lt. John J. Dunbar / Dances with Wolves (Lakota language|Lakota: Šuŋgmánitu Tȟáŋka Ób Wačhí)
* Mary McDonnell as Stands With A Fist (Napépȟeča Nážiŋ Wiŋ) Graham Greene as Kicking Bird (Ziŋtká Nagwáka)
* Rodney A. Grant as Wind In His Hair (Pȟehíŋ Otȟáte)
* Floyd Red Crow Westerman as Chief Ten Bears (Matȟó Wikčémna)
* Tantoo Cardinal as Black Shawl (Šiná Sápa Wiŋ)
* Jimmy Herman as Stone Calf (Íŋyaŋ Ptehíŋčala)
* Nathan Lee Chasing His Horse as Smiles A Lot (Iȟá S’a)
* Michael Spears as Otter (Ptáŋ)
* Jason R. Lone Hill as Worm (Waglúla)
* Charles Rocket as Lt. Elgin
* Robert Pastorelli as Timmons
* Larry Joshua as Sgt. Bauer
* Tony Pierce as Spivey
* Kirk Baltz as Edwards
* Tom Everett as Sgt. Pepper
* Maury Chaykin as Maj. Fambrough
* Wes Studi as the fiercest Pawnee
* Wayne Grace as The Major Michael Horton Captain Cargill (Extended version)
 

==Production== Michael Blake, Pierre and Rapid City, with a few scenes filmed in Wyoming. Specific locations included the Badlands National Park, the Black Hills, the Sage Creek Wilderness Area, and the Belle Fourche River area. The bison hunt scenes were filmed at the Triple U Buffalo Ranch outside Fort Pierre, South Dakota, as were the Fort Sedgwick scenes, the set being constructed on the property. 

Production delays were numerous, because of South Dakotas unpredictable weather, the difficulty of directing barely trainable wolves, and the complexity of the Indian battle scenes. Particularly arduous was the films centerpiece bison hunt sequence: this elaborate chase was filmed over three weeks using 100 Indian stunt riders and an actual stampeding herd of several thousand bison. During one shot, Costner (who did almost all of his own horseback riding) was "T-boned" by another rider and knocked off his horse, nearly breaking his back. The accident is captured in The Creation of an Epic, the behind-the-scenes documentary on the Dances with Wolves Special Edition DVD and Blu-ray.
 CGI was then in its infancy) and only a few were animatronic or otherwise fabricated. In fact, Costner and crew employed the largest domestically owned bison ranch, with two of the tame bison being borrowed from Neil Young; this was the herd used for the bison hunt sequence.
 Western in film history, satirically dubbed "Kevins Gate" by Hollywood critics and pundits skeptical of a three-hour, partially subtitled Western by a novice filmmaker.

Lakota Sioux language instructor Doris Leader Charge (1931–2001) was the on-set Lakota dialogue coach and also portrayed Pretty Shield, wife of Chief Ten Bears.

Despite portraying the adopted daughter of Graham Greenes character Kicking Bird, Mary McDonnell, then 37, was actually two months older than Greene, and less than two years younger than Tantoo Cardinal, the actress playing her adoptive mother.

==Reception== Sioux Nation Best Writing, Michael Blake), Best Director Best Picture of the Year. In 2007, the Library of Congress selected Dances with Wolves for preservation in the United States National Film Registry. 
 Native American activist and Lawrence of Arabia? That was Lawrence of the Plains. The odd thing about making that movie is that they had a woman teaching the actors the Lakota language, but Lakota has a male-gendered language and a female-gendered language. Some of the Indians and Kevin Costner were speaking in the feminine way. When I went to see it with a bunch of Lakota guys, we were laughing."   Other Native Americans like Michael Smith (Sioux), Director of San Franciscos long-running annual American Indian Film Festival, said, "Theres a lot of good feeling about the film in the Indian community, especially among the tribes. I think its going to be very hard to top this one." 

Some of the criticism was inspired by the fact that the pronunciation is not authentic since only one of the movies actors was a native speaker of the language. The movies dialogues in the native language have been lauded as a remarkable achievement.  However, other writers have noted that earlier otherwise English-language films, such as Eskimo (film)|Eskimo (1933), Wagon Master (1950) and The White Dawn (1974), had also incorporated Native dialogue. 

David Sirota of Salon (website)|Salon referred to Dances with Wolves as a “white savior” film, as Dunbar “fully embeds himself in the Sioux tribe and quickly becomes its primary protector”. He argued that its use of the “noble savage” character type “preemptively blunts criticism of the underlying White Savior story. The idea is that a film like Dances With Wolves cannot be bigoted or overly white-centric if it at least shows   Kicking Bird and Chief Ten Bears as special and exceptional. This, even though the whole story is about a white guy who saves the day.” 
  Richard Grenier was strongly critical of the film. Grenier accused Costner of misrepresenting the Sioux as peaceful, claiming that the films "portrait of the Sioux, the most bloodthirsty of all Plains Indian tribes and neither pacifists nor environmentalists, is false in every respect".   Richard
Grenier, The Chicago Tribune. March 29, 1991. Retrieved May 24 2014. 

==Awards and honors== Western film Best Picture since 1931s Cimarron (1931 film)|Cimarron,    Dances with Wolves won the following additional awards, thereby being established as one of the most honored films of 1990:   

{| class="wikitable" style="width:95%;"
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Result
|-
| rowspan="12"| Academy Awards March 25, 1991 Best Picture Jim Wilson and Kevin Costner
| rowspan="2"  
|- Best Director
| rowspan="2"| Kevin Costner
|- Best Actor in a Leading Role
| rowspan="3"  
|- Best Actor in a Supporting Role Graham Greene
|- Best Actress in a Supporting Role
| Mary McDonnell
|- Best Adapted Screenplay Michael Blake
| rowspan="2"  
|- Best Cinematography
| Dean Semler
|- Best Art Direction
| Jeffrey Beecroft (Production Design) and Lisa Dean (Set Decoration)
| rowspan="2"  
|- Best Costume Design
| Elsa Zamparelli
|- Best Sound Russell Williams II, Jeffrey Perkins, Bill W. Benton, and Gregory H. Watkins
| rowspan="4"  
|- Best Film Editing
| Neil Travis
|- Best Original Score John Barry
|-
| rowspan="6"| Golden Globe Awards January 19, 1991 Best Motion Picture – Drama
|- Best Actor in a Motion Picture – Drama
| Kevin Costner
| rowspan="2"  
|- Best Actress in a Supporting Role in a Motion Picture
| Mary McDonnell
|- Best Director – Motion Picture
| Kevin Costner
| rowspan="2"  
|- Best Screenplay – Motion Picture Michael Blake
|- Best Original Score – Motion Picture John Barry
|  
|-
|}

American Film Institute recognition:
*AFIs 100 Years...100 Movies – #75
*AFIs 100 Years...100 Heroes and Villains:
**Lt. John J. Dunbar – Nominated Hero 
*AFIs 100 Years of Film Scores – Nominated 
*AFIs 100 Years...100 Cheers – #59
*AFIs 100 Years...100 Movies (10th Anniversary Edition) – Nominated 
*AFIs 10 Top 10 – Nominated Western and Epic Film 

Other accolades:
*Silver Bear for an outstanding single achievement – Kevin Costner at the 41st Berlin International Film Festival   

==Sequel== Michael Blake, the author of both the original Dances with Wolves novel and the movie screenplay, was published in 2001.  It picks up eleven years after Dances with Wolves. John Dunbar is still married to Stands with a Fist and they have three children. Stands with a Fist and one of the children are kidnapped by a party of white rangers and Dances with Wolves must mount a rescue mission. As of 2007, Blake was writing a film adaptation, although Kevin Costner was not yet attached to the project.  In the end, however, Costner stated he would not take part in this production. Viggo Mortensen has been rumored to be attached to the project, playing Dunbar.  As of January 2015, according to IMDb, The Holy Road is a TV mini-series still under development.

==Historical references== Pawnees are portrayed as stereotypical villains. Most accounts of Massacre Canyon|Sioux-Pawnee relations see the Pawnees as victims of the more powerful Sioux." 

Fort Sedgwick, In both the novel and film, Sedgwick is spelled Sedgewick.  Colorado was erected as Camp Rankin and renamed for General John Sedgwick (1813–64). Sedgwick was killed May 9, 1864, at the Battle of Spotsylvania Court House, Virginia. Fort Sedgwick served as an army post from July 1864 to May 1871. John Sedgwick did erect a fort in Kansas in 1860.

Fort Hays, Kansas was named for General Alexander Hays (1819–64). Hays was killed May 5, 1864, in the Battle of the Wilderness, Virginia. Fort Hays served as an army post from October 11, 1865, to November 8, 1889.

There was a real John Dunbar who worked as a missionary for the Pawnee in the 1830s–40s, and sided with the Indians in a dispute with government farmers and a local Indian agent.  It is unclear whether the name "John Dunbar" was chosen as a corollary to the real historical figure. 

The fictional Lieutenant John Dunbar of 1863 is correctly shown in the film wearing a gold bar on his officer shoulder straps, indicating his rank as a First Lieutenant. From 1836–72, the rank of First Lieutenant was indicated by a gold bar; after 1872, the rank was indicated by a silver bar. Similarly, Captain Cargill is correctly depicted wearing a pair of gold bars, indicating the rank of Captain at that time. 

In an interview, author and screenwriter Michael Blake said that Stands With a Fist, the white captive woman who marries Dunbar, was actually based upon the story of Cynthia Ann Parker, the white girl captured by Comanches and mother of Quanah Parker. 

Ten Bearss account of his grandfathers grandfathers driving out the Spanish conquistadors and later the war on Mexico and then Texas is more the history of the novels southern Great Plains Comanche tribe (Comanche history#Spanish|Comanche-Spanish wars, Comanche–Mexico Wars, Texas–Indian wars).  In the northern Great Plains, the eastern Sioux had already lost the Dakota War of 1862 with the United States and were driven out of Minnesota to Nebraska and South Dakota.  The Sioux were more successful during the Colorado War of 1863–65.

The epilogue  is correct; 13 years after the film is set, after the Great Sioux War of 1876, the last band of free Sioux surrendered at Fort Robinson, Nebraska, and the dominance and prevalence of the Plains Indians was over. The last conflict was the Ghost Dance War 1890–91 which involved the Wounded Knee Massacre.

==Home video editions==
The first Laserdisc release of Dances with Wolves was on November 15, 1991, by Orion Home Video in a two-disc extended play set. 

The first Dances with Wolves VHS version was released in 1991. It was subsequently issued in several further VHS versions. The limited collectors edition set comes with two VHS tapes, six high gloss   lobby photos, Dances with Wolves: The Illustrated Story of the Epic Film book, and an organized collectors edition storage case. 
 DTS soundtrack; the third was released on May 20, 2003, as a two-disc set featuring the Extended Edition; and the fourth was released on May 25, 2004, as a single disc in full frame. 

Dances with Wolves was released on Blu-ray Disc|Blu-ray in Germany on December 5, 2008, in France on 15 April 2009, in the United Kingdom on 26 October 2009, and in the United States on January 11, 2011. The German, French, and American releases feature the Extended Edition, while the British release features the theatrical cut. 

==Alternate versions==
One year after the original theatrical release of Dances with Wolves, a 4-hour version of the film opened at select theaters in London. This longer cut was dubbed Dances with Wolves: The Special Edition, and it restored nearly an hours worth of scenes that had been removed to keep the original films running time under 3 hours. 

{{quotation | In a letter to British film reviewers, director Kevin Costner and producer Jim Wilson addressed their reasons for presenting a longer version of the film: 

:Upon the release of the four-hour Dances With Wolves, the question naturally arises: why? Why add another hour to a film that by most standards pushes the time limit of conventional movie making? We opted to produce an extended version of Dances With Wolves for several reasons. The 52 additional minutes that represent this "new" version were difficult to cut in the first place...the opportunity to introduce them to audiences is compelling.

:We have received countless letters from people worldwide asking when or if a sequel would be made, so it seemed like a logical step to enhance our film with existing footage. Virtually every character is richer, from the teamster, Timmons, to the tribal chief, Ten Bears.

:Making an extended version is by no means to imply that the original Dances With Wolves was unfinished or incomplete; rather, it creates an opportunity for those who fell in love with the characters and the spectacle of the film to experience more of both. We hope you enjoy it.}}

The genesis of the 4-hour version of the film was further explained in an article for Entertainment Weekly that appeared only 10 months after the premiere of the original film:

{{quotation | While the small screen has come to serve as a second chance for filmmakers who cant seem to let their babies go, Kevin Costner and his producing partner, Jim Wilson, hope that their newly completed version will hit theater screens first: 

:"I spent seven months working on it," Wilson says of the expanded Wolves. Hes quick to defend the Oscar-winning version as "the best picture we had in us at the time," yet Wilson also says hes "ecstatic" over the recut. "Its a brand-new picture," he insists. "Theres now more of a relationship between Kevin and Stands With a Fist, more with the wolf, more with the Indians — stuff thats integral all through the story."

:Of course, exhibitors may not want a longer version of an already widely seen movie, but Wilson remains optimistic. "I dont think the time is now," he acknowledges, "but ideally, there is a point at which it would come out with an intermission, booked into the very best venues in America." A premiere in the form of a two-night network miniseries (à la The Godfather Saga) is also a possibility. For now, however, the four-hour Wolves remains a private dancer. }}

This Special Edition was eventually broadcast in 1993 for the American network television premiere at ABC. For the DVD release, the Special Edition was dubbed an Extended Cut. For Blu-ray, the same cut was renamed Directors Cut.

Director Kevin Costner would later claim that he did not work on the creation of the 4-hour cut at all. 

==Soundtrack==
  John Barry composed the Oscar-winning score. It was issued in 1990 initially and again in 1995 with bonus tracks and in 2004 with the score "in its entirety" (although, in reality, approximately 25 minutes of the score is still missing from the 2004 release). 
* Peter Buffett scored the "Fire Dance" scene.

==See also==
 

* White savior narrative in film

==Bibliography==
*  
*  

==Notes==
 

==References==
 

==External links==
 
* Archived website http://web.archive.org/web/*/http://www.danceswithwolves.net/
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 