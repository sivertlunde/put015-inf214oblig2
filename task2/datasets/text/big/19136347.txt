Day of the Assassin
{{Infobox film
| name           = Day of the Assassin
| image          = 
| image size     =
| caption        = 
| director       = Brian Trenchard-Smith
| producer       = 
| writer         = 
| based on = 
| narrator       =
| starring       = Chuck Connors Glenn Ford
| music          = 
| cinematography = 
| editing        = 
| studio = 
| distributor    = 
| released       = 1979
| runtime        = 
| country        = USA Spain Mexico English
| budget         = 
| gross = 
| preceded by    =
| followed by    =
}} Mexican and American co-production, it was filmed in Spain, directed by Brian Trenchard-Smith. It starred Chuck Connors, Susana Dosamantes, Glenn Ford and Richard Roundtree.

Trenchard-Smith says making the movie was one of his craziest directing experiences:
 All directors, at some point in their career trajectory, find themselves hanging on to a runaway train; despite best efforts, things turn to custard on a daily basis. More often than not, The Movie from Hell is a co-production. Foreign locale, fast money, giant egos, high pressure schedule – all make a volatile witches’ brew, even before you factor in deep rooted national resentments. A co-production is a business model designed to diminish trust between nations.   Mexico, Spain, and the US were the partners, which meant that the Spaniards felt superior to their Mexican brothers, and the Americans felt superior to everybody. Each country gave undertakings to deliver certain elements of cast or crew. Disputes arose immediately. The American director bailed when his deposit failed to arrive. His agent, who was also mine, immediately slotted me in there, so within a day, the American producers, unbeknownst to me, offered their partners a replacement director: “the man who made the last Bruce Lee movie.” I had in fact made a documentary about the late Bruce Lee, “The World of Kung Fu.” The Mexicans thought they were getting Enter the Dragon director, Robert Clouse. Their disappointment was palpable when this misrepresentation became clear on the first day of prep in Mexico City. Must say, my sphincter tightened a little too. I had arrived in a war zone.  

==References==
 

==External links==
* 
* 
 

 
 
 
 


 