Aalkkoottathil Thaniye
 

{{Infobox film
| name           = Aalkkoottathil Thaniye 
| image          = Aalkkoottathil Thaniye.JPG
| image_size     = 
| alt            = 
| caption        = Theatrical poster
| director       = I.V. Sasi
| producer       = Raju Mathew
| writer         = M. T. Vasudevan Nair
| narrator       =  Seema  Balan K. Nair Shyam
| cinematography = Jayanan Vincent
| editing        = K Narayanan
| studio         = Century Films
| distributor    = Century Films 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}

Aalkkoottathil Thaniye   ) is a 1984 Malayalam film directed by I. V. Sasi, written by M. T. Vasudevan Nair, and starring Mammootty, Mohanlal and Seema (actress)|Seema.   

==Plot==

Rajan( Mammootty), though successful in his job, but his family life is not so happy. His wife,Nalini,( Unnimary)aspires for a fellowship in Harvard University,USA, leaving behind her husband and kid, Babumon.
When his father,Madhavan( Balan K Nair) , a retired school teacher,  is down with ailment, Rajan along with his family returns to his ancestral home. Rajans elder sister, Vishalam, younger sister,Seethalakshmi & husband Padmanabhan also arrive to meet their father, as his health condition was serious.
Madhavans daughters and in-laws were busy in their own ways, as they dont wish to spend many days with their dying father.In his sickness, Madhavan expresses his wish to meet his niece,Ammukutty(Seema),  Ammu arrives and took care of her dying uncle.Madhavans children except Rajan leaves as Ammu was there to look after their father.Rajan remembers his young days of romance with Ammu.

Flashback goes like this : Rajan and Ammu wished to marry each other.Ammu,an elementary school teacher helped him economically for his higher studies, which helped Rajan to conquer a good job.But then, Rajans boss,Balachandran , who is Madhavans disciple wished to marry his daughter Nalini to Rajan.Madhavan in spite of knowing his sons and Ammus love , insisted Rajan to marry Nalini. Helplessly , Rajan obeyed his father. Ammukkutty desperately started living alone separately in her house thereafter and continued her job.

Now,  Nalini went to go back to town from ancestral home, to attend her interview to Harvard University. Busy with his job, Rajan also want to leave.Upon Ammus request, he leaves his son with her.
Babumon and Ammu becomes close and they spend those days happily.
Later Nalini passes interview and is all set to go to USA.Knowing her sons relation with Ammu, she asks Rajan to bring Ammu as servant to look after her son , when shes away to USA.
Rajan  tells her , Ammu is the one who spent money for his higher studies.Nalini later tried to return  the amount of money which Ammu spent for her husband. Ammu remorsely shouts at her.Understanding his wifes fault, Rajan slaps her and reveals their unfruitful love story. Ammukutty later felt sorry for her words and advises Nalini.
Nalini repents for her rudeness and wished to stay back in India with family.But upon Ammus insistence,telling not to lose a great opportunity, Rajan and Nalini unanimously decide to get Nalini fly to USA for fellowship.
Madhavans health slowly recovers.Rajan ,Nalini and son  happily goes back ,leaving Ammukutty again in her loneliness.
 

==Cast==
 
*Mammootty as Rajan Seema as Ammukkutty
*Mohanlal as Anil Kumar
*Balan K. Nair as Madhavan
*Unnimary as Nalini
*Lalu Alex as Padmanabhan Sumithra as Seethalakshmi
*Master Prasobh as Babumon
*Kuthiravattam Pappu as Kutti Narayanan Janardhanan as Balachandran
*Adoor Bhasi as Achuthan
*Sukumari as Cheeru Shubha as Vishalam
*Jalaja as Sindhu
* Kundara Johny as Gopinath
 

==Soundtrack== Shyam and lyrics was written by Kavalam Narayana Panicker.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Allimalar || S Janaki || Kavalam Narayana Panicker ||
|-
| 2 || Onnaanaam oonjal || P Susheela, Chorus || Kavalam Narayana Panicker ||
|}

==References==
 

==External links==
*  

 
 
 
 
 
 