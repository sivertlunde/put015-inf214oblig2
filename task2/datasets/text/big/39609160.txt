Pastorale 1943
 
{{Infobox film
| name           = Pastorale 1943
| image          = 
| caption        = 
| director       = Wim Verstappen
| producer       = Frans Rasker
| writer         = Simon Vestdijk Wim Verstappen
| starring       = Frederik de Groot
| music          = 
| cinematography = Marc Felperlaan
| editing        = 
| distributor    = 
| released       =  
| runtime        = 125 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
}}
 Best Foreign Language Film at the 51st Academy Awards, but was not accepted as a nominee. 

==Cast==
* Frederik de Groot as Johan Schults
* Leen Jongewaard as Eskens
* Coen Flink as Hammer
* Sacco van der Made as Ballegooyen
* Hein Boele as Cohen
* Geert de Jong as Mies Evertse
* Liane Saalborn as juffrouw Schölvink
* Bram van der Vlugt as van Dale
* Bernard Droog as Poerstamper
* Femke Boersma as moeder Poerstamper
* Dick van den Toorn as Piet Poerstamper
* Peter Römer as Kees Poerstamper

==See also==
* List of submissions to the 51st Academy Awards for Best Foreign Language Film
* List of Dutch submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 
 