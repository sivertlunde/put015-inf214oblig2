An American Werewolf in Paris
{{Infobox film
| name           = An American Werewolf in Paris
| image          = ParisWerewolf.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Anthony Waller
| producer       = Richard Claus Tim Burns Tom Stern
| based on       =  
| starring       = Tom Everett Scott Julie Delpy Vince Vieluf Phil Buckman Julie Bowen
| music          = Wilbert Hirsch
| cinematography = Egon Werdin
| editing        = Peter R. Adam
| studio         = Hollywood Pictures Propaganda Films   J&M Entertainment   Cometstone Pictures   Avora Media   Delux Productions Buena Vista Pictures Becker Entertainment (AU) Entertainment Film Distributors (UK)
| released       =  
| runtime        = 105 minutes
| country        = United Kingdom Netherlands Luxembourg United States France
| language       = English French
| budget         = $25 million
| gross          = $26,570,463 
}}
An American Werewolf in Paris is a 1997 Comedy film|comedy-horror film directed by Anthony Waller and starring Tom Everett Scott and Julie Delpy. The film was in development for 6 years  and follows the general concept of, and is a loose sequel to, the 1981 film An American Werewolf in London.

The title of this film has its roots in the production of its predecessor; when production of the original London film ran into trouble with British British Actors Equity Association|Equity, director John Landis, having scouted locations in Paris, considered moving the production to France and changing the title of his film to An American Werewolf in Paris. 

==Plot== first film. That night, Andy, Chris, and Brad attend a night club called "Club de la Lune". The clubs owner, Claude, is actually the leader of a werewolf society that uses the club as a way to lure people (preferring tourists) in to be killed. Serafine arrives, tells Andy to run away and transforms into a werewolf. The club owners transform into werewolves as well and butcher all the guests. Chris escapes but is later kidnapped by Claude, Brads heart is eaten by a werewolf and Andy is bitten by another werewolf.

The next day, Andy wakes up  at Serafines house. He is still in shock, but Serafine allows him to feel her breasts to calm him down. She tells him hes transforming into a werewolf. This is interrupted by the sudden appearance of Alexs ghost. Andy leaves and soon Brads ghost appears to Andy and explains Andys werewolf condition. In order for Andy to become normal again, he must eat the heart of the werewolf that bit him; in order for Brads ghost to be at rest, the werewolf that ate his heart must be killed too. Andy hooks up with an American tourist named Amy, but transforms and kills her. Andy also kills a cop who had been tailing him, suspecting Andy was involved in the Club de la Lune massacre. Andy is arrested but escapes. He begins to see Amys ghost as well, and she begins trying to kill him.

Claude and his henchmen ask Andy to join their society, but to prove his loyalty, Andy must kill Chris. Serafine rescues Andy, explaining that her father prepared a drug to control werewolf transformations, but instead the drug forces werewolves to immediately transform into their beast form, as a result she killed her mother and savaged her stepfather. Claude and the other werewolves then raid Serafines stepfathers lab and kill him, taking the drug to transform immediately.

Serafine and Andy learn of a Fourth of July party Claude has planned and infiltrate it. They help the partygoers escape and Andy manages to kill the werewolf that ate Brads heart, thus setting Brad free. The cops arrive and a fight ensues. Andy and Serafine manage to kill many werewolves, with Serafine shifting to her beast form to fight when she runs out of ammunition. During a fight between Serafine and Claude, Andy shoots one of the wolves but it turns out that he has shot Serafine.

Claude makes his way onto a subway train, but slips onto the tracks. A train slams into him, causing him to transform back to a human. He tries to take another dose of the drug, but Andy stops him. As they fight, Andy discovers that Claude is the werewolf that bit him. Claude tries to inject himself with the drug but accidentally injects Andy instead. Andy transforms into a werewolf, kills Claude and eats his heart, breaking the werewolf curse. Serafine is taken in an ambulance, but begins to show signs of transforming. The EMT, thinking she is going into shock, administers adrenaline, which stops the transformation - where the "cure" (which turned out to be a sedative) triggered the change, adrenaline has the opposite effect.
 
The film ends with Serafine and Andy celebrating their wedding atop the Statue of Liberty with Andys pal Chris, who survives. The couple seem to be controlling the curse with a steady application of adrenaline-fueled activities. They bungee jump off it as the credits roll.

In an alternate ending, after Andy eats Claudes heart, Serafine has a vision of her step-father in the back of an ambulance, explaining how he found a cure before his death. The new closing scene shows Serafine and Andy having a child, whose eyes shift to look like the werewolves.

==Cast==
* Tom Everett Scott as Andy McDermott
* Julie Delpy as Sérafine Pigot McDermott
* Vince Vieluf as Brad
* Phil Buckman as Chris
* Julie Bowen as Amy Finch
* Thierry Lhermitte as Dr. Thierry Pigot
* Pierre Cosso as Claude
* Tom Novembre as Inspector Leduc
* Anthony Waller as Metro Driver

==Reception== Rick Baker, CGI for its transformation effects and chase sequences, a common point of derision from most critics. According to box-office sales and online reviews, this sequel proved to be much less successful than the first film.

The film opened on December 25, 1997 to #7 with $7,600,878. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 