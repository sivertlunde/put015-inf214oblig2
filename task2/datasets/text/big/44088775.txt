Kaumarapraayam
{{Infobox film 
| name           = Kaumarapraayam
| image          =
| caption        =
| director       = KS Gopalakrishnan
| producer       =
| writer         = Cheri Viswanath
| screenplay     = Cheri Viswanath Anuradha Sukumari Adoor Bhasi Shyam
| cinematography = V Karunakaran
| editing        = K Sankunni
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by KS Gopalakrishnan. The film stars Krishnachandran, Anuradha (actress)|Anuradha, Sukumari and Adoor Bhasi in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
*Krishnachandran Anuradha
*Sukumari
*Adoor Bhasi
*Sathaar
*Kuthiravattam Pappu Ravikumar

==Soundtrack== Shyam and lyrics was written by Chunakkara Ramankutty. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ee raavil njan || S Janaki || Chunakkara Ramankutty || 
|-
| 2 || Kaaveri Nadikkarayil Valarnna Kanyakayo  || Jolly Abraham || Chunakkara Ramankutty || 
|-
| 3 || Makarasankramaraathriyil || Vani Jairam, Jolly Abraham || Chunakkara Ramankutty || 
|-
| 4 || Swargavaathil thurannu || P Jayachandran || Chunakkara Ramankutty || 
|}

==References==
 

==External links==
*  

 
 
 

 