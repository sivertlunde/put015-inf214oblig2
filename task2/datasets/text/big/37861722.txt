Onnelliset leikit
{{Infobox film
| name           = Onnelliset leikit
| image          = 
| caption        = 
| director       = Aito Mäkinen Esko Elstelä
| producer       = Risto Orko
| writer         = Aito Mäkinen Esko Elstelä Satu Waltari
| starring       = Pirkko Peltonen
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = Finland
| language       = Finnish
| budget         = 
}}

Onnelliset leikit is a 1964 Finnish comedy film directed by Aito Mäkinen and Esko Elstelä. It was entered into the 4th Moscow International Film Festival.   

==Cast==
* Pirkko Peltonen (segment "Juulia")
* Raimo Nenonen (segment "Juulia")
* Riitta Elstelä as Mother (segment "Tikku")
* Lasse Liemola as Father (segment "Tikku")
* Eeva Elstelä as Daughter (segment "Tikku")
* Saara Elstelä as Daughter (segment "Tikku")
* Joel Elstelä as Son (segment "Tikku")
* Tuija Hulkko
* Etta-Liisa Kunnas
* Ritva Vepsä

==References==
 

==External links==
*  

 
 
 
 
 
 


 
 