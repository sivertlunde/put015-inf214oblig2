Gandu (film)
{{Infobox film
| name           = Gandu
| image          = Gandu poster.jpg
| caption        = 
| director       = Qaushiq Mukherjee, known as Q
| producer       = Overdose Joint
| writer         = Surojit Sen  Qaushiq Mukherjee|Q.
| screenplay     = 
| story          = 
| based on       =  
| starring       = Anubrata  Joyraj  Kamalika  Shilajit Rii   
| music          = Five Little Indians (music)  Q. (lyrics) Q
| consultant Editor = Rajarshi Basu
| editing        = Manas Mittal Q.
| sound          = Chaki
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}}
 Q who has described the film as a "rap musical".  It features Anubrata, Joyraj, Kamalika, Silajit, and Rii in the lead roles. The films music is by the alternative rock band Five Little Indians.  Gandu previewed at Yale University before making its international premiere on October 29, 2010 at the 2010 South Asian International Film Festival in New York City.    Gandu was an official selection at the 2011 Berlin International Film Festival and was also screened at the Slamdance Film Festival. 
 Osian Film Festival.  The main star Anubrata Basu is shown with his penis fully erect in a love scene. 

==Plot==
The movie is set on the life of an unnamed protagonist who is called Gandu by most who address him in the movie (an Indian slang/swear word that would literally translate to of the ass the English translation running in the subtitle translates it to the English Asshole). Gandu is portrayed as a frustrated teenager whose state of mind is shown by intercuts of him rapping in Bengali (it is later revealed in the movie that the protagonist is a member of a rap band). Gandus mother seemingly supports the family through the magnanimity of her lover Dasbabu (an unvoiced character who appears frequently) Gandus mother and Dasbabu are shown having graphic sex a number of times. Gandu himself is shown repeatedly sneaking into the room of the copulating couple of steal money from Dasbabus wallet. Gandu seemingly has complex emotions regarding this stealing and takes care to hide from his mother. 

Gandu is also apparently quite lonely. His choice of hairstyle (shaved nearly bald) and general proclivity to drugs and rap makes him an object of ridicule in front of most of his peers, who mock him as an Egglike Gandu. His loneliness is dispelled when he literally bumps into Rickshaw a cycle rickshaw driver who slaps Gandu and then scares him off by a bizarre show of Kung Fu (it is later revealed that Rickshaw literally worships Bruce Lee and models himself on his idol) That night Gandu has a dream of himself and Rickshaw in a near naked embrace (this has led to the common perception that Gandu and Rickshaw share a homosexual relationship but the movie itself never shows this explicitly and it maybe a more symbolic element) 

On one of his stealing missions, Gandus mother sees him and hides him from Dasbabu and seemingly laughs in complicity. This causes Gandu great anguish and he curses in frustration. His mother then knocks on his door and slaps him angrily and walks away. She is naked. 

Gandu urges Rickshaw to take him away Rickshaw and Gandu then go off to consume Dhatura Seeds which causes them both to go on a strange and extremely intense trip. When they come to their senses they find they have no money. In a meta narrativeistic scene the director Q himself drives into the scene and Gandu is told by Rickshaw how Gandu is a character in a movie being shot by Q. Gandu cannot quite comprehend this. 

Gandu comes back to Kolkata to find that he has won a lottery of Rs. 50,000 (less than 1000 dollars) he gives him mother some of the money and goes on a crack binge with Rickshaw. Rickshaw then admonishes him for never having actually sleeping with a woman despite incessantly rapping about cunt and pussy and sex. Gandu then has a surrealistic and very explicit sex scene (the only colour part in the movie) with an unnamed woman who constantly meows (it is unclear if this is symbolic) at the end of coitus she whispers demo to him. Gandu goes back to Rickshaw telling him how he will now record a demo of his rap and show it to ADF (Asian Dub Foundation) The movie closes with Gandu finally becoming successful.  

The film is shot mostly in black and white stressing on the bleak existence of the protagonist. At the end of the movie, the film shifts to the color mode.

Gandu also ponders on the meaning of life and what we must do with it.

==Cast==
*Anubrata Basu as Khusru/Gandu(The Protagonist)
*Joyraj Bhattacharjee/ Joyraj as Ricksha
*Kamalika Banerjee/ Kamalika as Gandus mother
*Shilajeet Majumdar/ Silajit as Dasbabu Rituparna Sen / Rii as the Girl in the Cafe/Kaali/the Prostitute

==Production== Boal Theatre of the Oppressed|techniques. Rituparna, who plays several different roles in the film, is Q.s real life girlfriend. 

==Title track==

The films soundtrack has garnered rave reviews as well, composed by the Calcutta-based alternative rock band Five Little Indians and mixed by London-based producer Miti Adhikari.

==Awards==
*2010 - Jury Award for Best Film at the South Asian International Film Festival 

==References==
 

==External links==
*  
*   on   (watch the full film in HD online.)

 
 
 
 
 
 
 
 
 
 