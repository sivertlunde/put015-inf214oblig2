Pursuit (1935 film)
{{Infobox film
| name           = Pursuit
| image          = 
| alt            = 
| caption        = 
| director       = Edwin L. Marin
| producer       = Lucien Hubbard Ned Marin
| screenplay     = Wells Root 
| story          = Lawrence G. Blochman
| starring       = Chester Morris Sally Eilers Scotty Beckett Henry Travers C. Henry Gordon Dorothy Peterson
| music          = William Axt
| cinematography = Charles G. Clarke Sidney Wagner
| editing        = George Boemler 	
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Pursuit is a 1935 American action film directed by Edwin L. Marin and written by Wells Root. The film stars Chester Morris, Sally Eilers, Scotty Beckett, Henry Travers, C. Henry Gordon and Dorothy Peterson. The film was released on August 9, 1935, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Chester Morris as Mr. Mitch Mitchell
*Sally Eilers as Maxine Bush
*Scotty Beckett as Donald McCoy Donny Smith
*Henry Travers as Thomas Tom Reynolds
*C. Henry Gordon as Nick Shawn
*Dorothy Peterson as Mrs. McCoy
*Granville Bates as Auto Camp Proprietor
*Minor Watson as Hale
*Harold Huber as Jake
*Dewey Robinson as Jo-Jo
*Erville Alderson as Cop 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 