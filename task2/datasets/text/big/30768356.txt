The County Fair (1920 film)
{{Infobox film
| name           = The County Fair
| image	=	The County Fair FilmPoster.jpeg
| caption        = poster for film Edmund Mortimer Maurice Tourneur
| producer       = Maurice Tourneur
| writer         = J. Grubb Alexander (writer) Charles Barnard (play)
| starring       = 
| music          =
| cinematography =   Charles Van Enger
| editing        =
| distributor    = Guy Croswell Smith
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         =
| gross          =
}}
 Edmund Mortimer and Maurice Tourneur.

==Plot==
As summarized in a film publication,    Aunt Abigail (Chapman) and her adopted daughter Sally (Eddy) are threatened with the loss of their home through foreclosure of a mortgage held by Solon Hammerhead (Mong). The only outlets available are either for Abigail to marry the old villain Salon or for Sally to marry his mean, scheming son Bruce (Housman). To prevent Aunt Abigail from losing her home, Sally is about to agree to marry the son, even though she loves the hired hand Joel (Butler). She has only a few days to decide before the mortgage falls due. That night former jockey Tim Vail breaks into the home looking for food, and sweet Aunt Abigail gives him a job on the farm. Tim discovers that Abigails horse "Cold Molasses" is a born racer, and Tim and Joel get permission to train the horse for the big $3000 race at the County Fair, which takes place the day the mortgage comes due. The Hammerheads, discovering that the horse would likely beat their entry, attempt to keep her out of the race by setting fire to the barn, but Tim rescues the horse. The big race starts with Cold Molasses in the lead. At the last second the Hammerhead horse passes her and it looks like the house will be gone, but then the Hammerhead entry is disqualified for foul play. The winnings pay off the mortgage, Joel and Sally are happy, and even Aunt Abigail finds a suitor.

== Cast ==
*Helen Jerome Eddy as Sally Greenway David Butler as Joel Bartlett
*Edythe Chapman as Aunt Abigail Prue
*William V. Mong as Solon Hammerhead
*Arthur Housman as Bruce Hammerhead
*John Steppling as Otis Tucker Charles Barton as Tim Vail
*Wesley Barry as Tommy Perkins

==References==
 

== External links ==
 
* 
* 

 

 
 
 
 
 
 
 
 
 


 