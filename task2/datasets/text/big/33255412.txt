Live Fast, Die Young (film)
 .]] Mary Murphy, Norma Eberhardt, Mike Connors and Sheridan Comerate. Considered a cult film, promotional campaigns used the tagline "a sin-steeped story of the rise of the Beat Generation."    

It was also known as Seed of Violence. Troy Donahue was borrowed from Universal-International to play his role. Basehart Gets Clown Story: Fisher, Wayne Team Seen; Betty Lanza in Film Debut
Schallert, Edwin. Los Angeles Times (1923-Current File)   18 Dec 1957: B13.  
==Plot==
Two sisters, Kim Winters (Murphy) and Jill Winters (Eberhardt) run away from their home and school.  They escape to the city, where they become criminals and jewelry thieves. 

==Cast== Mary Murphy as Kim Winters and the films narrator
* Norma Eberhardt as Jill Winters
* Mike Connors as Rick
* Sheridan Comerate as Jerry
* Peggy Maley as Sue Hawkins
* Troy Donahue as Artie Sanders 
* Carol Varga as Violet
* Joan Marshall as Judy Tobin Gordon Jones as Pop Winters 
* Robert Karnes as Tommy "Tubbs" Thompson Robert Carson as Frank Castellani  John Harmon as Jake, a Hobo
* Norman Leavitt as Sam, a hotel clerk

==Legacy==
A cult classic, Norma Eberhardt noted that, “The film tapped into what kids were feeling – that society sucked and they were rebelling against it."  Screenshots of Eberhardt were printed onto T-shirts worn by Slash (musician)|Slash, the former guitarist of Guns N Roses, in 2007.  Eberhardt was described as "highly amused" by Slashs wardrobe. 

==References==
 

==External links==
*  
*   at Turner Classic Movies

 
 
 
 
 

 