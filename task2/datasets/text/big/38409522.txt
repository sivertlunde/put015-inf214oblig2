Alkohol
{{Infobox film
| name           = Alkohol 
| image          = 
| caption        = 
| director       = Ewald André Dupont   Alfred Lind
| producer       = 
| writer         = Ewald André Dupont   Alfred Lind
| starring       = Wilhelm Diegelmann   Ernst Rückert   Georg H. Schnell   Emil Biron
| music          = 
| editing        = 
| cinematography = Karl Hasselmann   Charles Paulus
| studio         = Stern-Film
| distributor    = 
| released       =  
| runtime        = 
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
}} silent drama Enlightenment films" examing social issues, which were produced around the time. 

==Synopsis==
A middle-class man falls in love with a woman from a more ordinary background, and they end up working in a variety act where they sink into alcoholism. He then kills another man whom he mistakenly believes is a rival.

==Cast==
* Wilhelm Diegelmann   
* Ernst Rückert  
* Georg H. Schnell
* Emil Biron Jean Moreau   
* Auguste Pünkösdy   
* Ferry Sikla
* Toni Tetzlaff  
* Hanni Weisse   
* Maria Zelenka

==References==
 

==Bibliography==
* Bergfelder, Tim & Cargnelli, Christian. Destination London: German-speaking emigrés and British cinema, 1925-1950. Berghahn Books, 2008.
* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910-1933. Berghahn Books, 2005.

==External links==
* 

 

 
 
 
 
 
 
 
 