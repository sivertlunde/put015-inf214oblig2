Snow Dogs
{{Infobox film
| name = Snow Dogs
| image = Snow dogs.jpg
| caption = Theatrical release poster
| director = Brian Levant
| producer = Jordan Kerner Michael Goldberg Mark Gibson Philip Halprin
| based on =   Graham Greene
| music = John Debney
| cinematography = Thomas E. Ackerman
| editing = Roger Bondelli Kerner Entertainment Buena Vista Pictures Distribution
| released =  
| runtime = 99 minutes
| country = United States
| language = English
| budget = $33 million
| gross = $115 million
}}
Snow Dogs is a 2002 American   by Gary Paulsen.

==Plot==
Dr. Theodore "Ted" Brooks (  and a  s, thin ice, red fox|foxes, striped skunk|skunks, grizzly bear|bears, an intimidating, crusty old mountain man named James "Thunder Jack" Johnson (James Coburn), and the aggressive, defiant lead dog, Demon. All of this happens with the buzzing excitement of the Arctic Challenge Sled Dog Race which is only two weeks away.

During his stay in Tolketna, Brooks attempt to find the reason why he was given up for adoption, and including any information about his biological father. He meets a bar owner named Barb (Joanna Bacalso), who was a close friend of his biological mother. Eventually, Barb helps Brooks to deal with the dogs and teaches him how to drive a sled, and falls in love with him throughout the film. Brooks continues to encounter Thunder Jack, who wants all of his biological mothers sled dogs, and especially Demon. Barb told Brooks that Thunder Jack is Brooks real father. Brooks feels that the truth is worth the dogs themselves, and trades them to Thunder Jack. However, Jack tells Brooks that he and Lucy stayed in a cave during one of the Arctic Challenges. It is there that Brooks was conceived. The next morning, when Thunder Jack woke up, Lucy was gone. He says that he was looking for Lucy, but never found her. Lucy then gave Brooks up for adoption.

With Demon, Thunder Jack takes part in the Arctic Challenge Race, but is unable to finish it when he attempts to gain time by continuing through a huge snowstorm. Meanwhile, Brooks, who just returned to Miami, learns that his personal journey to Alaska is unfinished. Infuriated by evidence found proving that Thunder Jack was at the hospital when he was born, Brooks returns to Alaska to look for answers. When Brooks learns of the missing musher and his team, he feels that this may be the opportunity to save a man, and perhaps find out the whole truth once and for all. Brooks sets out to rescue Thunder Jack, with Nana as the lead dog. He eventually finds Thunder Jack in the old cave, who confirms that he had been there when Brooks was born. Thunder Jack reveals that he and Lucy agreed then that neither one of them could raise a baby, and confessed his love for Lucy. Brooks also finds out the reason for Demons bad temper is the pain caused by his bad tooth. He pulls it out and Demon now becomes a friendly dog (despite keeping his name). During the journey back to Tolketna, the sled nearly goes over a cliff into a river, but the dogs managed to pull themselves back up. Brooks finally brings Thunder Jack across the finish line. After Brooks introduces Thunder Jack to his adopted mother, Brooks and Thunder Jack decide to share their trophy together. 

Sometime later, Brooks and Barb are married and Nana and Demon have four puppies. Now having grown to love Tolketna, Brooks moves his dentistry practice there. Barb is shown to be pregnant and working as his receptionist. The film ends when Brooks adoptive cousin, Rupert (Sisqó), becomes the new famous Miami dentist with his face on every city bus.

==Cast==
===Human roles===
* Cuba Gooding Jr. as Dr. Theodore "Ted" Brooks (a.k.a. "Teddy Bear")
* Joanna Bacalso as Barb
* James Coburn as James "Thunder Jack" Johnson
* Sisqó as Dr. Rupert Brooks
* Nichelle Nichols as Amelia Brooks
* Christopher Judge as Dr. Brooks
* Michael Bolton as Himself
* M. Emmet Walsh as George
* Brian Doyle-Murray as Ernie Graham Greene as Peter Yellowbear
* Jean Michel Paré as Olivier
* Jason Pouliotte as Sneed Brother #1
* David Boyce as Sneed Brother #2
* Frank C. Turner as Neely
* Ron Small as Arthur
* Alison Matthews as TV Reporter
* Jascha Washington as Young Ted
* Linda Dahling as Mrs. Yepremian
* Danelle Folta as Rollerblader with Dog
* Peter Musooli as Valet
* Lossen Chambers as Receptionist
* Andrea Butterfield as Patient
* Oscar Goncalves as Ernesto Julio Santisto
* Angela Moore as Lucy
* Jim Belushi as the voice of Demon
* Jane Sibbett as the voice of Nana
* Richard Steven Horvitz as the voice of Scooper (uncredited)

===Animals===
* Fly as Nana
** Dash as Nana (stunt double)
* Don Juan/D.J. as Demon
** Cody as Demon (stunt double)
* Floyd as Mac
* Speedy as Scooper
* Nando as Diesel
* Buck as Sniff
* Koda as Yodel
* Gloria as Dutchess Grizzly Bear as the Bear
* Flower as the Skunk Red Fox as the Fox
 David Barclay.

==Production==
The films budget was United States dollar|US$33 million.
 
Canmore, Alberta|Canmore, Alberta, Canada was used to film the fictional city of Tolketna, Alaska.

The dogs D.J., Koda, Floyd and Buck also starred in the adventure film, Eight Below. Many of the dogs and mushers used in the film were locals. Two of the hero team doubles and all of Oliviers team were supplied by Nakitsilik Siberians of Bridge Lake, British Columbia. Mountain Mushers from Golden BC supplied the Thunder Jack team. Old Ernies team was supplied by Russ Gregory from Calgary, Alberta.  Arcticsun Siberian Husky Kennel from Edmonton, Alberta was one of many kennels — including Czyz, Snowy Owl, Gatt racing — from the area that supplied background for the film. Two of the dogs came from Kortar Kennels, in Ontario.

The animatronic effects were designed and built by Jim Hensons Creature Shop. The special effects were provided by The Secret Lab, the special effects division of Disney.

==Reception==
The film received generally negative reviews, with a 24% approval rating at Rotten Tomatoes based on 80 reviews.   It was however, a financial success, earning $115 million worldwide against a $33 million budget. 

==Awards==
John Debney won the ASCAP Award in 2003 for the soundtrack.

==Pop culture==
Referenced in the Gilmore Girls Season 2 episode, "Teach Me Tonight".

==See also==
*Eight Below
*Nankyoku Monogatari

== References ==
 
 

==External links==
* 
* 
* 
* 
* 

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 