Love Letters (1984 film)
{{Infobox film
| name           = Love Letters
| image          = Love-letters-movie-poster-1984.jpg
| image_size     =
| caption        =
| director       = Amy Holden Jones
| producer       = Roger Corman
| writer         = Amy Holden Jones
| based on       =
| starring       = Jamie Lee Curtis Bonnie Bartlett James Keach Amy Madigan
| music          =
| cinematography =
| editing        =
| distributor    = New World Pictures
| released       =  
| runtime        =
| country        = United States English
| budget         = $600,000 
| gross          =
}}
Love Letters is a 1984 film from director Amy Holden Jones which Roger Corman agreed to finance following her success with Slumber Party Massacre (1982). It is a romantic drama that stars Jamie Lee Curtis.

==Plot==
 
A young woman who discovers her mother had an affair with a married man for 15 years starts her own affair with a married man. 

==Cast==
*Jamie Lee Curtis as Anna Winter 
*James Keach as Oliver Andrews  
*Bonnie Bartlett as Maggie Winter Matt Clark as Chuck Winter
*Amy Madigan as Wendy
*Brian Wood as Frank
*Phil Coccioletti as Ralph Glass
*Larry Cedar as Jake
*Rance Howard as  Joseph Chesley 
*Jeff Doucette as Hippy
*Sally Kirkland as Sally  Lyman Ward as  Morgan Crawford 
*Bud Cort as Danny De Fronso

==Production==
Amy Holden Jones had just made her feature film debut as director with Slumber Party Massacre and wanted to follow it up with something non-horror. She and her husband had written love letters to each other several years previously when living on different coasts; she rediscovered them and wondered what would happen if their daughter came across the love letters. At the same time Jones saw Shoot the Moon about a man who has an affair and thought that while the story of an affair told from the point of view of the unfaithful married man had been done man times, she had never seen the story from the point of view of the girlfriend to the married man. Jones:
 I put that together with the love letters and thought it would be interesting if someone came upon the love letters and realized that their parents had had an extramarital affair, if the love letters were not in fact between her mother and father, as ours were, but between the mother and a lover. In other words, what would happen if you were confronted with an understanding of a time period in your parents life which you never really understand -- none of us have a real idea of what our parents were like in their twenties. How would that affect your life? And I thought it would be interesting if that then thrust her into an affair with a married man, trying to replicate what she saw her mother had. Basically, it was designed to be a movie about what happens to the woman outside of the marriage, who is usually, in fiction, painted as a terrible villain and often is kind of a victim who gets left in the end.   accessed 10 November 2013  
Jones said she was inspired to do flashbacks by the screenplays of Harold Pinter. 

Her first choice for the lead was Meg Tilly but her agent was demanding more money than Corman was willing to pay. Jamie Lee Curtis agreed to play the role for only $25,000 despite several nude scenes as it gave her a chance to break away from the horror films she had been mostly making up until that stage of her career. Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 226  The film was financed by Roger Corman who insisted on the inclusion of some nudity. Chris Nashawaty, Crab Monsters, Teenage Cavemen and Candy Stripe Nurses - Roger Corman: King of the B Movie, Abrams, 2013 p 190  Jones:
 He wants either sex, violence or humor. He actually told me that lovemaking wasnt so much required as nudity. And he didnt mind if she could just be lounging around the house nude, but there had to be nudity. He had to have some way to sell the thing. Its actually the one thing that troubles me about it. I find some of the nudity really gratuitous. But it was the price we paid to get it made.  

==References==
 

==External links==
*  at IMDB

 

 
 