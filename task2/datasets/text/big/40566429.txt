The Wedding Ringer
{{Infobox film
| name           = The Wedding Ringer
| image          = TheWeddingRingerPoster.jpg
| caption        = Theatrical release poster
| director       = Jeremy Garelick
| producer       = Adam Fields Will Packer 
| writer         = Jeremy Garelick Jay Lavender
| starring       = Kevin Hart Josh Gad Kaley Cuoco-Sweeting
| music          = Christopher Lennertz
| cinematography = Bradford Lipson 
| editing        = 
| studio         =  
| distributor    = Screen Gems
| released       =  
| runtime        = 101 minutes 
| country        = United States
| language       = English
| budget         = $23 million   
| gross          = $75.5 million 
}}
The Wedding Ringer is a 2015 comedy film co-written and directed by Jeremy Garelick. It stars Kevin Hart, Josh Gad, and Kaley Cuoco-Sweeting. The film was produced by Adam Fields, Will Packer Productions and Miramax Films, distributed by Screen Gems, and released on January 16, 2015.  

==Plot==
  best man services for guys who dont have the friends necessary for a wedding. As Doug Harris (Josh Gad), a successful tax attorney, and his fiancee Gretchen Palmer (Kaley Cuoco-Sweeting) are planning for their wedding day, Doug becomes frantic searching for a best man. He faints and is referred to Jimmys company, The Best Man Inc., by his party planner, Edmundo (Ignacio Serricchio).
 Bic Mitchum.
 Dan Gill), who can dislocate and relocate his shoulder, and Otis, who can say every sentence backwards.

Doug, excited, tells Gretchen that "Bic" flew in from El Salvador, and he will be at the wedding. He also presents to her two honeymoon tickets to Tahiti. Gretchen insists that Bic comes to the family brunch. Doug meets Jimmy and they memorize their identities. Doug also tells Jimmy that he must act as a military priest from North Dakota.

At the brunch, Gretchens father Ed (Ken Howard) asks Jimmy what part of North Dakota hes from. Doug becomes nervous and almost blows his own cover until Jimmy accidentally sets Grandma (Cloris Leachman) on fire. They take her to the emergency room, and Jimmy makes up a lie to Ed that Doug used to play football. Ed challenges Jimmy and Doug to a football game with some of his old college teammates who will be at the wedding.

Doug meets his groomsmen, who he is initially not fond of. Doris gives all of them fake identities, all of which share the last names of famous Los Angeles sports figures (Jim Plunkett|Plunkett, Kurt Rambis|Rambis, Steve Garvey|Garvey, Lyle Alzado|Alzado, Don Drysdale|Drysdale, Rod Carew|Carew, and Eric Dickerson|Dickerson) which they have to memorize. Jimmy takes the groomsmen on fake photo shoots of skydiving, scuba-diving, bowling, running a marathon, and climbing mountains. As Jimmy realizes Doug beginning to doubt him, he is taken to Edmundos house. Doug learns Edmundos flamboyant personality is just a ruse for better business. Edmundo tells him that the only key is to please Gretchen and her mother Lois (Mimi Rogers) and nothing else matters. To prove to Doug how good he is at being a best man, Jimmy takes Doug to a wedding where the best man (Josh Peck) makes a terrible speech. After, they have drinks and show off their dance moves. The two drink after the wedding and talk. Jimmy said he made an excellent best man speech for a minor friend which led to his career as a wedding ringer. Doug said that his father was an international tax attorney and moved frequently as a child, so he never got to make friends. He also tells Jimmy hes never been on a "guy trip" and just wants a friend to "sit down and have a beer with." When his parents died, Doug took over the business, and work consumed him, leaving him without a best man. Jimmy drives Doug home, and reiterates that they are in a business relationship, and Doug, although hurt, agrees.

Jimmy, thinking about what Doug said, is reminded by Doris why he started his business in the first place. She tells Jimmy he needs a real friend for himself, and he is motivated to pull off the rest of the wedding.
 basset hound to lick off the peanut butter. When the prank goes awry, the groomsmen and Nadia race to the hospital and evade a cop chase. When Doug wakes up the next day, Nadia kisses him goodbye, and hints she would like to know him better. Later, the groomsmen play football with Ed and his college football friends, including Joe Namath, John Riggins, and Ed "Too Tall" Jones. A mud bowl ensues and Ed blows out his knee on the last play.

At the rehearsal dinner that night, Gretchens bridesmaids sing a song, while Dougs groomsmen create a slideshow of the fake pictures they previously took, winning Gretchen over. That night, Gretchen, speaking to Doug, notices Bic razors and Mitchum deodarant in their cabinets. She recognizes the familiarity in the last names of the groomsmen and deduces the scheme. She asks Doug about it but he brushes it off, saying Gretchen is paranoid, to which she reluctantly agrees.

On the day of the wedding, the family priest cancels and is replaced by Jimmys old Catholic school principal. Doug hatches an idea where Reggie and Fitzgibbons kidnap the priest and Jimmy officiates the wedding as Bic Mitchum. The wedding reception is initially successful. Jimmy passes Gretchen and congratulates her on the wedding. Gretchen exclaims that the wedding is a disaster because her grandmother has third degree burns, her dads knee is blown out, the food is bad, the bridesmaids are not attracted to the groomsmen, the zipper on her dress is broken, and she isnt marrying the man she loves. Gretchen confesses to Jimmy she only married Doug because she was tired of having bad relationships and he can easily provide the lavish lifestyle she wants. Doug overhears this in the bathroom and tells Jimmy that he cant go through with the rest of the wedding. Jimmy dismisses Dougs ideas and makes the best man speech.
Before Jimmy wraps up his best man speech, Doug stops it and reveals he and Gretchen arent married since "Bic" is not a real priest. He also tells everyone that his groomsmen are fake. Jimmy, Doug, and the groomsmen try to escape. Before doing so, Jimmy asks Gretchens sister, Allison (Olivia Thirlby) on a date, which she accepts. Doug pays Jimmy his $50,000 like he promised and they accept each others friendship. As they leave, Jimmy has an idea.

They use Dougs first class honeymoon tickets to Tahiti, taking him on a guy trip where the groomsmen, Edmundo, Doris, and Nadia, who begins her romance with Doug, party in first class, while Lurch says "Pilot (Lost)|Ive got a bad feeling about this flight."

In a scene at the end of the credits, Jimmy and Doug are seen playing a painful game of tennis.

==Cast==
 
*Kevin Hart as Jimmy Callahan/Bic Mitchum
*Josh Gad as Doug Harris
*Kaley Cuoco-Sweeting as Gretchen Palmer
*Alan Ritchson as Kip/Carew
*Cloris Leachman as Grandma
*Mimi Rogers as Lois Palmer
*Ken Howard as Ed Palmer
*Affion Crockett as Reggie/Drysdale
*Jenifer Lewis as Doris Jenkins
*Olivia Thirlby as Allison Palmer 
*Jorge Garcia as Lurch/Garvey
*Josh Peck as Bad Best Man
*Joe Namath as Himself
*John Riggins as Himself
*Ed "Too Tall" Jones as Himself
*Aaron Takahashi as Endo/Rambis
*Dan Gill as Bornstein/Dickerson
*Corey Holcomb as Otis/Alzado
*Colin Kane as Fitzgibbons/Plunkett
*Ignacio Serricchio as Edmundo Nicole Whelan as Nadia
*Whitney Cummings as Holly Munk
*Jeff Ross as a Wedding Singer 
*Nikki Leigh as The 15 Year Old
 

==Production==
===Development=== Road Trip director Todd Phillips was attached to produce the comedy, which revolves around the preparations for a wedding for which the groom has hired a professional best man. 

In October 2006, The Hollywood Reporter reported that Garelick and Lavender, co-writers of the successful film The Break-Up, would now co-direct the film. The project, which the pair originally penned in 2001, had drifted through development hell as they wrote endless drafts, tailoring the script to various actors who flirted with the idea of starring in it.   

On June 10, 2013, it was announced that Screen Gems and Miramax Films would be collaborating on production of the film project, still using the working title The Golden Tux. It was announced that the buddy comedy film would star Kevin Hart and Josh Gad and Garelick would make his feature film directorial debut.  In September 2013, production had started on the project and the title of the film had officially been changed to The Wedding Ringer. 
 Ride Along, Sony moved up the release date for The Wedding Ringer to January 16, 2015, during the 2015 MLK Holiday weekend. 

===Filming===
The film was originally set in Chicago but was rewritten for Los Angeles after the producers won a $2.8-million California tax credit.    Filming took place in Santa Monica and Marina del Rey, California over the course of 38 days in the fall 2013. 

==Release==
===Promotion=== Restricted "Holiday" trailer on November 20, 2014.   

On October 8, 2014, Kevin Hart announced through his Twitter account that he was "doing a college tour" to help promote The Wedding Ringer.  Hart said that during this promotional tour, "he is focusing on college students because they are big social-media users" and can help promote the new film. He also encouraged the students to "tweet" about the film. 

===Home media===
The Wedding Ringer was released on DVD and Blu-ray on April 28, 2015. 

==Reception==
===Box office===
As of April 12, 2015, The Wedding Ringer has grossed $64.5 million in North America and $11 million in other territories for a worldwide total of $75.5 million, against a budget of $23 million.   
 MLK Holiday weekend, the film grossed $24,042,152. 

===Critical response===
The Wedding Ringer received generally negative reviews from critics. On Rotten Tomatoes the film holds a rating of 28%, based on 88 reviews, with an average rating of 4.4/10. The critical consensus reads, "Kevin Hart and Josh Gad might be two great comedians that go great together, but theres little evidence of it on display in The Wedding Ringer."  On Metacritic, the film has a score of 35 out of 100, based on 27 critics, indicating "generally unfavorable reviews". 

Richard Roeper gave the film 1.5 out of 4 stars, acknowledging the chemistry between Hart and Gad but criticizing the "cockamamie premise". 

IGN gave the film a 6.2/10, stating that "the movie is actually pretty funny." 

ABCNews gave the movie 3.5/5 stars, stating "Hart and Gad make you laugh simply by showing up. Add a moderately funny script, and The Wedding Ringer is a winner." 

Film review blog Movie Metropolis was more critical, saying that it felt like Hart was on autopilot for much of the films running time and the deeper storyline got lost in a mess of slapstick humour.   

Audiences polled after viewing The Wedding Ringer gave the film an "A-" CinemaScore. 

==Sequel==
The director has teased a possible sequel with Vince Vaughan.

==References==
{{reflist|2|refs=
   
   
}}

==External links==
* 
* 
* 

 
 
 
 
 
 
 