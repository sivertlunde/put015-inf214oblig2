My Dinner with Andre
{{Infobox film
| name           = My Dinner with Andre
| image          = My Dinner with Andre 1981 film theatrical release poster.jpg
| border         = yes
| alt            = This is the theatrical release poster for the 1981 film "My Dinner with Andre."
| caption        = Original theatrical release poster
| director       = Louis Malle
| producer       = {{Plainlist |
* George W. George
* Beverly Karp
}}
| writer         = {{Plainlist |
* Andre Gregory
* Wallace Shawn
}}
| starring       = {{Plainlist |
* Andre Gregory
* Wallace Shawn
}}
| music          = Allen Shawn
| cinematography = Jeri Sopanen
| editing        = Suzanne Baron
| distributor    = New Yorker Films
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
}}

My Dinner with Andre is a 1981 film starring Andre Gregory and Wallace Shawn, written by Gregory and Shawn, and directed by Louis Malle.

The film depicts a conversation between Gregory and Shawn (not necessarily playing themselves) at Café des Artistes.  Based mostly on conversation, the films dialogue covers such things as experimental theatre, the nature of theatre, and the nature of life, contrasting Shawns modest, down-to-earth humanism with Gregorys extravagant spiritual experiences.

==Structure== Findhorn in buried alive on Halloween night.
 taxi ride about his childhood and mentions that when he arrives at home, he tells his girlfriend Debbie about his dinner with Andre. Erik Saties Gymnopédies (Satie)|Gymnopédie No. 1 plays in the background.

==Production==
The idea for the film arose from Gregorys effort to work with a biographer on his life story, and Shawn simultaneously coming up with an idea for a story about two people having a conversation.   Gregory and Shawn, who had become friends through theatre work, decided to collaborate on the project. They agreed that it should be filmed rather than produced as a play.  Although the film was based on events in the actors lives, Shawn and Gregory denied (in an interview by film critic Roger Ebert) that they were playing themselves. They said that if they remade the film, they would swap the two characters to prove their point. In an interview with Noah Baumbach in 2009, Shawn said, 
 "I actually had a purpose as I was writing this: I wanted to destroy that guy that I played, to the extent that there was any of me there. I wanted to kill that side of myself by making the film, because that guy is totally motivated by fear."  

The screenplay went through numerous developmental changes in location; in the final version, it was set during a dinner at a restaurant. While Shawn was trying to find someone to direct the film, he received a phone call from French director Louis Malle. He had read a copy of the screenplay via a mutual friend and insisted that he work on the project, saying he wanted to direct, produce the film, or work on it in any capacity.   Shawn initially thought that the call was a prank, due to Malles stature in film. Malle later suggested that the dinner setup would not work, based on a rehearsal where Gregory was talking while eating.  Shawn argued throughout screenplay development for more scenes, which would have resulted in a three-hour film. Malle won many script cuts, but lost two arguments over scenes that were kept in the film. 
 Jefferson Hotel, which was then vacant, in Richmond, Virginia. (The hotel has since been restored and reopened as a luxury venue.) Lloyd Kaufman was the production manager on the film, and Troma Entertainment provided production support.    The filming was done over a period of two weeks, and edited to appear as if occurring in real-time. The set was created to look like the iconic Café des Artistes in New York City. 

==Reception==
Roger Ebert and Gene Siskel gave high praise to the film on Sneak Previews; the producers told Ebert that their praise helped keep the film in theaters for a year.    Ebert later named it as the best film of the year. In 1999, he added it to his Great Movies essay series. He said, "Someone asked me the other day if I could name a movie that was entirely devoid of clichés. I thought for a moment, and then answered, My Dinner With Andre.".  The Boston Society of Film Critics Awards ranked it as the "Best American Film" in 1982, and awarded Gregory and Shawn its prize for best screenplay.

== In popular culture ==
 
* My Dinner with Andre was parodied by Andy Kaufman and wrestler Fred Blassie in My Breakfast with Blassie (1983).
* In the eighth episode of the second season of the 1980s sitcom, Sledge Hammer!, entitled "Hammer Hits the Rock," a prisoner complains that prison is just "men talking;" a moment later, that nights film is announced as My Dinner with Andre.
* The 24th and final episode of the first season of Frasier was entitled "My Coffee With Niles;" it featured a long conversation about their lives between Frasier and Niles Crane|Niles, in which he frequently asked,"Are you happy?" The film is also referenced in the fifth season episode "The Zoo Story", where Martin inadvertently watches it after mixing up his rented video with Niles, who by contrast watches Death Wish.
* In the episode of The Simpsons, "Boy-Scoutz N the Hood", Martin Prince plays an arcade game based on the film. 
* In Waiting for Guffman, the character Corky St. Clair (played by Christopher Guest) displays My Dinner with Andre action figures during the tour of his shop.
* A strip from the comic The Far Side is captioned "My Dinner With Andy". The two characters in the panel have just finished a food fight.
* A season four episode of Touched by an Angel is titled "My Dinner With Andrew;" Andrew is the Angel of Death, played by John Dye, and he and a guest star talk in a restaurant, contrasting their views.
* A segment of Not Necessarily the News depicted two youngsters listlessly arguing over who would play which character in a "My Dinner With Andre" video game. episode 2x19, "Critical Film Studies": Abed invites Jeff to dinner and they have a deep conversation; later it is revealed Abed was trying to recreate the film.
* In the season 1 episode 3 "Movie Hecklers" sketch from the Comedy Central show Key & Peele, a character yells at a screen, "Its a visual medium, man. Enough with this My Dinner with Andre bullshit!"
* While hosting the 70th Academy Awards, Billy Crystal jokingly named the "favorite movies" of certain famous people.  He showed a photo of Bart the Bear and said "My Dinner WAS Andre".
* In Season 2 Episode 1 of Family Ties, Jennifer refers to My Dinner with Andre when talking with Mallory, who has no idea of the subject.
* The film is parodied in the Family Guy episode Brian The Closer, as My Dinner with Andre The Giant, where Andre, who suffered from gigantism, complains that hes wasting his shortened life on Gregorys discussions about mundane life.

==See also==
*Robert Ogilvie Crombie

== References ==
{{reflist|refs=

 My Dinner With Andre, Criterion Collection, 715515046114 

 {{cite web
 |  last=Rabin 
 |  first=Nathan 
 |  url=http://www.avclub.com/articles/wallace-shawn,26010/2/ 
 |  title=Wallace Shawn – Film – Random Roles 
 |  publisher=The A.V. Club 
 |  date=2009-04-01 
 |  accessdate=2012-07-04}} 

 {{cite web
 | url=http://www.criterion.com/current/posts/1186-when-noah-met-wally 
 | title=WHEN NOAH MET WALLY – From the Current – The Criterion Collection 
 | publisher=Criterion.com 
 | date= 
 | accessdate=2012-07-04}} 

 {{cite book
 | url=http://books.google.com/books?id=8IIwGgReVGIC&pg=PA196&dq=lloyd+kaufman+my+dinner+with+andre&hl=en&ei=ZLC9TrvEN6-KsALO74GvBA&sa=X&oi=book_result&ct=result&resnum=1&ved=0CDIQ6AEwAA#v=onepage&q&f=false 
 | title=Make Your Own Damn Movie!: Secrets of a Renegade Director – Lloyd Kaufman, Trent Haaga, Adam Jahnke – Google Books 
 | publisher=Books.google.com 
 | date= 
 | accessdate=2012-07-04}} 

 {{cite book
 | url=http://books.google.com/books?id=RHZyAQDhfsMC&pg=PA118&dq=lloyd+kaufman+my+dinner+with+andre&hl=en&ei=ZLC9TrvEN6-KsALO74GvBA&sa=X&oi=book_result&ct=result&resnum=2&ved=0CDkQ6AEwAQ#v=onepage&q=lloyd%20kaufman%20my%20dinner%20with%20andre&f=false 
 | title=Fifty Filmmakers: Conversations With Directors from Roger Avary to Steven Zaillian 
 | author=Andrew J. Rausch, Michael Dequina 
 | chapter = Lloyd Kaufman
 | publisher=Google books
 | pages=118- 
 | date=2008-02-25 
 | accessdate=2012-07-04}} 

 {{cite book
 | url=http://books.google.com/books?id=-eDfuEXUHp8C&pg=PA70&dq=lloyd+kaufman+my+dinner+with+andre&hl=en&ei=ZLC9TrvEN6-KsALO74GvBA&sa=X&oi=book_result&ct=result&resnum=4&ved=0CEQQ6AEwAw#v=onepage&q=lloyd%20kaufman%20my%20dinner%20with%20andre&f=false 
 | title=SPIN – Google Books 
 | publisher=Books.google.com 
 | date= 
 | accessdate=2012-07-04}} 

 {{cite web
 | url=http://archive.gamespy.com/reviews/september03/republicpc/ 
 | title=GameSpy.com – Review 
 | publisher=Archive.gamespy.com 
 | date=2003-09-12 
 | accessdate=2012-07-04}} 

}}

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 