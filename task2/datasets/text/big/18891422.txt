Prisoner of Zenda, Inc.
  1996 made Jonathan Jackson. It was produced for Showtime Networks under their family division, and first aired September 1996.   The film was written by Rodman Gregg and Richard Clark.
 1937 MGM version of The Prisoner of Zenda, starring Ronald Colman, The Prisoner of Zenda, Inc. was a contemporary version loosely based on the original. Zenda was the castle in mythical kingdom of Ruritania in the 1937, whereas Zenda Inc. was the name of a computer business empire in the 1996 version.   William Shatners character of the evil "Uncle Michael", based on Raymond Masseys character Prince Michael in the original, was deftly played with a comedic flair.

The film continued the theme of mistaken identities which was central to the plot of the 1937 MGM classic. Jackson plays Oliver and his lookalike Rudy (who is named after Prince Rudolf from the original, and in this case is a Star Trek fan—a nod to co-star Shatner). The film also starred American character actor Don S. Davis from the popular television series Stargate SG-1.

It was released on VHS under the renamed title Double Play, but reverted to the original title for the DVD release.

==External links==
* 

 
 
 
 
 
 
 
 


 