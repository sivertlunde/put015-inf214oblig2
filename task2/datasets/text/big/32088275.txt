Tom Brown's Schooldays (1916 film)
Tom British silent silent drama Rex Wilson Jack Coleman and Evelyn Boucher.  It is an adaptation of the novel Tom Browns Schooldays by Thomas Hughes. It is set at Rugby School in the 1830s where Tom Brown encounters the villainous bully Flashman. It was made at Catford Studios.

==Cast==
* Joyce Templeton as First Tom Brown Jack Coleman as Second Tom Brown Jack Hobbs as Third Tom Brown
* Miss Marley as Mrs. Arnold
* Evelyn Boucher as Cynthia Brown
* Wilfred Benson as Doctor  Arnold
* Mr. Daniels as Squire Brown
* Mr. Johnson as Harry East
* Laurie Leslie as Flashman
* E.C. Arundell as Wheelwright
* Mona Damt as Dame Brown
* Eric Barker as Arthur
* Rolf Leslie as Jacob Doodlecalf
* H. Dobell as Benjy
* Mr. Morley as Tadpole
* Mr. Canielli as Slogger Williams

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 