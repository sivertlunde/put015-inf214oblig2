Hungry Hill (film)
{{Infobox film
| name = Hungry Hill
| image = "Hungry_Hill"_(1947).jpg
| director = Brian Desmond Hurst
| producer = William Sistrom
| writers  = Terence Young and Daphne du Maurier, from the novel by Dappne du Maurier
| starring = Margaret Lockwood  Dennis Price Cecil Parker Dermot Walsh Michael Denison Jean Simmons
| music = John Greenwood, played by the London Symphony Orchestra, directed by Muir Mathieson
| cinematography = Desmond Dickinson
| editing = Alan Jaggs
| studio  = Two Cities Films
| distributor = General Film Distributors
| released = 7 January 1947 (London)(UK)
| running time = 109 minutes
| country        = United Kingdom
| language       = English
}}
 Terence Young the novel by Daphne du Maurier. 

A feud is waged between two families in Ireland - the Brodricks and the Donovans - over the sinking of a copper mine in Hungry Hill by Copper John Brodrick. The feud has repercussions down three generations. 

==Cast==
* Margaret Lockwood as Fanny Rosa
* Dennis Price as Greyhound John Brodrick
* Cecil Parker as Copper John Brodrick
* Michael Denison as Henry Brodrick
* F.J. McCormick as Old Tim
* Arthur Sinclair as Morty Donovan
* Jean Simmons as Jane Brodrick
* Eileen Crowe as Bridget
* Eileen Herlie as Katherine
* Barbara Waring as Barbara Brodrick Michael Golden as Sam Donovan
* Siobhán McKenna as Kate Donovan
* Dan OHerlihy as Harry Brodrick
* Henry Mollison as Dr. Armstrong
* Dermot Walsh as Wild Johnnie Brodrick

==Critical reception==
* The New York Times wrote, "the films running time is about average, ninety minutes, but the narrative, for all its ample conflict, progresses so ponderously that it seems interminable...The few moments of effective cinema in "Hungry Hill" are so fleeting as to be easily forgotten, but the sequence wherein a staid ball is turned into a lively jig session by the infectious music of a fiddler from the town is a bit of expert staging which you probably wont see duplicated again soon. The spontaneity and brilliant conception of this scene is almost sufficient cause to make one show more tolerance toward "Hungry Hill" than it deserves."  
*Britmovie noted a "stirring Irish saga based on the epic novel by Daphne du Maurier."  

==References==
 

==External links==
*  
*   at the website dedicated to Brian Desmond Hurst

 
 

 
 
 
 
 
 
 