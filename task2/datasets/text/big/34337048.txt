Fetih 1453
{{Infobox film
| name           = Fetih 1453
| image          = Conquest1453.jpg
| caption        = Theatrical release poster
| alt            = 
| director       = Faruk Aksoy
| producer       = Ayşe Germen
| writer         = İrfan Saruhan
| starring       =  
| music          = Benjamin Wallfisch
| cinematography = 
| editing        = 
| studio         = Aksoy film production
| distributor    = Tiglon Film Kinostar
| released       =  
| runtime        = 160 minutes
| country        = Turkey Turkish Greek Greek Arabic Arabic
| budget         = $18.2 million  
| gross          =  183.241.062  
}} epic action Fall of Ottoman Turks during the reign of Sultan Mehmed II..

==Storyline==
The film is opened in Medina during the time of the Islamic prophet, Muhammad (627). Abu Ayyub al-Ansari tells other sahabas that Constantinople will be conquered by a blessed commander and army..

The story shifts abruptly to the 15th century. Sultan Muhammad al-Fatih was given the throne by his father Murat II when he was 12; he learns of his fathers death while governing the Sanjak of Saruhan. This causes him much grief and paves the way for his ascension to the throne again, after the death of his brother Fathıl IV. When Sultan Mehmet had first ascended the throne, he was also 12 years old. Murat II, suffocated by the political hostility of his margraves and viziers, relinquished the throne due to the impact of his deep grief caused by his beloved son Mohamed’s death and enthroned Mehmet. Grand Vizier Halil Pasha, who had a great influence on janissary and the state, was dissatisfied because of this situation. He was especially troubled with Sultan Mehmet indicating that Constantinople’s conquest is vitally essential. He made Sultan Murat inherit the throne again in anticipation of the possibility of crusaders occupying Ottoman territories by taking advantage of Mehmet. Mehmet was suspended from the throne and sent to Sanjak of Saruhan.

Now, he succeeded to the throne again and was even more powerful. His priority target was still the conquest of Constantinople. He was gaining inspiration from the words of Muhammad: “Constantinople will surely be conquered. What a blessed commander is its and what a blessed army is its army.”

He worked out everything that would take him to the target. At the outset, he should live in peace with contiguous countries  until he made the preparations. He sent messengers to the Pontificate, to Hungarians, Serbians, Poles, Genoeses and Venetians and notified them of his intention to live in peace. He restored Gallipoli dockyard and because of this, 100 galleys could be produced there in a year. Meanwhile the Roman Emperor Constantine thought that Sultan Mehmet was inexperienced and foresightless and demanding heavy appropriations by trying to use Prince Orhan who was captive of Constantine. Constantine’s main intention was Sultan Mehmet becoming disrespectable by capitulating. Furthermore, Sultan Mehmet was already capitulating and accepting his demands. But this was just the strategy of Sultan Mehmet.

As soon as the news of Karamans rebellion received, Ottoman armies set forth Akşehir. Karamanoğlu İbrahim didn’t expect such a mighty army. He had to demand peace. Sultan Mehmet accepted the peace because he didn’t want his armies to be harmed unnecessarily. After the military expedition, on the way back a group of janissaries confronted the state tent for payment although they had notactually engaged in battle. In response, Sultan Mehmet sent out enthronements, he also sent the janissary master Kurtçu Doğan who was a man of Grand Vizier Halil Pashas into exile. With this incident, he properly gained dominion over his armies.

After he returned to Adrianople, he sent a messenger to Emperor Constantine and he declared that he would no longer send the subsidy he paid for Orhan. After that, he started to build the Boğazkesen (Rumelian) Fortress across the Anatolian Fortress. This meant actually to wage war against East-Roman Empire..

On 29 May 1453, the Byzantine soldiers on the ramparts were overwhelmed against Sultan Mehmet and thousands of Turkish soldiers. 

== Historical Accuracy ==
 had been western crusaders in 1204). The Great Palace was not in use at the time.    The films portrayals of the Byzantines as a wealthy, powerful empire whose rulers lived lives of decadence and luxury at the time of the Conquest may be motivated for dramatic purposes, but they do not reflect the true situation in Constantinople in 1453. 

The film only depicts the ethnically Turkish element of the Ottoman army. In reality, the Ottoman army was very diverse, including many Balkan converts to Islam as well as Christian levies and the armies of the Sultans Christian vassals. 

Giovanni Giustiniani was wounded by an Ottoman cannon while defending the walls of Constantinople, as opposed to being killed in single combat. Some sources say the wound was caused by a crossbow bolt. He died of the effects of his wound in the early days of June 1453. 

Constantine XI is given a burial.  In fact he had died fighting at the gates and as customary for Ottoman troops, his body was beheaded.  Although his body was recovered, his head was not, leading many Byzantines to believe that Constantine XI was alive. 

Sultan Mehmet entered the city after one day of looting of the army. 

==Production and release==
The production costs of the film are not well-known. The film was produced over a period of three years and cost an estimated $17 million.   Other sources claim that the actual cost of the film is US$8 million.  A Turkish journalist Ali Eyuboglu asked budget to producer and producer claimed that they never stated any budget to press. In addition to this, another co-producer commented to Ali Eyuboglu that 4 million ticket will be afford expenses for the film. In Turkey profit to producer is estimated $2 per ticket, so the filmshould cost no more than $8 million.  It is still the most expensive film in Turkish cinema history. The film trailer itself took one and a half months to complete and cost $600,000. The trailer was viewed by over 1.5 million people within 24 hours of its release. 
The size of the full cast was extensive; the film reportedly required the use of 16,000 extras. 

Fetih 1453 was released in different countries on 16 February 2012, including United States, the United Kingdom, France, Egypt, United Arab Emirates, Kazakhstan, Georgia, Germany, the Netherlands, Macedonia, Russia, Azerbaijan, South Korea, Japan and several others.  Universal Studios have expressed an interest in acquiring the distribution rights to the film. 

==Cast==
{| class="wikitable"
|-
! Actor name !! Role name !! Explanation
|-
| Devrim Evin || Mehmed II || The 7th Ottoman sultan who seeks to conquer Constantinople. Mehmeds childhood is played by Ege Uslu.
|-
| İbrahim Çelikkol || Ulubatlı Hasan || Mehmeds friend and mentor, leader of Ottoman cavalry corps. He martyrs himself when placing an Ottoman banner in the top of Walls of Constantinople while suffering multiple arrow wounds.
|-
| Dilek Serbest || Era || Orbans adopted daughter he bought from a slave market in Constantinople. She has a romantic relationship with Hasan. Eras childhood is played by Algun Molla.
|- Emperor Constantine XI || The last Byzantine emperor. In this film, when he dies, Mehmed orders Byzantine noblemen to bury him in Christian tradition.
|- Knight Giustiniani || Genoese general. He is killed by Hasan.
|-
| Erden Alkan || Çandarlı Halil Pasha || Ottoman Grand Vizier serving under Murad II and Mehmed II. He rejects all Mehmeds plans relating to the conquest of Constantinople and urges peace with Byzantium.
|- Grand Duke Notaras || The last Megas Doux of Constantinople. He shows strong opposition towards Constantines intention to seek help from Vatican and Genoa.
|-
| Erdoğan Aydemir || Orban || A Hungarian master who initially proposes his sketch to Doge of Genoa, but the Doge isnt interested in it. Orban refuses Notaras demand to design a cannon for Byzantium. When Notaras men attempts to arrest Orban for his refusal, Hasan saves him and Era, his adoptive daughter to Edirne. Orban later design the Great Bombard for the Ottoman Empire used in the siege of Constantinople.
|-
| İlker Kurt || Murad II || The 6th Ottoman sultan, father of Mehmed II.
|-
| Sedat Mert  || Zagan Pasha || An Ottoman military commander who is used to be an ardent advocate for the conquest of Constantinople. He often confronts with Halil Pasha urging to live in peace with Byzantine Empire.
|-
| Raif Hikmet Çam  || Akşemseddin || One of Mehmeds tutors. He comes to Mehmed in the 40th day of the siege, and motivates the then-upset and frustrated Sultan with the discovery of Abu Ayyub Al Ansharis tomb near the Walls of Constantinople.
|-
| Namık Kemal Yıiğittürk  || Molla Hüsrev || One of Mehmeds tutors who invites Akşemseddin to motivate the Sultan in the 40th day of the siege.
|-
| Öner As  || Molla Gürani || One of Mehmeds tutors who invites Akşemseddin to motivate the Sultan in the 40th day of the siege.
|-
| Mustafa Atilla Kunt  || Şahabettin Pasha || An Ottoman military commander and vizier. He is tasked by Sultan Mehmed II to make three furnaces. During the siege of Constantinople he attacks the city from Tekfur Palace (Palace of the Porphyrogenitus) and the Gate of Caligaria.
|-
| Özcan Aliser  || Saruca Pasha || An Ottoman military commander and vizier.
|-
| Murat Sezal  || İsa Pasha || An Ottoman military commander.
|-
| Faik Aksoy  || Karaca Pasha || An Ottoman military commander. During the siege of Constantinople, he attacks the city from the Gate of Charisius and Blachernae Palace (Ayvansaray).
|-
| Hüseyin Santur  || Suleiman Baltoghlu|Süleyman Pasha || An Ottoman admiral. During the siege of Constantinople, he attacks the city from the Golden Horn. He is banished by Mehmed after the failure to enter the Golden Horn.
|- Nicholas V).
|-
| Ali Ersin Yenar  || Doge of Genoa || An unnamed Doge of Genoa who orders Giustiniani to command Genoese army after an assault towards Genoese freight in the Bosphorus (the contemporary Doge in that time was Pietro di Campofregoso).
|- Cardinal Isidore || A cardinal who offers supports from Vatican to Byzantium.
|-
| Adnan Kürkçü  || Gennadius Scholarius || An Orthodox theologian who strongly opposes the Emperors plan to unite Eastern Orthodoxy with Roman Catholicism.
|-
| Şahika Koldemir  || Gülbahar Hatun I|Gülbahar Hatun || Mehmeds wife, mother of Prince Bayezid.
|- Prince Orhan || Pretender to the Ottoman throne who is an exile in Constantinople. During the siege of Constantinople, he is assigned to defend Port of Langa.
|-
| Aslan İzmirli  || İbrahim II of Karaman|Karamanoğlu İbrahim || Bey of Karamanids provoked to rebel against Ottoman Empire by Constantine XI.
|- Prince Bayezid || Mehmed IIs son.
|-
| Oğuz Oktay  || Osman I || The founder of Ottoman Empire, Mehmeds forefather. In this film, he is depicted to appear before Mehmed in Mehmeds dream. Osman tells Mehmed that he is the conqueror mentioned by Muhammad.
|-
| Tuncay Gençkalan  || Abu Ayyub al-Ansari || One of Muhammads sahaba depicted to retell Muhammads word about the capture of Constantinople by a blessed army and commander. In his later life, he joins a Muslim army to conquer Constantinople in 670s, but he dies in Constantinople and is buried there...
|-
| Halis Bayraktaroğlu  || Kurtçu Doğan || Leader of the Janissary.
|-
| Songül Kaya  || Lady Emine || Halil Pashas wife.
|-
| Hüseyin Özay  || Ali the Blacksmith || Hasans teacher.
|-
| Buminhan Dedecan || Mustafa || An Ottoman tunnel master.
|-
| Emrah Özdemir  || Selim || An Ottoman tunnel foreman.
|-
| Yiğit Yarar  || Hüseyin || An Ottoman soldier.
|-
| Hüseyin Bozdemir  || Mahmud || Orbans assistant.
|-
| Osman Volkan Erciyes ||  Fathıl IV ||  The last brother of Mehmed. In this film, when he dies, Mehmed becomes ruler.
|}

== Release==
The film was released on 15 February 2012 at 14:53 local time. It sold 1.4 million tickets on its first weekend and 2.23 million tickets in its first week of release.  In 18 days, it surpassed Recep İvedik 2 to become the most watched film ever in Turkey.  As of 13 May 2012, it has sold 6,468,777 tickets in Turkish cinemas. 

The film was released on Blu-ray October 2, 2012 in European markets. 

==Reception==
Turkish Prime Minister Recep Tayyip Erdoğan who watched a special advance screening liked the film very much.  Prior to its release, the film caused outrage in Greece, with many accusing it of being racist and obscuring historical facts,    while the Greek Proto Thema newspaper called it "a conquest propaganda by the Turks".   

==See also==

* Fall of Constantinople
* Mehmed the Conqueror

==References==

 

==External links==
*  
*  

 
 
 
 
 
 
 