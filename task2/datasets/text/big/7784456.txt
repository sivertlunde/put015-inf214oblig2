Willy McBean and his Magic Machine
{{Infobox film
| image          =
| image size     =
| caption        =
| director       = Arthur Rankin, Jr.
| producer       = Jules Bass Arthur Rankin, Jr.
| writer         =Anthony Peters  Arthur Rankin, Jr.
| narrator       =
| starring       = Larry D. Mann Billie Mae Richards Paul Soles
| music          = Gene Forrell
| cinematography =
| editing        =
| studio         = Dentsu Motion Pictures Rankin/Bass Productions
| distributor    =
| released       =  
| runtime        = 94 minutes (USA)
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
Willy McBean and his Magic Machine (1965) is a Rankin/Bass full-length stop-motion puppet animation film released in theaters. It was also the first stop-motion animated feature film in the United States. 

Professor Rasputin Von Rotten builds a time machine to travel into the past to all the important accomplishments made in history to take credit for them himself. It is up to Pablo, his talking pet monkey, and boy genius Willy McBean to stop him.

==Plot==
Willy McBean is sick of trying to learn history for school.  Meanwhile an evil scientist (Rasputin Von Rotten) is building a magical time machine so he can go back in time and be the most famous person in history. A monkey (Pablo) climbs through McBeans window, he explains that he escaped from Von Rotten and he tells McBean what he is planning to do. Pablo stole the plans to the time machine. 

McBean builds his own machine to go back in time to stop Von Rotten. The machine isnt working properly. They end up with General George Armstrong Custer, and escape moments before Custer is killed. 
 Wild West, where they meet Buffalo Bill Cody. Von Rotten plans to become the fastest gun in the west. Von Rotten asks Bill for a showdown, but both guns are sabotaged before anyone can be shot. 

Von Rotten moves onto his next target, Christopher Columbus. Once there, Von Rotten, disguised as a Chinese trader, convinces Columbuss crew that they should mutiny. Once more McBean and Pablo stop the evil professor by showing the crew that land is not far off.  
 Great Pyramid, but the duo reach Ancient Rome on the way.  Then they go back to prehistoric times to get a caveman to invent fire and the wheel before Von Rotten.  

As they return to the present, Von Rotten shows the students history through his magic machine (in the form of a movie projector) during history class.

==Voice cast==
*Larry D. Mann as Professor Von Rotten 
*Billie Mae Richards as Willie McBean 
*Paul Soles	as Pablo
*Alfie Scopp as Buffalo Bill Cody	 
*Paul Kligman as Christopher Columbus Bunny Cowan as  Khufu, Ned the Neanderthal
*Paul Soles 
*Peggi Loder (as Pegi Loder)

==External links==
* 

 

 
 
 
 
 
 
 