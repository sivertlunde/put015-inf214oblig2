Spies (1943 film)
{{Infobox film
| name           = Spies
| image          = Private SNAFU.JPG
| image_size     =
| caption        =
| director       = Chuck Jones
| producer       = Leon Schlesinger (producer) Dr. Seuss (supervising producer)
| writer         = Dr. Seuss
| starring       = Mel Blanc
| music          = Carl W. Stalling
| cinematography =
| editing        = Treg Brown
| distributor    =
| released       = August 9, 1943
| runtime        = 3 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Spies is part of the Private Snafu series of animated shorts produced by Warner Bros. during World War II. Released in 1943, the cartoon was directed by Chuck Jones and features the vocal talents of Mel Blanc. It was included as part of the International Spy Museum exhibits, specifically the exhibit showcasing World War II-era spying.

==Plot==
 
Private Snafu has learned a secret, but the enemy is listening and hed better zipper his lip. However, Snafu - little by little - lets his secret slip (by telling the audience, calling his mom, and drunkenly relaying it to a bar girl who works as a Nazi spy): His ship is about to set sail for Africa at 4:30. The information is picked up by spies and quickly relayed to Adolf Hitler, who orders the Nazis to attack the American fleet - which they do, shooting Snafu with torpedoes when he falls in the water. He then ends up in Hell boiling in a cauldron, demanding to know who leaked the secret. Adolf Hitler as well as Hitlers staff then appear as demons and reveal that he gave away the secret he was entrusted to keep, and then showed him a mirror that revealed a horses ass.

A scene in which Private Snafu becomes drunk is musically accompanied by an excerpt from Raymond Scotts composition, Powerhouse (song)|Powerhouse.

==References==
* Friedwald, Will and Jerry Beck. "The Warner Brothers Cartoons." Scarecrow Press Inc., Metuchen, N.J., 1981. ISBN 0-8108-1396-3.

==External links==
* 
* 

 
 
 
 


 