Beautiful Duckling
{{Infobox film
| name               = Beautiful Duckling
| film name      =  
| image              = 
| caption            = 
| director           = Li Hsing 
| producer           = James Shen
| writer             = Chang Yung-hsiang 
| starring           = Tang Pao-Yun, Ko Hsiang-Ting
| music              =  
| cinematography     = Lai Cheng-ying
| editing            =  
| distributor        =  
| released           =  
| runtime            = 119 minutes
| country            = Taiwan
| language           = Chinese
| budget             = 
| gross              = 
}}
Beautiful Duckling is a 1965 Taiwanese film directed by Li Hsing. It tells the story of a duck man Lin Tsai-tien and his adopted daughter Hsiao-yue. Hsiao-yue know nothing about her real descent, which Lin has designedly disguised. Her natural brother Chao-fu is a Taiwanese opera actor, he keep asking Lin for money. He bring hsiao-yue to the theatrical troupe to follow himself, where Hsiao-yue learn about his natural parents. Lin has to sell all his ducks, entrust Hsiao-yue to Chao-fu, and gives him money to let him start his own business. At the end of the film, Lins son comes back, and Chao-fu repents his behavior and returns the money to Lin.

The film is one of the representative works of Healthy Documentary Film in Taiwan, emboding the traditional Chinese ethics.  It received three Golden Horse Awards including wins for Best Picture, Best Director and Best Cinematography. 

==References==
 

==External links==
* 
* 

 

 
 
 
 