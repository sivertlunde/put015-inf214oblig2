Meri Jung
 
 
{{Infobox film
| name           =Meri Jung
| image          = Meri Jung, 1985 Hindi film.jpg
| image_size     = 
| caption        = 
| director       = Subhash Ghai
| producer       = N.N. Sippy
| writer         = Javed Akhtar
| narrator       = 
| starring       = Anil Kapoor Meenakshi Sheshadri Nutan Behl Javed Jaffrey Amrish Puri Parikshat Sahni
| music          = Laxmikant-Pyarelal
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 11 August 1985
| runtime        = 164 mins
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Meri Jung is a 1985 Hindi movie produced by N N Sippy and directed by Subhash Ghai. The film stars Anil Kapoor, Meenakshi Sheshadri, Nutan, Amrish Puri, Javed Jaffrey (in his debut film), A. K. Hangal, Iftekhar, Kushboo and Parikshat Sahni. The films music is by Laxmikant Pyarelal.

==Plot==
The story begins with a happy lower-middle-class family – Arun Verma (Anil Kapoor) an 8-year-old boy, his sister Komal Verma (Kushboo Sundar|Khushboo) a 5-year-old girl, his mother (Nutan) and his father (Girish Karnad) – living a peaceful life. Aruns parents teach their children through the famous title song "Zindagi Har Kadam Ek Nayi Jung Hai, Jeet Jayenge Hum, Jeet Jayenge Hum Tu Agar Sang Hai" which means that life is full of problems at every step and still we will overcome all these if we are together.

Aruns father is trapped in a murder case. A famous criminal lawyer G.D. Thakral (Amrish Puri) proves him guilty, and he is sentenced to be hanged by the court. Pleas by Aruns mother fail to convince Thakral to be truthful and spare her husband. Thakral asks her to give evidence for his innocence, to which she sadly quotes "Jiske paas koi sabut, koi gavanh nahi hote kya we begunah nahin hote" ("one who does not have any proof or witness of innocence, are they not innocent?"). Thakral was adamant to let her husband be hanged. He was hanged as per courts decision. His mother becomes mentally unstable from shock and is sent to a mental institution. Later Advocate Gupta (A.K. Hangal), the lawyer who defended Aruns father, finds out that Thakral had known all along that he was innocent, but only wanted him to be hanged. Arun finds out that Thakral abused the law.

Aruns house and all his property is auctioned off by the court. Arun and his sister are not offered help. He grows up with the seeds of revenge in his heart. He becomes a successful defence lawyer and keenly follows every case of Thakral so that some day he can stand up in court against the unbeaten Thakral and beat him.

One day Geeta Srivatsav (Meenakshi) comes to him and asks him to fight the case of her sister, Dr. Asha Mathur (Beena Banerjee|Bina) who is accused of killing a patient on duty with her medicine, which she gave from her purse. Arun refuses saying that if she can produce even an iota of evidence of her sister being innocent he will fight for her. Geeta replies coincidentally the same sentence "Jiske paas koi sabut, koi gavanh nahi hote kya we begunah nahin hote." Arun is instantly reminded of his mothers pleadings before Thakral. Geeta storms out of his office, but Arun is convinced to take the case. He meets Asha Mathur in police custody. He learns from her that on that fateful night she got a call from her ward assistant that her patient is in the ICU and needs her. She stops on the way (due to a traffic jam), and she impatiently moves out of car her to ask the reason. Meanwhile somebody replaces the medicine bottle with the look-alike bottle of poison from her purse. On arrival in the hospital, she gives the liquid from that bottle to stabilise the patient. This resulted in the patients death.

It is shown that Asha Mathurs husband, Dr. Dinesh Mathur (Parikshat Sahni), had gone to meet Thakral and had asked him to fight his wifes case. Thakral says no claiming he is very busy at the moment but later tells his assistant that there is no way that this case could be won. Arun meets Dinesh Mathur and informs him that he will fight the case. He asked him about the nature of poison and for how long a person can sustain the poison; he learns that poison if might result in death within 2 to 15 minutes depending on the body resistance.

The case begins with the prosecution lawyer recounting the events of that fateful day and telling that the medicine given to the patient was actually poison. Arun defends the case says that the patient did not die of this medicine. To prove himself right he drinks the medicine in court and refutes the claim. The court declares Geetas sister not guilty. Just after the judgement Dr. Mathur rushes Arun to hospital. It is revealed that the medicine contained poison, and Arun is saved in the nick of time.

Geeta calls him on a beach to appreciate his efforts and kindness at the canteen. They fall in love. Thakrals son Vikram Thakral (Javed Jaffrey), a spoilt brat, not knowing anything about Arun, passes bad comments at the couple and teases Geeta, which irritates the couple. Soon things take an ugly turn. He beats Vikram and his friends.

Dr. Mathur wants to appreciate and compensate Arun for the risky effort he took to save his wife. He calls him for a get-together in his mansion. Dr. Mathur gives Arun a blank cheque. Arun rejects the offers politely and says he did for his self-satisfaction. He then finds the same piano with the sticker of Bhagwat Geeta of Krishna which his father used to play and was auctioned. His memories are refreshed again and with the emotional request he tells Dr. Mathur that this piano is very significant to him and asks for it. Dr. Mathur very happily gives the piano and asks him the reason for accepting this small fees. He tells the Mathur family his tragic story. He says that his mother was never seen after his fathers death, and he assumes that she is dead.

Vikram plans to trap Aruns sister Komal and learns she studies in the same college. He tries to impress her by his charms, dancing skills, and ways of flattering people. The plan works. Komal falls for him and he convinces her to run away with him (so that he can publicly defame Arun by leaving his sister). At the planned moment, when one of Vikrams ex-girlfriends shows up and tells Komal of Vikrams evil intentions. In the ensuing argument, Vikram murders his girlfriend which is witnessed by the Dr. Mathurs family.

The Mathurs arrive at Aruns house to narrate the incident. Mrs. Mathur sees a photo of his mother hanging in the wall. She immediately recognises the face with the patient she is handling for mental disorders. Arun and his sister request them to take to her. In the mental hospital they see their mother. Her memories had stopped on the day of the incident; she feels that their children are eight and five, staying with father. Arun brings her mother to his house and tries to make her come back to normal; she resumes her memory on hearing the song "Zingadi Har Kadam..."

Arun had been waiting for this moment to take revenge on Thakral. He steps in as the prosecutor against Thakral who is defending his son. The case moves to and fro and, in the end, Arun has the upper hand. Thakral kidnaps Aruns mother and attempts to blackmail him. Arun goes to save his mother and is beaten up by Thakrals goons. Even though injured, Arun fights back. In an attempt to shoot Arun, Thakral shoots his friends son and is jailed. He is unsuccessful in saving his son, who is given the death penalty. Thakral becomes a mentally unstable person.

Arun and his family are happy again and sing the famous "Zindagi Har Kadam Ek Nayi Jung Hai" song.

==Cast==
{|class="wikitable"
|-
! Actor/Actress !! Character
|- Anil Kapoor Arun Verma
|- Girish Karnad Deepak Verma
|- Kushboo Sundar|Khushboo Komal
|- Nutan
|Arun and Komals mother
|- Meenakshi Sheshadri Geeta Mathur
|- Amrish Puri
|G. D. Thakral
|- Javed Jaffrey Vikram Thakral (Vicky)
|-
|A. K. Hangal Advocate Gupta
|-
|- Viju Khote as Arun Verma Assistant
|- Beena Banerjee
|Dr. Asha Mathur
|- Parikshat Sahni
|Dr. Dinesh Mathur
|- Pradeep Rawat Pradeep Rawat Government Advocate
|}

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="centr"
! # !! Title !! Singer(s)
|-
| 1
| "Zindagi Har Kadam" 	
| Lata Mangeshkar, Nitin Mukesh
|-
| 2
| "Zindagi Har Kadam Ek Nai Jung Hai – 1"
| Lata Mangeshkar, Shabbir Kumar
|-
| 3
| "Bol Baby Bol Rock-N-Roll"
| Kishore Kumar, Javed Jaffrey, S. Janaki
|-
| 4
| "O Mere Khwabon Ke Shehzade"
| Anuradha Paudwal
|-
| 5
| "Zindagi Har Kadam Ek Nai Jung Hai – 2"
| Lata Mangeshkar
|-
| 6
| "Jhoom Le Jhoom Le"
| Laxmikant Kudalkar, Subhash Ghai
|}

==Remakes==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="centr"
! Year !! Title !! Language !! Cast
|-
| 1986
| Vijrumbhana
| Telugu
| Shoban Babu
|-
| 1987
| Oru Thayin Sabhatham
| Tamil
| T.Rajendar
|-
| 1989
| Yuddha Kanda
| Kannada
| V Ravichandran
|-
|}

==Filmfare Awards==

===Won===
*Filmfare Best Supporting Actress Award for Nutan
*Filmfare Best Supporting Actor Award for Amrish Puri

===Nominations===
*Filmfare Award for Best Film – Subhash Ghai
*Filmfare Award for Best Actor – Anil Kapoor
*Filmfare Award for Best Music – Laxmikant Pyarelal
*Filmfare Award for Best Lyrics – Anand Bakshi (Zindagi Har Kadam)

==Reception==
The movie was critically acclaimed, with special praise heaped upon Amrish Puri and Anil Kapoor. The movie established Kapoor as a mature actor with a lot of talent, shedding his newcomer image.

==External links==
*  

 

 
 
 
 
 
 
 