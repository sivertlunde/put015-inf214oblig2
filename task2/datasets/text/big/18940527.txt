Idol on Parade
{{Infobox film
| name           = Idol on Parade
| image          = 
| caption        = 
| director       = John Gilling 
| producer       = 
| screenplay     = 
| starring       = Anthony Newley
| music          = 
| cinematography = 
| editing        = 
| studio         = Warwick Films
| distributor    = Columbia Pictures
| released       = 1959
| runtime        = 
| country        = United Kingdom
| language       = English
| gross = 
}}
Idol on Parade also known as Idle on Parade is a 1959 youth-oriented British comedy film directed by John Gilling and starring Anthony Newly, Sid James and Lionel Jeffries. It was John Antrobus first screenplay.   p.44     The film was based on the 1958 novel Idle on Parade by William Camp that was inspired by Elvis Presleys consription into the US Army.

The film was produced by Warwick Films and featured Newley singing five songs in a Cockney accent for the film.

One of the songs "Ive Waited for So Long" became a pop hit. 

==Cast==
* William Bendix as Sergeant Major Lush
* Anthony Newley as Jeep Jackson
* Anne Aubrey as Caroline
* Lionel Jeffries as Bertie
* Sid James as Herbie David Lodge as Shorty
* Dilys Laye as Renee William Kendall as Commanding Officer
* Bernie Winters as Joseph Jackson
* Harry Fowler as Ron
* Norman Atkyns as Stage manager Percy Herbert as Sergeant Hebrides

==References==
 

==External links==
* 
 

 
 
 
 
 
 
 
 


 
 