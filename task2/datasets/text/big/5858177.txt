Pathfinder (2007 film)
{{Infobox film
| name           = Pathfinder
| image          = Pathfinder Poster.png
| caption        = Promotional poster
| director       = Marcus Nispel
| producer       = {{plainlist|
* Mike Medavoy
* Arnold W. Messer
* Marcus Nispel
}}
| writer         = Laeta Kalogridis
| starring       = {{plainlist|
* Karl Urban
* Moon Bloodgood
* Russell Means
* Ralf Möller
* Clancy Brown
}}
| music          = Jonathan Elias Daniel Pearl
| editing        = {{plainlist|
* Jay Friedkin
* Glen Scantlebury
}}
| studio         = Phoenix Pictures
| distributor    = 20th Century Fox
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English  Old Norse 
| budget         = $45 million    
| gross          = $30.9 million 
}} of the same name although the geographic setting and peoples involved are very different.
 Native Americans across the in search of colonization.

Pathfinder received a widely negative critical reception upon release and was not successful at the box office, although the film did enjoy much better home video sales. It was also adapted into a graphic novel by Dark Horse Comics.

==Plot==
  Dark Ages, a raiding party of Vikings led by an unnamed "great Viking warrior" arrived in the Americas with the intention of plunder and colonizing the "cursed" land. In so doing they plan on slaughtering the local Native American tribe. The "Thule people|Thule", or "Skræling" as the Vikings call them, are considered inferior, primitive and true savages, and the Vikings wish to "cleanse" the land before they settle there. While slaughtering native villagers, the Viking leader orders his son, only a twelve-year-old boy, to join in the carnage and the boy refuses. In return the boy is beaten and whipped.

After their longship is wrecked, the Vikings are caught unawares and attacked by another native tribe, the Wampanoag people|Wampanoag, who call themselves the "People of The Dawn", and are massacred themselves. The sole survivor is the Viking leaders traumatized blond-haired son, who is discovered in the wreckage among the frozen corpses of slaves by a native woman who adopts him as her own, after seeing a white horse by the wreck there is a prophecy that a "creature swift of foot and white as snow" would bring about a time of great change. The boy is named "Ghost" for his paleness, and many of the tribe find it hard to accept him because of his similarity to the "demons who have never seen sunlight".

Fifteen years later, Ghost (Karl Urban) still lives among them in relative happiness, yet remains tormented by his dreams which, along with his different appearance to the other tribesmen, interfere with his ability to be fully accepted into the native community. Ghost is frustrated that the others do not trust him, and despairs that he will never be considered a Brave of The People. He also has feelings for a young woman from an allied neighbouring tribe named Starfire (Moon Bloodgood), the daughter of the Pathfinder (Russell Means), an elderly chieftain searching for a worthy successor after the next intended Pathfinder has been killed in an avalanche. Ghost is the only person capable of using a sword as he is still in possession of his fathers Viking sword that had been left behind with him,  and he often trains with it; the native tribe not having yet developed metalworking.

While hunting and gathering with the group, Ghosts little sister wanders off and encounters a scouting party for a new group of Viking raiders. She only just manages to escape back to warn her family, but to no avail as the Vikings were able to follow her tracks and viciously attack. They raze the village, murdering everyone with delight, except a few tribesmen whom they want to combat individually in "duels". Ghost arrives at the village only just in time to see his adoptive father brutally killed by Gunnar (Clancy Brown), the Viking leader. The Vikings are bemused as to Ghosts heritage, yet decide that since hes in possession of a sword he may present more of a challenge, and so make him duel nonetheless. Ghosts opponent, Ulfar (Ralf Möller), is taken unawares by Ghosts abilities, and Ghost maims him by cutting out his eye before escaping.

Injured by an arrow during the pursuit, Ghost flees by riding his shield down a snowy mountainside, all the while being pursued by sledding Vikings he only just manages to fight off. He then hides in a cave, where he is found by the allied tribes hunting party. He attempts to warn them but is overcome by his wounds, passing out, just as a bear inhabiting the cave attacks them; which Pathfinder is cleverly able to dispatch with. They bring him home and Pathfinder and his daughter heal his wound. From the arrow head removed from his wound, Pathfinder is able to discern they are under attack by the "Dragon People", which is what they call the Vikings. The warriors discuss taking the initiative against the Viking invaders; however, Ghost informs them of their savagery and ferocity, and how attacking them would only lead to their death. He warns them that their wood and stone weapons are no match for the Vikings metal armour and blades. Ghost advises the villagers that their only chance of survival is to flee, and he urges them to do so immediately, then he departs to take on the Vikings alone.

He finds that he has been covertly followed by Jester, a mute admirer, who refuses to leave his side no matter how many times told to go back. In an abandoned village they set a series of traps and manage to kill a few of the Vikings. Starfire, meanwhile, whos been in love with Ghost, chooses to leave the tribe instead and absconds to accompany him. The three pick the Vikings off individually, stealing their armour and weapons to better protect themselves. Pathfinder goes after his daughter, finds Ghost and joins the fight. The hunting party of the tribesmen that had also decided to help fight them, not aware of Ghosts plan accidentally set off one of his camouflaged spiked pit traps themselves, and are then massacred by the Vikings, much to Ghosts horror and guilt. Eventually, both Jester and Pathfinder are executed brutally, and Ghost and Starfire are captured. Gunnar recognizes Ghost as the son of a Viking, and tells Ghost that he knew of his father and admired him as a great warrior, then taunts him for not even knowing his own kind. They then threaten to torture Starfire if Ghost will not betray the location of the other villages. Just as Starfires hands are about to be chopped off, Ghost finally agrees to tell them what they want to know.

Having gained some of the Vikings trust by his cooperation, Ghost leads them instead to an iced-over lake hed encountered previously, believing that since Vikings know only of coldest winter they would not expect to fall through the spring thawing ice. The trap does work but not as efficiently as hed hoped, only drowning some of the Vikings instead of all. Next he leads them along a dangerous pass through the Appalachian Mountains and convinces Gunnar that the Vikings must be tied together to reduce the risk of members falling off the narrow ledge on the high cliff. Using a sling, Ghost then creates a domino effect so that the entire string of Vikings falls over the cliff: only Gunnar cuts himself free in time, coldly killing his lieutenant Ulfar in the process. Left dangling over the edge himself, Ghost convinces Starfire to leave and then triggers an avalanche, that still fails to finish off hardy villain Gunnar.
 Viking beliefs), and pleads with Ghost for such a mercy by saying he is the last of his kind in his miserable land. Ghost replies "You are not my kind", and instead of granting his wish, he cuts off the necklace, sending Gunnar plummeting far below to an ignominious demise.

Ghost returns to Starfire with Pathfinders necklace, thus making Starfire the new Pathfinder after her father. A while later we see Starfire has given birth to a blond-haired son. Ghost, having accepted both sides of himself, and now finally respected as the bravest of the tribe and one of their own, assumes his position watching over the coast in case the Vikings ever return.

==Cast==
*Karl Urban Ghost, the protagonist of the story and the son of a Viking raider, who was stranded in the Americas as a child. Raised by the Native American tribesmen, whom he sees as his adopted family, he is torn between two worlds. He is a great warrior and swordsman, and finds himself falling for Starfire.
*Moon Bloodgood Starfire, the Pathfinders beautiful, headstrong daughter, who also inherits his wisdom and faith in Ghost.
*Clancy Brown Gunnar, the antagonist of the film and a cruel and barbaric leader of a second party of Viking marauders. He wishes to "cleanse" the land of the Native Americans who he sees as "not even human", so that he and the rest of his people can settle there. Gunnar engages in a psychological as well as physical struggle with Ghost.
*Ralf Möller Ulfar, lieutenant of the Viking marauders and Gunnars right-hand-man. Loses an eye in a battle with Ghost.
*Russell Means Pathfinder, an elderly and wise Native American tribal chieftain who is searching for a worthy successor. "Pathfinder" is more than a name, it is a title, which is akin to something of a protective/guiding role to the tribe.
*Nathaniel Arcand Wind In Tree, Pathfinders son who believes in the prophecy that Ghost will deliver his people; he nonetheless follows his fathers orders without question and so does not fight, instead attempting to lead his people to the safety of the next village.
*Jay Tavare Blackwing, a warrior and Brave of Pathfinders tribe who is also in love with Starfire and something of a rival to her other suitor, Ghost. Blackwing also mistrusts Ghost due to his Viking ancestry. mute who is seen as something of a joke amongst the other tribesmen. He idolizes Ghost and is the first tribesmen to join Ghost in his solitary quest to kill the Vikings, proving his worth.

==Graphic novel==
The film was also adapted into a graphic novel by Dark Horse Comics.       The graphic novel was built around dialogue written by the film screenwriter Laeta Kalogridis and with art by comic book artist Christopher Shy, and which was subsequently published by Dark Horse Comics at the same time as the film.   From the beginning the graphic novel had a symbiotic relationship with the film. Film director Marcus Nispel, also a graphic novelist, decided to adapt the screenplay into a comic book format to appeal to his target audience more and help get a fan base to get his film made. However, his film got the green light before the graphic novel could be completed. 

==Reception==
The film received mainly negative reviews, with an 11% approval rating on Rotten Tomatoes based on over 60 reviews,  and a score of 29 on Metacritic based on 23 reviews, indicating "generally unfavorable reviews". 

A review on the   will be in Valhalla; everyone else should find a different path."   Peter Debruge of Variety (magazine)|Variety wrote, "This latest bit of historical balder-dash stands in direct defiance of proven action-movie formulas, trusting its brutal concept and striking visuals to overcome a lack of star power."   Manohla Dargis of The New York Times wrote, "All grunting, all goring, the witless action flick "Pathfinder" has little to recommend it".   Michael Ordona of the Los Angeles Times called it unintentionally funny,  and Mick LaSalle of the San Francisco Chronicle called it a failed attempt to make an art-house film out of a concept better suited to an exploitation film. 

There were however positive reviews; one example is Kirk Honeycutt of  ." 

==Box office==
Pathfinder opened theatrically in the United States on 13 April 2007, in sixth place on its opening weekend and had some big competition at the box office with 300 (film)|300, Blades of Glory, and Disturbia (film)|Disturbia amongst others, and although Pathfinder earned over $5 million in its opening weekend at the box office,  this quickly tailed off. Overall, worldwide the film earned just over $30 million at the box office, failing to recoup its $45 million budget, though it eventually made back its money through DVD sales. 

==Home video releases==

===DVD=== Starship Troopers, and as The Kurgan in Highlander (film)|Highlander, as well as others.

===Blu-ray===
The Blu-ray release went on sale on 20 November 2007. 

===Unrated version===
For the theatrical release and the initial DVD release, director Marcus Nispel had been forced to cut the gore and digitally remove some of the extreme violence out of at least 32 scenes, and also a scene of Ghost and Starfire making love in a cave, so that the film could gain an R rating from the MPAA. Nispel was also forced to trim down 23 scenes (including significant plot development) for reasons of time and pacing. In total around ten minutes were cut out of the film. These cuts were restored however, as well as the gore, for the unrated version, which was released on 27 August 2007. 

==Footnotes==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 