Unfinished Symphony (film)
 
{{Infobox film
| name           = Unfinished Symphony
| image          = 
| image_size     =
| caption        = 
| director       = Anthony Asquith Willi Forst
| producer       = 
| writer         = Willi Forst Benn W. Levy Walter Reisch
| starring       = Mártha Eggerth Helen Chandler Hans Jaray Ronald Squire
| music          =
| cinematography = Franz Planer
| editing        =
| studio         = Gaumont British Cine-Allianz Tonfilmproduktion
| distributor    = Gaumont British (UK) Fox Film Corporation (US) 
| released       = 23 August 1934 (UK) 11 January 1935 (US)
| runtime        = 84 minutes
| country        = United Kingdom Austria
| language       =
}} musical drama film directed by Anthony Asquith and starring Mártha Eggerth, Helen Chandler, Hans Jaray, and Ronald Squire.  The film is based on the story of Franz Schubert who, in the 1820s left his symphony unfinished after losing the love of his life.  The films alternate German-language version was called Gently My Songs Entreat.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 