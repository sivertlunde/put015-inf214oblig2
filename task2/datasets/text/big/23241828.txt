Stealth Fighter
 
{{Infobox film
| name           = Stealth Fighter
| image          = stealth-fighter.jpg
| caption        = DVD cover
| director       = Jim Wynorski
| producer       = Jim Wynorski (as Jay Andrews)
| writer         = Lenny Juliano T.L. Lankford John A. Sanders
| narrator       = William Sadler Ernie Hudson Andrew Divoff
| music          = Alex Wilkinson
| cinematography = J.E. Bash
| editing        = Vanick Moradian
| distributor    =
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
}} William Sadler, Ernie Hudson, and Andrew Divoff.

==Film synopsis== pilot who decides to fake his own death. After this, he ends up employed by a Latin American arms dealer. Turner steals a stealth fighter from a US Air Force base in the Philippines and uses it to target military installations around the world. However, a naval reserve officer is recruited to infiltrate Turners group and stop him.

==Cast==
* Ice-T as Owen Turner
* Costas Mandylor as Ryan Mitchell
* Erika Eleniak as Erin Mitchell
* Sarah Dampf as J.P. Mitchell William Sadler as Admiral Frank Peterson
* Ernie Hudson as President Westwood
* Andrew Divoff as Roberto Menendez

==Release==
The film was first released on TV in Spain in September 1999. On February 15, 2000, the film premiered on video in the United States. Later on that year, the film premiered in Iceland and Germany.

==External links==
* 
*  at Rotten Tomatoes

 
 
 
 
 
 
 


 
 