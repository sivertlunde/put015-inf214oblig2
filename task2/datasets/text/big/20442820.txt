Like You Know It All
{{Infobox film
| name           = Like You Know It All
| image          = Like You Know It All film poster.jpg
| alt            = 
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         =  
| rr             = Jal Aljido Mot-hamyeonseo
| mr             = Chal Aljido Mot-hamyŏnsŏ}}
| director       = Hong Sang-soo
| producer       = Hong Yi-yeon-jeong
| writer         = Hong Sang-soo Kim Tae-woo Uhm Ji-won Go Hyun-jung
| music          = Jeong Yong-jin
| cinematography = Kim Hoon-kwang
| editing        = Hahm Seong-won
| studio         = Jeonwonsa Films
| distributor    = Sponge Entertainment
| released       =  
| runtime        = 126 minutes
| country        = South Korea
| language       = Korean
| budget         = $100,000
| gross          = $208,867 
}}
Like You Know It All ( ; lit. "You Think You Know It All But You Dont") is a 2009 South Korean comedy-drama film written and directed by Hong Sang-soo. 

== Plot == Arthouse filmmaker Goo cant seem to direct a hit, but at least the critics love him. He goes to Jecheon, North Chungcheong Province to judge the local film festival, but the common practice for jurors is to schmooze by day, drink at night, and sleep through movies. He bumps into an old friend Boo Sang-yong in town and drinks till he passes out, but not before soundly offending his friends wife.

After Jecheon, Goo heads to Jeju Island to give a college lecture. There he meets up with a former mentor, who it turns out is now married to Goos ex-unrequited lover. DSa, Nigel (13 August 2008).  . Korean Film Council. Retrieved 2008-11-08.  Moon Seok. "Return of the Masters".  , pp. 16. Korean Film Council, 2008. Retrieved 2008-11-08.  

== Cast == Kim Tae-woo as Goo Kyeong-nam, a film director

=== In Jecheon ===
* Uhm Ji-won as Gong Hyeon-hee, a programmer of the film festival
* Gong Hyung-jin as Boo Sang-yong, Kyeong-nams ex-business partner Jung Yu-mi as Yoo Shin, Sang-yongs wife
* Seo Yeong-hwa as Oh Jin-sook, an actress and jury member for the film festival
* Eun Joo-hee as Oh Jeong-hee, a porn actress
* Kim Yeon-soo as a box office hitting film director
* Michael Rodgers as film critic Robert

=== In Jeju ===
* Go Hyun-jung as Go Soon, Cheon-soos wife and Goos ex-unrequited lover.
* Moon Chang-gil as Yang Cheon-soo, a painter and Kyeong-nams college senior
* Ha Jung-woo as a sculptor and Cheon-soos neighbor
* Yoo Jun-sang as Mr. Go, a chief officer of Jeju Film Commission
* Go Chang-gyoon as an officer of Jeju Film Commission

== Production == HD video Night and Day, released in 2008.  The film was produced independently with a short shooting period and low budget of United States dollar|$100,000, with the cast — some of whom have appeared in Hongs earlier films — working without any fees upfront.   Shooting commenced in Jecheon, August 2008, where the real-life Jecheon International Music & Film Festival is held annually, before moving to Jeju Island where filming continued into September. 

A member of the main production staff said, "the film steps away from the sharp beauty of cinematic form, trying to make some changes while being more loose in form but more dynamic
in atmosphere. It is more hilarious but also bitter at the same time." 

YesAsia said, "Hongs characteristic episodic narrative and elliptical reflection provide the brooding framework for a bitingly funny send-up of filmmakers, festivals, and the people and places in between."

== References ==
 

== External links ==
*    
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 