Dombey and Son (film)
{{Infobox film
| name           = Dombey and Son
| image          =
| caption        =
| director       = Maurice Elvey
| producer       = Fred Paul   Maurice Elvey
| writer         = Charles Dickens (novel)   Eliot Stannard Douglas Munro
| music          = 
| cinematography = 
| editing        = 
| studio         = Ideal Film Company
| distributor    = Ideal Film Company
| released       =  
| runtime        = 60 minutes
| country        = United Kingdom 
| language       = English
| budget         =
}} silent drama film directed by Maurice Elvey and starring Norman McKinnel, Lilian Braithwaite and Hayford Hobbs.  It is an adaptation of the novel Dombey and Son by Charles Dickens.

==Cast==
* Norman McKinnel - Paul Dombey
* Lilian Braithwaite - Edith Dombey
* Hayford Hobbs - Walter Dombey
* Mary Odette - Florence Dombey Douglas Munro - Solomon Gillis
* Jerrold Robertshaw - Carker
* Fewlass Llewellyn - Bagstock
* Will Corrie - Captain Scuttle
* Evelyn Walsh Hall - Mrs. Skenton

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 