Bommarillu
{{Infobox film
| name = Bommarillu
| image = Bommarillu-Movie-poster.jpg
| caption = Theatrical release poster Bhaskar
| producer = Dil Raju
| writer = Bhaskar Abburi Ravi
| starring = Siddharth Narayan Genelia DSouza Prakash Raj Kota Srinivasa Rao Jayasudha
| music = Devi Sri Prasad
| cinematography = Vijay C Chakravarthy
| editing = Marthand K. Venkatesh
| studio = Sri Venkateswara Creations
| distributor = Sri Venkateswara Creations
| released =  
| runtime = 168 minutes
| country = India
| language = Telugu
| budget =  6 crores 
| gross =  25 crores 
}} Telugu romantic Tamil as Bengali as Bhalobasa Bhalobasa Oriya as Dream Girl Hindi as Its My Life (film)|Its My Life (2013).
 South Filmfare Awards among other prominent awards. The films success broke several records at the box office during its prime and is one of the highest grossing Telugu films.

==Plot==
The film begins with a baby taking his first steps on a beach supported by his father. The narrator (Murali Mohan) states that, it is right for a father to support his child in his infancy, but questions whether the father should continue to hold the childs hand even after he is 24 years old. As the credits roll, a visibly angry Siddharth Siddhu Addala (Siddharth Narayan) begins verbally abusing all the fathers in the world. When queried about his disgust, he says that his father, Aravind (Prakash Raj), gives him more than what he asks for. He cites instances where his choices of dressing, hairstyle, and many others are overruled by his fathers. However, he vows that his career and the woman he marries will be his own choice.

Satti (Sunil (actor)|Sunil), Addala households dutiful servant wakes up Siddhu in the morning. In the background, Siddhus mother, Lakshmi (Jayasudha), is seen singing a devotional Telugu song while cooking. At the dining table, Aravind, Managing Director of their construction company, asks if Siddhu will join their office. When Siddhu deliberates, his father becomes impatient and plans for his marriage despite Siddhus silent protest. The next week, Siddhu returns home to realize that he is going to get engaged to Subbulakshmi (Neha Bamb|Neha) against his wishes. He speaks with her only to realize that she is a daddys girl (Tanikella Bharani being the father) and does not take a liking to her. However, with Aravinds final say, they eventually get engaged.

While contemplating on his options in a temple, Siddhu accidentally meets Hasini (Genelia DSouza), an engineering student. Siddhu is attracted by her cheerful nature and energy, and the couple begin to meet on a regular basis. As the days go by, Siddhu grows to admire the ever-friendly Hasini as someone who does what she loves, and he discovers many small things which make him happy to be in her company; he realizes that he has fallen in love with her.

Alongside this, Siddhu applies for a bank loan to start his construction company. When his love for Hasini deepens, he wishes to propose to her. He confesses to her that he is engaged to Subbulakshmi against his wishes, but who he really wants is her. On learning of his engagement, Hasini gets dejected, but comes back a few days later and asks him to do what he wishes for and accepts his proposal. At this juncture, the ecstatic Siddhu is seen by a furious Aravind. Siddhu is admonished back home and he expresses his disinterest in marrying Subbulakshmi. When asked for his reason to like Hasini, Siddhu replies that if Hasini can stay with their family for a week, then all their questions shall be answered. He convinces Hasini to stay at his house after lying to her father, Kanaka Rao (Kota Srinivasa Rao) that she is going on a college tour.

When Hasini is introduced to Siddhus family, she gets a lukewarm welcome. As she settles down in the house, the other family members begin to like her. Even though getting used to the living habits of the authoritarian Aravinds household is difficult, Hasini stays for Siddhus sake. In the meanwhile, Aravind reprimands Siddhu when he finds out about his bank loan and his career plans, only to further enrage Siddhu.

One day, the entire family along with Hasini attends a marriage ceremony. A cheerful Hasini cheers up the ceremony with her playful nature. Coincidentally, Kanaka Rao who happens to be around, recognizes Siddhu as the drunken young man whom he encountered on an earlier occasion. Hasini realizes her fathers presence and quickly exits to avoid his attention. After saving their face, Siddhu admonishes Hasini for her antics at the marriage. A sad and angry Hasini moves out of the house saying that she does not find Siddhu the same and that she cannot put on an act if she stays in their house. After getting back to her house, she rebuilds the trust her father has in her while Siddhu is left forlorn. Lakshmi confronts Aravind on Siddhus choices and wants. In the process, Siddhu opens up his heart, leaving Aravind to repent on his over-protectiveness. Siddhu requests Subbulakshmi and her parents to call off the marriage. While they relent, Aravind manages to convince Kanaka Rao about Siddhu and Hasinis love. When Kanaka Rao disagrees to get the two lovers married, Aravid suggests letting Siddhu stay with them for a week and the story returns to the pre-credits scene. The viewers are left to assume that the two lovers have a happy union.

==Cast==
* Siddharth Narayan as Siddharth "Siddu" Addala
* Genelia DSouza as Hasini
* Prakash Raj as Aravind (Siddus father)
* Kota Srinivasa Rao as Kanaka Rao (Hasinis father)
* Jayasudha as Lakshmi
* Satya Krishnan as Siddus sister-in-law
* Sudeepa Pinky as Siddus sister Sunil as Satti
* Neha Bamb as Subbalaxmi
* Tanikella Bharani as Subbulaxmis father Ravi Varma as Ravi
* Dharmavarapu Subramanyam as Kismat Kumar
* Brahmanandam as loan officer

==Production==

===Development=== Bhaskar assisted Telugu films such as Arya (2004 film)|Arya (2004) and Bhadra (2005 film)|Bhadra (2005).

On the sets of the film Arya, Raju offered Bhaskar a film to direct. On the sets of Bhadra, Bhaskar narrated the story to Raju and the saga began.    Thus, Bommarillu became the first directorial venture for Bhaskar.  In an interview, he said that the story for the film began taking shape in as early as 1997 when he wrote about a father and a sons relationship. However, when the plans of making the film arose, an element of love between the protagonists was added. In the interview, he said that the script, to an extent, is autobiographical.  He cites personal examples of some scenes from the film such as the choice of clothes for Siddhu by Aravind, the head-bump between the lead actors and Lakshmi singing in the kitchen.   
 cinematographer for the film, said that Dil Raju offered him the position in November 2005. For the film, Vijay said that he made use of Arriflex 435 camera and Hawk lenses.  In another interview, Bhaskar said working with Abburi Ravi, his co-writer, was unique. They used to converse in a closed room with a voice recorder, allowing the dialogues in the script to be natural.  He also heaped praise on Marthand K. Venkatesh, the films Film editing|editor. After filming, the length of the film reel came to   which amounted to a runtime of 3 hours and 15 minutes. The presence of Marthand brought this down to  . This meant a reduction in the runtime by 25 minutes. 

===Casting, location and music=== Siddharth was Genelia was Boys (2003), made them more comfortable to work with. The camaraderie that the lead actors shared during the filming, added to their good performances.   The choice of Prakash Raj was easy as he befitted the character he portrayed while, Jayasudha was persuaded to play the role of the lead actors mother. 

The palatial house where the entire family stayed in the film is part of Ramanaidu Studios at Nanakramguda, Hyderabad, India|Hyderabad. Several modifications were done by the art director, Prakash.  A couple of the songs were shot in a montage, another couple in Frankfurt am Main and other places in Germany and one song each in this house set and at a temple in Kakinada. 

For the films music and soundtrack, Raju renewed his previous association (Arya and Bhadra) with Devi Sri Prasad.  Savitha Reddy rendered the voice for Genelias character in the film.  A feature of this film is Siddharth singing one of the tracks from the film.  

==Release==

===Reception===
Bommarillu was released worldwide with 72 prints. Owing to the success of the film, the number of reels grew to about hundred. {{cite web|work=Idlebrain.com |accessdate=2007-10-16 |title=Trade Story: Bommarillu rocks
|url=http://www.idlebrain.com/trade/records/bommarillu.html}}  It collected a distributors share of  5 crore in its opening week in India.  Released in six major metros in the United States, the film collected $73,200 (then approximately  0.3 crore) within the first four days of screening.  A September 2006 survey done in the United States by a popular entertainment portal revealed that the film was watched by an Indian expatriate population of 65,000, which generated a revenue of  3 crore at that time.  A cumulative gross revenue for the film was reported to be as  25 crore including  3.5 crore from overseas, the largest for any Telugu film at that time.   

===Critical acclaim, controversies, awards and remakes===
The film received rave reviews right for the story and the performances. One entertainment portal has given a rating of 4.5/5 tagging the films review with Picture Perfect.  Another such portal suggests the film to the entire family. It goes on to applaud Siddharth Narayan, Genelia DSouza and Prakash Raj, the three prime actors from the film for their performances.  Similar reviews were voiced out by other such portals, many of which pointing out no real flaws from the film.   
HOURDOSE reviewed it by stating "‘Bommarillu’ is a pure example of a director’s faith in his script and his screenplay. All the wonderful performances of the lead cast and some nice talent from the crew made the film a grand success. This film is surely one of the most well-written and innovative films that Tollywood has produced in recent years."  
 Telugu film industry by quoting the cheap prices at which the film was being sold.  The films lead actor, Siddharth even went on to request the audiences to buy the original audio CD.  The films producer, Dil Raju, ensured a special code on each distributed print to track piracy with a warning for copyright violation which would incur a fine or a jail term. 

In April 2007, a case of copyright infringement was filed on the films producer and director that prompted a court to stall the screening of the film. The allegation pointed out that the film was made based on a compilation of short stories that was released in 1997. 
 Golden Nandi Debut director award for Best Actor Special jury awards respectively while Savitha Reddy won the Nandi Award for Best Female Dubbing Artist for her work.  At the 2007 Filmfare Awards South, the film won awards for the Best Film, Best Actress and Best Music Director.    
 Bengali and Oriya languages Bhalobasa Bhalobasa Dream Girl,  respectively, in 2008 and 2009.

==Home media==
The DVD version of the film was released on 4 June 2007    Dolby Digital progressive 24 Frame rate|FPS, widescreen and NTSC format.  

==Awards==
;Nandi Awards  Best Film (Gold Nandi) - Dil Raju Best Supporting Actor - Prakash Raj Best First Bhaskar
* Best Screenplay - Bhaskar Best Dialogue Writer - Abburi Ravi Best Female Dubbing Artist - Savitha Reddy Special Jury - Genelia DSouza

;Filmfare Awards South  Best Film&nbsp;– Telugu - Dil Raju Best Actress&nbsp;– Telugu - Genelia DSouza Best Music Director&nbsp;– Telugu - Devi Sri Prasad

==Soundtrack==
The film has seven songs composed by Devi Sri Prasad with the lyrics primarily penned by Chandrabose (lyricist)|Chandrabose, Ravi Kumar Bhaskarabhatla, Kulasekhar and Sirivennela Sitaramasastri.  The audio of the film released nationwide on 18 July 2006.  A repository of Indian songs has recommended the feel-good soundtracks to the audiences. 

{{Track listing
| extra_column = Singer(s) Ranjith & Andrea | length1 = 5:06
| title2 = Bommani Geesthe | extra2 = Gopika Poornima & Jeans Srinivas | length2 = 3:54
| title3 = Kaani Ippudu | extra3 = Devi Sri Prasad | length3 = 5:12
| title4 = Music Bit | extra4 = Sumangali | length4 = 0:45
| title5 = Laloo Darwaja | extra5 = Naveen, Murali, and Priya Prakash | length5 = 4:50
| title6 = Nammaka Thappani | extra6 = Sagar & Sumangali | length6 = 4:30
| title7 = Appudo Ippudo | extra7 = Siddharth Narayan | length7 = 4:02
}}

==Remakes==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC" align="center" Bhalobasa Bhalobasa Dream Girl (2009) (Oriya language|Oriya) || Its My Life (film)|Its My Life (2014) (Hindi)
|- Siddharth Narayan || Jayam Ravi || Hiran Chatterjee || Sabyasachi Misra || Harman Baweja
|- Genelia DSouza Srabanti Malakar || Priya Chowdhury || Genelia DSouza
|- Prakash Raj Nana Patekar
|- Kota Srinivasa Rao || Sayaji Shinde || Sushanta Dutta || Mihir Das ||
|- Jayasudha || Geetha || Laboni Sarkar || Aparajita Mohanty ||
|}

==References==
 

==External links==
*  

 
 
{{succession box
| title=Filmfare Best Film Award (Telugu)
| years=2006
| before=Nuvvostanante Nenoddantana Happy Days}}
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 