Cover Story (2000 film)
 
{{Infobox film
| name = Cover Story
| image = Cover Story.jpg
| image_size =
| alt =
| caption =
| director = G. S. Vijayan
| producer = Revathi Sureshkumar
| writer = B. Unnikrishnan
| narrator = Tabu  Biju Menon
| music = Sharreth
| cinematography =
| editing =
| studio =
| distributor =
| released = 2000
| runtime =
| country = India
| language = Malayalam
| budget =
| gross =
| preceded_by =
| followed_by =
}}
Cover Story is a 2000 Malayalam film starring Suresh Gopi and Tabu (actress)|Tabu.

==Plot==
Jasmine Khan (Tabu (actress)|Tabu) is the next door neighbour of Chandrashekar (Nedumudi Venu), a retired judge with a haunted past. The two become friends. Jasmine, a computer engineer, has to wear high power contact lenses because of her vision problems. She meets Vijay, a news reporter and the executive director of True Vision.

Chandrashekar is killed by a mysterious man, who is seen but not recognised by Jasmine, as she was not wearing contact lenses at that time. A corrupt policeman thinks that Jasmine is the killer. Eventually, she discovers that Vijay (Suresh Gopi) is actually the killer of Mohan, who also killed Chandashekar. The search for the real killer, and the police attempting to catch him, forms the rest of the story.

==Cast==
* Suresh Gopi ... Vijay
* Biju Menon ... ACP Anand S. Nair Tabu ... Jasmine Khan
* Nedumudi Venu ... Chandrashekara Menon Siddique
* Rajan P. Dev  Augustine
* T. P. Madhavan
* Rizabawa
* Sreekumar ... Sachidanandan
* N. F. Varghese

==External links==
*  

 
 
 
 


 
 

 