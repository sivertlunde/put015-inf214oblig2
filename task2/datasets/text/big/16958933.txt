Inferno (1999 film)
{{Infobox film
| name           = Inferno
| image          = Inferno.MoviePoster.png
| caption        = Official movie poster
| director       = John G. Avildsen
| producer       = {{plainlist|
* Lawrence Levy
* Evzen Kolar
}}
| writer         = Tom ORourke
| starring       = {{plainlist|
* Jean-Claude Van Damme
* Danny Trejo
* Pat Morita
}}
| music          = Bill Conti
| cinematography = Ross A. Maehl
| editing        = J. Douglas Seelig Columbia TriStar Home Video
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         =
}}
Inferno (released in the United Kingdom as Desert Heat) is a 1999 American action film directed by John G. Avildsen, and starring Jean-Claude Van Damme, Danny Trejo and Pat Morita.
 Van Damme) is a veteran soldier sick of life, wandering the desert looking for a reason to die. An incident with a few thugs from the nearby town who steal Eddies motorbike and beat him almost to death, starts in Eddie a flame for revenge. Helped by Mr. Early, and some other good fellows, Eddie is out for vengeance.

This film is a remake of and a self-conscious tribute to Yojimbo (film)|Yojimbo (1961) by Akira Kurosawa.

==Plot==
The film opens as Eddie Lomax (Jean-Claude Van Damme) drives an Indian motorcycle in an open desert plain (referred to as the Dry Lake). Soon enough, the motorcycle breaks down and Eddie dismounts, carrying nothing but his jacket, his .45 pistol, and a bottle of tequilla. As he lies in the desert drinking, he eventually sees his friend Johnny Sixtoes (Danny Trejo), a Mexican Indian, whom he had sent a postcard notifying him of his arrival.

In their conversation, Eddie reveals that the surprise he wrote about in the letter was, in fact, the Indian motorcycle. He also reveals that hes there to kill himself as he goes into a drunken rage, revealing deep regret from their days in the Army, claiming that the souls of those that he killed haunt him. Firing shots off in all directions, he vents his buried feelings to Johnny, and hopes he will "give him the OK to take a journey".
 Shark Fralick), Silas Weir Mitchell), and Petey Hogan (Jonathan Avildsen), sons of Ramsey Hogan (Larry Drake) get out of the truck, Matt furious as one of Eddies drunken shots almost killing him.

Eddie staggers to them, not wanting any trouble; Matt, however, insists that he apologize. Matt then proposes that they take the motorcycle, and theyll forget about the gunshot. Eddie persists that the motorcycle is a gift for his friend, telling Matt to get off. Matt seems to comply, until he delivers a cheap blow to Eddie. After a small fight, Eddie is left beaten and shot in his right shoulder. As the brothers stand over Eddie, another truck passes with a local restaurant waitress Dottie (Jaime Pressly) and cook Vern (Kevin West). Dottie expresses extreme concern; Vern, simply drives on, not wanting any trouble from the Hogans. Matt begins to load the motorcycle onto the truck, instructing Jesse to make sure that Eddie is dead.

Jesse asks for Eddies gun from Pete, who took it from the earlier struggle. Being the youngest of the three, Petey insists on doing the job. As Jesse and Matt load the motorcycle, Petey aims at Eddies head. Eddies eyes open briefly, which makes Petey uneasy. Two shots ring out, which make Matt and Jesse think that Pete shot Eddie twice. However, Petey froze and simply shot two rounds into the ground.

Johnny Six Toes finds Eddie and nurses him back to health. Eddie then comes back to town to recover his pistol from Leon (Gregory Scott Cummins) and Lester (Neil Delama) in the pawn shop by killing them. Eddie falls for Rhonda Reynolds (Gabrielle Fitzpatrick), the other waitress and enlists the help of Jubal Early (Pat Morita) to help him dispose of the bodies. He then sets the two rival gangs against each other in a bloody path to get his prized motorcycle for Johnny, get his revenge from the Hogans, and ultimately find a reason to carry on living in the end Eddie defeats Jesse and is once on the road with Johnny

== Cast ==
* Jean-Claude Van Damme as Eddie Lomax
* Danny Trejo as Johnny Six Toes
* Pat Morita as Jubal Early
* Gabrielle Fitzpatrick as Rhonda Reynolds
* Larry Drake as Ramsey Hogan
* Vincent Schiavelli as Mr. Singh Shark Fralick as Matt Hogan Silas Weir Mitchell as Jesse Hogan
* Jonathan Avildsen as Petey Hogan
* Jaime Pressly as Dottie Matthews

== Production ==
Variety (magazine)|Variety reported that filming started in June 1998 and had a planned schedule of eight weeks.   The original cut of the film was known as Coyote Moon.  Van Damme did not like this cut and had the film recut.  Avildsen unsuccessfully attempted to have his name removed from the film.   

== DVD release == Columbia TriStar UK in Region 2.

On September 20, 2010, Jean-Claude Van Damme ten movie collection DVD was released and including nine action films they were:  .

== Reception ==
Larry Powell and Tom Garrett wrote in The Films of John G. Avildsen that "reviews were scarce and the few that did get published were cruel."   Rotten Tomatoes, a review aggregator, reports that 0% of five surveyed critics gave the film a positive review; the average rating was 3/10. 

== See also ==
* Snake handling

== References ==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 