The Mutants (film)
{{Infobox film
| name           = The Mutants
| image          = 
| image_size     = 
| caption        = 
| director       = Teresa Villaverde
| producer       = Jacques Bidou
| writer         = Teresa Villaverde
| narrator       = 
| starring       = Ana Moreira
| music          = 
| cinematography = Acácio de Almeida
| editing        = Andrée Davanture
| distributor    = 
| released       = 27 November 1998
| runtime        = 113 minutes
| country        = Portugal
| language       = Portuguese
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1998 cinema Portuguese film by director Teresa Villaverde, starring Ana Moreira and Alexandre Pinto. It was screened in the Un Certain Regard section at the 1998 Cannes Film Festival.   

==Cast==
* Ana Moreira - Andreia
* Alexandre Pinto - Pedro
* Nelson Varela - Ricardo
* Alexandra Lencastre - Social Worker
* Paulo Pereira - Zezito
* Helder Tavares - Franklin
* Teresa Roby - Pedros Mother
* António Capelo - Pedros Father
* Jorge Bruno Gomes - Pedros Brother
* Samuel Costa - João Paulo
* Carlos Castilho - Artur
* Maria Tengarrinha - Julia
* Marta Sofia Vargas - Paula
* Isabel Ruth - Isabel

==Awards==
This film was nominated for four Golden Globes, Portugal which were:
*Best Film: Teresa Villaverde 
*Best Director: Teresa Villaverde 
*Best Actor: Alexandre Pinto 
*Best Actress: Ana Moreira

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 