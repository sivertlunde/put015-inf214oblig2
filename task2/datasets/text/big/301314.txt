Kill Bill Volume 1
{{Infobox film
| name           = Kill Bill Volume 1
| image          = Kill bill vol one ver.jpg
| caption        = Teaser poster
| director       = Quentin Tarantino
| producer       = Lawrence Bender
| writer         = Quentin Tarantino
| story          = Quentin Tarantino Uma Thurman
| starring       = Uma Thurman
| music          = RZA Robert Richardson
| editing        = Sally Menke
| studio         = A Band Apart
| distributor    = Miramax Films
| released       =   
| runtime        = 111 minutes
| country        = United States Japan  
| language       = English
| budget         = $30 million
| gross          = $180.9 million
}}

 
 martial arts action film written and directed by Quentin Tarantino. It is the first of two Kill Bill films produced at the same time, and was followed by Kill Bill Volume 2 (2004). It was originally set for a single theatrical release, but with a runtime of over 4 hours, it was divided into two films. It stars Uma Thurman as the Bride, who seeks revenge on an assassination squad led by Bill (David Carradine) after they try to kill her and her unborn child. The film received positive reviews from critics and was a commercial success.

==Plot== Bill (David Carradine), that she is carrying his baby. He shoots her.

Four years later, the Bride goes to the home of Vernita Green (Vivica A. Fox), planning to kill her. Both women were members of the Deadly Viper Assassination Squad, which has since disbanded; Vernita now leads a normal suburban family life. She tries to kill the Bride with a pistol hidden in a box of cereal, but the Bride throws a knife into her chest, killing her.
 Elle Driver (Daryl Hannah) prepares to assassinate her via lethal injection, but Bill aborts the mission at the last moment, considering it dishonorable to kill the Bride when she cannot defend herself.

The Bride awakens from her four-year coma and is horrified to find she is no longer pregnant. She kills a hospital worker who has been raping her while she was comatose, takes his truck, and teaches herself to walk again. Resolving to kill Bill and all four members of the Deadly Viper Assassination Squad, she picks her first target: O-Ren Ishii (Lucy Liu), now the leader of the Tokyo Yakuza.
 Hattori Hanzō (Sonny Chiba), who has sworn never to forge a sword again. After learning that her target is his former student, Bill, he agrees to forge his finest sword for her.

The Bride tracks down O-Ren at a Tokyo restaurant and defeats her Yakuza army, including the elite Crazy 88 and O-Rens bodyguard, schoolgirl Gogo Yubari (Chiaki Kuriyama). She duels with O-Ren in the restaurants Japanese garden and slices the top of her skull off with a sword stroke
.

The Bride tortures O-Rens assistant and Bills protege Sofie Fatale (Julie Dreyfus) for information about Bill, but leaves her alive as a threat. Bill asks Sofie if the Bride knows her daughter is alive.

==Cast==
  the Bride (Black Mamba): A former member of the Deadly Viper Assassination Squad who is described as "the deadliest woman in the world". She is targeted by her former allies in the wedding chapel massacre, and falls into a coma. When she awakens four years later, she embarks on a deadly trail of revenge against the perpetrators of the massacre. Bill (Snake Charmer); who is never seen except his hands, although his voice is heard: The former leader of the Deadly Viper Assassination Squad. He is also the former lover of The Bride, and the father of her daughter. He is the final and eponymous target of The Brides revenge.
* Lucy Liu as O-Ren Ishii (Cottonmouth): A former member of the Deadly Viper Assassination Squad. She later becomes "Queen of the Tokyo underworld". She is the first of The Brides revenge targets.
* Vivica A. Fox as Vernita Green (Copperhead): A former member of the Deadly Viper Assassination Squad. She later becomes a homemaker living under the false name Jeannie Bell. She is the second of The Brides revenge targets. Budd (Sidewinder): A former member of the Deadly Viper Assassination Squad and brother of Bill. He later becomes a bouncer living in a trailer. He is the third of The Brides revenge targets. Elle Driver (California Mountain Snake): A former member of the Deadly Viper Assassination Squad. She is the fourth of The Brides revenge targets.
* Julie Dreyfus as Sofie Fatale: O-Rens lawyer, best friend, and second lieutenant. She is also a former protégé of Bills, and was present at the wedding chapel massacre. Hattori Hanzo: Revered as the greatest swordsmith of all time. Although long retired, he agrees to craft a sword for The Bride when he finds out what vermin she wants to kill.
*   17-year-old who is O-Rens personal bodyguard. Johnny Mo: Head general of O-Rens personal army; the Crazy 88. The Bride fights him several times during the battle at House of Blue Leaves before she apparently kills him. Earl McGraw: A policeman who investigates the wedding chapel massacre. Michael Bowen as  Buck: An orderly at the hospital where The Bride lay comatose for four years. He has been selling sexual access to her body, as well as partaking himself.
* Jun Kunimura as  Boss Tanaka: A Yakuza who is disgruntled when O-Ren assumes power; when he ridicules O-Rens nationality, she decapitates him.
* Kenji Ohba as Shiro: Hattori Hanzos employee. James Parks Edgar McGraw: The son of Earl McGraw. He is also a policeman.
* Jonathan Loughran as Trucker: a client of Buck, whose attempt at raping the (supposedly) comatose Bride results in her killing him.
* Yuki Kazamatsuri as Proprietor of the House of Blue Leaves.
*  .
* Ambrosia Kelley as Nikki Bell, Vernitas 4-year-old daughter; She witnesses The Bride killing her mother, and The Bride offers her a chance to take revenge for it when she gets older, if she still "feels raw about it".
* The Band The 5.6.7.8s (Sachiko Fuji, Yoshiko Yamaguchi and Ronnie Yoshiko Fujiyama) as themselves.

==Production==
 , used as a filming location]]

Director Quentin Tarantino intended to produce Kill Bill as one film. With a budget of  , production lasted  . Harvey Weinstein, then co-chief of Miramax Films, was known for pressuring directors to keep their films running times short. When Tarantino began editing the film, he and Weinstein agreed to split the film so Tarantino could edit a fuller film, and Weinstein could have films with reasonable running times. The decision was announced in July 2003.   

The anime sequence was directed by  .

==Influences== Lady Snowblood, a 1973 Japanese film in which a woman kills off the gang who murdered her family. The Guardian commented that Lady Snowblood was "practically a template for the whole of Kill Bill Vol. 1". 

It references the TV show Yagyū ichizoku no inbō (Japanese for "Intrigue of the Yagyu Clan") by quoting a variant of the speech in the shows opening sequence.
:Jubei Yagyu (Sonny Chiba)  : "The Secret Doctrine of Ura Yagyu ("Hidden Yagyu") states: Once engaged in battle, fight to win. That is the first and cardinal rule of battle. Suppress all human emotions and compassion. Kill whosoever stands in thy way, even if that be a God or Buddha. Only then can one master the essence of the art. Once it is mastered, thou shall fear no one, though even devils block thy way."
:Hattori Hanzo XIV (Sonny Chiba)  : "For those regarded as warriors, when engaged in combat the vanquishing of thine enemy can be the warriors only concern. Suppress all human emotion and compassion. Kill whoever stands in thy way, even if that be Lord God or Buddha himself. This truth lies at the heart of the art of combat."
 swordmaker Masamune. Kage no Gundan (Shadow Warriors in America), in which Sonny Chiba portrayed a fictionalized version of the 16th century samurai Hattori Hanzō, as well as his descendants in later seasons. Tarantino, in Volume 1 special features, claims that his films Hanzō is one of those descendants.
 Shaw Brothers, is given an obvious nod by the inclusion of the Shaw Scope logo at the beginning of Kill Bill Volume 1. 
 Swedish Thriller – A Cruel Picture, released in the U.S. as They Call Her One Eye. Tarantino, who has called Thriller "the roughest revenge movie ever made",  recommended that actress Daryl Hannah watch the film to prepare for her role as the one-eyed killer Elle Driver. 

Before the end of the list of credits there is a list of names under R.I.P. that mentions further influences , including Charles Bronson, Chang Cheh, Kinji Fukasaku, Lo Lieh, Shintaro Katsu and William Witney.

==Music==
  Woo Hoo". Lady Snowblood is further established by the use of "The Flower of Carnage" the closing theme from that film. James Lasts "The Lonely Shepherd" by pan flute virtuoso Gheorghe Zamfir plays over the closing credits.

==Release==
===Theatrical release===

  Jackie Brown Jackie Brown and Pulp Fiction (the latter released in 1994) had each grossed   on their opening weekends.  Paul Dergarabedian, president of Exhibitor Relations, said Volume 1 s opening weekend gross was significant for a "very genre specific and very violent" film that in the United States was restricted to theatergoers 17 years old and up.  According to the studio, exit polls showed that 90% of the audience was interested in seeing the second volume after seeing the first. 

Outside the United States and Canada, Kill Bill Volume 1 was released in  . The film outperformed its main competitor Intolerable Cruelty in Norway, Denmark and Finland, though it ranked second in Italy. Volume 1 had a record opening in Japan, though expectations were higher due to the film being partially set there and having homages to Japanese martial arts. The film had "a muted entry" in the United Kingdom and Germany due to being restricted to theatergoers 18 years old and up, but "experienced acceptable drops" after its opening weekend in the two territories. By  , 2003, it had made   in the  .  Kill Bill Volume 1 grossed a total of   in the United States and Canada and   in other territories for a worldwide total of  . 

===Home release===
In the United States, Volume 1 was released on DVD and VHS on April 13, 2004, the week Volume 2 was released in theaters.

In a December 2005 interview, Tarantino addressed the lack of a special edition DVD for Kill Bill by stating "Ive been holding off because Ive been working on it for so long that I just wanted a year off from Kill Bill and then Ill do the big supplementary DVD package."   

The United States does not have a DVD boxed set of Kill Bill, though box sets of the two separate volumes are available in other countries, such as France, Japan and the United Kingdom. Upon the DVD release of Volume 2 in the US, however, Best Buy did offer an exclusive box set slipcase to house the two individual releases together.   
 High Definition on Blu-ray Disc|Blu-ray on September 9, 2008 in the United States.

==Critical reception==

Kill Bill Volume 1 received positive reviews from film critics. Review aggregator Rotten Tomatoes gives the film a score of 85% based on reviews from 224 critics and reports a rating average of 7.7 out of 10. Its consensus among critics is, "Kill Bill is nothing more than a highly stylized revenge flick. But what style!"  At Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, the film received an average score of 69 based on 43 reviews. 
 Jackie Brown were "an exploration of plausible characters and authentic emotions". He wrote of Kill Bill Volume 1, "Now, it seems, his interests have swung in the opposite direction, and he has immersed himself, his characters and his audience in a highly artificial world, a looking-glass universe that reflects nothing beyond his own cinematic obsessions." Scott attributed "the hurtling incoherence of the story" to Tarantinos sampling of different genres that include spaghetti westerns, blaxploitation, and Asian action films. The critic summarized, "But while being so relentlessly exposed to a filmmakers idiosyncratic turn-ons can be tedious and off-putting, the undeniable passion that drives Kill Bill is fascinating, even, strange to say it, endearing. Mr. Tarantino is an irrepressible showoff, recklessly flaunting his formal skills as a choreographer of high-concept violence, but he is also an unabashed cinephile, and the sincerity of his enthusiasm gives this messy, uneven spectacle an odd, feverish integrity." 

Manohla Dargis of the Los Angeles Times called Kill Bill Volume 1 "a blood-soaked valentine to movies" and wrote, "Its apparent that Tarantino is striving for more than an off-the-rack mash note or a pastiche of golden oldies. It is, rather, his homage to movies shot in celluloid and wide, wide, wide, wide screen—an ode to the time right before movies were radically secularized." Dargis said, "This kind of mad movie love explains Tarantinos approach and ambitions, and it also points to his limitations as a filmmaker," calling the abundance of references sometimes distracting. She recognized Tarantinos technical talent but thought Kill Bill Volume 1 s appeal was too limited to popular culture references, calling the films story "the least interesting part of the whole equation". 

Cultural historian Maud Lavin argues that The Brides embodiment of murderous revenge taps into viewers personal fantasies of committing violence.  For audiences, particularly women viewers, this overly aggressive female character provides a complex site for identification with ones own aggression. 

===Accolades=== Golden Globe Empire Magazines list of the 500 Greatest Films of All Time at number 325 and the Bride was also ranked number 66 in Empire magazines "100 Greatest Movie Characters". 

{| class="wikitable" 
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | Awards
|- style="text-align:center;"
! style="background:#ccc;"| Award
! style="background:#ccc;"| Category
! style="background:#ccc;"| Recipient(s)
! style="background:#ccc;"| Outcome
|- 57th British Academy Film Awards
|- BAFTA Award Best Actress Uma Thurman
| 
|- BAFTA Award Best Editing Sally Menke
| 
|- BAFTA Award Best Film Music RZA
| 
|- BAFTA Award Best Sound Michael Minkler, Myron Nettinga, Wylie Stateman, and Mark Ulano
| 
|- BAFTA Award Best Visual Effects Tommy Tom, Kia Kwan, Tam Wai, Kit Leung, Jaco Wong, and Hin Leung
| 
|- 9th Empire Awards
|- Empire Award Best Film
|Kill Bill Volume 1
| 
|- Empire Award Best Actress Uma Thurman
| 
|- Empire Award Best Director Quentin Tarantino
| 
|- Sony Ericsson Scene of the Year The House of the Blue Leaves
| 
|- 61st Golden Globe Awards Golden Globe Best Actress – Motion Picture Drama Uma Thurman
| 
|- 2004 MTV Movie Awards MTV Movie Best Female Performance Uma Thurman
| 
|- MTV Movie Best Villain Lucy Liu
| 
|- MTV Movie Best Fight Uma Thurman vs. Chiaki Kuriyama
| 
|- Golden Satellite 2003 Satellite Awards
|- Satellite Award Best Art Direction/Production Design
|Kill Bill Volume 1
| 
|- Satellite Award Best Original Screenplay Quentin Tarantino and Uma Thurman
| 
|- Satellite Award Best Sound
|Kill Bill Volume 1
| 
|- Satellite Award Best Visual Effects
|Kill Bill Volume 1
| 
|- 30th Saturn Awards
|- Saturn Award Best Action/Adventure Film
|Kill Bill Volume 1
| 
|- Saturn Award Best Actress Uma Thurman
| 
|- Saturn Award Best Supporting Actor Sonny Chiba
| 
|- Saturn Award Best Supporting Actress Lucy Liu
| 
|- Saturn Award Best Director Quentin Tarantino
| 
|- Saturn Award Best Screenplay Quentin Tarantino
| 
|- Genre Face of the Future Chiaki Kuriyama
| 
|}

==Parody== Jesus Christ hunts down his greatest nemesis, the Easter Bunny, "Quentin Tarantino|Tarantino-style" in "Kill Bunny", while also hunting down Santa Claus (in the place of O-Ren Ishii) and the "Crazy Jews" (a parody of the Crazy 88 composing only of rabbis). 
 Sami population. According to the Norwegian newspaper Dagbladet, Quentin Tarantino himself has watched the films trailer and was quite happy about it, looking forward to seeing the film itself. 

==See also==
 
* Zoë Bell
* Kill Buljo
* Kill Bill Volume 2

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 