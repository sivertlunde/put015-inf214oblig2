Vampira (1974 film)
{{Infobox film
| name           = Vampira
| image          = Vampiraposter.jpg
| caption        =
| director       = Clive Donner
| producer       = Jack Wiener
| writer         = Jeremy Lloyd
| starring       = David Niven Teresa Graves David Whitaker
| cinematography = Anthony B. Richmond
| editing        = Bill Butler
| distributor    = Columbia-Warner (UK) American International Pictures (U.S.)
| released       =  
| runtime        = 88 min.
| country        = United Kingdom
| language       = English
}}

:For other uses of the name "Vampira," see Vampira (disambiguation).
 1974 comedy film|comedy/horror film spoofing the vampire genre. It stars David Niven and Teresa Graves. Following the success of Young Frankenstein, Vampira was renamed Old Dracula for release in the United States in an attempt to cash in on Young Frankensteins success.

==Plot==
Count Dracula is an old vampire who, because of his advanced age, is forced to host tours of his castle to get new victims. In an attempt to revive his long-lost love, Vampira, Dracula sets out to collect blood from the bevy of Playboy Playmate|Playboy Playmates living at his castle. However, one of the Playmates whose blood is drained is Black people|black, turning the revived Vampira into a black woman.

Dracula enthralls the hapless Marc to collect blood from three white women in hopes of restoring Vampiras original skin color. Dracula transfuses the blood into her but she is unchanged; however, her bite turns Dracula black. Marc and his love Angela race to destroy Dracula but are taken aback upon seeing Draculas new skin tone. Their surprise gives the vampires time to slip away to catch a flight to Rio for Carnival.

==Cast==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| David Niven || Count Dracula
|-
| Teresa Graves || Countess Vampira
|-
| Nicky Henson || Marc
|-
| Jennie Linden || Angela 
|-
| Linda Hayden || Helga
|-
| Bernard Bresslaw || Pottinger
|-
| Andrea Allan || Eve
|-
| Veronica Carlson || Ritva
|-
| Minah Bird || Rose
|-
| Freddie Jones || Gilmore
|-
| Frank Thornton || Mr. King
|-
| Peter Bayliss || Maltravers
|-
| Cathie Shirriff || Nancy
|-
| Aimi MacDonald || Woman in hotel room
|-
| Patrick Newell || Man in hotel room
|-
| Kenneth Cranham || Paddy, the Delinquent
|-
| Carol Cleveland || Jane, the Delinquents Victim
|-
| Luan Peters || Pottingers Secretary
|-
| Nadim Sawalha || Airline Representative
|-
| Marcia Fox || Air Hostess
|-
| Penny Irving || Playboy Bunny
|-
| Hoima McDonald || Playboy Bunny
|-
| Nicola Austin || Playboy Bunny
|-
| David Rowlands || Drunk
|-
| Ben Aris || Policeman
|}

==Release==
The film was released theatrically in the United States by American International Pictures in 1975.
 VOD and MGM DVD-R.

== External links ==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 


 