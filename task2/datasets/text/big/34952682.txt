El Icha
 

{{Infobox film
| name           = El Icha
| image          = 
| alt            =  
| caption        = 
| director       = Walid Tayaa
| producer       = Ulysson
| writer         = 
| screenplay     = Walid Tayaa
| story          = 
| starring       = Sonsdos Belhassan Nabiha Ben Miled Dalanda Abdou Dalanda Ben Kilani Mourad Karrout
| music          = 
| cinematography = Nabil Saïdi
| editing        = Imen Jibéri
| studio         = 
| distributor    = 
| released       =   
| runtime        = 17
| country        = Tunisia
| language       = 
| budget         = 
| gross          = 
}}

El Icha  (Life) is a 2010 film from Tunisia.

== Synopsis ==
Havet is a Tunisian woman of some forty years. She is a widow and has a twenty-year-old son who has emigrated to Canada. She lives with her mother in a working-class neighborhood in Tunis and works as a telemarketing operator for a French company installed in Tunis. Each morning, she leaves for work, sinking deeper and deeper into suffocating routine. At home, her life is monotonous and of no interest. 

== Awards ==
* Festival de Cine Mediterráneo de Tetuán 2010

== References ==
 

 
 


 