Sarvamangala
{{Infobox film
| name           = Sarvamangala
| image          =
| caption        = 
| director       = Chaduranga
| producer       = Doddammanni Urs
| writer         = Chaduranga (dialogues)
| screenplay     = Chaduranga Rajkumar K. Kalpana Sampath
| music          = Chellapilla Satyam
| cinematography = T Ellappan V Manohar
| editing        = P U S Maniyam P S Murthy
| studio         = Premier Studio
| distrubutor    = KCN Movies
| released       =  
| country        = India Kannada
}}
 1968 Cinema Indian Kannada Kannada film, Kalpana and Sampath in lead roles. The film had musical score by Chellapilla Satyam.   

==Cast==
  Rajkumar as Nataraja
*K. S. Ashwath Kalpana as Mangala
*Sampath
*M Jayashree
*Mallikarjunappa
*Srirangamurthy
*Lakshmi Bai
*Chennappa
*Namadev
*Papamma
*M N Lakshmi Devi
*Shanthala
*C K Kalavathi
*Thara
*Master Vikram
*Baby Girija
*Sarvamangala
*R Gururaja Rao
 

==Soundtrack==
The music was composed by Chellapilla Satyam. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Javaraya Bandare || PB. Srinivas || Janapada Sahitya || 00.40
|- Janaki || Janapada Sahitya || 03.00
|-
| 3 || Nannavalu Nannadaya || PB. Srinivas || Kuvempu || 03.38
|-
| 4 || Maneya Jyothiyu || T. M. Soundararajan || Vijaya Narasimha || 03.59
|- Janaki || M. Nagendra Babu || 05.38
|-
| 6 || Andha Chendada Hoove || P. Leela, Jayadev || Chi. Udaya Shankar || 03.30
|-
| 7 || Cheluvina Siriye Barele || A. L. Raghavan || Chi. Udaya Shankar || 03.24
|-
| 8 || Kannadave Thaynudiyu || PB. Srinivas || Chi. Udaya Shankar || 01.43
|- Janaki || Chi. Udaya Shankar || 03.01
|- Janaki ||  || 02.59
|-
| 11 || Paripariya  Parimalidhi || PB. Srinivas || Chi. Udaya Shankar || 03.18
|- Susheela || Janapada Sahitya || 03.28
|}

==References==
 

==External links==
*  
*  

 
 
 

 