Taxi No. 9211
 
 
{{Infobox film
| name           = Taxi No. 9211
| image          = Taxinumber9211.JPG
| caption        = Theatrical release poster
| director       = Milan Luthria
| producer       = Ramesh Sippy Rohan Sippy
| story          = Rajat Arora
| screenplay     = Rajat Arora John Abraham Sameera Reddy Sonali Kulkarni Shivaji Satam
| music          = Vishal-Shekhar
| narrator       = Sanjay Dutt
| cinematography = Vijay Karthik
| editing        = Aarif Shaikh
| distributor    = Ramesh Sippy Entertainment Entertainment One
| released       =  
| runtime        =
| language       = Hindi
| country        = India
| gross          =  
}} John Abraham along with Nana Patekar in lead roles. It released on 24 February 2006, and received positive response from critics, and was a moderate success at the box office. The film is a loose remake of the Hollywood film Changing Lanes.

==Plot== John Abraham), the spoilt son of a late businessman, a lift. Jay is fighting for ownership rights of his late fathers estate. The cab involves in an accident with Jai escaping as hes in hurry. Jai loses the key to the vault containing his fathers will in the back of Raghavs taxi. Raghav decides to hide it from Jai, who, in search for his lost item, goes to Raghavs house and tells his wife what he really does for a living. She leaves him, taking their son, and Raghav decides to take revenge. Raghav and Jai vow to kill each other in their fight for their own properties. When Raghav fails to kill Jai he targets Jais girlfriend, Rupali (Sameera Reddy). As Raghav chases Rupali she is saved by Jai by hairs breadth. Jai lets Rupali escape and he attacks Raghav. They have a dirty car fight, but both survive. Raghav then decides to go to Jais place. Jai returns to his apartment from a second court hearing regarding his fathers estate in defeat, because he doesnt have his fathers will. He discovers the will, torn to pieces and pasted on the wall of his apartment. Jai becomes depressed and feels lonely after his friends leave him. Soon, Rupali dumps him too. Losing everything that used to be precious for him, Jai realises the hard hitting life, and starts respecting his father and his work. On the other side Raghav is caught again by police and taken to police station where his wife tells him his real character and problem within himself. Soon, he realises his mistake. Jai, having realised the value of close ones, then bails Raghav out of jail. Raghav insists Jai for a drink and they go to Jais house for a drink. They both find out that they share the same birthday. Raghav gives back his will, which he had earlier hidden under the sofa, and says that he had never destroyed it and that the torn will on the wall was a fake one. Raghav then goes to the railway station to stop his wife and son from leaving him, but arrives a little too late at the scene. He goes back home where he sees a birthday cake on the table.  He feels that he is hallucinating, but gets a pleasant shock when he sees his wife and son standing there, singing him a birthday song, and later finds out that it was Jai who brought them back. Jai then confronts Arjun Bajaj (Shivaji Satam), the friend and custodian of the property of Jais father, whom he tells that he has realised the value of life. The film ends with Jai rejecting the will and taking back the case, while Arjun Bajaj returns everything and tells him that Jais father would have been very happy had he lived to that day. They both share a emotional hug and Jai leaves.

==Cast==
* Nana Patekar as Raghav Shastri John Abraham as Jai Mittal
* Sameera Reddy as Rupali
* Sonali Kulkarni as Raghavs wife
* Kurush Deboo as Cyrus Batliwala
* Shivaji Satam as Arjun Bajaj
* Sanjay Dutt as a Narrator
* Priyanka Chopra in a Special Appearance
* Akash Khurana as Jai Mittals father
* Nassar Abdulla as Shivraj
* Smita Jaykar as Rupalis mother
* Rajesh Asthana as Car owner
* Ganesh Yadav as an Inspector
* Mangal Kenkre as a Judge
* Ajay Jadhav as a Constable
* Master Ashwin Chitale as Raghavs son
* Shankar Sachdev as Tiwari

== Crew ==
* Director: Milan Luthria
* Producer: Ramesh Sippy, Rohan Sippy & Hussain Shaikh
* Screenplay: Manoj Tyagi
* Story: Milan Luthria
* Dialogue: Rajat Arora
* Music: Vishal-Shekhar
* Cinematography: Karthik Vijay
* Choreography: Bosco-Ceaser
* Lyrics: Dev Kohli & Vishal Dadlani
* Action: Abbas Ali Moghul
* Art Direction:Wasiq Khan
* Editing: Aarif Sheikh
* Costume Designs: Rocky S & Lajjo C

== Soundtrack ==
{{Infobox album | made by amit and adit chakraborty |
  Type        = soundtrack |
  Artist      = Vishal-Shekhar |
  Cover       = Taxi9211cover.jpg |
  Background  = gainsboro |
  Released    =  28 January 2006 (India) |
  Recorded    =  |
  Genre       = Film soundtrack |
  Length      = 33:04 |
  Label       =  Saregama HMV |
  Producer    = Vishal-Shekhar |
  Last album  = Zinda (film)|Zinda (2006) |
  This album  = Taxi Number 9211 (2006) | Om Shanti Om (2007) |
}}
{{Album ratings|title=Soundtrack
| rev1      =  
| rev1Score =not rated  |}}
The music in the movie was composed by Vishal-Shekhar with lyrics by Vishal Dadlani and Dev Kohli. The movie has 8 songs in total with two remixes. The soundtrack also features a song sung by yesteryear music composer Bappi Lahiri. The soundtrack of the movie was released sometime in the third week of January 2006 under the label of Saregama HMV.

===Track listing===
{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = no John Abraham| length1 = 3:19
| title2 = Ek Nazar Mein Bhi| extra2 = Krishnakumar Kunnath|KK, Sunidhi Chauhan| length2 = 4:37
| title3 = Meter Down| extra3 = Adnan Sami| length3 = 3:12
| title4 = Aazmale| extra4 = Shekhar Ravjiani| length4 = 4:54
| title5 = Do Pal| extra5 = Lata Mangeshkar, Sonu Nigam| lyrics5 = | length5 = 4:27
| title6 = Bekhudi| extra6 = Shaan (singer)|Shaan| length6 = 4:33
| title7 = Udne Do| extra7 = Kunal Ganjawala, Harshdeep Kaur| length7 = 6:15
| title8 = Bombai Nagariya (Club Mix)| extra8 = Bappi Lahiri, Remix By Guru Sharma| length8 = 3:13
| title9 = Meter Down (Rock N Roll Mix)| extra9 = Lata Mangeshkar, Udit Narayan, Gurdas Mann| lyrics9 = | length9 = 3:01
}}

==Trivia==
* This movie is based, with added content and changes of context, on the Hollywood movie Changing Lanes Starring Ben Affleck and Samuel L. Jackson.
* This is the first movie soundtrack for which music composer Bappi Lahiri has sung for someone else besides himself.
*The account number of John Abrahams character, Jai Mittal, is 1129, which is 9211 backwards.
* This movie marks the comeback of actress Sonali Kulkarni in Hindi. She was last seen in Danav (2003)
* The movie was remade in Telugu as Game (2006), with Mohan Babu and his son Vishnu Babu.
* The movie was remade in Tamil as "TN 07 AL 4777 (2009)"

==Box office==
*Taxi Number 9211 was one of the highest grossing films of 2006.  . Nana Patekar and John Abraham were well applauded for their performances. Sonali Kulkarni was also appreciated.

==External links==
*  at the Internet Movie Database

 
 
 
 
 