The Battle of the Century
 
{{Infobox film
  | name           = Battle of the Century
  | image          = L&H_Battle_of_the_Century_1928.jpg
  | caption        = Theatrical release poster
  | director       = Clyde Bruckman
  | producer       = Hal Roach
  | writer         = Hal Roach   H.M. Walker
  | starring       = Stan Laurel   Oliver Hardy
  | music          = Leroy Shield
  | cinematography = George Stevens
  | editing        = Richard C. Currier
  | distributor    = Metro-Goldwyn-Mayer
  | released       = December 31, 1927
  | runtime        = 19 min. 10 min. (remained cut) English intertitles
  | country        = United States
  | budget         = 
}} silent short film starring American comedy double act Laurel and Hardy. The team appeared in a total of 107 films between 1921 and 1951. 

==Plot==
Hardy enrolls Laurel at a boxing competition. Laurel, however, is too weak, and unfortunately loses. Hardy then seeks advice by an insurer to be able to easily earn a lot of money: Laurel has to break something of his body, so he can pocket the insurance money. Hardy so places a banana peel on a sidewalk, then bringing Laurel here. But a pastry chef stumbles on the peel, and gets angry with Hardy, throwing a pie on his face. Hardy responds to provocation, and so that city quarter is unleashed an epic battle of cakes.

==Production notes==
*Though The Battle of the Century is an official Laurel and Hardy entry, the team had yet to take on their recognisable characters.
*A young Lou Costello can be seen in an early scene as a member of the audience at the prize-fight mentioned in the films title.
*For many years, footage from the famous climactic pie fight was known to be the only extant material from the film until the opening reel (featuring a boxing match) was discovered in 1979 by Richard Feiner.  The sequence involving Eugene Pallette is still lost, as is the very final gag where a cop gets a pie in his face and promptly chases Laurel and Hardy down the street.

==Cast==
*Stan Laurel - Canvasback Clump
*Oliver Hardy - Manager
*Noah Young - Thunderclap Callahan
*Eugene Pallette - Insurance agent Charlie Hall - Pie delivery man
*Sam Lufkin - Boxing referee
*Gene Morgan - Ring announcer Steve Murphy - Callahans second
*George B. French - Dentist
*Dick Sutherland - Dental patient
*Anita Garvin - Woman who slips on pie
*Dick Gilbert - Sewer worker
*Wilson Benge - Pie victim with top hat
*Jack OBrien - Shoeshine patron
*Ellinor Vanderveer - Lady in car
*Lyle Tayo - Woman at window
*Dorothy Coburn - Pie victim
*Al Hallett - Pie victim
*Lou Costello - Ringside spectator (extra) Jack Hill - Ringside spectator (extra)
*Ham Kinsey - Ringside spectator (extra)
*Ed Brandenburg - Warring pedestrian Bob OConnor - Warring pedestrian Jack Adams
*Bert Roach
*Dorothea Wolbert Charley Young

==See also==
*List of incomplete or partially lost films

==References==
 

==External links==
* 
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 