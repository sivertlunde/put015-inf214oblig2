A Band Called Death
{{Infobox film
| name           = A Band Called Death
| image          = A Band Called Death.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster for A Band Called Death featuring the group in the 1970s.
| film name      = 
| director       = {{plainlist|
*Mark Christopher Covino
*Jeff Howlett}}
| producer       = {{plainlist|
*Mark Christopher Covino
*Jeff Howlett
*Jerry Ferrara
*Kevin Mann
*Matthew Perniciaro
*Scott Mosier}}
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Bobby Hackney, Sr.
| music          = Tim Boland
| cinematography = 
| editing        = Rich Fox
| studio         = 
| distributor    = Drafthouse Films
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = 
| budget         = 
| gross          = 
}}
 Death and their new-found popularity decades after the group recorded their music.

The film premiered at the Los Angeles Film Festival in 2012 and was well received by film critics.

==Synopsis== Drag City record label and the groups eventual reunion and touring with a new guitarist.

==Production==
Director Jeff Howlett met Bobby Hackney Sr. and Dannis Hackney in the 1990s at a local music festival where his band and their band, Lambsbread, were playing. {{cite web|url=http://twitchfilm.com/2012/06/la-film-fest-2012-interview-a-band-called-death-co-dirs-jeff-howlett-mark-convino-on-the-discovery-t.html|work=Twitch Film|title=LA Film Fest 2012 Interview: A BAND CALLED DEATH Co-Dirs Jeff Howlett & Mark Covino On The Discovery That Changed Punk Rock History|accessdate=July 19, 2013|date=June 21, 2012|author=Fader
, Lainna}}  Bobby Hackney Jr. had formed his own band Rough Francis with his brothers to honor his fathers band Death.  After Howlett saw the show he was impressed with the music and with Bobby Jr. and his father they began working on the documentary.  In 2008, Mark Covino met Howlett while working on a video that Howlett was directing. 
The next year Howlett approached Covino to work on the film with him, but Covino initially hesitant about helping Howlett as he was attempting to finish his own documentary feature at the time but changed his mind after reading the New York Times article on the group and hearing the two songs from the "Politicians in my Eyes" 7" single. 

The film shown as the practice room where Death originally practiced their music was currently being used as a guest room in the house.  Covino spent months organizing and scanning photos from members of the groups family from Vermont, Ohio and Detroit.  Very little video footage of the groups guitarist David Hackney surfaced during their duos research. 

==Release==
A Band Called Death had its world premiere at the 2012 Los Angeles Film Festival on June 16.   DVD and blu-ray disc on August 13, 2013. 

==Reception== normalized rating average score of 77, based on 14 reviews.   

==Notes==
 

==External links==
*  

 
 
 
 