Kazhugu (2012 film)
{{Infobox film
| name           = Kazhugu
| image          =Kazhugu audio cover.jpg
| caption        =
| director       = Sathyasiva
| producer       = K. K. Sekhar K. S. Madhubala
| story          = Sathyasiva
| screenplay     = Sathyasiva
| starring       = Krishna Sekhar Bindu Madhavi Karunas Thambi Ramaiah Praveen
| music          = Yuvan Shankar Raja
| cinematography = Sathya
| editing        = Praveen K. L. N. B. Srikanth
| studio         = Talking Times Arun Film Entertainments
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = INR 3 Crores
| gross          = $1 Million
}} suicide victims who jump off a cliff.   The film, based on real-life incidents, has been shot in real locations, including Kodaikanal, Theni and Munnar. It had been in making since late 2010 and eventually released on March 16, 2012,  to mixed reviews. 

==Plot==
Sera (Krishna Sekhar|Krishna) is a guy who earns his living by retrieving dead bodies from Kodaikanals Green Valley View suicide point. Nandu (Karunas), Shanmugam (Thambi Ramaiah) and a mute friend are part of Seras crew. Chera meets Kavitha (Bindu Madhavi) when he and his crew retrieve her sisters body from a gully, after the sister had committed suicide with her boyfriend. Kavitha gradually falls in love with Sera. A parallel plot is of Iyya (Jayaprakash), who trades in stolen tea, with corrupt officials turning a blind eye to his activities.

Iyyas gang kills four police officers during a raid that goes wrong. Iyya, who knows Shanmugam, threatens him not to retrieve the bodies of the police officers. Fearing for their lives, Shanmugan tries to convince Sera not to retrieve the bodies. Brushing aside Shanmugams fears, Seras crew retrieves the bodies of the police officers due to a sense of duty. After retrieving the bodies, Sera informs the police of the murder by Iyya, resulting in Iyyas arrest.

The climax of the movie is on how Iyya retaliates and what becomes of Seras crew.

==Cast==
* Krishna Sekhar as Sera
* Bindu Madhavi as Kavitha
* Karunas as Nandu
* Thambi Ramaiah as Shanmugam (Chittappa @ Chithu)
* Praveen
* Jayaprakash as Ayya
* Nellai Siva
* Sujibala as Valli

Special appearances in promotional song by (in alphabetical order):
  Aarthi
*Arya Arya
*Divyadarshini
*Ganeshkar
*Janani Iyer Jennifer
*Jiiva
*Kala
*Keerthi Linguswamy
*Oviya
*Pandiraj
*Perarasu
*Premji Amaren
*Priya Anand
*Pushpavanam Kuppuswamy Radhika
*S. J. Surya
*Sanjeev
*Sivakarthikeyan Thaman
*Venkat Prabhu Vijayalakshmi
*Vishnuvardhan Vishnuvardhan
 

==Soundtrack==
{{Infobox album
| Name = Kazhugu
| Longtype = to Kazhugu
| Type = Soundtrack
| Artist = Yuvan Shankar Raja
| Cover = Kazhugu audio cover.jpg
| Released = 23 November 2011
| Recorded = 2011 Feature film soundtrack
| Length = 23:51 Tamil
| Label = Sony Music
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album = Panjaa (2011)
| This album = Kazhugu (2011)
| Next album = Rajapattai (2011)
}}

The soundtrack of Kazhugu was composed by Yuvan Shankar Raja. It features five tracks, which were sung by Yuvan Shankar Raja himself, his brother Karthik Raja and noted folk music artists Pushpavanam Kuppusamy and Velmurugan among others.    Yuvan Shankar Raja said that he composed two of the songs keeping the visuals in mind, after he had seen the video montages that were shot.  The lyrics were penned by Na. Muthukumar, Snehan and Eaknath. The soundtrack album was released on 23 November 2011 at Sathyam Cinemas.  In early March 2012, the video of the song "Aambalaikum Pombalaikum" was released as a promo track featuring several film personalities from Tamil cinema (see Kazhugu (2012 film)#Cast|cast). 

{{tracklist
| headline        = Track listing 
| extra_column    = Singer(s)
| total_length    = 23:51
| lyrics_credits  = yes
| title1          = Aambalaikum Pombalaikum
| lyrics1         = Snehan Sathyan
| length1         = 5:01
| title2          = Aathadi Manasudhan
| lyrics2         = Na. Muthukumar
| extra2          = Priya Himesh
| length2         = 5:09
| title3          = Paathagathi Kannupattu
| lyrics3         = Snehan
| extra3          = Yuvan Shankar Raja, Raju krishnamurthy
| length3         = 4:30
| title4          = Vaadi Vaadi
| lyrics4         = Eaknath 
| extra4          = Pushpavanam Kuppuswamy, Suvi Suresh, Anitha
| length4         = 4:01
| title5          = Aathadi Manasudhan
| lyrics5         = Na. Muthukumar
| extra5          = Karthik Raja
| length5         = 5:10
}}

==Release== Sun TV. The film was given a "U/A" certificate by the Indian Censor Board.

==Movie Highlights==
Bindu Madhavi, who featured in Veppam almost like a guest appearance, has got a good opportunity to score with her acting and she has done exactly that. Her makeup free face and clear expressions (without overacting) are the main highlights of her performance.
Comedy track between Krishna, Karunas and Thambi Ramaiah binds well with the screenplay and gives us laughter therapy every now and then.
Fight sequences have been choreographed well and Krishnas efforts needs a special mention here.
Cinematographer P Sathya has done a good job, especially in lighting and angles which he has used while shooting the night life of the dead body retrievers.
Yuvans background score has given life to the screenplay. Ambalaikkum Pombalaikkum song with the lyrics of Snehan is a sure one to listen and enjoy.
Director Sathya Siva has penned the screenplay well within 5 to 6 characters and at the same time has added value to each and every character. His efforts in filming the movie in hilly regions of Ooty and nearby places will earn him the tag as hard worker. On the whole, a good work by a debutant.
 

==Reception==

===Critical response===
The film, upon release, fetched mixed reviews. A reviewer from Sify.com said Kazhugu was a "realistic romantic thriller that seldom loses its grip on your attention. Credible performances from its leads, a nail-biting screenplay along with a fresh milieu makes it an engaging film".  The Times of India gave it 3.5 out of 5, claiming that it was "gripping from start to finish".  Indiaglitz.com noted that it was a "bright film with a dark theme".  Rohit Ramachandran of Nowrunning.com rated Kazhugu 1/5 calling it vile.  Malathi Rangarajam from The Hindu wrote: "Its heartening to see young filmmakers daring to steer clear of stereotypes. S. Sathyasiva who makes his bow with Kazhugu is the latest in this category". The critic further cited that director Sathyasiva was "a director to watch out for".  Kannan Vijayakumar of Moviecrow rated it 3 out of 5, and labelled it "Above Average" stating "Kazhugu is well-researched unique attempt with interesting backdrop, but failed to fully utilize its potential due to shaky screenplay". 

Pavithra Srinivasan from Rediff.com gave the film 2.5 out of 5 and commenting: Kazhugu starts off well but the extreme predictability of the screenplay makes sure that tedium sets in, leading to a rather tame climax".  Behindwoods.com gave it 2 out of 5 and said that the backdrop of the film was "genuinely interesting", while criticizing that the script should have been "more inclusive so that the possibilities of the theme can be fully exploited rather than ending up as an averagely executed revenge action film". 

==References==
 

==External links==
*  

 
 
 
 
 