Mutant (film)
{{Infobox Film |
|  name           = Mutant
|  image          = Mutant1984.jpg
|  caption        = Original poster designed by Design Projects, Inc.
|  producer       = Igo Kantor
|  director       = John "Bud" Cardos
|  writer         =
|  story          = {{plainlist|
* Michael Jones
* John C. Kruize
}}
|  Screenplay     = {{plainlist|
* Michael Jones
* John C. Kruize
* Peter Z. Orton
}}
|  starring       = {{plainlist|
* Wings Hauser
* Bo Hopkins
* Jody Medford
* Lee Montgomery
* Marc Clement
* Cary Guffey
* Jennifer Warren
}}
|  music          = Richard Band
|  cinematography = Alfred Taylor
|  editing        = Michael J. Duthie
|  distributor    = Film Ventures International
|  released       =  
|  runtime        = 99 minutes
|  language       = English
|  budget         =  
|  gross          = 
}} 1984 horror film.  It was initially released to theaters as Night Shadows, but it premiered on video with the Mutant title, which it has retained for all subsequent VHS and DVD releases.

==Plot==
Two brothers Josh (Wings Hauser) and Mike (Lee Montgomery), are run off the road by local rednecks and forced to spend the night in a small town whose inhabitants are suffering from a mysterious disease. Mike goes missing and so Josh has to team-up with the Sheriff (Bo Hopkins) to defeat the mutating townsfolk.

==Cast==
*Wings Hauser as Josh Cameron
*Bo Hopkins as Sheriff Will Stewart
*Jody Medford as Holly Pierce
*Lee Montgomery as Mike Cameron
*Marc Clement as Albert Hogue
*Cary Guffey as Billy
*Jennifer Warren as Dr. Myra Tate 

==Production==
The film was directed by John "Bud" Cardos.  Mark Rosman was originally hired to direct, but was replaced by Cardos early in the production after the studio objected to the way he was shooting the film.   Mutant was a production of  Edward L. Montoro,  and this films budget was one of the contributing factors to the downfall of Montoros company, Film Ventures International. 

==Home media==
The film is available on DVD from several different distributors.  DVDs released by both Elite Entertainment and Genius Products, under license from Liberation Entertainment, each show the film in widescreen.

== Soundtrack ==
The film score by Richard Band was released by Perseverance Records on April 28, 2008. It is an expanded release of the original score album, released by Intrada Records in 1993 (previously available as an LP for Varèse Sarabande). The score, performed by the National Philharmonic Orchestra, has been highly acclaimed over the years for its massive size, often lyrical scope and a surprisingly melodic nature.

== Reception ==
Writing in The Zombie Movie Encyclopedia, academic Peter Dendle called it a "fairly humorless and uncomplicated zombie invasion exercise" that is "derivative and unnecessary". 

== References ==
 

==External links==
*  
 
 
 
 
 
 
 
 