Bliss (1917 film)
 
{{Infobox film
| name           = Bliss
| image          = Bliss 1917 HAROLD LLOYD BEBE DANIELS SNUB POLLARD Alfred J Goulding.webm
| caption        = Full film
| director       = Alfred J. Goulding
| producer       = Hal Roach
| writer         =
| starring       = Harold Lloyd
| music          = Borodin and Bach
| cinematography = Walter Lundin
| editing        = Della Mullady
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 short comedy film featuring Harold Lloyd.   

==Cast==
* Harold Lloyd as Harold
* Snub Pollard as Snub
* Bebe Daniels as The Girl
* W.L. Adams
* William Blaisdell
* Sammy Brooks
* Lottie Case
* Billy Evans
* William Gillespie
* Sadie Gordon
* Charles Grider
* Arthur Harrison
* Clyde E. Hopkins
* Oscar Larson
* Gus Leonard
* Belle Mitchell
* Hazel Powell
* Hazel Redmond
* Zetta Robson
* Dorothy Saulter
* Nina Speight
* William Strohbach
* Lillian Sylvester
* David Voorhees

==See also==
* List of American films of 1917
* Harold Lloyd filmography

==References==
 

==External links==
* 
* 
*   on YouTube

 
 
 
 
 
 
 
 
 
 
 