Lovesick (1983 film)
{{Infobox film |
| name = Lovesick
| image = lovesick (DVD cover).jpg
| caption = Lovesick DVD cover
| director = Marshall Brickman
| writer = Marshall Brickman
| producer = Charles Okun
| starring = {{plainlist|
* Dudley Moore
* Elizabeth McGovern
* John Huston
* Alec Guinness
}}
| music = Philippe Sarde
| cinematography = Gerry Fisher
| editing = Nina Feinberg
| distributor = The Ladd Company   Warner Bros.
| runtime = 95 min.
| released = February 18, 1983
| awards = 
| preceded_by =
| followed_by =
| country = United States
| language = English
| budget =
| gross = $10,143,618 (USA)
}} 1983 romantic comedy film.  It was written and directed by Marshall Brickman.  It stars Dudley Moore and Elizabeth McGovern and features Alec Guinness as the ghost of Sigmund Freud.

==Plot==
Psychologist Saul Benjamin takes on a patient temporarily as a favor to a colleague friend, Otto Jaffe, who is infatuated with her.  After her doctor dies, Chloe Allen comes to see Dr. Benjamin and immediately he is smitten with her, too.

The doctor-patient relationship is violated by Dr. Benjamins romantic impulses toward Chloe and by his intense jealousy of anyone who comes near her, including Ted Caruso, an arrogant Broadway actor with whom she has become involved. The psychiatrists wife also is carrying on an affair with Jac Applezweig, an artist.

The ghost of Dr. Sigmund Freud, the father of modern psychology, visits Dr. Benjamin from time to time to dispense warnings and wisdom. Benjamins work begins to suffer as he abandons patients like Mrs. Mondragon, finding her tedious, and treats the cakeia of another, Marvin Zuckerman, by designing a peculiar handmade hat for him to wear.

A board of inquiry calls in Dr. Benjamin to consider revoking his license. In the end, he admits his feelings to Chloe and concludes that he prefers true love to treating the sick.

==Cast==
* Dudley Moore as Saul Bejmamin
* Elizabeth McGovern as Chloe Allen
* Alec Guinness as Sigmund Freud
* Wallace Shawn as Otto Jaffe
* Ron Silver as Ted Caruso
* John Huston as Dr. Larry Geller Alan King as Dr. Lionel Gross
* Selma Diamond as Dr. Harriet Singer
* Larry Rivers as Jac Applezweig
* David Strathairn as Zuckerman
* Christine Baranski as the Nymphomaniac
* Renée Taylor as Mrs. Mondragon

==External links==
*  
*  
*   at Rotten Tomatoes

 

 
 
 
 


 