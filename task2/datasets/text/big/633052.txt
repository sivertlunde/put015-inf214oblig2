Inglourious Basterds
 
 
{{Infobox film
| name           = Inglourious Basterds
| image          = Inglourious Basterds poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Quentin Tarantino 
| producer       = Lawrence Bender
| writer         = Quentin Tarantino
| starring       = {{Plain list| 
*Brad Pitt
*Christoph Waltz
*Michael Fassbender
*Eli Roth
*Diane Kruger
*Daniel Brühl
*Til Schweiger
*Mélanie Laurent
}}
| narrator       =  
| music          =   Robert Richardson
| editing        = Sally Menke
| studio         = {{Plain list|
*A Band Apart
*Studio Babelsberg
}}
| distributor    = {{Plain list|
*The Weinstein Company  
*Universal Pictures  
}}
| released       =   
| runtime        = 152 minutes  
| country        = {{Plain list|
*Germany     
*United States    
}}
| language       = {{Plain list|
*English
*German
*French
*Italian}}
| budget         = $75 million   
| gross          = $321.5 million   
}}
 war film French Jewish cinema proprietor (Laurent), and the other by a team of Jewish-American soldiers led by First Lieutenant Aldo Raine (Pitt). The films title was inspired by director Enzo G. Castellaris macaroni combat film, The Inglorious Bastards (1978).
 United States 62nd Cannes Film Festival. It was widely released in theaters in the United States and Europe in August 2009 by The Weinstein Company and Universal Pictures.
 Academy Award Best Picture, Best Director Best Original Best Actor BAFTA Award, Screen Actors Golden Globe Award, and the Academy Award for Best Supporting Actor.

==Plot==
  SS Standartenführer|colonel SS soldiers to shoot through the floorboards and kill the family. All are killed except Shosanna who manages to escape and begin fleeing through the field. She manages to escape the premises after Colonel Landa seems to spare her life instead of shooting her.
 1st Special Service Force Lieutenant Aldo Raine recruits American Jews|Jewish-American soldiers to the Basterds, who spread fear among the German soldiers by brutally killing them behind enemy lines. The Basterds also recruit Staff Sergeant Hugo Stiglitz, a German soldier who murdered thirteen Gestapo officers. Adolf Hitler interviews a Nazi soldier, Private Butz, who relates how his squad was ambushed and his sergeant beaten to death with a baseball bat by Staff Sergeant Donny Donowitz, the "Bear Jew". Raine carved a swastika into Butzs forehead with a knife so he can never escape his Nazi identity.

In June 1944, Shosanna Dreyfus runs a cinema in Paris under an alias. She meets Fredrick Zoller, a German sniper who has killed 250 soldiers in a single battle; Zoller is to star in a Nazi propaganda film, Stolz der Nation (Nations Pride). Infatuated with Shosanna, Zoller convinces Joseph Goebbels to hold the premiere at her cinema. Shosanna plots with her projectionist and lover, Marcel, to burn down the cinema and kill the top Nazi leaders at the premiere. Meanwhile, the British recruit Lieutenant Archie Hicox, a film critic specialising in German cinema, plans for a similar operation. Hicox will rendezvous with a double agent, German film star Bridget von Hammersmark, and the Basterds to plant explosives at the premiere.

Hicox, along with Hugo Stiglitz and another German speaker from the Basterds, meets von Hammersmark at a Rathskeller|tavern, where a German soldier, Wilhelm, is celebrating his sons birth. Major Dieter Hellstrom, a Gestapo officer notices Hicoxs odd German accent and becomes suspicious. Hicox convinces Hellstrom that he is from the German mountains, but gives himself away by ordering glasses with the incorrect hand gesture. After a Mexican standoff, the Basterds open fire, and everyone but Wilhelm and a wounded von Hammersmark is killed. Raine arrives at the tavern and negotiates with Wilhelm for von Hammersmarks release, only for von Hammersmark to shoot Wilhelm when he lowers his guard. Landa investigates the scene and finds evidence that von Hammersmark was there, including a lost shoe.

Raine learns from von Hammersmark that Hitler will attend the premiere, and decides to continue the mission. Two of the Basterds, Donowitz and Ulmer, join him in posing as Italians, hoping that their limited Italian will fool Germans unfamiliar with the language. Landa, who speaks fluent Italian, converses briefly with the Basterds before sending Donowitz and Ulmer to their seats. He takes von Hammersmark aside to a private room, replaces her shoe with the one from the tavern, and strangles her to death. He orders the capture of Raine and another of his men, Utivich, but instead of raising an alarm, has Raine contact his superior with the Office of Strategic Services|OSS. Landa cuts a deal, allowing the mission to proceed in exchange for immunity and rewards.
 submachine guns into the crowd until the bombs go off, killing everyone present.

Landa and his radio operator drive Raine and Utivich into Allied territory, where they surrender. Raine shoots the radio operator, then tells Landa that he cannot go unpunished for his actions. Raine carves a swastika into Landas forehead as a permanent reminder of his crimes.

==Cast==
  The Apache" Raine SD Standartenführer Hans Landa
* Michael Fassbender as Lieutenant Archie Hicox
* Eli Roth as Sergeant Donny "The Bear Jew" Donowitz
* Diane Kruger as Bridget von Hammersmark
* Daniel Brühl as Private First Class Fredrick Zoller
* Til Schweiger as Sergeant Hugo Stiglitz
* Mélanie Laurent as Shosanna Dreyfus/Emmanuelle Mimieux
* August Diehl as Major Dieter Hellstrom
* Julie Dreyfus as Francesca Mondino
* Sylvester Groth as Joseph Goebbels
* Jacky Ido as Marcel
* Denis Ménochet as Perrier LaPadite
* Mike Myers as General Ed Fenech
* Rod Taylor as Winston Churchill (this is Taylors final role before his death in 2015)
* Martin Wuttke as Adolf Hitler
* Gedeon Burkhard as Corporal Wilhelm Wicki
* B. J. Novak as Private First Class Smithson "The Little Man" Utivich
* Omar Doom as Private First Class Omar Ulmer
* Léa Seydoux as Charlotte LaPadite
* Richard Sammel as Sergeant Werner Rachtman
* Alexander Fehling as Staff Sergeant Wilhelm
* Christian Berkel as Proprietor Eric 
* Sönke Möhring as Private Butz
* Samm Levine as Private First Class Hirschberg
* Paul Rust as Private First Class Andy Kagan
* Michael Bacall as Private First Class Michael Zimmerman
* Rainer Bock as General Schonherr 
* Bo Svenson as an American Colonel in Nations Pride
* Enzo G. Castellari as Nazi General at film premiere
* Samuel L. Jackson (uncredited) as the narrator 
* Harvey Keitel (uncredited) as the voice of the OSS Commander 
* Bela B. (uncredited) as an usher   
 

==Production==
===Development=== Dirty Dozen Guns of Navarone kind of thing".  According to Tarantino, all his films make the audience laugh at things that are not supposed to be funny, but the sense of humor differs in each. 
 

By 2002, Tarantino found Inglourious Basterds to be a bigger film than planned and saw that other directors were working on World War II films.    At this point, Tarantino had produced three nearly finished scripts, proclaiming that it was "some of the best writing Ive ever done. But I couldnt come up with an ending."  Consequently, the director held off his planned film and moved on to direct the two-part film Kill Bill (2003–2004).  After the completion of Kill Bill, Tarantino went back to his first storyline draft and came up with the idea of turning it into a mini-series. Instead he trimmed the script, using his script for Pulp Fiction as a guide to the right length.  He then planned to begin production of Inglourious Basterds in 2005.    The revised premise focused on a group of soldiers who escape from their executions and embark on a mission to help the Allies of World War II|Allies. He described the men as "not your normal hero types that are thrown into a big deal in the Second World War". 

In November 2004, Tarantino decided to hold off the films production and instead took an acting role in  -esque touch."  He further commented on Late Show with David Letterman that Inglourious Basterds is a "Quentin Tarantino spelling." 

===Casting===
 ]]
Tarantino originally sought Leonardo DiCaprio to be cast as Hans Landa,  before deciding to have the character played by an older German actor.    The role ultimately went to Austrian Christoph Waltz, who, according to Tarantino, "gave me my movie" as he feared the part was "unplayable."  Pitt and Tarantino had wanted to work together for a number of years, but were forced to wait for the right project.    When Tarantino was halfway through the films script, he sensed that Pitt was a strong possibility for the role of Aldo Raine. By the time he had finished writing, Tarantino thought Pitt "would be terrific" and called Pitts agent to ask if he was available. 
 The Office actor and writer, B. J. Novak, was also cast in August 2008 as Private First Class Smithson Utivich, "a New York-born soldier of slight build". 

Tarantino talked to actress Nastassja Kinski about playing the role of Bridget von Hammersmark and even flew to Germany to meet her, but a deal could not be reached  and Tarantino cast Diane Kruger instead.     Rod Taylor was effectively retired from acting and no longer had an agent, but came out of retirement when Tarantino offered him the role of Winston Churchill in the film.    This would be Taylors last appearance on film before his death on January 7, 2015.  In preparation for the role, Taylor watched dozens of DVDs with footage of Churchill in order to get the Prime Ministers posture, body language, and voice, including a lisp, correct.  Taylor initially recommended British actor Albert Finney for the role during their conversation, but agreed to take the part because of Tarantinos "passion."  Mike Myers (as Gen. Ed Fenech), a fan of Tarantino, had inquired about being in the film since Myers parents had been in the British Armed Forces.  In terms of the characters dialect, Myers felt that it was a version of Received Pronunciation meeting the officer class, but mostly an attitude of "Im fed up with this war and if this dude can end it, great because my country is in ruins." 
 OSS commander, respectively.    German musician Bela B. has an uncredited cameo appearance as an usher at the cinema.  Two characters, Mrs. Himmelstein and Madame Ada Mimieux, played by Cloris Leachman and Maggie Cheung respectively, were both cut from the final film due to length reasons.    

===Filming=== Harvey and Universal Pictures to finance the rest of the film and distribute it internationally.     Germany and France were scheduled as filming locations and principal photography started in October 2008 on location in Germany.      Filming was scheduled to begin on October 13, 2008, and shooting started that week.       Special effects were handled by KNB EFX Group with Greg Nicotero  and much of the film was shot and edited in the Babelsberg Studios in Potsdam, Germany    and in Bad Schandau, a small spa town near Germanys border with the Czech Republic.  Roth claimed that they "almost got incinerated", during the theater fire scene, as they projected the fire would burn at 400&nbsp;°C (750&nbsp;°F), but it instead burned at 1200&nbsp;°C (2000&nbsp;°F). He claimed the swastika was not supposed to fall either, as it was fastened with steel cables, but the steel softened and snapped.    On January 11, 2013, on the BBCs The Graham Norton Show, Tarantino claimed that for the scene where Kruger was strangled, he personally strangled the actress, with his own bare hands, in one take, to aid authenticity. 

Following the films screening at Cannes, Tarantino stated that he would be re-editing the film in June before its ultimate theatrical release, allowing him time to finish assembling several scenes that were not completed in time for the hurried Cannes première. 

===Music===
 
Tarantino originally wanted Ennio Morricone to compose the films soundtrack.  Morricone was unable to, because the films sped-up production schedule conflicted with his scoring of Giuseppe Tornatores Baarìa - La porta del vento|Baarìa.  However, Tarantino did use eight tracks composed by Morricone in the film, with four of them included on the CD.  
 The Alamo. theme from Cat People.   This is the first of Tarantinos soundtracks that does not include dialogue excerpts from the film.  The soundtrack was released on August 18, 2009. 

==Release==
 , Mélanie Laurent, and producer Lawrence Bender at a premiere for the film in August 2009]] leaked on the Internet and several Tarantino fan sites began posting reviews and excerpts from the script.    

The films first full teaser trailer premiered on Entertainment Tonight on February 10, 2009,  and was shown in US theaters the following week attached to Friday the 13th (2009 film)|Friday the 13th.  The trailer features excerpts of Lt. Aldo Raine talking to the Basterds, informing them of the plan to ambush and kill, torture, and scalp unwitting Nazi servicemen, intercut with various other scenes from the film.    It also features the spaghetti-westernesque terms Once Upon A Time In Nazi Occupied France,  which was considered for the films title,  and A Basterds Work Is Never Done, a line not spoken in the final film (the line occurs in the script during the Bear Jews backstory). 

The film was released on August 19, 2009 in the United Kingdom and France,  two days earlier than the US release date of August 21, 2009.  It was released in Germany on August 20, 2009.  Some European cinemas, however, showed previews starting on August 15.   In Poland, the artwork on all advertisements and on DVD packaging is unchanged, but the title was translated non-literally to Bękarty Wojny (Bastards of War), so that Nazi iconography could stylize the letter "O". 

===Promotion in Germany=== Universal Pictures display of steel helmet German law, so the film itself is not censored in Germany. 

===Home media=== The Hangover, selling an estimated 1,581,220 DVDs making $28,467,652 in the United States. 

The German version is 50 seconds longer than the American version. The scene in the tavern has been extended. Although in other countries, the extended scene was released as a bonus feature, the German theatrical, DVD, and Blu-ray versions are the only ones to include the full scene. 

==Reception==

===Box office===
Opening in 3,165 screens, the film earned $14.3 million on the opening Friday of its North American release,  on the way to an opening weekend gross of $38 million, giving Tarantino a personal best weekend opening and the number one spot at the box office, ahead of District 9.  The film fell to number two in its second weekend, behind The Final Destination, with earnings of $20 million, and grossed $73.8 million in its first ten days.  Inglourious Basterds opened internationally at number one in 22 markets on 2,650 screens making $27.49 million. First place openings included France, taking in $6.09 million on 500 screens. The United Kingdom was not far behind making $5.92 million (£3.8m) on 444 screens. Germany took in $4.20 million on 439 screens and Australia with $2.56 million (A$2.8m) on 266 screens.  It has come to gross $120.5 million in the United States and Canada and $200.9 million in other territories, making its worldwide gross $321.4 million. Inglourious Basterds was Tarantinos highest grossing film, both in the U.S. and worldwide until Django Unchained in 2012. 

===Critical reception=== rating average of 7.8 out of 10.  According to the sites critical consensus, "A classic Tarantino genre-blending thrill ride, Inglourious Basterds is violent, unrestrained, and thoroughly entertaining."  Metacritic, which assigns a score of 1–100 to individual film reviews, gives the film an averaged rating of 69 based on 36 reviews. 

 ]]
Critics initial reactions at the   likened the experience of watching the film to "sitting in the dark having a great pot of warm piss emptied very slowly over your head." 

The film has met some criticism from Jewish press, as well. In Tablet, Liel Liebowitz criticizes the film as lacking moral depth. He argues that the power of film lies in its ability to impart knowledge and subtle understanding, but Inglourious Basterds serves more as an "alternative to reality, a magical and Manichaean world where we neednt worry about the complexities of morality, where violence solves everything, and where the Third Reich is always just a film reel and a lit match away from cartoonish defeat".    Anthony Frosh, writer for the online magazine Galus Australis, has criticized the film for failing to develop its characters sufficiently, labeling the film "Enthralling, but lacking in Jewish content". 

===Accolades===
 
Christoph Waltz was singled out for Cannes honors, receiving the   nomination but rather an actual Oscar in his hands.... he must have gold".  Best Motion Best Supporting Best Cast Best Supporting BAFTA Awards, Best Supporting Academy Awards, Best Picture, Best Director, Best Supporting Best Original Screenplay.  Waltz was awarded the Academy Award for Best Supporting Actor. 

==In popular culture==
On December 5, 2010, "The Fight Before Christmas", the eighth episode of The Simpsons  22nd season, featured an Inglourious Basterds sequence during a World War II flashback. 

When the Jewish,  , 314-pound American football player Gabe Carimi was drafted in the 2011 NFL Drafts first round by the Chicago Bears, he was nicknamed "The Bear Jew", a reference to the character in Inglourious Basterds.  

==See also==
* Vigilante film
* Jewish Brigade – an actual unit of Jewish Soldiers, formed by the British to fight the Nazis in WW2
* Nakam
* The White Hell of Pitz Palu – a prominently mentioned German mountain film

==References==
 

==External links==
 
 
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 