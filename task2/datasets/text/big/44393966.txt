The Cuban Love Song
{{Infobox film
| name           = The Cuban Love Song
| image          = The Cuban Love Song poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = W. S. Van Dyke	
| producer       = Albert Lewin John Colton Gilbert Emery Robert E. Hopkins Paul Hervey Fox 
| starring       = Lawrence Tibbett Lupe Vélez Ernest Torrence Jimmy Durante Karen Morley Louise Fazenda
| music          = Herbert Stothart
| cinematography = Harold Rosson
| editing        = Margaret Booth
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 John Colton, Gilbert Emery, Robert E. Hopkins and Paul Hervey Fox. The film stars Lawrence Tibbett, Lupe Vélez, Ernest Torrence, Jimmy Durante, Karen Morley and Louise Fazenda. The film was released on December 5, 1931, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Lawrence Tibbett as Terry
*Lupe Vélez as Nenita
*Ernest Torrence as Romance
*Jimmy Durante as O.O. Jones
*Karen Morley as Crystal
*Louise Fazenda as Elvira
*Hale Hamilton as John
*Mathilde Comont as Aunt Rose
*Philip Cooper as Terry Jr.

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 

 