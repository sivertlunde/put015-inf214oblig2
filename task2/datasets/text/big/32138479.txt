Rogues and Romance
 
{{Infobox film
| name           = Rogues and Romance
| image          =Rogues and Romance (1920).jpg
| caption        =Still from Rogues and Romance (1920)
| director       = George B. Seitz
| producer       = George B. Seitz
| writer         = Frank Leon Smith
| narrator       =
| starring       = Marguerite Courtot June Caprice George B. Seitz
| music          =
| cinematography =
| editing        =
| distributor    = Pathé Exchange
| released       =  
| runtime        = 
| country        = United States
| language       = Silent film with English intertitles
| budget         =
}} Pirate Gold, also directed by Seitz, and was shot in Europe.   

==Cast==
* June Caprice as Sylvia Lee
* George B. Seitz as Reginald Van Ransen
* Harry Semels as Pedro Pezet
* Marguerite Courtot as Carmelita
* William P. Burt as Don Jose
* Frank Redman as Bartholomew Washington Stump

==See also==
*List of lost films

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 