Oh, Mr Porter!
 
{{Infobox film
| name           = Oh, Mr Porter!
| image          = Oh Mr Porter.jpg
| image_size     = 250px
| caption        = (left to right) Graham Moffatt as Albert, Moore Marriott as Harbottle, and Will Hay as William Porter
| director       = Marcel Varnel Edward Black
| writer         = J.O.C. Orton Marriott Edgar Val Guest
| starring       = Will Hay Graham Moffatt  Moore Marriott
| music          = Louis Levy
| cinematography = Arthur Crabtree
| editing        = R.E. Dearing   Alfred Roome
| distributor    = Gainsborough Pictures
| released       = 1937
| runtime        = 85 minutes
| country        =   English
}} British comedy film starring Will Hay with Moore Marriott and Graham Moffatt and directed by Marcel Varnel. While not Hays most commercially successful, it is probably his best-known film to modern audiences. It is widely acclaimed as the best of Hays work, and a classic of its genre.
 The Ghost Train. The title was taken from Oh! Mr Porter, a music hall song.

==Plot== Northern Irish railway station in the (fictitious) town of Buggleskelly, situated on the border with the then Irish Free State.

After taking the ferry from England to Northern Ireland, Porter is aghast when he discovers how isolated the station is. It is situated out in the countryside, two miles cross-country from the nearest bus stop. To make matters worse, local legend has it that the ghost of One-Eyed Joe the Miller haunts the line and, as a result, no-one will go near the station after dark.

Porters co-workers at the station are the elderly deputy stationmaster, Harbottle (Moore Marriott), and an overweight, insolent young porter, Albert (Graham Moffatt), who make a living by stealing goods in transit and swapping railway tickets for food. They welcome Porter to his new job by regaling him with tales of the deaths and disappearances of previous stationmasters&nbsp;– each apparently the victim of the curse of One-Eyed Joe.

From the beginning it is obvious that the station is run very unprofessionally. Porter is woken up by a cow sticking its head through the window of the old railway carriage he is sleeping in (the cow has been lost in transit and is being milked by Harbottle), and the teams breakfast consists of bacon made from a litter of piglets which the railway are supposed to be looking after for a local farmer.
 excursion to Connemara.

Porter attempts to drum up business among the local people in the pub by offering tickets to this excursion, but as the locals begin to argue about where the excursion should go a fight breaks out. Porter crawls to safety in the landlords rooms next door, where he meets a one-eyed man who introduces himself as Joe and offers to buy all of the tickets for an away game that the village football team, the Buggleskelly Wednesday, are playing the following day.
 running guns Irish Free State. The football train leaves at six a.m. the following morning, rather than the scheduled ten a.m., at the insistence of Joe and although Porter questions some of the odd packages being loaded onto the train, he accepts Joes claim that these are in fact goalposts for the game.

The train disappears as the smugglers divert it down a disused branch line near the border, and with everybody claiming that Porter has lost his mind (there is no such team as Buggleskelly Wednesday, and Harbottle points out that the local team wouldnt leave without him as he is their centre forward). Unfortunately this huge misunderstanding causes Porter to lose his job, since no one has seen the train. Then after his co-workers talk about a tunnel on a nearby disused branch line, Porter decides to head off to track down the errant engine (in hopes of getting his job back).

The trio find the missing train inside a derelict railway tunnel, underneath a supposedly haunted Terling Windmill|windmill. They investigate and are briefly captured by the gun runners, but escape and climb progressively higher up the windmill until eventually they are trapped at the top.

Using the windmill sails, they contrive to get down where they hatch a plan to capture the gun runners. Coupling the carriages containing the criminals and their guns to their own engine, Gladstone, they carry them away from the border at full speed, burning everything from Harbottles underwear to level crossing gates they smash through in order to keep up steam. To keep the criminals quiet, Albert climbs on top of the carriage and hits anyone who sticks their head out with a large shovel.

Porter writes a note explaining the situation and places it in Harbottles empty medicine bottle. When they pass a large station, he throws the bottle through the window of the stationmasters office, alerting the authorities to their plight. The entire railway goes into action, with lines being closed and other trains re-routed so that Gladstone can finally crash into a siding where the waiting police force arrest the gun runners.

After a short-lived celebration, in which Harbottle points out that Gladstone is ninety years old and Porter claims it is good for another ninety, the engine explodes after its hectic journey, and Porter, Harbottle and Albert lower their hats in respect.

==Cast==
*Will Hay as William Porter
*Moore Marriott as Jeremiah Harbottle
*Graham Moffatt as Albert Brown
*Percy Walsh as Superintendent
*Dave OToole as Postman
*Sebastian Smith as Mr Trimbletow
*Agnes Lauchlan as Mrs Trimbletow
*Dennis Wyndham as Grogan/One-Eyed Joe
*Frederick Piper as Ledbetter
*Frederick Lloyd as Minister Frank Atkinson as Irishman in bar

  where the windmill scene was filmed]]

==Production==
Despite the majority of the film being set in Northern Ireland, none of the filming took place there; the railway station at Buggleskelly was the disused Cliddesden railway station on the Basingstoke and Alton Light Railway, which had closed to goods in 1936.  Filming took place from mid-June 1937 and lasted approximately two months. {{cite book
  | last = Dean | first = Martin
  |author2=Kevin Robertson |author3=Roger Simmonds
  | title = The Basingstoke and Alton Light Railway
  | publisher = Crusader Press | year = 1998 windmill in Hawthorn Leslie in 1899 and loaned by the Kent and East Sussex Railway to the film. The engine was returned to the company after completion of the film and remained in service until 1941, when it was scrapped. {{cite web
|url=http://www.hfstephens-museum.org.uk/locomotives/new-locomotives.html
|title=Colonel Stephens New Locomotives
|publisher=The Colonel Stephens Railway Museum, Tenterden, Kent
|accessdate=2011-05-13}}  {{cite web
|url=http://www.portisheadweb.org.uk/wcpr/Northiam.html
|title=Northiam
|publisher=Weston Clevedon and Portishead Railway
|accessdate=2011-05-13
|last=Gregory|first=Paul}} 
 Waterloo to John Huntley in his book Railways on Screen, " he editor reversed his negative at one stage in preparing the title backgrounds, causing them to come out reversed on the final print".  The scene in which Porter travels to Buggleskelly by bus, while being warned of a terrible danger by locals, parodies that of the Tod Browning film, Dracula (1931 English-language film)|Dracula (1931). 

The Southern Railway of Northern Ireland that Porter works for is fictitious, in reality from the route chosen on the map the line would have belonged to the Great Northern Railway (Ireland), with Buggleskelly being close to the real town of Lisnaskea. In addition, the Irish border on the map portrayed in the film is inaccurate, placing the border too far east, and roughly along the eastern coast of Lough Erne rather than the border of County Fermanagh.

==Reception==
The film has been very well received over time:

The British Film Institute included the film in its 360 Classic Feature Films list;  Variety (magazine)|Variety magazine described the movie as "amusing, if over-long", noting that there was "   o love interest to mar the comedy"; {{cite news
| url=http://www.variety.com/review/VE1117793667.html?categoryid=31&cs=1&p=0
| work=Variety
| title=Oh, Mr. Porter! Movie Review
| date=1 January 1937}}  and the cult website TV Cream listed it at number 41 in its list of cinemas Top 100 Films.   

The film critic Barry Norman included it among his 100 best films of all time, and fellow critic Derek Malcolm also included the film in his Century of Films, describing it as "perfectly representing a certain type of bumbling British humour",  despite being directed by a Parisian director.

The director Marcel Varnel considered the film as among his best work,  and it was described in 2006, by The Times in its obituary for writer Val Guest, as "a comic masterpiece of the British cinema".  Jimmy Perry, in his autobiography, wrote that the triumvirate of Captain Mainwaring, Corporal Jones and Private Pike in Dads Army was inspired by watching Oh, Mr Porter. 

==Reviews==

===Modern reviews===
* 
* 
* 
* 

===Contemporary reviews===
* 
* 

==Parody== spoof Documentary documentary Norbert Smith - a Life, as Oh, Mr Bank Robber! starring "Will Silly". 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 