Becoming Jane
 
 
 
{{Infobox film
| name           = Becoming Jane
| image          = Becoming jane ver4.jpg
| image_size     = 
| alt            = 
| caption        = UK release poster
| director       = Julian Jarrold Douglas Rae Sarah Williams
| starring       = {{Plainlist| 
* Anne Hathaway
* James McAvoy
* Julie Walters
* James Cromwell
* Maggie Smith
}} Adrian Johnston
| cinematography = Eigil Bryld
| editing        = Emma E. Hickox Blueprint Pictures Bord Scannán na hÉireann
| distributor    = Buena Vista International   Miramax Films  
| released       =  
| runtime        = 120 minutes  
| country        = United Kingdom Ireland
| language       = English
| budget         =  $16.5 million 
| gross          = $39,380,877 
}} historical biographical Blueprint Pictures. It also received funding from the Irish Film Board and the UK Film Council Premiere Fund.
 Sarah Williams and Kevin Hood, pieced together some known facts about Austen into a coherent story, in what co-producer Graham Broadbent called "our own Austenesque landscape." According to Hood, he attempted to weave together "what we know about Austens world from her books and letters," and believed Austens personal life was the inspiration for Pride and Prejudice.  Jarrold began production of the film in early 2006, opting to shoot primarily in Ireland as he found it had better-preserved locations than Hampshire, England, where Austen was raised.

Released firstly in the United Kingdom on 9 March 2007 and in other countries later in the year, Becoming Jane earned approximately $37 million worldwide. The film received mixed reviews from critics. Hathaways performance received mixed critical reception, with some reviewers negatively focusing on her nationality and accent. Commentators and scholars have analysed the presence of Austen characters and themes within the film, and also noted the implementation of mass marketing in the films release.

==Plot==
Jane Austen is the younger daughter of Reverend George Austen and his wife and has yet to find a suitable husband. She wishes to be a writer, to the dismay of her mother and proud delight of her father.

Thomas Lefroy is a promising lawyer with a bad reputation, which he describes as "typical" for people in the profession. Tom makes a bad first impression upon meeting Jane, when he nearly falls asleep while she gives a reading of her work for the company. Overhearing his subsequent criticism, Jane cannot stand the arrogant Irishman. Meanwhile, she turns down the affections of other men, including Mr Wisley, the nephew and heir of the wealthy Lady Gresham. Wisley proposes but Jane ultimately rejects him due to lack of affection. The mischievous Tom encounters Jane again; they argue but increasingly take interest in each other and Tom demonstrates that he takes Janes literary aspirations seriously. In time they fall in love.

Tom, Jane, her brother Henry and Janes rich widowed cousin, Eliza, Comtesse de Feullide, conspire to receive an invitation from Toms uncle and benefactor, the Lord Chief Judge Langlois of London, for the rich "Madame La Comtesse" and her friends. This visit is meant to be a short break in their journey to see Janes brother, Edward. This would allow Judge Langlois to get to know Jane before and give a blessing for their marriage. Full of hope, Jane cannot sleep during the night at the Judges place. In a flow of inspiration, she then begins the writing of First Impressions, the manuscript that will become Pride and Prejudice.

However, Judge Langlois receives a letter informing him of the genteel poverty of Janes family and he refuses to give Tom his blessing, declaring that he would wish Tom to be the whoremonger he had been rather than allow him to live in poverty because of a bad marriage. Tom tells Jane that he cannot marry her and she is crushed, not knowing that Tom has a legitimate reason; his family depends on him financially.

Jane goes back home and soon learns that Tom has become engaged to someone else at the arrangement of his family. Jane accepts the marriage proposal of Mr Wisley, whom she had earlier turned down. Later, Tom realises he cannot live without Jane, and returns, asking Jane to run away with him, for "what value will there be in life, if we are not together?" Jane agrees, and they leave, with only Janes sister Cassandra knowing they plan to marry in secret.

On the way, Jane stumbles upon a letter from Toms mother, and realises his situation: he sends money he receives from his uncle back to his parents and siblings, and his family cannot survive without it. She tells Tom that they cannot elope, not with so many people depending upon him. He insists that he and Jane must marry and tells her he will earn money, but Jane tells him that it will not be enough; he will never be able to make enough money to support his dependents with a High Court judge (his uncle) as an enemy and with a penniless wife. Distraught, Tom asks her if she loves him, and she replies, "Yes, but if our love destroys your family, then it will destroy itself, in a long, slow degradation of guilt and regret and blame." She leaves to go home. Jane catches a last glimpse of Tom through the carriage window as he briefly follows, the horses outpacing him.

Twenty years later, Jane, now a successful author and by choice unmarried, sees Tom pass by during a gathering. Henry, now married to Eliza, goes after Tom and brings him to her. Tom introduces his eldest daughter, who admires Janes novels. As she asks Jane to read aloud, he remonstrates her by her name, also Jane. Astonished that he named his eldest after her, Jane agrees to read. The last scene shows Toms daughter sitting by Jane as she reads aloud from Pride and Prejudice, while Tom watches Jane affectionately. As she concludes, their eyes meet, and Tom joins the rest of the company in honouring Jane and her work with applause.

==Cast==
* Anne Hathaway as Jane Austen Thomas "Tom" Lefroy
* Julie Walters as Mrs Austen
* James Cromwell as Reverend George Austen
* Maggie Smith as Lady Gresham
* Lucy Cohu as Eliza de Feuillide|Eliza, Comtesse de Feullide
* Laurence Fox as Mr Wisley Joe Anderson as Henry Austen
* Ian Richardson as Lord Chief Judge Langlois of London Sophie Vavassuer as Jane Lefroy
* Anna Maxwell Martin as Cassandra Austen
* Leo Bill as John Warren
* Jessica Ashworth as Lucy Lefroy
* Eleanor Methven as Mrs Lefroy Mrs Radcliffe
* Tom Vaughan-Lawlor as Robert Fowle

==Production==

===Conception and adaptation===
{{Quote box
| quote  = "Its like dot-to-dot. There are documented facts and weve joined the dots in our own Austenesque landscape."
| source = — Co-producer Graham Broadbent on the films story 
| width  = 22em
| align  =right
}} Sarah Williams Douglas Rae Tom Lefroy on Christmas 1795, into a coherent story about unrequited love. Bernstein agreed to adapt the work, believing that it depicted "a pivotal relationship in Jane Austens early life that was largely unknown to the public."    The books author, Jon Hunter Spence, was hired as a historical consultant on the film,     with the task of "see  that, given that the story is a work of imagination, the factual material was as accurate as possible within the limitations of the story." 

After Williams completed several drafts of the screenplay, the company hired Kevin Hood to aid in further script development. Bernstein believed that Hoods past work contained "a romantic sensibility... There is a poetic quality about his writing as well as there being a rigorous emotional truth which I thought was important for Jane."  Hood was attracted to the film because he believed "the story is such an important one and very much the inspiration for Pride and Prejudice."  Calling Austen a "genius" and "one of the top two or three prose writers of all time", Hood thought that her relationship with Lefroy "was absolutely essential in shaping her work."  Hood acknowledged however that Becoming Jane is "based on the facts as they are known and the majority of characters did exist, as did many of the situations and circumstances in the film. Some have been fictionalised, weaving together what we know about Austens world from her books and letters, creating a rich Austenite landscape." 
 Kinky Boots, which was released later that year.    According to Bernstein, he "liked   style as it was modern and visceral, and I just had a feeling that he was the right choice. This piece needed to be handed with delicacy but also with a certain amount of brio and Julian was able to bring those two things to the production."  The director began work on the project in early 2006, rereading the novels Pride and Prejudice, Sense and Sensibility, and Persuasion (novel)|Persuasion and also reviewing Austen biographies such as Spences book. Jarrold depended most heavily on the script, calling it "a rich, witty and clever screenplay from someone who obviously knew his subject very well. It is a love story but much more besides. Kevins screenplay has so many layers and interesting ideas. Apart from the love story I was very attracted by the themes of imagination and experience."  The director intended to "bring Austen up to date by roughening her up a bit" and adding "more life and energy and fun," opining that past Austen adaptations had been "a little bit picture-postcard and safe and sweet and nice." 

===Casting===
  focused on learning an English accent, believing that if she "didnt get that right, the rest of the performance wouldnt matter because people would write me off in the first five minutes." ]]

Jarrold sought to make Becoming Jane "look and feel" realistic "so everything is not lit in a very glamorous Hollywood way."  According to him, "One of the key ideas in the film was to get away from the old, stuffy costume drama kind of feel of what Jane Austen is and to look at somebody before she becomes a genius, when she is in her early twenties and on the verge of writing her great thing; she had a real exuberance for life, intelligent and independent and a sort of outsider in rural Hampshire, more intelligent than the people around her and kicking against all those pressures."  To further set his film apart from other costume dramas, American actress Anne Hathaway was cast as the titular character. A fan of Jane Austen since she was fourteen, Hathaway immediately began rereading Austens books, conducting historical research including perusing the authors letters, and also learned sign language, calligraphy, dance choreography, and playing the piano. She moved to England a month before production began to improve her English accent, and attempted to stay in character throughout filming, the first time she had done so for a movie. 
 White Teeth.  McAvoy first assumed that Becoming Jane would be directly associated with Pride and Prejudice, with his character possessing similarities with Mr. Darcy; the actor soon realised however "that the screenplay was nothing like Pride and Prejudice. The screenwriter probably speculated on some of the inspiration for Pride and Prejudice but it is a completely different story." 
 Joe Anderson portrayed Henry Austen, while Lucy Cohu played the widowed Eliza de Feuillide, the Austens worldly cousin and Henrys romantic interest. Cohu believed that her character "needs security. She is looking to be safe. She finds that security with Henry as she knows the Austen family." 

Anna Maxwell Martin appeared as Janes sister Cassandra Austen|Cassandra. The actress called her character "terribly sensible", noting that she "gets her heart broken. Its very sad. Shes the levelling force for Jane Austen, the wild one. She tries to get her back in line, but fails miserably."  Becoming Jane also featured Laurence Fox as Mr. Wisley and Dame Maggie Smith as Lady Gresham, whom Jarrold viewed as possessing "similarities to Lady Catherine De Burgh in Pride and Prejudice but in this film you get to see her hidden vulnerabilities – the pain of never having had children and her controlling maternal power over Wisley."  Other cast members included Ian Richardson as Judge Langlois, Leo Bill as John Warren, Jessica Ashworth as Lucy Lefroy, Michael James Ford as Mr. Lefroy, Tom Vaughan-Lawlor as Robert Fowle, and Helen McCrory as Ann Radcliffe|Mrs. Radcliffe. 
 

===Costume design===
  fashions of Cannes Film Brideshead Revisited. 
 empire waistline early 1790s. She explained, "We wanted to show that transition especially for the women. The look in London is very different from the look in the countryside. For the country ball the fashion for the older women is more of the old style but for the younger women we show the introduction of the Empire line." 

The costume designer created all of Hathaways outfits from scratch, and "looked for images of a young Jane Austen."  Ní Mhaoldhomhnaigh explained, "I wanted to get her youthfulness and innocence across through her dress. But crucially there was also her strength of character. So we kept away from frills and flounces. I wanted a definite look that was quite strong but also pretty at the same time. Jane was living on a working farm so her dress had to be practical as well. In terms of the costume we were definitely trying to steer away from the chocolate box image that we associate with Jane Austen." 
 1770s fashion, which was "the sort of dress that the character would have worn when she was much younger and suited her back then. Lady Gresham is very much her own character and is not someone who is dictated to by fashion."  Smiths dresses contained stiff fabrics "to emphasise a woman who was very set in her ways". 

===Filming=== Irish counties Meath and Wicklow instead of Hampshire, the birthplace of Austen,  because it held "a sense of countryside that felt more unchanged," while Hampshire had unfortunately become too "groomed and manicured".  Jarrold also found "a great variety of Georgian houses and older houses" in Ireland.  His production received funds from the Irish Film Board,  the UK Film Council Premiere Fund,   2 Entertain, Scion Films, and Miramax Films.  Film critic Andrew Sarris noted that in Ireland "happily, there are still architectural traces of life more than 200 years ago to correspond with the year 1795."  However, Ireland did include a few disadvantages: Stewart found that "the rural aspects were the most difficult as the Irish country landscape is nothing like Hampshire. There are no rolling hills so the vegetation and the landscaping was the trickiest thing for me as a production designer." 

  in Dublin was used to represent Regency London.]] Automated Dialogue Replacement in post-production helped correct this by re-dubbing her lines. 
 Trim in Northanger Abbey.  
 Elizabethan revival estate, provided the exterior shots of the property.    Other filming sites included Cloghlee Bridge in the Dublin Mountains (as Mr. Austens rectory) and Dublins Henrietta Street and North Great Georges Street as Regency London. A house on Henrietta Street also provided the filming site for Mrs. Radcliffes residence. Gentleman Jacksons club, where Lefroy boxes, was represented by "the dark and otherworldly" Mother Redcaps tavern, also in Dublin. 

===Music===
{{Infobox album  
| Name        = Becoming Jane   
| Type        = Soundtrack Adrian Johnston
| Released    = 31 July 2007
| Genre       = Film score Sony
| Last album  = Gideons Daughter (2006)
| This album  = Becoming Jane (2007) Brideshead Revisited (2008)
| Misc        = 
}} musical score Adrian Johnston. composition for Pride & Prejudice. 

Patsy Morita, a music critic for Allmusic, wrote that the second half of Johnstons score becomes as "unremarkable" as "so many other dramatic film scores of the early twenty-first century."  She continued, "It fulfills its purpose of underscoring the emotion of the story by being moody and slow to change melodically and harmonically, and by using many pregnant pauses and minimalist-leaning repetitive figures."  Morita added that "there is nothing in it to draw attention away from the film."    The score later received a nomination for Best Original Film Score at the 2008 Ivor Novello Awards.  The film soundtrack was released on 31 July 2007.  A track listing for the album is as follows: 

{{Track listing
| collapsed       = yes
| headline        = 
| writing_credits = 
| title1          = First Impressions
| length1         = 2:27
| title2          = Hampshire
| length2         = 0:40
| title3          = Bond Street Airs
| length3         = 1:48
| title4          = Basingstoke Assembly
| length4         = 2:03
| title5          = A Game of Cricket
| length5         = 2:47
| title6          = Selbourne Wood
| length6         = 2:29
| title7          = Lady Gresham
| length7         = 2:10
| title8          = Advice From a Young Lady
| length8         = 1:06
| title9          = Laverton Fair
| length9         = 0:58
| title10          = To the Ball
| length10         = 3:17
| title11          = Rose Garden
| length11         = 2:31
| title12          = Mrs. Radcliffe
| length12         = 2:24
| title13          = Goodbye Mr. Lefroy
| length13         = 1:48
| title14          = Distant Lives
| length14         = 2:57
| title15          = The Messenger
| length15         = 1:22
| title16          = An Adoring Heart
| length16         = 1:21
| title17          = Runaways
| length17         = 2:01
| title18          = A Letter from Limerick
| length18         = 1:50
| title19          = The Loss of Yours
| length19         = 1:05
| title20          = To Be Apart
| length20         = 2:33 Le Nozze di Figaro)
| length21         = 3:22
| title22          = Twenty Years Later
| length22         = 1:19
| title23          = A Last Reading
| length23         = 2:39
}}

Downloadable editions of the original soundtrack include six bonus tracks of music heard in the film:
{{tracklist
| collapsed       = yes
| headline        = Soundtrack bonus tracks
| writing_credits = 
| title1 = The Basingstoke Assembly: The Grand Viziers Flight
| length1 = 2:26
| title2 = Lady Greshams Ball: Mutual Love
| length2 = 3:35
| title3 = The Basingstoke Assembly: Softly Good Tummas
| length3 = 3:31
| title4 = The Basingstoke Assembly: The Happy Hit
| length4 = 2:37
| title5 = The Basingstoke Assembly: A Trip to Paris
| length5 = 1:32
| title6 = Lady Greshams Ball: The Hole in the Wall" (from Henry Purcells Abdelazar)
| length6 = 2:12
}}

==Themes and analysis==

===Fictionalisation of plot===
An important deviation of the films plot from history is that in real life Austen and Tom are not known to have fallen in love. Rather, all that is known of them together is that they danced at three Christmas balls before Tom returned to school and that Jane was "too proud" to ask his aunt about him two years later. Furthermore, Jane had attempted her first full-length novel before she met Tom (contrary to the films depiction of her) and had already read  The History of Tom Jones, a Foundling before meeting him.  In a cut scene from the movie, it is clear that she is reading the novel for the second time, but in the theatrical release without that scene, it appears he introduces her to it.

===Representation of Austen characters in story=== Pride and Prejudice. 
 Anna Smith, Empire magazine }}

===Place in mass marketing===
The implementation of   (2007) and Miss Austen Regrets (2008), confirmed "the generic status of the Austen phenomenon whilst dispensing with the incorporation of the literary text."   

Becoming Jane followed a different formula than the Austen adaptations of the 1990s and attempted to draw viewers from a variety of demographic groups.  Hathaways casting was intended to attract young female viewers who had enjoyed the actress in  .  According to producers, the view was that this demographic group would have been in their early teens during the release of the Princess films, making them "the right age" for Austen as 15-year-olds. Expecting Becoming Jane to be a popular film, in February 2007 Penguin Books announced new editions of six of Austens best-known novels; their redesigned covers were intended to attract teenage readers. 

===Heritage and other themes===
 
Becoming Jane has been referred to as a heritage costume drama film, a genre which has been popular in the United States among both its audiences and its film studios. According to Andrew Higson, Becoming Jane falls into the continuing trend of American attitudes influencing English film.  Belén Vidal wrote that the film "exploit  a well-defined heritage iconography and strategically combine  American stars with supporting casts of international quality players."  Hilary Radner analysed the presence of the "marriage plot" – a girl succeeding only by marrying the man of her choice – in film and television, and noted that while Becoming Jane critiques this film trope, it "points to the power of the traditional marriage plot as a residual paradigm influencing feminine identity." 

Jarrolds adaptation also came in the wake of a number of literary   inputting their personal experiences directly into their works.  Marina Cano López and Rosa María García-Periago explained that the film "follows the path opened by John Madden’s Shakespeare in Love. The numerous intertextual connections between both movies can be reduced to one:  just as Shakespeare is imagined as the hero of his own play, Jane Austen becomes the heroine of her own novel." Among other listed similarities, they noted that the romantic interests of both protagonists serve as their literary muses, and that the middle part of both films "lie" when viewed from a historical perspective. 

==Release== world premiere of Becoming Jane took place in London on 5 March 2007.  It was released to cinemas on 9 March 2007 in the United Kingdom   and a week later in Ireland by Buena Vista International.  It ultimately grossed £3.78 million in the UK and Ireland, placing in sixteenth among all UK films for the year in those markets. Sixty-three percent of the audience was female, and 40.5 percent were above the age of 55.  The films performance was considered "disappointing", and it influenced the US release date.  It arrived in Australia on 29 March. 

Miramax Films distributed the film in the United States,  giving it a release date of 3 August 2007.  Originally, the studio intended to release Becoming Jane in June or July due to a "counter-programming" strategy,  attempting to attract demographic groups who were not interested in large Blockbuster (entertainment)|blockbusters. Higson, p. 182.  The film was expected to perform well during all seven days of the week and gradually gain more viewers during its time in cinemas. Due to the presence of recognizable stars such as Hathaway, Becoming Jane was expected to also do well among mainstream audiences. However, due to its weak UK release, the films release was moved to August, when it opened on 100 screens in its first week. It increased to 601 screens the following week, later reaching 1,210 screens.  While the film made under $1 million in its first week, it was considered "a highly respectable showing for a heritage biopic" and enough of a figure to "justify a ten-week run."  The film eventually grossed a total of $18,670,946 in the US. 

On an international scale, Becoming Jane received a total of $37,311,672. It earned its highest grosses in the US, the UK, and Australia.   

==Reception==

===Critical response===
  
Film   called the film a "triumph" for Hathaway, but observed that "the screenplay’s pseudo-Austen tone is so consistent that its lapses into modern romance-novel fantasy   threatens to derail the film."  More positive, Entertainment Weekly called the film "a charmer," articulating that "the supporting cast (Julie Walters, Maggie Smith, James Cromwell) is top-drawer; and Anne Hathaway, with her coltish beauty and frank demeanor, is a welcome Jane."   

Critics lauded Hathaway and McAvoy for the chemistry between their characters, finding that it lent authenticity to the love story between Austen and Lefroy.       While Hathaway was admired for her performance by some critics,   some reviews negatively focused on her nationality  as well as the inauthenticity of her accent.   James McAvoy defended the decision of casting Hathaway by stating that a director should, "find the right actor…and   is undoubtedly brilliant."  Hathaway herself admitted the persistent tendency to "sound too much like myself and not at all like Jane", blaming cold weather in Ireland, which meant she had to do voice retakes for several scenes.  Nonetheless, Jarrold praised Hathaway for her performance. In a wrap up party after the filming, the director confessed that the actress had been a different person, "not just her accent but also the whole character, the way of holding yourself and speaking was so completely different".   
 Time Out London gave a positive review, enunciating that "Overall, the approach is less fluffily contrived than you’d expect, and though the alignment of circumstance and social status thwarting innocent passions is hardly fresh, it’s handled with thoughtful decorum. The emotional temperature’s rather restrained as a result, but with luxury casting all down the line,... elegant visuals balancing verdant and velvet, and a delightful faux-classical score, it’s a classy package, all right – just missing the extra spark."  Some reviewers have questioned the historical accuracy of the film, for instance critiquing the depicted relationship between Austen and Lefroy.  

===Accolades===
 
{| class="wikitable" style="font-size: 95%;"
|-
! Award
! Category
! Recipients and nominees
! Result
|- British Independent Film Awards 
|  Best Actress
| Anne Hathaway
|  
|- Evening Standard British Film Awards 
| Best Actor
| James McAvoy  Also for Atonement (film)|Atonement 
|  
|- Golden Trailer Awards 
|  Best Romance TV Spot
| Becoming Jane, "Great Story", Miramax Films, Mojo LLC
|  
|- Heartland Film Festival 
|  Truly Moving Sound Award
| Becoming Jane
|  
|- Irish Film and Television Awards 
| Best Costume Design
| Eimer Ni Mhaoldomhnaigh 
|  
|-
| Best Film
| Douglas Rae, James Flynn 
|  
|-
| Best Sound
| Nick Adams, Tom Johnson, Mervyn Moore 
|  
|- Ivor Novello Awards 
| Best Original Film Score
| Adrian Johnston
|  
|- 34th Peoples Choice Awards|Peoples Choice Awards   
|  Favorite Independent Movie
| Becoming Jane
|  
|}

==Home video release== Jane Eyre.

==Impact and legacy== Sense and Austen novel.  As the city is heavily associated with Austen, the company took advantage of Becoming Jane s release in order to celebrate the author and her writings.  In late September 2007, Bath launched the seventh Jane Austen festival, which included a parade of people in Regency costumes, readings, tours, and discussions about the author. In addition, the city offered events such as Tea with Mr. Darcy to mark the release of the Becoming Jane DVD. 
 John ODonoghue, the countrys Minister for Arts, Sport and Tourism, visited the set and stated  

==See also==
 
* Timeline of Jane Austen
* Jane Austen in popular culture
* Reception history of Jane Austen
 

==Notes==
 

==References==
 

==External links==
 
*    (UK)
*   (US)
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 