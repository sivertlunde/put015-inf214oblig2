Expiration (film)
{{Infobox film
| name = Expiration
| image = ExpirationFinalPoster.jpg
| caption = Expiration Poster
| director = Gavin Heffernan
| producer = Ben Dally, Sebastian Grobys, Samantha Gutterman
| writer = Gavin Heffernan
| starring = Janet Lane, Gavin Heffernan, Erin Simkin, Yetide Badaki
| music = Jon Day
| cinematography = Ben Dally, Sebastian Grobys
| editing = Gavin Heffernan
| distributor =
| released = 2004
| runtime = 102 min.
| country = Canada English
| budget =
| preceded_by =
| followed_by =
}}
Expiration (2004) is an independent feature film written, directed and starring Gavin Heffernan. It was the winner of the Grand Jury Prize and Best Film at the Canadian Filmmakers Festival.

==External links==
* 
* 

 
 
 
 