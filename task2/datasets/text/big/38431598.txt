Girlfriend, Boyfriend
 
{{Infobox film
| name           = Girlfriend, Boyfriend
| image          = GFBFMoviePoster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 2012 Film Poster
| director       = Gillies Ya-che Yang
| producer       = Weijan Liu
| writer         = Gillies Ya-che Yang
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Bryan Shu-Hao Chang,  Hsiao-chuan Chang,  Gwei Lun-Mei,  Rhydian Vaughan
| music          = 
| cinematography = Jake Pollock
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 105 min
| country        = Taiwan
| language       = Mandarin language|Mandarin, Min Nan
| budget         = 
| gross          = 
}}
Girlfriend, Boyfriend (also stylized as Gf*Bf) is a 2012 Taiwanese film directed by Gillies Ya-che Yang. The film released on August 8, 2012.

Of the film, director Yang stated that while he did not intentionally set out to make a "gay movie, but a political one, one which happened to include a gay character".    He further stated that homosexuality and politics "both in fact represent the theme of freedom". 

==Synopsis==
In the 1980s, high school students Aaron, Mabel and Liam are best friends and also caught in a love triangle. As the three friends go through the turbulent times, when social revolution takes hold over martial law, their relationships go through many ups and downs.

==Cast==
*Bryan Shu-Hao Chang as Sean
*Hsiao-chuan Chang as  Liam
*Gwei Lun-Mei as Mabel
*Rhydian Vaughan as Aaron

==Reception== aggregator Rotten Tomatoes giving the film a rating of 64% as of February 2013 based on 11 reviews.  The New York Times cited the performance of Gwei Lun-Mei as a highlight, saying that she was "by turns brazen and uncertain, fragile and steely".  In comparison the Los Angeles Times panned the film, stating that it "  to connect" and that it "cant balance its story lines of romance and societal change". 

==References==
 

==External links==
* 
* 

 
 
 
 


 