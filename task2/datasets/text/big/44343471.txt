After Many Days
{{Infobox film
| name           = After Many Days
| image          =
| caption        =
| director       = Sidney Morgan 
| producer       = Frank E. Spring 
| writer         = Sidney Morgan Bruce Gordon   Alice Russon   Irene Browne
| music          = 
| cinematography = 
| editing        = 
| studio         = Progress Films
| distributor    = Butchers Film Service
| released       = January 1919  
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama Bruce Gordon, Alice Russon and Irene Browne. In the film, a girl believes that her father has had an illegitimate child with an artists model, but discovers that it was his criminal brother.

==Cast==
*  Bruce Gordon (actor/director)| Bruce Gordon as Paul Irving  
* Alice Russon as Marion Bramleyn  
* Irene Browne as Connie  
* Adeline Hayden Coffin as Mrs. Irving  

==References==
 

==Bibliography==
* Low, Rachael. The History of the British Film 1918-1929. George Allen & Unwin, 1971.

==External links==
*  

 

 
 
 
 
 
 
 

 