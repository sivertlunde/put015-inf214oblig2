Radio Rhythm
{{Infobox Hollywood cartoon|
| cartoon_name = Radio Rhythm
| series = Oswald the Lucky Rabbit
| image = 
| caption = 
| director = Walter Lantz Bill Nolan
| story_artist = 
| animator = Clyde Geronimi Manuel Moreno Ray Abrams Fred Avery Lester Kline Chet Karrberg Pinto Colvig
| voice_actor = 
| musician = James Dietrich
| producer = Walter Lantz
| studio = Walter Lantz Productions
| distributor = Universal Pictures
| release_date = July 27, 1931
| color_process = Black and white
| runtime = 5:56 English
| preceded_by = The Stone Age
| followed_by = Kentucky Belles
}}

Radio Rhythm is a short animated film featuring Oswald the Lucky Rabbit. It is among the vast majority of Oswald cartoons produced by Walter Lantz Productions. It is the 45th Lantz Oswald cartoon and the 97th cartoon in total.

==Plot==
Oswald is a radio personality who runs a radio station. Individuals with musical talents are selected before they perform there. The first performers consists of a rat, a horse, and a ram who sing the song And If You See Our Darling Nellie. Their act is well received by the live spectators.

The next to perform is a porcupine with a cello. But because the porcupines performance is drab, an animated microphone decides to help out by taking and playing the instrument in a more upbeat fashion. But when he gets back the cello and continues to play poorly, the porcupine gets removed from the picture.

Oswald then turns to a jazz band which performs the song One More Time. The rabbit even plays along as he takes and plays a piano. Some musicians and listeners sing to it.

As Oswald plays another tune on his piano, a male donkey decides to take a female cow for a date. When the couple enter a saloon, the donkey gets hurled out of the place by a bully who wants to take the female cow. The thrown donkey lands on and smashes Oswalds piano. Oswald then declares an end to his stations broadcast.

==See also== One More Time, a Merrie Melodies cartoon which also uses the song One More Time.

==External links==
*  at the Big Cartoon Database
* 

 

 
 
 
 
 
 
 
 
 
 


 