Palimos ng Pag-ibig (film)
{{Infobox film
  | name =  Palimos ng Pag-ibig
  | image =  PNPdamovie.jpg
  | caption = 
  | director = Eddie Garcia
  | starring = Vilma Santos Edu Manzano Dina Bonnevie
  | distributor = Viva Films
  | released = 1985
  | runtime = 126 minutes
  | country = Philippines Tagalog  English
  |}}

Palimos ng Pag-ibig (Filipino language|Filipino, "Begging for Love") is a 1985 Filipino movie directed by Eddie Garcia. The film was turned into a teleserye in 2007 for the first installment of Sineserye Presents. 

The film was based on the 1970 movie The Baby Maker that was initially adapted by Nerissa Cabral into komiks.

== Plot ==
On the outside, married couple Fina (Santos) and Rodel (Manzano) Alcaraz appear to be a match made in heaven, but behind the thick walls of their home, their relationship is on the verge of crumbling. Despite being relatively affluent, the couple is empty as they are childless, for any attempt to conceive might prove fatal to Fina due to her condition. In an act of desperation, Rodel takes matters into his own hands and seeks the services of a surrogate pregnancy|surrogate, Ditas (Bonnevie). 

The plan goes awry when Rodel becomes genuinely attracted to the younger and more alluring Ditas, who has lived a destitute life, finds the notion of prosperity equally irresistible. The well-intentioned plan to resuscitate life back into a dying marriage becomes its undoing.

== Cast ==
* Vilma Santos as Fina Alcaraz
* Edu Manzano as Rodel Alcaraz  
* Dina Bonnevie as Ditas 
* Cherie Gil as Verna Castillo
* Laurice Guillen as Mitos   
* Pepito Rodriguez as Reggie Alcaraz

== Origins ==
Palimos ng Pag-ibig is an adaptation of the 1970 film, The Baby Maker  directed and co-written by James Bridges. It was first serialized in Komiks by Nerissa Cabral before it was adapted for film.

== TV adaptation ==
 

In 2007, ABS-CBN remade the film into a TV series as the first installment of Sineserye Presents. It stars Kristine Hermosa as Ditas, Diether Ocampo as Rodel and Rica Peralejo as Fina.

== In popular culture ==
The film was the origin of the line, "Para kang karenderiáng bukás sa lahát ng gustong kumain!" ("Youre like an eatery open to anyone who wants to eat!"). The line refers to a character accusing another of engaging in prostitution.

== External links ==
*  

 
 
 
 
 


 