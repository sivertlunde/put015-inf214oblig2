Tom Jones (1917 film)
 
 
{{Infobox film
| name = Tom Jones
| image =
| caption =
| director = Edwin J. Collins
| producer =
| writer = Henry Fielding (novel)   Eliot Stannard
| starring = Langhorn Burton Sybil Arundale Will Corrie   Wyndham Guise
| music =
| cinematography =
| editing =
| studio = Ideal Film Company
| distributor = Ideal Film Company
| released = 1917
| runtime =
| country = United Kingdom
| awards =
| language = English
| budget =
| preceded_by =
| followed_by =
}} British comedy Tom Jones by Henry Fielding. After being disgraced at home Tom Jones enjoys a series of adventures on the road to London.

==Cast==
* Langhorn Burton - Tom Jones
* Sybil Arundale - Molly Seagrim
* Will Corrie - Squire Western
* Wyndham Guise - Squire Allworthy
* Bert Wynne - William Blifil
* Nelson Ramsey - Thwackum
* Dora De Winton - Miss Western
* Jeff Barlow - Lieutenant Waters

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 