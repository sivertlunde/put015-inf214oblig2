The Merry Old Soul
{{Infobox Hollywood cartoon|
| cartoon_name = The Merry Old Soul
| series = Oswald the Lucky Rabbit
| image = OswaldAndKing.jpg
| caption = Oswald and Old King Cole in The Merry Old Soul. Bill Nolan
| story_artist = Walter Lantz Bill Nolan
| animator = Manuel Moreno Lester Kline Fred Kopietz George Grandpre Ernest Smythe
| voice_actor = 
| musician = James Dietrich
| producer = Walter Lantz
| distributor = Universal Pictures
| release_date = November 27, 1933
| color_process = Black and white
| runtime = 8:02 English
| preceded_by = The Zoo Parking Space
}}

The Merry Old Soul is a short animated film by Walter Lantz Productions, and part of the Oswald the Lucky Rabbit series. The cartoon also became a nominee for an Academy Award. {{cite web
|url=http://lantz.goldenagecartoons.com/1933.html
|title=The Walter Lantz Cartune Encyclopedia: 1933
|accessdate=2011-06-03
|publisher=The Walter Lantz Cartune Encyclopedia
}} 

==Plot==
Oswald goes to a dental clinic for treatment to his cavity problem. While the dentist tries to pull out his damaged tooth, news break out of a radio in the shop, reporting that the king is in serious depression and needs to be cheered up. In this, Oswald abandons his dental treatment, and heads outside to spread the word.

In his castle, the king is having a long face for some reason. His personal jester tries to brighten him up but to no avail, and therefore leaves the throne room. Oswald then shows up and reads to him a book of comical rhymes. The rabbits attempt helps a little and the kings mood improves slightly. Other entertainers also come in for a similar cause. Each of them has a distinctive method in providing comedy. It all comes down when some of them employs an act that involves hurling pies at each other. Eventually, everyone in the room joins the foodfight, and so does the king who starts to enjoy it.

Momentarilly, the jester returns to the throne room to see what is happening. Upon noticing the king back in good spirits, the jester grows envious, and decides to take his grudge on one of the entertainers. The jester captures Oswald and brings the rabbit into a torture chamber at a basement of the castle. He then puts a noose around Oswalds neck, with other end of the rope being pulled by three elephants. Oswald finds this ordeal impossible to get out of.

It appears the experience at the castle was nothing more than a nightmare as Oswald wakes up and sees himself still at the dental clinic. The dentist, at last, succeeds in extracting the tooth, and Oswald is relieved of his dental worries.

==Availability==
The short is available on The Woody Woodpecker and Friends Classic Cartoon Collection DVD box set.

==See also==
*Oswald the Lucky Rabbit filmography

==References==
 

==External links==
*   at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 
 
 


 