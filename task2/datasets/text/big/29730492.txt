Wuthering Heights (2011 film)
 
 
 
{{Infobox film
| name           = Wuthering Heights
| image          = WutheringHeights2011_poster.jpg
| image_size     =
| alt            =
| caption        =
| director       = Andrea Arnold
| producer       = Robert Bernstein Kevin Loader Douglas Rae
| writer         = Andrea Arnold Olivia Hetreed
| based on       =  
| starring       = Kaya Scodelario James Howson Oliver Milburn
| music          = Robbie Ryan
| editing        = Nicolas Chaudeurge
| studio         = HanWay Films Ecosse Films Film4
| distributor    = Artificial Eye   Oscilloscope Laboratories  
| released       =  
| runtime        = 129 minutes  
| country        = United Kingdom
| language       = English
| budget         = £5 million 
| gross          = $1,742,215 (worldwide) 
}} romantic drama Catherine and James Howson as Heathcliff (Wuthering Heights)|Heathcliff. The screenplay, written by Andrea Arnold and Olivia Hetreed, is based on Emily Brontës 1847 Wuthering Heights. 

== Cast ==
* Kaya Scodelario as Catherine Earnshaw Heathcliff
* Oliver Milburn as Mr. Linton
* Nichola Burley as Isabella Linton
* Eve Coverley as Young Isabella Linton James Northcote as Edgar Linton
* Lee Shaw as Hindley Earnshaw
* Amy Wren as Frances Earnshaw
* Shannon Beer as Young Catherine Earnshaw
* Solomon Glave as Young Heathcliff
* Steve Evets as Joseph Paul Hilton as Mr. Earnshaw
* Simone Jackson as Nelly Dean
* Jonny Powell as Young Edgar Linton
* Michael Hughes as Hareton

==Production== Cathy in a new film adaptation of the novel, but she withdrew in May.   In May 2008, director John Maybury cast Michael Fassbender as Heathcliff and Abbie Cornish as Cathy.   However, in May 2009, Peter Webber was announced as the new director, with Ed Westwick and Gemma Arterton attached to play Heathcliff and Cathy respectively.  However, the film did not get off the ground and in January 2010, it was announced that Andrea Arnold would direct the adaptation.    In April, she cast Kaya Scodelario as Catherine,  a more age-appropriate choice than previous adaptations.   
 Romani community. However, the community had some doubts.    The search was then expanded to Yorkshire actors aged 16 to 21 of mixed race, Indian, Pakistani, Bangladeshi or Middle Eastern descent.    In November, it was reported that James Howson had been cast as Heathcliff, the first time a black actor would portray the role.        Lucy Pardee was in charge of casting the children in the film (Gail Stevens cast the adults ). Pardee auditioned private school children with no history of acting. 
 Cotescue Park Wuthering Heights)  and with the production office being temporarily based in Hawes during filming. 

==Promotion and release==
The first footage of the film released was a four-shot teaser at Film4s pre-Cannes Film Festival party, with The Guardian noting that the teaser "wowed" the partygoers (including Venice Film Festival artistic director Marco Mueller who was present to scout films for his festival). 
 2011 Venice Film Festival  and appeared at the 2011 Toronto International Film Festival as a Special Presentation.  It was also shown at the London Film Festival,  Zurich Film Festival,  Maryland Film Festival, and the Leeds Film Festival.  The film was released in the UK on 11 November. 

Grammy Award-nominated band Mumford & Sons have recorded two songs for the film, one of which (entitled "Enemy") will be played over the closing credits. 

Photographer Agatha A. Nitecka shot promotional material for the film including photos for the poster, DVD cover, magazines and a photo-essay.   Film4 released the first promotional photo of James Howson as Heathcliff to their Twitter account the morning the Venice Film Festival line-up was announced.  With the announcement that the film would play at the Toronto Film Festival, four new promotional images were released. 

An exhibit of film stills and photographs taken on the set by Agatha A. Nitecka was displayed in Curzon Cinemas Renoir location and her photo-essay was available free to every customer who purchased a ticket. A video of the photo-essay was also released online. 

The film was released on 5 October in the United States.

==Critical and commercial reception==
The film took £156,931 on its opening weekend at the box office in the United Kingdom, placing at 16th for the weekend of 11–13 November 2011.  The film holds a 68% fresh rating on Rotten Tomatoes  and a 70% on Metacritic. 

Andrew OHehir of Salon (magazine)|Salon placed the film at number one on his list of the top 10 best films of 2012. 

==Awards==
{| class="wikitable plainrowheaders" width="95%;"
|-  style="background:#ccc; text-align:center;"
! scope="col" | Award
! scope="col" | Date of ceremony
! scope="col" | Category
! scope="col" | Nominee(s)
! scope="col" | Results
! scope="col" | Ref
|-
! scope="row" | Alliance of Women Film Journalists
| January 7, 2013
| Best Woman Director
| Andrea Arnold
|  
|    
|-
! scope="row" | Black Reel Awards 7 February 2013
| colspan="2" | Best Foreign Film
|  
|
|-
! scope="row" | Camerimage
| 3 December 2011
| Bronze Frog
| rowspan="2" | Robbie Ryan
|  
|  
|-
! scope="row" | Evening Standard British Film Awards
| 7 February 2012
| London Film Museum Award for Technical Achievement
|  
|  
|-
! scope="row" | Golden Trailer Awards
| 5 May 2013
| Technical Categories - Best Sound Editing
| Oscilloscope Laboratories Mark Woolen & Associates
|  
|  
|-
! scope="row" | International Istanbul Film Festival
| 15 April 2012
| FIPRESCI Award / International Competition
| Andrea Arnold
|  
|  
|-
! scope="row" | Irish Film & Television Awards 11 February 2012
| Best Director of Photography (Film / TV Drama)
| rowspan="3" | Robbie Ryan
|  
|  
|-
! scope="row" | London Film Critics Circle 19 January 2012
| Technical Achievement of the Year
|  
|  
|- Valladolid International Film Festival
| rowspan="2" | 29 October 2011
| Best Director of Photography
|  
| rowspan="2" |    
|-
| Honorable Mention to Young Actors
| Solomon Glave Shannon Beer
|  
|-
! scope="row" rowspan="2" | Venice Film Festival 10 September 2011
| Golden Lion
| Andrea Arnold
|  
|  
|- Osella for Best Cinematography
| Robbie Ryan
|  
|    
|}

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 