Ganda Hendathi
{{Infobox film
| name           = Ganda Hendathi
| image          = 
| image_size     = 
| caption        =
| director       = Ravi Shrivatsa
| producer       = Shailendra Babu
| writer         = Anurag Basu
| based on       = Murder (film)|Murder (2004)
| screenplay     = 
| narrator       = 
| starring       = Vishal Hegde Sanjjanaa   Thilak Shekar  
| music          = Gurukiran
| lyrics         = V. Manohar   Hrudaya Shiva
| cinematography = Mathew Rajan
| editing        = Lakshman Reddy
| studio         = Sri Shailendra Productions
| released       = July 27, 2006
| runtime        = 141 minutes
| country        = India
| language       = Kannada
| budget         = 
}}
 Kannada thriller erotic thriller film, directed by Ravi Shrivatsa and produced by Shailendra Babu. This film stars newcomers Vishal Hegde, Sanjjanaa and Thilak Shekar in lead roles. This movie is only for adults as children are prohibitted to see it. 

The film is a remake of Hindi hit film Murder (film)|Murder (2004), starring Mallika Sherawat, Emraan Hashmi and Ashmit Patel. The film created huge controversies upon release for its excessive erotic scenes and content.  The score and soundtrack was composed by Gurukiran for the lyrics of V. Manohar and Hrudaya Shiva. 

==Plot==
The story follows Simran (Sanjjana), a young woman married to Sudhir,  a workaholic. Due to her lonely, passionless married life, she begins an affair with her college love, Sachin (Thillak Shekar) , whom she accidentally meets. She starts telling lies to Sudhir so as to meet Sunny daily. She gets so engrossed in their affair that she forgets all her commitments as a wife and mother, and she realises that it is grossly wrong. She decides to end the affair but, to her surprise, she finds another woman named Radhika with him. Simran then realises the mistake of sleeping with Sunny just because of an unhappy marital life. She laments her unfaithful act.
Meanwhile, Sudhir starts to have doubts about his wife, and hires a detective to trace Simrans whereabouts. His doubts are confirmed when the detective provides him with photographs of the two together. He also finds out that Sunny is a womaniser, with many girlfriends. The next day, Sunny suddenly goes missing. The police come to their house and inquire about him on a report registered by his girlfriend. Simran sees the photographs of her and Sunny in Sudhirs pants, and realises that Sudhir has something to do with Sunny being missing. It is revealed that when Sudhir goes to Sunnys apartment asking him to end the affair, he fatally beats him and buries his body, in the heat of the moment. Simran accepts her mistake, supports Sudhir, and then tries to take the blame of Sunnys murder on herself. This develops and strengthens their love and faith. With Sunny murdered, she is arrested by the police and pleads guilty. On the other hand, Sudhir claims that it is he who killed Sunny. This confuses the police. Moreover, the body is missing from the burial spot.
The story takes a U-turn when it is found that this "murder" never took place; rather it was a plot planned by Sunny with the aid of Radhika to separate Simran and Sudhir, so that he could continue his love affair with Simran. Only Simran comes to realise this, and she is trapped by Sunny. Sunny chases Simran into a jungle, where Sudhir arrives and has another fight with Sunny. He manages to beat him, and Sunny leaves as he sees the two together, seemingly realising their love for each other. However, Sunny then runs up behind Sudhir with a shovel but is then shot in the back by a police officer, who arrives just in time. The couple reunites with the love developed on the strong foundations of the test of mutual support and understanding to lead a happily married life.
==Cast==
* Vishal Hegde as Susheel
* Sanjjanaa as Sanjana
* Thilak Shekar as Sachin
* Ravi Belagere 
* Manju Bhashini
* Rachana Mourya in a special appearance
* Praveen

==Soundtrack==
The music was composed by Gurukiran with most of the songs directly inspired from the original compositions by Anu Malik. 

{| class=wikitable sortable
|-
! # !! Title !! Singer(s) || Lyrics
|-
|  1 || "Oho Nasheyo" || Gurukiran || V. Manohar
|-
|  2 || "Nidiregu Raja" || Kunal Ganjawala || Hrudaya Shiva
|-
|  3 || "Dont Let Me" || Kunal Ganjawala || Hrudaya Shiva
|-
|  4 || "Maathu Muride" || Kunal Ganjawala || V. Manohar
|-
|  5 || "Maathu Muride"  ||  M. D. Pallavi Arun || V. Manohar
|-
|}

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 


 

 