Meenamasathile Sooryan
{{Infobox film
| name           = Meenamasathile Sooryan
| image          =
| caption        =
| director       = Lenin Rajendran
| producer       = CG Bhaskaran
| writer         = Lenin Rajendran
| screenplay     = Lenin Rajendran Murali Vijay Menon
| music          = MB Sreenivasan
| cinematography = Shaji N Karun
| editing        = Ravi
| studio         = Sauhruda Chithra
| distributor    = Sauhruda Chithra
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, Murali and Vijay Menon in lead roles. The film had musical score by MB Sreenivasan.   

==Cast==
 
*Shobhana as Revathy
*Venu Nagavally as Appu Murali as Abubakar
*Vijay Menon as Chirukandan
*Kakka Ravi as Kunjambu Nair (voice dubbed by Narendra Prasad)
*Sukumari as Abubakars mother Innocent as Adikari
*KPAC Lalitha as Chirukandans mother
*Nedumudi Venu as Pokkayi
*Vinayan as  Madhavan 
*Bharath Gopi as EK Nayanar Mashu
*Prasannan
*Krishnankutty Nair as Police officer
*Achankunju as Police officer
*Balan K Nair as Police officer
*KPAC Sunny as Jail warden
*Kannur Sreelatha as Kalyani
*Karamana Janardanan Nair as Janmi
*Ragini as Jaanu/Appus wife
*Soorya
*Thrissur Elsy
 

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by Ezhacheri Ramachandran and ONV Kurup. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Adaykkaakkuruvikal || S Janaki || Ezhacheri Ramachandran || 
|-
| 2 || Elelam kilimakale || K. J. Yesudas || Ezhacheri Ramachandran || 
|-
| 3 || Iruttinethire ||  || Ezhacheri Ramachandran || 
|-
| 4 || Ithanu Kayyoor ||  || ONV Kurup || 
|-
| 5 || Maarikkaar meyunna || K. J. Yesudas, Vani Jairam || Ezhacheri Ramachandran || 
|-
| 6 || Sathyame || Chorus || Ezhacheri Ramachandran || 
|}

==References==
 

==External links==
*  

 
 
 

 