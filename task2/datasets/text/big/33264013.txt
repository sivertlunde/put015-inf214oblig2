Strawberries in the Supermarket
 
{{Infobox film
| name           = Jagoda in the Supermarket Jagoda u supermarketu  (original title) 
| image          = Jagoda in the Supermarket.jpg
| caption        = DVD cover
| director       = Dušan Milić
| producer       = Emir Kusturica
| writer         = Dušan Milić
| narrator       = 
| starring       = Srđan Todorović Branka Katić
| music          = Nele Karajlić Dejo Sparavalo
| cinematography = Petar Popović
| editing        = Svetolik Zajc
| distributor    = Fandango (Italy) Kinowelt Home Entertainment (Germany)
| released       = 6 February 2003 
| runtime        = 83 min. Italy Cinema Germany Cinema Yugoslavia
| German Serbian Serbian Italian Italian
| gross         = €81,184
}} Yugoslav  comedy-action film directed by Dušan Milić. The film won the first prime at the 2004 Cinequest Film Festival in San Jose, California.  Branka Katić cast in the title role. 
 Berlin Film Festival Perry Seibert, Rovi.   

== Cast ==
* Branka Katić as Jagoda Dimitrijevic
* Srđan Todorović as Marko Kraljevic
* Dubravka Mijatović as Ljubica
* Goran Radaković as Nebojša
* Danilo Lazović as SWAT Commander
* Mirjana Karanović as Supermarket owner Nikola Simić as assassin “Kobac”
* Zorka Manojlović as grandma

;Rest of cast listed alphabetically:
* Đorđe Branković as young cop
* Stela Ćetković as Draga
* Branko Cvejić	as Dragan
* Milan Đorđević as Medic
* Nina Grahovac as cashier
* Marko Jeremić	as Limun
* Snežana Jeremić as cashier

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 