Sundaattam
 
{{Infobox film
| name = Sundaattam
| image = Sundattam.jpg
| caption = Poster
| director = Brahma G. Dev
| producer = R. Annadurai
| story = Brahma G. Dev
| screenplay = Brahma G. Dev Irfan Arundhati Arundhati Madhu Stalin Naren
| music = Britto Michael Achu Rajamani (BGM)
| cinematography = Balagurunathan
| editing = LVK. Dass
| studio = Film Fame Production
| released =  
| runtime =
| country = India
| language = Tamil
| budget =
| gross =
}}
 Tamil film Irfan play Arundhati as  heroine. Britto has composed the music and Achu of Maalai Pozhudhin Mayakathilaey fame, the background score.  Sundattam is a film based on Carrom gambling in Royapuram area in North Madras.  The film, which is based on a real life incident set in the backdrop of Chennai in the 1990s, released on 8 March 2013 to average reviews. 

==Cast== Irfan as Prabhakaran Arundhati as Kalaivani
* Madhu as Kasi
* Aadukalam Naren as Bhagya Annachi
* Stalin as Guna

==Soundtrack==
{{Infobox album
| Name = Sundaattam
| Longtype =
| Type = Soundtrack
| Artist = Britto Michael
| Cover = 
| Released = 2012
| Recorded =  Feature film soundtrack
| Length =  Tamil
| Label = 
| Producer = Britto Michael
| Reviews =
| Last album = 
| This album = Sundaattam (2012)
| Next album = 
}}
{{Track listing extra_column = Singer(s) music_credits = no lyrics_credits = yes

|title1= Narumugaye
|extra1= Aalap Raju, Madhumitha
|lyrics1= Snehan
|length1= 4:32
|title2= Adi Unnale Karthik
|lyrics2= Na. Muthukumar
|length2= 4:14
|title3= Nethiyilae
|extra3= Kaali
|lyrics3= Marana Gana Viji
|length3= 4:30
|title4= Kan Kondu
|extra4= Britto
|lyrics4= Britto
|length4= 2:46
|title5= Kadhal Varum Varai
|extra5= Saindhavi
|lyrics5= Palani Bharathi
|length5= 4:38
|title6= Vizhiyil Vithai
|extra6= Ranjith (singer)|Ranjith, Madhu, Roshini
|lyrics6= Britto
|length6= 3:37
}}

==References==

 

==External links==
*  

 
 
 
 


 