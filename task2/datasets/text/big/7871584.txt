Schramm (film)
{{Infobox film name        = Schramm image       = Schramm_DVD_Cover.jpg caption     = director    = Jörg Buttgereit writer      = Jörg Buttgereit Franz Rodenkirchen starring    = Florian Koerner von Gustorf Monika M. Micha Brendel Carolina Harnisch music       = Max Müller Gundula Schmitz editing     = Jörg Buttgereit Manfred O. Jelinski producer    = Manfred O. Jelinski distributor = released    =   runtime     = 65 min. country     = Germany language  German
|budget      =
}}
 German horror film directed by controversial taboo-breaking auteur Jörg Buttgereit. It is stylised and artfully shot on 16mm film. It tells  the story of a man who has been known in the media as the Lipstick Killer. Loosely based on true crime criminology profiles of Carl Panzram and similar serial killers.

== Synopsis ==

Lothar Schramm is a polite, neighbourly cab driver who makes an honest living and invites callers in for cognac. Later on, he might slit their throats and assemble their bodies in suggestive poses. He lives next door to a young, beautiful prostitute named Marianne, with whom he is smitten. Schramm is lonely. His sex life is seriously deranged and his social life is nonexistent. He makes love to inflatable plastic dolls, fantasizes about vaginas with teeth, nails his foreskin to tables and dreams of a visit to the dentist who extracts him an eyeball. He has constant flashbacks and paranoid delusions of his knee getting amputated. He whitewashes bloodstains off the walls of his flat. When Marianne is invited by some affluent gentlemen clients to a villa outside of town, she asks Schramm to chauffeur her so shell be safe. He accepts and he invites her to a friendly dinner, ignoring his desire for her. He takes her back to his flat, where he drugs her and strips her. He snaps photos and masturbates spitefully over her naked body. The next day Marianne rings at his door for a lift, but Schramm does not answer. He has fallen from a ladder while painting over the blood on his walls. His head has cracked on the floor. The next thing we see is Marianne in the villa outside of town, attired like a Hitler youth, bound and gagged on a chair, helpless victim to her eccentric clients.

Papers declare the Lonesome Death of Lipstick Killer.

== External links ==
*  
*  

 
 
 
 
 
 
 

 
 