Woman Thou Art Loosed
{{Infobox film
| name =  Woman Thou Art Loosed
| image = Woman Thou Art Loosed poster.jpg
| director = Michael Schultz
| based on =  
| screenplay = Stan Foster
| starring = Kimberly Elise Loretta Devine Debbi Morgan Michael Boatman Clifton Powell Idalis DeLeon T.D. Jakes
| music = Todd Cochran
| cinematography = Reinhart Peschke
| editing = Billy Fox 
| distributor = Magnolia Pictures
| released =  
| runtime = 94 minutes
| country = United States
| language = English
| budget = $3 million
| gross = $6,804,016
}}
Woman Thou Art Loosed is a 2004 film directed by Michael Schultz and written by Stan Foster. It was produced by Stan Foster and Reuben Cannon. It is the 44th film or series directed by Schultz and is adapted from the Woman Thou Art Loosed (novel)|self-help novel by Bishop T.D. Jakes.  The film tells the story of a young woman who must come to terms with a long history of sexual abuse, drug addiction, and poverty. It has been reported that the story was loosely based on the screenwriters past relationship with a college girlfriend. A gospel stage play preceded the film. It was directed by Tyler Perry and written by Terry McFaddin.

==Cast==
*Kimberly Elise as Michelle Jordan
*Loretta Devine as Cassey Jordan
*Debbie Morgan as Twana
*Michael Boatman as Todd
*Clifton Powell as Reggie
*Idalis DeLeon as Nicole
*T.D. Jakes as Himself
*Sean Blakemore as Pervis
*Destiny Edmond as Michelle as age 12 and 8
*Philip Bolden as Todd at age 8
*Maula Gale as Church Goer

==Awards==
The film was nominated for two Image Awards, and won the award for Outstanding Independent or Foreign film; Elise and Devine also received nominations at the Independent Spirit Awards for Best Lead Female and Best Supporting Female, respectively.

Woman Thou Art Loosed was also awarded at the American Black Film Festival for Best Film.

==Sequel==
A sequel, Woman Thou Art Loosed: On the 7th Day was released on April 13, 2012. 

==See also==
*2004 Movies

==References==
*  
* 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 