Gorosthaney Sabdhan
{{Infobox film
| name           = Gorosthaney Sabdhan
| image          = Gorosthaney Sabdhan poster.jpg
| caption        = Gorosthaney Sabdhan poster
| director       = Sandip Ray
| producer       = Mou Roychoudhury
| writer         = Satyajit Ray
| starring       = Sabyasachi Chakrabarty   Bibhu Bhattacharya   Saheb Bhattacharya
| music          = Sandip Ray
| cinematography =
| editing        =
| distributor    = V3G Films Private Limited,
| released       =  
| runtime        =
| country        = India
| language       = Bengali
| budget         = Rs.60 lakh
| gross          = Rs.1.0 crore
}} Tintorettor Jishu.

==Plot==

By accident, more than anything else, the three find themselves beside the grave of Thomas Godwin. The grave was dug up by some miscreants for unknown reasons. The rather colorful history of Mr. Godwin makes Feluda curious to know more about the man.From the diary of Thomas daughter Charlotte, Feluda finds that a very precious clock went to Thomas grave with him. To his surprise, Feluda finds that another party knows about this clock and they are trying to get it aided by the letter with them. Thanks to the brilliance of the detective and the help of Haripodobabu, the chauffeur of Mr. Ganguli, a new introduction in this book, their plot is foiled.

The Old   was the capital of British India. Just as the story of the Nawabs plays a vital part in Badshahi Angti (based on Lucknow), the story of British families who lived in the former capital of the British Raj, plays a prominent part in this story. Feluda goes to a Christian cemetery, to see the graves of the members of the Godwin family. He goes to Ripon Lane to meet a living member of the family. Later on, he finds that there is an Anglo-Indian branch of the family as well.

==Cast==
* Sabyasachi Chakrabarty as Feluda
* Saheb Bhattacharya as Topshe Jatayu
* Dhritiman Chatterjee
* Subhashish Mukherjee
* Haradhan Bandopadhyay
* Tinnu Anand
*

==External links==
* 
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 