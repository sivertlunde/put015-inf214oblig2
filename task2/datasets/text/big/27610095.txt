Zarkorr! The Invader
{{Infobox Film
| name           = Zarkorr! The Invader
| image          = Zaklorrr-invader-aff.jpg
| caption        = DVD artwork 
| directors      = Michael Deak Aaron Osborne
| producers      = Albert Band Charles Band Sally Clarke Michael Deak Kirk Edward Hansen Rob Martin Steve Sechrest
| writer         = Benjamin Carr Elizabeth Anderson
| music          = Richard Band
| distributor    = Full Moon Entertainment, Monster Island Entertainment (VHS and DVD)
| released       =  
| budget         = 
| runtime        = 80 minutes
| country        = United States
| language       = English
}}

Zarkorr! The Invader is a 1996 Low-budget film|low-budget monster movie produced by Full Moon Entertainment.

It was planned for a limited theatrical release, but was instead was made a direct-to-video film.

==Plot==
Intelligent aliens who have been studying Earth for centuries decide to challenge mankind by sending in a 185-foot, laser-eyed monster called Zarkorr to wreak city-crushing havoc. Only one incredibly average young man, postal worker Tommy Ward (Rhys Pugh), can find the beasts weakness and save the planet with the help of a 6-inch-tall pixie (Torie Lynch), who says she is "a mental image projected into his brain" by the aliens. She explains that Zarkorr cannot be destroyed by weapons, but that the key to the monsters destruction lies within the monster itself. Tommy, chosen as an average human, is the one destined to fight Zarkorr, who is programmed to kill him. Tommy asks scientist Dr. Stephanie Martin (DePrise Grossman) for advice about his mission, but everyone thinks he is crazy. He takes the scientist hostage, but manages to explain his predicament to one of the policemen, who believes him and helps him escape. Dr. Martin agrees to help him. Using computers belonging to a friend of hers, they establish that the monster, which is destroying city after city in the style of Godzilla, neither sleeps nor breathes. Going to the place where the monster first appeared, they come into possession of a strange metallic capsule that fell out of the sky at the time the monster arrived. It is believed to be unopenable, but it opens by itself for Tommy as he touches it. He uses the top of the capsule as a shield, reflecting Zarkorrs laser rays back at him, and the monster dissolves into a small glowing sphere flying into space. Tommy is taken to hospital to recover; a TV reporter congratulates him for saving the world, and he jokes he might run for president. Mitchell, Charles P. A guide to apocalyptic cinema, Greenwood Publishing Group 2001,  , ISBN 978-0-313-31527-5 

==Cast==
* Franklin A. Vallette as Horrace
* Don Yanan as Dunk
* Peter Looney as Billy
* Dyer McHenry as Al
* Rhys Pugh as Tommy Ward
* Torie Lynch as Proctor
* Stan Chambers as Stan Elizabeth Anderson as herself
* Robert Craighead as Marty Karlson
* Dileen Nesson as Debby Dalverson
* DePrise Grossman as Stephanie Martin
* Mary Ostow as Reporter
* Jim Glassman as Stage Manager
* Emmett Grennan as Crew Member
* Mike Terner as Guard One
* Robert J. Ferrelli as Guard Two
* Ron Barnes as Larry Bates
* Mark Hamilton as George Ray Charles Schneider as Arthur

==Production== Jurassic Park.

==Critical reception==
Zarkorr! The Invader received negative reviews from critics, but has a cult following who tend to praise the monster Zarkorr, but not the movie as a whole.

Review aggregator Rotten Tomatoes reports that 13% of critics have given the film a positive review based out of 1 review, with an average rating of 1.3/5. There is currently no critical consensus on the website.

==References==
 

==External links==
*  
*  
*   at Badmovies.org
*  

 
 
 
 
 
 
 