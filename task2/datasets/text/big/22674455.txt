Río Escondido (1948 film)
 
{{Infobox film
| name           = Río Escondido
| image          = Rio_escondido.jpg
| caption        = 
| director       = Emilio Fernández
| producer       = Raúl de Anda
| writer         = Emilio Fernández Mauricio Magdaleno Fernando Fernández Carlos López Moctezuma Columba Domínguez
| music          = Francisco Domínguez
| cinematography = Gabriel Figueroa
| editing        = Gloria Schoemann
| distributor    = Producciones Raul de Anda
| released       =  
| runtime        = 110 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}
Río Escondido is a 1948 Mexican drama film directed by Emilio Fernández and starring María Félix.

==Plot==
The President of México sends Rosaura (María Félix), a young teacher, to educate a remote rural village known as Río Escondido. The goodwill of Rosaura is impaired by  Don Regino Sandoval (Carlos López Moctezuma), the towns tyrannical sheriff. The situation worsens when Regino falls in love with Rosaura, making it a living hell that she can only win through her strong will.

== External links ==
* 

 

 
 
 
 
 
 
 

 
 