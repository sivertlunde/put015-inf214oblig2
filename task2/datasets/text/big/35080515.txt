Danton (1932 film)
{{Infobox film
| name           = Danton
| image          =
| caption        =
| director       = André Roubaud
| producer       = 
| writer         = 
| starring       =  Jacques Grétillat, Jacques Dumesnil and Andrée Ducret
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1932 (France)
| runtime        = 
| country        = France
| awards         = French
| budget         = 
| preceded_by    =
| followed_by    =
}}
 French historical historical drama French revolutionary Georges Danton and his eventual execution by hardliners of the Revolution. It was based in part on the 1929 play The Danton Case by Stanisława Przybyszewska. 

==Cast==
* Jacques Grétillat - Danton
* Andrée Ducret - Gabrielle Danton
* Jacques Dumesnil - Fabre dÉglantine
* André Fouché - Camille Desmoulins
* Simone Rouvière - Lucile Desmoulins
* Octave Berthier - Le père Charpentier
* Marguerite Weintenberger - Louise Danton
* Louis Merlac - Le boucher Legendre
* Thomy Bourdelle - Le général Westermann

==References==
 

==Bibliography==
* Oscherwitz, Dayna. Past Forward: French Cinema and the Post-Colonial Heritage. SIU Press, 2010.
* Stam, Robert & Raengo, Alessandra. A Companion to Literature and Film. John Wiley & Sons, 2004.

==External links==
* 

 
 
 
 
 
 
 
 

 
 