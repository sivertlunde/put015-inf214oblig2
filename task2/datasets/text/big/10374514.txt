I as in Icarus
{{Infobox film
| name           = I as in Icarus
| image          = Icare_affiche.jpg
| image_size     =
| caption        = 
| director       = Henri Verneuil
| producer       = 
| writer         = Didier Decoin Henri Verneuil
| starring       = Yves Montand
| music          = Ennio Morricone
| cinematography = Jean-Louis Picavet
| editing        = Henri Lanoë
| distributor    = 
| released       = 19 December 1979
| runtime        = 120 minutes
| country        = France French
| budget         = 
| gross          = 
}}
I as in Icarus ( ) is a 1979 French thriller film directed by Henri Verneuil.

==Selected cast==
*Yves Montand as Henri Volney
*Michel Etcheverry as Frédéric Heiniger
*Roger Planchon as Prof. David Naggara Pierre Vernier as Charly Feruda
*Jacques Denis as Despaul
*Georges Staquet as Le gardien de limmeuble dassassinat

==Plot== Kennedy assassination Warren Commission) refuses to agree to the commissions final findings. The film portrays the initial controversy about this, as well as Volney and his staffs reopening of the investigation.

==Fictional state==
The action takes place in a fictional Western state where the spoken language is mainly French, but German, English and Spanish also are spoken. Its location, or even continent, is unknown. The capital city is made only of modern buildings, like Brasília. The political regime is presidential, the President being Marc Jarry at the beginning of the film.

==Location==
The new town of Cergy in the northwestern suburbs of Paris was used as a filming location for the movie. 
The EDF-GDF tower designed by architect Renzo Moro is the building from which the shots were fired to assassinate president Marc Jarry.
The governors palace was the prefecture of the Val dOise. 
The huge room used for council meetings is the High Court of Justice. 
The long scene of the psychological experiment towards the end of the film, supposed to take place at the University of Laye, in fact  takes place at ESSEC Business School in Cergy. 
The filmmakers chose the modern and innovative architecture of the new town to avoid depicting any particular country. {{cite web
 |url=http://www.leparisien.fr/val-d-oise/le-president-d-i-comme-icare-a-ete-assassine-a-cergy-27-08-2003-2004339459.php
 |title=Le président d" I... comme Icare " a été assassiné à Cergy
 |author=Eric Bureau 
 |date=2003-08-27
 |language=French
 |accessdate=2011-11-20}} 

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 


 
 