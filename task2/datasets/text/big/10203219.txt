The Mysterious Mr. M
{{Infobox Film
| name           = The Mysterious Mr. M
| image          = Poster of the movie The Mysterious Mr. M.jpg
| image_size     = 
| caption        = 
| director       = Lewis D. Collins   Vernon Keays	 
| producer       =  Paul Huston   Joseph F. Poland 	
| narrator       =  Richard Martin Dennis Moore Edmund MacDonald Byron Foulger
| music          = 
| cinematography = Gus Peterson
| editing        = 
| distributor    = Universal Pictures
| released       = 1 August 1946   
| runtime        = 13 chapters (227 min)
| country        =   United States English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 movie serial, and was the 137th and last serial produced by Universal.

==Plot==
Anthony Waldron intends to steal a new submarine invention from Dr. Kittridge while blaming a fictitious mastermind he calls "Mr. M."  To further this plan, Waldron uses a mind control drug he has developed called "Hypnotreme."  However, a mystery villain soon appears claiming to be the real Mr. M and starts giving Waldron orders.
 Federal agent Grant Farrell, whose brother was killed by Waldron, is dispatched to find the mysterious villain and stop his nefarious plans, teaming up with Kirby Walsh and Shirley Clinton to do so.

==Cast== Richard Martin as Detective Lieutenant Kirby Walsh 
* Pamela Blake as Shirley Clinton, insurance investigator Dennis Moore as Agent Grant Farrell
* Virginia Brissac as Cornelia Waldron 
* Danny Morton (actor) as Derek Lamont, one of Waldrons henchmen
* Edmund MacDonald as Anthony Waldron, the original villain
* Byron Foulger as Wetherby, the "real" Mr. M 
* Jane Randolph as Marina Lamont, one of Waldrons henchmen Jack Ingram as William Shrag, the spearpoint heavy (chief henchman)
 SOURCE:  

==Chapter titles==
#When Clocks Chime Death 
#Danger Downward 
#Flood of Flames 
#The Double Trap 
#Highway Execution 
#Heavier than Water 
#Strange Collision 
#When Friend Kills Friend 
#Parachute Peril 
#The Human Time-bomb 
#The Key to Murder 
#High-line Smash-up 
#The Real Mr. M
 SOURCE:  

==See also==
* List of American films of 1946
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
* 
* 

 
{{succession box  Universal Serial Serial 
| before=Lost City of the Jungle (1946)
| years=The Mysterious Mr. M (1946)
| after=none}}
 

 

 
 
 
 
 
 
 
 