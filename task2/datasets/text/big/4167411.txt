Night at the Museum
{{Infobox film
| name = Night at the Museum 
| image = Night_at_the_Museum_poster.jpg
| caption = Promotional poster
| director = Shawn Levy
| producer =   Thomas Lennon Robert Ben Garant
| based on =  
| starring =  
| music = Alan Silvestri
| cinematography = Guillermo Navarro Don Zimmerman
| studio =  
| distributor = 20th Century Fox
| released =  
| runtime = 108 minutes
| country = United States
| language = English
| budget = $110 million
| gross = $574.5 million   
}} fantasy adventure-comedy of the night watchman Egyptian artifact, come to life at night.
 Thomas Lennon The State Chris Columbus and Michael Barnathan. The film stars Ben Stiller, Carla Gugino, Dick Van Dyke, Mickey Rooney, Bill Cobbs, Jake Cherry, Ricky Gervais, Owen Wilson, Steve Coogan, and Robin Williams. A novelization of the screenplay, by Leslie Goldman, was published as a film tie-in.

The first installment in the Night at the Museum (film series)|Night at the Museum trilogy, the film was a box office success despite receiving mixed reviews from critics, grossing over $574 million.

==Plot== bond trader trader Don (Paul Rudd), more than him.

Cecil (Dick Van Dyke), an elderly night security guard about to retire from the American Museum of Natural History, hires Larry despite his unpromising résumé. The museum plans to replace Cecil and two colleagues Gus (Mickey Rooney) and Reginald (Bill Cobbs) with one guard. They advise Larry to leave some of the lights on and warn him not to let anything "in... or out".
 miniature Roman Old West Roman general Octavius (Steve Coogan); an Easter Island Moai (Brad Garrett) obsessed with chewing gum|"gum-gum"; and a wax model of Theodore Roosevelt (Robin Williams).
 Egyptian Artifact (archaeology)|artifact&mdash;the Golden Tablet of Pharaoh Akhmenrah&mdash;came to the museum in 1952, all of the exhibits come to life each night. If the exhibits are outside of the museum during sunrise, however, they turn to dust. Roosevelt helps Larry by restoring order, and he decides to remain as a guard.

On Cecils advice, Larry studies history to prepare himself better. He also learns from a museum docent Rebecca Hutman (Carla Gugino), who is writing a dissertation on Sacagawea (Mizuo Peck).

The next night, Larry uses what he has learned to better control the exhibits. Four Neanderthals set fire to a display. One Neanderthal turns to dust when he leaves the museum at dawn. The next morning, museum director Dr. McPhee (Ricky Gervais) almost fires Larry after what happened to the Neanderthal exhibit. He offers Rebecca a meeting with Sacagawea, but she believes that he is mocking her and the museum.
 frame Larry and Nick for the thefts, and disabled the tablet to stop the exhibits from interfering. Nick reactivates the artifact, but Cecil locks him and his father in the Egyptian exhibit and flees with the tablet.

Larry releases the Akhmenrahs mummy (Rami Malek) from his sarcophagus to control the two Anubis statues. The pharaoh speaks English from many years as an exhibit at University of Cambridge|Cambridge, and helps Larry and Nick escape by making the Anubis statues destroy the gate. They run into Attila and his Huns, but Larry calms them down and they join the trio. The group finds the other exhibits fighting, but the Easter Island Moai gets the exhibits attention allowing Larry to convince them to work together. Although some of the exhibits capture Gus and Reginald without difficulty, Cecil escapes by stagecoach after Jedediah and Octavius soldiers crash Cecils truck. Larry, Nick, Akhmenrah, Jedediah, Octavius, Rexy, and Attilas Huns pursue Cecil in Central Park, where they stop him and regain the tablet. Akhmenrah then uses the Golden Tablet to make all the escaped exhibits return to the museum. Rebecca, who is driving by, sees Rexy during the return to the museum before sunrise and realizes that Larry was telling the truth. Rebecca returns to the museum, where Larry introduces her to Theodore Roosevelt and Sacagawea.
 subway station, footprints in Central Park, and cavemen sightings. He rehires him after he sees that these events raised attendance so much that the museum is crowded with visitors. Larry, Nicky, and the exhibits celebrate at night with a dance party, during which he uses his "The Snapper" invention to turn off his flashlight.

During the credits, Cecil, Gus, and Reginald are seen working unhappily as janitors at the museum.

==Cast==
* Ben Stiller as Larry Daley, a man who becomes a security guard for the night shift at the American Museum of Natural History.
* Robin Williams as Theodore Roosevelt, a wax model of the 26th President of the United States who befriends Larry.
* Carla Gugino as Rebecca Hutman, a museum docent who is interested in Sacagawea.
* Dick Van Dyke as Cecil Fredericks, a veteran security guard.
* Mickey Rooney as Gus, a veteran security guard.
* Bill Cobbs as Reginald, a veteran security guard.
* Jake Cherry as Nicky Daley, Larrys son.
* Ricky Gervais as Dr. McPhee, the curator of the Museum of Natural History and is Larrys (strict) boss.
* Kim Raver as Erica Daley, the former and past wife of Larry Daley. Patrick Gallagher Attila the Hun, the historic leader of the Huns who constantly targets Larry, but Larry eventually calms him down.
* Rami Malek as Ahkmenrah, the mummy of a Pharaoh that owns the Golden Tablet (also called the Tablet of Akhmenrah). He has a problem of being too "dark".
* Pierfrancesco Favino as Christopher Columbus, the bronze statue of the famous explorer that Larry can barely identify and speaks Italian.
* Charlie Murphy as Taxi Driver
* Owen Wilson as Jedediah (uncredited), a cowboy miniature figure from the Old West diorama.
* Steve Coogan as Octavius, a Roman general miniature figure from the Roman Empire diorama.
* Mizuo Peck as Sacagawea
* Kerry van der Griend as Neanderthal #1
* Dan Rizzuto as Neanderthal #2
* Matthew Harrison as Neanderthal #3
* Jody Racicot as Neanderthal #4 
* Paul Rudd as Don, Ericas boyfriend. Anne Meara-Stiller as Debbie
* Martin Christopher as Meriwether Lewis William Clark
* Randy Lee as Hun #1
* Darryl Quon as Hun #2
* Gerald Wong as Hun #3
* Paul Chih-Ping Cheng as Hun #4  Easter Island Head (voice), a talking Moai who is a gum enthusiast. 
* Crystal the Monkey as Dexter, a capuchin monkey that constantly tries to steal Larrys keys and ripped the instruction manual that Cecil gave Larry.

==Production==
The building featured in the film, which was constructed on a sound stage in Burnaby, British Columbia, is based on the American Museum of Natural History in New York City, external shots of which were used in the movie.   

Trainers spent several weeks training Crystal, who plays the troublemaking monkey Dexter, to slap and bite Stiller in the film.

Robin Williams Theodore Roosevelt costume closely resembles that of John Waynes character in The Shootist. 

Director Shawn Levy credited Ben Stiller for the ensemble cast: "When actors hear that Ben Stiller is in a movie they want to work with him. It  a high-water mark and it absolutely draws actors in and Im convinced thats a big part of why we got this cast." 

==Cinematic allusion== Paul Verhoeven with music by Giuseppe Becce the exhibits in a museum also come to life at night.

==Music==
===Songs===
  in the   films to learn how to imitate his running technique, shown here as Stiller portraying his film character running for dear life from the Tyrannosaurus Rex skeleton (Rexy).   ]] Friday Night" - performed by McFly (band)|McFly; not featured in American version of the film, but heard in some international cuts, used during the end credits. It can be heard on the American DVD on the Spanish dub.
* "September (Earth, Wind & Fire song)|September" - performed by Earth, Wind and Fire; used before the end credits where everyone in the museum is partying. Weapon of Choice" - performed by Fatboy Slim; used in the scene where Larry returns to the museum for his second night and is preparing for the chaos.
* "Tonight" - performed by Keke Palmer and Cham (singer)|Cham; used for the end credits (U.S. theatrical version only).
* "Eye of the Tiger" - performed by Ben Stiller; used in the scene where Larry is bored and messes around with the telephone at the front desk beatboxing the music. instrumental version of "Mandy (song)|Mandy" by Barry Manilow is used when Larry is standing in the elevator, while escaping from Attila the Hun. Ezekiel Saw Them Dry Bones" is the tune Larry whistles as he passes the empty T. Rex exhibit on his first night.
* "Camptown Races" by Stephen Foster is sung by the townspeople of the American West miniature diorama. This is a period-correct song.

===Score=== Horton Hears a Who!
{{Infobox album  
| Name        = Night at the Museum (Original Motion Picture Soundtrack)
| Type        = Film
| Artist      = Alan Silvestri
| Caption     = 
| Alt         = 
| Released    =  
| Recorded    = 2006
| Genre       = Film score
| Length      =  
| Label       = Varese Sarabande
| Producer    = 
}}

;Tracklist
Varese Sarabande released a soundtrack album of the score on December 19, 2006. 
{{Track listing
| headline = Night at the Museum (Original Motion Picture Soundtrack)
| total_length = 53:19
| all_writing     = Alan Silvestri
| title1 = Night at the Museum 
| length1 = 02:35
| title2 = One of Those Days 
| length2 = 00:49
| title3 = An Ordinary Guy? 
| length3 = 01:27 
| title4 = Tour of the Museum 
| length4 = 02:35
| title5 = Civil War Soldiers 
| length5 = 04:08
| title6 = Out of Africa 
| length6 = 01:07
| title7 = Meet Dexter 
| length7 = 01:27 
| title8 = Mayan Warriors 
| length8 = 00:57
| title9 = Wheres Rexy? 
| length9 = 00:48 
| title10 = West from Africa 
| length10 = 01:49 
| title11 = The Iron Horse 
| length11 = 01:06 
| title12 = Saved by Teddy 
| length12 = 01:57 
| title13 = Tablet of Akmenrah 
| length13 = 00:37
| title14 = Tracking, Dear Boy
| length14 = 01:08
| title15 =  Some Men Are Born Great 
| length15 = 00:50 
| title16 =  Sunrise 
| length16 = 00:42 
| title17 =  Study Up on History 
| length17 = 02:15 
| title18 =  Teddy Likes Sacagawea 
| length18 = 01:53
| title19 =  Tearing Limbs 
| length19 = 01:45
| title20 =  Caveman on Fire 
| length20 = 00:43 
| title21 =  Outrun the Sun 
| length21 = 00:58 
| title22 =  Show You What I Do 
| length22 = 02:55
| title23 =  Tablets Gone 
| length23 = 02:45
| title24 =  Theodore Roosevelt at Your Service
| length24 = 01:11 
| title25 =  This Is Your Moment
| length25 = 02:10
| title26 =  Rally the Troops 
| length26 = 01:07
| title27 =  Tree Take Down 
| length27 = 01:21 
| title28 =  Cecils Escape 
| length28 = 01:26 
| title29 =  Stage Coach 
| length29 = 02:28
| title30 =  Teddy in Two 
| length30 = 01:18 
| title31 =  Cab Ride 
| length31 = 00:50 
| title32 =  Big Fan 
| length32 = 01:03 
| title33 =  Heroes Return 
| length33 = 00:54 
| title34 =  A Great Man 
| length34 = 00:57 
| title35 =  Full House 
| length35 = 01:21
}}

==Release== premiere in New York City on December 17, 2006, in South Korea on December 21, 2006, on December 22, 2006 in United States, December 26, 2006 in UK, January 12, 2007 in Brazil, on February 14, 2007 in China and on March 17, 2007 in Japan.   

The film was released under the title of "Noche en el museo" in Spain, "Una notte al museo" in Italy, "La nuit au musée" in France, "Ночь в музее" in Russia and "Uma Noite no Museu" in Brazil. 

===Box office===
At the end of its box office, Night at the Museum earned a gross of $250,863,268 in North America and $323,617,573 in other territories, for a worldwide total of  $574,480,841 against a budget of $110 million.  It is also the highest grossing film worldwide of the trilogy, and also in the North America and other territories. 

The film was the highest grossing film in its opening weekend, grossing $30.8 million plating in 3,685 theaters, with a $8,258 per-theater average. For the four-day Christmas holiday weekend, it took in $42.2 million.    The movie was also released in IMAX large screen format, often on site at museums of science or natural history such as the Pacific Science Center in Seattle.
 second weekend, Night at the Museum expanded into eighty-three more theaters and took in approximately $36.7 million at the box-office, out-grossing its opening weekend. It maintained its #1 position in its third week, with an additional $23.7 million. 

During its opening weekend of December 15, 2006, the film grossed a figure of estimated $5,049,169, with the highest debut coming from South Korea($5.04 million).  The biggest market in the other territories being UK, Japan, South Korea, Germany where the film grossed $40.8 million, $30 million, $25.7 million, $22.9 million. 

===Critical reaction===
The film received mixed reviews from movie critics, receiving a 44% rating on Rotten Tomatoes, based on 131 reviews, with an average rating of 5.3/10. The sites consensus reads, "Parents might call this either a spectacle-filled adventure or a shallow and vapid CG-fest, depending on whether they choose to embrace this on the same level as their kids".    On Metacritic, the film a 48/100 rating, based on 28 reviews, indicating "mixed or average reviews". 

James Berardinelli of Reelviews gave it 2 stars out of 4, and commented on Stillers performance by stating "It might be fair to give Ben Stiller an A for effort, but to call what he does in this movie "acting" is a misnomer. He does a lot of running around, occasionally falling down or bumping into things."    One positive review by William Arnold of the Seattle Post-Intelligencer, gave it a B-, and stated that the film was "Out to impress and delight a family audience with the pageantry of human and natural history, and thats a surprisingly worthy ambition for a Hollywood comedy."   

In a case of life imitating art, museum officials at the American Museum of Natural History have credited the film for increasing the number of visitors during the holiday season by almost 20%. According to a museum official, between December 22, 2006, and January 2, 2007, there were 50,000 more visitors than during the same period the prior year.   

CinemaScore polls conducted during the opening weekend, cinema audiences gave the film an average grade of "A-" on an A+ to F scale. 

===Home media===
The film was released on a 2-Disc DVD edition in the United Kingdom on April 2, 2007. It was released on 1-Disc and 2-Disc DVD editions and Blu-ray Disc format on April 24, 2007 elsewhere.

The film became the first non-Disney film to be reviewed by Ultimate Disney (now known as DVDizzy.com), due to the website dealing with other studios besides The Walt Disney Company|Disney.      

 , the film has sold 9,191,694 DVDs and grossed $153,566,058 in DVD sales. 

==Sequels==
 
The first installment in the Night at the Museum (film series)|trilogy, Night at the Museum was followed by a sequel titled Night at the Museum: Battle of the Smithsonian , which was released on May 22, 2009 in North America. The third and final installment, Night at the Museum: Secret of the Tomb, was released on December 19, 2014 in North America. 

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*   on Variety.com
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 