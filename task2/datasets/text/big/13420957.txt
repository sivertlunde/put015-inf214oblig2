McVeagh of the South Seas
 
{{Infobox film
| name           = McVeagh of the South Seas
| image          = 
| caption        =  Harry Carey Cyril Bruce Gregory Allen (Assistant Director)
| producer       = The Progressive Motion Picture Company
| writer         = Harry Carey
| starring       = Harry Carey
| cinematography = 
| editing        = 
| distributor    = Alliance Films Corporation
| released       =  
| runtime        = 5 reels 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 Harry Carey.   

==Plot==
A shipwreck near the Solomon Islands leaves San Franciscan Harmon Darrell (Jack Terry|Terry) and his daughter Nancy (Kathleen Butler|Butler) adrift in a lifeboat. Cyril McVeagh (Harry Carey (actor born 1878)|Carey) a ships captain reduced to drunkenness and brutality by his shattered love affair with Nancy, rules one of the islands, accompanied only by his deranged mate "Pearly" Gates (Herbert Russell (actor)|Russell) and the islands natives. McVeagh is about to marry Liana (Fern Foster|Foster), a native who loves him but is desired by Pearly, when Nancy arrives on the island, horrified at McVeaghs dissipation. Tanarka, Lianas former betrothed, leads a native rebellion against McVeagh, who sends Nancy away in a boat before the attack. McVeagh struggles with his crazed mate in his burning shack before Pearly recovers his reason and the two hurriedly leave the island. Liana, believing McVeagh dead, remains behind to mourn him, while McVeagh sets a course for San Francisco and civilization.

==Cast== Harry Carey - Cyril Bruce McVeagh
* Fern Foster - Liana Herbert Russell - "Pearly" Gates
* Kathleen Butler - Nancy Darrell
* Jack Terry - Harmon Darrell

==See also==
* Harry Carey filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 