The Blue Star Hotel
 
{{Infobox film
| name           = The Blue Star Hotel
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Martin Frič
| producer       = Oldrich Papez
| writer         = Marie Svobodová Václav Wasserman Martin Frič
| starring       = Natasa Gollová
| music          = 
| cinematography = Ferdinand Pecenka
| editing        = Jan Kohout
| studio         = 
| distributor    = 
| released       = 28 August 1941
| runtime        = 89 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
| gross          = 
}}
 Czech comedy film directed by Martin Frič.    It was released in 1941.

==Cast==
* Natasa Gollová - Zuzana - heritor
* Oldrich Nový - Vladimír Rychta Rohan
* Adina Mandlová - Milada Landová
* Ella Nollová - Thief in Hotel Modrá Hvezda (as E. Nollová)
* Marie Nademlejnská - Miss Fafejtová - owner of Hotel Merkur (as M. Nademlejká)
* Jan Pivec - Jirka Tuma - musician
* Ladislav Pesek - Zdenek Junek - cook (as Lad. Pesek)
* Antonín Novotný - Frantisek Sojka - poet (as Ant. Novotný)
* Karel Dostal - Reditel hotelového koncernu (as K. Dostál) Karel Černý - Miladas father (as K. Cerný)
* Cenek Slégl - Reditel hotelu (as C. Slégel)
* Frantisek Roland - Notár (as Fr. Roland)
* Vojtech Novák - Psychiatr (as V. Novák)
* Jára Kohout - Zrízenec hlídaci spolecnosti (as J. Kohout)
* Ferenc Futurista - Opilec (as F. Futurista)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 