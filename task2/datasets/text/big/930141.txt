Lumumba (film)
{{Infobox film
| name = Lumumba
| image = Lumumba2000.jpg
| image_size =
| caption =
| director = Raoul Peck
| producer = Jacques Bidou, Raoul Peck
| writer =Pascal Bonitzer, Dan Edelstein, Raoul Peck
| narrator =
| starring =Eriq Ebouaney
| music =Jean-Claude Petit
| cinematography =Bernard Lutic
| editing = Jacques Comets
| distributor =
| released =   
| runtime =
| country = France Belgium Germany Haiti
| language =
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
 Republic of the Congo (Congo-Léopoldville) achieved independence from Belgium in June 1960. Raoul Pecks film is a coproduction of France, Belgium, Germany, and Haiti. Due to political unrest in the Democratic Republic of the Congo at the time of filming, the movie was shot in Zimbabwe and Beira, Mozambique.

==Plot==
The plot is based on the final months of Patrice Lumumba (played by Eriq Ebouaney) the first Prime Minister of the Congo, whose tenure in office lasted two months until he was driven from office. Joseph Kasa-Vubu (Maka Kotto) is sworn in alongside Lumumba as the first president of the country, and together they attempt to prevent the Congo succumbing to secession and anarchy. The film concludes with Joseph Mobutu (Alex Descas) seizing power.

==Cast==
* Eriq Ebouaney as Patrice Lumumba
* Alex Descas as Joseph Mobutu
* Théophile Sowié as Maurice Mpolo
* Maka Kotto as Joseph Kasa-Vubu
* Dieudonné Kabongo as Godefroid Munongo
* Pascal NZonzi as Moïse Tshombe
* Olivier Bony  as King Baudouin I of the Belgians
* André Debaar as Walter J. Ganshof Van der Meersch
* Cheik Doukouré as Joseph Okito
* Makena Diop as Thomas Kanza
* Mariam Kaba as Pauline Lumumba
* Rudi Delhem as General Émile Janssens

==Release==
The film premiered at the 2000 Cannes Film Festival on 14 May 2000, and was shown at various film festivals as well as having commercial releases in Belgium, France, Switzerland, the United States, and Canada. The film grossed $684,000 in the United States.  It also aired on HBO. 

==Disputed scene==
The film generated some controversy in 2002 when Frank Carlucci, a former American government official and policy advisor, persuaded HBO to delete a reference to him during the airing of the film. The scene in question involves a group of Belgian and Congolese officials deciding whether to kill Lumumba. Carlucci is asked for input, and he mumbles that the US government does not involve itself in the internal affairs of other countries. At the time, Carlucci was the second secretary of the U.S. Embassy in Congo. He denies playing any role in the death of Lumumba, saying "The scene is tendentious, false, libelous; it never happened and it is a cheap shot." According to one source, the scene was deleted from the version of the film that aired on HBO.  Another source says that the scene was not deleted but the word "Carlucci" was bleeped in the dialogue and the name masked in the credits.  The scene remains on the DVD version of the film.

==References==
{{reflist|refs=
    
   
   
   
}}

==External links==
*  
*  
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 