All-American Murder
{{Infobox film
| name           = All-American Murder
| image          = AAM.jpg
| image size     =
| caption        =
| director       = Anson Williams
| producer       = Bill Novodor
| writer         = Barry Sandler
| narrator       =
| starring       = Christopher Walken Josie Bissett Charlie Schlatter
| music          = Rod Slane
| cinematography =
| editing        = Jonas Thaler
| distributor    = Trimark Pictures
| released       =
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

All-American Murder is a 1992 thriller film starring Christopher Walken and Charlie Schlatter. It was released Direct-to-video on December 18, 1992 in UK.

==Plot==
Artie Logan (Schlatter) is the new guy on Campus. Suddenly, he meets Tally Fuller: the most popular and beautiful girl at Fairfield college and she finally agrees to go on a date with him. But that night she is brutally killed by a blowtorch-wielding maniac and Artie is wrongfully arrested. Despite protests from other Police officers, detective P.J. Decker (Walken) believes Arties story and gives him 24 hours to track down the real killer. But, as Artie gets closer to the killer, each suspect is murdered and all the clues point to him.

The campus in the film is Oklahoma State University, in Stillwater, OK.

The football scenes, the stadium and the shower scene were all filmed at Union High Schools Tuttle Stadium in Tulsa, Oklahoma in May 1991.

==External links==
*  

 
 
 
 
 
 
 


 
 