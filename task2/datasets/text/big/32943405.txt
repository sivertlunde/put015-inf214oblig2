Kismat (1968 film)
 
 
{{Infobox film
| name           = Kismat
| image          = Kismat 1968.jpg
| caption        = Theatrical poster
| director       = Manmohan Desai
| producer       = Kamal Mehra
| story          = Manmohan Desai
| screenplay     = Brij Katyal Babita Helen Helen Kamal Mehra Murad
| music          = O.P.Nayyar
| cinematography = Keki Mistry
| editing        = Kamlakar Karkhanis (as Kamlakar)
| studio         = Pride of Asia Films
| distributor    = Bollywood Entertainment (BEI) (2003) (USA) (DVD)
| released       =  
| runtime        = 139 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 Helen and Kamal Mehra in prominent roles. Other notable actors in the film are Bhagwan Sinha, Murad, M. B. Shetty (father of Rohit Shetty), Hari Shivdasani, Hiralal, Indra Kumar, Jagdish Raj (as Jagdishraj), Polsan, Prem Kumar, Paul Sharma, Tun Tun and Ullhas.  

It can be categorised under romantic family thriller film. It was shot in 14 reels, using 35mm film format in Eastman Color. 

The film has been noted mainly for the soundtrack given by O.P.Nayyar, which included three songs that became very popular – Aao Huzoor Tum Ko, Kajra Mohabbat Wala and Aankhon Mein Qayamat. Biswajeet is one of the first heroes in Bollywood to be dressed as a woman on screen for the song Kajra Mohabbat Wala.   

This film was the last collaboration between singer Shamshad Begum and music director O.P.Nayyar. They never worked together again. Shamshad Begum recorded the last song for O.P.Nayyar, Kajra Mohabbat Wala along with Asha Bhosle in this film. 

== Plot ==
The nation is struck by a series of bomb blasts spread across the country. The Commissioner of Police (Murad (actor)|Murad) is baffled as to who is behind these attacks. The Commissioner receives an anonymous call where the caller tells that he knows who the masterminds behind the blasts are and their associates. He also tells that he would like to help the Police. The informant then tells that he has stored the names and images of mastermind and their associates in a microdot. The informant here is the Mr. Gonz, owner of a store, Twist Musical & Photo Shop. The main mastermind or the villain is Scorpion. He gets to know that the informant is about to give their organisations information to the police. Scorpion then sends his men or goons to recover the microdot.

As soon as the goons enter the shop and start to threaten the informant to give the microdot, the hero Vicky (Biswajeet) enters the store to get his guitar. The informant slips the microdot into Vickys guitar and Vicky leaves from there. The goons come to know of this and start to follow Vicky. Vicky is a singer and guitar player at a local nightclub. He along with Nancy (Helen (actress)|Helen) entertain the crowds. After the night show, Vicky and Nancy head to Vickys room. Here they encounter Mr. Gonz who is badly injured. Meanwhile, the goons follow Vicky to his room and kill Mr. Gonz. They try to take the guitar (his fathers gift) containing the microdot from him, but Vicky escapes with the guitar. He is on the run and tries to convince the police that Mr.Gonz has been killed, but is unable to do so. He again encounters the goons. This time he escapes from them and boards a train. Here he meets Roma (Babita Kapoor|Babita) who has also run away (from her home) due to the strict rules enforced by her rich and wealthy father.

During the train journey, Vicky and Roma become friendly. The goons follow Vicky and tries to attack them in the train. Vicky and Roma manage to escape from the train. They are in the middle of nowhere and start heading towards the nearest town. On their way to the town, they encounter Jani Bhai, an inventor, who gives them a ride to the town, with his customised car. Jani and Vicky become best of friends and Jani gets Vicky a job as a guitarist / singer in the hotel by portraying him as a very famous singer. Incidentally, Roma is staying in the same hotel. Vicky and Roma fall in love. Scorpion comes to know that Vicky is staying in the hotel through his henchmen, Julie, present as an employee in the hotel. Meanwhile, Nancy comes to this hotel and creates a misunderstanding between Vicky and Roma. Roma dumps Vicky and Nancy takes Vicky to her room. Nancy plots to kill Vicky but in the process, gets herself killed accidentally. While dying, she tells Vicky that Scorpion had sent her and she was compelled to kill because her parents were taken hostage. The rest of the film deals with who Scorpion is and whether Vicky and Roma get together. 

== Cast ==
* Biswajeet as Vicky, singer and guitar player at a hotel nightclub Babita as Roma, an escapee from home Helen as Nancy Murad as  Commissioner of Police
* Jani Bhai, an eccentric inventor
* Scorpion, the main villain and chief of the Scorpion Organisation
* Hiralal as Lopez, the right hand man of Scorpion
* M.B.Shetty as Joe, a deadly member of the Scorpion Organisation
* Bhagwan Sinha as Gonz, Owner of Twist Musical & Photo Shop and informant to the police
* Yee Chang as Chang, an enemy agent and most wanted man by many countries

== Production ==

=== Filming === Babita as a Pathan. Biswajeet in an interview says this about the preparation that went about making the song, Kajra Mohabbat Wale: 

 

== Release ==

=== Home media ===
The film has been released in the following distribution formats:VCD  and recently as DVD. 

== Reception ==
The music by O.P.Nayyar was well received with three popular and memorable songs.

=== Box office ===
The film was declared an average film in the box office verdicts. It could be attributed to the story and the earnings it made (Net gross of INR   70,00,000 vs Gross of INR   1,40,00,000). 

== Controversies ==
The song Kajra Mohabbat Wala became very popular due to the way it was filmed, choreographed and for its memorable tune. Biswajeet did the drag act of putting a female disguise for this song. Film historian and critic, Firoze Rangoonwalla explains the controversy that took place after the film was released: 
 

== Soundtrack ==
The music of the film was composed by O.P.Nayyar. Notable tracks are Aankhon Mein Qayamat Ke Kajal sung by Mahendra Kapoor, Aao Huzoor Tum Ko sung by Asha Bhosle and Kajra Mohabbat Wala sung by Asha Bhosle and Shamshad Begum.

{{Infobox album
| Name = Kismat
| Type = soundtrack
| Artist =O.P.Nayyar
| Cover = Kismat 1968 Music Poster.JPG
| Caption = Original vinyl record poster from Penguin
| Released =  
| Recorded = 1968
| Genre = Film soundtrack
| Length =  
| Label = HMV, Penguin
| Producer = 
| Language    = Hindi
| Last album  = 
| This album  = 
| Next album  = 
}}

=== Track listing ===
Originally, the film consisted of 6 soundtracks (mainly for the cassette and gramophone version).   
{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = yes
| title1 = Aao Huzoor Tum Ko | extra1 = Asha Bhosle | lyrics1 = Noor Devasi | length1 = 05:51
| title2 = Kajra Mohabbat Wala | extra2 = Asha Bhosle, Shamshad Begum | lyrics2 = S H Bihari | length2 = 06:22
| title3 = Lakhon Hain Yahan Dilwale | extra3 = Mahendra Kapoor | lyrics3 = S H Bihari | length3 = 04:17
| title4 = Aankhon Mein Qayamat Ke Kajal | extra4 = Mahendra Kapoor | lyrics4 = S H Bihari | length4 = 04:22
| title5 = One Two Three Baby | extra5 = Asha Bhosle, Mahendra Kapoor | lyrics5 = S H Bihari | length5 = 06:54
| title6 = Kismat (Title Music) | extra6 =  | lyrics6 = | length6 = 03:13
}}

=== Additional tracks ===
In mid-2000, two additional tracks were added known as revival versions for Aao Huzoor Tum Ko and Kajra Mohabbat Wala songs. The CD and iTunes versions of the film now contain 8 tracks.   
{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = yes
| title1 = Aao Huzoor Tum Ko (Revival) | extra1 = Asha Bhosle | lyrics1 = Noor Devasi | length1 = 04:43
| title2 = Kajra Mohabbat Wala (Revival) | extra2 = Asha Bhosle, Shamshad Begum | lyrics2 = S H Bihari | length2 = 06:15
}}

=== Vinyl or gramophone record track Info ===
The Gramophone record consists of 2 sides having the following tracks: 

Side 1
{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = yes
| title1 = Aankhon Mein Qayamat Ke Kajal | extra1 = Mahendra Kapoor | lyrics1 = S H Bihari
| title2 = Kajra Mohabbat Wala | extra2 = Asha Bhosle, Shamshad Begum | lyrics2 = S H Bihari
| title3 = Title Music | extra3 =  | lyrics3 =
}}
Side 2
{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = yes
| title1 = Aao Huzoor Tum Ko | extra1 = Asha Bhosle | lyrics1 = Noor Devasi
| title2 = Lakhon Hain Yahan Dilwale | extra2 = Mahendra Kapoor | lyrics2 = S H Bihari
| title3 = One Two Three Baby | extra3 = Asha Bhosle, Mahendra Kapoor | lyrics3 = S H Bihari
}}

=== Songs remixed ===
The tracks that have been remixed the most are:
* Aao Huzoor Tumko
* Kajra Mohabbat Wala
Ironically, both these songs are sung by female singers and most notable, Asha Bhosle features in both of them.

The track Aao Huzoor Tumko has been remixed a number of times. Following is a list of notable films and music albums it was used in: Karunesh S  Double Cross – Ek Dhoka, a 2005 film
* Non Stop Love Shots, a 2005 album
* Salaam-e-Ishq, a 2007 Hindi film, as a small song clip (played between in the scene where its Anil Kapoors birthday and he sees Anjali)

=== Songs reused ===
The track, Kajra Mohabbat Wala, has been used as a non-remixed song in the 2011 Hindi movie, Tanu Weds Manu. The song has been used as a marriage celebration or ritual song. The song has been portrayed by Kangana Ranaut. The song rights have been obtained from HMV. 

=== Plagiarism of song ===
The track Lakhon Hain Yahan Dilwale sung by Mahendra Kapoor is said to have been inspired from the following tracks:  Red River Valley
* The Bright Sherman Valley (inspired from In the Bright Mohawk Valley)
The similarity includes Mahendra Kapoors singing drawl to that of the elaborate drawls of Red River Valley song besides the tune in the original. 

== Certification ==
The Central Board of Film Certification(CFBC), India has given an U certificate (Universal or Unrestricted) for viewing purposes.

== See also ==
Bollywood films of 1968

== References ==
 
 

 
   at the Internet Movie Database. IMDb. Retrieved: 1 September 2011. 
  . Cineplot. Article: 26 November 2010, Retrieved: 12 September 2011 
   in The Hindu. The Hindu. Article: 22 May 2010, Retrieved: 10 September 2011. 
  . India Glitz. Retrieved: 6 September 2011. 
  . Bollywood Hungama. Retrieved: 6 September 2011.  Screen magazine. Screen India. Retrieved: 9 September 2011. 
  . Retrieved: 1 September 2011. 
   at Sa Re Ga Ma. Sa Re Ga Ma. Retrieved: 3 September 2011. 
  . iTunes Apple. Retrieved: 4 September 2011. 
  . Retrieved: 3 September 2011. 
  . itwofs.com (eye-too-eff-ess). Retrieved: 4 September 2011. 
  . Last.fm. Retrieved: 6 September 2011. 
  . Indian Express. Article: 18 January 2011. Retrieved: 9 September 2011. 
  . Box Office India. Retrieved: 6 September 2011. 
  . Amazon UK. Retrieved 12 September 2011   . Induna.com. Retrieved 12 September 2011   . Cineplot. Article: 16 July 2010, Retrieved 12 September 2011 
 

== External links ==
*   at the Internet Movie Database
*   at iTunes

 

 
 
 
 
 