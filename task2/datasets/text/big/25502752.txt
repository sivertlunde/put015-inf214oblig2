Nirnayam (1991 film)
{{Infobox film
| name           = Nirnayam
| image          = Nirnayam1.jpg
| caption        =
| director       = Priyadarshan
| producer       = D. Kishore Murali Mohan  
| writer         = Ganesh Patro   
| story          = Priyadarshan 
| screenplay     = Priyadarshan Amala Akkineni
| music          = Ilaiyaraja
| cinematography = S. Kumar
| editing        = N. Gopalakrishnan
| studio         = Jayabheri Art Productions 
| distributor    =
| released       =   
| runtime        = 2:43:40
| country        =   India
| language       = Telugu
| budget         =
}} Telugu film Amala Akkineni in lead roles and music composed by Ilaiyaraja. The film recorded as Above Average at box-office. The movie was dubbed in Tamil and Hindi languages as Sambavam and Girafthari respectively. This film is a remake of Priyadarshans own 1989 Malayalam film, Vandanam starring Mohanlal. 

==Plot==
Akkineni Nagarjuna plays the role of a sincere Cop, Vamsi Krishna, who is sent undercover along with two other cops. Of which one cop is a close friend and associate to Vamsi played by Subhalekha Sudhakar, by Police Commissioner played by Giri Babu to track down a vicious and dangerous criminal named Raghuram played by Murali Mohan. Vamsi performs surveillance on Raghurams daughter Geetha played by Amala (actress)|Amala, by getting into Geetas opposite buildings flat in a Residential complex, to gather details and whereabouts of her criminal father. Geeta lives with her aunt played by Sukumari. When Vamsi in guise of a telephone department inspector, pursues her and falls in love with Geetha, she reciprocates Vamsis love interest initially but refuses him after she gets to know the truth that hes a Cop and is about to drag her father to prison. The rest of the story is about how Vamsi gets hold of Raghuram through Raghurams daughter Geetha and what facts he learns about the crime operation of the real Criminal played by Sharat Saxena who deceives Geethas father Raghuram who is actually an innocent victim, and in the process how the misunderstanding between Geetha and Vamsi gets cleared up, how Vamsi banishes the real criminal. The plot of the movie was inspired by the American movie Stakeout.

==Cast==
 
*Akkineni Nagarjuna as Vamsi Krishna Amala Akkineni as Geetha
*Murali Mohan as Raghuram
*Sharat Saxena
*Subhalekha Sudhakar as Inspector Shivram
*Giri Babu Charu Hasan
*Gollapudi Maruti Rao Allu Ramalingaiah
*Suthi Velu 
*Potti Veeraiyah 
*Chinni Jayanth 
*Husain 
*Prasanna Kumar 
*Bhimeswara Rao
*Rajeevi as Anarkali  Annapurna as Vamsis Mother
*Sukumari as Jolly Aunty  
*Jyothi 
 

==Soundtrack==
{{Infobox album
| Name        = Nirnayam
| Tagline     = 
| Type        = film
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1991
| Recorded    = 
| Genre       = Soundtrack
| Length      = 25:06
| Label       = Echo Music
| Producer    = Ilaiyaraaja
| Reviews     =
| Last album  = Stuartpuram Police Station   (1991) 
| This album  = Nirnayam   (1991)
| Next album  = Chaitanya (film)|Chaitanya   (1991)
}}

Music composed by Ilaiyaraaja. All the songs are blockbusters. Music released on ECHO Audio Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 25:06
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Hello Guru 
| lyrics1 = Ganesh Pathro SP Balu 
| length1 = 5:04

| title2  = Mila Mila Veturi
| extra2 = Mano (singer)|Mano, S. Janaki
| length2 = 5:05

| title3  = Epudepudepudani Sirivennela  SP Balu, S. Janaki
| length3 = 4:48

| title4  = Enta Enta Dooram
| lyrics4 = Ganesh Pathro SP Balu, Chitra
| length4 = 5:04 

| title5  = O Papalu
| lyrics5 = Ganesh Pathro SP Balu, Swarnalatha
| length5 =5:04
}}

==References==
 

==External links==
*  

 

 
 
 
 
 