Ranadheera Kanteerava
{{Infobox film
| name           = Ranadheera Kanteerava
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = N. C. Rajan Rajkumar  Narasimharaju  Balakrishna
| writer         = G. V. Iyer
| screenplay     = G. V. Iyer
| story          = G. V. Iyer
| based on       = 
| narrator       =  Rajkumar  Leelavathi  Balakrishna
| music          = G. K. Venkatesh
| cinematography = B. Dorairaj
| editing        = N. C. Rajan
| studio         = Kannada Chalanachitra Kalavidara Sangha
| distributor    = Ramesh Movies
| released       =  
| runtime        = 141 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          =
}} Kannada historical historical drama Narasimharaju and Sandhya in the prominent roles.
 Narasimharaju and acclaimed writer G. V. Iyer. Thus this was the first venture where Rajkumar turned producer for the film.  The film. post release suffered an initial hitch as no distributor came forward to screen the film. Later it was released in a single "Bharath" theater and found huge collections drawing.  This film screened at IFFI 1992.

==Cast== Rajkumar as Kanteerava
* R. Nagendra Rao as Dalavayi Vikrama Raya
* Udaykumar Balakrishna
* Leelavathi
* Sandhya
* Narasimharaju
* H. M. S. Shastry
* K. S. Ashwath
* Veerabhadrappa
* Kantha Rao
* Papamma
* G. V. Iyer
* Ramadevi
* Radha
* Saroja
* Eshwarappa

==Historical and Cultural Significance== Narasaraja Wodeyar Sati and ended their lives. 

==Soundtrack==
{{Infobox album  
| Name        = Ranadheera Kanteerava
| Type        = Soundtrack
| Artist      = G. K. Venkatesh
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    = Feature film soundtrack
| Length      =
| Label       = Sa Re Ga Ma
}}

Ranadheera Kanteerava soundtrack consists of 7 songs all composed by G. K. Venkatesh and written by G. V. Iyer. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) || Lyricist
|-
| 1 || "Radha Madhava" || P. B. Sreenivas, A. P. Komala || G. V. Iyer
|- Radha Jayalakshmi, Rajalakshmi || G. V. Iyer
|- Sulamangalam Rajalakshmi  || G. V. Iyer
|-
| 4 || "Sangeetha Devatheye" || P. B. Sreenivas || G. V. Iyer
|-
| 5 || "Enidu Rosha" || P. B. Sreenivas || G. V. Iyer
|- Radha Jayalakshmi || G. V. Iyer
|-
| 7 || "Sanchari Manasothe" || P. B. Sreenivas || G. V. Iyer
|-
|}

==See also==
* Kanthirava Narasaraja I
* Wadiyar dynasty

==References==
 

==External links==
*  
*  
*  K. Moti Gokulsing, Routledge Handbook of Indian Cinemas ISBN 978-0-415-67774-5

 
 
 
 
 
 
 
 
 
 
 
 

 