The Secret of Bombay
{{Infobox film
| name           = The Secret of Bombay
| image          = 
| image_size     = 
| caption        = 
| director       = Artur Holz 
| producer       = Erich Pommer
| writer         = Paul Beyer   Rolf E. Vanloo
| narrator       = 
| starring       = Conrad Veidt   Lil Dagover   Hermann Böttcher   Bernhard Goetzke
| music          = 
| editing        =
| cinematography = A.O. Weitzenberg
| studio         = Decla-Bioscop
| distributor    = Decla-Bioscop
| released       = 6 January 1921
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent adventure film directed by Artur Holz and starring Conrad Veidt, Lil Dagover and Hermann Böttcher. It premiered in Berlin on 6 January 1921. 

==Cast==
* Conrad Veidt as Dichter Tossi 
* Lil Dagover as Die Tänzerin Farnese 
* Hermann Böttcher as Lord Pombroke 
* Bernhard Goetzke as Indischer Abenteurer 
* Anton Edthofer as Schiffsarzt Vittorio 
* Karl Römer as Teehausbesitzer 
* Alfred Abel   
* Lewis Brody   
* Nien Soen Ling   
* Gustav Oberg

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.
* Hardt, Ursula. From Caligari to California: Erich Pommers life in the International Film Wars. Berghahn Books, 1996.

==External links==
* 

 
 
 
 
 
 
 
 


 
 