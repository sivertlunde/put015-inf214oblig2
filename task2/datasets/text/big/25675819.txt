Pugaippadam
{{Infobox film
| name           = Pugaippadam
| director       = Rajesh Lingam
| producer       = N. C. Manikandan
| writer         = Rajesh Lingam
| starring       =  
| music          = Gangai Amaran
| cinematography = Vijay Armstrong
| editing        = B. Lenin
| studio         =  
| distributor    =
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
}}
Pugaippadam ( ) is a 2010 Indian Tamil-language memoir-drama film written and directed by newcomer Rajesh Lingam, starring Priya Anand of Vaamanan fame along with 6 newcomers, namely Amjad, Harish, Sivam, Nandha, Mrinalini and Yamini, in lead roles. The film was released on 1 January 2010. 

==Plot==
Krishna(Amjad (actor)|Amjad),Nandha(Nandha),Bala(Harish (actor)|Harish),Guru(Sivam),Krithika Rao (KK)(Yamini (actress)|Yamini), Gowri(Mrinalini (actor)|Mrinalini)were friends in their college since I-year.Later, Shiney George(Priya Anand) joined in their gang and soon Shiney and Krishna Fell in Love. Gowri loved Guru Since they both met at first time .But,Guru neglected that by saying we should not broke the friendship and he is the one who neglected girls in their gang.so,its wrong to say he loves Gowri.Guru and Krishna kept lots of Arrears in their academics.So,Shiney Slapped Krishna and its bad for him to join with Guru and Krishna got angry.Guru fought with shiney to slap him.Thus the Gang splited as boys and girls.Due to this Gang didnt reunite at the departal of Gowri. After the interrogation of the Lectures they united. The other friends helped Guru and Krishna to clear their arrears and they got Placements in companies. Shineys father accepted her love and at the last day Krishna and Shiney opened their love to all their friends.On that night,Bala attempted suicide and died in the hospital.

There Gowri said he was died for shiney because he was in a love with her.Then,the truth came out.Bala admired at shiney at first sight and loved her when she supported him at the time of Ragging.And he told all his feelings to Gowri on that day morning and he decided to propose his love to all his friends before telling to shiney. But the plan went wrong when shiney slapped Krishna and the gang broken up.On hearing this everyone heart went broken and all of them felt sorry especially Krishna and shiney. Guru went mad and rushed to college where Balas memories were wandwering.

At college Shiney and Krishna said they wont get married because they felt guilty and pain for Balas death. KK told all its only possible if we all departed forever. Gowri yelled at her But Nandha told she is correct because we lost a member from our gang forever and without him we wont make a best gang.Adding to this he also said If we all know about anyones love earlier we wouldnt lost Bala. This means,we were not loyal to ourselves and friendship".On telling this everybody cried and one by one went from the sight forever. Shiney and Krishna were the last to go and the film ends.
==Cast== Amjad as Krishna
* Priya Anand as Shiney George
* Harish as Bala
* Sivam as Guru
* Nandha as Nandha Mrinalini as Gowri Yamini as Krithika Rao (KK)
* Venkat
* Vinu Chakravarthy
* Shanmuga Sundaram
* G. Gnanasambandam
* Neelima Rani

==Production==
The film was announced in May 2008, with the director being an associate of Selvaraghavan. The cast was almost entirely newcomers with the male lead, Amjad, being associated with Radio One. Priya Anand, whose second film Vaamanan released before Pugaippadam, was also introduced. Noted orator and professor G. Gnanasambandam was announced to don an important role in this film, whilst Gangai Amaran, was chosen to score the music. 

==Release==
===Reception===
The film opened in only a few centres across Chennai, Tamil Nadu to a below average opening. The film which grossed  1,70,670 in the opening weekend, failed commercially at the box-office. 

===Reviews===
Upon release, the film received mixed reviews. A reviewer from Behindwoods.com claimed that the overall the film "does manage to live up to the liveliness and energy that we associate with a campus movie" with the first half being "colorful and enjoyable with humor mixed at places", also giving praise to the "touching" climax.    However, the reviewer goes on to claim that the "entire movie appears episodic and fragmented" with the flow is "definitely missing at many places, which is a sore point". The cast also received mixed reviews with Shanmugasundaram’s role being criticized whilst Venkat, appearing as a professor, "fits the bill" whilst the young debut stars "have done their parts well". 

The crew of the film was adjudged by Behindwoods to have handled the camera "very well, capturing the scenic beauty of the landscape". Whilst the director, Rajesh Lingam, has "proved that he has the right stuff in him to make good movies if he gets the right material". Music by Gangai Amaran was also appreciated, with the song Oru Kudaiyil, being singled out by the reviewer.

==Soundtrack==
The soundtrack was composed by music director Gangai Amaran.
{{Infobox album
| Name       =  Pugaippadam
| Type       = Soundtrack
| Artist     = Gangai Amaran
| Cover      = 
| Released   =   
| Recorded   = 
| Genre      = World Music
| Label      = 
| Producer   =  
| Last album = 
| This album = Pugaippadam (2006)
| Next album = 
}}

{{tracklist
| headline     = Tracklist
| extra_column = Singer(s)
| total_length = 
| title1       = Pennilamae Onnumilae
| extra1       = Krish (singer)|Krish, Balaji, MK
| length1      = 
| title2       = Oru Kudayil Payanam
| extra2       = Venkat Prabhu, Prashanthini
| length2      = 
| title3       =Vaan Nilavudhaan
| extra3       = Vijay Yesudas
| length3      = 
| title4       = Idhu Kanavo Idhu Nijamo
| extra4       = S. P. Balusubrahmanyam, K. S. Chithra
| length4      = 
| title5       = Odaikanum Odaikunam
| extra5       = Haricharan, Vijay Yesudas
| length5      = 
| title6       = Padapadavena
| extra6       = 
| length6      = 
}}

==References==
 

==External links==
*  

 
 
 
 