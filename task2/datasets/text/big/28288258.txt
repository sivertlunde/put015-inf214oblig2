Jörg Ratgeb – Painter
{{Infobox film
| name           = Jörg Ratgeb – Painter
| image          = 
| caption        = 
| director       = Bernhard Stephan
| producer       = Walter Bockmayer
| writer         = Manfred Freitag Joachim Nestler
| starring       = Alois Svehlík
| music          = 
| cinematography = Otto Hanisch
| editing        = Brigitte Krex
| distributor    = 
| released       =  
| runtime        = 101 minutes
| country        = East Germany
| language       = German
| budget         = 
}}

Jörg Ratgeb – Painter ( ) is a 1978 East German drama film directed by Bernhard Stephan. It was entered into the 28th Berlin International Film Festival.   

==Cast==
* Alois Svehlík - Jerg Ratgeb|Jörg Ratgeb
* Margrit Tenner - Barbara
* Olgierd Lukaszewicz - Bischof
* Günter Naumann - Joß Fritz
* Malgorzata Braunek - Junge Bäurin
* Henry Hübchen - Thomas Niedler
* Rolf Hoppe - Gaukler
* Marylu Poolman - Seine Frau
* Martin Trettau - Albrecht Dürer
* Helga Göring - Agnes Dürer
* Hilmar Baumann - Vogt
* Thomas Neumann - Christoph Enderlin
* Monika Hildebrand - Frau Ratgeb
* Giso Weißbach - Kommandeur
* Günter Rüger - Fiedler
* Peter Pauli - Dudelsackpfeiffer
* Erich Petraschk - Alter Bauer
* Bodo Krämer - Landsknecht

==See also==
*Jerg Ratgeb

==References==
 

==External links==
* 

 
 
 
 
 
 
 