Sentinels of Silence
{{Infobox film
| name = Sentinels of Silence
| image =
| caption =
| director = Robert Amram
| producer = Manuel Arango
| writer = Robert Amram
| narrator =
| starring = Ricardo Montalbán Orson Welles
| music =
| cinematography = Jim Freeman Gustavo Olguin
| editing =
| distributor = Paramount Pictures
| released =  
| runtime = 18 minutes
| country = Mexico
| language = Spanish
| budget =
}}
 short documentary Mexican filmmaker Robert Amram, and is notable for being the first and only short film to win two Academy Awards.

==Plot==
Sentinels of Silence provides an 18-minute helicopter-based aerial visit across the archeological ruins in Mexico including Teotihuacan, Monte Alban, Mitla, Tulum, Palenque, Chichen Itza and Uxmal. The film’s narration details pre-Columbian Mayan culture, focusing on its achievements in mathematics and astronomy, and then questions how and why the Mayan society seemed to disappear, leaving behind its structures as the eponymous silent sentinels. 

==Production==
Sentinels of Silence was released in two versions, with Orson Welles providing the English-language narration and Ricardo Montalban providing the Spanish-language narration.  Both versions included a symphonic score by Mariano Moreno. Paramount Pictures acquired this production for U.S. theatrical release.

==Academy Awards== Best Short Best Documentary Short Subject.     This was the only time that a short film won Oscars in two categories. Afterwards, the Academy changed its rules to prevent documentaries from competing against narrative films in the Best Short Subject category. 

==Home video and non-theatrical release==
Sentinels of Silence was released on VHS video by ALTI Publishing in 1990 under the new title "Sentinels of Silence: The Ruins of Ancient Mexico."   To date, the film has not been made available on DVD. Although the film is no longer in theatrical circulation, the government of Mexico continues to present the film in non-theatrical screenings at its embassies and consulates around the world. 

There is, however, a DVD edition distributed by Mexico Antiguo, for sale only in Mexico.

==See also==
* Orson Welles filmography

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 