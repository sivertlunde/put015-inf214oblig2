Pane, burro e marmellata
{{Infobox film
| name           = Pane, burro e marmellata
| image          = Pane, burro e marmellata.jpg
| caption        = DVD cover
| director       = Giorgio Capitani
| producer       = 
| writer         = Giorgio Capitani Franco Ferrer Enrico Montesano
| starring       = Enrico Montesano
| music          = Piero Umiliani
| cinematography = Roberto Gerardi
| editing        = Renato Cinquini
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

Pane, burro e marmellata is a 1977 Italian comedy film directed by Giorgio Capitani and starring Enrico Montesano.

==Cast==
* Enrico Montesano - Bruno Desantis
* Rossana Podestà - Simona
* Claudine Auger - Betty
* Rita Tushingham - Vera
* Bente Szacinski
* Laura Trotter
* Franco Giacobini
* Jacques Herlin
* Adolfo Celi
* Stefano Amato
* Paola Arduini
* Dino Emanuelli
* Franca Scagnetti
* Simona Zezza

==External links==
* 

 
 
 
 
 
 
 