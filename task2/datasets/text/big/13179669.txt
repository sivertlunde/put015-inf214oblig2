Fluttering Hearts
 
{{Infobox film
| name           = Fluttering Hearts
| image          = 
| caption        = 
| director       = James Parrott
| producer       = Hal Roach
| writer         = Charley Chase H. M. Walker
| starring       = Charley Chase Oliver Hardy Eugene Pallette
| cinematography = 
| editing        = Richard C. Currier
| distributor    = 
| released       =  
| runtime        = 23 minutes
| country        = United States 
| language       = Silent film English intertitles
| budget         = 
}}

Fluttering Hearts is a 1927 film featuring Charley Chase, Oliver Hardy, and Eugene Pallette.

==Cast==
* Charley Chase - Charley
* Martha Sleeper - Daughter
* Oliver Hardy - Big Bill
* William Burress - Father
* Eugene Pallette - Motorcycle Cop
* Kay Deslys - Big Bills girl
* May Wallace - Mother Charlie Hall - Man under car

==See also==
* List of American films of 1927
* Oliver Hardy filmography

==External links==
* 

 
 
 
 
 
 
 
 
 


 