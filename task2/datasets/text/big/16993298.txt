Caravan (1946 film)
{{Infobox film
| name           = Caravan
| image          = "Caravan"_(1946_film).jpg
| image_size     =
| caption        = Detail from Italian poster
| director       = Arthur Crabtree
| producer       = Harold Huth
| writer         = Roland Pertwee (writer) Eleanor Smith  (original novel)
| starring       = Stewart Granger Jean Kent Anne Crawford Dennis Price Robert Helpmann Gerard Heinz
| music          = Bretton Byrd (uncredited) 
| cinematography = Stephen Dade Cyril J. Knowles (location photography)
| editing        = Charles Knott
| distributor    = General Film Distributors (UK)
| released       = 3 June 1946
| runtime        = 80 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          = 649,800 admissions (France)   at Box Office Story 
}}
Caravan is a 1946 British black and white drama film directed by Arthur Crabtree. It was one of the Gainsborough melodramas and is based on a novel Caravan (novel)|Caravan by Eleanor Smith.

==Plot==
In the late 19th-century, writer Richard Darrell (Stewart Granger) saves Don Carlos (Gerard Heinz) from two robbers.  Don Carlos gives Richard the task of taking a valuable necklace to Spain.  Bidding farewell to his fiancée, Oriana (Anne Crawford), Richard sets out.  On the way, he meets Wycroft (Robert Helpmann), who assaults, robs and nearly kills Richard on behalf of his dastardly master Sir Francis Castteldow (Dennis Price), an aristocrat who plans to steal Oriana from Richard.
 gypsy girl, Rosal (Jean Kent).  However, everyone will meet again...

==Cast==
*Stewart Granger - Richard Darrell 
*Jean Kent - Rosal 
*Anne Crawford - Oriana Camperdene 
*Dennis Price - Sir Francis Castleton 
*Robert Helpmann - Wycroft 
*Gerard Heinz - Don Carlos 
*Arthur Goullet - Suiza 
*John Salew - Diego 
*Julian Somers - Manoel 
*Pete Murray - Juan 
*Josef Ramart - José 
*Joseph ODonohue - Bandit 
*Sylvia St. Clair - Marie 
*Patricia Laffan - Betty 
*Henry Morrell - Cumbermere

==Production==
The film was meant to follow The Magic Bow but that was postponed due to an illness to Phyllis Calvert so Caravan had to be rushed into production. BUSY BRITONS: Two Down and One to Go
By C.A. LEJEUNE. New York Times (1923-Current file)   24 June 1945: 27.  

Jean Kent met her future husband during the making of the movie. 

==Release==
The film was one of the most popular British releases of 1946. 

==Critical reception==
The New York Times wrote, "Granger and the rest of the cast alternate between grappling with stilted lines and an embarrassingly archaic situation with neither the players nor plot making much entertainment, while "Caravan" moves with the speed of an oxcart.";  but TV Guide noted "strong direction, brilliant individual performances, and production values far above the usual run of British films work beautifully together as one melodramatic situation is piled on another."  

==References==
 

==External links==
* 
*  
*  at BFI Screenonline

 
 

 
 
 
 
 
 
 
 
 
 