Neck (film)
 
{{Infobox film
| name           = ＮＥＣＫ　ネック
| image          = Neck Movie Poster.jpg
| image_size     = 250px
| caption        =
| genre          = horror film|horror, comedy
| director       = Takeshi Shirakawa
| producer       = 
| writer         = Otaro Maijo   Itaru Era   Hisako Fujihira
| starring       = Saki Aibu   Junpei Mizobata  
| music          = 
| cinematography = Masao Nakabori
| editing        =
| distributor    = Asmik Ace Entertainment
| released       = 21 August 2010
| runtime        = 96 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}

  is a 2010 Japanese horror film|horror-comedy film starring Junpei Mizobata and Saki Aibu.  It was directed by Takeshi Shirakawa.

==Plot==

Tomokazu Shudo (Junpei Mizobata) has his first ever crush on Sugina Mayama (Saki Aibu), a senior student. Tomokazu has troubles confessing his feelings to the popular student, but one day, he is invited to the research lab where Sugina Mayama works. Tomokazu hopes to use this meeting to express his true feelings for her. To Tomokazus surprise, when he arrives he first sees a big wooden box in the middle of the dark lab. Sugina tells Tomokazu that he looks scared and then proceeds to push Tomokazu into the box!

While being placed into the box, Tomokazu is told that he will watch a horror movie produced from his mind, with only his neck & head protruding from the box. Tomokazu will be the first test subject for Suginas "Neck Machine". The device hopes to turn people into monsters from the neck down while watching or thinking horror-related things. The first experiment fails, but Sugina then gets the help of childhood friend Takashi Gori (Yuta Hiraoka), who is now a horror writer named Mataro Echizen.

==Cast==
* Junpei Mizobata as Tomokazu Shudo
* Saki Aibu as Sugina Mayama
** Sumina Teramoto as young Sugina
* Yuta Hiraoka as Takashi Gori/Mataro Echizen
** Takumi Hanaoka as young Takashi
* Chiaki Kuriyama as Eiko Akasak
* Kazuma Suzuki as Takeshitaira Yamamo
* Gota Watabe as Koike
* Ayaka Komatsu as Kaori
* Tomomi Kasai as Mao
* Yoichi Nukumizu as Okada
* Shigeki Hosokawa as Meiousei O
* Eiji Bando as Professor Qingtao
* Jiro Sato as Professor Jun
* Miki Honoka as Yukari
* Itsuji Itao as Reporter

==References==
 

==External links==
*     
*  
*  
*  

 
 
 
 
 
 
 