Anne of Green Gables (1934 film)
{{Infobox film
| name           = Anne of Green Gables
| image          = Anne of Green Gables VideoCover.png
| caption        = DVD cover art
| director       = George Nichols Jr.
| producer       = Kenneth Macgowan
| writer         = Sam Mint
| based on       =   Dawn ODay Tom Brown Ben Hall Paul Stanton
| music          = Alberto Colombo Max Steiner Roy Webb
| cinematography = Lucien Andriot
| editing        = Arthur P. Schmidt
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = $226,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p55 
| gross          = $793,000 
}}
 Anne Shirley, Dawn ODay, changed her stage name to Anne Shirley after making this film.
 Arlington House).

==Plot== Dawn ODay) is an orphan who has been adopted by farmer Matthew Cuthbert (O.P. Heggie) and his sister Marilla (Helen Westley).  Although the pair were expecting a young boy to help on their farm, Anne endears herself to them and to the local villagers.

==Cast== Dawn ODay as Anne Shirley (billed in subsequent films as "Anne Shirley") Tom Brown as Gilbert Blythe
* Helen Westley as Marilla Cuthbert
* O.P. Heggie as Matthew Cuthbert
* June Preston as Mrs. Blewetts Daughter
* Sara Haden as Mrs. Rachel Barry
* Murray Kinnell as Mr. Phillips, the Teacher
* Gertrude Messinger as Diana Barry
* Charley Grapewin as Dr. Tatum
* Hilda Vaughn as Mrs. Blewett

==Reception==
The film made a profit of $272,000. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 