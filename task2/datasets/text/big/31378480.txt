Gerald (film)
{{Infobox film
| name           = Gerald
| image          = Movie poster for Gerald (film).jpg
| border         = yes
| alt            = Poster for Gerald
| caption        = Film poster
| director       = Marc Clebanoff
| producer       = 
| writer         = Tim Gallagher
| starring       = Louis Mandylor Mackenzie Firgens Deborah Theaker Mickey Rooney
| music          = Ran Ballard
| cinematography = Dave Selle
| editing        = Tim Gallagher
| studio         = Gerald The Movie, LLC Odyssey Motion Pictures Krakatau Films
| distributor    = Gerald The Movie, LLC
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
}} Little Shop of Horrors and Mr. Gallaghers personal experience with mentally challenged individuals.

Gerald was filmed around Los Angeles, CA in 2008.  The film premiered at the Chinese American Film Festival and has screened at the Down Beach Film Festival, Castle Rock Film Festival, the Sacramento Film Festival and Indie Spirit Film Festival, winning several awards including best actor for Louis Mandylor.

The role of Gerald, a mentally challenged individual, was a departure from Louis Mandylors earlier tough guys roles, but it is also an endearing addition to his indie film roles, such as Nick Portokalos in My Big Fat Greek Wedding.

==Plot==
Gerald Andrews was dropped on his head at birth. 30 years later, the man-child Gerald works at the local bowling alley and lives with his mother in a mobile home park. When his mother suddenly dies, he finds himself still needing her close, and has her ashes placed inside her favorite ceramic doll, which he carries with him everywhere. With the help of his mothers eclectic friends, Gerald is coping, until the day the valuable doll is stolen. Gerald desperately searches for his mothers ashes. When Gerald finally finds the thief, he enlists the help of his friend, Helen, to steal the doll back. During the heist, things go awry when Helen is taken hostage. Gerald must decide between his mothers ashes or Helens life.

== Cast ==
* Louis Mandylor as Gerald
* Mackenzie Firgens as Helen
* Luca Palanca as Lemtats son
* Beau Puckett as Dolly
* Tara Karsian as Ethel
* Wylie Small as Kate
* Deborah Theaker as Mother
* Mickey Rooney as the Doctor
* Mark Damon Espinoza as the Cop
* Tim Gallagher as the Lawyer

== Accolades ==
* Golden Angel Award - Chinese American Film Festival
* Best Actor for Louis Mandylor - Down Beach Film Festival
* Best Actor for Louis Mandylor - Sacramento International Film Festival
* Best Supporting Actress nomination for Deborah Theaker (Samantha Andrews a.k.a. Mother) - Down Beach Film Festival
* Best Supporting Actress nomination for Mackenzie Firgens (Helen) - Down Beach Film Festival
* Best Director for Marc Clebanoff - Down Beach Film Festival
* Best Screenplay for Tim Gallagher - Down Beach Film Festival
* Special Presentation - Castle Rock Film Festival

== Distribution ==
Besides film festival screenings, Gerald was screened in several theaters in Colorado.  U.S. distribution is through Gerald, the Movie, LLC which has made Gerald available as DVD and digital downloads from.  Australia territory was sold in 2010.

== References ==
 

==External links==
*  
*  
*  

 
 
 
 