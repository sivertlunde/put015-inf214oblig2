Vidhi Vilasa
{{Infobox film 
| name           = Vidhi Vilasa
| image          =  
| caption        = 
| director       = S. V. Mahesh
| producer       = N N Murugayyan
| writer         = H. L. Narayana Rao
| screenplay     = H. L. Narayana Rao Rajkumar Udaykumar Narasimharaju K. S. Ashwath
| music          = T. Padman
| cinematography = S K Varadarajan K Govinda Swamy
| editing        = P S Murthy P U S Mani V S Mani
| studio         = Murugan Productions
| distributor    = Murugan Productions
| released       =  
| country        = India Kannada
}}
 1962 Cinema Indian Kannada Kannada film, Narasimharaju and K. S. Ashwath in lead roles. The film had musical score by T. Padman.  

==Cast==
  Rajkumar
*Udaykumar Narasimharaju
*K. S. Ashwath Leelavathi
*Harini
*Lakshmidevi
*B. Jaya (actress)|B. Jaya
*Vasanthi
*Mala
 

==Soundtrack==
The music was composed by T. Padman. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Janaki || Narayana Rao || 02.55
|-
| 2 || Sarasake Baa Sukumaara || Jamuna Rani || Narayana Rao || 03.18
|-
| 3 || Aananda Saamrajyada || S. Janaki|Janaki, PB. Srinivas || Narayana Rao || 02.59
|-
| 4 || Ariyade Ninnaya Gunagala || PB. Srinivas || Narayana Rao || 04.50
|-
| 5 || Bidu Gopi ||  || Narayana Rao || 02.05
|- Janaki || Narayana Rao || 03.18
|-
| 7 || Naane Neenu || Sarojini, Nagendra || Narayana Rao || 02.55
|}

==References==
 

==External links==
*  
*  
*  

 
 
 


 