Tom Horn (film)
{{Infobox Film
| name           = Tom Horn
| image          = Tom Horn cover.jpg
| image_size     = 
| caption        = 2005 DVD cover
| director       = William Wiard
| producer       = Fred Weintraub Steve McQueen (exec. producer)
| writer         = Thomas McGuane Bud Shrake Tom Horn (autobiography)
| starring       = Steve McQueen Linda Evans Richard Farnsworth Ernest Gold
| cinematography = John A. Alonzo
| editing        = 
| distributor    = Warner Bros.
| released       = March 28, 1980
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1980 Western legendary lawman, outlaw, and gunfighter. It starred Steve McQueen as the title character and was based on Horns own writings. 

==Plot== Jim Corbett, ending up in a livery stable, unconscious and badly bruised.

Cattle company owner John Coble finds Horn in the livery and offers him his ranch to recuperate. He also offers him work investigating and deterring cattle rustlers who steal from the grazing association to which Coble belongs. He implies that the association will support Horn in implementing vigilante justice. Horn accepts the offer and receives the approval of U.S. marshal Joe Belle at an association picnic where he also catches the eye of the local schoolteacher, Glendolene.

Calling himself a "stock detective," Horn confronts cowboys at an auction whose cattle bear Cobles brand.  After giving them fair warning he goes on a one man crusade to kill or otherwise drive off anyone who rustles the cattle of his benefactors.

Horns methods are brutal but effective. After a public gunfight, the local townspeople become alarmed at his violent nature and public opinion turns against him. The owners of the large cattle companies realize that while he is doing exactly what they hired him to do, his tactics will ultimately tarnish their image and begin to plot his demise.  Joe Belle, who has political ambitions, wants Horn out of the way for the same reasons. Their conspiracy is set in motion when a young boy tending sheep is shot by a .45-60; the same caliber rifle Tom Horn is known to use.

Horn is slow to realize that he is being set up. Proud and convinced of his own innocence, he refuses to leave the country or avoid the town. Glendolene and Coble try to warn him to be careful, but Horn ignores the warning.  Joe Belle coaxes Horn out of a saloon and back to his office where a man transcribing their conversation is hidden in the next room.  Horn does not admit to the murders but states that "If I did shoot that boy, it was the best shot I ever made." Based on this conversation Horn is taken prisoner.

Unaccustomed to being unable to come and go as he pleases into his beloved hills, Horn seems lost.  He breaks out of jail and attempts to flee.  He is recaptured and convicted based on the testimony of the newspaperman who skewed the conversation between Belle and Horn.

As his execution nears, Horn accepts his fate and remains resolved in the moments before he is hanged.

==Production==
 An Enemy Towering Inferno Raise the Richard Fleischers planned adaptation of Tai-Pan (novel)|Tai-Pan when the second $1m installment of his announced $10m fee failed to arrive (the actor having earned $1m for no work already). However, after his divorce from Ali MacGraw McQueen decided to get back into films. He initially wanted to adapt Harold Pinters play Old Times but First Artists insisted that he instead film Tom Horn, a script they had owned for some time, as the final film in the stars three picture deal he had signed with them under Warner Bros.
 DGA rules forbidding actors from taking over direction once filming had begun scotched these plans and instead TV movie director William Wiard was brought in to finish the film. This was Wiards only feature film directing credit.
 R rating.

It was during production that McQueen had trouble breathing and was later determined to have had a rare form of lung cancer called malignant mesothelioma.

==Cast==
*Steve McQueen as Tom Horn
*Linda Evans as Glendolene Kimmel
*Richard Farnsworth as John C. Coble
*Billy Green Bush as U.S. Marshal Joe Belle
*Slim Pickens as Sheriff Sam Creedmore
*Peter Canon as Assistant Prosecutor
*Elisha Cook, Jr. as Stablehand
*Harry Northup as Thomas Burke
*Drummond Barclay as Charlie Ohnhouse

==References==

 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 