The Outcasts of Poker Flat (1937 film)
{{Infobox film
| name           = The Outcasts of Poker Flat
| image          = The Outcasts of Poker Flat (1937 film) poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Christy Cabanne
| producer       = Robert Sisk 
| screenplay     = John Twist Harry Segall
| based on       =  
| starring       = Preston Foster Jean Muir Van Heflin Virginia Weidler Margaret Irving
| music          = Roy Webb
| cinematography = Robert De Grasse
| editing        = Ted Cheesman 
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film directed by Christy Cabanne and written by John Twist and Harry Segall. The film stars Preston Foster, Jean Muir, Van Heflin, Virginia Weidler and Margaret Irving. The film was released on April 16, 1937, by RKO Pictures.   
 short story 1919 with Harry Carey 1952 with Dale Robertson.

==Plot==
 

== Cast ==
*Preston Foster as John Oakhurst Jean Muir as Miss Helen Colby
*Van Heflin as Reverend Sam Woods
*Virginia Weidler as Luck
*Margaret Irving as The Duchess
*Frank M. Thomas as Mr. Bedford
*Si Jenks as Kentuck
*Dick Elliott as Stumpy Carter
*Al St. John as Uncle Billy
*Bradley Page as Sonoma
*Richard Lane as High-Grade
*Monte Blue as Indian Jim
*Billy Gilbert as Charley
*Dudley Clements as Wilkes

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 

 