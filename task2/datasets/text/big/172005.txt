Sadie Thompson
{{Infobox film
| name           = Sadie Thompson
| image          = Sadie_Thompson1.jpg
| image_size     =
| caption        = Click on to enlarge
| director       = Raoul Walsh
| producer       = Gloria Swanson Productions
| writer         = Gloria Swanson Raoul Walsh 
| story          = W. Somerset Maugham
| based on       =  
| starring       = Gloria Swanson Lionel Barrymore Blanche Friderici Charles Lane
| music          = George Barnes Robert Kurrle Oliver T. Marsh
| editing        = C. Gardner Sullivan
| distributor    = United Artists
| released       =  
| runtime        = 91 minutes
| country        = United States Silent English English intertitles
| budget         = US$650,000 
| gross          = $1 million (domestic)  $7 million (international) 
}}
 silent drama San Francisco. The film stars Gloria Swanson, Lionel Barrymore, and Raoul Walsh, and was one of Swansons better known silent films.
 John Colton and Clemence Randolph.

==Plot==

A smoking, drinking, jazz listening, young prostitute named Sadie Thompson (Gloria Swanson) arrives at the island of Tutuila (now part of American Samoa), claiming she is waiting for employment upon a ship.  At the same time, moralists arrive, including Mr. and Mrs. Davidson (Lionel Barrymore and Blanche Friderici).  They all end up staying in the same hotel, where the Davidsons plot to teach the natives about sin and Sadie entertains a bunch of Marines.

Sadie begins to fall in love with Sergeant Timothy OHara (Raoul Walsh), who isn’t fazed by her past.  He tells her that he has a best friend who married a former prostitute, and the couple now live happily in Australia.

Davidson sets about trying to redeem Sadie, much to her disgust.  She doesnt see why she should have to answer to him above anyone else; finding him self-important.  Davidson tricks her into telling him about her past in San Francisco and, once she refuses to repent, he declares that he will go to the Governor and have her deported.  Sadie is terrified of the idea but OHara assures her that it wont happen.  He tells her he wishes she would go to Australia and wait for his term of service to finish, after which  theyll marry. This they both agree upon.

Davidson gets his way, however, and Sadie is livid.  She and OHara go to plead with the Governor, begging him to let her go to Australia instead of back to San Francisco.  Once we find Davidson has also managed to get OHara punished for being immoral, were told that Sadie will be able to go to Australia instead, but only if Davidson okays it.  Davidson refuses and Sadie pleads, but to no avail.  She eventually confesses that, if she goes back to San Francisco, there is a man there who wont let her go straight, which is what she wants to do.  Davidson figures out this means that there is a warrant for her arrest back in San Francisco.  Sadie claims that she was framed and is innocent, but it wont matter since, if she ends up in San Francisco, shell have to go to prison nonetheless.

Davidson still refuses, saying she must atone for her past.  Sadie pleads and pleads and eventually offers to repent.  Davidson, however, says that her offer of repentance doesn’t matter and that the only way to fully repent is for her to go to prison.  Sadie runs to her room, crying out for Davidson.  Davidson returns and Sadie confesses she is afraid.  Davidson then tells her that, if she repents, there will be nothing to fear and he begins to pray with her.  Sadie converts to Christianity.

Three days later, were told that Sadie has been praying the entire time.  She has put away her old things and has become a modest woman.  OHara returns and finds Davidson is gone, apparently "trying to stop the locals from dancing on the beach".  OHara tells Sadie that he has a fishing boat waiting to take her and her things to a ship, that will then take her to Australia, where they can marry and be free.  Sadie is extremely afraid and refuses to go, saying that old Sadie is dead and she must go to San Francisco and prison, to repent.  She fully relies and believes in Davidson now.

OHara does everything he can, including forcibly taking her from the room, but Davidson is waiting outside.  OHara tries to attack him but Sadie asks him not to, so he doesn’t.  OHara, extremely upset, leaves and Sadie pleads with Davidson not to get him in trouble, for it was all her fault.  She has become childlike and dependent on Davidson.

Later that night, Sadie is asleep and everyone else is heading to bed.  Davidson can not sleep and goes out for a walk in the rain. (It has rained throughout almost the entire film.)  His wife says he can’t sleep for "the unpleasant dreams he’s been having about Miss Thompson".  A fellow boarder suspects they aren’t all that unpleasant.  Outside, Davidson is struggling with himself and realizes that he is sexually attracted to Sadie and unable to handle it.  He looks into her window and eventually is able to return inside to his room.

Sadie, frightened because she heard noises, is waiting in Davidsons room.  Davidson is shocked and sends her back to her room.  Here the last reel is missing but the rest of the film apparently included Davidson jumping into the ocean and killing himself, unable to handle his conflicted passions.  A fisherman finds his body.  Sadie and OHara reconcile and head for Australia.

==Cast==
*Lionel Barrymore as Mr. Alfred Davidson
*Blanche Friderici as Mrs. Alfred Davidson
*Charles Willis Lane as Dr. Angus McPhail
*Florence Midgley as Mrs. Angus McPhail
*James A. Marcus as Joe Horn, the trader
*Sophia Artega as Ameena Will Stanton as Quartermaster Bates
*Raoul Walsh as Sergeant Timothy Tim OHara
*Gloria Swanson as Sadie Thompson

==Production== Roxy Theatre. Gold Rush." Schenck pleaded with her to do a commercially successful film like The Last of Mrs. Cheyney. Swanson felt it too formulaic and decided to call upon director Raoul Walsh, who was signed with Fox Film Corporation at the time. 
 John Colton and Clemence Randolph play Rain (1923), which in turn was based on the story by W. Somerset Maugham titled "Miss Thompson" (1921).  Swanson had seen Jeanne Eagels perform the role on stage twice and enjoyed it. 
 The Scarlet Letter (1926) at MGM. 
 Will Hays for lunch and summarized the plot, naming the author and the sticking points. According to Swanson, Hays made a verbal promise that he would have no problem with the making of such a film. Swanson set about getting the rights to the play by having Schenck pretend to buy it in the name of United Artists, never to be used. Thus they were able to get the story rights for $60,000 instead of the original $100,000.  When news broke as to just what was intended with the play, the three authors threatened to sue.   Swanson later contacted Maugham requesting that he write an original story that served as a sequel. The sequel would involve following what became of Sadie in Australia. Maugham agreed to write a new story (for $100,000) but a sequel was never made. 

Swanson and Walsh set about writing the script,  and discreetly placed an ad announcing the film, thinking no one noticed, as Charles Lindbergh had just made his historic flight. However, the press picked up on it and sensationalized the story.   United Artists received a threatening two-page telegram from the MPAA, signed by all its members including Fox (Walshs studio) and Hays himself.     In addition, the rest of the signers owned several thousand movies houses and if they refused to screen the film, it could be a financial disaster.  This was the first time Swanson had heard the name of Joseph P. Kennedy, with whom she would later have an affair and who would finance her next few pictures, including Queen Kelly (1929). 

Swanson was angered by the response, as she felt those very studios had produced "questionable" films themselves and were jealous at not having the chance to produce Rain. Swanson 1981 p.307  After another threatening telegram, she decided to first appeal to MPAA and then the newspapers.   She only heard back from Marcus Loew, who promised to appeal on her behalf, and since he had a chain of theatres, this eased some of her concerns.  Figuring the silence meant the matter had been dropped, Swanson began filming on Sadie Thompson, which already had a quarter of a million dollars invested in it. 

Before casting began, 17 year old Douglas Fairbanks, Jr. wanted to audition for the role of Handsome OHara.  However, Swanson felt he was too young and not right for the role.   Lionel Barrymore had been first picked to play Davidson, but was thought to be too ill at the time, though he did eventually win the role.  Barrymore wore the same outfit for an entire week, aggravating Swanson.  She asked some of the crew to tell him to change and wash; which he did indeed do.   Despite this, Swanson was happy with his performance.  Walsh had not acted in front of a camera in eight years and feared he would not be able to both direct and act at the same time.  However, two days into filming, his fears were quelled. 
 Santa Catalina George Barnes away.  Swanson was furious, but the loan contract allowed Goldwyn to call him away as he pleased. Not wanting to let a hundred extras sit around for days, Swanson and Walsh tried to hire two other cameramen, but both were unsatisfactory.  Mary Pickford offered the services of her favorite cameraman, Charles Rosher, who was called in, but despite doing a decent job, could not match Barnes work.  Through Loew, MGM loaned Oliver Marsh, who completed the picture. Swanson 1981 p.321 

The entire cameraman problem was extremely costly to the production, yet the picture went on. With the picture half finished, it was already way over budget and Schenck was wary, as Swansons first picture had also been over budget and underperformed. Swanson talked with her advisers and sold her farm at Croton-on-Hudson, and offered to sell her New York penthouse as well. 

Despite reports that dirty words can be read on the characters lips, Swanson claims the censors went over everything with a fine-tooth comb. Swanson 1981 p.322  However, Swanson admitted one line while she was shouting at Davidson went, "Youd rip the wings off of a butterfly, you son of a bitch!", when recounting a conversation with Walsh later in life.   If the word "rain" was used in a title card, the censors asked that it be removed.  They also wanted to change Davidsons name to something else, but Swanson and Walsh refused. 

==Release and reception==
 
Upon its release, Sadie Thompson was praised by critics as was Swansons performance.  Contemporary film critics have cited Swansons performance as one of her best.  
 Sunset Blvd.  It went on to make $1 million in the United States and $7 million internationally.     However, at Kennedys advice, Swanson had sold her distribution rights for the film to Schenck, as Kennedy felt it would be a commercial failure. He also did not care for the image Swanson portrayed in the film. By this point, Queen Kelly had been a disaster and Swanson regretted it. Swanson 1981 p.374 

The film made the top 10 best pictures of the year list as well.   It would be the last film Raoul Walsh acted in, as he soon lost an eye in an accident.

==Award nominations== Best Cinematography at the 1st Academy Awards for this film, The Devil Dancer and The Magic Flame.  Swanson did not attend the ceremony, and always felt it was like "comparing apples to oranges". 

==Restoration and home media release== lost for Kino International 1932 adaptation (which cinematographer Oliver T. Marsh also shot) to restore the footage. A new score by Joseph Turrin was also composed for the film. 

Kinos version has been issued on DVD and online via Netflix.

==Other film adaptations==
*1932 - as Rain (1932 film)|Rain, starring Joan Crawford, Walter Huston, Guy Kibbee and Beulah Bondi; adapted by Maxwell Anderson, directed by Lewis Milestone. Don Wilson, adapted by True T. Thompson, directed by Spencer Williams
*1949 - a film noirish "Sadie Thompson" dance number from the Marx Brothers film Love Happy features Vera-Ellen dancing with a group of World War II Marines led by Paul Valentine. Academy Award Best Music, Song (for Lester Lee and Ned Washingtons "Sadie Thompsons Song (Blue Pacific Blues)").

==See also==
*Sadie Thompson Building, setting of Maughams story in Pago Pago, listed on the U.S. National Register of Historic Places
*Lionel Barrymore filmography

==References==
 

==External links==
*  
*  
*  
*   at Virtual History
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 