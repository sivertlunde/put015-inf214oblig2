Major League (film)
{{Infobox film name           = Major League image          = Major league movie.jpeg caption        = Theatrical release poster director       = David S. Ward producer       = Chris Chesser writer         = David S. Ward starring       = Tom Berenger Charlie Sheen Corbin Bernsen Margaret Whitton James Gammon Rene Russo Bob Uecker music          = James Newton Howard editing        = Dennis M. Hill cinematography = Reynaldo Villalobos studio         = Morgan Creek Productions Mirage Productions distributor    = Paramount Pictures   J&M Entertainment   released       =   runtime        = 107 minutes country        = United States language       = English budget         = $11 million gross          = $49,797,148
}}
Major League is a 1989 American  , which were released by Warner Bros.), neither of which replicated the success of the original film.

==Plot==
  Las Vegas General Manager doctor his pitches; and third baseman Roger Dorn, a one-time star who is under contract but has become a high-priced prima donna.  As manager, Phelps hires Lou Brown, a career minor league manager of the Toledo Mud Hens who works in the off-season as a tire salesman.
 pop flies, and Cerrano, despite his tremendous power, cannot hit a curveball. The veterans have their own problems: Dorn refuses to aggressively field ground balls, afraid that potential injuries will damage his upcoming contract negotiations, and it becomes clear that Taylors bad knees will be a season-long concern. On the final day, Brown cuts the team down to 25 players, including Vaughn, Mays-Hayes, Taylor, Dorn, Cerrano, and Harris.

After the team returns to Cleveland before the season begins, Taylor takes Vaughn and Hayes out to dinner but comes across his ex-girlfriend Lynn, who is dining with her current beau. Taylor believes he can try to win her back by proclaiming that he has a major league job again, but is disappointed to hear that she is already engagement|engaged.

The Indians season starts off poorly. Vaughns initial pitching appearances end in disaster, with his wild pitches earning him the derogatory title "Wild Thing." On a rare occasion when Vaughn does get a ball over the plate, it is hit well over 400 feet by the New York Yankees best hitter, Clu Haywood. Brown discovers that Vaughns eyesight is poor; once Vaughn gets glasses his control greatly improves, and he becomes the teams ace. Despite their flaws, the team begins to improve, so Phelps decides to demoralize them further by removing luxuries, such as replacing their team jet airplane first with a dilapidated prop plane, then with an old bus. However, these changes do not affect the Indians performance and the team continues to win. Donovan reveals Phelps plan to Brown, who then relays the same news to the players. Brown also tells them that if the team plays too well for Phelps to void the lease, she will release them all regardless. With nothing to lose, the team agrees to get back at Phelps by winning the pennant. Brown motivates the team further by providing a double-layered cardboard cutout of Phelps from her showgirl days; after every victory, a piece of the second layer is removed, eventually presenting a nude picture of Phelps.

The team plays very well down the stretch of the season, and clinch a tie for first in the American League East by beating the Chicago White Sox on the last day of the season. This forces a one-game playoff with the divisions co-leaders, the Yankees. Prior to the playoff, Taylor continues to try to woo Lynn back and they share a night together. Vaughn learns that he will not be the starting pitcher for the game and goes to a bar to mope, where he encounters Suzanne Dorn. Feeling spited after witnessing her husband Roger with another woman; she retaliates by luring Vaughn to sleep with her. Vaughn is unaware of who she is until she tells him before leaving the apartment.
 nemesis on three straight fastballs to end the inning.
 infield single. bunts instead, catching the Yankees infield off-guard. Despite his weak knees, Taylor manages to beat out the throw to first as Hayes rounds third and heads for home plate. Hayes slides safely into home, giving the Indians the win on a walk-off single. Jake finds Lynn in the stands, who raises her left hand to show that she is no longer wearing an engagement ring, and they reunite as the film closes.

==Alternate ending==
The theatrical releases ending includes Rachel Phelps, apparently unable to move the team because of increased attendance, angry and disappointed about the teams success. An alternate ending on the "Wild Thing Edition" DVD shows a very different characterization of Phelps.  Lou tenders his resignation and tells Phelps that he cant in good conscience work for her after she sought to sabotage the team for her own personal gain.  Phelps then tells him that, in fact, she loves the Indians and never intended to move them.  However, when she inherited the club from her late husband, it was on the brink of bankruptcy.  Unable to afford top flight players, she decided to take a chance on unproven players from the lower leagues, whom she personally scouted, and talented older players who were generally considered washed up.  She tells Lou that she likewise felt that he was the right manager to bring the ragtag group together.

Phelps made up the Miami scheme and adopted a catty, vindictive persona to unify and motivate the team.  As the players believed that she wanted the Indians to fail, she was able to conceal that the team could not afford basic amenities such as chartered jet travel behind a veil of taking them away to spite the players.

Lou does not resign, and Phelps reasserts her authority by saying that if he shares any part of their conversation with anyone, she will fire him.

Producers said that while the twist ending worked as a resolution of the plot, they scrapped it because test screening audiences preferred the Phelps character as a villain.

==Casting== US President David Palmer on the television series 24 (TV series)|24.
 1982 American League Cy Young Award winner Pete Vuckovich as Yankees first baseman Clu Haywood, former Milwaukee Brewers pitcher Willie Mueller as the Yankees pitcher known as "The Duke", and former Los Angeles Dodgers catcher Steve Yeager as third-base coach Duke Temple.  Former catcher and longtime Brewers broadcaster Bob Uecker played the Indians broadcaster Harry Doyle. The names of several crewmembers were also used for peripheral players.
 steroids for nearly two months to improve his athletic abilities in the film.) 

*Tom Berenger as Jake Taylor
*Charlie Sheen as Ricky Vaughn
*Corbin Bernsen as Roger Dorn
*Margaret Whitton as Rachel Phelps
*James Gammon as Lou Brown
*Rene Russo as Lynn Wells
*Wesley Snipes as Willie Mays Hayes
*Charles Cyphers as Charlie Donovan
*Chelcie Ross as Eddie Harris
*Dennis Haysbert as Pedro Cerrano
*Andy Romano as Pepper Leach
*Bob Uecker as Harry Doyle
*Steve Yeager as Duke Temple
*Peter Vuckovich as Clu Haywood
*Stacy Carroll as Suzanne Dorn
*Neil Flynn as longshoreman Indian fan

==Background==
The films opening montage is a series of somber blue-collar images of the Cleveland landscape synchronized to the score of   caught fire.
 Miller Park; new Cleveland Browns Stadium, a football-only facility owned by the City of Cleveland and used by the Browns, sits on the site of its predecessor. 

==Box office reception== Variety calling it "sheer crowd pleasing fun".

Due to the success of the film, two sequels have been produced - neither of which achieved the originals success.   again starred Corbin Bernsen, but this time, as the owner of the Minnesota Twins, attempting to turn around the Twins AAA team, the Buzz. This movie cost $46M and grossed only $3.6M. A possible third sequel, titled Major League 3, is reportedly in the works by David S. Ward, the writer and producer of Major League. The movie would return Charlie Sheen, Tom Berenger, and Snipes with the plot revolving around Ricky Vaughn coming out of retirement to work with a young player. 

==Roster==
The Team (by uniform number):

*00 - Willie Mays Hayes, CF
*7 - Jake Taylor, C
*8 - Duke Temple, 3rd Base Coach
*10 - Eddie Harris, SP
*12 - Crespi, Position Unknown
*13 - Pedro Cerrano, RF
*14 - Van Dyke, Position Unknown
*15 - Reyna, SS
*16 - Pepper Leach, 1st Base Coach/Pitching Coach
*20 - Larson, 2B
*21 - Kuntz, Position Unknown
*23 - Graham, Backup 1B
*24 - Roger Dorn, 3B
*26 - Lindberg, Position Unknown
*27 - Campi, Position Unknown
*28 - Pearson, Position Unknown
*30 - Metcalf/Ward, 1B
*33 - Bushnell, SP
*34 - Lou Brown, Manager
*37 - Stocker, Relief Pitcher
*38 - Tomlinson, LF
*39 - Reaves, Assistant Coach
*40 - Keltner, SP
*41 - Rhoads, Position Unknown
*44 - Mosser, Position Unknown
*45 - Schindler, Position Unknown
*47 - Gentry, SP (Cut in Spring Training; the player who finds a red tag in his locker)
*48 - Huffman SS
*49 - Winter, Position Unknown
*99 - Rick Vaughn, SP/RP
*?? - Mitchell Friedman, Position Unknown

== In popular culture == Wild Thing" as Williams came out of the bullpen. A few years later, in 1993 with the Phillies, Williams started wearing the number 99 on his jersey, the same number that Vaughn wears in the film. 
 David Ross filmed a one-man tribute to the film, with Ross playing the part (among others) of Lou Brown, Pedro Cerrano, Willie Mays Hayes, Rick Vaughn, and Roger Dorn.  Additionally, as part of their 2014 "Archives" set, the trading card company Topps celebrated the movies 25th anniversary by creating baseball cards (using the same design as the companys 1989 base set) of Roger Dorn, Jake Taylor, Eddie Harris, Rachel Phelps, Rick Vaughn, and "Jobu." 

Soon after the films 25th anniversary in 2015, the character Jobu (Pedro Cerranos voodoo figure) was immortalized and produced by a company called "The Jobu Lifestyle."  The packaging is a reference to Cerranos locker that made up Jobus shrine.    

==References==
 

==External links==
 
 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 