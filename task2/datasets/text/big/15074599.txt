Pardon My Rhythm
{{Infobox film
| name           = Pardon My Rhythm
| image          =
| caption        =
| director       = Felix E. Feist
| producer       =
| writer         =
| narrator       =
| starring       = Gloria Jean Patric Knowles Evelyn Ankers
| music          =
| cinematography = Paul Ivano
| editing        = Edward Curtiss
| distributor    = Universal Pictures
| released       =  
| runtime        = 62 minutes
| country        = United States English
| budget         =
}}

Pardon My Rhythm is a 1944 movie starring Gloria Jean, Patric Knowles, and Evelyn Ankers, featuring Mel Tormé and Bob Crosby, and directed by Felix E. Feist.

==Cast==
*Gloria Jean as Jinx Page
*Patric Knowles as Tony Page
*Evelyn Ankers as Julia Munson
*Marjorie Weaver as Dixie Moore
*Walter Catlett as OBannion
*Mel Tormé as Ricky OBannon
*Patsy OConnor as Doodles
*Ethel Griffies as Mrs. Dean
*Jack Slattery as Announcer
*Linda Reed as Soda Fountain Waitress
*Alphonse Martell as Headwaiter
*Bob Crosby as Orchestra Leader
*James Floyd as Bully

==External links==
* 

 
 
 
 
 
 
 


 