Love You Hamesha
 
{{Infobox Film| name           = Love You Hamesha image          = Love you Hamesha_soundtrack.jpg image_size     =  director       = Kailash Surendranath writer         = Sanjay Chhel Manoj Lalwani Vivek Sharma    starring       = Akshaye Khanna  Sonali Bendre  music          = A. R. Rahman | producer       = Vijay Amritraj art direction  = Bijon Das Gupta cinematography = R. M. Rao editing        = Omkar Bhakri distributor    =  released       = 2001 runtime        = 
| country       = India language       = Hindi language|Hindi| gross          = 
}}
 2001 Hindi Hindi musical film directed by Kailash Surendranath, with music by A. R. Rahman. The film features Akshaye Khanna and Sonali Bendre in the lead roles. The film was remade from the 1994 Tamil film  May Madham.

==Cast==
*Akshaye Khanna ...  Shaurat
*Sonali Bendre ...  Shivani
*Rishma Malik ...  Geeta
*Riya Sen ...  Meghna (as Rhea Dev Verma)
*Ketan Desai ...  Himself
*Lillete Dubey
*Vijayendra Ghatge
*Seema Kelkar
*Amitabh Nanda ...  Samrat / Shivanis finance
*Nirupa Roy
*Dalip Tahil

==Soundtrack==
{{Infobox album |
  Name        = Love you Hamesha |
  Type        = soundtrack |
  Artist      = A.R. Rahman |
  Cover       = |
  Background  =  |
  Released    = 2001|
  Recorded    = Panchathan Record Inn|
  Genre       =  Film soundtrack |
  Length      =  |
  Label       =  Saregama |
  Producer    =  A. R. Rahman |
  Reviews     =  |
  Last album  =    (2001) |
  This album  =  Love you Hamesha (2001) |
  Next album  =  Lagaan (2001)|
}}
All songs were composed by A. R. Rahman. He reused the songs he had composed for the original film, May Madham. The soundtrack features 6 songs with lyrics penned by Anand Bakshi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Song !! Artist(s)
|-
| 1
| "Ek Ladki Thi"
| Kavita Krishnamurthy
|-
| 2
| "Love You Hamesha"
| Sonu Nigam, Shweta Shetty, G. V. Prakash Kumar|G. V. Prakash 
|-
| 3
| "Gup Chup Baatein"
| Hariharan (singer)|Hariharan, Sadhana Sargam
|-
| 4
| "Yaar Teri Bewafaii"
| Mahalaxmi Iyer
|-
| 5
| "Botal Tod De"
| Sonu Nigam, Hema Sardesai
|-
| 6
| "Sone Ka Palang"
| Udit Narayan, Kavita Paudwal, Ila Arun
|}
==References==
 
==External links==
*  

 
 
 
 
 


 
 