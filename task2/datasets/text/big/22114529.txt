Pingami
{{ infobox film
| name           = Pingami
| image          = Pingami.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Sathyan Anthikkad
| producer       = Mohanlal
| writer         = Reghunath Paleri
| narrator       =  Kanaka  Jagathy Sreekumar Johnson
| cinematography = Vipin Mohan
| editing        = K. Rajagopal
| studio         = Pranavam Arts
| distributor    = Anupama Films
| released       = 1994
| runtime        = 160 mins
| country        = India Malayalam
| budget         = 1.5 crores
| gross          = 2 crores
| preceded by    = 
| followed by    = 
}}
Pingami (The Follower) (Malayalam : "പിന്‍ഗാമി")is a 1994 Malayalam action film directed by Sathyan Anthikkad and produced by Mohanlal under the banner of Pranavam Arts.

==Plot==
Kumaran (Thilakan) is a social worker. Captain Vijay (Mohanlal) is an army captain who is on his annual leave and has come to stay with his maternal uncle who raised him and his sister after the untimely death of their parents. In a turn of events Vijay sees Kumaran injured from an accident and crying for help. He takes Kumaran to a hospital but could not save his life. There develops a strong bond between Kumaran and Vijay which pulls him into finding out the accident was a preplanned murder. 

With the help of his friend Kutti Hassan (Jagathy Sreekumar|Jagathy) he starts on a trail to find out the mystery behind the murder and in the midst finding out the missing links of his own life. As the film proceeds it is revealed that Kumaran was murdered by the same people who killed Vijays father. Vijay finally takes revenge upon his enemies in his own method. Meanwhile, he falls in love with Kumarans daughter (Kanaka). He gets his sister who was in a charity home. (It is shown earlier that Kumaran took the girl to that home.)

The movie depicts the relation between two people even though they havent talked or met each other.

==Cast==
*Mohanlal as Captain Vijay Menon
*Thilakan as Kumaran aka Kumarettan
*Sukumaran as George Mathew Janardhanan as Koshy Varghese
*Jagathy Sreekumar as Kutti Hassan Innocent as Iyengar
*Oduvil Unnikrishnan as Menon
*Puneet Issar as Edwin Thomas/Achayan Kanaka as Sridevi
*Kuthiravattam Pappu as Achuthan
*Sankaradi as Muthappan
*Mala Aravindan as Velichappadu Devan as Vijay Menons father
*Shanthi Krishna as Vijay Menons mother
*Meenakumari as Menons wife Kunchan as auto driver
*Vinduja Menon as Ganga/Mary/Chinnumol
*Paravoor Bharathan as advocate
*T. P. Madhavan as newspaper editor
*V. K. Sreeraman as police officer
*Poornam Vishwanathan as Varma
*Sadiq as studio owner
*Ottapalam Pappan Seetha as Parvathi Santhakumari as Kumarettans wife
*Antony Perumbavoor as car driver Devasikutty
*Santha Devi as mother superior Abu Salim as Muthu
*Bindu Varappuzha as Rukhiya
*Meena Ganesh as Kuttyhasans mother

==Soundtrack==
{{Infobox album
| Name        = Pingami
| Type        = Soundtrack Johnson
| Language = Malayalam
| Genre       = Film
|}}
 Johnson and written by Kaithapram Damodaran Namboothiri|Kaithapram.

{| class="wikitable"
|-
! Track !! Song Title !! Singer(s)
|-
| 1 || Themmaadikkaatte Ninnaatte || K. J. Yesudas, MG Sreekumar
|-
| 2 || Vennilavo || K. S. Chithra
|}

==External links==
* 

 
 
 
 
 
 
 