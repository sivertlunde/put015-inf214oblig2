Rising to the Bait
{{Infobox film
| name           = Rising to the Bait
| image          = 
| caption        = 
| director       = Vadim Glowna
| producer       = Harald Reichebner
| writer         = Knut Boeser Christine Roesch
| starring       = Elsa Grube-Deister
| music          = 
| cinematography = Franz Ritschel
| editing        = Karola Mittelstädt
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = Germany
| language       = German
| budget         = 
}}

Rising to the Bait ( ) is a 1992 German comedy film directed by Vadim Glowna. It was entered into the 42nd Berlin International Film Festival.   

==Cast==
* Elsa Grube-Deister as Ada Fenske
* Rolf Zacher as Zwirner
* Muriel Baumeister as Svetlana
* Ben Becker as Funke
* Günter Kornas as Solters
* Franz Viehmann as Fiedler
* Hans Jochen Röhrig as Pastor Seidel
* Roman Silberstein as Hein Geerke
* Werner Schwuchow as Braeske
* Bruno Dunst as Schreker
* Wolf-Dietrich Berg as Naujok
* Heinz-Werner Kraehkamp as Raschke
* Ralf Sählbrandt as Benno

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 