What Lies Beneath
 
{{Infobox film name           = What Lies Beneath image          = What lies beneath.jpg caption        = Theatrical release poster director       = Robert Zemeckis producer       = Steve Starkey Robert Zemeckis Jack Rapke screenplay     = Clark Gregg story          = Sarah Kernochan Clark Gregg starring       = Harrison Ford Michelle Pfeiffer music          = Alan Silvestri cinematography = Don Burgess editing  Arthur Schmidt studio  ImageMovers
|distributor    = DreamWorks Pictures (North America)  20th Century Fox (International) released       =   runtime        = 130 minutes country        = United States language       = English budget         = $100 million http://www.boxofficemojo.com/movies/?id=whatliesbeneath.htm  gross          = $291.4 million 
}}
What Lies Beneath is a 2000 American psychological thriller film directed by Robert Zemeckis. It is the first film by ImageMovers. It stars Harrison Ford and Michelle Pfeiffer as a well-to-do couple who experience a strange haunting that uncovers secrets about their past.  The film opened in 2,813 theaters in North America and grossed $29,702,959. The film received mainly mixed reviews, and received three Saturn Award nominations.

==Plot== gaps in her memory. Combined with her daughter Caitlins (Katharine Towne) departure for college, this leaves Claire profoundly affected.

The Spencers — who have moved into the former home of Normans father — live next door to the Feurs, who have just moved in and frequently engage in loud arguments. Overhearing Mary Feur (Miranda Otto) sobbing one day, Claire becomes concerned. When she sees Marys husband Warren (James Remar) seemingly dragging something that resembles a body bag in the middle of the night and putting it in the trunk of the car, she suspects that Mary has been murdered.

Claire decides to investigate. After nobody answers the door, she walks around the side of the house and discovers a womans sandal with a dark stain on it. Claire begins spying on Warren with the help of her best friend Jody (Diana Scarwid). However, Claire soon witnesses strange occurrences when she is alone in the house and becomes convinced that Mary is dead and haunting her.

Desperate for closure, and facing little sympathy from Norman, Claire invites Jody to join her for a séance in her bathroom, where she seems to be able to communicate with Marys ghost.

An anxious Claire goes to Normans office to inform him of the séance. While traveling through Normans campus, Claire encounters Warren and hysterically accuses him of killing his wife. But Warren responds with confusion before Mary—very much alive—joins him. When Mary speaks with Claire later, she explains that she had left Warren and that he had been packing her belongings when Claire saw him.

The Spencers assume that the ordeal is over, but later a picture breaks in Normans study. Picking it up, Claire notices a newspaper clipping about a missing girl named Madison Elizabeth Frank (Amber Valletta). She learns that Madison attended the university where Norman was a lecturer. While visiting Madisons mother she steals a braid of Madisons hair. Reading from a book on the supernatural given to her by Jody, Claire uses the hair to perform a ritual that allows Madison to possess her. Norman comes home and Claire seduces him, but Claire morphs into Madison during the tryst, causing Norman to push her away. Claires memory returns, and she remembers that she had caught Norman and Madison together the night of her car accident. Outraged by Normans infidelity, Claire spends the night with Jody, who confirms to Claire her suspicion of Normans affair after having seen him and Madison together in a small cafe in the town of Adamant; unseen by Norman, Jody keeps this information to herself, until she confesses to Claire.

Norman eventually relents: he had a brief relationship with Madison, but realized that he loved Claire too much to leave her and ended the affair, causing the unstable Madison to threaten to kill Claire. He states Madison came to the house, and he found her dead of an overdose.  Panicked and unsure of what else to do, he placed Madisons body into her car, which he then pushed into the lake.

Claire urges Norman to contact the authorities, and he agrees to do so. He speaks with the police on the phone, arranges for a police officer to come to the house and then leaves to take a shower. Suspicious of her husband, Claire redials the phone and discovers that he had not really called the police. Norman, who has been hiding and left the shower tap running, suddenly grabs her from behind and sedates her. He drags her to the bathtub, which is still running, and places her in the rising water.

Norman then confesses to Claire that his previous story had been a lie; Madison was planning on going to the colleges dean about their affair, so he killed Madison and pushed her car and body into the lake. Norman then tells Claire he hopes her death will bring him closer to Caitlin, who looks so much like her.  Norman leans over Claires paralyzed body to give her one final kiss. While doing so, he notices that she is wearing a pendant around her neck. When he picks up Claires head to adjust it, her face suddenly morphs into the corpse-like face of Madison. He is startled and jumps back, slipping on the wet floor and hitting his head on the sink; he is left bloodied and unconscious on the floor.

Claire, recovering from the sedative, looks over the edge of the bathtub to see that Norman has crawled away, while she was struggling to drain the water. Recovering from the sedative, she crawls out of the bathtub, eventually regaining enough strength to walk down the stairs. The phone has been disconnected, so she tries to drive somewhere that will have cell phone reception. Norman, only stunned at the bottom of the stairs, chases her and jumps into the back of the truck. A ghostlike figure appears in the road, which Claire swerves to avoid, and she drives off the bridge into the same lake into which Norman pushed Madisons car. Underwater, Norman tries to drown her and grabs Claires leg as she escapes out the window, but Madisons decaying body, disturbed by debris falling from the car, floats up, grabs Norman and forces him to release Claires leg, so she can swim to the surface. Norman drowns while staring into the dead face of the woman he killed. Madisons body floats to the surface of the lake as the necklace sinks to the bottom.

The following winter, Claire places a single red rose on Madisons grave. The camera pans out, and an image of Madisons face appears in the snow.

==Cast==
*Harrison Ford as Dr. Norman Spencer
*Michelle Pfeiffer as Claire Spencer
*Diana Scarwid as Jody
*Miranda Otto as Mary Feur
*James Remar as Warren Feur
*Katharine Towne as Caitlin Spencer Ray Baker as Dr. Stan Powell
*Joe Morton as Dr. Drayton
*Amber Valletta as Madison Elizabeth Frank
*Wendy Crewson as Elena

==Production==
Documentary filmmaker Sarah Kernochan had adapted a personal experience with the paranormal as a script treatment featuring of a retirement aged couple dealing with restless but compassionate spirits. DreamWorks commissioned a rewrite from actor-writer Clark Gregg. This script was later delivered in 1998 by Steven Spielberg himself to his director friend Robert Zemeckis,  who had signed a deal for DreamWorks to distribute the films of newly founded production company  ImageMovers, and announced interest in doing a thriller film.  Production Notes|publisher=20th Century Fox |year=2000}}  Harrison Ford then signed to star on the film, even agreeing to clear room in his schedule for the project.  Michelle Pfeiffer then followed as DreamWorks started to deal with 20th Century Fox regarding the films distribution.  Ford and Pfeiffer were Zemeckis first and only choices for the lead roles.  Fox agreed to distribute both What Lies Beneath and Zemeckis other project Cast Away,  with the thriller having Fox doing the domestic distribution and DreamWorks the international one. 

Zemeckis filmed What Lies Beneath while Cast Away was shut down to allow Tom Hanks to lose weight and grow a beard for his characters development.  As Gregg had to remain with production for rewrites, he had to decline Aaron Sorkins offer to read for a major role in Sports Night - though Sorkin would later write for Gregg a minor role in the final episodes of the series. 

==Reaction==
===Box office===
What Lies Beneath opened in 2,813 theaters in North America and grossed $29,702,959 for an average of $10,559 per theater. The film ended up earning $155,464,351 domestically and $135,956,000 internationally for a total of $291,420,351 worldwide, close to triple its production budget of $100 million 

===Critical response===
The film received mainly mixed reviews. The film currently holds a rating of 46% on Rotten Tomatoes based on 124 reviews with an average rating of 5.5 out of 10 with the sites consensus stating that "Robert Zemeckis is unable to salvage an uncompelling and unoriginal film."  The film received a score of 51 on Metacritic based on 35 reviews. 

The   thought that, "after a slow build that at times makes every hair stand on end Zemeckis rolls out every thriller cliché there is. A pity, because until then its a smart, realistically staged, adult-oriented and extraordinarily effective domestic chiller."  Empire (magazine)|Empire wrote: "The biggest surprise is, perhaps, that what emerges is no masterpiece, but a semi-sophisticated shocker, playfully homaging Hitchcock like a mechanical masterclass in doing ‘genre’. The first hour is great fun... It’s an enjoyably giddy ride, certainly, but once you’re back from the edge of your seat, you realise most of the creaks and groans are from the decomposing script." 

Michelle Pfeiffer received some positive notice for her performance. Roger Ebert called her "convincing and sympathetic." 

In his review, Ebert said that he felt the problem with Zemeckiss desire to direct a Hitchcockian film was to involve the supernatural (the film contains several musical, visual and plot references to Psycho (1960 film)|Psycho and Vertigo (film)|Vertigo, among other Hitchcock films), which he believes to be something Alfred Hitchcock himself would never have done. 

===Accolades===
Harrison Ford and Michelle Pfeiffer both won Blockbuster Entertainment Awards for Favorite Actor Suspense and Favorite Actress Suspense, while Diana Scarwid was nominated for Favorite Supporting Actress Suspense.  Awards |publisher=IMDB |accessdate=December 26, 2009 }} 
 Best Horror Best Director Best Actress (Michelle Pfeiffer), but lost to Final Destination, Bryan Singer for X-Men (film)|X-Men and Tea Leoni for The Family Man, respectively. 

==See also==
*List of ghost films

==References==
 

==External links==
 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 