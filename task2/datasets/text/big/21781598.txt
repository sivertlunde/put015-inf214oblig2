Kosovo: Can You Imagine?
{{Infobox film
| name           = Kosovo: Can You Imagine?
| image          = Kosovo Can You Imagine For Wikipedia.jpg
| alt            =  
| caption        = Official poster
| director       = Boris Malagurski
| producer       = Brian Mitchell
| writer         = Boris Malagurski
| screenplay     = 
| story          = 
| starring       = Lewis MacKenzie James Byron Bissett Michel Chossudovsky
| music          = Filip Borac Mihaela Cristina Istrate Kevin Macleod Boris Malagurski
| cinematography = 
| editing        = Boris Malagurski
| studio         = Malagurski Cinema
| distributor    = 
| released       =  
| runtime        = 30 minutes
| country        = Canada
| language       = English Serbian Albanian
| budget         = 
| gross          = 
}} documentary film Serbian Canadian Serb communities living in Kosovo at the time the documentary was filmed. Former Canadian general Lewis MacKenzie, former Canadian diplomat James Byron Bissett, former UNMIK officer John Hawthorne and economist Michel Chossudovsky appear in the film.

==Synopsis== expelled from their homes, kidnapped and killed, while a large number of their houses, cultural and religious sites were burned and destroyed. After that, economist Michel Chossudovsky discusses an alleged link between Kosovos military and political leadership and organized crime.

The film presents most of the Kosovo Serbs as internally displaced, some of them living in Kosovo Serb enclaves|enclaves, in small camps in Kosovo. The film follows the stories of several Serb victims.

==Awards and screenings==
*2009, Silver Palm Award (one of 14 films awarded in the Student Film category) for "Kosovo: Can You Imagine?" at the Mexico International Film Festival 2009, Playas de Rosarito, Baja California|Rosarito, Mexico.    

==See also==
*Anti-Serb sentiment

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 