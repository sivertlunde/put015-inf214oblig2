Topper Returns
{{Infobox film
 | name = Topper Returns
 | caption = DVD cover
 | director = Roy Del Ruth
 | producer = Hal Roach
 | writer = Jonathan Latimer Thorne Smith (novel)
 | starring = Joan Blondell Roland Young Carole Landis Billie Burke
 | music = Werner R. Heymann
 | cinematography = Norbert Brodine
 | editing = James Newcom
 | studio = Hal Roach Studios
 | distributor = United Artists
 | released =  
 | runtime = 88 minutes
 | country = United States
 | language = English
 | budget = 
 | gross = 
 | image = Topper Returns VideoCover.jpeg
}}

Topper Returns (1941) is the third and final entry in the initial series of films inspired by the novels of Thorne Smith. It followed Topper (film)|Topper (1937) and Topper Takes a Trip (1938). As in the prior movies, Roland Young and Billie Burke play the Toppers, while Joan Blondell portrays a murder victim and ghost who tries to save her friend, played by Carole Landis, and unmask her killer with the help of a reluctant Cosmo Topper.
 Academy Award Best Sound, Recording (Elmer Raguse).   
 pilot called Topper Returns (1973 in film|1973)  was later made for a proposed TV series. There was also a TV Movie|made-for-TV remake of the original film, Topper in (1979 in film|1979). 
 public domain (in the USA) due to the claimants failure to renew its copyright registration in the 28th year after publication. 

==Cast==
* Joan Blondell as Gail Richards
* Roland Young as Cosmo Topper
* Carole Landis as Ann Carrington
* Billie Burke as Clara Topper
* Dennis OKeefe as Bob
* Patsy Kelly as Emily
* H. B. Warner as Henry Carrington Eddie Anderson as Eddie
* George Zucco as Dr. Jeris
* Donald McBride as Police Sergeant Roberts
* Rafaela Ottiano as Lillian
* Trevor Bardette as Rama

==See also==
*List of ghost films

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 