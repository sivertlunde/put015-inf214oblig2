Step Up: All In
{{Infobox film
| name           = Step Up: All In
| image 	 = Step Up All In poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Trish Sie 
| producer       = Jennifer Gibgot Adam Shankman
| writer         = John Swetnam
| based on       =   Adam Sevani Christopher Scott Luis Rosado
| music          = Jeff Cardoni
| cinematography = Brian Pearson
| editing        = Niven Howie
| studio         = Summit Entertainment Offspring Entertainment
| distributor    = Lions Gate Entertainment
| released       =  
| runtime        = 112 minutes 
| country        = United States
| language       = English
| budget         = $45 million 
| gross          = $86.2 million   
}} 3D dance film directed by Trish Sie and the fifth and final installment in the Step Up (film series)|Step Up film series. The film was released on August 8, 2014.

==Plot==
  the previous film are in Los Angeles trying to make a living from dancing, but are turned down at every audition. After being refused at another audition, the Mob visit a club where they are noticed and challenged to a dance battle by another crew, the Grim Knights. The Grim Knights win the battle and the Mob decides to pack up and leave Los Angeles and return to Miami, thinking there is nothing left for them and that they arent ready for Los Angeles.

Sean decides to stay and while there, he notices a dance competition called The Vortex taking place, inspiring him to put together a new crew with help from Moose (Adam Sevani). Moose gets Sean a job working as a janitor in a dance center owned by Mooses grandparents, where Sean takes up residence in a janitors closet. Sean and Moose recruit Andy West (Briana Evigan) and later, Vladd (Chadd Smith), Violet (Paris Goebel), the Santiago Twins (Facundo and Martin Lombard), Monster (Luis Rosado), Hair (Christopher Scott), Gauge (Cyrus "Glitch" Spencer), Jenny Kido (Mari Koda), and Chad (David "Kid David" Shreibman) to the crew. The group soon makes an audition video as the LMNTRIX and are accepted into the competition a few weeks later.

The crew heads to Las Vegas to compete. Upon arriving, Sean finds out that both the Grim Knights and the Mob are also in the competition, motivating the LMNTRIX to practice extra hard. While the rest of the crew are at a bar, Sean and Andie reveal they have broken up with their respective partners. Moose kisses another girl while freestyle dancing at the bar, which his girlfriend Camille (Alyson Stoner) witnesses. She runs off, prompting Moose to leave the crew and return to Los Angeles to make up with her. The LMNTRIX battle the Mob in a Vortex exhibition match; during the battle, Sean tries to force Andie to perform a trick they tried during one of their practices but Andie refuses and leaves. The LMNTRIX, however, still win the battle and the Mob leave, angry at Sean. Sean finds Andie outside, where she confronts him about his actions. Sean realize that he has made a lot of mistakes; he apologizes to the Mob and later makes up with Andie and the LMNTRIX.

Moose goes home and finds Camille on the patio, where she reveals that she wasnt actually upset at him, but was jealous when she saw his dancing and realized that she hasnt committed herself to it, despite being a talented dancer; they later make up. Chad and Kido overhear Alexxa Brava (Izabella Miko), the host of The Vortex, and Jasper (Steven "Stevo" Jones), the leader of the Grim Knights making out, realizing that Alexxa is rigging the competition. Once the whole crew finds out, they come up with a plan to teach Alexxa and the Grim Knights a lesson.

Moose returns and rejoins the crew (bringing Camille along), and the Mob join forces with the LMNTRIX for the competition. When the finals of The Vortex approach (the Grim Knights vs. the LMNTRIX), the Grim Knights give a great performance, which the host remarks will be hard to beat. Before LMNTRIX perform, Sean takes the stage and discusses with the crowd that his experiences have taught him what really matters. He then persuades the crowd to forget about winning or losing and just enjoy the show. The rest of the crew then take the stage and give an amazing performance. Andie and Sean decide to end the dance by performing the trick Sean wanted her to do earlier in the film, which they successfully complete, followed by a passionate kiss between the two, leaving the crowd amazed.

The producers call Alexxa and inform her that the LMNTRIX won and that they will get the three-year contract for their own show. The film ends with the LMNTRIX and the Mob happily celebrating their excellent performance and victory.

==Cast==
* Ryan Guzman as Sean Asa
* Briana Evigan as Andie West 
* Misha Gabriel as Eddy
* Adam Sevani as Robert "Moose" Alexander III 
* Alyson Stoner as Camille Gage
* Izabella Miko as Alexxa Brava
* Mari Koda as Jenny Kido
* Martín Lombard as Martin Santiago and Facundo Lombard as Marcos Santiago (known as The Santiago Twins). Christopher Scott as Hair
* Stephen "tWitch" Boss as Jason Hardlerson
* Luis Rosado as Monster
* Chadd Smith as Vladd
* Parris Goebel as Violet
* Stephen "Stevo" Jones as Jasper Tarik
* David "Kid David" Shreibman as Chad
* Celestina Aladekoba as Celestina
* Freddy HS as Accounting Manager
* Cyrus "Glitch" Spencer as Gauge

==Soundtrack==
* I Wont Let You Down - OK GO
* Judgement Day - Method Man
* Lapdance - N*E*R*D
* How You Do That - B.o.B DDP
* Celestina
* Demons - Zeds Dead
* Rage the Night Away - Waka Flocka Flame
* My Homies Still - Lil Wayne ft. Big Sean
* Revolution - Diplo ft. Faustix Celestina

==Home media==
The film was released on DVD and Blu-ray Disc|Blu-ray on November 4, 2014.

==Reception==

===Critical response===
The film has been met with mixed reviews. On  , the film has a score of 45 out of 100 based on 17 critics, indicating "mixed or average reviews". 

===Box office===
The film debuted at #6 in the North American box office, earning $6.5 million. The film grossed $14,904,384 in America and $71,261,262 overseas for a worldwide total of $86,165,646, making the lowest grossing film in the series. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 