One Summer of Happiness
{{Infobox film
| name           = One Summer of Happiness  
| image          = Hon dansade en sommar poster.jpg
| image size     = 
| caption        = 
| director       = Arne Mattsson
| producer       = Lennart Landheim
| screenplay         = Volodja Semitjov Olle Hellbom
| based on =  
| starring       =Ulla Jacobsson Folke Sundquist Edvin Adolphson John Elfström
| music          = Sven Sköld
| cinematography = Göran Strindberg
| editing        = Lennart Wallén
| studio = Nordisk Tonefilm
| distributor    = Nordisk Tonefilm
| released       =  
| runtime        = 103 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| preceded by    = 
| followed by    = 
}}
 1951 cinema Swedish film by director Arne Mattsson, based on the 1949 novel Sommardansen (The Summer Dance) by Per Olof Ekström. It was the first Swedish film to win the Golden Bear at the Berlin International Film Festival. It was also nominated for the Palme dOr at the 1952 Cannes Film Festival. Today, the film is mainly known for its nude scenes, which caused much controversy at the time and, together with  Ingmar Bergmans Summer with Monika (1953), spread the image of Swedish "free love" around the world.

==Plot==
The film tells the story about the university student Göran who spends a summer on his uncles farm, where he meets the young Kerstin. They instantly fall in love, but Kerstin is ruled by very strict relatives, so they must hide their love story from everyone, not the least from the extremely strict vicar. They experience an intensive summer together, and Göran dreads the idea of returning to university in the autumn. But a motorcycle accident puts an end to it all, with Kerstin dying in Görans arms.

==Main cast==
* Ulla Jacobsson as Kerstin
* Folke Sundquist as Göran
* Edvin Adolphson as Anders Persson
* Irma Christenson as Sigrid
* John Elfström as The Vicar
* Nils Hallberg as Nisse
* Gunvor Pontén as Sylvia
* Berta Hall as Anna

==Reception==
The film caused much international controversy, because of a nude swimming sequence and a love scene which included a close-up of Ulla Jacobssons breasts, but also because of its very anti-clerical message by portraying a local priest as the main villain. So, in spite of its awards, the film was banned in several countries, among them Spain,  and it wasnt released in the United States until 1955. 

==Awards==
;Won
* 2nd Berlin International Film Festival - Golden Bear   
* 1952 Cannes Film Festival - Best Music.   

;Nominated
* 1952 Cannes Film Festival - Palme dOr  

==See also==
*Nudity in film

==References==
 

==External links==
* 
* 
*  

 

 
 
 
 
 
 
 
 
 