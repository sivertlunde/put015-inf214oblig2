Bhangra (film)
{{Infobox film name = Bhangra ਭੰਗੜਾ image =  image size =  border =  alt =  caption =  director = Jugal Kishore producer = Mulk Raj Bhakhri writer =  screenplay =  story =  based on =  narrator =  starring = Sunder Nishi Nishi Manju music = Hansraj Behl cinematography = Raj Kumar Bhakhri editing = studio = distributor = released =   runtime = 120 minutes country = India language = Punjabi
|budget = gross =
}}
 Punjabi romance, comedy and drama film directed by Jugal Kishore,       starring Sunder, Nishi and Manju in lead roles. 

== Synopsis ==

Bhangra is a drama with nice comedy and romance. Sunder, the son of the moneylender Kaude Shah, goes to a poor farmer, Bulaki, in the village of Rangpur to get money-with-interest back from him, but falls in love with his daughter Banto. A suspended Munshi (English: Cashier/Assistant of a money-lender) of Kaude Shah, Mehnga Mall, also tries to get Banto; he steals Kaude Shahs jewellery and reaches Rangpur. He gives the jewellery to Sunder for staying away from him and Banto. Sunder gives the jewellery to Bulaki to pay back his debt and so did the unconscious Bulaki. The truth comes out and Bulaki is charged for stealing jewellery. He explains that the jewellery was given to him by Sunder and so Sunder is arrested. Finally, Mehnga Mall confesses and Bulaki and Sunder go free. After a little protest Sunders father, Kaude Shah, agrees to Sunder and Bantos marriage.

== Music ==

Noted music director Hansraj Behl composed the music    and Varma Malik wrote the lyrics for the film. Mohammad Rafi and Shamshad Begum are the lead playback singers. The music was a hit.

;Tracklist

*Rabb Na Kare
*Batti Bal Ke
*Chitte Dand Hasno Nahio Rehnde
*Jatt Kurhian Ton Darda Maara (Bolian)
*Ambiaan De Booteaan Te
*Been Na Wajaeen Mundia
*Mull Wikda Sajjan Mil Jaave

== Cast and crew ==

{|class="wikitable"
|-
!Actor/Actress
!Role
|- Sunder (actor)|Sunder|| Sunder
|- Nishi (actress)|Nishi|| Banto
|- Manju (actress)|Manju|| 
|}

;Crew

* Jugal Kishore ..Director
* Mulk Raj Bhakri ..Producer
* Hansraj Behl ..Music director
* Varma Malik ..Lyricist
* Mohammad Rafi ..Playback
* Shamshad Begum ..Playback
* Raj Kumar Bhakri ..Cinematographer
* Purushottam V. Naik ..Art director

== References ==
 

 
 
 