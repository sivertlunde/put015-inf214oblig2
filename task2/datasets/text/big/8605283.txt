Itlu Sravani Subramanyam
{{Infobox film
| name           = Itlu Sravani Subramanyam
| image          =
| imdb_id        =
| writer         = Puri Jagannadh  (story)  Puri Jagannadh  (screenplay)  Puri Jagannadh  (Dialogues) 
| starring       = Ravi Teja Tanu Roy Samrin
| director       = Puri Jagannadh
| producer       = K. Venugopal Reddy
| distributor    =
| released       = 14 September 2001
| runtime        =
| country        = India
| cinematography = K.Dutt
| editing        = Martand K. Venkatesh Telugu
| Chakri
| awards         =
| budget         =
}}
 Tollywood film written and directed by Puri Jagannadh. This film stars Ravi Teja, Tanu Roy, and Samrin in the main roles.Dubbed in hindi as Yes Or No.  

==Cast==
*Ravi Teja
*Tanu Roy
*Samrin 
*Ananth		 Annapoorna		
*Tanikella Bharani		
*Chinna		 Jeeva		
*Raghu Kunche		
*L.B. Sriram		
*M.S. Narayana		
*Prasanna		
*Kalpana Rai		
*Dharmavarapu Subramanyam		
*Uttej

==Plot==

The movie starts with Sravani (Tanu Roy) and Subramanyam (Ravi Teja), strangers to each other, meeting at suicide point of Vizag seashore. They realize that their goal is the same. Suicide, that is! They describe to each other their reasons to end their lives and duly write suicide notes. Sravanis reason is her nagging relatives who are also guardians of her. They are after Sravanis ancestral money. A friend who took a lump some of money by offering to get a job in Dubai cheated Subramanyam. Sravani and Subramanyam fulfil their last wishes with the help of each other. They consume ample amount of sleeping pills in a bid to end their lives in the room of Subramanyam. The house owner rescues them. Subramanyam lands up with a plum job later as the relatives of Sravani take her back to the home. Subramanyams marriage gets fixed with a girl of his mothers choice and Sravanis marriage with her maternal uncle. Sravani and Subramanyam flee from their respective marriage halls independently. The rest of the film is about how the lovers unite. In this film, Samrin acts as the bosss daughter loving Ravi Teja. She is the glamor doll of the film and turned out to be a big relief for this otherwise serious love subject. She added another angle to the love to make this film triangular. Music for this film was by Chakri. K. Dutt worked as the cinematographer for this film.

Some bits of this movie are seen in Hindi movie, Anjaana Anjaani directed by Siddharth Anand starring Priyanka Chopra and Ranbir Kapoor & the Hollywood movie, A Long Way Down, starring Pierce Brosnan and Toni Collette.which is not good at all.

== References ==
 

==External links==
*  

 

 
 
 


 