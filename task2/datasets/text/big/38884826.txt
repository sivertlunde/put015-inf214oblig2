Captain Thunder (film)
{{Infobox film name = Captain Thunder image = Captain_Thunder_1930_Title_Card.jpg producer = director = Alan Crosland writer = Gordon Rigby William K. Wells based on the story by Pierre Couderc Hal Davitt starring = Robert Elliott Bert Roach Natalie Moorhead music = David Mendoza Leon Rosebrook cinematography = James Van Trees editing = Arthur Hilton distributor = Warner Bros. released =   runtime = 65 minutes language = English
|country = United States
|}}
 costume drama film which was produced and distributed by Warner Bros. and released late in 1930. The film was directed by Alan Crosland and stars Victor Varconi in his first full-length all-talking feature. The film was based on a story The Gay Caballero which was written by Pierre Couderc and Hal Davitt. 

A copy is preserved at the Library of Congress. 

==Synopsis==
Victor Varconi is a Mexican bandit who brazenly flaunts his ventures until the people of El Paramo demand that something be done about it. Charles Judles, the Mexican sheriff, offers a large reward for the capture of Varconi. Meanwhile, a pair of lovers, played by Fay Wray and Don Alvarado are disappointed when Wrays father Alvarado request to marry his daughter. Wrays father has his eye on a wealthy gringo, played by Robert Elliott, and wants his daughter to marry him. When Alvarado hears about the large reward for Varconi he sets out for the bandit in the hopes of getting the money so that he can be wealthy enough to please her father and marry Wray. Judles decides to light bonfires at points where the bandit is seen in the hopes of capturing him. When Varconi shows up at Wray hacienda to pay homage to her, she, at first, lights a bonfire. She subsequently changes her mind and hides Varconi from the soldiers when they arrive.  At daybreak, Alvarado captures Varconi, in the hopes of getting the reward money. He turns him in and gets the reward money and prepares for the wedding. Varconi soon escapes from prison, to the embarrassment of Judles. At this point Elliot asks Varconi to perform a favor which he had promised him on a previous occasion. Elliot tells Varconi to break up the wedding so that he can marry Wray himself. Immediately after Wray is married by force she retires to her room. A gunshot is then heard. Wray returns to the party where Varconi informs her that she is now a widow and is free to marry Alvarado.

==Cast==
*Victor Varconi — El Capitan Thunder 
*Fay Wray — Ynez
*Charles Judels — Commandante Ruiz Robert Elliott — Morgan 
*Bert Roach — Pablo 
*Natalie Moorhead — Bonita 
*Frank Campeau — Hank 
*Don Alvarado — Juan
*John St. Polis — Pedro 
*Robert Emmett Keane — Don Miguel

==Preservation==
The film survives complete and has been shown on television and cable.

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 