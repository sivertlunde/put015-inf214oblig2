Gun Belt (film)
{{Infobox film
| name           = Gun Belt
| image size     = 
| image	=	
| caption        = 
| director       = Ray Nazarro
| producer       = Edward Small
| writer         = 
| based on       =  George Montgomery Tab Hunter
| music          = 
| cinematography = 
| editing        = 
| distributor    = United Artists
| studio         = Global Productions
| released       = July 24, 1953
| runtime        = 
| country        = USA
| language       = English
| budget         = 
}} Western drama film.The movie was directed by Ray Nazarro. The screenplay was written by Jack DeWitt and Arthur E. Orloff created the story. It stars George Montgomery, Tab Hunter and Helen Westcott.  

==Plot==
A former outlaw clashes with his old gang.   Ringo decides to hang up his guns, buy a ranch and wed Arlene Reach (Helen Westcott) . Chip’s father, Matt (John Dehner), is serving a prison sentence and Billy is determined to keep him off the outlaw trail. Ringo is trying to keep on the straight and narrow, with three other outlaws, Dixon, Hollaway and Hoke, frame Ringo into pulling a bank robbery with them. Pretending to side with them, after accidentally killing Matt, Ringo informs Marshal Wyatt Earp of their plan to rob a Wells Fargo express wagon. A gunfight ensues at the robbery and the three outlaws are killed and Ike Clinton, the ringleader, is turned over to Marshal Earp by Ringo. 

==Cast==
*George Montgomery as Billy Ringo
*Tab Hunter as Chip Ringo
*Helen Westcott as Arlene Reach
*John Dehner as Matt Ringo
*William Bishop as Ike Clinton
*Jack Elam as Kolloway
*Douglas Kennedy as Dixon
*James Millican as Wyatt Earp
*Hugh Sanders as Douglas Frazer
*Bruce Cowling as Virgil Earp

==Production==
The movie was originally known as Johnny Ringo and Screaming Eagles. Melodramas Scheduled
Los Angeles Times (1923-Current File)   13 May 1953: 19.   Filming started 13 November 1952 at the Goldwyn studios. Nelson Sidesteps Dance; Life of Casanova Set; Taps Signs TV Warbler
Scheuer, Philip K. Los Angeles Times (1923-Current File)   13 Nov 1952: B11.  

==References==
 
==External links==
*  at TCMDB
*  at IMDB
 
 
 
 
 

 