Kill Katie Malone
{{Infobox film
|  name     = Kill Katie Malone

|  image          = Kill_Katie_Malone_Movie_Poster.jpg
|  caption        = Kill Katie Malone (2010)
|  director       = Carlos Ramos Jr.
|  producer       = Melanie Wagor Simon Johnson
|  cinematography = Aaron Moorhead
|  editing        = Jeffrey Reid
|  writer         = Mark Onspaugh
|  starring       = Dean Cain Stephen Colletti Jonathan Lil J McDaniel Fernanda Romero Nick Hogan Masiela Lusha
|  studio         = Artifact 2613 Illuminary Pictures Scatena & Rosner Films Showtime
|  released       =   
|  runtime        = 91 minutes
|  country        = United States
|  language       = English
|  gross          = 
|  budget         = $1,000,000   
}}
 2010 American horror film directed by Carlos Ramos Jr. and starring Masiela Lusha from The George Lopez Show and Dean Cain. The screenplay was written by Mark Onspaugh.

== Plot ==
In the opener, Robert, who owns the box holding Katie Malones spirit, discovers the spirit threatening the life of his spouse. When he goes to save her, he is violently thrown from the room. Cut to some time later, Jim Duncan at the fictional college of Mission University notices the box for sale on uBid and is intrigued by it. He wants to buy it, but he doesnt have enough money and finagles the extra money he needs from his best friends Ginger Matheson and Kyle "Dixie" Canning, which makes them part owners of the box. However, now that Robert no longer owns the box, Katie is free to threaten him one last time at the bus station before vanishing from his life.

When Jim receives the box and opens it, a strange sound comes from it. Jim considers it a good luck charm and asks Ginger and Dixie to make wishes. Half seriously, Ginger asks for a red rose, but Dixie just wants his money back. When she gets her rose, Ginger accuses Jim and Dixie of trying to scare her. However, when a teacher accuses Dixie of plagiarism, he makes an off-the-cuff "wish" that she would leave him alone. That night, the teacher experiences a paranormal presence that forces her to bite off her own tongue. In class the next day, Jim and Ginger make amends  as Ginger discovers her roommate Misty has been borrowing her clothes without permission. After they learn about the teachers horrific death, which has been ruled a murder, Dixie and Ginger wonder if Katie was responsible. In art class, Misty snags Gingers sweater on a table and loses her arm to a paper slider.

Dixie and Ginger are convinced the box is responsible for the murders, but Jim is not sure. After a poltergeist attack in their dorm room, Jim discovers writing on the wall in a strange language. He tries to contact the previous owner of the box but is reminded of his commitment to help Ellen set up her sororitys haunted house. Tyler, Ellens boyfriend, does not appreciate his efforts and threatens him. As Tyler heads home, he encounters a ghostly girl in period dress that he tries to help. When he refuses to leave her alone, her banshee scream virtually disintegrates him. Jim contacts Amy, who asks him if he opened the box and released Katie. When Ellen asks Jim if he has seen Tyler, Jim tries to endear himself to her; she backs off and announces they can only be friends, which breaks Jims heart. While jogging, Ellen is attacked by a spirit and ripped apart under a bridge.
 Gaelic as "a family or death". After researching Katie Malone, he learns she was an Irish immigrant who was forced into slavery and beloved by the daughter of her last owner. After she fails to protect Katie from her father, the childs ghost now protects Katie into the afterlife. Meeting with Ginger and Dixie, he opts to lay Katie to rest by freeing her from the box. Jim and Ginger soon have a date that wraps at her room at the student housing structure. The daughters wrathful spirit terrifies Jim, and Ginger mistakenly believes Jim is not fond of her. She throws him out of the apartment.

Back at his room, Jim is contacted by Amy, who learns Jim released Katies spirit. She realizes the daughters vengeful spirit will now kill him and his friends unless he sells the box, which has returned to his room, back to her. At the campus Halloween party, Ginger recognizes the daughters ghost, who attacks the party and throws their classmates around the hall. Dixie and Ginger escape through the basement, but Dixie is abducted and killed. Ginger flees into the hall, and Jim drives the daughters ghost into the box. In the tag scene, Amy now has the box and swears to never open it, but Robert angrily attempts to wrestle it from her; it falls, hits the floor, and opens on impact. A strange sound fills the room, and Amys bedroom door slams shut as father and daughter fear the worst.

== Cast ==
* Masiela Lusha as Ginger
* Stephen Colletti as Jim 
* Jonathan Lil J McDaniel as Kyle "Dixie" Canning 
* Dean Cain as Robert 
* Nick Hogan as Tyler 
* Cassandra Jean as Ellen 
* Fernanda Romero as Misty 
* Sylvia Panacione as Katie Malone 
* Katy Townsend as Amy

== Production == San Gabriel, California. Carlos Ramos Jr. told Fangoria: "We shot all over LA, an old prison in Whittier, an awesome high-school in the Mission District and a graveyard for priests, which was super creepy." 

== Reception ==
Critical response to Kill Katie Malone (2010) has generally been negative. Scott Foy of Dread Central gives it  two out of five stars and called it "one of those not very good, not very bad horror movies thats really only good for a few (mostly unintentional) laughs."   Paul Pritchard of DVD Verdict wrote that the film "offers little to justify its existence."   Rich Rosell of DVD Talk rated it two out of five stars and wrote that the films fails to live up to the promise of "a predictably comfortable dumb-fun horror". 

== References ==
 

== External links ==
* 

 
 
 
 
 
 
 
 
 