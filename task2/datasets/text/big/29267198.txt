Mr. Satan (film)
{{Infobox film
| name           = Mr. Satan
| image          =
| image_size     =
| caption        =
| director       = Arthur B. Woods
| producer       = William Collier
| writer         = John Meehan Jr. J. O. C. Orton
| starring       = James Stephenson Chili Bouchier
| music          =
| cinematography = Robert La Presle
| editing        = Terence Fisher Warner Brothers-First First National Productions
| released       = February 1938
| runtime        = 79 minutes
| country        = United Kingdom
| language       = English
}} 1938 British spy thriller, directed by Arthur B. Woods and starring James Stephenson and Chili Bouchier.  Unlike a majority of Woods quota quickie productions of the 1930s which are believed Lost film|lost, this film survives in the British Film Institute National Archive.

==Plot==
News correspondent Tim Garnett (Stephenson) and his photographer Connelly (Richard "Skeets" Gallagher) have been on assignment in a country on the edge of revolution.  At the airport as they wait to leave, Connelly snaps a throwaway shot of an attractive woman boarding a plane.  When the photograph is developed, they realise that the womans male companion is Emile Zubova (Franklin Dyall), a notorious illegal arms dealer who had recently been reported as having committed suicide while on the run from agents wishing to track him down.

Connelly and Garnett start to investigate their scoop, but as they do so word gets back to Zubova that he has been photographed.  Connelly and Garnett manage to identify the woman in the picture, Jacqueline Manet (Bouchier), and trace her movements to France.  As they fly out, Zubova arranges to have their plane shot down in order to silence them.  The plane crashes, but both survive relatively unscathed.  Garnett locates Jacqueline and follows her, hoping she will lead him to Zubova.  She realises she is being tailed and challenges Garnett.  However the confrontation soon turns into mutual attraction and then love.

Having been informed of the dangerous situation by his lookout minions, Zubova has Garnett kidnapped and brought to his hideout, where plans are being made to torpedo an ocean liner in order to provoke another war from which Zubova can profit.  Jacqueline arrives at the hideout and shoots Zubova during a struggle, but is herself fatally wounded in return and dies in Garnetts arms.  Garnett is able to alert the authorities of the plot to sink the liner, the navy are put on hand to deal with the threat, and the submarine is destroyed.

==Cast==
* James Stephenson as Tim Garnett
* Chili Bouchier as Jacqueline Manet
* Richard "Skeets" Gallagher as Connelly
* Franklin Dyall as Emile Zubova
* Betty Lynne as Conchita
* Mary Cole as Billie
* Robert Rendel as Seymour
* Patricia Medina as Maria

== External links ==
*  
*   at BFI Film & TV Database

 

 
 
 
 
 
 
 