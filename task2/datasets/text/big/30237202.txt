Charlie Chan in Reno
{{Infobox film
| name           = Charlie Chan in Reno
| image          =
| caption        = Norman Foster John Stone
| based on       = "Death Makes a Decree" by Philip Wylie
| screenplay     = Frances Hyland Robert E. Kent Albert Ray
| starring       = Sidney Toler
| music          = Samuel Kaylin
| cinematography = Virgil Miller Fred Allen
| studio         = 20th Century Fox
| distributor    = 20th Century Fox
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
}} Norman Foster, starring Sidney Toler as the fictional Chinese-American detective Charlie Chan, based on an original story "Death Makes a Decree" by Philip Wylie.

==Plot==
Mary Whitman has arrived in Reno to obtain a divorce. While there, she is arrested on suspicion of murdering a fellow guest at her hotel (which specializes in divorcers). 

There are many others at the hotel who wanted the victim out of the way. Charlie Chan travels from his home in Honolulu to Reno to solve the murder at the request of Marys soon-to-be ex-husband.

==Cast==
* Sidney Toler as Charlie Chan
* Victor Sen Yung as Jimmy Chan
* Ricardo Cortez as Dr. Ainsley
* Phyllis Brooks as Vivian Wells
* Slim Summerville as Sheriff Tombstone Fletcher
* Kane Richmond as Curtis Whitman
* Pauline Moore as Mary Whitman
* Iris Wong as Choy Wong Eddie Collins as Cab Driver

==External links==
*  
* 
* 

 

 
 
 
 
 
 
 
 
 

 