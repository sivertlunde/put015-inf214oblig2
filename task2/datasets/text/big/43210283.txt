Blazing Days
{{infobox film
| title          = Blazing Days
| image          =
| imagesize      =
| caption        =
| director       = William Wyler
| producer       = Universal Pictures
| writer         = Florence Ryerson(story) Robert F. Hill George H. Plympton
| starring       = Fred Hume
| music          =
| cinematography = Alan Jones
| editing        =
| distributor    = Universal Pictures
| released       = March 27, 1927
| runtime        = 5 reels
| country        = USA
| language       = Silent film..(English intertitles)
}}
Blazing Days is a 1927 silent film western directed by William Wyler and produced and released by Universal Pictures.  

A print is preserved at the Library of Congress and UCLA Film/TV.  

==Cast==
*Fred Hume - Smilin Sam Perry
*Ena Gregory - Milly Morgan
*Churchill Ross - Jim Morgan Bruce Gordon - "Dude" Dutton
*Eva Thatcher - Ma Bascomb
*Bernard Siegel - Ezra Skinner
*Dick LEstrange - "Turtle-Neck-Pete"

==References==
 

==External links==
* 
* 

 
 
 
 
 
 


 
 