Almost Human (2013 film)
{{Infobox film
| name           = Almost Human
| image          = File:AlmostHuman2013Begos poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Joe Begos
| producer       = 
| writer         = Joe Begos
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Graham Skipper, Josh Ethier, Vanessa Leigh
| music          = Andy Garfield
| cinematography = Joe Begos
| editing        = Josh Ethier
| studio         = Channel 83 Films, Ambrosino / Delmenico
| distributor    = IFC Midnight  
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Almost Human is a 2013 science fiction horror film directed by Joe Begos. His feature film directorial debut,  it premiered on September 10, 2013 at the Toronto International Film Festival and stars Graham Skipper as a man whose best friend may or may not be committing a series of horrific murders.  

==Plot synopsis==
Two years ago Mark (Josh Ethier) was kidnapped and taken to parts unknown. Upon his return Marks personality is completely changed. His friends Seth (Graham Skipper) and Jen (Vanessa Leigh) decide to investigate Marks mysterious disappearance and re-appearance, but are soon met with a series of grisly murders.

==Cast==
*Graham Skipper as Seth Hampton
*Josh Ethier as Mark Fisher
*Vanessa Leigh as Jen Craven
*Susan T. Travers as Becky
*Anthony Amaral III as Clyde Dutton
*Michael A. LoCicero as Barry
*Jeremy Furtado as Gas Station Customer
*Jami Tennille as Tracy
*Chuck Doherty as Clancy
*Kristopher Avedisian as Hunter 1
*David Langill as Hunter 2
*John Palmer as Jimmy
*Andre Boudreau as Car Driver
*Eric Berghman as Earle Harris Mark OLeary as Dale

==Reception==
Critical reception for Almost Human has been mixed.  The Torontoist rated the film at 3 1/2 stars out of 5.   Much of the criticism centered around issues that Fearnet|Fearnets Scott Weinberg considered "first-timer problems",    such as the movies acting, repetition, and slowness in the second act.     However as a whole the reviewers generally enjoyed the film and believed that Begos did show future potential.  In contrast, ReelFilm was more critical of the film and stated that while it had a "watchable vibe", the movie "squandered the promise of its setup to become just another generic, shot-on-a-shoestring horror effort." 

==References==
 

==External links==
*  

 
 
 
 
 