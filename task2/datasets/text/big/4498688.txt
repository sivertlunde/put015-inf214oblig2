The Aftermath (1982 film)
{{Infobox Film
| name           = The Aftermath
| image_size     = 
| image	=	The Aftermath FilmPoster.jpeg
| caption        = 
| director       = Steve Barkett
| producer       = Steve Barkett
| writer         = Steve Barkett
| starring       = Steve Barkett Lynne Margulies Sid Haig
| music          = 
| cinematography = 
| editing        = Thomas F. Denove Dennis Skotak
| distributor    = 
| released       = 1982
| runtime        = 95 min.
| country        = United States English
| budget         = 
}}
 science fiction, action independent film. 

==Plot==
Three astronauts return to Earth after a nuclear holocaust, although one dies in a crash landing. The two survivors, Newman (Steve Barkett) and Matthews, encounter some mutants before discovering that Los Angeles has been completely destroyed. Seeking shelter, the men take refuge in an abandoned mansion. Newman later encounters a young boy, Chris, hiding in a museum with the curator (Forrest J Ackerman). When the curator passes away, Newman takes Chris under his care. While out seeking supplies one day, Newman and Chris encounter Sarah, who is running from a gang of outlaw bikers led by Cutter (Sid Haig). These survivors decide to fight this gang in this post-apocalypse wasteland.

==Cast==
*Steve Barkett as Newman 
*Lynne Margulies as Sarah 
*Sid Haig as Cutter 
*Christopher Barkett as Chris
*Alfie Martin as Getman 
*Forrest J Ackerman as Museum curator 
*Jim Danforth as Astronaut Williams 
*Linda Stiegler as Helen
*Adrian Torino as Bruce Will
*Levi Shanstan as Arnold Swatch / The Mummy
*Ramon Sumabal as Brad Pet
*Michael Eugene Romero as Silver-ster the Gold
*Jerone Meehleib as The lover Boy Irbo

==Release==
The film was released in the United States on various VHS labels and in a special edition laserdisc release by the Roan Group. It was released in the UK with the alternate title Zombie Aftermath.

==See also==
*List of zombie short films and undead-related projects

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 