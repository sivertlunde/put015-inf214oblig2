Devil in the Flesh (1986 film)
{{Infobox film
| name           = Devil in the Flesh 
| image          = Devil in the flesh poster.jpg
| image_size     = 
| caption        = English-language theatrical poster
| director       = Marco Bellocchio
| producer       = Leo Pescarolo
| screenplay        = Marco Bellocchio Ennio De Concini Enrico Palandri
| based on    =   Raymond Radiguet
| narrator       = 
| starring       = Maruschka Detmers Federico Pitzalis
| music          = Carlo Crivelli
| cinematography = Giuseppe Lanci
| editing        = Mirco Garrone
| distributor    = Bac Films
| released       =  
| runtime        = 114 min
| country        = Italy
| language       = Italian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
  Italian film released in 1986 and directed by Marco Bellocchio. 
 Le Diable au corps, the film stars Federico Pitzalis as a high school student who falls in love with an older woman (played by Maruschka Detmers).

==Storyline==
An Italian high school student named Andrea becomes infatuated with Giulia, an older woman he sees outside his classroom window. Giulias fiance, a leftist radical, has been jailed and is currently on trial for political crimes. Andrea and Giulia meet, and begin rendezvousing at her apartment, where a sexual relationship quickly unfolds. The situation is complicated when the fiances mother realizes this, and confronts Andreas father. Andreas father is a psychiatrist who treats patients with psychoanalysis. He has treated Giulia in the past. The psychiatrist finds himself a victim of giulias sexual advances, which he attributes to madness. As a passionate affair unfolds between Giulia and Andrea, the viewer is left wondering exactly how crazy Giulia really is, and whether Giulia had an affair with the boys father as well. Intensity of passion between Giulia and Andrea builds towards the climax. Andreas father confronts him about this affair, but andrea doesnt back down. Giulias husband is released from the jail. And the Last ten minutes of this movie do a poetic justice to all the confusion and passion built up craftily in the movie.

==Onscreen sexuality==
One of the more notable aspects of the film, is an extended (though darkly lit) scene in which the character played by Maruschka Detmers performs fellatio on the character played by Federico Pitzalis. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 