Boy (2010 film)
 
 
{{Infobox film
| name           = Boy
| image          = Boy2010movieposter.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Taika Waititi
| producer       = Cliff Curtis Ainsley Gardener Emanuel Michael
| writer         = Taika Waititi
| starring       = James Rolleston Taika Waititi Cohen Holloway Pana Hema Taylor
| music          = The Phoenix Foundation
| cinematography = Adam Clark
| editing        = Chris Plummer
| studio         = Whenua Films Unison Films New Zealand Film Production Fund New Zealand Film Commission New Zealand On Air Te Mangai Paho
| distributor    = Transmission Films
| released       =  
| runtime        = 88 minutes
| country        = New Zealand
| language       = English Maori
| budget         = 
| gross          = $8,621,535 
}}
Boy is a 2010 New Zealand Bildungsroman|coming-of-age comedy-drama film written and directed by Taika Waititi and financed by the New Zealand Film Commission. In New Zealand, the film eclipsed previous records for a first weeks box office takings for a local production.    Boy went on to become the highest grossing New Zealand film at the local box office.  The soundtrack to Boy features New Zealand artists such as The Phoenix Foundation, who previously provided music for Waititis film Eagle vs Shark.

==Plot==
The year is 1984. Alamein/"Boy" is an 11-year-old boy who lives in Waihau Bay, in the Bay of Plenty region of New Zealand, on a small farm with his grandmother, little brother Rocky, and several other cousins. Boy spends his time dreaming of Michael Jackson and his estranged father, Alamein, who has since left him and Rocky. Boy continually creates stories about his father such as him escaping prison and taking him to see Michael Jackson live. Even though Boy continues to believe this, his classmates never believe him anyway, which starts a fight between him and another classmate, Kingi, and his older brother Holden. When Boys grandmother leaves for a funeral one day, she leaves Boy in charge of the house and his brother and cousins; Boy is then surprised to see his father and two other men arrive at the farm. 

Boy and the others are happy to see Alamein return, but Rocky is displeased at their fathers sudden reappearance. It seems at first that Alamein is here to finally be in his sons lives but it is soon revealed that he is actually back to find a bag of money he had buried before being arrested by the police. With his patched gang, the Crazy Horses which is just him and two friends, he begins to dig up the money in a nearby field he had buried it at but has trouble remembering the exact spot. Boy sees this and offers to help and Alamein soon decides to hang out with Boy and be a father. The two have fun such as going on drives in Alameins car and getting revenge on Boys bully and the bullys brother. Alamein has even convinced Boy (and others) to call him Shogun, instead of Dad. Meanwhile, Alamein has no luck with digging up the money and decides to go in the business of marijuana after he discovers Boy bring some from helping his friends pick. Boy leads his father to the stalks of marijuana who gathers all of it to use and sell. Later, Alamein and his gang go to a local hotel, and get in a confrontation with the real gang who planted the marijuana that Alamein cut down, and are beaten up. Even though this happens, Boy sees his father doing a dance fighting sequence with the others, still seeing the best in him. 
 father is depressed in the barn and is visited by Rocky and finally sits to comfort him. Just then, Boy comes in and begins to hit his father in anger but Alamein stops his crying son who then goes back to the house. 

Boy and Rocky visit their mothers grave the next day to find Alamein sitting there, staring at the grave. The final scene of the film shows the boys silently joining Alamein as they sit around the grave, where Rocky asks Alamein, "How was Japan?"

==Cast== Maori kid who is a huge fan of Michael Jackson, Boy dreams of becoming rich and going to the city with his father, brother and his pet goat. His real name is Alamein, like his father.
* Te Aho Aho Eketone-Whitu as Rocky, Boys younger brother who believes he has super powers. Hes very shy, likely because of his mothers death during his birth.
* Taika Waititi as Alamein, Boy and Rockys Father; an ex-convict who wants to be the leader of a biker gang, and comes back to Waihau Bay to find his lost money. 
* Moerangi Tihore as Dynasty, Dallas sister and one of Boys best friends. She is the daughter of a biker, and seems to have feelings for Boy.
* Cherilee Martin as Kelly, Boys same-age cousin, who lives with him in the same house, with her three kid sisters.
* RickyLee Waipuka-Russell as Chardonnay, a teenage girl that Boy has a crush on, and that totally ignores him.
* Haze Reweti as Dallas, a long-haired boy and brother of Dynasty, who is one of Boys best friends.
* Maakariini Butler as Murray, one of Boys friends from school.
* Rajvinder Eria as Tane, Boys Indian friend, who is always alongside Murray and Dallas.
* Manihera Rangiuaia as Kingi, a school bully who often bullies Boy. He wears a Michael Jackson "Thriller (song)|Thriller" jacket.
* Darcy Ray Flavell-Hudson as Holden, Kingis older brother, who also bullies Boy but comes to fear Boys father Alamein and comes to admire him. Rachel House as Aunty Gracey, the sister of Boys deceased mother, who owns a store in front of the sea. Craig Hall as Mr Langston, the Pākehā school principal, who studied with Boys parents.
* Waihoroi Shortland as Weirdo, a strange fat man who lives near the bridge, and appears to always be looking for something. He seems to be childish and inoffensive.
* Cohen Holloway as Chuppa, a Pākehā friend of Alamein, who is an ex-convict and very foolish, along with Juju.
* Pana Hema Taylor as Juju, the other of Alameins friend. He has a strange haircut, and like Chuppa is always getting himself in trouble alongside the kids.
* Mavis Paenga as Nan, Boys grandmother and Alameins mother. She travels to someones funeral for two weeks, leaving Boy in charge. She calls the children " My Mokos ", which is short for Mokopuna, the Maori word for grandchildren.

==Production==
Waititi started developing Boy soon after finishing the short film Two Cars, One Night; and it first emerged as a film called Choice. The project was accepted into the Sundance Writers Lab in 2005, where Waititi workshopped it with script writers Frank Pierson, Susan Shilliday, David Benioff and Naomi Foner Gyllenhaal. Instead of making Boy his first film as a director, Waititi went on to make oddball romance Eagle vs Shark, and continued to develop the screenplay over the next three years. When the script was finally ready there was a small window of opportunity in which to make it. Waititi dropped the title Choice because he felt it would not translate to international audiences, and the film was retitled The Volcano. "It was a big pain about this kid’s potential to be bigger than he is or just bloom or explode," said Waititi. "So it was a character in the script as well. When we were shooting the film it was still called Volcano and during the editing. We ended up cutting a lot of the stuff out."  Waititi wanted to shoot the film in the place where he partly grew up, Waihau Bay. The story was set in summer, but it was challenging to shoot in the height of summer due to the areas popularity as a fishing and holiday destination. The film features the maize fields and the maize is harvested from late April.    James Rolleston was never actually intended to play the lead role of "Boy". Rolleston originally turned up for a costume fitting as an extra and after short deliberation was offered the role. 

==Soundtrack==
* Hine e Hine - The Phoenix Foundation
* Poi E - Patea Maori Club
* Pass the Dutchie - Musical Youth
* One Million Rainbows - The Phoenix Foundation Herbs
* Herbs
* Forget It - The Phoenix Foundation
* Aku Raukura (Disco Mix) - Patea Maori Club
* Out on the Street - Alastair Riddell
* Hine e Hine - St Josephs Maori Girls Choir
* Here We Are - The Phoenix Foundation
* Paki-o-Matariki - The Ratana Senior Concert Party
* Mum - Prince Tui Teka
* E Te Atua - St Josephs Maori Girls Choir
* Karu - Prince Tui Teka
* Flock of Hearts - The Phoenix Foundation

==Release==
Boy premiered at the 2010 Sundance Film Festival on 22 January 2010 and competed in the "World Cinema – Dramatic" category. 

==Reception==
===Critical response===
Based on 65 reviews collected by Rotten Tomatoes, the film has an overall approval rating from critics of 86%, with an average score of 7.2/10.  Peter Calder of The New Zealand Herald gave the film five out of five stars. He praised the performances by the three main actors and said "its hard to praise too highly the pitch-perfect tone of this movie." 

===Box office=== Alice in How to Clash of the Titans.  Boy then went on to become the highest grossing New Zealand film to date on its own soil, taking over The Worlds Fastest Indian which had held the position for five years. 

===Awards===
AFI Fest 2010
* Audience Award - Best International Feature Film - Taika Waititi - Won
Asia Pacific Screen Awards 2010
* Asia Pacific Screen Award - Best Childrens Feature Film - Ainsley Gardiner, Cliff Curtis, Emanuel Michael and Merata Mita - Nominated
Berlin International Film Festival 2010
* Deutsches Kinderhilfswerk Grand Prix - Best Feature Film - Taika Waititi - Won
Melbourne International Film Festival 2010
* Most Popular Feature Film - Taika Waititi - Won
Sundance Film Festival 2010
* Grand Jury Prize - World Cinema - Dramatic - Taika Waititi - Nominated
Sydney Film Festival 2010
* Audience Award - Best Feature Film - Taika Waititi - Won
New Zealand Film and TV Awards (II) 2010
* Film Award - Best Feature Film - Ainsley Gardiner - Won
* Film Award - Best Director in a Film Feature - Taika Waititi - Won
* Film Award - Best Supporting Actor in a Feature Film - Taika Waititi - Won
* Film Award - Best Screenplay for a Feature Film - Taika Waititi - Won
* Film Award - Best Cinematography in a Feature Film - Adam Clark - Won
* Film Award - Best Editing in a Feature Film - Chris Plummer - Won
* Film Award - Best Score - Lukasz Pawel Buda, Samuel Scott and Conrad Wedde - Won
* Film Award - Best Lead Actor in a Feature Film - James Rolleston - Nominated
* Film Award - Best Supporting Actor in a Feature Film - Te Aho Aho Eketone-Whitu - Nominated
* Film Award - Best Make-Up Design in a Feature Film - Dannelle Satherley - Nominated
* Film Award - Best Sound Design in a Feature Film - Ken Saville, Tim Prebble, Chris Todd, Michael Hedges and Gilbert Lake - Nominated
* Film Award - Best Production Design in a Feature Film - Shayne Radford - Nominated
* Film Award - Best Costume Design in a Feature Film - Amanda Neale - Nominated

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 