Royal Flash (film)
 
 
{{Infobox film
| name        = Royal Flash
| image       = RoyalFlashPoster.jpg
| director    = Richard Lester
| producer    = Dennis ODell David V. Picker
| writer      = George MacDonald Fraser based on the novel by George MacDonald Fraser
| starring    = Malcolm McDowell Oliver Reed Alan Bates Florinda Bolkan
| distributor = 20th Century Fox
| released    =  
| runtime     = 102 minutes
| country     = United Kingdom
| language    = English
| music       =
| awards      =
| budget      = $4,040,000 
}} Flashman novel, Royal Flash.  It starred Malcolm McDowell as Flashman. Additionally, Oliver Reed appeared in the role of Otto von Bismarck, Alan Bates as Rudi von Sternberg, and  Florinda Bolkan played Lola Montez.  Fraser wrote the screenplay and the film was directed by Richard Lester.

Though it got good reviews for its performances and action scenes, Royal Flash only saw limited release in theatres.

== Plot summary ==
The film begins with Flashman making a patriotic speech to the boys of Rugby School framed by a giant Union Flag, in a scene which appears to be a parody of the opening sequence in the 1970 film Patton (film)|Patton. There is a brief flashback to the events of the original Flashman (novel)|Flashman, with the head of the Rugby School (Michael Hordern) recounting Flashmans exploits in First Anglo-Afghan War|Afghanistan.
 Danish prince, Henry Cooper), at a house party.  Bismarck does not wish the Princess to marry a Dane, since this may tilt the balance on the Schleswig-Holstein Question and interfere with his plans for a united Germany. 

==Cast==
*Malcolm McDowell as Captain Harry Flashman
*Alan Bates as Rudi Von Sternberg
*Florinda Bolkan as Lola Montez
*Oliver Reed as Otto von Bismarck Tom Bell as De Gautet
*Joss Ackland as Sapten
*Christopher Cazenove as Eric Hansen
*Henry Cooper as John Gully MP
*Lionel Jeffries as Kraftstein
*Alastair Sim as Mr Greig
*Michael Hordern as Headmaster
*Britt Ekland as Duchess Irma
*Bob Hoskins as Police Constable
*Arthur Brough as King Ludwig of Bavaria Margaret Courtenay as Lady In Honour Duel
*Stuart Rayner as Speedicut
*Leon Greene as Grundwig
*David Jason as The Mayor
*Noel Johnson as Lord Chamberlain
*Ben Aris as Fireman
*Rula Lenska as Helga
*Bob Peck as Police Inspector John Stuart as English General
*Frank Grimes as Lieutenant
*Paul Burton as Tom Brown
*Tessa Dahl as 1st Girl

==Production notes==
The first Flashman novel had been published in 1968 and attracted the interest of film producers. At one stage Richard Lester was to direct, but the necessary financing could not be secured. However Lester enjoyed the novel and then hired George MacDonald Fraser to adapt The Three Musketeers for two successful films in the early 1970s. This enabled Lester to obtain finance for a Flashman movie.

Royal Flash was the second Flashman novel, and had been published in 1970. The New York Times said "Mr MacDonald Fraser has considerable narrative skill... most ingeniously plotted and often hilariously funny." 

The film was one of the first produced by executive David Picker following his resignation from being head of United Artists in 1973. 

At one stage John Alderton was reportedly considered to play Flashman. 

The film was shot partly on location in Bavaria. 
 Pennies From Heaven followed by British and American films, including the role of Eddie Valiant in Who Framed Roger Rabbit.

==Reception==
The Observer said the film "leave one breathless not so much with enchantment as with boredom".  The Wall Street Journal said it was "disappointing". 

== Home media ==
Although the film was released on VHS there was no United Kingdom DVD for years, though there was a North American (Region 1) version. A UK DVD and Blu-ray was eventually released on 20 May 2013 by Odeon Entertainment. 

==References==
 

== External links ==
* 
*  at Rotten Tomatoes

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 