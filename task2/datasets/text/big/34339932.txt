Caesar Must Die
 
{{Infobox film
| name           = Caesar Must Die
| image          = Cesare-deve-morire-poster.jpg
| caption        = Theatrical release poster Paolo Taviani Vittorio Taviani
| producer       = Grazia Volpi
| screenplay     = Paolo Taviani Vittorio Taviani
| based on       =  
| starring       = Salvatore Striano Cosimo Rega Giovanni Arcuri Antonio Frasca
| music          = Giuliano Taviani Carmelo Travia
| cinematography = Simone Zampagni
| editing        = Roberto Perpignani
| studio         = Rai Cinema La Talee Stemal Entertainment
| distributor    = Sacher Distribuzione
| released       =  
| runtime        = 77 minutes  
| country        = Italy
| language       = Italian
}}

Caesar Must Die ( ) is a 2012 Italian drama film directed by Paolo and Vittorio Taviani. The film competed at the 62nd Berlin International Film Festival    where it won the Golden Bear.       

==Plot==
  Rebibbia Prison Julius Caesar. 

==Cast==
* Salvatore Striano as Bruto (Marcus Junius Brutus the Younger|Brutus)
* Cosimo Rega as Cassio (Gaius Cassius Longinus|Cassius)
* Giovanni Arcuri as Cesare (Julius Caesar|Caesar)
* Antonio Frasca as Marcantonio (Mark Antony)
* Juan Dario Bonetti as Decio (Decius Brutus)
* Vincenzo Gallo as Lucio (Lucius)
* Rosario Majorana as Metello (Metellus Cimber)
* Francesco De Masi as Trebonio (Trebonius)
* Gennaro Solito as Cinna (Lucius Cornelius Cinna (suffect consul)|Cinna)
* Vittorio Parrella as Casca (Servilius Casca|Casca)
* Pasquale Crapetti as Legionär
* Francesco Carusone as Wahrsager
* Fabio Rizzuto as Stratone (Strato)
* Maurilio Giaffreda as Ottavio (Octavius)
* Fabio Cavalli as Theatre director

==Accolades== Best Foreign Language Oscar at the 85th Academy Awards, but it did not make the final shortlist.   
 humanist film" that "blends gentle humour with an emotional punch".  Paolo Taviani said that he hoped moviegoers would "say to themselves or even those around them... that even a prisoner with a dreadful sentence, even a life sentence, is and remains a human being". Vittorio Taviani read out the names of the cast. 

At Metacritic, the film was given a rating of 76/100 based on 12 critics, which evaluates as generally favorable reviews  

==See also==

* List of black-and-white films produced since 1970
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Italian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 