Closure of Catharsis
{{Infobox film
| name = Closure of Catharsis
| director = Rouzbeh Rashidi
| writer = Rouzbeh Rashidi
| starring = James Devereux
| producer = Rouzbeh Rashidi
| released =  
| country = United Kingdom Ireland
| language = English
| runtime = 100 minutes
}}
Closure of Catharsis  is a British-Irish Experimental film directed by Rouzbeh Rashidi that tells the visual story of a man who sits on a park bench talking to the camera, trying to weave together a thought that wont cohere while commenting on passers-by, his guests... Mysterious images intervene, overturning the serenity of the park-bench monologue.   

==Production==
Rouzbeh Rashidi made this film with a low budget and total cast and crew of four people including the actors. Intentionally working without a script or any kind of written pre-planning, Rouzbeh Rashidi constructed Closure Of Catharsis around an improvised monologue by actor James Devereaux, which took place on a park bench in Hackneys London Fields. Rashidi did not arrange any kind of rehearsal and gave only the briefest of instructions to the actor; "youre struggling to remember something, something from the past which you have repressed because of its traumatic effect upon you, and the memory can be true or false, or a mixture of both".

The improvisation took place over two hours without any break, and Rashidi did not attempt to direct or shape James performance in the traditional sense; in fact, Rashidi was not even present for much of the improvisation, instead, he would deliberately wander off for 20 or 30 minutes at a time, allowing James to simply respond to his immediate situation in the park; a couple of unleashed dangerous dogs, a squirrel darting across a branch, a jogger cruising by, some school girls giggling at the camera, all became grist to the improvisational mill. Rashidis only interjections were as an occasional mysterious off-camera "other", stimulating James by giving him random objects found in the park, such as an old pendant or an empty bottle of vodka. Aside from this however, Rashidi left James to his own devices in front of camera, the only boundary being the frame within which James had been placed, and sometimes not even the frame, as when he stepped outside of it. 

Rouzbeh Rashidi was inspired by Remodernist film manifesto in order to make Closure of Catharsis.

==Further reading==
*  
*  
*  
*  

==References==
 

==External links==
* 
*  on Mubi (website)

 
 
 
 
 
 