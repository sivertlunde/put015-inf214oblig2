The Big Knife
{{Infobox film
| name           = The Big Knife
| image          = Big-knife.jpeg
| caption        = Theatrical release poster
| alt            = 
| director       = Robert Aldrich
| producer       = Robert Aldrich
| screenplay     = James Poe
| based on       =  
| narrator       = Richard Boone
| starring       = Jack Palance Ida Lupino Wendell Corey Jean Hagen Rod Steiger Shelley Winters
| music          = Frank De Vol
| cinematography = Ernest Laszlo
| editing        = Michael Luciano
| studio         = The Associates Aldrich Company MGM (2003, DVD)
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
| budget = $460,000 Alain Silver and James Ursini, Whatever Happened to Robert Aldrich?, Limelight, 1995 p 242 
| gross = $1,250,000 Alain Silver and James Ursini, Whatever Happened to Robert Aldrich?, Limelight, 1995 p 14  220,066 admissions (France)   at Box Office Story 
}} the 1949 play by Clifford Odets. The film stars Jack Palance, Ida Lupino, Wendell Corey, Jean Hagen, Rod Steiger, Shelley Winters, Ilka Chase, and Everett Sloane. 

==Plot==
Charlie Castle, a very successful Hollywood actor, lives in a huge home. But his wife Marion is on the verge of leaving him, which he refuses to confirm to influential gossip columnist Patty Benedict.

On his wifes advice, Castle is adamantly refusing to renew his contract, which enrages Stanley Shriner Hoff, his powerful studio boss. Castle wants to be free from the studios grip on his life and career.

Hoff and his right-hand man Smiley Coy have knowledge of a hit-and-run accident in which Castle was involved and threaten to use this information against him. Hoff is willing to do anything to make the actor sign a seven-year renewal.

Castles soul is tortured. He wants to win back his idealistic wife, who has been proposed to by Hank Teagle, a writer. And he longs to do more inspiring work than the schlock films Hoff makes him do, pleading with his needy agent Nat to help him be free. But the studio chiefs blackmail works and Charlie signs the new contract.

Feeling sorry for himself, the darker side of his nature causes Castle to have a fling with Connie, the flirtatious wife of his friend Buddy Bliss, who had taken the blame for Charlies car accident.

When a struggling starlet named Dixie Evans threatens to reveal what she knows about the crash, Hoff and Smiley decide to have her silenced permanently. They try to involve Castle in their sinister plot and even extort Charlies wife, secretly recording her conversations with the new man in her life. That is the last straw for Castle, who finally defies the ruthless men who employ him.

However, having betrayed a friend, lost the woman he loves and sacrificed his integrity, Charlie can no longer live with himself. He has a hot bath drawn, gets into it and ends his own life.

==Cast==
* Jack Palance as Charlie Castle
* Ida Lupino as Marion Castle
* Wendell Corey as Smiley Coy
* Jean Hagen as Connie Bliss
* Rod Steiger as Stanley Shriner Hoff
* Shelley Winters as Dixie Evans
* Ilka Chase as Patty Benedict
* Everett Sloane as Nat Danziger
* Wesley Addy as Horatio "Hank" Teagle
* Paul Langton as Buddy Bliss
* Nick Dennis as Mickey Feeney
* Michael Winkelman as Billy Castle

==Reception==

===Critical response=== New York Times film critic Bosley Crowther, was disappointed and believed the storyline lacked credibility.  He wrote:
 

Film critic Jeff Stafford analyzed some of the films elements, and wrote:
 

===Awards===
Wins
* Venice Film Festival: Silver Lion, Robert Aldrich; 1955.

Nominations
* Venice Film Festival: Golden Lion, Robert Aldrich; 1955

==Stage play== Broadway at the National Theatre Broadway revival is set to open at the American Airlines Theatre on April 16, 2013, produced by Roundabout Theatre Company, directed by Doug Hughes and starring Bobby Cannavale.

==DVD==
The Big Knife was released to DVD by MGM Home Video on April 1st, 2003 as a Region 1 widescreen DVD.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 