The Hawk's Nest
{{Infobox film
| name           = The Hawks Nest
| image          =
| caption        =
| director       = Benjamin Christensen
| producer       =
| writer         = Wid Gunning (story) James T. ODonohoe (adaptation) Casey Robinson (titles)
| starring       = Milton Sills Doris Kenyon Sojin Montagu Love Mitchell Lewis Stuart Holmes
| cinematography = Sol Polito
| editing        = Frank Ware First National Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Hawks Nest was a 1928 American film directed by Benjamin Christensen. It is believed to be lost film|lost. It was released by First National Pictures and stars husband and wife Milton Sills and Doris Kenyon.   

==Plot==
The title of The Hawks Nest comes from the speakeasy around which most of the action revolves. Two bootleggers, played by Milton Sills and Mitchell Lewis, quarrel over a dancer (Doris Kenyon) while a political assassination plot.

==Cast==
*Milton Sills - The Hawk/John Finchley
*Doris Kenyon - Madelon Arden
*Sojin - Sojin
*Montagu Love - Dan Daugherty
*Mitchell Lewis - James Kent
*Stuart Holmes - Barney McGuire
*James Bradbury Jr. - Gangster(uncredited)

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 