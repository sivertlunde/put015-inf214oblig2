Bhoomi Thayiya Chochchala Maga
{{Infobox film
| name = Bhoomi Thayiya Chochchala Maga
| image = 
| caption =
| director = Rajendra Singh Babu
| writer = Rajendra Singh Babu Vijayalakshmi 
| producer = Jai Jagadish   Dushyanth Singh
| music = V. Manohar
| cinematography = B. C. Gowrishankar
| editing = Suresh Urs
| studio = Vaibhavalakshmi Productions
| released = 1998
| runtime = 144 minutes
| language = Kannada
| country = India
| budget =
}} drama film Vijayalakshmi and Shilpa in Karnataka State awards.

The film featured original score and soundtrack composed and written by V. Manohar.

== Cast ==
* Shivarajkumar as Karna
* Ramesh Aravind Vijayalakshmi  Shilpa
* Lokesh
* Rangayana Raghu
* Girija Lokesh
* Shankar Ashwath
* Karibasavaiah
* Chomanadudi Vasudeva Rao

== Soundtrack ==
The music was composed by V. Manohar. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Nesara Nesara
| extra1 = K. S. Chithra
| lyrics1 = V. Manohar
| length1 = 
| title2 = Sa Pa Ni Sa
| extra2 = Rajesh Krishnan
| lyrics2 = V. Manohar
| length2 = 
| title3 = Le Le Le Le
| extra3 = Rajesh Krishnan, K. S. Chithra, Ramesh Chandra
| lyrics3 = V. Manohar
| length3 = 
| title4 = Bhoomi Thayiya Rajkumar
| lyrics4 = Da Ra Bendre
| length4 = 
| title5 = Naa Preethiya Huduga
| extra5 = Rajesh Krishnan, Sowmya Raoh
| lyrics5 = V. Manohar
| length5 = 
| title6 = Jhumka Jhumka
| extra6 = Rajesh Krishnan, Sowmya Raoh
| lyrics6 = V. Manohar
| length6 = 
}}

==Awards==
* Karnataka State Film Awards
# Special Film of Social Concerns
# Best Story writer - Rajendra Singh Babu
# Best Dialogue writer - S. Surendranath

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 

 