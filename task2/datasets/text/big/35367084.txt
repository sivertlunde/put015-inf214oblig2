Vi hade i alla fall tur med vädret – igen
{{Infobox film
| name           = Vi hade i alla fall tur med vädret &ndash; igen
| image          = 
| image size     = 
| director       = Kjell-Åke Andersson
| producer       = Peter Possne
| writer         = Santiago Gil   Kjell-Åke Andersson
| narrator       = 
| starring       = 
| music          =  
| cinematography =
| editing        = 
| studio         = 
| distributor    = Svensk Filmindustri
| released       =  
| runtime        = 95 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}
 Swedish comedy film by Kjell-Åke Andersson which had premiere on 5 December 2008. It is a sequel to the 1980 film Vi hade i alla fall tur med vädret.

==Plot==
Now, almost 30 years after their travel trailer vacation, Gösta and Gun are pensioners. This summer theyll go to their son Johans wedding in Norrland. Gösta buys a recreational vehicle for their journey but Gun perhaps wants to go by aircraft, but in the end she wants to go with Gösta by the recreational vehicle. On the way, they pick up their granddaughter Magda. But may Gösta really make a good journey?

==Selected cast==
*Rolf Skoglund as Gösta
*Claire Wikholm as Gun
*Mikaela Knapp as Magda, Lottas and Peppes daughter
*Gustav Berg as Jens, Magdas boyfriend
*Magdalena in de Betou as Lotta, Göstas and Guns daughter
*Jacob Ericksson as Peppe
*Robin Stegmar as Johan
*Ellen Mattsson as Pia, Johans girlfriend
*Sissela Kyle as Doctor
*Johan Glans as Hambörje
*Ulf Kvensler as Camping receptionist
*Michalis Koutsogiannakis as Miklos

==External links==
* 
* 
* 

 
 
 
 
 

 