Remake (film)
 
 
{{Infobox Film
| name           = Remake
| image          = 
| image_size     = 
| caption        = 
| director       = Dino Mustafić
| producer       = 
| writer         = Zlatko Topčić
| narrator       = 
| starring       = Ermin Bravo Aleksandar Seksan Ermin Sijamija
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 23 January 2003 (Netherlands)
| runtime        = 102 minutes
| country        = Bosnia-Herzegovina/France/Turkey Bosnian
| budget         = 
}}
Remake is a 2003 Bosnian movie that was made in Turkish-French co-production of Bosnian director Dino Mustafić. The film stars Ermin Bravo, Aleksandar Seksan, and Ermin Sijamija, and was written by Zlatko Topčić and inspired by a true story.

Remake follows father Ahmed and son Tarik Karaga during World War II and the Siege of Sarajevo. The film premiered at International Film Festival Rotterdam on 23 January 2003, and was released to cinemas throughout Bosnia and Herzegovina on 22 February 2003.


==Cast and characters==

*Ermin Bravo as Tarik Karaga, the main character
*Aleksandar Seksan as Miro Jovanović, Tariks best friend
*Miralem Zupčević as Ahmed Karaga, Tariks father
**Ermin Sijamija portrays the younger version of Ahmed
*Dejan Acimović as Duke Mišo, one of the commanders of the local Serbian forces and Tariks neighbor
*Lucija Šerbedžija as Eva Bebek, Ahmed girlfriend from his youth
*Slaven Knezović as Marko Kalaba, Ahmeds friend
*Helena Minić as Alma Dizdarević, Tariks girlfriend
*Jasna Diklić as Desa Jovanović, Miros mother
*Zijah Sokolović as Mirsad Alihodžić, BH Refugee in France
*Evelyne Bouix as Katrin Leconte, wife of producer
*François Berléand as Francois - Charles Leconte, Producer
*Haris Begović as Adis Dizdarević, Almas younger brother
*Izudin Bajrović as Jovo, Serbian soldier
*Emir Hadžihafizbegović as Željko, Serbian guard
*Admir Glamočak as an interrogator, investigator Ustaša
*Mario Drmać as Remzo, a singer from the camp
*Miraj Grbić as Mitar, a guard

==Awards==

===Wins===
*2003 The Munich Film Festival - Special Prize 
*2004 The Wine Country Film Festival - Award for Peace and Cultural understanding

===Nominations===
*2003 The International Istanbul Film Festival - Best Film 
*2003 The Paris Film Festival - Best Film 

==Reception==
In Bosnia and Herzegovina, Remake was quite commercially successful, and in 2003 it was the most-watched motion picture from their home country. On IMDb, it is assessed grade 7.6 / 10, garnering general praise for the films emotional engagement through some of its shocking and touching scenes. The film has experienced success abroad. It was screened at festivals in the Netherlands, Poland, Croatia, France, Turkey, Germany, the Czech Republic and in Hungary experienced the TV premiere.

Remake is the first film directed by   as the lead actors Ermin Bravo, Ermin Sijamija, Mario Drmać and the young Croatian actress Helena Minić. It is essential to the emergence of world-renowned French actors Francois Berleand and Evelyne Bouix.

==External links==
* 

 
 
 
 
 