Hunter of Invisible Game
{{Infobox film
| name     = Hunter of Invisible Game
| image    = Hunterofinvisiblegame.jpg
| caption  = Poster for the short film
| director = Thom Zimny, Bruce Springsteen
| starring = Bruce Springsteen
| screenplay = 
| story    = 
| producer = 
| cinematography = 
| editing  = 
| studio   = 
| distributor = 
| released =  
| runtime  = 10 minutes
| country  = United States
| language = English
| budget   = 
| gross   = 
| music   = Bruce Springsteen
}}
 High Hopes. The film marks directorial debut of Springsteen who co-directed the film with Thom Zimny.   In the film, Springsteen plays a post-apocalyptic survivor, haunted by his past who sets out on his own, recalling times of togetherness with his family as he forages his memories alone.  

"For a long part of the year, Thom Zimny and I have been talking about shooting a short film for “Hunter Of Invisible Game.” We’ve finally got the job done, and we think it’s one of our best. Thanks Thom for the hard work and brotherly collaboration. You and your crew bring it all. And to all of you out there in E St. Nation, we hope you enjoy! See ya up the road." – Bruce Springsteen

==Cast==
*Bruce Springsteen - Man
*Danielle Panek - Woman
*Ludo Roveda - Boy
*Isabel Danyluk - Kid #1 in flashback
*Raffaela Danyluk - Kid #2 in flashback
*Roman Danyluk - Kid #3 in flashback
*George Fassbinder
*Richard Kall
*Ricky Loring
*Eric Matheny
*Mauricio Toledo
*Rossana Roveda
*Damien Wallace


==References==
 

== External links ==
*  

 


 
 
 
 