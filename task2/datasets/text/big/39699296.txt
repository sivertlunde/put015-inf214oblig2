Katharina Knie (film)
{{Infobox film
| name           = Katharina Knie 
| image          = 
| image_size     = 
| caption        = 
| director       = Karl Grune 
| producer       = Karl Grune
| writer         = Carl Zuckmayer (play)   Franz Höllering
| narrator       = 
| starring       = Eugen Klöpfer   Carmen Boni   Adele Sandrock   Fritz Kampers
| music          = Werner Schmidt-Boelcke
| editing        =
| cinematography = Karl Hasselmann
| studio         = Karl Grune Film
| distributor    =  Bavaria Film
| released       = 13 December 1929
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent drama play of the same title by Carl Zuckmayer.  The films art direction was by Robert Neppach and Erwin Scharf.

==Cast==
* Eugen Klöpfer as Der alte Knie 
* Carmen Boni as Katherina Knie 
* Adele Sandrockas Bibo 
* Fritz Kampers as Ignaz Scheel, Trapezkünstler 
* Vladimir Sokoloff as Julius, der Clown 
* Viktor de Kowa as Lorenz Knie 
* Peter Voß as Rothhacker, Gutsbesitzer 
* Frida Richard as Rothhackers Mutter 
* Fraenze Roloff as Magd 
* Willi Forst as Dr. Schindler 
* Ilse Bachmann as Seine Freundin 
* Louis Treumann as Variétedirektor 
* Wilhelm Diegelmann as Gerichtsvollzieher 
* Carla Bartheel    Ernst Busch   
* Karl Etlinger   
* Ursula Grabley   
* Otto Sauter-Sarto  
* Ludwig Stössel   
* Michael von Newlinsky   
* Aribert Wäscher

==References==
 

==Bibliography==
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918-1945. University of California Press, 1999.

==External links==
* 

 
 
 
 
 
 
 
 


 
 