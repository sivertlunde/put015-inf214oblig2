Eden (Johnston novel)
 
 

{{Infobox book
| name             = Eden
| image            = File:Cover of Eden (Johnston novel).jpg
| caption    = Front cover of Eden
| author           = Dorothy Johnston
| title_orig       = 
| translator       = 
| illustrator      = 
| cover_artist     = 
| country          = Australia English
| series           = Sandra Mahoney
| subject          = 
| genre            = Crime Fiction
| publisher        = Wakefield Press
| publisher2       = 
| pub_date         = 
| english_pub_date = 2007
| media_type       = 
| pages            =
| awards           = 
| isbn             = 9781862547605
| oclc             = 
| dewey            = 
| congress         =  The White Tower
| followed_by      = 
| wikisource       = 
}}

Eden is a detective fiction novel by Australian author Dorothy Johnston. It is the third novel in the Sandra Mahoney series.

==Plot Summary==
Eden Carmichael died on a hot Tuesday afternoon in January. He was found lying across a double bed at one of Canberra’s best-known brothels, dressed in a blue and white flowered silk dress and a blonde wig.

Eden is set in a hot summer that sees Canberra initially empty of its politicians and many of its citizens as well. Mahoney runs a consultancy business with her partner, Ivan Semyonov, and is often assisted by their friend in the ACT police force, Detective Sergeant Brook. Both these men, and Mahoneys children as well, are on holiday at the start of the story.

Mahoney is drawn into the ripples surrounding the cross-dressing local politicians death when an anti-censorship lobby group hires her to investigate CleanNet, a company producing Internet filters. At first the two seem to have no real connection, though Carmichael did attend a presentation the company arranged for a Federal Minister, which Mahoney considers odd, given Carmichaels long-standing anti-censorship views. Mahoney continues to research the company, at the same time building up a picture of Carmichael’s political and personal life, in particular his relationship with Margot Lancaster, the owner of the brothel where he died. His death is assumed to be the result of a heart attack, since he has already suffered one spectacular public heart attack, when he fell over the banisters at the Old Parliament House.

Mahoney’s investigation takes her to Sydney, where she meets CleanNet’s public face, and learns of an incident at Margots - the death of Jenny Bishop, a young woman who worked there. Bishops death is another that seems to have been an accident – a heroin overdose this time. By following leads that the police dont have time for, and that others, including the lobby group employing her, consider irrelevant, Mahoney gradually uncovers connections between Bishop, Carmichael, and two old friends, a Canberra pornographer and a Sydney-based florist. The florist obligingly lends the pornographer his website after new net censorship laws have made his site illegal.

A chase down the Federal Highway almost results in Mahoney’s death, then another of the women working at Margots disappears. After two tense days and nights, Mahoney manages to work out where shes gone. The denouement reveals who is really behind CleanNet, and exposes Bishop’s killer.

==Awards==

 

==Reviews==
 

==References==
 

 
 
 