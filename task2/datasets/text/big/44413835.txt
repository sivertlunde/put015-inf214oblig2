O Manishi Katha
{{Infobox film
| name           = O Manishi Katha
| image          =
| caption        =
| writer         = Aadi Ganesh     Radha Swamy Avula    
| producer       = Bala Bhai Chovatia
| director       = Radha Swamy Avula
| music          = Vijay Kurakula Kalyani
| cinematography = G. Ranganath
| editing        = K. Venkateswrulu
| studio         = Om Shiv Films
| released       =  
| runtime        = 
| country        = India
| language       = Telugu
| budget         = 
}}   Telugu drama Kalyani in the lead roles and music composed by Vijay Kurakula.

==Synopsis==
The film deals with the three  , rajas and Tamas (philosophy)|tamas.

==Cast==
*Jagapati Babu as Ramu Kalyani as Sita

==Soundtrack==
{{Infobox album
| Name        = 
| Tagline     = 
| Type        = film
| Artist      = Vijay Kurakula
| Cover       = 
| Released    = 2014
| Recorded    = 
| Genre       = Soundtrack
| Length      = 17:01
| Label       = Aditya Music
| Producer    = Vijay Kurakula
| Reviews     =
| Last album  =  
| This album  = 
| Next album  = 
}}

Music composed by Vijay Kurakula. Lyrics written by Suddala Ashok Teja. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 17:01
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =

| title1  = Nemmadiga Nemmadiga
| extra1  = Deepu,Geetha Madhuri
| length1 = 4:26

| title2  = Seethaleni Ramuni Katha
| extra2  = Suresh
| length2 = 3:34

| title3  = Kalalaa Vacchi Veluge Icchi
| extra3  = Krishna Chaitanya
| length3 = 3:32

| title4  = Ye Mattitho Chesavura Brahma 
| extra4  = Balaji
| length4 = 3:43

| title5  = Anadhaku Rakhi Katti  
| extra5  = Krishna Chaitanya
| length5 = 0:45

| title6  = Karunaku Nuvvu Chirunama    
| extra6  = Balaji
| length6 = 0:42
}}
   

==References==
 

 
 
 
 


 