Kène ya ma kène...
 

{{Infobox film
| name           = Kène ya ma kène...
| image          = 
| alt            =  
| caption        = 
| director       = Hichem Ben Ammar
| producer       = Hicham Ben Ammar
| screenplay     = Hichem Ben Ammar
| starring       = 
| music          = 
| cinematography = Hatem Nechi Abdessabour Belarbi Hichem Ben Ammar Rabii Messaoudi Anne Closset Louise Purnell Elodie Colomar Walid Mattar
| editing        = Inès Chérif
| studio         = 
| distributor    = 
| released       =   
| runtime        = 85 minutes
| country        = Tunisia
| language       = 
| budget         = 
| gross          = 
}}

Kène ya ma kène...  is a 2010 Tunisian documentary film.

== Synopsis ==
In a popular neighborhood of Tunis, a trombonist dreams that his son, Anès, becomes a great musician. The son makes his father’s dream his own, and develops extraordinary aptitudes with the violin. After winning several international competitions, he is offered the chance to enroll in the prestigious Yehudi Menuhin School in London. The film shows us Anès journey, the obstacles he faces on the way, and his evolution while in exile in Europe.

== Awards ==
* Milan 2010

== References ==
 

 
 
 
 
 
 
 


 
 