Last Days of the Coliseum
{{multiple issues|
 
 
}}
 

{{Infobox film
| name           = Last Days of the Coliseum
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Rich Hanley
| producer       = Rich Hanley
| writer         = Rich Hanley
| narrator       = Brian Smith
| starring       = 
| music          = 
| cinematography = Rich Hanley
| editing        = Rich Hanley
| studio         = River Ice GPH, LLC
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

Last Days of the Coliseum is a 2010 documentary film directed by Rich Hanley and narrated by Brian Smith that explores the history of the New Haven Coliseum and its role in southern New England culture.           The Coliseum was officially closed on September 1, 2002 by Mayor John DeStefano, Jr., and demolished on January 20, 2007.         

==Background==
Hanley made the decision to film the documentary of the New Haven Coliseum back in 2005 when it was announced the massive exposed steel building would be demolished,  because of the structure being a Connecticut cultural icon.   The film took five years to complete.   Featuring interviews with Coliseum architect Kevin Roche, World Wrestling Entertainment hall-of-fame ring announcer Howard Finkel and concert promoter Jim Koplik, the documentary reveals how the Coliseum rose amid the cultural upheavals of the 1960s but settled in as the home office for baby boomers in the 1970s and 1980s.   The film premiered on November 8, 2010, on Connecticut Public Television (CPTV)    and has subsequently been re-broadcast several times, as well as being screened locally.  The film was shot entirely in high-definition video|high-definition.   A 120 minute extended cut of the film is available on DVD.

==Reception==
New Haven Register wrote that while the documentary covers many of the events that took place in New Haven Coliseum, it also covers "the history and culture that forged New Haven’s hulking, rust-colored sports-and-entertainment palace," and that film "plays out in fascinating detail. Like any great documentary, the story explodes out to a history of New Haven in the latter half of the 20th century, particularly within the context of baby boomers’ cultural upheavals." 
 New Haven Independent writes that at "first glance The Last Days Of The Coliseum feels like a look backward," as the film takes the viewer "back to the rock-and-hockey palace’s roots at the former New Haven Arena, through the building’s noisy and ultimately catastrophic three decades of operation (hockey teams kept folding; the airborne garage starting crumbling in its second decade), ending with that unforgettable blast on a January morning in 2007."  Noting that the film covered former New Haven mayor Richard C. Lees wishes to "rebuild New Haven with dramatic, towering edifices", and how filmmaker Rich Hanley reminds the viewer of all the "famous rock acts and short-lived hockey teams and circuses" that brought thousands to the "concrete fortress that greeted drivers entering downtown."  But they note that the film is more nostalgia, and wrote that "the most enduring presence isn’t the building, but a now elderly man named Kevin Roche, the famous architect who moved here to build the New Brutalist behemoth. He sat for Hanley’s camera and demonstrated the human spirit that animated that concrete monster." 

==References==
 

==External links==
*  

 
 
 
 
 
 