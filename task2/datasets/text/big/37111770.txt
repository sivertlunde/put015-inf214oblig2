Ang Nawawala
{{Infobox film
| name           = Ang Nawawala
| image          = Ang Nawawala poster.jpg
| alt            = 
| caption        = 
| director       = Marie Jamora
| producer       = 
| writer         = {{Plainlist|
* Marie Jamora
* Ramon de Veyra
}}
| starring       = {{Plainlist|
* Dominic Roco 
* Dawn Zulueta
* Boboy Garovillo 
* Annicka Dolonius 
* Felix Roco 
}}
| music          = {{Plainlist|
* Mikey Amistoso
* Diego Mapa
* Jazz Nicolas
}}
| cinematography = Ming Kai Leung
| editing        = {{Plainlist|
* Marie Jamora
* Edsel Abesames
}}
| studio         = Indie Pop Films Brainchild Studios
| distributor    = Brainchild Studios
| released       =  
| runtime        = 
| country        = Philippines
| language       = Filipino, English
| budget         = 
| gross          = 
}}

Ang Nawawala is a 2012 Filipino drama film co-written and directed by Marie Jamora. The film stars Dominic Roco, Dawn Zulueta, Boboy Garovillo, Felix Roco, and Annicka Dolonius. The film was first screened as part of the 2012 Cinemalaya Philippine Independent Film Festival where it competed under the New Breed category.   

Ang Nawawala won the Audience Choice and Best Original Music Score awards during the Cinemalaya competition.   

==Synopsis==
Gibson Bonifacio stopped speaking when he was a child.

Now twenty years old, he is back in Manila for Christmas. While always festive in the Philippines, for his family it is tinged with sadness, marking the anniversary of his twin brother’s death. Against the backdrop of the vibrant local music scene, his childhood best friend tries to reconnect with him, while he unexpectedly finds a chance at his first, real romantic relationship.

Gibson reconsiders and redefines his relationship with his family, with himself, even with his dead brother: the only person he talks to.

==Cast==
*Dominic Roco as Gibson Bonifacio
*Dawn Zulueta as Esme Bonifacio
*Boboy Garovillo as Wes Bonifacio
*Felix Roco as Jamie Bonifacio
*Annicka Dolonius as Enid

==References==
 

==External links==
*  
*  

 
 
 
 


 