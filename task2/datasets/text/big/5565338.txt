Stroker Ace
 
{{Infobox film
| name           = Stroker Ace
| image          = Strokerposter.jpg
| caption        = Theatrical poster
| director       = Hal Needham
| producer       = Hank Moonjean
| based on       = {{based on|Stand On It  (novel) |{{plainlist|*William Neely
*Robert K. Ottum}}}} Hugh Wilson
*Hal Needham}}
| starring = {{plainlist|
* Burt Reynolds
* Ned Beatty
* Jim Nabors
* Parker Stevenson
* Loni Anderson
}}
| music          = Al Capps
| cinematography = Nick McLean
| editing        = {{plainlist|*William D. Gordean Carl Kress}}
| distributor    = Warner Bros. Universal Pictures
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $16.5 million
| gross          = $13 million (United&nbsp;States)
}}
Stroker Ace is a 1983 action comedy film, filmed in North Carolina   and Georgia (U.S. state)|Georgia, about a NASCAR driver, the eponymous Stroker Ace, played by Burt Reynolds.
 David Hobbs, and Chris Economaki. The movie was filmed on location at Charlotte Motor Speedway, Talladega Speedway and the Atlanta Motor Speedway in Hampton, Georgia. The theme song was performed by Charlie Daniels. 

Burt Reynolds turned down the role of astronaut Garrett Breedlove in Terms of Endearment to do this film. The role went to Jack Nicholson, who went on to win an Academy Award. Reynolds said he made this decision because "I felt I owed Hal more than I owed Jim" but that it was a turning point in his career from which he never recovered. "Thats where I lost them," he says of his fans. 

The movie was adapted from the 1971 novel Stand On It, an autobiography of fictional driver "Stroker Ace." The novels joint authors, William Neely and Robert K. Ottum, based the book on actual events from the racing world but with their protagonist as the subject. 

==Plot summary==
Stroker Ace is a popular race car driver from Waycross, Georgia, and (according to dialogue), a three-time champion, on the NASCAR circuit. An all-or-nothing man, he wins if he does not crash. He is arrogant and pompous, with no regard for the business side of his racing team. He also has an on-track, season-long rivalry with ambitious young driver Aubrey James (Parker Stevenson).
 Hugh Wilson), convince Stroker and his chief mechanic, Lugs Harvey, to sign up with him.

Overlooking his contract by not reading its specifics, Stroker begins a new life as the commercial face for the Chicken Pit fast-food restaurants. (The slogan on Strokers car reads: "The Fastest Chicken in the South.") His contract proves to stipulate that he must do personal appearances, which include dressing up in a chicken suit—feet included.

Realizing that he is locked into a bad deal, Stroker devises a plan with Lugs to get out of it. Torkel is on to Stroker, though, and allows his antics because he sees the racer as his big ticket to regional fame by promoting the Chicken Pit franchise.

A ladies man, Stroker tries to seduce the beautiful Pembrook, who is a Sunday School teacher, does not drink, and is a virgin. She spurns all of his advances until he learns to respect her views. One night, after getting her drunk on champagne, he has a chance to take advantage of her, but he does not.

Stroker is winning races under the Chicken Pit sponsorship and is in the running for the season-ending championship. At the beginning of the final race, Torkel is offered a deal to sell his franchise for a huge profit, as part of an elaborate scheme that Stroker and his friends have concocted. The catch is that if he wins the championship Stroker has to sell chicken for the next two years; if he loses is he out of the contract.

During the race Stroker is at odds with himself. He drops back in the race in an effort to lose, but his ego wont let him so he quickly begins moving back through the pack. Torkel, realizing that Stroker would rather lose than be bound by the contract, makes a public announcement that he is releasing Stroker immediately. He is unaware that Stroker is moving up through the field in an effort to win.

With the news that he is free from the contract, Stroker wins the championship in spectacular fashion by flipping his car over as he crosses the finish line. Torkel then finds that the lucrative offer for his chicken franchise is a fake, cooked up by Stroker and his friends.

==Reception==
The film was both a commercial and critical bomb. It received five Golden Raspberry Award nominations including Worst Picture, Worst Director, Worst Actress (Anderson) and Worst New Star (also for Anderson), winning one for Jim Nabors as Worst Supporting Actor.

Stroker Ace also got a 14% fresh rating on Rotten Tomatoes based on 14 reviews. 

==Main cast==
*Burt Reynolds as Stroker Ace
*Ned Beatty as Clyde Torkel
*Jim Nabors as Lugs Harvey
*Parker Stevenson as Aubrey James
*Loni Anderson as Pembrook Feeny
*John Byner as Doc Seegle
*Frank O. Hill as Dad Seegle
*Cassandra Peterson as Elvira, Mistress of the Dark
*Bubba Smith as Arnold
*Warren Stevens as Jim Catty
*Alfie Wise as Charlie
*Cary Guffey as Little Doc

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 