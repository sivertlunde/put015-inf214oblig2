David and Jonathan (film)
 
 
{{Infobox film
| name           = David and Jonathan 
| image          =
| caption        =
| director       = Alexander Butler 
| producer       = G.B. Samuelson
| writer         = E. Temple Thurston (novel)  Geoffrey Webb   Dick Ryan 
| music          = 
| cinematography = 
| editing        = 
| studio         = G.B. Samuelson Productions
| distributor    = General Film Distributors
| released       = July 1920
| runtime        = 6 reels  
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent adventure Geoffrey Webb Universal City shipwrecked on a desert island together with a girl they are both in love with.

==Cast==
* Madge Titheradge as Joan Meredith  Geoffrey Webb as David Mortlake 
* Dick Ryan as Jonathan Hawksley
* Sydney Wood as David, as a child 
* Jack Perks as David as a child

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

 

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 
 