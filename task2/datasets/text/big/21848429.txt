The First Day of Freedom
{{Infobox film
| name           = The First Day of Freedom
| image          = 
| image size     = 
| caption        = 
| director       = Aleksander Ford
| producer       = 
| writer         = Bohdan Czeszko Leon Kruczkowski
| starring       = Tadeusz Łomnicki
| music          = 
| cinematography = Tadeusz Wiezan
| editing        = Miroslawa Garlicka
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = Poland
| language       = Polish
| budget         = 
}}
The First Day of Freedom ( ) is a 1964 Polish drama film directed by Aleksander Ford. It was entered into the 1965 Cannes Film Festival.     

==Cast==
* Tadeusz Łomnicki - Lt. Jan
* Beata Tyszkiewicz - Inga Rhode
* Tadeusz Fijewski - Dr. Rhode
* Ryszard Barycz - Michal
* Krzysztof Chamiec - Hieronym
* Roman Kłosowski - Karol
* Mieczyslaw Stoor - Pawel
* Elzbieta Czyzewska - Luzzi Rhode
* Aldona Jaworska
* Mieczyslaw Kalenik - Otto
* Zdzislaw Lesniak - The Oddball
* Mieczyslaw Milecki
* Kazimierz Rudzki
* Vsevolod Sanayev - (as Wsewolod Sanejew)
* Mikhail Pugovkin

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 