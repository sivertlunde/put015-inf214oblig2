The White Silk Dress
{{Infobox film
| name           = The White Silk Dress
| image          = Alhdposter.jpg
| caption        = The film poster.
| director       = Luu Huynh
| producer       = Phuoc Sang Films
| writer         = Luu Huynh
| narrator       =
| starring       = Trương Ngọc Ánh, Nguyễn Quốc Khánh
| music          = Đức Trí
| distributor    = Phuoc Sang Films
| released       =  
| runtime        = 142 minutes
| country        = Vietnam
| language       = Vietnamese
| budget         = US$2 million    
}}
The White Silk Dress (Áo lụa Hà Đông in Vietnamese) is a 2006 Vietnamese war film directed by Luu Huynh starring Truong Ngoc Anh and Nguyen Quoc Khanh. With a budget of over 2 million dollars,  it is one of the most expensive Vietnamese films ever made. 

==Plot== French colonial rule in Vietnam.  Dan and Gu are lovers, as well as servants in different households who suffer at the hands of their cruel masters.  After Gus master is assassinated, Dan and Gu flee south, eventually ending up in the central Vietnamese seaside town of Hoi An.  There they raise a family, with Dan eventually giving birth to four girls.  Although impoverished, the family love and support each other, even as the horrors of encroaching war threaten to tear them apart.

The story emphasizes the importance of a white silk áo dài which Gu had given to Dan as a wedding gift before they fled south, with promises of a proper marriage someday in the future.  Years later in Hoi An, Dan must sacrifice this one valued possession (amongst other hardships and humiliations she must endure), in order to support her family and to provide for her daughters the áo dàis required to attend school.

The film ultimately is a tribute to the strength and heart of the Vietnamese woman, as symbolized through the áo dài.

==Reception==
The film was released in   who had attended the films screening at the Pusan International Film Festival called it "deeply moving" and remarked that "at packed screening caught, most audience members were in tears".   

To date, its victories abroad at   in   in China.     Best foreign language film category.

Despite its success, the film has also received some criticism as well as controversy, especially surrounding the directors political leanings as conveyed through the film.  This was preceded by similar controversy surrounding the director almost a decade earlier concerning a music video he was involved in which was perceived to be actively pro-communist by Vietnamese overseas.  Interestingly, this time around Huynh faces fire from both sides rather than just the   uprising would have been unlikely in 1954, having already occurred in 1945.       Furthermore, they charge that the modern áo dài, a recent development of the urban upper class in the 1930s, is unsuited to represent poor Vietnamese women.

==References==
 

== External links ==
*  
*  
*   
*   
*  

 
 
 
 
 