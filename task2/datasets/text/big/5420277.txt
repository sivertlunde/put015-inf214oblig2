The Babysitters
 
{{Infobox film
| name           = The Babysitters
| image          = Babysitters07poster.jpg
| caption        = Theatrical release poster
| director       = David Ross
| producer       =
| writer         = David Ross
| starring       = Katherine Waterston John Leguizamo Cynthia Nixon Andy Comeau
| music          = Chad Fischer
| cinematography = Michael McDonough
| editing        = Zene Baker
| distributor    = Peace Arch Releasing
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 David Ross. It stars Katherine Waterston, John Leguizamo, and Cynthia Nixon. The story follows a teenager who turns her babysitting service into a call girl service for married men after fooling around with one of her customers.

==Plot==

The movie begins with a voice over by Shirley Lyner (Waterston) as we are shown her babysitting service, a cabin with middle-aged men and teenage girls. A flashback takes us to Shirley being picked up by Michael Beltran (Leguizamo) for a babysitting job. After she is finished, they dine together and flirt with each other. Michael is frustrated with his wife and Shirley finds guys her age to be too immature.

Michael and his wife Gail (Nixon) meet with Michael’s friend Jerry (Comeau), who has a business offer for Michael. Gail is frustrated that Michael wants to take it, while Michael gets curious about an abandoned train yard behind the restaurant that Gail shows no interest in. When Michael is driving Shirley home that night, they stop at the train yard and explore it, eventually sharing a kiss and having sex. Michael pays Shirley extra.

Melissa discovers the truth about babysitting outings from Shirley and Michael tells Jerry about it. Michael asks Shirley if any of her other friends can “babysit” and her friend Melissa volunteers. Shirley asks her for 20% of the money as her cut to which Melissa agrees. They convince their friend Brenda to take the babysitting job as well and the girls set up a working business, going so far as to have business cards printed up. Michael learns that Shirley is babysitting for others besides him, which makes him uncomfortable.

Problem arises when Brenda invites her rough and aggressive younger stepsister Nadine into the group without checking with Shirley. She soon starts her own competing business behind Shirley’s back, and Shirley starts to lose customers. Shirley confronts Brenda and Brenda agrees to search Nadines room. She reports back that she found nothing. With Michael on watch, Shirley and Melissa search Nadines locker but find nothing. They trash the school to hide their true motive.

Melissa presents Shirley with fake permission slips for a trip to Jerry’s cabin for a weekend. The girls will be going under the make-believe of a school trip, while the men will be there on a “business retreat”. Michael is uncomfortable with Shirley being with other guys at the party. Jerry has supplied drugs at the party and attempts to rape Brenda.

Brenda wants to quit and Shirley agrees but Melissa is worried that Brenda will talk. Melissa has several of their customers attack and threaten Brenda’s brother, while Shirley gets angry at Melissa.

Michael’s wife confronts him about his distance and that he has been lying about his job situation and Michael finally voices some of his frustrations about the marriage, while Gail responds with hers. Michael tries to encourage Shirley to run away with him, while Shirley reminds him that this is just business and he’s cheating himself. She then tries unsuccessfully to contact Brenda. She hears that Nadine is babysitting without her knowledge and calls Melissa, who is with Jerry, and they go to confront her. Shirley discovers that it is her own father that is with Nadine.

The movie ends with another voiceover by Shirley. Across the street from Michaels house Shirley sees Michael interacting with his family.

==Critical reception==
The film received generally negative reviews from critics.   Metacritic reported the film had an average score of 35 out of 100, based on 11 reviews. 

==Soundtrack==
No official soundtrack was released. Music appearing in the film includes:
*"The New Science" – Ola Podrida
*"Too Many Stars" – Lets Go Sailing
*"21 People" – Eugene
*"Party Hard" – The Perceptionists
*"What What" – Eric V. Hachikian
*"Chedda" – Big City
*"Disaster" – The Besnard Lakes
*"Wild Winter" – Anubian Lights
*"Ping Pong" – Operator Please
*"Red Tandy" – Mother Hips
*"On the Road" – The Bossmen
*"Sap" –  The Freakwater
*"Bye Bye Bye" – Sia Furler and Chad Fischer

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 