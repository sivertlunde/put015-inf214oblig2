Shine a Light (film)
{{Infobox film
| name           = Shine a Light
| image          = Shinealightfilm.jpg
| caption        = Theatrical release poster
| director       = Martin Scorsese
| producer       = Steve Bing Michael Cohl Jack White Lisa Fischer Bill Clinton
| music          = The Rolling Stones Robert Richardson
| mixing         = Bob Clearmountain
I Live Recording by Bob Clearmountain & David Hewitt on the Silver Truck
| studio         = Steve Bing|Shangri-La Entertainment
| distributor    = Paramount Classics (USA) 20th Century Fox (UK/Australia)
| released       =  
| runtime        = 122 minutes
| country        = United States
| language       = English
| budget         = $1 million
| gross          = $15,773,351
}} Beacon Theatre the song soundtrack album Universal label. This is also the last movie by Paramount Classics, as the company merged into its sister company Paramount Vantage after the movie was released.

==Production and recording== Beacon Theatre on October 29 and November 1, 2006, but the performance footage used in the film is all from the second show. The music was recorded, mixed and co-produced by Bob Clearmountain. The  Live Recording was done on the Silver Truck with David Hewitt. The concert footage is preceded by a brief semi-fictionalized introduction about the preparations for the shows, and is intercut with historical news clips and archival interviews with band members. (A "break" in a backstage game of billiards a few minutes into the film recalls the opening scene of another Scorsese rockumentary, The Last Waltz.) The shows, which were added to the tour schedule for the purposes of the film shoot, featured a different set list than was typical of other shows on the tour (see below), and were noted for their star-studded crowds, including former United States President Bill Clinton and his wife, Hillary Clinton, who was then a United States Senator, and former President of Poland Aleksander Kwasniewski.         
 Jack White, Buddy Guy and Christina Aguilera performing with the Stones.
 
 
Prior to the October 29 show, 83-year old Ahmet Ertegün, a co-founder and executive of Atlantic Records and chairman of the Rock and Roll Hall of Fame and museum, was backstage in a VIP social area, the "Rattlesnake Inn," when Ertegün tripped and fell, striking his head on the concrete floor. He was rushed to the hospital, and died on December 14, 2006. The film was dedicated to his memory.

According to keyboardist Chuck Leavells tour diary, Mick Jagger had been ill with throat problems, forcing a postponement of the Stones scheduled Atlantic City concert and the October 31 Beacon Theater show was moved to November 1, to allow Jagger to recuperate.   
 Live at the Max, released in 1991.
 MPAA gave strong language, drug references, and smoking, and has been released on both DVD and Blu-ray disc by Paramount Home Entertainment, a division of Paramount Pictures.

==Performances== Mick Jagger and Keith Richards, except the ones which are noted.
# "Jumpin Jack Flash"
# "Shattered (song)|Shattered"
# "She Was Hot"
# "All Down the Line" Loving Cup"&nbsp;– Jack White As Tears Go By" (Jagger/Richards/Andrew Loog Oldham|Oldham) Some Girls" Just My Imagination" (Norman Whitfield/Barrett Strong)
# "Far Away Eyes"
# "Champagne & Reefer" (Muddy Waters)&nbsp;– with Buddy Guy
# "Tumbling Dice"
# * Band introductions
# "You Got the Silver"
# "Connection (Rolling Stones song)|Connection" (incomplete/cut with 1999 interview clips)
# "Sympathy for the Devil"
# "Live with Me"&nbsp;– with Christina Aguilera
# "Start Me Up" Brown Sugar"
# "(I Cant Get No) Satisfaction " Shine a Light" (incomplete; audio only)

Additional acoustic instrumental numbers are also played during the closing credits: Wild Horses"
* "Only Found Out Yesterday" (Richards)

Noting Martin Scorseses frequent use of Rolling Stones music in his scripted films, Mick Jagger has joked that Shine a Light may be the only Scorsese film that does not include "Gimme Shelter" in its soundtrack.   

==DVD release==
From Paramount Home Entertainment, Shine a Light was released on DVD and Blu-ray July 29, 2008.

;Bonus features
* A "featurette" with backstage and rehearsal footage
* "Undercover of the Night"
* "Paint It Black" (mistitled "Black" on some editions)
* "Little T&A" (mistitled "Shes my little rock and roll" on some editions)
* "Im Free (Rolling Stones song)|Im Free" (mistitled "Free to do what I want" on some editions)

The DVD sold 50,115 copies during the first week in the U.S. It sold 132,886 by August 14, 2008. 

==Critical reception==
Shine a Light received mostly positive reviews from critics. As of April 4, 2008, the review aggregator Rotten Tomatoes reported that 88% of critics gave the film positive reviews, based on 82 reviews. 
Metacritic reported the film had an average score of 76 out of 100, based on 33 reviews. 

The total U.S. box office gross as of 2011 stands at $5,505,267 while the international gross stands at $10,268,084 making for a total worldwide gross of $15,773,351.  . Box Office Mojo. Retrieved September 2, 2011. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 