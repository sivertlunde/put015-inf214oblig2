She's Gotta Have It
{{Infobox film
| name           = Shes Gotta Have It
| image          = Shes Gotta Have It film poster.jpg
| caption        = Theatrical release poster
| director       = Spike Lee
| producer       = Spike Lee  (credited as Shelton J. Lee) 
| writer         = Spike Lee
| starring       = Tracy Camilla Johns Tommy Redmond Hicks John Canada Terrell Spike Lee Raye Dowell 40 Acres & A Mule Filmworks Island Pictures
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English Bill Lee
| editing        = Spike Lee
| cinematography = Ernest Dickerson
| awards         =
| budget         = $185,000 
| gross          = $7,137,502 (USA) 
}}
 1986 American written and directed by Spike Lee.  It is Lees first feature-length film.

The film stars Tracy Camilla Johns, Tommy Redmond Hicks and John Canada Terrell. Also appearing are cinematographer Ernest Dickerson as a Queens resident and, in an early appearance, S. Epatha Merkerson as a doctor.

==Plot== model Greer Childs (John Canada Terrell); and the immature, motor-mouthed Mars Blackmon (Spike Lee). Nola is attracted to the best in each of them, but refuses to commit to any of them, cherishing her personal freedom instead, while each man wants her for himself.

==Themes==
Nola idealizes having what men in society have—multiple sex partners—which symbolizes her as an individual struggling against the group. “A woman (or, at least Nola) can be a sexual being, doesn’t have to belong to a man, and perhaps shouldn’t even wish for such a thing.”  Above all, Nola’s voice is the most revolutionary element in the film, a representation of African American womens struggle in society at the time. 

==Background== American cinema.

The New York Times wrote that the film "ushered in (along with Jim Jarmuschs Stranger Than Paradise) the American independent film movement of the 1980s. It was also a groundbreaking film for African-American filmmakers and a welcome change in the representation of blacks in American cinema, depicting men and women of color not as pimps and whores, but as intelligent, upscale urbanites." 

The film was shot in twelve days during the summer of 1985 on a budget of $175,000 and grossed $7,137,502 at the U.S. box office. 

The film served as a turning point for the Brooklyn neighborhood where it was filmed. Lee portrayed the neighborhood as a vibrant cosmopolitan community where successful African Americans thrived, focusing not only on Nola and her struggles, but also on local children, residents and graffiti, revealing the struggles of the neighborhood and the people in it to the world. A public park was used for the setting of much of the movie. This public space is made to feel like a comfortable place for the characters, serving to encourage others to investigate public spaces in the area and creating a link with viewers in other places who had similar thriving public spaces of community importance. 
After the movie was released media attention was drawn to Brooklyn, from which a flood of artists and musicians began emerging. 

==Cast==
{| style="width:100%;"
|-
|  style="vertical-align:top; width:50%;"|
*Tracy Camilla Johns — Nola Darling 
*Tommy Redmond Hicks — Jamie Overstreet 
*John Canada Terrell — Greer Childs 
*Spike Lee — Mars Blackmon
*Raye Dowell — Opal Gilstrap 
*Joie Lee — Clorinda Bradford 
*Dennis Karika — The Trainer
*S. Epatha Merkerson — Doctor Jamison Bill Lee — Sonny Darling
|  style="vertical-align:top; width:50%;"|
*Erik Dellums — Dog 3
*Reginald Hudlin — Dog 4
*Ernest Dickerson — Dog 8
*Fab Five Freddy — Dog 10
*Scott Sillers — Dog 11
*Geoffrey L. Garfield — Dog 12
|}

==Reception==

===Awards and nominations=== 1986 Cannes Film Festival
*"Award of the Youth" Foreign Film  — Spike Lee (won)
 1986 Los Angeles Film Critics Awards
*"New Generation Award" — Spike Lee (won)
 1987 Independent Spirit Awards
*Best First Feature  — Spike Lee (won)
*Best Female Lead — Tracy Camilla Johns (nominated)

In 2014, Lee said that his one regret as a filmmaker was the rape scene in Shes Gotta Have It: "If I was able to have any do-overs, that would be it. It was just totally ... stupid. I was immature. It made light of rape, and that’s the one thing I would take back. I was immature and I hate that I did not view rape as the vile act that it is. I can promise you, there will be nothing like that in Shes Gotta Have It, the TV show  , thats for sure." 

==Home media==
Shes Gotta Have It was released on VHS tape. It was later released for DVD in North America on January 15, 2008, by 20th Century Fox Home Entertainment through United Artists and MGM. Despite the films availability on DVD in the United Kingdom, the DVD release for Region 1 took longer than expected. 

In the mid-1990s, The Criterion Collection released the film on laserdisc. According to Lees agent, the film was to be eventually released on DVD. But Jonathan Turell of The Criterion Collection ended that rumor, saying "No for Shes Gotta Have It. We dont have DVD rights." 

It was the first movie to air on This TV when it launched on November 1, 2008, excluding some stations that started offering the network on the day before it aired. 

In 2010 the film was digitized in High Definition (1080i) and broadcast on MGM HD.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 