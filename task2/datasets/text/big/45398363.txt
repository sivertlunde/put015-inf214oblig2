Ladki Sahyadri Ki
{{Infobox film
| name           = Ladki Sahyadri Ki
| image          = Ladki_Sahyadri_Ki.jpg
| image size     =
| caption        = 
| director       = V. Shantaram
| producer       = V. Shantaram
| writer         = 
| narrator       = Sandhya Shalini Abhyankar Vatsala Deshmukh Kumar Dighe
| music          = Vasant Desai
| cinematography = V. Avadhoot
| editing        =
| studio         = Rajkamal Kalamandir
| distributor    =
| released       =  
| runtime        = 151 minutes
| country        = India
| language       = Hindi
| budget         =
| preceded by    =
| followed by    =
}} 1966 Hindi social drama film directed by V. Shantaram. Also called Iye Marathichiye Nagari (The Land of Maharashtra) in Marathi language|Marathi, it was a bilingual for V. Shantaram productions under the Rajkamal Kalamandir banner.    The music was composed by Vasant Desai, with lyrics by Bharat Vyas.  The cast included Sandhya, Shalini Abhyankar, Vatsala Deshmukh, Kumar Dighe, Keshavrao Date, and Baburao Pendharkar. 

The story revolves around Rani, a stage performer from an impoverished family, and her attempts to help repair a temple in her village.

==Plot==
Rani (Sandhya) and her family are poor but she is not allowed to work when offered a job in a theatrical company, as it is against the custom of her family to have girls working. However she takes on the job when the village temple needs money for repairs. The stage-owner starts drinking and makes inappropriate advances to Rani. When she snubs him he stops the work on the temple restoration. Rani is separated from her family but finally reunites with them and has the temple completed.

==Cast==  Sandhya as Rani
* Prabhakar Pansikar as Som Dutt
* Kumar Dighe as Balasab
* Ganesh Solanki
* Keshavrao Date
* Shalini Abhyankar
* Vatsala Deshmukh
* Baburao Pendharkar
* Manjiri Gadkar

==Soundtrack==
The music composed by Vasant Desai, had a bhajan in the voice of Pandit Jasraj "Vandana Karo Archana Karo" in Raga Ahir Bhairav. Pandit Jasraj sang only four songs in four films, Ladki Sahayadri Ki (1966), Birbal My Brother (1975) and 1920 (film)|1920 (2008) and Anant Mahadevans Gour Hari Dastaan (2013). He had, however, composed music for Mira Nairs Salaam Bombay (1988).   

The back ground music was also by Vasant Desai who had parted ways with Shantaram following Navrang. However, on Shantarams request he returned to compose music for Geet Gaya Pattharon Ne and Ladki Sahyadri Ki.    The lyricist for Ladki Sahyadri Ki  was Bharat Vyas, and the singers were Pandit Jasraj, Asha Bhosle, Amar Shaikh, Mahendra Kapoor and Suman Kalyanpur.   

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer 
|-
| 1 
| "Vandana Karo Archana Karo".
| Pandit Jasraj
|-
| 2
| Kahan Chale Chhaliya Mera Loot Ke Jiya 
| Asha Bhosle
|-
| 3
| Kyun Lage Laaj Preet Ki 
| Asha Bhosle
|-
| 4
| Meri Jhansi Nahi Doongi 
| Asha Bhosle
|-
| 5
| Tum Mujhe Khoon Do 
| Asha Bhosle
|-
| 6
| Karo Sab Nichhawar Bano Sab Fakir 
| Asha Bhosle
|-
| 7
| Angrezo Ne Jeet Li Jhansi 
| Asha Bhosle, Mahendra Kapoor, Amar Shaikh
|-
| 8
| Shubhra Kamal Jab Aayega 
| Mahendra Kapoor
|-
| 9 Tohri Ungli Se Krishna 
| Suman Kalyanpur
|}
 
==References==
 

==External links==
* 

 

 
 
 
 
 
 