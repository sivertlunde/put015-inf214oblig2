Dark Arc
{{Multiple issues|
 
 
}}

{{Infobox film
| name           =   Dark Arc
| image          =   
| image size     =
| caption        =   
| director       =   Dan Zukovic
| producer       =   David Barnett Jeremy Dyson Brendan Keown Mitch Mayer
| writer         =   Dan Zukovic
| narrator       =
| starring       =   Sarah Strange Dan Zukovic Kurt Max Runte
| music          =   Neil Burnett
| cinematography =   Geoff Denham
| editing        =   Stephen Cammarano Patrick Carroll
| studio         =   
| distributor    =   Third Tribe Productions
| released       =   2004
| runtime        =   99 minutes
| country        =   Canada
| language       =   English
| budget         =   $250,000 CAD (estimated) 
| gross          =   
| preceded by    =
| followed by    =
}}
Dark Arc, premiered in  2004, is the second independent feature film by the writer/director/actor Dan Zukovic.      

==Reception==
Dark Arc premiered at the Montreal World Film Festival in 2004 and was shown at other festivals including Cinequest Film Festival|Cinequest, The Calgary International Film Festival, The Edmonton International Film Festival, The Quebec City International Film Festival, The Charlotte Film Festival, The Portobello Film Festival, the Nickel Independent Film and Video Festival and the American Cinematheque Festival of Film Noir.

Called "Absolutely brilliant...truly and completely different...something youve never tasted before" by the  , Sarah Strange, was nominated for "Best Lead Performance - Female in a Feature Length Drama" at the 2005 BC Film Industry Leo Awards for her role in the film.

In August 2010, the film was released on DVD by Vanguard Cinema.

==References==
 

 
 
 


 