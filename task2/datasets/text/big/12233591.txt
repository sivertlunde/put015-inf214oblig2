Frightmare (1983 film)
 
 
{{Infobox film
| name = Frightmare
| image = Frightmareposter.jpg
| caption = Theatrical release poster
| director = Norman Thaddeus Vane 
| producer = Henry Gellis Hedayat Javid Callie Wright Patrick M. Wright
| writer = Norman Thaddeus Vane
| starring = Ferdy Mayne Luca Bercovici Nita Talbot Leon Askin Jeffrey Combs Jennifer Starrett
| music = Jerry Mosely
| cinematography = Joel King
| editing = Doug Jackson
| studio = 
| distributor = Troma Entertainment
| released = 1983
| runtime = 90 minutes
| country = United States
| language = English
| budget =
}}
Frightmare (also known as The Horror Star and Body Snatchers) is a 1981 American  .

The film follows a group of drama students who decide to kidnap the corpse of a recently deceased horror-movie star. By disrupting his tomb, they release an ancient black magic that begins consuming them one by one. It is currently distributed by Troma Entertainment.

== Plot ==
A group of drama students idolize their favourite horror film star, Conrad Razkoff. In the beginning, Conrad is acting in a commercial for dentures, and the Director stops the filming because he does not like Conrads performance. While the director sits on the edge of the balcony, an angry Conrad pushes him off with his cane. Later, Conrad visits a school and talks about his performances, only to faint under excitement. One of the Drama students, Meg, revives him. Later, as Conrad sits in his bed, his obese director visits him. After a long talk about his death arrangements, Conrad closes his eyes and tricks the director into believing he is dead. Then, the director denounces him until Conrad springs up and smothers him with a pillow. After Conrad dies, the seven drama students, Meg, Saint, Bob, Eve, Donna, Oscar, and Stu, go to the cemetery after dark, they sneak into the tomb where Conrads coffin resides. The lights turn on and they see a film of Conrad stating that he welcomes them into his tomb, unless,he says, they have broken in. Creeped out, Meg asks them not to take Conrad, but they do, and take him to an old mansion where they stay for the night.

That night, Conrads coffin explodes, and he rises from the dead. Later, Oscar and Donna are having sex but Donna says she is scared. Oscar goes to investigate and has his tongue ripped out by Conrad in the attic. Donna becomes worried and walks outside only to see Conrad, who uses black magic to set her on fire. Later, the five remaining teens realize that Oscar and Donna are missing and begin to look for them. Bob however is put in a trance by Conrad and walks to Conrads tomb, only to suffocate from vapors that are released inside the crypt. Eve decides to watch a movie and is lured out of her room by the sound of Donnas voice, only to be smashed into the wall by Conrads coffin, and hidden with Oscars body.

Meg, Saint, and Stu realize that the other two are now missing and begin to panic, and Meg decides shes going to tell the police. Stu runs upstairs to get a flashlight since Saints car will not start, only to be decapitated by Conrad who then sends his severed head out onto the front lawn of the mansion. Meg and Saint venture outside looking for Stu and find Donnas burnt body, and hurry back inside. Saint decides to try to fix his car, while Conrad traps Meg in the attic, where she impales him with a cross. Saint safely hides her unconscious body and takes Conrads body with him to put back in the crypt, while the police arrive to the mansion finding a delirious Meg and all her dead friends. At the cemetery, Conrad attacks Saint and puts him in the crematorium, and goes back to his grave. A while later, his wife and his psychic friend arrive to say their final goodbyes to the "dead" Conrad, only Conrad kills his psychic after she steals his jewelry from his body, and his wife locks the crypt for forever. The final scene shows a video being played on the television installed in the crypt of Conrad informing the audience that hell is actually quite pleasant and hopes that more respect will be shown for the dead.

== External links ==
*  

 
 
 
 
 
 