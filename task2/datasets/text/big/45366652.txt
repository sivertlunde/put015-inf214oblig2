Haddina Kannu
{{Infobox film
| name           = Haddina Kannu
| image          = 
| image size     = 
| caption        = 
| director       = A. V. Seshagiri Rao
| producer       = P. Krishna Raj
| story          = L. S. Iyer
| screenplay     = Anjaneya Pushpanand
| narrator       = Manjula Lokesh Srinath Satyam
| cinematography = V. K. Kannan
| editing        = Yadav Victor
| studio         = Mohan Murali Productions
| distributor    = 
| released       =  
| runtime        = 143 min.
| country        = India
| language       = Kannada
}}
 Kannada film Manjula in lead roles.  The film was scripted and presented by L. S. Iyer. 

==Cast==
* Shankar Nag  Manjula
* Srinath
* Lokesh
* Dwarakish
* Sundar Krishna Urs
* Musuri Krishnamurthy
* Tiger Prabhakar
* Jayamalini
* Vajramuni
* K. S. Ashwath
* Jyothi Lakshmi
* Pramila Joshai

==Box Office==
Haddina Kannu was a Super Hit at the box office.

==Soundtrack== Satyam with lyrics penned by Chi. Udaya Shankar.
{{Infobox album	
| Name = Haddina Kannu
| Longtype = to Haddina Kannu
| Type = Soundtrack	 Satyam	
| Cover = 
| Border = Yes	
| Alt = Yes	
| Caption = 
| Released = 1980
| Recorded = 
| Length =  Kannada 
| Label = 
| Producer = 
| Last album = 
| This album = 
| Next album = 
}}

===Tracklist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Ee Cheluvina Olavina"
| S. P. Balasubrahmanyam, P. Susheela
|-
| 2
| "Kannage Aaseya"
| S. P. Balasubrahmanyam, S. Janaki
|-
| 3
| "Jeevanavemba Rahasya"
| S. P. Balasubrahmanyam, S. Janaki
|-
| 4
| "Sumne Alla Sikkodu"
| S. Janaki
|-
| 5
| "Ee Daaha Bahala"
| S. Janaki
|-
|}

==Reference==
 

==External links==
*  
*  
*  


 
 
 
 


 