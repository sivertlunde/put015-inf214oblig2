Women in War
 
{{Infobox film
| name           = Women in War
| image          = Womenwarpos.jpg
| caption        = Film poster
| director       = John H. Auer
| producer       = Sol C. Siegel
| writer         = Doris Anderson F. Hugh Herbert
| starring       = Wendy Barrie Elsie Janis
| music          = Cy Feuer
| cinematography = Jack A. Marta
| editing        = 
| distributor    = Republic Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         = 
}}
 Best Visual William Bradford, Ellis J. Thackery, Herbert Norsch).   

==Plot==
Socialite Pamela Starr meets Mr Tedford, an older man in a London night club.  After he escorts her home he tries to enter her flat feeling he has deserved the right to sleep with her as he has paid for her entertainment.  Pamela thrusts a £5 note in his hands as reimbursement and attempts to enter her room but Tedford wont let her.  The spirited Pamela strikes the drunken Tedford sending him across the landing where he crashes through a railing over the stairwell sending Tedford to his death.

The ensuing court case doesnt go well for Pamela as her playgirl lifestyle is paraded as evidence against her and to Pamelas surprise Mr Tedford was actually a British Captain on leave from the war front.  Watching the trial is Matron ONeil who was formerly Pamelas mother until she divorced her husband and left to go nursing around the troubled world to help those in need. Pamela had never known her mother and her late libertine father had denied her moral leadership and discipline in raising her. ONeil and Pamelas defence solicitor concoct an arrangement where Pamela wont be charged with Tedfords death if she volunteers to be an Army nurse in France.  Pamela is assigned to a VAD Detachment led by Matron ONeil with Pamela still unaware that ONeil is her mother.
 RAF falls in love with Pamela. The tensions of the nurses continue as their detachment is sent to a dangerous area of the battle line.

==Cast==
* Elsie Janis as Matron ONeil, formerly Mrs. Starr
* Wendy Barrie as Pamela Starr
* Patric Knowles as Flt Lt. Larry Hall
* Mae Clarke as Gail Halliday
* Dennie Moore as Ginger
* Dorothy Peterson as Sister Frances
* Billy Gilbert as Pierre, the Cobbler
* Colin Tapley as Capt. Tedford, the Masher
* Stanley Logan as Col. Starr
* Barbara Pepper as Millie, Irish Nurse
* Pamela Randell as Phyllis Grant, Nurse
* Lawrence Grant as Sir Gordon, Defense Attorney
* Lester Matthews as Sir Humphrey, Prosecuting attorney
* Holmes Herbert as Chief Justice
* Peter Cushing as Capt. Evans

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 