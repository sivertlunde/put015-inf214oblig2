Tru Love (film)
 
{{Infobox film
| name           = Tru Love
| image          = 
| caption        = Film poster
| director       = Shauna MacDonald Kate Johnston
| producer       = 
| writer         = Shauna MacDonald Kate Johnston
| starring       =  
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Canada
| language       = English
| budget         = 
}} Canadian drama film, released in 2013. Written and directed by Shauna MacDonald and Kate Johnston, the film stars MacDonald as Tru, a commitment-phobic lesbian who is drawn into an emotionally intimate relationship with Alice (Kate Trotter), the mother of her friend Suzanne (Christine Horne). 

The film premiered in October 2013 at the Raindance Film Festival, and has been screened at both LGBT and general interest film festivals in 2014. It won the Best Feature Film Audience Award and the Emerging Canadian Artist Award at the 2014 Inside Out Film and Video Festival.

The film was released in digital formats by  , September 30, 2014.  and will be released on DVD on November 4. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 

 