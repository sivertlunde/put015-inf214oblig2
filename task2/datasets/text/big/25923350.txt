Romance sentimentale
{{Infobox film
| name           = Romance sentimentale
| image          =
| image_size     =
| caption        =
| director       = Grigori Aleksandrov Sergei M. Eisenstein
| producer       = Léonard Rosenthal (producer)
| writer         = Grigori Aleksandrov (writer) Sergei M. Eisenstein (writer)
| narrator       =
| starring       = See below
| music          = Alexis Arkhangelsky
| cinematography = Eduard Tisse
| editing        = Grigori Aleksandrov Sergei M. Eisenstein
| distributor    =
| released       = 12 September 1930
| runtime        = 20 minutes
| country        = France
| language       =
| budget         =
| gross          =
}}

Romance sentimentale is a 1930 French film directed by Grigori Aleksandrov and Sergei M. Eisenstein. The film is also known as Sentimental Romance (International English title).

== Synopsis == montage of scenes of elemental violence—crashing waves and falling trees alternate with images of trees speeding past as if viewed from a motorcar. The imagery gradually changes to more tranquil vistas of clouds, grass swaying in a breeze, and rippling water. The first interior shot shows a woman silhouetted against a window. There are several shots of a fireplace and of clocks and their pendulums. The woman moves from the window to a piano, and begins singing a Russian song. Partway through her song, she is surrounded by starbursts and the dark, indoor setting of the scene is replaced by radiant clouds. Images of swans are intercut with images of Rodin sculptures. Eventually the indoor setting returns and rain is seen falling against the night sky. After an interval, the sun is shown moving through the sky, and the singer finishes her song as flowering nature reappears.

== Cast ==
*Mara Griy as The singer

== External links ==
* 
* 

 

 


 

 
 
 
 
 
 
 