Playing Beatie Bow (film)
 
{{Infobox film
| name = Playing Beatie Bow
| image = PlayingBeatieBowPoster.jpg
| caption = Playing Beatie Bow poster
| director = Donald Crombie
| writer = Peter Gawler (written by) Irwin Lane (writer)   Ruth Park (novel) Based on the novel by Ruth Park
| starring = Peter Phelps Imogen Annesley Mouche Phillips Nikki Coghill Moya OSullivan
| producer = Jock Blair Bruce Moir John Morris
| music = Garry McDonald / Laurie Stone
| cinematography = Geoffrey Simpson
| editing = A.J. Prowse
| distributor = South Australian Film Corporation
| released =  
| runtime = 93 minutes
| country = Australia
| language = English
| budget = A$4.4 million Greg Kerr, "Playing Beatie Bow", Australian Film 1978-1992, Oxford Uni Press, 1993 p203 |
| gross = A$97,036 (Australia)
}} 

Playing Beatie Bow is a 1986 Australian drama film directed by Donald Crombie. The screenplay by Peter Gawler and Irwin Lane is based on the novel by Ruth Park.

==Plot summary==
Beatie Bow, a young Victorian-era girl, is summoned from the past to contemporary Sydney by children chanting her name. 14-year-old Abigail Kirk follows her back to 1873, in the colony of New South Wales.  Beaties family believe Abigail is the promised Stranger who will arrive to save The Gift for future generations of Bows. Abigail is trapped in the past until she does what she was sent to do, even though she doesnt know what this is. During her sojourn, she falls in love for the first time and gains a more mature perspective on her parents relationship.

==Cast==
*Imogen Annesley - Abigail
*Peter Phelps - Judah/Robert
*Mouche Phillips - Beatie Bow
*Nikki Coghill - Dovey
* Moya OSullivan - Granny
* Don Barker - Samuel
* Trent Graham - Punchy
*Lyndel Rowe - Kathy
* Barbara Stephens - Justine
* Damian Janko - Gibbie
* Phoebe Salter - Natalie

==Production== PG instead G   because Abigail says "Oh, shit" toward the end of the film. 

Most of the movie was shot in Adelaide. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p340  $400,000 was spent on recreating Sydneys Rocks area in a disused industrial site. 

==Box Office==
Playing Beatie Bow grossed $97,306 at the box office in Australia,  which is equivalent to $212,127 in 2009 dollars. However the movie was popular on video.  

==See also==
* Cinema of Australia
* List of Australian films
* South Australian Film Corporation

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 