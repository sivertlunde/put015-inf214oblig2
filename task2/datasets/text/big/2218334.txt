The Toll of the Sea
{{Infobox film image           = Anna May Wong holds child in The Toll of the Sea.jpg| name            = The Toll of the Sea director  Chester M. Franklin producer  Herbert T. Kalmus Technicolor writer          = Frances Marion starring        = Anna May Wong Kenneth Harlan Beatrice Bentley music           = Irving Berlin  Louis Silvers   cinematography  = J.A. Ball editing         = Hal C. Kern distributor     = Metro Pictures released        =   runtime         = 53 min. country         = United States language        = English budget          =
|}}
  Chester M. Technicolor Motion Metro Pictures, and featuring Anna May Wong in her first leading role.
 Sidney Franklin), with the lead roles played by Anna May Wong and Kenneth Harlan. The plot was a variation of the Madama Butterfly story, set in China instead of Japan.

==Plot==
When young Lotus Flower sees an unconscious man floating in the water near the seashore, she quickly gets help for him. The man is Allen Carver, an American visiting China. Soon the two have fallen in love, and Carver promises to take her with him when he returns home. But Carvers friends discourage him from doing this, and he returns to the USA alone.

By the time the two of them meet again a few years later, much has changed: Lotus Flower has a young son by Carver, but he has returned to China with a wife. Lotus Flower is reluctantly persuaded that her son would be better raised with his father in America. After they leave with the boy, Lotus Flower wades into the sea and drowns. The film ends with a title stating FINIS.

==Cast==
*Anna May Wong as Lotus Flower
*Kenneth Harlan as Allen Carver
*Beatrice Bentley as Barbara Elsie Carver
*Priscilla Moran as Little Allen (as Baby Moran)
*Etta Lee as Second Gossip
*Ming Young as First Gossip

==Production and preservation==
The film was the second Technicolor feature, the first color feature made in Hollywood, and the first color feature anywhere that did not require a special projector to be shown. The original camera negative survives except for the final two reels. The film premiered on 26 November 1922 at the Rialto Theatre in New York City, and went into general release on 22 January 1923. 
 lost in 35mm nitrate Technicolor Process 2, which involved cementing together two film strips base to base, the resulting image quality is better than the original prints appeared.  

Because the Technicolor camera divided the lens image into two beams to expose two film frames simultaneously through color filters, and at twice the normal frames per second, much higher lighting levels were needed. All scenes of The Toll of the Sea were shot under "natural light" and outdoors, with the one "interior" scene shot in sunlight under a muslin sheet.

==Availability==
The restored version is available as one of the titles included in the 4-DVD box-set Treasures from American Film Archives, 50 Preserved Films. 

==References==
 

==External links==
 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 