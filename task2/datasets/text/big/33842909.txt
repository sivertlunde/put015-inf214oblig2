Odds and Evens (film)
{{Infobox film
| name = Odds and Evens -Pari e dispari-
| image = Bud Spencer Odds And Evens.jpg
| caption =
| director = Sergio Corbucci
| writer = Mario Amendola, Bruno Corbucci
| starring = Terence Hill Bud Spencer
| music = Guido De Angelis Maurizio De Angelis
| cinematography =
| editing =
| producer =
| distributor =
| released =  
| runtime = 105 minutes
| awards =
| country = Italy United Staets
| language = Italian
| budget =
}}  Italian action-comedy movie directed in 1978 by Sergio Corbucci. 
 Golden Screen Award along with Superman (1978 film)|Superman and Close Encounters of the Third Kind.  

== Plot ==
Johnny Firpo, a Navy Soldier, is competing in many competitions in Miami, including American Football and Speedboat Racing. Charlie instead is a truck driver making deliveries in various places and ultimately must carry a tank full of dolphins. The two characters meet when some miscreants sabotage Johnnys speedboat. He thinks that it was Charlie who was eating a plate of beans, but in the end they discover that the real culprits are a gang of illegal bookmakers, led by a man known as Paraboulis The Greek, who are controlling and rigging every gambling competition in Florida. The Navy asks Johnny to infiltrate and dismantle the gang of bookmakers, giving him the tip to seek help of former gambler Charlie Firpo. Johnny eventually meets Charlie and proposes to work with him, but first Charlie disagrees because his acquaintance seems a clever, witty trickster and for his simple taste. In the end, however, the two combine to sabotage the gangsters plots with gleeful fisticuffs in the ensuing melee. Amongst this backdrop, Charlie, in despair, finds out he is the illegitimate brother of Johnny during an escapade of his father - still alive unbeknownst to Charlie - who pretends to be blind in order to collect a pension. After Johnny was passed off as a runner with the horses and Charlie took part in a second meeting, the two brothers find out which houses the boss of the gang of bookmakers and Johnny will have to play with him and his henchmen in a poker game. Johnny wins but the boss does not intend to make it go away, so Charlie and Johnny greet the members of the band with a new and more powerful punch and brawl.

== Cast ==
*Terence Hill: Johnny Firpo
*Bud Spencer: Charlie Firpo
*Luciano Catenacci: Paraboulis il greco
*Marisa Laurito: Suor Susanna		
*Salvatore Borgese: Ninfus
*Riccardo Pizzuti: Smilzo
*Claudio Ruffini: Picchio
*Sergio Smacchi: Mancino
*Vincenzo Maggio: Tappo
*Woody Woodbury: Admiral OConnor 

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 
 