Flaming Brothers
 
 
{{Infobox film
| name           = Flaming Brothers
| image          = FlamingBrothers.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 江湖龍虎鬥
| simplified     = 江湖龙虎斗
| pinyin         = Jiāng Hú Lóng Hǔ Dòu
| jyutping       = Gong1 Wu4 Lung4 Fu2 Dau3 }}
| director       = Joe Cheung
| producer       = Alan Tang
| writer         = 
| screenplay     = Wong Kar-wai
| story          = 
| based on       = 
| narrator       = 
| starring       = Chow Yun-fat Alan Tang Pat Ha Jenny Tseng Patrick Tse James Yi Philip Chan Norman Chu Violet Lam Stephen Shing The Melody Bank Bruton Music
| cinematography = Jingle Ma
| editing        = Poon Hung
| studio         = In-Gear Film Golden Harvest
| released       =  
| runtime        = 102 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$15,741,778
}}
 1987 Cinema Hong Kong crime romance film directed by Joe Cheung and starring Chow Yun-fat, Alan Tang, Pat Ha and Jenny Tseng. The film was shot in Hong Kong, Macau and Thailand.

==Plot==
Alan Chan (Alan Tang) and Cheung Ho Tin (Chow Yun-fat) are orphans wandering in the streets of Macau who became sworn brothers together, they make a living by stealing. One day, while Tin was stealing food in a church, he is discovered by Ho Ka Hei (Pat Ha), who encourages him not to steal and subsequently brings food to him and his friends everyday until one day, when Ka Hei was adopted and the two farewell in tears. Alan and Tin struggles very hard and finally succeeds and become triad leaders. During a gang warfare, Alan and Tin kill Chiu, the underling of a major Macau triad leader Ko Lo Sei (Patrick Tse). Ko mobilizes his men to seek revenge but the strong-willed Alan strikes back. Then, the cunning Ko pretends to reconcile with Alan and provides news for Alan to traffick arms in Thailand. However, the arms dealer turns out to be Kos rival Uncle Pui and Alan nearly lost his life. With his extraordinary courage, Alan gains Puis trust and successfully makes business. At the same time, Alan also meets Macau singer Jenny and falls in love with her and brings her back to Macau. While Alan was highlighting danger in Thailand, Tin re-encounters Ka Hei in a Catholic school and after lay each others heart bare, they get engaged. However, Ka Hei requests Tin to leave the underworld and wants to lead a peaceful life in Hong Kong. Tin summons his courage to tell Alan about this, but Alan, while preparing big business with Tin, becomes enraged. Having to choose between his lover and his brother, Tin sadly leaves Alan. When Ko discovers that Alan safely returned from Thailand, he falls out with him and annexes all his purchased arms, formally declaring war with Alan. Later, Alan falls into Kos trap and becomes encircled where his underlings are fully wiped out and Jenny is also killed from protecting him. Alan decides to single-handedly deal with Ko. In Hong Kong, Tin hears news of it and leaves his beloved wife and returns to Macau to fight with Ko alongside Alan in a gunfight where after a fierce battle, the trio dies together.

==Cast==
*Alan Tang as Alan Chan
*Chow Yun-fat as Cheung Ho Tin
*Pat Ha as Ho Ka Hei
*Jenny Tseng as Jenny
*Patrick Tse as Ko Lo Sei
*James Yi as Richard Lui
*Philip Chan as Police Commissioner Chan
*Norman Chu as Chiu
*Fong Yau as Uncle Pui
*Wong Kim Fung as Tai Fung
*Lau Shung Fung as Sai Fung
*Tam Yat Ching as Uncle Mosquito
*Tam Chuen Hing as Triad boss at negotiation table
*Kam Biu as Triad boss at negotiation table
*Yue Man Wa as Triad boss at negotiation table
*Ko Hung as Brother Hung
*Pa San as Kos bodyguard
*Steve Mak as Kos bodyguard
*Hung San Nam as Kos bodyguard
*Tang Tai Wo as Kos bodyguard
*Kan Tat Wah as Inspector Law
*Soh Hang-suen as Sister Lucia
*Felix Lok as Man at convenience store
*Joe Cheung as Alans man outside Chius pub
*Chun Wong as Street vendor
*Cheung Chok Chow as Bread street vendor
*Jeffrey Ho as Mr. Wong
*Sai Kwa Pau as Resident of old folks home
*Cheung Hei as Resident of old folks home
*Chin Tsi-ang as Resident of old folks home
*Sze To On as Resident of old folks home
*Chan Lap Ban as Resident of old folks home
*Stanley Tong as Hitman in Thailand
*Sam Wong as Hitman in Thailand
*Alex Ng as Chius man
*Wo Seung as Chius man
*Wong Chi Ming as Chius man
*Sing Yan as Brother Hungs man
*Yiu Man Kei as Kos man
*Choi Kwok Keung as Kos man
*Poon Kin Kwan as Kos man
*Tang Chiu Yan as Kos man
*Chan Ming Wai as Alans man
*Chun Kwai Bo as Alans man
*To Wai Wo as Kos horse trainer
*Ho Chi Moon as Club customer
*Wong Ka Leung
*Wong Chi Keung

==Theme song==
*I Am Just a Person (我祇是個人) Violet Lam
**Lyricist: Calvin Poon
**Singer: Su Rui

==Box office==
The film grossed HK$15,741,778 at the Hong Kong box office during its theatrical run 30 July to 18 August 1987 in Hong Kong.

==See also==
*Chow Yun-fat filmography

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 