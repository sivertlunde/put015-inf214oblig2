Ayya (2005 Kannada film)
 
{{Infobox film
| name           = Ayya
| image          = 
| director       = Om Prakash Rao
| producer       = Byre Gowda
| story          = Om Prakash Rao
| starring       = Darshan Thoogudeep Rakshitha
| music          = V. Ravichandran
| cinematography = Anaji Nagaraj
| editing        = S. Manohar
| studio         = Bhavya Cine Creations
| released       =  
| country        = India
| language       = Kannada
}} Kannada film directed by Om Prakash Rao, starring Darshan Thoogudeep and Rakshitha. Darshan plays the role of a brave police officer prepared to go any lengths to control crime in the city. The background music and soundtrack has been scored by V. Ravichandran.
 Vikram And Trisha Krishnan.

==Summary==
The dialogue "Naan convent alli odi police agiddalla. Corporation school alli odi police aagiddu" (Means, "I have not become a police officer by studying in convent, I am a police officer who studied in corporation school"), became very popular in the whole of Karnataka and could be heard from Darshans fans for many days. The film has some very exciting scenes such as the one where the police commissioner describes Ayya as a one man army, who is more powerful than the nuclear bomb (Pokhran-II) invented by A. P. J. Abdul Kalam|Dr. Abdul Kalam. The film also a scene where he intelligently kills some of the villains showing that Ayya is not only a brave police officer, but also a very very intelligent and brainy person.

==Cast== Darshan
* Rakshita
* Avinash
* Srinagar Kitty
* Hema Chowdhury
* Dharma
* Sadhu Kokila
* Shobhraj

==Soundtrack==
{{Infobox album  
| Name = Ayya
| Type = soundtrack
| Artist = V. Ravichandran
| Cover =
| Released = 2004
| Recorded = Feature film soundtrack
| Length =
| Label =
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) || Duration
|-
| 1 || Kettode Kettode || L. N. Shastry, Suma Shastry || 03:59
|-
| 2 || Tabla Tabla  || Udit Narayan, Anuradha Sriram || 04:52
|-
| 3 || Simha Gharjane || Hemant Kumar || 04:46
|-
| 4 || Surya Jothe || L. N. Shastry || 04:37
|-
| 5 || Ee Prema || Udit Narayan, Archana Udupa || 04:26
|-
| 6 || Rakshita Rakshita || Badri Prasad, Manjula Gururaj || 04:00
|-
|}

==Box Office==
The film enjoyed box office success, with many cinemas running it for over 100 days. 

==References==
 

 

 
 
 
 
 


 

 