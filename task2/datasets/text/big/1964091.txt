Next (2007 film)
 
{{Infobox film
| name           = Next
| image          = Next poster.jpg
| caption        = Promotional poster
| alt            = 
| director       = Lee Tamahori
| producer       = Nicolas Cage Todd Garner  Norm Golightly Graham King Arne Schmidt Gary Goldman Jonathan Hensleigh Paul Bernbaum
| story          = Gary Goldman (screen story)
| based on       =  
| starring       = Nicolas Cage Julianne Moore Jessica Biel Thomas Kretschmann Tory Kittles Peter Falk
| music          = Mark Isham
| cinematography = David Tattersall
| editing        = Christian Wagner
| studio         = Revolution Studios Initial Entertainment Group Virtual Studios Saturn Films
| distributor    = Paramount Pictures
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $70 million
| gross          = $76,066,841   
}} action Thriller thriller film directed by Lee Tamahori and stars Nicolas Cage, Julianne Moore and Jessica Biel. The films original script was very loosely based on the science fiction short story "The Golden Man" by Philip K. Dick. The film was released on April 27, 2007.

==Plot== Las Vegas, where he supplements his income with gambling, using his powers to win against the house.
 FBI agent Las Vegas police. Ferris tracks Cris to his home, but he escapes after foreseeing her arrival. Later that night, the casinos security chief is approached by two of the terrorists, is interrogated about Johnson and is then killed.

The following morning, Cris is at the diner again when he sees Liz Cooper (Jessica Biel), the woman from his vision. It turns out that not only can Cris see the future, he can see how his actions can affect that future. After envisioning different approaches—all of which fall flat—he  intervenes when Lizs ex-boyfriend arrives. Knowing that she is heading for Flagstaff, Arizona, Cris charms her into giving him a ride. Ferris follows, while the terrorists decide to kill him. A washed-out road forces Cris and Liz to spend the night in a motel. With the weapon tracked to Los Angeles, Ferris convinces her superiors to let her bring Cris in. The terrorists follow in the hope that the agents will lead them to Cris.

Ferris confronts Liz near the hotel. Claiming Cris is a dangerous psychopathy|sociopath, she asks her to drug Cris so that they can bring him in peacefully. Instead, Liz warns Cris, who tells her his secret. When she asks why he will not help the FBI stop the terrorists, he explains his limitations, noting the exception for events involving her. Asking for Liz to wait for him, he tries to escape from the FBI agents but is captured after saving Ferris from logs tumbling down the side of a mountain. Unable to kill Cris, the terrorists kidnap Liz instead.

In custody, Cris is strapped to a chair with his eyes held open and forced to watch television until he has a vision that can help the FBI. They expect him to see a report about the detonation of the bomb, but instead he envisions a broadcast from several hours in the future in which Liz is killed by a bomb while strapped to a wheelchair as bait for him. Cris escapes from captivity and races to the parking garage where she will be killed. Pursuing him to the garage, Ferris promises to help save her as long as Cris will help stop the bomb; she also sets up a plan to draw out the terrorists. 

Using his ability, Cris helps the FBI track the terrorists to the port where they are based. When they arrive, Cris is able to walk right up to the terrorist leader and avoid being hit, by seeing where the bullets will go and dodging them. After killing the terrorists and saving Liz, they find that the bomb has already been moved. Ferris shows Cris a seismograph, hoping that he will see any tremors caused by explosions before they happen. As he stares at the screen he realizes that he has made a mistake and that he was too late: the bomb detonates out at sea and completely destroys the port, as well as the rest of the city.

The timeline reverts to Cris and Liz in bed at the hotel in Arizona, before Liz goes outside to be confronted by Ferris. Because of Lizs involvement in events, Cris has been able to envision everything that could happen leading to the nuclear explosion. "Every time you look into the future, it changes."

Cris calls Ferris and offers to help prevent the nuclear disaster, then asks Liz to wait for him.

==Cast==
* Nicolas Cage as Cris Johnson / Frank Cadillac
* Julianne Moore as Callie Ferris
* Jessica Biel as Liz Cooper
* Thomas Kretschmann as Mr. Smith
* Tory Kittles as Cavanaugh
* Jose Zuniga as Security Chief Roybal
* Jim Beaver as Wisdom
* Jason Butler Harner as Jeff Baines
* Michael Trucco as Kendall
* Enzo Cilenti as Mr. Jones
* Laetitia Daniel as Miss Brown
* Nicolas Pajon as Mr. Green
* Peter Falk as Irv
* Sergej Trifunovic

==Production==
Gary Goldman and Jason Koornick initially optioned the science fiction short story "The Golden Man" by Philip K. Dick. Goldman wrote a script treatment that he and Koornick presented to Nicolas Cages production company, Saturn Films, but Goldman ended up writing the screenplay on spec.

===Original draft===
 
This first draft had more similarities to the short story, detailing the efforts of a government agency to capture and contain a precognitive mutant. 

To provide greater interaction between the opposing parties (as well as create a leading role), Cris was changed from a feral animal whose existence threatened humanitys into a more familiar and understandable social outcast. A romantic subplot was added: the character of Liz Cooper, who in this draft was not only destined to be the love of Criss life, but a mutant as well (born in Love Canal) and the only woman he has ever met with whom he can have children, herself incapable of procreating with normal humans.

As the original short story had a distinct tone of racist paranoia, the motivation for the pursuit of Cris was changed from an ironclad policy of exterminating mutations to a manipulative Department of Homeland Security (DHS) agents obsessive search for unconventional assets in the war on terror.

This script was filled with anti-authoritarian themes, with Cris often speaking clearly and eloquently of how he enjoys his independence. He states plainly that "what I want is freedom. And you don’t get it by giving it up."
 arms and legs, a suggestion his colleague considers humorous.

However, his enemies are skilled tormentors and eventually they drive him to the breaking point: when the DHS learns that Liz is pregnant with his child, they coldly decide to have her executed at a pre-determined time, thus pre-emptively proving to Cris their determination to possess him. He attempts to secure Lizs safety, but the DHS captures her. In retaliation, he demolishes the Las Vegas DHS headquarters with a barrel of C-4 (explosive)|C-4 explosive agents had seized earlier in a warrant-less search. He and Liz are the only survivors.

===Saturn re-draft===
Saturn Films had the script extensively rewritten and in the process almost completely eliminated its anti-authoritarian themes. Though Cris remained a meek social outcast, he is somewhat less sympathetic: he is portrayed as arrogant, as well as far more prone to applying violent solutions. The DHSs role was replaced with the Federal Bureau of Investigation (FBI). Despite a scene in which Cris experiences his worst nightmare – spending the rest of his life strapped into a chair with his eyes wedged open – the authorities are portrayed as sympathetic and Cris as uncooperative and belligerent. Their insistence on his obedience is reduced to the point that the authorities offer their assistance in rescuing Liz (whom they neglect to arrest despite her efforts to sabotage Criss capture) from the terrorists. This leads to the films greatest variation from the original draft – a confrontation with the terrorists.  No clue is given as to the motivation for the terrorists detonation of the nuclear device in Los Angeles.  The few terrorists who speak in the film with British, French and German accents, seem to be under the guidance of an unseen leader who has told them that they must kill Cris since "he says that Cris is the only threat to the plan". During the confrontation with the terrorists, Cris willingly supports the FBI with his abilities in a series of sequences similar to those in the original script, only with the authorities as allies instead of antagonists.

This was the script Saturn Films brought to the attention of Revolution Studios. Revolution Studios acquired the screenplay and in November 2004, Revolution Studios hired Lee Tamahori to direct the film, with Cage in the lead role. Filming was to begin in summer 2005.  In December 2005, Moore was cast as the federal agent who seeks people to help prevent future terrorism and uncovers Cages character as a potential candidate.  In November 2005, Initial Entertainment Group negotiated for rights of international distribution of Next, which had a target release date of 2007.  In February 2006, actress Jessica Biel was cast as the love interest of Cages character. 

In May 2006, Starz!|Starz! Entertainments 14-episode reality television miniseries, Looking for Stars, gave 200 contestants the opportunity to earn a speaking role in Next, {{cite news|first=Robert|last=Riddell|url= http://www.variety.com/article/VR1117944072.html 
|title=Looking for action|publisher= Variety (magazine)|Variety|date=2006-05-24|accessdate=2007-02-13}}  which was won by actor Marcus Welch. {{cite news|last=|first=|url= http://www.realitytvmagazine.com/blog/2006/09/marcus_welch_wi.html archiveurl = archivedate = 2007-02-04}} 

===Filming=== Running Springs San Bernardino. In order to make the restaurant look more like a hotel, a facade was attached to the building.  The facade is the section of the motel where Johnson and Liz Cooper were staying. Interior shots were filmed elsewhere. Following the end of production, the facade was removed. However, remnants of the signage placed and the paint works conducted remain intact. The property has been fenced off and a for sale sign has been posted. Running Springs served for scenes shot in the town. Scenes (in which a vehicle was rolled off the side of a cliff) shot in Big Bear Lake were shot at a campground. Due to the terrain located on the side of the cliff the Cliffhanger is located on, the producers decided to finish the scenes at the campground in Big Bear Lake. Paramount Pictures Corporation, Next DVD 

Next originally was to be distributed by Sony Pictures Entertainment (which had a deal with Revolution), set to be released on September 28, 2006, but that studio dumped it in January 2007, and Paramount Pictures subsequently picked it up and released the film on April 27, 2007 . {{cite news|first=Brandon|last=Gray|url= http://www.boxofficemojo.com/news/?id=2302&p=.htm Box Office Mojo | publisher = Amazon.com |date=2007-04-29|accessdate=2007-04-30}} 

==Reception==
===Box office    === Minority Report, Total Recall, A Scanner Darkly.   

===Critical=== 
Next received negative reviews. Rotten Tomatoes gives the film a score of 28% based on 128 reviews (36 "fresh", 92 "rotten"). {{cite web 
| title = Next (2007)
| url = http://www.rottentomatoes.com/m/next/ 
| work= Rotten Tomatoes 
| publisher = Flixster 
| accessdate= 2010-10-24 
}}  
Metacritic gives the film had an average score of 42% based on 23 reviews. {{cite web 
| url= http://www.metacritic.com/film/titles/next 
| title= Next (2007): Reviews 
| work= Metacritic
| publisher = CBS
| accessdate= 2010-10-24 
}} 
 climax as a "stunning cheat."  
James Berardinelli of ReelViews gave the film 2½ out of 4 stars and said parts of the film are "fascinating" and "compelling" but that "the whole thing ends up collapsing under its own weight." Berardinelli said Nicolas Cage "seems to be going through the motions", "Julianne Moore brings intensity to the part of Callie, although the character is incomplete", "Jessica Biel is appealing" but "the character is unfinished", and that "Thomas Kretschmann is unimpressive as a generic 24-style terrorist." He also said "some viewers will feel cheated by what Next does, and its hard to blame them."  
Connie Ogle of the Miami Herald gave the film 2 out of 4 stars and said the film looks like director Lee Tamahori "spent about 12 bucks on his special effects budget." Ogle said the film had a decent premise but "Next begins to seriously embarrass itself and its stars once it rolls to its climax."  
Toronto Star film critic Peter Howell gave the film 1½ stars out of 4 and called it a "colossal waste of time" and said it is "possibly the most egregious befouling of Dicks work to date." Howell said the roles "seem to be cut-and-pasted from other movies", called the film a "straight-to-DVD wannabe", and said the film "has one of the most infuriating endings ever."  

Moira MacDonald of the Seattle Times gave Next 1½ stars out of 4 and said "Late in the movie, Cris shouts at a bad guy, Ive seen every possible ending here. None of them are good for you. Its as if hes talking to the audience, and alas, hes right." and "Julianne Moore spends most of her screen time in Lee Tamahoris confused sci-fi thriller Next looking royally pissed off, like she got tricked into making the movie on a sucker bet. You cant blame her; this films audience is likely to look that way as well by the time the end credits roll."   The Illusionist, The Hours, and Biel seems fully aware she was hired only to provide a few glimpses of cheesecake." Sanford also remarked, "the ending of this film is not just a colossal cheat, its a hard slap in the face to anyone who has invested his or her time in watching it."  
Daniel Eagan of Film Journal International said the film "follows a familiar Hollywood pattern in which a few intriguing ideas are swamped by the demands of a big-budget, star-driven vehicle" and that it "wont add any luster to Nicolas Cages resume." Eagan said "Half of Next is a clever, unpredictable thriller that plays with Dicks customary obsessions with time and reality. The other half is a sloppy, bloated adventure marred by cheesy special effects and some equally cheesy acting" and also that "the script to Next has plenty of  , one or two egregious enough to demand ticket refunds." 
 Groundhog Day-like attempts to woo Jessica Biels character are "hilarious." Moore concluded "Its all so stupid and ends so perfunctorily that you cant call Next good, or even as good as the dopey Déjà Vu (2006 film)|Déjà Vu...but it does score over   in one important criterion. Its just fun." {{cite web 
| date = 2007-04-27 
| first = Roger |last= Moore 
| title = Who Says Preposterous Junk Cant Be Fun? 
| url = http://articles.orlandosentinel.com/2007-04-26/news/0704270464_1_lee-tamahori-cage-understatement 
| pages = 2
| work = Orlando Sentinel
| accessdate = 2010-10-24 
}}  
Wesley Morris of the Boston Globe gave the film 2½ out of 4 stars and called it a "watchably absurd popcorn flick" and that the film "bears almost no resemblance" to the original short story "The Golden Man", the short story it was adapted from. He described Moores performance as "enjoyably curt" and said "alongside Cages spontaneity, Biel seems humorless and earnestly dull." Morris said the film is fun "until it turns crass" and concluded, "when youre being toyed with that cheaply, you forget how much you admire Nicolas Cages shamelessness and start to resent the movies."  Diana Saenger of ReviewExpress gave the film 3½ stars and said "Next boasts a fresh plot with a tricky twist ending that can be misconstrued if you dont pay close attention and then pause to think about it." Saenger reported that it was Nicolas Cages idea for Cris to be a magician, and that it was his suggestion that his wife be part of the scene where a woman comes out of the audience to be part of the magic show. Saenger remarked that people complaining about the twist being a rip-off probably didnt understand it and said it made perfect sense and concluded "I liked the surprise twist and found Next very entertaining." 

The film was subject to the heckling of Bridget Jones Nelson and Michael J. Nelson in an October 2007 installment of Rifftrax. 

===Awards===
Next received two Razzie Award nominations including Worst Actor (Nicolas Cage) and Worst Supporting Actress (Jessica Biel), but lost both categories to Eddie Murphy for Norbit.

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 