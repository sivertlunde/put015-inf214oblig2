Nutty News
{{multiple issues|
 
 
}}

{{Infobox Hollywood cartoon
| image =  
| series = Looney Tunes Robert Clampett
| producer = Leon Schlesinger
| story artist = Warren Foster
| narrator = Arthur Q. Bryan (uncredited)
| voice actor = Mel Blanc Arthur Q. Bryan (both uncredited) Carl W. Stalling Milt Franklyn (uncredited)
| animator = Virgil Ross Vive Risto (uncredited)
| studio = Leon Schlesinger Studios Warner Bros. Pictures, Inc.
| release date = May 23, 1942 (USA)
| color process = Black and white
| runtime = 7 minutes
| country = United States
| language = English
}}
 1942 Warner Bros. cartoon in the Looney Tunes series. It was directed by Bob Clampett, animated by Virgil Ross and an uncredited Vive Risto, and musical direction by Carl Stalling.

Elmer Fudds voice can be heard as the narrator, but he is not seen.

==Plot synopsis==
*In the title card, after the title is shown, the credits are shown upside down, then it turns the right way, crediting the director, the writer, the animator, and the musical director.
*Elmer Fudd greets the audience, telling them where they get the latest news. When he says Texas, gunshots can be heard. the Rocky Mountains. The champion hunter is looking for a moose to hunt. The hunter gets out a moose caller, and blows. Close by, a moose gets out a caller, and it sounds like "YOOHOO!". The hunter comes to the moose, and the moose knocks out the hunter with the caller, and does the Tarzan call.
*Elmer narrates on how barbers have trouble with cutting little boys hair, not sitting still. An   invention is put in the shop, and as the boy doesnt sit still, a jack-in-the-box version of Adolf Hitler pops out of the invention, and the barber is able to cut the boys hair.
*Elmer tells the audience on how people are afraid of having their coats stolen while they eat in a restaurant. Luckily, a person (Henry Binder) tests an invention where he puts a portable, rear-view mirrow over his head while he eats. When he is done, he gets his stuff, only to not notice that his pants and shoes have been stolen.
*A scientific study reveals on the secrets of life. Its revealed as an overused joke on how rabbits multiply.
*In Eastern states, there are fireflies with their lights. However, the fireflies are having a blackout.
*Frank Putty, the famous artist, is painting a picture of a real-life supermodel. Elmer says that the secrets of his success is how he gets the proportions right. Elmer asks Frank if he can see the painting, and as it turns out, the artist was painting a picture of his thumb.
*Ducks love to swim in the water on the day they are born. A father duck is taking his three ducklings to the pond. Behind the third duckling is a chicken. The ducks go into the pond, while the chicken sinks. The papa says that chickens dont swim, and the chicken says, "Now he tells me."
*New safety signs have been put up all over the country. From the north (with a sign that says "No U Turns"), to the south (with a sign that says "No U-All Turns"). Carl Bubble is attempting to throw a silver dollar across the river. However, when he throws the dollar, Sandy from Porkys Pooch says that a dollar doesnt go far these days, which Elmer agrees with.
*Fox hunting season has begun. The lead dog is released, and the other dogs are off. However, they go back and forth a few times. Meanwhile, behind a bush, Willoughby (in his only Black and white appearance) flirts with a vixen.
*A photo is shown for a big, new department store. The contractor (Ken Harris) shows the owner (Leon Schlesinger) the blueprints. They both agree and get to work. Suddenly, a guy comes marching with a sign that says "This store will probably be unfair... as if you didnt know".
*The navy is trying to heat up its patrol of the seas to any weather. There is the U.S.S. Connecticut, the U.S.S. Mississippi, and finally, shining in the sun instead of being in the rain, is the U.S.S. California.

==Cast==
*Mel Blanc as the Moose Hunter, Barber, the Man Having Dinner, the Multiplying Rabbits, the Fireflies, Frank Putty, Papa Duck, Baby Chick, Sandy, Hunting Dogs, Willoughby
*Arthur Q. Bryan as Elmer Fudd

==Crew==
*Director: Bob Clampett
*Producer: Leon Schlesinger
*Writer: Warren Foster Carl W. Stalling
*Orchestrator: Milt Franklyn (uncredited)
*Film Editor: Treg Brown (uncredited)
*Sound Effects: Treg Brown (uncredited)
*Animation: Virgil Ross and Vive Risto

==Edited versions==
*When the short aired on Nickelodeon, the scene with the jack-in-the-box Hitler as a new invention for keeping little boys from squirming in the barber chair was cut.

==See also==
* Looney Tunes and Merrie Melodies filmography (1940-1949)

==External links==
*  

 
 
 
 
 
 
 