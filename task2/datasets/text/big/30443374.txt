Death in a French Garden
 
{{Infobox film
| name           = Death in a French Garden
| image          = Péril en la demeure.jpg
| caption        = Film poster
| director       = Michel Deville
| producer       = Emmanuel Schlumberger
| writer         = Michel Deville René Belletto Rosalinde Deville
| starring       = Anémone
| music          = 
| cinematography = Martial Thury
| editing        = Raymonde Guyot
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = France
| language       = French
| budget         = 
}}

 Death in a French Garden  ( ) is a 1985 French drama film directed by Michel Deville. It was entered into the 35th Berlin International Film Festival.   

==Plot==
David Aurphet, a struggling guitar teacher is invited to give lessons to Viviane Tombsthay the daughter of a well-to-do couple. The wife, Julia, seduces him easily. They seem to be set on a standard affair when David gets a video with footage of their adultery. He confides in a friend Edwige who has a VCR and is a neighbour of the Tombsthays.  At the same time, David is attacked but saved by the strange Daniel Forest. He admits to being a killer with Graham Thombsthay as a target. He warns David about Julia, giving him a gun. While her husband is away Julia invites David to their home. David arrives but finds Graham there wanting to kill him. David uses the gun. Julia warns David to hide and he seeks refuge with Edwige. She shows him a video that proves that Julia has killed Graham. David returns to the house where he finds Daniel, with a gun. David kills him, hides the body and leaves to look for Viviane with whom he will escape.

==Cast==
* Anémone as Edwige Ledieu
* Richard Bohringer as Daniel Forest
* Nicole Garcia as Julia Tombsthay
* Christophe Malavoy as David Aurphet
* Michel Piccoli as Graham Tombsthay
* Anaïs Jeanneret as Vivianne Tombsthay
* Jean-Claude Jay as Father
* Hélène Roussel as Mother
* Élisabeth Vitali as Waitress
* Franck de la Personne as Guitar dealer
* Daniel Vérité as Attacker

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 