Canta y no llores...
{{Infobox film
| name           = Canta y no llores...
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = Alfonso Patiño Gómez
| producer       = Salvador Elizondo
| writer         = 
| screenplay     = Alfonso Patiño Gómez
| story          = {{plainlist|
* Mane Sierra
* A. Guzmán Aguilera
* Carlos Ortega}}
| based on       =  
| starring       = {{plainlist|
* Irma Vila
* Carlos López Moctezuma}}
| narrator       = Irma Vila
| music          = {{plainlist|
* José de Pérez
* Federico Ruiz
* Rosalío Ramírez}}
| cinematography = Agustín Martínez Solares
| editing        = Jorge Bustos
| studio         = Estudios Clasa
| distributor    = 
| released       =  
| runtime        = 79 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          =  
}}
Canta y no llores... ("Sing and Dont Cry...") is a 1949 Mexican musical film directed by Alfonso Patiño Gómez and starring ranchera singer Irma Vila and Carlos López Moctezuma.   

==Cast==
* Irma Vila as Rosario "La Tapatía"
* Carlos López Moctezuma as Enrique
* Rodolfo Landa as Gabriel
* Nelly Montiel as Altagracia
* Felipe de Alba as Chuy Anaya
* María Gentil Arcos as Rosarios mother
* Manolo Noriega as Rosarios grandfather

==Songs==
* "Feria de las Flores", written by Chucho Monge
* "México Lindo y Querido|México lindo", written by Chucho Monge
* "Me das una pena", written by Chucho Monge
* "Besando la Cruz", written by Chucho Monge
* "Guadalajara (song)|Guadalajara", written by Pepe Guízar Miguel Lerdo de Tejada
* "Campanero", written by Federico Ruiz
* "Amor en Swing", written by Federico Ruiz
* "La Tequilera", written by Alfredo DOrsay
* "Cielito lindo", written by Manuel Castro Padilla

==References==
 

==External links==
*  

 
 
 


 
 