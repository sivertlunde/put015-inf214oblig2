Up Goes Maisie
{{Infobox film
| name           = Up Goes Maisie
| image          = Up Goes Maisie.jpg
| caption        = Theatrical Film Poster
| director       = Harry Beaumont
| producer       = George Haight
| writer         = Thelma Robinson Wilson Collison (character)
| narrator       =
| starring       = Ann Sothern George Murphy David Snell
| cinematography = Robert H. Planck
| editing        = Cotton Warburton
| studio         = Metro-Goldwyn-Mayer
| distributor    = Loews Inc.
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Amercican comedy Maisie Ravier. In this series entry, Maisie goes to work for an inventor played by George Murphy.

==Plot== John Eldredge). Paul Harvey).

Morton suspects Maisie is an   crew mates Mitch OHara (Murray Alper) and Bill Stuart (Lewis Howard), and college friend Tim Kingsby (Stephen McNally). Then he sets her to work not only in the office, but also at welding and other assembly tasks. Eventually, Maisie and Morton fall in love and become engaged.  

Unbeknownst to Morton, Kingsby, Nuboult and Nuboults daughter Barbara (Hillary Brooke) are scheming to steal his invention. When Maisie notices that they are being billed for twice as many parts (the plotters are building a second copy of the prototype), Barbara invites her to a Sunday social at an exclusive club, where she spikes her drink. Maisie ends up diving into the pool with her clothes on. Feeling she is no good for Morton, Maisie goes into hiding.
 Ray Collins) Rose Bowl, while hiring a private detective to search for Maisie. Then the plotters set their plan into motion, stealing the prototype and burning down the workshop with their copy inside. Maisie becomes suspicious when Nuboult shows up with Mortons canceled contract immediately after the fire is put out. When she cannot find in the wreckage the medals she welded to the helicopter for good luck, she guesses the truth. She, Mitch and Bill follow Tim to where the real helicopter is stashed. A fight breaks out when they try to get it back. Maisie is told to take the helicopter up. She manages to fly through Los Angeles to the Rose Bowl, where an impressed Hendrickson signs onto the project.

==Cast==
* Ann Sothern as Maisie Ravier
* George Murphy as Joseph Morton
* Hillary Brooke as Barbara Nuboult
* Stephen McNally as Tim Kingsby Ray Collins as Mr. Floyd Henderickson
* Jeff York as Elmer Saunders Paul Harvey as Mr. J.G. Nuboult
* Murray Alper as Mitch OHara
* Lewis Howard as Bill Stuart Jack Davis as Jonathan Marbey
* Gloria Grafton as Miss Wolfe John Eldredge as Benson

Connie Gilchrist and Barbara Billingsley have uncredited roles, Gilchrist as a startled cleaning woman who helps an airborne Maisie contact Morton.

==Reception== Hal Erickson of AllRovi disagreed about the aerial sequence; "The process work in this climactic sequences   is unusually good for an MGM production, providing an exciting wrap-up to an otherwise pedestrian project."  TV Guide stated the series "was just about out of gas in 1946" and called Up Goes Maisie "standard stuff." 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 