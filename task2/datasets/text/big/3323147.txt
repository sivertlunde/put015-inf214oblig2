Three from Prostokvashino
 
{{Infobox Film name = Three from Prostokvashino image = Troe iz Prostokvashino.jpg caption = Uncle Fyodor, His Dog and His Cat director = Vladimir Popov producer = Soyuzmultfilm writer = Eduard Uspensky starring = Lev Durov Valentina Talyzina Boris Novikov Maria Vinogradova Gyerman Kachin Oleg Tabakov music = Yevgeni Krylatov cinematography = Kabul Rasulov editing = Natalya Stepantseva distributor =  released =   runtime = 18 min 48 sec country = Soviet Union language = Russian budget = 
}} animated film based on the childrens book Uncle Fyodor, His Dog and His Cat (Дядя Фёдор, Пёс и Кот) by Eduard Uspensky. The film has two sequels, Vacation in Prostokvashino (Каникулы в Простоквашино) (1980) and Winter in Prostokvashino (Зима в Простоквашино) (1984).
 village called "Soured milk" (  Prostokvashino, from Russian Простокваша, Soured milk). There, they have many adventures, some involving the local mail carrier|mailman, Pechkin (voiced by Boris Novikov).

The series has been a source of many phrases in the post-Soviet countries. It has made an impact comparable to Nu, pogodi! in Russian culture.

==External links==
* 
* 

 
 
 
 
 
 

 
 