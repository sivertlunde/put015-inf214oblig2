Penelope (1978 film)
{{Infobox film
| name = Penelope
| image = Penelopa 1978.JPG
| alt = 
| caption = Theatrical release poster
| film name = {{Infobox name module
| original = Penelopa}}
| director = Štefan Uher
| producer = Ján Svikruha
| screenplay = Alfonz Bednár
| based on = Za hrsť drobných   
| writer = 
| starring =  
* Božidara Turzonovová
* Eva Kristínová
* Gustáv Valach 
* Michal Dočolomanský
 
| narrator = 
| music = Svetozár Stračina 
| cinematography = Stanislav Szomolányi
| editing = Maximilián Remeň
| distributor = Slovenská požičovňa filmov Bratislava
| released =  
| runtime = 88 minutes Czechoslovakia
| language = Slovak
| budget = 
}}
 Slovak psychological drama film directed by Štefan Uher. Starring Božidara Turzonovová and Eva Kristínová, the movie was released on January 20, 1978. 
 Slnko v sieti (1962), he reunited with screenwriter Alfonz Bednár and cinematographer Stanislav Szomolányi.

==Plot==
Restorer Eva Kamenická (Božidara Turzonovová) arrives to the village to repair frescoes of the heroines of ancient myths for the local castle. In daily life of the village inhabitants Eva soon finds some elements of ancient tragedy, especially in fate of a lonely old lady named Malovcová (Eva Kristínová). The woman, commonly referred to by locals as "Waitress of Change", awaits the return of her husband and son, who had went to work to America twenty years ago. Eva sees in her a picture of the devoted and patient "Penelope", whose courage and faith impress young restorer. Experience gained while staying in the country turns into her own life decision. Not long after she becomes close with the Secretary of the Municipal Committee, Viktor (Michal Dočolomanský), she decides to leave her daughter Mima (Jana Čeligová) with selfish husband and start a new life.

==Cast==
* Božidara Turzonovová - Eva Kamenická, restorer
* Eva Kristínová - Mrs. Malovcová, "Waitress of Change"/"Penelopa"
* Gustáv Valach - Kuzma, Sr.
* Michal Dočolomanský - Viktor Kuzma, Secretary of the Municipal Committee
* Ilja Prachař - Mišo Bajzlík
* Dušan Blaškovič - Vertel
* Milan Kiš - Gracík
* Anton Šulík - Lovčan
* Leopold Haverl - Stanko
* Jaroslav Vrzala - Nemlaha
* Igor Čillík - Ondro
* Jiřina Jelenská - Zora/Zuza
* Jana Nagyová - Verona
* Michal Nesvadba - Fero

 
 
;Uncredited 
* Igor Hrabinský - Miro
* Jana Čeligová - Mima, Evas daughter
* Elena Volková - Mrs. Kuzmová
* Štefan Mišovic - Blažej
* Ján Géc - publican
* Jozef Mifkovič - publican
* Emília Halamová - Amerikáns wife
* Vladimír Macko - plice officer
 
* Ľubomír Zaťovič - Molčan
* František Desset - Berto Bajzík
* Lenka Kořínková - girl
* Štefan Turňa
* Jaroslav Černík
* Ľudovít Greššo - Mišo Bajzík  
* Hana Kostolanská - Kuzmová  
* Dušan Kaprálik - Fero  
* Katarína Orbánová - Zuza  
 

==Awards ==
{| class=wikitable
! Year
! Nominated work
! Award 
! Category Result
|-
| 1978
| Štefan Uher
| Czechoslovak Film Festival České Budějovice 
| Direction - Special Mention
|  
| align=center rowspan=3|    
|-
| rowspan=2| 1979
| Božidara Turzonovová
| rowspan=2| Film a Divadlo Awards 
| Best Actress
|  
|-
| Michal Dočolomanský
| Best Actor
|  
|}

==See also==
* The Sun in a Net (1962s work by the same tandem Štefan Uher|Uher-Bednár-Szomolányi)

==References==
;General
* 
* 
;Specific
 

==External links==
*  

 
 
 
 
 