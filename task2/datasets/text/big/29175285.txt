Fiend of Dope Island
{{Infobox film
| name           = Fiend of Dope Island
| image          = Fiendpos.jpg
| caption        = Original film poster
| director       = Nate Watt
| producer       = J. Harold Odell
| writer         = Bruce Bennett  Mark Carabel Nate Watt (story) 
| starring       = Bruce Bennett Robert Bray Tania Velia
| music          = "Forever Hold Me" music and lyrics by Ken Darby
| cinematography = Gayne Rescher
| editing        = 
| distributor    = Essanjay Films
| released       =  
| runtime        = 76 min.
| country        = United States English
}}

Fiend of Dope Island, also released as Whiplash, was a lurid mens adventure type motion picture filmed in 1959 and released in 1961. It starred and was co-written by Bruce Bennett and was the final film directed by Nate Watt.  It was filmed in Puerto Rico  where producer J. Harold Odell had previously filmed his Machete (1958) and Counterplot (1959). Several scenes were censored for the United States release.  The film co-stars Tania Velia billed as the "Yugoslavian Bombshell" who had appeared in the July 1959 Playboy and Puerto Rican actor Miguel Ángel Álvarez.

==Plot==
Charlie Davis runs his own island in the Caribbean with a literal whip hand making his income as a marijuana grower, exporter and gunrunner. He hires a female entertainer to amuse the clients of his cantina and himself.

Charlies world falls apart when one of his employees is an undercover narcotics investigator. The trouble escalates to a full native rebellion and shark attack.

==Legacy==
The Cramps named their 2002 record album Fiends of Dope Island after the film. 

==Cast==
*Bruce Bennett ... Charlie Davis
*Robert Bray ... David
*Tania Velia ... Glory La Verne
*Ralph A. Rodriguez ... Naru
*Miguel Ángel Álvarez ... Capt. Fred

==Notes==
 

==External links==
*   
* Original film trailer http://www.youtube.com/watch?v=7wVqWp5OgDY

 
 
 
 
 
 
 

 