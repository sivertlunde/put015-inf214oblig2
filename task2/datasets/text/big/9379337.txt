The Last Word (2008 film)
 
{{Infobox Film |
  name     = The Last Word |
  image          = Last wordmp.jpg |
  caption        = Theatrical release poster |
  writer         = Geoffrey Haley |
  starring       = Winona Ryder Wes Bentley Ray Romano |
  director       = Geoffrey Haley |
  producer       = David Hillary Alexandra Milchan Timothy Wayne Peternel Bonnie Timmermann	 Jack Utsick |
  distributor    =  |
  music          = John Swihart |
  released       =   |
  runtime        =  |
  country  = United States | English |
  budget         = |
  music          = |
}}
The Last Word is an offbeat romantic comedy-drama film written and directed by Geoffrey Haley. It stars Winona Ryder and Wes Bentley. It had its world premiere at the Sundance Film Festival in January, and had a wider release in 2008 in film|2008.

==Plot==
An odd-but-gifted poet, Evan Merck (Wes Bentley) makes his living writing suicide notes for the soon-to-be departed. So when he meets Charlotte (Winona Ryder), the free-spirited sister of his latest client, Evan has no choice but to lie about his relationship to her late, lamented brother. Curiously attracted by his evasive charms, a smitten Charlotte begins her pursuit, forcing Evan to juggle an amorous new girlfriend, a sarcastic new client (Ray Romano) and an ever-increasing mountain of lies.

==Cast==

*Winona Ryder ... Charlotte Morris
*Wes Bentley ... Evan
*Ray Romano ... Abel
*Gina Hecht ... Hilde Morris
*A. J. Trauth ...  Greg
*John Billingsley ... Brady
*Kurt Caceres ...  Sammy
*Michael Cornacchia ...  Client

==External links==
* 
*http://www.the-soap.com/munity/index.php?showtopic=5397&pid=101297&st=0&#entry101297
*http://www.film.com/movies/sundance-review-the-last-word
*http://www.rottentomatoes.com/m/1210249-last_word
*http://www.chokingonpopcorn.com/popcorn/2009/07/the-last-word-2008/
*http://reviews.filmintuition.com/2009/04/dvd-review-last-word-2008.html
*http://www.hotflick.net/movies/2008_The_Last_Word.html

 
 
 
 
 
 

 