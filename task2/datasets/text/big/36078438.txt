Prairie Moon
{{Infobox film
| name           = Prairie Moon
| image          = Prairie_Moon_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Ralph Staub
| producer       = Harry Grey (associate)
| writer         = {{Plainlist|
* Betty Burbridge
* Stanley Roberts
}}
| starring       = {{Plainlist|
* Gene Autry
* Smiley Burnette
* Shirley Deane
}}
| music          = Raoul Kraushaar (musical director)
| cinematography = William Nobles
| editing        = Lester Orlebeck
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 58 minutes
| country        = United States
| language       = English
}} Western film directed by Ralph Staub and starring Gene Autry, Smiley Burnette, and Shirley Deane. Written by Betty Burbridge and Stanley Roberts, the film is about a singing cowboy who takes care of three tough boys sent west from Chicago after their father dies and leaves them a cattle ranch.   
 
==Plot==
Following a shootout with lawmen, cattle rustler Jim "Legs" Barton (William Pawley) with his dying words makes the local sheriff and childhood friend, Gene Autry (Gene Autry), promise to take care of things for him after hes gone. The next day, Bartons attorney, Arthur Dean, informs Gene that, as executor of the estate, he must look after Bartons three motherless boys. Gene sends his sidekick Frog Milhouse (Smiley Burnette) to Chicago to bring the children back, and then prepares the ranch for their homecoming, with the help of Peggy Shaw (Shirley Deane), the local schoolteacher. In Chicago, Frog locates the feisty boys, William "Brain", Clarence "Nails", and Hector "Slick" Barton, who are more than Frog can handle.

The boys have trouble adjusting to Western life on the ranch and long to return to the city. While they spy on Gene as he proposes to Peggy at a barn dance, general store owner Frank Welch (Stanley Andrews), Legss secret partner, leads a cattle rustling raid. The local ranchers chase after the rustlers, but the cattle seem to vanish. The ranchers do not suspect Welch, but when the boys see him riding out from behind a waterfall on their ranch, they realize what has happened. Welch tries to befriend them, saying that their father was his good friend. When the boys tell him about Genes plan to adopt them, Welch comes up with his own plan. 

At the adoption hearing, Welch and his wife offer to adopt the Barton boys. Gene agrees to the offer because he is unmarried and the boys say the want to go with Welch. Sometime late, one of the boys has a change of heart, and when he tells Gene what has happened, Gene produces a phony "Mrs. Barton" and her three children. The judge then rules that Brain, Nails, and Slick are imposters, but Welch detects that the plan is a trick. He plans to do away with the boys and secretly remove the rustled cattle from their ranch. Gene intervenes, however, and Welch and his gang are captured. Afterwards, Gene and Peggy plan to marry and adopt the boys, despite Frogs concern that they will be getting more than they bargained for.

==Cast==
* Gene Autry as Gene Autry
* Smiley Burnette as Frog Millhouse
* Shirley Deane as Peggy Shaw
* Tommy Ryan as William Brains Barton
* Walter Tetley as Clarence Nails Barton
* David Gorcey as Hector Slick Barton
* Stanley Andrews as Frank Welch
* William Pawley as Jim Legs Barton
* Warner Richmond as Lead Henchman Mullins
* Ray Bennett as Henchman Hartley (as Raphael Bennett)
* Tom London as Henchman Steve
* Bud Osborne as Henchman Pete
* Jack Rockwell as Sheriff
* Peter Potter as Bandleader
* Champion as Genes Horse (uncredited)   

==Production==
===Stuntwork===
* Tommy Coats
* Duke Taylor
* Joe Yrigoyen 

===Filming locations===
* Brandeis Ranch, Chatsworth, Los Angeles, California, USA 
* Iverson Ranch, 1 Iverson Lane, Chatsworth, Los Angeles, California, USA   

===Soundtrack===
* "Rhythm of the Hoofbeats" (Gene Autry, Fred Rose, Johnny Marvin) by Gene Autry, Smiley Burnette, and Cowhands
* "The Girl in the Middle of My Heart" (Walter Kent, Eddie Cherkose) by Gene Autry
* "In the Jailhouse Now" (Jimmie Rodgers) by Gene Autry and Smiley Burnette
* "Welcome Song" (Walter Kent, Eddie Cherkose) by the School Children
* "The West, a Nest, and You" (Billy Hill, Larry Yoell) by Gene Autry (guitar and vocal) at the barn dance
* "The West, a Nest, and You" (reprise) by Gene Autry, Smiley Burnette, Shirley Deane, Tommy Ryan, Walter Tetley, and David Gorcey at the end
* "The Story of Trigger Joe" (Walter Kent, Eddie Cherkose) by Smiley Burnette at the barn dance, and whistled by the attendees
* "Theres No Place Like Home (Home, Sweet Home)" (H.R. Bishop, John Howard Payne) by Tommy Ryan and Walter Tetley (a capella), and danced by David Gorcey   

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 