La Classe américaine
{{Infobox Film
| name           = La Classe américaine
| image          = 
| caption        = 
| director       = Michel Hazanavicius  Dominique Mézerette	
| producer       = Ève Vercel  Robert Nador  Michel Lecourt
| writer         = Michel Hazanavicius  Dominique Mézerette
| starring       = John Wayne  Dustin Hoffman  Robert Redford  Paul Newman
| music          = Laurent Petitgirard
| cinematography = 
| studio         = Dune Studio Canal +
| distributor    = Canal+ Group Warner Bros. Television (All Media)
| released       = 1993
| runtime        = 70 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
 dubbed with new lines so as to create an entirely new film.

==Plot==
The film begins with the following lines appearing on screen, complete with deliberate spelling mistakes: "Attention! Ce flim nest pas un flim sur le cyclimse. Merci de votre compréhension." ("Attention! This flim is not a flim about cyclign.   Thank you for your understanding.")

The story begins with the death of George Abitbol ( . Pom Pom Galli was taken from the Wayne movie The Sea Chase.

==Cast==
{{columns-list|3|
*John Wayne as George Abitbol
*Burt Lancaster as José
*Lana Turner as Isabelle
*Jason Robards as Editor in chief
*Dustin Hoffman as Peter
*Robert Redford as Steven
*Paul Newman as Dave
*Orson Welles as himself
*Martin Balsam as Callaghan
*Henry Fonda as Hugues
*Ricky Nelson as Georges friend
*Charles Bronson as the Native American chief
*James Stewart as Jacques
*Dean Martin as Dino
*Elvis Presley as the "putain dénergumène" ("damn oddball")
*Frank Sinatra as Franky
*Stuart Whitman as the futures man
*Ned Beatty as Frédéric
*Angie Dickinson as Jacqueline
*Spencer Tracy as the professional witness
*Ernest Borgnine as Ernest
*Jan-Michael Vincent as the helicopter fan
*Clark Gable as the actor
*Yvonne De Carlo as the actors wife
*Robert Mitchum as Yves
*Randolph Scott as Joël Hammond
*James Franciscus as Professor Hammond
*Lauren Bacall as Christelle Deep Throat
*Antonio Fargas as Huggy
*Walter Brennan as Stumpy
*Jacqueline Bisset as the helicopter woman
*Burgess Meredith as the helicopter man
*Jack Warden as a journalist
*Akim Tamiroff as Dinos friend
}}

==Production== All the The Candidate, Chisum, The Cowboys, The Crimson Pirate, and When Time Ran Out.

==See also==
* Dead Men Dont Wear Plaid
*John Wayne filmography
* Mozinor
* Guy Debord

==References==
*   on IMDB
*   on  

== External links ==
*  

 

 
 
 
 
 
 

 