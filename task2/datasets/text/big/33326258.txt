Placebo Effect (film)
{{Infobox film
| name        = Placebo Effect
| image       = 
| writer      = Luciano Saber
| starring    = Francesco Quinn Martin Halacy Luciano Saber
| director    = Alejandro Seri
| producer    = Ninoos Bethishou Cherelle George Lester Smith
| distributor = LS Films
| released    =    
| runtime     = 86 minutes
| music       = Bill Dunn
| language    = English
| budget      =
}} thriller written by Luciano Saber and directed by Alejandro Seri. It features Francesco Quinn, Martin Halacy, and Kirsten Berman. It was released in The United States on 31 January 1998 and won Best Film at the New York Independent Film Festival in 1998.

==Plot==
Aleksander Ivanov is a cab driver from Bosnia who is drawn into a plot to assassinate the Vice President of the United States. Ivanov tries to find who is behind the plot.

==Cast==
* Francesco Quinn  as  Zac  
* Luciano Saber  as  Aleksander Ivanov  
* Martin Halacy  as  Congressman Ryan  
* Christopher Richard Garrett  as  Mick  
* Kirsten Berman  as  Rachel  
* Marshall Bean  as  Mr. Jones  
* Sarah Charipar  as  Jo  
* Travis Estes as  Ethan  
* David Mersault  as  Jimmy White  
* Tyla Abercrumbie  as  Suit  
* Lester Smith  as  Bartender  
* Tracey Stiles  as  Detective  
* Laura Wade  as  Artist  
* Eileen Flahegerty  as  Model

==External links==
* 
* 
*  

 
 
 
 
 
 

 