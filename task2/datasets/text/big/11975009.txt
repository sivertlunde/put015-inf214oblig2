Little Thieves, Big Thieves
{{Infobox film
| name           = 100 años de perdón : Little thieves, big thieves
| image          = 100añosdeperdón.jpg
| image_size     = 
| caption        = 
| director       = Alejandro Saderman
| producer       = Ezequiel Burguillos, Maye Larotonda, Antonio Llerandi (executive)  Carlos González, Henry Herrera 
| narrator       =  Daniel Lugo  Aroldo Betancourt
| music          = Julio dEscriván
| cinematography = Hernán Toro
| editing        = Giuliano Ferrioli
| distributor    = Alejandro Saderman Producciones
| released       = 1998 March 21, 2002 (Germany)
| runtime        = 100 minutes
| country        = Venezuela
| language       = Spanish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1998 Venezuelan gangster comedy film directed by Alejandro Saderman. The film was a considerable success in Germany. 

==Plot==
In the wake of a banking crisis, four middle class friends throw a white glove hold up, only to find out that their bank has already gone bankrupt.

==Cast==
*Orlando Urdaneta ... Horacio  Daniel Lugo ... Valmore 
*Aroldo Betancourt ... Rogelio  Mariano Álvarez ... Vicente 
*Elluz Peraza ... Lucía Carvajal 
*Flavio Caballero ... Juan Carlos 
*Alicia Plaza... Rita 
*Armando Gota ... Pujol 
*Cayito Aponte ... Presidente del Banco 
*Basilio Álvarez ... Chofer de Taxi

== See also ==
*Venezuela 
*Venezuelan culture

== External links ==
*  

 
 
 
 
 


 
 