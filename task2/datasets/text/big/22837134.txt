A Town Called Panic (film)
 
{{Infobox film
| name           = A Town Called Panic
| image          = ATownCalledPanic.jpg
| caption        = Theatrical release poster
| director       = Stéphane Aubier Vincent Patar
| producer       = Adriana Piasek-Wanski
| writer         = Stéphane Aubier Vincent Patar
| starring       = Jeanne Balibar (Voice) Nicolas Buysse (Voice) Véronique Dumont (Voice)
| music          = Fabien Pochet
| cinematography = Jan Vandenbussche
| editing        = Peter Bernaers
| distributor    = Gébéka Films (France)
| released       =  
| runtime        = 75 minutes
| country        = Belgium Luxembourg France
| language       = French
| budget         = 
| gross          = $166,688
}}
 of the same name.    It premiered at the 2009 Cannes Film Festival and was the first stop-motion film to be screened at the festival.   

==Plot==
Three plastic toys named Cowboy, Indian and Horse share a house in a rural town. Cowboy and Indian plan to surprise Horse with a homemade barbecue for his birthday. However, they accidentally order 50 million bricks, instead of the 50 they actually require. This sets off a chain of events as the trio travel to the center of the earth, trek across frozen tundra and discover a parallel underwater universe of pointy-headed (and dishonest) creatures.

==Cast==
* Stéphane Aubier as (Cowboy, Max Briquenet, Mr Ernotte)
* Jeanne Balibar as (Madame Jacqueline Longrée; spelled "Longray" in some English subtitles)
* Nicolas Buysse as (Sheep, Jean-Paul)
* François De Brigode as (Sportscaster)
* Véronique Dumont as (Janine)
* Bruce Ellison as (Indian)
* Christine Grulois as (Cow, Student)
* Frédéric Jannin as (Policeman, Gérard, Brick Delivery Man)
* Bouli Lanners as (Postman, Simon, Cow)
* Christelle Mahy as (Chicken)
* Éric Muller as (Rocky Gaufres, Music Student 1)
* François Neyken as (Pig)
* Vincent Patar as (Horse, Mother Atlante)
* Pipou as (Michels laugh)
* Franco Piscopo as (Bear)
* Benoît Poelvoorde as (Steven)
* David Ricci as (Donkey, Michel)
* Ben Tesseur as (Scientist 1)
* Alexandre von Sivers as (Scientist 2)

==Production==
The film was made over the course of 260 days in a studio on the outskirts of Brussels. 1500 plastic toy figures were used during filming.   

==Reception==
The film has received mostly positive reviews with a score of 87% "fresh" on Rotten tomatoes, with an average rating of 7.6/10 based on 47 reviews (41 of which were positive). Little White Lies awarded the film 4 out of 5 for enjoyment stating "wide-eyed, broad smile" although in retrospect they scored the film 3 out of 5 suggesting that "like all toys. It will have a shelf life".   

Empire magazine were very positive awarding the film 4 stars, summing it up as "Toy Story on absinthe"    and stating the film was "One of the years true originals." Hollywood Reporter were positive also summarizing that "Theres really very little to say about this film beyond that its absolutely brilliant". Roger Ebert enjoyed the film, rating it 3.5/4 and stating that "Because the plot is just one doggoned thing after another without the slightest logic, theres no need to watch it all the way through at one sitting. If you watch it a chapter or two at a time, it should hold up nicely."    Ebert later placed the film on his list of the best animated films of 2010.

==Awards and nominations==
* Official Selection (Out of Competition), 2009 Cannes Film Festival.   
* Official Selection, 2009 Toronto Film Festival. 
* Winner, Audience Award, 2009 Fantastic Fest.   
* Winner, Best Sound, 2011 Magritte Awards   
* Winner, Best Production Design, 2011 Magritte Awards

==References==
 

==External links==
*  
*  
*  
*   at Twitter
*   at Facebook

 
 
 
 
 
 
 
 
 
 