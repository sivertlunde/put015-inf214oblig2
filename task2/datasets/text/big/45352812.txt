Urumeen
{{Infobox film
| name           = Urumeen
| image          = 
| alt            =  
| caption        = 
| director       = Sakthivel Perumalsamy
| producer       = G. Dillibabu
| writer         = Sakthivel Perumalsamy
| starring       = Bobby Simha Kalaiyarasan Reshmi Menon
| music          = Achu Rajamani
| cinematography = Ravindranath Guru
| editing        = San Lokesh
| studio         = Axess Film Factory
| distributor    = 
| released       = 
| runtime        = 
| country        = India
| language       = Tamil
| budget         =  
| gross          =
}}
 Tamil fantasy thriller film written and directed by newcomer Sakthivel Perumalsamy. It stars Bobby Simha, Kalaiyarasan and Reshmi Menon. Produced by Axess Film Factory, the film has cinematography by Ravindranath Guru and music composed by Achu Rajamani. The film, which is in the stage of post-production, is expected to release in May 2015. 

==Cast==
* Bobby Simha as Selva
* Kalaiyarasan as John Christopher
* Reshmi Menon
* Sandra Amy as Jennifer
* Appukutty
* Kaali Venkat
* Manobala
* Anupama Kumar

==Production==
After having worked as an assistant director, Sakthivel Perumalsamy wanted to direct a feature film, based on a real-life incident that took place in 1990.  In 2011, he first approached and discussed the story with Bobby Simha, whose short films he had seen. They waited to find good producers for the project, which therefore did not commence until March 2014.  Simha took up the lead role in the film, his first after a number of supporting roles.  His character, Selva, was a middle-class man who comes to Chennai from a small town to search for a job but gets into a problem,  and he said that his character "exhibits two behaviours".  While Kalaiyarasan landed the role of the villain, a newcomer Athithi was given the lead female role,  who was said to be paired opposite Simha.  Kalaiyarasan later revealed that the director had initially asked him to play the hero but that he could not agree as he had other films lined up, while he called his character, John Christopher, "  entirely evil". 

Over the next months, the film saw several changes in its principal cast and crew. Initially reported to be produced by Dr. L. Sivabalan under his Zero Rules Entertainment banner,  the project was later known to be funded by G. Dillibabus Axess Film Factory. Several actresses then claimed to played the lead female role and the team failed to straighten things out. In October 2014, Haritha Parokod first told that she was currently working in the film,  but she did not clarify whether she had replaced Athithi or played a different role. Later, Reshmi Menon was widely reported to be the heroine,    which was confirmed in the teaser.  In February 2015, Sandra Amy stated that she had been selected to portray a bold, tomboyish girl named Jennifer and Simhas romantic interest in the film and that she had begun shooting from early that month.  The cinematographer too was exchanged; instead of Sree Sarvanan G. Manoharan, as reported earlier,  Ravindranath Guru eventually handled the camera.

The film was shot across several South Indian states in locations including Chennai, Thekkady, Idukki, Ernakulam, Puducherry, Bengaluru and Pune.  The first look poster was launched by director Karthik Subbaraj in January 2015 across the social networking platforms Facebook and Twitter.  A first teaser was released in early February. 

== References ==
 

 
 
 
 