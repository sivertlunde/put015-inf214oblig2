The Diary of Sacco and Vanzetti
{{Infobox film
| name           = The Diary of Sacco and Vanzetti
| image          = The diary of sacco and vanzetti DVD cover.jpg
| caption        = DVD cover art
| director       = David Rothauser
| producer       = David Rothauser Rob W. Gray
| writer         = David Rothauser
| based on       =  
| starring       = David Rothauser 
| music          = John T. LaBarbera
| cinematography = Paul Taggart Russ Jaquith
| editing        = Russ Jaquith Chi-Ho Lee
| studio         = Memory Productions
| distributor    = 
| released       =  
| runtime        = 57 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 American docudrama, written and directed by David Rothauser, about the trial of Sacco and Vanzetti and an account of Vanzettis life from the moment of his arrival as an immigrant in the United States, to the events leading to his execution. Rothauser performs in his film in the role of Bartolomeo Vanzetti.          

==Background== graduate thesis at Boston University in 1973.  The concept was first developed as a full-length screenplay that included over 50 characters, but over the 30-year period of its development, Rothauser narrowed the focus to concerntrate on Bartolomeo Vanzetti as the films central figure,  and to point out the flaws in the trial which led to then-Governor of Massachusetts Michael Dukakis’ official declaration in August of 1977, that "Sacco and Vanzetti had been treated unjustly and any disgrace should be forever removed from their names." 

==Synopsis==
The film uses archived letters, speeches and documents to cover Bartolomeo Vanzettis arrival the United States as an immigrant, his involvement with Nicola Sacco, and the events of his trial leading up to his execution in Massachusetts in 1927. 

==Release==
The film had its theatrical premiere at the Boston Underground Film Festival in October 2003,    and continues to screen in New England seven years after initial release.       

==See also==
* Sacco e Vanzetti
* Sacco and Vanzetti (film)|Sacco and Vanzetti

==References==
 

==External links==
*  
*  


 
 
 
 
 

 
 