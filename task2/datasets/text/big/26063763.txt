Troublesome Night 17
 
 
{{Infobox film name = Troublesome Night 17 image = caption = traditional = 陰陽路十七之監房有鬼 simplified = 阴阳路十七之监房有鬼 pinyin = Yīn Yáng Lù Shí Qī Zhī Jiān Fáng Yóu Guǐ jyutping = Jam1 Joeng4 Lou6 Sap6 Cat1 Zi1 Gaam1 Fong2 Jau5 Gwai2}} director = Lam Wai-yin producer = Nam Yin writer = Jameson Lam starring =  music = Mak Jan-hung cinematography = Chan Yiu-ming Mike Pang Lum Lai-shing Wong Chun-mo Lee Chi-wai editing = Jacky Leung studio = Nam Yin Production Co., Ltd. East Entertainment Limited B&S Limited distributor = B&S Films Distribution Company Limited released =   runtime =  90 minutes country = Hong Kong language = Cantonese budget = gross = HK$15,290
}}
Troublesome Night 17 is a 2002 Hong Kong horror comedy film produced by Nam Yin and directed by Lam Wai-yin. It is the 17th of the 19 films in the Troublesome Night film series.

==Plot==
Bud Gay is arrested for carrying drugs and sent to prison. When his mother, Mrs Bud Lung, visits him, she realises that a ghost is haunting him, so she sends Bud Yan to help him. They learn that the spirit is a vengeful ghost of a woman, who had followed suit after her husband committed suicide when he was wrongfully imprisoned. All she wants is to see her husbands grave, which is located behind the prison. However, she thinks that she has been fooled when the burial site cannot be found, and unleashes her fury on everyone.

==Cast== Sam Lee as Mao Tomorrow
* Teresa Mak as Ng Mei-ho
* Law Lan as Mrs Bud Lung
* Simon Lui as Bud Pit
* Tong Ka-fai as Bud Gay
* Lee Siu-kei as Brother Gay
* Anita Chan as Audrey
* Ronnie Cheung as Bud Yan
* William Ho as Big Mic
* Baat Leung-gam as Officer Eight
* Kau Man-lung as Lai Chor-kau
* Kwai Chung as Lai Chor-pat
* Anna Ng as Mrs Ng
* Law Shu-kei as Prison warden
* Fung Chi-keung as Prisoner
* Jass Chan
* Li Kar-wing
* Big Cheong as Policeman
* Lam Wai-yin as Officer Kong
* Nelson Ngai as Lawyer Lee

==External links==
*  
*  

 

 
 
 
 
 
 
 


 
 