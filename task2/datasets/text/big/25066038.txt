The 80th Birthday Concert
 
{{Infobox album 
| Name        = The 80th Birthday Concertt
| Type        = live George Russell & The Living Time Orchestra
| Cover       = The 80th Birthday Concert.jpg
| Released    =  
| Recorded    = June 2003
| Genre       = Jazz
| Length      = Concept
| George Russell George Russell
| Last album  = Its About Time (George Russell album)|Its About Time (1996)
| This album  = The 80th Birthday Concert (2005)
}} George Russell Concept label in 2005, featuring a performance by Russell with his Living Time Orchestra recorded in 2003. The Allmusic review by Scott Yanow awarded the album 3½ stars and states "The 80th Birthday Concert, a two-CD set, stands as one of his finest recordings and sums up much of his career... it is the sound of the passionate ensembles, the very original writing, and the spirit of the musicians and the ageless Russell that makes this a highly recommended set". 

==Track listing==
{{Tracklist
| all_writing = George Russell except as indicated
| writing_credits = yes
| title1 = Listen to the Silence
| writer1 = 
| length1 = 6:07
| title2 = Announcement
| writer2 = 
| length2 = 0:27
| title3 = Electronic Sonata for Souls Loved by Nature
| writer3 = 
| length3 = 28:52
| title4 = The African Game
| writer4 = 
| length4 = 40:44
| title5 = Its About Time
| writer5 = 
| length5 = 12:19 So What
| writer6 = Miles Davis
| length6 = 17:36
}}

==Personnel== George Russell arranger
*Stuart Brooks, Stanton Davis, Palle Mikkelborg - trumpet
*Dave Bargeron - trombone Richard Henry - bass trombone
*Chris Biscoe - alto saxophone
*Andy Sheppard - tenor saxophone
*Pete Hurt - baritone saxophone, bass clarinet
*Hiro Honshuku - flute, electronics keyboards
*Bill Fender bass Mike Walker - guitar
*Richie Morales - drums
*Pat Hollenbeck - percussion

==References==
 

 
 
 
 