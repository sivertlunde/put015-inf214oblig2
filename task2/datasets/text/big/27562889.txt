Indraprastham (film)
{{Infobox film
| name           = Indraprastham
| image          = Indraprastham (film).jpg
| image_size     = 
| alt            = 
| caption        =  Haridas
| producer       = Premkumar Marath
| writer         = Robin Thirumala
| narrator       =  Simran  Vikram
| Vidyasagar
| cinematography = Sanjeev Shankar
| editing        = K. Sankunny 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 139 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Indraprastham is 1996 Malayalam movie made in India, Vikram in the lead roles. The movie was dubbed into Tamil as Delhi Durbar and Telugu as Indraprastham.

==Plot== morphed onto his body in the video, whereby he traps him for the crime. Kiran is jailed under the charge of murder. Chitra gets the video tape, and she smells foul and tries to reveal the truth behind the morphing of images. She is followed by the goons of Paul. Chitra reaches Banglore, but the goons reach there after her. She is saved by Sathish Menon (Mammootty), a computer expert.  He becomes her savior. After watching the video, Satish realizes the truth behind the crime and decides to expose Paul B. Issac. Meanwhile, Kiran is killed. Satishs software company suffers huge setbacks, after the illegal influence of Paul, causing huge losses. Satish openly begins his battle for justice for Chitra.

==Cast==
*Mammootty ...  Satheesh Menon Simran ...  Chitra Narayan Devan ...  Paul B. Isaac Vikram ...  Peter
*Prakash Raj ...  Mohan George
*Akshay Anand ...  Kiran Verma
*Azeez ...  Police Commissioner
*Hemant Birje ...  Shyam
*Priya Raman Abu Salim ...  Goonda
*Sivaji
*M. G. Soman ...  K.N. Nair
*Oduvil Unnikrishnan ...  Manikkoth Kunjukrishna Panicker
*Nirosha ... Anita Kulkarni

==Soundtrack== Vidyasagar

{|class="wikitable" width="60%"
! Song Title !! Singers
|-
| "Thanka Thinkal" || M. G. Sreekumar, K. S. Chitra
|-
| "Parayumo Mookayamaame" || K. J. Yesudas
|-
| "Mazhavillin Kottarathhil" || Sujatha Mohan, Biju Narayanan
|-
| "Dekho Simple Magic" || Biju Narayanan
|-
| "Bolo Bolo" || K. J. Yesudas
|-
|}

==External links==
*  
* http://popcorn.oneindia.in/title/7238/indraprastham.html

 
 
 
 
 
 
 


 
 