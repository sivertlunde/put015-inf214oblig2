Brothers Till We Die
{{Infobox film
| name = Brothers Till We Die
| image = Banda del gobbo.jpg
| caption =
| director = Umberto Lenzi
| writer =
| starring = Tomas Milian Pino Colizzi
| music = Franco Micalizzi
| cinematography = Federico Zanni
| editing = Eugenio Alabiso
| producer = Luciano Martino
| distributor =
| released =  
| runtime =
| awards =
| country = Italy
| language = Italian
| budget =
}} 
Brothers Till We Die  ( ) is an Italian poliziottesco-action film directed in 1978 by Umberto Lenzi. This film is the last collaboration among Lenzi and Tomas Milian. In this movie Milian plays two characters, Vincenzo Marazzi aka "The Hunchback" that he already played for Lenzi in Rome Armed to the Teeth, and his twin brother Sergio Marazzi aka "Er Monnezza", a role that he played for the first time in Lenzis Free Hand for a Tough Cop and later resumed in Destruction Force by Stelvio Massi. 

== Plot ==
The notorious Italian criminal known as "Hunchback" returns from Corsica. Together with his younger brother and other accomplices he plans to raid an armoured truck. But things go awry.

== Cast ==
* Tomas Milian: "The Hunchback"/ "Er Monnezza"
* Pino Colizzi: Commissioner Sarti
* Mario Piave:  Commissioner Valenzi
* Isa Danieli: Maria
* Sal Borgese: Milo Dragovic, aka "Albanese"
* Luciano Catenacci: Perrone
* Guido Leontini: Mario Di Gennaro, "Er Sogliola"
* Nello Pazzafini: Carmine Ciacci
* Solvi Stubing: Marika Engver

==References==
 

==External links==
* 

 

 
 
 
 
 


 
 