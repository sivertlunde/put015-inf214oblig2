The Long Ships (film)
{{Infobox film
| name           = The Long Ships
| image          = The long shipsposter.JPG
| caption        = Original cinema poster
| director       = Jack Cardiff
| producer       = Irving Allen
| screenplay     = Beverley Cross Berkely Mather
| based on       =  
| starring       = Richard Widmark Sidney Poitier Russ Tamblyn
| narrator       = Edward Judd
| music          = Dušan Radić
| cinematography = Christopher Challis
| editing        = Geoffrey Foot
| studio         = Warwick Films Avala Film
| distributor    = Columbia Pictures
| released       =  
| runtime        = 126 min.
| country        = United Kingdom Yugoslavia English
| budget         = 
| gross = est. $1,930,000 (US/ Canada) 
}}
 1964 United Kingdom|British-Yugoslavian adventure film shot in Technirama directed by Jack Cardiff and stars Richard Widmark, Sidney Poitier, and Russ Tamblyn.   

==Background== Swedish novel The Vikings El Cid Alfred the Great (1969).

==Plot== Moorish king Aly Mansuh (Sidney Poitier) is convinced that it does. Having collected all the legendary material about it that he can, he plans to mount an expedition to search for it. When the shipwrecked  Norsemen|Norseman, Rolfe (Richard Widmark), repeats the story of the bell in the marketplace, and hints that he knows its location, he is seized by Mansuhs men and brought in for questioning. Rolfe insists that he does not know and that the bell is only a myth. He manages to escape before the questioning continues under torture.

Managing to return home, Rolfe reveals to his father that he did indeed hear the bell pealing on the night his ship was wrecked in Africa.  However, Rolfes father has been made destitute after spending a fortune building a funeral ship for the Danish king, Harold Bluetooth, who then refuses to reimburse him citing an outstanding debt.  Rationalising that the ship does not yet belong to Harold (since he is still living), Rolfe and his brother steal not only the ship, but kidnap a number of inebriated Vikings to serve as its crew.  In order to prevent Harold from killing his father in revenge for the theft, he also takes the kings daughter as a hostage. Harold declares that he will summon every longship he can find and rescue her. After prolonged difficulties at sea, the ship is damaged in a maelstrom. The Norse are cast ashore in Mansuhs country. Captured by the Moors, the Norse are condemned to execution but Mansuhs favourite wife Aminah (Rosanna Schiaffino) convinces her husband to use them and their longship to retrieve the bell.

Arriving at the Pillars of Hercules, Rolfe and Mansuh find only a domed chapel with a small bronze bell where the Viking was certain he had heard The Mother of Voices. Frustrated, Rolfe throws the hanging bell against a wall and the resounding cacophony reveals that the chapel dome is the disguised Mother of Voices.  After a costly misadventure moving the Mother of Voices from its clifftop down to the sea, the expedition finally returns to the Moorish city, Aly Mansuh triumphantly riding through the streets with the bell in tow. As the group reaches Mansuhs palace, Aminah suddenly cries aloud that "The Long Ships came in the night" and is immediately shot down by a spear.  A group of Vikings come leaping out from behind the silent townspeople. These Norsemen are King Harolds men, out to rescue the princess, and the climactic battle ensues. It ends when the bell falls over and crushes Aly Mansuh. The Moors are defeated and the Vikings victorious. The film ends as Rolfe tells King Harold about the "three crowns of the Saxon kings."

==Cast==
*Richard Widmark  ...  Rolfe  
*Sidney Poitier  ...  Aly Mansuh  
*Russ Tamblyn  ...  Orm  
*Rosanna Schiaffino  ...  Aminah  
*Oskar Homolka  ...  Krok  
*Edward Judd  ...  Sven  
*Lionel Jeffries  ...  Aziz  
*Beba Loncar  ...  Gerda  
*Clifford Evans  ...  King Harald   Gordon Jackson  ...  Vahlin  
*Colin Blakely  ...  Rhykka   David Lodge  ...  Olla  
*Henry Oscar  ...  Auctioneer  
*Paul Stassino  ...  Raschid  
*Jeanne Moody  ...  Ylva   
*Leonard Rossiter ... Rider of the Mare of Steel

==Awards==
*Nominee Best Costume Design - BAFTA (Anthony Mendleson)

==Trivia==
*Maurice Binder filmed a pre-credit prologue resembling a mosaic explaining the background of the bell.
*The Viking village built in Montenegro for the film set was left standing and was a local tourist attraction. 
*Because Poitier was playing a Moor, publicity for the film included the slogan, "Sidney Poitier in his first non-Negro role!"
*Speaking of the film later Poitier said, "To say it was disastrous is a compliment." 

==References==
 

==External links==
* 
* 
* , Time Magazine, June 12, 1964

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 