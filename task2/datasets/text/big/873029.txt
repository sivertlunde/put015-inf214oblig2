To Wong Foo, Thanks for Everything! Julie Newmar
{{Infobox film
| name = To Wong Foo, Thanks for Everything! Julie Newmar
| image = towongfoo.jpg
| caption = Theatrical release poster
| director = Beeban Kidron 
| producer = Walter F. Parkes Bruce Cohen Douglas Beane
| starring = Wesley Snipes Patrick Swayze John Leguizamo Stockard Channing
| music = Rachel Portman
| cinematography = Steve Mason
| editing = Andrew Mondshein
| studio = Amblin Entertainment Universal Pictures
| released =  
| runtime = 109 minutes
| country = United States
| language = English
| gross = $47,774,193 
}}
To Wong Foo, Thanks for Everything! Julie Newmar is a 1995 American comedy film, starring Wesley Snipes, Patrick Swayze, and John Leguizamo as three New York drag queens who embark on a road trip. The films title refers to a totemic autographed photo of Julie Newmar that the trio carries with them on their journey.

==Plot== New Yorks Drag Queen Los Angeles by car, carrying with them an iconic photo of Julie Newmar (signed, "To Wong Foo, thanks for everything! Julie Newmar") that Vida purloined from a restaurant wall.
 homophobic Sheriff Dollard (Chris Penn), who tries to force himself onto Vida. He discovers Vida is not biologically female and, in the commotion, Dollard falls backwards and is knocked unconscious. The trio think he is dead, and they hurry off and leave him behind. As they recover from the incident at a rest stop, their car breaks down. A young man, Bobby Ray (Jason London) from the nearby small town of Snydersville, happens by and gives them a ride, where they take refuge in a bed and breakfast owned by Carol Ann (Stockard Channing) and her abusive car repairman husband, Virgil (Arliss Howard).

The drag queens become stranded in the town for the weekend as they wait for the replacement part for their car to arrive. While there, they are confronted by the towns small-mindedness, though it focuses not on their Genderqueer identities, but rather on their status as females. Chi-Chi is harassed by a group of roughnecks, but is saved by Bobby Ray. While volunteering to help with the towns strawberry social, the queens decide the towns small band of women need a day with the girls, which consists of the following steps: get your hair done, pick out a new outfit, and then just sit in a cafe and talk.  While searching for the new outfits, the drag queens are ecstatic to find vintage fashions from the 1960s in the towns clothing store and give the towns female residents (and themselves) a make-over.

Following their make-over, the group is abused by the same roughnecks that attempted to attack Chi-Chi. Fed up, Noxeema handles the situation in a typically New York manner and teaches their ringleader a lesson in manners. Vida, Noxeema, and Chi-Chi do what they can to be positive, and they set out to improve the lives of the townspeople, including offering assistance in organizing the annual "Strawberry Social" event.

Meanwhile, Sheriff Dollard is ridiculed by his colleagues, who believe he was beat up by a girl. He goes in search of the drag queens.

Vida, meanwhile, becomes acutely aware of Carol Anns abuse at the hands of her husband Virgil and, shortly thereafter, the three queens overhear Virgil giving Carol Ann another beating. Vida decides to intervene and beats up Virgil before throwing him out of the house. The next day, Virgil runs into Sheriff Dollard at a bar and the two realize that the newcomers to town are the same people Dollard has been searching for. They head back to Snydersville, and Dollard demands that the townspeople turn over the drag queens.

However, the townspeople, who now realize the biological sex of their new friends, begin to protect them. One by one they step up and confront Dollard, each one claiming to be a drag queen (in similar fashion as in the film Spartacus (film)|Spartacus). Dollard is humiliated and flees. The Strawberry Social commences with everyone dressed in vibrant red outfits for the party. Shortly after, Carol Ann repairs their car and the three queens are finally able to leave Snydersville, though they are now slightly saddened to leave behind their new friends. Carol Ann reveals to Vida that she knew Vida was a drag queen all along due to her Adams Apple, which women do not normally visibly have. In honor of their friendship, Vida gives Carol Ann the photo of Julie Newmar that has accompanied the queens on their trip.

The trio eventually makes it to Los Angeles where Chi-Chi, after having received many tips from Vida and Noxeema during their ordeal, wins the title of Drag Queen of the Year. Fittingly, the crown is presented by Julie Newmar herself.

==Cast==
* Wesley Snipes as Noxeema Jackson
* Patrick Swayze as Vida Boheme
* John Leguizamo as Chi-Chi Rodriguez
* Stockard Channing as Carol Ann
* Blythe Danner as Beatrice
* Arliss Howard as Virgil
* Jason London as Bobby Ray
* Chris Penn as Sheriff Dollard
* Melinda Dillon as Merna
* Beth Grant as Loretta
* Alice Drummond as Clara
* Michael Vartan as Tommy
* Jennifer Milmore as Bobbie Lee
* Mike Hodge as Jimmy Joe 

===Cameo appearances===
* Julie Newmar as Herself
* Naomi Campbell as Girl at China Bowl restaurant Joseph Arias as Joey Arias
* Daniel T. Sweetie Boothe as Pageant Hostess
* Lady Catiria as Herself
* Laritza Dumont as Herself Alexander Heimberg as Miss Understood
* Candis Cayne as Herself (credited as Brendan McDanniel) Clinton Leupp as Miss Coco Peru Steven Polito as Hedda Lettuce Jon Ingle as The Lady Bunny
* Quentin Crisp as NY pageant judge
* Flotilla DeBarge as NY pageant judge
* Jose Sarria as NY Pageant Judge
* RuPaul as Rachel Tensions
* Robin Williams as John Jacob Jingleheimer Schmidt

==Awards== Best Actor Best Supporting Actor respectively.

==Production== Lincoln and Omaha, Nebraska|Omaha. Though greatly faded, a "Welcome to Snyderville" an outdoor mural remains in Loma. 

Champions Tour professional Chi Chi Rodriguez sued the production company and distributor over the use of his name in the movie. 

This American film is a parallel story/plot idea of the earlier 1994 Australian comedy-drama, The Adventures of Priscilla, Queen of the Desert which stars Hugo Weaving, Terence Stamp, and Guy Pearce in the roles of the three cross-country cabaret drag queens.

==Soundtrack==
# "I Am the Body Beautiful" - Salt-N-Pepa
# "Free Yourself" - Chaka Khan
# "Turn It Out" - Labelle
# "Who Taught You How" - Crystal Waters Tom Jones
# "Brick House" - The Commodores
# "Nobodys Body" - Monifah
# "Do What You Wanna Do" - Charisse Arrington
# "Girls Just Want to Have Fun#(Hey Now) Girls Just Want to Have Fun|(Hey Now) Girls Just Want to Have Fun" - Cyndi Lauper
# "Over the Rainbow" - Patti LaBelle
# "To Wong Foo Suite" - Rachel Portman

* Music and songs not included in the soundtrack:

# "Gotta Move" - Barbra Streisand
# "1812 Overture" - Tchaikovsky
# "Zampa Overture" - Ferdinand Herold
# "China Girl" - Robert J. Walsh
# "Theme from Wonder Woman"
# "That Lady Youre with Aint No Lady" - Larry Applewhite/Gene Wisniewski
# "Stand by Your Man" - David Allan Coe
# "This Is a Mans World" - Sara Hickman
# "Behind Closed Doors" - Charlie Rich
# "Hold Me, Thrill Me, Kiss Me" - Johnny Mathis
# "Another Somebody Done Somebody Wrong Song" - B. J. Thomas

==DVD==
A DVD is available with several deleted scenes.

==See also==
* Cross-dressing in film and television

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 