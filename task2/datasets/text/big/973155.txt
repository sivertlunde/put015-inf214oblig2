Shaft (2000 film)
{{Infobox film
| name = Shaft
| image = Shaft (2000 movie poster).jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = John Singleton
| producer = Mark Roybal Scott Rudin Eric Steel Adam Schroeder Richard Price
| story = John Singleton Shane Salerno
| based on =   Jeffrey Wright Christian Bale Richard Roundtree
| music = David Arnold
| cinematography = Donald E. Thorin John Bloom Antonia von Drimmelen Al Rodgers Scott Rudin Productions
| distributor = Paramount Pictures
| released =  
| runtime = 99 minutes
| country = United States
| language = English Spanish
| budget = $46 million 
| gross = $107,196,498 
}} American action Jeffrey Wright, 1971 film reboot was announced in February 2015.  Jacksons John Shaft character is the nephew of the original John Shaft. The film received mainly positive reviews and opened at the number one position at the box office when it debuted June 16, 2000.

==Plot== NYPD Detective racially motivated murder of Trey Howard (Mekhi Phifer), committed by Walter Wade Jr. (Christian Bale), the son of a wealthy real estate tycoon. Shaft briefly meets a potential eyewitness to the murder, Diane Palmieri (Toni Collette), but she disappears soon after and cannot be found for the trial. Wade Jr. is let off on bail and flees to Switzerland.
 Jeffrey Wright), Dominican drug lord. Wade Jr., his passport relinquished, is let off on bail again, and in frustration Shaft resigns from the police force, promising to bring Wade Jr. to justice on his own terms. Worried that Shaft might find the missing eyewitness, Wade Jr. hires Peoples to find and kill her first.

Shaft continues his search for Diane, enlisting the help of his friends Detective Carmen Vasquez (Vanessa L. Williams) and taxi driver Rasaan (Busta Rhymes). While visiting Dianes uncooperative mother, Shaft and Carmen realise they are being followed by officers Jack Roselli (Dan Hedaya) and Jimmy Groves (Ruben Santiago-Hudson), who have been paid by Peoples to follow Shaft and get to Diane. Shaft finally finds Diane, but before they can talk, they are attacked by Peopless men. In the shootout, Shaft kills Peoples younger brother. Shaft, Diane, Rasaan, and Dianes brother manage to escape to Rasaans apartment, but they are followed by Roselli and Groves. While at the apartment, Diane confesses that she saw the entire murder, and kept silent in return for a payoff from Wades father.
 abusive boyfriend. Shaft is initially reluctant, but when he sees her injury, he decides to help her anyway. Shaft, along with his uncle (Richard Roundtree), go together to face to the victim.

==Cast==
* Samuel L. Jackson as John Shaft II
* Vanessa L. Williams as Carmen Vasquez Jeffrey Wright as Peoples Hernandez
* Christian Bale as Walter Wade, Jr. John Shaft I, the uncle of this films Shaft, and the protagonist of the previous films.
* Pat Hingle as Judge Dennis Bradford
* Busta Rhymes as Rasaan
* Toni Collette as Diane Palmieri
* Dan Hedaya as Jack Roselli
* Ruben Santiago-Hudson as Jimmy Groves
* Josef Sommer as Curt Flemming
* Lynne Thigpen as Carla Howard
* Philip Bosco as Walter Wade, Sr.
* Lee Tergesen as Luger
* Daniel von Bargen as Lt. Kearney
* Peter McRobbie as Lt. Cromartie
* Zach Grenier as Harrison Loeb
* Richard Cocchiaro as Frank Palmieri
* Ron Castellano as Mike Palmieri
* Mekhi Phifer as Trey Howard
* Sonja Sohn as Alice
* Elizabeth Banks as Girl at Bar
* Andre Royo as Tattoo
* Issac Hayes (Uncredited)
The director of the original Shaft, Gordon Parks, appears in a cameo at the Lenox Lounge party as "Mr. P," as a homage by director John Singleton to the original film.

==Release==

===Critical reception===
Shaft received mixed to positive reviews, earning a 68% fresh rating on Rotten Tomatoes; the consensus states "With a charismatic lead, this new Shaft knows how to push the right buttons."  On Metacritic, which uses an average of critics reviews, the film holds a 50/100, indicating "mixed or average reviews". 

===Box office===
The film opened at the box office at #1 with $21,714,757; by the end of its run, Shaft had grossed $70,334,258 in the domestic box office and $107,196,498 worldwide. Based on a $46 million budget, the film can be considered a success.   

===Merchandise===

In 2000 McFarlane Toys released a Shaft (Samuel L. Jackson) action figure as part of their Movie Maniacs series three toy line. Accessories included are a handgun, sunglasses and a replica of the films poster with a skulls and bones base.

==Soundtrack==
  hip hop and R&B music was released on June 6, 2000 by LaFace Records. It peaked at #22 on the Billboard 200|Billboard 200 and #3 on the Top R&B/Hip-Hop Albums.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 