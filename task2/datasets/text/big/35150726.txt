Dandupalya (film)
 
 
{{Infobox film
| name           = Dandupalya
| image          = 
| caption        = 
| director       = Srinivasa Raj
| producer       = Prashanth G. R. Girish T.
| writer         = Srinivasa Raj
| screenplay     = Srinivasa Raj
| based on       =  
| starring       = Pooja Gandhi   Raghu Mukherjee   Priyanka Kothari
| music          = Arjun Janya
| cinematography = Venkat Prasad
| editing        = S. Manohar
| studio         = Apple Blossom Creations
| distributor    = 
| released       =  
| runtime        = 
| country        = India Kannada
| budget         = 
| gross          = 
}}
 Tamil as Karimedu.

== Cast ==
* Pooja Gandhi as Lakshmi
* Raghu Mukherjee
* Priyanka Kothari
* P. Ravi Shankar as Inspector Chalapathi
* Makarand Deshpande as Krishna
* Ravi Kale as Hanuma
* Jaidev as Thimma
* Karisubbu as Muniya
* Sudharani
* Bhavya
* Doddanna
* Ramesh Bhat
* Srinivasa Murthy
* Harish Rai
* Prathik
* Bullet Prakash
* Pradeep Gadhadhar

== Controversy ==
The film criticised by social group Bahujan Samaj Horata Samiti, who claimed that its content was offensive. They objected particularly to the films portrayal of women. Further controversy followed when it was revealed that actress Pooja Gandhi would appear topless in the film. 

After the release of the movie, Ambedkar Kranthi Sena activists demonstrated in Bangalore against the films "glorification of anti-social activities." 

== Soundtrack ==

{{Infobox album
| Name       = Dandupalya
| Longtype   =
| Type       = Soundtrack
| Artist     = Arjun Janya
| Cover      = 
| Border     = yes
| Alt        = 
| Caption    = 
| Released   = 2012
| Recorded   =  Feature film soundtrack
| Length     =  Kannada
| Label      = Sai Audio
| Producer   = 
| Last album = 
| This album = Dandupalya (2012)
| Next album = 
}}

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 
| lyrics_credits = yes
| title1 = Aleyo Alegale
| lyrics1 = V. Nagendra Prasad
| extra1 = Karthik (singer)|Karthik, Anuradha Bhat
| length1 = 
| title2 = Kalli Nanu
| lyrics2 = V. Nagendra Prasad
| extra2 = Vasundhara Das, Harsha Sadanandam
| length2 = 
| title3 = Police Theme
| lyrics3 = V. Nagendra Prasad
| extra3 = Ravi Basur
| length3 = 
| title4 = Yaare Neenu
| lyrics4 = V. Nagendra Prasad
| extra4 = Nakul, Priya Himesh
| length4 = 
| title5 = Dandupalya Theme
| lyrics5 = V. Nagendra Prasad
| extra5 = Instrumental
| length5 = 
}}

== Release and reception ==
Dandupalya received generally positive response from critics upon its theatrical release. The Times of India awarded the film 3 stars out of 5, praising the performances of Pooja Gandhi, Makarand Deshapande, Ravi Kale and Ravishankar.  The film received excellent returns on its opening week across Karnataka state. 

{{Album ratings
 
| rev1 =Rediff
| rev1Score =   
| rev2 =Bangalore Mirror
| rev2Score =    DNA
| rev3Score =   
||}}

== Dubbed Versions == Telugu and released as Dandupalyam. It was well received in Andhra Pradesh. 
 Tamil dub titled Karimedu was produced by Rama Narayanans Sri Thenandal Films and released on 24 May 2013 to critical acclaim.  

The film was dubbed in Malayalam and was titled as Kuruthikalam

==Awards==

{| class="wikitable"
|-
! Ceremony
! Category
! Nominee
! Result
|- 2nd South Indian International Movie Awards SIIMA Award Best Actor in a Supporting Role Raghu Mukherjee
| 
|- SIIMA Award Best Actor in a Negative Role Makarand Deshpande
| 
|- Pooja Gandhi
| 
|-
|}

== References ==
 

 
 
 
 
 