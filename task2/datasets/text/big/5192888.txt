Knock on Any Door
{{Infobox film
| name           = Knock on Any Door
| image          = KnockonAnyDoorPoster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Nicholas Ray
| producer       = Robert Lord
| screenplay     = John Monks Jr. Daniel Taradash
| based on       =  
| narrator       = 
| starring       = Humphrey Bogart John Derek
| music          = George Antheil
| cinematography = Burnett Guffey
| editing        = Viola Lawrence
| studio         = Santana Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Knock on Any Door is a 1949 American court-room trial film noir directed by Nicholas Ray and starring Humphrey Bogart. The picture gave actor John Derek a break in developing his film career and was based on the 1947 novel of the same name by Willard Motley. 

==Plot==
Against the wishes of his law partners, lawyer Andrew Morton (Humphrey Bogart) takes the case of Nick Romano (John Derek), a troubled young man from the slums, partly because he himself came from the same slums, and partly because he feels guilty for botching the criminal trial of Nicks father years earlier (he was innocent). Nick is on trial for viciously killing a policeman point-blank and faces execution if convicted (the event is shown in a dark opening scene, but the killers face is not seen).

Mortons strategy in the courtroom is to argue that slums breed criminals and that the community is partly to blame for crimes committed by the people who are forced to live in such miserable conditions. Through flashbacks, Morton demonstrates that Romano is more a victim of society than a natural-born killer. Yet, Mortons strategy does not have the desired result on the jury thanks to the badgering of District Attorney Kernan (George Macready). Morton, however, does manage to arouse sympathy for the plight of those trapped by birth and circumstance in a dead-end existence.

==Cast==
* Humphrey Bogart as Andrew Morton
* John Derek as Nick Romano
* George Macready as Dist. Atty. Kerman
* Allene Roberts as Emma
* Candy Toxton as Adele Morton Mickey Knox as Vito
* Barry Kelley as Judge Drake
* Cara Williams as Nelly Watkins
* Sid Melton as "Squint" Zinsky
* Dooley Wilson as Piano player
* Pepe Hern as Juan Rodriguez

==Background==
Producer   was reportedly furious at this, fearing that other stars would do the same and major studios would lose their power.

According to critic Hal Erickson, the often-repeated credo spoken by the character Nick Romano: "Live fast, die young, and have a good-looking corpse," would become the "clarion call for a generation of disenfranchised youth." 

==Reception==

===Critical response===
Bosley Crowther, film critic for The New York Times, called the film "a pretentious social melodrama" and blasted the film message and the screenplay.  He wrote, "Rubbish! The only shortcoming of society which this film proves is that it casually tolerates the pouring of such fraudulence onto the public mind. Not only are the justifications for the boys delinquencies inept and superficial, as they are tossed off in the script, but the nature and aspect of the hoodlum are outrageously heroized." 

The staff at Variety (magazine)|Variety magazine was more receptive of the film writing, "An eloquent document on juvenile delinquency, its cause and effect, has been fashioned from Knock on Any Door...Nicholas Rays direction stresses the realism of the script taken from Willard Motleys novel of the same title, and gives the film a hard, taut pace that compels complete attention." 

==Adaption==
In 1960 a sequel to the film, Let No Man Write My Epitaph, was produced and directed by Philip Leacock starring Burl Ives, Shelley Winters, James Darren, Ella Fitzgerald, among others.  It was based on the 1958 novel of the same name by Willard Motley.

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 