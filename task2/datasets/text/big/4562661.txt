Rio Rita (1929 film)
 
{{Infobox film name           = Rio Rita image          = RioRita1929.jpg caption        = Theatrical release poster producer       = William LeBaron   Florenz Ziegfeld Jr. director       = Luther Reed writer         = Luther Reed based on       =   starring  John Boles Dorothy Lee music  Joseph McCarthy (lyrics) Harry Tierney (music) cinematography = Robert Kurrle (Technicolor)  Lloyd Knechtel editing  William Hamilton studio  RKO Radio Pictures distributor    = RKO Radio Pictures released       =   runtime        = Originally 140 minutes; Surviving reissue print:  103 minutes language       = English country        = United States
| budget = $678,000 
| gross = $2,400,000 
}} John Boles King Kong (1933).  Its finale was photographed in Technicolor#Two-color Technicolor|two-color Technicolor.  Rio Rita was chosen as one of the ten best films of 1929 by Film Daily.

==Plot== Dorothy Lee). After the wedding, Ned Levitt (Robert Woolsey), Chicks lawyer, informs Chick the divorce was invalid, and advises Wheeler to stay away from his bride.
 John Boles) and local warlord General Ravenoff (Georges Renavent). Ranger Jim is pursuing the notorious bandit Kinkajou along the Rio Grande, but is reluctant to openly accuse Ritas brother, Roberto (Don Alvarado), as the Kinkajou because he is in love with Rita.

Ravenoff successfully convinces Rita to spurn Ranger Jim on the pretext that Jim will arrest Roberto. Rita unhappily agrees to marry Ravenoff to prevent him from exposing Roberto as the Kinkajou. Meanwhile, Wheelers first wife, Katie (Helen Kaiser), shows up to accuse him of bigamy, but conveniently falls in love with Woolsey.

At this point, the film switches into Technicolor. During the wedding ceremony aboard Ravenoffs private barge, Ranger Jim cuts the crafts ropes so that it drifts north of the Rio Grande. The Texas Rangers storm the barge, arrest Ravenoff as the real Kinkajou just in time to prevent the wedding, and Roberto is revealed to be a member of the Mexican Secret Service. Jim takes Ritas hand in marriage and Roberto escorts Ravenoff back to Mexico for trial.

==Principal cast==
*Bebe Daniels as Rita Ferguson John Boles as Capt. Jim Stewart
*Bert Wheeler as Chick Bean
*Robert Woolsey as Ned Lovett Dorothy Lee as Dolly Bean
*Don Alvarado as Roberto Ferguson
*Georges Renavent as General Ravinoff
*Helen Kaiser as Mrs. Katie Bean

==Production notes==
* Wheeler and Woolsey were the only principals from the stage version to appear in the film. Based on the success of this film, Wheeler & Woolsey were also given contracts to star in a series of comedies for Radio Pictures.  John Boles RCA Victor phonograph records.  Ziegfeld stage presentation was like. 
*The choreography for the grand finale on the barge was created by Pearl Eaton. 
* The 1942 Abbott & Costello "Rio Rita (1942 film)|remake" has little in common with this version.  Two songs, "Rio Rita" and "The Rangers Song", made an appearance, but the story line was so different, that the screenplay was credited as an original piece. 
* A version for television was produced by NBC in 1950. 

==Songs==
* "Youre Always in My Arms (But Only in My Dreams)"
* "Sweetheart We Need Each Other"
* "Following the Sun Around"
* "Rio Rita"
* "If Youre in Love Youll Waltz"
* "The Kinkajou"
* "The Rangers Song"

==Release==
Rio Rita was a box-office success. Earning an estimated profit of $935,000,  it was RKOs biggest grossing film of 1929. 

It was generally well-received by critics. Photoplay praised it as nearly "the finest of the screen musicals" and judged that director Reed had done well with a "difficult assignment".  Mordaunt Hall of the New York Times was more overtly disappointed in Reed: "(He) has contented himself in making virtually an audible animated photographic conception of the successful Ziegfeld show," wrote Hall, and noted that Daniels, though capable, was "not up to the standard set by Ethlin Terry in the stage version". Hall was otherwise appreciative of the lavish, if thinly-plotted, production. 

==Preservation==
Five reels of the film are believed to be lost. The print currently circulating (105 minutes) is the re-release version from 1932, which was edited significantly, taking the original length of fifteen reels down to only ten reels. This is the print that is currently being broadcast on cable by Turner Classic Movies, which is missing about forty minutes of footage. New Yorks Museum of Modern Art used to have a print of the original full-length version, but this print seems to have been lost or stolen from their archives. The entire soundtrack for the original film survives on Vitaphone disks.  Both picture and sound for at least two musical numbers from the long version are also known to survive ("When Youre In Love, Youll Waltz" and "The Kinkajou").

==See also==
*List of early color feature films
*List of incomplete or partially lost films

==References==
{{reflist|refs=
   
   
   
   
   
   
}}

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 