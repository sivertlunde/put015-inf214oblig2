Manual of Love
{{Infobox film
| name           = Manual of Love
| image          = Manuale_damore_poster.jpg
| image size     =
| caption        = Italian poster
| director       = Giovanni Veronesi
| producer       = Aurelio De Laurentiis
| writer         = Vincenzo Cerami Ugo Chiti Giovanni Veronesi
| narrator       =
| starring       = Silvio Muccino Carlo Verdone Margherita Buy Jasmine Trinca Sergio Rubini Luciana Littizzetto
| music          = Paolo Buonvino
| cinematography = Giovanni Canevari
| editing        = Claudio Di Mauro
| distributor    =
| released       =  
| runtime        = 116 min
| country        = Italy
| language       = Italian
| budget         =
}}

 2005  Italian blockbuster romantic comedy film in four quartets. It was directed by  Giovanni Veronesi, who made two sequels, Manuale damore 2 – Capitoli successivi in 2007 in film|2007, and Manuale damore 3 in 2011 in film|2011.

==Plot==
Each of the four episodes follows a different couple, focusing on a particular stage of a typical love relationship:

*Innamoramento (falling in love): Giulia and Tommaso (Jasmine Trinca and  Silvio Muccino) meet and fall in love;

*Crisi (crisis): Barbara e Marco (Margherita Buy and Sergio Rubini) are a couple going through a crisis, close to divorce;

*Tradimento (cheating): A traffic police woman (Luciana Littizzetto) is cheated on by her husband and takes revenge;

*Abbandono (break-up): Goffredo (Carlo Verdone) is a rich doctor abandoned by his wife.

The characters in the movie are all related in one way or another, and each transition brings background characters from a previous episode to be the protagonists of the next episode. The film is set in Rome.

==Cast==
*Carlo Verdone: Goffredo
*Luciana Littizzetto: Ornella
*Silvio Muccino: Tommaso
*Sergio Rubini: Marco
*Margherita Buy: Barbara
*Jasmine Trinca: Giulia
*Rodolfo Corsato: Alberto Marchese
*Dino Abbrescia: Gabriele
*Dario Bandiera: Piero
*Luis Molteni: avvocato di Goffredo
*Sabrina Impacciatore: Luciana
*Anita Caprioli: Livia
*Francesco Mandelli: Dante

==Awards==
*2 Nastro dArgento: Best Supporting Actor (Carlo Verdone), Best Screenplay.
*2 David di Donatello Best Supporting Actor (Carlo Verdone), Best Supporting Actress (Margherita Buy).

== External links ==
*  

 
 
 
 
 
 
 
 
 

 
 