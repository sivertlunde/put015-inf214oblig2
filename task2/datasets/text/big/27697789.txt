Revenge of the Zombies
 
{{Infobox film
| name           = Revenge of the Zombies
| image          = Revenge-of-the-Zombies.jpg
| alt            = 
| caption        = 
| director       = Steve Sekely
| producer       = Lindsley Parsons
| writer         = {{plainlist|
* Edmond Kelso
* Van Norcross
}}
| starring       = {{plainlist|
* John Carradine
* Gale Storm
}}
| music          = 
| cinematography = Mack Stengler
| editing        = Richard C. Currier
| studio         = Edward J. Kay
| distributor    = Monogram Pictures
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Revenge of the Zombies is a 1943 horror film directed by Steve Sekely, starring John Carradine and Gale Storm.  Dr. Max Heinrich von Altermann (John Carradine), is a mad scientist working to create a race of living dead warriors for the Third Reich.

This was a follow-up to the horror-comedy King of the Zombies (1941) with Mantan Moreland reprising his role as Jeff and Madame Sul-Te-Wan returning as a different character.

== Plot ==
 
After the death of Maxs (John Carradine) wife Lila (Veda Ann Borg), he holds a funeral for her. However, he has also turned her into a zombie. He is amazed when Lila show signs of free will and challenges him for control. In the excitement Dr. Keating (Barry Macollum) goes missing after entering a tomb which should not have been entered.

During dinner, Scott Warrington (Mauritz Hugo) finds a radio in Maxs cabinet, and figures out that it communicates to Hitler. Max learns of this and gags and ties up Scott. Lazarus (James Baskett), Maxs right-hand man, finds a gun. While making soup with Rosella (Sybil Lewis), Jeff (Mantan Moreland) finds Warrington gagged in a closet, and he tells Jeff about the situation. Max discovers this and tries to flee the swamp. Lila and the hordes of zombies pursue Max, and both Max and Lila end up sinking into quicksand.

== Cast ==
* John Carradine as Dr. Max Heinrich von Altermann
* Gale Storm as Jennifer Rand Robert Lowery as Larry Adams Bob Steele as United States agent posing as Sheriff
* Mantan Moreland as Jeff
* Veda Ann Borg as Lila von Altermann
* Barry Macollum as Dr. Harvey Keating
* Mauritz Hugo as Scott Warrington
* Madame Sul-Te-Wan as Mammy Beulah, the housekeeper
* James Baskett as Lazarus
* Sybil Lewis as Rosella Robert Cherry as Pete, a zombie
* Franklyn Farnum as zombie

== Reception ==
Writing in The Zombie Movie Encyclopedia, Peter Dendle wrote that it is the first zombie film to presume that audiences know what a zombie is.  Dendle called it a remake of King of the Zombies and "stock fare from the Monogram horror mill." 

== References ==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 