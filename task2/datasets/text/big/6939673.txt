The Thing Called Love
{{Infobox film
| name           = The Thing Called Love
| image          = ThingcalledloveDVD.jpg
| caption        = Directors Cut DVD cover
| director       = Peter Bogdanovich John Davis
| writer         = Carol Heikkinen
| starring       = Samantha Mathis River Phoenix Sandra Bullock Dermot Mulroney
| music          = G. Marq Roswell Peter James
| editing        = Terry Stokes
| distributor    = Paramount Pictures
| released       =  
| runtime        = 116 min./120 min. (directors cut)
| country        = United States English
| budget         = $14,000,000 (est.)
| gross          = $1,029,721 (USA)
}}

The Thing Called Love is an American comedy-drama film released in 1993 in film|1993. It was directed by Peter Bogdanovich. The films tagline is: "Stand by your dream."
 The Bluebird Cafe. She is not invited to perform, but she accepts a job as a waitress. She meets and falls in love with James Wright (River Phoenix), and she befriends Linda Lue Linden (Sandra Bullock).

While the movie involves a love triangle and various complications in Mirandas route to success, it provides a sweetened glimpse at the lives of aspiring songwriters in Nashville.

This was Phoenixs final complete screen performance before his death.

A "making of" documentary is available on the films DVD release, titled The Thing Called Love: A Look Back. 

==Plot==
Miranda Presley is an aspiring singer/songwriter from New York City who loves country music and decides to take her chances in Nashville, Tennessee, where she hopes to become a star. After arriving in Music City after a long bus ride, Miranda makes her way to the Bluebird Cafe, a local watering hole with a reputation as a showcase for new talent. The bars owner, Lucy, takes a shine to the plucky newcomer and gives her a job as a waitress.

Before long, Miranda has gotten to know a number of other Nashville transplants who are looking to land a gig or sell a song, including sweet and open-hearted Kyle Davidson, moody but talented James Wright, and spunky Linda Lue Linden. As the four friends struggle to find their place in the competitive Nashville music scene, both Kyle and James display a romantic interest in Miranda, but she is drawn to James in spite of his moody temperament. Miranda pursues James, and they end up getting married, but they soon realize marriage takes work. James leaves Miranda behind to make his album, what he always wanted to do, but realizes he left his heart with her. He comes back to the Bluebird Cafe but discovers that Miranda has left town. Miranda returns and sings a new song, before tentatively reuniting with James. Kyle joins them as Linda Lue leaves for New York, and the remaining three discuss writing a song together.

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| River Phoenix || James Wright
|-
| Samantha Mathis || Miranda Presley
|-
| Dermot Mulroney || Kyle Davidson
|-
| Sandra Bullock || Linda Lue Linden
|-
| K. T. Oslin || Lucy
|- Anthony Clark || Billy
|-
| Webb Wilder || Ned
|-
| Deborah Allen || Herself
|-
| Jo-El Sonnier || Himself
|-
| Pam Tillis || Herself
|-
| Vern Monnett || Himself
|-
| Kevin Welch || Himself
|-
| Trisha Yearwood || Herself
|-
| Carol Grace Anderson || Diner Waitress
|-
| Garth Shaw || Diner Patron
|-
| Jimmie Dale Gilmore  || Himself
|}

==Reception==
The film received mixed reviews from critics, as it currently holds a 57% rating on Rotten Tomatoes based on 21 reviews.

==References==

 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 