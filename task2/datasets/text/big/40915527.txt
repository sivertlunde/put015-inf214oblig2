Guaguasi
 
{{Infobox film
| name           = Guaguasi
| image          = 
| caption        = 
| director       = Jorge Ulla
| producer       = 
| writer         = Orestes Matacena Clara Hernandez
| starring       = Orestes Matacena
| music          = 
| cinematography = Ramón F. Suárez
| editing        = 
| distributor    = 
| released       =  
| runtime        = 107 minutes
| country        = Dominican Republic
| language       = Spanish
| budget         = 
}}
 Best Foreign Language Film at the 56th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Orestes Matacena as Guaguasi
* Marilyn Pupo as Marina
* Raimundo Hidalgo-Gato as Moya
* Marco Santiago as Raul
* Rolando Barral as Cmndt. Jorge Montiel
* Clara Hernandez as Isabel
* Jose Bahamonde as Flor
* Oswaldo Calvo as Col. Acosta
* Mercedes Enriquez as Elisa

==See also==
* List of submissions to the 56th Academy Awards for Best Foreign Language Film
* List of Dominican submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 

 
 