Victoire Terminus
{{Infobox film
| name           = Victoire Terminus
| image          = 
| caption        = 
| director       = Renaud Barret, Florent de La Tullaye
| producer       = Sciapode
| writer         = 
| starring       = 
| distributor    = 
| released       = 2008
| runtime        = 80 minutes
| country        = France
| language       = 
| budget         = 
| gross          = 
| screenplay     = Renaud Barret, Florent de La Tullaye
| cinematography = Renaud Barret, Florent de La Tullaye
| editing        = Yannick Coutheron
| music          = 
}}

Victoire Terminus is a French 2008 documentary film about womens boxing in Kinshasa.   

== Synopsis == Tata Rafael stadium; the same one where Muhammad Ali knocked out George Foreman in 1974 during one of the most legendary matches in boxing. At dawn, thousands of people from the ghetto come to train and political parties rally. While others fight for the Presidency of Congo, Judex struggles to organise a woman’s boxing tournament with very little money... Kinshasa sings, Kinshasa starves and Judexs girls try to survive, they’re realistic but still full of hope. A film about women in a country where men have gone crazy.   

== Awards ==
* London Film Festival (2008)

== References ==
 
 

==External links==
 

 
 
 
 
 
 
 
 
 
 


 
 