The Mouse on the Moon
{{Infobox film
| name           = The Mouse on the Moon
| image          = Motmpos.jpg
| image_size     =
| caption        = Original film poster
| director       = Richard Lester
| producer       = Walter Shenson
| writer         = Michael Pertwee
| narrator       =
| starring       = Margaret Rutherford Bernard Cribbins Terry-Thomas
| music          = Ron Grainer
| cinematography = Wilkie Cooper
| editing        =
| distributor    = United Artists
| released       =  
| runtime        = 100 min.
| country        = United Kingdom
| language       = English
| budget         =
}} The Mouse Irish author The Mouse That Roared. In it, the people of the Duchy of Grand Fenwick, a microstate in Europe, attempt space flight using wine as a propellant. It satirises the space race, Cold War and politics.

Peter Sellers, who had played three roles in the first film, did not return for this sequel and was replaced by Margaret Rutherford, Ron Moody and Bernard Cribbins. Likewise Leo McKern did not replay his role of Benter; this part was played by Roddy McMillan. The film also featured June Ritchie  and Terry-Thomas, with David Kossoff reprising his role as Professor Kokintz.

==Plot==
Financial disaster looms for Grand Fenwick when the current vintage of its only export, wine, starts exploding in would-be consumers faces. Prime Minister Mountjoy (Ron Moody) decides to ask the United States for a loan, ostensibly to fund its entry in the race to the Moon, but actually to save the duchy (and install modern plumbing so he can have a hot bath). The devious politician knows that the Americans will not believe him, but will consider the half million dollars he is asking for to be cheap propaganda supporting their hollow call for international co-operation in space. He is delighted when they send him double the amount as an outright gift. The Soviets, not wishing to be one-upped by their Cold War rivals, deliver an obsolete rocket. Mountjoy asks resident scientist Professor Kokintz (David Kosoff) to arrange a small explosion during the "launch" of their lunar rocket to make it look like they have actually spent the money as intended.

Meanwhile, Mountjoys son Vincent (Bernard Cribbins) returns after being educated in England. Mountjoy is disappointed to find that Vincent has picked up the British sense of fair play and the ambition to be an  , and reports back to his bosses that it is all a hoax.

Mountjoy invites the Americans, Soviets, and British to the launching. To everyones surprise, the rocket leisurely takes off with Kokintz and Vincent aboard. Kokintz calculates it will take three weeks to reach the Moon. Humiliated, the Americans and Soviets decide to risk sending their own manned rockets, timing it so they will land at the same time as (or a little before) Grand Fenwicks ship. However, Vincent accidentally hits a switch, speeding up the vessel, and he and Kokintz become the first to set foot on the Moon. The latecomers are greatly disappointed. When the Americans and Soviets try to race home to salvage some sort of propaganda coup, they enter the wrong ships and each descend deep into the lunar dust. The American and Soviet spacemen have to hitch a ride with Kokintz and Vincent.

They return to Grand Fenwick during a memorial ceremony (they had been out of radio contact for weeks and presumed lost). The diplomats immediately begin squabbling about who reached the Moon first.

==Cast==
*Margaret Rutherford as Grand Duchess Gloriana XIII
*Ron Moody as Prime Minister Rupert Mountjoy
*Bernard Cribbins as Vincent Mountjoy
*David Kossoff as Professor Kokintz
*Terry-Thomas as Maurice Spender (as Terry Thomas)
*June Ritchie as Cynthia, Mountjoys protester niece and Vincents love interest
*John Le Mesurier as British delegate John Phillips as Bracewell, the American delegate
*Eric Barker as MI5 man
*Roddy McMillan as Benter
*Tom Aldredge as Wendover
*Michael Trubshawe as British aide
*Peter Sallis as Russian delegate
*Clive Dunn as Bandleader
*Hugh Lloyd as Plumber
*Graham Stark as Standard Bearer 
*Mario Fabrizi as Mario, the Valet
*Jan Conrad as Russian Aide 
*John Bluthal as Von Neidel  Archie Duncan as USAF General 
*Guy Deghy as Russian Scientist 
*Richard Marner as Russian Air Force General 
*Allan Cuthbertson as Member of Whitehall Conference 
*Robin Bailey as Member of Whitehall Conference John Wood as Countryman

==Production== A Hard Days Night.

The film had its American premiere at Cape Canaveral, where the attending cast members met American astronauts.

Dell Publishing issued a comic book of the film.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 