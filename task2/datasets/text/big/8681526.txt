Jet Lag (film)
{{Infobox film
| name           = Jet Lag
| image          = Decalagehoraire.jpg
| caption        = French movie poster
| director       = Danièle Thompson
| producer       = Alain Sarde Christophe Thompson Sergi López
| music          = Éric Serra
| cinematography = Patrick Blossier
| editing        = Sylvie Landra
| distributor    = Studio Canal France Miramax USA
| released       =  
| runtime        = 91 minutes
| country        = France
| language       = French
| budget         =
| gross          = 9,090,045 €
}}
Jet Lag ( ) is a 2002 film starring Juliette Binoche and Jean Reno. It is the second film directed by Danièle Thompson, after the 1999 release La Bûche.

==Plot==
At Charles de Gaulle Airport in Paris, a French beautician (Juliette Binoche) on her way to a new job in Mexico accidentally meets a French chef (Jean Reno) who has been delayed on his way to Germany from his residence in the United States. Labor strikes, bad weather, and pure luck cause the two of them to share a room overnight at the airport Hilton hotel. Their initial mutual indifference and downright hostility evolves into romance and a re-examination of their lives.

==Cast==
(in credits order)
* Juliette Binoche as Rose
* Jean Reno as Félix Sergi López as Sergio
* Scali Delpeyrat as The Doctor
* Karine Belly as Air France Attendant
* Raoul Billerey as Félixs Father
* Nadège Beausson-Diagne as A Roissy Passenger
* Alice Taglioni as Ground Hostess
* Jérôme Keen as The Concierge
* Sébastien Lalanne as The Barman
* Michel Lepriol as The Waiter
* Mbembo as Post Office Employee (as Mbembo)
* Laurence Colussi as Hostess
* Lucy Harrison as Hostess
* Rebecca Steele as Hostess
* Thiam - Hostess

==Accolades==
; César Awards 2003
* Nomination — Juliette Binoche - Best Actress

==Production==
Danièle Thompson originally wrote the script for Miramax Films in the early 1990s with Isabelle Adjani attached to star in an English-language version.(source: allocine.fr)

Thompson obtained permission to film at Charles de Gaulle Airport in Paris prior to September 11, 2001. After the terrorist attacks, permission was withdrawn. Thompson then obtained permission to use Lourdes Airport, but was not convinced that viewers would believe it was Charles de Gaulle. Eventually she managed to gain 10 shooting days access to Paris. The rest was filmed on sets and in Libby Airport.

Décalage Horaire is the second collaboration of mother and son writing team Danièle Thompson & Christophe Thompson after the 1999 film La Bûche.

Décalage Horaire is one of the rare comedies in Juliette Binoches career, after Les Nanas (1985, Annick Lanoë) A Couch in New York (1995, Chantal Akerman) and Chocolat (2000 film)|Chocolat (2000, Lasse Hallstrom).

==External links==
 
*  
*  

 
 
 
 
 
 
 

 
 