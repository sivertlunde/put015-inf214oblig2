Insomnia (2002 film)
{{Infobox film
| name                 = Insomnia
| image                = Insomnia2002Poster.jpg
| caption              = Theatrical release poster
| director             = Christopher Nolan
| producer             = {{Plain list|
* Paul Junger Witt
* Edward L. McDonnell
* Broderick Johnson Andrew A. Kosove
}}
| screenplay           = Hillary Seitz
| based on             =  
| starring             = {{Plain list|
* Al Pacino
* Robin Williams
* Hilary Swank
* Maura Tierney
* Martin Donovan
* Nicky Katt
* Paul Dooley
 
}}
| music                = David Julyan
| cinematography       = Wally Pfister
| editing              = Dody Dorn
| production companies = {{Plain list|
* Alcon Entertainment
* Witt/Thomas Productions
* Section Eight Productions
* Insomnia Productions
}}
| distributor          = Warner Bros. Pictures 
| released             =  
| runtime              = 118 minutes 
| country              = United States 
| language             = English
| budget               = $46 million   
| gross                = $114.7 million 
}}
 Norwegian film of the same name, Insomnia was released on May 24, 2002, to critical acclaim and commercial success, grossing $114 million worldwide. To date, this is the only film that Christopher Nolan has directed without receiving at least a share of one of the writing credits, even though he wrote the final draft of the script. He is also praised for his wonderful remaking from the film director of the original.

==Plot== LAPD detectives Will Dormer (Al Pacino) and Hap Eckhart (Martin Donovan) are sent to assist the local police with their investigation, at the request of police chief Nyback (Paul Dooley), an old colleague of Wills.
 Internal Affairs immunity deal in exchange for his testimony regarding one of Dormers past cases. Eckhart says that he has no choice but to accept the deal, to Dormers frustration.

Focusing on the Nightmute case, Dormer comes up with a plan to lure the murderer back to the scene of the crime. The attempt fails, however, and the suspect flees into the fog. The police chase, and the suspect shoots one through the leg. Dormer soon fires at a figure in the fog. On his way to the fallen figure, he picks up a .38 pistol the suspect has dropped. He then approaches the figure on the ground, only to discover that he has shot Eckhart. Eckhart accuses Dormer of murdering him as he dies. Because of Eckharts pending testimony against Dormer; Dormer knows that Internal Affairs will never believe the shooting was an accident. Dormer tells his colleagues Eckhart was shot by the suspect. He doesnt mention he has the .38. 

Ellie Burr (Hilary Swank), a young police officer, is put in charge of the investigation of Eckharts shooting. Police comb the scene and find the bullet that sliced the first officers leg. Its a .38.

At night, Dormer walks to an alley and fires the .38 pistol into an animal carcass. He takes the bullet out of the dead animal and cleans it. Later he visits the morgue where Eckharts body lies. The staffer hands him the bagged bullet retrieved from Eckharts body. She is unfamiliar with its type. Dormer leaves and switches the bullet with one from the .38.
 perpetual daylight. Dormer then starts receiving anonymous phone calls from the suspect, who claims to have witnessed Dormer kill his partner. The police are aware that Kay was a fan of a crime writer named Walter Finch (Robin Williams) after looking through her belongings. One of the books says Finch lives in Alaska, so Dormer looks up his address and breaks into his apartment. Finch soon comes home, realises the police have arrived, and evades Dormer after a chase. 

Dormer returns to Finchs apartment. While there, he plants the .38, which would frame Finch for the shooting of Eckhart.
 abusive boyfriend Jonathan Jackson) and will stay silent about Dormers role in the Eckhart shooting in return. Dormer gives advice on handling police questioning. After Finch has stepped off the ferry with Dormer still on board, he shows the detective a tape recorder he used to record the conversation.

Finch calls Dormer and tells him that Kays death was "an accident"—he beat her to death in a fit of rage after she rejected his advances. The next day, Finch, under Dormers instruction, gives false testimony at the station. Yet he surprises the detective when he says Randy had a gun. Dormer realises Finch discovered his plant and has hidden it at Randys home. Finchs cunning could send Randy to jail and give the writer the upper hand over Dormer. Dormer soon races to Randys place to find the gun before other officers, but is unsuccessful. Randy is arrested. Finch offers to give Burr letters indicating that Randy abused Kay, and asks her to come and collect evidence from his second home the next day.
 shell casing at the scene, which conflicts with the bullet type found in Eckharts body. She reads old case files from investigations Dormer was involved in and learns he has carried a 9mm. She begins to suspect Dormer has been lying about who shot Eckhart. 

Dormer returns to his hotel for one last night, where he confides in the hotel owner, Rachel Clement (  he was certain was guilty of murdering a child, and who would have been set free if Eckhart had testified. Clement refuses to pass judgement on Dormer, intimating that she moved to Alaska because she was "getting away from something".

Upon returning to Finchs apartment, Dormer discovers that Finch has gone to meet Burr, and realises that Finch intends to kill her after finding Kays letters were in the apartment.

Burr sees the dress and while unholstering her gun is knocked unconscious by Finch.Dormer eventually reaches the cabin, but is too disoriented to fight off Finch from lack of sleep. Burr saves Dormer, while Finch escapes.

At gunpoint, reveals she knows he shot Eckhart. Dormer admits that he shot his partner, but says he is no longer certain if it was an accident. From his shed, Finch shoots at them, and Burr returns fire, allowing Dormer to sneak around to Finchs location. A struggle ensues in which Finch and Dormer shoot each other, killing the former and fatally wounding the latter.

Burr rushes to Dormers aid, and then comforts him by affirming that Eckharts shooting was accidental, and moves to throw away the shell casing evidence to preserve Dormers secret. Dormer stops Burr, telling her not to lose her way, before he dies muttering he just wanted to sleep. After a brief moment of contemplation, Burr slips the shell casing back into its plastic evidence bag.

==Cast==
* Al Pacino as Detective Will Dormer
* Robin Williams as Walter Finch
* Hilary Swank as Ellie Burr
* Maura Tierney as Rachel Clement
* Martin Donovan as Detective Hap Eckhart
* Nicky Katt as Fred Duggar 
* Paul Dooley as Chief Charlie Nyback
* Crystal Lowe as Kay Connell
* Jay Brazeau as Francis
* Larry Holden as Farrell
* Kerry Sandomirsky as Trish Eckhart
* Lorne Cardinal as Rich
* Katharine Isabelle as Tanya Francke Jonathan Jackson as Randy Stetz
* Paula Shaw as the Coroner

==Reception== weighted mean score of 7.7/10.  On Metacritic, the film holds an average score of 78 out of 100, based on 36 reviews. 

Lou Lumenick of the New York Post gave the film an enthusiastic review, calling it a "four-course gourmet alternative to summer popcorn flicks, serving up the meatiest performances Al Pacino and Robin Williams have given in many years." Roger Ebert of the Chicago Sun-Times noted that "Unlike most remakes, the Nolan Insomnia is not a pale retread, but a re-examination of the material, like a new production of a good play." 

Erik Skjoldbjærg, the director of the original film, said of Nolans reinterpretation: "Well I havent seen it for quite a while, but when I first saw it was a very strange experience because it was quite close, stylistically, to the original. I felt lucky that its such a well crafted, smart film and that it had a really good director handling it, because as a remake I think it did really well and it doesnt hurt any original if a remake is well done. So I felt I was lucky that Christopher Nolan took it upon himself to do it." 

==Novelization==
Robert Westbrook adapted the screenplay to novel form, which was published by Alex in May 2002. 

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 